//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	net_clubhouse_private.sch									//
//		AUTHOR			:	Online Technical Team: A.Tabrizi, 							//
//		DESCRIPTION		:	Header file that combines all the common functionality 		//
//							for club house.												//
//							This deals with multi-player data.							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_include.sch"
USING "net_spawn_debug.sch"
USING "net_events.sch"
USING "commands_network.sch"
USING "context_control_public.sch"
USING "net_ambience.sch"
USING "net_nodes.sch"
USING "fmmc_launcher_menu.sch"
USING "net_freemode_cut.sch"
USING "net_doors.sch"
USING "net_personal_vehicle_menu.sch"
USING "dialogue_public.sch"
USING "net_gang_boss.sch"
USING "net_fps_cam.sch"
USING "rc_helper_functions.sch"

#IF IS_DEBUG_BUILD
USING "shared_debug.sch"
#ENDIF

USING "net_gun_locker.sch"
USING "apartment_minigame_general.sch"

//╒══════════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════════╡ CONSTS ╞═══════════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════════════════╛
CONST_INT CLUB_HOUSE_BS_IS_CAR_MOD_SCRIPT_READY							0
TWEAK_FLOAT	fCONST_CLUBHOUSE_MISSION_TRIGGER_RADIUS						2.0

//INT iUpgradeTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT

//╒══════════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ ENUM & STRUCT ╞═══════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════════════════╛

STRUCT CLUBHOUSE_STRUCT
	INT iLocalBS
	STRING sCarModScript
	THREADID CarModThread
ENDSTRUCT

STRUCT CLUBHOUSE_MENU_INPUT
	INT iLeftX, iLeftY, iRightX, iRightY, iPrevRightX[9], iPrevRightY[9]
	INT iMoveUp, iCursorValue, iFocusResetTime
	BOOL bAccept, bBack
	BOOL bLeft, bRight
	BOOL bUp, bDown
	FLOAT fPrevMouseX, fPrevMouseY
	VECTOR vCamNudge
ENDSTRUCT

ENUM CLUBHOUSE_MISSION_MENU_STATE
	CHM_MENU_STAGE_INIT,
	CHM_MENU_STAGE_WAITING_TO_TRIGGER,
	CHM_MENU_STAGE_ENTER_ANIM,
	CHM_MENU_STAGE_SELECT_MISSION,
	CHM_MENU_STAGE_EXIT_ANIM,
	CHM_MENU_STAGE_CLEANUP
ENDENUM

/// PURPOSE: Clubhouse MC pinbpard states
ENUM CLUBHOUSE_MISSION_WALL_STATES
	CLUBHOUSE_MISSION_WALL_STATE_LINK_RT = 0,
	CLUBHOUSE_MISSION_WALL_STATE_UPDATE_SCALEFORM,
	CLUBHOUSE_MISSION_WALL_STATE_DRAW_WALL
ENDENUM


STRUCT structPointCam
	VECTOR vCoord
	VECTOR vPoint
ENDSTRUCT


ENUM eCHM_BITSET
	eCHMBITSET_MISSION_CHANGED_AWAIT_CAM = 0,		// 0
	eCHMBITSET_CAM_CHANGED_UPDATE_MOVIE,			// 1
	
	eCHMBITSET_END // Keep this at the end
ENDENUM

STRUCT CLUBHOUSE_MISSION_MENU_STRUCT
	CLUBHOUSE_MISSION_MENU_STATE eMenuState
	INT iCHMissionMenuContext = NEW_CONTEXT_INTENTION
	INT iMenuCurrentColumn, iMenuCurrentRow
	MP_PROP_OFFSET_STRUCT sTriggerOffset
	TEXT_LABEL_23 tl23TriggerRoom
	CLUBHOUSE_MENU_INPUT sCHMenuInput
	TIME_DATATYPE scrollDelay
	TEXT_LABEL tlPrintedHelp
	
	CLUBHOUSE_MISSION_ENUM eCHMissions[iCONST_NUM_CLUBHOUSE_MISSIONS]
	BOOL bMissionAvailable[iCONST_NUM_CLUBHOUSE_MISSIONS]
	CLUBHOUSE_MISSION_ENUM eSelectedMission
	INT iCurrentMission, iHighlightedMission
	INT iUpdatingMissionSelectionBitset
	
	CAMERA_INDEX ciCam
	structPointCam sStoredOffset, sCurrentOffset
	FLOAT fCamZoomAlpha = 0.0
	BOOL bZoomToggle = FALSE, bFlickedIn = FALSE
	SCRIPT_TIMER timerCutsceneTotal
	SCRIPT_TIMER timerPostCleanupButtonDelay
	
	INT iRenderTargetID = -1
	PLAYER_INDEX pOwnerID
	SCALEFORM_INDEX sScaleform
	CLUBHOUSE_MISSION_WALL_STATES eWallStates = CLUBHOUSE_MISSION_WALL_STATE_LINK_RT
	
	FLOAT fCentreX	= 0.5	//
	FLOAT fCentreY	= 0.5	//
	FLOAT fWidth	= 1.0	//1280x	(not 1024x)
	FLOAT fHeight	= 1.0	//720x	(not 1024x)
	
	INT iToggleClubhouseMissionWall = -1
	
	#IF IS_DEBUG_BUILD
	BOOL bDebugDraw
	BOOL bDebugUpdateMenu
	BOOL bDebugInvalidateAll
	#ENDIF
ENDSTRUCT

ENUM CLUBHOUSE_DISPLAY_EMBLEMS_STATE
	CHM_EMBLEMS_STATE_INIT,
	CHM_EMBLEMS_STATE_LINK_RT,
	CHM_EMBLEMS_STATE_UPDATE_SCALEFORM
ENDENUM

STRUCT CLUBHOUSE_DISPLAY_EMBLEMS_STRUCT
	CLUBHOUSE_DISPLAY_EMBLEMS_STATE eEmblemState
	PLAYER_INDEX pOwnerID
	INT iCrewID
	INT iRenderTargetID = -1
	TEXT_LABEL_63 tlCrewTxd
	NETWORK_CLAN_DESC memberInfo
	BOOL bRestrictedAccount
	BOOL bRequestCrewEmblem = FALSE
ENDSTRUCT

ENUM CLUBHOUSE_MEMORIAL_WALL_STATE
	CHM_MEMORIAL_WALL_STATE_INIT,
	CHM_MEMORIAL_WALL_STATE_LINK_RT,
	CHM_MEMORIAL_WALL_STATE_UPDATE_SCALEFORM
ENDENUM

STRUCT CLUBHOUSE_MEMORIAL_WALL_STRUCT
	CLUBHOUSE_MEMORIAL_WALL_STATE eWallState
	PLAYER_INDEX pClubhouseOwner
	PLAYER_INDEX pParticipantId
	PLAYER_INDEX pPresidentID
	PLAYER_INDEX pVPresidentID
	PLAYER_INDEX pRoadCaptainID
	PLAYER_INDEX pSergeantID
	PLAYER_INDEX pEnforcerID
	PEDHEADSHOT_ID pedPresidentHeadShotID
	PEDHEADSHOT_ID pedVPresidentHeadShotID
	PEDHEADSHOT_ID pedRoadCaptainHeadShotID
	PEDHEADSHOT_ID pedSergeantHeadShotID
	PEDHEADSHOT_ID pedEnforcerHeadShotID
	TEXT_LABEL_63 tlPresidentHeadShot
	TEXT_LABEL_63 tlVPresidentHeadShot
	TEXT_LABEL_63 tlRoadCaptainHeadShot
	TEXT_LABEL_63 tlSergeantHeadShot
	TEXT_LABEL_63 tlEnforcerHeadShot
	INT iGangMemberCounter
	INT iPresidentRT   = -1
	INT iVPresidentRT  = -1
	INT iRoadCaptainRT = -1
	INT iSergeantRT    = -1
	INT iEnforcerRT    = -1
	INT iParticipant
	INT iTexturesRequested
	INT iTexturesLoaded
	INT iTotalGangMembers
	INT iValidPlayerTotal
	INT iPedHeadshotTotal
ENDSTRUCT

ENUM CLUBHOUSE_LAPTOP_MONITOR_STATE
	CHM_LAPTOP_MONITOR_STATE_INIT,
	CHM_LAPTOP_MONITOR_STATE_LINK_RT,
	CHM_LAPTOP_MONITOR_STATE_UPDATE_SCALEFORM
ENDENUM

STRUCT CLUBHOUSE_LAPTOP_MONITOR_STRUCT
	CLUBHOUSE_LAPTOP_MONITOR_STATE eLaptopState
	PLAYER_INDEX pOwnerID
	INT iCrewID
	INT iRenderTargetID = -1
	TEXT_LABEL_63 tlCrewTxd
	NETWORK_CLAN_DESC memberInfo
ENDSTRUCT

/// PURPOSE: Clubhouse MC name states
ENUM CLUBHOUSE_MC_NAME_STATES
	CLUBHOUSE_MC_NAME_STATE_LINK_RT = 0,
	CLUBHOUSE_MC_NAME_STATE_UPDATE_SCALEFORM,
	CLUBHOUSE_MC_NAME_STATE_DRAW_NAME
ENDENUM

/// PURPOSE: Clubhouse MC name struct
STRUCT CLUBHOUSE_MC_NAME_STRUCT
	INT iMCNameHash = -1
	INT iMCTempHash = -1
	INT iRenderTargetID = -1
	INT iCurrentFont
	INT iCurrentFontColour
	INT iHideMCName
	BOOL bUpdateScaleform = TRUE
	STRING sName
	PLAYER_INDEX pOwnerID
	SCALEFORM_INDEX sNameScaleform
	CLUBHOUSE_MC_NAME_STATES eNameStates = CLUBHOUSE_MC_NAME_STATE_LINK_RT
	
	#IF IS_DEBUG_BUILD
	BOOL bBlankMCName
	#ENDIF
	
	// Debug
	FLOAT fCentreX
	FLOAT fCentreY
	FLOAT fWidth
	FLOAT fHeight
ENDSTRUCT

//STRUCT NO_WANTED_LEVEL_GAIN_ZONES_DATA
//	INT iActiveZonesBS
//	INT iZoneStagger
//	INT iMaintainThisWantedLevel = -1
//	#IF IS_DEBUG_BUILD
//	BOOL db_bDrawZones
//	BOOL db_bCheckedCMD
//	#ENDIF
//ENDSTRUCT

//╒══════════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ PROCEDURES & FUNCTIONS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════════════════╛

PROC CLEAN_UP_CLUBHOUSE_GUN_LOCKER(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization,BOOL bForceCleanUp = FALSE)
	PRINTLN("Vault weapon loadout - CLEAN_UP_CLUBHOUSE_GUN_LOCKER")
	
	RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
	sWLoadoutCustomization.iVaultWeaponContext = NEW_CONTEXT_INTENTION
	sWLoadoutCustomization.iWeaponMenuCurrentItem = 0
	sWLoadoutCustomization.iWeaponMenuTopItem = 0
	sWLoadoutCustomization.iNumAvailableWeaponGroup = 0
	sWLoadoutCustomization.eWeaponCurrentMenu = VM_MAIN_MENU
	sWLoadoutCustomization.bMenuInitialised = FALSE
	sWLoadoutCustomization.bReBuildMenu = FALSE
	
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_DOOR_ANIM_STAGE_FINISHED)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_ANIM_RUNNING)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS, BS_GET_NUM_AVAILABLE_WEAPON_GROUP)
	
	INT iNumSubMenu
	FOR iNumSubMenu = 0 TO WEAPON_VAULT_TOTAL - 1
		CLEAR_BIT(sWLoadoutCustomization.iVaultWeaponBS,iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iAvailableWeaponBS,iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iHideAllWeaponBS,iNumSubMenu)
	ENDFOR

	IF bForceCleanUp
		INT iNumWeaponModels
		FOR iNumWeaponModels = 0 TO MAX_NUMBER_WEAPON_MODELS -1
			IF DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[iNumWeaponModels])
				DELETE_OBJECT(sWLoadoutCustomization.weapons[iNumWeaponModels])
			ENDIF
			IF IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, iNumWeaponModels)
				CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS, iNumWeaponModels)
			ENDIF
		ENDFOR
		
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_ANIM_INITIALISED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_NET_RESERVED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_DOOR_ANIM_STAGE_FINISHED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,OFFICE_GUN_VAULT_DOOR_CREATED)

		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT	
	ELSE
		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT
	ENDIF
ENDPROC

PROC MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization,PLAYER_INDEX clubhouseOwner, INT iCurrentProperty)
	IF clubhouseOwner != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(clubhouseOwner)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_PISTOL)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_PISTOL_CREATED)  
			INT pistolModel = HASH("W_PI_PISTOL")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[0])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,pistolModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,pistolModel))
					IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
						sWLoadoutCustomization.weapons[0] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,pistolModel),<<1123.5811, -3162.0339, -36.5180>>,FALSE,FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[0], <<90.0000, 0.0000, 45.0000>>)
					ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
						sWLoadoutCustomization.weapons[0] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,pistolModel),<<1008.0450, -3172.3279, -38.5522>>,FALSE,FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[0], <<90.0000, -0.0000, -45.0000>>)
					ENDIF
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[0], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[0], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[0], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[0], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,pistolModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_PISTOL_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS: successfully created pistol.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - W_PI_PISTOL model exist")
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(clubhouseOwner)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_RIFLE)
			IF NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_RIFLE_CREATED)  
				INT rifleModel = HASH("W_AR_SPECIALCARBINE")
				IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[1])
					REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,rifleModel))
					IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,rifleModel))
						IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
							sWLoadoutCustomization.weapons[1] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,rifleModel), <<1123.5430, -3161.7910, -37.087>>, FALSE, FALSE)
							SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[1], <<0.4000, -66.4000, 0.0000>>)
						ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
							sWLoadoutCustomization.weapons[1] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,rifleModel), <<1008.3000, -3172.2881, -39.1287>>, FALSE, FALSE)
							SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[1], <<0.0000, -66.4000, -90.0000>>)
						ENDIF
						FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[1], TRUE)
						SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[1], TRUE)
						SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[1], TRUE)
						SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[1], TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,rifleModel))
						SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_RIFLE_CREATED)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS: successfully created rifle.")
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - W_AR_SPECIALCARBINE model is not loaded")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - W_AR_SPECIALCARBINE model exist")
				ENDIF
			ELIF NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, VAULT_ASSRIFLE_CREATED)
				INT rifleModel = HASH("W_AR_ASSAULTRIFLE")
				IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[8])
					REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,rifleModel))
					IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,rifleModel))
						IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
							sWLoadoutCustomization.weapons[8] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,rifleModel), <<1123.5730, -3161.3440, -37.0979>>, FALSE, FALSE)
							SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[8], <<-0.5000, -67.5000, 0.0000>>)
						ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
							sWLoadoutCustomization.weapons[8] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,rifleModel), <<1008.7520, -3172.2981, -39.1248>>, FALSE, FALSE)
							SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[8], <<0.0000, -66.0000, -90.0000>>)
						ENDIF
						FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[8], TRUE)
						SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[8], TRUE)
						SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[8], TRUE)
						SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[8], TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,rifleModel))
						SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_ASSRIFLE_CREATED)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS: successfully created assrifle.")
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - W_AR_ASSAULTRIFLE model is not loaded")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - W_AR_ASSAULTRIFLE model exist")
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(clubhouseOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SHOTGUN)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_SHOTGUN_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - start creating shotgun")
			INT shotgunModel = HASH("w_sg_pumpshotgun")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[2])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,shotgunModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,shotgunModel))
					IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
						sWLoadoutCustomization.weapons[2] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,shotgunModel), <<1123.5520, -3161.6860, -37.1155>>,FALSE,FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[2], <<0.0000, -66.7000, -0.0000>>)
					ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
						sWLoadoutCustomization.weapons[2] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,shotgunModel), <<1008.4160, -3172.2981, -39.1587>>,FALSE,FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[2], <<0.0000, -66.0000, -90.0000>>)
					ENDIF
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[2], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[2], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[2], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[2], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,shotgunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_SHOTGUN_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS: successfully created shotgun.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - w_sg_pumpshotgun model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - w_sg_pumpshotgun exist")
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(clubhouseOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MICROSMG)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_SMG_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - start creating smg")
			INT smgModel = HASH("w_sb_microsmg")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[3])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,smgModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,smgModel))
					IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
						sWLoadoutCustomization.weapons[3] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,smgModel), <<1123.6429, -3161.7161, -36.5125>>,FALSE,FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[3], <<90.0000, -0.0000, 45.0000>>)
					ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
						sWLoadoutCustomization.weapons[3] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,smgModel), <<1008.3600, -3172.4080, -38.5543>>,FALSE,FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[3], <<90.0000, -0.0000, -45.0000>>)
					ENDIF
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[3], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[3], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[3], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[3], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,smgModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_SMG_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS: successfully created smg.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - w_sb_microsmg model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(clubhouseOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_BOMB)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_BOMB_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - start creating bomb")
			INT shotgunModel = HASH("w_ex_pe")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[4])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,shotgunModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,shotgunModel))
					IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
						sWLoadoutCustomization.weapons[4] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,shotgunModel), <<1123.6630, -3161.4241, -36.5038>>, FALSE, FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[4],  <<0.0000, -0.0000, 45.000>>)
					ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
						sWLoadoutCustomization.weapons[4] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,shotgunModel), <<1008.6260, -3172.3679, -38.5304>>, FALSE, FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[4],  <<0.0000, -0.0000, 45.0000>>)
					ENDIF
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[4], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[4], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[4], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[4], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,shotgunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_BOMB_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS: successfully created smg.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - w_ex_pe model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(clubhouseOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_GRENADE)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_GRENADE_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - start creating grenade")
			INT grenadeModel = HASH("w_ex_grenadefrag")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[5])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,grenadeModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,grenadeModel))
					IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
						sWLoadoutCustomization.weapons[5] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,grenadeModel), <<1123.4630, -3161.4729, -36.4626>>, FALSE, FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[5], <<0.0000, 0.0000, 0.0000>>)
					ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
						sWLoadoutCustomization.weapons[5] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,grenadeModel), <<1008.7170, -3172.2590, -38.5008>>, FALSE, FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[5], <<0.0000, 0.0000, 0.0000>>)
					ENDIF
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[5], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[5], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[5], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[5], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,grenadeModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_GRENADE_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS: successfully created grenade.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - w_ex_grenadefrag model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(clubhouseOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MG)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_MG_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - start creating mg")
			INT gunModel = HASH("w_mg_combatmg")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[6])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,gunModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,gunModel))
					IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
						sWLoadoutCustomization.weapons[6] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,gunModel), <<1123.5229, -3161.5720, -37.110>>, FALSE, FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[6], <<0.0000, -66.7000, -0.0000>>)
					ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
						sWLoadoutCustomization.weapons[6] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,gunModel), <<1008.5197, -3172.2781, -39.1481>>, FALSE, FALSE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[6], <<0.0000, -66.0000, -90.0000>>)
					ENDIF
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[6], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[6], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[6], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[6], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,gunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_MG_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS: successfully created mg.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - w_mg_combatmg model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - w_mg_combatmg exist")
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(clubhouseOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SNIPER)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_SNIPER_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - start creating snipper")
			INT gunModel = HASH("w_sr_marksmanrifle")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[7])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,gunModel))
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,HASH("w_at_scope_large")))
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,HASH("w_sr_marksmanrifle_mag1")))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,gunModel))
				AND HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,HASH("w_at_scope_large")))
				AND HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,HASH("w_sr_marksmanrifle_mag1")))
					IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
						sWLoadoutCustomization.weapons[7] = CREATE_WEAPON_OBJECT(WEAPONTYPE_DLC_MARKSMANRIFLE,0,<<1123.5330, -3161.4519, -37.0909>>,TRUE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[7], <<0.0000, -66.7000, -0.0000>>)
						SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[7],<<1123.5330, -3161.4519, -37.0909>>)
					ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
						sWLoadoutCustomization.weapons[7] = CREATE_WEAPON_OBJECT(WEAPONTYPE_DLC_MARKSMANRIFLE,0,<<1008.6380, -3172.3279, -39.146>>,TRUE)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[7], <<0.0000, -66.0000, -90.0000>>)
						SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[7],<<1008.6380, -3172.3279, -39.146>>)
					ENDIF
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[7], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[7], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[7], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[7], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,gunModel))
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,HASH("w_at_scope_large")))
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,HASH("w_sr_marksmanrifle_mag1")))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_SNIPER_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS: successfully created snipper.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - w_sr_marksmanrifle model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS - w_sr_marksmanrifle exist")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the relevant stat   enum for the given playlist ID
FUNC MP_INT_STATS GET_FAV_PLAYLIST_STAT_FROM_ID(INT iplayList)
	SWITCH iplayList
		CASE 0 RETURN MP_STAT_JUKEBOXPLAYLISTTIME0
		CASE 1 RETURN MP_STAT_JUKEBOXPLAYLISTTIME1
		CASE 2 RETURN MP_STAT_JUKEBOXPLAYLISTTIME2
		CASE 3 RETURN MP_STAT_JUKEBOXPLAYLISTTIME3
		CASE 4 RETURN MP_STAT_JUKEBOXPLAYLISTTIME9
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "<JUKEBOX> GET_FAV_PLAYLIST_STAT_FROM_ID invalid selected playlist. Returning MP_STAT_JUKEBOXPLAYLISTTIME0")
	
	RETURN MP_STAT_JUKEBOXPLAYLISTTIME0
ENDFUNC

/// PURPOSE:
///    Returns the players most played playlist 
FUNC INT GET_LOCAL_PLAYERS_FAVOURITE_JUKEBOX_PLAYLIST_ID()
	INT i, iPlaylistID, iFavoriteTimeTotal
	REPEAT 5 i
		INT iNewTimeTotal = GET_MP_INT_CHARACTER_STAT(GET_FAV_PLAYLIST_STAT_FROM_ID(i))
		
		IF iNewTimeTotal > iFavoriteTimeTotal
			iFavoriteTimeTotal 	= iNewTimeTotal
			iPlaylistID			= i
		ENDIF
	ENDREPEAT
	
	RETURN iPlaylistID
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_CLUBHOUSE_MISSION_MENU_STAGE_NAME(CLUBHOUSE_MISSION_MENU_STATE eMenuState)
	SWITCH eMenuState
		CASE CHM_MENU_STAGE_INIT 				RETURN "CHM_MENU_STAGE_INIT" BREAK
		CASE CHM_MENU_STAGE_WAITING_TO_TRIGGER 	RETURN "CHM_MENU_STAGE_WAITING_TO_TRIGGER" BREAK
		CASE CHM_MENU_STAGE_ENTER_ANIM 			RETURN "CHM_MENU_STAGE_ENTER_ANIM" BREAK
		CASE CHM_MENU_STAGE_SELECT_MISSION 		RETURN "CHM_MENU_STAGE_SELECT_MISSION" BREAK
		CASE CHM_MENU_STAGE_EXIT_ANIM			RETURN "CHM_MENU_STAGE_EXIT_ANIM" BREAK
		CASE CHM_MENU_STAGE_CLEANUP 			RETURN "CHM_MENU_STAGE_CLEANUP" BREAK
	ENDSWITCH
	RETURN "******INVALID MENU STAGE*******"
ENDFUNC
#ENDIF

/// PURPOSE:
///    sets up controls for office menus
/// PARAMS:
///    sCHMenuInput - inputs
///    iMenuCurrentColumn - current menu item 
PROC SETUP_CLUBHOUSE_MENU_CONTROLS(CLUBHOUSE_MENU_INPUT& sCHMenuInput, INT& iMenuCurrentColumn, BOOL bMouseGrabMode = FALSE, FLOAT fMouseSensitivity = 0.2)

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(sCHMenuInput.iLeftX,sCHMenuInput.iLeftY,sCHMenuInput.iRightX,sCHMenuInput.iRightY )
	
	// Set the input flag states
	sCHMenuInput.bAccept	= (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	sCHMenuInput.bBack		= (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	sCHMenuInput.bLeft		= ((sCHMenuInput.iLeftX < -64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT))
	sCHMenuInput.bRight		= ((sCHMenuInput.iLeftX > 64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT))
	sCHMenuInput.bUp		= ((sCHMenuInput.iLeftY < -64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP))
	sCHMenuInput.bDown		= ((sCHMenuInput.iLeftY > 64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))
	
								 							 
	// Apply PC movements cumulatively (clamped to normal min/max of 127)
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		FLOAT fMouseX
		FLOAT fMouseY
		FLOAT fMouseDistX
		FLOAT fMouseDistY
		
		fMouseX = GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X) 
		fMouseY = GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
		fMouseDistX = fMouseX - sCHMenuInput.fPrevMouseX
		fMouseDistY = fMouseY - sCHMenuInput.fPrevMouseY
		
		sCHMenuInput.fPrevMouseX = fMouseX
		sCHMenuInput.fPrevMouseY = fMouseY

		// This is for cameras like the planning board which on PC use a grab camera using cursor distance moved rather than a look camera.
		IF bMouseGrabMode
			sCHMenuInput.iRightX = -ROUND((fMouseDistX * fMouseSensitivity) * 127.0)
			sCHMenuInput.iRightY = -ROUND((fMouseDistY * fMouseSensitivity) * 127.0)
		ELSE
			// Get look input direct from LR/UD inputs, so they take account of mouse sensitivity and all inversions
			sCHMenuInput.iRightX = ROUND((GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCALED_LOOK_LR) * fMouseSensitivity) * 127.0)
			sCHMenuInput.iRightY = ROUND((GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCALED_LOOK_UD) * fMouseSensitivity) * 127.0)
		ENDIF
		
		sCHMenuInput.iRightX = CLAMP_INT(sCHMenuInput.iRightX + sCHMenuInput.iPrevRightX[0],-127,127)
		sCHMenuInput.iRightY = CLAMP_INT(sCHMenuInput.iRightY + sCHMenuInput.iPrevRightY[0],-127,127)
	
	ENDIF
	
	INT i
	CONST_FLOAT fCONST_RIGHT_STICK_MULT	0.0025
	
	sCHMenuInput.vCamNudge.x  = TO_FLOAT(sCHMenuInput.iRightX)
	REPEAT COUNT_OF(sCHMenuInput.iPrevRightX) i
		sCHMenuInput.vCamNudge.x += TO_FLOAT(sCHMenuInput.iPrevRightX[i])
	ENDREPEAT
	sCHMenuInput.vCamNudge.x /= COUNT_OF(sCHMenuInput.iPrevRightX)+1
	sCHMenuInput.vCamNudge.x *= fCONST_RIGHT_STICK_MULT
	
	sCHMenuInput.vCamNudge.y = 0.0
	
	sCHMenuInput.vCamNudge.z  = TO_FLOAT(sCHMenuInput.iRightX)
	REPEAT COUNT_OF(sCHMenuInput.iPrevRightY) i
		sCHMenuInput.vCamNudge.z += TO_FLOAT(sCHMenuInput.iPrevRightY[i])
	ENDREPEAT
	sCHMenuInput.vCamNudge.z /= COUNT_OF(sCHMenuInput.iPrevRightY)+1
	sCHMenuInput.vCamNudge.z *= fCONST_RIGHT_STICK_MULT
	
	// Invert gamepad stick if look is inverted in settings.
	IF NOT IS_LOOK_INVERTED()
		sCHMenuInput.vCamNudge.z *= -1.0
	ENDIF
	
			
	IF LOAD_MENU_ASSETS()
	AND GET_PAUSE_MENU_STATE() = PM_INACTIVE
	AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
	AND NOT g_sShopSettings.bProcessStoreAlert
	
		// Mouse control support  
		IF IS_PC_VERSION()	
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
			IF IS_USING_CURSOR(FRONTEND_CONTROL)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	
			//	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
			//	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		//		IF ((g_iMenuCursorItem = MENU_CURSOR_NO_ITEM) OR (g_iMenuCursorItem = MENU_CURSOR_NO_CAMERA_MOVE) OR (g_iMenuCursorItem = MENU_CURSOR_DRAG_CAM))
					ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
					ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
		//		ELSE
		//			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
		//			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
		//		ENDIF
		//		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
		//		HANDLE_MENU_CURSOR(FALSE)
			ENDIF
			
		//	// Mouse select
		//	IF IS_MENU_CURSOR_ACCEPT_PRESSED()
		//		IF g_iMenuCursorItem = iMenuCurrentColumn
		//			sCHMenuInput.bAccept = TRUE
		//		ELSE
		//			iMenuCurrentColumn = g_iMenuCursorItem
		//			SET_CURRENT_MENU_ITEM(iMenuCurrentColumn)		
		//		ENDIF
		//	ENDIF
		//
		//	// Menu cursor back
		//	IF IS_MENU_CURSOR_CANCEL_PRESSED()
		//		sCHMenuInput.bBack = TRUE
		//	ENDIF
		//	
		//	// Mouse scroll up
		//	IF IS_MENU_CURSOR_SCROLL_UP_PRESSED()
		//		sCHMenuInput.bUp = TRUE
		//	ENDIF
		//	
		//	// Mouse scroll down
		//	IF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
		//		sCHMenuInput.bDown = TRUE
		//	ENDIF
			UNUSED_PARAMETER(iMenuCurrentColumn)
			
		ENDIF
	ENDIF

//	sCHMenuInput.iPrevRightX[4] = sCHMenuInput.iPrevRightX[3]
//	sCHMenuInput.iPrevRightY[4] = sCHMenuInput.iPrevRightY[3]
//
//	sCHMenuInput.iPrevRightX[3] = sCHMenuInput.iPrevRightX[2]
//	sCHMenuInput.iPrevRightY[3] = sCHMenuInput.iPrevRightY[2]
//
//	sCHMenuInput.iPrevRightX[2] = sCHMenuInput.iPrevRightX[1]
//	sCHMenuInput.iPrevRightY[2] = sCHMenuInput.iPrevRightY[1]
//
//	sCHMenuInput.iPrevRightX[1] = sCHMenuInput.iPrevRightX[0]
//	sCHMenuInput.iPrevRightY[1] = sCHMenuInput.iPrevRightY[0]

	
	FOR i = COUNT_OF(sCHMenuInput.iPrevRightX)-1 TO 1 STEP -1
		sCHMenuInput.iPrevRightX[i] = sCHMenuInput.iPrevRightX[i-1]
		sCHMenuInput.iPrevRightY[i] = sCHMenuInput.iPrevRightY[i-1]
	ENDFOR
	
	//Check if moved from old focus position 
	IF sCHMenuInput.iPrevRightX[0] = sCHMenuInput.iRightX
	AND sCHMenuInput.iPrevRightY[0] = sCHMenuInput.iRightY
		//Do nothing if not moved but not time to reset yet
		IF sCHMenuInput.iFocusResetTime < GET_GAME_TIMER()
//			sCHMenuInput.iPrevRightX[4] = sCHMenuInput.iPrevRightX[3]
//			sCHMenuInput.iPrevRightY[4] = sCHMenuInput.iPrevRightY[3]
//			
//			sCHMenuInput.iPrevRightX[3] = sCHMenuInput.iPrevRightX[2]
//			sCHMenuInput.iPrevRightY[3] = sCHMenuInput.iPrevRightY[2]
//			
//			sCHMenuInput.iPrevRightX[2] = sCHMenuInput.iPrevRightX[1]
//			sCHMenuInput.iPrevRightY[2] = sCHMenuInput.iPrevRightY[1]
//			
//			sCHMenuInput.iPrevRightX[1] = sCHMenuInput.iPrevRightX[0]
//			sCHMenuInput.iPrevRightY[1] = sCHMenuInput.iPrevRightY[0]
			
			

			
			sCHMenuInput.iPrevRightX[0] = 0
			sCHMenuInput.iPrevRightY[0] = 0
			//Reset mouse position if on Kb&M
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				sCHMenuInput.iRightX = 0
				sCHMenuInput.iRightY = 0
			//	sCHMenuInput.bResetZoom = TRUE
			ENDIF
		ENDIF
	ELSE	
	//Register new positions
//		sCHMenuInput.iPrevRightX[4] = sCHMenuInput.iPrevRightX[3]
//		sCHMenuInput.iPrevRightY[4] = sCHMenuInput.iPrevRightY[3]
//		
//		sCHMenuInput.iPrevRightX[3] = sCHMenuInput.iPrevRightX[2]
//		sCHMenuInput.iPrevRightY[3] = sCHMenuInput.iPrevRightY[2]
//		
//		sCHMenuInput.iPrevRightX[2] = sCHMenuInput.iPrevRightX[1]
//		sCHMenuInput.iPrevRightY[2] = sCHMenuInput.iPrevRightY[1]
//		
//		sCHMenuInput.iPrevRightX[1] = sCHMenuInput.iPrevRightX[0]
//		sCHMenuInput.iPrevRightY[1] = sCHMenuInput.iPrevRightY[0]
		
		sCHMenuInput.iPrevRightX[0] = sCHMenuInput.iRightX
		sCHMenuInput.iPrevRightY[0] = sCHMenuInput.iRightY
		sCHMenuInput.iFocusResetTime = GET_GAME_TIMER() + FPC_FOCUS_RESET
	//	sCHMenuInput.bresetZoom = FALSE
	ENDIF
	
ENDPROC

/// PURPOSE:
///     Set clubhouse mission menu state to: 
PROC SET_CLUBHOUSE_MISSION_MENU_STATE(CLUBHOUSE_MISSION_MENU_STRUCT &sClubHouseMissionMenu,CLUBHOUSE_MISSION_MENU_STATE eMenuState)
	#IF IS_DEBUG_BUILD
		PRINTLN("[CLUBHOUSE] SET_CLUBHOUSE_MISSION_MENU_STATE - Current stage:",sClubHouseMissionMenu.eMenuState ," - New stage: ", GET_CLUBHOUSE_MISSION_MENU_STAGE_NAME(eMenuState))
	#ENDIF
	sClubHouseMissionMenu.eMenuState = eMenuState
ENDPROC

/// PURPOSE:
///    Get clubhouse mission menu state
/// RETURNS: CLUBHOUSE_MISSION_MENU_STATE  
FUNC CLUBHOUSE_MISSION_MENU_STATE GET_CH_MISSION_MENU_STATE(CLUBHOUSE_MISSION_MENU_STRUCT &sClubHouseMissionMenu)
	RETURN sClubHouseMissionMenu.eMenuState
ENDFUNC

FUNC BOOL IS_SAFE_TO_START_CLUBHOUSE_MISSION_MENU()
	
	IF IS_PHONE_ONSCREEN()
	OR Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
	OR MPGlobals.PlayerInteractionData.iTargetPlayerInt != -1 // interaction menu open
	OR IS_BROWSER_OPEN() //browser open
	OR IS_SELECTOR_ONSCREEN()
	OR IS_CUSTOM_MENU_ON_SCREEN()
	OR IS_CUTSCENE_ACTIVE()
	OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CONTRABAND_SELL
	OR (IS_PLAYER_USING_OFFICE_SEATID_VALID() OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_SIT"))
	OR IS_ANY_GB_BIG_MESSAGE_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	IF !GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
/*	//Don't request a mission if already running a gang only mission
	IF GB_IS_PLAYER_ON_GANG_BOSS_MISSION_OF_CATEGORY(PLAYER_ID(),GB_BOSS_MISSION_CATEGORY_GANG_ONLY)
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		RETURN FALSE
//	ELIF GB_RETURN_ARRAY_POS_FROM_TYPE(iType) != -1
//	AND !GB_HAS_BOSSWORK_COOLDOWN_TIMER_EXPIRED(iType)
//		RETURN FALSE
//	ELIF !GB_IS_BOSS_MISSION_AVAILABLE(PLAYER_ID(), iType)
//		RETURN FALSE
	ELIF !GB_IS_LOCAL_PLAYER_SAFE_TO_LAUNCH_GANG_BOSS_MISSION()
		RETURN FALSE
	ELIF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID()) != -1
		RETURN FALSE
	ELIF NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ELIF GB_IS_ANY_SESSION_GANG_BOSS_MISSION_LAUNCHING() OR GB_IS_ANY_SESSION_GANG_BOSS_MISSION_ACTIVE()
		RETURN FALSE
	ENDIF
	*/
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Get clubhouse mission menu item name 
/// PARAMS:
///    iCurrentItem - current menu item
FUNC STRING GET_CHM_ITEM_NAME(CLUBHOUSE_MISSION_ENUM eCHMission, BOOL bUpperCase = FALSE)
	
	SWITCH eCHMission
		CASE CLUBHOUSE_MISSION_DEAL_GONE_WRONG		IF bUpperCase RETURN "BDEAL_DEALN"		ELSE RETURN "DEAL_DEALN"		ENDIF BREAK
		CASE CLUBHOUSE_MISSION_RESCUE_CONTACT		IF bUpperCase RETURN "BIGM_RESCN"		ELSE RETURN "CELL_BIKER_RESC"	ENDIF BREAK
		CASE CLUBHOUSE_MISSION_LAST_RESPECTS		IF bUpperCase RETURN "LR_INTRO_ST"		ELSE RETURN "LR_INTRO_ST"		ENDIF BREAK
		CASE CLUBHOUSE_MISSION_CONTRACT_KILLING		IF bUpperCase RETURN "CELL_BIKER_CK"	ELSE RETURN "CELL_BIKER_CK"		ENDIF BREAK
		CASE CLUBHOUSE_MISSION_UNLOAD_WEAPONS		IF bUpperCase RETURN "GB_BIGUNLOAD_U"	ELSE RETURN "GB_BIGUNLOAD_T"	ENDIF BREAK
		CASE CLUBHOUSE_MISSION_STEAL_BIKES			IF bUpperCase RETURN "SB_INTRO_TITLE"	ELSE RETURN "SB_MENU_TITLE"		ENDIF BREAK
		CASE CLUBHOUSE_MISSION_FREE_PRISONER		IF bUpperCase RETURN "FP_INTRO_TITLE"	ELSE RETURN "FP_MENU_TITLE"		ENDIF BREAK
		CASE CLUBHOUSE_MISSION_SAFECRACKER			IF bUpperCase RETURN "SC_INTRO_TITLE"	ELSE RETURN "SC_MENU_TITLE"		ENDIF BREAK
		CASE CLUBHOUSE_MISSION_DESTROY_VANS			IF bUpperCase RETURN "DV_SH_BIG"		ELSE RETURN "DV_SH_TITLE"		ENDIF BREAK
		CASE CLUBHOUSE_MISSION_BURN_ASSETS			IF bUpperCase RETURN "BA_SH_BIG"		ELSE RETURN "BA_SH_TITLE"		ENDIF BREAK
		CASE CLUBHOUSE_MISSION_SHUTTLE				IF bUpperCase RETURN "SHU_SH_BIG"		ELSE RETURN "SHU_SH_TITLE"		ENDIF BREAK
		#IF FEATURE_DLC_1_2022
		CASE CLUBHOUSE_MISSION_ROOFTOP_CONTAINER	IF bUpperCase RETURN "RFC_SH_BIG"		ELSE RETURN "RFC_SH_TITLE"		ENDIF BREAK
		CASE CLUBHOUSE_MISSION_DEATHBIKE_DELIVERY	IF bUpperCase RETURN "DTH_SH_BIG"		ELSE RETURN "DTH_SH_TITLE"		ENDIF BREAK
		#ENDIF
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	ASSERTLN("[CLUBHOUSE] GET_CHM_ITEM_NAME - invalid item ", eCHMission, ", ", bUpperCase)
	#ENDIF
	RETURN "GET_CHM_ITEM_NAME - Invalid item"
ENDFUNC

/// PURPOSE:
///    Get clubhouse mission menu item description 
/// PARAMS:
///    iCurrentItem - current menu item
FUNC STRING GET_CHM_ITEM_DESCRIPTION(CLUBHOUSE_MISSION_ENUM eCHMission)
	
	SWITCH eCHMission
		CASE CLUBHOUSE_MISSION_DEAL_GONE_WRONG		RETURN "DEAL_DEALND" BREAK
		CASE CLUBHOUSE_MISSION_RESCUE_CONTACT		RETURN "CELL_BIKER_RESD" BREAK
		CASE CLUBHOUSE_MISSION_LAST_RESPECTS		RETURN "GFH_MENU_DESC" BREAK
		CASE CLUBHOUSE_MISSION_CONTRACT_KILLING		RETURN "CELL_BIKER_CKD" BREAK
		CASE CLUBHOUSE_MISSION_UNLOAD_WEAPONS		RETURN "GB_BIGUNLOAD_D" BREAK
		CASE CLUBHOUSE_MISSION_STEAL_BIKES			RETURN "SB_MENU_DESC" BREAK
		CASE CLUBHOUSE_MISSION_FREE_PRISONER		RETURN "FP_MENU_DESC" BREAK
		CASE CLUBHOUSE_MISSION_SAFECRACKER			RETURN "SC_MENU_DESC" BREAK
		CASE CLUBHOUSE_MISSION_DESTROY_VANS			RETURN "DV_MENU_DESC" BREAK
		CASE CLUBHOUSE_MISSION_BURN_ASSETS			RETURN "BA_MENU_DESC" BREAK
		CASE CLUBHOUSE_MISSION_SHUTTLE				RETURN "SHU_MENU_DESC" BREAK
		#IF FEATURE_DLC_1_2022
		CASE CLUBHOUSE_MISSION_ROOFTOP_CONTAINER	RETURN "RFC_MENU_DESC" BREAK
		CASE CLUBHOUSE_MISSION_DEATHBIKE_DELIVERY	RETURN "DTH_MENU_DESC" BREAK
		#ENDIF
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	ASSERTLN("[CLUBHOUSE] GET_CHM_ITEM_DESCRIPTION - invalid item ", eCHMission)
	#ENDIF
	RETURN "GET_CHM_ITEM_DESCRIPTION - Invalid item"
ENDFUNC

/// PURPOSE:
///    Get clubhouse mission menu item texture 
/// PARAMS:
///    iCurrentItem - current menu item
FUNC STRING GET_CHM_ITEM_TEXTURE(CLUBHOUSE_MISSION_ENUM eCHMission)
	
	SWITCH eCHMission
		CASE CLUBHOUSE_MISSION_DEAL_GONE_WRONG		RETURN "CHM_IMG0" BREAK
		CASE CLUBHOUSE_MISSION_RESCUE_CONTACT		RETURN "CHM_IMG8" BREAK
		CASE CLUBHOUSE_MISSION_LAST_RESPECTS		RETURN "CHM_IMG4" BREAK
		CASE CLUBHOUSE_MISSION_CONTRACT_KILLING		RETURN "CHM_IMG10" BREAK
		CASE CLUBHOUSE_MISSION_UNLOAD_WEAPONS		RETURN "CHM_IMG3" BREAK
		CASE CLUBHOUSE_MISSION_STEAL_BIKES			RETURN "CHM_IMG6" BREAK
		CASE CLUBHOUSE_MISSION_FREE_PRISONER		RETURN "CHM_IMG5" BREAK
		CASE CLUBHOUSE_MISSION_SAFECRACKER			RETURN "CHM_IMG1" BREAK
		CASE CLUBHOUSE_MISSION_DESTROY_VANS			RETURN "CHM_IMG2" BREAK
		CASE CLUBHOUSE_MISSION_BURN_ASSETS			RETURN "CHM_IMG9" BREAK
		CASE CLUBHOUSE_MISSION_SHUTTLE				RETURN "CHM_IMG7" BREAK
		#IF FEATURE_DLC_1_2022
		CASE CLUBHOUSE_MISSION_ROOFTOP_CONTAINER	RETURN "CHM_IMG12" BREAK
		CASE CLUBHOUSE_MISSION_DEATHBIKE_DELIVERY	RETURN "CHM_IMG11" BREAK
		#ENDIF
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	ASSERTLN("[CLUBHOUSE] GET_CHM_ITEM_TEXTURE - invalid item ", eCHMission)
	#ENDIF
	RETURN "GET_CHM_ITEM_TEXTURE - Invalid item"
ENDFUNC

/// PURPOSE:
///    Get clubhouse mission menu item location 
/// PARAMS:
///    iCurrentItem - current menu item
FUNC VECTOR GET_CHM_ITEM_LOCATION(CLUBHOUSE_MISSION_ENUM eCHMission)
	SWITCH eCHMission
		CASE CLUBHOUSE_MISSION_DEAL_GONE_WRONG		RETURN <<-423.0253, -1681.8883, 18.0291>> BREAK
		CASE CLUBHOUSE_MISSION_RESCUE_CONTACT		RETURN <<-1063.7284, -2692.1099, -8.4101>> BREAK
		CASE CLUBHOUSE_MISSION_LAST_RESPECTS		RETURN <<-1699.1350, -289.1163, 50.8123>> BREAK
		CASE CLUBHOUSE_MISSION_CONTRACT_KILLING		RETURN <<1991.0994, 3054.7307, 46.2150>> BREAK
		CASE CLUBHOUSE_MISSION_UNLOAD_WEAPONS		RETURN <<-564.1616, 302.2307, 82.1587>> BREAK
		CASE CLUBHOUSE_MISSION_STEAL_BIKES			RETURN <<-66.0629, -535.4803, 30.9105>> BREAK
		CASE CLUBHOUSE_MISSION_FREE_PRISONER		RETURN <<-2084.3989, -316.9909, 12.0233>> BREAK
		CASE CLUBHOUSE_MISSION_SAFECRACKER			RETURN <<981.9487, -103.0691, 73.8538>> BREAK
		CASE CLUBHOUSE_MISSION_DESTROY_VANS			RETURN <<1823.2039, 3939.8918, 32.3719>> BREAK
		CASE CLUBHOUSE_MISSION_BURN_ASSETS			RETURN <<508.5963, -597.9878, 23.7606>> BREAK
		CASE CLUBHOUSE_MISSION_SHUTTLE				RETURN <<327.4520, 2622.7529, 43.4970>> BREAK
		
		#IF FEATURE_DLC_1_2022
		CASE CLUBHOUSE_MISSION_ROOFTOP_CONTAINER	RETURN <<1715.3636, -1435.5209, 111.8353>> BREAK
		CASE CLUBHOUSE_MISSION_DEATHBIKE_DELIVERY	RETURN <<-800.3588,   331.4872, 219.4385>> BREAK
		#ENDIF
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	ASSERTLN("[CLUBHOUSE] GET_CHM_ITEM_LOCATION - invalid item ", eCHMission)
	#ENDIF
	RETURN <<0,0,0>>
ENDFUNC


FUNC BOOL IS_CH_MISSION_BLOCKED_BY_TUNEABLES(CLUBHOUSE_MISSION_ENUM eCHMission, INT iVariation = -1)
	SWITCH eCHMission
		CASE CLUBHOUSE_MISSION_DEAL_GONE_WRONG	//By the Pound
			IF g_sMPTunables.bBIKER_DISABLE_BY_THE_POUND
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.bBIKER_DISABLE_BY_THE_POUND_0 AND iVariation = 0
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_BY_THE_POUND_1 AND iVariation = 1
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_BY_THE_POUND_2 AND iVariation = 2
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_RESCUE_CONTACT	//P.O.W
			IF g_sMPTunables.bBIKER_DISABLE_POW
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.bBIKER_DISABLE_POW_0 AND iVariation = 0
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_POW_1 AND iVariation = 1
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_POW_2 AND iVariation = 2
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_LAST_RESPECTS	//Guns for Hire
			IF g_sMPTunables.bBIKER_DISABLE_GUNS_FOR_HIRE
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.bBIKER_DISABLE_GUNS_FOR_HIRE_0 AND iVariation = 0
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_GUNS_FOR_HIRE_1 AND iVariation = 1
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_GUNS_FOR_HIRE_2 AND iVariation = 2
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_CONTRACT_KILLING	//Weapon of Choice
			IF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE_YELLOW_JACK AND iVariation = 0
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE_BUS AND iVariation = 1
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE_ULSA AND iVariation = 2
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE_GOLF AND iVariation = 3
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE_CHICKEN AND iVariation = 4
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE_LSIA AND iVariation = 5
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE_DOWNTOWN AND iVariation = 6
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE_VINEWOOD AND iVariation = 7
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE_WILDERNESS AND iVariation = 8
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE_MOVIE AND iVariation = 9
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_WEAPON_OF_CHOICE_ROAMING AND iVariation = 10
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_UNLOAD_WEAPONS	//Gunrunning
			IF g_sMPTunables.bBIKER_DISABLE_GUNRUNNING
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.bBIKER_DISABLE_GUNRUNNING_0 AND iVariation = 0
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_GUNRUNNING_1 AND iVariation = 1
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_GUNRUNNING_2 AND iVariation = 2
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_STEAL_BIKES		//Nine Tenths of the Law
			IF g_sMPTunables.bBIKER_DISABLE_NINE_TENTHS
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.bBIKER_DISABLE_NINE_TENTHS_0 AND iVariation = 0
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_NINE_TENTHS_1 AND iVariation = 1
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_NINE_TENTHS_2 AND iVariation = 2
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_FREE_PRISONER	//Jailbreak
			IF g_sMPTunables.bBIKER_DISABLE_JAILBREAK
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.bBIKER_DISABLE_JAILBREAK_0 AND iVariation = 0
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_JAILBREAK_1 AND iVariation = 1
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_JAILBREAK_2 AND iVariation = 2
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_SAFECRACKER		//Cracked
			IF g_sMPTunables.bBIKER_DISABLE_CRACKED
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.bBIKER_DISABLE_CRACKED_0 AND iVariation = 0
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_CRACKED_1 AND iVariation = 1
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_CRACKED_2 AND iVariation = 2
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_DESTROY_VANS		//Fragile Goods
			IF g_sMPTunables.bBIKER_DISABLE_FRAGILE_GOODS
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.bBIKER_DISABLE_FRAGILE_GOODS_NE AND iVariation = 0
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_FRAGILE_GOODS_NW AND iVariation = 1
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_FRAGILE_GOODS_SE AND iVariation = 2
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_FRAGILE_GOODS_SW AND iVariation = 3
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_BURN_ASSETS		//Torched
			IF g_sMPTunables.bBIKER_DISABLE_TORCHED
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.bBIKER_DISABLE_TORCHED_CITY AND iVariation = 0
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_TORCHED_COUNTRY AND iVariation = 1
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_SHUTTLE			//Outrider
			IF g_sMPTunables.bBIKER_DISABLE_OUTRIDER
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.bBIKER_DISABLE_OUTRIDER_CITY AND iVariation = 0
				RETURN TRUE
			ELIF g_sMPTunables.bBIKER_DISABLE_OUTRIDER_COUNTRY AND iVariation = 1
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_ROOFTOP_CONTAINER		
			IF g_sMPTunables.bBIKER_DISABLE_ROOFTOP_CONTAINER
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLUBHOUSE_MISSION_DEATHBIKE_DELIVERY	
			IF g_sMPTunables.bBIKER_DISABLE_DEATHBIKE_DELIVERY
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_GLOBAL_CHM_MISSION_LIST()
	INT i
	
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		PRINTLN(GET_THIS_SCRIPT_NAME(), " [CLUBHOUSE ", GET_PLAYER_NAME(PLAYER_ID()), "] UPDATE_GLOBAL_CHM_MISSION_LIST bail, player is invalid")
		
		RETURN FALSE
	ELIF NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT()) != PLAYER_ID()
		IF NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT()) != INVALID_PLAYER_INDEX()
			PRINTLN(GET_THIS_SCRIPT_NAME(), " [CLUBHOUSE ", GET_PLAYER_NAME(PLAYER_ID()), "] UPDATE_GLOBAL_CHM_MISSION_LIST bail, host is \"", GET_PLAYER_NAME(NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT())), "\"")
		ELSE
			PRINTLN(GET_THIS_SCRIPT_NAME(), " [CLUBHOUSE ", GET_PLAYER_NAME(PLAYER_ID()), "] UPDATE_GLOBAL_CHM_MISSION_LIST bail, host is invalid")
		ENDIF
		
		RETURN FALSE
	ELIF GlobalServerBD_FM.ClubhouseMissionData.timestamp = NULL
		GlobalServerBD_FM.ClubhouseMissionData.timestamp = GET_NETWORK_TIME()
		GlobalServerBD_FM.ClubhouseMissionData.iCHMissionInvalidBS = 0
		
		BROADCAST_CLUBHOUSE_MISSION_WALL_REFRESHED() //notifies players that the mission board was refreshed
		
		
		REPEAT iCONST_NUM_CLUBHOUSE_MISSIONS i
			GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[i] = ENUM_TO_INT(CLUBHOUSE_MISSION_invalid)
			GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[i] = ENUM_TO_INT(CLUBHOUSE_MISSION_invalid)
		ENDREPEAT
		
		PRINTLN(GET_THIS_SCRIPT_NAME(), " [CLUBHOUSE ", GET_PLAYER_NAME(PLAYER_ID()), "] UPDATE_GLOBAL_CHM_MISSION_LIST reset timestamp:", GET_TIME_AS_STRING(GlobalServerBD_FM.ClubhouseMissionData.timestamp))
	ELSE
		TIME_DATATYPE current_timestamp = GET_NETWORK_TIME()
		INT iTimeDifference = GET_TIME_DIFFERENCE(current_timestamp, GlobalServerBD_FM.ClubhouseMissionData.timestamp)
		
		BOOL bAllCHMissionsInvalid = TRUE
		REPEAT iCONST_NUM_CLUBHOUSE_MISSIONS i
			IF NOT IS_BIT_SET(GlobalServerBD_FM.ClubhouseMissionData.iCHMissionInvalidBS, i)
				bAllCHMissionsInvalid = FALSE
			ENDIF
		ENDREPEAT
		
		IF ABSI(iTimeDifference) < g_sMPTunables.iBIKER_CLUBHOUSE_WALL_REFRESH*1000
		AND NOT bAllCHMissionsInvalid
			STRING sGlobal_timestamp = GET_TIME_AS_STRING(GlobalServerBD_FM.ClubhouseMissionData.timestamp)
			STRING sCurrent_timestamp = GET_TIME_AS_STRING(current_timestamp)
			
			PRINTLN(GET_THIS_SCRIPT_NAME(), " [CLUBHOUSE ", GET_PLAYER_NAME(PLAYER_ID()), "] UPDATE_GLOBAL_CHM_MISSION_LIST timestamp:",
					sGlobal_timestamp, " - current_timestamp:", sCurrent_timestamp,
					" = iTimeDifference:", iTimeDifference, " < ", g_sMPTunables.iBIKER_CLUBHOUSE_WALL_REFRESH*1000,
					PICK_STRING(bAllCHMissionsInvalid, " bAllCHMissionsInvalid", " NOT bAllCHMissionsInvalid"))
			RETURN FALSE
		ENDIF
		
		CDEBUG3LN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " [CLUBHOUSE ", GET_PLAYER_NAME(PLAYER_ID()), "] UPDATE_GLOBAL_CHM_MISSION_LIST timestamp:", GET_TIME_AS_STRING(GlobalServerBD_FM.ClubhouseMissionData.timestamp), " - current_timestamp:", GET_TIME_AS_STRING(current_timestamp), " = iTimeDifference:", iTimeDifference, " < ", g_sMPTunables.iBIKER_CLUBHOUSE_WALL_REFRESH*1000,  PICK_STRING(bAllCHMissionsInvalid, " bAllCHMissionsInvalid", " NOT bAllCHMissionsInvalid"))
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	BOOL bSummer2022ContractAdded = FALSE
	#ENDIF
	
	
	REPEAT iCONST_NUM_CLUBHOUSE_MISSIONS i
		CLUBHOUSE_MISSION_ENUM eRandCHMission = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CLUBHOUSE_MISSION_MAX_MENU_ITEM)))
		
		BOOL bFound = FALSE
		WHILE NOT bFound
			bFound = TRUE
			
			#IF FEATURE_DLC_1_2022
			IF bFound	
			AND NOT bSummer2022ContractAdded
			
				IF eRandCHMission = CLUBHOUSE_MISSION_ROOFTOP_CONTAINER
				OR eRandCHMission = CLUBHOUSE_MISSION_DEATHBIKE_DELIVERY
					bSummer2022ContractAdded = TRUE
				ENDIF
				
				IF NOT bSummer2022ContractAdded
				AND GET_RANDOM_INT_IN_RANGE(0, 100) <= (i+1)*33.3 //33% chance this gets checked for slot 0, 66% chance this is checked for slot 1 and 100% for slot 2 to ensure one of these missions is always in
					eRandCHMission = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(CLUBHOUSE_MISSION_ROOFTOP_CONTAINER), ENUM_TO_INT(CLUBHOUSE_MISSION_MAX_MENU_ITEM)))
					bSummer2022ContractAdded = TRUE
				ENDIF
			ENDIF
			#ENDIF
			
//			//first instance, no j			
//			IF eRandCHMission = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[j])
//				PRINTLN("[CLUBHOUSE] UPDATE_GLOBAL_CHM_MISSION_LIST ", GET_TIME_AS_STRING(GlobalServerBD_FM.ClubhouseMissionData.timestamp), " -     slot ", i, " [", eRandCHMission, "] equal to slot ", j, " [", INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[j]), "]")
//				bFound = FALSE
//			ENDIF
			IF bFound
				INT k
				REPEAT iCONST_NUM_CLUBHOUSE_MISSIONS k
					IF eRandCHMission = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[k])
						PRINTLN("[CLUBHOUSE] UPDATE_GLOBAL_CHM_MISSION_LIST ", GET_TIME_AS_STRING(GlobalServerBD_FM.ClubhouseMissionData.timestamp), " -         slot ", i, " [", eRandCHMission, "] equal to old slot ", k, " [", INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[k]), "]")
						bFound = FALSE
					ENDIF				
				ENDREPEAT
			ENDIF
			IF bFound
				IF IS_CH_MISSION_BLOCKED_BY_TUNEABLES(eRandCHMission)
					PRINTLN("[CLUBHOUSE] UPDATE_GLOBAL_CHM_MISSION_LIST ", GET_TIME_AS_STRING(GlobalServerBD_FM.ClubhouseMissionData.timestamp), " -         slot ", i, " [", eRandCHMission, "] disabled by tunable")
					bFound = FALSE
				ENDIF
			ENDIF
					
			IF NOT bFound
				eRandCHMission = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CLUBHOUSE_MISSION_MAX_MENU_ITEM)))
			ENDIF
		ENDWHILE
		
		INT j
		FOR j = 0 TO (i-1)
			bFound = FALSE
			WHILE NOT bFound
				bFound = TRUE
				
				IF eRandCHMission = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[j])
					PRINTLN("[CLUBHOUSE] UPDATE_GLOBAL_CHM_MISSION_LIST ", GET_TIME_AS_STRING(GlobalServerBD_FM.ClubhouseMissionData.timestamp), " -     slot ", i, " [", eRandCHMission, "] equal to slot ", j, " [", INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[j]), "]")
					bFound = FALSE
				ENDIF
				IF bFound
					INT k
					REPEAT iCONST_NUM_CLUBHOUSE_MISSIONS k
						IF eRandCHMission = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[k])
							PRINTLN("[CLUBHOUSE] UPDATE_GLOBAL_CHM_MISSION_LIST ", GET_TIME_AS_STRING(GlobalServerBD_FM.ClubhouseMissionData.timestamp), " -         slot ", i, " [", eRandCHMission, "] equal to old slot ", k, " [", INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[k]), "]")
							bFound = FALSE
						ENDIF
					ENDREPEAT
				ENDIF
				IF bFound
					IF IS_CH_MISSION_BLOCKED_BY_TUNEABLES(eRandCHMission)
						PRINTLN("[CLUBHOUSE] UPDATE_GLOBAL_CHM_MISSION_LIST ", GET_TIME_AS_STRING(GlobalServerBD_FM.ClubhouseMissionData.timestamp), " -         slot ", i, " [", eRandCHMission, "] disabled by tunable")
						bFound = FALSE
					ENDIF
				ENDIF
				
				IF NOT bFound
					eRandCHMission = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CLUBHOUSE_MISSION_MAX_MENU_ITEM)))
				ENDIF
			ENDWHILE
		ENDFOR
		GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[i] = ENUM_TO_INT(eRandCHMission)
		PRINTLN("[CLUBHOUSE] UPDATE_GLOBAL_CHM_MISSION_LIST ", GET_TIME_AS_STRING(GlobalServerBD_FM.ClubhouseMissionData.timestamp), " - slot ",i ," set to [", INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[i]), "] ", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[i]))))
		
		PRINTLN(GET_THIS_SCRIPT_NAME(), " [CLUBHOUSE ", GET_PLAYER_NAME(PLAYER_ID()), "] UPDATE_GLOBAL_CHM_MISSION_LIST ", GET_TIME_AS_STRING(GlobalServerBD_FM.ClubhouseMissionData.timestamp), " - slot ", i, " set to [", GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[i], "] ", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[i]))))
		
		
	ENDREPEAT
	GlobalServerBD_FM.ClubhouseMissionData.timestamp = GET_NETWORK_TIME() 
	GlobalServerBD_FM.ClubhouseMissionData.iCHMissionInvalidBS = 0
	BROADCAST_CLUBHOUSE_MISSION_WALL_REFRESHED() //notifies players that the mission board was refreshed
	
	RETURN TRUE
ENDFUNC

PROC INITIALISE_CHM_MISSION_LIST(CLUBHOUSE_MISSION_MENU_STRUCT &sClubHouseMissionMenu)
	
	IF NOT DOES_CAM_EXIST(sClubHouseMissionMenu.ciCam)
		g_bClubhouseMissionWallActive = FALSE
		GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.bClubhouseMissionWallActive = FALSE
	ENDIF
	
	UPDATE_GLOBAL_CHM_MISSION_LIST()
//	GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.timestamp = GlobalServerBD_FM.ClubhouseMissionData.timestamp
	INT i
	REPEAT iCONST_NUM_CLUBHOUSE_MISSIONS i
		IF NOT IS_BIT_SET(GlobalServerBD_FM.ClubhouseMissionData.iCHMissionInvalidBS, i)
		AND NOT IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissionInvalidBS, i)
			GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[i] = GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[i]
			
			IF GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[i] != ENUM_TO_INT(CLUBHOUSE_MISSION_invalid)
				PRINTLN("[CLUBHOUSE] INITIALISE_CHM_MISSION_LIST - slot ", i, " set to [", INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[i]), "] ", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[i]))))
			ELSE
				PRINTLN("[CLUBHOUSE] INITIALISE_CHM_MISSION_LIST - slot ", i, " set to [", CLUBHOUSE_MISSION_invalid, "] CLUBHOUSE_MISSION_invalid")
			ENDIF
			g_bClubhouseMissionWallUpdate = TRUE
		ELSE
			PRINTLN("[CLUBHOUSE] INITIALISE_CHM_MISSION_LIST - slot ", i, " set to [", CLUBHOUSE_MISSION_invalid, "] bit is set")
			GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[i] = ENUM_TO_INT(CLUBHOUSE_MISSION_invalid)
		ENDIF
	ENDREPEAT
	
	GB_MC_UPDATE_FAVOURITE_FORMATION()
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iFormationtime0				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME0)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iFormationtime1				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME1)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iFormationtime2				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME2)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iFormationtime3				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME3)
	SET_PLAYERS_FAVOURITE_PLAYLIST(GET_LOCAL_PLAYERS_FAVOURITE_JUKEBOX_PLAYLIST_ID())
//	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iXXX_Four					= GET_MP_INT_CHARACTER_STAT(MP_STAT_XXX_Four)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iMembersmarkedfordeath		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MEMBERSMARKEDFORDEATH)
	
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iMckills		 				= GET_MP_INT_CHARACTER_STAT(MP_STAT_MCKILLS)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iMcdeaths					= GET_MP_INT_CHARACTER_STAT(MP_STAT_MCDEATHS)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iRivalpresidentkills			= GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALPRESIDENTKILLS)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iRivalceoandvipkills	 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALCEOANDVIPKILLS)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iMeleekills			 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MELEEKILLS)
	
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iClubhousecontractscomplete	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBHOUSECONTRACTSCOMPLETE)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iClubhousecontractearnings	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBHOUSECONTRACTEARNINGS)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iClubworkcompleted			= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBWORKCOMPLETED)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iClubchallengescompleted		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBCHALLENGESCOMPLETED)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iMemberchallengescompleted	= GET_MP_INT_CHARACTER_STAT(MP_STAT_MEMBERCHALLENGESCOMPLETED)
	
ENDPROC

/// PURPOSE:
///    Initialize clubhouse mission menu
/// PARAMS:
///    iCurrentProperty - current clubhouse(property) 
PROC INIT_CHM_MENU(CLUBHOUSE_MISSION_MENU_STRUCT &sClubHouseMissionMenu,INT iCurrentProperty)
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty,MP_PROP_ELEMENT_CLUBHOUSE_MISSION_MENU_TRIGGER,sClubHouseMissionMenu.sTriggerOffset,GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty))
	
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		sClubHouseMissionMenu.tl23TriggerRoom = "bikerDLC_Int01_PlnRm"
	ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
		sClubHouseMissionMenu.tl23TriggerRoom = "int02_PlanRm"
	ELSE
		sClubHouseMissionMenu.tl23TriggerRoom = ""
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[CLUBHOUSE] INIT_CHM_MENU - iCurrentProperty:",iCurrentProperty ," Trigger position: ", sClubHouseMissionMenu.sTriggerOffset.vLoc)
	#ENDIF
	
	sClubHouseMissionMenu.iMenuCurrentColumn = 0
	sClubHouseMissionMenu.iMenuCurrentRow = 0
	
	//seems if we close the board and keep holding ESC pause menu opens quite quickly here, adding a little delay after cleanup to ensure we don't open pause menu by accident
	IF HAS_NET_TIMER_STARTED(sClubHouseMissionMenu.timerPostCleanupButtonDelay)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		IF HAS_NET_TIMER_EXPIRED(sClubHouseMissionMenu.timerPostCleanupButtonDelay, 500)
			SET_CLUBHOUSE_MISSION_MENU_STATE(sClubHouseMissionMenu,CHM_MENU_STAGE_WAITING_TO_TRIGGER)
			RESET_NET_TIMER(sClubHouseMissionMenu.timerPostCleanupButtonDelay)
		ENDIF
	ELSE
		SET_CLUBHOUSE_MISSION_MENU_STATE(sClubHouseMissionMenu,CHM_MENU_STAGE_WAITING_TO_TRIGGER)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Check if player is facing the clubhouse mission trigger point
FUNC BOOL IS_PLAYER_FACING_MISSION_MENU(CLUBHOUSE_MISSION_MENU_STRUCT &sClubHouseMissionMenu)
	IF NOT IS_STRING_NULL_OR_EMPTY(sClubHouseMissionMenu.tl23TriggerRoom)
	AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != GET_HASH_KEY(sClubHouseMissionMenu.tl23TriggerRoom)
		RETURN FALSE
	ENDIF
	
	VECTOR vToMissionMenu = NORMALISE_VECTOR((GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sClubHouseMissionMenu.sTriggerOffset.vRot.z, <<0,1,0>>) - GET_ENTITY_COORDS(PLAYER_PED_ID())))
	FLOAT fDot = DOT_PRODUCT(GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()), vToMissionMenu)
	
	IF fDot >= 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Clean up clubhouse mission menu 
PROC CLEANUP_CH_MISSION_MENU(CLUBHOUSE_MISSION_MENU_STRUCT &sClubHouseMissionMenu,BOOL bForceCleanup = FALSE)
	
	RELEASE_CONTEXT_INTENTION(sClubHouseMissionMenu.iCHMissionMenuContext)
	sClubHouseMissionMenu.iCHMissionMenuContext = NEW_CONTEXT_INTENTION
//	sClubHouseMissionMenu.iMenuCurrentColumn = 0
//	sClubHouseMissionMenu.iMenuCurrentRow = 0
//	sClubHouseMissionMenu.iMenuTopItem = 0
//	sClubHouseMissionMenu.bMenuInitialised = FALSE
//	sClubHouseMissionMenu.bReBuildMenu = FALSE
	
	IF DOES_CAM_EXIST(sClubHouseMissionMenu.ciCam)
		IF IS_CAM_ACTIVE(sClubHouseMissionMenu.ciCam)
			SET_PLAYER_VISIBLE_LOCALLY(PLAYER_ID())
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		ENDIF
		DESTROY_CAM(sClubHouseMissionMenu.ciCam)
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(sClubHouseMissionMenu.tlPrintedHelp)
		CLEAR_HELP()
		sClubHouseMissionMenu.tlPrintedHelp = ""
	ENDIF
	
	RESET_NET_TIMER(sClubHouseMissionMenu.timerCutsceneTotal)
	START_NET_TIMER(sClubHouseMissionMenu.timerPostCleanupButtonDelay)
	g_bClubhouseMissionWallActive = FALSE
	sClubHouseMissionMenu.bFlickedIn = FALSE
	GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.bClubhouseMissionWallActive = FALSE
	
	g_bClubhouseMissionWallUpdate = TRUE
	
	IF bForceCleanup 
		//SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("")
	ENDIF 
	
	sClubHouseMissionMenu.eMenuState = CHM_MENU_STAGE_INIT
ENDPROC

FUNC BOOL IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(PLAYER_INDEX playerID, CLUBHOUSE_MISSION_ENUM eCHMission, INT &iFailCondition)
	IF eCHMission = CLUBHOUSE_MISSION_invalid
		iFailCondition = 99
		RETURN TRUE
	ENDIF
	
	UNUSED_PARAMETER(playerID)
	iFailCondition = -1
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CH_MISSION_ON_COOLDOWN(INT iType)

	SWITCH iType
		CASE FMMC_TYPE_CLUBHOUSE_CONTRACTS
			RETURN FALSE
	ENDSWITCH
	
	RETURN GB_RETURN_ARRAY_POS_FROM_TYPE(iType) != -1
	AND !GB_HAS_BOSSWORK_COOLDOWN_TIMER_EXPIRED(iType)

ENDFUNC

/// PURPOSE:
///    Check if current menu item is unavailable
///    checks if item is not purchased
///    returns true if items is purchased
FUNC BOOL IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_LAUNCH(PLAYER_INDEX playerID, CLUBHOUSE_MISSION_ENUM eCHMission, INT &iFailCondition)
	IF IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(playerID, eCHMission, iFailCondition)
		iFailCondition = 99
		RETURN TRUE
	ENDIF
	
	INT iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(eCHMission)
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(playerID)
		iFailCondition = 1
		RETURN TRUE
	ELIF IS_CH_MISSION_ON_COOLDOWN(iType)
		
		#IF IS_DEBUG_BUILD
		IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_BikerFlowPlayVariations")
		#ENDIF
			iFailCondition = 0
			RETURN TRUE
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
	ELIF !GB_IS_BOSS_MISSION_AVAILABLE(playerID, iType)
		iFailCondition = 2
		RETURN TRUE
	ELIF !GB_IS_LOCAL_PLAYER_SAFE_TO_LAUNCH_GANG_BOSS_MISSION()
		iFailCondition = 3
		RETURN TRUE
	ENDIF
	
	BOOL bAnyPlayerNearCriticalLocation = TRUE
	INT i
	REPEAT GB_BIKER_GET_NUMBER_OF_VARIATIONS_FOR_CLUBHOUSE_MISSION(iType) i
		IF NOT GB_BIKER_IS_ANY_PLAYER_NEAR_CLUBHOUSE_MISSION_CRITICAL_LOCATION(iType, i)
			bAnyPlayerNearCriticalLocation = FALSE
			i = GB_BIKER_GET_NUMBER_OF_VARIATIONS_FOR_CLUBHOUSE_MISSION(iType)+1		//bail
		ENDIF
	ENDREPEAT
	IF bAnyPlayerNearCriticalLocation
		iFailCondition = 4
		RETURN TRUE
	ENDIF

	REPEAT GB_MAX_GANG_GOONS i
		PLAYER_INDEX memberID = GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].sMagnateGangBossData.GangMembers[i]
		IF memberID != INVALID_PLAYER_INDEX()
			IF MGID_CLUBHOUSE_PLAYER_GET(memberID) = eMGID_ARMWRESTLE
				CDEBUG3LN(DEBUG_AMBIENT, "IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_LAUNCH: memberID[", i, ":", GET_PLAYER_NAME(memberID), "] of playerID[", GET_PLAYER_NAME(playerID), "] is arm wrestling!!!")
				iFailCondition = 5
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF IS_CH_MISSION_BLOCKED_BY_TUNEABLES(eCHMission)
		iFailCondition = 6
		RETURN TRUE
	ENDIF
	
	iFailCondition = -1
	RETURN FALSE
ENDFUNC


FUNC BOOL BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED(INT iType, INT iVariation)
	CLUBHOUSE_MISSION_ENUM e
	CLUBHOUSE_MISSION_ENUM eCHMission = CLUBHOUSE_MISSION_invalid
	REPEAT CLUBHOUSE_MISSION_MAX_MENU_ITEM e
		IF iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(e)
			eCHMission = e
			e = CLUBHOUSE_MISSION_MAX_MENU_ITEM	//bail
		ENDIF
	ENDREPEAT
	
	IF GB_BIKER_IS_ANY_PLAYER_NEAR_CLUBHOUSE_MISSION_CRITICAL_LOCATION(iType, iVariation)
		CDEBUG3LN(DEBUG_AMBIENT, "BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED: mission \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" ", iVariation, " blocked for player in critical location")
		RETURN TRUE
	ENDIF
	
	IF IS_CH_MISSION_BLOCKED_BY_TUNEABLES(eCHMission, iVariation)
		CDEBUG3LN(DEBUG_AMBIENT, "BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED: mission \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" ", iVariation, " blocked for tunables")
		RETURN TRUE
	ENDIF
	
	CDEBUG3LN(DEBUG_AMBIENT, "BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED: mission \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" ", iVariation, " not blocked...")
	RETURN FALSE
ENDFUNC

FUNC BOOL BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED(INT iType, INT iVariation, INT iSubvariation)
	CLUBHOUSE_MISSION_ENUM e
	CLUBHOUSE_MISSION_ENUM eCHMission = CLUBHOUSE_MISSION_invalid
	REPEAT CLUBHOUSE_MISSION_MAX_MENU_ITEM e
		IF iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(e)
			eCHMission = e
			e = CLUBHOUSE_MISSION_MAX_MENU_ITEM	//bail
		ENDIF
	ENDREPEAT
	
	INT iPlayerIndex
	REPEAT NUM_NETWORK_PLAYERS iPlayerIndex
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iPlayerIndex)
		IF IS_NET_PLAYER_OK(playerID, TRUE, FALSE)
			IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID)
				IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = iType
					IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() = iVariation
						IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2() = iSubvariation
							CDEBUG3LN(DEBUG_AMBIENT, "BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED: mission \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" ", iVariation, " blocked. There is a gang already on this subvariation.")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDREPEAT
		
	IF IS_CH_MISSION_BLOCKED_BY_TUNEABLES(eCHMission, iVariation)
		CDEBUG3LN(DEBUG_AMBIENT, "BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED: mission \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" ", iVariation, " blocked for tunables")
		RETURN TRUE
	ENDIF
	
	CDEBUG3LN(DEBUG_AMBIENT, "BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED: mission \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" ", iVariation, " not blocked...")
	RETURN FALSE
ENDFUNC

#IF FEATURE_DLC_1_2022
FUNC INT GET_CLUBHOUSE_CONTRACT_MISSION_VARIATION(CLUBHOUSE_MISSION_ENUM eContract)
	SWITCH eContract
		CASE CLUBHOUSE_MISSION_DEATHBIKE_DELIVERY
			CPRINTLN(DEBUG_AMBIENT, "[FMMC_TYPE_CLUBHOUSE_CONTRACTS] GET_CLUBHOUSE_CONTRACT_MISSION_VARIATION: PLAYER LAUNCHING CLUBHOUSE_MISSION_DEATHBIKE_DELIVERY, VARIATION : CCV_MOTHERBOARD_ROBBERY")
			RETURN 	ENUM_TO_INT(CCV_MOTHERBOARD_ROBBERY)
		BREAK
		CASE CLUBHOUSE_MISSION_ROOFTOP_CONTAINER
			CPRINTLN(DEBUG_AMBIENT, "[FMMC_TYPE_CLUBHOUSE_CONTRACTS] GET_CLUBHOUSE_CONTRACT_MISSION_VARIATION: PLAYER LAUNCHING CLUBHOUSE_MISSION_ROOFTOP_CONTAINER, VARIATION : CCV_ROOFTOP_CONTAINER")
			RETURN ENUM_TO_INT(CCV_ROOFTOP_CONTAINER)
		BREAK
	ENDSWITCH
	
	CASSERTLN(DEBUG_AMBIENT, "[CLUBHOUSE] GET_CLUBHOUSE_CONTRACT_MISSION_VARIATION INVALID MISSION ENUM RETURNING . Tell Jan Mencfel")
	RETURN ENUM_TO_INT(CCV_INVALID)  
	
ENDFUNC
#ENDIF
FUNC INT GET_SAFE_GANG_BOSS_MISSION_VARIATION(INT iType)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_BikerFlowPlayVariations")
		IF iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(CLUBHOUSE_MISSION_DEAL_GONE_WRONG)	//-	By the Pound (Chicken Factory) - variation 2
			CPRINTLN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: temp force variation 2 for CLUBHOUSE_MISSION_DEAL_GONE_WRONG")
			RETURN 2
//		ELIF iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(CLUBHOUSE_MISSION_CONTRACT_KILLING)	//-	Weapon of Choice		
//			CPRINTLN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: temp force variation -1 for CLUBHOUSE_MISSION_CONTRACT_KILLING")
//			RETURN -1
		ELIF iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(CLUBHOUSE_MISSION_LAST_RESPECTS)	//-	Guns for Hire (Vanilla Unicorn) - variation 2		
			CPRINTLN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: temp force variation 2 for CLUBHOUSE_MISSION_LAST_RESPECTS")
			RETURN 2
		
//		ELIF iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(CLUBHOUSE_MISSION_FREE_PRISONER)		//-	Jailbreak
//			CPRINTLN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: temp force variation -1 for CLUBHOUSE_MISSION_FREE_PRISONER")
//			RETURN -1
//		ELIF iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(CLUBHOUSE_MISSION_SHUTTLE)			//-	Outrider
//			CPRINTLN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: temp force variation -1 for CLUBHOUSE_MISSION_SHUTTLE")
//			RETURN -1
		ELIF iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(CLUBHOUSE_MISSION_RESCUE_CONTACT)	//-	POW (O’ Neil farm)
			CPRINTLN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: temp force variation 2 for CLUBHOUSE_MISSION_RESCUE_CONTACT")
			RETURN 2	//POW (O’ Neil farm)
		
//		ELIF iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(CLUBHOUSE_MISSION_UNLOAD_WEAPONS)	//-	Gunrunning		
//			CPRINTLN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: temp force variation -1 for CLUBHOUSE_MISSION_UNLOAD_WEAPONS")
//			RETURN -1
//		ELIF iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(CLUBHOUSE_MISSION_DESTROY_VANS)		//-	Fragile Goods		
//			CPRINTLN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: temp force variation -1 for CLUBHOUSE_MISSION_DESTROY_VANS")
//			RETURN -1
		ELIF iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(CLUBHOUSE_MISSION_BURN_ASSETS)		//-	Torched (Textile City) - variation 0		
			CPRINTLN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: temp force variation 0 for CLUBHOUSE_MISSION_BURN_ASSETS")
			RETURN 0
		ENDIF
	ENDIF
	#ENDIF
	
	INT iNUMBER_OF_VARIATIONS_FOR_CLUBHOUSE_MISSION = GB_BIKER_GET_NUMBER_OF_VARIATIONS_FOR_CLUBHOUSE_MISSION(iType)
	IF BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED(iType, 0)
		IF BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED(iType, 1)
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: variation 2 (player near 0's critical location)")
			RETURN 2
		ELIF (iNUMBER_OF_VARIATIONS_FOR_CLUBHOUSE_MISSION = 3
		AND BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED(iType, 2))
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: variation 1 (player near 0's critical location)")
			RETURN 1
		ELSE
			IF (GET_RANDOM_BOOL()
			OR iNUMBER_OF_VARIATIONS_FOR_CLUBHOUSE_MISSION != 3)
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: random variation 1 (player near 0's critical location)")
				RETURN 1
			ELSE
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: random variation 2 (player near 0's critical location)")
				RETURN 2
			ENDIF
		ENDIF
	ELIF BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED(iType, 1)
		IF BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED(iType, 0)
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: variation 2 (player near 1's critical location)")
			RETURN 2
		ELIF (iNUMBER_OF_VARIATIONS_FOR_CLUBHOUSE_MISSION = 3
		AND BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED(iType, 2))
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: variation 0 (player near 1's critical location)")
			RETURN 0
		ELSE
			IF (GET_RANDOM_BOOL()
			OR iNUMBER_OF_VARIATIONS_FOR_CLUBHOUSE_MISSION != 3)
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: random variation 0 (player near 1's critical location)")
				RETURN 0
			ELSE
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: random variation 2 (player near 1's critical location)")
				RETURN 2
			ENDIF
		ENDIF
	ELIF (iNUMBER_OF_VARIATIONS_FOR_CLUBHOUSE_MISSION = 3
	AND BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED(iType, 2))
		IF BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED(iType, 0)
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: variation 0 (player near 2's critical location)")
			RETURN 1
		ELIF BIKER_IS_FMMC_TYPE_VARIATION_BLOCKED(iType, 1)
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: variation 1 (player near 2's critical location)")
			RETURN 0
		ELSE
			IF GET_RANDOM_BOOL()
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: random variation 0 (player near 2's critical location)")
				RETURN 0
			ELSE
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: random variation 1 (player near 2's critical location)")
				RETURN 1
			ENDIF
		ENDIF
	ELSE
		INT iRandomVariation = -1	//GET_RANDOM_INT_IN_RANGE(0, iNUMBER_OF_VARIATIONS_FOR_CLUBHOUSE_MISSION)
		CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_VARIATION: any random variation ", iRandomVariation)
		RETURN iRandomVariation
	ENDIF
ENDFUNC

FUNC INT GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION(INT iType, INT iVariation)
		
	INT iNUMBER_OF_SUBVARIATIONS_FOR_CLUBHOUSE_MISSION = GB_BIKER_GET_NUMBER_OF_SUBVARIATIONS_FOR_CLUBHOUSE_MISSION(iType)
	IF BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED(iType, iVariation, 0)
		IF BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED(iType, iVariation, 1)
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: subvariation 2 (player already on 0 subvariation)")
			RETURN 2
		ELIF (iNUMBER_OF_SUBVARIATIONS_FOR_CLUBHOUSE_MISSION = 3
		AND BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED(iType, iVariation, 2))
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: subvariation 1 (player already on 0 subvariation)")
			RETURN 1
		ELSE
			IF (GET_RANDOM_BOOL()
			OR iNUMBER_OF_SUBVARIATIONS_FOR_CLUBHOUSE_MISSION != 3)
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: random subvariation 1 (player already on 0 subvariation)")
				RETURN 1
			ELSE
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: random subvariation 2 (player already on 0 subvariation)")
				RETURN 2
			ENDIF
		ENDIF
	ELIF BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED(iType, iVariation, 1)
		IF BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED(iType, iVariation, 0)
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: subvariation 2 (player already on 1 subvariation)")
			RETURN 2
		ELIF (iNUMBER_OF_SUBVARIATIONS_FOR_CLUBHOUSE_MISSION = 3
		AND BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED(iType, iVariation, 2))
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: subvariation 0 (player already on 1 subvariation)")
			RETURN 0
		ELSE
			IF (GET_RANDOM_BOOL()
			OR iNUMBER_OF_SUBVARIATIONS_FOR_CLUBHOUSE_MISSION != 3)
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: random subvariation 0 (player already on 1 subvariation)")
				RETURN 0
			ELSE
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: random subvariation 2 (player already on 1 subvariation)")
				RETURN 2
			ENDIF
		ENDIF
	ELIF (iNUMBER_OF_SUBVARIATIONS_FOR_CLUBHOUSE_MISSION = 3
	AND BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED(iType, iVariation, 2))
		IF BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED(iType, iVariation, 0)
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: subvariation 0 (player already on 2 subvariation)")
			RETURN 1
		ELIF BIKER_IS_FMMC_TYPE_SUBVARIATION_BLOCKED(iType, iVariation, 1)
			CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: subvariation 1 (player already on 2 subvariation)")
			RETURN 0
		ELSE
			IF GET_RANDOM_BOOL()
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: random subvariation 0 (player already on 2 subvariation)")
				RETURN 0
			ELSE
				CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: random subvariation 1 (player already on 2 subvariation)")
				RETURN 1
			ENDIF
		ENDIF
	ELSE
		INT iRandomSubVariation = -1	//GET_RANDOM_INT_IN_RANGE(0, iNUMBER_OF_VARIATIONS_FOR_CLUBHOUSE_MISSION)
		CDEBUG3LN(DEBUG_AMBIENT, "GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION: any random subvariation ", iRandomSubVariation)
		RETURN iRandomSubVariation
	ENDIF
ENDFUNC

// Input used for zooming - different on PC.
FUNC CONTROL_ACTION GET_CH_MISSION_ZOOM_INPUT()
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN INPUT_FRONTEND_LS
	ELSE
		RETURN INPUT_FRONTEND_LT
	ENDIF
ENDFUNC

FUNC BOOL SAFE_TO_INVITE_MC_BOSSES_FOR_CLUBHOUSE_MISSION(CLUBHOUSE_MISSION_ENUM eCHMission)
	INT iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(eCHMission)
	
	//temp for 2950066
	IF iType != FMMC_TYPE_BIKER_DEAL_GONE_WRONG
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

BOOL bUpdateControls = FALSE

/// PURPOSE:
///    Update clubhouse mission main menu
PROC UPDATE_CH_MISSION_MAIN_MENU(CLUBHOUSE_MISSION_MENU_STRUCT &sClubHouseMissionMenu)
	// Ignore keypress when it's not safe.
	BOOL bKeyPressSafe = TRUE
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR IS_WARNING_MESSAGE_ACTIVE()
	OR NETWORK_TEXT_CHAT_IS_TYPING()
	OR g_sShopSettings.bProcessStoreAlert
		bKeyPressSafe = FALSE
	ENDIF
	
	CONST_INT iDelay		0100
	CONST_INT iTotalTime	1000
	CONST_FLOAT fCONST_MoveAllow	0.6
	FLOAT fTimeFactor = TO_FLOAT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GET_TIME_OFFSET(sClubHouseMissionMenu.timerCutsceneTotal.Timer, iDelay))) / TO_FLOAT(iTotalTime - iDelay)
	IF fTimeFactor < 0.0
	//	sClubHouseMissionMenu.sStoredOffset.vCoord = sClubHouseMissionMenu.sCurrentOffset.vCoord
	//	sClubHouseMissionMenu.sStoredOffset.vPoint = sClubHouseMissionMenu.sCurrentOffset.vPoint
		fTimeFactor = 0.0
	ENDIF
	IF fTimeFactor > 1.0
		fTimeFactor = 1.0
	ENDIF
	
	
	// Purchase
	IF (bKeyPressSafe AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT))
	OR (bKeyPressSafe AND sClubHouseMissionMenu.sCHMenuInput.bAccept)
	//OR iUpgradeTransactionState != GENERIC_TRANSACTION_STATE_DEFAULT
		
		BOOL bPurchaseMission = FALSE
		CLUBHOUSE_MISSION_ENUM eMenuSelectedItem = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn])
	
		PRINTLN("UPDATE_CH_MISSION_MAIN_MENU CHM_MENU_STAGE_SELECT_MISSION")
		PRINTLN("...INPUT_FRONTEND_ACCEPT = ", IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT))
		PRINTLN("...bAccept = ", sClubHouseMissionMenu.sCHMenuInput.bAccept)
		//PRINTLN("...iUpgradeTransactionState != GENERIC_TRANSACTION_STATE_DEFAULT = ", iUpgradeTransactionState != GENERIC_TRANSACTION_STATE_DEFAULT)
		
		IF sClubHouseMissionMenu.iMenuCurrentRow = 0
		AND sClubHouseMissionMenu.bFlickedIn
			INT iFailCondition
			IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_LAUNCH(sClubHouseMissionMenu.pOwnerID, eMenuSelectedItem, iFailCondition)
				sClubHouseMissionMenu.eSelectedMission = eMenuSelectedItem
			//	sClubHouseMissionMenu.iMenuCurrentColumn = 0
				
				bPurchaseMission = TRUE
			ELSE
				PLAY_SOUND_FRONTEND(-1, "ERROR", "DLC_Biker_Mission_Wall_Sounds")
				
				#IF IS_DEBUG_BUILD
				SWITCH iFailCondition
					CASE 1	//GB_IS_PLAYER_CRITICAL_TO_JOB
						CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU SELECT_MISSION ACCEPT: GB mission ", eMenuSelectedItem, " \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eMenuSelectedItem)), "\" not available, critical to job")
					BREAK
					CASE 0	//GB_HAS_BOSSWORK_COOLDOWN_TIMER_EXPIRED
						CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU SELECT_MISSION ACCEPT: GB mission ", eMenuSelectedItem, " \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eMenuSelectedItem)), "\" not available, cooldown timer")
					BREAK
					CASE 2	//GB_IS_BOSS_MISSION_AVAILABLE
						CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU SELECT_MISSION ACCEPT: GB mission ", eMenuSelectedItem, " \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eMenuSelectedItem)), "\" not available, type not available")
					BREAK
					CASE 3	//GB_IS_LOCAL_PLAYER_SAFE_TO_LAUNCH_GANG_BOSS_MISSION
						CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU SELECT_MISSION ACCEPT: GB mission ", eMenuSelectedItem, " \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eMenuSelectedItem)), "\" not available, not safe to launch")
					BREAK
					CASE 4	//GB_BIKER_IS_ANY_PLAYER_NEAR_CLUBHOUSE_MISSION_CRITICAL_LOCATION
						CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU SELECT_MISSION ACCEPT: GB mission ", eMenuSelectedItem, " \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eMenuSelectedItem)), "\" has player near critical location")
					BREAK
					CASE 5	//MGID_CLUBHOUSE_GET() = eMGID_ARMWRESTLE
						CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU SELECT_MISSION ACCEPT: GB mission ", eMenuSelectedItem, " \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eMenuSelectedItem)), "\" has player playing armwrestling...")
					BREAK
					
					CASE 99
						CERRORLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU SELECT_MISSION ACCEPT: GB mission ", eMenuSelectedItem, " \"CLUBHOUSE_MISSION_invalid\" not valid, iFailCondition:", iFailCondition)
					BREAK
					
					DEFAULT
						CERRORLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU SELECT_MISSION ACCEPT: GB mission ", eMenuSelectedItem, " \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eMenuSelectedItem)), "\" unknown fail condition ", iFailCondition)
					BREAK
				ENDSWITCH
				#ENDIF
			ENDIF
		ELSE
			//
		ENDIF
		
		IF bPurchaseMission
			PLAY_SOUND_FRONTEND(-1,"Highlight_Accept","DLC_Biker_Mission_Wall_Sounds")
			
			INT iMission = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(sClubHouseMissionMenu.eSelectedMission)
					
			#IF FEATURE_DLC_1_2022
			INT iVariation 
			INT iSubvariation
			if iMission = FMMC_TYPE_CLUBHOUSE_CONTRACTS
				iVariation = GET_CLUBHOUSE_CONTRACT_MISSION_VARIATION(sClubHouseMissionMenu.eSelectedMission)
				iSubvariation = GET_SAFE_GANG_BOSS_MISSION_SUBVARIATION(iMission, iVariation)
			ELSE
				iVariation = GET_SAFE_GANG_BOSS_MISSION_VARIATION(iMission)
			ENDIF
			#ENDIF
			#IF NOT FEATURE_DLC_1_2022
			INT iVariation = GET_SAFE_GANG_BOSS_MISSION_VARIATION(iMission)
			#ENDIF
			
			CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU ACCEPT: solo mission, variation ", iVariation)
			GB_SET_PLAYER_GANG_BOSS_MISSION_VARIATION(iVariation)
			#IF FEATURE_DLC_1_2022
			GB_SET_PLAYER_GANG_BOSS_MISSION_VARIATION2(iSubvariation)
			#ENDIF
			
			GB_BOSS_REQUEST_MISSION_LAUNCH_FROM_SERVER(GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(sClubHouseMissionMenu.eSelectedMission))
			
			
//			INT i
//			REPEAT NUM_NETWORK_PLAYERS i
//				IF GlobalPlayerBD_FM[i].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] != -1
//					SET_BIT(GlobalplayerBD_FM[i].ClubhouseMissionData.iCHMissionInvalidBS, sClubHouseMissionMenu.iMenuCurrentColumn)
//					GlobalPlayerBD_FM[i].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] = -1
//				ENDIF
//			ENDREPEAT
			
			GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] = -1
			SET_BIT(GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissionInvalidBS, sClubHouseMissionMenu.iMenuCurrentColumn)
			BROADCAST_CLUBHOUSE_MISSION_MISSION_LAUNCHED(sClubHouseMissionMenu.iMenuCurrentColumn)
			
			
			NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			SET_CLUBHOUSE_MISSION_MENU_STATE(sClubHouseMissionMenu,CHM_MENU_STAGE_CLEANUP)
		ELSE
			PLAY_SOUND_FRONTEND(-1,"Highlight_Error","DLC_Biker_Mission_Wall_Sounds")
			CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU ACCEPT: unpurchable mission")
		ENDIF
		
//		sClubHouseMissionMenu.bReBuildMenu = TRUE
//		sClubHouseMissionMenu.bMenuInitialised = FALSE
		
	// Block movememnt when it's not safe.
	ELIF NOT bKeyPressSafe
		// do nothing
		
	// Exit
	ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL))
	OR sClubHouseMissionMenu.sCHMenuInput.bBack
//		sClubHouseMissionMenu.bReBuildMenu = TRUE
		PLAY_SOUND_FRONTEND(-1, "Highlight_Back", "DLC_Biker_Mission_Wall_Sounds")
//		sClubHouseMissionMenu.bMenuInitialised = FALSE
		
//		sClubHouseMissionMenu.iMenuTopItem = GET_TOP_MENU_ITEM()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		SET_CLUBHOUSE_MISSION_MENU_STATE(sClubHouseMissionMenu,CHM_MENU_STAGE_CLEANUP)	
		
	// Navigate menu
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT) //AND ALLOW_ANALOGUE_MOVEMENT(sClubHouseMissionMenu.scrollDelay,sClubHouseMissionMenu.sCHMenuInput.iMoveUp, FALSE)
	OR sClubHouseMissionMenu.sCHMenuInput.bLeft AND ALLOW_ANALOGUE_MOVEMENT(sClubHouseMissionMenu.scrollDelay,sClubHouseMissionMenu.sCHMenuInput.iMoveUp, FALSE) AND (fTimeFactor >= fCONST_MoveAllow)
	//	IF sClubHouseMissionMenu.sCHMenuInput.iMoveUp > 0
			INT iPrevColumn = sClubHouseMissionMenu.iMenuCurrentColumn
			
			sClubHouseMissionMenu.iMenuCurrentColumn--
			IF sClubHouseMissionMenu.iMenuCurrentColumn < 0
				sClubHouseMissionMenu.iMenuCurrentColumn = iCONST_NUM_CLUBHOUSE_MISSIONS-1
			ENDIF
			
			IF sClubHouseMissionMenu.iMenuCurrentRow = 0
			#IF IS_DEBUG_BUILD
			AND NOT (IS_KEYBOARD_KEY_PRESSED(KEY_LMENU) OR IS_KEYBOARD_KEY_PRESSED(KEY_RMENU))
			#ENDIF
				INT iFailCondition
				CLUBHOUSE_MISSION_ENUM eMenuSelectedItem = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn])
				IF IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, eMenuSelectedItem, iFailCondition)
				
					BOOL bFound = FALSE
					INT iSelection = sClubHouseMissionMenu.iMenuCurrentColumn-1
					IF iSelection < 0
						iSelection = iCONST_NUM_CLUBHOUSE_MISSIONS-1
					ENDIF
					
					WHILE iSelection != sClubHouseMissionMenu.iMenuCurrentColumn
					AND NOT bFound
						eMenuSelectedItem = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[iSelection])
						IF iSelection < iCONST_NUM_CLUBHOUSE_MISSIONS
						AND !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, eMenuSelectedItem, iFailCondition)
							sClubHouseMissionMenu.iMenuCurrentColumn = iSelection
							bFound = TRUE
						ENDIF
						IF NOT bFound
							iSelection--
							IF iSelection < 0
								iSelection = iCONST_NUM_CLUBHOUSE_MISSIONS-1
							ENDIF
						ENDIF
					ENDWHILE
					
					IF NOT bFound
						sClubHouseMissionMenu.iMenuCurrentColumn = 0
					ENDIF
				ENDIF
			ENDIF
			
			REINIT_NET_TIMER(sClubHouseMissionMenu.timerCutsceneTotal)
//			SET_CURRENT_MENU_ITEM(sClubHouseMissionMenu.iMenuCurrentColumn)
			IF iPrevColumn != sClubHouseMissionMenu.iMenuCurrentColumn
				PLAY_SOUND_FRONTEND(-1, "Highlight_Move_Left_Right", "DLC_Biker_Mission_Wall_Sounds")
			ENDIF
			bUpdateControls = FALSE
			sClubHouseMissionMenu.bFlickedIn = TRUE
	//	ENDIF
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT) //AND ALLOW_ANALOGUE_MOVEMENT(sClubHouseMissionMenu.scrollDelay,sClubHouseMissionMenu.sCHMenuInput.iMoveUp, FALSE)
	OR sClubHouseMissionMenu.sCHMenuInput.bRight AND ALLOW_ANALOGUE_MOVEMENT(sClubHouseMissionMenu.scrollDelay,sClubHouseMissionMenu.sCHMenuInput.iMoveUp, FALSE) AND (fTimeFactor >= fCONST_MoveAllow)
	//	IF sClubHouseMissionMenu.sCHMenuInput.iMoveUp < 0
			INT iPrevColumn = sClubHouseMissionMenu.iMenuCurrentColumn
			
			sClubHouseMissionMenu.iMenuCurrentColumn++
			IF sClubHouseMissionMenu.iMenuCurrentColumn >= iCONST_NUM_CLUBHOUSE_MISSIONS
				sClubHouseMissionMenu.iMenuCurrentColumn = 0
			ENDIF
			
			IF sClubHouseMissionMenu.iMenuCurrentRow = 0
			#IF IS_DEBUG_BUILD
			AND NOT (IS_KEYBOARD_KEY_PRESSED(KEY_LMENU) OR IS_KEYBOARD_KEY_PRESSED(KEY_RMENU))
			#ENDIF
				INT iFailCondition
				CLUBHOUSE_MISSION_ENUM eMenuSelectedItem = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn])
				IF IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, eMenuSelectedItem, iFailCondition)
				
					BOOL bFound = FALSE
					INT iSelection = sClubHouseMissionMenu.iMenuCurrentColumn+1
					IF iSelection >= iCONST_NUM_CLUBHOUSE_MISSIONS
						iSelection = 0
					ENDIF
					
					WHILE iSelection != sClubHouseMissionMenu.iMenuCurrentColumn
					AND NOT bFound
						eMenuSelectedItem = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[iSelection])
						IF iSelection < iCONST_NUM_CLUBHOUSE_MISSIONS
						AND !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, eMenuSelectedItem, iFailCondition)
							sClubHouseMissionMenu.iMenuCurrentColumn = iSelection
							bFound = TRUE
						ENDIF
						IF NOT bFound
							iSelection++
							IF iSelection >= iCONST_NUM_CLUBHOUSE_MISSIONS
								iSelection = 0
							ENDIF
						ENDIF
					ENDWHILE
					
					IF NOT bFound
						sClubHouseMissionMenu.iMenuCurrentColumn = 0
					ENDIF
				ENDIF
			ENDIF
			
			REINIT_NET_TIMER(sClubHouseMissionMenu.timerCutsceneTotal)
			//SET_CURRENT_MENU_ITEM(sClubHouseMissionMenu.iMenuCurrentColumn)
			IF iPrevColumn != sClubHouseMissionMenu.iMenuCurrentColumn
				PLAY_SOUND_FRONTEND(-1, "Highlight_Move_Left_Right", "DLC_Biker_Mission_Wall_Sounds")
			ENDIF
			bUpdateControls = FALSE
			sClubHouseMissionMenu.bFlickedIn = TRUE
	//	ENDIF
	
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP) //AND ALLOW_ANALOGUE_MOVEMENT(sClubHouseMissionMenu.scrollDelay,sClubHouseMissionMenu.sCHMenuInput.iMoveUp, TRUE)
	OR sClubHouseMissionMenu.sCHMenuInput.bUP AND ALLOW_ANALOGUE_MOVEMENT(sClubHouseMissionMenu.scrollDelay,sClubHouseMissionMenu.sCHMenuInput.iMoveUp, TRUE) AND (fTimeFactor >= fCONST_MoveAllow)
	//	IF sClubHouseMissionMenu.sCHMenuInput.iMoveUp > 0
			sClubHouseMissionMenu.iMenuCurrentRow--
			IF sClubHouseMissionMenu.iMenuCurrentRow < 0
				sClubHouseMissionMenu.iMenuCurrentRow = iCONST_NUM_CLUBHOUSE_MISSIONS-1
			ENDIF
			
			IF sClubHouseMissionMenu.iMenuCurrentRow = 0
			#IF IS_DEBUG_BUILD
			AND NOT (IS_KEYBOARD_KEY_PRESSED(KEY_LMENU) OR IS_KEYBOARD_KEY_PRESSED(KEY_RMENU))
			#ENDIF
				INT iFailCondition
				CLUBHOUSE_MISSION_ENUM eMenuSelectedItem = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn])
				IF IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, eMenuSelectedItem, iFailCondition)
					SWITCH sClubHouseMissionMenu.iMenuCurrentColumn
						CASE 0
							//try 1
							IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try up one instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 1
							//try 2
							ELIF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try up two instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 2
							//bail
							ELSE
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU return up column instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentRow = 1
							ENDIF
						BREAK
						CASE 1
							//try 0
							IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try up zero instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 0
							//try 2
							ELIF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try up two instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 2
							//bail
							ELSE
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU return up column instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentRow = 1
							ENDIF
						BREAK
						CASE 2
							//try 1
							IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try up one instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 1
							//try 0
							ELIF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try up zero instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 0
							//bail
							ELSE
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU return up column instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentRow = 1
							ENDIF
						BREAK
						
						DEFAULT
							CASSERTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU unknown up iMenuCurrentColumn:", sClubHouseMissionMenu.iMenuCurrentColumn)
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
			
			REINIT_NET_TIMER(sClubHouseMissionMenu.timerCutsceneTotal)
			//SET_CURRENT_MENU_ITEM(sClubHouseMissionMenu.iMenuCurrentRow)
			PLAY_SOUND_FRONTEND(-1, "Highlight_Move_Up_Down", "DLC_Biker_Mission_Wall_Sounds")
			bUpdateControls = FALSE
			sClubHouseMissionMenu.bFlickedIn = TRUE
	//	ENDIF
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN) //AND ALLOW_ANALOGUE_MOVEMENT(sClubHouseMissionMenu.scrollDelay,sClubHouseMissionMenu.sCHMenuInput.iMoveUp, TRUE)
	OR sClubHouseMissionMenu.sCHMenuInput.bDown AND ALLOW_ANALOGUE_MOVEMENT(sClubHouseMissionMenu.scrollDelay,sClubHouseMissionMenu.sCHMenuInput.iMoveUp, TRUE) AND (fTimeFactor >= fCONST_MoveAllow)
	//	IF sClubHouseMissionMenu.sCHMenuInput.iMoveUp < 0
			sClubHouseMissionMenu.iMenuCurrentRow++
			IF sClubHouseMissionMenu.iMenuCurrentRow >= iCONST_NUM_CLUBHOUSE_MISSIONS
				sClubHouseMissionMenu.iMenuCurrentRow = 0
			ENDIF
			
			IF sClubHouseMissionMenu.iMenuCurrentRow = 0
			#IF IS_DEBUG_BUILD
			AND NOT (IS_KEYBOARD_KEY_PRESSED(KEY_LMENU) OR IS_KEYBOARD_KEY_PRESSED(KEY_RMENU))
			#ENDIF
				INT iFailCondition
				CLUBHOUSE_MISSION_ENUM eMenuSelectedItem = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn])
				IF IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, eMenuSelectedItem, iFailCondition)
					SWITCH sClubHouseMissionMenu.iMenuCurrentColumn
						CASE 0
							//try 1
							IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try down one instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 1
							//try 2
							ELIF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try down two instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 2
							//bail
							ELSE
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU return down column instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentRow = 1
							ENDIF
						BREAK
						CASE 1
							//try 0
							IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try down zero instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 0
							//try 2
							ELIF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try down two instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 2
							//bail
							ELSE
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU return down column instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentRow = 1
							ENDIF
						BREAK
						CASE 2
							//try 1
							IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try down one instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 1
							//try 0
							ELIF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0]), iFailCondition)
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU try down zero instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentColumn = 0
							//bail
							ELSE
								CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU return down column instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
								sClubHouseMissionMenu.iMenuCurrentRow = 1
							ENDIF
						BREAK
						
						DEFAULT
							CASSERTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU unknown down iMenuCurrentColumn:", sClubHouseMissionMenu.iMenuCurrentColumn)
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
			
			REINIT_NET_TIMER(sClubHouseMissionMenu.timerCutsceneTotal)
			//SET_CURRENT_MENU_ITEM(sClubHouseMissionMenu.iMenuCurrentRow)
			PLAY_SOUND_FRONTEND(-1, "Highlight_Move_Up_Down", "DLC_Biker_Mission_Wall_Sounds")
			bUpdateControls = FALSE
			sClubHouseMissionMenu.bFlickedIn = TRUE
	//	ENDIF
	ENDIF
	
	
	INT iScreenX
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		bUpdateControls = FALSE
	ENDIF
	IF NOT bUpdateControls
		IF LOAD_MENU_ASSETS()
		AND bKeyPressSafe
			REMOVE_MENU_HELP_KEYS()
			CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_MAIN_MENU bUpdateControls")
			
			ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_SCRIPT_LSTICK_ALL, "IB_NAVIGATE")
			
			IF sClubHouseMissionMenu.iMenuCurrentRow = 0
			AND sClubHouseMissionMenu.bFlickedIn
				CLUBHOUSE_MISSION_ENUM eMenuSelectedItem = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn])
				INT iFailCondition
				IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_LAUNCH(sClubHouseMissionMenu.pOwnerID, eMenuSelectedItem, iFailCondition)
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
				ENDIF
			ENDIF
			
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
			ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_SCRIPT_RSTICK_ALL, "PB_H_LOOK")
			ADD_MENU_HELP_KEY_CLICKABLE(GET_CH_MISSION_ZOOM_INPUT(), "ITEM_ZOOM")
			
			SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
			DRAW_MENU_HELP_SCALEFORM(iScreenX)
			bUpdateControls = TRUE
		ENDIF
	ELSE
		IF bKeyPressSafe
			SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
			DRAW_MENU_HELP_SCALEFORM(iScreenX)
		ENDIF
	ENDIF
	
ENDPROC

FUNC STRING GET_CH_MISSION_MENU_DESCRIPTION(PLAYER_INDEX pOwnerID, CLUBHOUSE_MISSION_ENUM eCHMission)
	INT iFailCondition
	IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_LAUNCH(pOwnerID, eCHMission, iFailCondition)
		//
	ELSE
		SWITCH iFailCondition
			CASE 1	//GB_IS_PLAYER_CRITICAL_TO_JOB
				CPRINTLN(DEBUG_AMBIENT, "GET_CH_MISSION_MENU_DESCRIPTION \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" not available - player critical to job")
				RETURN ("CHMMD_CRIT")
			BREAK
			CASE 0	//GB_HAS_BOSSWORK_COOLDOWN_TIMER_EXPIRED
				CPRINTLN(DEBUG_AMBIENT, "GET_CH_MISSION_MENU_DESCRIPTION \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" not available - cooldown timer not expired")
				RETURN ("CHMMD_COOLT")
			BREAK
			CASE 2	//GB_IS_BOSS_MISSION_AVAILABLE
				INT iType, iReason
				iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(eCHMission)
				iReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(pOwnerID, iType)
				
				CPRINTLN(DEBUG_AMBIENT, "GET_CH_MISSION_MENU_DESCRIPTION \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" not available - GB mission ", eCHMission, " not available, reason: ", iReason)
				
				TEXT_LABEL str
				SWITCH iReason
					CASE GB_MU_REASON_ENTITIES_RESERVED			RETURN "CHMMD_UNAV0" BREAK		//Mission not available because entities reserved.
					CASE GB_MU_REASON_ONLY_GANG_PLAYERS			RETURN "WH_MFREASON_0" BREAK	//Too many Special Cargo related activities ongoing.
					CASE GB_MU_REASON_SESSION_MISSION_ACTIVE	RETURN "CHMMD_UNAV2" BREAK		//Another mission is already active.
					CASE GB_MU_REASON_NO_OTHER_BOSSES			RETURN "WH_MFREASON_2" BREAK	//No other CEO's in this session.
					CASE GB_MU_REASON_NOT_ENOUGH_GOONS			RETURN "CHMMD_UNAV4" BREAK		//You do not have enough Members in your MC to start this mission.
					CASE GB_MU_REASON_TOO_MANY_INSTANCES		RETURN "WH_MFREASON_4" BREAK	//Too many Special Cargo related activities ongoing.
					CASE GB_MU_REASON_BOSS_CRITICAL_TO_FME		RETURN "WH_MFREASON_5" BREAK	//Cannot do this whilst you are active in a Freemode Event.
					CASE GB_MU_REASON_GOON_CRITICAL_TO_FME		RETURN "CHMMD_UNAV7" BREAK		//One of the Members of your MC is active in a Freemode Event.
					CASE GB_MU_REASON_GOON_FM_EVENT_ACTIVE		RETURN "CHMMD_UNAV7" BREAK		//One of the Members of your MC is active in a Freemode Event.
					CASE GB_MU_REASON_INVITE_SENT				RETURN "WH_MFREASON_8" BREAK	//Awaiting the response to your previous activity invites.
					CASE GB_MU_REASON_BOTH_SENT_BVB_INVITE		RETURN "WH_MFREASON_8" BREAK	//Awaiting the response to your previous activity invites.
					CASE GB_MU_REASON_ON_COOLDOWN				RETURN "WH_MFREASON_9" BREAK	//Please wait until a buyer can be found for your items.
					CASE GB_MU_REASON_UNSUITABLE_SESSION		RETURN "WH_MFREASON_12" BREAK	//Warehouse activities are currently unavailable.
					CASE GB_MU_REASON_CONTRABAND_DISABLED		RETURN "CHMMD_UNAV13" BREAK		//Mission not available because contraband disabled.
					CASE GB_MU_REASON_BOSS_IS_ANIMAL			RETURN "CHMMD_UNAV14" BREAK		//Contract not available while you are an animal.
					CASE GB_MU_REASON_GOON_IS_ANIMAL			RETURN "CHMMD_UNAV15" BREAK		//Clubhouse Contract unavailable. One of your Members is an animal.
					CASE GB_MU_REASON_GOON_GAMBLING				RETURN "CHMMD_UNAV16" BREAK		//Clubhouse Contract unavailable. One of your Members is currently gambling.
					
					DEFAULT
						str  = "CHMMD_UNAV"
						str += iReason
					BREAK
				ENDSWITCH
				RETURN GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(str, 0, GET_LENGTH_OF_LITERAL_STRING(str))
			BREAK
			CASE 3	//GB_IS_LOCAL_PLAYER_SAFE_TO_LAUNCH_GANG_BOSS_MISSION
				CPRINTLN(DEBUG_AMBIENT, "GET_CH_MISSION_MENU_DESCRIPTION \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" not available - not safe to lauunch GB mission")
				RETURN ("CHMMD_UNSAF")
			BREAK
			CASE 4	//GB_BIKER_IS_ANY_PLAYER_NEAR_CLUBHOUSE_MISSION_CRITICAL_LOCATION
				CPRINTLN(DEBUG_AMBIENT, "GET_CH_MISSION_MENU_DESCRIPTION \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" not available - has player near critical location")
				RETURN ("CHMMD_CRLOC")
			BREAK
			CASE 5	//MGID_CLUBHOUSE_GET() = eMGID_ARMWRESTLE
				CPRINTLN(DEBUG_AMBIENT, "GET_CH_MISSION_MENU_DESCRIPTION \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" not available - has player near critical location")
				RETURN ("CHMMD_ARMW")
			BREAK
			CASE 6	//IS_CH_MISSION_BLOCKED_BY_TUNEABLES(eCHMission)
				CPRINTLN(DEBUG_AMBIENT, "GET_CH_MISSION_MENU_DESCRIPTION \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" not available - has player near critical location")
				RETURN ("CHMMD_CRLOC")
			BREAK
			
			DEFAULT
				CASSERTLN(DEBUG_AMBIENT, "GET_CH_MISSION_MENU_DESCRIPTION \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" not available - UNKNOWN new condition ", iFailCondition, "???")
				RETURN ("CHMMD_GENFA")
			BREAK
		ENDSWITCH
	ENDIF
	RETURN ""
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DRAW_DEBUG_POINT_CAM(STRING text, structPointCam sPointCam, FLOAT radius, HUD_COLOURS eHudColour, FLOAT alpha_mult)
	INT red, green, blue, alpha_param
	GET_HUD_COLOUR(eHudColour, red, green, blue, alpha_param)
	
	DRAW_DEBUG_SPHERE(sPointCam.vCoord, radius, red, green, blue, ROUND(TO_FLOAT(alpha_param)*alpha_mult))
	DRAW_DEBUG_SPHERE(sPointCam.vPoint, radius, red, green, blue, ROUND(TO_FLOAT(alpha_param)*alpha_mult))
	DRAW_DEBUG_LINE(sPointCam.vCoord, sPointCam.vPoint, red, green, blue)
	
	DRAW_DEBUG_TEXT_WITH_OFFSET(text, sPointCam.vPoint,
			0,00,
			255-red, 255-green, 255-blue)
ENDPROC

OBJECT_INDEX tempCamObj, tempWallObj
PROC DRAW_DEBUG_CARMOD_CAM_TEXT(TEXT_LABEL_63 str, HUD_COLOURS eHudColour, INT &iStrOffset, INT &iColumn)
	INT iRed, iGreen, iBlue, iAlpha
	GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
	
	DRAW_DEBUG_TEXT_2D(str, <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, iAlpha)	iStrOffset++
	
	CONST_FLOAT  fTextWrap			0.025
	
	SET_TEXT_JUSTIFICATION(FONT_RIGHT)
	SET_TEXT_RIGHT_JUSTIFY(TRUE)
	
	SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255)
	SET_TEXT_EDGE(1, 0, 0, 0, 255)
	
	CONST_INT iMAX_RGB	255
	CONST_FLOAT fRATIO_RGB	0.5
	SET_TEXT_COLOUR(ROUND((TO_FLOAT(iRed)*fRATIO_RGB)+TO_FLOAT(iMAX_RGB)*(1.0-fRATIO_RGB)),
			ROUND((TO_FLOAT(iGreen)*fRATIO_RGB)+TO_FLOAT(iMAX_RGB)*(1.0-fRATIO_RGB)),
			ROUND((TO_FLOAT(iBlue)*fRATIO_RGB)+TO_FLOAT(iMAX_RGB)*(1.0-fRATIO_RGB)),
			iAlpha)
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_WRAP(fTextWrap, (1.0-fTextWrap))
	
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.5+(0.0*TO_FLOAT(iColumn)), 0.7+(0.03*TO_FLOAT(iStrOffset)), "STRING", str)
ENDPROC
PROC DEBUG_CAM_POINT(VECTOR vWallCoord,VECTOR vWallRot)
	INT iStrOffset = 0, iColumn = 0
	TEXT_LABEL_63 str = ""
	
	IF IS_CAM_RENDERING(GET_DEBUG_CAM())
		VECTOR vCamCoord = GET_CAM_COORD(GET_DEBUG_CAM())
		VECTOR vCamRot = GET_CAM_ROT(GET_DEBUG_CAM())
		FLOAT fWallCamDist = VDIST(vWallCoord, vCamCoord)
		
		IF NOT DOES_ENTITY_EXIST(tempCamObj)
		OR NOT DOES_ENTITY_EXIST(tempWallObj)
		
			MODEL_NAMES tempCamModel = PROP_GOLF_BALL
			REQUEST_MODEL(tempCamModel)
			IF HAS_MODEL_LOADED(tempCamModel)
				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 2)
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,2,0)
			
					tempCamObj = CREATE_OBJECT(tempCamModel, vCamCoord)
					SET_ENTITY_ROTATION(tempCamObj, vCamRot)
					SET_ENTITY_VISIBLE(tempCamObj, FALSE)
					
					tempWallObj = CREATE_OBJECT(tempCamModel, vWallCoord)
					SET_ENTITY_ROTATION(tempWallObj, vWallRot)
					SET_ENTITY_VISIBLE(tempWallObj, FALSE)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(tempCamModel)
				ENDIF
			ENDIF
			
		ELSE
			SET_ENTITY_COORDS(tempCamObj, vCamCoord)	SET_ENTITY_ROTATION(tempCamObj, vCamRot)
			SET_ENTITY_COORDS(tempWallObj, vWallCoord)	SET_ENTITY_ROTATION(tempWallObj, vWallRot)
			
			VECTOR vCoordOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempCamObj, <<0,fWallCamDist*4.0,0>>)
			VECTOR vClosestCoord = GET_CLOSEST_POINT_ON_LINE(vWallCoord, vCoordOffset, vCamCoord)
			DRAW_DEBUG_SPHERE(vCoordOffset, 0.2,	000, 000, 000, 128)
			DRAW_DEBUG_SPHERE(vClosestCoord, 0.2,	255, 255, 255, 128)
			
			VECTOR vClosestPointOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(tempWallObj, vClosestCoord)
			VECTOR vClosestCoordOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(tempWallObj, vCamCoord)
			
			
			str  = "vClosestPointOffset:"
			str += GET_STRING_FROM_FLOAT(vClosestPointOffset.x)
			str += ", "
			str += GET_STRING_FROM_FLOAT(vClosestPointOffset.y)
			str += ", "
			str += GET_STRING_FROM_FLOAT(vClosestPointOffset.z)
			DRAW_DEBUG_CARMOD_CAM_TEXT(str, HUD_COLOUR_PURE_WHITE, iStrOffset, iColumn)
			
			str  = "vClosestCoordOffset:"
			str += GET_STRING_FROM_FLOAT(vClosestCoordOffset.x)
			str += ", "
			str += GET_STRING_FROM_FLOAT(vClosestCoordOffset.y)
			str += ", "
			str += GET_STRING_FROM_FLOAT(vClosestCoordOffset.z)
			DRAW_DEBUG_CARMOD_CAM_TEXT(str, HUD_COLOUR_PURE_WHITE, iStrOffset, iColumn)
			
			IF IS_KEYBOARD_KEY_JUST_RELEASED(KEY_C)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("VECTOR vClosestPointOffset	= ")
				SAVE_VECTOR_TO_DEBUG_FILE(vClosestPointOffset)
				SAVE_STRING_TO_DEBUG_FILE(", vClosestCoordOffset = ")
				SAVE_VECTOR_TO_DEBUG_FILE(vClosestCoordOffset)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDIF
	ELSE
		IF NOT DOES_ENTITY_EXIST(tempCamObj)
			DELETE_OBJECT(tempCamObj)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(tempWallObj)
			DELETE_OBJECT(tempWallObj)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

CONST_FLOAT CONST_BROWSE_CAM_SHAKE	0.2

CONST_FLOAT F_CAM_FOV				42.5
CONST_FLOAT F_CAM_ZOOMED_FOV		32.5

PROC UPDATE_CH_MISSION_CAMERA(CLUBHOUSE_MISSION_MENU_STRUCT &sClubHouseMissionMenu,INT iCurrentProperty)
	// Ignore keypress when it's not safe.
	BOOL bKeyPressSafe = TRUE
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR IS_WARNING_MESSAGE_ACTIVE()
	OR NETWORK_TEXT_CHAT_IS_TYPING()
	OR g_sShopSettings.bProcessStoreAlert
		bKeyPressSafe = FALSE
	ENDIF
	
	structPointCam sSelectedMissionOffset
	sSelectedMissionOffset.vCoord = <<0,0,0>>
	sSelectedMissionOffset.vPoint = <<0,0,0>>
	
	VECTOR vPos = <<0,0,0>>, vRot = <<0,0,0>>
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
	//	bkr_prop_rt_clubhouse_plan_01a = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_rt_clubhouse_plan_01a"))
		vPos = <<1110.660,-3145.341,-36.465>>			vRot = <<0,0,90>>
	ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
	//	bkr_prop_rt_clubhouse_plan_01a = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_scriptrt_wall"))
		vPos = <<1011.592,-3150.676,-38.311>>			vRot = <<0,0,-90>>
	ELSE
	//	bkr_prop_rt_clubhouse_plan_01a = DUMMY_MODEL_FOR_SCRIPT
		vPos = <<0,0,0>>								vRot = <<0,0,0>>
		CASSERTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA unknown peoperty ", iCurrentProperty)
		EXIT
	ENDIF
	
	structPointCam sStartPointCam
	VECTOR vStartPointOffset = <<0.2629, -0.0099, -0.2105>>, vStartCoordOffset = <<0.2500, -2.6023, -0.1042>>	//VECTOR vStartPointOffset	= ((vMission1PointOffset+vMission2PointOffset+vMission3PointOffset)/3.0)+<<0,0,-0.25>>	VECTOR vStartCoordOffset	= vStartPointOffset+<<0,-2,0>>
	sStartPointCam.vCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vStartCoordOffset)
	sStartPointCam.vPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vStartPointOffset)
	
	structPointCam sMissionPointCam[iCONST_NUM_CLUBHOUSE_MISSIONS]
	VECTOR vMission1PointOffset	= <<0.0693, -0.0043, 0.0319>>, vMission1CoordOffset = <<-0.0061, -1.6710, -0.0272>>	//VECTOR vMission1PointOffset	= << 0.25,-0.05, 0.25>>, vMission1CoordOffset		= vMission1PointOffset+<<0,-2,0>>
	sMissionPointCam[0].vCoord	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vMission1CoordOffset)
	sMissionPointCam[0].vPoint	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vMission1PointOffset)
	VECTOR vMission2PointOffset	= <<0.7551, -0.0013, 0.0449>>, vMission2CoordOffset = <<0.7201, -1.63615, -0.0272>>	//VECTOR vMission2PointOffset	= << 0.75,-0.05, 0.25>>, vMission2CoordOffset		= vMission2PointOffset+<<0,-2,0>>
	sMissionPointCam[1].vCoord	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vMission2CoordOffset)
	sMissionPointCam[1].vPoint	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vMission2PointOffset)
	VECTOR vMission3PointOffset	= <<1.4409, 0.0017, 0.0579>>, vMission3CoordOffset = <<1.4463, -1.6013, -0.0272>>	//VECTOR vMission3PointOffset	= << 1.50,-0.05, 0.25>>, vMission3CoordOffset		= vMission3PointOffset+<<0,-2,0>>
	sMissionPointCam[2].vCoord	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vMission3CoordOffset)
	sMissionPointCam[2].vPoint	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vMission3PointOffset)
	
	structPointCam sStat1PointCam, sStat2PointCam, sStat3PointCam
	VECTOR vStat1PointOffset	= <<-0.4341, -0.0455, -0.6044>>, vStat1CoordOffset = <<-0.4592, -1.5156, -0.4756>>
	sStat1PointCam.vCoord		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vStat1CoordOffset)
	sStat1PointCam.vPoint		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vStat1PointOffset)
	VECTOR vStat2PointOffset	= <<0.2158, -0.0492, -0.6038>>, vStat2CoordOffset = <<0.2161, -1.5156, -0.4756>>
	sStat2PointCam.vCoord		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vStat2CoordOffset)
	sStat2PointCam.vPoint		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vStat2PointOffset)
	VECTOR vStat3PointOffset	= <<1.0171, -0.0529, -0.6208>>, vStat3CoordOffset = <<1.2954, -1.5156, -0.4756>>
	sStat3PointCam.vCoord		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vStat3CoordOffset)
	sStat3PointCam.vPoint		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, vStat3PointOffset)
	
	#IF IS_DEBUG_BUILD
	IF sClubHouseMissionMenu.bDebugDraw
		IF NOT IS_CAM_RENDERING(GET_DEBUG_CAM())
			TEXT_LABEL_23 tlMissionName
			IF GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0] != ENUM_TO_INT(CLUBHOUSE_MISSION_invalid)
				tlMissionName = GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0])))
			ELSE
				tlMissionName = "invalid 1"
			ENDIF
			DRAW_DEBUG_POINT_CAM(tlMissionName, sMissionPointCam[0], 0.05, HUD_COLOUR_BLUE, 0.5)
			IF GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1] != ENUM_TO_INT(CLUBHOUSE_MISSION_invalid)
				tlMissionName = GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1])))
			ELSE
				tlMissionName = "invalid 2"
			ENDIF
			DRAW_DEBUG_POINT_CAM(tlMissionName, sMissionPointCam[1], 0.05, HUD_COLOUR_GREEN, 0.5)
			IF GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2] != ENUM_TO_INT(CLUBHOUSE_MISSION_invalid)
				tlMissionName = GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2])))
			ELSE
				tlMissionName = "invalid 3"
			ENDIF
			DRAW_DEBUG_POINT_CAM(tlMissionName, sMissionPointCam[2], 0.05, HUD_COLOUR_RED, 0.5)
			
			DRAW_DEBUG_POINT_CAM("stat 1", sStat1PointCam, 0.05, HUD_COLOUR_BLUE, 0.5)
			DRAW_DEBUG_POINT_CAM("stat 2", sStat2PointCam, 0.05, HUD_COLOUR_GREEN, 0.5)
			DRAW_DEBUG_POINT_CAM("stat 3", sStat3PointCam, 0.05, HUD_COLOUR_RED, 0.5)
			
			DRAW_DEBUG_POINT_CAM("lool at", sStartPointCam, 0.05, HUD_COLOUR_BLACK, 1.0)
		ENDIF
		
		DEBUG_CAM_POINT(vPos, vRot)
	ENDIF
	
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_BikerFlowPlayVariations")
		SET_TEXT_COLOUR(1, 255, 255, 255)
		SET_TEXT_SCALE(0.4, 0.4)
		SET_TEXT_WRAP(0.0, 0.25)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.05, "STRING",
					"DEBUG: Alt+Up, Alt+Down to change mission in slot, Alt+S to fake pass")
		ELSE
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.15, "STRING",
					"DEBUG: Alt+Up, Alt+Down to change mission in slot, Alt+S to fake pass")
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_UP, KEYBOARD_MODIFIER_ALT, "UPDATE_CH_MISSION_CAMERA - mission up")
			INT iCurrentCHMission = ENUM_TO_INT(INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn]))
			iCurrentCHMission--
			IF iCurrentCHMission < 0
				iCurrentCHMission = ENUM_TO_INT(CLUBHOUSE_MISSION_MAX_MENU_ITEM)-1
			ENDIF
			GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] = iCurrentCHMission
			GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] = iCurrentCHMission
			sClubHouseMissionMenu.eCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, iCurrentCHMission)
			
			sClubHouseMissionMenu.bDebugUpdateMenu = TRUE
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_DOWN, KEYBOARD_MODIFIER_ALT, " UPDATE_CH_MISSION_CAMERA - mission down")
			INT iCurrentCHMission = ENUM_TO_INT(INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn]))
			iCurrentCHMission++
			IF iCurrentCHMission >= ENUM_TO_INT(CLUBHOUSE_MISSION_MAX_MENU_ITEM)
				iCurrentCHMission = 0
			ENDIF
			GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] = iCurrentCHMission
			GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] = iCurrentCHMission
			sClubHouseMissionMenu.eCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, iCurrentCHMission)
			
			sClubHouseMissionMenu.bDebugUpdateMenu = TRUE
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_S, KEYBOARD_MODIFIER_ALT, " UPDATE_CH_MISSION_CAMERA - fake purchase")
			
			
//			INT i
//			REPEAT NUM_NETWORK_PLAYERS i
//				IF GlobalPlayerBD_FM[i].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] != -1
//					SET_BIT(GlobalplayerBD_FM[i].ClubhouseMissionData.iCHMissionInvalidBS, sClubHouseMissionMenu.iMenuCurrentColumn)
//					GlobalPlayerBD_FM[i].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] = -1
//				ENDIF
//			ENDREPEAT
			
			GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn] = -1
			BROADCAST_CLUBHOUSE_MISSION_MISSION_LAUNCHED(sClubHouseMissionMenu.iMenuCurrentColumn)
		ENDIF
	ENDIF
	#ENDIF
	
	// // // // // // // // // // // // // // // // // // // // //
	VECTOR vStartCurrentPointOffset = sStartPointCam.vPoint			//<<1110.9622, -3145.1960, -36.0628>>
	VECTOR vStartCurrentPointCoord = sStartPointCam.vCoord			//<<1113.0636, -3145.3718, -36.1785>>
	// // // // // // // // // // // // // // // // // // // // //
	
	IF sClubHouseMissionMenu.bFlickedIn
		IF sClubHouseMissionMenu.iMenuCurrentRow = 0
			SWITCH sClubHouseMissionMenu.iMenuCurrentColumn
				CASE 0	sSelectedMissionOffset.vCoord = sMissionPointCam[0].vCoord	sSelectedMissionOffset.vPoint = sMissionPointCam[0].vPoint BREAK
				CASE 1	sSelectedMissionOffset.vCoord = sMissionPointCam[1].vCoord	sSelectedMissionOffset.vPoint = sMissionPointCam[1].vPoint BREAK
				CASE 2	sSelectedMissionOffset.vCoord = sMissionPointCam[2].vCoord	sSelectedMissionOffset.vPoint = sMissionPointCam[2].vPoint BREAK
			ENDSWITCH
		ELSE
			SWITCH sClubHouseMissionMenu.iMenuCurrentColumn
				CASE 0	sSelectedMissionOffset.vCoord = sStat1PointCam.vCoord	sSelectedMissionOffset.vPoint = sStat1PointCam.vPoint BREAK
				CASE 1	sSelectedMissionOffset.vCoord = sStat2PointCam.vCoord	sSelectedMissionOffset.vPoint = sStat2PointCam.vPoint BREAK
				CASE 2	sSelectedMissionOffset.vCoord = sStat3PointCam.vCoord	sSelectedMissionOffset.vPoint = sStat3PointCam.vPoint BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	IF NOT DOES_CAM_EXIST(sClubHouseMissionMenu.ciCam)
		// PC uses a different frontend input for zooming		
		sClubHouseMissionMenu.sCurrentOffset.vCoord = vStartCurrentPointCoord
		sClubHouseMissionMenu.sCurrentOffset.vPoint = vStartCurrentPointOffset
		START_NET_TIMER(sClubHouseMissionMenu.timerCutsceneTotal)
		
		sClubHouseMissionMenu.ciCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
		SET_CAM_COORD(sClubHouseMissionMenu.ciCam, sClubHouseMissionMenu.sCurrentOffset.vCoord)
		POINT_CAM_AT_COORD(sClubHouseMissionMenu.ciCam, sClubHouseMissionMenu.sCurrentOffset.vPoint)
		SET_CAM_FOV(sClubHouseMissionMenu.ciCam, COSINE_INTERP_FLOAT(F_CAM_FOV,F_CAM_ZOOMED_FOV,sClubHouseMissionMenu.fCamZoomAlpha))
		SET_CAM_ACTIVE(sClubHouseMissionMenu.ciCam, TRUE)
		
		SHAKE_CAM(sClubHouseMissionMenu.ciCam,"HAND_SHAKE",CONST_BROWSE_CAM_SHAKE)
		
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		g_bClubhouseMissionWallActive = TRUE
		GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.bClubhouseMissionWallActive = TRUE
		GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.bClubhouseMissionWallActive = TRUE
		
		sClubHouseMissionMenu.bFlickedIn = FALSE
		
		CDEBUG1LN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA: create mission cam")
	
	
		IF sClubHouseMissionMenu.iMenuCurrentRow = 0
			INT iFailCondition
			CLUBHOUSE_MISSION_ENUM eMenuSelectedItem = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn])
			IF IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, eMenuSelectedItem, iFailCondition)
				SWITCH sClubHouseMissionMenu.iMenuCurrentColumn
					CASE 0
						//try 1
						IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1]), iFailCondition)
							CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA try up one instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
							sClubHouseMissionMenu.iMenuCurrentColumn = 1
						//try 2
						ELIF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2]), iFailCondition)
							CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA try up two instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
							sClubHouseMissionMenu.iMenuCurrentColumn = 2
						//bail
						ELSE
							CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA return up column instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
							sClubHouseMissionMenu.iMenuCurrentRow = 1
						ENDIF
					BREAK
					CASE 1
						//try 0
						IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0]), iFailCondition)
							CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA try up zero instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
							sClubHouseMissionMenu.iMenuCurrentColumn = 0
						//try 2
						ELIF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2]), iFailCondition)
							CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA try up two instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
							sClubHouseMissionMenu.iMenuCurrentColumn = 2
						//bail
						ELSE
							CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA return up column instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
							sClubHouseMissionMenu.iMenuCurrentRow = 1
						ENDIF
					BREAK
					CASE 2
						//try 1
						IF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1]), iFailCondition)
							CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA try up one instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
							sClubHouseMissionMenu.iMenuCurrentColumn = 1
						//try 0
						ELIF !IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_NAV(sClubHouseMissionMenu.pOwnerID, INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0]), iFailCondition)
							CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA try up zero instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
							sClubHouseMissionMenu.iMenuCurrentColumn = 0
						//bail
						ELSE
							CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA return up column instead of ", sClubHouseMissionMenu.iMenuCurrentColumn)
							sClubHouseMissionMenu.iMenuCurrentRow = 1
						ENDIF
					BREAK
					
					DEFAULT
						CASSERTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA unknown up iMenuCurrentColumn:", sClubHouseMissionMenu.iMenuCurrentColumn)
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	IF ARE_VECTORS_EQUAL(sSelectedMissionOffset.vPoint, <<0,0,0>>)

		VECTOR vBase = vPos
		VECTOR vNudge = vBase
		IF bKeyPressSafe
			vNudge = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, sClubHouseMissionMenu.sCHMenuInput.vCamNudge*0.75)
		ENDIF
		
		VECTOR vCurrentNudgedPointOffset = sClubHouseMissionMenu.sCurrentOffset.vPoint+(vNudge-vBase)
		
		CONST_INT iDelay		0100
		CONST_INT iTotalTime	1000
		
		SET_CAM_COORD(sClubHouseMissionMenu.ciCam, sClubHouseMissionMenu.sCurrentOffset.vCoord)
		POINT_CAM_AT_COORD(sClubHouseMissionMenu.ciCam, vCurrentNudgedPointOffset)
		
		#IF IS_DEBUG_BUILD
		IF sClubHouseMissionMenu.bDebugDraw
			IF NOT IS_CAM_RENDERING(GET_DEBUG_CAM())
				DRAW_DEBUG_SPHERE(vCurrentNudgedPointOffset,					0.1,
								128,	128,	000, 128)
				DRAW_DEBUG_TEXT_WITH_OFFSET("nudge", vCurrentNudgedPointOffset,	0,00,
								128,	128, 	255)
				
				TEXT_LABEL_31 str31 = "iRight "
				str31 += sClubHouseMissionMenu.sCHMenuInput.iRightX
				str31 += ", "
				str31 += sClubHouseMissionMenu.sCHMenuInput.iRightY
				DRAW_DEBUG_TEXT_WITH_OFFSET(str31, vCurrentNudgedPointOffset,	0,10,
								128,	128, 	255)
				
				INT i
				REPEAT COUNT_OF(sClubHouseMissionMenu.sCHMenuInput.iPrevRightY) i
					str31  = "iRight["
					str31 += i
					str31 += "] "
					str31 += sClubHouseMissionMenu.sCHMenuInput.iPrevRightX[i]
					str31 += ", "
					str31 += sClubHouseMissionMenu.sCHMenuInput.iPrevRightY[i]
					DRAW_DEBUG_TEXT_WITH_OFFSET(str31, vCurrentNudgedPointOffset,	0,(2+i)*10,
									128,	128, 	255)
				ENDREPEAT
			ENDIF
		ENDIF
		#ENDIF
		
//		IF ARE_VECTORS_ALMOST_EQUAL(sSelectedMissionOffset____vPoint, sClubHouseMissionMenu.sCurrentOffset.vPoint, 0.01)
//			sClubHouseMissionMenu.sStoredOffset.vCoord = sClubHouseMissionMenu.sCurrentOffset.vCoord
//			sClubHouseMissionMenu.sStoredOffset.vPoint = sClubHouseMissionMenu.sCurrentOffset.vPoint
//			RESET_NET_TIMER(sClubHouseMissionMenu.timerCutsceneTotal)
//		ENDIF
	ELSE

		VECTOR vBase = vPos
		VECTOR vNudge = vBase
		IF bKeyPressSafe
			vNudge = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, vRot.z, sClubHouseMissionMenu.sCHMenuInput.vCamNudge)
		ENDIF
		
		VECTOR vCurrentNudgedPointOffset = sClubHouseMissionMenu.sCurrentOffset.vPoint+(vNudge-vBase)
		
		CONST_INT iDelay		0100
		CONST_INT iTotalTime	1000
		
		SET_CAM_COORD(sClubHouseMissionMenu.ciCam, sClubHouseMissionMenu.sCurrentOffset.vCoord)
		POINT_CAM_AT_COORD(sClubHouseMissionMenu.ciCam, vCurrentNudgedPointOffset)
		
		FLOAT fTimeFactor = TO_FLOAT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GET_TIME_OFFSET(sClubHouseMissionMenu.timerCutsceneTotal.Timer, iDelay))) / TO_FLOAT(iTotalTime - iDelay)
		IF fTimeFactor < 0.0
			sClubHouseMissionMenu.sStoredOffset.vCoord = sClubHouseMissionMenu.sCurrentOffset.vCoord
			sClubHouseMissionMenu.sStoredOffset.vPoint = sClubHouseMissionMenu.sCurrentOffset.vPoint
			fTimeFactor = 0.0
		ENDIF
		IF fTimeFactor > 1.0
			fTimeFactor = 1.0
		ENDIF
		
		IF IS_BIT_SET(sClubHouseMissionMenu.iUpdatingMissionSelectionBitset, ENUM_TO_INT(eCHMBITSET_MISSION_CHANGED_AWAIT_CAM))
			IF fTimeFactor >= 0.5
				CPRINTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_CAMERA bUpdatingMissionSelection is true, have we updated???")
				SET_BIT(sClubHouseMissionMenu.iUpdatingMissionSelectionBitset, ENUM_TO_INT(eCHMBITSET_CAM_CHANGED_UPDATE_MOVIE))
			ENDIF
		ENDIF
		
		VECTOR vCoordDiff = (sSelectedMissionOffset.vCoord-sClubHouseMissionMenu.sStoredOffset.vCoord)
		sClubHouseMissionMenu.sCurrentOffset.vCoord = sClubHouseMissionMenu.sStoredOffset.vCoord + (vCoordDiff*GET_GRAPH_TYPE_SIN_ACCEL_DECEL(fTimeFactor))
		
		VECTOR vPointDiff = (sSelectedMissionOffset.vPoint-sClubHouseMissionMenu.sStoredOffset.vPoint)
		sClubHouseMissionMenu.sCurrentOffset.vPoint = sClubHouseMissionMenu.sStoredOffset.vPoint + (vPointDiff*GET_GRAPH_TYPE_SIN_ACCEL_DECEL(fTimeFactor))
		
		#IF IS_DEBUG_BUILD
		IF sClubHouseMissionMenu.bDebugDraw
			INT iStrOffset = 9
			INT iColumn = 0
			
			IF NOT IS_CAM_RENDERING(GET_DEBUG_CAM())
				DRAW_DEBUG_SPHERE(vCurrentNudgedPointOffset,					0.1,
								128,	128,	000, 128)
				DRAW_DEBUG_TEXT_WITH_OFFSET("nudge", vCurrentNudgedPointOffset,	0,00,
								128,	128, 	255)
				
				TEXT_LABEL_31 str31 = "iRight "
				str31 += sClubHouseMissionMenu.sCHMenuInput.iRightX
				str31 += ", "
				str31 += sClubHouseMissionMenu.sCHMenuInput.iRightY
				DRAW_DEBUG_TEXT_WITH_OFFSET(str31, vCurrentNudgedPointOffset,	0,10,
								128,	128, 	255)
				
				INT i
				REPEAT COUNT_OF(sClubHouseMissionMenu.sCHMenuInput.iPrevRightY) i
					str31  = "iRight["
					str31 += i
					str31 += "] "
					str31 += sClubHouseMissionMenu.sCHMenuInput.iPrevRightX[i]
					str31 += ", "
					str31 += sClubHouseMissionMenu.sCHMenuInput.iPrevRightY[i]
					DRAW_DEBUG_TEXT_WITH_OFFSET(str31, vCurrentNudgedPointOffset,	0,(2+i)*10,
									128,	128, 	255)
				ENDREPEAT
				
				DRAW_DEBUG_POINT_CAM("current", sClubHouseMissionMenu.sCurrentOffset, 0.1, HUD_COLOUR_PURPLE, 0.5)
				DRAW_DEBUG_TEXT_WITH_OFFSET(GET_STRING_FROM_FLOAT(fTimeFactor), sClubHouseMissionMenu.sCurrentOffset.vPoint,	0,10,
								128,	128, 	255)
				TEXT_LABEL_63 str = "fTimeFactor: "
				str += GET_STRING_FROM_FLOAT(fTimeFactor)
				DRAW_DEBUG_TEXT_2D(str, <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)),
								128,	128, 	255) iStrOffset++
				
				DRAW_DEBUG_POINT_CAM("stored", sClubHouseMissionMenu.sStoredOffset, 0.1, HUD_COLOUR_YELLOWDARK, 0.5)
				DRAW_DEBUG_POINT_CAM("selected", sSelectedMissionOffset, 0.1, HUD_COLOUR_BLUEDARK, 0.5)
			ENDIF
		ENDIF
		#ENDIF
		
		IF ARE_VECTORS_ALMOST_EQUAL(sSelectedMissionOffset.vPoint, sClubHouseMissionMenu.sCurrentOffset.vPoint, 0.01)
			sClubHouseMissionMenu.sStoredOffset.vCoord = sClubHouseMissionMenu.sCurrentOffset.vCoord
			sClubHouseMissionMenu.sStoredOffset.vPoint = sClubHouseMissionMenu.sCurrentOffset.vPoint
			RESET_NET_TIMER(sClubHouseMissionMenu.timerCutsceneTotal)
		ENDIF
	ENDIF
	
	// Zoom
	IF bKeyPressSafe
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,GET_CH_MISSION_ZOOM_INPUT())
				sClubHouseMissionMenu.bZoomToggle = NOT sClubHouseMissionMenu.bZoomToggle
			ENDIF
		ELSE
			sClubHouseMissionMenu.bZoomToggle = FALSE
		ENDIF
		IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,GET_CH_MISSION_ZOOM_INPUT()) OR sClubHouseMissionMenu.bZoomToggle)
			IF sClubHouseMissionMenu.fCamZoomAlpha < 1
				SET_CAM_SHAKE_AMPLITUDE(sClubHouseMissionMenu.ciCam, (1-sClubHouseMissionMenu.fCamZoomAlpha)*CONST_BROWSE_CAM_SHAKE)
				sClubHouseMissionMenu.fCamZoomAlpha = sClubHouseMissionMenu.fCamZoomAlpha + 0.05
			ENDIF
			IF sClubHouseMissionMenu.fCamZoomAlpha > 1
				sClubHouseMissionMenu.fCamZoomAlpha = 1
			ENDIF
		ELSE
			IF sClubHouseMissionMenu.fCamZoomAlpha > 0
				SET_CAM_SHAKE_AMPLITUDE(sClubHouseMissionMenu.ciCam, (1-sClubHouseMissionMenu.fCamZoomAlpha)*CONST_BROWSE_CAM_SHAKE)
				sClubHouseMissionMenu.fCamZoomAlpha = sClubHouseMissionMenu.fCamZoomAlpha - 0.05
			ENDIF
			IF sClubHouseMissionMenu.fCamZoomAlpha < 0
				sClubHouseMissionMenu.fCamZoomAlpha = 0
			ENDIF
		ENDIF
	ENDIF
	SET_CAM_FOV(sClubHouseMissionMenu.ciCam, COSINE_INTERP_FLOAT(F_CAM_FOV,F_CAM_ZOOMED_FOV,sClubHouseMissionMenu.fCamZoomAlpha))
	
ENDPROC

PROC UPDATE_CH_MISSION_HELP(CLUBHOUSE_MISSION_MENU_STRUCT &sClubHouseMissionMenu)
	BOOL bPrintHelp = FALSE
	IF sClubHouseMissionMenu.iMenuCurrentRow = 0
	AND sClubHouseMissionMenu.iMenuCurrentColumn < iCONST_NUM_CLUBHOUSE_MISSIONS
	AND sClubHouseMissionMenu.bFlickedIn
		IF !sClubHouseMissionMenu.bMissionAvailable[sClubHouseMissionMenu.iMenuCurrentColumn]
			bPrintHelp = TRUE
		ENDIF
	ENDIF
	
	IF bPrintHelp
		CLUBHOUSE_MISSION_ENUM eCHMission = sClubHouseMissionMenu.eCHMissions[sClubHouseMissionMenu.iMenuCurrentColumn]
		TEXT_LABEL sHelp = GET_CH_MISSION_MENU_DESCRIPTION(sClubHouseMissionMenu.pOwnerID, eCHMission)
		
		
		IF NOT ARE_STRINGS_EQUAL(sHelp, "CHMMD_COOLT")
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelp)
				CLEAR_HELP()
				PRINT_HELP_FOREVER(sHelp)
				sClubHouseMissionMenu.tlPrintedHelp = sHelp
			ENDIF
		ELSE
			INT iType = GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(eCHMission)
			INT iMS = GB_GET_BOSSWORK_COOLDOWN_TIME_REMAINING(iType)
			
			IF iMS <= 0
				CASSERTLN(DEBUG_AMBIENT, "UPDATE_CH_MISSION_HELP cooldown timer for \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\" elapsed, update...")
				g_bClubhouseMissionWallUpdate = TRUE
				EXIT
			ENDIF
			
			TEXT_LABEL_15 tl_15MenuDescriptionTime = GET_TEXT_OF_TIME(iMS)
			IF NOT IS_THIS_HELP_MESSAGE_WITH_LITERAL_STRING_BEING_DISPLAYED(sHelp, tl_15MenuDescriptionTime, HUD_COLOUR_PURE_WHITE)
				CLEAR_HELP()
				PRINT_HELP_WITH_LITERAL_STRING(sHelp, tl_15MenuDescriptionTime, HUD_COLOUR_PURE_WHITE)
				sClubHouseMissionMenu.tlPrintedHelp = sHelp
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_STRING_NULL_OR_EMPTY(sClubHouseMissionMenu.tlPrintedHelp)
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
				sClubHouseMissionMenu.tlPrintedHelp = ""
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Update clubhouse mission menu
/// PARAMS:
///    sClubHouseMissionMenu - 
PROC UPDATE_CH_MISSION_MENU(CLUBHOUSE_MISSION_MENU_STRUCT &sClubHouseMissionMenu,INT iCurrentProperty)
	SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)		
			IF INT_TO_PLAYERINDEX(i) != PLAYER_ID()
				SET_PLAYER_INVISIBLE_LOCALLY(INT_TO_PLAYERINDEX(i))
			ENDIF
		ENDIF
	ENDREPEAT
	
	UPDATE_CH_MISSION_MAIN_MENU(sClubHouseMissionMenu)
	UPDATE_CH_MISSION_CAMERA(sClubHouseMissionMenu,iCurrentProperty)
	UPDATE_CH_MISSION_HELP(sClubHouseMissionMenu)
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_AIM)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL,INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL,INPUT_LOOK_LEFT)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL,INPUT_LOOK_RIGHT)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL,INPUT_LOOK_UP)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL,INPUT_LOOK_DOWN)
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_DPADDOWN_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()
ENDPROC

/// PURPOSE:
///    Process clubhouse mission menu
PROC PROCESS_CH_MISSION_MENU(CLUBHOUSE_MISSION_MENU_STRUCT &sClubHouseMissionMenu,INT iCurrentProperty)
	
	SETUP_CLUBHOUSE_MENU_CONTROLS(sClubHouseMissionMenu.sCHMenuInput,sClubHouseMissionMenu.iMenuCurrentColumn)
	
	IF LOAD_MENU_ASSETS()
	
//		IF sClubHouseMissionMenu.bReBuildMenu
//			BUILD_CH_MISSION_MAIN_MENU(sClubHouseMissionMenu)
//			sClubHouseMissionMenu.bReBuildMenu = FALSE
//		ELSE
//			SET_CURRENT_MENU_ITEM(sClubHouseMissionMenu.iMenuCurrentColumn)
//		ENDIF
		
		UPDATE_CH_MISSION_MENU(sClubHouseMissionMenu,iCurrentProperty)
//		DRAW_MENU()
		
		IF GET_CH_MISSION_MENU_STATE(sClubHouseMissionMenu) != CHM_MENU_STAGE_SELECT_MISSION
			CLEANUP_MENU_ASSETS()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Safty check if we can open cun locker menu
/// RETURNS:
///    True if it is safe
FUNC BOOL IS_SAFE_TO_START_CLUBHOUSE_GUN_LOCKER_MENUS(BOOL bAllowHeist = FALSE)
	
	IF !bAllowHeist
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			RETURN FALSE
		ENDIF
	ENDIF
		
	IF	IS_PHONE_ONSCREEN()
	OR IS_INTERACTION_MENU_OPEN()
	OR IS_BROWSER_OPEN()
	OR IS_SELECTOR_ONSCREEN()
	OR IS_CUSTOM_MENU_ON_SCREEN()
	OR IS_PAUSE_MENU_ACTIVE()
	OR (MPGlobalsAmbience.bTriggerPropertyExitOnFoot)
		RETURN FALSE
	ENDIF
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Update clubhouse mod shop advert 
PROC UPDATE_CLUBHOUSE_MOD_SHOP_AD_HELP(INT iCurrentProperty , PLAYER_INDEX clubhouseOwner)
	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
		IF clubhouseOwner != INVALID_PLAYER_INDEX()
			IF NOT DOES_PLAYER_OWNS_CLUBHOUSE_MOD_BOOTH(clubhouseOwner)
			AND PLAYER_ID() = clubhouseOwner
				BOOL  bDisplay = FALSE
				
				IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
					IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(clubhouseOwner), <<1100.994873,-3158.324463,-38.518600>>, <<1101.159546,-3167.602539,-35.974911>>, 7.500000)
						bDisplay = TRUE		
					ENDIF
				ELSE
					IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(clubhouseOwner), <<999.822083,-3153.615234,-39.907425>>, <<999.795837,-3144.596680,-36.795170>>, 6.750000)
						bDisplay = TRUE		
					ENDIF
				ENDIF
						
				IF bDisplay
				AND NOT g_bBrowserVisible
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLUB_MOD_AD")
					PRINT_HELP_FOREVER("CLUB_MOD_AD")
				ELIF NOT bDisplay
				AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLUB_MOD_AD")
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC 


