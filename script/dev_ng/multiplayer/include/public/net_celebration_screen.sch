// Name: net_celebration_screen.sch
// Author: Matthew Booton
// Date: 22/08/2013
// Purpose: Header file containing commands for controlling the end of job celebration screen.


// ****************************************************************************
//
//	ARENA CELEBRATION ANIMS HELP:
//
//		CELEBRATION_ARENA_ANIM_PODIUM = FFA modes, CELEBRATION_ARENA_ANIM_FLAT is team
//
//		Script host declares CELEB_SERVER_DATA serverBD
//		and calls INITIALISE_CELEBRATION_SCREEN_DATA_SERVER at start
//		and SERVER_CHOOSE_ARENA_ANIMATIONS when the job ends
//		
//		Clients store the anim dictionaries using GET_ARENA_CELEB_ANIMS
//		and loop through the winners using UPDATE_CELEBRATION_WINNER_ANIMS_ARENA
//
//		CELEBRATION_GET_PED_CREATION_DATA = clone warp positions

//		Facial anims: 	MAINTAIN_FACIAL_ANIMS
//		Prop ptfx:		MAINTAIN_ARENA_CELEB_PTFX 
//
//		Job flow order of important functions:
//		GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN	: grab all the players to show
//		SERVER_CHOOSE_ARENA_ANIMATIONS				: grab the relevant anims
//		ARENA_ANIMS_READY							: wait until anims load
//		UPDATE_CELEBRATION_WINNER_ANIMS				: animate the players
//
// ****************************************************************************

USING "globals.sch"
USING "net_include.sch"
USING "Stat_Data_Common.sch"
USING "commands_network.sch"
USING "fmmc_corona_menu.sch"
USING "net_interactions.sch"
USING "fmmc_corona_anim_scene.sch"
USING "fmmc_vars.sch"
USING "fmmc_corona_anim_scene.sch"
USING "fm_maintain_strand_mission.sch"
USING "script_misc.sch"
USING "net_clone_ped.sch"

CONST_INT CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME	2833 //Half a second more than the standard display time.
CONST_INT CELEBRATION_SCREEN_STAT_DISPLAY_TIME			2333//2725 //This is equal to the pause time + a single wipe.
CONST_INT CELEBRATION_SCREEN_STAT_WIPE_TIME				333//225
CONST_INT CELEBRATION_SCREEN_EXTENSION_FOR_ANIM_TIME	4000

CONST_FLOAT CELEBRATION_SCREEN_WINNER_Z_OFFSET			-0.3		//The offsets are all made relative to the centre of the background, this modifies the Z in case we need to shift everything up/down.

CONST_INT MAX_NUM_CELEBRATION_PEDS	16

CONST_INT ciMissionEndEffectDelay          				1000

CONST_INT ciRND_MAX 									10000

ENUM CELEBRATION_WINNER_IDLE_ANIM
	CELEBRATION_IDLE_ANIM_INVALID = 0,
	CELEBRATION_IDLE_ANIM_MALE_A,
	CELEBRATION_IDLE_ANIM_MALE_B,
	CELEBRATION_IDLE_ANIM_FEMALE
ENDENUM

ENUM CELEBRATION_SCREEN_STAGE
	CELEBRATION_STAGE_SETUP,
	CELEBRATION_STAGE_TRANSITIONING,
	CELEBRATION_STAGE_DOING_FLASH,
	CELEBRATION_STAGE_PLAYING,
	CELEBRATION_STAGE_END_TRANSITION,
	CELEBRATION_STAGE_FINISHED,
	CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM
ENDENUM

STRUCT CELEBRATION_SCRIPT_DATA
	GAMER_HANDLE leaderHandle
ENDSTRUCT

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_CELEB_STAGE(CELEBRATION_SCREEN_STAGE eStage)

	STRING sReturn  
	
	SWITCH eStage
		CASE CELEBRATION_STAGE_SETUP
			sReturn = "CELEBRATION_STAGE_SETUP"
		BREAK
		CASE CELEBRATION_STAGE_TRANSITIONING
			sReturn = "CELEBRATION_STAGE_TRANSITIONING"
		BREAK
		CASE CELEBRATION_STAGE_DOING_FLASH
			sReturn = "CELEBRATION_STAGE_DOING_FLASH"
		BREAK
		CASE CELEBRATION_STAGE_PLAYING
			sReturn = "CELEBRATION_STAGE_PLAYING"
		BREAK
		CASE CELEBRATION_STAGE_END_TRANSITION
			sReturn = "CELEBRATION_STAGE_END_TRANSITION"
		BREAK
		CASE CELEBRATION_STAGE_FINISHED
			sReturn = "CELEBRATION_STAGE_FINISHED"
		BREAK
		CASE CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM
			sReturn = "CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM"
		BREAK
	ENDSWITCH

	RETURN sReturn
ENDFUNC
#ENDIF

ENUM ARENA_ANIM_SET
	CELEBRATION_ARENA_ANIM_INVALID = -1,
	CELEBRATION_ARENA_ANIM_PODIUM,
	CELEBRATION_ARENA_ANIM_FLAT
ENDENUM

ENUM ARENA_CELEB_ANIM_CLIPS

	ARENA_CELEB_INVALID = -1,
	
	// PODIUM_______________________________________________
	
	// Solo no props 1st
	ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_a_1st=0,			
	ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_b_1st,
	ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_c_1st,
	ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_d_1st,
	ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_e_1st,
	ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_f_1st,
	
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_cheer_a_1st,						
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_clapping_a_1st,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_cocky_a_1st,			
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_crowd_point_a_1st,	
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_dance_a_1st,			
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_dance_b_1st,			
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_finger_guns_a_1st,	
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_finger_guns_b_1st,	
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_fist_pump_a_1st,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_a_1st,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_b_1st,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_c_1st,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_hands_air_b_1st,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_hands_air_c_1st,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_make_noise_a_1st,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_a_1st,			
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_b_1st,			
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_c_1st,			
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_shrug_off_a_1st,
	
	ARENA_CELEB_PODIUM_NO_PROPS_PAIR_1ST_air_slap_a_1st,	// PAIRED	25	
	
	// Paired props
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_1st,						
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_1st,					
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_1st,
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_1st,					
	ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_1st,				
	ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_1st,
	ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_1st,
	
	// Solo no props 2nd
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cheer_a_2nd,			
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cocky_a_2nd,			
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_crowd_point_a_2nd,	
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_finger_guns_a_2nd,	
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_finger_guns_b_2nd,	
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_a_2nd,	
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_b_2nd,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_c_2nd,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_make_noise_a_2nd,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_shrug_off_a_2nd,	
	
	// Solo no props 3rd
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_clapping_a_3rd,	
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_dance_a_3rd,			
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_dance_b_3rd,			
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_fist_pump_a_3rd,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_hands_air_b_3rd,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_hands_air_c_3rd,		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_regal_a_3rd,			
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_regal_b_3rd,			//50		
	ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_regal_c_3rd,
	
	// PODIUM PAIRED
	ARENA_CELEB_PODIUM_NO_PROPS_PAIR_2ND_air_slap_a_2nd,			
	
	// Solo no props 2nd, paired
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_2nd,
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_2nd,
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_2nd,
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_2nd,
	ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_2nd,
	ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_2nd,
	ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_2nd,

	// Solo no props 3rd, paired
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_3rd,
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_3rd,
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_3rd,
	ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_3rd,
	ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_3rd,	
	ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_3rd,
	ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_3rd,
	
	// FLAT_______________________________________________
	
	// Paired no props A
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_a,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_a,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_a,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_a,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_a,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_a,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_a,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_a,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_a,	//75
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_a,			
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_a,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_a,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_a,
	
	// Paired props A
	ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_a,					
	ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_a,
	ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_a,
	ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_a,
	ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_a,
	ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_a,
	ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_a,
	
	// Paired no props B
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_b,
	ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_b,

	// Paired props B
	ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_b,
	ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_b,
	ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_b,
	ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_b,
	ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_b,
	ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_b,
	ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_b,
	
	// Flat solo Prop anims A
	ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_a_player_a,
	ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_b_player_a,
	
	// Solo no props A
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_a_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_b_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_cap_a_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_flip_a_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_giggle_a_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_a_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_b_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_c_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_d_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_pageant_a_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_a_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_b_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_c_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_slugger_a_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_a_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_b_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_b_player_a,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_thumbs_down_a_player_a,
	
	// Flat solo Prop anims B
	ARENA_CELEB_FLAT_SOLO_PROPS_make_it_rain_b_player_b,			
	ARENA_CELEB_FLAT_SOLO_PROPS_smoking_b_player_b,

	ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_b_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_giggle_a_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_pageant_a_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_a_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_b_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_b_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_c_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_d_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_e_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_thumbs_down_a_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_wave_a_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_wow_a_player_b,
	ARENA_CELEB_FLAT_SOLO_NO_PROPS_wow_b_player_b,
	
	//____________________________________________________

	
	ARENA_CELEB_MAX // KEEP HERE
ENDENUM

#IF IS_DEBUG_BUILD

FUNC STRING ARENA_ANIM_SET_DBG(ARENA_ANIM_SET eAnimSet)

	STRING sReturn 
	
	SWITCH eAnimSet
		CASE CELEBRATION_ARENA_ANIM_INVALID 	sReturn = "CELEBRATION_ARENA_ANIM_INVALID" 									BREAK
		CASE CELEBRATION_ARENA_ANIM_PODIUM		sReturn = "CELEBRATION_ARENA_ANIM_PODIUM" 									BREAK
		CASE CELEBRATION_ARENA_ANIM_FLAT		sReturn = "CELEBRATION_ARENA_ANIM_FLAT" 									BREAK
	ENDSWITCH
	
	RETURN sReturn 
ENDFUNC

FUNC STRING ARENA_CLIP_NAME_DBG(ARENA_CELEB_ANIM_CLIPS eClip)

	STRING sReturn 

	SWITCH eClip

		CASE ARENA_CELEB_INVALID										sReturn = "ARENA_CELEB_INVALID" 									BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_a_1st					sReturn = "ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_a_1st"				BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_b_1st					sReturn = "ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_b_1st"			 	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_c_1st					sReturn = "ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_c_1st"			 	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_d_1st					sReturn = "ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_d_1st"				BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_e_1st					sReturn = "ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_e_1st"				BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_f_1st					sReturn = "ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_f_1st"				BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_cheer_a_1st			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_cheer_a_1st" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_clapping_a_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_clapping_a_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_cocky_a_1st			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_cocky_a_1st" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_crowd_point_a_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_crowd_point_a_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_dance_a_1st			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_dance_a_1st" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_dance_b_1st			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_dance_b_1st" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_finger_guns_a_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_finger_guns_a_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_finger_guns_b_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_finger_guns_b_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_fist_pump_a_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_fist_pump_a_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_a_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_a_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_b_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_b_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_c_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_c_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_hands_air_b_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_hands_air_b_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_hands_air_c_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_hands_air_c_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_make_noise_a_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_make_noise_a_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_a_1st			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_a_1st" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_b_1st			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_b_1st" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_c_1st			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_c_1st" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_shrug_off_a_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_shrug_off_a_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cheer_a_2nd			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cheer_a_2nd" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cocky_a_2nd			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cocky_a_2nd" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_crowd_point_a_2nd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_crowd_point_a_2nd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_finger_guns_a_2nd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_finger_guns_a_2nd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_finger_guns_b_2nd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_finger_guns_b_2nd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_a_2nd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_a_2nd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_b_2nd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_b_2nd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_c_2nd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_c_2nd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_make_noise_a_2nd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_make_noise_a_2nd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_shrug_off_a_2nd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_shrug_off_a_2nd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_clapping_a_3rd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_clapping_a_3rd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_dance_a_3rd			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_dance_a_3rd" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_dance_b_3rd			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_dance_b_3rd" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_fist_pump_a_3rd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_fist_pump_a_3rd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_hands_air_b_3rd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_hands_air_b_3rd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_hands_air_c_3rd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_hands_air_c_3rd" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_regal_a_3rd			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_regal_a_3rd" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_regal_b_3rd			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_regal_b_3rd" 		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_regal_c_3rd			sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_regal_c_3rd" 		BREAK
		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_PAIR_1ST_air_slap_a_1st		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_PAIR_1ST_air_slap_a_1st" 	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_PAIR_2ND_air_slap_a_2nd		sReturn = "ARENA_CELEB_PODIUM_NO_PROPS_PAIR_2ND_air_slap_a_2nd" 	BREAK
		
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_1st				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_1st				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_1st				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_1st				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_1st			sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_1st			sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_1st			sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_2nd				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_2nd				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_2nd				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_2nd				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_2nd			sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_2nd"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_2nd			sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_2nd"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_2nd			sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_2nd"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_3rd				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_3rd				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_3rd				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_3rd				sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_3rd			sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_3rd"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_3rd			sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_3rd"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_3rd			sReturn = "ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_3rd"	BREAK
		
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_a_player_a		sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_a_player_a"	BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_b_player_a		sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_b_player_a"	BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_b_player_b		sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_b_player_b"	BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_cap_a_player_a				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_cap_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_flip_a_player_a				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_flip_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_giggle_a_player_a			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_giggle_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_giggle_a_player_b			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_giggle_a_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_a_player_a				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_b_player_a				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_c_player_a				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_c_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_d_player_a				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_d_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_pageant_a_player_a			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_pageant_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_pageant_a_player_b			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_pageant_a_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_a_player_a			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_b_player_a			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_c_player_a			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_c_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_slugger_a_player_a			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_slugger_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_a_player_a				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_a_player_b				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_b_player_a				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_b_player_b				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_b_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_b_player_a			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_b_player_b			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_b_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_c_player_b			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_c_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_d_player_b			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_d_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_e_player_b			sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_e_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_thumbs_down_a_player_a		sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_thumbs_down_a_player_a"	BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_thumbs_down_a_player_b		sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_thumbs_down_a_player_b"	BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_wave_a_player_b				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_wave_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_wow_a_player_b				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_wow_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_wow_b_player_b				sReturn = "ARENA_CELEB_FLAT_SOLO_NO_PROPS_wow_b_player_b"			BREAK
		
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_a				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_b				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_a				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_b				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_a				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_b				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_a				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_b				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_a				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_b				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_a				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_b				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_a				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_b				sReturn = "ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_b"		BREAK
		
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_a			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_b			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_a				sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_a"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_b				sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_b"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_a		sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_b		sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_a		sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_b		sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_a		sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_b		sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_a				sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_a"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_b				sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_b"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_a			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_a"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_b			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_b"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_a			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_a"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_b			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_b"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_a		sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_b		sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_a		sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_b		sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_a			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_b			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_a			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_b			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_a			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_b			sReturn = "ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_b"			BREAK
																	
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_a_player_a			sReturn = "ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_b_player_a			sReturn = "ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_make_it_rain_b_player_b		sReturn = "ARENA_CELEB_FLAT_SOLO_PROPS_make_it_rain_b_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_smoking_b_player_b				sReturn = "ARENA_CELEB_ANIM_CLIPSsmoking_b_player_b"					BREAK
		
//		CASE ARENA_CELEB_FLAT_PROP_ANIM_cham_a_champagne				sReturn = "ARENA_CELEB_FLAT_PROP_ANIM_cham_a_champagne"					BREAK
//		CASE ARENA_CELEB_FLAT_PROP_ANIM_cham_b_champagne				sReturn = "ARENA_CELEB_FLAT_PROP_ANIM_cham_b_champagne"					BREAK
//		CASE ARENA_CELEB_FLAT_PROP_ANIM_cham_c_champagne				sReturn = "ARENA_CELEB_FLAT_PROP_ANIM_cham_c_champagne"					BREAK
//		CASE ARENA_CELEB_FLAT_PROP_ANIM_cham_d_champagne				sReturn = "ARENA_CELEB_FLAT_PROP_ANIM_cham_d_champagne"					BREAK
//		CASE ARENA_CELEB_FLAT_PROP_ANIM_trophy_a_trophy					sReturn = "ARENA_CELEB_FLAT_PROP_ANIM_trophy_a_trophy"					BREAK
//		CASE ARENA_CELEB_FLAT_PROP_ANIM_trophy_b_trophy					sReturn = "ARENA_CELEB_FLAT_PROP_ANIM_trophy_b_trophy"					BREAK
//		CASE ARENA_CELEB_FLAT_PROP_ANIM_trophy_c_trophy					sReturn = "ARENA_CELEB_FLAT_PROP_ANIM_trophy_c_trophy"					BREAK

		CASE ARENA_CELEB_MAX 											sReturn = "ARENA_CELEB_MAX" 											BREAK
	
	ENDSWITCH
	
	RETURN sReturn

ENDFUNC
#ENDIF

ENUM CELEBRATION_SCREEN_TYPE
	CELEBRATION_STANDARD,
	CELEBRATION_HEIST,
	CELEBRATION_GANGOPS
ENDENUM

CONST_INT MAX_PODIUM 	3
CONST_INT MAX_FLAT		2

CONST_INT MAX_ARENA_PROPS 3

CONST_INT POS_1ST 0
CONST_INT POS_2ND 1
CONST_INT POS_3RD 2

CONST_INT ciANIMS_GRABBED 0

STRUCT CELEB_SERVER_DATA
	ARENA_ANIM_SET aArenaAnimType = CELEBRATION_ARENA_ANIM_INVALID
	ARENA_CELEB_ANIM_CLIPS eArenaClips[NUM_NETWORK_PLAYERS] 
	INT iRand[MAX_PODIUM] 
	INT iBitSet
	INT iNumPropsRequired 
	INT iRandomPropValue = -1
	INT iNumPlayersFound
ENDSTRUCT

STRUCT CELEBRATION_SCREEN_DATA
	SCALEFORM_INDEX sfForeground
	SCALEFORM_INDEX sfBackground
	SCALEFORM_INDEX sfCelebration
	SCALEFORM_INDEX playerNameMovies[MAX_NUM_CELEBRATION_PEDS]
	PED_INDEX pedWinnerClone
	OBJECT_INDEX objWinnerBackground
	OBJECT_INDEX objPlinth
	PED_INDEX pedWinnerClones[MAX_NUM_CELEBRATION_PEDS]
	VEHICLE_INDEX vehWinnerClone
	INTERIOR_INSTANCE_INDEX winnerSceneInterior
	VEHICLE_SETUP_STRUCT_MP vehicleSetupMp
	TEXT_LABEL_63 tl31_pedWinnerClonesNames[MAX_NUM_CELEBRATION_PEDS]
	INT iPedWinnerClonePlayerId[MAX_NUM_CELEBRATION_PEDS]
	INT iEstimatedScreenDuration = 0
	INT iBlackRectangleAlpha
	INT iCurrentCelebrationCam
	INT iCurrentCelebrationCamModifier
	INT iTimePlayedGesture = 0
	INT iWinnerPlayerID					//These need to be stored at the start of the winner anim, in case the player leaves half-way through the screen.
	INT iWinnerBackgroundPlayerID		//These need to be stored at the start of the winner anim, in case the player leaves half-way through the screen.
	INT iPlayedWinnerAnim
	BOOL bTriggeredEarlyFlash
	BOOL bUseFullBodyWinnerAnim
	INT iNumPlayerFoundForWinnerScene
	SCRIPT_TIMER sCelebrationTimer
	SCRIPT_TIMER sCelebrationTransitionTimer
	SCRIPT_TIMER stNgFailSafeTimer
	CELEBRATION_SCREEN_STAGE eCurrentStage
	SHAPETEST_INDEX stiCelebrationCam
	INT iwinnerSyncedSceneId
	// Commented out for B* 1771409 - uncomment to add back in.
//	INT iFakeMpGamerTagId[MAX_NUM_CELEBRATION_PEDS]
//	STRING strFakeTagName[MAX_NUM_CELEBRATION_PEDS]
//	BOOL bSetupFakeGamerTags
	INT iKnuckleAudioBitset
	INT iKnuckAudioSoundId = -1
	ENTITY_INDEX entityProp
	ENTITY_INDEX ArenaProps[MAX_ARENA_PROPS]
	ENTITY_INDEX ArenaPodium
	INT iDisplayingLocalPlayerAsLoser = -1
	FLOAT fAnimLength
	CELEBRATION_WINNER_IDLE_ANIM eIdleAnimToUse
	INT iPlayWinnerAnimsStage
	INT iDrawNamesStage
	INT iNumNamesToDisplay = -1
	BOOL bPreLoadComplete
	SCRIPT_TIMER resetPreLoadForSepctatorTimer
	SCRIPT_TIMER timerDeathPostFxDelay
	BOOL bCalledForceAnimUpdate[MAX_NUM_CELEBRATION_PEDS]
	BOOL bToggleNames
	SCALEFORM_INDEX celebInstructions
	INT iPlayerListProgress
	SCALEFORM_INSTRUCTIONAL_BUTTONS scaleformStruct
	INT iLoadSceneStage
	INT iVoiceStateCopy[MAX_NUM_CELEBRATION_PEDS]
	BOOL bUseExtendedIdle
	BOOL bCreateWinnerSceneEntities
	SCRIPT_TIMER stPassedPostEffectTimer
	SCRIPT_TIMER timerCelebPreLoadPostFxBeenPlaying
	BOOL bCleanedUpEndWinnerScene
	BOOL bShownPreloadFX
	BOOL bAllowPlayerNameToggles
	INT iAnimationPlayingBitset
	BOOL bHidePlayers
	SCRIPT_TIMER SpecFillGlobalsTimeout
	INT iRenderphasePauseAttempts
	ARENA_POINTS ArenaPoints
	CELEBRATION_SCREEN_TYPE eCelebrationScreenType
	PTFX_ID PTFXID_Loop[MAX_PODIUM]	
//	SCRIPT_TIMER stFXTimer[MAX_PODIUM]	
	INT iFacialBitset
	CAMERA_INDEX camArenaWinner
	OBJECT_INDEX objectLightRig
	#IF IS_DEBUG_BUILD
	BOOL bDebugLightTest
	BOOL bResetCeleb
//	INT iNumWinners = -1
	#ENDIF
	OBJECT_INDEX oiIslandLightObject
	INT iBitSet
ENDSTRUCT

// iBitSet
CONST_INT ciCELEB_CLIENT_BIT_POSITIONED_LIGHT 0

//TWEAK_FLOAT TweakLightZ 0.0

FUNC BOOL ARENA_IS_PODIUM(CELEB_SERVER_DATA &sCelebServer)
	RETURN (sCelebServer.aArenaAnimType = CELEBRATION_ARENA_ANIM_PODIUM)
ENDFUNC

FUNC BOOL ARENA_IS_FLAT(CELEB_SERVER_DATA &sCelebServer)
	RETURN (sCelebServer.aArenaAnimType = CELEBRATION_ARENA_ANIM_FLAT)
ENDFUNC

FUNC INT MAX_CELEB_PLAYERS_TO_ANIMATE(CELEB_SERVER_DATA &sCelebServer)
	IF ARENA_IS_FLAT(sCelebServer)
		RETURN MAX_FLAT
	ENDIF

	RETURN MAX_PODIUM
ENDFUNC

FUNC INT NUM_CELEB_PLAYERS_TO_ANIMATE(CELEB_SERVER_DATA &sCelebServer)

	INT iReturn

	IF ARENA_IS_FLAT(sCelebServer)
		iReturn = MAX_FLAT
	ENDIF

	iReturn = MAX_PODIUM
	
	// Cap
	IF iReturn > sCelebServer.iNumPlayersFound
		iReturn = sCelebServer.iNumPlayersFound
	ENDIF	
	
	RETURN iReturn
ENDFUNC

FUNC TEXT_LABEL_63 ARENA_CAM_ANIM_DICT(CELEB_SERVER_DATA &sCelebServer)

	TEXT_LABEL_63 tl63_AnimDict
	
	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@"
	
	IF ARENA_IS_FLAT(sCelebServer)
	
		tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@"
	ENDIF
	
	RETURN tl63_AnimDict

ENDFUNC

FUNC STRING ARENA_CAM_NAME(CELEB_SERVER_DATA &sCelebServer)

	STRING sReturn

	sReturn = "PODIUM_CAM_1"
	
	IF ARENA_IS_FLAT(sCelebServer)
	
		sReturn = "FLAT_CAM_1"
	ENDIF

	RETURN sReturn 
ENDFUNC

PROC GET_ARENA_CELEB_ANIMS(CELEB_SERVER_DATA &sCelebServer, TEXT_LABEL_63 &tl63_AnimDict, TEXT_LABEL_63 &tl63_AnimName, INT iPosition)
	
	SWITCH sCelebServer.eArenaClips[iPosition]
	
		DEFAULT	
//			#IF IS_DEBUG_BUILD 
				PRINTLN("[NETCELEBRATION] [ARENA] ERROR GET_ARENA_CELEB_ANIMS, eArenaClips is empty at ", NATIVE_TO_INT(GET_NETWORK_TIME())) 
//				SCRIPT_ASSERT("GET_ARENA_CELEB_ANIMS, eArenaClips is empty ") 
//			#ENDIF 
		BREAK
	
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_a_1st						tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "trophy_a_1st"			BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_b_1st						tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "trophy_b_1st"			BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_c_1st						tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "trophy_c_1st"			BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_d_1st						tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "trophy_d_1st"			BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_e_1st						tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "trophy_e_1st"			BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_f_1st						tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "trophy_f_1st"			BREAK

		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_cheer_a_1st 		 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "cheer_a_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_clapping_a_1st		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "clapping_a_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_cocky_a_1st			 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "cocky_a_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_crowd_point_a_1st			tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "crowd_point_a_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_dance_a_1st			 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "dance_a_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_dance_b_1st			 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "dance_b_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_finger_guns_a_1st		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "finger_guns_a_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_finger_guns_b_1st		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "finger_guns_b_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_fist_pump_a_1st		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "fist_pump_a_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_a_1st		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "flip_off_a_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_b_1st		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "flip_off_b_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_c_1st		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "flip_off_c_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_hands_air_b_1st		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "hands_air_b_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_hands_air_c_1st		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "hands_air_c_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_make_noise_a_1st	 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "make_noise_a_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_a_1st			 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "regal_a_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_b_1st			 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "regal_b_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_c_1st			 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "regal_c_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_shrug_off_a_1st	 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "shrug_off_a_1st"	BREAK
				
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cheer_a_2nd			 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "cheer_a_2nd"			BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cocky_a_2nd			 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "cocky_a_2nd"			BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_crowd_point_a_2nd			tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "crowd_point_a_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_finger_guns_a_2nd			tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "finger_guns_a_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_finger_guns_b_2nd			tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "finger_guns_b_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_a_2nd		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "flip_off_a_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_b_2nd		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "flip_off_b_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_c_2nd		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "flip_off_c_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_make_noise_a_2nd		 	tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "make_noise_a_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_shrug_off_a_2nd	 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "shrug_off_a_2nd"		BREAK

		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_clapping_a_3rd	 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "clapping_a_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_dance_a_3rd		 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "dance_a_3rd"			BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_dance_b_3rd		 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "dance_b_3rd"			BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_fist_pump_a_3rd	 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "fist_pump_a_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_hands_air_b_3rd	 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "hands_air_b_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_hands_air_c_3rd	 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "hands_air_c_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_regal_a_3rd		 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "regal_a_3rd"			BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_regal_b_3rd		 		tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "regal_b_3rd"			BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_regal_c_3rd	 			tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "regal_c_3rd"			BREAK
																			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_PAIR_1ST_air_slap_a_1st			tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "air_slap_a_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_NO_PROPS_PAIR_2ND_air_slap_a_2nd			tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" tl63_AnimName = "air_slap_a_2nd"		BREAK
							
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_1st					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_chug_a_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_1st					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_spray_a_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_1st					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_spray_b_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_1st					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_spray_c_1st"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_1st				tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "confetti_canon_a_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_1st				tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "confetti_canon_c_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_1st				tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "confetti_canon_d_1st"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_2nd					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_chug_a_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_2nd					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_spray_a_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_2nd					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_spray_b_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_2nd					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_spray_c_2nd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_2nd				tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "confetti_canon_a_2nd"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_2nd				tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "confetti_canon_c_2nd"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_2nd				tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "confetti_canon_d_2nd"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_3rd					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_chug_a_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_3rd					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_spray_a_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_3rd					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_spray_b_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_3rd					tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "cham_spray_c_3rd"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_3rd				tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "confetti_canon_a_3rd"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_3rd				tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "confetti_canon_c_3rd"	BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_3rd				tl63_AnimDict = "anim@arena@celeb@podium@prop@"  tl63_AnimName = "confetti_canon_d_3rd"	BREAK		
		
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_a_player_a			tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "angry_clap_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_b_player_a			tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "angry_clap_b_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_angry_clap_b_player_b			tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "angry_clap_b_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_cap_a_player_a					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "cap_a_player_a"				BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_flip_a_player_a					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "flip_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_giggle_a_player_a				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "giggle_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_giggle_a_player_b				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "giggle_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_a_player_a					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "jump_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_b_player_a					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "jump_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_c_player_a					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "jump_c_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_jump_d_player_a					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "jump_d_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_pageant_a_player_a				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "pageant_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_pageant_a_player_b				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "pageant_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_a_player_a				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "slide_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_b_player_a				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "slide_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_c_player_a				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "slide_c_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_slugger_a_player_a				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "slugger_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_a_player_a					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "smug_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_a_player_b					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "smug_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_b_player_a					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "smug_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_smug_b_player_b					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "smug_b_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_b_player_a				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "taunt_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_b_player_b				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "taunt_b_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_c_player_b				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "taunt_c_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_d_player_b				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "taunt_d_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_taunt_e_player_b				tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "taunt_e_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_thumbs_down_a_player_a			tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "thumbs_down_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_thumbs_down_a_player_b			tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "thumbs_down_a_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_wave_a_player_b					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "wave_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_wow_a_player_b					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "wow_a_player_b"				BREAK
		CASE ARENA_CELEB_FLAT_SOLO_NO_PROPS_wow_b_player_b					tl63_AnimDict = "anim@arena@celeb@flat@solo@no_props@" tl63_AnimName = "wow_b_player_b"				BREAK				
		
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_a					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "cham_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_b					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "cham_a_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_a					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "cham_b_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_b					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "cham_b_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_a					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "cham_c_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_b					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "cham_c_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_a					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "cham_d_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_b					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "cham_d_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_a					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "trophy_a_player_a"	BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_b					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "trophy_a_player_b"	BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_a					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "trophy_b_player_a"	BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_b					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "trophy_b_player_b"	BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_a					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "trophy_c_player_a"	BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_b					tl63_AnimDict = "anim@arena@celeb@flat@paired@props@" tl63_AnimName = "trophy_c_player_b"	BREAK
		
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_a				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "baseball_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_b				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "baseball_a_player_b" 		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_a					tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "daps_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_b					tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "daps_b_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_a			tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "did_you_see_a_player_a"	BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_b			tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "did_you_see_a_player_b"	BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_a			tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "high_five_b_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_b			tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "high_five_b_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_a			tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "high_five_c_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_b			tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "high_five_c_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_a					tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "hype_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_b					tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "hype_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_a				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "laugh_a_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_b				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "laugh_a_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_a				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "laugh_b_player_a"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_b				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "laugh_b_player_b"			BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_a			tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "piggyback_b_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_b			tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "piggyback_b_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_a			tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "piggyback_c_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_b			tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "piggyback_c_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_a				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "the_bird_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_b				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "the_bird_a_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_a				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "this_guy_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_b				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "this_guy_a_player_b"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_a				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "this_guy_b_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_b				tl63_AnimDict = "anim@arena@celeb@flat@paired@no_props@" tl63_AnimName = "this_guy_b_player_b"		BREAK

		CASE ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_a_player_a				tl63_AnimDict = "anim@arena@celeb@flat@solo@props@" tl63_AnimName = "chug_beer_a_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_b_player_a				tl63_AnimDict = "anim@arena@celeb@flat@solo@props@" tl63_AnimName = "chug_beer_b_player_a"		BREAK
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_make_it_rain_b_player_b			tl63_AnimDict = "anim@arena@celeb@flat@solo@props@" tl63_AnimName = "make_it_rain_b_player_b"	BREAK
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_smoking_b_player_b					tl63_AnimDict = "anim@arena@celeb@flat@solo@props@" tl63_AnimName = "smoking_b_player_b"		BREAK
		
	ENDSWITCH

ENDPROC

PROC GET_ARENA_CELEB_PROP_ANIMS(CELEB_SERVER_DATA &sCelebServer, TEXT_LABEL_63 &tl63_AnimDict, TEXT_LABEL_63 &tl63_AnimName, INT iPosition)

	tl63_AnimDict = "anim@arena@celeb@podium@prop@"
	
	IF ARENA_IS_FLAT(sCelebServer)
	
		tl63_AnimDict = "anim@arena@celeb@flat@paired@props@"
	ENDIF
		
	SWITCH sCelebServer.eArenaClips[iPosition]

		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_a			tl63_AnimName = "cham_a_champagne"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_a			tl63_AnimName = "cham_b_champagne"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_a			tl63_AnimName = "cham_c_champagne"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_a			tl63_AnimName = "cham_d_champagne"				BREAK

		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_b			tl63_AnimName = "cham_a_champagne"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_b			tl63_AnimName = "cham_b_champagne"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_b			tl63_AnimName = "cham_c_champagne"				BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_b			tl63_AnimName = "cham_d_champagne"				BREAK
	
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_1st			tl63_AnimName = "cham_chug_a_champagne"			BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_1st			tl63_AnimName = "cham_spray_a_champagne"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_1st			tl63_AnimName = "cham_spray_b_champagne"		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_1st			tl63_AnimName = "cham_spray_c_champagne"		BREAK
		
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_1st		tl63_AnimName = "confetti_canon_a_1st"			BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_1st		tl63_AnimName = "confetti_canon_c_1st"			BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_1st		tl63_AnimName = "confetti_canon_d_1st"			BREAK
		
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_a_1st	
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_d_1st	
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_b
			tl63_AnimName = "trophy_a_trophy"				
		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_b_1st		
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_e_1st
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_b
			tl63_AnimName = "trophy_b_trophy"				
		BREAK
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_c_1st				
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_f_1st		
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_b
			tl63_AnimName = "trophy_c_trophy"				
		BREAK
		
	ENDSWITCH

ENDPROC

ENUM PAIRED_CELEBRATION_GENDER_COMBO
	PAIRED_CELEBRATION_GENDER_COMBO_NOT_SET = 0,
	PAIRED_CELEBRATION_GENDER_COMBO_M_M,
	PAIRED_CELEBRATION_GENDER_COMBO_M_F,
	PAIRED_CELEBRATION_GENDER_COMBO_F_F
ENDENUM

CONST_INT PAIRED_CELEBRATION_ANIM_MANLY_HANDSHAKE	0
CONST_INT PAIRED_CELEBRATION_ANIM_FIST_BUMP 		1
CONST_INT PAIRED_CELEBRATION_ANIM_BACKSLAP 			2
CONST_INT PAIRED_CELEBRATION_ANIM_HIGH_FIVE			3
CONST_INT PAIRED_CELEBRATION_ANIM_COWERING			4
CONST_INT PAIRED_CELEBRATION_ANIM_SARCASTIC			5
CONST_INT PAIRED_CELEBRATION_ANIM_BRO_HUG			6

ENUM GENERIC_MINIGAME_STAGE
	GENERIC_CELEBRATION_WINNER,
	GENERIC_CELEBRATION_STATS
ENDENUM

ENUM JOB_WIN_STATUS
	JOB_STATUS_WIN,
	JOB_STATUS_LOSE,
	JOB_STATUS_DRAW,
	JOB_STATUS_PASSED,
	JOB_STATUS_FAILED
ENDENUM

ENUM CHALLENGE_PART_TYPE
	CHALLENGE_PART_CREW,
	CHALLENGE_PART_HEAD_TO_HEAD
ENDENUM

ENUM CELEBRATION_CHALLENGE_TYPE
	CHALLENGE_TYPE_CREW,
	CHALLENGE_TYPE_HEAD_TO_HEAD,
	CHALLENGE_TYPE_PLAYLIST,
	CHALLENGE_TYPE_TOURNAMENT
ENDENUM

ENUM CELEBRATION_JOB_TYPE

	CELEBRATION_JOB_TYPE_MISSION,
	CELEBRATION_JOB_TYPE_VS_MISSION,
	CELEBRATION_JOB_TYPE_SURVIVAL,
	CELEBRATION_JOB_TYPE_PILOT_SCHOOL,
	CELEBRATION_JOB_TYPE_GANG_ATTACK,
	CELEBRATION_JOB_TYPE_LTS,
	
	CELEBRATION_JOB_TYPE_HEIST_FINALE,
	CELEBRATION_JOB_TYPE_HEIST_PREP,
	
	CELEBRATION_JOB_TYPE_MISSION_EARLY_FAIL,
	CELEBRATION_JOB_TYPE_DEATHMATCH_OBJECTIVE
	
ENDENUM

ENUM eADD_HEIST_CELEBRATION_DATA_CATEGORY
	eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_LEADER_PASS = 0,
	eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_NOT_LEADER_PASS,
	eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_LEADER_FAIL,
	eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_NOT_LEADER_FAIL,
	eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_LEADER_PASS,
	eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_NOT_LEADER_PASS,
	eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_LEADER_FAIL,
	eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_NOT_LEADER_FAIL,
	eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_TEST
ENDENUM

PROC ACTIVATE_CELEBRATION_AUDIO_CELEB_SCREEN_SCENE()
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_CELEB_STATS_SCENE")
		START_AUDIO_SCENE("DLC_HEIST_CELEB_STATS_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - started audio scene DLC_HEIST_CELEB_STATS_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC DEACTIVATE_CELEBRATION_AUDIO_CELEB_SCREEN_SCENE()
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_CELEB_STATS_SCENE")
		STOP_AUDIO_SCENE("DLC_HEIST_CELEB_STATS_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - stopped audio scene DLC_HEIST_CELEB_STATS_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC ACTIVATE_CELEBRATION_FADE_WHITE_SCENE()
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_STATS_SCREEN_FADE_WHITE_SCENE")
		START_AUDIO_SCENE("DLC_HEIST_POST_STATS_SCREEN_FADE_WHITE_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - started audio scene DLC_HEIST_POST_STATS_SCREEN_FADE_WHITE_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC DEACTIVATE_CELEBRATION_FADE_WHITE_SCENE()
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_STATS_SCREEN_FADE_WHITE_SCENE")
		STOP_AUDIO_SCENE("DLC_HEIST_POST_STATS_SCREEN_FADE_WHITE_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - stopped audio scene DLC_HEIST_POST_STATS_SCREEN_FADE_WHITE_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC ACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("MP_BLUR_PRE_CELEB_SCREEN_SCENE")
		START_AUDIO_SCENE("MP_BLUR_PRE_CELEB_SCREEN_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - started audio scene MP_BLUR_PRE_CELEB_SCREEN_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC DEACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_BLUR_PRE_CELEB_SCREEN_SCENE")
		STOP_AUDIO_SCENE("MP_BLUR_PRE_CELEB_SCREEN_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - stopped audio scene MP_BLUR_PRE_CELEB_SCREEN_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC DEACTIVATE_CELEBRATION_AUDIO_SPECIFIC_STRAND_SCENES()

	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_STATS_PACIFIC_SCENE")
		STOP_AUDIO_SCENE("DLC_HEIST_POST_CELEB_STATS_PACIFIC_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - stopped audio scene DLC_HEIST_POST_CELEB_STATS_PACIFIC_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_STATS_PRISON_SCENE")
		STOP_AUDIO_SCENE("DLC_HEIST_POST_CELEB_STATS_PRISON_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - stopped audio scene DLC_HEIST_POST_CELEB_STATS_PRISON_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_STATS_SERIES_A_SCENE")
		STOP_AUDIO_SCENE("DLC_HEIST_POST_CELEB_STATS_SERIES_A_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - stopped audio scene DLC_HEIST_POST_CELEB_STATS_SERIES_A_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_STATS_FLEECA_SCENE")
		STOP_AUDIO_SCENE("DLC_HEIST_POST_CELEB_STATS_FLEECA_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - stopped audio scene DLC_HEIST_POST_CELEB_STATS_FLEECA_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_STATS_HUMANE_SCENE")
		STOP_AUDIO_SCENE("DLC_HEIST_POST_CELEB_STATS_HUMANE_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - stopped audio scene DLC_HEIST_POST_CELEB_STATS_HUMANE_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC ACTIVATE_CELEBRATION_AUDIO_FADE_BLACK_SCENE()
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_SCREEN_FADE_BLACK_SCENE")
		START_AUDIO_SCENE("DLC_HEIST_POST_CELEB_SCREEN_FADE_BLACK_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - started audio scene DLC_HEIST_POST_CELEB_SCREEN_FADE_BLACK_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC DEACTIVATE_CELEBRATION_AUDIO_FADE_BLACK_SCENE()
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_SCREEN_FADE_BLACK_SCENE")
		STOP_AUDIO_SCENE("DLC_HEIST_POST_CELEB_SCREEN_FADE_BLACK_SCENE")
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - stopped audio scene DLC_HEIST_POST_CELEB_SCREEN_FADE_BLACK_SCENE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC ENABLE_PREPARE_CELEBRATION_STATS_SCREEN_STOP_MUSIC_EVENT()

	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STATS_SCREEN_STOP)
		SET_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STATS_SCREEN_STOP)
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - CELEB_AUDIO_PREPARE_MUSIC_EVENT_STATS_SCREEN_STOP")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC ENABLE_PREPARE_CELEBRATION_STRIP_CLUB_MUSIC_EVENT()

	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STRIP_CLUB)
		SET_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STRIP_CLUB)
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - ENABLE_PREPARE_CELEBRATION_STRIP_CLUB_MUSIC_EVENT")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC ENABLE_PREPARE_CELEBRATION_APARTMENT_MUSIC_EVENT()

	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_APARTMENT)
		SET_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_APARTMENT)
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - ENABLE_PREPARE_CELEBRATION_APARTMENT_MUSIC_EVENT")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
		
ENDPROC

PROC START_CELEBRATION_STRIP_CLUB_MUSIC_EVENT()

	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_STRIP_CLUB)
		SET_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_STRIP_CLUB)
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - CELEB_AUDIO_START_MUSIC_EVENT_STRIP_CLUB")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC START_CELEBRATION_APARTMENT_MUSIC_EVENT()

	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_APARTMENT)
		SET_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_APARTMENT)
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - CELEB_AUDIO_START_MUSIC_EVENT_APARTMENT")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
		
ENDPROC

PROC SET_CELEBRATION_ALLOW_OVER_RADIO_SCREEN_AUDIO_FLAG()
	
	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_RADIO_OVER_SCREEN_FLAG)
		SET_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_RADIO_OVER_SCREEN_FLAG)
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - set bit CELEB_AUDIO_SET_ALLOW_RADIO_OVER_SCREEN_FLAG.")
		#ENDIF
		DEBUG_PRINTCALLSTACK()
	ENDIF

ENDPROC

PROC CLEAR_CELEBRATION_ALLOW_OVER_RADIO_SCREEN_AUDIO_FLAG()
	
	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_RADIO_OVER_SCREEN_FLAG)
		SET_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_RADIO_OVER_SCREEN_FLAG)
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - set bit bit CELEB_AUDIO_CLEAR_ALLOW_RADIO_OVER_SCREEN_FLAG.")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC SET_CELEBRATION_ALLOW_CUTSCENE_OVER_SCREEN_AUDIO_FLAG()
	
	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_CUTSCENE_OVER_SCREEN_FLAG)
		SET_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_CUTSCENE_OVER_SCREEN_FLAG)
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - set bit CELEB_AUDIO_SET_ALLOW_CUTSCENE_OVER_SCREEN_FLAG.")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF

ENDPROC

PROC CLEAR_CELEBRATION_ALLOW_CUTSCENE_OVER_SCREEN_AUDIO_FLAG()
	
	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_CUTSCENE_OVER_SCREEN_FLAG)
		SET_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_CUTSCENE_OVER_SCREEN_FLAG)
		#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - [CELEB_AUDIO] - set bit bit CELEB_AUDIO_CLEAR_ALLOW_CUTSCENE_OVER_SCREEN_FLAG.")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_CELEB_AUDIO_FLAGS()
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
	
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_RADIO_OVER_SCREEN_FLAG)
			SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_RADIO_OVER_SCREEN_FLAG)
			PRINTLN("[SAC] - [CELEB_AUDIO] - cleared bit CELEB_AUDIO_SET_ALLOW_RADIO_OVER_SCREEN_FLAG and cleared audio flag, gone back to single player.")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_RADIO_OVER_SCREEN_FLAG)
			SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_RADIO_OVER_SCREEN_FLAG)
			PRINTLN("[SAC] - [CELEB_AUDIO] - cleared bit CELEB_AUDIO_CLEAR_ALLOW_RADIO_OVER_SCREEN_FLAG and cleared audio flag, gone back to single player.")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_CUTSCENE_OVER_SCREEN_FLAG)
			SET_AUDIO_FLAG("AllowCutsceneOverScreenFade", FALSE)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_RADIO_OVER_SCREEN_FLAG)
			PRINTLN("[SAC] - [CELEB_AUDIO] - cleared bit CELEB_AUDIO_SET_ALLOW_CUTSCENE_OVER_SCREEN_FLAG and cleared audio flag, gone back to single player.")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_CUTSCENE_OVER_SCREEN_FLAG)
			SET_AUDIO_FLAG("AllowCutsceneOverScreenFade", FALSE)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_CUTSCENE_OVER_SCREEN_FLAG)
			PRINTLN("[SAC] - [CELEB_AUDIO] - cleared bit CELEB_AUDIO_CLEAR_ALLOW_CUTSCENE_OVER_SCREEN_FLAG and cleared audio flag, gone back to single player.")
		ENDIF
		
	ELSE
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_RADIO_OVER_SCREEN_FLAG)
			SET_AUDIO_FLAG("AllowRadioOverScreenFade", TRUE)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_RADIO_OVER_SCREEN_FLAG)
			PRINTLN("[SAC] - [CELEB_AUDIO] - SET_AUDIO_FLAG(AllowRadioOverScreenFade, TRUE)")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_RADIO_OVER_SCREEN_FLAG)
			SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_RADIO_OVER_SCREEN_FLAG)
			PRINTLN("[SAC] - [CELEB_AUDIO] - SET_AUDIO_FLAG(AllowRadioOverScreenFade, FALSE)")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_CUTSCENE_OVER_SCREEN_FLAG)
			SET_AUDIO_FLAG("AllowCutsceneOverScreenFade", TRUE)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_SET_ALLOW_CUTSCENE_OVER_SCREEN_FLAG)
			PRINTLN("[SAC] - [CELEB_AUDIO] - SET_AUDIO_FLAG(AllowCutsceneOverScreenFade, FALSE)")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_CUTSCENE_OVER_SCREEN_FLAG)
			SET_AUDIO_FLAG("AllowCutsceneOverScreenFade", FALSE)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_CLEAR_ALLOW_CUTSCENE_OVER_SCREEN_FLAG)
			PRINTLN("[SAC] - [CELEB_AUDIO] - SET_AUDIO_FLAG(AllowCutsceneOverScreenFade, FALSE)")
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_CELEB_MUSIC_EVENTS()
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
	
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STRIP_CLUB)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STRIP_CLUB)
			PRINTLN("[SAC] - [CELEB_AUDIO] - cleared bit CELEB_AUDIO_PREPARE_MUSIC_EVENT_STRIP_CLUB without preparing music event, gone back to single player.")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_APARTMENT)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_APARTMENT)
			PRINTLN("[SAC] - [CELEB_AUDIO] - cleared bit CELEB_AUDIO_PREPARE_MUSIC_EVENT_APARTMENT without preparing music event, gone back to single player.")
		ENDIF
			
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STATS_SCREEN_STOP)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STATS_SCREEN_STOP)
			PRINTLN("[SAC] - [CELEB_AUDIO] - cleared bit CELEB_AUDIO_PREPARE_MUSIC_EVENT_STATS_SCREEN_STOP without preparing music event, gone back to single player.")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_STRIP_CLUB)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_STRIP_CLUB)
			PRINTLN("[SAC] - [CELEB_AUDIO] - cleared bit CELEB_AUDIO_START_MUSIC_EVENT_STRIP_CLUB without starting music event, gone back to single player.")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_APARTMENT)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_APARTMENT)
			PRINTLN("[SAC] - [CELEB_AUDIO] - cleared bit CELEB_AUDIO_START_MUSIC_EVENT_APARTMENT without starting music event, gone back to single player.")
		ENDIF
	
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_STATS_SCREEN_STOP)
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_STATS_SCREEN_STOP)
			PRINTLN("[SAC] - [CELEB_AUDIO] - cleared bit CELEB_AUDIO_START_MUSIC_EVENT_STATS_SCREEN_STOP without starting music event, gone back to single player.")
		ENDIF
		
	ELSE
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STRIP_CLUB)
			PREPARE_MUSIC_EVENT("HEIST_CELEB_STRIP_CLUB")
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STRIP_CLUB)
			PRINTLN("[SAC] - [CELEB_AUDIO] - PREPARE_MUSIC_EVENT(HEIST_CELEB_STRIP_CLUB)")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_APARTMENT)
			PREPARE_MUSIC_EVENT("HEIST_CELEB_APARTMENT")
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_APARTMENT)
			PRINTLN("[SAC] - [CELEB_AUDIO] - PREPARE_MUSIC_EVENT(HEIST_CELEB_APARTMENT)")
		ENDIF
			
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STATS_SCREEN_STOP)
			PREPARE_MUSIC_EVENT("HEIST_STATS_SCREEN_STOP_PREP")
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_PREPARE_MUSIC_EVENT_STATS_SCREEN_STOP)
			PRINTLN("[SAC] - [CELEB_AUDIO] - PREPARE_MUSIC_EVENT(HEIST_STATS_SCREEN_STOP_PREP)")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_STRIP_CLUB)
			TRIGGER_MUSIC_EVENT("HEIST_CELEB_STRIP_CLUB")
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_STRIP_CLUB)
			PRINTLN("[SAC] - [CELEB_AUDIO] - TRIGGER_MUSIC_EVENT(HEIST_CELEB_STRIP_CLUB)")
		ENDIF
		
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_APARTMENT)
			TRIGGER_MUSIC_EVENT("HEIST_CELEB_APARTMENT")
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_APARTMENT)
			PRINTLN("[SAC] - [CELEB_AUDIO] - TRIGGER_MUSIC_EVENT(HEIST_CELEB_APARTMENT)")
		ENDIF
			
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_STATS_SCREEN_STOP)
			TRIGGER_MUSIC_EVENT("HEIST_STATS_SCREEN_STOP_PREP")
			CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset2, CELEB_AUDIO_START_MUSIC_EVENT_STATS_SCREEN_STOP)
			PRINTLN("[SAC] - [CELEB_AUDIO] - TRIGGER_MUSIC_EVENT(HEIST_STATS_SCREEN_STOP_PREP)")
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_CELEBRATION_AUDIO()
	
	MAINTAIN_CELEB_AUDIO_FLAGS()
	MAINTAIN_CELEB_MUSIC_EVENTS()
	
	// Clear this bitset if not on the off stage, so when we return to the off stage we'll run through the deactivation checks once. 
	IF g_TransitionSessionNonResetVars.sCelebAudioData.eStage != eCAS_OFF
		CLEAR_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset, eCAS_BITSET_DEACTIVATED_ALL_FOR_OFF_STAGE)
	ENDIF
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
		IF g_TransitionSessionNonResetVars.sCelebAudioData.eStage != eCAS_OFF
			g_TransitionSessionNonResetVars.sCelebAudioData.eStage = eCAS_OFF
			PRINTLN("[SAC] - [CELEB_AUDIO] - GET_CURRENT_GAMEMODE() = GAMEMODE_SP, setting sCelebAudioData.eStage = eCAS_OFF.")
		ENDIF
	ENDIF
	
	SWITCH g_TransitionSessionNonResetVars.sCelebAudioData.eStage
		
		CASE eCAS_OFF
			
			IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset, eCAS_BITSET_DEACTIVATED_ALL_FOR_OFF_STAGE)
				CLEAR_CELEBRATION_ALLOW_OVER_RADIO_SCREEN_AUDIO_FLAG()
				DEACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
				DEACTIVATE_CELEBRATION_AUDIO_CELEB_SCREEN_SCENE()
				DEACTIVATE_CELEBRATION_AUDIO_SPECIFIC_STRAND_SCENES()
				DEACTIVATE_CELEBRATION_AUDIO_FADE_BLACK_SCENE()
				DEACTIVATE_CELEBRATION_FADE_WHITE_SCENE()
				RESET_NET_TIMER(g_TransitionSessionNonResetVars.sCelebAudioData.blackScreenFailTimer)
				SET_BIT(g_TransitionSessionNonResetVars.sCelebAudioData.iBitset, eCAS_BITSET_DEACTIVATED_ALL_FOR_OFF_STAGE)
			ENDIF
			
		BREAK
		
		CASE eCAS_BLUR_SCREEN_SCENE
			
			ACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
			
		BREAK
		
		CASE eCAS_CELEB_SCREEN_SCENE
			
			DEACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
			ACTIVATE_CELEBRATION_AUDIO_CELEB_SCREEN_SCENE()
			
		BREAK
		
		CASE eCAS_FADE_WHITE_SCENE
			
			ACTIVATE_CELEBRATION_FADE_WHITE_SCENE()
		
		BREAK
		
		CASE eCAS_PACIFIC_SCENE
			
			DEACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
			DEACTIVATE_CELEBRATION_AUDIO_CELEB_SCREEN_SCENE()
			DEACTIVATE_CELEBRATION_FADE_WHITE_SCENE()
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_STATS_PACIFIC_SCENE")
				START_AUDIO_SCENE("DLC_HEIST_POST_CELEB_STATS_PACIFIC_SCENE")
				#IF IS_DEBUG_BUILD
				PRINTLN("[SAC] - [CELEB_AUDIO] - started audio scene DLC_HEIST_POST_CELEB_STATS_PACIFIC_SCENE")
				DEBUG_PRINTCALLSTACK()
				#ENDIF
			ENDIF
			
		BREAK
		
		CASE eCAS_PRISON_SCENE
			
			DEACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
			DEACTIVATE_CELEBRATION_AUDIO_CELEB_SCREEN_SCENE()
			DEACTIVATE_CELEBRATION_FADE_WHITE_SCENE()
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_STATS_PRISON_SCENE")
				START_AUDIO_SCENE("DLC_HEIST_POST_CELEB_STATS_PRISON_SCENE")
				#IF IS_DEBUG_BUILD
				PRINTLN("[SAC] - [CELEB_AUDIO] - started audio scene DLC_HEIST_POST_CELEB_STATS_PRISON_SCENE")
				DEBUG_PRINTCALLSTACK()
				#ENDIF
			ENDIF
			
		BREAK
		
		CASE eCAS_SERIES_A_SCENE
			
			DEACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
			DEACTIVATE_CELEBRATION_AUDIO_CELEB_SCREEN_SCENE()
			DEACTIVATE_CELEBRATION_FADE_WHITE_SCENE()
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_STATS_SERIES_A_SCENE")
				START_AUDIO_SCENE("DLC_HEIST_POST_CELEB_STATS_SERIES_A_SCENE")
				#IF IS_DEBUG_BUILD
				PRINTLN("[SAC] - [CELEB_AUDIO] - started audio scene DLC_HEIST_POST_CELEB_STATS_SERIES_A_SCENE")
				DEBUG_PRINTCALLSTACK()
				#ENDIF
			ENDIF
			
		BREAK
		
		CASE eCAS_FLEECA_SCENE
			
			DEACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
			DEACTIVATE_CELEBRATION_AUDIO_CELEB_SCREEN_SCENE()
			DEACTIVATE_CELEBRATION_FADE_WHITE_SCENE()
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_STATS_FLEECA_SCENE")
				START_AUDIO_SCENE("DLC_HEIST_POST_CELEB_STATS_FLEECA_SCENE")
				#IF IS_DEBUG_BUILD
				PRINTLN("[SAC] - [CELEB_AUDIO] - started audio scene DLC_HEIST_POST_CELEB_STATS_FLEECA_SCENE")
				DEBUG_PRINTCALLSTACK()
				#ENDIF
			ENDIF
			
		BREAK
		
		CASE eCAS_HUMANE_SCENE
			
			DEACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
			DEACTIVATE_CELEBRATION_AUDIO_CELEB_SCREEN_SCENE()
			DEACTIVATE_CELEBRATION_FADE_WHITE_SCENE()
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_POST_CELEB_STATS_HUMANE_SCENE")
				START_AUDIO_SCENE("DLC_HEIST_POST_CELEB_STATS_HUMANE_SCENE")
				#IF IS_DEBUG_BUILD
				PRINTLN("[SAC] - [CELEB_AUDIO] - started audio scene DLC_HEIST_POST_CELEB_STATS_HUMANE_SCENE")
				DEBUG_PRINTCALLSTACK()
				#ENDIF
			ENDIF
			
		BREAK
		
		CASE eCAS_FADE_BLACK_SCENE
			
			DEACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
			DEACTIVATE_CELEBRATION_AUDIO_SPECIFIC_STRAND_SCENES()
			DEACTIVATE_CELEBRATION_FADE_WHITE_SCENE()
			DEACTIVATE_CELEBRATION_AUDIO_CELEB_SCREEN_SCENE()
			
			ACTIVATE_CELEBRATION_AUDIO_FADE_BLACK_SCENE()
			
			// Failsafe for if we end up in here for too long.
			IF g_sMPTunables.bRun_Celebration_Blackscreen_Failsafe_Timer 
				IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sCelebAudioData.blackScreenFailTimer)
					START_NET_TIMER(g_TransitionSessionNonResetVars.sCelebAudioData.blackScreenFailTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.sCelebAudioData.blackScreenFailTimer, 50000)
						PRINTLN("[SAC] - [CELEB_AUDIO] - black screen failsafe timer hit, turning off celeb audio.")
						START_CELEBRATION_STATS_SCREEN_STOP_MUSIC_EVENT()
						SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)
						RESET_NET_TIMER(g_TransitionSessionNonResetVars.sCelebAudioData.blackScreenFailTimer)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC RESET_CELEBRATION_DATA(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	INT i
	SCALEFORM_INDEX sfTemp
	PED_INDEX pedTemp
	OBJECT_INDEX objTemp
	VEHICLE_INDEX vehTemp
	TEXT_LABEL_31 tl31Temp
	TEXT_LABEL_63 tl63Temp
	TEXT_LABEL_15 tl15Temp
	SHAPETEST_INDEX shapeTemp
	ENTITY_INDEX entityTemp
	
	sCelebrationData.sfForeground = sfTemp
	sCelebrationData.sfBackground = sfTemp
	sCelebrationData.sfCelebration = sfTemp
	sCelebrationData.pedWinnerClone = pedTemp
	sCelebrationData.objWinnerBackground = objTemp
	sCelebrationData.objPlinth = objTemp
	
	REPEAT COUNT_OF(sCelebrationData.pedWinnerClones) i
		sCelebrationData.pedWinnerClones[i] = pedTemp
		sCelebrationData.tl31_pedWinnerClonesNames[i] = tl31Temp
		sCelebrationData.bCalledForceAnimUpdate[i] = FALSE
		sCelebrationData.iVoiceStateCopy[i] = 0
	ENDREPEAT
	
	sCelebrationData.vehWinnerClone = vehTemp
	
	sCelebrationData.bCleanedUpEndWinnerScene = FALSE
	sCelebrationData.iEstimatedScreenDuration = 0
	sCelebrationData.iBlackRectangleAlpha = 0
	sCelebrationData.iCurrentCelebrationCam = 0
	sCelebrationData.iCurrentCelebrationCamModifier = 0
	sCelebrationData.iTimePlayedGesture = 0
	sCelebrationData.iWinnerPlayerID = 0				//These need to be stored at the start of the winner anim, in case the player leaves half-way through the screen.
	sCelebrationData.iWinnerBackgroundPlayerID = 0		//These need to be stored at the start of the winner anim, in case the player leaves half-way through the screen.
	sCelebrationData.iPlayedWinnerAnim = 0
	sCelebrationData.bTriggeredEarlyFlash = FALSE
	sCelebrationData.bUseFullBodyWinnerAnim = FALSE
	sCelebrationData.iNumPlayerFoundForWinnerScene = 0
	RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
	RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
	RESET_NET_TIMER(sCelebrationData.stNgFailSafeTimer)
	sCelebrationData.eCurrentStage = INT_TO_ENUM(CELEBRATION_SCREEN_STAGE, 0)
	sCelebrationData.stiCelebrationCam = shapeTemp
	sCelebrationData.iwinnerSyncedSceneId = 0
	// Commented out for B* 1771409 - uncomment to add back in.
//	INT iFakeMpGamerTagId[8]
//	STRING strFakeTagName[8]
//	BOOL bSetupFakeGamerTags
	sCelebrationData.iKnuckleAudioBitset = 0
	sCelebrationData.iKnuckAudioSoundId = -1
	sCelebrationData.entityProp = entityTemp
	REPEAT MAX_ARENA_PROPS i
		sCelebrationData.ArenaProps[i] = entityTemp
	ENDREPEAT
	sCelebrationData.iDisplayingLocalPlayerAsLoser = -1
	sCelebrationData.fAnimLength = 0.0
	sCelebrationData.eIdleAnimToUse = INT_TO_ENUM(CELEBRATION_WINNER_IDLE_ANIM, 0)
	sCelebrationData.iPlayWinnerAnimsStage = 0
	g_bCelebrationPreLoadTriggered = FALSE
	sCelebrationData.bPreLoadComplete = FALSE
	RESET_NET_TIMER(sCelebrationData.resetPreLoadForSepctatorTimer)
	RESET_NET_TIMER(sCelebrationData.timerDeathPostFxDelay) 
	sCelebrationData.bUseExtendedIdle = FALSE
	sCelebrationData.iLoadSceneStage = 0
	
	sCelebrationData.bToggleNames = FALSE
	sCelebrationData.celebInstructions = sfTemp
	sCelebrationData.iPlayerListProgress = 0
	
	sCelebrationData.scaleformStruct.bInitialised = FALSE
	REPEAT MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED i
		sCelebrationData.scaleformStruct.Buttons[i].ButtonSlot = tl63Temp
		sCelebrationData.scaleformStruct.Buttons[i].SecondaryButtonSlot = tl63Temp
		sCelebrationData.scaleformStruct.Buttons[i].sButtonSlotString = tl15Temp
		sCelebrationData.scaleformStruct.Buttons[i].iButtonSlotInt = 0
		sCelebrationData.scaleformStruct.Buttons[i].tlGamerTag = tl63Temp
	ENDREPEAT
	sCelebrationData.scaleformStruct.iBS_ButtonSlotHasInt = 0
	sCelebrationData.scaleformStruct.iBS_ButtonSlotHasPlayer = 0
	sCelebrationData.scaleformStruct.bRefreshInstructionalButtons = FALSE
	sCelebrationData.scaleformStruct.ButtonCount = 0
	sCelebrationData.scaleformStruct.iIntAsTime = 0
	sCelebrationData.scaleformStruct.AlignX = 0.0
	sCelebrationData.scaleformStruct.AlignY = 0.0
	sCelebrationData.scaleformStruct.SizeX = 0.0
	sCelebrationData.scaleformStruct.SizeY = 0.0
	sCelebrationData.scaleformStruct.fButtonWrap = 1.0
	
ENDPROC


FUNC BOOL ARENA_IS_FLAT_SOLO_PROP(ARENA_CELEB_ANIM_CLIPS eClip)

	SWITCH eClip
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_a_player_a
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_b_player_a
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_make_it_rain_b_player_b
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_smoking_b_player_b
		
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARENA_IS_FLAT_PAIRED_PROP(ARENA_CELEB_ANIM_CLIPS eClip)

	SWITCH eClip			
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_b
		
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_b
		
			RETURN TRUE
	ENDSWITCH

	
	RETURN FALSE
ENDFUNC

FUNC BOOL ANIM_IS_TROPHY(ARENA_CELEB_ANIM_CLIPS eClip)

	SWITCH eClip
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_a_1st		
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_b_1st
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_c_1st
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_d_1st
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_e_1st
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_f_1st
		
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_a
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL ANIM_IS_BEER(ARENA_CELEB_ANIM_CLIPS eClip)

	SWITCH eClip
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_a_player_a
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_b_player_a
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL ANIM_IS_MONEY(ARENA_CELEB_ANIM_CLIPS eClip)

	SWITCH eClip
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_make_it_rain_b_player_b
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL ANIM_IS_SMOKE(ARENA_CELEB_ANIM_CLIPS eClip)

	SWITCH eClip
		CASE ARENA_CELEB_FLAT_SOLO_PROPS_smoking_b_player_b
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL ANIM_IS_CHAMPAGNE(ARENA_CELEB_ANIM_CLIPS eClip)

	SWITCH eClip
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_1st		
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_1st
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_1st
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_1st
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_3rd
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_b
		
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL ANIM_IS_CONFETTI(ARENA_CELEB_ANIM_CLIPS eClip)

	SWITCH eClip
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_1st	
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_1st
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_1st
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_3rd
		
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

ENUM ARENA_CELEB_PTFX
	eARENA_PTFX_BEER=0,
	eARENA_PTFX_MONEY,
	eARENA_PTFX_CHAMPAGNE,
	eARENA_PTFX_CONFETTI,
	eARENA_PTFX_NONE
ENDENUM

FUNC BOOL ANIM_NEEDS_PTFX(ARENA_CELEB_ANIM_CLIPS eClip, ARENA_CELEB_PTFX &ePtfx)

	IF ANIM_IS_BEER(eClip)
	
		ePtfx = eARENA_PTFX_BEER
	
		RETURN TRUE
	ENDIF
	
	IF ANIM_IS_MONEY(eClip)
	
		ePtfx = eARENA_PTFX_MONEY
	
		RETURN TRUE
	ENDIF
	
//	IF ANIM_IS_SMOKE(eClip)
//	
//		RETURN TRUE
//	ENDIF
	
	IF ANIM_IS_CHAMPAGNE(eClip)
		
		ePtfx = eARENA_PTFX_CHAMPAGNE
	
		RETURN TRUE
	ENDIF

	IF ANIM_IS_CONFETTI(eClip)
		
		ePtfx = eARENA_PTFX_CONFETTI
	
		RETURN TRUE
	ENDIF
	
	ePtfx = eARENA_PTFX_NONE

	RETURN FALSE
ENDFUNC

FUNC STRING ARENA_PTFX_ASSET()

	RETURN "scr_xs_celebration"
ENDFUNC

FUNC STRING ARENA_PTFX_NAME(ARENA_CELEB_PTFX eFX)

	STRING sReturn = ""

	SWITCH eFX
		CASE eARENA_PTFX_BEER		sReturn = "scr_xs_beer_chug"			BREAK
		CASE eARENA_PTFX_MONEY		sReturn = "scr_xs_money_rain_celeb"		BREAK
		CASE eARENA_PTFX_CHAMPAGNE	sReturn = "scr_xs_champagne_spray"		BREAK
		CASE eARENA_PTFX_CONFETTI	sReturn = "scr_xs_confetti_burst"		BREAK
	ENDSWITCH

	RETURN sReturn
	
ENDFUNC

FUNC STRING ARENA_PTFX_TAG_START(ARENA_CELEB_PTFX eFX)

	STRING sReturn = ""

	SWITCH eFX
		CASE eARENA_PTFX_BEER		sReturn = "vfx_xs_beer_chug_start"						BREAK
		CASE eARENA_PTFX_MONEY		sReturn = "vfx_xs_raining_cash_celeb_start"				BREAK
		CASE eARENA_PTFX_CHAMPAGNE	sReturn = "vfx_xs_champagne_spray_celeb_start"			BREAK
		CASE eARENA_PTFX_CONFETTI	sReturn = "vfx_xs_confetti_fire"						BREAK
	ENDSWITCH

	RETURN sReturn
	
ENDFUNC

FUNC STRING ARENA_PTFX_TAG_STOP(ARENA_CELEB_PTFX eFX)

	STRING sReturn = ""

	SWITCH eFX
		CASE eARENA_PTFX_BEER		sReturn = "vfx_xs_beer_chug_stop"						BREAK
		CASE eARENA_PTFX_MONEY		sReturn = "vfx_xs_raining_cash_celeb_stop"				BREAK
		CASE eARENA_PTFX_CHAMPAGNE	sReturn = "vfx_xs_champagne_spray_celeb_stop"			BREAK
//		CASE eARENA_PTFX_CONFETTI	sReturn = "scr_xs_celebration"							BREAK
	ENDSWITCH

	RETURN sReturn
	
ENDFUNC

FUNC VECTOR ARENA_PTFX_POS(ARENA_CELEB_PTFX eFX)

	VECTOR vReturn 

	SWITCH eFX
		CASE eARENA_PTFX_BEER		vReturn = <<0.0,0.0,0.0>>	BREAK
		CASE eARENA_PTFX_MONEY		vReturn = <<0.0,0.0,0.0>>	BREAK
		CASE eARENA_PTFX_CHAMPAGNE	vReturn = <<0.0,0.0,0.3>>	BREAK
		CASE eARENA_PTFX_CONFETTI	vReturn = <<0.0,0.0,0.0>>	BREAK
	ENDSWITCH

	RETURN vReturn
	
ENDFUNC

FUNC VECTOR ARENA_PTFX_ROT(ARENA_CELEB_PTFX eFX)
	VECTOR vReturn 

	SWITCH eFX
		CASE eARENA_PTFX_BEER		vReturn = <<0.0,0.0,0.0>>		BREAK
		CASE eARENA_PTFX_MONEY		vReturn = <<0.0, 90.0, 0.0>>	BREAK
		CASE eARENA_PTFX_CHAMPAGNE	vReturn = <<0.0,0.0,0.0>>		BREAK
		CASE eARENA_PTFX_CONFETTI	vReturn = <<0.0,0.0,0.0>>		BREAK
	ENDSWITCH

	RETURN vReturn
ENDFUNC

FUNC PED_BONETAG BONE_FOR_PROP(ARENA_CELEB_ANIM_CLIPS eClip)

	IF ANIM_IS_CONFETTI(eClip)
	OR ANIM_IS_MONEY(eClip)
		RETURN BONETAG_PH_L_HAND
	ENDIF
	
	RETURN BONETAG_PH_R_HAND
ENDFUNC

FUNC PED_BONETAG BONE_FOR_PTFX(ARENA_CELEB_ANIM_CLIPS eClip)

	IF ANIM_IS_CONFETTI(eClip)
	OR ANIM_IS_MONEY(eClip)
		RETURN BONETAG_PH_L_HAND
	ENDIF
	
	IF ANIM_IS_BEER(eClip)
	
		RETURN BONETAG_HEAD
	ENDIF
	
	RETURN BONETAG_PH_R_HAND
ENDFUNC

PROC MAINTAIN_ARENA_CELEB_PTFX(CELEB_SERVER_DATA &sCelebServer, CELEBRATION_SCREEN_DATA &sCelebrationData, PED_INDEX pedWinnerClone)

	ARENA_CELEB_PTFX ePtfx = eARENA_PTFX_NONE
	STRING sClip 
	STRING sAsset = ARENA_PTFX_ASSET()
	STRING sStart = ARENA_PTFX_TAG_START(ePtfx)
	STRING sEnd	= ARENA_PTFX_TAG_STOP(ePtfx)
	VECTOR vPos, vRot
//	FLOAT fScale = 0.0
	
	REQUEST_NAMED_PTFX_ASSET(sAsset)
	ENTITY_INDEX entProp

	IF HAS_NAMED_PTFX_ASSET_LOADED(sAsset)
		
		INT i

		REPEAT NUM_CELEB_PLAYERS_TO_ANIMATE(sCelebServer) i
		
			IF ANIM_NEEDS_PTFX(sCelebServer.eArenaClips[i], ePtfx)
			
				IF ePtfx != eARENA_PTFX_NONE
				
					sClip 	= ARENA_PTFX_NAME(ePtfx)
					vPos 	= ARENA_PTFX_POS(ePtfx)
					vRot 	= ARENA_PTFX_ROT(ePtfx)
					sStart 	= ARENA_PTFX_TAG_START(ePtfx)
					sEnd 	= ARENA_PTFX_TAG_STOP(ePtfx)
					
					IF DOES_ENTITY_EXIST(sCelebrationData.ArenaProps[i])
						entProp = sCelebrationData.ArenaProps[i]
					ENDIF
					
					PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX  A - i      = ", i)
					PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX  A - sClip  = ", sClip)
					PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX  A - vPos   = ", vPos)
					PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX  A - vRot   = ", vRot)
					PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX  A - sStart = ", sStart)
					PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX  A - sEnd   = ", sEnd)
					
					// Stop the vfx
					IF NOT IS_STRING_NULL_OR_EMPTY(sEnd)
					AND HAS_ANIM_EVENT_FIRED(pedWinnerClone, GET_HASH_KEY(sEnd))
					
						IF sCelebrationData.PTFXID_Loop[i] != INT_TO_NATIVE(PTFX_ID, -1)
						AND DOES_PARTICLE_FX_LOOPED_EXIST(sCelebrationData.PTFXID_Loop[i])
						
							SWITCH ePtfx
								CASE eARENA_PTFX_MONEY
									IF DOES_ENTITY_EXIST(entProp)
										DELETE_ENTITY(entProp)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX  DELETE_ENTITY, prop, i = ", i)
									ENDIF
								BREAK
							ENDSWITCH
						
							STOP_PARTICLE_FX_LOOPED(sCelebrationData.PTFXID_Loop[i])
							PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX  STOP_PARTICLE_FX_LOOPED - i      = ", i)
						ENDIF
					ELSE
						// Start the vfx
						IF (sCelebrationData.PTFXID_Loop[i] = INT_TO_NATIVE(PTFX_ID, -1))
							PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX  B - i      = ", i, " sStart = ", sStart)
							
							SWITCH ePtfx
								CASE eARENA_PTFX_MONEY
									USE_PARTICLE_FX_ASSET(sAsset) 
									sCelebrationData.PTFXID_Loop[i] = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(sClip, pedWinnerClone, vPos, vRot, GET_PED_BONE_INDEX(pedWinnerClone, BONE_FOR_PTFX(sCelebServer.eArenaClips[i])))
									
									PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_MONEY - i      = ", i)
									PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_MONEY - sClip  = ", sClip)
									PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_MONEY - vPos   = ", vPos)
									PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_MONEY - sStart = ", sStart)
									PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_MONEY - sEnd   = ", sEnd)
								BREAK
								
								CASE eARENA_PTFX_BEER	
									IF DOES_ENTITY_EXIST(entProp)
									AND HAS_ANIM_EVENT_FIRED(pedWinnerClone, GET_HASH_KEY(sStart))

										USE_PARTICLE_FX_ASSET(sAsset) 
										sCelebrationData.PTFXID_Loop[i] = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(sClip, pedWinnerClone, vPos, vRot, GET_PED_BONE_INDEX(pedWinnerClone, BONE_FOR_PTFX(sCelebServer.eArenaClips[i])))
										
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_BEER - i      = ", i)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_BEER - sClip  = ", sClip)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_BEER - vPos   = ", vPos)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_BEER - sStart = ", sStart)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_BEER - sEnd   = ", sEnd)
									ENDIF
								BREAK
								CASE eARENA_PTFX_CHAMPAGNE	
									IF DOES_ENTITY_EXIST(entProp)
									AND HAS_ANIM_EVENT_FIRED(pedWinnerClone, GET_HASH_KEY(sStart))

										USE_PARTICLE_FX_ASSET(sAsset) 
										sCelebrationData.PTFXID_Loop[i] = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(sClip, entProp, vPos, vRot, GET_ENTITY_BONE_INDEX_BY_NAME(entProp, "VFX"))
										
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_CHAMPAGNE,  - i      = ", i)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_CHAMPAGNE,  - sClip  = ", sClip)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_CHAMPAGNE,  - vPos   = ", vPos)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_CHAMPAGNE,  - sStart = ", sStart)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_CHAMPAGNE,  - sEnd   = ", sEnd)
									ENDIF
								BREAK
								CASE eARENA_PTFX_CONFETTI	
									IF DOES_ENTITY_EXIST(entProp)
									AND HAS_ANIM_EVENT_FIRED(pedWinnerClone, GET_HASH_KEY(sStart))
									
										USE_PARTICLE_FX_ASSET(sAsset) 
										START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(sClip, entProp, vPos, vRot)
										
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_CONFETTI - i      = ", i)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_CONFETTI - sClip  = ", sClip)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_CONFETTI - vPos   = ", vPos)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_CONFETTI - sStart = ", sStart)
										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - eARENA_PTFX_CONFETTI - sEnd   = ", sEnd)
									ENDIF
								BREAK									
							ENDSWITCH
//						ELSE
//							IF DOES_PARTICLE_FX_LOOPED_EXIST(sCelebrationData.PTFXID_Loop[i])
//							
//								SWITCH ePtfx
//									CASE eARENA_PTFX_CHAMPAGNE
//										
//										INT iTempTime
//										iTempTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCelebrationData.stFXTimer[i])
//										
//										IF IS_INT_IN_RANGE(iTempTime, 1000, 2000)
//											fScale = 0.4
//										ELIF IS_INT_IN_RANGE(iTempTime, 2000, 3000)
//											fScale = 0.8
//										ELSE
//											fScale = 1.0
//										ENDIF
//										
//										SET_PARTICLE_FX_LOOPED_EVOLUTION(sCelebrationData.PTFXID_Loop[i], "fade", fScale)
//										PRINTLN("[NETCELEBRATION] [ARENA]   - MAINTAIN_ARENA_CELEB_PTFX - iTempTime  = ", iTempTime)
//									BREAK
//								ENDSWITCH
//							
//							ENDIF

						ENDIF
					ENDIF

				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

ENDPROC

FUNC BOOL ANIM_IS_PODIUM(ARENA_CELEB_ANIM_CLIPS eClip)

	SWITCH eClip

		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_a_1st					
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_b_1st					
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_c_1st					
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_d_1st					
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_e_1st					
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_f_1st					
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_cheer_a_1st			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_clapping_a_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_cocky_a_1st			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_crowd_point_a_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_dance_a_1st			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_dance_b_1st			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_finger_guns_a_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_finger_guns_b_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_fist_pump_a_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_a_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_b_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_flip_off_c_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_hands_air_b_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_hands_air_c_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_make_noise_a_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_a_1st			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_b_1st			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_regal_c_1st			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_shrug_off_a_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cheer_a_2nd			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cocky_a_2nd			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_crowd_point_a_2nd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_finger_guns_a_2nd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_finger_guns_b_2nd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_a_2nd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_b_2nd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_flip_off_c_2nd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_make_noise_a_2nd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_shrug_off_a_2nd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_clapping_a_3rd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_dance_a_3rd			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_dance_b_3rd			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_fist_pump_a_3rd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_hands_air_b_3rd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_hands_air_c_3rd		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_regal_a_3rd			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_regal_b_3rd			
		CASE ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_regal_c_3rd			
		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_PAIR_1ST_air_slap_a_1st		
		CASE ARENA_CELEB_PODIUM_NO_PROPS_PAIR_2ND_air_slap_a_2nd		
		
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_1st				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_1st				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_1st				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_1st				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_1st			
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_1st			
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_1st			
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_2nd				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_2nd				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_2nd				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_2nd				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_2nd			
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_2nd			
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_2nd			
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_3rd				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_3rd				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_3rd				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_3rd				
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_3rd			
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_3rd			
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_3rd			

			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES TROPHY_MODEL(ARENA_CELEB_ANIM_CLIPS eClip)

	SWITCH eClip
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_a_1st		
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_d_1st
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_b_1st		
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_e_1st	
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_c_1st		
		CASE ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_f_1st
			SWITCH g_FMMC_STRUCT.sArenaInfo.iArena_Theme
				CASE ARENA_THEME_CONSUMERWASTELAND 			
						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_trophy_single_01a"))
				CASE ARENA_THEME_DYSTOPIAN			
						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_trophy_single_01b"))
				CASE ARENA_THEME_SCIFI 					
						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_trophy_single_01c"))
			ENDSWITCH
		BREAK
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_a	
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_a	
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_a	
			SWITCH g_FMMC_STRUCT.sArenaInfo.iArena_Theme
				CASE ARENA_THEME_DYSTOPIAN			
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_trophy_double_01a"))
				CASE ARENA_THEME_SCIFI				
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_trophy_double_01b"))
				CASE ARENA_THEME_CONSUMERWASTELAND					
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_trophy_double_01c"))
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_trophy_single_01a"))
ENDFUNC

FUNC MODEL_NAMES PODIUM_MODEL()

	MODEL_NAMES modelReturn

	SWITCH g_FMMC_STRUCT.sArenaInfo.iArena_Theme
	
		CASE ARENA_THEME_DYSTOPIAN			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_podium_01a"))			
		CASE ARENA_THEME_SCIFI				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_podium_02a"))
		CASE ARENA_THEME_CONSUMERWASTELAND	
			modelReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_podium_03a"))
			IF modelReturn != DUMMY_MODEL_FOR_SCRIPT
				RETURN modelReturn
			ENDIF
		BREAK

	ENDSWITCH

	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_podium_01a"))
ENDFUNC

FUNC VECTOR PODIUM_COORDS()
	
	RETURN <<2957.0017, -3866.8635, 140.0026>>
ENDFUNC

FUNC FLOAT PODIUM_HEADING()
	
	RETURN 43.504
ENDFUNC

FUNC BOOL CREATE_PODIUM(ENTITY_INDEX &entity)
	
	#IF IS_DEBUG_BUILD
	INT iTemp
	#ENDIF
	
	VECTOR vCreationCoords = PODIUM_COORDS()
	MODEL_NAMES modelToMake = PODIUM_MODEL()
	FLOAT fPropHeading = PODIUM_HEADING()
	
	IF DOES_ENTITY_EXIST(entity)
		RETURN TRUE
	ENDIF
	
	REQUEST_MODEL(modelToMake)
	IF HAS_MODEL_LOADED(modelToMake)
		IF CAN_REGISTER_MISSION_OBJECTS(1)
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+1)
			
			#IF IS_DEBUG_BUILD
			iTemp = GET_NUM_RESERVED_MISSION_OBJECTS()
			PRINTLN("[NETCELEBRATION] - [Paired_Anims] - CREATE_PODIUM - numreserved objects after call to RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+1) = ", iTemp)
			#ENDIF
		
			entity = CREATE_OBJECT(modelToMake, vCreationCoords, FALSE, FALSE)
			SET_ENTITY_HEADING(entity, fPropHeading)

			PRINTLN("[NETCELEBRATION] - [Paired_Anims] - CREATE_PODIUM - created prop at vCreationCoords = ", vCreationCoords, " fPropHeading = ", fPropHeading)
		ELSE
			PRINTLN("[NETCELEBRATION] - [Paired_Anims] - CREATE_PODIUM - CAN_REGISTER_MISSION_OBJECTS(1) = FALSE, cannot create prop.")
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("[NETCELEBRATION] - [Paired_Anims] - CREATE_PODIUM - HAS_MODEL_LOADED = FALSE, waiting on model to load.")
	ENDIF
	
	RETURN DOES_ENTITY_EXIST(entity)
ENDFUNC

PROC CREATE_ARENA_LIGHT_RIG(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	IF NOT DOES_ENTITY_EXIST(sCelebrationData.objectLightRig)
	
		MODEL_NAMES mnLightRig = EXILE1_LIGHTRIG//LIGHT_CAR_RIG
	
		IF REQUEST_LOAD_MODEL(mnLightRig)
	
			IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClones[0])
	
				VECTOR vRigPos = GET_ENTITY_COORDS(sCelebrationData.pedWinnerClones[0])
				
				PRINTLN("[NETCELEBRATION] CREATE_ARENA_LIGHT_RIG - Creating a light rig for the arena ", vRigPos)
				
				sCelebrationData.objectLightRig = CREATE_OBJECT(mnLightRig, vRigPos, FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(mnLightRig)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_ISLAND_LIGHT_PROP(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	IF NOT DOES_ENTITY_EXIST(sCelebrationData.oiIslandLightObject)
	
		MODEL_NAMES mnLightProp = INT_TO_ENUM(MODEL_NAMES, HASH("Reh_Prop_Celebration_LP"))
	
		IF REQUEST_LOAD_MODEL(mnLightProp)
	
			IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClones[0])
	
				VECTOR vPropPos = GET_ENTITY_COORDS(sCelebrationData.pedWinnerClones[0])
				vPropPos.z = vPropPos.z - 1.05
				
				PRINTLN("[NETCELEBRATION] CREATE_ISLAND_LIGHT_PROP - Creating a light prop for the island ", vPropPos)
				
				sCelebrationData.oiIslandLightObject = CREATE_OBJECT(mnLightProp, vPropPos, FALSE)

				SET_ENTITY_HEADING(sCelebrationData.oiIslandLightObject, 255.0)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(mnLightProp)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//TWEAK_FLOAT LRIG_X 0.0
//TWEAK_FLOAT LRIG_Y 0.0
//TWEAK_FLOAT LRIG_Z 0.0

PROC POSITION_ARENA_LIGHT_RIG(CELEB_SERVER_DATA &sCelebServer, CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	IF DOES_ENTITY_EXIST(sCelebrationData.objectLightRig)		
//		IF NOT IS_BIT_SET(sCelebrationData.iBitSet, ciCELEB_CLIENT_BIT_POSITIONED_LIGHT)
			IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClones[0])
			AND NOT IS_ENTITY_DEAD(sCelebrationData.pedWinnerClones[0])

				VECTOR vPlayerRot = GET_ENTITY_ROTATION(sCelebrationData.pedWinnerClones[0])
				
				VECTOR vPos 
				
				IF ARENA_IS_PODIUM(sCelebServer)
					vPos = PODIUM_COORDS()
					vPos.z += 6.500
					SET_ENTITY_COORDS(sCelebrationData.objectLightRig, vPos, FALSE)
					SET_ENTITY_ROTATION(sCelebrationData.objectLightRig, <<0, 0, (vPlayerRot.z - 100.00)>>)
				ELSE
					vPos = <<2966.5071, -3875.1870, 141.0027>>
//					vPos.z += LRIG_Z
					SET_ENTITY_COORDS(sCelebrationData.objectLightRig, vPos, FALSE)
//					SET_ENTITY_ROTATION(sCelebrationData.objectLightRig, <<0, 0, vPlayerRot.z>>)
				ENDIF
				
				PRINTLN("[CORONA] POSITION_ARENA_LIGHT_RIG - Set the light rig to player:  vPos = ", vPos, " vPlayerRot = ", vPlayerRot)
				
//				SET_BIT(sCelebrationData.iBitSet, ciCELEB_CLIENT_BIT_POSITIONED_LIGHT)

			ENDIF
//		ENDIF
	ENDIF
ENDPROC

FUNC MODEL_NAMES ARENA_PROP_MODEL(CELEB_SERVER_DATA &sCelebServer, INT i)
	
	IF ANIM_IS_BEER(sCelebServer.eArenaClips[i])
	
		RETURN PROP_BEER_LOGGER
	ELIF ANIM_IS_MONEY(sCelebServer.eArenaClips[i])
	
		RETURN PROP_CASH_PILE_02
		
	ELIF ANIM_IS_SMOKE(sCelebServer.eArenaClips[i])
	
		RETURN prop_cs_ciggy_01b
	ENDIF
	
	IF ANIM_IS_TROPHY(sCelebServer.eArenaClips[i])
		RETURN TROPHY_MODEL(sCelebServer.eArenaClips[i])
	ENDIF
	
	IF ANIM_IS_CHAMPAGNE(sCelebServer.eArenaClips[i])
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Champ_01b"))
	ENDIF
	
	IF ANIM_IS_CONFETTI(sCelebServer.eArenaClips[POS_1ST])
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Confetti_Cannon"))
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC VECTOR ARENA_PROP_CREATION_COORDS(INT i)

	SWITCH i
		CASE POS_1ST	RETURN <<2967.0283, -3875.9526, 139.9974>>
		CASE POS_2ND 	RETURN <<2967.0283, -3875.9526, 139.9974>>
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[NETCELEBRATION] [ARENA] ERROR ARENA_PROP_CREATION_COORDS, vector zero ")
		SCRIPT_ASSERT("ARENA_PROP_CREATION_COORDS is vector zero ")
	#ENDIF
	
	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC FLOAT ARENA_PROP_HEADING(INT i)

	SWITCH i
		CASE 0	RETURN 48.00
		CASE 1 	RETURN 48.00
	ENDSWITCH
	
	RETURN 48.00
ENDFUNC

// These functions decide where in ARENA_CELEB_ANIM_CLIPS to randomly grab anims for each player 
FUNC INT MIN_NUM_ARENA_CLIPS(CELEB_SERVER_DATA &sCelebServer, INT iPosition)

	SWITCH sCelebServer.aArenaAnimType
	
		CASE CELEBRATION_ARENA_ANIM_PODIUM
	
			SWITCH iPosition
				CASE POS_1ST	RETURN ENUM_TO_INT(ARENA_CELEB_PODIUM_PROPS_SOLO_trophy_a_1st)
				CASE POS_2ND	RETURN ENUM_TO_INT(ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cheer_a_2nd)
				CASE POS_3RD	RETURN ENUM_TO_INT(ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_clapping_a_3rd)
			ENDSWITCH
			
		BREAK
		
		CASE CELEBRATION_ARENA_ANIM_FLAT
			
			BOOL bPrioritisePaired
		
			IF sCelebServer.iNumPlayersFound >= MAX_FLAT
				bPrioritisePaired = TRUE
			ENDIF
		
			SWITCH iPosition
				CASE POS_1ST	
				
					IF bPrioritisePaired
						RETURN ENUM_TO_INT(ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_a)
					ENDIF
					
					RETURN ENUM_TO_INT(ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_a_player_a)
				CASE POS_2ND	
				
					IF bPrioritisePaired
						RETURN ENUM_TO_INT(ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_b)
					ENDIF
					
					RETURN ENUM_TO_INT(ARENA_CELEB_FLAT_SOLO_PROPS_make_it_rain_b_player_b)
			ENDSWITCH
		
		BREAK
	ENDSWITCH

	RETURN 0
ENDFUNC

FUNC INT MAX_NUM_ARENA_CLIPS(CELEB_SERVER_DATA &sCelebServer, INT iPosition)

	SWITCH sCelebServer.aArenaAnimType
	
		CASE CELEBRATION_ARENA_ANIM_PODIUM
	
			SWITCH iPosition
				CASE POS_1ST	
				
					IF sCelebServer.iNumPlayersFound < MAX_PODIUM	// Don't do paired anims unless we have a full podium
					#IF IS_DEBUG_BUILD
					AND NOT GET_COMMANDLINE_PARAM_EXISTS("celebArena")
					#ENDIF
						RETURN ENUM_TO_INT(ARENA_CELEB_PODIUM_NO_PROPS_PAIR_1ST_air_slap_a_1st)
					ENDIF
				
					RETURN ENUM_TO_INT(ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cheer_a_2nd)
					
				CASE POS_2ND	RETURN ENUM_TO_INT(ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_clapping_a_3rd)
				CASE POS_3RD	RETURN ENUM_TO_INT(ARENA_CELEB_PODIUM_NO_PROPS_PAIR_2ND_air_slap_a_2nd)
			ENDSWITCH
			
		BREAK
		
		CASE CELEBRATION_ARENA_ANIM_FLAT
		
			BOOL bPrioritisePaired
		
			IF sCelebServer.iNumPlayersFound >= MAX_FLAT
				bPrioritisePaired = TRUE
			ENDIF
		
			SWITCH iPosition
				CASE POS_1ST	
				
					IF bPrioritisePaired
						RETURN ENUM_TO_INT(ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_b)
					ENDIF
				
					RETURN ENUM_TO_INT(ARENA_CELEB_FLAT_SOLO_PROPS_make_it_rain_b_player_b)
					
				CASE POS_2ND	
				
					IF bPrioritisePaired
						RETURN ENUM_TO_INT(ARENA_CELEB_FLAT_SOLO_PROPS_chug_beer_a_player_a)
					ENDIF
				
					RETURN ENUM_TO_INT(ARENA_CELEB_MAX)

			ENDSWITCH
		
		BREAK
	ENDSWITCH

	RETURN 0
ENDFUNC

#IF IS_DEBUG_BUILD
INT iPlayer1Clip = -1
INT iPlayer2Clip = -1
INT iPlayer3Clip = -1
#ENDIF

FUNC BOOL IS_ARENA_ANIM_PAIRED(ARENA_CELEB_ANIM_CLIPS eClip)

	SWITCH eClip

		CASE ARENA_CELEB_PODIUM_NO_PROPS_PAIR_1ST_air_slap_a_1st	
		CASE ARENA_CELEB_PODIUM_NO_PROPS_PAIR_2ND_air_slap_a_2nd	
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_1st						
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_1st					
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_1st
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_1st					
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_1st			
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_1st
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_1st		
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_2nd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_3rd	
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_3rd
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_a
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_a
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_b

			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARENA_ANIM_PAIRED_WITH_3RD_POS(ARENA_CELEB_ANIM_CLIPS eClip)
	SWITCH eClip
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_1st
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_1st
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_1st
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_1st
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC ARENA_CELEB_ANIM_CLIPS PAIRED_ARENA_ANIM(ARENA_CELEB_ANIM_CLIPS eClip, INT iPlayer)
	SWITCH eClip
		CASE ARENA_CELEB_PODIUM_NO_PROPS_PAIR_1ST_air_slap_a_1st	RETURN ARENA_CELEB_PODIUM_NO_PROPS_PAIR_2ND_air_slap_a_2nd

		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_1st			IF iPlayer = POS_2ND RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_2nd 		ENDIF 		RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_cham_chug_a_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_1st			IF iPlayer = POS_2ND RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_2nd 		ENDIF 		RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_a_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_1st			IF iPlayer = POS_2ND RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_2nd 		ENDIF 		RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_b_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_1st			IF iPlayer = POS_2ND RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_2nd 		ENDIF 		RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_cham_spray_c_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_1st		IF iPlayer = POS_2ND RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_2nd 	ENDIF 		RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_a_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_1st		IF iPlayer = POS_2ND RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_2nd 	ENDIF 		RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_c_3rd
		CASE ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_1st		IF iPlayer = POS_2ND RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_2nd 	ENDIF 		RETURN ARENA_CELEB_PODIUM_PROPS_PAIR_confetti_canon_d_3rd
		
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_a			RETURN ARENA_CELEB_FLAT_PAIR_PROPS_cham_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_a			RETURN ARENA_CELEB_FLAT_PAIR_PROPS_cham_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_a			RETURN ARENA_CELEB_FLAT_PAIR_PROPS_cham_c_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_a			RETURN ARENA_CELEB_FLAT_PAIR_PROPS_cham_d_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_a			RETURN ARENA_CELEB_FLAT_PAIR_PROPS_trophy_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_a			RETURN ARENA_CELEB_FLAT_PAIR_PROPS_trophy_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_a			RETURN ARENA_CELEB_FLAT_PAIR_PROPS_trophy_c_player_b
		

		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_a		RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_baseball_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_a			RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_daps_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_a	RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_did_you_see_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_a	RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_a	RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_high_five_c_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_a			RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_hype_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_a		RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_a		RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_laugh_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_a	RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_b_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_a	RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_piggyback_c_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_a		RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_the_bird_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_a		RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_a_player_b
		CASE ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_a		RETURN ARENA_CELEB_FLAT_PAIR_NO_PROPS_this_guy_b_player_b
		
	ENDSWITCH
	
	RETURN ARENA_CELEB_INVALID
ENDFUNC

// Do we have to manually attach the prop instead of playing an anim on it?
FUNC BOOL ANIM_NEEDS_ATTACHED(CELEB_SERVER_DATA &sCelebServer, ARENA_CELEB_ANIM_CLIPS eClip)

	UNUSED_PARAMETER(sCelebServer)

	IF ARENA_IS_FLAT_PAIRED_PROP(eClip)
		RETURN FALSE
	ENDIF

	IF ARENA_IS_FLAT_SOLO_PROP(eClip)
		RETURN TRUE
	ENDIF

	IF ANIM_IS_TROPHY(eClip)
		RETURN TRUE
	ENDIF

	IF ANIM_IS_CONFETTI(eClip) 
//	AND SAFE_FOR_HEELS(eClip)
		RETURN TRUE
	ENDIF
	
//	IF ANIM_IS_CHAMPAGNE(eClip) 
//	AND SAFE_FOR_HEELS(eClip)
//		RETURN TRUE
//	ENDIF

	RETURN FALSE
ENDFUNC

// Do we need to play an anim on the actual prop too?
FUNC BOOL PROP_NEEDS_ANIMATED(CELEB_SERVER_DATA &sCelebServer, ARENA_CELEB_ANIM_CLIPS eClip)

	UNUSED_PARAMETER(sCelebServer)

	IF ARENA_IS_FLAT_SOLO_PROP(eClip)
		RETURN FALSE
	ENDIF

	IF ARENA_IS_FLAT_PAIRED_PROP(eClip)
		RETURN TRUE
	ENDIF
	
	IF ANIM_IS_CHAMPAGNE(eClip)
		RETURN TRUE
	ENDIF
	
//	IF ANIM_IS_CONFETTI(eClip) 
//	AND NOT SAFE_FOR_HEELS(eClip)
//		RETURN TRUE
//	ENDIF
	
//	IF ANIM_IS_CHAMPAGNE(eClip) 
//	AND NOT SAFE_FOR_HEELS(eClip)
//		RETURN TRUE
//	ENDIF

	RETURN FALSE
ENDFUNC

PROC SERVER_SET_NUM_ARENA_PROPS_REQUIRED(CELEB_SERVER_DATA &sCelebServer, INT i)

	INT iNumProps = 0

	IF ANIM_IS_TROPHY(sCelebServer.eArenaClips[POS_1ST])
		iNumProps += 1
	ENDIF
	
	IF ANIM_IS_CHAMPAGNE(sCelebServer.eArenaClips[POS_1ST])
		iNumProps += 1
	ENDIF
	
	IF ANIM_IS_CONFETTI(sCelebServer.eArenaClips[i])
		iNumProps += 1
	ENDIF
	
	IF ANIM_IS_BEER(sCelebServer.eArenaClips[POS_1ST])
		iNumProps += 1
	ENDIF
		
	IF ANIM_IS_MONEY(sCelebServer.eArenaClips[i])
		iNumProps += 1
	ENDIF
			
	IF ANIM_IS_SMOKE(sCelebServer.eArenaClips[i])
		iNumProps += 1
	ENDIF
	
	// Cap
	IF iNumProps > MAX_ARENA_PROPS
		iNumProps = MAX_ARENA_PROPS
	ENDIF
		
	sCelebServer.iNumPropsRequired = iNumProps
		
	PRINTLN("SERVER_SET_NUM_ARENA_PROPS_REQUIRED = ", sCelebServer.iNumPropsRequired)
ENDPROC

FUNC BOOL ARENA_ANIMS_READY(CELEB_SERVER_DATA &sCelebServer)

	TEXT_LABEL_63 tl63_AnimDict
	TEXT_LABEL_63 tl63_AnimName

	IF IS_BIT_SET(sCelebServer.iBitSet, ciANIMS_GRABBED)
	
		BOOL bWaitingOnAnimsLoading
	
		INT iLoop
		REPEAT NUM_CELEB_PLAYERS_TO_ANIMATE(sCelebServer) iLoop
			GET_ARENA_CELEB_ANIMS(sCelebServer, tl63_AnimDict, tl63_AnimName, iLoop)
			IF NOT IS_STRING_NULL_OR_EMPTY(tl63_AnimDict)
				REQUEST_ANIM_DICT(tl63_AnimDict)
				PRINTLN("[NETCELEBRATION] [ARENA] - initial anim still loading - ARENA_ANIMS_READY, tl63_AnimDict = ", tl63_AnimDict, " tl63_AnimName = ", tl63_AnimName, " at ", NATIVE_TO_INT(GET_NETWORK_TIME()), " iLoop = ", iLoop)
				IF NOT HAS_ANIM_DICT_LOADED(tl63_AnimDict)
					bWaitingOnAnimsLoading = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bWaitingOnAnimsLoading
			PRINTLN("[NETCELEBRATION] [ARENA] ARENA_ANIMS_READY  - FALSE, bWaitingOnAnimsLoading ")
			RETURN FALSE
		ENDIF
		
	ELSE
		PRINTLN("[NETCELEBRATION] [ARENA] ARENA_ANIMS_READY  - ciANIMS_GRABBED = FALSE ")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[NETCELEBRATION] [ARENA] ARENA_ANIMS_READY  - TRUE ")

	RETURN TRUE

ENDFUNC

PROC SERVER_CHOOSE_ARENA_ANIMATIONS(CELEB_SERVER_DATA &sCelebServer, ARENA_ANIM_SET aArenaAnimType, INT iNumPlayersFound)
	
	IF NOT IS_BIT_SET(sCelebServer.iBitSet, ciANIMS_GRABBED)
	
		IF iNumPlayersFound <= 0
			EXIT
		ENDIF
		
		IF sCelebServer.iNumPlayersFound != iNumPlayersFound
			sCelebServer.iNumPlayersFound = iNumPlayersFound
			PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS -  sCelebServer.iNumPlayersFound = ", sCelebServer.iNumPlayersFound)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF iArenaCelebNumPlayers != -1
				sCelebServer.iNumPlayersFound = iArenaCelebNumPlayers
				PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS -  IS_DEBUG_BUILD OVERRIDE sCelebServer.iNumPlayersFound = ", sCelebServer.iNumPlayersFound)
			ENDIF
		#ENDIF
		
		BOOL bPodiumMatch = sCelebServer.aArenaAnimType = CELEBRATION_ARENA_ANIM_PODIUM
		
		// This just allows me to randomise props chosen
		IF sCelebServer.iRandomPropValue = -1	
			sCelebServer.iRandomPropValue = GET_RANDOM_INT_IN_RANGE(0, ciRND_MAX)
			PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - sCelebServer.iRandomPropValue = ", sCelebServer.iRandomPropValue)
		ENDIF
	
		// Choose the anim broad categories, determines anim dictionary
		IF sCelebServer.aArenaAnimType = CELEBRATION_ARENA_ANIM_INVALID
			sCelebServer.aArenaAnimType = aArenaAnimType
			PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - ARENA_ANIM_SET = ", ARENA_ANIM_SET_DBG(sCelebServer.aArenaAnimType))
		ENDIF
		
		// Clip selection:
		
		// Allow debug override
		#IF IS_DEBUG_BUILD
		IF iPlayer1Clip != -1
			sCelebServer.eArenaClips[POS_1ST] = INT_TO_ENUM(ARENA_CELEB_ANIM_CLIPS, iPlayer1Clip) 
			PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - DEBUG_OVERRIDE, ARENA_CLIP_NAME_DBG = ", ARENA_CLIP_NAME_DBG(sCelebServer.eArenaClips[POS_1ST]))
		ENDIF
		IF iPlayer2Clip != -1
			sCelebServer.eArenaClips[POS_2ND] = INT_TO_ENUM(ARENA_CELEB_ANIM_CLIPS, iPlayer2Clip) 
			PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - DEBUG_OVERRIDE, ARENA_CLIP_NAME_DBG = ", ARENA_CLIP_NAME_DBG(sCelebServer.eArenaClips[POS_2ND]))
		ENDIF
		IF iPlayer3Clip != -1
			sCelebServer.eArenaClips[POS_3RD] = INT_TO_ENUM(ARENA_CELEB_ANIM_CLIPS, iPlayer3Clip) 
			PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - DEBUG_OVERRIDE, ARENA_CLIP_NAME_DBG = ", ARENA_CLIP_NAME_DBG(sCelebServer.eArenaClips[POS_3RD]))
		ENDIF
		#ENDIF

		// Choose appropriate anim clips for each player
		INT i
		INT iMinCeleb, iMaxCeleb

		REPEAT NUM_CELEB_PLAYERS_TO_ANIMATE(sCelebServer) i
			IF sCelebServer.eArenaClips[i] = ARENA_CELEB_INVALID
			
				PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS -  i = ", i)
			
				// Match up paired anims
				IF IS_ARENA_ANIM_PAIRED(sCelebServer.eArenaClips[POS_1ST])								// If player 1 needs a pair, match up the rest
				AND i != POS_1ST																		// It's all based on player 1 anims so no need to pair 
				AND PAIRED_ARENA_ANIM(sCelebServer.eArenaClips[POS_1ST], i) != ARENA_CELEB_INVALID
				AND ( i != POS_3RD OR bPodiumMatch OR (i = POS_3RD AND IS_ARENA_ANIM_PAIRED_WITH_3RD_POS(sCelebServer.eArenaClips[POS_1ST]) ))													// Only 2 players for flat team modes
				
					sCelebServer.eArenaClips[i] = PAIRED_ARENA_ANIM(sCelebServer.eArenaClips[POS_1ST], i)
					
					PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - [PAIRING] ", ARENA_CLIP_NAME_DBG(sCelebServer.eArenaClips[POS_1ST]), " with ", ARENA_CLIP_NAME_DBG(sCelebServer.eArenaClips[i]))
					PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - ARENA_CLIP_NAME for i = ", i, " is ", sCelebServer.eArenaClips[i], " [PAIRING] ")
				ELSE
				
					// Or else grab randoms
					iMinCeleb = MIN_NUM_ARENA_CLIPS(sCelebServer, i)
					iMaxCeleb = MAX_NUM_ARENA_CLIPS(sCelebServer, i)
					
					IF sCelebServer.iRand[i] = -1 
						sCelebServer.iRand[i] = GET_RANDOM_INT_IN_RANGE(iMinCeleb, iMaxCeleb)
						PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - iMinCeleb       = ", iMinCeleb, " iMaxCeleb       = ", iMaxCeleb)
						PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - iRand           = ", sCelebServer.iRand[i])
					ENDIF
					
					sCelebServer.eArenaClips[i] = INT_TO_ENUM(ARENA_CELEB_ANIM_CLIPS, sCelebServer.iRand[i]) 
					
					PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - ARENA_CLIP_NAME for i = ", i, " is ", ARENA_CLIP_NAME_DBG(sCelebServer.eArenaClips[i]))
				ENDIF
			ENDIF
			
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
		BOOL bAssert
		#ENDIF
		
		// FAILSAFE, set any invalid ones to something safe
		IF sCelebServer.eArenaClips[POS_1ST] = ARENA_CELEB_INVALID
			IF bPodiumMatch
				sCelebServer.eArenaClips[POS_1ST] = ARENA_CELEB_PODIUM_NO_PROPS_SOLO_1ST_cheer_a_1st
			ELSE
				sCelebServer.eArenaClips[POS_1ST] = ARENA_CELEB_FLAT_SOLO_NO_PROPS_slide_a_player_a
			ENDIF
			PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - FAILSAFE hit  POS_1ST  ")
			#IF IS_DEBUG_BUILD
			bAssert = TRUE
			#ENDIF
		ENDIF
		IF sCelebServer.iNumPlayersFound > 1
			IF sCelebServer.eArenaClips[POS_2ND] = ARENA_CELEB_INVALID
				IF bPodiumMatch
					sCelebServer.eArenaClips[POS_2ND] = ARENA_CELEB_PODIUM_NO_PROPS_SOLO_2ND_cheer_a_2nd
				ELSE
					sCelebServer.eArenaClips[POS_2ND] = ARENA_CELEB_FLAT_SOLO_NO_PROPS_thumbs_down_a_player_b
				ENDIF
				PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - FAILSAFE hit  POS_2ND  ")
				#IF IS_DEBUG_BUILD
				bAssert = TRUE
				#ENDIF
			ENDIF
		ENDIF
		IF sCelebServer.iNumPlayersFound > 2
			IF sCelebServer.eArenaClips[POS_3RD] = ARENA_CELEB_INVALID
			AND bPodiumMatch
	//			IF bPodiumMatch
					sCelebServer.eArenaClips[POS_3RD] = ARENA_CELEB_PODIUM_NO_PROPS_SOLO_3RD_clapping_a_3rd
	//			ELSE
	//				sCelebServer.eArenaClips[POS_3RD] = ARENA_CELEB_FLAT_SOLO_NO_PROPS_wave_a_player_b
	//			ENDIF
				PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - FAILSAFE hit  POS_3RD  ")
				#IF IS_DEBUG_BUILD
				bAssert = TRUE
				#ENDIF
			ENDIF
		ENDIF
		
		// How many props are needed?
		SERVER_SET_NUM_ARENA_PROPS_REQUIRED(sCelebServer, POS_1ST)
		
		PRINTLN("[NETCELEBRATION] [ARENA]  SERVER_CHOOSE_ARENA_ANIMATIONS - COMPLETE AT  ", NATIVE_TO_INT(GET_NETWORK_TIME()))
		
		SET_BIT(sCelebServer.iBitSet, ciANIMS_GRABBED)
	
		#IF IS_DEBUG_BUILD
		IF bAssert 
			SCRIPT_ASSERT("SERVER_CHOOSE_ARENA_ANIMATIONS, failsafe hit ")
		ENDIF
		#ENDIF
	
	ENDIF
ENDPROC


#IF IS_DEBUG_BUILD
	CONST_INT ALLOW_CELEBRATION_WIDGETS					0 //Celebration widgets are off by default (due to breaking debug stacks in certain modes). To turn them on, set this value to 1.

	STRUCT CELEBRATION_WIDGET_DATA
		CELEBRATION_SCREEN_DATA sDebugCelebScreen
	
		BOOL bDebugEnableCelebScreen
		BOOL bDebugTriggerCelebScreen
		BOOL bDebugTestWinnerScreenPlacement
		BOOL bDebugUseWinnerRenderTarget
		BOOL bDebugOutputPedOffsets
		BOOL bDebugDrawWinnerLight
		
		BOOL bCelebScreenAddFinishPos
		BOOL bCelebScreenAddJobPoints
		BOOL bCelebScreenAddRepPoints
		BOOL bCelebScreenAddCash
		BOOL bCelebScreenAddRankBar
		BOOL bCelebScreenAddChallengePart
		BOOL bCelebScreenAddTime
		BOOL bCelebScreenAddWorldRecord
		BOOL bCelebScreenAddWinner
		BOOL bCelebScreenAddBackground
		BOOL bCelebScreenAddScore
		BOOL bCelebScreenWonChallengePart
		BOOL bCelebScreenOverrideSummaryCams
		BOOL bCelebScreenUseParachuteSummaryCams
		BOOL bCelebScreenUseVehicleForWinnerScreen
		
		INT iCelebScreenFinishPos
		INT iCelebScreenJobPoints
		INT iCelebScreenRepPoints
		INT iCelebScreenCash
		INT iCelebScreenBetWinnings
		INT iCelebScreenCurrentLvl
		INT iCelebScreenNextLvl
		INT iCelebScreenPointsBeforeRP
		INT iCelebScreenCurrentLvlRPStart
		INT iCelebScreenNextLvlRPStart
		INT iCelebScreenChallengeType
		INT iCelebScreenWinStatus
		INT iCelebScreenFinishTime
		INT iCelebScreenPersonalBest
		INT iCelebScreenScore
		INT iCelebScreenChallengePart
		INT iCelebScreenTotalChallengePart
		INT iCurrentScreenID
		INT iBackgroundBrightness
		INT iCelebScreenCamMainIndex
		INT iCelebScreenCamRandomIndex
		INT iDebugLightColourR[3]
		INT iDebugLightColourG[3]
		INT iDebugLightColourB[3]
		INT iCelebScreenDrawModeTest1
		INT iCelebScreenDrawModeTest2
		INT iCelebScreenBackgroundAlpha
		
		VECTOR vDebugBackgroundOffset
		VECTOR vDebugBackgroundRot
		VECTOR vDebugBackgroundSize
		VECTOR vDebugLightOffset[3]
		
		FLOAT fDebugLightRange[3]
		FLOAT fDebugLightIntensity[3]
		
		TEXT_WIDGET_ID textWidgetBackgroundColour
		TEXT_WIDGET_ID textWidgetWinnerName
		TEXT_WIDGET_ID textWidgetCrewName
		
		VEHICLE_INDEX vehDebugCelebWinnerTest
	ENDSTRUCT
	
	#IF ALLOW_CELEBRATION_WIDGETS
		CELEBRATION_WIDGET_DATA sCelebrationWidgetData
	#ENDIF
#ENDIF

PROC CLEAR_CELEBRATION_MISC_POST_FX()
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[SAC] - called CLEAR_CELEBRATION_MISC_POST_FX")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
		PRINTLN("[SAC] - stopped MP_Celeb_Preload_Fade")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("SuccessNeutral")
		ANIMPOSTFX_STOP("SuccessNeutral")
		PRINTLN("[SAC] - stopped SuccessNeutral")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("HeistCelebEnd")
		ANIMPOSTFX_STOP("HeistCelebEnd")
		PRINTLN("[SAC] - stopped HeistCelebEnd")
	ENDIF
	
ENDPROC

PROC CLEAR_CELEBRATION_PASS_POST_FX()
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[SAC] - called CLEAR_CELEBRATION_PASS_POST_FX")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("HeistCelebPass")
		ANIMPOSTFX_STOP("HeistCelebPass")
		PRINTLN("[SAC] - stopped HeistCelebPass")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("HeistCelebPassBW")
		ANIMPOSTFX_STOP("HeistCelebPassBW")
		PRINTLN("[SAC] - stopped HeistCelebPassBW")
	ENDIF
	
ENDPROC

PROC CLEAR_CELEBRATION_FAIL_POST_FX()

	#IF IS_DEBUG_BUILD
	PRINTLN("[SAC] - called CLEAR_CELEBRATION_FAIL_POST_FX")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("HeistCelebFail")
		ANIMPOSTFX_STOP("HeistCelebFail")
		PRINTLN("[SAC] - stopped HeistCelebFail")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("HeistCelebFailBW")
		ANIMPOSTFX_STOP("HeistCelebFailBW")
		PRINTLN("[SAC] - stopped HeistCelebFailBW")
	ENDIF
	
ENDPROC

PROC CLEAR_ALL_CELEBRATION_POST_FX()
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[SAC] - called CLEAR_ALL_CELEBRATION_POST_FX")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	CLEAR_CELEBRATION_MISC_POST_FX()
	CLEAR_CELEBRATION_FAIL_POST_FX()
	CLEAR_CELEBRATION_PASS_POST_FX()
	
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				DPAD_LEADERBOARD												║

// iDpadBitSet
CONST_INT ciPLAYER_LIST_INITIALISE 	0
CONST_INT ciDPAD_HAVE_UPDATED		1

CONST_INT LIST_INIT					0
CONST_INT LIST_POPULATE 			1
CONST_INT LIST_DRAW 				2

PROC CLEANUP_PLAYER_LIST(DPAD_VARS& dpadVars, BOOL bResetTimer)
//	RESET_DPAD_REFRESH()
	CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_HAVE_UPDATED)
	dpadVars.iDrawProgress = LIST_INIT
	IF bResetTimer
		IF HAS_NET_TIMER_STARTED(dpadVars.timerDpad)
			RESET_NET_TIMER(dpadVars.timerDpad)	
			PRINTLN("[PLAYER_LIST] RESET_NET_TIMER")
		ENDIF
	ENDIF
	IF HAS_SCALEFORM_MOVIE_LOADED(dpadVars.scaleformFreemodeMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(dpadVars.scaleformFreemodeMovie)
	ENDIF
	IF IS_BIT_SET(dpadVars.iDpadBitSet, ciPLAYER_LIST_INITIALISE)
		CLEAR_BIT(dpadVars.iDpadBitSet, ciPLAYER_LIST_INITIALISE)
	ENDIF
ENDPROC

FUNC BOOL REQUEST_PLAYER_LIST_SUMMARY_CARD(SCALEFORM_INDEX& siMovie)

	siMovie = REQUEST_SCALEFORM_MOVIE("mp_mm_card_freemode")

	RETURN HAS_SCALEFORM_MOVIE_LOADED(siMovie)
ENDFUNC

/// PURPOSE: Sets the title on the scaleform movie passed.
PROC SET_PLAYER_LIST_CARD_TITLE(SCALEFORM_INDEX& siMovie, STRING strTitle, BOOL bUseSecondPage = TRUE)
	PRINTLN(" [PLAYER_LIST] MP Scaleform: SET_PLAYER_LIST_CARD_TITLE - title: ", strTitle)
	INT iNumberOne 
	INT iNumberTwo 
	IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
		BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_TITLE")
		
			// 1. STRING Title
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strTitle)

			// 2. STRING Difficulty/Page number
			IF DOES_DPAD_LBD_NEED_SECOND_PAGE()
			AND bUseSecondPage
				IF ON_SECOND_PAGE_DPAD_LBD()
					// 2/2
					iNumberOne = 2
					iNumberTwo = 2
				ELSE
					// 1/2
					iNumberOne = 1
					iNumberTwo = 2
				ENDIF
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("LBD_DPD_CNT")
					ADD_TEXT_COMPONENT_INTEGER(iNumberOne)
					ADD_TEXT_COMPONENT_INTEGER(iNumberTwo)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				// Mission difficulty
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
				// Dpad icons briefcase etc.
			ENDIF
			
//			// 3. ICON			
//			IF iJobIcon <> -1
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iJobIcon)  // tiICON
//			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DRAW_PLAYER_LIST(DPAD_VARS& dpadVars)	

	// Enable dpad leaderboard in case folk are blocking
	DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()
	
//	BOOL bGolf = IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_GOLF)
	
	// Push dpad down
	IF NOT HAS_NET_TIMER_STARTED(dpadVars.timerDpad)	
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_DOWN)
		OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X))
			REQUEST_PLAYER_LIST_SUMMARY_CARD(dpadVars.scaleformFreemodeMovie)
			// Set the 10 second viewing timer
			START_NET_TIMER(dpadVars.timerDpad)
			CLEANUP_PLAYER_LIST(dpadVars, FALSE)
			PRINTLN("[PLAYER_LIST] INPUT_FRONTEND_DOWN")
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(dpadVars.timerDpad)	
		
			// Quit when time expires
//			IF HAS_NET_TIMER_EXPIRED(dpadVars.timerDpad, DPAD_DOWN_TIMER)
//			OR GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE AND NOT bGolf
//			OR bGolf AND IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_DOWN)
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_DOWN) AND NOT ON_SECOND_PAGE_DPAD_LBD()
			OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X) AND NOT ON_SECOND_PAGE_DPAD_LBD())
				
				// Reset timer when 10 secs up or dpad pressed again
				CLEANUP_PLAYER_LIST(dpadVars, TRUE)
				PRINTLN("[PLAYER_LIST] DPAD_DOWN_TIMER")
				
				RETURN FALSE
			ELSE

				IF IS_BIT_SET(dpadVars.iDpadBitSet, ciPLAYER_LIST_INITIALISE)
				
					PRINTLN("[PLAYER_LIST] ciPLAYER_LIST_INITIALISE")
					
					RETURN TRUE
				ELSE

					IF REQUEST_PLAYER_LIST_SUMMARY_CARD(dpadVars.scaleformFreemodeMovie) //  we can't spam this 
					
						SCALEFORM_SET_DATA_SLOT_EMPTY(dpadVars.scaleformFreemodeMovie)

						PRINTLN("[PLAYER_LIST] SHOULD_DRAW_PLAYER_LIST")

						SET_PLAYER_LIST_CARD_TITLE(dpadVars.scaleformFreemodeMovie, "CELEB_LIST", DEFAULT)

						SET_BIT(dpadVars.iDpadBitSet, ciPLAYER_LIST_INITIALISE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_THE_PLAYER_LIST(DPAD_VARS& dpadVars)

	PLAYER_INDEX PlayerId
	STRING strHeadshotTxd
	PEDHEADSHOT_ID pedHeadshot
	TEXT_LABEL_15 tlCrewTag
	INT iRank, iScore, iPlayer, iIcon
	dpadVars.bShowJp = FALSE
	
	IF SHOULD_DRAW_PLAYER_LIST(dpadVars)
	
		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
	
		IF dpadVars.iDrawProgress = LIST_INIT
		
//			g_i_NumDpadLbdPlayers = 0
			
			PRINTLN("[PLAYER_LIST] DRAW_THE_PLAYER_LIST")
		
			INT i
			INT iCount
			REPEAT NUM_NETWORK_PLAYERS i
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
					PlayerId = INT_TO_PLAYERINDEX(i)
					IF NOT IS_PLAYER_SCTV(PlayerId)
						IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PlayerId, PLAYER_ID())

							iPlayer = NATIVE_TO_INT(PlayerId)
						
							iRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
							iScore = -1
					
							// Grab crew tags
							tlCrewTag = GET_CREW_TAG_DPAD_LBD(PlayerId)
						
							dpadVars.tl63LeaderboardPlayerNames = GET_PLAYER_NAME(PlayerId) 			// grab names
							
							// Attempt to get player headshot
							pedHeadshot = Get_HeadshotID_For_Player(playerId)
							strHeadshotTxd = ""
							IF pedHeadshot <> NULL
								strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
							ENDIF
							
//							g_i_NumDpadLbdPlayers++
							
							PRINTLN("[PLAYER_LIST] Freemode about to call POPULATE_NEW_FREEMODE_DPAD_ROWS with row = ", iCount, " player name = ", dpadVars.tl63LeaderboardPlayerNames, " iPlayer = ", iPlayer)

							POPULATE_NEW_FREEMODE_DPAD_ROWS(SUB_FREEMODE, dpadVars.scaleformFreemodeMovie, dpadVars, iCount, tlCrewTag, strHeadshotTxd, iRank, iScore, HUD_COLOUR_FREEMODE, DISPLAY_HEADSHOT, -1)
							
							iIcon = GET_DPAD_PLAYER_VOICE_ICON(dpadVars.sDpadMics[i].iVoiceChatState)
							SET_PLAYER_MIC(dpadVars.scaleformFreemodeMovie, iCount, iIcon, iRank)
							
							iCount++
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			PRINTLN("[PLAYER_LIST] DPAD_DRAW")
		
			dpadVars.iDrawProgress = DPAD_DRAW
		ENDIF
		
		IF dpadVars.iDrawProgress = DPAD_DRAW
			
			IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_HAVE_UPDATED)
				DISPLAY_VIEW(dpadVars.scaleformFreemodeMovie)
				SET_BIT(dpadVars.iDpadBitSet, ciDPAD_HAVE_UPDATED)
			ENDIF
	
			IF HAS_SCALEFORM_MOVIE_LOADED(dpadVars.scaleformFreemodeMovie)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
					DRAW_SCALEFORM_MOVIE(dpadVars.scaleformFreemodeMovie, SDPAD_X, SDPAD_Y, SDPAD_W, SDPAD_H, 255, 255, 255, 255)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			ENDIF
			
			IF MAINTAIN_DPAD_VOICE_CHAT(dpadVars)
			
				PRINTLN("[PLAYER_LIST] LIST_INIT")
			
				dpadVars.iDrawProgress = LIST_INIT
			ENDIF

		ENDIF
			
	ELSE
		dpadVars.iDrawProgress = LIST_INIT
		RESET_DPAD_REFRESH()
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
ENDPROC

PROC MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS(CELEBRATION_SCREEN_DATA &sCelebrationData, BOOL bShowPlayerListOption = TRUE, BOOL bShowPlayerNamesOption = TRUE)
	
	SPRITE_PLACEMENT aSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	SWITCH sCelebrationData.iPlayerListProgress
		
		CASE 0
			
			IF LOAD_MENU_ASSETS()
				
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS - LOAD_MENU_ASSETS = TRUE")	
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS - calling REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS")	
				
				REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sCelebrationData.scaleformStruct)
				
				IF bShowPlayerListOption
					
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS - bShowPlayerListOption = TRUE")	
					
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS - IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) = TRUE")	
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_X, "CELEB_LIST_TTL", sCelebrationData.scaleformStruct, TRUE)
					ELSE
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS - IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) = FALSE")	
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN), "CELEB_LIST_TTL", sCelebrationData.scaleformStruct)
					ENDIF
				ENDIF
				
				IF bShowPlayerNamesOption
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS - bShowPlayerNamesOption = TRUE")
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, "CELEB_NAMES", sCelebrationData.scaleformStruct, TRUE)
				ENDIF
				
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					sCelebrationData.iPlayerListProgress++
				ELSE
					sCelebrationData.iPlayerListProgress = 2
				ENDIF
				
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS - set sCelebrationData.iPlayerListProgress = ", sCelebrationData.iPlayerListProgress)
				
			ENDIF
			
		BREAK
		
		CASE 1
		
			RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sCelebrationData.celebInstructions, aSprite, sCelebrationData.scaleformStruct, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sCelebrationData.scaleformStruct)) 
			
			SET_MOUSE_CURSOR_THIS_FRAME()
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_X)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_Y)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			
			IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				sCelebrationData.iPlayerListProgress = 0
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS - IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) now is FALSE, need to refresh. Set sCelebrationData.iPlayerListProgress = ", sCelebrationData.iPlayerListProgress)
			ENDIF
			
		BREAK
		
		CASE 2
			
			RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sCelebrationData.celebInstructions, aSprite, sCelebrationData.scaleformStruct, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sCelebrationData.scaleformStruct)) 
			
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				sCelebrationData.iPlayerListProgress = 0
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS - IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) now is TRUE, need to refresh. Set sCelebrationData.iPlayerListProgress = ", sCelebrationData.iPlayerListProgress)
			ENDIF
			
		BREAk
		
	ENDSWITCH
	
ENDPROC


//╚═════════════════════════════════════════════════════════════════════════════╝


PROC CELEB_SET_PLAYERS_INVISIBLE_THIS_FRAME()
	
	INT i
	
	SET_LOCAL_PLAYER_INVISIBLE_LOCALLY(TRUE)
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i), FALSE)
			SET_PLAYER_INVISIBLE_LOCALLY(INT_TO_NATIVE(PLAYER_INDEX, i), TRUE)
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Enables/disables chat on celebration screens, will also enable chat for players that joined as spectators.
///    Do not call this until the job has ended, otherwise spectators will be able to chat to people who are still playing.
/// PARAMS:
///    bEnable - 
PROC SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(BOOL bEnable)
	IF bEnable
		CLIENT_SET_UP_EVERYONE_CHAT()
		
		PRINTLN("[NETCELEBRATION] SET_CHAT_FOR_CELEBRATION_SCREEN(TRUE) called by: ", GET_THIS_SCRIPT_NAME())
	ELSE
		PRINTLN("[NETCELEBRATION] SET_CHAT_FOR_CELEBRATION_SCREEN(FALSE) called by: ", GET_THIS_SCRIPT_NAME())
	ENDIF
	
	NETWORK_SET_OVERRIDE_SPECTATOR_MODE(bEnable)
ENDPROC

FUNC TEXT_LABEL_63 GET_CREW_NAME_FOR_CELEBRATION_SCREEN(PLAYER_INDEX playerId)
	TEXT_LABEL_63 strCrewName
	
	IF IS_NET_PLAYER_OK(playerId, FALSE)
		GAMER_HANDLE sGamerHandle = GET_GAMER_HANDLE_PLAYER(playerId)
		
		IF IS_PLAYER_IN_ACTIVE_CLAN(sGamerHandle)	
			NETWORK_CLAN_DESC sCrewHandle = GET_PLAYER_CREW(playerId)
			strCrewName = sCrewHandle.ClanName
		ENDIF
	ENDIF

	RETURN strCrewName
ENDFUNC

PROC CLEANUP_WINNER_SCENE_ENTITIES(CELEBRATION_SCREEN_DATA &sCelebrationData)
	IF DOES_ENTITY_EXIST(sCelebrationData.objWinnerBackground)
		DELETE_OBJECT(sCelebrationData.objWinnerBackground)
//		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_BIG_CIN_SCREEN)
	ENDIF
	
	IF IS_VALID_INTERIOR(sCelebrationData.winnerSceneInterior)
		IF IS_INTERIOR_READY(sCelebrationData.winnerSceneInterior)
			UNPIN_INTERIOR(sCelebrationData.winnerSceneInterior)
		ENDIF
	ENDIF
			
	REMOVE_ANIM_DICT("Move_m@generic_idles@std")
	REMOVE_ANIM_DICT("Move_f@generic_idles@std")
	REMOVE_ANIM_DICT("mp_player_intfinger")
	REMOVE_ANIM_DICT("mp_player_intsalute")
	REMOVE_ANIM_DICT("mp_player_introck")
	REMOVE_ANIM_DICT("mp_player_intwank")
	
	INT i = 0
	REPEAT COUNT_OF(sCelebrationData.pedWinnerClones) i
		IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClones[i])
			DELETE_PED(sCelebrationData.pedWinnerClones[i])
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(sCelebrationData.objectLightRig)
		DELETE_OBJECT(sCelebrationData.objectLightRig)
		PRINTLN("[NETCELEBRATION] CREATE_ARENA_LIGHT_RIG - DELETE_ENTITY")
	ENDIF
	
	IF DOES_ENTITY_EXIST(sCelebrationData.oiIslandLightObject)
		DELETE_OBJECT(sCelebrationData.oiIslandLightObject)
		PRINTLN("[NETCELEBRATION] CREATE_ISLAND_LIGHT_PROP - DELETE_ENTITY")
	ENDIF
ENDPROC

FUNC BOOL CELEBRATION_IS_ISLAND_MODE()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
ENDFUNC

PROC FORCE_WEATHER_AND_TIME_FOR_CELEBRATION_WINNER()
	
	INT iPrevWeather, iNextWeather
	FLOAT fInterp
	
	GET_CURR_WEATHER_STATE(iPrevWeather, iNextWeather, fInterp)
	
	SET_OVERRIDE_WEATHER("clear")
	
	IF NOT CELEBRATION_IS_ISLAND_MODE()
		PRINTLN("[NETCELEBRATION] FORCE_WEATHER_AND_TIME_FOR_CELEBRATION_WINNER - Setting time to midnight")
		SET_TIME_OF_DAY(TIME_MIDNIGHT)
	ENDIF
	
	UNLOAD_ALL_CLOUD_HATS()
	
	PRINTLN("[NETCELEBRATION] FORCE_WEATHER_AND_TIME_FOR_CELEBRATION_WINNER - Forcing weather and time")
	
ENDPROC

PROC RESET_WEATHER_AND_TIME_AFTER_CELEBRATION_WINNER(BOOL bClearTimeOverride = TRUE)

	IF NOT g_sMPTunables.bTurn_snow_on_off AND NOT IS_HEIST_QUICK_RESTART()
		CLEAR_OVERRIDE_WEATHER()
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION
	AND g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART	
	AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
	AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
	AND bClearTimeOverride
		SET_TIME_OF_DAY(TIME_OFF, TRUE)
		SET_CLOUD_SETTINGS_OVERRIDE("")
	ENDIF
	
	
	PRINTLN("[NETCELEBRATION] - Resetting weather and time")
	
ENDPROC

/// PURPOSE:
///    Kills all other scripted UI systems in preparation for the celebration screen (e.g. putting away the phone, quitting shops, etc).
///    Call this at the same time as soing the camera cut for the celebration screen.
PROC KILL_UI_FOR_CELEBRATION_SCREEN(BOOL bPreservePhone = FALSE)
	IF NOT bPreservePhone
		HANG_UP_AND_PUT_AWAY_PHONE()
	ENDIF
	
	CLEAR_ALL_BIG_MESSAGES()
	CLOSE_WEB_BROWSER()
ENDPROC

/// PURPOSE:
///    Hides any HUD elements that could potentially appear during the celebration screen (e.g. feed messages).
PROC HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME(BOOL bAllowSubtitles = FALSE)
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM() 
		EXIT
	ENDIF
	
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	DISABLE_CELLPHONE_INTERNET_APP_THIS_FRAME_ONLY()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
	THEFEED_HIDE_THIS_FRAME()
	
	DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_TURRET_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), GET_SEAT_PED_IS_IN(PLAYER_PED_ID())))
	  	  	DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			DISABLE_FIRST_PERSON_CAMS()
			DISABLE_CINEMATIC_VEHICLE_IDLE_MODE_THIS_UPDATE()
		ENDIF
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	ENDIF
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
	BOOL IsHQRHELPDisplaying = FALSE
		IsHQRHELPDisplaying = IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQRHELP")
	IF NOT IsHQRHELPDisplaying
		HIDE_HELP_TEXT_THIS_FRAME()
	ENDIF
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
	IF NOT bAllowSubtitles
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
	ENDIF
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_1)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_2)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)	
ENDPROC

FUNC BOOL FADE_FAKE_RECTANGLE_FOR_WINNER_SCREEN(BOOL bFadeIn)
	
	INT iR, iG, iB
	
	IF g_TransitionSessionNonResetVars.sRectangleData.bWhiteRectangle
		iR = 255
		iG = 255
		iB = 255
	ENDIF
	
	IF bFadeIn
		g_TransitionSessionNonResetVars.sRectangleData.iAlpha = g_TransitionSessionNonResetVars.sRectangleData.iAlpha + 5
	ELSE
		g_TransitionSessionNonResetVars.sRectangleData.iAlpha = g_TransitionSessionNonResetVars.sRectangleData.iAlpha - 5
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sRectangleData.iAlpha > 255
		g_TransitionSessionNonResetVars.sRectangleData.iAlpha = 255
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sRectangleData.iAlpha < 0
		g_TransitionSessionNonResetVars.sRectangleData.iAlpha = 0
	ENDIF
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	DRAW_RECT(0.5, 0.5, 3.0, 3.0, iR, iG, iB, g_TransitionSessionNonResetVars.sRectangleData.iAlpha)
	
	IF bFadeIn
		IF g_TransitionSessionNonResetVars.sRectangleData.iAlpha >= 255
			RETURN TRUE
		ENDIF
	ELSE
		IF g_TransitionSessionNonResetVars.sRectangleData.iAlpha <= 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DRAW_RECTANGLE_WITH_CURRENT_ALPHA()

	INT iR, iG, iB
	
	IF g_TransitionSessionNonResetVars.sRectangleData.bWhiteRectangle
		iR = 255
		iG = 255
		iB = 255
	ENDIF
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	DRAW_RECT(0.5, 0.5, 3.0, 3.0, iR, iG, iB, g_TransitionSessionNonResetVars.sRectangleData.iAlpha)
	
ENDPROC

PROC MAINTAIN_RECTANGLE()
	
	#IF IS_DEBUG_BUILD
		IF g_TransitionSessionNonResetVars.sRectangleData.bFadeInActivated
		AND g_TransitionSessionNonResetVars.sRectangleData.bFadeOutActivated
			SCRIPT_ASSERT("[SAC] - [NETCELEBRATION] - [RECT] - bFadeInActivated and bFadeOutActivated. No.")
			g_TransitionSessionNonResetVars.sRectangleData.bFadeOutActivated = TRUE
			g_TransitionSessionNonResetVars.sRectangleData.bFadeInActivated = FALSE
		ENDIF
	#ENDIF
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
		IF NOT IS_RECTANGLE_FADED_OUT()
		AND NOT IS_RECTANGLE_FADING_OUT()
			PRINTLN("[SAC] - [NETCELEBRATION] - [RECT] - GET_CURRENT_GAMEMODE() = GAMEMODE_SP - need to fade out rectangle.")
			ACTIVATE_RECTANGLE_FADE_IN(FALSE)
			ACTIVATE_RECTANGLE_FADE_OUT(TRUE)
		ENDIF
	ENDIF
	
	SWITCH g_TransitionSessionNonResetVars.sRectangleData.eState
		
		CASE ENUMRECTANGLESTATE_FADED_OUT
			IF g_TransitionSessionNonResetVars.sRectangleData.bFadeInActivated
				PRINTLN("[SAC] - [NETCELEBRATION] - [RECT] - in ENUMRECTANGLESTATE_FADED_OUT, going to ENUMRECTANGLESTATE_FADING_IN.")
				g_TransitionSessionNonResetVars.sRectangleData.eState = ENUMRECTANGLESTATE_FADING_IN
			ENDIF
		BREAK
		
		CASE ENUMRECTANGLESTATE_FADING_IN
			IF FADE_FAKE_RECTANGLE_FOR_WINNER_SCREEN(TRUE)
				PRINTLN("[SAC] - [NETCELEBRATION] - [RECT] - in ENUMRECTANGLESTATE_FADING_IN, FADE_FAKE_RECTANGLE_FOR_WINNER_SCREEN = TRUE, going to ENUMRECTANGLESTATE_FADED_IN.")
				g_TransitionSessionNonResetVars.sRectangleData.eState = ENUMRECTANGLESTATE_FADED_IN
			ENDIF
			IF g_TransitionSessionNonResetVars.sRectangleData.bFadeOutActivated
				PRINTLN("[SAC] - [NETCELEBRATION] - [RECT] - in ENUMRECTANGLESTATE_FADING_IN, bFadeOutActivated = TRUE, going to ENUMRECTANGLESTATE_FADING_OUT.")
				g_TransitionSessionNonResetVars.sRectangleData.eState = ENUMRECTANGLESTATE_FADING_OUT
			ENDIF
		BREAK
		
		CASE ENUMRECTANGLESTATE_FADED_IN
			DRAW_RECTANGLE_WITH_CURRENT_ALPHA()
			IF g_TransitionSessionNonResetVars.sRectangleData.bFadeOutActivated
				PRINTLN("[SAC] - [NETCELEBRATION] - [RECT] - in ENUMRECTANGLESTATE_FADED_IN, bFadeOutActivated = TRUE, going to ENUMRECTANGLESTATE_FADING_OUT.")
				g_TransitionSessionNonResetVars.sRectangleData.eState = ENUMRECTANGLESTATE_FADING_OUT
			ENDIF
		BREAK
		
		CASE ENUMRECTANGLESTATE_FADING_OUT
			IF FADE_FAKE_RECTANGLE_FOR_WINNER_SCREEN(FALSE)
				PRINTLN("[SAC] - [NETCELEBRATION] - [RECT] - in ENUMRECTANGLESTATE_FADING_OUT, FADE_FAKE_RECTANGLE_FOR_WINNER_SCREEN = TRUE, going to ENUMRECTANGLESTATE_FADED_OUT.")
				g_TransitionSessionNonResetVars.sRectangleData.eState = ENUMRECTANGLESTATE_FADED_OUT
			ENDIF
			IF g_TransitionSessionNonResetVars.sRectangleData.bFadeInActivated
				PRINTLN("[SAC] - [NETCELEBRATION] - [RECT] - in ENUMRECTANGLESTATE_FADING_OUT, bFadeInActivated = TRUE, going to ENUMRECTANGLESTATE_FADING_IN.")
				g_TransitionSessionNonResetVars.sRectangleData.eState = ENUMRECTANGLESTATE_FADING_IN
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Flags the celebration screen as currently active, so other scripts can query if necessary (e.g. to disable awards appearing during the screen).
PROC SET_CELEBRATION_SCREEN_AS_ACTIVE(BOOL bIsActive, BOOL bActivateInteractionMenu = TRUE)
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[NETCELEBRATION] SET_CELEBRATION_SCREEN_AS_ACTIVE - has been called:")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bIsActive
			PRINTLN("[NETCELEBRATION] SET_CELEBRATION_SCREEN_AS_ACTIVE set to TRUE by ", GET_THIS_SCRIPT_NAME())
		ELSE
			PRINTLN("[NETCELEBRATION] SET_CELEBRATION_SCREEN_AS_ACTIVE set to FALSE by ", GET_THIS_SCRIPT_NAME())
		ENDIF
	#ENDIF
	
	IF bIsActive
		DISABLE_INTERACTION_MENU()
		SET_MULTIHEAD_SAFE(TRUE) // CMcM: Fix for 2186922
		set_widescreen_borders(TRUE, 0) // WK: url:bugstar:2296580
		PRINTLN("[NETCELEBRATION] SET_CELEBRATION_SCREEN_AS_ACTIVE - DISABLE_INTERACTION_MENU called by ", GET_THIS_SCRIPT_NAME())
	ELSE
		IF bActivateInteractionMenu
			ENABLE_INTERACTION_MENU()
			SET_MULTIHEAD_SAFE(FALSE) // CMcM: Fix for 2186922			
			PRINTLN("[NETCELEBRATION] SET_CELEBRATION_SCREEN_AS_ACTIVE - ENABLE_INTERACTION_MENU called by ", GET_THIS_SCRIPT_NAME())
		#IF IS_DEBUG_BUILD
			PRINTLN("[NETCELEBRATION] SET_CELEBRATION_SCREEN_AS_ACTIVE - not calling ENABLE_INTERACTION_MENU, bActivateInteractionMenu = FALSE.")
		#ENDIF
		ENDIF
		set_widescreen_borders(FALSE, 0) // WK: url:bugstar:2296580  KevBo: url:bugstar:2340102 - moved outside if statement
	ENDIF
	
	g_bCelebrationScreenIsActive = bIsActive
	IF bIsActive = FALSE
		g_bIgnoreCelebrationScreenPV = FALSE
	ENDIF
	
ENDPROC

PROC ADJUST_ESTIMATED_TIME_BASED_ON_ANIM_LENGTH(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	INT iAnimLength 
	INT iMinLength
	
	iMinLength = sCelebrationData.iEstimatedScreenDuration
	iAnimLength = CEIL(sCelebrationData.fAnimLength)
	iAnimLength = iAnimLength*1000//00000
	
	PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - iAnimLength = ", iAnimLength)
	PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - iEstimatedScreenDuration = ", sCelebrationData.iEstimatedScreenDuration)
	
	IF sCelebrationData.iEstimatedScreenDuration < iAnimLength
		sCelebrationData.iEstimatedScreenDuration = iAnimLength - 1500
		PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - sCelebrationData.iEstimatedScreenDuration < iAnimLength. Setting to iAnimLength + 2000. Now equals ", sCelebrationData.iEstimatedScreenDuration)
	ENDIF
	
	IF sCelebrationData.iEstimatedScreenDuration < iMinLength
	
		PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - iEstimatedScreenDuration < iMinLength ")
		PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - iEstimatedScreenDuration = ", sCelebrationData.iEstimatedScreenDuration)
		PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - iEstimatedScreenDuration = ", iMinLength)
		
		sCelebrationData.iEstimatedScreenDuration = iMinLength
		PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - set iEstimatedScreenDuration = iMinLength = ", sCelebrationData.iEstimatedScreenDuration)
		
	ENDIF
	
	IF iAnimLength < sCelebrationData.iEstimatedScreenDuration
		sCelebrationData.bUseExtendedIdle = TRUE
		PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - set bUseExtendedIdle TRUE since anim will finish before end of screen.")
	ENDIF
	
ENDPROC


PROC SET_DOING_HEIST_CELEBRATION_SCREEN(BOOL bDoingScene)
	
	#IF IS_DEBUG_BUILD
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration != bDoingScene
		PRINTLN("[SAC] - [RCC MISSION] - [NETCELEBRATION] - SET_DOING_HEIST_CELEBRATION_SCREEN - set g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration to ", bDoingScene)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration = bDoingScene
	
ENDPROC

PROC SET_DOING_HEIST_END_WINNER_SCENE(BOOL bDoingScene)
	
	#IF IS_DEBUG_BUILD
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene != bDoingScene
		PRINTLN("[SAC] - [RCC MISSION] - [NETCELEBRATION] - SET_DOING_HEIST_CELEBRATION_SCREEN - set g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene to ", bDoingScene)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene = bDoingScene
	
ENDPROC


///Stores the interaction anim values for the local player in the MBGlobalsInteractions struct. This can then be queried later for the winner screen, even if the winner has left the session.
PROC STORE_PLAYER_INTERACTION_ANIM_VALUES_FOR_CELEBRATION_SCREEN(BOOL bIsRallyDriver = FALSE)
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		INT iPlayerIndex = NATIVE_TO_INT(PLAYER_ID())
		
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_ANIM_TY) = ENUM_TO_INT(INTERACTION_ANIM_TYPE_IN_CAR)
			GlobalplayerBD[iPlayerIndex].iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER)
		ELSE
			GlobalplayerBD[iPlayerIndex].iInteractionType = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_ANIM_TY)
		ENDIF
		
		GlobalplayerBD[iPlayerIndex].iInteractionAnim = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_ANIM)
		
		GlobalplayerBD[iPlayerIndex].iPairedAnim = GET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_STAT_RALLY_ANIM)
		
		GlobalplayerBD[iPlayerIndex].bIsRallyDriver = bIsRallyDriver
		
		GlobalplayerBD[iPlayerIndex].iCrewAnim = GetCrewAnim()
		
		PRINTLN("[NETCELEBRATION] [Paired_Anims] STORE_PLAYER_INTERACTION_ANIM_VALUES_FOR_CELEBRATION_SCREEN called for player ", iPlayerIndex, " ", 
				  GlobalplayerBD[iPlayerIndex].iInteractionType, " ", GlobalplayerBD[iPlayerIndex].iInteractionAnim, ", iPairedAnim = ", GlobalplayerBD[iPlayerIndex].iPairedAnim)
	
	#IF IS_DEBUG_BUILD
	ELSE
	
		PRINTLN("[NETCELEBRATION] - IS_NET_PLAYER_OK(PLAYER_ID(), FALSE) has returned FALSE when calling STORE_PLAYER_INTERACTION_ANIM_VALUES_FOR_CELEBRATION_SCREEN.")
		SCRIPT_ASSERT("[NETCELEBRATION] - IS_NET_PLAYER_OK(PLAYER_ID(), FALSE) has returned FALSE when calling STORE_PLAYER_INTERACTION_ANIM_VALUES_FOR_CELEBRATION_SCREEN.")
		DEBUG_PRINTCALLSTACK()
		
	#ENDIF
	
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_IN_SAME_CREW_AS_ME(GAMER_HANDLE &GamerHandle, BOOL &bReady) 
	NETWORK_CLAN_DESC retDesc 
	INT iPlayersCrew 
	GAMER_HANDLE PlayerGamerHandle 
	
	IF IS_GAMER_HANDLE_VALID(GamerHandle) 
		IF NETWORK_CLAN_SERVICE_IS_VALID()
			PlayerGamerHandle = GET_LOCAL_GAMER_HANDLE() 
			NETWORK_CLAN_PLAYER_GET_DESC(retDesc, SIZE_OF(retDesc), PlayerGamerHandle) 
			iPlayersCrew = retDesc.Id 
			
			PRINTLN("[NETCELEBRATION] IS_PLAYER_IN_SAME_CREW_AS_ME - Gamer handle is valid, player's crew is ", iPlayersCrew, " ", GET_THIS_SCRIPT_NAME())
			
			IF iPlayersCrew != 0
				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(GamerHandle)
					IF NETWORK_CLAN_PLAYER_GET_DESC(retDesc, SIZE_OF(retDesc), GamerHandle) 
						PRINTLN("[NETCELEBRATION] IS_PLAYER_IN_SAME_CREW_AS_ME - NETWORK_CLAN_PLAYER_GET_DESC returned TRUE ", GET_THIS_SCRIPT_NAME())
					
						bReady = TRUE 
						RETURN (retDesc.Id = iPlayersCrew) 
					ELSE 
						PRINTLN("[NETCELEBRATION] IS_PLAYER_IN_SAME_CREW_AS_ME - NETWORK_CLAN_PLAYER_GET_DESC returned FALSE ", GET_THIS_SCRIPT_NAME())
				
						bReady = TRUE 
						RETURN FALSE 
					ENDIF 
				ELSE
					PRINTLN("[NETCELEBRATION] IS_PLAYER_IN_SAME_CREW_AS_ME - NETWORK_CLAN_PLAYER_IS_ACTIVE returned FALSE ", GET_THIS_SCRIPT_NAME())
				
					bReady = TRUE 
					RETURN FALSE 
				ENDIF
			ELSE
				PRINTLN("[NETCELEBRATION] IS_PLAYER_IN_SAME_CREW_AS_ME - iPlayersCrew = 0 ", GET_THIS_SCRIPT_NAME())
				
				bReady = TRUE 
				RETURN FALSE 
			ENDIF
		ELSE
			PRINTLN("[NETCELEBRATION] IS_PLAYER_IN_SAME_CREW_AS_ME - NETWORK_CLAN_SERVICE_IS_VALID returned FALSE ", GET_THIS_SCRIPT_NAME())
		
			bReady = TRUE 
			RETURN FALSE
		ENDIF
	ELSE
		PRINTLN("[NETCELEBRATION] IS_PLAYER_IN_SAME_CREW_AS_ME - IS_GAMER_HANDLE_VALID returned FALSE ", GET_THIS_SCRIPT_NAME())
		
		bReady = TRUE 
		RETURN FALSE
	ENDIF
		
	RETURN FALSE 
ENDFUNC

PROC TRIGGER_APARTMENT_HANDOVER(INT &playerHashes[], BOOL bMissionWasPrep = FALSE)

	CDEBUG1LN(DEBUG_ACT_CELEB, "Celebration apartment handover Triggered")
	
	INT i
	
	REPEAT 4 i
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iInteriorPlayerNameHash[i] = playerHashes[i]
		CDEBUG2LN(DEBUG_ACT_CELEB, "TRIGGER_APARTMENT_HANDOVER: Player[", i, "] ", playerHashes[i])
	ENDREPEAT
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bReadyForApartmentTakeOver = TRUE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMissionWasPrep = bMissionWasPrep
	
	SET_LOCAL_PLAYER_DOING_APARTMENT_POST_MISSION_SCENE(TRUE)
	
ENDPROC

PROC SET_CELEBRATION_GLOBALS_FIRST_UPDATE(BOOL bFirstUpdate)
	
	IF bFirstUpdate != g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstUpdate
		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstUpdate = bFirstUpdate
		
		#IF IS_DEBUG_BUILD
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstUpdate
			PRINTLN("[NETCELEBRATION] - [RCC MISSION] - [PMC] - [SAC] - setting g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstUpdate to TRUE.")
		ELSE
			PRINTLN("[NETCELEBRATION] - [RCC MISSION] - [PMC] - [SAC] - setting g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstUpdate to FALSE.")
		ENDIF
		#ENDIF
		
	ENDIF
	
ENDPROC

PROC CLEANUP_STAND_ALONE_CELEBRATION_SCREEN_GLOBALS(BOOL bResetNeilFlag = TRUE)
	
	PRINTLN("[NETCELEBRATION] - [RCC MISSION] - [SAC] - [PMC] - CLEANUP_STAND_ALONE_CELEBRATION_SCREEN_GLOBALS has been called.")
	DEBUG_PRINTCALLSTACK()
	TEXT_LABEL_63 tlTemp
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPilotSchoolLessontime = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.fPilotSchoolAvHeight = -1.0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.fPilotSchoolDistance = -1.0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[0] = HUD_COLOUR_WHITE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[1] = HUD_COLOUR_WHITE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[2] = HUD_COLOUR_WHITE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[3] = HUD_COLOUR_WHITE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpGained = -1
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpStarted = -1
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelStartPoints = -1
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelEndPoints = -1
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel = -1
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNextLevel = -1
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingPilotSchoolCelebration = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iAvHeight = -1
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPoints = -1
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iDistance = -1
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCash = -1
	
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bJoinedMissionAsSpectator = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bInTestMode = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHaveAllHeistPlayersCelebrationShardsFallenAway = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail = FALSE
	
	SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(FALSE)
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = -1
	
	SET_PAUSE_INTRO_DROP_AWAY(FALSE)
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDrawBlackRectangle[0] = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDrawBlackRectangle[1] = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockStripClubPedsForCongratsScreen = FALSE
	SET_DOING_HEIST_CELEBRATION_SCREEN(FALSE)
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHeistCelebrationShardHasFallenAway = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bStreamingRequestsComplete = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockStripClubAssets = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[0] = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[1] = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[2] = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[3] = 0
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = 0
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName = tlTemp
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iActualTake = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyCutPercentage = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake = 0
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDifficultyHighEnoughForEliteChallenge = TRUE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumEliteChallengeComponents = 0
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentMissionTime = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetMissionTime = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMissionTime = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMissionTimeChallengeComplete = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentVehicleDamage = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageTarget = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageActual = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bVehicleDamagePercentagePassed = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentRachDamage = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageTarget = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageActual = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bRachDamagePercentagePassed = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentExtractionTime = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetExtractionTime = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iExtractionTime = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bExtractionTimeChallengeComplete = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedKills = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKillsTarget = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKills = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedKillsChallengeComplete = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNooseCalled = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNooseCalled = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedHeadshots = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshotsTarget = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshots = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedHeadshotsChallengeComplete = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentLootBagFull = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bLootBagFullChallengeComplete = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHackFailed = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailedTarget = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailed = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumHackFailedChallengeComplete = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHealthDamage = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageTarget = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageActual = 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHealthDamagePercentagePassed = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentLivesLost = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumLivesLostChallengeComplete = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentTripSkip = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDroppedToDirect = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDidQuickRestart = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iEliteChallengeBonusValue = 0
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstTimeBonusShouldBeShown = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iFirstTimeBonusValue = 0
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOrderBonusShouldBeShown = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iOrderBonusValue = 0
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bSameTeamBonusShouldBeShown = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iSameTeamBonusValue = 0
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bUltimateBonusShouldBeShown = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iUltimateBonusValue = 0
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMemberBonusShouldBeShown = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMemberBonusValue = 0
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__NONE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[0] = tlTemp
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[1] = tlTemp
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[2] = tlTemp
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[3] = tlTemp
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[0] = tlTemp
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[1] = tlTemp
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[2] = tlTemp
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[3] = tlTemp
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOkToKillCelebrationScript = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason = tlTemp
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral = tlTemp
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral2 = tlTemp
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumJobPoints = -1
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut = FALSE
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iGangopsAwardBS = 0
	
	IF bResetNeilFlag
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHandOverToNeilCelebration = FALSE
		PRINTLN("[SAC] - CLEANUP_STAND_ALONE_CELEBRATION_SCREEN_GLOBALS - set bDoingHandOverToNeilCelebration to FALSE.")
	ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstUpdate = TRUE
	
	PRINTLN("[SAC] - cleaned up stand alone celebration screen globals.")
	
ENDPROC

/// PURPOSE:
///    Handles the player if they were in an air vehicle when the job switched to the celebration screen.
PROC UPDATE_PLAYER_IN_AIR_VEHICLE_DURING_CELEBRATION_SCREEN()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehCelebration = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF NOT IS_VEHICLE_FUCKED(vehCelebration)
				IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehCelebration))
				OR IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehCelebration))
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) <> WAITING_TO_START_TASK
						VECTOR vCelebrationGotoPos
						VECTOR vCelebrationVehPos
						FLOAT fCelebrationVehHeading
						
						vCelebrationVehPos = GET_ENTITY_COORDS(vehCelebration)
						fCelebrationVehHeading = GET_ENTITY_HEADING(vehCelebration)
						vCelebrationGotoPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCelebrationVehPos, fCelebrationVehHeading, <<0.0, 200.0, 100.0>>)
					
						TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), vehCelebration, vCelebrationGotoPos, 20.0, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH, 2.0, 10.0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Resets all of the data inside the celebration screen struct, ready to use again.
PROC INITIALISE_CELEBRATION_SCREEN_DATA(CELEBRATION_SCREEN_DATA &sCelebrationData)
	sCelebrationData.eCurrentStage = CELEBRATION_STAGE_SETUP
	sCelebrationData.sfForeground = NULL
	sCelebrationData.sfBackground = NULL
	sCelebrationData.sfCelebration = NULL
	sCelebrationData.iEstimatedScreenDuration = 0
	sCelebrationData.iBlackRectangleAlpha = 0
	sCelebrationData.stiCelebrationCam = NULL
	sCelebrationData.iPlayedWinnerAnim = 0
	sCelebrationData.bTriggeredEarlyFlash = FALSE
	sCelebrationData.iRenderphasePauseAttempts = 0
	RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
	RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
	SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
	
	INT i
	REPEAT MAX_PODIUM i
		sCelebrationData.PTFXID_Loop[i] = INT_TO_NATIVE(PTFX_ID, -1)
	ENDREPEAT
	
	PRINTLN("[NETCELEBRATION] Celebration screen initialised by ", GET_THIS_SCRIPT_NAME())
ENDPROC

PROC INITIALISE_CELEBRATION_SCREEN_DATA_SERVER(CELEB_SERVER_DATA &sCelebServer)
	
	INT i
	REPEAT MAX_PODIUM i
		sCelebServer.iRand[i] = -1
	ENDREPEAT
	
	REPEAT NUM_NETWORK_PLAYERS i
		sCelebServer.eArenaClips[i] = ARENA_CELEB_INVALID
	ENDREPEAT
	
	sCelebServer.iRandomPropValue = -1
	
	PRINTLN("[NETCELEBRATION] INITIALISE_CELEBRATION_SCREEN_DATA_SERVER initialised by ", GET_THIS_SCRIPT_NAME())
ENDPROC

/// PURPOSE:
///    Resets the celebration screen data but keeps the scaleform movies loaded in memory. This is useful if you need to trigger multiple celebration screens in succession
///    (e.g. a race summary followed by winner display).
PROC RESET_CELEBRATION_SCREEN_STATE(CELEBRATION_SCREEN_DATA &sCelebrationData)
	sCelebrationData.eCurrentStage = CELEBRATION_STAGE_SETUP
	sCelebrationData.iEstimatedScreenDuration = 0
	sCelebrationData.iBlackRectangleAlpha = 0
	sCelebrationData.stiCelebrationCam = NULL
	sCelebrationData.iPlayedWinnerAnim = 0
	sCelebrationData.bTriggeredEarlyFlash = FALSE
	sCelebrationData.iRenderphasePauseAttempts = 0
	RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
	RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
	IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClone)
		DELETE_PED(sCelebrationData.pedWinnerClone)
	ENDIF
	PRINTLN("[NETCELEBRATION] Celebration screen state reset by ", GET_THIS_SCRIPT_NAME())
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Requests any assets required for the celebration screen.
PROC REQUEST_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, CELEBRATION_SCREEN_TYPE eCelebrationScreenType = CELEBRATION_STANDARD)

	STRING strBack
	STRING strFore
	STRING strCeleb
	
	SWITCH eCelebrationScreenType
		
		CASE CELEBRATION_STANDARD
			strBack = "MP_CELEBRATION_BG"
			strFore = "MP_CELEBRATION_FG"
			strCeleb = "MP_CELEBRATION"
		BREAK
		
		CASE CELEBRATION_HEIST
			strBack = "HEIST_CELEBRATION_BG"
			strFore = "HEIST_CELEBRATION_FG"
			strCeleb = "HEIST_CELEBRATION"
		BREAK
		
		CASE CELEBRATION_GANGOPS
			strBack = "HEIST2_CELEBRATION_BG"
			strFore = "HEIST2_CELEBRATION_FG"
			strCeleb = "HEIST2_CELEBRATION"
		BREAK
		
	ENDSWITCH
	
	IF sCelebrationData.sfForeground = NULL
		sCelebrationData.sfForeground = REQUEST_SCALEFORM_MOVIE(strBack)
		PRINTLN("[NETCELEBRATION] REQUEST_SCALEFORM_MOVIE(", strBack, ") ", GET_THIS_SCRIPT_NAME())
	ENDIF
	
	IF sCelebrationData.sfBackground = NULL
		sCelebrationData.sfBackground = REQUEST_SCALEFORM_MOVIE(strFore)
		PRINTLN("[NETCELEBRATION] REQUEST_SCALEFORM_MOVIE(", strFore, ") ", GET_THIS_SCRIPT_NAME())
	ENDIF
	
	IF sCelebrationData.sfCelebration = NULL
		sCelebrationData.sfCelebration = REQUEST_SCALEFORM_MOVIE(strCeleb)
		PRINTLN("[NETCELEBRATION] REQUEST_SCALEFORM_MOVIE(", strCeleb,") ", GET_THIS_SCRIPT_NAME())
	ENDIF
	
	IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.eLanguage != GET_CURRENT_LANGUAGE() 
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.eLanguage = GET_CURRENT_LANGUAGE() 
		PRINTLN("REQUEST_CELEBRATION_SCREEN - Language is ",GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.eLanguage)
	ENDIF
	
	PRINTLN("[NETCELEBRATION] Celebration scaleform requested by ", GET_THIS_SCRIPT_NAME())
	DEBUG_PRINTCALLSTACK()
	
ENDPROC

/// PURPOSE:
///    Returns TRUE once all required assets for the celebration screen have loaded.
FUNC BOOL HAS_CELEBRATION_SCREEN_LOADED(CELEBRATION_SCREEN_DATA &sCelebrationData)
	IF HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfForeground)
	AND HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfBackground)
	AND HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfCelebration)
		PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SCREEN_LOADED TRUE")
		RETURN TRUE
	ELSE
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfForeground)
			PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SCREEN_LOADED false : sfForeground")
		ENDIF
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfBackground)
			PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SCREEN_LOADED false : sfBackground")
		ENDIF
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfCelebration)
			PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SCREEN_LOADED false : sfCelebration")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PLAY_MP_CELEB_PRELOAD_POST_FX(BOOL &bShownPreloadFX, SCRIPT_TIMER &timerCelebPreLoadPostFxBeenPlaying)
	
	IF NOT ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
		IF NOT bShownPreloadFX
			ANIMPOSTFX_PLAY("MP_Celeb_Preload_Fade", 0, TRUE)
			PLAY_SOUND_FRONTEND(-1,"Pre_Screen_Stinger","DLC_HEISTS_PREP_SCREEN_SOUNDS", FALSE)
			PRINTLN("[SAC] - [RCC MISSION] - [NETCELEBRATION] - [CELEB_AUDIO] - PLAY_MP_CELEB_PRELOAD_POST_FX - played Pre_Screen_Stinger, DLC_HEISTS_PREP_SCREEN_SOUNDS")
			bShownPreloadFX = TRUE
			IF NOT HAS_NET_TIMER_STARTED(timerCelebPreLoadPostFxBeenPlaying)
				START_NET_TIMER(timerCelebPreLoadPostFxBeenPlaying)
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - ANIMPOSTFX_PLAY = MP_Celeb_Preload_Fade - SOUND = Mission_Pass_Notify")
			ENDIF
			PRINTLN("[NETCELEBRATION] - PLAY_MP_CELEB_PRELOAD_POST_FX - bPlayPreLoadPostFx = TRUE.")
		ELSE
			PRINTLN("[NETCELEBRATION] - PLAY_MP_CELEB_PRELOAD_POST_FX - Not playing as bShownPreloadFX = TRUE")
		ENDIF
		DEBUG_PRINTCALLSTACK()
	ENDIF
	
ENDPROC

PROC PLAY_CELEB_WIN_POST_FX()
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[NETCELEBRATION] - PLAY_CELEB_WIN_POST_FX has been called.")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
	ENDIF
	
	ANIMPOSTFX_PLAY("MP_Celeb_Win", 0, TRUE)
	
ENDPROC

PROC PLAY_CELEB_LOSE_POST_FX()
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[NETCELEBRATION] - PLAY_CELEB_LOSE_POST_FX has been called.")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
	ENDIF
	
	ANIMPOSTFX_PLAY("MP_Celeb_Lose", 0, TRUE)
	
ENDPROC

PROC PLAY_CELEB_WIN_OUT_POST_FX()
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[NETCELEBRATION] - PLAY_CELEB_WIN_OUT_POST_FX has been called.")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Win")
		ANIMPOSTFX_STOP("MP_Celeb_Win")
	ENDIF
	
	ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
	
ENDPROC

PROC PLAY_CELEB_LOSE_OUT_POST_FX()
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[NETCELEBRATION] - PLAY_CELEB_LOSE_OUT_POST_FX has been called.")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Lose")
		ANIMPOSTFX_STOP("MP_Celeb_Lose")
	ENDIF
	
	ANIMPOSTFX_PLAY("MP_Celeb_Lose_Out", 0, FALSE)
	
ENDPROC

PROC TRIGGER_CELEBRATION_PRE_LOAD(CELEBRATION_SCREEN_DATA &sCelebrationData, BOOL bPlayPreLoadPostFx = TRUE)
	
	IF IS_THIS_IS_A_STRAND_MISSION()
		EXIT
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		bPlayPreLoadPostFx = FALSE
		PRINTLN("[NETCELEBRATION] - player not net ok, setting bPlayPreLoadPostFx = FALSE.")
	ENDIF
	
	IF NOT g_bCelebrationPreLoadTriggered
		IF bPlayPreLoadPostFx
			IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
				IF GET_REQUESTINGNIGHTVISION()
				OR GET_USINGNIGHTVISION()
					DISABLE_NIGHT_VISION_CONTROLLER(TRUE) 
					ENABLE_NIGHTVISION(VISUALAID_OFF)
				ENDIF
				PLAY_MP_CELEB_PRELOAD_POST_FX(sCelebrationData.bShownPreloadFX, sCelebrationData.timerCelebPreLoadPostFxBeenPlaying)
			ENDIF
		ENDIF
		g_bCelebrationPreLoadTriggered = TRUE
		PRINTLN("[NETCELEBRATION] - TRIGGER_CELEBRATION_PRE_LOAD - g_bCelebrationPreLoadTriggered set TRUE.")
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC

PROC RESET_CELEBRATION_PRE_LOAD(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	sCelebrationData.bPreLoadComplete = FALSE
	g_bCelebrationPreLoadTriggered = FALSE
	RESET_NET_TIMER(sCelebrationData.resetPreLoadForSepctatorTimer)
	
	IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
		PRINTLN("[NETCELEBRATION] - RESET_CELEBRATION_PRE_LOAD - stopped MP_Celeb_Preload_Fade")
	ENDIF
	
ENDPROC

PROC MAINTAIN_CELEBRATION_PRE_LOAD(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	IF NOT sCelebrationData.bPreLoadComplete
		
		IF g_bCelebrationPreLoadTriggered
			
			REQUEST_CELEBRATION_SCREEN(sCelebrationData)
			
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
				sCelebrationData.bPreLoadComplete = TRUE
				PRINTLN("[NETCELEBRATION] - celebration screen pre load complete.")
				DEBUG_PRINTCALLSTACK()
			ENDIF
			
		ENDIF
	
	ELSE
		
		IF IS_PLAYER_SPECTATING(PLAYER_ID())
		AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		AND NOT IS_THIS_A_SURVIVAL() // Survival makes repeated use of it, and you can come back from being a spectator.
			IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.resetPreLoadForSepctatorTimer)
				
				START_NET_TIMER(sCelebrationData.resetPreLoadForSepctatorTimer)
				
			ELIF HAS_NET_TIMER_EXPIRED(sCelebrationData.resetPreLoadForSepctatorTimer, 5000)
				
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sCelebrationData.sfForeground)
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sCelebrationData.sfBackground)
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sCelebrationData.sfCelebration)
				sCelebrationData.bPreLoadComplete = FALSE
				g_bCelebrationPreLoadTriggered = FALSE
				RESET_NET_TIMER(sCelebrationData.resetPreLoadForSepctatorTimer)
				PRINTLN("[NETCELEBRATION] - celebration screen pre load reset, player is a spectator.")
				DEBUG_PRINTCALLSTACK()
			
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Stops the currently playing celebration screen anim, useful for cleaning up if there's a chance the anim could still be playing.
PROC STOP_CELEBRATION_SCREEN_ANIM(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID)
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[NETCLEBRATION] - [SAC] - STOP_CELEBRATION_SCREEN_ANIM - has been called. Callstack:")
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[NETCLEBRATION] - [SAC] - STOP_CELEBRATION_SCREEN_ANIM - strScreenID = ", strScreenID)
	#ENDIF
	
	IF sCelebrationData.sfForeground != NULL
	AND HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfForeground)
		BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "CLEANUP")
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
		PRINTLN("[NETCLEBRATION] - [SAC] - STOP_CELEBRATION_SCREEN_ANIM - cleaned up sfForeground.")
	ENDIF
	
	IF sCelebrationData.sfBackground != NULL
	AND HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfBackground)
		BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "CLEANUP")
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
		PRINTLN("[NETCLEBRATION] - [SAC] - STOP_CELEBRATION_SCREEN_ANIM - cleaned up sfBackground.")
	ENDIF
	
	IF sCelebrationData.sfCelebration != NULL
	AND HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfCelebration)
		BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "CLEANUP")
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
		PRINTLN("[NETCLEBRATION] - [SAC] - STOP_CELEBRATION_SCREEN_ANIM - cleaned up sfCelebration.")
	ENDIF
	
ENDPROC

PROC CLEAR_CELEBRATION_SCREEN_MOVIE_IDS(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	IF sCelebrationData.sfForeground != NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sCelebrationData.sfForeground)
		sCelebrationData.sfForeground = NULL
	ENDIF
	
	IF sCelebrationData.sfBackground != NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sCelebrationData.sfBackground)
		sCelebrationData.sfBackground = NULL
	ENDIF
	
	IF sCelebrationData.sfCelebration != NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sCelebrationData.sfCelebration)
		sCelebrationData.sfCelebration = NULL
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Removes any assets used for the celebration screen.
PROC CLEANUP_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, BOOL bSetAsNotActive = TRUE, STRING strScreenID = NULL, BOOL bJoinAsSpectator = FALSE)

	IF NOT IS_STRING_NULL_OR_EMPTY(strScreenID)
		STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, strScreenID)
	ENDIF
	
	CLEAR_CELEBRATION_SCREEN_MOVIE_IDS(sCelebrationData)
	
	IF DOES_CAM_EXIST(sCelebrationData.camArenaWinner)
		SET_CAM_ACTIVE(sCelebrationData.camArenaWinner, FALSE)
		DESTROY_CAM(sCelebrationData.camArenaWinner)
		PRINTLN("[NETCELEBRATION] Celebration cam winner cleaned up by ", GET_THIS_SCRIPT_NAME())
	ENDIF
	
	INT i
	
	REPEAT 8 i
	IF sCelebrationData.playerNameMovies[i] != NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sCelebrationData.playerNameMovies[i])
		sCelebrationData.playerNameMovies[i] = NULL
	ENDIF
	ENDREPEAT
	
	PRINTLN("[NETCELEBRATION] Celebration scaleform cleaned up by ", GET_THIS_SCRIPT_NAME())
	
	REMOVE_CORONA_IDLE_ANIMS()
	
	CLEANUP_WINNER_SCENE_ENTITIES(sCelebrationData)
	RESET_CELEBRATION_SCREEN_STATE(sCelebrationData)
	IF bSetAsNotActive
		SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
	ENDIF
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,DEFAULT,DEFAULT,bJoinAsSpectator)
	
	#IF IS_DEBUG_BUILD
		iArenaCelebNumPlayers = -1
	#ENDIF
	
	g_bCelebrationPodiumIsActive = FALSE	
	
ENDPROC

/// PURPOSE:
///    Begins the creation of a celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this celebration screen. All operations performed on the screen must use this name.
///    strBackgroundColour - The background colour, uses the hud colour enum (e.g. HUD_COLOUR_BLACK, HUD_COLOUR_FREEMODE, etc)
PROC CREATE_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, STRING strBackgroundColour, BOOL bBlackOverride = TRUE, INT iSequenceType = 0)
	
	//1637943 - Currently just making it black in all cases.
	IF bBlackOverride
		strBackgroundColour = "HUD_COLOUR_BLACK"
	ENDIF
	
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		PRINTLN("[SHARD][JR] - Checking celebration is for passing mission: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission)
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission
			PRINTLN("[SHARD][JR] - Setting background colour to HUD_COLOUR_HDARK.")
			strBackgroundColour = "HUD_COLOUR_HARK" 
		ENDIF
	ENDIF
	PRINTLN("[SHARD][JR] - Current strBackgroundColour is ", strBackgroundColour)
	STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, strScreenID)
	
	RESET_GLOBAL_ACTIONSCRIPT_FLAG(ACTIONSCRIPT_GLOBAL_ID)
	PRINTLN("[NETCELEBRATION] - [SAC] - CREATE_CELEBRATION_SCREEN - called RESET_GLOBAL_ACTIONSCRIPT_FLAG(ACTIONSCRIPT_GLOBAL_ID).")
	
	IF sCelebrationData.sfForeground != NULL
	AND HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfForeground)
		BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "CREATE_STAT_WALL")
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strBackgroundColour)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			IF iSequenceType > 0
			AND iSequenceType <= 3
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSequenceType)
			ENDIF
			
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	IF sCelebrationData.sfBackground != NULL
	AND HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfBackground)
		BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "CREATE_STAT_WALL")
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strBackgroundColour)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			IF iSequenceType > 0
			AND iSequenceType <= 3
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSequenceType)
			ENDIF
			
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	IF sCelebrationData.sfCelebration != NULL
	AND HAS_SCALEFORM_MOVIE_LOADED(sCelebrationData.sfCelebration)
		BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "CREATE_STAT_WALL")
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strBackgroundColour)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			IF iSequenceType > 0
			AND iSequenceType <= 3
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSequenceType)
			ENDIF
			
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	PRINTLN("[NETCELEBRATION] Celebration screen started by ", GET_THIS_SCRIPT_NAME())
ENDPROC

/// PURPOSE:
///    Adds a background to the celebration screen, colour is determined by the colour passed to CREATE_CELEBRATION_SCREEN.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this celebration screen. All the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iAlphs - Use 100 for full backgrounds
///    iCustomTexture - cutsom screen texture at sides of celebration. 2= heist, 4 = race flags
PROC ADD_BACKGROUND_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iAlpha = -1, INT iCustomTexture = 0)
	
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		iCustomTexture = 1
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_BACKGROUND_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
			PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added screenId: ", strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		IF iAlpha > -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
			PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added alpha: ", iAlpha)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(75)
			PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added alpha: ", 75)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCustomTexture)
		PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added mesh int: ", iCustomTexture)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_BACKGROUND_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
			PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added screenId: ", strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		IF iAlpha > -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
			PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added alpha: ", iAlpha)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(75)
			PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added alpha: ", 75)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCustomTexture)
		PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added mesh int: ", iCustomTexture)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_BACKGROUND_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
			PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added screenId: ", strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		IF iAlpha > -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
			PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added alpha: ", iAlpha)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(75)
			PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added alpha: ", 75)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCustomTexture)
		PRINTLN("[NETCELEBRATION] - ADD_BACKGROUND_TO_CELEBRATION_SCREEN - foreground - added mesh int: ", iCustomTexture)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE:
///    Performs a white flash on the celebration screen, use this for transitions (e.g. from the summary screen to the winner screen.
PROC FLASH_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, FLOAT fFadeInTime = 0.25, FLOAT fHoldTime = 0.15, FLOAT fFadeOutTime = 0.25)
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "SHOW_FLASH")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeInTime) //inDuration
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fHoldTime) //holdDuration
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeOutTime) //outDuration
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("HUD_COLOUR_WHITE")
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_BACKGROUND_TO_WALL")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeInTime) //inDuration
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fHoldTime) //holdDuration
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeOutTime) //outDuration
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("HUD_COLOUR_WHITE")
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_BACKGROUND_TO_WALL")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeInTime) //inDuration
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fHoldTime) //holdDuration
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeOutTime) //outDuration
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("HUD_COLOUR_WHITE")
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a "You Finished X" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iPosition - The position that the local player finished.
///    strStrapLine - Text label of strapline text. Will be 'YOU FINISHED" if left null.
///    NOTE: If iPosition is set to -1 then it'll treat as DNF and display appropriate text.
PROC ADD_FINISH_POSITION_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iPosition, STRING strStrapLine = NULL)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_POSITION_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF iPosition > -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPosition)
			
			IF NOT IS_STRING_NULL(strStrapLine)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStrapLine)
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_POSITION_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF iPosition > -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPosition)
			
			IF NOT IS_STRING_NULL(strStrapLine)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStrapLine)
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_POSITION_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF iPosition > -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPosition)
			
			IF NOT IS_STRING_NULL(strStrapLine)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStrapLine)
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

FUNC BOOL IS_CELEBRATION_FOR_NON_MATCH_OR_LAST_ROUND_OF_MATCH()
	
	IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
	#IF IS_DEBUG_BUILD
	OR g_bFakeRound
	#ENDIF
		IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
		#IF IS_DEBUG_BUILD
		OR g_bFakeEndOfMatch
		#ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC RESET_GLOBAL_CELEBRATION_ROUNDS_DATA()
	CELEBRATION_ROUNDS_GLOBALS sTemp
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData = sTemp
ENDPROC

PROC ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iCash, STRING strReason, STRING strReasonValue, BOOL bDroppedCash = FALSE, BOOL bBlockReasonText = FALSE, BOOL bBlockReasonValue = FALSE)
	
	TEXT_LABEL_23 tl23_cashType = "CEL_BCASH"
	TEXT_LABEL_23 tl23_cash = ""
	tl23_cash += iCash
	
	IF bDroppedCash
		tl23_cashType = "CEL_DRCASH"
	ENDIF
	
	PRINTLN("[NETCELEBRATION] - [SAC] - ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN - been called.")
	PRINTLN("[NETCELEBRATION] - [SAC] - ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN - tl23_cashType = ", tl23_cashType)
	PRINTLN("[NETCELEBRATION] - [SAC] - ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN - iCash = ", iCash)
	PRINTLN("[NETCELEBRATION] - [SAC] - ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN - tl23_cash = ", tl23_cash)
	PRINTLN("[NETCELEBRATION] - [SAC] - ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN - strReason = ", strReason)
	PRINTLN("[NETCELEBRATION] - [SAC] - ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN - strReasonValue = ", strReasonValue)
	PRINTLN("[NETCELEBRATION] - [SAC] - ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN - bDroppedCash = ", bDroppedCash)
	DEBUG_PRINTCALLSTACK()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_MISSION_RESULT_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl23_cashType)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELEB_CASHSYM")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl23_cash)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF NOT bBlockReasonText
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strReason)
				IF NOT bBlockReasonValue
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strReasonValue)
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_MISSION_RESULT_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl23_cashType)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELEB_CASHSYM")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl23_cash)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF NOT bBlockReasonText
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strReason)
				IF NOT bBlockReasonValue
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strReasonValue)
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_MISSION_RESULT_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl23_cashType)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELEB_CASHSYM")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl23_cash)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF NOT bBlockReasonText
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strReason)
				IF NOT bBlockReasonValue
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strReasonValue)
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE:
///    Adds a "Mission Passed/Failed" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    bPassed - TRUE if the player passed the mission.
PROC ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, CELEBRATION_JOB_TYPE eJobType, BOOL bPassed, STRING strFailReason, STRING strFailLiteral, STRING strFailLiteral2)
	STRING strResultLabel
	STRING strMissionTypeLabel
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[SAC] - [NETCELEBRATION] - [RCC MISSION] - ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN has been called.")
		PRINTLN("[SAC] - [NETCELEBRATION] - [RCC MISSION] - strScreenID = ", strScreenID)
		PRINTLN("[SAC] - [NETCELEBRATION] - [RCC MISSION] - bPassed = ", bPassed)
		PRINTLN("[SAC] - [NETCELEBRATION] - [RCC MISSION] - strFailReason = ", strFailReason)
		BOOL bEmpty = IS_STRING_NULL_OR_EMPTY(strFailLiteral) 
		PRINTLN("[SAC] - [NETCELEBRATION] - [RCC MISSION] - strFailLiteral is empty = ", bEmpty)
		PRINTLN("[SAC] - [NETCELEBRATION] - [RCC MISSION] - strFailLiteral = ", strFailLiteral)
		bEmpty = IS_STRING_NULL_OR_EMPTY(strFailLiteral2) 
		PRINTLN("[SAC] - [NETCELEBRATION] - [RCC MISSION] - strFailLiteral2 is empty = ", bEmpty)
		PRINTLN("[SAC] - [NETCELEBRATION] - [RCC MISSION] - strFailLiteral2 = ", strFailLiteral2)
	#ENDIF
	
	IF IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION()
	#IF IS_DEBUG_BUILD
	OR g_bFakeRound
	#ENDIF
	
		IF bPassed
			strResultLabel = "CELEB_WINNER"
		ELSE
			strResultLabel = "CELEB_LOSER"
		ENDIF
			
		IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
		#IF IS_DEBUG_BUILD
		OR g_bFakeEndOfMatch
		#ENDIF
			strMissionTypeLabel = "CELEB_MATCH"
		ELSE
			strMissionTypeLabel = "CELEB_ROUND"
		ENDIF
		
	ELSE
		
		IF eJobType = CELEBRATION_JOB_TYPE_MISSION_EARLY_FAIL
			strResultLabel = "CELEB_OUT_OF_LIVES"
		ELSE
			IF eJobType = CELEBRATION_JOB_TYPE_SURVIVAL
			OR eJobType = CELEBRATION_JOB_TYPE_MISSION
			OR eJobType = CELEBRATION_JOB_TYPE_DEATHMATCH_OBJECTIVE
			OR eJobType = CELEBRATION_JOB_TYPE_GANG_ATTACK
			
			OR eJobType = CELEBRATION_JOB_TYPE_HEIST_FINALE
			OR eJobType = CELEBRATION_JOB_TYPE_HEIST_PREP
			
			OR eJobType = CELEBRATION_JOB_TYPE_PILOT_SCHOOL
				IF bPassed
					strResultLabel = "CELEB_PASSED"
				ELSE
					strResultLabel = "CELEB_FAILED"
				ENDIF
			ELSE
				IF bPassed
					strResultLabel = "CELEB_WINNER"
				ELSE
					strResultLabel = "CELEB_LOSER"
				ENDIF
			ENDIF
		ENDIF
		
		IF eJobType = CELEBRATION_JOB_TYPE_DEATHMATCH_OBJECTIVE
			strMissionTypeLabel = "CELEB_OBJECTIVE"
		ELIF eJobType = CELEBRATION_JOB_TYPE_SURVIVAL
			strMissionTypeLabel = "CELEB_SURVIVAL"
		ELIF eJobType = CELEBRATION_JOB_TYPE_GANG_ATTACK
			strMissionTypeLabel = "CELEB_GANG_ATTACK"
			
		ELIF eJobType = CELEBRATION_JOB_TYPE_HEIST_FINALE
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				SWITCH GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION()
					CASE ciGANGOPS_FLOW_MISSION_IAABASE_FINALE
						strMissionTypeLabel = "CELEB_ACT_1"
					BREAK
					CASE ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE
						strMissionTypeLabel = "CELEB_ACT_2"
					BREAK
					CASE ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2
					CASE ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE
						strMissionTypeLabel = "CELEB_ACT_3"
					BREAK
				ENDSWITCH
			ELSE
				strMissionTypeLabel = "CELEB_HEIST"
			ENDIF
		ELIF eJobType = CELEBRATION_JOB_TYPE_HEIST_PREP
			strMissionTypeLabel = "FMMC_RSTAR_HP"
		
		ELIF eJobType = CELEBRATION_JOB_TYPE_LTS
		OR eJobType = CELEBRATION_JOB_TYPE_MISSION_EARLY_FAIL
		OR eJobType = CELEBRATION_JOB_TYPE_VS_MISSION
			strMissionTypeLabel = ""
		ELIF eJobType = CELEBRATION_JOB_TYPE_PILOT_SCHOOL
			strMissionTypeLabel = "CELEB_LESSON"
		ELIF g_FMMC_STRUCT.iCelebrationType = FMMC_CELEBRATION_TYPE_TUNER_ROBBERY
			strMissionTypeLabel = "CELEB_ROBBERY"
		ELSE
			strMissionTypeLabel = "CELEB_MISSION"
		ENDIF
		
	ENDIF
	
	BOOL bDontDisplayFailReason

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_MISSION_RESULT_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF IS_STRING_NULL_OR_EMPTY(strMissionTypeLabel)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strMissionTypeLabel)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strMissionTypeLabel)
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strResultLabel)
		
		//Display a fail reason only if the mission failed or we're displaying a deathmat
		IF (NOT bPassed OR eJobType = CELEBRATION_JOB_TYPE_DEATHMATCH_OBJECTIVE)
			IF NOT IS_STRING_NULL_OR_EMPTY(strFailReason)
				IF NOT IS_STRING_NULL_OR_EMPTY(strFailLiteral) 
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strFailReason)
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strFailLiteral)
						IF NOT IS_STRING_NULL_OR_EMPTY(strFailLiteral2) 
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strFailLiteral2)
						ENDIF
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strFailReason)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
			ELSE
				bDontDisplayFailReason = TRUE
			ENDIF
		ELSE
			bDontDisplayFailReason = TRUE
		ENDIF
		
		IF bDontDisplayFailReason
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
			
			PRINTLN("[LM][ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN][TURF WAR] - Overriding HUD COLOUR based on team colour.")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(GET_PLAYER_TEAM(PLAYER_ID()), PLAYER_ID())))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_MISSION_RESULT_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF IS_STRING_NULL_OR_EMPTY(strMissionTypeLabel)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strMissionTypeLabel)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strMissionTypeLabel)
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strResultLabel)
		
		//Display a fail reason only if the mission failed or we're displaying a deathmat
		IF (NOT bPassed OR eJobType = CELEBRATION_JOB_TYPE_DEATHMATCH_OBJECTIVE)
			IF NOT IS_STRING_NULL_OR_EMPTY(strFailReason)
				IF NOT IS_STRING_NULL_OR_EMPTY(strFailLiteral) 
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strFailReason)
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strFailLiteral)
						IF NOT IS_STRING_NULL_OR_EMPTY(strFailLiteral2) 
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strFailLiteral2)
						ENDIF
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strFailReason)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
			ELSE
				bDontDisplayFailReason = TRUE
			ENDIF
		ELSE
			bDontDisplayFailReason = TRUE
		ENDIF
		
		IF bDontDisplayFailReason
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
			
			PRINTLN("[LM][ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN][TURF WAR] - Overriding HUD COLOUR based on team colour.")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(GET_PLAYER_TEAM(PLAYER_ID()), PLAYER_ID())))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_MISSION_RESULT_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF IS_STRING_NULL_OR_EMPTY(strMissionTypeLabel)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strMissionTypeLabel)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strMissionTypeLabel)
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strResultLabel)
		
		//Display a fail reason only if the mission failed or we're displaying a deathmat
		IF (NOT bPassed OR eJobType = CELEBRATION_JOB_TYPE_DEATHMATCH_OBJECTIVE)
			IF NOT IS_STRING_NULL_OR_EMPTY(strFailReason)
				IF NOT IS_STRING_NULL_OR_EMPTY(strFailLiteral) 
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strFailReason)
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strFailLiteral)
						IF NOT IS_STRING_NULL_OR_EMPTY(strFailLiteral2) 
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strFailLiteral2)
						ENDIF
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strFailReason)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
			ELSE
				bDontDisplayFailReason = TRUE
			ENDIF
		ELSE
			bDontDisplayFailReason = TRUE
		ENDIF
		
		IF bDontDisplayFailReason
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
			
			PRINTLN("[LM][ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN][TURF WAR] - Overriding HUD COLOUR based on team colour.")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(GET_PLAYER_TEAM(PLAYER_ID()), PLAYER_ID())))
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC ADD_OBJECTIVE_RESULT_TO_WALL(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, STRING strObjectiveLabel, BOOL bPassed)
	STRING strResultLabel
	
	IF bPassed
		strResultLabel = "CELEB_OBJECTIVE_PASSED"
	ELSE
		strResultLabel = "CELEB_OBJECTIVE_FAILED"
	ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_OBJECTIVE_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strResultLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strObjectiveLabel)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_OBJECTIVE_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strResultLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strObjectiveLabel)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_OBJECTIVE_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strResultLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strObjectiveLabel)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a "Time X" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iFinishTime - The player's finishing time.
///    iPersonalBest - The player's personal best.
PROC ADD_FINISH_TIME_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iFinishTime, INT iPersonalBest)
	STRING strTimeLabel
	
	PRINTLN("[NETCELEBRATION] - ADD_FINISH_TIME_TO_CELEBRATION_SCREEN being called.")
	PRINTLN("[NETCELEBRATION] - ADD_FINISH_TIME_TO_CELEBRATION_SCREEN - iFinishTime = ", iFinishTime)
	PRINTLN("[NETCELEBRATION] - ADD_FINISH_TIME_TO_CELEBRATION_SCREEN - iPersonalBest = ", iPersonalBest)
	
	//If the player doesn't have a personal best then just make it equal to the finish time.
	IF iPersonalBest <= 0
		iPersonalBest = iFinishTime
		PRINTLN("[NETCELEBRATION] - ADD_FINISH_TIME_TO_CELEBRATION_SCREEN - iPersonalBest <= 0. Setting to equal iFinishTime. iPersonalBest now equals ", iPersonalBest)
	ENDIF
	
	INT iTimeDiff = iFinishTime - iPersonalBest
	PRINTLN("[NETCELEBRATION] - ADD_FINISH_TIME_TO_CELEBRATION_SCREEN - iTimeDiff = iFinishTime - iPersonalBest = ", iTimeDiff)
	
	IF iTimeDiff < 0 
		iTimeDiff = iTimeDiff*-1
		PRINTLN("[NETCELEBRATION] - ADD_FINISH_TIME_TO_CELEBRATION_SCREEN - iTimeDiff < 0. Multiplaying by -1. iTimeDiff now equals ", iTimeDiff)
	ENDIF
	
	IF iFinishTime >= iPersonalBest
		strTimeLabel = "CELEB_TIME"
		PRINTLN("[NETCELEBRATION] - ADD_FINISH_TIME_TO_CELEBRATION_SCREEN - iFinishTime >= iPersonalBest, setting strTimeLabel = CELEB_TIME = ", strTimeLabel)
	ELSE
		strTimeLabel = "CELEB_PERSONAL_BEST"
		PRINTLN("[NETCELEBRATION] - ADD_FINISH_TIME_TO_CELEBRATION_SCREEN - iFinishTime < iPersonalBest, setting strTimeLabel = CELEB_PERSONAL_BEST = ", strTimeLabel)
	ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_TIME_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFinishTime)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strTimeLabel)
		
		IF iFinishTime < iPersonalBest
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTimeDiff)
			PRINTLN("[NETCELEBRATION] - ADD_FINISH_TIME_TO_CELEBRATION_SCREEN - iFinishTime < iPersonalBest. Called SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTimeDiff) with iTimeDiff = ", iTimeDiff)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_TIME_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFinishTime)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strTimeLabel)
		
		IF iFinishTime < iPersonalBest
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTimeDiff)
			PRINTLN("[NETCELEBRATION] - ADD_FINISH_TIME_TO_CELEBRATION_SCREEN - iFinishTime < iPersonalBest. Called SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTimeDiff) with iTimeDiff = ", iTimeDiff)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_TIME_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFinishTime)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strTimeLabel)
		
		IF iFinishTime < iPersonalBest
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTimeDiff)
			PRINTLN("[NETCELEBRATION] - ADD_FINISH_TIME_TO_CELEBRATION_SCREEN - iFinishTime < iPersonalBest. Called SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTimeDiff) with iTimeDiff = ", iTimeDiff)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a "World Record" message to the celebration screen if the player broke the world record.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iFinishTime - The player's finishing time.
PROC ADD_WORLD_RECORD_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iFinishTime)
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_WORLD_RECORD_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFinishTime)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_WORLD_RECORD_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFinishTime)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_WORLD_RECORD_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFinishTime)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a "JP +X" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iJobPoints - The number of Job Points the local player received for this job.
PROC ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iJobPoints, BOOL bOverrideAlign = FALSE, BOOL bAlignLeft = FALSE)
	
	PRINTLN("[NETCELEBRATION] - ADD_JOB_POINTS_TO_CELEBRATION_SCREEN has been called. JP: ", iJobPoints)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_JOB_POINTS_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iJobPoints)
		
		IF bOverrideAlign
			IF bAlignLeft
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_JOB_POINTS_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iJobPoints)
		
		IF bOverrideAlign
			IF bAlignLeft
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_JOB_POINTS_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iJobPoints)
		
		IF bOverrideAlign
			IF bAlignLeft
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a "Cash +X" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iCash - The amount of cash the local player received for this job.
PROC ADD_CASH_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iCash, BOOL bOverrideAlign = FALSE, BOOL bAlignLeft = FALSE)
	
	PRINTLN("[NETCELEBRATION] - ADD_CASH_TO_CELEBRATION_SCREEN has been called. Cash: $", iCash)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_CASH_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCash)
		
		IF bOverrideAlign
			IF bAlignLeft
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_CASH_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCash)
		
		IF bOverrideAlign
			IF bAlignLeft
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_CASH_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCash)
		
		IF bOverrideAlign
			IF bAlignLeft
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//
///// PURPOSE:
/////    Adds a "Cash +X" message to the celebration screen.
///// PARAMS:
/////    sCelebrationData - The celebration screen data struct.
/////    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
/////    iCash - The amount of cash the local player received for this job.
//PROC ADD_TOURNAMENT_TIME_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iTime, BOOL bOverrideAlign = FALSE, BOOL bAlignLeft = FALSE)
//	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_CASH_TO_WALL")
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
//		END_TEXT_COMMAND_SCALEFORM_STRING()
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTime)
//		
//		IF bOverrideAlign
//			IF bAlignLeft
//				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
//				END_TEXT_COMMAND_SCALEFORM_STRING()
//			ELSE
//				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
//				END_TEXT_COMMAND_SCALEFORM_STRING()
//			ENDIF
//		ENDIF
//	END_SCALEFORM_MOVIE_METHOD()
//	
//	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_CASH_TO_WALL")
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
//		END_TEXT_COMMAND_SCALEFORM_STRING()
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTime)
//		
//		IF bOverrideAlign
//			IF bAlignLeft
//				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
//				END_TEXT_COMMAND_SCALEFORM_STRING()
//			ELSE
//				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
//				END_TEXT_COMMAND_SCALEFORM_STRING()
//			ENDIF
//		ENDIF
//	END_SCALEFORM_MOVIE_METHOD()
//	
//	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_CASH_TO_WALL")
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
//		END_TEXT_COMMAND_SCALEFORM_STRING()
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTime)
//		
//		IF bOverrideAlign
//			IF bAlignLeft
//				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
//				END_TEXT_COMMAND_SCALEFORM_STRING()
//			ELSE
//				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
//				END_TEXT_COMMAND_SCALEFORM_STRING()
//			ENDIF
//		ENDIF
//	END_SCALEFORM_MOVIE_METHOD()
//ENDPROC

/// PURPOSE:
///    Adds a rep points display and rank bar plus a rank up message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iRPGained - The amount of Reputation Points the local player received for this job.
///    iRPStarted - The total amount of RP the player had before the new points were received.
///    iCurrentLvlStartPoints - The number of RP required to reach the player's current level.
///    iCurrentLvlEndPoints - The number of RP required to reach the player's next level.
///    iCurrentLvl - The player's current level.
///    iNextLvl - The player's next level.
PROC ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iRPGained, INT iRPStarted, INT iCurrentLvlStartPoints, INT iCurrentLvlEndPoints, 
										INT iCurrentLvl, INT iNextLvl)
										
	PRINTLN("[NETCELEBRATION] - ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN has been called.", iRPGained, "RP")
										
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_REP_POINTS_AND_RANK_BAR_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRPGained)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRPStarted)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentLvlStartPoints)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentLvlEndPoints)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentLvl)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNextLvl)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_REP_POINTS_AND_RANK_BAR_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRPGained)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRPStarted)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentLvlStartPoints)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentLvlEndPoints)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentLvl)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNextLvl)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_REP_POINTS_AND_RANK_BAR_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRPGained)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRPStarted)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentLvlStartPoints)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentLvlEndPoints)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentLvl)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNextLvl)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a "Score X" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iScore - The player's score for this job.
PROC ADD_SCORE_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iScore)

	PRINTLN("[NETCELEBRATION] - ADD_SCORE_TO_CELEBRATION_SCREEN has been called. Score: ", iScore)

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_SCORE_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_SCORE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_SCORE_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_SCORE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_SCORE_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_SCORE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC ADD_CASH_WON_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iActualCash, INT iMaxCash, BOOL bFinale, BOOL bAlignLeft = FALSE)
	
	STRING strTemp
	
	IF bFinale 
		strTemp = "CELEB_CUT_TAKE"
	ELSE
		strTemp = "CELEB_PAYMENT"
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_CASH_WON_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strTemp)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iActualCash)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMaxCash)
		IF bAlignLeft
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("left")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_CASH_WON_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strTemp)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iActualCash)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMaxCash)
		IF bAlignLeft
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("left")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_CASH_WON_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strTemp)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iActualCash)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMaxCash)
		IF bAlignLeft
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("left")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

PROC CREATE_INCREMENTAL_CASH_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "CREATE_INCREMENTAL_CASH_ANIMATION")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("SUMMARY")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CASH")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "CREATE_INCREMENTAL_CASH_ANIMATION")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("SUMMARY")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CASH")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "CREATE_INCREMENTAL_CASH_ANIMATION")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("SUMMARY")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CASH")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

PROC ADD_INCREMENTAL_CASH_STEP(CELEBRATION_SCREEN_DATA &sCelebrationData, FLOAT fStartingValue, FLOAT fEndValue, STRING strTopText, STRING strBottomText, STRING strTargetStat, STRING strAchievedStat, INT iPassedChallenge = 3, INT iPercentageCut = -1, INT iOverridePassedSound = -1)
	
	INT iPassedSound
	
	// Audio uses different values than the ticks, need to convert.
	SWITCH iPassedChallenge
		CASE 1	iPassedSound = 1																										BREAK
		CASE 2	iPassedSound = 2																										BREAK
		CASE 3	iPassedSound = 0																										BREAK
		DEFAULT	iPassedSound = 0 #IF IS_DEBUG_BUILD SCRIPT_ASSERT("ADD_INCREMENTAL_CASH_STEP - iPassedChallenge < 1 OR > 3") #ENDIF		BREAK
	ENDSWITCH
	
	IF iOverridePassedSound != -1
		iPassedSound = iOverridePassedSound
		PRINTLN("[SAC] - [NETCELEBRATION] - ADD_INCREMENTAL_CASH_STEP - iOverridePassedSound = ", iOverridePassedSound, ", setting iPassedSound to ",iPassedSound)
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_INCREMENTAL_CASH_WON_STEP")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("SUMMARY")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CASH")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartingValue)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fEndValue)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(strTopText)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strTopText)
				IF iPercentageCut != (-1)
					ADD_TEXT_COMPONENT_INTEGER(iPercentageCut)
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTopText)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strBottomText)
			IF NOT IS_STRING_NULL_OR_EMPTY(strTargetStat)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTargetStat)
			ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strAchievedStat)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPassedChallenge)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPassedSound)
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_INCREMENTAL_CASH_WON_STEP")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("SUMMARY")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CASH")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartingValue)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fEndValue)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(strTopText)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strTopText)
				IF iPercentageCut != (-1)
					ADD_TEXT_COMPONENT_INTEGER(iPercentageCut)
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTopText)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strBottomText)
			IF NOT IS_STRING_NULL_OR_EMPTY(strTargetStat)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTargetStat)
			ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strAchievedStat)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPassedChallenge)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPassedSound)
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_INCREMENTAL_CASH_WON_STEP")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("SUMMARY")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CASH")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStartingValue)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fEndValue)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(strTopText)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strTopText)
				IF iPercentageCut != (-1)
					ADD_TEXT_COMPONENT_INTEGER(iPercentageCut)
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTopText)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strBottomText)
			IF NOT IS_STRING_NULL_OR_EMPTY(strTargetStat)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTargetStat)
			ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strAchievedStat)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPassedChallenge)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPassedSound)
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

PROC ADD_INCREMENTAL_CASH_TO_WALL(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_INCREMENTAL_CASH_ANIMATION_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("SUMMARY")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CASH")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_INCREMENTAL_CASH_ANIMATION_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("SUMMARY")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CASH")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_INCREMENTAL_CASH_ANIMATION_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("SUMMARY")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CASH")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE:
///    Adds a "Total Kills X" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iTotalKills - The number of kills for this job.
PROC ADD_TOTAL_KILLS_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iTotalKills, BOOL bForceAlign = FALSE, BOOL bAlignLeft = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_TOTAL_KILLS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalKills)
		
		IF bForceAlign
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				IF bAlignLeft
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_TOTAL_KILLS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalKills)
		
		IF bForceAlign
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				IF bAlignLeft
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_TOTAL_KILLS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalKills)
		
		IF bForceAlign
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				IF bAlignLeft
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a "Gamertag X" message to the celebration screen, indicating the number of kills.
PROC ADD_GAMERTAG_KILLS_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, STRING strGamerTag, INT iTotalKills, BOOL bForceAlign = FALSE, BOOL bAlignLeft = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strGamerTag)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalKills)
		
		IF bForceAlign
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				IF bAlignLeft
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strGamerTag)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalKills)
		
		IF bForceAlign
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				IF bAlignLeft
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strGamerTag)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalKills)
		
		IF bForceAlign
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				IF bAlignLeft
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("left")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("right")
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a "Gamertag X" message to the celebration screen, indicating the number of kills.
PROC ADD_WAVE_REACHED_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iWaveReached, BOOL bUseWaveCompleteText)
	TEXT_LABEL strWaveReached = iWaveReached

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_WAVE_REACHED_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELEB_WAVE_NUMBER")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strWaveReached)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bUseWaveCompleteText
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_SURVIVED")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_REACHED")
		ENDIF		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_WAVE_REACHED_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELEB_WAVE_NUMBER")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strWaveReached)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bUseWaveCompleteText
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_SURVIVED")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_REACHED")
		ENDIF	
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_WAVE_REACHED_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELEB_WAVE_NUMBER")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strWaveReached)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bUseWaveCompleteText
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_SURVIVED")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_REACHED")
		ENDIF	
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC CREATE_CELEBRATION_STAT_TABLE(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, STRING strTableID)
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "CREATE_STAT_TABLE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "CREATE_STAT_TABLE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "CREATE_STAT_TABLE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()		
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC ADD_ITEM_TO_CELEBRATION_STAT_TABLE(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, STRING strTableID, STRING strStatName, STRING strStat, 
									    BOOL bIsStatNameRawText, BOOL bIsStatValueRawText, BOOL bUseBigText, BOOL bFormatAsTime = FALSE, HUD_COLOURS eHudColour = HUD_COLOUR_PINK, FLOAT fStat = 0.0, BOOL bPassFloat = FALSE)
	
	STRING stHudColour
	
	SWITCH eHudColour
		CASE HUD_COLOUR_PLATINUM
			stHudColour = "HUD_COLOUR_PLATINUM"
		BREAK
		CASE HUD_COLOUR_GOLD
			stHudColour = "HUD_COLOUR_GOLD"
		BREAK
		CASE HUD_COLOUR_SILVER
			stHudColour = "HUD_COLOUR_SILVER"
		BREAK
		CASE HUD_COLOUR_BRONZE
			stHudColour = "HUD_COLOUR_BRONZE"
		BREAK
		DEFAULT
			stHudColour = "HUD_COLOUR_WHITE"
		BREAK
	ENDSWITCH
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_STAT_TO_TABLE")
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding strScreenID = ", strScreenID)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding strTableID = ", strTableID)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bIsStatNameRawText
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding raw text strStatName = ", strStatName)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStatName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding non raw text strStatName = ", strStatName)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStatName)
		ENDIF
		
		IF bPassFloat
			
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding fStat = ", fStat)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
				ADD_TEXT_COMPONENT_FLOAT(fStat, 2)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
		ELSE
			
			IF bIsStatValueRawText
				PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding raw text strStat = ", strStat)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStat)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding non raw text strStat = ", strStat)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStat)
			ENDIF
		
		ENDIF
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bIsStatNameRawText = ", bIsStatNameRawText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatNameRawText)	
				
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bIsStatValueRawText = ", bIsStatValueRawText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatValueRawText)	
				
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bUseBigText = ", bUseBigText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseBigText)
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bFormatAsTime = ", bFormatAsTime)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bFormatAsTime)
		
		IF eHudColour != HUD_COLOUR_PINK
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding stHudColour = ", stHudColour)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(stHudColour)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_STAT_TO_TABLE")
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding strScreenID = ", strScreenID)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding strTableID = ", strTableID)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bIsStatNameRawText
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding raw text strStatName = ", strStatName)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStatName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding non raw text strStatName = ", strStatName)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStatName)
		ENDIF
		
		IF bPassFloat
			
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding fStat = ", fStat)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
				ADD_TEXT_COMPONENT_FLOAT(fStat, 2)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
		ELSE
			
			IF bIsStatValueRawText
				PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding raw text strStat = ", strStat)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStat)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding non raw text strStat = ", strStat)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStat)
			ENDIF
		
		ENDIF
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bIsStatNameRawText = ", bIsStatNameRawText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatNameRawText)	
				
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bIsStatValueRawText = ", bIsStatValueRawText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatValueRawText)	
				
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bUseBigText = ", bUseBigText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseBigText)
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bFormatAsTime = ", bFormatAsTime)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bFormatAsTime)
		
		IF eHudColour != HUD_COLOUR_PINK
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding stHudColour = ", stHudColour)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(stHudColour)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_STAT_TO_TABLE")
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding strScreenID = ", strScreenID)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding strTableID = ", strTableID)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bIsStatNameRawText
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding raw text strStatName = ", strStatName)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStatName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding non raw text strStatName = ", strStatName)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStatName)
		ENDIF
		
		IF bPassFloat
			
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding fStat = ", fStat)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
				ADD_TEXT_COMPONENT_FLOAT(fStat, 2)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
		ELSE
			
			IF bIsStatValueRawText
				PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding raw text strStat = ", strStat)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStat)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding non raw text strStat = ", strStat)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStat)
			ENDIF
		
		ENDIF
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bIsStatNameRawText = ", bIsStatNameRawText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatNameRawText)	
				
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bIsStatValueRawText = ", bIsStatValueRawText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatValueRawText)	
				
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bUseBigText = ", bUseBigText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseBigText)
		
		PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding bFormatAsTime = ", bFormatAsTime)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bFormatAsTime)
		
		IF eHudColour != HUD_COLOUR_PINK
			PRINTLN("[NETCELEBRATIONS] - ADD_ITEM_TO_CELEBRATION_STAT_TABLE - foreground - adding stHudColour = ", stHudColour)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(stHudColour)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

PROC ADD_PILOT_SCHOOL_MEDAL_TO_STAT_TABLE(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, STRING strTableID, HUD_COLOURS eHudColour = HUD_COLOUR_PINK, BOOL bIsStatNameRawText = FALSE, BOOL bIsStatValueRawText = FALSE, BOOL bUseBigText = TRUE, BOOL bFormatAsTime = FALSE)
	
	STRING stHudColour
	STRING strStatName = "CELEB_MEDAL"
	STRING strStat
	
	SWITCH eHudColour
		CASE HUD_COLOUR_GOLD
			stHudColour = "HUD_COLOUR_GOLD"
			strStat = "CELEB_GOLD"
		BREAK
		CASE HUD_COLOUR_SILVER
			stHudColour = "HUD_COLOUR_SILVER"
			strStat = "CELEB_SILVER"
		BREAK
		CASE HUD_COLOUR_BRONZE
			stHudColour = "HUD_COLOUR_BRONZE"
			strStat = "CELEB_BRONZE"
		BREAK
		DEFAULT
			stHudColour= HUD_COLOUR_AS_STRING(eHudColour)
		BREAK
	ENDSWITCH
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_STAT_TO_TABLE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bIsStatNameRawText
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStatName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStatName)
		ENDIF
		
		IF bIsStatValueRawText
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStat)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStat)
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatNameRawText)	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatValueRawText)	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseBigText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bFormatAsTime)
		
		IF eHudColour != HUD_COLOUR_PINK
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(stHudColour)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_STAT_TO_TABLE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bIsStatNameRawText
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStatName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStatName)
		ENDIF
		
		IF bIsStatValueRawText
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStat)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStat)
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatNameRawText)	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatValueRawText)	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseBigText)	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bFormatAsTime)
		
		IF eHudColour != HUD_COLOUR_PINK
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(stHudColour)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_STAT_TO_TABLE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bIsStatNameRawText
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStatName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStatName)
		ENDIF
		
		IF bIsStatValueRawText
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strStat)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strStat)
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatNameRawText)	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsStatValueRawText)	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseBigText)	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bFormatAsTime)
		
		IF eHudColour != HUD_COLOUR_PINK
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(stHudColour)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

PROC ADD_CELEBRATION_STAT_TABLE_TO_CURRENT_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, STRING strTableID)
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_STAT_TABLE_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_STAT_TABLE_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_STAT_TABLE_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTableID)
		END_TEXT_COMMAND_SCALEFORM_STRING()		
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


/// PURPOSE:
///    Adds a "Headshots X" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iHeadshots - The number of headshots for this job.
PROC ADD_HEADSHOTS_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iHeadshots)
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_HEADSHOTS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHeadshots)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_HEADSHOTS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHeadshots)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_HEADSHOTS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHeadshots)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a "Point Blank X" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iPointBlanks - The number of point blank kills for this job.
PROC ADD_POINT_BLANKS_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iPointBlanks)
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_POINT_BLANK")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPointBlanks)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_POINT_BLANK")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPointBlanks)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_STAT_NUMERIC_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_POINT_BLANK")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPointBlanks)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a "Winner X" message to the celebration screen, where X is the player name.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    eWinStatus - Whether the player won, lost or drew the job.
///    strWinnerName - The name of the winner.
///    iBetAmount - The player's bet winnings (or losses).
PROC ADD_WINNER_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, JOB_WIN_STATUS eWinStatus, STRING strWinnerName, STRING strCrewName, STRING strTeamName, INT iBetWinnings, 
									  BOOL bIsInFlow = FALSE, BOOL bMultipleWinners = FALSE, BOOL bCongrats = FALSE, BOOL bTeamNameIsTextLabel = FALSE)
	
	DEBUG_PRINTCALLSTACK()
	
	STRING strWinLoseDrawLabel
	PRINTLN("[ADD_WINNER_TO_CELEBRATION_SCREEN][RH] The winning player name is ", strWinnerName, " bTeamNameIsTextLabel: ", bTeamNameIsTextLabel)
	IF eWinStatus = JOB_STATUS_WIN
		IF bCongrats
			strWinLoseDrawLabel = "CELEB_FINCOMP"
		ELSE
			IF bMultipleWinners
				strWinLoseDrawLabel = "CELEB_WINNERS"
			ELSE
				strWinLoseDrawLabel = "CELEB_WINNER"
			ENDIF
		ENDIF
	ELIF eWinStatus = JOB_STATUS_LOSE
		strWinLoseDrawLabel = "CELEB_LOSER"
	ELIF eWinStatus = JOB_STATUS_PASSED
		strWinLoseDrawLabel = "CELEB_PASSED"
	ELIF eWinStatus = JOB_STATUS_FAILED
		strWinLoseDrawLabel = "CELEB_FAILED"
	ELSE
		strWinLoseDrawLabel = "CELEB_DRAW"
		strWinnerName = ""
		strCrewName = ""
	ENDIF
	
	IF NOT IS_ACCOUNT_OVER_17_FOR_UGC()
	OR NOT IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
		strCrewName = ""
		PRINTLN( "[RCC MISSION] - [NETCELEBRATION] - IS_ACCOUNT_OVER_17_FOR_UGC = FALSE, clearing crew name.")
	ELSE
		PRINTLN( "[RCC MISSION] - [NETCELEBRATION] - IS_ACCOUNT_OVER_17_FOR_UGC = TRUE, allowing crew name.")
	ENDIF
	
//	IF (sCelebrationData.iNumPlayerFoundForWinnerScene <= 1)
//		strWinnerName = ""
//		IF ARE_STRINGS_EQUAL(strWinLoseDrawLabel, "CELEB_WINNERS")
//			strWinLoseDrawLabel = "CELEB_WINNER"
//		ENDIF
//	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_WINNER_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF NOT bCongrats
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinLoseDrawLabel)
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strWinLoseDrawLabel)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
//		IF NOT bTeamNameIsTextLabel
//			PRINTLN( "[RCC MISSION] - [NETCELEBRATION] - bTeamNameIsTextLabel = FALSE, adding team name as literal.")
//			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strWinnerName)
//			END_TEXT_COMMAND_SCALEFORM_STRING()
//		ELSE
//			PRINTLN( "[RCC MISSION] - [NETCELEBRATION] - bTeamNameIsTextLabel = TRUE, adding team name as label.")
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strWinnerName)
//		ENDIF

		// 3, gamerName:String, 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinnerName)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strCrewName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBetWinnings)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsInFlow)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTeamName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bTeamNameIsTextLabel
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_WINNER_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF NOT bCongrats
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinLoseDrawLabel)
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strWinLoseDrawLabel)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strWinnerName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strCrewName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBetWinnings)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsInFlow)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTeamName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bTeamNameIsTextLabel
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_WINNER_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF NOT bCongrats
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinLoseDrawLabel)
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strWinLoseDrawLabel)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strWinnerName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strCrewName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBetWinnings)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsInFlow)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTeamName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF bTeamNameIsTextLabel
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    Adds a "Winner/Loser Challenge Part" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    eChallengeType - Indicates the challenge type (currently head-to-head or crew).
///    bWonChallengePart - If TRUE then the player won the challenge.
PROC ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, CHALLENGE_PART_TYPE eChallengeType, 
													 INT iChallengePart, INT iTotalChallengeParts, BOOL bWonChallengePart)
	STRING strWinLoseLabel
	TEXT_LABEL strChallengePart = ""
	TEXT_LABEL strTotalChallengeParts = ""
	STRING temp
	
	strChallengePart += iChallengePart
	strTotalChallengeParts += iTotalChallengeParts

	IF eChallengeType = CHALLENGE_PART_HEAD_TO_HEAD
		IF bWonChallengePart
			strWinLoseLabel = "CELEB_WINNERS"
		ELSE
			strWinLoseLabel = "CELEB_LOSERS"
		ENDIF
		temp = "CELEB_H2H_PART_OF"
	ELSE
		IF bWonChallengePart
			strWinLoseLabel = "CELEB_WINNER"
		ELSE
			strWinLoseLabel = "CELEB_LOSER"
		ENDIF
		temp = "CELEB_CHALLENGE_PART_OF"
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_CHALLENGE_PART_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinLoseLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(temp)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strChallengePart)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTotalChallengeParts)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_CHALLENGE_PART_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinLoseLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(temp)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strChallengePart)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTotalChallengeParts)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_CHALLENGE_PART_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinLoseLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(temp)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strChallengePart)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTotalChallengeParts)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    Adds a "Challenge Time/Score Set" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    eChallengeType - Indicates the challenge type (currently head-to-head or crew).
///    bWonChallengePart - If TRUE then the player won the challenge.
PROC ADD_CHALLENGE_VALUES_SET_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, STRING strChallengeName, INT iScore = 0, INT iTime = 0)
	STRING strChallengeTypeLabel

	IF iScore > -1
		IF iTime > 0
			strChallengeTypeLabel = "CELEB_CHALLENGE_TIME_AND_SCORE_SET"
		ELSE
			strChallengeTypeLabel = "CELEB_CHALLENGE_SCORE_SET"
		ENDIF
	ELIF iTime > 0
		strChallengeTypeLabel = "CELEB_CHALLENGE_TIME_SET"
	ELSE
		//Neither were set, just do a fallback.
		strChallengeTypeLabel = "CELEB_CHALLENGE_SCORE_SET"
	ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_CHALLENGE_SET_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTime)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strChallengeTypeLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strChallengeName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_CHALLENGE_SET_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTime)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strChallengeTypeLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strChallengeName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_CHALLENGE_SET_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTime)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strChallengeTypeLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strChallengeName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC ADD_FINALE_COMPLETE_TO_WALL(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_COMPLETE_MESSAGE_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)
//		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_COMPLETE")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(75)
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_COMPLETE_MESSAGE_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)
//		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_COMPLETE")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(75)
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_COMPLETE_MESSAGE_TO_WALL")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)
//		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CELEB_COMPLETE")
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(75)
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE:
///    Adds a "Challenge Winner/Loser" message to the celebration screen.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    eChallengeType - Indicates the challenge type (currently head-to-head or crew).
///    bWonChallengePart - If TRUE then the player won the challenge.
///    NOTE: strCrewName can also hold the player name using the <br> tag e.g. Player Name<br>Crew Name.
PROC ADD_CHALLENGE_WINNER_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, CELEBRATION_CHALLENGE_TYPE eChallengeType, BOOL bWonChallenge, 
												STRING strCrewName, STRING strChallengeName, INT iCashWon)
	STRING strChallengeTypeLabel
	STRING strWinLoseLabel

	IF eChallengeType != CHALLENGE_TYPE_PLAYLIST
		IF eChallengeType != CHALLENGE_TYPE_HEAD_TO_HEAD
			strChallengeTypeLabel = "CELEB_CHALLENGE_CREW"
			
			IF bWonChallenge
				strWinLoseLabel = "CELEB_WINNER"
			ELSE
				strWinLoseLabel = "CELEB_LOSER"
			ENDIF
			
		ELSE
			strChallengeTypeLabel = "CELEB_CHALLENGE_H2H"
			
			IF bWonChallenge
				strWinLoseLabel = "CELEB_WINNERS"
			ELSE
				strWinLoseLabel = "CELEB_LOSERS"
			ENDIF
		ENDIF
	ELSE
		strChallengeTypeLabel = "CELEB_PLAYLIST_WINNER"
		
		IF bWonChallenge
			strWinLoseLabel = "CELEB_WINNER"
		ELSE
			strWinLoseLabel = "CELEB_LOSER"
		ENDIF
	ENDIF
	
	IF eChallengeType = CHALLENGE_TYPE_TOURNAMENT
		
		strChallengeTypeLabel = "CELEB_TOURN_WINNER"
		
		IF bWonChallenge
			strWinLoseLabel = "CELEB_WINNERS"
		ELSE
			strWinLoseLabel = "CELEB_LOSERS"
		ENDIF
		
	ENDIF
	
	IF (sCelebrationData.iNumPlayerFoundForWinnerScene <= 1)
		IF ARE_STRINGS_EQUAL(strWinLoseLabel, "CELEB_WINNERS")
			strWinLoseLabel = "CELEB_WINNER"
		ELIF ARE_STRINGS_EQUAL(strWinLoseLabel, "CELEB_WINNERS")
			strWinLoseLabel = "CELEB_LOSER"
		ENDIF
	ENDIF
	
	IF NOT IS_ACCOUNT_OVER_17()
	OR NOT IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
		strCrewName = ""
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_CHALLENGE_WINNER_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strChallengeTypeLabel)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinLoseLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strCrewName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strChallengeName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF iCashWon != (-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCashWon)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_CHALLENGE_WINNER_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strChallengeTypeLabel)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinLoseLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strCrewName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strChallengeName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF iCashWon != (-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCashWon)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_CHALLENGE_WINNER_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strChallengeTypeLabel)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinLoseLabel)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strCrewName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strChallengeName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF iCashWon != (-1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCashWon)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Add a "x GAMES OF y GAMES WON" slide to the celebration scaleform, mainly used for minigames
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
///    iGamesWon - Number games one in the match
///    iTotalGames - Total number of games played in the match
PROC ADD_GAMES_WON_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iGamesWon, INT iTotalGames)
	STRING strTotalGames
	STRING strTotalLabel
	
	strTotalGames = CONVERT_INT_TO_STRING(iTotalGames)
	
	strTotalLabel = "CELEB_GAMES_WON"
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_GAMES_WON_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGamesWon)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strTotalLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTotalGames)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_GAMES_WON_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGamesWon)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strTotalLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTotalGames)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_GAMES_WON_TO_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGamesWon)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strTotalLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strTotalGames)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Tells the celebration screen to begin animating.
/// PARAMS:
///    sCelebrationData - The celebration screen data struct.
///    strScreenID - The unique name for this particular celebration screen, all the elements added to the screen need to have the same ID as the one passed to CREATE_CELEBRATION_SCREEN. 
PROC SHOW_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID)
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "SHOW_STAT_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "SHOW_STAT_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "SHOW_STAT_WALL")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	PRINTLN("[NETCELEBRATION] Celebration screen shown by ", GET_THIS_SCRIPT_NAME())
ENDPROC

//Adds a pause to the end of the current celebration screen. Call after SHOW_CELEBRATION_SCREEN.
PROC ADD_PAUSE_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iPauseDuration, BOOL bIndefinitePause = FALSE)
	FLOAT fTimeInSeconds = TO_FLOAT(iPauseDuration) / 1000.0

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "PAUSE_BEFORE_PREVIOUS_LAYOUT")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF NOT bIndefinitePause
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "PAUSE_BEFORE_PREVIOUS_LAYOUT")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF NOT bIndefinitePause
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "PAUSE_BEFORE_PREVIOUS_LAYOUT")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF NOT bIndefinitePause
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	PRINTLN("[NETCELEBRATION] Celebration screen shown by ", GET_THIS_SCRIPT_NAME())
ENDPROC

PROC CELEBRATION_ADD_PAUSE_TO_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iPauseDuration)
	
	FLOAT fTimeInSeconds = TO_FLOAT(iPauseDuration) / 1000.0

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "PAUSE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "PAUSE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "PAUSE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
	END_SCALEFORM_MOVIE_METHOD()
	
	PRINTLN("[NETCELEBRATION] Celebration screen shown by ", GET_THIS_SCRIPT_NAME())
	
ENDPROC

PROC UNPAUSE_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "UNPAUSE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "UNPAUSE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "UNPAUSE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE:
///    Changes the stat display time: default is two seconds.
PROC SET_CELEBRATION_SCREEN_STAT_DISPLAY_TIME(CELEBRATION_SCREEN_DATA &sCelebrationData, INT iPauseDuration = 2000)
	FLOAT fTimeInSeconds = TO_FLOAT(iPauseDuration) / 1000.0

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "SET_PAUSE_DURATION")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "SET_PAUSE_DURATION")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "SET_PAUSE_DURATION")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
	END_SCALEFORM_MOVIE_METHOD()
	
	PRINTLN("[NETCELEBRATION] SET_CELEBRATION_SCREEN_STAT_DISPLAY_TIME - Pause duration has been updated: ", iPauseDuration)
ENDPROC

//TEXT_PLACEMENT pedNamesPlacement[8]
//TEXT_STYLE pedNameStyle[8]
//FLOAT fXTemp[8]//, fYTemp[8]
//FLOAT fScaleX[8], fScaleY[8]

FUNC VECTOR GET_PLAYER_NAME_OFFSET_NG(PED_INDEX &peds, INT iIndex)
	
	FLOAT x, y, z
	
	x = 0.0
	y = 0.5
	
	IF iIndex = 5 // Put the fifth peds offset slightly higher to balance out scene.
		IF IS_PED_WEARING_HIGH_HEELS(peds)
			z = 0.10
		ELSE
			z = 0.05
		ENDIF
	ELSE
		IF IS_PED_WEARING_HIGH_HEELS(peds)
			z = 0.10
		ELSE
			z = 0.05
		ENDIF
	ENDIF
	
	RETURN << x, y, z >>
	
ENDFUNC

PROC OFFSET_PLAYER_NAME_FROM_PED(PED_INDEX &peds, VECTOR &vPedCoords, INt iIndex)
	
	IF iIndex = 5 // Put the fifth peds offset slightly higher to balance out scene.
		IF IS_PED_WEARING_HIGH_HEELS(peds)
//			vCoords.z += 0.5
//			vCoords.z -= 0.35
//			vPedCoords.z += 0.45
			vPedCoords.z += 0.15
		ELSE
//			vCoords.z += 0.45
//			vCoords.z -= 0.3
//			vPedCoords.z += 0.4
			vPedCoords.z += 0.1
		ENDIF
	ELSE
		IF IS_PED_WEARING_HIGH_HEELS(peds)
//			vCoords.z += 0.41
//			vCoords.z -= 0.26
//			vPedCoords.z += 0.45
			vPedCoords.z += 0.15
		ELSE
//			vCoords.z += 0.36
//			vCoords.z -= 0.21
//			vPedCoords.z += 0.4
			vPedCoords.z += 0.1
		ENDIF
	ENDIF
	
	IF NOT IS_PS3_VERSION()
	AND NOT IS_XBOX360_VERSION()
		vPedCoords.y -= 0.5
	ENDIF
	
ENDPROC

FUNC VECTOR GET_NG_CELEBRATION_PLAYER_NAME_WORLD_SCALE(INT iPed)
	
	SWITCH iPed
		
		CASE 0
			RETURN << 0.9, 0.6, 2.0 >>
		
		CASE 1
			RETURN << 1.2, 0.7, 2.0 >>
		
		CASE 2
			RETURN << 1.3, 0.7, 2.0 >>
		
		CASE 3
			RETURN << 1.2, 0.7, 2.0 >>
		
		CASE 4
			RETURN << 1.3, 0.8, 2.0 >>
		
		CASE 5
			RETURN << 1.4, 0.8, 2.0 >>
		
		CASE 6
			RETURN << 1.6, 0.8, 2.0 >>
		
		CASE 7
			RETURN << 1.7, 0.9, 2.0 >>
		
	ENDSWITCH
	
	RETURN << 1.4, 0.8, 2.0 >>
	
ENDFUNC

PROC SET_NG_PLAYER_NAME_SPEAKER_STATE(CELEBRATION_SCREEN_DATA &sCelebrationData, INT iState, INT iMovieIndex)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.playerNameMovies[iMovieIndex], "SET_SPEAKER_STATE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iState)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

PROC MAINTAIN_PLAYER_NAME_SPEAKER_ICONS(CELEBRATION_SCREEN_DATA &sCelebrationData, INT iIndex)
	
	PLAYER_INDEX playerId
	GAMER_HANDLE gamerHandle
	INT iVoiceState
	
	// Check our index is valid.
	IF iIndex > -1
	AND iIndex < MAX_NUM_CELEBRATION_PEDS
		
		// Check our player index is valid.
		IF sCelebrationData.iPedWinnerClonePlayerId[iIndex] > -1
		AND sCelebrationData.iPedWinnerClonePlayerId[iIndex] < NUM_NETWORK_PLAYERS
			
			// Get the player id.
			playerId = INT_TO_PLAYERINDEX(sCelebrationData.iPedWinnerClonePlayerId[iIndex])
			
			// Check the player is still playing.
			IF IS_NET_PLAYER_OK(playerId, FALSE)
				
				// Get the players gamer handle.
				gamerHandle	= GET_GAMER_HANDLE_PLAYER(playerId)
				
				// Get the current state of the player's chat.
				IF NETWORK_CAN_COMMUNICATE_WITH_GAMER(gamerHandle)
					IF NETWORK_IS_GAMER_TALKING(gamerHandle)
						iVoiceState = ciVOICE_CHAT_TALKING
					ELSE
						iVoiceState = ciVOICE_CHAT_NOT_TALKING
					ENDIF
				ELSE
					IF NETWORK_IS_GAMER_MUTED_BY_ME(gamerHandle)
						iVoiceState = ciVOICE_CHAT_MUTED
					ELSE
						iVoiceState = ciVOICE_CHAT_NULL
					ENDIF
				ENDIF
				
				// Compare it to our saved state for the player's chat, and update iconand state if needed.
				IF sCelebrationData.iVoiceStateCopy[iIndex] != iVoiceState
					SWITCH iVoiceState
						CASE ciVOICE_CHAT_NULL			SET_NG_PLAYER_NAME_SPEAKER_STATE(sCelebrationData, 1, iIndex)	BREAK
						CASE ciVOICE_CHAT_MUTED			SET_NG_PLAYER_NAME_SPEAKER_STATE(sCelebrationData, 4, iIndex)	BREAK
						CASE ciVOICE_CHAT_TALKING		SET_NG_PLAYER_NAME_SPEAKER_STATE(sCelebrationData, 3, iIndex)	BREAK
						CASE ciVOICE_CHAT_NOT_TALKING	SET_NG_PLAYER_NAME_SPEAKER_STATE(sCelebrationData, 2, iIndex)	BREAK
					ENDSWITCH
					sCelebrationData.iVoiceStateCopy[iIndex] = iVoiceState
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC DRAW_NG_CELEBRATION_PLAYER_NAMES(CELEBRATION_SCREEN_DATA &sCelebrationData, INT &iDrawNamesStage, INT &iNumNamesToDisplay, PED_INDEX &peds[], TEXT_LABEL_63 &names[], CELEBRATION_SCREEN_STAGE eCurrentstage, SCALEFORM_INDEX &movie, SCALEFORM_INDEX &playerNameMovies[], BOOL &bNameToggle)
	
	PRINTLN("[NG PLAYER NAMES] - DRAW_NG_CELEBRATION_PLAYER_NAMES being called.")
	
	IF IS_PS3_VERSION()
	OR IS_XBOX360_VERSION()
		EXIT
	ENDIF
	
//	VECTOR vCamRot
//	vCamRot = GET_FINAL_RENDERED_CAM_ROT()

	//Don't draw the screen if we're in skycam.
//	IF (ABSF(vCamRot.x) >= 45.0)
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		PRINTLN("[NG PLAYER NAMES] - DRAW_NG_CELEBRATION_PLAYER_NAMES - (ABSF(vCamRot.x) >= 45.0), exiting.")
		DEBUG_PRINTCALLSTACK()
		EXIT
	ENDIF
	
	IF eCurrentstage = eCurrentstage
		eCurrentstage = eCurrentstage
	ENDIF
	
	IF (movie = NULL)
		PRINTLN("[NG PLAYER NAMES] - DRAW_NG_CELEBRATION_PLAYER_NAMES - (movie = NULL), exiting.")
		DEBUG_PRINTCALLSTACK()
		EXIT
	ENDIF
	
	INT iIndex, iCount
	SWITCH iDrawNamesStage
		
		CASE 0
			
			IF iNumNamesToDisplay = (-1)
				
				PRINTLN("[NG PLAYER NAMES]* - DRAW_NG_CELEBRATION_PLAYER_NAMES - iNumNamesToDisplay = (-1).")
				
				REPEAT COUNT_OF(peds) iIndex
					IF iIndex <= 7 // Lmited to 8 player name movies just now.
						IF DOES_ENTITY_EXIST(peds[iIndex])
							IF NOT IS_ENTITY_DEAD(peds[iIndex])
								iCount++
								PRINTLN("[NG PLAYER NAMES]* - DRAW_NG_CELEBRATION_PLAYER_NAMES - ped ", iIndex, " exists. iCount = ", iCount)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF iCount > 0
					iNumNamesToDisplay = iCount
					PRINTLN("[NG PLAYER NAMES]* - DRAW_NG_CELEBRATION_PLAYER_NAMES - iNumNamesToDisplay = ", iNumNamesToDisplay)
				ENDIF
				
			ELSE
				
				TEXT_LABEL_63 tl63Temp
				BOOL bLoadedAllMovies
				bLoadedAllMovies = TRUE
				
				REPEAT iNumNamesToDisplay iIndex
					IF iIndex <= 7 // Lmited to 8 player name movies just now.
						tl63Temp = "PLAYER_NAME_0"
						tl63Temp += (iIndex + 1)
						playerNameMovies[iIndex] = REQUEST_SCALEFORM_MOVIE(tl63Temp)
						PRINTLN("[NG PLAYER NAMES]* - DRAW_NG_CELEBRATION_PLAYER_NAMES - requesting movie ", tl63Temp)
						IF NOT HAS_SCALEFORM_MOVIE_LOADED(playerNameMovies[iIndex])
							bLoadedAllMovies = FALSE
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF bLoadedAllMovies
					iDrawNamesStage++
					PRINTLN("[NG PLAYER NAMES]* - DRAW_NG_CELEBRATION_PLAYER_NAMES - all movies loaded.")
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 1
			
			REPEAT iNumNamesToDisplay iIndex
				
				BEGIN_SCALEFORM_MOVIE_METHOD(playerNameMovies[iIndex],"SET_PLAYER_NAME")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(names[iIndex])
					PRINTLN("[NG PLAYER NAMES]* - DRAW_NG_CELEBRATION_PLAYER_NAMES - called set player name with ", names[iIndex])
				END_SCALEFORM_MOVIE_METHOD()
				
			ENDREPEAT
			
			iDrawNamesStage++
			
		BREAK
		
		CASE 2
			
			VECTOR vRotation, vPedCoords
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
				PRINTLN("[NG PLAYER NAMES]* - DRAW_NG_CELEBRATION_PLAYER_NAMES - (FRONTEND_CONTROL, INPUT_FRONTEND_Y) just released.")
				IF bNameToggle
					bNameToggle = FALSE
					PRINTLN("[NG PLAYER NAMES]* - DRAW_NG_CELEBRATION_PLAYER_NAMES - set bNameToggle to FALSE.")
				ELSE
					bNameToggle = TRUE
					PRINTLN("[NG PLAYER NAMES]* - DRAW_NG_CELEBRATION_PLAYER_NAMES - set bNameToggle to TRUE.")
				ENDIF
				PLAY_SOUND_FRONTEND(-1,"TOGGLE_ON","HUD_FRONTEND_DEFAULT_SOUNDSET")
			ENDIF
				
			IF bNameToggle
				
				REPEAT iNumNamesToDisplay iIndex
					
					IF iIndex <= 7 // Lmited to 8 player name movies just now.
						
						IF DOES_ENTITY_EXIST(peds[iIndex])
							IF NOT IS_ENTITY_DEAD(peds[iIndex])
								IF IS_PS3_VERSION()
								OR IS_XBOX360_VERSION()
									vPedCoords = GET_PED_BONE_COORDS(peds[iIndex], BONETAG_SPINE3, <<0,0,0>>)
									OFFSET_PLAYER_NAME_FROM_PED(peds[iIndex], vPedCoords, iIndex)
									vRotation = GET_MISSION_LOCATE_ROTATION_VECTOR(vPedCoords, GET_FINAL_RENDERED_CAM_COORD())
									DRAW_SCALEFORM_MOVIE_3D_SOLID(playerNameMovies[iIndex], vPedCoords, vRotation, << 0.75, 0.5, 0.375 >>, << 0.75, 0.5, 0.375 >>)//vNamesMovieSize1, vNamesMovieSize2) 
									PRINTLN("[NG PLAYER NAMES]* - DRAW_NG_CELEBRATION_PLAYER_NAMES - drawing name movie for ", names[iIndex])
								ELSE
									VECTOR vOffset, vWorldScale
									vOffset = GET_PLAYER_NAME_OFFSET_NG(peds[iIndex], iIndex)
									vPedCoords = GET_PED_BONE_COORDS(peds[iIndex], BONETAG_SPINE3, <<0,0,0>>)
									VECTOR vRelativeOffset
									vRelativeOffset = (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(peds[iIndex], vOffset) - GET_ENTITY_COORDS(peds[iIndex]))
									VECTOR vTextCoords
									vTextCoords = (vPedCoords + vRelativeOffset)
									vRotation = GET_MISSION_LOCATE_ROTATION_VECTOR(vPedCoords, GET_FINAL_RENDERED_CAM_COORD())
									vWorldScale = GET_NG_CELEBRATION_PLAYER_NAME_WORLD_SCALE(iIndex)
									DRAW_SCALEFORM_MOVIE_3D_SOLID(playerNameMovies[iIndex], vTextCoords, vRotation, vWorldScale, vWorldScale)//vNamesMovieSize1, vNamesMovieSize2) 
									MAINTAIN_PLAYER_NAME_SPEAKER_ICONS(sCelebrationData, iIndex)
									PRINTLN("[NG PLAYER NAMES]** - DRAW_NG_CELEBRATION_PLAYER_NAMES - drawing name movie for ", names[iIndex])
								ENDIF
							ENDIF
						ENDIF
					
					ENDIF
					
				ENDREPEAT
				
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC DISPLAY_CELEBRATION_PLAYER_NAMES(PED_INDEX &peds[], TEXT_LABEL_63 &names[], CELEBRATION_SCREEN_STAGE eCurrentstage, SCALEFORM_INDEX movie)
	
	IF NOT IS_PS3_VERSION()
	AND NOT IS_XBOX360_VERSION()
		EXIT
	ENDIF
	
	VECTOR vCamRot
	vCamRot = GET_FINAL_RENDERED_CAM_ROT()

	//Don't draw the screen if we're in skycam.
	IF (ABSF(vCamRot.x) >= 45.0)
//		PRINTLN("[Celebrations] - [Paired Anims] - DISPLAY_CELEBRATION_PLAYER_NAMES - cam pointing downwards, we're done with the celbration rendering, EXIT.")
		EXIT
	ENDIF
	
	IF NOT ( (eCurrentstage > CELEBRATION_STAGE_DOING_FLASH) AND (eCurrentstage < CELEBRATION_STAGE_FINISHED) )
//		PRINTLN("[Celebrations] - [Paired Anims] - DISPLAY_CELEBRATION_PLAYER_NAMES - not in correct celebration stage, EXIT.")
		EXIT
	ENDIF
	
	IF (movie = NULL)
//		PRINTLN("[Celebrations] - [Paired Anims] - DISPLAY_CELEBRATION_PLAYER_NAMES - sfCelebration = NULL, no valid movie loaded, EXIT.")
		EXIT
	ENDIF
	
	INT i
	VECTOR vCoords
	FLOAT fXPos = -1
	FLOAT fYPos = -1
	TEXT_LABEL_63 tl31Name
	
	TEXT_PLACEMENT pedNamesPlacement
	TEXT_STYLE pedNameStyle
	
	REPEAT 8 i
		SET_STANDARD_UI_AMPM(pedNameStyle, DROPSTYLE_DROPSHADOWONLY)
		pedNameStyle.aFont = FONT_CURSIVE
		SWITCH i
			CASE 0
				pedNameStyle.XScale = 0.55
				pedNameStyle.YScale = 0.55
			BREAK
			CASE 1
				pedNameStyle.XScale = 0.475
				pedNameStyle.YScale = 0.475
			BREAK
			CASE 2
				pedNameStyle.XScale = 0.475
				pedNameStyle.YScale = 0.475
			BREAK
			CASE 3
				pedNameStyle.XScale = 0.4
				pedNameStyle.YScale = 0.4
			BREAK
			CASE 4
				pedNameStyle.XScale = 0.4
				pedNameStyle.YScale = 0.4
			BREAK
			CASE 5
				pedNameStyle.XScale = 0.475
				pedNameStyle.YScale = 0.475
			BREAK
			CASE 6
				pedNameStyle.XScale = 0.4
				pedNameStyle.YScale = 0.4
			BREAK
			CASE 7
				pedNameStyle.XScale = 0.4
				pedNameStyle.YScale = 0.4
			BREAK
		ENDSWITCH
		tl31Name = names[i]
		
		IF DOES_ENTITY_EXIST(peds[i])
			IF NOT IS_ENTITY_DEAD(peds[i])	
				vCoords = GET_PED_BONE_COORDS(peds[i], BONETAG_SPINE3, <<0,0,0>>)
				IF i = 5 // Put the fifth peds offset slightly higher to balance out scene.
					IF IS_PED_WEARING_HIGH_HEELS(peds[i])
//						vCoords.z += 0.5
//						vCoords.z -= 0.35
						vCoords.z += 0.25
					ELSE
//						vCoords.z += 0.45
//						vCoords.z -= 0.3
						vCoords.z += 0.22
					ENDIF
				ELSE
					IF IS_PED_WEARING_HIGH_HEELS(peds[i])
//						vCoords.z += 0.41
//						vCoords.z -= 0.26
						vCoords.z += 0.25
					ELSE
//						vCoords.z += 0.36
//						vCoords.z -= 0.21
						vCoords.z += 0.22
					ENDIF
				ENDIF
				GET_SCREEN_COORD_FROM_WORLD_COORD(vCoords, fXPos, fYPos)
				IF fXPos != (-1)
				AND fYPos != (-1)
					pedNamesPlacement.x = fXPos
					pedNamesPlacement.y = fYPos
				ENDIF
//				pedNameStyle[i].XScale = fScaleX[i]
//				pedNameStyle[i].YScale = fScaleY[i]
				DRAW_TEXT_WITH_PLAYER_NAME(pedNamesPlacement, pedNameStyle, tl31Name, "", HUD_COLOUR_WHITE, FONT_CENTRE)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC DRAW_CELEBRATION_SCREEN_THIS_FRAME(CELEBRATION_SCREEN_DATA &sCelebrationData, BOOL bDisableNearClipScan = TRUE)
	
	PRINTLN("[DRAW_CELEBRATION_SCREEN_THIS_FRAME] Called... ")
	
	IF bDisableNearClipScan
		DISABLE_NEAR_CLIP_SCAN_THIS_UPDATE()
	ENDIF
	
	IF sCelebrationData.sfForeground != NULL
	AND sCelebrationData.sfBackground != NULL
	AND sCelebrationData.sfCelebration != NULL
		PRINTLN("[DRAW_CELEBRATION_SCREEN_THIS_FRAME] Calling scaleform natives... ")
		DRAW_SCALEFORM_MOVIE_FULLSCREEN_MASKED(sCelebrationData.sfForeground, sCelebrationData.sfBackground, 255, 255, 255, 255)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(sCelebrationData.sfCelebration, 255, 255, 255, 255)
	ENDIF
		
ENDPROC

/// PURPOSE:
///    Intended for use with the "WINNER" celebration screens, a scaleform background is drawn behind the ped and text drawn in front.
///    Use bDrawScreenInFront if you want the UI3DScene behaviour for the ped but still want to draw the scaleform in front of them (e.g. when doing the white flashes).
PROC DRAW_CELEBRATION_SCREEN_WITH_PED_MASKING_THIS_FRAME(CELEBRATION_SCREEN_DATA &sCelebrationData, PED_INDEX &pedToMask, BOOL bDrawScreenInFront = FALSE, BOOL bDisableNearClipScan = TRUE)
	
	IF bDisableNearClipScan
		DISABLE_NEAR_CLIP_SCAN_THIS_UPDATE()
	ENDIF
	
	IF UI3DSCENE_IS_AVAILABLE()
		IF UI3DSCENE_PUSH_PRESET("CELEBRATION_WINNER")
			IF NOT IS_PED_INJURED(pedToMask)
				UI3DSCENE_ASSIGN_PED_TO_SLOT("CELEBRATION_WINNER", pedToMask, 0, <<0.0, 0.0, 0.0>>)
				UI3DSCENE_ASSIGN_PED_TO_SLOT("CELEBRATION_WINNER", pedToMask, 1, <<0.0, 0.0, 0.0>>)
				UI3DSCENE_ASSIGN_PED_TO_SLOT("CELEBRATION_WINNER", pedToMask, 2, <<0.0, 0.0, 0.0>>)
			ENDIF
		
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
			DRAW_RECT(0.5, 0.5, 1.5, 1.5, 0, 0, 0, 255)
			
			IF bDrawScreenInFront
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
			ENDIF
			
			DRAW_SCALEFORM_MOVIE_FULLSCREEN_MASKED(sCelebrationData.sfForeground, sCelebrationData.sfBackground, 255, 255, 255, 255)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sCelebrationData.sfCelebration, 255, 255, 255, 255)
			
			//Draw a rectangle that fades in at the start
			/*IF NOT HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 300)
				INT iTimeDiff = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sCelebrationData.sCelebrationTimer.Timer)
				INT iAlpha = ROUND((1.0 - (TO_FLOAT(iTimeDiff) / 300.0)) * 255.0)
			
				SCRIPT_ASSERT("111")
			
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
				DRAW_RECT(0.5, 0.5, 1.5, 1.5, 0, 0, 0, iAlpha)
			ENDIF*/
		ENDIF
	ENDIF 
ENDPROC

/// PURPOSE:
///    Draws a fake black rectangle, this is done as regular fades don't work nicely with the winner screen scaleform commands.
FUNC BOOL FADE_IN_FAKE_RECTANGLE_FOR_WINNER_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, BOOL bWhite = FALSE)
	
	INT iR, iG, iB
	
	IF bWhite
		iR = 255
		iG = 255
		iB = 255
	ENDIF
	
	sCelebrationData.iBlackRectangleAlpha = sCelebrationData.iBlackRectangleAlpha + 30 //ROUND(30.0 * (GET_FRAME_TIME() * 30.0)) 
	
	IF sCelebrationData.iBlackRectangleAlpha > 255
		sCelebrationData.iBlackRectangleAlpha = 255
	ENDIF
	
	//PRINTLN("NETCELEBRATION ", sCelebrationData.iBlackRectangleAlpha)
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	DRAW_RECT(0.5, 0.5, 3.0, 3.0, iR, iG, iB, sCelebrationData.iBlackRectangleAlpha)
	
	IF sCelebrationData.iBlackRectangleAlpha >= 255
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL FADE_OUT_FAKE_RECTANGLE_FOR_WINNER_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, BOOL bWhite = FALSE)
	
	INT iR, iG, iB
	
	IF bWhite
		iR = 255
		iG = 255
		iB = 255
	ENDIF
	
	sCelebrationData.iBlackRectangleAlpha = sCelebrationData.iBlackRectangleAlpha - 30 //ROUND(20.0 * (GET_FRAME_TIME() * 30.0)) 
	
	IF sCelebrationData.iBlackRectangleAlpha < 0
		sCelebrationData.iBlackRectangleAlpha = 0
	ENDIF
	
	IF sCelebrationData.iBlackRectangleAlpha != 0
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
		DRAW_RECT(0.5, 0.5, 3.0, 3.0, iR, iG, iB, sCelebrationData.iBlackRectangleAlpha)
	ENDIF
	
	IF sCelebrationData.iBlackRectangleAlpha <= 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC ISLAND_RACE_DRAW_FAKE_RECTANGLE_FOR_WINNER_SCREEN()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		EXIT
	ENDIF
	INT iR, iG, iB
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD_PRIORITY_HIGH)
	DRAW_RECT(0.5, 0.5, 3.0, 3.0, iR, iG, iB, 150)
ENDPROC

/// PURPOSE:
///    Draws a black rectangle that fades in at the end of the winner screen. This is used to hide some lighting pops when transitioning out of the winner screen.
///    The screen must be fully faded out by iWinnerScreenEndTime.
PROC DRAW_BLACK_RECT_FOR_WINNER_SCREEN_END_TRANSITION(INT iWinnerScreenCurrentTime, INT iWinnerScreenEndTime, INT iFadeTime = 333)
	
	IF iWinnerScreenCurrentTime > iWinnerScreenEndTime
		iWinnerScreenCurrentTime = iWinnerScreenEndTime
	ELIF iWinnerScreenCurrentTime < iWinnerScreenEndTime - iFadeTime
		iWinnerScreenCurrentTime = iWinnerScreenEndTime - iFadeTime
	ENDIF

	//This should be a value between 0 and 1
	FLOAT fFadeProgress = TO_FLOAT(iWinnerScreenCurrentTime - (iWinnerScreenEndTime - iFadeTime)) / TO_FLOAT(iWinnerScreenEndTime - (iWinnerScreenEndTime - iFadeTime))
	INT iCurrentAlpha = ROUND(fFadeProgress * 255.0)
	
	//PRINTLN("DRAW_BLACK_RECT_FOR_WINNER_SCREEN_END_TRANSITION - ", iWinnerScreenCurrentTime, " ", iWinnerScreenEndTime, " ", fFadeProgress, " ", iCurrentAlpha)
	
	IF iCurrentAlpha != 0
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
		DRAW_RECT(0.5, 0.5, 3.0, 3.0, 0, 0, 0, iCurrentAlpha)
	ENDIF
	
ENDPROC

FUNC BOOL GET_CELEBRATION_CAMERA_OFFSETS(CELEBRATION_SCREEN_DATA &sCelebrationData, FLOAT fYOffset, FLOAT fZoffset, BOOL bGetCycleOffsets, BOOL bGetInVehicleOffsets, VECTOR &vLookOffset, VECTOR &vAttachOffset, VECTOR &vPointOffset, FLOAT &fFOV, BOOL &bUsingCoverOffsets, BOOL &bUseFullHeadLook)
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS been called.")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF bGetCycleOffsets
		
		PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS player is on a bicycle.")
		
		IF sCelebrationData.iCurrentCelebrationCam < 6
			IF sCelebrationData.iCurrentCelebrationCamModifier = 0
				vAttachOffset = <<0.3792, 1.6661, 0.2530>>
				vPointOffset = <<-0.1166, -1.2463, 0.7746>>
				fFOV = 34.0457
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
				RETURN TRUE
			ELIF sCelebrationData.iCurrentCelebrationCamModifier = 1	
				vAttachOffset = <<0.8206, 2.4757, -0.1400>>
				vPointOffset = <<0.0702, -0.3920, 0.3218>>
				fFOV = 34.0457
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
				RETURN TRUE
			ELIF sCelebrationData.iCurrentCelebrationCamModifier = 2
				vAttachOffset = <<-0.5169, 2.6701, 0.1763>>
				vPointOffset = <<-0.1689, -0.3063, 0.3164>>
				fFOV = 34.0457
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
				RETURN TRUE
			ELIF sCelebrationData.iCurrentCelebrationCamModifier = 3
				vAttachOffset = <<-1.7521, 1.9222, 0.2656>>
				vPointOffset = <<0.3639, -0.2033, 0.2015>>
				fFOV = 34.0457
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
				RETURN TRUE
			ELIF sCelebrationData.iCurrentCelebrationCamModifier = 4
				vAttachOffset = <<-1.1718, 1.6414, 0.7172>>
				vPointOffset = <<1.3122, 0.1011, 0.0411>>
				fFOV = 32.6085
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
				RETURN TRUE
			ELIF sCelebrationData.iCurrentCelebrationCamModifier = 5
				vAttachOffset = <<-0.0185, 1.7642, 0.4961>>
				vPointOffset = <<0.6648, -1.1468, 0.2534>>
				fFOV = 35.6186
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF bGetInVehicleOffsets
		PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS player is in a vehicle.")
		IF sCelebrationData.iCurrentCelebrationCam < 6
			IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID()) OR IS_PED_IN_ANY_JETSKI(PLAYER_PED_ID())
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS player is in a bike or jetski.")
				SWITCH sCelebrationData.iCurrentCelebrationCamModifier % 7
					CASE 0	vAttachOffset = <<-0.4500, 2.500, 0.3500>>	vPointOffset = <<0.300, 0.0000, 0.4500>>	fFOV = 26.0000 	BREAK
					CASE 1	vAttachOffset = <<1.1000, 2.500, 0.0500>>	vPointOffset = <<-0.300, 0.0000, 0.5000>>	fFOV = 26.0000	BREAK
					CASE 2	vAttachOffset = <<-1.4500, 2.150, 0.0500>>	vPointOffset = <<0.500, 0.0000, 0.5000>>	fFOV = 26.0000 	BREAK
					CASE 3	vAttachOffset = <<-0.175, 1.100, 0.788>>	vPointOffset = <<0.063, 0.0000, 0.466>>		fFOV = 37.800	BREAK
					CASE 4	vAttachOffset = <<1.600, 1.338, 0.688>>		vPointOffset = <<0.025, 0.0000, 0.416>>		fFOV = 26.000	BREAK
					CASE 5	vAttachOffset = <<-1.013, 0.726, 0.813>>	vPointOffset = <<-0.050, 0.0000, 0.441>>	fFOV = 37.800	BREAK
					CASE 6	vAttachOffset = <<1.038, 1.376, 0.826>>		vPointOffset = <<-0.050, 0.0000, 0.466>>	fFOV = 26.000	BREAK
				ENDSWITCH
			ELSE
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS player is in a vehicle other than a bike or jetski.")
				SWITCH sCelebrationData.iCurrentCelebrationCamModifier % 6
					CASE 0	vAttachOffset = <<-0.450, 2.500, 0.350>>	vPointOffset = <<0.300, 0.0000, 0.263>>		fFOV = 26.0000 BREAK
					CASE 1	vAttachOffset = <<1.100, 2.500, 0.050>>		vPointOffset = <<-0.300, 0.0000, 0.375>>	fFOV = 26.0000 BREAK
					CASE 2	vAttachOffset = <<-1.450, 2.150, 0.050>>	vPointOffset = <<0.500, 0.0000, 0.375>>		fFOV = 26.0000 BREAK						
					CASE 3	vAttachOffset = <<0.388, 2.489, 0.400>>		vPointOffset = <<-0.287, 0.0000, 0.304>>	fFOV = 26.0000 BREAK						
					CASE 4	vAttachOffset = <<0.388, 2.177, -0.575>>	vPointOffset = <<-0.237, 0.0000, 0.617>>	fFOV = 26.0000 BREAK						
					CASE 5	vAttachOffset = <<-0.575, 1.901, -0.112>>	vPointOffset = <<0.275, 0.0000, 0.516>>		fFOV = 26.0000 BREAK						
				ENDSWITCH
			ENDIF
			PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
			PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
			PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
			PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
			RETURN TRUE
		ELSE
			PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
			RETURN FALSE
		ENDIF
	ELSE
		
		IF IS_PED_IN_COVER(PLAYER_PED_ID())
			
			bUsingCoverOffsets = TRUE
				
			PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS player is in cover.")
			
			IF IS_PED_IN_HIGH_COVER(PLAYER_PED_ID())
				
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS cover type is standing.")
				
				IF sCelebrationData.iCurrentCelebrationCam < 6
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCam = ", sCelebrationData.iCurrentCelebrationCam)
					IF sCelebrationData.iCurrentCelebrationCamModifier = 0
						vAttachOffset = <<-0.5356, 1.4423, 0.3999>>
						vPointOffset = <<0.9181, -1.1788, 0.5275>>
						fFOV = 29.2736
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
						RETURN TRUE
					ELIF sCelebrationData.iCurrentCelebrationCamModifier = 1
						vAttachOffset = <<0.0181, 1.4527, 0.4043>>
						vPointOffset = <<-0.1861, -1.5314, 0.6351>>
						fFOV = 29.2736
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
						RETURN TRUE
					ELIF sCelebrationData.iCurrentCelebrationCamModifier = 2
						vAttachOffset = <<-0.5997, 1.0214, 0.4170>>
						vPointOffset = <<1.4133, -1.1792, 0.7419>>
						fFOV = 29.2736
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
						RETURN TRUE
					ENDIF	
				ELSE
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
					RETURN FALSE
				ENDIF
				
			ELSE
				
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS cover type is crouching.")
				
				IF sCelebrationData.iCurrentCelebrationCam < 6
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCam = ", sCelebrationData.iCurrentCelebrationCam)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
					IF sCelebrationData.iCurrentCelebrationCamModifier > 2
						DEBUG_PRINTCALLSTACK()
						PRINTLN("[NETCELEBRATION] [B*3452165] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN  iCurrentCelebrationCamModifier should have been below 3")
						SCRIPT_ASSERT("[NETCELEBRATION] [B*3452165] Alasdair Deacon - PLACE_CAMERA_FOR_CELEBRATION_SCREEN  iCurrentCelebrationCamModifier should have been below 3")
						sCelebrationData.iCurrentCelebrationCamModifier = GET_RANDOM_INT_IN_RANGE(0, 3)
					ENDIF
					IF sCelebrationData.iCurrentCelebrationCamModifier = 0
						vAttachOffset = <<-0.5893, 1.4073, -0.0900>>
						vPointOffset = <<0.8156, -1.2407, -0.2103>>
						fFOV = 31.7183
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
						RETURN TRUE
					ELIF sCelebrationData.iCurrentCelebrationCamModifier = 1
						vAttachOffset = <<-0.0030, 1.4062, -0.0960>>
						vPointOffset = <<-0.5691, -1.5399, -0.1137>>
						fFOV = 31.7183
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
						RETURN TRUE
					ELIF sCelebrationData.iCurrentCelebrationCamModifier = 2
						vAttachOffset = <<-0.6984, 1.1364, -0.0971>>
						vPointOffset = <<1.2011, -1.1855, -0.1149>>
						fFOV = 31.7183
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
						RETURN TRUE
					ENDIF	
				ELSE
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
					RETURN FALSE
				ENDIF
				
			ENDIF
			
			IF NOT IS_PED_IN_COVER_FACING_LEFT(PLAYER_PED_ID())
				vAttachOffset.X = vAttachOffset.X * (-1)
				vPointOffset.X = vPointOffset.X * (-1)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS cover is facing left.")
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
			ENDIF
			
		ELSE
			
			PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS player is not in cover.")
			
			IF sCelebrationData.iCurrentCelebrationCam < 6
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCam = ", sCelebrationData.iCurrentCelebrationCam)
				IF sCelebrationData.iCurrentCelebrationCamModifier = 0
					vLookOffset = <<1.3494, 0.8708, 0.3814>>
					vAttachOffset = <<0.6428, 1.0178 + fYOffset, 0.0411 + fZoffset>>
					vPointOffset = <<-0.3699, -1.5251 + fYOffset, 1.2691 + fZoffset>>
					fFOV = 34.3832
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vLookOffset = ", vLookOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
					RETURN TRUE
				ELIF sCelebrationData.iCurrentCelebrationCamModifier = 1
					vLookOffset = <<0.2657, 1.3743, 0.3640>>
					vLookOffset = (vLookOffset / VMAG(vLookOffset)) * 5.0 //This is to ensure that if the player is running forward then they don't run past the look coords.
					vAttachOffset = <<0.6768, 0.9824 + fYOffset, 0.1864 + fZoffset>>
					vPointOffset = <<-1.3043, -1.0657 + fYOffset, 1.1243 + fZoffset>>
					fFOV = 34.3832
					bUseFullHeadLook = TRUE
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vLookOffset = ", vLookOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
					RETURN TRUE
				ELIF sCelebrationData.iCurrentCelebrationCamModifier = 2
					vLookOffset = <<0.5182, 1.1454, 0.4312>>
					vAttachOffset = <<0.2419, 1.1082 + fYOffset, 0.5226 + fZoffset>>
					vPointOffset = <<0.1327, -1.8846 + fYOffset, 0.6999 + fZoffset>>
					fFOV = 34.3832
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vLookOffset = ", vLookOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
					RETURN TRUE
				ELIF sCelebrationData.iCurrentCelebrationCamModifier = 3
					vLookOffset = <<0.4529, 1.0868, 0.3650>>
					vLookOffset = (vLookOffset / VMAG(vLookOffset)) * 5.0
					vAttachOffset = <<0.6216, 0.9493 + fYOffset, 0.4129 + fZoffset>>
					vPointOffset = <<-1.4273, -1.1768 + fYOffset, 0.9439 + fZoffset>>
					fFOV = 34.3832
					bUseFullHeadLook = TRUE
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vLookOffset = ", vLookOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
					RETURN TRUE
					
				ELIF sCelebrationData.iCurrentCelebrationCamModifier = 4
					vLookOffset = <<1.3811, 1.7491, 0.2960>>
					vLookOffset = (vLookOffset / VMAG(vLookOffset)) * 5.0 
					vAttachOffset = <<1.7091, 1.1087 + fYOffset, 0.3014 + fZoffset>>
					vPointOffset = <<-1.0311, -0.1093 + fYOffset, 0.3918 + fZoffset>>
					fFOV = 34.3832
					bUseFullHeadLook = TRUE
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vLookOffset = ", vLookOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
					RETURN TRUE
				ELIF sCelebrationData.iCurrentCelebrationCamModifier = 5
					vLookOffset = <<1.3494, 0.8708, 0.3814>>
					vAttachOffset = <<0.7036, 1.7661 + fYOffset, 0.2981 + fZoffset>>
					vPointOffset = <<0.2668, -1.2009 + fYOffset, 0.3770 + fZoffset>>
					fFOV = 34.3832
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vLookOffset = ", vLookOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vAttachOffset = ", vAttachOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS vPointOffset = ", vPointOffset)
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS fFOV = ", fFOV)
					RETURN TRUE
				ENDIF	
			ELSE
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: - GET_CELEBRATION_CAMERA_OFFSETS iCurrentCelebrationCamModifier = ", sCelebrationData.iCurrentCelebrationCamModifier)
					RETURN FALSE
			ENDIF
			
		ENDIF
	
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Checks if the local player is in the passenger seat of the valkyrie on the humane labs missions
/// RETURNS:
///    TRUE If the player is, False if they aren't
FUNC BOOL IS_PLAYER_VALKYRIE_GUNNER()
	
	VEHICLE_INDEX tempVeh
	PED_INDEX ped = PLAYER_PED_ID()
	
	IF g_FMMC_STRUCT.iRootContentIdHash = g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid
	OR g_FMMC_STRUCT.iRootContentIdHash = g_sMPTUNABLES.iroot_id_HASH_Humane_Labs_Valkyrie
		IF IS_PED_IN_ANY_HELI(ped)
			tempVeh = GET_VEHICLE_PED_IS_USING(ped)
			IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(ped)) = VALKYRIE 
			OR GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(ped)) = VALKYRIE2
				IF IS_PED_SITTING_IN_VEHICLE_SEAT(ped, tempVeh, VS_FRONT_RIGHT)
					PRINTLN("[NETCELEBRATION] [VEHICLE_CINEMATIC_CAM] - Player is the gunner in the valkyrie, returning true")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

INT iCinematicCamCounter = 0
/// PURPOSE:
///    Activates the cinematic cam when inside a vehicle for celebration screen
/// RETURNS:
///    TRUE once the cinematic cam has been activated or if the vehicle is not ok to freeze the screen, FALSE otherwise
FUNC BOOL ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN( BOOL bWaitOneFrame = TRUE )
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		PRINTLN("[NETCELEBRATION] [VEHICLE_CINEMATIC_CAM] - IS_NET_PLAYER_OK(TRUE)")
		
		SWITCH( iCinematicCamCounter )
			CASE 0
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					PRINTLN("[NETCELEBRATION] [VEHICLE_CINEMATIC_CAM] - IS_VEHICLE_DRIVEABLE(TRUE)")
					SET_CINEMATIC_MODE_ACTIVE(TRUE)
					PRINTLN("[NETCELEBRATION] [VEHICLE_CINEMATIC_CAM] - SET_CINEMATIC_MODE_ACTIVE(TRUE)")
					IF( bWaitOneFrame )
						iCinematicCamCounter = 1
					ELSE
						PRINTLN("[NETCELEBRATION] [VEHICLE_CINEMATIC_CAM] - Returning TRUE")
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[NETCELEBRATION] [VEHICLE_CINEMATIC_CAM] - Vehicle ped is in is not okay. Not activating Cinematic Cam but returning true to freeze the screen")
					RETURN TRUE
				ENDIF
			BREAK
			CASE 1
				PRINTLN("[NETCELEBRATION] [VEHICLE_CINEMATIC_CAM] - Returning TRUE")
				iCinematicCamCounter = 0
				RETURN TRUE
			BREAK
		ENDSWITCH
	ELSE
		PRINTLN("[NETCELEBRATION] [VEHICLE_CINEMATIC_CAM] - Player is not OK. Not activating Cinematic Cam but returning true to freeze the screen")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SHAKE_CELEB_CAM(CAMERA_INDEX &cam, FLOAT fByThisMuch)

	IF CONTENT_IS_USING_ARENA()
		EXIT
	ENDIF

	SHAKE_CAM(cam, "HAND_SHAKE", fByThisMuch)
	PRINTLN("SHAKE_CELEB_CAM by ", fByThisMuch)
ENDPROC

/// PURPOSE:
///    Places a camera in front of the player if safe to do so.
/// PARAMS:
///    sCelebrationData - The celebration data struct.
///    cam - The cam index used to place the end cam.
///    bSuccessfullyPlaced - TRUE if a cam was successfully placed. If FALSE then there needs to be a fallback option in the job script.
///    bUseParachuteCams - For basejumps there are some specific cam options that need to be used.
///    bUseInVehicleCams - Overrides a legacy lack of support for vehicles with a handful of generic suitable cameras
/// RETURNS:
///    TRUE once the function has either placed the cam or determined that it's not safe to place a cam in front of the player.
FUNC BOOL PLACE_CAMERA_FOR_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, CAMERA_INDEX &cam, BOOL &bSuccessfullyPlaced, BOOL bUseParachuteCams = FALSE, BOOL bUseInVehicleCams = FALSE)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		INT iHitCount
		VECTOR vHitPoint
		VECTOR vHitNormal
		ENTITY_INDEX entityHit
		
		VECTOR vAttachOffset
		VECTOR vPointOffset
		VECTOR vLookOffset
		FLOAT fFOV
		FLOAT fZoffset
		FLOAT fYOffset
		BOOL bUseFullHeadLook
		
		BOOL bGetBicycleOffsets
		
		BOOL bUsingCoverOffsets
		
		BOOL bInInvalidVehicle
		bInInvalidVehicle = TRUE
			
		#IF IS_DEBUG_BUILD
			#IF ALLOW_CELEBRATION_WIDGETS
				IF sCelebrationWidgetData.bCelebScreenOverrideSummaryCams
					bUseParachuteCams = sCelebrationWidgetData.bCelebScreenUseParachuteSummaryCams
				ENDIF
			#ENDIF
		#ENDIF
		
		IF bUseParachuteCams
			IF sCelebrationData.stiCelebrationCam = NULL
				IF IS_PED_IN_COVER(PLAYER_PED_ID())
					sCelebrationData.iCurrentCelebrationCamModifier = GET_RANDOM_INT_IN_RANGE(0, 3)
				ELSE
					sCelebrationData.iCurrentCelebrationCamModifier = GET_RANDOM_INT_IN_RANGE(0, 6)
				ENDIF
			ENDIF
		
			#IF IS_DEBUG_BUILD
				#IF ALLOW_CELEBRATION_WIDGETS
					IF sCelebrationWidgetData.bCelebScreenOverrideSummaryCams
						sCelebrationData.iCurrentCelebrationCam = sCelebrationWidgetData.iCelebScreenCamMainIndex
						sCelebrationData.iCurrentCelebrationCamModifier = sCelebrationWidgetData.iCelebScreenCamRandomIndex
					ENDIF
				#ENDIF
			#ENDIF
			
			IF sCelebrationData.iCurrentCelebrationCam < 6
				IF sCelebrationData.iCurrentCelebrationCamModifier = 0
					vLookOffset = <<1.3494, 0.8708, 0.3814>>
					vAttachOffset = <<-0.3050, 3.4040, 1.8730>>
					vPointOffset = <<0.3318, 0.6829, 0.7820>>
					fFOV = 48.8720
				ELIF sCelebrationData.iCurrentCelebrationCamModifier = 1	
					vLookOffset = <<1.3494, 0.8708, 0.3814>>
					vAttachOffset = <<-0.3027, 1.9371, 0.8861>>
					vPointOffset = <<0.7526, -0.7846, 0.1942>>
					fFOV = 49.9607
				ELIF sCelebrationData.iCurrentCelebrationCamModifier = 2
					vLookOffset = <<1.3494, 0.8708, 0.3814>>
					vAttachOffset = <<-0.3685, 5.6646, 3.8144>>
					vPointOffset = <<0.3407, 3.2028, 2.2534>>
					fFOV = 49.9607
				ELIF sCelebrationData.iCurrentCelebrationCamModifier = 3
					vLookOffset = <<1.3494, 0.8708, 0.3814>>
					vAttachOffset = <<-0.5510, 1.5299, 0.0099>>
					vPointOffset = <<1.1136, -0.8876, 0.6298>>
					fFOV = 40.5195
				ELIF sCelebrationData.iCurrentCelebrationCamModifier = 4
					vLookOffset = <<-0.6761, 2.3219, 0.3315>>
					vAttachOffset = <<0.1252, 1.6920, 0.0052>>
					vPointOffset = <<-0.9193, -1.0597, 0.5854>>
					fFOV = 40.5195
				ELIF sCelebrationData.iCurrentCelebrationCamModifier = 5
					vLookOffset = <<1.3494, 0.8708, 0.3814>>
					vAttachOffset = <<-0.0980, 1.4671, 0.0645>>
					vPointOffset = <<0.3334, -1.4352, 0.6893>>
					fFOV = 40.5195
				ENDIF
			ELSE
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: All options exhausted, no suitable cam available.")
		
				bSuccessfullyPlaced = FALSE
				sCelebrationData.stiCelebrationCam = NULL
				sCelebrationData.iCurrentCelebrationCam = 0
				
				RETURN TRUE
			ENDIF
		ELSE
			
			IF sCelebrationData.stiCelebrationCam = NULL
				IF IS_PED_IN_COVER(PLAYER_PED_ID())
					sCelebrationData.iCurrentCelebrationCamModifier = GET_RANDOM_INT_IN_RANGE(0, 3)
				ELSE
					sCelebrationData.iCurrentCelebrationCamModifier = GET_RANDOM_INT_IN_RANGE(0, 6)
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				#IF ALLOW_CELEBRATION_WIDGETS
					IF sCelebrationWidgetData.bCelebScreenOverrideSummaryCams
						sCelebrationData.iCurrentCelebrationCam = sCelebrationWidgetData.iCelebScreenCamMainIndex
						sCelebrationData.iCurrentCelebrationCamModifier = sCelebrationWidgetData.iCelebScreenCamRandomIndex
						PRINTLN("[NETCELEBRATION] [B*3452165] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN  iCurrentCelebrationCamModifier was set here")
					ENDIF
				#ENDIF
			#ENDIF
			
			//The original offsets were grabbed using a high heel ped so we need to adjust.
			IF NOT IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
				fZoffset = -0.09
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX tempVeh
				tempVeh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
				MODEL_NAMES eTempModel
				eTempModel = GET_ENTITY_MODEL(tempVeh)
				IF IS_THIS_MODEL_A_BICYCLE(eTempModel)
					bInInvalidVehicle = FALSE
					bGetBicycleOffsets = TRUE
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: on a bike or bycicle, setting bInInvalidVehicle = FALSE.")
				ENDIF
			ELSE
				bInInvalidVehicle = FALSE
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: not in a vehicle, setting bInInvalidVehicle = FALSE.")
			ENDIF
			IF bInInvalidVehicle AND bUseInVehicleCams
				IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
					sCelebrationData.iCurrentCelebrationCamModifier = GET_RANDOM_INT_IN_RANGE(0, 7)
				ELSE
					sCelebrationData.iCurrentCelebrationCamModifier = GET_RANDOM_INT_IN_RANGE(0, 6)
				ENDIF
			ENDIF
		
			// The original PLACE_CAMERA_FOR_CELEBRATION_SCREEN considered all vehicles accept bicycles invalid
			// Introducing bUseInVehicleCams provides some support for these vehicles so: 
			// `bUseInVehicleCams AND bInInvalidVehicle` checks if we're in a vehicle that needs one of these cams
			IF NOT GET_CELEBRATION_CAMERA_OFFSETS(sCelebrationData, fYOffset, fZoffset, bGetBicycleOffsets, bUseInVehicleCams AND bInInvalidVehicle, vLookOffset, vAttachOffset, vPointOffset, fFOV, bUsingCoverOffsets, bUseFullHeadLook)
				PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: GET_CELEBRATION_CAMERA_OFFSETS - All options exhausted, no suitable cam available.")
				bSuccessfullyPlaced = FALSE
				sCelebrationData.stiCelebrationCam = NULL
				sCelebrationData.iCurrentCelebrationCam = 0
				RETURN TRUE
			ENDIF
			
			IF bInInvalidVehicle AND bUseInVehicleCams
				bInInvalidVehicle = FALSE
			ENDIF
		ENDIF
				
		VECTOR vCamPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vAttachOffset)
		VECTOR vShapeTestCamPos = vCamPos
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		VECTOR vPlayerVel = GET_ENTITY_VELOCITY(PLAYER_PED_ID())
		FLOAT fPlayerSpeed = VMAG(vPlayerVel)
		
		PRINTLN("[NETCELEBRATION][PlayerPos] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: vPlayerPos: ", vPlayerPos)
		
		//If the player is moving then run the shapetest slightly further ahead.
		IF ABSF(fPlayerSpeed) > 0.5
			VECTOR vDir = vPlayerVel / fPlayerSpeed
			vShapeTestCamPos = vCamPos + vDir
		ENDIF

		IF sCelebrationData.stiCelebrationCam = NULL
			INT iFlags = SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_GLASS | SCRIPT_INCLUDE_FOLIAGE | SCRIPT_INCLUDE_PED // Included ped for url:bugstar:4426599
			//sCelebrationData.stiCelebrationCam = START_SHAPE_TEST_LOS_PROBE(vCamPos, vPlayerPos, iFlags, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_DEFAULT)
			
			sCelebrationData.stiCelebrationCam = START_SHAPE_TEST_SWEPT_SPHERE(vShapeTestCamPos, vPlayerPos, 0.25, iFlags, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_DEFAULT)
		ELSE
			SHAPETEST_STATUS eStatus = GET_SHAPE_TEST_RESULT(sCelebrationData.stiCelebrationCam, iHitCount, vHitPoint, vHitNormal, entityHit)
			
			SWITCH eStatus
				CASE SHAPETEST_STATUS_RESULTS_READY
					sCelebrationData.stiCelebrationCam = NULL
				
					IF iHitCount > 0
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: SHAPETEST_STATUS_RESULTS_READY, shapetest hit something. ", 
								  sCelebrationData.iCurrentCelebrationCam, " ", sCelebrationData.iCurrentCelebrationCamModifier)

						sCelebrationData.iCurrentCelebrationCam++
					ELSE
						PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: SHAPETEST_STATUS_RESULTS_READY, shapetest passed. ",
								  sCelebrationData.iCurrentCelebrationCam, " ", sCelebrationData.iCurrentCelebrationCamModifier)
						
						//NOTE: may need an option here for vehicles too?
						IF NOT bInInvalidVehicle
						AND NOT IS_PED_FALLING(PLAYER_PED_ID())
						AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
						AND NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID())
//						AND NOT IS_PED_IN_COVER(PLAYER_PED_ID())
						AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
						AND NOT IS_PED_CLIMBING(PLAYER_PED_ID())
							FLOAT fPlayerGroundZ, fCamGroundZ
							
							GET_GROUND_Z_FOR_3D_COORD(vPlayerPos, fPlayerGroundZ)
							GET_GROUND_Z_FOR_3D_COORD(vCamPos, fCamGroundZ)
						
							IF ABSF(fPlayerGroundZ - fCamGroundZ) > 5.0
								PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: SHAPETEST_STATUS_RESULTS_READY, ground heights are too different, possible collision with map. ",
										  sCelebrationData.iCurrentCelebrationCam, " ", sCelebrationData.iCurrentCelebrationCamModifier)

								sCelebrationData.iCurrentCelebrationCam++
							ELSE
								IF NOT DOES_CAM_EXIST(cam)
									cam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
								ENDIF
							
								STOP_CAM_POINTING(cam)
								
								// Prevents seeing under the world.
								IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
								AND NOT IS_PED_INJURED(PLAYER_PED_ID())
									PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: Player is not spectating/dead, attaching cam.")
									ATTACH_CAM_TO_ENTITY(cam, PLAYER_PED_ID(), vAttachOffset)
								ELSE
									PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: Player IS spectating, DON'T attach the cam.")
								ENDIF
								
								POINT_CAM_AT_ENTITY(cam, PLAYER_PED_ID(), vPointOffset)
								SET_CAM_FOV(cam, fFOV)
								SHAKE_CELEB_CAM(cam, 0.3)
								SET_CAM_NEAR_CLIP(cam, 0.15)
								
								IF NOT bUsingCoverOffsets
									IF bUseFullHeadLook
										TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vLookOffset), 20000, SLF_WHILE_NOT_IN_FOV)
									ELSE
										TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vLookOffset), 20000, SLF_WHILE_NOT_IN_FOV | SLF_USE_EYES_ONLY)
									ENDIF
								ENDIF
								
								PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: SHAPETEST_STATUS_RESULTS_READY, Created camera succesfully. ",
										  sCelebrationData.iCurrentCelebrationCam, " ", sCelebrationData.iCurrentCelebrationCamModifier)
								
								bSuccessfullyPlaced = TRUE
								sCelebrationData.iCurrentCelebrationCam = 0
								
								RETURN TRUE
							ENDIF
						ELSE
							PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: SHAPETEST_STATUS_RESULTS_READY, not safe to create on-foot camera. ",
									  sCelebrationData.iCurrentCelebrationCam, " ", sCelebrationData.iCurrentCelebrationCamModifier)
							
							sCelebrationData.iCurrentCelebrationCam++
						ENDIF
					ENDIF				
				BREAK
				
				CASE SHAPETEST_STATUS_NONEXISTENT
					PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: SHAPETEST_STATUS_NONEXISTENT ",
							  sCelebrationData.iCurrentCelebrationCam, " ", sCelebrationData.iCurrentCelebrationCamModifier)
					
					sCelebrationData.stiCelebrationCam = NULL
					sCelebrationData.iCurrentCelebrationCam++
	            BREAK
			ENDSWITCH
		ENDIF
	ELSE
		PRINTLN("[NETCELEBRATION] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN: SHAPETEST_STATUS_NONEXISTENT ",
					 sCelebrationData.iCurrentCelebrationCam, " ", sCelebrationData.iCurrentCelebrationCamModifier)
		bSuccessfullyPlaced = FALSE
		sCelebrationData.iCurrentCelebrationCam = 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Takes an existing celebration screen camera and does a zoom trnasition, this is intended for use when transitioning from the summary screen to gameplay/winner.
FUNC BOOL START_CELEBRATION_CAMERA_ZOOM_TRANSITION(CAMERA_INDEX &cam)
	IF DOES_CAM_EXIST(cam)
		VECTOR vCamPos = GET_CAM_COORD(cam)
		VECTOR vCamRot = GET_CAM_ROT(cam)
		FLOAT fCamFOV = GET_CAM_FOV(cam)
		
		STOP_CAM_POINTING(cam)
		SET_CAM_PARAMS(cam, vCamPos, vCamRot, fCamFOV - 3.0, 0)
		SET_CAM_PARAMS(cam, vCamPos, vCamRot, fCamFOV, 1500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
		SHAKE_CELEB_CAM(cam, 0.3)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Places the given camera at a suitable location to exit the celebration screen at the end of the job (currently set to look at the sky).  
FUNC BOOL PLACE_CAMERA_FOR_CELEBRATION_SCREEN_END(CAMERA_INDEX &cam)
	IF NOT DOES_CAM_EXIST(cam)
		cam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
	ENDIF

	VECTOR vCamPos = GET_ENTITY_COORDS(PLAYER_PED_ID()) + 20.0
	
	STOP_CAM_POINTING(cam)
	SET_CAM_COORD(cam, vCamPos)
	SET_CAM_ROT(cam, <<89.5, 0.0, 0.0>>)
	SET_CAM_FOV(cam, 45.0)
	STOP_CAM_SHAKING(cam, TRUE)
	SET_CAM_NEAR_CLIP(cam, 0.01)
	
	IF NOT IS_CAM_RENDERING(cam)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ENDIF
	
	ANIMPOSTFX_PLAY("SwitchShortNeutralIn", 0, FALSE)
	
	RETURN TRUE
ENDFUNC

PROC CREATE_CLONE_OF_PED_FOR_WINNER_SCENE(PED_INDEX &ped, PED_INDEX pedToClone, VECTOR vPos, FLOAT fHeading, INT iPlayerID, BOOL bWearingProblemMaskOrHelmet, BOOL bIsWinner)

	//2228298
	vPos.z += 1
	CREATE_CLONE_OF_PED_AT_COORDS(ped, pedToClone, vPos, fHeading, iPlayerID, bWearingProblemMaskOrHelmet)

	IF DOES_ENTITY_EXIST(ped)
		IF IS_PED_WEARING_JUGGERNAUT_SUIT(ped)
		AND bIsWinner
			PRINTLN("CREATE_CLONE_OF_PED_FOR_WINNER_SCENE - Winner scene ped is a juggernaut. Giving minigun to ped")
			GIVE_WEAPON_TO_PED(ped, WEAPONTYPE_MINIGUN, -1, TRUE, TRUE)
		ENDIF
		
		IF CONTENT_IS_USING_ARENA()
			IF IS_PED_WEARING_HIGH_HEELS(ped)
				PRINTLN("CREATE_CLONE_OF_PED_FOR_WINNER_SCENE - Winner scene ped is wearing high heels in arena. Replacing high heels.")
				SET_PED_COMPONENT_VARIATION(ped, PED_COMP_FEET, 3, 0)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    The winner screen is a specific type of celebration screen that shows the player who won the job. This function will setup the scene backdrop: it'll create a clone of the winning player,
///    position them against a plain backdrop (most likely under the map) and place a camera in front of them.
/// PARAMS:
///    camWinner - The index of the cam that'll be pointed at the ped. In most cases this screen follows on directly from a job end summary, so the same cam can be used.
///    playerWinner - The ID of the player who won the job.
///    pedAIWinner - If an AI ped won the race instead of a player then they can be cloned using this index (used for the tutorial with Lamar).
/// RETURNS:
///    TRUE if the winner scene has been setup successfully. If FALSE then it may have failed to clone the winning player.
FUNC BOOL CREATE_WINNER_SCENE(CELEBRATION_SCREEN_DATA &sCelebrationData, CAMERA_INDEX &camWinner, PLAYER_INDEX playerWinner, PED_INDEX pedAIWinner = NULL)
	IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClone)
		RETURN TRUE
	ELSE
		VECTOR vCurrentCamPos = GET_FINAL_RENDERED_CAM_COORD()
		VECTOR vNewCamPos = <<vCurrentCamPos.x, vCurrentCamPos.y, 1320.0>> //This is the height of the skycam.
		BOOL bWearingProblemMaskOrHat
		
		IF pedAIWinner != NULL
			CREATE_CLONE_OF_PED_FOR_WINNER_SCENE(sCelebrationData.pedWinnerClone, pedAIWinner, vNewCamPos, 0.0, -1, FALSE, TRUE)
		ELSE
			PED_INDEX pedTemp = GET_PLAYER_PED(playerWinner)			
		
			//As a fallback if the winning player doesn't exist then use the local player.
			IF IS_PED_INJURED(pedTemp)
				PRINTLN("[NETCELEBRATION] winning player index is invalid.")
				
				playerWinner = PLAYER_ID()
				pedTemp = PLAYER_PED_ID()
			ENDIF
			
			bWearingProblemMaskOrHat = IS_PED_WEARING_PROBLEM_MASK_OR_HAT_FOR_INTERACTION(	pedTemp, 
																							GlobalplayerBD[NATIVE_TO_INT(playerWinner)].iInteractionType, 
																							GlobalplayerBD[NATIVE_TO_INT(playerWinner)].iInteractionAnim,
																							TRUE, 
																							FALSE)
			
			CREATE_CLONE_OF_PED_FOR_WINNER_SCENE(sCelebrationData.pedWinnerClone, pedTemp, vNewCamPos, 0.0, NATIVE_TO_INT(playerWinner), bWearingProblemMaskOrHat, TRUE)
			SET_FACIAL_IDLE_ANIM_OVERRIDE(sCelebrationData.pedWinnerClone, GET_PLAYER_MOOD_ANIM_FROM_INDEX(GlobalplayerBD_FM[NATIVE_TO_INT(playerWinner)].iPlayerMood))
			PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE, SET_FACIAL_IDLE_ANIM_OVERRIDE for playerWinner  ")
		ENDIF
		
		IF NOT IS_PED_INJURED(sCelebrationData.pedWinnerClone)
			//NOTE: no need to line the ped up with the camera now as we're using the UI3D commands.
			SET_ENTITY_COORDS(sCelebrationData.pedWinnerClone, vNewCamPos + <<20.0 + NATIVE_TO_INT(PLAYER_ID()), -0.260, -2.850>>)
			SET_ENTITY_ROTATION(sCelebrationData.pedWinnerClone, <<0.000, 0.000, 0.000>>)
		
			//SET_ENTITY_COORDS(sCelebrationData.pedWinnerClone, vNewCamPos + <<0.0, -0.260, -2.850>>)
			//SET_ENTITY_ROTATION(sCelebrationData.pedWinnerClone, <<-85.000, 0.000, 200.000>>, EULER_ZXY) //Euler order changed so we can lay the ped horizontal but turned slightly.
			FREEZE_ENTITY_POSITION(sCelebrationData.pedWinnerClone, TRUE)
			
			IF IS_PED_MALE(sCelebrationData.pedWinnerClone)
				TASK_PLAY_ANIM(sCelebrationData.pedWinnerClone, "move_m@generic", "idle", INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
			ELSE
				TASK_PLAY_ANIM(sCelebrationData.pedWinnerClone, "move_f@generic", "idle", INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
			ENDIF
			
			TASK_LOOK_AT_COORD(sCelebrationData.pedWinnerClone, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebrationData.pedWinnerClone, <<0.7866, 2.2904, 0.7608>>), 
							   -1, SLF_WHILE_NOT_IN_FOV)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(sCelebrationData.pedWinnerClone)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sCelebrationData.pedWinnerClone, TRUE)
		ENDIF
		
		IF NOT DOES_CAM_EXIST(camWinner)
			camWinner = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
		ENDIF
		
		STOP_CAM_POINTING(camWinner)
		SET_CAM_COORD(camWinner, vNewCamPos)
		//SET_CAM_ROT(camWinner, <<-89.5, 0.0, 0.0>>)
		SET_CAM_ROT(camWinner, <<0.0, 0.0, 0.0>>)
		SET_CAM_FOV(camWinner, 45.0)
		SHAKE_CELEB_CAM(camWinner, 0.05)
		SET_CAM_NEAR_CLIP(camWinner, 0.01)
		
		IF NOT IS_CAM_RENDERING(camWinner)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets an offset from the winner background based on the given leaderboard position. Players at the top of the leaderboard are placed further away from the background
///    (closer to the camera).
FUNC VECTOR private_GET_WINNER_PED_OFFSET_FROM_LEADERBOARD_POSITION(INT iLeaderboardPos, BOOL bForCarScene = FALSE)
	VECTOR vOffset
	VECTOR vRootOffset = <<-0.0496, -4.7404, 0.0000>>//<<0.0000, -4.7404, 0.0000>>
	
	IF iLeaderboardPos = 0
		vOffset = vRootOffset
	ELIF iLeaderboardPos = 1
		vOffset = <<0.4779, -3.8497, 0.0000>>
	ELIF iLeaderboardPos = 2
		vOffset = <<-0.9088, -3.4767, 0.0000>>
	ELIF iLeaderboardPos = 3
		vOffset = <<1.2558, -2.6567, 0.0000>>
	ELIF iLeaderboardPos = 4
		vOffset = <<-0.5742, -2.9006, 0.0000>>
	ELIF iLeaderboardPos = 5
		vOffset = <<1.4563, -3.1976, 0.0000>> 
	ELIF iLeaderboardPos = 6
		vOffset = <<-1.5582, -2.8027, 0.0000>>
	ELSE
		vOffset = <<2.0871, -2.7957, 0.0000>>
	ENDIF
	
	IF bForCarScene
		vOffset.x += 1.0
		vOffset.y += 0.6
	ENDIF
	
	RETURN vOffset
ENDFUNC

FUNC VECTOR private_GET_WINNER_PED_ROTATION_FROM_LEADERBOARD_POSITION(INT iLeaderboardPos)
	VECTOR vRotation
	
	IF iLeaderboardPos = 0
		vRotation = <<0.0, 0.0, 180.0000>>
	ELIF iLeaderboardPos = 1
		vRotation = <<0.0, 0.0, 152.2800>>
	ELIF iLeaderboardPos = 2
		vRotation = <<0.0, 0.0, 194.7600>>
	ELIF iLeaderboardPos = 3
		vRotation = <<0.0, 0.0, 147.9600>>
	ELIF iLeaderboardPos = 4
		vRotation = <<0.0, 0.0, 171.3600>>
	ELIF iLeaderboardPos = 5
		vRotation = <<0.0, 0.0, 149.4000>>
	ELIF iLeaderboardPos = 6
		vRotation = <<0.0, 0.0, 206.2800>>
	ELSE
		vRotation = <<0.0, 0.0, 138.9600>>
	ENDIF
	
	RETURN vRotation
ENDFUNC

FUNC VECTOR private_GET_WINNER_PED_OFFSET_FROM_LEADERBOARD_POSITION_FOR_CAR(INT iLeaderboardPos)
	VECTOR vOffset
	VECTOR vRootOffset = <<0.2404, -4.1699, 0.0000>>
	
	IF iLeaderboardPos = 0
		vOffset = vRootOffset
	ELSE
		vOffset = <<-1.1021, -2.4800, 0.0000>>
	ENDIF
	
	RETURN vOffset
ENDFUNC

FUNC VECTOR private_GET_WINNER_PED_ROTATION_FROM_LEADERBOARD_POSITION_FOR_CAR(INT iLeaderboardPos)	
	VECTOR vRotation
	
	IF iLeaderboardPos = 0
		vRotation = <<0.0, 0.0, 210.9680>>
	ELSE
		vRotation = <<0.0, 0.0, 197.6400>>
	ENDIF
	
	RETURN vRotation
ENDFUNC

//PURPOSE: Picks a random Base for the ped
PROC GET_CELEBRATION_IDLE_ANIM_BASE(PED_INDEX pedId, TEXT_LABEL_63 &Dict, TEXT_LABEL_31 &Base, FLOAT &fRandomPhase)

	INT iRand

	//MALE
	IF IS_PED_MALE(pedId)
		
		iRand = GET_RANDOM_INT_IN_RANGE(0, 11)
		
		IF iRand <= 1     //Double Chance (due to having idles)
			Dict = "ANIM@MP_CORONA_IDLES@MALE_A@BASE"
			Base = "BASE"
		ELIF iRand <= 3   //Double Chance (due to having idles)
			Dict =  "ANIM@MP_CORONA_IDLES@MALE_B@BASE"
			Base = "BASE"
		ELIF iRand <= 4
			Dict =  "ANIM@MP_CORONA_IDLES@MALE_C@BASE"
			Base = "BASE"
		ELIF iRand <= 5
			Dict =  "ANIM@MP_CORONA_IDLES@MALE_D@BASE"
			Base = "BASE"
		ELIF iRand <= 6
			Dict =  "ANIM@MP_CORONA_IDLES@MALE_E@BASE"
			Base = "BASE"
		ELIF iRand <= 7
			Dict =  "ANIM@HEISTS@HEIST_CORONA@SINGLE_TEAM"
			Base = "Single_Team_Loop_two"
		ELIF iRand <= 8
			Dict =  "ANIM@HEISTS@HEIST_CORONA@TEAM_IDLES@MALE_A"
			Base = "idle"
		ELIF iRand <= 9
			Dict =  "ANIM@HEISTS@HEIST_CORONA@TEAM_IDLES@MALE_B"
			Base = "idle"
		ELSE
			Dict =  "ANIM@HEISTS@HEIST_CORONA@TEAM_IDLES@MALE_C"
			Base = "idle"
		ENDIF

	//FEMALE
	ELSE
		
		iRand = GET_RANDOM_INT_IN_RANGE(0, 6)
		
		IF iRand <= 1     //Double Chance (due to having idles)
			Dict =  "ANIM@MP_CORONA_IDLES@FEMALE_A@BASE"
			Base = "BASE"
		ELIF iRand <= 2
			Dict =  "ANIM@MP_CORONA_IDLES@FEMALE_B@BASE"
			Base = "BASE"
		ELIF iRand <= 3
			Dict =  "ANIM@MP_CORONA_IDLES@FEMALE_C@BASE"
			Base = "BASE"
		ELIF iRand <= 4
			Dict =  "ANIM@HEISTS@HEIST_CORONA@SINGLE_TEAM"
			Base = "Single_Team_Loop_three"
		ELSE
			Dict =  "ANIM@HEISTS@HEIST_CORONA@TEAM_IDLES@FEMALE_A"
			Base = "idle"
		ENDIF
		
	ENDIF
	
	fRandomPhase = GET_RANDOM_FLOAT_IN_RANGE()
	
	PRINTLN("[NETCELEBATION] - GET_CELEBRATION_IDLE_ANIM_BASE - Dict = ", Dict, " Base = ", Base, " fRandomPhase = ", fRandomPhase) 

ENDPROC

PROC SET_HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	IF NOT sCelebrationData.bHidePlayers
		#IF IS_DEBUG_BUILD
		PRINTLN("[NETCELEBRATION] - SET_HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE - set bHidePlayers = TRUE.")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		sCelebrationData.bHidePlayers = TRUE
	ENDIF
	
ENDPROC

PROC HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	IF sCelebrationData.bHidePlayers
		
		INT iPlayer
		PLAYER_INDEX playerId
		
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPlayer
			
			playerId = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
			
			IF IS_NET_PLAYER_OK(playerId, FALSE)
				SET_PLAYER_INVISIBLE_LOCALLY(playerId)
			ENDIF
		
		ENDREPEAT
	
	ENDIF
	
ENDPROC

FUNC BOOL CELEBRATION_WINNER_USES_INTERIOR()

	IF CELEBRATION_IS_ISLAND_MODE()
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC STRING INTERIOR_NAME()

	IF CONTENT_IS_USING_ARENA()
		RETURN "xs_x18_int_01"
	ENDIF

	RETURN "v_winningroom"
ENDFUNC

FUNC VECTOR INTERIOR_POS()

	IF CONTENT_IS_USING_ARENA()
		RETURN <<2800.000,-3800.000,100.000>>
	ENDIF

	RETURN << 414.4, -977.6, -99.0 >>
ENDFUNC

FUNC VECTOR CELEBRATION_GET_PED_OFFSCREEN_POS()

	IF CELEBRATION_IS_ISLAND_MODE()
		RETURN <<4496.8687, -4497.9380, 3.1895>>
	ENDIF
		
	IF CONTENT_IS_USING_ARENA()
	
		RETURN <<2939.9082, -3883.8604, 139.1>>
	ENDIF

	RETURN <<418.9326, -979.3876, -100.0042>> 
	
ENDFUNC

FUNC VECTOR CELEBRATION_GET_PED_WARP_POS(CELEB_SERVER_DATA &sCelebServer, INT iPed = 0)

	IF CELEBRATION_IS_ISLAND_MODE()
		SWITCH iPed
			CASE 0			RETURN <<4523.0000, -4488.2642, 3.1814>>
			CASE 1			RETURN <<4525.7861, -4485.8188, 3.1660>>
			CASE 2			RETURN <<4526.7002, -4487.2690, 3.1580>>
			CASE 3			RETURN <<4527.2031, -4484.3589, 3.1700>>
			CASE 4			RETURN <<4525.7441, -4484.2422, 3.1750>>
			CASE 5			RETURN <<4526.4829, -4490.8521, 3.1690>>
			CASE 6			RETURN <<4526.0278, -4491.9668, 3.1770>>
			CASE 7			RETURN <<4526.8760, -4491.6948, 3.1710>>
			CASE 8			RETURN <<4527.9590, -4484.8711, 3.1660>>
			CASE 9			RETURN <<4527.7852, -4488.5361, 3.1540>>
			CASE 10			RETURN <<4528.4468, -4486.5298, 3.1560>>
			CASE 11			RETURN <<4528.6211, -4489.3008, 3.1520>>
			CASE 12			RETURN <<4528.6929, -4487.8901, 3.1480>>
			CASE 13			RETURN <<4527.0850, -4493.3149, 3.1780>>
			CASE 14			RETURN <<4527.1538, -4485.9800, 3.1620>>
			CASE 15			RETURN <<4526.2441, -4493.4038, 3.1810>>
		ENDSWITCH
		
		RETURN <<4523.0000, -4488.2642, 3.1814>>
	ENDIF

	IF CONTENT_IS_USING_ARENA()
	
		IF ARENA_IS_PODIUM(sCelebServer)
		
			SWITCH iPed
				CASE 0
					RETURN <<2828.9197, -3901.4749, 139.3957>>
				BREAK
				CASE 1
					RETURN <<2829.5100, -3902.0154, 139.2491>>
				BREAK
				CASE 2
					RETURN <<2828.3645, -3900.9666, 139.2491>>
				BREAK
			ENDSWITCH
			
			RETURN <<2828.9197, -3901.4749, 139.3957>>
		ENDIF
	
		SWITCH iPed
		
			CASE 0			RETURN <<2963.9250, -3870.1370, 140.0820>>
			CASE 1			RETURN <<2963.0681, -3870.5969, 140.0820>>
			CASE 2			RETURN <<2960.2170, -3874.7029, 140.0820>>
			CASE 3			RETURN <<2960.6116, -3873.7019, 140.0732>>
			CASE 4			RETURN <<2965.2717, -3869.3054, 140.0820>>
			CASE 5			RETURN <<2959.6426, -3871.9988, 140.0820>>
			CASE 6			RETURN <<2964.4490, -3869.2000, 140.0820>>
			CASE 7			RETURN <<2963.4331, -3868.5659, 140.0820>>
			CASE 8			RETURN <<2959.9141, -3872.7859, 140.0820>>
			CASE 9			RETURN <<2959.7351, -3873.8950, 140.0750>>
			CASE 10			RETURN <<2965.6599, -3868.6580, 140.0820>>
			CASE 11			RETURN <<2962.8059, -3869.4861, 140.0820>>
			CASE 12			RETURN <<2960.4307, -3871.7800, 140.0820>>
			CASE 13			RETURN <<2959.7371, -3875.4561, 140.0820>>
			CASE 14			RETURN <<2961.8320, -3870.3049, 140.0820>>
			CASE 15			RETURN <<2957.6748, -3874.1975, 140.0816>>
			
		ENDSWITCH
	
		RETURN <<2828.2856, -3904.4929, 139.0100>>
	ENDIF

	RETURN <<418.9326, -979.3876, -100.0042>> // Inside interior but out of shot.
ENDFUNC

FUNC FLOAT CELEBRATION_GET_PED_WARP_ROT(INT iPed = 0)

	IF CELEBRATION_IS_ISLAND_MODE()
		SWITCH iPed				   
			CASE 0			RETURN 68.00
			CASE 1			RETURN 122.5990
			CASE 2			RETURN 92.1990
			CASE 3			RETURN 137.5980
			CASE 4			RETURN 139.7990
			CASE 5			RETURN 56.7990
			CASE 6			RETURN 23.5990
			CASE 7			RETURN 66.3990
			CASE 8			RETURN 109.5980
			CASE 9			RETURN 85.1990
			CASE 10			RETURN 91.9980
			CASE 11			RETURN 82.7980
			CASE 12			RETURN 94.5980
			CASE 13			RETURN 41.1980
			CASE 14			RETURN 104.5970
			CASE 15			RETURN 15.9970
		ENDSWITCH
	
		RETURN 68.0
	ENDIF

	IF CONTENT_IS_USING_ARENA()
	
		SWITCH iPed
			CASE 0			RETURN 203.6000
			CASE 1			RETURN 214.6000
			CASE 2			RETURN 256.8000
			CASE 3			RETURN 247.1997
			CASE 4			RETURN 188.6000
			CASE 5			RETURN 229.1990
			CASE 6			RETURN 203.7990
			CASE 7			RETURN 212.5990
			CASE 8			RETURN 247.9990
			CASE 9			RETURN 247.9990
			CASE 10			RETURN 189.1990
			CASE 11			RETURN 213.5990
			CASE 12			RETURN 242.3990
			CASE 13			RETURN 269.5990
			CASE 14			RETURN 218.1990
			CASE 15			RETURN 266.6238
		ENDSWITCH
	
		RETURN 306.5976
	ENDIF

	RETURN 0.0
ENDFUNC

FUNC VECTOR CELEBRATION_CAM_POS()

	IF CELEBRATION_IS_ISLAND_MODE()
		RETURN <<4519.4204, -4486.8188, 4.1106>>
	ENDIF

	IF CONTENT_IS_USING_ARENA()
		RETURN <<2968.0618, -3879.5862, 141.3566>>
	ENDIF

	RETURN <<416.0675, -981.3500, -99.0175>>
ENDFUNC

FUNC VECTOR CELEBRATION_CAM_ROT()

	IF CELEBRATION_IS_ISLAND_MODE()
		RETURN <<0.1831, 0.0223, -99.9511>>
	ENDIF

	IF CONTENT_IS_USING_ARENA()
		RETURN <<6.1166, -0.1429, 44.3658>>
	ENDIF

	RETURN <<2.5875, -0.2000, 21.8200>>
ENDFUNC

FUNC FLOAT CELEBRATION_CAM_FOV()

	IF CELEBRATION_IS_ISLAND_MODE()
		RETURN 45.0
	ENDIF

	IF CONTENT_IS_USING_ARENA()
		RETURN 51.4505
	ENDIF

	RETURN 39.0000
ENDFUNC

FUNC VECTOR ARENA_WARP_VEH_POS(MODEL_NAMES eVehicleModel)

	SWITCH eVehicleModel 
		CASE CERBERUS
		CASE CERBERUS2
		CASE CERBERUS3
			RETURN <<2823.8596, -3909.3079, 139.0008>>
	ENDSWITCH

	IF IS_THIS_MODEL_A_BICYCLE(eVehicleModel)
		RETURN <<2824.3691, -3904.2183, 139.4829>>
	ENDIF

	RETURN <<2824.3691, -3904.2183, 139.0015>>
ENDFUNC

FUNC FLOAT ARENA_WARP_VEH_ROTATION(MODEL_NAMES eVehicleModel)

	SWITCH eVehicleModel 
		CASE CERBERUS
		CASE CERBERUS2
		CASE CERBERUS3
			RETURN 9.8

	ENDSWITCH

	RETURN 323.6979
ENDFUNC

FUNC VECTOR ISLAND_WARP_VEH_POS()
	RETURN <<4525.934, -4487.330, 3.740>>
ENDFUNC

FUNC FLOAT ISLAND_WARP_VEH_ROTATION()
	RETURN 52.07
ENDFUNC

FUNC BOOL CELEBRATION_IS_WINNER_INTERIOR_PINNED(CELEBRATION_SCREEN_DATA &sCelebrationData)

	IF NOT CELEBRATION_WINNER_USES_INTERIOR()
		RETURN TRUE
	ENDIF

	IF NOT IS_VALID_INTERIOR(sCelebrationData.winnerSceneInterior)
		
		STRING sInterior = INTERIOR_NAME()
		VECTOR vInterior = INTERIOR_POS()
		
		sCelebrationData.winnerSceneInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInterior, sInterior) 
		
		PRINTLN("[NETCELEBRATION] - CELEBRATION_IS_WINNER_INTERIOR_PINNED - sInterior = ", sInterior)
		PRINTLN("[NETCELEBRATION] - CELEBRATION_IS_WINNER_INTERIOR_PINNED - vInterior = ", vInterior)
	ELSE
		PRINTLN("[NETCELEBRATION] - CELEBRATION_IS_WINNER_INTERIOR_PINNED - IS_VALID_INTERIOR ")
	
		IF IS_INTERIOR_DISABLED(sCelebrationData.winnerSceneInterior) 
			DISABLE_INTERIOR(sCelebrationData.winnerSceneInterior, FALSE)
		ENDIF
		
		PIN_INTERIOR_IN_MEMORY(sCelebrationData.winnerSceneInterior)
	ENDIF

	RETURN IS_INTERIOR_READY(sCelebrationData.winnerSceneInterior)
ENDFUNC

FUNC BOOL LOAD_WINNER_SCENE_INTERIOR(CELEBRATION_SCREEN_DATA &sCelebrationData)

	#IF IS_DEBUG_BUILD
	VECTOR vDebug
	#ENDIF
		
	IF CELEBRATION_IS_WINNER_INTERIOR_PINNED(sCelebrationData)
		PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - IS_INTERIOR_READY sCelebrationData.iLoadSceneStage: ", sCelebrationData.iLoadSceneStage)
		SWITCH sCelebrationData.iLoadSceneStage
			
			CASE 0
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
					PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - called NEW_LOAD_SCENE_STOP().")
				ENDIF
				
				IF IS_PLAYER_SCTV(PLAYER_ID())
					SET_SPECTATOR_OVERRIDE_COORDS(CELEBRATION_GET_PED_OFFSCREEN_POS()) // Inside interior but out of shot.
				ENDIF
				
				PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - going to stage 1.")
				sCelebrationData.iLoadSceneStage++
				
			BREAK
			
			CASE 1
			
				PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - Case 1")
				
				IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
				OR (IS_SCREEN_FADED_OUT() AND sCelebrationData.iRenderphasePauseAttempts >= 10)
				OR sCelebrationData.iRenderphasePauseAttempts >= 200
					PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - GET_TOGGLE_PAUSED_RENDERPHASES_STATUS = FALSE")
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), CELEBRATION_GET_PED_OFFSCREEN_POS()) // Inside interior but out of shot.
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
						#IF IS_DEBUG_BUILD
						vDebug = CELEBRATION_GET_PED_OFFSCREEN_POS()
						PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - put player ped at coords:  ", vDebug)
						PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - frozen player pos, set invisible and turne doff collison.")
						PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - going to stage 2.")		
						#ENDIF
						sCelebrationData.iLoadSceneStage++
					ENDIF
				ELSE
					PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - GET_TOGGLE_PAUSED_RENDERPHASES_STATUS = TRUE - Setting to pause them.")
					TOGGLE_PAUSED_RENDERPHASES(FALSE)
					
					sCelebrationData.iRenderphasePauseAttempts++
					
					IF sCelebrationData.iRenderphasePauseAttempts >= 10
						PRINTLN("[LM][url:bugstar:4372431 - LOAD_WINNER_SCENE_INTERIOR - Emergancy Screen Fade In. Celebration Screen should start.")
						IF NOT IS_SCREEN_FADING_OUT()
							DO_SCREEN_FADE_OUT(250)
						ENDIF
						TOGGLE_PAUSED_RENDERPHASES(TRUE) // Resetting the dirty Data.
					ENDIF
					
					// Old way causes this on occasion: url:bugstar:4496831 - Fatal Error when playing UCG Capture-  Fatal Error: Building Pool Full, Size == 55000 (you need to raise Building PoolSize in common/data/gameconfig.xml) 
					//PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - going to stage 2.")
					//sCelebrationData.iLoadSceneStage++
				ENDIF
				
			BREAK
			
			CASE 2					
				IF IS_PLAYER_SWITCH_IN_PROGRESS()
					RETURN TRUE
				ENDIF
				
				NEWLOADSCENE_FLAGS LoadSceneFlags
				IF CELEBRATION_WINNER_USES_INTERIOR()
					LoadSceneFlags = NEWLOADSCENE_FLAG_INTERIOR_AND_EXTERIOR
				ENDIF				
				
				IF NEW_LOAD_SCENE_START_SPHERE(INTERIOR_POS(), 40.0, LoadSceneFlags)
					#IF IS_DEBUG_BUILD
					vDebug = INTERIOR_POS()
					PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - going to stage 3. NEW_LOAD_SCENE_START_SPHERE(40.0, NEWLOADSCENE_FLAG_INTERIOR_AND_EXTERIOR) = TRUE.", vDebug)
					#ENDIF
					sCelebrationData.iLoadSceneStage++
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - NEW_LOAD_SCENE_START_SPHERE(40.0, NEWLOADSCENE_FLAG_INTERIOR_AND_EXTERIOR) = FALSE.", vDebug)
				#ENDIF
				ENDIF
			
			BREAK
			
			CASE 3
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - IS_NEW_LOAD_SCENE_ACTIVE = TRUE.")
					IF IS_NEW_LOAD_SCENE_LOADED()
						PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - IS_NEW_LOAD_SCENE_LOADED() = TRUE, going to stage 4.")
						sCelebrationData.iLoadSceneStage++
					ELSE
						PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - IS_NEW_LOAD_SCENE_LOADED = FALSE.")
					ENDIF
				ELSE
					PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - IS_NEW_LOAD_SCENE_ACTIVE() = FALSE, going to stage 4.")
					sCelebrationData.iLoadSceneStage++
				ENDIF
				
			BREAK
			
			CASE 4
				
				// Wait a frame for bug 2096299.
				PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - waited a frame after load scene has said it is loaded, going to stage 5. Called NEW_LOAD_SCENE_STOP()")
				NEW_LOAD_SCENE_STOP()
				sCelebrationData.iLoadSceneStage++
				
			BREAK
			
			CASE 5
			
				RETURN TRUE
				
			BREAK
			
		ENDSWITCH

	ENDIF
	
	// url:bugstar:5524780 - Arena - Bomb Ball II - All players become stuck on skycam with no loading spinner after remote player uses Join Game to join the playlist as the final mode ends
	// If we have failed to get an interior by now then bomb out
	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
	AND CELEBRATION_WINNER_USES_INTERIOR()
		IF NOT IS_VALID_INTERIOR(sCelebrationData.winnerSceneInterior)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC GET_WINNER_VEHICLE_CREATION_DATA(MODEL_NAMES eVehicleModel, VECTOR &vCoords, FLOAT &fRotation, BOOL bDoingRallyRace = FALSE)

	IF CONTENT_IS_USING_ARENA()
		vCoords = ARENA_WARP_VEH_POS(eVehicleModel)
		fRotation = ARENA_WARP_VEH_ROTATION(eVehicleModel)
		EXIT
	ENDIF
	
	IF CELEBRATION_IS_ISLAND_MODE()
		vCoords = ISLAND_WARP_VEH_POS()
		fRotation = ISLAND_WARP_VEH_ROTATION()
		EXIT
	ENDIF
	
	IF (eVehicleModel = MONSTER)
		vCoords = <<413.179, -974.422, -100.004>>
		fRotation = 158.368
		EXIT
	ELIF IS_THIS_MODEL_A_BICYCLE(eVehicleModel)
		IF bDoingRallyRace
			vCoords = <<413.2641, -975.4441, -100.0042>>
			fRotation = 247.8625
		ELSE
			vCoords = <<415.1816, -976.0641, -100.0042>>
			fRotation = 139.7340
		ENDIF
		EXIT
	ELIF IS_THIS_MODEL_A_BIKE(eVehicleModel)
		IF bDoingRallyRace
			vCoords = <<413.2641, -975.4441, -100.0042>>
			fRotation = 247.8625
		ELSE
			vCoords = <<414.7016, -975.6291, -100.0042>>
			fRotation = 237.3050
		ENDIF
		EXIT
	ELSE
		vCoords = <<413.8590, -974.6100, -100.0042>>
		fRotation = 149.8085
		EXIT
	ENDIF
	
ENDPROC

PROC CELEBRATION_GET_PED_CREATION_DATA(CELEB_SERVER_DATA &sCelebServer, INT iPed, VECTOR &vCoords, VECTOR &vRotation, INT iPosition)
	SWITCH sCelebServer.aArenaAnimType
		CASE CELEBRATION_ARENA_ANIM_PODIUM
		
			vCoords = <<2956.9841, -3866.8152, 140.00>>
			
			// url:bugstar:5519010 allow for different height model
			SWITCH g_FMMC_STRUCT.sArenaInfo.iArena_Theme
				CASE ARENA_THEME_CONSUMERWASTELAND	
					vCoords.z = vCoords.z + 0.05
				BREAK
			ENDSWITCH
	
			vRotation = << 0.000, -0.000, 48.00 >>
			
		BREAK
		CASE CELEBRATION_ARENA_ANIM_FLAT
		
			IF iPosition = POS_1ST
			OR ( IS_ARENA_ANIM_PAIRED(sCelebServer.eArenaClips[iPosition]) AND iPosition = POS_2ND ) // We need to give paired peds their partners coords so that the anims are in synch
				vCoords = <<2966.7688, -3874.8408, 140.0011>>
				vRotation = << 0.000, -0.000, 48.00 >>
			ELSE
				// Spread the players out
				vCoords = CELEBRATION_GET_PED_WARP_POS(sCelebServer, iPed)
				vCoords.z -= 1.0
				vRotation = << 0.000, -0.000, CELEBRATION_GET_PED_WARP_ROT(iPed) >>
			ENDIF

		BREAK
		DEFAULT
			vCoords = CELEBRATION_GET_PED_WARP_POS(sCelebServer, iPed)
			vCoords.z -= 1.0
			vRotation = << 0.0, 0.0, CELEBRATION_GET_PED_WARP_ROT(iPed) >>
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_WINNER_PED_CREATION_DATA(CELEB_SERVER_DATA &sCelebServer, INT iPed, VECTOR &vCoords, VECTOR &vRotation, BOOL bForCarScene = FALSE, BOOL bForAi = FALSE, BOOL bForPaired = FALSE)
	
	IF CONTENT_IS_USING_ARENA()
	OR CELEBRATION_IS_ISLAND_MODE()
	
		CELEBRATION_GET_PED_CREATION_DATA(sCelebServer, iPed, vCoords, vRotation, iPed)
	
//		vCoords = CELEBRATION_GET_PED_WARP_POS_TEMP(iPed)
//		vCoords.z -= 1.0
//		vRotation = << 0.0, 0.0, CELEBRATION_GET_PED_WARP_ROT(iPed) >>
		EXIT
	ENDIF
	
	IF bForAi
		vCoords = <<414.4000, -977.6000, -100.0042>>
		vCoords.z -= 1.0
		vRotation = << 0.0, 0.0, 200.0000 >>
		EXIT
	ELIF bForPaired
		vCoords = <<414.6500, -978.4600, -99.0040>>
		vCoords.z -= 1.0
		vRotation = << 0.0, 0.0, 200.0000 >>
		EXIT
	ENDIF
	
	SWITCH iPed
		CASE 0
			vCoords = <<414.4000, -977.6000, -100.0042>>
			vRotation = << 0.0, 0.0, 200.0000 >>
		BREAK
		CASE 1
			IF bForCarScene
				vCoords = <<412.5202, -977.6000, -100.0042>>
				vRotation = << 0.0, 0.0, 225.3750 >>
			ELSE
				vCoords = <<413.0202, -976.6559, -100.0042>>
				vRotation = << 0.0, 0.0, 225.3750 >>
			ENDIF
		BREAK
		CASE 2
			vCoords = <<414.9879, -975.4411, -100.0042>>
			vRotation = << 0.0, 0.0, 183.6250 >>
		BREAK
		CASE 3
			vCoords = <<416.2123, -975.5722, -100.0042>>
			vRotation = << 0.0, 0.0, 155.3000 >>
		BREAK
		CASE 4
			vCoords = <<410.7824, -976.5933, -100.0042>>
			vRotation = << 0.0, 0.0, 240.9000 >>
		BREAK
		CASE 5
			vCoords = <<411.1806, -975.8190, -100.0042>>
			vRotation = << 0.0, 0.0, 227.5000 >>
		BREAK
		CASE 6
			vCoords = <<414.0178, -974.6259, -100.0042>>
			vRotation = << 0.0, 0.0, 206.6500 >>
		BREAK
		CASE 7
			vCoords = <<415.7338, -972.9619, -100.0042>>
			vRotation = << 0.0, 0.0, 170.7750 >>
		BREAK
		CASE 8
			vCoords = <<412.1275, -974.0275, -100.0042>>
			vRotation = << 0.0, 0.0, 200.0000 >>
		BREAK
		CASE 9
			vCoords = <<417.3252, -970.7984, -100.0042>>
			vRotation = << 0.0, 0.0, 160.0250 >>
		BREAK
		CASE 10
			vCoords = <<409.8050, -975.0361, -100.0042>>
			vRotation = << 0.0, 0.0, 240.6500 >>
		BREAK
		CASE 11
			vCoords = <<410.6598, -974.3722, -100.0042>>
			vRotation = << 0.0, 0.0, 222.1500 >>
		BREAK
		CASE 12
			vCoords = <<407.5374, -974.3933, -100.0042>>
			vRotation = << 0.0, 0.0, 258.1750 >>
		BREAK
		CASE 13
			vCoords = <<413.1881, -969.0190, -100.0042>>
			vRotation = << 0.0, 0.0, 204.0250 >>
		BREAK
		CASE 14
			vCoords = <<415.0103, -969.1959, -100.0042>>
			vRotation = << 0.0, 0.0, 182.3500 >>
		BREAK
	ENDSWITCH
	
	vCoords.z -= 1.0
	
ENDPROC

FUNC BOOL HAVE_ARENA_PROPS_LOADED(CELEB_SERVER_DATA &sCelebServer)
	
	INT i
	MODEL_NAMES model[MAX_ARENA_PROPS]
	
	IF sCelebServer.iNumPropsRequired > 0
		REPEAT sCelebServer.iNumPropsRequired i //MAX_ARENA_PROPS i
			
			model[i] = ARENA_PROP_MODEL(sCelebServer, i)
			
			REQUEST_MODEL(model[i])
			IF NOT HAS_MODEL_LOADED(model[i])
				RETURN FALSE
			ENDIF
			
		ENDREPEAT
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CREATE_ARENA_PROP(CELEB_SERVER_DATA &sCelebServer, ENTITY_INDEX &entity, VECTOR vCreationCoords, MODEL_NAMES modelToMake, FLOAT fPropHeading)
	
	#IF IS_DEBUG_BUILD
	INT iTemp
	#ENDIF
	
	IF DOES_ENTITY_EXIST(entity)
		RETURN TRUE
	ENDIF
	
	IF CAN_REGISTER_MISSION_OBJECTS(1)
		RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+1)
		#IF IS_DEBUG_BUILD
		iTemp = GET_NUM_RESERVED_MISSION_OBJECTS()
		PRINTLN("[NETCELEBRATION] - [Paired_Anims] - CREATE_ARENA_PROP - numreserved objects after call to RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+1) = ", iTemp)
		#ENDIF
		IF HAVE_ARENA_PROPS_LOADED(sCelebServer)
			entity = CREATE_OBJECT(modelToMake, vCreationCoords, FALSE, FALSE)
			SET_ENTITY_HEADING(entity, fPropHeading)

			PRINTLN("[NETCELEBRATION] - [Paired_Anims] - CREATE_ARENA_PROP - created prop at vCreationCoords = ", vCreationCoords, " fPropHeading = ", fPropHeading)
		ENDIF
	ELSE
		PRINTLN("[NETCELEBRATION] - [Paired_Anims] - CREATE_ARENA_PROP - CAN_REGISTER_MISSION_OBJECTS(1) = FALSE, cannot create prop.")
		RETURN TRUE
	ENDIF
	
	RETURN DOES_ENTITY_EXIST(entity)
	
ENDFUNC

PROC ATTACH_ARENA_PROP(ARENA_CELEB_ANIM_CLIPS eClip, PED_INDEX ped, ENTITY_INDEX prop)

	IF DOES_ENTITY_EXIST(ped)
	AND DOES_ENTITY_EXIST(prop)
		ATTACH_ENTITY_TO_ENTITY(prop, ped, GET_PED_BONE_INDEX(ped, BONE_FOR_PROP(eClip)), <<0, 0, 0>>, <<0, 0, 0>>) 
		PRINTLN("[NETCELEBRATION] - [Paired_Anims] - ATTACH_ARENA_PROP - done.")
	ENDIF

ENDPROC

FUNC BOOL HAVE_REQUIRED_PROPS_LOADED(INT iAnimToPlay, BOOL bdoingRallyRace)
	
	MODEL_NAMES model
	
	IF NOT bdoingRallyRace	
		
		IF (iAnimToPlay = ENUM_TO_INT(PLAYER_INTERACTION_SMOKE))
			model = Prop_CS_Ciggy_01
		ELSE
			RETURN TRUE
		ENDIF
		
		REQUEST_MODEL(model)
		IF NOT HAS_MODEL_LOADED(model)
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CREATE_CELEBRATION_PROP(ENTITY_INDEX &entity, INT iAnimToPlay, BOOL bdoingRallyRace, VECTOR vCreationCoords)
	
	#IF IS_DEBUG_BUILD
	INT iTemp
	#ENDIF
	
	MODEL_NAMES model
	FLOAT fPropHeading = -1.0
	
	IF bdoingRallyRace
		RETURN TRUE
	ENDIF
	
	IF (iAnimToPlay = ENUM_TO_INT(PLAYER_INTERACTION_SMOKE))
		model = Prop_CS_Ciggy_01	
	ELSE
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(entity)
		RETURN TRUE
	ENDIF
	
	IF CAN_REGISTER_MISSION_OBJECTS(1)
		RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+1)
		#IF IS_DEBUG_BUILD
		iTemp = GET_NUM_RESERVED_MISSION_OBJECTS()
		PRINTLN("[NETCELEBRATION] - [Paired_Anims] - CREATE_CELEBRATION_PROP - numreserved objects after call to RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+1) = ", iTemp)
		#ENDIF
		IF HAVE_REQUIRED_PROPS_LOADED(iAnimToPlay, bdoingRallyRace)
			entity = CREATE_OBJECT(model, vCreationCoords, FALSE, FALSE)
			IF fPropHeading != -1.0
				SET_ENTITY_HEADING(entity, fPropHeading)
			ENDIF
			PRINTLN("[NETCELEBRATION] - [Paired_Anims] - CREATE_CELEBRATION_PROP - created prop at vCreationCoords = ", vCreationCoords, " fPropHeading = ", fPropHeading)
		ENDIF
	ELSE
		PRINTLN("[NETCELEBRATION] - [Paired_Anims] - CREATE_CELEBRATION_PROP - CAN_REGISTER_MISSION_OBJECTS(1) = FALSE, cannot create prop.")
		RETURN TRUE
	ENDIF
	
	RETURN DOES_ENTITY_EXIST(entity)
	
ENDFUNC

/// PURPOSE:
///    New version of winner screen: instead of using the UI3D commands to render the ped it uses a background object as a rendertarget.
/// PARAMS:
///    sCelebrationData - The celebration data struct
///    piWinners - The players we want to show on the screen (8 max)
///    pedAIWinner - Currently just for Lamar tutorial: use this if an AI ped won the job instead of a player.
///    bUseLoserAnims - If TRUE the ped will be set to look like they lost (use for "LOSER" screens).
///    vehWinner - Race celebrations feature the car, pass the winner's vehicle in to force the screen to setup this scenario.
/// RETURNS:
///    TRUE when the scene is ready.
FUNC BOOL CREATE_WINNER_SCENE_ENTITIES(CELEB_SERVER_DATA &sCelebServer, CELEBRATION_SCREEN_DATA &sCelebrationData, PLAYER_INDEX &piWinners[], PED_INDEX pedAIWinner = NULL, BOOL bUseLoserAnims = FALSE, VEHICLE_INDEX vehWinner = NULL, BOOL bLandRaceWinnerScene = FALSE, BOOL bIsFinalTournamentPlaylist = FALSE, BOOL bGrabbedVehicle = FALSE, BOOL bDoingRallyRace = FALSE, MODEL_NAMES eAiVehModel = DUMMY_MODEL_FOR_SCRIPT, INT iAnimToPlay = -1, BOOL bForJuggernaut = FALSE)
	
	BOOL bReturnValue = TRUE
	INT i
	BOOL bPlinthLoaded
	VECTOR vPedPos
	VECTOR vPedRot
	BOOL bVehicleLoaded
	TEXT_LABEL_63 tl63IdleDict
	TEXT_LABEL_31 tl31Anim
	FLOAT fRandomPhase
	
	BOOL bLoadedCoronaAnims
	
	#IF IS_DEBUG_BUILD
	INT iTempPlayer
	#ENDIF
	
	IF bUseLoserAnims = bUseLoserAnims
		bUseLoserAnims = bUseLoserAnims
	ENDIF
	
	IF bUseLoserAnims = bUseLoserAnims
		bUseLoserAnims = bUseLoserAnims
	ENDIF
	
	STRING sFacial
//	STRING sFacialDict = NULL
	
	TEXT_LABEL_63 tl63_AnimDict
	TEXT_LABEL_63 tl63_AnimName
	
	TEXT_LABEL_63 tl63_AnimDictCam
	
	BOOL bArena
	IF CONTENT_IS_USING_ARENA()
		bLandRaceWinnerScene = FALSE // url:bugstar:5505940 - Remove winner vehicle from celebration screens
		bArena = TRUE
		GET_ARENA_CELEB_ANIMS(sCelebServer, tl63_AnimDict, tl63_AnimName, 0)
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63_AnimDict)
			REQUEST_ANIM_DICT(tl63_AnimDict)
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("CREATE_WINNER_SCENE_ENTITIES, tl63_AnimDict is empty at ", NATIVE_TO_INT(GET_NETWORK_TIME()))		
		#ENDIF
		ENDIF
		
		tl63_AnimDictCam = ARENA_CAM_ANIM_DICT(sCelebServer)
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63_AnimDictCam)
			REQUEST_ANIM_DICT(tl63_AnimDictCam)
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("CREATE_WINNER_SCENE_ENTITIES, tl63_AnimDictCam is empty at ", NATIVE_TO_INT(GET_NETWORK_TIME()))		
		#ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sCelebrationData.objWinnerBackground)
		
		IF bLandRaceWinnerScene
			IF (pedAIWinner = NULL)
				IF bGrabbedVehicle
					IF sCelebrationData.vehicleSetupMp.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
						IF REQUEST_LOAD_MODEL(sCelebrationData.vehicleSetupMp.VehicleSetup.eModel)
							bVehicleLoaded = TRUE
						ENDIF
					ELSE
//						SCRIPT_ASSERT("[NETCELEBRATION] - save race winner car - trying to load vehicle for landwinnerscene but sCelebrationData.vehicleSetupMp.VehicleSetup.eModel) = DUMMY_MODEL_FOR_SCRIPT.")
						PRINTLN("[NETCELEBRATION] - save race winner car - trying to load vehicle for landwinnerscene but sCelebrationData.vehicleSetupMp.VehicleSetup.eModel) = DUMMY_MODEL_FOR_SCRIPT.")
						bVehicleLoaded = TRUE
					ENDIF
				ELSE
					PRINTLN("[NETCELEBRATION] - save race winner car - bGrabbedVehicle = FALSE, skipping veh creation.")
					bVehicleLoaded = TRUE
				ENDIF
			ELSE
				IF (eAiVehModel != DUMMY_MODEL_FOR_SCRIPT)
					sCelebrationData.vehicleSetupMp.VehicleSetup.eModel = eAiVehModel
					IF REQUEST_LOAD_MODEL(sCelebrationData.vehicleSetupMp.VehicleSetup.eModel)
						bVehicleLoaded = TRUE
					ENDIF
				ELSE
					PRINTLN("[NETCELEBRATION] - save race winner car - eAiVehModel = DUMMY_MODEL_FOR_SCRIPT, skipping veh creation.")
					bVehicleLoaded = TRUE
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[NETCELEBRATION] - save race winner car - bLandRaceWinnerScene = FALSE, skipping veh creation.")
			bVehicleLoaded = TRUE
		ENDIF
		
		IF bIsFinalTournamentPlaylist
			//REQUEST_MODEL(HEI_PROP_HEIST_PLINTH)
			bPlinthLoaded  = TRUE //= HAS_MODEL_LOADED(HEI_PROP_HEIST_PLINTH)
		ELSE
			bPlinthLoaded = TRUE
		ENDIF
		
		bLoadedCoronaAnims = HAVE_CORONA_IDLE_ANIMS_LOADED()
		
		IF bLoadedCoronaAnims
		AND bPlinthLoaded
		AND bVehicleLoaded
			
			//Create the peds: peds ordered first in the array will be closest to the camera.
			IF pedAIWinner != NULL
			
				GET_WINNER_PED_CREATION_DATA(sCelebServer, 0, vPedPos, vPedRot, bLandRaceWinnerScene, TRUE, bDoingRallyRace)
				CREATE_CLONE_OF_PED_FOR_WINNER_SCENE(sCelebrationData.pedWinnerClones[0], pedAIWinner, vPedPos, vPedRot.z, -1, TRUE, TRUE)
				PRINTLN("[NETCELEBRATION] Creating clone of AI winner.")
				
			ELSE
				
				INT iNumPedsSuccessfullyCreated = 0
//				VECTOR vOffsetFromWall
				BOOL bWearingProblemMaskOrHat
				
				INT ii 
				REPEAT COUNT_OF(piWinners) ii
					IF piWinners[ii] = INVALID_PLAYER_INDEX()
						PRINTLN("[WINNERS][RH]3 piPlayersTest[",ii,"] = INVALID")
					ELSE
						PRINTLN("[WINNERS][RH]3 piPlayersTest[",ii,"] = ",  GET_PLAYER_NAME(piWinners[ii]))
					ENDIF
				ENDREPEAT
				
				REPEAT COUNT_OF(piWinners) i
					
					PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - piWinners[", i, "].")
					
					IF piWinners[i] != INVALID_PLAYER_INDEX()
						
						#IF IS_DEBUG_BUILD
						iTempPlayer = NATIVE_TO_INT(piWinners[i])
						PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - piWinners[", i, "] != INVALID_PLAYER_INDEX(). Id = ", iTempPlayer)
						#ENDIF
						
						IF IS_NET_PLAYER_OK(piWinners[i], FALSE) //We can clone dead players, but not ones that have left the session.
							
							#IF IS_DEBUG_BUILD
							PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - player ", iTempPlayer, " = IS_NET_PLAYER_OK.")
							#ENDIF
							
							sCelebrationData.tl31_pedWinnerClonesNames[i] = GET_PLAYER_NAME(piWinners[i])
							sCelebrationData.iPedWinnerClonePlayerId[i] = NATIVE_TO_INT(piWinners[i])
							
							#IF IS_DEBUG_BUILD
							PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - player ", iTempPlayer, " name = ", sCelebrationData.tl31_pedWinnerClonesNames[i])
							#ENDIF
							
							IF (vehWinner != NULL AND i < 2) //If we're putting a vehicle in the scene then only use the first two winner peds.
							OR vehWinner = NULL
							
								PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - (vehWinner != NULL AND i < 2) OR vehWinner = NULL ")
								
								PED_INDEX pedTemp = GET_PLAYER_PED(piWinners[i])
//								sCelebrationData.strFakeTagName[i] = GET_PLAYER_NAME(piWinners[i]) // Commented out for B* 1771409 - uncomment to add back in.
								
								IF DOES_ENTITY_EXIST(pedTemp)
									
									#IF IS_DEBUG_BUILD
									PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - player ", iTempPlayer, " ped exists.")
									#ENDIF

									bWearingProblemMaskOrHat = FALSE
									
									IF NOT bForJuggernaut
										IF i = 0
											bWearingProblemMaskOrHat = IS_PED_WEARING_PROBLEM_MASK_OR_HAT_FOR_INTERACTION(	pedTemp, 
																															GlobalplayerBD[NATIVE_TO_INT(piWinners[i])].iInteractionType, 
																															GlobalplayerBD[NATIVE_TO_INT(piWinners[i])].iInteractionAnim,
																															TRUE, 
																															FALSE)
										ENDIF
									ENDIF
									
									GET_WINNER_PED_CREATION_DATA(sCelebServer, i, vPedPos, vPedRot, bLandRaceWinnerScene, FALSE, bDoingRallyRace)
									CREATE_CLONE_OF_PED_FOR_WINNER_SCENE(sCelebrationData.pedWinnerClones[iNumPedsSuccessfullyCreated], pedTemp, vPedPos, 0.0, NATIVE_TO_INT(piWinners[i]), bWearingProblemMaskOrHat, i = 0)
									
									IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClones[i])
										IF NOT IS_ENTITY_DEAD(sCelebrationData.pedWinnerClones[i])
											SET_ENTITY_ROTATION(sCelebrationData.pedWinnerClones[i], vPedRot)
											sFacial = GET_PLAYER_MOOD_ANIM_FROM_INDEX(GlobalplayerBD_FM[NATIVE_TO_INT(piWinners[i])].iPlayerMood)
											
											IF NOT bArena		
												SET_FACIAL_IDLE_ANIM_OVERRIDE(sCelebrationData.pedWinnerClones[i], sFacial)
//												PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES, SET_FACIAL_IDLE_ANIM_OVERRIDE for i = ", i, " sFacial = ", sFacial, " sFacialDict = ", sFacialDict)
											ENDIF
											
										ENDIF
									ENDIF
									
									PRINTLN("[NETCELEBRATION] Created clone of player: ", iTempPlayer, " in slot ", iNumPedsSuccessfullyCreated)
									
									iNumPedsSuccessfullyCreated++
									
								ENDIF
									
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[NETCELEBRATION][RH] piWinners[",i,"] was invalid")
					ENDIF
				ENDREPEAT
				
				//If none of the winner IDs were valid then use the current player as a fallback.
				IF iNumPedsSuccessfullyCreated = 0
					PRINTLN("[NETCELEBRATION] winning player IDs were invalid, creating fallback.")
					
					bWearingProblemMaskOrHat = IS_PED_WEARING_PROBLEM_MASK_OR_HAT_FOR_INTERACTION(	sCelebrationData.pedWinnerClones[0], 
																									GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iInteractionType, 
																									GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iInteractionAnim,
																									TRUE, 
																									FALSE)																							
					
																																							
					GET_WINNER_PED_CREATION_DATA(sCelebServer, 0, vPedPos, vPedRot, bLandRaceWinnerScene, FALSE, bDoingRallyRace)					
					CREATE_CLONE_OF_PED_FOR_WINNER_SCENE(sCelebrationData.pedWinnerClones[0], PLAYER_PED_ID(), vPedPos, vPedRot.z, NATIVE_TO_INT(PLAYER_ID()), bWearingProblemMaskOrHat, TRUE)
					
					IF NOT IS_PED_INJURED(sCelebrationData.pedWinnerClones[0])
					AND NOT bArena
						SET_FACIAL_IDLE_ANIM_OVERRIDE(sCelebrationData.pedWinnerClones[0], GET_PLAYER_MOOD_ANIM_FROM_INDEX(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPlayerMood))
						PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES 2, SET_FACIAL_IDLE_ANIM_OVERRIDE for 0 ")
					ENDIF
				ENDIF
			ENDIF
			
			IF bLandRaceWinnerScene
				IF bGrabbedVehicle OR (pedAIWinner != NULL)
					IF sCelebrationData.vehicleSetupMp.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
					AND sCelebrationData.vehicleSetupMp.VehicleSetup.eModel != RALLYTRUCK
						
						IF NOT DOES_ENTITY_EXIST(sCelebrationData.vehWinnerClone)
						
							// Declare variables.
							VECTOR vVehiclePos, vVehRot
							
							GET_WINNER_VEHICLE_CREATION_DATA(sCelebrationData.vehicleSetupMp.VehicleSetup.eModel, vVehiclePos, vVehRot.z, bDoingRallyRace)
							
							// Create the car where the ped is and freeze it in place, so it doesn't fall out of the sky.
							sCelebrationData.vehWinnerClone = CREATE_VEHICLE(sCelebrationData.vehicleSetupMp.VehicleSetup.eModel, vVehiclePos, vVehRot.z, FALSE, FALSE)
							SET_VEHICLE_SETUP_MP(sCelebrationData.vehWinnerClone, sCelebrationData.vehicleSetupMp)
							
							PRINTLN("[NETCELEBRATION] - bLandRaceWinnerScene - vVehiclePos = ", vVehiclePos)
							PRINTLN("[NETCELEBRATION] - bLandRaceWinnerScene - vVehRot = ", vVehRot.z) 
							
							// Set the vehicle position and rotation for the scene.
							IF NOT IS_ENTITY_DEAD(sCelebrationData.vehWinnerClone)
								SET_ENTITY_COORDS(sCelebrationData.vehWinnerClone, vVehiclePos)
								SET_ENTITY_ROTATION(sCelebrationData.vehWinnerClone, vVehRot)
								
								SET_ENTITY_DYNAMIC(sCelebrationData.vehWinnerClone, FALSE)
								SET_ENTITY_COLLISION(sCelebrationData.vehWinnerClone, FALSE)
								FREEZE_ENTITY_POSITION(sCelebrationData.vehWinnerClone, TRUE)
							ENDIF
							
						ENDIF
					
					ELSE
						IF sCelebrationData.vehicleSetupMp.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
							PRINTLN("[NETCELEBRATION] - sCelebrationData.vehicleSetupMp.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT")
						ENDIF
						IF sCelebrationData.vehicleSetupMp.VehicleSetup.eModel = RALLYTRUCK
							PRINTLN("[NETCELEBRATION] - sCelebrationData.vehicleSetupMp.VehicleSetup.eModel = RALLYTRUCK")
						ENDIf
					ENDIF
				ELSE
					PRINTLN("[NETCELEBRATION] - bGrabbedVehicle = ", bGrabbedVehicle)
				ENDIF
			ELSE
				PRINTLN("[NETCELEBRATION] - bLandRaceWinnerScene = ", bLandRaceWinnerScene)
			ENDIF		
				
			//Set the initial behaviour of all the peds.
			INT iNumClones = COUNT_OF(sCelebrationData.pedWinnerClones)
			
			REPEAT iNumClones i
				IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClones[i])
					IF NOT IS_PED_INJURED(sCelebrationData.pedWinnerClones[i])
						IF NOT IS_BIT_SET(sCelebrationData.iAnimationPlayingBitset, i)
							FREEZE_ENTITY_POSITION(sCelebrationData.pedWinnerClones[i], TRUE)
							SET_PED_CAN_PLAY_AMBIENT_ANIMS(sCelebrationData.pedWinnerClones[i], FALSE)
							SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(sCelebrationData.pedWinnerClones[i], FALSE)
							
							GET_GENERIC_PED_ANIM_BASE(tl63IdleDict, tl31Anim, fRandomPhase)
							TASK_PLAY_ANIM(sCelebrationData.pedWinnerClones[i], tl63IdleDict, tl31Anim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_IGNORE_GRAVITY | AF_NOT_INTERRUPTABLE, fRandomPhase)
							SET_BIT(sCelebrationData.iAnimationPlayingBitset, i)
							
							PRINTLN("[NETCELEBRATION] - called TASK_PLAY_ANIM on clone ", i," with the following data:")
							PRINTLN("[NETCELEBRATION] - tl63IdleDict = ", tl63IdleDict)
							PRINTLN("[NETCELEBRATION] - tl31Anim = ", tl31Anim)
							PRINTLN("[NETCELEBRATION] - fRandomPhase = ", fRandomPhase)
							PRINTLN("[NETCELEBRATION] - REALLY_SLOW_BLEND_IN")
							PRINTLN("[NETCELEBRATION] - REALLY_SLOW_BLEND_OUT")
							PRINTLN("[NETCELEBRATION] - AF_LOOPING | AF_IGNORE_GRAVITY | AF_NOT_INTERRUPTABLE")
							
							IF NOT sCelebrationData.bCalledForceAnimUpdate[i]
								FORCE_PED_AI_AND_ANIMATION_UPDATE(sCelebrationData.pedWinnerClones[i])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sCelebrationData.pedWinnerClones[i], TRUE)
								sCelebrationData.bCalledForceAnimUpdate[i] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
//			//Setup the vehicle if one was included.
//			IF NOT IS_ENTITY_DEAD(vehWinner)
//				VECTOR vVehPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebrationData.objWinnerBackground, <<0.6079, -2.6196, -0.3600>>) + <<0.0, 0.0, CELEBRATION_SCREEN_WINNER_Z_OFFSET>>
//				VECTOR vVehRot = <<0.0, 0.0, 126.720>> + <<0.0, 0.0, 126.720>>
//			
//				SET_VEHICLE_FIXED(vehWinner)
//				SET_VEHICLE_ENGINE_ON(vehWinner, FALSE, TRUE)
//				FREEZE_ENTITY_POSITION(vehWinner, TRUE)
//				SET_ENTITY_COLLISION(vehWinner, FALSE)
//				SET_ENTITY_INVINCIBLE(vehWinner, TRUE)
//				SET_ENTITY_COORDS_NO_OFFSET(vehWinner, vVehPos)
//				SET_ENTITY_ROTATION(vehWinner, vVehRot)
//				sCelebrationData.vehWinnerClone = vehWinner
//			ENDIF
			
		ELSE
			
			IF bLoadedCoronaAnims
				PRINTLN("[NETCELEBRATION] - bLoadedCoronaAnims = FALSE.")
			ENDIF
			IF bPlinthLoaded
				PRINTLN("[NETCELEBRATION] - bPlinthLoaded = FALSE.")
			ENDIF
			bReturnValue = FALSE

			#IF IS_DEBUG_BUILD
			
//			IF NOT HAS_MODEL_LOADED(PROP_BIG_CIN_SCREEN)
//				PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - Waiting on - (PROP_BIG_CIN_SCREEN) to load.")
//			ENDIF
			
			IF NOT bPlinthLoaded
				PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - Waiting on - bPlinthLoaded.")
			ENDIF
			
			IF NOT bVehicleLoaded
				PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - Waiting on - bVehicleLoaded.")
			ENDIF
			
			IF NOT HAVE_CORONA_IDLE_ANIMS_LOADED()
				PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - Waiting on - HAVE_CORONA_IDLE_ANIMS_LOADED.")
			ENDIF
			
			#ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClones[0])
		VECTOR vPropPos = GET_ENTITY_COORDS(sCelebrationData.pedWinnerClones[0])
		
		IF CONTENT_IS_USING_ARENA()
		
			// Make the podium
			IF ARENA_IS_PODIUM(sCelebServer)
				IF NOT DOES_ENTITY_EXIST(sCelebrationData.ArenaPodium)
					CREATE_PODIUM(sCelebrationData.ArenaPodium)
					bReturnValue = FALSE
					PRINTLN("[NETCELEBRATION] [ARENA] CREATE_WINNER_SCENE_ENTITIES - Waiting on - CREATE_PODIUM ")
				ENDIF
			ENDIF
			
			CREATE_ARENA_LIGHT_RIG(sCelebrationData)
		
			IF sCelebServer.iNumPropsRequired > 0
			
				// Create Arena anim props, podium, beer etc.
				REPEAT sCelebServer.iNumPropsRequired i
					IF NOT CREATE_ARENA_PROP(sCelebServer, sCelebrationData.ArenaProps[i], ARENA_PROP_CREATION_COORDS(i), ARENA_PROP_MODEL(sCelebServer, i), ARENA_PROP_HEADING(i))
						bReturnValue = FALSE
						PRINTLN("[NETCELEBRATION] [ARENA] CREATE_WINNER_SCENE_ENTITIES - Waiting on - CREATE_ARENA_PROP.", sCelebServer.iNumPropsRequired)
					ENDIF			
				ENDREPEAT
				
				// Attach necessary arena props, ones that don't need animated
				REPEAT sCelebServer.iNumPropsRequired i
					IF ANIM_NEEDS_ATTACHED(sCelebServer, sCelebServer.eArenaClips[i])
						ATTACH_ARENA_PROP(sCelebServer.eArenaClips[i], sCelebrationData.pedWinnerClones[i], sCelebrationData.ArenaProps[i])
					ENDIF
				ENDREPEAT
			
			ENDIF
		ELSE
			IF CELEBRATION_IS_ISLAND_MODE()
				CREATE_ISLAND_LIGHT_PROP(sCelebrationData)
			ENDIF
		
			IF NOT CREATE_CELEBRATION_PROP(sCelebrationData.entityProp, iAnimToPlay, bDoingRallyRace, vPropPos)
				bReturnValue = FALSE
				PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - Waiting on - CREATE_CELEBRATION_PROP.")
			ENDIF
		ENDIF
	ENDIF
	
	// Wait until all peds have streamed.
	REPEAT COUNT_OF(sCelebrationData.pedWinnerClones) i
		IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClones[i])
			IF NOT IS_PED_INJURED(sCelebrationData.pedWinnerClones[i])
				IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sCelebrationData.pedWinnerClones[i])
					bReturnValue = FALSE
					PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - Waiting on - HAVE_ALL_STREAMING_REQUESTS_COMPLETED.")
				ENDIF
				IF NOT HAS_PED_HEAD_BLEND_FINISHED(sCelebrationData.pedWinnerClones[i])
					bReturnValue = FALSE
					PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - Waiting on - HAS_PED_HEAD_BLEND_FINISHED.")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bReturnValue
	
		CLEAR_AREA(vPedPos, 20.00, TRUE, FALSE)
		CLEAR_AREA_OF_OBJECTS(vPedPos, 20.00)		
		CLEAR_AREA_OF_PROJECTILES(vPedPos, 20.00)
//		REMOVE_PARTICLE_FX_IN_RANGE(vPedPos, 20.00)	
		REMOVE_DECALS_IN_RANGE(vPedPos, 20.00)
		PRINTLN("[NETCELEBRATION] CREATE_WINNER_SCENE_ENTITIES - REMOVE_DECALS_IN_RANGE, vPedPos = ", vPedPos)
	
		sCelebrationData.bCreateWinnerSceneEntities = TRUE
	ENDIF
	
	RETURN bReturnValue
	
ENDFUNC

/// PURPOSE:
///    Places the camera in front of the winner scene entities, this is split off from creation so camera cuts can be synched with other events.
///    bSetScriptForCutscene will call SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE), this helps with forcing the anims in the winner scene to play.
///    Some of the full body celebration anims require different cameras, to use these cameras set bUseFullBodyCamInterps to TRUE and set iFrontWinnerIndex to the player at the front of the screen. 
FUNC BOOL PLACE_WINNER_SCENE_CAMERA(CELEB_SERVER_DATA &sCelebServer, CELEBRATION_SCREEN_DATA &sCelebrationData, CAMERA_INDEX &camWinner, BOOL bSetScriptForCutscene = TRUE, INT iFrontWinnerIndex = -1, BOOL bUseFullBodyCamInterps = FALSE, BOOL bDoingRallyRace = FALSE)
	
	IF sCelebrationData.iBlackRectangleAlpha = sCelebrationData.iBlackRectangleAlpha
		sCelebrationData.iBlackRectangleAlpha = sCelebrationData.iBlackRectangleAlpha
	ENDIF
	IF iFrontWinnerIndex = iFrontWinnerIndex
		iFrontWinnerIndex = iFrontWinnerIndex
	ENDIF
	IF bUseFullBodyCamInterps
		bUseFullBodyCamInterps = bUseFullBodyCamInterps
	ENDIF
	IF bDoingRallyRace
		bDoingRallyRace = bDoingRallyRace
	ENDIF

	VECTOR vCamPos = CELEBRATION_CAM_POS()
	VECTOR vCamRot = CELEBRATION_CAM_ROT()
	FLOAT fFOV = CELEBRATION_CAM_FOV()
	
	TEXT_LABEL_63 tl63_AnimDict
	STRING sCamName

	tl63_AnimDict 	= ARENA_CAM_ANIM_DICT(sCelebServer)
	sCamName 		= ARENA_CAM_NAME(sCelebServer)
	
	IF CONTENT_IS_USING_ARENA()
	
		CELEBRATION_GET_PED_CREATION_DATA(sCelebServer, 0, vCamPos, vCamRot, 0)
	
		IF NOT DOES_CAM_EXIST(sCelebrationData.camArenaWinner)
			sCelebrationData.camArenaWinner = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
		ENDIF
										
		PLAY_CAM_ANIM(sCelebrationData.camArenaWinner, sCamName, tl63_AnimDict, vCamPos, vCamRot)
		
		PRINTLN("PLACE_WINNER_SCENE_CAMERA, sCamName = ", sCamName)
		PRINTLN("PLACE_WINNER_SCENE_CAMERA, tl63_AnimDict = ", tl63_AnimDict)
		PRINTLN("PLACE_WINNER_SCENE_CAMERA, vCamPos = ", vCamPos)
		PRINTLN("PLACE_WINNER_SCENE_CAMERA, vCamRot = ", vCamRot)
		PRINTLN("PLACE_WINNER_SCENE_CAMERA, 5510912 TIME = ", NATIVE_TO_INT(GET_NETWORK_TIME()))
		
		IF NOT IS_CAM_ACTIVE(sCelebrationData.camArenaWinner)
			SET_CAM_ACTIVE(sCelebrationData.camArenaWinner, TRUE)
			PRINTLN("[NETCELEBRATION] - PLACE_WINNER_SCENE_CAMERA - set cam as active.")
		ENDIF
		
		IF NOT IS_CAM_RENDERING(sCelebrationData.camArenaWinner)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			PRINTLN("[NETCELEBRATION] - PLACE_WINNER_SCENE_CAMERA - set cam as rendering.")
		ENDIF

	ELSE
	
		IF NOT DOES_CAM_EXIST(camWinner)
			camWinner = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
		ENDIF

		IF bDoingRallyRace
			SET_CAM_COORD(camWinner, <<415.2375, -979.8000, -99.0575>>) // <<415.5, -980.6, -99.5>>)//<<414.4, -980.3, -98.4>>)
			SET_CAM_ROT(camWinner, <<6.9325, -1.3500, 21.3400>>) // <<8.2, -0.1, 21.4>>)//<<-8.1, 0.0, -1.9>>)
			SET_CAM_FOV(camWinner, 47.2700) // 45.0)
		ELSE
			SET_CAM_COORD(camWinner, vCamPos) 
			SET_CAM_ROT(camWinner, vCamRot) 
			SET_CAM_FOV(camWinner, fFOV) 
		ENDIF
		SET_CAM_NEAR_CLIP(camWinner, 0.01)
		SHAKE_CELEB_CAM(camWinner, 0.3)
		
		SET_CAM_DOF_FNUMBER_OF_LENS(camWinner, 250.0)
		SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE(camWinner, 0.0)
		SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(camWinner, 1.0)
		
		PRINTLN("[NETCELEBRATION] - PLACE_WINNER_SCENE_CAMERA - camera placed details: ")
		PRINTLN("[NETCELEBRATION] - PLACE_WINNER_SCENE_CAMERA - coords =  ", vCamPos)
		PRINTLN("[NETCELEBRATION] - PLACE_WINNER_SCENE_CAMERA - rotation =  ", vCamRot)
		PRINTLN("[NETCELEBRATION] - PLACE_WINNER_SCENE_CAMERA - FOV =  ", fFOV)
		PRINTLN("[NETCELEBRATION] - PLACE_WINNER_SCENE_CAMERA - near clip = 0.01.")
		PRINTLN("[NETCELEBRATION] - PLACE_WINNER_SCENE_CAMERA - shake = 0.3.")
		
		IF NOT IS_CAM_ACTIVE(camWinner)
			SET_CAM_ACTIVE(camWinner, TRUE)
			PRINTLN("[NETCELEBRATION] - PLACE_WINNER_SCENE_CAMERA - set cam as active.")
		ENDIF
		
		IF NOT IS_CAM_RENDERING(camWinner)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			PRINTLN("[NETCELEBRATION] - PLACE_WINNER_SCENE_CAMERA - set cam as rendering.")
		ENDIF
		
	ENDIF
	
	
	IF bSetScriptForCutscene
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

//Draws the celebration screen with the background drawn in 3D relative to the winner screen object.
PROC DRAW_CELEBRATION_SCREEN_USING_3D_BACKGROUND(CELEBRATION_SCREEN_DATA &sCelebrationData, VECTOR vOffsetFromBackground, VECTOR vRotation, VECTOR vGlobalSize)
	//NOTE: currently it's just a black background (B*1637943), so no need to draw the background scaleform.
	//SET_SCALEFORM_MOVIE_3D_BRIGHTNESS(sCelebrationData.sfForeground, sCelebrationWidgetData.iBackgroundBrightness)
	//DRAW_SCALEFORM_MOVIE_3D_SOLID(sCelebrationData.sfForeground, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebrationData.objWinnerBackground, vOffsetFromBackground) + <<0.0, 0.0, CELEBRATION_SCREEN_WINNER_Z_OFFSET>>, 
	//						  	  vRotation, <<1.0, 1.0, 1.0>>, vGlobalSize) 
	IF vOffsetFromBackground.z = vGlobalSize.z //TEMP: for compile, as the background is commented out.
	OR vOffsetFromBackground.z = vRotation.z
	
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
	AND sCelebrationData.iRenderphasePauseAttempts >= 10
		PRINTLN("[NETCELEBRATION] - DRAW_CELEBRATION_SCREEN_THIS_FRAME - Fading in Screen (Renderphase failsafe)")
		DO_SCREEN_FADE_IN(100)
	ENDIF
	
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(sCelebrationData.sfCelebration, 255, 255, 255, 255)
ENDPROC

FUNC STRING GET_STAND_ALONE_CELEBRATIONS_SCRIPT_NAME()
	RETURN "Celebrations"
ENDFUNC

FUNC BOOL LAUNCH_STAND_ALONE_CELEBRATIONS_SCRIPT()
	
	STRING sScript = GET_STAND_ALONE_CELEBRATIONS_SCRIPT_NAME()
	
	REQUEST_SCRIPT(sScript)
	
	IF HAS_SCRIPT_LOADED(sScript)
		START_NEW_SCRIPT(sScript, MULTIPLAYER_MISSION_STACK_SIZE)
		SET_SCRIPT_AS_NO_LONGER_NEEDED(sScript)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC TEXT_LABEL_63 GET_TIME_AS_CLOCK_STRING(INT iTime, BOOL bAddMilliseconds = TRUE)
	
	INT y, h, m, s, ms
	TEXT_LABEL_63 tl63_return
	
	PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - iTime passed in = ", iTime)
	
	y = 60*60*1000
	h = iTime/y
	m = (iTime-(h*y))/(y/60)
	s = (iTime-(h*y)-(m*(y/60)))/1000
	ms = iTime-(h*y)-(m*(y/60))-(s*1000)
	
	PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - y = ", y)
	PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - h = ", h)
	PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - m = ", m)
	PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING -s = ", s)
	PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - ms = ", ms)
	
	tl63_return = ""
	
	IF (h != 0)
		tl63_return += h
		tl63_return += ":"
		PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
	ENDIF
	
	IF (m != 0)
	
		IF m < 10
			tl63_return += "0"
			PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
		ENDIF
	
		tl63_return += m
		PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
	ELSE
		tl63_return += "00"
		PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
	ENDIF
	
	tl63_return += ":"
	PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
	
	IF (s != 0)
	
		IF s < 10
			tl63_return += "0"
			PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
		ENDIF
		
		tl63_return += s
		PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
	ELSE
		tl63_return += "00"
		PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
	ENDIF
	
	IF bAddMilliseconds
		
		tl63_return += "."
		PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
		
		IF (ms != 0)
		
			IF ms < 100
				tl63_return += "0"
				PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
			ENDIF
			
			IF ms < 10
				tl63_return += "0"
				PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
			ENDIF
		
			tl63_return += ms
			PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
		ELSE
			tl63_return += "000"
			PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - tl63_return = ", tl63_return)
		ENDIF
		
	ENDIF
	
	PRINTLN("[NETCELEBRATION] - [SAC] - GET_TIME_AS_CLOCK_STRING - returning ", tl63_return)
	RETURN tl63_return
	
ENDFUNC

FUNC TEXT_LABEL_63 GET_DISTANCE_AS_STRING(INT iDistance)
	
	INT m, d
	TEXT_LABEL_63 tl63_return
	
	m = iDistance / 100
	d = iDistance % 100
	
	tl63_return = ""
	
	IF (m != 0)	
		tl63_return += m
	ELSE
		tl63_return += "0"
	ENDIF
	
	tl63_return += "."
	
	IF (d != 0)
	
		IF d < 10
			tl63_return += "0"
		ENDIF
		
		tl63_return += d
	ELSE
		tl63_return += "00"
	ENDIF
	
	RETURN tl63_return
	
ENDFUNC

FUNC FLOAT CONVERT_PILOT_SCHOOL_METERS_TO_FEET(FLOAT meters)
	RETURN meters/0.3048
ENDFUNC

FUNC TEXT_LABEL_63 GET_PILOT_SCHOOL_SCORE_TO_PASS_AS_STRING(INT &iScore)
	
	// Declare vars.
	TEXT_LABEL_63 tlReturn
	
	// convert score to a string.
	tlReturn = GET_DISTANCE_AS_STRING(iScore)
	
	// Append score with correct unit of measurement.
	tlReturn += " m"
	
	// Return score as sttring.
	RETURN tlReturn
	
ENDFUNC

PROC ADD_PILOT_SCHOOL_DATA_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	STRING strScreenID = "ch"
	STRING stStatTableId = "st"
	TEXT_LABEL_63 stValueToPass
	STRING strEmpty
	STRING strScoreLabel
	TEXT_LABEL_63 strTime = GET_TIME_AS_CLOCK_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPilotSchoolLessontime)
	BOOL bDoDistanceHeightVersion
	FLOAT fValuetoPass
	
	ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, CELEBRATION_JOB_TYPE_PILOT_SCHOOL, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission, strEmpty, strEmpty, strEmpty)
	sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission
		
		stStatTableId = "st2"
		CREATE_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId)
		
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.fPilotSchoolAvHeight != (-1.0)
			strScoreLabel = "CELEB_AVHGHT"
			fValuetoPass = g_TransitionSessionNonResetVars.sGlobalCelebrationData.fPilotSchoolAvHeight
			bDoDistanceHeightVersion = TRUE
		ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPoints != (-1)	
			stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPoints
			strScoreLabel = "CELEB_PSCORE"
		ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.fPilotSchoolDistance != (-1.0)
			strScoreLabel = "CELEB_DIS"
			fValuetoPass = g_TransitionSessionNonResetVars.sGlobalCelebrationData.fPilotSchoolDistance
			bDoDistanceHeightVersion = TRUE
		ELSE
			stValueToPass += strTime
			strScoreLabel = "CELEB_TIME"
		ENDIF
		
		IF bDoDistanceHeightVersion
			ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, strScoreLabel, stValueToPass, false, true, false, false, HUD_COLOUR_PINK, fValuetoPass, TRUE)
		ELSE
			ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, strScoreLabel, stValueToPass, false, true, false, false)
		ENDIF
		sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
		
		ADD_PILOT_SCHOOL_MEDAL_TO_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[0])
		ADD_CELEBRATION_STAT_TABLE_TO_CURRENT_SCREEN(sCelebrationData, strScreenID, stStatTableId)
		sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
		
		CELEBRATION_ADD_PAUSE_TO_SCREEN(sCelebrationData, strScreenID, CELEBRATION_SCREEN_STAT_DISPLAY_TIME)
		sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
		
		ADD_CASH_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCash )
		sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
		
		
		ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(	sCelebrationData, 
															strScreenID, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpGained, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpStarted, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelStartPoints, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelEndPoints, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNextLevel)
		sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
	
	ENDIF												
	
	
ENDPROC


PROC HEIST_CELEBRATION_ADD_RESULTS_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, CELEBRATION_JOB_TYPE eJobType, BOOL bPassed, STRING strFailReason, STRING strFailLiteral, STRING strFailLiteral2)
	
	ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, eJobType, bPassed, strFailReason, strFailLiteral, strFailLiteral2)
	sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_DISPLAY_TIME
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - adding results layer.")
	PRINTLN("[NETCELEBRATION] - [SAC] - strFailReason = ", strFailReason)
	PRINTLN("[NETCELEBRATION] - [SAC] - strLiteralFail = ", strFailLiteral)
	PRINTLN("[NETCELEBRATION] - [SAC] - strLiteralFail2 = ", strFailLiteral2)
	
ENDPROC
//
//PROC HEIST_CELEBRATION_ADD_BONUS_CASH_LAYERS(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT &iNumStatsAdded)
//	
//	TEXT_LABEL_63 stValueToPass
//	
//	// For debugging. 
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bMissionTimeBonusAchieved = TRUE
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iMissionTimeCash = 1000
////	
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bVehicleDamageBonusAchieved = TRUE
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iVehicleDamageCash = 2000
////	
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bPedDamageBonusAchieved = TRUE
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iPedDamageCash = 3000
////	
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bKillsBonusAchieved = TRUE
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iKillsCash = 4000
////	
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bSwatBonusAchieved = TRUE
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iSwatCash = 5000
////	
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bDoBagCashBonus = TRUE
////	g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iBagCashDropped = 6000
//	
//	stValueToPass = GET_TIME_AS_CLOCK_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMissionTime)
//	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bMissionTimeBonusAchieved
//		ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iMissionTimeCash, "CELEB_TIME_B", stValueToPass)
//		iNumStatsAdded++
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTotalVehicleDamageTaken
//	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bVehicleDamageBonusAchieved
//		ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iVehicleDamageCash, "CELEB_VDAM_B", stValueToPass)
//		iNumStatsAdded++
//	ENDIF	
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVipDamage
//	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bPedDamageBonusAchieved
//		ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iPedDamageCash, "CELEB_VIPDAM_B", stValueToPass)
//		iNumStatsAdded++
//	ENDIF	
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKills
//	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bKillsBonusAchieved
//		ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iKillsCash, "CELEB_EKILLS_B", stValueToPass)
//		iNumStatsAdded++
//	ENDIF		
//	
//	TEXT_LABEL_23 blank
//	stValueToPass = blank
//	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bSwatBonusAchieved
//		ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iSwatCash, "CELEB_SWAT", stValueToPass, FALSE, FALSE, TRUE)
//		iNumStatsAdded++
//	ENDIF	
//	
//	stValueToPass = blank
//	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.bDoBagCashBonus
//		INT iCashDropped = g_TransitionSessionNonResetVars.sGlobalCelebrationData.sBonusCashData.iBagCashDropped
//		ADD_MISSION_BONUS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, iCashDropped, "CELEB_CSHDRPD", stValueToPass, TRUE, TRUE, TRUE)
//		iNumStatsAdded++
//	ENDIF
//	
//ENDPROC
//
//PROC HEIST_CELEBRATION_ADD_STATS_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT &iNumStatsAdded)
//	
//	TEXT_LABEL_63 stValueToPass
//	STRING stStatTableId = "st"
//	TEXT_LABEL_63 tl63_temp
//	
//	// Add rating screen, based creator flags that dictate ifeach individual stat should be on the screen. Note each value must be turned onto a string.
//	CREATE_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId)
//	
//	stValueToPass = ""
//	tl63_temp = GET_TIME_AS_CLOCK_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMissionTime)
//	stValueToPass += tl63_temp
//	PRINTLN("[NETCELEBRATION] - [SAC] - time value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_Time)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_TIME", stValueToPass, false, true, false, FALSE)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - time value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKills
//	PRINTLN("[NETCELEBRATION] - [SAC] - num enemy peds killed value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_TotalKills)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_ENKILLS", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - num enemy peds killed value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iChoppersDestroyed
//	stValueToPass += " / "
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumChoppersDestroyed
//	PRINTLN("[NETCELEBRATION] - [SAC] - num choppers killed value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_ChoppersDestroyed)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_CHOPPERS_DESTROYED", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - num choppers killed value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehiclesDestoryed
//	stValueToPass += " / "
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumVehDestroyed
//	PRINTLN("[NETCELEBRATION] - [SAC] - num vehs killed value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_VehDestroyed)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_VEHICLES_DESTROYED", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - num vehs killed value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iEndingAccuracy
//	stValueToPass += "%"
//	PRINTLN("[NETCELEBRATION] - [SAC] - accuracy value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_Accuracy)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_SHOOTING_ACCURACY", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - accuracy value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPrisonersKilled
//	PRINTLN("[NETCELEBRATION] - [SAC] - prisoners killed value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_PrisonersKilled)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_PRISONERS_KILLED", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - prisoners killed value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVIPDamage 
//	stValueToPass += "%"
//	PRINTLN("[NETCELEBRATION] - [SAC] - VIP damage taken value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_VipDamage)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_VIP_DAMAGE_TAKEN", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - VIP damage taken value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	tl63_temp = GET_TIME_AS_CLOCK_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iLoseWantedTime*1000) // Larry gives us this in seconds, multiply up to milliseconds for get as clock string function.
//	stValueToPass += tl63_temp
//	PRINTLN("[NETCELEBRATION] - [SAC] - lose wanted time in van value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_WantedLoseTime)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_LOSE_WANTED_TIME", stValueToPass, false, true, false, FALSE)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - lose wanted time not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iBikehighSpeed
//	stValueToPass += "Kph"
//	PRINTLN("[NETCELEBRATION] - [SAC] - top speed value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_BikeTopspeed)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_TOP_SPEED", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - top speed value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iDistanceTravelled
//	stValueToPass += "m"
//	PRINTLN("[NETCELEBRATION] - [SAC] - distance travelled value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_DistanceTravelled)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_DIS_TRAV", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - distance travelled value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iDeaths
//	PRINTLN("[NETCELEBRATION] - [SAC] - deaths value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_Deaths)		
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_DEATHS", stValueToPass, false, true, false)	
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - deaths value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iInnocentsKilled
//	PRINTLN("[NETCELEBRATION] - [SAC] - innocents killed value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_InnocentsKilled)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_INN_KILLED", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - innocents killed value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeadshots
//	PRINTLN("[NETCELEBRATION] - [SAC] - headshots value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_Headshots)	
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_HSHOTS", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - headshots value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTotalVehicleDamageTaken
//	PRINTLN("[NETCELEBRATION] - [SAC] - vehicle damage taken value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_TotalVehicleDamageTaken)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_VEH_DAM", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - vehicle damage taken value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iAvgTeamAccuracy
//	stValueToPass += "%"
//	PRINTLN("[NETCELEBRATION] - [SAC] - average team accuracy value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_TeamAccuracy)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_TACCURACY", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - average team accuracy value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	stValueToPass = ""
//	stValueToPass += g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPhotosSnapped
//	PRINTLN("[NETCELEBRATION] - [SAC] - photos snapped value to pass = ", stValueToPass)
//	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSBitSet, ciFMMC_HeistCS_PhotoTaken)
//		ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, "CELEB_PHTAKEN", stValueToPass, false, true, false)
//		iNumStatsAdded++
//	#IF IS_DEBUG_BUILD
//	ELSE 
//		PRINTLN("[NETCELEBRATION] - [SAC] - photos snapped value not being passed, not set in cloud data.")
//	#ENDIF
//	ENDIF
//	
//	ADD_CELEBRATION_STAT_TABLE_TO_CURRENT_SCREEN(sCelebrationData, strScreenID, stStatTableId)
//	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - adding stats layer.")
//	
//ENDPROC

FUNC INT HEIST_CELEBRATION_GET_STATS_LAYER_PAUSE_TIME(INT iNumStatsAdded)
	
	INT iWipeTime = (CELEBRATION_SCREEN_STAT_WIPE_TIME*iNumStatsAdded)
	INT iLesExtraDelay = (3000 - iWipeTime)
	
	RETURN (iWipeTime + (iNumStatsAdded*750) + iLesExtraDelay)
	
ENDFUNC

FUNC INT HEIST_CELEBRATION_GET_BONUS_LAYERS_PAUSE_TIME(INT iNumStatsAdded)
	
	INT iWipeTime = (CELEBRATION_SCREEN_STAT_WIPE_TIME*iNumStatsAdded)
	
	RETURN (iWipeTime + (iNumStatsAdded*4000))
	
ENDFUNC

PROC HEIST_CELEBRATION_ADD_PAUSE_FOR_STATS_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenName, INT iNumStatsAdded)
	
	INT iAmountToAdd = HEIST_CELEBRATION_GET_STATS_LAYER_PAUSE_TIME(iNumStatsAdded)
	CELEBRATION_ADD_PAUSE_TO_SCREEN(sCelebrationData, strScreenName, iAmountToAdd)
	
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - adding ", iAmountToAdd, " of a pause to the stats layer. Num stats in layer = ", iNumStatsAdded)

ENDPROC

PROC HEIST_CELEBRATION_ADD_MEDALS_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT &iNumMedalsAdded)
	
	INT i
	
	// Setup screen of medal rating for each participant.
	STRING stStatTableId = "st2"
	CREATE_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId)
	
	REPEAT 4 i
	
		PRINTLN("[NETCELEBRATION] - [SAC] - checking player ", i, ", in medals array.")
		
		IF NOT IS_STRING_NULL_OR_EMPTY(g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i])
			
			ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId, g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i], g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[i], true, false, false, false, g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[i])
			iNumMedalsAdded++
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[NETCELEBRATION] - [SAC] - adding medal for player ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i])
				PRINTLN("[NETCELEBRATION] - [SAC] - string being passed = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[i])
				SWITCH g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[i]
					CASE HUD_COLOUR_PLATINUM	PRINTLN("[NETCELEBRATION] - [SAC] - adding platinum medal - enum = HUD_COLOUR_PLATINUM.") 											BREAK
					CASE HUD_COLOUR_GOLD 		PRINTLN("[NETCELEBRATION] - [SAC] - adding gold medal - enum = HUD_COLOUR_GOLD.") 													BREAK
					CASE HUD_COLOUR_SILVER 		PRINTLN("[NETCELEBRATION] - [SAC] - adding silver medal - enum = HUD_COLOUR_SILVER.") 												BREAK
					CASE HUD_COLOUR_BRONZE 		PRINTLN("[NETCELEBRATION] - [SAC] - adding bronze medal - enum = HUD_COLOUR_BRONZE.") 												BREAK
					CASE HUD_COLOUR_WHITE 		PRINTLN("[NETCELEBRATION] - [SAC] - adding none medal - enum = HUD_COLOUR_WHITE.") 												BREAK
					DEFAULT 					PRINTLN("[NETCELEBRATION] - [SAC] - something has gone wrong, adding enum colour that is not platinum, gold, silver or bronze.")	BREAK
				ENDSWITCH
			#ENDIF
			
		#IF IS_DEBUG_BUILD
		ELSE 
			PRINTLN("[NETCELEBRATION] - [SAC] - medal values not being passed, player name is empty.")
		#ENDIF
		
		ENDIF
	ENDREPEAT
	
	// Actually add medals. 
	ADD_CELEBRATION_STAT_TABLE_TO_CURRENT_SCREEN(sCelebrationData, strScreenID, stStatTableId)
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - adding medals layer.")
	
ENDPROC

PROC HEIST_CELEBRATION_ADD_TAKE_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, CELEBRATION_JOB_TYPE eJobType)
	
	// Setup screen of medal rating for each participant.
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - adding take layer.")
	PRINTLN("[NETCELEBRATION] - [SAC] - take values to pass. actual take = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake, ", max take = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake)
	ADD_CASH_WON_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake, 0, (eJobType = CELEBRATION_JOB_TYPE_HEIST_FINALE) ) // , g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake)
	
ENDPROC

FUNC INT HEIST_CELEBRATION_GET_MEDALS_LAYER_PAUSE_TIME(INT iNumMedalsAdded)
	
	INT iValue
	
	iValue = CELEBRATION_SCREEN_STAT_WIPE_TIME*iNumMedalsAdded
	iValue += CELEBRATION_SCREEN_STAT_DISPLAY_TIME*iNumMedalsAdded
	iValue += CELEBRATION_SCREEN_STAT_WIPE_TIME
	
	RETURN iValue
	
ENDFUNC

PROC HEIST_CELEBRATION_ADD_PAUSE_FOR_MEDALS_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, INT iNumMedalsAdded)
	
	INT iAmountToAdd = HEIST_CELEBRATION_GET_MEDALS_LAYER_PAUSE_TIME(iNumMedalsAdded)
	
	IF iAmountToAdd < (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
		iAmountToAdd = (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
	ENDIF
	
	CELEBRATION_ADD_PAUSE_TO_SCREEN(sCelebrationData, strScreenID, iAmountToAdd)
	
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - adding ", iAmountToAdd, " of a pause to the medals layer. Medals in layer = ", iNumMedalsAdded)
	
ENDPROC

PROC HEIST_CELEBRATION_ADD_JOB_POINTS_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, BOOL bAlignRight = FALSE)
	
	ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumJobPoints, bAlignRight)
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - job was passed, adding job points layer.")
															
ENDPROC

PROC HEIST_CELEBRATION_STANDARD_CASH_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, BOOL bAlignLeft = FALSE)
	
	ADD_CASH_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake, TRUE, bAlignLeft)
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - job was passed, adding standard cash layer.")
	
ENDPROC

PROC HEIST_CELEBRATION_ADD_RP_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID)

	ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(	sCelebrationData, 
															strScreenID, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpGained, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpStarted, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelStartPoints, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelEndPoints, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel, 
															g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNextLevel)
	
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - job was passed, adding RP/rankup layer.")
															
ENDPROC

PROC HEIST_CELEBRATION_ADD_FAKE_LAYER_FOR_BEFORE_RP_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID)
	
	STRING stStatTableId = "st3"
	CREATE_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId)
	ADD_CELEBRATION_STAT_TABLE_TO_CURRENT_SCREEN(sCelebrationData, strScreenID, stStatTableId)
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - adding fake layer for before RP layer.")

ENDPROC

PROC HEIST_CELEBRATION_ADD_FINAL_FAKE_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID)
	
	STRING stStatTableId = "st4"
	CREATE_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenID, stStatTableId)
	ADD_CELEBRATION_STAT_TABLE_TO_CURRENT_SCREEN(sCelebrationData, strScreenID, stStatTableId)
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - adding fake layer.")

ENDPROC

PROC HEIST_CELEBRATION_ADD_PAUSE_FOR_LAYER_BEFORE_FINAL_FAKE_LAYER(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID)
	
	INT iAmountToAdd = 6000
	ADD_PAUSE_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenID, iAmountToAdd, g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition)
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - adding ", iAmountToAdd, " of a pause to the final layer.")
	
ENDPROC

FUNC INT GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(BOOL bPassed)
	IF bPassed
		RETURN 1
	ENDIF
	RETURN 2
ENDFUNC

PROC UPDATE_CASH_VALUES_BASED_ON_ELITE_CHALLENGE_COMPONENT_COUNT(INT &iPreviousValue, INT &iCurrentValue, INT iEliteChallengeComponentCount)

	IF iEliteChallengeComponentCount = 0
	AND g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumEliteChallengeComponents > 1
		iPreviousValue = iCurrentValue
	ELSE
		iCurrentValue = iPreviousValue
		IF iEliteChallengeComponentCount = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumEliteChallengeComponents
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete
				iCurrentValue = iCurrentValue + g_TransitionSessionNonResetVars.sGlobalCelebrationData.iEliteChallengeBonusValue
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC STRING GET_ELITE_CHALLENGE_RESULT_TOP_TEXT_LABEL()
	
	IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete
		RETURN "CELEB_EL_CH_F"
	ENDIF
	
	RETURN "CELEB_EL_CH_P"
	
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_CELEBRATION_GLOBAL_DATA_VALUES()
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES")
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bPassedMission = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iPilotSchoolLessontime = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPilotSchoolLessontime)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - fPilotSchoolAvHeight = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.fPilotSchoolAvHeight)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - fPilotSchoolDistance = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.fPilotSchoolDistance)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iRpGained = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpGained)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iRpStarted = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpStarted)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iCurrentLevelStartPoints = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelStartPoints)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iCurrentLevelEndPoints = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelEndPoints)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iCurrentLevel = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iNextLevel = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNextLevel)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bDoingPilotSchoolCelebration = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingPilotSchoolCelebration)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iAvHeight = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iAvHeight)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iPoints = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPoints)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iDistance = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iDistance)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iCash = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCash)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bInTestMode = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bInTestMode)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iWinnerSceneType = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bReadyToMoveOnFromHeistCelebration = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bReadyToMoveOnFromHeistCelebration)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bReadyForApartmentTakeOver = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bReadyForApartmentTakeOver)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bDrawBlackRectangle[0] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDrawBlackRectangle[0])
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bDrawBlackRectangle[1] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDrawBlackRectangle[1])
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bBlockStripClubPedsForCongratsScreen = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockStripClubPedsForCongratsScreen)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bDoingHeistCelebration = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bDoingHeistEndWinnerScene = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bDoingAfterApartmentPanHeistCelebration = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bCloningComplete = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bStreamingRequestsComplete = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bStreamingRequestsComplete)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iPlayerNameHash[0] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[0])
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iPlayerNameHash[1] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[1])
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iPlayerNameHash[2] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[2])
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iPlayerNameHash[3] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[3])
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - tl63_missionName = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iMaximumTake = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iActualTake = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iActualTake)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iMyCutPercentage = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyCutPercentage)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iMyTake = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bDifficultyHighEnoughForEliteChallenge = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDifficultyHighEnoughForEliteChallenge)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iNumEliteChallengeComponents = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumEliteChallengeComponents)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentMissionTime = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentMissionTime)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iTargetMissionTime = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetMissionTime)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iMissionTime = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMissionTime)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bMissionTimeChallengeComplete = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMissionTimeChallengeComplete)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentVehicleDamage = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentVehicleDamage)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iVehicleDamagePercentageTarget = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageTarget)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iVehicleDamagePercentageActual = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageActual)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bVehicleDamagePercentagePassed = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bVehicleDamagePercentagePassed)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentRachDamage = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentRachDamage)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iRachDamagePercentageTarget = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageTarget)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iRachDamagePercentageActual = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageActual)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bRachDamagePercentagePassed = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bRachDamagePercentagePassed)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentExtractionTime = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentExtractionTime)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iTargetExtractionTime = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetExtractionTime)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iExtractionTime = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iExtractionTime)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bExtractionTimeChallengeComplete = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bExtractionTimeChallengeComplete)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentNumPedKills = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedKills)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iNumPedKillsTarget = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKillsTarget)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iNumPedKills = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKills)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bNumPedKillsChallengeComplete = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedKillsChallengeComplete)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentNooseCalled = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNooseCalled)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bNooseCalled = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNooseCalled)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentHealthDamage = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHealthDamage)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iHealthDamagePercentageTarget = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageTarget)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iHealthDamagePercentageActual = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageActual)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bHealthDamagePercentagePassed = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHealthDamagePercentagePassed)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentLivesLost = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentLivesLost)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bNumLivesLostChallengeComplete = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumLivesLostChallengeComplete)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentTripSkip = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentTripSkip)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentDroppedToDirect = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDroppedToDirect)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentDidQuickRestart = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDidQuickRestart)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentNumPedHeadshots = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedHeadshots)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iNumPedHeadshotsTarget = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshotsTarget)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iNumPedHeadshots = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshots)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bNumPedHeadshotsChallengeComplete = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedHeadshotsChallengeComplete)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentHackFailed = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHackFailed)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iNumHacksFailedTarget = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailedTarget)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iNumHacksFailed = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailed)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bNumHackFailedChallengeComplete = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumHackFailedChallengeComplete)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bShowEliteComponentLootBagFull = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentLootBagFull)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bLootBagFullChallengeComplete = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bLootBagFullChallengeComplete)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bEliteChallengeComplete = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iEliteChallengeBonusValue = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iEliteChallengeBonusValue)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bFirstTimeBonusShouldBeShown = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstTimeBonusShouldBeShown)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iFirstTimeBonusValue = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iFirstTimeBonusValue)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bOrderBonusShouldBeShown = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOrderBonusShouldBeShown)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iOrderBonusValue = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iOrderBonusValue)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bSameTeamBonusShouldBeShown = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bSameTeamBonusShouldBeShown)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iSameTeamBonusValue = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iSameTeamBonusValue)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bUltimateBonusShouldBeShown = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bUltimateBonusShouldBeShown)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iUltimateBonusValue = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iUltimateBonusValue)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iHeistType = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - strLeaderBoardPlayerName[0] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[0])
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - strLeaderBoardPlayerName[1] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[1])
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - strLeaderBoardPlayerName[2] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[2])
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - strLeaderBoardPlayerName[3] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[3])
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - strRatingColour[0] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[0])
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - strRatingColour[1] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[1])
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - strRatingColour[2] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[2])
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - strRatingColour[3] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[3])
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bOkToKillCelebrationScript= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOkToKillCelebrationScript)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bIAmHeistLeader = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - strFailReason = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - strFailReasonLiteral = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - strFailReasonLiteral2 = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral2)
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - iNumJobPoints = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumJobPoints)
	
	
	PRINTLN("[SAC] - [NETCELEBRATION] - PRINT_CELEBRATION_DATA_VALUES - bFirstUpdate = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstUpdate)
	
ENDPROC
#ENDIF


FUNC INT GET_CHALLENGE_OVERALL_RESULT_SOUND_ID(BOOL bPassedOverall)
	
	IF bPassedOverall
		RETURN 6
	ENDIF
	
	RETURN 2
	
ENDFUNC

PROC SET_CHALLENGE_SOUND_ID(BOOL &bPlayedFirstTimeSound, INT &iSoundValue)
	
	IF NOT bPlayedFirstTimeSound
		iSoundValue = 6
		bPlayedFirstTimeSound = TRUE
	ELSE
		iSoundValue = -1
	ENDIF
	
ENDPROC

PROC ADD_END_HEIST_DATA_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - calling ADD_END_HEIST_DATA_TO_CELEBRATION_SCREEN.")
	DEBUG_PRINTCALLSTACK()
	
	// Variables.
	INT iNumMedalsAdded
	STRING strScreenName = "SUMMARY"
	CELEBRATION_JOB_TYPE eJobType
	TEXT_LABEL_63 strLiteralFail, strLiteralFail2
	eADD_HEIST_CELEBRATION_DATA_CATEGORY eCelebrationScreenStructure
	
	
	// ******************
	// Get the job type.
	// ******************
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__PREP
		eJobType = CELEBRATION_JOB_TYPE_HEIST_PREP
		PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - eJobType = CELEBRATION_JOB_TYPE_HEIST_PREP")
	ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__FINALE
		eJobType = CELEBRATION_JOB_TYPE_HEIST_FINALE
		PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - eJobType = CELEBRATION_JOB_TYPE_HEIST_FINALE")
	#IF IS_DEBUG_BUILD
	ELSE
		SCRIPT_ASSERT("ADD_END_HEIST_DATA_TO_CELEBRATION_SCREEN - iHeistType != 1 or 2")
		PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - iHeistType != 1 or 2")
	#ENDIF
	ENDIF
	
	
	// ***********************************
	// Get screen structure to construct.
	// ***********************************
	
	IF eJobType = CELEBRATION_JOB_TYPE_HEIST_PREP
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission
				eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_LEADER_PASS
				PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_LEADER_PASS")
			ELSE
				eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_LEADER_FAIL
				PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_LEADER_FAIL")
			ENDIF
		ELSE
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission
				eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_NOT_LEADER_PASS
				PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_NOT_LEADER_PASS")
			ELSE
				eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_NOT_LEADER_FAIL
				PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_NOT_LEADER_FAIL")
			ENDIF
		ENDIF
	ELIF eJobType = CELEBRATION_JOB_TYPE_HEIST_FINALE
		IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission
				eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_LEADER_PASS
				PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_LEADER_PASS")
			ELSE
				eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_LEADER_FAIL
				PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_LEADER_FAIL")
			ENDIF
		ELSE
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission
				eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_NOT_LEADER_PASS
				PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_NOT_LEADER_PASS")
			ELSE
				eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_NOT_LEADER_FAIL
				PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_NOT_LEADER_FAIL")
			ENDIF
		ENDIF
	ENDIF
	
//	eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_TEST
	
	// ***********************************************
	// Get any data required for screen construction.
	// ***********************************************
	
	IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission
		strLiteralFail = g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral
		strLiteralFail2 = g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral2
		PRINTLN("[NETCELEBRATION] - [SAC] - bPassedMission = TRUE, setting strLiteralFail = ", strLiteralFail)
		PRINTLN("[NETCELEBRATION] - [SAC] - bPassedMission = TRUE, setting strLiteralFail2 = ", strLiteralFail2)
	ENDIF
	
	
	// ***************************************
	// Print out all global celebration data.
	// ***************************************
	
	#IF IS_DEBUG_BUILD
	PRINT_CELEBRATION_GLOBAL_DATA_VALUES()
	#ENDIF
	
	
	// ************************************************************
	// Construct screen and track how long screen should last for.
	// ************************************************************
	
	INT iIncrementCashPauseTime
	INT iPreviousValue, iCurrentValue
	TEXT_LABEL_63 tl63_targetValue, tl63_actualValue
	INT iEliteChallengeComponentPassed
	BOOL bChallengeVoided
	STRING strBottomTxt
	INT iOverridePassedSoundFirstEliteChallenge = -1
	BOOL bPlayedEliteChallengeStartSound
	
//	eCelebrationScreenStructure = eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_NOT_LEADER_PASS
	
	SWITCH eCelebrationScreenStructure
		
		CASE eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_TEST
			
		BREAK
		
		CASE eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_LEADER_PASS
		
			HEIST_CELEBRATION_ADD_RESULTS_LAYER(sCelebrationData, strScreenName, eJobType, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission, g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason, strLiteralFail, strLiteralFail2)
			sCelebrationData.iEstimatedScreenDuration = (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for results layer.")
			
			HEIST_CELEBRATION_ADD_MEDALS_LAYER(sCelebrationData, strScreenName, iNumMedalsAdded)
			HEIST_CELEBRATION_ADD_PAUSE_FOR_MEDALS_LAYER(sCelebrationData, strScreenName, iNumMedalsAdded)
			sCelebrationData.iEstimatedScreenDuration += HEIST_CELEBRATION_GET_MEDALS_LAYER_PAUSE_TIME(iNumMedalsAdded)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for medals layer.")
			
			HEIST_CELEBRATION_ADD_JOB_POINTS_LAYER(sCelebrationData, strScreenName, TRUE)
			sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for job points layer.")
			
			HEIST_CELEBRATION_ADD_RP_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_PAUSE_FOR_LAYER_BEFORE_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			sCelebrationData.iEstimatedScreenDuration += (6000 + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for final layer.")
			
		BREAK
		
		CASE eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_NOT_LEADER_PASS
		
			HEIST_CELEBRATION_ADD_RESULTS_LAYER(sCelebrationData, strScreenName, eJobType, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission, g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason, strLiteralFail, strLiteralFail2)
			sCelebrationData.iEstimatedScreenDuration = (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for results layer.")
			
			HEIST_CELEBRATION_ADD_MEDALS_LAYER(sCelebrationData, strScreenName, iNumMedalsAdded)
			HEIST_CELEBRATION_ADD_PAUSE_FOR_MEDALS_LAYER(sCelebrationData, strScreenName, iNumMedalsAdded)
			sCelebrationData.iEstimatedScreenDuration += HEIST_CELEBRATION_GET_MEDALS_LAYER_PAUSE_TIME(iNumMedalsAdded)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for medals layer.")
			
			ADD_CASH_WON_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake,0, (eJobType = CELEBRATION_JOB_TYPE_HEIST_FINALE), TRUE)
			sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for cash layer.")
			
			HEIST_CELEBRATION_ADD_JOB_POINTS_LAYER(sCelebrationData, strScreenName)
			sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for job points layer.")
			
			HEIST_CELEBRATION_ADD_RP_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_PAUSE_FOR_LAYER_BEFORE_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			sCelebrationData.iEstimatedScreenDuration += (6000 + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for final layer.")
			
		BREAK
		
		CASE eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_LEADER_FAIL
			
			HEIST_CELEBRATION_ADD_RESULTS_LAYER(sCelebrationData, strScreenName, eJobType, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission, g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason, strLiteralFail, strLiteralFail2)
			sCelebrationData.iEstimatedScreenDuration = (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for results layer.")
			
			HEIST_CELEBRATION_ADD_RP_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_PAUSE_FOR_LAYER_BEFORE_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			sCelebrationData.iEstimatedScreenDuration += (6000 + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for final layer.")
			
		BREAK
		
		CASE eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_PREP_NOT_LEADER_FAIL
			
			HEIST_CELEBRATION_ADD_RESULTS_LAYER(sCelebrationData, strScreenName, eJobType, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission, g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason, strLiteralFail, strLiteralFail2)
			sCelebrationData.iEstimatedScreenDuration = (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for results layer.")
			
			ADD_CASH_WON_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake,0, (eJobType = CELEBRATION_JOB_TYPE_HEIST_FINALE), FALSE)
			sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for cash layer.")
			
			HEIST_CELEBRATION_ADD_RP_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_PAUSE_FOR_LAYER_BEFORE_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			sCelebrationData.iEstimatedScreenDuration += (6000 + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for final layer.")
			
		BREAK
		
		CASE eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_LEADER_PASS
		CASE eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_NOT_LEADER_PASS
			TEXT_LABEL_31 tl31
			// 1. Result.
			HEIST_CELEBRATION_ADD_RESULTS_LAYER(sCelebrationData, strScreenName, eJobType, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission, g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason, strLiteralFail, strLiteralFail2)
			sCelebrationData.iEstimatedScreenDuration = (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - added results. Passed = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission)
			
			// 2. Medals.
			HEIST_CELEBRATION_ADD_MEDALS_LAYER(sCelebrationData, strScreenName, iNumMedalsAdded)
			HEIST_CELEBRATION_ADD_PAUSE_FOR_MEDALS_LAYER(sCelebrationData, strScreenName, iNumMedalsAdded)
			sCelebrationData.iEstimatedScreenDuration += HEIST_CELEBRATION_GET_MEDALS_LAYER_PAUSE_TIME(iNumMedalsAdded)
			PRINTLN("[NETCELEBRATION] - [SAC] - added medals. iNumMedalsAdded = ", iNumMedalsAdded)
			
			// 3.Create screen.
			CREATE_INCREMENTAL_CASH_CELEBRATION_SCREEN(sCelebrationData)
			sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_WIPE_TIME
			PRINTLN("[NETCELEBRATION] - [SAC] - created cash wall.")
			
			// 4. Add screens - potential take.
			iPreviousValue = 0
			iCurrentValue = 0
			PRINTLN("[NETCELEBRATION] - [SAC] - initialised previous and current values to 0.")
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				tl31 = "CELEB_P_EARN"
			ELSE
				tl31 = "CELEB_POTENTIAL_TAKE"
			ENDIF
			iCurrentValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake
			ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), tl31, "", "", "", 3, -1, 3)
			iIncrementCashPauseTime = 3333
			sCelebrationData.iEstimatedScreenDuration += 3000
			PRINTLN("[NETCELEBRATION] - [SAC] - added max take. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
			
			// 5. Add screens - actual take.
			iPreviousValue = iCurrentValue
			iCurrentValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iActualTake
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				tl31 = "CELEB_A_EARN"
			ELSE
				tl31 = "CELEB_ACTUAL_TAKE"
			ENDIF
			ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), tl31, "", "", "",  3, -1, 4)
			iIncrementCashPauseTime += 3000
			sCelebrationData.iEstimatedScreenDuration += 3000
			PRINTLN("[NETCELEBRATION] - [SAC] - added actual take. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
			
			// 6. Add screens - percentage cut of take.
			iPreviousValue = iCurrentValue
			iCurrentValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				tl31 = "CELEB_C_EARN"
			ELSE
				tl31 = "PER_CELEB_CUT_TAKE"
			ENDIF
			ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), tl31, "", "", "", 3, g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyCutPercentage, 5)
			iIncrementCashPauseTime += 3000
			sCelebrationData.iEstimatedScreenDuration += 3000
			PRINTLN("[NETCELEBRATION] - [SAC] - added my take. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
			
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDifficultyHighEnoughForEliteChallenge
				
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentTripSkip
					
					iPreviousValue = iCurrentValue
					iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(FALSE)
					SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
					ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH_V", "CELEB_EL_NOTRIP", "", "", iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
					iIncrementCashPauseTime += 3000
					sCelebrationData.iEstimatedScreenDuration += 3000
					
					bChallengeVoided = TRUE
					PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge void for trip skip. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
				
				ENDIF
				
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDroppedToDirect
					
					iPreviousValue = iCurrentValue
					iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(FALSE)
					SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
					ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH_V", "CELEB_EL_NODROP", "", "", iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
					iIncrementCashPauseTime += 3000
					sCelebrationData.iEstimatedScreenDuration += 3000
					
					bChallengeVoided = TRUE
					PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge void for dropping to direct. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
				
				ENDIF
					
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDidQuickRestart
					
					iPreviousValue = iCurrentValue
					iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(FALSE)
					SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
					ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH_V", "CELEB_EL_NOQR", "", "", iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
					iIncrementCashPauseTime += 3000
					sCelebrationData.iEstimatedScreenDuration += 3000
					
					bChallengeVoided = TRUE
					PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge void for quick restart. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
				
				ENDIF
					
				IF NOT bChallengeVoided
					
					iPreviousValue = iCurrentValue
					
					// 7. Add screens - mission time elite challenge.
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentMissionTime
						tl63_targetValue = GET_TIME_AS_CLOCK_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetMissionTime, FALSE)
						tl63_actualValue = GET_TIME_AS_CLOCK_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMissionTime, FALSE)
						iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMissionTimeChallengeComplete)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge mission time. iEliteChallengeComponentPassed = ", iEliteChallengeComponentPassed)
						SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge mission time. bPlayedEliteChallengeStartSound = ", bPlayedEliteChallengeStartSound)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge mission time. iOverridePassedSoundFirstEliteChallenge = ", iOverridePassedSoundFirstEliteChallenge)
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH", "CELEB_EL_COMPT", tl63_targetValue, tl63_actualValue, iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge mission time. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
					ENDIF
					
					// 8. Add screens - vehicle damage elite challenge.
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentVehicleDamage
						tl63_targetValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageTarget
						tl63_targetValue += "%"
						tl63_actualValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageActual
						tl63_actualValue += "%"
						iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bVehicleDamagePercentagePassed)
						strBottomTxt = "CELEB_EL_VEHDM"
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageTarget <= 0
							strBottomTxt = "CELEB_EL_NVEHDM"
						ENDIF
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge vehicle damage. iEliteChallengeComponentPassed = ", iEliteChallengeComponentPassed)
						SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge vehicle damage. bPlayedEliteChallengeStartSound = ", bPlayedEliteChallengeStartSound)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge vehicle damage. iOverridePassedSoundFirstEliteChallenge = ", iOverridePassedSoundFirstEliteChallenge)
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH", strBottomTxt, tl63_targetValue, tl63_actualValue, iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge vehicle damage. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
					ENDIF
					
					// 9. Add screens - vip damage elite challenge.
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentRachDamage
						tl63_targetValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageTarget
						tl63_targetValue += "%"
						tl63_actualValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageActual
						tl63_actualValue += "%"
						iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bRachDamagePercentagePassed)
						strBottomTxt = "CELEB_EL_RCHDM"
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageTarget <= 0
							strBottomTxt = "CELEB_EL_NRCHDM"
						ENDIF
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge rach damage. iEliteChallengeComponentPassed = ", iEliteChallengeComponentPassed)
						SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge rach damage. bPlayedEliteChallengeStartSound = ", bPlayedEliteChallengeStartSound)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge rach damage. iOverridePassedSoundFirstEliteChallenge = ", iOverridePassedSoundFirstEliteChallenge)
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH", strBottomTxt, tl63_targetValue, tl63_actualValue, iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge rach damage. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
					ENDIF
					
					// 10. Add screens - extraction time elite challenge.
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentExtractionTime
						tl63_targetValue = GET_TIME_AS_CLOCK_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetExtractionTime, FALSE)
						tl63_actualValue = GET_TIME_AS_CLOCK_STRING(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iExtractionTime, FALSE)
						iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bExtractionTimeChallengeComplete)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge extraction time. iEliteChallengeComponentPassed = ", iEliteChallengeComponentPassed)
						SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge extraction time. bPlayedEliteChallengeStartSound = ", bPlayedEliteChallengeStartSound)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge extraction time. iOverridePassedSoundFirstEliteChallenge = ", iOverridePassedSoundFirstEliteChallenge)
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH", "CELEB_EL_EXTTM", tl63_targetValue, tl63_actualValue, iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge extraction time. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
					ENDIF
					
					// 11. Add screens - ped kills elite challenge.
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedKills
						tl63_targetValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKillsTarget
						tl63_actualValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKills
						iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedKillsChallengeComplete)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge num ped kills. iEliteChallengeComponentPassed = ", iEliteChallengeComponentPassed)
						SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge num ped kills. bPlayedEliteChallengeStartSound = ", bPlayedEliteChallengeStartSound)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge num ped kills. iOverridePassedSoundFirstEliteChallenge = ", iOverridePassedSoundFirstEliteChallenge)
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH", "CELEB_EL_KLLEN", tl63_targetValue, tl63_actualValue, iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge num ped kills. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
					ENDIF
					
					// 12. Add screens - show noose elite challenge.
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNooseCalled
						iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(NOT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNooseCalled))
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge no noose called. iEliteChallengeComponentPassed = ", iEliteChallengeComponentPassed)
						SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge no noose called. bPlayedEliteChallengeStartSound = ", bPlayedEliteChallengeStartSound)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge no noose called. iOverridePassedSoundFirstEliteChallenge = ", iOverridePassedSoundFirstEliteChallenge)
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH", "CELEB_EL_NOOSE", "", "", iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge no noose called. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
					ENDIF
					
					// 13. Add screens - team health elite challenge.
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHealthDamage
						tl63_targetValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageTarget
						tl63_targetValue += "%"
						tl63_actualValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageActual
						tl63_actualValue += "%"
						iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHealthDamagePercentagePassed)
						strBottomTxt = "CELEB_EL_HTHDM"
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageTarget <= 0
							strBottomTxt = "CELEB_EL_NHTHDM"
						ENDIF
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge team health. iEliteChallengeComponentPassed = ", iEliteChallengeComponentPassed)
						SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge team health. bPlayedEliteChallengeStartSound = ", bPlayedEliteChallengeStartSound)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge team health. iOverridePassedSoundFirstEliteChallenge = ", iOverridePassedSoundFirstEliteChallenge)
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH", strBottomTxt, tl63_targetValue, tl63_actualValue, iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge team health. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
					ENDIF
					
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentLivesLost
						iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumLivesLostChallengeComplete)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge lives lost. iEliteChallengeComponentPassed = ", iEliteChallengeComponentPassed)
						SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge lives lost. bPlayedEliteChallengeStartSound = ", bPlayedEliteChallengeStartSound)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge lives lost. iOverridePassedSoundFirstEliteChallenge = ", iOverridePassedSoundFirstEliteChallenge)
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH", "CELEB_EL_NODIE", "", "", iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge lives lost. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
					ENDIF
					
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHackFailed
						tl63_targetValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailedTarget
						tl63_actualValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailed
						iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumHackFailedChallengeComplete)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge no hack fails. iEliteChallengeComponentPassed = ", iEliteChallengeComponentPassed)
						SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge no hack fails. bPlayedEliteChallengeStartSound = ", bPlayedEliteChallengeStartSound)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge no hack fails. iOverridePassedSoundFirstEliteChallenge = ", iOverridePassedSoundFirstEliteChallenge)
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH", "CELEB_EL_HCKF", tl63_targetValue, tl63_actualValue, iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge no hack fails. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
					ENDIF
					
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedHeadshots
						tl63_targetValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshotsTarget
						tl63_actualValue = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshots
						iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedHeadshotsChallengeComplete)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge num ped headshots. iEliteChallengeComponentPassed = ", iEliteChallengeComponentPassed)
						SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge num ped headshots. bPlayedEliteChallengeStartSound = ", bPlayedEliteChallengeStartSound)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge num ped headshots. iOverridePassedSoundFirstEliteChallenge = ", iOverridePassedSoundFirstEliteChallenge)
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH", "CELEB_EL_HSHT", tl63_targetValue, tl63_actualValue, iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge num ped headshots. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
					ENDIF
					
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentLootBagFull
						iEliteChallengeComponentPassed = GET_ELITE_CHALLENGE_COMPONENT_RESULT_AS_VALID_INT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bLootBagFullChallengeComplete)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge loot bag full. iEliteChallengeComponentPassed = ", iEliteChallengeComponentPassed)
						SET_CHALLENGE_SOUND_ID(bPlayedEliteChallengeStartSound, iOverridePassedSoundFirstEliteChallenge)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge loot bag full. bPlayedEliteChallengeStartSound = ", bPlayedEliteChallengeStartSound)
						PRINTLN("[NETCELEBRATION] - [SAC] - [ELITE_SOUND] - elite challenge loot bag full. iOverridePassedSoundFirstEliteChallenge = ", iOverridePassedSoundFirstEliteChallenge)
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_ELITE_CH", "CELEB_EL_BAG", "", "", iEliteChallengeComponentPassed, -1, iOverridePassedSoundFirstEliteChallenge)
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge loot bag full. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
					ENDIF
					
					// 14. Add screens - elite challenge result.
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumEliteChallengeComponents != 0
						iPreviousValue = iCurrentValue
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete
							iCurrentValue = iPreviousValue + g_TransitionSessionNonResetVars.sGlobalCelebrationData.iEliteChallengeBonusValue
						ENDIF
						ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), GET_ELITE_CHALLENGE_RESULT_TOP_TEXT_LABEL(), "", "", "", 3, -1, GET_CHALLENGE_OVERALL_RESULT_SOUND_ID(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete))
						//3, -1, GET_CHALLENGE_OVERALL_RESULT_SOUND_ID(g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete))
						iIncrementCashPauseTime += 3000
						sCelebrationData.iEstimatedScreenDuration += 3000
						PRINTLN("[NETCELEBRATION] - [SAC] - added elite challenge complete. previous value = ", iPreviousValue, ", current value = ", iCurrentValue, ", complete = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete)
					ENDIF
					
				ENDIF
				
			ENDIF
			
			// 15. Add screens - flow bonus, first time.
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstTimeBonusShouldBeShown
				iPreviousValue = iCurrentValue
				iCurrentValue = (iPreviousValue + g_TransitionSessionNonResetVars.sGlobalCelebrationData.iFirstTimeBonusValue)
				ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_FB_1_TM", "CELEB_FB_1_TM2", "", "", 3, -1, 3)
				iIncrementCashPauseTime += 3000
				sCelebrationData.iEstimatedScreenDuration += 3000
				PRINTLN("[NETCELEBRATION] - [SAC] - added first time bonus. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
			ENDIF
			
			// 16. Add screens - flow bonus, order bonus.
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOrderBonusShouldBeShown
				iPreviousValue = iCurrentValue
				iCurrentValue = (iPreviousValue + g_TransitionSessionNonResetVars.sGlobalCelebrationData.iOrderBonusValue)
				ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_FB_ORDER", "CELEB_FB_ORD2", "", "", 3, -1, 3)
				iIncrementCashPauseTime += 3000
				sCelebrationData.iEstimatedScreenDuration += 3000
				PRINTLN("[NETCELEBRATION] - [SAC] - added order bonus. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
			ENDIF
			
			// 17. Add screens - flow bonus, same team bonus.
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bSameTeamBonusShouldBeShown
				iPreviousValue = iCurrentValue
				iCurrentValue = (iPreviousValue + g_TransitionSessionNonResetVars.sGlobalCelebrationData.iSameTeamBonusValue)
				ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_FB_SMTM", "CELEB_FB_SMTM2", "", "", 3, -1, 3)
				iIncrementCashPauseTime += 3000
				sCelebrationData.iEstimatedScreenDuration += 3000
				PRINTLN("[NETCELEBRATION] - [SAC] - added same team bonus. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
			ENDIF
			
			// 18. Add screens - flow bonus, ultimate bonus.
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bUltimateBonusShouldBeShown
				iPreviousValue = iCurrentValue
				iCurrentValue = (iPreviousValue + g_TransitionSessionNonResetVars.sGlobalCelebrationData.iUltimateBonusValue)
				ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_FB_ULT", "CELEB_FB_ULT2", "", "", 3, -1, 3)
				iIncrementCashPauseTime += 3000
				sCelebrationData.iEstimatedScreenDuration += 3000
				PRINTLN("[NETCELEBRATION] - [SAC] - added ultimate bonus. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
			ENDIF
			
			// 19. Add screens - one off heist member completion bonus.
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMemberBonusShouldBeShown
				iPreviousValue = iCurrentValue
				iCurrentValue = (iPreviousValue + g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMemberBonusValue)
				ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_B_MEM1", "CELEB_B_MEM2", "", "", 3, -1, 3)
				iIncrementCashPauseTime += 3000
				sCelebrationData.iEstimatedScreenDuration += 3000
				PRINTLN("[NETCELEBRATION] - [SAC] - added member bonus. previous value = ", iPreviousValue, ", current value = ", iCurrentValue)
			ENDIF
			
			// 20. Add screens - first person bonus.
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstPersonBonusShouldBeShown
				iPreviousValue = iCurrentValue
				iCurrentValue = (iPreviousValue + g_TransitionSessionNonResetVars.sGlobalCelebrationData.iFirstPersonBonusValue)
				ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), "CELEB_FP_CHAL", "CELEB_FP_CHAL_1", "", "", 3)
				iIncrementCashPauseTime += 3000
				sCelebrationData.iEstimatedScreenDuration += 3000
				PRINTLN("[NETCELEBRATION] - [Stand Alone Celebrations] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for flow bonus layer.")
			ENDIF
			
			//GANG OPS REWARDS
			
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			
				//All In Order II
				IF IS_BIT_SET(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iGangopsAwardBS, ENUM_TO_INT(MPPLY_AWD_GANGOPS_ALLINORDER_INDEX))
					iPreviousValue = iCurrentValue
					iCurrentValue = (iPreviousValue + GET_AWARD_VALUE_FOR_GANGOPS_AWARD(MPPLY_AWD_GANGOPS_ALLINORDER_INDEX))
					ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), GET_GANGOPS_AWARD_TEXT_LABEL(MPPLY_AWD_GANGOPS_ALLINORDER_INDEX), GET_GANGOPS_AWARD_DESCRIPTION_TEXT_LABEL(MPPLY_AWD_GANGOPS_ALLINORDER_INDEX), "", "", 3)
					iIncrementCashPauseTime += 3000
					sCelebrationData.iEstimatedScreenDuration += 3000
					PRINTLN("[NETCELEBRATION] - [Stand Alone Celebrations] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for MPPLY_AWD_GANGOPS_ALLINORDER_INDEX.")
				ENDIF
				
				//Loyalty II
				IF IS_BIT_SET(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iGangopsAwardBS, ENUM_TO_INT(MPPLY_AWD_GANGOPS_LOYALTY2_INDEX))
					iPreviousValue = iCurrentValue
					iCurrentValue = (iPreviousValue + GET_AWARD_VALUE_FOR_GANGOPS_AWARD(MPPLY_AWD_GANGOPS_LOYALTY2_INDEX))
					ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), GET_GANGOPS_AWARD_TEXT_LABEL(MPPLY_AWD_GANGOPS_LOYALTY2_INDEX), GET_GANGOPS_AWARD_DESCRIPTION_TEXT_LABEL(MPPLY_AWD_GANGOPS_LOYALTY2_INDEX), "", "", 3)
					iIncrementCashPauseTime += 3000
					sCelebrationData.iEstimatedScreenDuration += 3000
					PRINTLN("[NETCELEBRATION] - [Stand Alone Celebrations] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for MPPLY_AWD_GANGOPS_LOYALTY2_INDEX.")
				ENDIF
				
				//Loyalty III
				IF IS_BIT_SET(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iGangopsAwardBS, ENUM_TO_INT(MPPLY_AWD_GANGOPS_LOYALTY3_INDEX))
					iPreviousValue = iCurrentValue
					iCurrentValue = (iPreviousValue + GET_AWARD_VALUE_FOR_GANGOPS_AWARD(MPPLY_AWD_GANGOPS_LOYALTY3_INDEX))
					ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), GET_GANGOPS_AWARD_TEXT_LABEL(MPPLY_AWD_GANGOPS_LOYALTY3_INDEX), GET_GANGOPS_AWARD_DESCRIPTION_TEXT_LABEL(MPPLY_AWD_GANGOPS_LOYALTY3_INDEX), "", "", 3)
					iIncrementCashPauseTime += 3000
					sCelebrationData.iEstimatedScreenDuration += 3000
					PRINTLN("[NETCELEBRATION] - [Stand Alone Celebrations] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for MPPLY_AWD_GANGOPS_LOYALTY3_INDEX.")
				ENDIF
				
				//Loyalty IV
				IF IS_BIT_SET(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iGangopsAwardBS, ENUM_TO_INT(MPPLY_AWD_GANGOPS_LOYALTY_INDEX))
					iPreviousValue = iCurrentValue
					iCurrentValue = (iPreviousValue + GET_AWARD_VALUE_FOR_GANGOPS_AWARD(MPPLY_AWD_GANGOPS_LOYALTY_INDEX))
					ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), GET_GANGOPS_AWARD_TEXT_LABEL(MPPLY_AWD_GANGOPS_LOYALTY_INDEX), GET_GANGOPS_AWARD_DESCRIPTION_TEXT_LABEL(MPPLY_AWD_GANGOPS_LOYALTY_INDEX), "", "", 3)
					iIncrementCashPauseTime += 3000
					sCelebrationData.iEstimatedScreenDuration += 3000
					PRINTLN("[NETCELEBRATION] - [Stand Alone Celebrations] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for MPPLY_AWD_GANGOPS_LOYALTY_INDEX.")
				ENDIF
				
				//Criminal Mastermind II
				IF IS_BIT_SET(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iGangopsAwardBS, ENUM_TO_INT(MPPLY_AWD_GANGOPS_CRIMMASMD2_INDEX))
					iPreviousValue = iCurrentValue
					iCurrentValue = (iPreviousValue + GET_AWARD_VALUE_FOR_GANGOPS_AWARD(MPPLY_AWD_GANGOPS_CRIMMASMD2_INDEX))
					ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), GET_GANGOPS_AWARD_TEXT_LABEL(MPPLY_AWD_GANGOPS_CRIMMASMD2_INDEX), GET_GANGOPS_AWARD_DESCRIPTION_TEXT_LABEL(MPPLY_AWD_GANGOPS_CRIMMASMD2_INDEX), "", "", 3)
					iIncrementCashPauseTime += 3000
					sCelebrationData.iEstimatedScreenDuration += 3000
					PRINTLN("[NETCELEBRATION] - [Stand Alone Celebrations] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for MPPLY_AWD_GANGOPS_CRIMMASMD2_INDEX.")
				ENDIF
				
				//Criminal Mastermind III
				IF IS_BIT_SET(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iGangopsAwardBS, ENUM_TO_INT(MPPLY_AWD_GANGOPS_CRIMMASMD3_INDEX))
					iPreviousValue = iCurrentValue
					iCurrentValue = (iPreviousValue + GET_AWARD_VALUE_FOR_GANGOPS_AWARD(MPPLY_AWD_GANGOPS_CRIMMASMD3_INDEX))
					ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), GET_GANGOPS_AWARD_TEXT_LABEL(MPPLY_AWD_GANGOPS_CRIMMASMD3_INDEX), GET_GANGOPS_AWARD_DESCRIPTION_TEXT_LABEL(MPPLY_AWD_GANGOPS_CRIMMASMD3_INDEX), "", "", 3)
					iIncrementCashPauseTime += 3000
					sCelebrationData.iEstimatedScreenDuration += 3000
					PRINTLN("[NETCELEBRATION] - [Stand Alone Celebrations] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for MPPLY_AWD_GANGOPS_CRIMMASMD3_INDEX.")
				ENDIF
				
				//Criminal Mastermind IV
				IF IS_BIT_SET(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iGangopsAwardBS, ENUM_TO_INT(MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX))
					iPreviousValue = iCurrentValue
					iCurrentValue = (iPreviousValue + GET_AWARD_VALUE_FOR_GANGOPS_AWARD(MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX))
					ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), GET_GANGOPS_AWARD_TEXT_LABEL(MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX), GET_GANGOPS_AWARD_DESCRIPTION_TEXT_LABEL(MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX), "", "", 3)
					iIncrementCashPauseTime += 3000
					sCelebrationData.iEstimatedScreenDuration += 3000
					PRINTLN("[NETCELEBRATION] - [Stand Alone Celebrations] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX.")
				ENDIF
				
				//Supporting Role II
				IF IS_BIT_SET(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iGangopsAwardBS, ENUM_TO_INT(MPPLY_AWD_GANGOPS_SUPPORT_INDEX))
					iPreviousValue = iCurrentValue
					iCurrentValue = (iPreviousValue + GET_AWARD_VALUE_FOR_GANGOPS_AWARD(MPPLY_AWD_GANGOPS_SUPPORT_INDEX))
					ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iPreviousValue), TO_FLOAT(iCurrentValue), GET_GANGOPS_AWARD_TEXT_LABEL(MPPLY_AWD_GANGOPS_SUPPORT_INDEX), GET_GANGOPS_AWARD_DESCRIPTION_TEXT_LABEL(MPPLY_AWD_GANGOPS_SUPPORT_INDEX), "", "", 3)
					iIncrementCashPauseTime += 3000
					sCelebrationData.iEstimatedScreenDuration += 3000
					PRINTLN("[NETCELEBRATION] - [Stand Alone Celebrations] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX.")
				ENDIF
			ENDIF
			
			// 21. Add screens - total cash earned.
			ADD_INCREMENTAL_CASH_STEP(sCelebrationData, TO_FLOAT(iCurrentValue), TO_FLOAT(iCurrentValue), "CELEB_TOTAL_EARNED", "", "", "", 3, -1, 7)
			iIncrementCashPauseTime += 3333
			sCelebrationData.iEstimatedScreenDuration += 3333
			PRINTLN("[NETCELEBRATION] - [SAC] - added total cash earned. iCurrentValue value = ", iCurrentValue, ", current value = ", iCurrentValue)
			
			// 22. Add incremental cash to wall.
			ADD_INCREMENTAL_CASH_TO_WALL(sCelebrationData)
			CELEBRATION_ADD_PAUSE_TO_SCREEN(sCelebrationData, "SUMMARY", iIncrementCashPauseTime)
			PRINTLN("[NETCELEBRATION] - [SAC] - added cash to screen.")
			
			// 23. Add job points.
			HEIST_CELEBRATION_ADD_JOB_POINTS_LAYER(sCelebrationData, strScreenName)
			sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - added jobs points to screen.")
			
			// Add RP.
			HEIST_CELEBRATION_ADD_RP_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			PRINTLN("[NETCELEBRATION] - [SAC] - added RP to screen.")
			
		BREAK
		
		CASE eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_LEADER_FAIL
			
			HEIST_CELEBRATION_ADD_RESULTS_LAYER(sCelebrationData, strScreenName, eJobType, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission, g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason, strLiteralFail, strLiteralFail2)
			sCelebrationData.iEstimatedScreenDuration = (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for results layer.")
			
			HEIST_CELEBRATION_ADD_RP_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_PAUSE_FOR_LAYER_BEFORE_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			sCelebrationData.iEstimatedScreenDuration += (6000 + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for final layer.")
			
		BREAK
		
		CASE eADD_HEIST_CELEBRATION_DATA_CATEGORY_HEIST_FINALE_NOT_LEADER_FAIL
			
			HEIST_CELEBRATION_ADD_RESULTS_LAYER(sCelebrationData, strScreenName, eJobType, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission, g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason, strLiteralFail, strLiteralFail2)
			sCelebrationData.iEstimatedScreenDuration = (CELEBRATION_SCREEN_STAT_DISPLAY_TIME + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for results layer.")
			
			HEIST_CELEBRATION_ADD_RP_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			HEIST_CELEBRATION_ADD_PAUSE_FOR_LAYER_BEFORE_FINAL_FAKE_LAYER(sCelebrationData, strScreenName)
			sCelebrationData.iEstimatedScreenDuration += (6000 + CELEBRATION_SCREEN_STAT_WIPE_TIME)
			PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for final layer.")
			
		BREAK
		
	ENDSWITCH
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission
		sCelebrationData.iEstimatedScreenDuration += 16000
		PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for transitions between layers etc.")
	ELSE
		sCelebrationData.iEstimatedScreenDuration += 5000
		PRINTLN("[NETCELEBRATION] - [SAC] - [ADD AND PAUSE] - updated sCelebrationData.iEstimatedScreenDuration to ", sCelebrationData.iEstimatedScreenDuration, " for transitions between layers etc.")
	ENDIF
	
	PRINTLN("[NETCELEBRATION] - [SAC] - total movie esimated time = ", sCelebrationData.iEstimatedScreenDuration)
	
ENDPROC


/// PURPOSE:
///    Decides if we should play a full body winner anim. Call this once and store the value for use in UPDATE_CELEBRATION_WINNER_ANIMS.
///    The general rule is only play a full body anim if the winner is alone, and the player picked a suitable anim.
FUNC BOOL SHOULD_WINNER_SCREEN_USE_FULL_BODY_ANIM(CELEBRATION_SCREEN_DATA &sCelebrationData, INT iWinnerIndex)
	IF IS_PED_INJURED(sCelebrationData.pedWinnerClones[1])
	AND iWinnerIndex >= 0
		IF GlobalplayerBD[iWinnerIndex].iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER)
			IF GlobalplayerBD[iWinnerIndex].iInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_FINGER)
			OR GlobalplayerBD[iWinnerIndex].iInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_SALUTE)
			OR GlobalplayerBD[iWinnerIndex].iInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_ROCK)
			OR GlobalplayerBD[iWinnerIndex].iInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_WANK)
				PRINTLN("[NETCELEBRATION] - SHOULD_WINNER_SCREEN_USE_FULL_BODY_ANIM: Player is suitable for full body anim.")
				
				//IF GET_RANDOM_INT_IN_RANGE(0, 3) = 1
				//	PRINTLN("[NETCELEBRATION] - SHOULD_WINNER_SCREEN_USE_FULL_BODY_ANIM: Player passed random check.")
				
					RETURN TRUE
				//ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PLAY_KNUCKLE_CRUNCH_AUDIO(CELEBRATION_SCREEN_DATA &sCelebrationData, PED_INDEX &pedWinnerClone)
	
	// Stop the compiler bitching.
	UNUSED_PARAMETER(pedWinnerClone)
	
	// If the celebration is running.
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sCelebrationData.iwinnerSyncedSceneId)
		
		PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - scene playing. Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
		
		FLOAT fPhase = GET_SYNCHRONIZED_SCENE_PHASE(sCelebrationData.iwinnerSyncedSceneId)
		
		// Make sure we have a ped to play the sound from.
		IF NOT DOES_ENTITY_EXIST(pedWinnerClone)
			PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - pedWinnerClone does not exist. Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
			EXIT
		ENDIF
		
		IF IS_ENTITY_DEAD(pedWinnerClone)
			PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - pedWinnerClone is dead. Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
			EXIT
		ENDIF
		
		// Get a sound Id for the knuckle crunching.
		IF sCelebrationData.iKnuckAudioSoundId = (-1)
			sCelebrationData.iKnuckAudioSoundId = GET_SOUND_ID()
		ENDIF
		
		// If the ped in the scene is female.
		IF NOT IS_PED_MALE(pedWinnerClone)
			
			PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - pedWinnerClone is female. Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
			
			// Play sounds at each point during the anim as required.
			IF fPhase >= 0.622
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 0)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Slap_Cel", "MP_SNACKS_SOUNDSET")
//					PLAY_SOUND_FROM_ENTITY(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Slap", pedWinnerClone, "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 0)
					PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - played Knuckle_Crack_Slap_Cel, phase >= 0.622, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
				ENDIF
			ELIF fPhase >= 0.495
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 1)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Slap_Cel", "MP_SNACKS_SOUNDSET")
//					PLAY_SOUND_FROM_ENTITY(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Slap", pedWinnerClone, "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 1)
					PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - played Knuckle_Crack_Slap_Cel, phase >= 0.495, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
				ENDIF
			ELIF fPhase >= 0.333
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 2)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard_Cel", "MP_SNACKS_SOUNDSET")
//					PLAY_SOUND_FROM_ENTITY(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard", pedWinnerClone, "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 2)
					PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - played Knuckle_Crack_Hard_Cel, phase >= 0.333, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
				ENDIF
			ELIF fPhase >= 0.162
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 3)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard_Cel", "MP_SNACKS_SOUNDSET")
//					PLAY_SOUND_FROM_ENTITY(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard", pedWinnerClone, "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 3)
					PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - played Knuckle_Crack_Hard_Cel, phase >= 0.162, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
				ENDIF
			ELIF fPhase >= 0.131
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 4)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Slap_Cel", "MP_SNACKS_SOUNDSET")
//					PLAY_SOUND_FROM_ENTITY(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Slap", pedWinnerClone, "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 4)
					PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - played Knuckle_Crack_Slap_Cel, phase >= 0.131, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
				ENDIF
			ENDIF
			
		// If the ped is male.
		ELSE
			
			PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - pedWinnerClone is male. Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
			
			// Play sounds at each point during the anim as required.
			IF fPhase >= 0.595
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 0)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard_Cel", "MP_SNACKS_SOUNDSET")
//					PLAY_SOUND_FROM_ENTITY(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard", pedWinnerClone, "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 0)
					PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - played Knuckle_Crack_Hard_Cel, phase >= 0.595, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
				ENDIF
			ELIF fPhase >= 0.445
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 1)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard_Cel", "MP_SNACKS_SOUNDSET")
//					PLAY_SOUND_FROM_ENTITY(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard", pedWinnerClone, "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 1)
					PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - played Knuckle_Crack_Hard_Cel, phase >= 0.445, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
				ENDIF
			ELIF fPhase >= 0.282
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 2)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard_Cel", "MP_SNACKS_SOUNDSET")
//					PLAY_SOUND_FROM_ENTITY(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard", pedWinnerClone, "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 2)
					PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - played Knuckle_Crack_Hard_Cel, phase >= 0.282, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
				ENDIF
			ELIF fPhase >= 0.141
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 3)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard_Cel", "MP_SNACKS_SOUNDSET")
//					PLAY_SOUND_FROM_ENTITY(sCelebrationData.iKnuckAudioSoundId, "Knuckle_Crack_Hard", pedWinnerClone, "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 3)
					PRINTLN("[Celebrations] - PLAY_KNUCKLE_CRUNCH_AUDIO - played Knuckle_Crack_Hard_Cel, phase >= 0.141, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PLAY_SLOW_CLAP_AUDIO(CELEBRATION_SCREEN_DATA &sCelebrationData, PED_INDEX &pedWinnerClone)
	
	// Stop the compiler bitching.
	UNUSED_PARAMETER(pedWinnerClone)
	
	// If the celebration is running.
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sCelebrationData.iwinnerSyncedSceneId)
		
		FLOAT fPhase = GET_SYNCHRONIZED_SCENE_PHASE(sCelebrationData.iwinnerSyncedSceneId)
		
		PRINTLN("[Celebrations] - PLAY_SLOW_CLAP_AUDIO - scene playing. Scene ID = ", sCelebrationData.iwinnerSyncedSceneId, ". Phase = ", fPhase)
		
		// Make sure we have a ped to play the sound from.
		IF NOT DOES_ENTITY_EXIST(pedWinnerClone)
			PRINTLN("[Celebrations] - PLAY_SLOW_CLAP_AUDIO - pedWinnerClone does not exist. Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
			EXIT
		ENDIF
		
		IF IS_ENTITY_DEAD(pedWinnerClone)
			PRINTLN("[Celebrations] - PLAY_SLOW_CLAP_AUDIO - pedWinnerClone is dead. Scene ID = ", sCelebrationData.iwinnerSyncedSceneId)
			EXIT
		ENDIF
		
		// Get a sound Id for the knuckle crunching.
		IF sCelebrationData.iKnuckAudioSoundId = (-1)
			
			PRINTLN("[Celebrations] - PLAY_SLOW_CLAP_AUDIO - Scene ID = ", sCelebrationData.iwinnerSyncedSceneId, ", iKnuckAudioSoundId = -1, grabbing ID.")
			sCelebrationData.iKnuckAudioSoundId = GET_SOUND_ID()
		
		ELSE
			
			PRINTLN("[Celebrations] - PLAY_SLOW_CLAP_AUDIO - Scene ID = ", sCelebrationData.iwinnerSyncedSceneId, ", iKnuckAudioSoundId = ", sCelebrationData.iKnuckAudioSoundId)
			
			// Play sounds at each point during the anim as required.
			IF fPhase >= 0.595
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 0)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Slow_Clap_Cel", "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 0)
					PRINTLN("[Celebrations] - PLAY_SLOW_CLAP_AUDIO - played Slow_Clap_Cel, phase >= 0.595, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId, ", phase = ", fPhase)
				ELSE
					PRINTLN("[Celebrations] - PLAY_SLOW_CLAP_AUDIO - iKnuckleAudioBitset, 0 bit is set. Scene ID = ", sCelebrationData.iwinnerSyncedSceneId, ", phase = ", fPhase)
				ENDIF
			ELIF fPhase >= 0.371
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 1)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Slow_Clap_Cel", "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 1)
					PRINTLN("[Celebrations] - PLAY_SLOW_CLAP_AUDIO - played Slow_Clap_Cel, phase >= 0.371, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId, ", phase = ", fPhase)
				ELSE
					PRINTLN("[Celebrations] - PLAY_SLOW_CLAP_AUDIO - iKnuckleAudioBitset, 1 bit is set. Scene ID = ", sCelebrationData.iwinnerSyncedSceneId, ", phase = ", fPhase)
				ENDIF
			ELIF fPhase >= 0.152
				IF NOT IS_BIT_SET(sCelebrationData.iKnuckleAudioBitset, 2)
					PLAY_SOUND_FRONTEND(sCelebrationData.iKnuckAudioSoundId, "Slow_Clap_Cel", "MP_SNACKS_SOUNDSET")
					SET_BIT(sCelebrationData.iKnuckleAudioBitset, 2)
					PRINTLN("[Celebrations] - PLAY_SLOW_CLAP_AUDIO - played Slow_Clap_Cel, phase >= 0.152, Scene ID = ", sCelebrationData.iwinnerSyncedSceneId, ", phase = ", fPhase)
				ELSE
					PRINTLN("[Celebrations] - PLAY_SLOW_CLAP_AUDIO - iKnuckleAudioBitset, 2 bit is set. Scene ID = ", sCelebrationData.iwinnerSyncedSceneId, ", phase = ", fPhase)
				ENDIF	
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL HAVE_REQUIRED_SOUND_BANKS_LOADED(INT iAnimToPlay, BOOL bdoingRallyRace)
	IF bDoingRallyRace
		RETURN TRUE
	ENDIF
	IF (iAnimToPlay != ENUM_TO_INT(PLAYER_INTERACTION_KNUCKLE_CRUNCH))
	AND (iAnimToPlay != ENUM_TO_INT(PLAYER_INTERACTION_SLOW_CLAP))
		RETURN TRUE
	ENDIF
	RETURN REQUEST_SCRIPT_AUDIO_BANK("DLC_GTAO/SNACKS", FALSE)
ENDFUNC

FUNC BOOL HAS_NG_FAILSAFE_TIMER_EXPIRED(CELEBRATION_SCREEN_DATA &sCelebrationData)
	
	IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.stNgFailSafeTimer)
		START_NET_TIMER(sCelebrationData.stNgFailSafeTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(sCelebrationData.stNgFailSafeTimer, 10000)
			PRINTLN("[Celebrations] - HAS_NG_FAILSAFE_TIMER_EXPIRED = TRUE.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM(INT iAnimToPlay, BOOL bDoingPairedAnim = FALSE)
	
	IF bDoingPairedAnim
		
		PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - bDoingPairedAnim = TRUE.")

		SWITCH iAnimToPlay
			CASE PAIRED_CELEBRATION_ANIM_HIGH_FIVE	
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PAIRED_CELEBRATION_ANIM_HIGH_FIVE, returning TRUE.")
				RETURN TRUE 
			CASE PAIRED_CELEBRATION_ANIM_COWERING	
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PAIRED_CELEBRATION_ANIM_COWERING, returning TRUE.")
				RETURN TRUE 
			CASE PAIRED_CELEBRATION_ANIM_SARCASTIC	
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PAIRED_CELEBRATION_ANIM_SARCASTIC, returning TRUE.")
				RETURN TRUE 
			CASE PAIRED_CELEBRATION_ANIM_BRO_HUG	
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PAIRED_CELEBRATION_ANIM_BRO_HUG, returning TRUE.")
				RETURN TRUE 
		ENDSWITCH
		
	ELSE
		
		PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - bDoingPairedAnim = FALSE.")
		
		SWITCH INT_TO_ENUM(PLAYER_INTERACTIONS, iAnimToPlay)
			CASE PLAYER_INTERACTION_SMOKE				
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PLAYER_INTERACTION_SMOKE, returning TRUE.")
				RETURN TRUE 
			CASE PLAYER_INTERACTION_AIR_GUITAR			
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PLAYER_INTERACTION_AIR_GUITAR, returning TRUE.")
				RETURN TRUE 
			#IF FEATURE_INTERACTIONS_UNKNOWN
			CASE PLAYER_INTERACTION_DANCE				
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PLAYER_INTERACTION_DANCE, returning TRUE.")
				RETURN TRUE 
			CASE PLAYER_INTERACTION_RAVING				
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM -iAnimToPlay = PLAYER_INTERACTION_RAVING, returning TRUE. ")
				RETURN TRUE 
			#ENDIF			
			CASE PLAYER_INTERACTION_WAVE				
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PLAYER_INTERACTION_WAVE, returning TRUE.")
				RETURN TRUE 
			CASE PLAYER_INTERACTION_SURRENDER			
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PLAYER_INTERACTION_SURRENDER, returning TRUE. ")
				RETURN TRUE 
			CASE PLAYER_INTERACTION_SHUSH				
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PLAYER_INTERACTION_SHUSH, returning TRUE.")
				RETURN TRUE 
			CASE PLAYER_INTERACTION_PHOTOGRAPHY			
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PLAYER_INTERACTION_PHOTOGRAPHY, returning TRUE.")
				RETURN TRUE 
			CASE PLAYER_INTERACTION_DJ					
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PLAYER_INTERACTION_DJ, returning TRUE.")
				RETURN TRUE 
			CASE PLAYER_INTERACTION_AIR_SYNTH			
				PRINTLN("[NETCELEBRATION] - DOES_CELEBRATION_NEED_TIME_EXTENSION_FOR_ANIM - iAnimToPlay = PLAYER_INTERACTION_AIR_SYNTH, returning TRUE.")
				RETURN TRUE 
		ENDSWITCH
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC GET_PAIRED_CELEBRATION_ANIM_TO_PLAY(PAIRED_CELEBRATION_GENDER_COMBO eGenderCombo, INT iRallyAnimToPlay, PED_INDEX pedWinnerClone, BOOL bRallyDriver, TEXT_LABEL_63 &tl63_AnimDict, TEXT_LABEL_63 &tl63_AnimName)
	
	// *************************************************************************
	// Note we temporarily only have one anim dict and one male_male 
	// anim to test with. Once more anim dicts and proper gender combinations 
	// are added, they should be added to the logic below.
	//
	// Note females are always on the left.
	//
	// *************************************************************************
	
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - being called.")
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - eGenderCombo = ", eGenderCombo)
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - iRallyAnimToPlay = ", iRallyAnimToPlay)
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - bRallyDriver = ", bRallyDriver)
	
	BOOL bIAmLeftside
	
	SWITCH eGenderCombo
		CASE PAIRED_CELEBRATION_GENDER_COMBO_M_F
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - gender combo = PAIRED_CELEBRATION_GENDER_COMBO_M_F")
			IF IS_PED_MALE(pedWinnerClone)
				bIAmLeftside = FALSE
			ELSE
				bIAmLeftside = TRUE
			ENDIF
		BREAK
		CASE PAIRED_CELEBRATION_GENDER_COMBO_M_M
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - gender combo = PAIRED_CELEBRATION_GENDER_COMBO_M_M")
			IF bRallyDriver
				bIAmLeftside = TRUE
			ELSE
				bIAmLeftside = FALSE
			ENDIF
		BREAK
		CASE PAIRED_CELEBRATION_GENDER_COMBO_F_F
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - gender combo = PAIRED_CELEBRATION_GENDER_COMBO_F_F")
			IF bRallyDriver
				bIAmLeftside = TRUE
			ELSE
				bIAmLeftside = FALSE
			ENDIF
		BREAK
		
	ENDSWITCH

	SWITCH iRallyAnimToPlay
		
		CASE PAIRED_CELEBRATION_ANIM_MANLY_HANDSHAKE
			
			// Get the dictionary, based on combo of genders of two peds in scene.
			SWITCH eGenderCombo
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_M
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@m_m_manly_handshake"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_F
					// male_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_m_manly_handshake"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_F_F
					// female_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_f_manly_handshake"
				BREAK
			ENDSWITCH
			
			// Get anim to play, based on gender of ped.
			IF bIAmLeftside
				tl63_AnimName = "MANLY_HANDSHAKE_LEFT"
			ELSE
				tl63_AnimName = "MANLY_HANDSHAKE_RIGHT"
			ENDIF
			
		BREAK
		
 		CASE PAIRED_CELEBRATION_ANIM_FIST_BUMP
			
			// Get the dictionary, based on combo of genders of two peds in scene.
			SWITCH eGenderCombo
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_M
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@m_m_fist_bump"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_F
					// male_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_m_fist_bump"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_F_F
					// female_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_f_fist_bump"
				BREAK
			ENDSWITCH
			
			// Get anim to play, based on gender of ped.
			IF bIAmLeftside
				tl63_AnimName = "FIST_BUMP_LEFT"
			ELSE
				tl63_AnimName = "FIST_BUMP_RIGHT"
			ENDIF
			
		BREAK
		
 		CASE PAIRED_CELEBRATION_ANIM_BACKSLAP
			
			// Get the dictionary, based on combo of genders of two peds in scene.
			SWITCH eGenderCombo
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_M
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@m_m_backslap"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_F
					// male_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_m_backslap"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_F_F
					// female_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_f_backslap"
				BREAK
			ENDSWITCH
			
			// Get anim to play, based on gender of ped.
			IF bIAmLeftside
				tl63_AnimName = "BACKSLAP_LEFT"
			ELSE
				tl63_AnimName = "BACKSLAP_RIGHT"
			ENDIF
			
		BREAK
		
		CASE PAIRED_CELEBRATION_ANIM_HIGH_FIVE
			
			// Get the dictionary, based on combo of genders of two peds in scene.
			SWITCH eGenderCombo
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_M
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@m_m_high_five"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_F
					// male_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_m_high_five"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_F_F
					// female_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_f_high_five"
				BREAK
			ENDSWITCH
			
			// Get anim to play, based on gender of ped.
			IF bIAmLeftside
				tl63_AnimName = "HIGH_FIVE_LEFT"
			ELSE
				tl63_AnimName = "HIGH_FIVE_RIGHT"
			ENDIF
			
		BREAK
		
		CASE PAIRED_CELEBRATION_ANIM_COWERING
			
			// Get the dictionary, based on combo of genders of two peds in scene.
			SWITCH eGenderCombo
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_M
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@m_m_cowering"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_F
					// male_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_m_cowering"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_F_F
					// female_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_f_cowering"
				BREAK
			ENDSWITCH
			
			// Get anim to play, based on gender of ped.
			IF bIAmLeftside
				tl63_AnimName = "COWERING_LEFT"
			ELSE
				tl63_AnimName = "COWERING_RIGHT"
			ENDIF
			
		BREAK
		
		CASE PAIRED_CELEBRATION_ANIM_SARCASTIC
			
			// Get the dictionary, based on combo of genders of two peds in scene.
			SWITCH eGenderCombo
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_M
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@m_m_sarcastic"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_F
					// male_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_m_sarcastic"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_F_F
					// female_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_f_sarcastic"
				BREAK
			ENDSWITCH
			
			// Get anim to play, based on gender of ped.
			IF bIAmLeftside
				tl63_AnimName = "SARCASTIC_LEFT"
			ELSE
				tl63_AnimName = "SARCASTIC_RIGHT"
			ENDIF
			
		BREAK
		
		CASE PAIRED_CELEBRATION_ANIM_BRO_HUG
			
			// Get the dictionary, based on combo of genders of two peds in scene.
			SWITCH eGenderCombo
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_M
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@m_m_bro_hug"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_M_F
					// male_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_m_bro_hug"
				BREAK
				CASE PAIRED_CELEBRATION_GENDER_COMBO_F_F
					// female_female dict goes here.
					tl63_AnimDict = "anim@mp_player_intcelebrationpaired@f_f_bro_hug"
				BREAK
			ENDSWITCH
			
			// Get anim to play, based on gender of ped.
			IF bIAmLeftside
				tl63_AnimName = "BRO_HUG_LEFT"
			ELSE
				tl63_AnimName = "BRO_HUG_RIGHT"
			ENDIF
			
		BREAK

	ENDSWITCH
		
ENDPROC

PROC GET_CELEBRATION_IDLE_ANIM_TO_USE(CELEBRATION_SCREEN_DATA &sCelebrationData, TEXT_LABEL_63 &tl63_IdleAnimDict, TEXT_LABEL_63 &tl63_IdleAnimName, BOOL bIsPedMale)
	
	IF sCelebrationData.eIdleAnimToUse = CELEBRATION_IDLE_ANIM_INVALID
		IF bIsPedMale
			INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRandom = 0
				sCelebrationData.eIdleAnimToUse = CELEBRATION_IDLE_ANIM_MALE_A
			ELSE
				sCelebrationData.eIdleAnimToUse = CELEBRATION_IDLE_ANIM_MALE_B
			ENDIF
		ELSE
			sCelebrationData.eIdleAnimToUse = CELEBRATION_IDLE_ANIM_FEMALE
		ENDIF
	ENDIF
	
	SWITCH sCelebrationData.eIdleAnimToUse
		CASE CELEBRATION_IDLE_ANIM_MALE_A
			tl63_IdleAnimDict = "MP_CELEBRATION@IDLES@MALE"
			tl63_IdleAnimName = "CELEBRATION_IDLE_M_A"
		BREAK
		CASE CELEBRATION_IDLE_ANIM_MALE_B
			tl63_IdleAnimDict = "MP_CELEBRATION@IDLES@MALE"
			tl63_IdleAnimName = "CELEBRATION_IDLE_M_B"
		BREAK
		CASE CELEBRATION_IDLE_ANIM_FEMALE
			tl63_IdleAnimDict = "MP_CELEBRATION@IDLES@FEMALE"
			tl63_IdleAnimName = "CELEBRATION_IDLE_F_A"
		BREAK
		CASE CELEBRATION_IDLE_ANIM_INVALID
			tl63_IdleAnimDict = ""
			tl63_IdleAnimName = ""
		BREAK
	ENDSWITCH
	
ENDPROC

PROC SET_CUSTOM_PED_MODEL_CELEBRATION_ANIMS(INT iCustomPedModel, TEXT_LABEL_63 &tl63_AnimDict, TEXT_LABEL_63 &tl63_AnimName)
	PRINTLN("SET_CUSTOM_PED_MODEL_CELEBRATION_ANIMS iCustomPedModel = ", iCustomPedModel)
	SWITCH iCustomPedModel
		CASE ciFMMC_PlayerPedModel_Franklin
			tl63_AnimDict = "anim@mp_player_intcelebrationmale@knuckle_crunch"
			tl63_AnimName = "knuckle_crunch"
		BREAK
		CASE ciFMMC_PlayerPedModel_Lamar
			tl63_AnimDict = "anim@mp_player_intcelebrationmale@finger"
			tl63_AnimName = "finger"
		BREAK
	ENDSWITCH	
ENDPROC

PROC GET_CELEBRATION_ANIM_TO_PLAY(INT iAnimToPlay, BOOL bMaleAnim, BOOL bGettingAnimForLamar, TEXT_LABEL_63 &tl63_AnimDict, TEXT_LABEL_63 &tl63_AnimName, INTERACTION_ANIM_TYPES eAnimType = INTERACTION_ANIM_TYPE_PLAYER, 
									CREW_INTERACTIONS eCrewAnimToPlay = CREW_INTERACTION_BRO_LOVE, BOOL bJuggernaut = FALSE, INT iCustomPedModel = -1)
	
	IF bGettingAnimForLamar
		tl63_AnimDict = "anim@mp_player_intcelebrationmale@finger"
		tl63_AnimName = "finger"
		EXIT	
	ENDIF
	
	IF bJuggernaut
		tl63_AnimDict = "ANIM@MP_JUGGERNAUT@"
		tl63_AnimName = "Idle_Intro"
		EXIT	
	ENDIF
	
	IF iCustomPedModel != -1
		SET_CUSTOM_PED_MODEL_CELEBRATION_ANIMS(iCustomPedModel, tl63_AnimDict, tl63_AnimName)
		EXIT
	ENDIF
	
	SWITCH eAnimType
		
		CASE INTERACTION_ANIM_TYPE_CREW 
			
			SWITCH eCrewAnimToPlay
				
				CASE CREW_INTERACTION_BRO_LOVE
					IF bMaleAnim
			            tl63_AnimDict = "anim@mp_player_intcelebrationmale@bro_love"
			            tl63_AnimName = "bro_love"
			        ELSE
			            tl63_AnimDict = "anim@mp_player_intcelebrationfemale@bro_love"
			            tl63_AnimName = "bro_love"
			        ENDIF
				BREAK
				
				CASE CREW_INTERACTION_FINGER
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@finger"
						tl63_AnimName = "finger"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@finger"
						tl63_AnimName = "finger"
					ENDIF
				BREAK
				
				CASE CREW_INTERACTION_WANK
					tl63_AnimDict = "anim@mp_player_intcelebrationmale@wank"
					tl63_AnimName = "wank"
				BREAK
						
				CASE CREW_INTERACTION_UP_YOURS
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@finger"
						tl63_AnimName = "finger"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@finger"
						tl63_AnimName = "finger"
					ENDIF
				BREAK
				
			ENDSWITCH
			
		BREAK
		
		CASE INTERACTION_ANIM_TYPE_PLAYER
			
			SWITCH INT_TO_ENUM(PLAYER_INTERACTIONS, iAnimToPlay)
				
				CASE PLAYER_INTERACTION_BLOW_KISS
			        IF bMaleAnim
			            tl63_AnimDict = "anim@mp_player_intcelebrationmale@blow_kiss"
			            tl63_AnimName = "blow_kiss"
			        ELSE
			            tl63_AnimDict = "anim@mp_player_intcelebrationfemale@blow_kiss"
			            tl63_AnimName = "blow_kiss"
			        ENDIF
			    BREAK
				CASE PLAYER_INTERACTION_FINGER		
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@finger"
						tl63_AnimName = "finger"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@finger"
						tl63_AnimName = "finger"
					ENDIF
				BREAK
				// These three don't have female variants.
				CASE PLAYER_INTERACTION_ROCK			
					tl63_AnimDict = "anim@mp_player_intcelebrationmale@rock"
					tl63_AnimName = "rock"
				BREAK
				CASE PLAYER_INTERACTION_SALUTE			
					tl63_AnimDict = "anim@mp_player_intcelebrationmale@salute"
					tl63_AnimName = "salute"
				BREAK
				CASE PLAYER_INTERACTION_WANK			
					tl63_AnimDict = "anim@mp_player_intcelebrationmale@wank"
					tl63_AnimName = "wank"
				BREAK
				CASE PLAYER_INTERACTION_AIR_SHAGGING
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@air_shagging"
						tl63_AnimName = "air_shagging"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@air_shagging"
						tl63_AnimName = "air_shagging"
					ENDIF
				BREAK	
				CASE PLAYER_INTERACTION_DOCK
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@dock"
						tl63_AnimName = "dock"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@dock"
						tl63_AnimName = "dock"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_KNUCKLE_CRUNCH	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@knuckle_crunch"
						tl63_AnimName = "knuckle_crunch"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@knuckle_crunch"
						tl63_AnimName = "knuckle_crunch"
					ENDIF
				BREAK

			// Business Pack 2
				CASE PLAYER_INTERACTION_SLOW_CLAP	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@slow_clap"
						tl63_AnimName = "slow_clap"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@slow_clap"
						tl63_AnimName = "slow_clap"
					ENDIF
				BREAK	
				CASE PLAYER_INTERACTION_FACE_PALM
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@face_palm"
						tl63_AnimName = "face_palm"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@face_palm"
						tl63_AnimName = "face_palm"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_THUMBS_UP	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@thumbs_up"
						tl63_AnimName = "thumbs_up"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@thumbs_up"
						tl63_AnimName = "thumbs_up"
					ENDIF
				BREAK	
				CASE PLAYER_INTERACTION_JAZZ_HANDS	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@jazz_hands"
						tl63_AnimName = "jazz_hands"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@jazz_hands"
						tl63_AnimName = "jazz_hands"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_NOSE_PICK	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@nose_pick"
						tl63_AnimName = "nose_pick"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@nose_pick"
						tl63_AnimName = "nose_pick"
					ENDIF
				BREAK	

			// Hipster
				CASE PLAYER_INTERACTION_SMOKE
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@smoke_flick"
						tl63_AnimName = "smoke_flick"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@smoke_flick"
						tl63_AnimName = "smoke_flick"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_AIR_GUITAR
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@air_guitar"
						tl63_AnimName = "air_guitar"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@air_guitar"
						tl63_AnimName = "air_guitar"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_WAVE	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@wave"
						tl63_AnimName = "wave"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@wave"
						tl63_AnimName = "wave"
					ENDIF
				BREAK
				
				CASE PLAYER_INTERACTION_SURRENDER	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@surrender"
						tl63_AnimName = "surrender"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@surrender"
						tl63_AnimName = "surrender"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_SHUSH	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@shush"
						tl63_AnimName = "shush"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@shush"
						tl63_AnimName = "shush"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_PHOTOGRAPHY	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@photography"
						tl63_AnimName = "photography"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@photography"
						tl63_AnimName = "photography"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_DJ	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@dj"
						tl63_AnimName = "dj"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@dj"
						tl63_AnimName = "dj"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_AIR_SYNTH	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@air_synth"
						tl63_AnimName = "air_synth"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@air_synth"
						tl63_AnimName = "air_synth"
					ENDIF
				BREAK
			
				CASE PLAYER_INTERACTION_NO_WAY
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@no_way"
						tl63_AnimName = "no_way"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@no_way"
						tl63_AnimName = "no_way"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_CHICKEN_TAUNT
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@chicken_taunt"
						tl63_AnimName = "chicken_taunt"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@chicken_taunt"
						tl63_AnimName = "chicken_taunt"
					ENDIF
				BREAK   			
				CASE PLAYER_INTERACTION_CHIN_BRUSH
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@chin_brush"
						tl63_AnimName = "chin_brush"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@chin_brush"
						tl63_AnimName = "chin_brush"
					ENDIF
				BREAK	 					
			
			// Lowrider2
				CASE PLAYER_INTERACTION_YOU_LOCO 
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@you_loco"
						tl63_AnimName = "you_loco"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@you_loco"
						tl63_AnimName = "you_loco"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_FINGER_KISS		
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@finger_kiss"
						tl63_AnimName = "finger_kiss"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@finger_kiss"
						tl63_AnimName = "finger_kiss"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_PEACE
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@peace"
						tl63_AnimName = "peace"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@peace"
						tl63_AnimName = "peace"
					ENDIF
				BREAK
			
			// halloween
				CASE PLAYER_INTERACTION_FREAKOUT	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@freakout"
						tl63_AnimName = "freakout"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@freakout"
						tl63_AnimName = "freakout"
					ENDIF
				BREAK  				
				CASE PLAYER_INTERACTION_THUMB_ON_EARS	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@thumb_on_ears"
						tl63_AnimName = "thumb_on_ears"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@thumb_on_ears"
						tl63_AnimName = "thumb_on_ears"
					ENDIF
				BREAK
			
			// sports
			#IF FEATURE_INTERACTIONS_SPORTS
				CASE PLAYER_INTERACTION_BRO_LOVE
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@bro_love"
						tl63_AnimName = "bro_love"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@bro_love"
						tl63_AnimName = "bro_love"
					ENDIF
				BREAK						
				CASE PLAYER_INTERACTION_FAKE_HORSE
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@fake_horse"
						tl63_AnimName = "fake_horse"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@fake_horse"
						tl63_AnimName = "fake_horse"
					ENDIF
				BREAK					
				CASE PLAYER_INTERACTION_LOSER		
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@loser"
						tl63_AnimName = "loser"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@loser"
						tl63_AnimName = "loser"
					ENDIF
				BREAK     					
				CASE PLAYER_INTERACTION_OK		
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@ok"
						tl63_AnimName = "ok"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@ok"
						tl63_AnimName = "ok"
					ENDIF
				BREAK		
				CASE PLAYER_INTERACTION_FISHING		
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@fishing"
						tl63_AnimName = "fishing"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@fishing"
						tl63_AnimName = "fishing"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_THUMB_ON_NOSE	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@thumb_on_nose"
						tl63_AnimName = "thumb_on_nose"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@thumb_on_nose"
						tl63_AnimName = "thumb_on_nose"
					ENDIF
				BREAK	
			#ENDIF

			// army
			#IF FEATURE_INTERACTIONS_ARMY
				CASE PLAYER_INTERACTION_BICEP_FLEX
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@bicep_flex"
						tl63_AnimName = "bicep_flex"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@bicep_flex"
						tl63_AnimName = "bicep_flex"
					ENDIF
				BREAK					
				CASE PLAYER_INTERACTION_COWBOY_GUN
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@cowboy_gun"
						tl63_AnimName = "cowboy_gun"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@cowboy_gun"
						tl63_AnimName = "cowboy_gun"
					ENDIF
				BREAK	 					
				CASE PLAYER_INTERACTION_FIST_PUMP	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@fist_pump"
						tl63_AnimName = "fist_pump"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@fist_pump"
						tl63_AnimName = "fist_pump"
					ENDIF
				BREAK    			
				CASE PLAYER_INTERACTION_GUN_HEAD	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@gun_head"
						tl63_AnimName = "gun_head"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@gun_head"
						tl63_AnimName = "gun_head"
					ENDIF
				BREAK        			
				CASE PLAYER_INTERACTION_PHWAOR		
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@phwaor"
						tl63_AnimName = "phwaor"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@phwaor"
						tl63_AnimName = "phwaor"
					ENDIF
				BREAK	   						
				CASE PLAYER_INTERACTION_PRETEND_CRYING	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@pretend_crying"
						tl63_AnimName = "pretend_crying"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@pretend_crying"
						tl63_AnimName = "pretend_crying"
					ENDIF
				BREAK 				
				CASE PLAYER_INTERACTION_SLIT_THROAT	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@slit_throat"
						tl63_AnimName = "slit_throat"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@slit_throat"
						tl63_AnimName = "slit_throat"
					ENDIF
				BREAK					
				CASE PLAYER_INTERACTION_UP_YOURS	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@up_yours"
						tl63_AnimName = "up_yours"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@up_yours"
						tl63_AnimName = "up_yours"
					ENDIF
				BREAK  					
				CASE PLAYER_INTERACTION_WATCHING_YOU	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@watching_you"
						tl63_AnimName = "watching_you"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@watching_you"
						tl63_AnimName = "watching_you"
					ENDIF
				BREAK	
				CASE PLAYER_INTERACTION_SALUTE_2	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@salute_v2"
						tl63_AnimName = "salute_v2"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@salute_v2"
						tl63_AnimName = "salute_v2"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_SALUTE_3	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@salute_v3"
						tl63_AnimName = "salute_v3"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@salute_v3"
						tl63_AnimName = "salute_v3"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_SALUTE_4	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@salute_v4"
						tl63_AnimName = "salute_v4"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@salute_v4"
						tl63_AnimName = "salute_v4"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_SALUTE_5	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@salute_v5"
						tl63_AnimName = "salute_v5"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@salute_v5"
						tl63_AnimName = "salute_v5"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_SALUTE_6	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@salute_v6"
						tl63_AnimName = "salute_v6"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@salute_v6"
						tl63_AnimName = "salute_v6"
					ENDIF
				BREAK
			#ENDIF
			#IF FEATURE_INTERACTIONS_UNKNOWN
				CASE PLAYER_INTERACTION_DANCE
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@dance"
						tl63_AnimName = "dance"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@dance"
						tl63_AnimName = "dance"
					ENDIF
				BREAK											
				CASE PLAYER_INTERACTION_RAVING		
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@raving"
						tl63_AnimName = "raving"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@raving"
						tl63_AnimName = "raving"
					ENDIF
				BREAK 	
			#ENDIF
			
			
				CASE PLAYER_INTERACTION_BANGING_TUNES
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@banging_tunes"
						tl63_AnimName = "banging_tunes"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@banging_tunes"
						tl63_AnimName = "banging_tunes"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_BANGING_TUNES_LEFT
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@banging_tunes_left"
						tl63_AnimName = "banging_tunes"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@banging_tunes_left"
						tl63_AnimName = "banging_tunes"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_BANGING_TUNES_RIGHT
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@banging_tunes_right"
						tl63_AnimName = "banging_tunes"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@banging_tunes_right"
						tl63_AnimName = "banging_tunes"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_OH_SNAP
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@oh_snap"
						tl63_AnimName = "oh_snap"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@oh_snap"
						tl63_AnimName = "oh_snap"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_CATS_CRADLE
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@cats_cradle"
						tl63_AnimName = "cats_cradle"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@cats_cradle"
						tl63_AnimName = "cats_cradle"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_RAISE_THE_ROOF
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@raise_the_roof"
						tl63_AnimName = "raise_the_roof"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@raise_the_roof"
						tl63_AnimName = "raise_the_roof"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_FIND_THE_FISH
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@find_the_fish"
						tl63_AnimName = "find_the_fish"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@find_the_fish"
						tl63_AnimName = "find_the_fish"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_SALSA_ROLL
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@salsa_roll"
						tl63_AnimName = "salsa_roll"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@salsa_roll"
						tl63_AnimName = "salsa_roll"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_HEART_PUMPING
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@heart_pumping"
						tl63_AnimName = "heart_pumping"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@heart_pumping"
						tl63_AnimName = "heart_pumping"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_UNCLE_DISCO	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@uncle_disco"
						tl63_AnimName = "uncle_disco"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@uncle_disco"
						tl63_AnimName = "uncle_disco"
					ENDIF
				BREAK
				
			
				
				CASE PLAYER_INTERACTION_MAKE_IT_RAIN	
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@raining_cash"
						tl63_AnimName = "raining_cash"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@raining_cash"
						tl63_AnimName = "raining_cash"
					ENDIF
				BREAK
							
				#IF FEATURE_CASINO
				CASE PLAYER_INTERACTION_CRY_BABY
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@cry_baby"
						tl63_AnimName = "cry_baby"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@cry_baby"
						tl63_AnimName = "cry_baby"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_CUT_THROAT
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@cut_throat"
						tl63_AnimName = "cut_throat"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@cut_throat"
						tl63_AnimName = "cut_throat"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_KARATE_CHOPS
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@karate_chops"
						tl63_AnimName = "karate_chops"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@karate_chops"
						tl63_AnimName = "karate_chops"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_SHADOW_BOXING
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@shadow_boxing"
						tl63_AnimName = "shadow_boxing"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@shadow_boxing"
						tl63_AnimName = "shadow_boxing"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_THE_WOOGIE
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@the_woogie"
						tl63_AnimName = "the_woogie"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@the_woogie"
						tl63_AnimName = "the_woogie"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_STINKER
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@stinker"
						tl63_AnimName = "stinker"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@stinker"
						tl63_AnimName = "stinker"
					ENDIF
				BREAK
				#ENDIF		
				
				#IF FEATURE_CASINO_HEIST
				CASE PLAYER_INTERACTION_AIR_DRUMS
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@air_drums"
						tl63_AnimName = "air_drums"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@air_drums"
						tl63_AnimName = "air_drums"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_CALL_ME
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@call_me"
						tl63_AnimName = "call_me"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@call_me"
						tl63_AnimName = "call_me"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_COIN_ROLL_AND_TOSS
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@coin_roll_and_toss"
						tl63_AnimName = "coin_roll_and_toss"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@coin_roll_and_toss"
						tl63_AnimName = "coin_roll_and_toss"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_BANG_BANG
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@bang_bang"
						tl63_AnimName = "bang_bang"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@bang_bang"
						tl63_AnimName = "bang_bang"
					ENDIF
				BREAK
				CASE PLAYER_INTERACTION_RESPECT
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@respect"
						tl63_AnimName = "respect"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@respect"
						tl63_AnimName = "respect"
					ENDIF		
				BREAK
				CASE PLAYER_INTERACTION_MIND_BLOWN
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@mind_blown"
						tl63_AnimName = "mind_blown"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@mind_blown"
						tl63_AnimName = "mind_blown"
					ENDIF
				BREAK
				#ENDIF
				
				#IF FEATURE_COPS_N_CROOKS
				CASE PLAYER_INTERACTION_POUR_ONE_OUT
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@pour_one_out"
						tl63_AnimName = "POUR_ONE_OUT"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@pour_one_out"
						tl63_AnimName = "POUR_ONE_OUT"
					ENDIF
				BREAK
				
				CASE PLAYER_INTERACTION_HAND_SIGNALS
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@anim_cnc@mp_player_intcelebrationmale@hand_signals"
						tl63_AnimName = "hand_signals"
					ELSE
						tl63_AnimDict = "anim_cnc@anim_cnc@mp_player_intcelebrationmale@hand_signals"
						tl63_AnimName = "hand_signals"
					ENDIF				
				BREAK
				
				CASE PLAYER_INTERACTION_TACTICAL_SALUTE
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@salute"
						tl63_AnimName = "salute"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@salute"
						tl63_AnimName = "salute"
					ENDIF	
				BREAK
				
				CASE PLAYER_INTERACTION_TECHY_FINGERS
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@techy_fingers"
						tl63_AnimName = "techy_fingers"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@techy_fingers"
						tl63_AnimName = "techy_fingers"
					ENDIF					
				BREAK
				
				CASE PLAYER_INTERACTION_TALK_INTO_EAR_PIECE
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@talkinto_earpiece"
						tl63_AnimName = "talkinto_earpiece"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@talkinto_earpiece"
						tl63_AnimName = "talkinto_earpiece"
					ENDIF					
				BREAK
				
				CASE PLAYER_INTERACTION_BATON_TWIRL
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@baton_twirl"
						tl63_AnimName = "baton_twirl"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@baton_twirl"
						tl63_AnimName = "baton_twirl"
					ENDIF	
				BREAK
				
				CASE PLAYER_INTERACTION_FLASH_BADGE
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@flash_cop_badge"
						tl63_AnimName = "flash_cop_badge"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@flash_cop_badge"
						tl63_AnimName = "flash_cop_badge"
					ENDIF	
				BREAK
				
				CASE PLAYER_INTERACTION_EAT_DOUGHNUT
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@eat_doughnut"
						tl63_AnimName = "eat_doughnut"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@eat_doughnut"
						tl63_AnimName = "eat_doughnut"
					ENDIF	
				BREAK
				
				CASE PLAYER_INTERACTION_HAND_RUB
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@hand_rub"
						tl63_AnimName = "hand_rub"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@hand_rub"
						tl63_AnimName = "hand_rub"
					ENDIF
				BREAK
				
				CASE PLAYER_INTERACTION_SOLIDER_PEACE
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@peace"
						tl63_AnimName = "peace"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@peace"
						tl63_AnimName = "peace"
					ENDIF
				BREAK
				
				CASE PLAYER_INTERACTION_BRING_IT
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@bring_it"
						tl63_AnimName = "bring_it"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@bring_it"
						tl63_AnimName = "bring_it"
					ENDIF
				BREAK
				
				CASE PLAYER_INTERACTION_JACKED_UP
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@jacked_up"
						tl63_AnimName = "jacked_up"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@jacked_up"
						tl63_AnimName = "jacked_up"
					ENDIF
				BREAK
				
				CASE PLAYER_INTERACTION_HACKY_FINGERS
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@hacky_fingers"
						tl63_AnimName = "hacky_fingers"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@hacky_fingers"
						tl63_AnimName = "hacky_fingers"
					ENDIF				
				BREAK
				
				CASE PLAYER_INTERACTION_WASNT_ME
					IF bMaleAnim
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@wasnt_me"
						tl63_AnimName = "wasnt_me"
					ELSE
						tl63_AnimDict = "anim_cnc@mp_player_intcelebrationmale@wasnt_me"
						tl63_AnimName = "wasnt_me"
					ENDIF				
				BREAK
				#ENDIF
				
				CASE PLAYER_INTERACTION_DRINK_3
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@spray_champagne"
						tl63_AnimName = "spray_champagne"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@spray_champagne"
						tl63_AnimName = "spray_champagne"
					ENDIF
				BREAK
				
				#IF FEATURE_HEIST_ISLAND_DANCES
				CASE PLAYER_INTERACTION_CROWD_INVITATION
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@crowd_invitation"
						tl63_AnimName = "crowd_invitation"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@crowd_invitation"
						tl63_AnimName = "crowd_invitation"
					ENDIF
				BREAK
				
				CASE PLAYER_INTERACTION_DRIVER
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@driver"
						tl63_AnimName = "driver"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@driver"
						tl63_AnimName = "driver"
					ENDIF
				BREAK
				
				CASE PLAYER_INTERACTION_RUNNER
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@runner"
						tl63_AnimName = "runner"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@runner"
						tl63_AnimName = "runner"
					ENDIF
				BREAK
				
				CASE PLAYER_INTERACTION_SHOOTING
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@shooting"
						tl63_AnimName = "shooting"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@shooting"
						tl63_AnimName = "shooting"
					ENDIF
				BREAK
				
				CASE PLAYER_INTERACTION_SUCK_IT
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@suck_it"
						tl63_AnimName = "suck_it"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@suck_it"
						tl63_AnimName = "suck_it"
					ENDIF
				BREAK
				
				CASE PLAYER_INTERACTION_TAKE_SELFIE
					IF bMaleAnim
						tl63_AnimDict = "anim@mp_player_intcelebrationmale@take_selfie"
						tl63_AnimName = "take_selfie"
					ELSE
						tl63_AnimDict = "anim@mp_player_intcelebrationfemale@take_selfie"
						tl63_AnimName = "take_selfie"
					ENDIF
				BREAK
				#ENDIF
				
				 // FEATURE_ARENA_WARS
			
				// If we can't find anything to play for some reason, default to the salute.
				DEFAULT
					tl63_AnimDict = "anim@mp_player_intcelebrationmale@salute"
					tl63_AnimName = "salute"
				BREAK
			ENDSWITCH
		
		BREAK
		
		DEFAULT
			tl63_AnimDict = "anim@mp_player_intcelebrationmale@salute"
			tl63_AnimName = "salute"
		BREAK
				
	ENDSWITCH
	
	PRINTLN("[NETCELEBRATION] - GET_CELEBRATION_ANIM_TO_PLAY - has been called.")
	PRINTLN("[NETCELEBRATION] - GET_CELEBRATION_ANIM_TO_PLAY - iAnimToPlay = ", iAnimToPlay)
	PRINTLN("[NETCELEBRATION] - GET_CELEBRATION_ANIM_TO_PLAY - bMaleAnim = ", bMaleAnim)
	PRINTLN("[NETCELEBRATION] - GET_CELEBRATION_ANIM_TO_PLAY - bGettingAnimForLamar = ", bGettingAnimForLamar)
	PRINTLN("[NETCELEBRATION] - GET_CELEBRATION_ANIM_TO_PLAY - tl63_AnimDict = ", tl63_AnimDict)
	PRINTLN("[NETCELEBRATION] - GET_CELEBRATION_ANIM_TO_PLAY - tl63_AnimName = ", tl63_AnimName)
	PRINTLN("[NETCELEBRATION] - GET_CELEBRATION_ANIM_TO_PLAY - eAnimType = ", ENUM_TO_INT(eAnimType))
	PRINTLN("[NETCELEBRATION] - GET_CELEBRATION_ANIM_TO_PLAY - eCrewAnimToPlay = ", ENUM_TO_INT(eCrewAnimToPlay))
	
ENDPROC

/// PURPOSE:
///    Handles the full body celebration anims for the winner.
PROC UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS(CELEBRATION_SCREEN_DATA &sCelebrationData, PED_INDEX &pedWinnerClone, INT iWinnerIndex, INT iRallyCoDriverIndex, BOOL bGettingAnimForLamar, BOOL bdoingRallyRace, BOOL bRallyDriver, PAIRED_CELEBRATION_GENDER_COMBO eGenderCombo, INT iOverrideAnim = -1, INT iTimeToStartAnim = 200, BOOL bJuggernaut = FALSE, INT iCustomPedModel = -1)
	
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS being called.")
	
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS iWinnerIndex = ", iWinnerIndex)
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS bGettingAnimForLamar = ", bGettingAnimForLamar)
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS bdoingRallyRace = ", bdoingRallyRace)
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS bRallyDriver = ", bRallyDriver)
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS eGenderCombo = ", eGenderCombo)
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS iOverrideAnim = ", iOverrideAnim)
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS iTimeToStartAnim = ", iTimeToStartAnim)
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS bJuggernaut = ", bJuggernaut)
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS iCustomPedModel = ", iCustomPedModel)
	
	CELEB_SERVER_DATA sCelebServer
	
	IF NOT IS_PED_INJURED(pedWinnerClone)	
	AND ( (iWinnerIndex >= 0) OR (bGettingAnimForLamar) )
		
		PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - NOT IS_PED_INJURED(pedWinnerClone) AND iWinnerIndex >= 0")
		
		SET_PED_RESET_FLAG(pedWinnerClone, PRF_SuppressInAirEvent, TRUE)
	
		TEXT_LABEL_63 tl63_AnimDict = ""
		TEXT_LABEL_63 tl63_AnimName = ""
		INT iAnimToPlay
		INT iAnimType
		INT iCrewAnim
		
		TEXT_LABEL_63 tl63_IdleAnimDict = ""
		TEXT_LABEL_63 tl63_IdleAnimName = ""
		
		IF iOverrideAnim > -1
			iAnimToPlay = iOverrideAnim // *WJK - only for Lamar.
		ELSE
			iAnimToPlay = GlobalplayerBD[iWinnerIndex].iInteractionAnim // *WJK - corona choice, stored at start of each job, look for something like store_celebration_anim or similar - race, dm, missions, golf, not survival. iWinnerIndex = player index of winner
			iAnimType = GlobalplayerBD[iWinnerIndex].iInteractionType
			iCrewAnim = GlobalplayerBD[iWinnerIndex].iCrewAnim
			// *WJK - prob add in male/female flag in global player bd, store at same time anim is stored.
		ENDIF
		
		IF iTimeToStartAnim = iTimeToStartAnim
			iTimeToStartAnim = iTimeToStartAnim
		ENDIF
		
		IF NOT bDoingRallyRace
			
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - not doing a rally race, calling GET_CELEBRATION_ANIM_TO_PLAY  ")
			
			GET_CELEBRATION_ANIM_TO_PLAY(iAnimToPlay, IS_PED_MALE(pedWinnerClone), bGettingAnimForLamar, tl63_AnimDict, tl63_AnimName, INT_TO_ENUM(INTERACTION_ANIM_TYPES, iAnimType), INT_TO_ENUM(CREW_INTERACTIONS, iCrewAnim), bJuggernaut, iCustomPedModel)
			
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - tl63_AnimDict = ", tl63_AnimDict, " tl63_AnimName = ", tl63_AnimName, ", GlobalplayerBD[iWinnerIndex].iInteractionAnim = ", GlobalplayerBD[iWinnerIndex].iInteractionAnim, " iWinnerIndex = ", iWinnerIndex)
			
		ELSE
			
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - doing rally race, calling GET_PAIRED_CELEBRATION_ANIM_TO_PLAY")
			
			GET_PAIRED_CELEBRATION_ANIM_TO_PLAY(eGenderCombo, GlobalplayerBD[iWinnerIndex].iPairedAnim, pedWinnerClone, bRallyDriver, tl63_AnimDict, tl63_AnimName) // Note we will read the stat set in the pause menu to get the anim to play and replace PAIRED_CELEBRATION_ANIM_MANLY_HANDSHAKE with this.
			
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - tl63_AnimDict = ", tl63_AnimDict, " tl63_AnimName = ", tl63_AnimName, ", GlobalplayerBD[iWinnerIndex].iPairedAnim = ", GlobalplayerBD[iWinnerIndex].iPairedAnim)
			
		ENDIF
		
		GET_CELEBRATION_IDLE_ANIM_TO_USE(sCelebrationData, tl63_IdleAnimDict, tl63_IdleAnimName, IS_PED_MALE(pedWinnerClone))
		
		IF sCelebrationData.eIdleAnimToUse != CELEBRATION_IDLE_ANIM_INVALID
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tl63_AnimDict)
			AND NOT IS_STRING_NULL_OR_EMPTY(tl63_IdleAnimDict)
				REQUEST_ANIM_DICT(tl63_AnimDict)
				REQUEST_ANIM_DICT(tl63_IdleAnimDict)
				IF HAS_ANIM_DICT_LOADED(tl63_AnimDict)
				AND HAS_ANIM_DICT_LOADED(tl63_IdleAnimDict)
				AND HAVE_REQUIRED_SOUND_BANKS_LOADED(iAnimToPlay, bdoingRallyRace)
				AND HAVE_REQUIRED_PROPS_LOADED(iAnimToPlay, bdoingRallyRace)
					
					SWITCH sCelebrationData.iPlayWinnerAnimsStage
						
						CASE 0
							
							IF iWinnerIndex > (-1)
							AND ((bdoingRallyRace AND (iRallyCoDriverIndex >  (-1))) OR (NOT bdoingRallyRace))
								
								IF NOT IS_BIT_SET(sCelebrationData.iPlayedWinnerAnim, iWinnerIndex)
								OR ( (bdoingRallyRace AND (NOT IS_BIT_SET(sCelebrationData.iPlayedWinnerAnim, iRallyCoDriverIndex))) )
									
									VECTOR vScenePos, vSceneRot, vPedPos, vPedRot
									
									IF bdoingRallyRace
										
										GET_WINNER_PED_CREATION_DATA(sCelebServer, 0, vScenePos, vSceneRot, TRUE, FALSE, TRUE)
										vSceneRot.z += 90.0
										
										sCelebrationData.iwinnerSyncedSceneId = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneRot)
										
										vPedPos = GET_ANIM_INITIAL_OFFSET_POSITION(tl63_AnimDict, tl63_AnimName, vScenePos, vSceneRot)
										vPedRot = GET_ANIM_INITIAL_OFFSET_ROTATION(tl63_AnimDict, tl63_AnimName, vScenePos, vSceneRot)
										
										IF DOES_ENTITY_EXIST(pedWinnerClone)
											IF NOT IS_ENTITY_DEAD(pedWinnerClone)
												SET_ENTITY_COORDS_NO_OFFSET(pedWinnerClone, vPedPos)
												SET_ENTITY_ROTATION(pedWinnerClone, vPedRot)
											ENDIF
										ENDIF
										
										PRINTLN("[NETCELEBRATION] - [Paired_Anims] - iWinnerIndex ", iWinnerIndex, " vScenePos = ", vScenePos)
										PRINTLN("[NETCELEBRATION] - [Paired_Anims] - iWinnerIndex ", iWinnerIndex, " vSceneRot = ", vSceneRot)
										PRINTLN("[NETCELEBRATION] - [Paired_Anims] - iWinnerIndex ", iWinnerIndex, " vPedPos = ", vPedPos)
										PRINTLN("[NETCELEBRATION] - [Paired_Anims] - iWinnerIndex ", iWinnerIndex, " vPedRot = ", vPedRot)
										
									ELSE
										
										VECTOR vWinnerCoords, vPropPos
										vWinnerCoords = GET_ENTITY_COORDS(pedWinnerClone)
										vPropPos = vWinnerCoords
										
										CREATE_CELEBRATION_PROP(sCelebrationData.entityProp, iAnimToPlay, bdoingRallyRace, vPropPos)
										sCelebrationData.iwinnerSyncedSceneId = CREATE_SYNCHRONIZED_SCENE(vWinnerCoords, GET_ENTITY_ROTATION(pedWinnerClone))
										
									ENDIF
									
									IF DOES_ENTITY_EXIST(sCelebrationData.entityProp)
										PRINTLN("[NETCELEBRATION] - [Paired_Anims] - PLAY_SYNCHRONIZED_ENTITY_ANIM - played scene on prop. tl63_AnimDict = ", tl63_AnimDict, ", tl63_AnimName = ", tl63_AnimName, ", iwinnerSyncedSceneId = ", sCelebrationData.iwinnerSyncedSceneId)
										PLAY_SYNCHRONIZED_ENTITY_ANIM(	sCelebrationData.entityProp, sCelebrationData.iwinnerSyncedSceneId, tl63_AnimName, tl63_AnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
									ENDIF
									
									TASK_SYNCHRONIZED_SCENE(	pedWinnerClone, 
																sCelebrationData.iwinnerSyncedSceneId, 
																tl63_AnimDict, tl63_AnimName, 
																INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 
																SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN,
																AIK_DISABLE_LEG_IK | 
																AIK_DISABLE_ARM_IK |
																AIK_DISABLE_HEAD_IK |
																AIK_DISABLE_TORSO_IK |
																AIK_DISABLE_TORSO_REACT_IK	)
									
									IF bdoingRallyRace
										IF bRallyDriver
											IF iWinnerIndex >= 0
											AND iWinnerIndex < 32
												SET_BIT(sCelebrationData.iPlayedWinnerAnim, iWinnerIndex)
											ENDIF
										ELSE
											IF iRallyCoDriverIndex >= 0
											AND iRallyCoDriverIndex < 32
												SET_BIT(sCelebrationData.iPlayedWinnerAnim, iRallyCoDriverIndex)
											ENDIF
										ENDIF
									ELSE
										IF iWinnerIndex >= 0
										AND iWinnerIndex < 32
											SET_BIT(sCelebrationData.iPlayedWinnerAnim, iWinnerIndex)
										ENDIF
									ENDIF
									
									PRINTLN("[NETCELEBRATION] - Playing synchronized scene anim for player ", iWinnerIndex, " ", iAnimToPlay, " ", tl63_AnimDict, " ", tl63_AnimName, " ", iTimeToStartAnim)
									
								ELSE
									
									sCelebrationData.iPlayWinnerAnimsStage++
									
								ENDIF
							
							ELSE
								
								sCelebrationData.iPlayWinnerAnimsStage = 2
								PRINTLN("[NETCELEBRATION] - [Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - iWinnerIndex = ", iWinnerIndex, ", iRallyCoDriverIndex = ", iRallyCoDriverIndex)
								PRINTLN("[NETCELEBRATION] - [Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - cannot play paired anims, skipping on without playing.")
							ENDIF
							
						BREAK
						
						CASE 1
							
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sCelebrationData.iwinnerSyncedSceneId)
								
								IF NOT bDoingRallyRace
									IF (iAnimToPlay = ENUM_TO_INT(PLAYER_INTERACTION_KNUCKLE_CRUNCH))
										PLAY_KNUCKLE_CRUNCH_AUDIO(sCelebrationData, pedWinnerClone)
									ENDIF
									IF (iAnimToPlay = ENUM_TO_INT(PLAYER_INTERACTION_SLOW_CLAP))
										PLAY_SLOW_CLAP_AUDIO(sCelebrationData, pedWinnerClone)
									ENDIF
								ENDIF
								
								
								IF sCelebrationData.bUseExtendedIdle
									
	  								IF GET_SYNCHRONIZED_SCENE_PHASE(sCelebrationData.iwinnerSyncedSceneId) < 0.98
										IF (iAnimToPlay = ENUM_TO_INT(PLAYER_INTERACTION_KNUCKLE_CRUNCH))
											PLAY_KNUCKLE_CRUNCH_AUDIO(sCelebrationData, pedWinnerClone)
										ENDIF
										IF (iAnimToPlay = ENUM_TO_INT(PLAYER_INTERACTION_SLOW_CLAP))
											PLAY_SLOW_CLAP_AUDIO(sCelebrationData, pedWinnerClone)
										ENDIF
									ELSE
										TASK_PLAY_ANIM_ADVANCED(pedWinnerClone, tl63_IdleAnimDict, tl63_IdleAnimName, GET_ENTITY_COORDS(pedWinnerClone), GET_ENTITY_ROTATION(pedWinnerClone), 
																REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT, 0, EULER_YXZ, 
																AIK_DISABLE_LEG_IK | 
																AIK_DISABLE_ARM_IK |
																AIK_DISABLE_HEAD_IK |
																AIK_DISABLE_TORSO_IK |
																AIK_DISABLE_TORSO_REACT_IK)
										sCelebrationData.iPlayWinnerAnimsStage++
									ENDIF
									
								ENDIF
								
							ENDIF
										
						BREAK
									
						CASE 2
								
							// Done. 
									
						BREAK
						
					ENDSWITCH
					
				ELSE
					IF NOT HAS_ANIM_DICT_LOADED(tl63_AnimDict)
						PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - tl63_AnimDict loaded = FALSE. Dict = ", tl63_AnimDict)
					ENDIF
					IF NOT HAVE_REQUIRED_SOUND_BANKS_LOADED(iAnimToPlay, bdoingRallyRace)
						PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - HAVE_REQUIRED_SOUND_BANKS_LOADED = FALSE.")
					ENDIF
					IF NOT HAS_ANIM_DICT_LOADED(tl63_IdleAnimDict)
						PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - tl63_IdleAnimDict loaded = FALSE. Dict = ", tl63_IdleAnimDict)
					ENDIF
					IF NOT HAVE_REQUIRED_PROPS_LOADED(iAnimToPlay, bdoingRallyRace)
						PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - HAVE_REQUIRED_PROPS_LOADED = FALSE.")
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - tl63_AnimDict is NULL or empty.")
			ENDIF
			
		ELSE
		
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - sCelebrationData.eIdleAnimToUse = CELEBRATION_IDLE_ANIM_INVALID.")
		
		ENDIF
		
	ELSE
	
		IF IS_PED_INJURED(pedWinnerClone)	
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - IS_PED_INJURED(pedWinnerClone) = TRUE")
		ENDIF
		
		IF iWinnerIndex < 0
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS - iWinnerIndex < 0")
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_FACIAL_ANIMS(CELEB_SERVER_DATA &sCelebServer, CELEBRATION_SCREEN_DATA &sCelebrationData, PED_INDEX &pedWinnerClone, INT i)

	IF NOT IS_BIT_SET(sCelebrationData.iFacialBitset, i)

		TEXT_LABEL_63 tl63_AnimDict = ""
		TEXT_LABEL_63 tl63_AnimName = ""
		
		STRING sFacial
		STRING sFacialDict = NULL

		GET_ARENA_CELEB_ANIMS(sCelebServer, tl63_AnimDict, tl63_AnimName, i)

		tl63_AnimName += "_FACE"

		sFacial 	= TEXT_LABEL_TO_STRING(tl63_AnimName)
		sFacialDict = TEXT_LABEL_TO_STRING(tl63_AnimDict)

		REQUEST_ANIM_DICT(sFacialDict)

		IF HAS_ANIM_DICT_LOADED(sFacialDict)

			PLAY_FACIAL_ANIM(pedWinnerClone, sFacial, sFacialDict)
			PRINTLN("[NETCELEBRATION] MAINTAIN_FACIAL_ANIMS, PLAY_FACIAL_ANIM for i = ", i, " sFacial = ", sFacial, " sFacialDict = ", sFacialDict)
			
			SET_BIT(sCelebrationData.iFacialBitset, i)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Handles the full body celebration anims for the winner.
PROC UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA(CELEB_SERVER_DATA &sCelebServer, CELEBRATION_SCREEN_DATA &sCelebrationData, PED_INDEX &pedWinnerClone, INT iWinnerIndex, INT iLoop)
	
	INT iTimeToStartAnim = 200
	
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA being called, iLoop = ", iLoop)
	PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA iWinnerIndex = ", iWinnerIndex)
	
	IF NOT IS_PED_INJURED(pedWinnerClone)	
	AND (iWinnerIndex >= 0) 
		
		PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA - NOT IS_PED_INJURED(pedWinnerClone) AND iWinnerIndex >= 0")
		
		SET_PED_RESET_FLAG(pedWinnerClone, PRF_SuppressInAirEvent, TRUE)
	
		TEXT_LABEL_63 tl63_AnimDict = ""
		TEXT_LABEL_63 tl63_AnimName = ""
		INT iAnimToPlay
		
		TEXT_LABEL_63 tl63_IdleAnimDict = ""
		TEXT_LABEL_63 tl63_IdleAnimName = ""
		
		TEXT_LABEL_63 tl63_AnimDictProp = ""
		TEXT_LABEL_63 tl63_AnimNameProp = ""
		
		IF iTimeToStartAnim = iTimeToStartAnim
			iTimeToStartAnim = iTimeToStartAnim
		ENDIF
			
		GET_ARENA_CELEB_ANIMS(sCelebServer, tl63_AnimDict, tl63_AnimName, iLoop)
		
		IF PROP_NEEDS_ANIMATED(sCelebServer, sCelebServer.eArenaClips[iLoop])
			GET_ARENA_CELEB_PROP_ANIMS(sCelebServer, tl63_AnimDictProp, tl63_AnimNameProp, iLoop)
			PRINTLN("[NETCELEBRATION] [ARENA]  - [Paired_Anims] - PROP_NEEDS_ANIMATED ", iWinnerIndex, " tl63_AnimDictProp = ", tl63_AnimDictProp, " tl63_AnimNameProp = ", tl63_AnimNameProp, " for iLoop ", iLoop)
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63_AnimDictProp)
			REQUEST_ANIM_DICT(tl63_AnimDictProp)
		ENDIF
			
//			//DEBUG TEST
//			tl63_AnimDict = "anim@arena@celeb@podium@no_prop@" 
//			tl63_AnimName = "cheer_a_1st"

		IF IS_STRING_NULL_OR_EMPTY(tl63_AnimDict)
			PRINTLN("[NETCELEBRATION] [ARENA]   - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA - IS_STRING_NULL_OR_EMPTY(tl63_AnimDict) ")
			
		ELSE
			PRINTLN("[NETCELEBRATION] [ARENA]   - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA - bPodium, tl63_AnimDict = ", tl63_AnimDict, " tl63_AnimName = ", tl63_AnimName, 
					" iWinnerIndex = ", iWinnerIndex, " at ", NATIVE_TO_INT(GET_NETWORK_TIME()))
					
			PRINTLN("[NETCELEBRATION] [ARENA]  - [Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA iLoop = ", iLoop)
			
			GET_CELEBRATION_IDLE_ANIM_TO_USE(sCelebrationData, tl63_IdleAnimDict, tl63_IdleAnimName, IS_PED_MALE(pedWinnerClone))			
			
			MAINTAIN_ARENA_CELEB_PTFX(sCelebServer, sCelebrationData, pedWinnerClone)
			
			MAINTAIN_FACIAL_ANIMS(sCelebServer, sCelebrationData, pedWinnerClone, iLoop)	
			
			POSITION_ARENA_LIGHT_RIG(sCelebServer, sCelebrationData)
			
			IF sCelebrationData.eIdleAnimToUse != CELEBRATION_IDLE_ANIM_INVALID
			
				PRINTLN("[NETCELEBRATION] [ARENA]  - [Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA 5511230 A, iLoop = ", iLoop)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(tl63_AnimDict)
				AND NOT IS_STRING_NULL_OR_EMPTY(tl63_IdleAnimDict)
				AND ( IS_STRING_NULL_OR_EMPTY(tl63_AnimDictProp) OR HAS_ANIM_DICT_LOADED(tl63_AnimDictProp) )
				
					PRINTLN("[NETCELEBRATION] [ARENA]  - [Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA 5511230 B, iLoop = ", iLoop)
				
					REQUEST_ANIM_DICT(tl63_AnimDict)
					REQUEST_ANIM_DICT(tl63_IdleAnimDict)
					IF HAS_ANIM_DICT_LOADED(tl63_AnimDict)
					AND HAS_ANIM_DICT_LOADED(tl63_IdleAnimDict)
					AND HAVE_REQUIRED_SOUND_BANKS_LOADED(iAnimToPlay, FALSE)
					AND HAVE_ARENA_PROPS_LOADED(sCelebServer)
					
						PRINTLN("[NETCELEBRATION] [ARENA]  - [Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA 5511230 C, iLoop = ", iLoop)
						
						SWITCH sCelebrationData.iPlayWinnerAnimsStage
							
							CASE 0
								
								IF iWinnerIndex > (-1)
									
									IF NOT IS_BIT_SET(sCelebrationData.iPlayedWinnerAnim, iLoop)
									
										g_bCelebrationPodiumIsActive = TRUE
										
										VECTOR vScenePos, vSceneRot, vPedPos, vPedRot
											
										CELEBRATION_GET_PED_CREATION_DATA(sCelebServer, iWinnerIndex, vScenePos, vSceneRot, iLoop)
										
										sCelebrationData.iwinnerSyncedSceneId = CREATE_SYNCHRONIZED_SCENE(vScenePos, vSceneRot)
										
										vPedPos = GET_ANIM_INITIAL_OFFSET_POSITION(tl63_AnimDict, tl63_AnimName, vScenePos, vSceneRot)
										vPedRot = GET_ANIM_INITIAL_OFFSET_ROTATION(tl63_AnimDict, tl63_AnimName, vScenePos, vSceneRot)
										
										PRINTLN("[NETCELEBRATION] - [bPodium] - iWinnerIndex ", iWinnerIndex, " vScenePos = ", vScenePos)
										PRINTLN("[NETCELEBRATION] - [bPodium] - iWinnerIndex ", iWinnerIndex, " vSceneRot = ", vSceneRot)
										PRINTLN("[NETCELEBRATION] - [bPodium] - iWinnerIndex ", iWinnerIndex, " vPedPos = ", vPedPos)
										PRINTLN("[NETCELEBRATION] - [bPodium] - iWinnerIndex ", iWinnerIndex, " vPedRot = ", vPedRot)
										PRINTLN("[NETCELEBRATION] - [bPodium] - iWinnerIndex ", iWinnerIndex, " tl63_AnimDict = ", tl63_AnimDict)
										PRINTLN("[NETCELEBRATION] - [bPodium] - iWinnerIndex ", iWinnerIndex, " tl63_AnimName = ", tl63_AnimName)
										PRINTLN("[NETCELEBRATION] - [bPodium] - iWinnerIndex ", iWinnerIndex, " tl63_AnimDictProp = ", tl63_AnimDictProp)
										PRINTLN("[NETCELEBRATION] - [bPodium] - iWinnerIndex ", iWinnerIndex, " tl63_AnimNameProp = ", tl63_AnimNameProp)
										PRINTLN("[NETCELEBRATION] - [bPodium] - iWinnerIndex ", iWinnerIndex, " iLoop = ", iLoop)
										
										// Play anim on props
										IF PROP_NEEDS_ANIMATED(sCelebServer, sCelebServer.eArenaClips[iLoop])
										AND DOES_ENTITY_EXIST(sCelebrationData.ArenaProps[iLoop])
										AND NOT IS_STRING_NULL_OR_EMPTY(tl63_AnimNameProp)
											PLAY_SYNCHRONIZED_ENTITY_ANIM(sCelebrationData.ArenaProps[iLoop], sCelebrationData.iwinnerSyncedSceneId, tl63_AnimNameProp, tl63_AnimDictProp, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
											PRINTLN("[NETCELEBRATION] [ARENA]  - [Paired_Anims] - PROP_NEEDS_ANIMATED, PLAY_ENTITY_ANIM ", iWinnerIndex, " tl63_AnimDictProp = ", tl63_AnimDictProp, " tl63_AnimNameProp = ", tl63_AnimNameProp)
											
											#IF IS_DEBUG_BUILD
										ELSE
											IF NOT DOES_ENTITY_EXIST(sCelebrationData.ArenaProps[iLoop])
												PRINTLN("[NETCELEBRATION] [ARENA]  - [Paired_Anims] - PROP_NEEDS_ANIMATED, PLAY_ENTITY_ANIM, ERROR NOT DOES_ENTITY_EXIST ")
											ENDIF
											IF IS_STRING_NULL_OR_EMPTY(tl63_AnimNameProp)
												PRINTLN("[NETCELEBRATION] [ARENA]  - [Paired_Anims] - PROP_NEEDS_ANIMATED, PLAY_ENTITY_ANIM, ERROR IS_STRING_NULL_OR_EMPTY ")
											ENDIF
											#ENDIF // #IF IS_DEBUG_BUILD
										ENDIF
										
										TASK_SYNCHRONIZED_SCENE(	pedWinnerClone, 
																	sCelebrationData.iwinnerSyncedSceneId, 
																	tl63_AnimDict, tl63_AnimName, 
																	INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 
																	SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN,
																	AIK_DISABLE_LEG_IK | 
																	AIK_DISABLE_ARM_IK |
																	AIK_DISABLE_HEAD_IK |
																	AIK_DISABLE_TORSO_IK |
																	AIK_DISABLE_TORSO_REACT_IK	)
										

										IF iLoop >= 0
										AND iLoop < 32
											SET_BIT(sCelebrationData.iPlayedWinnerAnim, iLoop)
										ENDIF
							
										
										PRINTLN("[NETCELEBRATION] - Playing synchronized scene anim for player ", iLoop, " ", iAnimToPlay, " ", tl63_AnimDict, " ", tl63_AnimName, " ", iTimeToStartAnim)
										
										PRINTLN("[Paired_Anims] - TASK_SYNCHRONIZED_SCENE, 5510912 TIME ", NATIVE_TO_INT(GET_NETWORK_TIME()))
										
									ELSE
										
										sCelebrationData.iPlayWinnerAnimsStage++
										
									ENDIF
								
								ELSE
									
									sCelebrationData.iPlayWinnerAnimsStage = 2
									PRINTLN("[NETCELEBRATION] - [Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA - iWinnerIndex = ", iWinnerIndex)
									PRINTLN("[NETCELEBRATION] - [Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA - cannot play paired anims, skipping on without playing.")
								ENDIF
								
							BREAK
							
							CASE 1
								
								IF IS_SYNCHRONIZED_SCENE_RUNNING(sCelebrationData.iwinnerSyncedSceneId)
						
									IF (iAnimToPlay = ENUM_TO_INT(PLAYER_INTERACTION_KNUCKLE_CRUNCH))
										PLAY_KNUCKLE_CRUNCH_AUDIO(sCelebrationData, pedWinnerClone)
									ENDIF
									IF (iAnimToPlay = ENUM_TO_INT(PLAYER_INTERACTION_SLOW_CLAP))
										PLAY_SLOW_CLAP_AUDIO(sCelebrationData, pedWinnerClone)
									ENDIF
									
									IF sCelebrationData.bUseExtendedIdle
										
		  								IF GET_SYNCHRONIZED_SCENE_PHASE(sCelebrationData.iwinnerSyncedSceneId) < 0.98
											IF (iAnimToPlay = ENUM_TO_INT(PLAYER_INTERACTION_KNUCKLE_CRUNCH))
												PLAY_KNUCKLE_CRUNCH_AUDIO(sCelebrationData, pedWinnerClone)
											ENDIF
											IF (iAnimToPlay = ENUM_TO_INT(PLAYER_INTERACTION_SLOW_CLAP))
												PLAY_SLOW_CLAP_AUDIO(sCelebrationData, pedWinnerClone)
											ENDIF
										ELSE
											TASK_PLAY_ANIM_ADVANCED(pedWinnerClone, tl63_IdleAnimDict, tl63_IdleAnimName, GET_ENTITY_COORDS(pedWinnerClone), GET_ENTITY_ROTATION(pedWinnerClone), 
																	REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT, 0, EULER_YXZ, 
																	AIK_DISABLE_LEG_IK | 
																	AIK_DISABLE_ARM_IK |
																	AIK_DISABLE_HEAD_IK |
																	AIK_DISABLE_TORSO_IK |
																	AIK_DISABLE_TORSO_REACT_IK)
																	
					
																	
											PRINTLN("[Paired_Anims] - TASK_PLAY_ANIM_ADVANCED, 5510912 TIME ", NATIVE_TO_INT(GET_NETWORK_TIME()))
																	
											sCelebrationData.iPlayWinnerAnimsStage++
										ENDIF
										
									ENDIF
									
								ENDIF
											
							BREAK
										
							CASE 2
									
								// Done. 
										
							BREAK
							
						ENDSWITCH
						
					ELSE
						IF NOT HAS_ANIM_DICT_LOADED(tl63_AnimDict)
							PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA, ERROR - tl63_AnimDict loaded = FALSE. Dict = ", tl63_AnimDict, " i = ", iLoop)
						ENDIF
						IF NOT HAVE_REQUIRED_SOUND_BANKS_LOADED(iAnimToPlay, FALSE)
							PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA, ERROR - HAVE_REQUIRED_SOUND_BANKS_LOADED = FALSE.", " i = ", iLoop)
						ENDIF
						IF NOT HAS_ANIM_DICT_LOADED(tl63_IdleAnimDict)
							PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA, ERROR - tl63_IdleAnimDict loaded = FALSE. Dict = ", tl63_IdleAnimDict, " i = ", iLoop)
						ENDIF
						IF NOT HAVE_ARENA_PROPS_LOADED(sCelebServer)
							PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA, ERROR - HAVE_REQUIRED_PROPS_LOADED = FALSE.", " i = ", iLoop)
						ENDIF
					ENDIF
				ELSE				
					PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA, ERROR - tl63_AnimDict is NULL or empty, tl63_AnimDict         = ", tl63_AnimDict, " i = ", iLoop)
					PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA, ERROR - tl63_IdleAnimDict is NULL or empty, tl63_IdleAnimDict = ", tl63_IdleAnimDict, " i = ", iLoop)
					PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA, ERROR - tl63_AnimDictProp is NULL or empty, tl63_AnimDictProp = ", tl63_AnimDictProp, " i = ", iLoop)
				ENDIF
				
			ELSE
			
				PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA, ERROR - sCelebrationData.eIdleAnimToUse = CELEBRATION_IDLE_ANIM_INVALID.", " i = ", iLoop)
			
			ENDIF
		ENDIF
		
	ELSE
	
		IF IS_PED_INJURED(pedWinnerClone)	
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA, ERROR - IS_PED_INJURED(pedWinnerClone) = TRUE,  i = ", iLoop)
		ENDIF
		
		IF iWinnerIndex < 0
			PRINTLN("[Paired_Anims] - UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA, ERROR - iWinnerIndex < 0, i = ", iLoop)
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Handles the gesture anim for the winning player ped during the winners screen. Call this every frame after starting the winner screen.
PROC UPDATE_CELEBRATION_WINNER_ANIMS_ARENA(CELEB_SERVER_DATA &sCelebServer, CELEBRATION_SCREEN_DATA &sCelebrationData, PED_INDEX &pedWinnerClone, INT iWinnerIndex, INT iLoop)
	
	PRINTLN("[Paired_Anims] - UPDATE_CELEBRATION_WINNER_ANIMS_ARENA is being called. Calling UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS.")
	PRINTLN("[Paired_Anims] - UPDATE_CELEBRATION_WINNER_ANIMS_ARENA iWinnerIndex = ", iWinnerIndex)
	
	UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS_ARENA(sCelebServer, sCelebrationData, pedWinnerClone, iWinnerIndex, iLoop) 
ENDPROC

/// PURPOSE:
///    Handles the gesture anim for the winning player ped during the winners screen. Call this every frame after starting the winner screen.
///    Use bIsBackgroundPlayer to play anims on peds in the background.
PROC UPDATE_CELEBRATION_WINNER_ANIMS(CELEBRATION_SCREEN_DATA &sCelebrationData, PED_INDEX &pedWinnerClone, INT iWinnerIndex, INT iRallycoDriverIndex, BOOL bGettingAnimForLamar, BOOL bdoingRallyRace, BOOL bRallyDriver, PAIRED_CELEBRATION_GENDER_COMBO eGenderCombo, INT iOverrideAnimType = -1, INT iOverrideAnim = -1, 
								     INT iTimeToStartAnim = 200, BOOL bIsBackgroundPlayer = FALSE, BOOL bJuggernaut = FALSE, INT iCustomPedModel = -1)
	//NOTE: This needs a rewrite, it'll work for what's needed in first release but it's messy. What we really need is to cache iWinnerIndex in advance for all 8 clones.
	//Best place to do this is when the clones are created, so even if the player leaves the session straight afterwards we already have their GlobalplayerBD index cached.
	//If they're cached in advance as part of the celebration data struct then we don't need to pass in the iWinnerIndex variable, just a number that indicates which clone we want to play the anim on.
	
	PRINTLN("[Paired_Anims] - UPDATE_CELEBRATION_WINNER_ANIMS is being called. Calling UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS.")
	
	PRINTLN("[Paired_Anims] - UPDATE_CELEBRATION_WINNER_ANIMS iWinnerIndex = ", iWinnerIndex)
	PRINTLN("[Paired_Anims] - UPDATE_CELEBRATION_WINNER_ANIMS bGettingAnimForLamar = ", bGettingAnimForLamar)
	PRINTLN("[Paired_Anims] - UPDATE_CELEBRATION_WINNER_ANIMS bdoingRallyRace = ", bdoingRallyRace)
	PRINTLN("[Paired_Anims] - UPDATE_CELEBRATION_WINNER_ANIMS bRallyDriver = ", bRallyDriver)
	PRINTLN("[Paired_Anims] - UPDATE_CELEBRATION_WINNER_ANIMS eGenderCombo = ", eGenderCombo)
	PRINTLN("[Paired_Anims] - UPDATE_CELEBRATION_WINNER_ANIMS bJuggernaut = ", bJuggernaut)
	
	IF iOverrideAnimType = iOverrideAnimType
	 	iOverrideAnimType = iOverrideAnimType
	ENDIF
	
	IF bIsBackgroundPlayer = bIsBackgroundPlayer
	 	bIsBackgroundPlayer = bIsBackgroundPlayer
	ENDIF
	
	UPDATE_FULL_BODY_CELEBRATION_WINNER_ANIMS(sCelebrationData, pedWinnerClone, iWinnerIndex, iRallycoDriverIndex, bGettingAnimForLamar, bdoingRallyRace, bRallyDriver, eGenderCombo, iOverrideAnim, iTimeToStartAnim, bJuggernaut, iCustomPedModel) // *WJK - keep this.

ENDPROC

/// PURPOSE:
///    generic celebration wrapper to be used for minigames
/// PARAMS:
///    sCelebrationData - struct used for celebration proc's
///    camEndScreen - camera index used for celebration screens
///    tl_23XpType - RP hash used in GIVE_LOCAL_PLAYER_XP
///    anXPType - RP type used in GIVE_LOCAL_PLAYER_XP
///    anXPCategory - RP category used in GIVE_LOCAL_PLAYER_XP
///    iXPGiven - RP given at the end of the minigame
///    eJobWinStatus - end result of minigame, win lose or draw
///    iBetWinnings - money earned, should be polled from the return value of BROADCAST_BETTING_MISSION_FINISHED
///    eGenericMGStage - stage that decides whether it's winner celebration or summary celebration
///    winningPlayer - player ID of the winner
///    bUseCustomCams - FALSE to use the normal celebration cams, TRUE to use a specific script cam
///    iGamesWon - Games Won in a match
///    iTotalGames - Total Games in a match
/// RETURNS:
///    TRUE when all the celebration flows are done
FUNC BOOL HAS_GENERIC_CELEBRATION_FINISHED(CELEBRATION_SCREEN_DATA &sCelebrationData, CAMERA_INDEX & camEndScreen,
											STRING tl_23XpType, XPTYPE anXPType, XPCATEGORY anXPCategory , INT iXPGiven,
											JOB_WIN_STATUS eJobWinStatus, INT iBetWinnings,
											GENERIC_MINIGAME_STAGE & eGenericMGStage, PLAYER_INDEX winningPlayer,
											BOOL bUseCustomCams = FALSE,
											INT iGamesWon = 0, INT iTotalGames = 0, BOOL bEarlyReturn = FALSE, BOOL bSetPedsInvisible = TRUE, XP_TYPE eXPType = eXPTYPE_CO_OP_AMBIENTFM_ON_SCRIPT)
	
	TEXT_LABEL_63 tl63_AnimDict, tl63_AnimName
	
	// Hide ui so it doesn't interfere with the celebration screen.
	IF sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP
		HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
	ENDIF
	
	// Save winning player Id as an int.
	INT iWinningPlayer = NATIVE_TO_INT(winningPlayer)
	
	// eGenericMGStage is now redundant after improving the flow.  will take this out once everyone is in the studio at the same time, and make it cleaner
	eGenericMGStage = GENERIC_CELEBRATION_STATS
	
	IF bSetPedsInvisible
		CELEB_SET_PLAYERS_INVISIBLE_THIS_FRAME()
	ENDIF
	
	IF IS_XBOX360_VERSION()
	OR IS_PS3_VERSION()
		DISPLAY_CELEBRATION_PLAYER_NAMES(sCelebrationData.pedWinnerClones, sCelebrationData.tl31_pedWinnerClonesNames, sCelebrationData.eCurrentStage, sCelebrationData.sfCelebration)
	ELSE
		DRAW_NG_CELEBRATION_PLAYER_NAMES(	sCelebrationData, sCelebrationData.iDrawNamesStage, sCelebrationData.iNumNamesToDisplay, 
											sCelebrationData.pedWinnerClones, sCelebrationData.tl31_pedWinnerClonesNames, 
											sCelebrationData.eCurrentStage, sCelebrationData.sfCelebration,
											sCelebrationData.playerNameMovies, sCelebrationData.bToggleNames)
	ENDIF
	
	HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(sCelebrationData) // Fix for B* 2278693 - stop player models being seen during winner scene, only want ot see ped clones.
	
	SWITCH eGenericMGStage
		
		CASE GENERIC_CELEBRATION_WINNER
			
			SWITCH sCelebrationData.eCurrentStage
				CASE CELEBRATION_STAGE_SETUP
					REQUEST_CELEBRATION_SCREEN(sCelebrationData)
					
					IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
						
						FADE_IN_FAKE_RECTANGLE_FOR_WINNER_SCREEN(sCelebrationData)
						
						IF (HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTransitionTimer, 500) OR IS_SCREEN_FADED_OUT())
						AND CREATE_WINNER_SCENE(sCelebrationData, camEndScreen, winningPlayer)
							
							GET_CELEBRATION_ANIM_TO_PLAY(GlobalplayerBD[iWinningPlayer].iInteractionAnim, IS_PED_MALE(sCelebrationData.pedWinnerClones[0]), FALSE, tl63_AnimDict, tl63_AnimName, INT_TO_ENUM(INTERACTION_ANIM_TYPES, GlobalplayerBD[iWinningPlayer].iInteractionType), INT_TO_ENUM(CREW_INTERACTIONS, GlobalplayerBD[iWinningPlayer].iCrewAnim))
							
							REQUEST_ANIM_DICT(tl63_AnimDict)
							
							IF HAS_ANIM_DICT_LOADED(tl63_AnimDict)
							OR HAS_NG_FAILSAFE_TIMER_EXPIRED(sCelebrationData)
								
								RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
								START_NET_TIMER(sCelebrationData.sCelebrationTimer)
								
								SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
								
								sCelebrationData.eCurrentStage = CELEBRATION_STAGE_TRANSITIONING
								
							ENDIF
							
						ENDIF
					ENDIF
				BREAK
				
				CASE CELEBRATION_STAGE_TRANSITIONING
					DRAW_CELEBRATION_SCREEN_WITH_PED_MASKING_THIS_FRAME(sCelebrationData, sCelebrationData.pedWinnerClone)
					FADE_OUT_FAKE_RECTANGLE_FOR_WINNER_SCREEN(sCelebrationData)
					
					IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 375)
						SET_HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(sCelebrationData)
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 750)
						STRING strScreenName, strBackgroundColour
						TEXT_LABEL_63 strWinnerName
						TEXT_LABEL_63 strCrewName
					
						//Retrieve all the required stats for the race end screen.
						strScreenName = "WINNER"
						strWinnerName = GET_PLAYER_NAME(winningPlayer)
						strCrewName = GET_CREW_NAME_FOR_CELEBRATION_SCREEN(winningPlayer)
						sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2 
						
						IF NOT IS_ACCOUNT_OVER_17()
						OR NOT IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
							strCrewName = ""
						ENDIF
	
						IF eJobWinStatus = JOB_STATUS_WIN
							strBackgroundColour = "HUD_COLOUR_FRIENDLY"
						ELSE
							strBackgroundColour = "HUD_COLOUR_NET_PLAYER1"
						ENDIF
					
						//Build the screen
						CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour)
						ADD_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, eJobWinStatus, strWinnerName, strCrewName, "", iBetWinnings, TRUE)
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME * 2 //Winner screen is guaranteed.
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
						
						ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, 75)
						SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
						
						RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
						START_NET_TIMER(sCelebrationData.sCelebrationTimer)
						
						SET_SKYFREEZE_CLEAR(TRUE)
						
						ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
						
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Starting playback of winner screen.")
						
						ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
						RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
						
						MP_TEXT_CHAT_DISABLE(TRUE)
						
						sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
					ENDIF
				BREAK
				
				CASE CELEBRATION_STAGE_PLAYING
					DRAW_CELEBRATION_SCREEN_WITH_PED_MASKING_THIS_FRAME(sCelebrationData, sCelebrationData.pedWinnerClone)
					
					//Once the timer finishes then progress: this depends on how many elements were added to the screen.
					IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration)
						sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
					ENDIF
				BREAK
				
				CASE CELEBRATION_STAGE_FINISHED
					RESET_CELEBRATION_SCREEN_STATE(sCelebrationData)
					MP_TEXT_CHAT_DISABLE(FALSE)
					eGenericMGStage = GENERIC_CELEBRATION_STATS
					sCelebrationData.eCurrentStage = CELEBRATION_STAGE_SETUP
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GENERIC_CELEBRATION_STATS
			//Handle the screen flow.
			SWITCH sCelebrationData.eCurrentStage
				CASE CELEBRATION_STAGE_SETUP
					//Place a camera in front of the player if safe to do so.
					BOOL bPlacedCameraSuccessfully
					
					#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D("CELEBRATION_STAGE_SETUP", <<0.5, 0.7, 0.0>>)
					PRINTLN("CELEBRATION_STAGE_SETUP")
					#ENDIF
					
					IF NOT bUseCustomCams
						IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camEndScreen, bPlacedCameraSuccessfully)
							//ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE) //removed for Thom feedback
							REQUEST_CELEBRATION_SCREEN(sCelebrationData)
							SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
							KILL_UI_FOR_CELEBRATION_SCREEN()
							HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
							IF bPlacedCameraSuccessfully
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
							ENDIF
							
							sCelebrationData.eCurrentStage = CELEBRATION_STAGE_TRANSITIONING
						ENDIF
					ELSE
						//ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE) //removed for Thom feedback
						REQUEST_CELEBRATION_SCREEN(sCelebrationData)
						SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
						KILL_UI_FOR_CELEBRATION_SCREEN()
						HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
						sCelebrationData.eCurrentStage = CELEBRATION_STAGE_TRANSITIONING
					ENDIF
				BREAK
				
				CASE CELEBRATION_STAGE_TRANSITIONING
					//Set up all the assets and begin when ready.
					REQUEST_CELEBRATION_SCREEN(sCelebrationData)
					
					#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D("CELEBRATION_STAGE_TRANSITIONING", <<0.5, 0.7, 0.0>>)
					PRINTLN("CELEBRATION_STAGE_TRANSITIONING")
					#ENDIF
					
					IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
					AND IS_SCREEN_FADED_IN()
					AND NOT ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
					
						STRING strScreenName, strBackgroundColour
						INT iCurrentRP, iCurrentLvl, iNextLvl, iRPToReachCurrentLvl, iRPToReachNextLvl, iXPToShow
						
						TEXT_LABEL_63 strWinnerName
						TEXT_LABEL_63 strCrewName
						
						strWinnerName = GET_PLAYER_NAME(PLAYER_ID())
						strCrewName = GET_CREW_NAME_FOR_CELEBRATION_SCREEN(PLAYER_ID())
						sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2 
						
						//Retrieve all the required stats for the race end screen.
						strScreenName = "SUMMARY"
						strBackgroundColour = "HUD_COLOUR_BLACK"
						iCurrentRP = GET_PLAYER_FM_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) 
						iXPToShow = GIVE_LOCAL_PLAYER_XP(eXPType, tl_23XpType, anXPType, anXPCategory, iXPGiven)
						iCurrentLvl = GET_FM_RANK_FROM_XP_VALUE(iCurrentRP) //GET_PLAYER_GLOBAL_RANK(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) //We don't want the current level, but their level before the XP was given.
						iNextLvl = iCurrentLvl + 1
						iRPToReachCurrentLvl = GET_XP_NEEDED_FOR_FM_RANK(iCurrentLvl)
						iRPToReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(iNextLvl)
						sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
						
						IF NOT IS_ACCOUNT_OVER_17()
						OR NOT IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
							strCrewName = ""
						ENDIF
						
						//Build the screen
						CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour)
						
						IF eJobWinStatus = JOB_STATUS_DRAW
							// if it's a draw, pass in blank strings for name and crew name
							ADD_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, eJobWinStatus, "", "", "", iBetWinnings, TRUE)
						ELSE
							ADD_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, eJobWinStatus, strWinnerName, strCrewName,  "", iBetWinnings, TRUE)
						ENDIF
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
						
						IF iTotalGames > 0
							ADD_GAMES_WON_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, iGamesWon, iTotalGames)
							sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
						ENDIF
						
						ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, iXPTOShow, iCurrentRP, 
																			iRPToReachCurrentLvl, iRPToReachNextLvl, iCurrentLvl, iNextLvl)
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME// * 2
						
						//If the player levelled up we need to add a bit more to the duration.
						IF iCurrentRP + iXPToShow >= iRPToReachNextLvl
							sCelebrationData.iEstimatedScreenDuration += ROUND(TO_FLOAT(CELEBRATION_SCREEN_STAT_DISPLAY_TIME) / 1.75)
						ENDIF
						
						ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
						SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
						
						RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
						START_NET_TIMER(sCelebrationData.sCelebrationTimer)
						
						PRINTLN("[NETCELEBRATION] Adding stats ---------------------------")
						PRINTLN("iCurrentRP ", iCurrentRP)
						PRINTLN("iXPToShow ", iXPToShow)
						PRINTLN("iCurrentLvl ", iCurrentLvl)
						PRINTLN("iNextLvl ", iNextLvl)
						PRINTLN("iRPToReachCurrentLvl ", iRPToReachCurrentLvl)
						PRINTLN("iRPToReachNextLvl ", iRPToReachNextLvl)
						PRINTLN("---------------------------------------------------------")
												
						IF CONTENT_IS_USING_ARENA()
							START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						ELSE
							START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						ENDIF
						
						RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
						sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
						MP_TEXT_CHAT_DISABLE(TRUE)
						
					ELSE
						
						IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
							IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.timerDeathPostFxDelay)
								START_NET_TIMER(sCelebrationData.timerDeathPostFxDelay)
								PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, started timer timerDeathPostFxDelay.")
							ELSE
								IF HAS_NET_TIMER_EXPIRED(sCelebrationData.timerDeathPostFxDelay, 1000)
									PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, timer timerDeathPostFxDelay >= 1000. Calling CLEAR_KILL_STRIP_DEATH_EFFECTS.")
									CLEAR_KILL_STRIP_DEATH_EFFECTS()
								ENDIF
							ENDIF	
						ENDIF
				
					ENDIF
				BREAK
				
				CASE CELEBRATION_STAGE_PLAYING
					DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
					
					#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D("CELEBRATION_STAGE_PLAYING", <<0.5, 0.7, 0.0>>)
					PRINTLN("CELEBRATION_STAGE_PLAYING")
					#ENDIF
					
					//Once the timer finishes then progress: this depends on how many elements were added to the screen.
					IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration)
						//ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE)
						//PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
						
						IF bEarlyReturn
							
							ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
							CLEAR_ALL_BIG_MESSAGES()
							STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
							CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "SUMMARY")
							SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
							
							//NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, TRUE, FALSE, TRUE)
							
							IF DOES_CAM_EXIST(camEndScreen)
								DESTROY_CAM(camEndScreen, TRUE)
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
								SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							ENDIF
						
							sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
							
							IF CONTENT_IS_USING_ARENA()
								STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
							ELSE
								STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
							ENDIF
							
							RETURN TRUE
							
						ELSE
							
							IF NOT bUseCustomCams
								START_CELEBRATION_CAMERA_ZOOM_TRANSITION(camEndScreen)
							ENDIF
							
							RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
							START_NET_TIMER(sCelebrationData.sCelebrationTimer)
						
							sCelebrationData.eCurrentStage = CELEBRATION_STAGE_END_TRANSITION
						ENDIF
					ENDIF
				BREAK
				
				CASE CELEBRATION_STAGE_END_TRANSITION
					DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
					
					#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D("CELEBRATION_STAGE_END_TRANSITION", <<0.5, 0.7, 0.0>>)
					PRINTLN("CELEBRATION_STAGE_END_TRANSITION")
					#ENDIF
					
					//IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 1000)
						ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
						CLEAR_ALL_BIG_MESSAGES()
						STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
						CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "SUMMARY")
						SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
						
						//NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, TRUE, FALSE, TRUE)
						
						IF DOES_CAM_EXIST(camEndScreen)
							DESTROY_CAM(camEndScreen, TRUE)
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						ENDIF
					
						sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
						
						IF CONTENT_IS_USING_ARENA()
							STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						ELSE
							STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						ENDIF
						
						RETURN TRUE
					//ENDIF
				BREAK
				
				CASE CELEBRATION_STAGE_FINISHED
					
					#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D("CELEBRATION_STAGE_FINISHED", <<0.5, 0.7, 0.0>>)
					PRINTLN("CELEBRATION_STAGE_FINISHED")
					#ENDIF
					
					MP_TEXT_CHAT_DISABLE(FALSE)
					
					IF CONTENT_IS_USING_ARENA()
						STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
					ELSE
						STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
					ENDIF
					
					RETURN TRUE
				BREAK
				
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	IF sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP
	AND sCelebrationData.eCurrentStage < CELEBRATION_STAGE_FINISHED
		IF DOES_CAM_EXIST(camEndScreen)
			IF IS_CAM_RENDERING(camEndScreen)
				UPDATE_CELEBRATION_WINNER_ANIMS(sCelebrationData, sCelebrationData.pedWinnerClone, iWinningPlayer, 0, FALSE, FALSE, FALSE, PAIRED_CELEBRATION_GENDER_COMBO_NOT_SET)
			ENDIF
		ENDIF
	ENDIF
					
	RETURN FALSE
ENDFUNC


PROC PLAY_HEIST_FAIL_FX(STRING fx)
	
	IF ARE_STRINGS_EQUAL(fx,"HeistCelebFail")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
	ELIF ARE_STRINGS_EQUAL(fx,"HeistCelebFailBW")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
		ANIMPOSTFX_STOP("HeistCelebFail")
	ELIF ARE_STRINGS_EQUAL(fx,"HeistCelebEnd")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
		ANIMPOSTFX_STOP("HeistCelebFail")
		ANIMPOSTFX_STOP("HeistCelebFailBW")
	ENDIF
	
	CLEAR_KILL_STRIP_DEATH_EFFECTS()
	
	ANIMPOSTFX_PLAY(fx, 0, TRUE)
	PRINTLN("[NETCELEBRATION] - PLAY_HEIST_FAIL_FX - ",fx)
	
ENDPROC

PROC PLAY_HEIST_PASS_FX(STRING fx)
	
	IF ARE_STRINGS_EQUAL(fx,"HeistCelebPass")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
	ELIF ARE_STRINGS_EQUAL(fx,"HeistCelebPassBW")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
		ANIMPOSTFX_STOP("HeistCelebPass")
	ELIF ARE_STRINGS_EQUAL(fx,"HeistCelebEnd")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
		ANIMPOSTFX_STOP("HeistCelebPass")
		ANIMPOSTFX_STOP("HeistCelebPassBW")
	ELIF ARE_STRINGS_EQUAL(fx,"HeistCelebToast")
		ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
		ANIMPOSTFX_STOP("HeistCelebPass")
		ANIMPOSTFX_STOP("HeistCelebPassBW")
		ANIMPOSTFX_STOP("HeistCelebEnd")
	ENDIF
	
	ANIMPOSTFX_PLAY(fx, 0, TRUE)
	PRINTLN("[NETCELEBRATION] - PLAY_HEIST_PASS_FX - ",fx)
	
ENDPROC

#IF IS_DEBUG_BUILD

	PROC CREATE_CELEBRATION_SCREEN_WIDGETS()
		#IF ALLOW_CELEBRATION_WIDGETS
			sCelebrationWidgetData.iCelebScreenFinishPos = 1
		#ENDIF
		
//		INT i
		
		START_WIDGET_GROUP("Celebration Screen")
			ADD_WIDGET_BOOL("Force Celebration Screen Active", g_bCelebrationScreenIsActive)
//			ADD_WIDGET_VECTOR_SLIDER("Name Size 1", vNamesMovieSize1, -1000.0, 1000.0, 0.01)
//			ADD_WIDGET_VECTOR_SLIDER("Name Size 2", vNamesMovieSize2, -1000.0, 1000.0, 0.01)
			
			#IF ALLOW_CELEBRATION_WIDGETS
				ADD_WIDGET_BOOL("Request Celebration Screen", sCelebrationWidgetData.bDebugEnableCelebScreen)
				ADD_WIDGET_BOOL("Trigger Celebration Screen", sCelebrationWidgetData.bDebugTriggerCelebScreen)
				
				ADD_WIDGET_STRING("Celebration Screen Blocks: select which stats you want to display")
				
				ADD_WIDGET_BOOL("Include Finish Position", sCelebrationWidgetData.bCelebScreenAddFinishPos)
				ADD_WIDGET_BOOL("Include Score", sCelebrationWidgetData.bCelebScreenAddScore)
				ADD_WIDGET_BOOL("Include Time", sCelebrationWidgetData.bCelebScreenAddTime)
				ADD_WIDGET_BOOL("Include World Record", sCelebrationWidgetData.bCelebScreenAddWorldRecord)
				ADD_WIDGET_BOOL("Include Job Points", sCelebrationWidgetData.bCelebScreenAddJobPoints)
				ADD_WIDGET_BOOL("Include Reputation Points", sCelebrationWidgetData.bCelebScreenAddRepPoints)
				ADD_WIDGET_BOOL("Include Rank Bar", sCelebrationWidgetData.bCelebScreenAddRankBar)
				ADD_WIDGET_BOOL("Include Cash", sCelebrationWidgetData.bCelebScreenAddCash)
				ADD_WIDGET_BOOL("Include Challenge Part", sCelebrationWidgetData.bCelebScreenAddChallengePart)
				ADD_WIDGET_BOOL("Include Winner", sCelebrationWidgetData.bCelebScreenAddWinner)
				ADD_WIDGET_BOOL("Include Background", sCelebrationWidgetData.bCelebScreenAddBackground)
				
				ADD_WIDGET_STRING("Block data")
				
				sCelebrationWidgetData.textWidgetBackgroundColour = ADD_TEXT_WIDGET("Background Colour")
				sCelebrationWidgetData.textWidgetWinnerName = ADD_TEXT_WIDGET("Winner Name")
				sCelebrationWidgetData.textWidgetCrewName = ADD_TEXT_WIDGET("Winner Crew Name")
				ADD_WIDGET_INT_SLIDER("Finish Position", sCelebrationWidgetData.iCelebScreenFinishPos, 1, 64, 1)
				ADD_WIDGET_INT_SLIDER("Score", sCelebrationWidgetData.iCelebScreenScore, 0, 100000, 10)
				ADD_WIDGET_INT_SLIDER("Finish Time (ms)", sCelebrationWidgetData.iCelebScreenFinishTime, 0, 10000000, 100)
				ADD_WIDGET_INT_SLIDER("Personal Best (ms)", sCelebrationWidgetData.iCelebScreenPersonalBest, 0, 10000000, 100)
				ADD_WIDGET_INT_SLIDER("Job Points", sCelebrationWidgetData.iCelebScreenJobPoints, 0, 1000, 1)
				ADD_WIDGET_INT_SLIDER("Reputation Points", sCelebrationWidgetData.iCelebScreenRepPoints, 0, 100000, 10)
				ADD_WIDGET_INT_SLIDER("Cash", sCelebrationWidgetData.iCelebScreenCash, 0, 100000, 10)
				ADD_WIDGET_INT_SLIDER("Bet Winnings", sCelebrationWidgetData.iCelebScreenBetWinnings, -100000, 100000, 10)
				ADD_WIDGET_INT_SLIDER("Current Rank", sCelebrationWidgetData.iCelebScreenCurrentLvl, 0, 200, 1)
				ADD_WIDGET_INT_SLIDER("Next Rank", sCelebrationWidgetData.iCelebScreenNextLvl, 0, 200, 1)
				ADD_WIDGET_INT_SLIDER("Total RP Before Job", sCelebrationWidgetData.iCelebScreenPointsBeforeRP, 0, 10000000, 100)
				ADD_WIDGET_INT_SLIDER("Total RP To Reach Current Lvl", sCelebrationWidgetData.iCelebScreenCurrentLvlRPStart, 0, 10000000, 100)
				ADD_WIDGET_INT_SLIDER("Total RP To Reach Next Lvl", sCelebrationWidgetData.iCelebScreenNextLvlRPStart, 0, 10000000, 100)
				ADD_WIDGET_INT_SLIDER("Player Win Status", sCelebrationWidgetData.iCelebScreenWinStatus, 0, 2, 1)
				ADD_WIDGET_INT_SLIDER("Challenge Type", sCelebrationWidgetData.iCelebScreenChallengeType, 0, 1, 1)
				ADD_WIDGET_INT_SLIDER("Challenge Part", sCelebrationWidgetData.iCelebScreenChallengePart, 0, 20, 1)
				ADD_WIDGET_INT_SLIDER("Total Challenge Parts", sCelebrationWidgetData.iCelebScreenTotalChallengePart, 0, 20, 1)
				ADD_WIDGET_INT_SLIDER("Background Alpha", sCelebrationWidgetData.iCelebScreenBackgroundAlpha, 0, 255, 1)
				ADD_WIDGET_BOOL("Won Challenge Part", sCelebrationWidgetData.bCelebScreenWonChallengePart)
				
				START_WIDGET_GROUP("Winner Screen Ped")
					ADD_WIDGET_BOOL("Test Winner Ped Placement", sCelebrationWidgetData.bDebugTestWinnerScreenPlacement)
					ADD_WIDGET_BOOL("Draw scaleform background in 3D", sCelebrationWidgetData.bDebugUseWinnerRenderTarget)
					ADD_WIDGET_BOOL("Draw black background", sCelebrationWidgetData.bCelebScreenDrawBlackBackground)
					ADD_WIDGET_BOOL("Use car in winner scene", sCelebrationWidgetData.bCelebScreenUseVehicleForWinnerScreen)
					ADD_WIDGET_BOOL("Output ped offsets", sCelebrationWidgetData.bDebugOutputPedOffsets)
					ADD_WIDGET_INT_SLIDER("Background brightness", sCelebrationWidgetData.iBackgroundBrightness, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Draw mode test 1", sCelebrationWidgetData.iCelebScreenDrawModeTest1, -1, 3, 1)
					ADD_WIDGET_INT_SLIDER("Draw mode test 2", sCelebrationWidgetData.iCelebScreenDrawModeTest2, -1, 3, 1)
					ADD_WIDGET_BOOL("Draw light", sCelebrationWidgetData.bDebugDrawWinnerLight)
					ADD_WIDGET_VECTOR_SLIDER("Background offset", sCelebrationWidgetData.vDebugBackgroundOffset, -10.0, 10.0, 0.01)
					ADD_WIDGET_VECTOR_SLIDER("Background rotation", sCelebrationWidgetData.vDebugBackgroundRot, -360.0, 360.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Background size", sCelebrationWidgetData.vDebugBackgroundSize, -50.0, 50.0, 0.01)
					
					
					START_WIDGET_GROUP("Light 1")
						ADD_WIDGET_VECTOR_SLIDER("Light offset", sCelebrationWidgetData.vDebugLightOffset[0], -50.0, 50.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("Light range", sCelebrationWidgetData.fDebugLightRange[0], 0.0, 100.0, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("Light intensity", sCelebrationWidgetData.fDebugLightIntensity[0], 0.0, 100.0, 0.01)
						ADD_WIDGET_INT_SLIDER("Light colour R", sCelebrationWidgetData.iDebugLightColourR[0], 0, 255, 1)
						ADD_WIDGET_INT_SLIDER("Light colour G", sCelebrationWidgetData.iDebugLightColourG[0], 0, 255, 1)
						ADD_WIDGET_INT_SLIDER("Light colour B", sCelebrationWidgetData.iDebugLightColourB[0], 0, 255, 1)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Light 2")
						ADD_WIDGET_VECTOR_SLIDER("Light offset", sCelebrationWidgetData.vDebugLightOffset[1], -50.0, 50.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("Light range", sCelebrationWidgetData.fDebugLightRange[1], 0.0, 100.0, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("Light intensity", sCelebrationWidgetData.fDebugLightIntensity[1], 0.0, 100.0, 0.01)
						ADD_WIDGET_INT_SLIDER("Light colour R", sCelebrationWidgetData.iDebugLightColourR[1], 0, 255, 1)
						ADD_WIDGET_INT_SLIDER("Light colour G", sCelebrationWidgetData.iDebugLightColourG[1], 0, 255, 1)
						ADD_WIDGET_INT_SLIDER("Light colour B", sCelebrationWidgetData.iDebugLightColourB[1], 0, 255, 1)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Light 3")
						ADD_WIDGET_VECTOR_SLIDER("Light offset", sCelebrationWidgetData.vDebugLightOffset[2], -50.0, 50.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("Light range", sCelebrationWidgetData.fDebugLightRange[2], 0.0, 100.0, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("Light intensity", sCelebrationWidgetData.fDebugLightIntensity[2], 0.0, 100.0, 0.01)
						ADD_WIDGET_INT_SLIDER("Light colour R", sCelebrationWidgetData.iDebugLightColourR[2], 0, 255, 1)
						ADD_WIDGET_INT_SLIDER("Light colour G", sCelebrationWidgetData.iDebugLightColourG[2], 0, 255, 1)
						ADD_WIDGET_INT_SLIDER("Light colour B", sCelebrationWidgetData.iDebugLightColourB[2], 0, 255, 1)
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Summary cam")
					ADD_WIDGET_BOOL("Override cam indices", sCelebrationWidgetData.bCelebScreenOverrideSummaryCams)
					ADD_WIDGET_BOOL("Use parachute cams", sCelebrationWidgetData.bCelebScreenUseParachuteSummaryCams)
					ADD_WIDGET_INT_SLIDER("Main cam index", sCelebrationWidgetData.iCelebScreenCamMainIndex, 0, 6, 1)
					ADD_WIDGET_INT_SLIDER("Random cam index", sCelebrationWidgetData.iCelebScreenCamRandomIndex, 0, 6, 1)
				STOP_WIDGET_GROUP()
//				
//				START_WIDGET_GROUP("Player Name Widgets")
//					TEXT_LABEL_63 tl63Temp
//					REPEAT MAX_NUM_CELEBRATION_PEDS i
//						tl63Temp = "Name Scale X "
//						tl63Temp += i
//						ADD_WIDGET_FLOAT_SLIDER(tl63Temp, fScaleX[i], -10000.0, 10000.0, 0.01)
//						tl63Temp = "Name Scale Y "
//						tl63Temp += i
//						ADD_WIDGET_FLOAT_SLIDER(tl63Temp, fScaleY[i], -10000.0, 10000.0, 0.01)
//						tl63Temp = "Name Offset Z "
//						tl63Temp += i
//						ADD_WIDGET_FLOAT_SLIDER(tl63Temp, fXTemp[i], -10000.0, 10000.0, 0.01)
////						tl63Temp = "Name Offset Y "
////						tl63Temp += i
////						ADD_WIDGET_FLOAT_SLIDER(tl63Temp, fYTemp[i], -10000.0, 10000.0, 0.01)
//					ENDREPEAT
//				STOP_WIDGET_GROUP()
//				
			#ENDIF
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC UPDATE_CELEBRATION_SCREEN_WIDGETS()
		#IF ALLOW_CELEBRATION_WIDGETS
			IF sCelebrationWidgetData.bDebugEnableCelebScreen
				//Request Assets.
				REQUEST_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen)
			
				IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationWidgetData.sDebugCelebScreen)
					IF sCelebrationWidgetData.bDebugTriggerCelebScreen
						STRING strBackground = GET_CONTENTS_OF_TEXT_WIDGET(sCelebrationWidgetData.textWidgetBackgroundColour)
						STRING strWinner = GET_CONTENTS_OF_TEXT_WIDGET(sCelebrationWidgetData.textWidgetWinnerName)
						STRING strWinnerCrew = GET_CONTENTS_OF_TEXT_WIDGET(sCelebrationWidgetData.textWidgetCrewName)
						TEXT_LABEL strName = sCelebrationWidgetData.iCurrentScreenID
					
						CREATE_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, strBackground)
					
						//Add relevant blocks and trigger.
						IF sCelebrationWidgetData.bCelebScreenAddFinishPos
							ADD_FINISH_POSITION_TO_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, sCelebrationWidgetData.iCelebScreenFinishPos)
						ENDIF
						
						IF sCelebrationWidgetData.bCelebScreenAddScore
							ADD_SCORE_TO_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, sCelebrationWidgetData.iCelebScreenScore)
						ENDIF
								
						IF sCelebrationWidgetData.bCelebScreenAddChallengePart
							ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, INT_TO_ENUM(CHALLENGE_PART_TYPE, sCelebrationWidgetData.iCelebScreenChallengeType),
																			sCelebrationWidgetData.iCelebScreenChallengePart, sCelebrationWidgetData.iCelebScreenTotalChallengePart, 
																			sCelebrationWidgetData.bCelebScreenWonChallengePart)
						ENDIF
						
						IF sCelebrationWidgetData.bCelebScreenAddTime
							ADD_FINISH_TIME_TO_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, sCelebrationWidgetData.iCelebScreenFinishTime, sCelebrationWidgetData.iCelebScreenPersonalBest)
						ENDIF
						
						IF sCelebrationWidgetData.bCelebScreenAddWorldRecord
							ADD_WORLD_RECORD_TO_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, sCelebrationWidgetData.iCelebScreenFinishTime)
						ENDIF
								
						IF sCelebrationWidgetData.bCelebScreenAddJobPoints
							IF sCelebrationWidgetData.bCelebScreenAddCash
								ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, sCelebrationWidgetData.iCelebScreenJobPoints)
							ELSE
								ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, sCelebrationWidgetData.iCelebScreenJobPoints, TRUE, FALSE)
							ENDIF
						ENDIF
						
						IF sCelebrationWidgetData.bCelebScreenAddCash
							ADD_CASH_TO_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, sCelebrationWidgetData.iCelebScreenCash)
						ENDIF
						
						IF sCelebrationWidgetData.bCelebScreenAddRankBar
							ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, sCelebrationWidgetData.iCelebScreenRepPoints, sCelebrationWidgetData.iCelebScreenPointsBeforeRP,
															   sCelebrationWidgetData.iCelebScreenCurrentLvlRPStart, sCelebrationWidgetData.iCelebScreenNextLvlRPStart,
															   sCelebrationWidgetData.iCelebScreenCurrentLvl, sCelebrationWidgetData.iCelebScreenNextLvl)
						ENDIF
						
						IF sCelebrationWidgetData.bCelebScreenAddWinner
							ADD_WINNER_TO_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, INT_TO_ENUM(JOB_WIN_STATUS, sCelebrationWidgetData.iCelebScreenWinStatus),
															 strWinner, strWinnerCrew, "", sCelebrationWidgetData.iCelebScreenBetWinnings)
						ENDIF
						
						IF sCelebrationWidgetData.bCelebScreenAddBackground
							ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName, sCelebrationWidgetData.iCelebScreenBackgroundAlpha)
						ENDIF
						
						SHOW_CELEBRATION_SCREEN(sCelebrationWidgetData.sDebugCelebScreen, strName)
					
						sCelebrationWidgetData.bDebugTriggerCelebScreen = FALSE
						sCelebrationWidgetData.iCurrentScreenID++
					ENDIF
					
					IF sCelebrationWidgetData.bDebugUseWinnerRenderTarget
						DRAW_CELEBRATION_SCREEN_USING_3D_BACKGROUND(sCelebrationWidgetData.sDebugCelebScreen, sCelebrationWidgetData.vDebugBackgroundOffset, 
																	sCelebrationWidgetData.vDebugBackgroundRot, sCelebrationWidgetData.vDebugBackgroundSize)
					ELSE
						DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationWidgetData.sDebugCelebScreen, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF sCelebrationWidgetData.bDebugTestWinnerScreenPlacement
				
				IF NOT DOES_ENTITY_EXIST(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground)
					
					PLAYER_INDEX piPlayersTest[MAX_NUM_CELEBRATION_PEDS]
					CAMERA_INDEX camTest
					
					piPlayersTest[0] = PLAYER_ID()
					piPlayersTest[1] = PLAYER_ID()
					piPlayersTest[2] = PLAYER_ID()
					piPlayersTest[3] = PLAYER_ID()
					piPlayersTest[4] = PLAYER_ID()
					piPlayersTest[5] = PLAYER_ID()
					piPlayersTest[6] = PLAYER_ID()
					piPlayersTest[7] = PLAYER_ID()
					sCelebrationWidgetData.sDebugCelebScreen.tl31_pedWinnerClonesNames[0] = "WWWWWWWWWWWWWWWW"
					sCelebrationWidgetData.sDebugCelebScreen.tl31_pedWinnerClonesNames[1] = "WWWWWWWWWWWWWWWW"
					sCelebrationWidgetData.sDebugCelebScreen.tl31_pedWinnerClonesNames[2] = "WWWWWWWWWWWWWWWW"
					sCelebrationWidgetData.sDebugCelebScreen.tl31_pedWinnerClonesNames[3] = "WWWWWWWWWWWWWWWW"
					sCelebrationWidgetData.sDebugCelebScreen.tl31_pedWinnerClonesNames[4] = "WWWWWWWWWWWWWWWW"
					sCelebrationWidgetData.sDebugCelebScreen.tl31_pedWinnerClonesNames[5] = "WWWWWWWWWWWWWWWW"
					sCelebrationWidgetData.sDebugCelebScreen.tl31_pedWinnerClonesNames[6] = "WWWWWWWWWWWWWWWW"
					sCelebrationWidgetData.sDebugCelebScreen.tl31_pedWinnerClonesNames[7] = "WWWWWWWWWWWWWWWW"
					
					IF sCelebrationWidgetData.bCelebScreenUseVehicleForWinnerScreen
						IF NOT DOES_ENTITY_EXIST(sCelebrationWidgetData.vehDebugCelebWinnerTest)
							REQUEST_MODEL(TAILGATER)
							
							WHILE NOT HAS_MODEL_LOADED(TAILGATER)
								WAIT(0)
							ENDWHILE
							
							sCelebrationWidgetData.vehDebugCelebWinnerTest = CREATE_VEHICLE(TAILGATER, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, 20.0>>)
							SET_MODEL_AS_NO_LONGER_NEEDED(TAILGATER)
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(sCelebrationWidgetData.vehDebugCelebWinnerTest)
							DELETE_VEHICLE(sCelebrationWidgetData.vehDebugCelebWinnerTest)
							sCelebrationWidgetData.vehDebugCelebWinnerTest = NULL
						ENDIF
					ENDIF
				
					IF CREATE_WINNER_SCENE_ENTITIES(sCelebrationWidgetData.sDebugCelebScreen, piPlayersTest, NULL, FALSE, sCelebrationWidgetData.vehDebugCelebWinnerTest)
						
						PLACE_WINNER_SCENE_CAMERA(sCelebrationWidgetData.sDebugCelebScreen, camTest)
						
						sCelebrationWidgetData.vDebugBackgroundOffset = <<0.0, -0.5, 0.0>>
						sCelebrationWidgetData.vDebugBackgroundRot = <<0.0, 0.0, 0.0>>
						sCelebrationWidgetData.vDebugBackgroundSize = <<10.0, 5.0, 5.0>>
						
						sCelebrationWidgetData.bDebugTestWinnerScreenPlacement = FALSE
					ENDIF
				ENDIF
			ENDIF
			
//			IF IS_XBOX360_VERSION()
//			OR IS_PS3_VERSION()
//				DISPLAY_CELEBRATION_PLAYER_NAMES(sCelebrationWidgetData.sDebugCelebScreen.pedWinnerClones, sCelebrationWidgetData.sDebugCelebScreen.tl31_pedWinnerClonesNames, sCelebrationWidgetData.sDebugCelebScreen.eCurrentStage, sCelebrationWidgetData.sDebugCelebScreen.sfCelebration)
//			ELSE
//				DRAW_NG_CELEBRATION_PLAYER_NAMES(	sCelebrationWidgetData, sCelebrationWidgetData.sDebugCelebScreen.iDrawNamesStage, sCelebrationWidgetData.sDebugCelebScreen.iNumNamesToDisplay, 
//													sCelebrationWidgetData.sDebugCelebScreen.pedWinnerClones, sCelebrationWidgetData.sDebugCelebScreen.tl31_pedWinnerClonesNames, 
//													sCelebrationWidgetData.sDebugCelebScreen.eCurrentStage, sCelebrationWidgetData.sDebugCelebScreen.sfCelebration,
//													sCelebrationWidgetData.sDebugCelebScreen.playerNameMovies)
//			ENDIF
			
			IF sCelebrationWidgetData.bDebugDrawWinnerLight
				IF DOES_ENTITY_EXIST(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground)
					DRAW_LIGHT_WITH_RANGE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground, sCelebrationWidgetData.vDebugLightOffset[0]),
										  sCelebrationWidgetData.iDebugLightColourR[0], sCelebrationWidgetData.iDebugLightColourG[0], sCelebrationWidgetData.iDebugLightColourB[0], 
										  sCelebrationWidgetData.fDebugLightRange[0], sCelebrationWidgetData.fDebugLightIntensity[0]) 
										  
					DRAW_LIGHT_WITH_RANGE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground, sCelebrationWidgetData.vDebugLightOffset[1]),
										  sCelebrationWidgetData.iDebugLightColourR[1], sCelebrationWidgetData.iDebugLightColourG[1], sCelebrationWidgetData.iDebugLightColourB[1], 
										  sCelebrationWidgetData.fDebugLightRange[1], sCelebrationWidgetData.fDebugLightIntensity[1]) 
										  
					DRAW_LIGHT_WITH_RANGE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground, sCelebrationWidgetData.vDebugLightOffset[2]),
										  sCelebrationWidgetData.iDebugLightColourR[2], sCelebrationWidgetData.iDebugLightColourG[2], sCelebrationWidgetData.iDebugLightColourB[2], 
										  sCelebrationWidgetData.fDebugLightRange[2], sCelebrationWidgetData.fDebugLightIntensity[2]) 
										  
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground, sCelebrationWidgetData.vDebugLightOffset[0]), 0.1, 0, 0, 255, 255)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground, sCelebrationWidgetData.vDebugLightOffset[1]), 0.1, 0, 0, 255, 255)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground, sCelebrationWidgetData.vDebugLightOffset[2]), 0.1, 0, 0, 255, 255)
					
					//DRAW_POLY_ANGLED_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground, sCelebrationWidgetData.vDebugBackgroundOffset), 
					//		  			  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground, sCelebrationWidgetData.vDebugBackgroundRot),
					//		 			  sCelebrationWidgetData.fDebugLightRange, 0, 0, 0, 255, FALSE, TRUE)
				ENDIF
			ENDIF
			
			IF sCelebrationWidgetData.bDebugOutputPedOffsets
				INT i = 0
				REPEAT COUNT_OF(sCelebrationWidgetData.sDebugCelebScreen.pedWinnerClones) i
					IF NOT IS_PED_INJURED(sCelebrationWidgetData.sDebugCelebScreen.pedWinnerClones[i])
						SAVE_NEWLINE_TO_DEBUG_FILE()
		            
			            SAVE_STRING_TO_DEBUG_FILE("Offset ")
						SAVE_INT_TO_DEBUG_FILE(i)
						SAVE_STRING_TO_DEBUG_FILE(": ")
			            SAVE_VECTOR_TO_DEBUG_FILE(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground, 
											      GET_ENTITY_COORDS(sCelebrationWidgetData.sDebugCelebScreen.pedWinnerClones[i])))
						SAVE_STRING_TO_DEBUG_FILE("  ")
						SAVE_VECTOR_TO_DEBUG_FILE(GET_ENTITY_ROTATION(sCelebrationWidgetData.sDebugCelebScreen.pedWinnerClones[i]))
			            
			            SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDREPEAT
				
				IF NOT IS_ENTITY_DEAD(sCelebrationWidgetData.sDebugCelebScreen.vehWinnerClone)
					SAVE_NEWLINE_TO_DEBUG_FILE()
		        
			        SAVE_STRING_TO_DEBUG_FILE("Car Offset ")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_STRING_TO_DEBUG_FILE(": ")
			        SAVE_VECTOR_TO_DEBUG_FILE(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sCelebrationWidgetData.sDebugCelebScreen.objWinnerBackground, 
										      GET_ENTITY_COORDS(sCelebrationWidgetData.sDebugCelebScreen.vehWinnerClone)))
					SAVE_STRING_TO_DEBUG_FILE("  ")
					SAVE_VECTOR_TO_DEBUG_FILE(GET_ENTITY_ROTATION(sCelebrationWidgetData.sDebugCelebScreen.vehWinnerClone))
			        
			        SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				sCelebrationWidgetData.bDebugOutputPedOffsets = FALSE
			ENDIF
		#ENDIF
	ENDPROC
#ENDIF

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				ARENA CELEBRATION SHARDS/WINNER SCREENS							║


PROC FEED_ARENA_POINTS_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, ARENA_POINTS &sArenaPoints)
	sCelebrationData.ArenaPoints.iAPGained		= sArenaPoints.iAPGained		
	sCelebrationData.ArenaPoints.iAPStart		= sArenaPoints.iAPStart		
	sCelebrationData.ArenaPoints.iAPMin			= sArenaPoints.iAPMin			
	sCelebrationData.ArenaPoints.iAPMax			= sArenaPoints.iAPMax			
	sCelebrationData.ArenaPoints.iRankCurrent	= sArenaPoints.iRankCurrent	
	sCelebrationData.ArenaPoints.iRankNext		= sArenaPoints.iRankNext		
	
	
	PRINTLN("FEED_ARENA_POINTS_TO_CELEBRATION_SCREEN, iAPGained    = ", sCelebrationData.ArenaPoints.iAPGained)
	PRINTLN("FEED_ARENA_POINTS_TO_CELEBRATION_SCREEN, iAPStart     = ", sCelebrationData.ArenaPoints.iAPStart)
	PRINTLN("FEED_ARENA_POINTS_TO_CELEBRATION_SCREEN, iAPMin       = ", sCelebrationData.ArenaPoints.iAPMin)
	PRINTLN("FEED_ARENA_POINTS_TO_CELEBRATION_SCREEN, iAPMax       = ", sCelebrationData.ArenaPoints.iAPMax)
	PRINTLN("FEED_ARENA_POINTS_TO_CELEBRATION_SCREEN, iRankCurrent = ", sCelebrationData.ArenaPoints.iRankCurrent)
	PRINTLN("FEED_ARENA_POINTS_TO_CELEBRATION_SCREEN, iRankNext    = ", sCelebrationData.ArenaPoints.iRankNext)

	
ENDPROC

PROC POPULATE_ARENA_POINTS_METHODS(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(strScreenID)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sCelebrationData.ArenaPoints.iAPGained) // Arena Points Gained
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sCelebrationData.ArenaPoints.iAPStart) // Start Arena Points
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sCelebrationData.ArenaPoints.iAPMin) // Min Arena Points for Rank
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sCelebrationData.ArenaPoints.iAPMax) // Max Arena Points for Rank
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sCelebrationData.ArenaPoints.iRankCurrent) // Current Rank
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sCelebrationData.ArenaPoints.iRankNext) // Next Rank
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CLB_RNKA") // TIER
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CLB_RNKB") // LEVEL
ENDPROC

PROC ADD_ARENA_POINTS_BAR_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID)

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfForeground, "ADD_ARENA_POINTS_AND_RANK_BAR_TO_WALL") 
		POPULATE_ARENA_POINTS_METHODS(sCelebrationData, strScreenID)
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfBackground, "ADD_ARENA_POINTS_AND_RANK_BAR_TO_WALL")
		POPULATE_ARENA_POINTS_METHODS(sCelebrationData, strScreenID)
	END_SCALEFORM_MOVIE_METHOD()

	BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "ADD_ARENA_POINTS_AND_RANK_BAR_TO_WALL")
		POPULATE_ARENA_POINTS_METHODS(sCelebrationData, strScreenID)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

PROC SF_METHOD_ADD_ARENA_UNLOCK_TO_WALL(SCALEFORM_INDEX movie, STRING strScreenID, TEXT_LABEL_15 tlRewardName, TEXT_LABEL_15 tl15RewardSubString, INT iValuePassed)
	BEGIN_SCALEFORM_MOVIE_METHOD(movie, "ADD_ARENA_UNLOCK_TO_WALL") 
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("CLB_UNLOCK")
		
		IF IS_STRING_NULL_OR_EMPTY(tl15RewardSubString)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlRewardName)
				IF iValuePassed > 0
					ADD_TEXT_COMPONENT_INTEGER(iValuePassed)
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlRewardName)
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tl15RewardSubString)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC ADD_ARENA_REWARDS_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, TEXT_LABEL_15 tlRewardName, TEXT_LABEL_15 tl15RewardSubString, INT iValuePassed)

	SF_METHOD_ADD_ARENA_UNLOCK_TO_WALL(sCelebrationData.sfForeground, strScreenID, tlRewardName, tl15RewardSubString, iValuePassed)
	SF_METHOD_ADD_ARENA_UNLOCK_TO_WALL(sCelebrationData.sfBackground, strScreenID, tlRewardName, tl15RewardSubString, iValuePassed)	
	SF_METHOD_ADD_ARENA_UNLOCK_TO_WALL(sCelebrationData.sfCelebration, strScreenID, tlRewardName, tl15RewardSubString, iValuePassed)
	
ENDPROC

FUNC BOOL SHOULD_SHOW_ARENA_WINNER_HEADINGS_FOR_LANGUAGE()
	
	SWITCH GET_CURRENT_LANGUAGE()
		CASE LANGUAGE_JAPANESE
		CASE LANGUAGE_CHINESE
		CASE LANGUAGE_CHINESE_SIMPLIFIED
		CASE LANGUAGE_KOREAN
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE

ENDFUNC

FUNC STRING GET_ARENA_WINNER_SKILL_HEADING()

	IF NOT SHOULD_SHOW_ARENA_WINNER_HEADINGS_FOR_LANGUAGE()
		RETURN "CLB_NUMSTR"
	ENDIF
	
	RETURN "CLB_SKLL"

ENDFUNC

FUNC STRING GET_ARENA_WINNER_VEHICLE_NAME_HEADING()

	IF NOT SHOULD_SHOW_ARENA_WINNER_HEADINGS_FOR_LANGUAGE()
		RETURN "STRING"
	ENDIF
	
	RETURN "CLB_VNME"

ENDFUNC

// WINNER screen with player name, vehicle name and skill title
PROC POPULATE_ARENA_WINNER_METHODS(CELEBRATION_SCREEN_DATA &sCelebrationData, SCALEFORM_INDEX &siMovie, STRING strScreenID, STRING strWinnerName, TEXT_LABEL_63 tl63VehicleName, STRING sSkillLevelTitle, BOOL bWonChallenge, BOOL bWinnerNameIsTextLabel, INT iSkillLevel)

	STRING strWinLoseLabel
	IF bWonChallenge
		strWinLoseLabel = "CELEB_WINNER"
	ELSE
		strWinLoseLabel = "CELEB_LOSER"
	ENDIF
	
	IF (sCelebrationData.iNumPlayerFoundForWinnerScene <= 1)
		IF ARE_STRINGS_EQUAL(strWinLoseLabel, "CELEB_WINNERS")
			strWinLoseLabel = "CELEB_WINNER"
		ELIF ARE_STRINGS_EQUAL(strWinLoseLabel, "CELEB_WINNERS")
			strWinLoseLabel = "CELEB_LOSER"
		ENDIF
	ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "ADD_WINNER_TO_WALL")
	
		// 1, wallId:String
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// 2, winLoseTextLabel:String
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinLoseLabel)
		
		// 3, gamerName:String, 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strWinnerName)
		
		// 4, crewName:String, (Skill Level Title)
		IF iSkillLevel <= 0 
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(GET_ARENA_WINNER_SKILL_HEADING())
					ADD_TEXT_COMPONENT_INTEGER(iSkillLevel)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sSkillLevelTitle)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		// 5, betAmount:Number, 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		
		// 6, isInFlow:Boolean, 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		
		// 7, teamName:String, (Vehicle Name)
		SET_TEXT_FONT(FONT_CONDENSED_NOT_GAMERNAME) 
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(GET_ARENA_WINNER_VEHICLE_NAME_HEADING())
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl63VehicleName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SET_TEXT_FONT(FONT_STANDARD)
		
		// 8, gamerNameIsLabel:Boolean
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bWinnerNameIsTextLabel)
		
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

// url:bugstar:5327210
PROC ADD_ARENA_WINNER_TO_CELEBRATION_SCREEN(CELEBRATION_SCREEN_DATA &sCelebrationData, STRING strScreenID, STRING strWinnerName, BOOL bWonChallenge, TEXT_LABEL_63 tl63VehicleName, STRING sSkillLevelTitle, BOOL bWinnerNameIsTextLabel, INT iSkillLevel)

	POPULATE_ARENA_WINNER_METHODS(sCelebrationData, sCelebrationData.sfForeground, strScreenID, strWinnerName, tl63VehicleName, sSkillLevelTitle, bWonChallenge, bWinnerNameIsTextLabel, iSkillLevel)
	
	POPULATE_ARENA_WINNER_METHODS(sCelebrationData, sCelebrationData.sfBackground, strScreenID, strWinnerName, tl63VehicleName, sSkillLevelTitle, bWonChallenge, bWinnerNameIsTextLabel, iSkillLevel)

	POPULATE_ARENA_WINNER_METHODS(sCelebrationData, sCelebrationData.sfCelebration, strScreenID, strWinnerName, tl63VehicleName, sSkillLevelTitle, bWonChallenge, bWinnerNameIsTextLabel, iSkillLevel)

ENDPROC

FUNC BOOL SHOULD_DO_TRANSITION_MENU_AUDIO()
	IF CONTENT_IS_USING_ARENA()
		PRINTLN("SHOULD_DO_TRANSITION_MENU_AUDIO - Arena Wars mode, not using transition menu audio.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

 // #IF FEATURE_ARENA_WARS

//╚═════════════════════════════════════════════════════════════════════════════╝ // ARENA END



