USING "rage_builtins.sch"
USING "globals.sch"



// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_Info_Private.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Stores any MP Mission Info functions used by the public functions.
//		NOTES			:	This header should only contain global data access routines routines so that it doesn't need to include any
//								other headers. It's used by both the Initialisation and the public header.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Mission Info Helper Functions
//		NOTE: These are required by Keith as part of re-structuring the mission data from SWITCH statements
//				to storage in global data arrays
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Team Data Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the team is the Cop team
//
// INPUT PARAMS:		paramTeamID				The Team ID (ie: the CONSTS that represent the actual team, not the team storage ID used to handle storage array access)
// RETURN VALUE:		BOOL					TRUE if this team ID is a Cop team
//
// NOTES:	Added this function in preparation for having to sort out all the Team data - the current idea being each set of team data will identify the type of team it is (Cop, Crook, Whatever)
FUNC BOOL Is_Team_A_Cop_Team(INT paramTeamID)

	If (paramTeamID = TEAM_INVALID)
		RETURN FALSE
	ENDIF

//	// TEMP: Directly access the team data - when cyclic header issue is resolved then remove this function and all calls and use the team data header instead
//	IF (TeamData[paramTeamID].TeamType = TEAM_TYPE_COP)
//		RETURN TRUE
//	ENDIF
	
	// Not a Cop Team
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the team is a Crook Gang
//
// INPUT PARAMS:		paramTeamID				The Team ID (ie: the CONSTS that represent the actual team, not the team storage ID used to handle storage array access)
// RETURN VALUE:		BOOL					TRUE if this team ID is a Crook Gang
//
// NOTES:	Added this function in preparation for having to sort out all the Team data - the current idea being each set of team data will identify the type of team it is (Cop, Crook, Whatever)
FUNC BOOL Is_Team_A_Crook_Gang(INT paramTeamID)

	If (paramTeamID = TEAM_INVALID)
		RETURN FALSE
	ENDIF

//	// TEMP: Directly access the team data - when cyclic header issue is resolved then remove this function and all calls and use the team data header instead
//	IF (TeamData[paramTeamID].TeamType = TEAM_TYPE_CRIMINAL)
//		RETURN TRUE
//	ENDIF
	
	// Not a Crook Team
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Convert a Team ID used by the game (setup as a Team CONST_INT) to a Team Storage ID used by the Mission Data Storage Routines
//
// INPUT PARAMS:		paramMissionID			The Mission ID
//						paramTeamID				The Team ID (ie: the CONSTS that represent the actual team, not the team storage ID used to handle storage array access)
// RETURN VALUE:		g_eMITeamStorage		The Team Storage ID used to reference team array positions within the mission data storage
FUNC g_eMITeamStorage Convert_Game_Team_ID_To_Storage_ID_For_Mission(MP_MISSION paramMissionID, INT paramTeamID)

	// Even for the same team, the return value will differ depending on the team storage requirements setup for a particular mission.
	// EG: Most Missions will be setup with MIT_CP_CR storage (one struct to hold Cop team data, and one struct to hold Crook Gang data - because data for all Crook Gangs is usually the same)
	//			so passing in the VAGOS team or the LOST team will return MIT_CROOKS because the data is stored on the generic struct used for all Crook Gangs.
	// However, if a mission requires separate storage for individual Crook Gangs (there hasn't been required so far, and hopefully this won't be needed), then passing in the VAGOS team
	//			or the LOST team won't return MIT_CROOKS, but instead a new ID to identify the VAGOS team data storage or the LOST team data storage.
	
	// Need to get the type of team storage allocated for this mission
	g_eMITeamStorage allocatedTeamStorage = g_sMissionInfo[paramMissionID].teamStorage
	
	// Based on the allocated team storage, return the appropriate team storage ID that will be used to access the data on the team arrays
	SWITCH (allocatedTeamStorage)
		// One piece of storage allocated for ALL players to use the same data
		CASE MIT_ALL
			// The team is ignored, so the only storage element allocated should be used by ALL players
			RETURN MIT_ALL
		
		// Storage allocated for Cops only
		CASE MIT_COP
			IF (Is_Team_A_Cop_Team(paramTeamID))
				// This team is a cop team, so access the Cop Team storage
				RETURN MIT_COP
			ENDIF
			
			// Team isn't a Cop Team, so don't allow access
			RETURN NO_MIT_DATA
			
		// Storage allocated for Crooks only - all data for individual Crook Gangs is identical
		CASE MIT_CROOKS
			IF (Is_Team_A_Crook_Gang(paramTeamID))
				// This team is a Crook Gang, so access the Crook Gang storage
				RETURN MIT_CROOKS
			ENDIF
			
			// Team isn't a Crook Gang, so don't allow access
			RETURN NO_MIT_DATA
			
		// Storage allocated for Cops and Crooks only - all data for individual Crook Gangs is identical
		CASE MIT_CP_CR
			IF (Is_Team_A_Cop_Team(paramTeamID))
				// This team is a Cop team, so access the Cop Team storage
				RETURN MIT_COP
			ENDIF
			
			IF (Is_Team_A_Crook_Gang(paramTeamID))
				// This team is a Crook Gang, so access the Crook Gang storage
				RETURN MIT_CROOKS
			ENDIF
			
			// Team isn't a Cop Team or a Crook Gang, so don't allow access
			RETURN NO_MIT_DATA
		
		// Illegal Storage IDs
		CASE NO_MIT_DATA
			RETURN NO_MIT_DATA
	ENDSWITCH
	
	// Unknown
	PRINTSTRING("...KGM MP [MissionInfo]: Convert_Game_Team_ID_To_Storage_ID_For_Mission() - Unknown Team Storage ID. Add to SWITCH statement.") NET_NL()
	SCRIPT_ASSERT("Convert_Game_Team_ID_To_Storage_ID_For_Mission() - Unknown Team Storage ID. Tell Keith.")
	
	RETURN NO_MIT_DATA

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the number of elements of team storage required based on the Team Requirements ID
//
// INPUT PARAMS:			paramTeamStorage		The Team Storage ID - used to decide how many team storage structs are required for this mission
// RETURN VALUE:			INT						The number of elements of storage required
FUNC INT Get_Number_Of_Elements_Of_Team_Storage(g_eMITeamStorage paramTeamStorage)

	SWITCH (paramTeamStorage)
		// These require one element of team storage
		CASE MIT_ALL
		CASE MIT_COP
		CASE MIT_CROOKS
			RETURN 1
	
		// These require two elements of team storage
		CASE MIT_CP_CR
			RETURN 2
		
		// Illegal, so return 0
		CASE NO_MIT_DATA
			RETURN 0
	ENDSWITCH
	
	// Unknown
	PRINTSTRING("...KGM MP [MissionInfo]: Get_Number_Of_Elements_Of_Team_Storage() - Unknown Team Storage ID. Add to SWITCH statement.") NET_NL()
	SCRIPT_ASSERT("Get_Number_Of_Elements_Of_Team_Storage() - Unknown Team Storage ID. Tell Keith.")
	
	RETURN 0

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Team Storage ID is a valid ID for array access routines
//
// INPUT PARAMS:		paramTeamStorage			The Team Storage ID
// RETURN VALUE:		BOOL						TRUE if the team storage ID is valid for array access, otherwise FALSE
FUNC BOOL Is_TeamDataID_Valid_For_Array_Access(g_eMITeamStorage paramTeamStorage)

	// Some Team Storage IDs are illegal when trying to access a specific team storage array position
	SWITCH (paramTeamStorage)
		// Legal Team Storage IDs for accessing a data array
		CASE MIT_ALL
		CASE MIT_COP
		CASE MIT_CROOKS
			RETURN TRUE
			
		// Illegal Team Storage IDs for accessing data arrays
		CASE MIT_CP_CR
		CASE NO_MIT_DATA
			RETURN FALSE
	ENDSWITCH
	
	PRINTSTRING("...KGM MP [MissionInfo]: Is_TeamDataID_Valid_For_Array_Access() - Unknown Team Storage ID being checked for array access. Add to SWITCH statement") NET_NL()
	SCRIPT_ASSERT("Is_TeamDataID_Valid_For_Array_Access() - Unknown Team Storage ID being checked for array access. Tell Keith.")
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the team storage position pointed to by the mission info array for this team storage ID
//
// INPUT PARAMS:		paramMissionID				The Mission ID
//						paramTeamStorage			The Team Storage ID
// RETURN VALUE:		INT							The array position on the Team Storage For Mission array
//
// NOTES:	Storage Elements is allocated in the following order: COP, CROOKS
FUNC INT Get_MP_Mission_Team_ArrayPos(MP_MISSION paramMissionID, g_eMITeamStorage paramTeamStorage)

	// Check to ensure the Team Storage ID is valid for array access
	IF NOT Is_TeamDataID_Valid_For_Array_Access(paramTeamStorage)
		RETURN UNDEFINED_MISSION_INFO
	ENDIF
	
	// Get the first Mission Team Data array position used by this mission
	INT firstTeamArrayPos = g_sMissionInfo[paramMissionID].arrayPosFirstTeam

	// Get the team storage requirements that has been setup for this mission
	g_eMITeamStorage allocatedStorage = g_sMissionInfo[paramMissionID].teamStorage
	
	SWITCH (allocatedStorage)
		// Storage allocated for all players - this was included for Freemode Activities which are team-independent
		CASE MIT_ALL
			// Only ALL Storage is valid for this mission - this is where all players use the same storage struct
			IF (paramTeamStorage = MIT_ALL)
				// All players use the same storage struct, so it has to be in array position 0
				RETURN (firstTeamArrayPos)
			ENDIF
			
			// Illegal Team Storage ID for allocated storage
			RETURN UNDEFINED_MISSION_INFO
	
		// Storage allocated for the Cop Team only
		CASE MIT_COP
			// Only Cop Team Storage is valid for this Mission
			IF (paramTeamStorage = MIT_COP)
				// There is only Cop Team Storage allocated, so it has to be in array position 0
				RETURN (firstTeamArrayPos)
			ENDIF
			
			// Illegal Team Storage ID for allocated storage
			RETURN UNDEFINED_MISSION_INFO
		
		// Storage allocated for Crook Gangs only (where all Crook Gangs share the same data with no Gang-Specific differences)
		CASE MIT_CROOKS
			// Only Crook Team Storage is valid for this Mission
			IF (paramTeamStorage = MIT_CROOKS)
				// There is only Crook Team Storage allocated, so it has to be in array position 0
				RETURN (firstTeamArrayPos)
			ENDIF
			
			// Illegal Team Storage ID for allocated storage
			RETURN UNDEFINED_MISSION_INFO
			
		// Storage allocated for Cop Team and for Crook Gangs
		CASE MIT_CP_CR
			// Cop Team Storage is valid for this Mission
			IF (paramTeamStorage = MIT_COP)
				// Cop Team Storage is allocated, so it will be in array position 0
				RETURN (firstTeamArrayPos)
			ENDIF
			
			// Crook Team Storage is valid for this Mission
			IF (paramTeamStorage = MIT_CROOKS)
				// Crook Team Storage is allocated, so it will be in array position 1
				RETURN (firstTeamArrayPos + 1)
			ENDIF
			
			// Illegal Team Storage ID for allocated storage
			RETURN UNDEFINED_MISSION_INFO
			
		// No storage allocated
		CASE NO_MIT_DATA
			RETURN UNDEFINED_MISSION_INFO
	ENDSWITCH
	
	// Unknown Storage ID
	PRINTSTRING("...KGM MP [MissionInfo]: Get_MP_Mission_Team_ArrayPos() - Unknown Team Storage ID setup for mission. Add to SWITCH statement.") NET_NL()
	SCRIPT_ASSERT("Get_MP_Mission_Team_ArrayPos() - Unknown Team Storage ID setup for mission. Tell Keith.")
	
	RETURN UNDEFINED_MISSION_INFO

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the specified Mission Team Data Optional Flag is set for the specified team
//
// INPUT PARAMS:		paramFlagID				The optional mission info flag to check for
//						paramMissionID			The Mission ID
//						paramTeamID				The Team ID (ie: the CONSTS that represent the actual team, not the team storage ID used to handle storage array access)
// RETURN VALUE:		BOOL					TRUE if the flag is set, otherwise FALSE
FUNC BOOL Is_This_Mission_Team_Flag_Set(INT paramFlagID, MP_MISSION paramMissionID, INT paramTeamID)

	// Convert the team ID used by the game, to the generic team storage ID required by the mission data storage routines
	g_eMITeamStorage teamStorageID = Convert_Game_Team_ID_To_Storage_ID_For_Mission(paramMissionID, paramTeamID)
	IF (teamStorageID = NO_MIT_DATA)
		RETURN FALSE
	ENDIF
	
	// Get the array position on the Team Data Per Mission array for this team's data
	INT arrayPos = Get_MP_Mission_Team_ArrayPos(paramMissionID, teamStorageID)
	IF (arrayPos = UNDEFINED_MISSION_INFO)
		RETURN FALSE
	ENDIF
	
	// Return the flag value
	RETURN (IS_BIT_SET(g_sTeamInfoPerMission[arrayPos].bitsTeamMissionFlags, paramFlagID))

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Info Array Position Retrieval And Standard Checking Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Used By the functions below to access data within the new mission data structure
//
// INPUT PARAMS:		paramMissionID				The Mission ID
//						paramVariation				The Mission Variation
// RETURN VALUE:		INT							The array position of this mission data on the Mission Variation array
FUNC INT Get_MP_Mission_Info_Variations_ArrayPos(MP_MISSION paramMissionID, INT paramVariation)

	// If 'no variation' is passed in, then the query doesn't care about being variation-specific, so just pass back the details in array position 0
	IF (paramVariation = NO_MISSION_VARIATION)
		paramVariation = 0
	ELIF (paramVariation < 0)
// KGM 16/7/12: Removing the assert and return value for now until all mission data re-structured, then I'll re-activate it because it'll be valid again.
//		PRINTSTRING("...KGM MP [MissionInfo]: trying to get the array position of a mission variation flag but variationID is negative.") PRINTNL()
//		SCRIPT_ASSERT("Get_MP_Mission_Info_Variations_ArrayPos() - Mission Variation is negative. Tell Keith.")
//		
//		RETURN UNDEFINED_MISSION_INFO
		RETURN (g_sMissionInfo[paramMissionID].arrayPosFirstVar)	// TEMP: Return first variation for this mission
	ELIF (paramVariation >= g_sMissionInfo[paramMissionID].numVariations)
// KGM 16/7/12: Removing the assert and return value for now until all mission data re-structured, then I'll re-activate it because it'll be valid again.
//		PRINTSTRING("...KGM MP [MissionInfo]: trying to get the array position of a mission variation flag but variationID exceeds number of setup variations for mission.") PRINTNL()
//		SCRIPT_ASSERT("Get_MP_Mission_Info_Variations_ArrayPos() - Mission Variation is outwith known number of variations for mission. Tell Keith.")
//		
//		RETURN UNDEFINED_MISSION_INFO
		RETURN (g_sMissionInfo[paramMissionID].arrayPosFirstVar)	// TEMP: Return first variation for this mission
	ENDIF
	
	// Retrieve the array position for the mission variation data
	RETURN (g_sMissionInfo[paramMissionID].arrayPosFirstVar + paramVariation)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the specified Mission Info Optional Flag is set in the mission Variation data at the specified array position
//
// INPUT PARAMS:		paramFlagID				The optional mission info flag to check for
//						paramMissionID			The Mission ID
//						paramVariation			The Mission Variation (all missions have at least one variation - the main variation)
// RETURN VALUE:		BOOL					TRUE if the flag is set, otherwise FALSE
FUNC BOOL Is_This_Mission_Variations_Flag_Set(INT paramFlagID, MP_MISSION paramMissionID, INT paramVariation)
	
	INT arrayPos = Get_MP_Mission_Info_Variations_ArrayPos(paramMissionID, paramVariation)
	IF (arrayPos = UNDEFINED_MISSION_INFO)
		RETURN FALSE
	ENDIF
	
	RETURN (IS_BIT_SET(g_sMissionVariations[arrayPos].bitsOptionalFlags, paramFlagID))

ENDFUNC
