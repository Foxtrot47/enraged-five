//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_CLUB_MUSIC_BASE.sch																					//
// Description: Header file containing look ups tables for club music.													//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		30/03/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "script_conversion.sch"
USING "rc_helper_functions.sch"

#IF FEATURE_HEIST_ISLAND
USING "net_club_music_vars.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ ENUMS ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

ENUM CLUB_MUSIC_PROCEDURES
	E_GET_CLUB_DJ,
	E_GET_CLUB_MAX_DJS,
	E_GET_CLUB_RADIO_STATION,
	E_GET_CLUB_RADIO_STATION_NAME,
	E_GET_CLUB_RADIO_STATION_MAX_TRACKS,
	E_GET_CLUB_RADIO_STATION_TRACK_NAME,
	E_GET_CLUB_RADIO_STATION_TRACK_DURATION_MS,
	E_GET_CLUB_RADIO_STATION_TRACK_INTENSITY,
	E_DOES_CLUB_USE_MUSIC_INTENSITY,
	E_GET_CLUB_MAX_STATIC_EMITTERS,
	E_GET_CLUB_STATIC_EMITTER_NAME,
	E_GET_CLUB_AUDIO_SCENE_NAME,
	E_GET_CLUB_ZONE_NAME,
	E_DOES_DJ_DATA_LOAD_FROM_DATAFILE
ENDENUM

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTION POINTERS ╞═══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

TYPEDEF FUNC CLUB_DJS 				_T_GET_CLUB_DJ									(DJ_SERVER_DATA &DJServerData,INT iDJ, BOOL bNextDJ = FALSE, BOOL bUsingPathB = FALSE)
TYPEDEF FUNC INT 					_T_GET_CLUB_MAX_DJS								()
TYPEDEF FUNC CLUB_RADIO_STATIONS 	_T_GET_CLUB_RADIO_STATION						(CLUB_DJS eClubDJ, BOOL bPrivateMode = FALSE)
TYPEDEF FUNC STRING 				_T_GET_CLUB_RADIO_STATION_NAME					(CLUB_RADIO_STATIONS eRadioStation)
TYPEDEF FUNC INT 					_T_GET_CLUB_RADIO_STATION_MAX_TRACKS			(CLUB_RADIO_STATIONS eRadioStation)
TYPEDEF FUNC STRING 				_T_GET_CLUB_RADIO_STATION_TRACK_NAME			(CLUB_RADIO_STATIONS eRadioStation, INT iTrackID)
TYPEDEF FUNC INT 					_T_GET_CLUB_RADIO_STATION_TRACK_DURATION_MS		(CLUB_RADIO_STATIONS eRadioStation, INT iTrackID)
TYPEDEF PROC						_T_GET_CLUB_RADIO_STATION_TRACK_INTENSITY		(CLUB_RADIO_STATIONS eRadioStation, INT iTrackID, INT iTrackIntensityID, INT &iTrackIntensityTimeMS, CLUB_MUSIC_INTENSITY &eClubMusicIntensity)
TYPEDEF FUNC BOOL					_T_DOES_CLUB_USE_MUSIC_INTENSITY				()
TYPEDEF FUNC INT 					_T_GET_CLUB_MAX_STATIC_EMITTERS					()
TYPEDEF FUNC STRING 				_T_GET_CLUB_STATIC_EMITTER_NAME					(INT iEmitterID)
TYPEDEF FUNC STRING 				_T_GET_CLUB_AUDIO_SCENE_NAME					()
TYPEDEF FUNC STRING 				_T_GET_CLUB_ZONE_NAME							()
TYPEDEF FUNC BOOL 					_T_DOES_DJ_DATA_LOAD_FROM_DATAFILE				(CLUB_DJS eClubDJ, TEXT_LABEL_63 &sDataFileName)

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INTERFACE ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

STRUCT CLUB_MUSIC_INTERFACE
	// Radio Station
	_T_GET_CLUB_DJ									returnGetClubDJ
	_T_GET_CLUB_MAX_DJS								returnGetClubMaxDJS
	_T_GET_CLUB_RADIO_STATION						returnGetClubRadioStation
	_T_GET_CLUB_RADIO_STATION_NAME					returnGetClubRadioStationName
	_T_GET_CLUB_RADIO_STATION_MAX_TRACKS			returnGetClubRadioStationMaxTracks
	_T_GET_CLUB_RADIO_STATION_TRACK_NAME			returnGetClubRadioStationTrackName
	_T_GET_CLUB_RADIO_STATION_TRACK_DURATION_MS		returnGetClubRadioStationTrackDurationMS
	_T_GET_CLUB_RADIO_STATION_TRACK_INTENSITY		getClubRadioStationTrackIntensity
	_T_DOES_CLUB_USE_MUSIC_INTENSITY				returnDoesClubUseMusicIntensity
	// Emitters
	_T_GET_CLUB_MAX_STATIC_EMITTERS					returnGetClubMaxStaticEmitters
	_T_GET_CLUB_STATIC_EMITTER_NAME					returnGetClubStaticEmitterName
	
	// Audio Scenes
	_T_GET_CLUB_AUDIO_SCENE_NAME					returnGetClubAudioSceneName
	
	// Audio Zone 
	_T_GET_CLUB_ZONE_NAME							returnGetClubZoneName
	
	// DJ Datafile
	_T_DOES_DJ_DATA_LOAD_FROM_DATAFILE				returnDoesDJDataLoadFromDataFile
ENDSTRUCT
#ENDIF //FEATURE_HEIST_ISLAND
