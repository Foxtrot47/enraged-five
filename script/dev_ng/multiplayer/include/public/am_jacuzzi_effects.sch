//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        am_jacuzzi_effects.sch																			
/// Description: Jacuzzi effects system pulled from the orginal yacht jacuzzi effects. Refactored to be
///    		     re-useable for other jacuzzis.
///																											
/// Written by:  Online Technical Team: Tom Turner,	Conor McGuire (orignial yacht jacuzzi)														
/// Date:  		31/07/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////  

USING "globals.sch"
USING "rc_helper_functions.sch"

/// PURPOSE: 
///    Bitset relating to jacuzzi effects
ENUM JACUZZI_EFFECTS_BIT_SET
	JACUZZI_EFFECTS_INITIALIZED,
	JACUZZI_EFFECTS_BS_EXTERIOR_EFFECTS_STARTED,
	JACUZZI_EFFECTS_BS_WADE_STARTED,
	JACUZZI_EFFECTS_BS_DRIPS_STARTED,
	JACUZZI_EFFECTS_BS_DRIPS_NEEDED,
	JACUZZI_EFFECTS_BS_PLAYER_SUBMERGED,
	JACUZZI_EFFECTS_BS_SPLASH_STARTED,
	JACUZZI_EFFECTS_BS_SPLASH_NEEDED,
	JACUZZI_EFFECTS_BS_SOUND_SPLASH_NEEDED,
	JACUZZI_EFFECTS_BS_WATER_EXIT_SOUND_STARTED,
	JACUZZI_EFFECTS_BS_GOT_SOUND_IDS
ENDENUM

CONST_INT JACUZZI_EFFECTS_DRIP_EFFECT_DURATION 7000
CONST_INT JACUZZI_EFFECTS_BIG_SPLASH_DURATION 920

/// PURPOSE: 
///    DData for running a jacuzzi effects instance
STRUCT JACUZZI_EFFECTS
	INT iBS = 0
	INT iJacuzziSoundID = -1
	
	PTFX_ID ptfxJacuzziSteam
	PTFX_ID ptfxJacuzziWade
	PTFX_ID ptfxJacuzziSplash
	PTFX_ID ptfxJacuzziDripsLHand
	PTFX_ID ptfxJacuzziDripsRHand
	
	TIME_DATATYPE tdJacuzziDripsTimer
	TIME_DATATYPE tdSplashTimer
	
	INT iJacuzziSplashType = -1
	INT iSplashSoundID = -1
	
	INT iWaterExitSoundID = -1
	VECTOR vPlayerPositionLastFrame
	
	VECTOR vJacuzziPos
	FLOAT fJacuzziRadius
	FLOAT fJacuzziWaterLevel
	FLOAT fSFXLoadRadiusOffset = 4.5
ENDSTRUCT

/// PURPOSE:
///    Starts the wade effect for a jacuzzi, only works if not already started
/// PARAMS:
///    sJacuzziEffect - instance data
///    vPos - wade start position
PROC JACUZZI_EFFECTS_START_WADE(JACUZZI_EFFECTS &sJacuzziEffect, VECTOR &vPos)
	IF IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_WADE_STARTED)
		EXIT
	ENDIF
	
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
		EXIT
	ENDIF
	
	USE_PARTICLE_FX_ASSET("scr_apartment_mp")
	sJacuzziEffect.ptfxJacuzziWade = START_PARTICLE_FX_LOOPED_AT_COORD(
	"scr_apa_jacuzzi_wade", vPos, <<0.0, 0.0, 0.0>>, 1.0,
	FALSE, FALSE, FALSE, TRUE)
	
	SET_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_WADE_STARTED)	
	PRINTLN("[JACUZZI_EFFECTS] START_JACUZZI_EFFECT_WADE - started")
ENDPROC

/// PURPOSE:
///    Stops the wade effect for a jacuzzi, stopping the related particle effect.
///    Only works if a wade has been started with JACUZZI_EFFECTS_START_WADE
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_STOP_WADE(JACUZZI_EFFECTS &sJacuzziEffect)
	IF NOT IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_WADE_STARTED)
		EXIT
	ENDIF
	
	STOP_PARTICLE_FX_LOOPED(sJacuzziEffect.ptfxJacuzziWade, TRUE)
	CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_WADE_STARTED)		
	PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_STOP_WADE - stopped")
ENDPROC

/// PURPOSE:
///    Adds a drip particle effect to a bone on the local player
/// PARAMS:
///    eBoneToAttachDripsTo - which bone the drip particle will be attached to
/// RETURNS:
///    The created particles PTFX_ID
FUNC PTFX_ID JACUZZI_EFFECTS_ADD_DRIPS_TO_LOCAL_PLAYERS_BONE(PED_BONETAG eBoneToAttachDripsTo)
	USE_PARTICLE_FX_ASSET("scr_apartment_mp")
	RETURN START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(
	"scr_apa_jacuzzi_drips", PLAYER_PED_ID(), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 
	GET_PED_BONE_INDEX(PLAYER_PED_ID(), eBoneToAttachDripsTo))
ENDFUNC

/// PURPOSE:
///    Adds the drip particle effects to the local players hands
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_ADD_DRIPS_TO_LOCAL_PLAYERS_HANDS(JACUZZI_EFFECTS &sJacuzziEffect)
	sJacuzziEffect.ptfxJacuzziDripsLHand = JACUZZI_EFFECTS_ADD_DRIPS_TO_LOCAL_PLAYERS_BONE(BONETAG_L_HAND)	
	PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_ADD_DRIPS_TO_LOCAL_PLAYERS_HANDS",
	" - started left hand drips at bone ",
	GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_L_HAND))
	
	sJacuzziEffect.ptfxJacuzziDripsRHand = JACUZZI_EFFECTS_ADD_DRIPS_TO_LOCAL_PLAYERS_BONE(BONETAG_R_HAND)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[JACUZZI_EFFECTS] JACUZZI_EFFECTS_ADD_DRIPS_TO_LOCAL_PLAYERS_HANDS",
	" - started right hand drips at bone ",
	GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_R_HAND))
ENDPROC

/// PURPOSE:
///    Starts the jacuzzi water drip effect on the local player, this will only work
///    if not already started
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_START_DRIPS(JACUZZI_EFFECTS &sJacuzziEffect)
	IF IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_STARTED)
		EXIT
	ENDIF
	
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
		EXIT	
	ENDIF
	
	JACUZZI_EFFECTS_ADD_DRIPS_TO_LOCAL_PLAYERS_HANDS(sJacuzziEffect)
	SET_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_STARTED)
	sJacuzziEffect.tdJacuzziDripsTimer = GET_NETWORK_TIME()	
	PRINTLN("[JACUZZI_EFFECTS] START_JACUZZI_EFFECT_DRIPS - started drips")
ENDPROC

/// PURPOSE:
///    Stops the jacuzzi water drip effect on the local player, this will only work
///    if it has been started with JACUZZI_EFFECTS_START_DRIPS
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_STOP_DRIPS(JACUZZI_EFFECTS &sJacuzziEffect)
	IF NOT IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_STARTED)
		EXIT
	ENDIF
	
	STOP_PARTICLE_FX_LOOPED(sJacuzziEffect.ptfxJacuzziDripsLHand, TRUE)
	STOP_PARTICLE_FX_LOOPED(sJacuzziEffect.ptfxJacuzziDripsRHand, TRUE)
	CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_STARTED)
ENDPROC

/// PURPOSE:
///    Maintains getting and releasing sound ids based on if they should be loaded
/// PARAMS:
///    sJacuzziEffect - instance data
///    bLoadSoundIds - if the sound ids should be loaded
PROC JACUZZI_EFFECTS_MAINTAIN_SOUND_IDS(JACUZZI_EFFECTS &sJacuzziEffect, BOOL bLoadSoundIds)
	// Request sound IDs as soon as player is near the jacuzzi
	IF bLoadSoundIds
		IF sJacuzziEffect.iSplashSoundID = -1
			sJacuzziEffect.iSplashSoundID = GET_SOUND_ID()	
			PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - got sound ID iSplashSoundID")
		ENDIF
		
		IF sJacuzziEffect.iWaterExitSoundID = -1
			sJacuzziEffect.iWaterExitSoundID = GET_SOUND_ID()
			PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - got sound ID iWaterExitSoundID")
		ENDIF
		EXIT
	ENDIF
	
	// Else release ids if out of range
	IF sJacuzziEffect.iSplashSoundID != -1
		RELEASE_SOUND_ID(sJacuzziEffect.iSplashSoundID)
		sJacuzziEffect.iSplashSoundID = -1
		PRINTLN(DEBUG_SAFEHOUSE, "[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - released iSplashSoundID")
	ENDIF
	
	IF sJacuzziEffect.iWaterExitSoundID != -1
		RELEASE_SOUND_ID(sJacuzziEffect.iWaterExitSoundID)
		sJacuzziEffect.iWaterExitSoundID = -1
		PRINTLN(DEBUG_SAFEHOUSE, "[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - released iWaterExitSoundID")
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the local player is submerged in the jacuzzi according to the water level
/// PARAMS:
///    fJacuzziWaterLevel - the water levels height in world space
/// RETURNS:
///    TRUE if submerged in the jacuzzis water
FUNC BOOL JACUZZI_EFFECTS_IS_LOCAL_PLAYER_SUBMERGED(FLOAT fJacuzziWaterLevel)
	INT i
	VECTOR vBonePosition
	PED_BONETAG iBoneTags[6]
	iBoneTags[0] = BONETAG_L_FOOT
	iBoneTags[1] = BONETAG_R_FOOT
	iBoneTags[2] = BONETAG_L_FOREARM
	iBoneTags[3] = BONETAG_R_FOREARM
	iBoneTags[4] = BONETAG_ROOT
	iBoneTags[5] = BONETAG_HEAD
	
	/// Check some critical bones, if any of them is in the water then at least 
	/// some part of the player is under water for sure
	REPEAT 6 i
		vBonePosition = GET_WORLD_POSITION_OF_ENTITY_BONE(PLAYER_PED_ID(),
		GET_PED_BONE_INDEX(PLAYER_PED_ID(), iBoneTags[i]))
		
		IF vBonePosition.Z <= fJacuzziWaterLevel
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintains the water wading effect for the jacuzzi
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_MAINTAIN_WADING(JACUZZI_EFFECTS &sJacuzziEffect)
	FLOAT fWadeScale
	FLOAT fSubmersionFactor // 0.0 - not submerged, 1.0 - fully submerged
	FLOAT fPlayerSubmersion
	FLOAT fPlayerSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	IF fPlayerSpeed > 0.0
		VECTOR vPlayerFootCoords
		vPlayerFootCoords = GET_WORLD_POSITION_OF_ENTITY_BONE(PLAYER_PED_ID(),
		GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_L_FOOT))
		
		FLOAT fMaxSubmersion = sJacuzziEffect.fJacuzziWaterLevel - sJacuzziEffect.vJacuzziPos.Z
		fPlayerSubmersion = FMAX(0.0, ABSF(sJacuzziEffect.fJacuzziWaterLevel - (vPlayerFootCoords.Z)))// by how much the player is submerged in jacuzzi water
		fSubmersionFactor = fPlayerSubmersion / fMaxSubmersion
		fWadeScale = FMIN(1.0, (fSubmersionFactor + (fPlayerSpeed * 0.5)) * 0.5)
	ENDIF
	
	IF fPlayerSpeed > 0.1 AND fWadeScale > 0.0
		VECTOR vWadePosition = <<vPlayerCoords.X, vPlayerCoords.Y, sJacuzziEffect.fJacuzziWaterLevel>>
		JACUZZI_EFFECTS_START_WADE(sJacuzziEffect, vWadePosition)	
		SET_PARTICLE_FX_LOOPED_OFFSETS(sJacuzziEffect.ptfxJacuzziWade, vWadePosition, <<0.0, 0.0, 0.0>>)
		SET_PARTICLE_FX_LOOPED_EVOLUTION(sJacuzziEffect.ptfxJacuzziWade, "size", fWadeScale, TRUE)
	ELSE
		JACUZZI_EFFECTS_STOP_WADE(sJacuzziEffect)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the current splash type that needs to be used next according to the players
///    velocity
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_UPDATE_REQUIRED_SPLASH_TYPE(JACUZZI_EFFECTS &sJacuzziEffect)
	// Player just touched the water surface
	// See if their speed is large enough to trigger splashes
	VECTOR vPlayerVelocity = GET_ENTITY_VELOCITY(PLAYER_PED_ID())
	FLOAT fDownwardSpeed = ABSF(vPlayerVelocity.Z)
	PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - WATER CONTACT! fDownwardSpeed: ", fDownwardSpeed)
	
	IF fDownwardSpeed > 5.5
		sJacuzziEffect.iJacuzziSplashType = 3
	ELIF fDownwardSpeed > 4.0
		sJacuzziEffect.iJacuzziSplashType = 2
	ELIF fDownwardSpeed > 2.0
		sJacuzziEffect.iJacuzziSplashType = 1
	ELIF fDownwardSpeed > 0.5
		sJacuzziEffect.iJacuzziSplashType = 0
	ELSE
		sJacuzziEffect.iJacuzziSplashType = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the splashing effects for a jacuzzi
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_MAINTAIN_SPLASHING(JACUZZI_EFFECTS &sJacuzziEffect)
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	// Only allow other splash to happen if the previous one already stopped (both particles and sound)
	IF NOT IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SPLASH_STARTED)
	AND HAS_SOUND_FINISHED(sJacuzziEffect.iSplashSoundID)
		
		/// Detect any sudden changes in position which mean that we might need to do another splash 
		/// (player jumping or being pushed etc.)
		FLOAT fZChange = vPlayerCoords.Z - sJacuzziEffect.vPlayerPositionLastFrame.Z
		
		IF fZChange < -0.04
			sJacuzziEffect.iJacuzziSplashType = 0
			IF fZChange < -0.06
				sJacuzziEffect.iJacuzziSplashType = 1
			ENDIF
				
			SET_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SOUND_SPLASH_NEEDED)
			PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - Player jump around, need splash")
		ENDIF
	ENDIF
	
	sJacuzziEffect.vPlayerPositionLastFrame = vPlayerCoords
	JACUZZI_EFFECTS_UPDATE_REQUIRED_SPLASH_TYPE(sJacuzziEffect)
ENDPROC

/// PURPOSE:
///    Maintains the climbing out of jacuzzi effects
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_MAINTAIN_CLIMBING_OUT_EFFECTS(JACUZZI_EFFECTS &sJacuzziEffect)
	// Monitor player leaving the water through climbing out so we can play water exit sound
	IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	IF IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)
		EXIT	
	ENDIF
	
	STOP_SOUND(sJacuzziEffect.iWaterExitSoundID)
	PLAY_SOUND_FROM_ENTITY(sJacuzziEffect.iWaterExitSoundID, "ExitWater", PLAYER_PED_ID(), "GTAO_Hot_Tub_PED_INSIDE_WATER", TRUE)
	SET_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)
	PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - Playing water exit sound")
ENDPROC

/// PURPOSE:
///    Maintains all the effects used when the player is submerged in the jacuzzi water
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_MAINTAIN_SUBMERGED_EFFECTS(JACUZZI_EFFECTS &sJacuzziEffect)
	IF NOT IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_PLAYER_SUBMERGED)
		JACUZZI_EFFECTS_UPDATE_REQUIRED_SPLASH_TYPE(sJacuzziEffect)
		SET_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_PLAYER_SUBMERGED)
		SET_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SOUND_SPLASH_NEEDED)
		EXIT
	ENDIF
	
	JACUZZI_EFFECTS_MAINTAIN_SPLASHING(sJacuzziEffect)
	JACUZZI_EFFECTS_MAINTAIN_CLIMBING_OUT_EFFECTS(sJacuzziEffect)
ENDPROC

/// PURPOSE:
///    Plays the sounde effect for any requested splashes according to the current splash type
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_PLAY_ANY_REQUIRED_SPLASH_SOUNDS(JACUZZI_EFFECTS &sJacuzziEffect)
	// Do we need to play a splash sound?
	IF NOT IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SOUND_SPLASH_NEEDED)
		EXIT
	ENDIF
	
	SET_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SPLASH_NEEDED) // along with the sound go particles	
	STOP_SOUND(sJacuzziEffect.iSplashSoundID)
	
	STRING sndName = ""
	SWITCH sJacuzziEffect.iJacuzziSplashType
		CASE 0 sndName = "FallingInWaterSmall" BREAK
		CASE 1 sndName = "FallingInWaterMedium" BREAK
		CASE 2 sndName = "FallingInWaterHeavy" BREAK
		CASE 3 sndName = "DiveInWater" BREAK
	ENDSWITCH

	PLAY_SOUND_FROM_ENTITY(sJacuzziEffect.iSplashSoundID, sndName, PLAYER_PED_ID(), "PED_INSIDE_WATER", TRUE)
	PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - Playing splash sound: ", sndName)	
	CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SOUND_SPLASH_NEEDED)
ENDPROC

/// PURPOSE:
///    Checks if a splash effect has been requested
/// PARAMS:
///    sJacuzziEffect - instance data
/// RETURNS:
///    TRUE if a splash effect is needed
FUNC BOOL JACUZZI_EFFECTS_IS_A_SPLASH_EFFECT_REQUIRED(JACUZZI_EFFECTS &sJacuzziEffect)
	RETURN IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SPLASH_NEEDED)
	AND NOT IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SPLASH_STARTED)
	AND sJacuzziEffect.iJacuzziSplashType > 0
ENDFUNC

/// PURPOSE:
///    Creates the splash particle effect for any requested 
///    splashs according to the current splash type
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_CREATE_ANY_REQUIRE_SPLASH_PARTICLE_EFFECTS(JACUZZI_EFFECTS &sJacuzziEffect)
	// Do we need to show splash effect?
	IF NOT JACUZZI_EFFECTS_IS_A_SPLASH_EFFECT_REQUIRED(sJacuzziEffect)
		EXIT
	ENDIF
	
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
		EXIT
	ENDIF
	
	VECTOR vSplashPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	vSplashPosition.Z = sJacuzziEffect.fJacuzziWaterLevel + 0.1
	
	FLOAT fScale = 1.0
	SWITCH sJacuzziEffect.iJacuzziSplashType
		CASE 1 fScale = 1.25 BREAK
		CASE 2 fScale = 1.66 BREAK
		CASE 3 fScale = 2.0 BREAK
	ENDSWITCH

	USE_PARTICLE_FX_ASSET("scr_apartment_mp")
	sJacuzziEffect.ptfxJacuzziSplash = START_PARTICLE_FX_LOOPED_AT_COORD("scr_apa_jacuzzi_wade",
	vSplashPosition, <<0.0, 0.0, 0.0>>, fScale, FALSE, FALSE, FALSE, TRUE)
	SET_PARTICLE_FX_LOOPED_EVOLUTION(sJacuzziEffect.ptfxJacuzziSplash, "size", 1.0, TRUE)
	PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - splash particles started")
	
	sJacuzziEffect.tdSplashTimer = GET_NETWORK_TIME()
	SET_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SPLASH_STARTED)
	CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SPLASH_NEEDED)
ENDPROC

/// PURPOSE:
///    Maintains creating any requested splash effects
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_MAINTAIN_SPLASH_EFFECT_REQUESTS(JACUZZI_EFFECTS &sJacuzziEffect)
	JACUZZI_EFFECTS_PLAY_ANY_REQUIRED_SPLASH_SOUNDS(sJacuzziEffect)
	JACUZZI_EFFECTS_CREATE_ANY_REQUIRE_SPLASH_PARTICLE_EFFECTS(sJacuzziEffect)
ENDPROC

/// PURPOSE:
///    Maintains startind the water dripping effect on the player, if the drip
///    effect has been requested
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_MAINTAIN_DRIPS_STARTING(JACUZZI_EFFECTS &sJacuzziEffect)
	IF NOT IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_NEEDED)
		EXIT
	ENDIF
	
	IF IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_STARTED)
		sJacuzziEffect.tdJacuzziDripsTimer = GET_NETWORK_TIME()
	ENDIF
	
	JACUZZI_EFFECTS_START_DRIPS(sJacuzziEffect)
		
	IF IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_STARTED)
		CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_NEEDED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the drip effect, stopping it after the drip effect duration has finished
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_MAINTAIN_DRIPS_EFFECT(JACUZZI_EFFECTS &sJacuzziEffect)
	IF NOT IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_STARTED)
		EXIT
	ENDIF
	
	IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sJacuzziEffect.tdJacuzziDripsTimer)) > JACUZZI_EFFECTS_DRIP_EFFECT_DURATION
		JACUZZI_EFFECTS_STOP_DRIPS(sJacuzziEffect)
		PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - stopped drips")
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the big splash effect, stopping the looped particle after the big splash duration
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_MAINTAIN_BIG_SPLASH_EFFECT(JACUZZI_EFFECTS &sJacuzziEffect)
	IF NOT IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SPLASH_STARTED)
		EXIT
	ENDIF
	
	IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sJacuzziEffect.tdSplashTimer)) > JACUZZI_EFFECTS_BIG_SPLASH_DURATION
		STOP_PARTICLE_FX_LOOPED(sJacuzziEffect.ptfxJacuzziSplash)
		CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SPLASH_STARTED)
		PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - stopped splash particles")
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the exiting the jacuzzi sound effect making sure it can only
///    be rquested if the previous has finished
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_MAINTAIN_EXIT_SOUND_EFFECT(JACUZZI_EFFECTS &sJacuzziEffect)
	IF NOT IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)
		EXIT
	ENDIF
	
	IF HAS_SOUND_FINISHED(sJacuzziEffect.iWaterExitSoundID)
		CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates all the effects for the jacuzzi
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_UPDATE(JACUZZI_EFFECTS &sJacuzziEffect)	
	IF NOT IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_INITIALIZED)
		SCRIPT_ASSERT("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - Not initialized! you must call JACUZZI_EFFECTS_INIT before updating")
		PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_UPDATE - Not initialized - not updating")
		EXIT
	ENDIF
	
	BOOL bInLoadRadius 
	bInLoadRadius = IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), sJacuzziEffect.vJacuzziPos,
	sJacuzziEffect.fJacuzziRadius + sJacuzziEffect.fSFXLoadRadiusOffset)
	
	BOOL bInJacuzziRadius 
	bInJacuzziRadius = IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),
	sJacuzziEffect.vJacuzziPos, sJacuzziEffect.fJacuzziRadius)
	
	JACUZZI_EFFECTS_MAINTAIN_SOUND_IDS(sJacuzziEffect, bInLoadRadius)

	IF bInJacuzziRadius
		JACUZZI_EFFECTS_MAINTAIN_WADING(sJacuzziEffect)	
		SET_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_NEEDED)

		IF JACUZZI_EFFECTS_IS_LOCAL_PLAYER_SUBMERGED(sJacuzziEffect.fJacuzziWaterLevel)
			JACUZZI_EFFECTS_MAINTAIN_SUBMERGED_EFFECTS(sJacuzziEffect)
		ENDIF

		JACUZZI_EFFECTS_MAINTAIN_SPLASH_EFFECT_REQUESTS(sJacuzziEffect)
	ELSE
		JACUZZI_EFFECTS_STOP_WADE(sJacuzziEffect)
		JACUZZI_EFFECTS_MAINTAIN_DRIPS_STARTING(sJacuzziEffect)		
		CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_PLAYER_SUBMERGED)
	ENDIF
	
	JACUZZI_EFFECTS_MAINTAIN_DRIPS_EFFECT(sJacuzziEffect)
	JACUZZI_EFFECTS_MAINTAIN_BIG_SPLASH_EFFECT(sJacuzziEffect)
	JACUZZI_EFFECTS_MAINTAIN_EXIT_SOUND_EFFECT(sJacuzziEffect)
ENDPROC

/// PURPOSE:
///    Initializes the jacuzzi effects ensuring instance specific data is set
///    and the required assets are requested
/// PARAMS:
///    sJacuzziEffect - instance data
///    vJacuzziPos - centre position of the jacuzzi
///    fJacuzziRadius - radius of the jacuzzi
///    fJacuzziWaterLevelOffset - water level height as offset from centre position
///    fSFXLoadRadiusOffset - additional radius distance when assets are loaded
PROC JACUZZI_EFFECTS_INIT(JACUZZI_EFFECTS &sJacuzziEffect, VECTOR vJacuzziPos,
FLOAT fJacuzziRadius, FLOAT fJacuzziWaterLevelOffset, FLOAT fSFXLoadRadiusOffset = 4.5)
	sJacuzziEffect.vJacuzziPos = vJacuzziPos
	sJacuzziEffect.fJacuzziRadius = fJacuzziRadius
	sJacuzziEffect.fJacuzziWaterLevel = fJacuzziWaterLevelOffset + vJacuzziPos.z
	sJacuzziEffect.fSFXLoadRadiusOffset = fSFXLoadRadiusOffset
	REQUEST_NAMED_PTFX_ASSET("scr_apartment_mp")
	SET_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_INITIALIZED)
	PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_INIT - Initialized")
ENDPROC

/// PURPOSE:
///    Cleans up the jacuzzi effects instance
/// PARAMS:
///    sJacuzziEffect - instance data
PROC JACUZZI_EFFECTS_CLEANUP(JACUZZI_EFFECTS &sJacuzziEffect)
	IF IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_WADE_STARTED)
		REMOVE_PARTICLE_FX(sJacuzziEffect.ptfxJacuzziWade)
		CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_WADE_STARTED)
		PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_CLEANUP - released PTFX ptfxJacuzziWade")
	ENDIF
	
	IF IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_STARTED)
		REMOVE_PARTICLE_FX(sJacuzziEffect.ptfxJacuzziDripsLHand, TRUE)
		REMOVE_PARTICLE_FX(sJacuzziEffect.ptfxJacuzziDripsRHand, TRUE)
		CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_DRIPS_STARTED)
		PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_CLEANUP - released PTFX ptfxJacuzziDripsLHand, ptfxJacuzziDripsRHand")
	ENDIF
	
	IF IS_BIT_SET_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SPLASH_STARTED)
		REMOVE_PARTICLE_FX(sJacuzziEffect.ptfxJacuzziSplash)
		CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_BS_SPLASH_STARTED)
		PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_CLEANUP - released PTFX ptfxJacuzziSplash")
	ENDIF
	
	REMOVE_NAMED_PTFX_ASSET("scr_apartment_mp")	
	
	// Release sound IDs if they haven't been released when player left jacuzzi sphere
	// (this happens when script terminates when player is in jacuzzi)
	IF sJacuzziEffect.iSplashSoundID != -1
		RELEASE_SOUND_ID(sJacuzziEffect.iSplashSoundID)
		sJacuzziEffect.iSplashSoundID = -1
		PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_CLEANUP - released iSplashSoundID")
	ENDIF
	
	IF sJacuzziEffect.iWaterExitSoundID != -1
		RELEASE_SOUND_ID(sJacuzziEffect.iWaterExitSoundID)
		sJacuzziEffect.iWaterExitSoundID = -1
		PRINTLN("[JACUZZI_EFFECTS] JACUZZI_EFFECTS_CLEANUP - released iWaterExitSoundID")
	ENDIF
	
	CLEAR_BIT_ENUM(sJacuzziEffect.iBS, JACUZZI_EFFECTS_INITIALIZED)
ENDPROC
