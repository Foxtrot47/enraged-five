//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_INTERIOR_WEAPON_PICKUPS.sch																			//
// Description: Header file containing functionality for weapon pickups within simple interiors.						//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		21/04/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_TUNER
USING "net_include.sch"
USING "reward_unlocks.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ STRUCTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
STRUCT INTERIOR_WEAPON_PICKUP_DEBUG_DATA
	BOOL bEnableDebug			= FALSE
	BOOL bResetWeaponPickups 	= FALSE
	VECTOR vPosition[MAX_NUM_INTERIOR_WEAPON_PICKUPS]
	VECTOR vRotation[MAX_NUM_INTERIOR_WEAPON_PICKUPS]
	VECTOR vCachedPosition[MAX_NUM_INTERIOR_WEAPON_PICKUPS]
	VECTOR vCachedRotation[MAX_NUM_INTERIOR_WEAPON_PICKUPS]
ENDSTRUCT
#ENDIF  // IS_DEBUG_BUILD

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ FUCNTIONS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CLEANUP_INTERIOR_WEAPON_PICKUPS(INTERIOR_WEAPON_PICKUP_DATA &WeaponPickupData[MAX_NUM_INTERIOR_WEAPON_PICKUPS])
	
	INT iPickup
	REPEAT MAX_NUM_INTERIOR_WEAPON_PICKUPS iPickup
		IF DOES_PICKUP_EXIST(WeaponPickupData[iPickup].PickupWeapon)
			REMOVE_PICKUP(WeaponPickupData[iPickup].PickupWeapon)
			WeaponPickupData[iPickup].PickupWeapon = NULL
			
			IF (WeaponPickupData[iPickup].PlacementData.eCustomPickupModel != DUMMY_MODEL_FOR_SCRIPT)
				SET_MODEL_AS_NO_LONGER_NEEDED(WeaponPickupData[iPickup].PlacementData.eCustomPickupModel)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC CLEAR_INTERIOR_WEAPON_PICKUP_DATA(INTERIOR_WEAPON_PICKUP_DATA &WeaponPickupData[MAX_NUM_INTERIOR_WEAPON_PICKUPS])
	
	INT iPickup
	REPEAT MAX_NUM_INTERIOR_WEAPON_PICKUPS iPickup
		WeaponPickupData[iPickup].PlacementData.vPosition 		= <<0.0, 0.0, 0.0>>
		WeaponPickupData[iPickup].PlacementData.vRotation 		= <<0.0, 0.0, 0.0>>
		WeaponPickupData[iPickup].PlacementData.ePickupType 	= PICKUP_TYPE_INVALID
		WeaponPickupData[iPickup].PlacementData.eWeaponType 	= WEAPONTYPE_INVALID
		WeaponPickupData[iPickup].PlacementData.iAmmoCount		= 0
		WeaponPickupData[iPickup].PlacementData.iCatalogueKey 	= -1
		CLEAR_BIT(WeaponPickupData[iPickup].iPickupFlags, 0)
		CLEAR_BIT(WeaponPickupData[iPickup].iPickupFlags, 1)
	ENDREPEAT
	
ENDPROC

PROC SET_INTERIOR_WEAPON_PICKUP_DATA(INTERIOR_WEAPON_PICKUP_DATA &WeaponPickupData, PICKUP_TYPE ePickupType, WEAPON_TYPE eWeaponType, VECTOR vPosition, VECTOR vRotation, INT iAmmoCount, INT iCatalogueKey, MODEL_NAMES eCustomPickupModel = DUMMY_MODEL_FOR_SCRIPT)
	WeaponPickupData.PlacementData.vPosition 			= vPosition
	WeaponPickupData.PlacementData.vRotation 			= vRotation
	WeaponPickupData.PlacementData.ePickupType 			= ePickupType
	WeaponPickupData.PlacementData.eWeaponType 			= eWeaponType
	WeaponPickupData.PlacementData.iAmmoCount			= iAmmoCount
	WeaponPickupData.PlacementData.iCatalogueKey 		= iCatalogueKey
	WeaponPickupData.PlacementData.eCustomPickupModel	= eCustomPickupModel
ENDPROC

FUNC BOOL IS_INTERIOR_WEAPON_PICKUP_VALID(INTERIOR_WEAPON_PICKUP_PLACEMENT_DATA &PlacementData)
	RETURN (PlacementData.ePickupType != PICKUP_TYPE_INVALID)
ENDFUNC

PROC SET_INTERIOR_WEAPON_PICKUP_AS_PURCHASED(INTERIOR_WEAPON_PICKUP_DATA &WeaponPickupData)
	
	IF NOT bSetInteriorWeaponPickupsOwned
	OR (WeaponPickupData.PlacementData.eWeaponType = WEAPONTYPE_INVALID)
		EXIT
	ENDIF
	
	IF NOT IS_MP_WEAPON_PURCHASED(WeaponPickupData.PlacementData.eWeaponType)
		IF USE_SERVER_TRANSACTIONS()
			IF (WeaponPickupData.PlacementData.iCatalogueKey != -1)
				IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1
					IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_WEAPON, WeaponPickupData.PlacementData.iCatalogueKey, NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, DEFAULT, CTPF_AUTO_RETRY|CTPF_FAIL_ALERT|CTPF_AUTO_PROCESS_REPLY)
					AND NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
						SET_MP_WEAPON_EQUIPPED(WeaponPickupData.PlacementData.eWeaponType, TRUE)
					ELSE
						PRINTLN("SET_INTERIOR_WEAPON_PICKUP_AS_PURCHASED - Unable to add/checkout basket tranaction for itemID: ", WeaponPickupData.PlacementData.iCatalogueKey)
						DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					ENDIF
				ELSE
					PRINTLN("SET_INTERIOR_WEAPON_PICKUP_AS_PURCHASED - Unable to start tranaction for itemID: ", WeaponPickupData.PlacementData.iCatalogueKey)
				ENDIF
			ENDIF
		ELSE
			SET_MP_WEAPON_PURCHASED(WeaponPickupData.PlacementData.eWeaponType, TRUE)
			SET_MP_WEAPON_EQUIPPED(WeaponPickupData.PlacementData.eWeaponType, TRUE)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_INTERIOR_WEAPON_PICKUPS(INTERIOR_WEAPON_PICKUP_DATA &WeaponPickupData[MAX_NUM_INTERIOR_WEAPON_PICKUPS])
	
	INT iPickup
	REPEAT MAX_NUM_INTERIOR_WEAPON_PICKUPS iPickup
		IF IS_INTERIOR_WEAPON_PICKUP_VALID(WeaponPickupData[iPickup].PlacementData)
			
			BOOL bProcessPickup = TRUE
			
			IF (WeaponPickupData[iPickup].PlacementData.eCustomPickupModel != DUMMY_MODEL_FOR_SCRIPT)
				REQUEST_MODEL(WeaponPickupData[iPickup].PlacementData.eCustomPickupModel)
				IF NOT HAS_MODEL_LOADED(WeaponPickupData[iPickup].PlacementData.eCustomPickupModel)
					bProcessPickup = FALSE
				ENDIF
			ENDIF
			
			IF (bProcessPickup)
				IF DOES_PICKUP_EXIST(WeaponPickupData[iPickup].PickupWeapon)
					IF HAS_PICKUP_BEEN_COLLECTED(WeaponPickupData[iPickup].PickupWeapon)
						REMOVE_PICKUP(WeaponPickupData[iPickup].PickupWeapon)
						WeaponPickupData[iPickup].iLastWeaponCreateTime 	= GET_CLOUD_TIME_AS_INT()
						WeaponPickupData[iPickup].PickupWeapon 				= NULL
						WeaponPickupData[iPickup].bLastWeaponPickedUp 		= TRUE
						SET_INTERIOR_WEAPON_PICKUP_AS_PURCHASED(WeaponPickupData[iPickup])
					ELSE
						IF NOT IS_BIT_SET(WeaponPickupData[iPickup].iPickupFlags, 1)	// Safe to collect?
							// Remove so we can re-create
							REMOVE_PICKUP(WeaponPickupData[iPickup].PickupWeapon)
							WeaponPickupData[iPickup].iLastWeaponCreateTime = 0
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(WeaponPickupData[iPickup].iPickupFlags, 0) 			// First weapon processed
					SET_BIT(WeaponPickupData[iPickup].iPickupFlags, 0)					
					IF NOT WeaponPickupData[iPickup].bLastWeaponPickedUp
						WeaponPickupData[iPickup].iLastWeaponCreateTime = 0
					ENDIF
				ENDIF
				
				IF (GET_CLOUD_TIME_AS_INT() - WeaponPickupData[iPickup].iLastWeaponCreateTime) > MAX_NUM_INTERIOR_WEAPON_PICKUPS_COOLDOWN_TIME_SECS
					IF DOES_PICKUP_EXIST(WeaponPickupData[iPickup].PickupWeapon)
						WeaponPickupData[iPickup].iLastWeaponCreateTime = GET_CLOUD_TIME_AS_INT()-(MAX_NUM_INTERIOR_WEAPON_PICKUPS_COOLDOWN_TIME_SECS/2)
					ELSE
						WeaponPickupData[iPickup].iLastWeaponCreateTime = GET_CLOUD_TIME_AS_INT()
						
						INT iPlacementFlags = 0
						SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
				  		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
						
						WeaponPickupData[iPickup].PickupWeapon = CREATE_PICKUP_ROTATE(WeaponPickupData[iPickup].PlacementData.ePickupType, WeaponPickupData[iPickup].PlacementData.vPosition, 
																					  WeaponPickupData[iPickup].PlacementData.vRotation, iPlacementFlags, WeaponPickupData[iPickup].PlacementData.iAmmoCount, 
																					  DEFAULT, FALSE, WeaponPickupData[iPickup].PlacementData.eCustomPickupModel)
						
						WeaponPickupData[iPickup].bLastWeaponPickedUp = FALSE
						SET_BIT(WeaponPickupData[iPickup].iPickupFlags, 1) 				// Safe to collect?
						
						PRINTLN("MAINTAIN_INTERIOR_WEAPON_PICKUPS - Creating Pickup: ", iPickup)
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ DEBUG ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC INITIALISE_INTERIOR_WEAPON_PICKUP_DEBUG_WIDGETS(INTERIOR_WEAPON_PICKUP_DEBUG_DATA &DebugData, INTERIOR_WEAPON_PICKUP_DATA &WeaponPickupData[MAX_NUM_INTERIOR_WEAPON_PICKUPS])
	
	INT iPickup
	REPEAT MAX_NUM_INTERIOR_WEAPON_PICKUPS iPickup
		IF IS_INTERIOR_WEAPON_PICKUP_VALID(WeaponPickupData[iPickup].PlacementData)
			DebugData.vPosition[iPickup] 		= WeaponPickupData[iPickup].PlacementData.vPosition
			DebugData.vRotation[iPickup] 		= WeaponPickupData[iPickup].PlacementData.vRotation
			DebugData.vCachedPosition[iPickup] 	= WeaponPickupData[iPickup].PlacementData.vPosition
			DebugData.vCachedRotation[iPickup] 	= WeaponPickupData[iPickup].PlacementData.vRotation
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC CREATE_INTERIOR_WEAPON_PICKUP_DEBUG_WIDGETS(INTERIOR_WEAPON_PICKUP_DEBUG_DATA &DebugData, INTERIOR_WEAPON_PICKUP_DATA &WeaponPickupData[MAX_NUM_INTERIOR_WEAPON_PICKUPS])
	INITIALISE_INTERIOR_WEAPON_PICKUP_DEBUG_WIDGETS(DebugData, WeaponPickupData)
	
	START_WIDGET_GROUP("Weapon Pickups")
		ADD_WIDGET_BOOL("Enable Debug", DebugData.bEnableDebug)
		ADD_WIDGET_BOOL("Reset Weapon Pickups", DebugData.bResetWeaponPickups)
		
		INT iPickup
		TEXT_LABEL_63 tlWidgetGroupName = ""
		
		REPEAT MAX_NUM_INTERIOR_WEAPON_PICKUPS iPickup
			IF IS_INTERIOR_WEAPON_PICKUP_VALID(WeaponPickupData[iPickup].PlacementData)
				tlWidgetGroupName = "Pickup: "
				tlWidgetGroupName += iPickup
				START_WIDGET_GROUP(tlWidgetGroupName)
					ADD_WIDGET_VECTOR_SLIDER("Position", DebugData.vPosition[iPickup], -99999.9, 99999.9, 0.01)
					ADD_WIDGET_VECTOR_SLIDER("Rotation", DebugData.vRotation[iPickup], -360.0, 360.0, 0.1)
				STOP_WIDGET_GROUP()
				tlWidgetGroupName = ""
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_INTERIOR_WEAPON_PICKUP_DEBUG_WIDGETS(INTERIOR_WEAPON_PICKUP_DEBUG_DATA &DebugData, INTERIOR_WEAPON_PICKUP_DATA &WeaponPickupData[MAX_NUM_INTERIOR_WEAPON_PICKUPS])
	
	IF NOT DebugData.bEnableDebug
		EXIT
	ENDIF
	
	IF (DebugData.bResetWeaponPickups)
		DebugData.bResetWeaponPickups = FALSE
		
		INT iPickup
		REPEAT MAX_NUM_INTERIOR_WEAPON_PICKUPS iPickup
			IF IS_INTERIOR_WEAPON_PICKUP_VALID(WeaponPickupData[iPickup].PlacementData)
				IF DOES_PICKUP_EXIST(WeaponPickupData[iPickup].PickupWeapon)
					REMOVE_PICKUP(WeaponPickupData[iPickup].PickupWeapon)
				ENDIF
				WeaponPickupData[iPickup].iLastWeaponCreateTime		= 0
				WeaponPickupData[iPickup].bLastWeaponPickedUp		= FALSE
				WeaponPickupData[iPickup].iPickupFlags				= 0
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
	ENDIF
	
	INT iPickups
	REPEAT MAX_NUM_INTERIOR_WEAPON_PICKUPS iPickups
		IF IS_INTERIOR_WEAPON_PICKUP_VALID(WeaponPickupData[iPickups].PlacementData)
			IF DOES_PICKUP_EXIST(WeaponPickupData[iPickups].PickupWeapon)
				IF NOT ARE_VECTORS_EQUAL(DebugData.vPosition[iPickups], DebugData.vCachedPosition[iPickups])
				OR NOT ARE_VECTORS_EQUAL(DebugData.vRotation[iPickups], DebugData.vCachedRotation[iPickups])
					DebugData.vCachedPosition[iPickups] = DebugData.vPosition[iPickups]
					DebugData.vCachedRotation[iPickups] = DebugData.vRotation[iPickups]
					
					REMOVE_PICKUP(WeaponPickupData[iPickups].PickupWeapon)
					
					INT iPlacementFlags = 0
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
			  		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
					
					WeaponPickupData[iPickups].PickupWeapon = CREATE_PICKUP_ROTATE(WeaponPickupData[iPickups].PlacementData.ePickupType, DebugData.vPosition[iPickups], 
																				 DebugData.vRotation[iPickups], iPlacementFlags, WeaponPickupData[iPickups].PlacementData.iAmmoCount, 
																				 DEFAULT, FALSE, WeaponPickupData[iPickups].PlacementData.eCustomPickupModel)
				ENDIF
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF // IS_DEBUG_BUILD
#ENDIF // FEATURE_TUNER
