USING "rage_builtins.sch"
USING "globals.sch"

USING "title_update_globals.sch"

USING "net_prints.sch"



// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_missions_shared_public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Access to public functions without creating cyclic headers.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      GROUP ACCESS FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Retrieve the Group Coords for this group slot
//
// INPUT PARAMS:			paramGroupArrayPos		Pending Activities Group Array pos
// RETURN VALUE:			VECTOR					The Group array position associated with this slot, or << 0.0, 0.0, 0.0 >>
FUNC VECTOR Get_Group_Coords_For_Group_Slot(INT paramGroupArrayPos)
	RETURN (GlobalServerBD_MissionsShared.groups[paramGroupArrayPos].msgGroupLocation)
ENDFUNC




// ===========================================================================================================
//      PENDING ACTIVITIES ARRAY ACCESS FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Check if the Pending Activities slot is in use
//
// INPUT PARAMS:			paramSlot					Missions Shared slot
// RETURN VALUE:			BOOL						TRUE if the slot is in use, otherwise FALSE
FUNC BOOL Is_Pending_Activities_Slot_In_Use(INT paramSlot)
	RETURN (GlobalServerBD_MissionsShared.activities[paramSlot].paInUse)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the GroupSlot for a specific Pending Activities slot
FUNC INT Get_Pending_Activities_GroupSlot_For_Slot(INT paramSlot)
	RETURN (GlobalServerBD_MissionsShared.activities[paramSlot].paGroupSlot)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the ContactID for a specific Pending Activities slot
FUNC enumCharacterList Get_Pending_Activities_ContactID_For_Slot(INT paramSlot)
	RETURN (GlobalServerBD_MissionsShared.activities[paramSlot].paContact)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Registered PlayerBits for a specific Pending Activities slot
//
// INPUT PARAMS:			paramSlot					Pending Activities slot
// RETURN VALUE:			INT							Registered PlayerBits in slot
FUNC INT Get_Pending_Activities_Registered_PlayerBits_For_Slot(INT paramSlot)
	RETURN (GlobalServerBD_MissionsShared.activities[paramSlot].paPlayersReg)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if any of the players passed as a bitfield match the players registered for this slot
//
// INPUT PARAMS:			paramSlot					Pending Activities slot
//							paramPlayerBits				Player Bitfield
// RETURN VALUE:			BOOL						TRUE if any of the passed in players is registered for this slot, otherwise FALSE
FUNC BOOL Are_Any_Of_These_Players_Registered_with_Pending_Activity_In_Slot(INT paramSlot, INT paramPlayerBits)

	IF (paramPlayerBits = ALL_PLAYER_BITS_CLEAR)
		RETURN FALSE
	ENDIF

	IF ((paramPlayerBits & Get_Pending_Activities_Registered_PlayerBits_For_Slot(paramSlot)) = ALL_PLAYER_BITS_CLEAR)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Mission Rank for a specific Pending Activities slot
//
// INPUT PARAMS:			paramSlot					Pending Activities slot
// RETURN VALUE:			INT							Rank in slot
FUNC INT Get_Pending_Activities_Rank_For_Slot(INT paramSlot)
	RETURN (GlobalServerBD_MissionsShared.activities[paramSlot].paRank)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the details in the specified slot match the passed-in parameters
//
// INPUT PARAMS:		paramSlot			The Missions Shared slot
//						paramGroupArrayPos	The Group Array position (or ILLEGAL_PENDING_ACTIVITIES_GROUP to ignore Group Array)
//						paramContactID		The ContactID (or NO_CHARACTER to ignore contactID)
//						paramPlayerBits		Search for all missions from each player in bitfield (or ALL_PLAYER_BITS_CLEAR to ignore players)
//						paramMinRank		Minimum Mission Rank - Inclusive (or MIN_PENDING_ACTIVITIES_RANK)
//						paramMaxRank		Maximum Mission Rank - Inclusive (or MAX_PENDING_ACTIVITIES_RANK)
// RETURN VALUE:		BOOL				TRUE if the details match, otherwise FALSE
FUNC BOOL Do_Pending_Activities_Slot_Details_Match(INT paramSlot, INT paramGroupArrayPos, enumCharacterList paramContactID, INT paramPlayerBits, INT paramMinRank, INT paramMaxRank)

	// Slot in use?
	IF NOT (Is_Pending_Activities_Slot_In_Use(paramSlot))
		RETURN FALSE
	ENDIF

	// Rank Error Checking
	IF (paramMinRank > paramMaxRank)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared]: Do_Pending_Activities_Slot_Details_Match() - ERROR: Min Rank (")
			NET_PRINT_INT(paramMinRank)
			NET_PRINT(") is greater than Max Rank (")
			NET_PRINT_INT(paramMaxRank)
			NET_PRINT(")")
			NET_NL()
			DEBUG_PRINTCALLSTACK()
			SCRIPT_ASSERT("Do_Pending_Activities_Slot_Details_Match(): ERROR - Min Rank is greater than Max Rank. Setting Max to Min to allow processing to continue. Tell Keith.")
		#ENDIF
	
		// Set Max to Min to allow processing to continue
		paramMaxRank = paramMinRank
	ENDIF

	// What to search for?
	BOOL matchGroup		= FALSE
	BOOL matchContact	= FALSE
	BOOL matchPlayers	= FALSE
	
	IF (paramGroupArrayPos != ILLEGAL_PENDING_ACTIVITIES_GROUP)
		matchGroup		= TRUE
	ENDIF
	
	IF (paramContactID != NO_CHARACTER)
		matchContact	= TRUE
	ENDIF
	
	IF (paramPlayerBits != ALL_PLAYER_BITS_CLEAR)
		matchPlayers	= TRUE
	ENDIF
	
	// Check for matching Group (if required)
	IF (matchGroup)
		IF (Get_Pending_Activities_GroupSlot_For_Slot(paramSlot) != paramGroupArrayPos)
			// ...failed to match group
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Check for matching contact (if required)
	IF (matchContact)
		IF (Get_Pending_Activities_ContactID_For_Slot(paramSlot) != paramContactID)
			// ...failed to match contact
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Check for matching players (if required)?
	IF (matchPlayers)
		// ...check for matching players - it just needs one of the passed in players to match to treat this as matched
		IF NOT (Are_Any_Of_These_Players_Registered_with_Pending_Activity_In_Slot(paramSlot, paramPlayerBits))
			// ...failed to find any matching players
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Check for max rank
	INT rankInSlot = Get_Pending_Activities_Rank_For_Slot(paramSlot)
	
	IF (rankInSlot < paramMinRank)
	OR (rankInSlot > paramMaxRank)
		// ...mission rank is outside the min/max rank boundaries
		RETURN FALSE
	ENDIF
	
	// This must be a match
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if any pending Activities match these details
//
// INPUT PARAMS:		paramGroupArrayPos	The Group Array position (or ILLEGAL_PENDING_ACTIVITIES_GROUP to ignore Group Array)
//						paramContactID		The ContactID (or NO_CHARACTER to ignore contactID)
//						paramPlayerBits		Search for all missions from each player in bitfield (or ALL_PLAYER_BITS_CLEAR to ignore players)
//						paramMinRank		Minimum Mission Rank - Inclusive (or MIN_PENDING_ACTIVITIES_RANK)
//						paramMaxRank		Maximum Mission Rank - Inclusive (or MAX_PENDING_ACTIVITIES_RANK)
// RETURN VALUE:		BOOL				TRUE if at least one match is found, otherwise FALSE
FUNC BOOL Do_Any_Pending_Activities_Match_These_Details(INT paramGroupArrayPos, enumCharacterList paramContactID, INT paramPlayerBits, INT paramMinRank, INT paramMaxRank)

	INT tempLoop = 0
	
	REPEAT MAX_PENDING_ACTIVITIES tempLoop
		IF (Do_Pending_Activities_Slot_Details_Match(tempLoop, paramGroupArrayPos, paramContactID, paramPlayerBits, paramMinRank, paramMaxRank))
			// ...found a match
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	// No Match
	RETURN FALSE

ENDFUNC







