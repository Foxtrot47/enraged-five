USING "rage_builtins.sch"
USING "globals.sch"

USING "cellphone_public.sch"



// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_App_Public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains common functions used by network cellphone apps.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      Variables and Constants
// ===========================================================================================================

// Active MP App Buttons (so that input routines can react correctly) - BITS
CONST_INT	CLEAR_ALL_MP_APP_BITS						0
CONST_INT	MP_APP_ALLOW_POSITIVE_RESPONSE_INPUT_BIT	0
CONST_INT	MP_APP_ALLOW_SPECIAL_RESPONSE_INPUT_BIT		1
CONST_INT	MP_APP_ALLOW_NEGATIVE_RESPONSE_INPUT_BIT	2


// -----------------------------------------------------------------------------------------------------------

// An 'Ignore Cellphone Bit' CONST
CONST_INT	IGNORE_STEVE_CELLPHONE_BIT			-1


// -----------------------------------------------------------------------------------------------------------

// Navigation Delay
CONST_INT	MP_APP_NAVIGATION_DELAY_msec		50


// -----------------------------------------------------------------------------------------------------------

// The Standard scaleform Soft Key Icon ENUMS used for button graphics (see scaleform wiki)
CONST_INT	MP_APP_ICON_BLANK		1
CONST_INT	MP_APP_ICON_SELECT		2
CONST_INT	MP_APP_ICON_BACK		4
CONST_INT	MP_APP_ICON_HANGUP		6
CONST_INT	MP_APP_ICON_DELETE		12
CONST_INT	MP_APP_ICON_YES			13
CONST_INT	MP_APP_ICON_NO			14


// -----------------------------------------------------------------------------------------------------------

// The standard struct - an instance needs to be declared in all App scripts and passed into the functions below.
STRUCT structMPAppData
	INT		mpadBitset				// Contains any Mp App specific bits
	//TIME_DATATYPE		mpadNavTimeout			// The Time when the navigation delay expires
	INT 				mpadNavTimeout			// Changed to int because its needed in SP and MP
	//TIME_DATATYPE		mpadNetworkTimer		// A stored instance of the network timer used throughout the app
ENDSTRUCT






// ===========================================================================================================
//      Initialisation Routines
// ===========================================================================================================

// PURPOSE:	Clear the variables
//
// REF PARAMS:		paramAppData			An instance of the App Data struct
PROC Initialise_MP_App_Variables(structMPAppData &paramAppData)

	paramAppData.mpadBitset		= CLEAR_ALL_MP_APP_BITS

ENDPROC





// ===========================================================================================================
//      Input Routines
// ===========================================================================================================

// PURPOSE:	Check for up/down navigation on a list screen
//
// REF PARAMS:		paramAppData			An instance of the App Data struct
//
// NOTES:	Ignores input unless screen is fully faded in.
PROC Check_For_MP_App_Navigation_Inputs(structMPAppData &paramAppData)

	// Are we allowed to accept up/down input?
	//IF IS_TIME_LESS_THAN(paramAppData.mpadNetworkTimer , paramAppData.mpadNavTimeout)
	IF GET_GAME_TIMER() < paramAppData.mpadNavTimeout
		EXIT
	ENDIF
	
	// Ignore JobList navigation if screen is not fully faded in
	IF NOT (IS_SCREEN_FADED_IN())
		EXIT
	ENDIF
	
	// Navigation is allowed
	// Check for UP
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT)))
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)
		Call_Scaleform_Input_Keypress_Up()
		paramAppData.mpadNavTimeout = GET_GAME_TIMER() + MP_APP_NAVIGATION_DELAY_msec
		//paramAppData.mpadNavTimeout = GET_TIME_OFFSET(paramAppData.mpadNetworkTimer , MP_APP_NAVIGATION_DELAY_msec)
	ENDIF

	// Check for DOWN
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT)))
	OR	IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)
		Call_Scaleform_Input_Keypress_Down()
		paramAppData.mpadNavTimeout = GET_GAME_TIMER() + MP_APP_NAVIGATION_DELAY_msec
		//paramAppData.mpadNavTimeout = GET_TIME_OFFSET(paramAppData.mpadNetworkTimer , MP_APP_NAVIGATION_DELAY_msec)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player pressed the 'Positive Response' button
//
// REF PARAMS:			paramAppData	An instance of the App Data struct
// RETURN VALUE:		BOOL			TRUE if the player pressed 'YES/CONFIRM/SELECT/etc' (left) button, otherwise FALSE
//
// NOTES:	Ignores input unless screen is fully faded in.
FUNC BOOL Did_Player_Press_MP_App_Positive_Response_Button(structMPAppData &paramAppData)

	// Is a positive response allowed (ie: is the positive input button being displayed)
	IF NOT (IS_BIT_SET(paramAppData.mpadBitset, MP_APP_ALLOW_POSITIVE_RESPONSE_INPUT_BIT))
		RETURN FALSE
	ENDIF

	// Need to disable standard POSITIVE BUTTON functionality
//	CONTROL_ACTION theAction = INT_TO_ENUM(CONTROL_ACTION, PHONE_POSITIVE_INPUT)
//	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, theAction)

	// Check if another cellphone button press is currently allowed
	IF (g_InputButtonJustPressed)
		RETURN FALSE
	ENDIF
	
	// Ignore button input if screen is not fully faded in
	IF NOT (IS_SCREEN_FADED_IN())
		RETURN FALSE
	ENDIF
	
	// A cellphone button press is allowed, so check if the Select button was pressed
	IF NOT (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)))
		RETURN FALSE
	ENDIF
	
	// Select was pressed
	Play_Select_Beep()
	g_InputButtonJustPressed = TRUE
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Did_Player_Press_MP_App_Positive_Response_Button() - Player pressed POSITIVE RESPONSE") NET_NL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player pressed the 'Negative Response' button
//
// REF PARAMS:			paramAppData	An instance of the App Data struct
// RETURN VALUE:		BOOL			TRUE if the player pressed 'NO/BACK/etc' (right) button, otherwise FALSE
//
// NOTES:	Ignores input unless screen is fully faded in.
FUNC BOOL Did_Player_Press_MP_App_Negative_Response_Button(structMPAppData &paramAppData)

	// Is a negative response allowed (ie: is the negative input button being displayed)
	IF NOT (IS_BIT_SET(paramAppData.mpadBitset, MP_APP_ALLOW_NEGATIVE_RESPONSE_INPUT_BIT))
		RETURN FALSE
	ENDIF

	// Need to disable standard NEGATIVE BUTTON functionality
//	CONTROL_ACTION theAction = INT_TO_ENUM(CONTROL_ACTION, PHONE_NEGATIVE_INPUT)
//	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, theAction)

	// Check if another cellphone button press is currently allowed
	IF (g_InputButtonJustPressed)
		RETURN FALSE
	ENDIF
	
	// Ignore button input if screen is not fully faded in
	IF NOT (IS_SCREEN_FADED_IN())
		RETURN FALSE
	ENDIF
	
	// A cellphone button press is allowed, so check if the Back button was pressed
	IF NOT (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)))
		RETURN FALSE
	ENDIF
	
	// Select was pressed
	Play_Back_Beep()
	g_InputButtonJustPressed = TRUE
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Did_Player_Press_MP_App_Negative_Response_Button() - Player pressed NEGATIVE RESPONSE") NET_NL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player pressed the 'Special Response' button
//
// REF PARAMS:			paramAppData	An instance of the App Data struct
// RETURN VALUE:		BOOL			TRUE if the player pressed the middle button, otherwise FALSE
//
// NOTES:	Ignores input unless screen is fully faded in.
FUNC BOOL Did_Player_Press_MP_App_Special_Response_Button(structMPAppData &paramAppData)

	// Is a special response allowed (ie: is the special input button being displayed)
	IF NOT (IS_BIT_SET(paramAppData.mpadBitset, MP_APP_ALLOW_SPECIAL_RESPONSE_INPUT_BIT))
		RETURN FALSE
	ENDIF

	// Need to disable standard SPECIAL BUTTON functionality
//	CONTROL_ACTION theAction = INT_TO_ENUM(CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)
//	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, theAction)

	// Check if another cellphone button press is currently allowed
	IF (g_InputButtonJustPressed)
		RETURN FALSE
	ENDIF
	
	// Ignore button input if screen is not fully faded in
	IF NOT (IS_SCREEN_FADED_IN())
		RETURN FALSE
	ENDIF
	
	// A cellphone button press is allowed, so check if the Special button was pressed
	IF NOT (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)))
		RETURN FALSE
	ENDIF
	
	// Select was pressed
	Play_Select_Beep()
	g_InputButtonJustPressed = TRUE
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Did_Player_Press_MP_App_Special_Response_Button() - Player pressed SPECIAL RESPONSE") NET_NL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC





// ===========================================================================================================
//      Display Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Button Display
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If necessary, set or clear a BIT within Steve's cellphone routines so that he knows that the pad button is required by the cellphone.
//
// INPUT PARAMS:		paramButtonBit			The BIT within the BitSet_CellphoneDisplay to set or clear.
//						paramState				TRUE to set the bit, FALSE to clear it
PROC Set_Steve_Cellphone_Button_Bit(INT paramButtonBit, BOOL paramState)

	IF (paramButtonBit = IGNORE_STEVE_CELLPHONE_BIT)
		EXIT
	ENDIF
	
	// A bit needs set or cleared
	IF (paramState)
		// ...set the bit
		SET_BIT(BitSet_CellphoneDisplay, paramButtonBit)
		
		EXIT
	ENDIF
	
	// Clear the Bit
	CLEAR_BIT(BitSet_CellphoneDisplay, paramButtonBit)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Button Display
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Accept the data for displaying one cellphone button and optional text and returns whether a button was displayed or not
//
// INPUT PARAMS:		paramButtonID			ButtonID Number
//						paramIcon				Soft Key Icon enum constants
//						paramText				Help Text for icon
//						paramKeyBit				The Bit to set or clear to ensure allowed input corresponds with the buttons on display
//						paramButtonBit			[DEFAULT = IGNORE_STEVE_CELLPHONE_BIT] Sets a Bit in Steve's cellphone to TRUE if button on display, otherwise FALSE
// REF PARAMS:			paramAppData			An instance of the App Data struct
//
// NOTES:	I tried using an INT variable set to 1 or 0 for display or not, but it only seemed to work if I passed in 1 or 0 directly
//			The paramButtonBit is used by Steve to allow or diallow other game functionality associated with that button - my MP app button routines also do something similar
PROC Display_One_MP_App_Button(INT paramButtonID, INT paramIcon, STRING paramText, INT paramKeyBit, structMPAppData &paramAppData, INT paramButtonBit = IGNORE_STEVE_CELLPHONE_BIT)

	IF (paramIcon = MP_APP_ICON_BLANK)
		// ...don't Display
		LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieIndex, "SET_SOFT_KEYS", (TO_FLOAT(paramButtonID)), 0, (TO_FLOAT(paramIcon)), INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM)
		CLEAR_BIT(paramAppData.mpadBitset, paramKeyBit)
		Set_Steve_Cellphone_Button_Bit(paramButtonBit, FALSE)
		EXIT
	ENDIF
	
	IF (g_b_ToggleButtonLabels)
		// ...display with text
		LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieIndex, "SET_SOFT_KEYS", (TO_FLOAT(paramButtonID)), 1, (TO_FLOAT(paramIcon)), INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, paramText)
		SET_BIT(paramAppData.mpadBitset, paramKeyBit)
		Set_Steve_Cellphone_Button_Bit(paramButtonBit, TRUE)
		EXIT
	ENDIF
	
	// Display without text
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieIndex, "SET_SOFT_KEYS", (TO_FLOAT(paramButtonID)), 1, (TO_FLOAT(paramIcon)), INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM)
	SET_BIT(paramAppData.mpadBitset, paramKeyBit)
	Set_Steve_Cellphone_Button_Bit(paramButtonBit, TRUE)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Accept the data for displaying the cellphone buttons and text
//
// INPUT PARAMS:		paramLeftIcon			Left:	Soft Key Icon enum constants
//						paramLeftText			Left:	Help Text for icon
//						paramCentreIcon			Centre:	Soft Key Icon enum constants
//						paramCentreText			Centre:	Help Text for icon
//						paramRightIcon			Right:	Soft Key Icon enum constants
//						paramRightText			Right:	Help Text for icon
// REF PARAMS:			paramAppData			An instance of the App Data struct
PROC Display_MP_App_Buttons(INT paramLeftIcon, STRING paramLeftText, INT paramCentreIcon, STRING paramCentreText, INT paramRightIcon, STRING paramRightText, structMPAppData &paramAppData)

	// Display Icons and Help Text, and update the 'allow' variable so that the input routines only work when a button is being displayed
	Display_One_MP_App_Button(BADGER_POS,	paramLeftIcon,		paramLeftText,		MP_APP_ALLOW_POSITIVE_RESPONSE_INPUT_BIT,	paramAppData)
	Display_One_MP_App_Button(BADGER_OTHER,	paramCentreIcon,	paramCentreText,	MP_APP_ALLOW_SPECIAL_RESPONSE_INPUT_BIT,	paramAppData,	g_BS_OTHER_OPTION_IS_DISPLAYED)
	Display_One_MP_App_Button(BADGER_NEG,	paramRightIcon,		paramRightText,		MP_APP_ALLOW_NEGATIVE_RESPONSE_INPUT_BIT,	paramAppData)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Scaleform Display Wrappers
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the screen header
//
// INPUT PARAMS:		paramHeader			The text label to display
//
// NOTES:	Assumes SF_MovieIndex is already setup (should be connected to the cellphone movie)
PROC Display_MP_App_Header(STRING paramHeader)
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING(SF_MovieIndex, "SET_HEADER", paramHeader)
ENDPROC








