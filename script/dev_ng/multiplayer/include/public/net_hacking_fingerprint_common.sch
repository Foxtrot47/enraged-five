USING "net_common_hacking_base.sch"

STRUCT FINGERPRINT_ENTITY
	INT iSprite_Id // sprite to be used
	INT iBitset // bitset for details
	INT iPositioning // which position
	
ENDSTRUCT

STRUCT FINGERPRINT_MINIGAME_GAMEPLAY_DATA
	HACKING_GAME_STRUCT sBaseStruct
	
	//Gameplay
	INT iF_Bs
	INT iSelectedElement
	
	INT iNumberOfLastMatchedElements
	
	INT iCurrentFingerprintIndex
	
	INT iSelectedElementCount
	
	INT iFingerPrintIds[8]
	FINGERPRINT_ENTITY sFingerPrintElements[8][8] 
	
	INT iCircleDecoration[3]
	INT iScrambledValues[8]
	INT iCheckingAlphaValue[8]
	// TIMERS
	INT iGridNoiseCounter
	INT iGridDetails1Counter
	INT iGridDetails2Counter
	
	INT iProcessingSoundID = -1
	
	HG_RGBA_COLOUR_STRUCT rgbaTintColour

ENDSTRUCT
