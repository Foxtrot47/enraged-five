//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_management.sch																			//
// Description: Should only contain generic functions used for MP management.								//
// Written by:  Aidan Temple																				//
// Date:  		11/11/2016																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


USING "net_simple_interior.sch"
USING "net_realty_biker_warehouse.sch"

STRUCT NO_WANTED_LEVEL_GAIN_ZONES_DATA
	INT iActiveZonesBS
	INT iZoneStagger
	INT iMaintainThisWantedLevel = -1
	BOOL bDisabledNoWantedLevelGainZones
	
	TIME_DATATYPE tTimeToReset
	
	#IF IS_DEBUG_BUILD
	BOOL db_bDrawZones
	BOOL db_bCheckedCMD
	#ENDIF
ENDSTRUCT

CONST_INT ciRESET_TIME 			15000

/// PURPOSE:
///    Checks if given zone should block gaining of wanted level
/// PARAMS:
///    iZone - 0 to 5 where 0 is owned clubhouse, 1-5 are owned factories
///    vZoneCenter - 
/// RETURNS:
///    
FUNC BOOL SHOULD_WANTED_LEVEL_GAINING_BE_SUPPRESSED_AROUND_THIS_ZONE(INT iZone, VECTOR &vZoneCenter)
	BOOL bShouldSuppress
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX playerOwner
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
	OR GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
		playerOwner = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
	ELSE
		// Not blocking if not part of gang.
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
		IF iZone = 0
			// Zone 0 is clubhouse. If we own it or our boss owns it - block gaining.
			IF GlobalPlayerBD_FM[NATIVE_TO_INT(playerOwner)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_CLUBHOUSE] > -1
				vZoneCenter = mpProperties[GlobalPlayerBD_FM[NATIVE_TO_INT(playerOwner)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_CLUBHOUSE]].entrance[0].vEntranceMarkerLoc
				bShouldSuppress = TRUE
			ENDIF
		ELIF iZone > 0 AND iZone < ciMAX_OWNED_FACTORIES + 1
			// Zones 1 to 1+n where n is max number of factories that can be owned - are factories.
			FACTORY_ID eFactoryID = GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iZone - 1)
			IF DOES_PLAYER_OWN_FACTORY(playerOwner, eFactoryID)
				GET_FACTORY_ENTRY_LOCATE(GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(eFactoryID), vZoneCenter)
				bShouldSuppress = TRUE
				
				#IF IS_DEBUG_BUILD
				PRINTLN("SHOULD_WANTED_LEVEL_GAINING_BE_SUPPRESSED_AROUND_THIS_ZONE - Players gang boss owns Factory property with ID ", ENUM_TO_INT(eFactoryID), " bShouldSuppress = TRUE")
				#ENDIF
			ENDIF
		ENDIF
	ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
		IF DOES_PLAYER_OWN_A_BUNKER(playerOwner)
			IF iZone > 0
				FACTORY_ID eBunkerID = GET_OWNED_BUNKER(playerOwner)
				
				IF eBunkerID != FACTORY_ID_INVALID
					GET_FACTORY_ENTRY_LOCATE(GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(eBunkerID), vZoneCenter)
					bShouldSuppress = TRUE
					
					#IF IS_DEBUG_BUILD
					PRINTLN("SHOULD_WANTED_LEVEL_GAINING_BE_SUPPRESSED_AROUND_THIS_ZONE - Players gang boss owns Bunker property with ID ", ENUM_TO_INT(eBunkerID), " bShouldSuppress = TRUE")
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			IF iZone > 0
				IMPORT_EXPORT_GARAGES eGarageID = GET_PLAYERS_OWNED_IE_GARAGE(playerOwner)
								
				IF eGarageID != IE_GARAGE_INVALID
				AND DOES_PLAYER_OWN_IE_GARAGE(playerOwner, eGarageID)
					GET_IE_GARAGE_ENTRY_LOCATE(GET_SIMPLE_INTERIOR_ID_FROM_IMPORT_EXPORT_GARAGE_ID(eGarageID), vZoneCenter)
					bShouldSuppress = TRUE
					
					#IF IS_DEBUG_BUILD
					PRINTLN("SHOULD_WANTED_LEVEL_GAINING_BE_SUPPRESSED_AROUND_THIS_ZONE - Players gang boss owns Garage property with ID ", ENUM_TO_INT(eGarageID), " bShouldSuppress = TRUE")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		// Do nothing.
	ENDIF
	
	RETURN bShouldSuppress
ENDFUNC

/// PURPOSE:
///    Disable no wanted level gain zones around properties.
/// PARAMS:
///    bDisable - 
PROC SET_DISABLE_NO_WANTED_LEVEL_GAIN_ZONES(BOOL bDisable)
	IF bDisable
		IF NOT g_DisableNoWantedLevelGainZones
			g_DisableNoWantedLevelGainZones = TRUE
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_DISABLE_NO_WANTED_LEVEL_GAIN_ZONES - Disabling!")
			#ENDIF
		ENDIF
	ELSE
		IF g_DisableNoWantedLevelGainZones
			g_DisableNoWantedLevelGainZones = FALSE
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_DISABLE_NO_WANTED_LEVEL_GAIN_ZONES - Enabling!")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Players around owned clubhouses and factories can't gain wanted level.
/// PARAMS:
///    localData -
PROC MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES(NO_WANTED_LEVEL_GAIN_ZONES_DATA &localData)

	#IF IS_DEBUG_BUILD
	IF NOT localData.db_bCheckedCMD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawBikerNoWantedLevelGainZones")
			localData.db_bDrawZones = TRUE
		ENDIF
		localData.db_bCheckedCMD = TRUE
	ENDIF
	#ENDIF

	IF localData.bDisabledNoWantedLevelGainZones
		#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - bDisabledNoWantedLevelGainZones = true, resetting flags.")
		#ENDIF
	
		g_DisableNoWantedLevelGainZones = FALSE
		localData.bDisabledNoWantedLevelGainZones = FALSE
		localData.tTimeToReset = GET_NETWORK_TIME()
	ENDIF
	
	// Fix for B*3274276.
	IF(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), localData.tTimeToReset)) > ciRESET_TIME)
		#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - tTimeToReset has elapsed.")
		#ENDIF
		
		g_DisableNoWantedLevelGainZones = TRUE
		localData.bDisabledNoWantedLevelGainZones = TRUE
	ENDIF

	IF g_DisableNoWantedLevelGainZones
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 360 = 0
			PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - Not running because g_DisableNoWantedLevelGainZones is TRUE")
			ENDIF
		#ENDIF
		
		IF localData.iActiveZonesBS > 0
			#IF IS_DEBUG_BUILD
			PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - iActiveZonesBS = ", localData.iActiveZonesBS)
			#ENDIF
			localData.iActiveZonesBS = 0
		ENDIF
		
		IF localData.iMaintainThisWantedLevel > 0
			#IF IS_DEBUG_BUILD
			PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - iMaintainThisWantedLevel = ", localData.iMaintainThisWantedLevel)
			#ENDIF
			localData.iMaintainThisWantedLevel = -1
		ENDIF
		
		EXIT
	ENDIF
	
	VECTOR vCoords
	BOOL bShouldSuppress = SHOULD_WANTED_LEVEL_GAINING_BE_SUPPRESSED_AROUND_THIS_ZONE(localData.iZoneStagger, vCoords)
	VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())

	vCoords.Z = 0.0
	vPlayerCoords.Z = 0.0
	
	IF NOT IS_BIT_SET(localData.iActiveZonesBS, localData.iZoneStagger)
		IF bShouldSuppress
		AND VDIST(vPlayerCoords, vCoords) <= 50.0
			IF localData.iActiveZonesBS = 0
				localData.iMaintainThisWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
			ENDIF
			SET_BIT(localData.iActiveZonesBS, localData.iZoneStagger)
			
			#IF IS_DEBUG_BUILD
			PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - We got close to zone ", localData.iZoneStagger, " and wanted level gaining should be suppressed around it.")
			#ENDIF
		ENDIF
	ELSE
		IF (NOT bShouldSuppress)
		OR (VDIST(vPlayerCoords, vCoords) > 50.1)
			CLEAR_BIT(localData.iActiveZonesBS, localData.iZoneStagger)
			
			IF localData.iActiveZonesBS = 0
				localData.iMaintainThisWantedLevel = -1
			ENDIF
			
			#IF IS_DEBUG_BUILD
			PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - We got away from zone ", localData.iZoneStagger, " or it's not supposed to block wanted level gaining.")
			#ENDIF
		ENDIF
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
		localData.iZoneStagger = (localData.iZoneStagger + 1) % 6
	ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
		localData.iZoneStagger = (localData.iZoneStagger + 1) % 1
	ENDIF
	
	IF localData.iActiveZonesBS > 0
		IF localData.iMaintainThisWantedLevel > -1
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > localData.iMaintainThisWantedLevel
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), localData.iMaintainThisWantedLevel)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				#IF IS_DEBUG_BUILD
					PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - Player gained wanted level, setting it back to ", localData.iMaintainThisWantedLevel)
				#ENDIF
			ELIF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < localData.iMaintainThisWantedLevel
				// Allow max wanted level to drop so if players lose it while being in the zone they can't gain it again.
				localData.iMaintainThisWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF localData.db_bDrawZones
			INT iZones
			
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
				iZones = 6
			ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
				iZones = 1
			ENDIF
		
			INT i
			REPEAT iZones i
				SHOULD_WANTED_LEVEL_GAINING_BE_SUPPRESSED_AROUND_THIS_ZONE(i, vCoords)
				//DRAW_DEBUG_SPHERE(vCoords, 50.0, 191, 96, 215, )
				DRAW_MARKER(MARKER_CYLINDER, vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<50.0 * 2.0, 50.0 * 2.0, 10.0>>, PICK_INT(IS_BIT_SET(localData.iActiveZonesBS, i), 255, 140), 120, 120, 128)
			ENDREPEAT
		ENDIF
	#ENDIF
ENDPROC
