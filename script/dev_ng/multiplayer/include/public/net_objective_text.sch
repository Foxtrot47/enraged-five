USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_network.sch"

USING "net_comms_public.sch"
USING "net_prints.sch"
USING "net_hud_activating.sch"

#IF IS_DEBUG_BUILD
USING "script_metrics_public.sch"
#ENDIF


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Objective_Text.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Allows a script to specify a piece of text as Objective Text which will be re-displayed when all other God
//								Text has been removed.
//
//		NOTES			:	Objective Text On The Brief Screen - 18/5/13
//							Code usually ensures that a duplicate of a previus entry on teh brief screen doesn't get displayed, so it
//							usually doesn't matter how often I re-display a piece of Objective Text, it only appears on the Brief
//							Screen once. This isn't the case with Literal Strings. Code can't detect duplicates of these on the
//							Brief Screen, so I need to handle this.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      Objective Text Debug routines
// ===========================================================================================================

#IF IS_DEBUG_BUILD
FUNC STRING Convert_Objective_Text_Type_To_String()

	SWITCH (g_sObjectiveTextMP.otTextType)
		CASE OTT_NONE
			RETURN "None"
			
		CASE OTT_STRING
			RETURN "String Only"
			
		CASE OTT_STRING_WITH_NUMBER
			RETURN "String With Number"
			
		CASE OTT_STRING_WITH_TWO_NUMBERS
			RETURN "String With Two Numbers"
			
		CASE OTT_STRING_WITH_STRING
			RETURN "String With String"
			
		CASE OTT_STRING_WITH_TWO_STRINGS
			RETURN "String With Two Strings"
			
		CASE OTT_STRING_WITH_PLAYER_NAME
			RETURN "String With Uncoloured Player Name"
			
		CASE OTT_STRING_WITH_COLOURED_PLAYER_NAME
			RETURN "String With Coloured Player Name"
			
		CASE OTT_STRING_WITH_TEAM_NAME
			RETURN "String With uncoloured Team Name"
			
		CASE OTT_STRING_WITH_TEAM_COLOURED_TEAM_NAME
			RETURN "String With Team-Coloured Team Name"
			
		CASE OTT_STRING_WITH_USER_CREATED_STRING
			RETURN "String With User-Created String"
			
		CASE OTT_STRING_WITH_TWO_USER_CREATED_STRINGS
			RETURN "String With Two User-Created Strings"
			
		CASE OTT_STRING_AS_USER_CREATED_STRING
			RETURN "String As User-Created String"
			
		CASE OTT_STRING_WITH_PLAYER_NAME_AND_STRING
			RETURN "String With Player Name and String"
		
		CASE OTT_STRING_WITH_TWO_STRINGS_COLOURED
			RETURN "String With Two Strings Coloured"
			
		CASE OTT_STRING_WITH_COLOURED_NAME_AND_STRING
			RETURN "String With Coloured Player Name and String"
		
		CASE OTT_STRING_WITH_PLAYER_NAME_AND_STRING_COLOURED
			RETURN "String With Player Name and String coloured"
		
		CASE OTT_STRING_WITH_COLOURED_STRING
			RETURN "String With Coloured String"
			
		CASE OTT_STRING_WITH_COLOURED_TEXT_LABEL
			RETURN "String With Coloured Text Label"
		
		CASE OTT_STRING_WITH_TWO_PLAYER_NAMES_AND_STRING
			RETURN "String with Two Player Names and String"
			
		CASE OTT_STRING_WITH_TWO_PLAYER_NAMES_AND_STRING_COLOURED
			RETURN "String with Two Player Names and String (all coloured)"
			
	ENDSWITCH
	
	RETURN "UNKNOWN"

ENDFUNC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
PROC Debug_Output_Objective_Text_To_Console_Log(INT paramDisplayTime = -1)

	NET_PRINT("           original request from script  : ") NET_PRINT(g_sObjectiveTextMP.otScriptName) NET_PRINT("  [HASH = ") NET_PRINT_INT(g_sObjectiveTextMP.otScriptNameHash) NET_PRINT("]") NET_NL()
	NET_PRINT("           Objective Text Type           : ") NET_PRINT(Convert_Objective_Text_Type_To_String()) NET_NL()
	
	IF (g_sObjectiveTextMP.otTextType != OTT_STRING_AS_USER_CREATED_STRING)
		NET_PRINT("           Main String                   : [") NET_PRINT(g_sObjectiveTextMP.otMainTextLabel) NET_PRINT("] ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otMainTextLabel)) NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_STRING)
	OR (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TWO_STRINGS)
		NET_PRINT("           First Additional String       : [") NET_PRINT(g_sObjectiveTextMP.otStringInString1) NET_PRINT("] ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otStringInString1)) NET_NL()
		
		IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TWO_STRINGS)
			NET_PRINT("           Second Additional String      : [") NET_PRINT(g_sObjectiveTextMP.otStringInString2) NET_PRINT("] ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otStringInString2)) NET_NL()
		ENDIF
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_NUMBER)
	OR (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TWO_NUMBERS)
		NET_PRINT("           First Number                  : [") NET_PRINT_INT(g_sObjectiveTextMP.otNumber1) NET_PRINT("] ") NET_NL()
		
		IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TWO_NUMBERS)
			NET_PRINT("           Second Number                 : [") NET_PRINT_INT(g_sObjectiveTextMP.otNumber2) NET_PRINT("] ") NET_NL()
		ENDIF
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_PLAYER_NAME)
		NET_PRINT("           (uncoloured) Player Name      : [")
		IF (IS_NET_PLAYER_OK(g_sObjectiveTextMP.otPlayerID,FALSE))
			NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		ELSE
			NET_PRINT("Player Index No Longer Valid - ")
			NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		ENDIF
		NET_PRINT("] ") NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_COLOURED_PLAYER_NAME)
		NET_PRINT("           Coloured Player Name          : [")
		IF (IS_NET_PLAYER_OK(g_sObjectiveTextMP.otPlayerID,FALSE))
			NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		ELSE
			NET_PRINT("Player Index No Longer Valid - ")
			NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		ENDIF
		NET_PRINT("  (Team = ")
		NET_PRINT(Convert_Team_To_String(g_sObjectiveTextMP.otTeamID))
		NET_PRINT(")] ") NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TEAM_NAME)
		NET_PRINT("           (uncoloured) Team Name        : [")
		NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otStringInString1))
		NET_PRINT("] ") NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TEAM_COLOURED_TEAM_NAME)
		NET_PRINT("           Team-Coloured Team Name       : [")
		NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otStringInString1))
		NET_PRINT("] ") NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_USER_CREATED_STRING)
		NET_PRINT("           Additional User-Created String: ")
		NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TWO_USER_CREATED_STRINGS)
		NET_PRINT("           Additional User-Created Strings: ")
		NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		NET_NL() NET_PRINT(g_sObjectiveTextMP.otStringInString2)
		NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_PLAYER_NAME_AND_STRING)
		NET_PRINT("           Additional Player Name with String: ")
		NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		NET_NL() NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otStringInString2))
		NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_COLOURED_NAME_AND_STRING)
		NET_PRINT("           Additional Coloured Player Name with String: ")
		NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		NET_NL() NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otStringInString2))
		NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TWO_STRINGS_COLOURED)
		NET_PRINT("           Additional Two Strings Coloured: ")
		NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otStringInString1))
		NET_NL() NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otStringInString2))
		NET_NL()
	ENDIF
	
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_PLAYER_NAME_AND_STRING_COLOURED)
		NET_PRINT("           Additional Player Name with String Coloured: ")
		NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		NET_NL() NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otStringInString2))
		NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_COLOURED_STRING)
		NET_PRINT("           Additional Coloured String: ")
		NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_COLOURED_TEXT_LABEL)
		NET_PRINT("           Additional Coloured Text Label: ")
		NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_AS_USER_CREATED_STRING)
		NET_PRINT("           User-Created String           : ")
		NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		NET_NL()
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TWO_PLAYER_NAMES_AND_STRING)
		NET_PRINT("           User-Created Strings           : ")
		NET_PRINT(g_sObjectiveTextMP.otStringInString1)
		NET_NL() NET_PRINT(g_sObjectiveTextMP.otStringInString2)
		NET_NL()
	ENDIF
	
	
	
	IF (paramDisplayTime >= 0)
		NET_PRINT("           Display Time Remaining        : ")
		NET_PRINT(GET_STRING_FROM_MILISECONDS(paramDisplayTime))
		NET_PRINT(" (h:m:s.ms) ") NET_NL()
	ENDIF
	
	// Any optional flags
	NET_PRINT("           Special Display Options       :")
	IF (g_sObjectiveTextMP.otOptionsBitfield = ALL_OBJECTIVE_TEXT_BITS_CLEAR)
		NET_PRINT(" NONE")
	ELSE
		// Display while hands tied?
		IF (IS_BIT_SET(g_sObjectiveTextMP.otOptionsBitfield, OT_BITFLAG_DISPLAY_WHILE_HANDS_TIED))
			NET_PRINT(" [DISPLAY WHILE HANDS TIED]")
		ENDIF
	ENDIF
	NET_NL()
		
	DEBUG_PRINTCALLSTACK()
ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
// PURPOSE:	Create the MP Objective Text Widgets
PROC Create_MP_Objective_Text_Widgets()

	g_mainObjectiveTextWidgetGroup = START_WIDGET_GROUP("KGM: MP Objective Text")
		ADD_WIDGET_BOOL("Objective Text Received?",										g_WIDGET_ObjectiveTextReceived)
		ADD_WIDGET_BOOL("Objective Text Has Been Displayed (possibly interrupted)?",	g_WIDGET_ObjectiveTextOnDisplay)
		ADD_WIDGET_BOOL("Objective Text Deliberately Hidden (hands tied, etc)?",		g_WIDGET_ObjectiveTextHiddenOnPurpose)
		ADD_WIDGET_BOOL("Objective Text Blocked (because Comms Device Active)?",		g_WIDGET_ObjectiveTextBlocked_Comms)
		ADD_WIDGET_BOOL("Objective Text Blocked (because MP HUD Active)?",				g_WIDGET_ObjectiveTextBlocked_MPHUD)
		ADD_WIDGET_BOOL("Objective Text Blocked (because it has been Paused)?",			g_WIDGET_ObjectiveTextBlocked_Paused)
		ADD_WIDGET_BOOL("For Info: Is Any God Text On Display?",						g_WIDGET_ObjectiveTextIsGodTextOn)
		ADD_WIDGET_BOOL("For Info: Is Communication Device Active?",					g_WIDGET_ObjectiveTextIsCommsActive)
		ADD_WIDGET_BOOL("For Info: Is MP HUD On Display?",								g_WIDGET_ObjectiveTextIsMPHUDOn)
		ADD_WIDGET_BOOL("For Info: Is Paused?",											g_WIDGET_ObjectiveTextIsPaused)
	STOP_WIDGET_GROUP()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
// PURPOSE:	Destroy the MP Objective Text Widgets
PROC Destroy_MP_Objective_Text_Widgets()
	IF g_mainObjectiveTextWidgetGroup != NULL 
		DELETE_WIDGET_GROUP(g_mainObjectiveTextWidgetGroup)
		g_mainObjectiveTextWidgetGroup = NULL
	ENDIF
ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
// PURPOSE:	Reset all the widget globals
PROC Reset_MP_Objective_Text_Widget_Globals()

	g_WIDGET_ObjectiveTextReceived			= FALSE
	g_WIDGET_ObjectiveTextOnDisplay			= FALSE
	g_WIDGET_ObjectiveTextHiddenOnPurpose	= FALSE
	g_WIDGET_ObjectiveTextBlocked_Comms		= FALSE
	g_WIDGET_ObjectiveTextBlocked_Paused	= FALSE
	g_WIDGET_ObjectiveTextIsCommsActive		= FALSE
	g_WIDGET_ObjectiveTextIsGodTextOn		= FALSE
	g_WIDGET_ObjectiveTextIsMPHUDOn			= FALSE
	g_WIDGET_ObjectiveTextIsPaused			= FALSE

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
// PURPOSE:	Update the MP Objective Text Widgets that help with the decision making
PROC Update_MP_Objective_Text_Info_Widgets()

	g_WIDGET_ObjectiveTextIsGodTextOn		= IS_MESSAGE_BEING_DISPLAYED()
	g_WIDGET_ObjectiveTextIsCommsActive		= Is_There_An_Active_MP_Comms_Phonecall_To_The_Player()
	g_WIDGET_ObjectiveTextIsMPHUDOn			= IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	g_WIDGET_ObjectiveTextIsPaused			= g_sObjectiveTextControlMP.otcIsPaused

ENDPROC
#ENDIF




// ===========================================================================================================
//      Objective Text Global Variable Clear and Set Routines
// ===========================================================================================================

// PURPOSE:	Clear out all the objective text control variables
//
// INPUT PARAMS:	paramBeingInitialised		TRUE if this is being called during initialisation [default = FALSE]
PROC Clear_MP_Objective_Text_Variables(BOOL paramBeingInitialised = FALSE)

	BOOL inNetworkGame = NETWORK_IS_GAME_IN_PROGRESS()

	g_sObjectiveTextMP.otTextType			= OTT_NONE
	g_sObjectiveTextMP.otScriptName			= ""
	g_sObjectiveTextMP.otScriptNameHash		= 0
	IF (inNetworkGame)
		g_sObjectiveTextMP.otDisplayTimeout		= GET_NETWORK_TIME()
		g_sObjectiveTextMP.otRefreshTimeout		= GET_NETWORK_TIME()
	ENDIF
	g_sObjectiveTextMP.otMainTextLabel		= ""
	g_sObjectiveTextMP.otStringInString1	= ""
	g_sObjectiveTextMP.otStringInString2	= ""
	g_sObjectiveTextMP.otNumber1			= 0
	g_sObjectiveTextMP.otNumber2			= 0
	g_sObjectiveTextMP.otPlayerID			= NULL
	g_sObjectiveTextMP.otTeamID				= TEAM_INVALID
	g_sObjectiveTextMP.otHudColour			= HUD_COLOUR_PURE_WHITE
	g_sObjectiveTextMP.otOptionsBitfield	= ALL_OBJECTIVE_TEXT_BITS_CLEAR
	
	#IF IS_DEBUG_BUILD
		g_WIDGET_ObjectiveTextReceived = FALSE
		NET_PRINT("...KGM MP [Objective Text]: Clear_MP_Objective_Text_Variables() - All details cleared") NET_NL()
	#ENDIF
	
	// Don't update the widgets if being initialised
	// NOTE: the widgets don't yet exist
	IF (paramBeingInitialised)
		EXIT
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// Purpose: Clear out the Objective Text Pause Controls variables
PROC Clear_Objective_Text_Pause()

	g_sObjectiveTextControlMP.otcIsPaused				= FALSE
	g_sObjectiveTextControlMP.otcPauseScriptName		= ""
	g_sObjectiveTextControlMP.otcPauseScriptNameHash	= 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Clear_Objective_Text_Pause() - All pause details cleared") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out the Objective Text Control variables
PROC Clear_MP_Objective_Text_Controls()

	Clear_Objective_Text_Pause()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store initial values for the display timeout and the refresh timeout
// NOTES:	The refresh timeout should be set to refresh immediately for instant text display
PROC Calculate_And_Store_MP_Objective_Text_Timers()
	
	g_sObjectiveTextMP.otDisplayTimeout	= GET_TIME_OFFSET(GET_NETWORK_TIME(), OBJECTIVE_TEXT_DISPLAY_TIME_msec)
	g_sObjectiveTextMP.otRefreshTimeout	= GET_NETWORK_TIME()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Allows this Objective Text to be displayed while a player's hands are tied (or not)
//
// INPUT PARAMS:		paramDisplayWhileHandsTied			TRUE if the Objective Text should remain on display while a player's hands are tied, FALSE if it should be hidden
//
// NOTES:	By default, Objective Text is hidden while hands tied
PROC Set_Objective_Text_Option_Display_While_Hands_Tied(BOOL paramDisplayWhileHandsTied)

	IF (paramDisplayWhileHandsTied)
		SET_BIT(g_sObjectiveTextMP.otOptionsBitfield, OT_BITFLAG_DISPLAY_WHILE_HANDS_TIED)
		EXIT
	ENDIF
	
	CLEAR_BIT(g_sObjectiveTextMP.otOptionsBitfield, OT_BITFLAG_DISPLAY_WHILE_HANDS_TIED)
	
ENDPROC





// ===========================================================================================================
//      Objective Text Brief Screen Control routines - clear, set, query
//		(Code can't match text with literal strings to existing entries on the Brief, so always adds the
//		text to the Brief Screen again when re-displaying Objective Text with literal strings)
// ===========================================================================================================

// PURPOSE:	Set this Objective Text to be displayed on the brief screen
PROC Set_Objective_Text_Option_Display_On_Brief_Screen()

	SET_BIT(g_sObjectiveTextMP.otOptionsBitfield, OT_BITFLAG_DISPLAY_ON_BRIEF_SCREEN)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Set 'Display On Brief' Flag [Display Objective Text On Brief Screen].") NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set this Objective Text to not be displayed on the brief screen
PROC Clear_Objective_Text_Option_Display_On_Brief_Screen()

	CLEAR_BIT(g_sObjectiveTextMP.otOptionsBitfield, OT_BITFLAG_DISPLAY_ON_BRIEF_SCREEN)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Clear ' Display On Brief' Flag [Do Not Display Objective Text On Brief Screen].") NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this Objective Text should be displayed on the Brief screen
//
// RETURN VALUE:		BOOL			TRUE if this text should be displayed on the Brief screen, FALSE if not
FUNC BOOL Should_Objective_Text_Be_Displayed_On_Brief()
	RETURN (IS_BIT_SET(g_sObjectiveTextMP.otOptionsBitfield, OT_BITFLAG_DISPLAY_ON_BRIEF_SCREEN))
ENDFUNC




// ===========================================================================================================
//      Objective Text Display routines - clear, set, query
// ===========================================================================================================

// PURPOSE:	Check if any Objective Text has been received (whether on display or not)
//
// RETURN VALUE:		BOOL					TRUE if any Objective Text received, otherwise FALSE
FUNC BOOL Has_Any_MP_Objective_Text_Been_Received()

	IF (g_sObjectiveTextMP.otTextType = OTT_NONE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Adds all the additional components to a string ready for printing or checking if on display
PROC Add_Additional_Text_Elements_To_Main_String()

	SWITCH (g_sObjectiveTextMP.otTextType)
		CASE OTT_NONE
			EXIT
			
		CASE OTT_STRING
			// Strings have no additional components
			EXIT
			
		CASE OTT_STRING_WITH_NUMBER
			// Strings with Number have an additional INT component
			ADD_TEXT_COMPONENT_INTEGER(g_sObjectiveTextMP.otNumber1)
			EXIT
			
		CASE OTT_STRING_WITH_TWO_NUMBERS
			// Strings with Two Number have two additional INT components
			ADD_TEXT_COMPONENT_INTEGER(g_sObjectiveTextMP.otNumber1)
			ADD_TEXT_COMPONENT_INTEGER(g_sObjectiveTextMP.otNumber2)
			EXIT
			
		CASE OTT_STRING_WITH_STRING
			// Strings with String have an additional TEXT_LABEL component
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString1)
			EXIT
			
		CASE OTT_STRING_WITH_TWO_STRINGS
			// Strings with Two String have two additional TEXT_LABEL components
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString1)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString2)
			EXIT
			
		CASE OTT_STRING_WITH_PLAYER_NAME
			// Strings with Player Name have additional pre-generated PLAYER NAME string component
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString1)
			EXIT
			
		CASE OTT_STRING_WITH_COLOURED_PLAYER_NAME
			// Strings with Player Name have additional COLOUR and pre-generated PLAYER NAME string components
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString1)
			EXIT
			
		CASE OTT_STRING_WITH_TEAM_NAME
			// Strings with Team Name have additional pre-generated TEAM NAME textlabel component
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString1)
			EXIT
			
		CASE OTT_STRING_WITH_TEAM_COLOURED_TEAM_NAME
			// Strings with Team Name have additional COLOUR and pre-generated TEAM NAME textlabel components
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString1)
			EXIT
			
		CASE OTT_STRING_WITH_USER_CREATED_STRING
			// Strings with User-Created Strings have an additional 'keyboard display' component added to allow user-generated mission creater content to be output
			ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sObjectiveTextMP.otStringInString1)
			EXIT
			
		CASE OTT_STRING_WITH_TWO_USER_CREATED_STRINGS
			// Strings with User-Created Strings have an additional 'keyboard display' component added to allow user-generated mission creater content to be output
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString1)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString2)
			EXIT
			
		CASE OTT_STRING_WITH_PLAYER_NAME_AND_STRING
			// Strings with Player Name followed by a text label component
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString1)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString2)
			EXIT
			
		CASE OTT_STRING_WITH_TWO_STRINGS_COLOURED
			// Two coloured strings
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString1)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour2)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString2)
			EXIT 
			
		CASE OTT_STRING_AS_USER_CREATED_STRING
			// User-Created strings use the 'keyboard display' text command which was added especially to output user-generated mission creator content
			ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sObjectiveTextMP.otStringInString1)
			EXIT
			
		CASE OTT_STRING_WITH_COLOURED_NAME_AND_STRING
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString1)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString2)
			EXIT
			
		CASE OTT_STRING_WITH_PLAYER_NAME_AND_STRING_COLOURED
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString1)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour2)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString2)
			EXIT
			
		CASE OTT_STRING_WITH_TWO_PLAYER_NAMES_AND_STRING
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString1)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString2)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString3)
			EXIT
			
		CASE OTT_STRING_WITH_COLOURED_STRING
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString1)
			EXIT
			
		CASE OTT_STRING_WITH_COLOURED_TEXT_LABEL
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString1)
			EXIT
			
		CASE OTT_STRING_WITH_TWO_PLAYER_NAMES_AND_STRING_COLOURED
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString1)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(g_sObjectiveTextMP.otHudColour)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sObjectiveTextMP.otStringInString3)
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sObjectiveTextMP.otStringInString2)
			EXIT
			
	ENDSWITCH
	
	// Unknown Objective Text Type
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Add_Additional_Text_Elements_To_Main_String(): Unknown Objective Text Type in CASE statement - needs added.") NET_NL()
		Debug_Output_Objective_Text_To_Console_Log()
		
		SCRIPT_ASSERT("Add_Additional_Text_Elements_To_Main_String(): Ubknown Objective Text Type in CASE statement. See console log. Tell Keith.")
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Objective Text is on display
//
// RETURN VALUE:		BOOL			TRUE if on display, otherwise FALSE
FUNC BOOL Is_MP_Objective_Text_On_Display()

	// Do no more checking if no Objective Text has been received
	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF
	
	// Need to generate the full display string in order to test if it is on display
	BEGIN_TEXT_COMMAND_IS_MESSAGE_DISPLAYED(g_sObjectiveTextMP.otMainTextLabel)
		Add_Additional_Text_Elements_To_Main_String()
	RETURN END_TEXT_COMMAND_IS_MESSAGE_DISPLAYED()
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears the Objective Text if on-screen and clears the variables
PROC Remove_MP_Objective_Text_From_Being_Displayed()

	IF NOT (Is_MP_Objective_Text_On_Display())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Objective Text]: Remove_MP_Objective_Text_From_Being_Displayed() - Current Objective Text isn't on display") NET_NL()
		#ENDIF
	ENDIF
	
	IF (Has_Any_MP_Objective_Text_Been_Received())
		// Whether or not the Objective Text is on display, clear it
		BEGIN_TEXT_COMMAND_CLEAR_PRINT(g_sObjectiveTextMP.otMainTextLabel)
			Add_Additional_Text_Elements_To_Main_String()
		END_TEXT_COMMAND_CLEAR_PRINT()
		
		#IF IS_DEBUG_BUILD
			g_WIDGET_ObjectiveTextOnDisplay = FALSE
			NET_PRINT("...KGM MP [Objective Text]: Remove_MP_Objective_Text_From_Being_Displayed() - Clear This Objective Text - called whether or not the text is on display") NET_NL()
			Debug_Output_Objective_Text_To_Console_Log()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Objective Text]: Remove_MP_Objective_Text_From_Being_Displayed() - There is no current Objective Text received - so not requesting scaleform to clear anything") NET_NL()
		#ENDIF
	ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears the Objective Text if on-screen and clears the variables
PROC Delete_MP_Objective_Text()

	Remove_MP_Objective_Text_From_Being_Displayed()
	Clear_MP_Objective_Text_Variables()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Objective Text
// NOTES:	Assumes all relevant checks have been made prior to calling this function to ensure it won't overwrite existing text.
PROC Display_MP_Objective_Text()

	// The time left to display is the timeout time - current network time
	INT theDuration = GET_TIME_DIFFERENCE(g_sObjectiveTextMP.otDisplayTimeout, GET_NETWORK_TIME())
	
	// This should never happen, but ensure the timer hasn't expired, and if it has then cleanup
	IF (theDuration < 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text() Error - display time remaining is less than 0") NET_NL()
			Debug_Output_Objective_Text_To_Console_Log()
			NET_PRINT("           network Timer: ") NET_PRINT(GET_TIME_AS_STRING(GET_NETWORK_TIME())) NET_NL()
			SCRIPT_ASSERT("Display_MP_Objective_Text(): Time Remaining is less than 0 - Text should have expired. See console log. Tell Keith.")
		#ENDIF
		
		Delete_MP_Objective_Text()
		EXIT
	ENDIF

	// Do no more work if no Objective Text has been received
	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		EXIT
	ENDIF
	
	// Do an additional check if a Player Name has to be displayed
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_PLAYER_NAME)
	OR (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_COLOURED_PLAYER_NAME)
		IF NOT (IS_NET_PLAYER_OK(g_sObjectiveTextMP.otPlayerID,FALSE))
			// ...there is a problem with the PlayerID, so cleanup
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying text with Player Name, but Player is not OK - cleaning up the text") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log()
			#ENDIF
					
			Delete_MP_Objective_Text()
			EXIT
		ENDIF
	ENDIF
	
	// Need to generate the full display string in order to print it
	// KGM 18/5/13: Decide if it should be displayed on Brief Screen or not
	BOOL printNow		= TRUE
	BOOL displayOnBrief	= Should_Objective_Text_Be_Displayed_On_Brief()
	
	ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(displayOnBrief)
	BEGIN_TEXT_COMMAND_PRINT(g_sObjectiveTextMP.otMainTextLabel)
		Add_Additional_Text_Elements_To_Main_String()
	END_TEXT_COMMAND_PRINT(theDuration, printNow)
	
	Clear_Objective_Text_Option_Display_On_Brief_Screen()
	
	// Debug Output and widget setting
	#IF IS_DEBUG_BUILD
		SWITCH (g_sObjectiveTextMP.otTextType)
			CASE OTT_NONE
				EXIT
				
			CASE OTT_STRING
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying main text only") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_NUMBER
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying main text with additional number") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_TWO_NUMBERS
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying main text with two additional numbers") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_STRING
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying main text with additional text label") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_TWO_STRINGS
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying main text with two additional text labels") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_PLAYER_NAME
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying text with an additional (uncoloured) player name") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_COLOURED_PLAYER_NAME
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying text with an additional coloured player name") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_TEAM_NAME
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying text with an additional (uncoloured) team name") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_TEAM_COLOURED_TEAM_NAME
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying text with an additional team-coloured team name") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_USER_CREATED_STRING
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying text with additional user-created text") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_TWO_USER_CREATED_STRINGS
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying text with two additional user-created text") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_PLAYER_NAME_AND_STRING
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying text with player name and string") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_AS_USER_CREATED_STRING
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying user-created text") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_COLOURED_NAME_AND_STRING
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying coloured player name and string") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_TWO_STRINGS_COLOURED
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying 2 coloured strings") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_PLAYER_NAME_AND_STRING_COLOURED
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying player name and string (optional colour)") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_COLOURED_STRING
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying coloured string") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_COLOURED_TEXT_LABEL
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying coloured text label") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_TWO_PLAYER_NAMES_AND_STRING
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying two player names and string") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
				
			CASE OTT_STRING_WITH_TWO_PLAYER_NAMES_AND_STRING_COLOURED
				g_WIDGET_ObjectiveTextOnDisplay = TRUE
				NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Displaying two player names and string (all coloured)") NET_NL()
				Debug_Output_Objective_Text_To_Console_Log(theDuration)
				EXIT
		ENDSWITCH
	
		// Unknown Objective Text Type
		NET_PRINT("...KGM MP [Objective Text]: Display_MP_Objective_Text(): Unknown Objective Text Type in CASE statement - needs added.") NET_NL()
		Debug_Output_Objective_Text_To_Console_Log(theDuration)
		
		SCRIPT_ASSERT("Display_MP_Objective_Text(): Unknown Objective Text Type in CASE statement. See console log. Tell Keith.")
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If the Objective Text is on display, check if it should be hidden
//
// RETURN VALUE:		BOOL			TRUE if it should be hidden, otherwise FALSE
//
// NOTES:	Objective Text is usually only removed by another piece of God Text or dialogue - this function checks only for special conditions
FUNC BOOL Should_MP_Objective_Text_Be_Temporarily_Hidden()

	// Is the Objective Text on display?
	IF NOT (Is_MP_Objective_Text_On_Display())
		RETURN FALSE
	ENDIF
	
	// Remove it if the MP HUD is on display
	IF (IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Objective Text]: Should_MP_Objective_Text_Be_Temporarily_Hidden(): Objective Text is being removed because the MP HUD is active") NET_NL()
			Debug_Output_Objective_Text_To_Console_Log()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Remove it in an MP Cutscene
	IF (g_sObjectiveTextControlMP.otcIsPaused)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Objective Text]: Should_MP_Objective_Text_Be_Temporarily_Hidden(): Objective Text is being removed because it has been paused by ")
			NET_PRINT(g_sObjectiveTextControlMP.otcPauseScriptName)
			NET_NL()
			Debug_Output_Objective_Text_To_Console_Log()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// No reason to remove it
	RETURN FALSE

ENDFUNC





// ===========================================================================================================
//      The Main MP Objective Text Control routines
// ===========================================================================================================

// PURPOSE:	A one-off Objective Text initialisation.
PROC Initialise_MP_Objective_Text()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Initialise_MP_Objective_Text") NET_NL()
	#ENDIF

	BOOL beingInitialised = TRUE
	Clear_MP_Objective_Text_Variables(beingInitialised)
	Clear_MP_Objective_Text_Controls()
	
	#IF IS_DEBUG_BUILD
		Reset_MP_Objective_Text_Widget_Globals()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	This procedure should be called every frame to control access to the Objective Text systems.
PROC Maintain_MP_Objective_Text()

	#IF IS_DEBUG_BUILD
		Update_MP_Objective_Text_Info_Widgets()
	#ENDIF
	
	// Clear the Pause state if the script that requested the Pause is no longer running.
	IF (g_sObjectiveTextControlMP.otcIsPaused)
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sObjectiveTextControlMP.otcPauseScriptNameHash) = 0)
			NET_PRINT("...KGM MP [Objective Text]: The Script that Paused Objective Text (")
			NET_PRINT(g_sObjectiveTextControlMP.otcPauseScriptName)
			NET_PRINT(") is no longer active - unpausing Objective Text.")
			NET_NL()
		
			Clear_Objective_Text_Pause()
		ENDIF
	ENDIF
	
	// Is there any stored text?
	IF (g_sObjectiveTextMP.otTextType = OTT_NONE)
		// No stored text, so restart the timer
		g_sObjectiveTextMP.otRefreshTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), OBJECTIVE_TEXT_REFRESH_TIME_msec)
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_objectiveTextDebug")
			PRINTLN("[BUYOBJ] Maintain_MP_Objective_Text - g_sObjectiveTextMP.otTextType = OTT_NONE")
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	// If the Objective Text involves a player, remove the text if the player is no longer in the game
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_PLAYER_NAME)
	OR (g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_COLOURED_PLAYER_NAME)
		// The text involves a player name, remove the text if the player no longer exists
		IF NOT (IS_NET_PLAYER_OK(g_sObjectiveTextMP.otPlayerID, FALSE))
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Objective Text]: Player displayed as part of Objective Text is no longer in the game - deleting Objective Text.")
				NET_NL()
			#ENDIF
			
			Delete_MP_Objective_Text()
			
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_objectiveTextDebug")
				PRINTLN("[BUYOBJ] Maintain_MP_Objective_Text - IS_NET_PLAYER_OK(g_sObjectiveTextMP.otPlayerID, FALSE)")
			ENDIF
			#ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	// Check if the refresh timer has expired, EXIT if not
	IF (IS_TIME_MORE_THAN(g_sObjectiveTextMP.otRefreshTimeout, GET_NETWORK_TIME()))
	
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_objectiveTextDebug")
			PRINTLN("[BUYOBJ] Maintain_MP_Objective_Text - IS_TIME_MORE_THAN(g_sObjectiveTextMP.otRefreshTimeout, GET_NETWORK_TIME())")
		ENDIF
		#ENDIF
	
		EXIT
	ENDIF
	
	// Time for an update
	// Restart the timer ready for the next update
	g_sObjectiveTextMP.otRefreshTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), OBJECTIVE_TEXT_REFRESH_TIME_msec)
	
	// There is stored text, so check if the text display timer has expired
	IF (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sObjectiveTextMP.otDisplayTimeout))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Objective Text]: Objective Text Timer has expired - deleting Objective Text.")
			NET_NL()
		#ENDIF
		
		Delete_MP_Objective_Text()
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_objectiveTextDebug")
			PRINTLN("[BUYOBJ] Maintain_MP_Objective_Text - (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sObjectiveTextMP.otDisplayTimeout)")
		ENDIF
		#ENDIF

		EXIT
	ENDIF
	
	// The display timer is still active, so check if the requesting script is still active
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sObjectiveTextMP.otScriptNameHash) = 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Objective Text]: Objective Text requesting script (")
			NET_PRINT(g_sObjectiveTextMP.otScriptName)
			NET_PRINT(") is no longer active - deleting Objective Text.")
			NET_NL()
		#ENDIF
		
		Delete_MP_Objective_Text()
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_objectiveTextDebug")
			PRINTLN("[BUYOBJ] Maintain_MP_Objective_Text - GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_sObjectiveTextMP.otScriptNameHash) = 0")
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	// If the Objective Text involves a player name and the player no longer exists, then remove the Objective Text
	
	
	// Text still needs to be displayed
	// Is there an existing message on display?
	IF (IS_MESSAGE_BEING_DISPLAYED())
		// If the message is the Objective Text, check for reasons why it should be hidden
		IF (Should_MP_Objective_Text_Be_Temporarily_Hidden())
			Remove_MP_Objective_Text_From_Being_Displayed()
			
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_objectiveTextDebug")
				PRINTLN("[BUYOBJ] Maintain_MP_Objective_Text - Should_MP_Objective_Text_Be_Temporarily_Hidden()")
			ENDIF
			#ENDIF
			
			EXIT
		ENDIF
		
		// Check if it thinks the objective text is on display but it has been overwritten
		#IF IS_DEBUG_BUILD
			IF (g_WIDGET_ObjectiveTextOnDisplay)
				IF NOT (Is_MP_Objective_Text_On_Display())
					NET_PRINT("...KGM MP [Objective Text]: Objective Text had been displayed but has been replaced by God Text") NET_NL()
					
					IF (Has_Any_MP_Objective_Text_Been_Received())
						NET_PRINT("   Stored Objective Text Details:") NET_NL()
						Debug_Output_Objective_Text_To_Console_Log()
					ENDIF
					
					g_WIDGET_ObjectiveTextOnDisplay = FALSE
				ENDIF
			ENDIF
		#ENDIF
	
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_objectiveTextDebug")
			PRINTLN("[BUYOBJ] Maintain_MP_Objective_Text - IS_MESSAGE_BEING_DISPLAYED()")
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
		
	// Check if it thinks the objective text is on display but it has been removed
	#IF IS_DEBUG_BUILD
		IF (g_WIDGET_ObjectiveTextOnDisplay)
			IF NOT (Is_MP_Objective_Text_On_Display())
				NET_PRINT("...KGM MP [Objective Text]: Objective Text had been displayed but has now been removed") NET_NL()
				
				IF (Has_Any_MP_Objective_Text_Been_Received())
					NET_PRINT("   Stored Objective Text Details:") NET_NL()
					Debug_Output_Objective_Text_To_Console_Log()
				ENDIF
				
				g_WIDGET_ObjectiveTextOnDisplay = FALSE
			ENDIF
		ENDIF
	#ENDIF
	
	// There is no message on screen, so check if the Objective Text should be redisplayed
	// Is there an active MP phonecall to the player?
	IF (Is_There_An_Active_MP_Comms_Phonecall_To_The_Player())
		#IF IS_DEBUG_BUILD
			// Output a console log message if this has just happened
			IF NOT (g_WIDGET_ObjectiveTextBlocked_Comms)
				NET_PRINT("...KGM MP [Objective Text]: Objective Text blocked because an MP Communication Device is actively communicating.") NET_NL()
				g_WIDGET_ObjectiveTextBlocked_Comms = TRUE
			ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_objectiveTextDebug")
			PRINTLN("[BUYOBJ] Maintain_MP_Objective_Text - Is_There_An_Active_MP_Comms_Phonecall_To_The_Player()")
		ENDIF
		#ENDIF
			
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// Objective Text is not blocked because MP Comms is active
		g_WIDGET_ObjectiveTextBlocked_Comms = FALSE
	#ENDIF
	
	// Is the player in MP HUD on display?
	IF (IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD))
		#IF IS_DEBUG_BUILD
			// Output a console log message if this has just happened
			IF NOT (g_WIDGET_ObjectiveTextBlocked_MPHUD)
				NET_PRINT("...KGM MP [Objective Text]: Objective Text blocked because the MP HUD is active.") NET_NL()
				g_WIDGET_ObjectiveTextBlocked_MPHUD = TRUE
			ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_objectiveTextDebug")
			PRINTLN("[BUYOBJ] Maintain_MP_Objective_Text - IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)")
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// MP HUD is not active
		g_WIDGET_ObjectiveTextBlocked_MPHUD = FALSE
	#ENDIF
	
	// Has the Objective Text been paused?
	IF (g_sObjectiveTextControlMP.otcIsPaused)
		#IF IS_DEBUG_BUILD
			// Output a console log message if this has just happened
			IF NOT (g_WIDGET_ObjectiveTextBlocked_Paused)
				NET_PRINT("...KGM MP [Objective Text]: Objective Text blocked because it has been paused.") NET_NL()
				g_WIDGET_ObjectiveTextBlocked_Paused = TRUE
			ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_objectiveTextDebug")
			PRINTLN("[BUYOBJ] Maintain_MP_Objective_Text - g_sObjectiveTextControlMP.otcIsPaused")	
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// Objective Text has not been paused
		g_WIDGET_ObjectiveTextBlocked_Paused = FALSE
	#ENDIF
	
	// It's safe to display the Objective Text
	Display_MP_Objective_Text()

ENDPROC



// ===========================================================================================================
//      Public Access Functions
// ===========================================================================================================

// PURPOSE:	Check if this User-Created Objective Text has been received (whether on display or not)
//
// INPUT PARAMS:		paramUserString			The User-Created String
// RETURN VALUE:		BOOL					TRUE if Objective Text received, otherwise FALSE
FUNC BOOL Has_This_MP_Objective_Text_User_Created_Been_Received(STRING paramUserString)

	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramUserString))
		RETURN FALSE
	ENDIF

	RETURN (GET_HASH_KEY(paramUserString) = GET_HASH_KEY(g_sObjectiveTextMP.otStringInString1))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this Objective Text has been received (whether on display or not)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
// RETURN VALUE:		BOOL					TRUE if Objective Text received, otherwise FALSE
FUNC BOOL Has_This_MP_Objective_Text_Been_Received(STRING paramMainText)

	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF
	
	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_AS_USER_CREATED_STRING)
		RETURN (Has_This_MP_Objective_Text_User_Created_Been_Received(paramMainText))
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramMainText))
		RETURN FALSE
	ENDIF

	RETURN (GET_HASH_KEY(paramMainText) = GET_HASH_KEY(g_sObjectiveTextMP.otMainTextLabel))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this Objective Text With Number has been received (whether on display or not)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramNumber				The Number to display in the text
// RETURN VALUE:		BOOL					TRUE if Objective Text received, otherwise FALSE
FUNC BOOL Has_This_MP_Objective_Text_With_Number_Been_Received(STRING paramMainText, INT paramNumber)

	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramMainText))
		RETURN FALSE
	ENDIF

	IF NOT (GET_HASH_KEY(paramMainText) = GET_HASH_KEY(g_sObjectiveTextMP.otMainTextLabel))
		RETURN FALSE
	ENDIF
	
	RETURN (paramNumber = g_sObjectiveTextMP.otNumber1)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this Objective Text With Two Numbers has been received (whether on display or not)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramNumber				The Number to display in the text
//						paramNumber2			The Second Number to display in the text
// RETURN VALUE:		BOOL					TRUE if Objective Text received, otherwise FALSE
FUNC BOOL Has_This_MP_Objective_Text_With_Two_Numbers_Been_Received(STRING paramMainText, INT paramNumber, INT paramNumber2)

	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramMainText))
		RETURN FALSE
	ENDIF

	IF NOT (GET_HASH_KEY(paramMainText) = GET_HASH_KEY(g_sObjectiveTextMP.otMainTextLabel))
		RETURN FALSE
	ENDIF
	
	IF NOT (paramNumber = g_sObjectiveTextMP.otNumber1)
		RETURN FALSE
	ENDIF
	
	RETURN (paramNumber2 = g_sObjectiveTextMP.otNumber2)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this Objective Text With String has been received (whether on display or not)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramString				The text Label for the String to display in the text
// RETURN VALUE:		BOOL					TRUE if Objective Text received, otherwise FALSE
FUNC BOOL Has_This_MP_Objective_Text_With_String_Been_Received(STRING paramMainText, STRING paramString)

	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramMainText))
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramString))
		RETURN FALSE
	ENDIF

	IF NOT (GET_HASH_KEY(paramMainText) = GET_HASH_KEY(g_sObjectiveTextMP.otMainTextLabel))
		RETURN FALSE
	ENDIF
	
	RETURN (GET_HASH_KEY(paramString) = GET_HASH_KEY(g_sObjectiveTextMP.otStringInString1))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this Objective Text With Two Strings has been received (whether on display or not)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramString				The text Label for the first String to display in the text
//						paramString2			The text Label for the second String to display in the text
// RETURN VALUE:		BOOL					TRUE if Objective Text received, otherwise FALSE
FUNC BOOL Has_This_MP_Objective_Text_With_Two_Strings_Been_Received(STRING paramMainText, STRING paramString, STRING paramString2)

	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramMainText))
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramString))
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramString2))
		RETURN FALSE
	ENDIF

	IF NOT (GET_HASH_KEY(paramMainText) = GET_HASH_KEY(g_sObjectiveTextMP.otMainTextLabel))
		RETURN FALSE
	ENDIF

	IF NOT (GET_HASH_KEY(paramString) = GET_HASH_KEY(g_sObjectiveTextMP.otStringInString1))
		RETURN FALSE
	ENDIF
	
	RETURN (GET_HASH_KEY(paramString2) = GET_HASH_KEY(g_sObjectiveTextMP.otStringInString2))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this Objective Text With Team ID has been received (whether on display or not)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramTeamID				The TeamID for the team name to be displayed within the string
// RETURN VALUE:		BOOL					TRUE if Objective Text received, otherwise FALSE
FUNC BOOL Has_This_MP_Objective_Text_With_Team_Name_Been_Received(STRING paramMainText, INT paramTeamID)

	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramMainText))
		RETURN FALSE
	ENDIF

	IF NOT (GET_HASH_KEY(paramMainText) = GET_HASH_KEY(g_sObjectiveTextMP.otMainTextLabel))
		RETURN FALSE
	ENDIF
	
	RETURN (paramTeamID = g_sObjectiveTextMP.otTeamID)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this Objective Text With User-Created String has been received (whether on display or not)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramUserString			The user created text to display in the main text
// RETURN VALUE:		BOOL					TRUE if Objective Text received, otherwise FALSE
FUNC BOOL Has_This_MP_Objective_Text_With_User_Created_String_Been_Received(STRING paramMainText, STRING paramUserString)

	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramMainText))
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramUserString))
		RETURN FALSE
	ENDIF

	IF NOT (GET_HASH_KEY(paramMainText) = GET_HASH_KEY(g_sObjectiveTextMP.otMainTextLabel))
		RETURN FALSE
	ENDIF
	
	RETURN (GET_HASH_KEY(paramUserString) = GET_HASH_KEY(g_sObjectiveTextMP.otStringInString1))
	
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this Objective Text With User-Created String has been received (whether on display or not)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramUserString			The user created text to display in the main text
// RETURN VALUE:		BOOL					TRUE if Objective Text received, otherwise FALSE
FUNC BOOL Has_This_MP_Objective_Text_With_Two_User_Created_Strings_Been_Received(STRING paramMainText, STRING paramUserString1, STRING paramUserString2)

	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramMainText))
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramUserString1))
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramUserString2))
		RETURN FALSE
	ENDIF

	IF NOT (GET_HASH_KEY(paramMainText) = GET_HASH_KEY(g_sObjectiveTextMP.otMainTextLabel))
		RETURN FALSE
	ENDIF
	
	IF NOT (GET_HASH_KEY(paramUserString1) = GET_HASH_KEY(g_sObjectiveTextMP.otStringInString1))
		RETURN FALSE
	ENDIF
	
	RETURN (GET_HASH_KEY(paramUserString2) = GET_HASH_KEY(g_sObjectiveTextMP.otStringInString2))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this Objective Text With Player Name has been received (whether on display or not)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramPlayerID			The PlayerID for the player name to be displayed within the string
// RETURN VALUE:		BOOL					TRUE if Objective Text received, otherwise FALSE
//
// NOTES:	KGM 23/6/15: This was added for CnC and the player names are team-coloured. Now re-directing to a different function to allow any coloured player names.
FUNC BOOL Has_This_MP_Objective_Text_With_Player_Name_Been_Received(STRING paramMainText, PLAYER_INDEX paramPlayerID)

	RETURN (Has_This_MP_Objective_Text_With_User_Created_String_Been_Received(paramMainText, GET_PLAYER_NAME(paramPlayerID)))

//	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
//		RETURN FALSE
//	ENDIF
//	
//	IF (IS_STRING_NULL_OR_EMPTY(paramMainText))
//		RETURN FALSE
//	ENDIF
//	
//	IF NOT (IS_NET_PLAYER_OK(paramPlayerID))
//		RETURN FALSE
//	ENDIF
//
//	IF NOT (GET_HASH_KEY(paramMainText) = GET_HASH_KEY(g_sObjectiveTextMP.otMainTextLabel))
//		RETURN FALSE
//	ENDIF
//	
//	RETURN (paramPlayerID = g_sObjectiveTextMP.otPlayerID)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text(STRING paramMainText, BOOL paramOnWhileHandsTied = FALSE)
	
	//#IF IS_DEBUG_BUILD
	//PRINTLN("Print_Objective_Text(): being called:")
	//DEBUG_PRINTCALLSTACK()
	//#ENDIF
	
	// Error Checking
	IF (Is_String_Null_Or_Empty(paramMainText))
		SCRIPT_ASSERT("Print_Objective_Text(): The Main Text Label string is NULL or empty")
		EXIT
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text(): The Main Text Label string is longer than expected. Tell Keith.")
		EXIT
	ENDIF
	
	// Do nothing if already received
	IF (Has_This_MP_Objective_Text_Been_Received(paramMainText))
		EXIT
	ENDIF
	
	// Display the initial piece of debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Request to Print_Objective_Text() - details received") NET_NL()
	#ENDIF
	
	// This will replace any existing Objective Text
	Delete_MP_Objective_Text()
	
	// Store the relevant details
	g_sObjectiveTextMP.otTextType			= OTT_STRING
	g_sObjectiveTextMP.otScriptName			= GET_THIS_SCRIPT_NAME()
	g_sObjectiveTextMP.otScriptNameHash		= GET_HASH_KEY(g_sObjectiveTextMP.otScriptName)
	g_sObjectiveTextMP.otMainTextLabel		= paramMainText
	Calculate_And_Store_MP_Objective_Text_Timers()
	
	// Store any optional parameters
	Set_Objective_Text_Option_Display_While_Hands_Tied(paramOnWhileHandsTied)
	Set_Objective_Text_Option_Display_On_Brief_Screen()
	
	// Display the details
	#IF IS_DEBUG_BUILD
		g_WIDGET_ObjectiveTextReceived = TRUE
		NET_PRINT("   Objective Text Newly Stored Details") NET_NL()
		ADD_SCRIPT_METRIC_MARKER(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otMainTextLabel))
		Debug_Output_Objective_Text_To_Console_Log()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with a Number
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramNumber				The Number to display in the text
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text_With_Number(STRING paramMainText, INT paramNumber, BOOL paramOnWhileHandsTied = FALSE)

	// Error Checking
	IF (Is_String_Null_Or_Empty(paramMainText))
		SCRIPT_ASSERT("Print_Objective_Text_With_Number(): The Main Text Label string is NULL or empty")
		EXIT
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_With_Number(): The Main Text Label string is longer than expected. Tell Keith.")
		EXIT
	ENDIF
	
	// Do nothing if already received
	IF (Has_This_MP_Objective_Text_With_Number_Been_Received(paramMainText, paramNumber))
		EXIT
	ENDIF
	
	// Display the initial piece of debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Request to Print_Objective_Text_With_Number() - details received") NET_NL()
	#ENDIF
	
	// This will replace any existing Objective Text
	Delete_MP_Objective_Text()
	
	// Store the relevant details
	g_sObjectiveTextMP.otTextType			= OTT_STRING_WITH_NUMBER
	g_sObjectiveTextMP.otScriptName			= GET_THIS_SCRIPT_NAME()
	g_sObjectiveTextMP.otScriptNameHash		= GET_HASH_KEY(g_sObjectiveTextMP.otScriptName)
	g_sObjectiveTextMP.otMainTextLabel		= paramMainText
	g_sObjectiveTextMP.otNumber1			= paramNumber
	Calculate_And_Store_MP_Objective_Text_Timers()
	
	// Store any optional parameters
	Set_Objective_Text_Option_Display_While_Hands_Tied(paramOnWhileHandsTied)
	Set_Objective_Text_Option_Display_On_Brief_Screen()
	
	// Display the details
	#IF IS_DEBUG_BUILD
		g_WIDGET_ObjectiveTextReceived = TRUE
		NET_PRINT("   Objective Text Newly Stored Details") NET_NL()
		ADD_SCRIPT_METRIC_MARKER(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otMainTextLabel))
		Debug_Output_Objective_Text_To_Console_Log()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with two Numbers
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramNumber				The Number to display in the text
//						paramNumber2			The Second Number to display in the text
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text_With_Two_Numbers(STRING paramMainText, INT paramNumber, INT paramNumber2, BOOL paramOnWhileHandsTied = FALSE)

	// Error Checking
	IF (Is_String_Null_Or_Empty(paramMainText))
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_Numbers(): The Main Text Label string is NULL or empty")
		EXIT
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_Numbers(): The Main Text Label string is longer than expected. Tell Keith.")
		EXIT
	ENDIF
	
	// Do nothing if already received
	IF (Has_This_MP_Objective_Text_With_Two_Numbers_Been_Received(paramMainText, paramNumber, paramNumber2))
		EXIT
	ENDIF
	
	// Display the initial piece of debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Request to Print_Objective_Text_With_Two_Numbers() - details received") NET_NL()
	#ENDIF
	
	// This will replace any existing Objective Text
	Delete_MP_Objective_Text()
	
	// Store the relevant details
	g_sObjectiveTextMP.otTextType			= OTT_STRING_WITH_TWO_NUMBERS
	g_sObjectiveTextMP.otScriptName			= GET_THIS_SCRIPT_NAME()
	g_sObjectiveTextMP.otScriptNameHash		= GET_HASH_KEY(g_sObjectiveTextMP.otScriptName)
	g_sObjectiveTextMP.otMainTextLabel		= paramMainText
	g_sObjectiveTextMP.otNumber1			= paramNumber
	g_sObjectiveTextMP.otNumber2			= paramNumber2
	Calculate_And_Store_MP_Objective_Text_Timers()
	
	// Store any optional parameters
	Set_Objective_Text_Option_Display_While_Hands_Tied(paramOnWhileHandsTied)
	Set_Objective_Text_Option_Display_On_Brief_Screen()
	
	// Display the details
	#IF IS_DEBUG_BUILD
		g_WIDGET_ObjectiveTextReceived = TRUE
		NET_PRINT("   Objective Text Newly Stored Details") NET_NL()
		ADD_SCRIPT_METRIC_MARKER(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otMainTextLabel))
		Debug_Output_Objective_Text_To_Console_Log()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with a String
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramString				The text Label for the String to display in the text
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
FUNC BOOL Print_Objective_Text_With_String(STRING paramMainText, STRING paramString, BOOL paramOnWhileHandsTied = FALSE, HUD_COLOURS hcColourOverride = HUD_COLOUR_PURE_WHITE)

	// Error Checking
	IF (Is_String_Null_Or_Empty(paramMainText))
		PRINTLN("Print_Objective_Text_With_String(): The Main Text Label string is NULL or empty")
		SCRIPT_ASSERT("Print_Objective_Text_With_String(): The Main Text Label string is NULL or empty")
		RETURN FALSE
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
		PRINTLN("Print_Objective_Text_With_String(): The Main Text Label string is longer than expected. Tell Keith.")
		SCRIPT_ASSERT("Print_Objective_Text_With_String(): The Main Text Label string is longer than expected. Tell Keith.")
		RETURN FALSE
	ENDIF

	// Error Checking - additional string
	IF (Is_String_Null_Or_Empty(paramString))
		PRINTLN("Print_Objective_Text_With_String(): The Additional Text Label string is NULL or empty")
		SCRIPT_ASSERT("Print_Objective_Text_With_String(): The Additional Text Label string is NULL or empty")
		RETURN FALSE
	ENDIF

//	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramString) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
//		PRINTLN("Print_Objective_Text_With_String(): The Additional Text Label string is longer than expected. Tell Keith.")
//		SCRIPT_ASSERT("Print_Objective_Text_With_String(): The Additional Text Label string is longer than expected. Tell Keith.")
//		RETURN FALSE
//	ENDIF
	
	// Do nothing if already received
	IF (Has_This_MP_Objective_Text_With_String_Been_Received(paramMainText, paramString))
	AND g_sObjectiveTextMP.otHudColour = g_sObjectiveTextMP.otHudColourStored
		RETURN FALSE
	ENDIF
	
	// Display the initial piece of debug output
	#IF IS_DEBUG_BUILD
		PRINTLN("...KGM MP [Objective Text]: Request to Print_Objective_Text_With_String() - details received") NET_NL()
	#ENDIF
	
	// This will replace any existing Objective Text
	Delete_MP_Objective_Text()
	
	// Store the relevant details
	g_sObjectiveTextMP.otTextType			= OTT_STRING_WITH_STRING
	g_sObjectiveTextMP.otScriptName			= GET_THIS_SCRIPT_NAME()
	g_sObjectiveTextMP.otScriptNameHash		= GET_HASH_KEY(g_sObjectiveTextMP.otScriptName)
	g_sObjectiveTextMP.otMainTextLabel		= paramMainText
	g_sObjectiveTextMP.otStringInString1	= paramString
	g_sObjectiveTextMP.otHudColourStored	= hcColourOverride
	g_sObjectiveTextMP.otHudColour 			= hcColourOverride
	Calculate_And_Store_MP_Objective_Text_Timers()
	
	// Store any optional parameters
	Set_Objective_Text_Option_Display_While_Hands_Tied(paramOnWhileHandsTied)
	Set_Objective_Text_Option_Display_On_Brief_Screen()
	
	// Display the details
	#IF IS_DEBUG_BUILD
		g_WIDGET_ObjectiveTextReceived = TRUE
		NET_PRINT("   Objective Text Newly Stored Details") NET_NL()
		ADD_SCRIPT_METRIC_MARKER(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otMainTextLabel))
		Debug_Output_Objective_Text_To_Console_Log()
	#ENDIF
	RETURN TRUE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with two Strings
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramString				The text Label for the String to display in the text
//						paramString2			The text Label for the Second String to display in the text
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
FUNC BOOL Print_Objective_Text_With_Two_String(STRING paramMainText, STRING paramString, STRING paramString2, BOOL paramOnWhileHandsTied = FALSE)

	// Error Checking
	IF (Is_String_Null_Or_Empty(paramMainText))
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_String(): The Main Text Label string is NULL or empty")
		RETURN FALSE
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_String(): The Main Text Label string is longer than expected. Tell Keith.")
		RETURN FALSE
	ENDIF

	// Error Checking - additional string
	IF (Is_String_Null_Or_Empty(paramString))
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_String(): The Additional Text Label string is NULL or empty")
		RETURN FALSE
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramString) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_String(): The Additional Text Label string is longer than expected. Tell Keith.")
		RETURN FALSE
	ENDIF

	// Error Checking - second additional string
	IF (Is_String_Null_Or_Empty(paramString2))
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_String(): The Second Additional Text Label string is NULL or empty")
		RETURN FALSE
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramString2) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_String(): The Second Additional Text Label string is longer than expected. Tell Keith.")
		RETURN FALSE
	ENDIF
	
	// Do nothing if already received
	IF (Has_This_MP_Objective_Text_With_Two_Strings_Been_Received(paramMainText, paramString, paramString2))
		RETURN FALSE
	ENDIF
	
	// Display the initial piece of debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Request to Print_Objective_Text_With_Two_String() - details received") NET_NL()
	#ENDIF
	
	// This will replace any existing Objective Text
	Delete_MP_Objective_Text()
	
	// Store the relevant details
	g_sObjectiveTextMP.otTextType			= OTT_STRING_WITH_TWO_STRINGS
	g_sObjectiveTextMP.otScriptName			= GET_THIS_SCRIPT_NAME()
	g_sObjectiveTextMP.otScriptNameHash		= GET_HASH_KEY(g_sObjectiveTextMP.otScriptName)
	g_sObjectiveTextMP.otMainTextLabel		= paramMainText
	g_sObjectiveTextMP.otStringInString1	= paramString
	g_sObjectiveTextMP.otStringInString2	= paramString2
	Calculate_And_Store_MP_Objective_Text_Timers()
	
	// Store any optional parameters
	Set_Objective_Text_Option_Display_While_Hands_Tied(paramOnWhileHandsTied)
	Set_Objective_Text_Option_Display_On_Brief_Screen()
	
	// Display the details
	#IF IS_DEBUG_BUILD
		g_WIDGET_ObjectiveTextReceived = TRUE
		NET_PRINT("   Objective Text Newly Stored Details") NET_NL()
		ADD_SCRIPT_METRIC_MARKER(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otMainTextLabel))
		Debug_Output_Objective_Text_To_Console_Log()
	#ENDIF
	RETURN TRUE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with a Team Name (optionally team-coloured)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramTeamID				The TeamID for the team name to be displayed within the string
//						paramUseTeamColour		[DEFAULT = TRUE] Colour the team name using the team colour
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
FUNC BOOL Print_Objective_Text_With_Team_Name(STRING paramMainText, INT paramTeamID, BOOL paramUseTeamColour = TRUE, BOOL paramOnWhileHandsTied = FALSE)

	// Error Checking
	IF (Is_String_Null_Or_Empty(paramMainText))
		SCRIPT_ASSERT("Print_Objective_Text_With_Team_Name(): The Main Text Label string is NULL or empty")
		RETURN FALSE
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_With_Team_Name(): The Main Text Label string is longer than expected. Tell Keith.")
		RETURN FALSE
	ENDIF
	
	// Do nothing if already received
	IF (Has_This_MP_Objective_Text_With_Team_Name_Been_Received(paramMainText, paramTeamID))
		RETURN FALSE
	ENDIF
	
	// Error Checking - teamID
	IF (paramTeamID < 0)
	//OR (paramTeamID >= MAX_NUM_TEAMS)
		SCRIPT_ASSERT("Print_Objective_Text_With_Team_Name(): The TeamID is illegal. Add a bug for the Mission Scripter.")
		RETURN FALSE
	ENDIF
	
	// Display the initial piece of debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Request to Print_Objective_Text_With_Team_Name() - details received") NET_NL()
	#ENDIF
	
	// This will replace any existing Objective Text
	Delete_MP_Objective_Text()
	
	// Store the relevant details
	IF (paramUseTeamColour)
		g_sObjectiveTextMP.otTextType		= OTT_STRING_WITH_TEAM_COLOURED_TEAM_NAME
	ELSE
		g_sObjectiveTextMP.otTextType		= OTT_STRING_WITH_TEAM_NAME
	ENDIF
	
	g_sObjectiveTextMP.otScriptName			= GET_THIS_SCRIPT_NAME()
	g_sObjectiveTextMP.otScriptNameHash		= GET_HASH_KEY(g_sObjectiveTextMP.otScriptName)
	g_sObjectiveTextMP.otMainTextLabel		= paramMainText
	g_sObjectiveTextMP.otStringInString1	= GET_TEAM_NAME_KEY(paramTeamID)
	g_sObjectiveTextMP.otTeamID				= paramTeamID
	
	// Store the HUD Colour
	IF (paramUseTeamColour)
		g_sObjectiveTextMP.otHudColour		= GET_TEAM_HUD_COLOUR(g_sObjectiveTextMP.otTeamID)
	ENDIF
	
	Calculate_And_Store_MP_Objective_Text_Timers()
	
	// Store any optional parameters
	Set_Objective_Text_Option_Display_While_Hands_Tied(paramOnWhileHandsTied)
	Set_Objective_Text_Option_Display_On_Brief_Screen()
	
	// Display the details
	#IF IS_DEBUG_BUILD
		g_WIDGET_ObjectiveTextReceived = TRUE
		NET_PRINT("   Objective Text Newly Stored Details") NET_NL()
		ADD_SCRIPT_METRIC_MARKER(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otMainTextLabel))
		Debug_Output_Objective_Text_To_Console_Log()
	#ENDIF

	RETURN TRUE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with a User-Created String
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramUserString			The user created text to display in the main text
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
FUNC BOOL Print_Objective_Text_With_User_Created_String(STRING paramMainText, STRING paramUserString, BOOL paramOnWhileHandsTied = FALSE, HUD_COLOURS hcColourOverride = HUD_COLOUR_WHITE)

	// Error Checking
	IF (Is_String_Null_Or_Empty(paramMainText))
		SCRIPT_ASSERT("Print_Objective_Text_With_User_Created_String(): The Main Text Label string is NULL or empty")
		RETURN FALSE
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_With_User_Created_String(): The Main Text Label string is longer than expected. Tell Keith.")
		RETURN FALSE
	ENDIF

	// Error Checking - additional string
	IF (Is_String_Null_Or_Empty(paramUserString))
		SCRIPT_ASSERT("Print_Objective_Text_With_User_Created_String(): The Additional User Created Text is NULL or empty")
		RETURN FALSE
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramUserString) > MAX_LENGTH_MP_USER_CREATED_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_With_User_Created_String(): The Additional User Created Text Label is longer than expected. Tell Keith.")
		RETURN FALSE
	ENDIF
	
	// Do nothing if already received
	IF (Has_This_MP_Objective_Text_With_User_Created_String_Been_Received(paramMainText, paramUserString))
	AND g_sObjectiveTextMP.otHudColour = g_sObjectiveTextMP.otHudColourStored
		RETURN FALSE
	ENDIF
	
	// Display the initial piece of debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Request to Print_Objective_Text_With_User_Created_String() - details received") NET_NL()
	#ENDIF
	
	//Added to fix url:bugstar:3230805
	PLAYER_INDEX tempPlayerID = g_sObjectiveTextMP.otPlayerID
	
	// This will replace any existing Objective Text
	Delete_MP_Objective_Text()
	
	// Store the relevant details
	g_sObjectiveTextMP.otTextType			= OTT_STRING_WITH_USER_CREATED_STRING
	g_sObjectiveTextMP.otScriptName			= GET_THIS_SCRIPT_NAME()
	g_sObjectiveTextMP.otScriptNameHash		= GET_HASH_KEY(g_sObjectiveTextMP.otScriptName)
	g_sObjectiveTextMP.otMainTextLabel		= paramMainText
	g_sObjectiveTextMP.otStringInString1	= paramUserString
	g_sObjectiveTextMP.otHudColourStored	= hcColourOverride
	g_sObjectiveTextMP.otHudColour			= hcColourOverride
	g_sObjectiveTextMP.otPlayerID			= tempPlayerID
	Calculate_And_Store_MP_Objective_Text_Timers()
	
	// Store any optional parameters
	Set_Objective_Text_Option_Display_While_Hands_Tied(paramOnWhileHandsTied)
	Set_Objective_Text_Option_Display_On_Brief_Screen()
	
	// Display the details
	#IF IS_DEBUG_BUILD
		g_WIDGET_ObjectiveTextReceived = TRUE
		NET_PRINT("   Objective Text Newly Stored Details") NET_NL()
		ADD_SCRIPT_METRIC_MARKER(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otMainTextLabel))
		Debug_Output_Objective_Text_To_Console_Log()
	#ENDIF

	RETURN TRUE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with a User-Created String
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramUserString			The user created text to display in the main text
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
FUNC BOOL Print_Objective_Text_With_Two_User_Created_Strings(STRING paramMainText, STRING paramUserString1, STRING paramUserString2, BOOL paramOnWhileHandsTied = FALSE, HUD_COLOURS hcColourOverride = HUD_COLOUR_WHITE)

	// Error Checking
	IF (Is_String_Null_Or_Empty(paramMainText))
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_User_Created_Strings(): The Main Text Label string is NULL or empty")
		RETURN FALSE
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_User_Created_Strings(): The Main Text Label string is longer than expected. Tell Keith.")
		RETURN FALSE
	ENDIF

	// Error Checking - additional string
	IF (Is_String_Null_Or_Empty(paramUserString1))
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_User_Created_Strings(): The First Additional User Created Text is NULL or empty")
		RETURN FALSE
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramUserString1) > MAX_LENGTH_MP_USER_CREATED_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_User_Created_Strings(): The First Additional User Created Text Label is longer than expected. Tell Keith.")
		RETURN FALSE
	ENDIF
	
	// Error Checking - additional string
	IF (Is_String_Null_Or_Empty(paramUserString2))
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_User_Created_Strings(): The Second Additional User Created Text is NULL or empty")
		RETURN FALSE
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramUserString2) > MAX_LENGTH_MP_USER_CREATED_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_With_Two_User_Created_Strings(): The Second Additional User Created Text Label is longer than expected. Tell Keith.")
		RETURN FALSE
	ENDIF
	
	// Do nothing if already received
	IF (Has_This_MP_Objective_Text_With_Two_User_Created_Strings_Been_Received(paramMainText, paramUserString1, paramUserString2))
	AND g_sObjectiveTextMP.otHudColour = g_sObjectiveTextMP.otHudColourStored
		RETURN FALSE
	ENDIF
	
	// Display the initial piece of debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Request to Print_Objective_Text_With_Two_User_Created_Strings() - details received") NET_NL()
	#ENDIF
	
	// This will replace any existing Objective Text
	Delete_MP_Objective_Text()
	
	// Store the relevant details
	g_sObjectiveTextMP.otTextType			= OTT_STRING_WITH_TWO_USER_CREATED_STRINGS
	g_sObjectiveTextMP.otScriptName			= GET_THIS_SCRIPT_NAME()
	g_sObjectiveTextMP.otScriptNameHash		= GET_HASH_KEY(g_sObjectiveTextMP.otScriptName)
	g_sObjectiveTextMP.otMainTextLabel		= paramMainText
	g_sObjectiveTextMP.otStringInString1	= paramUserString1
	g_sObjectiveTextMP.otStringInString2	= paramUserString2
	g_sObjectiveTextMP.otHudColourStored	= hcColourOverride
	g_sObjectiveTextMP.otHudColour 			= hcColourOverride
	Calculate_And_Store_MP_Objective_Text_Timers()
	
	// Store any optional parameters
	Set_Objective_Text_Option_Display_While_Hands_Tied(paramOnWhileHandsTied)
	Set_Objective_Text_Option_Display_On_Brief_Screen()
	
	// Display the details
	#IF IS_DEBUG_BUILD
		g_WIDGET_ObjectiveTextReceived = TRUE
		NET_PRINT("   Objective Text Newly Stored Details") NET_NL()
		ADD_SCRIPT_METRIC_MARKER(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otMainTextLabel))
		Debug_Output_Objective_Text_To_Console_Log()
	#ENDIF
	RETURN TRUE
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with a Player Name Literal String and a text label
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramPlayerID			The PlayerID for the player name to be displayed within the string
//						paramString1			The text label to display alongside the player name
PROC Print_Objective_Text_With_Player_Name_And_String(STRING paramMainText, PLAYER_INDEX paramPlayerID, STRING paramString1, BOOL paramOnWhileHandsTied = FALSE)

	PRINTLN(".KGM [Objective Text]: Print_Objective_Text_With_Player_Name_And_String() - REDIRECTED TO Print_Objective_Text_With_User_Created_String()")
	
	IF Print_Objective_Text_With_Two_User_Created_Strings(paramMainText, GET_PLAYER_NAME(paramPlayerID), paramString1, paramOnWhileHandsTied)
		g_sObjectiveTextMP.otPlayerID = paramPlayerID
		g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_PLAYER_NAME_AND_STRING
	ENDIF
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with a Player Name Literal String (optionally team-coloured)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramPlayerID			The PlayerID for the player name to be displayed within the string
//						paramUseColour			[DEFAULT = TRUE] Colour the player name using the team colour if available, or the player colour if not in a team
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
//
// NOTES:	KGM 20/8/11: The optional 'use team colour' parameter was added so the the player name would display in red for 'enemies' for assassination.
// NOTES:	KGM 23/6/15: This was added for CnC and the player names are team-coloured. Now re0directing to a different function to allow any coloured player names.
PROC Print_Objective_Text_With_Player_Name(STRING paramMainText, PLAYER_INDEX paramPlayerID, BOOL paramUseColour = TRUE, BOOL paramOnWhileHandsTied = FALSE)

	PRINTLN(".KGM [Objective Text]: Print_Objective_Text_With_Player_Name() - REDIRECTED TO Print_Objective_Text_With_User_Created_String()")
	
	Print_Objective_Text_With_User_Created_String(paramMainText, GET_PLAYER_NAME(paramPlayerID), paramOnWhileHandsTied)
	
	UNUSED_PARAMETER(paramUseColour)

//	// Error Checking
//	IF (Is_String_Null_Or_Empty(paramMainText))
//		SCRIPT_ASSERT("Print_Objective_Text_With_Player_Name(): The Main Text Label string is NULL or empty")
//		EXIT
//	ENDIF
//
//	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
//		SCRIPT_ASSERT("Print_Objective_Text_With_Player_Name(): The Main Text Label string is longer than expected. Tell Keith.")
//		EXIT
//	ENDIF
//	
//	// Error Checking - player index
//	IF NOT (IS_NET_PLAYER_OK(paramPlayerID))
//		SCRIPT_ASSERT("Print_Objective_Text_With_Player_Name(): The PlayerID is NOT OK. Add a bug for the Mission Scripter.")
//		EXIT
//	ENDIF
//	
//	// Do nothing if already received
//	IF (Has_This_MP_Objective_Text_With_Player_Name_Been_Received(paramMainText, paramPlayerID))
//		EXIT
//	ENDIF
//	
//	// Display the initial piece of debug output
//	#IF IS_DEBUG_BUILD
//		NET_PRINT("...KGM MP [Objective Text]: Request to Print_Objective_Text_With_Player_Name() - details received") NET_NL()
//	#ENDIF
//	
//	// This will replace any existing Objective Text
//	Delete_MP_Objective_Text()
//	
//	// Store the relevant details
//	IF (paramUseColour)
//		g_sObjectiveTextMP.otTextType		= OTT_STRING_WITH_COLOURED_PLAYER_NAME
//	ELSE
//		g_sObjectiveTextMP.otTextType		= OTT_STRING_WITH_PLAYER_NAME
//	ENDIF
//	
//	g_sObjectiveTextMP.otScriptName			= GET_THIS_SCRIPT_NAME()
//	g_sObjectiveTextMP.otScriptNameHash		= GET_HASH_KEY(g_sObjectiveTextMP.otScriptName)
//	g_sObjectiveTextMP.otMainTextLabel		= paramMainText
//	g_sObjectiveTextMP.otPlayerID			= paramPlayerID
//	g_sObjectiveTextMP.otStringInString1	= GET_PLAYER_NAME(paramPlayerID)
//	g_sObjectiveTextMP.otTeamID				= GET_PLAYER_TEAM(paramPlayerID)
//	
//	// Store the HUD Colour
//	IF (paramUseColour)
//		IF (g_sObjectiveTextMP.otTeamID = TEAM_INVALID)
//			g_sObjectiveTextMP.otHudColour	= GET_PLAYER_HUD_COLOUR(g_sObjectiveTextMP.otPlayerID, DEFAULT, TRUE)
//		ELSE
//			g_sObjectiveTextMP.otHudColour	= GET_TEAM_HUD_COLOUR(g_sObjectiveTextMP.otTeamID)
//		ENDIF
//	ENDIF
//	
//	Calculate_And_Store_MP_Objective_Text_Timers()
//	
//	// Store any optional parameters
//	Set_Objective_Text_Option_Display_While_Hands_Tied(paramOnWhileHandsTied)
//	Set_Objective_Text_Option_Display_On_Brief_Screen()
//	
//	// Display the details
//	#IF IS_DEBUG_BUILD
//		g_WIDGET_ObjectiveTextReceived = TRUE
//		NET_PRINT("   Objective Text Newly Stored Details") NET_NL()
//		ADD_SCRIPT_METRIC_MARKER(GET_STRING_FROM_TEXT_FILE(g_sObjectiveTextMP.otMainTextLabel))
//		Debug_Output_Objective_Text_To_Console_Log()
//	#ENDIF

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with a two user substrings (TL63) with an additional Substring (TL15) passed in as the second param.
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramUserString1		Additional text label to be printed, can be a name, not coloured
//						paramStringTL15			Additional string to insert into main text (TL15)
//						paramUserString2		Additional text label to be printed, can be a name, not coloured
//						hcColourOverride		The colour you wish to render the TL15 in.
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text_With_Two_User_Created_Strings_And_String_Coloured(STRING paramMainText, STRING paramUserString1, STRING paramStringTL15, STRING paramUserString2, HUD_COLOURS hcColourOverride = HUD_COLOUR_WHITE, BOOL paramOnWhileHandsTied = FALSE)
	PRINTLN(".KGM [Objective Text]: Print_Objective_Text_With_Two_User_Created_Strings_And_String_Coloured() - hcColourOverride = ", ENUM_TO_INT(hcColourOverride), ", TL15 string: ", paramStringTL15)
	
	// Verify the 2 TL63s, no need to verify the TL15 as it'll break either way.
	IF Print_Objective_Text_With_Two_User_Created_Strings(paramMainText, paramUserString1, paramUserString2, paramOnWhileHandsTied, hcColourOverride)
		g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TWO_PLAYER_NAMES_AND_STRING_COLOURED
		g_sObjectiveTextMP.otStringInString3 = paramStringTL15
		g_sObjectiveTextMP.otHudColour = hcColourOverride
	ENDIF
ENDPROC

// PURPOSE:	Display one piece of Objective Text with a Player Name Literal String that is coloured with the HUD_COLOURS you pass through
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramPlayerID			The PlayerID for the player name to be displayed within the string
//						paramAdditionalText		Additional text label to be printed, coloured the same as name.
//						hcColourOverride		The colour you wish to render the player's name in.
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text_With_Coloured_Player_Name_And_String(STRING paramMainText, PLAYER_INDEX paramPlayerID, STRING paramAdditionalText, HUD_COLOURS hcColourOverride, BOOL paramOnWhileHandsTied = FALSE)
	PRINTLN(".KGM [Objective Text]: Print_Objective_Text_With_Coloured_Player_Name_And_String() - REDIRECTED TO Print_Objective_Text_With_User_Created_String(). hcColourOverride = ", ENUM_TO_INT(hcColourOverride))
	
	IF Print_Objective_Text_With_Two_User_Created_Strings(paramMainText, GET_PLAYER_NAME(paramPlayerID), paramAdditionalText, paramOnWhileHandsTied, hcColourOverride)
		g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_COLOURED_NAME_AND_STRING
		g_sObjectiveTextMP.otHudColour = hcColourOverride
		g_sObjectiveTextMP.otPlayerID = paramPlayerID
	ENDIF
ENDPROC

// PURPOSE:	Display one piece of Objective Text with a Player Name Literal String that is coloured with the HUD_COLOURS you pass through
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramPlayerID			The PlayerID for the player name to be displayed within the string
//						paramAdditionalText		Additional text label to be printed, coloured the same as name.
//						hcPlayerNameOverride	The colour you wish to render the player's name in.
//						hcStringOverride		The colour you wish to render the substring in
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text_With_Player_Name_And_String_Coloured(STRING paramMainText, PLAYER_INDEX paramPlayerID, STRING paramAdditionalText, HUD_COLOURS hcPlayerNameOverride = HUD_COLOUR_WHITE, HUD_COLOURS hcStringOverride = HUD_COLOUR_WHITE, BOOL paramOnWhileHandsTied = FALSE)
	PRINTLN(".KGM [Objective Text]: Print_Objective_Text_With_Player_Name_And_String_Coloured() - REDIRECTED TO Print_Objective_Text_With_User_Created_String(). hcPlayerNameOverride = ", ENUM_TO_INT(hcPlayerNameOverride), ", hcStringOverride = ", ENUM_TO_INT(hcStringOverride))
	
	IF Print_Objective_Text_With_Two_User_Created_Strings(paramMainText, GET_PLAYER_NAME(paramPlayerID), paramAdditionalText, paramOnWhileHandsTied, hcPlayerNameOverride)
		g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_PLAYER_NAME_AND_STRING_COLOURED
		g_sObjectiveTextMP.otHudColour = hcPlayerNameOverride
		g_sObjectiveTextMP.otHudColour2 = hcStringOverride
		g_sObjectiveTextMP.otPlayerID = paramPlayerID
	ENDIF
ENDPROC

// PURPOSE:	Display one piece of Objective Text with a two strings that are coloured with the HUD_COLOURS you pass through
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramAdditionalText		Additional text label to be printed, coloured the same as name.
//						paramAdditionalText2	2nd Additional text label to be printed, coloured the same as name.
//						hcPlayerNameOverride	The colour you wish to render the 1st substring in.
//						hcStringOverride		The colour you wish to render the 2nd substring in
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text_With_Two_Strings_Coloured(STRING paramMainText, STRING paramAdditionalText, STRING paramAdditionalText2, HUD_COLOURS hcStringOverride = HUD_COLOUR_WHITE, HUD_COLOURS hcString2Override = HUD_COLOUR_WHITE, BOOL paramOnWhileHandsTied = FALSE)
	PRINTLN(".KGM [Objective Text]: Print_Objective_Text_With_Two_Strings_Coloured() - REDIRECTED TO Print_Objective_Text_With_User_Created_String(). hcPlayerNameOverride = ", ENUM_TO_INT(hcStringOverride), ", hcStringOverride = ", ENUM_TO_INT(hcString2Override))
	
	IF Print_Objective_Text_With_Two_User_Created_Strings(paramMainText, paramAdditionalText, paramAdditionalText2, paramOnWhileHandsTied, hcStringOverride)
		g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TWO_STRINGS_COLOURED
		g_sObjectiveTextMP.otHudColour = hcStringOverride
		g_sObjectiveTextMP.otHudColour2 = hcString2Override
	ENDIF
ENDPROC

// PURPOSE:	Display one piece of Objective Text with a Substring that is coloured with the HUD_COLOURS you pass through
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramPlayerID			The PlayerID for the player name to be displayed within the string
//						paramAdditionalText		Additional text label to be printed, coloured the same as name.
//						hcColourOverride		The colour you wish to render the string in.
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text_With_Coloured_String(STRING paramMainText, STRING paramAdditionalText, HUD_COLOURS hcColourOverride, BOOL paramOnWhileHandsTied = FALSE)
	PRINTLN(".KGM [Objective Text]: Print_Objective_Text_With_Coloured_String() - REDIRECTED TO Print_Objective_Text_With_String(). hcColourOverride = ", ENUM_TO_INT(hcColourOverride))
	
	IF Print_Objective_Text_With_String(paramMainText, paramAdditionalText, paramOnWhileHandsTied, hcColourOverride)
		g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_COLOURED_STRING
		g_sObjectiveTextMP.otHudColour = hcColourOverride
	ENDIF
ENDPROC

// PURPOSE:	Display one piece of Objective Text with a Substring text label that is coloured with the HUD_COLOURS you pass through
//			Actually does what _With_Coloured_String should do
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramPlayerID			The PlayerID for the player name to be displayed within the string
//						paramAdditionalText		Additional text label to be printed, coloured the same as name.
//						hcColourOverride		The colour you wish to render the string in.
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text_With_Coloured_Text_Label(STRING paramMainText, STRING paramAdditionalText, HUD_COLOURS hcColourOverride, BOOL paramOnWhileHandsTied = FALSE)
	PRINTLN(".KGM [Objective Text]: Print_Objective_Text_With_Coloured_Text_Label() - REDIRECTED TO Print_Objective_Text_With_String(). paramMainText = ", paramMainText, " - hcColourOverride = ", ENUM_TO_INT(hcColourOverride))	
	
	IF Print_Objective_Text_With_String(paramMainText, paramAdditionalText, paramOnWhileHandsTied, hcColourOverride)
	
		g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_COLOURED_TEXT_LABEL
		g_sObjectiveTextMP.otHudColour = hcColourOverride
	ENDIF
ENDPROC

// PURPOSE:	Display one piece of Objective Text with a two user substrings with an additional Substring (TL15) that is coloured with the HUD_COLOURS you pass through
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramUserString1		Additional text label to be printed, can be a name
//						paramUserString2		Additional text label to be printed, can be a name
//						paramString				Additional 3rd string to insert into main text 
//						hcColourOverride		The colour you wish to render the third string in.
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text_With_Two_User_Created_Strings_And_String(STRING paramMainText, STRING paramUserString1, STRING paramUserString2, STRING paramString, HUD_COLOURS hcColourOverride = HUD_COLOUR_WHITE, BOOL paramOnWhileHandsTied = FALSE)
	PRINTLN(".KGM [Objective Text]: Print_Objective_Text_With_Two_User_Created_Strings_And_String() - REDIRECTED TO Print_Objective_Text_With_Two_User_Created_Strings(). hcColourOverride = ", ENUM_TO_INT(hcColourOverride), ", third string: ", paramString)
	
	IF Print_Objective_Text_With_Two_User_Created_Strings(paramMainText, paramUserString1, paramUserString2, paramOnWhileHandsTied, hcColourOverride)
		g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_TWO_PLAYER_NAMES_AND_STRING
		g_sObjectiveTextMP.otStringInString3 = paramString
		g_sObjectiveTextMP.otHudColour = hcColourOverride
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with a Player Name Literal String that is coloured with the HUD_COLOURS you pass through
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramPlayerID			The PlayerID for the player name to be displayed within the string
//						hcColourOverride		The colour you wish to render the player's name in.
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text_With_Coloured_Player_Name(STRING paramMainText, PLAYER_INDEX paramPlayerID, HUD_COLOURS hcColourOverride, BOOL paramOnWhileHandsTied = FALSE)
	PRINTLN(".KGM [Objective Text]: Print_Objective_Text_With_Coloured_Player_Name() - REDIRECTED TO Print_Objective_Text_With_User_Created_String(). hcColourOverride = ", ENUM_TO_INT(hcColourOverride))
	
	IF Print_Objective_Text_With_User_Created_String(paramMainText, GET_PLAYER_NAME(paramPlayerID), paramOnWhileHandsTied, hcColourOverride)
		g_sObjectiveTextMP.otTextType = OTT_STRING_WITH_COLOURED_PLAYER_NAME
		g_sObjectiveTextMP.otHudColour = hcColourOverride
		g_sObjectiveTextMP.otPlayerID = paramPlayerID
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text with a Player Name Literal String (optionally team-coloured)
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
//						paramPlayerID			The PlayerID for the player name to be displayed within the string
//						paramUseColour			[DEFAULT = TRUE] Colour the player name using the team colour if available, or the player colour if not in a team
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
//
// NOTES:	KGM 20/8/11: The optional 'use team colour' parameter was added so the the player name would display in red for 'enemies' for assassination.
// NOTES:	KGM 23/6/15: This was added for CnC and the player names are team-coloured. Now re0directing to a different function to allow any coloured player names.
PROC Print_Objective_Text_With_Two_Player_Names(STRING paramMainText, PLAYER_INDEX paramPlayerID1, PLAYER_INDEX paramPlayerID2, BOOL paramUseColour = TRUE, BOOL paramOnWhileHandsTied = FALSE)

	PRINTLN(".KGM [Objective Text]: Print_Objective_Text_With_Two_Player_Names() - REDIRECTED TO Print_Objective_Text_With_Two_User_Created_Strings()")
	
	Print_Objective_Text_With_Two_User_Created_Strings(paramMainText, GET_PLAYER_NAME(paramPlayerID1), GET_PLAYER_NAME(paramPlayerID2), paramOnWhileHandsTied)
	
	UNUSED_PARAMETER(paramUseColour)
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display one piece of Objective Text that is User-Created
//
// INPUT PARAMS:		paramUserString			The user-created string
//						paramOnWhileHandsTied	[DEFAULT = FALSE] Allow this text to display while a player's hands are tied
PROC Print_Objective_Text_User_Created(STRING paramUserString, BOOL paramOnWhileHandsTied = FALSE)

	// Error Checking
	IF (Is_String_Null_Or_Empty(paramUserString))
		SCRIPT_ASSERT("Print_Objective_Text_User_Created(): The User-Created String is NULL or empty")
		EXIT
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramUserString) > MAX_LENGTH_MP_USER_CREATED_TEXT)
		SCRIPT_ASSERT("Print_Objective_Text_User_Created(): The User-Created String is longer than expected. Tell Keith.")
		EXIT
	ENDIF
	
	// Do nothing if already received
	IF (Has_This_MP_Objective_Text_User_Created_Been_Received(paramUserString))
		EXIT
	ENDIF
	
	// Display the initial piece of debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Request to Print_Objective_Text_User_Created() - details received") NET_NL()
	#ENDIF
	
	// This will replace any existing Objective Text
	Delete_MP_Objective_Text()
	
	// Store the relevant details
	g_sObjectiveTextMP.otTextType			= OTT_STRING_AS_USER_CREATED_STRING
	g_sObjectiveTextMP.otScriptName			= GET_THIS_SCRIPT_NAME()
	g_sObjectiveTextMP.otScriptNameHash		= GET_HASH_KEY(g_sObjectiveTextMP.otScriptName)
	g_sObjectiveTextMP.otMainTextLabel		= "STRING"
	g_sObjectiveTextMP.otStringInString1	= paramUserString
	Calculate_And_Store_MP_Objective_Text_Timers()
	
	// Store any optional parameters
	Set_Objective_Text_Option_Display_While_Hands_Tied(paramOnWhileHandsTied)
	
	IF NOT ARE_STRINGS_EQUAL(paramUserString, " ")
		Set_Objective_Text_Option_Display_On_Brief_Screen()
	ELSE
		PRINTLN("[ML][Print_Objective_Text_User_Created] Blocking print to brief screen as string is just a single white space. Fixes a hack sometimes used by content")
	ENDIF
	
	// Display the details
	#IF IS_DEBUG_BUILD
		g_WIDGET_ObjectiveTextReceived = TRUE
		NET_PRINT("   Objective Text Newly Stored Details") NET_NL()
		ADD_SCRIPT_METRIC_MARKER(paramUserString)	// Assume this is correct - storing the user string as the metric marker text
		Debug_Output_Objective_Text_To_Console_Log()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear a specific piece of Objective Text
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
PROC Clear_This_Objective_Text(STRING paramMainText)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Clear_This_Objective_Text() called from: ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_NL()
	#ENDIF

	IF (Is_String_Null_Or_Empty(paramMainText))
		SCRIPT_ASSERT("Clear_This_Objective_Text(): The Main Text Label string is NULL or empty")
		EXIT
	ENDIF
	
	// Clear the text if it is the current Objective Text
	BOOL foundThisText = FALSE
	IF (Has_Any_MP_Objective_Text_Been_Received())
		IF (g_sObjectiveTextMP.otTextType = OTT_STRING_AS_USER_CREATED_STRING)
			// User Created Objective Text should use the additional text storage (the main storage will always be "STRING")
			IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_USER_CREATED_TEXT)
				SCRIPT_ASSERT("Clear_This_Objective_Text(): The Main Text Label string is longer than expected for user-created text. Tell Keith.")
				EXIT
			ENDIF

			foundThisText = (GET_HASH_KEY(paramMainText) = GET_HASH_KEY(g_sObjectiveTextMP.otStringInString1))
		ELSE
			// Normal Objective Text
			IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
				SCRIPT_ASSERT("Clear_This_Objective_Text(): The Main Text Label string is longer than expected. Tell Keith.")
				EXIT
			ENDIF

			foundThisText = (GET_HASH_KEY(paramMainText) = GET_HASH_KEY(g_sObjectiveTextMP.otMainTextLabel))
		ENDIF
	ENDIF

	
	IF NOT (foundThisText)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........The requested text (")
			NET_PRINT(paramMainText)
			NET_PRINT(") is not stored. This text is stored: ")
			IF (g_sObjectiveTextMP.otTextType = OTT_STRING_AS_USER_CREATED_STRING)
				// User-Created Objective Text
				NET_PRINT(g_sObjectiveTextMP.otStringInString1)
				NET_PRINT("  [User-Created]")
			ELSE
				// Normal Objective Text
				NET_PRINT(g_sObjectiveTextMP.otMainTextLabel)
			ENDIF
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	Delete_MP_Objective_Text()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear Any Objective Text from this script
PROC Clear_Any_Objective_Text_From_This_Script()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Clear_Any_Objective_Text_From_This_Script() called from: ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		DEBUG_PRINTCALLSTACK()
		NET_NL()
	#ENDIF

	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........There is no Objective Text to be cleared.")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Clear the text if it is from this script
	IF NOT (GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = g_sObjectiveTextMP.otScriptNameHash)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........The current Objective text didn't come from this script but from: ")
			NET_PRINT(g_sObjectiveTextMP.otScriptName)
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF

	Delete_MP_Objective_Text()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear Any Objective Text
PROC Clear_Any_Objective_Text()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Clear_Any_Objective_Text() called from: ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_NL()
	#ENDIF

	Delete_MP_Objective_Text()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Pause The Objective Text
// NOTES:	Doesn't display it but retains the information - must be unpaused
PROC Pause_Objective_Text()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Pause_Objective_Text() called from: ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_NL()
		
		IF (g_sObjectiveTextControlMP.otcIsPaused)
			NET_PRINT("         OBJECTIVE TEXT WAS ALREADY PAUSED BY: ")
			NET_PRINT(g_sObjectiveTextControlMP.otcPauseScriptName)
			NET_NL()
		ENDIF
	#ENDIF

	g_sObjectiveTextControlMP.otcIsPaused				= TRUE
	g_sObjectiveTextControlMP.otcPauseScriptName		= GET_THIS_SCRIPT_NAME()
	g_sObjectiveTextControlMP.otcPauseScriptNameHash	= GET_HASH_KEY(g_sObjectiveTextControlMP.otcPauseScriptName)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Unpause The Objective Text
// NOTES:	Allows the retained information to be displayed again
PROC Unpause_Objective_Text()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Objective Text]: Unpause_Objective_Text() called from: ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_NL()
		
		IF (g_sObjectiveTextControlMP.otcIsPaused)
			IF NOT (GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = g_sObjectiveTextControlMP.otcPauseScriptNameHash)
				NET_PRINT("         UNPAUSING, BUT OBJECTIVE TEXT WAS PAUSED BY A DIFFERENT SCRIPT: ")
				NET_PRINT(g_sObjectiveTextControlMP.otcPauseScriptName)
				NET_NL()
			ENDIF
		ENDIF
	#ENDIF
	
	IF NOT (g_sObjectiveTextControlMP.otcIsPaused)
		NET_PRINT("         Objective Text was not paused")
		NET_NL()
		EXIT
	ENDIF

	Clear_Objective_Text_Pause()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a specific piece of Objective Text has been received
//
// INPUT PARAMS:		paramMainText			Text Label containing the main display text
// RETURN VALUE:		BOOL					TRUE if this specific Objective Text is the current Objective Text, otherwise FALSE
//
// NOTES:	KGM 7/9/12: The originally internal function is now being called externally, so this function now just calls the other function (performing a couple of pre-checks)
FUNC BOOL Is_This_The_Current_Objective_Text(STRING paramMainText)

	IF (Is_String_Null_Or_Empty(paramMainText))
		SCRIPT_ASSERT("Is_This_The_Current_Objective_Text(): The Main Text Label string is NULL or empty")
		RETURN FALSE
	ENDIF
	
	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF

	IF (g_sObjectiveTextMP.otTextType = OTT_STRING_AS_USER_CREATED_STRING)
		IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_USER_CREATED_TEXT)
			SCRIPT_ASSERT("Is_This_The_Current_Objective_Text(): The Main Text Label string is longer than expected for User-Created text. Tell Keith.")
			RETURN FALSE
		ENDIF
	ELSE
		IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMainText) > MAX_LENGTH_MP_TEXT_LABEL_FOR_TEXT)
			SCRIPT_ASSERT("Is_This_The_Current_Objective_Text(): The Main Text Label string is longer than expected. Tell Keith.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Check if this text is the current Objective Text
	RETURN (Has_This_MP_Objective_Text_Been_Received(paramMainText))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check For Any Current Objective Text from this script
//
// RETURN VALUE:		BOOL					TRUE if the current Objective Text was requested by this script, otherwise FALSE
FUNC BOOL Is_There_Any_Current_Objective_Text_From_This_Script()

	IF NOT (Has_Any_MP_Objective_Text_Been_Received())
		RETURN FALSE
	ENDIF
	
	// Clear the text if it is from this script
	IF NOT (GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = g_sObjectiveTextMP.otScriptNameHash)
		RETURN FALSE
	ENDIF
	
	// The current Objective Text is from this script
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check For Any Current Objective Text
//
// RETURN VALUE:		BOOL					TRUE if there is any current Objective Text, otherwise FALSE
FUNC BOOL Is_There_Any_Current_Objective_Text()

	RETURN (Has_Any_MP_Objective_Text_Been_Received())
	
ENDFUNC






