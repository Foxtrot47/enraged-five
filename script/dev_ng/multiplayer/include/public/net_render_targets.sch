USING "net_include.sch"

/// PURPOSE: Returns TRUE when a render target has been registered
FUNC BOOL REGISTER_RENDER_TARGET(STRING sRenderTarget)
	IF NOT IS_STRING_NULL_OR_EMPTY(sRenderTarget)
		IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
			REGISTER_NAMED_RENDERTARGET(sRenderTarget)
			PRINTLN("[SCRIPT_RT] REGISTER_RENDER_TARGET - Registering rendertarget ", sRenderTarget)
		ELSE
			PRINTLN("[SCRIPT_RT] REGISTER_RENDER_TARGET - Rendertarget ", sRenderTarget, " is already registered.")
		ENDIF
		RETURN TRUE
	ELSE
		ASSERTLN("[SCRIPT_RT] REGISTER_RENDER_TARGET - sRenderTarget = NULL or Empty.")
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE when a render target has been linked
FUNC BOOL LINK_RENDER_TARGET(MODEL_NAMES eRenderTargetModel)
	IF (eRenderTargetModel != DUMMY_MODEL_FOR_SCRIPT)
		IF NOT IS_NAMED_RENDERTARGET_LINKED(eRenderTargetModel)
			LINK_NAMED_RENDERTARGET(eRenderTargetModel)
			PRINTLN("[SCRIPT_RT] LINK_RENDER_TARGET - Linking rendertarget with model #", ENUM_TO_INT(eRenderTargetModel))
		ELSE
			PRINTLN("[SCRIPT_RT] LINK_RENDER_TARGET - Rendertarget with model #", ENUM_TO_INT(eRenderTargetModel), " is already linked.")
		ENDIF
		RETURN TRUE
	ELSE
		ASSERTLN("[SCRIPT_RT] LINK_RENDER_TARGET - eRenderTargetModel = DUMMY_MODEL_FOR_SCRIPT.")
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the ID of a render target.
///    Render target should be registered and linked first.
///    Returns -1 if ID is invalid.
FUNC INT GET_RENDER_TARGET_ID(STRING sRenderTarget)
	IF NOT IS_STRING_NULL_OR_EMPTY(sRenderTarget)
		RETURN GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
	ENDIF
	RETURN -1
ENDFUNC

/// PURPOSE: Returns TRUE when a render target has been registered and linked.
FUNC BOOL REGISTER_AND_LINK_RENDER_TARGET(STRING sRenderTarget, MODEL_NAMES eRenderTargetModel)
	RETURN (REGISTER_RENDER_TARGET(sRenderTarget) AND LINK_RENDER_TARGET(eRenderTargetModel))
ENDFUNC

/// PURPOSE: Releases a registered render target.
PROC RELEASE_REGISTERED_RENDER_TARGET(STRING sRenderTarget)
	IF IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
		RELEASE_NAMED_RENDERTARGET(sRenderTarget)
	ENDIF
ENDPROC
