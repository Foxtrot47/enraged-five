//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_CLUBHOUSE.sch																					//
// Description: Functions to implement from net_peds_base.sch and look up tables to return CLUBHOUSE ped data.			//
// Written by:  Jan Mencfel																								//
// Date:  		06/04/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_DLC_1_2022
USING "net_peds_base.sch"
USING "website_public.sch"
USING "net_peds_data_common.sch"
USING "net_simple_cutscene_common.sch"

FUNC VECTOR _GET_CLUBHOUSE_PED_LOCAL_COORDS_BASE_POSITION(PED_LOCATIONS ePedLocation)
UNUSED_PARAMETER(ePedLocation)
	RETURN << 0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT _GET_CLUBHOUSE_PED_LOCAL_HEADING_BASE_HEADING(PED_LOCATIONS ePedLocation)
UNUSED_PARAMETER(ePedLocation)
	RETURN 0.0
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//SINGLE LEVEL CLUBHOUSE
PROC _SET_CLUBHOUSE_PED_DATA_LAYOUT_A_0(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 17039364
					Data.iPackedDrawable[1]								= 16973827
					Data.iPackedDrawable[2]								= 1025
					Data.iPackedTexture[0] 								= 50397184
					Data.iPackedTexture[1] 								= 50529026
					Data.iPackedTexture[2] 								= 65537
					Data.vPosition 										= <<1122.7600, -3143.6860, -37.2750>>
					Data.vRotation 										= <<0.0000, 0.0000, 85.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_F)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 16842753
					Data.iPackedDrawable[1]								= 16843009
					Data.iPackedDrawable[2]								= 65793
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 196611
					Data.iPackedTexture[2] 								= 3
					Data.vPosition 										= <<1125.5350, -3149.0740, -37.546>>
					Data.vRotation 										= <<0.0000, 0.0000, 98.8900>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 67305476
					Data.iPackedDrawable[1]								= 131076
					Data.iPackedDrawable[2]								= 769
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 16974595
					Data.iPackedTexture[2] 								= 131075
					Data.vPosition 										= <<1124.4670, -3148.5300, -37.0375>>
					Data.vRotation 										= <<0.0000, 0.0000, 236.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_CLUBHOUSE_PED_DATA_LAYOUT_A_1(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 50397189
					Data.iPackedDrawable[1]								= 17039620
					Data.iPackedDrawable[2]								= 259
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 50332416
					Data.iPackedTexture[2] 								= 131072
					Data.vPosition 										= <<1120.2314, -3148.8044, -37.0500>>
					Data.vRotation 										= <<0.0000, 0.0000, 32.8650>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_SMOKING_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33685506
					Data.iPackedDrawable[1]								= 33686018
					Data.iPackedDrawable[2]								= 131586
					Data.iPackedTexture[0] 								= 50397184
					Data.iPackedTexture[1] 								= 65537
					Data.iPackedTexture[2] 								= 65536
					Data.vPosition 										= <<1119.8665, -3148.2266, -37.0680>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 83951622
					Data.iPackedDrawable[1]								= 65793
					Data.iPackedDrawable[2]								= 259
					Data.iPackedTexture[0] 								= 33619969
					Data.iPackedTexture[1] 								= 16908546
					Data.iPackedTexture[2] 								= 65536
					Data.vPosition 										= <<1125.3540, -3146.4299, -37.5460>>
					Data.vRotation 										= <<0.0000, 0.0000, 90.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_CLUBHOUSE_PED_DATA_LAYOUT_A_2(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_LEAN_DRINK)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 67436550
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 1280
					Data.iPackedTexture[0] 								= 50462720
					Data.iPackedTexture[1] 								= 33620225
					Data.iPackedTexture[2] 								= 65538
					Data.vPosition 										= <<1123.0410, -3142.5950, -37.0500>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_F)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 50528259
					Data.iPackedDrawable[1]								= 50529027
					Data.iPackedDrawable[2]								= 197379
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 196608
					Data.iPackedTexture[2] 								= 131072
					Data.vPosition 										= <<1124.6200, -3147.9761, -37.0500>>
					Data.vRotation 										= <<0.0000, 0.0000, 114.5000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_F)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842753
					Data.iPackedTexture[1] 								= 16842755
					Data.iPackedTexture[2] 								= 131073
					Data.vPosition 										= <<1123.8760, -3147.9651, -37.0500>>
					Data.vRotation 										= <<0.0000, 0.0000, 279.2500>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC
//TWO LEVEL CLUBHOUSE
PROC _SET_CLUBHOUSE_PED_DATA_LAYOUT_B_0(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33619969
					Data.iPackedDrawable[1]								= 65793
					Data.iPackedDrawable[2]								= 258
					Data.iPackedTexture[0] 								= 33554434
					Data.iPackedTexture[1] 								= 50397442
					Data.iPackedTexture[2] 								= 65539
					Data.vPosition 										= <<999.875, -3167.542, -34.076>>
					Data.vRotation 										= <<0.0000, 0.0000, -65.650>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_F)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908289
					Data.iPackedTexture[1] 								= 16908291
					Data.iPackedTexture[2] 								= 131073
					Data.vPosition 										= <<1000.7605, -3167.4880, -34.0500>>
					Data.vRotation 										= <<0.0000, 0.0000, 117.000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_LEANING_PHONE_F)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 16842753
					Data.iPackedDrawable[1]								= 16843009
					Data.iPackedDrawable[2]								= 65793
					Data.iPackedTexture[0] 								= 16777218
					Data.iPackedTexture[1] 								= 196611
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<997.4065, -3164.083, -34.05>>
					Data.vRotation 										= <<0.0000, 0.0000, -90>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC
PROC _SET_CLUBHOUSE_PED_DATA_LAYOUT_B_1(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_F)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33685506
					Data.iPackedDrawable[1]								= 33686018
					Data.iPackedDrawable[2]								= 131586
					Data.iPackedTexture[0] 								= 33554432
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 131072
					Data.vPosition 										= <<997.9190, -3166.064, -34.05>>
					Data.vRotation 										= <<0.0000, 0.0000, -162.360>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_F)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685505
					Data.iPackedTexture[1] 								= 16973825
					Data.iPackedTexture[2] 								= 131073
					Data.vPosition 										= <<997.6565, -3166.819, -34.05>>
					Data.vRotation 										= <<0.0000, 0.0000, 29.160>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR_B)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 131079
					Data.iPackedDrawable[1]								= 33816579
					Data.iPackedDrawable[2]								= 512
					Data.iPackedTexture[0] 								= 50331649
					Data.iPackedTexture[1] 								= 512
					Data.iPackedTexture[2] 								= 131074
					Data.vPosition 										= <<999.9925, -3169.012, -34.3>>
					Data.vRotation 										= <<0.0000, 0.0000, 117.250>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC
PROC _SET_CLUBHOUSE_PED_DATA_LAYOUT_B_2(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_F)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 16842753
					Data.iPackedDrawable[1]								= 16843009
					Data.iPackedDrawable[2]								= 65793
					Data.iPackedTexture[0] 								= 50397186
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 3
					Data.vPosition 										= <<997.5905, -3166.324, -34.0750>>
					Data.vRotation 										= <<0.0000, 0.0000, -75.120>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 84213765
					Data.iPackedDrawable[1]								= 16777475
					Data.iPackedDrawable[2]								= 66819
					Data.iPackedTexture[0] 								= 50397186
					Data.iPackedTexture[1] 								= 50463490
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<999.8905, -3165.08, -34.075>>
					Data.vRotation 										= <<0.0000, 0.0000, 176.75>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUBHOUSE_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 65538
					Data.iPackedDrawable[1]								= 16842753
					Data.iPackedDrawable[2]								= 256
					Data.iPackedTexture[0] 								= 33554432
					Data.iPackedTexture[1] 								= 131586
					Data.iPackedTexture[2] 								= 131074
					Data.vPosition 										= <<999.8905, -3165.783, -34.075>>
					Data.vRotation 										= <<0.0000, 0.0000, 8.640>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC
//PROC _SET_CLUBHOUSE_PED_PROP_INDEXES(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
//	UNUSED_PARAMETER(ePedModel)
//	UNUSED_PARAMETER(iLayout)
//	SWITCH iPed		
//		CASE 1
//			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
//		BREAK
//	ENDSWITCH 
//ENDPROC
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTIONS TO IMPLEMENT ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_CLUBHOUSE_PED_SCRIPT_LAUNCH()
	RETURN IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
ENDFUNC

FUNC BOOL _IS_CLUBHOUSE_PARENT_A_SIMPLE_INTERIOR
	RETURN FALSE
ENDFUNC

PROC _SET_CLUBHOUSE_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
UNUSED_PARAMETER(ServerBD)
UNUSED_PARAMETER(ePedLocation)
	//CLUBHOUSE LAYOUT A
	IF IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty, PROPERTY_CLUBHOUSE_1_BASE_A)
		SWITCH iLayout
			CASE 0
				PRINTLN("_SET_CLUBHOUSE_PED_DATA   USING LAYOUT A 0, PED: ", iPed)	
				_SET_CLUBHOUSE_PED_DATA_LAYOUT_A_0(Data, iPed, bSetPedArea)
			BREAK
			CASE 1
				PRINTLN("_SET_CLUBHOUSE_PED_DATA   USING LAYOUT A 1, PED: ", iPed)	
				_SET_CLUBHOUSE_PED_DATA_LAYOUT_A_1(Data, iPed, bSetPedArea)
			BREAK
			CASE 2
				PRINTLN("_SET_CLUBHOUSE_PED_DATA   USING LAYOUT A 2, PED: ", iPed)	
				_SET_CLUBHOUSE_PED_DATA_LAYOUT_A_2(Data, iPed, bSetPedArea)
			BREAK
		ENDSWITCH
	ELSE
	//CLUBHOUSE LAYOUT B
		SWITCH iLayout
			CASE 0
				PRINTLN("_SET_CLUBHOUSE_PED_DATA   USING LAYOUT B 0, PED: ", iPed)	
				_SET_CLUBHOUSE_PED_DATA_LAYOUT_B_0(Data, iPed, bSetPedArea)
			BREAK
			CASE 1
				PRINTLN("_SET_CLUBHOUSE_PED_DATA   USING LAYOUT B 1, PED: ", iPed)	
				_SET_CLUBHOUSE_PED_DATA_LAYOUT_B_1(Data, iPed, bSetPedArea)
			BREAK
			CASE 2
				PRINTLN("_SET_CLUBHOUSE_PED_DATA   USING LAYOUT B 2, PED: ", iPed)	
				_SET_CLUBHOUSE_PED_DATA_LAYOUT_B_2(Data, iPed, bSetPedArea)
			BREAK
		ENDSWITCH
	ENDIF
		
ENDPROC

FUNC INT _GET_CLUBHOUSE_NETWORK_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
UNUSED_PARAMETER(ServerBD)
UNUSED_PARAMETER(ePedLocation)
	RETURN 0
ENDFUNC

FUNC INT _GET_CLUBHOUSE_LOCAL_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
UNUSED_PARAMETER(ePedLocation)
UNUSED_PARAMETER(ServerBD)
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_BAR_RESUPPLY_CR) > 0
		PRINTLN("[AM_MP_PEDS] _GET_CLUBHOUSE_LOCAL_PED_TOTAL - WILL SPAWN 3 PEDS")
		RETURN 3
	ENDIF
	
	PRINTLN("[AM_MP_PEDS] _GET_CLUBHOUSE_LOCAL_PED_TOTAL - WILL SPAWN 0 PEDS")
	RETURN 0
	
ENDFUNC

FUNC INT _GET_CLUBHOUSE_SERVER_PED_LAYOUT_TOTAL()
	RETURN 3
ENDFUNC

FUNC INT _GET_CLUBHOUSE_SERVER_PED_LAYOUT()	
	RETURN GET_RANDOM_INT_IN_RANGE(0, _GET_CLUBHOUSE_SERVER_PED_LAYOUT_TOTAL())
ENDFUNC

FUNC INT _GET_CLUBHOUSE_SERVER_PED_LEVEL()
	RETURN 2  // Ped levels 0, 1 & 2 spawn by default
ENDFUNC

PROC _SET_CLUBHOUSE_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
UNUSED_PARAMETER(ePedLocation)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBD.iLayout 			= _GET_CLUBHOUSE_SERVER_PED_LAYOUT()
		ServerBD.iLevel 			= _GET_CLUBHOUSE_SERVER_PED_LEVEL()
		ServerBD.iMaxLocalPeds 		= _GET_CLUBHOUSE_LOCAL_PED_TOTAL(ServerBD)
		ServerBD.iMaxNetworkPeds	= _GET_CLUBHOUSE_NETWORK_PED_TOTAL(ServerBD)
		PRINTLN("[AM_MP_PEDS] _SET_CLUBHOUSE_PED_SERVER_DATA - PLAYER IS THE HOST OF THE SCRIPT")
	ELSE
		PRINTLN("[AM_MP_PEDS] _SET_CLUBHOUSE_PED_SERVER_DATA - PLAYER IS NOT THE HOST OF THE SCRIPT")
	ENDIF
		
	g_iPedLayout = ServerBD.iLayout		
	IF ServerBD.iMaxLocalPeds = 0
		PRINTLN("[AM_MP_PEDS] _SET_CLUBHOUSE_PED_SERVER_DATA - Host set max peds to 0 ( Didn't complete the resupply mission) NOT SPAWNING PATRON PEDS")
		g_iPedLayout = -1
	ENDIF
	
	PRINTLN("[AM_MP_PEDS] _SET_CLUBHOUSE_PED_SERVER_DATA - Layout: ", ServerBD.iLayout)
	PRINTLN("[AM_MP_PEDS] _SET_CLUBHOUSE_PED_SERVER_DATA - g_iPedLayout: ", g_iPedLayout)
	PRINTLN("[AM_MP_PEDS] _SET_CLUBHOUSE_PED_SERVER_DATA - Level: ", ServerBD.iLevel)
	PRINTLN("[AM_MP_PEDS] _SET_CLUBHOUSE_PED_SERVER_DATA - Max Local Peds: ", ServerBD.iMaxLocalPeds)
	PRINTLN("[AM_MP_PEDS] _SET_CLUBHOUSE_PED_SERVER_DATA - Max Network Peds: ", ServerBD.iMaxNetworkPeds)
ENDPROC

FUNC BOOL _IS_PLAYER_IN_CLUBHOUSE_PARENT_PROPERTY(PLAYER_INDEX playerID)
PRINTLN("[AM_MP_PEDS] _IS_PLAYER_IN_CLUBHOUSE_PARENT_PROPERTY")
	RETURN IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty)
ENDFUNC

FUNC BOOL _HAS_CLUBHOUSE_PED_BEEN_CREATED(PEDS_DATA &Data, INT iLevel)
	
	IF (IS_ENTITY_ALIVE(Data.PedID)
		AND (GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK))
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
	OR (Data.iLevel > iLevel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED MODELS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_CLUBHOUSE_LOCAL_PED_PROPERTIES(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)
	CLEAR_PED_TASKS(PedID)
ENDPROC

PROC _SET_CLUBHOUSE_NETWORK_PED_PROPERTIES(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NETWORK_GET_ENTITY_FROM_NETWORK_ID(NetworkPedID), TRUE)
	SET_NETWORK_ID_CAN_MIGRATE(NetworkPedID, FALSE)
	
	IF IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, FALSE)
	ELSE
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, TRUE)
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED ANIM DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Note: Activities with multiple animations have their anim data populated in their own functions below.
//		 If an activity only has one animation associated with it, it is populated within _GET_CLUBHOUSE_PED_ANIM_DATA itself.
//
// Note: Each activity either has a _M_ or _F_ in its title to specify which ped gender should be using that activity.
//		 If an activity does not has either an _M_ or _F_ in its title; it is unisex, and can be used by either ped gender.
//
// Note: Some animations have been excluded from activities. 
//		 The excluded anims have Z axis starting positions that dont line up with the other anims in the same dictionary.
//		 This causes a snap that a blend cannot fix. Use the widget 'RAG/Script/AM_MP_PEDS/Animation/Output Initial Activity Anim Data' to see which Z axis anims are broken.

//╒═══════════════════════════════╕
//╞══════╡ LEANING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_CLUBHOUSE_LEANING_SMOKING_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_D"
		BREAK
		CASE 5
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_E"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CLUBHOUSE_SITTING_DRINKING_M_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@base"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_a"
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_a"
			pedAnimData.sAnimClip 		= "idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_a"
			pedAnimData.sAnimClip 		= "idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_b"
			pedAnimData.sAnimClip 		= "idle_d"
		BREAK
		CASE 5
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_b"
			pedAnimData.sAnimClip 		= "idle_e"
		BREAK
		CASE 6
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_b"
			pedAnimData.sAnimClip 		= "idle_f"
		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞═════╡ STANDING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_CLUBHOUSE_STANDING_SMOKING_M_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞════════╡ ANIM LOOKUP ╞════════╡
//╘═══════════════════════════════╛
PROC _GET_CLUBHOUSE_PED_ANIM_DATA(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	UNUSED_PARAMETER(iPedID)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(eTransitionState)
	UNUSED_PARAMETER(bPedTransition)
	UNUSED_PARAMETER(iClip)
	RESET_PED_ANIM_DATA(pedAnimData)
	SWITCH eActivity
		CASE PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR
		CASE PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR_B
			pedAnimData.sAnimDict		= "amb@prop_human_seat_bar@male@elbows_on_bar@base"
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_M						
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			

			GET_CLUBHOUSE_SITTING_DRINKING_M_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_M						
			pedAnimData.sAnimDict		= "amb@world_human_drinking@beer@male@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK					
		CASE PED_ACT_LEANING_SMOKING_M_01
		CASE PED_ACT_LEANING_SMOKING_F_01
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.2, 0, DEFAULT, TRUE)		
			pedAnimData.bVFXAnimClip	= TRUE
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_CLUBHOUSE_LEANING_SMOKING_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_F						
			pedAnimData.sAnimDict		= "amb@prop_human_seat_chair_drink_beer@female@base"
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CLUBHOUSE_LEAN_DRINK						
			pedAnimData.sAnimDict		= "amb@world_human_leaning@male@wall@back@beer@base"
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_M	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)

			GET_CLUBHOUSE_STANDING_SMOKING_M_ANIM_CLIP(pedAnimData, iClip)
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.2, 0, DEFAULT, TRUE)		
			pedAnimData.bVFXAnimClip	= TRUE
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_F						
			pedAnimData.sAnimDict		= "amb@world_human_drinking@beer@female@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_LEANING_PHONE_F						
			pedAnimData.sAnimDict		= "amb@world_human_leaning@female@wall@back@mobile@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_F						
			pedAnimData.sAnimDict		= "amb@world_human_smoking@female@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
	
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.2, 0, DEFAULT, TRUE)		
			pedAnimData.bVFXAnimClip	= TRUE
		BREAK
		CASE PED_ACT_WAREHOUSE_STAND_TEXTING_F
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC INT _GET_CLUBHOUSE_ACTIVE_PED_LEVEL_THRESHOLD(INT iLevel)
	INT iThreshold = -1
	SWITCH iLevel
		// Peds have level 0, 1 & 2. 
		// Level 2 peds are culled if the player count > 10
		// Level 1 peds are culled if the player count > 20
		// Level 0 peds are base and always remain
		CASE 0	iThreshold = 20	BREAK
		CASE 1	iThreshold = 10	BREAK
		CASE 2	iThreshold = 0	BREAK
	ENDSWITCH
	RETURN iThreshold
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_CLUBHOUSE_LOOK_UP_TABLE(PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		// Script Launching
		CASE E_SHOULD_PED_SCRIPT_LAUNCH
			interface.returnShouldPedScriptLaunch = &_SHOULD_CLUBHOUSE_PED_SCRIPT_LAUNCH
		BREAK
		CASE E_IS_PARENT_A_SIMPLE_INTERIOR
			interface.returnIsParentASimpleInterior = &_IS_CLUBHOUSE_PARENT_A_SIMPLE_INTERIOR
		BREAK
		
		// Ped Data
		CASE E_GET_LOCAL_PED_TOTAL
			interface.returnGetLocalPedTotal = &_GET_CLUBHOUSE_LOCAL_PED_TOTAL
		BREAK
		CASE E_GET_NETWORK_PED_TOTAL
			interface.returnGetNetworkPedTotal = &_GET_CLUBHOUSE_NETWORK_PED_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT_TOTAL
			interface.returnGetServerPedLayoutTotal= &_GET_CLUBHOUSE_SERVER_PED_LAYOUT_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT
			interface.returnGetServerPedLayout = &_GET_CLUBHOUSE_SERVER_PED_LAYOUT
		BREAK
		CASE E_GET_SERVER_PED_LEVEL
			interface.returnGetServerPedLevel = &_GET_CLUBHOUSE_SERVER_PED_LEVEL
		BREAK
		CASE E_SET_PED_DATA
			interface.setPedData = &_SET_CLUBHOUSE_PED_DATA
		BREAK
		CASE E_SET_PED_SERVER_DATA
			interface.setPedServerData = &_SET_CLUBHOUSE_PED_SERVER_DATA
		BREAK
		CASE E_GET_PED_ANIM_DATA
			interface.getPedAnimData = &_GET_CLUBHOUSE_PED_ANIM_DATA
		BREAK
		CASE E_IS_PLAYER_IN_PARENT_PROPERTY
			interface.returnIsPlayerInParentProperty = &_IS_PLAYER_IN_CLUBHOUSE_PARENT_PROPERTY
		BREAK
		CASE E_GET_PED_LOCAL_COORDS_BASE_POSITION
			interface.returnGetPedLocalCoordsBasePosition = &_GET_CLUBHOUSE_PED_LOCAL_COORDS_BASE_POSITION
		BREAK
		CASE E_GET_PED_LOCAL_HEADING_BASE_HEADING
			interface.returnGetPedLocalHeadingBaseHeading = &_GET_CLUBHOUSE_PED_LOCAL_HEADING_BASE_HEADING
		BREAK
		CASE E_GET_ACTIVE_PED_LEVEL_THRESHOLD
			interface.getActivePedLevelThreshold = &_GET_CLUBHOUSE_ACTIVE_PED_LEVEL_THRESHOLD
		BREAK
		
		// Ped Creation
		CASE E_SET_LOCAL_PED_PROPERTIES
			interface.setLocalPedProperties = &_SET_CLUBHOUSE_LOCAL_PED_PROPERTIES
		BREAK
		CASE E_SET_NETWORK_PED_PROPERTIES
			interface.setNetworkPedProperties = &_SET_CLUBHOUSE_NETWORK_PED_PROPERTIES
		BREAK
//		CASE E_SET_PED_PROP_INDEXES
//			interface.setPedPropIndexes = &_SET_CLUBHOUSE_PED_PROP_INDEXES
//		BREAK
		CASE E_HAS_PED_BEEN_CREATED
			interface.returnHasPedBeenCreated = &_HAS_CLUBHOUSE_PED_BEEN_CREATED
		BREAK
						
	ENDSWITCH
ENDPROC
#ENDIF	// FEATURE_TUNER
