
// ====================================================================================
// ====================================================================================
//
// Name:        net_SCTV_common.sch
// Description: Common SCTV functions for use in .sc files.
// Written By:  David Gentles
//
// ====================================================================================
// ====================================================================================

//USING "net_hud_activating.sch"
USING "globals.sch"
USING "net_spectator_variables.sch"
USING "net_ticker_logic.sch"
USING "net_mission.sch"



//PULLED OUT OF IS_DEBUG_BUILD as needed for FINAL PRINTS
/// PURPOSE:
///    turns eSCTVModeSwitchStageList into a debug string
/// PARAMS:
///    eModeSwitchStage - 
/// RETURNS:
///    eModeSwitchStage as a string
FUNC STRING GET_SCTV_MODE_SWITCH_STAGE_NAME(eSCTVModeSwitchStageList eModeSwitchStage)
	SWITCH eModeSwitchStage
		CASE SCTV_MODE_SWITCH_STAGE_START
			RETURN "SCTV_MODE_SWITCH_STAGE_START"
		BREAK
		CASE SCTV_MODE_SWITCH_STAGE_FADE_OUT
			RETURN "SCTV_MODE_SWITCH_STAGE_FADE_OUT"
		BREAK
		CASE SCTV_MODE_SWITCH_STAGE_CLEANUP_OLD
			RETURN "SCTV_MODE_SWITCH_STAGE_CLEANUP_OLD"
		BREAK
		CASE SCTV_MODE_SWITCH_STAGE_SETUP_NEW
			RETURN "SCTV_MODE_SWITCH_STAGE_SETUP_NEW"
		BREAK
		CASE SCTV_MODE_SWITCH_STAGE_FINISH
			RETURN "SCTV_MODE_SWITCH_STAGE_FINISH"
		BREAK
		CASE SCTV_MODE_SWITCH_STAGE_COMPLETE
			RETURN "SCTV_MODE_SWITCH_STAGE_COMPLETE"
		BREAK
	ENDSWITCH
	RETURN "INVALID_MODE_SWITCH_STAGE"
ENDFUNC

FUNC BOOL IS_PLAYER_RUNNING_ANY_TYPE_OF_POST_MISSION_SCENE(PLAYER_INDEX playerId)
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bDoingApartmentPostMissionScene)//GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].bDoingApartmentPostMissionScene
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bDoingStripPostMissionScene)//GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].bDoingStripPostMissionScene
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_RUNNING_A_POST_CELEBRATION_SCENE(playerId)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    turns eSCTVModeList into a debug string
/// PARAMS:
///    eMode - 
/// RETURNS:
///    eMode as a string
FUNC STRING GET_SCTV_MODE_NAME(eSCTVModeList eMode)
	SWITCH eMode
		CASE SCTV_MODE_INIT
			RETURN "SCTV_MODE_INIT"
		BREAK
		CASE SCTV_MODE_FOLLOW_CAM
			RETURN "SCTV_MODE_FOLLOW_CAM"
		BREAK
		CASE SCTV_MODE_FREE_CAM
			RETURN "SCTV_MODE_FREE_CAM"
		BREAK
		CASE SCTV_MODE_FOLLOW_TO_SESSION
			RETURN "SCTV_MODE_FOLLOW_TO_SESSION"
		BREAK
	ENDSWITCH
	RETURN "INVALID_MODE"
ENDFUNC	

#IF IS_DEBUG_BUILD


/// PURPOSE:
///    turns eSCTVStageList into a debug string
/// PARAMS:
///    eStage - 
/// RETURNS:
///    eStage as a string
	FUNC STRING GET_SCTV_STAGE_NAME(eSCTVStageList eStage)
		SWITCH eStage
			CASE SCTV_STAGE_INIT
				RETURN "SCTV_STAGE_INIT"
			BREAK
			CASE SCTV_STAGE_RUNNING
				RETURN "SCTV_STAGE_RUNNING"
			BREAK
			CASE SCTV_STAGE_CLEANUP
				RETURN "SCTV_STAGE_CLEANUP"
			BREAK
		ENDSWITCH
		RETURN "INVALID_STAGE"
	ENDFUNC


	
#ENDIF

PROC SET_IM_WATCHING_A_MOCAP()
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(GlobalPlayerBD_SCTV[NATIVE_TO_INT(PLAYER_ID())].iGeneralBitSet, GPBDS_GBI_WATCHING_A_MOCAP)
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SET_IM_WATCHING_A_MOCAP")
	ENDIF
	#ENDIF
	SET_BIT(GlobalPlayerBD_SCTV[NATIVE_TO_INT(PLAYER_ID())].iGeneralBitSet, GPBDS_GBI_WATCHING_A_MOCAP)
ENDPROC
PROC CLEAR_IM_WATCHING_A_MOCAP()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(GlobalPlayerBD_SCTV[NATIVE_TO_INT(PLAYER_ID())].iGeneralBitSet, GPBDS_GBI_WATCHING_A_MOCAP)
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === CLEAR_IM_WATCHING_A_MOCAP")
	ENDIF
	#ENDIF
	CLEAR_BIT(GlobalPlayerBD_SCTV[NATIVE_TO_INT(PLAYER_ID())].iGeneralBitSet, GPBDS_GBI_WATCHING_A_MOCAP)
ENDPROC
FUNC BOOL IS_PLAYER_WATCHING_A_MOCAP(PLAYER_INDEX piPlayer)
	INT iGbd = NATIVE_TO_INT(piPlayer)
	IF iGbd != -1
		RETURN IS_BIT_SET(GlobalPlayerBD_SCTV[iGbd].iGeneralBitSet, GPBDS_GBI_WATCHING_A_MOCAP)
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Checks if the Player is a Rockstar Dev
FUNC BOOL IS_PLAYER_A_ROCKSTAR_DEV()
	//Force TRUE for Debug
	//#IF IS_DEBUG_BUILD
	//	RETURN TRUE
	//#ENDIF
	
	RETURN IS_ROCKSTAR_DEV()
ENDFUNC

//PURPOSE: If the player is in SCTV this sets SCTV to the Cleanup Stage
PROC SET_SCTV_TO_CLEANUP()
	IF IS_PLAYER_SCTV(PLAYER_ID())
		//g_b_HasEnteredGameAsSpectator[GET_ACTIVE_CHARACTER_SLOT()] = FALSE
		//SET_PLAYER_TEAM(PLAYER_ID(), TEAM_FREEMODE)
		//MPSpecGlobals.SCTVData.stage = SCTV_STAGE_CLEANUP
		//DO_SCREEN_FADE_OUT(0)//snap to black!
		SET_ENTERED_GAME_AS_SCTV(FALSE)
		SET_PLAYER_TEAM(PLAYER_ID(), TEAM_FREEMODE)
		SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_QUIT_TO_JOIN_NORMAL_SESSION)
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SCTV_BIT_QUIT_TO_JOIN_NORMAL_SESSION SET")
		DEBUG_PRINTCALLSTACK()
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SET_SCTV_TO_CLEANUP called")
	ENDIF
ENDPROC

/// PURPOSE:
///    use to check if sctv is currently ready to change camera mode
/// RETURNS:
///    true/false
FUNC BOOL IS_SAFE_TO_SWITCH_SCTV_MODE()
	IF MPSpecGlobals.SCTVData.stage = SCTV_STAGE_RUNNING
		IF MPSpecGlobals.SCTVData.switchToMode = MPSpecGlobals.SCTVData.currentMode
			IF NOT IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_CLEANUP_MODE_REQUESTED)
				IF NOT IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_INIT_MODE_REQUESTED)
					IF IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_READY_FOR_MODE_SWITCH)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    change sctv mode
/// PARAMS:
///    thisMode - mode to change to
PROC SWITCH_SCTV_MODE(eSCTVModeList thisMode)
	IF MPSpecGlobals.SCTVData.switchToMode <> thisMode
		IF thisMode <> SCTV_MODE_FREE_CAM
		OR IS_PLAYER_A_ROCKSTAR_DEV()
			MPSpecGlobals.SCTVData.switchToMode = thisMode
			MPSpecGlobals.SCTVData.modeSwitchStage = SCTV_MODE_SWITCH_STAGE_START
			CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_READY_FOR_MODE_SWITCH)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SWITCH_SCTV_MODE(", GET_SCTV_MODE_NAME(thisMode), ")")
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === NOT ROCKSTAR DEV IS TRYING TO MOVE TO FREE CAM)")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SWITCH_SCTV_MODE called using current mode : ", GET_SCTV_MODE_NAME(thisMode))
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    get the sctv current mode
/// RETURNS:
///    eSCTVModeList of the current mode
FUNC eSCTVModeList GET_SCTV_MODE()
	RETURN MPSpecGlobals.SCTVData.currentMode
ENDFUNC

/// PURPOSE:
///    Get the mode we are switching to
/// RETURNS:
///    eSCTVModeList of the current switching mode 
FUNC eSCTVModeList GET_SCTV_SWITCHING_TO_MODE()
	RETURN MPSpecGlobals.SCTVData.switchToMode
ENDFUNC


/// PURPOSE:
///    use to check if sctv should call BAIL_FROM_SCTV_FOLLOW if the local machine cannot find a suitable target
/// RETURNS:
///    true/false
FUNC BOOL SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS()
	
	MPSpecGlobals.SCTVData.bBailCalledThisFrame = TRUE
	
	#IF IS_DEBUG_BUILD
	IF MPSpecGlobals.SCTVData.bBailTimeIsUp
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS bBailTimeIsUp = TRUE")
	ENDIF
	#ENDIF
	
	IF MPSpecGlobals.SCTVData.coronaState = SCTV_CORONA_STATE_WAIT_FOR_MISSION
		IF NOT MPSpecGlobals.SCTVData.bBailTimeIsUp
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS false, sctv corona state wating for mission")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_SPECTATOR_RUNNING_CUTSCENE()
	OR IS_PLAYER_WATCHING_A_MOCAP(PLAYER_ID())
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS false, MOCAP RUNNING")
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_TRANSITION_SESSION_IS_A_SCTV_CONTROLLED_PLAYLIST()
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS false, IS_THIS_TRANSITION_SESSION_IS_A_SCTV_CONTROLLED_PLAYLIST")
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS false, IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST")
		RETURN FALSE
	ENDIF
	
	IF IS_ROCKSTAR_DEV()
	AND NETWORK_IS_ACTIVITY_SESSION()
	AND IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS false, IS_ROCKSTAR_DEV")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
	OR NETWORK_IS_IN_TRANSITION()
	OR IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
		IF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID(), FALSE)
			IF NOT g_bAllPlayersSCTV
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS false, we're on a mission and not everyone is SCTV")
				#ENDIF
				RETURN FALSE
			ENDIF
		ELSE
			IF NOT MPSpecGlobals.SCTVData.bBailTimeIsUp
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS false, is in activity or transition but not mission")
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
					
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    bail function for when SCTV can no longer spectate a target, either calls NETWORK_BAIL or moves to a different camera mode
PROC BAIL_FROM_SCTV_FOLLOW(BOOL bForceBail = FALSE)
	IF (GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM
	AND GET_SCTV_SWITCHING_TO_MODE() != SCTV_MODE_FOLLOW_TO_SESSION)
	OR bForceBail
		IF NETWORK_CAN_BAIL()
			IF IS_PLAYER_A_ROCKSTAR_DEV()
			AND NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === BAIL_FROM_SCTV_FOLLOW, SWITCH_SCTV_MODE(SCTV_MODE_FREE_CAM)")
				#ENDIF
				SWITCH_SCTV_MODE(SCTV_MODE_FREE_CAM)
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === BAIL_FROM_SCTV_FOLLOW, NETWORK_BAIL()")
				CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_SCTV_NO_OTHER_PLAYERS)")
				DEBUG_PRINTCALLSTACK()
				#ENDIF
				DO_SCREEN_FADE_OUT(0)//snap to black!
				SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_SCTV_NO_OTHER_PLAYERS)
				NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_BAIL_FROM_SCTV_FOLLOW_NO_OTHER_PLAYERS))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	FUNC STRING GET_SCTV_CORONA_STATE_DEBUG_STRING(SCTV_CORONA_STATE aSCTVCoronaState)
		SWITCH aSCTVCoronaState
			CASE SCTV_CORONA_STATE_NULL					RETURN "SCTV_CORONA_STATE_NULL"
			CASE SCTV_CORONA_STATE_IN_CORONA			RETURN "SCTV_CORONA_STATE_IN_CORONA"
			CASE SCTV_CORONA_STATE_WAIT_FOR_MISSION		RETURN "SCTV_CORONA_STATE_WAIT_FOR_MISSION"
			CASE SCTV_CORONA_STATE_WAIT_FOR_PLAYLIST	RETURN "SCTV_CORONA_STATE_WAIT_FOR_PLAYLIST"
			CASE SCTV_CORONA_STATE_CLEANUP				RETURN "SCTV_CORONA_STATE_CLEANUP"
		ENDSWITCH
		
		RETURN "UNKNOWN"
	ENDFUNC
#ENDIF

/// PURPOSE: Sets the SCTV corona state
PROC SET_SCTV_CORONA_STATE(SCTV_CORONA_STATE aSCTVCoronaState)

	CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SET_SCTV_CORONA_STATE - Changing SCTV Corona state from: ", GET_SCTV_CORONA_STATE_DEBUG_STRING(MPSpecGlobals.SCTVData.coronaState),
				" to ", GET_SCTV_CORONA_STATE_DEBUG_STRING(aSCTVCoronaState), " T(", GET_CLOUD_TIME_AS_INT(), ")")

	MPSpecGlobals.SCTVData.coronaState = aSCTVCoronaState
ENDPROC

/// PURPOSE: Returns the SCTV corona state
FUNC SCTV_CORONA_STATE GET_SCTV_CORONA_STATE()
	RETURN MPSpecGlobals.SCTVData.coronaState
ENDFUNC

/// PURPOSE:
///    Checks if the local player can set a new SCTV target (the player that was selected to join to begin spectating)
/// RETURNS:
///    true/false
FUNC BOOL CAN_SET_SCTV_FOLLOW_TARGET()
	
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
		
		IF NOT IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALREADY_CLEARED_SCTV_TARGET_THIS_JOB)//If already lost a target, allow target switch until next session
			IF NETWORK_IS_PLAYER_ACTIVE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)	//only allow changes on jobs if the current player is gone
				
				IF NOT IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_SPECTATING_TEMP_TARGET_AS_FOLLOW_TARGET_UNSUITABLE)
					
					SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_SPECTATING_TEMP_TARGET_AS_FOLLOW_TARGET_UNSUITABLE)
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_SPECTATING_TEMP_TARGET_AS_FOLLOW_TARGET_UNSUITABLE)")
					#ENDIF
					
					IF IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALREADY_CLEARED_SCTV_TARGET_THIS_JOB)
						CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALREADY_CLEARED_SCTV_TARGET_THIS_JOB)
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALREADY_CLEARED_SCTV_TARGET_THIS_JOB)")
						#ENDIF
					ENDIF
					
				ENDIF
				
				RETURN FALSE
				
			ELSE
				SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALREADY_CLEARED_SCTV_TARGET_THIS_JOB)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALREADY_CLEARED_SCTV_TARGET_THIS_JOB)")
				#ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Sets the player that the local SCTV player will follow into other sessions
/// PARAMS:
///    thisPlayer - desired new player to follow
PROC SET_SCTV_FOLLOW_TARGET(PLAYER_INDEX thisPlayer)

	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget <> thisPlayer
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget = thisPlayer
		g_iTargetPlayerArrayIndex = NATIVE_TO_INT(thisPlayer)
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SET_SCTV_FOLLOW_TARGET_PLAYER(", GET_PLAYER_NAME(thisPlayer), ")")
		#ENDIF
	ENDIF
	
	g_tl23SCTVTargetName = GET_PLAYER_NAME(thisPlayer)
	
ENDPROC

//Deal with setting the vars for the sctv ticker for DMs and missions
PROC UPDATE_SCTV_TICKER_SCORE_BASED_ON_WHO_IM_SPECTATING_DM_LEADERBOARD_STRUCT(THE_LEADERBOARD_STRUCT &sLeaderBoards[], TEXT_LABEL_63 &tParticipantNames[])
	
	//get out if not SCTV
	IF NOT IS_ROCKSTAR_DEV()
	OR NOT IS_PLAYER_SCTV(PLAYER_ID())	
		EXIT
	ENDIF
	
	//Get who we're spectating
	PLAYER_INDEX piPlayer =  NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())
	//If they are active
	IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
		INT iFirstScore
		INT iFirstPos
		TEXT_LABEL_63 sFirstName
		INT iSecondScore
		INT iSecondPos
		TEXT_LABEL_63 sSecondName
		INT iLoop
		INT iPos = -1
		BOOL bUseiPlayerScore
		//Set if we're to use the iPlayerScore VAR
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_VERSUS_MISSION)
		OR (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_LTS_MISSION)
			bUseiPlayerScore = TRUE
		ENDIF
		//If spectating player is in first
		IF sLeaderBoards[0].playerID = piPlayer
			//If it's a deathmatch
			IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
				//If it's a team death match then we use the team score
				IF g_bFM_ON_TEAM_DEATHMATCH
					iFirstScore = sLeaderBoards[0].iTeamScore
				//Not team use the player score
				ELSE
					iFirstScore = sLeaderBoards[0].iScore
				ENDIF
			//If it's a mission
			ELSE
				//If we should use the player score
				IF bUseiPlayerScore
					iFirstScore = sLeaderBoards[0].iPlayerScore
				//Use kills
				ELSE
					iFirstScore = sLeaderBoards[0].iKills
				ENDIF	
			ENDIF
			iFirstPos = 1
			//Get who is in second
			IF sLeaderBoards[1].iParticipant != -1	
				sFirstName = tParticipantNames[sLeaderBoards[0].iParticipant]			
				//If it's a deathmatch
				IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
					//If it's a team death match then we use the team score
					IF g_bFM_ON_TEAM_DEATHMATCH
						iSecondScore = sLeaderBoards[1].iTeamScore
					//Not team use the player score
					ELSE
						iSecondScore = sLeaderBoards[1].iScore
					ENDIF
				//If it's a mission
				ELSE
					//If we should use the player score
					IF bUseiPlayerScore
						iFirstScore = sLeaderBoards[1].iPlayerScore
					//Use kills
					ELSE
						iFirstScore = sLeaderBoards[1].iKills
					ENDIF	
				ENDIF
				iSecondPos = 2
				sSecondName = tParticipantNames[sLeaderBoards[1].iParticipant]
			ENDIF
		//not in first then get who is in first
		ELSE
			IF sLeaderBoards[0].iParticipant != -1
				//If it's a team death match then we use the team score
				IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
					IF g_bFM_ON_TEAM_DEATHMATCH
						iFirstScore = sLeaderBoards[0].iTeamScore
					//Not team use the player score
					ELSE
						iFirstScore = sLeaderBoards[0].iScore
					ENDIF
				//If it's a mission
				ELSE
					//If we should use the player score
					IF bUseiPlayerScore
						iFirstScore = sLeaderBoards[0].iPlayerScore
					//Use kills
					ELSE
						iFirstScore = sLeaderBoards[0].iKills
					ENDIF
				ENDIF
				iFirstPos = 1
				sFirstName = tParticipantNames[sLeaderBoards[0].iParticipant]	
			ENDIF
			//Find who we're spectating in the LB
			FOR iLoop = 0 TO (COUNT_OF(sLeaderBoards) - 1)
				IF piPlayer = sLeaderBoards[iLoop].playerID
					iPos = iLoop
					iLoop = COUNT_OF(sLeaderBoards) - 1
				ENDIF
			ENDFOR
			//If we got them then set the vars
			IF iPos != -1
				IF sLeaderBoards[iPos].iParticipant != -1
					//If it's a team death match then we use the team score
					IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
						IF g_bFM_ON_TEAM_DEATHMATCH
							iSecondScore = sLeaderBoards[iPos].iTeamScore
						//Not team use the player score
						ELSE
							iSecondScore = sLeaderBoards[iPos].iScore
						ENDIF
					//If it's a mission
					ELSE
						//If we should use the player score
						IF bUseiPlayerScore
							iSecondScore = sLeaderBoards[iPos].iPlayerScore
						//Use kills
						ELSE
							iSecondScore = sLeaderBoards[iPos].iKills
						ENDIF
					ENDIF
					iSecondPos = iPos
					sSecondName = tParticipantNames[sLeaderBoards[iPos].iParticipant]	
				ENDIF
			ENDIF		
		ENDIF		
		//Call the common set function
		MAINTAIN_SCTV_TICKER_SCORE(iFirstScore, iFirstPos, sFirstName, iSecondScore, iSecondPos, sSecondName)
	ENDIF
ENDPROC



//Deal with setting the vars for the sctv ticker for Races
PROC UPDATE_SCTV_TICKER_SCORE_BASED_ON_WHO_IM_SPECTATING_RACE_LEADERBOARD_STRUCT(THE_LEADERBOARD_STRUCT &sLeaderBoards[], TEXT_LABEL_63 &tParticipantNames[])
	
	//get out if not SCTV
	IF NOT IS_ROCKSTAR_DEV()
	OR NOT IS_PLAYER_SCTV(PLAYER_ID())	
		EXIT
	ENDIF
	
	//Get who we're spectating
	PLAYER_INDEX piPlayer =  NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())
	//If they are active
	IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
		INT iFirstScore
		INT iFirstPos
		TEXT_LABEL_63 sFirstName
		INT iSecondScore
		INT iSecondPos
		TEXT_LABEL_63 sSecondName
		INT iLoop
		INT iPos = -1
		//If spectating player is in first
		IF sLeaderBoards[0].playerID = piPlayer		
			IF g_b_On_BASEJUMP_RACE 
				iFirstScore = sLeaderBoards[0].iScore
				IF iFirstScore < 0
					iFirstScore = 0
				ENDIF
			ELSE
				iFirstScore = sLeaderBoards[0].iBestLapTime
			ENDIF
			iFirstPos = 1
			//Get who is in second
			IF sLeaderBoards[1].iParticipant != -1
				sFirstName = tParticipantNames[sLeaderBoards[0].iParticipant]			
				IF g_b_On_BASEJUMP_RACE 
					iSecondScore = sLeaderBoards[1].iScore
					IF iSecondScore < 0
						iSecondScore = 0
					ENDIF
				ELSE
					iSecondScore = sLeaderBoards[1].iBestLapTime
				ENDIF
				iSecondPos = 2
				sSecondName = tParticipantNames[sLeaderBoards[1].iParticipant]
			ENDIF
		//not in first then get who is in first
		ELSE
			IF sLeaderBoards[0].iParticipant != -1
				IF g_b_On_BASEJUMP_RACE 
					iFirstScore = sLeaderBoards[0].iScore
					IF iFirstScore < 0
						iFirstScore = 0
					ENDIF
				ELSE
					iFirstScore = sLeaderBoards[0].iBestLapTime
				ENDIF
				iFirstPos = 1
				sFirstName = tParticipantNames[sLeaderBoards[0].iParticipant]	
			ENDIF
			//Find who we're spectating in the LB
			FOR iLoop = 0 TO (COUNT_OF(sLeaderBoards) - 1)
				IF piPlayer = sLeaderBoards[iLoop].playerID
					iPos = iLoop
					iLoop = COUNT_OF(sLeaderBoards) - 1
				ENDIF
			ENDFOR
			//If we got them then set the vars
			IF iPos != -1
				IF sLeaderBoards[iPos].iParticipant != -1
					IF g_b_On_BASEJUMP_RACE 
						iSecondScore = sLeaderBoards[iPos].iScore
						IF iSecondScore < 0
							iSecondScore = 0
						ENDIF
					ELSE
						iSecondScore = sLeaderBoards[iPos].iBestLapTime
					ENDIF
					iSecondPos = (iPos + 1)
					sSecondName = tParticipantNames[sLeaderBoards[iPos].iParticipant]	
				ENDIF
			ENDIF		
		ENDIF		
		//Call the common set function
		MAINTAIN_SCTV_TICKER_SCORE(iFirstScore, iFirstPos, sFirstName, iSecondScore, iSecondPos, sSecondName)
	ENDIF
ENDPROC



//Deal with setting the vars for the sctv ticker for Horde
PROC UPDATE_SCTV_TICKER_SCORE_BASED_ON_WHO_IM_SPECTATING_HORDE_LEADERBOARD_STRUCT(THE_LEADERBOARD_STRUCT &sLeaderBoards[], TEXT_LABEL_23 &tParticipantNames[])
	
	//get out if not SCTV
	IF NOT IS_ROCKSTAR_DEV()
	OR NOT IS_PLAYER_SCTV(PLAYER_ID())	
		EXIT
	ENDIF
	
	//Get who we're spectating
	PLAYER_INDEX piPlayer =  NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())
	//If they are active
	IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
		INT iFirstScore
		INT iFirstPos
		TEXT_LABEL_23 sFirstName
		INT iSecondScore
		INT iSecondPos
		TEXT_LABEL_23 sSecondName
		INT iLoop
		INT iPos = -1
		//If spectating player is in first
		IF sLeaderBoards[0].playerID = piPlayer		
			iFirstScore = sLeaderBoards[0].iPlayerScore
			iFirstPos = 1
			//Get who is in second
			IF sLeaderBoards[1].iParticipant != -1
				sFirstName = tParticipantNames[sLeaderBoards[0].iParticipant]			
				iSecondScore = sLeaderBoards[1].iPlayerScore
				iSecondPos = 2
				sSecondName = tParticipantNames[sLeaderBoards[1].iParticipant]
			ENDIF
		//not in first then get who is in first
		ELSE
			IF sLeaderBoards[0].iParticipant != -1
				iFirstScore = sLeaderBoards[0].iPlayerScore
				iFirstPos = 1
				sFirstName = tParticipantNames[sLeaderBoards[0].iParticipant]
			ENDIF
			//Find who we're spectating in the LB
			FOR iLoop = 0 TO (COUNT_OF(sLeaderBoards) - 1)
				IF piPlayer = sLeaderBoards[iLoop].playerID
					iPos = iLoop
					iLoop = COUNT_OF(sLeaderBoards) - 1
				ENDIF
			ENDFOR
			//If we got them then set the vars
			IF iPos != -1
				IF sLeaderBoards[iPos].iParticipant != -1
					iSecondScore = sLeaderBoards[iPos].iPlayerScore
					iSecondPos = iPos
					sSecondName = tParticipantNames[sLeaderBoards[iPos].iParticipant]	
				ENDIF
			ENDIF		
		ENDIF		
		//Call the common set function
		MAINTAIN_SCTV_TICKER_SCORE(iFirstScore, iFirstPos, sFirstName, iSecondScore, iSecondPos, sSecondName)
	ENDIF
ENDPROC


//PURPOSE: Set to TRUE if a POST Celebration Screen is running and you need SCTV to fade in once it is done
PROC DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB(BOOL bSet)
	IF bSet = TRUE
		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB)
		CPRINTLN(DEBUG_SPECTATOR, " DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB - iABI_DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB - SET")
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB)
		CPRINTLN(DEBUG_SPECTATOR, " DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB - iABI_DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB - CLEARED")
	ENDIF
	DEBUG_PRINTCALLSTACK()
ENDPROC

//PURPOSE: Retursn TRUE if DO_SCTV_EXTERNAL_FADE_AFTER_POST_CELEB has been called with TRUE
FUNC BOOL IS_DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB_SET()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB)
ENDFUNC
