//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Author: 	Orlando																							//
// Team:	Online Tech																						//
// Info:	This header contains functionality for player to dance in new business battles clubs.			//
//    																										//
//    																										//
// API OVERVIEW:																							//
// * DANCE_INSTANCE_DATA : Struct used to describe instance specific data (e.g. Nightclub, Casino).         //
// * INIT_PLAYER_DANCE( PLAYER_DANCE_LOCAL_STRUCT&, DANCE_INSTANCE_DATA& ) to init or reinit after cleanup.	//
// * DANCE_MAINTAIN_UPDATE( PLAYER_DANCE_LOCAL_STRUCT&, BOOL bIsPlayerBusy) every frame.					//
// * DANCE_CLEANUP( PLAYER_DANCE_LOCAL_STRUCT& ) to clean up.												//
// * Disable debug widget creation & processing by setting. 											    //
//   PLAYER_DANCE_LOCAL_STRUCT.debug.bUseDebug = FALSE, wrapped in a #IF IS_DEBUG_BUILD check.				//
// * Logging: Search for [DANCING] in DEBUG_NET_DANCING channel and logs.									//
//																											//
//																											//
// ANIMATION RULES:																							//
// * Simon Crossan created the anims for the Nightclub. If in doubt, ask him.								//
//																											//
// * Each "style" needs a clipset and dictionary of clips with the following names:	                        //
//   | Format: intensity_horizontal_vertical                                                                //
//   | Note: These clips must all be of the same length                                                     //
//                                                                                                          //
//   HIGH_CENTER,   HIGH_CENTER_DOWN,   HIGH_CENTER_UP,													    //
//   HIGH_LEFT,     HIGH_LEFT_DOWN,     HIGH_LEFT_UP,													    //
//   HIGH_RIGHT,    HIGH_RIGHT_DOWN,    HIGH_RIGHT_UP,													    //
//   MED_CENTER,    MED_CENTER_DOWN,    MED_CENTER_UP,													    //
//   MED_LEFT,      MED_LEFT_DOWN,      MED_LEFT_UP,													    //
//   MED_RIGHT,     MED_RIGHT_DOWN,     MED_RIGHT_UP,													    //
//   LOW_CENTER,    LOW_CENTER_DOWN,    LOW_CENTER_UP,													    //
//   LOW_LEFT,      LOW_LEFT_DOWN,      LOW_LEFT_UP,													    //
//   LOW_RIGHT,     LOW_RIGHT_DOWN,     LOW_RIGHT_UP,													    //
//                                                                                                          //
//   | These clips do not need to be of any particular length:											    //
//   INTRO, OUTRO																						    //
//                                                                                                          //
// * Two "beat tags" are required on each clip denoting the first and second beat of the dance:             //
//   "FirstBeat", "SecondBeat"																			    //
//                                                                                                          //
//                                                                                                          //
// BEHAVIOUR AND CONTROLS OVERVIEW:																			//
// * Refer to the in-game help text "DANCE_S_0":                                                            //
//    			                                                                                            //
//   BUILD INTENSITY      Current intensity level is represented by pulsing rings around the 'A' graphic.   //
//                        The progress in the level is shown by the innermost yellow ring.                  //
//                        Higher intensity causes higher intensity anims to be blended in.                  //
//                        See the ANIMATION RULES section "Format: *intensity*_horizontal_vertical".        //
//                        Long periods of consecutive beat matching will reward the player with RP and      //
//                        a small stamina stat boost.                                                       //
//                                                                                                          //
//   DANCE MOVES          Blends the anims betwen vertical and horizontal clips.                            //
//                        See the ANIMATION RULES section "Format: intensity_*horizontal_vertical*".        //
//                                                                                                          //
//   HOLD INTENSITY       Slows down intensity decay for missed beats and pause the beat matching counter.  //
//																											//
//   BOOST INTENSITY      Jumps to max intensity. If also holding <HOLD INTENSITY> then jump up one tier.   //
//																											//
//   DROP INTENSITY       Jumps to min intensity. If also holding <HOLD INTENSITY> then jump down one tier. //
//																											//
//   ROTATE               Move network doesn't support rotation so ped is directly rotated. Speed is        //
//                        modulated to help make it look more natural. See COMPUTE_ROTATION_SPEED(..)       //
//																											//
//   PERFORM ACTION		  AKA Interaction, Flourish.                                                        //
//                        Tap for single "FULLBODY". Hold for looping "UPPERBODY". Release and hold during  //
//                        UPPERBODY flourish to start a fullbody.                                           //
//                        UPPERBODY played through the move network. Fullbody played through interaction    //
//                        system as standalone animation.                                                   //
//																											//
//   DANCE STYLE          Every set of dance moves is loaded in during DLS_DANCE_INIT stage so that player  //
//                        can see remote players dancing.                                                   //
//                        The style is linked to a stat that can be updated while dancing and through the   //
//                        PI menu.                                                                          //
//                        See the ANIMATION RULES section.                                                  //
//																											//
//   DANCE ACTION         AKA Interaction, Flourish.                                                        //
//                        Players usually set interactions through PI menu.                                 //
//                        This control lets players cycle through and choose from a hand-picked range.      //
//                        DANCING_IS_INTERACTION_A_DANCE_INTERACTION(..) defines the hand-picked actions.          //
//                        Any UPPERBODY action played that is not from the "cycle select" list will not     //
//                        be networked.                                                                     //
//                        | UPPERBODY CLIPS ARE LOADED IN UNTIL CLEANUP FOR EACH ACTION DEFINED IN THE      // 
//                        | CYCLE SELECT LIST.                                                              //
//                        IS_PLAYER_INTERACTION_ACTION_BLOCKED(..) defines actions which cannot ever        //
//                        be played while dancing.                                                          //
//																											//
//																											//
// NOTES:																									//
// * Try to avoid adding code specific to a particular implementation. If you find yourself writing			//
//   "IF SOME_INTERIOR()" then consider if any behaviour could be pushed out to the script file that is 	//
//   using this header. However, if it still makes sense to do it this way after thinking about it, do it.	//
//																											//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "commands_entity.sch"
USING "commands_player.sch"
USING "script_network.sch"
USING "script_maths.sch"
USING "freemode_header.sch"
USING "net_interactions.sch"
USING "net_dancing.sch"
USING "heist_island_loading.sch"
#IF FEATURE_HEIST_ISLAND_DANCES
USING "rc_helper_functions.sch"
#ENDIF

CONST_INT DANCING_FEATURE_BLEND_EVENTS 1
CONST_INT DANCING_FEATURE_DUAL_DANCE 1

TWEAK_INT ciHOLD_RHYTHM_BTN_FLASH_MS 200
CONST_INT ciINPUT_HISTORY_COUNT 75

CONST_FLOAT cfSCRIPT_INTENSITY_BUFFER 0.0 //Aditional buffer that signal can overflow into (still cap move network signal at 1.0)

TWEAK_FLOAT cfDIRECTION_ACC						13.5
TWEAK_FLOAT cfDIRECTION_EASE_OUT_START			0.3
TWEAK_FLOAT	cfDIRECTION_EASE_OUT_MODIFIER		0.5
TWEAK_FLOAT cfDIRECTION_GAIN_PER_SEC			2.0
TWEAK_FLOAT	cfDIRECTION_DECAY_PER_SEC			0.1

TWEAK_FLOAT	cfINTENSITY_EASE_OUT_START		0.150
TWEAK_FLOAT cfINTENSITY_EASE_OUT_MODIFIER	0.6

TWEAK_FLOAT	cfINTENSITY_DECAY_PER_SEC		0.65		
TWEAK_FLOAT cfINTENSITY_GAIN_PER_SEC		0.90
TWEAK_FLOAT cfINTENSITY_GAIN_BOOST_MULTIPLIER 2.20

TWEAK_INT ciDANCE_CAMERA_INTERPOLATION 			1000
TWEAK_INT ciDANCE_CAMERA_INTERPOLATION_HALF		2000

TWEAK_INT ciDANCE_BOOST_SHAKE_DURATION	65
TWEAK_INT ciDANCE_BOOST_SHAKE_FREQUENCY	240
TWEAK_INT ciDANCE_RESTART_TIME 0

// ** discrete "levels" ** 
// Level names 
CONST_INT LEVEL_1 		0 
CONST_INT LEVEL_2 		1
CONST_INT LEVEL_3 		2
CONST_INT LEVEL_4		3
CONST_INT LEVEL_COUNT 	4
// Beats per level (hits)
/* No value for level one */
CONST_INT ciLEVEL_2_BEATS_HIT 8
CONST_INT ciLEVEL_3_BEATS_HIT 8
CONST_INT ciLEVEL_4_BEATS_HIT 8
// Beats per level (misses)
/* No value for level one */
CONST_INT ciLEVEL_2_BEATS_MISS 16
CONST_INT ciLEVEL_3_BEATS_MISS 16
CONST_INT ciLEVEL_4_BEATS_MISS 16
// Beats per level (fails)
/* No value for level one */
CONST_INT ciLEVEL_2_BEATS_FAIL 8
CONST_INT ciLEVEL_3_BEATS_FAIL 8
CONST_INT ciLEVEL_4_BEATS_FAIL 8
// Min intensity to get into level (inclusive)
CONST_FLOAT cfLEVEL_1_MIN 0.00
CONST_FLOAT cfLEVEL_2_MIN 0.00 
CONST_FLOAT cfLEVEL_3_MIN 0.25
CONST_FLOAT cfLEVEL_4_MIN 0.75
// ************************

// Decay
CONST_INT	ciINTENSITY_BEATS_BEFORE_DECAY	32 	// Wait for 32 beats when holding [LT] before decaying
CONST_INT	ciINTENSITY_HOLD_COOLDOWN_BEATS	8 	// Maximum of 10 beat decay (this will take you to 0)
CONST_FLOAT	cfINTENSITY_DECAY_VALUE			0.1	// Ammount of intensity to lose per beat while decaying

CONST_INT	ciINTENSITY_FLOURISH_IMMUNITY_BEATS 4 // Four immune beats out of flourish (canceled with input)

TWEAK_INT 	ciDANCE_BREAKOUT_TIME			1500

TWEAK_FLOAT cfSHAKE_AMP_MIN 0.3
TWEAK_FLOAT cfSHAKE_AMP_MAX 1.0

ENUM DANCE_LOCAL_BITSET
	DANCE_LOCAL_BS_CONT_INPUT
	,DANCE_LOCAL_BS_IN_DANCE_AREA // In dance area?
	,DANCE_LOCAL_BS_IN_DANCE_AREA_PREV // In dance area in previous frame?
	,DANCE_LOCAL_BS_WAITING_TO_PRINT_HINT
	,DANCE_LOCAL_BS_PLAYER_IS_BUSY // True if the player is apparently busy (See dance_maintain_update_
	,DANCE_LOCAL_BS_PC_INPUT_MAP_LOADED
	,DANCE_LOCAL_BS_RADAR_HIDDEN
	,DANCE_LOCAL_BS_WAITING_TO_PRINT_COMBO_STOP
	,DANCE_LOCAL_BS_SHOW_MINIMISED_HELP
	,DANCE_LOCAL_BS_HAVE_SHOWN_COMBO_STOP
	,DANCE_LOCAL_BS_USING_KEYBOARD
	,DANCE_LOCAL_BS_REQUESTED_SCALEFORM
	,DANCE_LOCAL_BS_HOLDING_INTENSITY
	,DANCE_LOCAL_BS_LOADING_FLOURISH
	,DANCE_LOCAL_BS_USING_ALT_RHYTHM_CONTROL	// Is the player using the secondary rhythm button?
	,DANCE_LOCAL_BS_WAITING_TO_PRINT_STAIRS
	,DANCE_LOCAL_BS_SET_PCF
	,DANCE_LOCAL_BS_ALLOW_RED_METER
	,DANCE_LOCAL_BS_SHOW_CONTROLS
	,DANCE_LOCAL_BS_CHANGED_STYLE
	,DANCE_LOCAL_BS_HIDE_HELP_TEXT
	,DANCE_LOCAL_BS_HEALTH_REGEN_CHANGED
	,DANCE_LOCAL_BS_OVERRIDE_ACTION
	,DANCE_LOCAL_BS_BOOST_LEVEL_1
	,DANCE_LOCAL_BS_BOOST_LEVEL_2
	,DANCE_LOCAL_BS_DELAY_MOVEMENT
	
	#IF DANCING_FEATURE_DUAL_DANCE
	,DANCE_LOCAL_BS_TRIGGERED_DUAL_DANCE_END
	,DANCE_LOCAL_BS_DUAL_DANCE_ANIM_STARTED
	,DANCE_LOCAL_BS_STARTED_SHAPE_TEST
	,DANCE_LOCAL_BS_DISABLED_INTERACTION_MENU
	,DANCE_LOCAL_BS_SHOW_DUAL_DANCE_BLOCKED
	#ENDIF
	
ENDENUM

CONST_INT ciDANCE_MAX_HINT_COUNT 3 //Show off-floor dance hint 3 times max

CONST_FLOAT cfDANCE_MAX_BPM					140.0
CONST_FLOAT cfDANCE_BPM_REDUCTION 			0.25
CONST_FLOAT cfDANCE_MAX_SPEED_MULTIPLIER	1.2
CONST_FLOAT cfDANCE_MIN_SPEED_MULTIPLIER	0.8

TWEAK_FLOAT	cfBEAT_SYNC_FORGIVENESS_SEC 0.0001

// Values taken from "difficulty level 2". B* url:bugstar:5131951 (basically removes the fail window)
TWEAK_FLOAT cfBEAT_MATCH_WINDOW	0.5 		// Half of the relative window for successful beat matched inputs (normalised between previous & next beat)
TWEAK_FLOAT ciBEAT_HALF_MIN_ABS_WINDOW_MS 0.0 // 
TWEAK_FLOAT ciBEAT_HALF_MAX_ABS_WINDOW_MS 10000.0 // 125.0

TWEAK_FLOAT	cfPOW_TIME				1.5
TWEAK_INT ciDANCE_CONTROL_INTERP_TIME_MS 500
TWEAK_INT ciDANCE_CONTROL_BEAT_INTERP_TIME_MS 75
TWEAK_INT ciDANCE_CONTROL_HIT_R		0
TWEAK_INT ciDANCE_CONTROL_HIT_G		255
TWEAK_INT ciDANCE_CONTROL_HIT_B		0

TWEAK_INT ciDANCE_CONTROL_FAIL_R	255
TWEAK_INT ciDANCE_CONTROL_FAIL_G	0
TWEAK_INT ciDANCE_CONTROL_FAIL_B	200

TWEAK_INT ciDANCE_CONTROL_DEFAULT_R	255
TWEAK_INT ciDANCE_CONTROL_DEFAULT_G	255
TWEAK_INT ciDANCE_CONTROL_DEFAULT_B	255

TWEAK_INT ciDANCE_CONTROL_MISS_R	0
TWEAK_INT ciDANCE_CONTROL_MISS_G	0
TWEAK_INT ciDANCE_CONTROL_MISS_B	255

CONST_INT ciHOLD_BUTTON_TIME_MS		500
CONST_INT ciTIME_BEFORE_MINIMISING_HELP	15000
CONST_INT ciFLOURISH_INPUT_HOLD_TIME 500
TWEAK_FLOAT cfROTATION_SPEED 75.0 // Degrees per sec

TWEAK_INT ciBOOST_COOLDOWN_BEATS 24

TWEAK_FLOAT cfFIRST_PERSON_DANCE_CAM_PITCH_MIN_LIMIT -39.0
TWEAK_FLOAT cfFIRST_PERSON_DANCE_CAM_PITCH_MAX_LIMIT 65.0
TWEAK_FLOAT cfFIRST_PERSON_DANCE_CAM_HEADING_MIN_LIMIT -65.0
TWEAK_FLOAT cfFIRST_PERSON_DANCE_CAM_HEADING_MAX_LIMIT 65.0

#IF DANCING_FEATURE_DUAL_DANCE
TWEAK_FLOAT cfDANCING_DUAL_DANCE_FACING_ACCURACY 60.0
TWEAK_FLOAT cfDANCING_DUAL_DANCE_RAYCHECK_DIST 1.0
TWEAK_FLOAT cfDANCING_DUAL_DANCE_RAYCHECK_HEIGHT 0.15
TWEAK_FLOAT cfDANCING_DUAL_DANCE_RAYCHECK_RADIUS 0.5

TWEAK_FLOAT cfDANCING_DUAL_DANCE_IN_RANGE_FORWARD_OFFSET 0.5
TWEAK_FLOAT cfDANCING_DUAL_DANCE_IN_RANGE_RADIUS 1.0
TWEAK_FLOAT cfDANCING_DUAL_DANCE_AREA_CLEAR_CHECK_RANGE 0.5
TWEAK_FLOAT cfDANCING_DUAL_DANCE_AREA_CLEAR_CHECK_OFFSET_X	0.01044
TWEAK_FLOAT cfDANCING_DUAL_DANCE_AREA_CLEAR_CHECK_OFFSET_Y	1.39745
TWEAK_FLOAT cfDANCING_DUAL_DANCE_AREA_CLEAR_CHECK_OFFSET_Z	0.0
TWEAK_FLOAT cfDANCING_DUAL_DANCE_MOVE_TO_POSITION_ARRIVE_RANGE 0.45

ENUM DUAL_DANCE_RESTRICTION
	DDR_INVALID = -1
	,DDR_EVERYONE = 0
	,DDR_CREW = 1
	,DDR_CREW_AND_FRIENDS = 2
	,DDR_NO_ONE
	,DDR_ORGANIZATION
ENDENUM

#ENDIF

ENUM DANCE_LOCAL_STAGE
	DLS_INVALID = -1
	,DLS_CLEANUP_COMPLETE
	
	// General system
	,DLS_INIT
	,DLS_LOADING
	,DLS_IDLE_NOT_DANCING
	
	// Solo dancing
	,DLS_DANCING_INTRO
	,DLS_DANCING
	,DLS_FLOURISH	// lower body played through interaction system
	,DLS_MOVE_FLOURISH 	// upperbody played through move network
	,DLS_STOP_DANCING
	
	#IF DANCING_FEATURE_DUAL_DANCE
	,DLS_WAIT_FOR_DUAL_DANCE_START
	,DLS_DUAL_DANCE_INTRO
	,DLS_DUAL_DANCING
	,DLS_DUAL_DANCE_EXIT
	,DLS_DUAL_DANCE_WAIT_FOR_LOOP_END
	#ENDIF
	
ENDENUM

ENUM DANCE_SERVER_STAGE
	DSS_INIT,
	DSS_IDLE,
	DSS_END
ENDENUM

#IF DANCING_FEATURE_DUAL_DANCE
STRUCT DANCE_BROADCAST_DATA
	BOOL bLeadDancer // If this player determines the dance performed and creates the sync scene
	BOOL bReadyToDance
	PLAYER_INDEX piDancePartner
	INT iSyncSceneID = -1
	INT iDualDanceStyle
	TRANSFORM_STRUCT sSyncSceneOrigin	
	DANCE_LOCAL_STAGE eStage
ENDSTRUCT

#IF IS_DEBUG_BUILD
DEBUGONLY FUNC STRING DEBUG_GET_DUAL_DANCE_CLIP_AS_STRING(DUAL_DANCE_CLIP eEnum)
	SWITCH eEnum
		CASE DDC_INVALID						RETURN	"INVALID"
		CASE DDC_DUAL_DANCE_INTRO				RETURN	"INTRO"
		CASE DDC_DUAL_DANCE_IDLE				RETURN	"IDLE"
		CASE DDC_DUAL_DANCE_EXIT				RETURN	"EXIT"
		CASE DDC_DUAL_DANCE_STANDING_INTRO		RETURN	"STANDING INTRO"
		CASE DDC_DUAL_DANCE_STANDING_EXIT		RETURN	"STANDING EXIT"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_DUAL_DANCE_CLIP_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC
#ENDIF

TYPEDEF FUNC BOOL T_PREDICATE_LOOP_GET_ANIM_DICT(INT iIndex, DUAL_DANCE_ANIM &sAnimOut)
#ENDIF // DANCING_FEATURE_DUAL_DANCE

TYPEDEF FUNC BOOL T_PREDICATE_PED_IN_AREA(PED_INDEX ped)
TYPEDEF FUNC BOOL T_PREDICATE_LOOP_AREA_CHECK(INT index, PED_INDEX ped, BOOL& result)
TYPEDEF FUNC BOOL T_PREDICATE_LOOP_GET_DANCE_CLIPSET_DATA(INT index, DANCE_CLIPSET& clipset)

TYPEDEF FUNC BOOL T_IS_CINEMATIC_CAM_AVAILABLE(INT danceAreaIndex)
TYPEDEF PROC T_ON_FULL_MIN_BEAT_MATCHING(INT iTotalMins)
TYPEDEF PROC T_ACTION()

/// PURPOSE:
///    Use for T_ACTION callbacks where no action is required.
PROC NO_ACTION()
ENDPROC

/// PURPOSE:
///    Use for callbacks where no action is required.
PROC NO_ACTION_I(INT _)
	UNUSED_PARAMETER(_)
ENDPROC

/// PURPOSE:
///    Use where no area checks are required for a T_PREDICATE_LOOP_AREA_CHECK callback.
FUNC BOOL NO_AREA_CHECKS(INT index, PED_INDEX ped, BOOL& result)
	UNUSED_PARAMETER(index)
	UNUSED_PARAMETER(ped)
	result = FALSE
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Use as callback for T_IS_CINEMATIC_CAM_AVAILABLE  when cinematic cam will never be available.
FUNC BOOL NO_CINEMATIC_CAM_AVAILABLE(INT danceAreaIndex)
	UNUSED_PARAMETER(danceAreaIndex)
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Holds information specific to each player dancing instance.
///    Safe default callbacks are commented after the definitions.
///    Callbacks with no default require an implementation.
STRUCT DANCE_INSTANCE_DATA
	// Return FALSE when <index> is out of range. 
	// <result> reflects whether the <ped> is in the dance area <index>.
	T_PREDICATE_LOOP_AREA_CHECK isPlayerInDanceArea
	
	// Return FALSE when <index> is out of range. 
	// <result> reflects whether the <ped> is in the blocker area <index>.
	T_PREDICATE_LOOP_AREA_CHECK isPlayerInBlockArea // = &NO_AREA_CHECKS

	// Return FALSE when <index> is out of range. 
	// <clipset> contains info about the clipset at <index>.
	// NOTE: See top of file for animation clip rules.
	T_PREDICATE_LOOP_GET_DANCE_CLIPSET_DATA getDanceClipsetData

	// Return TRUE if the cinematic cam can be used. <danceAreaIndex> will contain the index
	// of the dance area the player is in, dictated by <isPlayerInDanceArea>.
	T_IS_CINEMATIC_CAM_AVAILABLE isCinematicCamAvailable // = &NO_CINEMATIC_CAM_AVAILABLE
	
	// Called after every full minute of player dancing regardless of 
	// whether the player is beat matching.
	T_ACTION onFullMinDancing // = &NO_ACTION
	
	// Called after every full minute of player dancing while they are maintaining perfect beat matching.
	// <iTotalMins> will contain the new number of consecutive mins the beat matching has reached.
	T_ON_FULL_MIN_BEAT_MATCHING onFullMinBeatMatching // = &NO_ACTION_I
		
	// The stat which is used to store the preferred dance style for this instance.
	// E.g. MP_STAT_NIGHTCLUBDANCSSETTING was used for the nightclub.
	MP_INT_STATS ePiMenuDanceStyleStat
	
	// The stat which is used to store how many times we have shown the dance anywhere help text.
	STATS_PACKED eDanceAnywhereCounterStat
	
	// If TRUE the player will be able to dance outside of the areas defined by 
	// <isPlayerInDanceArea> without any context intention showing up.
	BOOL bCanDanceAnywhere
	
	// will display use right stick to use boiler cam if a cinematic dance camera is active
	BOOL bShowBoilerCamOption = FALSE
	
	INT iLocationHash
	
	DANCE_CONTROL_SCHEME eInitControlScheme = DANCE_CONTROL_SCHEME_STANDARD
	
	#IF DANCING_FEATURE_DUAL_DANCE
	BOOL bCanDualDanceAnywhere = FALSE
	T_PREDICATE_LOOP_GET_ANIM_DICT getDualDanceAnimDict
	T_PREDICATE_PED_IN_AREA isPlayerInDualDanceArea
	#ENDIF
	
ENDSTRUCT

#IF IS_DEBUG_BUILD

CONST_INT ciDANCE_DIFF_HARD 	2
CONST_INT ciDANCE_DIFF_MED 		1
CONST_INT ciDANCE_DIFF_EASY 	0
CONST_INT ciDANCE_DIFF_COUNT 	3
CONST_INT ciDANCE_DIFF_DEFAULT  ciDANCE_DIFF_EASY

TWEAK_FLOAT cfDANCE_DIFF_TEXT_X 0.75
TWEAK_FLOAT cfDANCE_DIFF_TEXT_Y 0.1

TWEAK_INT ciDANCE_DIFF_TEXT_TIME_MS 4000
TWEAK_INT ciDANCE_DIFF_TEXT_FADE_MS 3000

ENUM DANCE_WIDGET_STAGE
	DWS_INIT,
	DWS_UPDATE
ENDENUM

STRUCT PLAYER_DANCE_WIDGET_STRUCT
	DANCE_WIDGET_STAGE 	eWidgetStage 		
	BOOL				bRefresh 			
	BOOL 				bUseDebug = TRUE
	BOOL				bOverrideSignals
	FLOAT 				fOverrideIntensity = 1.0
	FLOAT				fOverrideDirectionX = 0.5
	FLOAT				fOverrideDirectionY = 0.5
	FLOAT				fOverrideSpeed = 1.0
	BOOL				bDoDebugDrawing 
	INT 				iDanceStage
	BOOL				bPrintBs
	BOOL				bPrintBsOnce
	INT					iForgivenessThisFrame
	INT					iInputTime
	BOOL				bInputHit
	INT 				iDanceArea
	INT 				iDominantClipIndex
	INT					iBlockArea
	INT					iComboResetTimer
	INT					iLifetimeComboDurationMins
	INT					iComboCountToday
	INT 				iComboDurationMs
	
	BOOL				bAutoBeatMatch
	BOOL				bResetComboStats
	BOOL				bUpdateStats
	BOOL				bClearHaveDancedStat
	BOOL				bResetHintCounterStat
	SCRIPT_TIMER		tDifficultyTextTimer
	INT					iDifficulty
	
	FLOAT fIntensityBeforeChange
	FLOAT fLastIntensityChange
	
	#IF DANCING_FEATURE_DUAL_DANCE
	BOOL bDrawBroadcastData
	BOOL bDualDanceWithBot
	PED_INDEX pedDanceBot
	INT iBlendInOverrides[COUNT_OF(DUAL_DANCE_CLIP)][2] // Two peds multiple clips
	INT iBlendOutOverrides[COUNT_OF(DUAL_DANCE_CLIP)][2] // Two peds multiple clips
	#ENDIF
ENDSTRUCT
#ENDIF //IS_DEBUG_BUILD

ENUM DANCE_CONTEXT_INTENTION
	DCI_INVALID
	,DCI_SOLO
	,DCI_SOLO_ANYWHERE
	,DCI_SOLO_STOP_ANYWHERE
	,DCI_SOLO_STOP_ANYWHERE_DRUNK
	,DCI_HIDE_CONTROLS
	,DCI_NO_MORE_COMBO
	
	#IF DANCING_FEATURE_DUAL_DANCE
	,DCI_DUAL_JOIN
	,DCI_DUAL_CONTROLS
	,DCI_DUAL_LEAD_CONTROLS
	#ENDIF
ENDENUM

STRUCT DANCE_CLIP_DATUM
	FLOAT fFirstBeatPhase
	FLOAT fBeatPhaseDiff
	FLOAT fBps
ENDSTRUCT

ENUM DANCE_CLIP
	// dc_move_dance_intensity_horizontal_vetical
	DC_MOVE_DANCE_HIGH_CENTER_CENTER
	,DC_MOVE_DANCE_HIGH_CENTER_DOWN
	,DC_MOVE_DANCE_HIGH_CENTER_UP
	
	,DC_MOVE_DANCE_HIGH_LEFT_CENTER
	,DC_MOVE_DANCE_HIGH_LEFT_DOWN
	,DC_MOVE_DANCE_HIGH_LEFT_UP
		
	,DC_MOVE_DANCE_HIGH_RIGHT_CENTER
	,DC_MOVE_DANCE_HIGH_RIGHT_DOWN
	,DC_MOVE_DANCE_HIGH_RIGHT_UP
	
	,DC_MOVE_DANCE_MED_CENTER_CENTER
	,DC_MOVE_DANCE_MED_CENTER_DOWN
	,DC_MOVE_DANCE_MED_CENTER_UP
	
	,DC_MOVE_DANCE_MED_LEFT_CENTER
	,DC_MOVE_DANCE_MED_LEFT_DOWN
	,DC_MOVE_DANCE_MED_LEFT_UP
		
	,DC_MOVE_DANCE_MED_RIGHT_CENTER
	,DC_MOVE_DANCE_MED_RIGHT_DOWN
	,DC_MOVE_DANCE_MED_RIGHT_UP
	
	,DC_MOVE_DANCE_LOW_CENTER_CENTER
	,DC_MOVE_DANCE_LOW_CENTER_DOWN
	,DC_MOVE_DANCE_LOW_CENTER_UP
	
	,DC_MOVE_DANCE_LOW_LEFT_CENTER
	,DC_MOVE_DANCE_LOW_LEFT_DOWN
	,DC_MOVE_DANCE_LOW_LEFT_UP
		
	,DC_MOVE_DANCE_LOW_RIGHT_CENTER
	,DC_MOVE_DANCE_LOW_RIGHT_DOWN
	,DC_MOVE_DANCE_LOW_RIGHT_UP
	
ENDENUM

/// PURPOSE: Get an animation clip name
FUNC STRING GET_DANCE_CLIP_STRING(DANCE_CLIP eClip)
	SWITCH eClip		
		CASE DC_MOVE_DANCE_HIGH_CENTER_CENTER RETURN "HIGH_CENTER"
		CASE DC_MOVE_DANCE_HIGH_CENTER_DOWN RETURN "HIGH_CENTER_DOWN"
		CASE DC_MOVE_DANCE_HIGH_CENTER_UP RETURN "HIGH_CENTER_UP"

		CASE DC_MOVE_DANCE_HIGH_LEFT_CENTER RETURN "HIGH_LEFT"
		CASE DC_MOVE_DANCE_HIGH_LEFT_DOWN RETURN "HIGH_LEFT_DOWN"
		CASE DC_MOVE_DANCE_HIGH_LEFT_UP RETURN "HIGH_LEFT_UP"
		
		CASE DC_MOVE_DANCE_HIGH_RIGHT_CENTER RETURN "HIGH_RIGHT"
		CASE DC_MOVE_DANCE_HIGH_RIGHT_DOWN RETURN "HIGH_RIGHT_DOWN"
		CASE DC_MOVE_DANCE_HIGH_RIGHT_UP RETURN "HIGH_RIGHT_UP"

		CASE DC_MOVE_DANCE_MED_CENTER_CENTER RETURN "MED_CENTER"
		CASE DC_MOVE_DANCE_MED_CENTER_DOWN RETURN "MED_CENTER_DOWN"
		CASE DC_MOVE_DANCE_MED_CENTER_UP RETURN "MED_CENTER_UP"

		CASE DC_MOVE_DANCE_MED_LEFT_CENTER RETURN "MED_LEFT"
		CASE DC_MOVE_DANCE_MED_LEFT_DOWN RETURN "MED_LEFT_DOWN"
		CASE DC_MOVE_DANCE_MED_LEFT_UP RETURN "MED_LEFT_UP"
		
		CASE DC_MOVE_DANCE_MED_RIGHT_CENTER RETURN "MED_RIGHT"
		CASE DC_MOVE_DANCE_MED_RIGHT_DOWN RETURN "MED_RIGHT_DOWN"
		CASE DC_MOVE_DANCE_MED_RIGHT_UP RETURN "MED_RIGHT_UP"

		CASE DC_MOVE_DANCE_LOW_CENTER_CENTER RETURN "LOW_CENTER"
		CASE DC_MOVE_DANCE_LOW_CENTER_DOWN RETURN "LOW_CENTER_DOWN"
		CASE DC_MOVE_DANCE_LOW_CENTER_UP RETURN "LOW_CENTER_UP"

		CASE DC_MOVE_DANCE_LOW_LEFT_CENTER RETURN "LOW_LEFT"
		CASE DC_MOVE_DANCE_LOW_LEFT_DOWN RETURN "LOW_LEFT_DOWN"
		CASE DC_MOVE_DANCE_LOW_LEFT_UP RETURN "LOW_LEFT_UP"
		
		CASE DC_MOVE_DANCE_LOW_RIGHT_CENTER RETURN "LOW_RIGHT"
		CASE DC_MOVE_DANCE_LOW_RIGHT_DOWN RETURN "LOW_RIGHT_DOWN"
		CASE DC_MOVE_DANCE_LOW_RIGHT_UP RETURN "LOW_RIGHT_UP"
		
		DEFAULT RETURN  "UNDEFINED"
	ENDSWITCH
ENDFUNC

STRUCT CACHED_DANCE_CLIP_DATA
	DANCE_CLIP_DATUM	soloDanceData[COUNT_OF(DANCE_CLIP)]
	FLOAT				fSoloDanceLength // All of the clips should be uniform length.
ENDSTRUCT

STRUCT COLOUR_RGB
	INT r
	INT g
	INT b
ENDSTRUCT

STRUCT DANCE_CONTROL_LIGHTS
	COLOUR_RGB		currentColour
	COLOUR_RGB 		endColour
	COLOUR_RGB 		startColour
	INT 			iDuration	// how long should the transition take?
	SCRIPT_TIMER	tColourChangeTime				// Last colour change
ENDSTRUCT

STRUCT DANCE_STATS_STRUCT
	INT iConsecutiveBeatHits
	INT iConsecutiveBeatMissFails
	SCRIPT_TIMER tFirstHitTime // Time of first hit for consecutive hit sequence 
	SCRIPT_TIMER tFirstHitPauseTime // Timer used to pause the tFirstHitTime timer
	INT iComboMins // How many times have we rewarded the player RP since starting a hit sequence?
	INT iRpPeriodCount // how many "Rp reward periods" have passed? (default period = 1 min, linked to tunable)
	
	SCRIPT_TIMER fullMinDanceTimer // Loops 0->1min, gain award on loop
	SCRIPT_TIMER tLastDanceInputTime // resets fullMinDanceTimer if expires (no input for x seconds)
ENDSTRUCT

STRUCT DANCE_AUDIO_DATA
	FLOAT 	fSecondsToNextBeat
	FLOAT 	fBpm
	INT		iTotalBeatCount	
	INT		iPrevBeatTime 	
	INT 	iBeatIndex		
	BOOL	bBeatThisFrame
ENDSTRUCT

ENUM FLOURISH_CLIP
	FLC_FULLBODY,
	FLC_UPPER_OUTRO // Used to track which anims are playing on the ped.
ENDENUM

ENUM FLOURISH_DICT
	FLD_FULLBODY,
	FLD_UPPERBODY
ENDENUM

STRUCT DANCE_INTERACTION_DATA
	TEXT_LABEL_63 txtDicts[COUNT_OF(FLOURISH_DICT)]
	FLOURISH_DICT eDict
	
	TEXT_LABEL_63 txtClips[COUNT_OF(FLOURISH_CLIP)]
	FLOURISH_CLIP eExitClip
	
	INT iActionType = 0
	INT iActionAnim = 0
	
	BOOL bStartedLoad = FALSE
	
	#IF FEATURE_HEIST_ISLAND_DANCES
	OBJECT_INDEX objModel = NULL
	MODEL_NAMES ePropModel = DUMMY_MODEL_FOR_SCRIPT
	BOOL bAttachPropModelToRightHand = FALSE
	BOOL bAttachPropModelToLeftHand = FALSE
	#ENDIF

	// Move flourish data
	INT iState = 0 // Move network flourish state
ENDSTRUCT

STRUCT FLOURISH_INPUT_STRUCT
	FLOAT fFlourishTriggerFT = 0.0	//Frame time incremented between pressing and releasing the flourish button
	BOOL bInputReleased
ENDSTRUCT

STRUCT DANCE_SCALEFORM
	SCALEFORM_INDEX 	scaleform
	INT					iIntensity
	FLOAT 				fMeterValue	// [0, 1]
ENDSTRUCT	

ENUM DANCE_BEAT_MATCH_STATE
	DBMS_DEAFULT,
	DBMS_HOLD
ENDENUM

STRUCT VECTOR2
	FLOAT x
	FLOAT y
ENDSTRUCT

FUNC VECTOR VECTOR2_TO_3(VECTOR2 vec2)
	RETURN <<vec2.x, vec2.y, 0.0>>
ENDFUNC

FUNC VECTOR2 VECTOR3_TO_2(VECTOR vec3)
	VECTOR2 vec2
	vec2.x = vec3.x
	vec2.y = vec3.y
	RETURN vec2
ENDFUNC

STRUCT STICK_INPUT_HISTORY
	VECTOR2			v2Inputs[ciINPUT_HISTORY_COUNT]
	INT				iTimeSinceLastWriteMs[ciINPUT_HISTORY_COUNT] // Could get away with packing this.
	INT				iRead
	INT				iWrite
	SCRIPT_TIMER 	tLastWriteTime
ENDSTRUCT

/// PURPOSE:
///	min - inclusive
/// max - exclusive
FUNC INT LOOP_INT_ONE(INT x, INT min, INT max, BOOL bForwards)
	IF bForwards 
		x++
		IF x >= max 
			x = min
		ENDIF
	ELSE
		IF x <= min 
			x = max
		ENDIF
		x--
	ENDIF
	RETURN x
ENDFUNC

PROC HISTORY_RESET(STICK_INPUT_HISTORY& ref_data)
	// Stick poisitions need to be normalised between 0-1
	VECTOR2 vec2
	vec2.x = 0.5
	vec2.y = 0.5
	
	INT i
	REPEAT ciINPUT_HISTORY_COUNT i
		ref_data.v2Inputs[i] = vec2 			
		ref_data.iTimeSinceLastWriteMs[i] = 0	
	ENDREPEAT

	ref_data.iRead = (ciINPUT_HISTORY_COUNT - 1)
	ref_data.iWrite = 0
	
	RESET_NET_TIMER(ref_data.tLastWriteTime)
ENDPROC

PROC HISTORY_WRITE_NEW_INPUT(STICK_INPUT_HISTORY& ref_data, FLOAT x, FLOAT y)
	// NOTE : if the write head catches up with read the current behaviour
	// is effectively "start reading from front of queue again".
	VECTOR2 v2NewInput
	v2NewInput.x = x
	v2NewInput.y = y

	ref_data.v2Inputs[ref_data.iWrite] = v2NewInput
	ref_data.iTimeSinceLastWriteMs[ref_data.iWrite] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_data.tLastWriteTime)
	
	ref_data.iWrite = LOOP_INT_ONE(ref_data.iWrite, 0, ciINPUT_HISTORY_COUNT, TRUE)
	REINIT_NET_TIMER(ref_data.tLastWriteTime)
ENDPROC

FUNC INT HISTORY_ONE_BEHIND_WRITE_INDEX(STICK_INPUT_HISTORY& ref_data)
	RETURN LOOP_INT_ONE(ref_data.iWrite, 0, ciINPUT_HISTORY_COUNT, FALSE)
ENDFUNC

FUNC VECTOR HISTORY_READ_NEWEST(STICK_INPUT_HISTORY& ref_data)
	RETURN VECTOR2_TO_3(ref_data.v2Inputs[HISTORY_ONE_BEHIND_WRITE_INDEX(ref_data)])
ENDFUNC

FUNC VECTOR HISTORY_READ(STICK_INPUT_HISTORY& ref_data)
	RETURN VECTOR2_TO_3(ref_data.v2Inputs[ref_data.iRead])
ENDFUNC

PROC HISTORY_MOVE_READ_FORWARD(STICK_INPUT_HISTORY& ref_data)
	ref_data.iRead = LOOP_INT_ONE(ref_data.iRead, 0, ciINPUT_HISTORY_COUNT, TRUE)
ENDPROC

FUNC VECTOR HISTORY_POP_READ(STICK_INPUT_HISTORY& ref_data)
	ref_data.iRead = LOOP_INT_ONE(ref_data.iRead, 0, ciINPUT_HISTORY_COUNT, TRUE)
	RETURN HISTORY_READ(ref_data)
ENDFUNC

TWEAK_INT ciHISTORY_SPAM_TIME_MS 300
TWEAK_FLOAT cfHISTORY_SPAM_SPEED 0.75

FUNC BOOL HISTORY_IS_PLAYER_SPAMMING(STICK_INPUT_HISTORY& ref_data)
	FLOAT fTotalDistance = 0
	INT fTotalTimeMs = 0
	
	INT i = HISTORY_ONE_BEHIND_WRITE_INDEX(ref_data)
	VECTOR v3Prev = VECTOR2_TO_3(ref_data.v2Inputs[i])
	
	INT iCount = 0
	WHILE (iCount < ciINPUT_HISTORY_COUNT) AND (fTotalTimeMs < ciHISTORY_SPAM_TIME_MS)
		VECTOR2 v2Input = ref_data.v2Inputs[i]
		VECTOR v3Input = <<v2Input.x, v2Input.y, 0.0>>	
		
		fTotalDistance += VMAG(v3Input - v3Prev)
		fTotalTimeMs += ref_data.iTimeSinceLastWriteMs[i]

		i = LOOP_INT_ONE(i, 0, ciINPUT_HISTORY_COUNT, FALSE)
		v3Prev = v3Input
		iCount++
	ENDWHILE
	
	IF fTotalTimeMs <= 0
		RETURN FALSE
	ENDIF
	
	FLOAT fAvgSpeed = fTotalDistance / (fTotalTimeMs / 1000.0)
	RETURN fAvgSpeed > cfHISTORY_SPAM_SPEED
ENDFUNC

STRUCT DANCE_FACE_DATA
	SCRIPT_TIMER tFaceAnimStart
	INT iFaceAnimDurationMs
	NIGHTCLUB_AUDIO_TAGS eAnimIntensity
	
	#IF IS_DEBUG_BUILD
	BOOL bForceNewRandAnim
	BOOL bBlock
	INT iRandomOverride
	#ENDIF
ENDSTRUCT

STRUCT REMOTE_ACTION_DATA 
	INT iPlayerActions[NUM_NETWORK_PLAYERS]
ENDSTRUCT

STRUCT PLAYER_DANCE_LOCAL_STRUCT
	INT 				iDanceStyle
	INT					iNewDanceStyle
	DANCE_LOCAL_STAGE	eStage			// Script stage
	DANCE_BEAT_MATCH_STATE	eBeatMatchState // Beat match stage
	
	#IF DANCING_FEATURE_DUAL_DANCE
	DANCE_BROADCAST_DATA playerBD[NUM_NETWORK_PLAYERS]
	#ENDIF
	
	BOOL				bDoneHelpSound
	INT 				iBs				// Bitset DANCE_LOCAL_BS_
	DANCE_CONTEXT_INTENTION	eContext
	INT 				iContext
	
	INT					iBlockAreaStagger
	
	FLOAT 				fCurrentIntensitySignal
	
	VECTOR2				v2DirectionSignals
	STICK_INPUT_HISTORY	inputHistory
	
	FLOAT				fTrueIntensitySignal
	FLOAT 				fPhaseSignal					
	FLOAT				fSpeedSignal 					// Make sure we set local speed for remote player peds to 1 (or proccess this too). 
	FLOAT				fXSignalVelocity	
	FLOAT				fYSignalVelocity			
	
	FLOAT				fTargetHeading					// Target Heading for rotation
	
	BOOL				bStartMoveNetwork
	BOOL				bWaitForIntro					// Has the move network just started?
	BOOL 				bHaveStartedDance				// Have we fully started the solo dance move network?
	BOOL				bShouldMoveToDance				// Should we fully start the solo dance move network?
	INT					iStopDancingTime 				// Time that we stopped dancing - used to know when is safe to break out of anim (ms)
	
	SCRIPT_TIMER		tLeftDanceFloorTime				// Time that the player left the dance floor
	SCRIPT_TIMER		tContextHoldTime				// Time that the context button was pressed. Only valid while holding ~INPUT_CONTEXT~.
	SCRIPT_TIMER		tHideHelpTimer					// Timer used to track when we should hide the help text on first dance.
	SCRIPT_TIMER		tComboStopHelp					// Show combo stop help for some time before showing controls (or minimised controls) help
	SCRIPT_TIMER		tFlourishStartTime					
	SCRIPT_TIMER		tlDanceTimer
	SCRIPT_TIMER		tRestartTime
	SCRIPT_TIMER		tHoldRhythmButtonDisplay
	SCRIPT_TIMER		tRPDurationMinute
	
	#IF DANCING_FEATURE_DUAL_DANCE
	SCRIPT_TIMER		sStateBailTimer
	SHAPETEST_INDEX iDancePosShapeTestID
	PLAYER_INDEX piDualDanceTarget	
	INT iPlayerPassedPosCheck
	VECTOR vWalkToTarget
	#ENDIF
	
	INT					iBeatMatchImmunityLastBeat 		// The beat count value after which we will stop giving the player immunity to beat match misses
	BOOL				bPulseIconOnImmunity			// Should flash the icon while immune?	
	
	BOOL				bApplyBoostMultipler
	
	BOOL				bDoneInitialHelpClear			// Need to clear help text once after we have been given context priority
	BOOL				bAcceptedInputForNextBeat
	BOOL				bAcceptedInputForPrevBeat = TRUE

	BOOL				bUsingRhythmButtonAsHold		// Are we using the rhythm button as the "hold" button?
	
	FLOAT				fIntensitySignalMax
	FLOAT				fCamShakeAtReset
	
	BOOL bShouldShowDanceCamHelp = FALSE
	
	DANCE_SCALEFORM	        display
	
	CACHED_DANCE_CLIP_DATA	clipData			
	DANCE_CONTROL_LIGHTS 	lights
	DANCE_STATS_STRUCT 		stats
	DANCE_AUDIO_DATA 		audio // Cache "next audible beat" data
	DANCE_FACE_DATA			faceAnim
	INT 					iDanceAreaIndex
	
	DANCE_INTERACTION_DATA  flourish
	FLOURISH_INPUT_STRUCT   flourishInput	// Helper for managing flourish inputs [LS] + [RS]
	REMOTE_ACTION_DATA      remoteActions
	
	INT iBeatCountSinceLastBoost
	
	INT					iComplexDanceHelpNum = -1
	SCRIPT_TIMER		tComplexDanceHelpDisplay
	
	INT	iSavedActionAnim // Action saved prior to dancing
	INT iSavedActionType // 
	
	SCRIPT_TIMER tUpperToFullFlourishInput
	
	DANCE_INSTANCE_DATA 	instance
	
	DANCE_CONTROL_SCHEME	eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
	
	BOOL					bDancingLocked = FALSE
	BOOL					bBoostLocked = FALSE
	BOOL 					bAltIntensities = FALSE
	BOOL					bAltBoost = TRUE
	
	SCRIPT_TIMER			stBoostDelayTimer
	SCRIPT_TIMER			stBoostTimer
	SCRIPT_TIMER			stBoostSecondaryTimer
	SCRIPT_TIMER			stMovementDelayTimer
	
	FLOAT fLevel1 = 0.10
	FLOAT fLevel2 = 0.35
	FLOAT fLevel3 = 0.60
	FLOAT fLevel4 = 0.85
	FLOAT fLevel5 = 1.00
	
	PLAYER_INDEX piLocalPlayer
	INT iLocalPlayer
	PED_INDEX pedLocalPlayer
	BOOL bLocalPlayerOk
	BOOL bLocalPlayerPedAlive
	
	#IF IS_DEBUG_BUILD
	BOOL bUsingDanceBot = FALSE
	
	INT iIntensityOverride = 0
	PLAYER_DANCE_WIDGET_STRUCT debug
	#ENDIF
ENDSTRUCT	



#IF IS_DEBUG_BUILD
FUNC STRING GET_DANCE_LOCAL_STAGE_STRING(DANCE_LOCAL_STAGE eStage)
	SWITCH eStage
		CASE DLS_INIT 				RETURN "DLS_INIT"
		CASE DLS_LOADING			RETURN "DLS_LOADING"
		CASE DLS_IDLE_NOT_DANCING 	RETURN "DLS_IDLE_NOT_DANCING"
		CASE DLS_DANCING_INTRO		RETURN "DLS_DANCING_INTRO"
		CASE DLS_DANCING 			RETURN "DLS_DANCING"
		CASE DLS_FLOURISH			RETURN "DLS_FLOURISH"
		CASE DLS_MOVE_FLOURISH		RETURN "DLS_MOVE_FLOURISH"
		CASE DLS_STOP_DANCING		RETURN "DLS_STOP_DANCING"
		CASE DLS_CLEANUP_COMPLETE	RETURN "DLS_CLEANUP_COMPLETE"
		
		#IF DANCING_FEATURE_DUAL_DANCE
		CASE DLS_WAIT_FOR_DUAL_DANCE_START 	RETURN "DLS_WAIT_FOR_DUAL_DANCE_START"
		CASE DLS_DUAL_DANCE_INTRO			RETURN "DLS_DUAL_DANCE_INTRO"
		CASE DLS_DUAL_DANCING				RETURN "DLS_DUAL_DANCING"
		CASE DLS_DUAL_DANCE_EXIT			RETURN "DLS_DUAL_DANCE_EXIT"
		CASE DLS_DUAL_DANCE_WAIT_FOR_LOOP_END 	RETURN "DLS_DUAL_DANCE_WAIT_FOR_LOOP_END"
		#ENDIF
		
	ENDSWITCH
	RETURN "UNDEFINED"
ENDFUNC
#ENDIF //IS_DEBUG_BUILD

PROC DANCING_BLOCK_COMBAT_AND_VEHICLE_CONTROLS_THIS_FRAME()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	
	HOLSTER_WEAPON_IN_SIMPLE_INTERIOR()

    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_RIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UP)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BIKE_WINGS)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_HOLD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
ENDPROC

PROC DANCE_SET_STAGE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_LOCAL_STAGE eNewStage)
	PRINTLN("[DANCING] Setting LOCAL stage from ", GET_DANCE_LOCAL_STAGE_STRING(ref_local.eStage)," to ", GET_DANCE_LOCAL_STAGE_STRING(eNewStage))
	ref_local.eStage = eNewStage
	
	#IF DANCING_FEATURE_DUAL_DANCE
	ref_local.playerBD[ref_local.iLocalPlayer].eStage = ref_local.eStage
	PRINTLN("[DANCING][DUAL] DANCE_SET_STAGE - playerBD eStage: ", GET_DANCE_LOCAL_STAGE_STRING(ref_local.eStage))
	RESET_NET_TIMER(ref_local.sStateBailTimer)
	#ENDIF
ENDPROC

FUNC BOOL DANCE_HAS_SCALEFORM_LOADED(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF NOT IS_BIT_SET_ENUM(ref_local.iBs,DANCE_LOCAL_BS_REQUESTED_SCALEFORM)
		CDEBUG1LN(DEBUG_NET_DANCING,"[DANCING] Start loading dance scaleform")
		ref_local.display.scaleform = REQUEST_SCALEFORM_MOVIE("DANCER")
		SET_BIT_ENUM(ref_local.iBs,DANCE_LOCAL_BS_REQUESTED_SCALEFORM)
	ENDIF
	
	RETURN HAS_SCALEFORM_MOVIE_LOADED(ref_local.display.scaleform)
ENDFUNC

PROC DANCE_UNLOAD_SCALEFORM(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF IS_BIT_SET_ENUM(ref_local.iBs,DANCE_LOCAL_BS_REQUESTED_SCALEFORM)
		CDEBUG1LN(DEBUG_NET_DANCING,"[DANCING] Unloading dance scaleform")
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(ref_local.display.scaleform)
		CLEAR_BIT_ENUM(ref_local.iBs,DANCE_LOCAL_BS_REQUESTED_SCALEFORM)
	ENDIF
ENDPROC

PROC DANCING_ENABLE_INTERACTION_MENU(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bEnable)
	IF bEnable
		IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DISABLED_INTERACTION_MENU)
			ENABLE_INTERACTION_MENU()
			CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DISABLED_INTERACTION_MENU)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DISABLED_INTERACTION_MENU)
			DISABLE_INTERACTION_MENU()
			SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DISABLED_INTERACTION_MENU)
		ENDIF
	ENDIF
ENDPROC


#IF DANCING_FEATURE_DUAL_DANCE
FUNC BOOL DANCING_ARE_DUAL_DANCES_AVAILABLE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF ref_local.instance.getDualDanceAnimDict = NULL
		RETURN FALSE
	ENDIF
	
	// Can't dance anywhere and there is no dedicated spot for it
	IF ref_local.instance.isPlayerInDualDanceArea = NULL
	AND NOT ref_local.instance.bCanDanceAnywhere
		RETURN FALSE
	ENDIF
	
	IF g_SpawnData.bPassedOutDrunk
	OR IS_PED_RAGDOLL(ref_local.pedLocalPlayer)
	OR Get_Peds_Drunk_Alcohol_Hit_Count(ref_local.pedLocalPlayer) >= 10
	OR IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD[ref_local.iLocalPlayer].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_USING_BAR)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC DANCING_SET_LOCAL_PLAYERS_DANCE_PARTNER(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piDancePartner)
	ref_local.playerBD[ref_local.iLocalPlayer].piDancePartner = piDancePartner
ENDPROC

PROC DANCING_SET_LOCAL_PLAYER_DUAL_DANCE_LEAD(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bIsLead)
	ref_local.playerBD[ref_local.iLocalPlayer].bLeadDancer = bIsLead
ENDPROC

PROC DANCING_SET_LOCAL_PLAYER_READY_FOR_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bReady)
	ref_local.playerBD[ref_local.iLocalPlayer].bReadyToDance = bReady
ENDPROC

PROC DANCING_SET_LOCAL_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT iSyncSceneId)
	ref_local.playerBD[ref_local.iLocalPlayer].iSyncSceneID = iSyncSceneId
ENDPROC

FUNC BOOL DANCING_IS_PLAYER_READY_FOR_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer)
	RETURN ref_local.playerBD[NATIVE_TO_INT(piPlayer)].bReadyToDance
ENDFUNC

FUNC BOOL DANCING_IS_LOCAL_PLAYER_READY_FOR_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN DANCING_IS_PLAYER_READY_FOR_DUAL_DANCE(ref_local, ref_local.piLocalPlayer)
ENDFUNC

FUNC PLAYER_INDEX DANCING_GET_PLAYERS_DANCE_PARTNER(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer)
	RETURN ref_local.playerBD[NATIVE_TO_INT(piPlayer)].piDancePartner
ENDFUNC

FUNC PLAYER_INDEX DANCING_GET_LOCAL_PLAYERS_DANCE_PARTNER(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF ref_local.piLocalPlayer != INVALID_PLAYER_INDEX()
		RETURN DANCING_GET_PLAYERS_DANCE_PARTNER(ref_local, ref_local.piLocalPlayer)
	ENDIF
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC DANCE_LOCAL_STAGE DANCING_GET_PLAYERS_DANCE_STAGE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer)
	RETURN ref_local.playerBD[NATIVE_TO_INT(piPlayer)].eStage
ENDFUNC

FUNC DANCE_LOCAL_STAGE DANCING_GET_DANCE_PARTNERS_DANCE_STAGE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	#IF IS_DEBUG_BUILD
	IF ref_local.bUsingDanceBot
		PRINTLN("[DANCING] DANCING_GET_DANCE_PARTNERS_DANCE_STAGE - Using dance bot!")
		RETURN DLS_INVALID
	ENDIF	
	#ENDIF
	
	PLAYER_INDEX piPartner = DANCING_GET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local)
	RETURN DANCING_GET_PLAYERS_DANCE_STAGE(ref_local, piPartner)
ENDFUNC

PROC DANCING_SET_PLAYERS_SYNC_SCENE_TRANSFORM(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer, VECTOR vPos, VECTOR vRot)
	INT iPlayerIndex = NATIVE_TO_INT(piPlayer)
	ref_local.playerBD[iPlayerIndex].sSyncSceneOrigin.Position = vPos
	ref_local.playerBD[iPlayerIndex].sSyncSceneOrigin.Rotation = vRot
ENDPROC

FUNC TRANSFORM_STRUCT DANCING_GET_PLAYERS_SYNC_SCENE_TRANSFORM(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer)
	RETURN ref_local.playerBD[NATIVE_TO_INT(piPlayer)].sSyncSceneOrigin
ENDFUNC

FUNC BOOL DANCING_IS_PED_IN_DUAL_DANCE_AREA(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PED_INDEX piPed)
	IF ref_local.instance.isPlayerInDualDanceArea = NULL
		RETURN FALSE
	ENDIF
	
	RETURN CALL ref_local.instance.isPlayerInDualDanceArea(piPed)
ENDFUNC

FUNC BOOL DANCING_IS_PLAYER_IN_DUAL_DANCE_AREA(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer)
	RETURN DANCING_IS_PED_IN_DUAL_DANCE_AREA(ref_local, GET_PLAYER_PED(piPlayer))
ENDFUNC

FUNC BOOL DANCING_IS_LOCAL_PLAYER_IN_DUAL_DANCE_AREA(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN DANCING_IS_PLAYER_IN_DUAL_DANCE_AREA(ref_local, ref_local.piLocalPlayer)
ENDFUNC

FUNC BOOL DANCING_DOES_PLAYER_HAVE_DANCE_PARTNER(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer)
	RETURN ref_local.playerBD[NATIVE_TO_INT(piPlayer)].piDancePartner != NULL 
		AND ref_local.playerBD[NATIVE_TO_INT(piPlayer)].piDancePartner != INVALID_PLAYER_INDEX()
ENDFUNC

FUNC BOOL DANCING_IS_DUAL_DANCING_AVAILABLE_TO_PLAYER(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer)
	IF NOT DANCING_ARE_DUAL_DANCES_AVAILABLE(ref_local)
		RETURN FALSE
	ENDIF
	
	RETURN DANCING_IS_PLAYER_IN_DUAL_DANCE_AREA(ref_local, piPlayer) OR ref_local.instance.bCanDualDanceAnywhere
ENDFUNC

FUNC BOOL DANCING_IS_DUAL_DANCING_AVAILABLE_TO_LOCAL_PLAYER(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN DANCING_IS_DUAL_DANCING_AVAILABLE_TO_PLAYER(ref_local, ref_local.piLocalPlayer)
ENDFUNC

FUNC BOOL DANCING_IS_PLAYER_DUAL_DANCE_LEAD(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer)
	IF piPlayer = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	RETURN ref_local.playerBD[NATIVE_TO_INT(piPlayer)].bLeadDancer
ENDFUNC

FUNC BOOL DANCING_IS_LOCAL_PLAYER_DUAL_DANCE_LEAD(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN DANCING_IS_PLAYER_DUAL_DANCE_LEAD(ref_local, ref_local.piLocalPlayer)
ENDFUNC

FUNC BOOL DANCING_ARE_BOTH_DUAL_DANCERS_LEADS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	PLAYER_INDEX piPartner = DANCING_GET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local)
	RETURN DANCING_IS_LOCAL_PLAYER_DUAL_DANCE_LEAD(ref_local)
		AND DANCING_IS_PLAYER_DUAL_DANCE_LEAD(ref_local, piPartner)
ENDFUNC

FUNC INT DANCING_GET_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer)
	RETURN ref_local.playerBD[NATIVE_TO_INT(piPlayer)].iSyncSceneID
ENDFUNC

FUNC INT DANCING_GET_LOCAL_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN DANCING_GET_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(ref_local, ref_local.piLocalPlayer)
ENDFUNC

FUNC INT DANCING_GET_PLAYERS_SELECTED_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer)
	RETURN ref_local.playerBD[NATIVE_TO_INT(piPlayer)].iDualDanceStyle
ENDFUNC

FUNC INT DANCING_GET_LOCAL_PLAYERS_SELECTED_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN ref_local.playerBD[ref_local.iLocalPlayer].iDualDanceStyle
ENDFUNC

FUNC INT DANCING_GET_LEADS_SELECTED_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF DANCING_IS_LOCAL_PLAYER_DUAL_DANCE_LEAD(ref_local)
		RETURN DANCING_GET_LOCAL_PLAYERS_SELECTED_DUAL_DANCE(ref_local)
	ENDIF 
	
	// Dance partner must be lead
	PLAYER_INDEX piDancePartner = DANCING_GET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local)
	RETURN DANCING_GET_PLAYERS_SELECTED_DUAL_DANCE(ref_local, piDancePartner)
ENDFUNC

FUNC INT DANCING_GET_DUAL_DANCE_LEADS_SYNC_SCENE_ID(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF DANCING_IS_LOCAL_PLAYER_DUAL_DANCE_LEAD(ref_local)
		RETURN DANCING_GET_LOCAL_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(ref_local)
	ENDIF 
	
	// Dance partner must be lead
	PLAYER_INDEX piDancePartner = DANCING_GET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local)
	IF piDancePartner != INVALID_PLAYER_INDEX()
		RETURN DANCING_GET_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(ref_local, piDancePartner)
	ENDIF
	
	//not lead, dance partner didn't return a synced scene id, maybe local player stores and ID after all despite not being a lead in the system
	IF ref_local.playerBD[NATIVE_TO_INT(PLAYER_ID())].iSyncSceneID != -1
		RETURN ref_local.playerBD[NATIVE_TO_INT(PLAYER_ID())].iSyncSceneID
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC PLAYER_INDEX DANCING_GET_DUAL_DANCE_LEAD_PLAYER(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF DANCING_IS_LOCAL_PLAYER_DUAL_DANCE_LEAD(ref_local)
		RETURN ref_local.piLocalPlayer
	ENDIF 
	
	RETURN DANCING_GET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local)
ENDFUNC

FUNC PLAYER_INDEX DANCING_GET_DUAL_DANCE_NON_LEAD_PLAYER(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF DANCING_IS_LOCAL_PLAYER_DUAL_DANCE_LEAD(ref_local)
		RETURN DANCING_GET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local)
	ENDIF 
	
	RETURN ref_local.piLocalPlayer
ENDFUNC

FUNC STRING DANCING_GET_DUAL_DANCING_ANIM_DICT_SAFE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT iDictIndex, BOOL &bSuccess)
	IF ref_local.instance.getDualDanceAnimDict = NULL
		PRINTLN("[DANCING][DUAL] GET_DUAL_DANCING_ANIM_DICT_SAFE - No dual dance dict assigned")
		bSuccess = FALSE
		RETURN ""
	ENDIF

	DUAL_DANCE_ANIM sAnim
	bSuccess = CALL ref_local.instance.getDualDanceAnimDict(iDictIndex, sAnim)
	RETURN sAnim.strDict
ENDFUNC

FUNC STRING DANCING_GET_DUAL_DANCING_ANIM_DICT(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT iDictIndex)
	BOOL bSuccess
	RETURN DANCING_GET_DUAL_DANCING_ANIM_DICT_SAFE(ref_local, iDictIndex, bSuccess)
ENDFUNC

FUNC DUAL_DANCE_ANIM DANCING_GET_DUAL_DANCING_ANIM_SAFE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT iAnimIndex, BOOL &bSuccess)
	DUAL_DANCE_ANIM sAnim
	
	IF ref_local.instance.getDualDanceAnimDict = NULL
		PRINTLN("[DANCING][DUAL] GET_DUAL_DANCING_ANIM_SAFE - No dual dance dict assigned")
		bSuccess = FALSE
		RETURN sAnim
	ENDIF
	
	bSuccess = CALL ref_local.instance.getDualDanceAnimDict(iAnimIndex, sAnim)
	RETURN sAnim
ENDFUNC

FUNC DUAL_DANCE_ANIM DANCING_GET_DUAL_DANCE_ANIM(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT iAnimIndex)
	BOOL bSuccess
	RETURN DANCING_GET_DUAL_DANCING_ANIM_SAFE(ref_local, iAnimIndex, bSuccess)
ENDFUNC

FUNC BOOL DANCING_LOAD_ALL_DUAL_DANCING_ANIM_DICTS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	BOOL bAllLoaded = TRUE
	BOOL bSuccess
	INT index = 0
	STRING strDict = DANCING_GET_DUAL_DANCING_ANIM_DICT_SAFE(ref_local, index, bSuccess)
	
	WHILE bSuccess	
		REQUEST_ANIM_DICT(strDict)
		IF NOT HAS_ANIM_DICT_LOADED(strDict)
			bAllLoaded = FALSE
			PRINTLN("[DANCING][DUAL] LOAD_ALL_DUAL_DANCING_ANIM_DICTS - ", strDict ," (", index, ") not loaded")
		ENDIF
		
		index++
		strDict = DANCING_GET_DUAL_DANCING_ANIM_DICT_SAFE(ref_local, index, bSuccess)
	ENDWHILE
	
	PRINTLN("[DANCING][DUAL] LOAD_ALL_DUAL_DANCING_ANIM_DICTS - bAllLoaded: ", bAllLoaded)
	RETURN bAllLoaded
ENDFUNC

PROC DANCING_UNLOAD_ALL_DUAL_DANCING_ANIM_DICTS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	BOOL bSuccess
	INT index = 0
	STRING strDict = DANCING_GET_DUAL_DANCING_ANIM_DICT_SAFE(ref_local, index, bSuccess)
	
	WHILE bSuccess
		REMOVE_ANIM_DICT(strDict)
		index++
		strDict = DANCING_GET_DUAL_DANCING_ANIM_DICT_SAFE(ref_local, index, bSuccess)
	ENDWHILE
ENDPROC

FUNC BOOL DANCING_IS_LOCAL_PLAYER_FACING_PLAYER(PLAYER_INDEX piPlayer, FLOAT fAccuracy)
	VECTOR vTarget = GET_ENTITY_COORDS(GET_PLAYER_PED(piPlayer))
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
	FLOAT fDirHeaing = GET_HEADING_BETWEEN_VECTORS_2D(vPlayerPos, vTarget)	
	RETURN IS_HEADING_ACCEPTABLE_CORRECTED(fDirHeaing, fPlayerHeading, fAccuracy)
ENDFUNC

FUNC FLOAT DANCING_GET_DOT_TO_TARGET(PLAYER_INDEX piPlayer)
	VECTOR vDir = GET_ENTITY_COORDS(GET_PLAYER_PED(piPlayer)) - GET_ENTITY_COORDS(PLAYER_PED_ID())
	RETURN DOT_PRODUCT(GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()), vDir)
ENDFUNC

FUNC BOOL DANCING_IS_SPHERE_OCCUPIED(VECTOR vCoords, FLOAT fRadius2, PED_INDEX& piActiveRemotePlayerPeds[], INT iNumActiveRemotePlayers, PED_INDEX piPartner)

	PED_INDEX ped
	// First check if there are any ambient/mission peds in our area...
	IF GET_CLOSEST_PED(vCoords, SQRT(fRadius2), TRUE, TRUE, ped, FALSE, TRUE)
	AND ped != PLAYER_PED_ID() AND ped != piPartner
		RETURN TRUE
	ENDIF
	
	// Now check for participant peds
	INT i
	REPEAT iNumActiveRemotePlayers i
		ped = piActiveRemotePlayerPeds[i]
		IF ped !=  piPartner
		AND DOES_ENTITY_EXIST(ped)
		AND NOT IS_ENTITY_DEAD(ped)
			VECTOR vPedCoords = GET_ENTITY_COORDS(ped)
			FLOAT vPointDistance2 = VMAG2(vCoords - vPedCoords)
			IF vPointDistance2 <= fRadius2
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DANCING_IS_PLAYER_IN_RANGE_FOR_DUAL_DANCE(PLAYER_INDEX piPlayer)
	PED_INDEX piTargetPed = GET_PLAYER_PED(piPlayer)
	VECTOR vTargetPos = GET_ENTITY_COORDS(piTargetPed)
	VECTOR vTargetForward = GET_ENTITY_FORWARD_VECTOR(piTargetPed)
	VECTOR vInFrontOfTarget = vTargetPos + (vTargetForward * cfDANCING_DUAL_DANCE_IN_RANGE_FORWARD_OFFSET)
	VECTOR vLocalPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fDistSquared = VMAG2(vLocalPos - vInFrontOfTarget)
	RETURN fDistSquared <= (cfDANCING_DUAL_DANCE_IN_RANGE_RADIUS * cfDANCING_DUAL_DANCE_IN_RANGE_RADIUS)
ENDFUNC

/// PURPOSE:
///    Checks if the local player can dance with the given player based on their dual dance partner restrictions
///    that they set in the PI menu.
/// PARAMS:
///    piRemotePlayer - the player we are wanting to dance with
/// RETURNS:
///    TRUE if the local player is not restricted from dancing with the target player
FUNC BOOL DANCING_IS_LOCAL_PLAYER_ALLOWED_TO_DANCE_WITH_PLAYER(PLAYER_INDEX piRemotePlayer)
	DUAL_DANCE_RESTRICTION eRestriction = INT_TO_ENUM(DUAL_DANCE_RESTRICTION, GlobalplayerBD[NATIVE_TO_INT(piRemotePlayer)].iDancePartnerOption)
	
	INT iLocalCrewID = GET_LOCAL_PLAYER_CLAN_ID()
	GAMER_HANDLE remotePlayerHandle = GET_GAMER_HANDLE_PLAYER(piRemotePlayer)
	INT iRemoteCrewID = GET_MP_PLAYER_CLAN_ID(remotePlayerHandle)
	BOOL bInSameCrew = iLocalCrewID = iRemoteCrewID
	
	SWITCH eRestriction
		CASE DDR_CREW
			PRINTLN("[DANCING][DUAL] IS_LOCAL_PLAYER_ALLOWED_TO_DANCE_WITH_PLAYER - crew restricted for player: ", GET_PLAYER_NAME(piRemotePlayer))
			RETURN bInSameCrew
		CASE DDR_CREW_AND_FRIENDS
			PRINTLN("[DANCING][DUAL] IS_LOCAL_PLAYER_ALLOWED_TO_DANCE_WITH_PLAYER - crew and friend restricted for player: ", GET_PLAYER_NAME(piRemotePlayer))
			RETURN ARE_PLAYERS_FRIENDS(PLAYER_ID(), piRemotePlayer) OR bInSameCrew
		CASE DDR_EVERYONE
			PRINTLN("[DANCING][DUAL] IS_LOCAL_PLAYER_ALLOWED_TO_DANCE_WITH_PLAYER - no restriction for player: ", GET_PLAYER_NAME(piRemotePlayer))
			RETURN TRUE
		CASE DDR_NO_ONE
			PRINTLN("[DANCING][DUAL] IS_LOCAL_PLAYER_ALLOWED_TO_DANCE_WITH_PLAYER - full restriction for player: ", GET_PLAYER_NAME(piRemotePlayer))
			RETURN FALSE
		CASE DDR_ORGANIZATION
			PRINTLN("[DANCING][DUAL] IS_LOCAL_PLAYER_ALLOWED_TO_DANCE_WITH_PLAYER - organization restricted for player: ", GET_PLAYER_NAME(piRemotePlayer))
			RETURN GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), piRemotePlayer)
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DANCING_IS_PLAYER_AVAILABLE_FOR_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piPlayer, FLOAT fFacingAccuracy)
	IF NOT IS_PLAYER_DANCING(piPlayer)
		//PRINTLN("[DANCING][DUAL] IS_PLAYER_AVAILABLE_FOR_DUAL_DANCE - player not dancing")
		RETURN FALSE
	ENDIF
	
	DANCE_LOCAL_STAGE eStage = ref_local.playerBD[NATIVE_TO_INT(piPlayer)].eStage
	
	IF eStage = DLS_FLOURISH OR eStage = DLS_MOVE_FLOURISH
		RETURN FALSE
	ENDIF
	
	IF DANCING_DOES_PLAYER_HAVE_DANCE_PARTNER(ref_local, piPlayer)
		//PRINTLN("[DANCING][DUAL] IS_PLAYER_AVAILABLE_FOR_DUAL_DANCE - player already has dance partner")
		RETURN FALSE
	ENDIF
	
	IF NOT DANCING_IS_PLAYER_IN_RANGE_FOR_DUAL_DANCE(piPlayer)
		//PRINTLN("[DANCING][DUAL] IS_PLAYER_AVAILABLE_FOR_DUAL_DANCE - local player not in range for dual dance")
		RETURN FALSE
	ENDIF
	
	IF NOT DANCING_IS_LOCAL_PLAYER_FACING_PLAYER(piPlayer, fFacingAccuracy)
		//PRINTLN("[DANCING][DUAL] IS_PLAYER_AVAILABLE_FOR_DUAL_DANCE - local player not facing player")
		RETURN FALSE
	ENDIF
	
	IF NOT DANCING_IS_LOCAL_PLAYER_ALLOWED_TO_DANCE_WITH_PLAYER(piPlayer)
		PRINTLN("[DANCING][DUAL] IS_PLAYER_AVAILABLE_FOR_DUAL_DANCE - dancing is restricted with the player: ", GET_PLAYER_NAME(piPlayer))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC FLOAT DANCING_GET_ANIM_ENTER_EVENT_PHASE(STRING strDict, STRING strClip)
	FLOAT fStart, fEnd
	IF FIND_ANIM_EVENT_PHASE(strDict, strClip, "ENTER", fStart, fEnd)
		RETURN fStart
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT DANCING_GET_ANIM_EXIT_EVENT_PHASE(STRING strDict, STRING strClip)
	FLOAT fStart, fEnd
	IF FIND_ANIM_EVENT_PHASE(strDict, strClip, "EXIT", fStart, fEnd)
		RETURN fStart
	ENDIF
	
	RETURN 1.0
ENDFUNC

FUNC BOOL DANCING_IS_POINT_INSIDE_PLAYERS_CURRENT_INTERIOR(VECTOR vPosToCheck, PLAYER_INDEX piPlayer)
	IF IS_COLLISION_MARKED_OUTSIDE(vPosToCheck)
		RETURN FALSE
	ENDIF
		
	INTERIOR_INSTANCE_INDEX interiorIndexToCheck = GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(piPlayer))
	
	// make sure the handle to the specific interior to check is valid first
	IF NOT IS_VALID_INTERIOR(interiorIndexToCheck)
		RETURN FALSE
	ENDIF

	INTERIOR_INSTANCE_INDEX interiorIndexAtPosition = GET_INTERIOR_FROM_COLLISION(vPosToCheck)
	RETURN interiorIndexAtPosition = interiorIndexToCheck
ENDFUNC

ENUM DANCING_VALID_DUAL_DANCE_POS_RESULT
	DVDDPR_INVALID_POSITION
	,DVDDPR_VALID_POSITION
	,DVDDPR_PENDING_SHAPE_TEST
ENDENUM

PROC DANCING_CLEANUP_POSITION_CHECK_SHAPE_TEST(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	ref_local.iDancePosShapeTestID = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
	CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_STARTED_SHAPE_TEST)
ENDPROC

#IF IS_DEBUG_BUILD
DEBUGONLY PROC DANCING_DEBUG_DRAW_DUAL_DANCE_RAYCAST_CHECK(PLAYER_INDEX piDancingPlayer)
	
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_DrawDualDanceShapeTest")
		EXIT
	ENDIF
	
	VECTOR vPos = GET_ENTITY_COORDS(GET_PLAYER_PED(piDancingPlayer))
	VECTOR vForward = GET_ENTITY_FORWARD_VECTOR(GET_PLAYER_PED(piDancingPlayer))
	VECTOR vRayStart = vPos + (vForward * cfDANCING_DUAL_DANCE_RAYCHECK_RADIUS) + <<0.0, 0.0, cfDANCING_DUAL_DANCE_RAYCHECK_HEIGHT>>
	VECTOR vRayEnd = vPos + (vForward * cfDANCING_DUAL_DANCE_RAYCHECK_DIST) + <<0.0, 0.0, cfDANCING_DUAL_DANCE_RAYCHECK_HEIGHT>> 
	
	DRAW_DEBUG_SPHERE(vRayStart, cfDANCING_DUAL_DANCE_RAYCHECK_RADIUS, DEFAULT, DEFAULT, DEFAULT, 150)
	DRAW_DEBUG_SPHERE(vRayEnd, cfDANCING_DUAL_DANCE_RAYCHECK_RADIUS, 255, 0, 0, 150)
	DRAW_DEBUG_LINE(vRayStart, vRayEnd)

ENDPROC
#ENDIF

PROC DANCING_START_DUAL_DANCE_SHAPE_TEST(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piDancingPlayer)
	IF ref_local.iDancePosShapeTestID != NULL
		PRINTLN("[DANCING][SHAPE_TEST] START_DUAL_DANCE_SHAPE_TEST - shape test in progress")
		EXIT
	ENDIF
	
	VECTOR vPos = GET_ENTITY_COORDS(GET_PLAYER_PED(piDancingPlayer))
	VECTOR vForward = GET_ENTITY_FORWARD_VECTOR(GET_PLAYER_PED(piDancingPlayer))
	VECTOR vRayStart = vPos + (vForward * cfDANCING_DUAL_DANCE_RAYCHECK_RADIUS) + <<0.0, 0.0, cfDANCING_DUAL_DANCE_RAYCHECK_HEIGHT>>
	VECTOR vRayEnd = vPos + (vForward * cfDANCING_DUAL_DANCE_RAYCHECK_DIST) + <<0.0, 0.0, cfDANCING_DUAL_DANCE_RAYCHECK_HEIGHT>> 
	
	INT iFlags = SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_GLASS | SCRIPT_INCLUDE_RIVER | SCRIPT_INCLUDE_FOLIAGE | SCRIPT_INCLUDE_MOVER
	
	ref_local.iDancePosShapeTestID = START_SHAPE_TEST_SWEPT_SPHERE(vRayStart, vRayEnd, cfDANCING_DUAL_DANCE_RAYCHECK_RADIUS, iFlags, GET_PLAYER_PED(piDancingPlayer), SCRIPT_SHAPETEST_OPTION_IGNORE_NO_COLLISION)
	PRINTLN("[DANCING][SHAPE_TEST] START_DUAL_DANCE_SHAPE_TEST - shape test started")
ENDPROC

FUNC SHAPETEST_STATUS DANCING_GET_DUAL_DANCE_HAS_PASSED_SHAPE_TEST(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL &bHitSomething)	
	INT iHitSomething
	ENTITY_INDEX entityHit
	VECTOR vTargetHitPos, vNormalAtPosHit
	SHAPETEST_STATUS eResult = GET_SHAPE_TEST_RESULT(ref_local.iDancePosShapeTestID, iHitSomething, vTargetHitPos, vNormalAtPosHit, entityHit)
	
	IF eResult = SHAPETEST_STATUS_RESULTS_READY
		bHitSomething = iHitSomething != 0
		ref_local.iDancePosShapeTestID = NULL
		PRINTLN("[DANCING][SHAPE_TEST] IS_VALID_DANCE_POSITION_FOR_PLAYER - result ready, iHitSomething: ", iHitSomething, " bHitSomething: ", bHitSomething)	
		RETURN eResult
	ENDIF
	
	RETURN eResult
ENDFUNC

STRUCT DUAL_DANCE_CANDIDATE
	FLOAT fAngle
	PLAYER_INDEX piPlayer
ENDSTRUCT

PROC DANCING_QUICK_SORT_DUAL_DANCE_CANDIDATES(DUAL_DANCE_CANDIDATE &sArray[], INT iLeft, INT iRight)
	INT i, j
	DUAL_DANCE_CANDIDATE p = sArray[((i + j) / 2)]
	DUAL_DANCE_CANDIDATE q
	i = iLeft
	j = iRight

	WHILE (i <= j)
	
		WHILE ((sArray[i].fAngle < p.fAngle) AND (i < iRight))
			i++
		ENDWHILE
		
		WHILE ((p.fAngle < sArray[j].fAngle) AND (j > iLeft))
			j--
		ENDWHILE
		
		IF (i <= j)
			q = sArray[i]
			sArray[i] = sArray[j]
			sArray[j] = q
			i++
			j--
		ENDIF
	
	ENDWHILE
	
	IF (i < iRight)
		DANCING_QUICK_SORT_DUAL_DANCE_CANDIDATES(sArray, i, iRight)
	ENDIF
	
	IF (iLeft < j)
		DANCING_QUICK_SORT_DUAL_DANCE_CANDIDATES(sArray, iLeft, j)
	ENDIF

ENDPROC

PROC DANCING_GET_LIST_OF_DUAL_DANCING_CANDIDATES(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DUAL_DANCE_CANDIDATE &sCandidates[], FLOAT fFacingAccuracy)
	INT iParticipant = 0
	INT iCandidates = 0
	INT iMaxArray = COUNT_OF(sCandidates)
	PLAYER_INDEX piRemotePlayer
	PARTICIPANT_INDEX piParticipantIndex	

	REPEAT iMaxArray iParticipant	
		// Init array to invalid
		sCandidates[iParticipant].piPlayer = INVALID_PLAYER_INDEX()
		
		piParticipantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)	
		
		// Ignore inactive
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piParticipantIndex)
			RELOOP
		ENDIF
		
		piRemotePlayer = NETWORK_GET_PLAYER_INDEX(piParticipantIndex)
		
		// Ignore ourselves
		IF piRemotePlayer = ref_local.piLocalPlayer
			RELOOP
		ENDIF
		
		// Only include valid players
		IF NOT IS_NET_PLAYER_OK(piRemotePlayer)
			RELOOP
		ENDIF
		
		IF NOT DANCING_IS_PLAYER_AVAILABLE_FOR_DUAL_DANCE(ref_local, piRemotePlayer, fFacingAccuracy)
			RELOOP
		ENDIF
		
		FLOAT fAngle = DANCING_GET_DOT_TO_TARGET(piRemotePlayer)
		sCandidates[iCandidates].fAngle = fAngle
		sCandidates[iCandidates].piPlayer = piRemotePlayer
		++iCandidates
		
		PRINTLN("[DANCING] GET_LIST_OF_DUAL_DANCING_CANDIDATES - adding: ", GET_PLAYER_NAME(piRemotePlayer), " angle: ", fAngle)
	ENDREPEAT
	
	DANCING_QUICK_SORT_DUAL_DANCE_CANDIDATES(sCandidates, 0, iCandidates - 1)
ENDPROC

FUNC BOOL DANCING_IS_LOCAL_PLAYER_ABLE_TO_TRIGGER_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX &ref_Player, FLOAT fFacingAccuracy)
	ref_Player = INVALID_PLAYER_INDEX()
	
	IF NOT DANCING_IS_DUAL_DANCING_AVAILABLE_TO_LOCAL_PLAYER(ref_local)
		RETURN FALSE
	ENDIF
	
	// Find valid dance partner
	DUAL_DANCE_CANDIDATE sCandidates[NUM_NETWORK_PLAYERS]
	DANCING_GET_LIST_OF_DUAL_DANCING_CANDIDATES(ref_local, sCandidates, fFacingAccuracy)
	ref_player = sCandidates[0].piPlayer // temp just use closest candidate
	RETURN ref_Player != INVALID_PLAYER_INDEX()
ENDFUNC

PROC DANCING_UPDATE_DUAL_DANCE_SYNC_SCENE_TRANSFORM(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piDancePartner, BOOL bIsLead)
	IF bIsLead
		/// If we're the lead we initated the dual dance on an already dancing player, so use their position and rotation because they
		/// are stuck standing still
		PED_INDEX pedDancePartner = GET_PLAYER_PED(piDancePartner)
		VECTOR vPos = GET_ENTITY_COORDS(pedDancePartner) - <<0.0, 0.0, 1.0>> // Subtract mover offset
		FLOAT fPartnerHeading = GET_ENTITY_HEADING(pedDancePartner)
		PRINTLN("[DANCING][DUAL][LEAD] UPDATE_DUAL_DANCE_SYNC_SCENE_TRANSFORM - partner heading: ", fPartnerHeading)
		
		#IF IS_DEBUG_BUILD
		IF ref_local.bUsingDanceBot
			vPos += GET_ENTITY_FORWARD_VECTOR(pedDancePartner)
			VECTOR vBotRot = <<0.0, 0.0, fPartnerHeading>>
			DANCING_SET_PLAYERS_SYNC_SCENE_TRANSFORM(ref_local, ref_local.piLocalPlayer, vPos, vBotRot)
			PRINTLN("[DANCING][DUAL][LEAD] UPDATE_DUAL_DANCE_SYNC_SCENE_TRANSFORM - Using dance bot coords")
			EXIT
		ENDIF
		#ENDIF		
		
		fPartnerHeading = fPartnerHeading + 180
		fPartnerHeading = (((fPartnerHeading % 360.0) + 360.0) % 360.0)
		VECTOR vRot = <<0.0, 0.0, fPartnerHeading>>
		PRINTLN("[DANCING][DUAL][LEAD] UPDATE_DUAL_DANCE_SYNC_SCENE_TRANSFORM - corrected rotation heading: ", vRot)
		
		DANCING_SET_PLAYERS_SYNC_SCENE_TRANSFORM(ref_local, ref_local.piLocalPlayer, vPos, vRot)
		PRINTLN("[DANCING][DUAL][LEAD] UPDATE_DUAL_DANCE_SYNC_SCENE_TRANSFORM - set sync scene transform")
	ELSE
		/// If not the led the other player has already defined the sync scene coords so use theirs
		TRANSFORM_STRUCT sTransform = DANCING_GET_PLAYERS_SYNC_SCENE_TRANSFORM(ref_local, piDancePartner)
		DANCING_SET_PLAYERS_SYNC_SCENE_TRANSFORM(ref_local, ref_local.piLocalPlayer, sTransform.Position, sTransform.Rotation)
		PRINTLN("[DANCING][DUAL] UPDATE_DUAL_DANCE_SYNC_SCENE_TRANSFORM - copied sync scene transform, pos: ", sTransform.Position, " rot: ", sTransform.Rotation)
	ENDIF
ENDPROC

PROC DANCING_GET_DUAL_DANCE_CLIPS_START_TRANSFORM_FOR_CURRENT_ANIM_DICT(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DUAL_DANCE_CLIP eDualDanceClip, BOOL bPedA, TRANSFORM_STRUCT &sTransformOut)
	INT iSelectedDance = DANCING_GET_LOCAL_PLAYERS_SELECTED_DUAL_DANCE(ref_local)
	DUAL_DANCE_ANIM sAnim = DANCING_GET_DUAL_DANCE_ANIM(ref_local, iSelectedDance)
	
	STRING strAnimDict = sAnim.strDict
	STRING strAnimClip
	
	IF bPedA
		strAnimClip = sAnim.sPedAClips[eDualDanceClip].strClip
	ELSE
		strAnimClip = sAnim.sPedBClips[eDualDanceClip].strClip
	ENDIF
	
	TRANSFORM_STRUCT sSceneTransform = DANCING_GET_PLAYERS_SYNC_SCENE_TRANSFORM(ref_local, ref_local.piLocalPlayer)
	FLOAT fPhase = DANCING_GET_ANIM_ENTER_EVENT_PHASE(strAnimDict, strAnimClip)
	
	sTransformOut.Position = GET_ANIM_INITIAL_OFFSET_POSITION(strAnimDict, strAnimClip, sSceneTransform.Position, sSceneTransform.Rotation, fPhase)
	sTransformOut.Rotation = GET_ANIM_INITIAL_OFFSET_ROTATION(strAnimDict, strAnimClip, sSceneTransform.Position, sSceneTransform.Rotation, fPhase)
ENDPROC

PROC DANCING_MAKE_LOCAL_PLAYER_WALK_TO_DUAL_DANCE_LEAD_START_POSITION(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	TRANSFORM_STRUCT sTransform 
	DANCING_GET_DUAL_DANCE_CLIPS_START_TRANSFORM_FOR_CURRENT_ANIM_DICT(ref_local, DDC_DUAL_DANCE_STANDING_INTRO, FALSE, sTransform)
	ref_local.vWalkToTarget = sTransform.Position
	TASK_FOLLOW_NAV_MESH_TO_COORD(ref_local.pedLocalPlayer, sTransform.Position, PEDMOVE_WALK, DEFAULT, DEFAULT, DEFAULT, sTransform.Rotation.z)
ENDPROC

FUNC INT DANCING_GET_COUNT_OF_DUAL_DANCES(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF ref_local.instance.getDualDanceAnimDict = NULL
		RETURN 0
	ENDIF
	
	INT iDanceCount = 0
	INT iAnimIndex = 0
	DUAL_DANCE_ANIM sAnim
	WHILE CALL ref_local.instance.getDualDanceAnimDict(iAnimIndex, sAnim)
		++iAnimIndex
		++iDanceCount
	ENDWHILE
	
	PRINTLN("[DANCING][DUAL] GET_COUNT_OF_DUAL_DANCES - iDanceCount: ", iDanceCount)
	RETURN iDanceCount
ENDFUNC

/// PURPOSE:
///    Shake speed increases with the player's dance intensity.
FUNC FLOAT COMPUTE_SHAKE_SPEED(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN LERP_FLOAT(cfSHAKE_AMP_MIN, cfSHAKE_AMP_MAX, ref_local.fCurrentIntensitySignal)
ENDFUNC

/// PURPOSE:
///   Joins a dual dance with the given player, the dance partner must also call this function to accept the dance
///   if joining as the lead they will walk to the anim start position
/// PARAMS:
///    ref_local - the dancing instance
///    piDancePartner - In debug build if set to invalid player a bot will be used as the partner
///    bIsLead - if the player is the ead dancer who initiates the dance, they will have control over creating the sync scenes and dance selection
PROC DANCING_JOIN_DUAL_DANCE_WITH_PLAYER(PLAYER_DANCE_LOCAL_STRUCT& ref_local, PLAYER_INDEX piDancePartner, BOOL bIsLead)
	PRINTLN("[DANCING][DUAL] JOIN_DUAL_DANCE_WITH_PLAYER - Joining dance with: ", GET_PLAYER_NAME(piDancePartner))
	DANCING_SET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local, piDancePartner)
	DANCING_SET_LOCAL_PLAYER_DUAL_DANCE_LEAD(ref_local, bIsLead) // Initiator is the lead
	DANCING_UPDATE_DUAL_DANCE_SYNC_SCENE_TRANSFORM(ref_local, piDancePartner, bIsLead)
	DANCING_SET_LOCAL_PLAYER_READY_FOR_DUAL_DANCE(ref_local, TRUE)
	DANCING_ENABLE_INTERACTION_MENU(ref_local, FALSE)
	DANCE_UNLOAD_SCALEFORM(ref_local)
	RESET_NET_TIMER(ref_local.sStateBailTimer)
	
	
	SET_PLAYER_CONTROL(ref_local.piLocalPlayer, FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
	
	IF bIsLead
		DANCING_MAKE_LOCAL_PLAYER_WALK_TO_DUAL_DANCE_LEAD_START_POSITION(ref_local)
		
		INT iDanceStyle = GET_RANDOM_INT_IN_RANGE(0, DANCING_GET_COUNT_OF_DUAL_DANCES(ref_local))
		ref_Local.playerBD[ref_local.iLocalPlayer].iDualDanceStyle = iDanceStyle
		PLAYSTATS_MINIGAME_USAGE(MINIGAME_TELEMETRY_DANCING, ref_local.instance.iLocationHash)
		PRINTLN("[DANCING][DUAL] JOIN_DUAL_DANCE_WITH_PLAYER - iDanceStyle: ", iDanceStyle)
	ELSE
		// Reset shake interpolation
		ref_local.fCamShakeAtReset = COMPUTE_SHAKE_SPEED(ref_local) 
		RESET_NET_TIMER(ref_local.tRestartTime)
		START_NET_TIMER(ref_local.tRestartTime)
	ENDIF
	
	DANCE_SET_STAGE(ref_local, DLS_WAIT_FOR_DUAL_DANCE_START)
ENDPROC

#IF IS_DEBUG_BUILD
DEBUGONLY PROC DANCING_JOIN_DUAL_DANCE_WITH_DANCE_BOT(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	ref_local.bUsingDanceBot = TRUE
	DANCING_JOIN_DUAL_DANCE_WITH_PLAYER(ref_local, INVALID_PLAYER_INDEX(), TRUE)
	RESET_NET_TIMER(ref_local.sStateBailTimer)
ENDPROC
#ENDIF

PROC DANCING_CLEANUP_LOCAL_PLAYER_DUAL_DANCE_BROADCAST_DATA(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	DANCING_SET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local, INVALID_PLAYER_INDEX())
	DANCING_SET_LOCAL_PLAYER_DUAL_DANCE_LEAD(ref_local, FALSE)
	DANCING_SET_LOCAL_PLAYER_READY_FOR_DUAL_DANCE(ref_local, FALSE)
	DANCING_SET_LOCAL_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(ref_local, -1)	
	PRINTLN("[DANCING][DUAL] CLEANUP_LOCAL_PLAYER_DUAL_DANCE_BROADCAST_DATA - Cleaned up")
ENDPROC

PROC DANCING_CLEANUP_DUAL_DANCING(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RESET_NET_TIMER(ref_local.sStateBailTimer)
	
	// Cleanup sync scene
	INT iSyncScene = DANCING_GET_DUAL_DANCE_LEADS_SYNC_SCENE_ID(ref_local)
	IF iSyncScene != -1
		NETWORK_STOP_SYNCHRONISED_SCENE(iSyncScene)
		PRINTLN("[DANCING][DUAL][LEAD] CLEANUP_DUAL_DANCING - Stopped sync scene")
	ENDIF
	
	STOP_GAMEPLAY_CAM_SHAKING()
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_THIRD_PERSON_FAR
		SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
	ENDIF
	IF IS_ENTITY_ALIVE(ref_local.pedLocalPlayer)
		CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(ref_local.pedLocalPlayer)
	ENDIF
	CLEAR_HELP()
	
	DANCING_ENABLE_INTERACTION_MENU(ref_local, TRUE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	DANCING_CLEANUP_LOCAL_PLAYER_DUAL_DANCE_BROADCAST_DATA(ref_local)
	CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_TRIGGERED_DUAL_DANCE_END)
	CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DUAL_DANCE_ANIM_STARTED)
	DANCING_CLEANUP_POSITION_CHECK_SHAPE_TEST(ref_local)
	
	#IF IS_DEBUG_BUILD
	ref_local.bUsingDanceBot = FALSE
	SAFE_DELETE_PED(ref_local.debug.pedDanceBot)
	#ENDIF
	
	PRINTLN("[DANCING][DUAL][LEAD] CLEANUP_DUAL_DANCING - Clean up called")
ENDPROC

PROC DANCING_REGISTER_BROADCAST_DATA(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(ref_local.playerBD, SIZE_OF(ref_local.playerBD))
	PRINTLN("[DANCING][DUAL] REGISTER_BROADCAST_DATA - Registered broadcast data")
ENDPROC

#ENDIF

/// PURPOSE:
///    Return FALSE when <index> is out of range. 
///    <result> reflects whether the <ped> is in the area <index>
FUNC BOOL IS_PLAYER_IN_BLOCK_LOCATE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT index, PED_INDEX ped, BOOL& ref_bResult)
	RETURN CALL ref_local.instance.isPlayerInBlockArea(index, ped, ref_bResult)
ENDFUNC

/// PURPOSE:
///    Return FALSE when <index> is out of range. 
///    <result> reflects whether the <ped> is in the area <index>
FUNC BOOL IS_PLAYER_IN_DANCE_LOCATE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT index, PED_INDEX ped, BOOL& ref_bResult)
	RETURN CALL ref_local.instance.isPlayerInDanceArea(index, ped, ref_bResult)
ENDFUNC

/// PURPOSE:
///    Return TRUE if the player is in any dance locate.
///    If this returns TRUE <ref_iDanceAreaIndex> contains the index of the locate.
FUNC BOOL IS_PLAYER_IN_ANY_DANCE_AREA(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT& ref_iDanceAreaIndex)
	PED_INDEX ped = ref_local.pedLocalPlayer
	BOOL result
	ref_iDanceAreaIndex = 0
	WHILE IS_PLAYER_IN_DANCE_LOCATE(ref_local, ref_iDanceAreaIndex, ped, result)
		IF result
			RETURN TRUE
		ENDIF
		ref_iDanceAreaIndex++
	ENDWHILE
	
	#IF DANCING_FEATURE_DUAL_DANCE
	// Dual dance areas count as general dance areas because one player must be dancing to initiate a dual dance
	IF DANCING_IS_LOCAL_PLAYER_IN_DUAL_DANCE_AREA(ref_local)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Return TRUE if the player is in any blocker locate.
FUNC BOOL IS_PLAYER_IN_ANY_BLOCK_AREA(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	PED_INDEX ped = ref_local.pedLocalPlayer
	BOOL result
	INT index
	WHILE IS_PLAYER_IN_BLOCK_LOCATE(ref_local, index, ped, result)
		IF result
			RETURN TRUE
		ENDIF
		index++
	ENDWHILE
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Return TRUE if the player is in any blocker locate. This function does one loop
///    iteration per frame.
FUNC BOOL IS_PLAYER_IN_BLOCK_AREA_STAGGERED(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	BOOL bResult = FALSE

	IF IS_PLAYER_IN_BLOCK_LOCATE(ref_local, ref_local.iBlockAreaStagger, ref_local.pedLocalPlayer, bResult)
		ref_local.iBlockAreaStagger += 1
	ELSE
		ref_local.iBlockAreaStagger = 0
	ENDIF
	
	RETURN bResult
ENDFUNC

/// PURPOSE:
///    Called on every full minute of player dancing.
PROC TRIGGER_ON_FULL_MIN_DANCING(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	PRINTLN("[DANCING] CALLING ref_local.instance.onFullMinDancing")
	CALL ref_local.instance.onFullMinDancing()
ENDPROC

/// PURPOSE:
///    Called on every full minute of player continuously beat matching.
PROC TRIGGER_ON_FULL_MIN_BEAT_MATCHING(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT iNumFullMins)
	PRINTLN("[DANCING] CALLING ref_local.instance.onFullMinBeatMatching")
	CALL ref_local.instance.onFullMinBeatMatching(iNumFullMins)
ENDPROC

/// PURPOSE:
///    Get anim data <ref_clipset> at <index>. 
///    Returns FALSE if <index> is out of range.
FUNC BOOL GET_DANCE_CLIPSET_DATA(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT index, DANCE_CLIPSET& ref_clipset)
	RETURN CALL ref_local.instance.getDanceClipsetData(index, ref_clipset)
ENDFUNC


/// PURPOSE:
///    Returns the variable clipset name for the move network.
FUNC STRING DANCE_GET_VARIABLE_CLIPSET_STRING()
	RETURN "DANCE_VARIABLE_CLIPSET"
ENDFUNC

/// PURPOSE:
///    Returns the hash of the variable clipset for the move network.
FUNC INT DANCE_GET_VARIABLE_CLIPSET_HASH()
	RETURN HASH("DANCE_VARIABLE_CLIPSET")
ENDFUNC

FUNC STRING DANCE_GET_MOVE_NETWORK_STRING()
	RETURN "Dance_Mini_Game"
ENDFUNC	

FUNC INT DANCE_GET_FLOURISH_VARIABLE_CLIPSET_HASH()
	RETURN HASH("DanceUpperBodyClipset")
ENDFUNC

FUNC STRING DANCE_GET_FLOURISH_VARIABLE_CLIPSET()
	RETURN "DanceUpperBodyClipset"
ENDFUNC

//*********** DANCE SCALEFORM ********

// EXCLUSIVE
FUNC FLOAT GET_MIN_INTENSITY_FOR_LEVEL(INT iLevel)
	SWITCH iLevel
		CASE LEVEL_1 RETURN cfLEVEL_1_MIN 
		CASE LEVEL_2 RETURN cfLEVEL_2_MIN
		CASE LEVEL_3 RETURN cfLEVEL_3_MIN
		CASE LEVEL_4 RETURN cfLEVEL_4_MIN
		DEFAULT ASSERTLN("[PLAYER_DANCING] Invalid intensity level ", iLevel) RETURN 0.0
	ENDSWITCH
ENDFUNC

// INCLUSIVE
FUNC FLOAT GET_MAX_INTENSITY_FOR_LEVEL(INT iLevel)
	SWITCH iLevel
		CASE LEVEL_1 RETURN cfLEVEL_2_MIN
		CASE LEVEL_2 RETURN cfLEVEL_3_MIN
		CASE LEVEL_3 RETURN cfLEVEL_4_MIN
		CASE LEVEL_4 RETURN 1.0
		DEFAULT ASSERTLN("[PLAYER_DANCING] Invalid intensity level ", iLevel) RETURN 0.0
	ENDSWITCH
ENDFUNC

FUNC FLOAT GET_INTENSITY_PER_HIT(INT iLevel)
	SWITCH iLevel
		CASE LEVEL_1 	// Fallthrough
		CASE LEVEL_2	RETURN (1.0 / ciLEVEL_2_BEATS_HIT) * (cfLEVEL_3_MIN - 0.0)
		CASE LEVEL_3	RETURN (1.0 / ciLEVEL_3_BEATS_HIT) * (cfLEVEL_4_MIN - cfLEVEL_3_MIN)
		CASE LEVEL_4	RETURN (1.0 / ciLEVEL_4_BEATS_HIT) * (1.0 - cfLEVEL_4_MIN)
		DEFAULT ASSERTLN("[DANCING] GET_INTENSITY_CHANGE_FOR_HIT invalid value") RETURN 0.0
	ENDSWITCH
ENDFUNC

FUNC FLOAT GET_INTENSITY_PER_MISS(INT iLevel)
	SWITCH iLevel
		CASE LEVEL_1	// Fallthrough
		CASE LEVEL_2	RETURN (-1.0 / ciLEVEL_2_BEATS_MISS) * (cfLEVEL_3_MIN - 0.0)
		CASE LEVEL_3	RETURN (-1.0 / ciLEVEL_3_BEATS_MISS) * (cfLEVEL_4_MIN - cfLEVEL_3_MIN)
		CASE LEVEL_4	RETURN (-1.0 / ciLEVEL_4_BEATS_MISS) * (1.0 - cfLEVEL_4_MIN)
		DEFAULT ASSERTLN("[DANCING] GET_INTENSITY_CHANGE_FOR_MISS invalid value") RETURN 0.0
	ENDSWITCH
ENDFUNC

FUNC FLOAT GET_INTENSITY_PER_FAIL(INT iLevel)
	SWITCH iLevel
		CASE LEVEL_1	// Fallthrough
		CASE LEVEL_2	RETURN (-1.0 / ciLEVEL_2_BEATS_FAIL) * (cfLEVEL_3_MIN - 0.0)
		CASE LEVEL_3	RETURN (-1.0 / ciLEVEL_3_BEATS_FAIL) * (cfLEVEL_4_MIN - cfLEVEL_3_MIN)
		CASE LEVEL_4	RETURN (-1.0 / ciLEVEL_4_BEATS_FAIL) * (1.0 - cfLEVEL_4_MIN)
		DEFAULT ASSERTLN("[DANCING] GET_INTENSITY_CHANGE_FOR_FAIL invalid value") RETURN 0.0
	ENDSWITCH
ENDFUNC

FUNC INT CONVERT_INTENSITY_SIGNAL_TO_INTENSITY_LEVEL(FLOAT fSignal, BOOL bMinInclusive = FALSE)
	IF bMinInclusive
		IF fSignal >= cfLEVEL_4_MIN
			RETURN LEVEL_4
		ELIF fSignal >= cfLEVEL_3_MIN
			RETURN LEVEL_3
		ELIF fSignal > cfLEVEL_2_MIN // 0.0 = LEVEL_1 special case always exclusive
			RETURN LEVEL_2
		ENDIF	
		
		RETURN LEVEL_1
	ELSE
		IF fSignal > cfLEVEL_4_MIN
			RETURN LEVEL_4
		ELIF fSignal > cfLEVEL_3_MIN
			RETURN LEVEL_3
		ELIF fSignal > cfLEVEL_2_MIN
			RETURN LEVEL_2
		ENDIF	
		
		RETURN LEVEL_1
	ENDIF
ENDFUNC

// There are 4 levels [0, 3]
PROC DANCE_SCALEFORM_SET_LEVEL(DANCE_SCALEFORM& ref_data,INT iLevel)
	BEGIN_SCALEFORM_MOVIE_METHOD(ref_data.scaleform, "SET_LEVEL")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLevel)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC DANCE_SCALEFORM_PULSE(DANCE_SCALEFORM& ref_data)
	CALL_SCALEFORM_MOVIE_METHOD(ref_data.scaleform, "PULSE_ICON")
ENDPROC

PROC DANCE_SCALEFORM_MUSIC_BEAT(DANCE_SCALEFORM& ref_data)
	CALL_SCALEFORM_MOVIE_METHOD(ref_data.scaleform, "MUSIC_BEAT")
ENDPROC

PROC DANCE_SCALEFORM_PLAYER_BEAT(DANCE_SCALEFORM& ref_data, BOOL bMatchedAudioBeat)
	BEGIN_SCALEFORM_MOVIE_METHOD(ref_data.scaleform, "PLAYER_BEAT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bMatchedAudioBeat)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC DANCE_SCALEFORM_FLASH_ICON(DANCE_SCALEFORM& ref_data)
	BEGIN_SCALEFORM_MOVIE_METHOD(ref_data.scaleform, "FLASH_ICON")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC DANCE_SCALEFORM_SET_CONTROL_ICON(DANCE_SCALEFORM& ref_data, BOOL bUsingMouse)
	BEGIN_SCALEFORM_MOVIE_METHOD(ref_data.scaleform, "SET_IS_MOUSE_CONTROL")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUsingMouse)
	END_SCALEFORM_MOVIE_METHOD()
	PRINTLN("[DANCING] DANCE_SCALEFORM_SET_CONTROL_ICON( ",bUsingMouse," )")
ENDPROC

PROC DANCE_SCALEFORM_SET_ALT_RHYTHM_CONTROL(DANCE_SCALEFORM& ref_data, BOOL bUsingAltControl)
	BEGIN_SCALEFORM_MOVIE_METHOD(ref_data.scaleform, "SET_IS_LT_CONTROL")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUsingAltControl)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC DANCE_SCALEFORM_SET_METER_IS_RED(DANCE_SCALEFORM& ref_data, BOOL bValue)
	BEGIN_SCALEFORM_MOVIE_METHOD(ref_data.scaleform, "SET_METER_IS_RED")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bValue)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC DANCE_SCALEFORM_SET_METER(DANCE_SCALEFORM& ref_data, FLOAT fValue, BOOL bAllowRedMeter = TRUE)
	IF bAllowRedMeter 
	AND fValue <= 0.25
		DANCE_SCALEFORM_SET_METER_IS_RED(ref_data, TRUE)
	ELSE
		DANCE_SCALEFORM_SET_METER_IS_RED(ref_data, FALSE)
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(ref_data.scaleform, "SET_METER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fValue)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC DANCE_SCALEFORM_RESET_UI(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	ref_local.display.fMeterValue = 0.0
	ref_local.display.iIntensity = 0
	IF HAS_SCALEFORM_MOVIE_LOADED(ref_local.display.scaleform)
		DANCE_SCALEFORM_SET_METER(ref_local.display, 0.0)
		DANCE_SCALEFORM_SET_LEVEL(ref_local.display, 0)
	ENDIF
ENDPROC

FUNC FLOAT GET_METER_VALUE_FOR_LEVEL(INT iLevel, FLOAT fSignal)
	IF iLevel = LEVEL_1
	OR fSignal = 0.0
		RETURN 0.0
	ENDIF
	IF fSignal = 1.0
		RETURN 1.0
	ENDIF
	
	FLOAT min = GET_MIN_INTENSITY_FOR_LEVEL(iLevel)
	FLOAT max = GET_MAX_INTENSITY_FOR_LEVEL(iLevel)
	FLOAT normalised = CLAMP((fSignal - min) / (max - min), 0.0, 1.0)
	RETURN normalised
ENDFUNC	


TWEAK_FLOAT cfMETER_LERP_SPEED 6.0
PROC DANCE_SCALEFORM_MAINTIAN_METER_AND_LEVEL(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	// ALWAYS use exclusive minimum for the display 
	INT iLevel = CONVERT_INTENSITY_SIGNAL_TO_INTENSITY_LEVEL(ref_local.fTrueIntensitySignal)
	
	IF ref_local.bAltIntensities
		IF ref_local.fTrueIntensitySignal = ref_local.fLevel1
			iLevel = 0
		ELIF ref_local.fTrueIntensitySignal = ref_local.fLevel2
			iLevel = 1
		ELIF ref_local.fTrueIntensitySignal = ref_local.fLevel3
			iLevel = 2
		ELIF ref_local.fTrueIntensitySignal = ref_local.fLevel4
			iLevel = 3
		ELIF ref_local.fTrueIntensitySignal = ref_local.fLevel5
			iLevel = 4
		ENDIF
	ENDIF
	
	IF iLevel <> ref_local.display.iIntensity
		ref_local.display.iIntensity = iLevel
		DANCE_SCALEFORM_SET_LEVEL(ref_local.display, iLevel)
	ENDIF
	
	FLOAT fTargetValue = GET_METER_VALUE_FOR_LEVEL(iLevel, ref_local.fTrueIntensitySignal)	
	ref_local.display.fMeterValue = INTERP_DEGREES(360 * ref_local.display.fMeterValue, 360 * fTargetValue, cfMETER_LERP_SPEED * GET_FRAME_TIME() ) / 360.0
	DANCE_SCALEFORM_SET_METER(ref_local.display, ref_local.display.fMeterValue, IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_ALLOW_RED_METER))
ENDPROC

FUNC COLOUR_RGB GET_FAIL_COLOUR()
	COLOUR_RGB c
	c.r = ciDANCE_CONTROL_FAIL_R
	c.g = ciDANCE_CONTROL_FAIL_G
	c.b = ciDANCE_CONTROL_FAIL_B
	RETURN c
ENDFUNC

FUNC COLOUR_RGB GET_HIT_COLOUR()
	COLOUR_RGB c
	c.r = ciDANCE_CONTROL_HIT_R
	c.g = ciDANCE_CONTROL_HIT_G
	c.b = ciDANCE_CONTROL_HIT_B
	RETURN c
ENDFUNC

FUNC COLOUR_RGB GET_MISS_COLOUR()
	COLOUR_RGB c
	c.r = ciDANCE_CONTROL_MISS_R
	c.g = ciDANCE_CONTROL_MISS_G
	c.b = ciDANCE_CONTROL_MISS_B
	RETURN c
ENDFUNC

FUNC COLOUR_RGB GET_DEFAULT_COLOUR()
	COLOUR_RGB c
	c.r = ciDANCE_CONTROL_DEFAULT_R
	c.g = ciDANCE_CONTROL_DEFAULT_B
	c.b = ciDANCE_CONTROL_DEFAULT_G
	RETURN c
ENDFUNC

FUNC BOOL ARE_COLOUR_EQUAL(COLOUR_RGB a, COLOUR_RGB b)
	RETURN 	a.r = b.r
		AND	a.g = b.g
		AND a.b = b.b
ENDFUNC

PROC DANCE_LIGHTS_MUSIC_BEAT(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF ARE_COLOUR_EQUAL(ref_local.lights.endColour, GET_FAIL_COLOUR())
		EXIT
	ENDIF
	
	RESET_NET_TIMER(ref_local.lights.tColourChangeTime)
	START_NET_TIMER(ref_local.lights.tColourChangeTime)
	ref_local.lights.iDuration = ciDANCE_CONTROL_BEAT_INTERP_TIME_MS
	ref_local.lights.startColour = ref_local.lights.currentColour
	ref_local.lights.endColour = GET_MISS_COLOUR()// PREVIOUSLY ALL LERPED TO DEFAULT WITH STARTCOL = MISS ETC
ENDPROC

PROC DANCE_LIGHTS_PLAYER_BEAT_HIT(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RESET_NET_TIMER(ref_local.lights.tColourChangeTime)
	START_NET_TIMER(ref_local.lights.tColourChangeTime)
	ref_local.lights.iDuration = ciDANCE_CONTROL_BEAT_INTERP_TIME_MS
	ref_local.lights.startColour = ref_local.lights.currentColour
	ref_local.lights.endColour = GET_HIT_COLOUR()
ENDPROC

PROC DANCE_LIGHTS_PLAYER_BEAT_FAIL(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RESET_NET_TIMER(ref_local.lights.tColourChangeTime)
	START_NET_TIMER(ref_local.lights.tColourChangeTime)
	ref_local.lights.iDuration = ciDANCE_CONTROL_BEAT_INTERP_TIME_MS
	ref_local.lights.startColour = ref_local.lights.currentColour
	ref_local.lights.endColour = GET_FAIL_COLOUR()
ENDPROC

FUNC COLOUR_RGB LERP_COLOUR_RGB(COLOUR_RGB startColour, COLOUR_RGB endColour, FLOAT t)
	COLOUR_RGB colour
	colour.r = ROUND(CLAMP(LERP_FLOAT(TO_FLOAT(startColour.r), TO_FLOAT(endColour.r), t), 0, 255))
	colour.g = ROUND(CLAMP(LERP_FLOAT(TO_FLOAT(startColour.g), TO_FLOAT(endColour.g), t), 0, 255))
	colour.b = ROUND(CLAMP(LERP_FLOAT(TO_FLOAT(startColour.b), TO_FLOAT(endColour.b), t), 0, 255))
	RETURN colour
ENDFUNC

PROC DANCE_MAINTAIN_CONTROL_LIGHTS(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bClearEffect = FALSE)
	IF NOT HAS_NET_TIMER_STARTED(ref_local.lights.tColourChangeTime)
	OR bClearEffect
		CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
		EXIT
	ENDIF
	
	INT iTimeSinceChange = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.lights.tColourChangeTime)
	FLOAT fNormalisedTime = CLAMP(TO_FLOAT(iTimeSinceChange)/ TO_FLOAT(ref_local.lights.iDuration), 0, 1)
	
	COLOUR_RGB defaultColour = GET_DEFAULT_COLOUR()
	// if we've gone over then fade back to default
	IF fNormalisedTime >=  1
	AND NOT ARE_COLOUR_EQUAL(ref_local.lights.endColour, defaultColour)
		fNormalisedTime = 0
		RESET_NET_TIMER(ref_local.lights.tColourChangeTime)
		START_NET_TIMER(ref_local.lights.tColourChangeTime)
		ref_local.lights.iDuration = ciDANCE_CONTROL_INTERP_TIME_MS
		ref_local.lights.startColour = ref_local.lights.endColour
		ref_local.lights.endColour = defaultColour
	ENDIF
	
	COLOUR_RGB colour = LERP_COLOUR_RGB(ref_local.lights.startColour, ref_local.lights.endColour, POW(fNormalisedTime, cfPOW_TIME))
	SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, colour.r, colour.g, colour.b)
	ref_local.lights.currentColour = colour
ENDPROC

PROC DANCE_MAINTAIN_PC_CONTROLS(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bShutdown)
	IF bShutdown
		IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PC_INPUT_MAP_LOADED)
			CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PC_INPUT_MAP_LOADED)
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			PRINTLN("[DANCING] SHUTDOWN_PC_SCRIPTED_CONTROLS( )")
		ENDIF
	ELSE
		IF NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PC_INPUT_MAP_LOADED)
		AND INIT_PC_SCRIPTED_CONTROLS("DANCING")
			SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PC_INPUT_MAP_LOADED)
			PRINTLN("[DANCING] INIT_PC_SCRIPTED_CONTROLS( 'DANCING' )")
		ENDIF
	ENDIF
ENDPROC

//************************************
CONST_FLOAT cfFLOURISH_INPUT_RELEASE_DELAY 0.17
TWEAK_FLOAT cfFLOURISH_INPUT_DOUBLE_PRESS_DELAY 0.2

FUNC CONTROL_ACTION GET_FLOURISH_CONTROL_ACTION(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
	AND ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
		RETURN INPUT_SCRIPT_RDOWN
	ENDIF
	
	SWITCH ref_local.eControlScheme
		CASE DANCE_CONTROL_SCHEME_STANDARD		RETURN INPUT_SCRIPT_RT
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_1	RETURN INPUT_SCRIPT_RT
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_2	RETURN INPUT_SCRIPT_RT
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_3	RETURN INPUT_SCRIPT_RB
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_4	RETURN INPUT_SCRIPT_RB
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_5	RETURN INPUT_SCRIPT_RB
	ENDSWITCH
	
	RETURN INPUT_SCRIPT_RT
ENDFUNC

FUNC BOOL IS_FLOURISH_JUST_TRIGGERED(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL& out_bDoublePress, BOOL& out_bWaitForDoubleTap)
	out_bDoublePress = FALSE
	out_bWaitForDoubleTap = FALSE
	
	IF ref_local.flourishInput.fFlourishTriggerFT = 0
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, GET_FLOURISH_CONTROL_ACTION(ref_local))
			ref_local.flourishInput.fFlourishTriggerFT = 0.0001
			PRINTLN("[DANCING] IS_FLOURISH_JUST_TRIGGERED Flourish input start")
		ENDIF
	ELSE
		ref_local.flourishInput.fFlourishTriggerFT += GET_FRAME_TIME()

		IF ref_local.flourishInput.fFlourishTriggerFT >= 0.5
		AND IS_CONTROL_PRESSED(PLAYER_CONTROL, GET_FLOURISH_CONTROL_ACTION(ref_local))
			//We have pressed, and held so trigger the upper body anim
			ref_local.flourishInput.fFlourishTriggerFT = 0.0
			ref_local.flourishInput.bInputReleased 	= FALSE
			out_bDoublePress 			= FALSE
			PRINTLN("[DANCING] IS_FLOURISH_JUST_TRIGGERED Flourish input end. Held down trigger upper")
			RETURN TRUE
		ELIF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, GET_FLOURISH_CONTROL_ACTION(ref_local))
			//We have double pressed in time so trigger the full body anim
			ref_local.flourishInput.fFlourishTriggerFT = 0.0
			ref_local.flourishInput.bInputReleased 	= FALSE
			out_bDoublePress 			= TRUE
			PRINTLN("[DANCING] IS_FLOURISH_JUST_TRIGGERED Flourish input end. Single press.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	out_bWaitForDoubleTap = (ref_local.flourishInput.fFlourishTriggerFT > 0.0)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ACTION_TYPE_VALID_FOR_DANCE_FLOURISH(INT iActionType)
	RETURN (iActionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER))
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the action <iActionAnim> is blocked (for both curated list and PI menu dancing).
///    NOTE: <iActionAnim> should be a value from enum PLAYER_INTERACTION.
FUNC BOOL IS_PLAYER_INTERACTION_ACTION_BLOCKED(INT iActionAnim)
	PLAYER_INTERACTIONS eAnim = INT_TO_ENUM(PLAYER_INTERACTIONS, iActionAnim)
	RETURN eAnim = PLAYER_INTERACTION_AIR_SYNTH
		OR eAnim = PLAYER_INTERACTION_DJ
		OR eAnim = PLAYER_INTERACTION_SMOKE
		OR eAnim = PLAYER_INTERACTION_EAT_1
		OR eAnim = PLAYER_INTERACTION_EAT_2
		OR eAnim = PLAYER_INTERACTION_EAT_3
		OR eAnim = PLAYER_INTERACTION_DRINK_1
		OR eAnim = PLAYER_INTERACTION_DRINK_2
		#IF FEATURE_DLC_1_2022
		OR eAnim = PLAYER_INTERACTION_DRINK_4
		#ENDIF
		OR eAnim = PLAYER_INTERACTION_NONE
ENDFUNC

CONST_INT ciDANCE_DEFAULT_ACTION_TYPE 	INTERACTION_ANIM_TYPE_PLAYER
CONST_INT ciDANCE_DEFAULT_ACTION_ANIM 	PLAYER_INTERACTION_BANGING_TUNES

PROC DANCE_UNLOAD_FLOURISH_DATA(DANCE_INTERACTION_DATA& ref_data)
	INT i
	REPEAT COUNT_OF(FLOURISH_DICT) i
				
		IF NOT IS_STRING_NULL_OR_EMPTY(ref_data.txtDicts[i])
			IF (i = ENUM_TO_INT(FLD_UPPERBODY))
			AND (ref_data.iActionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER)) 
			AND DANCING_IS_INTERACTION_A_DANCE_INTERACTION(ref_data.iActionAnim)
				// DO NOT UNLOAD THE UPPER BODY CYCLE LIST ANIMATIONS BECAUSE 
				// WE NEED THEM IN AT ALL TIMES TO SEE ON REMOTE PLAYERS.
				PRINTLN("[DANCING] NOT REMOVING DICT BECAUSE IT'S ON CURATED LIST ", ref_data.txtDicts[i])
				RELOOP
			ENDIF
			
			REMOVE_ANIM_DICT(ref_data.txtDicts[i])
			PRINTLN("[DANCING] REMOVE_ANIM_DICT ", ref_data.txtDicts[i])
		ENDIF
	ENDREPEAT
	
	#IF FEATURE_HEIST_ISLAND_DANCES
	IF ref_data.objModel != NULL
		SAFE_DELETE_OBJECT(ref_data.objModel)
		PRINTLN("[DANCING][MODEL] Deleted Model")
	ENDIF
	#ENDIF
	
	DANCE_INTERACTION_DATA newData
	ref_data = newData
ENDPROC

/// PURPOSE:
///    Starts loading the interactions <ref_data.txtDicts[]> and returns TRUE when complete.
FUNC BOOL DANCE_HAS_FLOURISH_LOADED(DANCE_INTERACTION_DATA& ref_data)
	BOOL bResult = TRUE
	INT i
	REPEAT COUNT_OF(FLOURISH_DICT) i
		IF NOT IS_STRING_NULL_OR_EMPTY(ref_data.txtDicts[i])
			REQUEST_ANIM_DICT(ref_data.txtDicts[i])
			IF NOT HAS_ANIM_DICT_LOADED(ref_data.txtDicts[i])
				bResult = FALSE
				PRINTLN("[DANCING] Loading ", ref_data.txtDicts[i])
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[DANCING] Loaded ", ref_data.txtDicts[i])
			#ENDIF
			ENDIF
		ELSE
			bResult = FALSE
			PRINTLN("[DANCING] null dict ", i)
		ENDIF
		
		#IF FEATURE_HEIST_ISLAND_DANCES
		IF ref_data.ePropModel != DUMMY_MODEL_FOR_SCRIPT
			REQUEST_MODEL(ref_data.ePropModel)
			IF NOT HAS_MODEL_LOADED(ref_data.ePropModel)
				bResult = FALSE
				PRINTLN("[DANCING] Loading model", ref_data.ePropModel)
			ENDIF
		ENDIF
		#ENDIF
	ENDREPEAT
	ref_data.bStartedLoad = TRUE
	RETURN bResult
ENDFUNC

/// PURPOSE:
///    Set whether to play the FULLBODY (<bFullBody> = TRUE) or UPPERBODY (<bFullBody> = FALSE)
///    flourish actions when DANCE_TASK_SCRIPTED_ANIMATION(..) is called.
PROC DANCE_SET_FLOURISH_IS_FULLBODY(DANCE_INTERACTION_DATA& out_data, BOOL bFullBody)
	IF bFullBody
		out_data.eExitClip = FLC_FULLBODY
		out_data.eDict = FLD_FULLBODY
		PRINTLN("[DANCING] setting flourish is fullbody")
	ELSE
		out_data.eExitClip = FLC_UPPER_OUTRO
		out_data.eDict = FLD_UPPERBODY
		PRINTLN("[DANCING] setting flourish is upper body")
	ENDIF
ENDPROC

/// PURPOSE:
///    Call once before DANCE_HAS_FLOURISH_LOADED to fill <out_data> with valid data.
///    This proc will call DANCE_HAS_FLOURISH_LOADED once to initiate loading.
///    
///    The animations are chosen based on the INTERACTION SYSTEM functions GET_MP_PLAYER_ANIM_TYPE
///    and GET_MP_PLAYER_ANIM_SETTING. 
///    
///    If the current anim settings are invalid, a valid interaction will be used instead.
PROC DANCE_PRELOAD_FLOURISH_ACTION(DANCE_INTERACTION_DATA& out_data)
	PRINTLN("[DANCING][FLOURISH] DANCE_PRELOAD_FLOURISH_ACTION")

	INT iFullBodyDictIdx = PICK_INT(IS_PED_FEMALE(PLAYER_PED_ID()), AD_FULL_BODY_F, AD_FULL_BODY_M)
	INT iUpperBodyDictIdx = AD_DEFAULT
	
	INT iActionType = GET_MP_PLAYER_ANIM_TYPE()
	INT iActionAnim = GET_MP_PLAYER_ANIM_SETTING()

	// Pre-data-fetch validation 
	IF NOT IS_ACTION_TYPE_VALID_FOR_DANCE_FLOURISH(iActionType)
	OR (iActionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER)
	AND IS_PLAYER_INTERACTION_ACTION_BLOCKED(iActionType))
		PRINTLN("[DANCING][FLOURISH]  Pre-data-fetch validation failed type = ",iActionType, " anim = ", iActionAnim)
		iActionType = ciDANCE_DEFAULT_ACTION_TYPE
		iActionAnim = ciDANCE_DEFAULT_ACTION_ANIM	
	ENDIF
	
	// Fetch data
	INTERACTION_DATA interactionData
	FillInteractionData(iActionType, iActionAnim, interactionData)
		
	// Allow certain actions even with props
	BOOL bBypassPropCheck = (iActionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER)) AND DANCING_IS_INTERACTION_A_DANCE_INTERACTION(iActionAnim)
		
	// Post-data-fetch validation 
	IF IS_STRING_NULL_OR_EMPTY(interactionData.strAnimDict[iFullBodyDictIdx])
	OR IS_STRING_NULL_OR_EMPTY(interactionData.strAnimDict[iUpperBodyDictIdx])
	OR (NOT bBypassPropCheck
	AND interactionData.bHasProp)
		PRINTLN("[DANCING][FLOURISH]  Post-data-fetch validation failed type = ",iActionType, " anim = ", iActionAnim)
		iActionType = ciDANCE_DEFAULT_ACTION_TYPE
		iActionAnim = ciDANCE_DEFAULT_ACTION_ANIM	
		FillInteractionData(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), ENUM_TO_INT(PLAYER_INTERACTION_BANGING_TUNES), interactionData)
	ENDIF

	// Get remote players to set this action for us (remote players can only play actions off the curated list on our ped)
	IF DANCING_IS_INTERACTION_A_DANCE_INTERACTION(iActionAnim) 
		BROADCAST_DANCE_ACTION(INT_TO_ENUM(PLAYER_INTERACTIONS, iActionAnim))
		PRINTLN("[DANCING][FLOURISH]  BROADCAST_DANCE_ACTION - ACTION IS SUITABLE")
	ENDIF

	out_data.txtDicts[FLD_UPPERBODY] = interactionData.strAnimDict[iUpperBodyDictIdx]
	out_data.txtDicts[FLD_FULLBODY] = interactionData.strAnimDict[iFullBodyDictIdx]
	out_data.txtClips[FLC_UPPER_OUTRO] = interactionData.strAnimNameOutro
	out_data.txtClips[FLC_FULLBODY] = interactionData.strAnimNameFullBody
	out_data.iActionType = iActionType
	out_data.iActionAnim = iActionAnim
	out_data.eDict = FLD_FULLBODY 		//	default
	out_data.eExitClip = FLC_FULLBODY	// 	default

	#IF FEATURE_HEIST_ISLAND_DANCES
	out_data.ePropModel = interactionData.PropModel
	out_data.bAttachPropModelToRightHand = interactionData.bForceRightHandAttach
	out_data.bAttachPropModelToLeftHand = interactionData.bForceLeftHandAttach
	#ENDIF
	
	// Trigger loading of dicts
	DANCE_HAS_FLOURISH_LOADED(out_data)
		
	PRINTLN("[DANCING][FLOURISH]  Full body dict:   ", out_data.txtDicts[FLD_FULLBODY])
	PRINTLN("[DANCING][FLOURISH]  Full body clip:   ", out_data.txtClips[FLC_FULLBODY])
	PRINTLN("[DANCING][FLOURISH]  Upper dict:       ", out_data.txtDicts[FLD_UPPERBODY])
	PRINTLN("[DANCING][FLOURISH]  Upper outro clip: ", out_data.txtClips[FLC_UPPER_OUTRO])
	PRINTLN("[DANCING][FLOURISH]  Type:             ", iActionType)
	PRINTLN("[DANCING][FLOURISH]  Anim:             ", iActionAnim)
ENDPROC

/// PURPOSE:
///    Returns true once the dance clipset has been loaded.
FUNC BOOL DANCE_LOAD_ALL_CLIPSETS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	// Unfortunately we have to load in all the clipsets!
	BOOL bAllLoaded = TRUE
	DANCE_CLIPSET clipset
	INT index = 0
	WHILE GET_DANCE_CLIPSET_DATA(ref_local, index, clipset)
		REQUEST_CLIP_SET(clipset.sClipset)
		IF NOT HAS_CLIP_SET_LOADED(clipset.sClipset)
			bAllLoaded = FALSE
		ENDIF
		index++
	ENDWHILE
	RETURN bAllLoaded
ENDFUNC

PROC DANCE_UNLOAD_ALL_CLIPSETS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	DANCE_CLIPSET clipset
	INT index = 0
	WHILE GET_DANCE_CLIPSET_DATA(ref_local, index, clipset)
		REMOVE_CLIP_SET(clipset.sClipset)
		index++
	ENDWHILE
ENDPROC

FUNC BOOL CAN_PLAYER_USE_DANCE_CAMERA(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN IS_PLAYER_DANCING(ref_local.piLocalPlayer) AND CALL ref_local.instance.isCinematicCamAvailable(ref_local.iDanceAreaIndex)
ENDFUNC

#IF DANCING_FEATURE_DUAL_DANCE
FUNC BOOL DANCING_IS_LOCAL_PLAYER_DUAL_DANCING(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	SWITCH ref_local.eStage
		#IF DANCING_FEATURE_DUAL_DANCE
		CASE DLS_WAIT_FOR_DUAL_DANCE_START
		CASE DLS_DUAL_DANCE_INTRO
		CASE DLS_DUAL_DANCING
		CASE DLS_DUAL_DANCE_EXIT
		#ENDIF
			RETURN TRUE
		DEFAULT RETURN FALSE
	ENDSWITCH
ENDFUNC
#ENDIF

/// PURPOSE:
///    Returns true if the local player is doing a solo dance.
FUNC BOOL DANCE_IS_LOCAL_PLAYER_DANCING(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	SWITCH ref_local.eStage
		CASE DLS_DANCING
		CASE DLS_STOP_DANCING
		CASE DLS_FLOURISH
		CASE DLS_MOVE_FLOURISH
		CASE DLS_DANCING_INTRO
		#IF DANCING_FEATURE_DUAL_DANCE
		CASE DLS_WAIT_FOR_DUAL_DANCE_START
		CASE DLS_DUAL_DANCE_INTRO
		CASE DLS_DUAL_DANCING
		CASE DLS_DUAL_DANCE_EXIT
		#ENDIF
		
			RETURN TRUE
		DEFAULT RETURN FALSE
	ENDSWITCH
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_DANCE_CLIP_DATUM(DANCE_CLIP_DATUM& ref_clip, STRING sClip)
	CDEBUG2LN(DEBUG_NET_DANCING, "[DANCING][DD2] sClip ==  ", sClip)
	CDEBUG2LN(DEBUG_NET_DANCING, "[DANCING][DD2] fFirstBeatPhase ==  ", ref_clip.fFirstBeatPhase)
	CDEBUG2LN(DEBUG_NET_DANCING, "[DANCING][DD2] fBeatPhaseDiff ==  ", ref_clip.fBeatPhaseDiff)
	CDEBUG2LN(DEBUG_NET_DANCING, "[DANCING][DD2] fBps ==  ", ref_clip.fBps)
ENDPROC
#ENDIF //IS_DEBUG_BUILD

/// PURPOSE:
///    Cache data about the animation clips so that we can try to beat sync the dancing.
///    All the clips must be loaded in memory before this can be called.
PROC PROCESS_ANIMATION_CLIPS(PLAYER_DANCE_LOCAL_STRUCT& ref_local) 
	DANCE_CLIPSET clipset
	GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iDanceStyle, clipset)
		
	STRING sDict = clipset.sDictionary
	STRING sClip
	FLOAT fDummy
	DANCE_CLIP eClip
	DANCE_CLIP_DATUM d
		
	// The length of the clips is uniform so we can use any clip to grab the length.
	ref_local.clipData.fSoloDanceLength = GET_ANIM_DURATION(sDict, GET_DANCE_CLIP_STRING(DC_MOVE_DANCE_MED_CENTER_CENTER))
	
	PRINTLN("[DANCING] *** PROCESS_ANIMATION_CLIPS *** ")	
	PRINTLN("[DANCING] sDict ==  ", sDict)
	PRINTLN("[DANCING] fSoloDanceLength ==  ", ref_local.clipData.fSoloDanceLength)
	
	FLOAT fSecondBeatPhase
	INT i
	REPEAT COUNT_OF(DANCE_CLIP) i
		// Grab next clip from list 
		eClip = INT_TO_ENUM(DANCE_CLIP, i)
		sClip = GET_DANCE_CLIP_STRING(eClip)
		
		// Get phase of first and second beats
		BOOL bFirstBeatPhaseFound = FIND_ANIM_EVENT_PHASE(sDict, sClip, "FirstBeat", d.fFirstBeatPhase, fDummy) 
		BOOL bSecondBeatPhaseFound = FIND_ANIM_EVENT_PHASE(sDict, sClip, "SecondBeat", fSecondBeatPhase, fDummy) 
		
		#IF IS_DEBUG_BUILD
			// Debug output for variables used for dance signal
			IF GET_COMMANDLINE_PARAM_EXISTS("playerDanceSignalDebug")
				PRINTLN("[DANCING][PROCESS_ANIMATION_CLIPS] Dict:", sDict, " Clip: ", sClip, " FirstBeat phase, Found= ", bFirstBeatPhaseFound, " Phase= ", d.fFirstBeatPhase)
				PRINTLN("[DANCING][PROCESS_ANIMATION_CLIPS] Dict: ", sDict, " Clip: ", sClip, " SecondBeat phase, Found= ", bSecondBeatPhaseFound, " Phase= ", fSecondBeatPhase)
			ENDIF
		#ENDIF
		
		IF NOT bFirstBeatPhaseFound
			ASSERTLN("[PLAYER_DANCING][PROCESS_ANIMATION_CLIPS] First beat event not found on ", sClip, " animation clip")
		ENDIF
		
		IF NOT bSecondBeatPhaseFound
			ASSERTLN("[PLAYER_DANCING][PROCESS_ANIMATION_CLIPS] Second beat event not found on ", sClip, " animation clip")
		ENDIF
		
		// Get the difference in phase between the two known beats
		d.fBeatPhaseDiff = fSecondBeatPhase - d.fFirstBeatPhase
		
		// Work out approx beats per second
		FLOAT fBeatsPerFullAnim = (1.0 / d.fBeatPhaseDiff) + 1
		d.fBps = fBeatsPerFullAnim / ref_local.clipData.fSoloDanceLength
		
		// Store data
		ref_local.clipData.soloDanceData[i] = d
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[DANCING] ")
		PRINT_DANCE_CLIP_DATUM(d, sClip)
		#ENDIF
	ENDREPEAT
ENDPROC

PROC _DANCE_MAINTAINED_CACHED_VALUES(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	ref_local.piLocalPlayer = PLAYER_ID()
	ref_local.iLocalPlayer = NATIVE_TO_INT(ref_local.piLocalPlayer)
	ref_local.pedLocalPlayer = PLAYER_PED_ID()
	ref_local.bLocalPlayerOk = IS_NET_PLAYER_OK(ref_local.piLocalPlayer)
	ref_local.bLocalPlayerPedAlive = IS_ENTITY_ALIVE(ref_local.pedLocalPlayer)
ENDPROC

/// PURPOSE:
///    Initialise dance system. This is called automatically on first updates
PROC DANCE_INIT(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	_DANCE_MAINTAINED_CACHED_VALUES(ref_local)

	ref_local.iDanceStyle = GET_MP_INT_CHARACTER_STAT(ref_local.instance.ePiMenuDanceStyleStat)
	ref_local.iNewDanceStyle = ref_local.iDanceStyle
	
	ref_local.iContext = NEW_CONTEXT_INTENTION
	ref_local.eContext = DCI_INVALID
	
	#IF DANCING_FEATURE_DUAL_DANCE
	ref_local.piDualDanceTarget = INVALID_PLAYER_INDEX()
	#ENDIF
	
	// Start loading anims
	DANCE_LOAD_ALL_CLIPSETS(ref_local)
	
	#IF DANCING_FEATURE_DUAL_DANCE
	NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(TRUE)
	DANCING_SET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local, INVALID_PLAYER_INDEX())
	
	#IF IS_DEBUG_BUILD
		RESERVE_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS() + 1)
	#ENDIF
	
	#ENDIF
		
	DANCE_SET_STAGE(ref_local, DLS_LOADING)
ENDPROC

PROC DANCE_LOADING(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	#IF DANCING_FEATURE_DUAL_DANCE
	BOOL bDualDancesLoaded = DANCING_LOAD_ALL_DUAL_DANCING_ANIM_DICTS(ref_local)
	#ENDIF
	
	BOOL bLoadedMoveNetwork = DANCE_LOAD_ALL_CLIPSETS(ref_local)
	
	// ** LOAD ACTIONS **
	BOOL bLoadedActions = TRUE
	INTERACTION_DATA interaction
	INT iInteraction = 0
	REPEAT MAX_PLAYER_INTERACTIONS iInteraction
		IF IS_PLAYER_INTERACTION_ACTION_BLOCKED(iInteraction)
		OR NOT DANCING_IS_INTERACTION_A_DANCE_INTERACTION(iInteraction) // We can't load that many anims in!
			RELOOP
		ENDIF
		
		FillInteractionData(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), iInteraction, interaction)
		STRING sDict = TEXT_LABEL_TO_STRING(interaction.strAnimDict[AD_DEFAULT])

		IF IS_STRING_NULL_OR_EMPTY(sDict)
			RELOOP
		ENDIF
		
		REQUEST_ANIM_DICT(sDict)
		IF bLoadedActions
		AND NOT HAS_ANIM_DICT_LOADED(sDict)
			bLoadedActions = FALSE
		ENDIF
	ENDREPEAT
	
	IF bLoadedMoveNetwork AND bLoadedActions
	#IF DANCING_FEATURE_DUAL_DANCE
	AND bDualDancesLoaded
	#ENDIF
		// We have to process this here, not in INIT, because we require the clips to be loaded.
		PROCESS_ANIMATION_CLIPS(ref_local) 
		DANCE_SET_STAGE(ref_local, DLS_IDLE_NOT_DANCING)
	ENDIF
ENDPROC

PROC RESET_DONE_INITIAL_HELP_CLEAR(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	ref_local.bDoneInitialHelpClear = FALSE
ENDPROC

STRUCT CONTEXT_DETAILS
	CONTEXT_PRIORITY_ENUM ePriority = CP_HIGH_PRIORITY
	STRING sHelp = NULL
	BOOL bNoHelp = FALSE
	BOOL bForever = FALSE
	BOOL bHoldButton = FALSE
	BOOL bNoIntention = FALSE // No intention should be registered, just show help text.
	BOOL bUseAltPauseAsContext = FALSE	
	BOOL bActionLabel = FALSE
	BOOL bStyleLabel = FALSE
	BOOL bLockedLabel = FALSE
	BOOL bAltControlScheme = FALSE
	INT iOverrideDuration = -1
ENDSTRUCT

FUNC BOOL IS_CURRENT_LANGUAGE_TOO_COMPLEX()
	IF g_bSetCurrentLanguageComplexForDancingHelp
		RETURN TRUE
	ENDIF
	
	SWITCH GET_CURRENT_LANGUAGE()
		CASE LANGUAGE_JAPANESE			RETURN TRUE
		CASE LANGUAGE_RUSSIAN			RETURN TRUE
		CASE LANGUAGE_FRENCH			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC DANCING_ENABLE_DANCE_CAM_HELP(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bEnabled)
	IF bEnabled = ref_local.bShouldShowDanceCamHelp
		EXIT
	ENDIF
	
	ref_local.bShouldShowDanceCamHelp = bEnabled
	
	IF IS_PLAYER_DANCING(ref_local.piLocalPlayer) AND NOT DANCING_IS_LOCAL_PLAYER_DUAL_DANCING(ref_local)
		ref_local.eContext = DCI_INVALID // Give the help text system a kick
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the help label for the context & whether it should be displayed with bShowHelp.
///    Pass in a fresh CONTEXT_DETAILS.
FUNC BOOL GET_DANCE_CONTEXT_INTENTION_DATA(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_CONTEXT_INTENTION eType, CONTEXT_DETAILS& ref_details)	
	SWITCH eType
		CASE DCI_SOLO
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
				ref_details.sHelp = "DANCE_0"
			ELSE
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ref_details.sHelp = "DANCE_ALT_KB"
					ref_details.bAltControlScheme = TRUE
				ELSE
					ref_details.sHelp = "DANCE_ALT"
					ref_details.bAltControlScheme = TRUE
				ENDIF
				
				ref_details.bNoIntention = TRUE
			ENDIF
			
			RETURN TRUE
		
		CASE DCI_NO_MORE_COMBO
			ref_details.sHelp = "COMBO_STOP"
			ref_details.bNoIntention = TRUE
			ref_details.iOverrideDuration = 5000
			RETURN TRUE
		
		CASE DCI_HIDE_CONTROLS
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
			OR IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
				OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
					ref_details.bAltControlScheme = TRUE
				ENDIF
				
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ref_details.bUseAltPauseAsContext = TRUE
				ENDIF
				
				ref_details.sHelp = "DANCE_MIN"
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
				ref_details.sHelp = "DANCE_MIN_ALT"
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bAltControlScheme = TRUE
			ENDIF
			
			RETURN TRUE

		CASE DCI_SOLO_STOP_ANYWHERE
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ref_details.sHelp = "DANCE_S_K"
					ref_details.bUseAltPauseAsContext = TRUE
				ELSE
					ref_details.sHelp = "DANCE_S"
				ENDIF
				
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bActionLabel = TRUE
				ref_details.bStyleLabel = TRUE
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
				ref_details.sHelp = "DANCE_PROP_S"
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bActionLabel = TRUE
				ref_details.bStyleLabel = TRUE
				ref_details.bLockedLabel = TRUE
				ref_details.bAltControlScheme = TRUE
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
				ref_details.sHelp = "DANCE_ALTA_S"
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bActionLabel = TRUE
				ref_details.bStyleLabel = TRUE
				ref_details.bLockedLabel = TRUE
				ref_details.bAltControlScheme = TRUE
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					IF ref_local.instance.bShowBoilerCamOption AND ref_local.bShouldShowDanceCamHelp
						ref_details.sHelp = "DANCE_B_K_C_B"
					ELIF CAN_PLAYER_USE_DANCE_CAMERA(ref_local)
						ref_details.sHelp = "DANCE_B_K_C"
					ELSE
						ref_details.sHelp = "DANCE_B_K"
					ENDIF
					
					ref_details.bUseAltPauseAsContext = TRUE
					ref_details.bAltControlScheme = TRUE
				ELSE
					ref_details.bAltControlScheme = TRUE
					ref_details.bLockedLabel = TRUE
					
					IF ref_local.instance.bShowBoilerCamOption AND ref_local.bShouldShowDanceCamHelp
						ref_details.sHelp = "DANCE_B_S_C_B"
					ELIF CAN_PLAYER_USE_DANCE_CAMERA(ref_local)
						ref_details.sHelp = "DANCE_B_S_C"
					ELSE
						ref_details.sHelp = "DANCE_B_S"
					ENDIF
				ENDIF
				
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bActionLabel = TRUE
				ref_details.bStyleLabel = TRUE
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
				ref_details.sHelp = "DANCE_ALTC_S"
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bActionLabel = TRUE
				ref_details.bStyleLabel = TRUE
				ref_details.bLockedLabel = TRUE
				ref_details.bAltControlScheme = TRUE
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
				ref_details.sHelp = "DANCE_ALTD_S"
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bActionLabel = TRUE
				ref_details.bStyleLabel = TRUE
				ref_details.bLockedLabel = TRUE
				ref_details.bAltControlScheme = TRUE
			ENDIF
			
			RETURN TRUE
		
		CASE DCI_SOLO_STOP_ANYWHERE_DRUNK
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ref_details.sHelp = "DANCE_S_D_K"
					ref_details.bUseAltPauseAsContext = TRUE
				ELSE
					ref_details.sHelp = "DANCE_S_D"
				ENDIF
				
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bStyleLabel = TRUE
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
				ref_details.sHelp = "DANCE_PROP_D"
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bStyleLabel = TRUE
				ref_details.bLockedLabel = TRUE
				ref_details.bAltControlScheme = TRUE
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
				ref_details.sHelp = "DANCE_ALTA_D"
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bStyleLabel = TRUE
				ref_details.bLockedLabel = TRUE
				ref_details.bAltControlScheme = TRUE
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ref_details.sHelp = "DANCE_S_K"
					ref_details.bUseAltPauseAsContext = TRUE
					ref_details.bAltControlScheme = TRUE
				ELSE
					ref_details.sHelp = "DANCE_ALTB_D"
					
					ref_details.bAltControlScheme = TRUE
				ENDIF
				
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bStyleLabel = TRUE
				ref_details.bLockedLabel = TRUE
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
				ref_details.sHelp = "DANCE_ALTC_D"
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bStyleLabel = TRUE
				ref_details.bLockedLabel = TRUE
				ref_details.bAltControlScheme = TRUE
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
				ref_details.sHelp = "DANCE_ALTD_D"
				ref_details.bNoIntention = TRUE
				ref_details.bForever = TRUE
				ref_details.bStyleLabel = TRUE
				ref_details.bLockedLabel = TRUE
				ref_details.bAltControlScheme = TRUE
			ENDIF
			
			RETURN TRUE
		
		CASE DCI_SOLO_ANYWHERE
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
				ref_details.bNoHelp = TRUE 	
				ref_details.ePriority = CP_MINIMUM_PRIORITY
				ref_details.bHoldButton = TRUE
			ELSE
				ref_details.bNoHelp = TRUE 	
				ref_details.ePriority = CP_MINIMUM_PRIORITY
				ref_details.bHoldButton = FALSE
				ref_details.bAltControlScheme = TRUE
			ENDIF
			
			RETURN TRUE
			
		#IF DANCING_FEATURE_DUAL_DANCE
		CASE DCI_DUAL_JOIN
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
				ref_details.sHelp = "DM_DD_JOIN"
			ELSE
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					ref_details.sHelp = "DM_DD_JOIN_K"
				ELSE
					ref_details.sHelp = "DM_DD_JOIN_ALT"
				ENDIF
			ENDIF
			
			RETURN TRUE	
		CASE DCI_DUAL_CONTROLS
			ref_details.sHelp = "DM_DD_CTRL"
			ref_details.bNoIntention = TRUE	
			ref_details.bForever = TRUE
			RETURN TRUE
		CASE DCI_DUAL_LEAD_CONTROLS
			ref_details.sHelp = "DM_DD_L_CTRL"
			ref_details.bNoIntention = TRUE	
			ref_details.bForever = TRUE
			RETURN TRUE
		#ENDIF
			
		DEFAULT RETURN FALSE
	ENDSWITCH
ENDFUNC

FUNC STRING GET_DANCE_STYLE_LABEL(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT index)
	DANCE_CLIPSET sClipset
	IF NOT GET_DANCE_CLIPSET_DATA(ref_local, index, sClipset)
		RETURN "STRING"
	ENDIF
	
	RETURN sClipset.sDisplayNameLabel
ENDFUNC

FUNC STRING GET_DANCE_LOCK_LABEL(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF ref_local.bDancingLocked
		RETURN "DANCE_LOCK"
	ENDIF
	
	RETURN "DANCE_UNLOCK"
ENDFUNC

FUNC BOOL IS_DANCE_HELP_MESSAGE_BEING_DISPLAYED(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT iDanceStyle)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DANCE_0")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DANCE_ALT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DANCE_ALT_KB")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("COMBO_STOP")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DANCE_MIN")
		RETURN TRUE
	ENDIF
	
	STRING sStyleLabel = GET_DANCE_STYLE_LABEL(ref_local, iDanceStyle)
	IF IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("DANCE_S_D_K", sStyleLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("DANCE_S_D", sStyleLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("DANCE_S_D_K_0", sStyleLabel)					OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("DANCE_S_D_K_1", sStyleLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("DANCE_S_D_0", sStyleLabel)						OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("DANCE_S_D_1", sStyleLabel)
		RETURN TRUE
	ENDIF
	
	STRING sActionLabel = INTERACTION_ANIMS_TEXT_LABEL(GET_MP_PLAYER_ANIM_TYPE(), GET_MP_PLAYER_ANIM_SETTING())
	IF IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_S_K", sStyleLabel, sActionLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_S", sStyleLabel, sActionLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_S_K_0", sStyleLabel, sActionLabel)	OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_S_K_1", sStyleLabel, sActionLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_S_0", sStyleLabel, sActionLabel)		OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_S_1", sStyleLabel, sActionLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_B_K", sStyleLabel, sActionLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_B_K_C", sStyleLabel, sActionLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_B_K_C_B", sStyleLabel, sActionLabel)
		RETURN TRUE
	ENDIF
	
	STRING sLockedLabel = GET_DANCE_LOCK_LABEL(ref_local)
	IF IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_PROP_S", sStyleLabel, sActionLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_ALTA_S", sStyleLabel, sActionLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_B_S", sStyleLabel, sActionLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_ALTC_S", sStyleLabel, sActionLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_ALTD_S", sStyleLabel, sActionLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_B_S_C", sStyleLabel, sActionLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_B_S_C_B", sStyleLabel, sActionLabel, sLockedLabel)
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_PROP_D", sStyleLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_ALTA_D", sStyleLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_ALTB_D", sStyleLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_ALTC_D", sStyleLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED("DANCE_ALTD_D", sStyleLabel, sLockedLabel)
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_B_S_0", sStyleLabel, sActionLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_B_S_C_0", sStyleLabel, sActionLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_B_S_C_B_0", sStyleLabel, sActionLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_B_S_1", sStyleLabel, sActionLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_B_S_C_1", sStyleLabel, sActionLabel, sLockedLabel)
	OR IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED("DANCE_B_S_C_B_1", sStyleLabel, sActionLabel, sLockedLabel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DANCE_RELEASE_CONTEXT_INTENTION(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	ref_local.eContext = DCI_INVALID
	RELEASE_CONTEXT_INTENTION(ref_local.iContext)
	ref_local.iContext = NEW_CONTEXT_INTENTION
	RESET_DONE_INITIAL_HELP_CLEAR(ref_local)
	
	IF IS_DANCE_HELP_MESSAGE_BEING_DISPLAYED(ref_local, ref_local.iDanceStyle)
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

PROC DANCING_HIDE_HELP_TEXT(PLAYER_DANCE_LOCAL_STRUCT &ref_local, BOOL bHidden)
	IF bHidden AND IS_PLAYER_DANCING(ref_local.piLocalPlayer)
		IF IS_DANCE_HELP_MESSAGE_BEING_DISPLAYED(ref_local, ref_local.iDanceStyle)
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
				PRINTLN("[DANCING] HIDE_HELP_TEXT - Cleared dance system help text")
			ENDIF
		ENDIF
	ENDIF
	
	ENABLE_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_HIDE_HELP_TEXT, bHidden)
	PRINTLN("[DANCING] HIDE_HELP_TEXT - Setting help text hidden: ", bHidden)
	DEBUG_PRINTCALLSTACK()
ENDPROC

FUNC BOOL DANCING_IS_HELP_TEXT_HIDDEN(PLAYER_DANCE_LOCAL_STRUCT &ref_local)
	RETURN IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_HIDE_HELP_TEXT)
ENDFUNC

PROC PRINT_DANCE_HELP(PLAYER_DANCE_LOCAL_STRUCT& ref_local, STRING sHelp, BOOL bStyleLabel, BOOL bActionLabel, BOOL bLockedLabel, BOOL bSound = TRUE, BOOL bForever = FALSE, INT iOverrideDuration = -1)
	IF IS_STRING_NULL_OR_EMPTY(sHelp)
		EXIT
	ENDIF
	
	IF DANCING_IS_HELP_TEXT_HIDDEN(ref_local)
		EXIT
	ENDIF
	
	IF bStyleLabel
		STRING sStyleLabel = GET_DANCE_STYLE_LABEL(ref_local, ref_local.iDanceStyle)
		IF bActionLabel
			STRING sActionLabel = INTERACTION_ANIMS_TEXT_LABEL(GET_MP_PLAYER_ANIM_TYPE(), GET_MP_PLAYER_ANIM_SETTING())
			IF bLockedLabel
				STRING sLockedLabel = GET_DANCE_LOCK_LABEL(ref_local)
				IF NOT IS_THIS_HELP_MESSAGE_WITH_3_STRINGS_BEING_DISPLAYED(sHelp, sStyleLabel, sActionLabel, sLockedLabel)
					BEGIN_TEXT_COMMAND_DISPLAY_HELP(sHelp)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sStyleLabel)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sActionLabel)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sLockedLabel)
					END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, bForever, bSound, iOverrideDuration)
				ENDIF
			ELSE
				IF NOT IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED(sHelp, sStyleLabel, sActionLabel)
					BEGIN_TEXT_COMMAND_DISPLAY_HELP(sHelp)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sStyleLabel)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sActionLabel)
					END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, bForever, bSound, iOverrideDuration)
				ENDIF
			ENDIF
		ELSE
			IF bLockedLabel
				STRING sLockedLabel = GET_DANCE_LOCK_LABEL(ref_local)
				IF NOT IS_THIS_HELP_MESSAGE_WITH_2_STRINGS_BEING_DISPLAYED(sHelp, sStyleLabel, sLockedLabel)
					BEGIN_TEXT_COMMAND_DISPLAY_HELP(sHelp)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sStyleLabel)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sLockedLabel)
					END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, bForever, bSound, iOverrideDuration)
				ENDIF
			ELSE
				IF NOT IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED(sHelp, sStyleLabel)
					BEGIN_TEXT_COMMAND_DISPLAY_HELP(sHelp)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sStyleLabel)
					END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, bForever, bSound, iOverrideDuration)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelp)
			BEGIN_TEXT_COMMAND_DISPLAY_HELP(sHelp)
			END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, bForever, bSound, iOverrideDuration)
		ENDIF
	ENDIF
ENDPROC

FUNC CONTROL_ACTION GET_DANCING_TRIGGER_CONTROL(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		RETURN INPUT_CONTEXT
	ENDIF
	
	SWITCH ref_local.eControlScheme
		CASE DANCE_CONTROL_SCHEME_STANDARD		RETURN INPUT_CONTEXT
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_1	RETURN INPUT_FRONTEND_LT
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_2	RETURN INPUT_FRONTEND_LT
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_3	RETURN INPUT_FRONTEND_LT
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_4	RETURN INPUT_FRONTEND_LT
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_5	RETURN INPUT_FRONTEND_LT
	ENDSWITCH
	
	RETURN INPUT_CONTEXT
ENDFUNC

/// PURPOSE:
///    Registers the context intention if not already registered, and returns true.
///    If bReleaseOnTrigger = TRUE the context is released when context input is detected.    
///    If the eType details from GET_DANCE_CONTEXT_INTENTION_DATA bNoIntention = TRUE then the
///    context intention system isn't used.
///    Uses fontend control to match context_intention system.
FUNC BOOL HAS_CONTEXT_INTENTION_TRIGGERED(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_CONTEXT_INTENTION eType, BOOL bReleaseOnTrigger = TRUE, PLAYER_INDEX piPlayerToAppend = NULL, BOOL bForceHelp = FALSE)
	IF eType = DCI_INVALID
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DUAL_DANCE_BLOCK")
		RETURN FALSE
	ENDIF
	
	BOOL bResult
	CONTEXT_DETAILS details
	IF NOT GET_DANCE_CONTEXT_INTENTION_DATA(ref_local, eType, details)
		ASSERTLN("Player dance help data not found for eType = ", ENUM_TO_INT(eType))
		RETURN FALSE
	ENDIF
	
	IF DANCING_IS_HELP_TEXT_HIDDEN(ref_local)
		PRINTLN("[DANCING] HAS_CONTEXT_INTENTION_TRIGGERED - help text is hidden")
		details.bNoHelp = TRUE
	ENDIF
	
	STRING strContextSubstring = NULL
	BOOL bPlayerNameIsSubstring = FALSE
	
	IF piPlayerToAppend != INVALID_PLAYER_INDEX()
		strContextSubstring = GET_PLAYER_NAME(piPlayerToAppend)
		bPlayerNameIsSubstring = TRUE
	ENDIF
	
	IF IS_CURRENT_LANGUAGE_TOO_COMPLEX()
		IF NOT HAS_NET_TIMER_STARTED(ref_local.tComplexDanceHelpDisplay)
			ref_local.iComplexDanceHelpNum = 0
			REINIT_NET_TIMER(ref_local.tComplexDanceHelpDisplay)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(ref_local.tComplexDanceHelpDisplay, 5000)
				ref_local.iComplexDanceHelpNum++
				IF ref_local.iComplexDanceHelpNum > 1
					ref_local.iComplexDanceHelpNum = 0
				ENDIF
				
				REINIT_NET_TIMER(ref_local.tComplexDanceHelpDisplay)
			ENDIF
		ENDIF
	ELSE
		ref_local.iComplexDanceHelpNum = -1
		RESET_NET_TIMER(ref_local.tComplexDanceHelpDisplay)
	ENDIF
	IF ref_local.iComplexDanceHelpNum != -1
	AND NOT IS_STRING_NULL_OR_EMPTY(details.sHelp)
		IF NOT ARE_STRINGS_EQUAL(details.sHelp, "DANCE_0")
		AND NOT ARE_STRINGS_EQUAL(details.sHelp, "COMBO_STOP")
		AND NOT ARE_STRINGS_EQUAL(details.sHelp, "DANCE_MIN")
		AND NOT ARE_STRINGS_EQUAL(details.sHelp, "DANCE_ALT")
		AND NOT ARE_STRINGS_EQUAL(details.sHelp, "DANCE_ALT_KB")
		AND NOT ARE_STRINGS_EQUAL(details.sHelp, "DANCE_MIN_ALT")
		AND NOT ARE_STRINGS_EQUAL(details.sHelp, "DM_DD_JOIN")
		AND NOT ARE_STRINGS_EQUAL(details.sHelp, "DM_DD_JOIN_ALT")
		AND NOT ARE_STRINGS_EQUAL(details.sHelp, "DM_DD_JOIN_K")
		AND NOT ARE_STRINGS_EQUAL(details.sHelp, "DM_DD_L_CTRL")
		AND NOT ARE_STRINGS_EQUAL(details.sHelp, "DM_DD_CTRL")
			TEXT_LABEL_63 tl63TempHelp = details.sHelp
			tl63TempHelp += "_"
			tl63TempHelp += (ref_local.iComplexDanceHelpNum)
			
			details.sHelp = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63TempHelp, 0, GET_LENGTH_OF_LITERAL_STRING(tl63TempHelp))
		ENDIF
	ENDIF
	
	IF eType <> ref_local.eContext
		PRINTLN(" Registering new help/context... ", details.sHelp)
		DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
		ref_local.eContext = eType
		ref_local.bDoneHelpSound = FALSE
	ENDIF
		
	IF details.bUseAltPauseAsContext
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
	
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
	ENDIF
	
	IF details.bAltControlScheme
	AND NOT details.bNoIntention
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
				PRINTLN("[DANCING] Accepting input for alt control scheme '",details.sHelp,"'")
				
				IF bReleaseOnTrigger
					DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
				ENDIF
				
				bResult = TRUE
			ENDIF
		ELSE
			IF GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT) >= g_fDancingControlNormal
				PRINTLN("[DANCING] Accepting input for alt control scheme '",details.sHelp,"'")
				
				IF bReleaseOnTrigger
					DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
				ENDIF
				
				bResult = TRUE
			ENDIF
		ENDIF
	ELIF details.bNoIntention
	OR details.bUseAltPauseAsContext
		PRINT_DANCE_HELP(ref_local, details.sHelp, details.bStyleLabel, details.bActionLabel, details.bLockedLabel, NOT ref_local.bDoneHelpSound, details.bForever, details.iOverrideDuration)
		ref_local.bDoneHelpSound = TRUE
		
		IF details.bAltControlScheme
			IF eType = DCI_SOLO
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
						IF bReleaseOnTrigger
							DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
						ENDIF
						
						bResult = TRUE
					ENDIF
				ELSE
					IF GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT) >= g_fDancingControlNormal
						IF bReleaseOnTrigger
							DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
						ENDIF
						
						bResult = TRUE
					ENDIF
				ENDIF
			ELSE
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
						IF bReleaseOnTrigger
							DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
						ENDIF
						
						bResult = TRUE
					ENDIF
				ELSE
					IF GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT) < g_fDancingControlNormal
					AND NOT ref_local.bDancingLocked
					AND NOT bForceHelp
						IF bReleaseOnTrigger
							DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
						ENDIF
						
						bResult = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELIF details.bHoldButton			
		AND NOT details.bUseAltPauseAsContext
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
				IF HAS_NET_TIMER_EXPIRED(ref_local.tContextHoldTime, ciHOLD_BUTTON_TIME_MS)
					RESET_NET_TIMER(ref_local.tContextHoldTime)
					PRINTLN("[DANCING] Accepting input for context intention '",details.sHelp,"' (button held)")	
					IF bReleaseOnTrigger
						DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
					ENDIF
					bResult = TRUE
				ENDIF
			ELSE
				RESET_NET_TIMER(ref_local.tContextHoldTime)
			ENDIF	
		ELIF ((NOT details.bUseAltPauseAsContext) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT))
		OR (details.bUseAltPauseAsContext AND IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE))
			PRINTLN("[DANCING] Accepting input for context intention '",details.sHelp,"'")	
			IF bReleaseOnTrigger
				DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
			ENDIF
			
			bResult = TRUE
		ELSE
			RESET_NET_TIMER(ref_local.tContextHoldTime)
		ENDIF
	ELSE
		IF ref_local.iContext = NEW_CONTEXT_INTENTION 
			PRINTLN("[DANCING] Registering context intention '", details.sHelp,"'")
			IF details.bHoldButton
				// We will manage the help text for a "hold" ourselves to stop flickering.
				REGISTER_CONTEXT_INTENTION(ref_local.iContext, details.ePriority, "", details.bNoHelp, strContextSubstring, DEFAULT, bPlayerNameIsSubstring)
			ELSE
				REGISTER_CONTEXT_INTENTION(ref_local.iContext, details.ePriority, details.sHelp, details.bNoHelp, strContextSubstring, DEFAULT, bPlayerNameIsSubstring)
			ENDIF
			bResult = FALSE
			
		// If the context system has been cleared from somewhere else etc...
		ELIF GET_CONTEXT_INTENTION_ARRAY_INDEX(ref_local.iContext) < 0
			PRINTLN("[DANCING] Releasing and registering context intention '",details.sHelp,"'")	
			REGISTER_CONTEXT_INTENTION(ref_local.iContext, details.ePriority, details.sHelp, details.bNoHelp, strContextSubstring, DEFAULT, bPlayerNameIsSubstring)
			ref_local.bDoneInitialHelpClear = FALSE
			bResult = FALSE
			
		ELIF details.bHoldButton
			IF IS_CONTEXT_INTENTION_TOP(ref_local.iContext)
				// We have context priority so we can print help...
				PRINT_DANCE_HELP(ref_local, details.sHelp, details.bStyleLabel, details.bActionLabel, details.bLockedLabel, NOT ref_local.bDoneHelpSound)
				ref_local.bDoneHelpSound = TRUE 
				
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					IF HAS_NET_TIMER_EXPIRED(ref_local.tContextHoldTime, ciHOLD_BUTTON_TIME_MS)
						RESET_NET_TIMER(ref_local.tContextHoldTime)
						PRINTLN("[DANCING] Accepting input for context intention '",details.sHelp,"' (button held)")	
						IF bReleaseOnTrigger
							DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
						ENDIF
						bResult = TRUE
					ENDIF
				ELSE
					RESET_NET_TIMER(ref_local.tContextHoldTime)
				ENDIF	
			ELSE
				RESET_NET_TIMER(ref_local.tContextHoldTime)
			ENDIF		
		ELIF HAS_CONTEXT_BUTTON_TRIGGERED(ref_local.iContext)
			PRINTLN("[DANCING] Accepting input for context intention '",details.sHelp,"'")	
			IF bReleaseOnTrigger
				DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
			ENDIF
			
			bResult = TRUE
		ELSE
			RESET_NET_TIMER(ref_local.tContextHoldTime)
		ENDIF
	ENDIF
	
	IF NOT ref_local.bDoneInitialHelpClear
	AND NOT details.bNoHelp
	AND NOT details.bNoIntention
	AND IS_CONTEXT_INTENTION_TOP(ref_local.iContext)
	AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CLUB_SAFE_HT") // B*4782848
	AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DM_AREA_BLOCK")
	AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DUAL_DANCE_BLOCK")
		PRINTLN("[DANCING] Doing inital clear of '",details.sHelp,"' because we now have context priority")	
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF
		ref_local.bDoneInitialHelpClear = TRUE
	ENDIF
	
	RETURN bResult
ENDFUNC

PROC DANCE_INITIALISE_SINGALS_FOR_DANCING(PLAYER_DANCE_LOCAL_STRUCT& ref_local, FLOAT fXSignal = 0.5, FLOAT fYSignal = 0.5, FLOAT fIntensitySignal = 0.0)
	ref_local.v2DirectionSignals.y 		= fYSignal
	ref_local.v2DirectionSignals.x 		= fXSignal
	ref_local.fCurrentIntensitySignal 	= fIntensitySignal
	ref_local.fTrueIntensitySignal 		= fIntensitySignal
	ref_local.bApplyBoostMultipler 		= FALSE
ENDPROC

PROC DANCE_SET_BEAT_MATCH_STATE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_BEAT_MATCH_STATE eState)
	CDEBUG1LN(DEBUG_NET_DANCING,"[DANCING] BEAT MATCH STATE [ ",ENUM_TO_INT(ref_local.eBeatMatchState)," ] to [ ",ENUM_TO_INT(eState)," ]")
	ref_local.eBeatMatchState = eState
ENDPROC

// The type of anim MUST be INTERACTION_ANIM_TYPE_PLAYER
FUNC INT DANCE_GET_FLOURISH_ACTION_CLIPSET_HASH(INT iActionAnim)	
	PLAYER_INTERACTIONS eClip = INT_TO_ENUM(PLAYER_INTERACTIONS, iActionAnim)
	SWITCH eClip
		CASE PLAYER_INTERACTION_AIR_GUITAR		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@AIR_GUITAR")
		CASE PLAYER_INTERACTION_AIR_SHAGGING	RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@AIR_SHAGGING")
		CASE PLAYER_INTERACTION_AIR_SYNTH		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@AIR_SYNTH")
		CASE PLAYER_INTERACTION_BLOW_KISS		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@BLOW_KISS")
		CASE PLAYER_INTERACTION_CHICKEN_TAUNT	RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@CHICKEN_TAUNT")
		CASE PLAYER_INTERACTION_CHIN_BRUSH		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@CHIN_BRUSH")
		CASE PLAYER_INTERACTION_DJ				RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@DJ")
		CASE PLAYER_INTERACTION_DOCK			RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@DOCK")
		CASE PLAYER_INTERACTION_FACE_PALM		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@FACE_PALM")
		CASE PLAYER_INTERACTION_FINGER			RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@FINGER")
		CASE PLAYER_INTERACTION_FINGER_KISS		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@FINGER_KISS")
		CASE PLAYER_INTERACTION_FREAKOUT 		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@FREAKOUT")
		CASE PLAYER_INTERACTION_JAZZ_HANDS		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@JAZZ_HANDS")
		CASE PLAYER_INTERACTION_KNUCKLE_CRUNCH	RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@KNUCKLE_CRUNCH")
		CASE PLAYER_INTERACTION_NOSE_PICK		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@NOSE_PICK")
		CASE PLAYER_INTERACTION_NO_WAY			RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@NO_WAY")
		CASE PLAYER_INTERACTION_PEACE			RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@PEACE")
		CASE PLAYER_INTERACTION_PHOTOGRAPHY		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@PHOTOGRAPHY")
		CASE PLAYER_INTERACTION_ROCK			RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@ROCK")
		CASE PLAYER_INTERACTION_SALUTE			RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@SALUTE")
		CASE PLAYER_INTERACTION_SHUSH			RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@SHUSH")
		CASE PLAYER_INTERACTION_SLOW_CLAP		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@SLOW_CLAP")
		CASE PLAYER_INTERACTION_SURRENDER		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@SURRENDER")
		CASE PLAYER_INTERACTION_THUMBS_UP		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@THUMBS_UP")
		CASE PLAYER_INTERACTION_THUMB_ON_EARS	RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@THUMB_ON_EARS")
		//CASE PLAYER_INTERACTION_@@?? what is this? RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@V_SIGN") <- ANIM@MP_PLAYER_INTUPPERV_SIGN - UNUSED?
		CASE PLAYER_INTERACTION_WANK			RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@WANK")
		CASE PLAYER_INTERACTION_YOU_LOCO		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@YOU_LOCO")
		CASE PLAYER_INTERACTION_BANGING_TUNES	RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@BANGING_TUNES")
		CASE PLAYER_INTERACTION_BANGING_TUNES_LEFT	RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@BANGING_TUNES_LEFT")
		CASE PLAYER_INTERACTION_BANGING_TUNES_RIGHT	RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@BANGING_TUNES_RIGHT")
		CASE PLAYER_INTERACTION_CATS_CRADLE		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@CATS_CRADLE")
		CASE PLAYER_INTERACTION_FIND_THE_FISH	RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@FIND_THE_FISH")
		CASE PLAYER_INTERACTION_HEART_PUMPING	RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@HEART_PUMPING")
		CASE PLAYER_INTERACTION_OH_SNAP			RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@OH_SNAP")
		CASE PLAYER_INTERACTION_RAISE_THE_ROOF	RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@RAISE_THE_ROOF")
		CASE PLAYER_INTERACTION_SALSA_ROLL		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@SALSA_ROLL")
		CASE PLAYER_INTERACTION_UNCLE_DISCO		RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@UNCLE_DISCO")
		
		#IF FEATURE_HEIST_ISLAND_DANCES
		CASE PLAYER_INTERACTION_CROWD_INVITATION	RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@CROWD_INVITATION")
		CASE PLAYER_INTERACTION_DRIVER				RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@DRIVER")
		CASE PLAYER_INTERACTION_RUNNER				RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@RUNNER")
		CASE PLAYER_INTERACTION_SHOOTING			RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@SHOOTING")
		CASE PLAYER_INTERACTION_SUCK_IT				RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@SUCK_IT")
		CASE PLAYER_INTERACTION_TAKE_SELFIE			RETURN HASH("NIGHTCLUB@DANCE_MINIGAME@UPPERBODY@TAKE_SELFIE")
		#ENDIF
		
	ENDSWITCH
	
	PRINTLN("[DANCING] DANCE_GET_FLOURISH_ACTION_CLIPSET_HASH - failed to get clipset hash for iActionAnim: ", iActionAnim)
	RETURN 0
ENDFUNC

PROC TRIGGER_DANCING_INTRO(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	PRINTLN("[DANCING] TRIGGER_DANCING_INTRO")	
	
	// If the player has never danced then show them the full help
	// text for 15 seconds when they start dancing, otherwise just 
	// start minimised.
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PLAYER_HAS_DANCED)
		PRINTLN("[DANCING] Player has danced before - starting help minimised.")
		IF NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_CONTROLS)
			SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_MINIMISED_HELP)
		ENDIF
		RESET_NET_TIMER(ref_local.tHideHelpTimer) // Make sure the timer isn't active from our first dance
	ELSE	
		RESET_NET_TIMER(ref_local.tHideHelpTimer)
		START_NET_TIMER(ref_local.tHideHelpTimer)
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PLAYER_HAS_DANCED, TRUE)
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_MINIMISED_HELP)
	ENDIF
	
	ref_local.iBeatCountSinceLastBoost = 100
		
	DANCING_ENABLE_INTERACTION_MENU(ref_local, FALSE)
	DANCE_INITIALISE_SINGALS_FOR_DANCING(ref_local, ref_local.fXSignalVelocity, ref_local.fYSignalVelocity, ref_local.fIntensitySignalMax)

	
	// Start loading the scaleform movie
	DANCE_HAS_SCALEFORM_LOADED(ref_local)
	
	// Reset this to force the scaleform to update
	CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_USING_KEYBOARD)

	SHAKE_GAMEPLAY_CAM("CLUB_DANCE_SHAKE", cfSHAKE_AMP_MIN)	
		
	DANCE_SET_BEAT_MATCH_STATE(ref_local, DBMS_DEAFULT)	
	
	DANCE_SCALEFORM_RESET_UI(ref_local)
	
	// Save existing action & type, if it isn't valid then choose default option
	ref_local.iSavedActionAnim = GET_MP_PLAYER_ANIM_SETTING()
	ref_local.iSavedActionType = GET_MP_PLAYER_ANIM_TYPE()
	IF (ref_local.iSavedActionType <> ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER))
	OR (DANCE_GET_FLOURISH_ACTION_CLIPSET_HASH(ref_local.iSavedActionAnim) = 0)
		SET_MP_PLAYER_ANIM_SETTING(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), ENUM_TO_INT(PLAYER_INTERACTION_BANGING_TUNES))
		
		SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_OVERRIDE_ACTION)
	ENDIF
		
	DANCE_SET_STAGE(ref_local, DLS_DANCING_INTRO)
ENDPROC

PROC RESET_BEAT_MATCH_STATS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	DANCE_STATS_STRUCT newStats
	ref_local.stats = newStats
	PRINTLN("[DANCING][DANCE_STATS] RESET_BEAT_MATCH_STATS")
ENDPROC

PROC DANCE_PAUSE_COMBO_TIMER_THIS_FRAME(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF ref_local.stats.iConsecutiveBeatHits <> 0
		NET_TIMER_PAUSE_THIS_FRAME(ref_local.stats.tFirstHitTime, ref_local.stats.tFirstHitPauseTime)
	ENDIF
ENDPROC

PROC UPDATE_BEAT_MATCH_STATS(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bMiss, BOOL bFail, BOOL bHit, BOOL bPause)
	IF bPause
		DANCE_PAUSE_COMBO_TIMER_THIS_FRAME(ref_local)
	ELIF bHit
		NET_TIMER_PAUSE_RESET(ref_local.stats.tFirstHitPauseTime)
		IF ref_local.stats.iConsecutiveBeatHits = 0
			ref_local.stats.iComboMins = 0
			ref_local.stats.iRpPeriodCount = 0
			RESET_NET_TIMER(ref_local.stats.tFirstHitTime)
			START_NET_TIMER(ref_local.stats.tFirstHitTime)
			PRINTLN("[DANCING][DANCE_STATS] Hit : Starting timer at game time (ms) ", GET_GAME_TIMER())	
			PRINTLN("[DANCING][DANCE_STATS] Hit : Broke fail streak of ", ref_local.stats.iConsecutiveBeatMissFails)
		ENDIF
			
		ref_local.stats.iConsecutiveBeatHits++
		ref_local.stats.iConsecutiveBeatMissFails = 0
			
	ELIF bMiss OR bFail
		#IF IS_DEBUG_BUILD
		IF ref_local.stats.iConsecutiveBeatHits > 0
			PRINTLN("[DANCING][DANCE_STATS] Miss (",bMiss,") or fail (",bFail,") : Broke combo streak of ", ref_local.stats.iConsecutiveBeatHits)
		ENDIF
		#ENDIF
		ref_local.stats.iConsecutiveBeatMissFails++
		ref_local.stats.iConsecutiveBeatHits = 0
		ref_local.stats.iRpPeriodCount = 0
		ref_local.stats.iComboMins = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the number of consecutive beat matches in the current combo.
FUNC INT GET_PLAYER_BEAT_MATCH_COMBO(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN ref_local.stats.iConsecutiveBeatHits
ENDFUNC

/// PURPOSE:
/// Get how long has the player has been consecutively hitting beats without messing up (ms).
FUNC INT GET_PLAYER_BEAT_MATCH_COMBO_DURATION(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF ref_local.stats.iConsecutiveBeatHits = 0
		RETURN 0
	ELSE
		RETURN GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.stats.tFirstHitTime)
	ENDIF
ENDFUNC

/// PURPOSE:
///    Disable player movement controls.
PROC DANCE_SUPPRESS_MOVEMENT()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
ENDPROC

/// PURPOSE:
///    Does final checks to make sure the player can actually dance right now.
PROC DANCE_MAINTAIN_DANCING_INTRO(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	DANCE_SUPPRESS_MOVEMENT()
	
	PED_INDEX pedLocal = ref_local.pedLocalPlayer
	IF IS_PED_STILL(pedLocal)
	AND VMAG2(GET_ENTITY_VELOCITY(pedLocal)) <= 0.1
		BOOL bCanDance = TRUE
		
		// Do another full locate check now that we are still
		IF IS_PLAYER_IN_ANY_BLOCK_AREA(ref_local)
			SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_WAITING_TO_PRINT_STAIRS)
			RESET_NET_TIMER(ref_local.tContextHoldTime)
			bCanDance = FALSE
		ENDIF
		
		IF bCanDance
		AND NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PLAYER_IS_BUSY)
			PLAYSTATS_MINIGAME_USAGE(MINIGAME_TELEMETRY_DANCING, ref_local.instance.iLocationHash)
			RESET_BEAT_MATCH_STATS(ref_local)
			HISTORY_RESET(ref_local.inputHistory)
			DANCE_SET_STAGE(ref_local, DLS_DANCING)
		ELSE
			IF NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PLAYER_IS_BUSY)
				SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_WAITING_TO_PRINT_STAIRS)
			ENDIF
			DANCE_UNLOAD_SCALEFORM(ref_local)
			DANCE_SET_STAGE(ref_local, DLS_IDLE_NOT_DANCING)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PREVENT_PLAYER_STARTING_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF IS_BIT_SET(g_SimpleInteriorData.iForthBs, BS4_SIMPLE_INTERIOR_BLOCK_PLAYER_DANCING_PROMPT_THIS_FRAME)
		CLEAR_BIT(g_SimpleInteriorData.iForthBs, BS4_SIMPLE_INTERIOR_BLOCK_PLAYER_DANCING_PROMPT_THIS_FRAME)
		RETURN TRUE
	ENDIF
	
	IF NOT ref_local.bLocalPlayerOk
		RETURN TRUE
	ENDIF

	PED_INDEX pedLocal = ref_local.pedLocalPlayer
	// We don't want to give help text or let the player dance outside of a locate if they 
	// are being forced to move away from an area
	SCRIPTTASKSTATUS eMoveToCoordTask = GET_SCRIPT_TASK_STATUS(pedLocal, SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
	IF eMoveToCoordTask = WAITING_TO_START_TASK 
	OR eMoveToCoordTask = PERFORMING_TASK
		PRINTLN("[DANCING] SHOULD_PREVENT_PLAYER_STARTING_DANCE - Player is being tasked to move.")	
		RETURN TRUE
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("spawn_activities")) > 0 OR g_TransitionSpawnData.bSpawnActivityReady
		PRINTLN("[DANCING]SHOULD_PREVENT_PLAYER_STARTING_DANCE - Local player wakeing up drunk")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PLAYER_IS_BUSY)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(pedLocal, TRUE)
		RETURN TRUE
	ENDIF
	
	INT iAreaIndex
	IF NOT ref_local.instance.bCanDanceAnywhere AND
	NOT IS_PLAYER_IN_ANY_DANCE_AREA(ref_local, iAreaIndex)
		RETURN TRUE
	ENDIF
	
	// No dancing with the mermaids
	IF IS_ENTITY_IN_WATER(pedLocal) OR IS_PED_SWIMMING(pedLocal)
		PRINTLN("[DANCING] SHOULD_PREVENT_PLAYER_STARTING_DANCE - Player is in water")
		RETURN TRUE
	ENDIF
	
	// Or the birds
	IF IS_ENTITY_IN_AIR(pedLocal) 
		PRINTLN("[DANCING] SHOULD_PREVENT_PLAYER_STARTING_DANCE - Player is in air")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_ON_FIRE(pedLocal)
		PRINTLN("[DANCING] SHOULD_PREVENT_PLAYER_STARTING_DANCE - Player is on fire")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_CUTSCENE(ref_local.piLocalPlayer)
		PRINTLN("[DANCING] SHOULD_PREVENT_PLAYER_STARTING_DANCE - Player is in a cutscene")
		RETURN TRUE
	ENDIF
	
	IF NOT ref_local.bLocalPlayerOk
	OR NOT IS_PLAYER_CONTROL_ON(ref_local.piLocalPlayer)
	OR IS_ANY_INTERACTION_ANIM_PLAYING()
	OR IS_PAUSE_MENU_ACTIVE_EX()
	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(ref_local.piLocalPlayer)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DANCE_STYLE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF GET_FRAME_COUNT() % 4 = 0
		EXIT
	ENDIF
	
	INT iPiMenuOption = GET_MP_INT_CHARACTER_STAT(ref_local.instance.ePiMenuDanceStyleStat)
	IF ref_local.iDanceStyle <> iPiMenuOption
		ref_local.iDanceStyle = iPiMenuOption
		ref_local.iNewDanceStyle = iPiMenuOption
		#IF IS_DEBUG_BUILD
		DANCE_CLIPSET clipset
		GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iDanceStyle, clipset)
		PRINTLN("[DANCING] Changing dance style to clipset ", clipset.sClipset)
		#ENDIF //IS_DEBUG_BUILD
	ENDIF
ENDPROC

#IF DANCING_FEATURE_DUAL_DANCE
FUNC BOOL DANCING_MAINTAIN_TRIGGERING_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF SHOULD_PREVENT_PLAYER_STARTING_DANCE(ref_local)
		RETURN FALSE
	ENDIF
	
	IF NOT DANCING_IS_LOCAL_PLAYER_ABLE_TO_TRIGGER_DUAL_DANCE(ref_local, ref_local.piDualDanceTarget, cfDANCING_DUAL_DANCE_FACING_ACCURACY)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DANCING_DEBUG_DRAW_DUAL_DANCE_RAYCAST_CHECK(ref_local.piDualDanceTarget)
	#ENDIF
	
	PRINTLN("[DANCING][DUAL] DANCE_MAINTAIN_IDLE_NOT_DANCING - Showing dual dance prompt")
	HAS_CONTEXT_INTENTION_TRIGGERED(ref_local, DCI_DUAL_JOIN, DEFAULT, ref_local.piDualDanceTarget) // Join dual dance

	
	IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
	OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF (IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL))
			// Start normal dance
			TRIGGER_DANCING_INTRO(ref_local)
			ENABLE_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA, IS_PLAYER_IN_ANY_DANCE_AREA(ref_local, ref_local.iDanceAreaIndex))
			RETURN TRUE
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
			// Start normal dance
			TRIGGER_DANCING_INTRO(ref_local)
			ENABLE_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA, IS_PLAYER_IN_ANY_DANCE_AREA(ref_local, ref_local.iDanceAreaIndex))
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT) >= g_fDancingControlNormal
			// Start normal dance
			TRIGGER_DANCING_INTRO(ref_local)
			ENABLE_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA, IS_PLAYER_IN_ANY_DANCE_AREA(ref_local, ref_local.iDanceAreaIndex))
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
		DANCING_START_DUAL_DANCE_SHAPE_TEST(ref_local, ref_local.piDualDanceTarget)
		PRINTLN("[DANCING][SHAPE_TEST] DANCE_MAINTAIN_IDLE_NOT_DANCING - Starting shape test")
	ENDIF
	
	BOOL bHitSomething = FALSE
	
	IF DANCING_GET_DUAL_DANCE_HAS_PASSED_SHAPE_TEST(ref_local, bHitSomething) = SHAPETEST_STATUS_RESULTS_READY
		IF bHitSomething
			PRINTLN("[DANCING][SHAPE_TEST] DANCE_MAINTAIN_IDLE_NOT_DANCING - Collision check failed, showing you can't dance here prompt")
			SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_DUAL_DANCE_BLOCKED)
			DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
			RESET_NET_TIMER(ref_local.tContextHoldTime)
			RETURN TRUE
		ELSE
			DANCING_JOIN_DUAL_DANCE_WITH_PLAYER(ref_local, ref_local.piDualDanceTarget, TRUE)
			DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
			PRINTLN("[DANCING][SHAPE_TEST] DANCE_MAINTAIN_IDLE_NOT_DANCING - Collision check passed")
			RETURN TRUE
		ENDIF
	ENDIF
	
	// So we block all other prompts and only show the dual dance option prompt
	RETURN TRUE
ENDFUNC
#ENDIF

/// PURPOSE:
///    Update for when the dancing system is ready & player is not dancing
PROC DANCE_MAINTAIN_IDLE_NOT_DANCING(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF HAS_NET_TIMER_STARTED(ref_local.tRPDurationMinute)
		RESET_NET_TIMER(ref_local.tRPDurationMinute)
	ENDIF	
	
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA)
		SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA_PREV)
	ELSE
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA_PREV)
	ENDIF
	
	MAINTAIN_DANCE_STYLE(ref_local)
	
	#IF DANCING_FEATURE_DUAL_DANCE
	IF DANCING_MAINTAIN_TRIGGERING_DUAL_DANCE(ref_local)
		EXIT
	ENDIF
	#ENDIF
	
	IF SHOULD_PREVENT_PLAYER_STARTING_DANCE(ref_local)
		DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
		EXIT
	ENDIF
	
	// Check to see if player is trying to dance anywhere in a restricted area (stairs?)
	IF IS_PLAYER_IN_ANY_BLOCK_AREA(ref_local)
		DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, GET_DANCING_TRIGGER_CONTROL(ref_local))
			SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_WAITING_TO_PRINT_STAIRS)
		ENDIF
		RESET_NET_TIMER(ref_local.tContextHoldTime)
		EXIT
	ENDIF
	
	IF IS_PLAYER_IN_ANY_DANCE_AREA(ref_local, ref_local.iDanceAreaIndex)
		IF HAS_CONTEXT_INTENTION_TRIGGERED(ref_local, DCI_SOLO)
			TRIGGER_DANCING_INTRO(ref_local)
		ENDIF
		SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA)
	ELSE	
		IF ref_local.instance.bCanDanceAnywhere
		AND HAS_CONTEXT_INTENTION_TRIGGERED(ref_local, DCI_SOLO_ANYWHERE)
			TRIGGER_DANCING_INTRO(ref_local)
		ENDIF
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA)
	ENDIF
ENDPROC

PROC DANCE_MAINTAIN_BUSY_HINT(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_WAITING_TO_PRINT_STAIRS)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			// If we add non-stairs areas in the future, add function to get help text from block area
			PRINT_HELP("DM_AREA_BLOCK")
		ENDIF
		
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_WAITING_TO_PRINT_STAIRS)
	ENDIF
	
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_DUAL_DANCE_BLOCKED)
		// If we add non-stairs areas in the future, add function to get help text from block area
		PRINT_HELP("DUAL_DANCE_BLOCK", 5000)
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_DUAL_DANCE_BLOCKED)
	ENDIF
ENDPROC

CONST_INT ciDANCE_HINT_WAIT_TIME_MS 5000
PROC DANCE_MAINTAIN_OFF_FLOOR_HINT(PLAYER_DANCE_LOCAL_STRUCT& ref_local)	
	IF NOT ref_local.instance.bCanDanceAnywhere
		EXIT
	ENDIF
	
	INT iHintDisplayCount = GET_PACKED_STAT_INT(ref_local.instance.eDanceAnywhereCounterStat)
	
	IF IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_HAS_DANCE_HELP_DISPLAYED)
	OR iHintDisplayCount >= ciDANCE_MAX_HINT_COUNT
		EXIT
	ENDIF
	
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA_PREV)
	AND NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA)
		SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_WAITING_TO_PRINT_HINT)
		RESET_NET_TIMER(ref_local.tLeftDanceFloorTime)
		START_NET_TIMER(ref_local.tLeftDanceFloorTime)
	ELIF NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA_PREV)
	AND IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA)
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_WAITING_TO_PRINT_HINT)
	ENDIF
	
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_WAITING_TO_PRINT_HINT)
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()	
	AND HAS_NET_TIMER_EXPIRED(ref_local.tLeftDanceFloorTime, ciDANCE_HINT_WAIT_TIME_MS)
		IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
			PRINT_HELP("DANCE_HINT")
		ELSE
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				PRINT_HELP("DANCE_HINT_K")
			ELSE
				PRINT_HELP("DANCE_HINT_ALT")
			ENDIF
		ENDIF
		
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_WAITING_TO_PRINT_HINT)
		SET_BIT(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_HAS_DANCE_HELP_DISPLAYED)
		SET_PACKED_STAT_INT(ref_local.instance.eDanceAnywhereCounterStat, iHintDisplayCount + 1)
		PRINTLN("[DANCING][DANCE_STATS] Help counter increased to ", iHintDisplayCount+1)	
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns a number of MS that should be used to check either side of a beat 
///    to define an acceptable input window.
FUNC INT COMPUTE_BEAT_MATCH_FORGIVENESS(FLOAT fBpm)
	FLOAT fBps  = fBpm / 60
	FLOAT fTimeBetweenBeatsInSeconds =  1/ fBps
	FLOAT fTimeBetweenBeatsInMs = fTimeBetweenBeatsInSeconds * 1000
	RETURN ROUND(CLAMP(cfBEAT_MATCH_WINDOW * fTimeBetweenBeatsInMs, ciBEAT_HALF_MIN_ABS_WINDOW_MS, ciBEAT_HALF_MAX_ABS_WINDOW_MS))
ENDFUNC

/// PURPOSE:
// 	If all params are false this returns 0.0
//	Returns the intensity change per beat for each state.
FUNC FLOAT GET_INTENSITY_CHANGE(INT iLevel, FLOAT fCurrent, BOOL bHold, BOOL bHit, BOOL bFail, BOOL bMiss)
	IF bHit
		IF bHold
			FLOAT fChange = GET_INTENSITY_PER_HIT(iLevel)
			FLOAT max = GET_MAX_INTENSITY_FOR_LEVEL(iLevel)
			IF fCurrent + fChange > max
				fChange = max - fCurrent
			ENDIF
			RETURN fChange
		ELSE
			RETURN GET_INTENSITY_PER_HIT(iLevel)	
		ENDIF
	ELIF bFail
		RETURN GET_INTENSITY_PER_FAIL(iLevel)
	ELIF bMiss
		IF bHold
			RETURN GET_INTENSITY_PER_MISS(iLevel) / 2.0
		ELSE
			RETURN GET_INTENSITY_PER_MISS(iLevel)
		ENDIF
	ELSE
		RETURN 0.0 // Return no change
	ENDIF
ENDFUNC

/// PURPOSE:
///    Returns the RP reward value for the number of Rp reward periods (default 60000 ms) beat matching without failure. 
FUNC INT GET_RP_REWARD_FOR_COMBO_TIME(INT iPeriodCount)
	IF iPeriodCount < 1
		RETURN 0
	ELIF iPeriodCount < 4
		RETURN g_sMPTunables.iBB_NIGHTCLUB_BEAT_MATCHING_TIER_1_RP_AWARD
	ELIF iPeriodCount < 7
		RETURN g_sMPTunables.iBB_NIGHTCLUB_BEAT_MATCHING_TIER_2_RP_AWARD
	ELIF iPeriodCount < 10
		RETURN g_sMPTunables.iBB_NIGHTCLUB_BEAT_MATCHING_TIER_3_RP_AWARD
	ELSE // >= 10 mins
		RETURN g_sMPTunables.iBB_NIGHTCLUB_BEAT_MATCHING_TIER_4_RP_AWARD
	ENDIF	
ENDFUNC

STRUCT DANCE_INPUT_STRUCT
	BOOL bBeatMatchThisFrame
	BOOL bBeatMatchReleasedThisFrame
	BOOL bBeatMatch
	
	BOOL bBoostInputThisFrame
	BOOL bDropInputThisFrame
	
	BOOL bHoldIntensityReleaseThisFrame
	BOOL bHoldIntensityThisFrame
	BOOL bHoldIntensity
ENDSTRUCT

/// PURPOSE:
///    This control scheme has been changed 10000 times so we have this in a separate function to avoid
///    changes to the rhythm function.
PROC GET_RHYTHM_INPUT(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_INPUT_STRUCT& out_inputs)
	out_inputs.bHoldIntensity = IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LT)
	out_inputs.bHoldIntensityReleaseThisFrame = IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SCRIPT_LT)
	out_inputs.bHoldIntensityThisFrame = IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LT)

	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RDOWN)
	// Auto beat match with RAG widget.
	#IF IS_DEBUG_BUILD 
	OR (ref_local.debug.bAutoBeatMatch
	AND ref_local.audio.bBeatThisFrame)
	#ENDIF // IS_DEBUG_BUILD
		out_inputs.bBeatMatchThisFrame = TRUE
	ELSE 
		out_inputs.bBeatMatchThisFrame = FALSE
	ENDIF
	
	IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
		out_inputs.bBeatMatchReleasedThisFrame = IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SCRIPT_RDOWN)
		out_inputs.bBeatMatch = IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RDOWN)
		
		IF Is_Drunk_Or_High_Cam_Effect_Active()
			out_inputs.bBoostInputThisFrame = FALSE
			out_inputs.bDropInputThisFrame = FALSE
		ELSE
			out_inputs.bBoostInputThisFrame = IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT)
			out_inputs.bDropInputThisFrame = IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RUP)
		ENDIF
		
		ref_local.bBoostLocked = FALSE
	ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
		ref_local.bBoostLocked = FALSE
	ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
	OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
	OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
	OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
		IF ref_local.bAltBoost
			CONTROL_ACTION eBoostControl = INPUT_SCRIPT_RDOWN
			
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				eBoostControl = INPUT_SCRIPT_RLEFT
			ENDIF
			
			ref_local.bBoostLocked = FALSE
			
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, eBoostControl)
				IF NOT HAS_NET_TIMER_STARTED(ref_local.stBoostDelayTimer)
				OR HAS_NET_TIMER_EXPIRED(ref_local.stBoostDelayTimer, g_sMPTunables.iIH_DANCING_BOOST_DELAY)
					IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_BOOST_LEVEL_2)
						ANIMPOSTFX_PLAY("DanceIntensity03", 0, FALSE)
					ELIF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_BOOST_LEVEL_1)
						SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_BOOST_LEVEL_2)
						
						ANIMPOSTFX_PLAY("DanceIntensity02", 0, FALSE)
					ELSE
						SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_BOOST_LEVEL_1)
						
						ANIMPOSTFX_PLAY("DanceIntensity01", 0, FALSE)
					ENDIF
					
					IF Is_Drunk_Or_High_Cam_Effect_Active()
						PLAY_SOUND_FRONTEND(-1, "Beat_Pulse_Default", "GTAO_Dancing_Sounds", FALSE)
					ENDIF
					
					REINIT_NET_TIMER(ref_local.stBoostDelayTimer)
					
					RESET_NET_TIMER(ref_local.stBoostTimer)
					RESET_NET_TIMER(ref_local.stBoostSecondaryTimer)
				ENDIF
			ENDIF
		ELSE
			IF Is_Drunk_Or_High_Cam_Effect_Active()
				ref_local.bBoostLocked = FALSE
			ELSE
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RDOWN)
					IF ref_local.bBoostLocked
						ref_local.bBoostLocked = FALSE
					ELSE
						ref_local.bBoostLocked = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC ADD_DANCE_REP_GAINS_CHECK_FOR_CAP(INT iRep)
	
	PRINTLN("[NET_DANCING_PLAYER][DANCE_DURATION_REP_CAP] ADD_DANCE_REP_GAINS_CHECK_FOR_CAP  Added ", iRep, " Rep.")
	//ADD REP TO A ROLLING SUM
	g_sDanceRepCap.iRepGained+= iRep
	
	//IF THE TIME WAS NOT TAKEN BEFORE , TAKE TIME
	IF g_sDanceRepCap.iPosix = 0
		g_sDanceRepCap.iPosix = GET_CLOUD_TIME_AS_INT()
	ENDIF
	
	//IF WE HIT THE REP CAP BEFORE 48MIN PASS, PAUSE REP GAINS
	IF g_sDanceRepCap.iRepGained >= g_sMPTunables.iDANCE_REPUTATION_CAP_PER_TIME //2300 by default
	AND NOT IS_BIT_SET(g_sDanceRepCap.iBS, BS_DANCE_REP_GAINS_PAUSED)
		
		INT iMinutesSinceMidnightBy10 = ((g_sDanceRepCap.iPosix%86400)/60)/10
		SET_PACKED_STAT_INT(PACKED_MP_INT_DANCE_REP_CAP_TIMEX, iMinutesSinceMidnightBy10)
		PRINTLN("[NET_DANCING_PLAYER][DANCE_DURATION_REP_CAP] ADD_DANCE_REP_GAINS_CHECK_FOR_CAP  Total rep gained = ", g_sDanceRepCap.iRepGained, " Rep. Pausing rep gains. SETTING PACKED STAT TO ", iMinutesSinceMidnightBy10)
		SET_BIT(g_sDanceRepCap.iBS, BS_DANCE_REP_GAINS_PAUSED)
	ENDIF
	
	//IF 48 MINUTES PASSED SINCE FIRST REP ADD, RESET TIME AND REP GAINED
	IF g_sDanceRepCap.iPosix !=0
	AND GET_CLOUD_TIME_AS_INT()-g_sDanceRepCap.iPosix >=g_sMPTunables.iDANCE_REPUTATION_CAP_TIME_IN_MINUTES*60 //48min
		PRINTLN("[NET_DANCING_PLAYER][DANCE_DURATION_REP_CAP] ADD_DANCE_REP_GAINS_CHECK_FOR_CAP  48 minutes passed since first rep gained. Did not hit rep cap. Resetting time.")
		g_sDanceRepCap.iPosix = GET_CLOUD_TIME_AS_INT()
		g_sDanceRepCap.iRepGained = iRep
	ENDIF
	
ENDPROC

PROC RESET_DANCE_REP_CAP_TIMER()
	SET_PACKED_STAT_INT(PACKED_MP_INT_DANCE_REP_CAP_TIMEX, 0)
ENDPROC

FUNC BOOL SHOULD_REWARD_DANCE_REP(SCRIPT_TIMER &danceTimer)
	IF IS_BIT_SET(g_sDanceRepCap.iBS, BS_DANCE_REP_GAINS_PAUSED)
		//CHECK IF 48 minutes passed since first rep was awarded
		INT iTimeElapsed = GET_CLOUD_TIME_AS_INT()-g_sDanceRepCap.iPosix
		
		IF iTimeElapsed >= g_sMPTunables.iDANCE_REPUTATION_CAP_TIME_IN_MINUTES*60
			PRINTLN("[NET_DANCING_PLAYER][DANCE_DURATION_REP_CAP] SHOULD_REWARD_DANCE_REP  48 minutes passed since first rep gained. Unpausing rep gains")
			CLEAR_BIT(g_sDanceRepCap.iBS, BS_DANCE_REP_GAINS_PAUSED)
			//to ensure we start from lowest reward again
			RESET_NET_TIMER(danceTimer)
			START_NET_TIMER(danceTimer)
			g_sDanceRepCap.iPosix = 0
			g_sDanceRepCap.iRepGained = 0
			RETURN TRUE
		ELSE
			PRINTLN("[NET_DANCING_PLAYER][DANCE_DURATION_REP_CAP] SHOULD_REWARD_DANCE_REP  FALSE   - REP GAINS WILL RESUME IN ", ROUND_UP_TO_NEAREST((g_sMPTunables.iDANCE_REPUTATION_CAP_TIME_IN_MINUTES*60-iTimeElapsed)/60 , 1) ," minutes." )
		ENDIF
		
		RETURN FALSE
	ELSE
		//FILL THE DATA IN 
		IF NOT IS_BIT_SET(g_sDanceRepCap.iBS, BS_DANCE_REP_DATA_INITIALISED)
			IF g_sDanceRepCap.iPosix = 0	
				INT iTimex = GET_PACKED_STAT_INT(PACKED_MP_INT_DANCE_REP_CAP_TIMEX) * 10
				INT iMinutesSinceMidnight = ((GET_CLOUD_TIME_AS_INT()%86400)/60)
				IF iTimex != 0
					INT iTimeDiff = iMinutesSinceMidnight-iTimex	
					
					IF iTimeDiff <g_sMPTunables.iDANCE_REPUTATION_CAP_TIME_IN_MINUTES
						SET_BIT(g_sDanceRepCap.iBS, BS_DANCE_REP_GAINS_PAUSED)
						g_sDanceRepCap.iPosix = GET_CLOUD_TIME_AS_INT()-(iTimeDiff*60)
						SET_BIT(g_sDanceRepCap.iBS, BS_DANCE_REP_DATA_INITIALISED)
						PRINTLN("[DANCE_DURATION_REP_CAP] MINUTES SINCE MIDNIGHT = ",iMinutesSinceMidnight, " iTIMEX = ", iTimex, " TIME DIFFERENCE IS LESS THAN CAP LIMIT : ", iTimeDiff) 
						RETURN FALSE
					ENDIF
					
					PRINTLN("[DANCE_DURATION_REP_CAP] MINUTES SINCE MIDNIGHT = ",iMinutesSinceMidnight, " iTIMEX = ", iTimex, " TIME DIFFERENCE  : ", iTimeDiff) 
						
				ENDIF		
			ENDIF
			SET_BIT(g_sDanceRepCap.iBS, BS_DANCE_REP_DATA_INITIALISED)
		ENDIF
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

CONST_FLOAT cfCOMBO_STAMINA_REWARD 0.25
CONST_INT ciMAX_COMBO_REWARDS_PER_DAY 10
CONST_INT ciGTAV_DAY_MINS 48

CONST_INT ciINPUT_DOWNTIME_MAX_MS 45000 // How long before we cound the player as idling?

FUNC INT DANCING_GET_RP_REWARD_FOR_DURATION(INT iDanceDurationMS)
	CONST_INT MINUTE 60 * 1000
	
	IF iDanceDurationMS >= MINUTE * 10
		RETURN 500
	ENDIF
	
	IF iDanceDurationMS >= MINUTE * 9
		RETURN 300
	ENDIF
	
	IF iDanceDurationMS >= MINUTE * 8
		RETURN 300
	ENDIF
	
	IF iDanceDurationMS >= MINUTE * 7
		RETURN 200
	ENDIF
	
	IF iDanceDurationMS >= MINUTE * 6
		RETURN 200
	ENDIF
	
	IF iDanceDurationMS >= MINUTE * 5
		RETURN 200
	ENDIF
	
	IF iDanceDurationMS >= MINUTE * 4
		RETURN 200
	ENDIF
	
	IF iDanceDurationMS >= MINUTE * 3
		RETURN 200
	ENDIF
	
	IF iDanceDurationMS >= MINUTE * 2
		RETURN 100
	ENDIF
	   
	IF iDanceDurationMS >= MINUTE
		RETURN 100
	ENDIF
  	
	// No reward for less than a minute
	RETURN 0
ENDFUNC

PROC DANCING_MAINTAIN_GIVE_DURATION_RP_REWARDS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF NOT HAS_NET_TIMER_EXPIRED(ref_local.tRPDurationMinute, 60000)
		EXIT
	ENDIF
	
	RESET_NET_TIMER(ref_local.tRPDurationMinute)
	
	IF NOT SHOULD_REWARD_DANCE_REP(ref_local.tlDanceTimer)
		EXIT
	ENDIF
		
	INT iDanceDurationMS = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.tlDanceTimer) 
	INT iRpReward = DANCING_GET_RP_REWARD_FOR_DURATION(iDanceDurationMS)
	//keeping track of gained REP. Will pause rep gains if cap of 2300 is reached	
	PRINTLN("[DANCING] MAINTAIN_DANCING_DURATION_RP_REWARDS - dance duration: ", iDanceDurationMS)
	PRINTLN("[DANCING] ADD_DANCE_REP_GAINS_CHECK_FOR_CAP")
	ADD_DANCE_REP_GAINS_CHECK_FOR_CAP(iRpReward)	
	IF iRpReward > 0
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, ref_local.pedLocalPlayer, "", XPTYPE_ACTION, XPCATEGORY_DANCE_BEAT_MATCH_COMBO, iRpReward)
		PRINTLN("[DANCING] MAINTAIN_DANCING_DURATION_RP_REWARDS - Awarding rp: ", iRpReward, " dance duration: ", iDanceDurationMS)
	ENDIF
ENDPROC

#IF DANCING_FEATURE_DUAL_DANCE
FUNC INT DANCING_GET_RP_REWARD_FOR_DUAL_DANCE_DURATION(INT iDanceDurationMS)
	CONST_INT MINUTE 60 * 1000
		
	// Flat reward of 200
	IF iDanceDurationMS >= MINUTE
		RETURN 200
	ENDIF
	
	// No reward for less than a minute
	RETURN 0
ENDFUNC

PROC DANCING_MAINTAIN_GIVE_DUAL_DANCE_DURATION_RP_REWARDS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF NOT HAS_NET_TIMER_EXPIRED(ref_local.tRPDurationMinute, 60000)
		EXIT
	ENDIF
	
	RESET_NET_TIMER(ref_local.tRPDurationMinute)
	
	INT iDanceDurationMS = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.tlDanceTimer) 
	PRINTLN("[DANCING] MAINTAIN_DANCING_DUAL_DANCE_DURATION_RP_REWARDS - dance duration: ", iDanceDurationMS)
	INT iRpReward = DANCING_GET_RP_REWARD_FOR_DUAL_DANCE_DURATION(iDanceDurationMS)
	
	IF iRpReward > 0
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, ref_local.pedLocalPlayer, "", XPTYPE_ACTION, XPCATEGORY_DANCE_BEAT_MATCH_COMBO, iRpReward)
		PRINTLN("[DANCING] MAINTAIN_DANCING_DUAL_DANCE_DURATION_RP_REWARDS - Awarding rp: ", iRpReward, " dance duration: ", iDanceDurationMS)
	ENDIF
ENDPROC
#ENDIF

/// PURPOSE:
///    Rewards for general dancing (don't have to be beat matched!)
PROC MAINTAIN_DANCE_FULL_MIN_REWARDS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)		
	// No input after ciINPUT_DOWNTIME_MAX_MS? : Reset award timer
	IF HAS_NET_TIMER_EXPIRED(ref_local.stats.tLastDanceInputTime, ciINPUT_DOWNTIME_MAX_MS)
		#IF IS_DEBUG_BUILD
			IF HAS_NET_TIMER_EXPIRED(ref_local.stats.fullMinDanceTimer, 100)
				PRINTLN("[DANCING][DANCE_STATS] Restarting full min dancing timer because no dance input detected!")
			ENDIF
		#ENDIF
		RESET_NET_TIMER(ref_local.stats.fullMinDanceTimer)
		START_NET_TIMER(ref_local.stats.fullMinDanceTimer)
	ENDIF
	
	// If we detect any acceptable dance input... 
	// (prefer to check state over inputs to minimise dupe problems if controls change)
	IF (ref_local.fTrueIntensitySignal > 0)
	OR (ref_local.eBeatMatchState = DBMS_HOLD)
	OR (ref_local.eStage = DLS_FLOURISH)								// In flourish anim
	OR (ref_local.eStage = DLS_MOVE_FLOURISH)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LB)  			// Rotate left
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)				// Rotate right
	OR VMAG2(<<GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_y),0>>) > 0.3 // Any directional input
	#IF IS_DEBUG_BUILD
	OR ref_local.debug.bAutoBeatMatch									// DEBUG auto beat match
	#ENDIF //IS_DEBUG_BUILD
		RESET_NET_TIMER(ref_local.stats.tLastDanceInputTime)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(ref_local.stats.fullMinDanceTimer, 60000)
		IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
			// Save num mins dancing so that we can boost player stamina stat (see CALCULATE_PLAYER_STAT_VALUE)
			INT iLifetimeComboMins = 1 + GET_MP_INT_CHARACTER_STAT(MP_STAT_DANCE_COMBO_DURATION_MINS) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_DANCE_COMBO_DURATION_MINS, iLifetimeComboMins)
			PRINTLN("[DANCING][DANCE_STATS] MAINTAIN_DANCE_FULL_MIN_REWARDS (lifetime combo) increased to ", iLifetimeComboMins)	
		ENDIF
		
		TRIGGER_ON_FULL_MIN_DANCING(ref_local)
		RESET_NET_TIMER(ref_local.stats.fullMinDanceTimer)
		START_NET_TIMER(ref_local.stats.fullMinDanceTimer)
	ENDIF
ENDPROC

/// PURPOSE:
///    Rewards for beat matching perfectly (combo)
PROC MAINTAIN_BEAT_MATCH_COMBO_REWARDS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	INT iComboTimeMs = GET_PLAYER_BEAT_MATCH_COMBO_DURATION(ref_local)
	
	// Give player XP every 'iXpPeriodMs' MS that they are beatmatching.
	INT iRpPeriodMs = g_sMPTunables.iBB_NIGHTCLUB_BEAT_MATCHING_TIME_BEAT_MATCHING_TO_INCREASE
	INT iNumFullRpPeriods = iComboTimeMs / iRpPeriodMs
	IF ref_local.stats.iRpPeriodCount < iNumFullRpPeriods
		ref_local.stats.iRpPeriodCount++
		
		// Freemode will reset the combo reward count stat when a full day (48min) since the first reward was issued has passed 
		INT iNumComboRpRewardsToday = GET_PACKED_STAT_INT(PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_COUNT)
		IF iNumComboRpRewardsToday < ciMAX_COMBO_REWARDS_PER_DAY
			INT iRpReward = GET_RP_REWARD_FOR_COMBO_TIME(iNumFullRpPeriods)
			
			// Award RP
			GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, ref_local.pedLocalPlayer, "", XPTYPE_ACTION, XPCATEGORY_DANCE_BEAT_MATCH_COMBO, iRpReward)
					
			// If this is the first reward in the last 48 mins start the timer
			IF iNumComboRpRewardsToday = 0
				SET_PACKED_STAT_INT(PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_TIME, ciGTAV_DAY_MINS)
				PRINTLN("[DANCING][DANCE_STATS] PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_TIME set to ", ciGTAV_DAY_MINS)
			ENDIF
			
			// Record that we have given a reward
			iNumComboRpRewardsToday++
			SET_PACKED_STAT_INT(PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_COUNT, iNumComboRpRewardsToday)
					
			PRINTLN("[DANCING][DANCE_STATS] Rewarding player  ",iRpReward, " RP for ", iNumFullRpPeriods, " periods (",iRpPeriodMs," ms) beat matching combo (combo count = ",GET_PLAYER_BEAT_MATCH_COMBO(ref_local),").")
			PRINTLN("[DANCING][DANCE_STATS] PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_COUNT increased to ", iNumComboRpRewardsToday)		
		ELSE
			IF iNumComboRpRewardsToday = ciMAX_COMBO_REWARDS_PER_DAY
			AND NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_HAVE_SHOWN_COMBO_STOP)
				SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_HAVE_SHOWN_COMBO_STOP)
				START_NET_TIMER(ref_local.tComboStopHelp)
			ENDIF
		ENDIF	
	ENDIF
	
	INT iNumFullMins = iComboTimeMs / 60000
	IF ref_local.stats.iComboMins < iNumFullMins 
		ref_local.stats.iComboMins++
		
		// Save num mins dancing so that we can boost player stamina stat (see CALCULATE_PLAYER_STAT_VALUE)
		INT iLifetimeComboMins = 1 + GET_MP_INT_CHARACTER_STAT(MP_STAT_DANCE_COMBO_DURATION_MINS) 
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DANCE_COMBO_DURATION_MINS, iLifetimeComboMins)
		PRINTLN("[DANCING][DANCE_STATS] MP_STAT_DANCE_COMBO_DURATION_MINS (lifetime combo) increased to ", iLifetimeComboMins)	
			
		TRIGGER_ON_FULL_MIN_BEAT_MATCHING(ref_local, ref_local.stats.iComboMins)
	ENDIF
ENDPROC

PROC DANCE_FORMAT_TEXT(INT r = 255, INT g = 255, INT b = 255, INT a = 255)
	SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(r, g, b, a) 
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	SET_TEXT_FONT(FONT_STANDARD)
	SET_TEXT_WRAP(0, 1)
ENDPROC	

FUNC BOOL DANCE_IS_BEAT_MATCH_IMMUNITY_ACTIVE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN ref_local.iBeatMatchImmunityLastBeat - ref_local.audio.iTotalBeatCount >= 0
ENDFUNC

PROC DANCE_SET_BEAT_MATCH_IMMUNITY(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT iBeatCount, BOOL bFlashIcon = FALSE)
	ref_local.iBeatMatchImmunityLastBeat = ref_local.audio.iTotalBeatCount + iBeatCount
	ref_local.bPulseIconOnImmunity = bFlashIcon
	CDEBUG1LN(DEBUG_NET_DANCING,"[DANCING][IMMUNITY] Starting beat match immunity on beat ", ref_local.audio.iTotalBeatCount, " for ",iBeatCount," beats to end on beat ", ref_local.iBeatMatchImmunityLastBeat, " bFlashIcon = ", bFlashIcon)
ENDPROC

PROC DANCE_STOP_BEAT_MATCH_IMMUNITY(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	CDEBUG1LN(DEBUG_NET_DANCING,"[DANCING][IMMUNITY] DANCE_STOP_BEAT_MATCH_IMMUNITY called on beat ", ref_local.audio.iTotalBeatCount)
	#IF IS_DEBUG_BUILD
	IF DANCE_IS_BEAT_MATCH_IMMUNITY_ACTIVE(ref_local)
		CDEBUG1LN(DEBUG_NET_DANCING,"[DANCING][IMMUNITY] DANCE_STOP_BEAT_MATCH_IMMUNITY stopped current immunity with ", (ref_local.iBeatMatchImmunityLastBeat - ref_local.audio.iTotalBeatCount) ," beats left")
	ENDIF
	#ENDIF
	
	ref_local.iBeatMatchImmunityLastBeat = 0
ENDPROC

ENUM DANCE_BEAT_MATCH_RESULT
	DBMR_NONE,
	DBMR_MISS,
	DBMR_FAIL,
	DBMR_HIT
ENDENUM

FUNC BOOL DANCE_IS_BOOST_ON_COOLDOWN(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	UNUSED_PARAMETER(ref_local)
	RETURN FALSE
	//RETURN ref_local.iBeatCountSinceLastBoost < ciBOOST_COOLDOWN_BEATS
ENDFUNC

FUNC BOOL DANCE_IS_BOOST_COMPLETE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN ref_local.fCurrentIntensitySignal > (ref_local.fIntensitySignalMax + cfSCRIPT_INTENSITY_BUFFER - 0.01)
ENDFUNC

FUNC DANCE_BEAT_MATCH_RESULT DANCE_GET_INPUT_RESULT(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_INPUT_STRUCT& in_inputs)
	DANCE_BEAT_MATCH_RESULT result = DBMR_NONE
	
	INT iForgiveness = COMPUTE_BEAT_MATCH_FORGIVENESS(ref_local.audio.fBpm)
	#IF IS_DEBUG_BUILD
	ref_local.debug.iForgivenessThisFrame = iForgiveness
	#ENDIF
	
	IF in_inputs.bBeatMatchThisFrame
		IF NOT ref_local.bAcceptedInputForNextBeat
		OR NOT ref_local.bAcceptedInputForPrevBeat	
			INT iTimeSinceLastBeatMs = GET_GAME_TIMER() - ref_local.audio.iPrevBeatTime
			FLOAT fTimeSinceLastBeat = TO_FLOAT(iTimeSinceLastBeatMs)/ 1000.0
			FLOAT fTimeBetweenBeats = fTimeSinceLastBeat + ref_local.audio.fSecondsToNextBeat
			INT iTimeBetweenBeatsMs = ROUND(fTimeBetweenBeats * 1000)
			
			IF iTimeSinceLastBeatMs < iForgiveness
				// Prev beat
				IF ref_local.bAcceptedInputForPrevBeat
					CDEBUG2LN(DEBUG_NET_DANCING, "[DANCING] FAIL 1")
					result = DBMR_FAIL
				ELSE
					CDEBUG2LN(DEBUG_NET_DANCING, "[DANCING] HIT ref_local.bAcceptedInputForPrevBeat : iTimeSinceLastBeatMs (",iTimeSinceLastBeatMs,") < iForgiveness (",iForgiveness,")")
					ref_local.bAcceptedInputForPrevBeat = TRUE
					result = DBMR_HIT
				ENDIF
			ELIF iTimeSinceLastBeatMs > iTimeBetweenBeatsMs - iForgiveness
				// Next beat
				IF ref_local.bAcceptedInputForNextBeat
					CDEBUG2LN(DEBUG_NET_DANCING, "[DANCING] FAIL 2")
					result = DBMR_FAIL
				ELSE
					CDEBUG2LN(DEBUG_NET_DANCING, "[DANCING] HIT ref_local.bAcceptedInputForNextBeat : iTimeSinceLastBeatMs (",iTimeSinceLastBeatMs,") < iTimeBetweenBeatsMs (",iTimeBetweenBeatsMs,") - iForgiveness (",iForgiveness,")")
					ref_local.bAcceptedInputForNextBeat = TRUE
					result = DBMR_HIT
				ENDIF
			ELSE
				CDEBUG2LN(DEBUG_NET_DANCING, "[DANCING] FAIL 3")
				result = DBMR_FAIL
				
				// If they manage to input dead on between the two beats
				// just give them a HIT for free.
				// @difficulty
				PRINTLN("[DANCING] Medium mode -> Convert FAIL(3) to HIT")
				result = DBMR_HIT			
			ENDIF
		ELSE
			CDEBUG2LN(DEBUG_NET_DANCING, "[DANCING] FAIL 4")
			result = DBMR_FAIL
		ENDIF 
	ELSE
		IF NOT ref_local.bAcceptedInputForPrevBeat								//	Haven't already punished them, or they have successfully pressed [A] on beat in the past.
		AND GET_GAME_TIMER() - ref_local.audio.iPrevBeatTime > iForgiveness		//	We have gone over the forgiveness threshold.
			ref_local.bAcceptedInputForPrevBeat = TRUE							// Don't accept input for this beat
			IF NOT DANCE_IS_BEAT_MATCH_IMMUNITY_ACTIVE(ref_local)				// Ignore miss if immunity is active
				CDEBUG2LN(DEBUG_NET_DANCING, "[DANCING] Miss")
				result = DBMR_MISS
			ENDIF
		ENDIF
	ENDIF
	
	RETURN result
ENDFUNC

FUNC BOOL DANCE_HAS_TRIGGERED_HOLD(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_INPUT_STRUCT& in_inputs)//, DANCE_BEAT_MATCH_RESULT eBeatMatchResults)
	IF ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD
		RETURN FALSE
	ENDIF
	
	IF in_inputs.bBeatMatch
	AND NOT in_inputs.bHoldIntensity
	AND (ref_local.audio.bBeatThisFrame) 
	AND NOT ref_local.bAcceptedInputForNextBeat
	AND NOT ref_local.bAcceptedInputForPrevBeat
		ref_local.bUsingRhythmButtonAsHold = TRUE
	ELSE
		ref_local.bUsingRhythmButtonAsHold = FALSE
	ENDIF
		
	IF (in_inputs.bHoldIntensity
	OR ref_local.bUsingRhythmButtonAsHold)
		
		IF NOT ref_local.bUsingRhythmButtonAsHold
			DANCE_SCALEFORM_SET_ALT_RHYTHM_CONTROL(ref_local.display, TRUE)
		ENDIF
		
		PRINTLN("[DANCING] Started hold on beat ", ref_local.audio.iTotalBeatCount, " using alt hold ", ref_local.bUsingRhythmButtonAsHold)
		DANCE_STOP_BEAT_MATCH_IMMUNITY(ref_local)
		
		RESET_NET_TIMER(ref_local.tHoldRhythmButtonDisplay)
		DANCE_SET_BEAT_MATCH_STATE(ref_local, DBMS_HOLD)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DANCE_TRIGGER_BOOST(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT iLevel = LEVEL_4)
	iLevel++
	IF iLevel > LEVEL_4
		iLevel = LEVEL_4
	ENDIF
	FLOAT fTargetSignal = GET_MAX_INTENSITY_FOR_LEVEL(iLevel)

	ref_local.fTrueIntensitySignal = fTargetSignal
	ref_local.display.fMeterValue = GET_METER_VALUE_FOR_LEVEL(iLevel, fTargetSignal)
	
	// Trigger cooldown
	ref_local.iBeatCountSinceLastBoost = 0
	
	// Fast intensity gain (visual)
	ref_local.bApplyBoostMultipler = TRUE
	PRINTLN("[DANCING] DANCE_TRIGGER_BOOST (Setting boost multiplier)")	
	
	DANCE_SET_BEAT_MATCH_IMMUNITY(ref_local, 2)
ENDPROC

PROC DANCE_TRIGGER_DROP(PLAYER_DANCE_LOCAL_STRUCT& ref_local, INT iLevel = LEVEL_1)
	PRINTLN("[DANCING] DANCE_TRIGGER_DROP (Removing boost multiplier)")	
	ref_local.bApplyBoostMultipler = FALSE
	
	FLOAT fTargetSignal = GET_MIN_INTENSITY_FOR_LEVEL(iLevel)

	ref_local.fTrueIntensitySignal = fTargetSignal
	ref_local.display.fMeterValue = GET_METER_VALUE_FOR_LEVEL(iLevel, fTargetSignal)

	DANCE_SET_BEAT_MATCH_IMMUNITY(ref_local, 2)
ENDPROC

PROC DANCE_MAINTAIN_BEAT_MATCH_STATE_DEFAULT(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_INPUT_STRUCT& in_inputs, DANCE_BEAT_MATCH_RESULT& out_eResult)
	out_eResult = DBMR_NONE
	
	// If we detect input we shuold cancel the immunity
	IF DANCE_IS_BEAT_MATCH_IMMUNITY_ACTIVE(ref_local)
	AND in_inputs.bBeatMatchThisFrame
		DANCE_STOP_BEAT_MATCH_IMMUNITY(ref_local)
	ENDIF
	
	out_eResult = DANCE_GET_INPUT_RESULT(ref_local, in_inputs)
	IF DANCE_HAS_TRIGGERED_HOLD(ref_local, in_inputs)
		EXIT
	ENDIF
	
	IF in_inputs.bBoostInputThisFrame
	AND NOT DANCE_IS_BOOST_ON_COOLDOWN(ref_local)
	AND NOT DANCE_IS_BOOST_COMPLETE(ref_local)
		DANCE_STOP_BEAT_MATCH_IMMUNITY(ref_local)
		DANCE_SCALEFORM_PLAYER_BEAT(ref_local.display, TRUE)
		DANCE_TRIGGER_BOOST(ref_local, LEVEL_4)
		EXIT
	ENDIF
	
	IF in_inputs.bDropInputThisFrame
	AND NOT Is_Drunk_Or_High_Cam_Effect_Active()
		DANCE_TRIGGER_DROP(ref_local, LEVEL_1)
		EXIT
	ENDIF
ENDPROC

PROC DANCE_MAINTAIN_HOLD_MODE_SWITCH(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_INPUT_STRUCT& in_inputs)
	IF ref_local.bUsingRhythmButtonAsHold
		IF in_inputs.bHoldIntensity
			PRINTLN("[DANCING] switching to [LT] hold")
			ref_local.bUsingRhythmButtonAsHold = FALSE
			DANCE_SCALEFORM_SET_ALT_RHYTHM_CONTROL(ref_local.display, TRUE)
		ENDIF
	ELSE
		IF in_inputs.bBeatMatch
		AND NOT in_inputs.bHoldIntensity
			PRINTLN("[DANCING] switching to [A] hold")
			ref_local.bUsingRhythmButtonAsHold = TRUE
			DANCE_SCALEFORM_SET_ALT_RHYTHM_CONTROL(ref_local.display, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC DANCE_MAINTAIN_BEAT_MATCH_STATE_HOLD(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_INPUT_STRUCT& in_inputs, DANCE_BEAT_MATCH_RESULT& out_eResults)
	out_eResults = DBMR_NONE

	// Boost
	IF in_inputs.bBoostInputThisFrame	
	AND NOT DANCE_IS_BOOST_ON_COOLDOWN(ref_local)
	AND NOT DANCE_IS_BOOST_COMPLETE(ref_local)
	AND NOT Is_Drunk_Or_High_Cam_Effect_Active()
		DANCE_SCALEFORM_PLAYER_BEAT(ref_local.display, TRUE)
		DANCE_TRIGGER_BOOST(ref_local, ref_local.display.iIntensity)
	ENDIF
	
	// Drop
	IF in_inputs.bDropInputThisFrame
	AND NOT Is_Drunk_Or_High_Cam_Effect_Active()
		DANCE_TRIGGER_DROP(ref_local, ref_local.display.iIntensity)
	ENDIF
	
	// Handle switching between modes
	DANCE_MAINTAIN_HOLD_MODE_SWITCH(ref_local, in_inputs)
	
	// Process input
	out_eResults = DANCE_GET_INPUT_RESULT(ref_local, in_inputs)
	
	IF ref_local.bUsingRhythmButtonAsHold
		// Ignore trigger : only react to beat match button
		// (now we process inputs as normal and just reduce loss on misses..
		IF in_inputs.bBeatMatchReleasedThisFrame
			DANCE_SET_BEAT_MATCH_STATE(ref_local, DBMS_DEAFULT)
			EXIT
		ENDIF
	ELSE	
		IF out_eResults = DBMR_HIT
		OR out_eResults = DBMR_FAIL
			REINIT_NET_TIMER(ref_local.tHoldRhythmButtonDisplay)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(ref_local.tHoldRhythmButtonDisplay)
		AND NOT HAS_NET_TIMER_EXPIRED(ref_local.tHoldRhythmButtonDisplay, ciHOLD_RHYTHM_BTN_FLASH_MS)
			DANCE_SCALEFORM_SET_ALT_RHYTHM_CONTROL(ref_local.display, FALSE)
		ELSE
			DANCE_SCALEFORM_SET_ALT_RHYTHM_CONTROL(ref_local.display, TRUE)
			RESET_NET_TIMER(ref_local.tHoldRhythmButtonDisplay)
		ENDIF
		
		IF in_inputs.bHoldIntensityReleaseThisFrame
			DANCE_SCALEFORM_SET_ALT_RHYTHM_CONTROL(ref_local.display, FALSE)
			DANCE_SET_BEAT_MATCH_STATE(ref_local, DBMS_DEAFULT)
			EXIT
		ENDIF
	ENDIF
ENDPROC

// ONLY CALL ONCE PER FRAME
PROC DANCE_FINAL_UPDATE_INTENSITY(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	
	// Apply speed & modify "current" intensity
	FLOAT fTrueDiffCur = ref_local.fTrueIntensitySignal - ref_local.fCurrentIntensitySignal
	FLOAT fSpeed
	IF fTrueDiffCur > 0
		fSpeed = cfINTENSITY_GAIN_PER_SEC * GET_FRAME_TIME()
		IF (ref_local.bApplyBoostMultipler)
			fSpeed *= cfINTENSITY_GAIN_BOOST_MULTIPLIER
		ENDIF
		
		// Ease out
		IF fTrueDiffCur <= cfINTENSITY_EASE_OUT_START
			fSpeed *= CLAMP((fTrueDiffCur / cfINTENSITY_EASE_OUT_START) * cfINTENSITY_EASE_OUT_MODIFIER, 0.1 * fSpeed, 1)	
		ENDIF
		
		ref_local.fCurrentIntensitySignal = CLAMP(ref_local.fCurrentIntensitySignal + fSpeed, 0, ref_local.fTrueIntensitySignal)
		
		IF ref_local.bApplyBoostMultipler
		AND (ref_local.fTrueIntensitySignal - ref_local.fCurrentIntensitySignal <= 0.001)
			ref_local.bApplyBoostMultipler = FALSE
			PRINTLN("[DANCING] Removing boost multiplier (0)")
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF ref_local.bApplyBoostMultipler
			PRINTLN("[DANCING] Removing boost multiplier (1)")
		ENDIF
		#ENDIF
		ref_local.bApplyBoostMultipler = FALSE
		
		fSpeed = -cfINTENSITY_DECAY_PER_SEC * GET_FRAME_TIME()
		
		// Ease out
		FLOAT fCurDiffTrue = ref_local.fCurrentIntensitySignal -  ref_local.fTrueIntensitySignal
		IF fCurDiffTrue <= cfINTENSITY_EASE_OUT_START
			fSpeed *= CLAMP((fCurDiffTrue / cfINTENSITY_EASE_OUT_START) * cfINTENSITY_EASE_OUT_MODIFIER, 0.1 * fSpeed, 1)
		ENDIF
		
		ref_local.fCurrentIntensitySignal = CLAMP(ref_local.fCurrentIntensitySignal + fSpeed, ref_local.fTrueIntensitySignal, 1.0 + cfSCRIPT_INTENSITY_BUFFER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Call to manage player beat matching mechanic.
PROC DANCE_MANAGE_INTENSITY_RHYTHM(PLAYER_DANCE_LOCAL_STRUCT& ref_local, NIGHTCLUB_AUDIO_TAGS eAudioIntensity)
	// Get inputs
	DANCE_INPUT_STRUCT inputs	
	GET_RHYTHM_INPUT(ref_local, inputs)
	
	IF inputs.bBeatMatch
	OR inputs.bHoldIntensity
		DANCE_SCALEFORM_FLASH_ICON(ref_local.display)
	ENDIF
	
	// Grab intensity cap & boost input
	IF Is_Drunk_Or_High_Cam_Effect_Active()
	AND ref_local.eControlScheme != DANCE_CONTROL_SCHEME_ALTERNATE_3
		ref_local.fIntensitySignalMax = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_2)
	ELSE
		ref_local.fIntensitySignalMax = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_4)
	ENDIF
	
	IF ref_local.audio.bBeatThisFrame
		ref_local.bAcceptedInputForPrevBeat = ref_local.bAcceptedInputForNextBeat
		ref_local.bAcceptedInputForNextBeat = FALSE
		ref_local.iBeatCountSinceLastBoost++
		DANCE_SCALEFORM_MUSIC_BEAT(ref_local.display)
		IF DANCE_IS_BEAT_MATCH_IMMUNITY_ACTIVE(ref_local)
		AND ref_local.bPulseIconOnImmunity
			DANCE_SCALEFORM_PULSE(ref_local.display)
		ENDIF
	ENDIF

	// Main beat match processing
	DANCE_BEAT_MATCH_RESULT beatMatchResults = DBMR_NONE
	SWITCH ref_local.eBeatMatchState
		CASE DBMS_DEAFULT
			DANCE_MAINTAIN_BEAT_MATCH_STATE_DEFAULT(ref_local, inputs, beatMatchResults)
		BREAK
		CASE DBMS_HOLD
			DANCE_MAINTAIN_BEAT_MATCH_STATE_HOLD(ref_local, inputs, beatMatchResults)
		BREAK
	ENDSWITCH
		
	IF (beatMatchResults = DBMR_HIT)
		DANCE_SCALEFORM_PLAYER_BEAT(ref_local.display, TRUE)
		
		IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
			SET_CONTROL_SHAKE(PLAYER_CONTROL, ciDANCE_BOOST_SHAKE_DURATION, ciDANCE_BOOST_SHAKE_FREQUENCY)
		ENDIF
	ELIF (beatMatchResults = DBMR_FAIL)
		DANCE_SCALEFORM_PLAYER_BEAT(ref_local.display, FALSE)	
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF (beatMatchResults = DBMR_HIT) OR (beatMatchResults = DBMR_FAIL)
		ref_local.debug.bInputHit = (beatMatchResults = DBMR_HIT) AND NOT (beatMatchResults = DBMR_FAIL)
		ref_local.debug.iInputTime = GET_GAME_TIMER()
	ENDIF
	#ENDIF
	
	BOOL bPauseCombo = (ref_local.eBeatMatchState = DBMS_HOLD AND ref_local.bUsingRhythmButtonAsHold) OR DANCE_IS_BEAT_MATCH_IMMUNITY_ACTIVE(ref_local)
	UPDATE_BEAT_MATCH_STATS(ref_local, (beatMatchResults = DBMR_MISS), (beatMatchResults = DBMR_FAIL), (beatMatchResults = DBMR_HIT), bPauseCombo)

	// Exclusive minimum depends on whether we're goin up or down...
	// ...because we don't want to gain LEVEL_2 intensity gain going into LEVEL_3 for example.
	BOOL bMinInclusive
	
	IF (ref_local.eBeatMatchState = DBMS_HOLD)
	OR (beatMatchResults = DBMR_FAIL)
	OR (beatMatchResults = DBMR_MISS)
		bMinInclusive = FALSE
	ELSE
		bMinInclusive = TRUE
	ENDIF
	
	// Modify "true" intensity
	// We can't used the cached level because it won't be up to date..ref_local.display.iIntensity
	INT iLevel = CONVERT_INTENSITY_SIGNAL_TO_INTENSITY_LEVEL(ref_local.fTrueIntensitySignal, bMinInclusive)
	
	FLOAT fChange = GET_INTENSITY_CHANGE(iLevel, ref_local.fTrueIntensitySignal, (ref_local.eBeatMatchState = DBMS_HOLD), (beatMatchResults = DBMR_HIT), (beatMatchResults = DBMR_FAIL), (beatMatchResults = DBMR_MISS))	
	#IF IS_DEBUG_BUILD
	IF fChange <> 0.0
		ref_local.debug.fLastIntensityChange = fChange
		ref_local.debug.fIntensityBeforeChange = ref_local.fTrueIntensitySignal
	ENDIF
	#ENDIF
	ref_local.fTrueIntensitySignal = CLAMP(ref_local.fTrueIntensitySignal + fChange, 0, ref_local.fIntensitySignalMax + cfSCRIPT_INTENSITY_BUFFER)
	
	IF ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD
		#IF IS_DEBUG_BUILD
		IF ref_local.iIntensityOverride > 0
			SWITCH ref_local.iIntensityOverride
				CASE 1
					ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_1)
				BREAK
				
				CASE 2
					ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_2)
				BREAK
				
				CASE 3
					ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_3)
				BREAK
				
				CASE 4
					ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_4)
				BREAK
			ENDSWITCH
		ELSE
		#ENDIF
			IF ref_local.bAltIntensities
				FLOAT fPercentage
				FLOAT fIntensityDifference
				
				SWITCH eAudioIntensity
					CASE AUDIO_TAG_NULL
					CASE AUDIO_TAG_LOW
						IF ref_local.bAltBoost
							IF HAS_NET_TIMER_STARTED(ref_local.stBoostSecondaryTimer)
								fPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.stBoostSecondaryTimer)) / TO_FLOAT(g_sMPTunables.iIH_DANCING_BOOST_SECONDARY_TIMER)
								fIntensityDifference = ref_local.fLevel2 - ref_local.fLevel1
								ref_local.fTrueIntensitySignal = ref_local.fLevel2 - (fIntensityDifference * fPercentage)
							ELIF HAS_NET_TIMER_STARTED(ref_local.stBoostTimer)
								fPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.stBoostTimer)) / TO_FLOAT(g_sMPTunables.iIH_DANCING_BOOST_PRIMARY_TIMER)
								fIntensityDifference = ref_local.fLevel3 - ref_local.fLevel2
								ref_local.fTrueIntensitySignal = ref_local.fLevel3 - (fIntensityDifference * fPercentage)
							ELIF HAS_NET_TIMER_STARTED(ref_local.stBoostDelayTimer)
								ref_local.fTrueIntensitySignal = ref_local.fLevel3
							ELSE
								ref_local.fTrueIntensitySignal = ref_local.fLevel1
							ENDIF
						ELIF ref_local.bBoostLocked
							ref_local.fTrueIntensitySignal = ref_local.fLevel2
						ELSE
							ref_local.fTrueIntensitySignal = ref_local.fLevel1
						ENDIF
					BREAK
					
					CASE AUDIO_TAG_MEDIUM
						IF ref_local.bAltBoost
							IF HAS_NET_TIMER_STARTED(ref_local.stBoostSecondaryTimer)
								fPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.stBoostSecondaryTimer)) / TO_FLOAT(g_sMPTunables.iIH_DANCING_BOOST_SECONDARY_TIMER)
								fIntensityDifference = ref_local.fLevel3 - ref_local.fLevel2
								ref_local.fTrueIntensitySignal = ref_local.fLevel3 - (fIntensityDifference * fPercentage)
							ELIF HAS_NET_TIMER_STARTED(ref_local.stBoostTimer)
								fPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.stBoostTimer)) / TO_FLOAT(g_sMPTunables.iIH_DANCING_BOOST_PRIMARY_TIMER)
								fIntensityDifference = ref_local.fLevel4 - ref_local.fLevel3
								ref_local.fTrueIntensitySignal = ref_local.fLevel4 - (fIntensityDifference * fPercentage)
							ELIF HAS_NET_TIMER_STARTED(ref_local.stBoostDelayTimer)
								ref_local.fTrueIntensitySignal = ref_local.fLevel4
							ELSE
								ref_local.fTrueIntensitySignal = ref_local.fLevel2
							ENDIF
						ELIF ref_local.bBoostLocked
							ref_local.fTrueIntensitySignal = ref_local.fLevel3
						ELSE
							ref_local.fTrueIntensitySignal = ref_local.fLevel2
						ENDIF
					BREAK
					
					CASE AUDIO_TAG_HIGH
						IF ref_local.bAltBoost
							IF HAS_NET_TIMER_STARTED(ref_local.stBoostSecondaryTimer)
								fPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.stBoostSecondaryTimer)) / TO_FLOAT(g_sMPTunables.iIH_DANCING_BOOST_SECONDARY_TIMER)
								fIntensityDifference = ref_local.fLevel4 - ref_local.fLevel3
								ref_local.fTrueIntensitySignal = ref_local.fLevel4 - (fIntensityDifference * fPercentage)
							ELIF HAS_NET_TIMER_STARTED(ref_local.stBoostTimer)
								fPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.stBoostTimer)) / TO_FLOAT(g_sMPTunables.iIH_DANCING_BOOST_PRIMARY_TIMER)
								fIntensityDifference = ref_local.fLevel5 - ref_local.fLevel4
								ref_local.fTrueIntensitySignal = ref_local.fLevel5 - (fIntensityDifference * fPercentage)
							ELIF HAS_NET_TIMER_STARTED(ref_local.stBoostDelayTimer)
								ref_local.fTrueIntensitySignal = ref_local.fLevel5
							ELSE
								ref_local.fTrueIntensitySignal = ref_local.fLevel3
							ENDIF
						ELIF ref_local.bBoostLocked
							ref_local.fTrueIntensitySignal = ref_local.fLevel4
						ELSE
							ref_local.fTrueIntensitySignal = ref_local.fLevel3
						ENDIF
					BREAK
					
					CASE AUDIO_TAG_HIGH_HANDS
						IF ref_local.bAltBoost
							IF HAS_NET_TIMER_STARTED(ref_local.stBoostSecondaryTimer)
								fPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.stBoostSecondaryTimer)) / TO_FLOAT(g_sMPTunables.iIH_DANCING_BOOST_SECONDARY_TIMER)
								fIntensityDifference = ref_local.fLevel5 - ref_local.fLevel4
								ref_local.fTrueIntensitySignal = ref_local.fLevel5 - (fIntensityDifference * fPercentage)
							ELIF HAS_NET_TIMER_STARTED(ref_local.stBoostTimer)
								ref_local.fTrueIntensitySignal = ref_local.fLevel5
							ELIF HAS_NET_TIMER_STARTED(ref_local.stBoostDelayTimer)
								ref_local.fTrueIntensitySignal = ref_local.fLevel5
							ELSE
								ref_local.fTrueIntensitySignal = ref_local.fLevel4
							ENDIF
						ELIF ref_local.bBoostLocked
							ref_local.fTrueIntensitySignal = ref_local.fLevel5
						ELSE
							ref_local.fTrueIntensitySignal = ref_local.fLevel4
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				IF Is_Drunk_Or_High_Cam_Effect_Active()
					IF eAudioIntensity = AUDIO_TAG_NULL
					OR eAudioIntensity = AUDIO_TAG_LOW
						ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_1)
					ELSE
						ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_2)
					ENDIF
				ELSE
					SWITCH eAudioIntensity
						CASE AUDIO_TAG_NULL
						CASE AUDIO_TAG_LOW
							IF ref_local.bBoostLocked
								ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_2)
							ELSE
								ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_1)
							ENDIF
						BREAK
						
						CASE AUDIO_TAG_MEDIUM
							IF ref_local.bBoostLocked
								ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_3)
							ELSE
								ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_2)
							ENDIF
						BREAK
						
						CASE AUDIO_TAG_HIGH
						CASE AUDIO_TAG_HIGH_HANDS
							IF ref_local.bBoostLocked
								ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_4)
							ELSE
								ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_3)
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF ref_local.bApplyBoostMultipler
			ref_local.fTrueIntensitySignal = GET_MAX_INTENSITY_FOR_LEVEL(LEVEL_4)
		ENDIF
	ENDIF
	
	IF (beatMatchResults = DBMR_FAIL)
	OR (beatMatchResults = DBMR_MISS)
		SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_ALLOW_RED_METER)
	ELIF (beatMatchResults = DBMR_HIT)
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_ALLOW_RED_METER)
	ENDIF
	
	// Update the "current" intensity
	DANCE_FINAL_UPDATE_INTENSITY(ref_local)
	
	IF (beatMatchResults = DBMR_HIT)
		DANCE_LIGHTS_PLAYER_BEAT_HIT(ref_local)
	ELIF (beatMatchResults = DBMR_FAIL)
		DANCE_LIGHTS_PLAYER_BEAT_FAIL(ref_local)
	ENDIF
	
	IF ref_local.audio.bBeatThisFrame
		IF NOT ref_local.bAcceptedInputForPrevBeat
			DANCE_LIGHTS_MUSIC_BEAT(ref_local)
		ELIF (NOT DANCE_IS_BOOST_ON_COOLDOWN(ref_local))
		AND inputs.bBoostInputThisFrame
			DANCE_LIGHTS_PLAYER_BEAT_HIT(ref_local)
		ENDIF
	ENDIF
ENDPROC

PROC DANCE_MANAGE_DIRECTION_SIGNAL(FLOAT& ref_fTrueSignal, FLOAT& ref_fCurrentSignal, FLOAT& ref_fVelocity, FLOAT fRawInput)	
	// Map the input between 0-1 
	ref_fTrueSignal = (fRawInput +  1) / 2 
	
	FLOAT fAcc
	IF ref_fTrueSignal - ref_fCurrentSignal > 0 // Positive increase 
		IF ref_fVelocity < 0
			ref_fVelocity = 0
		ENDIF
		fAcc = cfDIRECTION_ACC
	
	ELIF ref_fTrueSignal - ref_fCurrentSignal < 0 // Negative increase 
		IF ref_fVelocity > 0
			ref_fVelocity = 0		
		ENDIF
		fAcc = -cfDIRECTION_ACC
	ELSE 
		ref_fVelocity = 0	
		fAcc = 0
	ENDIF
		
	IF ABSF(fRawInput) >= 0.2 // If the player is holding the stick in any direction
		ref_fVelocity = CLAMP(ref_fVelocity + (fAcc * GET_FRAME_TIME()), -cfDIRECTION_GAIN_PER_SEC, cfDIRECTION_GAIN_PER_SEC)
	ELSE
		ref_fVelocity = CLAMP(ref_fVelocity + (fAcc * GET_FRAME_TIME()), -cfDIRECTION_DECAY_PER_SEC, cfDIRECTION_DECAY_PER_SEC)
	ENDIF
	
	// Ease out
	FLOAT fVelThisFrame = ref_fVelocity * GET_FRAME_TIME()
	FLOAT fDiff = ABSF(ref_fCurrentSignal - ref_fTrueSignal) 
	IF fDiff <= cfDIRECTION_EASE_OUT_START
		fVelThisFrame *= cfDIRECTION_EASE_OUT_MODIFIER * fDiff / cfDIRECTION_EASE_OUT_START
	ENDIF
		
	
	// Apply speed
	FLOAT fNewVal = ref_fCurrentSignal + fVelThisFrame
	IF ref_fVelocity > 0
		ref_fCurrentSignal = CLAMP(fNewVal,0, ref_fTrueSignal) 	
	ELSE
		ref_fCurrentSignal = CLAMP(fNewVal, ref_fTrueSignal, 1)
	ENDIF
ENDPROC

FUNC INT FLATTEN_3x3x3(INT x, INT y, INT z)
	RETURN x + 3 * (y + 3 * z)
ENDFUNC

FUNC INT GET_DOMINANT_CLIP_INDEX(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	// High = 0, Med = 1, Low = 2
	INT iIntensity = PICK_INT(ref_local.fCurrentIntensitySignal > 0.75, 0, PICK_INT(ref_local.fCurrentIntensitySignal > 0.25, 1, 2))
	// Center = 0, Left = 1, Right = 2																		
	INT iHorizontal = PICK_INT(ref_local.v2DirectionSignals.x < 0.25, 1, PICK_INT(ref_local.v2DirectionSignals.x > 0.75, 2, 0))				
	// Center = 0, Down = 1, Up = 2
	INT iVertical = PICK_INT(ref_local.v2DirectionSignals.y > 0.75, 1, PICK_INT(ref_local.v2DirectionSignals.y  < 0.25, 2, 0))
	
	RETURN FLATTEN_3x3x3(iVertical, iHorizontal, iIntensity)
ENDFUNC

PROC FORCE_REMOTE_ACTION_CLIPSETS(REMOTE_ACTION_DATA& ref_actionData)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 debug_txtTime = GET_TIME_AS_STRING(GET_NETWORK_TIME())
	#ENDIF
	
	INT iVariableClipsetHash = DANCE_GET_FLOURISH_VARIABLE_CLIPSET_HASH()
	INT iPlayer
	REPEAT COUNT_OF(g_danceData.iPlayerActions) iPlayer


		// Overwrite global if player is not dancing
		IF g_danceData.iPlayerActions[iPlayer] <> 0
			PED_INDEX ped = GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
			
			IF NOT DOES_ENTITY_EXIST(ped)
			OR IS_ENTITY_DEAD(ped)
			OR NOT IS_TASK_MOVE_NETWORK_ACTIVE(ped)
				g_danceData.iPlayerActions[iPlayer] = 0
				ref_actionData.iPlayerActions[iPlayer] = 0
				PRINTLN("[DANCING][REMOTE_ACTION] (0) Overwrite global action from ", g_danceData.iPlayerActions[iPlayer], " to 0 for remote player ", iPlayer, ": NET TIME: ", debug_txtTime)
				RELOOP
			ENDIF
		ENDIF
		
		IF g_danceData.iPlayerActions[iPlayer] <> ref_actionData.iPlayerActions[iPlayer]
			PED_INDEX ped = GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
			
			IF DOES_ENTITY_EXIST(ped)
			AND NOT IS_ENTITY_DEAD(ped)
			AND IS_TASK_MOVE_NETWORK_ACTIVE(ped)
				// Only set if we managed to actually set the Move clipset so that it keeps trying...
				ref_actionData.iPlayerActions[iPlayer] = g_danceData.iPlayerActions[iPlayer]
				
				// Set Move clipset
				INT iClipsetHash = DANCE_GET_FLOURISH_ACTION_CLIPSET_HASH(g_danceData.iPlayerActions[iPlayer])
				
				IF iClipsetHash = 0
					PRINTLN("[DANCING][REMOTE_ACTION] Player actions has no flourish clipset: ", g_danceData.iPlayerActions[iPlayer])
					EXIT
				ENDIF
				
				SET_TASK_MOVE_NETWORK_ANIM_SET(ped, iClipsetHash , iVariableClipsetHash)
				PRINTLN("[DANCING][REMOTE_ACTION] Set clip hash:", iClipsetHash, " for remote player : NET TIME: ", debug_txtTime)
			ELSE
				g_danceData.iPlayerActions[iPlayer] = 0
				ref_actionData.iPlayerActions[iPlayer] = 0
				PRINTLN("[DANCING][REMOTE_ACTION] (1) Overwrite global action from ", g_danceData.iPlayerActions[iPlayer], " to 0 for remote player ", iPlayer, ": NET TIME: ", debug_txtTime)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

// This doesn't work as expected but we don't have time to change it - don't touch.
PROC DANCE_MANAGE_SPEED_SIGNAL(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	
	//	[1]	Work out our dominant clip: We will base all the calculations
	// 		off of the cached data for this clip.
	//
	INT iClipIndex = GET_DOMINANT_CLIP_INDEX(ref_local)
	
	#IF IS_DEBUG_BUILD
		ref_local.debug.iDominantClipIndex = iClipIndex 
	#ENDIF	
	
	//	[2]	Get approximately where the next animation beat is for the
	//		dominant clip based off of our current phase (output from 
	//		as move network "phase_out")
	//
	DANCE_CLIP_DATUM d = ref_local.clipData.soloDanceData[iClipIndex]
	FLOAT fNextBeatPhase = d.fFirstBeatPhase
	
	WHILE fNextBeatPhase < ref_local.fPhaseSignal AND fNextBeatPhase <= 1 AND d.fBeatPhaseDiff > 0
	 	fNextBeatPhase += d.fBeatPhaseDiff	
	ENDWHILE
	
	IF fNextBeatPhase > 1.0
		// we are going to wrap - we're going to use a phase target of > 1 for these calcs
		fNextBeatPhase = 1- ref_local.fPhaseSignal + d.fFirstBeatPhase 
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("playerDanceSignalDebug")
			PRINTLN("[DANCING][DANCE_MANAGE_SPEED_SIGNAL] Clip index: ", iClipIndex, " sPhaseSignal = ", ref_local.fPhaseSignal,
			" d.fFirstBeatPhase= ", d.fFirstBeatPhase, " fNextBeatPhase= ", fNextBeatPhase, "d.fBeatPhaseDiff= ", d.fBeatPhaseDiff)
		ENDIF
	#ENDIF
	
	//	[3]	Calculate how long (seconds) until the next animation beat using 
	//		the phase values from step [2].
	//		seconds_until_beat_at_normal_speed = (next_beat_phase - current_phase) * length_of_clip
	//		seconds_until_beat_at_current_playback_speed = seconds_until_beat_at_normal_speed / convert_to_real_speed(speed_signal)
	//
	FLOAT fTimeToNextAnimBeat = (fNextBeatPhase -  ref_local.fPhaseSignal) * ref_local.clipData.fSoloDanceLength / ref_local.fSpeedSignal

	//	[4]	Grab audio data
	//	(Moved to DANCE_MAINTAIN_AUDIO_DATA())
	
	//	[5]	We don't need to do anything if our anim beat falls close
	//		to an audio beat.
	FLOAT fAnimDiffAudio = fTimeToNextAnimBeat - ref_local.audio.fSecondsToNextBeat 	
	IF ABSF(fAnimDiffAudio) > cfBEAT_SYNC_FORGIVENESS_SEC
		//	[5.1]	Calculate speed
		//
		// 	[5.1.1]	Convert beats per min to beats per sec
		FLOAT fAudioBps = ref_local.audio.fBpm / 60
		
		// If the bpm is too high we need to reduce it otherwise the anims go wonky.
		IF ref_local.audio.fBpm > cfDANCE_MAX_BPM
			fAudioBps = (ref_local.audio.fBpm * (1.0-cfDANCE_BPM_REDUCTION)) / 60
		ENDIF
		
		//	[5.1.2]	Work out how far away the audio beat is - this is our constant for our distance/speed|time triangle
		FLOAT fDistance = ref_local.audio.fSecondsToNextBeat * fAudioBps
		//	[5.1.3]	We know how far we have to go and how long we have to get there to match audio, so we can calculate a target Bps
		FLOAT fTargetAnimSpeed = fDistance / ref_local.audio.fSecondsToNextBeat
		//	[5.1.4] Given our target Bps we can calculate a multiplier for our current playback speed
		FLOAT fSignal = fTargetAnimSpeed / d.fBps
		ref_local.fSpeedSignal = CLAMP(fSignal,cfDANCE_MIN_SPEED_MULTIPLIER, cfDANCE_MAX_SPEED_MULTIPLIER)
	ENDIF
ENDPROC

TWEAK_FLOAT cfMOVE_NETWORK_BLEND 0.5

PROC DANCE_START_MOVE_NETWORK(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	DANCE_CLIPSET clipset
	GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iDanceStyle, clipset)
	
	MOVE_INITIAL_PARAMETERS args
	args.clipSetVariableHash0 	= DANCE_GET_VARIABLE_CLIPSET_HASH()
	args.clipSetHash0 			= GET_HASH_KEY(clipset.sClipset)
	STRING sMoveNetwork 		= DANCE_GET_MOVE_NETWORK_STRING()
	
	TASK_MOVE_NETWORK_BY_NAME_WITH_INIT_PARAMS(ref_local.pedLocalPlayer, sMoveNetwork, args, cfMOVE_NETWORK_BLEND, FALSE, DEFAULT, MOVE_USE_KINEMATIC_PHYSICS | MOVE_SECONDARY)
	PRINTLN("[DANCING]")	
	PRINTLN("[DANCING] args.clipSetVariableHash0 = HASH( '",DANCE_GET_VARIABLE_CLIPSET_STRING(),"' )")	
	PRINTLN("[DANCING] args.clipSetHash0 = HASH( '", clipset.sClipset, "' )")	
	PRINTLN("[DANCING] TASK_MOVE_NETWORK_BY_NAME_WITH_INIT_PARAMS( .., '",sMoveNetwork,"', args, .. )")
	
	ref_local.bWaitForIntro = TRUE
ENDPROC

/// PURPOSE:
///    Map unit circle coords to a unit square.
///    This is a very rough approximation; The output looks more like a 4-leaf clover.
/// PARAMS:
///    u, v - Circle point
///    x, y - Result point
PROC MAP_CIRCLE_TO_SQUARE(FLOAT u, FLOAT v, FLOAT& ref_x, FLOAT& ref_y)
	// Easiest way to explain this is with example numbers: (u, v) <= (0.71, 0.71) <= (1 /sqrt(2), 1 / sqrt(2)) <= vec_norm(1, 1)
	// angle = ATAN2(0.71, 0.71)
	//       = PI/4 
	FLOAT fAngle = ATAN2(v, u)
	
	// factor = 1 + abs(sin(PI/2) * 0.41214)
	//        = 1 + abs(1 * 0.41214)
	//        = 1.41214
	FLOAT fFactor = 1 + ABSF(SIN(fAngle * 2) * 0.41214)
	
	// x = clamp( 0.71 * 1.41214, -1, 1 )
	//   ~ 1
	ref_x = CLAMP( u * fFactor, -1, 1 )
	ref_y = CLAMP( v * fFactor, -1, 1 )
ENDPROC

/// PURPOSE:
///    Maintain the correct help prompt & returns true if the player wants to stop dancing.
FUNC BOOL PLAYER_INPUT_STOP_DANCING(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bForceHelp = FALSE)
	IF ref_local.bHaveStartedDance
	OR bForceHelp
		DANCE_CONTEXT_INTENTION eHelpType
		
		IF HAS_NET_TIMER_STARTED(ref_local.tComboStopHelp)
			IF HAS_NET_TIMER_EXPIRED(ref_local.tComboStopHelp, 5000)
				RESET_NET_TIMER(ref_local.tComboStopHelp)
			ENDIF
			
			eHelpType = DCI_NO_MORE_COMBO
		ELSE
			IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_MINIMISED_HELP)
				eHelpType = DCI_HIDE_CONTROLS
			ELSE
				IF Is_Drunk_Or_High_Cam_Effect_Active()
				AND ref_local.eControlScheme != DANCE_CONTROL_SCHEME_ALTERNATE_3
					eHelpType = DCI_SOLO_STOP_ANYWHERE_DRUNK
				ELSE
					eHelpType = DCI_SOLO_STOP_ANYWHERE
				ENDIF
			ENDIF
			
			IF HAS_CONTEXT_INTENTION_TRIGGERED(ref_local, eHelpType, DEFAULT, DEFAULT, bForceHelp)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DANCE_MAINTAIN_MINIMISING_CONTROLS_HELP(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF HAS_NET_TIMER_STARTED(ref_local.tHideHelpTimer)
	AND HAS_NET_TIMER_EXPIRED(ref_local.tHideHelpTimer, ciTIME_BEFORE_MINIMISING_HELP)
		PRINTLN("[DANCING] Minimising dance controls because this is the player's first time dancing and the timer has expired.")	
		RESET_NET_TIMER(ref_local.tHideHelpTimer)
		SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_MINIMISED_HELP)
	ENDIF
	
	CONTROL_ACTION eHideControlsAction = INPUT_SCRIPT_PAD_LEFT
	
	IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
	OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
	OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
	OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
		eHideControlsAction = INPUT_SCRIPT_RLEFT
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		eHideControlsAction = INPUT_SCRIPT_PAD_LEFT
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, eHideControlsAction)
		// We don't want to do the first time hiding of the help text 
		// if the player has already hidden it themselves.
		RESET_NET_TIMER(ref_local.tHideHelpTimer)
		IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_MINIMISED_HELP)
			PRINTLN("[DANCING] Showing dance controls")	
			CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_MINIMISED_HELP)
			SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_CONTROLS)
		ELSE
			PRINTLN("[DANCING] Minimising dance controls")	
			SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_MINIMISED_HELP)
			CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SHOW_CONTROLS)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the speed to rotate the player based on the music.
///    Fastest around beat times and slows down in between. 
FUNC FLOAT COMPUTE_ROTATION_SPEED(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	FLOAT fTimeSinceLastBeat = TO_FLOAT(GET_GAME_TIMER() - ref_local.audio.iPrevBeatTime)/ 1000.0
	FLOAT fMaxTime = fTimeSinceLastBeat + ref_local.audio.fSecondsToNextBeat
	FLOAT fCurrentTime = fMaxTime - ref_local.audio.fSecondsToNextBeat
	FLOAT fNormalisedTime = 360 * fCurrentTime / fMaxTime
	// prev beat -> |             | <- next beat
	// fMul:	   1.0    0.0    1.0 
	FLOAT fMul = (COS(fNormalisedTime) + 1) / 2
	RETURN GET_FRAME_TIME() * cfROTATION_SPEED * fMul
ENDFUNC

TWEAK_FLOAT cfFLOURISH_INTRO_PHASE 0.0

/// PURPOSE:
///    Start a "flourish" dance move. 
///    Call DANCE_SET_FLOURISH_IS_FULLBODY(.., BOOL) before this to set upperbody vs full body flourish.
/// PARAMS:
///    ref_local - 
PROC DANCE_TASK_SCRIPTED_ANIMATION(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	FLOAT fPhase = 0
	IF ref_local.flourish.eExitClip = FLC_UPPER_OUTRO
		fPhase = cfFLOURISH_INTRO_PHASE
	ENDIF
	
	#IF DANCING_FEATURE_BLEND_EVENTS	
		INTERACTION_DATA sInteractData
		FillInteractionData(ref_local.flourish.iActionType, ref_local.flourish.iActionAnim, sInteractData, TRUE, IS_PLAYER_FEMALE())
		STRING strDict = PICK_STRING(IS_PLAYER_FEMALE(), GET_STRING_FROM_TL(sInteractData.strAnimDict[AD_FULL_BODY_F]), GET_STRING_FROM_TL(sInteractData.strAnimDict[AD_FULL_BODY_M]))
		STRING strClip = GET_STRING_FROM_TL(sInteractData.strAnimNameFullBody)
		
		FLOAT fStart, fEnd
		IF FIND_ANIM_EVENT_PHASE(strDict, strClip, "ENTER", fStart, fEnd)	
			fPhase = fStart
			PRINTLN("[DANCING] DANCE_TASK_SCRIPTED_ANIMATION - Using enter event for phase, dict: ", strDict, " clip: ", strClip, " phase: ", fPhase)
		ELSE
			PRINTLN("[DANCING] DANCE_TASK_SCRIPTED_ANIMATION - No enter event on anim, dict: ", strDict, " clip: ", strClip)
		ENDIF
	#ENDIF
	
	IF ref_local.flourish.eExitClip = FLC_FULLBODY
		
		// Fullbody clips play through the INTERACTION system.
		
		START_INTERACTION_ANIM(ref_local.flourish.iActionType, ref_local.flourish.iActionAnim, FALSE, FALSE, TRUE, DEFAULT, DEFAULT, fPhase, TRUE)	
		RESET_NET_TIMER(ref_local.tFlourishStartTime)
		START_NET_TIMER(ref_local.tFlourishStartTime)
		PRINTLN("[DANCING][FLOURISH]  DANCE_TASK_SCRIPTED_ANIMATION - Full body")
		
		ref_local.bHaveStartedDance = FALSE
		DANCE_SET_STAGE(ref_local, DLS_FLOURISH)
	ELSE
		// Upperbody clips play through the move network.
		
		INT iVariableClipsetHash = DANCE_GET_FLOURISH_VARIABLE_CLIPSET_HASH()
		INT iClipsetHash = DANCE_GET_FLOURISH_ACTION_CLIPSET_HASH(ref_local.flourish.iActionAnim) 
		
		IF iClipsetHash = 0
			PRINTLN("[DANCING][FLOURISH]  DANCE_TASK_SCRIPTED_ANIMATION - No clipset hash for flourish: ", ref_local.flourish.iActionAnim)
			EXIT
		ENDIF
		
		SET_TASK_MOVE_NETWORK_ANIM_SET(ref_local.pedLocalPlayer, iClipsetHash , iVariableClipsetHash)
		PRINTLN("[DANCING][FLOURISH]  DANCE_TASK_SCRIPTED_ANIMATION - Upper body")
		PRINTLN("[DANCING][FLOURISH]  DANCE_TASK_SCRIPTED_ANIMATION - Variable clipset : ", DANCE_GET_FLOURISH_VARIABLE_CLIPSET(), " (", iVariableClipsetHash, ")")
		PRINTLN("[DANCING][FLOURISH]  DANCE_TASK_SCRIPTED_ANIMATION - Clipset          : HASH ONLY (", iClipsetHash, ")")
		ref_local.flourish.iState = 0	
		DANCE_SET_STAGE(ref_local, DLS_MOVE_FLOURISH)
	ENDIF

	PRINTLN("[DANCING][FLOURISH]  Play interaction type: ", ref_local.flourish.iActionType)
	PRINTLN("[DANCING][FLOURISH]  Play interaction anim: ", ref_local.flourish.iActionAnim)
ENDPROC

PROC DANCE_SCALEFORM_DRAW(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	// B* 4776675
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_BROWSER_OPEN()
	OR SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
		EXIT	
	ENDIF
	
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(ref_local.display.scaleform, 255, 255, 255, 0)
ENDPROC

PROC MAINTAIN_MINIMAL_DANCE_UI(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF ref_local.audio.bBeatThisFrame
		DANCE_SCALEFORM_MUSIC_BEAT(ref_local.display)
	ENDIF
	
	DANCE_SCALEFORM_MAINTIAN_METER_AND_LEVEL(ref_local)
	
	IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
		DANCE_SCALEFORM_DRAW(ref_local)
	ENDIF
ENDPROC

TWEAK_FLOAT ciDANCE_FP_CAM_FORCE_HEADING 0.0
TWEAK_FLOAT ciDANCE_FP_CAM_FORCE_PITCH 0.0
TWEAK_FLOAT ciDANCE_FP_CAM_SMOOTH_RATE 3.20
PROC DANCE_ROTATE_PLAYER(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bLeft, BOOL bRight)
	FLOAT fRot
	IF bLeft
		fRot = COMPUTE_ROTATION_SPEED(ref_local)
	ELIF bRight
		fRot = -COMPUTE_ROTATION_SPEED(ref_local)
	ELSE
		EXIT
	ENDIF
		
	PED_INDEX pedPlayer = ref_local.pedLocalPlayer
	FLOAT fTargetHeading = GET_ENTITY_HEADING(pedPlayer) + fRot
	SET_ENTITY_HEADING(pedPlayer, fTargetHeading)
	
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
		// Smooth cam rot ourselves because there isn't a way to smooth heading with FORCE_CAMERA_RELATIVE_HEADING_AND_PITCH
		FLOAT fCurrentRelHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
		FLOAT fCurrentRelPitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
		FLOAT fSpeed = ciDANCE_FP_CAM_SMOOTH_RATE * GET_FRAME_TIME()
		FLOAT fNewHeading = LERP_FLOAT(fCurrentRelHeading, 0, fSpeed)
		FLOAT fNewPitch = LERP_FLOAT(fCurrentRelPitch, 0, fSpeed)
		FORCE_CAMERA_RELATIVE_HEADING_AND_PITCH(fNewHeading, fNewPitch, 0)
	ENDIF
ENDPROC

PROC DANCE_MAINTAIN_HOLD_THROUGH_FLOURISH(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_INPUT_STRUCT& in_inputs)
	// Only give the overlay if they HOLD rhytm button (tapping does nothing while flourishing, 
	// L2/A can be held to maintain "hold" state when exiting flourish.
	IF (in_inputs.bBeatMatch AND NOT in_inputs.bBeatMatchThisFrame)
	OR in_inputs.bHoldIntensity
		DANCE_SCALEFORM_FLASH_ICON(ref_local.display)
	ENDIF	

	DANCE_SCALEFORM_SET_ALT_RHYTHM_CONTROL(ref_local.display, in_inputs.bHoldIntensity)
	ref_local.bUsingRhythmButtonAsHold = (NOT in_inputs.bHoldIntensity)
ENDPROC

PROC DANCE_SET_NEXT_FLOURISH_ACTION(BOOL bForwards)
	INT iNewAction = GET_MP_PLAYER_ANIM_SETTING()
	INT iLoopCount = 0
	PRINTLN("[DANCING] DANCE_SET_NEXT_FLOURISH_ACTION Starting search from ", iNewAction)
	
	// Cycle anims
	iNewAction = LOOP_INT_ONE(iNewAction, 0, ENUM_TO_INT(MAX_PLAYER_INTERACTIONS), bForwards)
	WHILE NOT DANCING_IS_INTERACTION_A_DANCE_INTERACTION(iNewAction)
	OR IS_PLAYER_INTERACTION_ACTION_BLOCKED(iNewAction)
	
		IF iLoopCount > ENUM_TO_INT(MAX_PLAYER_INTERACTIONS)
			PRINTLN("[DANCING] DANCE_SET_NEXT_FLOURISH_ACTION !! NO SUITABLE ACTION FOUND !! ")
			EXIT
		ENDIF
		
		iNewAction = LOOP_INT_ONE(iNewAction, 0, ENUM_TO_INT(MAX_PLAYER_INTERACTIONS), bForwards)
		iLoopCount++
	ENDWHILE
	
	SET_MP_PLAYER_ANIM_SETTING(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), iNewAction)
	PRINTLN("[DANCING] DANCE_SET_NEXT_FLOURISH_ACTION new anim = ", iNewAction, " found after ",iLoopCount, " loops")
ENDPROC

PROC DANCE_MAINTAIN_MOVE_FLOURISH(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD
		// Maintain help text (ignore input..)
		PLAYER_INPUT_STOP_DANCING(ref_local, (ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD))
	ENDIF
	
	PED_INDEX pedLocal = ref_local.pedLocalPlayer
	IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(pedLocal)
		PRINTLN("[DANCING] Player is no longer in move network.")
		ref_local.flourish.iState = 0
		DANCE_UNLOAD_FLOURISH_DATA(ref_local.flourish)
		DANCE_SET_STAGE(ref_local, DLS_DANCING)
		//DANCE_SET_BEAT_MATCH_STATE(ref_local, DBMS_DEAFULT)
		EXIT
	ENDIF
	
	IF ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD
		// Rotate the player if they are holding bumpers.
		BOOL bRotateLeft
		BOOL bRotateRight
		
		IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
		OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			bRotateLeft = IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LB)
			bRotateRight = IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
			bRotateLeft = (GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) < -0.5)
			bRotateRight = (GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) > 0.5)
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
				bRotateLeft = (GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) < -0.5)
				bRotateRight = (GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) > 0.5)
			ENDIF
		ENDIF
		
		DANCE_ROTATE_PLAYER(ref_local, bRotateLeft, bRotateRight)
	
		// Dance action (flourish, interaction) selection
		IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
		OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
				DANCE_SET_NEXT_FLOURISH_ACTION(TRUE) //Cycle forwards as now we only have one button
			ENDIF
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
				DANCE_SET_NEXT_FLOURISH_ACTION(TRUE)
			ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
				DANCE_SET_NEXT_FLOURISH_ACTION(FALSE)
			ENDIF
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
				DANCE_SET_NEXT_FLOURISH_ACTION(TRUE)
			ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
				DANCE_SET_NEXT_FLOURISH_ACTION(FALSE)
			ENDIF
		ENDIF
		
		// Dance style selection 
		IF NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_LOADING_FLOURISH)
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
			OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
					ref_local.iNewDanceStyle += 1
					DANCE_CLIPSET clipset
					// Check if the new index is out of range
					IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
						ref_local.iNewDanceStyle = 0
					ENDIF
				ENDIF
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
					ref_local.iNewDanceStyle += 1
					DANCE_CLIPSET clipset
					// Check if the new index is out of range
					IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
						ref_local.iNewDanceStyle = 0
					ENDIF
				ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
					ref_local.iNewDanceStyle -= 1
					DANCE_CLIPSET clipset
					// Check if the new index is out of range
					IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
						#IF FEATURE_HEIST_ISLAND_DANCES
						ref_local.iNewDanceStyle = 9
						#ENDIF
						
						#IF NOT FEATURE_HEIST_ISLAND_DANCES
						ref_local.iNewDanceStyle = 3
						#ENDIF
					ENDIF
				ENDIF
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
					ref_local.iNewDanceStyle += 1
					DANCE_CLIPSET clipset
					// Check if the new index is out of range
					IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
						ref_local.iNewDanceStyle = 0
					ENDIF
				ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
					ref_local.iNewDanceStyle -= 1
					DANCE_CLIPSET clipset
					// Check if the new index is out of range
					IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
						#IF FEATURE_HEIST_ISLAND_DANCES
						ref_local.iNewDanceStyle = 9
						#ENDIF
						
						#IF NOT FEATURE_HEIST_ISLAND_DANCES
						ref_local.iNewDanceStyle = 3
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF ref_local.iNewDanceStyle <> ref_local.iDanceStyle
				// Increment and loop the dance style 
				SET_MP_INT_CHARACTER_STAT(ref_local.instance.ePiMenuDanceStyleStat, ref_local.iNewDanceStyle)
				MAINTAIN_DANCE_STYLE(ref_local)
				// Keep up a basic UI
				MAINTAIN_MINIMAL_DANCE_UI(ref_local)
				SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_CHANGED_STYLE)
			ENDIF
		ENDIF
	ENDIF
	
	// Maintain the "hold" icon
	DANCE_INPUT_STRUCT inputs	
	GET_RHYTHM_INPUT(ref_local, inputs)
	DANCE_MAINTAIN_HOLD_THROUGH_FLOURISH(ref_local, inputs)
	
	IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
		IF ref_local.audio.bBeatThisFrame
			ref_local.bAcceptedInputForPrevBeat = ref_local.bAcceptedInputForNextBeat
			ref_local.bAcceptedInputForNextBeat = FALSE
			ref_local.iBeatCountSinceLastBoost++
			DANCE_SCALEFORM_MUSIC_BEAT(ref_local.display)
			IF DANCE_IS_BEAT_MATCH_IMMUNITY_ACTIVE(ref_local)
			AND ref_local.bPulseIconOnImmunity
				DANCE_SCALEFORM_PULSE(ref_local.display)
			ENDIF
		ENDIF
		
		DANCE_BEAT_MATCH_RESULT beatMatchResults = DBMR_NONE
		SWITCH ref_local.eBeatMatchState
			CASE DBMS_DEAFULT
				DANCE_MAINTAIN_BEAT_MATCH_STATE_DEFAULT(ref_local, inputs, beatMatchResults)
			BREAK
			CASE DBMS_HOLD
				DANCE_MAINTAIN_BEAT_MATCH_STATE_HOLD(ref_local, inputs, beatMatchResults)
			BREAK
		ENDSWITCH
			
		IF (beatMatchResults = DBMR_HIT)
			DANCE_SCALEFORM_PLAYER_BEAT(ref_local.display, TRUE)
			
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
				SET_CONTROL_SHAKE(PLAYER_CONTROL, ciDANCE_BOOST_SHAKE_DURATION, ciDANCE_BOOST_SHAKE_FREQUENCY)
			ENDIF
		ELIF (beatMatchResults = DBMR_FAIL)
			DANCE_SCALEFORM_PLAYER_BEAT(ref_local.display, FALSE)	
		ENDIF
	ENDIF
	
	DANCE_MANAGE_SPEED_SIGNAL(ref_local)
	SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "INTENSITY", CLAMP(ref_local.fCurrentIntensitySignal, 0.0, ref_local.fIntensitySignalMax)) // fCurrentIntensitySignal can be > 1.0
	SET_TASK_MOVE_NETWORK_SIGNAL_LOCAL_FLOAT(pedLocal, "SPEED", ref_local.fSpeedSignal) 				
	SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "DIRECTION", ref_local.v2DirectionSignals.x)
	SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "DIRECTION_UP_DOWN", ref_local.v2DirectionSignals.y)
	ref_local.fPhaseSignal = GET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "PHASE_OUT")
	
	STRING sEvent_UpperBodyClipFinished = "UpperbodyClipEnded"
	SWITCH ref_local.flourish.iState
		CASE 0 // Start
			SET_TASK_MOVE_NETWORK_SIGNAL_BOOL(pedLocal, "UpperBodyOnOff", TRUE)
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 debug_txtTime
			debug_txtTime = GET_TIME_AS_STRING(GET_NETWORK_TIME())
			PRINTLN("[DANCING][FLOURISH]DANCE_MAINTAIN_MOVE_FLOURISH Set UpperBodyOnOff : TRUE : NET TIME: ", debug_txtTime)
			#ENDIF
			ref_local.flourish.iState++
		BREAK
		CASE 1 // Wait for enter clip to finish
			SET_TASK_MOVE_NETWORK_SIGNAL_BOOL(pedLocal, "UpperBodyOnOff", TRUE) // Need to spam this for remote players' benefit
			IF GET_TASK_MOVE_NETWORK_EVENT(pedLocal, sEvent_UpperBodyClipFinished)
				PRINTLN("[DANCING][FLOURISH]DANCE_MAINTAIN_MOVE_FLOURISH Event : ", sEvent_UpperBodyClipFinished)
				RESET_NET_TIMER(ref_local.tUpperToFullFlourishInput)
				ref_local.flourish.iState++
			ENDIF
		BREAK
		CASE 2 // Wait for player to stop holding R2
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, GET_FLOURISH_CONTROL_ACTION(ref_local))
				IF HAS_NET_TIMER_STARTED(ref_local.tUpperToFullFlourishInput)
					PRINTLN("[DANCING][FLOURISH]DANCE_MAINTAIN_MOVE_FLOURISH Set UpperBodyOnOff : FALSE ##FULLBODY TRIGGERED##")
					SET_TASK_MOVE_NETWORK_SIGNAL_BOOL(pedLocal, "UpperBodyOnOff", FALSE)
					ref_local.flourish.iState++
				ELSE
					SET_TASK_MOVE_NETWORK_SIGNAL_BOOL(pedLocal, "UpperBodyOnOff", TRUE) // Need to spam this for remote players' benefit
				ENDIF
			ELSE	
				IF HAS_NET_TIMER_STARTED(ref_local.tUpperToFullFlourishInput)
					IF HAS_NET_TIMER_EXPIRED(ref_local.tUpperToFullFlourishInput, ciFLOURISH_INPUT_HOLD_TIME)
						SET_TASK_MOVE_NETWORK_SIGNAL_BOOL(pedLocal, "UpperBodyOnOff", FALSE)
						PRINTLN("[DANCING][FLOURISH]DANCE_MAINTAIN_MOVE_FLOURISH Set UpperBodyOnOff : FALSE")
						RESET_NET_TIMER(ref_local.tUpperToFullFlourishInput)
						ref_local.flourish.iState++
					ENDIF
				ELSE
					REINIT_NET_TIMER(ref_local.tUpperToFullFlourishInput)
				ENDIF
			ENDIF
		BREAK
		CASE 3 // Wait for exit clip to finish
			SET_TASK_MOVE_NETWORK_SIGNAL_BOOL(pedLocal, "UpperBodyOnOff", FALSE) // Need to spam this for remote players' benefit
			IF GET_TASK_MOVE_NETWORK_EVENT(pedLocal, sEvent_UpperBodyClipFinished)
				PRINTLN("[DANCING][FLOURISH]DANCE_MAINTAIN_MOVE_FLOURISH Event : ", sEvent_UpperBodyClipFinished)
				ref_local.flourish.iState = 0
				IF HAS_NET_TIMER_STARTED(ref_local.tUpperToFullFlourishInput)
					DANCE_SET_FLOURISH_IS_FULLBODY(ref_local.flourish, TRUE)
					DANCE_TASK_SCRIPTED_ANIMATION(ref_local)
				ELSE
					DANCE_UNLOAD_FLOURISH_DATA(ref_local.flourish)
					DANCE_SET_STAGE(ref_local, DLS_DANCING)
					IF NOT inputs.bHoldIntensity
						DANCE_SET_BEAT_MATCH_STATE(ref_local, DBMS_DEAFULT)
					ENDIF
					DANCE_SET_BEAT_MATCH_IMMUNITY(ref_local, 4, NOT inputs.bHoldIntensity)
				ENDIF	
			ENDIF
		BREAK
	ENDSWITCH
		
	MAINTAIN_MINIMAL_DANCE_UI(ref_local)
ENDPROC

FUNC BOOL SHOULD_FLOURISH_ANIM_END_NOW_FOR_BLEND_OUT(STRING pAnimDictName, STRING pAnimName, INT iTimeToEnd)
	PED_INDEX pedLocalPlayer = PLAYER_PED_ID()
	
	IF IS_ENTITY_PLAYING_ANIM(pedLocalPlayer, pAnimDictName, pAnimName)
		#IF DANCING_FEATURE_BLEND_EVENTS
		FLOAT fStart, fEnd	
		IF FIND_ANIM_EVENT_PHASE(pAnimDictName, pAnimName, "EXIT", fStart, fEnd)
			PRINTLN("[DANCING] SHOULD_FLOURISH_ANIM_END_NOW_FOR_BLEND_OUT - Using exit event")
			RETURN HAS_ANIM_EVENT_FIRED(pedLocalPlayer, HASH("EXIT"))
		ENDIF
		#ENDIF
		
		FLOAT fCurrentPhase 	= GET_ENTITY_ANIM_CURRENT_TIME(pedLocalPlayer, pAnimDictName, pAnimName)
		FLOAT fTotalDuration 	= GET_ENTITY_ANIM_TOTAL_TIME(pedLocalPlayer, pAnimDictName, pAnimName)
		
		IF fCurrentPhase > 0.0
			RETURN (ROUND(fTotalDuration - (fTotalDuration * fCurrentPhase)) < iTimeToEnd)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

TWEAK_INT ciDANCE_FLOURISH_ANIM_CHAIN_TIME 750
TWEAK_INT ciDANCE_FLOURISH_ANIM_CHAIN_TIME_FB 925

#IF FEATURE_HEIST_ISLAND_DANCES
PROC DANCE_SET_FLOURISH_PROP_VISIBLE(DANCE_INTERACTION_DATA &ref_data, BOOL bSetVisible)
	IF ref_data.objModel = NULL
		EXIT
	ENDIF
	
	SET_ENTITY_VISIBLE(ref_data.objModel, bSetVisible)
ENDPROC
#ENDIF

PROC DANCE_MAINTAIN_SHAKE_LERP(PLAYER_DANCE_LOCAL_STRUCT& ref_local)	
	IF NOT HAS_NET_TIMER_STARTED(ref_local.tRestartTime)
	OR HAS_NET_TIMER_EXPIRED(ref_local.tRestartTime, ciDANCE_RESTART_TIME)
		EXIT
	ENDIF
	
	// If we're restarting we need to lerp cam shake from whatever prev 
	// value (ref_local.fCamShakeAtReset) down to cfSHAKE_AMP_MIN
	FLOAT fCamShakeAmp = LERP_FLOAT(ref_local.fCamShakeAtReset, cfSHAKE_AMP_MIN, TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.tRestartTime)) / TO_FLOAT(ciDANCE_RESTART_TIME))
	SET_GAMEPLAY_CAM_SHAKE_AMPLITUDE(fCamShakeAmp)
	PRINTLN("[DANCING] DANCE_MAINTAIN_SHAKE_LERP - shake ", fCamShakeAmp)
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    MAINTAIN for the fullbody flourish.
PROC DANCE_MAINTAIN_FLOURISH(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	
	// url:bugstar:4973405 - display controls during flourish..
	// Handle hiding the controls
	DANCE_MAINTAIN_MINIMISING_CONTROLS_HELP(ref_local)
	// Maintain help text (ignore input..)
	PLAYER_INPUT_STOP_DANCING(ref_local, (ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD))
	
	// Maintain the "hold" icon
	DANCE_INPUT_STRUCT inputs	
	GET_RHYTHM_INPUT(ref_local, inputs)
	DANCE_MAINTAIN_HOLD_THROUGH_FLOURISH(ref_local, inputs)
	
	IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
		IF ref_local.audio.bBeatThisFrame
			ref_local.bAcceptedInputForPrevBeat = ref_local.bAcceptedInputForNextBeat
			ref_local.bAcceptedInputForNextBeat = FALSE
			ref_local.iBeatCountSinceLastBoost++
			DANCE_SCALEFORM_MUSIC_BEAT(ref_local.display)
			IF DANCE_IS_BEAT_MATCH_IMMUNITY_ACTIVE(ref_local)
			AND ref_local.bPulseIconOnImmunity
				DANCE_SCALEFORM_PULSE(ref_local.display)
			ENDIF
		ENDIF
		
		DANCE_BEAT_MATCH_RESULT beatMatchResults = DBMR_NONE
		SWITCH ref_local.eBeatMatchState
			CASE DBMS_DEAFULT
				DANCE_MAINTAIN_BEAT_MATCH_STATE_DEFAULT(ref_local, inputs, beatMatchResults)
			BREAK
			CASE DBMS_HOLD
				DANCE_MAINTAIN_BEAT_MATCH_STATE_HOLD(ref_local, inputs, beatMatchResults)
			BREAK
		ENDSWITCH
			
		IF (beatMatchResults = DBMR_HIT)
			DANCE_SCALEFORM_PLAYER_BEAT(ref_local.display, TRUE)
			
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
				SET_CONTROL_SHAKE(PLAYER_CONTROL, ciDANCE_BOOST_SHAKE_DURATION, ciDANCE_BOOST_SHAKE_FREQUENCY)
			ENDIF
		ELIF (beatMatchResults = DBMR_FAIL)
			DANCE_SCALEFORM_PLAYER_BEAT(ref_local.display, FALSE)	
		ENDIF
	ENDIF
	
	// Rotate the player if they are holding bumpers.
	BOOL bRotateLeft
	BOOL bRotateRight
		
	IF ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD
		
		IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
		OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			bRotateLeft = IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LB)
			bRotateRight = IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
			bRotateLeft = (GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) < -0.5)
			bRotateRight = (GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) > 0.5)
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
				bRotateLeft = (GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) < -0.5)
				bRotateRight = (GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) > 0.5)
			ENDIF
		ENDIF
		
		// Dance action (flourish, interaction) selection
		IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
		OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
				DANCE_SET_NEXT_FLOURISH_ACTION(TRUE) //Cycle forwards as now we only have one button
			ENDIF
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
		OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
				DANCE_SET_NEXT_FLOURISH_ACTION(TRUE)
			ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
				DANCE_SET_NEXT_FLOURISH_ACTION(FALSE)
			ENDIF
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
				DANCE_SET_NEXT_FLOURISH_ACTION(TRUE)
			ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
				DANCE_SET_NEXT_FLOURISH_ACTION(FALSE)
			ENDIF
		ENDIF
		
		// Dance style selection 
		IF NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_LOADING_FLOURISH)
			ref_local.iNewDanceStyle = ref_local.iDanceStyle
			
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
			OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
					ref_local.iNewDanceStyle += 1
					DANCE_CLIPSET clipset
					// Check if the new index is out of range
					IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
						ref_local.iNewDanceStyle = 0
					ENDIF
				ENDIF
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
					ref_local.iNewDanceStyle += 1
					DANCE_CLIPSET clipset
					// Check if the new index is out of range
					IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
						ref_local.iNewDanceStyle = 0
					ENDIF
				ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
					ref_local.iNewDanceStyle -= 1
					DANCE_CLIPSET clipset
					// Check if the new index is out of range
					IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
						#IF FEATURE_HEIST_ISLAND_DANCES
						ref_local.iNewDanceStyle = 9
						#ENDIF
						
						#IF NOT FEATURE_HEIST_ISLAND_DANCES
						ref_local.iNewDanceStyle = 3
						#ENDIF
					ENDIF
				ENDIF
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
					ref_local.iNewDanceStyle += 1
					DANCE_CLIPSET clipset
					// Check if the new index is out of range
					IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
						ref_local.iNewDanceStyle = 0
					ENDIF
				ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
					ref_local.iNewDanceStyle -= 1
					DANCE_CLIPSET clipset
					// Check if the new index is out of range
					IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
						#IF FEATURE_HEIST_ISLAND_DANCES
						ref_local.iNewDanceStyle = 9
						#ENDIF
						
						#IF NOT FEATURE_HEIST_ISLAND_DANCES
						ref_local.iNewDanceStyle = 3
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF ref_local.iNewDanceStyle <> ref_local.iDanceStyle
				// Increment and loop the dance style 
				SET_MP_INT_CHARACTER_STAT(ref_local.instance.ePiMenuDanceStyleStat, ref_local.iNewDanceStyle)
				MAINTAIN_DANCE_STYLE(ref_local)
				// Keep up a basic UI
				MAINTAIN_MINIMAL_DANCE_UI(ref_local)
				SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_CHANGED_STYLE)
			ENDIF
		ENDIF
	ENDIF
	
	// blend out early
	BOOL bEndFlourish = FALSE
	IF SHOULD_FLOURISH_ANIM_END_NOW_FOR_BLEND_OUT(ref_local.flourish.txtDicts[ref_local.flourish.eDict], ref_local.flourish.txtClips[FLC_FULLBODY], ciDANCE_FLOURISH_ANIM_CHAIN_TIME_FB)
	OR SHOULD_FLOURISH_ANIM_END_NOW_FOR_BLEND_OUT(ref_local.flourish.txtDicts[ref_local.flourish.eDict], ref_local.flourish.txtClips[FLC_UPPER_OUTRO], ciDANCE_FLOURISH_ANIM_CHAIN_TIME)
		PRINTLN("[DANCING][FLOURISH] Blending out interaction")
		FLOAT fTempHeading = GET_ENTITY_HEADING(ref_local.pedLocalPlayer)
		//CLEAR_PED_TASKS(ref_local.pedLocalPlayer)
		SET_ENTITY_HEADING(ref_local.pedLocalPlayer, fTempHeading)
		#IF FEATURE_HEIST_ISLAND_DANCES
		DANCE_SET_FLOURISH_PROP_VISIBLE(ref_local.flourish, FALSE)
		#ENDIF
		bEndFlourish = TRUE
	ENDIF	
	
	// Don't rotate if we are exiting the flourish
	IF ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD AND bEndFlourish = FALSE
		IF INT_TO_ENUM(PLAYER_INTERACTIONS, ref_local.flourish.iActionAnim) != PLAYER_INTERACTION_TAKE_SELFIE
			DANCE_ROTATE_PLAYER(ref_local, bRotateLeft, bRotateRight)
		ENDIF
	ENDIF
		
	SCRIPTTASKSTATUS eTask = GET_SCRIPT_TASK_STATUS(ref_local.pedLocalPlayer, SCRIPT_TASK_PERFORM_SEQUENCE)
	SCRIPTTASKSTATUS eAnimTask = GET_SCRIPT_TASK_STATUS(ref_local.pedLocalPlayer, SCRIPT_TASK_PLAY_ANIM)
	IF bEndFlourish
	OR ((eTask <> WAITING_TO_START_TASK 
	AND eTask <> PERFORMING_TASK
	AND eAnimTask <> WAITING_TO_START_TASK 
	AND eAnimTask <> PERFORMING_TASK)
	AND NOT IS_ANY_INTERACTION_ANIM_PLAYING())
	AND HAS_NET_TIMER_EXPIRED(ref_local.tFlourishStartTime, 1000)
		PRINTLN("[DANCING][FLOURISH] interaction has finished apparently")

		DANCE_UNLOAD_FLOURISH_DATA(ref_local.flourish)
		DANCE_SET_STAGE(ref_local, DLS_DANCING)
		
		IF NOT inputs.bHoldIntensity
			DANCE_SET_BEAT_MATCH_STATE(ref_local, DBMS_DEAFULT)
		ENDIF
		DANCE_SET_BEAT_MATCH_IMMUNITY(ref_local, 4, NOT inputs.bHoldIntensity)
	ENDIF	
	
	MAINTAIN_MINIMAL_DANCE_UI(ref_local)
ENDPROC

#IF FEATURE_HEIST_ISLAND_DANCES
PROC DANCE_CREATE_CURRENT_FLOURISH_PROP_MODEL(DANCE_INTERACTION_DATA &ref_data)
	IF ref_data.ePropModel = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[DANCING][MODEL] DANCE_CREATE_CURRENT_FLOURISH_PROP_MODEL - Model not assigend to flourish")
		EXIT
	ENDIF
	
	IF ref_data.objModel != NULL
		PRINTLN("[DANCING][MODEL] DANCE_CREATE_CURRENT_FLOURISH_PROP_MODEL - Model already exists")
		DANCE_SET_FLOURISH_PROP_VISIBLE(ref_data, TRUE)
		EXIT
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(ref_data.ePropModel)
		PRINTLN("[DANCING][MODEL] DANCE_CREATE_CURRENT_FLOURISH_PROP_MODEL - Model assigned but hasn't loaded")	
		EXIT
	ENDIF
	
	ref_data.objModel = CREATE_OBJECT(ref_data.ePropModel, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0, 0, -1.0>>, TRUE, FALSE)
	PRINTLN("[DANCING][MODEL] DANCE_CREATE_CURRENT_FLOURISH_PROP_MODEL - Created Model")	
		
	IF ref_data.bAttachPropModelToRightHand
		PED_INDEX piPlayerPed = PLAYER_PED_ID()
		ATTACH_ENTITY_TO_ENTITY(ref_data.objModel, piPlayerPed, GET_PED_BONE_INDEX(piPlayerPed, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE)
		PRINTLN("[DANCING][MODEL] DANCE_CREATE_CURRENT_FLOURISH_PROP_MODEL - Attached to right hand ")
	
	ELIF ref_data.bAttachPropModelToLeftHand
		PED_INDEX piPlayerPed = PLAYER_PED_ID()
		ATTACH_ENTITY_TO_ENTITY(ref_data.objModel, piPlayerPed, GET_PED_BONE_INDEX(piPlayerPed, BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE)
		PRINTLN("[DANCING][MODEL] DANCE_CREATE_CURRENT_FLOURISH_PROP_MODEL - Attached to left hand ")
	ENDIF
ENDPROC
#ENDIF

#IF DANCING_FEATURE_DUAL_DANCE

FUNC BOOL DANCING_MAINTAIN_DUAL_DANCE_REQUESTS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF NOT DANCING_ARE_DUAL_DANCES_AVAILABLE(ref_local)
		IF GET_FRAME_COUNT() % 120 = 0
			PRINTLN("[DANCING][DUAL] MAINTAIN_DUAL_DANCE_REQUESTS - Dual Dancing is not available to local player")
		ENDIF
		RETURN FALSE
	ENDIF
	
	INT iParticipant
	INT iMaxParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
	PLAYER_INDEX piPlayer
	PLAYER_INDEX piLocalPlayer = ref_local.piLocalPlayer
	PARTICIPANT_INDEX piParticipant
	
	REPEAT iMaxParticipant iParticipant
		piParticipant = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piParticipant)
			RELOOP
		ENDIF
		
		piPlayer = NETWORK_GET_PLAYER_INDEX(piParticipant)
		
		IF piPlayer = piLocalPlayer
			RELOOP
		ENDIF
		
		IF NOT IS_NET_PLAYER_OK(piPlayer)
			IF GET_FRAME_COUNT() % 120 = 0
				PRINTLN("[DANCING][DUAL] MAINTAIN_DUAL_DANCE_REQUESTS - Player is not ok: ", GET_PLAYER_NAME(piPlayer))
			ENDIF
			RELOOP
		ENDIF
		
		IF DANCING_GET_PLAYERS_DANCE_PARTNER(ref_local, piPlayer) != piLocalPlayer
			IF GET_FRAME_COUNT() % 120 = 0
				PRINTLN("[DANCING][DUAL] MAINTAIN_DUAL_DANCE_REQUESTS - Player is not my dance partner: ", GET_PLAYER_NAME(piPlayer))
			ENDIF
			RELOOP
		ENDIF
		
		IF NOT DANCING_IS_PLAYER_READY_FOR_DUAL_DANCE(ref_local, piPlayer)
			IF GET_FRAME_COUNT() % 120 = 0
				PRINTLN("[DANCING][DUAL] MAINTAIN_DUAL_DANCE_REQUESTS - Player is not ready for dance: ", GET_PLAYER_NAME(piPlayer))
			ENDIF
			RELOOP
		ENDIF
		
		IF DANCING_GET_PLAYERS_DANCE_STAGE(ref_local, piPlayer) != DLS_WAIT_FOR_DUAL_DANCE_START
			IF GET_FRAME_COUNT() % 120 = 0
				PRINTLN("[DANCING][DUAL] MAINTAIN_DUAL_DANCE_REQUESTS - Player is not in starting state yet: ", GET_PLAYER_NAME(piPlayer))
			ENDIF
			RELOOP
		ENDIF
		
		// Join the player who requested to dance but we are not the lead dancer
		DANCING_JOIN_DUAL_DANCE_WITH_PLAYER(ref_local, piPlayer, FALSE)
		DANCE_UNLOAD_SCALEFORM(ref_local) 
		RETURN TRUE
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL DANCE_SHOULD_STOP_DANCING(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	// We must wait for the dance to start before allowing exit, otherwise we get stuck
	IF NOT ref_local.bHaveStartedDance
		RETURN FALSE
	ENDIF
	
	IF PLAYER_INPUT_STOP_DANCING(ref_local)	
		PRINTLN("[DANCING] SHOULD_STOP_DANCING - Player pressed stop dancing")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PLAYER_IS_BUSY)
		PRINTLN("[DANCING] SHOULD_STOP_DANCING - Player is busy")
		RETURN TRUE
	ENDIF
	
	// should always check because player can move by spamming flourishes
	IF IS_PLAYER_IN_BLOCK_AREA_STAGGERED(ref_local) 
		PRINTLN("[DANCING] SHOULD_STOP_DANCING - Player is in block area")
		RETURN TRUE
	ENDIF
	
	PED_INDEX pedLocal = ref_local.pedLocalPlayer
	
	// No dancing with the mermaids
	IF IS_ENTITY_IN_WATER(pedLocal) OR IS_PED_SWIMMING(pedLocal)
		PRINTLN("[DANCING] SHOULD_STOP_DANCING - Player is in water")
		RETURN TRUE
	ENDIF
	
	// Or the birds
	IF IS_ENTITY_IN_AIR(pedLocal) 
		PRINTLN("[DANCING] SHOULD_STOP_DANCING - Player is in air")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_ON_FIRE(pedLocal)
		PRINTLN("[DANCING] SHOULD_PREVENT_PLAYER_STARTING_DANCE - Player is on fire")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC DANCE_CONTROL_SCHEME GET_NEXT_CONTROL_SCHEME(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	SWITCH ref_local.eControlScheme
		CASE DANCE_CONTROL_SCHEME_STANDARD		RETURN DANCE_CONTROL_SCHEME_ALTERNATE_1
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_1	RETURN DANCE_CONTROL_SCHEME_ALTERNATE_2
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_2	RETURN DANCE_CONTROL_SCHEME_ALTERNATE_3
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_3	RETURN DANCE_CONTROL_SCHEME_ALTERNATE_4
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_4	RETURN DANCE_CONTROL_SCHEME_ALTERNATE_5
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_5	RETURN DANCE_CONTROL_SCHEME_STANDARD
	ENDSWITCH
	
	RETURN DANCE_CONTROL_SCHEME_STANDARD
ENDFUNC

FUNC STRING GET_CONTROL_SCHEME_HELP(DANCE_CONTROL_SCHEME eControlScheme)
	SWITCH eControlScheme
		CASE DANCE_CONTROL_SCHEME_STANDARD		RETURN "DANCE_CTRL_1"
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_1	RETURN "DANCE_CTRL_2"
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_2	RETURN "DANCE_CTRL_3"
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_3	RETURN "DANCE_CTRL_4"
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_4	RETURN "DANCE_CTRL_5"
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_5	RETURN "DANCE_CTRL_6"
	ENDSWITCH
	
	RETURN ""
ENDFUNC
#ENDIF

PROC DANCE_MAINTAIN_LOCKING_STATE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	CONTROL_ACTION eLockAction = INPUT_CONTEXT
	
	IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
	OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
	OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
	OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
		eLockAction = INPUT_SCRIPT_RUP
	ENDIF
	
	IF ref_local.bDancingLocked
	AND IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_LT)
		PRINTLN("[DANCING] Unlocking dance state - player is actively dancing")	
		ref_local.bDancingLocked = FALSE
	ELSE
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, eLockAction)
			RESET_NET_TIMER(ref_local.tHideHelpTimer)
			IF ref_local.bDancingLocked
				PRINTLN("[DANCING] Unlocking dance state")	
				ref_local.bDancingLocked = FALSE
			ELSE
				PRINTLN("[DANCING] Locking dance state")	
				ref_local.bDancingLocked = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Update for when the player is dancing.
PROC DANCE_MAINTAIN_DANCING(PLAYER_DANCE_LOCAL_STRUCT& ref_local, NIGHTCLUB_AUDIO_TAGS eAudioIntensity)
	#IF DANCING_FEATURE_DUAL_DANCE
	IF DANCING_MAINTAIN_DUAL_DANCE_REQUESTS(ref_local)
		PRINTLN("[DANCING][DUAL] DANCE_MAINTAIN_DANCING - Going from dance to dual dance")
		EXIT
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_2, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
		ref_local.iIntensityOverride++
		
		IF ref_local.iIntensityOverride > 4
			ref_local.iIntensityOverride = 0
		ENDIF
	ENDIF
	
	IF ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_5, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			IF ref_local.bAltBoost
				ref_local.bAltBoost = FALSE
			ELSE
				ref_local.bAltBoost = TRUE
			ENDIF
		ENDIF
	ENDIF
	#ENDIF
	
	IF ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD
		PLAYER_INPUT_STOP_DANCING(ref_local, TRUE)
		
		IF ref_local.bAltBoost
			IF HAS_NET_TIMER_STARTED(ref_local.stBoostDelayTimer)
				IF HAS_NET_TIMER_EXPIRED(ref_local.stBoostDelayTimer, g_sMPTunables.iIH_DANCING_BOOST_DELAY)
					IF NOT HAS_NET_TIMER_STARTED(ref_local.stBoostTimer)
						START_NET_TIMER(ref_local.stBoostTimer)
					ELIF HAS_NET_TIMER_EXPIRED(ref_local.stBoostTimer, g_sMPTunables.iIH_DANCING_BOOST_PRIMARY_TIMER)
						CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_BOOST_LEVEL_1)
						CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_BOOST_LEVEL_2)
						
						IF NOT HAS_NET_TIMER_STARTED(ref_local.stBoostSecondaryTimer)
							START_NET_TIMER(ref_local.stBoostSecondaryTimer)
						ELIF HAS_NET_TIMER_EXPIRED(ref_local.stBoostSecondaryTimer, g_sMPTunables.iIH_DANCING_BOOST_SECONDARY_TIMER)
							RESET_NET_TIMER(ref_local.stBoostDelayTimer)
							RESET_NET_TIMER(ref_local.stBoostTimer)
							RESET_NET_TIMER(ref_local.stBoostSecondaryTimer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PED_INDEX pedLocal = ref_local.pedLocalPlayer
	
	// If we somehow started dancing while we should be moving away from the DJ booth...
	SCRIPTTASKSTATUS eMoveToCoordTask = GET_SCRIPT_TASK_STATUS(pedLocal, SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
	IF eMoveToCoordTask = WAITING_TO_START_TASK 
	OR eMoveToCoordTask = PERFORMING_TASK
		PRINTLN("[DANCING] DANCE_MAINTAIN_DANCING - Player is being tasked to move.")	
		DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
		DANCE_SET_STAGE(ref_local, DLS_IDLE_NOT_DANCING)
		EXIT
	ENDIF	
	
	IF NOT DANCE_HAS_SCALEFORM_LOADED(ref_local)
		CDEBUG1LN(DEBUG_NET_DANCING,"[DANCING] Scaleform movie hasn't loaded, not allowing any dance control.")
		EXIT
	ENDIF
	
	IF DANCE_SHOULD_STOP_DANCING(ref_local)
		ref_local.iStopDancingTime = GET_GAME_TIMER()
		IF NOT IS_PED_INJURED(pedLocal) AND ref_local.bLocalPlayerPedAlive
			CLEAR_PED_TASKS(pedLocal)
		ENDIF
		
		DANCE_SET_STAGE(ref_local, DLS_STOP_DANCING)
		EXIT
	ENDIF
	
	// Handle hiding the controls
	DANCE_MAINTAIN_MINIMISING_CONTROLS_HELP(ref_local)
	
	DANCE_MAINTAIN_LOCKING_STATE(ref_local)
	
	SCRIPTTASKSTATUS eTaskStatus = GET_SCRIPT_TASK_STATUS(pedLocal, SCRIPT_TASK_MOVE_NETWORK)
	
	IF (eTaskStatus <> WAITING_TO_START_TASK
	AND eTaskStatus <> PERFORMING_TASK)
		// [ INIT ]	
		PRINTLN("[DANCING] Starting dance")
		DANCE_START_MOVE_NETWORK(ref_local)	
		ref_local.bShouldMoveToDance = FALSE
		ref_local.bHaveStartedDance = FALSE
		MAINTAIN_MINIMAL_DANCE_UI(ref_local)
	ELIF ref_local.bStartMoveNetwork
		DANCE_START_MOVE_NETWORK(ref_local)	
		ref_local.bStartMoveNetwork = FALSE
		ref_local.bShouldMoveToDance = FALSE
		ref_local.bHaveStartedDance = FALSE
		MAINTAIN_MINIMAL_DANCE_UI(ref_local)
	ELIF IS_TASK_MOVE_NETWORK_ACTIVE(pedLocal)	
		// [ RUNNING ]	
		
		IF (NOT ref_local.bWaitForIntro OR (ref_local.bWaitForIntro AND GET_TASK_MOVE_NETWORK_EVENT(pedLocal, "IntroFinished")))
		AND NOT ref_local.bHaveStartedDance
			ref_local.bWaitForIntro = FALSE
			ref_local.bShouldMoveToDance = TRUE
			PRINTLN("[DANCING] GET_TASK_MOVE_NETWORK_EVENT( .., 'IntroFinished' )")	
		ENDIF
		
		IF ref_local.bShouldMoveToDance
		AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(pedLocal)
			PRINTLN("[DANCING] IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION( ) == TRUE ")	
			IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(pedLocal, "DANCE")
				ref_local.bShouldMoveToDance = FALSE
				ref_local.bHaveStartedDance = TRUE
				RESET_NET_TIMER(ref_local.tlDanceTimer)
				START_NET_TIMER(ref_local.tlDanceTimer)
				
				RESET_NET_TIMER(ref_local.tRPDurationMinute)
				START_NET_TIMER(ref_local.tRPDurationMinute)
				PRINTLN("[DANCING] REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION( 'DANCE' )")	
			ENDIF
		ENDIF
		
		IF ref_local.bHaveStartedDance 
		
			// Flourish input, loading & startup
			BOOL bExitForFlourish
			BOOL bFullBody
			BOOL bWaitngForDoubleTap
			IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_LOADING_FLOURISH)
				IF DANCE_HAS_FLOURISH_LOADED(ref_local.flourish)
					CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_LOADING_FLOURISH)
					#IF FEATURE_HEIST_ISLAND_DANCES
					DANCE_CREATE_CURRENT_FLOURISH_PROP_MODEL(ref_local.flourish)
					#ENDIF
					DANCE_TASK_SCRIPTED_ANIMATION(ref_local)
					DANCE_TRIGGER_BOOST(ref_local, ref_local.display.iIntensity)
				ENDIF
				bExitForFlourish = TRUE
			ELIF (NOT Is_Drunk_Or_High_Cam_Effect_Active() OR (ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3))
			AND IS_FLOURISH_JUST_TRIGGERED(ref_local, bFullBody, bWaitngForDoubleTap)
				SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_LOADING_FLOURISH)
				IF NOT ref_local.flourish.bStartedLoad
					DANCE_PRELOAD_FLOURISH_ACTION(ref_local.flourish)
				ENDIF
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					bFullbody = FALSE
				ENDIF
				DANCE_SET_FLOURISH_IS_FULLBODY(ref_local.flourish, bFullbody)
				bExitForFlourish = TRUE
			ELIF bWaitngForDoubleTap
				IF NOT ref_local.flourish.bStartedLoad
					DANCE_PRELOAD_FLOURISH_ACTION(ref_local.flourish)
				ENDIF
				bExitForFlourish = TRUE
			ENDIF
			IF bExitForFlourish
				DANCE_INPUT_STRUCT inputs	
				GET_RHYTHM_INPUT(ref_local, inputs)
				IF inputs.bBeatMatch
				OR inputs.bHoldIntensity
					DANCE_SCALEFORM_FLASH_ICON(ref_local.display)
				ENDIF		
				MAINTAIN_MINIMAL_DANCE_UI(ref_local)
				EXIT
			ENDIF
			
			// Dance action (flourish, interaction) selection
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
			OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				IF NOT Is_Drunk_Or_High_Cam_Effect_Active()
				OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
				OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
						DANCE_SET_NEXT_FLOURISH_ACTION(TRUE) //Cycle forwards as now we only have one button
					ENDIF
				ENDIF
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
				IF NOT Is_Drunk_Or_High_Cam_Effect_Active()
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
						DANCE_SET_NEXT_FLOURISH_ACTION(TRUE)
					ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
						DANCE_SET_NEXT_FLOURISH_ACTION(FALSE)
					ENDIF
				ENDIF
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
					DANCE_SET_NEXT_FLOURISH_ACTION(TRUE)
				ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
					DANCE_SET_NEXT_FLOURISH_ACTION(FALSE)
				ENDIF
			ENDIF
			
			// Dance style selection 
			IF NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_LOADING_FLOURISH)
				IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
				OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
				OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
						ref_local.iNewDanceStyle += 1
						DANCE_CLIPSET clipset
						// Check if the new index is out of range
						IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
							ref_local.iNewDanceStyle = 0
						ENDIF
					ENDIF
				ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
				OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
				OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
						ref_local.iNewDanceStyle += 1
						DANCE_CLIPSET clipset
						// Check if the new index is out of range
						IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
							ref_local.iNewDanceStyle = 0
						ENDIF
					ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
						ref_local.iNewDanceStyle -= 1
						DANCE_CLIPSET clipset
						// Check if the new index is out of range
						IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
							#IF FEATURE_HEIST_ISLAND_DANCES
							ref_local.iNewDanceStyle = 9
							#ENDIF
							
							#IF NOT FEATURE_HEIST_ISLAND_DANCES
							ref_local.iNewDanceStyle = 3
							#ENDIF
						ENDIF
					ENDIF
				ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
						ref_local.iNewDanceStyle += 1
						DANCE_CLIPSET clipset
						// Check if the new index is out of range
						IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
							ref_local.iNewDanceStyle = 0
						ENDIF
					ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
						ref_local.iNewDanceStyle -= 1
						DANCE_CLIPSET clipset
						// Check if the new index is out of range
						IF NOT GET_DANCE_CLIPSET_DATA(ref_local, ref_local.iNewDanceStyle, clipset)
							#IF FEATURE_HEIST_ISLAND_DANCES
							ref_local.iNewDanceStyle = 9
							#ENDIF
							
							#IF NOT FEATURE_HEIST_ISLAND_DANCES
							ref_local.iNewDanceStyle = 3
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF ref_local.iNewDanceStyle <> ref_local.iDanceStyle
				OR IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_CHANGED_STYLE)
					CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_CHANGED_STYLE)
					// Increment and loop the dance style 
					SET_MP_INT_CHARACTER_STAT(ref_local.instance.ePiMenuDanceStyleStat, ref_local.iNewDanceStyle)
					MAINTAIN_DANCE_STYLE(ref_local)
					// Keep up a basic UI
					MAINTAIN_MINIMAL_DANCE_UI(ref_local)
//					DANCE_MAINTAIN_SHAKE_LERP(ref_local)
					
					// Re-init some stuff
					// We need to make a note of the camera shake so we can lerp down to MIN while restarting.
//					ref_local.fCamShakeAtReset = COMPUTE_SHAKE_SPEED(ref_local) 
					DANCE_INITIALISE_SINGALS_FOR_DANCING(ref_local, ref_local.fXSignalVelocity, ref_local.fYSignalVelocity, ref_local.fIntensitySignalMax)
					DANCE_SET_BEAT_MATCH_STATE(ref_local, DBMS_DEAFULT)					
					DANCE_SCALEFORM_SET_ALT_RHYTHM_CONTROL(ref_local.display, FALSE)
					DANCE_STOP_BEAT_MATCH_IMMUNITY(ref_local)
					DANCE_SCALEFORM_RESET_UI(ref_local)
					ref_local.bHaveStartedDance = FALSE
					ref_local.bStartMoveNetwork = TRUE
					//CLEAR_PED_TASKS(pedLocal)
					EXIT
				ENDIF	
			ENDIF
			
			// Rotate the player if they are holding bumpers.
			BOOL bRotateLeft
			BOOL bRotateRight
			
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
			OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				bRotateLeft = IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LB)
				bRotateRight = IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
			OR ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
				bRotateLeft = (GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) < -0.5)
				bRotateRight = (GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) > 0.5)
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
					bRotateLeft = (GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) < -0.5)
					bRotateRight = (GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) > 0.5)
				ENDIF
			ENDIF
			
			DANCE_ROTATE_PLAYER(ref_local, bRotateLeft, bRotateRight)
		
			// Process directional inputs
			VECTOR v3Inputs
			
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
			OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				v3Inputs.x = GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR)
				v3Inputs.y = GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD)
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				
				v3Inputs.x = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_LR)
				v3Inputs.y = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_UD)
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
					
					v3Inputs.x = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_LR)
					v3Inputs.y = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_UD)
				ENDIF
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
					
					IF IS_LOOK_INVERTED()
						v3Inputs.x = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_LR)
						v3Inputs.y = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_UD) * -1
					ELSE
						v3Inputs.x = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_LR)
						v3Inputs.y = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_UD)
					ENDIF
				ENDIF
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
					
					v3Inputs.x = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR)
					v3Inputs.y = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD)
				ENDIF
			ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
				IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
					
					v3Inputs.x = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR)
					v3Inputs.y = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD)
				ENDIF
			ENDIF
			
			// Keyboard input WASD already produces a square
			IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				MAP_CIRCLE_TO_SQUARE(v3Inputs.x, v3Inputs.y, v3Inputs.x, v3Inputs.y)
			ENDIF
			
			// Map input ranges [-1, 1] to [0, 1]
			v3Inputs.x = (v3Inputs.x + 1) / 2
			v3Inputs.y = (v3Inputs.y + 1) / 2
			
			IF HAS_NET_TIMER_EXPIRED(ref_local.inputHistory.tLastWriteTime, 60)
				// Record input
				HISTORY_WRITE_NEW_INPUT(ref_local.inputHistory, v3Inputs.x, v3Inputs.y)
			ENDIF
			INT iOneBehindWriteIndex = HISTORY_ONE_BEHIND_WRITE_INDEX(ref_local.inputHistory)

			// If the player isn't spamming then we don't need to trail off
			BOOL bPlayerIsSpamming = HISTORY_IS_PLAYER_SPAMMING(ref_local.inputHistory)
			IF NOT bPlayerIsSpamming
				ref_local.inputHistory.iRead = iOneBehindWriteIndex
			ENDIF
			
			// Get oldest input and check how close we are to it
			v3Inputs = HISTORY_READ(ref_local.inputHistory)
			VECTOR v3Signal = VECTOR2_TO_3(ref_local.v2DirectionSignals)
			VECTOR v3InputDiffSignals = v3Inputs - v3Signal
			FLOAT fDist2 = VMAG2(v3InputDiffSignals)
			
			WHILE fDist2 <= 0.0001 AND (ref_local.inputHistory.iRead <> iOneBehindWriteIndex)
				v3Inputs = HISTORY_POP_READ(ref_local.inputHistory)
				v3InputDiffSignals = v3Inputs - v3Signal
				fDist2 = VMAG2(v3InputDiffSignals)
			ENDWHILE
			
			IF fDist2 > 0.0001
				FLOAT fSpeed = 0
				IF VMAG2(v3Inputs - <<0.5, 0.5, 0.5>>) >= 0.1 // If the player is holding the stick in any direction
					fSpeed = cfDIRECTION_GAIN_PER_SEC
				ELSE
					fSpeed = cfDIRECTION_DECAY_PER_SEC
				ENDIF
				fSpeed *= GET_FRAME_TIME()
				VECTOR v3Direction = NORMALISE_VECTOR(v3InputDiffSignals)
				VECTOR v3Change = v3Direction * <<fSpeed, fSpeed, fSpeed>>
								
				// Ease out
				FLOAT fDistanceToTarget = SQRT(fDist2)
				IF fDistanceToTarget < cfDIRECTION_EASE_OUT_START
					FLOAT fLerpValue = CLAMP(fDistanceToTarget / cfDIRECTION_EASE_OUT_START, 0.2, 1)
					FLOAT fModifer = cfDIRECTION_EASE_OUT_MODIFIER * fLerpValue
					VECTOR v3Modifier = <<fModifer, fModifer, 0.0>>
					v3Change *= v3Modifier
				ENDIF
				
				VECTOR v3NewVal = v3Signal + v3Change
				
				// Apply min/max so we don't actually overtake the input value
				VECTOR v3Min
				VECTOR v3Max
				//Positive X change?
				IF (v3Inputs.x > ref_local.v2DirectionSignals.x)
					v3Min.x = 0
					v3Max.x = v3Inputs.x
				ELSE
					v3Min.x = v3Inputs.x
					v3Max.x = 1
				ENDIF	
				//Positive Y change?
				IF (v3Inputs.y > ref_local.v2DirectionSignals.y)
					v3Min.y = 0
					v3Max.y = v3Inputs.y
				ELSE
					v3Min.y = v3Inputs.y
					v3Max.y = 1
				ENDIF
				
				IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
				AND NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
						IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_LR) != 0
						OR GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_LOOK_UD) != 0
							IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DELAY_MOVEMENT)
								IF NOT HAS_NET_TIMER_STARTED(ref_local.stMovementDelayTimer)
									START_NET_TIMER(ref_local.stMovementDelayTimer)
								ELIF HAS_NET_TIMER_EXPIRED(ref_local.stMovementDelayTimer, 350)
									RESET_NET_TIMER(ref_local.stMovementDelayTimer)
									
									CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DELAY_MOVEMENT)
								ENDIF
							ELSE
								ref_local.v2DirectionSignals = VECTOR3_TO_2(CLAMP_VECTOR_COMPONENTS(v3NewVal, v3Min, v3Max))
							ENDIF
						ELSE
							SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DELAY_MOVEMENT)
						ENDIF
					ELSE
						ref_local.v2DirectionSignals = VECTOR3_TO_2(CLAMP_VECTOR_COMPONENTS(v3NewVal, v3Min, v3Max))
					ENDIF
				ELSE
					ref_local.v2DirectionSignals = VECTOR3_TO_2(CLAMP_VECTOR_COMPONENTS(v3NewVal, v3Min, v3Max))
				ENDIF
			ENDIF	
									
			DANCE_MANAGE_INTENSITY_RHYTHM(ref_local, eAudioIntensity)
			DANCE_MANAGE_SPEED_SIGNAL(ref_local)
			
			SET_GAMEPLAY_CAM_SHAKE_AMPLITUDE(COMPUTE_SHAKE_SPEED(ref_local))
			
												
			#IF IS_DEBUG_BUILD
			IF ref_local.debug.bOverrideSignals
				// Move signals
				SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "INTENSITY", ref_local.debug.fOverrideIntensity)
				SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "DIRECTION", ref_local.debug.fOverrideDirectionX)
				SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "DIRECTION_UP_DOWN", ref_local.debug.fOverrideDirectionY)
				SET_TASK_MOVE_NETWORK_SIGNAL_LOCAL_FLOAT(pedLocal, "Speed", ref_local.debug.fOverrideSpeed)
				ref_local.fPhaseSignal = GET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "PHASE_OUT")
				
				// Update dominant clip
				ref_local.fCurrentIntensitySignal = ref_local.debug.fOverrideIntensity
				ref_local.v2DirectionSignals.x = ref_local.debug.fOverrideDirectionX
				ref_local.v2DirectionSignals.y = ref_local.debug.fOverrideDirectionY
				ref_local.debug.iDominantClipIndex = GET_DOMINANT_CLIP_INDEX(ref_local)
			ELSE
			#ENDIF //IS_DEBUG_BUILD
				
				// Move signals
				SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "INTENSITY", CLAMP(ref_local.fCurrentIntensitySignal, 0.0, ref_local.fIntensitySignalMax)) // fCurrentIntensitySignal can be > 1.0
				SET_TASK_MOVE_NETWORK_SIGNAL_LOCAL_FLOAT(pedLocal, "SPEED", ref_local.fSpeedSignal) 				
				SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "DIRECTION", ref_local.v2DirectionSignals.x)
				SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "DIRECTION_UP_DOWN", ref_local.v2DirectionSignals.y)
				ref_local.fPhaseSignal = GET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(pedLocal, "PHASE_OUT")
				SET_TASK_MOVE_NETWORK_SIGNAL_BOOL(pedLocal, "UpperBodyOnOff", FALSE) // We need to spam this otherwise remote players might get stuck in the inline state machine when processing our network
				
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF //IS_DEBUG_BUILD
				
			//B* 4986156				
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT_LERP_RATE(pedLocal, "INTENSITY", 0.5)
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT_LERP_RATE(pedLocal, "DIRECTION", 0.5)
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT_LERP_RATE(pedLocal, "DIRECTION_UP_DOWN", 0.5)
			
			//B* 5071285
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.tlDanceTimer) < 500
				SET_PED_CAPSULE(pedLocal, 0.5)
			ENDIF
			
			DANCE_SCALEFORM_MAINTIAN_METER_AND_LEVEL(ref_local)
			
			IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_STANDARD
				MAINTAIN_BEAT_MATCH_COMBO_REWARDS(ref_local)
				DANCE_SCALEFORM_DRAW(ref_local)			
			ENDIF
			
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		ELSE
			MAINTAIN_MINIMAL_DANCE_UI(ref_local)
		ENDIF
	ENDIF
ENDPROC

PROC DANCE_MAINTAIN_STOP_DANCING(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	BOOL bBreakout
	IF ((GET_GAME_TIMER() - ref_local.iStopDancingTime) > ciDANCE_BREAKOUT_TIME 
	AND VMAG2(<<GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X),GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_y),0>>) > 0.3)
		PRINTLN("[DANCING] DANCE_MAINTAIN_STOP_DANCING Breakout...")
		bBreakout = TRUE
	ENDIF
	
	IF bBreakout
	OR NOT IS_TASK_MOVE_NETWORK_ACTIVE(ref_local.pedLocalPlayer)
	OR GET_TASK_MOVE_NETWORK_EVENT(ref_local.pedLocalPlayer, "OutroFinished")
		PRINTLN("[DANCING] GET_TASK_MOVE_NETWORK_EVENT( .., 'OutroFinished' )")	
		CLEAR_PED_TASKS(ref_local.pedLocalPlayer)
		
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_BOOST_LEVEL_1)
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_BOOST_LEVEL_2)
		
		RESET_NET_TIMER(ref_local.stBoostDelayTimer)
		RESET_NET_TIMER(ref_local.stBoostTimer)
		RESET_NET_TIMER(ref_local.stBoostSecondaryTimer)
		DANCING_ENABLE_INTERACTION_MENU(ref_local, TRUE)
		DANCE_UNLOAD_SCALEFORM(ref_local) 
		
		STOP_GAMEPLAY_CAM_SHAKING()
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_THIRD_PERSON_FAR
			SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
		ENDIF
		IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_HEALTH_REGEN_CHANGED)
			SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(ref_local.piLocalPlayer, 0.5)
			
			CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_HEALTH_REGEN_CHANGED)
		ENDIF
		CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(ref_local.pedLocalPlayer)
		CLEAR_HELP()
		
		IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_OVERRIDE_ACTION)
			SET_MP_PLAYER_ANIM_SETTING(ref_local.iSavedActionType, ref_local.iSavedActionAnim)
			CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_OVERRIDE_ACTION)
		ENDIF
		
		ref_local.bDancingLocked = FALSE
		ref_local.bStartMoveNetwork = FALSE
		DANCE_SET_STAGE(ref_local, DLS_IDLE_NOT_DANCING)	
	ENDIF	
ENDPROC

/// PURPOSE:
///    Updates the global BS (g_SimpleInteriorData.iForthBS, BS_4_SIMPLE_INTERIOR_IS_PLAYER_DANCING)
///    and returns true if the player is dancing.
FUNC BOOL DANCE_MAINTAIN_DANCING_GLOBALS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	BOOL bDancing	
	IF DANCE_IS_LOCAL_PLAYER_DANCING(ref_local)
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(GlobalplayerBD[ref_local.iLocalPlayer].SimpleInteriorBD.iBs4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_PLAYER_IS_DANCING)
			PRINTLN("[DANCING] DANCE_MAINTAIN_DANCING_GLOBALS - BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_PLAYER_IS_DANCING [SET]")		
		ENDIF
		#ENDIF		
		SET_BIT(GlobalplayerBD[ref_local.iLocalPlayer].SimpleInteriorBD.iBs4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_PLAYER_IS_DANCING)	
		bDancing = TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD[ref_local.iLocalPlayer].SimpleInteriorBD.iBs4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_PLAYER_IS_DANCING)
			PRINTLN("[DANCING] DANCE_MAINTAIN_DANCING_GLOBALS - BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_PLAYER_IS_DANCING [CLEAR]")		
		ENDIF
		#ENDIF
		CLEAR_BIT(GlobalplayerBD[ref_local.iLocalPlayer].SimpleInteriorBD.iBs4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_PLAYER_IS_DANCING)
		bDancing = FALSE
	ENDIF
	
	IF bDancing
	AND CAN_PLAYER_USE_DANCE_CAMERA(ref_local)
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_PLAYER_CAN_USE_DANCE_CAM)
			PRINTLN("[DANCING] DANCE_MAINTAIN_DANCING_GLOBALS - BS4_SIMPLE_INTERIOR_PLAYER_CAN_USE_DANCE_CAM [SET]")		
		ENDIF				
		#ENDIF
		SET_BIT(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_PLAYER_CAN_USE_DANCE_CAM)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_PLAYER_CAN_USE_DANCE_CAM)
			PRINTLN("[DANCING] DANCE_MAINTAIN_DANCING_GLOBALS - BS4_SIMPLE_INTERIOR_PLAYER_CAN_USE_DANCE_CAM [CLEAR]")		
		ENDIF
		#ENDIF		
		CLEAR_BIT(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_PLAYER_CAN_USE_DANCE_CAM)
	ENDIF
	RETURN bDancing
ENDFUNC

PROC DANCE_CLEANUP_TASKS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	PED_INDEX pedPlayer = ref_local.pedLocalPlayer
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
	OR IS_ENTITY_DEAD(pedPlayer)
		EXIT
	ENDIF

	SCRIPT_TASK_NAME eTask = SCRIPT_TASK_INVALID
	SWITCH ref_local.eStage
		// No task
		CASE DLS_INIT 				
		CASE DLS_LOADING			
		CASE DLS_IDLE_NOT_DANCING 	
			PRINTLN("[DANCING] DANCE_CLEANUP_TASKS - Not checking task")	
		BREAK
		// Anim
		CASE DLS_DANCING_INTRO
		CASE DLS_FLOURISH
			PRINTLN("[DANCING] DANCE_CLEANUP_TASKS - Checking SCRIPT_TASK_PERFORM_SEQUENCE")
			eTask = SCRIPT_TASK_PERFORM_SEQUENCE
		BREAK
		// Move network
		CASE DLS_DANCING				
			PRINTLN("[DANCING] DANCE_CLEANUP_TASKS - Checking SCRIPT_TASK_MOVE_NETWORK")	
			eTask = SCRIPT_TASK_MOVE_NETWORK
		BREAK
		// Achieve heading - is this correct? @@
		CASE DLS_STOP_DANCING		
			PRINTLN("[DANCING] DANCE_CLEANUP_TASKS - Checking SCRIPT_TASK_ACHIEVE_HEADING")	
			eTask = SCRIPT_TASK_ACHIEVE_HEADING
		BREAK
	ENDSWITCH	
	
	IF eTask <> SCRIPT_TASK_INVALID
		SCRIPTTASKSTATUS eStatus = GET_SCRIPT_TASK_STATUS(pedPlayer, eTask)
		IF eStatus = WAITING_TO_START_TASK
		OR eStatus = PERFORMING_TASK
			PRINTLN("[DANCING] DANCE_CLEANUP_TASKS - Task is playing or in queue. Calling CLEAR_PED_TASKS_IMMEDIATELY( .. )")	
			CLEAR_PED_TASKS_IMMEDIATELY(pedPlayer)	
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    To be called by script using this header on cleanup.
PROC DANCE_CLEANUP(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bClearPedTasks = TRUE)
	PRINTLN("[DANCING] DANCE_CLEANUP START")
	DEBUG_PRINTCALLSTACK()
	
	IF ref_local.eStage = DLS_CLEANUP_COMPLETE
		PRINTLN("[DANCING] DANCE_CLEANUP ALREADY CLEANED UP - EXITING CLEANUP")
		EXIT
	ENDIF

	IF bClearPedTasks
		DANCE_CLEANUP_TASKS(ref_local)
	ENDIF
	
	IF DANCE_IS_LOCAL_PLAYER_DANCING(ref_local)
		IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_OVERRIDE_ACTION)
			SET_MP_PLAYER_ANIM_SETTING(ref_local.iSavedActionType, ref_local.iSavedActionAnim)
			CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_OVERRIDE_ACTION)
		ENDIF
		
		CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(ref_local.pedLocalPlayer)
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_THIRD_PERSON_FAR
			SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
		ENDIF
		CLEAR_HELP()
	ENDIF
	
	DANCING_ENABLE_INTERACTION_MENU(ref_local, TRUE)
	DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
	STOP_GAMEPLAY_CAM_SHAKING()
	DANCE_UNLOAD_ALL_CLIPSETS(ref_local)
	DANCE_UNLOAD_SCALEFORM(ref_local)
	DANCE_UNLOAD_FLOURISH_DATA(ref_local.flourish)
	DANCING_CLEANUP_POSITION_CHECK_SHAPE_TEST(ref_local)
	
	#IF DANCING_FEATURE_DUAL_DANCE
	NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(FALSE)	
	DANCING_CLEANUP_DUAL_DANCING(ref_local)
	DANCING_UNLOAD_ALL_DUAL_DANCING_ANIM_DICTS(ref_local)
	ref_local.piDualDanceTarget = INVALID_PLAYER_INDEX()
	#ENDIF

	ref_local.bHaveStartedDance = FALSE
		
	IF IS_RADAR_HIDDEN()
	AND IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_RADAR_HIDDEN)
	AND NOT IS_BROWSER_OPEN()
		DISPLAY_RADAR(TRUE)
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_RADAR_HIDDEN)
	ENDIF
		
	CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)	
	
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SET_PCF)
		SET_PED_CONFIG_FLAG(ref_local.pedLocalPlayer, PCF_UseKinematicModeWhenStationary, FALSE)
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SET_PCF)
		PRINTLN("[DANCING] SET_PED_CONFIG_FLAG - PCF_UseKinematicModeWhenStationary - FALSE")
	ENDIF
	
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_HEALTH_REGEN_CHANGED)
		SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(ref_local.piLocalPlayer, 0.5)
		
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_HEALTH_REGEN_CHANGED)
	ENDIF
	
	// Change state to DLS_CLEANUP_COMPLETE - cannot check state for dancing below this line.
	DANCE_SET_STAGE(ref_local, DLS_CLEANUP_COMPLETE)
	DANCE_MAINTAIN_PC_CONTROLS(ref_local, TRUE)
	DANCE_MAINTAIN_DANCING_GLOBALS(ref_local) // Must set state to cleanup_complete before hitting this function call.
	
	CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_BOOST_LEVEL_1)
	CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_BOOST_LEVEL_2)
	
	RESET_NET_TIMER(ref_local.stBoostDelayTimer)
	RESET_NET_TIMER(ref_local.stBoostTimer)
	RESET_NET_TIMER(ref_local.stBoostSecondaryTimer)
	RESET_NET_TIMER(ref_local.tRPDurationMinute)
	
	PRINTLN("[DANCING] DANCE_CLEANUP END")
	PRINTLN("[DANCING] ------------------------------------------------------")
ENDPROC

#IF IS_DEBUG_BUILD
PROC PRINT_KEY_VAL(TEXT_LABEL_15& key, TEXT_LABEL_15& val, FLOAT x, FLOAT& y, BOOL bValueIsListeral = TRUE)
	DANCE_FORMAT_TEXT()
	
	DISPLAY_TEXT_WITH_LITERAL_STRING(x, y, "STRING", key)
	
	DANCE_FORMAT_TEXT()
	
	IF bValueIsListeral
		DISPLAY_TEXT_WITH_LITERAL_STRING(x + 0.1, y, "STRING", val)
	ELSE
		DISPLAY_TEXT(x + 0.1, y, val)
	ENDIF
	
	y += 0.03
ENDPROC

FUNC STRING GET_CONTROL_SCHEME_NAME(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	SWITCH ref_local.eControlScheme
		CASE DANCE_CONTROL_SCHEME_STANDARD		RETURN "Standard"
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_1	RETURN "Alternate 1"
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_2	RETURN "Alternate 2"
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_3	RETURN "Alternate 3"
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_4	RETURN "Alternate 4"
		CASE DANCE_CONTROL_SCHEME_ALTERNATE_5	RETURN "Alternate 5"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_AUDIO_INTENSITY_NAME(NIGHTCLUB_AUDIO_TAGS eAudioIntensity)
	SWITCH eAudioIntensity
		CASE AUDIO_TAG_NULL			RETURN "Null"
		CASE AUDIO_TAG_LOW			RETURN "Low"
		CASE AUDIO_TAG_MEDIUM		RETURN "Medium"
		CASE AUDIO_TAG_HIGH			RETURN "High"
		CASE AUDIO_TAG_HIGH_HANDS	RETURN "High Hands"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_INTENSITY_OVERRIDE_STRING(INT iIntensityOverride)
	SWITCH iIntensityOverride
		CASE 0	RETURN "Off"
		CASE 1	RETURN "Level 1"
		CASE 2	RETURN "Level 2"
		CASE 3	RETURN "Level 3"
		CASE 4	RETURN "Level 4"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC DANCE_DO_MANDATORY_DEBUG_DRAWING(PLAYER_DANCE_LOCAL_STRUCT& ref_local, NIGHTCLUB_AUDIO_TAGS eAudioIntensity)
	FLOAT fDistanceBetweenText = 0.03
	FLOAT x = cfDANCE_DIFF_TEXT_X
	FLOAT y = cfDANCE_DIFF_TEXT_Y + fDistanceBetweenText
	
	TEXT_LABEL_15 key
	TEXT_LABEL_15 val
	
 	key = "Dance style"
	val = GET_DANCE_STYLE_LABEL(ref_local, ref_local.iDanceStyle)
	PRINT_KEY_VAL(key, val, x, y, FALSE)
	
	key = "Player action"
	val = INTERACTION_ANIMS_TEXT_LABEL(GET_MP_PLAYER_ANIM_TYPE(), GET_MP_PLAYER_ANIM_SETTING())
	PRINT_KEY_VAL(key, val, x, y, FALSE)
	
	key = "Intensity level"
	val = (ref_local.display.iIntensity + 1)
	PRINT_KEY_VAL(key, val, x, y)
	
	key = "Signal value"
	val = FLOAT_TO_STRING(ref_local.fCurrentIntensitySignal * 100, 0,FORMATFLOATTOSTRING_PERCENTAGE)
	PRINT_KEY_VAL(key, val, x, y)
	
	key = "Control scheme"
	val = GET_CONTROL_SCHEME_NAME(ref_local)
	PRINT_KEY_VAL(key, val, x, y)
	
	key = "Control normal"
	val = FLOAT_TO_STRING(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT))
	PRINT_KEY_VAL(key, val, x, y)
	
	key = "Audio intensity"
	val = GET_AUDIO_INTENSITY_NAME(eAudioIntensity)
	PRINT_KEY_VAL(key, val, x, y)
	
	IF ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD
	AND ref_local.eControlScheme != DANCE_CONTROL_SCHEME_ALTERNATE_1
		key = "Boost lock"
		val = PICK_STRING(ref_local.bBoostLocked, "Locked", "Unlocked")
		PRINT_KEY_VAL(key, val, x, y)
	ENDIF
	
	IF ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD
		key = "Alternate Boost"
		val = PICK_STRING(ref_local.bAltBoost, "On", "Off")
		PRINT_KEY_VAL(key, val, x, y)
		
		key = "Force intensity"
		val = GET_INTENSITY_OVERRIDE_STRING(ref_local.iIntensityOverride)
		PRINT_KEY_VAL(key, val, x, y)
	ENDIF
ENDPROC

PROC DO_PLAYER_DANCE_DEBUG_DRAWING(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	// Current intensity
	INT r,g,b,a 
	r = CEIL((1 - ref_local.fCurrentIntensitySignal) * 255.0)
	g = CEIL(ref_local.fCurrentIntensitySignal * 255.0)
	b = 25
	a = 255
	FLOAT fMaxHeight, fHeight
	fMaxHeight = 0.3
	fHeight  = (ref_local.fCurrentIntensitySignal * fMaxHeight)
	DRAW_RECT( 0.05, 0.6 - (fMaxHeight/2), 0.03, fMaxHeight , r, g, b, a/4 )
	DRAW_RECT( 0.05, 0.6 - (fHeight/2), 0.03, fHeight , r, g, b, a )	
	
	// True intensity (input value)
	r = 220 g = 220 b = 220 a = 255
	FLOAT fSliderHeight = 0.008
	DRAW_RECT( 0.05, 0.6 - (ref_local.fTrueIntensitySignal * fMaxHeight) - (fSliderHeight *0.5), 0.03 , fSliderHeight, r, g, b, a )	
				
	VECTOR vInputs = HISTORY_READ_NEWEST(ref_local.inputHistory)			
	// Current x direction
	r = CEIL((1 - ref_local.v2DirectionSignals.x) * 255.0)
	g = CEIL(ref_local.v2DirectionSignals.x * 255.0)
	b = 25
	a = 255		
	FLOAT fMaxWidth, fSliderWidth
	fMaxWidth = 0.2
	fSliderWidth = 0.005
	DRAW_RECT( 0.035 + (0.5 * fMaxWidth), 0.65, fMaxWidth, 0.04, r, g, b, a/4)
	DRAW_RECT( 0.035 + (ref_local.v2DirectionSignals.x * fMaxWidth) + (fSliderWidth*0.5), 0.65, fSliderWidth, 0.04, r, g, b, a)
	
	// True x direction (input value)
	r = 220 g = 220 b = 220 a = 255
	DRAW_RECT( 0.035 + (vInputs.x * fMaxWidth) + (fSliderWidth*0.5), 0.65, fSliderWidth, 0.04, r, g, b, a)	
		
	// Current y direction
	r = CEIL(ref_local.v2DirectionSignals.y * 255.0)
	g = CEIL((1-ref_local.v2DirectionSignals.y) * 255.0)
	b = 25
	a = 255		
	fHeight  = ((1-ref_local.v2DirectionSignals.y) * fMaxHeight)
	DRAW_RECT( 0.09, 0.6 - (fMaxHeight/2), 0.03, fMaxHeight , r, g, b, a/4 )
	DRAW_RECT( 0.09, 0.6 - fHeight - (fSliderHeight *0.5), 0.03 , fSliderHeight, r, g, b, a )	
		
	// True y direction (input value)
	r = 220 g = 220 b = 220 a = 255
	DRAW_RECT( 0.09, 0.6 - ((1 - vInputs.y) * fMaxHeight) - (fSliderHeight *0.5), 0.03 , fSliderHeight, r, g, b, a )		
	
	// Speed
	r = 220 g = 220 b = 220 a = 255
	fHeight = fMaxHeight * (ref_local.fSpeedSignal - cfDANCE_MIN_SPEED_MULTIPLIER) / (cfDANCE_MAX_SPEED_MULTIPLIER-cfDANCE_MIN_SPEED_MULTIPLIER) 
	DRAW_RECT( 0.13, 0.6 - (fMaxHeight/2), 0.03, fMaxHeight , r, g, b, a/4 )
	DRAW_RECT( 0.13, 0.6 - fHeight - (fSliderHeight *0.5), 0.03 , fSliderHeight, r, g, b, a )	
		
	
	// Phase
	r = 220 g = 220 b = 220 a = 255
	DRAW_RECT( 0.17, 0.6 - (fMaxHeight/2), 0.03, fMaxHeight , r, g, b, a/4 )
	DRAW_RECT( 0.17, 0.6 - (ref_local.fPhaseSignal * fMaxHeight) - (fSliderHeight *0.5), 0.03 , fSliderHeight, r, g, b, a )	
	
	// Next beat
	FLOAT fTimeLeft = ref_local.audio.fSecondsToNextBeat
	FLOAT fTimeSinceLastBeat = TO_FLOAT(GET_GAME_TIMER() - ref_local.audio.iPrevBeatTime)/ 1000.0
	FLOAT fMaxTime =fTimeSinceLastBeat + fTimeLeft
	FLOAT fCurrentTime = fMaxTime-fTimeLeft
	FLOAT fNormalisedTime = fCurrentTime/ fMaxTime
	fHeight = fNormalisedTime * fMaxHeight
	DRAW_RECT( 0.21, 0.6 - (fMaxHeight/2), 0.03, fMaxHeight , r, g, b, a/4 )
	DRAW_RECT( 0.21, 0.6 - (fHeight/2), 0.03, fHeight , r, g, b, a )
	
	r = 0 g = 0 b = 0 a = 255
	IF ref_local.debug.bInputHit
		g = 200
	ELSE
		r = 200
	ENDIF
	FLOAT fPrevInputTime =  TO_FLOAT(ref_local.debug.iInputTime - ref_local.audio.iPrevBeatTime) / 1000.0
	FLOAT fPrevInputNormalisedTime = fPrevInputTime / fMaxTime
	IF fPrevInputNormalisedTime >= 0
		DRAW_RECT( 0.21, 0.6 - (fPrevInputNormalisedTime * fMaxHeight) - (fSliderHeight *0.5), 0.03 , fSliderHeight, r, g, b, a )	
	ENDIF
	FLOAT fNormWindow =  (TO_FLOAT(ref_local.debug.iForgivenessThisFrame) / 1000.0)/fMaxTime
	r = 0 g = 0 b = 200 a = 200
	DRAW_RECT( 0.21, 0.6 - (fNormWindow * fMaxHeight) - (fSliderHeight *0.5), 0.03 , fSliderHeight, r, g, b, a )	
	fNormWindow = 1.0- ((TO_FLOAT(ref_local.debug.iForgivenessThisFrame) / 1000.0)/fMaxTime)
	DRAW_RECT( 0.21, 0.6 - (fNormWindow * fMaxHeight) - (fSliderHeight *0.5), 0.03 , fSliderHeight, r, g, b, a )	
	
	TEXT_LABEL_31 txt = "Combo count: "
	txt += ref_local.stats.iConsecutiveBeatHits
	DANCE_FORMAT_TEXT()
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.7,"STRING", txt)
	
	txt = "Beat match state: "
	txt += ENUM_TO_INT(ref_local.eBeatMatchState)
	DANCE_FORMAT_TEXT()
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.73,"STRING", txt)

	txt = "Move state: "
	PED_INDEX pedLocal = ref_local.pedLocalPlayer
	IF IS_TASK_MOVE_NETWORK_ACTIVE(pedLocal)
		txt += 	GET_TASK_MOVE_NETWORK_STATE(pedLocal)
	ELSE
		txt += "None"
	ENDIF
	DANCE_FORMAT_TEXT()
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.76,"STRING", txt)
	
ENDPROC

#IF DANCING_FEATURE_DUAL_DANCE

ENUM DANCING_ANIM_BLENDS
	DAB_NONE
	,DAB_WALK_BLEND          
	,DAB_REALLY_SLOW_BLEND
	,DAB_SLOW_BLEND               
	,DAB_NORMAL_BLEND            
	,DAB_FAST_BLEND                  
	,DAB_INSTANT_BLEND     
ENDENUM

DEBUGONLY FUNC STRING DANCING_DEBUG_GET_DANCING_ANIM_BLENDS_AS_STRING(DANCING_ANIM_BLENDS eEnum)
	SWITCH eEnum
		CASE DAB_NONE				RETURN	"NONE"
		CASE DAB_WALK_BLEND			RETURN	"WALK BLEND"
		CASE DAB_REALLY_SLOW_BLEND	RETURN	"REALLY SLOW BLEND"
		CASE DAB_SLOW_BLEND			RETURN	"SLOW BLEND"
		CASE DAB_NORMAL_BLEND		RETURN	"NORMAL BLEND"
		CASE DAB_FAST_BLEND			RETURN	"FAST BLEND"
		CASE DAB_INSTANT_BLEND		RETURN	"INSTANT BLEND"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_DANCING_ANIM_BLENDS_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

DEBUGONLY FUNC FLOAT DANCING_GET_BLEND_IN_DURATION_VALUE(DANCING_ANIM_BLENDS eBlend)
	SWITCH eBlend
		CASE DAB_WALK_BLEND			RETURN	WALK_BLEND_IN
		CASE DAB_REALLY_SLOW_BLEND	RETURN	REALLY_SLOW_BLEND_IN
		CASE DAB_SLOW_BLEND			RETURN	SLOW_BLEND_IN
		CASE DAB_NORMAL_BLEND		RETURN	NORMAL_BLEND_IN
		CASE DAB_FAST_BLEND			RETURN	FAST_BLEND_IN
		CASE DAB_INSTANT_BLEND		RETURN	INSTANT_BLEND_IN
	ENDSWITCH
	
	RETURN NORMAL_BLEND_IN
ENDFUNC

DEBUGONLY FUNC FLOAT DANCING_GET_BLEND_OUT_DURATION_VALUE(DANCING_ANIM_BLENDS eBlend)
	RETURN -DANCING_GET_BLEND_IN_DURATION_VALUE(eBlend)
ENDFUNC

DEBUGONLY PROC DANCING_DEBUG_ADD_DUAL_DANCE_BLEND_COMBO(INT &ISelection, STRING strName, STRING strSuffix) 	
	START_NEW_WIDGET_COMBO()
		INT iMaxBlendTypes = COUNT_OF(DANCING_ANIM_BLENDS)
		INT iBlendType
		TEXT_LABEL_63 tlName
		DANCING_ANIM_BLENDS eBlend
		
		REPEAT iMaxBlendTypes iBlendType
			eBlend = INT_TO_ENUM(DANCING_ANIM_BLENDS, iBlendType)
			tlName = DANCING_DEBUG_GET_DANCING_ANIM_BLENDS_AS_STRING(eBlend)
			tlName += " "
			tlName += strSuffix
			ADD_TO_WIDGET_COMBO(tlName)
		ENDREPEAT
	STOP_WIDGET_COMBO(strName, ISelection)
ENDPROC

DEBUGONLY PROC DANCING_DEBUG_ADD_DUAL_DANCE_CLIP_BLEND_OVERRIDE_WIDGET(PLAYER_DANCE_LOCAL_STRUCT &ref_local, DUAL_DANCE_CLIP eClip)
	INT iClip = ENUM_TO_INT(eClip)
	STRING strClipName = DEBUG_GET_DUAL_DANCE_CLIP_AS_STRING(eClip)
	
	START_WIDGET_GROUP(strClipName)
		START_WIDGET_GROUP("PED A")
			DANCING_DEBUG_ADD_DUAL_DANCE_BLEND_COMBO(ref_local.debug.iBlendInOverrides[iClip][1], "Blend In", "IN")
			DANCING_DEBUG_ADD_DUAL_DANCE_BLEND_COMBO(ref_local.debug.iBlendOutOverrides[iClip][1], "Blend Out", "OUT")
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("PED B")
			DANCING_DEBUG_ADD_DUAL_DANCE_BLEND_COMBO(ref_local.debug.iBlendInOverrides[iClip][0], "Blend In", "IN")
			DANCING_DEBUG_ADD_DUAL_DANCE_BLEND_COMBO(ref_local.debug.iBlendOutOverrides[iClip][0], "Blend Out", "OUT")
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC DANCING_DEBBUG_ADD_ALL_DUAL_DANCE_BLEND_OVERRIDE_WIDGETS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	START_WIDGET_GROUP("Anim Blend Overrides")
		INT iMaxAnimBlends = COUNT_OF(ref_local.debug.iBlendInOverrides)
		INT iAnim 
		DUAL_DANCE_CLIP eClip = DDC_INVALID
		
		REPEAT iMaxAnimBlends iAnim
			
			eClip = INT_TO_ENUM(DUAL_DANCE_CLIP, iAnim)
			DANCING_DEBUG_ADD_DUAL_DANCE_CLIP_BLEND_OVERRIDE_WIDGET(ref_local, eClip)
		ENDREPEAT
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

/// PURPOSE:
///   Call once in widget init of script using this header.
///    Called from DANCE_MAINTAIN_UPDATE if IS_DEBUG_BUILD.
///    Prevent widget processing by setting PLAYER_DANCE_LOCAL_STRUCT.debug.bUseDebug = FALSE
PROC DANCE_MAINTAIN_WIDGETS(PLAYER_DANCE_LOCAL_STRUCT& ref_local, NIGHTCLUB_AUDIO_TAGS eAudioIntensity)
	IF NOT ref_local.debug.bUseDebug
		EXIT
	ENDIF
	INT i
	SWITCH ref_local.debug.eWidgetStage
		CASE DWS_INIT
			START_WIDGET_GROUP("Player dancing")
				ADD_WIDGET_INT_SLIDER("Boost Vibration Duration", ciDANCE_BOOST_SHAKE_DURATION, 0, 10000, 1)
				ADD_WIDGET_INT_SLIDER("Boost Vibration Frequency", ciDANCE_BOOST_SHAKE_FREQUENCY, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Camera Interpolation Time", ciDANCE_CAMERA_INTERPOLATION, 0, 10000, 1)
				ADD_WIDGET_INT_SLIDER("Camera Interpolation Time Half Press", ciDANCE_CAMERA_INTERPOLATION_HALF, 0, 10000, 1)
				ADD_WIDGET_BOOL("Reload", ref_local.debug.bRefresh)
				ADD_WIDGET_BOOL("Do debug drawing", ref_local.debug.bDoDebugDrawing)
				ADD_WIDGET_BOOL("Clear 'have danced' stat", ref_local.debug.bClearHaveDancedStat)
				ADD_WIDGET_BOOL("Reset 'dance hint' stat", ref_local.debug.bResetHintCounterStat)
				ADD_WIDGET_FLOAT_SLIDER("Beat sync forgivenss (s)", cfBEAT_SYNC_FORGIVENESS_SEC, 0, 0.01, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Move network blend", cfMOVE_NETWORK_BLEND, -1000, 1000, 0.1)
				ADD_WIDGET_INT_SLIDER("Breakout timer", ciDANCE_BREAKOUT_TIME,0,2500,100)
				ADD_WIDGET_INT_SLIDER("Move flourish state", ref_local.flourish.iState,0,3,1)
				
				ADD_WIDGET_BOOL("Current Language Complex", g_bSetCurrentLanguageComplexForDancingHelp)
				
				ADD_WIDGET_INT_READ_ONLY("Dance area", ref_local.iDanceAreaIndex)
				ADD_WIDGET_INT_READ_ONLY("Block area", ref_local.iBlockAreaStagger)
							
				START_WIDGET_GROUP("First person cam limits")
					ADD_WIDGET_FLOAT_SLIDER("Pitch Min", cfFIRST_PERSON_DANCE_CAM_PITCH_MIN_LIMIT, -360, 360, 1)
					ADD_WIDGET_FLOAT_SLIDER("Pitch Max", cfFIRST_PERSON_DANCE_CAM_PITCH_MAX_LIMIT, -360, 360, 1)
					ADD_WIDGET_FLOAT_SLIDER("Heading Min", cfFIRST_PERSON_DANCE_CAM_HEADING_MIN_LIMIT, -360, 360, 1)
					ADD_WIDGET_FLOAT_SLIDER("Heading Max", cfFIRST_PERSON_DANCE_CAM_HEADING_MAX_LIMIT, -360, 360, 1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Facial anims")
					START_NEW_WIDGET_COMBO()
						ADD_TO_WIDGET_COMBO("None")
						ADD_TO_WIDGET_COMBO("mood_dancing_low_1")
						ADD_TO_WIDGET_COMBO("mood_dancing_low_2")
						ADD_TO_WIDGET_COMBO("mood_dancing_low_3")
						ADD_TO_WIDGET_COMBO("mood_dancing_medium_1")
						ADD_TO_WIDGET_COMBO("mood_dancing_medium_2")
						ADD_TO_WIDGET_COMBO("mood_dancing_medium_3")
						ADD_TO_WIDGET_COMBO("mood_dancing_high_1")
						ADD_TO_WIDGET_COMBO("mood_dancing_high_2")
					STOP_WIDGET_COMBO("Random anim override", ref_local.faceAnim.iRandomOverride)
					ADD_WIDGET_BOOL("Force new random anim", ref_local.faceAnim.bForceNewRandAnim)
					ADD_WIDGET_BOOL("Block anims", ref_local.faceAnim.bBlock)
				STOP_WIDGET_GROUP()
							
				STARt_WIDGET_GROUP("Anti spam")			
					ADD_WIDGET_INT_SLIDER("Spam check window (ms)", ciHISTORY_SPAM_TIME_MS, 0, 5000, 500)
					ADD_WIDGET_FLOAT_SLIDER("Spam speed", cfHISTORY_SPAM_SPEED, 0.0, 100.0, 1.0)
					ADD_WIDGET_INT_READ_ONLY("Write index", ref_local.inputHistory.iWrite)
					ADD_WIDGET_INT_READ_ONLY("Read index", ref_local.inputHistory.iRead)
				STOP_WIDGET_GROUP()			
							
				START_WIDGET_GROUP("Flourish")
					ADD_WIDGET_FLOAT_SLIDER("Double tap delay (s)", cfFLOURISH_INPUT_DOUBLE_PRESS_DELAY, 0, 2.0, 0.1)
					ADD_WIDGET_INT_SLIDER("Restart timer", ciDANCE_RESTART_TIME,0, 2000, 100)
					ADD_WIDGET_FLOAT_SLIDER("Flourish intro phase", cfFLOURISH_INTRO_PHASE, 0, 1, 0.05)	
					ADD_WIDGET_INT_SLIDER("Flourish exit time (Seconds)", ciDANCE_FLOURISH_ANIM_CHAIN_TIME, 0, 1000, 1)
					ADD_WIDGET_INT_SLIDER("Flourish exit time (Seconds) Full body", ciDANCE_FLOURISH_ANIM_CHAIN_TIME_FB, 0, 1000, 1)		
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Audio")
					ADD_WIDGET_FLOAT_READ_ONLY("Time to next beat (seconds)", ref_local.audio.fSecondsToNextBeat)
					ADD_WIDGET_FLOAT_READ_ONLY("bpm", ref_local.audio.fBpm)
					ADD_WIDGET_INT_READ_ONLY("Total beat count", ref_local.audio.iTotalBeatCount)
					ADD_WIDGET_INT_READ_ONLY("Prev beat time (ms)", ref_local.audio.iPrevBeatTime)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Combo")
					ADD_WIDGET_INT_READ_ONLY("Combo count", ref_local.stats.iConsecutiveBeatHits)
					ADD_WIDGET_INT_READ_ONLY("Combo duration (mins)", ref_local.stats.iComboMins)
					ADD_WIDGET_INT_READ_ONLY("Combo duration (ms)", ref_local.debug.iComboDurationMs)
					ADD_WIDGET_INT_SLIDER("Rp period duration (ms)", g_sMPTunables.iBB_NIGHTCLUB_BEAT_MATCHING_TIME_BEAT_MATCHING_TO_INCREASE, 0, 120000, 10000)
					ADD_WIDGET_INT_READ_ONLY("Rp period count", ref_local.stats.iRpPeriodCount)
					
					START_WIDGET_GROUP("Stats")
						ADD_WIDGET_BOOL("Update stats", ref_local.debug.bUpdateStats)
						ADD_WIDGET_BOOL("Reset stats", ref_local.debug.bResetComboStats)
						ADD_WIDGET_INT_READ_ONLY("Lifetime combo duration (mins)", ref_local.debug.iLifetimeComboDurationMins)
						ADD_WIDGET_INT_READ_ONLY("Combo reward count today", ref_local.debug.iComboCountToday)
						ADD_WIDGET_INT_READ_ONLY("Reward count reset timer (mins)", ref_local.debug.iComboResetTimer)
					STOP_WIDGET_GROUP()
					
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Beat matching inputs")
					ADD_WIDGET_BOOL("Auto beat match", ref_local.debug.bAutoBeatMatch)
					ADD_WIDGET_FLOAT_SLIDER("Beat half window", cfBEAT_MATCH_WINDOW, 0, 1, 0.05)
					ADD_WIDGET_FLOAT_SLIDER("Beat half window abs min (ms)", ciBEAT_HALF_MIN_ABS_WINDOW_MS, 0, 500, 25)
					ADD_WIDGET_FLOAT_SLIDER("Beat half window abs max (ms)", ciBEAT_HALF_MAX_ABS_WINDOW_MS, 0, 10000, 25)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Controller")
					ADD_WIDGET_FLOAT_SLIDER("Lerp time pow", cfPOW_TIME, 0, 5, 0.25)
					ADD_WIDGET_INT_SLIDER("Lerp time to default (ms)", ciDANCE_CONTROL_INTERP_TIME_MS, 0, 5000, 100)
					ADD_WIDGET_INT_SLIDER("Lerp time (ms)", ciDANCE_CONTROL_BEAT_INTERP_TIME_MS, 0, 5000, 100)
					ADD_WIDGET_INT_SLIDER("Default r", ciDANCE_CONTROL_DEFAULT_R, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Default g", ciDANCE_CONTROL_DEFAULT_G, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Default b", ciDANCE_CONTROL_DEFAULT_B, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Hit r", ciDANCE_CONTROL_HIT_R, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Hit g", ciDANCE_CONTROL_HIT_G, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Hit b", ciDANCE_CONTROL_HIT_B, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Miss r", ciDANCE_CONTROL_MISS_R, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Miss g", ciDANCE_CONTROL_MISS_G, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Miss b", ciDANCE_CONTROL_MISS_B, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Fail r", ciDANCE_CONTROL_FAIL_R, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Fail g", ciDANCE_CONTROL_FAIL_G, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("Fail b", ciDANCE_CONTROL_FAIL_B, 0, 255, 1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Camera")
					ADD_WIDGET_FLOAT_SLIDER("Rotation: FP heading",ciDANCE_FP_CAM_FORCE_HEADING, 0, 360, 1)
					ADD_WIDGET_FLOAT_SLIDER("Rotation: FP pitch",ciDANCE_FP_CAM_FORCE_PITCH, 0, 360, 1)
					ADD_WIDGET_FLOAT_SLIDER("Rotation: FP smooth rate",ciDANCE_FP_CAM_SMOOTH_RATE, 0, 100, 0.1)		
					ADD_WIDGET_FLOAT_SLIDER("Shake min amp: ", cfSHAKE_AMP_MIN, 0.0, 2.5, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Shake min amp: ", cfSHAKE_AMP_MAX, 0.0, 2.5, 0.1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Signals")
					ADD_WIDGET_FLOAT_READ_ONLY("Last inten sig change", ref_local.debug.fLastIntensityChange)
					ADD_WIDGET_FLOAT_READ_ONLY("Inten sig before last change", ref_local.debug.fIntensityBeforeChange)
					
					ADD_WIDGET_BOOL("Override signals", ref_local.debug.bOverrideSignals)
					ADD_WIDGET_FLOAT_SLIDER("[override] Signal : Intensity", ref_local.debug.fOverrideIntensity, 0, 1, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("[override] Signal : x", ref_local.debug.fOverrideDirectionX, 0, 1, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("[override] Signal : y", ref_local.debug.fOverrideDirectionY, 0, 1, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("[override] Signal : Speed", ref_local.debug.fOverrideSpeed, 0, 2, 0.01) 
					ADD_WIDGET_FLOAT_READ_ONLY("True signal : Intensity", ref_local.fTrueIntensitySignal)
					ADD_WIDGET_FLOAT_READ_ONLY("Current signal : Intensity", ref_local.fCurrentIntensitySignal)
					ADD_WIDGET_FLOAT_READ_ONLY("Current signal : x", ref_local.v2DirectionSignals.x)
					ADD_WIDGET_FLOAT_READ_ONLY("Current signal : y", ref_local.v2DirectionSignals.y)
					ADD_WIDGET_FLOAT_READ_ONLY("RO signal : Phase", ref_local.fPhaseSignal) 
					ADD_WIDGET_FLOAT_READ_ONLY("signal : Speed", ref_local.fSpeedSignal)	
					START_NEW_WIDGET_COMBO()
						DANCE_CLIP eClip
						REPEAT COUNT_OF(DANCE_CLIP) i
							eClip = INT_TO_ENUM(DANCE_CLIP, i)
							ADD_TO_WIDGET_COMBO(GET_DANCE_CLIP_STRING(eClip))
						ENDREPEAT
					STOP_WIDGET_COMBO("Dominant clip", ref_local.debug.iDominantClipIndex)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Intensity modifiers")
					ADD_WIDGET_FLOAT_SLIDER("Boost multiplier", cfINTENSITY_GAIN_BOOST_MULTIPLIER, 0, 10, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Intensity decay speed", cfINTENSITY_DECAY_PER_SEC, 0, 5, 0.05)
					ADD_WIDGET_FLOAT_SLIDER("Intensity increase speed", cfINTENSITY_GAIN_PER_SEC, 0, 5, 0.05)
					ADD_WIDGET_FLOAT_SLIDER("Input ease out start", cfINTENSITY_EASE_OUT_START, 0, 1, 0.005)
					ADD_WIDGET_FLOAT_SLIDER("Input ease out modifer", cfINTENSITY_EASE_OUT_MODIFIER, 0, 10, 0.1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Direction modifiers")
					ADD_WIDGET_FLOAT_SLIDER("Direction acceleration", cfDIRECTION_ACC, 0, 20, 0.05)
					ADD_WIDGET_FLOAT_SLIDER("Input ease out start", cfDIRECTION_EASE_OUT_START, 0, 1, 0.005)
					ADD_WIDGET_FLOAT_SLIDER("Input ease out modifer", cfDIRECTION_EASE_OUT_MODIFIER, 0, 20, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Direction match input speed", cfDIRECTION_GAIN_PER_SEC, 0, 20, 0.05)
					ADD_WIDGET_FLOAT_SLIDER("Direction return-to-default speed", cfDIRECTION_DECAY_PER_SEC, 0, 20, 0.05)
				STOP_WIDGET_GROUP()
								
				START_WIDGET_GROUP("Scaleform")
					ADD_WIDGET_INT_READ_ONLY("current level", ref_local.display.iIntensity)
					ADD_WIDGET_FLOAT_READ_ONLY("meter", ref_local.display.fMeterValue)
					ADD_WIDGET_FLOAT_SLIDER("meter speed", cfMETER_LERP_SPEED, 0.0, 1000.0, 5.0)
					ADD_WIDGET_INT_SLIDER("Hold rhythm btn duration", ciHOLD_RHYTHM_BTN_FLASH_MS, 0, 1000, 50)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Rotation")
					ADD_WIDGET_FLOAT_SLIDER("Degrees per sec", cfROTATION_SPEED, 0, 2500, 100)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Difficulty")
					ADD_WIDGET_FLOAT_SLIDER("Text x", cfDANCE_DIFF_TEXT_X, 0, 1, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Text y", cfDANCE_DIFF_TEXT_Y, 0, 1, 0.1)
					ADD_WIDGET_INT_SLIDER("Text display time (ms)", ciDANCE_DIFF_TEXT_TIME_MS, 0, 10000, 500)
					ADD_WIDGET_INT_SLIDER("Text begin fadeout time (ms)", ciDANCE_DIFF_TEXT_FADE_MS, 0, 10000, 500)
					ADD_WIDGET_INT_SLIDER("Boost cooldown", ciBOOST_COOLDOWN_BEATS, 0, 255, 1)
				STOP_WIDGET_GROUP()
				
				#IF DANCING_FEATURE_DUAL_DANCE
				IF ref_local.instance.getDualDanceAnimDict != NULL
					START_WIDGET_GROUP("Dual Dance")
						ADD_WIDGET_BOOL("Draw Broadcast Data", ref_local.debug.bDrawBroadcastData)
						ADD_WIDGET_BOOL("Dual Dance Anywhere", ref_local.instance.bCanDualDanceAnywhere)
						ADD_WIDGET_BOOL("Start dual dance with bot", ref_local.debug.bDualDanceWithBot)
						ADD_WIDGET_INT_SLIDER("Dance Style", ref_local.playerBD[ref_local.iLocalPlayer].iDualDanceStyle, 0, 9, 1)
						
						START_WIDGET_GROUP("Activation")
							ADD_WIDGET_FLOAT_SLIDER("Activation Facing Accuracy (degrees)", cfDANCING_DUAL_DANCE_FACING_ACCURACY, 0.0, 360.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("In Range Check Forward Offset", cfDANCING_DUAL_DANCE_IN_RANGE_FORWARD_OFFSET, 0.0, 5.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("In Range check radius", cfDANCING_DUAL_DANCE_IN_RANGE_RADIUS, 0.0, 5.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("Ray check dist", cfDANCING_DUAL_DANCE_RAYCHECK_DIST, 0.0, 100.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("Ray check height", cfDANCING_DUAL_DANCE_RAYCHECK_HEIGHT, 0.0, 1.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("Ray check radius", cfDANCING_DUAL_DANCE_RAYCHECK_RADIUS, 0.0, 1.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("Walk to arrive range", cfDANCING_DUAL_DANCE_MOVE_TO_POSITION_ARRIVE_RANGE, 0.0, 10.0, 0.1)
							
						STOP_WIDGET_GROUP()
						
						START_WIDGET_GROUP("Area Clear Check")
							ADD_WIDGET_FLOAT_SLIDER("Area clear check radius", cfDANCING_DUAL_DANCE_AREA_CLEAR_CHECK_RANGE, 0.0, 10.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("Area clear check offset X", cfDANCING_DUAL_DANCE_AREA_CLEAR_CHECK_OFFSET_X, -10.0, 10.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("Area clear check offset Y", cfDANCING_DUAL_DANCE_AREA_CLEAR_CHECK_OFFSET_Y, -10.0, 10.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("Area clear check offset Z", cfDANCING_DUAL_DANCE_AREA_CLEAR_CHECK_OFFSET_Z, -10.0, 10.0, 0.1)
						STOP_WIDGET_GROUP()
						
						DANCING_DEBBUG_ADD_ALL_DUAL_DANCE_BLEND_OVERRIDE_WIDGETS(ref_local)
					STOP_WIDGET_GROUP()
				ENDIF
				#ENDIF
				
				START_WIDGET_GROUP("Alternate Intensity Levels")
					ADD_WIDGET_FLOAT_SLIDER("Level 1", ref_local.fLevel1, 0.0, 1.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Level 2", ref_local.fLevel2, 0.0, 1.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Level 3", ref_local.fLevel3, 0.0, 1.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Level 4", ref_local.fLevel4, 0.0, 1.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Level 5", ref_local.fLevel5, 0.0, 1.0, 0.01)
				STOP_WIDGET_GROUP()
				
			STOP_WIDGET_GROUP()	
			ref_local.debug.eWidgetStage = DWS_UPDATE
		BREAK
		CASE DWS_UPDATE
			IF ref_local.debug.bRefresh
				ref_local.debug.bRefresh = FALSE
				DANCE_CLEANUP(ref_local)
				DANCE_SET_STAGE(ref_local, DLS_INIT)
			ENDIF
			
			IF IS_PLAYER_DANCING(ref_local.piLocalPlayer)
			#IF DANCING_FEATURE_DUAL_DANCE
			AND NOT DANCING_IS_LOCAL_PLAYER_DUAL_DANCING(ref_local)
			#ENDIF
				DANCE_DO_MANDATORY_DEBUG_DRAWING(ref_local, eAudioIntensity)			
			ENDIF
			
			IF ref_local.debug.bDoDebugDrawing
				DO_PLAYER_DANCE_DEBUG_DRAWING(ref_local)
			ENDIF
						
			IF ref_local.debug.bResetComboStats
				ref_local.debug.bResetComboStats = FALSE
				SET_PACKED_STAT_INT(PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_COUNT, 0)
				SET_PACKED_STAT_INT(PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_TIME, 0)
				RESET_BEAT_MATCH_STATS(ref_local)
			ENDIF
			
			IF ref_local.debug.bClearHaveDancedStat
				ref_local.debug.bClearHaveDancedStat = FALSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PLAYER_HAS_DANCED, FALSE)
			ENDIF
			
			IF ref_local.debug.bResetHintCounterStat
				ref_local.debug.bResetHintCounterStat = FALSE
				SET_PACKED_STAT_INT(PACKED_MP_INT_NIGHTCLUB_DANCE_HINT_COUNT, 0)
				SET_PACKED_STAT_INT(PACKED_MP_INT_CASINOCLUB_DANCE_HINT_COUNT, 0)
				SET_PACKED_STAT_INT(PACKED_MP_INT_BEACHPARTY_DANCE_HINT_COUNT, 0)
				CDEBUG1LN(DEBUG_NET_DANCING,"[DANCING][DANCE_STATS] debug reset PACKED_MP_INT_NIGHTCLUB_DANCE_HINT_COUNT to 0")
				CDEBUG1LN(DEBUG_NET_DANCING,"[DANCING][DANCE_STATS] debug reset PACKED_MP_INT_CASINOCLUB_DANCE_HINT_COUNT to 0")
				CDEBUG1LN(DEBUG_NET_DANCING,"[DANCING][DANCE_STATS] debug reset PACKED_MP_INT_BEACHPARTY_DANCE_HINT_COUNT to 0")
			ENDIF
			
			IF ref_local.stats.iConsecutiveBeatHits > 0
				ref_local.debug.iComboDurationMs = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.stats.tFirstHitTime)
			ELSE
				ref_local.debug.iComboDurationMs = -1
			ENDIF
			
			IF ref_local.debug.bUpdateStats
				ref_local.debug.iLifetimeComboDurationMins = GET_MP_INT_CHARACTER_STAT(MP_STAT_DANCE_COMBO_DURATION_MINS)
				ref_local.debug.iComboCountToday = GET_PACKED_STAT_INT(PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_COUNT)
				ref_local.debug.iComboResetTimer = GET_PACKED_STAT_INT(PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_TIME)
			ENDIF
			
			#IF DANCING_FEATURE_DUAL_DANCE
			
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_DualDanceBotShortcut")
			AND IS_DEBUG_KEY_JUST_PRESSED(KEY_D, KEYBOARD_MODIFIER_CTRL_SHIFT, "Spawn Dance Bot")
				ref_local.debug.bDualDanceWithBot = TRUE
			ENDIF
			
			IF ref_local.instance.getDualDanceAnimDict = NULL
				ref_local.debug.bDualDanceWithBot = FALSE
			ENDIF
			
			IF ref_local.debug.bDualDanceWithBot
				ref_local.debug.bDualDanceWithBot = FALSE
				
				IF ref_local.eStage = DLS_IDLE_NOT_DANCING
					DANCING_JOIN_DUAL_DANCE_WITH_DANCE_BOT(ref_local)
				ENDIF
			ENDIF			
			#ENDIF
			
			ref_local.debug.iDanceStage = ENUM_TO_INT(ref_local.eStage)
			
			
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF //IS_DEBUG_BUILD

/// PURPOSE:
///    To be called from script using this header (e.g. am_mp_nightclub.sc)
PROC DANCE_INIT_PLAYER_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DANCE_INSTANCE_DATA& ref_instanceData)
	PRINTLN("[DANCING] ")	
	PRINTLN("[DANCING] DANCE_INIT_PLAYER_DANCE ")	
	DANCE_SET_STAGE(ref_local, DLS_INIT)
	ref_local.instance = ref_instanceData
	ref_local.eControlScheme = ref_instanceData.eInitControlScheme
ENDPROC

PROC DANCE_SUPPRESS_HUD(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	INVALIDATE_IDLE_CAM()
	HUD_FORCE_WEAPON_WHEEL(FALSE)
	DISPLAY_AMMO_THIS_FRAME(FALSE)			
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
	DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	WEAPON_TYPE eWeapon
	IF GET_CURRENT_PED_WEAPON(ref_local.pedLocalPlayer,eWeapon)
	AND eWeapon != WEAPONTYPE_UNARMED
		SET_CURRENT_PED_WEAPON(ref_local.pedLocalPlayer, WEAPONTYPE_UNARMED, TRUE)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MULTIPLAYER_INFO)
	
	IF NOT IS_RADAR_HIDDEN()
		SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_RADAR_HIDDEN)
		DISPLAY_RADAR(FALSE)
	ENDIF

	BOOL bAllowMessages = FALSE
	
	/// We need to show a text when dancing during the island scoping mission but missions check is returning invalid and its too risky to fix at this point
	/// so we are going to have to infer this by checking if we are on the island but not in the beach party instance
	/// TODO: get a proper fix for this after release
	IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND() AND NETWORK_IS_IN_TUTORIAL_SESSION()
		IF NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(ref_local.piLocalPlayer) != ciHEIST_ISLAND_BEACH_PARTY_TUTORIAL_INSTANCE_ID
			bAllowMessages = TRUE
		ENDIF
	ENDIF
	
	IF NOT bAllowMessages
		THEFEED_HIDE_THIS_FRAME()
		THEFEED_FLUSH_QUEUE()
	ENDIF
ENDPROC

PROC DANCE_SET_PLAYER_IS_BUSY(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bBusy)
	
	IF bBusy
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PLAYER_IS_BUSY)
			PRINTLN("[DANCING] Player is now busy.")
		ENDIF
		#ENDIF
		SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PLAYER_IS_BUSY)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PLAYER_IS_BUSY)
			PRINTLN("[DANCING] Player is no longer busy.")
		ENDIF
		#ENDIF
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PLAYER_IS_BUSY)
	ENDIF

ENDPROC

/// PURPOSE:
///    To be called from freemode.sc to keep the dance combo rewards timer up to date.
CONST_INT ciDANCE_COMBO_REWARD_TIMER_UPDATE_PERIOD_MS 60000
CONST_INT ciDANCE_COMBO_REWARD_TIMER_UPDATE_PERIOD_MINS ciDANCE_COMBO_REWARD_TIMER_UPDATE_PERIOD_MS / 60000
STRUCT DANCE_COMBO_REWARDS_STRUCT
	SCRIPT_TIMER tLastUpdate
ENDSTRUCT
PROC DANCE_MAINTAIN_COMBO_REWARDS_TIMER(DANCE_COMBO_REWARDS_STRUCT& ref_comboData)
	// If the player is already free to combo there is nothing to do
	INT iTimeLeft = GET_PACKED_STAT_INT(PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_TIME)
	IF iTimeLeft = 0
		RESET_NET_TIMER(ref_comboData.tLastUpdate)
		EXIT
	ENDIF
	
	// Restrict this update frequency
	IF NOT HAS_NET_TIMER_EXPIRED(ref_comboData.tLastUpdate, ciDANCE_COMBO_REWARD_TIMER_UPDATE_PERIOD_MS)
		EXIT
	ENDIF
	
	iTimeLeft -= ciDANCE_COMBO_REWARD_TIMER_UPDATE_PERIOD_MINS
	IF iTimeLeft <= 0
		iTimeLeft = 0
		SET_PACKED_STAT_INT(PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_COUNT, 0)
		PRINTLN("[DANCING][DANCE_STATS] PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_COUNT set to 0 because our daily timer has expired.")
	ENDIF
	SET_PACKED_STAT_INT(PACKED_MP_INT_NIGHTCLUB_DANCE_COMBO_REWARD_TIME, iTimeLeft)
	
	// Reset update timer
	RESET_NET_TIMER(ref_comboData.tLastUpdate)
ENDPROC

PROC SCALEFORM_MAINTAIN_CONTROL_ICON(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(ref_local.display.scaleform)
		EXIT
	ENDIF
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)				
		IF NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_USING_KEYBOARD)
			SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_USING_KEYBOARD)
			DANCE_SCALEFORM_SET_CONTROL_ICON(ref_local.display, TRUE)
		ENDIF	
	ELSE
		IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_USING_KEYBOARD)
			CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_USING_KEYBOARD)
			DANCE_SCALEFORM_SET_CONTROL_ICON(ref_local.display, FALSE)
		ENDIF	
	ENDIF
ENDPROC

PROC DANCE_MAINTAIN_AUDIO_DATA(DANCE_AUDIO_DATA& ref_audio)
	INT iBeatIndex
	GET_NEXT_AUDIBLE_BEAT(ref_audio.fSecondsToNextBeat, ref_audio.fBpm, iBeatIndex)
	IF ref_audio.iBeatIndex <> iBeatIndex		
		ref_audio.iBeatIndex = iBeatIndex
		ref_audio.bBeatThisFrame = TRUE
		ref_audio.iPrevBeatTime = GET_GAME_TIMER()
		ref_audio.iTotalBeatCount++
	ELSE
		ref_audio.bBeatThisFrame = FALSE
	ENDIF	
ENDPROC

#IF IS_DEBUG_BUILD
//@difficulty
PROC DANCE_DEBUG_MAINTAIN_DIFFICULTY_SELECT(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bWasDancingLastFrame)
	
	BOOL bReset = NOT bWasDancingLastFrame AND IS_PLAYER_DANCING(ref_local.piLocalPlayer)
	
	IF bReset
		REINIT_NET_TIMER(ref_local.debug.tDifficultyTextTimer)
	ENDIF
	
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_MULTIPLY, KEYBOARD_MODIFIER_NONE, "CYCLE DANCE DIFFICULTY")
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT) // NOTE: This cannot be used because it'll minimise help
	OR bReset
		REINIT_NET_TIMER(ref_local.debug.tDifficultyTextTimer)
		ref_local.debug.iDifficulty = (ref_local.debug.iDifficulty + 1) % ciDANCE_DIFF_COUNT
		
		IF bReset
			ref_local.debug.iDifficulty	= ciDANCE_DIFF_DEFAULT
		ENDIF
		
		SWITCH ref_local.debug.iDifficulty
			CASE ciDANCE_DIFF_HARD 	
				//Default windows
				cfBEAT_MATCH_WINDOW	= 0.25
				ciBEAT_HALF_MIN_ABS_WINDOW_MS = 150.0
				ciBEAT_HALF_MAX_ABS_WINDOW_MS = 300.0
			BREAK
			CASE ciDANCE_DIFF_MED
				// You can still fail this by spamming (one input per beat)
				cfBEAT_MATCH_WINDOW	= 0.5
				ciBEAT_HALF_MIN_ABS_WINDOW_MS = 0.0
				ciBEAT_HALF_MAX_ABS_WINDOW_MS = 100000.0
			BREAK
			CASE ciDANCE_DIFF_EASY
				// Reset to defualt "hard" because we just manually ignore 
				// failed inputs when difficulty is "easy".
				cfBEAT_MATCH_WINDOW	= 0.25
				ciBEAT_HALF_MIN_ABS_WINDOW_MS = 150.0
				ciBEAT_HALF_MAX_ABS_WINDOW_MS = 300.0
			BREAK
		ENDSWITCH
		PRINTLN("[DANCING] Cycled to difficulty ", ref_local.debug.iDifficulty, " (reset = ", bReset, ")")
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(ref_local.debug.tDifficultyTextTimer)
	AND NOT HAS_NET_TIMER_EXPIRED(ref_local.debug.tDifficultyTextTimer, ciDANCE_DIFF_TEXT_TIME_MS)
		FLOAT fFadeDuration = TO_FLOAT(ciDANCE_DIFF_TEXT_TIME_MS - ciDANCE_DIFF_TEXT_FADE_MS)
		FLOAT fAlpha = 1.0 - CLAMP((GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(ref_local.debug.tDifficultyTextTimer) - ciDANCE_DIFF_TEXT_FADE_MS)/ fFadeDuration, 0, 1)
		INT a = ROUND(255.0 * fAlpha)
		INT r = 255, g = 255, b = 255
		
		TEXT_LABEL_63 txt = "Beat match style "
		SWITCH ref_local.debug.iDifficulty
			CASE ciDANCE_DIFF_HARD 	txt += (ciDANCE_DIFF_HARD + 1)	BREAK
			CASE ciDANCE_DIFF_MED 	txt += (ciDANCE_DIFF_MED + 1)	BREAK
			CASE ciDANCE_DIFF_EASY	txt += (ciDANCE_DIFF_EASY + 1)	BREAK
		ENDSWITCH
		DANCE_FORMAT_TEXT(r, g, b, a)
		DISPLAY_TEXT_WITH_LITERAL_STRING(cfDANCE_DIFF_TEXT_X, cfDANCE_DIFF_TEXT_Y,"STRING", txt)
	ENDIF
	
ENDPROC	
#ENDIF

PROC DANCE_MAINTAIN_FACIAL_ANIM(DANCE_FACE_DATA& ref_faceData, INT iDanceLevel, NIGHTCLUB_AUDIO_TAGS eAnimIntensity)
	// Debug only blocking of anims
	#IF IS_DEBUG_BUILD
	IF ref_faceData.bBlock
		IF (ref_faceData.eAnimIntensity <> AUDIO_TAG_NULL)
			PRINTLN("[PLAYER_DANCING] DANCE_MAINTAIN_FACIAL_ANIM - Clearing face anims.")
			ref_faceData.eAnimIntensity = AUDIO_TAG_NULL
			CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID())
		ENDIF	
		EXIT
	ENDIF
	#ENDIF
	
	// Adjust intensity tag to take into account player dancing intensity
	NIGHTCLUB_AUDIO_TAGS eMaxIntensity 
	NIGHTCLUB_AUDIO_TAGS eMinIntensity 
	SWITCH iDanceLevel
		CASE LEVEL_1 	eMaxIntensity = AUDIO_TAG_LOW		eMinIntensity = AUDIO_TAG_LOW BREAK
		CASE LEVEL_2 	eMaxIntensity = AUDIO_TAG_MEDIUM 	eMinIntensity = AUDIO_TAG_LOW BREAK
		CASE LEVEL_3 	eMaxIntensity = AUDIO_TAG_MEDIUM 	eMinIntensity = AUDIO_TAG_LOW BREAK
		CASE LEVEL_4 	eMaxIntensity = AUDIO_TAG_HIGH 		eMinIntensity = AUDIO_TAG_MEDIUM BREAK
		DEFAULT 		eMaxIntensity = AUDIO_TAG_HIGH 		eMinIntensity = AUDIO_TAG_LOW BREAK
	ENDSWITCH
	IF eAnimIntensity > eMaxIntensity
		eAnimIntensity = eMaxIntensity
	ELIF eAnimIntensity < eMinIntensity
		eAnimIntensity = eMinIntensity
	ENDIF
		
	IF HAS_NET_TIMER_EXPIRED(ref_faceData.tFaceAnimStart, ref_faceData.iFaceAnimDurationMs)	
	OR eAnimIntensity <> ref_faceData.eAnimIntensity // Maybe we have changed intensity or the music has changed intensity.
	#IF IS_DEBUG_BUILD
	OR (ref_faceData.bForceNewRandAnim)
		ref_faceData.bForceNewRandAnim = FALSE
	#ENDIF
		
		// Grab a random clip for this intensity...
		STRING sClipName
		INT iRand
		SWITCH eAnimIntensity
			CASE AUDIO_TAG_NULL
			CASE AUDIO_TAG_LOW
				iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
				SWITCH iRand
					CASE 0 	sClipName = "mood_dancing_low_1" BREAK
					CASE 1	sClipName = "mood_dancing_low_2" BREAK
					CASE 2	sClipName = "mood_dancing_low_3" BREAK
				ENDSWITCH
			BREAK
			CASE AUDIO_TAG_MEDIUM
				iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
				SWITCH iRand
					CASE 0 	sClipName = "mood_dancing_medium_1" BREAK
					CASE 1	sClipName = "mood_dancing_medium_2" BREAK
					CASE 2	sClipName = "mood_dancing_medium_3" BREAK
				ENDSWITCH
			BREAK
			CASE AUDIO_TAG_HIGH
			CASE AUDIO_TAG_HIGH_HANDS
				iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
				SWITCH iRand
					CASE 0 	sClipName = "mood_dancing_high_1" BREAK
					CASE 1	sClipName = "mood_dancing_high_2" BREAK
				ENDSWITCH			
			BREAK
		ENDSWITCH
		
		// Allow debug override of the new clip (combo box)
		#IF IS_DEBUG_BUILD
		SWITCH ref_faceData.iRandomOverride
			CASE 0 /*NONE*/ 						BREAK
			CASE 1 sClipName = "mood_dancing_low_1" BREAK
			CASE 2 sClipName = "mood_dancing_low_2" BREAK
			CASE 3 sClipName = "mood_dancing_low_3" BREAK
			CASE 4 sClipName = "mood_dancing_medium_1" BREAK
			CASE 5 sClipName = "mood_dancing_medium_2" BREAK
			CASE 6 sClipName = "mood_dancing_medium_3" BREAK
			CASE 7 sClipName = "mood_dancing_high_1" BREAK
			CASE 8 sClipName = "mood_dancing_high_2" BREAK
		ENDSWITCH
		#ENDIF
		
		// Play facial anim
		SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), sClipName)
		// Update data - "facials@gen_male@base" is always loaded in because it's the directory for all default facial anims.
		REINIT_NET_TIMER(ref_faceData.tFaceAnimStart)
		ref_faceData.iFaceAnimDurationMs = FLOOR(GET_ANIM_DURATION("facials@gen_male@base", sClipName)*1000.0)
		ref_faceData.eAnimIntensity = eAnimIntensity
		
		PRINTLN("[PLAYER_DANCING] DANCE_MAINTAIN_FACIAL_ANIM - giving facial anim ", sClipName)
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads the dance minigame assets if in stage DLS_INIT and will return 
///    true when finished.
FUNC BOOL DANCE_LOAD_MINIGAME(PLAYER_DANCE_LOCAL_STRUCT& ref_local)	
	SWITCH ref_local.eStage
		CASE DLS_CLEANUP_COMPLETE	RETURN FALSE
		CASE DLS_INIT 				DANCE_INIT(ref_local)	RETURN FALSE
		CASE DLS_LOADING			DANCE_LOADING(ref_local)RETURN FALSE
		DEFAULT RETURN TRUE
	ENDSWITCH
ENDFUNC

/// PURPOSE:
///    Returns true if the dance minigame is ready to be used (assets are loaded)
FUNC BOOL DANCE_HAS_MINIGAME_LOADED(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	RETURN ref_local.eStage > DLS_LOADING 
		AND ref_local.eStage <> DLS_CLEANUP_COMPLETE
ENDFUNC

#IF DANCING_FEATURE_DUAL_DANCE
FUNC BOOL DANCING_ARE_BOTH_PLAYERS_READY_FOR_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	PLAYER_INDEX piDancePartner = DANCING_GET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local)
	RETURN DANCING_IS_PLAYER_READY_FOR_DUAL_DANCE(ref_local, piDancePartner)
		AND DANCING_IS_LOCAL_PLAYER_READY_FOR_DUAL_DANCE(ref_local)
ENDFUNC

PROC DANCING_STOP_CURRENT_SYNC_SCENE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	INT iSyncScene = DANCING_GET_LOCAL_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(ref_local)
	
	IF iSyncScene = -1
		EXIT
	ENDIF
	
	NETWORK_STOP_SYNCHRONISED_SCENE(iSyncScene)
	DANCING_SET_LOCAL_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(ref_local, -1)
	PRINTLN("[DANCING][DUAL] STOP_CURRENT_SYNC_SCENE - Stopped sync scene")
ENDPROC

FUNC BOOL DANCING_HAS_CURRENT_DUAL_DANCE_SYNC_SCENE_FINISHED(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	INT iSyncScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(DANCING_GET_LOCAL_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(ref_local))
	
	IF HAS_ANIM_EVENT_FIRED(ref_local.pedLocalPlayer, HASH("EXIT"))
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DUAL_DANCE_ANIM_STARTED)
		PRINTLN("[DANCING][DUAL] HAS_CURRENT_DUAL_DANCE_SYNC_SCENE_FINISHED - hit exit event")
		RETURN TRUE
	ENDIF
	
	// Sysnc scene over or lost
	IF iSyncScene = -1
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene)
		RETURN TRUE
	ENDIF
	
	// Make sure the scene has at least started before checking if it is no longer running
	IF GET_SYNCHRONIZED_SCENE_PHASE(iSyncScene) > 0.5
		SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DUAL_DANCE_ANIM_STARTED)
		PRINTLN("[DANCING][DUAL] HAS_CURRENT_DUAL_DANCE_SYNC_SCENE_FINISHED - anim over halfway complete")
	ENDIF
	
	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene) AND IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DUAL_DANCE_ANIM_STARTED)
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DUAL_DANCE_ANIM_STARTED)
		PRINTLN("[DANCING][DUAL] HAS_CURRENT_DUAL_DANCE_SYNC_SCENE_FINISHED - sync scene not running")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DANCING_DEBUG_APPLY_DUAL_DANCE_BLEND_OVERRIDES(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DUAL_DANCE_CLIP eClip, BOOL bPedA, FLOAT &fBlendIn, FLOAT &fBlendOut)
	INT iPed = BOOL_TO_INT(bPedA)
	
	IF eClip = DDC_INVALID
		EXIT
	ENDIF
	
	DANCING_ANIM_BLENDS eBlendIn = INT_TO_ENUM(DANCING_ANIM_BLENDS, ref_local.debug.iBlendInOverrides[ENUM_TO_INT(eClip)][iPed])
	DANCING_ANIM_BLENDS eBlendOut = INT_TO_ENUM(DANCING_ANIM_BLENDS, ref_local.debug.iBlendOutOverrides[ENUM_TO_INT(eClip)][iPed])
	
	STRING strPed = PICK_STRING(bPedA, "Ped A", "Ped B")
	
	IF eBlendIn != DAB_NONE
		fBlendIn = DANCING_GET_BLEND_IN_DURATION_VALUE(eBlendIn)
		PRINTLN("[DANCING][DUAL] DEBUG_APPLY_DUAL_DANCE_BLEND_OVERRIDES - Overriding blend in for ", strPed, ": ",  fBlendIn)
	ENDIF
	
	IF eBlendOut != DAB_NONE
		fBlendOut = DANCING_GET_BLEND_OUT_DURATION_VALUE(eBlendOut)
		PRINTLN("[DANCING][DUAL] DEBUG_APPLY_DUAL_DANCE_BLEND_OVERRIDES - Overriding blend out for ", strPed, ": ",  fBlendOut)
	ENDIF
ENDPROC
#ENDIF

PROC DANCING_START_DUAL_DANCE_CLIP(PLAYER_DANCE_LOCAL_STRUCT& ref_local, DUAL_DANCE_CLIP ePedAClip, DUAL_DANCE_CLIP ePedBClip, BOOL bLooped, BOOL bHoldLastFrame = FALSE)
	
	PLAYER_INDEX piLead = DANCING_GET_DUAL_DANCE_LEAD_PLAYER(ref_local)
	PLAYER_INDEX piDancePartner = DANCING_GET_DUAL_DANCE_NON_LEAD_PLAYER(ref_local)
	PED_INDEX pedA = GET_PLAYER_PED(piDancePartner) // Non lead is already dancing in a fixed position so we use them as ped A (scene origin)
	PED_INDEX pedB = GET_PLAYER_PED(piLead) // Lead initiates so is ped B
	
	#IF IS_DEBUG_BUILD
	IF ref_local.bUsingDanceBot
		PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_CLIP - Using dance bot: ", NATIVE_TO_INT(ref_local.debug.pedDanceBot))
		pedA = ref_local.debug.pedDanceBot
	ENDIF
	#ENDIF
	
	INT iSelectedDance = DANCING_GET_LEADS_SELECTED_DUAL_DANCE(ref_local)
	DUAL_DANCE_ANIM sAnim = DANCING_GET_DUAL_DANCE_ANIM(ref_local, iSelectedDance)
	STRING strDict = sAnim.strDict
	
	IF IS_STRING_NULL_OR_EMPTY(strDict)
		PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_CLIP - Dual dance dict null at index: ", iSelectedDance)
		ASSERTLN("[DANCING][DUAL] START_DUAL_DANCE_CLIP - Dual dance dict null at index: ", iSelectedDance)	
		EXIT
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(strDict)
		PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_CLIP - Dual dance dict not loaded at index: ", iSelectedDance)
		ASSERTLN("[DANCING][DUAL] START_DUAL_DANCE_CLIP - Dual dance dict not loaded at index: ", iSelectedDance)	
		EXIT
	ENDIF
	
	DUAL_DANCE_CLIP_DATA sPedAClip = sAnim.sPedAClips[ePedAClip]
	
	IF IS_STRING_NULL_OR_EMPTY(sPedAClip.strClip)
		PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_CLIP - Ped A clip null at index: ", iSelectedDance)
		ASSERTLN("[DANCING][DUAL] START_DUAL_DANCE_CLIP - Ped A clip null at index: ", iSelectedDance)	
		EXIT
	ENDIF
	
	DUAL_DANCE_CLIP_DATA sPedBClip = sAnim.sPedBClips[ePedBClip]
			
	IF IS_STRING_NULL_OR_EMPTY(sPedBClip.strClip)
		PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_CLIP - Ped B clip null at index: ", iSelectedDance)
		ASSERTLN("[DANCING][DUAL] START_DUAL_DANCE_CLIP - Ped B clip null at index: ", iSelectedDance)	
		EXIT
	ENDIF
	
	TRANSFORM_STRUCT sSceneTransform = DANCING_GET_PLAYERS_SYNC_SCENE_TRANSFORM(ref_local, ref_local.piLocalPlayer)
	VECTOR vSyncScenePos = sSceneTransform.Position
	VECTOR vSyncSceneRot = sSceneTransform.Rotation
	PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_CLIP - Scene Pos: ", vSyncScenePos, " Scene rot: ", vSyncSceneRot)
	
	FLOAT fStartPhase = DANCING_GET_ANIM_ENTER_EVENT_PHASE(strDict, sPedAClip.strClip)
	FLOAT fEndPhase = DANCING_GET_ANIM_EXIT_EVENT_PHASE(strDict, sPedAClip.strClip)
	
	FLOAT fPedABlendIn = sPedAClip.fBlendIn
	FLOAT fPedABlendOut = sPedAClip.fBlendOut
	
	FLOAT fPedBBlendIn = sPedBClip.fBlendIn
	FLOAT fPedBBlendOut = sPedBClip.fBlendOut
	
	#IF IS_DEBUG_BUILD
		DANCING_DEBUG_APPLY_DUAL_DANCE_BLEND_OVERRIDES(ref_local, ePedAClip, TRUE, fPedABlendIn, fPedABlendOut)
		DANCING_DEBUG_APPLY_DUAL_DANCE_BLEND_OVERRIDES(ref_local, ePedBClip, FALSE, fPedBBlendIn, fPedBBlendOut)
	#ENDIF
	
	INT iSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vSyncScenePos, vSyncSceneRot, EULER_YXZ, bHoldLastFrame, bLooped, fEndPhase, fStartPhase)
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedA, iSyncSceneID, strDict, sPedAClip.strClip, fPedABlendIn, fPedABlendOut, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedB, iSyncSceneID, strDict, sPedBClip.strClip, fPedBBlendIn, fPedBBlendOut, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
	NETWORK_START_SYNCHRONISED_SCENE(iSyncSceneID)
	
	TEXT_LABEL_63 faceClip = sPedAClip.strClip
	faceClip += "_facial"
	PLAY_FACIAL_ANIM(ref_local.pedLocalPlayer, faceClip, sAnim.strDict)
	
	DANCING_SET_LOCAL_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(ref_local, iSyncSceneID)
	PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_CLIP - Started dual dance sync scene ID: ", iSyncSceneID, " dict: ", strDict, " clipd: ", sPedBClip.strClip, " faceClip: ", faceClip)
ENDPROC

PROC DANCING_START_DUAL_DANCE_INTRO_SYNC_SCENE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_INTRO_SYNC_SCENE - Started dual dance sync scene")
	DANCING_START_DUAL_DANCE_CLIP(ref_local, DDC_DUAL_DANCE_INTRO, DDC_DUAL_DANCE_INTRO, FALSE, TRUE)
ENDPROC

PROC DANCING_START_DUAL_DANCE_IDLE_SYNC_SCENE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_IDLE_SYNC_SCENE - Started dual dance sync scene")
	DANCING_START_DUAL_DANCE_CLIP(ref_local, DDC_DUAL_DANCE_IDLE, DDC_DUAL_DANCE_IDLE, TRUE)
ENDPROC

PROC DANCING_START_DUAL_DANCE_EXIT_TASK_ANIM(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bInitiatedExit, PED_INDEX piTargetPed)
	INT iSelectedDance = DANCING_GET_LEADS_SELECTED_DUAL_DANCE(ref_local)
	DUAL_DANCE_ANIM sAnim = DANCING_GET_DUAL_DANCE_ANIM(ref_local, iSelectedDance)
	STRING strDict = sAnim.strDict
	
	IF IS_STRING_NULL_OR_EMPTY(strDict)
		PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_EXIT_TASK_ANIM - Dual dance dict null at index: ", iSelectedDance)
		ASSERTLN("[DANCING][DUAL] START_DUAL_DANCE_EXIT_TASK_ANIM - Dual dance dict null at index: ", iSelectedDance)	
		EXIT
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(strDict)
		PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_EXIT_TASK_ANIM - Dual dance dict not loaded at index: ", iSelectedDance)
		ASSERTLN("[DANCING][DUAL] START_DUAL_DANCE_EXIT_TASK_ANIM - Dual dance dict not loaded at index: ", iSelectedDance)	
		EXIT
	ENDIF
	
	DUAL_DANCE_CLIP_DATA sClip
	DUAL_DANCE_CLIP eDanceClip
	
	// If we started the exit we go to standing
	IF bInitiatedExit
		eDanceClip = DDC_DUAL_DANCE_STANDING_EXIT
	ELSE
		eDanceClip = DDC_DUAL_DANCE_EXIT
	ENDIF
	
	BOOL bIsPlayerPedA = NOT DANCING_IS_LOCAL_PLAYER_DUAL_DANCE_LEAD(ref_local) 
	
	#IF IS_DEBUG_BUILD 
	IF piTargetPed = ref_local.debug.pedDanceBot
		bIsPlayerPedA = TRUE
	ENDIF
	#ENDIF
	
	// If we are the lead we need to use ped Bs variation
	IF bIsPlayerPedA
		sClip = sAnim.sPedAClips[eDanceClip]
	ELSE
		sClip = sAnim.sPedBClips[eDanceClip]
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(sClip.strClip)
		PRINTLN("[DANCING][DUAL] START_DUAL_DANCE_EXIT_TASK_ANIM - Dual dance clip null at index: ", iSelectedDance)
		ASSERTLN("[DANCING][DUAL] START_DUAL_DANCE_EXIT_TASK_ANIM - Dual dance clip null at index: ", iSelectedDance)	
		EXIT
	ENDIF
	
	FLOAT fBlendIn = sClip.fBlendIn
	FLOAT fBlendOut = sClip.fBlendOut
	
	#IF IS_DEBUG_BUILD
		DANCING_DEBUG_APPLY_DUAL_DANCE_BLEND_OVERRIDES(ref_local, eDanceClip, bIsPlayerPedA, fBlendIn, fBlendOut)
	#ENDIF
	
	FLOAT fStartPhase = DANCING_GET_ANIM_ENTER_EVENT_PHASE(strDict, sClip.strClip)
	
	TRANSFORM_STRUCT sSceneTransform = DANCING_GET_PLAYERS_SYNC_SCENE_TRANSFORM(ref_local, ref_local.piLocalPlayer)
	VECTOR vSyncScenePos = sSceneTransform.Position
	VECTOR vSyncSceneRot = sSceneTransform.Rotation
	
	INT iSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vSyncScenePos, vSyncSceneRot, EULER_YXZ, FALSE, FALSE, DEFAULT, fStartPhase)
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(piTargetPed, iSyncSceneID, strDict, sClip.strClip, fBlendIn, fBlendOut, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
	NETWORK_START_SYNCHRONISED_SCENE(iSyncSceneID)
	DANCING_SET_LOCAL_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(ref_local, iSyncSceneID)
ENDPROC

PROC DANCING_COPY_PARTNERS_DUAL_DANCE_SYNC_SCENE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	INT iSyncScene = DANCING_GET_DUAL_DANCE_LEADS_SYNC_SCENE_ID(ref_local)
	DANCING_SET_LOCAL_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(ref_local, iSyncScene)
	PRINTLN("[DANCING][DUAL] COPY_PARTNERS_DUAL_DANCE_SYNC_SCENE - Copied: ", iSyncScene)
ENDPROC

PROC DANCING_MAINTAIN_DUAL_DANCE_BACKGROUND_SYSTEMS(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	DANCE_SUPPRESS_MOVEMENT()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT_SECONDARY)
	SET_PED_RESET_FLAG(ref_local.pedLocalPlayer, PRF_UseKinematicPhysics, TRUE)
	DANCE_SUPPRESS_HUD(ref_local)	
	MAINTAIN_DANCE_FULL_MIN_REWARDS(ref_local)
	DANCING_MAINTAIN_GIVE_DUAL_DANCE_DURATION_RP_REWARDS(ref_local)
	
	//B* 5116948
	#IF USE_REPLAY_RECORDING_TRIGGERS
	DISABLE_REPLAY_RECORDING_UI_THIS_FRAME()
	#ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
FUNC BOOL DANCING_CREATE_DEBUG_DANCE_BOT(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	TRANSFORM_STRUCT sStartTransform = DANCING_GET_PLAYERS_SYNC_SCENE_TRANSFORM(ref_local, ref_local.piLocalPlayer)
	
	IF DOES_ENTITY_EXIST(ref_local.debug.pedDanceBot)
		SET_ENTITY_COORDS_NO_OFFSET(ref_local.debug.pedDanceBot, sStartTransform.Position)
		SET_ENTITY_ROTATION(ref_local.debug.pedDanceBot, sStartTransform.Rotation)
		RETURN TRUE
	ENDIF

	MODEL_NAMES eDanceBotModel = MP_M_FREEMODE_01
	
	REQUEST_MODEL(eDanceBotModel)
	IF NOT HAS_MODEL_LOADED(eDanceBotModel)
		RETURN FALSE
	ENDIF
	
	// Sync scene is actually backwards so correct by 180
	sStartTransform.Rotation.z = sStartTransform.Rotation.z + 180
	sStartTransform.Rotation.z = (((sStartTransform.Rotation.z % 360.0) + 360.0) % 360.0)
	
	ref_local.debug.pedDanceBot = CREATE_PED(PEDTYPE_MISSION, eDanceBotModel, sStartTransform.Position, sStartTransform.Rotation.z)
	SET_ENTITY_INVINCIBLE(ref_local.debug.pedDanceBot, TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(eDanceBotModel)
	RETURN TRUE
ENDFUNC
#ENDIF

FUNC BOOL DANCING_IS_PLAYER_OK_TO_DUAL_DANCE(PLAYER_INDEX piPlayer)
	PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)	
	
	IF NOT IS_ENTITY_ALIVE(pedPlayer)
		PRINTLN("[DANCING][DUAL] IS_PLAYER_OK_TO_DUAL_DANCE - ", GET_PLAYER_NAME(piPlayer), " not alive")
		RETURN FALSE
	ENDIF	
		
	IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayer)
		PRINTLN("[DANCING][DUAL] IS_PLAYER_OK_TO_DUAL_DANCE - ", GET_PLAYER_NAME(piPlayer), " not a participant in this script")
		RETURN FALSE
	ENDIF
	
	PARTICIPANT_INDEX parPlayer = NETWORK_GET_PARTICIPANT_INDEX(piPlayer)
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(parPlayer)
		PRINTLN("[DANCING][DUAL] IS_PLAYER_OK_TO_DUAL_DANCE - ", GET_PLAYER_NAME(piPlayer), " not an active participant")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CUTSCENE(piPlayer)
		PRINTLN("[DANCING][DUAL] IS_PLAYER_OK_TO_DUAL_DANCE - ", GET_PLAYER_NAME(piPlayer), " is in a cutscene")
		RETURN FALSE
	ENDIF
	
	PED_INDEX pedLocal = GET_PLAYER_PED(piPlayer)
	
	IF IS_ENTITY_IN_WATER(pedLocal) OR IS_PED_SWIMMING(pedLocal)
		PRINTLN("[DANCING][DUAL] IS_PLAYER_OK_TO_DUAL_DANCE - Player is in water")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_AIR(pedLocal) 
		PRINTLN("[DANCING][DUAL] IS_PLAYER_OK_TO_DUAL_DANCE - Player is in air")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_ON_FIRE(pedLocal)
		PRINTLN("[DANCING] IS_PLAYER_OK_TO_DUAL_DANCE - Player is on fire")
		RETURN TRUE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the local players dance partner is actually dancing with them 
FUNC BOOL DANCING_IS_LOCAL_PLAYERS_DANCE_PARTNER_DANCING_WITH_THE_LOCAL_PLAYER(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	PLAYER_INDEX piLocalPlayersDancePartner = DANCING_GET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local)
	PLAYER_INDEX piRemotePlayersDancePartner = DANCING_GET_PLAYERS_DANCE_PARTNER(ref_local, piLocalPlayersDancePartner)
	RETURN piRemotePlayersDancePartner = ref_local.piLocalPlayer
ENDFUNC

FUNC BOOL DANCING_SHOULD_BAIL_FROM_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bIgnoreBailTimer = FALSE, INT iBailTimerDuration = 15000)
	PLAYER_INDEX piDancePartner = DANCING_GET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local)
	
	IF NOT DANCING_IS_PLAYER_OK_TO_DUAL_DANCE(piDancePartner)
		PRINTLN("[DANCING][DUAL] SHOULD_BAIL_FROM_DUAL_DANCE - Dance partner not ok to dual dance")
		RETURN TRUE
	ENDIF
	
	IF NOT DANCING_IS_PLAYER_OK_TO_DUAL_DANCE(ref_local.piLocalPlayer)
		PRINTLN("[DANCING][DUAL] SHOULD_BAIL_FROM_DUAL_DANCE - Local player not ok to dual dance")
		RETURN TRUE
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("spawn_activities")) > 0 OR g_TransitionSpawnData.bSpawnActivityReady
		PRINTLN("[DANCING][DUAL] SHOULD_BAIL_FROM_DUAL_DANCE - Local player wakeing up drunk")
		RETURN TRUE
	ENDIF
	
	IF DANCING_ARE_BOTH_PLAYERS_READY_FOR_DUAL_DANCE(ref_local)
	AND NOT DANCING_IS_LOCAL_PLAYERS_DANCE_PARTNER_DANCING_WITH_THE_LOCAL_PLAYER(ref_local)
		PRINTLN("[DANCING][DUAL] SHOULD_BAIL_FROM_DUAL_DANCE - Local players dance partner is dancing with someone else :(")
		RETURN TRUE
	ENDIF
	
	IF NOT bIgnoreBailTimer
		IF HAS_NET_TIMER_EXPIRED(ref_local.sStateBailTimer, iBailTimerDuration)
			RESET_NET_TIMER(ref_local.sStateBailTimer)
			PRINTLN("[DANCING][DUAL] SHOULD_BAIL_FROM_DUAL_DANCE - Exceeded Bail Timer")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_PLAYER_IS_BUSY)
		PRINTLN("[DANCING] SHOULD_BAIL_FROM_DUAL_DANCE - Player is busy")
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC DANCING_UPDATE_LEAD_WAIT_FOR_DUAL_DANCE_START(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	#IF IS_DEBUG_BUILD
	// If dancing with real player wait for them to accept the dance
	IF NOT ref_local.bUsingDanceBot
	#ENDIF	
		// If both players ready for dual dance start the anim and switch state
		IF NOT DANCING_ARE_BOTH_PLAYERS_READY_FOR_DUAL_DANCE(ref_local)
			EXIT
		ENDIF
		
		IF DANCING_SHOULD_BAIL_FROM_DUAL_DANCE(ref_local)
			PRINTLN("[DANCING][DUAL][LEAD] UPDATE_WAIT_FOR_DUAL_DANCE_START - Need to bail")
			DANCE_SET_STAGE(ref_local, DLS_IDLE_NOT_DANCING)
			DANCING_CLEANUP_DUAL_DANCING(ref_local)
			EXIT
		ENDIF	
		
	#IF IS_DEBUG_BUILD
	ELSE
		// Dance bot is born to dance
		IF NOT DANCING_CREATE_DEBUG_DANCE_BOT(ref_local)
			EXIT
		ENDIF
	ENDIF
	#ENDIF
	
//	DRAW_DEBUG_CROSS(ref_local.vWalkToTarget, 1.0)
	
	IF GET_SCRIPT_TASK_STATUS(ref_local.pedLocalPlayer, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK	
		EXIT
	ENDIF
	
	PRINTLN("[DANCING][DUAL][LEAD] UPDATE_WAIT_FOR_DUAL_DANCE_START - Progressing to intro anim state")
	DANCING_START_DUAL_DANCE_INTRO_SYNC_SCENE(ref_local)
	SHAKE_GAMEPLAY_CAM("CLUB_DANCE_SHAKE", cfSHAKE_AMP_MIN)	
	
	RESET_NET_TIMER(ref_local.tlDanceTimer)
	START_NET_TIMER(ref_local.tlDanceTimer)
	RESET_NET_TIMER(ref_local.tRPDurationMinute)
	START_NET_TIMER(ref_local.tRPDurationMinute)
	DANCE_SET_STAGE(ref_local, DLS_DUAL_DANCE_INTRO)
ENDPROC

PROC DANCING_UPDATE_NON_LEAD_WAIT_FOR_DUAL_DANCE_START(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	// Wait for dance lead to start intro anim before continuing
	IF DANCING_GET_DANCE_PARTNERS_DANCE_STAGE(ref_local) >= DLS_DUAL_DANCE_INTRO
		PRINTLN("[DANCING][DUAL] UPDATE_WAIT_FOR_DUAL_DANCE_START - Following to intro anim state")
		DANCING_COPY_PARTNERS_DUAL_DANCE_SYNC_SCENE(ref_local)
		DANCE_SET_STAGE(ref_local, DLS_DUAL_DANCE_INTRO)
	ENDIF
	
	IF DANCING_SHOULD_BAIL_FROM_DUAL_DANCE(ref_local)
		PRINTLN("[DANCING][DUAL] UPDATE_WAIT_FOR_DUAL_DANCE_START - Need to bail to dancing")
		DANCING_CLEANUP_DUAL_DANCING(ref_local)
		TRIGGER_DANCING_INTRO(ref_local)
		ENABLE_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA, IS_PLAYER_IN_ANY_DANCE_AREA(ref_local, ref_local.iDanceAreaIndex))
	ENDIF	
ENDPROC

PROC DANCING_UPDATE_WAIT_FOR_DUAL_DANCE_START(PLAYER_DANCE_LOCAL_STRUCT& ref_local)	
	DANCING_MAINTAIN_DUAL_DANCE_BACKGROUND_SYSTEMS(ref_local)
	SET_FOLLOW_PED_CAM_THIS_UPDATE("NIGHTCLUB_FOLLOW_PED_CAMERA", ciDANCE_CAMERA_INTERPOLATION)
	
	
	// Need to resolve lead conflicts
	IF DANCING_ARE_BOTH_DUAL_DANCERS_LEADS(ref_Local)
		PRINTLN("[DANCING][DUAL] UPDATE_WAIT_FOR_DUAL_DANCE_START - Have two leads!")
		PLAYER_INDEX piPartner = DANCING_GET_LOCAL_PLAYERS_DANCE_PARTNER(ref_local)
		
		IF ref_local.iLocalPlayer < NATIVE_TO_INT(piPartner)
			DANCING_JOIN_DUAL_DANCE_WITH_PLAYER(ref_Local, piPartner, FALSE)
			PRINTLN("[DANCING][DUAL] UPDATE_WAIT_FOR_DUAL_DANCE_START - Rejoining as non lead!")
		ENDIF
		EXIT
	ENDIF
	
	IF DANCING_IS_LOCAL_PLAYER_DUAL_DANCE_LEAD(ref_local)
		DANCING_UPDATE_LEAD_WAIT_FOR_DUAL_DANCE_START(ref_local)
	ELSE
		DANCE_MAINTAIN_SHAKE_LERP(ref_local)
		DANCING_UPDATE_NON_LEAD_WAIT_FOR_DUAL_DANCE_START(ref_local)
	ENDIF	
ENDPROC

PROC DANCING_UPDATE_DUAL_DANCE_INTRO(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	DANCING_MAINTAIN_DUAL_DANCE_BACKGROUND_SYSTEMS(ref_local)
	SET_FOLLOW_PED_CAM_THIS_UPDATE("NIGHTCLUB_FOLLOW_PED_CAMERA", ciDANCE_CAMERA_INTERPOLATION)
	DANCE_MAINTAIN_SHAKE_LERP(ref_local)
	
	IF DANCING_IS_LOCAL_PLAYER_DUAL_DANCE_LEAD(ref_local)
		IF DANCING_HAS_CURRENT_DUAL_DANCE_SYNC_SCENE_FINISHED(ref_local)
			PRINTLN("[DANCING][DUAL][LEAD] UPDATE_DUAL_DANCE_INTRO - Progressing to idle anim state")		
			DANCING_START_DUAL_DANCE_IDLE_SYNC_SCENE(ref_local)
			DANCE_SET_STAGE(ref_local, DLS_DUAL_DANCING)
		ENDIF
		
		IF DANCING_SHOULD_BAIL_FROM_DUAL_DANCE(ref_local)
			PRINTLN("[DANCING][DUAL][LEAD] UPDATE_DUAL_DANCE_INTRO - Need to bail")
			DANCE_SET_STAGE(ref_local, DLS_IDLE_NOT_DANCING)
			DANCING_CLEANUP_DUAL_DANCING(ref_local)
			EXIT
		ENDIF	
		
	ELSE
		IF DANCING_GET_DANCE_PARTNERS_DANCE_STAGE(ref_local) >= DLS_DUAL_DANCING
			PRINTLN("[DANCING][DUAL] UPDATE_DUAL_DANCE_INTRO - Following to idle anim state")
			DANCING_COPY_PARTNERS_DUAL_DANCE_SYNC_SCENE(ref_local)
			DANCE_SET_STAGE(ref_local, DLS_DUAL_DANCING)
		ENDIF
		
		IF DANCING_SHOULD_BAIL_FROM_DUAL_DANCE(ref_local)
			PRINTLN("[DANCING][DUAL] UPDATE_DUAL_DANCE_INTRO - Need to bail to dancing")
			DANCING_CLEANUP_DUAL_DANCING(ref_local)
			TRIGGER_DANCING_INTRO(ref_local)
			ENABLE_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA, IS_PLAYER_IN_ANY_DANCE_AREA(ref_local, ref_local.iDanceAreaIndex))
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL DANCING_HAS_PLAYER_TRIGGERED_STOP_DUAL_DANCING(PLAYER_DANCE_LOCAL_STRUCT& ref_local) 
	DANCE_CONTEXT_INTENTION eHelpType = DCI_DUAL_CONTROLS
	RETURN HAS_CONTEXT_INTENTION_TRIGGERED(ref_local, eHelpType)
ENDFUNC

PROC DANCING_UPDATE_DUAL_DANCE(PLAYER_DANCE_LOCAL_STRUCT& ref_local)	
	DANCING_MAINTAIN_DUAL_DANCE_BACKGROUND_SYSTEMS(ref_local)
	SET_FOLLOW_PED_CAM_THIS_UPDATE("NIGHTCLUB_FOLLOW_PED_CAMERA", ciDANCE_CAMERA_INTERPOLATION)
	DANCE_MAINTAIN_SHAKE_LERP(ref_local)
	
	#IF IS_DEBUG_BUILD
	IF NOT ref_local.bUsingDanceBot
	#ENDIf
	
		// Either player can choose to end the dance so progress if either have exited
		IF DANCING_GET_DANCE_PARTNERS_DANCE_STAGE(ref_local) = DLS_DUAL_DANCE_EXIT
		OR DANCING_GET_DANCE_PARTNERS_DANCE_STAGE(ref_local) = DLS_IDLE_NOT_DANCING
		OR DANCING_GET_DANCE_PARTNERS_DANCE_STAGE(ref_local) = DLS_DANCING
		OR DANCING_GET_DANCE_PARTNERS_DANCE_STAGE(ref_local) = DLS_DUAL_DANCE_WAIT_FOR_LOOP_END
			PRINTLN("[DANCING][DUAL] UPDATE_DUAL_DANCE - Following to exit anim state")	
			DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
			DANCING_START_DUAL_DANCE_EXIT_TASK_ANIM(ref_local, FALSE, ref_local.pedLocalPlayer)
			DANCE_SET_STAGE(ref_local, DLS_DUAL_DANCE_EXIT)
			EXIT
		ENDIF
		
		IF DANCING_SHOULD_BAIL_FROM_DUAL_DANCE(ref_local, TRUE)
			PRINTLN("[DANCING][DUAL] UPDATE_DUAL_DANCE - Need to bail to dancing")
			DANCING_CLEANUP_DUAL_DANCING(ref_local)
			TRIGGER_DANCING_INTRO(ref_local)
			ENABLE_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA, IS_PLAYER_IN_ANY_DANCE_AREA(ref_local, ref_local.iDanceAreaIndex))
		ENDIF	
	
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	IF DANCING_HAS_PLAYER_TRIGGERED_STOP_DUAL_DANCING(ref_local)
		PRINTLN("[DANCING][DUAL] UPDATE_DUAL_DANCE - Stopped dance, progressing to exit anim state")
		DANCE_RELEASE_CONTEXT_INTENTION(ref_local)
		SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_TRIGGERED_DUAL_DANCE_END)
		DANCE_SET_STAGE(ref_local, DLS_DUAL_DANCE_WAIT_FOR_LOOP_END)
	ENDIF	
ENDPROC

PROC DANCING_UPDATE_WAIT_FOR_LOOP_END(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	DANCING_MAINTAIN_DUAL_DANCE_BACKGROUND_SYSTEMS(ref_local)
	SET_FOLLOW_PED_CAM_THIS_UPDATE("NIGHTCLUB_FOLLOW_PED_CAMERA", ciDANCE_CAMERA_INTERPOLATION)
	DANCE_MAINTAIN_SHAKE_LERP(ref_local)
	
	IF DANCING_SHOULD_BAIL_FROM_DUAL_DANCE(ref_local)
		PRINTLN("[DANCING][DUAL] UPDATE_DUAL_DANCE - Need to bail to exit")
		DANCING_START_DUAL_DANCE_EXIT_TASK_ANIM(ref_local, TRUE, ref_local.pedLocalPlayer)
		DANCE_SET_STAGE(ref_local, DLS_DUAL_DANCE_EXIT)
		EXIT
	ENDIF	
	
	// Will wait until current loops exit event has triggered or the loop hits end phase
	IF NOT DANCING_HAS_CURRENT_DUAL_DANCE_SYNC_SCENE_FINISHED(ref_local)
		#IF IS_DEBUG_BUILD
		IF ref_local.bUsingDanceBot
			EXIT
		ENDIF
		#ENDIf
		
		IF DANCING_GET_DANCE_PARTNERS_DANCE_STAGE(ref_local) = DLS_DUAL_DANCE_WAIT_FOR_LOOP_END
			EXIT
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF ref_local.bUsingDanceBot
		DANCING_START_DUAL_DANCE_EXIT_TASK_ANIM(ref_local, FALSE, ref_Local.debug.pedDanceBot)
	ENDIF
	#ENDIF
	
	// At this point each player does thier own sync scene so they can exit at different times
	DANCING_START_DUAL_DANCE_EXIT_TASK_ANIM(ref_local, TRUE, ref_local.pedLocalPlayer)
	DANCE_SET_STAGE(ref_local, DLS_DUAL_DANCE_EXIT)
ENDPROC

FUNC BOOL DANCING_HAS_PLAYER_TRIGGERED_EARLY_ANIM_BREAKOUT()
	VECTOR vLeftInput
	vLeftInput = <<GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y), 0.0>>	
	RETURN HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BREAK_OUT")) AND (VMAG(vLeftInput) >= 0.24)
ENDFUNC

FUNC BOOL DANCING_HAS_EXIT_ANIM_FINISHED(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_TRIGGERED_DUAL_DANCE_END)
	AND DANCING_HAS_PLAYER_TRIGGERED_EARLY_ANIM_BREAKOUT()
		RETURN TRUE
	ENDIF
	
	RETURN DANCING_HAS_CURRENT_DUAL_DANCE_SYNC_SCENE_FINISHED(ref_local)
ENDFUNC

PROC DANCING_UPDATE_DUAL_DANCE_EXIT(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	DANCING_MAINTAIN_DUAL_DANCE_BACKGROUND_SYSTEMS(ref_local)
	DANCE_MAINTAIN_SHAKE_LERP(ref_local)
		
	IF DANCING_SHOULD_BAIL_FROM_DUAL_DANCE(ref_local)
		PRINTLN("[DANCING][DUAL] UPDATE_DUAL_DANCE_EXIT - Need to bail")
		DANCE_SET_STAGE(ref_local, DLS_IDLE_NOT_DANCING)
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_TRIGGERED_DUAL_DANCE_END)
		DANCING_CLEANUP_DUAL_DANCING(ref_local)
		EXIT
	ENDIF	
	
	// Wait for exit anim end
	IF NOT DANCING_HAS_EXIT_ANIM_FINISHED(ref_local)
		DANCING_MAINTAIN_DUAL_DANCE_BACKGROUND_SYSTEMS(ref_local)
		EXIT	
	ENDIF
		
	PRINTLN("[DANCING][DUAL] UPDATE_DUAL_DANCE_EXIT - Exit anim finished")
	// Make sure scene ends in case of early break out
	DANCING_STOP_CURRENT_SYNC_SCENE(ref_local)
	
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_TRIGGERED_DUAL_DANCE_END)
	AND NOT (ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD AND ref_local.bDancingLocked) // should always return to dancing if locked on in the new control schemes
		// Go to idle
		DANCE_SET_STAGE(ref_local, DLS_IDLE_NOT_DANCING)
		CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_TRIGGERED_DUAL_DANCE_END)
		DANCING_CLEANUP_DUAL_DANCING(ref_local)
		PRINTLN("[DANCING][DUAL] UPDATE_DUAL_DANCE_EXIT - Progressing to idle")
	ELSE
		// Go to solo dance
		DANCING_CLEANUP_DUAL_DANCING(ref_local) // Must be done before triggering solo dance
		TRIGGER_DANCING_INTRO(ref_local)
		ENABLE_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_IN_DANCE_AREA, IS_PLAYER_IN_ANY_DANCE_AREA(ref_local, ref_local.iDanceAreaIndex))
		PRINTLN("[DANCING][DUAL] UPDATE_DUAL_DANCE_EXIT - Progressing to solo dance")
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC DANCING_DRAW_DEBUG_INFO(PLAYER_DANCE_LOCAL_STRUCT& ref_local)
	IF NOT ref_local.debug.bDrawBroadcastData
		EXIT
	ENDIF
	
	INT iParticipant
	INT iMaxParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
	PLAYER_INDEX piRemotePlayer
	PARTICIPANT_INDEX piParticipantIndex
	TEXT_LABEL_63 tlBroadcastData
	
	VECTOR vTextStartPos = <<0.1, 0.1, 0.0>>
	VECTOR vTextOrigin = vTextStartPos
	VECTOR vTextOffset = <<0.0, 0.025, 0.0>>
	FLOAT fPlayerXOffset = 0.3
	
	REPEAT iMaxParticipant iParticipant	
		piParticipantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)	
		
		// Ignore inactive
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piParticipantIndex)
			RELOOP
		ENDIF
		
		piRemotePlayer = NETWORK_GET_PLAYER_INDEX(piParticipantIndex)
				
		// Only include valid players
		IF NOT IS_NET_PLAYER_OK(piRemotePlayer)
			RELOOP
		ENDIF
		
		vTextOrigin = vTextStartPos
		vTextOrigin += <<iParticipant * fPlayerXOffset, 0.0, 0.0>>
		
		tlBroadcastData = "Player: "
		tlBroadcastData += GET_PLAYER_NAME(piRemotePlayer)
		DRAW_DEBUG_TEXT_2D(tlBroadcastData, vTextOrigin)
		
		vTextOrigin += vTextOffset
		
		tlBroadcastData = "bLeadDancer: "
		tlBroadcastData += GET_BOOL_DISPLAY_STRING_FROM_BOOL(DANCING_IS_PLAYER_DUAL_DANCE_LEAD(ref_local, piRemotePlayer))
		DRAW_DEBUG_TEXT_2D(tlBroadcastData, vTextOrigin)
		
		vTextOrigin += vTextOffset
		
		tlBroadcastData = "bReadyToDance: "
		tlBroadcastData += GET_BOOL_DISPLAY_STRING_FROM_BOOL(DANCING_IS_PLAYER_READY_FOR_DUAL_DANCE(ref_local, piRemotePlayer))
		DRAW_DEBUG_TEXT_2D(tlBroadcastData, vTextOrigin)
		
		vTextOrigin += vTextOffset
		
		tlBroadcastData = "piPartner: "
		tlBroadcastData += GET_PLAYER_NAME(DANCING_GET_PLAYERS_DANCE_PARTNER(ref_local, piRemotePlayer))
		DRAW_DEBUG_TEXT_2D(tlBroadcastData, vTextOrigin)
		
		vTextOrigin += vTextOffset
		
		tlBroadcastData = "iSyncSceneID: "
		tlBroadcastData += DANCING_GET_PLAYERS_DUAL_DANCE_SYNC_SCENE_ID(ref_local, piRemotePlayer)
		DRAW_DEBUG_TEXT_2D(tlBroadcastData, vTextOrigin)
		
		vTextOrigin += vTextOffset
		
		tlBroadcastData = "eStage: "
		tlBroadcastData += GET_DANCE_LOCAL_STAGE_STRING(DANCING_GET_PLAYERS_DANCE_STAGE(ref_local, piRemotePlayer))
		DRAW_DEBUG_TEXT_2D(tlBroadcastData, vTextOrigin)
		
		
	ENDREPEAT
ENDPROC
#ENDIF
#ENDIF // DANCING_FEATURE_DUAL_DANCE



/// PURPOSE:
///    To be called from script using this header every frame after calling INIT_PLAYER_DANCE(e.g. am_mp_nightclub.sc)
PROC DANCE_MAINTAIN_UPDATE(PLAYER_DANCE_LOCAL_STRUCT& ref_local, BOOL bIsPlayerBusy, NIGHTCLUB_AUDIO_TAGS eAudioIntensity)
	_DANCE_MAINTAINED_CACHED_VALUES(ref_local)
	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
		ref_local.eControlScheme = GET_NEXT_CONTROL_SCHEME(ref_local)
		
		PRINT_HELP(GET_CONTROL_SCHEME_HELP(ref_local.eControlScheme))
	ENDIF
	#ENDIF
	
	IF ref_local.eControlScheme != DANCE_CONTROL_SCHEME_STANDARD
		ref_local.bAltIntensities = TRUE
	ELSE
		ref_local.bAltIntensities = FALSE
	ENDIF
	
	IF ref_local.eStage = DLS_CLEANUP_COMPLETE
		PRINTLN("[DANCING] DANCE_MAINTAIN_UPDATE - System has been cleaned up!")
		EXIT
	ENDIF
	
	IF ref_local.eStage != DLS_INIT AND ref_local.eStage != DLS_LOADING
		FORCE_REMOTE_ACTION_CLIPSETS(ref_local.remoteActions)
	ENDIF
	
	DANCE_MAINTAIN_AUDIO_DATA(ref_local.audio)
	BOOL bDancingPrevFrame = IS_PLAYER_DANCING(ref_local.piLocalPlayer)
	
	#IF IS_DEBUG_BUILD
	DANCE_MAINTAIN_WIDGETS(ref_local, eAudioIntensity)
	#ENDIF //IS_DEBUG_BUILD
	
	DANCE_SET_PLAYER_IS_BUSY(ref_local, bIsPlayerBusy)

	SWITCH ref_local.eStage
		CASE DLS_INIT 				DANCE_INIT(ref_local)								BREAK
		CASE DLS_LOADING			DANCE_LOADING(ref_local)							BREAK
		CASE DLS_IDLE_NOT_DANCING 	DANCE_MAINTAIN_IDLE_NOT_DANCING(ref_local)			BREAK
		CASE DLS_DANCING_INTRO		DANCE_MAINTAIN_DANCING_INTRO(ref_local)				BREAK
		CASE DLS_DANCING			DANCE_MAINTAIN_DANCING(ref_local, eAudioIntensity)	BREAK
		CASE DLS_FLOURISH			DANCE_MAINTAIN_FLOURISH(ref_local)					BREAK
		CASE DLS_MOVE_FLOURISH		DANCE_MAINTAIN_MOVE_FLOURISH(ref_local)				BREAK
		CASE DLS_STOP_DANCING		DANCE_MAINTAIN_STOP_DANCING(ref_local)				BREAK
		
		#IF DANCING_FEATURE_DUAL_DANCE
		CASE DLS_WAIT_FOR_DUAL_DANCE_START		DANCING_UPDATE_WAIT_FOR_DUAL_DANCE_START(ref_local) BREAK
		CASE DLS_DUAL_DANCE_INTRO				DANCING_UPDATE_DUAL_DANCE_INTRO(ref_local) 			BREAK
		CASE DLS_DUAL_DANCING					DANCING_UPDATE_DUAL_DANCE(ref_local) 				BREAK
		CASE DLS_DUAL_DANCE_WAIT_FOR_LOOP_END	DANCING_UPDATE_WAIT_FOR_LOOP_END(ref_local)			BREAK
		CASE DLS_DUAL_DANCE_EXIT				DANCING_UPDATE_DUAL_DANCE_EXIT(ref_local) 			BREAK
		#ENDIF
	ENDSWITCH
	
	IF ref_local.eStage >= DLS_DANCING_INTRO
	AND ref_local.eStage <= DLS_STOP_DANCING
		IF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_1
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_2
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			ENDIF
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_3
			IF NOT IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_HEALTH_REGEN_CHANGED)
				SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(ref_local.piLocalPlayer, 1.0)
				
				SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_HEALTH_REGEN_CHANGED)
			ENDIF
			
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			ENDIF
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_4
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
			ENDIF
		ELIF ref_local.eControlScheme = DANCE_CONTROL_SCHEME_ALTERNATE_5
			IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		#IF DANCING_FEATURE_DUAL_DANCE
			DANCING_DRAW_DEBUG_INFO(ref_local)
		#ENDIF
	#ENDIF
	
	IF ref_local.eStage <> DLS_DANCING
		DANCE_PAUSE_COMBO_TIMER_THIS_FRAME(ref_local)
	ENDIF

	DANCE_MAINTAIN_BUSY_HINT(ref_local)
	DANCE_MAINTAIN_OFF_FLOOR_HINT(ref_local)
		
	IF IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_DISABLED_INTERACTION_MENU)
	AND NOT IS_PLAYER_DANCING(ref_local.piLocalPlayer)
	#IF DANCING_FEATURE_DUAL_DANCE
	AND NOT DANCING_IS_LOCAL_PLAYER_DUAL_DANCING(ref_local)
	#ENDIF
		DANCING_ENABLE_INTERACTION_MENU(ref_local, TRUE)
	ENDIF
	
	#IF DANCING_FEATURE_DUAL_DANCE
	IF DANCING_IS_LOCAL_PLAYER_DUAL_DANCING(ref_local)
		DANCING_BLOCK_COMBAT_AND_VEHICLE_CONTROLS_THIS_FRAME()
		//DANCE_MAINTAIN_FACIAL_ANIM(ref_local.faceAnim, ref_local.display.iIntensity, eAudioIntensity)
		EXIT // Bail because dual dancing background systems are handled in the state machine
	ENDIF
	#ENDIF		
		
	IF DANCE_MAINTAIN_DANCING_GLOBALS(ref_local)	
		
		IF ref_local.eStage = DLS_FLOURISH
		OR NOT ref_local.bHaveStartedDance
			DANCE_SUPPRESS_MOVEMENT()
		ENDIF
		
		DANCING_BLOCK_COMBAT_AND_VEHICLE_CONTROLS_THIS_FRAME()
		
		// Don't allow interactions while dancing...
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
		// Prevent other scripts using secondary context while dancing
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT_SECONDARY)

		DANCE_SUPPRESS_HUD(ref_local)	
		SET_FOLLOW_PED_CAM_THIS_UPDATE("NIGHTCLUB_FOLLOW_PED_CAMERA", ciDANCE_CAMERA_INTERPOLATION)
		
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
			SET_FIRST_PERSON_AIM_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(cfFIRST_PERSON_DANCE_CAM_HEADING_MIN_LIMIT, cfFIRST_PERSON_DANCE_CAM_HEADING_MAX_LIMIT)
			SET_FIRST_PERSON_AIM_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(cfFIRST_PERSON_DANCE_CAM_PITCH_MIN_LIMIT, cfFIRST_PERSON_DANCE_CAM_PITCH_MAX_LIMIT)
		ENDIF
		
		DANCE_MAINTAIN_CONTROL_LIGHTS(ref_local, FALSE)
		DANCE_MAINTAIN_PC_CONTROLS(ref_local, FALSE)
		SCALEFORM_MAINTAIN_CONTROL_ICON(ref_local)
		MAINTAIN_DANCE_FULL_MIN_REWARDS(ref_local)
		
		IF ref_local.instance.eInitControlScheme != DANCE_CONTROL_SCHEME_STANDARD
			DANCING_MAINTAIN_GIVE_DURATION_RP_REWARDS(ref_local)
		ENDIF
		
		SET_PED_RESET_FLAG(ref_local.pedLocalPlayer, PRF_UseKinematicPhysics, TRUE)
		IF NOT bDancingPrevFrame
			SET_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SET_PCF)
			SET_PED_CONFIG_FLAG(ref_local.pedLocalPlayer, PCF_UseKinematicModeWhenStationary, TRUE)
			PRINTLN("[DANCING] SET_PED_CONFIG_FLAG - PCF_UseKinematicModeWhenStationary - TRUE")
		ENDIF
		DANCE_MAINTAIN_FACIAL_ANIM(ref_local.faceAnim, ref_local.display.iIntensity, eAudioIntensity)
		
		//B* 5116948
		#IF USE_REPLAY_RECORDING_TRIGGERS
		DISABLE_REPLAY_RECORDING_UI_THIS_FRAME()
		#ENDIF
	ELSE
		IF bDancingPrevFrame
			CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_SET_PCF)
			SET_PED_CONFIG_FLAG(ref_local.pedLocalPlayer, PCF_UseKinematicModeWhenStationary, FALSE)
			PRINTLN("[DANCING] SET_PED_CONFIG_FLAG - PCF_UseKinematicModeWhenStationary - FALSE")
		ENDIF
		
		IF IS_RADAR_HIDDEN()
		AND IS_BIT_SET_ENUM(ref_local.iBs, DANCE_LOCAL_BS_RADAR_HIDDEN)
		AND NOT IS_BROWSER_OPEN()
			DISPLAY_RADAR(TRUE)
			CLEAR_BIT_ENUM(ref_local.iBs, DANCE_LOCAL_BS_RADAR_HIDDEN)
		ENDIF

		DANCE_MAINTAIN_CONTROL_LIGHTS(ref_local, TRUE)
		DANCE_MAINTAIN_PC_CONTROLS(ref_local, TRUE)
	ENDIF
	
ENDPROC
