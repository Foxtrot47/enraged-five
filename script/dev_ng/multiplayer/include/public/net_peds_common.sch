//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_COMMON.sch																						//
// Description: Header file containing common functionality for the peds system.										//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_private.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ FUNCTIONS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC INT GET_PED_NETWORK_ID(INT iNetworkPedArrayID)
	RETURN (iNetworkPedArrayID+NETWORK_PED_ID_0)
ENDFUNC

FUNC INT GET_PED_NETWORK_ARRAY_ID(INT iPed)
	RETURN (iPed-NETWORK_PED_ID_0)
ENDFUNC

//// PURPOSE:
///    Wrapper function for GET_FRAME_COUNT to process a function every iFrameCount frames.
/// PARAMS:
///    iFrameCount - Number of frames apart to process function.
/// RETURNS: TRUE when to process the function this frame.
FUNC BOOL PROCESS_FUNCTION_THIS_FRAME(INT iFrameCount)
	IF (iFrameCount < 0)
		RETURN FALSE
	ENDIF
	RETURN (GET_FRAME_COUNT() % iFrameCount = 0)
ENDFUNC

/// PURPOSE:
///    Requests and loads an animation dictionary.
/// PARAMS:
///    sAnimDict - The animation dictionary to request and load.
/// RETURNS: TRUE when the animation dictionary has been requested and loaded, FALSE otherwise.
FUNC BOOL REQUEST_AND_LOAD_ANIMATION_DICT(STRING sAnimDict)
	IF NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
		REQUEST_ANIM_DICT(sAnimDict)
		RETURN HAS_ANIM_DICT_LOADED(sAnimDict)
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the visibility state for all props of a local ped.
/// PARAMS:
///    Data - Ped data to query.
///    bVisible - Visibility state.
PROC SET_LOCAL_PED_PROPS_VISIBLE(PEDS_DATA &Data, BOOL bVisible)
	
	INT iProp
	REPEAT MAX_NUM_PED_PROPS iProp
		IF DOES_ENTITY_EXIST(Data.PropID[iProp])
			SET_ENTITY_VISIBLE(Data.PropID[iProp], bVisible)
			PRINTLN("[AM_MP_PEDS] SET_LOCAL_PED_PROPS_VISIBLE Prop ",iProp," HIDDEN")
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///     Sets the visibility state for all props of a local ped.
/// PARAMS:
///    NetworkPedData - Network ped data to query.
///    bVisible - Visibility state.
///    iPropsToUpdate - Counts the number of props still needing their visibility state changed.
PROC SET_NETWORK_PED_PROPS_VISIBLE(NETWORK_PED_DATA &NetworkPedData, BOOL bVisible, INT &iPropsToUpdate)
	
	INT iProp
	REPEAT MAX_NUM_PED_PROPS iProp
		IF DOES_ENTITY_EXIST(NetworkPedData.Data.PropID[iProp])
			IF (bVisible AND NOT IS_ENTITY_VISIBLE(NetworkPedData.Data.PropID[iProp]))	// Still to turn visible
			OR (NOT bVisible AND IS_ENTITY_VISIBLE(NetworkPedData.Data.PropID[iProp]))	// Still to turn invisible
				iPropsToUpdate++
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(NetworkPedData.niPropID[iProp])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(NetworkPedData.niPropID[iProp])
						SET_ENTITY_VISIBLE(NetworkPedData.Data.PropID[iProp], bVisible)
						SET_ENTITY_VISIBLE_IN_CUTSCENE(NET_TO_OBJ(NetworkPedData.niPropID[iProp]), bVisible)
						SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedData.niPropID[iProp], bVisible)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(NetworkPedData.niPropID[iProp])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Gets the phase of the current task anim playing on a ped.
///    Calculating the anim phase via script is less expensive that querying the native GET_ENTITY_ANIM_CURRENT_TIME each frame.
/// PARAMS:
///    Data - Ped data to query.
/// RETURNS: Phase of the current task anim playing as a FLOAT.
FUNC FLOAT GET_CURRENT_TASK_ANIM_PHASE(PEDS_DATA &Data)
	INT iDifference		 = (GET_GAME_TIMER()-Data.animData.iTaskAnimStartTimeMS)
	INT iIncrease		 = (Data.animData.iTaskAnimDurationMS-iDifference)
	FLOAT fPercentage	 = (TO_FLOAT(iIncrease)/TO_FLOAT(Data.animData.iTaskAnimDurationMS))*100.0
	FLOAT fPhase		 = (100.0-fPercentage)
	RETURN (fPhase/100)
ENDFUNC

/// PURPOSE:
///    Calculates the time until the end of the current animation in seconds.
/// PARAMS:
///    Data - Ped data to query.
/// RETURNS: Time until the end of the current animation in seconds.
FUNC INT GET_TIME_UNTIL_END_OF_CURRENT_ANIM_IN_SECONDS(PEDS_DATA &Data)
	INT iDifference = (GET_GAME_TIMER()-Data.animData.iTaskAnimStartTimeMS)
	RETURN (Data.animData.iTaskAnimDurationMS-iDifference)/1000
ENDFUNC

/// PURPOSE:
///    Performs the Fisher-Yates shuffle algorithm on an array.
/// PARAMS:
///    array - The array to shuffle.
///    iSortToElement - The element to shuffle up to. -1 means it will shuffle the entire array.
PROC PERFORM_FISHER_YATES_SHUFFLE(INT &array[], INT iSortToElement = -1)
	
	INT i
	INT iRandNumber
	INT iTempElement
	INT iMaxElement = COUNT_OF(array)
	
	// Is iSortToElement valid
	IF (iSortToElement != -1 
	AND iSortToElement < iMaxElement)
		iMaxElement = iSortToElement
	ENDIF
	
	// Increment backwards and swap random element with new last element in the array.
	FOR i = (iMaxElement-1) TO 0 STEP -1
		iRandNumber = GET_RANDOM_INT_IN_RANGE(0, i+1)
		iTempElement = array[iRandNumber]
		array[iRandNumber] = array[i]
		array[i] = iTempElement
	ENDFOR
	
ENDPROC

/// PURPOSE:
///    Resets the HEAD_TRACKING_DATA struct.
/// PARAMS:
///    HeadTrackingData - Struct to reset.
PROC RESET_PED_CHANGED_CAPSULE_SIZE_DATA(CHANGED_CAPSULE_SIZE_DATA &ChangedCapsuleData)
	CHANGED_CAPSULE_SIZE_DATA blankChangedCapsuleSizeData
	ChangedCapsuleData = blankChangedCapsuleSizeData
ENDPROC
/// PURPOSE:
///    Sets the local ped changed capsule size data.
///    Populates the local changed capsule size array. Array size: MAX_NUM_LOCAL_CHANGED_CAPSULE_SIZE_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_PED_CHANGED_CAPSULE_SIZE_LOCAL_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iArrayID
	REPEAT MAX_NUM_LOCAL_CHANGED_CAPSULE_SIZE_PEDS iArrayID
		RESET_PED_CHANGED_CAPSULE_SIZE_DATA(LocalData.ChangedCapsuleData[iArrayID])
		SET_PED_CHANGED_CAPSULE_SIZE_DATA(ePedLocation, LocalData.ChangedCapsuleData[iArrayID], ServerBD.iLayout, iArrayID, LocalData.iChangedCapsuleSizePedID)
	ENDREPEAT
	
ENDPROC
/// PURPOSE:
///    Initialises the ped head tracking local array.
///    Sets all elements to -1.
/// PARAMS:
///    LocalData - Local ped data to set.
PROC INITIALISE_PED_CHANGED_CAPSULE_SIZE_LOCAL_ARRAY_ID(SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		LocalData.iChangedCapsuleSizePedID[iPed] = -1
	ENDREPEAT
	
ENDPROC
/// PURPOSE:
///    Sets the changed capsule size data.
///    Populates the changed capsule size arrays. 
///    Local array max: MAX_NUM_LOCAL_CHANGED_CAPSULE_SIZE_PEDS
///    Network array size: N/A
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_ALL_PED_CHANGED_CAPSULE_SIZE_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	INITIALISE_PED_CHANGED_CAPSULE_SIZE_LOCAL_ARRAY_ID(LocalData)
	SET_PED_CHANGED_CAPSULE_SIZE_LOCAL_DATA(ePedLocation, ServerBD, LocalData)
ENDPROC
PROC UPDATE_LOCAL_PED_CAPSULE_SIZE(SCRIPT_PED_DATA &LocalData, INT iPedID)
	
	IF NOT IS_PEDS_BIT_SET(LocalData.PedData[iPedID].iBS, BS_PED_DATA_CHANGE_CAPSULE_SIZE)
		EXIT
	ENDIF
		
	IF (LocalData.iChangedCapsuleSizePedID[iPedID] = -1)
		EXIT
	ENDIF
		
	IF IS_PEDS_BIT_SET(LocalData.PedData[iPedID].iBS, BS_PED_DATA_CHANGE_CAPSULE_SIZE)
		SET_PED_CAPSULE(LocalData.PedData[iPedID].PedID, LocalData.ChangedCapsuleData[LocalData.iChangedCapsuleSizePedID[iPedID]].fCapsuleRadius)
	ENDIF	
ENDPROC

FUNC BOOL IS_CUTSCENE_RUNNING_FOR_PED_LOCATION(PED_LOCATIONS ePedLocation)
	SWITCH ePedLocation
		#IF FEATURE_TUNER
		CASE PED_LOCATION_CAR_MEET
			RETURN IS_PLAYER_LOCALLY_RUNNING_CAR_MEET_MOCAP()
		BREAK
		CASE PED_LOCATION_CAR_MEET_SANDBOX
			RETURN MPGlobals.bStartedMPCutscene
		BREAK
		CASE PED_LOCATION_AUTO_SHOP
			RETURN IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_AUTO_SHOP_STAFF_DELIVERY_CUTSCENE_ACTIVE)
		BREAK
		CASE PED_LOCATION_MUSIC_STUDIO
			RETURN (g_clubMusicData.bHasDJSwitchOverSceneStarted OR g_bIsPlayerWatchingMusicStudioCutscene)
		BREAK
		CASE PED_LOCATION_ISLAND
			RETURN IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID()) OR IS_CUTSCENE_PLAYING()
		BREAK
		#ENDIF
		DEFAULT
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC RESET_PED_PROP_DATA(PED_PROP_DATA &pedPropData)
	pedPropData.ePropModel 				= DUMMY_MODEL_FOR_SCRIPT
	pedPropData.ePedBone 				= BONETAG_NULL
	pedPropData.bAttachToBone 			= FALSE
	pedPropData.bUseCustomSpawnLocation	= FALSE
	pedPropData.bFreezeProp				= FALSE
	pedPropData.vBoneOffsetPosition 	= <<0.0, 0.0, 0.0>>
	pedPropData.vBoneOffsetRotation 	= <<0.0, 0.0, 0.0>>
	pedPropData.vPropSpawnCoords 		= <<0.0, 0.0, 0.0>>
	pedPropData.vPropSpawnRotation		= <<0.0, 0.0, 0.0>>
ENDPROC

PROC GET_PED_PROP_DATA2(PED_LOCATIONS ePedLocation, PED_ACTIVITIES eActivity, INT iProp, PED_PROP_DATA &pedPropData)
	UNUSED_PARAMETER(ePedLocation)
	RESET_PED_PROP_DATA(pedPropData)
	SWITCH eActivity
	#IF FEATURE_DLC_1_2022
		CASE PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_BAR_STOOL_01
					pedPropData.vPropSpawnCoords		= <<1122.760, -3143.686, -38.063>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0,  90.0>>
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR_B
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_BAR_STOOL_01
					pedPropData.vPropSpawnCoords		= <<999.901, -3169.038, -35.088>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0,  90.0>>
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_F
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_M
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_CIGGY_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_M
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_F
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= ex_p_ex_tumbler_01_s//prop_bottle_macbeth
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
					pedPropData.vBoneOffsetPosition		= <<0.00, 0.000, 0.035>>
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_M
		CASE PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_F
		CASE PED_ACT_CLUBHOUSE_LEAN_DRINK
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= Prop_CS_Beer_Bot_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		CASE PED_ACT_MUSIC_STUDIO_E_STAND_DRINK_CUP
			SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				= ex_p_ex_tumbler_01_s
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK	
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_SIT_DRINK
			SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				= EX_P_EX_TUMBLER_01_S
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK	
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_FRANKLIN
			SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				= P_CS_Joint_01
				BREAK
				CASE 1
					pedPropData.ePropModel				= prop_npc_phone
					pedPropData.bUseCustomSpawnLocation = TRUE
					pedPropData.vPropSpawnCoords		= <<-1002.372, -81.416, -100.166>>
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_LAMAR_VFX_PASS
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_CS_Joint_01
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_LAMAR
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_CS_Lighter_01
				BREAK
				CASE 1 
					pedPropData.ePropModel				= Prop_CS_Beer_Bot_01
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_CLIPBOARD
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= p_amb_clipboard_01
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_FIXER_TEXTING
		#IF FEATURE_DLC_1_2022
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_LEANING_PHONE_F
		CASE PED_ACT_WAREHOUSE_STAND_TEXTING_F
		CASE PED_ACT_WAREHOUSE_STAND_TEXTING_M
		#ENDIF
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		#IF FEATURE_DLC_1_2022
		CASE PED_ACT_WAREHOUSE_NOTEPAD
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= p_notepad_01_s
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
				CASE 1
					pedPropData.ePropModel				= prop_pencil_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
					pedPropData.vBoneOffsetPosition		= <<0.00, 0.000, -0.025>>
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		CASE PED_ACT_FIXER_STAND_ON_PHONE
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_FIXER_DRINK
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_PAPER_CUP
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_TEXTING
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= prop_npc_phone
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
					pedPropData.vBoneOffsetPosition		= <<0.00, 0.000, 0.035>>
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING_2
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_DRUMS
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_VOCALS
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Headphones_DJ"))
				BREAK	
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("hei_Prop_Heist_Off_Chair"))
					pedPropData.bUseCustomSpawnLocation = TRUE
					pedPropData.vPropSpawnCoords		= <<-996.4230, -68.3292, -99.9830>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0, 0.0>>				
				BREAK	
				CASE 2
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_S_Mixer_01a"))
					pedPropData.bUseCustomSpawnLocation = TRUE
					pedPropData.vPropSpawnCoords		= <<-995.6716, -68.6484, -99.1514>>
					pedPropData.vPropSpawnRotation		= <<13.178, 0.0, -119.726>>	
				BREAK
				CASE 3
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_S_Mixer_01b"))
					pedPropData.bUseCustomSpawnLocation = TRUE
					pedPropData.vPropSpawnCoords		= <<-996.1022, -68.4025, -99.2365>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0, -119.726>>	
				BREAK
				CASE 4 //DONE
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_02a"))
					pedPropData.bUseCustomSpawnLocation = TRUE
					pedPropData.vPropSpawnCoords		= <<-995.1022, -67.9159, -99.1514>>
					pedPropData.vPropSpawnRotation		= <<13.178, 0.0, -119.726>>			
				BREAK
				CASE 5
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_02a"))
					pedPropData.bUseCustomSpawnLocation = TRUE
					pedPropData.vPropSpawnCoords		= <<-996.0898, -69.3809, -99.1514>>
					pedPropData.vPropSpawnRotation		= <<13.178, 0.0, -119.726>>		
				BREAK
				CASE 6 //DONE
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_S_Mixer_02b"))
					pedPropData.bUseCustomSpawnLocation = TRUE
					pedPropData.vPropSpawnCoords		= <<-995.6840, -67.6700, -99.2365>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0, -119.726>>		
				BREAK						
				CASE 7
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_S_Mixer_02b"))
					pedPropData.bUseCustomSpawnLocation = TRUE
					pedPropData.vPropSpawnCoords		= <<-996.5204, -69.1349, -99.2365>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0, -119.726>>		
				BREAK			
			ENDSWITCH
		BREAK	
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING_2
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRUMS
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_VOCALS
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRE_ABSENT
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("hei_Prop_Heist_Off_Chair"))	
				BREAK
			ENDSWITCH
		BREAK	
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_DRUMS
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Headphones_DJ"))
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Drum_Stick_01a"))
				BREAK
				CASE 2
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Drum_Stick_01a"))
				BREAK
				CASE 3
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_drum_kit_01a"))
				BREAK
				CASE 4
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_chair_stool_08a"))
				BREAK
			ENDSWITCH	
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_VOCALS
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("BA_Prop_Battle_Headphones_DJ"))
				BREAK
			ENDSWITCH	
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_PATROL
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_AMB_CLIPBOARD_01
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK		
	ENDSWITCH
ENDPROC
PROC GET_PED_PROP_DATA(PED_LOCATIONS ePedLocation, PED_ACTIVITIES eActivity, INT iProp, PED_PROP_DATA &pedPropData)
	UNUSED_PARAMETER(ePedLocation)
	RESET_PED_PROP_DATA(pedPropData)
	
	SWITCH eActivity
		CASE PED_ACT_LEANING_TEXTING_M_01
		CASE PED_ACT_LEANING_TEXTING_F_01
		CASE PED_ACT_ON_PHONE_M_01
		CASE PED_ACT_ON_PHONE_F_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_LEANING_SMOKING_M_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_CIGGY_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_LEANING_SMOKING_F_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_CIGGY_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
					pedPropData.vBoneOffsetPosition		= <<0.02, -0.0175, -0.02>>
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_STANDING_SMOKING_M_01
		CASE PED_ACT_STANDING_SMOKING_F_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_CIGGY_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_SMOKING_WEED_M_01
		CASE PED_ACT_SMOKING_WEED_F_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_AMB_JOINT_01
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DRINKING_M_01
		CASE PED_ACT_DRINKING_F_01
		CASE PED_ACT_SITTING_DRINKING_M_01
		CASE PED_ACT_SITTING_DRINKING_F_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_BEER_BOTTLE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_TUMBLER_01_S
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
					pedPropData.vBoneOffsetPosition		= <<-0.0125, 0.0175, 0.0>>
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_02
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_TUMBLER_01_S
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
					pedPropData.vBoneOffsetPosition		= <<-0.025, -0.0175, 0.0>>
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_03
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_BEER_LOGGER
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
					pedPropData.vBoneOffsetPosition		= <<-0.0135, -0.004, 0.0>>
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_F_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_WINE_GLASS_S
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_F_02
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_BEER_LOGGER
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_02
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_F_01
		CASE PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_M_01
		CASE PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_F_01
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_01
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_02
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_F_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_CS_JOINT_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01
		CASE PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_M_01
		CASE PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_F_01
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_01
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_02
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_F_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_PAPER_CUP
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_01
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01
		CASE PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_M_01
		CASE PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_F_01
		CASE PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_M_01
		CASE PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_F_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_02
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_01
		CASE PED_ACT_DANCING_SINGLE_PROP_M_02
		CASE PED_ACT_DANCING_SINGLE_PROP_M_05
		CASE PED_ACT_DANCING_SINGLE_PROP_M_06
		CASE PED_ACT_DANCING_SINGLE_PROP_F_02
		CASE PED_ACT_DANCING_SINGLE_PROP_F_04
		CASE PED_ACT_DANCING_SINGLE_PROP_F_05
		CASE PED_ACT_DANCING_SINGLE_PROP_F_06
		CASE PED_ACT_DANCING_AMBIENT_F_02
		CASE PED_ACT_DANCING_AMBIENT_F_06
		CASE PED_ACT_DANCING_AMBIENT_M_01
		CASE PED_ACT_DANCING_AMBIENT_M_02
		CASE PED_ACT_DANCING_AMBIENT_M_06
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_BEER_BOTTLE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
					pedPropData.vBoneOffsetPosition		= <<0.0, 0.0, 0.15>>
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_03
		CASE PED_ACT_DANCING_SINGLE_PROP_M_04
		CASE PED_ACT_DANCING_SINGLE_PROP_F_01
		CASE PED_ACT_DANCING_SINGLE_PROP_F_03
		CASE PED_ACT_DANCING_AMBIENT_F_01
		CASE PED_ACT_DANCING_AMBIENT_F_03
		CASE PED_ACT_DANCING_AMBIENT_M_04
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_PAPER_CUP
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPA_F_PARENT
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_NPC_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPB_F_PARENT
		CASE PED_ACT_DANCING_GROUPC_M_PARENT
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_NPC_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_vape_01"))
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPD_F_PARENT
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_vape_01"))
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPE_F_PARENT
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_NPC_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_01
		CASE PED_ACT_DANCING_BEACH_PROP_M_02
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_BEER_BOTTLE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
					pedPropData.vBoneOffsetPosition		= <<0.0, 0.0, 0.150>>
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_03
		CASE PED_ACT_DANCING_BEACH_PROP_F_02
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_CIGGY_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_04
		CASE PED_ACT_DANCING_BEACH_PROP_F_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_TUMBLER_01_S
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_SOLOMUN
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("BA_Prop_Battle_Headphones_DJ"))
				BREAK
				CASE 1
					pedPropData.ePropModel				= P_WINE_GLASS_S
				BREAK
			ENDSWITCH
		BREAK						
		CASE PED_ACT_DJ_PALMS_TRAX	
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_headphones_dj"))
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Can_Beer_01a"))
				BREAK
			ENDSWITCH
		BREAK	
		CASE PED_ACT_DJ_KEINEMUSIK_RAMPA
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("BA_Prop_Battle_Headphones_DJ"))
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("apa_Prop_cs_Plastic_cup_01"))
				BREAK
				CASE 2
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_T_Bottle_02b"))
				BREAK
				CASE 3
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_P_CS_Shot_Glass_2_S"))
				BREAK
				CASE 4
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_P_CS_Shot_Glass_2_S"))
				BREAK
				CASE 5
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_P_CS_Shot_Glass_2_S"))
				BREAK
			ENDSWITCH
		BREAK	
		CASE PED_ACT_DJ_KEINEMUSIK_ME
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_headphones_dj"))
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_Club_Water_Bottle"))
				BREAK
				CASE 2
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_coconutdrink_01a"))
				BREAK
			ENDSWITCH
		BREAK	
		CASE PED_ACT_DJ_KEINEMUSIK_ADAM
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_headphones_dj"))
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_Club_Water_Bottle"))
				BREAK
				CASE 2
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("apa_Prop_cs_Plastic_cup_01"))
				BREAK
			ENDSWITCH	
		BREAK
		CASE PED_ACT_DJ_MOODYMANN
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_turntable_01a"))
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_turntable_01a"))
				BREAK
				CASE 2
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_lp_01a"))
				BREAK
				CASE 3
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_lp_01a"))
				BREAK
				CASE 4
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_bag_djlp_01a"))
				BREAK
				CASE 5
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_Tumbler_01"))
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_PAVEL_SCREENS
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_AMB_CLIPBOARD_01
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
				BREAK
				CASE 1
					pedPropData.ePropModel				= PROP_PENCIL_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
				CASE 2
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Engine_FuseBox_01a"))
					pedPropData.vPropSpawnCoords		= <<1562.2163, 430.1717, -55.8142>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0, 0.0>>
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_PAVEL_TORPEDOS
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_CIGGY_01B
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Engine_FuseBox_01a"))
					pedPropData.vPropSpawnCoords		= <<1562.2163, 430.1717, -55.8142>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0, 0.0>>
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_PAVEL_CAVIAR
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_Caviar_Tin_01a"))
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_Caviar_Spoon_01a"))
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
				CASE 2
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Engine_FuseBox_01a"))
					pedPropData.vPropSpawnCoords		= <<1562.2163, 430.1717, -55.8142>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0, 0.0>>
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_PAVEL_ENGINE
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= Prop_CS_Wrench
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Engine_FuseBox_01a"))
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_PAVEL_BUTTONS
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Engine_FuseBox_01a"))
					pedPropData.vPropSpawnCoords		= <<1562.2163, 430.1717, -55.8142>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0, 0.0>>
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_KAYLEE
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_JACKIE
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_CS_SHOT_GLASS_2_S
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_PATRICIA
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_CS_SHOT_GLASS_2_S
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("Ba_prop_battle_champ_open_03"))
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
				BREAK
				CASE 2
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_T_bottle_01a"))
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_MIGUEL
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
				CASE 1
					pedPropData.ePropModel				= P_CS_SHOT_GLASS_2_S
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_VIP_ELRUBIO_IG2
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_CIGGY_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_VIP_SCOTT_IG3
		CASE PED_ACT_BEACH_VIP_SCOTT_IG4	
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("apa_Prop_cs_Plastic_cup_01"))
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_GIRL
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_COUPLE_MALE_PARENT
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_STANDING_CALL
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_LEAN_TEXT
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0, 90.0>>
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CLIPBOARD
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= P_CS_CLIPBOARD
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_JANITOR
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_TOOL_BROOM
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH	
		BREAK
		CASE PED_ACT_AUTO_SHOP_MECHANIC_LEAN_PNONE
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH	
		BREAK
		CASE PED_ACT_AUTO_SHOP_MECHANIC_TEXTING
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_AUTO_SHOP_SESSANTA_TEXTING
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("prop_off_chair_04_s"))
					pedPropData.vPropSpawnCoords		= <<-1358.3520, 137.9780,  -96.07>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0, 90.0>>
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK
			ENDSWITCH	
		BREAK
		CASE PED_ACT_AUTO_SHOP_SESSANTA_DESK
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Laptop_Jimmy"))
					pedPropData.vPropSpawnCoords		= <<-1358.1020, 137.9780, -95.374>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0,  270.0>>
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("prop_off_chair_04_s"))
					pedPropData.vPropSpawnCoords		= <<-1358.7920, 137.9780,  -96.07>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0,  90.0>>
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK				
			ENDSWITCH	
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_SITTING_LAPTOP
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_laptop_01c"))
					pedPropData.vPropSpawnCoords		= <<-1020.656, -84.475, -99.606>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0,  -166.095>>		
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_RECEPTION
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("prop_off_chair_04_s"))
					pedPropData.vPropSpawnCoords		= <<-1022.5550, -83.4179, -100.3531>>
					pedPropData.vPropSpawnRotation		= <<0.0000, 0.0000, 18.6748>>	
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK				
			ENDSWITCH	
		BREAK
		CASE PED_ACT_FIXER_HQ_RECEPTION
		CASE PED_ACT_FIXER_HQ_SECURITY_DESK
			SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("ex_prop_offchair_exec_03"))
				BREAK		
			ENDSWITCH	
		BREAK
		CASE PED_ACT_IMANI
			SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				= Prop_Mug_01
				BREAK	
				CASE 1 
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_OffChair_03"))
				BREAK	
			ENDSWITCH	
		BREAK
		CASE PED_ACT_FIXER_FRANKLIN_0
			SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("ex_Prop_OffChair_Exec_04"))
				BREAK	
				CASE 1 
					pedPropData.ePropModel				= PROP_NPC_PHONE
				BREAK						
			ENDSWITCH
		BREAK
		CASE PED_ACT_FIXER_FRANKLIN_1				
			SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("ex_prop_exec_ashtray_01"))
				BREAK	
				CASE 1 
					pedPropData.ePropModel				= P_CS_JOINT_01
				BREAK	
			ENDSWITCH
		BREAK
		CASE PED_ACT_FIXER_FRANKLIN_2
			SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("ex_Prop_OffChair_Exec_04"))
				BREAK	
				CASE 1 
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Keyboard_01a"))
				BREAK	
				CASE 2 
					pedPropData.ePropModel				= Prop_CS_Mouse_01
				BREAK	
			ENDSWITCH
		BREAK
		CASE PED_ACT_FIXER_LAMAR
			SWITCH GET_LAMARS_VARIATION(g_iAnimLayout)
				CASE NET_PEDS_FIXER_LAMAR_VARIATION_0
					SWITCH iProp
						CASE 0 
							pedPropData.ePropModel				= P_CS_JOINT_01
						BREAK	
					ENDSWITCH
				BREAK
				CASE NET_PEDS_FIXER_LAMAR_VARIATION_1
					SWITCH iProp
						CASE 0 
							pedPropData.ePropModel				= P_CS_JOINT_01
						BREAK	
						CASE 1 
							pedPropData.ePropModel				= PROP_NPC_PHONE
						BREAK	
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_COUCH
		CASE PED_ACT_MUSIC_STUDIO_POOH_STAND
			SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				= P_CS_JOINT_01
				BREAK	
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_TEXT_STAND
		SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				=  prop_npc_phone
				BREAK	
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_TEXT_SIT
		SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				= prop_npc_phone
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK	
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_DRINK_BAR
			SWITCH iProp
				CASE 0 
					pedPropData.ePropModel				= EX_P_EX_TUMBLER_01_S
				BREAK	
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_STAND_DRINK
		SWITCH iProp			
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_BEER_BOTTLE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK	
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_DRINK_SIT
			SWITCH iProp			
				CASE 0
					pedPropData.ePropModel				= EX_P_EX_TUMBLER_01_S
				BREAK	
			ENDSWITCH
		BREAK
		CASE PED_ACT_AUTO_SHOP_SESSANTA_BARRIER_LEAN
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
				CASE 1
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("prop_off_chair_04_s"))
					pedPropData.vPropSpawnCoords		= <<-1358.3520, 137.9780,  -96.07>>
					pedPropData.vPropSpawnRotation		= <<0.0, 0.0,  90.0>>
					pedPropData.bUseCustomSpawnLocation	= TRUE
					pedPropData.bFreezeProp				= TRUE
				BREAK
			ENDSWITCH	
		BREAK
		CASE PED_ACT_CAR_MEET_PHONE_F
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CAR_MEET_PHONE_M
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_DANCER_01
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CAR_MEET_DRINK_COFFEE_F
		CASE PED_ACT_CAR_MEET_DRINK_COFFEE_M
		CASE PED_ACT_MUSIC_STUDIO_DRINK_COFFEE
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_PAPER_CUP
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CAR_MEET_PAPARAZZI
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= prop_pap_camera_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_B
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A_TRANS_B
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_A
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_MALE_A
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= prop_ld_can_01  
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= prop_ld_can_01  
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_STAND_MEDIC_TOD
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= prop_notepad_01
					pedPropData.ePedBone 				= BONETAG_PH_L_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
				CASE 1
					pedPropData.ePropModel				= prop_pencil_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_H
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_B
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_C
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_D
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_E
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_F
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_G
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_H
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_C
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_D
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_E
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_F
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_G
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_H
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_D
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_E
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_F
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_G
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_H
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_E
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_F
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_G
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_H
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_F
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_G
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_H
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_G
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_H
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G_TRANS_H
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_D
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_B
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_C
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_D
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_C
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_D
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C_TRANS_D
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE  // SHOULD BE REPLACED
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CAR_MEET_PATROL
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_C
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_B
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_C
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B_TRANS_C
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_A
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_B
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_FEMALE_A
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_CS_CIGGY_01
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE PED_ACT_FIXER_TALK_ON_PHONE
		CASE PED_ACT_FIXER_TALK_ON_PHONE_FEMALE
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= PROP_AMB_PHONE
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_SIT_TABLET
			SWITCH iProp
				CASE 0
					pedPropData.ePropModel				= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_tablet_01a"))
					pedPropData.ePedBone 				= BONETAG_PH_R_HAND
					pedPropData.bAttachToBone			= TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	

	
	
	IF pedPropData.ePropModel = DUMMY_MODEL_FOR_SCRIPT
		GET_PED_PROP_DATA2(ePedLocation, eActivity, iProp, pedPropData)
	ENDIF
	
ENDPROC

PROC GET_ANIM_PROP_VISIBLE_PHASES(FLOAT &fPropVisibleStartPhase, FLOAT &fPropVisibleEndPhase, PED_ACTIVITIES eActivity, INT iProp = 0, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	fPropVisibleStartPhase 	= -1.0
	fPropVisibleEndPhase 	= -1.0
	
	SWITCH eActivity
		CASE PED_ACT_DANCING_GROUPA_F_PARENT
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 6
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.4
									fPropVisibleEndPhase 	= 0.91
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPB_F_PARENT
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 5
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.65
									fPropVisibleEndPhase 	= 0.97
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.52
									fPropVisibleEndPhase 	= 0.96
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 3
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.42
									fPropVisibleEndPhase 	= 0.99
								ENDIF
							BREAK
							CASE 7
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.28
									fPropVisibleEndPhase 	= 0.65
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.8
									fPropVisibleEndPhase 	= 0.99
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				CASE 1	//ba_prop_battle_vape_01
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 3
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.4
									fPropVisibleEndPhase 	= 0.91
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPC_M_PARENT
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 4
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.05
									fPropVisibleEndPhase 	= 0.96
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 6
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.28
									fPropVisibleEndPhase 	= 0.85
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.15
									fPropVisibleEndPhase 	= 0.88
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				CASE 1 //ba_prop_battle_vape_01
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 1
								IF (bDancingTransition)
									IF (eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
									OR (eMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
										fPropVisibleStartPhase 	= 0.65
										fPropVisibleEndPhase 	= 0.99
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPD_F_PARENT
			SWITCH iProp
				CASE 0	//ba_prop_battle_vape_01
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 6
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.15
									fPropVisibleEndPhase 	= 0.83
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 8
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.02
									fPropVisibleEndPhase 	= 0.96
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPE_F_PARENT
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 4
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.13
									fPropVisibleEndPhase 	= 0.86
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 1
								IF (bDancingTransition)
									IF (eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
									OR (eMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
										fPropVisibleStartPhase 	= 0.03
										fPropVisibleEndPhase 	= 0.76
									ENDIF
								ENDIF
							BREAK
							CASE 3
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.00
									fPropVisibleEndPhase 	= 0.55
								ENDIF
							BREAK
							CASE 6
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.27
									fPropVisibleEndPhase 	= 0.83
								ENDIF
							BREAK
							CASE 7
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.38
									fPropVisibleEndPhase 	= 0.92
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.52
									fPropVisibleEndPhase 	= 0.79
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 3
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.13
									fPropVisibleEndPhase 	= 0.81
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									fPropVisibleStartPhase 	= 0.52
									fPropVisibleEndPhase 	= 0.99
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_DANCER_01
			SWITCH iProp
				CASE 0	//PROP_AMB_PHONE
					SWITCH iClip
						CASE 5
							fPropVisibleStartPhase 	= 0.08
							fPropVisibleEndPhase 	= 0.27
						BREAK
					ENDSWITCH
				BREAK
		ENDSWITCH
		BREAK
		
		
	ENDSWITCH
	
ENDPROC

/// Renamed from GET_PED_MODEL to GET_PEDS_MODEL in B*6956620
FUNC MODEL_NAMES GET_PEDS_MODEL(PED_MODELS ePedModel)
	MODEL_NAMES eModel = DUMMY_MODEL_FOR_SCRIPT
	SWITCH ePedModel
		CASE PED_MODEL_HIPSTER_MALE_01			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_ClubCust_01"))			BREAK
		CASE PED_MODEL_HIPSTER_MALE_02			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_ClubCust_02"))			BREAK
		CASE PED_MODEL_HIPSTER_MALE_03			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_ClubCust_03"))			BREAK
		CASE PED_MODEL_HIPSTER_MALE_04			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_ClubCust_04"))			BREAK
		CASE PED_MODEL_HIPSTER_FEMALE_01		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_ClubCust_01"))			BREAK
		CASE PED_MODEL_HIPSTER_FEMALE_02		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_ClubCust_02"))			BREAK
		CASE PED_MODEL_HIPSTER_FEMALE_03		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_ClubCust_03"))			BREAK
		CASE PED_MODEL_HIPSTER_FEMALE_04		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_ClubCust_04"))			BREAK
		CASE PED_MODEL_BEACH_MALE_01			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_M_O_Beach_02"))				BREAK
		CASE PED_MODEL_BEACH_MALE_02			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_Beach_04"))				BREAK
		CASE PED_MODEL_BEACH_FEMALE_01			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_Beach_02"))				BREAK
		CASE PED_MODEL_HIGH_SECURITY_01			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_Y_Casino_01"))				BREAK
		CASE PED_MODEL_RECEPTIONIST				 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_F_Y_Casino_01"))				BREAK
		CASE PED_MODEL_SOLOMUN					 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("IG_Sol"))						BREAK
		CASE PED_MODEL_PAVEL					 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_helmsmanpavel"))			BREAK
		CASE PED_MODEL_KAYLEE					 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_kaylee"))					BREAK
		CASE PED_MODEL_BODYGUARD				 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("s_m_m_highsec_04"))			BREAK
		CASE PED_MODEL_PATRICIA					 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_patricia_02"))				BREAK
		CASE PED_MODEL_MIGUEL					 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_miguelmadrazo"))			BREAK
		CASE PED_MODEL_JACKIE					 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_jackie"))					BREAK
		CASE PED_MODEL_KEINEMUSIK_RAMPA			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_isldj_00"))					BREAK
		CASE PED_MODEL_KEINEMUSIK_ME			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_isldj_01"))					BREAK
		CASE PED_MODEL_KEINEMUSIK_ADAM			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_isldj_02"))					BREAK
		CASE PED_MODEL_PALMS_TRAX				 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_isldj_03"))					BREAK
		CASE PED_MODEL_MOODYMANN				 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_isldj_04"))					BREAK
		CASE PED_MODEL_KEINEMUSIK_DANC_01		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_Beach_01"))				BREAK
		CASE PED_MODEL_KEINEMUSIK_DANC_02		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_Beach_01"))				BREAK
		CASE PED_MODEL_MOODYMANN_DANC_01		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_isldj_04_e_01"))			BREAK
		CASE PED_MODEL_MOODYMANN_DANC_02		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_isldj_04_d_02"))			BREAK
		CASE PED_MODEL_MOODYMANN_DANC_03		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_isldj_04_d_01"))			BREAK
		CASE PED_MODEL_CLUB_BOUNCERS			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_BOUNCER_02"))			BREAK
		CASE PED_MODEL_ISLAND_GUARDS			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_m_cartelguards_02"))		BREAK
		CASE PED_MODEL_ELRUBIO					 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_juanstrickler"))			BREAK
		CASE PED_MODEL_SCOTT					 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_sss"))						BREAK
		CASE PED_MODEL_DAVE					 	 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("IG_EnglishDave_02"))			BREAK
		CASE PED_MODEL_HAO						 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_hao_02"))					BREAK
		CASE PED_MODEL_AUTO_SHOP_MECHANIC_M		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_Autoshop_03"))			BREAK
		CASE PED_MODEL_AUTO_SHOP_MECHANIC_F		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_F_M_Autoshop_01"))			BREAK
		CASE PED_MODEL_CAR_MEET_MALE			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_CarClub_01"))			BREAK
		CASE PED_MODEL_CAR_MEET_FEMALE			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_CarClub_01"))			BREAK
		CASE PED_MODEL_CAR_MEET_JACKER_MALE		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))			BREAK
		CASE PED_MODEL_CAR_MEET_JACKER_FEMALE	 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))			BREAK
		CASE PED_MODEL_SESSANTA					 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("IG_Sessanta"))					BREAK
		CASE PED_MODEL_BENNY					 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("IG_Benny_02"))					BREAK
		CASE PED_MODEL_BENNYS_MECHANIC			 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("MP_F_BENNYMECH_01"))			BREAK
		CASE PED_MODEL_JOHNNY_JONES				 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_Y_XMECH_01"))				BREAK
		CASE PED_MODEL_LSC_MECHANIC				 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_AUTOSHOP_01"))			BREAK
		CASE PED_MODEL_WAREHOUSE_MECHANIC		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_WAREMECH_01"))			BREAK
		CASE PED_MODEL_CAR_MEET_MOODYMANN		 eModel = INT_TO_ENUM(MODEL_NAMES, HASH("IG_Moodyman_02"))				BREAK
		//MUSIC STUDIO
		CASE PED_MODEL_FIXER_STUDIO_RECEPTIONIST eModel =INT_TO_ENUM(MODEL_NAMES, HASH("S_F_Y_ClubBar_01"))				BREAK
		CASE PED_MODEL_FIXER_JIMMY				 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("IG_JIO_02"))					BREAK
		CASE PED_MODEL_FIXER_DJPOOH				 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("IG_MJO_02"))					BREAK
		CASE PED_MODEL_FIXER_MUSICGIRL			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_BevHills_01"))			BREAK
		CASE PED_MODEL_FIXER_ENGINEER			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("IG_SoundEng_00"))				BREAK
		CASE PED_MODEL_FIXER_ENTOURAGE_1		 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("IG_Entourage_A"))				BREAK
		CASE PED_MODEL_FIXER_ENTOURAGE_2		 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("IG_Entourage_B"))				BREAK
		CASE PED_MODEL_FIXER_MUSICIAN_1			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("IG_Musician_00"))				BREAK
		CASE PED_MODEL_FIXER_STUDIO_ASSISTANT_1	 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("S_F_M_StudioAssist_01"))		BREAK
		CASE PED_MODEL_FIXER_STUDIO_ASSISTANT_2	 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_StudioAssist_02"))		BREAK
		CASE PED_MODEL_FIXER_STUDIO_SOUND_ENG	 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_StudioSouEng_02"))		BREAK
		CASE PED_MODEL_FIXER_STUDIO_PRODUCER     eModel =INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_StudioProd_01"))			BREAK
		//FIXER HQ
		CASE PED_MODEL_FIXER_HQ_RECEPTIONIST	 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_Business_04"))			BREAK
		CASE PED_MODEL_FIXER_HQ_SECURITY		 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("ig_security_a"))				BREAK
		CASE PED_MODEL_FIXER_HQ_GUARD		 	 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("s_m_m_highsec_05"))				BREAK
		
		CASE PED_MODEL_FIXER_FRANKLIN			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("p_franklin_02"))				BREAK
		CASE PED_MODEL_FIXER_LAMAR				 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("IG_LamarDavis_02"))				BREAK		
		CASE PED_MODEL_FIXER_OFFICE_MALE 		 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_STUDIOASSIST_02"))		BREAK
		CASE PED_MODEL_FIXER_OFFICE_FEMALE_1	 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_SOUCENT_02"))				BREAK
		CASE PED_MODEL_FIXER_OFFICE_FEMALE_2	 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("a_f_y_vinewood_01"))			BREAK
		CASE PED_MODEL_FIXER_IMANI				 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("IG_Imani"))						BREAK
		CASE PED_MODEL_FIXER_DR_DRE				 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("IG_ARY_02"))					BREAK
		#IF FEATURE_DLC_1_2022
		CASE PED_MODEL_NIGHTCLUB_MALE_1			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_ClubCust_01"))			BREAK
		CASE PED_MODEL_NIGHTCLUB_MALE_2			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_ClubCust_03"))			BREAK
		CASE PED_MODEL_NIGHTCLUB_MALE_3			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_ClubCust_02"))			BREAK
		CASE PED_MODEL_CLUBHOUSE_MALE			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("a_m_m_genbiker_01"))			BREAK
		CASE PED_MODEL_CLUBHOUSE_FEMALE			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("a_f_m_genbiker_01"))			BREAK
		CASE PED_MODEL_NIGHTCLUB_VIP_1			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("a_m_y_gencaspat_01"))			BREAK
		CASE PED_MODEL_NIGHTCLUB_VIP_2			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_StBla_02"))				BREAK
		CASE PED_MODEL_NIGHTCLUB_VIP_3			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("a_m_y_vinewood_01"))			BREAK
		CASE PED_MODEL_NIGHTCLUB_VIP_4			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("a_m_y_smartcaspat_01"))			BREAK
		CASE PED_MODEL_WAREHOUSE_BOSS			 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("IG_WarehouseBoss"))				BREAK	
		CASE PED_MODEL_WAREHOUSE_M				 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_Warehouse_01"))			BREAK	
		CASE PED_MODEL_WAREHOUSE_F				 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("S_F_M_Warehouse_01"))			BREAK	
		#ENDIF
		#IF FEATURE_DLC_2_2022
		CASE PED_MODEL_JUGGALO_HIDEOUT_MALE		 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("a_m_y_juggalo_01"))				BREAK	
		CASE PED_MODEL_JUGGALO_HIDEOUT_FEMALE	 eModel =INT_TO_ENUM(MODEL_NAMES, HASH("a_f_y_juggalo_01"))				BREAK	
		#ENDIF
	ENDSWITCH
	RETURN eModel
ENDFUNC	

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ COMMON DEBUG ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_PED_LOCATION_STRING(PED_LOCATIONS eLocation)
	STRING sLocation = ""
	SWITCH eLocation
		CASE PED_LOCATION_INVALID							sLocation = "PED_LOCATION_INVALID"							BREAK
		CASE PED_LOCATION_ISLAND							sLocation = "PED_LOCATION_ISLAND"							BREAK
		CASE PED_LOCATION_SUBMARINE							sLocation = "PED_LOCATION_SUBMARINE"						BREAK
		#IF FEATURE_CASINO_NIGHTCLUB
		CASE PED_LOCATION_CASINO_NIGHTCLUB					sLocation = "PED_LOCATION_CASINO_NIGHTCLUB"					BREAK
		#ENDIF
		#IF FEATURE_TUNER
		CASE PED_LOCATION_CAR_MEET							sLocation = "PED_LOCATION_CAR_MEET"							BREAK
		CASE PED_LOCATION_CAR_MEET_SANDBOX					sLocation = "PED_LOCATION_CAR_MEET_SANDBOX"					BREAK
		CASE PED_LOCATION_AUTO_SHOP							sLocation = "PED_LOCATION_AUTO_SHOP"						BREAK
		#ENDIF
		#IF FEATURE_MUSIC_STUDIO
		CASE PED_LOCATION_MUSIC_STUDIO						sLocation = "PED_LOCATION_MUSIC_STUDIO"						BREAK
		#ENDIF
		#IF FEATURE_FIXER
		CASE PED_LOCATION_FIXER_HQ_HAWICK					sLocation = "PED_LOCATION_FIXER_HQ"							BREAK
		CASE PED_LOCATION_FIXER_HQ_ROCKFORD					sLocation = "PED_LOCATION_FIXER_HQ_ROCKFORD"				BREAK
		CASE PED_LOCATION_FIXER_HQ_SEOUL					sLocation = "PED_LOCATION_FIXER_HQ_SEOUL"					BREAK
		CASE PED_LOCATION_FIXER_HQ_VESPUCCI					sLocation = "PED_LOCATION_FIXER_HQ_VESPUCCI"				BREAK
		#ENDIF
		#IF FEATURE_DLC_1_2022
		CASE PED_LOCATION_NIGHTCLUB							sLocation = "PED_LOCATION_NIGHTCLUB"						BREAK
		CASE PED_LOCATION_CLUBHOUSE							sLocation = "PED_LOCATION_CLUBHOUSE"						BREAK
		CASE PED_LOCATION_WAREHOUSE							sLocation = "PED_LOCATION_WAREHOUSE"						BREAK
		#ENDIF
		#IF FEATURE_DLC_2_2022
		CASE PED_LOCATION_JUGGALO_HIDEOUT					sLocation = "PED_LOCATION_JUGGALO_HIDEOUT"					BREAK
		#ENDIF
		#IF IS_DEBUG_BUILD
		CASE PED_LOCATION_DEBUG								sLocation = "PED_LOCATION_DEBUG"							BREAK
		#ENDIF
		CASE PED_LOCATION_TOTAL								sLocation = "PED_LOCATION_TOTAL"							BREAK
	ENDSWITCH
	RETURN sLocation
ENDFUNC

FUNC STRING DEBUG_GET_PED_SPEECH_STRING(PED_SPEECH eSpeech)
	STRING sSpeech = ""
	SWITCH eSpeech
		CASE PED_SPH_INVALID										sSpeech = "PED_SPH_INVALID"											BREAK
		CASE PED_SPH_PT_GREETING									sSpeech = "PED_SPH_PT_GREETING"										BREAK
		CASE PED_SPH_PT_BYE											sSpeech = "PED_SPH_PT_BYE"											BREAK
		CASE PED_SPH_PT_BUMP										sSpeech = "PED_SPH_PT_BUMP"											BREAK
		CASE PED_SPH_PT_LOITER										sSpeech = "PED_SPH_PT_LOITER"										BREAK
		CASE PED_SPH_PT_CHAMPAGNE									sSpeech = "PED_SPH_PT_CHAMPAGNE"									BREAK
		CASE PED_SPH_PT_WHERE_TO									sSpeech = "PED_SPH_PT_WHERE_TO"										BREAK
		CASE PED_SPH_PT_BEACH_GUARD									sSpeech = "PED_SPH_PT_BEACH_GUARD"									BREAK
		CASE PED_SPH_PT_SOURCE_CARGO_POSITIVE						sSpeech = "PED_SPH_PT_SOURCE_CARGO_POSITIVE"						BREAK
		CASE PED_SPH_PT_SOURCE_CARGO_NEGATIVE						sSpeech = "PED_SPH_PT_SOURCE_CARGO_NEGATIVE"						BREAK
		CASE PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_ACCEPT				sSpeech = "PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_ACCEPT"				BREAK
		CASE PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_REJECT				sSpeech = "PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_REJECT"				BREAK
		CASE PED_SPH_PT_TOTAL										sSpeech = "PED_SPH_PT_TOTAL"										BREAK
		CASE PED_SPH_CT_WHATS_UP									sSpeech = "PED_SPH_CT_WHATS_UP"										BREAK
		CASE PED_SPH_CT_PARTY_GUY_I_AM_FUCKED						sSpeech = "PED_SPH_CT_PARTY_GUY_I_AM_FUCKED"						BREAK
		CASE PED_SPH_CT_PARTY_GUY_TAKE_A_PISS						sSpeech = "PED_SPH_CT_PARTY_GUY_TAKE_A_PISS"						BREAK
		CASE PED_SPH_CT_PARTY_GUY_FUCK_YOU_BUDDY					sSpeech = "PED_SPH_CT_PARTY_GUY_FUCK_YOU_BUDDY"						BREAK
		CASE PED_SPH_CT_PARTY_GUY_TOO_HARD_TOO_FAST					sSpeech = "PED_SPH_CT_PARTY_GUY_TOO_HARD_TOO_FAST"					BREAK
		CASE PED_SPH_CT_PARTY_GUY_AVERAGE_SHIT						sSpeech = "PED_SPH_CT_PARTY_GUY_AVERAGE_SHIT"						BREAK
		CASE PED_SPH_CT_PARTY_GUY_MUSIC_STARTED						sSpeech = "PED_SPH_CT_PARTY_GUY_MUSIC_STARTED"						BREAK
		CASE PED_SPH_CT_PARTY_GUY_JUST_A_MINUTE						sSpeech = "PED_SPH_CT_PARTY_GUY_JUST_A_MINUTE"						BREAK
		CASE PED_SPH_CT_PARTY_GUY_TIME_OUT							sSpeech = "PED_SPH_CT_PARTY_GUY_TIME_OUT"							BREAK
		CASE PED_SPH_CT_PARTY_GUY_HE_OWNS_THE_PLACE					sSpeech = "PED_SPH_CT_PARTY_GUY_HE_OWNS_THE_PLACE"					BREAK
		CASE PED_SPH_CT_PARTY_GUY_I_AM_SO_HOT						sSpeech = "PED_SPH_CT_PARTY_GUY_I_AM_SO_HOT"						BREAK
		CASE PED_SPH_CT_PARTY_GIRL_IM_THERE_THOUGH					sSpeech = "PED_SPH_CT_PARTY_GIRL_IM_THERE_THOUGH"					BREAK
		CASE PED_SPH_CT_PARTY_GIRL_ITS_INSANE						sSpeech = "PED_SPH_CT_PARTY_GIRL_ITS_INSANE"						BREAK
		CASE PED_SPH_CT_PARTY_GIRL_PRONTO							sSpeech = "PED_SPH_CT_PARTY_GIRL_PRONTO"							BREAK
		CASE PED_SPH_CT_PARTY_COUPLE_GUY_SAW_IT_WHEN_FLYING_IN		sSpeech = "PED_SPH_CT_PARTY_COUPLE_GUY_SAW_IT_WHEN_FLYING_IN"		BREAK
		CASE PED_SPH_CT_PARTY_COUPLE_GUY_LIKE_REALLY				sSpeech = "PED_SPH_CT_PARTY_COUPLE_GUY_LIKE_REALLY"					BREAK
		CASE PED_SPH_CT_PARTY_COUPLE_GUY_LS_IN_NO_TIME				sSpeech = "PED_SPH_CT_PARTY_COUPLE_GUY_LS_IN_NO_TIME"				BREAK
		CASE PED_SPH_CT_SCOTT_IG3_DEFINITELY_NOT_GETTING			sSpeech = "PED_SPH_CT_SCOTT_IG3_DEFINITELY_NOT_GETTING"				BREAK
		CASE PED_SPH_CT_SCOTT_IG3_GOOD_JOB_MY_MANAGER				sSpeech = "PED_SPH_CT_SCOTT_IG3_GOOD_JOB_MY_MANAGER"				BREAK
		CASE PED_SPH_CT_SCOTT_IG3_I_SHOULDDA_BROUGHT				sSpeech = "PED_SPH_CT_SCOTT_IG3_I_SHOULDDA_BROUGHT"					BREAK
		CASE PED_SPH_CT_SCOTT_IG3_SEEN_A_FEW_PRIVATE				sSpeech = "PED_SPH_CT_SCOTT_IG3_SEEN_A_FEW_PRIVATE"					BREAK
		CASE PED_SPH_CT_SCOTT_IG3_SERIOUS_PLACE						sSpeech = "PED_SPH_CT_SCOTT_IG3_SERIOUS_PLACE"						BREAK
		CASE PED_SPH_CT_SCOTT_IG3_THIS_RUBIO_GUY					sSpeech = "PED_SPH_CT_SCOTT_IG3_THIS_RUBIO_GUY"						BREAK
		CASE PED_SPH_CT_SCOTT_IG3_YOU_KNOW_THE_OWNER				sSpeech = "PED_SPH_CT_SCOTT_IG3_YOU_KNOW_THE_OWNER"					BREAK
		CASE PED_SPH_CT_SCOTT_IG3_NICE_TUNES						sSpeech = "PED_SPH_CT_SCOTT_IG3_NICE_TUNES"							BREAK
		CASE PED_SPH_CT_SCOTT_IG4_AFTER_PARTY						sSpeech = "PED_SPH_CT_SCOTT_IG4_AFTER_PARTY"						BREAK
		CASE PED_SPH_CT_SCOTT_IG4_BACK_IN_THE_GAME					sSpeech = "PED_SPH_CT_SCOTT_IG4_BACK_IN_THE_GAME"					BREAK
		CASE PED_SPH_CT_SCOTT_IG4_BACK_WHERE_I_BELONG				sSpeech = "PED_SPH_CT_SCOTT_IG4_BACK_WHERE_I_BELONG"				BREAK
		CASE PED_SPH_CT_SCOTT_IG4_DONT_WORRY_BABE					sSpeech = "PED_SPH_CT_SCOTT_IG4_DONT_WORRY_BABE"					BREAK
		CASE PED_SPH_CT_SCOTT_IG4_GO_AHEAD_GIRL						sSpeech = "PED_SPH_CT_SCOTT_IG4_GO_AHEAD_GIRL"						BREAK
		CASE PED_SPH_CT_SCOTT_IG4_I_KNOW_YOU_GIRL					sSpeech = "PED_SPH_CT_SCOTT_IG4_I_KNOW_YOU_GIRL"					BREAK
		CASE PED_SPH_CT_SCOTT_IG4_LIKE_I_NEVER_LEFT					sSpeech = "PED_SPH_CT_SCOTT_IG4_LIKE_I_NEVER_LEFT"					BREAK
		CASE PED_SPH_CT_SCOTT_IG4_THIS_IS_WHAT_A_COMEBACK			sSpeech = "PED_SPH_CT_SCOTT_IG4_THIS_IS_WHAT_A_COMEBACK"			BREAK
		CASE PED_SPH_CT_SCOTT_IG4_WHAT_HAPPENS_IN_PARADISE			sSpeech = "PED_SPH_CT_SCOTT_IG4_WHAT_HAPPENS_IN_PARADISE"			BREAK
		CASE PED_SPH_CT_SCOTT_IG4_YOU_SEE_HOW_HEAVY					sSpeech = "PED_SPH_CT_SCOTT_IG4_YOU_SEE_HOW_HEAVY"					BREAK
		CASE PED_SPH_CT_SCOTT_IG4_YOU_WAIT_AND_SEE					sSpeech = "PED_SPH_CT_SCOTT_IG4_YOU_WAIT_AND_SEE"					BREAK
		CASE PED_SPH_CT_DAVE_IG5_CAN_CAN_IN_HERE					sSpeech = "PED_SPH_CT_DAVE_IG5_CAN_CAN_IN_HERE"						BREAK
		CASE PED_SPH_CT_DAVE_IG5_DAVEY_IS_THE_LIFE_AND_SOUL			sSpeech = "PED_SPH_CT_DAVE_IG5_DAVEY_IS_THE_LIFE_AND_SOUL"			BREAK
		CASE PED_SPH_CT_DAVE_IG5_MAN_OF_THE_MATCH_RIGHT_HERE		sSpeech = "PED_SPH_CT_DAVE_IG5_MAN_OF_THE_MATCH_RIGHT_HERE"			BREAK
		CASE PED_SPH_CT_DAVE_IG5_NICE_ONE_NICE_ONE					sSpeech = "PED_SPH_CT_DAVE_IG5_NICE_ONE_NICE_ONE"					BREAK
		CASE PED_SPH_CT_DAVE_IG5_NUDGE_NUDGE_WINK_WINK				sSpeech = "PED_SPH_CT_DAVE_IG5_NUDGE_NUDGE_WINK_WINK"				BREAK
		CASE PED_SPH_CT_DAVE_IG5_RIGHT_OUT_OF_THE_GATE				sSpeech = "PED_SPH_CT_DAVE_IG5_RIGHT_OUT_OF_THE_GATE"				BREAK
		CASE PED_SPH_CT_DAVE_IG5_RIGHT_UP_MY_ALLEY					sSpeech = "PED_SPH_CT_DAVE_IG5_RIGHT_UP_MY_ALLEY"					BREAK
		CASE PED_SPH_CT_DAVE_IG5_THIS_TUNE_HAS_THE_SAME_BPM			sSpeech = "PED_SPH_CT_DAVE_IG5_THIS_TUNE_HAS_THE_SAME_BPM"			BREAK
		CASE PED_SPH_CT_DAVE_IG5_WELLY_ON_THE_DECKS					sSpeech = "PED_SPH_CT_DAVE_IG5_WELLY_ON_THE_DECKS"					BREAK
		CASE PED_SPH_CT_DAVE_IG6_I_KNOW_WHAT_YOURE_THINKING			sSpeech = "PED_SPH_CT_DAVE_IG6_I_KNOW_WHAT_YOURE_THINKING"			BREAK
		CASE PED_SPH_CT_DAVE_IG6_I_LOST_IT							sSpeech = "PED_SPH_CT_DAVE_IG6_I_LOST_IT"							BREAK
		CASE PED_SPH_CT_DAVE_IG6_IS_IT_ME_OR_IS_IT_GETTING_FRUITY	sSpeech = "PED_SPH_CT_DAVE_IG6_IS_IT_ME_OR_IS_IT_GETTING_FRUITY"	BREAK
		CASE PED_SPH_CT_DAVE_IG6_LEAN_INTO_IT						sSpeech = "PED_SPH_CT_DAVE_IG6_LEAN_INTO_IT"						BREAK
		CASE PED_SPH_CT_DAVE_IG6_ONLY_A_LITTLE_KERFUFFLE			sSpeech = "PED_SPH_CT_DAVE_IG6_ONLY_A_LITTLE_KERFUFFLE"				BREAK
		CASE PED_SPH_CT_DAVE_IG6_SKEW_WHIFF_COMPADRE				sSpeech = "PED_SPH_CT_DAVE_IG6_SKEW_WHIFF_COMPADRE"					BREAK
		CASE PED_SPH_CT_DAVE_IG6_SOMEBODY_CHECK_IN					sSpeech = "PED_SPH_CT_DAVE_IG6_SOMEBODY_CHECK_IN"					BREAK
		CASE PED_SPH_CT_DAVE_IG6_WERE_YOU_GONE						sSpeech = "PED_SPH_CT_DAVE_IG6_WERE_YOU_GONE"						BREAK
		CASE PED_SPH_CT_DAVE_IG6_WOBBLY_AVOIDED						sSpeech = "PED_SPH_CT_DAVE_IG6_WOBBLY_AVOIDED"						BREAK
		CASE PED_SPH_CT_DAVE_IG6_WORD_THE_WISE						sSpeech = "PED_SPH_CT_DAVE_IG6_WORD_THE_WISE"						BREAK
		CASE PED_SPH_CT_DAVE_IG6_YOURE_BACK							sSpeech = "PED_SPH_CT_DAVE_IG6_YOURE_BACK"							BREAK
		CASE PED_SPH_CT_DAVE_IG7_FULLY_PROJECTED					sSpeech = "PED_SPH_CT_DAVE_IG7_FULLY_PROJECTED"						BREAK
		CASE PED_SPH_CT_DAVE_IG7_I_AM_AT_ONE						sSpeech = "PED_SPH_CT_DAVE_IG7_I_AM_AT_ONE"							BREAK
		CASE PED_SPH_CT_DAVE_IG7_I_AM_HERBY_DISEMBALMED				sSpeech = "PED_SPH_CT_DAVE_IG7_I_AM_HERBY_DISEMBALMED"				BREAK
		CASE PED_SPH_CT_DAVE_IG7_I_HAVE_MAPPED						sSpeech = "PED_SPH_CT_DAVE_IG7_I_HAVE_MAPPED"						BREAK
		CASE PED_SPH_CT_DAVE_IG7_NAMASTE							sSpeech = "PED_SPH_CT_DAVE_IG7_NAMASTE"								BREAK
		CASE PED_SPH_CT_DAVE_IG7_OMMMMMMMM							sSpeech = "PED_SPH_CT_DAVE_IG7_OMMMMMMMM"							BREAK
		CASE PED_SPH_CT_DAVE_IG7_ONE_LOVE							sSpeech = "PED_SPH_CT_DAVE_IG7_ONE_LOVE"							BREAK
		CASE PED_SPH_CT_DAVE_IG7_PERFECT_ALIGNMENT					sSpeech = "PED_SPH_CT_DAVE_IG7_PERFECT_ALIGNMENT"					BREAK
		CASE PED_SPH_CT_DAVE_IG7_SUIVEZ_MOI							sSpeech = "PED_SPH_CT_DAVE_IG7_SUIVEZ_MOI"							BREAK
		CASE PED_SPH_CT_DAVE_IG7_THOSE_WERE_MY_LAST_WORDS			sSpeech = "PED_SPH_CT_DAVE_IG7_THOSE_WERE_MY_LAST_WORDS"			BREAK
		CASE PED_SPH_CT_ELRUBIO_IG1_EMPECEMOS_CON_ESTO				sSpeech = "PED_SPH_CT_ELRUBIO_IG1_EMPECEMOS_CON_ESTO"				BREAK
		CASE PED_SPH_CT_ELRUBIO_IG1_MANOS_A_LA_OBRA					sSpeech = "PED_SPH_CT_ELRUBIO_IG1_MANOS_A_LA_OBRA"					BREAK
		CASE PED_SPH_CT_ELRUBIO_IG1_MUY_BIEN						sSpeech = "PED_SPH_CT_ELRUBIO_IG1_MUY_BIEN"							BREAK
		CASE PED_SPH_CT_ELRUBIO_IG1_VAMONOS							sSpeech = "PED_SPH_CT_ELRUBIO_IG1_VAMONOS"							BREAK
		CASE PED_SPH_CT_ELRUBIO_IG2_STAY_AS_LONG_AS_YOU_LIKE		sSpeech = "PED_SPH_CT_ELRUBIO_IG2_STAY_AS_LONG_AS_YOU_LIKE"			BREAK
		CASE PED_SPH_CT_ELRUBIO_IG2_DAVEY_IS_ENJOYING_HIMSELF		sSpeech = "PED_SPH_CT_ELRUBIO_IG2_DAVEY_IS_ENJOYING_HIMSELF"		BREAK
		CASE PED_SPH_CT_ELRUBIO_IG2_GO_ON_ENJOY_YOURSELF			sSpeech = "PED_SPH_CT_ELRUBIO_IG2_GO_ON_ENJOY_YOURSELF"				BREAK
		CASE PED_SPH_CT_ELRUBIO_IG2_INCREDIBLE_SET_RIGHT			sSpeech = "PED_SPH_CT_ELRUBIO_IG2_INCREDIBLE_SET_RIGHT"				BREAK
		CASE PED_SPH_CT_ELRUBIO_IG2_I_WILL_GET_BACK_ON_THE_FLOOR	sSpeech = "PED_SPH_CT_ELRUBIO_IG2_I_WILL_GET_BACK_ON_THE_FLOOR"		BREAK
		CASE PED_SPH_CT_ELRUBIO_IG2_JUST_TAKING_A_BREAK				sSpeech = "PED_SPH_CT_ELRUBIO_IG2_JUST_TAKING_A_BREAK"				BREAK
		CASE PED_SPH_CT_ELRUBIO_IG2_NEVER_SEEN_ANYTHING_LIKE_IT		sSpeech = "PED_SPH_CT_ELRUBIO_IG2_NEVER_SEEN_ANYTHING_LIKE_IT"		BREAK
		CASE PED_SPH_CT_ELRUBIO_IG2_YOU_HAVING_FUN					sSpeech = "PED_SPH_CT_ELRUBIO_IG2_YOU_HAVING_FUN"					BREAK
		CASE PED_SPH_CT_ELRUBIO_IG2_YOU_NEED_ANYTHING				sSpeech = "PED_SPH_CT_ELRUBIO_IG2_YOU_NEED_ANYTHING"				BREAK
		CASE PED_SPH_CT_ELRUBIO_IG2_YOU_WOULDNT_WANT_TO_MISS		sSpeech = "PED_SPH_CT_ELRUBIO_IG2_YOU_WOULDNT_WANT_TO_MISS"			BREAK
		CASE PED_SPH_CT_CASINO_NIGHTCLUB_DJ							sSpeech = "PED_SPH_CT_CASINO_NIGHTCLUB_DJ"							BREAK
		CASE PED_SPH_CT_PAVEL_IG1_SCREENS_PIECE_OF_SHIT				sSpeech = "PED_SPH_CT_PAVEL_IG1_SCREENS_PIECE_OF_SHIT"				BREAK
		CASE PED_SPH_CT_PAVEL_IG1_SCREENS_FUCKING_SCREENS			sSpeech = "PED_SPH_CT_PAVEL_IG1_SCREENS_FUCKING_SCREENS"			BREAK
		CASE PED_SPH_CT_PAVEL_IG1_SCREENS_SIX_DEGREES				sSpeech = "PED_SPH_CT_PAVEL_IG1_SCREENS_SIX_DEGREES"				BREAK
		CASE PED_SPH_CT_PAVEL_IG1_SCREENS_BIDE_YOUR_TIME			sSpeech = "PED_SPH_CT_PAVEL_IG1_SCREENS_BIDE_YOUR_TIME"				BREAK
		CASE PED_SPH_CT_PAVEL_IG1_SCREENS_WONDER_WHAT_THAT_MEANS	sSpeech = "PED_SPH_CT_PAVEL_IG1_SCREENS_WONDER_WHAT_THAT_MEANS"		BREAK
		CASE PED_SPH_CT_PAVEL_IG1_SCREENS_BEEP_TO_YOU_TOO			sSpeech = "PED_SPH_CT_PAVEL_IG1_SCREENS_BEEP_TO_YOU_TOO"			BREAK
		CASE PED_SPH_CT_PAVEL_IG1_SCREENS_SURE_ITS_NOTHING			sSpeech = "PED_SPH_CT_PAVEL_IG1_SCREENS_SURE_ITS_NOTHING"			BREAK
		CASE PED_SPH_CT_PAVEL_IG1_SCREENS_WHY_LETTER_SO_SMALL		sSpeech = "PED_SPH_CT_PAVEL_IG1_SCREENS_WHY_LETTER_SO_SMALL"		BREAK
		CASE PED_SPH_CT_PAVEL_IG1_SCREENS_PREFER_WHEN_IT_WAS_TV		sSpeech = "PED_SPH_CT_PAVEL_IG1_SCREENS_PREFER_WHEN_IT_WAS_TV"		BREAK
		CASE PED_SPH_CT_PAVEL_IG1_SCREENS_FUCKING_BRIDGE			sSpeech = "PED_SPH_CT_PAVEL_IG1_SCREENS_FUCKING_BRIDGE"				BREAK
		CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_OH_YES					sSpeech = "PED_SPH_CT_PAVEL_IG2_BUTTONS_OH_YES"						BREAK
		CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_CRUSH_DEPTH				sSpeech = "PED_SPH_CT_PAVEL_IG2_BUTTONS_CRUSH_DEPTH"				BREAK
		CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_A_LITTLE_OF_THIS_ONE		sSpeech = "PED_SPH_CT_PAVEL_IG2_BUTTONS_A_LITTLE_OF_THIS_ONE"		BREAK
		CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_BEAUTIFUL					sSpeech = "PED_SPH_CT_PAVEL_IG2_BUTTONS_BEAUTIFUL"					BREAK
		CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_SPEAK_TO_ME				sSpeech = "PED_SPH_CT_PAVEL_IG2_BUTTONS_SPEAK_TO_ME"				BREAK
		CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_I_WILL_FIX_IT				sSpeech = "PED_SPH_CT_PAVEL_IG2_BUTTONS_I_WILL_FIX_IT"				BREAK
		CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_BEARING_TWO_ONE_ZONE		sSpeech = "PED_SPH_CT_PAVEL_IG2_BUTTONS_BEARING_TWO_ONE_ZONE"		BREAK
		CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_FLUSH_THE_TANK			sSpeech = "PED_SPH_CT_PAVEL_IG2_BUTTONS_FLUSH_THE_TANK"				BREAK
		CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_AND_SILENT				sSpeech = "PED_SPH_CT_PAVEL_IG2_BUTTONS_AND_SILENT"					BREAK
		CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_BROUGHT_MY_WRENCH			sSpeech = "PED_SPH_CT_PAVEL_IG2_BUTTONS_BROUGHT_MY_WRENCH"			BREAK
		CASE PED_SPH_CT_PAVEL_IG6_ENGINE_PERFECT					sSpeech = "PED_SPH_CT_PAVEL_IG6_ENGINE_PERFECT"						BREAK
		CASE PED_SPH_CT_PAVEL_IG6_ENGINE_GENTLY						sSpeech = "PED_SPH_CT_PAVEL_IG6_ENGINE_GENTLY"						BREAK
		CASE PED_SPH_CT_PAVEL_IG6_ENGINE_DONT_DO_THIS				sSpeech = "PED_SPH_CT_PAVEL_IG6_ENGINE_DONT_DO_THIS"				BREAK
		CASE PED_SPH_CT_PAVEL_IG6_ENGINE_I_AM_PATIENT				sSpeech = "PED_SPH_CT_PAVEL_IG6_ENGINE_I_AM_PATIENT"				BREAK
		CASE PED_SPH_CT_PAVEL_IG6_ENGINE_PLAY_NICE					sSpeech = "PED_SPH_CT_PAVEL_IG6_ENGINE_PLAY_NICE"					BREAK
		CASE PED_SPH_CT_PAVEL_IG6_ENGINE_UGH_I_AM_SORRY				sSpeech = "PED_SPH_CT_PAVEL_IG6_ENGINE_UGH_I_AM_SORRY"				BREAK
		CASE PED_SPH_CT_PAVEL_IG6_ENGINE_FINE						sSpeech = "PED_SPH_CT_PAVEL_IG6_ENGINE_FINE"						BREAK
		CASE PED_SPH_CT_PAVEL_IG6_ENGINE_I_WARNED_YOU				sSpeech = "PED_SPH_CT_PAVEL_IG6_ENGINE_I_WARNED_YOU"				BREAK
		CASE PED_SPH_CT_PAVEL_IG6_ENGINE_DONT_TALK_BACK				sSpeech = "PED_SPH_CT_PAVEL_IG6_ENGINE_DONT_TALK_BACK"				BREAK
		CASE PED_SPH_CT_PAVEL_IG6_ENGINE_IF_I_HAVE_TO_COMEDOWN		sSpeech = "PED_SPH_CT_PAVEL_IG6_ENGINE_IF_I_HAVE_TO_COMEDOWN"		BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_ACTOR				sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_ACTOR"				BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_AUNTIE				sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_AUNTIE"				BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_CHOP				sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_CHOP"				BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_EYEDROPS			sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_EYEDROPS"			BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FAKE				sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FAKE"				BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FOOD				sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FOOD"				BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_MOON				sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_MOON"				BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_PHONE				sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_PHONE"				BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SIMEON				sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SIMEON"				BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SUPERSTONED		sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SUPERSTONED"			BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_TONYA				sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_TONYA"				BREAK
		CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_WEAK				sSpeech = "PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_WEAK"				BREAK
		CASE PED_SPH_CT_TOTAL										sSpeech = "PED_SPH_CT_TOTAL"										BREAK
		CASE PED_SPH_TOTAL											sSpeech = "PED_SPH_TOTAL"											BREAK
		CASE PED_SPH_LISTEN_DISTANCE								sSpeech = "PED_SPH_LISTEN_DISTANCE"									BREAK
	ENDSWITCH
	RETURN sSpeech
ENDFUNC

FUNC STRING DEBUG_GET_PED_MODEL_STRING(PED_MODELS eModel)
	STRING sModel = ""
	SWITCH eModel
		CASE PED_MODEL_INVALID								sModel = "PED_MODEL_INVALID"								BREAK
		CASE PED_MODEL_HIPSTER_MALE_01						sModel = "PED_MODEL_HIPSTER_MALE_01"						BREAK
		CASE PED_MODEL_HIPSTER_MALE_02						sModel = "PED_MODEL_HIPSTER_MALE_02"						BREAK
		CASE PED_MODEL_HIPSTER_MALE_03						sModel = "PED_MODEL_HIPSTER_MALE_03"						BREAK
		CASE PED_MODEL_HIPSTER_MALE_04						sModel = "PED_MODEL_HIPSTER_MALE_04"						BREAK
		CASE PED_MODEL_HIPSTER_FEMALE_01					sModel = "PED_MODEL_HIPSTER_FEMALE_01"						BREAK
		CASE PED_MODEL_HIPSTER_FEMALE_02					sModel = "PED_MODEL_HIPSTER_FEMALE_02"						BREAK
		CASE PED_MODEL_HIPSTER_FEMALE_03					sModel = "PED_MODEL_HIPSTER_FEMALE_03"						BREAK
		CASE PED_MODEL_HIPSTER_FEMALE_04					sModel = "PED_MODEL_HIPSTER_FEMALE_04"						BREAK
		CASE PED_MODEL_BEACH_MALE_01						sModel = "PED_MODEL_BEACH_MALE_01"							BREAK
		CASE PED_MODEL_BEACH_MALE_02						sModel = "PED_MODEL_BEACH_MALE_02"							BREAK
		CASE PED_MODEL_BEACH_FEMALE_01						sModel = "PED_MODEL_BEACH_FEMALE_01"						BREAK
		CASE PED_MODEL_HIGH_SECURITY_01						sModel = "PED_MODEL_HIGH_SECURITY_01"						BREAK
		CASE PED_MODEL_RECEPTIONIST							sModel = "PED_MODEL_RECEPTIONIST"							BREAK
		CASE PED_MODEL_SOLOMUN								sModel = "PED_MODEL_SOLOMUN"								BREAK
		CASE PED_MODEL_PAVEL								sModel = "PED_MODEL_PAVEL"									BREAK
		CASE PED_MODEL_KAYLEE								sModel = "PED_MODEL_KAYLEE"									BREAK
		CASE PED_MODEL_BODYGUARD							sModel = "PED_MODEL_BODYGUARD"								BREAK
		CASE PED_MODEL_PATRICIA								sModel = "PED_MODEL_PATRICIA"								BREAK
		CASE PED_MODEL_MIGUEL								sModel = "PED_MODEL_MIGUEL"									BREAK
		CASE PED_MODEL_JACKIE								sModel = "PED_MODEL_JACKIE"									BREAK
		CASE PED_MODEL_KEINEMUSIK_RAMPA						sModel = "PED_MODEL_KEINEMUSIK_RAMPA"						BREAK
		CASE PED_MODEL_KEINEMUSIK_ME						sModel = "PED_MODEL_KEINEMUSIK_ME"							BREAK
		CASE PED_MODEL_KEINEMUSIK_ADAM						sModel = "PED_MODEL_KEINEMUSIK_ADAM"						BREAK
		CASE PED_MODEL_PALMS_TRAX							sModel = "PED_MODEL_PALMS_TRAX"								BREAK
		CASE PED_MODEL_MOODYMANN							sModel = "PED_MODEL_MOODYMANN"								BREAK
		CASE PED_MODEL_KEINEMUSIK_DANC_01					sModel = "PED_MODEL_KEINEMUSIK_DANC_01"						BREAK
		CASE PED_MODEL_KEINEMUSIK_DANC_02					sModel = "PED_MODEL_KEINEMUSIK_DANC_02"						BREAK
		CASE PED_MODEL_MOODYMANN_DANC_01					sModel = "PED_MODEL_MOODYMANN_DANC_01"						BREAK
		CASE PED_MODEL_MOODYMANN_DANC_02					sModel = "PED_MODEL_MOODYMANN_DANC_02"						BREAK
		CASE PED_MODEL_MOODYMANN_DANC_03					sModel = "PED_MODEL_MOODYMANN_DANC_03"						BREAK
		CASE PED_MODEL_CLUB_BOUNCERS						sModel = "PED_MODEL_CLUB_BOUNCERS"							BREAK
		CASE PED_MODEL_ISLAND_GUARDS						sModel = "PED_MODEL_ISLAND_GUARDS"							BREAK
		CASE PED_MODEL_ELRUBIO								sModel = "PED_MODEL_ELRUBIO"								BREAK
		CASE PED_MODEL_SCOTT								sModel = "PED_MODEL_SCOTT"									BREAK
		CASE PED_MODEL_DAVE									sModel = "PED_MODEL_DAVE"									BREAK
		CASE PED_MODEL_HAO							    	sModel = "PED_MODEL_HAO"									BREAK
		CASE PED_MODEL_BENNY							    sModel = "PED_MODEL_BENNY"									BREAK
		CASE PED_MODEL_JOHNNY_JONES						  	sModel = "PED_MODEL_JOHNNY_JONES"							BREAK
		CASE PED_MODEL_LSC_MECHANIC							sModel = "PED_MODEL_LSC_MECHANIC"							BREAK
		CASE PED_MODEL_WAREHOUSE_MECHANIC					sModel = "PED_MODEL_WAREHOUSE_MECHANIC"						BREAK
		CASE PED_MODEL_BENNYS_MECHANIC						sModel = "PED_MODEL_BENNYS_MECHANIC"						BREAK
		CASE PED_MODEL_AUTO_SHOP_MECHANIC_M					sModel = "PED_MODEL_AUTO_SHOP_MECHANIC_M"					BREAK
		CASE PED_MODEL_AUTO_SHOP_MECHANIC_F					sModel = "PED_MODEL_AUTO_SHOP_MECHANIC_F"					BREAK
		CASE PED_MODEL_CAR_MEET_MALE						sModel = "PED_MODEL_CAR_MEET_MALE"							BREAK
		CASE PED_MODEL_CAR_MEET_FEMALE						sModel = "PED_MODEL_CAR_MEET_FEMALE"						BREAK
		CASE PED_MODEL_CAR_MEET_JACKER_MALE					sModel = "PED_MODEL_CAR_MEET_JACKER_MALE"					BREAK
		CASE PED_MODEL_CAR_MEET_JACKER_FEMALE				sModel = "PED_MODEL_CAR_MEET_JACKER_FEMALE"					BREAK
		CASE PED_MODEL_SESSANTA						    	sModel = "PED_MODEL_SESSANTA"								BREAK
		CASE PED_MODEL_CAR_MEET_MOODYMANN					sModel = "PED_MODEL_CAR_MEET_MOODYMANN"						BREAK
		CASE PED_MODEL_FIXER_STUDIO_RECEPTIONIST			sModel = "PED_MODEL_FIXER_STUDIO_RECEPTIONIST"				BREAK
		CASE PED_MODEL_FIXER_STUDIO_ASSISTANT_1				sModel = "PED_MODEL_FIXER_STUDIO_ASSISTANT_1"				BREAK
		CASE PED_MODEL_FIXER_STUDIO_ASSISTANT_2				sModel = "PED_MODEL_FIXER_STUDIO_ASSISTANT_2"				BREAK
		CASE PED_MODEL_FIXER_STUDIO_SOUND_ENG				sModel = "PED_MODEL_FIXER_STUDIO_SOUND_ENG"					BREAK
		CASE PED_MODEL_FIXER_STUDIO_PRODUCER				sModel = "PED_MODEL_FIXER_STUDIO_PRODUCER"					BREAK
		CASE PED_MODEL_FIXER_MUSICIAN_1						sModel = "PED_MODEL_FIXER_MUSICIAN_1"						BREAK
		CASE PED_MODEL_FIXER_HQ_SECURITY					sModel = "PED_MODEL_FIXER_HQ_SECURITY"						BREAK
		CASE PED_MODEL_FIXER_HQ_GUARD						sModel = "PED_MODEL_FIXER_HQ_GUARD"							BREAK
		CASE PED_MODEL_FIXER_HQ_RECEPTIONIST				sModel = "PED_MODEL_FIXER_HQ_RECEPTIONIST"					BREAK
		CASE PED_MODEL_FIXER_FRANKLIN						sModel = "PED_MODEL_FIXER_FRANKLIN"							BREAK
		CASE PED_MODEL_FIXER_LAMAR							sModel = "PED_MODEL_FIXER_LAMAR"							BREAK
		CASE PED_MODEL_FIXER_JIMMY							sModel = "PED_MODEL_FIXER_JIMMY"							BREAK
		CASE PED_MODEL_FIXER_DJPOOH							sModel = "PED_MODEL_FIXER_DJPOOH"							BREAK
		CASE PED_MODEL_FIXER_MUSICGIRL						sModel = "PED_MODEL_FIXER_MUSICGIRL"						BREAK
		CASE PED_MODEL_FIXER_ENGINEER						sModel = "PED_MODEL_FIXER_ENGINEER"							BREAK
		CASE PED_MODEL_FIXER_ENTOURAGE_1					sModel = "PED_MODEL_FIXER_ENTOURAGE_1"						BREAK
		CASE PED_MODEL_FIXER_ENTOURAGE_2					sModel = "PED_MODEL_FIXER_ENTOURAGE_2"						BREAK
		CASE PED_MODEL_FIXER_OFFICE_MALE					sModel = "PED_MODEL_FIXER_OFFICE_MALE"						BREAK
		CASE PED_MODEL_FIXER_OFFICE_FEMALE_1				sModel = "PED_MODEL_FIXER_OFFICE_FEMALE_1"					BREAK
		CASE PED_MODEL_FIXER_OFFICE_FEMALE_2				sModel = "PED_MODEL_FIXER_OFFICE_FEMALE_2"					BREAK
		CASE PED_MODEL_FIXER_IMANI							sModel = "PED_MODEL_FIXER_IMANI"							BREAK
		CASE PED_MODEL_FIXER_DR_DRE							sModel = "PED_MODEL_FIXER_DR_DRE"							BREAK
		CASE PED_MODEL_CLUBHOUSE_MALE						sModel = "PED_MODEL_CLUBHOUSE_MALE"							BREAK
		CASE PED_MODEL_CLUBHOUSE_FEMALE						sModel = "PED_MODEL_CLUBHOUSE_FEMALE"						BREAK
		CASE PED_MODEL_NIGHTCLUB_MALE_1						sModel = "PED_MODEL_NIGHTCLUB_MALE_1"						BREAK
		CASE PED_MODEL_NIGHTCLUB_MALE_2						sModel = "PED_MODEL_NIGHTCLUB_MALE_2"						BREAK
		CASE PED_MODEL_NIGHTCLUB_MALE_3						sModel = "PED_MODEL_NIGHTCLUB_MALE_3"						BREAK
		CASE PED_MODEL_NIGHTCLUB_VIP_1						sModel = "PED_MODEL_NIGHTCLUB_VIP_1"						BREAK
		CASE PED_MODEL_NIGHTCLUB_VIP_2						sModel = "PED_MODEL_NIGHTCLUB_VIP_2"						BREAK
		CASE PED_MODEL_NIGHTCLUB_VIP_3						sModel = "PED_MODEL_NIGHTCLUB_VIP_3"						BREAK
		CASE PED_MODEL_NIGHTCLUB_VIP_4						sModel = "PED_MODEL_NIGHTCLUB_VIP_4"						BREAK
		CASE PED_MODEL_WAREHOUSE_BOSS						sModel = "PED_MODEL_WAREHOUSE_BOSS"							BREAK
		CASE PED_MODEL_WAREHOUSE_F							sModel = "PED_MODEL_WAREHOUSE_F"							BREAK
		CASE PED_MODEL_WAREHOUSE_M							sModel = "PED_MODEL_WAREHOUSE_M"							BREAK
		#IF FEATURE_DLC_2_2022
		CASE PED_MODEL_JUGGALO_HIDEOUT_MALE		 			sModel = "PED_MODEL_JUGGALO_HIDEOUT_MALE"					BREAK	
		CASE PED_MODEL_JUGGALO_HIDEOUT_FEMALE				sModel = "PED_MODEL_JUGGALO_HIDEOUT_MALE"					BREAK	
		#ENDIF
		CASE PED_MODEL_TOTAL								sModel = "PED_MODEL_TOTAL"									BREAK
		
	ENDSWITCH
	RETURN sModel
ENDFUNC

FUNC STRING DEBUG_GET_PED_AREA_STRING(PED_AREAS eArea)
	STRING sArea = ""
	SWITCH eArea
		CASE PED_AREA_INVALID								sArea = "PED_AREA_INVALID"									BREAK
		CASE PED_AREA_PERMANENT								sArea = "PED_AREA_PERMANENT"								BREAK
		CASE PED_AREA_ONE									sArea = "PED_AREA_ONE"										BREAK
		CASE PED_AREA_TWO									sArea = "PED_AREA_TWO"										BREAK
		CASE PED_AREA_TOTAL									sArea = "PED_AREA_TOTAL"									BREAK
	ENDSWITCH
	RETURN sArea
ENDFUNC

FUNC STRING DEBUG_GET_PED_CULLING_STATE_STRING(PED_CULLING_STATES eState)
	STRING sState = ""
	SWITCH eState
		CASE PED_CULL_STATE_IDLE							sState = "PED_CULL_STATE_IDLE"								BREAK
		CASE PED_CULL_STATE_SET_DATA						sState = "PED_CULL_STATE_SET_DATA"							BREAK
		CASE PED_CULL_STATE_MOVE_PED						sState = "PED_CULL_STATE_MOVE_PED"							BREAK
		CASE PED_CULL_STATE_RESET_PED_DATA					sState = "PED_CULL_STATE_RESET_PED_DATA"					BREAK
		CASE PED_CULL_STATE_UNLOAD_ASSETS					sState = "PED_CULL_STATE_UNLOAD_ASSETS"						BREAK
	ENDSWITCH
	RETURN sState
ENDFUNC

FUNC STRING DEBUG_GET_PED_DANCING_TRANSITION_STATE_STRING(PED_DANCING_TRANSITION_STATES eState)
	STRING sState = ""
	SWITCH eState
		CASE PED_DANCING_TRANS_STATE_INVALID				sState = "PED_DANCING_TRANS_STATE_INVALID"					BREAK
		CASE PED_DANCING_TRANS_STATE_SET_TRANSITION			sState = "PED_DANCING_TRANS_STATE_SET_TRANSITION"			BREAK
		CASE PED_DANCING_TRANS_STATE_FINALISE_TRANSITION	sState = "PED_DANCING_TRANS_STATE_FINALISE_TRANSITION"		BREAK
		CASE PED_DANCING_TRANS_STATE_TOTAL					sState = "PED_DANCING_TRANS_STATE_TOTAL"					BREAK
	ENDSWITCH
	RETURN sState
ENDFUNC

FUNC STRING DEBUG_GET_PED_ACTIVE_STATE_STRING(PED_ACTIVE_STATES eState)
	STRING sState = ""
	SWITCH eState
		CASE PED_ACTIVE_STATE_INVALID						sState = "PED_ACTIVE_STATE_INVALID"							BREAK
		CASE PED_ACTIVE_STATE_INIT							sState = "PED_ACTIVE_STATE_INIT"							BREAK
		CASE PED_ACTIVE_STATE_UPDATE						sState = "PED_ACTIVE_STATE_UPDATE"							BREAK
		CASE PED_ACTIVE_STATE_TOTAL							sState = "PED_ACTIVE_STATE_TOTAL"							BREAK
	ENDSWITCH
	RETURN sState
ENDFUNC

FUNC STRING DEBUG_GET_PED_ACTIVITY_STRING3(PED_ACTIVITIES eActivity)
	STRING sActivity = ""
	SWITCH eActivity			
		//MUSIC STUDIO
		CASE PED_ACT_MUSIC_STUDIO_RECEPTION							sActivity = "PED_ACT_MUSIC_STUDIO_RECEPTION"						BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING						sActivity = "PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING_2					sActivity = "PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING_2"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_DRUMS						sActivity = "PED_ACT_MUSIC_STUDIO_DR_DRE_DRUMS"						BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_VOCALS						sActivity = "PED_ACT_MUSIC_STUDIO_DR_DRE_VOCALS"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRE_ABSENT		sActivity = "PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRE_ABSENT"		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING			sActivity = "PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING"			BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING_2			sActivity = "PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING_2"			BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRUMS				sActivity = "PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRUMS"			BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_VOCALS			sActivity = "PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_VOCALS"			BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_VOCALS			sActivity = "PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_VOCALS"			BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_DRUMS				sActivity = "PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_DRUMS"			BREAK
		CASE PED_ACT_MUSIC_STUDIO_KNEEL_INSPECT						sActivity = "PED_ACT_MUSIC_STUDIO_KNEEL_INSPECT"					BREAK		
		CASE PED_ACT_MUSIC_STUDIO_CLIPBOARD							sActivity = "PED_ACT_MUSIC_STUDIO_CLIPBOARD"						BREAK
		CASE PED_ACT_MUSIC_STUDIO_CROUCH_INSPECT					sActivity = "PED_ACT_MUSIC_STUDIO_CROUCH_INSPECT"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_SIT_TABLET						sActivity = "PED_ACT_MUSIC_STUDIO_SIT_TABLET"						BREAK
		CASE PED_ACT_MUSIC_STUDIO_DRINK_COFFEE						sActivity = "PED_ACT_MUSIC_STUDIO_DRINK_COFFEE"						BREAK
		CASE PED_ACT_MUSIC_STUDIO_MOVE_STUDIO_LIGHT					sActivity = "PED_ACT_MUSIC_STUDIO_MOVE_STUDIO_LIGHT"				BREAK
		CASE PED_ACT_MUSIC_STUDIO_LEAN_INSPECT						sActivity = "PED_ACT_MUSIC_STUDIO_LEAN_INSPECT"						BREAK
		CASE PED_ACT_MUSIC_STUDIO_STAND_MEDIC_TOD					sActivity = "PED_ACT_MUSIC_STUDIO_STAND_MEDIC_TOD"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_LEAN_TEXT							sActivity = "PED_ACT_MUSIC_STUDIO_LEAN_TEXT"						BREAK
		CASE PED_ACT_MUSIC_STUDIO_STANDING_CALL						sActivity = "PED_ACT_MUSIC_STUDIO_STANDING_CALL"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_SITTING_LAPTOP					sActivity = "PED_ACT_MUSIC_STUDIO_SITTING_LAPTOP"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_COUCH						sActivity = "PED_ACT_MUSIC_STUDIO_POOH_COUCH"						BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_DRINK_BAR					sActivity = "PED_ACT_MUSIC_STUDIO_POOH_DRINK_BAR"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_DRINK_SIT					sActivity = "PED_ACT_MUSIC_STUDIO_POOH_DRINK_SIT"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_STAND						sActivity = "PED_ACT_MUSIC_STUDIO_POOH_STAND"						BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_TEXT_SIT						sActivity = "PED_ACT_MUSIC_STUDIO_POOH_TEXT_SIT"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_TEXT_STAND					sActivity = "PED_ACT_MUSIC_STUDIO_POOH_TEXT_STAND"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_SIT_DRINK						sActivity = "PED_ACT_MUSIC_STUDIO_E_SIT_DRINK"						BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_SIT_LEGS_CROSSED				sActivity = "PED_ACT_MUSIC_STUDIO_E_SIT_LEGS_CROSSED"				BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_STAND_ARM_HOLD					sActivity = "PED_ACT_MUSIC_STUDIO_E_STAND_ARM_HOLD"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_STAND_ARMS_CROSSED				sActivity = "PED_ACT_MUSIC_STUDIO_E_STAND_ARMS_CROSSED"				BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_TEXTING							sActivity = "PED_ACT_MUSIC_STUDIO_E_TEXTING"						BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_STAND_DRINK						sActivity = "PED_ACT_MUSIC_STUDIO_E_STAND_DRINK"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_STAND_DRINK_CUP					sActivity = "PED_ACT_MUSIC_STUDIO_E_STAND_DRINK_CUP"				BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_WINDOW_SHOP						sActivity = "PED_ACT_MUSIC_STUDIO_E_WINDOW_SHOP"					BREAK
		CASE PED_ACT_MUSIC_STUDIO_FRANKLIN							sActivity = "PED_ACT_MUSIC_STUDIO_FRANKLIN"							BREAK
		CASE PED_ACT_MUSIC_STUDIO_LAMAR								sActivity = "PED_ACT_MUSIC_STUDIO_LAMAR"							BREAK
		CASE PED_ACT_MUSIC_STUDIO_LAMAR_VFX_PASS					sActivity = "PED_ACT_MUSIC_STUDIO_LAMAR_VFX_PASS"					BREAK
		#IF FEATURE_DLC_1_2022
		CASE PED_ACT_NIGHTCLUB_VOMIT								sActivity = "PED_ACT_NIGHTCLUB_VOMIT"								BREAK
		CASE PED_ACT_NIGHTCLUB_STADING_BUM_DRUNK					sActivity = "PED_ACT_NIGHTCLUB_STADING_BUM_DRUNK"					BREAK
		CASE PED_ACT_NIGHTCLUB_STADING_DRUNK_FIDGET					sActivity = "PED_ACT_NIGHTCLUB_STADING_DRUNK_FIDGET"				BREAK
		CASE PED_ACT_NIGHTCLUB_ARGUE_WITH_BOUNCER					sActivity = "PED_ACT_NIGHTCLUB_ARGUE_WITH_BOUNCER"					BREAK
		CASE PED_ACT_NIGHTCLUB_PASSED_OUT_UPSTAIRS_BAR				sActivity = "PED_ACT_NIGHTCLUB_PASSED_OUT_UPSTAIRS_BAR"				BREAK
		CASE PED_ACT_NIGHTCLUB_PASSED_OUT_VIP_SECTION				sActivity = "PED_ACT_NIGHTCLUB_PASSED_OUT_VIP_SECTION"				BREAK
		CASE PED_ACT_NIGHTCLUB_PASSED_OUT_OFFICE_BED				sActivity = "PED_ACT_NIGHTCLUB_PASSED_OUT_OFFICE_BED"				BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN sActivity	
ENDFUNC

FUNC STRING DEBUG_GET_PED_ACTIVITY_STRING2(PED_ACTIVITIES eActivity)
	STRING sActivity = ""
	SWITCH eActivity
			//CHECKOUT ENGINE
			//DRINK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_B			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_B"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_A		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_A"		BREAK
			//REGULAR
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_C				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_C"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_D				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_D"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_G				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_G"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_H				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_H"				BREAK
		
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_A    		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_A"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_B    		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_B"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_FEMALE_A    	sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_FEMALE_A"		BREAK
		
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A"					BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B"					BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C"					BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D"					BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E"					BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F"					BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_G					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_G"					BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_H					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_H"					BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C"				BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_D					sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_D"				BREAK
		
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_MALE_A				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_MALE_A"			BREAK
		
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_C				sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_C"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A"			BREAK
		
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A"					BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B"					BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C"					BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D"					BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E"					BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F"					BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G"					BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_H					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_H"					BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A"					BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B"					BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C"					BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_D					sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_D"					BREAK
		
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A					sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A"					BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B					sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B"					BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_C					sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_C"					BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_D 					sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_D"					BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_A					sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_A"				BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_B					sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_B"				BREAK
		
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A_TRANS_B	sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A_TRANS_B"	BREAK
	
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_B		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_B"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_C		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_C"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_D		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_D"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_E		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_E"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_F		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_F"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_G		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_G"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_H		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_H"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_C		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_C"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_D		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_D"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_E		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_E"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_F		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_F"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_G		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_G"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_H		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_H"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_D		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_D"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_E		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_E"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_F		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_F"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_G		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_G"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_H		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_H"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_E		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_E"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_F		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_F"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_G		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_G"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_H		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_H"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_F		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_F"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_G		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_G"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_H		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_H"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F_TRANS_G		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F_TRANS_G"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F_TRANS_H		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F_TRANS_H"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_G_TRANS_H		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_G_TRANS_H"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_B		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_B"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_C		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_C"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_D		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_D"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B_TRANS_C		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B_TRANS_C"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B_TRANS_D		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B_TRANS_D"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_C_TRANS_D		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_C_TRANS_D"		BREAK
		
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_B		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_B"	BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_C		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_C"	BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B_TRANS_C		sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B_TRANS_C"	BREAK
		
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_B			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_B"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_C			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_C"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_D			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_D"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_E			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_E"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_F			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_F"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_G			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_H			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_C			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_C"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_D			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_D"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_E			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_E"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_F			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_F"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_G			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_H			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_D			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_D"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_E			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_E"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_F			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_F"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_G			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_H			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_E			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_E"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_F			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_F"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_G			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_H			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_F			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_F"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_G			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_H			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F_TRANS_G			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F_TRANS_H			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_G_TRANS_H			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_G_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_B			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_B"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_C			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_C"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_D			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_D"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B_TRANS_C			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B_TRANS_C"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B_TRANS_D			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B_TRANS_D"		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C_TRANS_D			sActivity = "PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C_TRANS_D"		BREAK
		
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_B			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_B"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_C			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_C"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_D			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_D"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_E			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_E"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_F			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_F"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_G			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_H			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_C			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_C"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_D			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_D"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_E			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_E"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_F			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_F"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_G			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_H			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_D			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_D"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_E			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_E"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_F			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_F"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_G			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_H			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_E			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_E"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_F			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_F"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_G			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_H			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_F			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_F"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_G			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_H			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_G			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_G"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_H			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G_TRANS_H			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G_TRANS_H"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_B			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_B"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_C			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_C"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_D			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_D"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_C			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_C"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_D			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_D"			BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C_TRANS_D			sActivity = "PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C_TRANS_D"			BREAK
		
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_B			sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_B"			BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_C			sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_C"			BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_D			sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_D"			BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_C			sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_C"			BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_D			sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_D"			BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_C_TRANS_D			sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_C_TRANS_D"			BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_A_TRANS_B			sActivity = "PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_A_TRANS_B"		BREAK
		
		
		//FIXER HQ
		CASE PED_ACT_FIXER_HQ_RECEPTION								sActivity = "PED_ACT_FIXER_HQ_RECEPTION"							BREAK		
		CASE PED_ACT_FIXER_HQ_SECURITY_DESK							sActivity = "PED_ACT_FIXER_HQ_SECURITY_DESK"						BREAK
		CASE PED_ACT_FIXER_TEXTING									sActivity = "PED_ACT_FIXER_TEXTING"									BREAK
		CASE PED_ACT_FIXER_STAND_ON_PHONE							sActivity = "PED_ACT_FIXER_STAND_ON_PHONE"							BREAK
		CASE PED_ACT_FIXER_LEAN_ON_RAIL								sActivity = "PED_ACT_FIXER_LEAN_ON_RAIL"							BREAK
		CASE PED_ACT_FIXER_DRINK									sActivity = "PED_ACT_FIXER_DRINK"									BREAK
		
		
		CASE PED_ACT_IMANI											sActivity = "PED_ACT_IMANI"											BREAK
		CASE PED_ACT_FIXER_DESK_WORKER_MALE							sActivity = "PED_ACT_FIXER_DESK_WORKER_MALE"						BREAK
		CASE PED_ACT_FIXER_DESK_WORKER_FEMALE						sActivity = "PED_ACT_FIXER_DESK_WORKER_FEMALE"						BREAK
		CASE PED_ACT_FIXER_TALK_ON_PHONE							sActivity = "PED_ACT_FIXER_TALK_ON_PHONE"							BREAK
		CASE PED_ACT_FIXER_TALK_ON_PHONE_FEMALE						sActivity = "PED_ACT_FIXER_TALK_ON_PHONE_FEMALE"					BREAK
		
		CASE PED_ACT_FIXER_FRANKLIN_0								sActivity = "PED_ACT_FIXER_FRANKLIN_0"								BREAK
		CASE PED_ACT_FIXER_FRANKLIN_1								sActivity = "PED_ACT_FIXER_FRANKLIN_1"								BREAK
		CASE PED_ACT_FIXER_FRANKLIN_2								sActivity = "PED_ACT_FIXER_FRANKLIN_2"								BREAK
		CASE PED_ACT_FIXER_LAMAR									sActivity = "PED_ACT_FIXER_LAMAR"									BREAK
		#IF FEATURE_DLC_1_2022
		CASE PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR						sActivity = "PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR"						BREAK
		CASE PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR_B						sActivity = "PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR_B"						BREAK
		CASE PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_M					sActivity = "PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_M"					BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_M				sActivity = "PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_M"				BREAK
		CASE PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_F					sActivity = "PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_F"					BREAK
		CASE PED_ACT_CLUBHOUSE_LEAN_DRINK							sActivity = "PED_ACT_CLUBHOUSE_LEAN_DRINK"							BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_M				sActivity = "PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_M"				BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_F				sActivity = "PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_F"				BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_LEANING_PHONE_F			sActivity = "PED_ACT_CLUBHOUSE_WORLD_HUMAN_LEANING_PHONE_F"			BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_F				sActivity = "PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_F"				BREAK
		#ENDIF
	ENDSWITCH
	IF IS_STRING_NULL_OR_EMPTY(sActivity)
		RETURN DEBUG_GET_PED_ACTIVITY_STRING3(eActivity)
	ENDIF
	RETURN sActivity	
ENDFUNC
FUNC STRING DEBUG_GET_PED_ACTIVITY_STRING(PED_ACTIVITIES eActivity)
	STRING sActivity = ""
	SWITCH eActivity
		CASE PED_ACT_INVALID									sActivity = "PED_ACT_INVALID"									BREAK
		CASE PED_ACT_AMBIENT_M_01								sActivity = "PED_ACT_AMBIENT_M_01"								BREAK
		CASE PED_ACT_AMBIENT_F_01								sActivity = "PED_ACT_AMBIENT_F_01"								BREAK
		CASE PED_ACT_LEANING_TEXTING_M_01						sActivity = "PED_ACT_LEANING_TEXTING_M_01"						BREAK
		CASE PED_ACT_LEANING_TEXTING_F_01						sActivity = "PED_ACT_LEANING_TEXTING_F_01"						BREAK
		CASE PED_ACT_LEANING_SMOKING_M_01						sActivity = "PED_ACT_LEANING_SMOKING_M_01"						BREAK
		CASE PED_ACT_LEANING_SMOKING_F_01						sActivity = "PED_ACT_LEANING_SMOKING_F_01"						BREAK
		CASE PED_ACT_STANDING_SMOKING_M_01						sActivity = "PED_ACT_STANDING_SMOKING_M_01"						BREAK
		CASE PED_ACT_STANDING_SMOKING_F_01						sActivity = "PED_ACT_STANDING_SMOKING_F_01"						BREAK
		CASE PED_ACT_HANGOUT_M_01								sActivity = "PED_ACT_HANGOUT_M_01"								BREAK
		CASE PED_ACT_HANGOUT_F_01								sActivity = "PED_ACT_HANGOUT_F_01"								BREAK
		CASE PED_ACT_SMOKING_WEED_M_01							sActivity = "PED_ACT_SMOKING_WEED_M_01"							BREAK
		CASE PED_ACT_SMOKING_WEED_F_01							sActivity = "PED_ACT_SMOKING_WEED_F_01"							BREAK
		CASE PED_ACT_ON_PHONE_M_01								sActivity = "PED_ACT_ON_PHONE_M_01"								BREAK
		CASE PED_ACT_ON_PHONE_F_01								sActivity = "PED_ACT_ON_PHONE_F_01"								BREAK
		CASE PED_ACT_MUSCLE_FLEX								sActivity = "PED_ACT_MUSCLE_FLEX"								BREAK
		CASE PED_ACT_CHECK_OUT_MIRROR							sActivity = "PED_ACT_CHECK_OUT_MIRROR"							BREAK
		CASE PED_ACT_DRINKING_M_01								sActivity = "PED_ACT_DRINKING_M_01"								BREAK
		CASE PED_ACT_DRINKING_F_01								sActivity = "PED_ACT_DRINKING_F_01"								BREAK
		CASE PED_ACT_SITTING_DRINKING_M_01						sActivity = "PED_ACT_SITTING_DRINKING_M_01"						BREAK
		CASE PED_ACT_SITTING_DRINKING_F_01						sActivity = "PED_ACT_SITTING_DRINKING_F_01"						BREAK
		CASE PED_ACT_SITTING_AT_BAR_M_01						sActivity = "PED_ACT_SITTING_AT_BAR_M_01"						BREAK
		CASE PED_ACT_SITTING_AT_BAR_M_02						sActivity = "PED_ACT_SITTING_AT_BAR_M_02"						BREAK
		CASE PED_ACT_SITTING_AT_BAR_F_01						sActivity = "PED_ACT_SITTING_AT_BAR_F_01"						BREAK
		CASE PED_ACT_SITTING_AT_BAR_F_02						sActivity = "PED_ACT_SITTING_AT_BAR_F_02"						BREAK
		CASE PED_ACT_GUARD_STANDING								sActivity = "PED_ACT_GUARD_STANDING"							BREAK
		CASE PED_ACT_SUNBATHING_BACK_M_01						sActivity = "PED_ACT_SUNBATHING_BACK_M_01"						BREAK
		CASE PED_ACT_SUNBATHING_BACK_F_01						sActivity = "PED_ACT_SUNBATHING_BACK_F_01"						BREAK
		CASE PED_ACT_SUNBATHING_FRONT_M_01						sActivity = "PED_ACT_SUNBATHING_FRONT_M_01"						BREAK
		CASE PED_ACT_SUNBATHING_FRONT_F_01						sActivity = "PED_ACT_SUNBATHING_FRONT_F_01"						BREAK
		CASE PED_ACT_VIP_BAR_HANGOUT_M_01						sActivity = "PED_ACT_VIP_BAR_HANGOUT_M_01"						BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_01						sActivity = "PED_ACT_VIP_BAR_DRINKING_M_01"						BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_02						sActivity = "PED_ACT_VIP_BAR_DRINKING_M_02"						BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_03						sActivity = "PED_ACT_VIP_BAR_DRINKING_M_03"						BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_F_01						sActivity = "PED_ACT_VIP_BAR_DRINKING_F_01"						BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_F_02						sActivity = "PED_ACT_VIP_BAR_DRINKING_F_02"						BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_M_01						sActivity = "PED_ACT_BEACH_PARTY_STAND_M_01"					BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_M_02						sActivity = "PED_ACT_BEACH_PARTY_STAND_M_02"					BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_F_01						sActivity = "PED_ACT_BEACH_PARTY_STAND_F_01"					BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_M_01						sActivity = "PED_ACT_BEACH_PARTY_LEAN_M_01"						BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_M_02						sActivity = "PED_ACT_BEACH_PARTY_LEAN_M_02"						BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_F_01						sActivity = "PED_ACT_BEACH_PARTY_LEAN_F_01"						BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_M_01					sActivity = "PED_ACT_BEACH_PARTY_SEATED_M_01"					BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_F_01					sActivity = "PED_ACT_BEACH_PARTY_SEATED_F_01"					BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01			sActivity = "PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_02			sActivity = "PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_02"			BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_F_01			sActivity = "PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_F_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_M_01			sActivity = "PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_M_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_F_01			sActivity = "PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_F_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_01			sActivity = "PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_01"		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_02			sActivity = "PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_02"		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_F_01			sActivity = "PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_F_01"		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01			sActivity = "PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02			sActivity = "PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02"			BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01			sActivity = "PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_M_01			sActivity = "PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_M_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_F_01			sActivity = "PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_F_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_01			sActivity = "PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_02			sActivity = "PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_02"			BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_F_01			sActivity = "PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_F_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_01			sActivity = "PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_02			sActivity = "PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_02"			BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01			sActivity = "PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_M_01			sActivity = "PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_M_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_F_01			sActivity = "PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_F_01"			BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_M_01			sActivity = "PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_M_01"		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_F_01			sActivity = "PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_F_01"		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_01						sActivity = "PED_ACT_DANCING_AMBIENT_M_01"						BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_02						sActivity = "PED_ACT_DANCING_AMBIENT_M_02"						BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_03						sActivity = "PED_ACT_DANCING_AMBIENT_M_03"						BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_04						sActivity = "PED_ACT_DANCING_AMBIENT_M_04"						BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_05						sActivity = "PED_ACT_DANCING_AMBIENT_M_05"						BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_06						sActivity = "PED_ACT_DANCING_AMBIENT_M_06"						BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_01						sActivity = "PED_ACT_DANCING_AMBIENT_F_01"						BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_02						sActivity = "PED_ACT_DANCING_AMBIENT_F_02"						BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_03						sActivity = "PED_ACT_DANCING_AMBIENT_F_03"						BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_04						sActivity = "PED_ACT_DANCING_AMBIENT_F_04"						BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_05						sActivity = "PED_ACT_DANCING_AMBIENT_F_05"						BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_06						sActivity = "PED_ACT_DANCING_AMBIENT_F_06"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_01						sActivity = "PED_ACT_DANCING_FACE_DJ_M_01"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_02						sActivity = "PED_ACT_DANCING_FACE_DJ_M_02"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_03						sActivity = "PED_ACT_DANCING_FACE_DJ_M_03"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_04						sActivity = "PED_ACT_DANCING_FACE_DJ_M_04"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_05						sActivity = "PED_ACT_DANCING_FACE_DJ_M_05"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_06						sActivity = "PED_ACT_DANCING_FACE_DJ_M_06"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_01						sActivity = "PED_ACT_DANCING_FACE_DJ_F_01"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_02						sActivity = "PED_ACT_DANCING_FACE_DJ_F_02"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_03						sActivity = "PED_ACT_DANCING_FACE_DJ_F_03"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_04						sActivity = "PED_ACT_DANCING_FACE_DJ_F_04"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_05						sActivity = "PED_ACT_DANCING_FACE_DJ_F_05"						BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_06						sActivity = "PED_ACT_DANCING_FACE_DJ_F_06"						BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_01					sActivity = "PED_ACT_DANCING_SINGLE_PROP_M_01"					BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_02					sActivity = "PED_ACT_DANCING_SINGLE_PROP_M_02"					BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_03					sActivity = "PED_ACT_DANCING_SINGLE_PROP_M_03"					BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_04					sActivity = "PED_ACT_DANCING_SINGLE_PROP_M_04"					BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_05					sActivity = "PED_ACT_DANCING_SINGLE_PROP_M_05"					BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_06					sActivity = "PED_ACT_DANCING_SINGLE_PROP_M_06"					BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_01					sActivity = "PED_ACT_DANCING_SINGLE_PROP_F_01"					BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_02					sActivity = "PED_ACT_DANCING_SINGLE_PROP_F_02"					BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_03					sActivity = "PED_ACT_DANCING_SINGLE_PROP_F_03"					BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_04					sActivity = "PED_ACT_DANCING_SINGLE_PROP_F_04"					BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_05					sActivity = "PED_ACT_DANCING_SINGLE_PROP_F_05"					BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_06					sActivity = "PED_ACT_DANCING_SINGLE_PROP_F_06"					BREAK
		CASE PED_ACT_DANCING_GROUPA_F_PARENT					sActivity = "PED_ACT_DANCING_GROUPA_F_PARENT"					BREAK
		CASE PED_ACT_DANCING_GROUPA_M_CHILD_01					sActivity = "PED_ACT_DANCING_GROUPA_M_CHILD_01"					BREAK
		CASE PED_ACT_DANCING_GROUPB_F_PARENT					sActivity = "PED_ACT_DANCING_GROUPB_F_PARENT"					BREAK
		CASE PED_ACT_DANCING_GROUPB_F_CHILD_01					sActivity = "PED_ACT_DANCING_GROUPB_F_CHILD_01"					BREAK
		CASE PED_ACT_DANCING_GROUPC_M_PARENT					sActivity = "PED_ACT_DANCING_GROUPC_M_PARENT"					BREAK
		CASE PED_ACT_DANCING_GROUPC_M_CHILD_01					sActivity = "PED_ACT_DANCING_GROUPC_M_CHILD_01"					BREAK
		CASE PED_ACT_DANCING_GROUPD_F_PARENT					sActivity = "PED_ACT_DANCING_GROUPD_F_PARENT"					BREAK
		CASE PED_ACT_DANCING_GROUPD_M_CHILD_01					sActivity = "PED_ACT_DANCING_GROUPD_M_CHILD_01"					BREAK
		CASE PED_ACT_DANCING_GROUPD_M_CHILD_02					sActivity = "PED_ACT_DANCING_GROUPD_M_CHILD_02"					BREAK
		CASE PED_ACT_DANCING_GROUPE_F_PARENT					sActivity = "PED_ACT_DANCING_GROUPE_F_PARENT"					BREAK
		CASE PED_ACT_DANCING_GROUPE_F_CHILD_01					sActivity = "PED_ACT_DANCING_GROUPE_F_CHILD_01"					BREAK
		CASE PED_ACT_DANCING_GROUPE_M_CHILD_02					sActivity = "PED_ACT_DANCING_GROUPE_M_CHILD_02"					BREAK
		CASE PED_ACT_DANCING_BEACH_M_01							sActivity = "PED_ACT_DANCING_BEACH_M_01"						BREAK
		CASE PED_ACT_DANCING_BEACH_M_02							sActivity = "PED_ACT_DANCING_BEACH_M_02"						BREAK
		CASE PED_ACT_DANCING_BEACH_M_03							sActivity = "PED_ACT_DANCING_BEACH_M_03"						BREAK
		CASE PED_ACT_DANCING_BEACH_M_04							sActivity = "PED_ACT_DANCING_BEACH_M_04"						BREAK
		CASE PED_ACT_DANCING_BEACH_M_05							sActivity = "PED_ACT_DANCING_BEACH_M_05"						BREAK
		CASE PED_ACT_DANCING_BEACH_F_01							sActivity = "PED_ACT_DANCING_BEACH_F_01"						BREAK
		CASE PED_ACT_DANCING_BEACH_F_02							sActivity = "PED_ACT_DANCING_BEACH_F_02"						BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_01					sActivity = "PED_ACT_DANCING_BEACH_PROP_M_01"					BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_02					sActivity = "PED_ACT_DANCING_BEACH_PROP_M_02"					BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_03					sActivity = "PED_ACT_DANCING_BEACH_PROP_M_03"					BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_04					sActivity = "PED_ACT_DANCING_BEACH_PROP_M_04"					BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_F_01					sActivity = "PED_ACT_DANCING_BEACH_PROP_F_01"					BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_F_02					sActivity = "PED_ACT_DANCING_BEACH_PROP_F_02"					BREAK
		CASE PED_ACT_DANCING_CLUB_M_01							sActivity = "PED_ACT_DANCING_CLUB_M_01"							BREAK
		CASE PED_ACT_DANCING_CLUB_M_02							sActivity = "PED_ACT_DANCING_CLUB_M_02"							BREAK
		CASE PED_ACT_DANCING_CLUB_M_03							sActivity = "PED_ACT_DANCING_CLUB_M_03"							BREAK
		CASE PED_ACT_DANCING_CLUB_F_01							sActivity = "PED_ACT_DANCING_CLUB_F_01"							BREAK
		CASE PED_ACT_DANCING_CLUB_F_02							sActivity = "PED_ACT_DANCING_CLUB_F_02"							BREAK
		CASE PED_ACT_DANCING_CLUB_F_03							sActivity = "PED_ACT_DANCING_CLUB_F_03"							BREAK
		CASE PED_ACT_DJ_SOLOMUN									sActivity = "PED_ACT_DJ_SOLOMUN"								BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_RAMPA						sActivity = "PED_ACT_DJ_KEINEMUSIK_RAMPA"						BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_ME							sActivity = "PED_ACT_DJ_KEINEMUSIK_ME"							BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_ADAM							sActivity = "PED_ACT_DJ_KEINEMUSIK_ADAM"						BREAK
		CASE PED_ACT_DJ_PALMS_TRAX								sActivity = "PED_ACT_DJ_PALMS_TRAX"								BREAK		
		CASE PED_ACT_DJ_MOODYMANN								sActivity = "PED_ACT_DJ_MOODYMANN"								BREAK	
		CASE PED_ACT_DJ_KEINEMUSIK_DANCER_01					sActivity = "PED_ACT_DJ_KEINEMUSIK_DANCER_01"					BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_DANCER_02					sActivity = "PED_ACT_DJ_KEINEMUSIK_DANCER_02"					BREAK
		CASE PED_ACT_DJ_MOODYMANN_DANCER_01						sActivity = "PED_ACT_DJ_MOODYMANN_DANCER_01"					BREAK
		CASE PED_ACT_DJ_MOODYMANN_DANCER_02						sActivity = "PED_ACT_DJ_MOODYMANN_DANCER_02"					BREAK
		CASE PED_ACT_DJ_MOODYMANN_DANCER_03						sActivity = "PED_ACT_DJ_MOODYMANN_DANCER_03"					BREAK
		CASE PED_ACT_PAVEL_SCREENS								sActivity = "PED_ACT_PAVEL_SCREENS"								BREAK
		CASE PED_ACT_PAVEL_BUTTONS								sActivity = "PED_ACT_PAVEL_BUTTONS"								BREAK
		CASE PED_ACT_PAVEL_TORPEDOS								sActivity = "PED_ACT_PAVEL_TORPEDOS"							BREAK
		CASE PED_ACT_PAVEL_CAVIAR								sActivity = "PED_ACT_PAVEL_CAVIAR"								BREAK
		CASE PED_ACT_PAVEL_ENGINE								sActivity = "PED_ACT_PAVEL_ENGINE"								BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_KAYLEE				sActivity = "PED_ACT_CASINO_NIGHTCLUB_VIP_KAYLEE"				BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_JACKIE				sActivity = "PED_ACT_CASINO_NIGHTCLUB_VIP_JACKIE"				BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_PATRICIA				sActivity = "PED_ACT_CASINO_NIGHTCLUB_VIP_PATRICIA"				BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_MIGUEL				sActivity = "PED_ACT_CASINO_NIGHTCLUB_VIP_MIGUEL"				BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_BODYGUARD				sActivity = "PED_ACT_CASINO_NIGHTCLUB_VIP_BODYGUARD"			BREAK
		CASE PED_ACT_BEACH_VIP_ELRUBIO_IG1						sActivity = "PED_ACT_BEACH_VIP_ELRUBIO_IG1"						BREAK
		CASE PED_ACT_BEACH_VIP_ELRUBIO_IG2						sActivity = "PED_ACT_BEACH_VIP_ELRUBIO_IG2"						BREAK
		CASE PED_ACT_BEACH_VIP_SCOTT_IG3						sActivity = "PED_ACT_BEACH_VIP_SCOTT_IG3"						BREAK
		CASE PED_ACT_BEACH_VIP_SCOTT_IG4						sActivity = "PED_ACT_BEACH_VIP_SCOTT_IG4"						BREAK
		CASE PED_ACT_BEACH_VIP_SCOTT_IG4_DANCER					sActivity = "PED_ACT_BEACH_VIP_SCOTT_IG4_DANCER"				BREAK
		CASE PED_ACT_BEACH_VIP_DAVE_IG5							sActivity = "PED_ACT_BEACH_VIP_DAVE_IG5"						BREAK
		CASE PED_ACT_BEACH_VIP_DAVE_IG6							sActivity = "PED_ACT_BEACH_VIP_DAVE_IG6"						BREAK
		CASE PED_ACT_BEACH_VIP_DAVE_IG7							sActivity = "PED_ACT_BEACH_VIP_DAVE_IG7"						BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_GUY						sActivity = "PED_ACT_BEACH_VIP_PARTY_GUY"						BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_GIRL						sActivity = "PED_ACT_BEACH_VIP_PARTY_GIRL"						BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_COUPLE_MALE_PARENT			sActivity = "PED_ACT_BEACH_VIP_PARTY_COUPLE_MALE_PARENT"		BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_COUPLE_FEMALE_CHILD_01 	sActivity = "PED_ACT_BEACH_VIP_PARTY_COUPLE_FEMALE_CHILD_01"	BREAK
		CASE PED_ACT_CLIPBOARD									sActivity = "PED_ACT_CLIPBOARD"									BREAK
		CASE PED_ACT_JANITOR									sActivity = "PED_ACT_JANITOR"									BREAK
		CASE PED_ACT_AUTO_SHOP_MECHANIC_LEAN_PNONE				sActivity = "PED_ACT_AUTO_SHOP_MECHANIC_LEAN_PNONE"				BREAK
		CASE PED_ACT_AUTO_SHOP_SESSANTA_TEXTING					sActivity = "PED_ACT_AUTO_SHOP_SESSANTA_TEXTING"				BREAK
		CASE PED_ACT_AUTO_SHOP_SESSANTA_DESK					sActivity = "PED_ACT_AUTO_SHOP_SESSANTA_DESK"					BREAK
		CASE PED_ACT_AUTO_SHOP_SESSANTA_BARRIER_LEAN			sActivity = "PED_ACT_AUTO_SHOP_SESSANTA_BARRIER_LEAN"			BREAK
		CASE PED_ACT_AUTO_SHOP_MECHANIC_IMPATIENT				sActivity = "PED_ACT_AUTO_SHOP_MECHANIC_IMPATIENT"				BREAK
		CASE PED_ACT_AUTO_SHOP_MECHANIC_TEXTING					sActivity = "PED_ACT_AUTO_SHOP_MECHANIC_TEXTING"				BREAK
		CASE PED_ACT_CAR_MEET_DRINK_COFFEE_F					sActivity = "PED_ACT_CAR_MEET_DRINK_COFFEE_F"				    BREAK
		CASE PED_ACT_CAR_MEET_DRINK_COFFEE_M					sActivity = "PED_ACT_CAR_MEET_DRINK_COFFEE_M"			     	BREAK
		CASE PED_ACT_CAR_MEET_PHONE_M							sActivity = "PED_ACT_CAR_MEET_PHONE_M"							BREAK
		CASE PED_ACT_CAR_MEET_PHONE_F							sActivity = "PED_ACT_CAR_MEET_PHONE_F"							BREAK
		CASE PED_ACT_CAR_MEET_PAPARAZZI							sActivity = "PED_ACT_CAR_MEET_PAPARAZZI"						BREAK
		CASE PED_ACT_CAR_MEET_INSPECT							sActivity = "PED_ACT_CAR_MEET_INSPECT"							BREAK	
		CASE PED_ACT_TOTAL										sActivity = "PED_ACT_TOTAL"										BREAK
	ENDSWITCH
	IF IS_STRING_NULL_OR_EMPTY(sActivity)
		RETURN DEBUG_GET_PED_ACTIVITY_STRING2(eActivity)
	ENDIF
	RETURN sActivity
ENDFUNC
#ENDIF	// IS_DEBUG_BUILD
#ENDIF	// FEATURE_HEIST_ISLAND
