USING "net_hacking_fingerprint_clone.sch"
USING "net_hacking_fingerprint_retro.sch"

PROC PROCESS_FINGERPRINT_CLONE_MINIGAME(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, INT iNumberOfStages = 3, INT iNumberOfPlays = -1, BOOL bShowFinalQuitWarning = FALSE)

	IF sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_SLEEP
	AND sGameplayData.sBaseStruct.eHackingGameState < HACKING_GAME_CLEANUP
	OR sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
		#IF IS_DEBUG_BUILD
		IF sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_LOAD_LEVEL
			PROCESS_FINGERPINT_CLONE_DEBUG(sGameplayData)
		ENDIF
		#ENDIF
		IF sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_CLEANUP
			PROCESS_FINGERPRINT_CLONE_EVERY_FRAME_MISC(sGameplayData, sControllerStruct)
		ENDIF
		IF NOT g_bHideHackingVisuals
		AND sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_LOAD_LEVEL
		AND sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_CLEANUP
			DRAW_FINGERPRINT_CLONE_PLAY_AREA(sGameplayData)
		ENDIF
	ENDIF
	
	SWITCH sGameplayData.sBaseStruct.eHackingGameState
		CASE HACKING_GAME_SLEEP
			PROCESS_HACKING_GAME_SLEEP(sGameplayData.sBaseStruct, sControllerStruct)
			BREAK
		CASE HACKING_GAME_INIT
			PROCESS_FINGERPRINT_CLONE_INIT(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_LOAD_LEVEL
			PROCESS_FINGERPRINT_CLONE_LOAD_LEVEL(sGameplayData, sControllerStruct, iNumberOfStages)
		BREAK
		CASE HACKING_GAME_STARTUP
			PROCESS_FINGERPRINT_CLONE_STARTUP(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_PLAY
			PROCESS_FINGERPRINT_CLONE_PLAY(sGameplayData, sControllerStruct, iNumberOfPlays, bShowFinalQuitWarning)
		BREAK
		CASE HACKING_GAME_PASS
			PROCESS_FINGERPRINT_CLONE_PASS(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_FAIL
			PROCESS_FINGERPRINT_CLONE_FAIL(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_CLEANUP
			CLEAN_UP_FINGERPRINT_CLONE(sGameplayData, sControllerStruct)
		BREAK
#IF IS_DEBUG_BUILD
		CASE HACKING_GAME_VISUAL_TEST
			PROCESS_FINGERPRINT_CLONE_VISUAL_TEST(sGameplayData, sControllerStruct)
		BREAK
#ENDIF
	ENDSWITCH
	
	IF NOT g_bHideHackingVisuals
	AND sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_LOAD_LEVEL
	AND sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_CLEANUP
	OR sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
		DRAW_PLAY_AREA_OVERLAY(sGameplayData.sBaseStruct)
	ENDIF
		
ENDPROC


PROC PROCESS_FINGERPRINT_RETRO_MINIGAME(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, INT iNumberOfStages = 3, INT iNumberOfPlays = -1)
		
	IF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_PLAY
	AND NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH) // Safety net
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdRetroTimer, sGameplayData.sBaseStruct.iMaxTime)
		
			UNUSED_PARAMETER(sControllerStruct)
			CDEBUG1LN(DEBUG_MINIGAME, "[HACKING_GAME][BAZ] Retro Game has timed out")
			IF NOT HAS_NET_TIMER_STARTED(sGameplayData.sBaseStruct.tdEndDelay)
				REINIT_NET_TIMER(sGameplayData.sBaseStruct.tdEndDelay)
				LOAD_BINK_MOVIE(sGameplayData.sBaseStruct, "FingerPrint_Fail")
				CDEBUG1LN(DEBUG_MINIGAME, "[HACKING_GAME] Fail = Starting time")
				PLAY_SOUND_FRONTEND(-1, "Hack_Failed", sGameplayData.sBaseStruct.sAudioSet)
			ELSE
				IF MAINTAIN_BINK_MOVIE(sGameplayData.sBaseStruct)
				OR HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdEndDelay, 8000)
					PRINTLN("[HACKING_GAME] Fail = Moving to cleanup")
					UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_CLEANUP)
					SET_BIT(sControllerStruct.iBS, ciHGBS_FAILED)
				ENDIF
			ENDIF
			
			EXIT // Don't go beyond here if timed out.
		ENDIF
	ENDIF

	//PRINTLN("[MC][FingerPrintRETRO] Running Process FingerPrint RETRO")
	IF sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_SLEEP
	AND sGameplayData.sBaseStruct.eHackingGameState < HACKING_GAME_CLEANUP
	OR sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
		#IF IS_DEBUG_BUILD
		IF sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_LOAD_LEVEL
			PROCESS_FINGERPRINT_RETRO_DEBUG(sGameplayData)
		ENDIF
		#ENDIF
		IF sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_CLEANUP
			PROCESS_FINGERPRINT_RETRO_EVERY_FRAME_MISC(sGameplayData, sControllerStruct)
		ENDIF
		IF NOT g_bHideHackingVisuals
		AND sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_LOAD_LEVEL
		AND sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_CLEANUP
			DRAW_FINGERPRINT_RETRO_PLAY_AREA(sGameplayData)
		ENDIF
	ENDIF
	
	SWITCH sGameplayData.sBaseStruct.eHackingGameState
		CASE HACKING_GAME_SLEEP
			PROCESS_HACKING_GAME_SLEEP(sGameplayData.sBaseStruct, sControllerStruct)
			BREAK
		CASE HACKING_GAME_INIT
			PROCESS_FINGERPRINT_RETRO_INIT(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_LOAD_LEVEL
			PROCESS_FINGERPRINT_RETRO_LOAD_LEVEL(sGameplayData, sControllerStruct, iNumberOfStages)
		BREAK
		CASE HACKING_GAME_STARTUP
			PROCESS_FINGERPRINT_RETRO_STARTUP(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_PLAY
			PROCESS_FINGERPRINT_RETRO_PLAY(sGameplayData, sControllerStruct, iNumberOfPlays)
		BREAK
		CASE HACKING_GAME_PASS
			PROCESS_FINGERPRINT_RETRO_PASS(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_FAIL
			PROCESS_FINGERPRINT_RETRO_FAIL(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_CLEANUP
			CLEAN_UP_FINGERPRINT_RETRO(sGameplayData, sControllerStruct, DEFAULT, DEFAULT, GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST4_SESSION_ID_POSTIME))
		BREAK
#IF IS_DEBUG_BUILD
		CASE HACKING_GAME_VISUAL_TEST
			PROCESS_FINGERPRINT_RETRO_VISUAL_TEST(sGameplayData, sControllerStruct)
		BREAK
#ENDIF
		
	ENDSWITCH
	
		
	IF NOT g_bHideHackingVisuals
	AND sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_LOAD_LEVEL
	AND sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_CLEANUP
	OR sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
		DRAW_PLAY_AREA_OVERLAY_RETRO()
	ENDIF

ENDPROC	

PROC PROCESS_FINGERPRINT_MINIGAME(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, INT iNumberOfStages = 3, INT iNumberOfPlays = -1, BOOL bShowFinalQuitWarning = FALSE, BOOL bFciRetro = FALSE)
	
#IF IS_DEBUG_BUILD
	IF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_INIT
		IF IS_KEYBOARD_KEY_PRESSED(KEY_J)
		AND NOT bFciRetro
			bFciRetro = TRUE
			CDEBUG1LN(DEBUG_MINIGAME, "PROCESS_HACKING_GAME_INIT - Switching to Retro version.")
		ENDIF
		
		IF IS_KEYBOARD_KEY_PRESSED(KEY_K)
		AND bFciRetro
			bFciRetro = FALSE
			CDEBUG1LN(DEBUG_MINIGAME, "PROCESS_HACKING_GAME_INIT - Switching to Standard version.")
		ENDIF
	ENDIF
#ENDIF
	
	IF !bFciRetro
		PROCESS_FINGERPRINT_CLONE_MINIGAME(sGameplayData, sControllerStruct, iNumberOfStages, iNumberOfPlays, bShowFinalQuitWarning)
	ELSE
		PROCESS_FINGERPRINT_RETRO_MINIGAME(sGameplayData, sControllerStruct, iNumberOfStages, iNumberOfPlays)
	ENDIF

ENDPROC
