USING "globals.sch"
USING "net_mission.sch"
USING "net_spawn_move.sch"

#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_SPAWN_AREA_DETAILS(SPAWN_AREA &SPArea)
	
	NET_PRINT("[spawning] Spawn Area Details:") NET_NL()
	NET_PRINT("[spawning] vPos = ") NET_PRINT_VECTOR(SPArea.vCoords1) NET_NL()
	NET_PRINT("[spawning] vMaxPos = ") NET_PRINT_VECTOR(SPArea.vCoords2) NET_NL()
	NET_PRINT("[spawning] fRadius = ") NET_PRINT_FLOAT(SPArea.fFloat) NET_NL()
	NET_PRINT("[spawning] fIncreaseDist = ") NET_PRINT_FLOAT(SPArea.fIncreaseDist) NET_NL()
	NET_PRINT("[spawning] bConsiderCentrePointAsValid = ") NET_PRINT_BOOL(SPArea.bConsiderCentrePointAsValid) NET_NL()
	
	SWITCH SPArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			NET_PRINT("[spawning] iShape = CIRCLE") NET_NL()
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			NET_PRINT("[spawning] iShape = BOX") NET_NL()
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			NET_PRINT("[spawning] iShape = ANGLED") NET_NL()
		BREAK
	ENDSWITCH
	
	NET_PRINT("[spawning] bIsActive = ") NET_PRINT_BOOL(SPArea.bIsActive) NET_NL()
	

	
ENDPROC
#ENDIF

FUNC INT GET_MAP_SPAWNING_SECTOR_FROM_COORD(VECTOR vCoords)
	
	IF (vCoords.y > g_fSpawningSectorYLine[0])
		RETURN 0
	ELIF (vCoords.y > g_fSpawningSectorYLine[1])
	
		IF (vCoords.x < g_fSpawningSectorXLine[0])
			RETURN 1
		ELSE
			RETURN 2
		ENDIF
	
	ELIF (vCoords.y > g_fSpawningSectorYLine[2])
	
		IF (vCoords.x < g_fSpawningSectorXLine[1])
			RETURN 3
		ELIF (vCoords.x < g_fSpawningSectorXLine[2])
			RETURN 4
		ELIF (vCoords.x < g_fSpawningSectorXLine[3])
			RETURN 5
		ELSE
			RETURN 6
		ENDIF
		
	ELSE
		RETURN 7
	ENDIF

	
//	IF (VCoords.x < 0.0)
//		IF (VCoords.y > 4000.0)
//			RETURN 0
//		ELIF (VCoords.y > 0.0)
//			RETURN 2
//		ELIF (VCoords.y > -2000.0)
//			RETURN 4
//		ELSE
//			RETURN 6
//		ENDIF
//	ELSE
//		IF (VCoords.y > 4000.0)
//			RETURN 1
//		ELIF (VCoords.y > 0.0)
//			RETURN 3
//		ELIF (VCoords.y > -2000.0)
//			RETURN 5
//		ELSE
//			RETURN 7
//		ENDIF	
//	ENDIF
//
//	RETURN 8 // this is global
	
ENDFUNC

FUNC INT GET_MAP_SPAWNING_SECTOR_FROM_PROBLEM_AREA(PROBLEM_AREA &ProblemArea)

	VECTOR vec1
	VECTOR vCross
	VECTOR vCorner[4]
	VECTOR vMin, vMax
	
	vMin = <<99999.9, 99999.9, 99999.9>>
	vMax = <<-99999.9, -99999.9, -99999.9>>

	// vec1 = from angled coord1 to coord2
	vec1 = ProblemArea.vCoords2 - ProblemArea.vCoords1
	vec1.z = 0.0

	vCross = CROSS_PRODUCT(<<0.0, 0.0, 1.0>>, vec1)
	vCross /= VMAG(vCross)
	vCross *= (ProblemArea.fFloat*0.5)
	
	vCorner[0] = ProblemArea.vCoords1 + vCross
	vCorner[1] = ProblemArea.vCoords1 - vCross
	vCorner[2] = ProblemArea.vCoords2 + vCross
	vCorner[3] = ProblemArea.vCoords2 - vCross
	
	INT i
	REPEAT 4 i
		IF (vCorner[i].x < vMin.x)
			vMin.x = vCorner[i].x	
		ENDIF
		IF (vCorner[i].x > vMax.x)
			vMax.x = vCorner[i].x	
		ENDIF
		IF (vCorner[i].y < vMin.y)
			vMin.y = vCorner[i].y	
		ENDIF
		IF (vCorner[i].y > vMax.y)
			vMax.y = vCorner[i].y	
		ENDIF
	ENDREPEAT
	
	INT iSector[4]
	
	iSector[0] = GET_MAP_SPAWNING_SECTOR_FROM_COORD(<< vMin.x, vMin.y, 0.0>>)
	iSector[1] = GET_MAP_SPAWNING_SECTOR_FROM_COORD(<< vMin.x, vMax.y, 0.0>>)
	iSector[2] = GET_MAP_SPAWNING_SECTOR_FROM_COORD(<< vMax.x, vMin.y, 0.0>>)
	iSector[3] = GET_MAP_SPAWNING_SECTOR_FROM_COORD(<< vMax.x, vMax.y, 0.0>>)
	
	INT j
	
	// if any of the sectors don't match then return the global sector
	REPEAT 4 j
		REPEAT 4 i
			IF NOT (iSector[i] = iSector[j])
				RETURN NUMBER_OF_SPAWNING_SECTORS-1
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	RETURN iSector[0]
	
ENDFUNC

//PROC COUNT_PROBLEM_AREAS_IN_SECTOR(PROBLEM_AREA &ProblemArea[], STRING strName)
//	INT iSectorTotals[NUMBER_OF_SPAWNING_SECTORS]
//	INT i
//	REPEAT COUNT_OF(ProblemArea) i
//		iSectorTotals[GET_MAP_SPAWNING_SECTOR_FROM_PROBLEM_AREA(ProblemArea[i])] += 1
//	ENDREPEAT	
//	REPEAT COUNT_OF(iSectorTotals) i
//		PRINTLN(strName, " iSectorTotals[", 0, "] = ", iSectorTotals[i])
//	ENDREPEAT
//ENDPROC

PROC DEBUG_PRINT_PROBLEM_AREA(PROBLEM_AREA &ProblemArea)
	NET_PRINT_VECTOR(ProblemArea.vCoords1)
	NET_PRINT(", ")
	NET_PRINT_VECTOR(ProblemArea.vCoords2)
	NET_PRINT(", ")
	NET_PRINT_FLOAT(ProblemArea.fFloat)
ENDPROC

PROC ADD_TO_PROBLEM_AREAS_FORBIDDEN(PROBLEM_AREA ProblemArea)
	INT iThisSector	= GET_MAP_SPAWNING_SECTOR_FROM_PROBLEM_AREA(ProblemArea)
	#IF IS_DEBUG_BUILD
		IF NOT (g_iTotalForbiddenProblemAreasForSector[iThisSector] < MAX_NUMBER_OF_FORBIDDEN_AREAS_IN_SECTOR)
			NET_PRINT("[spawning] ADD_TO_PROBLEM_AREAS_FORBIDDEN - need to increase MAX_NUMBER_OF_FORBIDDEN_AREAS_IN_SECTOR") NET_NL()
			SCRIPT_ASSERT("[spawning] ADD_TO_PROBLEM_AREAS_FORBIDDEN - need to increase MAX_NUMBER_OF_FORBIDDEN_AREAS_IN_SECTOR, Bug Aidan Temple.")
			EXIT
		ENDIF
	#ENDIF
	ProblemArea_Forbidden[iThisSector][g_iTotalForbiddenProblemAreasForSector[iThisSector]] = ProblemArea
	g_iTotalForbiddenProblemAreasForSector[iThisSector]++
	NET_PRINT("[spawning] ADD_TO_PROBLEM_AREAS_FORBIDDEN - added to sector ")
	NET_PRINT_INT(iThisSector)
	NET_PRINT(", new total for sector is ")
	NET_PRINT_INT(g_iTotalForbiddenProblemAreasForSector[iThisSector])
	NET_PRINT(", ")
	DEBUG_PRINT_PROBLEM_AREA(ProblemArea)
	NET_NL()
ENDPROC

PROC ADD_TO_PROBLEM_AREAS_NODES(PROBLEM_AREA ProblemArea)
	INT iThisSector	= GET_MAP_SPAWNING_SECTOR_FROM_PROBLEM_AREA(ProblemArea)
	#IF IS_DEBUG_BUILD
		IF NOT (g_iTotalNodesProblemAreasForSector[iThisSector] < MAX_NUMBER_OF_PROBLEM_NODE_AREAS_IN_SECTOR)
			NET_PRINT("[spawning] ADD_TO_PROBLEM_AREAS_NODES - need to increase MAX_NUMBER_OF_PROBLEM_NODE_AREAS_IN_SECTOR") NET_NL()
			SCRIPT_ASSERT("[spawning] ADD_TO_PROBLEM_AREAS_NODES - need to increase MAX_NUMBER_OF_PROBLEM_NODE_AREAS_IN_SECTOR, bug Neil F.")
			EXIT
		ENDIF
	#ENDIF	
	ProblemArea_Nodes[iThisSector][g_iTotalNodesProblemAreasForSector[iThisSector]] = ProblemArea
	g_iTotalNodesProblemAreasForSector[iThisSector]++
	NET_PRINT("[spawning] ADD_TO_PROBLEM_AREAS_NODES - added to sector ")
	NET_PRINT_INT(iThisSector)
	NET_PRINT(", new total for sector is ")
	NET_PRINT_INT(g_iTotalNodesProblemAreasForSector[iThisSector])
	NET_PRINT(", ")
	DEBUG_PRINT_PROBLEM_AREA(ProblemArea)
	NET_NL()
ENDPROC




//FUNC BOOL IS_POINT_IN_PROBLEM_AREA_INTERIOR_BLEED(VECTOR vPoint)
//	INT i
//	REPEAT TOTAL_NUMBER_OF_INTERIOR_BLEEDS i 
//		IF IsPointInsideProblemArea(vPoint, ProblemArea_InteriorBleed[i])
//			NET_PRINT("[spawning] IS_POINT_IN_PROBLEM_AREA_INTERIOR_BLEED - returning TRUE for coord ") NET_PRINT_VECTOR(vPoint) NET_NL()		
//			RETURN(TRUE)
//		ENDIF
//	ENDREPEAT
//	RETURN(FALSE)
//ENDFUNC

FUNC BOOL IS_POINT_IN_PROBLEM_AREA_FORBIDDEN(VECTOR &vPoint, BOOL bMovePointOutside=FALSE)
	INT i
	VECTOR vTheseCoords = vPoint
	INT iSector = GET_MAP_SPAWNING_SECTOR_FROM_COORD(vTheseCoords)
	// search that sector
	REPEAT g_iTotalForbiddenProblemAreasForSector[iSector] i	
		IF IsPointInsideProblemArea(vTheseCoords, ProblemArea_Forbidden[iSector][i])
			NET_PRINT("[spawning] IS_POINT_IN_PROBLEM_AREA_FORBIDDEN - returning TRUE for coord ") NET_PRINT_VECTOR(vTheseCoords) NET_PRINT(", sector ") NET_PRINT_INT(iSector) NET_NL()		
			
			IF (bMovePointOutside)
				MovePointOutsideAngledArea(vTheseCoords, ProblemArea_Forbidden[iSector][i].vCoords1, ProblemArea_Forbidden[iSector][i].vCoords2, ProblemArea_Forbidden[iSector][i].fFloat)
				NET_PRINT("[spawning] IS_POINT_IN_PROBLEM_AREA_FORBIDDEN - moved point to ") NET_PRINT_VECTOR(vTheseCoords) NET_NL()
				vPoint = vTheseCoords
			ENDIF
			
			RETURN(TRUE)
		ENDIF
	ENDREPEAT
	// also search through the global forbidden areas
	REPEAT g_iTotalForbiddenProblemAreasForSector[NUMBER_OF_SPAWNING_SECTORS-1] i	
		IF IsPointInsideProblemArea(vTheseCoords, ProblemArea_Forbidden[NUMBER_OF_SPAWNING_SECTORS-1][i])
			NET_PRINT("[spawning] IS_POINT_IN_PROBLEM_AREA_FORBIDDEN - returning TRUE for coord ") NET_PRINT_VECTOR(vTheseCoords) NET_PRINT(", global sector ")   NET_NL()		
			
			IF (bMovePointOutside)
				MovePointOutsideAngledArea(vTheseCoords, ProblemArea_Forbidden[NUMBER_OF_SPAWNING_SECTORS-1][i].vCoords1, ProblemArea_Forbidden[NUMBER_OF_SPAWNING_SECTORS-1][i].vCoords2, ProblemArea_Forbidden[NUMBER_OF_SPAWNING_SECTORS-1][i].fFloat)
				NET_PRINT("[spawning] IS_POINT_IN_PROBLEM_AREA_FORBIDDEN - moved point to ") NET_PRINT_VECTOR(vTheseCoords) NET_NL()
				vPoint = vTheseCoords
			ENDIF
			
			
			RETURN(TRUE)
		ENDIF
	ENDREPEAT	
	RETURN(FALSE)
ENDFUNC

PROC GET_FORBIDDEN_AREA_POINT_IS_IN(VECTOR vPoint, VECTOR &vCoords1, VECTOR &vCoords2, FLOAT &fWidth)
	INT i
	VECTOR vTheseCoords = vPoint
	INT iSector = GET_MAP_SPAWNING_SECTOR_FROM_COORD(vTheseCoords)
	// search that sector
	REPEAT g_iTotalForbiddenProblemAreasForSector[iSector] i	
		IF IsPointInsideProblemArea(vTheseCoords, ProblemArea_Forbidden[iSector][i])

			vCoords1 = 	ProblemArea_Forbidden[iSector][i].vCoords1
			vCoords2 = 	ProblemArea_Forbidden[iSector][i].vCoords2
			fWidth = ProblemArea_Forbidden[iSector][i].fFloat

			PRINTLN("[spawning] GET_FORBIDDEN_AREA_POINT_IS_IN - returning TRUE for vPoint ", vPoint, ", ProblemArea_Forbidden[", iSector, "][", i, "] vCoords1 = ", vCoords1, " vCoords2 = ", vCoords2, ", fWidth = ", fWidth)			
			
			EXIT 
		ENDIF
	ENDREPEAT
	// also search through the global forbidden areas
	REPEAT g_iTotalForbiddenProblemAreasForSector[NUMBER_OF_SPAWNING_SECTORS-1] i	
		IF IsPointInsideProblemArea(vTheseCoords, ProblemArea_Forbidden[NUMBER_OF_SPAWNING_SECTORS-1][i])

			vCoords1 = 	ProblemArea_Forbidden[NUMBER_OF_SPAWNING_SECTORS-1][i].vCoords1
			vCoords2 = 	ProblemArea_Forbidden[NUMBER_OF_SPAWNING_SECTORS-1][i].vCoords2
			fWidth = ProblemArea_Forbidden[NUMBER_OF_SPAWNING_SECTORS-1][i].fFloat
			
			PRINTLN("[spawning] GET_FORBIDDEN_AREA_POINT_IS_IN - global - returning TRUE for vPoint ", vPoint, ", ProblemArea_Forbidden[", NUMBER_OF_SPAWNING_SECTORS-1, "][", i, "] vCoords1 = ", vCoords1, " vCoords2 = ", vCoords2, ", fWidth = ", fWidth)			
						
			
			EXIT
		ENDIF
	ENDREPEAT	

ENDPROC


FUNC BOOL IS_POINT_IN_PROBLEM_NODE_AREA(VECTOR vPoint)
	INT i
	INT iSector = GET_MAP_SPAWNING_SECTOR_FROM_COORD(vPoint)
	REPEAT g_iTotalNodesProblemAreasForSector[iSector] i 
		IF IsPointInsideProblemArea(vPoint, ProblemArea_Nodes[iSector][i])
			NET_PRINT("[spawning] IS_POINT_IN_PROBLEM_NODE_AREA - returning TRUE for coord ") NET_PRINT_VECTOR(vPoint) 
			NET_PRINT(", ProblemArea_Nodes[") NET_PRINT_INT(iSector) NET_PRINT("][") NET_PRINT_INT(i) NET_PRINT("]= ")   
			NET_PRINT_VECTOR(ProblemArea_Nodes[iSector][i].vCoords1)
			NET_PRINT(", ")
			NET_PRINT_VECTOR(ProblemArea_Nodes[iSector][i].vCoords2)
			NET_PRINT(", ")
			NET_PRINT_FLOAT(ProblemArea_Nodes[iSector][i].fFloat)
			NET_NL()		
			RETURN(TRUE)			
		ENDIF
	ENDREPEAT
	REPEAT g_iTotalNodesProblemAreasForSector[NUMBER_OF_SPAWNING_SECTORS-1] i 
		IF IsPointInsideProblemArea(vPoint, ProblemArea_Nodes[NUMBER_OF_SPAWNING_SECTORS-1][i])
			NET_PRINT("[spawning] IS_POINT_IN_PROBLEM_NODE_AREA - returning TRUE for coord ") NET_PRINT_VECTOR(vPoint) 
			NET_PRINT(", ProblemArea_Nodes[") NET_PRINT_INT(NUMBER_OF_SPAWNING_SECTORS-1) NET_PRINT("][") NET_PRINT_INT(i) NET_PRINT("]= ")   
			NET_PRINT_VECTOR(ProblemArea_Nodes[NUMBER_OF_SPAWNING_SECTORS-1][i].vCoords1)
			NET_PRINT(", ")
			NET_PRINT_VECTOR(ProblemArea_Nodes[NUMBER_OF_SPAWNING_SECTORS-1][i].vCoords2)
			NET_PRINT(", ")
			NET_PRINT_FLOAT(ProblemArea_Nodes[NUMBER_OF_SPAWNING_SECTORS-1][i].fFloat)
			NET_NL()		
			RETURN(TRUE)			
		ENDIF
	ENDREPEAT	
	RETURN(FALSE)
ENDFUNC


FUNC BOOL IS_POINT_IN_PROBLEM_NODESEARCH_AREA(VECTOR &vPoint, BOOL bMovePointOutside=FALSE)
	INT i
	VECTOR vTheseCoords = vPoint
	REPEAT NUMBER_OF_PROBLEM_NODESEARCH_AREAS i 
	
		// if the z is below the map then consider it. (it might be using point from a respawning vehicle)
		IF (vTheseCoords.z <= 0.0)
			vTheseCoords.z = (ProblemArea_NodeSearch[i].vCoords1.z + ProblemArea_NodeSearch[i].vCoords2.z) * 0.5
			NET_PRINT("[spawning] IS_POINT_IN_PROBLEM_NODESEARCH_AREA - z was below 0, using new z ") NET_PRINT_VECTOR(vTheseCoords) NET_NL()
		ENDIF
	
		IF IsPointInsideProblemArea(vTheseCoords, ProblemArea_NodeSearch[i])
		
		
			NET_PRINT("[spawning] IS_POINT_IN_PROBLEM_NODESEARCH_AREA - returning TRUE for coord ") NET_PRINT_VECTOR(vTheseCoords) 
			NET_PRINT(", ProblemArea_NodeSearch[") NET_PRINT_INT(i) NET_PRINT("]= ")   
			NET_PRINT_VECTOR(ProblemArea_NodeSearch[i].vCoords1)
			NET_PRINT(", ")
			NET_PRINT_VECTOR(ProblemArea_NodeSearch[i].vCoords2)
			NET_PRINT(", ")
			NET_PRINT_FLOAT(ProblemArea_NodeSearch[i].fFloat)
			NET_NL()	
			
			IF (bMovePointOutside)
				MovePointOutsideAngledArea(vTheseCoords, ProblemArea_NodeSearch[i].vCoords1, ProblemArea_NodeSearch[i].vCoords2, ProblemArea_NodeSearch[i].fFloat)
				NET_PRINT("[spawning] IS_POINT_IN_PROBLEM_NODESEARCH_AREA - moved point to ") NET_PRINT_VECTOR(vTheseCoords) NET_NL()
				vPoint = vTheseCoords
			ENDIF
			
			RETURN(TRUE)					
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_DISPLACE_GLOBAL_EXCLUSION_ZONE(INT iZone)
	SWITCH iZone 
		CASE GLOBAL_EXCLUSION_AREA_PROLOGUE
		CASE GLOBAL_EXCLUSION_AREA_ARENA
		CASE GLOBAL_EXCLUSION_AREA_ARENA_UNDER_CHILLIAD
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_POINT_IN_DISPLACED_GLOBAL_EXCLUSION_ZONE(VECTOR &vPoint, BOOL bMovePointOutsideZone=FALSE)
	
	INT i
	VECTOR vNewPoint
	REPEAT MAX_NUMBER_OF_GLOBAL_EXCLUSION_AREAS i 
		IF IS_DISPLACE_GLOBAL_EXCLUSION_ZONE(i)
			IF IsPointInsideSpawnArea(vPoint, GlobalExclusionArea[i].ExclusionArea, DEFAULT, TRUE)
				#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
						NET_PRINT("[spawning] IS_POINT_IN_DISPLACED_GLOBAL_EXCLUSION_ZONE - returning TRUE -  point inside global occlusion area") NET_NL()
					ENDIF
				#ENDIF
				IF (bMovePointOutsideZone)	
					IF (GlobalExclusionArea[i].bUseSpecificRespawnArea)
						vPoint = GlobalExclusionArea[i].vSpecificRespawnArea	
						NET_PRINT("[spawning] IS_POINT_IN_DISPLACED_GLOBAL_EXCLUSION_ZONE - returning TRUE -  moving to specific area") NET_NL()
					ELSE
						vNewPoint = vPoint
						MovePointOutsideSpawnArea(vNewPoint, GlobalExclusionArea[i].ExclusionArea, DEFAULT, DEFAULT, DEFAULT)
						NET_PRINT("[spawning] IS_POINT_IN_DISPLACED_GLOBAL_EXCLUSION_ZONE - MovePointOutsideSpawnArea GlobalExclusionArea - new vNewPoint = ") NET_PRINT_VECTOR(vNewPoint) NET_NL()					
						// if new point is in another area then move out, but this time flip
						IF IS_POINT_IN_DISPLACED_GLOBAL_EXCLUSION_ZONE(vNewPoint)
							vNewPoint = vPoint
							MovePointOutsideSpawnArea(vNewPoint, GlobalExclusionArea[i].ExclusionArea, DEFAULT, TRUE)
							NET_PRINT("[spawning] IS_POINT_IN_DISPLACED_GLOBAL_EXCLUSION_ZONE - MovePointOutsideSpawnArea GlobalExclusionArea (flipped) - new vNewPoint = ") NET_PRINT_VECTOR(vNewPoint) NET_NL()
						ENDIF										
						vPoint = vNewPoint
					ENDIF
				ENDIF
				RETURN(TRUE)
			ELSE
				#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
						PRINTLN("[spawning] IS_POINT_IN_DISPLACED_GLOBAL_EXCLUSION_ZONE - not inside spawn area") 
					ENDIF
				#ENDIF		
			ENDIF			
		ENDIF
	ENDREPEAT

	RETURN FALSE

ENDFUNC



FUNC BOOL IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(VECTOR &vPoint, BOOL bMovePointOutsideZone=FALSE, BOOL bPermanentAreasOnly=FALSE, BOOL bUseCarNodesForMove=FALSE, BOOL bIncludeAirSpaceBelow=TRUE)
	INT i
	VECTOR vNewPoint
	REPEAT MAX_NUMBER_OF_GLOBAL_EXCLUSION_AREAS i 
		IF (GlobalExclusionArea[i].ExclusionArea.bIsActive = TRUE)		
			
			IF NOT (bPermanentAreasOnly)
			OR ((bPermanentAreasOnly) AND (GlobalExclusionArea[i].bAlwaysConsider))					

				#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
						PRINTLN("[spawning] IS_POINT_IN_GLOBAL_EXCLUSION_ZONE - checking ", vPoint, " in global exclusion ", i) 
					ENDIF
				#ENDIF			
			
				IF IsPointInsideSpawnArea(vPoint, GlobalExclusionArea[i].ExclusionArea, DEFAULT, bIncludeAirSpaceBelow)
					#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
							NET_PRINT("[spawning] IS_POINT_IN_GLOBAL_EXCLUSION_ZONE - returning TRUE -  point inside global occlusion area") NET_NL()
						ENDIF
					#ENDIF
					IF (bMovePointOutsideZone)	
						IF (GlobalExclusionArea[i].bUseSpecificRespawnArea)
							vPoint = GlobalExclusionArea[i].vSpecificRespawnArea	
							NET_PRINT("[spawning] IS_POINT_IN_GLOBAL_EXCLUSION_ZONE - returning TRUE -  moving to specific area") NET_NL()
						ELSE
							vNewPoint = vPoint
							MovePointOutsideSpawnArea(vNewPoint, GlobalExclusionArea[i].ExclusionArea, DEFAULT, DEFAULT, bUseCarNodesForMove)
							NET_PRINT("[spawning] IS_POINT_IN_GLOBAL_EXCLUSION_ZONE - MovePointOutsideSpawnArea GlobalExclusionArea - new vNewPoint = ") NET_PRINT_VECTOR(vNewPoint) NET_NL()					
							// if new point is in another area then move out, but this time flip
							IF IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vNewPoint)
								vNewPoint = vPoint
								MovePointOutsideSpawnArea(vNewPoint, GlobalExclusionArea[i].ExclusionArea, DEFAULT, TRUE)
								NET_PRINT("[spawning] IS_POINT_IN_GLOBAL_EXCLUSION_ZONE - MovePointOutsideSpawnArea GlobalExclusionArea (flipped) - new vNewPoint = ") NET_PRINT_VECTOR(vNewPoint) NET_NL()
							ENDIF										
							vPoint = vNewPoint
						ENDIF
					ENDIF
					RETURN(TRUE)
				ELSE
					#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
							PRINTLN("[spawning] IS_POINT_IN_GLOBAL_EXCLUSION_ZONE - not inside spawn area") 
						ENDIF
					#ENDIF		
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF (g_SpawnData.bShowAdvancedSpew)
					NET_PRINT("[spawning] IS_POINT_IN_GLOBAL_EXCLUSION_ZONE - bIsActive = FALSE for area ") NET_PRINT_INT(i) NET_NL()
				ENDIF
			#ENDIF	
		ENDIF	
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_POINT_UNDER_GLOBAL_EXCLUSION_ZONE(VECTOR &vPoint, BOOL bMovePointOutsideZone=FALSE)
	INT i
	VECTOR vNewPoint
	REPEAT MAX_NUMBER_OF_GLOBAL_EXCLUSION_AREAS i 
		IF (GlobalExclusionArea[i].ExclusionArea.bIsActive = TRUE)								
			IF IsPointInsideSpawnArea(vPoint, GlobalExclusionArea[i].ExclusionArea, DEFAULT, FALSE, TRUE)
			AND NOT IsPointInsideSpawnArea(vPoint, GlobalExclusionArea[i].ExclusionArea, DEFAULT, FALSE, FALSE)
				#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
						NET_PRINT("[spawning] IS_POINT_UNDER_GLOBAL_EXCLUSION_ZONE - returning TRUE -  point under global occlusion area") NET_NL()
					ENDIF
				#ENDIF
				IF (bMovePointOutsideZone)
					IF (GlobalExclusionArea[i].bUseSpecificRespawnArea)
						vPoint = GlobalExclusionArea[i].vSpecificRespawnArea						
					ELSE
						vNewPoint = vPoint
						vNewPoint.z = GetSpawnAreaMiddleZ(GlobalExclusionArea[i].ExclusionArea)
						MovePointOutsideSpawnArea(vNewPoint, GlobalExclusionArea[i].ExclusionArea)
						NET_PRINT("[spawning] IS_POINT_UNDER_GLOBAL_EXCLUSION_ZONE - MovePointOutsideSpawnArea GlobalExclusionArea - new vNewPoint = ") NET_PRINT_VECTOR(vNewPoint) NET_NL()					
						// if new point is in another area then move out, but this time flip
						IF IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vNewPoint)
							vNewPoint = vPoint
							MovePointOutsideSpawnArea(vNewPoint, GlobalExclusionArea[i].ExclusionArea, DEFAULT, TRUE)
							NET_PRINT("[spawning] IS_POINT_UNDER_GLOBAL_EXCLUSION_ZONE - MovePointOutsideSpawnArea GlobalExclusionArea (flipped) - new vNewPoint = ") NET_PRINT_VECTOR(vNewPoint) NET_NL()
						ENDIF										
						vPoint = vNewPoint
					ENDIF
				ENDIF
				RETURN(TRUE)
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF (g_SpawnData.bShowAdvancedSpew)
					NET_PRINT("[spawning] IS_POINT_UNDER_GLOBAL_EXCLUSION_ZONE - bIsActive = FALSE for area ") NET_PRINT_INT(i) NET_NL()
				ENDIF
			#ENDIF	
		ENDIF	
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_SPAWN_RADIUS_LARGE(FLOAT fRadius)
	IF (fRadius > 50.0)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_SPAWN_ANGLED_AREA_LARGE(VECTOR vCoords1, VECTOR vCoords2, FLOAT fWidth)
	VECTOR vMin
	VECTOR vMax
	IF (vCoords1.x >  vCoords2.x)
		vMax.x = vCoords1.x
		vMin.x = vCoords2.x
	ELSE
		vMax.x = vCoords2.x
		vMin.x = vCoords1.x
	ENDIF
	IF (vCoords1.y >  vCoords2.y)
		vMax.y = vCoords1.y
		vMin.y = vCoords2.y
	ELSE
		vMax.y = vCoords2.y
		vMin.y = vCoords1.y
	ENDIF
	IF (vCoords1.z >  vCoords2.z)
		vMax.z = vCoords1.z
		vMin.z = vCoords2.z
	ELSE
		vMax.z = vCoords2.z
		vMin.z = vCoords1.z
	ENDIF	
	IF VMAG(vMax -vMin) > 100.0
		RETURN(TRUE)
	ENDIF
	IF (fWidth > 50.0)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL DOES_SPAWN_AREA_EXIST()
	INT i
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i
		IF (g_SpawnData.MissionSpawnDetails.SpawnArea[i].bIsActive)
			IF NOT IS_VECTOR_ZERO(g_SpawnData.MissionSpawnDetails.SpawnArea[i].vCoords1)
			OR NOT IS_VECTOR_ZERO(g_SpawnData.MissionSpawnDetails.SpawnArea[i].vCoords2)
			OR g_SpawnData.MissionSpawnDetails.SpawnArea[i].fFloat > 0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SPAWN_AREA_LARGE()
	INT i
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i
		IF (g_SpawnData.MissionSpawnDetails.SpawnArea[i].bIsActive)	
			SWITCH g_SpawnData.MissionSpawnDetails.SpawnArea[i].iShape	
				CASE SPAWN_AREA_SHAPE_CIRCLE		
					RETURN IS_SPAWN_RADIUS_LARGE(g_SpawnData.MissionSpawnDetails.SpawnArea[i].fFloat)
				BREAK	
				CASE SPAWN_AREA_SHAPE_BOX
					RETURN IS_SPAWN_ANGLED_AREA_LARGE(g_SpawnData.MissionSpawnDetails.SpawnArea[i].vCoords1, g_SpawnData.MissionSpawnDetails.SpawnArea[i].vCoords2, 0.0)					
				BREAK
				CASE SPAWN_AREA_SHAPE_ANGLED
					RETURN IS_SPAWN_ANGLED_AREA_LARGE(g_SpawnData.MissionSpawnDetails.SpawnArea[i].vCoords1, g_SpawnData.MissionSpawnDetails.SpawnArea[i].vCoords2, g_SpawnData.MissionSpawnDetails.SpawnArea[i].fFloat)
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC


FUNC BOOL IS_POINT_IN_MISSION_SPAWN_AREA(VECTOR vCoords, FLOAT fErrorMargin=0.01)
	INT i
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i
		IF (g_SpawnData.MissionSpawnDetails.SpawnArea[i].bIsActive)
			IF IsPointInsideSpawnArea(vCoords, g_SpawnData.MissionSpawnDetails.SpawnArea[i], fErrorMargin)
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC


FUNC INT GET_MISSION_SPAWN_AREA_POINT_IS_IN(VECTOR vCoords, FLOAT fErrorMargin=0.01)
	INT i
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i
		IF (g_SpawnData.MissionSpawnDetails.SpawnArea[i].bIsActive)
			IF IsPointInsideSpawnArea(vCoords, g_SpawnData.MissionSpawnDetails.SpawnArea[i], fErrorMargin)
				RETURN(i)
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(-1)
ENDFUNC


FUNC VECTOR GetCentrePointOfSpawnArea(SPAWN_AREA &SpArea)
	SWITCH SPArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			RETURN SpArea.vCoords1	
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
		CASE SPAWN_AREA_SHAPE_ANGLED
			RETURN (SpArea.vCoords1 + SpArea.vCoords2) * 0.5
		BREAK
	ENDSWITCH
	RETURN SpArea.vCoords1
ENDFUNC

FUNC BOOL DO_GLOBAL_EXCLUSION_ZONES_APPLY(VECTOR vInitialCoords)
	IF NOT (g_SpawnData.bIgnoreGlobalExclusionCheck)
	AND NOT (g_SpawnData.bIgnoreGlobalExclusionCheckForVehicleSpawning)	
	
		IF NOT (g_SpawnData.MissionSpawnDetails.bGlobalExclusionIsIgnored)
			
			IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
				//PRINTLN("[spawning] DO_GLOBAL_EXCLUSION_ZONES_APPLY - not on mission - returning TRUE")
				RETURN TRUE
			ENDIF
		
			
			IF NOT IS_POINT_IN_MISSION_SPAWN_AREA(vInitialCoords)
		
				IF NOT IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vInitialCoords) //g_SpawnData.vMyDeadCoords)
					//PRINTLN("[spawning] DO_GLOBAL_EXCLUSION_ZONES_APPLY - not in global exclusion zone - returning TRUE")
					RETURN(TRUE)
				ELSE
					// if dead coords are in global exclusion that always applies then return true.
					IF IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vInitialCoords, FALSE, TRUE) //g_SpawnData.vMyDeadCoords)
						//PRINTLN("[spawning] DO_GLOBAL_EXCLUSION_ZONES_APPLY - in permanent global exclusion zone - returning TRUE")
						RETURN(TRUE)
					ELSE
						//PRINTLN("[spawning] DO_GLOBAL_EXCLUSION_ZONES_APPLY - not in any global exclusion zone - returning FALSE")	
					ENDIF
				ENDIF
			ELSE
				// if the centre of the spawn area is not in an exclusion area, then check if teh initial coords are inside.
				INT iMissionSpawnAreaIndex = GET_MISSION_SPAWN_AREA_POINT_IS_IN(vInitialCoords)
				IF (iMissionSpawnAreaIndex > -1)				
					VECTOR vCentre = GetCentrePointOfSpawnArea(g_SpawnData.MissionSpawnDetails.SpawnArea[iMissionSpawnAreaIndex])			
					IF NOT IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vCentre)
						IF NOT IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vInitialCoords)
							RETURN(TRUE)
						ENDIF
					ENDIF				
				ENDIF
				
				//PRINTLN("[spawning] DO_GLOBAL_EXCLUSION_ZONES_APPLY - point is in mission spawn area - returning FALSE")		
			ENDIF
		ELSE
			//PRINTLN("[spawning] DO_GLOBAL_EXCLUSION_ZONES_APPLY - g_SpawnData.MissionSpawnDetails.bGlobalExclusionIsIgnored - returning FALSE")		
		ENDIF
	ELSE
		//PRINTLN("[spawning] DO_GLOBAL_EXCLUSION_ZONES_APPLY - g_SpawnData.bIgnoreGlobalExclusionCheck = TRUE - returning FALSE")			
	ENDIF
		
	RETURN(FALSE)
ENDFUNC


FUNC INT GetFreeMissionExclusionIndex()
	INT i
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS i
		IF NOT (g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].bIsActive)
			RETURN(i)
		ENDIF
	ENDREPEAT
	RETURN(-1)
ENDFUNC

FUNC INT GetTotalMissionExclusionAreas()
	INT i
	INT iCount
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS i
		IF (g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].bIsActive)
			iCount++
		ENDIF
	ENDREPEAT	
	RETURN(iCount)
ENDFUNC

FUNC BOOL PointIsInNearExclusionRadius(VECTOR vPoint)
	IF (g_SpawnData.fNearExclusionRadius > 0.0)
		FLOAT fDist
		fDist = VDIST(vPoint, g_SpawnData.vExclusionCoord)
		IF (fDist < g_SpawnData.fNearExclusionRadius)
			#IF IS_DEBUG_BUILD
				IF (g_SpawnData.bShowAdvancedSpew)
					NET_PRINT("[spawning] PointIsInNearExclusionRadius - returning TRUE - fDist < g_SpawnData.fNearExclusionRadius, fDist = ") NET_PRINT_FLOAT(fDist) NET_NL()
				ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL PointIsInMissionExclusionZone(VECTOR vPoint, INT &iReturnedAreaIndex)
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				NET_PRINT("[spawning] PointIsInMissionExclusionZone - returning FALSE player is walking in or out of simple interior ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	INT i
	INT iTotalMissionExlcusionAreas = GetTotalMissionExclusionAreas()
	REPEAT iTotalMissionExlcusionAreas i 
		IF (g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].bIsActive = TRUE)					
			IF IsPointInsideSpawnArea(vPoint, g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i])
				#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
						NET_PRINT("[spawning] PointIsInMissionExclusionZone - returning TRUE -  point inside mission occlusion area") NET_NL()
					ENDIF
				#ENDIF		
				iReturnedAreaIndex = i
				RETURN(TRUE)
			ENDIF		
		ENDIF	
	ENDREPEAT
	RETURN(FALSE)	
ENDFUNC

FUNC BOOL IS_POINT_IN_MISSION_EXCLUSION_ZONE(VECTOR &vPoint, BOOL bMovePointOutsideZone=FALSE, BOOL bUseCarNodesForMove=FALSE)

	INT i, j
	VECTOR vNewPoint
	
	IF PointIsInMissionExclusionZone(vPoint, i)
		IF (bMovePointOutsideZone)
			vNewPoint = vPoint
			MovePointOutsideSpawnArea(vNewPoint, g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i], DEFAULT, DEFAULT, bUseCarNodesForMove)
			NET_PRINT("[spawning] IS_POINT_IN_MISSION_EXCLUSION_ZONE - MovePointOutsideSpawnArea g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea - new vNewPoint = ") NET_PRINT_VECTOR(vNewPoint) NET_NL()
			
			// if new point is in another area then move out, but this time flip
			IF PointIsInMissionExclusionZone(vNewPoint, j)
			OR PointIsInNearExclusionRadius(vNewPoint)
				vNewPoint = vPoint
				MovePointOutsideSpawnArea(vNewPoint, g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i], DEFAULT, TRUE, bUseCarNodesForMove)
				NET_PRINT("[spawning] IS_POINT_IN_MISSION_EXCLUSION_ZONE - MovePointOutsideSpawnArea g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea (flipped) - new vNewPoint = ") NET_PRINT_VECTOR(vNewPoint) NET_NL()
			ENDIF
			
			vPoint = vNewPoint			
		ENDIF		
		RETURN TRUE
	ENDIF
	RETURN FALSE

ENDFUNC



FUNC BOOL IS_POINT_IN_NEAR_EXCLUSION_RADIUS(VECTOR &vPoint, BOOL bMovePointOutsideZone=FALSE)
	
	INT i
	VECTOR vNewPoint
	
	FLOAT fHeadingOffset
	
	IF (PointIsInNearExclusionRadius(vPoint))
		IF (bMovePointOutsideZone)
			vNewPoint = vPoint
			
			fHeadingOffset = GET_RANDOM_FLOAT_IN_RANGE(0.01, 360.0)
			
			MovePointOutsideSphere(vNewPoint, g_SpawnData.vExclusionCoord, g_SpawnData.fNearExclusionRadius, DEFAULT, FALSE, fHeadingOffset)
			PRINTLN("[spawning]IS_POINT_IN_NEAR_EXCLUSION_RADIUS - MovePointOutsideSphere vExclusionCoord = ", g_SpawnData.vExclusionCoord, ", fNearExclusionRadius = ", g_SpawnData.fNearExclusionRadius, ", fHeadingOffset = ", fHeadingOffset ,", vNewPoint = ", vNewPoint) 
			
			// if new point is in another area then move out, but this time flip
			IF PointIsInMissionExclusionZone(vNewPoint, i)
			OR PointIsInNearExclusionRadius(vNewPoint)
				vNewPoint = vPoint
				MovePointOutsideSphere(vNewPoint, g_SpawnData.vExclusionCoord, g_SpawnData.fNearExclusionRadius, DEFAULT, TRUE, fHeadingOffset)
				PRINTLN("[spawning]IS_POINT_IN_NEAR_EXCLUSION_RADIUS - MovePointOutsideSphere (flipped) vExclusionCoord = ", g_SpawnData.vExclusionCoord, ", fNearExclusionRadius = ", g_SpawnData.fNearExclusionRadius, ", fHeadingOffset = ", fHeadingOffset ,", vNewPoint = ", vNewPoint)
			ENDIF
			vPoint = vNewPoint
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC



PROC MovePointInsidePlayableZone(VECTOR &vPoint, FLOAT fDist=100.0)

	IF (vPoint.x < PLAYABLE_ZONE_MIN_X)
		vPoint.x = PLAYABLE_ZONE_MIN_X + fDist
		NET_PRINT("[spawning] MovePointInsidePlayableZone - moved point inside playable zone, min x, new point = ") NET_PRINT_VECTOR(vPoint) NET_NL()
	ENDIF
	IF (vPoint.x > PLAYABLE_ZONE_MAX_X)
		vPoint.x = PLAYABLE_ZONE_MAX_X - fDist
		NET_PRINT("[spawning] MovePointInsidePlayableZone - moved point inside playable zone, max x, new point = ") NET_PRINT_VECTOR(vPoint) NET_NL()
	ENDIF	
	IF (vPoint.y < PLAYABLE_ZONE_MIN_Y)
		vPoint.y = PLAYABLE_ZONE_MIN_Y + fDist
		NET_PRINT("[spawning] MovePointInsidePlayableZone - moved point inside playable zone, min y, new point = ") NET_PRINT_VECTOR(vPoint) NET_NL()
	ENDIF
	IF (vPoint.y > PLAYABLE_ZONE_MAX_Y)
		vPoint.y = PLAYABLE_ZONE_MAX_Y - fDist
		NET_PRINT("[spawning] MovePointInsidePlayableZone - moved point inside playable zone, max y, new point = ") NET_PRINT_VECTOR(vPoint) NET_NL()
	ENDIF	
	
ENDPROC

PROC ShiftPointOutOfAnyExclusions(VECTOR &vPoint, BOOL bCheckGlobalExclusion, BOOL bCheckMissionExclusion, BOOL bCheckNearExclusion, BOOL bCheckOtherPlayers, BOOL bIncludeAirSpaceBelowForGlobalExclusions=TRUE)

	NET_PRINT("[spawning] ShiftPointOutOfAnyExclusions - start point = ") NET_PRINT_VECTOR(vPoint) NET_NL()

	IF (bCheckGlobalExclusion)
		IF DO_GLOBAL_EXCLUSION_ZONES_APPLY(g_SpawnData.vMyDeadCoords)
			IF IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vPoint, TRUE, DEFAULT, DEFAULT, bIncludeAirSpaceBelowForGlobalExclusions)
				NET_PRINT("[spawning] ShiftPointOutOfAnyExclusions - moved outside global exclusion, new point = ") NET_PRINT_VECTOR(vPoint) NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	IF (bCheckMissionExclusion)
		IF IS_POINT_IN_MISSION_EXCLUSION_ZONE(vPoint, TRUE)
			NET_PRINT("[spawning] ShiftPointOutOfAnyExclusions - moved outside mission exclusion, new point = ") NET_PRINT_VECTOR(vPoint) NET_NL()
		ENDIF
	ENDIF
	
	IF (bCheckNearExclusion)
		IF IS_POINT_IN_NEAR_EXCLUSION_RADIUS(vPoint, TRUE)
			NET_PRINT("[spawning] ShiftPointOutOfAnyExclusions - moved outside near exclusion, new point = ") NET_PRINT_VECTOR(vPoint) NET_NL()
		ENDIF
	ENDIF

	IF (bCheckOtherPlayers)
		MovePointOutsideAnyOtherSpawningPlayer(vPoint, 7.0, PLAYER_ID())
	ENDIF
	
	MovePointInsidePlayableZone(vPoint)

ENDPROC

PROC ShiftZInMinHeightArea(VECTOR &vPoint)
	
	INT i
	REPEAT NUMBER_OF_MIN_AIRPLANE_HEIGHT_AREAS i
		IF IS_POINT_IN_ANGLED_AREA(vPoint, ProblemArea_MinAirplaneHeight[i].vCoords1, ProblemArea_MinAirplaneHeight[i].vCoords2, ProblemArea_MinAirplaneHeight[i].fFloat)			
			IF (ProblemArea_MinAirplaneHeight[i].vCoords2.z > ProblemArea_MinAirplaneHeight[i].vCoords1.z)
				vPoint.z = 	ProblemArea_MinAirplaneHeight[i].vCoords2.z
			ELSE
				vPoint.z = 	ProblemArea_MinAirplaneHeight[i].vCoords1.z
			ENDIF
			NET_PRINT("[spawning] ShiftZInMinHeightArea - point in min Z area, shifting up. New coords=") NET_PRINT_VECTOR(vPoint) NET_NL()
		ENDIF
	ENDREPEAT

ENDPROC

PROC MakePotentialSpawnPointInAirSafe(VECTOR &vPos, BOOL bAlreadyInMissionSpawnArea=FALSE)
	
	FLOAT fMinZ
	
	IF NOT (bAlreadyInMissionSpawnArea)
		PRINTLN("[spawning] MakePotentialSpawnPointInAirSafe - bAlreadyInMissionSpawnArea = FALSE")
		ShiftPointOutOfAnyExclusions(vPos, TRUE, TRUE, TRUE, TRUE)	
	ELSE
		PRINTLN("[spawning] MakePotentialSpawnPointInAirSafe - bAlreadyInMissionSpawnArea = TRUE")
		ShiftPointOutOfAnyExclusions(vPos, TRUE, TRUE, TRUE, TRUE, FALSE)	
	ENDIF

	// set the z to safely above the ground
	fMinZ = GET_APPROX_HEIGHT_FOR_POINT(vPos.x, vPos.y)
	fMinZ += 100.0

	PRINTLN("[spawning] MakePotentialSpawnPointInAirSafe - fMinZ = ", fMinZ)
	
	IF (vPos.z < fMinZ)
		vPos.z = fMinZ
	ENDIF	

	ShiftZInMinHeightArea(vPos)
ENDPROC

FUNC BOOL IsPotentialSpawnPointInAirSafe(VECTOR &vPos, BOOL bAlreadyInMissionSpawnArea=FALSE)
	MakePotentialSpawnPointInAirSafe(vPos, bAlreadyInMissionSpawnArea)
	IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos,6,1,1,5,FALSE,FALSE,FALSE,120,FALSE,-1,TRUE,0.0)
		PRINTLN("[spawning] IsPotentialSpawnPointInAirSafe - returning = ", vPos)
		RETURN TRUE
	ELSE
		PRINTLN("[spawning] IsPotentialSpawnPointInAirSafe - failed with ", vPos) 
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VECTOR GetPotentialSpawnPointInAirForLocalPlayer(VECTOR vAreaCoords1, VECTOR vAreaCoords2, FLOAT fAreaFloat, INT iShape)

	INT i
	VECTOR vPos
	
	REPEAT 10 i
	
		SWITCH iShape
			CASE SPAWN_AREA_SHAPE_CIRCLE
				vPos = GetRandomPointInCircle(vAreaCoords1, fAreaFloat)
			BREAK
			CASE SPAWN_AREA_SHAPE_BOX
				vPos = GetRandomPointInBox(vAreaCoords1, vAreaCoords2)
			BREAK
			CASE SPAWN_AREA_SHAPE_ANGLED
				vPos = GetRandomPointInAngledArea(vAreaCoords1, vAreaCoords2, fAreaFloat)
			BREAK
		ENDSWITCH
			
	
		IF IsPotentialSpawnPointInAirSafe(vPos)
			NET_PRINT("[spawning] GetPotentialSpawnPointInAirForLocalPlayer - returning = ") NET_PRINT_VECTOR(vPos) NET_NL()
			RETURN vPos
		ELSE
			NET_PRINT("[spawning] GetPotentialSpawnPointInAirForLocalPlayer - failed with ") NET_PRINT_VECTOR(vPos) NET_PRINT(", i = ") NET_PRINT_INT(i) NET_NL()
		ENDIF
		
	ENDREPEAT

	NET_PRINT("[spawning] GetPotentialSpawnPointInAirForLocalPlayer - fallback returning = ") NET_PRINT_VECTOR(vPos) NET_NL()
	RETURN vPos

ENDFUNC



FUNC BOOL IS_PLAYER_IN_MISSION_SPAWN_AREA()
	RETURN IS_POINT_IN_MISSION_SPAWN_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
ENDFUNC

FUNC BOOL IsBoxInsideBox(VECTOR vMin1, VECTOR vMax1, VECTOR vMin2, VECTOR vMax2)
	IF (vMin1.x >= vMin2.x)
	AND (vMin1.y >= vMin2.y)
	AND (vMin1.z >= vMin2.z)
	AND (vMax1.x <= vMax2.x)
	AND (vMax1.y <= vMax2.y)
	AND (vMax1.z <= vMax2.z)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsCircleInsideCircle(VECTOR vCircle1, FLOAT fRadius1, VECTOR vCircle2, FLOAT fRadius2)
	VECTOR vec
	vec = vCircle2 - vCircle1
	IF (VMAG(vec) + fRadius1 < fRadius2)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsCircleInsideBox(VECTOR vCircle, FLOAT fRadius, VECTOR vMin, VECTOR vMax)
	VECTOR vCircleMin
	VECTOR vCircleMax
	vCircleMin = vCircle - <<fRadius, fRadius, fRadius>>
	vCircleMax = vCircle + <<fRadius, fRadius, fRadius>>
	RETURN(IsBoxInsideBox(vCircleMin, vCircleMax, vMin, vMax))
ENDFUNC

FUNC BOOL IsCircleInsideAngledArea(VECTOR vCircle, FLOAT fRadius, VECTOR vCoords1, VECTOR vCoords2, FLOAT fWidth)
	VECTOR vCircleOffset[4]

	// north
	vCircleOffset[0] = vCircle + <<0.0, fRadius, 0.0>>
	
	// south
	vCircleOffset[1] = vCircle + <<0.0, -1.0 * fRadius, 0.0>>
	
	// east
	vCircleOffset[2] = vCircle + <<fRadius, 0.0, 0.0>>
	
	// west
	vCircleOffset[3] = vCircle + <<-1.0 * fRadius, 0.0, 0.0>>
	
	INT i
	REPEAT 4 i
		IF NOT IS_POINT_IN_ANGLED_AREA(	vCircleOffset[i] , vCoords1, vCoords2, fWidth)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IsCircleInsideSpawnArea(VECTOR vCentre, FLOAT fRadius, SPAWN_AREA &SPArea)

	SWITCH SPArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			RETURN IsCircleInsideCircle(vCentre, fRadius, SPArea.vCoords1, SPArea.fFloat)	
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			RETURN IsCircleInsideBox(vCentre, fRadius, SPArea.vCoords1, SPArea.vCoords2)	
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			RETURN IsCircleInsideAngledArea(vCentre, fRadius, SPArea.vCoords1, SPArea.vCoords2, SPArea.fFloat)
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IsBoxInsideCircle(VECTOR vMin, VECTOR vMax, VECTOR vCentre, FLOAT fRadius)

	VECTOR vec1
	vec1 = vMin - vCentre
	
	VECTOR vec2
	vec2 = vMax - vCentre
	
	IF (VMAG(vec1) < fRadius)
	AND (VMAG(vec2) < fRadius)
		RETURN(TRUE)
	ENDIF

	RETURN FALSE

ENDFUNC


FUNC BOOL IsBoxInsideAngledArea(VECTOR vMin, VECTOR vMax, VECTOR vCoords1, VECTOR vCoords2, FLOAT fWidth)
	VECTOR vOffset[8]
	
	vOffset[0] = <<vMin.x, vMin.y, vMin.z>>
	vOffset[1] = <<vMin.x, vMin.y, vMax.z>>
	vOffset[2] = <<vMin.x, vMax.y, vMax.z>>
	vOffset[3] = <<vMin.x, vMax.y, vMin.z>>
	
	vOffset[4] = <<vMax.x, vMin.y, vMin.z>>
	vOffset[5] = <<vMax.x, vMin.y, vMax.z>>
	vOffset[6] = <<vMax.x, vMax.y, vMax.z>>
	vOffset[7] = <<vMax.x, vMax.y, vMin.z>>
	
	INT i
	REPEAT 8 i
		IF NOT IS_POINT_IN_ANGLED_AREA(	vOffset[i] , vCoords1, vCoords2, fWidth)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC PopulateAngledAreaCorners(VECTOR vCoord1, VECTOR vCoord2, FLOAT fWidth, VECTOR &vCorner[8])
	
	IF (vCoord1.z = vCoord2.z)
		vCoord2.z += 0.01
	ENDIF

	// find corner points of angled area
	VECTOR vec = vCoord1 - vCoord2
	VECTOR vCross = CROSS_PRODUCT(vec, <<vec.x, vec.y, 0.0>>)

	vCross /= VMAG(vCross)
	vCross *= fWidth * 0.5
	
	FLOAT fMinZ, fMaxX
	
	IF (vCoord1.z > vCoord2.z)
		fMinZ = vCoord2.z
		fMaxX = vCoord1.z
	ELSE
		fMinZ = vCoord1.z
		fMaxX = vCoord2.z
	ENDIF
	
	vCorner[0] = <<vCoord1.x, vCoord1.y, fMinZ>> + vCross
	vCorner[1] = <<vCoord1.x, vCoord1.y, fMinZ>> - vCross
	vCorner[2] = <<vCoord1.x, vCoord1.y, fMaxX>> - vCross	
	vCorner[3] = <<vCoord1.x, vCoord1.y, fMaxX>> + vCross	
	vCorner[4] = <<vCoord2.x, vCoord2.y, fMinZ>> + vCross
	vCorner[5] = <<vCoord2.x, vCoord2.y, fMinZ>> - vCross
	vCorner[6] = <<vCoord2.x, vCoord2.y, fMaxX>> - vCross
	vCorner[7] = <<vCoord2.x, vCoord2.y, fMaxX>> + vCross
	
ENDPROC

FUNC BOOL IsAngledAreaInsideCircle(VECTOR vCoord1, VECTOR vCoord2, FLOAT fWidth, VECTOR vCircle, FLOAT fRadius)

	VECTOR vCorner[8]
	PopulateAngledAreaCorners(vCoord1, vCoord2, fWidth, vCorner)
	
	INT i
	VECTOR vec
	REPEAT 8 i 
		vec = vCircle - vCorner[i]
		IF NOT (VMAG(vec) < fRadius)
			RETURN FALSE
		ENDIF	
	ENDREPEAT

	RETURN TRUE

ENDFUNC


FUNC BOOL IsAngledAreaInsideBox(VECTOR vCoord1, VECTOR vCoord2, FLOAT fWidth, VECTOR vMin, VECTOR vMax)

	VECTOR vCorner[8]
	PopulateAngledAreaCorners(vCoord1, vCoord2, fWidth, vCorner)
	
	INT i
	REPEAT 8 i 
		IF (vCorner[i].x > vMin.x)
		AND (vCorner[i].x < vMax.x)
		AND (vCorner[i].y > vMin.y)
		AND (vCorner[i].y < vMax.y)
		AND (vCorner[i].z > vMin.z)
		AND (vCorner[i].z < vMax.z)
			// inside
		ELSE
			RETURN FALSE
		ENDIF
	ENDREPEAT

	RETURN TRUE

ENDFUNC

FUNC BOOL IsAngledAreaInsideAngledArea(VECTOR vCoordA1, VECTOR vCoordA2, FLOAT fWidthA, VECTOR vCoordB1, VECTOR vCoordB2, FLOAT fWidthB)

	VECTOR vCorner[8]
	PopulateAngledAreaCorners(vCoordA1, vCoordA2, fWidthA, vCorner)
	
	INT i
	REPEAT 8 i 
	
		//PRINTLN("IsAngledAreaInsideAngledArea - vCorner[", i, "] = ", vCorner[i])
	
		IF NOT IS_POINT_IN_ANGLED_AREA(	vCorner[i] , vCoordB1, vCoordB2, fWidthB)
		
			//PRINTLN("IsAngledAreaInsideAngledArea - returning FALSE")
			RETURN FALSE
		ENDIF
	ENDREPEAT

	RETURN TRUE

ENDFUNC


FUNC BOOL IsBoxInsideSpawnArea(VECTOR vMin, VECTOR vMax, SPAWN_AREA &SPArea)

	SWITCH SPArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			RETURN IsBoxInsideCircle(vMin, vMax, SPArea.vCoords1, SPArea.fFloat)	
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			RETURN IsBoxInsideBox(vMin, vMax, SPArea.vCoords1, SPArea.vCoords2)	
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			RETURN IsBoxInsideAngledArea(vMin, vMax, SPArea.vCoords1, SPArea.vCoords2, SPArea.fFloat)
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IsAngledAreaInsideSpawnArea(VECTOR vCoord1, VECTOR vCoord2, FLOAT fWidth, SPAWN_AREA &SPArea)

	SWITCH SPArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			RETURN IsAngledAreaInsideCircle(vCoord1, vCoord2, fWidth, SPArea.vCoords1, SPArea.fFloat)	
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			RETURN IsAngledAreaInsideBox(vCoord1, vCoord2, fWidth, SPArea.vCoords1, SPArea.vCoords2)
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			RETURN IsAngledAreaInsideAngledArea(vCoord1, vCoord2, fWidth, SPArea.vCoords1, SPArea.vCoords2, SPArea.fFloat)
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_SPAWN_AREA_ENTIRELY_INSIDE_SPAWN_AREA(SPAWN_AREA &InsideSA, SPAWN_AREA &OutsideSA)

	SWITCH InsideSA.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			RETURN IsCircleInsideSpawnArea(InsideSA.vCoords1, InsideSA.fFloat, OutsideSA)
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			RETURN IsBoxInsideSpawnArea(InsideSA.vCoords1, InsideSA.vCoords2, OutsideSA)
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			RETURN IsAngledAreaInsideSpawnArea(InsideSA.vCoords1, InsideSA.vCoords2, InsideSA.fFloat, OutsideSA)
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA(SPAWN_AREA &InsideSA, VECTOR vCoords1, VECTOR vCoords2, FLOAT fWidth)

	PRINTLN("[spawning] IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA - called with:")
	#IF IS_DEBUG_BUILD
	DEBUG_PRINT_SPAWN_AREA_DETAILS(InsideSA)
	#ENDIF
	PRINTLN("[spawning] IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA - vCoords1 = ", vCoords1, ", vCoords2 = ", vCoords2, ", fWidth = ", fWidth )
	
	SWITCH InsideSA.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			IF IsCircleInsideAngledArea(InsideSA.vCoords1, InsideSA.fFloat, vCoords1, vCoords2, fWidth)
				PRINTLN("[spawning] IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA - true for circle. ", InsideSA.vCoords1, ", ", InsideSA.fFloat, ", ", vCoords1, ", ", vCoords2, ", ", fWidth)
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			IF IsBoxInsideAngledArea(InsideSA.vCoords1, InsideSA.vCoords2, vCoords1, vCoords2, fWidth)
				PRINTLN("[spawning] IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA - true for box. ", InsideSA.vCoords1, ", ", InsideSA.vCoords2, ", ", vCoords1, ", ", vCoords2, ", ", fWidth)
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			IF IsAngledAreaInsideAngledArea(InsideSA.vCoords1, InsideSA.vCoords2, InsideSA.fFloat, vCoords1, vCoords2, fWidth)
				PRINTLN("[spawning] IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA - true for angled area. ", InsideSA.vCoords1, ", ", InsideSA.vCoords2, ", ", InsideSA.fFloat, ", ", vCoords1, ", ", vCoords2, ", ", fWidth)
				RETURN(TRUE)
			ENDIF
		BREAK
	ENDSWITCH
	
	PRINTLN("[spawning] IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA - false")
	RETURN FALSE

ENDFUNC



