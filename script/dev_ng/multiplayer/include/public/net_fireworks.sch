// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	net_fireworks.sch
//		AUTHOR			:	Martin McMillan
//		DESCRIPTION		:   Firework header for multiplayer
//
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//	INCLUDES
//----------------------
USING "globals.sch"
USING "commands_network.sch"
USING "net_include.sch"
USING "rage_builtins.sch"
USING "net_mission.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_event.sch"
//
////----------------------
////	CONSTANTS
////----------------------
//CONST_INT NO_FIREWORK_TYPES		4
//CONST_INT NO_FIREWORK_COLOURS	3
//CONST_INT MAX_FIREWORK_TIMER	60
//
//
//CONST_INT ST_INIT				0
//CONST_INT ST_WAITING_RESULT		1
//CONST_INT ST_RESULT_READY		2
//
////----------------------
////	Local Bitset
////----------------------
//
////fireworkVars.iFireworkLocalBitset
//CONST_INT NOT_SAFE_TO_PLACE		0
//CONST_INT DRAW_LAUNCH_TIMER		1
//CONST_INT CHECK_CREATE_FIREWORK	2
//CONST_INT CHECK_DETACH_FIREWORK	3
//CONST_INT DO_SHOCKING_EVENT		4
//
////fireworkVars.iFireworkBitSet
//CONST_INT biShapeTestResultsDone		0
//CONST_INT biShapeTestUpperResultsDone	1
//CONST_INT biShapeTestFailed				2
//
////----------------------
////	STRUCTS
////----------------------
//STRUCT FIREWORK
//	OBJECT_INDEX obj
//	INT iType
//	INT iColour
//	INT iTimer
//	BOOL bPlaced
//	BOOL bDoShapeTest
//	VECTOR vLocation
//	VECTOR vRocketStart
//ENDSTRUCT
//
//STRUCT FIREWORK_VARIABLES
//	INT iNoPlacedFireworks
//
//	FIREWORK fireworks[MAX_OWNED_FIREWORKS]
//
//	SHAPETEST_INDEX sti_Firework
//	SHAPETEST_INDEX sti_FireworkUpper
//
//	INT iShapeTestStage = ST_INIT
//
//	SCRIPT_TIMER launchTimer
//	INT iNextFirework
//	INT iElapsedTime
//
//	INT iShockingEvent
//	VECTOR vShockingEventPos
//
//	INT iRocketCleanupBitSet
//	INT iRocketToCheck
//
//	INT iNewFirework 
//	
//	INT iFireworkLocalBitset
//
//	INT iFireworkBitSet
//
//	SCRIPT_TIMER iRocketCleanupTime[MAX_OWNED_FIREWORKS]
//
//	#IF IS_DEBUG_BUILD
//	VECTOR stDebug1
//	VECTOR stDebug2
//	VECTOR stDebug3
//	VECTOR stDebug4
//	#ENDIF
//	
//ENDSTRUCT
//
///// PURPOSE:
/////    Increments the quantity of a given firework type 
//PROC DECREMENT_FIREWORK_TYPE_QUANTITY(int iType,int iColour)
//	SWITCH(iType)
//		CASE 0
//			SWITCH(iColour)
//				CASE 0
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_RED)
//				BREAK
//				CASE 1
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_WHITE)
//				BREAK
//				CASE 2
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_BLUE)
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE 1
//			SWITCH(iColour)
//				CASE 0
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_RED)
//				BREAK
//				CASE 1
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_WHITE)
//				BREAK
//				CASE 2
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_BLUE)
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE 2
//			SWITCH(iColour)
//				CASE 0
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_RED)
//				BREAK
//				CASE 1
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_WHITE)
//				BREAK
//				CASE 2
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_BLUE)
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE 3
//			SWITCH(iColour)
//				CASE 0
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_RED)
//				BREAK
//				CASE 1
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_WHITE)
//				BREAK
//				CASE 2
//					DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_BLUE)
//				BREAK
//			ENDSWITCH
//		BREAK
//	ENDSWITCH
//ENDPROC
//
///// PURPOSE:
///////    Gets the number of owned fireworks of a given type 
////FUNC INT GET_NO_OWNED_FIREWORK_TYPE(int iType)
////	SWITCH(iType)
////		CASE 0
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_RED)
////		BREAK
////		CASE 1
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_WHITE)
////		BREAK
////		CASE 2
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_BLUE)
////		BREAK
////		CASE 3
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_RED)
////		BREAK
////		CASE 4
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_WHITE)
////		BREAK
////		CASE 5
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_BLUE)
////		BREAK
////		CASE 6
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_RED)
////		BREAK
////		CASE 7
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_WHITE)
////		BREAK
////		CASE 8
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_BLUE)
////		BREAK
////		CASE 9
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_RED)
////		BREAK
////		CASE 10
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_WHITE)
////		BREAK
////		CASE 11
////			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_BLUE)
////		BREAK
////	ENDSWITCH
////	RETURN 0
////ENDFUNC
//
/////// PURPOSE:
///////    Gets the number of owned fireworks of a given type 
//FUNC INT GET_NO_OWNED_FIREWORK_TYPE_AND_COLOUR(int iType,int iColour)
//	SWITCH(iType)
//		CASE 0
//			SWITCH(iColour)
//				CASE 0
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_RED)
//				BREAK
//				CASE 1
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_WHITE)
//				BREAK
//				CASE 2
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_BLUE)
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE 1
//			SWITCH(iColour)
//				CASE 0
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_RED)
//				BREAK
//				CASE 1
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_WHITE)
//				BREAK
//				CASE 2
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_BLUE)
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE 2
//			SWITCH(iColour)
//				CASE 0
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_RED)
//				BREAK
//				CASE 1
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_WHITE)
//				BREAK
//				CASE 2
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_BLUE)
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE 3
//			SWITCH(iColour)
//				CASE 0
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_RED)
//				BREAK
//				CASE 1
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_WHITE)
//				BREAK
//				CASE 2
//					RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_BLUE)
//				BREAK
//			ENDSWITCH
//		BREAK
//	ENDSWITCH
//	RETURN 0
//ENDFUNC
//
/////// PURPOSE:
///////    Gets the total number of owned fireworks 
//FUNC INT GET_NO_OWNED_FIREWORK_TOTAL()
//	INT iTotal
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_RED)
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_WHITE)
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_1_BLUE)
//	
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_RED)
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_WHITE)
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_2_BLUE)
//	
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_RED)
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_WHITE)
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_3_BLUE)
//	
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_RED)
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_WHITE)
//	iTotal += GET_MP_INT_CHARACTER_STAT(MP_STAT_FIREWORK_TYPE_4_BLUE)
//
//	PRINTLN("net_fireworks - GET_NO_OWNED_FIREWORK_TOTAL = ", iTotal)
//	RETURN iTotal
//ENDFUNC
//
///// PURPOSE:
/////    Gets the anim name of a given firework type 
//FUNC STRING GET_FIREWORK_TYPE_ANIM_NAME(INT iType)
//	SWITCH(iType)
//		CASE 0
//			RETURN "PLACE_FIREWORK_4_CONE" 		//Cone
//		BREAK
//		CASE 1
//			RETURN "PLACE_FIREWORK_3_BOX" 		//Box
//		BREAK
//		CASE 2
//			RETURN "PLACE_FIREWORK_2_CYLINDER" 	//Cylinder
//		BREAK
//		CASE 3
//			RETURN "PLACE_FIREWORK_1_ROCKET" 	//Rocket
//		BREAK
//	ENDSWITCH
//	RETURN ""
//ENDFUNC
//
///// PURPOSE:
/////    Gets the model name of a given firework type 
//FUNC MODEL_NAMES GET_FIREWORK_TYPE_MODEL_NAME(int iType)
//	SWITCH(iType)
//		CASE 0
//			RETURN IND_PROP_FIREWORK_04 //Cone
//		BREAK
//		CASE 1
//			RETURN IND_PROP_FIREWORK_02 //Box
//		BREAK
//		CASE 2
//			RETURN IND_PROP_FIREWORK_03 //Cylinder
//		BREAK
//		CASE 3
//			RETURN IND_PROP_FIREWORK_01 //Rocket
//		BREAK
//	ENDSWITCH
//	RETURN IND_PROP_FIREWORK_01
//ENDFUNC
//
///// PURPOSE:
/////    Gets the model name of a given firework type 
//FUNC STRING GET_FIREWORK_TYPE_VFX_NAME(int iType)
//	SWITCH(iType)
//		CASE 0
//			RETURN "scr_indep_firework_fountain" //Cone
//		BREAK
//		CASE 1
//			RETURN "scr_indep_firework_shotburst" //Box
//		BREAK
//		CASE 2
//			RETURN "scr_indep_firework_starburst" //Cylinder
//		BREAK
//		CASE 3
//			RETURN "scr_indep_firework_trailburst" //Rocket
//		BREAK
//	ENDSWITCH
//	RETURN "scr_indep_firework_fountain"
//ENDFUNC
//
///// PURPOSE:
/////    Finds the next empty firework slot 
//FUNC INT FIND_UNALLOCATED_FIREWORK(FIREWORK_VARIABLES &fireworkVars)
//	INT iTemp
//	
//	REPEAT MAX_OWNED_FIREWORKS iTemp
//		IF fireworkVars.fireworks[iTemp].bPlaced = FALSE
//			RETURN iTemp
//		ENDIF
//	ENDREPEAT
//	
//	RETURN -1
//ENDFUNC
//
///// PURPOSE:
/////    Creates and places a firework on the ground 
//PROC PLAY_FIREWORK_ANIM(FIREWORK &fireworkData,FIREWORK_VARIABLES &fireworkVars)
//	println("net_fireworks - PLAY_FIREWORK_ANIM iType: ",fireworkData.iType)
//	IF GET_NO_OWNED_FIREWORK_TYPE_AND_COLOUR(fireworkData.iType,fireworkData.iColour) > 0
//		IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),"anim@mp_fireworks", GET_FIREWORK_TYPE_ANIM_NAME(fireworkData.iType))
//			IF FIND_UNALLOCATED_FIREWORK(fireworkVars) != -1
//				TASK_PLAY_ANIM(PLAYER_PED_ID(),"anim@mp_fireworks", GET_FIREWORK_TYPE_ANIM_NAME(fireworkData.iType), DEFAULT, DEFAULT, DEFAULT, AF_HIDE_WEAPON)
//				SET_BIT(fireworkVars.iFireworkLocalBitset,CHECK_CREATE_FIREWORK)
//				SET_BIT(fireworkVars.iFireworkLocalBitset,CHECK_DETACH_FIREWORK)
//				PRINTLN("net_fireworks - PLAY_FIREWORK_ANIM - ANIM PLAYED - FIREWORK SLOT FOUND")
//			ELSE
//				PRINTLN("net_fireworks - PLAY_FIREWORK_ANIM - NO FREE FIREWORK SLOT FOUND")
//			ENDIF
//		ENDIF	
//	ENDIF
//ENDPROC
//
//PROC DESTROY_FIREWORK(FIREWORK &fw,FIREWORK_VARIABLES &fireworkVars, BOOL bDelete = FALSE)
//	PRINTLN("net_fireworks - DESTROY_FIREWORK - iType = ", fw.iType, " iColour = ", fw.iColour, " iTimer = ", fw.iTimer)
//	IF DOES_ENTITY_EXIST(fw.obj)
//		IF bDelete = FALSE
//			SET_OBJECT_AS_NO_LONGER_NEEDED(fw.obj)
//		ELSE
//			DELETE_OBJECT(fw.obj)
//		ENDIF
//	ENDIF
//	fireworkVars.vShockingEventPos = fw.vLocation
//	fw.iType = 0
//	fw.iColour = 0
//	fw.iTimer = 0
//	fw.bPlaced = FALSE
//	fw.vLocation = <<0.0,0.0,0.0>>
//ENDPROC
//
//PROC PLACE_FIREWORK(FIREWORK &fireworkData,FIREWORK_VARIABLES &fireworkVars)
//	VECTOR vTemp
//	
//	//Create
//	IF IS_BIT_SET(fireworkVars.iFireworkLocalBitset, CHECK_CREATE_FIREWORK)
//		//IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "anim@mp_fireworks", GET_FIREWORK_TYPE_ANIM_NAME(fireworkData.iType)) >= 0.1	//0.20
//		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("CREATE_PROP"))
//			fireworkVars.iNewFirework = FIND_UNALLOCATED_FIREWORK(fireworkVars)
//		
//			IF fireworkVars.iNewFirework != -1
//				fireworkVars.fireworks[fireworkVars.iNewFirework].iType = fireworkData.iType
//				fireworkVars.fireworks[fireworkVars.iNewFirework].iColour = fireworkData.iColour
//				fireworkVars.fireworks[fireworkVars.iNewFirework].iTimer = fireworkData.iTimer
//				fireworkVars.fireworks[fireworkVars.iNewFirework].vLocation = fireworkData.vLocation
//				fireworkVars.fireworks[fireworkVars.iNewFirework].bPlaced = TRUE
//			
//				fireworkVars.fireworks[fireworkVars.iNewFirework].obj = CREATE_OBJECT(GET_FIREWORK_TYPE_MODEL_NAME(fireworkVars.fireworks[fireworkVars.iNewFirework].iType),fireworkVars.fireworks[fireworkVars.iNewFirework].vLocation,FALSE,TRUE)
//				SET_ENTITY_INVINCIBLE(fireworkVars.fireworks[fireworkVars.iNewFirework].obj,TRUE)
//				//FREEZE_ENTITY_POSITION(fireworkVars.fireworks[fireworkVars.iNewFirework].obj,TRUE)
//				
//				//ATTACH_ENTITY_TO_ENTITY(fireworkVars.fireworks[fireworkVars.iNewFirework].obj, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_R_HAND), vFireworkAttachOffset, vFireworkAttachRot, TRUE, TRUE)
//				ATTACH_ENTITY_TO_ENTITY(fireworkVars.fireworks[fireworkVars.iNewFirework].obj, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_PH_R_HAND), <<0, 0, 0>>, <<0, 0, 0>>, TRUE, TRUE)
//				
//				CLEAR_BIT(fireworkVars.iFireworkLocalBitset, CHECK_CREATE_FIREWORK)
//				#IF IS_DEBUG_BUILD
//				vTemp = GET_ENTITY_COORDS(fireworkVars.fireworks[fireworkVars.iNewFirework].obj)
//				println("net_fireworks - PLACE_FIREWORK - NEW FIREWORK CREATED AND ATTACHED - COORDS = ", vTemp)
//				println("net_fireworks - iType: ",fireworkVars.fireworks[fireworkVars.iNewFirework].iType," iColour: ",fireworkVars.fireworks[fireworkVars.iNewFirework].iColour, " iTimer",fireworkVars.fireworks[fireworkVars.iNewFirework].iTimer)
//				#ENDIF
//			ELSE
//				println("net_fireworks - PLACE_FIREWORK - NO FREE FIREWORK SLOT FOUND ", fireworkVars.iNewFirework)
//				CLEAR_BIT(fireworkVars.iFireworkLocalBitset, CHECK_DETACH_FIREWORK)
//				CLEAR_BIT(fireworkVars.iFireworkLocalBitset, CHECK_CREATE_FIREWORK)
//			ENDIF
//		ENDIF
//	
//	//Detach and Place on Ground
//	ELIF IS_BIT_SET(fireworkVars.iFireworkLocalBitset, CHECK_DETACH_FIREWORK)
//		//IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "anim@mp_fireworks", GET_FIREWORK_TYPE_ANIM_NAME(fireworkData.iType)) >= 0.80	//0.50
//		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("RELEASE_PROP"))
//			IF DOES_ENTITY_EXIST(fireworkVars.fireworks[fireworkVars.iNewFirework].obj)
//				DETACH_ENTITY(fireworkVars.fireworks[fireworkVars.iNewFirework].obj)
//				FREEZE_ENTITY_POSITION(fireworkVars.fireworks[fireworkVars.iNewFirework].obj, TRUE)
//				SET_ENTITY_COLLISION(fireworkVars.fireworks[fireworkVars.iNewFirework].obj, FALSE)
//				
//				vTemp = GET_ENTITY_COORDS(fireworkVars.fireworks[fireworkVars.iNewFirework].obj)
//				IF fireworkVars.fireworks[fireworkVars.iNewFirework].iType = 3
//					vTemp.z -= 0.5
//				ELSE
//					vTemp.z -= 0.08
//				ENDIF
//				BROADCAST_CREATE_REMOTE_FIREWORK(fireworkVars.iNewFirework, fireworkVars.fireworks[fireworkVars.iNewFirework].iType, vTemp, ALL_PLAYERS(FALSE))
//				
//				println("net_fireworks - PLACE_FIREWORK - NEW FIREWORK DETACHED - COORDS = ", vTemp)
//				DECREMENT_FIREWORK_TYPE_QUANTITY(fireworkVars.fireworks[fireworkVars.iNewFirework].iType, fireworkVars.fireworks[fireworkVars.iNewFirework].iColour)
//				fireworkVars.iNoPlacedFireworks++
//					
//				CLEAR_BIT(fireworkVars.iFireworkLocalBitset, CHECK_DETACH_FIREWORK)
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	//Cleanup Firework if player is killed or made ragdoll
//	IF IS_BIT_SET(fireworkVars.iFireworkLocalBitset, CHECK_DETACH_FIREWORK)
//		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
//		OR IS_PED_RAGDOLL(PLAYER_PED_ID())
//		OR IS_PED_RUNNING_RAGDOLL_TASK(PLAYER_PED_ID())
//		OR IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
//		OR IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
//		OR (NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "anim@mp_fireworks", GET_FIREWORK_TYPE_ANIM_NAME(fireworkVars.fireworks[fireworkVars.iNewFirework].iType)) AND NOT IS_BIT_SET(fireworkVars.iFireworkLocalBitset, CHECK_CREATE_FIREWORK))
//			println("net_fireworks - PLACE_FIREWORK - PLAYER DEAD OR RAGDOLL SO CLEANUP FIREWORK ", fireworkVars.iNewFirework)
//			DESTROY_FIREWORK(fireworkVars.fireworks[fireworkVars.iNewFirework],fireworkVars, TRUE)
//			CLEAR_BIT(fireworkVars.iFireworkLocalBitset, CHECK_DETACH_FIREWORK)
//			CLEAR_BIT(fireworkVars.iFireworkLocalBitset, CHECK_CREATE_FIREWORK)
//		ENDIF
//	ENDIF
//ENDPROC
//
//PROC SORT_FIREWORKS_BY_TIMER(FIREWORK_VARIABLES &fireworkVars)
//	INT iCount
//	BOOL bSwapped = TRUE
//	INT iDebug
//	FIREWORK iTemp
//
//	IF fireworkVars.iNoPlacedFireworks > 1
//
//		WHILE bSwapped
//			FOR iCount = 1 TO fireworkVars.iNoPlacedFireworks-1 
//				bSwapped = FALSE
//				println("net_fireworks - SORTING FIREWORK: ",iCount," ",fireworkVars.iNoPlacedFireworks-1)
//				IF fireworkVars.fireworks[iCount-1].bPlaced AND fireworkVars.fireworks[iCount].bPlaced
//					IF fireworkVars.fireworks[iCount-1].iTimer > fireworkVars.fireworks[iCount].iTimer
//						bSwapped = TRUE
//						iTemp = fireworkVars.fireworks[iCount]
//						fireworkVars.fireworks[iCount] = fireworkVars.fireworks[iCount-1]
//						fireworkVars.fireworks[iCount-1]= iTemp
//					ENDIF
//				ENDIF
//			ENDFOR
//		ENDWHILE
//	ELSE
//		println("net_fireworks - ONLY 1 FIREWORK PLACED, NO SORT REQUIRED")
//	ENDIF
//	
//	REPEAT fireworkVars.iNoPlacedFireworks iDebug
//		println("net_fireworks - Firework ",iDebug," = ",fireworkVars.fireworks[iDebug].iTimer)
//	ENDREPEAT
//	println("net_fireworks - SORTED FIREWORKS ")
//ENDPROC
//
//FUNC INT GET_FIREWORK_CLEANUP_TIME(INT iType)
//	SWITCH(iType)
//		CASE 0		RETURN 8000	 	//Cone
//		CASE 1		RETURN 3500 	//Box
//		CASE 2		RETURN 3000 	//Cylinder
//		CASE 3		RETURN 1500 	//Rocket
//	ENDSWITCH
//	
//	RETURN 4000	
//ENDFUNC
//
////PURPOSE: Checks the height of the Rocket and then cleans it up. Safety check of a timer.
//PROC PROCESS_ROCKET_CLEANUP(FIREWORK &fw[],FIREWORK_VARIABLES &fireworkVars)
//	IF fireworkVars.iRocketCleanupBitSet != 0
//		IF IS_BIT_SET(fireworkVars.iRocketCleanupBitSet, fireworkVars.iRocketToCheck)
//			IF fw[fireworkVars.iRocketToCheck].iType = 3
//				FLOAT fDistance = VDIST(fw[fireworkVars.iRocketToCheck].vRocketStart, GET_ENTITY_COORDS(fw[fireworkVars.iRocketToCheck].obj))
//				IF fDistance > 21.0
//				OR HAS_NET_TIMER_EXPIRED(fireworkVars.iRocketCleanupTime[fireworkVars.iRocketToCheck], GET_FIREWORK_CLEANUP_TIME(fw[fireworkVars.iRocketToCheck].iType))
//					PRINTLN("net_fireworks - PROCESS_ROCKET_CLEANUP - DELETED UP ROCKET ", fireworkVars.iRocketToCheck)
//					DESTROY_FIREWORK(fw[fireworkVars.iRocketToCheck],fireworkVars, TRUE)
//					CLEAR_BIT(fireworkVars.iRocketCleanupBitSet, fireworkVars.iRocketToCheck)
//					RESET_NET_TIMER(fireworkVars.iRocketCleanupTime[fireworkVars.iRocketToCheck])
//				ENDIF
//			ELSE
//				IF HAS_NET_TIMER_EXPIRED(fireworkVars.iRocketCleanupTime[fireworkVars.iRocketToCheck], GET_FIREWORK_CLEANUP_TIME(fw[fireworkVars.iRocketToCheck].iType))
//					PRINTLN("net_fireworks - PROCESS_ROCKET_CLEANUP - DELETED UP GROUND FIREWORK ", fireworkVars.iRocketToCheck)
//					DESTROY_FIREWORK(fw[fireworkVars.iRocketToCheck],fireworkVars, TRUE)
//					CLEAR_BIT(fireworkVars.iRocketCleanupBitSet, fireworkVars.iRocketToCheck)
//					RESET_NET_TIMER(fireworkVars.iRocketCleanupTime[fireworkVars.iRocketToCheck])
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		fireworkVars.iRocketToCheck++
//		IF fireworkVars.iRocketToCheck > MAX_OWNED_FIREWORKS
//			fireworkVars.iRocketToCheck = 0
//		ENDIF
//	ENDIF
//ENDPROC
//
////PURPOSE: Creates a shocking event when the Fireworks have been triggered
//PROC PROCESS_SHOCKING_EVENT(FIREWORK_VARIABLES &fireworkVars)
//	IF IS_BIT_SET(fireworkVars.iFireworkLocalBitset, DO_SHOCKING_EVENT)
//		IF fireworkVars.iShockingEvent = 0
//		AND NOT IS_VECTOR_ZERO(fireworkVars.vShockingEventPos)
//			fireworkVars.iShockingEvent = ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_PARACHUTER_OVERHEAD, fireworkVars.vShockingEventPos, 60000)
//			IF fireworkVars.iShockingEvent > 0
//				CLEAR_BIT(fireworkVars.iFireworkLocalBitset, DO_SHOCKING_EVENT)
//				NET_PRINT_TIME() NET_PRINT("net_fireworks - PROCESS_SHOCKING_EVENT - EVENT_SHOCKING_PARACHUTER_OVERHEAD - SHOCKING EVENT CREATED = ") NET_PRINT_INT(fireworkVars.iShockingEvent) NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC	
//
//PROC DRAW_TIMER(FIREWORK_VARIABLES &fireworkVars)
//	IF GET_CURRENT_LANGUAGE() = LANGUAGE_RUSSIAN
//	OR GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
//	OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
//	OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
//		SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
//	ELSE
//		SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
//	ENDIF
//	
//	IF (fireworkVars.fireworks[fireworkVars.iNextFirework].iTimer-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(fireworkVars.launchTimer)) >= 0
//		DRAW_GENERIC_TIMER((fireworkVars.fireworks[fireworkVars.iNextFirework].iTimer-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(fireworkVars.launchTimer)), "NFW_TIMER", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE)
//	ELSE
//		DRAW_GENERIC_TIMER(0, "NFW_TIMER", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE)
//	ENDIF
//ENDPROC
//
//PROC START_FIREWORK_COUNTDOWN(FIREWORK_VARIABLES &fireworkVars)
//	SORT_FIREWORKS_BY_TIMER(fireworkVars)
//	fireworkVars.iNextFirework = 0
//	fireworkVars.iElapsedTime = fireworkVars.fireworks[fireworkVars.iNextFirework].iTimer
//	SET_BIT(fireworkVars.iFireworkLocalBitset,DRAW_LAUNCH_TIMER)
//	RESET_NET_TIMER(fireworkVars.launchTimer)
//	fireworkVars.iShockingEvent = 0
//ENDPROC
//
//PROC GET_FIREWORK_COLOUR(INT iColour, FLOAT &fR, FLOAT &fG, FLOAT &fB)
//	SWITCH iColour
//		CASE 0
//			fR = 1
//			fG = 0.1
//			fB = 0.1
//			PRINTLN("net_fireworks - GET_FIREWORK_COLOUR - RED - R=", fR, " G=", fG, " B=", fB)
//		BREAK
//		CASE 1
//			fR = 1
//			fG = 1
//			fB = 1
//			PRINTLN("net_fireworks - GET_FIREWORK_COLOUR - WHITE - R=", fR, " G=", fG, " B=", fB)
//		BREAK
//		CASE 2
//			fR = 0.1
//			fG = 0.1
//			fB = 1
//			PRINTLN("net_fireworks - GET_FIREWORK_COLOUR - BLUE - R=", fR, " G=", fG, " B=", fB)
//		BREAK
//	ENDSWITCH
//ENDPROC
//
//PROC PLAY_FIREWORK(FIREWORK &fw, INT iFirework,FIREWORK_VARIABLES &fireworkVars)
//	USE_PARTICLE_FX_ASSET("scr_indep_fireworks")
//	println("net_fireworks - PLAY_FIREWORK - USE_PARTICLE_FX_ASSET: ","scr_indep_fireworks")
//	FLOAT fR, fG, fB
//	GET_FIREWORK_COLOUR(fw.iColour, fR, fG, fB)
//	SET_PARTICLE_FX_NON_LOOPED_COLOUR(fR, fG, fB)
//	println("net_fireworks - PLAY_FIREWORK - SET_PARTICLE_FX_NON_LOOPED_COLOUR: ","scr_indep_fireworks")
//	START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD(GET_FIREWORK_TYPE_VFX_NAME(fw.iType),GET_ENTITY_COORDS(fw.obj),<<0.0,0.0,0.0>>)
//	println("net_fireworks - PLAY_FIREWORK - START_PARTICLE_FX_NON_LOOPED_ON_ENTITY: ",GET_FIREWORK_TYPE_VFX_NAME(fw.iType))
//	SET_BIT(fireworkVars.iFireworkLocalBitset, DO_SHOCKING_EVENT)
//	
//	IF fw.iType = 3
//		fw.vRocketStart = GET_ENTITY_COORDS(fw.obj)
//		SET_ENTITY_COLLISION(fw.obj, TRUE)
//		FREEZE_ENTITY_POSITION(fw.obj, FALSE)
//		SET_ENTITY_VELOCITY(fw.obj, <<0, 0, 70>>)	//vFireworkAttachOffset)
//				
//		SET_BIT(fireworkVars.iRocketCleanupBitSet, iFirework)
//		println("net_fireworks - PLAY_FIREWORK - APPLY VELOCITY TO ROCKET - grt - fireworkVars.iRocketCleanupBitSet SET FOR ", iFirework)
//	ELSE
//		SET_BIT(fireworkVars.iRocketCleanupBitSet, iFirework)	//DESTROY_FIREWORK(fw, TRUE)
//	ENDIF
//ENDPROC
//
//
///// PURPOSE:
/////    Initialises the fireworks 
//FUNC BOOL INIT_FIREWORKS
//	REQUEST_MODEL(IND_PROP_FIREWORK_01)
//	REQUEST_MODEL(IND_PROP_FIREWORK_02)
//	REQUEST_MODEL(IND_PROP_FIREWORK_03)
//	REQUEST_MODEL(IND_PROP_FIREWORK_04)
//	REQUEST_ANIM_DICT("anim@mp_fireworks")
//	REQUEST_NAMED_PTFX_ASSET("scr_indep_fireworks")
//	
//	IF NOT HAS_MODEL_LOADED(IND_PROP_FIREWORK_01)
//		RETURN FALSE
//	ENDIF
//	
//	IF NOT HAS_MODEL_LOADED(IND_PROP_FIREWORK_02)
//		RETURN FALSE
//	ENDIF
//	
//	IF NOT HAS_MODEL_LOADED(IND_PROP_FIREWORK_03)
//		RETURN FALSE
//	ENDIF
//	
//	
//	IF NOT HAS_MODEL_LOADED(IND_PROP_FIREWORK_04)
//		RETURN FALSE
//	ENDIF
//	
//	IF NOT HAS_ANIM_DICT_LOADED("anim@mp_fireworks")
//		RETURN FALSE
//	ENDIF
//	
//	println("net_fireworks - Trying to load: ","scr_indep_fireworks")
//	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_indep_fireworks")
//		println("net_fireworks - Not loaded yet: ","scr_indep_fireworks")
//		RETURN FALSE
//	ENDIF
//	println("net_fireworks - Loaded: ","scr_indep_fireworks")
//	
//	RETURN TRUE
//ENDFUNC
//
//
///// PURPOSE:
/////    Performs some cleanup when menu is closed - does not end script
//PROC LOCAL_CLEANUP_FIREWORKS
//	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_FLARE_01)
//	SET_MODEL_AS_NO_LONGER_NEEDED(IND_PROP_FIREWORK_01)
//	SET_MODEL_AS_NO_LONGER_NEEDED(IND_PROP_FIREWORK_02)
//	SET_MODEL_AS_NO_LONGER_NEEDED(IND_PROP_FIREWORK_03)
//	SET_MODEL_AS_NO_LONGER_NEEDED(IND_PROP_FIREWORK_04)
//	//REMOVE_NAMED_PTFX_ASSET("scr_indep_fireworks")
//ENDPROC
//
//FUNC VECTOR GET_FIREWORK_DIMENSIONS(INT iType, BOOL bGetUpper = FALSE)
//	IF bGetUpper = FALSE
//		SWITCH(iType)
//			CASE 0		RETURN <<0.5, 0.25, 12.0>>		//Cone
//			CASE 1		RETURN <<0.5, 0.5, 10.0>>		//Box
//			CASE 2		RETURN <<0.25, 0.25, 15.0>>		//Cylinder
//			CASE 3		RETURN <<0.25, 0.25, 20.0>>	 	//Rocket
//		ENDSWITCH
//	ELSE
//		SWITCH(iType)
//			CASE 0		RETURN <<6.0, 6.0, 14.0>>		//Cone
//			CASE 1		RETURN <<15.0, 15.0, 16.0>>		//Box
//			CASE 2		RETURN <<12.0, 12.0, 22.0>>		//Cylinder
//			CASE 3		RETURN <<15.0, 15.0, 14.0>>	 	//Rocket
//		ENDSWITCH
//	ENDIF
//	
//	RETURN <<0.25, 0.25, 15.0>>
//ENDFUNC
//
////PURPOSE: Controls the Creation and Cleanup of Remote Player Firework Props.
//PROC PROCESS_REMOTE_FIREWORKS(FIREWORK_VARIABLES &fireworkVars)
//	
//	//Check if any Players have used a Firework
//	IF MPGlobalsFW.iProcessRFWBitSet != 0
//	OR MPGlobalsFW.iProcessCFWBitSet != 0
//		INT i
//		INT k
//		VECTOR vTemp
//		REPEAT NUM_NETWORK_REAL_PLAYERS() i
//			
//			//Check if the Player has set a Firework Flag
//			IF MPGlobalsFW.iRFWPlayerBitSet[i] != 0
//				
//				//Check if we should Cleanup
//				IF IS_BIT_SET(MPGlobalsFW.iRFWPlayerBitSet[i], biRFW_CLEANUP_FIREWORKS)
//					REPEAT MAX_OWNED_FIREWORKS k
//						IF DOES_ENTITY_EXIST(MPGlobalsFW.RFWdata[i][k].obFirework)
//							//SET_OBJECT_AS_NO_LONGER_NEEDED(MPGlobalsFW.RFWdata[i][k].obFirework)
//							DELETE_OBJECT(MPGlobalsFW.RFWdata[i][k].obFirework)
//						ENDIF
//					ENDREPEAT
//					//CLEAR_BIT(iRFWPlayerBitSet[i], biRFW_CLEANUP_FIREWORKS)
//					MPGlobalsFW.iRFWPlayerBitSet[i] = 0	//CLEAR THE WHOLE BITSET
//					CLEAR_BIT(MPGlobalsFW.iProcessRFWBitSet, i)	//Clear Process this Player Bit
//					PRINTLN("net_fireworks - PROCESS_REMOTE_FIREWORKS - biRFW_CLEANUP_FIREWORKS - DONE - REMOTE PLAYER ", i)
//					
//				//Check if we should Create Fireworks
//				ELSE
//					REPEAT MAX_OWNED_FIREWORKS k
//						IF IS_BIT_SET(MPGlobalsFW.iRFWPlayerBitSet[i], k)
//							IF NOT DOES_ENTITY_EXIST(MPGlobalsFW.RFWdata[i][k].obFirework)
//								REQUEST_MODEL(GET_FIREWORK_TYPE_MODEL_NAME(MPGlobalsFW.RFWdata[i][k].iType))
//								IF HAS_MODEL_LOADED(GET_FIREWORK_TYPE_MODEL_NAME(MPGlobalsFW.RFWdata[i][k].iType))
//									MPGlobalsFW.RFWdata[i][k].obFirework = CREATE_OBJECT(GET_FIREWORK_TYPE_MODEL_NAME(MPGlobalsFW.RFWdata[i][k].iType), MPGlobalsFW.RFWdata[i][k].vLocation, FALSE, TRUE)
//									SET_ENTITY_INVINCIBLE(MPGlobalsFW.RFWdata[i][k].obFirework, TRUE)
//									FREEZE_ENTITY_POSITION(MPGlobalsFW.RFWdata[i][k].obFirework, TRUE)
//									SET_ENTITY_COLLISION(MPGlobalsFW.RFWdata[i][k].obFirework, FALSE)
//									
//									SET_MODEL_AS_NO_LONGER_NEEDED(GET_FIREWORK_TYPE_MODEL_NAME(MPGlobalsFW.RFWdata[i][k].iType))
//									CLEAR_BIT(MPGlobalsFW.iRFWPlayerBitSet[i], k)
//									PRINTLN("net_fireworks - PROCESS_REMOTE_FIREWORKS - CREATED FIREWORK ", k, " FOR REMOTE PLAYER ", i)
//								ENDIF
//							ELSE
//								CLEAR_BIT(MPGlobalsFW.iRFWPlayerBitSet[i], k)
//								PRINTLN("net_fireworks - PROCESS_REMOTE_FIREWORKS - FIREWORK ", k, " FOR REMOTE PLAYER ", i, " ALREADY EXISTS")
//							ENDIF
//						ENDIF
//					ENDREPEAT
//				ENDIF
//				
//				//Quit out next player with a bit set will be done next frame
//				//EXIT
//			ENDIF
//			
//			//Check if we need to tell a new player about our Fireworks
//			IF MPGlobalsFW.iProcessCFWBitSet != 0
//				IF NOT IS_BIT_SET(fireworkVars.iFireworkLocalBitset,DRAW_LAUNCH_TIMER)
//					IF IS_BIT_SET(MPGlobalsFW.iProcessCFWBitSet, i)
//						REPEAT MAX_OWNED_FIREWORKS k
//							IF fireworkVars.fireworks[k].bPlaced = TRUE
//								vTemp = GET_ENTITY_COORDS(fireworkVars.fireworks[k].obj)
//								IF fireworkVars.fireworks[k].iType = 3
//									vTemp.z -= 0.5
//								ELSE
//									vTemp.z -= 0.08
//								ENDIF
//								BROADCAST_CREATE_REMOTE_FIREWORK(k, fireworkVars.fireworks[k].iType, vTemp, SPECIFIC_PLAYER(INT_TO_PLAYERINDEX(i)))
//								PRINTLN("net_fireworks - PROCESS_REMOTE_FIREWORKS - iProcessCFWBitSet - BROADCAST_CREATE_REMOTE_FIREWORK - FIREWORK ", k)
//							ENDIF
//						ENDREPEAT
//						CLEAR_BIT(MPGlobalsFW.iProcessCFWBitSet, i)
//						PRINTLN("net_fireworks - PROCESS_REMOTE_FIREWORKS - iProcessCFWBitSet - BROADCAST_CREATE_REMOTE_FIREWORK - ALL - TO PLAYER ", i)
//					ENDIF
//				ELSE
//					MPGlobalsFW.iProcessCFWBitSet = 0
//					PRINTLN("net_fireworks - PROCESS_REMOTE_FIREWORKS - iProcessCFWBitSet - DRAW_LAUNCH_TIMER SET - CLEAR iProcessCFWBitSet ")
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
//ENDPROC
//
///// PURPOSE:
/////    Maintain the placement and current state of fireworks
//PROC MAINTAIN_FIREWORKS(FIREWORK &fireworkData,BOOL &bResetMenuNow,FIREWORK_VARIABLES &fireworkVars)
//
//	#IF IS_DEBUG_BUILD
//	DRAW_DEBUG_BOX(fireworkVars.stDebug1,fireworkVars.stDebug2)
//	DRAW_DEBUG_BOX(fireworkVars.stDebug3,fireworkVars.stDebug4)
//	#ENDIF
//	
//	PLACE_FIREWORK(fireworkData,fireworkVars)
//	
//	IF fireworkData.bDoShapeTest
//		SWITCH (fireworkVars.iShapeTestStage)
//			CASE ST_INIT				
//				VECTOR vModelMin
//				VECTOR vModelMax
//				FLOAT fGroundZ
//				VECTOR vFireworkLoc
//				VECTOR vDim
//				VECTOR vDimUpper
//				VECTOR vFireworkUpper
//				
//				GET_MODEL_DIMENSIONS(GET_FIREWORK_TYPE_MODEL_NAME(fireworkData.iType),vModelMin,vModelMax)
//				vDim = vModelMax - vModelmin
//				
//				//vDim.x += 0.25
//				//vDim.y += 0.25
//				//vDim.z += GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID())
//				
//				vDim += GET_FIREWORK_DIMENSIONS(fireworkData.iType)
//				vDimUpper = GET_FIREWORK_DIMENSIONS(fireworkData.iType, TRUE)
//				
//				IF fireworkData.iType != 3
//					vFireworkLoc = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0.0,0.5,0.0>>)
//				ELSE
//					vFireworkLoc = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<-0.08,0.6,0.0>>)
//				ENDIF
//				GET_GROUND_Z_FOR_3D_COORD(vFireworkLoc,fGroundZ)
//				vFireworkLoc.z = fGroundZ
//				fireworkData.vLocation = vFireworkLoc
//				vFireworkLoc.z += (vDim.z/2.0) + 0.1 // So shapetest doesn't intersect ground
//				println("net_fireworks - vDim = ", vDim)
//				println("net_fireworks - vFireworkLoc = ", vFireworkLoc)
//				
//				vFireworkUpper = vFireworkLoc
//				println("net_fireworks - vFireworkUpper = vFireworkLoc = ", vFireworkUpper)
//				vFireworkUpper.z = (fGroundZ+vDim.z)
//				println("net_fireworks - (fGroundZ+vDim.z) = ", (fGroundZ+vDim.z))
//				vFireworkUpper.z += (vDimUpper.z/2.0)
//				vFireworkUpper = vFireworkUpper
//				println("net_fireworks - vDimUpper = ", vDimUpper)
//				println("net_fireworks - vFireworkUpper = ", vFireworkUpper)
//				
//				#IF IS_DEBUG_BUILD
//				fireworkVars.stDebug1 = vFireWorkLoc - (vDim/2.0)
//				fireworkVars.stDebug2 = vFireWorkLoc - (vDim/-2.0)
//				println("net_fireworks - fireworkVars.stDebug1 = ", fireworkVars.stDebug1)
//				println("net_fireworks - fireworkVars.stDebug2 = ", fireworkVars.stDebug2)
//				fireworkVars.stDebug3 = vFireworkUpper - (vDimUpper/2.0)
//				fireworkVars.stDebug4 = vFireworkUpper - (vDimUpper/-2.0)
//				println("net_fireworks - fireworkVars.stDebug3 = ", fireworkVars.stDebug3)
//				println("net_fireworks - fireworkVars.stDebug4 = ", fireworkVars.stDebug4)
//				#ENDIF
//				
//				
//				//Check if Fireworks model is already there
//				INT i
//				OBJECT_INDEX obTemp
//				REPEAT NO_FIREWORK_TYPES i
//					obTemp = GET_CLOSEST_OBJECT_OF_TYPE(fireworkData.vLocation, 0.2, GET_FIREWORK_TYPE_MODEL_NAME(i), FALSE)
//					IF obTemp != NULL
//						SET_BIT(fireworkVars.iFireworkBitSet, biShapeTestFailed)
//						println("net_fireworks - biShapeTestFailed - FIREWORK MODEL CHECK")
//					ENDIF
//				ENDREPEAT
//				
//				//Do Shape Test
//				fireworkVars.sti_Firework = START_SHAPE_TEST_BOX(vFireWorkLoc,vDim,<<0,0,-0.1>>,EULER_YXZ,SCRIPT_INCLUDE_ALL,PLAYER_PED_ID())
//				fireworkVars.sti_FireworkUpper = START_SHAPE_TEST_BOX(vFireworkUpper,vDimUpper,<<0,0,-0.1>>,EULER_YXZ,SCRIPT_INCLUDE_ALL,PLAYER_PED_ID())
//				
//				println("net_fireworks - STARTED SHAPE TEST AT: ",vFireWorkLoc," dimensions: ",vDim," rotation: ",<<0,0,-0.1>>)
//				println("net_fireworks - STARTED SHAPE TEST UPPER AT: ",vFireworkUpper," dimensions: ",vDimUpper," rotation: ",<<0,0,-0.1>>)
//				fireworkVars.iShapeTestStage = ST_WAITING_RESULT
//				
//			BREAK
//			CASE ST_WAITING_RESULT
//				VECTOR vTemp1
//				VECTOR vTemp2
//				ENTITY_INDEX hitEntity
//				INT iHitSomething
//				
//				println("net_fireworks - WAITING FOR RESULT OF SHAPE TEST")
//				
//				IF GET_SHAPE_TEST_RESULT(fireworkVars.sti_Firework,iHitSomething,vTemp1,vTemp2,hitEntity) = SHAPETEST_STATUS_RESULTS_READY
//					println("net_fireworks - SHAPE TEST RESULT READY")
//					SET_BIT(fireworkVars.iFireworkBitSet, biShapeTestResultsDone)
//					IF iHitsomething != 0
//						SET_BIT(fireworkVars.iFireworkBitSet, biShapeTestFailed)
//						println("net_fireworks - biShapeTestFailed - LOWER")
//					ENDIF
//				ENDIF
//				IF GET_SHAPE_TEST_RESULT(fireworkVars.sti_FireworkUpper,iHitSomething,vTemp1,vTemp2,hitEntity) = SHAPETEST_STATUS_RESULTS_READY
//					println("net_fireworks - SHAPE TEST UPPER RESULT READY")
//					SET_BIT(fireworkVars.iFireworkBitSet, biShapeTestUpperResultsDone)
//					IF iHitsomething != 0
//						SET_BIT(fireworkVars.iFireworkBitSet, biShapeTestFailed)
//						println("net_fireworks - biShapeTestFailed - UPPER")
//					ENDIF
//				ENDIF
//				
//				IF IS_BIT_SET(fireworkVars.iFireworkBitSet, biShapeTestResultsDone)
//				AND IS_BIT_SET(fireworkVars.iFireworkBitSet, biShapeTestUpperResultsDone)
//					println("net_fireworks - SHAPE TEST RESULT READY")
//					IF NOT IS_BIT_SET(fireworkVars.iFireworkBitSet, biShapeTestFailed)
//						println("net_fireworks - SAFE - PLACING FIREWORK")
//						PLAY_FIREWORK_ANIM(fireworkData,fireworkVars)
//						bResetMenuNow = TRUE
//					ELSE
//						SET_BIT(fireworkVars.iFireworkLocalBitset,NOT_SAFE_TO_PLACE)
//						println("net_fireworks - NOT SAFE - ENDING TEST")
//					ENDIF
//					fireworkData.bDoShapeTest = FALSE
//					fireworkVars.iShapeTestStage = ST_INIT
//					CLEAR_BIT(fireworkVars.iFireworkBitSet, biShapeTestResultsDone)
//					CLEAR_BIT(fireworkVars.iFireworkBitSet, biShapeTestUpperResultsDone)
//					CLEAR_BIT(fireworkVars.iFireworkBitSet, biShapeTestFailed)
//				ENDIF
//			BREAK
//		ENDSWITCH
//	ENDIF
//	
//	IF IS_BIT_SET(fireworkVars.iFireworkLocalBitset,DRAW_LAUNCH_TIMER)
//		println("net_fireworks - TIMER COUNTING iTimer = ",fireworkVars.fireworks[fireworkVars.iNextFirework].iTimer)
//		IF HAS_NET_TIMER_EXPIRED(fireworkVars.launchTimer, fireworkVars.fireworks[fireworkVars.iNextFirework].iTimer)
//			println("net_fireworks - IF HAS_NET_TIMER_EXPIRED(fireworkVars.launchTimer, fireworkVars.fireworks[fireworkVars.iNextFirework].iTimer)")
//			PLAY_FIREWORK(fireworkVars.fireworks[fireworkVars.iNextFirework], fireworkVars.iNextFirework,fireworkVars)
//			fireworkVars.iNextFirework++
//			IF fireworkVars.iNextFirework != fireworkVars.iNoPlacedFireworks
//			AND fireworkVars.iNextFirework < MAX_OWNED_FIREWORKS
//				fireworkVars.fireworks[fireworkVars.iNextFirework].iTimer -= fireworkVars.iElapsedTime
//				fireworkVars.iElapsedTime += fireworkVars.fireworks[fireworkVars.iNextFirework].iTimer
//				RESET_NET_TIMER(fireworkVars.launchTimer)
//			ELSE
//				REMOVE_NAMED_PTFX_ASSET("scr_indep_fireworks")
//				fireworkVars.iNextFirework = 0
//				fireworkVars.iNoPlacedFireworks = 0
//				CLEAR_BIT(fireworkVars.iFireworkLocalBitset,DRAW_LAUNCH_TIMER)
//				BROADCAST_REMOVE_PLAYERS_REMOTE_FIREWORKS(ALL_PLAYERS(FALSE))
//			ENDIF
//		ENDIF
//		DRAW_TIMER(fireworkVars)
//	ENDIF
//	
//	PROCESS_ROCKET_CLEANUP(fireworkVars.fireworks,fireworkVars)
//	PROCESS_SHOCKING_EVENT(fireworkVars)
//	PROCESS_REMOTE_FIREWORKS(fireworkVars)
//ENDPROC
