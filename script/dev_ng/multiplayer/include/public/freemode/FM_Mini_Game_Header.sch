//////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FMMC_											//
// Description: Controls the custom missions in free mode that have been created with the creator	//
// Written by:  Robert Wright																		//
// Date: 28/01/2012																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////
USING "rage_builtins.sch"
USING "globals.sch"
USING "script_network.sch"
USING "commands_network.sch"
USING "net_mission_info.sch"

/// PURPOSE:gets the start location of a mini game
FUNC VECTOR GET_FMMC_MINI_GAME_START_LOCATION(INT iType, INT iVariation = 0)
	SWITCH iType
		//Golf
		CASE FMMC_TYPE_MG_GOLF				
			RETURN (<<-1370.3983, 	 56.4489, 		52.5023>>)
		BREAK
		
		//Pilot School
		CASE FMMC_TYPE_MG_PILOT_SCHOOL				
			RETURN (<<-1154.8163, -2716.4529, 18.8923>>)
		BREAK
		
		//Rangs	
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	
			SWITCH iVariation
				CASE 0 RETURN (<< 16.3823, -1099.9557, 28.7970 >>) 	// Pillbox Hill
				CASE 1 RETURN (<<817.6979, -2161.9824, 28.6190>>)	// Cypress Flats
			ENDSWITCH
		BREAK
				
		//Tennis	
		CASE FMMC_TYPE_MG_TENNIS			
			SWITCH iVariation
				CASE 0 RETURN    (<<-1162.5516, -1628.3932, 3.3739>>)	//beach
				CASE 1 RETURN 	 (<<479.8226, -234.8953, 52.7934>>)		//VinewoodHotel1
				CASE 2 RETURN 	 (<<-1237.3899, 333.6207, 78.9863>>)	//RichmanHotel1
				CASE 3 RETURN 	 <<-2870.1614, 9.0811, 10.6081>>
				CASE 4 RETURN 	 (<<-939.5128, -1255.6967, 6.9671>>)	//Spire House
				CASE 5 RETURN 	 (<<-1618.9148, 249.3260, 58.5552>>)	//Church Adjacent -939.6168, -1255.7323, 6.9773
				CASE 6 RETURN 	 (<<-1369.9988, -117.2200, 49.7046>>)	//Weazel Court 1
			ENDSWITCH			
		BREAK		
		CASE FMMC_TYPE_MG_ARM_WRESTLING		
			SWITCH iVariation
				CASE 0 RETURN (<<904.3290, 	  -173.7431, 		73.0754 >>)
				CASE 1 RETURN (<<1440.4320,   -2215.5957, 		60.1330 >>)
				CASE 2 RETURN (<< -83.8271,   -1330.8877, 		28.2913 >>)
				CASE 3 RETURN (<<366.4715, 	  -2446.2266,		5.1129  >>)
				CASE 4 RETURN (<< 1222.7200,  2721.2000, 		37.0042 >>)
				CASE 5 RETURN (<< 1051.4794,  2667.2903, 		38.5509 >>)
				CASE 6 RETURN (<< 273.3710,   2607.7676, 		43.7028 >>)
				CASE 7 RETURN (<< -193.7423,  6277.6753, 		30.4892 >>)
				CASE 8 RETURN (<< 1687.1375,  4966.8379, 		42.0569 >>)	
				CASE 9 RETURN (<< 2343.9714,  3134.0540, 		47.2088 >>)	
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_MG_DARTS	
			SWITCH iVariation
				CASE 0 RETURN (<<1994.7828, 3049.3230,	46.1123 >>) 
				//CASE 1 RETURN (<<-573.5116, 292.4443, 78.1765>>) //(<<-572.5222, 292.3346, 	78.1765>>) 
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	RETURN (<<0.0, 0.0, 0.0>>)
ENDFUNC

/// PURPOSE: Returns TRUE if the minigame has a heading
FUNC BOOL GET_MINI_GAME_CORONA_HEADING(INT iType, FLOAT &fHeading)
	
	SWITCH iType
		CASE FMMC_TYPE_MG_PILOT_SCHOOL
			fHeading = 150.0
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:gets the start location of a mini game
FUNC VECTOR GET_FMMC_MINI_GAME_CAMERA_LOCATION(INT iType, INT iVariation = 0)
	SWITCH iType
		//Golf
		CASE FMMC_TYPE_MG_GOLF
			RETURN <<-1297.8552, 84.6478, 54.6519>> //<<-1342.8724,57.8378,73.4356>>
		CASE FMMC_TYPE_MG_PILOT_SCHOOL				
			RETURN <<-855.7661, -2645.3760, 100.8438>>

		//Rangs
		CASE FMMC_TYPE_MG_SHOOTING_RANGE
			SWITCH iVariation
				CASE 0 RETURN <<13.4678,-1097.7811,30.192>>
				CASE 1 RETURN <<816.6221, -2160.6592, 30.3139>>
			ENDSWITCH
		BREAK
		//Tennis
		CASE FMMC_TYPE_MG_TENNIS
			SWITCH iVariation
				CASE 0 RETURN <<-1172.1110, -1641.5487, 5.7494>>
				CASE 1 RETURN <<498.4758, -226.4275, 71.0569>>
				CASE 2 RETURN <<-1217.6320, 364.4556, 86.5270>>
				CASE 3 RETURN <<-2884.7537, 32.9823, 15.1123>>
				CASE 4 RETURN <<-948.1036, -1260.2487, 8.2280>>
				CASE 5 RETURN <<-1614.2163, 302.0945, 66.0158>>
				CASE 6 RETURN <<-1380.7211, -107.9713, 50.6998>> //<<-1377.819,-125.2824,58.2569>>//Weazel Court 1
			ENDSWITCH
		BREAK
		CASE FMMC_TYPE_MG_ARM_WRESTLING
			SWITCH iVariation
				CASE 0 RETURN <<903.8763, -192.2140, 74.6401>>
				CASE 1 RETURN <<1419.5167, -2216.0344, 61.8467>>
				CASE 2 RETURN <<-83.9713, -1331.6351, 29.8383>>
				CASE 3 RETURN <<364.6455, -2441.8967, 6.2607>>
				CASE 4 RETURN <<1237.2067, 2715.9993, 37.6424>>
				CASE 5 RETURN <<1019.1531, 2680.4268, 41.2254>>
				CASE 6 RETURN <<269.5203, 2617.4104, 44.7054>>
				CASE 7 RETURN <<-194.2518, 6295.6875, 31.0991>>
				CASE 8 RETURN <<1682.7593, 4954.0879, 42.6891>>
				CASE 9 RETURN <<2325.4468, 3145.9099, 58.4284>>
			ENDSWITCH
		BREAK
		CASE FMMC_TYPE_MG_DARTS
			SWITCH iVariation
				CASE 0 RETURN<<1993.6974, 3049.6675, 47.7241>>
				//CASE 1 RETURN<<-572.5292,291.8475,79.789>> //(<<-572.5222, 292.3346, 78.1765>> 
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN<<0.0, 0.0, 0.0>>
ENDFUNC

/// PURPOSE:gets the start location of a mini game
FUNC VECTOR GET_FMMC_MINI_GAME_CAMERA_ROTATION(INT iType, INT iVariation = 0)
	SWITCH iType
		//Golf
		CASE FMMC_TYPE_MG_GOLF
			RETURN <<1.3773, 0.0000, -88.8270>>
			
		//Pilot School	
		CASE FMMC_TYPE_MG_PILOT_SCHOOL				
			RETURN <<-18.1605, 2.6167, 113.0513>>
			
		//Rangs
		CASE FMMC_TYPE_MG_SHOOTING_RANGE
			SWITCH iVariation
				CASE 0 RETURN <<2.3039,0,-19.6903>>
				CASE 1 RETURN <<-5.9341, 0.0000, -151.6385>>
			ENDSWITCH
		BREAK

		//Tennis
		CASE FMMC_TYPE_MG_TENNIS
			SWITCH iVariation
				CASE 0 RETURN <<-3.6270, -0.0000, 34.6935>>
				CASE 1 RETURN <<-57.9221, 0.0000, 58.3042>>
				CASE 2 RETURN <<-15.4498, -0.0000, 161.1361>>
				CASE 3 RETURN <<-1.8386, 0.0000, -135.0223>>
				CASE 4 RETURN <<0.5233, -0.0000, -85.7377>>
				CASE 5 RETURN <<-4.2364, -0.0000, 173.6634>>
				CASE 6 RETURN <<-3.74,0,-30.7752>>
			ENDSWITCH
		BREAK
		CASE FMMC_TYPE_MG_ARM_WRESTLING
			SWITCH iVariation
				CASE 0 RETURN <<4.1441, -0.0000, 15.6909>>
				CASE 1 RETURN <<5.4815, -0.0000, -127.6695>>
				CASE 2 RETURN <<-4.1889, -0.0000, -15.0654>>
				CASE 3 RETURN <<16.4433, -0.0000, -60.3988>>
				CASE 4 RETURN <<7.8225, -0.0000, 39.2409>>
				CASE 5 RETURN <<2.0405, 0.0000, -122.3459>>
				CASE 6 RETURN <<7.3382, -0.0000, 157.6388>>
				CASE 7 RETURN <<6.4810, -0.0000, 172.9874>>
				CASE 8 RETURN <<14.0573, -0.0000, 5.0622>>
				CASE 9 RETURN <<-10.1444, 0.0000, -131.4038>>
			ENDSWITCH
		BREAK
		CASE FMMC_TYPE_MG_DARTS
			SWITCH iVariation
				CASE 0 RETURN<<8.8196, 0.0000, 57.5677>>
				//CASE 1 RETURN<<9.2089,0,-39.0344>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN<<0.0, 0.0, 0.0>>
ENDFUNC

//get the pictures
FUNC STRING GET_FMMC_MINI_GAME_PICTURE_DIRECTORY()
	RETURN "minigame/coronaimage/"
ENDFUNC

/// PURPOSE:gets name of the picture for the 
FUNC STRING GET_FMMC_MINI_GAME_PICTURE_NAME(INT iType, INT iVariation = 0)
	SWITCH iType
		//Golf
		CASE FMMC_TYPE_MG_GOLF
			RETURN "golf.jpg"
		//Golf
		CASE FMMC_TYPE_MG_PILOT_SCHOOL
			RETURN "pilot.jpg"
		//Range
		CASE FMMC_TYPE_MG_SHOOTING_RANGE
			SWITCH iVariation
				CASE 0 RETURN "range0.jpg"
				CASE 1 RETURN "range1.jpg" 
			ENDSWITCH
		BREAK
		//Tennis
		CASE FMMC_TYPE_MG_TENNIS
			SWITCH iVariation
				CASE 0 RETURN  "ten0.jpg"
				CASE 1 RETURN  "ten1.jpg"
				CASE 2 RETURN  "ten2.jpg"
				CASE 3 RETURN  "ten3.jpg"
				CASE 4 RETURN  "ten4.jpg"
				CASE 5 RETURN  "ten5.jpg"
				CASE 6 RETURN  "ten6.jpg"
			ENDSWITCH
		BREAK
		//Arm
		CASE FMMC_TYPE_MG_ARM_WRESTLING
			SWITCH iVariation
				CASE 0 RETURN  "arm0.jpg"
				CASE 1 RETURN  "arm1.jpg"
				CASE 2 RETURN  "arm2.jpg"
				CASE 3 RETURN  "arm3.jpg"
				CASE 4 RETURN  "arm4.jpg"
				CASE 5 RETURN  "arm5.jpg"
				CASE 6 RETURN  "arm6.jpg"
				CASE 7 RETURN  "arm7.jpg"
				CASE 8 RETURN  "arm8.jpg"
				CASE 9 RETURN  "arm9.jpg"
			ENDSWITCH
		BREAK
		//Darts
		CASE FMMC_TYPE_MG_DARTS
			SWITCH iVariation
				CASE 0 RETURN "darts.jpg"			
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC


/// PURPOSE:gets name of the picture for the 
FUNC FLOAT GET_FMMC_MINI_GAME_CAMERA_FOC(INT iType, INT iVariation = 0)
	SWITCH iType
		//Golf
		CASE FMMC_TYPE_MG_GOLF
			RETURN 40.5914
		//Pilot School
		CASE FMMC_TYPE_MG_PILOT_SCHOOL
			RETURN 50.0
		//Range
		CASE FMMC_TYPE_MG_SHOOTING_RANGE
			SWITCH iVariation
				CASE 0 RETURN 50.0000
				CASE 1 RETURN 50.0000 
			ENDSWITCH
		BREAK
		//Tennis
		CASE FMMC_TYPE_MG_TENNIS
			SWITCH iVariation
				CASE 0 RETURN  50.0000
				CASE 1 RETURN  40.4324
				CASE 2 RETURN  42.2709
				CASE 3 RETURN  43.6444
				CASE 4 RETURN  37.3862
				CASE 5 RETURN  38.6952
				CASE 6 RETURN  40.9091
			ENDSWITCH
		BREAK
		//Arm
		CASE FMMC_TYPE_MG_ARM_WRESTLING
			SWITCH iVariation
				CASE 0 RETURN  40.5238
				CASE 1 RETURN  47.4321
				CASE 2 RETURN  38.6571
				CASE 3 RETURN  44.0641
				CASE 4 RETURN  39.2991
				CASE 5 RETURN  31.7484
				CASE 6 RETURN  35.6342
				CASE 7 RETURN  36.0072
				CASE 8 RETURN  42.5782
				CASE 9 RETURN  37.4336
			ENDSWITCH
		BREAK
		//Darts
		CASE FMMC_TYPE_MG_DARTS
			RETURN 50.0
	ENDSWITCH
	RETURN 50.0000
ENDFUNC
FUNC INT GET_MAX_MINI_GAME_VARIATIONS(INT iType)
	SWITCH iType
		CASE FMMC_TYPE_MG_GOLF				
			RETURN ciFM_MINI_GAME_MAX_GOLF
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	
			RETURN ciFM_MINI_GAME_MAX_SHOOTING_RANGE
		CASE FMMC_TYPE_MG_TENNIS			
			RETURN ciFM_MINI_GAME_MAX_TENNIS
		CASE FMMC_TYPE_MG_DARTS				
			RETURN ciFM_MINI_GAME_MAX_DARTS
		CASE FMMC_TYPE_MG_ARM_WRESTLING		
			RETURN ciFM_MINI_GAME_MAX_ARM_WRESTLING
	ENDSWITCH
	RETURN 1
ENDFUNC

/// PURPOSE:gets the min start number
/// NOTES:	KGM 19/12/12: Redirects this call to the net_mission_info data.
FUNC INT GET_FMMC_MINI_GAME_MIN_START_NUMBER(INT iType)
	RETURN (Get_Minimum_Number_Of_Players_Required_For_MP_Mission(Convert_FM_Mission_Type_To_MissionID(iType)))
ENDFUNC

/// PURPOSE:Gets the max start number
/// NOTES:	KGM 19/12/12: Redirects this call to the net_mission_info data.
FUNC INT GET_FMMC_MINI_GAME_MAX_START_NUMBER(INT iType)
	RETURN (GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(Convert_FM_Mission_Type_To_MissionID(iType)))
ENDFUNC

