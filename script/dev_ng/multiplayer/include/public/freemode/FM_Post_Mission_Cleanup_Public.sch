USING "rage_builtins.sch"
USING "globals.sch"

USING "script_network.sch"


FUNC BOOL IS_PLAYER_ON_CONTACT_STANDARD_OR_HEIST_MISSION(PLAYER_INDEX playerID)
	
	IF NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION)
		RETURN FALSE
	ENDIF
	
	IF Is_Player_Currently_On_MP_Versus_Mission(playerID)
		RETURN FALSE
	ENDIF
	IF Is_Player_Currently_On_MP_CTF_Mission(playerID)
		RETURN FALSE
	ENDIF
	IF Is_Player_Currently_On_MP_LTS_Mission(playerID)
		RETURN FALSE
	ENDIF
	
	IF Is_Player_Currently_On_MP_Heist(playerID)
	OR Is_Player_Currently_On_MP_Heist_Planning(playerID)	// KGM 5/3/14: Also check for Heist Planning
		RETURN TRUE
	ENDIF
	
	IF Is_Player_Currently_On_MP_Contact_Mission(playerID) 
		RETURN TRUE
	ENDIF
	
	IF Is_Player_Currently_On_MP_Random_Event(playerID)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


// KGM 5/3/14: Heist Planning missions should be treated the same as Heist missions
FUNC BOOL IS_PLAYER_ON_CONTACT_OR_HEIST_MISSION(PLAYER_INDEX playerID)

	IF Is_Player_Currently_On_MP_Heist(playerID)
	OR Is_Player_Currently_On_MP_Heist_Planning(playerID)	// KGM 5/3/14: Also check for Heist Planning
		RETURN TRUE
	ENDIF
	
	IF Is_Player_Currently_On_MP_Contact_Mission(playerID) 
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_ON_LTS_OR_CTF_MISSION(PLAYER_INDEX playerID)
	
	IF Is_Player_Currently_On_MP_LTS_Mission(playerID)
		RETURN TRUE
	ENDIF
	
	IF Is_Player_Currently_On_MP_CTF_Mission(playerID)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


