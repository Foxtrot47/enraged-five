USING "net_weapon_functions.sch"
USING "FMMC_Shared_External.sch"
USING "FMMC_Vars.sch" //Replace with FMMC_Consts or similar

FUNC BOOL IS_THIS_A_HEIST_CREATION()
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF Is_Player_Currently_On_MP_Heist(GET_PLAYER_INDEX()) 
		OR Is_Player_Currently_On_MP_Heist_Planning(GET_PLAYER_INDEX())
			PRINTLN("[MMacK] IS_THIS_A_HEIST_CREATION = TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	PRINTLN("[MMacK] IS_THIS_A_HEIST_CREATION = FALSE")
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Debug Text Printing
// ##### Description: 
// ##### TODO: Variables should not be in a shared header - Remove/Replace
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD

VECTOR vBlankDbg = <<0, 0, 999>>
INT iBlankDbg = -4242
FLOAT fBlankDbg = -999.42
INT iPrintNum

PROC PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(STRING sName, STRING sAppend, VECTOR vAppend, INT iAppend = -4242, FLOAT fAppend = -999.42, BOOL bPrintMeToo = FALSE, BOOL bCallstack = FALSE)
	
	TEXT_LABEL_63 tl63 = sName
	VECTOR vPos = <<0.3, 0.20+(0.03*iPrintNum), 0.20+(0.02*iPrintNum)>>
	
	IF iPrintNum > 18
		vPos.x += 0.3
		vPos.y -= (0.02*17)
		vPos.z += 0.3
	ENDIF
	
	IF iAppend != iBlankDbg
		tl63 += "  "
		tl63 += iAppend
	ENDIF
	
	IF fAppend != fBlankDbg
		tl63 += "  "
		tl63 += FLOAT_TO_STRING(fAppend)
	ENDIF
	
	IF NOT IS_STRING_NULL(sAppend)
	AND NOT ARE_STRINGS_EQUAL(sAppend, "")
		tl63 += "  "
		tl63 += sAppend
	ENDIF
	
	IF NOT ARE_VECTORS_EQUAL(vAppend, vBlankDbg)
		tl63 += FLOAT_TO_STRING(vAppend.x)
		DRAW_DEBUG_TEXT_2D(tl63, vPos)
	
		vPos.x += 0.13
		tl63 = FLOAT_TO_STRING(vAppend.y)		
		DRAW_DEBUG_TEXT_2D(tl63, vPos)
		
		vPos.x += 0.1
		tl63 = FLOAT_TO_STRING(vAppend.z)		
		DRAW_DEBUG_TEXT_2D(tl63, vPos)
	ELSE
		DRAW_DEBUG_TEXT_2D(tl63, vPos)
	ENDIF
	
	IF bCallstack
		DEBUG_PRINTCALLSTACK()
	ENDIF
	
	IF bPrintMeToo
		PRINTLN("[PRINT_LUKE_ON_SCREEN_DEBUG_TEXT] - ", sName, "       ", sAppend, "       ", vAppend, "       ", iAppend, "       ", fAppend)
	ENDIF
	iPrintNum++
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
ENDPROC

PROC PRINT_ON_SCREEN_DEBUG_TEXT(STRING sName)
	
	TEXT_LABEL_63 tl63 = sName
	VECTOR vPos = <<0.3, 0.20+(0.03*iPrintNum), 0.20+(0.02*iPrintNum)>>
	
	IF iPrintNum > 18
		vPos.x += 0.3
		vPos.y -= (0.02*17)
		vPos.z += 0.3
	ENDIF
		
	DRAW_DEBUG_TEXT_2D(tl63, vPos)
	
	iPrintNum++
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
ENDPROC
PROC PRINT_ON_SCREEN_DEBUG_TEXT_WITH_VECTOR(STRING sName, VECTOR vVector)
	
	TEXT_LABEL_63 tl63 = sName
	VECTOR vPos = <<0.3, 0.20+(0.03*iPrintNum), 0.20+(0.02*iPrintNum)>>
	
	IF iPrintNum > 18
		vPos.x += 0.3
		vPos.y -= (0.02*17)
		vPos.z += 0.3
	ENDIF
	
	tl63 += " <<"
	tl63 += FLOAT_TO_STRING(vVector.x)
	tl63 += ", "
	tl63 += FLOAT_TO_STRING(vVector.y)
	tl63 += ", "
	tl63 += FLOAT_TO_STRING(vVector.z)
	tl63 += " >>"
	
	DRAW_DEBUG_TEXT_2D(tl63, vPos)
	
	iPrintNum++
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
ENDPROC
PROC PRINT_ON_SCREEN_DEBUG_TEXT_WITH_INT(STRING sName, INT iInt)
	
	TEXT_LABEL_63 tl63 = sName
	VECTOR vPos = <<0.3, 0.20+(0.03*iPrintNum), 0.20+(0.02*iPrintNum)>>
	
	IF iPrintNum > 18
		vPos.x += 0.3
		vPos.y -= (0.02*17)
		vPos.z += 0.3
	ENDIF
	
	tl63 += " "
	tl63 += iInt
		
	DRAW_DEBUG_TEXT_2D(tl63, vPos)
	
	iPrintNum++
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
ENDPROC
PROC PRINT_ON_SCREEN_DEBUG_TEXT_WITH_FLOAT(STRING sName, FLOAT fFloat)
	
	TEXT_LABEL_63 tl63 = sName
	VECTOR vPos = <<0.3, 0.20+(0.03*iPrintNum), 0.20+(0.02*iPrintNum)>>
	
	IF iPrintNum > 18
		vPos.x += 0.3
		vPos.y -= (0.02*17)
		vPos.z += 0.3
	ENDIF
	
	tl63 += " "
	tl63 += FLOAT_TO_STRING(fFloat)
		
	DRAW_DEBUG_TEXT_2D(tl63, vPos)
	
	iPrintNum++
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
ENDPROC

#ENDIF

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Common Text, Lines, Markers and Shapes
// ##### Description: Functions for diplaying various visual elements used by the controllers and creators
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC DRAW_DEBUG_ARROW(VECTOR vPos, FLOAT fHeading, FLOAT fOffset, FLOAT fSize = 2.0, INT iR = 255, INT iG = 255, INT iB = 0, FLOAT fOffsetDefaultZ = 0.0)

	VECTOR vScale = <<fSize, fSize, fSize>>

	DRAW_MARKER(MARKER_CHEVRON_1, 
			GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading , <<0.0, (fOffset/2), 0.0>>) + <<0.0, 0.0, 0.5+fOffsetDefaultZ>>, 
			<<0.0, 0.0, 0.0>>, 
			<<90.0, fHeading - 180.0, 0.0>>, 
			vScale,
			iR, iG, iB, 140, FALSE, FALSE, EULER_XYZ)
	
ENDPROC

PROC DRAW_WIRE_CYLINDER(VECTOR p0, VECTOR p1, float radius, int r, int g, int b, int a)

	VECTOR vertexArray[CIRCLE_SEGMENTS*2]
	GET_CYLINDER_VERTICES(p0, p1, radius, vertexArray)
	
	INT i
	
	REPEAT CIRCLE_SEGMENTS i
	
		INT next = i + 1
		INT top = i + CIRCLE_SEGMENTS
		INT topNext = top + 1
		
		IF i + 1 >= CIRCLE_SEGMENTS
			next = 0
			topNext = CIRCLE_SEGMENTS
		ENDIF
		
		DRAW_LINE(p0, vertexArray[i], r, g, b, a)
		DRAW_LINE(p1, vertexArray[top], r, g, b, a)
		DRAW_LINE(vertexArray[i], vertexArray[next], r, g, b, a)
		DRAW_LINE(vertexArray[top], vertexArray[topNext], r, g, b, a)
		DRAW_LINE(vertexArray[top], vertexArray[i], r, g, b, a)
		
	ENDREPEAT

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Common Entity Helpers
// ##### Description: Helpers for dealing with entities in the controllers and creators 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_CREATOR_ID_INT_DECOR_ON_ENTITY(ENTITY_INDEX eiPassed, INT iMC_EntityID, BOOL bChasePed = FALSE)

	IF bChasePed
		IF DECOR_IS_REGISTERED_AS_TYPE("MC_ChasePedID", DECOR_TYPE_INT)
			IF NOT DECOR_EXIST_ON(eiPassed,"MC_ChasePedID")
				DECOR_SET_INT(eiPassed,"MC_ChasePedID",iMC_EntityID)
				PRINTLN("[RCC MISSION] SET_CREATOR_ID_INT_DECOR_ON_ENTITY: ", iMC_EntityID)
			ENDIF
		ENDIF	
	ELSE
		IF DECOR_IS_REGISTERED_AS_TYPE("MC_EntityID", DECOR_TYPE_INT)
			IF NOT DECOR_EXIST_ON(eiPassed,"MC_EntityID")
				DECOR_SET_INT(eiPassed,"MC_EntityID",iMC_EntityID)
				PRINTLN("[RCC MISSION] SET_CREATOR_ID_INT_DECOR_ON_ENTITY: ", iMC_EntityID)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_VEHICLE_A_HILL_ENTITY(INT iVeh)

	IF NOT IS_KING_OF_THE_HILL()
		RETURN FALSE
	ENDIF

	INT i
	
	FOR i = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
		IF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__VEHICLE
		AND g_FMMC_STRUCT.sKotHData.sHillData[i].iHillEntity = iVeh
			PRINTLN("[KOTH] IS_VEHICLE_A_HILL_ENTITY || Vehicle ", iVeh, " is a hill entity")
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OBJECT_A_HILL_ENTITY(INT iObj)

	IF NOT IS_KING_OF_THE_HILL()
		RETURN FALSE
	ENDIF

	INT i
	
	FOR i = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
		IF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__OBJECT
		AND g_FMMC_STRUCT.sKotHData.sHillData[i].iHillEntity = iObj
			PRINTLN("[KOTH] IS_OBJECT_A_HILL_ENTITY || Object ", iObj, " is a hill entity")
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if a ped has been damaged by melee
/// PARAMS:
///    pedID - ped
/// RETURNS: True or False
///    
FUNC BOOL WAS_PED_DAMAGED_BY_MELEE(PED_INDEX pedID)

	IF NOT DOES_ENTITY_EXIST(pedID)
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_DEAD(pedID) OR IS_PED_INJURED(pedID)
		
		WEAPON_TYPE weaponType = GET_PED_CAUSE_OF_DEATH(pedID)
		
		IF weaponType = WEAPONTYPE_INVALID
			RETURN FALSE
		ENDIF
		
		DAMAGE_TYPE damageType = GET_WEAPON_DAMAGE_TYPE(weaponType)
		WEAPON_GROUP weaponGroup = GET_WEAPONTYPE_GROUP(weaponType)
		
		IF IS_WEAPON_A_MELEE_WEAPON(weaponType)
		OR weaponType = WEAPONTYPE_UNARMED
		OR weaponGroup = WEAPONGROUP_MELEE
		OR damageType = DAMAGE_TYPE_MELEE
		OR WAS_PED_KILLED_BY_TAKEDOWN(pedID)
		OR WAS_PED_KILLED_BY_STEALTH(pedID)
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Common Prop Entity Helpers
// ##### Description: Helpers for dealing with Vehicles in the controllers and creators 

FUNC BOOL IS_PROP_WATER_TYPE(MODEL_NAMES mnPassed)
	IF mnPassed = PROP_JETSKI_RAMP_01
	OR mnPassed = Prop_Dock_Bouy_1
	OR mnPassed = PROP_DOCK_BOUY_2
	OR mnPassed = prop_byard_float_02
	OR mnPassed = PROP_IND_BARGE_01_CR
	OR mnPassed = INT_TO_ENUM(MODEL_NAMES, HASH("ar_prop_ar_jetski_ramp_01_dev"))
	OR mnPassed = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_loch_monster"))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// [ChildDyno]
FUNC MODEL_NAMES GET_MODEL_NAME_OF_CHILD_PROP_FOR_DYNO(MODEL_NAMES mn)
	MODEL_NAMES mnReturn = DUMMY_MODEL_FOR_SCRIPT
	
	//Small 
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_01a"))			mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_b_01a")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_02a"))			mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_b_01a")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_03a"))			mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_b_01a")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_01a_SF"))		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_b_01a_SF")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_Arena_Turntable_02f_SF"))		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_b_01a_SF")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_03a_SF"))		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_b_01a_SF")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_01a_WL"))		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_b_01a_WL")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_02a_WL"))		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_b_01a_WL")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_03a_WL"))		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_b_01a_WL")) ENDIF
	// Large
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Turntable_01b"))			mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Turntable_B_01b")) ENDIF
	
	
	RETURN mnReturn
ENDFUNC

FUNC MODEL_NAMES GET_MODEL_NAME_OF_PARENT_DYNO_FOR_PROP_CHILD(MODEL_NAMES mn)
	MODEL_NAMES mnReturn = DUMMY_MODEL_FOR_SCRIPT
	
	//Small 
	IF mn = INT_TO_ENUM(MODEL_NAMES,HASH("xs_prop_arena_turntable_b_01a"))			mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_01a"))	ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES,HASH("xs_prop_arena_turntable_b_01a"))			mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_02a"))	ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES,HASH("xs_prop_arena_turntable_b_01a"))			mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_03a"))	ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES,HASH("xs_prop_arena_turntable_b_01a_SF")) 		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_01a_SF"))	ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES,HASH("xs_prop_arena_turntable_b_01a_SF")) 		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_Arena_Turntable_02f_SF")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES,HASH("xs_prop_arena_turntable_b_01a_SF")) 		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_03a_SF")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES,HASH("xs_prop_arena_turntable_b_01a_WL")) 		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_01a_WL")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES,HASH("xs_prop_arena_turntable_b_01a_WL")) 		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_02a_WL")) ENDIF
	IF mn = INT_TO_ENUM(MODEL_NAMES,HASH("xs_prop_arena_turntable_b_01a_WL")) 		mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_turntable_03a_WL")) ENDIF
	// Large																											
	IF mn = INT_TO_ENUM(MODEL_NAMES,HASH("xs_Prop_Arena_Turntable_B_01b"))			mnReturn = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Turntable_01b"))	ENDIF
	
	
	RETURN mnReturn
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Common Door Entity Helpers
// ##### Description: Helpers for dealing with Doors in the controllers and creators 

FUNC VECTOR GET_DOOR_DIRECTIONAL_LOCK_RIGHT_DIRECTION(FMMCselectedDoorStruct& sDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	
	VECTOR vDir = <<1,0,0>>
	IF sDoorConfig.eDirectionalLockType = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__FRONT
		vDir = CROSS_PRODUCT(sDoorConfig.vDoorUnlockDirection, <<0,0,1>>)
	ELIF sDoorConfig.eDirectionalLockType = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__BACK
		vDir = CROSS_PRODUCT(-sDoorConfig.vDoorUnlockDirection, <<0,0,1>>)
	ENDIF
	
	IF IS_BIT_SET(sDoor.iBitSet, ciDoorBS_DoorShouldBeFlipped)
		vDir = -vDir
	ENDIF
	
	RETURN vDir
ENDFUNC

FUNC VECTOR GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_OFFSET_BASED_ON_MODEL(FMMCselectedDoorStruct& sDoor)
	
	IF sDoor.mnDoorModel =  V_ILEV_GARAGELIFTDOOR
		RETURN <<0, 0, 0.7>>
	ELIF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_r"))
	OR sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_l"))
		RETURN <<0, 0, 0.7>>
	ELIF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_lobay_gate01"))
		RETURN <<0.0, 0.0, 1.5>>
	ELIF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("prop_gate_prison_01"))
		RETURN <<0.0, 0.0, 1.95>>
	ENDIF
	
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC FLOAT GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_RIGHT_OFFSET_SIZE_BASED_ON_MODEL(FMMCselectedDoorStruct& sDoor)
	
	IF sDoor.mnDoorModel =  V_ILEV_GARAGELIFTDOOR
		RETURN 0.5
	ELIF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_r"))
		RETURN 0.5
	ELIF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_l"))
		RETURN -0.5
	ELIF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_lobay_gate01"))
		RETURN 1.75
	ELIF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("prop_gate_prison_01"))
		RETURN 3.25
	ENDIF
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_FIXER)
		VECTOR vMin, vMax
		GET_MODEL_DIMENSIONS(sDoor.mnDoorModel, vMin, vMax)
		RETURN (vMax.x - vMin.x) * 0.5
	ELSE
		RETURN 0.5
	ENDIF
ENDFUNC

FUNC FLOAT GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_SIZE(FMMCselectedDoorStruct& sDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	FLOAT fSize = 1.0
	IF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_lobay_gate01"))
		fSize = 5.5
	ELIF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_r"))
	OR sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_l"))
		fSize = 1.75
	ELIF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("prop_gate_prison_01"))
		fSize = 5.5
	ENDIF
	
	IF sDoorConfig.fAutomaticDistance > 0
		fSize *= sDoorConfig.fAutomaticDistance * 0.5
	ENDIF
	
	RETURN fSize
ENDFUNC

FUNC VECTOR GET_DOOR_MIDDLE(FMMCselectedDoorStruct& sDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	
	FLOAT fRightDistance = GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_RIGHT_OFFSET_SIZE_BASED_ON_MODEL(sDoor)
	VECTOR vRightVector = GET_DOOR_DIRECTIONAL_LOCK_RIGHT_DIRECTION(sDoor, sDoorConfig)
	
	RETURN (sDoor.vPos + (vRightVector  * fRightDistance))
ENDFUNC

FUNC VECTOR GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION(FMMCselectedDoorStruct& sDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)

	FLOAT fDistance = GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_SIZE(sDoor, sDoorConfig)

	VECTOR vUnlockDirection = sDoorConfig.vDoorUnlockDirection * fDistance
	VECTOR vUnlockPosition = (GET_DOOR_MIDDLE(sDoor, sDoorConfig) - vUnlockDirection)
	
	vUnlockPosition += GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_OFFSET_BASED_ON_MODEL(sDoor)
	
	RETURN vUnlockPosition
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DRAW_FMMC_DIRECTIONAL_DOOR_LOCK_HUD(FMMCselectedDoorStruct& sDoor, FMMC_DOOR_CONFIGURATION& sDoorConfig)
	
	IF sDoorConfig.iBaseLockType != ciDOOR_BASE_LOCK_TYPE__DIRECTIONAL
	OR sDoorConfig.eDirectionalLockType = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NONE
	OR sDoorConfig.eDirectionalLockType = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NO_OVERRIDE
		EXIT
	ENDIF
	
	VECTOR vArrowPos = GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION(sDoor, sDoorConfig)
	VECTOR vArrowDir = sDoorConfig.vDoorUnlockDirection
	FLOAT fArrowHeading = GET_HEADING_FROM_VECTOR_2D(vArrowDir.x, vArrowDir.y)
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	DRAW_DEBUG_ARROW(vArrowPos, fArrowHeading, 0.0, 1.0)
	DRAW_DEBUG_SPHERE(vArrowPos, GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_SIZE(sDoor, sDoorConfig), 255, 0, 0, 50)
	
	DRAW_DEBUG_LINE(vArrowPos, vArrowPos + GET_DOOR_DIRECTIONAL_LOCK_RIGHT_DIRECTION(sDoor, sDoorConfig), 255, 0, 0)
	DRAW_DEBUG_LINE(vArrowPos, vArrowPos + sDoorConfig.vDoorUnlockDirection, 0, 0, 255)
	
ENDPROC
#ENDIF

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Common Vehicle Entity Helpers
// ##### Description: Helpers for dealing with Vehicles in the controllers and creators 

/// PURPOSE: Returns the colour index and text label for specified menu item
FUNC INT GET_COLOUR_FROM_RANDOM_INT(INT iRandomInt)
	
	SWITCH iRandomInt	
		CASE 0		RETURN 15	 //- BLACK
		CASE 1		RETURN 16	 //- BLACK POLY
		CASE 2		RETURN 18	 //- SILVER
		CASE 3		RETURN 19	 //- GUNMETAL
		CASE 4		RETURN 14	 //- GREY
		CASE 5		RETURN 111	 //- WHITE
		CASE 6		RETURN 27	 //- RED
		CASE 7		RETURN 40	 //- MATTE DARK RED
		CASE 8		RETURN 41	 //- MATTE ORANGE
		CASE 9		RETURN 42	 //- MATTE YELLOW
		CASE 10		RETURN 49	 //- DARK GREEN
		CASE 11		RETURN 50	 //- GREEN
		CASE 12		RETURN 54	 //- AQUA BLUE
		CASE 13		RETURN 80	 //- BLUE POLY
		CASE 14		RETURN 62	 //- DARK BLUE
		CASE 15		RETURN 64	 //- BLUE
		CASE 16		RETURN 88	 //- YELLOW
		CASE 17		RETURN 89	 //- OFF YELLOW
		CASE 18		RETURN 105	 //- DESSERT SAND
		CASE 19		RETURN 37	 //- GOLD
		CASE 20		RETURN 90	 //- BRONZE
		CASE 21		RETURN 135	 //- HOT PINK
		CASE 22		RETURN 137	 //- DARK SALMON
		CASE 23		RETURN 136	 //- SALMON PINK
		CASE 24		RETURN 145	 //- PURPLE
		CASE 25		RETURN 71	 //- DARK PURPLE
		CASE 26		RETURN 92	 //- LIME GREEN
		CASE 27		RETURN 53	 //- BRIGHT GREEN
		CASE 28		RETURN 70	 //- ULTRA BLUE
		CASE 29		RETURN 31	 // - GRACE RED
		CASE 30		RETURN 34	 // - CABERNET RED
		CASE 31		RETURN 38	 // - ORANGE
		CASE 32		RETURN 138	 // - BRIGHT ORANGE	
		CASE 33		RETURN 12	 //- MATTE BLACK
		CASE 34		RETURN 13	 //- MATTE GRAY
		CASE 35		RETURN 112	 //- MATTE ICE WHITE
		CASE 36		RETURN 83	 //- MATTE BLUE
		CASE 37		RETURN 39	 //- MATTE RED
		CASE 38		RETURN 128	 //- MATTE GREEN
		CASE 39		RETURN 55	 //- MATTE LIME GREEN
		CASE 40		RETURN 148	 //- MATTE SCHAFTER PURPLE
		CASE 41		RETURN 153	 // - MATTE DARK EARTH
		CASE 42		RETURN 154	 // - MATTE DESERT TAN		
		CASE 43		RETURN 117	 //- BRUSHED STEEL
		CASE 44		RETURN 119	 //- BRUSHED ALUMINIUM
		CASE 45		RETURN 159	 //- BRUSHED GOLD
		CASE 46		RETURN 120	 //- CHROME
		CASE 47		RETURN 67	 //- Diamond Blue
	ENDSWITCH
	
	RETURN 135
	
ENDFUNC

FUNC BOOL SHOULD_NEW_RANDOM_COLOUR_BE_APPLIED_TO_DLC_VEHICLE(VEHICLE_INDEX veh)
	
	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(veh)
	
	IF mnVeh = DUKES2
	OR mnVeh = VIGILANTE
	OR mnVeh = VISIONE
	OR mnVeh = RETINUE
	OR mnVeh = RAPIDGT3
	OR mnVeh = CYCLONE
	OR mnVeh = YOSEMITE
	OR mnVeh = HERMES
	OR mnVeh = STREITER
	OR mnVeh = SC1
	OR mnVeh = RIATA
	OR mnVeh = RAIDEN
	OR mnVeh = GT500
	OR mnVeh = Z190
	OR mnVeh = HUSTLER
	OR mnVeh = NEON
	OR mnVeh = KAMACHO
	OR mnVeh = COMET5
	OR mnVeh = COMET4
	OR mnVeh = SAVESTRA
	OR mnVeh = VISERIS
	OR mnVeh = TEZERACT
	OR mnVeh = ELLIE
	OR mnVeh = ISSI3
	OR mnVeh = HOTRING
	OR mnVeh = FLASHGT
	OR mnVeh = SEASPARROW
	OR mnVeh = SWINGER
	OR mnVeh = ROMERO
	OR mnVeh = MENACER
	OR mnVeh = FREECRAWLER
	OR mnVeh = STAFFORD
	OR mnVeh = TOROS
	OR mnVeh = CLIQUE
	OR mnVeh = ITALIGTO
	OR mnVeh = SCHLAGEN
	OR mnVeh = DEVESTE
	OR mnVeh = PBUS2
	OR mnVeh = MULE4
	OR mnVeh = SPEEDO4
	OR mnVeh = ISSI3
	OR mnVeh = BRIOSO
	OR mnVeh = HERMES
	OR mnVeh = HUSTLER
	OR mnVeh = RAPTOR
	OR mnVeh = TAMPA2
	OR mnVeh = JESTER3
	OR mnVeh = SWINGER
	OR mnVeh = ENTITY2
	OR mnVeh = TAIPAN
	OR mnVeh = TOROS
	OR mnVeh = BALLER4
	OR mnVeh = FREECRAWLER
	OR mnVeh = KAMACHO
	OR mnVeh = OPPRESSOR
	OR mnVeh = DIABLOUS2
	OR mnVeh = BARRAGE
	OR mnVeh = THRUSTER
	OR mnVeh = VOLATOL
	OR mnVeh = ALPHAZ1
	OR mnVeh = BLAZER5
	OR mnVeh = DRAFTER
	OR mnVeh = ISSI7
	OR mnVeh = PARAGON
	OR mnVeh = GAUNTLET3
	OR mnVeh = KOMODA
	OR mnVeh = VSTR
	OR mnVeh = REBLA
	OR mnVeh = ZHABA
	OR mnVeh = SUGOI
	OR mnVeh = SULTAN2
	OR mnVeh = EVERON
	OR mnVeh = VAGRANT
	OR mnVeh = OUTLAW
	#IF FEATURE_COPS_N_CROOKS
	OR mnVeh = ZOKU
	OR mnVeh = SURFER3
	#ENDIF
	OR mnVeh = DUKES3
	OR mnVeh = GAUNTLET5
	OR mnVeh = CLUB
	OR mnVeh = BTYPE2
	OR mnVeh = PEYOTE3
	OR mnVeh = YOSEMITE3
	OR mnVeh = REBEL2
	OR mnVeh = MANANA2
	OR mnVeh = FORMULA
	OR mnVeh = FORMULA2
	OR mnVeh = OPENWHEEL1
	OR mnVeh = OPENWHEEL2
	OR mnVeh = SEMINOLE2
	OR mnVeh = YOUGA3
	OR mnVeh = GLENDALE2
	OR mnVeh = TIGON
	OR mnVeh = ITALIRSX
	OR mnVeh = WEEVIL
	OR mnVeh = BRIOSO2
	OR mnVeh = SLAMTRUCK
	OR mnVeh = MANCHEZ2
	OR mnVeh = CALICO
	OR mnVeh = JESTER4
	OR mnVeh = ZR350
	OR mnVeh = REMUS
	OR mnVeh = VECTRE
	OR mnVeh = CYPHER
	OR mnVeh = DOMINATOR7
	OR mnVeh = COMET6
	OR mnVeh = WARRENER2
	OR mnVeh = RT3000
	OR mnVeh = TAILGATER2
	OR mnVeh = SULTAN3
	OR mnVeh = DOMINATOR8
	OR mnVeh = PREVION
	OR mnVeh = EUROS
	OR mnVeh = GROWLER
	OR mnVeh = VETO
	OR mnVeh = VETO2
	OR mnVeh = GRANGER2
	OR mnVeh = IGNUS
	OR mnVeh = DEITY
	OR mnVeh = BUFFALO4
	OR mnVeh = ASTRON
	OR mnVeh = COMET7
	OR mnVeh = BALLER7
	OR mnVeh = JUBILEE
	OR mnVeh = CINQUEMILA
	OR mnVeh = IWAGEN
	OR mnVeh = ZENO
	OR mnVeh = CHAMPION
	OR mnVeh = PATRIOT3
	OR mnVeh = SHINOBI
	OR mnVeh = REEVER
	#IF FEATURE_GEN9_EXCLUSIVE
	OR mnVeh = INT_TO_ENUM(MODEL_NAMES, HASH("ARBITERGT"))
	OR mnVeh = INT_TO_ENUM(MODEL_NAMES, HASH("S95"))
	OR mnVeh = INT_TO_ENUM(MODEL_NAMES, HASH("CYCLONE2"))
	OR mnVeh = INT_TO_ENUM(MODEL_NAMES, HASH("IGNUS2"))
	OR mnVeh = INT_TO_ENUM(MODEL_NAMES, HASH("ASTRON2"))
	#ENDIF
	OR mnVeh = CORSITA
	OR mnVeh = BRIOSO3
	OR mnVeh = DRAUGUR
	OR mnVeh = RHINEHART
	OR mnVeh = KANJOSJ
	OR mnVeh = LM87
	OR mnVeh = POSTLUDE
	OR mnVeh = SM722
	OR mnVeh = TENF
	OR mnVeh = TENF2
	OR mnVeh = TORERO2
	OR mnVeh = VIGERO2
	OR mnVeh = WEEVIL2
	OR mnVeh = GREENWOOD
	OR mnVeh = RUINER4
	OR mnVeh = SENTINEL4
	OR mnVeh = CONADA
	OR mnVeh = OMNISEGT
	OR DOES_DLC_VEHICLE_MODEL_NEED_RANDOM_COLOUR_AND_EXCEPTION(mnVeh)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC FMMC_SET_THIS_VEHICLE_COLOURS(VEHICLE_INDEX veh, INT iColour, INT iLivery, INT iColourComb = -1, INT iColour2 = -1, INT iColour3 = -1, FLOAT fEnvEff = -1.0, INT iCustomModIndex = -1)
	PRINTLN("[RCC_MSSION] FMMC_SET_THIS_VEHICLE_COLOURS called with model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(veh)), " iColour = ", iColour, " iLivery = ", iLivery, " iColourComb = ", iColourComb, " fEnvEff = ", fEnvEff)

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMAKE_VEH_SUITABLE_FOR_SUMO)
		MODEL_NAMES mnCreationModel = GET_ENTITY_MODEL(veh)
		IF mnCreationModel = MONSTER
			IF iColour = 50
				PRINTLN("[LH][VehColour] Monster truck for sumo is coloured Green! Changing to lime green.")
				iColour = 92
			ENDIF
		ENDIF
	ENDIF
	
	IF iLivery != -1
		IF GET_NUM_MOD_KITS(veh) > 0 
			PRINTLN("FMMC_SET_THIS_VEHICLE_COLOURS - applying mod kit to vehicle") 
			SET_VEHICLE_MOD_KIT(veh, 0) 
		ENDIF
	ENDIF
	
	IF iColour != -1
		IF iColour2 != -1
			SET_VEHICLE_COLOURS(veh, iColour, iColour2)
		ELSE
			SET_VEHICLE_COLOURS(veh, iColour, iColour)
		ENDIF
		SET_VEHICLE_EXTRA_COLOURS(veh, iColour, iColour)
		CLEAR_VEHICLE_CUSTOM_SECONDARY_COLOUR(veh)
		IF iCustomModIndex = -1
			IF iLivery != -1
				IF GET_NUM_VEHICLE_MODS(veh, MOD_LIVERY) > 0
					SET_VEHICLE_MOD(veh, MOD_LIVERY, CLAMP_INT(iLivery - 1, -1, GET_NUM_VEHICLE_MODS(veh, MOD_LIVERY) - 1), FALSE)
				ELIF GET_VEHICLE_LIVERY_COUNT(veh) > 0 AND iLivery < GET_VEHICLE_LIVERY_COUNT(veh)
					SET_VEHICLE_LIVERY(veh, iLivery)
				ENDIF
			ELSE
				SET_VEHICLE_LIVERY(veh, -1)
			ENDIF
		ENDIF
		IF iColour3 != -1
			IF IS_VEHICLE_A_SUPERMOD_MODEL(GET_ENTITY_MODEL(veh))
				SET_VEHICLE_EXTRA_COLOUR_5(veh, iColour3) // trim (tertiary)
			ENDIF
		ENDIF
	ELSE
		IF FMMC_CAN_VEHICLE_COLOURS_BE_EDITED(GET_ENTITY_MODEL(veh))
			INT iRand
			IF NOT SHOULD_NEW_RANDOM_COLOUR_BE_APPLIED_TO_DLC_VEHICLE(veh)
				IF iColourComb > -1 
				AND iColourComb < GET_NUMBER_OF_VEHICLE_COLOURS(veh)
					PRINTLN("[RCC_MSSION] FMMC_SET_THIS_VEHICLE_COLOURS Setting vehicle colour comb ", iColourComb)
					SET_VEHICLE_COLOUR_COMBINATION(veh, iColourComb)
				ELSE
					iRand = GET_RANDOM_INT_IN_RANGE(0, GET_NUMBER_OF_VEHICLE_COLOURS(veh))
					PRINTLN("[RCC_MSSION] FMMC_SET_THIS_VEHICLE_COLOURS no colour chosen and colour Combination not valid. Using random combination ", iRand)
					SET_VEHICLE_COLOUR_COMBINATION(veh, iRand)
				ENDIF
			ELSE
				iRand = GET_RANDOM_INT_IN_RANGE(0, 27)
				iRand = GET_COLOUR_FROM_RANDOM_INT(iRand)
				SET_VEHICLE_EXTRA_COLOURS(veh, iRand, iRand)
				SET_VEHICLE_COLOURS(veh, iRand, iRand)
				PRINTLN("[RCC_MSSION] FMMC_SET_THIS_VEHICLE_COLOURS Setting vehicle colour ", iRand)
			ENDIF
			IF iLivery != -1
				IF GET_NUM_VEHICLE_MODS(veh, MOD_LIVERY) > 0
					SET_VEHICLE_MOD(veh, MOD_LIVERY, CLAMP_INT(iLivery - 1, -1, GET_NUM_VEHICLE_MODS(veh, MOD_LIVERY) - 1), FALSE)
				ELIF GET_VEHICLE_LIVERY_COUNT(veh) > 0 AND iLivery < GET_VEHICLE_LIVERY_COUNT(veh)
					SET_VEHICLE_LIVERY(veh, iLivery)
				ENDIF
			ENDIF
			PRINTLN("FMMC_SET_THIS_VEHICLE_COLOURS - RANDOM COLOUR = ", iRand)
		ELSE
			IF iLivery != -1
				IF GET_NUM_VEHICLE_MODS(veh, MOD_LIVERY) > 0
					SET_VEHICLE_MOD(veh, MOD_LIVERY, CLAMP_INT(iLivery - 1, -1, GET_NUM_VEHICLE_MODS(veh, MOD_LIVERY) - 1), FALSE)
				ELIF GET_VEHICLE_LIVERY_COUNT(veh) > 0 AND iLivery < GET_VEHICLE_LIVERY_COUNT(veh)
					SET_VEHICLE_LIVERY(veh, iLivery)
				ENDIF
			ENDIF
			CLEAR_VEHICLE_CUSTOM_SECONDARY_COLOUR(veh)
			SET_VEHICLE_COLOUR_COMBINATION(veh, 0)
		ENDIF
	ENDIF
	
	IF fEnvEff != -1.0
		SET_VEHICLE_ENVEFF_SCALE(veh, fEnvEff)
	ENDIF
	
	SET_VEHICLE_DIRT_LEVEL(veh, 0.0)
ENDPROC

FUNC INT GET_CARMOD_COLOUR_DATA_FOR_DM(INT iSelection)
	
	SWITCH iSelection

		CASE 0 		RETURN 38 		//- ORANGE
		CASE 1		RETURN 145 		//- PURPLE
		CASE 2		RETURN 135 		//- HOT PINK
		CASE 3		RETURN 50 		//- GREEN
		CASE 4		RETURN 89 		//- OFF YELLOW
		CASE 5		RETURN 80 		//- BLUE POLY
		CASE 6		RETURN 53 		//- BRIGHT GREEN
		CASE 7		RETURN 40 		//- DARK RED
		CASE 8		RETURN 71 		//- DARK PURPLE
		CASE 9		RETURN 62 		//- DARK BLUE
		CASE 10		RETURN 105 		//- DESSERT SAND
		CASE 11		RETURN 136 		//- SALMON PINK
		CASE 12		RETURN 138 		//- BRIGHT ORANGE
		CASE 13		RETURN 54 		//- AQUA BLUE
		CASE 14		RETURN 128 		//- MATTE GREEN
		CASE 15 	RETURN 31 		//- GRACE RED
		CASE 16		RETURN 111		//- WHITE
		CASE 17		RETURN 117 		//- BRUSHED STEEL
		CASE 18		RETURN 159 		//- BRUSHED GOLD
		CASE 19		RETURN 70 		//- ULTRA BLUE
		CASE 20		RETURN 92 		//- LIME GREEN
		CASE 21		RETURN 15		//- BLACK
		CASE 22		RETURN 14 		//- GREY
		CASE 23 	RETURN 34 		//- CABERNET RED
		CASE 24		RETURN 49 		//- DARK GREEN
		CASE 25		RETURN 18 		//- SILVER
		CASE 26		RETURN 90 		//- BRONZE
		CASE 27		RETURN 37 		//- GOLD
		CASE 28		RETURN 64 		//- BLUE
		CASE 29		RETURN 27 		//- RED

	ENDSWITCH
	
	ASSERTLN("GET_CARMOD_COLOUR_DATA_FOR_DM - Selection out of range! This is likely because the player count for deathmatches has been increased. Please add more options to GET_CARMOD_COLOUR_DATA_FOR_DM() | iSelection: ", iSelection)
	PRINTLN("GET_CARMOD_COLOUR_DATA_FOR_DM - Selection out of range! Defaulting vehicle colour to black | iSelection: ", iSelection)
	
	RETURN 15 // BLACK
	
ENDFUNC

FUNC STRING GET_RADIO_STATION_NAME_FOR_EMITTER(INT iEntityStation)
	IF iEntityStation = ciRC_HIDDEN_OLD_SCHOOL_HIP_HOP_RADIO
		RETURN "HIDDEN_RADIO_09_HIPHOP_OLD"
	ELIF iEntityStation = ciRC_FIXER_LOWRIDER_MIX
		RETURN "RADIO_36_AUDIOPLAYER"
	ELIF iEntityStation = ciRC_FIXER_PARTY
		RETURN "HIDDEN_RADIO_SEC_PROMOTER"
	ELIF iEntityStation = ciRC_FIXER_PARTY_INTRO
		RETURN "HIDDEN_RADIO_SEC_PROMOTER_INTRO"
	ELIF iEntityStation = ciRC_FIXER_BILLIONAIRE
		RETURN "HIDDEN_RADIO_SEC_BILLIONAIRE"
	ENDIF
	
	RETURN GET_RADIO_STATION_NAME(iEntityStation)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interiors
// ##### Description: Functions for setting up and processing interiors across creators and controllers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_CASINO_APARTMENT_STYLE_FROM_CREATOR()
	SWITCH g_FMMC_STRUCT.sCasinoApartment.iCasinoApartPattern
		CASE 0 RETURN "Set_Pent_Pattern_01"
		CASE 1 RETURN "Set_Pent_Pattern_02"
		CASE 2 RETURN "Set_Pent_Pattern_03"
		CASE 3 RETURN "Set_Pent_Pattern_04"
		CASE 4 RETURN "Set_Pent_Pattern_05"
		CASE 5 RETURN "Set_Pent_Pattern_06"
		CASE 6 RETURN "Set_Pent_Pattern_07"
		CASE 7 RETURN "Set_Pent_Pattern_08"
		CASE 8 RETURN "Set_Pent_Pattern_09"
	ENDSWITCH
	
	RETURN "Set_Pent_Pattern_01"
ENDFUNC

FUNC STRING GET_CASINO_APARTMENT_LIGHTING_FROM_CREATOR()
	SWITCH g_FMMC_STRUCT.sCasinoApartment.iCasinoApartColour
		CASE 1 RETURN "set_pent_bar_light_0"
		CASE 2 RETURN "set_pent_bar_light_01"
		CASE 3 RETURN "set_pent_bar_light_02"
	ENDSWITCH
	RETURN "set_pent_bar_light_0"
ENDFUNC

FUNC STRING GET_CASINO_APARTMENT_ARCADE_CAB_FROM_CREATOR()
	SWITCH g_FMMC_STRUCT.sCasinoApartment.iCasinoApartArcade
		CASE 1 RETURN "Set_Pent_Arcade_Retro"
		CASE 2 RETURN "Set_Pent_Arcade_Modern"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_CASINO_APARTMENT_PARTY_FROM_CREATOR()
	SWITCH g_FMMC_STRUCT.sCasinoApartment.iCasinoApartParty
		CASE 0 RETURN "set_pent_bar_party_0"
		CASE 1 RETURN "set_pent_bar_party_1"
		CASE 2 RETURN "set_pent_bar_party_2"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC SETUP_CASINO_APARTMENT_PARTY_ENTITY_SETS(INTERIOR_INSTANCE_INDEX& iIdx)
	
	IF g_FMMC_STRUCT.sCasinoApartment.iCasinoApartParty = -1
		EXIT
	ENDIF
	
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Bar_Clutter")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Clutter_01")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Clutter_02")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Clutter_03")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, GET_CASINO_APARTMENT_PARTY_FROM_CREATOR())
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "set_pent_bar_light_0")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "set_pent_bar_light_01")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "set_pent_bar_light_02")
	
	IF g_FMMC_STRUCT.sCasinoApartment.iCasinoApartParty != -1
	AND g_FMMC_STRUCT.sCasinoApartment.iCasinoPartyRadio != -1
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_FM_MISSION_CONTROLLER_SCRIPT_NAME_HASH()) != 0
		SET_STATIC_EMITTER_ENABLED("se_vw_dlc_casino_apart_Apart_Party_Music_01", TRUE)
		SET_STATIC_EMITTER_ENABLED("se_vw_dlc_casino_apart_Apart_Party_Music_02", TRUE)
		SET_STATIC_EMITTER_ENABLED("se_vw_dlc_casino_apart_Apart_Party_Music_03", TRUE)
		SET_STATIC_EMITTER_ENABLED("se_vw_dlc_casino_apart_Apart_Arcade_Room_radio", FALSE)
		SET_STATIC_EMITTER_ENABLED("se_vw_dlc_casino_apart_Apart_Lounge_Room_radio", FALSE)
		
		STRING sRadio = GET_RADIO_STATION_NAME_FOR_EMITTER(g_FMMC_STRUCT.sCasinoApartment.iCasinoPartyRadio)
		SET_EMITTER_RADIO_STATION("se_vw_dlc_casino_apart_Apart_Party_Music_01", sRadio)
		SET_EMITTER_RADIO_STATION("se_vw_dlc_casino_apart_Apart_Party_Music_02", sRadio)
		SET_EMITTER_RADIO_STATION("se_vw_dlc_casino_apart_Apart_Party_Music_03", sRadio)
		
		SET_AMBIENT_ZONE_STATE("IZ_vw_dlc_casino_apart_Apart_Arcade_Room_Party", TRUE, TRUE)
		PRINTLN("SETUP_CASINO_APARTMENT_PARTY_ENTITY_SETS - Apartment Emitters set up")
	ENDIF

ENDPROC

FUNC STRING GET_CASINO_APARTMENT_SPA_DOOR_FROM_CREATOR()

	IF IS_BIT_SET(g_FMMC_STRUCT.sCasinoApartment.iCasinoApartBitSet, ciCasinoApart_SpaClosed)
		RETURN "Set_Pent_Spa_Bar_Closed"
	ENDIF
	
	RETURN "Set_Pent_Spa_Bar_Open"
ENDFUNC

PROC SETUP_CASINO_APARTMENT_SPA_DOOR(INTERIOR_INSTANCE_INDEX& iIdx)
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, GET_CASINO_APARTMENT_SPA_DOOR_FROM_CREATOR())
ENDPROC



PROC TINT_CASINO_APARTMENT_DOORS(INT iTint)
	OBJECT_INDEX objDoor
	VECTOR vDoorCoords = <<959.8186, 38.5589, 116.6799>>
	MODEL_NAMES eDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_door_slide_01a"))
	
	objDoor = GET_CLOSEST_OBJECT_OF_TYPE(vDoorCoords, 0.1, eDoorModel, FALSE, FALSE, FALSE)
	
	IF DOES_ENTITY_EXIST(objDoor)
		SET_OBJECT_TINT_INDEX(objDoor, iTint)
	ENDIF
	
	vDoorCoords = <<957.8350, 39.7984, 116.6799>>
	objDoor = GET_CLOSEST_OBJECT_OF_TYPE(vDoorCoords, 0.1, eDoorModel, FALSE, FALSE, FALSE)
	
	IF DOES_ENTITY_EXIST(objDoor)
		SET_OBJECT_TINT_INDEX(objDoor, iTint)
	ENDIF
ENDPROC

PROC INIT_CASINO_APARTMENT_CREATOR_SETUP(INTERIOR_INSTANCE_INDEX& iIdx)
	IF iIdx = NULL
		EXIT
	ENDIF
	BOOL bNeedRefresh
	
	PRINTLN("INIT_CASINO_APARTMENT_CREATOR_SETUP")
	
	bNeedRefresh = NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iIdx, "Set_Pent_Tint_Shell")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Tint_Shell")
	
	INT iCasinoApartColour = g_FMMC_STRUCT.sCasinoApartment.iCasinoApartColour
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_PreventColourFromMatchingHostApartment)
		
		PLAYER_INDEX piHost = GET_FMMC_LOBBY_LEADER()
		IF piHost != INVALID_PLAYER_INDEX()
			PRINTLN("INIT_CASINO_APARTMENT_CREATOR_SETUP - ciOptionsBS29_PreventColourFromMatchingHostApartment enabled - piHost: ", GET_PLAYER_NAME(piHost))
	
			IF DOES_PLAYER_OWN_A_CASINO_APARTMENT(piHost)						
				PRINTLN("INIT_CASINO_APARTMENT_CREATOR_SETUP - ciOptionsBS29_PreventColourFromMatchingHostApartment enabled - Incrementing Colour.")	
				IF GET_PLAYER_PURCHASED_CASINO_APARTMENT_TINT_OPTION(piHost) = g_FMMC_STRUCT.sCasinoApartment.iCasinoApartColour
					iCasinoApartColour++
				ENDIF
				IF iCasinoApartColour >= 4
					iCasinoApartColour = 1
				ENDIF				
			ELSE
				PRINTLN("INIT_CASINO_APARTMENT_CREATOR_SETUP - ciOptionsBS29_PreventColourFromMatchingHostApartment enabled - piHost does not own a casino apartment.")	
			ENDIF
		ELSE		
			PRINTLN("INIT_CASINO_APARTMENT_CREATOR_SETUP - ciOptionsBS29_PreventColourFromMatchingHostApartment enabled - piHost: IS INVALID.")	
		ENDIF
	ENDIF
	
	SET_INTERIOR_ENTITY_SET_TINT_INDEX(iIdx, "Set_Pent_Tint_Shell", iCasinoApartColour)
	
	//Media room doors
	TINT_CASINO_APARTMENT_DOORS(iCasinoApartColour)
	
	bNeedRefresh = NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iIdx, GET_CASINO_APARTMENT_STYLE_FROM_CREATOR())
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, GET_CASINO_APARTMENT_STYLE_FROM_CREATOR())
	SET_INTERIOR_ENTITY_SET_TINT_INDEX(iIdx, GET_CASINO_APARTMENT_STYLE_FROM_CREATOR(), g_FMMC_STRUCT.sCasinoApartment.iCasinoApartColour)
	
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, GET_CASINO_APARTMENT_LIGHTING_FROM_CREATOR())
	
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_CASINO_APARTMENT_ARCADE_CAB_FROM_CREATOR())
		bNeedRefresh = NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iIdx, GET_CASINO_APARTMENT_ARCADE_CAB_FROM_CREATOR())
		ACTIVATE_INTERIOR_ENTITY_SET(iIdx, GET_CASINO_APARTMENT_ARCADE_CAB_FROM_CREATOR())
		SET_INTERIOR_ENTITY_SET_TINT_INDEX(iIdx, GET_CASINO_APARTMENT_ARCADE_CAB_FROM_CREATOR(), g_FMMC_STRUCT.sCasinoApartment.iCasinoApartColour)
	ELSE
		IF IS_INTERIOR_ENTITY_SET_ACTIVE(iIdx, GET_CASINO_APARTMENT_ARCADE_CAB_FROM_CREATOR())
			bNeedRefresh = TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_CASINO_APARTMENT_PARTY_FROM_CREATOR())
		bNeedRefresh = NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iIdx, GET_CASINO_APARTMENT_PARTY_FROM_CREATOR())
		SETUP_CASINO_APARTMENT_PARTY_ENTITY_SETS(iIdx)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_CASINO_APARTMENT_SPA_DOOR_FROM_CREATOR())
		bNeedRefresh = NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iIdx, GET_CASINO_APARTMENT_SPA_DOOR_FROM_CREATOR())
		SETUP_CASINO_APARTMENT_SPA_DOOR(iIdx)
		SET_INTERIOR_ENTITY_SET_TINT_INDEX(iIdx, GET_CASINO_APARTMENT_SPA_DOOR_FROM_CREATOR(), g_FMMC_STRUCT.sCasinoApartment.iCasinoApartColour)
	ENDIF
	
	PRINTLN("INIT_CASINO_APARTMENT_CREATOR_SETUP - Using colour: ", g_FMMC_STRUCT.sCasinoApartment.iCasinoApartColour, " and pattern ", GET_CASINO_APARTMENT_STYLE_FROM_CREATOR())
	
	IF bNeedRefresh
		PRINTLN("INIT_CASINO_APARTMENT_CREATOR_SETUP - Refreshing interior now!")
		REFRESH_INTERIOR(iIdx)
	ENDIF
ENDPROC

PROC CLEANUP_CASINO_APARTMENT_SETUP(INTERIOR_INSTANCE_INDEX& iIdx)
	IF iIdx = NULL
		EXIT
	ENDIF
	
	PRINTLN("CLEANUP_CASINO_APARTMENT_SETUP called")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Tint_Shell")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, GET_CASINO_APARTMENT_STYLE_FROM_CREATOR())
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, GET_CASINO_APARTMENT_LIGHTING_FROM_CREATOR())
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_CASINO_APARTMENT_ARCADE_CAB_FROM_CREATOR())
		DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, GET_CASINO_APARTMENT_ARCADE_CAB_FROM_CREATOR())
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_CASINO_APARTMENT_PARTY_FROM_CREATOR())
		DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Bar_Clutter")
		DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Clutter_01")
		DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Clutter_02")
		DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Clutter_03")
		DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, GET_CASINO_APARTMENT_PARTY_FROM_CREATOR())
		SET_STATIC_EMITTER_ENABLED("se_vw_dlc_casino_apart_Apart_Party_Music_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("se_vw_dlc_casino_apart_Apart_Party_Music_02", FALSE)
		SET_STATIC_EMITTER_ENABLED("se_vw_dlc_casino_apart_Apart_Party_Music_03", FALSE)
		SET_AMBIENT_ZONE_STATE("IZ_vw_dlc_casino_apart_Apart_Arcade_Room_Party", FALSE, TRUE)
	ENDIF
	
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Spa_Bar_Open")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "Set_Pent_Spa_Bar_Closed")
	
	REMOVE_MODEL_HIDE(<<951.86, 3.55, 117.43>>, 1.0, INT_TO_ENUM(MODEL_NAMES, (HASH("vw_vwint02_bar_spa_doors_closed"))))

ENDPROC

PROC INIT_MUSIC_LOCKER_CREATOR_SETUP(INTERIOR_INSTANCE_INDEX& iIdx)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_ENABLE_DJ_SET)
		EXIT
	ENDIF
	
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "dj_04_lights_04")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "entityset_dj_lighting")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_bar_content")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_dj04")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_dry_ice")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_equipment_upgrade")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_security_upgrade")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_style02_podium")
	ACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_lightgrid_01")
ENDPROC

PROC CLEANUP_MUSIC_LOCKER_SETUP(INTERIOR_INSTANCE_INDEX& iIdx)
	
	IF NOT IS_VALID_INTERIOR(iIdx)
		EXIT
	ENDIF
	
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "dj_04_lights_04")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "entityset_dj_lighting")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_bar_content")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_dj04")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_dry_ice")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_equipment_upgrade")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_security_upgrade")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_style02_podium")
	DEACTIVATE_INTERIOR_ENTITY_SET(iIdx, "int01_ba_lightgrid_01")
ENDPROC

FUNC BOOL IS_MISSION_BASEMENT_INTERIOR_ENABLED()
	RETURN g_FMMC_STRUCT.iMissionBasement != -1
ENDFUNC

FUNC STRING GET_INTERIOR_SET_FOR_MISSION_BASEMENT(INT iVariation)
	SWITCH iVariation
		CASE 0	RETURN "Entity_Set_Style_1"
		CASE 1	RETURN "Entity_Set_Style_2"
		CASE 2	RETURN "Entity_Set_Style_3"
		CASE 3	RETURN "Entity_Set_Style_4"
		CASE 4	RETURN "Entity_Set_Style_5"
	ENDSWITCH
	
	RETURN "Entity_Set_Style_1"
ENDFUNC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Weapons
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC RESET_WEAPON_DAMAGE_MODIFIERS()
	
	PLAYER_INDEX LocalPlayer = PLAYER_ID()
	
	PRINTLN("RESET_WEAPON_DAMAGE_MODIFIERS - Resetting Modifiers to Default.")
	
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_PISTOL, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_COMBATPISTOL, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_APPISTOL, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_SNSPISTOL, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_HEAVYPISTOL, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_VINTAGEPISTOL, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_PISTOL50, 1.0)					
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_MARKSMANPISTOL, 1.0)					
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_MACHINEPISTOL, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_FLAREGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_REVOLVER, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_PISTOL_MK2, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_SNSPISTOL_MK2, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_REVOLVER_MK2, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_DOUBLEACTION, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_RAYPISTOL, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_STUNGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_CERAMICPISTOL, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_NAVYREVOLVER, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_STUNGUNG_MP, 1.0)
	
	// Shotguns																	
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_PUMPSHOTGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_SAWNOFFSHOTGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_ASSAULTSHOTGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_HEAVYSHOTGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_BULLPUPSHOTGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_DBSHOTGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_AUTOSHOTGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_PUMPSHOTGUN_MK2, 1.0)
	
	// Light machine guns															
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_MICROSMG, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_SMG, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_ASSAULTSMG, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_MG, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_COMBATMG, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_GUSENBERG, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_COMBATPDW, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_MINISMG, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_SMG_MK2, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_COMBATMG_MK2, 1.0)
																			
	// Assault rifles														
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_ASSAULTRIFLE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_CARBINERIFLE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_ADVANCEDRIFLE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_SPECIALCARBINE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_BULLPUPRIFLE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_MUSKET, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_COMPACTRIFLE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_ASSAULTRIFLE_MK2, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_CARBINERIFLE_MK2, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_BULLPUPRIFLE_MK2, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_SPECIALCARBINE_MK2, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_RAYCARBINE	, 1.0)
	#IF FEATURE_COPS_N_CROOKS
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_POLICERIFLE, 1.0)
	#ENDIF
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_MILITARYRIFLE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_TACTICALRIFLE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_HEAVYRIFLE, 1.0)
	
	// Sniper Rifles														
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_SNIPERRIFLE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_HEAVYSNIPER, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_MARKSMANRIFLE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_HEAVYSNIPER_MK2, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_MARKSMANRIFLE_MK2, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_PRECISIONRIFLE, 1.0)
	
	//Heavy
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_GRENADELAUNCHER, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_RPG, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_MINIGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_FIREWORK, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_HOMINGLAUNCHER, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_RAILGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_COMPACTLAUNCHER, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_RAYMINIGUN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_EMPLAUNCHER, 1.0)
	
	// Thrown
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_GRENADE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_STICKYBOMB, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_SMOKEGRENADE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_MOLOTOV, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_PETROLCAN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_FERTILIZERCAN, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_PROXMINE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_PIPEBOMB, 1.0)
																										 	
	// Melee																			
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_KNIFE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_NIGHTSTICK, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_BAT, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_CROWBAR, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_GOLFCLUB, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_BOTTLE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_DAGGER, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_KNUCKLE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_MACHETE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_HATCHET, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_HAMMER, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_SWITCHBLADE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_POOLCUE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_BATTLEAXE, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_WRENCH, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_FLASHLIGHT, 1.0)
	SET_WEAPON_DAMAGE_MODIFIER(WEAPONTYPE_DLC_STONE_HATCHET, 1.0)
	
	// Defaults
	IF LocalPlayer != INVALID_PLAYER_INDEX()
		SET_PLAYER_WEAPON_DAMAGE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_WEAPON_DAMAGE)
		SET_PLAYER_WEAPON_DEFENSE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_WEAPON_DEFENSE)
		SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_MELEE_WEAPON_DAMAGE)	
		SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE)
		SET_PLAYER_VEHICLE_DAMAGE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_VEHICLE_DAMAGE)
		SET_PLAYER_VEHICLE_DEFENSE_MODIFIER(LocalPlayer, 1.0)
		SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(LocalPlayer, 1.0)
	ENDIF
	
	IF g_FMMC_STRUCT.wtVehicleWeaponToModifyDamage != WEAPONTYPE_INVALID
		PRINTLN("[RCC MISSION][CLEANUP] Resetting damage modifier for: ", GET_WEAPON_NAME(g_FMMC_STRUCT.wtVehicleWeaponToModifyDamage), " to 1.0")
		SET_WEAPON_DAMAGE_MODIFIER(g_FMMC_STRUCT.wtVehicleWeaponToModifyDamage, 1.0)
	ENDIF
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ammo
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_WEAPON_GROUP(WEAPON_TYPE wtWeapon)

	IF IS_WEAPON_A_PISTOL(wtWeapon)
		RETURN ciDM_WEAPON_GROUP_MODIFIER_PISTOL
	ELIF IS_WEAPON_AN_SMG(wtWeapon)
		RETURN ciDM_WEAPON_GROUP_MODIFIER_SMG
	ELIF IS_WEAPON_AN_ASSAULTRIFLE(wtWeapon)
		RETURN ciDM_WEAPON_GROUP_MODIFIER_AR
	ELIF IS_WEAPON_A_MACHINEGUN(wtWeapon)
		RETURN ciDM_WEAPON_GROUP_MODIFIER_MG
	ELIF IS_WEAPON_A_SNIPER(wtWeapon)
		RETURN ciDM_WEAPON_GROUP_MODIFIER_SNIPER
	ELIF IS_WEAPON_A_MELEE_WEAPON(wtWeapon)
		RETURN ciDM_WEAPON_GROUP_MODIFIER_MELEE
	ELIF IS_WEAPON_A_SHOTGUN(wtWeapon)
		RETURN ciDM_WEAPON_GROUP_MODIFIER_SHOTGUN
	ELIF IS_WEAPON_A_HEAVY(wtWeapon)
		RETURN ciDM_WEAPON_GROUP_MODIFIER_HEAVY
	ELIF IS_WEAPON_A_PROJECTILE(wtWeapon)
		RETURN ciDM_WEAPON_GROUP_MODIFIER_THROWABLE
	ELSE
		PRINTLN("GET_WEAPON_GROUP - Invalid weapon used! Please add weapon to a weapon group. Weapon type: ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(wtWeapon))
	ENDIF
	
	RETURN -1

ENDFUNC

FUNC BOOL CAN_AMMO_BE_AWARDED_TO_WEAPON(WEAPON_TYPE wtWeapon)

	IF wtWeapon = WEAPONTYPE_INVALID
		RETURN FALSE
	ENDIF

	INT iWeaponGroup = GET_WEAPON_GROUP(wtWeapon)
	
	IF iWeaponGroup = -1
	OR iWeaponGroup = ciDM_WEAPON_GROUP_MODIFIER_MELEE
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC

PROC PROCESS_GIVE_PLAYER_AMMO(INT iAmmoQuantity, INT iWeaponGroup = ciDM_GIVE_AMMO_TO_CURRENT, BOOL bBullets = FALSE)

	WEAPON_TYPE wtWeapon
	AMMO_TYPE atAmmoType
	
	IF iWeaponGroup = ciDM_GIVE_AMMO_TO_OFF
		EXIT
	ENDIF
	
	IF iWeaponGroup = ciDM_GIVE_AMMO_TO_CURRENT
	
		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeapon)
		
			IF NOT CAN_AMMO_BE_AWARDED_TO_WEAPON(wtWeapon)
				PRINTLN("PROCESS_GIVE_PLAYER_AMMO - Invalid weapon! Exiting function. Weapon: ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(wtWeapon))
				EXIT
			ENDIF
		
			atAmmoType = GET_PED_AMMO_TYPE_FROM_WEAPON(PLAYER_PED_ID(), wtWeapon)
			ADD_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), atAmmoType, FMMC_GET_AMMO_FOR_PICKUP_WEAPON(wtWeapon, iAmmoQuantity, bBullets))
			PRINTLN("PROCESS_GIVE_PLAYER_AMMO - Giving ammo to current weapon | atAmmoType: ", atAmmoType, " | wtWeapon: ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(wtWeapon))
			
		ENDIF
		
	ELSE
		
		INT iWeaponLoop
		PED_WEAPONS_MP_STRUCT sPedWeapons
		GET_PED_WEAPONS_MP(PLAYER_PED_ID(), sPedWeapons)
		
		FOR iWeaponLoop = 0 TO NUM_PLAYER_PED_WEAPON_SLOTS-1
		
			IF NOT CAN_AMMO_BE_AWARDED_TO_WEAPON(sPedWeapons.sWeaponInfo[iWeaponLoop].eWeaponType)
				RELOOP
			ELSE
			
				wtWeapon = sPedWeapons.sWeaponInfo[iWeaponLoop].eWeaponType
				IF iWeaponGroup = ciDM_GIVE_AMMO_TO_ALL
				OR GET_WEAPON_GROUP(wtWeapon) = (iWeaponGroup - ciDM_GIVE_AMMO_TO_WEAPON_GROUP_START)
					atAmmoType = GET_PED_AMMO_TYPE_FROM_WEAPON(PLAYER_PED_ID(), wtWeapon)
					ADD_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), atAmmoType, FMMC_GET_AMMO_FOR_PICKUP_WEAPON(wtWeapon, iAmmoQuantity, bBullets))
					PRINTLN("PROCESS_GIVE_PLAYER_AMMO - Giving ammo to non-dlc weapon | iWeaponLoop: ", iWeaponLoop, " | atAmmoType: ", atAmmoType, " | wtWeapon: ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(wtWeapon))
				ENDIF
				
			ENDIF
			
		ENDFOR
		
		FOR iWeaponLoop = 0 TO NUMBER_OF_DLC_WEAPONS_MP-1
		
			IF NOT CAN_AMMO_BE_AWARDED_TO_WEAPON(sPedWeapons.sDLCWeaponInfo[iWeaponLoop].eWeaponType)
				RELOOP
			ELSE
			
				wtWeapon = sPedWeapons.sDLCWeaponInfo[iWeaponLoop].eWeaponType
				IF iWeaponGroup = ciDM_GIVE_AMMO_TO_ALL
				OR GET_WEAPON_GROUP(wtWeapon) = (iWeaponGroup - ciDM_GIVE_AMMO_TO_WEAPON_GROUP_START)
					atAmmoType = GET_PED_AMMO_TYPE_FROM_WEAPON(PLAYER_PED_ID(), wtWeapon)
					ADD_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), atAmmoType, FMMC_GET_AMMO_FOR_PICKUP_WEAPON(wtWeapon, iAmmoQuantity, bBullets))
					PRINTLN("PROCESS_GIVE_PLAYER_AMMO - Giving ammo to dlc weapon | iWeaponLoop: ", iWeaponLoop, " | atAmmoType: ", atAmmoType, " | wtWeapon: ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(wtWeapon))
				ENDIF
				
			ENDIF
			
		ENDFOR
		
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Pickups
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_CUSTOM_PICKUP_EVENT(INT iPickup)
	
	IF iPickup = -1
		EXIT
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupType
		CASE ciCUSTOM_PICKUP_TYPE__AMMO
			
			INT iAmmoSetting
			INT iWeaponGroup
			BOOL bBullets
			
			iAmmoSetting = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iClips
			bBullets = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_DM_UseBulletsForAmmo)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_DM_CustomAmmoAllWeapons)
				iWeaponGroup = ciDM_GIVE_AMMO_TO_ALL
			ELSE
				iWeaponGroup = ciDM_GIVE_AMMO_TO_CURRENT
			ENDIF
			
			PRINTLN("PROCESS_CUSTOM_PICKUP_EVENT - GIVING AMMO")
			PROCESS_GIVE_PLAYER_AMMO(iAmmoSetting, iWeaponGroup, bBullets)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_PLAYER_IN_COORD_BLIP_RANGE(VECTOR vCoord, FMMC_BLIP_INFO_STRUCT &sBlipStruct, BOOL bIgnoreZ = FALSE)
	IF sBlipStruct.fBlipRange > 0.0
	AND NOT IS_VECTOR_ZERO(vCoord)
			
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF bIgnoreZ
			vPlayerPos.z = vCoord.z
		ENDIF
		
		FLOAT fDist = VDIST2(vPlayerPos, vCoord)
		IF fDist > POW(sBlipStruct.fBlipRange, 2)
			PRINTLN("[Blip] IS_PLAYER_IN_COORD_BLIP_RANGE - Coord is out of blip range ", fDist ," <= ", POW(sBlipStruct.fBlipRange, 2))
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_COORD_IN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(VECTOR vCoord, FMMC_BLIP_INFO_STRUCT &sBlipStruct)
	
	IF sBlipStruct.fBlipHeightDifference > 0.0
	AND NOT IS_VECTOR_ZERO(vCoord)
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF NOT IS_VECTOR_ZERO(vPlayerPos)
			FLOAT fZDifference = ABSF(vPlayerPos.z - vCoord.z)
			IF fZDifference > sBlipStruct.fBlipHeightDifference
				PRINTLN("[Blip] IS_COORD_IN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP - Coord is out of blip height range ", fZDifference ," <= ", sBlipStruct.fBlipHeightDifference)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PICKUP_HAVE_BLIP(PICKUP_INDEX piPickup, FMMC_BLIP_INFO_STRUCT &sBlipStruct, INT iPickup)

	IF NOT DOES_PICKUP_OBJECT_EXIST(piPickup)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
	
		IF NOT IS_PLAYER_IN_COORD_BLIP_RANGE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].vPos, sBlipStruct, TRUE)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_COORD_IN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].vPos, sBlipStruct)
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: On Kill
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_DM_EXTRA_HEALTH_FROM_SELECTION(FLOAT iDamage, INT iMaxHealth, INT iSelection)

	SWITCH iSelection
		CASE DM_EXTRA_HEALTH_DEFAULT				RETURN 0
		CASE DM_EXTRA_HEALTH_DAMAGE_DEALT			RETURN ROUND(iDamage)
		CASE DM_EXTRA_HEALTH_HALF_DAMAGE_DEALT		RETURN ROUND(iDamage * 0.5)
		CASE DM_EXTRA_HEALTH_10						RETURN ROUND(iMaxHealth * 0.1)
		CASE DM_EXTRA_HEALTH_20						RETURN ROUND(iMaxHealth * 0.2)
		CASE DM_EXTRA_HEALTH_30						RETURN ROUND(iMaxHealth * 0.3)
		CASE DM_EXTRA_HEALTH_40						RETURN ROUND(iMaxHealth * 0.4)
		CASE DM_EXTRA_HEALTH_50						RETURN ROUND(iMaxHealth * 0.5)
		CASE DM_EXTRA_HEALTH_60						RETURN ROUND(iMaxHealth * 0.6)
		CASE DM_EXTRA_HEALTH_70						RETURN ROUND(iMaxHealth * 0.7)
		CASE DM_EXTRA_HEALTH_80						RETURN ROUND(iMaxHealth * 0.8)
		CASE DM_EXTRA_HEALTH_90						RETURN ROUND(iMaxHealth * 0.9)
		CASE DM_EXTRA_HEALTH_100					RETURN iMaxHealth
	ENDSWITCH
	
	RETURN 0

ENDFUNC

//Reset AOE
//Current default value is 1.0 for all weapons.
//Set in "CWeaponInfo()" constructor, corresponding value "m_AoEModifier" in file "game\weapons\info\WeaponInfo.cpp"
PROC RESET_WEAPON_AREA_OF_EFFECT()
	PRINTLN("RESET_WEAPON_AREA_OF_EFFECT - Resetting AOE to Default.")

	//Sticky Bomb
	SET_WEAPON_AOE_MODIFIER(WEAPONTYPE_STICKYBOMB, 1.0)
ENDPROC

/// PURPOSE:
///    Fades the screen from the game camera to black. For use in cutscenes.
/// PARAMS:
///    iTime - Milliseconds length of the fade.
PROC FMMC_SAFE_FADE_SCREEN_OUT_TO_BLACK(INT iTime = 500)
	IF IS_SCREEN_FADED_IN()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(iTime)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Fades the screen from black, back to the game camera. For use in cutscenes.
/// PARAMS:
///    iTime - Milliseconds length of the fade.
PROC FMMC_SAFE_FADE_SCREEN_IN_FROM_BLACK(INT iTime = 500)
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		IF NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(iTime)
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Target Floating Score
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC VECTOR GET_CENTER_OF_SCREEN()

	INT iWidth, iHeight
	GET_SCREEN_RESOLUTION(iWidth, iHeight)
	RETURN CONVERT_PIXELS_TO_SCREENSPACE(TO_FLOAT(iWidth) / 2, TO_FLOAT(iHeight) / 2)
	
ENDFUNC

/// PURPOSE:
///    Convert an y pixel position to a screen float position
/// PARAMS:
///    y -				Y position
///    iScreenHeight - 	Screen height, when passed in prevents a call to GET_SCREEN_RESOLUTION
/// RETURNS:
///    y position as a screen float
FUNC FLOAT GET_PIXEL_Y_TO_FLOAT(FLOAT y, INT iScreenHeight = 0)
	IF iScreenHeight = 0
		INT iScreenWidth
		GET_SCREEN_RESOLUTION(iScreenWidth, iScreenHeight)
	ENDIF
	RETURN y / iScreenHeight
ENDFUNC

/// PURPOSE:
///    Helper function to get a color that matches hud
/// PARAMS:
///    hudColor - The hud color you want to match
/// RETURNS:
///    The INT_COLOR of the hud color you pass in
FUNC INT GET_TEXT_COLOUR_FROM_HUD_COLOUR(HUD_COLOURS hudColour, INT iAlpha = -1)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(hudColour, iR, iG, iB, iA)
	
	IF iAlpha = -1
		RETURN INT_COLOUR(iR, iG, iB, iA)
	ELSE
		RETURN INT_COLOUR(iR, iG, iB, iAlpha)
	ENDIF
ENDFUNC

/// PURPOSE:
///    Sorts the floating score queue based off of time spent alive.
PROC TARGET_SORT_FLOATING_SCORES(TARGET_FLOATING_SCORE& sTargetFloatingScores[])

	INT i, j
	TARGET_FLOATING_SCORE sTempScore
	
	i = 1
	WHILE i < TARGET_MAX_FS
	
		sTempScore = sTargetFloatingScores[i]
		
		j = i
		WHILE (j > 0) AND (sTempScore.fFrameTime < sTargetFloatingScores[j - 1].fFrameTime)
			sTargetFloatingScores[j] = sTargetFloatingScores[j - 1]
			j--
		ENDWHILE
		
		sTargetFloatingScores[j] = sTempScore
		i++
		
	ENDWHILE
ENDPROC

/// PURPOSE:
///    Makes a singular floating score move up the screen.
PROC TARGET_SCROLL_FLOATING_SCORE(TARGET_FLOATING_SCORE& sTargetFloatingScore)

	FLOAT fStart, fEnd
	FLOAT fInterpTime
	FLOAT fAlphaStartFadeOut = 0.5
	
	// Increment our frame count
	sTargetFloatingScore.fFrameTime += GET_FRAME_TIME()
	
	// Find out how much we should be flipping
	fInterpTime = sTargetFloatingScore.fFrameTime / sTargetFloatingScore.fLifetime
	
	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
	
		// Position
		fStart = GET_PIXEL_Y_TO_FLOAT(300)
		fEnd = GET_PIXEL_Y_TO_FLOAT(260)
		
		// Quad OUT
		sTargetFloatingScore.fPosY = -(fEnd - fStart) * (fInterpTime / sTargetFloatingScore.fLifetime) * (fInterpTime - 2) + fStart
		
		// Alpha
		IF fInterpTime < fAlphaStartFadeOut
			sTargetFloatingScore.iAlpha = 255
		ELSE
			sTargetFloatingScore.iAlpha = 255 - ROUND((fInterpTime - fAlphaStartFadeOut) / fAlphaStartFadeOut * 255)
		ENDIF
		
		sTargetFloatingScore.bActive = TRUE
		
	ELSE
		
		// Dissapear
		sTargetFloatingScore.iAlpha = 0
		sTargetFloatingScore.bActive = FALSE
		sTargetFloatingScore.fFrameTime = -1
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Updates all floating scores in the floating score queue.
PROC TARGET_UPDATE_FLOATING_SCORES(TARGET_FLOATING_SCORE& sTargetFloatingScores[])

	INT i, iActiveCount
	BOOL bNeedSort = FALSE
	
	// Scroll the text up after its been displayed
	REPEAT TARGET_MAX_FS i
	
		// Is this active
		IF sTargetFloatingScores[i].bActive
		
			TARGET_SCROLL_FLOATING_SCORE(sTargetFloatingScores[i])
			iActiveCount++
			
			// Sort SPTs when SPT becomes inactive
			IF NOT sTargetFloatingScores[i].bActive
				bNeedSort = TRUE
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	IF iActiveCount > 0
	
		IF bNeedSort
			TARGET_SORT_FLOATING_SCORES(sTargetFloatingScores)
		ENDIF
		
		// Reposition based on latest entry
		FOR i = 0 TO TARGET_MAX_FS - 2
			IF sTargetFloatingScores[i].bActive
			AND sTargetFloatingScores[i+1].bActive
			AND sTargetFloatingScores[i+1].fPosY > sTargetFloatingScores[i].fPosY - sTargetFloatingScores[i].fTargetScoreSpacing
				sTargetFloatingScores[i+1].fPosY = sTargetFloatingScores[i].fPosY - sTargetFloatingScores[i].fTargetScoreSpacing
			ENDIF
		ENDFOR
		
	ENDIF
	
ENDPROC

FUNC BOOL HAS_TARGET_FLOATING_SCORES_TEXTURES_LOADED(TARGET_FLOATING_SCORE &sTargetFloatingScores[])
	
	INT i
	FOR i = (TARGET_MAX_FS - 1) TO 0 STEP -1
	
		IF sTargetFloatingScores[i].bActive
		AND NOT IS_STRING_NULL_OR_EMPTY(sTargetFloatingScores[i].stIconDictionary)
		
			REQUEST_STREAMED_TEXTURE_DICT(sTargetFloatingScores[i].stIconDictionary)
			IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTargetFloatingScores[i].stIconDictionary)
				PRINTLN("[DM_INDICATOR] HAS_TARGET_FLOATING_SCORES_TEXTURES_LOADED - Dictionaries not yet loaded")
				RETURN FALSE
			ENDIF
		ENDIF
		
	ENDFOR
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Draws all active scores in the floating score queue.
PROC TARGET_DRAW_FLOATING_SCORES(TARGET_FLOATING_SCORE &sTargetFloatingScores[])
	
	IF NOT HAS_TARGET_FLOATING_SCORES_TEXTURES_LOADED(sTargetFloatingScores)
		EXIT
	ENDIF
	
	INT iRed, iGreen, iBlue, iAlpha, iColor
	FLOAT fPosX, fPosY, fPosOffsetX, fPosOffsetY, fWidth, fHeight, fTargetScoreScale
	
	TARGET_UPDATE_FLOATING_SCORES(sTargetFloatingScores)
	
	INT i
	FOR i = (TARGET_MAX_FS - 1) TO 0 STEP -1
		
		IF sTargetFloatingScores[i].bActive
		
			SET_TEXT_OUTLINE()
			
			STRING stToUse = sTargetFloatingScores[i].stTextLabel
			IF IS_STRING_NULL_OR_EMPTY(stToUse)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_TimedExplosionRace)
					stToUse = "FMMC_SEL_SEC"
				ELIF (sTargetFloatingScores[i].iPtValue > 0)
					stToUse = "AMMO_REWARD"
				ELSE
					stToUse = "NUMBER"
				ENDIF
				
			ENDIF
			
			fTargetScoreScale = sTargetFloatingScores[i].fTargetScoreScale
			
			fPosOffsetY = GET_RENDERED_CHARACTER_HEIGHT(fTargetScoreScale) * fTargetScoreScale
			
			fPosX = sTargetFloatingScores[i].fPosX
			fPosY = sTargetFloatingScores[i].fPosY - fPosOffsetY / 2.0
			fWidth = fTargetScoreScale
			fHeight = fTargetScoreScale
			
			iColor = GET_TEXT_COLOUR_FROM_HUD_COLOUR(sTargetFloatingScores[i].eColor, sTargetFloatingScores[i].iAlpha)
			IF sTargetFloatingScores[i].iPtValue != 0
				fPosOffsetX = GET_STRING_WIDTH_WITH_NUMBER(stToUse, sTargetFloatingScores[i].iPtValue) * fTargetScoreScale
				DISPLAY_TEXT_LABEL_WITH_NUMBER(stToUse, fPosX, fPosY, fWidth, fHeight, 1.0, 1.0, iColor, sTargetFloatingScores[i].iPtValue, 0,	0, FONT_STANDARD)
			ELSE
				fPosOffsetX = GET_STRING_WIDTH(stToUse) * fTargetScoreScale
				DISPLAY_TEXT_LABEL(stToUse, fPosX, fPosY, fWidth, fHeight, 1.0, 1.0, iColor, 0, 0, FONT_STANDARD)
			ENDIF
			
			// Add Icon
			IF NOT IS_STRING_NULL_OR_EMPTY(sTargetFloatingScores[i].stIconDictionary)
			AND NOT IS_STRING_NULL_OR_EMPTY(sTargetFloatingScores[i].stIconTexture)
			
				GET_HUD_COLOUR(sTargetFloatingScores[i].eColor, iRed, iGreen, iBlue, iAlpha)
				
				fWidth = GET_RENDERED_CHARACTER_HEIGHT(fTargetScoreScale) / GET_ASPECT_RATIO(FALSE)
				fHeight = GET_RENDERED_CHARACTER_HEIGHT(fTargetScoreScale)
				fPosX = sTargetFloatingScores[i].fPosX + (fWidth) * fTargetScoreScale + fPosOffsetX
				fPosY = sTargetFloatingScores[i].fPosY + (fHeight / 2.0) * fTargetScoreScale
				
				DRAW_SPRITE(sTargetFloatingScores[i].stIconDictionary, sTargetFloatingScores[i].stIconTexture, fPosX, fPosY, fWidth, fHeight, 0, iRed, iGreen, iBlue, sTargetFloatingScores[i].iAlpha)
				
			ENDIF
			
		ENDIF
		
	ENDFOR
	
ENDPROC

/// PURPOSE:
///    Finds the next open slot.
FUNC INT TARGET_GET_NEXT_FLOATING_SCORE_SLOT(TARGET_FLOATING_SCORE &sTargetFloatingScores[])
	
	INT index
	
	// Sort first.
	TARGET_SORT_FLOATING_SCORES(sTargetFloatingScores)
	
	// First, go through all scores, and make sure we have an active spot.
	REPEAT TARGET_MAX_FS index
		IF NOT (sTargetFloatingScores[index].bActive)
			RETURN index
		ENDIF
	ENDREPEAT
	
	RETURN TARGET_MAX_FS - 1
	
ENDFUNC

/// PURPOSE:
///    Adds a floating score to our array.
PROC TARGET_ADD_FLOATING_SCORE(
		INT iValue, 
		VECTOR vScoreLoc, 
		HUD_COLOURS eColor, 
		TARGET_FLOATING_SCORE &sTargetFloatingScores[], 
		STRING stTextLabel = NULL, 
		STRING stIconDictionary = NULL, 
		STRING stIconTexture = NULL,
		FLOAT fTargetScoreScale = TARGET_SCORE_SCALE,
		FLOAT fTargetScoreSpacing = TARGET_SCORE_SPACING,
		FLOAT fLifetime = TARGET_SCORE_LIFETIME,
		BOOL bUseScreenCoords = FALSE
	)
	
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - Adding a floating score")
	
	// Get our next slot
	INT iSlot = TARGET_GET_NEXT_FLOATING_SCORE_SLOT(sTargetFloatingScores)
	
	// Set the score up
	FLOAT fPosX
	FLOAT fPosY
	
	IF bUseScreenCoords
		fPosX = vScoreLoc.x
		fPosY = vScoreLoc.y
	ELSE
		GET_SCREEN_COORD_FROM_WORLD_COORD(vScoreLoc, fPosX, fPosY)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(stIconDictionary)
		PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - Requesting streamed texture dictionary: ", stIconDictionary)
		REQUEST_STREAMED_TEXTURE_DICT(stIconDictionary)
	ENDIF
		
	sTargetFloatingScores[iSlot].fPosX = fPosX
	sTargetFloatingScores[iSlot].fPosY = fPosY
	sTargetFloatingScores[iSlot].iAlpha = 255
	sTargetFloatingScores[iSlot].iPtValue = iValue
	sTargetFloatingScores[iSlot].bActive = TRUE
	sTargetFloatingScores[iSlot].fFrameTime = 0
	sTargetFloatingScores[iSlot].eColor = eColor
	sTargetFloatingScores[iSlot].stTextLabel = stTextLabel
	sTargetFloatingScores[iSlot].stIconDictionary = stIconDictionary
	sTargetFloatingScores[iSlot].stIconTexture = stIconTexture
	sTargetFloatingScores[iSlot].fTargetScoreScale = fTargetScoreScale
	sTargetFloatingScores[iSlot].fTargetScoreSpacing = fTargetScoreSpacing
	sTargetFloatingScores[iSlot].fLifetime = fLifetime
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - fPosX: ", sTargetFloatingScores[iSlot].fPosX)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - fPosY: ", sTargetFloatingScores[iSlot].fPosY)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - iAlpha: ", sTargetFloatingScores[iSlot].iAlpha)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - iPtValue: ", sTargetFloatingScores[iSlot].iPtValue)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - bActive: ", sTargetFloatingScores[iSlot].bActive)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - fFrameTime: ", sTargetFloatingScores[iSlot].fFrameTime)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - eColor: ", sTargetFloatingScores[iSlot].eColor)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - stTextLabel: ", sTargetFloatingScores[iSlot].stTextLabel)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - stIconDictionary: ", sTargetFloatingScores[iSlot].stIconDictionary)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - stIconTexture: ", sTargetFloatingScores[iSlot].stIconTexture)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - fTargetScoreScale: ", sTargetFloatingScores[iSlot].fTargetScoreScale)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - fTargetScoreSpacing: ", sTargetFloatingScores[iSlot].fTargetScoreSpacing)
	PRINTLN("[DM_INDICATOR] TARGET_ADD_FLOATING_SCORE - fLifetime: ", sTargetFloatingScores[iSlot].fLifetime)
	#ENDIF
	
	TARGET_SORT_FLOATING_SCORES(sTargetFloatingScores)
	
ENDPROC

FUNC VECTOR GET_POINTS_COORDS(ENTITY_INDEX entIndex)

 	VECTOR vCameraPos = GET_GAMEPLAY_CAM_COORD()
	VECTOR vEntityPos = GET_ENTITY_COORDS(entIndex)
	VECTOR vPosition = vEntityPos - vCameraPos
	FLOAT fMinDistance = 5
	NORMALISE_VECTOR(vPosition)
	vPosition.x *= fMinDistance
	vPosition.y *= fMinDistance
	vPosition.z *= fMinDistance
	
	vPosition += vCameraPos

	RETURN vPosition

ENDFUNC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Swaps
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


DEBUGONLY FUNC STRING GET_VEHICLE_STATE_STRING(VEHICLE_SWAP_STATE eState)
	SWITCH(eState)
		CASE eVehicleSwapState_IDLE				RETURN "eVehicleSwapState_IDLE"
		CASE eVehicleSwapState_INITIALISE		RETURN "eVehicleSwapState_INITIALISE"
		CASE eVehicleSwapState_CREATE			RETURN "eVehicleSwapState_CREATE"
		CASE eVehicleSwapState_APPLY_FORCES		RETURN "eVehicleSwapState_APPLY_FORCES"
		CASE eVehicleSwapState_CLEANUP			RETURN "eVehicleSwapState_CLEANUP"
	ENDSWITCH
	RETURN "Invalid State"
ENDFUNC

PROC DO_TRANSFORM_SHAPETEST(TRANSFORM_SHAPE_TEST &sShapeTest, VECTOR vCurrentPosition, VECTOR vCurrentRotation)
	
	IF sShapeTest.stiShapeTest = NULL
		sShapeTest.vPos = vCurrentPosition
		sShapeTest.vPos.z += 1.0
		sShapeTest.vDimensions = <<2.5, 4.0, 2.0>>
		sShapeTest.vRot =vCurrentRotation
		INT iFlags = SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT
		sShapeTest.stiShapeTest = START_SHAPE_TEST_BOX(sShapeTest.vPos, sShapeTest.vDimensions, sShapeTest.vRot, DEFAULT, iFlags)
	ELSE
		VECTOR vTemp1Test, vTemp2Test
		ENTITY_INDEX hitEntity
		GET_SHAPE_TEST_RESULT(sShapeTest.stiShapeTest, sShapeTest.iResult, vTemp1Test, vTemp2Test, hitEntity)
		sShapeTest.stiShapeTest = NULL
		RELEASE_SCRIPT_GUID_FROM_ENTITY(hitEntity)
	ENDIF
	
ENDPROC

FUNC BOOL IS_VEHICLE_IN_VALID_POSITION_FOR_TRANSFORM(VEHICLE_INDEX viCurrentVeh)
	
	IF NOT IS_VEHICLE_ON_ALL_WHEELS(viCurrentVeh)
	AND NOT IS_ENTITY_IN_AIR(viCurrentVeh)
		PRINTLN("IS_VEHICLE_IN_VALID_POSITION_FOR_TRANSFORM - On ground but not on wheels")
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_UPSIDEDOWN(viCurrentVeh)
		PRINTLN("IS_VEHICLE_IN_VALID_POSITION_FOR_TRANSFORM - Upsidedown")
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_IN_AIR(viCurrentVeh)
	AND NOT IS_ENTITY_UPRIGHT(viCurrentVeh, 50.0)
		PRINTLN("IS_VEHICLE_IN_VALID_POSITION_FOR_TRANSFORM - In the air and not upright enough")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC SET_VEHICLE_SWAP_STATE(VEHICLE_SWAP_STATE &eVehicleSwapState, VEHICLE_SWAP_STATE eNewState)
	PRINTLN("[JS][VehSwap] - SET_VEHICLE_SWAP_STATE - Old State: ",GET_VEHICLE_STATE_STRING(eVehicleSwapState)," New State: ",GET_VEHICLE_STATE_STRING(eNewState))
	eVehicleSwapState = eNewState
ENDPROC

FUNC BOOL SHOULD_TRANSFORM_PED_SKY_DIVE()
	FLOAT fGroundZ
	VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	IF GET_GROUND_Z_FOR_3D_COORD(vPos, fGroundZ, TRUE)
		IF (vPos.z - fGroundZ) > 7.5
			PRINTLN("[JS][VehSwap] - SHOULD_TRANSFORM_PED_SKY_DIVE - TRUE - distance above ground: ", vPos.z - fGroundZ)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GET_PLAYER_VEHICLE_COLOUR_AS_FLOATS(FLOAT &fR, FLOAT &fG, FLOAT &fB, VEHICLE_INDEX vi = NULL)
	INT iR, iG, iB
	IF DOES_ENTITY_EXIST(vi)
		GET_VEHICLE_CUSTOM_PRIMARY_COLOUR(vi, iR, iG, iB)
	ELSE
		INT iA
		GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_GREY), iR, iG, iB, iA)
	ENDIF
	
	IF iR = iG AND iG = iB
	AND iR <= 15
		//Smoke is "too black"
		INT iA
		GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_GREYDARK), iR, iG, iB, iA)
	ENDIF
	
	fR = TO_FLOAT(iR) / 255
	fG = TO_FLOAT(iG) / 255
	fB = TO_FLOAT(iB) / 255
ENDPROC

PROC SET_TRANSFORM_PTFX_EVOLUTIONS(PTFX_ID &ptfx, MODEL_NAMES mn)
	FLOAT fWings, fWidth, fLength
	IF IS_THIS_MODEL_A_BICYCLE(mn)
	OR IS_THIS_MODEL_A_BIKE(mn)
		fWings = 0.0
		fWidth = 0.0
		fLength = 0.0
	ELIF mn = AVENGER
		fWings = 1.25
		fWidth = 1.5
		fLength = 1.25
	ELIF IS_THIS_MODEL_A_PLANE(mn)
		fWings = 1.0
		fWidth = 1.0
		fLength = 1.0
	ELIF mn = THRUSTER
		//Thruster needs specific values because camera is too close.
		fWings = 0.0
		fWidth = 0.2
		fLength = 0.125
	ELIF IS_THIS_MODEL_A_HELI(mn)
		fWings = 0.0
		fWidth = 1.0
		fLength = 1.0
	ELIF mn = TUG
		fWings = 0.0
		fWidth = 1.0
		fLength = 2.5
	ELIF IS_THIS_MODEL_A_BOAT(mn)
		fWings = 0.0
		fWidth = 1.0
		fLength = 1.0
	ELSE
		//Probably a car
		fWings = 0.0
		fWidth = 1.0
		fLength = 1.0
	ENDIF
	IF fWings != 0.0
		SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx, "wings", fWings, FALSE)
	ENDIF
	SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx, "width", fWidth, FALSE)
	SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx, "length", fLength, FALSE)
ENDPROC

PROC DISABLE_SWAP_BAD_CONTROLS()
	//Push Bike Brakes
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_FRONT_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_REAR_BRAKE)
	//Vehicle Weapons
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK2)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM)
	CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 5.0)
ENDPROC

FUNC BOOL IS_THIS_A_VALID_VEHICLE_SWAP_MODEL(MODEL_NAMES mn)
	IF mn != DUMMY_MODEL_FOR_SCRIPT
	AND NOT IS_ON_FOOT_TRANSFORM_MODEL(mn)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_CONSTRAINED_TRANSFORM_FORCE(FLOAT fValue, FLOAT fMaxForce, FLOAT fMinForce)
	fValue = CLAMP(fValue, -fMaxForce, fMaxForce)
	fValue = PICK_FLOAT((fValue > fMinForce OR fValue < -fMinForce), fValue, 0.0)
	RETURN fValue
ENDFUNC

PROC CONSTRAIN_TRANSFORMATION_TORQUE(VECTOR &vTorque)
	vTorque.x = GET_CONSTRAINED_TRANSFORM_FORCE(vTorque.x, cfTRANSFORM_MAX_TORQUE_X, cfTRANSFORM_MIN_TORQUE_X)
	vTorque.y = GET_CONSTRAINED_TRANSFORM_FORCE(vTorque.y, cfTRANSFORM_MAX_TORQUE_Y, cfTRANSFORM_MIN_TORQUE_Y)
	vTorque.z = 0.0 //Adding any yaw seems to spin the vehicle out most of the time
ENDPROC

FUNC BOOL IS_THIS_A_MEDIUM_LARGE_VEHICLE_MODEL(MODEL_NAMES mn)
	IF mn = TECHNICAL3
	Or mn = TECHNICAL2
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_A_LARGE_VEHICLE_MODEL(MODEL_NAMES mn)
	IF IS_THIS_MODEL_A_FIGHTER_JET(mn)
	OR mn = HAULER
	OR mn = HAULER2
	OR mn = PHANTOM
	OR mn = PHANTOM2
	OR mn = PHANTOM3
	OR mn = AVENGER
	OR mn = AKULA
	OR mn = TUG
	OR mn = VOLATOL
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_A_SMALL_VEHICLE_MODEL(MODEL_NAMES mn)
	IF mn = VETO
	OR mn = VETO2
	OR mn = MINITANK
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC VS_PROCESS_IDLE(VEHICLE_SWAP_DATA &sVehicleSwap)
	PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
	IF HAS_NET_TIMER_STARTED(sVehicleSwap.tdGhosted)
		IF HAS_NET_TIMER_EXPIRED(sVehicleSwap.tdGhosted, sVehicleSwap.iGhostTime)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID)
			AND sVehicleSwap.iPTFXTime = -1
				STOP_PARTICLE_FX_LOOPED(sVehicleSwap.ptfxID, FALSE)
				PRINTLN("[JS][VehSwap] PTFX Stopped (Header 2)")
			ENDIF
			#IF IS_DEBUG_BUILD
			IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_NoGhostTransform")
			#ENDIF
			IF NOT IS_BIT_SET(sVehicleSwap.iBitset, ciVEHSWAPBS_NON_CONTACT)
				PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - No longer ghosted")
				SET_LOCAL_PLAYER_AS_GHOST(FALSE, FALSE)
				SET_ENTITY_ALPHA(LocalPlayerPed, 255, FALSE)
			ENDIF
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			RESET_NET_TIMER(sVehicleSwap.tdGhosted)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sVehicleSwap.tdGhosted, 1000)
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID)
					AND sVehicleSwap.iPTFXTime = -1
						STOP_PARTICLE_FX_LOOPED(sVehicleSwap.ptfxID, FALSE)
						PRINTLN("[JS][VehSwap] PTFX Stopped (Header 1)")
					ENDIF
				ENDIF
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)))
					DISABLE_SWAP_BAD_CONTROLS()
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(sVehicleSwap.tdGhosted, 350)
					IF NOT IS_ENTITY_VISIBLE(LocalPlayerPed)
						SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_NET_TIMER_EXPIRED(sVehicleSwap.tdGhosted, ciVEHICLE_SWAP_TRANSFORM_BLOCK)
					PRINTLN("[VehSwap] VS_PROCESS_IDLE - Blocking transform input this frame due to having just transformed")
					//DRAW_DEBUG_TEXT_2D("[VehSwap] VS_PROCESS_IDLE - Blocking transform input this frame due to having just transformed", <<0.5, 0.5, 0.5>>)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM)
				ENDIF
				
				IF IS_BIT_SET(sVehicleSwap.iBitset, ciVEHSWAPBS_BLOCK_ROTATE_INPUT)
				
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UP_ONLY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN_ONLY)
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UP_ONLY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UP_ONLY)
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_UP)
					
					VEHICLE_INDEX vVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
					IF NOT IS_VEHICLE_ON_ALL_WHEELS(vVeh)
						VECTOR vRot = GET_ENTITY_ROTATION(vVeh)
						FLOAT fT = GET_FRAME_TIME() * 1.0
						SET_ENTITY_ROTATION(vVeh, <<LERP_FLOAT(vRot.x, 0.0, fT), LERP_FLOAT(vRot.y, 0.0, fT), vRot.z>>)
						PRINTLN("[VehSwap] VS_PROCESS_IDLE - Rotating the player due to ciVEHSWAPBS_BLOCK_ROTATE_INPUT")
						//DRAW_DEBUG_TEXT_2D("[VehSwap] VS_PROCESS_IDLE - Rotating!", <<0.5, 0.5, 0.5>>)
					ENDIF
					
					DRAW_DEBUG_TEXT_2D("[VehSwap] VS_PROCESS_IDLE - Blocking inputs due to ciVEHSWAPBS_BLOCK_ROTATE_INPUT", <<0.5, 0.75, 0.5>>)
					
					IF HAS_NET_TIMER_EXPIRED(sVehicleSwap.tdGhosted, ciVEHICLE_SWAP_ROTATE_BLOCK)
						CLEAR_BIT(sVehicleSwap.iBitset, ciVEHSWAPBS_BLOCK_ROTATE_INPUT)
						PRINTLN("[VehSwap] VS_PROCESS_IDLE - Clearing ciVEHSWAPBS_BLOCK_ROTATE_INPUT")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sVehicleSwap.tdPTFX)
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sVehicleSwap.tdPTFX, sVehicleSwap.iPTFXTime)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID) 
				STOP_PARTICLE_FX_LOOPED(sVehicleSwap.ptfxID, FALSE)
				PRINTLN("[JS][VehSwap] PTFX Stopped (Header 3)")
				RESET_NET_TIMER(sVehicleSwap.tdPTFX)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RETAIN_APPROXIMATE_WEAPON(VEHICLE_SWAP_DATA &sVehicleSwap, MODEL_NAMES mnNewVeh)
	PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
	UNUSED_PARAMETER(mnNewVeh)
	
	//Try to give player rockets
	IF sVehicleSwap.wtPrevWep = WEAPONTYPE_VEHICLE_WEAPON_PLANE_ROCKET
	OR sVehicleSwap.wtPrevWep = WEAPONTYPE_VEHICLE_SPACE_ROCKET
	OR sVehicleSwap.wtPrevWep = WEAPONTYPE_VEHICLE_ROCKET
	OR sVehicleSwap.wtPrevWep = WEAPONTYPE_DLC_VEHICLE_DOGFIGHTER_MISSILE
	OR sVehicleSwap.wtPrevWep = WEAPONTYPE_DLC_VEHICLE_ROGUE_MISSILE
	OR sVehicleSwap.wtPrevWep = WEAPONTYPE_DLC_VEHICLE_HUNTER_MISSILE
	OR sVehicleSwap.wtPrevWep = WEAPONTYPE_DLC_VEHICLE_VIGILANTE_MISSILE
	OR sVehicleSwap.wtPrevWep = WEAPONTYPE_DLC_VEHICLE_TRAILER_MISSILE
	OR sVehicleSwap.wtPrevWep = WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE
	
		PRINTLN("[TMS][VehSwap] Setting weapon to some form of missile/rocket")
		
		IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_WEAPON_PLANE_ROCKET)
			PRINTLN("[TMS][VehSwap] Weapon is now WEAPONTYPE_VEHICLE_WEAPON_PLANE_ROCKET")
			EXIT
		ELSE
			PRINTLN("[TMS][VehSwap] Weapon can't be ", WEAPONTYPE_VEHICLE_WEAPON_PLANE_ROCKET)
		ENDIF
		
		IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_SPACE_ROCKET)
			PRINTLN("[TMS][VehSwap] Weapon is now WEAPONTYPE_VEHICLE_SPACE_ROCKET")
			EXIT
		ELSE
			PRINTLN("[TMS][VehSwap] Weapon can't be ", WEAPONTYPE_VEHICLE_SPACE_ROCKET)
		ENDIF
		
		IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_ROCKET)
			PRINTLN("[TMS][VehSwap] Weapon is now WEAPONTYPE_VEHICLE_ROCKET")
			EXIT
		ELSE
			PRINTLN("[TMS][VehSwap] Weapon can't be ", WEAPONTYPE_VEHICLE_ROCKET)
		ENDIF
		
		IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_DOGFIGHTER_MISSILE)
			PRINTLN("[TMS][VehSwap] Weapon is now WEAPONTYPE_DLC_VEHICLE_DOGFIGHTER_MISSILE")
			EXIT
		ELSE
			PRINTLN("[TMS][VehSwap] Weapon can't be ", WEAPONTYPE_DLC_VEHICLE_DOGFIGHTER_MISSILE)
			PRINTLN("[TMS][VehSwap] Weapon can't be dogfighter missile")
		ENDIF
		
		IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_ROGUE_MISSILE)
			PRINTLN("[TMS][VehSwap] Weapon is now WEAPONTYPE_DLC_VEHICLE_ROGUE_MISSILE")
			EXIT
		ELSE
			PRINTLN("[TMS][VehSwap] Weapon can't be ", WEAPONTYPE_DLC_VEHICLE_ROGUE_MISSILE)
		ENDIF
		
		IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_HUNTER_MISSILE)
			PRINTLN("[TMS][VehSwap] Weapon is now WEAPONTYPE_DLC_VEHICLE_HUNTER_MISSILE")
			EXIT
		ELSE
			PRINTLN("[TMS][VehSwap] Weapon can't be ", WEAPONTYPE_DLC_VEHICLE_HUNTER_MISSILE)
		ENDIF
		
		IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_VIGILANTE_MISSILE)
			PRINTLN("[TMS][VehSwap] Weapon is now WEAPONTYPE_DLC_VEHICLE_VIGILANTE_MISSILE")
			EXIT
		ELSE
			PRINTLN("[TMS][VehSwap] Weapon can't be ", WEAPONTYPE_DLC_VEHICLE_VIGILANTE_MISSILE)
		ENDIF
		
		IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_TRAILER_MISSILE)
			PRINTLN("[TMS][VehSwap] Weapon is now WEAPONTYPE_DLC_VEHICLE_TRAILER_MISSILE")
			EXIT
		ELSE
			PRINTLN("[TMS][VehSwap] Weapon can't be ", WEAPONTYPE_DLC_VEHICLE_TRAILER_MISSILE)
		ENDIF
		
		IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE)
			PRINTLN("[TMS][VehSwap] Weapon is now WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE")
			EXIT
		ELSE
			PRINTLN("[TMS][VehSwap] Weapon can't be ", WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE)
		ENDIF
		
		PRINTLN("[TMS][VehSwap] No rockets worked")
	ENDIF
ENDPROC

PROC VS_PROCESS_CREATE(VEHICLE_SWAP_DATA &sVehicleSwap, MODEL_NAMES mnNewVeh)
	PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
	PLAYER_INDEX LocalPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)
	BOOL bLocalPlayerPedOK = IS_ENTITY_ALIVE(LocalPlayerPed)
	
	IF IS_THIS_A_VALID_VEHICLE_SWAP_MODEL(mnNewVeh)
	AND HAS_MODEL_LOADED(mnNewVeh)
		BOOL bPrevVehOK = DOES_ENTITY_EXIST(sVehicleSwap.viPrevVeh) AND IS_ENTITY_ALIVE(sVehicleSwap.viPrevVeh)
		IF bPrevVehOK
			SET_ENTITY_COLLISION(sVehicleSwap.viPrevVeh, FALSE)
		ENDIF
		sVehicleSwap.viNewVeh = CREATE_VEHICLE(mnNewVeh, sVehicleSwap.sPrevData.vPosition, sVehicleSwap.sPrevData.fHeading, NOT IS_FAKE_MULTIPLAYER_MODE_SET(), FALSE)
		
		//To fix issue with bike animation
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_PreventGoingIntoStillInVehicleState, TRUE)
		
		IF DOES_ENTITY_EXIST(sVehicleSwap.viNewVeh)
		AND IS_ENTITY_ALIVE(sVehicleSwap.viNewVeh)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
				IF NOT ANIMPOSTFX_IS_RUNNING("MP_TransformRaceFlash")
					ANIMPOSTFX_PLAY("MP_TransformRaceFlash", 2000, FALSE)
				ENDIF
			ENDIF
			BOOL bPedInVeh = IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			BOOL bHeli = IS_THIS_MODEL_A_HELI(mnNewVeh)
			BOOL bPlane = IS_THIS_MODEL_A_PLANE(mnNewVeh)
			DISABLE_SWAP_BAD_CONTROLS()
			SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(sVehicleSwap.viNewVeh) 
			IF mnNewVeh = SUBMERSIBLE
			OR mnNewVeh = SUBMERSIBLE2
				DISABLE_FIRST_PERSON_CAMERA_WATER_CLIPPING_TEST_THIS_UPDATE()
			ENDIF
			IF DOES_ENTITY_EXIST(sVehicleSwap.viPrevVeh)
				SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(sVehicleSwap.viPrevVeh) 
				
				//url:bugstar:4419278
				IF mnNewVeh = THRUSTER
					VECTOR vRotPrev =  GET_ENTITY_ROTATION(sVehicleSwap.viPrevVeh)
					FLOAT fMinRollRotation = 150 //Magical rotation degree
					IF ABSF(vRotPrev.y) > fMinRollRotation
						vRotPrev.y = 0
						SET_ENTITY_ROTATION(sVehicleSwap.viPrevVeh,vRotPrev)
					ENDIF
				ENDIF
				//
			ENDIF
			
			IF bPedInVeh
				IF bPrevVehOK
					sVehicleSwap.sPrevData.vPosition = GET_ENTITY_COORDS(sVehicleSwap.viPrevVeh)
					sVehicleSwap.sPrevData.fHeading = GET_ENTITY_HEADING(sVehicleSwap.viPrevVeh)
					sVehicleSwap.sPrevData.vRotation = GET_ENTITY_ROTATION(sVehicleSwap.viPrevVeh)
					sVehicleSwap.sPrevData.vTorque = GET_ENTITY_ROTATION_VELOCITY(sVehicleSwap.viPrevVeh)
					sVehicleSwap.sPrevData.vVelocity = GET_ENTITY_VELOCITY(sVehicleSwap.viPrevVeh)
					PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Storing old vehicle data. Coords: ",sVehicleSwap.sPrevData.vPosition," Heading: ",sVehicleSwap.sPrevData.fHeading," Rotation: ",sVehicleSwap.sPrevData.vRotation," Torque: ",sVehicleSwap.sPrevData.vTorque," Velocity: ", sVehicleSwap.sPrevData.vVelocity)
				ENDIF
			ELSE	
				IF bLocalPlayerPedOK
					sVehicleSwap.sPrevData.vPosition = GET_ENTITY_COORDS(LocalPlayerPed)
					sVehicleSwap.sPrevData.fHeading = GET_ENTITY_HEADING(LocalPlayerPed)
					sVehicleSwap.sPrevData.vRotation = GET_ENTITY_ROTATION(LocalPlayerPed)
					sVehicleSwap.sPrevData.vTorque = GET_ENTITY_ROTATION_VELOCITY(LocalPlayerPed)
					IF GET_PED_PARACHUTE_STATE(LocalPlayerPed) = PPS_PARACHUTING
						ENTITY_INDEX ei = GET_ENTITY_ATTACHED_TO(LocalPlayerPed)
						IF DOES_ENTITY_EXIST(ei)
							PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Grabbing Velocity from Parachute")
							sVehicleSwap.sPrevData.vVelocity = GET_ENTITY_VELOCITY(ei)
						ELSE
							sVehicleSwap.sPrevData.vVelocity = GET_ENTITY_VELOCITY(LocalPlayerPed)
						ENDIF
					ELSE
						sVehicleSwap.sPrevData.vVelocity = GET_ENTITY_VELOCITY(LocalPlayerPed)
					ENDIF
					IF IS_BIT_SET(sVehicleSwap.iBitSet, ciVEHSWAPBS_HORIZONTAL_PLANE_SWAP)
						//Ignore pitch and push forwards
						sVehicleSwap.sPrevData.vVelocity = <<sVehicleSwap.sPrevData.vVelocity.x, FMAX(sVehicleSwap.sPrevData.vVelocity.y, sVehicleSwap.sPrevData.vVelocity.z), 0>>
						sVehicleSwap.sPrevData.vRotation = <<0.0, sVehicleSwap.sPrevData.vRotation.y, sVehicleSwap.sPrevData.vRotation.z>>
					ENDIF
					PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Storing old ped data. Coords: ",sVehicleSwap.sPrevData.vPosition," Heading: ",sVehicleSwap.sPrevData.fHeading," Rotation: ",sVehicleSwap.sPrevData.vRotation," Torque: ",sVehicleSwap.sPrevData.vTorque," Velocity: ", sVehicleSwap.sPrevData.vVelocity)
					CLEAN_UP_PLAYER_PED_PARACHUTE()
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressInAirEvent, TRUE)
				ENDIF
			ENDIF
			IF bLocalPlayerPedOK
				sVehicleSwap.sPrevData.fForwardSpeed = GET_ENTITY_SPEED(LocalPlayerPed)
			ENDIF
			
			IF bPedInVeh
				SET_ENTITY_NO_COLLISION_ENTITY(sVehicleSwap.viPrevVeh, sVehicleSwap.viNewVeh, FALSE)
			ENDIF	
			SET_ENTITY_AS_MISSION_ENTITY(sVehicleSwap.viNewVeh, FALSE, TRUE)
			SET_ENTITY_PROOFS(sVehicleSwap.viNewVeh, FALSE, FALSE, TRUE, FALSE, FALSE)
			
			IF IS_RACE()
				SET_USER_RADIO_CONTROL_ENABLED(FALSE)
				SET_VEHICLE_RADIO_ENABLED(sVehicleSwap.viNewVeh, FALSE)
			ELSE
				STRING sRadioStationName = GET_RADIO_STATION_NAME(sVehicleSwap.iCurrentRadioStation)
				SET_VEH_HAS_NORMAL_RADIO(sVehicleSwap.viNewVeh)
				SET_VEH_RADIO_STATION(sVehicleSwap.viNewVeh, sRadioStationName)
				PRINTLN("[TMS][Radio] Setting radio station of new vehicle to ", sRadioStationName, " (", sVehicleSwap.iCurrentRadioStation, ")")
			ENDIF
			
			IF mnNewVeh = HOTRING
				SET_VEHICLE_MOD_KIT(sVehicleSwap.viNewVeh, 0) 
				INT iMaxMods = GET_NUM_VEHICLE_MODS(sVehicleSwap.viNewVeh,  MOD_LIVERY)-1
				INT iRandom = GET_RANDOM_INT_IN_RANGE(0, iMaxMods)
				SET_VEHICLE_MOD(sVehicleSwap.viNewVeh, MOD_LIVERY, iRandom)	
			ENDIF 
			
			/////Putting ped into vehicle
			
			#IF IS_DEBUG_BUILD
			VECTOR vVehSwapVehPos = GET_ENTITY_COORDS(sVehicleSwap.viNewVeh)
			PRINTLN("[TMS][VehSwap] Setting player ped into vehicle which is at ", vVehSwapVehPos)
			#ENDIF
			
			IF IS_THIS_A_DEATHMATCH()
			AND IS_FAKE_MULTIPLAYER_MODE_SET()
				TASK_ENTER_VEHICLE(LocalPlayerPed, sVehicleSwap.viNewVeh, -1, DEFAULT, DEFAULT, ECF_WARP_PED)
			ELSE
				SET_PED_INTO_VEHICLE(LocalPlayerPed, sVehicleSwap.viNewVeh)
			ENDIF
			
			IF bPrevVehOK
				SET_ENTITY_VISIBLE(sVehicleSwap.viPrevVeh, FALSE)
			ENDIF
			SET_VEHICLE_ENGINE_ON(sVehicleSwap.viNewVeh, TRUE, TRUE, FALSE)
			
			PRINTLN("[TMS][VehSwap] - PROCESS_VEHICLE_SWAP - Resetting reticule")
			RESET_RETICULE_VALUES()
			
			IF Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
				INT iColour1
				INT iColour2
				GET_VEHICLE_COLOURS(sVehicleSwap.viPrevVeh, iColour1, iColour2)
				FMMC_SET_THIS_VEHICLE_COLOURS(sVehicleSwap.viNewVeh, iColour1, -1, iColour2)
			ENDIF
			
			IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh",DECOR_TYPE_INT)
				DECOR_SET_INT(sVehicleSwap.viNewVeh,"Not_Allow_As_Saved_Veh",1)
			ENDIF
			IF bHeli
			OR bPlane
				IF bHeli
					SET_HELI_TURBULENCE_SCALAR(sVehicleSwap.viNewVeh, 0.3)
				ELSE
					SET_PLANE_TURBULENCE_MULTIPLIER(sVehicleSwap.viNewVeh, 0)
				ENDIF
				SET_HELI_BLADES_FULL_SPEED(sVehicleSwap.viNewVeh) //Does plane props too
				
				IF GET_VEHICLE_HAS_LANDING_GEAR(sVehicleSwap.viNewVeh)
					IF IS_VEHICLE_ON_ALL_WHEELS(sVehicleSwap.viPrevVeh)
					AND NOT IS_RACE() //Only do this check if you're in a mission - does not affect transform race behaviour
						CONTROL_LANDING_GEAR(sVehicleSwap.viNewVeh, LGC_DEPLOY_INSTANT)
						PRINTLN("[TMS][VehSwap] - PROCESS_VEHICLE_SWAP - Deploying landing gear instantly because the previous vehicle was on all wheels")
					ELSE
						CONTROL_LANDING_GEAR(sVehicleSwap.viNewVeh, LGC_RETRACT_INSTANT)
						PRINTLN("[TMS][VehSwap] - PROCESS_VEHICLE_SWAP - Retracting landing gear instantly")
					ENDIF
				ENDIF
				
				IF mnNewVeh = HYDRA
				OR mnNewVeh = TULA
					SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(sVehicleSwap.viNewVeh, 0.0)
				ENDIF
			ENDIF
					
			PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - (Before) New Pos: ", sVehicleSwap.sPrevData.vPosition , " New Rotation: ", sVehicleSwap.sPrevData.vRotation)
			IF NOT GET_CAN_VEHICLE_BE_PLACED_HERE(sVehicleSwap.viNewVeh, sVehicleSwap.sPrevData.vPosition, sVehicleSwap.sPrevData.vRotation)
			AND NOT IS_VECTOR_ZERO(sVehicleSwap.vBackupCoords)
				VECTOR vVehPrevSizeMin
				VECTOR vVehPrevSizeMax
				VECTOR vVehNewSizeMin
				VECTOR vVehNewSizeMax
				GET_VEHICLE_SIZE(sVehicleSwap.viPrevVeh, vVehPrevSizeMin, vVehPrevSizeMax)
				GET_VEHICLE_SIZE(sVehicleSwap.viNewVeh, vVehNewSizeMin, vVehNewSizeMax)
				
				FLOAT fCenterDiff = VDIST2(<<0, 0, vVehPrevSizeMin.z>>, <<0, 0, vVehNewSizeMin.z>>)
								
				PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - fCenterDiff: ", fCenterDiff)
				
				// GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS Might need to use something like that for going upsdie down in tubes?
				sVehicleSwap.sPrevData.vPosition.z += fCenterDiff
				
				IF NOT GET_CAN_VEHICLE_BE_PLACED_HERE(sVehicleSwap.viNewVeh, sVehicleSwap.sPrevData.vPosition, sVehicleSwap.sPrevData.vRotation)
					PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - OVERRIDING LOCATION NEW POS IS BAD ", sVehicleSwap.vBackupCoords)
					sVehicleSwap.sPrevData.vPosition = sVehicleSwap.vBackupCoords
					sVehicleSwap.sPrevData.vRotation = sVehicleSwap.vBackUpRotation
				ELSE
					PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Successfully offset the Vehicle Center spawn.")
				ENDIF
			ENDIF			
			IF IS_THIS_MODEL_A_HELI(mnNewVeh)
				FLOAT fGroundZ
				IF GET_GROUND_Z_FOR_3D_COORD(sVehicleSwap.sPrevData.vPosition, fGroundZ, TRUE)
					IF (sVehicleSwap.sPrevData.vPosition.z - fGroundZ) < 5
						PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Placing a heli on the ground, boost it up a bit")
						sVehicleSwap.sPrevData.vPosition.z += 5
					ENDIF
				ENDIF
			ENDIF
			IF bPlane
				FLOAT fGroundZ
				IF GET_GROUND_Z_FOR_3D_COORD(sVehicleSwap.sPrevData.vPosition, fGroundZ, TRUE)
					IF (sVehicleSwap.sPrevData.vPosition.z - fGroundZ) < 5
						PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Placing a Plane on the ground, boost it up a bit")
						sVehicleSwap.sPrevData.vPosition.z += 2.5
					ENDIF
				ENDIF
			ENDIF
			SET_ENTITY_COORDS_NO_OFFSET(sVehicleSwap.viNewVeh, sVehicleSwap.sPrevData.vPosition) 
			SET_ENTITY_ROTATION(sVehicleSwap.viNewVeh, sVehicleSwap.sPrevData.vRotation)
			#IF IS_DEBUG_BUILD
				VECTOR vNewCoords = GET_ENTITY_COORDS(sVehicleSwap.viNewVeh)
				VECTOR vNewRotation = GET_ENTITY_ROTATION(sVehicleSwap.viNewVeh)
				PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - (After) New Pos: ", vNewCoords , " New Rotation: ", vNewRotation)
			#ENDIF
			IF bHeli
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_THIRD_PERSON_FAR
					PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Setting ped out of top down heli cam")
					SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
				ENDIF
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(mnNewVeh)
			SET_VEHICLE_SWAP_STATE(sVehicleSwap.eVehicleSwapState, eVehicleSwapState_APPLY_FORCES)
		ENDIF
	ELSE
		IF IS_THIS_A_VALID_VEHICLE_SWAP_MODEL(mnNewVeh)
			PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - requesting model")
			REQUEST_MODEL(mnNewVeh)
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF IS_ENTITY_ALIVE(LocalPlayerPed)
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressInAirEvent, TRUE)
				ENDIF
			ENDIF
		ELSE
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF IS_ENTITY_ALIVE(sVehicleSwap.viPrevVeh)
					sVehicleSwap.sPrevData.vRotation = GET_ENTITY_ROTATION(sVehicleSwap.viPrevVeh)
					sVehicleSwap.sPrevData.vVelocity = GET_ENTITY_VELOCITY(sVehicleSwap.viPrevVeh)
					SET_ENTITY_COLLISION(sVehicleSwap.viPrevVeh, FALSE)
				ENDIF
			ELSE
				IF bLocalPlayerPedOK
					sVehicleSwap.sPrevData.vRotation = GET_ENTITY_ROTATION(LocalPlayerPed)
					IF GET_PED_PARACHUTE_STATE(LocalPlayerPed) = PPS_PARACHUTING
						ENTITY_INDEX ei = GET_ENTITY_ATTACHED_TO(LocalPlayerPed)
						IF DOES_ENTITY_EXIST(ei)
							PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Grabbing Velocity from Parachute")
							sVehicleSwap.sPrevData.vVelocity = GET_ENTITY_VELOCITY(ei)
						ELSE
							sVehicleSwap.sPrevData.vVelocity = GET_ENTITY_VELOCITY(LocalPlayerPed)
						ENDIF
					ELSE
						sVehicleSwap.sPrevData.vVelocity = GET_ENTITY_VELOCITY(LocalPlayerPed)
					ENDIF
				ENDIF
			ENDIF
			IF bLocalPlayerPedOK
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					ANIMPOSTFX_PLAY("MP_TransformRaceFlash", 2000, FALSE)
				ENDIF
				CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
				SET_ENTITY_ROTATION(LocalPlayerPed, sVehicleSwap.sPrevData.vRotation)
				IF mnNewVeh = PROP_CS_DILDO_01
					ACTIVATE_PHYSICS(LocalPlayerPed)
					IF IS_BIT_SET(sVehicleSwap.iBitSet, ciVEHSWAPBS_PED_PARACHUTING)
					OR SHOULD_TRANSFORM_PED_SKY_DIVE()
						PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Sky Dive")
						TASK_SKY_DIVE(LocalPlayerPed, TRUE)
					ELSE
						IF sVehicleSwap.sPrevData.vVelocity.z > 1.0
							SET_PED_TO_RAGDOLL(LocalPlayerPed, 0, 1000, TASK_NM_BALANCE)
						ENDIF
					ENDIF
				ELIF mnNewVeh = V_RES_D_DILDO_B
					SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableParachuting, TRUE)
					SET_FORCED_JUMP_THIS_FRAME(LocalPlayer)
					SET_BEAST_JUMP_THIS_FRAME(LocalPlayer)
					PLAY_SOUND_FROM_ENTITY(-1, "Beast_Attack", localPlayerPed, "DLC_AR_Beast_Soundset", FALSE)
				ENDIF
				SET_ENTITY_VELOCITY(LocalPlayerPed, sVehicleSwap.sPrevData.vVelocity)
				IF DOES_ENTITY_EXIST(sVehicleSwap.viPrevVeh) AND IS_ENTITY_ALIVE(sVehicleSwap.viPrevVeh)
					SET_ENTITY_VISIBLE(sVehicleSwap.viPrevVeh, FALSE)
				ENDIF
				FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
			ENDIF
			#IF IS_DEBUG_BUILD
			IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_NoGhostTransform")
			#ENDIF
				IF NOT IS_BIT_SET(sVehicleSwap.iBitset, ciVEHSWAPBS_NON_CONTACT)
					SET_LOCAL_PLAYER_AS_GHOST(TRUE, TRUE)
				ELSE
					SET_LOCAL_PLAYER_AS_GHOST(TRUE)
				ENDIF
				SET_ENTITY_ALPHA(LocalPlayerPed, 175, FALSE)
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Kicking player out of vehicle and ghosting")			
			SET_VEHICLE_SWAP_STATE(sVehicleSwap.eVehicleSwapState, eVehicleSwapState_CLEANUP)	
		ENDIF
	ENDIF
ENDPROC

PROC VS_PROCESS_APPLY_FORCES(VEHICLE_SWAP_DATA &sVehicleSwap, MODEL_NAMES mnNewVeh)
	IF IS_THIS_A_VALID_VEHICLE_SWAP_MODEL(mnNewVeh)	
		IF DOES_ENTITY_EXIST(sVehicleSwap.viNewVeh)
		AND IS_ENTITY_ALIVE(sVehicleSwap.viNewVeh)
			BOOL bPrevVehOK = DOES_ENTITY_EXIST(sVehicleSwap.viPrevVeh) AND IS_ENTITY_ALIVE(sVehicleSwap.viPrevVeh)
			BOOL bPlane = IS_THIS_MODEL_A_PLANE(mnNewVeh)
		
			IF bPlane
				//Planes need a "slight" boost
				SET_VEHICLE_FORWARD_SPEED(sVehicleSwap.viNewVeh, 30)		
			ELSE
				SET_VEHICLE_FORWARD_SPEED(sVehicleSwap.viNewVeh, 1)		
			ENDIF
			PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - FORCE Velocity: ",sVehicleSwap.sPrevData.vVelocity)
			
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_PreventGoingIntoStillInVehicleState, TRUE)

			#IF IS_DEBUG_BUILD
				VECTOR vOldVel = GET_ENTITY_VELOCITY(sVehicleSwap.viNewVeh)
				PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Pre Velocity Change: ",vOldVel)
			#ENDIF
			SET_ENTITY_VELOCITY(sVehicleSwap.viNewVeh, sVehicleSwap.sPrevData.vVelocity)
			#IF IS_DEBUG_BUILD
				VECTOR vNewVel = GET_ENTITY_VELOCITY(sVehicleSwap.viNewVeh)
				PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Post Velocity Change: ",vNewVel)
			#ENDIF
			IF bPrevVehOK
				MODEL_NAMES mnPrev = GET_ENTITY_MODEL(sVehicleSwap.viPrevVeh)
				IF NOT IS_THIS_MODEL_A_PLANE(mnPrev) //Rotational force from air vehicles seem to be very severe
				AND NOT IS_THIS_MODEL_A_HELI(mnPrev)
					PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - FORCE Pre Torque: ",sVehicleSwap.sPrevData.vTorque)
					CONSTRAIN_TRANSFORMATION_TORQUE(sVehicleSwap.sPrevData.vTorque)
					PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - FORCE Post Torque: ",sVehicleSwap.sPrevData.vTorque)
					APPLY_FORCE_TO_ENTITY(sVehicleSwap.viNewVeh, APPLY_TYPE_ANGULAR_IMPULSE, sVehicleSwap.sPrevData.vTorque, <<0,0,0>>, 0 , TRUE, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	SET_VEHICLE_SWAP_STATE(sVehicleSwap.eVehicleSwapState, eVehicleSwapState_CLEANUP)	
ENDPROC

PROC VS_PROCESS_INITIALISE(VEHICLE_SWAP_DATA &sVehicleSwap, MODEL_NAMES mnNewVeh)
	PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		sVehicleSwap.viPrevVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		VEHICLE_SET_JET_WASH_FORCE_ENABLED(FALSE)
		DISABLE_SWAP_BAD_CONTROLS()
		IF IS_ENTITY_ALIVE(sVehicleSwap.viPrevVeh)
			sVehicleSwap.mnPrevVeh = GET_ENTITY_MODEL(sVehicleSwap.viPrevVeh)
			sVehicleSwap.sPrevData.vPosition = GET_ENTITY_COORDS(sVehicleSwap.viPrevVeh)
			sVehicleSwap.sPrevData.fHeading = GET_ENTITY_HEADING(sVehicleSwap.viPrevVeh)
			sVehicleSwap.iCurrentRadioStation = GET_PLAYER_RADIO_STATION_INDEX()
			SET_ENTITY_NO_COLLISION_ENTITY(LocalPlayerPed, sVehicleSwap.viPrevVeh, FALSE)
			PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Setting no collision between player and old vehicle")
			
			GET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, sVehicleSwap.wtPrevWep)
			PRINTLN("[TMS][VehSwap] Saving old swap vehicle weapon as ", sVehicleSwap.wtPrevWep)
			sVehicleSwap.bMissilesHoming = !IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(NETWORK_GET_PLAYER_INDEX_FROM_PED(localPlayerPed))
			PRINTLN("[TMS][VehSwap] Saving homing state as: ", sVehicleSwap.bMissilesHoming)
		ELSE
			PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Previous vehicle is in a bad state, cleanup")
			EXIT
		ENDIF
	ELSE	
		IF IS_ENTITY_ALIVE(LocalPlayerPed)
			sVehicleSwap.sPrevData.vPosition = GET_ENTITY_COORDS(LocalPlayerPed)
			sVehicleSwap.sPrevData.fHeading = GET_ENTITY_HEADING(LocalPlayerPed)
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_NoGhostTransform")
	#ENDIF
		IF NOT IS_BIT_SET(sVehicleSwap.iBitset, ciVEHSWAPBS_NON_CONTACT)
			SET_LOCAL_PLAYER_AS_GHOST(TRUE, TRUE)
		ELSE
			SET_LOCAL_PLAYER_AS_GHOST(TRUE)
		ENDIF
		IF mnNewVeh = THRUSTER
			//SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE)
			SET_ENTITY_ALPHA(LocalPlayerPed, 0, FALSE)
		ELSE
			SET_ENTITY_ALPHA(LocalPlayerPed, 175, FALSE)
		ENDIF
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	PRINTLN("[VehSwap] Setting local player to be invisible!")
	SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE)
	
	SET_PED_RESET_FLAG(LocalPlayerPed, PRF_PreventGoingIntoStillInVehicleState, TRUE)
	
	SET_DISABLE_COLLISIONS_BETWEEN_CARS_AND_CAR_PARACHUTE(TRUE)
	IF IS_THIS_A_VALID_VEHICLE_SWAP_MODEL(mnNewVeh)
		REQUEST_MODEL(mnNewVeh)
	ENDIF		
	IF NOT IS_VECTOR_ZERO(sVehicleSwap.sPrevData.vPosition)
		FLOAT fR, fG, fB
		IF IS_ENTITY_ALIVE(sVehicleSwap.viPrevVeh)
			GET_PLAYER_VEHICLE_COLOUR_AS_FLOATS(fR, fG, fB, sVehicleSwap.viPrevVeh)
		ENDIF
		FLOAT fScale = 1.0
		IF IS_THIS_A_LARGE_VEHICLE_MODEL(mnNewVeh)
			fScale = 2.0
		ENDIF
		IF IS_THIS_A_SMALL_VEHICLE_MODEL(mnNewVeh)
			fScale = 0.6
		ELIF mnNewVeh = TUG
			fScale = 6.5
		ELIF mnNewVeh = DODO
			fScale = 1.75
		ENDIF
		
		VECTOR vOffset = <<0,0,0>>
		
		IF mnNewVeh = MOLOTOK
		OR mnNewVeh = LAZER
		OR mnNewVeh = HYDRA
		OR mnNewVeh = AKULA
			vOffset.y = -2.0
		ENDIF
		IF mnNewVeh = AVENGER
		OR mnNewVeh = VOLATOL
			vOffset.y = -4
			vOffset.z = 5.5
			fScale = 9.0
		ENDIF
		IF mnNewVeh = SPEEDER
			vOffset.x = 0.5
			fScale = 2.0
			vOffset.z = -0.7
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			// Fixes a cleanup issue. url:bugstar:4480470 | Might need to switch to REMOVE_PARTICLE_FX if we still get this.
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID)
				STOP_PARTICLE_FX_LOOPED(sVehicleSwap.ptfxID)
			ENDIF
			
			USE_PARTICLE_FX_ASSET("scr_as_trans")
			sVehicleSwap.ptfxID = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_as_trans_smoke", LocalPlayerPed, vOffset, <<0,0,0>>, fScale, DEFAULT, DEFAULT, DEFAULT, fR, fG, fB)	
			PRINTLN("[JS][VehSwap] PTFX Triggered (2)")
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID) 
				SET_TRANSFORM_PTFX_EVOLUTIONS(sVehicleSwap.ptfxID, mnNewVeh)
			ENDIF
			
			IF IS_THIS_MODEL_A_HELI(sVehicleSwap.mnPrevVeh)
			OR sVehicleSwap.mnPrevVeh = THRUSTER
			AND NOT IS_THIS_MODEL_A_HELI(mnNewVeh)
				SET_BIT(sVehicleSwap.iBitset, ciVEHSWAPBS_BLOCK_ROTATE_INPUT)
				PRINTLN("[JS][VehSwap] Setting ciVEHSWAPBS_BLOCK_ROTATE_INPUT")
			ENDIF
		ENDIF
		SET_VEHICLE_SWAP_STATE(sVehicleSwap.eVehicleSwapState, eVehicleSwapState_CREATE)
		IF IS_THIS_A_VALID_VEHICLE_SWAP_MODEL(mnNewVeh)
			VS_PROCESS_CREATE(sVehicleSwap, mnNewVeh) //Calling this here for a potential 1 frame transform
		ENDIF
	ENDIF
ENDPROC

PROC VS_PROCESS_CLEANUP(VEHICLE_SWAP_DATA &sVehicleSwap, MODEL_NAMES mnNewVeh)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID) 
			SET_TRANSFORM_PTFX_EVOLUTIONS(sVehicleSwap.ptfxID, mnNewVeh)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(sVehicleSwap.viNewVeh)
		IF mnNewVeh = SUBMERSIBLE
		OR mnNewVeh = SUBMERSIBLE2
			DISABLE_FIRST_PERSON_CAMERA_WATER_CLIPPING_TEST_THIS_UPDATE()
		ENDIF
		SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(sVehicleSwap.viNewVeh) 
	ENDIF
	IF DOES_ENTITY_EXIST(sVehicleSwap.viPrevVeh)
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(sVehicleSwap.viPrevVeh)
			NETWORK_REQUEST_CONTROL_OF_ENTITY(sVehicleSwap.viPrevVeh)
		ENDIF
		SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(sVehicleSwap.viPrevVeh) 
	ENDIF
	DISABLE_SWAP_BAD_CONTROLS()
	
	IF mnNewVeh != THRUSTER
		PRINTLN("[VehSwap] Setting local player to be visible again")
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sVehicleSwap.viNewVeh)
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
	OR NOT IS_THIS_A_VALID_VEHICLE_SWAP_MODEL(mnNewVeh)
		IF DOES_ENTITY_EXIST(sVehicleSwap.viPrevVeh)
		
			IF NOT IS_RACE() //Behave differently in missions
				IF NOT NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(sVehicleSwap.viPrevVeh)
					NETWORK_REGISTER_ENTITY_AS_NETWORKED(sVehicleSwap.viPrevVeh)
					PRINTLN("[TMS][VehSwap] Old vehicle in swap isn't registered to this thread - registering before cleaning it up")							
					EXIT
				ENDIF
			ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(sVehicleSwap.viPrevVeh)
				PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Deleting old vehicle")
				SET_ENTITY_AS_MISSION_ENTITY(sVehicleSwap.viPrevVeh, FALSE, TRUE)
				DELETE_VEHICLE(sVehicleSwap.viPrevVeh)
			ELSE
				PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Setting old vehicle ready for cleanup")
				SET_VEHICLE_AS_NO_LONGER_NEEDED(sVehicleSwap.viPrevVeh)
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(sVehicleSwap.viNewVeh)
			AND IS_ENTITY_ALIVE(sVehicleSwap.viNewVeh)
				SET_ENTITY_PROOFS(sVehicleSwap.viNewVeh, FALSE, FALSE, FALSE, FALSE, FALSE)
			ENDIF
			SET_DISABLE_COLLISIONS_BETWEEN_CARS_AND_CAR_PARACHUTE(FALSE)
			VEHICLE_SET_JET_WASH_FORCE_ENABLED(FALSE)
			PRINTLN("[JS][VehSwap] - PROCESS_VEHICLE_SWAP - Resetting struct and starting ghost timer")
			PTFX_ID ptfxTemp
			ptfxTemp = sVehicleSwap.ptfxID
			
			INT iTimerLength
			INT iPTFXTimerLength = -1
			iTimerLength = sVehicleSwap.iGhostTime
			iPTFXTimerLength = sVehicleSwap.iPTFXTime
			
			BOOL bBlockRotate = IS_BIT_SET(sVehicleSwap.iBitset, ciVEHSWAPBS_BLOCK_ROTATE_INPUT)
			
			VEHICLE_SWAP_DATA sEmpty
			sVehicleSwap = sEmpty
			sVehicleSwap.ptfxID = ptfxTemp //Need to keep this around a little longer...
			sVehicleSwap.iGhostTime = iTimerLength
			REINIT_NET_TIMER(sVehicleSwap.tdGhosted)
			sVehicleSwap.iPTFXTime = iPTFXTimerLength
			IF sVehicleSwap.iPTFXTime != -1
				REINIT_NET_TIMER(sVehicleSwap.tdPTFX)
				PRINTLN("[VehSwap] Starting timer for PTFX")
			ENDIF
			
			IF bBlockRotate
				SET_BIT(sVehicleSwap.iBitset, ciVEHSWAPBS_BLOCK_ROTATE_INPUT)
				PRINTLN("[VehSwap] Setting ciVEHSWAPBS_BLOCK_ROTATE_INPUT")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_SWAP(VEHICLE_SWAP_DATA &sVehicleSwap, MODEL_NAMES mnNewVeh)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH(sVehicleSwap.eVehicleSwapState)
			CASE eVehicleSwapState_IDLE
				VS_PROCESS_IDLE(sVehicleSwap)
			BREAK
			CASE eVehicleSwapState_INITIALISE
				VS_PROCESS_INITIALISE(sVehicleSwap, mnNewVeh)
			BREAK
			CASE eVehicleSwapState_CREATE
				VS_PROCESS_CREATE(sVehicleSwap, mnNewVeh)
			BREAK
			CASE eVehicleSwapState_APPLY_FORCES
				VS_PROCESS_APPLY_FORCES(sVehicleSwap, mnNewVeh)
			BREAK
			CASE eVehicleSwapState_CLEANUP
				VS_PROCESS_CLEANUP(sVehicleSwap, mnNewVeh)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

DEBUGONLY FUNC STRING GET_VEHICLE_WARP_STATE_STRING(VEHICLE_WARP_STATE eState)
	SWITCH(eState)
		CASE eVehicleWarpState_IDLE		RETURN "eVehicleWarpState_IDLE"
		CASE eVehicleWarpState_WARP		RETURN "eVehicleWarpState_WARP"
		CASE eVehicleWarpState_GHOST	RETURN "eVehicleWarpState_GHOST"
	ENDSWITCH
	RETURN "Invalid State"
ENDFUNC

PROC SET_VEHICLE_WARP_STATE(VEHICLE_WARP_STATE &eVehicleWarpState, VEHICLE_WARP_STATE eNewState)
	PRINTLN("[JS][VehWarp] - SET_VEHICLE_WARP_STATE - Old State: ",GET_VEHICLE_WARP_STATE_STRING(eVehicleWarpState)," New State: ",GET_VEHICLE_WARP_STATE_STRING(eNewState))
	eVehicleWarpState = eNewState
ENDPROC

FUNC FLOAT GET_RACE_MAX_GRID_SIZE(INT iRaceType)
	IF IS_AIR_RACE(iRaceType)
		RETURN 47.0
	ELIF IS_BOAT_RACE(iRaceType)
		RETURN 27.45
	ELSE
		RETURN 22.55
	ENDIF
ENDFUNC

PROC GET_RACE_CHECKPOINT_INFO(INT index, VECTOR & position, BOOL bSecondary = FALSE)
	IF index < (GET_FMMC_MAX_NUM_CHECKPOINTS() - 1)
		IF index > -1
			IF bSecondary
				position = g_FMMC_STRUCT.sPlacedCheckpoint[index].vCheckPointSecondary
			ELSE
				position = g_FMMC_STRUCT.sPlacedCheckpoint[index].vCheckPoint
			ENDIF
		#IF IS_DEBUG_BUILD	
		ELSE
			SCRIPT_ASSERT("GET_RACE_CHECKPOINT_INFO - index < 0")
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD	
	ELSE
		SCRIPT_ASSERT("GET_RACE_CHECKPOINT_INFO - index >= GET_FMMC_MAX_NUM_CHECKPOINTS()")
	#ENDIF
	ENDIF
ENDPROC

PROC GET_RACE_CHECKPOINT_RESPAWNS(INT iIndex, VECTOR & respawnpos[], INT iRaceType, BOOL bSecondry = FALSE, BOOL bOffset = FALSE)
	INT i
	VECTOR vec
	VECTOR vCheckPoint
	VECTOR vRespawnPos[FMMC_MAX_NUM_CHECKPOINT_RESPAWN]
	
	IF bSecondry
		vCheckPoint = g_FMMC_STRUCT.sPlacedCheckpoint[iIndex].vCheckPointSecondary
		REPEAT FMMC_MAX_NUM_CHECKPOINT_RESPAWN i
			vRespawnPos[i] = g_FMMC_STRUCT.sPlacedCheckpoint[iIndex].sCheckpointRespawnSecondary.vRespawnPos[i]
		ENDREPEAT
	ELSE
		vCheckPoint = g_FMMC_STRUCT.sPlacedCheckpoint[iIndex].vCheckPoint
		REPEAT FMMC_MAX_NUM_CHECKPOINT_RESPAWN i
			vRespawnPos[i] = g_FMMC_STRUCT.sPlacedCheckpoint[iIndex].sCheckpointRespawn.vRespawnPos[i]
		ENDREPEAT
	ENDIF
	
	IF iIndex < (GET_FMMC_MAX_NUM_CHECKPOINTS() - 1)
		IF IS_AIR_RACE(iRaceType)
			// if it's an air race we only want to use points directly behind.
			IF VMAG(vRespawnPos[2]) > 0.0
				
				vec = vCheckPoint - vRespawnPos[2]
				vec /= 3.0
				
				vRespawnPos[1] = vRespawnPos[2] + vec
				vRespawnPos[0] = vRespawnPos[1] + vec
				
				
				NET_PRINT("GET_RACE_CHECKPOINT_RESPAWNS - air race, re aligning points behind main checkpoint") NET_NL()
				
			ELSE
				
				REPEAT FMMC_MAX_NUM_CHECKPOINT_RESPAWN i
					vRespawnPos[i] = <<0.0, 0.0, 0.0>>
				ENDREPEAT	
				
				NET_PRINT("GET_RACE_CHECKPOINT_RESPAWNS - air race, but behind point is not safe so clearing.") NET_NL()
				
			ENDIF
			
		ENDIF
		
		// check the secondary respawn is within range of the primary respawn
		IF NOT bOffset
			REPEAT FMMC_MAX_NUM_CHECKPOINT_RESPAWN i
				IF VMAG(vRespawnPos[i]) > 0.0
					IF (VDIST(vRespawnPos[i], vCheckPoint) > (GET_RACE_MAX_GRID_SIZE(iRaceType)+5.0))
						NET_PRINT("GET_RACE_CHECKPOINT_RESPAWNS - respawn point too far from checkpoint!: ") NET_NL()
						NET_PRINT("   g_FMMC_STRUCT.sPlacedCheckpoint[") NET_PRINT_INT(iIndex) NET_PRINT("].sCheckpointRespawn.vRespawnPos[") NET_PRINT_INT(i) NET_PRINT("] = ") NET_PRINT_VECTOR(vRespawnPos[i])  NET_NL()
						NET_PRINT("   g_FMMC_STRUCT.sPlacedCheckpoint[") NET_PRINT_INT(iIndex) NET_PRINT("].vCheckPoint = ") NET_PRINT_VECTOR(vCheckPoint)  NET_NL()
						SCRIPT_ASSERT("Secondary respawn point is too far from checkpoint! Race may need to have it's checkpoints resaved.")
						vRespawnPos[i] = <<0.0, 0.0, 0.0>>
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		REPEAT FMMC_MAX_NUM_CHECKPOINT_RESPAWN i
			respawnpos[i] = vRespawnPos[i]
			NET_PRINT("GET_RACE_CHECKPOINT_RESPAWNS - g_FMMC_STRUCT.sCheckpointRespawn[")
			NET_PRINT_INT(iIndex)
			NET_PRINT("].vRespawnPos[")
			NET_PRINT_INT(i)
			NET_PRINT("] = ")
			NET_PRINT_VECTOR(vRespawnPos[i])
			NET_NL()
		ENDREPEAT
		
	#IF IS_DEBUG_BUILD	
	ELSE
		SCRIPT_ASSERT("GET_RACE_CHECKPOINT_RESPAWNS - iIndex >= GET_FMMC_MAX_NUM_CHECKPOINTS()")
	#ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Calculates the heading of the current checkpoint based on its position and the next
/// PARAMS:
///    bigRaceVarsPassed.m_raceData - Race Data struct
///    currentCheckpoint - the checkpoint we want the heading for
FUNC FLOAT GET_CHECKPOINT_HEADING(INT iNumberCheckpoints, INT iCurrentCheckPoint, BOOL debugOutput = FALSE, BOOL bSecondary = FALSE )
	
	VECTOR checkPointCurrent, checkPointNext
		
	INT iNextCheckpoint = iCurrentCheckPoint + 1
	IF iNextCheckpoint = iNumberCheckpoints
		iNextCheckpoint = 0
	ENDIF
	IF bSecondary
		GET_RACE_CHECKPOINT_INFO(iCurrentCheckPoint, checkPointCurrent, TRUE)
		GET_RACE_CHECKPOINT_INFO(iNextCheckpoint, checkPointNext, TRUE)
		IF IS_VECTOR_ZERO(checkPointNext)
			GET_RACE_CHECKPOINT_INFO(iNextCheckpoint, checkPointNext)
		ENDIF
	ELSE
		GET_RACE_CHECKPOINT_INFO(iCurrentCheckPoint, checkPointCurrent)
		GET_RACE_CHECKPOINT_INFO(iNextCheckpoint, checkPointNext)
	ENDIF
	
	IF debugOutput
		#IF IS_DEBUG_BUILD
		NET_PRINT_STRING_INT("iCurrentCheckPoint = ", iCurrentCheckPoint )	NET_NL()
		NET_PRINT_STRING_INT("iNextCheckpoint = ", iNextCheckpoint )	NET_NL()
		#ENDIF
	ENDIF
		
	VECTOR unitVector = checkPointNext - checkPointCurrent
	
	IF debugOutput
		#IF IS_DEBUG_BUILD
		NET_PRINT_STRING_VECTOR("checkPointNext = ", checkPointNext )	NET_NL()
		NET_PRINT_STRING_VECTOR("checkPointCurrent = ", checkPointCurrent )	NET_NL()
		NET_PRINT_STRING_VECTOR("unitVector = ", unitVector )	NET_NL()
		#ENDIF
	ENDIF
	
	unitVector = NORMALISE_VECTOR( unitVector )
	
	IF debugOutput
		#IF IS_DEBUG_BUILD
		NET_PRINT_STRING_VECTOR("normalized unitVector = ", unitVector )	NET_NL()
		NET_PRINT_STRING_FLOAT("heading = ", GET_HEADING_FROM_VECTOR_2D( unitVector.x, unitVector.y ) )	NET_NL()
		#ENDIF
	ENDIF
	
	RETURN GET_HEADING_FROM_VECTOR_2D( unitVector.x, unitVector.y )
	
ENDFUNC

PROC UPDATE_TEAM_WANTED_LEVELS()
	INT i
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		IF g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedChange[0] < 1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedChange[0] = 1
		ENDIF
	ENDFOR
ENDPROC

PROC UPDATE_BULLETPROOF_TYRES_OPTIONS(structFMMC_MENU_ITEMS &sFMMCMenu)
	BOOL bBulletProofsTyres
	INT iveh
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles iveh
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn != DUMMY_MODEL_FOR_SCRIPT
			bBulletProofsTyres = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.iVehicleTireBitSet ,iveh)
		 	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_BULLETPROOF_TYRES)
			AND bBulletProofsTyres
		 		SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_BULLETPROOF_TYRES)
		 	ENDIF
		ENDIF
	ENDREPEAT
	g_FMMC_STRUCT_ENTITIES.iVehicleTireBitSet = 0
	sFMMCMenu.iTyreType = 0
ENDPROC

FUNC BOOL GET_SAFE_WARP_DATA_FROM_CHECKPOINT(VECTOR &vPosition, VECTOR &vRotation, INT iCheckpoint, BOOL bSecondary, VEHICLE_INDEX viPlayerVeh, INT iRaceType, INT iNumberOfCheckPoints, VECTOR vInitialPos, FLOAT fInitialHeading)
	
	IF iCheckpoint < 0
		vPosition = vInitialPos
		vRotation.y = fInitialHeading
	ELSE
		IF bSecondary 
		AND IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].vCheckPointSecondary)
			bSecondary = FALSE
		ENDIF
		IF IS_VECTOR_ZERO(vPosition)
			VECTOR vCheckpointPos
			VECTOR vCheckPointRespawns[FMMC_MAX_NUM_CHECKPOINT_RESPAWN]

			GET_RACE_CHECKPOINT_INFO(iCheckpoint, vCheckpointPos, bSecondary)
			GET_RACE_CHECKPOINT_RESPAWNS(iCheckpoint, vCheckPointRespawns, iRaceType, bSecondary, TRUE)
			IF IS_VECTOR_ZERO(vRotation)
				IF g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckPointRespawn != 0.0
					vRotation.z =  g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckPointRespawn
				ELSE
					vRotation.z = GET_CHECKPOINT_HEADING(iNumberOfCheckPoints, iCheckpoint, DEFAULT, bSecondary)
				ENDIF
				IF IS_CHECKPOINT_CUSTOM_RESPAWN_ROTATION(iCheckpoint, bSecondary)
					vRotation.y = g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].vCheckpointArrowDirectionOverride.x
					vRotation.z = g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].vCheckpointArrowDirectionOverride.z
				ELIF IS_CHECKPOINT_NON_BILLBOARD(iCheckpoint, bSecondary)
					vRotation.y = GET_CHECKPOINT_PITCH_FOR_RESPAWN(iCheckpoint, bSecondary)
				ENDIF
			ENDIF
			INT iRspPoint
			FOR iRspPoint = 0 TO FMMC_MAX_NUM_CHECKPOINT_RESPAWN - 1
			 	IF NOT IS_VECTOR_ZERO(vCheckPointRespawns[iRspPoint])
					vPosition = vCheckPointRespawns[iRspPoint]
					IF DOES_ENTITY_EXIST(viPlayerVeh)
						IF GET_CAN_VEHICLE_BE_PLACED_HERE(viPlayerVeh, vPosition, vRotation)
							BREAKLOOP
						ENDIF
					ELSE
						BREAKLOOP
					ENDIF
				ENDIF
			ENDFOR
			IF IS_VECTOR_ZERO(vPosition)
				vPosition = vCheckpointPos
				IF DOES_ENTITY_EXIST(viPlayerVeh)
					IF NOT GET_CAN_VEHICLE_BE_PLACED_HERE(viPlayerVeh, vPosition, vRotation)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC VW_PROCESS_WARP(VEHICLE_WARP_DATA &sVehicleWarp)
	PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
	//TRIGGER_SCREENBLUR_FADE_IN(1)
	ANIMPOSTFX_PLAY("MP_WarpCheckpoint", 2000, FALSE)
	#IF IS_DEBUG_BUILD
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_NoGhostTransform")
	#ENDIF
		IF NOT IS_BIT_SET(sVehicleWarp.iBitset, ciVEHWARPBS_NON_CONTACT)
			SET_LOCAL_PLAYER_AS_GHOST(TRUE, TRUE)
		ELSE
			SET_LOCAL_PLAYER_AS_GHOST(TRUE)
		ENDIF
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF NOT IS_ENTITY_DEAD(viPlayerVeh)
		
			IF NETWORK_GET_ENTITY_IS_NETWORKED(viPlayerVeh)
				PLAY_SOUND_FROM_ENTITY(-1, "Vehicle_Warp", viPlayerVeh, "DLC_Air_Race_Sounds_Player", TRUE, 250)
			ELSE
				//PLAY_SOUND_FROM_COORD(-1, "Vehicle_Warp", GET_ENTITY_COORDS(viPlayerVeh), "DLC_Air_Race_Sounds_Player", TRUE, 250)
				PLAY_SOUND_FROM_ENTITY(-1, "Vehicle_Warp", viPlayerVeh, "DLC_Air_Race_Sounds_Player", FALSE, 250)
			ENDIF
			
			MODEL_NAMES mn = GET_ENTITY_MODEL(viPlayerVeh)
			BOOL bPlane = IS_THIS_MODEL_A_PLANE(mn)
			IF NETWORK_IS_GAME_IN_PROGRESS()
				FLOAT fR, fG, fB
				GET_PLAYER_VEHICLE_COLOUR_AS_FLOATS(fR, fG, fB, viPlayerVeh)
				FLOAT fScale = 1.0
				IF IS_THIS_A_LARGE_VEHICLE_MODEL(mn)
					fScale = 2.0
				ENDIF
				USE_PARTICLE_FX_ASSET("scr_as_trans")
				sVehicleWarp.ptfxID = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_as_trans_smoke", LocalPlayerPed, <<0,0,0>>, <<0,0,0>>, fScale, DEFAULT, DEFAULT, DEFAULT, fR, fG, fB)
				PRINTLN("[JS][VehWarp] - VW_PROCESS_WARP - Triggering PTFX  (3)")
				IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleWarp.ptfxID) 
					SET_TRANSFORM_PTFX_EVOLUTIONS(sVehicleWarp.ptfxID, mn)
				ENDIF
			ENDIF
			sVehicleWarp.sPrevData.vTorque = GET_ENTITY_ROTATION_VELOCITY(viPlayerVeh)
			sVehicleWarp.sPrevData.vVelocity = GET_ENTITY_VELOCITY(viPlayerVeh)
			
			SET_ENTITY_COORDS_NO_OFFSET(viPlayerVeh, sVehicleWarp.sPrevData.vPosition) 
			SET_ENTITY_ROTATION(viPlayerVeh, sVehicleWarp.sPrevData.vRotation)
			
			IF bPlane
				SET_VEHICLE_FORWARD_SPEED(viPlayerVeh, 30)		
			ELSE
				SET_VEHICLE_FORWARD_SPEED(viPlayerVeh, 1)		
			ENDIF
			
			FLOAT fMagnitude = SQRT((sVehicleWarp.sPrevData.vVelocity.x * sVehicleWarp.sPrevData.vVelocity.x) + (sVehicleWarp.sPrevData.vVelocity.y * sVehicleWarp.sPrevData.vVelocity.y) + (sVehicleWarp.sPrevData.vVelocity.z * sVehicleWarp.sPrevData.vVelocity.z))
			VECTOR vRotatedVelocity = GET_ENTITY_FORWARD_VECTOR(viPlayerVeh)
			
			vRotatedVelocity = NORMALISE_VECTOR(vRotatedVelocity)
			vRotatedVelocity.x = vRotatedVelocity.x * fMagnitude
			vRotatedVelocity.y = vRotatedVelocity.y * fMagnitude
			vRotatedVelocity.z = vRotatedVelocity.z * fMagnitude
			
			SET_ENTITY_VELOCITY(viPlayerVeh, vRotatedVelocity)
			
			CONSTRAIN_TRANSFORMATION_TORQUE(sVehicleWarp.sPrevData.vTorque)
			APPLY_FORCE_TO_ENTITY(viPlayerVeh, APPLY_TYPE_ANGULAR_IMPULSE, sVehicleWarp.sPrevData.vTorque, <<0,0,0>>, 0 , TRUE, TRUE, TRUE)
			SET_VEHICLE_WARP_STATE(sVehicleWarp.eVehicleWarpState, eVehicleWarpState_GHOST)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
			USE_PARTICLE_FX_ASSET("scr_as_trans")
			sVehicleWarp.ptfxID = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_as_trans_smoke", LocalPlayerPed, <<0,0,0>>, <<0,0,0>>, 1.0, DEFAULT, DEFAULT, DEFAULT, 0.2, 0.2, 0.2)	
			PRINTLN("[JS][VehWarp] - VW_PROCESS_WARP - Triggering PTFX  (4)")
			
			//Populate Forces
			IF GET_PED_PARACHUTE_STATE(LocalPlayerPed) = PPS_PARACHUTING
				ENTITY_INDEX ei = GET_ENTITY_ATTACHED_TO(LocalPlayerPed)
				IF DOES_ENTITY_EXIST(ei)
					PRINTLN("[JS][VehWarp] - VW_PROCESS_WARP - Grabbing Velocity from Parachute")
					sVehicleWarp.sPrevData.vVelocity = GET_ENTITY_VELOCITY(ei)
				ELSE
					sVehicleWarp.sPrevData.vVelocity = GET_ENTITY_VELOCITY(LocalPlayerPed)
				ENDIF
			ELSE
				sVehicleWarp.sPrevData.vVelocity = GET_ENTITY_VELOCITY(LocalPlayerPed)
			ENDIF
			
			//Position
			SET_ENTITY_COORDS_NO_OFFSET(LocalPlayerPed, sVehicleWarp.sPrevData.vPosition) 
			SET_ENTITY_ROTATION(LocalPlayerPed, sVehicleWarp.sPrevData.vRotation)
			
			//Forces
			SET_ENTITY_VELOCITY(LocalPlayerPed, sVehicleWarp.sPrevData.vVelocity)
			
			SET_VEHICLE_WARP_STATE(sVehicleWarp.eVehicleWarpState, eVehicleWarpState_GHOST)
		ENDIF
	ENDIF
ENDPROC

PROC VW_PROCESS_GHOST(VEHICLE_WARP_DATA &sVehicleWarp)
	PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleWarp.ptfxID) 
		MODEL_NAMES mn = DUMMY_MODEL_FOR_SCRIPT
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF NOT IS_ENTITY_DEAD(viPlayerVeh)
				mn = GET_ENTITY_MODEL(viPlayerVeh)
			ENDIF
		ENDIF
		SET_TRANSFORM_PTFX_EVOLUTIONS(sVehicleWarp.ptfxID, mn)
	ENDIF
	IF NOT HAS_NET_TIMER_STARTED(sVehicleWarp.tdGhosted)
		//TRIGGER_SCREENBLUR_FADE_OUT(1000)
		REINIT_NET_TIMER(sVehicleWarp.tdGhosted)
	ELIF HAS_NET_TIMER_EXPIRED(sVehicleWarp.tdGhosted, 1000)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleWarp.ptfxID) 
				STOP_PARTICLE_FX_LOOPED(sVehicleWarp.ptfxID, FALSE)
			ENDIF
		ENDIF
		IF HAS_NET_TIMER_EXPIRED(sVehicleWarp.tdGhosted, ciVEHICLE_WARP_GHOST)
			RESET_NET_TIMER(sVehicleWarp.tdGhosted)
			#IF IS_DEBUG_BUILD
			IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_NoGhostTransform")
			#ENDIF
				IF NOT IS_BIT_SET(sVehicleWarp.iBitset, ciVEHWARPBS_NON_CONTACT)
					SET_LOCAL_PLAYER_AS_GHOST(FALSE)
				ENDIF
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			VEHICLE_WARP_DATA sEmpty
			sVehicleWarp = sEmpty
			SET_VEHICLE_WARP_STATE(sVehicleWarp.eVehicleWarpState, eVehicleWarpState_IDLE)
		ENDIF
	ELSE
		IF NOT HAS_NET_TIMER_EXPIRED(sVehicleWarp.tdGhosted, 555)
		OR GET_FRAME_TIME() % 2 = 0
			//DRAW_DEBUG_TEXT_2D("Blocking steering now!", <<0.5, 0.5, 0.0>>)
			PRINTLN("VW_PROCESS_GHOST - Blocking steering now!")
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_WARP(VEHICLE_WARP_DATA &sVehicleWarp)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH(sVehicleWarp.eVehicleWarpState)
			CASE eVehicleWarpState_IDLE
				
			BREAK
			CASE eVehicleWarpState_WARP
				VW_PROCESS_WARP(sVehicleWarp)
			BREAK
			CASE eVehicleWarpState_GHOST
				VW_PROCESS_GHOST(sVehicleWarp)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC
