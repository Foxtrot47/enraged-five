USING "rage_builtins.sch" 
USING "globals.sch"
USING "net_include.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Model Loading
// ##### Description: Called from FMMC_Launcher to preload on Strand Missions
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//////////////////////////////      LOAD ALL PROP MODELS 		//////////////////////////////
FUNC BOOL REQUEST_LOAD_FMMC_PROP_MODELS( )//INT iStage = 0 )
	
	BOOL bLoaded = TRUE
	INT i

	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i
	
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
		//AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iStage = iStage 
		
			if g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = HEI_PROP_HEIST_APECRATE 
				request_model(A_C_Rhesus)
				request_anim_dict("missfbi5ig_30Monkeys")
				
				IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
					request_script_audio_bank("FBI_05_MONKEY_SCREAMS_01")
				ENDIF
				
				request_script_audio_bank("FBI_05_MONKEYS")
			endif
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speed"))
				IF NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speeda")))
				AND NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speedb")))
					PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_PROP_MODELS - Failing to load model for prop ",i,", model name ",g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
					bLoaded = FALSE
				ELSE
					PRINTLN("[LM] LOADED: REQUEST_LOAD_FMMC_PROP_MODELS: stt_Prop_Stunt_Tube_Speeda + stt_Prop_Stunt_Tube_Speedb")
				ENDIF
			ENDIF
			
			IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
				PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_PROP_MODELS - Failing to load model for prop ",i,", model name ",g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
				bLoaded = FALSE
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_barge_01"))
				IF NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_01")))
				AND NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_02")))
					PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_PROP_MODELS - Failing to load extra barge collision models for prop ",i,", model name ",g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
					bLoaded = FALSE
				ELSE
					PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_PROP_MODELS - LOADED: xm_prop_x17_Barge_Col_02 + xm_prop_x17_Barge_Col_01")
				ENDIF
			ENDIF
			
			if g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = HEI_PROP_HEIST_APECRATE 				
				if not has_model_loaded(A_C_Rhesus)
				or not has_anim_dict_loaded("missfbi5ig_30Monkeys")				
				or not request_script_audio_bank("FBI_05_MONKEYS")
					PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_PROP_MODELS - Failing to load ape crate prop ",i)
					bLoaded = FALSE
				endif  
				
				IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
					IF NOT request_script_audio_bank("FBI_05_MONKEY_SCREAMS_01") 
						PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_PROP_MODELS - Failing to load ape crate prop ",i)
						bLoaded = FALSE
					ENDIF	
				ENDIF				
			endif
			
		ELSE
			//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedObject[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))PRINTNL()
		ENDIF
	ENDREPEAT
	
	return bLoaded
	
ENDFUNC

//////////////////////////////      LOAD ALL MODELS 		//////////////////////////////
FUNC BOOL REQUEST_LOAD_FMMC_MODELS_STAGGERED( INT iNumTimesCalledThisFunction )
	INT i
	INT iRequestCount
	INT iRequestThisFrame
	INT iStartRequestValueThisCall = iNumTimesCalledThisFunction * PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciMAX_PER_FRAME_REQUESTS_AIRLOCK, ciMAX_PER_FRAME_REQUESTS)
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
			IF iRequestCount = iStartRequestValueThisCall
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] - REQUEST_LOAD_FMMC_MODELS_STAGGERED - requesting model for ped : ", i)
				iStartRequestValueThisCall++
				REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn)
				IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciMAX_PER_FRAME_REQUESTS_AIRLOCK, ciMAX_PER_FRAME_REQUESTS)
					RETURN FALSE
				ELSE
					iRequestThisFrame++
				ENDIF
			ENDIF
			iRequestCount++
		ENDIF
	ENDREPEAT	

	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			IF iRequestCount = iStartRequestValueThisCall
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] - REQUEST_LOAD_FMMC_MODELS_STAGGERED - requesting model for veh : ", i)
				iStartRequestValueThisCall++
				REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
				IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciMAX_PER_FRAME_REQUESTS_AIRLOCK, ciMAX_PER_FRAME_REQUESTS)
					RETURN FALSE
				ELSE
					iRequestThisFrame++
				ENDIF
			ENDIF
			iRequestCount++
		ENDIF
	ENDREPEAT	
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn != DUMMY_MODEL_FOR_SCRIPT
			IF iRequestCount = iStartRequestValueThisCall
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] - REQUEST_LOAD_FMMC_MODELS_STAGGERED - requesting model for obj : ", i)
				iStartRequestValueThisCall++
				REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
				IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciMAX_PER_FRAME_REQUESTS_AIRLOCK, ciMAX_PER_FRAME_REQUESTS)
					RETURN FALSE
				ELSE
					iRequestThisFrame++
				ENDIF
			ENDIF
			iRequestCount++
		ENDIF
	ENDREPEAT
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			IF iRequestCount = iStartRequestValueThisCall
				iStartRequestValueThisCall++
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] - REQUEST_LOAD_FMMC_MODELS_STAGGERED - requesting model for dyno : ", i)
				REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn)
				IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciMAX_PER_FRAME_REQUESTS_AIRLOCK, ciMAX_PER_FRAME_REQUESTS)
					RETURN FALSE
				ELSE
					iRequestThisFrame++
				ENDIF
			ENDIF
			iRequestCount++
		ENDIF
	ENDREPEAT
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model != DUMMY_MODEL_FOR_SCRIPT
			IF iRequestCount = iStartRequestValueThisCall
				iStartRequestValueThisCall++
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] - REQUEST_LOAD_FMMC_MODELS_STAGGERED - requesting model for Interactable : ", i)
				REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model)
				IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciMAX_PER_FRAME_REQUESTS_AIRLOCK, ciMAX_PER_FRAME_REQUESTS)
					RETURN FALSE
				ELSE
					iRequestThisFrame++
				ENDIF
			ENDIF
			iRequestCount++
		ENDIF
	ENDREPEAT
	
	RETURN TRUE 
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Cargo/Crates
// ##### Description: Spawning cargo in vehicles - used in freemode content
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_VEHICLE_CARGO_VALID_FOR_THIS_VEHICLE(INT iCargo, MODEL_NAMES mnVehModel)
	SWITCH iCargo
		CASE ciVEHICLE_CARGO_AMMO
		CASE ciVEHICLE_CARGO_AMMO_STACKED
		CASE ciVEHICLE_CARGO_AMMO_SIDE
		CASE ciVEHICLE_CARGO_DROP_CRATES
		CASE ciVEHICLE_CARGO_WEED
		CASE ciVEHICLE_CARGO_METAL_CRATES
		CASE ciVEHICLE_CARGO_DRUGS
		CASE ciVEHICLE_CARGO_STATUES
		CASE ciVEHICLE_CARGO_AVON_CRATE
			SWITCH mnVehModel
				CASE SPEEDO
				CASE gburrito
				CASE GRANGER
				CASE BARRACKS
				CASE BARRACKS3
				CASE BENSON
				CASE GBURRITO2
				CASE MULE
				CASE MULE3
				CASE BOXVILLE4
					RETURN TRUE
			ENDSWITCH
			
			IF mnVehModel = INT_TO_ENUM(MODEL_NAMES, HASH("MULE5"))
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciVEHICLE_CARGO_MINI_SERVER
			SWITCH mnVehModel
				CASE GBURRITO2
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE ciVEHICLE_CARGO_MINI_SERVER_2
			SWITCH mnVehModel
				CASE GBURRITO2
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE ciVEHICLE_CARGO_TRANSPONDER
			SWITCH mnVehModel
				CASE BOXVILLE4
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE ciVEHICLE_CARGO_GREEN_CRATE
			SWITCH mnVehModel
				CASE SPEEDO
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE ciVEHICLE_CARGO_EMP
			SWITCH mnVehModel
				CASE INSURGENT
				CASE INSURGENT2
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE ciVEHICLE_CARGO_AIR_CARGO
			SWITCH mnVehModel
				CASE MULE
				CASE MULE3
					RETURN TRUE
			ENDSWITCH
			
			IF mnVehModel = INT_TO_ENUM(MODEL_NAMES, HASH("MULE5"))
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciVEHICLE_CARGO_WEED_SUPPLIES
			SWITCH mnVehModel
				CASE MULE
				CASE MULE3
					RETURN TRUE
			ENDSWITCH
			
			IF mnVehModel = INT_TO_ENUM(MODEL_NAMES, HASH("MULE5"))
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciVEHICLE_CARGO_WOOD_CRATE
			SWITCH mnVehModel
				CASE YOUGA2
				CASE YOUGA4
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE ciVEHICLE_CARGO_GOLF_CLUBS
			SWITCH mnVehModel
				CASE CADDY
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE ciVEHICLE_CARGO_SERVER_BOMB
			SWITCH mnVehModel
				CASE YOUGA3
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE ciVEHICLE_CARGO_TECH_CRATE
			SWITCH mnVehModel
				CASE SPEEDO 
				CASE PONY
				CASE BURRITO
				CASE RUMPO
				CASE BOXVILLE2
					RETURN TRUE
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	IF iCargo = ciVEHICLE_CARGO_OFF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_VEHICLE_CARGO_MODEL_FROM_INT(INT iVehicleCargo, MODEL_NAMES mnVehModel, BOOL bDrivable = TRUE)
	DEBUG_PRINTCALLSTACK()
	
	SWITCH iVehicleCargo
		CASE ciVEHICLE_CARGO_AMMO
			IF mnVehModel = GRANGER
			OR mnVehModel = BARRACKS
			OR mnVehModel = BARRACKS3
				RETURN PROP_BOX_AMMO03A_SET2
			ELSE
				RETURN PROP_BOX_AMMO03A_SET
			ENDIF
		BREAK
		CASE ciVEHICLE_CARGO_AMMO_STACKED
			RETURN hei_Prop_Hei_Ammo_Pile
		BREAK	
		CASE ciVEHICLE_CARGO_AMMO_SIDE
			RETURN hei_prop_hei_ammo_pile_02
		BREAK	
		CASE ciVEHICLE_CARGO_DROP_CRATES
			IF mnVehModel = GRANGER
			OR mnVehModel = BARRACKS
			OR mnVehModel = BARRACKS3
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("hei_prop_crate_stack_01")) //hei_prop_crate_stack_01
			ELSE
				RETURN PROP_DROP_CRATE_01_SET
			ENDIF
		BREAK	
		CASE ciVEHICLE_CARGO_WEED
			RETURN hei_Prop_Heist_Weed_Pallet_02
		BREAK	
		CASE ciVEHICLE_CARGO_METAL_CRATES
			IF mnVehModel = GRANGER
				RETURN DUMMY_MODEL_FOR_SCRIPT
			ELSE
				RETURN PROP_MB_CRATE_01A_SET
			ENDIF
		BREAK	
		CASE ciVEHICLE_CARGO_DRUGS
			RETURN hei_Prop_Heist_Tub_Truck
			
		CASE ciVEHICLE_CARGO_STATUES
			RETURN HEI_PROP_DRUG_STATUE_STACK
		BREAK
		
		CASE ciVEHICLE_CARGO_MINI_SERVER
			IF bDrivable
				RETURN hei_Prop_MINI_Sever_02
			ELSE
				RETURN HEI_PROP_MINI_SEVER_BROKEN
			ENDIF
		BREAK
		
		CASE ciVEHICLE_CARGO_MINI_SERVER_2
			RETURN hei_Prop_MINI_Sever_03
		BREAK
		
		CASE ciVEHICLE_CARGO_TRANSPONDER
			RETURN hei_Prop_Heist_Transponder
		BREAK
		
		CASE ciVEHICLE_CARGO_GREEN_CRATE
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_vancrate_01a"))
		BREAK
		
		CASE ciVEHICLE_CARGO_EMP
			RETURN HEI_PROP_HEIST_EMP
		BREAK
		
		CASE ciVEHICLE_CARGO_AIR_CARGO
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_AIR_CARGO_04a"))
		BREAK
		
		CASE ciVEHICLE_CARGO_WEED_SUPPLIES
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Pallet_01a"))
		
		CASE ciVEHICLE_CARGO_WOOD_CRATE
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_Box_Wood_01a"))
		
		CASE ciVEHICLE_CARGO_GOLF_CLUBS
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Golf_Bag_01b"))
			
		CASE ciVEHICLE_CARGO_AVON_CRATE
			RETURN REH_PROP_REH_BOX_WOOD01A
			
		CASE ciVEHICLE_CARGO_SERVER_BOMB
			RETURN REH_PROP_REH_BOMB_TECH_01A
		
		CASE ciVEHICLE_CARGO_TECH_CRATE
			RETURN REH_PROP_REH_BOX_METAL_01A
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
	
ENDFUNC

FUNC STRING GET_VEHICLE_CARGO_BONE_FROM_INT(INT iVehicleCargo, MODEL_NAMES mnVehModel)
	
	SWITCH iVehicleCargo
		CASE ciVEHICLE_CARGO_AMMO
		CASE ciVEHICLE_CARGO_AMMO_STACKED
		CASE ciVEHICLE_CARGO_AMMO_SIDE
		CASE ciVEHICLE_CARGO_WEED
		CASE ciVEHICLE_CARGO_STATUES
		CASE ciVEHICLE_CARGO_AIR_CARGO
		CASE ciVEHICLE_CARGO_WEED_SUPPLIES
		CASE ciVEHICLE_CARGO_WOOD_CRATE
		CASE ciVEHICLE_CARGO_AVON_CRATE
			IF mnVehModel = GRANGER
				RETURN "Prop_Box_Ammo03A_2"
			ELIF mnVehModel = BARRACKS
			OR mnVehModel = BARRACKS3
			OR mnVehModel = MULE
			OR mnVehModel = MULE3
			OR mnVehModel = INT_TO_ENUM(MODEL_NAMES, HASH("MULE5"))
			OR mnVehModel = YOUGA2
			OR mnVehModel = YOUGA4
				RETURN "chassis_dummy"
			ELIF mnVehModel = BENSON
				RETURN "exhaust"
			ELSE
				RETURN "Prop_Box_Ammo03A"
			ENDIF
		BREAK
		CASE ciVEHICLE_CARGO_DROP_CRATES
		CASE ciVEHICLE_CARGO_METAL_CRATES
			IF mnVehModel = GRANGER
				RETURN "Prop_Drop_Crate_01_2"
			ELIF mnVehModel = BARRACKS
			OR mnVehModel = BARRACKS3
			OR mnVehModel = MULE
			OR mnVehModel = MULE3
			OR mnVehModel = INT_TO_ENUM(MODEL_NAMES, HASH("MULE5"))
				RETURN "chassis_dummy"
			ELIF mnVehModel = BENSON
				RETURN "exhaust"
			ELSE
				RETURN "Prop_Drop_Crate_01"
			ENDIF
		BREAK
		CASE ciVEHICLE_CARGO_DRUGS
		CASE ciVEHICLE_CARGO_TRANSPONDER
		CASE ciVEHICLE_CARGO_EMP
			RETURN "chassis_dummy"
		BREAK
		
		CASE ciVEHICLE_CARGO_MINI_SERVER
		CASE ciVEHICLE_CARGO_MINI_SERVER_2
			RETURN "Prop_MB_crate_01A"
		BREAK
		
		CASE ciVEHICLE_CARGO_GREEN_CRATE
			RETURN "chassis_dummy"
		BREAK
		
		CASE ciVEHICLE_CARGO_GOLF_CLUBS
			RETURN "chassis_dummy"
		BREAK
		
		CASE ciVEHICLE_CARGO_SERVER_BOMB
			RETURN "chassis_dummy"
		BREAK
		
		CASE ciVEHICLE_CARGO_TECH_CRATE
			RETURN "chassis_dummy"
		BREAK
		
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

FUNC MODEL_NAMES GET_VEHICLE_CRATE_MODEL(INT iDoorBit, INT iDoorBit2, INT iDoorBit7, VEHICLE_INDEX veh)
	
	IF IS_BIT_SET(iDoorBit, ciFMMC_VEHICLE_EXTRA_AMMO_CRATE)
		IF GET_ENTITY_MODEL(veh) = GRANGER
		OR GET_ENTITY_MODEL(veh) = BARRACKS
		OR GET_ENTITY_MODEL(veh) = BARRACKS3
			RETURN PROP_BOX_AMMO03A_SET2
		ELSE
			RETURN PROP_BOX_AMMO03A_SET
		ENDIF
		
	ELIF IS_BIT_SET(iDoorBit, ciFMMC_VEHICLE_EXTRA_DROP_CRATE)
		IF GET_ENTITY_MODEL(veh) = GRANGER
		OR GET_ENTITY_MODEL(veh) = BARRACKS
		OR GET_ENTITY_MODEL(veh) = BARRACKS3
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("hei_prop_crate_stack_01")) //hei_prop_crate_stack_01
		ELSE
			RETURN PROP_DROP_CRATE_01_SET
		ENDIF
		
	ELIF IS_BIT_SET(iDoorBit, ciFMMC_VEHICLE_EXTRA_METAL_CRATE)
		IF GET_ENTITY_MODEL(veh) = GRANGER
			RETURN DUMMY_MODEL_FOR_SCRIPT
		ELSE
			RETURN PROP_MB_CRATE_01A_SET
		ENDIF
		
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_MINI_SERVER)
		IF IS_VEHICLE_DRIVEABLE(veh)
			RETURN hei_Prop_MINI_Sever_02
		ELSE
			RETURN HEI_PROP_MINI_SEVER_BROKEN
		ENDIF
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_MINI_SERVER_2)
		RETURN hei_Prop_MINI_Sever_03
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_DRUGS)
		RETURN hei_Prop_Heist_Tub_Truck
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_AMMO_CRATES_2)
		RETURN hei_Prop_Hei_Ammo_Pile
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_AMMO_CRATES_2_SIDE_BY_SIDE)
		RETURN hei_prop_hei_ammo_pile_02
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_WEED_PALLET)
		RETURN hei_Prop_Heist_Weed_Pallet_02
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_STATUE_BOX)
		RETURN HEI_PROP_DRUG_STATUE_STACK
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_TRANSPONDER)
		RETURN hei_Prop_Heist_Transponder
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_EMP)
		RETURN HEI_PROP_HEIST_EMP
	ELIF IS_BIT_SET(iDoorBit7, ciFMMC_VEHICLE7_EXTRA_GREEN_CRATE)
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_vancrate_01a"))
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
	
ENDFUNC

FUNC MODEL_NAMES GET_VEHICLE_CRATE_MODEL_RC(INT iDoorBit, INT iDoorBit2, INT iDoorBit7, MODEL_NAMES VehModel)

	IF IS_BIT_SET(iDoorBit, ciFMMC_VEHICLE_EXTRA_AMMO_CRATE)
		IF VehModel = GRANGER
		OR VehModel = BARRACKS
		OR VehModel = BARRACKS3
			RETURN PROP_BOX_AMMO03A_SET2
		ELSE
			RETURN PROP_BOX_AMMO03A_SET
		ENDIF
		
	ELIF IS_BIT_SET(iDoorBit, ciFMMC_VEHICLE_EXTRA_DROP_CRATE)
		IF VehModel= GRANGER
		OR VehModel = BARRACKS
		OR VehModel = BARRACKS3
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("hei_prop_crate_stack_01")) //hei_prop_crate_stack_01
		ELSE
			RETURN PROP_DROP_CRATE_01_SET
		ENDIF
		
	ELIF IS_BIT_SET(iDoorBit, ciFMMC_VEHICLE_EXTRA_METAL_CRATE)
		IF VehModel = GRANGER
			RETURN DUMMY_MODEL_FOR_SCRIPT
		ELSE
			RETURN PROP_MB_CRATE_01A_SET
		ENDIF
		
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_MINI_SERVER)
		RETURN hei_Prop_MINI_Sever_02
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_MINI_SERVER_2)
		RETURN hei_Prop_MINI_Sever_03
		
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_DRUGS)
		RETURN hei_Prop_Heist_Tub_Truck
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_AMMO_CRATES_2)
		RETURN hei_Prop_Hei_Ammo_Pile
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_AMMO_CRATES_2_SIDE_BY_SIDE)
		RETURN hei_prop_hei_ammo_pile_02
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_WEED_PALLET)
		RETURN hei_Prop_Heist_Weed_Pallet_02
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_STATUE_BOX)
		RETURN HEI_PROP_DRUG_STATUE_STACK
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_TRANSPONDER)
		RETURN hei_Prop_Heist_Transponder
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_EMP)
		RETURN HEI_PROP_HEIST_EMP
	ELIF IS_BIT_SET(iDoorBit7, ciFMMC_VEHICLE7_EXTRA_GREEN_CRATE)
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_vancrate_01a"))
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
	
ENDFUNC

FUNC STRING GET_VEHICLE_CRATE_BONE(INT iDoorBit, INT iDoorBit2, INT iDoorBit7, VEHICLE_INDEX veh)

	IF IS_BIT_SET(iDoorBit, ciFMMC_VEHICLE_EXTRA_AMMO_CRATE)
	OR IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_AMMO_CRATES_2)
	OR IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_AMMO_CRATES_2_SIDE_BY_SIDE)
	OR IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_WEED_PALLET)
	OR IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_STATUE_BOX)
		IF GET_ENTITY_MODEL(veh) = GRANGER
			RETURN "Prop_Box_Ammo03A_2"
		ELIF GET_ENTITY_MODEL(veh) = BARRACKS
		OR GET_ENTITY_MODEL(veh) = BARRACKS3
			RETURN "chassis_dummy"
		ELIF GET_ENTITY_MODEL(veh) = BENSON
			RETURN "exhaust"
		ELIF GET_ENTITY_MODEL(veh) = MULE
		OR GET_ENTITY_MODEL(veh) = MULE3
			RETURN "chassis_dummy"
		ELSE
			RETURN "Prop_Box_Ammo03A"
		ENDIF
		
	ELIF IS_BIT_SET(iDoorBit, ciFMMC_VEHICLE_EXTRA_DROP_CRATE)
		IF GET_ENTITY_MODEL(veh) = GRANGER
			RETURN "Prop_Drop_Crate_01_2"
		ELIF GET_ENTITY_MODEL(veh) = BARRACKS
		OR GET_ENTITY_MODEL(veh) = BARRACKS3
			RETURN "chassis_dummy"
		ELIF GET_ENTITY_MODEL(veh) = BENSON
			RETURN "exhaust"
		ELIF GET_ENTITY_MODEL(veh) = MULE
		OR GET_ENTITY_MODEL(veh) = MULE3
			RETURN "chassis_dummy"
		ELSE
			RETURN "Prop_Drop_Crate_01"
		ENDIF
		
	ELIF IS_BIT_SET(iDoorBit, ciFMMC_VEHICLE_EXTRA_METAL_CRATE)
		IF GET_ENTITY_MODEL(veh) = GRANGER
			RETURN "Prop_Drop_Crate_01_2"
		ELIF GET_ENTITY_MODEL(veh) = BARRACKS
		OR GET_ENTITY_MODEL(veh) = BARRACKS3
			RETURN "chassis_dummy"
		ELIF GET_ENTITY_MODEL(veh) = BENSON
			RETURN "exhaust"
		ELIF GET_ENTITY_MODEL(veh) = MULE
		OR GET_ENTITY_MODEL(veh) = MULE3
		OR GET_ENTITY_MODEL(veh) = INT_TO_ENUM(MODEL_NAMES, HASH("MULE5"))
			RETURN "chassis_dummy"
		ELSE
			RETURN "Prop_MB_crate_01A"
		ENDIF
		
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_MINI_SERVER)
	OR IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_MINI_SERVER_2)
		RETURN "Prop_MB_crate_01A"
		
	ELIF IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_DRUGS)
	OR (IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_TRANSPONDER) AND GET_ENTITY_MODEL(veh) = BOXVILLE4)
	OR (IS_BIT_SET(iDoorBit2, ciFMMC_VEHICLE2_EXTRA_EMP) AND (GET_ENTITY_MODEL(veh) = INSURGENT OR GET_ENTITY_MODEL(veh) = INSURGENT2))
	OR IS_BIT_SET(iDoorBit7, ciFMMC_VEHICLE2_EXTRA_EMP)
	
		RETURN "chassis_dummy"
		
	ENDIF
	
	RETURN ""
	
ENDFUNC

FUNC INT GET_VEHICLE_CRATE_DOORS_COUNT(VEHICLE_INDEX veh)

	IF GET_ENTITY_MODEL(veh) = GRANGER
		RETURN 5
	ELIF GET_ENTITY_MODEL(veh) = SPEEDO
		RETURN 4
	ELIF GET_ENTITY_MODEL(veh) = GBURRITO
		RETURN 4
	ELIF GET_ENTITY_MODEL(veh) = BARRACKS			// BARRACKS ONLY HAS 2 DOORS - Dave W - 2057796
		RETURN 2
	ELIF GET_ENTITY_MODEL(veh) = BARRACKS3			// BARRACKS ONLY HAS 2 DOORS - Dave W - 2057796
		RETURN 2
	ELIF GET_ENTITY_MODEL(veh) = BENSON
		RETURN 5
	ELIF GET_ENTITY_MODEL(veh) = GBURRITO2
		RETURN 4
	ELIF GET_ENTITY_MODEL(veh) = MULE
		RETURN 4
	ELIF GET_ENTITY_MODEL(veh) = MULE3
		RETURN 4
	ENDIF
	
	RETURN 0

ENDFUNC

FUNC VECTOR GET_CRATE_OFFSET_FOR_VEHICLE(VEHICLE_INDEX veh, OBJECT_INDEX crates)
	MODEL_NAMES mnCrate = GET_ENTITY_MODEL(crates)
	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(veh)
	IF mnVeh = BARRACKS
	OR mnVeh = BARRACKS3
	//	RETURN <<0, 4, 0.52>>
		RETURN << 0.0, -0.09, 1.5059 >> // Changed as GET_VEHICLE_CRATE_BONE has changed for the Barracks, see 2090171
	ELIF mnVeh = BENSON
		IF mnCrate = HEI_PROP_HEIST_WEED_PALLET_02
			RETURN <<0.4, -3, 0.65>>
		ELSE
			RETURN <<0.4, -2, 1.09>>
		ENDIF
	ELIF mnVeh = GRANGER
		IF mnCrate = hei_prop_hei_ammo_pile_02
			RETURN <<0, 0, -0.257>>
		ENDIF
	ELIF mnVeh = GBURRITO2
		IF mnCrate = hei_Prop_MINI_Sever_02
		OR mnCrate = hei_Prop_MINI_Sever_03
		OR mnCrate = HEI_PROP_MINI_SEVER_BROKEN
			RETURN <<0, 1.1, -0.39>>
		ELIF mnCrate = HEI_PROP_DRUG_STATUE_STACK
			RETURN <<0, 1.29, -0.14>>
		ENDIF
	ELIF mnVeh = GBURRITO
		IF mnCrate = hei_Prop_MINI_Sever_02
		OR mnCrate = hei_Prop_MINI_Sever_03
		OR mnCrate = HEI_PROP_MINI_SEVER_BROKEN
			RETURN <<0, 1.1, -0.39>>
		ELIF mnCrate = HEI_PROP_DRUG_STATUE_STACK
			RETURN <<0, -2.5, -0.14>>
		ENDIF
	ELIF mnVeh = BOXVILLE4
		IF mnCrate = hei_Prop_Heist_Transponder
			RETURN <<0.28, -3.36, 1.52>>
		ENDIF
	ELIF mnVeh = MULE
	OR mnVeh = MULE3
	OR mnVeh = INT_TO_ENUM(MODEL_NAMES, HASH("MULE5"))
		IF mnCrate = hei_Prop_Heist_Tub_Truck
			RETURN <<0, 0.1, 0>>
		ENDIF
	ELIF mnVeh = INSURGENT
		IF mnCrate = HEI_PROP_HEIST_EMP
			RETURN <<0,-2.138671,0.682988>>
		ENDIF
	ELIF mnVeh = INSURGENT2
		IF mnCrate = HEI_PROP_HEIST_EMP
			RETURN <<0,-2.138671,0.562988>>
		ENDIF
	ELIF mnVeh = SPEEDO
		IF mnCrate = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_vancrate_01a"))
			RETURN <<0.0, -2.17, 0.13>>
		ELIF mnCrate = REH_PROP_REH_BOX_WOOD01A
			RETURN <<0.0, -0.5, -0.1>>
		ELIF mnCrate = REH_PROP_REH_BOX_METAL_01A
			RETURN <<0.0, -0.6, -0.127>>
		ENDIF	
	ELIF mnVeh = YOUGA2
		RETURN <<0.0, -1.5, -0.445>>
	ELIF mnVeh = YOUGA4
		RETURN <<0.0, -1.5, 0.095>>
	ELIF mnVeh = CADDY
		RETURN <<0.15, -1.15, 0.51>>
	ELIF mnVeh = YOUGA3
		IF mnCrate = REH_PROP_REH_BOMB_TECH_01A
			RETURN <<-0.2, 0.38, -0.445>>
		ENDIF
	ELIF mnVeh = PONY
		RETURN <<0, -0.9, -0.142>>
	ELIF mnVeh = BURRITO
		RETURN <<0, -1.2, -0.3>>
	ELIF mnVeh = RUMPO
		RETURN <<0, -0.9, -0.46>>
	ELIF mnVeh = BOXVILLE2
		RETURN <<0, -1.5, -0.28>>
	ENDIF
	
	RETURN <<0,0,0>>

ENDFUNC

FUNC VECTOR GET_CRATE_ROTATION_FOR_VEHICLE(VEHICLE_INDEX veh, OBJECT_INDEX crates)
	
	IF GET_ENTITY_MODEL(veh) = BOXVILLE4
		IF GET_ENTITY_MODEL(crates) = hei_Prop_Heist_Transponder
			RETURN <<90, 0, 180>>
		ENDIF
	ELIF GET_ENTITY_MODEL(veh) = GRANGER
		IF GET_ENTITY_MODEL(crates) = hei_prop_hei_ammo_pile_02
			RETURN <<0, 0, 90>>
		ENDIF
	ELIF GET_ENTITY_MODEL(veh) = RUMPO
	OR GET_ENTITY_MODEL(veh) = PONY
	OR GET_ENTITY_MODEL(veh) = BURRITO
	OR GET_ENTITY_MODEL(veh) = BOXVILLE2
	OR GET_ENTITY_MODEL(veh) = SPEEDO
		IF GET_ENTITY_MODEL(crates) = REH_PROP_REH_BOX_METAL_01A
			RETURN <<0, 0, -90>>
		ENDIF
	ELIF GET_ENTITY_MODEL(veh) = GBURRITO
		IF GET_ENTITY_MODEL(crates) = HEI_PROP_DRUG_STATUE_STACK
			RETURN <<0, 0, 180>>
		ENDIF
	ENDIF
	
	RETURN <<0,0,0>>

ENDFUNC

FUNC BOOL SHOULD_CRATE_COLLISION_BE_ENABLED(VEHICLE_INDEX veh)
	
	MODEL_NAMES vehModel = GET_ENTITY_MODEL(veh)
	
	SWITCH vehModel
		CASE BENSON
		CASE GBURRITO // Added GBURRITO for 2192800
		CASE GBURRITO2 //url:bugstar:2211622 - [Pacific Standard - Hack] Both vans appear to move around after the mid-mission cutscene, mph_pac_hac_mcs1
		CASE YOUGA2
		CASE CADDY
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE

ENDFUNC

FUNC BOOL FMMC_SET_THIS_VEHICLE_CRATES(VEHICLE_INDEX veh, INT iDoorBit, INT iDoorBit2, INT iDoorBit7, OBJECT_INDEX &crates, INT iCargo)	
	
	MODEL_NAMES crateModel = GET_VEHICLE_CRATE_MODEL(iDoorBit, iDoorBit2, iDoorBit7, veh)
	IF iCargo != ciVEHICLE_CARGO_OFF
		crateModel = GET_VEHICLE_CARGO_MODEL_FROM_INT(iCargo, GET_ENTITY_MODEL(veh))
	ENDIF
	STRING sBone = GET_VEHICLE_CRATE_BONE(iDoorBit, iDoorBit2, iDoorBit7, veh)
	
	IF DOES_ENTITY_EXIST(crates)
		PRINTLN("THAT CRATE EXISTS, DELETE IT")
		DELETE_OBJECT(crates)
	ENDIF

	IF crateModel != DUMMY_MODEL_FOR_SCRIPT
		REQUEST_MODEL(crateModel)
		PRINTLN("SETTING THE VEHICLE CRATES")
		IF HAS_MODEL_LOADED(crateModel)
			IF NOT DOES_ENTITY_EXIST(crates)
				crates = CREATE_OBJECT(crateModel,  GET_WORLD_POSITION_OF_ENTITY_BONE(veh, GET_ENTITY_BONE_INDEX_BY_NAME(veh, sBone)))
				ATTACH_ENTITY_TO_ENTITY(crates, veh, GET_ENTITY_BONE_INDEX_BY_NAME(veh, sBone), GET_CRATE_OFFSET_FOR_VEHICLE(veh, crates), GET_CRATE_ROTATION_FOR_VEHICLE(veh, crates))
				SET_ENTITY_LOD_DIST(crates, 100)
				SET_MODEL_AS_NO_LONGER_NEEDED(crateModel)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		PRINTLN("NO VEHICLE CRATES. CARRY ON")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL FMMC_SET_THIS_VEHICLE_CRATES_ROWAN_C(VEHICLE_INDEX veh, INT iDoorBit, INT iDoorBit2, INT iDoorBit7, NETWORK_INDEX &nicrates)	
	
	MODEL_NAMES crateModel = GET_VEHICLE_CRATE_MODEL(iDoorBit, iDoorBit2, iDoorBit7, veh)
	STRING sBone = GET_VEHICLE_CRATE_BONE(iDoorBit, iDoorBit2, iDoorBit7, veh)
	
	IF crateModel != DUMMY_MODEL_FOR_SCRIPT
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(nicrates)
			IF CAN_REGISTER_MISSION_OBJECTS( 1 )
				REQUEST_MODEL(crateModel)
				PRINTLN("SETTING THE VEHICLE CRATES sBone = ", sBone)
				IF HAS_MODEL_LOADED(crateModel)
					RESERVE_NETWORK_MISSION_OBJECTS( GET_NUM_RESERVED_MISSION_OBJECTS() + 1 ) 	
					IF CREATE_NET_OBJ(nicrates,crateModel,GET_WORLD_POSITION_OF_ENTITY_BONE(veh, GET_ENTITY_BONE_INDEX_BY_NAME(veh, sBone)))
						ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(nicrates), veh, GET_ENTITY_BONE_INDEX_BY_NAME(veh, sBone), GET_CRATE_OFFSET_FOR_VEHICLE(veh, NET_TO_OBJ(nicrates)), GET_CRATE_ROTATION_FOR_VEHICLE(veh, NET_TO_OBJ(nicrates)), TRUE, FALSE, SHOULD_CRATE_COLLISION_BE_ENABLED(veh))
						SET_ENTITY_LOD_DIST(NET_TO_OBJ(nicrates), 100)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(nicrates),FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(crateModel)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("crate exists. CARRY ON")
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("NO VEHICLE CRATES. return false")
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Added by Conor McGuire for 2046795 please keep in synce with function above
FUNC BOOL FMMC_SET_THIS_VEHICLE_CRATES_ROWAN_C_NON_NETWORKED(VEHICLE_INDEX &veh, INT iDoorBit, INT iDoorBit2, INT iDoorBit7, OBJECT_INDEX &crateID, BOOL bCreateVisible = TRUE)	
	
	MODEL_NAMES crateModel = GET_VEHICLE_CRATE_MODEL(iDoorBit, iDoorBit2, iDoorBit7, veh)
	STRING sBone = GET_VEHICLE_CRATE_BONE(iDoorBit, iDoorBit2, iDoorBit7, veh)

	IF crateModel != DUMMY_MODEL_FOR_SCRIPT
		IF NOT DOES_ENTITY_EXIST(crateID)
		AND (DOES_ENTITY_EXIST(veh) AND IS_VEHICLE_DRIVEABLE(veh))
			REQUEST_MODEL(crateModel)
			PRINTLN("SETTING THE VEHICLE CRATES")
			IF HAS_MODEL_LOADED(crateModel)
				crateID = CREATE_OBJECT(crateModel,GET_WORLD_POSITION_OF_ENTITY_BONE(veh, GET_ENTITY_BONE_INDEX_BY_NAME(veh, sBone)),FALSE,FALSE,TRUE)
				ATTACH_ENTITY_TO_ENTITY(crateID, veh, GET_ENTITY_BONE_INDEX_BY_NAME(veh, sBone), GET_CRATE_OFFSET_FOR_VEHICLE(veh, crateID), GET_CRATE_ROTATION_FOR_VEHICLE(veh, crateID), FALSE, FALSE, SHOULD_CRATE_COLLISION_BE_ENABLED(veh))
				SET_ENTITY_LOD_DIST(crateID, 100)
				SET_ENTITY_VISIBLE(crateID,bCreateVisible)
				PRINTLN("bCreateVisible = ",bCreateVisible)
				SET_MODEL_AS_NO_LONGER_NEEDED(crateModel)
				PRINTLN("crate created!")
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("crate exists or vehicle doesn't. CARRY ON")
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("NO VEHICLE CRATES. return false")
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC
