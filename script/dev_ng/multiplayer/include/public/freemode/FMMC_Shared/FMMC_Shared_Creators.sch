USING "FMMC_Shared_Common.sch"
USING "FMMC_Camera.sch"

#IF IS_DEBUG_BUILD
PROC PROCESS_FORCED_PUBLIC_CREATOR_FOR_DEV(structFMMC_MENU_ITEMS& sFMMCMenu, BOOL &bForcedPublicCreator)
	IF bForcedPublicCreator != g_bFMMCBlockRockstarDevCheckForCreator
		bForcedPublicCreator = g_bFMMCBlockRockstarDevCheckForCreator
		REFRESH_MENU_ASAP(sFMMCMenu)
	ENDIF
	
	IF bForcedPublicCreator
		SET_TEXT_SCALE(0.6800, 0.6800)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_CENTRE(TRUE)
		SET_TEXT_DROPSHADOW(2, 0, 0, 0, 255)
		SET_TEXT_EDGE(0, 0, 0, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.0650, "STRING", "FORCED PUBLIC CREATOR ACTIVE")
	ENDIF
	
ENDPROC
#ENDIF

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Privileges & Privacy
// ##### Description: Creator functions related to console privileges and privacy settings
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL CAN_I_PUBLISH_WITH_CURRENT_PRIVILEGES()
	IF IS_XBOX_PLATFORM()
		IF !NETWORK_HAS_ROS_PRIVILEGE(RLROS_PRIVILEGEID_CREATORS)
		OR !NETWORK_HAS_SOCIAL_NETWORKING_SHARING_PRIV()
		OR !NETWORK_HAVE_USER_CONTENT_PRIVILEGES()
			PRINTLN("CAN_I_PUBLISH_WITH_CURRENT_PRIVILEGES - Current Xbox privileges blocking publishing")
			RETURN FALSE
		ENDIF
	ELIF IS_PLAYSTATION_PLATFORM()
		IF !NETWORK_IS_NP_AVAILABLE()
		OR !NETWORK_HAS_ROS_PRIVILEGE(RLROS_PRIVILEGEID_CREATORS)
		OR !NETWORK_HAVE_USER_CONTENT_PRIVILEGES()
			PRINTLN("CAN_I_PUBLISH_WITH_CURRENT_PRIVILEGES - Current PlayStation privileges blocking publishing")
			RETURN FALSE
		ENDIF
	ELSE
		IF !NETWORK_CHECK_USER_CONTENT_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE)
		AND!NETWORK_CHECK_USER_CONTENT_PRIVILEGES(PC_FRIENDS, GAMER_INDEX_ANYONE)
		OR !NETWORK_HAS_ROS_PRIVILEGE(RLROS_PRIVILEGEID_CREATORS)
			PRINTLN("CAN_I_PUBLISH_WITH_CURRENT_PRIVILEGES - Current PC privileges blocking publishing")
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC	


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Linked Entities
// ##### Description: Linked Entity options will be updated when other entities are removed to keep the option pointing to the same entity.
// #####    		  See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(INT iEntityID, BOOL bAllDeleted, INT &iLinkedOption)
	
	IF bAllDeleted
		iLinkedOption = -1
		EXIT
	ENDIF
	
	IF iLinkedOption > iEntityID
		iLinkedOption--
	ELIF iLinkedOption = iEntityID
		iLinkedOption = -1
	ENDIF
	
ENDPROC

PROC GET_ENTITY_LINK_BITSET_MASKS(INT iEntityID, INT &iMask0, INT &iMask1)
	INT iFullMask = -1
	iMask1 = SHIFT_LEFT(iFullMask, iEntityID) //All bits to the right of iEntityID in the bitset set
	iMask0 = (iMask1 * -1) - 1 //All bits to the left of iEntityID in the bitset set
ENDPROC

PROC UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(INT iEntityID, BOOL bAllDeleted, INT &iLinkedBitset, INT iMask0, INT iMask1)
	
	IF bAllDeleted
		iLinkedBitset = 0
		EXIT
	ENDIF
	
	CLEAR_BIT(iLinkedBitset, iEntityID) //Clear iEntityID's bit to make the rest less complicated
	
	INT iMasked0 = iLinkedBitset - (iLinkedBitset & iMask0) //All of the bits in iLinkedBitset to the left of iEntityID - to be shifted down
	
	iMasked0 = SHIFT_RIGHT(iMasked0, 1)
	CLEAR_BIT(iMasked0, 31) //The last bit is left dirty from the shift
	
	INT iMasked1 = iLinkedBitset - (iLinkedBitset & iMask1) //All of the bits in iLinkedBitset to the right of iEntityID - left alone
	
	iLinkedBitset = iMasked0 | iMasked1
	
ENDPROC

/// NOTE:
///    This only works for the FMMC Long Bitset format, using it with the old format will not work due to missed bits
PROC UPDATE_ENTITY_LINK_LONG_BITSET_AFTER_DELETION(INT iLongBitsetIndex, BOOL bAllDeleted, INT &iLinkedBitset[], INT iMask0, INT iMask1, INT iLongBitsetArray)
	
	IF bAllDeleted
		CLEAR_LONG_BITSET(iLinkedBitset)
		EXIT
	ENDIF
	
	//Sort out the bitset the entity is in first
	UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iLongBitsetIndex, bAllDeleted, iLinkedBitset[iLongBitsetArray], iMask0, iMask1)
	
	IF iLongBitsetArray = COUNT_OF(iLinkedBitset) - 1
		//No further shifting required
		EXIT
	ENDIF
	
	//Sort the rest down
	INT i
	FOR i = iLongBitsetArray + 1 TO COUNT_OF(iLinkedBitset) - 1
		IF IS_BIT_SET(iLinkedBitset[i], 0)
			SET_BIT(iLinkedBitset[i - 1], 31) //Move the first bit to the last bit of the previous index
		ENDIF
		iLinkedBitset[i] = SHIFT_RIGHT(iLinkedBitset[i], 1)
		CLEAR_BIT(iLinkedBitset[i], 31) //The last bit is left dirty from the shift
	ENDFOR
	
ENDPROC
		
/// PURPOSE:
///    Add any options in the Peds struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_PEDS(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
	INT i, j
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1
	
		FOR j = 0 TO MAX_ASSOCIATED_GOTO_TASKS - 1
			
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[j].eCustomScenarioType
				CASE FMMC_PED_CUSTOM_SCENARIO_SHOOT_AT_ENTITY
					IF iEntityType = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[j].iCustomScenarioInts[ciFMMC_CUSTOM_SCENARIO_SHOOT_AT_ENTITY_MENU_ENTITY_TYPE]
						UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[j].iCustomScenarioInts[ciFMMC_CUSTOM_SCENARIO_SHOOT_AT_ENTITY_MENU_ENTITY_ID])
					ENDIF
				BREAK
				CASE FMMC_PED_CUSTOM_SCENARIO_SUPPRESSING_FIRE
					IF iEntityType = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[j].iCustomScenarioInts[ciFMMC_CUSTOM_SCENARIO_SUPPRESSING_FIRE_MENU_ENTITY_TYPE]
						UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[j].iCustomScenarioInts[ciFMMC_CUSTOM_SCENARIO_SUPPRESSING_FIRE_MENU_ENTITY_ID])
					ENDIF
				BREAK
				CASE FMMC_PED_CUSTOM_SCENARIO_BLIND_FIRE
					IF iEntityType = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[j].iCustomScenarioInts[ciFMMC_CUSTOM_SCENARIO_BLIND_FIRE_MENU_ENTITY_TYPE]
						UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[j].iCustomScenarioInts[ciFMMC_CUSTOM_SCENARIO_BLIND_FIRE_MENU_ENTITY_ID])
					ENDIF
				BREAK
				CASE FMMC_PED_CUSTOM_SCENARIO_BLIND_EXPLOSIVE
					IF iEntityType = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[j].iCustomScenarioInts[ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_ENTITY_TYPE]
						UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[j].iCustomScenarioInts[ciFMMC_CUSTOM_SCENARIO_BLIND_EXPLOSIVE_MENU_ENTITY_ID])
					ENDIF
				BREAK
			ENDSWITCH
		ENDFOR
	
		//Variable Entity Type Options
		IF iEntityType = GET_LINKED_ENTITY_TYPE_FROM_SPAWN_NEAR_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpawnNearType)
			UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpawnNearEntityID)
		ENDIF
		IF iEntityType = GET_LINKED_ENTITY_TYPE_FROM_RULE_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPrimaryTargetOverrideType)
			UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPrimaryTargetOverrideID)
		ENDIF
		IF iEntityType = GET_LINKED_ENTITY_TYPE_FROM_RULE_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondaryTargetOverrideType)
			UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondaryTargetOverrideID)
		ENDIF
		
		SWITCH iEntityType
			CASE ciFMMC_ENTITY_LINK_TYPE_PED
				SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpecialAnim
					CASE ciPED_IDLE_ANIM__GOLF_CLUB_INTERROGATION_FRANKLIN
					CASE ciPED_IDLE_ANIM__GOLF_CLUB_INTERROGATION_GOLFER
						UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCustomAnimOptionA)
					BREAK
				ENDSWITCH
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedFixationTarget)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpookLinkedPed)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCleanupVehicle)
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpookPedOnVehicleDamageBS, iMask0, iMask1)
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpookPedOnVehicleTouchedBS, iMask0, iMask1)
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.iVehiclePartialDisguiseBS, iMask0, iMask1)
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.iVehicleFullDisguiseBS, iMask0, iMask1)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DIALOGUE_TRIGGER
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDialogueTriggerBlock)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sCleanupData.eDespawnOption = FMMC_DESPAWN_USING_ZONE
					UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sCleanupData.iCleanupValue1)
				ENDIF
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAggroZone)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedFleeZone)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDropPickupIndex)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sTrainAttachmentData.iTrainIndex)
			BREAK
		ENDSWITCH
	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Vehicles struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_VEHICLES(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when first bitset option is added
	UNUSED_PARAMETER(iMask0)
	UNUSED_PARAMETER(iMask1)
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1
		
		//Variable Entity Type Options
		IF iEntityType = GET_LINKED_ENTITY_TYPE_FROM_SPAWN_NEAR_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSpawnNearType)
			UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSpawnNearEntityID)
		ENDIF
	
		SWITCH iEntityType
			CASE ciFMMC_ENTITY_LINK_TYPE_PED
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sCleanupData.eDespawnOption = FMMC_DESPAWN_USING_ZONE
					UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sCleanupData.iCleanupValue1)
				ENDIF
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sTrainAttachmentData.iTrainIndex)
			BREAK
		ENDSWITCH
	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Objects struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_OBJECTS(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when first bitset option is added
	UNUSED_PARAMETER(iMask0)
	UNUSED_PARAMETER(iMask1)
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
		
		//Variable Entity Type Options
		IF iEntityType = GET_LINKED_ENTITY_TYPE_FROM_SPAWN_NEAR_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType)
			UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID)
		ENDIF
	
		SWITCH iEntityType
			CASE ciFMMC_ENTITY_LINK_TYPE_PED
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectInteractionVeh)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectToPickUpAfterMinigame)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectInteractionObj)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sCleanupData.eDespawnOption = FMMC_DESPAWN_USING_ZONE
					UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sCleanupData.iCleanupValue1)
				ENDIF
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.iTrainIndex)
			BREAK
		ENDSWITCH
	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Props struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_PROPS(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when first bitset option is added
	UNUSED_PARAMETER(iMask0)
	UNUSED_PARAMETER(iMask1)
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1
		SWITCH iEntityType
			CASE ciFMMC_ENTITY_LINK_TYPE_PED
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iTrapToActivate)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
			BREAK
		ENDSWITCH
	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Dynoprops struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_DYNOPROPS(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when first bitset option is added
	UNUSED_PARAMETER(iMask0)
	UNUSED_PARAMETER(iMask1)
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoprops - 1
		SWITCH iEntityType
			CASE ciFMMC_ENTITY_LINK_TYPE_PED
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sCleanupData.eDespawnOption = FMMC_DESPAWN_USING_ZONE
					UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sCleanupData.iCleanupValue1)
				ENDIF
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sDynopropTrainAttachmentData.iTrainIndex)
			BREAK
		ENDSWITCH
	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Interactables struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_INTERACTABLES(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables - 1
		SWITCH iEntityType
			CASE ciFMMC_ENTITY_LINK_TYPE_PED
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
				UPDATE_ENTITY_LINK_LONG_BITSET_AFTER_DELETION(iLongBitsetIndex, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sConsequences.iInteractable_LinkedInteractablesBS, iMask0, iMask1, iLongBitsetArray)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sConsequences.iInteractable_DoorsToSwitchConfigBS, iMask0, iMask1)
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sConsequences.iInteractable_DoorsToLockBS, iMask0, iMask1)
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sConsequences.iInteractable_DoorsToUnlockBS, iMask0, iMask1)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sInteractableTrainAttachmentData.iTrainIndex)
			BREAK
		ENDSWITCH
	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Locations struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_LOCATIONS(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when the first option is added
	UNUSED_PARAMETER(iEntityType)
	UNUSED_PARAMETER(iEntityID)
	UNUSED_PARAMETER(bAllDeleted)
	//Remove when first bitset option is added
	UNUSED_PARAMETER(iMask0)
	UNUSED_PARAMETER(iMask1)
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
//	INT i
//	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations - 1
//		SWITCH iEntityType
//			CASE ciFMMC_ENTITY_LINK_TYPE_PED
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
//			BREAK
//
//			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
//			BREAK
//		ENDSWITCH
//	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Dialogue Triggers struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_DIALOGUE_TRIGGERS(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
	INT i, ii
	FOR i = 0 TO g_FMMC_STRUCT.iNumberOfDialogueTriggers - 1
		
		//Variable Entity Type Options
		IF iEntityType = GET_LINKED_ENTITY_TYPE_FROM_ENTITY_EXISTS_TYPE(g_FMMC_STRUCT.sDialogueTriggers[i].iEntityExistsType)
			UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iEntityExistsIndex)
		ENDIF
		
		SWITCH iEntityType
			CASE ciFMMC_ENTITY_LINK_TYPE_PED
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iPedVoice)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iSecondPedVoice)
				FOR ii = 0 TO ciDIALOGUE_PED_IN_RANGE_MAX-1
					UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iPedInRangeTrigger[ii])
				ENDFOR
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iFocusPed)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iPedKilled)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iPedTrigger)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iVehicleTrigger)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iSpecificVehicle)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iSpecificVehicle2)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iSpecificVehicle3)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iSpecificVehicle4)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iSpecificVehicle5)
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iVehicleHacked, iMask0, iMask1)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iRequireCrashIntoSpecificVehicle)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iDrillBreakLockObjIndex)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iObjectToCollect)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iInteractWithObj)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iRequiredCompletedInteractable)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iRequiredUncompletedInteractable)
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iRequiredInUseInteractable)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iZoneIndex)
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iTriggeredByZoneBS, iMask0, iMask1)
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sDialogueTriggers[i].iBlockedByZoneBS, iMask0, iMask1)
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
			BREAK
		ENDSWITCH
	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Zones struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_ZONES(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when first bitset option is added
	UNUSED_PARAMETER(iMask0)
	UNUSED_PARAMETER(iMask1)
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1
		SWITCH iEntityType
			CASE ciFMMC_ENTITY_LINK_TYPE_PED
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
			BREAK

			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
			BREAK
		ENDSWITCH
		
		IF iEntityType = GET_LINKED_ENTITY_TYPE_FROM_ENTITY_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkType)
			UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
		ENDIF
		IF iEntityType = GET_LINKED_ENTITY_TYPE_FROM_ENTITY_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAltEntityLinkType)
			UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAltEntityLinkIndex)
		ENDIF
	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Doors struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_DOORS(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when the first option is added
	UNUSED_PARAMETER(iEntityType)
	UNUSED_PARAMETER(iEntityID)
	UNUSED_PARAMETER(bAllDeleted)
	//Remove when first bitset option is added
	UNUSED_PARAMETER(iMask0)
	UNUSED_PARAMETER(iMask1)
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
//	INT i
//	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
//		SWITCH iEntityType
//			CASE ciFMMC_ENTITY_LINK_TYPE_PED
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
//			BREAK
//
//			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
//			BREAK
//		ENDSWITCH
//	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Pickups/Weapons struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_PICKUPS(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when the first option is added
	UNUSED_PARAMETER(iEntityType)
	UNUSED_PARAMETER(iEntityID)
	UNUSED_PARAMETER(bAllDeleted)
	//Remove when first bitset option is added
	UNUSED_PARAMETER(iMask0)
	UNUSED_PARAMETER(iMask1)
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
//	INT i
//	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations - 1
//		SWITCH iEntityType
//			CASE ciFMMC_ENTITY_LINK_TYPE_PED
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
//			BREAK
//
//			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
//			BREAK
//		ENDSWITCH
//	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Trains struct which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_TRAINS(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when the first option is added
	UNUSED_PARAMETER(iEntityType)
	UNUSED_PARAMETER(iEntityID)
	UNUSED_PARAMETER(bAllDeleted)
	//Remove when first bitset option is added
	UNUSED_PARAMETER(iMask0)
	UNUSED_PARAMETER(iMask1)
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
//	INT i
//	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTrains - 1
//		SWITCH iEntityType
//			CASE ciFMMC_ENTITY_LINK_TYPE_PED
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_PROP
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
//			BREAK
//			
//			CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
//			BREAK
//
//			CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
//			BREAK
//		ENDSWITCH
//	ENDFOR

ENDPROC

/// PURPOSE:
///    Add any options in the Rules struct (sFMMCEndConditions) which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_RULES(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	INT i, iTeam, iBounds
	FOR iTeam = 0 TO g_FMMC_STRUCT.iMaxNumberOfTeams - 1
		
		//Team Arrayed Only
		SWITCH iEntityType
			CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInLastMissionVehicleFromBS, iMask0, iMask1)
			BREAK
		ENDSWITCH
		
		//Rule Arrayed
		FOR i = 0 TO g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iNumberOfTeamRules - 1
			SWITCH iEntityType
				CASE ciFMMC_ENTITY_LINK_TYPE_PED
					UPDATE_ENTITY_LINK_LONG_BITSET_AFTER_DELETION(iLongBitsetIndex, bAllDeleted, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHighPriorityPed[i], iMask0, iMask1, iLongBitsetArray)					
					UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iProgressAnimStageForPed[i])
				BREAK
				
				CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
					UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInMissionVehicle[i])
				BREAK
				
				CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT					
					UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[i])
				BREAK
				
				CASE ciFMMC_ENTITY_LINK_TYPE_PROP
				BREAK
				
				CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
				BREAK
				
				CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
				BREAK
				
				CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
				BREAK
				
				CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
					FOR iBounds = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
						UPDATE_ENTITY_LINK_LONG_BITSET_AFTER_DELETION(iLongBitsetIndex, bAllDeleted, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[i][iBounds].iZonesToUseBS, iMask0, iMask1, iLongBitsetArray)
					ENDFOR
					UPDATE_ENTITY_LINK_LONG_BITSET_AFTER_DELETION(iLongBitsetIndex, bAllDeleted, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffZonesBS[i], iMask0, iMask1, iLongBitsetArray)
				BREAK
				
				CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
				BREAK
				
				CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
				BREAK
				
				CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN
				BREAK
			ENDSWITCH
		ENDFOR
	ENDFOR

ENDPROC

FUNC BOOL UPDATE_CUTSCENCE_ENTITY_LINKS(FMMC_CUTSCENE_ENTITY &sCutsceneEntities[], INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE)
	BOOL bDeletedAny = FALSE
	INT i
	FOR i = 0 TO COUNT_OF(sCutsceneEntities) - 1
	
		IF sCutsceneEntities[i].iIndex = -1
			RELOOP
		ENDIF
		
		IF iEntityType != GET_LINKED_ENTITY_TYPE_FROM_CREATION_TYPE(sCutsceneEntities[i].iType)
			RELOOP
		ENDIF
		
		UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, sCutsceneEntities[i].iIndex)
		
		IF sCutsceneEntities[i].iIndex = -1
			CLEAR_CUTSCENE_ENTITY_DATA(sCutsceneEntities[i])
			bDeletedAny = TRUE
		ENDIF
	ENDFOR
	
	RETURN bDeletedAny
ENDFUNC

/// PURPOSE:
///    Add any options in the Misc structs  which reference another entity here.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    Params are filled in from PROCESS_LINKED_ENTITY_REMOVAL. This should not be called elsewhere
PROC UPDATE_ENTITY_LINKS_MISC(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE, INT iMask0 = 0, INT iMask1 = 0, INT iLongBitsetArray = 0, INT iLongBitsetIndex = 0)
	
	//Remove when first long bitset option is added
	UNUSED_PARAMETER(iLongBitsetArray)
	UNUSED_PARAMETER(iLongBitsetIndex)
	
	INT i
	
	//Cached Vehicles
	IF iEntityType = ciFMMC_ENTITY_LINK_TYPE_VEHICLE
		INT iPart
		FOR i = 0 TO FMMC_MAX_TEAMS - 1
			FOR iPart = 0 TO MAX_PLAYERS_PER_TEAM - 1
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT.iCachedVehicle[i][iPart])
			ENDFOR
		ENDFOR
	ENDIF
	
	//Rockets
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfRocketPositions - 1
		SWITCH iEntityType
			CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT					
				UPDATE_ENTITY_LINK_OPTION_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[i])
			BREAK
			
			CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
				UPDATE_ENTITY_LINK_BITSET_AFTER_DELETION(iEntityID, bAllDeleted, g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketZoneBS[i], iMask0, iMask1)
			BREAK
			
		ENDSWITCH
	ENDFOR
	
	//Mocap Cutscenes
	FOR i = 0 TO MAX_MOCAP_CUTSCENES - 1
		IF UPDATE_CUTSCENCE_ENTITY_LINKS(g_FMMC_STRUCT.sMocapCutsceneData[i].sCutsceneEntities, iEntityType, iEntityID, bAllDeleted)
			SORT_CUTSCENE_ENTITIES(g_FMMC_STRUCT.sMocapCutsceneData[i].sCutsceneEntities)
		ENDIF
	ENDFOR

	//Scripted Cutscenes
	FOR i = 0 TO _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES - 1
		IF UPDATE_CUTSCENCE_ENTITY_LINKS(g_FMMC_STRUCT.sScriptedCutsceneData[i].sCutsceneEntities, iEntityType, iEntityID, bAllDeleted)
			SORT_CUTSCENE_ENTITIES(g_FMMC_STRUCT.sScriptedCutsceneData[i].sCutsceneEntities)
		ENDIF
	ENDFOR
	
	//End Cutscene
	IF UPDATE_CUTSCENCE_ENTITY_LINKS(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities, iEntityType, iEntityID, bAllDeleted)
		SORT_CUTSCENE_ENTITIES(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Called when a linkable entity is deleted (or an entire entity struct array) to update links in other arrays
///    Linked Entity options will be updated when other entities are removed to keep the option pointing to the same entity.
///    See: https://hub.gametools.dev/display/RSGGTAV/Linked+Entity+Menu+Items+-+GTA+V+Instanced+Content
/// PARAMS:
///    iEntityType - The type of entity beign removed. Should match one of the ciFMMC_ENTITY_LINK_TYPE_ consts.
///    iEntityID - The entity being removed, leave as -1 if settign the whole array and set bAllDeleted to TRUE
///    bAllDeleted - TRUE if this is called after the entire struct is wiped
PROC PROCESS_LINKED_ENTITY_REMOVAL(INT iEntityType, INT iEntityID = -1, BOOL bAllDeleted = FALSE)
	   
	IF iEntityType = ciFMMC_ENTITY_LINK_TYPE_NONE
		EXIT
	ENDIF
	
	// Cache Bitset values in advance so they can be reused
	INT iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex
	IF iEntityID >= 32
		iLongBitsetArray = iEntityID / 32
		iLongBitsetIndex = iEntityID % 32
		GET_ENTITY_LINK_BITSET_MASKS(iLongBitsetIndex, iMask0, iMask1)
	ELIF iEntityID != -1
		iLongBitsetArray = 0
		iLongBitsetIndex = iEntityID
		GET_ENTITY_LINK_BITSET_MASKS(iEntityID, iMask0, iMask1)
	ENDIF
	
	UPDATE_ENTITY_LINKS_PEDS(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_VEHICLES(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_OBJECTS(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_PROPS(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_DYNOPROPS(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_INTERACTABLES(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_LOCATIONS(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_DIALOGUE_TRIGGERS(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_ZONES(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_DOORS(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_PICKUPS(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_TRAINS(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_RULES(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	UPDATE_ENTITY_LINKS_MISC(iEntityType, iEntityID, bAllDeleted, iMask0, iMask1, iLongBitsetArray, iLongBitsetIndex)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Creator Text, Lines, Markers and Shapes
// ##### Description: Functions for diplaying various visual elements used by the creators
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC ADD_PRECISION_MARKER(CREATION_VARS_STRUCT &sCurrentVarsStruct, VECTOR vCoord, VECTOR vRot, FLOAT fScale = 1.0)
	INT i = 0
	FOR i = 0 TO MAX_CREATOR_PRECISION_MARKERS-1
		IF NOT IS_VECTOR_ZERO(sCurrentVarsStruct.vPrecisionPos[i])
			RELOOP
		ENDIF
		
		sCurrentVarsStruct.vPrecisionPos[i] = vCoord
		sCurrentVarsStruct.vPrecisionRot[i] = vRot
		sCurrentVarsStruct.fPrecisionScale[i] = fScale
		
		BREAKLOOP
	ENDFOR
ENDPROC

PROC DRAW_GENERIC_CREATOR_ON_SCREEN_TEXT(VECTOR vPos, STRING sTxt, FLOAT fExtraHeight = 0.0, FLOAT fScale = 0.25, FLOAT fFloatComponent = -1.0, INT iIntComponent = -1)
	FLOAT fTempX2, fTempY2
	TEXT_PLACEMENT eTP
	TEXT_STYLE eTextStyle
	
	vPos.z += fExtraHeight
	
	VECTOR vCamPos
	
	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
		vCamPos = GET_CAM_COORD(GET_RENDERING_CAM())
		
		FLOAT fCamDist = VDIST2(vPos, vCamPos)
		
		IF fCamDist > 30000
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vPos)
		IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPos, fTempx2, fTempy2)
		
			eTP.x = fTempx2
			eTP.y = fTempy2
			eTextStyle.aFont = FONT_STANDARD
			eTextStyle.XScale = fScale
			eTextStyle.YScale = fScale
			eTextStyle.drop = DROPSTYLE_ALL
			HUD_COLOURS col = HUD_COLOUR_WHITE
			
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(col), eTextStyle.r, eTextStyle.g, eTextStyle.b, eTextStyle.a)
	
			TEXT_LABEL_15 tl15Txt = sTxt
 			
			SET_TEXT_STYLE(eTextStyle)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(tl15Txt)
				IF fFloatComponent > -1.0
					ADD_TEXT_COMPONENT_FLOAT(fFloatComponent, 1)
				ENDIF
				IF iIntComponent > -1
					ADD_TEXT_COMPONENT_INTEGER(iIntComponent)
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(eTP.x, eTP.y)
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_POLY_BETWEEN_POINTS(VECTOR vPos1, VECTOR vPos2, FLOAT fWidth, INT iR, INT iG, INT iB, INT iA = 255)
	
	VECTOR vCornerPrev[2]
	UNUSED_PARAMETER(vCornerPrev)
	IF vPos1.x != 0
	AND vPos1.y != 0
	
		VECTOR vecPrev = vPos1 - vPos2
		VECTOR vCrossPrev = CROSS_PRODUCT(vecPrev, <<vecPrev.x, vecPrev.y, 0.0>>)
		vCrossPrev /= VMAG(vCrossPrev)
		vCrossPrev *= fWidth * 0.5
		
		vCornerPrev[0] = <<vPos1.x, vPos1.y, vPos1.z>> + vCrossPrev
		vCornerPrev[1] = <<vPos1.x, vPos1.y, vPos1.z>> - vCrossPrev
	ENDIF
	
	VECTOR vec = vPos1 - vPos2
	VECTOR vCross = CROSS_PRODUCT(vec, <<vec.x, vec.y, 0.0>>)
	
	vCross /= VMAG(vCross)
	vCross *= fWidth * 0.5
	
	VECTOR vCorner[4]
	
	vCorner[3] = <<vPos1.x, vPos1.y, vPos1.z>> + vCross
	vCorner[2] = <<vPos1.x, vPos1.y, vPos1.z>> - vCross
	vCorner[1] = <<vPos2.x, vPos2.y, vPos2.z>> - vCross
	vCorner[0] = <<vPos2.x, vPos2.y, vPos2.z>> + vCross
	
	SET_BACKFACECULLING(FALSE)
	
	DRAW_POLY(vCorner[0], vCorner[1], vCorner[2], iR, iG, iB, iA)
	DRAW_POLY(vCorner[2], vCorner[3], vCorner[0], iR, iG, iB, iA)
	
	SET_BACKFACECULLING(TRUE)
ENDPROC

PROC DRAW_CURVED_LINE_BETWEEN_POINTS(CREATION_VARS_STRUCT& sCurrentVarsStruct, VECTOR vStartPos, VECTOR vEndPos, STRING sLabel, INT iLabelInt = -1, FLOAT fLabelFloat = -1.0, INT iSmoothness = 20, FLOAT fTargetHeight = 2.0, INT iR = 255, INT iG = 255, INT iB = 255)

	/// LINE ///
	VECTOR vPrevPos = vStartPos
	
	//Give the line extra height based on distance, especially vertical distance (but not too much if the pressurepad is the one which is higher up)
	FLOAT fXDistance = ABSF(vEndPos.x - vStartPos.x)
	FLOAT fYDistance = ABSF(vEndPos.y - vStartPos.y)
	FLOAT fZDistance = ABSF(vEndPos.z - vStartPos.z)
	
	fTargetHeight *= 1.0 + (fXDistance * 0.010)
	fTargetHeight *= 1.0 + (fYDistance * 0.010)
	fTargetHeight *= 1.0 + CLAMP(fZDistance, -10.0, 100.0) * 0.125
	
	INT c = 100 + ROUND((GET_ANIMATED_SINE_VALUE(5) * 50.0))
	
	INT i
	FOR i = 0 TO iSmoothness
		FLOAT fCompleteness = 0
		INT iAlpha = c
		
		IF i > 0
			fCompleteness = (TO_FLOAT(i) / TO_FLOAT(iSmoothness))
		ENDIF
		
		//Highlight this little bit of the line if the timing is right
		IF FLOOR(sCurrentVarsStruct.fCurvedLineHighlightSegment) = i
			iAlpha = 255
		ENDIF
		IF sCurrentVarsStruct.fCurvedLineHighlightSegment > iSmoothness * 1.5
			sCurrentVarsStruct.fCurvedLineHighlightSegment = 0
		ENDIF
		
		VECTOR vPoint = LERP_VECTOR(vStartPos, vEndPos, fCompleteness)
		FLOAT fLineHeightScalar = (ABSF(fCompleteness - 0.5) * 2)
		
		FLOAT fHeight = COSINE_INTERP_FLOAT(fTargetHeight, 0, fLineHeightScalar)
		
		IF fHeight <= 0.0
			fHeight = 0.0
		ENDIF
		
		vPoint.z += fHeight
		
		IF i !=0
		AND i != iSmoothness
			DRAW_POLY_BETWEEN_POINTS(vPoint, vPrevPos, 0.1, iR, iG, iB, iAlpha)
		ENDIF
		
		IF i = iSmoothness / 2
			// We're at the halfway point
			IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
				DRAW_GENERIC_CREATOR_ON_SCREEN_TEXT(vPoint, sLabel, 0, 0.25, fLabelFloat, iLabelInt)
			ENDIF
		ENDIF
		
		vPrevPos = vPoint
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Creator Pickup and Placement
// ##### Description: Functions dealing with picking up and placing entities in the creator
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CREATE_PLACEMENT_PARTICLE_FX(BOOL bOnWater, VECTOR vParticlePos)
	BOOL bPTFXLoaded
	
	IF NOT IS_THIS_A_SURVIVAL()
		bPTFXLoaded = HAS_PTFX_ASSET_LOADED()
	ELSE
		bPTFXLoaded = HAS_NAMED_PTFX_ASSET_LOADED("scr_mp_creator")
		IF bPTFXLoaded
			USE_PARTICLE_FX_ASSET("scr_mp_creator")
		ENDIF
	ENDIF
	
	IF bPTFXLoaded 
	AND NOT g_FMMC_STRUCT.bParticlesOff
		IF bOnWater
			PRINTLN("SPLASH PARTICLE GO NOW!")
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mp_splash", vParticlePos, <<0,0,0>>)
		ELSE
			PRINTLN("DUST PARTICLE GO NOW!")
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mp_dust_cloud", vParticlePos, <<0,0,0>>)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_THE_CORONA_DISC_VALUES(CREATION_VARS_STRUCT &sCurrentVarsStruct, ENTITY_INDEX eiPassed, BOOL bBelowEntity = TRUE, BOOL bOverrideEntityCheck = FALSE, FLOAT fOffset = fDEFAULT_PLACEMENT_DISC_HEIGHT, BOOL bVisible = TRUE)
	
	PRINTLN("[Creator] UPDATE_THE_CORONA_DISC_VALUES | bBelowEntity: ", bBelowEntity, " | bOverrideEntityCheck: ", bOverrideEntityCheck, " | fOffset: ", fOffset, " | bVisible: ", bVisible)
	
	IF bVisible
		sCurrentVarsStruct.bDiscVisible = TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(eiPassed) 
		VECTOR vMin
		VECTOR vMax
		
		IF bBelowEntity = FALSE
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(eiPassed), vMin, vMax)
			sCurrentVarsStruct.vCoronaDiscPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiPassed, <<0.0, 0.0, vMax.z>>) - sCurrentVarsStruct.vCoronaPos
		ELSE
			sCurrentVarsStruct.vCoronaDiscPos = <<0.0, 0.0, fOffset>>
		ENDIF
	ELSE
		IF bOverrideEntityCheck
			sCurrentVarsStruct.vCoronaDiscPos = <<0.0, 0.0, fOffset>>
		ENDIF
	ENDIF
	
ENDPROC

FUNC CONTROL_ACTION GET_ACCEPT_BUTTON(FMMC_LOCAL_STRUCT &sFMMCdata)
	IF IS_THIS_A_RACE()
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
			PED_INDEX playerPed = PLAYER_PED_ID()
			IF IS_PED_IN_ANY_VEHICLE(playerPed)
				VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_USING(playerPed)
				IF NOT IS_VEHICLE_FUCKED_MP(viPlayerVeh)
				AND IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(viPlayerVeh))
					IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						RETURN INPUT_FRONTEND_Y
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN INPUT_FRONTEND_ACCEPT
ENDFUNC

FUNC BOOL CAN_PICK_UP_PLACED_ZONES(structFMMC_MENU_ITEMS &sFMMCmenu)

	IF IS_MISSION_CREATOR_RUNNING()
	AND NOT IS_MENU_ENTRY_MODIFIER_ACTIVE(sFMMCmenu, eFMMC_MENU_ENTRY_MODIFIER__BOUNDS)
		RETURN FALSE
	ENDIF
	
	IF DOES_ZONE_SHAPE_USE_TWO_POSITIONS(sFMMCmenu.sMenuZone.iAreaType)
	AND NOT IS_VECTOR_ZERO(sFMMCmenu.sMenuZone.vPos[0])
	AND IS_VECTOR_ZERO(sFMMCmenu.sMenuZone.vPos[1])
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PICK_UP_PLACED_ENTITY(CREATION_VARS_STRUCT& sCurrentVarsStruct, structFMMC_MENU_ITEMS &sFMMCmenu)

	IF sFMMCMenu.iSelectedEntity > -1
		// Already picked something up!
		RETURN FALSE
	ENDIF
	
	IF sCurrentVarsStruct.iHoverEntityType != sFMMCmenu.iEntityCreation
		// We're not currently looking for this entity type
		RETURN FALSE
	ENDIF
	
	SWITCH sCurrentVarsStruct.iHoverEntityType
		CASE CREATION_TYPE_FMMC_ZONE
			RETURN CAN_PICK_UP_PLACED_ZONES(sFMMCMenu)
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_CREATOR_PLACEMENT_BUTTON_PRESSED(structFMMC_MENU_ITEMS &sFMMCmenu)
	RETURN IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED_EXCLUDING_GAMEPAD(sFMMCmenu)
ENDFUNC

PROC START_PLACEMENT_COOLDOWN_TIMER(CREATION_VARS_STRUCT &sCurrentVarsStruct)
	REINIT_NET_TIMER(sCurrentVarsStruct.sPlacementCooldownTimer)
	PRINTLN("START_PLACEMENT_COOLDOWN_TIMER - Starting sPlacementCooldownTimer")
ENDPROC

PROC CLEANUP_CREATOR_DEATHMATCH_BOUNDS_BLIPS(DEATHMATCH_BOUNDS_RUNTIME_LOCAL_STRUCT& sBoundsData[])
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_DM_BOUNDS - 1
		IF DOES_BLIP_EXIST(sBoundsData[i].biBoundsBlip)
			REMOVE_BLIP(sBoundsData[i].biBoundsBlip)
		ENDIF
	ENDFOR
ENDPROC

PROC REFRESH_DEATHMATCH_BOUNDS(DEATHMATCH_BOUNDS_RUNTIME_LOCAL_STRUCT& sBoundsData)
	IF DOES_BLIP_EXIST(sBoundsData.biBoundsBlip)
		REMOVE_BLIP(sBoundsData.biBoundsBlip)
	ENDIF
	
	//Setting the bounds centre point when using the angled area bounds shape
	INT iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
	IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape = ciDMBOUNDSSHAPE_ANGLED_AREA
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[0])
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[1])
			PRINTLN("REFRESH_DEATHMATCH_BOUNDS - Updating angled area bounds centre point")
			VECTOR vMidpoint
			vMidpoint.x = (g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[0].x + g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[1].x) * 0.5
			vMidpoint.y = (g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[0].y + g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[1].y) * 0.5
			vMidpoint.z = (g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[0].z + g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[1].z) * 0.5
			FLOAT fZGround
			GET_GROUND_Z_FOR_3D_COORD(vMidpoint, fZGround)
			vMidpoint.z = fZGround
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsCentre = vMidpoint
		ENDIF
	ENDIF
ENDPROC

PROC PLACE_KOTH_HILL(structFMMC_MENU_ITEMS &sFMMCmenu, CREATION_VARS_STRUCT &sCurrentVarsStruct)

	IF g_FMMC_STRUCT.sKotHData.iNumberOfHills >= ciKotH_MAX_HILLS
	AND sFMMCmenu.iSelectedEntity = -1
		PRINTLN("[KOTH] PLACE_KOTH_HILL - Can't place a new Hill, already placed maximum!")
		EXIT
	ENDIF
	
	INT iTempNumHills
	iTempNumHills = g_FMMC_STRUCT.sKotHData.iNumberOfHills
	
	IF sFMMCmenu.iSelectedEntity = -1
		PRINTLN("[KOTH] PLACE_KOTH_HILL - Placing new Hill ", g_FMMC_STRUCT.sKotHData.iNumberOfHills, " at ", sFMMCmenu.sTempHillStruct.vHill_Center)
		g_FMMC_STRUCT.sKotHData.sHillData[g_FMMC_STRUCT.sKotHData.iNumberOfHills] = sFMMCMenu.sTempHillStruct
		g_FMMC_STRUCT.sKotHData.iNumberOfHills++
	ELSE
		PRINTLN("[KOTH] PLACE_KOTH_HILL - Placing edited Hill ", sFMMCmenu.iSelectedEntity)
		g_FMMC_STRUCT.sKotHData.sHillData[sFMMCmenu.iSelectedEntity] = sFMMCMenu.sTempHillStruct
		sFMMCmenu.iSelectedEntity = -1
	ENDIF
	
	//Remove the blip of Hill 0 so we can refresh it and give it the correct sprite
	IF iTempNumHills = 1
		IF DOES_BLIP_EXIST(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biCenterBlip)
			REMOVE_BLIP(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biCenterBlip)
		ENDIF
		
		IF DOES_BLIP_EXIST(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biAreaBlip)
			REMOVE_BLIP(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biAreaBlip)
		ENDIF
	ENDIF
	
	PLAY_SOUND_FRONTEND(-1, "PLACE_VEHICLE", GET_CREATOR_SPECIFIC_SOUND_SET()) //"PLACE_OBJECT" doesn't seem to work
	
	IF IS_HILL_TYPE_A_SHAPE(sFMMCMenu.sTempHillStruct.iHillType)
		sFMMCmenu.sTempHillStruct.fHill_Radius = CLAMP(sFMMCmenu.sTempHillStruct.fHill_Radius, cfKOTH_HILL_MIN_RADIUS, cfKOTH_HILL_MAX_RADIUS)
		sFMMCmenu.sTempHillStruct.iHillEntity = -1
		sFMMCmenu.sTempHillRuntimeLocalStruct.eiMyEntity = NULL
	ELSE
		INT iTempType = sFMMCmenu.sTempHillStruct.iHillType
		RESET_KOTH_HILL_DATA(sFMMCmenu.sTempHillStruct, FALSE)
		sFMMCmenu.sTempHillStruct.iHillType = iTempType
	ENDIF

	sCurrentVarsStruct.creationStats.bMadeAChange = TRUE
	sFMMCMenu.fKOTH_SelectionCooldown = 0.15
	INT iBoundsIndex = 0
	REFRESH_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex])
ENDPROC

PROC SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iReason)
	
	IF FMMC_IS_ROCKSTAR_DEV()
		IF iReason = bNotInArenaFail
			EXIT
		ENDIF
	ENDIF
	
	sCurrentVarsStruct.iFailPlacementReason[0] = 0
	sCurrentVarsStruct.iFailPlacementReason[1] = 0
	SET_BIT(sCurrentVarsStruct.iFailPlacementReason[GET_LONG_BITSET_INDEX(iReason)], GET_LONG_BITSET_BIT(iReason))
	PRINTLN("JT DBUG - ", sCurrentVarsStruct.iFailPlacementReason[0], " & ", sCurrentVarsStruct.iFailPlacementReason[1], " iReason(",iReason,"): ", sCurrentVarsStruct.iFailPlacementReason[GET_LONG_BITSET_INDEX(iReason)])
	
	IF sCurrentVarsStruct.iPrevFailPlacementReason != sCurrentVarsStruct.iFailPlacementReason[GET_LONG_BITSET_INDEX(iReason)] 
		PRINTLN("SET_THE_PLACEMENT_FAIL_AS_THIS_REASON has been called and it's different to the last time it was called. The reason was: ", iReason, "! Callstack coming up")
		DEBUG_PRINTCALLSTACK()
		sCurrentVarsStruct.iPrevFailPlacementReason = sCurrentVarsStruct.iFailPlacementReason[GET_LONG_BITSET_INDEX(iReason)]
	ENDIF
ENDPROC


FUNC BOOL IS_THIS_FAIL_REASON_SET(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iFailReason)
	RETURN IS_BIT_SET(sCurrentVarsStruct.iFailPlacementReason[GET_LONG_BITSET_INDEX(iFailReason)], GET_LONG_BITSET_BIT(iFailReason))
ENDFUNC

FUNC STRING GET_GENERIC_PLACEMENT_FAIL_DESCRIPTION(CREATION_VARS_STRUCT &sCurrentVarsStruct)
	IF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bShapeTestFail)
		RETURN "FMMC_ER_015"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bSlopeFail)
		RETURN "FMMC_ER_007"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bWaterFail)
		RETURN "FMMC_ER_009"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bDistFail)
		RETURN "FMMC_ER_019"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bMapFail)
		RETURN "FMMC_ER_034"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bLandFail)
		IF DOES_ENTITY_EXIST(sCurrentVarsStruct.eiCurProp)
		AND IS_MODEL_A_SEA_MINE(GET_ENTITY_MODEL(sCurrentVarsStruct.eiCurProp))
			RETURN "FMMC_ER_SM"
		ELSE
			RETURN "FMMC_ER_008"
		ENDIF
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bInDoorTriggerFail)
		RETURN "FMMC_ER_020"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bCPSpawnFail)
		RETURN "FMMC_ER_022"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bRestrictedFail)
		RETURN "FMMC_ER_024"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bSelectedFail)
		IF IS_THIS_A_SURVIVAL()
			RETURN "FMMC_ER_CPOP"
		ELSE
			RETURN "FMMC_ER_025"
		ENDIF
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bCloseToGroundFail)
		RETURN "FMMC_ER_027"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bCloseToWaterFail)
		RETURN "FMMC_ER_035"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bTooCloseToBoostRing)
		RETURN "FMMC_ER_B_RING"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bDLCLockedItem)
		RETURN "FMMC_ER_031"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bMemoryFail)
		RETURN "FMMC_ER_MEM"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bPropCantGrabForTemplate)
		RETURN "FMMC_ER_PGRB"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bTooManyPropsInImmediateArea)
		RETURN "FMMC_ER_FPROX"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bTooManyIntersectingAnimatedProps)
		RETURN "FMMC_ER_APROX"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bOverlappingProps)
		RETURN "FMMC_ER_OVLP"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bPropOverlappingTeamStartPoint)
		RETURN "FMMC_ER_POTSP"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bWarpTooFarFromDest)
		RETURN "FMMC_ER_B_WARP"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bDestTooFarFromWarp)
		RETURN "FMMC_ER_C_WARP"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bDoubleWarp)
		RETURN "FMMC_ER_D_WARP"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bOrientationLand)
		RETURN "FMMC_ER_ORIEN"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bInvalidWarpExit)
		RETURN "FMMC_ER_WRP"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct,  bDriveOnlyUnderwater)
		RETURN "FMMC_ER_DOU"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bInvalidWarpWaterVeh)
		RETURN "FMMC_ER_IWV"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bInvalidWarpAirVeh)
		RETURN "FMMC_ER_IAV"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bInvalidOffScreen)
		RETURN "FMMC_ER_NIV"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bInArenaFail)
		RETURN "FMMC_ER_IAF"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bNotInArenaFail)
		RETURN "FMMC_ER_NIA"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bNoHillsFail)
		RETURN "FMMC_ER_NHA"	
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bNotInSurvivalBounds)
		RETURN "FMMC_ER_NPA"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bNotSurvivalBoundsTooBig)
		RETURN "FMMC_ER_NCE"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bNotInRangeOfBounds)
		RETURN "FMMC_ER_NIR"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bInvalidArenaTower)
		RETURN "FMMC_ER_ITW"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bInSurvivalBounds)
		RETURN "FMMC_ER_IPA"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bBoundsTooSmall)
		RETURN "FMMC_ER_NFE"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bTooCloseToAnotherPed) 
		RETURN "FMMC_ER_APTC"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bTooCloseToAnotherVeh)
		RETURN "FMMC_ER_LVTC"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bTooManyHills)
		RETURN "KTHH_AM_TMN"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bTooCloseToTop)
		RETURN "FMMC_ER_TCTT"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bNotOnGridProp)
		RETURN "FMMC_ER_SSC"
	ELIF IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bTooManyInteractionsOfThisType)
		RETURN "MC_H_TMIOTT"
	ENDIF
	
	IF NOT sCurrentVarsStruct.bFirstShapeTestCheckDone
		RETURN "FMMC_ER_CHPL"
	ENDIF	
	IF NOT sCurrentVarsStruct.bAreaIsGoodForPlacement
		RETURN "FMMC_ER_GENE"
	ENDIF
	
	RETURN "FMMC_M0_SL"
ENDFUNC

FUNC BOOL IS_AREA_CLEAR_PLACEMENT_OVERRIDE_ACTIVE(structFMMC_MENU_ITEMS &sFMMCmenu)
	RETURN IS_BIT_SET(sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_STACK)
	AND sFMMCmenu.iEntityCreation = CREATION_TYPE_PROPS
ENDFUNC

PROC SET_AREA_GOOD_FOR_PLACEMENT(CREATION_VARS_STRUCT &sCurrentVarsStruct, BOOL bIsGoodForPlacement)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("SET_AREA_GOOD_FOR_PLACEMENT - Setting bAreaIsGoodForPlacement to ", PICK_STRING(bIsGoodForPlacement,"TRUE","FALSE"))
	sCurrentVarsStruct.bAreaIsGoodForPlacement = bIsGoodForPlacement
ENDPROC

FUNC FLOAT ADD_TO_CURRENT_HEADING(FLOAT &fAdd, BOOL bStartingGrid = FALSE, BOOL bWorld = TRUE, BOOL bForceUpdate = FALSE, BOOL bNoLimit = FALSE, BOOL bUseDpad = FALSE)
	FLOAT fScalar = 0.1
	CONTROL_ACTION caLeftInput = INPUT_FRONTEND_LB
	CONTROL_ACTION caRightInput = INPUT_FRONTEND_RB
	
	IF bUseDpad
		caLeftInput = INPUT_FRONTEND_LEFT
		caRightInput = INPUT_FRONTEND_RIGHT
	ENDIF
	
	FLOAT fLimit = 6.0
	IF bWorld OR NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	AND NOT bUseDpad
		fScalar = 1.0
		
		fLimit = 12.0
	ENDIF
	
	IF bUseDpad
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			fScalar = 8.0
			fLimit = 10.0
		ELSE
			fScalar = 0.2
			fLimit = 3.0
		ENDIF
	ENDIF
	
	IF bForceUpdate
		fScalar = 3.0
		
		fLimit = 36.0
	ENDIF
	
	IF IS_PC_VERSION()
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			fScalar = GET_FRAME_TIME() / (1.0 / 30.0)
		ELSE
			fScalar *= g_fCreatorEntityRotationAccelModifierPC	// Commandline to allow content creators to determine the acceleration of rotation of an entity when placing
		ENDIF
	ENDIF
	
	FLOAT fValue = 0.0
	
	IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL, caLeftInput) AND fAdd <= 0)
	OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, caRightInput) AND fAdd >= 0)
	OR bForceUpdate
		if fValue = 0
			IF bStartingGrid =  TRUE
				IF g_FMMC_STRUCT.fGridLength > g_FMMC_STRUCT.fGridWidth
					fValue = 15/g_FMMC_STRUCT.fGridLength
				ELSE
					fValue = 15/g_FMMC_STRUCT.fGridWidth
				ENDIF
			ELSE
				fValue = 0.5
			ENDIF
		ENDIF
		IF bStartingGrid =  TRUE
			fValue = (0.2 * fScalar)
		ELSE
			fValue = (0.2 * fScalar)
		ENDIF
	ELIF 	IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caLeftInput)
	OR 		IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caRightInput)
		fValue = 0.25
	ELSE
		fAdd = 0.0
	ENDIF
	
	IF fValue > 0.0
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, caLeftInput)
			fValue *= -1.0
		ENDIF
		fAdd += fValue
	ENDIF
	
	IF NOT bNoLimit
		fAdd = CLAMP(fAdd, -fLimit, fLimit)
	ENDIF
	
	RETURN fAdd
	
ENDFUNC

FUNC BOOL HAS_CURRENT_PROP_BEEN_FREE_ROTATED(structFMMC_MENU_ITEMS &sFMMCmenu)
	RETURN IS_BIT_SET(sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_FREE_ROTATION)
	AND (sFMMCmenu.iEntityCreation = CREATION_TYPE_PROPS OR sFMMCmenu.iEntityCreation = CREATION_TYPE_DYNOPROPS)
ENDFUNC

PROC CLEAR_PROP_FREE_ROTATION(structFMMC_MENU_ITEMS &sFMMCmenu, BOOL bForce = FALSE)
	IF IS_BIT_SET(sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_FREE_ROTATION)
	OR bForce
		CLEAR_BIT(sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_FREE_ROTATION)
		sFMMCmenu.iCurrentAxis = -1
		DEBUG_PRINTCALLSTACK()
		REFRESH_MENU(sFMMCmenu)
	ENDIF
ENDPROC

PROC DRAW_FREE_ROTATION_MARKERS(structFMMC_MENU_ITEMS &sFMMCmenu, CREATION_VARS_STRUCT &sCurrentVarsStruct, ENTITY_INDEX eiPassed)
	
	BOOL bShouldDraw = FALSE
	MODEL_NAMES eiModel = GET_ENTITY_MODEL(eiPassed)
	
	IF (sFMMCMenu.sActiveMenu = eFmmc_DYNOPROP_BASE OR sFMMCMenu.sActiveMenu = eFmmc_PROP_BASE or sFMMCMenu.sActiveMenu = eFmmc_FMC_PROP_MENU)
		IF (sFMMCmenu.iCurrentSelection = PROP_MENU_SHARED_ROTATION_TYPE AND NOT IS_MODEL_STUNT_PROP(eiModel))
		OR (sFMMCmenu.iCurrentSelection = PROP_MENU_SHARED_ROTATION_TYPE AND IS_MODEL_STUNT_PROP(eiModel))
			bShouldDraw = TRUE
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_FMC_OVERRIDE_ROTATION
	OR sFMMCMenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION
		bShouldDraw = TRUE
	ENDIF
	
	IF NOT bShouldDraw
		EXIT
	ENDIF
 
	VECTOR vMin, vMax, vDim, vDir, vRot = <<-90,0,0>>, vScale = <<1,1,1>>
	FLOAT fMakerOffset = 1.25
	GET_MODEL_DIMENSIONS(eiModel, vMin, vMax)
	vDim = vMax - vMin
	FLOAT fLargest
	VECTOR vAbs = <<ABSF(vDim.X), ABSF(vDim.Y), ABSF(vDim.Z)>>
	IF vAbs.X > vAbs.Y AND vAbs.X > vAbs.Z
		fLargest = vAbs.X
	ELIF vAbs.Y > vAbs.X AND vAbs.Y > vAbs.Z
		fLargest = vAbs.Y
	ELIF vAbs.Z > vAbs.X AND vAbs.Z > vAbs.Y
		fLargest = vAbs.Z
	ENDIF
	IF fLargest > 10.0
		FLOAT fScaleChange = (fLargest / 10.0)
		
		vScale *= fScaleChange
		fMakerOffset *= fScaleChange
	ENDIF
	
	INT iR, iG, iB, iA = 2
	GET_HUD_COLOUR(HUD_COLOUR_PURPLE, iR, iG, iB, iA)
	
	BOOL bInverted = IS_PROP_XY_INVERTED(eiModel)
	
	IF (sFMMCmenu.iCurrentAxis = FREE_ROTATION_PITCH AND NOT bInverted) OR (sFMMCmenu.iCurrentAxis = FREE_ROTATION_ROLL AND bInverted)
		vDir = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiPassed, <<1,0,0>>) - GET_ENTITY_COORDS(eiPassed)
		DRAW_MARKER(MARKER_CONE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiPassed, <<vMax.X + fMakerOffset, 0, 0>>), 	vDir,	vRot, vScale, iR, iG, iB, iA)
		DRAW_MARKER(MARKER_CONE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiPassed, <<vMin.X - fMakerOffset, 0, 0>>), 	-vDir, 	vRot, vScale, iR, iG, iB, iA)
	ELIF (sFMMCmenu.iCurrentAxis = FREE_ROTATION_ROLL AND NOT bInverted) OR (sFMMCmenu.iCurrentAxis = FREE_ROTATION_PITCH AND bInverted)
		vDir = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiPassed, <<0,1,0>>) - GET_ENTITY_COORDS(eiPassed)
		DRAW_MARKER(MARKER_CONE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiPassed, <<0, vMax.Y + fMakerOffset, 0>>), 	vDir, 	vRot, vScale, iR, iG, iB, iA)
		DRAW_MARKER(MARKER_CONE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiPassed, <<0, vMin.Y - fMakerOffset, 0>>), 	-vDir, 	vRot, vScale, iR, iG, iB, iA)
	ELIF (sFMMCmenu.iCurrentAxis = FREE_ROTATION_YAW)
		vDir = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiPassed, <<0,0,1>>) - GET_ENTITY_COORDS(eiPassed)
		DRAW_MARKER(MARKER_CONE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiPassed, <<0, 0, vMax.Z + fMakerOffset>>), 	vDir, 	vRot, vScale, iR, iG, iB, iA)
		DRAW_MARKER(MARKER_CONE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiPassed, <<0, 0, vMin.Z - fMakerOffset>>), 	-vDir, 	vRot, vScale, iR, iG, iB, iA)
	ELIF (sFMMCmenu.iCurrentAxis = FREE_ROTATION_HEADING) OR (sFMMCmenu.iCurrentAxis = FREE_ROTATION_NORMAL)
		sCurrentVarsStruct.fCreationHeading = sFMMCmenu.fCurrentAxisRotation
	ENDIF
ENDPROC

PROC DETACH_ENTITY_FROM_ROTATION_ANCHOR(structFMMC_MENU_ITEMS &sFMMCMenu, ENTITY_INDEX eiPassed, OBJECT_INDEX& eiRotationAnchor)
	
	UNUSED_PARAMETER(sFMMCMenu)
	
	IF DOES_ENTITY_EXIST(eiPassed)
		IF IS_ENTITY_ATTACHED(eiPassed)
			DETACH_ENTITY(eiPassed, FALSE)
			FREEZE_ENTITY_POSITION(eiPassed, TRUE)
			SET_ENTITY_COLLISION(eiPassed, FALSE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(eiRotationAnchor)
		DELETE_OBJECT(eiRotationAnchor)
		PRINTLN("DETACH_ENTITY_FROM_ROTATION_ANCHOR | Deleting rotation anchor")
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC

PROC CHANGE_FREE_ROTATION_AXIS(structFMMC_MENU_ITEMS &sFMMCmenu, INT iChangeValue, OBJECT_INDEX eiPassed)
	
	IF NOT IS_BIT_SET(sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_FREE_ROTATION)
		SET_BIT(sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_FREE_ROTATION)
		IF sFMMCMenu.iCurrentAxis = FREE_ROTATION_NORMAL
		AND iChangeValue < 0
			sFMMCMenu.iCurrentAxis = FREE_ROTATION_HEADING
		ENDIF
	ENDIF
	
	sFMMCmenu.iCurrentAxis += iChangeValue
	
	DETACH_ENTITY_FROM_ROTATION_ANCHOR(sFMMCMenu, eiPassed, sFMMCmenu.oiFreeRotationAnchor)
	
	IF sFMMCMenu.iCurrentAxis > FREE_ROTATION_HEADING
		sFMMCmenu.iCurrentAxis = FREE_ROTATION_PITCH
	ELIF sFMMCMenu.iCurrentAxis < FREE_ROTATION_PITCH
		sFMMCMenu.iCurrentAxis = FREE_ROTATION_HEADING
	ENDIF
	
	IF DOES_ENTITY_EXIST(eiPassed)
		IF SHOULD_PROP_NOT_ROLL(GET_ENTITY_MODEL(eiPassed))
			IF sFMMCMenu.iCurrentAxis = FREE_ROTATION_ROLL
				sFMMCmenu.iCurrentAxis += iChangeValue
			ENDIF
		ENDIF
	ENDIF
	
	sFMMCMenu.fCurrentAxisRotation = 0.0
	PRINTLN("url:bugstar:5249289 - Setting sFMMCMenu.fCurrentAxisRotation 10")
	
	PLAY_EDIT_MENU_ITEM_SOUND()
	
ENDPROC

PROC CHANGE_2D_ROTATION_AXIS(structFMMC_MENU_ITEMS &sFMMCmenu)
	
	IF sFMMCMenu.iCurrentAxis <> FREE_ROTATION_PITCH
		sFMMCMenu.iCurrentAxis = FREE_ROTATION_PITCH
	ELSE
		sFMMCMenu.iCurrentAxis = FREE_ROTATION_HEADING
	ENDIF
	
	sFMMCMenu.fCurrentAxisRotation = 0.0
	PRINTLN("url:bugstar:5249289 - Setting sFMMCMenu.fCurrentAxisRotation 9")
	
ENDPROC

FUNC BOOL CAN_PROP_ROTATION_BE_RESET(structFMMC_MENU_ITEMS &sFMMCmenu, CREATION_VARS_STRUCT &sCurrentVarsStruct)

	IF IS_MENU_ITEM_FLAG_SET_ON_CURRENT_SELECTED_OPTION(sFMMCmenu, ciMENU_ITEM_FLAG__ADJUST_FINE)
	OR IS_MENU_ITEM_FLAG_SET_ON_CURRENT_SELECTED_OPTION(sFMMCmenu, ciMENU_ITEM_FLAG__ADJUST_FAST)
	OR IS_MENU_ITEM_FLAG_SET_ON_CURRENT_SELECTED_OPTION(sFMMCmenu, ciMENU_ITEM_FLAG__ADJUST_BOTH)
		RETURN FALSE
	ENDIF
	
	RETURN HAS_CURRENT_PROP_BEEN_FREE_ROTATED(sFMMCmenu)
	AND NOT DOES_ENTITY_EXIST(sCurrentVarsStruct.vCoronaHitEntity)
ENDFUNC

PROC ROTATE_ENTITY_USING_ANCHOR(structFMMC_MENU_ITEMS &sFMMCmenu, CREATION_VARS_STRUCT &sCurrentVarsStruct, ENTITY_INDEX eiPassed, OBJECT_INDEX& eiRotationAnchor, INT iAxis = FREE_ROTATION_PITCH, BOOL bSelectedSomething = FALSE, BOOL bSnappingObj = FALSE,  BOOL bForceUpdate = FALSE, FLOAT fBonePosX = 0.0, FLOAT fBonePosY = 0.0, FLOAT fBonePosZ = 0.0, FLOAT fRotate = 0.0, INT iSnappedRotationIncrement = 0, FLOAT fAddThisRotation = 0.0, BOOL bTemplate = FALSE)
	
	VECTOR vRotPos
	
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		EXIT
	ENDIF
	
	BOOL bDoingLocalOverrideRotation = CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu) AND sFMMCmenu.iCurrentAlignment_Rotation = FREE_POSITION_LOCAL
	IF IS_FREEMODE_CREATOR()
		bDoingLocalOverrideRotation = IS_BIT_SET(sFMMCMenu.iCreatorInfoBitset, ciCIB_ROTATING_OVERRIDE_ROTATION_PROP)
	ENDIF
	CLEAR_BIT(sFMMCMenu.iCreatorInfoBitset, ciCIB_ROTATING_OVERRIDE_ROTATION_PROP)
	
	IF bSnappingObj
		vRotPos = <<fBonePosX, fBonePosY, fBonePosZ>>
		
	ELIF DOES_ENTITY_EXIST(eiPassed)
		vRotPos = GET_ENTITY_COORDS(eiPassed)
	ELIF CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
		vRotPos = sFMMCmenu.vOverridePosition
	ELSE
		vRotPos = sCurrentVarsStruct.vCoronaPos
	ENDIF
	
	IF (sFMMCmenu.iCurrentAxis >= 0	AND (sFMMCmenu.iEntityCreation = CREATION_TYPE_PROPS OR sFMMCmenu.iEntityCreation = CREATION_TYPE_DYNOPROPS))
	OR bDoingLocalOverrideRotation
	OR bSnappingObj
		
		IF NOT bSelectedSomething
		AND NOT bDoingLocalOverrideRotation
			DRAW_FREE_ROTATION_MARKERS(sFMMCMenu, sCurrentVarsStruct, eiPassed)
		ENDIF
		
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		OR bForceUpdate
		OR bDoingLocalOverrideRotation
			
			IF NOT DOES_ENTITY_EXIST(eiRotationAnchor)
				REQUEST_MODEL(PROP_LD_TEST_01)
				IF HAS_MODEL_LOADED(PROP_LD_TEST_01)
					PRINTLN("ROTATE_ENTITY_USING_ANCHOR | Creating rotation anchor")
					eiRotationAnchor = CREATE_OBJECT(PROP_LD_TEST_01, vRotPos)
					
					IF sFMMCmenu.iCurrentAxis != FREE_ROTATION_HEADING
					OR bSnappingObj
						SET_ENTITY_ROTATION(eiRotationAnchor, GET_ENTITY_ROTATION(eiPassed))
					ENDIF
					
					SET_ENTITY_COLLISION(eiRotationAnchor, FALSE)
					FREEZE_ENTITY_POSITION(eiRotationAnchor, TRUE)
					IF sFMMCmenu.iCurrentAxis != FREE_ROTATION_HEADING
					OR bSnappingObj
						ATTACH_ENTITY_TO_ENTITY(eiPassed, eiRotationAnchor, 0, EMPTY_VEC(), EMPTY_VEC())
					ELSE
						ATTACH_ENTITY_TO_ENTITY(eiPassed, eiRotationAnchor, 0, EMPTY_VEC(), GET_ENTITY_ROTATION(eiPassed))
					ENDIF
					sFMMCmenu.fCurrentAxisRotation = 0.0
					PRINTLN("ROTATE_ENTITY_USING_ANCHOR - Setting sFMMCMenu.fCurrentAxisRotation 8")
					
				ENDIF				
			ELSE
				
				SET_ENTITY_COORDS(eiRotationAnchor, vRotPos)

				IF iSnappedRotationIncrement = 0
					IF NOT bForceUpdate
						IF NOT bDoingLocalOverrideRotation
							sFMMCmenu.fCurrentAxisRotation -= ADD_TO_CURRENT_HEADING(sCurrentVarsStruct.fRotationVeloc, DEFAULT, (sFMMCmenu.iCurrentAxis = -1), bForceUpdate, DEFAULT, IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override))
							PRINTLN("ROTATE_ENTITY_USING_ANCHOR - heading - sFMMCmenu.fCurrentAxisRotation: ", sFMMCmenu.fCurrentAxisRotation)
						ELSE							
							EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sCurrentVarsStruct.bResetEntityCreationMenuASAP, sFMMCmenu, sFMMCmenu.fCurrentAxisRotation, -5000.0, 5000.0, FALSE, 0.1, 50.0)
							PRINTLN("ROTATE_ENTITY_USING_ANCHOR - bDoingLocalOverrideRotation edit - sFMMCmenu.fCurrentAxisRotation: ", sFMMCmenu.fCurrentAxisRotation)
						ENDIF
						PRINTLN("ROTATE_ENTITY_USING_ANCHOR - Setting sFMMCMenu.fCurrentAxisRotation 10")
					ELIF fAddThisRotation = 0.0
						sFMMCmenu.fCurrentAxisRotation = fRotate
						PRINTLN("ROTATE_ENTITY_USING_ANCHOR - Setting sFMMCMenu.fCurrentAxisRotation 7 fRotate: ", fRotate)
					ELSE
						sFMMCmenu.fCurrentAxisRotation -= fAddThisRotation
						PRINTLN("ROTATE_ENTITY_USING_ANCHOR - Setting sFMMCMenu.fCurrentAxisRotation 9 fAddThisRotation: ", fAddThisRotation)
					ENDIF
					IF NOT bDoingLocalOverrideRotation
						IF sFMMCmenu.fCurrentAxisRotation > 179.9
							sFMMCmenu.fCurrentAxisRotation = -179.9
							PRINTLN("ROTATE_ENTITY_USING_ANCHOR - Setting sFMMCMenu.fCurrentAxisRotation 6")
						ELIF sFMMCmenu.fCurrentAxisRotation < -179.9
							sFMMCmenu.fCurrentAxisRotation = 179.9
							PRINTLN("ROTATE_ENTITY_USING_ANCHOR - Setting sFMMCMenu.fCurrentAxisRotation 5")
						ENDIF
					ENDIF
				ELSE
					//For when the user wants to rotate by certain amount (e.g. 45 degrees) each time they press a shoulder button
					FLOAT fSnappedRotationIncrement = TO_FLOAT(iSnappedRotationIncrement)
					PRINTLN("ROTATE_ENTITY_USING_ANCHOR - snap rotating by ", fSnappedRotationIncrement, " degrees")
					PRINTLN("ROTATE_ENTITY_USING_ANCHOR - ANGLE BEFORE SNAP ROTATING: ", sFMMCmenu.fCurrentAxisRotation, " degrees")
					sFMMCmenu.fCurrentAxisRotation -= ADD_TO_CURRENT_HEADING(fSnappedRotationIncrement, DEFAULT, (sFMMCmenu.iCurrentAxis = -1), FALSE, TRUE)
					PRINTLN("ROTATE_ENTITY_USING_ANCHOR - ANGLE NOW: ", sFMMCmenu.fCurrentAxisRotation, " degrees")
					sFMMCmenu.fCurrentAxisRotation = TO_FLOAT(ROUND(sFMMCmenu.fCurrentAxisRotation))
					PRINTLN("ROTATE_ENTITY_USING_ANCHOR - ANGLE AFTER ROUNDING: ", sFMMCmenu.fCurrentAxisRotation, " degrees")
					
					//To prevent the sFMMCmenu.fCurrentAxisRotation from potentially going too high/too low if the player just keeps rotating forever
					IF sFMMCmenu.fCurrentAxisRotation > 5000
						sFMMCmenu.fCurrentAxisRotation = 0
						PRINTLN("ROTATE_ENTITY_USING_ANCHOR - Setting sFMMCMenu.fCurrentAxisRotation 4")
					ELIF sFMMCmenu.fCurrentAxisRotation < -5000
						sFMMCmenu.fCurrentAxisRotation = 0
						PRINTLN("ROTATE_ENTITY_USING_ANCHOR - Setting sFMMCMenu.fCurrentAxisRotation 3")
					ENDIF
				ENDIF

				VECTOR vAttachRotate = EMPTY_VEC()
				MODEL_NAMES mnEntity = GET_ENTITY_MODEL(eiPassed)
				BOOL bInverted = (IS_PROP_XY_INVERTED(mnEntity) AND NOT sFMMCmenu.bPropSnapped)
				
				IF bDoingLocalOverrideRotation
					//sFMMCmenu.fCurrentAxisRotation = TO_FLOAT(FLOOR(sFMMCmenu.fCurrentAxisRotation * 10.0)) / 10.0 //Commented out it was causing rotation issues // url:bugstar:5143525
				ENDIF
				
				IF (iAxis = FREE_ROTATION_PITCH AND NOT bInverted) OR (iAxis = FREE_ROTATION_ROLL AND bInverted)
					vAttachRotate = <<sFMMCmenu.fCurrentAxisRotation, 0, 0>>
				ELIF (iAxis = FREE_ROTATION_ROLL AND NOT bInverted) OR (iAxis = FREE_ROTATION_PITCH AND bInverted)
					vAttachRotate = <<0, sFMMCmenu.fCurrentAxisRotation, 0>>
				ELIF (iAxis = FREE_ROTATION_YAW)
					vAttachRotate = <<0, 0, sFMMCmenu.fCurrentAxisRotation>>
				ELIF (iAxis = FREE_ROTATION_HEADING)
					vAttachRotate = <<0, 0, sFMMCmenu.fCurrentAxisRotation>>
				ENDIF
				PRINTLN("ROTATE_ENTITY_USING_ANCHOR - sFMMCmenu.fCurrentAxisRotation: ", sFMMCmenu.fCurrentAxisRotation)
				
				IF iAxis != FREE_ROTATION_HEADING
				OR bSnappingObj
					DETACH_ENTITY(eiPassed, FALSE, TRUE)
					ATTACH_ENTITY_TO_ENTITY(eiPassed, eiRotationAnchor, 0, EMPTY_VEC(), vAttachRotate)
					
					IF bDoingLocalOverrideRotation
						SET_OVERRIDE_ROTATION_FROM_ENTITY(sFMMCMenu, eiPassed)				
					ENDIF
				ELSE
					SET_ENTITY_ROTATION(eiRotationAnchor, <<0, 0, sFMMCmenu.fCurrentAxisRotation>>)
				ENDIF
			ENDIF
		ELSE
			sCurrentVarsStruct.fRotationVeloc = 0.0
			DETACH_ENTITY_FROM_ROTATION_ANCHOR(sFMMCmenu, eiPassed, eiRotationAnchor)
		ENDIF
	ELSE
		DETACH_ENTITY_FROM_ROTATION_ANCHOR(sFMMCmenu, eiPassed, eiRotationAnchor)
		SET_ENTITY_HEADING(eiPassed, GET_ENTITY_HEADING(eiPassed))
		sFMMCmenu.fCurrentAxisRotation = 0.0
		IF bTemplate
			sCurrentVarsStruct.bClearedRotationPoint = TRUE
			PRINTLN("ROTATE_ENTITY_USING_ANCHOR - Template setting sCurrentVarsStruct.bClearedRotationPoint ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(eiPassed)))
		ENDIF
		PRINTLN("url:bugstar:5249289 - Setting sFMMCMenu.fCurrentAxisRotation 2")
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
	AND CAN_PROP_ROTATION_BE_RESET(sFMMCmenu, sCurrentVarsStruct)
		CLEAR_PROP_FREE_ROTATION(sFMMCmenu)
		REFRESH_MENU(sFMMCmenu)
	ENDIF
	
ENDPROC

PROC MOVE_ENTITY(ENTITY_INDEX eiPassed, VECTOR &vCoord, FLOAT &fHead, CREATION_VARS_STRUCT &sCurrentVarsStruct, FMMC_LOCAL_STRUCT &sFMMCdata, structFMMC_MENU_ITEMS &sFMMCmenu, FLOAT fZmovement = 0.0, BOOL bCanBeUnderwater = FALSE, BOOL bPickup = FALSE, BOOL bFreeRotate = FALSE, BOOL bRotationOverriden = FALSE)
	
	FLOAT fTemp
	UNUSED_PARAMETER(sFMMCdata)
	VECTOR vTemp = <<0, 0, -1000.0>>
	BOOL bStickToEntityListPos = (IS_BIT_SET(sFMMCMenu.iFMMCMenuBitset, ciFMMCMenuBS_EditingFromEntityList) AND sFMMCMenu.iSelectedEntity > -1)
	
	IF CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
		sCurrentVarsStruct.vCoronaPos = sFMMCmenu.vOverridePosition
		sFMMCmenu.vOffsetPosition = <<0,0,0>>
		fZmovement = 0.0
		
	ELIF bStickToEntityListPos
		sCurrentVarsStruct.vCoronaPos = sFMMCMenu.vEditFromEntityListPos
		sFMMCmenu.vOffsetPosition = <<0,0,0>>
		fZmovement = 0.0	
	ENDIF

	IF eiPassed != NULL
	
		IF sCurrentVarsStruct.bCoronaOnWater = FALSE
		OR bCanBeUnderwater
		OR bStickToEntityListPos
		OR (IS_CORONA_UNDERWATER() AND IS_UNDERWATER_PLACEMENT_ALLOWED(sFMMCmenu))
		OR CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
			vCoord = sCurrentVarsStruct.vCoronaPos
			
		ELSE
			
			IF GET_WATER_HEIGHT_NO_WAVES(sCurrentVarsStruct.vCoronaPos + <<0.0, 0.0, 2.0>> ,fTemp)
				//Set it's coordinates
				vTemp 	=  <<sCurrentVarsStruct.vCoronaPos.x, sCurrentVarsStruct.vCoronaPos.y, fTemp>> + <<0.0, 0.0, 0.2>>		
				vCoord = vTemp
			ELSE
				vCoord = sCurrentVarsStruct.vCoronaPos
			ENDIF
			
		ENDIF
		
		vCoord += <<0.0, 0.0, fZmovement>>
		
		IF IS_PUBLIC_ARENA_CREATOR()
			IF vCoord.z > 200
				vCoord.z = 200
				PRINTLN("[TMS][ARENA] Capping vertical height of moving entity")
			ENDIF
		ENDIF
		
		IF vCoord.x < 16000
		AND vCoord.y < 16000
			
			IF IS_ENTITY_AN_OBJECT(eiPassed)
			
				IF bPickup
				AND NOT bFreeRotate
					SET_ENTITY_COORDS(eiPassed, sCurrentVarsStruct.vCoronaPos + <<0.0, 0.0, fZmovement>>)
					IF IS_BIT_SET(sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_POSITION_OFFSET)	
						SET_ENTITY_COORDS(eiPassed, sCurrentVarsStruct.vCoronaPos + sFMMCmenu.vOffsetPosition + <<0.0, 0.0, fZmovement>>)
					ENDIF
				ELSE
					SET_ENTITY_COORDS(eiPassed, vCoord)
					IF IS_BIT_SET(sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_POSITION_OFFSET)
						PRINTLN("sFMMCmenu.vOffsetPosition = <<", sFMMCmenu.vOffsetPosition.X, ", ", sFMMCmenu.vOffsetPosition.Y, ", ", sFMMCmenu.vOffsetPosition.Z, ">>")
						SET_ENTITY_COORDS(eiPassed, sCurrentVarsStruct.vCoronaPos + sFMMCmenu.vOffsetPosition + <<0.0, 0.0, fZmovement>>)
					ENDIF
				ENDIF
				
			ELIF IS_ENTITY_A_PED(eiPassed)
			
				SET_ENTITY_COORDS(eiPassed, vCoord)
				
			ELSE
			
				IF bPickup
					SET_ENTITY_COORDS(eiPassed, sCurrentVarsStruct.vCoronaPos + <<0.0, 0.0, fZmovement>>)	
				ELSE
					SET_ENTITY_COORDS(eiPassed, vCoord)	
				ENDIF
				
			ENDIF
			
			IF IS_ENTITY_A_VEHICLE(eiPassed)
			
				SET_VEHICLE_ENGINE_HEALTH(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiPassed), 1000.0)
				SET_ENTITY_HEALTH(eiPassed, 1000)
				
			ENDIF
			
			IF NOT CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu)
				IF (NOT bFreeRotate AND NOT IS_ENTITY_ATTACHED(eiPassed) AND NOT bRotationOverriden AND NOT (IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround) AND IS_SPECIAL_VEHICLE_THRUSTER_RACE()))
				OR sFMMCmenu.bForceRotate
					
					SET_ENTITY_HEADING(eiPassed, fHead)
				
					IF sFMMCMenu.bRuleMenuActive	 = FALSE
						FLOAT fNewRot = ADD_TO_CURRENT_HEADING(sCurrentVarsStruct.fRotationVeloc, DEFAULT, DEFAULT, sFMMCmenu.bForceRotate)
						IF fNewRot != 0.0					
							fHead 		= GET_ENTITY_HEADING(eiPassed) - fNewRot					
							sCurrentVarsStruct.fCreationHeading 	= fHead
							SET_ENTITY_HEADING(eiPassed, fHead)
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
	ELSE
		
		IF GET_WATER_HEIGHT_NO_WAVES(sCurrentVarsStruct.vCoronaPos ,fTemp)
		AND NOT (IS_CORONA_UNDERWATER() AND IS_UNDERWATER_PLACEMENT_ALLOWED(sFMMCmenu))
			vTemp 	=  <<sCurrentVarsStruct.vCoronaPos.x, sCurrentVarsStruct.vCoronaPos.y, fTemp>> + <<0.0, 0.0, fZmovement>>
		ENDIF
		
		IF sCurrentVarsStruct.vCoronaPos.z > vTemp.z
			vCoord = sCurrentVarsStruct.vCoronaPos
		ELSE
			vCoord = vTemp
		ENDIF
		
		fHead -= ADD_TO_CURRENT_HEADING(sCurrentVarsStruct.fRotationVeloc)
		IF fHead > 360.0
			fHead = 0.0
		ELIF fHead < 0.0 
			fHead = 360.0
		ENDIF
		
		sCurrentVarsStruct.fCreationHeading = fHead
		
		PRINTLN("fHeading is " , fHead)
		
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CURRENT_MENU_ACTION_BLOCK_PLACEMENT(structFMMC_MENU_ITEMS &sFMMCmenu)
	
	// Put special case actions that should allow placement here
	IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_KotH_PLACE_HILL
	OR sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Public_Zone_Creation	
		RETURN FALSE
	ENDIF
	
	RETURN sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
ENDFUNC

FUNC BOOL IS_PLACEMENT_COOLDOWN_RUNNING(CREATION_VARS_STRUCT &sCurrentVarsStruct)
	IF HAS_NET_TIMER_STARTED(sCurrentVarsStruct.sPlacementCooldownTimer)
		IF HAS_NET_TIMER_EXPIRED(sCurrentVarsStruct.sPlacementCooldownTimer, 250)
		OR (HAS_NET_TIMER_EXPIRED(sCurrentVarsStruct.sPlacementCooldownTimer, 50) AND FMMC_IS_ROCKSTAR_DEV())
			RESET_NET_TIMER(sCurrentVarsStruct.sPlacementCooldownTimer)
			PRINTLN("IS_PLACEMENT_COOLDOWN_RUNNING || resetting sPlacementCooldownTimer")
		ELSE
			PRINTLN("IS_PLACEMENT_COOLDOWN_RUNNING || returning TRUE due to sPlacementCooldownTimer")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLACE_BUTTON_BE_PRESSED(structFMMC_MENU_ITEMS &sFMMCmenu, CREATION_VARS_STRUCT &sCurrentVarsStruct)
	
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= MAX_MENU_ITEMS
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenPopupValidThisFrame)	
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADING_IN()
	OR NOT IS_SCREEN_FADED_IN()
		PRINTLN("CAN_PLACE_BUTTON_BE_PRESSED returning FALSE due to screen fading")
		RETURN FALSE
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_SWITCH_CAMERA_BUTTON())
		PRINTLN("CAN_PLACE_BUTTON_BE_PRESSED returning FALSE due to pressing switch button")
		RETURN FALSE
	ENDIF
		
	IF IS_PLACEMENT_COOLDOWN_RUNNING(sCurrentVarsStruct)
		PRINTLN("CAN_PLACE_BUTTON_BE_PRESSED || returning FALSE due to IS_PLACEMENT_COOLDOWN_RUNNING")
		RETURN FALSE
	ENDIF
	
	IF JUST_ENTERED_MENU(sFMMCMenu)
		PRINTLN("CAN_PLACE_BUTTON_BE_PRESSED || returning FALSE due to JUST_ENTERED_MENU")
		RETURN FALSE
	ENDIF
	
	IF sFMMCmenu.bButtonDelay
		sFMMCmenu.bButtonDelay = FALSE
		PRINTLN("CAN_PLACE_BUTTON_BE_PRESSED || returning FALSE due to sFMMCmenu.bButtonDelay")
		RETURN FALSE
	ENDIF
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = -1
		PRINTLN("CAN_PLACE_BUTTON_BE_PRESSED || returning FALSE due to sFMMCmenu.bButtonDelay")
		RETURN FALSE
	ENDIF
	
	IF sFMMCMenu.bRuleMenuActive	 = TRUE
		PRINTLN("CAN_PLACE_BUTTON_BE_PRESSED || returning FALSE because we're in the right-hand menu")
		RETURN FALSE
	ENDIF
	
	IF SHOULD_CURRENT_MENU_ACTION_BLOCK_PLACEMENT(sFMMCmenu)
		PRINTLN("CAN_PLACE_BUTTON_BE_PRESSED || returning FALSE because the selected option has an assigned menu action")
		RETURN FALSE
	ENDIF
	
	IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
	AND NOT IS_DELETE_BUTTON_AS_GO_TO_MENU(sFMMCMenu)
	AND NOT (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_RT_BUTTON()) AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_LT_BUTTON()))
		PRINTLN("CAN_PLACE_BUTTON_BE_PRESSED || returning FALSE because the selected option has an assigned menu go-to (", sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)], ")")
		RETURN FALSE
	ENDIF
	
	IF (sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU AND sFMMCmenu.iEntityCreation = CREATION_TYPE_PEDS AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_COVER_FROM_POINT OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_COVER_POINT))
	OR (sFMMCmenu.sActiveMenu = eFmmc_ACTOR_DIALOGUE_MENU AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ACTOR_DIALOGUE_WIDTH OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ACTOR_DIALOGUE_MIN_HEIGHT OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ACTOR_DIALOGUE_MAX_HEIGHT))
	OR (sFMMCmenu.sActiveMenu = eFmmc_ENEMY_ASS_RULE_ACTION_ACTIVATION_MENU AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ACTION_ACTIVATION_AA_WIDTH OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ACTION_ACTIVATION_AA_MIN_HEIGHT OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ACTION_ACTIVATION_AA_MAX_HEIGHT))
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_EXTRAS
	OR sFMMCmenu.sActiveMenu = eFmmc_ENEMY_STOP_TASKS_MENU
	OR (sFMMCmenu.sActiveMenu = eFMMC_MISSION_BRANCHING AND sFMMCmenu.iEntityCreation != CREATION_TYPE_OBJECTS)
	OR sFMMCmenu.sActiveMenu = eFmmc_ALL_TEAM_AT_LOCATE_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_LINE_MARKERS_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_LOCATION_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_MANIPULATE_TIME_REMAINING_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_PED_FOLLOW_TEAM_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_PED_FOLLOW_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_TRANSFORM_VEHICLE_CUSTOM
	OR (sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iMenuLengthForSnappingOptions)
	OR (sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS AND (FMMC_IS_ROCKSTAR_DEV() AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_DELETE_BUTTON())))
	OR (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPreviewSoundPos)
	OR (sFMMCmenu.sActiveMenu = eFMMC_GENERIC_OVERRIDE_POSITION AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_RESET))
	OR (sFMMCmenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_RESET))
	OR (sFMMCmenu.sActiveMenu = eFMMC_DUMMYBLIP_OVERRIDE_POSITION AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_DUMMYBLIP_OVERRIDE_USE_OVERRIDE))
	OR (sFMMCmenu.sActiveMenu = eFmmc_SOUND_TRIGGER_OPTIONS AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SND_PROP_OPT_SND_PREVIEW) // preview sound option
	OR (sFMMCmenu.sActiveMenu = eFmmc_CHECKPOINT_ASS_VECTOR AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CHECKPOINT_ASS_VECTOR_CLEAR)
	OR (sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PROP_UPDATE_ROTATION_CLEAR)
	OR (sFMMCmenu.sActiveMenu = efmmc_PED_HELI_ESCORT_OFFSET AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_HELI_ESCORT_OFFSET_CLEAR)
	OR sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Pickup_Placement_Choose_Weapon
	OR (sFMMCmenu.sActiveMenu != eFmmc_KotH_HILLS AND DOING_HILL_VEHICLE_CREATION(sFMMCmenu))
	OR (sFMMCmenu.sActiveMenu != eFmmc_KotH_HILLS AND DOING_HILL_OBJECT_CREATION(sFMMCmenu))
	OR (sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS AND DOING_HILL_SHAPE_CREATION(sFMMCmenu) AND g_FMMC_STRUCT.sKotHData.iNumberOfHills >= ciKotH_MAX_HILLS AND sFMMCMenu.iSelectedEntity = -1)
	OR sFMMCmenu.sActiveMenu = eFmmc_VEH_WARP_LOCATIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_WARP_LOCATION_SETTINGS
	OR sFMMCmenu.sActiveMenu = eFmmc_PED_WARP_LOCATIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_OBJ_WARP_LOCATIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_INTERACTABLE_WARP_LOCATIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_LOC_WARP_LOCATIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_MISSION_LOC_ADDITIONAL_SPAWNING_SUBMENU
	OR sFMMCMenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_SETTINGS
	OR sFMMCMenu.sActiveMenu = eFMMC_DEFINE_SPAWN_GROUP_ACTIVATION
	OR sFMMCMenu.sActiveMenu = eFmmc_ENEMY_DISABLE_PED_BEING_TARGETABLE_ON_RULE_OPTIONS
	OR sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_KINEMATIC_PHYSICS_OPTIONS
	OR (sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_ROTATION AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_RESET))
	OR (sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_POSITION AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_RESET))
	OR (sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_POSITION AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_USE_OVERRIDE))
	OR (sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_ROTATION AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_USE_OVERRIDE))
	OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_CLEAR_DATA
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_RADIO
	OR IS_MENU_A_CUSTOM_VARIABLE_MENU(sFMMCmenu.sActiveMenu)
	OR IS_MENU_A_ENTITY_PTFX_MENU(sFMMCmenu)
	OR ((GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_LOAD_INDEX OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_SAVE_INDEX) AND sFMMCMenu.sActiveMenu = eFmmc_PED_StealthSystem)	
		PRINTLN("CAN_PLACE_BUTTON_BE_PRESSED || returning FALSE due to sFMMCmenu.sActiveMenu")
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Default Setters
// ##### Description: Fucntions to set the default state for globals when creating new content
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_NEW_2020_PED_DEFAULTS(structFMMC_MENU_ITEMS &sFMMCmenu, BOOL bClearAllSettings)

	IF bClearAllSettings
		sFMMCMenu.iWepHolstered = 1
		sFMMCMenu.fPedRange = cfMIN_PATROL_RANGE
		sFMMCMenu.iRespawnDelay = 3
	ENDIF
	
	CLEAR_ALL_PED_ASSOCIATED_GOTO_DATA(sFMMCmenu.sAssociatedGotoTaskData)
				
	SET_BIT(sFMMCmenu.iActorMenuBitsetThree, ciPED_BSThree_DontCombatOnActivationRange)
	CLEAR_BIT(sFMMCMenu.iActorMenuBitset, ciPED_BS_ExtendedLOS)
	SET_BIT(sFMMCMenu.iActorMenuBitsetFive, ciPED_BSFive_ExtendLOSOnCombat)
	SET_BIT(sFMMCmenu.iActorMenuBitsetSeven, ciPED_BSSeven_DisableDislikeAsHateWhenInCombat)
	SET_BIT(sFMMCmenu.iActorMenuBitsetThirteen, ciPed_BSThirteen_DisableCowering)
	SET_BIT(sFMMCMenu.iActorMenuBitset, ciPED_BS_DontFleeDropOffs)
	SET_BIT(sFMMCMenu.iActorMenuBitset, ciPED_BS_GoToOnFoot)
	SET_BIT(sFMMCMenu.iActorMenuBitset, ciPED_BS_PedDoesntCleanUpOnDelivery)
	SET_BIT(sFMMCMenu.iActorMenuBitsetSix,  ciPED_BSSix_DefensiveAreaMovesWithGotos)
	SET_BIT(sFMMCMenu.iActorMenuBitsetTwo, ciPED_BSTwo_AggroDoesntFail)
	SET_BIT(sFMMCMenu.iActorMenuBitsetThree, ciPED_BSThree_AggroFailHasDelay)
	SET_BIT(sFMMCMenu.iActorMenuBitsetEleven, ciPED_BSEleven_UsePerceptionConeForAimedAtEvents)
	
ENDPROC

PROC SET_NEW_2020_VEH_DEFAULTS(structFMMC_MENU_ITEMS &sFMMCmenu, BOOL bClearAllSettings)

	IF bClearAllSettings
		
	ENDIF
	
	SET_BIT(sFMMCMenu.iVehBitSet, ciFMMC_VEHICLE_DONTCLEANUPONDELIVER)
ENDPROC

PROC SET_NEW_2020_PROP_DEFAULTS(BOOL bClearAllSettings)
	IF bClearAllSettings
		
	ENDIF
	
ENDPROC

PROC SET_NEW_2020_OBJECT_DEFAULTS(structFMMC_MENU_ITEMS &sFMMCmenu, OBJECT_CREATION_STRUCT& sObjStruct, BOOL bClearAllSettings)

	IF bClearAllSettings
		sFMMCmenu.iObjectHealth = -1
		sFMMCmenu.iMinigameType = ciFMMC_OBJECT_MINIGAME_TYPE__NONE
		
		INT iTeam
		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
			sObjStruct.iSelectedObjectRule[iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
		ENDFOR
	ENDIF
	
	SET_BIT(sFMMCMenu.iObjMenuBitset, cibsOBJ_Unfraggable)
ENDPROC

PROC SET_NEW_2020_SPAWNPOINT_DEFAULTS(structFMMC_MENU_ITEMS &sFMMCmenu, BOOL bClearAllSettings)
	IF bClearAllSettings
		sFMMCMenu.iAreaScale = 10
	ENDIF
ENDPROC

PROC SET_NEW_2020_ZONE_DEFAULTS(structFMMC_MENU_ITEMS &sFMMCmenu, BOOL bClearAllSettings)

	IF bClearAllSettings
		sFMMCMenu.sMenuZone.iBlipColor = BLIP_COLOUR_YELLOW
		sFMMCMenu.sMenuZone.iBlipAlpha = 60
	ENDIF
	
	SET_BIT(sFMMCMenu.sMenuZone.iZoneBS, ciFMMC_ZONEBS_DONT_CLEAR_AREA)
ENDPROC

PROC SET_NEW_2020_WARP_PORTAL_DEFAULTS(BOOL bClearAllSettings)
	IF bClearAllSettings
		INT iWarpPortal
		FOR iWarpPortal = 0 TO FMMC_MAX_WARP_PORTALS - 1
			g_FMMC_STRUCT.sWarpPortals[iWarpPortal].fCamAnimFOV = 40
		ENDFOR
	ENDIF
ENDPROC

PROC SET_NEW_2020_DUMMY_BLIP_DEFAULTS(BOOL bClearAllSettings)
	IF bClearAllSettings
		g_FMMC_STRUCT.sCurrentDummyBlip.sDummyBlipBlipData.iBlipColour = 5
		g_FMMC_STRUCT.sCurrentDummyBlip.iBlipSize = ciDUMMY_BLIP_SIZE_LOCATION
	ENDIF
ENDPROC

PROC SET_NEW_2020_PICKUP_DEFAULTS(structFMMC_MENU_ITEMS &sFMMCmenu, BOOL bClearAllSettings)
	IF bClearAllSettings
		g_FMMC_STRUCT_ENTITIES.sEditedWeapon.iCustomPickupType = ciCUSTOM_PICKUP_TYPE__MISSION_EQUIPMENT
		sFMMCmenu.iWepPlacedBitset = 0
	ENDIF
ENDPROC

PROC SET_NEW_2020_SHARED_SPAWN_SETTINGS_DEFAULTS(structFMMC_MENU_ITEMS &sFMMCmenu, BOOL bClearAllSettings)
	
	IF bClearAllSettings
		sFMMCmenu.iSpawnGroup = 0
		sFMMCmenu.iSpawnGroupSelectedIndex = -1		
		sFMMCmenu.iModifySpawnGroupOnEvent = 0 
		sFMMCmenu.iSpawnGroupAdditionalFunctionalityBS = 0
		INT i = 0
		FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			sFMMCmenu.iSubSpawnGroupBS[i] = 0
			sFMMCmenu.iModifySpawnSubGroupOnEventBS[i] = 0
		ENDFOR

		FOR i = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
			sFMMCmenu.iSpawnBlockCategory[i] = ciSPAWN_CONDITION_FLAG_CATEGORIES_GENERAL
			sFMMCmenu.eSpawnConditionFlag[i] = SPAWN_CONDITION_FLAG_NOT_SET
		ENDFOR
	ENDIF
	
ENDPROC

PROC SET_NEW_2020_ENTITY_CREATION_DEFAULTS(structFMMC_MENU_ITEMS &sFMMCmenu, BOOL bIsInitialCall, OBJECT_CREATION_STRUCT& sObjStruct)
	
	BOOL bClearAllSettings = bIsInitialCall OR IS_MENU_A_RESET_MENU(sFMMCmenu.sActiveMenu)
	PRINTLN("[Creator] SET_NEW_2020_ENTITY_CREATION_DEFAULTS | bIsInitialCall: ", bIsInitialCall, " / bClearAllSettings: ", bClearAllSettings)
	
	SET_NEW_2020_PED_DEFAULTS(sFMMCmenu, bClearAllSettings)
	SET_NEW_2020_VEH_DEFAULTS(sFMMCmenu, bClearAllSettings)
	SET_NEW_2020_PROP_DEFAULTS(bClearAllSettings)
	SET_NEW_2020_OBJECT_DEFAULTS(sFMMCmenu, sObjStruct, bClearAllSettings)
	SET_NEW_2020_SPAWNPOINT_DEFAULTS(sFMMCmenu, bClearAllSettings)
	SET_NEW_2020_ZONE_DEFAULTS(sFMMCmenu, bClearAllSettings)
	SET_NEW_2020_WARP_PORTAL_DEFAULTS(bClearAllSettings)
	SET_NEW_2020_DUMMY_BLIP_DEFAULTS(bClearAllSettings)
	SET_NEW_2020_PICKUP_DEFAULTS(sFMMCmenu, bClearAllSettings)
	SET_NEW_2020_SHARED_SPAWN_SETTINGS_DEFAULTS(sFMMCMenu, bClearAllSettings)
	
	CLEAR_ALL_FMMC_ENTITY_OVERRIDES(sFMMCmenu)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Creator Entity Helpers
// ##### Description: Generic helpers for dealing with entities in the creators
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CREATE_FMMC_BLIP(BLIP_INDEX &blip, VECTOR coords, HUD_COLOURS colour, STRING name, FLOAT size, BOOL bHighPriority = FALSE, BOOL bShowCone = FALSE, BLIP_SPRITE bsCustomSprite = RADAR_TRACE_INVALID)
	IF DOES_BLIP_EXIST(blip)
		REMOVE_BLIP(blip)
	ENDIF
	blip = ADD_BLIP_FOR_COORD(coords)
	IF bsCustomSprite != RADAR_TRACE_INVALID
		SET_BLIP_SPRITE(blip, bsCustomSprite)
	ENDIF
	SET_BLIP_COLOUR_FROM_HUD_COLOUR(blip, colour)
	SET_BLIP_SCALE(blip, size)
	SET_BLIP_AS_MINIMAL_ON_EDGE(blip, TRUE)
	IF bHighPriority
		SET_BLIP_PRIORITY(blip,BLIPPRIORITY_MED_HIGH)
	ELSE
		SET_BLIP_PRIORITY(blip,BLIPPRIORITY_MED)
	ENDIF
	SHOW_HEIGHT_ON_BLIP(blip, FALSE)
	IF bShowCone
		PRINTLN("Creating a blip with a cone")
	ENDIF
	SET_BLIP_SHOW_CONE(blip, bShowCone)
	IF NOT IS_STRING_NULL_OR_EMPTY(name)
		SET_BLIP_NAME_FROM_TEXT_FILE(blip, name)
	ENDIF
ENDPROC

// PURPOSE: Displays on-screen limits and returns TRUE if there are too many Networked Objects to place another.
FUNC BOOL PROCESS_NETWORKED_OBJECT_LIMIT(CREATION_VARS_STRUCT& sCurrentVarsStruct)

	IF NOT FMMC_IS_ROCKSTAR_DEV()
		RETURN FALSE
	ENDIF
	
	IF CAN_ANOTHER_NETWORKED_OBJECT_BE_PLACED()
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_NETWORKED_OBJECTS)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_PLACED_NETWORKED_OBJECTS(), FMMC_MAX_NUM_NETWORKED_OBJ_POOL, "FMMC_NTWK_OB", -1, HUD_COLOUR_WHITE, HUDORDER_BOTTOM, FALSE, HUDFLASHING_NONE, 0)
	
	ELSE
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_NETWORKED_OBJECTS)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_PLACED_NETWORKED_OBJECTS(), FMMC_MAX_NUM_NETWORKED_OBJ_POOL, "FMMC_NTWK_OB", -1, HUD_COLOUR_RED, HUDORDER_BOTTOM, FALSE, HUDFLASHING_FLASHRED, 0)
		
		sCurrentVarsStruct.bDisplayFailReason = TRUE
		sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
		sCurrentVarsStruct.sFailReason = "FMMC_ER_MNOP"
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_UP_MODEL_MEMORY_METER(MODEL_NAMES model, CREATION_VARS_STRUCT &sCurrentVarsStruct)
	IF model != DUMMY_MODEL_FOR_SCRIPT
		IF sCurrentVarsStruct.mnThingToPlaceForBudget != model
			IF sCurrentVarsStruct.mnThingToPlaceForBudget != DUMMY_MODEL_FOR_SCRIPT
				REMOVE_MODEL_FROM_CREATOR_BUDGET(sCurrentVarsStruct.mnThingToPlaceForBudget)
			ENDIF
			sCurrentVarsStruct.mnThingToPlaceForBudget = model
			ADD_MODEL_TO_CREATOR_BUDGET(sCurrentVarsStruct.mnThingToPlaceForBudget)
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_MODEL_MEMORY_METER()
	IF GET_USED_CREATOR_BUDGET() < 1
//		PRINTLN("Budget cool at - ", GET_USED_CREATOR_BUDGET())
		DRAW_GENERIC_METER(ROUND(GET_USED_CREATOR_BUDGET() * 1000), 1000, "FMMC_M_MEM", HUD_COLOUR_WHITE, -1, HUDORDER_TOP)
	ELSE
//			PRINTLN("Budget full at - ", GET_USED_CREATOR_BUDGET())
		DRAW_GENERIC_METER(1, 1, "FMMC_M_MEM", HUD_COLOUR_RED, -1, HUDORDER_TOP)
	ENDIF
ENDPROC

PROC DO_TOO_MANY_ENTITY_ERROR(CREATION_VARS_STRUCT &sCurrentVarsStruct, HUDFLASHING &flashColour, STRING sFail, INT iIndex = 0)
	MPGlobalsScoreHud.iGoalFadeFlashing_GenericScore[iIndex] = 255
	REINIT_NET_TIMER(MPGlobalsScoreHud.iGoalMetFlashing_GenericScore[iIndex], TRUE)	
	PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
	sCurrentVarsStruct.bDisplayFailReason = TRUE
	sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
	sCurrentVarsStruct.sFailReason = sFail
	flashColour = HUDFLASHING_FLASHRED
	PRINTSTRING("TOO MANY ENTITY ERROR SOUND EFFECT!!!")PRINTNL()
ENDPROC

FUNC ENTITY_INDEX GET_CREATOR_ENTITY_FROM_INDEX(INT iEntityType, INT iEntityIndex)
	
	ENTITY_INDEX eiEntity = NULL
	
	PED_INDEX piPed = NULL
	INT iVehicleSearchFlags = (VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED | VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK | VEHICLE_SEARCH_FLAG_ALLOW_TRAILERS | VEHICLE_SEARCH_FLAG_ALLOW_BLIMPS | VEHICLE_SEARCH_FLAG_ALLOW_SUBMARINES)

	SWITCH iEntityType
		CASE ciENTITY_TYPE_PED
			IF GET_CLOSEST_PED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityIndex].vPos, 1.0, FALSE, TRUE, piPed, FALSE, TRUE)
				eiEntity = piPed
			ENDIF
		BREAK
		CASE ciENTITY_TYPE_VEHICLE
			eiEntity = GET_CLOSEST_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityIndex].vPos, 1.0, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityIndex].mn, iVehicleSearchFlags)
		BREAK
		CASE ciENTITY_TYPE_OBJECT
			eiEntity = GET_CLOSEST_OBJECT_OF_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityIndex].vPos, 1.0, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityIndex].mn)
		BREAK
		CASE ciENTITY_TYPE_PROPS
			eiEntity = GET_CLOSEST_OBJECT_OF_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iEntityIndex].vPos, 1.0, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iEntityIndex].mn)
		BREAK
		CASE ciENTITY_TYPE_DYNO_PROPS
			eiEntity = GET_CLOSEST_OBJECT_OF_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iEntityIndex].vPos, 1.0, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iEntityIndex].mn)
		BREAK
		CASE ciENTITY_TYPE_INTERACTABLES
			eiEntity = GET_CLOSEST_OBJECT_OF_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iEntityIndex].vInteractable_Position, 1.0, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iEntityIndex].mnInteractable_Model)
		BREAK
		
		DEFAULT
			PRINTLN("GET_CREATOR_ENTITY_FROM_INDEX | Entity type isn't hooked up!")
			ASSERTLN("GET_CREATOR_ENTITY_FROM_INDEX | Entity type isn't hooked up!")
		BREAK
	ENDSWITCH
	
	RETURN eiEntity
ENDFUNC

PROC SET_HOVER_ENTITY(CREATION_VARS_STRUCT 	&sCurrentVarsStruct, INT iEntityType, INT iEntityIndex)
	IF sCurrentVarsStruct.iHoverEntityType != iEntityType
	OR sCurrentVarsStruct.iHoverEntityIndex != iEntityIndex
	
		sCurrentVarsStruct.iHoverEntityType = iEntityType
		sCurrentVarsStruct.iHoverEntityIndex = iEntityIndex
		PRINTLN("SET_HOVER_ENTITY | Setting hover entity to type ", iEntityType, " and index ", iEntityIndex)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Units Creator Entity Helpers
// ##### Description: Helpers for dealing with Units in the creators
FUNC BOOL CREATE_UNIT_BASE_OBJECT_FOR_PLACEMENT(CREATION_VARS_STRUCT &sCurrentVarsStruct, UNIT_CREATION_STRUCT	&sUnitStruct)
	IF NOT DOES_ENTITY_EXIST(sUnitStruct.oiBaseObject)
		sUnitStruct.oiBaseObject = CREATE_OBJECT(PROP_LD_TEST_01, sCurrentVarsStruct.vCoronaPos)
		SET_ENTITY_COLLISION(sUnitStruct.oiBaseObject, FALSE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_UNIT_VEHICLES_FOR_PLACEMENT(CREATION_VARS_STRUCT &sCurrentVarsStruct, UNIT_CREATION_STRUCT	&sUnitStruct, INT iUnitNumber)
	
	UNUSED_PARAMETER(iUnitNumber)
	INT iUnitType = g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_VEHICLES(iUnitType)-1
		IF NOT DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.viVehicles[i])
			
			IF DOES_ENTITY_EXIST(sUnitStruct.oiBaseObject)
				
				MODEL_NAMES mnVeh = g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[i].mnVehicleModel
				
				REQUEST_MODEL(mnVeh)
				
				IF NOT HAS_MODEL_LOADED(mnVeh)
					RETURN FALSE
				ENDIF
				
				sUnitStruct.sCoronaEntities.viVehicles[i] = CREATE_VEHICLE(g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[i].mnVehicleModel, sCurrentVarsStruct.vCoronaPos)
				PRINTLN("CREATE_UNIT_VEHICLE_FOR_PLACEMENT, CREATION_STAGE_MAKE, CREATE_VEHICLE ")
				
				SET_ENTITY_HEADING(sUnitStruct.sCoronaEntities.viVehicles[i], sCurrentVarsStruct.fCreationHeading)
				
				sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
				sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL
				SET_ENTITY_COLLISION(sUnitStruct.sCoronaEntities.viVehicles[i], FALSE)
				SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.viVehicles[i], 0, FALSE)
				
				INT iUnitVariation = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iVariation
				
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading, GET_OFFSET_FOR_UNIT_VEHICLE(iUnitType, i, iUnitVariation))
				SET_ENTITY_COORDS(sUnitStruct.sCoronaEntities.viVehicles[i], vPos)
				
				#IF IS_DEBUG_BUILD
				VECTOR vTemp = GET_ENTITY_COORDS(sUnitStruct.oiBaseObject)
				PRINTLN("CREATE_UNIT_VEHICLE_FOR_PLACEMENT - Unit pos: ", vTemp)
				vTemp = GET_ENTITY_COORDS(sUnitStruct.sCoronaEntities.viVehicles[i])
				PRINTLN("CREATE_UNIT_VEHICLE_FOR_PLACEMENT - Vehicle ", i, " created at ", vTemp, " heading ", GET_ENTITY_HEADING(sUnitStruct.sCoronaEntities.viVehicles[i]))
				#ENDIF
				
				FMMC_SET_THIS_VEHICLE_COLOURS(sUnitStruct.sCoronaEntities.viVehicles[i], -1, -1)
				IF g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[i].iVehicleModPreset = -1
					FMMC_CLEAR_ALL_VEHICLE_MODS(sUnitStruct.sCoronaEntities.viVehicles[i])
				ELSE
					FMMC_SET_VEH_MOD_PRESET(sUnitStruct.sCoronaEntities.viVehicles[i], GET_ENTITY_MODEL(sUnitStruct.sCoronaEntities.viVehicles[i]), g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[i].iVehicleModPreset)
				ENDIF

				SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[i].mnVehicleModel)
				
			ENDIF
			
			RETURN FALSE
		ELSE
			PRINTLN("CREATE_UNIT_VEHICLE_FOR_PLACEMENT, Vehicle ", i, " exists")
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CREATE_PLACED_UNIT_VEHICLES(UNIT_PLACEMENT_STRUCT &sUnitEntities[], INT iUnitNumber, BOOL bTest)
	INT iUnitType = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_VEHICLES(iUnitType)-1
		PRINTLN("CREATE_PLACED_UNIT_VEHICLES - Creating vehicle ", i,"/", GET_NUMBER_OF_UNIT_VEHICLES(iUnitType)-1, " Unit Type: ", iUnitType)
		IF NOT DOES_ENTITY_EXIST(sUnitEntities[iUnitNumber].viVehicles[i])
			
			MODEL_NAMES mnVeh = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].sUnitVeh[i].mnVehicleModel
			
			REQUEST_MODEL(mnVeh)
			
			IF HAS_MODEL_LOADED(mnVeh)
				BOOL bNetworked = TRUE
				IF IS_FAKE_MULTIPLAYER_MODE_SET()
					bNetworked = FALSE
				ENDIF
				
				sUnitEntities[iUnitNumber].viVehicles[i] = CREATE_VEHICLE(mnVeh, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, DEFAULT, bNetworked)
				PRINTLN("CREATE_PLACED_UNIT_VEHICLES, CREATION_STAGE_MAKE, CREATE_VEHICLE ")
				
				INT iUnitVariation = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iVariation
				
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading, GET_OFFSET_FOR_UNIT_VEHICLE(iUnitType, i, iUnitVariation))
				SET_ENTITY_COORDS(sUnitEntities[iUnitNumber].viVehicles[i], vPos)
				FLOAT fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading + GET_HEADING_FOR_UNIT_VEHICLE(iUnitType, i, iUnitVariation)
				SET_ENTITY_HEADING(sUnitEntities[iUnitNumber].viVehicles[i], fHeading)
				
				SET_ENTITY_LOAD_COLLISION_FLAG(sUnitEntities[iUnitNumber].viVehicles[i], TRUE)
				
				PRINTLN("CREATE_PLACED_UNIT_VEHICLES - Unit pos: ", g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos)
				PRINTLN("CREATE_PLACED_UNIT_VEHICLES - Unit ", iUnitNumber, " Vehicle ", i, " created at ", vPos, " heading ", fHeading)
				
				FMMC_SET_THIS_VEHICLE_COLOURS(sUnitEntities[iUnitNumber].viVehicles[i], -1, -1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].sUnitVeh[i].iVehicleModPreset != -1
					FMMC_SET_VEH_MOD_PRESET(sUnitEntities[iUnitNumber].viVehicles[i], GET_ENTITY_MODEL(sUnitEntities[iUnitNumber].viVehicles[i]), g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].sUnitVeh[i].iVehicleModPreset)
				ENDIF
				
				IF bTest
					SET_UP_UNIT_VEHICLE(iUnitType, sUnitEntities[iUnitNumber].viVehicles[i], i, iUnitNumber)
				ENDIF
				
				SET_VEHICLE_ON_GROUND_PROPERLY(sUnitEntities[iUnitNumber].viVehicles[i])
				
				SET_MODEL_AS_NO_LONGER_NEEDED(mnVeh)
			ENDIF
			
			RETURN FALSE
		ELSE
			PRINTLN("CREATE_PLACED_UNIT_VEHICLES, Vehicle ", i, " exists")
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC SET_UP_PLACED_UNIT_VEHICLES(UNIT_CREATION_STRUCT &sUnitStruct, INT iUnitNumber)
	PRINTLN("[Units] SET_UP_PLACED_UNIT_VEHICLES")
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_VEHICLES(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType)-1
		IF DOES_ENTITY_EXIST(sUnitStruct.sEntities[iUnitNumber].viVehicles[i])
			PRINTLN("[Units] SET_UP_PLACED_UNIT_VEHICLES - Setting up vehicle ", i, " for unit ", iUnitNumber)
			VEHICLE_INDEX viUnitVeh = sUnitStruct.sEntities[iUnitNumber].viVehicles[i]
			
			IF IS_VEHICLE_DRIVEABLE(viUnitVeh)
				// Update the vehicle entity now that it's sitting in the world
				SET_ENTITY_COLLISION(viUnitVeh, TRUE)
				SET_CAN_CLIMB_ON_ENTITY(viUnitVeh, FALSE)
				SET_ENTITY_PROOFS(viUnitVeh, FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viUnitVeh, FALSE)
				
				MODEL_NAMES mnVeh = GET_ENTITY_MODEL(viUnitVeh)
				
				IF VEHICLE_IS_A_WATER_BASED_VEHICLE(viUnitVeh)
				AND (IS_ENTITY_IN_WATER(viUnitVeh) OR mnVeh = MARQUIS)
				AND mnVeh != STROMBERG
					SET_VEHICLE_ON_GROUND_PROPERLY(viUnitVeh)
					VECTOR vBoatCoords
					vBoatCoords = GET_ENTITY_COORDS(viUnitVeh)
					FLOAT fFloorZ = 0.0
					IF NOT GET_GROUND_Z_FOR_3D_COORD(vBoatCoords, fFloorZ)
					OR fFloorZ < (vBoatCoords.z - 3.0)
						SET_BOAT_ANCHOR(viUnitVeh, TRUE)
						PRINTLN("ANCHOR THE BOAT, PLEASE.")
					ELSE
						PRINTLN("WATER NOT DEEP ENOUGH TO ANCHOR")
					ENDIF
				ELSE
					SET_VEHICLE_ON_GROUND_PROPERLY(viUnitVeh)
				ENDIF
				
				FREEZE_ENTITY_POSITION(viUnitVeh, TRUE)
				
				ADD_MODEL_TO_CREATOR_BUDGET(mnVeh)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL CREATE_UNIT_PEDS_FOR_PLACEMENT(CREATION_VARS_STRUCT &sCurrentVarsStruct, UNIT_CREATION_STRUCT	&sUnitStruct, INT iUnitNumber)
	
	UNUSED_PARAMETER(iUnitNumber)
	INT iUnitType = g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType
	INT iUnitVariation = g_FMMC_STRUCT_ENTITIES.sEditedUnit.iVariation
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_PEDS(iUnitType, iUnitVariation)-1
		IF NOT DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.piPeds[i])
			
			IF DOES_ENTITY_EXIST(sUnitStruct.oiBaseObject)
				sUnitStruct.sCoronaEntities.piPeds[i] = CREATE_PED(PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel, sCurrentVarsStruct.vCoronaPos)
				PRINTLN("CREATE_UNIT_PEDS_FOR_PLACEMENT, CREATION_STAGE_MAKE, CREATE_PED")
				
				SET_ENTITY_HEADING(sUnitStruct.sCoronaEntities.piPeds[i], sCurrentVarsStruct.fCreationHeading)
				
				sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
				sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL
				SET_ENTITY_COLLISION(sUnitStruct.sCoronaEntities.piPeds[i], FALSE)
				SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.piPeds[i], 0, FALSE)
				
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading, GET_OFFSET_FOR_UNIT_PED(iUnitType, i, iUnitVariation))
				SET_ENTITY_COORDS(sUnitStruct.sCoronaEntities.piPeds[i], vPos)
				
				#IF IS_DEBUG_BUILD
				VECTOR vTemp = GET_ENTITY_COORDS(sUnitStruct.oiBaseObject)
				PRINTLN("CREATE_UNIT_PEDS_FOR_PLACEMENT - Unit pos: ", vTemp)
				vTemp = GET_ENTITY_COORDS(sUnitStruct.sCoronaEntities.piPeds[i])
				PRINTLN("CREATE_UNIT_PEDS_FOR_PLACEMENT - Ped ", i, " created at ", vTemp, " heading ", GET_ENTITY_HEADING(sUnitStruct.sCoronaEntities.piPeds[i]))
				#ENDIF

				
				IF i = GET_NUMBER_OF_UNIT_PEDS(iUnitType, iUnitVariation)-1
					SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel)
				ENDIF
			ENDIF
			
			RETURN FALSE
		ELSE
			PRINTLN("CREATE_UNIT_PEDS_FOR_PLACEMENT, Ped ", i, " exists")
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CREATE_PLACED_UNIT_PEDS(UNIT_PLACEMENT_STRUCT &sUnitEntities[], INT iUnitNumber, BOOL bTest = FALSE)
	INT iUnitType = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType
	INT iUnitVariation = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iVariation
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_PEDS(iUnitType, iUnitVariation)-1
		PRINTLN("CREATE_PLACED_UNIT_PEDS - Creating ped ", i,"/", GET_NUMBER_OF_UNIT_PEDS(iUnitType, iUnitVariation)-1)
		IF NOT DOES_ENTITY_EXIST(sUnitEntities[iUnitNumber].piPeds[i])
			
			REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].mnPedModel)
			
			IF HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].mnPedModel)
				BOOL bNetworked = TRUE
				IF IS_FAKE_MULTIPLAYER_MODE_SET()
					bNetworked = FALSE
				ENDIF
				
				sUnitEntities[iUnitNumber].piPeds[i] = CREATE_PED(PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].mnPedModel, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, DEFAULT, bNetworked)
				PRINTLN("CREATE_PLACED_UNIT_PEDS, CREATION_STAGE_MAKE, CREATE_PED ")
				
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading, GET_OFFSET_FOR_UNIT_PED(iUnitType, i, iUnitVariation))
				SET_ENTITY_COORDS(sUnitEntities[iUnitNumber].piPeds[i], vPos)
				
				FLOAT fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading + GET_HEADING_FOR_UNIT_PED(iUnitType, i, iUnitVariation)
				SET_ENTITY_HEADING(sUnitEntities[iUnitNumber].piPeds[i], fHeading)
				
				PRINTLN("CREATE_PLACED_UNIT_PEDS - Unit pos: ", g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos)
				PRINTLN("CREATE_PLACED_UNIT_PEDS - Unit ", iUnitNumber, " Ped ", i, " created at ", vPos, " heading ", fHeading)
				
				IF NOT bTest
					FREEZE_ENTITY_POSITION(sUnitEntities[iUnitNumber].piPeds[i], TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sUnitEntities[iUnitNumber].piPeds[i], TRUE)
				ENDIF
				
				SET_WAIT_FOR_COLLISIONS_BEFORE_PROBE(sUnitEntities[iUnitNumber].piPeds[i], TRUE)
				
				IF i = GET_NUMBER_OF_UNIT_PEDS(iUnitType, iUnitVariation)-1
					SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].mnPedModel)
				ENDIF
			ENDIF
			
			RETURN FALSE
		ELSE
			PRINTLN("CREATE_PLACED_UNIT_PEDS, Ped ", i, " exists")
		ENDIF
	ENDFOR
	
	
	RETURN TRUE
ENDFUNC

PROC SET_UP_PLACED_UNIT_PEDS(UNIT_CREATION_STRUCT &sUnitStruct, INT iUnitNumber)
	PRINTLN("[Units] SET_UP_PLACED_UNIT_PEDS")
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_PEDS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iVariation)-1
		IF DOES_ENTITY_EXIST(sUnitStruct.sEntities[iUnitNumber].piPeds[i])
			PRINTLN("[Units] SET_UP_PLACED_UNIT_PEDS - Setting up ped ", i, " for unit ", iUnitNumber)
			PED_INDEX piUnitPed = sUnitStruct.sEntities[iUnitNumber].piPeds[i]
			
			IF DOES_ENTITY_EXIST(piUnitPed)
				// Update the ped entity now that it's sitting in the world
				SET_ENTITY_COLLISION(piUnitPed, TRUE)
				SET_ENTITY_PROOFS(piUnitPed, FALSE, FALSE, FALSE, FALSE, FALSE)
				FLOAT fZ = 0
				VECTOR vPos = GET_ENTITY_COORDS(piUnitPed)
				GET_GROUND_Z_FOR_3D_COORD(vPos, fZ)
				vPos.z = fZ
				SET_ENTITY_COORDS(piUnitPed, vPos)
				
				FREEZE_ENTITY_POSITION(piUnitPed, TRUE)
				
				ADD_MODEL_TO_CREATOR_BUDGET(GET_ENTITY_MODEL(piUnitPed))
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL CREATE_UNIT_DYNOPROPS_FOR_PLACEMENT(CREATION_VARS_STRUCT &sCurrentVarsStruct, UNIT_CREATION_STRUCT	&sUnitStruct, INT iUnitNumber)
	
	UNUSED_PARAMETER(iUnitNumber)
	INT iUnitType = g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_DYNOPROPS(iUnitType)-1
		IF NOT DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.oiProps[i])
			
			IF DOES_ENTITY_EXIST(sUnitStruct.oiBaseObject)
				sUnitStruct.sCoronaEntities.oiProps[i] = CREATE_OBJECT(GET_DEFAULT_MODEL_NAME_FOR_UNIT_DYNOPROP(iUnitType), sCurrentVarsStruct.vCoronaPos)
				PRINTLN("CREATE_UNIT_DYNOPROPS_FOR_PLACEMENT, CREATION_STAGE_MAKE, CREATE_DYNOPROp")
				
				SET_ENTITY_HEADING(sUnitStruct.sCoronaEntities.oiProps[i], sCurrentVarsStruct.fCreationHeading)
				
				sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
				sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL
				SET_ENTITY_COLLISION(sUnitStruct.sCoronaEntities.oiProps[i], FALSE)
				SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.oiProps[i], 0, FALSE)
				
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading, GET_OFFSET_FOR_UNIT_DYNOPROP(iUnitType, i))
				FLOAT fZ = 0
				IF GET_GROUND_Z_FOR_3D_COORD(vPos, fZ)
					vPos.Z = fZ
				ENDIF
				SET_ENTITY_COORDS(sUnitStruct.sCoronaEntities.oiProps[i], vPos)
				
				#IF IS_DEBUG_BUILD
				VECTOR vTemp = GET_ENTITY_COORDS(sUnitStruct.oiBaseObject)
				PRINTLN("CREATE_UNIT_DYNOPROPS_FOR_PLACEMENT - Unit pos: ", vTemp)
				vTemp = GET_ENTITY_COORDS(sUnitStruct.sCoronaEntities.oiProps[i])
				PRINTLN("CREATE_UNIT_DYNOPROPS_FOR_PLACEMENT - Dyno-prop ", i, " created at ", vTemp, " heading ", GET_ENTITY_HEADING(sUnitStruct.sCoronaEntities.oiProps[i]))
				#ENDIF

				
				IF i = GET_NUMBER_OF_UNIT_DYNOPROPS(iUnitType)-1
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_DEFAULT_MODEL_NAME_FOR_UNIT_DYNOPROP(iUnitType))
				ENDIF
			ENDIF
			
			RETURN FALSE
		ELSE
			PRINTLN("CREATE_UNIT_DYNOPROPS_FOR_PLACEMENT, Dyno-prop ", i, " exists")
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CREATE_PLACED_UNIT_DYNOPROPS(UNIT_PLACEMENT_STRUCT &sUnitEntities[], INT iUnitNumber, BOOL bTest = FALSE)
	INT iUnitType = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_DYNOPROPS(iUnitType)-1
		PRINTLN("CREATE_PLACED_UNIT_DYNOPROPS - Creating dyno-prop ", i,"/", GET_NUMBER_OF_UNIT_DYNOPROPS(iUnitType)-1)
		IF NOT DOES_ENTITY_EXIST(sUnitEntities[iUnitNumber].oiProps[i])
			
			MODEL_NAMES mnDynoProp = GET_DEFAULT_MODEL_NAME_FOR_UNIT_DYNOPROP(iUnitType)
			
			REQUEST_MODEL(mnDynoProp)
			
			IF HAS_MODEL_LOADED(mnDynoProp)
				BOOL bNetworked = TRUE
				IF IS_FAKE_MULTIPLAYER_MODE_SET()
					bNetworked = FALSE
				ENDIF
				
				sUnitEntities[iUnitNumber].oiProps[i] = CREATE_OBJECT(mnDynoProp, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, bNetworked)
				PRINTLN("CREATE_PLACED_UNIT_DYNOPROPS, CREATION_STAGE_MAKE, CREATE_DYNOPROP ")
				
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading, GET_OFFSET_FOR_UNIT_DYNOPROP(iUnitType, i))
				FLOAT fZ = 0
				IF GET_GROUND_Z_FOR_3D_COORD(vPos, fZ)
					vPos.Z = fZ
				ENDIF
				SET_ENTITY_COORDS(sUnitEntities[iUnitNumber].oiProps[i], vPos)
				FLOAT fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading + GET_HEADING_FOR_UNIT_DYNOPROP(iUnitType, i)
				SET_ENTITY_HEADING(sUnitEntities[iUnitNumber].oiProps[i], fHeading)
				
				PRINTLN("CREATE_PLACED_UNIT_DYNOPROPS - Unit pos: ", g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos)
				PRINTLN("CREATE_PLACED_UNIT_DYNOPROPS - Unit ", iUnitNumber, " Dyno-prop ", i, " created at ", vPos, " heading ", fHeading)
				
				IF !bTest
					FREEZE_ENTITY_POSITION(sUnitEntities[iUnitNumber].oiProps[i], TRUE)
				ENDIF
								
				IF i = GET_NUMBER_OF_UNIT_DYNOPROPS(iUnitType)-1
					SET_MODEL_AS_NO_LONGER_NEEDED(mnDynoProp)
				ENDIF
			ENDIF
			
			RETURN FALSE
		ELSE
			PRINTLN("CREATE_PLACED_UNIT_DYNOPROPS, Dyno-prop ", i, " exists")
		ENDIF
	ENDFOR
	
	
	RETURN TRUE
ENDFUNC

PROC SET_UP_PLACED_UNIT_DYNO_PROP(UNIT_CREATION_STRUCT &sUnitStruct, INT iUnitNumber)
	PRINTLN("[Units] SET_UP_PLACED_UNIT_DYNO_PROP")
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_DYNOPROPS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType)-1
		IF DOES_ENTITY_EXIST(sUnitStruct.sEntities[iUnitNumber].oiProps[i])
			PRINTLN("[Units] SET_UP_PLACED_UNIT_DYNO_PROP - Setting up dyno-prop ", i, " for unit ", iUnitNumber)
			OBJECT_INDEX oiUnitProp = sUnitStruct.sEntities[iUnitNumber].oiProps[i]
			
			IF IS_ENTITY_ALIVE(oiUnitProp)
			
				SET_ENTITY_COLLISION(oiUnitProp, TRUE)
				SET_ENTITY_PROOFS(oiUnitProp, FALSE, FALSE, FALSE, FALSE, FALSE)
				VECTOR vPos = GET_ENTITY_COORDS(oiUnitProp)
				FLOAT fZ = 0
				vPos.z += 1
				IF GET_GROUND_Z_FOR_3D_COORD(vPos, fZ)
					vPos.Z = fZ
				ENDIF
				SET_ENTITY_COORDS(oiUnitProp, vPos)
				
				FREEZE_ENTITY_POSITION(oiUnitProp, TRUE)
				
				ADD_MODEL_TO_CREATOR_BUDGET(GET_ENTITY_MODEL(oiUnitProp))
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL CREATE_ALL_PLACED_UNITS(UNIT_PLACEMENT_STRUCT &sUnitEntities[], BOOL bTest = FALSE)
	INT iUnit, iCreated
	FOR iUnit = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced-1
		
		IF CREATE_PLACED_UNIT_VEHICLES(sUnitEntities, iUnit, bTest)
		AND CREATE_PLACED_UNIT_PEDS(sUnitEntities, iUnit, bTest)
		AND CREATE_PLACED_UNIT_DYNOPROPS(sUnitEntities, iUnit, bTest)
			iCreated++
			PRINTLN("CREATE_ALL_PLACED_UNITS - Units created ", iCreated,"/", g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced)
		ENDIF
	ENDFOR
	
	RETURN iCreated = g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced
ENDFUNC

PROC DELETE_PLACED_UNIT_ENTITIES(UNIT_CREATION_STRUCT &sUnitStruct, INT iUnitNumber)
	INT i
	
	MODEL_NAMES mnUnitEntModel
	
	//Vehicles
	FOR i = 0 TO FMMC_MAX_NUM_UNIT_VEHICLES-1
		IF DOES_ENTITY_EXIST(sUnitStruct.sEntities[iUnitNumber].viVehicles[i])
			mnUnitEntModel = GET_ENTITY_MODEL(sUnitStruct.sEntities[iUnitNumber].viVehicles[i])
				
			DELETE_VEHICLE(sUnitStruct.sEntities[iUnitNumber].viVehicles[i])
			
		ENDIF
	ENDFOR
	
	//Peds
	FOR i = 0 TO FMMC_MAX_NUM_UNIT_PEDS-1
		IF DOES_ENTITY_EXIST(sUnitStruct.sEntities[iUnitNumber].piPeds[i])
			mnUnitEntModel = GET_ENTITY_MODEL(sUnitStruct.sEntities[iUnitNumber].piPeds[i])
			REMOVE_MODEL_FROM_CREATOR_BUDGET(mnUnitEntModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnUnitEntModel)
			DELETE_PED(sUnitStruct.sEntities[iUnitNumber].piPeds[i])
		ENDIF
	ENDFOR
	
	//Props
	FOR i = 0 TO FMMC_MAX_NUM_UNIT_PROPS-1
		IF DOES_ENTITY_EXIST(sUnitStruct.sEntities[iUnitNumber].oiProps[i])
			mnUnitEntModel = GET_ENTITY_MODEL(sUnitStruct.sEntities[iUnitNumber].oiProps[i])
			REMOVE_MODEL_FROM_CREATOR_BUDGET(mnUnitEntModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnUnitEntModel)
			DELETE_OBJECT(sUnitStruct.sEntities[iUnitNumber].oiProps[i])
		ENDIF
	ENDFOR

	IF DOES_BLIP_EXIST(sUnitStruct.sEntities[iUnitNumber].biBlip)
		REMOVE_BLIP(sUnitStruct.sEntities[iUnitNumber].biBlip)
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[Units] DELETE_PLACED_UNIT_ENTITIES - Clearing unit: ", iUnitNumber)
ENDPROC

PROC DELETE_CORONA_UNIT_ENTITIES(UNIT_CREATION_STRUCT &sUnitStruct)
	INT i
	
	//Vehicles
	FOR i = 0 TO FMMC_MAX_NUM_UNIT_VEHICLES-1
		IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.viVehicles[i])
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(sUnitStruct.sCoronaEntities.viVehicles[i]))	
			DELETE_VEHICLE(sUnitStruct.sCoronaEntities.viVehicles[i])
		ENDIF
	ENDFOR
	
	//Peds
	FOR i = 0 TO FMMC_MAX_NUM_UNIT_PEDS-1
		IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.piPeds[i])
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(sUnitStruct.sCoronaEntities.piPeds[i]))	
			DELETE_PED(sUnitStruct.sCoronaEntities.piPeds[i])
		ENDIF
	ENDFOR
	
	//Props
	FOR i = 0 TO FMMC_MAX_NUM_UNIT_PROPS-1
		IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.oiProps[i])
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(sUnitStruct.sCoronaEntities.oiProps[i]))	
			DELETE_OBJECT(sUnitStruct.sCoronaEntities.oiProps[i])
		ENDIF
	ENDFOR
	
	IF DOES_ENTITY_EXIST(sUnitStruct.oiBaseObject)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(sUnitStruct.oiBaseObject))	
		DELETE_OBJECT(sUnitStruct.oiBaseObject)
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[Units] DELETE_CORONA_UNIT_ENTITIES - Clearing placement unit")
ENDPROC

PROC FMMC_REMOVE_ALL_UNITS(UNIT_CREATION_STRUCT &sUnitStruct, BOOL bClearData)
	
	PRINTLN("FMMC_REMOVE_ALL_UNITS - Called")
	
	DELETE_CORONA_UNIT_ENTITIES(sUnitStruct)
	INT iUnit
	FOR iUnit = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced-1
		DELETE_PLACED_UNIT_ENTITIES(sUnitStruct, iUnit)
	ENDFOR
	
	IF bClearData
		CLEAR_UNIT_STRUCT()
	ENDIF
	
ENDPROC

PROC CLEAR_CURRENT_UNIT_AND_SET_UP_SELECTED(CREATION_VARS_STRUCT &sCurrentVarsStruct, UNIT_CREATION_STRUCT &sUnitStruct, INT iUnitNumber, structFMMC_MENU_ITEMS &sFMMCmenu)
	
	IF DOES_BLIP_EXIST(sUnitStruct.sEntities[iUnitNumber].biBlip)
		REMOVE_BLIP(sUnitStruct.sEntities[iUnitNumber].biBlip)
	ENDIF
	
	DELETE_PLACED_UNIT_ENTITIES(sUnitStruct, iUnitNumber)
	
	g_FMMC_STRUCT_ENTITIES.sEditedUnit = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber]
	
	sCurrentVarsStruct.fCreationHeading = g_FMMC_STRUCT_ENTITIES.sEditedUnit.fHeading
	
	//Vehicles
	IF g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[0].mnVehicleModel = DUMMY_MODEL_FOR_SCRIPT
		g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[0].mnVehicleModel = GET_DEFAULT_MODEL_NAME_FOR_UNIT_VEHICLE(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType)
	ENDIF
	sFMMCmenu.iVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[0].mnVehicleModel)
	sFMMCmenu.iVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[0].mnVehicleModel)
	REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[0].mnVehicleModel)
	
	//Peds
	IF g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel = DUMMY_MODEL_FOR_SCRIPT
		g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel = GET_DEFAULT_MODEL_NAME_FOR_UNIT_PED(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType)
	ENDIF
	sFMMCMenu.iPedLibrary = GET_PED_LIBRARY_FROM_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel, sFMMCMenu)
	sFMMCMenu.iPedType = GET_PED_TYPE_FROM_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel, sFMMCMenu)
	REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel)
	
	RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
	
	PRINTLN("CLEAR_CURRENT_UNIT_AND_SET_UP_SELECTED - Unit: ", iUnitNumber)
	
ENDPROC

FUNC BOOL PROCESS_UNIT_ENTITY_PLACEMENT_TEST(ENTITY_INDEX eiUnitEntity, CREATION_VARS_STRUCT &sCurrentVarsStruct, FMMC_LOCAL_STRUCT &sFMMCdata)
	VECTOR vEntMin, vEntMax, vEntityCoords
	VECTOR vVehMax, vVehMin, vEntCoords
	FLOAT fPropSphereRad, fVehSphereRad
	
	IF DOES_ENTITY_EXIST(eiUnitEntity)
		vEntityCoords = GET_ENTITY_COORDS(eiUnitEntity)
		
		MODEL_NAMES mnUnitModel = GET_ENTITY_MODEL(eiUnitEntity)
		IF IS_MODEL_VALID(mnUnitModel)
			GET_MODEL_DIMENSIONS(mnUnitModel, vEntMin, vEntMax)
		ENDIF
		fPropSphereRad = VDIST2(vEntMax, vEntMin)
		
		PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
		AND NOT IS_PED_INJURED(LocalPlayerPed)
			ENTITY_INDEX eiEntity
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF DOES_ENTITY_EXIST(viVeh)
				AND IS_VEHICLE_DRIVEABLE(viVeh)
					vEntCoords = GET_ENTITY_COORDS(viVeh)
					
					IF IS_MODEL_VALID(GET_ENTITY_MODEL(viVeh))
						GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(viVeh), vVehMin, vVehMax)
					ENDIF
					
					eiEntity = viVeh
					
				ENDIF
			ELSE
				vEntCoords = GET_ENTITY_COORDS(LocalPlayerPed)
				
				IF IS_MODEL_VALID(GET_ENTITY_MODEL(LocalPlayerPed))
					GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(LocalPlayerPed), vVehMin, vVehMax)
				ENDIF
				
				eiEntity = LocalPlayerPed

			ENDIF
			
			IF DOES_ENTITY_EXIST(eiEntity)
				fVehSphereRad = VDIST2(vVehMax, vVehMin)
				IF VDIST2(vEntCoords, vEntityCoords) < fVehSphereRad + fPropSphereRad + 10.0
					IF IS_ENTITY_TOUCHING_ENTITY(eiUnitEntity, eiEntity)
					OR GET_ENTITY_SPEED(eiEntity) > 0
						SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
						SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bShapeTestFail)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL MOVE_LAST_PLACED_UNIT(structFMMC_MENU_ITEMS 	&sFMMCmenu,
								UNIT_CREATION_STRUCT	&sUnitStruct,
								INT						iUnitNumber,
								VECTOR					&vCoord, 
								VECTOR					&vRotation, 
								FLOAT 					&fHead, 
								CREATION_VARS_STRUCT 	&sCurrentVarsStruct,
								FMMC_LOCAL_STRUCT 		&sFMMCdata,
								BOOL 					bSelectedSomething = FALSE)
	
	UNUSED_PARAMETER(iUnitNumber)

	IF DOES_ENTITY_EXIST(sUnitStruct.oiBaseObject)
		
		INT i
						
		MOVE_ENTITY(sUnitStruct.oiBaseObject, vCoord, fHead, sCurrentVarsStruct, sFMMCdata, sFMMCmenu, 0.0, DEFAULT, DEFAULT, HAS_CURRENT_PROP_BEEN_FREE_ROTATED(sFMMCmenu))
		
		vRotation = GET_ENTITY_ROTATION(sUnitStruct.oiBaseObject)

		ROTATE_ENTITY_USING_ANCHOR(sFMMCmenu, sCurrentVarsStruct, sUnitStruct.oiBaseObject, sFMMCmenu.oiFreeRotationAnchor, sFMMCmenu.iCurrentAxis, bSelectedSomething)
//		IF sCurrentVarsStruct.bClearedRotationPoint
//			PRINTLN("[Template_Snap] Prop(InTemplate) ", iSnapParentEntity, " - Cleared rotation prop, resnapping")
//			SET_ENTITY_COORDS(sUnitStruct.oiBaseObject, GET_ENTITY_COORDS(eiPassed[iSnapParentEntity]))
//			SET_ENTITY_ROTATION(sUnitStruct.oiBaseObject, GET_ENTITY_ROTATION(eiPassed[iSnapParentEntity]))
//			ATTACH_ENTITY_TO_ENTITY(sUnitStruct.oiBaseObject, eiPassed[iSnapParentEntity] , 0, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sUnitStruct.oiBaseObject, GET_ENTITY_COORDS(eiPassed[iSnapParentEntity])), g_FMMC_STRUCT.sSavedPropTemplates[sFMMcmenu.iPropType].vRotation[iSnapParentEntity], DEFAULT, DEFAULT, DEFAULT, DEFAULT, EULER_XYZ)
//			sCurrentVarsStruct.bClearedRotationPoint = FALSE
//		ENDIF

		INT iNumVeh = GET_NUMBER_OF_UNIT_VEHICLES(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType)
		INT iNumPed = GET_NUMBER_OF_UNIT_PEDS(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType, g_FMMC_STRUCT_ENTITIES.sEditedUnit.iVariation)
		INT iNumProps = GET_NUMBER_OF_UNIT_DYNOPROPS(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType)
		
		#IF IS_DEBUG_BUILD
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(sUnitStruct.oiBaseObject, "Base Object", 4.0)
		FOR i = 0 TO iNumVeh-1
			TEXT_LABEL_15 tl15 = "Vehicle "
			tl15 += i
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(sUnitStruct.sCoronaEntities.viVehicles[i], tl15, 5.0)
		ENDFOR
		FOR i = 0 TO iNumPed-1
			TEXT_LABEL_15 tl15 = "Ped "
			tl15 += i
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(sUnitStruct.sCoronaEntities.piPeds[i], tl15, 5.0)
		ENDFOR
		FOR i = 0 TO iNumProps-1
			TEXT_LABEL_15 tl15 = "Dyno-prop "
			tl15 += i
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(sUnitStruct.sCoronaEntities.oiProps[i], tl15, 5.0)
		ENDFOR
		#ENDIF
			
		IF NOT HAS_CURRENT_PROP_BEEN_FREE_ROTATED(sFMMCmenu)
			SET_ENTITY_ROTATION(sUnitStruct.oiBaseObject, vRotation)
		ENDIF
		
		VECTOR vBaseObjPos = GET_ENTITY_COORDS(sUnitStruct.oiBaseObject)
		
		INT iUnitVariation = g_FMMC_STRUCT_ENTITIES.sEditedUnit.iVariation
		
		INT iEntity
		FOR iEntity = 0 TO iNumVeh-1
			IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.viVehicles[iEntity])
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBaseObjPos, fHead, GET_OFFSET_FOR_UNIT_VEHICLE(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType, iEntity, iUnitVariation))
				SET_ENTITY_COORDS(sUnitStruct.sCoronaEntities.viVehicles[iEntity], vPos)
				FLOAT fHeading = fHead + GET_HEADING_FOR_UNIT_VEHICLE(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType, iEntity, iUnitVariation)
				SET_ENTITY_HEADING(sUnitStruct.sCoronaEntities.viVehicles[iEntity], fHeading)
				SET_VEHICLE_ON_GROUND_PROPERLY(sUnitStruct.sCoronaEntities.viVehicles[iEntity])
			ENDIF
		ENDFOR
		FOR iEntity = 0 TO iNumPed-1
			IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.piPeds[iEntity])
				
			
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBaseObjPos, fHead, GET_OFFSET_FOR_UNIT_PED(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType, iEntity, iUnitVariation))
				SET_ENTITY_COORDS(sUnitStruct.sCoronaEntities.piPeds[iEntity], vPos)
				
				FLOAT fHeading = fHead + GET_HEADING_FOR_UNIT_PED(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType, iEntity, iUnitVariation)
				SET_ENTITY_HEADING(sUnitStruct.sCoronaEntities.piPeds[iEntity], fHeading)
			ENDIF
		ENDFOR
		FOR iEntity = 0 TO iNumProps-1
			IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.oiProps[iEntity])
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBaseObjPos, fHead, GET_OFFSET_FOR_UNIT_DYNOPROP(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType, iEntity))
				FLOAT fZ = 0
				IF GET_GROUND_Z_FOR_3D_COORD(vPos, fZ)
					vPos.Z = fZ
				ENDIF
				SET_ENTITY_COORDS(sUnitStruct.sCoronaEntities.oiProps[iEntity], vPos)
				FLOAT fHeading = fHead + GET_HEADING_FOR_UNIT_DYNOPROP(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType, iEntity)
				SET_ENTITY_HEADING(sUnitStruct.sCoronaEntities.oiProps[iEntity], fHeading)
			ENDIF
		ENDFOR
		
		BOOL bOverlapingFail = FALSE
		SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
		AND GET_MENU_SECTION_FROM_MENU(sFMMCmenu.sActiveMenu) = MENU_SECTION_UNITS
			
			IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
				IF CAN_PLACE_BUTTON_BE_PRESSED(sFMMCmenu, sCurrentVarsStruct)
				
					IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
						
						FOR i = 0 TO iNumVeh-1
							VEHICLE_INDEX viUnitVeh = sUnitStruct.sCoronaEntities.viVehicles[i]
							IF NOT PROCESS_UNIT_ENTITY_PLACEMENT_TEST(viUnitVeh, sCurrentVarsStruct, sFMMCData)
								BREAKLOOP
							ENDIF
						ENDFOR
						
						FOR i = 0 TO iNumPed-1
							PED_INDEX piUnitPed = sUnitStruct.sCoronaEntities.piPeds[i]
							IF NOT PROCESS_UNIT_ENTITY_PLACEMENT_TEST(piUnitPed, sCurrentVarsStruct, sFMMCData)
								BREAKLOOP
							ENDIF
						ENDFOR
						
						FOR i = 0 TO iNumProps-1
							OBJECT_INDEX oiProp = sUnitStruct.sCoronaEntities.oiProps[i]
							IF NOT PROCESS_UNIT_ENTITY_PLACEMENT_TEST(oiProp, sCurrentVarsStruct, sFMMCData)
								BREAKLOOP
							ENDIF
						ENDFOR
					ENDIF
					
					IF FMMC_IS_ROCKSTAR_DEV() AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_DELETE_BUTTON())
					OR (IS_AREA_CLEAR_PLACEMENT_OVERRIDE_ACTIVE(sFMMCmenu) AND NOT bOverlapingFail)
					OR IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
					OR IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
						IF !sCurrentVarsStruct.sCoronaShapeTest.bGridVehicleHit
							SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
						ENDIF
					ENDIF
					
					IF GET_USED_CREATOR_BUDGET() >= 1
					AND NOT FMMC_IS_ROCKSTAR_DEV()
						SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bMemoryFail)
						SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)		
					ENDIF
					
					INT iMaxUnitObjects = FMMC_MAX_NUM_UNIT_OBJECTS_TOTAL - g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps
					IF (g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects + GET_NUMBER_OF_UNIT_DYNOPROPS(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType)) > iMaxUnitObjects
						SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bMemoryFail)
						SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
					ENDIF
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_ACCEPT_BUTTON(sFMMCdata)) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu)
					AND NOT bSelectedSomething
						IF sCurrentVarsStruct.bAreaIsGoodForPlacement
						AND IS_BIT_SET(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
							PLAY_SOUND_FRONTEND(-1,  GET_SOUND_CLIP_FROM_CREATOR_TYPE(), GET_CREATOR_SPECIFIC_SOUND_SET())
							PRINTSTRING("PLACE_OBJECT SOUND EFFECT!!!")PRINTNL()	
							sCurrentVarsStruct.bDisplayFailReason = FALSE
							CLEAR_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
							
							FOR i = 0 TO iNumVeh-1
								IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.viVehicles[i])
									RESET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.viVehicles[i])
								ENDIF
							ENDFOR
							
							FOR i = 0 TO iNumPed-1
								IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.piPeds[i])
									RESET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.piPeds[i])
								ENDIF
							ENDFOR
							
							FOR i = 0 TO iNumProps-1
								IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.oiProps[i])
									RESET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.oiProps[i])
								ENDIF
							ENDFOR
							
							RETURN TRUE	
						ELSE
							PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
							PRINTSTRING("MOVE_LAST_PLACED_VEHICLE ERROR SOUND EFFECT 2!!!")PRINTNL()
							sCurrentVarsStruct.bDisplayFailReason = TRUE
							sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
							sCurrentVarsStruct.sFailReason = GET_GENERIC_PLACEMENT_FAIL_DESCRIPTION(sCurrentVarsStruct)
						ENDIF
					ENDIF
					
				ELSE
					SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
				ENDIF
			ELSE
				PRINTLN("[LM] - Creator Bit - Blocking Placement. We had just entered a sub-menu. 10")
			ENDIF

		ELSE
			SET_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
		ENDIF
			
		IF NOT bSelectedSomething
		
			SET_ENTITY_COORDS(sUnitStruct.oiBaseObject, vCoord)
			
			FOR i = 0 TO iNumVeh-1
				IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.viVehicles[i])
					IF IS_BIT_SET(sCurrentVarsStruct.iCreationBitset, ciCREATION_VARS_BS_HIDE_PLACEMENT_ENTITY)
						SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.viVehicles[i], 0, FALSE)
					ELIF !sCurrentVarsStruct.bAreaIsGoodForPlacement
					OR sFMMCmenu.sActiveMenu = eFmmc_UNIT_BASE
						INT iAlpha = 150 
						IF sFMMCmenu.sActiveMenu = eFmmc_UNIT_BASE
							iAlpha = 200
						ENDIF
						SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.viVehicles[i], iAlpha, FALSE)
					ELSE
						RESET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.viVehicles[i])
					ENDIF
				ENDIF
				
			ENDFOR
			
			FOR i = 0 TO iNumPed-1
				IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.piPeds[i])
					IF IS_BIT_SET(sCurrentVarsStruct.iCreationBitset, ciCREATION_VARS_BS_HIDE_PLACEMENT_ENTITY)
						SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.piPeds[i], 0, FALSE)
					ELIF !sCurrentVarsStruct.bAreaIsGoodForPlacement
					OR sFMMCmenu.sActiveMenu = eFmmc_UNIT_BASE
						INT iAlpha = 150 
						IF sFMMCmenu.sActiveMenu = eFmmc_UNIT_BASE
							iAlpha = 200
						ENDIF
						SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.piPeds[i], iAlpha, FALSE)
					ELSE
						RESET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.piPeds[i])
					ENDIF
				ENDIF
			ENDFOR
			FOR i = 0 TO iNumProps-1
				IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.oiProps[i])
					IF IS_BIT_SET(sCurrentVarsStruct.iCreationBitset, ciCREATION_VARS_BS_HIDE_PLACEMENT_ENTITY)
						SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.oiProps[i], 0, FALSE)
					ELIF !sCurrentVarsStruct.bAreaIsGoodForPlacement
					OR sFMMCmenu.sActiveMenu = eFmmc_UNIT_BASE
						INT iAlpha = 150 
						IF sFMMCmenu.sActiveMenu = eFmmc_UNIT_BASE
							iAlpha = 200
						ENDIF
						SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.oiProps[i], iAlpha, FALSE)
					ELSE
						RESET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.oiProps[i])
					ENDIF
				ENDIF
			ENDFOR
		ELSE
			FOR i = 0 TO iNumVeh-1
				IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.viVehicles[i])
					SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.viVehicles[i], 0, FALSE )
				ENDIF
			ENDFOR
			FOR i = 0 TO iNumPed-1
				IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.piPeds[i])
					SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.piPeds[i], 0, FALSE )
				ENDIF
			ENDFOR
			FOR i = 0 TO iNumProps-1
				IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.oiProps[i])
					SET_ENTITY_ALPHA(sUnitStruct.sCoronaEntities.oiProps[i], 0, FALSE )
				ENDIF
			ENDFOR
			SET_ENTITY_COORDS(sUnitStruct.oiBaseObject, vCoord)
		ENDIF
	ENDIF		
		
	RETURN FALSE
ENDFUNC

FUNC BOOL LOAD_UNIT_VEHICLE_MODELS()
	INT iVeh
	FOR iVeh = 0 TO GET_NUMBER_OF_UNIT_VEHICLES(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType)-1
		REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[iVeh].mnVehicleModel)
	ENDFOR
	
	FOR iVeh = 0 TO GET_NUMBER_OF_UNIT_VEHICLES(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType)-1
		IF NOT HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[iVeh].mnVehicleModel)
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_UNIT_CREATION_MODELS_VALID()
	INT iVeh
	FOR iVeh = 0 TO GET_NUMBER_OF_UNIT_VEHICLES(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType)-1
		IF NOT IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[iVeh].mnVehicleModel)
			PRINTLN("ARE_UNIT_CREATION_MODELS_VALID - Veh model ", iVeh," is invalid")
			RETURN FALSE
		ENDIF
	ENDFOR
	
	IF NOT IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel)
		PRINTLN("ARE_UNIT_CREATION_MODELS_VALID - Ped model is invalid")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CORRECT_EDITED_UNIT_PLACEMENT_COUNTS()
	INT iUnit, iVehicles, iPeds, iObjects
	FOR iUnit = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced-1
		INT iUnitType = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].iUnitType
		INT iUnitVariation = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].iVariation
		iVehicles += GET_NUMBER_OF_UNIT_VEHICLES(iUnitType)
		iPeds += GET_NUMBER_OF_UNIT_PEDS(iUnitType, iUnitVariation)
		iObjects += GET_NUMBER_OF_UNIT_DYNOPROPS(iUnitType)
	ENDFOR
	
	IF iVehicles != g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles
		g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles = iVehicles
		PRINTLN("CORRECT_EDITED_UNIT_PLACEMENT_COUNTS - Vehicle count updated to ", g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles)
	ENDIF
	
	IF iPeds != g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds
		g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds = iPeds
		PRINTLN("CORRECT_EDITED_UNIT_PLACEMENT_COUNTS - Ped count updated to ", g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds)
	ENDIF
	
	IF iObjects != g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects
		g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects = iObjects
		PRINTLN("CORRECT_EDITED_UNIT_PLACEMENT_COUNTS - Object count updated to ", g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects)
	ENDIF
ENDPROC

PROC DO_UNIT_CREATION(CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
							UNIT_CREATION_STRUCT		&sUnitStruct,
							structFMMC_MENU_ITEMS 		&sFMMCmenu,
							FMMC_LOCAL_STRUCT 			&sFMMCdata,
							HUD_COLOURS_STRUCT			&sHCS,
							INT							&iLocalBitset)
							
	INT iUnitNumber
	INT iMaximum = FMMC_MAX_NUM_UNITS
			
	IF sFMMCMenu.iSelectedEntity = -1
	
		iUnitNumber = g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced
//		INT iEntity
//		FOR iEntity = 0 TO GET_NUMBER_OF_UNIT_VEHICLES(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType)-1
//			g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[iEntity].mnVehicleModel = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
//		ENDFOR
		g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel = GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
	ELSE
		
		iUnitNumber = sFMMCMenu.iSelectedEntity
		
		IF iUnitNumber > iMaximum 
			iUnitNumber = iMaximum - 1
			sFMMCMenu.iSelectedEntity = iUnitNumber
		ENDIF
		
		// Remove the existing vehicle if it already exists and update the creation data to the selected vehicles data
		IF DOES_ENTITY_EXIST(sUnitStruct.sEntities[iUnitNumber].viVehicles[0])
		OR DOES_ENTITY_EXIST(sUnitStruct.sEntities[iUnitNumber].piPeds[0])
			
			INT iEntity
			FOR iEntity = 0 TO FMMC_MAX_NUM_UNIT_VEHICLES-1
				IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.viVehicles[iEntity])
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(sUnitStruct.sCoronaEntities.viVehicles[iEntity]))
					DELETE_VEHICLE(sUnitStruct.sCoronaEntities.viVehicles[iEntity])
				ENDIF
			ENDFOR
			FOR iEntity = 0 TO FMMC_MAX_NUM_UNIT_PEDS-1
				IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.piPeds[iEntity])
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(sUnitStruct.sCoronaEntities.piPeds[iEntity]))
					DELETE_PED(sUnitStruct.sCoronaEntities.piPeds[iEntity])
				ENDIF
			ENDFOR
			FOR iEntity = 0 TO FMMC_MAX_NUM_UNIT_PROPS-1
				IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.oiProps[iEntity])
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(sUnitStruct.sCoronaEntities.oiProps[iEntity]))
					DELETE_OBJECT(sUnitStruct.sCoronaEntities.oiProps[iEntity])
				ENDIF
			ENDFOR
			
			CLEAR_CURRENT_UNIT_AND_SET_UP_SELECTED(sCurrentVarsStruct, sUnitStruct, iUnitNumber, sFMMCmenu)
			
			g_FMMC_STRUCT_ENTITIES.sEditedUnit = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber]
			
			sFMMCmenu.iVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[0].mnVehicleModel)
			sFMMCmenu.iVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[0].mnVehicleModel)
			
			sFMMCMenu.iPedLibrary = GET_PED_LIBRARY_FROM_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel, sFMMCMenu)
			sFMMCMenu.iPedType = GET_PED_TYPE_FROM_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel, sFMMCMenu)
		ENDIF
		
	ENDIF
	
	// Too many Vehs
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		AND sFMMCMenu.iSelectedEntity = -1
			STRING sMax = "FMMC_ER_TV"
			DO_TOO_MANY_ENTITY_ERROR(sCurrentVarsStruct, sUnitStruct.flashColour, sMax)
		ENDIF
	ENDIF
	
	BOOL bPlacementLimitReached
	IF sFMMCMenu.iSelectedEntity = -1
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced >= iMaximum	
		AND iUnitNumber >= iMaximum
			sUnitStruct.iCreationStage = CREATION_STAGE_WAIT
			SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
			bPlacementLimitReached = TRUE
			PRINTLN("DO_UNIT_CREATION - Too many Units")
		ENDIF
		
		IF (g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects >= FMMC_MAX_NUM_UNIT_OBJECTS_TOTAL AND GET_NUMBER_OF_UNIT_DYNOPROPS(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType) > 0)
		OR (GET_NUMBER_OF_PLACED_NETWORKED_OBJECTS() + GET_NUMBER_OF_UNIT_DYNOPROPS(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType)) > FMMC_MAX_NUM_UNIT_OBJECTS_TOTAL
			sUnitStruct.iCreationStage = CREATION_STAGE_WAIT
			SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_DYNOPROPS)
			bPlacementLimitReached = TRUE
			PRINTLN("DO_UNIT_CREATION - Too many Objects")
		ENDIF
		
		IF (g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds >= FMMC_MAX_NUM_UNIT_PEDS_TOTAL AND GET_NUMBER_OF_UNIT_PEDS(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType, g_FMMC_STRUCT_ENTITIES.sEditedUnit.iVariation) > 0)
		OR (g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds + GET_NUMBER_OF_UNIT_PEDS(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType, g_FMMC_STRUCT_ENTITIES.sEditedUnit.iVariation)) > FMMC_MAX_NUM_UNIT_PEDS_TOTAL
			sUnitStruct.iCreationStage = CREATION_STAGE_WAIT
			SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PEDS)
			bPlacementLimitReached = TRUE
			PRINTLN("DO_UNIT_CREATION - Too many Peds")
		ENDIF
		
		IF (g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles >= FMMC_MAX_NUM_UNIT_VEHICLES_TOTAL AND GET_NUMBER_OF_UNIT_VEHICLES(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType) > 0)
		OR (g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles + GET_NUMBER_OF_UNIT_VEHICLES(g_FMMC_STRUCT_ENTITIES.sEditedUnit.iUnitType)) > FMMC_MAX_NUM_UNIT_VEHICLES_TOTAL
			sUnitStruct.iCreationStage = CREATION_STAGE_WAIT
			SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
			bPlacementLimitReached = TRUE
			PRINTLN("DO_UNIT_CREATION - Too many Vehicles")
		ENDIF
	ENDIF
	
	IF bPlacementLimitReached
		IF sUnitStruct.flashColour = HUDFLASHING_NONE
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects, FMMC_MAX_NUM_UNIT_OBJECTS_TOTAL, "FMMC_AB_15", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles, FMMC_MAX_NUM_UNIT_VEHICLES_TOTAL, "FMMC_AB_02", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds, FMMC_MAX_NUM_UNIT_PEDS_TOTAL, "BSO_CATE_PED", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced,	iMaximum, "FMMC_UNT", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour)
		ELSE
			INT iFlashTime = 500
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects, FMMC_MAX_NUM_UNIT_OBJECTS_TOTAL, "FMMC_AB_15", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour, iFlashTime)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles, FMMC_MAX_NUM_UNIT_VEHICLES_TOTAL, "FMMC_AB_02", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour, iFlashTime)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds, FMMC_MAX_NUM_UNIT_PEDS_TOTAL, "BSO_CATE_PED", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour, iFlashTime)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced,	iMaximum, "FMMC_UNT", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour, iFlashTime)
		ENDIF
	ELSE
		sUnitStruct.flashColour = HUDFLASHING_NONE
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_DYNOPROPS)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PEDS)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects, FMMC_MAX_NUM_UNIT_OBJECTS_TOTAL, "FMMC_AB_15", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles, FMMC_MAX_NUM_UNIT_VEHICLES_TOTAL, "FMMC_AB_02", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds, FMMC_MAX_NUM_UNIT_PEDS_TOTAL, "BSO_CATE_PED", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced,	iMaximum, "FMMC_UNT", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sUnitStruct.flashColour)
	ENDIF
	
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
	OR IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_DYNOPROPS)
	OR IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PEDS)
		sHCS.hcCurrentCoronaColour = sHCS.hcColourCantPlace
		EXIT
	ENDIF
	HANDLE_MODEL_MEMORY_METER()
	
	IF PROCESS_NETWORKED_OBJECT_LIMIT(sCurrentVarsStruct)
		EXIT
	ENDIF
		
	// The switch case
	SWITCH sUnitStruct.iCreationStage
	
		// Remove any old vehicle before creating a new one. Make sure there aren't too many
		CASE CREATION_STAGE_WAIT
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced <= FMMC_MAX_NUM_UNITS
			AND iUnitNumber < FMMC_MAX_NUM_UNITS
				PRINTLN("DO_UNIT_CREATION - Moving to SET UP")
				sUnitStruct.iCreationStage = CREATION_STAGE_SET_UP
				DELETE_CORONA_UNIT_ENTITIES(sUnitStruct)
			ENDIF
		BREAK
		
		// Request model data
		CASE CREATION_STAGE_SET_UP
			
			IF NOT DOES_ENTITY_EXIST(sUnitStruct.oiBaseObject)
				IF ARE_UNIT_CREATION_MODELS_VALID()
					
					REQUEST_MODEL(PROP_LD_TEST_01)
					REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel)
					
					g_FMMC_STRUCT_ENTITIES.sEditedUnit.vPos = sCurrentVarsStruct.vCoronaPos
					g_FMMC_STRUCT_ENTITIES.sEditedUnit.fHeading = sCurrentVarsStruct.fCreationHeading
					
					IF HAS_MODEL_LOADED(PROP_LD_TEST_01)
					AND LOAD_UNIT_VEHICLE_MODELS()
					AND HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel)
						PRINTLN("DO_UNIT_CREATION - moving to MAKE")
						sUnitStruct.iCreationStage = CREATION_STAGE_MAKE
					ENDIF
					
				ELSE
					PRINTLN("DO_UNIT_CREATION, CREATION_STAGE_SET_UP, mnCreationModel INVALID ")
				ENDIF
			ELSE
				PRINTLN("DO_UNIT_CREATION - Moving to PLACE")
				sUnitStruct.iCreationStage = CREATION_STAGE_PLACE
			ENDIF
			
		BREAK
		
		// Create the new placement vehicle
		CASE CREATION_STAGE_MAKE
		
			IF CREATE_UNIT_BASE_OBJECT_FOR_PLACEMENT(sCurrentVarsStruct, sUnitStruct)
				IF CREATE_UNIT_VEHICLES_FOR_PLACEMENT(sCurrentVarsStruct, sUnitStruct, iUnitNumber)
				AND CREATE_UNIT_PEDS_FOR_PLACEMENT(sCurrentVarsStruct, sUnitStruct, iUnitNumber)
				AND CREATE_UNIT_DYNOPROPS_FOR_PLACEMENT(sCurrentVarsStruct, sUnitStruct, iUnitNumber)
					PRINTLN("DO_UNIT_CREATION - Moving to PLACE")
					sUnitStruct.iCreationStage = CREATION_STAGE_PLACE
				ENDIF
			ENDIF
			
			REFRESH_MENU(sFMMCmenu)
		BREAK
		
		// Do the placement of the veh, updating it's position/rotation/model/etc based on user input
		CASE CREATION_STAGE_PLACE
		
//			IF IS_ENTITY_ALIVE(sVehStruct.viCoronaVeh)
//				SET_UP_MODEL_MEMORY_METER(GET_ENTITY_MODEL(sVehStruct.viCoronaVeh), sCurrentVarsStruct)
//			ENDIF
						
			g_FMMC_STRUCT_ENTITIES.sEditedUnit.vPos = sCurrentVarsStruct.vCoronaPos
			g_FMMC_STRUCT_ENTITIES.sEditedUnit.fHeading = sCurrentVarsStruct.fCreationHeading
			
			IF DOES_ENTITY_EXIST(sUnitStruct.oiBaseObject)
			AND (DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.viVehicles[0]) OR DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.piPeds[0]))
			
				BOOL bSelectedEntity
				bSelectedEntity = IS_BIT_SET(iLocalBitset, biCanEditEntityInCorona)
				
				// Update the vehicle's position/rotation and check the area is safe to place in
				IF MOVE_LAST_PLACED_UNIT(sFMMCmenu, 
									sUnitStruct,
									iUnitNumber,
									g_FMMC_STRUCT_ENTITIES.sEditedUnit.vPos,
									g_FMMC_STRUCT_ENTITIES.sEditedUnit.vRotation,
									g_FMMC_STRUCT_ENTITIES.sEditedUnit.fHeading,
									sCurrentVarsStruct,
									sFMMCData,
									bSelectedEntity)				
					
					// Copy the edited data into the struct if it has been placed
					g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber] = g_FMMC_STRUCT_ENTITIES.sEditedUnit
					PRINTLN("DO_UNIT_CREATION - Moving to DONE")
					sUnitStruct.iCreationStage = CREATION_STAGE_DONE
					
				ENDIF
			ELSE
				
				IF DOES_ENTITY_EXIST(sUnitStruct.oiBaseObject)
					DELETE_OBJECT(sUnitStruct.oiBaseObject)
				ENDIF
				
				PRINTLN("DO_UNIT_CREATION - Moving to SET UP - Non existent entity")
				sUnitStruct.iCreationStage = CREATION_STAGE_SET_UP
			ENDIF
			
			//Go back through set up if the object model has changed
			IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.viVehicles[0])
			OR DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.piPeds[0])
				MODEL_NAMES mnToUse
				BOOL bVehChanged, bPedChanged
				IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.viVehicles[sFMMCmenu.iUnitEntity])
					mnToUse = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCmenu.iVehicleLibrary, sFMMCmenu.iVehicleType)
					bVehChanged = (mnToUse != GET_ENTITY_MODEL(sUnitStruct.sCoronaEntities.viVehicles[sFMMCmenu.iUnitEntity]))
				ENDIF
				IF DOES_ENTITY_EXIST(sUnitStruct.sCoronaEntities.piPeds[0])
					mnToUse = GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
					bPedChanged = (mnToUse != GET_ENTITY_MODEL(sUnitStruct.sCoronaEntities.piPeds[0]))
				ENDIF
				
				IF bVehChanged
				OR bPedChanged
				OR sUnitStruct.bRefresh
					
					DELETE_CORONA_UNIT_ENTITIES(sUnitStruct)
					
					PRINTLN("DO_UNIT_CREATION - Moving to SET UP - Entity change")
					sUnitStruct.iCreationStage = CREATION_STAGE_SET_UP
					
					IF sUnitStruct.bRefresh
						sUnitStruct.bRefresh = FALSE
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		// Update the ped you've just placed, increase the counter and reset for the next one
		CASE CREATION_STAGE_DONE
			
			sUnitStruct.sEntities[iUnitNumber].viVehicles = sUnitStruct.sCoronaEntities.viVehicles 
			sUnitStruct.sEntities[iUnitNumber].piPeds = sUnitStruct.sCoronaEntities.piPeds
			sUnitStruct.sEntities[iUnitNumber].oiProps = sUnitStruct.sCoronaEntities.oiProps
			
			SET_UP_PLACED_UNIT_VEHICLES(sUnitStruct, iUnitNumber)
			SET_UP_PLACED_UNIT_PEDS(sUnitStruct, iUnitNumber)
			SET_UP_PLACED_UNIT_DYNO_PROP(sUnitStruct, iUnitNumber)
					
			REAPPLY_NEW_2020_ENTITY_CREATION_DEFAULTS(sFMMCMenu)
			START_PLACEMENT_COOLDOWN_TIMER(sCurrentVarsStruct)
			sFMMCmenu.iSwitchCam = iUnitNumber
			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
			CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)
			
			CREATE_FMMC_BLIP(sUnitStruct.sEntities[iUnitNumber].biBlip, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, HUD_COLOUR_ORANGE, "FMMC_BLP_UNT", 1)
			
			// Increase the count if you've placed a new one
			IF sFMMCMenu.iSelectedEntity = -1
				
				g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced++
				
				g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles += GET_NUMBER_OF_UNIT_VEHICLES(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType)
				g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds += GET_NUMBER_OF_UNIT_PEDS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iVariation)
				g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects += GET_NUMBER_OF_UNIT_DYNOPROPS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType)
				
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced > FMMC_MAX_NUM_UNITS
					g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced = FMMC_MAX_NUM_UNITS
				ENDIF
			ELSE
				CORRECT_EDITED_UNIT_PLACEMENT_COUNTS()
			ENDIF
			
			INT iEntity
			FOR iEntity = 0 TO FMMC_MAX_NUM_UNIT_VEHICLES-1
				sUnitStruct.sCoronaEntities.viVehicles[iEntity] = NULL
			ENDFOR
			FOR iEntity = 0 TO FMMC_MAX_NUM_UNIT_PEDS-1
				sUnitStruct.sCoronaEntities.piPeds[iEntity] = NULL
			ENDFOR
			FOR iEntity = 0 TO FMMC_MAX_NUM_UNIT_PROPS-1
				sUnitStruct.sCoronaEntities.oiProps[iEntity] = NULL
			ENDFOR
			
			sFMMCMenu.iSelectedEntity = -1
			PRINTLN("DO_UNIT_CREATION - Moving to WAIT")
			sUnitStruct.iCreationStage = CREATION_STAGE_WAIT
			
		BREAK
	ENDSWITCH
	
	UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE)
	
ENDPROC

PROC MOVE_UNIT_ARRAY_DOWN_1(UNIT_CREATION_STRUCT &sUnitStruct, INT iStart)
	
	INT iUnit, iEntity
	FMMC_UNITS_STRUCT sTempUnitStruct
	
	PRINTLN("MOVE_UNIT_ARRAY_DOWN_1 - Called on unit: ", iStart)
	DEBUG_PRINTCALLSTACK()
	
	INT iUnitType = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iStart].iUnitType
	g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles -= GET_NUMBER_OF_UNIT_VEHICLES(iUnitType)
	g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds -= GET_NUMBER_OF_UNIT_PEDS(iUnitType, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iStart].iVariation)
	g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects -= GET_NUMBER_OF_UNIT_DYNOPROPS(iUnitType)
	
	FOR iUnit = iStart+1 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced-1)
	
		IF DOES_BLIP_EXIST(sUnitStruct.sEntities[iUnit].biBlip)
			REMOVE_BLIP(sUnitStruct.sEntities[iUnit].biBlip)
		ENDIF
		
		IF DOES_BLIP_EXIST(sUnitStruct.sEntities[iUnit-1].biBlip)
			REMOVE_BLIP(sUnitStruct.sEntities[iUnit-1].biBlip)
		ENDIF
				
		sUnitStruct.sEntities[iUnit-1].biBlip = sUnitStruct.sEntities[iUnit].biBlip
		sUnitStruct.sEntities[iUnit-1].viVehicles = sUnitStruct.sEntities[iUnit].viVehicles
		sUnitStruct.sEntities[iUnit-1].piPeds = sUnitStruct.sEntities[iUnit].piPeds
		sUnitStruct.sEntities[iUnit-1].oiProps = sUnitStruct.sEntities[iUnit].oiProps
		g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit-1] = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit]
		
		CREATE_FMMC_BLIP(sUnitStruct.sEntities[iUnit-1].biBlip, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit-1].vPos, HUD_COLOUR_ORANGE, "FMMC_BLP_UNT", 1)
		
		sUnitStruct.sEntities[iUnit].biBlip	= NULL
		FOR iEntity = 0 TO FMMC_MAX_NUM_UNIT_VEHICLES-1
			sUnitStruct.sEntities[iUnit].viVehicles[iEntity] = NULL
		ENDFOR
		FOR iEntity = 0 TO FMMC_MAX_NUM_UNIT_PEDS-1
			sUnitStruct.sEntities[iUnit].piPeds[iEntity] = NULL
		ENDFOR
		FOR iEntity = 0 TO FMMC_MAX_NUM_UNIT_PROPS-1
			sUnitStruct.sEntities[iUnit].oiProps[iEntity] = NULL
		ENDFOR
		g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit] = sTempUnitStruct
		
	ENDFOR
	
	g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced--
	IF  g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced < 0
		 g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced = 0
	ENDIF
	
ENDPROC

PROC DELETE_LAST_PLACED_UNIT(UNIT_CREATION_STRUCT &sUnitStruct, INT iUnitNumber)
	
	DELETE_PLACED_UNIT_ENTITIES(sUnitStruct, iUnitNumber)
	MOVE_UNIT_ARRAY_DOWN_1(sUnitStruct, iUnitNumber)
	
ENDPROC

PROC UNLOAD_ALL_UNIT_MODELS(structFMMC_MENU_ITEMS &sFMMCmenu)
	UNUSED_PARAMETER(sFMMCmenu)
	INT iEntity
	FOR iEntity = 0 TO FMMC_MAX_NUM_UNIT_VEHICLES-1
		SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sEditedUnit.sUnitVeh[iEntity].mnVehicleModel)
	ENDFOR
	SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sEditedUnit.mnPedModel)
	PRINTLN("UNLOAD_ALL_UNIT_MODELS")
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Creator Goto/Search
// ##### Description: Shared wrappers for the creator functionality of searching a menu.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD
FUNC BOOL HAS_CREATOR_SEARCH_BEEN_TRIGGERED()

	IF NOT FMMC_IS_ROCKSTAR_DEV()
		RETURN FALSE
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
	AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
		RETURN TRUE
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
	AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
#ENDIF

FUNC TEXT_LABEL_23 GET_DESCRIPTION_AS_A_TEXT_LABEL(structFMMC_MENU_ITEMS &sFMMCmenu)
	
	IF sFMMCmenu.itlOptionDescriptionSelection != -1
	AND sFMMCmenu.itlOptionDescriptionSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
	AND sFMMCmenu.stlOptionDescriptionActiveMenu = sFMMCmenu.sActiveMenu
		RETURN sFMMCmenu.tlOptionDescription
	ENDIF
	
	TEXT_LABEL_23 tl23
	
	SWITCH sFMMCMenu.sActiveMenu
		CASE eFmmc_DIALOGUE_TRIGGER_BASE		
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ID
				IF DOES_TEXT_BLOCK_EXIST(sFMMCMenu.sDialogueTrigger.tlBlock)
					REQUEST_ADDITIONAL_TEXT_FOR_DLC(sFMMCMenu.sDialogueTrigger.tlBlock, DLC_MISSION_DIALOGUE_TEXT_SLOT)
					
					tl23 = sFMMCMenu.sDialogueTrigger.tlRoot
					tl23 += "_1"
				ENDIF
			ENDIF
		BREAK	
	ENDSWITCH
	
	RETURN tl23
ENDFUNC

FUNC STRING GET_SELECTED_ITEM_STRING_DESCRIPTION_BASE(INT iType)
	SWITCH iType
		CASE CREATION_TYPE_VEHICLES
			RETURN "FMMC_SELV"
		BREAK
		CASE CREATION_TYPE_WEAPONS
			IF IS_THIS_A_SURVIVAL()
				RETURN "FMMC_SEL_PUSP"
			ELSE
				RETURN "FMMC_SELW"
			ENDIF
		BREAK
		CASE CREATION_TYPE_PROPS
			RETURN "FMMC_SELP"
		BREAK
		CASE CREATION_TYPE_DYNOPROPS
			RETURN "FMMC_SELD"
		BREAK
		CASE CREATION_TYPE_WORLD_PROPS
			RETURN "FMMC_SELFR"
		BREAK
		CASE CREATION_TYPE_HILL
			RETURN "FMMC_HILL"
		BREAK
	ENDSWITCH
	
	RETURN "FMMC_SELDF"
ENDFUNC

PROC PROCESS_SHOWING_HIGHLIGHTED_ENTITY(CREATION_VARS_STRUCT &sCurrentVarsStruct)
	IF sCurrentVarsStruct.iSelectedDescriptionTimer = GET_GAME_TIMER()
		SET_CURRENT_MENU_ITEM_DESCRIPTION("FMMC_SEL", 0, MENU_ICON_ALERT)	
		ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SELECTED_ITEM_STRING_DESCRIPTION_BASE(sCurrentVarsStruct.iHoverEntityType))
		ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(sCurrentVarsStruct.sSelectedName)
	ELSE
		sCurrentVarsStruct.bDisplaySelected = FALSE
	ENDIF
ENDPROC

PROC SET_MENU_ALERT(structFMMC_MENU_ITEMS& sFMMCmenu, INT iAlert)
	IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, iAlert)
		PRINTLN("SET_MENU_ALERT - Setting bit ", iAlert)
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, iAlert)
	ENDIF
ENDPROC

PROC SET_MENU_ALERT_2(structFMMC_MENU_ITEMS& sFMMCmenu, INT iAlert)
	IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, iAlert)
		PRINTLN("SET_MENU_ALERT_2 - Setting bit ", iAlert)
		SET_BIT(sFMMCmenu.iBitMenuItemAlert2, iAlert)
	ENDIF
ENDPROC

PROC CLEAR_MENU_ALERT(structFMMC_MENU_ITEMS& sFMMCmenu, INT iAlert)
	IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, iAlert)
		PRINTLN("CLEAR_MENU_ALERT - Clearing bit ", iAlert)
		CLEAR_BIT(sFMMCmenu.iBitMenuItemAlert, iAlert)
	ENDIF
ENDPROC

PROC CLEAR_MENU_ALERT_2(structFMMC_MENU_ITEMS& sFMMCmenu, INT iAlert)
	IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, iAlert)
		PRINTLN("CLEAR_MENU_ALERT_2 - Clearing bit ", iAlert)
		CLEAR_BIT(sFMMCmenu.iBitMenuItemAlert2, iAlert)
	ENDIF
ENDPROC
