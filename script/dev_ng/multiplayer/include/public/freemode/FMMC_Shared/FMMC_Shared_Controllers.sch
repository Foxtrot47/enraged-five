USING "FMMC_Shared_Common.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Controller Entity Helpers
// ##### Description: Helpers for dealing with entities in the controllers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Train Controller Entity Helpers
// ##### Description: Helpers for dealing with Objects in the controllers

FUNC BOOL REQUEST_FMMC_TRAIN_ASSETS()
	REQUEST_MODEL(FREIGHT)
	REQUEST_MODEL(FREIGHTCAR)
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("FREIGHTCAR2")))
	REQUEST_MODEL(FREIGHTGRAIN)
	REQUEST_MODEL(FREIGHTCONT1)
	REQUEST_MODEL(FREIGHTCONT2)
	REQUEST_MODEL(TANKERCAR)
	REQUEST_MODEL(METROTRAIN)
	
	IF HAS_MODEL_LOADED(FREIGHT)
	AND HAS_MODEL_LOADED(FREIGHTCAR)
	AND HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("FREIGHTCAR2")))
	AND HAS_MODEL_LOADED(FREIGHTGRAIN)
	AND HAS_MODEL_LOADED(FREIGHTCONT1)
	AND HAS_MODEL_LOADED(FREIGHTCONT2)
	AND HAS_MODEL_LOADED(TANKERCAR)
	AND HAS_MODEL_LOADED(METROTRAIN)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RELEASE_FMMC_TRAIN_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHT)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCAR)
	SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("FREIGHTCAR2")))
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTGRAIN)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT1)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT2)
	SET_MODEL_AS_NO_LONGER_NEEDED(TANKERCAR)
	SET_MODEL_AS_NO_LONGER_NEEDED(METROTRAIN)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Controller Ped Entity Helpers
// ##### Description: Helpers for dealing with Peds in the controllers

FUNC STRING GET_DEAD_PED_ANIM_DICT_NAME(INT iDeadAnim)
	SWITCH iDeadAnim
		CASE ci_DEAD_PED_ANIM_FBI_CPR_IDLE			RETURN "missfbi1"
		CASE ci_DEAD_PED_ANIM_FBI_DEAD_C			RETURN "missfbi5ig_12"
		CASE ci_DEAD_PED_ANIM_DEAD_A				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_B				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_C				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_D				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_E				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_F				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_G				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_DEAD_H				RETURN "dead"
		CASE ci_DEAD_PED_ANIM_IAA_MORGUE_SEARCH		RETURN "anim@GangOps@Morgue@Table@"
	ENDSWITCH
	RETURN "missfbi1"
ENDFUNC
FUNC STRING GET_DEAD_PED_ANIM_NAME(INT iDeadAnim)
	SWITCH iDeadAnim
		CASE ci_DEAD_PED_ANIM_FBI_CPR_IDLE 			RETURN "cpr_pumpchest_idle"
		CASE ci_DEAD_PED_ANIM_FBI_DEAD_C 			RETURN "dead_c"
		CASE ci_DEAD_PED_ANIM_DEAD_A 				RETURN "dead_a"
		CASE ci_DEAD_PED_ANIM_DEAD_B 				RETURN "dead_b"
		CASE ci_DEAD_PED_ANIM_DEAD_C 				RETURN "dead_c"
		CASE ci_DEAD_PED_ANIM_DEAD_D 				RETURN "dead_d"
		CASE ci_DEAD_PED_ANIM_DEAD_E 				RETURN "dead_e"
		CASE ci_DEAD_PED_ANIM_DEAD_F 				RETURN "dead_f"
		CASE ci_DEAD_PED_ANIM_DEAD_G 				RETURN "dead_g"
		CASE ci_DEAD_PED_ANIM_DEAD_H 				RETURN "dead_h"
		CASE ci_DEAD_PED_ANIM_IAA_MORGUE_SEARCH 	RETURN "Body_Search"
	ENDSWITCH
	RETURN "cpr_pumpchest_idle"
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Object Controller Entity Helpers
// ##### Description: Helpers for dealing with Objects in the controllers

PROC SET_UP_TROLLEY_STARTING_AMOUNT(MODEL_NAMES mnToCheck, OBJECT_INDEX oiObj, INT iNumCashPiles = 0)
	FLOAT fAnimPhase
	IF mnToCheck = PROP_GOLD_TROLLY_FULL
	OR mnToCheck = CH_PROP_GOLD_TROLLY_01A
	OR mnToCheck = CH_PROP_GOLD_TROLLY_01B
	OR mnToCheck = CH_PROP_GOLD_TROLLY_01C
		fAnimPhase = 0.5037
		PRINTLN("SET_UP_GOLD_TROLLEY_STARTING_AMOUNT Setting up gold trolley. Anim phase: ", fAnimPhase)
	ENDIF
	
	IF mnToCheck = CH_PROP_CASH_LOW_TROLLY_01A
	OR mnToCheck = CH_PROP_CASH_LOW_TROLLY_01B
	OR mnToCheck = CH_PROP_CASH_LOW_TROLLY_01C
		IF iNumCashPiles != 0
			fAnimPhase = (iNumCashPiles * 0.0219)
			PRINTLN("SET_UP_TROLLEY_STARTING_AMOUNT Setting up cash grab trolley using fAnimPhaseOverride. Anim phase: ", fAnimPhase)
		ENDIF
	ENDIF
	
	IF fAnimPhase != 0.0
		
		INT iTrolleySceneID
		iTrolleySceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(oiObj),
															GET_ENTITY_ROTATION(oiObj),
															EULER_YXZ, TRUE, FALSE, DEFAULT,
															fAnimPhase, 0.0)
						
		IF DOES_ENTITY_EXIST(oiObj)
			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(oiObj, iTrolleySceneID,
													 "anim@heists@ornate_bank@grab_cash" ,"cart_cash_dissapear",
													 INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiObj)
		ENDIF
	
		NETWORK_START_SYNCHRONISED_SCENE(iTrolleySceneID)
	ENDIF
ENDPROC

FUNC INT GET_NETWORKED_OBJECT_COUNT()
	RETURN g_FMMC_STRUCT_ENTITIES.iNumberOfObjects + g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps + g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables
ENDFUNC

FUNC BOOL IS_THIS_OBJECT_A_HACK_CONTAINER( INT iContainerIndex )
	BOOL bReturn = FALSE
	INT iThisTeam = 0
	
	FOR iThisTeam = 0 TO FMMC_MAX_TEAMS - 1	
		IF ( g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iContainerIndex ].mn = Prop_Container_LD )
		AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iContainerIndex ].iRule[ iThisTeam ] = FMMC_OBJECTIVE_LOGIC_MINIGAME
			bReturn = TRUE
		ENDIF
	ENDFOR
	
	RETURN bReturn
ENDFUNC

FUNC BOOL DO_CRATE_DOORS_EXIST( NETWORK_INDEX& niDoors[], BOOL bCheckLock = TRUE )
	BOOL bReturn = FALSE
	
	INT left 	= 0
	INT right 	= 1
	INT lock	= 2
	
	IF  NETWORK_DOES_NETWORK_ID_EXIST( niDoors[ left ] )
	AND NETWORK_DOES_NETWORK_ID_EXIST( niDoors[ right ] )
		IF bCheckLock AND NETWORK_DOES_NETWORK_ID_EXIST( niDoors[ lock ] )
			bReturn = true
		ELIF NOT bCheckLock
			bReturn = TRUE
		ENDIF
	ENDIF

	RETURN bReturn
ENDFUNC

FUNC BOOL HAS_OBJECT_CREATION_FINISHED( NETWORK_INDEX niObj, INT iIndex, NETWORK_INDEX& niDoors[], MODEL_NAMES mnObj )
	BOOL bReturn = TRUE
	
	IF mnObj != DUMMY_MODEL_FOR_SCRIPT 
	AND NOT NETWORK_DOES_NETWORK_ID_EXIST( niObj )
		bReturn = FALSE
	ENDIF
	
	IF IS_THIS_OBJECT_A_HACK_CONTAINER( iIndex )
	AND NOT DO_CRATE_DOORS_EXIST( niDoors )
		bReturn = FALSE
	ENDIF

	RETURN bReturn
ENDFUNC

FUNC BOOL HAS_CONTAINER_AND_ASSETS_LOADED()
	
	BOOL bReturn = FALSE
	
	MODEL_NAMES	mnContainerModel		= Prop_Container_LD
	MODEL_NAMES	mnContainerLeftDoor 	= hei_Prop_CntrDoor_MPH_L
	MODEL_NAMES	mnContainerRightDoor 	= hei_Prop_CntrDoor_MPH_R
	MODEL_NAMES mnContainerLock			= Hei_Prop_Container_Lock
	
	REQUEST_MODEL(mnContainerModel)
	REQUEST_MODEL(mnContainerLeftDoor)
	REQUEST_MODEL(mnContainerRightDoor)
	REQUEST_MODEL(mnContainerLock)
	
	IF 	HAS_MODEL_LOADED(mnContainerModel)
	AND HAS_MODEL_LOADED(mnContainerLeftDoor)
	AND HAS_MODEL_LOADED(mnContainerRightDoor)
	AND HAS_MODEL_LOADED(mnContainerLock)
		bReturn = TRUE
	ENDIF
	
	RETURN bReturn
	
ENDFUNC

FUNC BOOL CREATE_AND_ATTACH_CRATE_DOORS( 	NETWORK_INDEX& niCrate, 
											NETWORK_INDEX& niDoors[],
											VECTOR& vPos )
											
	BOOL bReturn = FALSE

	MODEL_NAMES	mnContainerLeftDoor 	= hei_Prop_CntrDoor_MPH_L//Prop_CntrDoor_LD_L
	MODEL_NAMES	mnContainerRightDoor 	= hei_Prop_CntrDoor_MPH_R//Prop_CntrDoor_LD_R
	MODEL_NAMES mnContainerLock			= Hei_Prop_Container_Lock
	
	INT left 	= 0
	INT right 	= 1
	INT lock 	= 2
	
	// The offsets at which the doors will be placed
	VECTOR vLeftDoorOffset 	= << 1.295, 6.075, -1.4 >>
	VECTOR vRightDoorOffset = << -1.295, 6.075, -1.4 >>
	VECTOR vLockOffset = << -0.001, 6.130, -1.7 >>
	
	VECTOR vRot = <<0,0,0>>
	
	IF HAS_CONTAINER_AND_ASSETS_LOADED()
	AND NOT DO_CRATE_DOORS_EXIST( niDoors )
		
		niDoors[ left ] 	=  OBJ_TO_NET( CREATE_OBJECT( mnContainerLeftDoor, vPos + vLeftDoorOffset ) )   // Left Door
		niDoors[ right ] 	=  OBJ_TO_NET( CREATE_OBJECT( mnContainerRightDoor, vPos + vRightDoorOffset) ) // Right Door
		niDoors[ lock ] 	=  OBJ_TO_NET( CREATE_OBJECT( mnContainerLock, vPos + vLockOffset) ) // Lock
		
		SET_ENTITY_VISIBLE(NET_TO_ENT(niDoors[lock]),TRUE)
		
		SET_ENTITY_PROOFS( NET_TO_OBJ( niDoors[ left ] ), TRUE, TRUE, TRUE, TRUE, TRUE )
		SET_ENTITY_PROOFS( NET_TO_OBJ( niDoors[ right ] ), TRUE, TRUE, TRUE, TRUE, TRUE )
		SET_ENTITY_PROOFS( NET_TO_OBJ( niDoors[ lock ] ), TRUE, TRUE, TRUE, TRUE, TRUE )
		
	ENDIF
	
	IF DO_CRATE_DOORS_EXIST( niDoors )
	
		ATTACH_ENTITY_TO_ENTITY( NET_TO_OBJ( niDoors[ left ] ), NET_TO_OBJ( niCrate ), -1, -vLeftDoorOffset, vRot, DEFAULT, TRUE, TRUE )
		ATTACH_ENTITY_TO_ENTITY( NET_TO_OBJ( niDoors[ right ] ), NET_TO_OBJ( niCrate ), -1, -vRightDoorOffset, vRot, DEFAULT, TRUE, TRUE )
		ATTACH_ENTITY_TO_ENTITY( NET_TO_OBJ( niDoors[ lock ] ), NET_TO_OBJ( niCrate ), -1, -vLockOffset, vRot, DEFAULT, TRUE, TRUE )
		
		bReturn = true
	ENDIF
	
	RETURN bReturn
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Controller Prop Entity Helpers
// ##### Description: Helpers for dealing with Vehicles in the controllers

FUNC BOOL IS_INFLATABLE_STUNT_PROP(MODEL_NAMES mnProp)
	IF IS_PROP_IN_LIBRARY(mnProp, PROP_LIBRARY_STUNT_INFLATABLE_GATES)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_OBJECT_BE_UNFROZEN(OBJECT_INDEX oiPassed)
	
	MODEL_NAMES mnProp = GET_ENTITY_MODEL(oiPassed)
	
	IF IS_MODEL_A_SEA_MINE(mnProp)
	OR IS_TRAP_A_FIRE_PIT(mnProp)
	OR GET_MODEL_NAME_OF_CHILD_PROP_FOR_DYNO(mnProp) != DUMMY_MODEL_FOR_SCRIPT // [ChildDyno]
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_OBJECT_BE_FROZEN(OBJECT_INDEX oiPassed)
	
	MODEL_NAMES mnProp = GET_ENTITY_MODEL(oiPassed)
	
	IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_01"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_02"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_03"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_04"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_05"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_06"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_07"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_08"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_09"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_10"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_11"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_12"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_13"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_corner_sign_14"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Pit_Sign_01a"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sum_prop_ac_pit_sign_Left"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_FLARE_01"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_03"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_01"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_boxpile_01"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_boxpile_02"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_01"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_02"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_03"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_track_wall"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_tube_wall"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ar_Prop_AR_Ammu_Sign"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_mp_spec_races_ammu_sign"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Mine_01a"))
	OR IS_INFLATABLE_STUNT_PROP(mnProp)
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Track_ExShort"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Track_Short"))
		PRINTLN("SHOULD_OBJECT_BE_FROZEN - not freezing object")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_MODEL_TARGETABLE(MODEL_NAMES mnProp)
	IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ar_Prop_AR_Ammu_Sign"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_mp_spec_races_ammu_sign"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_track_wall"))
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_tube_wall"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_REMOVING_TARGETTABLE_FROM_DYNO_PROP(OBJECT_INDEX oiProp)
	
	IF NOT DOES_ENTITY_EXIST(oiProp)
		EXIT
	ENDIF
	
	MODEL_NAMES mnProp = GET_ENTITY_MODEL(oiProp)
	
	IF NOT IS_MODEL_TARGETABLE(mnProp)
		EXIT
	ENDIF
	
	IF GET_OBJECT_FRAGMENT_DAMAGE_HEALTH(oiProp, FALSE) <= 0.75
		SET_OBJECT_TARGETTABLE(oiProp, FALSE)
	ENDIF
	
ENDPROC

FUNC INT GET_SPEED_UP_AMOUNT(INT iCurrentSlowAmount)

#IF IS_DEBUG_BUILD	
	IF i_SPEED_OverrideAll > 0
		RETURN i_SPEED_OverrideAll
	ENDIF
	SWITCH iCurrentSlowAmount
		CASE 1					
		IF i_SPEED_Small > 0
			PRINTLN("[LM][GET_SPEED_UP_AMOUNT] Using Rag Amount Value: ", i_SPEED_Small)
			RETURN i_SPEED_Small
		ENDIF
		BREAK		
		CASE 2			
		IF i_SPEED_Medium > 0
			PRINTLN("[LM][GET_SPEED_UP_AMOUNT] Using Rag Amount Value: ", i_SPEED_Medium)
			RETURN i_SPEED_Medium
		ENDIF	
		BREAK		
		CASE 3	
		IF i_SPEED_Large > 0
			PRINTLN("[LM][GET_SPEED_UP_AMOUNT] Using Rag Amount Value: ", i_SPEED_Large)
			RETURN i_SPEED_Large
		ENDIF
		BREAK		
		CASE 4			
		IF i_SPEED_Extra > 0
			PRINTLN("[LM][GET_SPEED_UP_AMOUNT] Using Rag Amount Value: ", i_SPEED_Extra)
			RETURN i_SPEED_Extra
		ENDIF
		BREAK
		CASE 5			
		IF i_SPEED_ULTRA > 0
			PRINTLN("[LM][GET_SPEED_UP_AMOUNT] Using Rag Amount Value: ", i_SPEED_ULTRA)
			RETURN i_SPEED_ULTRA
		ENDIF
		BREAK
	ENDSWITCH
#ENDIF

	SWITCH iCurrentSlowAmount
		CASE 1					
		RETURN 15
		
		CASE 2			
		RETURN 25
		
		CASE 3	
		RETURN 35
		
		CASE 4			
		RETURN 45

		CASE 5			
		RETURN 100
	ENDSWITCH
	
	RETURN 25
ENDFUNC
FUNC INT GET_SLOW_DOWN_AMOUNT(INT iCurrentSlowAmount)

#IF IS_DEBUG_BUILD
	IF i_SLOW_OverrideAll > 0
		RETURN i_SLOW_OverrideAll
	ENDIF
	SWITCH iCurrentSlowAmount
		CASE 1			
		IF i_SLOW_Small > 0
			PRINTLN("[LM][GET_SLOW_DOWN_AMOUNT] Using Rag Amount Value: ", i_SLOW_Small)	
			RETURN i_SLOW_Small
		ENDIF
		BREAK
		CASE 2			
		IF i_SLOW_Medium > 0
			PRINTLN("[LM][GET_SLOW_DOWN_AMOUNT] Using Rag Amount Value: ", i_SLOW_Medium)	
			RETURN i_SLOW_Medium
		ENDIF
		BREAK
		CASE 3			
		IF i_SLOW_LARGE > 0
			PRINTLN("[LM][GET_SLOW_DOWN_AMOUNT] Using Rag Amount Value: ", i_SLOW_LARGE)	
			RETURN i_SLOW_LARGE
		ENDIF
		BREAK
	ENDSWITCH
#ENDIF	

	SWITCH iCurrentSlowAmount
		CASE 1			
		RETURN 44
		
		CASE 2			
		RETURN 30
		
		CASE 3			
		RETURN 16
	ENDSWITCH
	RETURN 30
ENDFUNC

FUNC FLOAT GET_SPEED_BOOST_TIME(INT iCurrentSpeedAmount)
	
#IF IS_DEBUG_BUILD
	IF f_TIME_OverrideAll > 0
		RETURN f_TIME_OverrideAll
	ENDIF
	SWITCH iCurrentSpeedAmount
		CASE 1			
		IF f_TIME_Small > 0
			PRINTLN("[LM][GET_SPEED_BOOST_TIME] Using Rag Time Value: ", f_TIME_Small)	
			RETURN f_TIME_Small
		ENDIF
		BREAK
		CASE 2			
		IF f_TIME_Medium > 0
			PRINTLN("[LM][GET_SPEED_BOOST_TIME] Using Rag Time Value: ", f_TIME_Medium)	
			RETURN f_TIME_Medium
		ENDIF
		BREAK
		CASE 3			
		IF f_TIME_Large > 0
			PRINTLN("[LM][GET_SPEED_BOOST_TIME] Using Rag Time Value: ", f_TIME_Large)	
			RETURN f_TIME_Large
		ENDIF
		BREAK
		CASE 4			
		IF f_TIME_Extra > 0
			PRINTLN("[LM][GET_SPEED_BOOST_TIME] Using Rag Time Value: ", f_TIME_Extra)	
			RETURN f_TIME_Extra
		ENDIF
		BREAK
		CASE 5			
		IF f_TIME_Ultra > 0
			PRINTLN("[LM][GET_SPEED_BOOST_TIME] Using Rag Time Value: ", f_TIME_Ultra)	
			RETURN f_TIME_Ultra			
		ENDIF
		BREAK
	ENDSWITCH
#ENDIF

	SWITCH iCurrentSpeedAmount
		CASE 1			
		RETURN 0.3
		
		CASE 2			
		RETURN 0.4
		
		CASE 3			
		RETURN 0.5
		
		CASE 4			
		RETURN 0.5		
		
		CASE 5			
		RETURN 0.5
	ENDSWITCH
	
	RETURN 0.4
ENDFUNC

PROC INITIALIZE_SPEED_BOOST_PROPS(INT iProp, OBJECT_INDEX oiProp)
	// Speed Up Pad
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp_T1"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SpeedUp_T2"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_speed"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_speedb"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_prop_ar_speed_ring"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_Prop_AR_Tube_Speed"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_Prop_AR_Tube_2x_Speed"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_Prop_AR_Tube_4x_Speed"))
		IF DOES_ENTITY_EXIST(oiProp)	
			PRINTLN("INITIALIZE_SPEED_BOOST_PROPS, SET_OBJECT_SPEED_BOOST_AMOUNT,   iProp = ", iProp, " GET_SPEED_DOWN_AMOUNT = ", GET_SPEED_UP_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
			SET_OBJECT_SPEED_BOOST_AMOUNT(oiProp, GET_SPEED_UP_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
			PRINTLN("INITIALIZE_SPEED_BOOST_PROPS, SET_OBJECT_SPEED_BOOST_DURATION, iProp = ", iProp, " GET_SPEED_BOOST_TIME = ", GET_SPEED_BOOST_TIME(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
			SET_OBJECT_SPEED_BOOST_DURATION(oiProp, GET_SPEED_BOOST_TIME(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
		
			SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Requires_Alpha_Flash)
			PRINTLN("[LM][PROCESS_PROPS] - ciFMMC_PROP_Requires_Alpha_Flash has been set on iProp: ", iProp)
		ENDIF
	ENDIF
	// Slow Down Pad
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T1"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T2"))
	OR IS_PROP_CAPABLE_OF_SLOW_DOWN(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn)
		PRINTLN("INITIALIZE_SPEED_BOOST_PROPS, SET_OBJECT_SPEED_BOOST_AMOUNT,   iProp = ", iProp, " GET_SPEED_DOWN_AMOUNT = ", GET_SLOW_DOWN_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
		SET_OBJECT_SPEED_BOOST_AMOUNT(oiProp, GET_SLOW_DOWN_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpeedBoostAmount))
	ENDIF
ENDPROC

PROC INITIALIZE_SOUND_TRIGGER_PROPS(INT iProp, OBJECT_INDEX oiProp, BOOL bReveal)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_speakerstack_01a"))
		IF DOES_ENTITY_EXIST(oiProp)
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSTMTriggerCounter = 0
			CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Sound_Trigger_Played_This_Lap)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Sound_Trigger_Is_Invisible)
				IF bReveal = TRUE
					SET_ENTITY_ALPHA(oiProp, 255, bReveal)
					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiProp, bReveal)
				ELSE
					SET_ENTITY_ALPHA(oiProp, 0, bReveal)
					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiProp, bReveal)
				ENDIF	
			ENDIF	
		ENDIF		
	ENDIF
ENDPROC

FUNC BOOL IS_PROP_A_BOMBABLE_BLOCK(INT iPropNumber)
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Track_ExShort")) //If this prop is a bombable block...
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Track_Short")))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROP_A_CCTV_WALL_ATTACHMENT(INT iPropNumber)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_cctv_wall_atta_01a"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: This function sets the passed prop up correctly and visible/invisible as required
///    EXCEPT for the model PROP_LD_ALARM_01, which will ALWAYS be set as invisible!
PROC SET_UP_FMMC_PROP_RC( OBJECT_INDEX oiPassed, INT iPropNumber, BOOL bVisible = TRUE )

	PRINTLN("[SET_UP_FMMC_PROP_RC] iPropNumber: ", iPropNumber, " mn: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn))

	SET_ENTITY_COORDS(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].vPos)
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].vRot)
		SET_ENTITY_ROTATION(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].vRot)
	ELSE
		SET_ENTITY_HEADING(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].fHead)
	ENDIF
	IF GET_ENTITY_MODEL(oiPassed) != PROP_LD_ALARM_01
		IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
			SET_ENTITY_VISIBLE(oiPassed, TRUE)
		ELSE
			SET_ENTITY_VISIBLE(oiPassed, bVisible)
		ENDIF
	ELSE
		SET_ENTITY_VISIBLE(oiPassed, FALSE)
	ENDIF

	INITIALIZE_SPEED_BOOST_PROPS(iPropNumber, oiPassed)
	INITIALIZE_SOUND_TRIGGER_PROPS(iPropNumber, oiPassed, bVisible)
	CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitset, ciFMMC_PROP_PTFX_Played)

	SET_OBJECT_TARGETTABLE(oiPassed, FALSE)
	SET_WAIT_FOR_COLLISIONS_BEFORE_PROBE( oiPassed, TRUE )
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iLODDistance > 0
		PRINTLN("SET_UP_FMMC_PROP_RC - Setting prop ", iPropNumber, "'s LOD distance to ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iLODDistance)
		SET_ENTITY_LOD_DIST(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iLODDistance)
	ENDIF
		
	IF NOT IS_PROP_WATER_TYPE(GET_ENTITY_MODEL(oiPassed)) //Water props need to behave correctly in water
		IF SHOULD_OBJECT_BE_FROZEN(oiPassed)
			SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
			SET_ENTITY_PROOFS(oiPassed, TRUE,TRUE,TRUE,TRUE,TRUE)
			FREEZE_ENTITY_POSITION(oiPassed, TRUE)
			SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY(oiPassed,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bEnableAmericanFlagStuntRaces
		PRINTLN("[LH][FlagColours] g_sMPTunables.bEnableAmericanFlagStuntRaces = TRUE")
		PRINTLN("[LH][FlagColours] Setting g_FMMC_STRUCT_ENTITIES.sPlacedProp[",iPropNumber,"] from ",g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropColour," to 0")
		g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropColour = 0
	ENDIF
	IF CAN_SET_MODEL_TINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn)
		PRINTLN("[SET_UP_FMMC_PROP_RC] Setting TINT g_FMMC_STRUCT_ENTITIES.sPlacedProp[",iPropNumber,"] with tint index ",g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropColour)
		SET_OBJECT_TINT_INDEX(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropColour)
	ENDIF
	IF IS_MODEL_STUNT_PROP(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_ARENA)
		SET_IS_EXTERIOR_ONLY(oiPassed, TRUE)
	ENDIF
	//Target Race Time Bonus
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TARGET_PRACTICE)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_s_01a"))
		OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_m_01a"))
		OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_b_01a"))
			SET_OBJECT_TINT_INDEX(oiPassed, 9) //Green
			SET_ENTITY_INVINCIBLE(oiPassed, FALSE)
			SET_ENTITY_PROOFS(oiPassed, FALSE, FALSE, FALSE, FALSE, FALSE)
		ENDIF
	ENDIF
	
	// Disable Speed Boost FX.
	IF IS_PROP_CAPABLE_OF_SLOW_DOWN(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].mn)
		/*IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitSet2,  ciFMMC_PROP2_SLOW_DOWN_ENABLE)
			SET_SLOW_DOWN_EFFECT_DISABLED(FALSE)
		ELSE
			SET_SLOW_DOWN_EFFECT_DISABLED(TRUE)
		ENDIF*/
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitSet2,  ciFMMC_PROP2_SLOW_DOWN_ENABLE_EFFECT)
			SET_SLOW_DOWN_EFFECT_DISABLED(FALSE)
		ELSE
			SET_SLOW_DOWN_EFFECT_DISABLED(TRUE)
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iRespawnOnRuleChangeBS > 0
		SET_ALLOW_MIGRATE_TO_SPECTATOR(oiPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitSet,  ciFMMC_PROP_SetInvisible)
		SET_ENTITY_VISIBLE(oiPassed, FALSE)
		PRINTLN("SET_UP_FMMC_PROP_RC - Prop ",ipropnumber," should be invisible.")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitSet2,  ciFMMC_PROP2_NoCollision)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiPassed, FALSE)
		PRINTLN("SET_UP_FMMC_PROP_RC - Completely disabling collision for prop ", ipropnumber, "!")
	ENDIF
	
	IF IS_PROP_A_CCTV_WALL_ATTACHMENT(iPropNumber)
		SET_ENTITY_COLLISION(oiPassed, FALSE)
		PRINTLN("SET_UP_FMMC_PROP_RC - Disabling standard collision for camera attachment prop ", ipropnumber, "!")
	ENDIF
	
	IF IS_PROP_A_BOMBABLE_BLOCK(iPropNumber)
		PRINTLN("SET_UP_FMMC_PROP - Setting bombable block prop proofs")
		SET_ENTITY_PROOFS(oiPassed, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE)
	ENDIF
	
	IF GET_ENTITY_MODEL(oiPassed) = XM_PROP_X17_BARGE_01
		PRINTLN("SET_UP_FMMC_PROP - Creating extra collision for barge")
		VECTOR vCoords = GET_ENTITY_COORDS(oiPassed)
		
		g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[0] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_01")), vCoords, FALSE, FALSE, TRUE)
		ATTACH_ENTITY_TO_ENTITY(g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[0], oiPassed, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE, TRUE, FALSE, DEFAULT)
		
		g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[1] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Barge_Col_02")), vCoords, FALSE, FALSE, TRUE)
		ATTACH_ENTITY_TO_ENTITY(g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[1], oiPassed, 0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE, TRUE, FALSE, DEFAULT)
	ENDIF
	
	IF IS_THIS_MODEL_A_TRAP_PRESSURE_PAD(GET_ENTITY_MODEL(oiPassed))
		SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
		FREEZE_ENTITY_POSITION(oiPassed, FALSE)
		
		SET_OBJECT_IS_A_PRESSURE_PLATE(oiPassed, TRUE)
		PRINTLN("[ARENATRAPS]", "[PRESSUREPAD ", iPropNumber, "]", " Initialising! 2")
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
		PRINTLN("SET_UP_FMMC_PROP - SET_ENTITY_LOD_DIST 750 - Forced on Arena.")
		SET_ENTITY_LOD_DIST(oiPassed, 750)
	ENDIF
	
	IF IS_MODEL_AN_ARENA_TURRET_TOWER(GET_ENTITY_MODEL(oiPassed))
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].iPropBitSet2, ciFMMC_PROP2_UseAsTurret)
		ARENA_CONTESTANT_TURRET_STACK_PUSH(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropNumber].fTurretHeading)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Controller Vehicle Entity Helpers
// ##### Description: Helpers for dealing with Props in the controllers

///// PURPOSE:
/////    Tests if a point is in a flatbed's loading area.
///// PARAMS:
/////    paramVector - The vector being tested (usually the corona position in the creator
/////    sVehStruct - The vehicles struct for the creator
///// RETURNS:
/////     The index of vehicle in the vehstruct array or -1 if false.
///   I hate this and everything 
FUNC INT GET_VEHICLE_INDEX_AT_POINT(VECTOR paramVector, VEHICLE_CREATION_STRUCT &sVehStruct)

	VECTOR vFlatBedForwardOffset = <<0.0, 0.8, 2.2>>
	VECTOR vFlatBedBackwardOffset = <<0.0, -4.8, 0.1>>
	FLOAT fFlatBedOffsetWidth = 2.5
	
	VECTOR vTitanForwardOffset = <<0, 10, 10>>
	VECTOR vTitanBackwardOffset = <<0, -15, -10>>
	FLOAT fTitanOffsetWidth = 2.5
	
	VECTOR vSpeedoForwardOffset = <<0, -0.45, 1.75>>
	VECTOR vSpeedoBackwardOffset = <<0, -2.75, -0.5>>
	FLOAT fSpeedoOffsetWidth = 2.0
	
	INT iTemp
	REPEAT COUNT_OF(sVehStruct.veVehcile) iTemp
		IF IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].mn)
			PRINTLN("GET_VEHICLE_INDEX_AT_POINT checking vehicle ", iTemp, " model = ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].mn))
		ENDIF
		
		IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[iTemp])
		AND IS_VEHICLE_DRIVEABLE(sVehStruct.veVehcile[iTemp])
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].mn = FLATBED
				IF IS_POINT_IN_ANGLED_AREA(paramVector, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vFlatBedForwardOffset), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vFlatBedBackwardOffset), fFlatBedOffsetWidth)
					RETURN iTemp
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].mn = TITAN
				IF IS_POINT_IN_ANGLED_AREA(paramVector, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vTitanForwardOffset), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vTitanBackwardOffset), fTitanOffsetWidth)
					RETURN iTemp
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].mn = TR2
				IF IS_POINT_IN_ANGLED_AREA(paramVector, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vFlatBedForwardOffset), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vFlatBedBackwardOffset), fFlatBedOffsetWidth)
					RETURN iTemp
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].mn = WASTELANDER
				IF IS_POINT_IN_ANGLED_AREA(paramVector, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vFlatBedForwardOffset), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vFlatBedBackwardOffset), fFlatBedOffsetWidth)
					RETURN iTemp
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].mn = CARGOBOB2
				IF IS_POINT_IN_ANGLED_AREA(paramVector, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vFlatBedForwardOffset), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vFlatBedBackwardOffset), fFlatBedOffsetWidth)
					RETURN iTemp
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].mn = SPEEDO
				PRINTLN("GET_VEHICLE_INDEX_AT_POINT -  g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[", iTemp, "].mn = SPEEDO")
				IF IS_POINT_IN_ANGLED_AREA(paramVector, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vSpeedoForwardOffset), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vSpeedoBackwardOffset), fSpeedoOffsetWidth)
					RETURN iTemp
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].mn = TRFLAT
				IF IS_POINT_IN_ANGLED_AREA(paramVector, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vFlatBedForwardOffset), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vFlatBedBackwardOffset), fFlatBedOffsetWidth)
					RETURN iTemp
				ENDIF
			ELSE
				VECTOR vMin = <<0,0,0>>
				VECTOR vMax = <<0,0,0>>
				GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].mn, vMin, vMax)
				IF IS_POINT_IN_ANGLED_AREA(paramVector, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vMin), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.veVehcile[iTemp], vMax), GET_MODEL_WIDTH(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].mn), TRUE)
					RETURN iTemp
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN -1
	
ENDFUNC

FUNC INT GET_OBJECT_INDEX_AT_POINT(VECTOR paramVector, OBJECT_CREATION_STRUCT &sObjStruct)

	VECTOR vForwardOffset 	= <<0.0, 	0.8, 	6.0>>
	VECTOR vBackwardOffset 	= <<0.0, 	-4.8, 	0.1>>
	FLOAT fOffsetWidth = 2.5
	
	INT iTemp
	REPEAT COUNT_OF(sObjStruct.oiObject) iTemp
		IF DOES_ENTITY_EXIST(sObjStruct.oiObject[iTemp])
		AND VDIST2(paramVector, GET_ENTITY_COORDS(sObjStruct.oiObject[iTemp])) < 100
			IF IS_POINT_IN_ANGLED_AREA(paramVector, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sObjStruct.oiObject[iTemp], vForwardOffset), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sObjStruct.oiObject[iTemp], vBackwardOffset), fOffsetWidth)
				RETURN iTemp
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN -1
	
ENDFUNC

PROC ATTACH_OBJECT_TO_FLATBED(OBJECT_INDEX objToAttach, VEHICLE_INDEX vehFlatBed, VECTOR vOffset, VECTOR vOffsetRotation, BOOL battach = FALSE, BOOL bOffsetLocal = TRUE)
	IF IS_VEHICLE_DRIVEABLE(vehFlatBed)
		
		VECTOR vInitialOffset
		INT iBone = 0
		
		MODEL_NAMES mnVehModel = GET_ENTITY_MODEL(vehFlatBed)
		
		SWITCH mnVehModel
			CASE FLATBED
				//Temp hard-coded offset hack that works just for placing the container on the flatbed
				vInitialOffset.y = -2.8
				vInitialOffset.z = 0.41
			BREAK
			
			CASE TITAN
			
				//INT iBone = 56//gear_rm bone for Titan
				VECTOR vMin, vMax
				GET_MODEL_DIMENSIONS(mnVehModel, vMin, vMax)
				vInitialOffset.z = -0.213 - vMin.z
				iBone = 56
				bOffsetLocal = FALSE
				
				IF battach
					ATTACH_ENTITY_TO_ENTITY(objToAttach,vehFlatBed,56,vInitialOffset,<<0,0,0>>,FALSE,FALSE,TRUE)
				ENDIF
				
			BREAK
			
			CASE TR2
				vInitialOffset.y = -2.8
				vInitialOffset.z = 0.41
			BREAK
			
			CASE WASTELANDER
				vInitialOffset.y = -1.75
			BREAK
			
			CASE SPEEDO
				vInitialOffset.y = -2.0
				vInitialOffset.z = -0.15
			BREAK
			
			CASE TRFLAT
				vInitialOffset.y = -2.8
				vInitialOffset.z = 0.41
			BREAK
			
		ENDSWITCH
		
		IF !bOffsetLocal
			PRINTLN("ATTACH_OBJECT_TO_FLATBED - !bOffsetLocal - vOffset: <<", vOffset.x, ",", vOffset.y, ",", vOffset.z, ">>")
			SET_ENTITY_COORDS_NO_OFFSET(objToAttach, (GET_WORLD_POSITION_OF_ENTITY_BONE(vehFlatBed,iBone) + vOffset))
			SET_ENTITY_HEADING(objToAttach, GET_ENTITY_HEADING(vehFlatBed))
		ELSE
			PRINTLN("ATTACH_OBJECT_TO_FLATBED - vOffset: <<", vOffset.x, ",", vOffset.y, ",", vOffset.z, ">> - vOffsetRotation: <<", vOffsetRotation.x, ",", vOffsetRotation.y, ",", vOffsetRotation.z, ">>")
			VECTOR vMin, vMax
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(objToAttach), vMin, vMax)
			vOffset += vInitialOffset - <<0,0,vMin.Z>>
			SET_ENTITY_COORDS_NO_OFFSET(objToAttach, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehFlatBed , vOffset))
			VECTOR vRot = GET_ENTITY_ROTATION(vehFlatBed)
			vRot.x = vRot.x + vOffsetRotation.x
			IF vRot.x > 360.0
				vRot.x = 360.0 - vRot.x
			ENDIF
			vRot.y = vRot.y + vOffsetRotation.y
			IF vRot.y > 360.0
				vRot.y = 360.0 - vRot.y
			ENDIF
			vRot.z = vRot.z + vOffsetRotation.z
			IF vRot.z > 360.0
				vRot.z = 360.0 - vRot.z
			ENDIF
			
			SET_ENTITY_ROTATION(objToAttach, vRot)
		ENDIF
		
		IF battach
			PRINTLN("ATTACH_OBJECT_TO_FLATBED - bAttach - Entity: ", NATIVE_TO_INT(objToAttach), " - Entity To Attach To: ", NATIVE_TO_INT(vehFlatBed))
			DEBUG_PRINTCALLSTACK()
			ATTACH_ENTITY_TO_ENTITY(objToAttach,vehFlatBed,iBone,vOffset,vOffsetRotation,FALSE,FALSE,TRUE)
		ENDIF
		
	ENDIF
ENDPROC

FUNC BOOL CAN_VEHICLE_BE_ATTACHED_TO(VEHICLE_CREATION_STRUCT &sVehStruct, INT iVehIndex)
	INT iTemp
	IF IS_VEHICLE_DRIVEABLE(sVehStruct.veVehcile[iVehIndex])
		FOR iTemp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].iAttachParent = iVehIndex //sVehStruct.iAttachParent
			AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].iAttachParentType = CREATION_TYPE_VEHICLES
				RETURN FALSE
			ENDIF
		ENDFOR
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_A_CAR(VEHICLE_INDEX vehToCheck)

	MODEL_NAMES mnVehModel = GET_ENTITY_MODEL(vehToCheck)
	IF (IS_VEHICLE_MODEL_IN_CREATOR_LIBRARY(mnVehModel,VEH_LIBRARY_COMPACTS)
	OR 	IS_VEHICLE_MODEL_IN_CREATOR_LIBRARY(mnVehModel,VEH_LIBRARY_COUPES)
	OR 	IS_VEHICLE_MODEL_IN_CREATOR_LIBRARY(mnVehModel, VEH_LIBRARY_SEDANS)
	OR 	IS_VEHICLE_MODEL_IN_CREATOR_LIBRARY(mnVehModel, VEH_LIBRARY_MUSCLE)
	OR 	IS_VEHICLE_MODEL_IN_CREATOR_LIBRARY(mnVehModel, VEH_LIBRARY_SPORTS)
	OR 	IS_VEHICLE_MODEL_IN_CREATOR_LIBRARY(mnVehModel, VEH_LIBRARY_SPORTSCLASSIC)
	OR 	IS_VEHICLE_MODEL_IN_CREATOR_LIBRARY(mnVehModel, VEH_LIBRARY_SUPER)
	OR 	IS_VEHICLE_MODEL_IN_CREATOR_LIBRARY(mnVehModel, VEH_LIBRARY_SUVS)
	OR 	IS_VEHICLE_MODEL_IN_CREATOR_LIBRARY(mnVehModel, VEH_LIBRARY_OFFROAD)
	OR 	IS_VEHICLE_MODEL_IN_CREATOR_LIBRARY(mnVehModel, VEH_LIBRARY_POLICE))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_CARGOBOB_ATTACH_TO_VEHICLE(VEHICLE_CREATION_STRUCT &sVehStruct, INT iVehIndex)
	INT i
	IF IS_VEHICLE_DRIVEABLE(sVehStruct.veVehcile[iVehIndex])
		IF IS_VEHICLE_A_CAR(sVehStruct.veVehcile[iVehIndex])
		AND GET_ENTITY_MODEL(svehStruct.viCoronaVeh) = CARGOBOB2
			FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent = iVehIndex //sVehStruct.iAttachParent
				AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType = CREATION_TYPE_VEHICLES
					RETURN FALSE
				ENDIF
			ENDFOR
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_OBJECT_BE_ATTACHED_TO(VEHICLE_CREATION_STRUCT &sVehStruct, OBJECT_CREATION_STRUCT &sObjStruct, INT iObjIndex)
	INT iTemp
	IF DOES_ENTITY_EXIST(sObjStruct.oiObject[iObjIndex])
	AND GET_ENTITY_MODEL(sObjStruct.oiObject[iObjIndex]) = PROP_CONTAINER_LD
		FOR iTemp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].iAttachParent = iObjIndex //sVehStruct.iAttachParent
			AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].iAttachParentType = CREATION_TYPE_OBJECTS
			AND DOES_ENTITY_EXIST(sVehStruct.veVehcile[iTemp])
				RETURN FALSE
			ENDIF
		ENDFOR
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_VEHICLE_ATTACH_CHILD(VEHICLE_CREATION_STRUCT &sVehStruct, INT iVehicleNumber)
	INT iTemp
	FOR iTemp = 0 TO FMMC_MAX_VEHICLES - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTemp].iAttachParent = iVehicleNumber
		AND DOES_ENTITY_EXIST(sVehStruct.veVehcile[iTemp])
			RETURN iTemp
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

FUNC INT GET_OBJECT_ATTACH_CHILD(OBJECT_CREATION_STRUCT &sObjStruct, INT iObjNumber)
	INT iTemp
	FOR iTemp = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iTemp].iAttachParent = iObjNumber
		AND DOES_ENTITY_EXIST(sObjStruct.oiObject[iTemp])
			RETURN iTemp
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

FUNC INT GET_VEHICLE_ARRAY_INDEX(VEHICLE_CREATION_STRUCT &sVehStruct, VEHICLE_INDEX veh)
	INT iTemp
	IF DOES_ENTITY_EXIST(veh)
		FOR iTemp  = 0 TO FMMC_MAX_VEHICLES - 1
			IF sVehStruct.veVehcile[iTemp] = veh
				RETURN iTemp
			ENDIF
		ENDFOR
	ENDIF
	RETURN -1
ENDFUNC

PROC FMMC_ATTACH_CARGOBOB_TO_VEHICLE(VEHICLE_INDEX vehCargobob, VEHICLE_INDEX vehToAttachTo, BOOL bAttach = FALSE)
	IF IS_VEHICLE_DRIVEABLE(vehCargobob)
	AND IS_VEHICLE_DRIVEABLE(vehToAttachTo)
	AND IS_VEHICLE_A_CAR(vehToAttachTo)
		VECTOR vMin, vMax, vOffset
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehToAttachTo),vMin,vMax)
		vOffset.z = vOffset.z + 6
		
		SET_ENTITY_COORDS_NO_OFFSET(vehCargobob, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehToAttachTo, vOffset))
		IF bAttach
			ATTACH_ENTITY_TO_ENTITY(vehCargobob, vehToAttachTo,0,vOffset,<<0,0,0>>,FALSE,FALSE,TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC FMMC_ATTACH_VEHICLE_TO_VEHICLE(VEHICLE_INDEX vehToAttach, VEHICLE_INDEX vehFlatBed, BOOL battach = FALSE, BOOL bDisableDummies = FALSE)
	IF IS_VEHICLE_DRIVEABLE(vehFlatBed)
	AND IS_VEHICLE_DRIVEABLE(vehToAttach)
	AND (IS_VEHICLE_MODEL(vehFlatBed, FLATBED) OR IS_VEHICLE_MODEL(vehFlatBed, TR2) OR IS_VEHICLE_MODEL(vehFlatBed, INT_TO_ENUM(MODEL_NAMES, HASH("WASTELANDER"))))
		VECTOR vOffset, vMin, vMax, vDim
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehToAttach), vMin, vMax)
		vDim = vMax - vMin
		IF IS_VEHICLE_MODEL(vehFlatBed, INT_TO_ENUM(MODEL_NAMES, HASH("WASTELANDER")))
			vOffset.y = -1.75
		ELSE
			vOffset.y = -2.75 
		ENDIF
		IF vDim.y > 4
			vOffset.y -= 2-(vDim.y/2) //move the attached vehicle back by however much longer it is than 4m
		ENDIF
		IF IS_VEHICLE_MODEL(vehFlatBed, TR2)
			vOffset.z = ((vDim.z) - 0.45)
		ELIF IS_VEHICLE_MODEL(vehFlatBed, INT_TO_ENUM(MODEL_NAMES, HASH("WASTELANDER")))
			IF IS_VEHICLE_MODEL(vehToAttach, INT_TO_ENUM(MODEL_NAMES, HASH("RUINER3")))	
				vOffset.z = 1.4
			ELSE
				vOffset.z = 1.63
			ENDIF
		ELSE
			vOffset.z = ((vDim.z) - 0.58)// move the attached vehicle up or down by the difference of it's height to 0.58m
		ENDIF
		
		IF bDisableDummies
			SET_DISABLE_SUPERDUMMY(vehToAttach, TRUE)
			SET_DISABLE_SUPERDUMMY(vehFlatBed, TRUE)
			PRINTLN("FMMC_ATTACH_VEHICLE_TO_VEHICLE | Disabling dummy for attachment vehicles")
		ENDIF
		
		SET_ENTITY_COORDS_NO_OFFSET(vehToAttach, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehFlatBed , vOffset))
		SET_ENTITY_HEADING(vehToAttach, GET_ENTITY_HEADING(vehFlatBed))
		
		IF battach
			ATTACH_ENTITY_TO_ENTITY(vehToAttach,vehFlatBed,0,vOffset,<<0,0,0>>,FALSE,FALSE,TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC FMMC_ATTACH_VEHICLE_TO_OBJECT(VEHICLE_INDEX vehToAttach, OBJECT_INDEX objAttachParent, BOOL battach = FALSE)	
	
	IF DOES_ENTITY_EXIST(objAttachParent)
	AND IS_VEHICLE_DRIVEABLE(vehToAttach)
	AND GET_ENTITY_MODEL(objAttachParent) = PROP_CONTAINER_LD
		VECTOR vOffset, vMin, vMax, vDim
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehToAttach), vMin, vMax)
		vDim = vMax - vMin
		vOffset.z = -1 + vDim.z
		SET_ENTITY_COORDS_NO_OFFSET(vehToAttach, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objAttachParent , vOffset))
		SET_ENTITY_HEADING(vehToAttach, GET_ENTITY_HEADING(objAttachParent)+180)
		
		SET_VEHICLE_DOORS_LOCKED(vehToAttach, VEHICLELOCK_LOCKED)
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehToAttach, TRUE)
		SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehToAttach,TRUE)
		SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehToAttach,TRUE)
		
		#IF IS_DEBUG_BUILD
			MODEL_NAMES mCar = GET_ENTITY_MODEL(vehToAttach)
			MODEL_NAMES mObj = GET_ENTITY_MODEL(objAttachParent)
			
			PRINTLN("[RCC MISSION] [dsw] [FMMC_ATTACH_VEHICLE_TO_OBJECT] Attaching vehicle with model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mCar), " to object with model ", ENUM_TO_INT(mObj))
		#ENDIF
		IF battach
			ATTACH_ENTITY_TO_ENTITY(vehToAttach, objAttachParent, 0, vOffset, <<0,0,180>>, FALSE, FALSE, TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_ATTACH_CURRENT_VEHICLE_TO_VEHICLE(structFMMC_MENU_ITEMS &sFMMCmenu, VEHICLE_CREATION_STRUCT &sVehStruct, CREATION_VARS_STRUCT &sCurrentVarsStruct)
	sVehStruct.iAttachParent = -1
	IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
	AND IS_VEHICLE_DRIVEABLE(sVehStruct.viCoronaVeh)
	AND g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	AND g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_CTF
	AND (	sFMMCmenu.iVehicleLibrary = VEH_LIBRARY_COMPACTS
		OR 	sFMMCmenu.iVehicleLibrary = VEH_LIBRARY_COUPES
		OR 	sFMMCmenu.iVehicleLibrary = VEH_LIBRARY_SEDANS
		OR 	sFMMCmenu.iVehicleLibrary = VEH_LIBRARY_MUSCLE
		OR 	sFMMCmenu.iVehicleLibrary = VEH_LIBRARY_SPORTS
		OR 	sFMMCmenu.iVehicleLibrary = VEH_LIBRARY_SPORTSCLASSIC
		OR 	sFMMCmenu.iVehicleLibrary = VEH_LIBRARY_SUPER
		OR 	sFMMCmenu.iVehicleLibrary = VEH_LIBRARY_SUVS
		OR 	sFMMCmenu.iVehicleLibrary = VEH_LIBRARY_OFFROAD
		OR 	sFMMCmenu.iVehicleLibrary = VEH_LIBRARY_MOTORCYCLES
		OR 	sFMMCmenu.iVehicleLibrary = VEH_LIBRARY_POLICE)
		INT iAttach = GET_VEHICLE_INDEX_AT_POINT(sCurrentVarsStruct.vCoronaPos, sVehStruct)
		IF iAttach >= 0
			IF CAN_VEHICLE_BE_ATTACHED_TO(sVehStruct, iAttach)
			AND (IS_VEHICLE_MODEL(sVehStruct.veVehcile[iAttach],FLATBED)
			OR IS_VEHICLE_MODEL(sVehStruct.veVehcile[iAttach],TR2)
			OR IS_VEHICLE_MODEL(sVehStruct.veVehcile[iAttach],INT_TO_ENUM(MODEL_NAMES, HASH("WASTELANDER"))))
				sVehStruct.iAttachParent = iAttach
				sVehStruct.iAttachParentType = CREATION_TYPE_VEHICLES
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_ATTACH_CARGOBOB_TO_VEHICLE(VEHICLE_CREATION_STRUCT &sVehStruct, CREATION_VARS_STRUCT &sCurrentVarsStruct)
	IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
	AND IS_VEHICLE_DRIVEABLE(sVehStruct.viCoronaVeh)
	AND GET_ENTITY_MODEL(sVehStruct.viCoronaVeh) = CARGOBOB2
	AND g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	AND g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_CTF
		INT iAttach = GET_VEHICLE_INDEX_AT_POINT(sCurrentVarsStruct.vCoronaPos, sVehStruct)
		IF iAttach >= 0
			IF CAN_CARGOBOB_ATTACH_TO_VEHICLE(sVehStruct, iAttach)
				//Setting the attach parent to the car already placed, this might have to be the other way around...
				sVehStruct.iAttachParent = iAttach
				sVehStruct.iAttachparentType = CREATION_TYPE_VEHICLES
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_ATTACH_CURRENT_VEHICLE_TO_OBJECT(VEHICLE_CREATION_STRUCT &sVehStruct, CREATION_VARS_STRUCT &sCurrentVarsStruct, OBJECT_CREATION_STRUCT &sObjStruct)
	sVehStruct.iAttachParent = -1
	IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
	AND IS_VEHICLE_DRIVEABLE(sVehStruct.viCoronaVeh)
	AND g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	AND g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_CTF
		INT iAttach = GET_OBJECT_INDEX_AT_POINT(sCurrentVarsStruct.vCoronaPos, sObjStruct)
		IF iAttach >= 0
			IF CAN_OBJECT_BE_ATTACHED_TO(sVehStruct, sObjStruct, iAttach)
				sVehStruct.iAttachParentType = CREATION_TYPE_OBJECTS
				sVehStruct.iAttachParent = iAttach
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_VEHICLE_HAVE_DOOR(VEHICLE_INDEX viPassed, SC_DOOR_LIST doorPassed)
	
	IF doorPassed = SC_DOOR_FRONT_LEFT
		IF GET_ENTITY_BONE_INDEX_BY_NAME(viPassed, "door_dside_f") <> -1		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF doorPassed = SC_DOOR_FRONT_RIGHT
		IF GET_ENTITY_BONE_INDEX_BY_NAME(viPassed, "door_pside_f") <> -1		
			RETURN TRUE
		ENDIF
	ENDIF

	IF doorPassed = SC_DOOR_REAR_LEFT
		IF GET_ENTITY_BONE_INDEX_BY_NAME(viPassed, "door_dside_r") <> -1		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF doorPassed = SC_DOOR_REAR_RIGHT
		IF GET_ENTITY_BONE_INDEX_BY_NAME(viPassed, "door_pside_r") <> -1		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF doorPassed = SC_DOOR_BONNET
		IF GET_ENTITY_BONE_INDEX_BY_NAME(viPassed, "bonnet") <> -1		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF doorPassed = SC_DOOR_BOOT
		IF GET_ENTITY_BONE_INDEX_BY_NAME(viPassed, "boot") <> -1		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_LOCKED_FOR_TEAM(INT iVeh, INT iteam)

	IF IS_VEHICLE_A_HILL_ENTITY(iVeh)
		RETURN FALSE
	ENDIF

	SWITCH iteam
		CASE 0
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo,ciFMMC_VEHICLE2_RESTRICTED_TEAM1)
				PRINTLN("SHOULD_VEHICLE_BE_LOCKED_FOR_TEAM - Locking vehicle ", iVeh, " because iTeam is ", iTeam)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo,ciFMMC_VEHICLE2_RESTRICTED_TEAM2)
				PRINTLN("SHOULD_VEHICLE_BE_LOCKED_FOR_TEAM - Locking vehicle ", iVeh, " because iTeam is ", iTeam)
				RETURN TRUE
			ENDIF		
		BREAK
		
		CASE 2
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo,ciFMMC_VEHICLE2_RESTRICTED_TEAM3)
				PRINTLN("SHOULD_VEHICLE_BE_LOCKED_FOR_TEAM - Locking vehicle ", iVeh, " because iTeam is ", iTeam)
				RETURN TRUE
			ENDIF		
		BREAK
		
		CASE 3
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo,ciFMMC_VEHICLE2_RESTRICTED_TEAM4)
				PRINTLN("SHOULD_VEHICLE_BE_LOCKED_FOR_TEAM - Locking vehicle ", iVeh, " because iTeam is ", iTeam)
				RETURN TRUE
			ENDIF		
		BREAK
	
	ENDSWITCH
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_RESTRICT_FOR_ALL)
		PRINTLN("SHOULD_VEHICLE_BE_LOCKED_FOR_TEAM - Locking vehicle ", iVeh, " because of ciFMMC_VEHICLE8_RESTRICT_FOR_ALL")
		RETURN TRUE
	ENDIF
	
	PRINTLN("SHOULD_VEHICLE_BE_LOCKED_FOR_TEAM - NOT locking vehicle ", iVeh, " - it's not restricted for my team")
	RETURN FALSE
ENDFUNC

PROC HANDLE_VEH_TEAM_RESTRICTION(INT iVeh, VEHICLE_INDEX viPassed)
 	INT iTeam
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS-1)
		IF SHOULD_VEHICLE_BE_LOCKED_FOR_TEAM(iveh, iTeam)
			IF NETWORK_GET_ENTITY_IS_NETWORKED(viPassed)
				SET_VEHICLE_DOORS_LOCKED_FOR_TEAM(viPassed, iTeam, TRUE)
				SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viPassed, TRUE)
				SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(viPassed, TRUE)
				PRINTLN("[RCC MISSION] HANDLE_VEH_TEAM_RESTRICTION - locking the doors for vehicle: ",iveh, " for team: ", iTeam, " Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
			ELSE
				IF iTeam = g_FMMC_STRUCT.iTestMyTeam
					SET_VEHICLE_DOORS_LOCKED(viPassed, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					PRINTLN("[RCC MISSION] HANDLE_VEH_TEAM_RESTRICTION - locking with VEHICLELOCK_LOCKOUT_PLAYER_ONLY")
				ELSE
					//SET_VEHICLE_DOORS_LOCKED(viPassed, VEHICLELOCK_CANNOT_ENTER)
				ENDIF
				
				PRINTLN("[RCC MISSION] HANDLE_VEH_TEAM_RESTRICTION - Not networked! Player team is: ", g_FMMC_STRUCT.iTestMyTeam, " Locking ",iveh," for team: ", iTeam, " Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] HANDLE_VEH_TEAM_RESTRICTION - Vehicle is not restricted: ",iveh," for team: ", iTeam, " Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_RESTRICT_FOR_ALL)
		SET_VEHICLE_DOORS_LOCKED(viPassed, VEHICLELOCK_LOCKED)
		IF NETWORK_GET_ENTITY_IS_NETWORKED(viPassed)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viPassed, TRUE)
			SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(viPassed, TRUE)
		ENDIF
		PRINTLN("[RCC MISSION] HANDLE_VEH_TEAM_RESTRICTION - Vehicle restricted for all players: ",iveh," Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
	ENDIF
ENDPROC

PROC LOCK_VEHICLE_DOORS_FOR_CRATES(VEHICLE_INDEX viPassed, INT iveh)
	PRINTLN("[RCC MISSION] LOCK_VEHICLE_DOORS_FOR_CRATES called iveh = ", iveh, " Veh Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
	
	MODEL_NAMES mCrate = GET_VEHICLE_CRATE_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven,viPassed)
	IF mCrate != DUMMY_MODEL_FOR_SCRIPT
		IF mCrate != hei_prop_mini_sever_02 // 2055557
		AND mCrate != HEI_PROP_MINI_SEVER_03 // 2090722
		AND mCrate != HEI_PROP_MINI_SEVER_BROKEN
			PRINTLN("[RCC MISSION] [dsw] vehicle has crates, will attempt to lock rear doors. iVeh =  ",iveh, " Crate model int = ", ENUM_TO_INT(mCrate)) 
			SWITCH GET_VEHICLE_CRATE_DOORS_COUNT(viPassed)
				CASE 4
					SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viPassed, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
					SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viPassed, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
					NET_PRINT("LOCKING VEHICLE SC_DOOR_REAR_LEFT & SC_DOOR_REAR_RIGHT FOR VEHICLE CRATES") NET_NL()
				BREAK
				CASE 5
					SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viPassed, ENUM_TO_INT(SC_DOOR_BOOT), VEHICLELOCK_LOCKED)
					NET_PRINT("LOCKING VEHICLE SC_DOOR_BOOT FOR VEHICLE CRATES") NET_NL()
				BREAK
				
				DEFAULT
					NET_PRINT("Failed to fin entr in GET_VEHICLE_CRATE_DOORS_COUNT") NET_NL()
				BREAK
			ENDSWITCH
		ELSE
			PRINTLN("[RCC MISSION] [dsw] Crate found but model is hei_prop_mini_sever_02. Won't lock. iVeh =  ", iveh)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_VEHICLE_CREATION_FINISHED(NETWORK_INDEX niVehicle,NETWORK_INDEX niVehicleCrate,INT i)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
	AND NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)	
		RETURN FALSE
	ENDIF
	MODEL_NAMES crateModel = GET_VEHICLE_CRATE_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetTwo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, NET_TO_VEH(niVehicle))
	IF crateModel != DUMMY_MODEL_FOR_SCRIPT
	AND NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicleCrate)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


PROC FMMC_SET_THIS_VEHICLE_EXTRAS(VEHICLE_INDEX veh, INT iDoorBit, INT iLiveryBitset = -1)	
	IF IS_BIT_SET(iDoorBit, ciFMMC_VEHICLE_EXTRA1)
		IF GET_ENTITY_MODEL(veh) = MULE
			SET_VEHICLE_EXTRA(veh, 1, FALSE)
			SET_VEHICLE_EXTRA(veh, 2, TRUE)
			SET_VEHICLE_EXTRA(veh, 3, TRUE)
			SET_VEHICLE_EXTRA(veh, 4, TRUE)
			SET_VEHICLE_EXTRA(veh, 5, TRUE)
			SET_VEHICLE_EXTRA(veh, 6, TRUE)
			SET_VEHICLE_EXTRA(veh, 7, TRUE)
		ELIF GET_ENTITY_MODEL(veh) = BENSON
			SET_VEHICLE_EXTRA(veh, 1, TRUE)
			SET_VEHICLE_EXTRA(veh, 2, TRUE)
			SET_VEHICLE_EXTRA(veh, 3, TRUE)
			SET_VEHICLE_EXTRA(veh, 4, TRUE)
			SET_VEHICLE_EXTRA(veh, 5, TRUE)
			SET_VEHICLE_EXTRA(veh, 6, TRUE)
			SET_VEHICLE_EXTRA(veh, 7, FALSE)
		ELIF GET_ENTITY_MODEL(veh) = DUNE
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMAKE_VEH_SUITABLE_FOR_SUMO)
				SET_VEHICLE_EXTRA(veh, 1, FALSE)
				SET_VEHICLE_EXTRA(veh, 2, TRUE)
				SET_VEHICLE_EXTRA(veh, 3, FALSE)
				SET_VEHICLE_EXTRA(veh, 4, FALSE)
				SET_VEHICLE_EXTRA(veh, 5, FALSE)
				SET_VEHICLE_EXTRA(veh, 6, FALSE)
				SET_VEHICLE_EXTRA(veh, 7, FALSE)
				PRINTLN("SETTING THE VEHICLE EXTRAS FOR DUNE")
			ENDIF
		ENDIF
		PRINTLN("SETTING THE VEHICLE EXTRAS")
	ELSE
		IF GET_ENTITY_MODEL(veh) = DUNE
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMAKE_VEH_SUITABLE_FOR_SUMO)
				SET_VEHICLE_EXTRA(veh, 1, FALSE)
				SET_VEHICLE_EXTRA(veh, 2, TRUE)
				SET_VEHICLE_EXTRA(veh, 3, FALSE)
				SET_VEHICLE_EXTRA(veh, 4, FALSE)
				SET_VEHICLE_EXTRA(veh, 5, FALSE)
				SET_VEHICLE_EXTRA(veh, 6, FALSE)
				SET_VEHICLE_EXTRA(veh, 7, FALSE)
				PRINTLN("SETTING THE VEHICLE EXTRAS FOR DUNE")
			ELSE
				PRINTLN("[VEHICLE CREATION] - Forcing dune buggy to have a hood")
				// url:bugstar:5101905 - Capture Creator - The hood of the Dune Buggy is not present and unable to change the vehicle colour
				SET_VEHICLE_EXTRA(veh, 2, FALSE)
				SET_VEHICLE_EXTRA(veh, 4, FALSE)
			ENDIF
		ELSE 
			IF GET_ENTITY_MODEL(veh) = MULE
			AND iLiveryBitset != -1
				INT iBitSet = FLOOR(POW(TO_FLOAT(2), to_FLOAT(iLiveryBitset+2)))
				SET_VEHICLE_EXTRA(veh, 1, TRUE)
				SET_VEHICLE_EXTRA(veh, 2, NOT IS_BIT_SET(iBitSet, 2))  
				SET_VEHICLE_EXTRA(veh, 3, NOT IS_BIT_SET(iBitSet, 3))  
				SET_VEHICLE_EXTRA(veh, 4, NOT IS_BIT_SET(iBitSet, 4))  
				SET_VEHICLE_EXTRA(veh, 5, NOT IS_BIT_SET(iBitSet, 5))  
				SET_VEHICLE_EXTRA(veh, 6, NOT IS_BIT_SET(iBitSet, 6))  
				SET_VEHICLE_EXTRA(veh, 7, NOT IS_BIT_SET(iBitSet, 7))  
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_VEHICLE_WEAPON_MODS(VEHICLE_INDEX veh, INT iVModTurret, INT iVModArmour, INT iVehBitset, INT iVeh, INT iVModFrontBumper, INT iVModExhaust, INT iVModBombbay, INT iVModSpoiler, INT iTeam = -1, BOOL bForceUseBombBayMod = FALSE, INT iNeonColour = -1, INT iWaterBombAmmo = 0)
	
	PRINTLN("SET_VEHICLE_WEAPON_MODS - iVModTurret ",iVModTurret,", iVModArmour ",iVModArmour,", iVehBitset ",iVehBitset,", iVModFrontBumper: ", iVModFrontBumper)
	DEBUG_PRINTCALLSTACK()
	
	IF GET_NUM_MOD_KITS(veh) > 0
		SET_VEHICLE_MOD_KIT(veh, 0)
		PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting Mod Kit!")
	ENDIF

	IF GET_NUM_VEHICLE_MODS(veh, MOD_ROOF) > 0
		PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting MOD_ROOF to mod ", iVModTurret)
		PRELOAD_VEHICLE_MOD(veh, MOD_ROOF, iVModTurret)
		SET_VEHICLE_MOD(veh, MOD_ROOF, iVModTurret)
	ENDIF
	
	IF GET_NUM_VEHICLE_MODS(veh, MOD_CHASSIS) > 0
		PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting MOD_CHASSIS to mod ", iVModArmour)
		PRELOAD_VEHICLE_MOD(veh, MOD_CHASSIS, iVModArmour)
		SET_VEHICLE_MOD(veh, MOD_CHASSIS, iVModArmour)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
		IF iVeh > -1
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_ALLOW_MOC_ENTRY)
			IF GET_ENTITY_MODEL(veh) = TRAILERLARGE
				IF GET_NUM_VEHICLE_MODS(veh, MOD_ROOF) > 0
					PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting Mobile Operations Center roof mod to ", 1)
					PRELOAD_VEHICLE_MOD(veh, MOD_ROOF, 1)
					SET_VEHICLE_MOD(veh, MOD_ROOF, 1)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_ENTITY_MODEL(veh) = OPPRESSOR2
		SET_AIRCRAFT_COUNTERMEASURE_TYPE(veh, iVModFrontBumper, FALSE)
	ELSE
		IF GET_NUM_VEHICLE_MODS(veh, MOD_BUMPER_F) > 0
			PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting MOD_BUMPER_F to mod ", iVModFrontBumper)
			PRELOAD_VEHICLE_MOD(veh, MOD_BUMPER_F, iVModFrontBumper)
			SET_VEHICLE_MOD(veh, MOD_BUMPER_F, iVModFrontBumper)
		ENDIF
	ENDIF
	
	IF GET_NUM_VEHICLE_MODS(veh, MOD_WING_R) > 0 	
		IF (iTeam > -1 AND iTeam < FMMC_MAX_TEAMS AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2,  ciBS2_SPAWN_VEHICLE_APPLY_BOMB_MOD))
		OR (iVeh > -1 AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_BOMB_BAY_MOD))
		OR (iVModBombBay > -1 AND IS_THIS_A_DEATHMATCH())//url:bugstar:4530561
		OR bForceUseBombBayMod
			PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting MOD_WING_R to mod ", iVModBombBay)
			PRELOAD_VEHICLE_MOD(veh, MOD_WING_R, iVModBombBay)
			//SET_VEHICLE_MOD(veh, MOD_WING_R, iVModBombBay)
			
			IF iVModBombBay > -1
				SET_VEHICLE_MOD(veh, MOD_WING_R, 0)
			ENDIF
			
			SWITCH iVModBombBay
				CASE 0
					SET_AIRCRAFT_BOMB_TYPE(veh, WEAPONTYPE_DLC_VEHICLE_BOMB)
				BREAK
				CASE 1
					SET_AIRCRAFT_BOMB_TYPE(veh, WEAPONTYPE_DLC_VEHICLE_BOMB_INCENDIARY)
				BREAK
				CASE 2
					SET_AIRCRAFT_BOMB_TYPE(veh, WEAPONTYPE_DLC_VEHICLE_BOMB_GAS)
				BREAK
				CASE 3
					SET_AIRCRAFT_BOMB_TYPE(veh, WEAPONTYPE_DLC_VEHICLE_BOMB_CLUSTER)
				BREAK
				CASE 4
					SET_AIRCRAFT_BOMB_TYPE(veh, WEAPONTYPE_DLC_VEHICLE_BOMB) //Water Bombs use the regular bomb and override the weapon type
					IF iWaterBombAmmo > 0
						PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting water bomb ammo to : ", iWaterBombAmmo)
						SET_VEHICLE_BOMB_AMMO(veh, iWaterBombAmmo)
					ENDIF
				BREAK
			ENDSWITCH
			
			PRINTLN("[TMS] Applying bomb type ", iVModBombBay)
		ELSE
			SET_VEHICLE_MOD(veh, MOD_WING_R, -1)
		ENDIF
	ENDIF
	
	IF GET_NUM_VEHICLE_MODS(veh, MOD_EXHAUST) > 0
		PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting MOD_EXHAUST to mod ", iVModExhaust)
		PRELOAD_VEHICLE_MOD(veh, MOD_EXHAUST, iVModExhaust)
		SET_VEHICLE_MOD(veh, MOD_EXHAUST, iVModExhaust)
	ENDIF	
	
	IF GET_NUM_VEHICLE_MODS(veh, MOD_BUMPER_R) > 0
		IF IS_BIT_SET(iVehBitset, ciFMMC_VEHICLE5_USE_REAR_WEAPON_MOD)
			PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting MOD_BUMPER_R")
			PRELOAD_VEHICLE_MOD(veh, MOD_BUMPER_R, 0)
			SET_VEHICLE_MOD(veh, MOD_BUMPER_R, 0)
		ELSE
			SET_VEHICLE_MOD(veh, MOD_BUMPER_R, -1)
		ENDIF
	ENDIF
	
	IF GET_NUM_VEHICLE_MODS(veh, MOD_SPOILER) > 0
		PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting Spoiler to mod ", iVModSpoiler)
		PRELOAD_VEHICLE_MOD(veh, MOD_SPOILER, iVModSpoiler)
		SET_VEHICLE_MOD(veh, MOD_SPOILER, iVModSpoiler)
	ENDIF
	
	//SPECIAL CASES (DUNE3, TAMPA3 etc) //
	IF GET_ENTITY_MODEL(veh) = DUNE3
		IF iVeh > -1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_ENABLE_REAR_MINES_DUNE3)
				IF GET_NUM_VEHICLE_MODS(veh, MOD_WING_R) > 0
					PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting MOD_WING_R to mod 0")
					PRELOAD_VEHICLE_MOD(veh, MOD_WING_R, 0)
					SET_VEHICLE_MOD(veh, MOD_WING_R, 0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_ENTITY_MODEL(veh) = TAMPA3
		IF GET_NUM_VEHICLE_MODS(veh, MOD_BUMPER_F) > 0
			IF IS_BIT_SET(iVehBitset, ciFMMC_VEHICLE5_USE_FRONT_WEAPON_MOD)
				PRINTLN("SET_VEHICLE_WEAPON_MODS - Setting MOD_BUMPER_F")
				PRELOAD_VEHICLE_MOD(veh, MOD_BUMPER_F, 0)
				SET_VEHICLE_MOD(veh, MOD_BUMPER_F, 0)
			ELSE
				SET_VEHICLE_MOD(veh, MOD_BUMPER_F, -1)
			ENDIF
		ENDIF
	ENDIF
	//END OF SPECIAL CASES //
	
	IF iNeonColour > -1
		PRINTLN("SET_VEHICLE_WEAPON_MODS APPLYING THE VEHICLE XENON LIGHTS iNeonColour: ", iNeonColour)	
		TOGGLE_VEHICLE_MOD(veh, MOD_TOGGLE_XENON_LIGHTS, TRUE)
		SET_VEHICLE_NEON_ENABLED(veh, NEON_FRONT, TRUE)
		SET_VEHICLE_NEON_ENABLED(veh, NEON_LEFT, TRUE)
		SET_VEHICLE_NEON_ENABLED(veh, NEON_RIGHT, TRUE)
		SET_VEHICLE_NEON_ENABLED(veh, NEON_BACK, TRUE)
		SET_VEHICLE_NEON_INDEX_COLOUR(veh, iNeonColour)			
	ENDIF
	
	RELEASE_PRELOAD_MODS(veh)
ENDPROC

FUNC BOOL HAVE_VEHICLE_WEAPON_MODS_LOADED(VEHICLE_INDEX veh, INT iVModTurret, INT iVModArmour, INT iVehBitset, INT iVeh, INT iVModFrontBumper, INT iVModExhaust, INT iVModBombBay, INT iVModSpoiler, INT iTeam = -1)

	BOOL bAllModsLoaded = TRUE
	IF DOES_ENTITY_EXIST(veh)
	
		IF NOT IS_VEHICLE_DRIVEABLE(veh)
			PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - VEHICLE NOT DRIVEABLE - exiting function")
			RETURN TRUE
		ENDIF
		
		IF IS_ENTITY_DEAD(veh)
			PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - VEHICLE IS DEAD - exiting function")
			RETURN TRUE
		ENDIF
		
		IF GET_ENTITY_MODEL(veh) = DUNE3
			IF GET_NUM_VEHICLE_MODS(veh, MOD_WING_R) > 0
				PRELOAD_VEHICLE_MOD(veh, MOD_WING_R, 0)
				IF NOT HAS_PRELOAD_MODS_FINISHED(veh)
					PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_WING_R - has not loaded")
					bAllModsLoaded = FALSE
				ELSE
					PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_WING_R - has loaded")
				ENDIF
			ENDIF
		ENDIF
		IF GET_NUM_VEHICLE_MODS(veh, MOD_ROOF) > 0
		AND iVModTurret != GET_VEHICLE_MOD(veh, MOD_ROOF)			
			PRELOAD_VEHICLE_MOD(veh, MOD_ROOF, iVModTurret)
			IF NOT HAS_PRELOAD_MODS_FINISHED(veh)
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_ROOF has not loaded")
				bAllModsLoaded = FALSE
			ELSE
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_ROOF has loaded")
			ENDIF
		ENDIF
		IF GET_NUM_VEHICLE_MODS(veh, MOD_CHASSIS) > 0
		AND iVModArmour > -1
		AND iVModArmour != GET_VEHICLE_MOD(veh, MOD_CHASSIS)
			PRELOAD_VEHICLE_MOD(veh, MOD_CHASSIS, iVModArmour)
			
			IF NOT HAS_PRELOAD_MODS_FINISHED(veh)
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_CHASSIS has not loaded")
				bAllModsLoaded = FALSE
			ELSE
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_CHASSIS has loaded")
			ENDIF
		ENDIF
		IF GET_NUM_VEHICLE_MODS(veh, MOD_BUMPER_F) > 0
		AND (IS_BIT_SET(iVehBitset, ciFMMC_VEHICLE5_USE_FRONT_WEAPON_MOD)
		OR iVModFrontBumper > -1)
			IF iVModFrontBumper > -1
				PRELOAD_VEHICLE_MOD(veh, MOD_BUMPER_F, iVModFrontBumper)
			ELSE
				PRELOAD_VEHICLE_MOD(veh, MOD_BUMPER_F, 0)
			ENDIF
			
			IF NOT HAS_PRELOAD_MODS_FINISHED(veh)
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_BUMPER_F has not loaded")
				bAllModsLoaded = FALSE
			ELSE
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_BUMPER_F has loaded")
			ENDIF
		ENDIF
		IF GET_NUM_VEHICLE_MODS(veh, MOD_BUMPER_R) > 0
		AND IS_BIT_SET(iVehBitset, ciFMMC_VEHICLE5_USE_REAR_WEAPON_MOD)
			
			PRELOAD_VEHICLE_MOD(veh, MOD_BUMPER_R, 0)
			
			IF NOT HAS_PRELOAD_MODS_FINISHED(veh)
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_BUMPER_R has not loaded")
				bAllModsLoaded = FALSE
			ELSE
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_BUMPER_R has loaded")
			ENDIF
		ENDIF
		IF GET_NUM_VEHICLE_MODS(veh, MOD_WING_R) > 0
		AND iVModBombBay > -1
		AND iVModBombBay != GET_VEHICLE_MOD(veh, MOD_WING_R)
		AND ((iTeam > -1 AND iTeam < FMMC_MAX_TEAMS AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2,  ciBS2_SPAWN_VEHICLE_APPLY_BOMB_MOD))
			OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_BOMB_BAY_MOD))
			
			PRELOAD_VEHICLE_MOD(veh, MOD_WING_R, 0)
			
			IF NOT HAS_PRELOAD_MODS_FINISHED(veh)
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_WING_R has not loaded")
				bAllModsLoaded = FALSE
			ELSE
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_WING_R has loaded")
			ENDIF
		ENDIF
		IF GET_NUM_VEHICLE_MODS(veh, MOD_EXHAUST) > 0
		AND iVModExhaust != GET_VEHICLE_MOD(veh, MOD_EXHAUST)		
			PRELOAD_VEHICLE_MOD(veh, MOD_EXHAUST, iVModExhaust)
			
			IF NOT HAS_PRELOAD_MODS_FINISHED(veh)
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_EXHAUST has not loaded")
				bAllModsLoaded = FALSE
			ELSE
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_EXHAUST has loaded")
			ENDIF
		ENDIF
		IF GET_NUM_VEHICLE_MODS(veh, MOD_SPOILER) > 0
		AND iVModSpoiler != GET_VEHICLE_MOD(veh, MOD_SPOILER)		
			PRELOAD_VEHICLE_MOD(veh, MOD_SPOILER, iVModSpoiler)
			
			IF NOT HAS_PRELOAD_MODS_FINISHED(veh)
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_SPOILER has not loaded")
				bAllModsLoaded = FALSE
			ELSE
				PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - MOD_SPOILER has loaded")
			ENDIF
		ENDIF
	ELSE
		PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED - Entity does not exist ")
		bAllModsLoaded = FALSE
	ENDIF
	IF bAllModsLoaded
		SET_VEHICLE_WEAPON_MODS(veh, iVModTurret, iVModArmour, iVehBitset, iVeh, iVModFrontBumper, iVModExhaust, iVModBombBay, iVModSpoiler)
	ENDIF
	
	RETURN bAllModsLoaded
	
ENDFUNC

PROC SET_VEHICLE_CUSTOM_MOD(VEHICLE_INDEX viVeh, INT& iModSetting, MOD_TYPE eAssociatedModType, INT iUpdateIndex = -1, INT iForceUpdateIndex = -1)
	
	UNUSED_PARAMETER(iUpdateIndex)
	UNUSED_PARAMETER(iForceUpdateIndex)
	
//	IF iModSetting = -1
//	AND iUpdateIndex != iForceUpdateIndex
//		EXIT
//	ENDIF

	INT iSettingToUse = iModSetting
	iSettingToUse = CLAMP_INT(iSettingToUse, -1, GET_NUM_VEHICLE_MODS(viVeh, eAssociatedModType))

	IF GET_NUM_VEHICLE_MODS(viVeh, eAssociatedModType) >= iSettingToUse
		PRINTLN("[VehCustomMods] SET_VEHICLE_CUSTOM_MOD | eAssociatedModType: ", eAssociatedModType, " / iModSetting: ", iModSetting, " || Applying on veh ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(viVeh)))
		SET_VEHICLE_MOD(viVeh, eAssociatedModType, iSettingToUse, FALSE)
	ELSE
		PRINTLN("[VehCustomMods] SET_VEHICLE_CUSTOM_MOD | eAssociatedModType: ", eAssociatedModType, " / iModSetting: ", iModSetting, " || INVALID for veh ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(viVeh)))
		iModSetting = -1
	ENDIF
ENDPROC

FUNC INT FMMC_GET_XENON_LIGHT_COLOUR_INDEX(INT iXenonIndex)
	
	IF iXenonIndex = -1
		RETURN 255
	ENDIF
	
	RETURN iXenonIndex	
	
ENDFUNC

PROC SET_VEHICLE_WHEEL_RIM_COLOUR(VEHICLE_INDEX viVeh, INT iColour)
	INT iColour1, iColourTemp
	GET_VEHICLE_EXTRA_COLOURS(viVeh,  iColour1, iColourTemp)
	SET_VEHICLE_EXTRA_COLOURS(viVeh, iColour1, iColour)
ENDPROC

PROC SET_VEHICLE_ALL_CUSTOM_MODS(VEHICLE_INDEX viVeh, INT iSlot, INT iForceUpdateIndex = -1)
	
	IF iSlot = -1
		EXIT
	ENDIF
	
	PLACED_VEHICLE_CUSTOM_MOD_SETTINGS sCustomModSettings = g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iSlot]
	
	IF NOT IS_BIT_SET(sCustomModSettings.iCustomMod_Bitset, ciCUSTOM_MOD_BITSET__IN_USE)
		PRINTLN("[VehCustomMods] SET_VEHICLE_ALL_CUSTOM_MODS | Settings in slot ", iSlot, " are not in use/enabled")
		EXIT
	ENDIF
	
	PRINTLN("[VehCustomMods] SET_VEHICLE_ALL_CUSTOM_MODS | Applying all custom mods to ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(viVeh)), " iSlot: ", iSlot, " =================================================")
	DEBUG_PRINTCALLSTACK()

	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Spoiler, MOD_SPOILER, 0, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Bumper_F, MOD_BUMPER_F, 1, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Bumper_R, MOD_BUMPER_R, 2, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Skirt, MOD_SKIRT, 3, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Exhaust, MOD_EXHAUST, 4, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Chassis, MOD_CHASSIS, 5, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Grill, MOD_GRILL, 6, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Bonnet, MOD_BONNET, 7, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Wing_L, MOD_WING_L, 8, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Wing_R, MOD_WING_R, 9, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Roof, MOD_ROOF, 10, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Engine, MOD_ENGINE, 11, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Brakes, MOD_BRAKES, 12, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Gearbox, MOD_GEARBOX, 13, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Horn, MOD_HORN, 14, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Suspension, MOD_SUSPENSION, 15, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Armour, MOD_ARMOUR, 16, iForceUpdateIndex)
//	INT iCustomMod_Toggle_Nitrous = -1
//	INT iCustomMod_Toggle_Subwoofer = -1
//	INT iCustomMod_Toggle_Hydraulics = -1	
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Wheels, MOD_WHEELS, 17, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Rear_Wheels, MOD_REAR_WHEELS, 18, iForceUpdateIndex)
	//INT iCustomMod_PLTHolder = -1
	//INT iCustomMod_PLTVanity = -1
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Interior1, MOD_INTERIOR1, 19, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Interior2, MOD_INTERIOR2, 20, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Interior3, MOD_INTERIOR3, 21, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Interior4, MOD_INTERIOR4, 22, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Interior5, MOD_INTERIOR5, 23, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Seats, MOD_SEATS, 24, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Steering, MOD_STEERING, 25, iForceUpdateIndex)
	//INT iCustomMod_Knob = -1
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Plaque, MOD_PLAQUE, 26, iForceUpdateIndex)
	//INT iCustomMod_Ice = -1
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Trunk, MOD_TRUNK, 27, iForceUpdateIndex)
	//INT iCustomMod_Hydro = -1
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_EngineBay1, MOD_ENGINEBAY1, 28, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_EngineBay2, MOD_ENGINEBAY2, 29, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_EngineBay3, MOD_ENGINEBAY3, 30, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Chassis2, MOD_CHASSIS2, 31, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Chassis3, MOD_CHASSIS3, 32, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Chassis4, MOD_CHASSIS4, 33, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Chassis5, MOD_CHASSIS5, 34, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Door_L, MOD_DOOR_L, 35, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Door_R, MOD_DOOR_R, 36, iForceUpdateIndex)
	SET_VEHICLE_CUSTOM_MOD(viVeh, sCustomModSettings.iCustomMod_Livery, MOD_LIVERY, 37, iForceUpdateIndex)
	
	IF sCustomModSettings.iCustomMod_PlateText != -1
		SET_VEHICLE_NUMBER_PLATE_TEXT(viVeh, GET_CUSTOM_STRING_LIST_STRING(sCustomModSettings.iCustomMod_PlateText))
	ENDIF
	
	IF sCustomModSettings.iCustomMod_PLTHolder >= 0 
	AND sCustomModSettings.iCustomMod_PLTHolder < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
		SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(viVeh, sCustomModSettings.iCustomMod_PLTHolder)
	ENDIF
	
	IF sCustomModSettings.iCustomMod_Toggle_Xenon_Lights != -1
		TOGGLE_VEHICLE_MOD(viVeh, MOD_TOGGLE_XENON_LIGHTS, TRUE)
		SET_VEHICLE_XENON_LIGHT_COLOR_INDEX(viVeh, FMMC_GET_XENON_LIGHT_COLOUR_INDEX(sCustomModSettings.iCustomMod_Toggle_Xenon_Lights))
	ENDIF
	
	IF sCustomModSettings.iCustomMod_Toggle_Turbo != -1
		TOGGLE_VEHICLE_MOD(viVeh, MOD_TOGGLE_TURBO, TRUE)
	ENDIF
	
	IF sCustomModSettings.iCustomMod_Toggle_Tyre_Smoke != 0
		TOGGLE_VEHICLE_MOD(viVeh, MOD_TOGGLE_TYRE_SMOKE, TRUE)
		SET_TYRE_SMOKE_COLOUR_FROM_ENUM(viVeh, INT_TO_ENUM(TYRE_SMOKE_COLOUR_ENUM, sCustomModSettings.iCustomMod_Toggle_Tyre_Smoke))
	ENDIF
	
	IF sCustomModSettings.iCustomMod_WheelRimColour != -1
		SET_VEHICLE_WHEEL_RIM_COLOUR(viVeh, sCustomModSettings.iCustomMod_WheelRimColour)
	ENDIF
	
	PRINTLN("[VehCustomMods] SET_VEHICLE_ALL_CUSTOM_MODS | ================================================================================================")
	
ENDPROC



/// PURPOSE:
///    Want to be able to set individual vehicle doors as unbrakable, so they won't be missing in cutscenes etc. See 2085878
PROC SET_UNBREAKABLE_DOORS_FOR_VEHICLE(VEHICLE_INDEX veh, INT index )
	PRINTLN("[RCC MISSION] SET_BREAKABLE_DOORS_FOR_VEHICLE called ")
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[index].iUnbreakableDoorBitset, ciUNBREAKABLE_DOOR_FRONT_LEFT)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(veh, SC_DOOR_FRONT_LEFT, FALSE)						
		PRINTLN("[RCC MISSION] SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF SC_DOOR_FRONT_LEFT can't be broken ")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[index].iUnbreakableDoorBitset, ciUNBREAKABLE_DOOR_FRONT_RIGHT)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(veh, SC_DOOR_FRONT_RIGHT, FALSE)						
		PRINTLN("[RCC MISSION] SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF SC_DOOR_FRONT_RIGHT can't be broken ")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[index].iUnbreakableDoorBitset, ciUNBREAKABLE_DOOR_REAR_LEFT)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(veh, SC_DOOR_REAR_LEFT, FALSE)						
		PRINTLN("[RCC MISSION] SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF SC_DOOR_REAR_LEFT can't be broken ")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[index].iUnbreakableDoorBitset, ciUNBREAKABLE_DOOR_REAR_RIGHT)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(veh, SC_DOOR_REAR_RIGHT, FALSE)						
		PRINTLN("[RCC MISSION] SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF SC_DOOR_REAR_RIGHT can't be broken ")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[index].iUnbreakableDoorBitset, ciUNBREAKABLE_DOOR_BONNET)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(veh, SC_DOOR_BONNET, FALSE)						
		PRINTLN("[RCC MISSION] SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF SC_DOOR_BONNET can't be broken ")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[index].iUnbreakableDoorBitset, ciUNBREAKABLE_DOOR_BOOT)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(veh, SC_DOOR_BOOT, FALSE)						
		PRINTLN("[RCC MISSION] SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF SC_DOOR_BOOT can't be broken ")
	ENDIF
ENDPROC

PROC CHECK_TO_OPEN_THE_DOORS(VEHICLE_INDEX &vehicle, INT iDoorBitset, INT iDoorAvailable)

	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(vehicle)
	
	IF mnVeh = AVISA
		EXIT
	ENDIF
	
	PRINTLN("CHECK_TO_OPEN_THE_DOORS = iDoorBitset: ", iDoorBitset, " iDoorAvailable: ", iDoorAvailable)
	
	IF IS_BIT_SET(iDoorAvailable, enum_to_int(SC_DOOR_FRONT_LEFT))
		IF IS_BIT_SET(iDoorBitset, enum_to_int(SC_DOOR_FRONT_LEFT))
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_FRONT_LEFT) < 0.2
				SET_VEHICLE_DOOR_OPEN(vehicle, SC_DOOR_FRONT_LEFT, FALSE, FALSE)
				PRINTLN("SC_DOOR_FRONT_LEFT OPEN")
			ENDIF
		ELSE
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_FRONT_LEFT) > 0
				SET_VEHICLE_DOOR_SHUT(vehicle, SC_DOOR_FRONT_LEFT, TRUE)
				PRINTLN("SC_DOOR_FRONT_LEFT CLOSED")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iDoorAvailable, enum_to_int(SC_DOOR_FRONT_RIGHT))
		IF IS_BIT_SET(iDoorBitset, enum_to_int(SC_DOOR_FRONT_RIGHT))
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_FRONT_RIGHT) < 0.2
				SET_VEHICLE_DOOR_OPEN(vehicle, SC_DOOR_FRONT_RIGHT, FALSE, FALSE)
				PRINTLN("SC_DOOR_FRONT_RIGHT OPEN")
			ENDIF
		ELSE
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_FRONT_RIGHT) > 0
				SET_VEHICLE_DOOR_SHUT(vehicle, SC_DOOR_FRONT_RIGHT, TRUE)
				PRINTLN("SC_DOOR_FRONT_RIGHT CLOSED")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iDoorAvailable, enum_to_int(SC_DOOR_REAR_LEFT))
		IF IS_BIT_SET(iDoorBitset, enum_to_int(SC_DOOR_REAR_LEFT))
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_REAR_LEFT) < 0.2
				SET_VEHICLE_DOOR_OPEN(vehicle, SC_DOOR_REAR_LEFT, FALSE, FALSE)
				PRINTLN("SC_DOOR_REAR_LEFT OPEN")
			ENDIF
		ELSE
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_REAR_LEFT) > 0
				SET_VEHICLE_DOOR_SHUT(vehicle, SC_DOOR_REAR_LEFT, TRUE)
				PRINTLN("SC_DOOR_REAR_LEFT CLOSED")
			ENDIF
		ENDIF
	ENDIF
			
	IF IS_BIT_SET(iDoorAvailable, enum_to_int(SC_DOOR_REAR_RIGHT))
		IF IS_BIT_SET(iDoorBitset, enum_to_int(SC_DOOR_REAR_RIGHT))
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_REAR_RIGHT) < 0.2
				SET_VEHICLE_DOOR_OPEN(vehicle, SC_DOOR_REAR_RIGHT, FALSE, FALSE)
				PRINTLN("SC_DOOR_REAR_RIGHT OPEN")
			ENDIF
		ELSE
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_REAR_RIGHT) > 0
				SET_VEHICLE_DOOR_SHUT(vehicle, SC_DOOR_REAR_RIGHT, TRUE)
				PRINTLN("SC_DOOR_REAR_RIGHT CLOSED")
			ENDIF
		ENDIF
	ENDIF
			
	IF IS_BIT_SET(iDoorAvailable, enum_to_int(SC_DOOR_BONNET))
		IF IS_BIT_SET(iDoorBitset, enum_to_int(SC_DOOR_BONNET))
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_BONNET) < 0.2
				SET_VEHICLE_DOOR_OPEN(vehicle, SC_DOOR_BONNET, FALSE, FALSE)
				PRINTLN("SC_DOOR_BONNET OPEN")
			ENDIF
		ELSE
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_BONNET) > 0
				SET_VEHICLE_DOOR_SHUT(vehicle, SC_DOOR_BONNET, TRUE)
				PRINTLN("SC_DOOR_BONNET CLOSED")
			ENDIF
		ENDIF
	ENDIF
			
	IF IS_BIT_SET(iDoorAvailable, enum_to_int(SC_DOOR_BOOT))
		IF IS_BIT_SET(iDoorBitset, enum_to_int(SC_DOOR_BOOT))
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_BOOT) < 0.2
				SET_VEHICLE_DOOR_OPEN(vehicle, SC_DOOR_BOOT, FALSE, FALSE)
				PRINTLN("SC_DOOR_BOOT OPEN")
			ENDIF
		ELSE
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehicle, SC_DOOR_BOOT) > 0
				SET_VEHICLE_DOOR_SHUT(vehicle, SC_DOOR_BOOT, TRUE)
				PRINTLN("SC_DOOR_BOOT CLOSED")
			ENDIF
		ENDIF
	ENDIF
			
			
ENDPROC

PROC SET_MENU_OPTIONS_FROM_VEHICLE_EXTRAS(structFMMC_MENU_ITEMS &sFMMCmenu, VEHICLE_INDEX veh)
	INT i
	FOR i = 0 TO 13
		IF IS_VEHICLE_EXTRA_TURNED_ON(veh, i+1)
			SET_BIT(sFMMCmenu.iVehicleExtrasBitset, i)
		ELSE
			CLEAR_BIT(sFMMCmenu.iVehicleExtrasBitset, i)
		ENDIF
	ENDFOR
ENDPROC

PROC SET_VEHICLE_EXTRAS_FROM_BITSET(INT iBitset, VEHICLE_INDEX veh)
	INT i
	FOR i = 0 TO 13
		IF IS_BIT_SET(iBitset, i)
		AND NOT IS_VEHICLE_EXTRA_TURNED_ON(veh, i+1)
			SET_VEHICLE_EXTRA(veh, i+1, FALSE)
		ELIF NOT IS_BIT_SET(iBitset, i)
		AND IS_VEHICLE_EXTRA_TURNED_ON(veh, i+1)
			SET_VEHICLE_EXTRA(veh, i+1, TRUE)
		ENDIF
	ENDFOR
ENDPROC

PROC CHECK_IF_VEHICLE_SHOULD_BE_RETAINED_IN_INTERIOR(VEHICLE_INDEX viPassed, VECTOR vSpawnPos, INT iSpawnInterior #IF IS_DEBUG_BUILD , INT iveh #ENDIF )
	
	IF iSpawnInterior != -1
		
		INTERIOR_INSTANCE_INDEX tempInterior
		
		PRINTLN("[Vehicles][Vehicle ", iveh, "] CHECK_IF_VEHICLE_SHOULD_BE_RETAINED_IN_INTERIOR - vehicle ", iveh, " has iInterior ", iSpawnInterior)
		tempInterior = INT_TO_NATIVE(INTERIOR_INSTANCE_INDEX, iSpawnInterior)
		
		IF IS_VALID_INTERIOR(tempInterior)
			
			VECTOR vVehicle = GET_ENTITY_COORDS(viPassed)
			
			IF IS_INTERIOR_READY(tempInterior)
				PRINTLN("[Vehicles][Vehicle ", iveh, "] CHECK_IF_VEHICLE_SHOULD_BE_RETAINED_IN_INTERIOR - Vehicle ", iveh, " RETAIN_ENTITY_IN_INTERIOR called on interior ", iSpawnInterior)
				RETAIN_ENTITY_IN_INTERIOR(viPassed, tempInterior)
			
				IF NOT ARE_VECTORS_ALMOST_EQUAL(vVehicle, vSpawnPos, 0.1)
					PRINTLN("[Vehicles][Vehicle ", iveh, "] CHECK_IF_VEHICLE_SHOULD_BE_RETAINED_IN_INTERIOR - Moving vehicle ", iveh," as it is not within 0.1 of where it should be: vVehicle ",vVehicle,", vSpawnPos ",vSpawnPos)
					SET_ENTITY_COORDS(viPassed, vSpawnPos)
				ELSE
					PRINTLN("[Vehicles][Vehicle ", iveh, "] CHECK_IF_VEHICLE_SHOULD_BE_RETAINED_IN_INTERIOR - Vehicle ", iveh," is where it should be - ",vSpawnPos)
				ENDIF
			ELSE
				PRINTLN("[Vehicles][Vehicle ", iveh, "] CHECK_IF_VEHICLE_SHOULD_BE_RETAINED_IN_INTERIOR - Interior is valid but not ready, veh ", iveh," might not be correctly placed - vSpawnPos ",vSpawnPos,", vVehicle ",vVehicle)
			ENDIF
		ELSE
			CASSERTLN(DEBUG_CONTROLLER, "[Vehicles][Vehicle ", iveh, "] CHECK_IF_VEHICLE_SHOULD_BE_RETAINED_IN_INTERIOR - Vehicle ", iveh," supposed to be retained in interior ", iSpawnInterior," but it isn't valid!")
			PRINTLN("[Vehicles][Vehicle ", iveh, "] CHECK_IF_VEHICLE_SHOULD_BE_RETAINED_IN_INTERIOR - Vehicle ", iveh," supposed to be retained in interior ", iSpawnInterior," but it isn't valid!")
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_VEHICLE_CREATION_IGNORE_GROUND_CHECK(INT iVeh, VECTOR vSpawnPos, INT iSpawnInterior)
	
	#IF NOT IS_DEBUG_BUILD
	UNUSED_PARAMETER(iVeh)
	#ENDIF
	
	IF vSpawnPos.z < -100
	AND iSpawnInterior != -1
		PRINTLN("[RCC MISSION] SHOULD_VEHICLE_CREATION_IGNORE_GROUND_CHECK - Returning TRUE for vehicle ",iVeh," - spawn pos z ",vSpawnPos.z," is < -100, and has valid iSpawnInterior ",iSpawnInterior)
		RETURN TRUE
	ENDIF
	
	PRINTLN("[RCC MISSION] SHOULD_VEHICLE_CREATION_IGNORE_GROUND_CHECK - Returning FALSE for vehicle ",iVeh," - spawn pos z ",vSpawnPos.z," is >= -100 or has invalid iSpawnInterior ",iSpawnInterior)
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_USE_OLD_STYLE_CAMO(MODEL_NAMES nmPassedInVeh)
	IF nmPassedInVeh = BESRA
	OR nmPassedInVeh = LAZER
	OR nmPassedInVeh = BARRACKS
	OR nmPassedInVeh = BARRACKS2
	OR nmPassedInVeh = BARRACKS3
	OR nmPassedInVeh = DUNE3
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


FUNC INT GET_FMMC_VEHICLE_MAX_HEALTH(INT iVeh, INT iNumPlayers = 0)

	PRINTLN("[RCC MISSION] g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iHealth = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iHealth, ", iNumPlayers = ", iNumPlayers)
	
	FLOAT fHealth = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iHealth/100.0
	
	INT iReturnHealth
	iReturnHealth = FLOOR(fHealth*1000)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_SCALE_HEALTH)
	AND iNumPlayers > 0
		iReturnHealth = iReturnHealth * iNumPlayers
		PRINTLN("[JS] GET_FMMC_VEHICLE_MAX_HEALTH - Scaling health by ", iNumPlayers ," = ", iReturnHealth)
	ENDIF
	
	IF iReturnHealth > 262143 //max allowed
		iReturnHealth = 262143
	ENDIF
	
	RETURN iReturnHealth
	
ENDFUNC
FUNC BOOL IS_VALID_ROTATION_VECTOR(VECTOR vVectToCheck)
	IF IS_VECTOR_ZERO(vVectToCheck) // If it's 0 then we might as well just use the heading.
	OR ARE_VECTORS_EQUAL(vVectToCheck, <<999, 999, 999>>)
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

PROC SET_FMMC_VEHICLE_ROTATION(VEHICLE_INDEX viPlacedVehicle, VECTOR vRotToSetTo)
	INT iEscape = 0
	WHILE(vRotToSetTo.x < -180 AND iEscape < 10)
		IF vRotToSetTo.x < -180
			vRotToSetTo.x += 360
		ENDIF
		iEscape++
	ENDWHILE
	
	iEscape = 0
	WHILE(vRotToSetTo.x > 180 AND iEscape < 10)
		IF vRotToSetTo.x > 180
			vRotToSetTo.x -= 360
		ENDIF
		iEscape++
	ENDWHILE
	
	iEscape = 0
	WHILE(vRotToSetTo.y < -180 AND iEscape < 10)
		IF vRotToSetTo.y < -180
			vRotToSetTo.y += 360
		ENDIF
	iEscape++
	ENDWHILE
	
	iEscape = 0
	WHILE(vRotToSetTo.y > 180 AND iEscape < 10)
		IF vRotToSetTo.y > 180
			vRotToSetTo.y -= 360
		ENDIF
		iEscape++
	ENDWHILE
	
	iEscape = 0
	WHILE(vRotToSetTo.z < -180 AND iEscape < 10)
		IF vRotToSetTo.z < -180
			vRotToSetTo.z += 360
		ENDIF
		iEscape++
	ENDWHILE
	
	iEscape = 0
	WHILE(vRotToSetTo.z > 180 AND iEscape < 10)
		IF vRotToSetTo.z > 180
			vRotToSetTo.z -= 360
		ENDIF
		iEscape++
	ENDWHILE
	
	PRINTLN("[ROTATION_VECTOR][RH][SET_FMMC_VEHICLE_ROTATION] vRotToSetTo: ", vRotToSetTo)
	SET_ENTITY_ROTATION(viPlacedVehicle, vRotToSetTo)
ENDPROC


//////////////////////////////      MAKING  VEHICELS           //////////////////////////////
PROC SET_UP_FMMC_VEH(VEHICLE_INDEX viPassed, INT iVehNumber)
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_23 tl23
		tl23 = "niVehicle "
		tl23 += iVehNumber 
		SET_VEHICLE_NAME_DEBUG(viPassed, tl23)
	#ENDIF
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(viPassed, TRUE)
	SET_VEHICLE_HAS_STRONG_AXLES(viPassed, TRUE)
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehNumber].iVehBitsetSeven, ciFMMC_VEHICLE7_BULLETPROOF_TYRES)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH] Setting vehicle ", iVehNumber," Tyres cannot burst.")
		SET_VEHICLE_TYRES_CAN_BURST(viPassed, FALSE)
	ENDIF
	
	INT iTempBS
	IF DECOR_EXIST_ON(viPassed, "MPBitset")
		iTempBS = DECOR_GET_INT(viPassed,"MPBitset")
	ENDIF
	
	IF IS_VEHICLE_A_HILL_ENTITY(iVehNumber)
		SET_BIT(iTempBS, MP_DECORATOR_BS_IS_HILL_ENTITY)
		DECOR_SET_INT(viPassed, "MPBitset", iTempBS)
		PRINTLN("[KOTH] SET_UP_FMMC_VEH - setting MP_DECORATOR_BS_IS_HILL_ENTITY on veh ", iVehNumber)
	ENDIF
	
	HANDLE_VEH_TEAM_RESTRICTION(iVehNumber, viPassed)
ENDPROC
	
FUNC BOOL IS_VEHICLE_MISSION_CRITICAL(INT iveh)

INT iteam
	
	FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iPriority[iteam] < FMMC_MAX_RULES
		OR IS_BIT_SET(g_FMMC_STRUCT.iMissionCriticalvehBS[iTeam],iveh)	
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE

ENDFUNC

FUNC BOOL DOES_VEHICLE_RULE_REQUIRE_A_KILL(INT iRule, INT iTeam, INT iPriority)
	
	SWITCH iRule
		CASE FMMC_OBJECTIVE_LOGIC_KILL
			RETURN TRUE
		
		CASE FMMC_OBJECTIVE_LOGIC_CAPTURE
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iPriority], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

//With a Team of -1, this will check through all teams:
FUNC BOOL IS_VEHICLE_MISSION_KILL(INT iVeh, INT iTeam = -1)
	
	BOOL bKillVeh = FALSE
	
	IF iTeam = -1
		
		INT iTeamLoop
		
		FOR iTeamLoop = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)
			
			//Recursion!
			IF IS_VEHICLE_MISSION_KILL(iVeh,iTeamLoop)
				bKillVeh = TRUE
				iTeamLoop = FMMC_MAX_TEAMS//Break out
			ENDIF
			
		ENDFOR
		
	ELSE
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam] < FMMC_MAX_RULES
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
			IF DOES_VEHICLE_RULE_REQUIRE_A_KILL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRule[iTeam], iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam])
				bKillVeh = TRUE
				iTeam = FMMC_MAX_TEAMS //Break out, we've found a team with this on a kill rule!
			ELSE
				//Check extra objectives:
				INT iextraObjectiveNum
				// Loop through all potential entites that have multiple rules
				FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
					// If the type matches the one passed
					IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = ciRULE_TYPE_VEHICLE
					AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iVeh
						INT iextraObjectiveLoop
						
						FOR iextraObjectiveLoop = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)
							IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] < FMMC_MAX_RULES
							AND (GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]) != FMMC_OBJECTIVE_LOGIC_NONE)
								
								IF DOES_VEHICLE_RULE_REQUIRE_A_KILL(GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]), iTeam, g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam])
									bKillVeh = TRUE
									iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY //Break out - we've found a kill rule
								ENDIF
								
							ELSE
								iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY //Break out - we've reached the number of extra rules
							ENDIF
						ENDFOR
						//We found the multi-rule number for this entity, there won't be any more so break out of the loop:
						iextraObjectiveNum = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN bKillVeh

ENDFUNC

FUNC FLOAT GET_FMMC_VEHICLE_MAX_BODY_HEALTH(INT iVeh, INT numPlayers = 0)	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_VEHICLE_SCALE_COMPONENTS_HEALTH)
	AND numPlayers > 0
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fBodyHealth * numPlayers
	ELSE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fBodyHealth
	ENDIF
ENDFUNC

FUNC FLOAT GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(INT iVeh, INT numPlayers = 0)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_VEHICLE_SCALE_COMPONENTS_HEALTH)
	AND numPlayers > 0
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fEngineHealth * numPlayers
	ELSE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fEngineHealth
	ENDIF	
ENDFUNC

FUNC FLOAT GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(INT iVeh, INT numPlayers = 0)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_VEHICLE_SCALE_COMPONENTS_HEALTH)
	AND numPlayers > 0
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fPetrolHealth * numPlayers
	ELSE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fPetrolHealth
	ENDIF	
ENDFUNC


FUNC FLOAT GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(INT iVeh, INT numPlayers = 0)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_VEHICLE_SCALE_COMPONENTS_HEALTH)
	AND numPlayers > 0
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHeliMainRotorHealth * numPlayers
	ELSE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHeliMainRotorHealth
	ENDIF	
ENDFUNC


FUNC FLOAT GET_FMMC_HELI_MAX_TAIL_ROTOR_HEALTH(INT iVeh, INT numPlayers = 0)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_VEHICLE_SCALE_COMPONENTS_HEALTH)
	AND numPlayers > 0
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHeliTailRotorHealth * numPlayers
	ELSE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHeliTailRotorHealth
	ENDIF	
ENDFUNC

FUNC FLOAT GET_VEHICLE_HEALTH_PRIVATE(VEHICLE_INDEX tempVeh, INT iVeh, INT numPlayers)
	RETURN GET_VEHICLE_HEALTH_PERCENTAGE(tempVeh,
		GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iVeh, numPlayers),
		GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(iVeh, numPlayers),
		GET_FMMC_VEHICLE_MAX_BODY_HEALTH(iVeh, numPlayers), 
		GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iVeh, numPlayers),
		GET_FMMC_HELI_MAX_TAIL_ROTOR_HEALTH(iVeh, numPlayers))
ENDFUNC

FUNC FLOAT GET_RESPAWN_VEHICLE_TOTAL_HEALTH_PERCENTAGE(VEHICLE_INDEX tempVeh,INT iTeam)
	IF NOT IS_ENTITY_ALIVE(tempVeh)
		RETURN 0.0
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_TEAM_VEHICLE_USE_ONLY_HEALTH_BODY)
		RETURN (100.0 * GET_VEHICLE_BODY_HEALTH(tempVeh) / GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(iTeam))
	ENDIF
	FLOAT iHealth = TO_FLOAT(GET_FMMC_PLAYER_VEHICLE_HEALTH_FOR_TEAM(iTeam))
	RETURN GET_VEHICLE_HEALTH_PERCENTAGE(tempVeh,iHealth,iHealth, GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(iTeam))
ENDFUNC

FUNC FLOAT GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(VEHICLE_INDEX tempVeh, INT iVeh, INT numPlayers = 0, BOOL bTrailer = FALSE)
	IF NOT IS_ENTITY_ALIVE(tempVeh)
		RETURN 0.0
	ENDIF
	
	IF iveh = -1
		SCRIPT_ASSERT("[LM][GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE] - Passing in -1. BAD. Sort it out. (likely GoTo Assigned)")
		RETURN 0.0
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR )		
		FLOAT fReturn = (100.0 * GET_VEHICLE_BODY_HEALTH(tempVeh) / GET_FMMC_VEHICLE_MAX_BODY_HEALTH(iVeh, numPlayers))
		PRINTLN("[LM][GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE] - Returning ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR special: ", fReturn)
		RETURN fReturn
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_CUSTOM_HEALTH_BAR_CALCULATION )		
		FLOAT health1 = GET_VEHICLE_HEALTH_PRIVATE(tempVeh, iVeh, numPlayers)
		FLOAT health2 = 100.0 * GET_VEHICLE_ENGINE_HEALTH(tempVeh) / GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iVeh, numPlayers)
		FLOAT health3 = 100.0 * GET_VEHICLE_PETROL_TANK_HEALTH(tempVeh) / GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(iVeh, numPlayers)
		FLOAT health4
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_VEHICLE_EXPLODE_AT_ZERO_BODY)
			health4 = 100.0 * GET_VEHICLE_BODY_HEALTH(tempVeh) / GET_FMMC_VEHICLE_MAX_BODY_HEALTH(iVeh, numPlayers)
			IF health4 < health3
				health3 = health4
			ENDIF
		ENDIF
		IF health3 < health2
			health2 = health3
		ENDIF
		IF health1 < health2
			RETURN health1
		ENDIF
		RETURN health2
	ENDIF
	IF bTrailer
		FLOAT fMaxHealth = GET_FMMC_VEHICLE_MAX_BODY_HEALTH(iVeh, numPlayers)
		FLOAT fBodyHealth = GET_VEHICLE_BODY_HEALTH(tempVeh)
		RETURN (fBodyHealth / fMaxHealth) * 100
	ELSE
		RETURN GET_VEHICLE_HEALTH_PRIVATE(tempVeh, iVeh, numPlayers)
	ENDIF
ENDFUNC

FUNC FLOAT GET_CARGOBOB_ROPE_DAMPENING(INT iVeh)
	
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fCargobobDamp
	
ENDFUNC

FUNC BOOL IS_VEHICLE_A_DROP_OFF_VEHICLE(INT iVeh, BOOL& bHookDropOff)
	
	BOOL bDropOff = FALSE
	
	INT iTeam
	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		
		INT iRule
		
		FOR iRule = 0 TO (FMMC_MAX_RULES - 1)
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_VEHICLE
			AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule] = iVeh
				
				bDropOff = TRUE
				
				IF (NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehDropOff_GetInBS, iRule))
				AND IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehDropOff_HookUpBS, iRule)
					bHookDropOff = TRUE
					//Break out, no more information is needed:
					iRule = FMMC_MAX_RULES
					iTeam = FMMC_MAX_TEAMS
				ENDIF
				
			ENDIF
		ENDFOR
		
	ENDFOR
	
	RETURN bDropOff
	
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_HAVE_SEARCHLIGHT_ON()
	
	INT iHours = GET_CLOCK_HOURS()
	
	IF iHours <= 5 
	OR iHours >= 21
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_MISSION_VEHICLE_TO_BE_USED_AS_A_RESPAWN_VEHICLE_BY_ANYBODY(INT iveh)
	
	INT iTeam, iRule
	
	FOR iTeam = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInLastMissionVehicleFromBS, iveh)
			PRINTLN("[RCC MISSION] IS_MISSION_VEHICLE_TO_BE_USED_AS_A_RESPAWN_VEHICLE_BY_ANYBODY - Returning TRUE for iveh ",iveh,", set in team ",iTeam,"'s iRespawnInLastMissionVehicleFromBS ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInLastMissionVehicleFromBS)
			RETURN TRUE
		ENDIF
		
		FOR iRule = 0 TO (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iNumberOfTeamRules - 1)
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInMissionVehicle[iRule] = iveh
				PRINTLN("[RCC MISSION] IS_MISSION_VEHICLE_TO_BE_USED_AS_A_RESPAWN_VEHICLE_BY_ANYBODY - Returning TRUE for iveh ",iveh,", used by team ",iTeam," rule ",iRule)
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDFOR
	
	RETURN FALSE
	
ENDFUNC

PROC HANDLE_SETTING_GANGOPS_DELUXO_COLOUR(INT iVeh, VEHICLE_INDEX viPassed)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iDeluxoIndex > -1		
		INT iColour1, iColour2, iExtraColour1, iExtraColour2
		
		PRINTLN("[DeluxoColours] HANDLE_SETTING_MY_DELUXO_COLOUR - Gang boss is now about to set the colours of the car with Deluxo index of ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iDeluxoIndex)
		
		SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iDeluxoIndex
			CASE 0
				GET_GANG_OPS_VEHICLE_ZERO_COLOUR_STATS(iColour1, iColour2, iExtraColour1, iExtraColour2)
			BREAK
			
			CASE 1
				GET_GANG_OPS_VEHICLE_ONE_COLOUR_STATS(iColour1, iColour2, iExtraColour1, iExtraColour2)
			BREAK
			
			CASE 2
				GET_GANG_OPS_VEHICLE_TWO_COLOUR_STATS(iColour1, iColour2, iExtraColour1, iExtraColour2)
			BREAK
			
			CASE 3
				GET_GANG_OPS_VEHICLE_THREE_COLOUR_STATS(iColour1, iColour2, iExtraColour1, iExtraColour2)
			BREAK
		ENDSWITCH
		
		IF iColour1 > -1
		AND iColour2 > -1
			SET_VEHICLE_COLOURS(viPassed, iColour1, iColour2)
			SET_VEHICLE_EXTRA_COLOURS(viPassed, iExtraColour1, iExtraColour2)
			PRINTLN("[DeluxoColours] HANDLE_SETTING_MY_DELUXO_COLOUR - Deluxo Colour has now been changed based on stats from the Gang Boss! Deluxo Colours should now be: ", iColour1, " / ", iColour2, " / ", iExtraColour1, " / ", iExtraColour2, " for Veh ", iVeh)	
		ELSE
			PRINTLN("[DeluxoColours] HANDLE_SETTING_MY_DELUXO_COLOUR - Required mission was not done (freemode) so vars are set to -1. ", iColour1, " / ", iColour2, " / ", iExtraColour1, " / ", iExtraColour2, " for Veh ", iVeh, " NOT SETTING COLOUR.")
		ENDIF
	ENDIF
	
	PRINTLN("[DeluxoColours] HANDLE_SETTING_MY_DELUXO_COLOUR - Deluxo Index of veh ", iVeh, " is ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iDeluxoIndex)
	
ENDPROC

PROC BURST_VEHICLE_TYRES(VEHICLE_INDEX tempVeh)
	MODEL_NAMES mn = GET_ENTITY_MODEL(tempVeh)
	IF NOT IS_THIS_MODEL_A_BOAT(mn) AND NOT IS_THIS_MODEL_A_PLANE(mn) AND NOT IS_THIS_MODEL_A_HELI(mn) IS_THIS_MODEL_A_JETSKI(mn)
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_FRONT_LEFT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_FRONT_LEFT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_FRONT_RIGHT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_FRONT_RIGHT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_MID_LEFT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_MID_LEFT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_MID_RIGHT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_MID_RIGHT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_REAR_LEFT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_REAR_LEFT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_REAR_RIGHT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_CAR_REAR_RIGHT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_BIKE_FRONT)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_BIKE_FRONT, TRUE)
		ENDIF
		IF NOT IS_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_BIKE_REAR)
			SET_VEHICLE_TYRE_BURST(tempVeh, SC_WHEEL_BIKE_REAR, TRUE)
		ENDIF
	ENDIF
	PRINTLN("[JR][BTOS]Popping tyres because ciFMMC_VEHICLE7_BURST_TYRES_ON_SPAWN is set to TRUE.")
ENDPROC


PROC SET_UP_FMMC_VEH_RC(VEHICLE_INDEX viPassed,INT iveh, NETWORK_INDEX &niVehicle[], VECTOR vSpawnLoc, BOOL bUsingRestartPoint, INT iSpawnInterior = -1, BOOL bVisible = TRUE, INT iNumPlayers = -1)
	
	UNUSED_PARAMETER(bUsingRestartPoint)
	
	INT iHealth, i
	PRINTLN("SET_UP_FMMC_VEH_RC - Veh: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(viPassed)), " / iVeh: ", iVeh)
	
	IF IS_VEHICLE_MODEL(viPassed, MULE)
		FMMC_SET_THIS_VEHICLE_EXTRAS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery)
	ELSE
		FMMC_SET_THIS_VEHICLE_EXTRAS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet)
		IF IS_VEHICLE_MODEL(viPassed, TRAILERLARGE)
			IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_ALLOW_MOC_ENTRY))
				IF DECOR_IS_REGISTERED_AS_TYPE("Creator_Trailer", DECOR_TYPE_INT)
					IF NOT DECOR_EXIST_ON(viPassed, "Creator_Trailer")
						IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
							DECOR_SET_INT(viPassed, "Creator_Trailer", NETWORK_HASH_FROM_PLAYER_HANDLE(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] setting ID decorator on created trailer to Creator_Trailer")
						ELSE
							SCRIPT_ASSERT("[RCC MISSION][SET_UP_FMMC_VEH_RC] - player's boss is invalid! Make sure you are in an MC.")
						ENDIF
					ENDIF
				ENDIF
				SET_VEHICLE_WEAPON_MODS(viPassed, 1, 0, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, iveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVModAirCounter, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVModExhaust, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVModBombBay, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVModSpoiler)
			ENDIF
		ELIF IS_VEHICLE_MODEL(viPassed, AVENGER)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
				
				IF DECOR_IS_REGISTERED_AS_TYPE("Creator_Trailer", DECOR_TYPE_INT)
				
					IF NOT DECOR_EXIST_ON(viPassed, "Creator_Trailer")
					
						IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
							
							DECOR_SET_INT(viPassed, "Creator_Trailer", NETWORK_HASH_FROM_PLAYER_HANDLE(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] setting ID decorator on created Avenger to Creator_Trailer")
						ELSE
							
							SCRIPT_ASSERT("[RCC MISSION][SET_UP_FMMC_VEH_RC] - player's boss is invalid! Make sure you are in an MC.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_MODEL(viPassed, CARGOBOB4)
		SET_FORCE_HD_VEHICLE(viPassed, TRUE)
	ENDIF
	
	BOOL bInstantRLDoorOpen = !IS_VEHICLE_MODEL(viPassed, CARGOBOB4)	
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleDoorSettingsOnRule = -1 	// [ML] Only set up the vehicle doors like this from spawn if there is not a rule set where these settings should be applied
		PRINTLN("[ML][SET_UP_FMMC_VEH_RC] Setting up initial door settings (open) to what is set in creator")
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_FRONT_LEFT))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_FRONT_LEFT,FALSE,TRUE)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 1.0)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC  (visual)- opening FL DOOR for vehicle: ",iveh) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_FRONT_RIGHT))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_FRONT_RIGHT,FALSE,TRUE)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 1.0)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC  (visual)- opening FR DOOR for vehicle: ",iveh) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_REAR_LEFT))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_REAR_LEFT,FALSE,bInstantRLDoorOpen)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 1.0)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC  (visual)- opening RL DOOR for vehicle: ",iveh) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_REAR_RIGHT))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_REAR_RIGHT,FALSE,TRUE)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_REAR_RIGHT, DT_DOOR_INTACT, 1.0)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC  (visual)- opening RR DOOR for vehicle: ",iveh) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_BONNET))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_BONNET,FALSE,TRUE)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_BONNET, DT_DOOR_INTACT, 1.0)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC  (visual)- opening BONNET for vehicle: ",iveh) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_BOOT))
			SET_VEHICLE_DOOR_OPEN(viPassed,SC_DOOR_BOOT,FALSE,TRUE)
			SET_VEHICLE_DOOR_CONTROL(viPassed, SC_DOOR_BOOT, DT_DOOR_INTACT, 1.0)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC (visual) - opening BOOT for vehicle: ",iveh) 
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, ciFMMC_VEHICLE_HEADLIGHTS_ON)
		SET_VEHICLE_LIGHTS(viPassed, SET_VEHICLE_LIGHTS_ON)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - turning on lights for vehicle: ",iveh)
	ENDIF
	
	// Set the Vehicle Window Tints. creator should guard against vehicles that can't have tints: GET_NUM_VEHICLE_WINDOW_TINTS()
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleWindowTintColourIndex > -1
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Setting tinted windows for vehicle: ", iveh, " iVehicleWindowTintColourIndex: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleWindowTintColourIndex)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleWindowTintColourIndex = 0
			SET_VEHICLE_WINDOW_TINT(viPassed, 0)
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleWindowTintColourIndex = 1
			SET_VEHICLE_WINDOW_TINT(viPassed, 3)
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleWindowTintColourIndex = 2
			SET_VEHICLE_WINDOW_TINT(viPassed, 2)
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleWindowTintColourIndex = 3
			SET_VEHICLE_WINDOW_TINT(viPassed, 1)
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven,  ciFMMC_VEHICLE7_DELOREAN_SEATING_PREF_OVERRIDE)		
		IF g_FMMC_STRUCT.iNumberOfTeams = 3
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iTeamSeatPreference[2] = ciFMMC_SeatPreference_Driver
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Setting vehicle ", iveh," Overriding iTeamSeatPreference to be ciFMMC_SeatPreference_Driver for Team: 2")
		ENDIF
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Heist_Veh_ID",DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(viPassed,"Heist_Veh_ID")
			DECOR_SET_INT(viPassed,"Heist_Veh_ID",i)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] setting ID decorator on created vehicle ", iVeh)
		ENDIF
	ENDIF
	
	IF DOES_USE_OLD_STYLE_CAMO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
		FMMC_SET_THIS_VEHICLE_COLOURS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColour, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery,  g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColourCombination, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColour2, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColour3, 0.5, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex)	
	ELSE					
		FMMC_SET_THIS_VEHICLE_COLOURS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColour, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery,  g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColourCombination, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColour2, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColour3, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fEnvEff, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex)
	ENDIF
	
	SET_VEHICLE_HAS_STRONG_AXLES(viPassed, TRUE)
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_VEHICLE_NOT_CONSIDERED_BY_PLAYERS)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viPassed, FALSE)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC]")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_VEHICLE_NOT_CONSIDERED_BY_FLEEING_PEDS)
		SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(viPassed, FALSE)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Setting vehicle ", iveh," to not be usable by fleeing peds")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_BULLETPROOF_TYRES)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Setting vehicle ", iveh," Tyres cannot burst.")
		SET_VEHICLE_TYRES_CAN_BURST(viPassed, FALSE)
	ENDIF
	
	//3943017
	CHECK_IF_VEHICLE_SHOULD_BE_RETAINED_IN_INTERIOR(viPassed, vSpawnLoc, iSpawnInterior #IF IS_DEBUG_BUILD , iveh #ENDIF )
	
	IF IS_THIS_MODEL_A_BOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = SUBMERSIBLE
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = SUBMERSIBLE2
		//SET_ENTITY_LOAD_COLLISION_FLAG(viPassed,TRUE)
		IF NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = SUBMERSIBLE)
		AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = SUBMERSIBLE2)
			//Asserts if not called on a boat
			SET_FORCE_LOW_LOD_ANCHOR_MODE(viPassed,FALSE)
		ENDIF
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_DONT_PLACE_ON_GROUND)
			SET_VEHICLE_ON_GROUND_PROPERLY(viPassed)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] calling SET_VEHICLE_ON_GROUND_PROPERLY for boat veh: ", iveh)
		ENDIF
		IF CAN_ANCHOR_BOAT_HERE (viPassed )
		AND GET_ENTITY_MODEL(viPassed) != STROMBERG
			SET_BOAT_ANCHOR(viPassed,TRUE)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] calling SET_BOAT_ANCHOR for boat veh: ", iveh)
		ENDIF
		IF NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = SUBMERSIBLE)
		AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = SUBMERSIBLE2)
			SET_BOAT_LOW_LOD_ANCHOR_DISTANCE(viPassed,99999.0)
		ENDIF
		IF (GET_ENTITY_MODEL(viPassed) = SEASHARK) OR (GET_ENTITY_MODEL(viPassed) = SEASHARK2)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - Jetski has been created, setting engine off")
			SET_VEHICLE_ENGINE_ON(viPassed, FALSE, TRUE)
		ENDIF
	ELSE		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
		AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TITAN AND IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash))
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(viPassed, FALSE)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - Setting SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION false for veh: ", iVeh)
		ELSE
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(viPassed, TRUE)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - Setting SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION true for veh: ", iVeh)
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_FREEZE_POS)
	AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TITAN AND IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash))
		SET_HELI_BLADES_FULL_SPEED(viPassed)
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)	
	// Fix for url:bugstar:4384190          ||			 Seems like a setting was turned on by mistake here...
	AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TITAN AND IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash))
		CONTROL_LANDING_GEAR(viPassed, LGC_RETRACT_INSTANT)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - Setting landing gear to LGC_RETRACT_INSTANT for plane, ", iVeh, " because of ciFMMC_VEHICLE3_VehicleStartsAirborne")
	ENDIF
	
	// Fix for url:bugstar:4384190          ||			 Seems like a setting was turned on by mistake here...
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TITAN 
	AND IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - Setting landing gear to LGC_DEPLOY_INSTANT for plane: ", iVeh)
		CONTROL_LANDING_GEAR(viPassed, LGC_DEPLOY_INSTANT)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(viPassed, TRUE)
	ENDIF	
	
	SET_DISABLE_VEHICLE_PETROL_TANK_DAMAGE(viPassed,FALSE)
	//SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(viPassed, TRUE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_BREAKING)
		SET_VEHICLE_CAN_BREAK(viPassed, FALSE)
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viPassed))
			SET_VEHICLE_BROKEN_PARTS_DONT_AFFECT_AI_HANDLING(viPassed, TRUE)
		ENDIF
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Setting vehicle can break to false")
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_HELI_EXPLODE_FROM_BODY_DAMAGE)
		SET_DISABLE_HELI_EXPLODE_FROM_BODY_DAMAGE(viPassed, TRUE)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Setting heli disable explode from body damage")
	ENDIF
	
	// url:bugstar:2199931 - set heli searchlight
	IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(viPassed))
	AND DOES_VEHICLE_HAVE_WEAPONS(viPassed)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] [ST_DEBUG] Model is a heli")
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_UseSearchLightInDark)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] [ST_DEBUG] Bit is set")
			IF SHOULD_VEHICLE_HAVE_SEARCHLIGHT_ON()
				PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] [ST_DEBUG] Light should be on")
				IF NOT IS_VEHICLE_SEARCHLIGHT_ON(viPassed)
					PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] [ST_DEBUG] SET_UP_FMMC_VEH_RC - SET_VEHICLE_SEARCHLIGHT(TRUE)")
					SET_VEHICLE_SEARCHLIGHT(viPassed, TRUE, TRUE)
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] [ST_DEBUG] Light should not be on")
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] [ST_DEBUG] Bit is not set")
		#ENDIF
		ENDIF
	ENDIF
	

	iHealth = GET_FMMC_VEHICLE_MAX_HEALTH(iveh, iNumPlayers)
	
	SET_ENTITY_HEALTH(viPassed, iHealth)
	SET_ENTITY_MAX_HEALTH(viPassed, iHealth)
	IF IS_THIS_A_CAPTURE()//url:bugstar:4489638
	OR IS_THIS_A_LTS()
		FLOAT fHealth = TO_FLOAT(iHealth)
		SET_VEHICLE_ENGINE_HEALTH(viPassed,fHealth)
		SET_VEHICLE_PETROL_TANK_HEALTH(viPassed,fHealth)
		SET_VEHICLE_BODY_HEALTH(viPassed,fHealth)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] [CAPTURE][LTS]- vehicle ",iveh," engine/petrol/body health changed to ", fHealth)
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_RIGHT_SELF_WHEN_OUT_OF_SIGHT)
		ADD_VEHICLE_STUCK_CHECK_WITH_WARP(viPassed, 1.0, 1000, TRUE, TRUE, TRUE, 1)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - Calling ADD_VEHICLE_STUCK_CHECK_WITH_WARP on Vehicle: ",iveh)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, ciFMMC_VEHICLE_STOLEN)
		SET_VEHICLE_IS_STOLEN(viPassed, TRUE)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - vehicle: ",iveh," is stolen: true ")
	ELSE
		SET_VEHICLE_IS_STOLEN(viPassed, FALSE)
		SET_VEHICLE_INFLUENCES_WANTED_LEVEL(viPassed,FALSE)
		SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(viPassed,TRUE)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - vehicle: ",iveh," is stolen: false ")
	ENDIF
	
	SET_VEHICLE_MAY_BE_USED_BY_GOTO_POINT_ANY_MEANS(viPassed, TRUE)
	SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(viPassed, TRUE)
	
	PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - vehicle: ",iveh," ciFMMC_VEHICLE6_EXPLODE_ON_CONTACT: ",IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_EXPLODE_ON_CONTACT))
	SET_ALLOW_VEHICLE_EXPLODES_ON_CONTACT(viPassed,IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_EXPLODE_ON_CONTACT))
	
	SET_VEHICLE_ENGINE_CAN_DEGRADE(viPassed,FALSE)
	
	if ihealth >= 4000
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(viPassed,FALSE)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - vehicle: ",iveh," SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE: false ")
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TANKER)
		OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TANKER2)
		OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn =ARMYTANKER)
			SET_VEHICLE_BODY_HEALTH(viPassed,1000)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - vehicle: ",iveh," TANKER, set_vehicle_body_health as 1000 to stop it exploding so easily")
		ENDIF
	ENDIF
	
	
	PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - vehicle: ",iveh," health is: ",ihealth, " model is: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
	
	IF GET_ENTITY_MODEL(viPassed) = LUXOR2
		PRINTLN("[VEHICLE CREATION][SET_UP_FMMC_VEH_RC] - Setting decor for LUXOR2 - 'DECOR_SET_INT(viPassed, 'EnableVehLuxeActs', 1)'")
		DECOR_SET_INT(viPassed, "EnableVehLuxeActs", 1)
	ENDIF

	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_OPEN_PERSONAL_VEHICLE_DOORS )
		PRINTLN("[JJT][SET_UP_FMMC_VEH_RC] Setting vehicle as peacock:", iveh)
		IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
			INT iMPDecorator
			
			IF DECOR_EXIST_ON( viPassed,"MPBitset" ) 
				iMPDecorator = DECOR_GET_INT(viPassed,"MPBitset" )
			ENDIF
			
			SET_BIT( iMPDecorator,MP_DECORATOR_BS_PIM_DOOR_CONTROL )
			DECOR_SET_INT( viPassed, "MPBitset", iMPDecorator )
		ENDIF
	ENDIF
	
	IF NOT IS_THIS_A_CAPTURE()//url:bugstar:4489638
	AND NOT IS_THIS_A_LTS()
		//1788588
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fEngineHealth <> 1000	
		AND NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR )		
			SET_VEHICLE_ENGINE_HEALTH(viPassed,GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iveh, iNumPlayers))
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - vehicle ",iveh," engine health changed to ",GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iveh, iNumPlayers))
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fPetrolHealth <> 1000
		AND NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR )		
			SET_VEHICLE_PETROL_TANK_HEALTH(viPassed,GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(iveh, iNumPlayers))
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - vehicle ",iveh," petrol health changed to ",GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(iveh, iNumPlayers))
		ENDIF
		
		//1948846
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fBodyHealth <> 1000
			SET_VEHICLE_BODY_HEALTH(viPassed,GET_FMMC_VEHICLE_MAX_BODY_HEALTH(iveh, iNumPlayers))
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - vehicle ",iveh," body health changed to ",GET_FMMC_VEHICLE_MAX_BODY_HEALTH(iveh, iNumPlayers))
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR )
		IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(viPassed))
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHeliMainRotorhealth <> 1000
				SET_HELI_MAIN_ROTOR_HEALTH(viPassed,GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iveh, iNumPlayers))
				PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - heli ",iveh," main rotor health changed to ",GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iveh, iNumPlayers))
			ENDIF
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHeliTailRotorhealth <> 1000
				SET_HELI_TAIL_ROTOR_HEALTH(viPassed,GET_FMMC_HELI_MAX_TAIL_ROTOR_HEALTH(iveh, iNumPlayers))
				PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - heli ",iveh," tail rotor health changed to ",GET_FMMC_HELI_MAX_TAIL_ROTOR_HEALTH(iveh, iNumPlayers))
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_HELI_ROTOR_BOOM_UNBREAKABLE)
				SET_HELI_TAIL_BOOM_CAN_BREAK_OFF(viPassed, FALSE)
				PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - heli ",iveh," tail boom set to unbreakable")
			ENDIF
		ELIF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viPassed))
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHeliMainRotorhealth <> 1000
				SET_PLANE_PROPELLER_HEALTH(viPassed,GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iveh, iNumPlayers))
				PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - plane ",iveh," propeller health changed to ",GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iveh, iNumPlayers))
			ENDIF
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fEngineHealth <> 1000
				SET_PLANE_ENGINE_HEALTH(viPassed,GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iveh, iNumPlayers))
				PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - plane ",iveh," engine health changed to ",GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iveh, iNumPlayers))
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,ciFMMC_VEHICLE_EASYEXPLODER)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle ",iveh," ciFMMC_VEHICLE_EASYEXPLODER ")
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fEngineHealth = 1001
			SET_VEHICLE_ENGINE_HEALTH(viPassed,400)
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fPetrolHealth = 1001
			SET_VEHICLE_PETROL_TANK_HEALTH(viPassed,400)
		ENDIF
		SET_FORCE_VEHICLE_ENGINE_DAMAGE_BY_BULLET(viPassed,TRUE)
	ENDIF
	
	PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Passing Vehicle petrol bit in car's creation")
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour,ciFMMC_VEHICLE4_PETROL_CANNOT_BE_DAMAGED)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle ",iveh," ciFMMC_VEHICLE4_PETROL_CANNOT_BE_DAMAGED")
		SET_DISABLE_VEHICLE_PETROL_TANK_DAMAGE(viPassed,TRUE)
		SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(viPassed,TRUE)
	ENDIF		
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_PreventEngineFromMisfiring)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle ",iveh," ciFMMC_VEHICLE3_PreventEngineFromMisfiring is set, turn off engine misfiring")
		SET_VEHICLE_CAN_ENGINE_MISSFIRE(viPassed, FALSE)
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fDamageScalarWithMap <> 1.0
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle ", iVeh, " Calling to scale the incoming damage, setting map to: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fDamageScalarWithMap)
		SET_VEHICLE_DAMAGE_SCALES(viPassed, 1.0, 1.0, 1.0, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fDamageScalarWithMap)		
	ENDIF
		
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR )
		SET_VEHICLE_ENGINE_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) vehicle ",iveh," engine health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS")
		
		SET_VEHICLE_PETROL_TANK_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) vehicle ",iveh," petrol health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS")
		
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viPassed))
			SET_PLANE_PROPELLER_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) plane ",iveh," propeller health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS ")
		
			SET_PLANE_ENGINE_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) plane ",iveh," engine health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS")
			
			IF GET_ENTITY_MODEL(viPassed) = AVENGER
				//Block Wing Damage
				PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) Blocking Wing Damage For Avenger")
				SET_PLANE_SECTION_DAMAGE_SCALE(viPassed, WING_L, 0.0)
				SET_PLANE_SECTION_DAMAGE_SCALE(viPassed, WING_R, 0.0)
				SET_PLANE_SECTION_DAMAGE_SCALE(viPassed, ENGINE_L, 0.0)
				SET_PLANE_SECTION_DAMAGE_SCALE(viPassed, ENGINE_R, 0.0)
			ENDIF
		ENDIF
		
		IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(viPassed))
			SET_HELI_MAIN_ROTOR_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) heli ",iveh," main rotor health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS")
		
			SET_HELI_TAIL_ROTOR_HEALTH(viPassed, ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) heli ",iveh," tail rotor health changed to ciFMMC_VEHICLE_ARCADE_HEALTH_FOR_COMPONENTS")	
		
			SET_HELI_TAIL_BOOM_CAN_BREAK_OFF(viPassed, FALSE)
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) heli ",iveh," tail boom set to unbreakable")
		ENDIF
		
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - (forced - ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR) vehicle ",iveh," ciFMMC_VEHICLE4_PETROL_CANNOT_BE_DAMAGED")
		SET_DISABLE_VEHICLE_PETROL_TANK_DAMAGE(viPassed,TRUE)
		SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(viPassed,TRUE)
	ENDIF	
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = DELUXO
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Vehicle ", iveh," turning on flight mode for deluxo")
		SET_SPECIAL_FLIGHT_MODE_ALLOWED(viPassed, TRUE)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_DELOREAN_START_HOVERING)
			SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(viPassed, 1)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Vehicle ", iveh," created with wheels in special flight position")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, ciFMMC_VEHICLE_SIREN_LIGHT)
		SET_VEHICLE_SIREN(viPassed, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - turning on lights for vehicle: ",iveh) 
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, ciFMMC_VEHICLE_SIREN_SOUND)
			SET_VEHICLE_HAS_MUTED_SIRENS(viPassed, TRUE)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - turning on sounds for vehicle: ",iveh) 
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, ciFMMC_VEHICLE_SIREN_SOUND)
			SET_SIREN_WITH_NO_DRIVER(viPassed, TRUE)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - turning on sounds for vehicle: ",iveh) 
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, ciFMMC_VEHICLE_DOORS_LOCKED)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_LOCKED_TO_PLAYERS_ONLY)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viPassed, TRUE)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viPassed,TRUE)
			SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(viPassed,TRUE)
			NET_PRINT("[RCC MISSION] SET_UP_FMMC_VEH_RC - locking the doors for vehicle (players only): ") NET_PRINT_INT(iveh) NET_PRINT(" model = ") NET_PRINT( GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed))) NET_NL()
		ELSE
			SET_VEHICLE_DOORS_LOCKED(viPassed, VEHICLELOCK_LOCKED)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viPassed, TRUE)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viPassed,TRUE)
			SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(viPassed,TRUE)
			NET_PRINT("[RCC MISSION] SET_UP_FMMC_VEH_RC - locking the doors for vehicle: ") NET_PRINT_INT(iveh) NET_PRINT(" model = ") NET_PRINT( GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed))) NET_NL()
		ENDIF
	ELSE
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleDoorSettingsOnRule = -1 	// [ML] Only set up the vehicle doors like this from spawn if there is not a rule set where these settings should be applied
			PRINTLN("[ML][SET_UP_FMMC_VEH_RC] Setting up initial door settings (locked/unlocked) to what is set in creator")
			FOR i = SC_DOOR_FRONT_LEFT TO ENUM_TO_INT(SC_DOOR_BOOT)
				IF DOES_VEHICLE_HAVE_DOOR(viPassed, INT_TO_ENUM(SC_DOOR_LIST, i))
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_DOOR_FRONT_LEFT_LOCKED + i) 
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_LOCKED_TO_PLAYERS_ONLY)
						SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viPassed, i, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
						PRINTLN("[RCC MISSION][JJT] SET_UP_FMMC_VEH_RC - locking door (players only) ", i ," for vehicle: ",iveh," Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viPassed,TRUE)
					ELSE
						PRINTLN("[RCC MISSION][JJT] SET_UP_FMMC_VEH_RC - locking door ", i ," for vehicle: ",iveh," Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(viPassed)))
						SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viPassed, i, VEHICLELOCK_LOCKED)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		HANDLE_VEH_TEAM_RESTRICTION(iveh, viPassed)
	ENDIF
	
	IF IS_VEHICLE_MISSION_KILL(iveh)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON for vehicle: ",iveh) 
		SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(viPassed,TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.iVehicleTireBitSet ,iveh)
		SET_VEHICLE_TYRES_CAN_BURST(viPassed,FALSE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Tyres are bullet proof for vehicle: ",iveh) 
	ENDIF
	
	IF Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - SET_VEHICLE_EXTENDED_REMOVAL_RANGE for vehicle: ",iveh) 
		SET_VEHICLE_EXTENDED_REMOVAL_RANGE(viPassed,150)
	ENDIF
	
	SET_VEHICLE_AI_CAN_USE_EXCLUSIVE_SEATS(viPassed, TRUE)
	
	IF ( (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = CARGOBOB) OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = CARGOBOB2) OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = CARGOBOB3) )
		BOOL bHookDropOff
		IF IS_VEHICLE_A_DROP_OFF_VEHICLE(iVeh,bHookDropOff)
			IF bHookDropOff
				//Create magnet to attach to:
				CREATE_PICK_UP_ROPE_FOR_CARGOBOB(viPassed, PICKUP_MAGNET)
				SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(viPassed, 4, 6)
				
				PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Cargobob vehicle ",iveh," needs to be used as a drop off w magnet, creating magnet...")
				
				SET_CARGOBOB_PICKUP_MAGNET_SET_AMBIENT_MODE(viPassed, TRUE, TRUE)
				SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(viPassed, FALSE)
				SET_CARGOBOB_PICKUP_MAGNET_REDUCED_STRENGTH(viPassed, 0)
				SET_CARGOBOB_PICKUP_MAGNET_PULL_STRENGTH(viPassed, 0) // Default 2.0
				
				SET_CARGOBOB_PICKUP_MAGNET_STRENGTH(viPassed, 0.65) // Default 1.0
				SET_CARGOBOB_PICKUP_MAGNET_FALLOFF(viPassed, -0.5) // Default -0.5
				SET_CARGOBOB_PICKUP_MAGNET_REDUCED_FALLOFF(viPassed, -0.4)
				SET_CARGOBOB_PICKUP_MAGNET_PULL_ROPE_LENGTH(viPassed, 1.5) // Default 3.0
				
				SET_CARGOBOB_PICKUP_ROPE_DAMPING_MULTIPLIER(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fCargobobDamp) //default 1.0
				
				SET_CARGOBOB_PICKUP_MAGNET_ENSURE_PICKUP_ENTITY_UPRIGHT(viPassed, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_ENTITY_BONE_INDEX_BY_NAME(viPassed, "attach_female") <> -1
		SET_VEHICLE_AUTOMATICALLY_ATTACHES(viPassed, TRUE, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - setting attachment for veh: ",iveh) 
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh",DECOR_TYPE_INT)
		DECOR_SET_INT(viPassed,"Not_Allow_As_Saved_Veh",1)
	ENDIF
	
	IF NOT IS_THIS_A_HEIST_CREATION()
		SET_ENTITY_VISIBLE(viPassed,bVisible)
	ELSE
		SET_ENTITY_VISIBLE(viPassed,TRUE)
	ENDIF
	
	//Vehicle window removal
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_FRONT_LEFT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_FRONT_LEFT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_FRONT_RIGHT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_FRONT_RIGHT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_REAR_LEFT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_REAR_LEFT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_REAR_RIGHT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_REAR_RIGHT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_MIDDLE_LEFT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_MIDDLE_LEFT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDOW_MIDDLE_RIGHT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDOW_MIDDLE_RIGHT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDSCREEN_FRONT)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDSCREEN_FRONT)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_WINDSCREEN_REAR)
		REMOVE_VEHICLE_WINDOW(viPassed, SC_WINDSCREEN_REAR)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_DYNAMIC_APARTMENT_SPAWN_OVERRIDE)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_UseDynamicArcadeSpawnLoc)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_DONT_PLACE_ON_GROUND)
			SET_VEHICLE_ON_GROUND_PROPERLY(viPassed)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," SET_VEHICLE_ON_GROUND_PROPERLY - ciFMMC_VEHICLE2_DYNAMIC_APARTMENT_SPAWN_OVERRIDE/ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE/ciFMMC_VEHICLE8_UseDynamicArcadeSpawnLoc")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_RESIST_EXPLOSIONS)
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viPassed))
			SET_PLANE_RESIST_TO_EXPLOSION(viPassed,TRUE)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," SET_PLANE_RESIST_TO_EXPLOSION ")
		ELSE
			IF GET_ENTITY_MODEL(viPassed) = INT_TO_ENUM(MODEL_NAMES, HASH("PHANTOM2"))
				SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(viPassed,TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_REQUIRES_HOTWIRE)
		SET_VEHICLE_DOORS_LOCKED(viPassed,VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
		SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(viPassed,TRUE)		
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," Set as requiring Hotwire")
	ELSE
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," ciFMMC_VEHICLE8_REQUIRES_HOTWIRE not set")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_MAKE_INVINCIBLE)
		SET_ENTITY_INVINCIBLE(viPassed,TRUE)
		SET_ENTITY_PROOFS(viPassed,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," SET_ENTITY_INVINCIBLE ")
	ELSE
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," ciFMMC_VEHICLE4_MAKE_INVINCIBLE not set ")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_MAKE_STRONG)
		SET_VEHICLE_STRONG(viPassed, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," SET_VEHICLE_STRONG ")
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_RESIST_EXPLODING_VEHICLES)
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(viPassed,FALSE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," SET_VEHICLE RESIST EXPLODING VEHICLES")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_AUTO_IGNITION_ON)
		SET_VEHICLE_ENGINE_ON(viPassed, TRUE, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," ciFMMC_VEHICLE2_AUTO_IGNITION_ON")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_FREEZE_POS)
		FREEZE_ENTITY_POSITION(viPassed, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," ciFMMC_VEHICLE4_VEHICLE_FREEZE_POS")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_PLACE_CHEVRON)
		FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(viPassed, 
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iChevronColours[0],
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iChevronColours[1],
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iChevronColours[2])
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," ciFMMC_VEHICLE4_VEHICLE_PLACE_CHEVRON")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RESIST_RAMP_CARS)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," ciFMMC_VEHICLE4_VEHICLE_RESIST_RAMP_CARS")
		SET_ALLOW_RAMMING_SOOP_OR_RAMP(viPassed, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_IMMUNE_HOMING_LAUNCHER)
		SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(viPassed, FALSE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," ciFMMC_VEHICLE4_VEHICLE_IMMUNE_HOMING_LAUNCHER - SET")
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_ALLOW_EMPTY_VEH_HOMING_LOCKON)
		SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(viPassed, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - vehicle: ",iveh," ciFMMC_VEHICLE5_ALLOW_EMPTY_VEH_HOMING_LOCKON - SET")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_IS_CARGOBOB_ATTACHED_TO_VEH)
		IF NOT DOES_CARGOBOB_HAVE_PICKUP_MAGNET(viPassed)
			SET_VEHICLE_ENGINE_ON(viPassed, TRUE, TRUE )
			SET_HELI_BLADES_FULL_SPEED(viPassed)
		
			CREATE_PICK_UP_ROPE_FOR_CARGOBOB(viPassed, PICKUP_MAGNET)
			SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(viPassed, 4, 6, TRUE)
		ENDIF
	ENDIF
	
	FMMC_SET_VEH_MOD_PRESET(viPassed,
							g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn,
							g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset,
							g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iNeonColour)
							
	SET_VEHICLE_ALL_CUSTOM_MODS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("dune4"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("dune5"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("phantom2"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("boxville5"))
		PRINTLN("[VEHICLE CREATION][SET_UP_FMMC_VEH_RC] - Setting VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE FALSE ... iVeh = ", iVeh, " ... Model = ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn))
		VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE(viPassed, FALSE)
	ENDIF
	
//	IF bFreeze
//		SET_ENTITY_COLLISION(viPassed,FALSE)
//		FREEZE_ENTITY_POSITION(viPassed, TRUE)
//	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
	AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = TITAN AND IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash))
	
		BOOL bShouldPlaceOnGround = TRUE
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_DONT_PLACE_ON_GROUND)
			bShouldPlaceOnGround = FALSE
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Not calling SET_VEHICLE_ON_GROUND_PROPERLY due to ciFMMC_VEHICLE5_DONT_PLACE_ON_GROUND | iVeh: ", iveh)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehInterior = 275201 //NATIVE_TO_INT of the main Casino Interior
			bShouldPlaceOnGround = FALSE
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Not calling SET_VEHICLE_ON_GROUND_PROPERLY due to Casino interior issues | iVeh: ", iveh)
		ENDIF
		
		IF bShouldPlaceOnGround
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] calling SET_VEHICLE_ON_GROUND_PROPERLY for veh: ", iveh)
			VECTOR tempVec
			tempVec = GET_ENTITY_COORDS(viPassed)
			PRINTLN("[JT][SET_UP_FMMC_VEH_RC] - pre on ground properly position: ", tempVec)
			SET_VEHICLE_ON_GROUND_PROPERLY(viPassed)
			tempVec = GET_ENTITY_COORDS(viPassed)
			PRINTLN("[JT][SET_UP_FMMC_VEH_RC] - post on ground properly position: ", tempVec)
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Not calling SET_VEHICLE_ON_GROUND_PROPERLY as ciFMMC_VEHICLE3_VehicleStartsAirborne | iVeh: ", iveh)
	ENDIF
	
	SET_CREATOR_ID_INT_DECOR_ON_ENTITY(GET_ENTITY_FROM_PED_OR_VEHICLE(viPassed), iVeh)
	
	IF IS_THIS_ROCKSTAR_MISSION_SVM_DUNE4(g_FMMC_STRUCT.iRootContentIDHash)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("dune4"))
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Enabling ramp car side impulse")
			VEHICLE_SET_ENABLE_RAMP_CAR_SIDE_IMPULSE(viPassed, TRUE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  coFMMC_VEHICLE5_ENABLE_NORMALISED_RAMPS)
		VEHICLE_SET_ENABLE_NORMALISE_RAMP_CAR_VERTICAL_VELOCTIY(viPassed, TRUE)
		PRINTLN("RCC MISSION] SET_UP_FMMC_VEH_RC - Normalising ramp car vertical velocity for vehicle: ", iveh)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iDuneFlipScale != 10
	PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Calling SET_SCRIPT_RAMP_IMPULSE_SCALE with creator set value of: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iDuneFlipScale)
		FLOAT fScale
		fScale = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iDuneFlipScale) / 10
		SET_SCRIPT_RAMP_IMPULSE_SCALE(viPassed,fScale)
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS > 0
		SET_ALLOW_MIGRATE_TO_SPECTATOR(viPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciDISABLE_VEHICLE_KERS)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Disabling KERS on vehicle: ", iveh)
		BLOCK_VEHICLE_KERS(viPassed, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTwo, ciFMMC_VEHICLE2_USE_CUSTOM_EXTRAS)
		SET_VEHICLE_EXTRAS_FROM_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleExtrasBitset, viPassed)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iHealth > 4999
		SET_VEHICLE_STRONG(viPassed,TRUE)
		PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] Setting vehicle strong")
	ENDIF
	
	SET_UNBREAKABLE_DOORS_FOR_VEHICLE(viPassed,iVeh)
	
	// If we have an attached objectcheers
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParentType = CREATION_TYPE_VEHICLES

		IF IS_NET_VEHICLE_DRIVEABLE(niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent])
			IF GET_ENTITY_MODEL(viPassed) = CARGOBOB2
				FMMC_ATTACH_CARGOBOB_TO_VEHICLE(viPassed, NET_TO_VEH(niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent]),TRUE)
			ELSE
				FMMC_ATTACH_VEHICLE_TO_VEHICLE(viPassed,NET_TO_VEH(niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent]),TRUE)
			ENDIF
			PRINTLN("[RCC MISSION][SET_UP_FMMC_VEH_RC] attaching vehicle: ",iVeh, "to trailer: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent)
		ENDIF
	
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_HOMING_ROCKET_PRIORITY_TARGET)
		SET_ENTITY_IS_TARGET_PRIORITY(viPassed, TRUE)
		PRINTLN("[RCC MISSION] Setting SET_ENTITY_IS_TARGET_PRIORITY on placed vehicle: ", iVeh)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_CANT_BE_DAMAGED_BY_PLAYERS)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - setting cant be damaged by players for veh: ", iVeh)
		SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(viPassed, FALSE, rgfm_PlayerTeam[0])
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_DONT_TERMINATE_TASK_WHEN_ACHIEVED)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - setting dont terminate task when achieved for veh: ", iVeh)
		SET_VEHICLE_DONT_TERMINATE_TASK_WHEN_ACHIEVED(viPassed)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_RESIST_OWNED_EXPLOSION_DAMAGE)
		SET_VEHICLE_NO_EXPLOSION_DAMAGE_FROM_DRIVER(viPassed, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - setting cant be damaged owned explosions for veh: ", iVeh)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_SET_VEHICLE_AS_BULLET_PROOF)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_MAKE_INVINCIBLE) //To keep full invincibility
		SET_ENTITY_PROOFS(viPassed, TRUE, FALSE, FALSE, FALSE, FALSE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Setting Bullet proof on vehicle: ", iVeh)
	ENDIF
	
	IF GET_ENTITY_MODEL(viPassed) = AVENGER
	AND GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_MAKE_INVINCIBLE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Setting fire proof on Avenger: ", iVeh)
		SET_ENTITY_PROOFS(viPassed, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_SET_VEHICLE_AS_BULLET_PROOF), TRUE, FALSE, FALSE, FALSE)
	ENDIF
	
	IF DOES_VEHICLE_HAVE_ANY_WEAPON_MODS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
		SET_VEHICLE_WEAPON_MODS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVModTurret, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVModArmour, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, iveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVModAirCounter, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVModExhaust, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVModBombBay, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVModSpoiler)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciALLOW_BOMBUSHKA_TURRETS_IN_MISSION)
	AND IS_VEHICLE_MODEL(viPassed, BOMBUSHKA)
		SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS(viPassed, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Setting SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS on vehicle ", iVeh)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_SPACE_ROCKETS)
		DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, viPassed, NULL)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Disabling space rockets on vehicle ", iVeh)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_IN_VTOL_MODE)
	AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viPassed))
		SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(viPassed, 1.0)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Setting vehicle to start in Flight Mode Vertical ", iVeh)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_ALLOW_OBJECT_TARGETING)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Setting vehicle able to target objects ", iVeh)
		SET_VEHICLE_WEAPON_CAN_TARGET_OBJECTS(viPassed, TRUE)
	ENDIF
	
	IF IS_MISSION_VEHICLE_TO_BE_USED_AS_A_RESPAWN_VEHICLE_BY_ANYBODY(iVeh)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Setting vehicle as a specific respawn vehicle ", iVeh)
		SET_VEHICLE_AS_A_SPECIFIC_RESPAWN_VEHICLE(viPassed)
	ENDIF
	
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		IF IS_VEHICLE_MODEL(viPassed, DELUXO)
			HANDLE_SETTING_GANGOPS_DELUXO_COLOUR(iVeh, viPassed)
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex = 0
		BURST_VEHICLE_TYRES(viPassed)
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex > 0
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex = 4
			BURST_VEHICLE_TYRES(viPassed)
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex = 3
			BURST_VEHICLE_TYRES(viPassed)
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex = 2
			BURST_VEHICLE_TYRES(viPassed)
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurstTyresRespawnIndex = 1
			BURST_VEHICLE_TYRES(viPassed)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_HIGH_SPEED_EDGE_FALL_DETECTION)
		SET_NETWORK_ENABLE_HIGH_SPEED_EDGE_FALL_DETECTION(viPassed,TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Enabling vehicle HIGH SPEED EDGE FALL DETECTION in", iVeh)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_PlayerVehiclesExplosionResistant)
		SET_DISABLE_VEHICLE_EXPLOSIONS_DAMAGE(TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Calling SET_DISABLE_VEHICLE_EXPLOSIONS_DAMAGE - SET_UP_FMMC_VEH_RC")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Monster3IncreasedCrushDamage)
		IF IS_VEHICLE_A_MONSTER(GET_ENTITY_MODEL(viPassed))
			SET_INCREASE_WHEEL_CRUSH_DAMAGE(viPassed, TRUE)
		ENDIF
	ENDIF
	
	// Retroactive Fix, in the future content need to set up the vehicles to be stationary until the peds have attached.
	// WE will need to provide something that links the vehicles and peds, so we know to freeze the vehicle and not to unfreeze it until the linked entities are attached.
	// Right now we'd have to loop through all peds here to determine whether we need to freeze.
	IF IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash)
		IF iVeh = 18
		OR iVeh = 16
		OR iVeh = 4
		OR iVeh = 3
		OR iVeh = 2
			PRINTLN("[RCC MISSION][url:bugstar:4384208]  SET_UP_FMMC_VEH_RC - Calling FREEZE_ENTITY_POSITION Special fix for creator mistake: vehicle ", iVeh)
			FREEZE_ENTITY_POSITION(viPassed, TRUE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_CanOnlyBeDamagedByPlayers)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - setting Can Only be damaged by players for veh: ", iVeh)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(viPassed, TRUE)
	ENDIF
		
	// If any of these are set, then we want to trigger them all.
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iEntityRelGroupDamageRestrictions <> 0
		INT iEntDmgRestriction = 0
		FOR iEntDmgRestriction = 0 TO OPTION_ENTITY_CAN_ONLY_BE_DAMAGE_BY_RELGR_MAX						
			PRINTLN("[LM][RCC MISSION] SET_UP_FMMC_VEH_RC - veh: ", iVeh, " Relationship Group Number: ", iEntDmgRestriction, " Set: ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iEntityRelGroupDamageRestrictions, iEntDmgRestriction))
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(viPassed, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iEntityRelGroupDamageRestrictions, iEntDmgRestriction), GET_REL_GROUP_HASH_FROM_INT(iEntDmgRestriction))
		ENDFOR
	ENDIF
	
	VECTOR vCurrentCoord = GET_ENTITY_COORDS(viPassed)
	IF IS_COLLISION_MARKED_OUTSIDE(vCurrentCoord)
	OR GET_INTERIOR_AT_COORDS(vCurrentCoord) = NULL // (false positive) sometimes.
	OR GET_INTERIOR_FROM_COLLISION(vCurrentCoord) = NULL
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - There is no Interior at the location we are spawning iVeh: ", iVeh, " Calling CLEAR_ROOM_FOR_ENTITY (vCurrentCoord: ", vCurrentCoord, ")")
		CLEAR_ROOM_FOR_ENTITY(viPassed)	
	ELSE
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - There is an Interior at the location we are spawning iVeh: ", iVeh, " (vCurrentCoord: ", vCurrentCoord, ")")
	ENDIF
	
	//url:bugstar:6084136 - Apply player's chosen modifications to getaway vehicle when spawned on heist mission.
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehGetawayVehicleSlot > -1
		IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
			INT iSlot = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehGetawayVehicleSlot
			INT iHeistLeader = NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			
			IF iHeistLeader > -1
				APPLY_MODS_TO_HEIST_VEHICLE(viPassed, iSlot, GlobalplayerBD_CasinoHeist[iHeistLeader].GetAwayVehData.iVehUpgradeLevel[iSlot], 
				GlobalplayerBD_CasinoHeist[iHeistLeader].GetAwayVehData.iVehColour[iSlot],
				GlobalplayerBD_CasinoHeist[iHeistLeader].GetAwayVehData.iVehRemoveLivery[iSlot])
				PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - Calling APPLY_MODS_TO_HEIST_VEHICLE for iVeh: ", iVeh, " with SlotID: ", iSlot)
			ELSE
				PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - g_sCasinoHeistData.piLeader is invalid for APPLY_MODS_TO_HEIST_VEHICLE")
				SCRIPT_ASSERT("SET_UP_FMMC_VEH_RC - g_sCasinoHeistData.piLeader is invalid for APPLY_MODS_TO_HEIST_VEHICLE")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE is FALSE but iVehGetawayVehicleSlot > -1")
			SCRIPT_ASSERT("SET_UP_FMMC_VEH_RC - CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE is FALSE but iVehGetawayVehicleSlot > -1")
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] SET_UP_FMMC_VEH_RC - iVehGetawayVehicleSlot = -1")
	ENDIF
	
	SET_VEHICLE_ALL_CUSTOM_MODS(viPassed, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Sub-Section Name: Units Controller Entity Helpers
// ##### Description: Helpers for dealing with Units in the controllers
FUNC BOOL CREATE_PLACED_UNIT_VEHICLES_MP(UNIT_NETWORK_ENTITY_STRUCT &sUnitEntities[], INT iUnitNumber, INT &iCreatedNetVehicles)
	INT iUnitType = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_VEHICLES(iUnitType)-1
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sUnitEntities[iUnitNumber].viVehicles[i])
			
			MODEL_NAMES mnVeh = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].sUnitVeh[i].mnVehicleModel
			
			REQUEST_MODEL(mnVeh)
			
			IF HAS_MODEL_LOADED(mnVeh)
				INT iUnitVariation = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iVariation
				
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading, GET_OFFSET_FOR_UNIT_VEHICLE(iUnitType, i, iUnitVariation))
				FLOAT fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading + GET_HEADING_FOR_UNIT_VEHICLE(iUnitType, i, iUnitVariation)
				
				IF CREATE_NET_VEHICLE(sUnitEntities[iUnitNumber].viVehicles[i], mnVeh, vPos, fHeading)
										
					VEHICLE_INDEX viVeh = NET_TO_VEH(sUnitEntities[iUnitNumber].viVehicles[i])
					
					PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Vehicle ", i, "] CREATE_PLACED_UNIT_VEHICLES_MP - Unit pos: ", g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos)
					PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Vehicle ", i, "] CREATE_PLACED_UNIT_VEHICLES_MP - Unit ", iUnitNumber, " Vehicle created at ", vPos, " heading ", fHeading)
					
					IF DOES_ENTITY_EXIST(viVeh)
						SET_VEHICLE_ON_GROUND_PROPERLY(viVeh)
						FMMC_SET_THIS_VEHICLE_COLOURS(viVeh, -1, -1)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].sUnitVeh[i].iVehicleModPreset != -1
							FMMC_SET_VEH_MOD_PRESET(viVeh, GET_ENTITY_MODEL(viVeh), g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].sUnitVeh[i].iVehicleModPreset)
						ENDIF
						SET_UP_UNIT_VEHICLE(iUnitType, viVeh, i, iUnitNumber)
						SET_CREATOR_ID_INT_DECOR_ON_ENTITY(viVeh, iCreatedNetVehicles)
					ENDIF
					
					iCreatedNetVehicles++
					PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Vehicle ", i, "] CREATE_PLACED_UNIT_VEHICLES_MP - Number of created vehicles is now: ", iCreatedNetVehicles)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(mnVeh)
				ENDIF
			ENDIF
			
			RETURN FALSE
		ELSE
			PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Vehicle ", i, "] CREATE_PLACED_UNIT_VEHICLES_MP - exists")
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_PLACED_UNIT_PEDS_MP(UNIT_NETWORK_ENTITY_STRUCT &sUnitEntities[], INT iUnitNumber, INT &iCreatedNetPeds)
	INT iUnitType = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType
	INT iUnitVariation = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iVariation
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_PEDS(iUnitType, iUnitVariation)-1
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sUnitEntities[iUnitNumber].piPeds[i])
			
			REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].mnPedModel)
			
			IF HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].mnPedModel)
				
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading, GET_OFFSET_FOR_UNIT_PED(iUnitType, i, iUnitVariation))
				FLOAT fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading + GET_HEADING_FOR_UNIT_PED(iUnitType, i, iUnitVariation)
				
				IF CREATE_NET_PED(sUnitEntities[iUnitNumber].piPeds[i], PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].mnPedModel, vPos, fHeading)
					
					PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Ped ", i, "] CREATE_PLACED_UNIT_PEDS_MP - Unit pos: ", g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos)
					PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Ped ", i, "] CREATE_PLACED_UNIT_PEDS_MP - Unit ", iUnitNumber, " Ped created at ", vPos, " heading ", fHeading)
					
					iCreatedNetPeds++
					PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Ped ", i, "] CREATE_PLACED_UNIT_PEDS_MP - Number of created peds is now: ", iCreatedNetPeds)
					
					IF i = GET_NUMBER_OF_UNIT_PEDS(iUnitType, iUnitVariation)-1
						SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].mnPedModel)
					ENDIF
				ENDIF
			ENDIF
			
			RETURN FALSE
		ELSE
			PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Ped ", i, "] CREATE_PLACED_UNIT_PEDS_MP - exists")
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_PLACED_UNIT_DYNOPROP_MP(UNIT_NETWORK_ENTITY_STRUCT &sUnitEntities[], INT iUnitNumber, INT &iNumCreatedObjects)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableUnitObjects")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	
	INT iUnitType = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].iUnitType
	INT i
	FOR i = 0 TO GET_NUMBER_OF_UNIT_DYNOPROPS(iUnitType)-1
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sUnitEntities[iUnitNumber].oiProps[i])
			
			MODEL_NAMES mnDynoProp = GET_DEFAULT_MODEL_NAME_FOR_UNIT_DYNOPROP(iUnitType)
			
			REQUEST_MODEL(mnDynoProp)
			
			IF HAS_MODEL_LOADED(mnDynoProp)
				
				VECTOR vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading, GET_OFFSET_FOR_UNIT_DYNOPROP(iUnitType, i))
				
				IF CREATE_NET_OBJ(sUnitEntities[iUnitNumber].oiProps[i], mnDynoProp, vPos)	
					FLOAT fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].fHeading + GET_HEADING_FOR_UNIT_DYNOPROP(iUnitType, i)
					
					PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Dyno ", i, "] CREATE_PLACED_UNIT_DYNOPROP_MP - Unit pos: ", g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].vPos)
					PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Dyno ", i, "] CREATE_PLACED_UNIT_DYNOPROP_MP - Unit ", iUnitNumber, " dyno prop created at ", vPos, " heading ", fHeading)
					
					OBJECT_INDEX oiDynoProp = NET_TO_OBJ(sUnitEntities[iUnitNumber].oiProps[i])
					
					IF DOES_ENTITY_EXIST(oiDynoProp)
						SET_ENTITY_HEADING(oiDynoProp, fHeading)
						SET_ENTITY_LOAD_COLLISION_FLAG(oiDynoProp, TRUE)
					ENDIF
					
					iNumCreatedObjects++
					PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Dyno ", i, "] CREATE_PLACED_UNIT_DYNOPROP_MP - Number of created dyno-prop is now: ", iNumCreatedObjects)
					
					IF i = GET_NUMBER_OF_UNIT_DYNOPROPS(iUnitType)-1
						SET_MODEL_AS_NO_LONGER_NEEDED(mnDynoProp)
					ENDIF
				ENDIF
			ENDIF
			
			RETURN FALSE
		ELSE
			OBJECT_INDEX oiDynoProp = NET_TO_OBJ(sUnitEntities[iUnitNumber].oiProps[i])
					
			IF DOES_ENTITY_EXIST(oiDynoProp)
				PRINTLN("CREATE_PLACED_UNIT_DYNOPROP_MP, Dyno-prop ", i, " exists")
				IF NOT HAS_COLLISION_LOADED_AROUND_ENTITY(oiDynoProp)
					PRINTLN("[Creation][Units][Unit ", iUnitNumber,"][Dyno ", i, "] CREATE_PLACED_UNIT_DYNOPROP_MP - Collision not loaded")
					
					RETURN FALSE
				ELSE
					FLOAT fZ = 0
					VECTOR vPos = GET_ENTITY_COORDS(oiDynoProp)
					IF GET_GROUND_Z_FOR_3D_COORD(vPos, fZ)
						vPos.Z = fZ
						SET_ENTITY_COORDS(oiDynoProp, vPos)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_ALL_PLACED_UNITS_MP(UNIT_NETWORK_ENTITY_STRUCT &sUnitEntities[], BOOL bCreated, INT &iCreatedNetVehicles, INT &iCreatedNetPeds, INT &iNumCreatedObjects)
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced = 0
		RETURN TRUE
	ENDIF
	
	IF bCreated
		RETURN TRUE
	ENDIF
	
	INT iUnit, iUnitsCreated
	INT iPedTotal, iVehicleTotal, iObjTotal
	
	iPedTotal = g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds
	iVehicleTotal = g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles
	iObjTotal = g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects	
	
	iVehicleTotal -= iCreatedNetVehicles
	iPedTotal -= iCreatedNetPeds
	iObjTotal -= iNumCreatedObjects
	
	IF CAN_REGISTER_MISSION_ENTITIES(iPedTotal, iVehicleTotal, iObjTotal, 0)
		FOR iUnit = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced-1
			IF CREATE_PLACED_UNIT_VEHICLES_MP(sUnitEntities, iUnit, iCreatedNetVehicles)
			AND CREATE_PLACED_UNIT_PEDS_MP(sUnitEntities, iUnit, iCreatedNetPeds)
			AND CREATE_PLACED_UNIT_DYNOPROP_MP(sUnitEntities, iUnit, iNumCreatedObjects)
				iUnitsCreated++
				PRINTLN("[Creation][Units][Unit ", iUnit,"] CREATE_ALL_PLACED_UNITS_MP - Units created: ", iUnitsCreated)
			ENDIF
		ENDFOR
	ELSE
		PRINTLN("[Creation][Units] CREATE_ALL_PLACED_UNITS_MP - Can't register entities")
	ENDIF
	
	RETURN iUnitsCreated = g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Model Loading
// ##### Description: Model Loading functions shared by the controllers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//////////////////////////////      LOAD ALL MODELS 		//////////////////////////////
FUNC BOOL REQUEST_LOAD_FMMC_MODELS()//INT iStage = 0 )
	BOOL bReturn = TRUE
	
	INT i
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
		//AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStage = iStage 
			
			PRINTLN("[RCC MISSION] - requesting model for ped : ", i)
			IF NOT REQUEST_LOAD_MODEL( g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn )
				PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedPed[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn))
				bReturn = FALSE
			ENDIF
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = IG_LESTERCREST
				REQUEST_CLIP_SET("move_heist_lester")
				IF NOT HAS_CLIP_SET_LOADED("move_heist_lester")
					PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_MODELS - FAILING ON - lester ped clipset ", i)
					bReturn = FALSE
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iInventoryBS > 0
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iInventoryBS,ciPED_INV_Briefcase_prop_ld_case_01)
					REQUEST_MODEL(PROP_LD_CASE_01)
					REQUEST_ANIM_DICT("weapons@misc@jerrycan@mp_male")
					IF (NOT HAS_MODEL_LOADED(PROP_LD_CASE_01))
						PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON load Ped inventory Briefcase model PROP_LD_CASE_01")
						bReturn = FALSE
					ENDIF
					IF (NOT HAS_ANIM_DICT_LOADED("weapons@misc@jerrycan@mp_male"))
						PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON load Ped inventory Briefcase anim dict weapons@misc@jerrycan@mp_male")
						bReturn = FALSE
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.iNumberOfPeds[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn))PRINTNL()
		ENDIF
	ENDREPEAT	
//	REPEAT g_FMMC_STRUCT.iNumberOfAnimals 	 i
//		IF g_FMMC_STRUCT.sPlacedAnimal[i].mn != DUMMY_MODEL_FOR_SCRIPT
//		//AND g_FMMC_STRUCT.sPlacedAnimal[i].iStage = iStage 
//			IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT.sPlacedAnimal[i].mn)
//				bReturn = FALSE
//			ENDIF
//		ELSE
//			//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.iNumberOfPeds[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn))PRINTNL()
//		ENDIF
//	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
		//AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iStage = iStage 
			PRINTLN("[RCC MISSION] - setting requesting model for veh : ", i)
			IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
				PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
				bReturn = FALSE
			ELSE
				PRINTLN("Loaded veh ", i, " all good")
			ENDIF
		ELSE
			//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))PRINTNL()
		ENDIF
	ENDREPEAT	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn != DUMMY_MODEL_FOR_SCRIPT
		//AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iStage = iStage 
			PRINTLN("[RCC MISSION] - setting requesting model for object : ", i, " || ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))
			IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
				PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedObject[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))
				bReturn = FALSE
			ENDIF
		ELSE
			//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedObject[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))PRINTNL()
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
		//AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iStage = iStage 
			IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn)
				PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn))
				bReturn = FALSE
			ENDIF
		ELSE
			//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn))PRINTNL()
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
		//AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iStage = iStage 
		
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speed"))
				IF NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speeda")))
				AND NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speedb")))
					PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedProp[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
					bReturn = FALSE
				ELSE
					PRINTLN("[LM] LOADED: REQUEST_LOAD_FMMC_MODELS")
				ENDIF
			ENDIF
			IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
				PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedProp[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
				bReturn = FALSE
			ENDIF
		ELSE
			//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedProp[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))PRINTNL()
		ENDIF
	ENDREPEAT
	RETURN bReturn 
ENDFUNC

//////////////////////////////      remove ALL MODELS 		//////////////////////////////
PROC SET_FMMC_MODELS_AS_NOT_NEEDED()
	INT i
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_StartDead)
				REMOVE_ANIM_DICT(GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDeadAnim))
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iInventoryBS,ciPED_INV_Briefcase_prop_ld_case_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_CASE_01)
				REMOVE_ANIM_DICT("weapons@misc@jerrycan@mp_male")
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn)
			PRINTLN("[RCC MISSION] - setting cleanup model for ped : ", i)
		ENDIF
	ENDREPEAT	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
			PRINTLN("[RCC MISSION] - setting cleanup model for veh : ", i)
		ENDIF
	ENDREPEAT	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn)
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
		ENDIF
	ENDREPEAT
	RELEASE_FMMC_TRAIN_ASSETS()
ENDPROC

//////////////////////////////      remove ALL PROP MODELS 		//////////////////////////////
PROC SET_FMMC_PROP_MODELS_AS_NOT_NEEDED()
	
	INT i

	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps 	 i
		
		if g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = HEI_PROP_HEIST_APECRATE
			SET_MODEL_AS_NO_LONGER_NEEDED(A_C_Rhesus)
		endif 
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
		ENDIF

	ENDREPEAT

ENDPROC
