/// FM_Maintain_Strand_Mission
/// Bobby - 03/07/2014
/// functionality for dealing with the strand missions
///    
USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_cutscene.sch"
USING "net_team_info.sch"
USING "net_include.sch"

FUNC BOOL IS_THIS_A_ROUNDS_MISSION()
	RETURN g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds > 0
ENDFUNC

PROC SET_THIS_IS_A_ROUNDS_MISSION_INITIALISATION()
	PRINTLN("[TS] [MSROUND] - SET_THIS_IS_A_ROUNDS_MISSION_INITIALISATION")
	g_sTransitionSessionData.sMissionRoundData.bMissionRoundsInt = TRUE
ENDPROC
//Clear that this is a strand based mission
PROC CLEAR_THIS_IS_A_ROUNDS_MISSION_INITIALISATION()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sMissionRoundData.bMissionRoundsInt
		PRINTLN("[TS] [MSROUND] - CLEAR_THIS_IS_A_ROUNDS_MISSION_INITIALISATION")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sMissionRoundData.bMissionRoundsInt = FALSE
ENDPROC	
//Is this is a strand based mission
FUNC BOOL IS_THIS_IS_A_ROUNDS_MISSION_INITIALISATION()
	RETURN g_sTransitionSessionData.sMissionRoundData.bMissionRoundsInt
ENDFUNC	

//Set that we should show the rounds mission leaderboard
PROC SET_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
	PRINTLN("[TS] [MSROUND] - SET_DISPLAY_ROUNDS_MISSION_LEADER_BOARD")
	g_sTransitionSessionData.sMissionRoundData.bDisplayFinalLeaderBoard = TRUE
ENDPROC
//Clear that we should show the rounds mission leaderboard
PROC CLEAR_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sMissionRoundData.bDisplayFinalLeaderBoard
		PRINTLN("[TS] [MSROUND] - CLEAR__DISPLAY_ROUNDS_MISSION_LEADER_BOARD")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sMissionRoundData.bDisplayFinalLeaderBoard = FALSE
ENDPROC	
//Should we show the rounds mission leaderboard
FUNC BOOL SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
	RETURN g_sTransitionSessionData.sMissionRoundData.bDisplayFinalLeaderBoard
ENDFUNC	

//Set that we should show the rounds mission leaderboard
PROC SET_ROUNDS_TELEMETRY_STARTED()
	PRINTLN("[TS] [MSROUND] - SET_ROUNDS_TELEMETRY_STARTED")
	g_sTransitionSessionData.sMissionRoundData.bRoundsTelemertyStarted = TRUE
ENDPROC
//Clear that we should show the rounds mission leaderboard
PROC CLEAR_ROUNDS_TELEMETRY_STARTED()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sMissionRoundData.bRoundsTelemertyStarted
		PRINTLN("[TS] [MSROUND] - CLEAR_ROUNDS_TELEMETRY_STARTED")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sMissionRoundData.bRoundsTelemertyStarted = FALSE
ENDPROC	
//Should we show the rounds mission leaderboard
FUNC BOOL HAS_ROUNDS_TELEMETRY_STARTED()
	RETURN g_sTransitionSessionData.sMissionRoundData.bRoundsTelemertyStarted
ENDFUNC	

//Sets that we have JIPPed and need to set our outfits the next round
PROC ROUNDS_SET_NEED_TO_SET_VS_OUTFITS()
	PRINTLN("[TS] [MSROUND] - ROUNDS_SET_NEED_TO_SET_VS_OUTFITS")
	SET_BIT(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_SET_UP_VS_OUTFITS)
ENDPROC
//Clears that we have JIPPed and need to set our outfits the next round
PROC ROUNDS_CLEAR_NEED_TO_SET_VS_OUTFITS()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_SET_UP_VS_OUTFITS)
		PRINTLN("[TS] [MSROUND] - ROUNDS_CLEAR_NEED_TO_SET_VS_OUTFITS")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_SET_UP_VS_OUTFITS)
ENDPROC	
//Have we JIPPed and need to set our outfits the next round
FUNC BOOL ROUNDS_NEED_TO_SET_VS_OUTFITS()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_SET_UP_VS_OUTFITS)
ENDFUNC	

//Sets that we have JIPPed and need to set our vehicle the next round
PROC ROUNDS_SET_NEED_TO_SET_VEHICLE()
	PRINTLN("[TS] [MSROUND] - ROUNDS_SET_NEED_TO_SET_VEHICLE")
	SET_BIT(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_SET_UP_VEHICLE)
ENDPROC
//Clears that we have JIPPed and need to set our vehicle the next round
PROC ROUNDS_CLEAR_NEED_TO_SET_VEHICLE()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_SET_UP_VEHICLE)
		PRINTLN("[TS] [MSROUND] - ROUNDS_CLEAR_NEED_TO_SET_VEHICLE")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_SET_UP_VEHICLE)
ENDPROC	
//Have we JIPPed and need to set our vehicle the next round
FUNC BOOL ROUNDS_NEED_TO_SET_VEHICLE()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_SET_UP_VEHICLE)
ENDFUNC	


FUNC BOOL IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION()
		
	IF IS_THIS_A_ROUNDS_MISSION()
		RETURN TRUE
	ENDIF

	IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

//Count elegible players 
FUNC INT GET_NUMBER_OF_ELEGIABLE_PLAYERS_IN_SESSION(BOOL bCheckOnMissionScript = FALSE)
	INT iReturn
	INT iParticipant
	PLAYER_INDEX playerID
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF NETWORK_IS_PLAYER_ACTIVE(playerID)
				//Doing this on the mission script
				IF bCheckOnMissionScript
					//If they're not SCTV
					IF NOT IS_PLAYER_SCTV(playerId)	
					//And are on a mission then count them
					AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
						iReturn++
						PRINTLN("[TS] GET_NUMBER_OF_ELEGIABLE_PLAYERS_IN_SESSION - player ", GET_PLAYER_NAME(playerID), " counted")	
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[TS] GET_NUMBER_OF_ELEGIABLE_PLAYERS_IN_SESSION - player ", GET_PLAYER_NAME(playerID), " is ignored SCTV/Not playing")
						#ENDIF
					ENDIF	
				//In the corona 
				ELSE
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].sClientCoronaData.iBitSet, ciCORONA_BS_IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA)
					AND NOT IS_PLAYER_SCTV(playerId)	
						iReturn++
						PRINTLN("[TS][CORONA] GET_NUMBER_OF_ELEGIABLE_PLAYERS_IN_SESSION - player ", GET_PLAYER_NAME(playerID), " counted")	
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[TS][CORONA] GET_NUMBER_OF_ELEGIABLE_PLAYERS_IN_SESSION - player ", GET_PLAYER_NAME(playerID), " is ignored SCTV/Not playing")
						#ENDIF
					ENDIF	
				ENDIF
			ENDIF	
		ENDIF
	ENDREPEAT
	RETURN iReturn
ENDFUNC

//Should we swap to the smallest team if we won?
FUNC BOOL SHOULD_WINNER_SWAP_TO_SMALLEST_TEAM()
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_WinnerToSmallest")
			RETURN TRUE
		ENDIF
	#ENDIF
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciSWAP_WINNER_TO_SMALLEST_TEAM)
ENDFUNC
//Set that we should stop the rounds mission due to a team leaving
PROC SET_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING()
	INT iNumPlayers = GET_NUMBER_OF_ELEGIABLE_PLAYERS_IN_SESSION(TRUE) 
	IF (SHOULD_WINNER_SWAP_TO_SMALLEST_TEAM()
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciALWAYS_SWAP_SMALL_TEAM))
	AND iNumPlayers > g_FMMC_STRUCT.iMaxNumberOfTeams
		PRINTLN("[TS] [MSROUND] - SHOULD_WINNER_SWAP_TO_SMALLEST_TEAM = TRUE - GET_NUMBER_OF_ELEGIABLE_PLAYERS_IN_SESSION(TRUE) = ", iNumPlayers)
		EXIT
	ENDIF
	PRINTLN("[TS] [MSROUND] - SET_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING")
	g_sTransitionSessionData.sMissionRoundData.bEndTeamLeft = TRUE
ENDPROC
//Clear that we should stop the rounds mission due to a team leaving
PROC CLEAR_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sMissionRoundData.bEndTeamLeft
		PRINTLN("[TS] [MSROUND] - CLEAR_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sMissionRoundData.bEndTeamLeft = FALSE
ENDPROC	
//Should stop the rounds mission due to a team leaving
FUNC BOOL SHOULD_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING()
	RETURN g_sTransitionSessionData.sMissionRoundData.bEndTeamLeft
ENDFUNC	

//Sets that this is a starnd based mission
PROC ROUNDS_SET_NEED_TO_BALANCE_MY_TEAM()
	PRINTLN("[TS] [MSROUND] - ROUNDS_SET_NEED_TO_BALANCE_MY_TEAM")
	SET_BIT(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_BALANCE_MY_TEAM)
ENDPROC
//Clear that this is a strand based mission
PROC ROUNDS_CLEAR_NEED_TO_BALANCE_MY_TEAM()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_BALANCE_MY_TEAM)
		PRINTLN("[TS] [MSROUND] - ROUNDS_CLEAR_NEED_TO_BALANCE_MY_TEAM")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_BALANCE_MY_TEAM)
ENDPROC	
//Is this is a strand based mission
FUNC BOOL ROUNDS_NEED_TO_BALANCE_MY_TEAM()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_BALANCE_MY_TEAM)
ENDFUNC	

//Sets that this is a starnd based mission
PROC SET_AT_END_OF_ROUNDS_MISSION()
	PRINTLN("[TS] [MSROUND] - SET_AT_END_OF_ROUNDS_MISSION")
	SET_BIT(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_AT_END_OF_MISSION)
ENDPROC
//Clear that this is a strand based mission
PROC CLEAR_AT_END_OF_ROUNDS_MISSION()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_AT_END_OF_MISSION)
		PRINTLN("[TS] [MSROUND] - CLEAR_AT_END_OF_ROUNDS_MISSION")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_AT_END_OF_MISSION)
ENDPROC	
//Is this is a strand based mission
FUNC BOOL AM_I_AT_END_OF_ROUNDS_MISSION()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_AT_END_OF_MISSION)
ENDFUNC	

PROC SET_ROUNDS_MISSION_CONTENT_ID(STRING stContentID)
	PRINTLN("[TS] [MSROUND] - SET_ROUNDS_MISSION_CONTENT_ID - ", stContentID)
	g_sTransitionSessionData.sMissionRoundData.tl23NextContentID = stContentID
ENDPROC	
PROC CLEAR_ROUNDS_MISSION_CONTENT_ID()
	#IF IS_DEBUG_BUILD
	IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sMissionRoundData.tl23NextContentID)
		PRINTLN("[TS] [MSROUND] - CLEAR_ROUNDS_MISSION_CONTENT_ID")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sMissionRoundData.tl23NextContentID = ""
ENDPROC
FUNC TEXT_LABEL_23 GET_ROUNDS_MISSION_CONTENT_ID()
	RETURN g_sTransitionSessionData.sMissionRoundData.tl23NextContentID
ENDFUNC
			
FUNC BOOL SHOULD_SHOULD_ROUNDS_MISSION_INTRO()
	RETURN g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds != 0
ENDFUNC
FUNC BOOL SHOULD_SHOULD_ROUNDS_MISSION_LB()
	RETURN g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds != 0
ENDFUNC

//Set the status of the transition joining
PROC SET_MAINTAIN_ROUNDS_MISSION_STATUS(INT iStatus)
	#IF IS_DEBUG_BUILD
	SWITCH iStatus
		CASE ciMAINTAIN_MISSION_ROUNDS_WAIT 				PRINTLN("[TS] [MSROUND] - SET_MAINTAIN_ROUNDS_MISSION_STATUS - WAIT")			BREAK
		CASE ciMAINTAIN_MISSION_ROUNDS_MISSION_END			PRINTLN("[TS] [MSROUND] - SET_MAINTAIN_ROUNDS_MISSION_STATUS - MISSION_END")	BREAK
		CASE ciMAINTAIN_MISSION_ROUNDS_DOWNLOAD_NEXT		PRINTLN("[TS] [MSROUND] - SET_MAINTAIN_ROUNDS_MISSION_STATUS - DOWNLOAD_NEXT")	BREAK
		CASE ciMAINTAIN_MISSION_ROUNDS_SET_UP_NEXT			PRINTLN("[TS] [MSROUND] - SET_MAINTAIN_ROUNDS_MISSION_STATUS - SET_UP_NEXT")	BREAK
		CASE ciMAINTAIN_MISSION_ROUNDS_CORONA_WAIT			PRINTLN("[TS] [MSROUND] - SET_MAINTAIN_ROUNDS_MISSION_STATUS - CORONA_WAIT")	BREAK
		CASE ciMAINTAIN_MISSION_ROUNDS_RESET				PRINTLN("[TS] [MSROUND] - SET_MAINTAIN_ROUNDS_MISSION_STATUS - RESET")			BREAK
	ENDSWITCH
	#ENDIF
	g_sTransitionSessionData.sMissionRoundData.iSwitchState = iStatus
ENDPROC

//Check to see if we have a clear rounds mission
FUNC BOOL DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER()
	INT iLoop
//	INT iHighestScore
//	INT iHighestTeam
//	BOOL bClearHighScoreWinner = TRUE 
	
	//Print some debug information
	#IF IS_DEBUG_BUILD
	FOR iLoop = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		IF g_MissionControllerserverBD_LB.sleaderboard[iLoop].iTeam != -1
			IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(iLoop))
			AND NOT IS_PLAYER_SCTV(INT_TO_PLAYERINDEX(iLoop))
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR((INT_TO_PLAYERINDEX(iLoop)))
				PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - g_MissionControllerserverBD_LB.sleaderboard[", iLoop, "].playerID     = ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iLoop)))
			ELSE
				PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - g_MissionControllerserverBD_LB.sleaderboard[", iLoop, "].             = Not active")
			ENDIF
			PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - g_MissionControllerserverBD_LB.sleaderboard[", iLoop, "].iRoundsScore = ", GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore)
		ENDIF
	ENDFOR
	#ENDIF
	
	//If we've not played the number of rounds yet
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed < g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds
		PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed < g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds - bClearHighScoreWinner = FALSE")
		RETURN FALSE
	ENDIF
	
	BOOL bReturn
	#IF IS_DEBUG_BUILD
	PLAYER_INDEX playerID
	#ENDIF
	//If a player has played the correct number of rounds
	FOR iLoop = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		IF GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore >= g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds+1
			g_sTransitionSessionData.sMissionRoundData.piWinner = INT_TO_PLAYERINDEX(iLoop)
			#IF IS_DEBUG_BUILD
			playerID = g_sTransitionSessionData.sMissionRoundData.piWinner
			IF NETWORK_IS_PLAYER_ACTIVE(playerID)
			AND NOT IS_PLAYER_SCTV(playerID)
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerID)
				PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - ", GET_PLAYER_NAME(playerID), " - GlobalplayerBD_FM_2[", iLoop, "].sJobRoundData.iRoundScore     = ", GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore)
			ENDIF
			#ENDIF
			bReturn = TRUE
		ENDIF
	ENDFOR
	
	RETURN bReturn
	
//This is the logic for best of, we now do first too	
//	//If the number of rounds played is less than the number of rounds then there's no clear winner, unless they've got more than half
//	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed < g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds
//		PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed < g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds - bClearHighScoreWinner = FALSE")
//		bClearHighScoreWinner = FALSE
//	ENDIF
//	
//	BOOL bClearedBecauseTwoPeopleTheSame 
//	
//	//Calculate the highest score
//	FOR iLoop = 0 TO (MAX_NUM_MC_PLAYERS - 1)
//		//Check to see if there is a clear high score winner
//		IF bClearHighScoreWinner
//		AND GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore = iHighestScore
//		//And the scores not 0
//		AND iHighestScore != 0
//		//And it's not the same team
//		AND iHighestTeam != GlobalplayerBD_FM_2[iLoop].sJobRoundData.iTeam
//			PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - Two people have the same high score - bClearHighScoreWinner = FALSE")
//			bClearHighScoreWinner = FALSE
//			bClearedBecauseTwoPeopleTheSame = TRUE
//		ENDIF
//		//Store that we've got a high score winner
//		IF GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore > iHighestScore
//			PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - iHighestScore = g_MissionControllerserverBD_LB.sleaderboard[", iLoop, "].iRoundsScore = ", GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore)
//			iHighestScore = GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore
//			iHighestTeam = GlobalplayerBD_FM_2[iLoop].sJobRoundData.iTeam
//			//Already cleared that we've got a highest winner then clear it
//			IF bClearHighScoreWinner = FALSE
//			AND bClearedBecauseTwoPeopleTheSame
//				PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - We have a new higest winner - bClearHighScoreWinner = TRUE")
//				bClearHighScoreWinner = TRUE
//			ENDIF
//		ENDIF
//	ENDFOR
//	
//	//If there's a clear winner
//	IF bClearHighScoreWinner	
//		//Loop all players to find him
//		FOR iLoop = 0 TO (MAX_NUM_MC_PLAYERS - 1)
//			//If the score is = to the highest score then this player is the winner
//			IF GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore = iHighestScore
//				PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - g_MissionControllerserverBD_LB.sleaderboard[", iLoop, "].iRoundsScore = iHighestScore")
//				RETURN TRUE
//			ENDIF
//		ENDFOR
//	//Not a clear winner then see if we've got a best of rounds winner only if we've not played all the rounds
//	ELIF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed < g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds
//		FOR iLoop = 0 TO (MAX_NUM_MC_PLAYERS - 1)
//			IF GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore >= CEIL((TO_FLOAT(g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds+1))/2.0)
//				PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - g_MissionControllerserverBD_LB.sleaderboard[", iLoop, "].iRoundsScore >= CEIL((TO_FLOAT(g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds+1))/2.0)")
//				PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - ", GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore, " >= CEIL((TO_FLOAT(", g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds+1, "))/2.0)")
//				PRINTLN("[TS] [MSROUND] - DO_WE_HAVE_A_CLEAR_ROUNDS_MISSION_WINNER - ", GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore, " >= ", CEIL((TO_FLOAT(g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds+1))/2.0))
//				RETURN TRUE
//			ENDIF
//		ENDFOR
//	ENDIF
	RETURN FALSE
ENDFUNC
//find the smallest team
FUNC INT FIND_SMALLEST_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION()

	//Hard-coded for this mode due to team-swapping issues // url:bugstar:4925636
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("FIND_SMALLEST_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION - Returning 2 due to IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK")
		RETURN 2
	ENDIF
	
	INT iReturn = -1
	INT iLoop
	INT iMaxPlayers
	FOR iLoop = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)
		IF(g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop] < iMaxPlayers 
		OR iReturn = -1)
		AND g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop] != 0
			iReturn = iLoop
			iMaxPlayers = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop]
		ENDIF
	ENDFOR
	PRINTLN("[SST] - FIND_SMALLEST_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION iReturn = ", iReturn)
	RETURN iReturn
ENDFUNC

//find the largest team
FUNC INT FIND_LARGET_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION()
	INT iReturn = -1
	INT iLoop
	INT iMaxPlayers
	FOR iLoop = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)
		//If it's 0 then there's no cap so it's a largest team
		IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop] = 0
			PRINTLN("[SST] - FIND_LARGET_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION - g_FMMC_STRUCT.iMaxNumPlayersPerTeam[", iLoop, "] = 0 - RETURN = ", iLoop)
			RETURN iLoop
		ELIF(g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop] > iMaxPlayers 
		OR iReturn = -1)
			iReturn = iLoop
			iMaxPlayers = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop]
		ENDIF
	ENDFOR
	PRINTLN("[SST] - FIND_LARGET_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION iReturn = ", iReturn)
	RETURN iReturn
ENDFUNC

//set that I won the last swap to smallest mission
PROC SET_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION()
	PRINTLN("[SST] - SET_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION")
	g_sTransitionSessionData.bWonLastSwapToSmallestMission = TRUE
ENDPROC
//Clear that I won the last swap to smallest mission
PROC CLEAR_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.bWonLastSwapToSmallestMission
		PRINTLN("[SST] - CLEAR_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.bWonLastSwapToSmallestMission = FALSE
ENDPROC
//Did I win the last swap to smallest mission
FUNC BOOL WON_LAST_SWAP_TO_SMALLEST_MISSION()
	RETURN g_sTransitionSessionData.bWonLastSwapToSmallestMission
ENDFUNC
//set that I won the last swap to smallest mission
PROC SET_THAT_I_DIDNT_KILL_BEAST_LAST_MISSION()
	PRINTLN("[SST] - SET_THAT_I_DIDNT_KILL_BEAST_LAST_MISSION")
	g_sTransitionSessionData.bDidntKillBeastLastMission = TRUE
ENDPROC
//Clear that I won the last swap to smallest mission
PROC CLEAR_THAT_I_DIDNT_KILL_BEAST_LAST_MISSION()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.bDidntKillBeastLastMission
		PRINTLN("[SST] - CLEAR_THAT_I_DIDNT_KILL_BEAST_LAST_MISSION")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.bDidntKillBeastLastMission = FALSE
ENDPROC
//Did I win the last swap to smallest mission
FUNC BOOL DIDNT_KILL_BEAST_LAST_MISSION()
	RETURN g_sTransitionSessionData.bDidntKillBeastLastMission
ENDFUNC

//Set that SCTV has launch the mission script
PROC SET_SCTV_READY_FOR_NEXT_MISSION()
	PRINTLN("[TS] - SET_SCTV_READY_FOR_NEXT_MISSION")
	g_sTransitionSessionData.bSctvReadyForNextMission = TRUE
ENDPROC
//Clear that SCTV has launch the mission script
PROC CLEAR_SCTV_READY_FOR_NEXT_MISSION()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.bSctvReadyForNextMission
		PRINTLN("[TS] - CLEAR_SCTV_READY_FOR_NEXT_MISSION")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.bSctvReadyForNextMission = FALSE
ENDPROC
//Has SCTV has launch the mission script
FUNC BOOL SCTV_READY_FOR_NEXT_MISSION()
	RETURN g_sTransitionSessionData.bSctvReadyForNextMission
ENDFUNC

PROC SET_VARS_AND_END_OF_ALWAYS_SWAP_SMALLEST_TEAM_MISSION(BOOL bSwapMe)
	
	//IF check creator bit
	
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = FIND_SMALLEST_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION()
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = FIND_LARGET_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION()
			CLEAR_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION()
			PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - [JS] Swapping to larger team")
		ELSE
			IF bSwapMe
			AND NOT IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_HAS_JOINED_SMALLEST_TEAM)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = FIND_SMALLEST_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION()
				SET_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION()
				PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - [JS] Swapping to Smallest team")
			ENDIF
			PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - [JS] Doing nothing")
		ENDIF
		
	//ENDIF
	
ENDPROC

//Set the vars at the end of a swap to smallest team
PROC SET_VARS_AND_END_OF_SWAP_TO_SMALLEST_TEAM_MISSION(BOOL bIWon)
	PRINTLN("[SST] * - SHOULD_WINNER_SWAP_TO_SMALLEST_TEAM - bIWon = ", bIWon)
	IF SHOULD_WINNER_SWAP_TO_SMALLEST_TEAM()
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciALWAYS_SWAP_SMALL_TEAM)
		//If I won
		IF bIWon
			PRINTLN("[SST] - SHOULD_WINNER_SWAP_TO_SMALLEST_TEAM - bIWon")
			//set me on to the smallest team
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = FIND_SMALLEST_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION()
			PRINTLN("[SST] * - SHOULD_WINNER_SWAP_TO_SMALLEST_TEAM - bIWon - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
			//set the persistant flag so the next corona can be set up correctly
			SET_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION()
		//If I lost and I'm on the smallest team them move me to the largert team
		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = FIND_SMALLEST_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION()
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = FIND_LARGET_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION()
			PRINTLN("[SST] * - SHOULD_WINNER_SWAP_TO_SMALLEST_TEAM - bIWon = FALSE, I was on smallest team - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
			//Clear the persistant flag so the next corona can be set up correctly
			CLEAR_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION()
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[SST] * - SHOULD_WINNER_SWAP_TO_SMALLEST_TEAM ELSE ")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC SET_VARS_AND_SWAP_TO_SMALLEST_BEAST_TEAM(BOOL bKilledBeast)
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIDHash)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = 0
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = 1
			PRINTLN("[SST] * - SET_VARS_AND_SWAP_TO_SMALLEST_BEAST_TEAM - iTeamChosen = 0 - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
			//set the persistant flag so the next corona can be set up correctly
			CLEAR_THAT_I_DIDNT_KILL_BEAST_LAST_MISSION()
			CLEAR_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION()
		ELIF bKilledBeast
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = 0
			PRINTLN("[SST] * - SET_VARS_AND_SWAP_TO_SMALLEST_BEAST_TEAM - bKilledBeast - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
			SET_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION()
			CLEAR_THAT_I_DIDNT_KILL_BEAST_LAST_MISSION()
		ELSE
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = 0
			PRINTLN("[SST] * - SET_VARS_AND_SWAP_TO_SMALLEST_BEAST_TEAM - bKilledBeast = FALSE, I was on smallest team - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
			SET_THAT_I_DIDNT_KILL_BEAST_LAST_MISSION()
			CLEAR_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PASSES_JUGGERNAUT_TEAM_COUNT_CHECK(INT iLoop, INT icurrentTeamCount)
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ENDIF
	IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop] != 1
	OR g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop] = 0
		RETURN TRUE
	ENDIF
	IF icurrentTeamCount = 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
	

//Move the players team on a JIP
PROC PUSH_ME_TO_MOST_APPROPREATE_TEAM_ONROUNDS_JIP()
	IF ROUNDS_NEED_TO_BALANCE_MY_TEAM()
		PRINTLN("[TS] [MSROUND] - SET_PLAYER_BROADCAST_DATA_ON_JIP - g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds")
		//Max team     = g_FMMC_STRUCT.iMaxNumberOfTeams
		//Max per team = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] 
		INT icurrentTeamCount[FMMC_MAX_TEAMS]
		INT iLoop, iSmallestCount, iSmallestTeam, iGbdSlot
		PLAYER_INDEX playerID
		//Count all the players on the teams
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iLoop
			//If they are active
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLoop))
				//Get player ID
				playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLoop))
				//Not sctv
				IF NOT IS_PLAYER_SCTV(playerID)
					IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerID)
					OR IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(playerID)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_SET_UP_MY_TEAMS)
						iGbdSlot = NATIVE_TO_INT(playerID)
						//no array over run
						IF iGbdSlot != -1
						AND GlobalplayerBD_FM[iGbdSlot].sClientCoronaData.iTeamChosen != -1
						AND GlobalplayerBD_FM[iGbdSlot].sClientCoronaData.iTeamChosen< FMMC_MAX_TEAMS
							//Up the team count
							icurrentTeamCount[GlobalplayerBD_FM[iGbdSlot].sClientCoronaData.iTeamChosen]++
							PRINTLN("[TS] [MSROUND] - PUSH_ME_TO_MOST_APPROPREATE_TEAM_ONROUNDS_JIP - icurrentTeamCount[", GlobalplayerBD_FM[iGbdSlot].sClientCoronaData.iTeamChosen, "] = ",  icurrentTeamCount[GlobalplayerBD_FM[iGbdSlot].sClientCoronaData.iTeamChosen])		
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT			
		
		IF NOT g_sMPTunables.bDisableDeadLineTeamCheck
		AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
			REPEAT FMMC_MAX_TEAMS iLoop
				IF icurrentTeamCount[iLoop] = 0
					//Set the team that I should be in
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = iLoop
					PRINTLN("[SST] * [TS] [MSROUND] - PUSH_ME_TO_MOST_APPROPREATE_TEAM_ONROUNDS_JIP - IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ",  iLoop)		
					ROUNDS_CLEAR_NEED_TO_BALANCE_MY_TEAM()
					EXIT
				ENDIF
			ENDREPEAT 
		ELSE
			//Find the smallest team
			REPEAT g_FMMC_STRUCT.iMaxNumberOfTeams iLoop
				//if this team is smallest or the smallet is set to 0
				IF (icurrentTeamCount[iLoop] < iSmallestCount OR iSmallestCount = 0)
				//And the team count is not higher than the maximum, or the maximum is 0
				AND (icurrentTeamCount[iLoop] < g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop] OR g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop] = 0)
				AND PASSES_JUGGERNAUT_TEAM_COUNT_CHECK(iLoop, icurrentTeamCount[iLoop])
					//Store the count of the smallest team
					iSmallestCount = icurrentTeamCount[iLoop]
					PRINTLN("[TS] [MSROUND] - PUSH_ME_TO_MOST_APPROPREATE_TEAM_ONROUNDS_JIP - iSmallestCount = icurrentTeamCount[", iLoop, "] = ",  iSmallestCount)		
					//Store the smalleset team
					iSmallestTeam = iLoop
					PRINTLN("[TS] [MSROUND] - PUSH_ME_TO_MOST_APPROPREATE_TEAM_ONROUNDS_JIP - iSmallestCount = iLoop = ",  iLoop)		
				ENDIF
			ENDREPEAT 
		ENDIF
		//Set the team that I should be in
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = iSmallestTeam
		SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_SET_UP_MY_TEAMS)
		PRINTLN("[SST] * [TS] [MSROUND] - PUSH_ME_TO_MOST_APPROPREATE_TEAM_ONROUNDS_JIP - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ",  iSmallestTeam)		
		ROUNDS_CLEAR_NEED_TO_BALANCE_MY_TEAM()
	ENDIF	
ENDPROC




PROC FORCE_TEAM_MAX_SIZES_BASED_ON_RATIO(INT iCurrentNumPlayers)
	//If it's a mission
	IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION 
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH)
	//And sub type CTF/LTS/VS
	AND (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
	OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_COOP
	OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
	OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_VERSUS
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH)
	//And there's two teams?
	AND g_FMMC_STRUCT.iMaxNumberOfTeams = 2
	//And the ratio is not 50%
	AND g_FMMC_STRUCT.iTeamBalance != 5	
	//And there's more than two people
	AND iCurrentNumPlayers > 2
		PRINTLN("[TEAM BAL DATA] [SST] * FORCE_TEAM_MAX_SIZES_BASED_ON_RATIO - iCurrentNumPlayers = ", iCurrentNumPlayers)
		PRINTLN("[TEAM BAL DATA] [SST] * FORCE_TEAM_MAX_SIZES_BASED_ON_RATIO - g_FMMC_STRUCT.iTeamBalance = ", g_FMMC_STRUCT.iTeamBalance)
		//Get the menu option as a ratio
		FLOAT fRatio = TO_FLOAT(g_FMMC_STRUCT.iTeamBalance)/10
		PRINTLN("[TEAM BAL DATA] [SST] * FORCE_TEAM_MAX_SIZES_BASED_ON_RATIO - fRatio = TO_FLOAT(", g_FMMC_STRUCT.iTeamBalance, ")/10 = ", fRatio)
		INT iNumPlayersOnTeamOne = ROUND(TO_FLOAT(iCurrentNumPlayers) * fRatio)
		PRINTLN("[TEAM BAL DATA] [SST] * FORCE_TEAM_MAX_SIZES_BASED_ON_RATIO - iNumPlayersOnTeamOne  = ROUND(TO_FLOAT(", iCurrentNumPlayers, ") * ", fRatio, ") = ", iNumPlayersOnTeamOne )
		//Make it more than zero
		IF iNumPlayersOnTeamOne  = 0
			PRINTLN("[TEAM BAL DATA] [SST] * FORCE_TEAM_MAX_SIZES_BASED_ON_RATIO - iNumPlayersOnTeamOne  = 0 forcing to 1")
			iNumPlayersOnTeamOne  = 1
		ENDIF		
		//If it's set to the number of players in the session then change it to be 1 less
		IF iNumPlayersOnTeamOne = iCurrentNumPlayers
			PRINTLN("[TEAM BAL DATA] [SST] * FORCE_TEAM_MAX_SIZES_BASED_ON_RATIO - iNumPlayersOnTeamOne = iCurrentNumPlayers = ", iCurrentNumPlayers, " reducing it by one")
			iNumPlayersOnTeamOne--
		ENDIF
		PRINTLN("[TEAM BAL DATA] [SST] * fRatio = ", fRatio)
		g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] = iNumPlayersOnTeamOne 
		g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1] = iCurrentNumPlayers - iNumPlayersOnTeamOne 
		PRINTLN("[TEAM BAL DATA] [SST] * g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] = iNumPlayersOnTeamOne ")
		PRINTLN("[TEAM BAL DATA] [SST] * g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1] = iCurrentNumPlayers - iNumPlayersOnTeamOne ")
		PRINTLN("[TEAM BAL DATA] [SST] * g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] = ", iNumPlayersOnTeamOne , " = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0])
		PRINTLN("[TEAM BAL DATA] [SST] * g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1] = ", iCurrentNumPlayers, " - ", iNumPlayersOnTeamOne , " = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1])
		//If the numbers are the same then this is bad
		IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1]
			PRINTLN("[TEAM BAL DATA] [SST] * g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1] = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1])
			//If team 0 should be the biggest:
			IF fRatio > 0.5
				PRINTLN("[TEAM BAL DATA] [SST] * fRatio > 0.5 setting biggest team to 0 g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] = 0")
				g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] = 0
			ELSE
				PRINTLN("[TEAM BAL DATA] [SST] * fRatio > 0.5 setting biggest team to 0 g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1] = 0")
				g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1] = 0
			ENDIF
		ENDIF
		PRINTLN("[TEAM BAL DATA] [SST] * FORCE_TEAM_MAX_SIZES_BASED_ON_RATIO - g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0])
		PRINTLN("[TEAM BAL DATA] [SST] * FORCE_TEAM_MAX_SIZES_BASED_ON_RATIO - g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1] = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1])
	ENDIF
ENDPROC











