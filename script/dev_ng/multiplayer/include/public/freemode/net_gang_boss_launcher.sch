
USING "net_gang_boss.sch"
USING "fm_content_launching.sch"

// Legacy - Nothing should be added to this
FUNC BOOL GB_DOES_MISSION_USE_LEGACY_LOCATION_SYSTEM(INT iMission)

	SWITCH iMission
		CASE FMMC_TYPE_GB_CONTRABAND_BUY
		CASE FMMC_TYPE_BIKER_BUY
		CASE FMMC_TYPE_VEHICLE_EXPORT_BUY
		CASE FMMC_TYPE_GUNRUNNING_BUY
		CASE FMMC_TYPE_GUNRUNNING_SELL
		CASE FMMC_TYPE_SMUGGLER_BUY
		CASE FMMC_TYPE_SMUGGLER_SELL
		CASE FMMC_TYPE_FM_GANGOPS
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC GB_RESERVE_MISSION_LOCATION(INT iMission, INT iPlayer, INT iLocation)
	
	IF GlobalServerBD_FM_events.sGBWorkData.sLocationInUse[iPlayer].iMission != iMission
	OR GlobalServerBD_FM_events.sGBWorkData.sLocationInUse[iPlayer].iLocation != iLocation
		GlobalServerBD_FM_events.sGBWorkData.sLocationInUse[iPlayer].iMission = iMission	
		GlobalServerBD_FM_events.sGBWorkData.sLocationInUse[iPlayer].iLocation = iLocation			
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"][RESERVATIONS] GB_RESERVE_MISSION_LOCATION - Reserved location = ", iLocation," for mission ",iMission," for player ", iPlayer, " - ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)))
	ENDIF

ENDPROC

PROC GB_CLEAR_MISSION_LOCATION_RESERVATION(INT iPlayer)
	
	IF GlobalServerBD_FM_events.sGBWorkData.sLocationInUse[iPlayer].iMission != -1
	OR GlobalServerBD_FM_events.sGBWorkData.sLocationInUse[iPlayer].iLocation != -1
		GlobalServerBD_FM_events.sGBWorkData.sLocationInUse[iPlayer].iMission = -1
		GlobalServerBD_FM_events.sGBWorkData.sLocationInUse[iPlayer].iLocation = -1
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"][RESERVATIONS] GB_CLEAR_MISSION_LOCATION_RESERVATION - player ",iPlayer)
	ENDIF

ENDPROC

FUNC BOOL GB_SHOULD_CLEAR_MISSION_LOCATION_RESERVATION(INT iMission, INT iMissionToLaunch, INT iPlayer)

	RETURN GlobalServerBD_FM_events.sGBWorkData.sLocationInUse[iPlayer].iMission != iMission
		AND GlobalServerBD_FM_events.sGBWorkData.sLocationInUse[iPlayer].iMission != iMissionToLaunch

ENDFUNC

FUNC BOOL GET_VARIATION_FOR_MISSION_REQUEST(INT iMissionRequest, GB_BOSS_WORK_SERVER_VARIATION_DATA &vData, PLAYER_INDEX staggerPlayer, INT iStagger)

	INT iAttempts
	REPEAT 100 iAttempts
		
		SWITCH(iMissionRequest)
		
			CASE FMMC_TYPE_GB_CONTRABAND_BUY
				IF GB_GET_RANDOM_CONTRABAND_BUY_VARIATION_DATA(vData.iVariation1, vData.iVariation2, vData.iVariation3, staggerPlayer,GlobalServerBD_FM_events.sGBWorkData.iWarehouse[iStagger])
					RETURN TRUE			
				ENDIF
			BREAK
			
			CASE FMMC_TYPE_VEHICLE_EXPORT_BUY
				IF GB_GET_RANDOM_VEHICLE_EXPORT_VARIATION_DATA(vData.iVariation1, vData.iVariation2, vData.iVariation3, staggerPlayer)
					RETURN TRUE			
				ENDIF
			BREAK
			
			CASE FMMC_TYPE_BIKER_BUY
				IF GB_GET_RANDOM_ILLICIT_GOODS_VARIATION_DATA(vData.iVariation1, vData.iVariation2, vData.iVariation3, staggerPlayer, GlobalServerBD_FM_events.sGBWorkData.iWarehouse[iStagger])
					RETURN TRUE			
				ENDIF
			BREAK
			
			CASE FMMC_TYPE_GUNRUNNING_BUY
			CASE FMMC_TYPE_GUNRUNNING_SELL
				IF GB_GET_RANDOM_GUNRUN_VARIATION_DATA(vData.iVariation1, vData.iVariation2, vData.iVariation3, vData.sDropOffProperties, staggerPlayer, iMissionRequest)
					RETURN TRUE			
				ENDIF
			BREAK
			
			CASE FMMC_TYPE_SMUGGLER_BUY
			CASE FMMC_TYPE_SMUGGLER_SELL
				IF GB_GET_RANDOM_SMUGGLER_VARIATION_DATA(vData.iVariation1, vData.iVariation2, vData.iVariation3, staggerPlayer, iMissionRequest,vData.sDropOffProperties)
					RETURN TRUE			
				ENDIF
			BREAK
			
			CASE FMMC_TYPE_FM_GANGOPS
				IF GB_GET_RANDOM_GANGOPS_VARIATION_DATA(vData.iVariation1, vData.iVariation2, vData.iVariation3, staggerPlayer, iMissionRequest,vData.sDropOffProperties)
					RETURN TRUE			
				ENDIF
			BREAK
			
			CASE FMMC_TYPE_FMBB_SELL
			CASE FMMC_TYPE_FMBB_CLUB_MANAGEMENT
				IF GB_GET_RANDOM_MB_VARIATION_DATA(vData.iVariation1, vData.iVariation2, iMissionRequest, staggerPlayer)
					RETURN TRUE			
				ENDIF
			BREAK
			
			CASE FMMC_TYPE_GB_CASINO
				IF GB_GET_RANDOM_GBC_VARIATION_DATA(vData.iVariation1, vData.iVariation2, staggerPlayer, vData.sDropOffProperties)
					RETURN TRUE			
				ENDIF
			BREAK
			
			CASE FMMC_TYPE_GB_CASINO_HEIST
				IF GB_GET_RANDOM_CSH_VARIATION_DATA(vData.iVariation1, vData.iVariation2, staggerPlayer, vData.sDropOffProperties)
					RETURN TRUE			
				ENDIF
			BREAK
					
			DEFAULT
				IF GET_FREEMODE_CONTENT_VARIATION_DATA(iMissionRequest, vData.iVariation1, vData.iVariation2, staggerPlayer)
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			BREAK
			
		ENDSWITCH	
	
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC

//Stagger through participants and check for mission launch requests
PROC GB_LAUNCHER_SERVER_MAINTAIN_REQUESTS(INT &iStagger)
	
	BOOL bPlayerActive
	PLAYER_INDEX staggerPlayer = INT_TO_PLAYERINDEX(iStagger)
	IF NETWORK_IS_PLAYER_ACTIVE(staggerPlayer)
		bPlayerActive = TRUE
	ENDIF
	
	IF GlobalServerBD_FM_events.sGBWorkData.iMissionRequest[iStagger] != -1
		IF bPlayerActive
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_REQUESTS - Mission is ",GET_MP_MISSION_NAME(Convert_FM_Mission_Type_To_MissionID(GlobalServerBD_FM_events.sGBWorkData.iMissionRequest[iStagger])))
			BOOL bOkToLaunch = GB_IS_BOSS_MISSION_AVAILABLE(staggerPlayer,GlobalServerBD_FM_events.sGBWorkData.iMissionRequest[iStagger],TRUE)
			
			GB_BOSS_WORK_SERVER_VARIATION_DATA vData
			
			IF bOkToLaunch
				INT iMissionRequest2 = GlobalServerBD_FM_events.sGBWorkData.iMissionRequest[iStagger]
			
				IF DOES_MISSION_REQUEST_NEED_VARIATION_SELECTION(iMissionRequest2)
					IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(INT_TO_PLAYERINDEX(iStagger)) = iMissionRequest2
						vData.iVariation1 = GlobalServerBD_FM_events.sGBWorkData.iVariationRequest[iStagger]
						vData.iVariation2 = GlobalServerBD_FM_events.sGBWorkData.iVariationRequest2[iStagger]
						vData.iVariation3 = GlobalServerBD_FM_events.sGBWorkData.iVariationRequest3[iStagger]
						
						IF GET_VARIATION_FOR_MISSION_REQUEST(iMissionRequest2,vData, staggerPlayer, iStagger)
							IF GB_SHOULD_MISSION_RESERVE_LOCATION(iMissionRequest2)
								IF GB_DOES_MISSION_USE_LEGACY_LOCATION_SYSTEM(iMissionRequest2)
									GB_RESERVE_MISSION_LOCATION(iMissionRequest2, iStagger, vData.iVariation3)
								ELSE
									GB_RESERVE_MISSION_LOCATION(iMissionRequest2, iStagger, vData.iVariation2)
								ENDIF
							ENDIF
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GET_VARIATION_FOR_MISSION_REQUEST - Variation ",vData.iVariation1," / ", vData.iVariation2, " / " ,vData.iVariation3, " used by player ",GET_PLAYER_NAME(staggerPlayer))
						ELSE
							bOkToLaunch = FALSE
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_REQUESTS - Ran out of attempts to find mission variation, bOkToLaunch = FALSE")
						ENDIF
					ELSE
						iStagger++
						IF iStagger >= NUM_NETWORK_PLAYERS
							iStagger = 0
						ENDIF
						PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_REQUESTS - Player BD not set, Exiting...")
						EXIT
					ENDIF
				ENDIF
			ENDIF
			
			IF bOkToLaunch
				IF GB_GET_CATEGORY_OF_BOSS_MISSION(GlobalServerBD_FM_events.sGBWorkData.iMissionRequest[iStagger]) = GB_BOSS_MISSION_CATEGORY_GANG_VS_SESSION
					GlobalServerBD_FM_events.sGBWorkData.iGBSessionMissionLaunching = GlobalServerBD_FM_events.sGBWorkData.iMissionRequest[iStagger]
					PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_REQUESTS - Launching session boss mission: ",GlobalServerBD_FM_events.sGBWorkData.iMissionRequest[iStagger])
				ENDIF

				GB_MISSION eMission = GB_GET_GB_MISSION_FROM_FMMC_TYPE(GlobalServerBD_FM_events.sGBWorkData.iMissionRequest[iStagger])
				GlobalServerBD_FM_events.sGBWorkData.iNumMissionInstances[eMission]++
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_REQUESTS - Num instances is now: ",GlobalServerBD_FM_events.sGBWorkData.iNumMissionInstances[eMission])
			ENDIF
			BROADCAST_GB_BOSS_WORK_SERVER_CONFIRM(bOkToLaunch,GlobalServerBD_FM_events.sGBWorkData.iMissionRequest[iStagger],SPECIFIC_PLAYER(staggerPlayer),vData)
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_REQUESTS - Sending response of ",bOkToLaunch," to player ",iStagger," ",GET_PLAYER_NAME(staggerPlayer))
		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_REQUESTS - Player ",iStagger," not active, discarding request.")
		ENDIF
		//Destroy the request
		GlobalServerBD_FM_events.sGBWorkData.iMissionRequest[iStagger] = -1
		GlobalServerBD_FM_events.sGBWorkData.iVariationRequest[iStagger] = -1
		GlobalServerBD_FM_events.sGBWorkData.iVariationRequest2[iStagger] = -1
		GlobalServerBD_FM_events.sGBWorkData.iVariationRequest3[iStagger] = -1
		GlobalServerBD_FM_events.sGBWorkData.iWarehouse[iStagger] = -1
	ENDIF
	
	iStagger++
	IF iStagger >= NUM_NETWORK_PLAYERS
		iStagger = 0
	ENDIF

ENDPROC

FUNC BOOL GB_IS_SESSION_GANG_BOSS_MISSION_ENDING()

	RETURN GlobalServerBD_FM_events.sGBWorkData.bGBSessionMissionEnding

ENDFUNC

/// PURPOSE: Handles the server authentication for all session gang boss missions and missions with limited instances
///    
PROC GB_LAUNCHER_SERVER_MAINTAIN_INSTANCES()

	BOOL bMissionActive = FALSE

	INT iPotentialInstances[eGBM_MAX]
	
	BOOL bOnAnyMission
	BOOL bLaunchingAnyMission
	INT iCurrentMission
	INT iCurrentMissionToLaunch
	GB_BOSS_MISSION_CATEGORY eCategory
	PLAYER_INDEX missionHost
	
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerID
		playerID = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_NET_PLAYER_OK(playerID, FALSE)
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
		
			// Cache data
			iCurrentMission = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID)
			iCurrentMissionToLaunch = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(playerID)
			eCategory = GB_GET_CATEGORY_OF_BOSS_MISSION(iCurrentMission)
			bOnAnyMission = GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID)
			bLaunchingAnyMission = GB_IS_PLAYER_LAUNCHING_ANY_GANG_BOSS_MISSION(playerID)
			missionHost = GB_GET_PLAYER_MISSION_HOST(playerID)
		
			IF bOnAnyMission
				IF eCategory = GB_BOSS_MISSION_CATEGORY_GANG_VS_SESSION
					IF missionHost != INVALID_PLAYER_INDEX()
					AND NETWORK_IS_PLAYER_ACTIVE(missionHost)
						IF GlobalServerBD_FM_events.sGBWorkData.iGBSessionMission != iCurrentMission
						OR GlobalServerBD_FM_events.sGBWorkData.iGBSessionMissionHost != missionHost
							GlobalServerBD_FM_events.sGBWorkData.iGBSessionMission = iCurrentMission
							GlobalServerBD_FM_events.sGBWorkData.iGBSessionMissionHost = missionHost
							GlobalServerBD_FM_events.sGBWorkData.iGBSessionMissionLaunching = -1
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_INSTANCES - Gang VS Session Mission Active")
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_INSTANCES - Mission: ",GET_MP_MISSION_NAME(Convert_FM_Mission_Type_To_MissionID(GlobalServerBD_FM_events.sGBWorkData.iGBSessionMission)))
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_INSTANCES - Launched By: ",GET_PLAYER_NAME(GlobalServerBD_FM_events.sGBWorkData.iGBSessionMissionHost))
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_INSTANCES - Running for: ",GET_PLAYER_NAME(playerID))
						ENDIF
						
						IF GB_IS_GLOBAL_CLIENT_BIT0_SET(playerID,eGB_GLOBAL_CLIENT_BITSET_0_PLAYER_ON_MISSION_END_STAGE)
							//Set server bit mission ending
							IF NOT GlobalServerBD_FM_events.sGBWorkData.bGBSessionMissionEnding
								GlobalServerBD_FM_events.sGBWorkData.bGBSessionMissionEnding = TRUE
								PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_INSTANCES - Session mission ending according to player: ",GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
					ELSE
						IF GlobalServerBD_FM_events.sGBWorkData.iGBSessionMissionLaunching != -1
							GlobalServerBD_FM_events.sGBWorkData.iGBSessionMissionLaunching = -1
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_INSTANCES - Launching player has left session, clearing launching all session mission.")
						ENDIF
					ENDIF
					bMissionActive = TRUE
				ENDIF
			ENDIF

			//Calculate potential instances based off of how many people are running the script, or have requested to run it
			INT iMissionType = -1
			IF bOnAnyMission
				iMissionType = iCurrentMission
			ELIF (bLaunchingAnyMission
			AND GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.eMissionLaunchStage >= eGB_MISSION_STAGE_AWAITING_SERVER)
				iMissionType = iCurrentMissionToLaunch
			ENDIF
			
			//Running or launching a mission
			IF iMissionType != -1
				IF GB_GET_CATEGORY_OF_BOSS_MISSION(iMissionType) != GB_BOSS_MISSION_CATEGORY_GANG_VS_SESSION
					IF missionHost = playerID	//player launched mission
						GB_MISSION eMission = GB_GET_GB_MISSION_FROM_FMMC_TYPE(iMissionType)
						iPotentialInstances[eMission]++
					ENDIF
				ENDIF
			ENDIF
			
			//Check if reserved location is still being used			
			IF GB_SHOULD_CLEAR_MISSION_LOCATION_RESERVATION(iCurrentMission, iCurrentMissionToLaunch, iPlayer)
				GB_CLEAR_MISSION_LOCATION_RESERVATION(iPlayer)
			ENDIF
		ELSE			
			GB_CLEAR_MISSION_LOCATION_RESERVATION(iPlayer)
		ENDIF
	ENDREPEAT
	
	//If there are more actual instances than potential instances, we need to update our count of actual instances
	//We don't care if there are more potentials than actuals since iNumMissionInstances is set by the server
	INT iMission
	REPEAT eGBM_MAX iMission
		IF GlobalServerBD_FM_events.sGBWorkData.iNumMissionInstances[iMission] > iPotentialInstances[iMission]
			GlobalServerBD_FM_events.sGBWorkData.iNumMissionInstances[iMission] = iPotentialInstances[iMission]
			STRING missionName = GET_MP_MISSION_NAME(Convert_FM_Mission_Type_To_MissionID(GB_GET_FMMC_TYPE_FROM_GB_MISSION(INT_TO_ENUM(GB_MISSION, iMission))))
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_INSTANCES - Num instances for mission: ",missionName," is now: ",GlobalServerBD_FM_events.sGBWorkData.iNumMissionInstances[iMission])
		ENDIF
	ENDREPEAT

	//Cleanup session mission data if it is finished
	IF NOT bMissionActive
		IF GlobalServerBD_FM_events.sGBWorkData.iGBSessionMission != -1
		OR GlobalServerBD_FM_events.sGBWorkData.iGBSessionMissionHost != INVALID_PLAYER_INDEX()
			GlobalServerBD_FM_events.sGBWorkData.iGBSessionMission = -1
			GlobalServerBD_FM_events.sGBWorkData.iGBSessionMissionHost = INVALID_PLAYER_INDEX()
			GlobalServerBD_FM_events.sGBWorkData.bGBSessionMissionEnding = FALSE
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_SERVER_MAINTAIN_INSTANCES - Gang VS Session Mission Finished.")
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Checks if the all clients are ready to clean up the current boss mission
///    
/// RETURNS: TRUE or FALSE
///    
FUNC BOOL GB_CAN_CLIENT_CLEANUP_BOSS_MISSION()

	IF GB_IS_ANY_SESSION_GANG_BOSS_MISSION_ACTIVE()
		IF GB_GET_CATEGORY_OF_BOSS_MISSION(Convert_MissionID_To_FM_Mission_Type(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.mdID.idMission)) = GB_BOSS_MISSION_CATEGORY_GANG_VS_SESSION
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_CAN_CLIENT_CLEANUP_BOSS_MISSION - FALSE - Waiting for GB_IS_ANY_SESSION_GANG_BOSS_MISSION_ACTIVE to be FALSE.")
			RETURN FALSE
		ENDIF
	ENDIF

	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_NET_PLAYER_OK(playerID,FALSE)
			IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID)
			AND GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.mdID.idMission = GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.GB_MissionData.mdID.idMission
			AND GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.iInstanceId = GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.GB_MissionData.iInstanceId
			AND ((GB_GET_CATEGORY_OF_BOSS_MISSION(Convert_MissionID_To_FM_Mission_Type(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.GB_MissionData.mdID.idMission)) != GB_BOSS_MISSION_CATEGORY_GANG_ONLY
			AND GB_GET_CATEGORY_OF_BOSS_MISSION(Convert_MissionID_To_FM_Mission_Type(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.GB_MissionData.mdID.idMission)) != GB_BOSS_MISSION_CATEGORY_NON_AFFILIATED)
			OR GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(playerID))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE

ENDFUNC


/// PURPOSE: Checks if the players's flow should be reset due to a change in the launching player's circumstances
///    
/// RETURNS: TRUE if flow should be reset, FALSE otherwise
///    
FUNC BOOL GB_SHOULD_CLEAR_MISSION_LAUNCH_PROGRESS()

	IF GB_GET_MISSION_LAUNCH_STAGE() > eGB_MISSION_STAGE_IDLE 
	AND GB_GET_MISSION_LAUNCH_STAGE() < eGB_MISSION_STAGE_RUNNING

		IF GB_GET_LOCAL_PLAYER_MISSION_HOST() = INVALID_PLAYER_INDEX()
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_SHOULD_CLEAR_MISSION_LAUNCH_PROGRESS - TRUE - Host is invalid.")
			RETURN TRUE	
		ENDIF
		
		IF NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(GB_GET_LOCAL_PLAYER_MISSION_HOST(),TRUE)	
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_SHOULD_CLEAR_MISSION_LAUNCH_PROGRESS - TRUE - Host is no longer on mission.")
			RETURN TRUE	
		ENDIF
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC GB_CLEAR_MISSION_LAUNCH_PROGRESS(BOOL bResetLaunchStage = TRUE, BOOL bPendingMission = FALSE)

	GB_PUBLIC_CLEAR_MISSION_LAUNCH_PROGRESS(bResetLaunchStage, bPendingMission)

	IF DOES_PLAYER_HAVE_ANY_DROPOFFS_RESERVED(PLAYER_ID())
		FREEMODE_DELIVERY_SET_ALL_DROPOFFS_DISABLED(FALSE)
		FREEMODE_DELIVERY_DEACTIVATE_ALL_DROPOFFS()
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_CLEAR_MISSION_LAUNCH_PROGRESS - FREEMODE_DELIVERY_DEACTIVATE_ALL_DROPOFFS")
	ENDIF
	
ENDPROC

PROC GB_LAUNCHER_SERVER_MAINTAIN_TRIGGERS()

	IF IS_CONTRABAND_GAMEPLAY_DISABLED()
		EXIT
	ENDIF
	
	IF g_sMPTunables.bexec_disable_defend_missions 
		EXIT
	ENDIF

	IF HAS_NET_TIMER_EXPIRED(GlobalServerBD_FM_events.sGBWorkData.defendTimer,GB_GET_DEFEND_MISSION_TRIGGER_TIME())
		INT iWarehouses[ciMaxOwnedWarehouses * NUM_NETWORK_PLAYERS]
		INT iNumValidWarehouses[NUM_NETWORK_PLAYERS]
		INT iCount
	
		INT iPlayer
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iPlayer)
			IF IS_NET_PLAYER_OK(playerID, FALSE)
			
				PRINTLN("[MAGNATE_GANG_BOSS] GB_LAUNCHER_SERVER_MAINTAIN_TRIGGERS - Checking player ",iPlayer," : ",GET_PLAYER_NAME(playerID))
		
				IF DOES_PLAYER_OWN_OFFICE(playerID)
				AND GB_IS_BOSS_MISSION_AVAILABLE(playerID, FMMC_TYPE_GB_CONTRABAND_DEFEND,TRUE)
			 	AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID,TRUE)
				AND NOT IS_PLAYER_IN_MP_PROPERTY(playerID, FALSE)
				AND NOT IS_PLAYER_IN_YACHT_PROPERTY(playerID)
				AND NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(playerID)
				AND NOT IS_PLAYER_IN_BUNKER(playerID)
				AND NOT IS_PLAYER_IN_ARMORY_TRUCK(playerID)
				
					//For all owned warehouses
					INT iWarehouse
					FOR iWarehouse = 1 TO ciMaxWarehouses
						IF DOES_PLAYER_OWN_WAREHOUSE(playerID,iWarehouse)
							PRINTLN("[MAGNATE_GANG_BOSS] GB_LAUNCHER_SERVER_MAINTAIN_TRIGGERS - Warehouse = ",iWarehouse, " has ",GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE_OF_REMOTE_PLAYER(iWarehouse,playerID))
							//is warehouse nearly full
							IF GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE_OF_REMOTE_PLAYER(iWarehouse,playerID) >= CEIL(GET_WAREHOUSE_MAX_CAPACITY(iWarehouse) * g_sMPTunables.fexec_warehouse_stock_defend_threshold)
								VECTOR vCoords = GET_WAREHOUSE_SPAWN_LOCATION_TO_CHECK_FOR_DEFEND_CONTRABAND(iWarehouse)
								IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoords,0,0,0,DEFAULT,TRUE,TRUE,TRUE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,100)
									//add to list
									iWarehouses[iCount] = iWarehouse
									iCount++
									iNumValidWarehouses[iPlayer]++
									PRINTLN("[MAGNATE_GANG_BOSS] GB_LAUNCHER_SERVER_MAINTAIN_TRIGGERS - count = ",iCount)
									PRINTLN("[MAGNATE_GANG_BOSS] GB_LAUNCHER_SERVER_MAINTAIN_TRIGGERS - iNumValidWarehouses[",iPlayer,"] = ",iNumValidWarehouses[iPlayer])
								ELSE
									PRINTLN("[MAGNATE_GANG_BOSS] GB_LAUNCHER_SERVER_MAINTAIN_TRIGGERS - NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION - Not counting warehouse ",iWarehouse)
								ENDIF
							ENDIF
						ENDIF	
					ENDFOR
					
				ELSE
					PRINTLN("[MAGNATE_GANG_BOSS] GB_LAUNCHER_SERVER_MAINTAIN_TRIGGERS - Player ",GET_PLAYER_NAME(playerID)," does not meet criteria for defend mission.")
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iCount > 0
		
			//pick random from list
			INT iSelectedIndex = GET_RANDOM_INT_IN_RANGE(0,iCount)
			INT iSelectedWarehouse = iWarehouses[iSelectedIndex]
			INT iSelectedPlayer
			
			//Get owner of selected warehouse
			INT iIndex
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				iIndex += iNumValidWarehouses[iPlayer]
				IF iIndex > iSelectedIndex
					iSelectedPlayer = iPlayer
					BREAKLOOP
				ENDIF
			ENDREPEAT
			
			PRINTLN("[MAGNATE_GANG_BOSS] GB_LAUNCHER_SERVER_MAINTAIN_TRIGGERS - Selected player ",iSelectedPlayer," : ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iSelectedPlayer))," at warehouse ",iSelectedWarehouse)

			BROADCAST_GB_TRIGGER_DEFEND_MISSION(SPECIFIC_PLAYER(INT_TO_PLAYERINDEX(iSelectedPlayer)),iSelectedWarehouse)
			PRINTLN("[MAGNATE_GANG_BOSS] GB_LAUNCHER_SERVER_MAINTAIN_TRIGGERS - Launch Defend Mission at warehouse ",iSelectedWarehouse)

		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS] GB_LAUNCHER_SERVER_MAINTAIN_TRIGGERS -  No warehouses in session that meet criteria for defend mission.")
		ENDIF

		GlobalServerBD_FM_events.sGBWorkData.iDefendTime = -1
		RESET_NET_TIMER(GlobalServerBD_FM_events.sGBWorkData.defendTimer)
	ENDIF

ENDPROC

/// PURPOSE: Controls the main flow for launching boss work/challenge missions
PROC GB_LAUNCHER_CLIENT()

	INT iMission, iVariation, iSubvariation
	STRING sMissionName
	
	IF GB_SHOULD_CLEAR_MISSION_LAUNCH_PROGRESS()
		GB_CLEAR_MISSION_LAUNCH_PROGRESS()
	ENDIF
	
	// Remove phonecall launch contact after phone is off screen (can't be cleaned up exiting state as phone is still on screen for reply)
	IF MPGlobalsAmbience.sMagnateGangBossData.ePhonecallLaunchTempContact != MAX_CHARACTERS_MP
	AND NOT IS_PHONE_ONSCREEN()
		IF IS_CONTACT_IN_PHONEBOOK(GET_PHONECALL_MISSION_CHARACTER(iMission, iVariation), MULTIPLAYER_BOOK)
			REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(GET_PHONECALL_MISSION_CHARACTER(iMission, iVariation), MULTIPLAYER_BOOK)
		ENDIF
		MPGlobalsAmbience.sMagnateGangBossData.ePhonecallLaunchTempContact = MAX_CHARACTERS_MP
		PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_CLIENT - Removing char from phonebook for phone call after call finished.")
	ENDIF

	SWITCH(GB_GET_MISSION_LAUNCH_STAGE())

		//Waiting for mission to launch
		CASE eGB_MISSION_STAGE_IDLE
		

			IF GB_IS_LOCAL_PLAYER_SAFE_TO_LAUNCH_GANG_BOSS_MISSION()
				BOOL bLaunchingGangMission
				
				//If we are joining a player on a mission
				IF GB_IS_LOCAL_PLAYER_JOINING_ANY_GANG_BOSS_MISSION()
					IF GB_IS_PLAYER_LAUNCHING_ANY_GANG_BOSS_MISSION(GB_GET_LOCAL_PLAYER_GANG_BOSS_MISSION_PLAYER_TO_JOIN())
						bLaunchingGangMission = TRUE
					ELSE
						IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(GB_GET_LOCAL_PLAYER_GANG_BOSS_MISSION_PLAYER_TO_JOIN())
							GB_SET_PLAYER_LAUNCHING_GANG_BOSS_MISSION(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(GB_GET_LOCAL_PLAYER_GANG_BOSS_MISSION_PLAYER_TO_JOIN()))
							GB_SET_LOCAL_PLAYER_MISSION_HOST(GB_GET_PLAYER_MISSION_HOST(GB_GET_LOCAL_PLAYER_GANG_BOSS_MISSION_PLAYER_TO_JOIN()))
							GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PREPARE)
							bLaunchingGangMission = TRUE
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Launching mission target player is on.")
						ENDIF
					ENDIF
					
				//If we are boss launching mission from menu
				ELIF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				OR NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()	// Added to support solo launches
				OR GB_GET_CATEGORY_OF_BOSS_MISSION(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID())) = GB_BOSS_MISSION_CATEGORY_NON_AFFILIATED	//Added to support gang member launches of non-affiliated missions.
					IF GB_IS_PLAYER_LAUNCHING_ANY_GANG_BOSS_MISSION(PLAYER_ID())
						GB_SET_LOCAL_PLAYER_MISSION_HOST(PLAYER_ID())
						GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_AWAITING_SERVER)
						PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Launching mission from menu.")
						bLaunchingGangMission = TRUE
					ENDIF
					
				//If we are goon launching mission that boss is on	
				ELIF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
				AND GB_GET_CATEGORY_OF_BOSS_MISSION(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(GB_GET_LOCAL_PLAYER_GANG_BOSS())) != GB_BOSS_MISSION_CATEGORY_NON_AFFILIATED
					IF GB_IS_PLAYER_LAUNCHING_ANY_GANG_BOSS_MISSION(GB_GET_LOCAL_PLAYER_GANG_BOSS())	//If boss is launching mission	
						bLaunchingGangMission = TRUE
					ELSE
						IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(GB_GET_LOCAL_PLAYER_GANG_BOSS())	//If boss is actually running mission
							GB_SET_PLAYER_LAUNCHING_GANG_BOSS_MISSION(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							GB_SET_LOCAL_PLAYER_MISSION_HOST(GB_GET_PLAYER_MISSION_HOST(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PREPARE)
							bLaunchingGangMission = TRUE
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Launching mission my boss is on.")
						ENDIF
					ENDIF
				ENDIF
			
				//If anyone on whole session mission
				IF NOT bLaunchingGangMission
					IF GB_IS_ANY_SESSION_GANG_BOSS_MISSION_ACTIVE()
					AND NOT GB_IS_SESSION_GANG_BOSS_MISSION_ENDING()
						GB_SET_PLAYER_LAUNCHING_GANG_BOSS_MISSION(GB_GET_SESSION_CURRENT_GANG_BOSS_MISSION())
						GB_SET_LOCAL_PLAYER_MISSION_HOST(GB_GET_SESSION_MISSION_HOST())
						GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PREPARE)
						PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Launching mission as enemy gang.")
					ENDIF
				ENDIF
			ELSE
				GB_CLEAR_MISSION_LAUNCH_PROGRESS()
			ENDIF

		BREAK
		
		//Waiting for Freemode server to confirm if mission can be launched, only for player who launched mission
		CASE eGB_MISSION_STAGE_AWAITING_SERVER
		
			IF IS_BIT_SET(MPGlobalsAmbience.sMagnateGangBossData.iServerResponseBS,srGB_ACCEPT_MISSION)
			
				// If I launched the mission, tell my goons to kill any mission they are on to join this one
				IF GB_HAS_LOCAL_PLAYER_REQUESTED_MISSION_LAUNCH()
					BROADCAST_KILL_ACTIVE_BOSS_MISSION_LOCAL(ALL_PLAYERS_IN_MY_GANG(FALSE))
				ENDIF
			
				IF GB_GET_CATEGORY_OF_BOSS_MISSION(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID())) = GB_BOSS_MISSION_CATEGORY_GANG_VS_GANG
					INT iType
					IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID()) = FMMC_TYPE_BIKER_JOUST
						iType = BOSSVBOSS_JOUST
					ENDIF
					SEND_BOSSVBOSS_CHALLENGE(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.missionTarget,FALSE , iType )
					GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_AWAITING_RESPONSE)
				ELSE
					IF GB_DOES_BOSS_MISSION_NEED_ACCEPT_PHONE_CALL(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID()))
						IF GB_DOES_BOSS_MISSION_PHONE_CALL_USE_HEADSET_AUDIO(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID()))
							GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PHONE_CALL_HEADSET_AUDIO)
						ELIF GB_DOES_BOSS_MISSION_PHONE_CALL_USE_TEXT_MESSAGE(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID()))
							GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PHONE_CALL_TEXT_MESSAGE)
						ELSE
							GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PHONE_CALL)
						ENDIF
					ELSE
						GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PREPARE)
					ENDIF
				ENDIF
				MPGlobalsAmbience.sMagnateGangBossData.iServerResponseBS = 0
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - iServerResponseBS = srGB_ACCEPT_MISSION")	
			ELIF IS_BIT_SET(MPGlobalsAmbience.sMagnateGangBossData.iServerResponseBS,srGB_REJECT_MISSION)
				GB_HANDLE_BOSS_MISSION_REJECT_HELP()
				GB_CLEAR_MISSION_LAUNCH_PROGRESS()
				MPGlobalsAmbience.sMagnateGangBossData.iServerResponseBS = 0
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - iServerResponseBS = srGB_REJECT_MISSION")
			ELSE
				//Resend event if a mission is pending or the timer runs out
				IF GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_PENDING_MISSION)
				OR HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.sMagnateGangBossData.resendRequestTimer, g_sMPTunables.iSERVER_REQUEST_TIMEOUT)
					IF GB_IS_PLAYER_LAUNCHING_ANY_GANG_BOSS_MISSION(PLAYER_ID())
						GB_BOSS_REQUEST_MISSION_LAUNCH_FROM_SERVER(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID()),GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.contrabandMissionData.iMissionWarehouse)
						RESET_NET_TIMER(MPGlobalsAmbience.sMagnateGangBossData.resendRequestTimer)
						GB_CLEAR_GLOBAL_NON_BD_BIT1(eGB_GLOBAL_NON_BD_BITSET_1_PENDING_MISSION)
						PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Mission is pending or server has not responded, resending request for mission ",GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID()))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//Waiting for player to respond to Boss Vs Boss DM invite
		CASE eGB_MISSION_STAGE_AWAITING_RESPONSE
		

		
		BREAK
		
		//If the mission uses an accept/decline phone call, run it here
		CASE eGB_MISSION_STAGE_PHONE_CALL
		
			iMission = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID())
			iVariation = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION()
			iSubvariation = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()
			
			IF NOT IS_CONTACT_IN_PHONEBOOK(GET_PHONECALL_MISSION_CHARACTER(iMission, iVariation), MULTIPLAYER_BOOK)
				ADD_CONTACT_TO_PHONEBOOK(GET_PHONECALL_MISSION_CHARACTER(iMission, iVariation), MULTIPLAYER_BOOK, FALSE)
				MPGlobalsAmbience.sMagnateGangBossData.ePhonecallLaunchTempContact = GET_PHONECALL_MISSION_CHARACTER(iMission, iVariation)
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Added char to phonebook for phone call.")
			ENDIF
		
			IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_SET_UP_ACCEPTED_PHONE_CALL)
				IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_SET_UP_ACCEPT_PHONE_CALL)
					IF NOT HAS_PED_BEEN_ADDED_FOR_DIALOGUE(MPGlobalsAmbience.sMagnateGangBossData.bossMissionPedStruct, GET_PHONECALL_MISSION_SPEAKER_ID(iMission))
						ADD_PED_FOR_DIALOGUE(MPGlobalsAmbience.sMagnateGangBossData.bossMissionPedStruct, GET_PHONECALL_MISSION_SPEAKER_ID(iMission), NULL, GET_PHONECALL_MISSION_VOICE_ID(iMission))
						PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Added ped for phone call.")
					ENDIF
					IF CHAR_CALL_PLAYER_CELLPHONE_WITH_REPLIES_FORCE_ANSWER(MPGlobalsAmbience.sMagnateGangBossData.bossMissionPedStruct, GET_PHONECALL_MISSION_CHARACTER(iMission, iVariation), GET_PHONECALL_MISSION_BLOCK(iMission), GB_GET_PHONECALL_MISSION_INTRO_ROOT(iMission,iVariation), CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT_BYPASS_SLEEPMODE, GB_GET_PHONECALL_MISSION_QUESTION_STRING(iMission, iVariation), GB_GET_PHONECALL_MISSION_ACCEPT_ROOT(iMission,iVariation,iSubvariation), GB_GET_PHONECALL_MISSION_DECLINE_ROOT(iMission,iVariation))
						GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_SET_UP_ACCEPT_PHONE_CALL)
						RESET_NET_TIMER(MPGlobalsAmbience.sMagnateGangBossData.phonecallTimeoutTimer)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.sMagnateGangBossData.phonecallTimeoutTimer, 60000)
							HANG_UP_AND_PUT_AWAY_PHONE()
							GB_CLEAR_MISSION_LAUNCH_PROGRESS()
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Phone call not set up after 60 seconds - GB_CLEAR_MISSION_LAUNCH_PROGRESS")
						ENDIF
					ENDIF
				ELSE
					//Help
					IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_DONE_PHONE_CALL_INTRO_HELP)
					AND iMission = FMMC_TYPE_FMBB_PHONECALL
						IF IS_CELLPHONE_CONVERSATION_PLAYING()
							PRINT_HELP("FMBB_PCM_INTH")
							GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_DONE_PHONE_CALL_INTRO_HELP)
						ENDIF
					ELIF IS_CELLPHONE_CALL_WITH_REPLIES_WAITING_ON_USER_INPUT()
						IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_DONE_PHONE_CALL_MISSION_HELP)
							PRINT_PHONECALL_MISSION_CHOICE_HELP(iMission, iVariation)
							GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_DONE_PHONE_CALL_MISSION_HELP)
						ENDIF
					ENDIF
				
					SWITCH CHECK_RESPONSE_TO_CELLPHONE_PROMPT() 
						CASE RESPONDED_YES
							IF DOES_PHONECALL_MISSION_NEED_TRANSACTION(iMission)
								IF MPGlobalsAmbience.sMagnateGangBossData.iPhonecallLaunchTransactionStatus != GENERIC_TRANSACTION_STATE_SUCCESS
								AND MPGlobalsAmbience.sMagnateGangBossData.iPhonecallLaunchTransactionStatus != GENERIC_TRANSACTION_STATE_FAILED
									IF PROCESS_NIGHTCLUB_DJ_TRANSACTION(GET_PHONECALL_MISSION_DJ(iMission, iVariation),MPGlobalsAmbience.sMagnateGangBossData.iPhonecallLaunchTransactionStatus,GET_PLAYERS_OWNED_NIGHTCLUB(PLAYER_ID()))
										SWITCH MPGlobalsAmbience.sMagnateGangBossData.iPhonecallLaunchTransactionStatus
											CASE GENERIC_TRANSACTION_STATE_SUCCESS
												GB_RUN_PHONECALL_LAUNCH_SUCCESS_LOGIC(iMission, iVariation)
											BREAK
											CASE GENERIC_TRANSACTION_STATE_FAILED
												SET_PHONECALL_MISSION_PHONECALL_COMPLETE_STATS(iMission, iVariation, TRUE)
												IF NOT PRINT_PHONECALL_MISSION_DECLINE_HELP(iMission, iVariation)
													CLEAR_HELP(TRUE)
												ENDIF
												GB_CLEAR_MISSION_LAUNCH_PROGRESS()
												PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Phonecall response - RESPONDED_YES, but transaction failed.")
											BREAK
										ENDSWITCH
									ENDIF
								ENDIF
							ELSE
								GB_RUN_PHONECALL_LAUNCH_SUCCESS_LOGIC(iMission, iVariation)
							ENDIF
						BREAK
						CASE RESPONDED_NO
							SET_PHONECALL_MISSION_PHONECALL_COMPLETE_STATS(iMission, iVariation, TRUE)
							IF NOT PRINT_PHONECALL_MISSION_DECLINE_HELP(iMission, iVariation)
								CLEAR_HELP(TRUE)
							ENDIF
							GB_CLEAR_MISSION_LAUNCH_PROGRESS()
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Phonecall response - RESPONDED_NO")
						BREAK
						DEFAULT
							IF HAS_CELLPHONE_CALL_FINISHED()
								SET_PHONECALL_MISSION_PHONECALL_COMPLETE_STATS(iMission, iVariation, TRUE)
								GB_CLEAR_MISSION_LAUNCH_PROGRESS()
								CLEAR_HELP(TRUE)
								PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Phone call finished with no response - GB_CLEAR_MISSION_LAUNCH_PROGRESS")
							ELIF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.sMagnateGangBossData.phonecallTimeoutTimer, 60000)
								SET_PHONECALL_MISSION_PHONECALL_COMPLETE_STATS(iMission, iVariation, TRUE)
								FORCE_NEGATIVE_RESPONSE_TO_CALL_WITH_REPLIES(TRUE)
								GB_CLEAR_MISSION_LAUNCH_PROGRESS()
								CLEAR_HELP(TRUE)
								PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - No response to phone call after 60 seconds - GB_CLEAR_MISSION_LAUNCH_PROGRESS")
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ELSE
				IF HAS_CELLPHONE_CALL_FINISHED()
					GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PREPARE)
				ENDIF
			ENDIF
		
		BREAK
		
		//If the mission uses accept headset audio, run it here
		CASE eGB_MISSION_STAGE_PHONE_CALL_HEADSET_AUDIO
		
			iMission = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID())
			iVariation = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION()
			iSubvariation = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()
			
			IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_SET_UP_ACCEPTED_PHONE_CALL)
				IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_SET_UP_ACCEPT_PHONE_CALL)
					IF NOT HAS_PED_BEEN_ADDED_FOR_DIALOGUE(MPGlobalsAmbience.sMagnateGangBossData.bossMissionPedStruct, GET_PHONECALL_MISSION_SPEAKER_ID(iMission))
						ADD_PED_FOR_DIALOGUE(MPGlobalsAmbience.sMagnateGangBossData.bossMissionPedStruct, GET_PHONECALL_MISSION_SPEAKER_ID(iMission), NULL, GET_PHONECALL_MISSION_VOICE_ID(iMission))
						PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Added ped for headset audio.")
					ENDIF
					
					IF CREATE_CONVERSATION(MPGlobalsAmbience.sMagnateGangBossData.bossMissionPedStruct, GET_PHONECALL_MISSION_BLOCK(iMission), GB_GET_PHONECALL_MISSION_INTRO_ROOT(iMission, iVariation), CONV_PRIORITY_VERY_HIGH)
						GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_SET_UP_ACCEPT_PHONE_CALL)
						RESET_NET_TIMER(MPGlobalsAmbience.sMagnateGangBossData.phonecallTimeoutTimer)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.sMagnateGangBossData.phonecallTimeoutTimer, 60000)
							GB_CLEAR_MISSION_LAUNCH_PROGRESS()
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Headset audio not set up after 60 seconds - GB_CLEAR_MISSION_LAUNCH_PROGRESS")
						ENDIF
					ENDIF
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(MPGlobalsAmbience.sMagnateGangBossData.phonecallTimeoutTimer)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PRINT_HELP(GB_GET_PHONECALL_MISSION_HELP_TEXT(iMission, iVariation))
							START_NET_TIMER(MPGlobalsAmbience.sMagnateGangBossData.phonecallTimeoutTimer)
						ENDIF
					ELSE
						IF NOT HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.sMagnateGangBossData.phonecallTimeoutTimer, 60000)
							IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_DONE_PHONE_CALL_MISSION_HELP)
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GB_GET_PHONECALL_MISSION_HELP_TEXT(iMission, iVariation))
									GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_DONE_PHONE_CALL_MISSION_HELP)
								ENDIF
							ELSE
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GB_GET_PHONECALL_MISSION_HELP_TEXT(iMission, iVariation))
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM)
									
									IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CONTEXT)
									OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CONTEXT)
										PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Headset audio response - accepted.")
										CLEAR_HELP()
										
										GB_RUN_PHONECALL_LAUNCH_SUCCESS_LOGIC(iMission, iVariation)
									ENDIF
								ELSE
									PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Headset audio response - declined.")
									
									IF NOT HAS_PED_BEEN_ADDED_FOR_DIALOGUE(MPGlobalsAmbience.sMagnateGangBossData.bossMissionPedStruct, GET_PHONECALL_MISSION_SPEAKER_ID(iMission))
										ADD_PED_FOR_DIALOGUE(MPGlobalsAmbience.sMagnateGangBossData.bossMissionPedStruct, GET_PHONECALL_MISSION_SPEAKER_ID(iMission), NULL, GET_PHONECALL_MISSION_VOICE_ID(iMission))
										PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Added ped for headset audio.")
									ENDIF
									
									IF CREATE_CONVERSATION(MPGlobalsAmbience.sMagnateGangBossData.bossMissionPedStruct, GET_PHONECALL_MISSION_BLOCK(iMission), GB_GET_PHONECALL_MISSION_DECLINE_ROOT(iMission, iVariation), CONV_PRIORITY_VERY_HIGH)
										SET_PHONECALL_MISSION_PHONECALL_COMPLETE_STATS(iMission, iVariation, TRUE)
										GB_CLEAR_MISSION_LAUNCH_PROGRESS()
									ENDIF
								ENDIF
							ENDIF
						ELSE
							SET_PHONECALL_MISSION_PHONECALL_COMPLETE_STATS(iMission, iVariation, TRUE)
							GB_CLEAR_MISSION_LAUNCH_PROGRESS()
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Headset audio not set up after 60 seconds - GB_CLEAR_MISSION_LAUNCH_PROGRESS")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PREPARE)
			ENDIF
		
		BREAK
		
		CASE eGB_MISSION_STAGE_PHONE_CALL_TEXT_MESSAGE
			
			iMission = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID())
			iVariation = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION()
			iSubvariation = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()
			
			IF NOT IS_CONTACT_IN_PHONEBOOK(GET_PHONECALL_MISSION_CHARACTER(iMission, iVariation), MULTIPLAYER_BOOK)
				ADD_CONTACT_TO_PHONEBOOK(GET_PHONECALL_MISSION_CHARACTER(iMission, iVariation), MULTIPLAYER_BOOK, FALSE)
				MPGlobalsAmbience.sMagnateGangBossData.ePhonecallLaunchTempContact = GET_PHONECALL_MISSION_CHARACTER(iMission, iVariation)
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Added char to phonebook for phone call.")
			ENDIF
		
			IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_SET_UP_ACCEPTED_PHONE_CALL)
				IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_SET_UP_ACCEPT_PHONE_CALL)
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_PHONECALL_MISSION_CHARACTER(iMission, iVariation), GB_GET_PHONECALL_MISSION_INTRO_TEXT_MESSAGE(iMission, iVariation), TXTMSG_LOCKED, TXTMSG_CRITICAL, TXTMSG_DO_NOT_AUTO_UNLOCK, REPLY_IS_REQUIRED)
						GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_SET_UP_ACCEPT_PHONE_CALL)
						
						g_ShouldForceSelectionOfLatestAppItem = TRUE
						LAUNCH_CELLPHONE_APPLICATION(appTEXTS, FALSE, TRUE)
						
						RESET_NET_TIMER(MPGlobalsAmbience.sMagnateGangBossData.phonecallTimeoutTimer)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.sMagnateGangBossData.phonecallTimeoutTimer, 60000)
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Text message not set up after 60 seconds - GB_CLEAR_MISSION_LAUNCH_PROGRESS")
							
							GB_CLEAR_MISSION_LAUNCH_PROGRESS()
						ENDIF
					ENDIF
				ELSE
					IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_DONE_PHONE_CALL_MISSION_HELP)
						PRINT_PHONECALL_MISSION_CHOICE_HELP(iMission, iVariation)
						GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_DONE_PHONE_CALL_MISSION_HELP)
					ENDIF
					
					SWITCH GET_TEXT_MESSAGE_REPLY_STATUS(GB_GET_PHONECALL_MISSION_INTRO_TEXT_MESSAGE(iMission, iVariation))
						CASE REPLIED_YES
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Text message response - REPLIED_YES")
							
							CLEAR_HELP()
							HANG_UP_AND_PUT_AWAY_PHONE()
							UNLOCK_TEXT_MESSAGE_BY_LABEL(GB_GET_PHONECALL_MISSION_INTRO_TEXT_MESSAGE(iMission, iVariation))
							DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER(GB_GET_PHONECALL_MISSION_INTRO_TEXT_MESSAGE(iMission, iVariation))
							
							GB_RUN_PHONECALL_LAUNCH_SUCCESS_LOGIC(iMission, iVariation)
						BREAK
						
						CASE REPLIED_NO
							PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Text message response - REPLIED_NO")
							
							CLEAR_HELP()
							HANG_UP_AND_PUT_AWAY_PHONE()
							UNLOCK_TEXT_MESSAGE_BY_LABEL(GB_GET_PHONECALL_MISSION_INTRO_TEXT_MESSAGE(iMission, iVariation))
							DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER(GB_GET_PHONECALL_MISSION_INTRO_TEXT_MESSAGE(iMission, iVariation))
							
							SET_PHONECALL_MISSION_PHONECALL_COMPLETE_STATS(iMission, iVariation, TRUE)
							GB_CLEAR_MISSION_LAUNCH_PROGRESS()
						BREAK
						
						DEFAULT
							IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.sMagnateGangBossData.phonecallTimeoutTimer, 30000)
								PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - No response to text message after 30 seconds - GB_CLEAR_MISSION_LAUNCH_PROGRESS")
								
								CLEAR_HELP()
								HANG_UP_AND_PUT_AWAY_PHONE()
								UNLOCK_TEXT_MESSAGE_BY_LABEL(GB_GET_PHONECALL_MISSION_INTRO_TEXT_MESSAGE(iMission, iVariation))
								DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER(GB_GET_PHONECALL_MISSION_INTRO_TEXT_MESSAGE(iMission, iVariation))
								
								SET_PHONECALL_MISSION_PHONECALL_COMPLETE_STATS(iMission, iVariation, TRUE)
								GB_CLEAR_MISSION_LAUNCH_PROGRESS()
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ELSE
				GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PREPARE)
			ENDIF
		
		BREAK
		
		//Prepare the data needed to launch the mission
		CASE eGB_MISSION_STAGE_PREPARE
			
			//Set the mission data
 			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.mdID.idMission = Convert_FM_Mission_Type_To_MissionID(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID()))
			MPGlobalsAmbience.sMagnateGangBossData.activeGBMission = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.mdID.idMission
			PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - eGB_MISSION_STAGE_PREPARE - mission = ",GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.mdID.idMission)
						
			IF GB_GET_CATEGORY_OF_BOSS_MISSION(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID())) = GB_BOSS_MISSION_CATEGORY_GANG_VS_SESSION
				GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.iInstanceId = -1
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - eGB_MISSION_STAGE_PREPARE - Setting instance to -1 ")
			ELSE
				GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.iInstanceId = NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_MISSION_HOST())
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - eGB_MISSION_STAGE_PREPARE - Setting instance to gang boss = ",GET_PLAYER_NAME(GB_GET_LOCAL_PLAYER_MISSION_HOST()))
			ENDIF
			
			GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_LAUNCH)
			
		BREAK
		
		//Attempt to launch the mission
		CASE eGB_MISSION_STAGE_LAUNCH
		
			sMissionName = GET_MP_MISSION_NAME(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.mdID.idMission)
			BOOL bLaunch
			
			//Attempt to launch, and move on once active
			IF NOT NETWORK_IS_SCRIPT_ACTIVE(sMissionName, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.iInstanceId, TRUE)
				IF NOT NETWORK_IS_SCRIPT_ACTIVE(sMissionName,GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.iInstanceId)
					IF GB_GET_LOCAL_PLAYER_MISSION_HOST() = PLAYER_ID()
						bLaunch = TRUE
						PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Script not active and I am launcher - bLaunch = TRUE")
					ENDIF
				ELSE
					IF NETWORK_GET_NUM_SCRIPT_PARTICIPANTS(sMissionName,GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.iInstanceId) < NUM_NETWORK_PLAYERS
						bLaunch = TRUE
						PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Script active and not at max participants - bLaunch = TRUE")
					ENDIF
				ENDIF
			ENDIF	
			
			IF bLaunch
				IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData)
					PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Launching ",sMissionName)
				ENDIF
			ENDIF
			
			IF NETWORK_IS_SCRIPT_ACTIVE(sMissionName, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.iInstanceId, TRUE)
				MPGlobalsAmbience.sMagnateGangBossData.iServerResponseBS = 0
				GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_RUNNING)
			ENDIF
			
		BREAK
		
		//Mission is now running, check for cleanup conditions
		CASE eGB_MISSION_STAGE_RUNNING
		
			sMissionName = GET_MP_MISSION_NAME(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.mdID.idMission)
			
			//If boss is launching a gang only mission while an gang vs session mission is active
			IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
			AND GB_IS_PLAYER_LAUNCHING_ANY_GANG_BOSS_MISSION(PLAYER_ID())
			AND IS_BIT_SET(MPGlobalsAmbience.sMagnateGangBossData.iServerResponseBS,srGB_ACCEPT_MISSION)
				//Kill locally
				MPGlobalsAmbience.sMagnateGangBossData.bKillActiveBossMissionLocal = TRUE
				PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_MAINTAIN_BOSS_WORK_LAUNCHING - Attempting to kill current event ",sMissionName," and launch ",GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID()))
				CLEAR_BIT(MPGlobalsAmbience.sMagnateGangBossData.iServerResponseBS,srGB_ACCEPT_MISSION)
				GB_SET_GLOBAL_NON_BD_BIT1(eGB_GLOBAL_NON_BD_BITSET_1_PENDING_MISSION)
			ENDIF
			
			//If script not active locally anymore, move to cleanup
			IF NOT NETWORK_IS_SCRIPT_ACTIVE(sMissionName,GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GB_MissionData.iInstanceId,TRUE)
				
				IF GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_PENDING_MISSION)
					//Send kill event to goons
					BROADCAST_KILL_ACTIVE_BOSS_MISSION_LOCAL(ALL_PLAYERS_IN_MY_GANG(FALSE))
				ENDIF
				
				GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_CLEANUP)
			ENDIF
		BREAK
		
		//Cleanup the mission data and restart flow
		CASE eGB_MISSION_STAGE_CLEANUP
		
			BOOL bPendingMission
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
				IF GB_IS_PLAYER_LAUNCHING_ANY_GANG_BOSS_MISSION(GB_GET_LOCAL_PLAYER_GANG_BOSS())	//Boss is launching new mission
					bPendingMission = TRUE
					PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_CLIENT - bPendingMission = TRUE - Gang boss mission is ",GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
				ENDIF
			ELSE
				IF GB_IS_PLAYER_LAUNCHING_ANY_GANG_BOSS_MISSION(PLAYER_ID())	//Local player is launching new mission
					bPendingMission = TRUE
					PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_CLIENT - bPendingMission = TRUE - Local player mission is ",GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(PLAYER_ID()))
				ENDIF
			ENDIF
		
			IF GB_GET_LOCAL_PLAYER_MISSION_HOST() = INVALID_PLAYER_INDEX()	
			OR NOT NETWORK_IS_PLAYER_ACTIVE(GB_GET_LOCAL_PLAYER_MISSION_HOST())					//If no host
			OR GB_GET_LOCAL_PLAYER_MISSION_HOST() = PLAYER_ID()									//If I am host
			OR NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(GB_GET_LOCAL_PLAYER_MISSION_HOST())	//If host has cleaned up
			OR (GB_IS_PLAYER_ON_GANG_BOSS_MISSION_OF_CATEGORY(GB_GET_LOCAL_PLAYER_MISSION_HOST(),GB_BOSS_MISSION_CATEGORY_GANG_ONLY)
			AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST()))
			OR GB_IS_PLAYER_ON_GANG_BOSS_MISSION_OF_CATEGORY(GB_GET_LOCAL_PLAYER_MISSION_HOST(),GB_BOSS_MISSION_CATEGORY_NON_AFFILIATED)
			OR bPendingMission
				GB_CLEAR_MISSION_LAUNCH_PROGRESS(FALSE,bPendingMission)
				
				IF GB_CAN_CLIENT_CLEANUP_BOSS_MISSION()
				OR bPendingMission
					GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_IDLE)
					PRINTLN("[MAGNATE_GANG_BOSS][GB_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] GB_LAUNCHER_CLIENT - Cleanup done.")
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

PROC GB_LAUNCHER_SERVER(INT &iStagger)

	GB_LAUNCHER_SERVER_MAINTAIN_REQUESTS(iStagger)
	GB_LAUNCHER_SERVER_MAINTAIN_INSTANCES()
	GB_LAUNCHER_SERVER_MAINTAIN_TRIGGERS()

ENDPROC

PROC GB_MAINTAIN_GANG_BOSS_LAUNCHER(STRUCT_MAGNATE_GANG_BOSS_LOCAL_DATA &sMagnateGangBossLocalData)

	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		EXIT
	ENDIF
	
	IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_COMPLETED_INITIAL_GANG_BOSS_DATA_SETUP)
		EXIT
	ENDIF

	GB_LAUNCHER_CLIENT()

	// Job launching
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		GB_LAUNCHER_SERVER(sMagnateGangBossLocalData.iServerStaggeredPlayer)
	ENDIF 
	
ENDPROC

