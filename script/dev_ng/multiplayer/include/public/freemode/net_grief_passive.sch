//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_grief_passive.sch																			//
// Description: System for managing the local player becoming passive against players who grief them			//
// Written by:  Steve Tiley																						//
// Date: 		05/01/2016																						//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


USING "rage_builtins.sch"
USING "globals.sch"
USING "mp_globals_fm.sch"
USING "net_include.sch"
USING "net_ticker_logic.sch"


STRUCT GRIEF_PASSIVE_DATA_STRUCT
	INT iGriefTimerLoop = 0
	INT iGriefPlayerLoop = 0
	INT iPassiveAvailableBitSet
	
	BOOL bDoLoop
ENDSTRUCT

FUNC BOOL IS_GRIEF_PASSIVE_ENABLED()
	RETURN g_sMPTunables.bGriefPassiveEnabled
ENDFUNC

FUNC INT GET_GRIEF_WINDOW_TIME()
	RETURN g_sMPTunables.iGriefWindowTime
ENDFUNC

FUNC INT GET_GRIEF_PASSIVE_DURATION()
	RETURN g_sMPTunables.iGriefPassiveDuration
ENDFUNC

FUNC INT GET_NUM_GRIEF_KILLS_REQUIRED()
	RETURN g_sMPTunables.iGriefNumKills
ENDFUNC

/// PURPOSE: Check if the local player is playing one of the FM events modes
FUNC BOOL IS_PLAYER_PLAYING_AGGRESSIVE_FREEMODE_EVENT(PLAYER_INDEX player)

	IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(player) = FMMC_TYPE_MOVING_TARGET	
	OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(player) = FMMC_TYPE_HOLD_THE_WHEEL	
	OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(player) = FMMC_TYPE_HOT_PROPERTY	
	OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(player) = FMMC_TYPE_DEAD_DROP	
	OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(player) = FMMC_TYPE_KING_CASTLE
	OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(player) = FMMC_TYPE_CRIMINAL_DAMAGE
	OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(player) = FMMC_TYPE_KILL_LIST	
	OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(player) = FMMC_TYPE_HUNT_THE_BEAST
	OR FM_EVENT_IS_PLAYER_CURRENT_FM_EVENT_A_BUSINESS_BATTLE(player)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Check if the local player should track griefers
FUNC BOOL CAN_PLAYER_USE_GRIEF_PASSIVE()
	#IF IS_DEBUG_BUILD
	BOOL bPrintSpam = GET_COMMANDLINE_PARAM_EXISTS("sc_CAN_PLAYER_USE_GRIEF_PASSIVE")
	#ENDIF

	IF GlobalServerBD_FM.currentBounties[NATIVE_TO_INT(PLAYER_ID())].bTargeted = TRUE
		#IF IS_DEBUG_BUILD
		IF bPrintSpam PRINTLN("[GO_GHOSTED] - CAN_PLAYER_USE_GRIEF_GHOSTING - FALSE - IS_BOUNTY_SET_ON_PLAYER(PLAYER_INDEX playerID)") ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	IF IS_MP_PASSIVE_MODE_ENABLED()
		IF NOT IS_TEMP_PASSIVE_MODE_ENABLED()
			#IF IS_DEBUG_BUILD
			IF bPrintSpam PRINTLN("[GO_GHOSTED] - CAN_PLAYER_USE_GRIEF_GHOSTING - FALSE - IS_MP_PASSIVE_MODE_ENABLED()") ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_bFM_ON_IMPROMPTU_DEATHMATCH = TRUE
		#IF IS_DEBUG_BUILD
		IF bPrintSpam PRINTLN("[GO_GHOSTED] - CAN_PLAYER_USE_GRIEF_GHOSTING - FALSE - g_bFM_ON_IMPROMPTU_DEATHMATCH = TRUE") ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bPrintSpam PRINTLN("[GO_GHOSTED] - CAN_PLAYER_USE_GRIEF_GHOSTING - FALSE - GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())") ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bPrintSpam PRINTLN("[GO_GHOSTED] - CAN_PLAYER_USE_GRIEF_GHOSTING - FALSE - GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())") ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		#IF IS_DEBUG_BUILD
		IF bPrintSpam PRINTLN("[GO_GHOSTED] - CAN_PLAYER_USE_GRIEF_GHOSTING - FALSE - GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()") ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_JOINING_A_GANG(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bPrintSpam PRINTLN("[GO_GHOSTED] - CAN_PLAYER_USE_GRIEF_GHOSTING - FALSE - GB_IS_PLAYER_JOINING_A_GANG(PLAYER_ID())") ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bPrintSpam PRINTLN("[GO_GHOSTED] - CAN_PLAYER_USE_GRIEF_GHOSTING - FALSE - FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())") ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
		IF (FM_EVENT_PLAYER_IS_ON_AGGRESSVE_FM_CHALLENGE(PLAYER_ID())
		OR IS_PLAYER_PLAYING_AGGRESSIVE_FREEMODE_EVENT(PLAYER_ID()))
		AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
			#IF IS_DEBUG_BUILD
			IF bPrintSpam PRINTLN("[GO_GHOSTED] - CAN_PLAYER_USE_GRIEF_GHOSTING - FALSE - FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID()) + FM event is aggressive event/challenge") ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_AN_ANIMAL()
		#IF IS_DEBUG_BUILD
		IF bPrintSpam PRINTLN("[GO_GHOSTED] - CAN_PLAYER_USE_GRIEF_GHOSTING - FALSE - IS_PLAYER_AN_ANIMAL(PLAYER_ID())") ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_BE_CLASSED_AS_GRIEFER(PLAYER_INDEX thisPlayer)
	// My killer is critical to boss work
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(thisPlayer)
		PRINTLN("[GO_GHOSTED] - CAN_PLAYER_BE_CLASSED_AS_GRIEFER - FALSE - Player is critical to boss work and potentially defending objective")
		RETURN FALSE
	ENDIF
	
	// My killer is a permanent participant in boss work
	IF GB_IS_PLAYER_PERMANENT_PARTICIPANT(thisPlayer)
		PRINTLN("[GO_GHOSTED] - CAN_PLAYER_BE_CLASSED_AS_GRIEFER - FALSE - Player is permanent to boss work and potentially defending objective")
		RETURN FALSE
	ENDIF
	
	// My killer is critical to a freemode event
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(thisPlayer)
		PRINTLN("[GO_GHOSTED] - CAN_PLAYER_BE_CLASSED_AS_GRIEFER - FALSE - Player is critical to freemode event and potentially defending objective")
		RETURN FALSE
	ENDIF
	
	// My killer is a permanent participant in a freemode event
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(thisPlayer)
		IF (FM_EVENT_PLAYER_IS_ON_AGGRESSVE_FM_CHALLENGE(thisPlayer)
		OR IS_PLAYER_PLAYING_AGGRESSIVE_FREEMODE_EVENT(thisPlayer))
		AND NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(thisPlayer)
			PRINTLN("[GO_GHOSTED] - CAN_PLAYER_BE_CLASSED_AS_GRIEFER - FALSE - Player is permanent to freemode event and potentially defending objective")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// I've requested a 1v1 death match with this player
	IF g_sImpromptuVars.playerImpromptuRival != INVALID_PLAYER_INDEX()
		IF g_sImpromptuVars.playerImpromptuRival = thisPlayer
		AND IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_ACTIVE_REQUEST_AWAITING_REPLY)
			PRINTLN("[GO_GHOSTED] - CAN_PLAYER_BE_CLASSED_AS_GRIEFER - FALSE - I have requested a 1v1 deathmatch with this player")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// My killer is an animal
	IF IS_PLAYER_AN_ANIMAL(thisPlayer)
		PRINTLN("[GO_GHOSTED] - CAN_PLAYER_BE_CLASSED_AS_GRIEFER - FALSE - Player is an animal")
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the player is on a job type where players are allowed to be ghosted even if they are critical or permanent
/// PARAMS:
///    piGriefer - the player being processed
/// RETURNS:
///    
FUNC BOOL IS_GRIEF_PASSIVE_PROTECTION_PERMITTED_AGAINST_PLAYER_ON_JOB(PLAYER_INDEX piGriefer)

	SWITCH GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piGriefer)
		CASE FMMC_TYPE_GB_TERMINATE
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

//PURPOSE: Check if the remote player should no longer be ghosted e.g. local entered passive, joined freemode event.
FUNC BOOL SHOULD_GRIEF_PASSIVE_END(PLAYER_INDEX piGriefer)
	
	IF NOT CAN_PLAYER_USE_GRIEF_PASSIVE()
		#IF IS_DEBUG_BUILD
		PRINTLN("[GO_GHOSTED] - SHOULD_GRIEF_PASSIVE_END - TRUE - NOT CAN_PLAYER_USE_GRIEF_PASSIVE()")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(piGriefer)
	AND NOT IS_GRIEF_PASSIVE_PROTECTION_PERMITTED_AGAINST_PLAYER_ON_JOB(piGriefer)
		#IF IS_DEBUG_BUILD
		PRINTLN("[GO_GHOSTED] - SHOULD_GRIEF_PASSIVE_END - TRUE - GB_IS_PLAYER_CRITICAL_TO_JOB(piGriefer)")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_PERMANENT_PARTICIPANT(piGriefer)
	AND NOT IS_GRIEF_PASSIVE_PROTECTION_PERMITTED_AGAINST_PLAYER_ON_JOB(piGriefer)
		#IF IS_DEBUG_BUILD
		PRINTLN("[GO_GHOSTED] - SHOULD_GRIEF_PASSIVE_END - TRUE - GB_IS_PLAYER_PERMANENT_PARTICIPANT(piGriefer)")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(piGriefer)
		#IF IS_DEBUG_BUILD
		PRINTLN("[GO_GHOSTED] - SHOULD_GRIEF_PASSIVE_END - TRUE - FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(piGriefer)")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(piGriefer)
		IF (FM_EVENT_PLAYER_IS_ON_AGGRESSVE_FM_CHALLENGE(piGriefer)
		OR IS_PLAYER_PLAYING_AGGRESSIVE_FREEMODE_EVENT(piGriefer))
		AND FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(piGriefer)
			#IF IS_DEBUG_BUILD
			PRINTLN("[GO_GHOSTED] - SHOULD_GRIEF_PASSIVE_END - TRUE - FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(piGriefer) - not an aggressive event")
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NATIVE_TO_INT(piGriefer) != -1
		IF GlobalplayerBD_FM_3[NATIVE_TO_INT(piGriefer)].bInPassive
		AND NOT GlobalplayerBD_FM_3[NATIVE_TO_INT(piGriefer)].bInTempPassive
			#IF IS_DEBUG_BUILD
			PRINTLN("[GO_GHOSTED] - SHOULD_GRIEF_PASSIVE_END - TRUE - GlobalplayerBD_FM_3[NATIVE_TO_INT(piGriefer)].bInPassive")
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	// My killer is an animal
	IF IS_PLAYER_AN_ANIMAL(piGriefer)
		#IF IS_DEBUG_BUILD
		PRINTLN("[GO_GHOSTED] - SHOULD_GRIEF_PASSIVE_END - TRUE - IS_PLAYER_AN_ANIMAL(piGriefer)")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_ALREADY_GHOSTED_TO_KILLER()
	IF NETWORK_IS_PLAYER_ACTIVE(MPGlobals.g_KillStrip.killerData.killerID)
		IF MPGlobals.g_KillStrip.killerData.killerID != INVALID_PLAYER_INDEX()
		AND MPGlobals.g_KillStrip.killerData.killerID != PLAYER_ID()
			IF HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID)].GriefDurationTimer)
			AND NOT HAS_NET_TIMER_EXPIRED(g_sGriefPassivePlayer[NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID)].GriefDurationTimer, GET_GRIEF_PASSIVE_DURATION())
				PRINTLN("[GO_GHOSTED] - AM_I_ALREADY_GHOSTED_TO_PLAYER - TRUE, player is already ghosted to ", GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the killstrip player has killed the local player enough times to class as a griefer
/// RETURNS:
///    
FUNC BOOL IS_KILLER_A_GRIEFER()
	IF MPGlobals.g_KillStrip.killerData.killerID != INVALID_PLAYER_INDEX()
	AND MPGlobals.g_KillStrip.killerData.killerID != PLAYER_ID()
		IF g_sGriefPassivePlayer[NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID)].iKills >= GET_NUM_GRIEF_KILLS_REQUIRED()
			IF g_sImpromptuVars.playerImpromptuRival = MPGlobals.g_KillStrip.killerData.killerID
			AND IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_ACTIVE_REQUEST_AWAITING_REPLY)
				PRINTLN("[GO_GHOSTED] - IS_KILLER_A_GRIEFER - FALSE - Requested a 1v1 DM with this player")
				RETURN FALSE
			ELSE
				PRINTLN("[GO_GHOSTED] - IS_KILLER_A_GRIEFER - TRUE - ", GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID), " is a griefer")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	PRINTLN("[GO_GHOSTED] - IS_KILLER_A_GRIEFER - FALSE")
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_A_POTENTIAL_GRIEFER_OR_ACTUAL_GRIEFER(PLAYER_INDEX thisPlayer)
	IF NETWORK_IS_PLAYER_ACTIVE(thisPlayer)
		IF thisPlayer != INVALID_PLAYER_INDEX()
		AND thisPlayer != PLAYER_ID()
			IF g_sGriefPassivePlayer[NATIVE_TO_INT(thisPlayer)].iKills > 0
				PRINTLN("[GO_GHOSTED] - IS_THIS_PLAYER_A_GRIEFER - TRUE - ", GET_PLAYER_NAME(thisPlayer), " is a griefer")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[GO_GHOSTED] - IS_THIS_PLAYER_A_GRIEFER - NETWORK_IS_PLAYER_ACTIVE - FALSE")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LOCAL_PLAYER_GONE_GHOSTED_TO_THIS_PLAYER(PLAYER_INDEX playerID)
	IF NATIVE_TO_INT(playerID) != -1
		RETURN HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[NATIVE_TO_INT(playerID)].GriefDurationTimer)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_GHOSTED_TO_ALL_MEMBERS_OF_THIS_GANG(PLAYER_INDEX gangBossID)
	IF NOT HAS_LOCAL_PLAYER_GONE_GHOSTED_TO_THIS_PLAYER(gangBossID)
		RETURN FALSE
	ENDIF
	
	INT iLoop
	REPEAT GB_GET_NUM_GOONS_IN_PLAYER_GANG(gangBossID) iLoop
		IF GB_GET_GANG_GOON_AT_INDEX(gangBossID, iLoop) != INVALID_PLAYER_INDEX()
			IF NOT HAS_LOCAL_PLAYER_GONE_GHOSTED_TO_THIS_PLAYER(GB_GET_GANG_GOON_AT_INDEX(gangBossID, iLoop))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_GHOSTED_TO_A_MEMBER_OF_THIS_GANG(PLAYER_INDEX gangBossID)
	IF HAS_LOCAL_PLAYER_GONE_GHOSTED_TO_THIS_PLAYER(gangBossID)
		RETURN TRUE
	ENDIF
	
	INT iLoop
	REPEAT GB_GET_NUM_GOONS_IN_PLAYER_GANG(gangBossID) iLoop
		IF GB_GET_GANG_GOON_AT_INDEX(gangBossID, iLoop) != INVALID_PLAYER_INDEX()
			IF HAS_LOCAL_PLAYER_GONE_GHOSTED_TO_THIS_PLAYER(GB_GET_GANG_GOON_AT_INDEX(gangBossID, iLoop))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC REFRESH_GO_GHOSTED_DURATION_AGAINST_GANG(PLAYER_INDEX gangBossID)
	IF HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[NATIVE_TO_INT(gangBossID)].GriefDurationTimer)
		REINIT_NET_TIMER(g_sGriefPassivePlayer[NATIVE_TO_INT(gangBossID)].GriefDurationTimer)
		PRINTLN("[GO_GHOSTED] - REFRESH_GO_GHOSTED_DURATION_AGAINST_GANG - Reinit net timer for boss ", GET_PLAYER_NAME(gangBossID))
	ENDIF
	
	INT iLoop
	PLAYER_INDEX piGoon
	REPEAT GB_GET_NUM_GOONS_IN_PLAYER_GANG(gangBossID) iLoop
		piGoon = GB_GET_GANG_GOON_AT_INDEX(gangBossID, iLoop)
		IF piGoon != INVALID_PLAYER_INDEX()
			IF HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[NATIVE_TO_INT(piGoon)].GriefDurationTimer)
				REINIT_NET_TIMER(g_sGriefPassivePlayer[NATIVE_TO_INT(piGoon)].GriefDurationTimer)
				PRINTLN("[GO_GHOSTED] - REFRESH_GO_GHOSTED_DURATION_AGAINST_GANG - Reinit net timer for goon ", GET_PLAYER_NAME(piGoon), " at index ", iLoop)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//PURPOSE: Sets the player to passive against a single player.
PROC SET_PLAYER_TO_PASSIVE_AGAINST_PLAYER(PLAYER_INDEX piGriefer, BOOL bPrintTicker = TRUE)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_MP_PASSIVE_MODE_ENABLED()
			IF NATIVE_TO_INT(piGriefer) != -1
				IF NETWORK_IS_PLAYER_ACTIVE(piGriefer)
					SET_REMOTE_PLAYER_AS_GHOST(piGriefer, TRUE)
					START_NET_TIMER(g_sGriefPassivePlayer[NATIVE_TO_INT(piGriefer)].GriefDurationTimer)
					
					IF bPrintTicker
						PRINT_TICKER_WITH_PLAYER_NAME("GRIEF_TICK_VIC", piGriefer)
					ENDIF
					
					SCRIPT_EVENT_DATA_TICKER_MESSAGE tickerData
					tickerData.playerID = PLAYER_ID()
					tickerData.TickerEvent = TICKER_EVENT_GRIEF_PASSIVE_PLAYER_GONE_PASSIVE
					BROADCAST_TICKER_EVENT(tickerData, SPECIFIC_PLAYER(piGriefer))
				
					IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_ACTIVE_REQUEST_AWAITING_REPLY)
						PRINTLN("[GO_GHOSTED] - SET_PLAYER_TO_PASSIVE_AGAINST_PLAYER - player was awaiting an impromput DM, cancelling this request")
						SET_BIT(g_sImpromptuVars.iBitSet, ciIMPROMPTU_KILL_ALL_REQUESTS)
					ENDIF
					
					GAMER_HANDLE gHandle = GET_GAMER_HANDLE_PLAYER(piGriefer)
					SEND_METRIC_GHOSTING_TO_PLAYER(gHandle)
					
					PRINTLN("[GO_GHOSTED] - SET_PLAYER_TO_PASSIVE_AGAINST_PLAYER called by ", GET_THIS_SCRIPT_NAME(), " - TARGETTED PASSIVE MODE - SET_REMOTE_PLAYER_AS_GHOST(", GET_PLAYER_NAME(piGriefer), ", TRUE)")
					DEBUG_PRINTCALLSTACK()
				ENDIF
			ELSE
				PRINTLN("[GO_GHOSTED] - SET_PLAYER_TO_PASSIVE_AGAINST_PLAYER called by ", GET_THIS_SCRIPT_NAME(), " - TARGETTED PASSIVE MODE - piGriefer <> INVALID_PLAYER_INDEX()")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_PLAYER_TO_PASSIVE_AGAINST_PLAYERS_GANG(PLAYER_INDEX piGriefer)
	INT iLoop
	PLAYER_INDEX piGrieferBoss = GB_GET_THIS_PLAYER_GANG_BOSS(piGriefer)
	
	// Do a ticker if we're not already ghosted to anyone in the gang
	IF NOT IS_LOCAL_PLAYER_GHOSTED_TO_A_MEMBER_OF_THIS_GANG(piGrieferBoss)
		PRINT_TICKER_WITH_ONE_CUSTOM_STRING_WITH_HUD_COLOUR("GRIEF_TICK_VIC", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(piGriefer), GB_GET_PLAYER_GANG_HUD_COLOUR(piGriefer))
	ENDIF
	
	// Ghost player to the griefer's boss
	IF NOT HAS_LOCAL_PLAYER_GONE_GHOSTED_TO_THIS_PLAYER(piGrieferBoss)
		SET_PLAYER_TO_PASSIVE_AGAINST_PLAYER(piGrieferBoss, FALSE)
		PRINTLN("[GO_GHOSTED] - SET_PLAYER_TO_PASSIVE_AGAINST_PLAYERS_GANG called by ", GET_THIS_SCRIPT_NAME(), " - TARGETTED PASSIVE MODE - Calling SET_PLAYER_TO_PASSIVE_AGAINST_PLAYER for boss ", GET_PLAYER_NAME(piGrieferBoss))
	ELSE
		PRINTLN("[GO_GHOSTED] - SET_PLAYER_TO_PASSIVE_AGAINST_PLAYERS_GANG - TARGETTED PASSIVE MODE - Player is already ghosted to boss ", GET_PLAYER_NAME(piGrieferBoss))
	ENDIF
	
	// Ghost player to the griefer's boss' goons
	REPEAT GB_GET_NUM_GOONS_IN_PLAYER_GANG(piGrieferBoss) iLoop
		IF GB_GET_GANG_GOON_AT_INDEX(piGrieferBoss, iLoop) != INVALID_PLAYER_INDEX()
			IF NOT HAS_LOCAL_PLAYER_GONE_GHOSTED_TO_THIS_PLAYER(GB_GET_GANG_GOON_AT_INDEX(piGrieferBoss, iLoop))
				PRINTLN("[GO_GHOSTED] - SET_PLAYER_TO_PASSIVE_AGAINST_PLAYERS_GANG called by ", GET_THIS_SCRIPT_NAME(), " - TARGETTED PASSIVE MODE - Calling SET_PLAYER_TO_PASSIVE_AGAINST_PLAYER for goon ", GET_PLAYER_NAME(GB_GET_GANG_GOON_AT_INDEX(piGrieferBoss, iLoop)), " at index ", iLoop)
				SET_PLAYER_TO_PASSIVE_AGAINST_PLAYER(GB_GET_GANG_GOON_AT_INDEX(piGrieferBoss, iLoop), FALSE)
			ELSE
				PRINTLN("[GO_GHOSTED] - SET_PLAYER_TO_PASSIVE_AGAINST_PLAYERS_GANG - TARGETTED PASSIVE MODE - Player is already passive to goon ", GET_PLAYER_NAME(GB_GET_GANG_GOON_AT_INDEX(piGrieferBoss, iLoop)), " at index ", iLoop)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Refresh the duration of ghosted against all members
	REFRESH_GO_GHOSTED_DURATION_AGAINST_GANG(piGrieferBoss)
	
ENDPROC

//PURPOSE: Stops the player from being passive against a single player.
PROC END_PLAYER_PASSIVE_AGAINST_PLAYER(PLAYER_INDEX thisPlayer, BOOL bPrintTicker = TRUE)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF thisPlayer <> INVALID_PLAYER_INDEX()
				SET_REMOTE_PLAYER_AS_GHOST(thisPlayer, FALSE)
				RESET_NET_TIMER(g_sGriefPassivePlayer[NATIVE_TO_INT(thisPlayer)].GriefDurationTimer)
				
				IF bPrintTicker
					PRINT_TICKER_WITH_PLAYER_NAME("GRIEF_TICK_VIC1", thisPlayer)
				ENDIF
				
				PRINTLN("[GO_GHOSTED] - END_PLAYER_PASSIVE_AGAINST_PLAYER called by ", GET_THIS_SCRIPT_NAME(), " - TARGETTED PASSIVE MODE - SET_REMOTE_PLAYER_AS_GHOST(", GET_PLAYER_NAME(thisPlayer), ", FALSE)")
				DEBUG_PRINTCALLSTACK()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC END_PLAYER_PASSIVE_AGAINST_PLAYERS_GANG(PLAYER_INDEX thisPlayer)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF thisPlayer <> INVALID_PLAYER_INDEX()
			
				PLAYER_INDEX piGrieferBoss = GB_GET_THIS_PLAYER_GANG_BOSS(thisPlayer)
	
				// Do a ticker if we're not already ghosted to anyone in the gang
				PRINT_TICKER_WITH_ONE_CUSTOM_STRING_WITH_HUD_COLOUR("GRIEF_TICK_VIC1", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(piGrieferBoss), GB_GET_PLAYER_GANG_HUD_COLOUR(piGrieferBoss))
				
				// Ghost player to the griefer's boss
				IF HAS_LOCAL_PLAYER_GONE_GHOSTED_TO_THIS_PLAYER(piGrieferBoss)
					END_PLAYER_PASSIVE_AGAINST_PLAYER(piGrieferBoss, FALSE)
					PRINTLN("[GO_GHOSTED] - END_PLAYER_PASSIVE_AGAINST_PLAYERS_GANG called by ", GET_THIS_SCRIPT_NAME(), " - TARGETTED PASSIVE MODE - Calling END_PLAYER_PASSIVE_AGAINST_PLAYER for boss ", GET_PLAYER_NAME(piGrieferBoss))
				ELSE
					PRINTLN("[GO_GHOSTED] - END_PLAYER_PASSIVE_AGAINST_PLAYERS_GANG - TARGETTED PASSIVE MODE - Player was not ghosted to boss ", GET_PLAYER_NAME(piGrieferBoss))
				ENDIF
				
				// Ghost player to the griefer's boss' goons
				INT iLoop
				REPEAT GB_GET_NUM_GOONS_IN_PLAYER_GANG(piGrieferBoss) iLoop
					IF GB_GET_GANG_GOON_AT_INDEX(piGrieferBoss, iLoop) != INVALID_PLAYER_INDEX()
						IF HAS_LOCAL_PLAYER_GONE_GHOSTED_TO_THIS_PLAYER(GB_GET_GANG_GOON_AT_INDEX(piGrieferBoss, iLoop))
							PRINTLN("[GO_GHOSTED] - END_PLAYER_PASSIVE_AGAINST_PLAYERS_GANG called by ", GET_THIS_SCRIPT_NAME(), " - TARGETTED PASSIVE MODE - Calling END_PLAYER_PASSIVE_AGAINST_PLAYER for goon ", GET_PLAYER_NAME(GB_GET_GANG_GOON_AT_INDEX(piGrieferBoss, iLoop)), " at index ", iLoop)
							END_PLAYER_PASSIVE_AGAINST_PLAYER(GB_GET_GANG_GOON_AT_INDEX(piGrieferBoss, iLoop), FALSE)
						ELSE
							PRINTLN("[GO_GHOSTED] - END_PLAYER_PASSIVE_AGAINST_PLAYERS_GANG - TARGETTED PASSIVE MODE - Player was not ghosted to goon ", GET_PLAYER_NAME(GB_GET_GANG_GOON_AT_INDEX(piGrieferBoss, iLoop)), " at index ", iLoop)
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GRIEF_WINDOW_CHECKS(GRIEF_PASSIVE_DATA_STRUCT &dataStruct, BOOL bActive)
	IF HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[dataStruct.iGriefTimerLoop].GriefPassiveWindowTimer)
		IF HAS_NET_TIMER_EXPIRED(g_sGriefPassivePlayer[dataStruct.iGriefTimerLoop].GriefPassiveWindowTimer, GET_GRIEF_WINDOW_TIME())
		OR NOT bActive
		OR NOT CAN_PLAYER_USE_GRIEF_PASSIVE()
			PRINTLN("[GO_GHOSTED] - MAINTAIN_LOOP_THROUGH_TIMERS - Timer ", dataStruct.iGriefTimerLoop, " has expired or player is inactive, resetting. Elapsed time: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_sGriefPassivePlayer[dataStruct.iGriefTimerLoop].GriefPassiveWindowTimer))
			RESET_NET_TIMER(g_sGriefPassivePlayer[dataStruct.iGriefTimerLoop].GriefPassiveWindowTimer)
			g_sGriefPassivePlayer[dataStruct.iGriefTimerLoop].iKills = 0
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_GRIEF_TIMER_FOR_PARTICIPANT(INT iParticipant)
	RESET_NET_TIMER(g_sGriefPassivePlayer[iParticipant].GriefDurationTimer)
	RESET_NET_TIMER(g_sGriefPassivePlayer[iParticipant].GriefPassiveWindowTimer)
	g_sGriefPassivePlayer[iParticipant].iKills = 0
	PRINTLN("[GO_GHOSTED] - CLEANUP_GRIEF_TIMER_FOR_PARTICIPANT - Ending timer for player ", iParticipant)
ENDPROC

//PURPOSE: Stops the player from being passive against a single player.
PROC MAINTAIN_GRIEF_DURATION_CHECKS(GRIEF_PASSIVE_DATA_STRUCT &dataStruct, PLAYER_INDEX PlayerID, BOOL bActive)
	IF HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[dataStruct.iGriefTimerLoop].GriefDurationTimer)
		IF bActive			
			IF SHOULD_GRIEF_PASSIVE_END(PlayerID)
				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerID)
					PRINTLN("[GO_GHOSTED] - MAINTAIN_GRIEF_DURATION_CHECKS - SHOULD_GRIEF_PASSIVE_END returning TRUE. Unghosting from ", GET_PLAYER_NAME(PlayerID), "'s gang")
					END_PLAYER_PASSIVE_AGAINST_PLAYERS_GANG(PlayerID)
				ELSE
					PRINTLN("[GO_GHOSTED] - MAINTAIN_GRIEF_DURATION_CHECKS - SHOULD_GRIEF_PASSIVE_END returning TRUE. Unghosting from ", GET_PLAYER_NAME(PlayerID))
					END_PLAYER_PASSIVE_AGAINST_PLAYER(PlayerID)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(g_sGriefPassivePlayer[dataStruct.iGriefTimerLoop].GriefDurationTimer, GET_GRIEF_PASSIVE_DURATION())
					IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerID)
						PRINTLN("[GO_GHOSTED] - MAINTAIN_GRIEF_DURATION_CHECKS - Timer ", dataStruct.iGriefTimerLoop, " expired. Unghosting remote player's gang.")
						END_PLAYER_PASSIVE_AGAINST_PLAYERS_GANG(PlayerID)
					ELSE
						PRINTLN("[GO_GHOSTED] - MAINTAIN_GRIEF_DURATION_CHECKS - Timer ", dataStruct.iGriefTimerLoop, " expired. Unghosting remote player.")
						END_PLAYER_PASSIVE_AGAINST_PLAYER(PlayerID)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[GO_GHOSTED] - MAINTAIN_GRIEF_DURATION_CHECKS - Participant is not active for ", dataStruct.iGriefTimerLoop)
			CLEANUP_GRIEF_TIMER_FOR_PARTICIPANT(dataStruct.iGriefTimerLoop)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check that all players of organisations are ghosted if any of them become it
///    
/// PARAMS:
///    dataStruct - 
PROC MAINTAIN_GRIEF_ORGANIZATION_CHECKS(PLAYER_INDEX PlayerID)
	IF PlayerID != PLAYER_ID()
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerID)
		AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GB_GET_LOCAL_PLAYER_GANG_BOSS())
			PLAYER_INDEX gangBossID = GB_GET_THIS_PLAYER_GANG_BOSS(PlayerID)
			IF gangBossID != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(gangBossID)
				// The player is ghosted to us
				IF HAS_LOCAL_PLAYER_GONE_GHOSTED_TO_THIS_PLAYER(PlayerID)
					// Do we need to ghost the other gang members?
					IF NOT IS_LOCAL_PLAYER_GHOSTED_TO_ALL_MEMBERS_OF_THIS_GANG(gangBossID)
						PRINTLN("[GO_GHOSTED] - MAINTAIN_GRIEF_ORGANIZATION_CHECKS - Player is ghosted to ", GET_PLAYER_NAME(PlayerID)," but not to all members of ", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(gangBossID), " so we need to go ghosted to the gang.")
					
						// Ghost local player to members of the gang
						SET_PLAYER_TO_PASSIVE_AGAINST_PLAYERS_GANG(PlayerID)
						
					ENDIF
					
				// The player is not ghosted
				ELSE
					// Should the player become ghosted because they joined a gang who are?
					IF IS_LOCAL_PLAYER_GHOSTED_TO_A_MEMBER_OF_THIS_GANG(gangBossID)
						PRINTLN("[GO_GHOSTED] - MAINTAIN_GRIEF_ORGANIZATION_CHECKS - Player is not ghosted to ", GET_PLAYER_NAME(PlayerID)," but they are ghosted to members of ", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(gangBossID), " so we need to go ghosted to the player.")
					
						// Ghost the local player to the player that has joined the gang that is ghosted to us
						SET_PLAYER_TO_PASSIVE_AGAINST_PLAYER(PlayerID)
						
						// Refresh the duration of the ghosting on all gang members so that players can't join a gang with small duration remaining to clear their grief timer
						REFRESH_GO_GHOSTED_DURATION_AGAINST_GANG(gangBossID)
						
						// Print a ticker to say this player is now ghosted because they joined a gang that was ghosted
						PRINT_TICKER_WITH_PLAYER_NAME_AND_LITERAL_STRING_WITH_HUD_COLOUR("GRIEF_TICK_VICb", PlayerID, GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(PlayerID), GB_GET_PLAYER_GANG_HUD_COLOUR(PlayerID), GB_GET_PLAYER_GANG_HUD_COLOUR(PlayerID))
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_GRIEF_PASSIVE_WIDGETS(INT iPlayer)
	START_WIDGET_GROUP(" Anti-Grief Passive/Ghosting")			
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			AND iPlayer != PARTICIPANT_ID_TO_INT()
				START_WIDGET_GROUP(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayer))))
					ADD_WIDGET_INT_SLIDER("Kills on me: ", g_sGriefPassivePlayer[iPlayer].iKills, 0, HIGHEST_INT, 1)
				STOP_WIDGET_GROUP()
			ENDIF				
		ENDREPEAT
	STOP_WIDGET_GROUP()	
ENDPROC
#ENDIF

PROC INIT_GRIEF_PASSIVE()
	INT iPlayerLoop
	REPEAT NUM_NETWORK_PLAYERS iPlayerLoop
		g_sGriefPassivePlayer[IPlayerLoop].iKills = 0
		RESET_NET_TIMER(g_sGriefPassivePlayer[IPlayerLoop].GriefPassiveWindowTimer)
		RESET_NET_TIMER(g_sGriefPassivePlayer[IPlayerLoop].GriefDurationTimer)
	ENDREPEAT
ENDPROC

PROC GRIEF_PASSIVE_CLEAR_PLAYER_DATA(PLAYER_INDEX thisPlayer)
	IF IS_GRIEF_PASSIVE_ENABLED()
		IF NETWORK_IS_PLAYER_ACTIVE(thisPlayer)
			IF thisPlayer != INVALID_PLAYER_INDEX()
				PRINTLN("[GO_GHOSTED] - GRIEF_PASSIVE_CLEAR_PLAYER_DATA - Clearing grief passive data for ", GET_PLAYER_NAME(thisPlayer))
				// Reset the grief window
				IF HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[NATIVE_TO_INT(thisPlayer)].GriefPassiveWindowTimer)
					RESET_NET_TIMER(g_sGriefPassivePlayer[NATIVE_TO_INT(thisPlayer)].GriefPassiveWindowTimer)
				ENDIF
				
				// Reset the passive duration if already passive
				IF HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[NATIVE_TO_INT(thisPlayer)].GriefDurationTimer)
					RESET_NET_TIMER(g_sGriefPassivePlayer[NATIVE_TO_INT(thisPlayer)].GriefDurationTimer)
				ENDIF
				
				// Reset number of kills
				g_sGriefPassivePlayer[NATIVE_TO_INT(thisPlayer)].iKills = 0
				
				// Remove ghosting
				SET_REMOTE_PLAYER_AS_GHOST(thisPlayer, FALSE)
			ELSE
				PRINTLN("[GO_GHOSTED] - GRIEF_PASSIVE_CLEAR_PLAYER_DATA - Invalid player index passed.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GRIEF_PASSIVE_CLEANUP()
	IF IS_GRIEF_PASSIVE_ENABLED()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			PRINTLN("[GO_GHOSTED] - GRIEF_PASSIVE_CLEANUP has been called")
			INT i
			REPEAT NUM_NETWORK_PLAYERS i
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					IF PARTICIPANT_ID() != INT_TO_PARTICIPANTINDEX(i)
						// Reset the grief window
						IF HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[i].GriefPassiveWindowTimer)
							RESET_NET_TIMER(g_sGriefPassivePlayer[i].GriefPassiveWindowTimer)
						ENDIF
						
						// Reset the passive duration if already passive
						IF HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[i].GriefDurationTimer)
							RESET_NET_TIMER(g_sGriefPassivePlayer[i].GriefDurationTimer)
						ENDIF
						
						// Reset number of kills
						g_sGriefPassivePlayer[i].iKills = 0
						
						// Remove ghosting
						SET_REMOTE_PLAYER_AS_GHOST(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)), FALSE)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL MAINTAIN_GRIEF_PASSIVE(GRIEF_PASSIVE_DATA_STRUCT &dataStruct)

	IF NOT MPGlobals.sFreemodeCache.bNetworkIsActivitySession	
		IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			// Loop through all players but only once
			IF dataStruct.bDoLoop = TRUE
			
				IF CAN_PLAYER_USE_GRIEF_PASSIVE()
				
					PRINTLN("[GO_GHOSTED] - MAINTAIN_GRIEF_PASSIVE - Player has been killed, looping through all players to find the killer")
					INT iPlayerLoop
					REPEAT NUM_NETWORK_PLAYERS iPlayerLoop		
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
							PLAYER_INDEX PlayerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerLoop))
							WEAPON_TYPE tWeapon = WEAPONTYPE_INVALID
							IF NETWORK_GET_KILLER_OF_PLAYER(PLAYER_ID(), tWeapon) = PlayerID
								IF CAN_PLAYER_BE_CLASSED_AS_GRIEFER(PlayerID)
									IF NOT HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[iPlayerLoop].GriefPassiveWindowTimer)
										START_NET_TIMER(g_sGriefPassivePlayer[iPlayerLoop].GriefPassiveWindowTimer)
										g_sGriefPassivePlayer[iPlayerLoop].iKills++
										PRINTLN("[GO_GHOSTED] - MAINTAIN_GRIEF_PASSIVE - Local player was killed by ", GET_PLAYER_NAME(PlayerID), ". Starting a grief window for this player.")
									ELSE
										PRINTLN("[GO_GHOSTED] - MAINTAIN_GRIEF_PASSIVE - Local player was killed by ", GET_PLAYER_NAME(PlayerID), " again. Incrementing number of kills from this player.")
										g_sGriefPassivePlayer[iPlayerLoop].iKills++
									ENDIF
								ENDIF
								iPlayerLoop = NUM_NETWORK_PLAYERS
							ENDIF
						ENDIF
					ENDREPEAT
					
				ENDIF	
				
				// Dont loop again, only do this once
				dataStruct.bDoLoop	= FALSE
			ENDIF
		ELSE
			IF dataStruct.bDoLoop = FALSE
				dataStruct.bDoLoop = TRUE // Reset when respawned
			ENDIF
		ENDIF
			
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
			PLAYER_INDEX PlayerID
			BOOL bActive
			PARTICIPANT_INDEX Participant = INT_TO_PARTICIPANTINDEX(dataStruct.iGriefTimerLoop)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(Participant)
				PlayerID = NETWORK_GET_PLAYER_INDEX(Participant)
				IF PlayerID != INVALID_PLAYER_INDEX()
				AND NETWORK_IS_PLAYER_ACTIVE(PlayerID)
					bActive = TRUE
					MAINTAIN_GRIEF_ORGANIZATION_CHECKS(PlayerID)
				ENDIF
			ENDIF
			
			MAINTAIN_GRIEF_WINDOW_CHECKS(dataStruct, bActive)			
			MAINTAIN_GRIEF_DURATION_CHECKS(dataStruct, PlayerID, bActive)
		ENDIF
		
		dataStruct.iGriefTimerLoop++
		IF dataStruct.iGriefTimerLoop = NUM_NETWORK_PLAYERS
			dataStruct.iGriefTimerLoop = 0
		ENDIF
		
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


