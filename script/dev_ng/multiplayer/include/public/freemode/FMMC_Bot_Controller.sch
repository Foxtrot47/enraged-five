//////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FMMC_Bot_Controller																	//
// Description: Manages the creation and handling of test bots for UGC modes						//
// Written by:  Martin MacKinnon + Various	 														//
// Date: 25/07/2014																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////
///

USING "globals.sch"
USING "net_common_functions.sch"
USING "fmmc_creation.sch"

PROC SET_TEAM_RELATIONSHIPS(RELATIONSHIP_TYPE relType, REL_GROUP_HASH &relGroup[], INT iVal)
	INT i	
	REPEAT MAX_NUM_NETWORK_SCRIPTS_ALLOWED i
		IF relGroup[iVal] <> relGroup[i]
			SET_RELATIONSHIP_BETWEEN_GROUPS(relType, relGroup[iVal], relGroup[i]) 
		ENDIF
	ENDREPEAT 
ENDPROC

PROC SHOULD_PLAYER_BE_INVINCIBLE()
	IF IS_THIS_A_LTS()
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
		ENDIF
	ENDIF
	
	SPECIAL_ABILITY_RESET(PLAYER_ID())
ENDPROC

FUNC BOOL PROCESS_PRE_BOTNET(MODEL_NAMES &mnDefaultPeds[], REL_GROUP_HASH &rgTeamRel[])
	IF mnDefaultPeds[0] != G_M_Y_Lost_02
		g_FMMC_STRUCT.g_b_QuitTest = FALSE
		
		mnDefaultPeds[0] = G_M_Y_Lost_02
		mnDefaultPeds[1] = G_M_Y_MexGoon_02
		mnDefaultPeds[2] = G_M_Y_BallaOrig_01
		mnDefaultPeds[3] = G_M_Y_Korean_01
		
		ALLOW_MISSION_CREATOR_WARP(FALSE)
		
		INT i
		REPEAT MAX_NUM_NETWORK_SCRIPTS_ALLOWED i
			TEXT_LABEL_63 tlRelGroup
			tlRelGroup = "rgFM_BOTTEAM"
			tlRelGroup += i
			ADD_RELATIONSHIP_GROUP(tlRelGroup, rgTeamRel[i])
		ENDREPEAT
		
		i = 0
		REPEAT MAX_NUM_NETWORK_SCRIPTS_ALLOWED i
			SET_TEAM_RELATIONSHIPS(ACQUAINTANCE_TYPE_PED_HATE, rgTeamRel, i)
		ENDREPEAT
		
		i = 0
		REPEAT MAX_NUM_NETWORK_SCRIPTS_ALLOWED i		
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()), rgTeamRel[i])
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgTeamRel[i], GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
		ENDREPEAT
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked)
			REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
			INT iAmmoToGive = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet)) * 3)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet), iAmmoToGive, TRUE)
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet), TRUE)
		ELSE
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 5)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SMG, 200)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 300)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 40)
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC BLIP_ENEMIES_IN_TEST(TEST_PEDS &tpBotnet[])
	INT i
	HUD_COLOURS teamcolour
	INT R,G,B,A
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD0)
			REPEAT 32 i 
				IF DOES_BLIP_EXIST(tpBotnet[i].blipID)
					REMOVE_BLIP(tpBotnet[i].blipID)
				ENDIF
			ENDREPEAT
		ENDIF
	#ENDIF
	
	REPEAT 32 i 
		IF NOT IS_PED_INJURED(tpBotnet[i].pedID)
			IF NOT DOES_BLIP_EXIST(tpBotnet[i].blipID)
				tpBotnet[i].blipID = ADD_BLIP_FOR_ENTITY(tpBotnet[i].pedID)	
				IF GET_PED_RELATIONSHIP_GROUP_HASH(tpBotnet[i].pedID) = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
				AND (IS_THIS_A_LTS() OR g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM)
					SET_BLIP_AS_FRIENDLY(tpBotnet[i].blipID, TRUE)
					SHOW_FRIEND_INDICATOR_ON_BLIP(tpBotnet[i].blipID, TRUE)
				ELSE
					teamcolour = GET_HUD_COLOUR_FOR_TARGET_RELATION(PLAYER_ID(),TRUE,tpBotnet[i].iTeam)
					SET_BLIP_AS_FRIENDLY(tpBotnet[i].blipID, FALSE)
					GET_HUD_COLOUR(teamcolour,R,G,B,A)
					SET_BLIP_SECONDARY_COLOUR(tpBotnet[i].blipID, R, G, B) 
					SHOW_OUTLINE_INDICATOR_ON_BLIP(tpBotnet[i].blipID, TRUE)
				ENDIF
												
				SET_BLIP_SCALE(tpBotnet[i].blipID, BLIP_SIZE_NETWORK_PED)
			ENDIF
		ELSE
			// Remove
			IF DOES_BLIP_EXIST(tpBotnet[i].blipID)
				REMOVE_BLIP(tpBotnet[i].blipID)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CREATE_BOTS_WITH_NO_TEAMS(INT iPed, MODEL_NAMES &mnPedModels[], REL_GROUP_HASH  &rghTeam[], TEST_PEDS &sPedPassed[])
	IF NOT DOES_ENTITY_EXIST(sPedPassed[iPed].pedID)
		REQUEST_MODEL(mnPedModels[0])
		IF HAS_MODEL_LOADED(mnPedModels[0])
		
			INT iRandomSpot = GET_SAFE_ENEMY_SPAWN_POSITION(sPedPassed, FALSE, sPedPassed[iPed].iTeam)
				
			sPedPassed[iPed].pedID = CREATE_PED(PEDTYPE_MISSION, mnPedModels[0], g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRandomSpot].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRandomSpot].fHead, FALSE, FALSE)
			
			SET_THE_COMMON_TEST_PED_VARIABLES(sPedPassed[iPed].pedID, rghTeam[iPed],sPedPassed[iPed].iTeam)

			SET_MODEL_AS_NO_LONGER_NEEDED(mnPedModels[0])
			sPedPassed[iPed].iRespawnTimer = 0
		ENDIF
						
		EXIT
	ENDIF
ENDPROC

PROC SET_BOT_ON_CORRECT_TEAM(INT &iBotTeam, INT &iTeamToPlace, INT &iTeams[])
	IF iTeamToPlace >= g_FMMC_STRUCT.iNumberOfTeams
		iTeamToPlace = 0
	ENDIF

	IF iTeamToPlace = g_FMMC_STRUCT.iTestMyTeam
		//placing on the same team as the player, make sure we count them as a team member
		iTeams[iTeamToPlace]++
	ENDIF
	
	INT i
	IF iTeams[iTeamToPlace] >= g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeamToPlace]
		IF iTeamToPlace != g_FMMC_STRUCT.iTestMyTeam
			iTeams[g_FMMC_STRUCT.iTestMyTeam]++
		ENDIF
		
		FOR i = 0 TO FMMC_MAX_TEAMS-1
			IF iTeams[i] < g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i]
				iTeamToPlace = i
			ENDIF
		ENDFOR
		
		IF iTeamToPlace != g_FMMC_STRUCT.iTestMyTeam
			iTeams[g_FMMC_STRUCT.iTestMyTeam]--
		ENDIF
	ENDIF
	
	iTeams[iTeamToPlace]++
	
	iBotTeam = iTeamToPlace
	
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		PRINTLN("TEAM HAS ", iTeams[i])
	ENDFOR
	
	IF iTeamToPlace = g_FMMC_STRUCT.iTestMyTeam
		//remove the player from the count so we dont count them every check
		iTeams[iTeamToPlace]--
	ENDIF
	
	iTeamToPlace++
ENDPROC

PROC HANDLE_BOT_TEAM_NUMBERS(INT &iServerVal[], INT &iScratchVal[], INT &iLocalBoolCheck, INT &iTeams[])
	INT i
	
	FOR i=0 TO FMMC_MAX_TEAMS-1
		iServerVal[i] = iTeams[i]
		iScratchVal[i] = iTeams[i]
		IF iTeams[i] > 0
			SET_BIT(iLocalBoolCheck,4+i)
		ENDIF
	ENDFOR
ENDPROC

PROC CREATE_BOTS_WITH_TEAMS(MODEL_NAMES &mnDefaultPeds[], TEST_PEDS &tpBotnet[], REL_GROUP_HASH &rgTeamRel[], INT iPed, INT iPlayerTeam, BOOL bUseTeamSpawn, INT &iTeamToPlace, INT &iTeamArray[])
	BOOL bHasLoaded = TRUE
	INT iModel = 0
	
	FOR iModel = 0 TO FMMC_MAX_TEAMS-1
		REQUEST_MODEL(mnDefaultPeds[iModel])
		IF NOT HAS_MODEL_LOADED(mnDefaultPeds[iModel])
			bHasLoaded = FALSE
		ENDIF
	ENDFOR
	
	IF bHasLoaded	
		IF iTeamToPlace >= FMMC_MAX_TEAMS
			iTeamToPlace = 0
		ENDIF
				
		SET_BOT_ON_CORRECT_TEAM(tpBotnet[iPed].iTeam, iTeamToPlace, iTeamArray)
		
		INT iRandomSpot = GET_SAFE_ENEMY_SPAWN_POSITION(tpBotnet, bUseTeamSpawn, tpBotnet[iPed].iTeam)
									
		IF NOT DOES_ENTITY_EXIST(tpBotnet[iPed].pedID)													
			IF bUseTeamSpawn
				tpBotnet[iPed].pedID = CREATE_PED(PEDTYPE_MISSION, mnDefaultPeds[tpBotnet[iPed].iTeam], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[tpBotnet[iPed].iTeam][iRandomSpot].vPos, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[tpBotnet[iPed].iTeam][iRandomSpot].fHead, FALSE, FALSE)
			ELSE
				tpBotnet[iPed].pedID = CREATE_PED(PEDTYPE_MISSION, mnDefaultPeds[tpBotnet[iPed].iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRandomSpot].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRandomSpot].fHead, FALSE, FALSE)
			ENDIF
			
			TEXT_LABEL_7 tl7 = "BOT" 
			tl7 += iPed
			SET_PED_NAME_DEBUG(tpBotnet[iPed].pedID, tl7)
			
			IF tpBotnet[iPed].iTeam = iPlayerTeam
				SET_THE_COMMON_TEST_PED_VARIABLES(tpBotnet[iPed].pedID, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()),tpBotnet[iPed].iTeam)
				PRINTLN("ON PLAYER TEAM")
			ELSE
				SET_THE_COMMON_TEST_PED_VARIABLES(tpBotnet[iPed].pedID, rgTeamRel[tpBotnet[iPed].iTeam],tpBotnet[iPed].iTeam)
				PRINTLN("NOT ON PLAYER TEAM")
			ENDIF
			
			tpBotnet[iPed].iRespawnTimer = 0
			
			PRINTLN("*******************************************")
			PRINTLN("MAKING PED FOR TEAM ", tpBotnet[iPed].iTeam)
			PRINTLN("RANDOM SPOT CHOSEN IS ", iRandomSpot)
			PRINTLN("PLACING PED AT ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[tpBotnet[iPed].iTeam][iRandomSpot].vPos)
			PRINTLN("*******************************************")
		ENDIF
	ENDIF
ENDPROC

PROC SPAWN_BOTS(MODEL_NAMES &mnPedModels[], REL_GROUP_HASH  &rghTeam[], TEST_PEDS &sPedPassed[], INT iPlayerTeam, BOOL bUseTeamSpawn, INT &iTeamArray[])
	INT iPed
	INT iTeam
	WEAPON_TYPE wtWep
	INT iSpawnPoints = g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints
	INT iTeamToPlace = g_FMMC_STRUCT.iTestMyTeam+1
	
	IF IS_THIS_A_LTS()
		FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
			iSpawnPoints += g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeam]
		ENDFOR
		
		iSpawnPoints++ //hack around the DM way of handling spawns
	ENDIF
			
	IF iSpawnPoints != 0
		FOR iPed = 0 TO g_FMMC_STRUCT.iNumParticipants-1	
			IF iPed < (iSpawnPoints - 1)
				IF DOES_ENTITY_EXIST(sPedPassed[iPed].pedID)
					IF IS_PED_DEAD_OR_DYING(sPedPassed[iPed].pedID)
					OR IS_PED_INJURED(sPedPassed[iPed].pedID)
						IF NOT IS_THIS_A_LTS()
							SET_PED_AS_NO_LONGER_NEEDED(sPedPassed[iPed].pedID)	
						ENDIF
					ELSE
						IF sPedPassed[iPed].iTeam != iPlayerTeam
							IF NOT IS_PED_IN_COMBAT(sPedPassed[iPed].pedID, PLAYER_PED_ID())
								TASK_COMBAT_PED(sPedPassed[iPed].pedID, PLAYER_PED_ID())
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(sPedPassed[iPed].pedID, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sPedPassed[iPed].pedID, 295.0)
							ENDIF
						ENDIF
						
						IF NOT IS_PED_DEAD_OR_DYING(sPedPassed[iPed].pedID)
							IF GET_CURRENT_PED_WEAPON(sPedPassed[iPed].pedID, wtWep)
								IF wtWep = WEAPONTYPE_PISTOL
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked)
									OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked)
										IF GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet) = WEAPONTYPE_PISTOL
										ELSE
											REMOVE_ALL_PED_WEAPONS(sPedPassed[iPed].pedID)
											GIVE_WEAPON_TO_PED(sPedPassed[iPed].pedID, GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet), 100, TRUE, TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_A_LTS()
						CREATE_BOTS_WITH_TEAMS(mnPedModels, sPedPassed, rghTeam, iPed, iPlayerTeam, bUseTeamSpawn, iTeamToPlace, iTeamArray)
					ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
						IF GET_GAME_TIMER() > sPedPassed[iPed].iRespawnTimer
							IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_STANDARD
								CREATE_BOTS_WITH_NO_TEAMS(iPed, mnPedModels, rghTeam, sPedPassed)
							ELSE
								CREATE_BOTS_WITH_TEAMS(mnPedModels, sPedPassed, rghTeam, iPed, iPlayerTeam, bUseTeamSpawn, iTeamToPlace, iTeamArray)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

FUNC BOOL SHOULD_USE_TEAM_SPAWN(BOOL bHasSpawned)
	IF g_FMMC_Struct.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_TEAM_RESPAWN)
	AND NOT bHasSpawned
		RETURN TRUE
	ELIF IS_THIS_A_LTS()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_TEST_BOTS(MODEL_NAMES &mnPedModels[], REL_GROUP_HASH  &rghTeam[], TEST_PEDS &sPedPassed[], INT iPlayerTeam, BOOL bHasSpawned, INT &iTeam[])	
	SPAWN_BOTS(mnPedModels, rghTeam, sPedPassed, iPlayerTeam, SHOULD_USE_TEAM_SPAWN(bHasSpawned), iTeam)
	SHOULD_PLAYER_BE_INVINCIBLE()
ENDPROC

PROC UPDATE_TEST_STATE(TEST_PEDS &sPedPassed[])	
	INT j
	BOOL bGameOver = FALSE
	
	IF IS_THIS_A_LTS()
		FOR j = 0 TO g_FMMC_STRUCT.iNumParticipants
			IF DOES_ENTITY_EXIST(sPedPassed[j].pedID)
				IF sPedPassed[j].iTeam != g_FMMC_STRUCT.iTestMyTeam
					IF IS_PED_DEAD_OR_DYING(sPedPassed[j].pedID)
						bGameOver = TRUE
					ELSE
						bGameOver = FALSE
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF bGameOver
		IF IS_SCREEN_FADED_OUT() AND NOT IS_PAUSE_MENU_ACTIVE()
			IF IS_THIS_A_LTS()
				SET_WARNING_MESSAGE_WITH_HEADER("FMMC_ENDLTS","FMMC_ENDLTS2", FE_WARNING_OK)
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					SET_BIT(g_FMMC_STRUCT.biTeamTestComplete, g_FMMC_STRUCT.iTestMyTeam)
					g_FMMC_STRUCT.g_b_QuitTest = TRUE
					TRIGGER_MUSIC_EVENT("MP_MC_STOP")
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(4000)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC
