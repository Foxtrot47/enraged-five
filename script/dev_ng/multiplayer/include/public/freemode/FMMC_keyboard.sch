
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////			   Script Keyboard by			 //////////////////////////////
//////////////////////////////				 Bobby Wright				 //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING"commands_streaming.sch"
USING"commands_socialclub.sch"
USING "fmmc_cloud_loader.sch"

CONST_INT OSK_STAGE_SET_UP  0
CONST_INT OSK_STAGE_RUNNING 1
CONST_INT OSK_STAGE_FAILED 	2

FUNC BOOL USE_SHORT_OBJECTIVE_TEXT()
	IF GET_CURRENT_LANGUAGE() = LANGUAGE_RUSSIAN
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_PLAYER_KICKED_TO_GO_OFFLINE_FROM_KEYBOARD(BOOL bHasEntered)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		IF bHasEntered
			NET_NL()NET_PRINT("*********************<<<SET_PLAYER_KICKED_TO_GO_OFFLINE_FROM_KEYBOARD = TRUE >>>****************************** ")
		ELSE
			NET_NL()NET_PRINT("*********************<<<SET_PLAYER_KICKED_TO_GO_OFFLINE_FROM_KEYBOARD = FALSE >>>******************************")
		ENDIF
	#ENDIF
	#IF USE_FINAL_PRINTS
	  DEBUG_PRINTCALLSTACK()
	  IF bHasEntered
		  PRINTLN_FINAL("*********************<<<SET_PLAYER_KICKED_TO_GO_OFFLINE_FROM_KEYBOARD = TRUE >>>****************************** " )
	  ELSE
		  PRINTLN_FINAL("*********************<<<SET_PLAYER_KICKED_TO_GO_OFFLINE_FROM_KEYBOARD = FALSE >>>****************************** " )
	  ENDIF
	#ENDIF
	g_b_Private_Kick_player_to_switch_to_offline_mode = bHasEntered
ENDPROC


/// PURPOSE: return true when the keybord has finished
/// PARAMS:
///	oskStatus - the current status
///	iCurrentstatus - control int
/// RETURNS:
///	
FUNC BOOL RUN_ON_SCREEN_KEYBOARD(OSK_STATUS		&oskStatus, 
								INT				&iProfanityToken, 
								INT				&iCurrentstatus, 
								BOOL			bIsObjectiveText		= FALSE, 
								BOOL			bMissionName			= TRUE, 
								STRING			stToChange				= NULL, 
								BOOL			bSMSMessage				= FALSE, 
								INT				iMissObjectiveType		= 0, 
								BOOL			bSMSMessageShort		= FALSE, 
								BOOL			bMissionTags			= FALSE,
								STRING			stToChange2				= NULL,
								BOOL			bUsingLaptopEmail		= FALSE,
								BOOL			bRenameOutfit			= FALSE,
								BOOL			bCutsceneName			= FALSE,
								BOOL			bCustomTeamName			= FALSE,
								BOOL			bCustomRoleName			= FALSE,
								INT				iCurrentSMS				= 0,
								BOOL			bCustomYachtName		= FALSE,
								INT				iCustomOrginisation		= 0,
								BOOL			bTooShort				= FALSE,
								BOOL			bCanceled				= FALSE,
								BOOL			bInUse					= FALSE,
								BOOL			bUGC					= TRUE, 
								BOOL 			bInvalidCharacter 		= FALSE,
								BOOL 			bSearch					= FALSE,
								BOOL			bBlimpMessage			= FALSE,
								BOOL			bEnterNumber			= FALSE,
								BOOL			bCustomProp				= FALSE,
								BOOL			bCustomTemplateName		= FALSE,
								BOOL			bCustomVehicleName		= FALSE, 
								INT				iDeathmatchHelpText		= -1,
								INT				iDeathmatchHelpTextTeam	= -1,
								BOOL 			bJustCustomText 		= FALSE,
								STRING			sCustomTitleForWindow = NULL,
								BOOL			bCustomDevModel			= FALSE)
	
	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADED_OUT()
		RETURN FALSE
	ENDIF
		
	CONST_INT iProfanity_0_valid 0
	CONST_INT iProfanity_error -1
	CONST_INT iProfanity_empty -2
	CONST_INT iProfanity_contains_spaces -3
	ePROFANITY_CHECK_STATUS status 
	INT iOriginalNameLength, iCurrentNameLength, iLoop
	STRING sTitle, sDesc
	//Set up the keyyboard and then wait for it's return status
	SWITCH iCurrentstatus
		//Set up
		CASE OSK_STAGE_SET_UP
			
			PRINTLN("RUN_ON_SCREEN_KEYBOARD - Coming in iProfanityToken = ", iProfanityToken)
			IF bIsObjectiveText
				INT iValue
				
				IF iMissObjectiveType = 6
					iValue = 121
				ELIF iMissObjectiveType = 7
					iValue = 63
				ELSE
					IF USE_SHORT_OBJECTIVE_TEXT()
						iValue = 21 //BC: limit the japanese string to 21 characters as that's all we have storage for. Class B 1706658
					ELSE
						iValue = 51
					ENDIF
				ENDIF
				IF iProfanityToken = iProfanity_0_valid
					IF iMissObjectiveType = 0
					OR iMissObjectiveType = 1
					OR iMissObjectiveType = 2
						IF USE_SHORT_OBJECTIVE_TEXT()
							sTitle = "FMMC_KEY_TIS2SJ"
						ELSE
							sTitle = "FMMC_KEY_TIP2"
						ENDIF
					ELIF iMissObjectiveType = 3
						sTitle = "FMMC_KEY_TIP7"
						iValue = 21
					ELIF iMissObjectiveType = 4
						sTitle = "FMMC_KEY_TIP5"
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("fm_capture_creator")) > 0 
							iValue = 21
						ELSE
							iValue = 23
						ENDIF
					ELIF iMissObjectiveType = 5
						sTitle = "FMMC_KEY_TIP6"
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("fm_capture_creator")) > 0 
							iValue = 21
						ELSE
							iValue = 23
						ENDIF
					ELIF iMissObjectiveType = 6
						sTitle = "FMMC_KEY_TIP10"
					ELIF iMissObjectiveType = 8
						sTitle = "FMMC_KEY_TXT7"
					ELIF iMissObjectiveType = 9
						sTitle = "FMMC_KEY_TXT8"
					ELIF iMissObjectiveType = 10
						sTitle = "FMMC_KEY_VSS"
					ENDIF
				ELSE
					IF USE_SHORT_OBJECTIVE_TEXT()
						sTitle = "FMMC_KEY_TIS2FJ"
					ELSE
						sTitle = "FMMC_KEY_TIP2F"
					ENDIF
				ENDIF
				
				IF IS_XBOX360_VERSION()
				OR IS_XBOX_PLATFORM()
					sDesc = sTitle
					IF iMissObjectiveType = 0
					OR iMissObjectiveType = 1
					OR iMissObjectiveType = 2
						sTitle = "FMMC_SS_RL_35"
					ELIF iMissObjectiveType = 3
						sTitle = "FMMC_SS_RL_32"
					ELIF iMissObjectiveType = 4
						sTitle = "FMMC_SS_RL_33"
					ELIF iMissObjectiveType = 5
						sTitle = "FMMC_SS_RL_34"
					ELIF iMissObjectiveType = 6
						sTitle = "FMMC_SS_RL_49"
					ELIF iMissObjectiveType = 7
						sTitle = "FMMC_SS_RL_57"
					ENDIF //
				ENDIF
				
				eKeyboardType keyType
				//ONSCREEN_KEYBOARD_FILENAME stops certain punctuation from being input - Content need this.
				#IF NOT IS_DEBUG_BUILD
				IF IS_PC_VERSION()
					keyType = ONSCREEN_KEYBOARD_FILENAME
				ELSE
				#ENDIF
					IF GET_CURRENT_LANGUAGE() = LANGUAGE_ENGLISH
						keyType = ONSCREEN_KEYBOARD_ENGLISH
					ELSE
						keyType = ONSCREEN_KEYBOARD_LOCALISED
					ENDIF
				#IF NOT IS_DEBUG_BUILD
				ENDIF
				#ENDIF
				
				IF bSearch
					IF bEnterNumber
						sTitle = "LINE_NUMBER"
					ELSE
						sTitle = "WEB_SEARCH"
					ENDIF
				ENDIF
				IF IS_STRING_NULL_OR_EMPTY(stToChange)
					DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc,"","","","", iValue)
				ELSE
					DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc, stToChange,stToChange2,"","", iValue)
				ENDIF
			ELSE
				eKeyboardType keyType
				INT iNumOfChars
				
				//ONSCREEN_KEYBOARD_FILENAME stops certain punctuation from being input - Content need this.
				#IF NOT IS_DEBUG_BUILD
				IF IS_PC_VERSION()
					keyType = ONSCREEN_KEYBOARD_FILENAME
				ELSE
				#ENDIF
					IF GET_CURRENT_LANGUAGE() = LANGUAGE_ENGLISH
						keyType = ONSCREEN_KEYBOARD_ENGLISH
					ELSE
						keyType = ONSCREEN_KEYBOARD_LOCALISED
					ENDIF
				#IF NOT IS_DEBUG_BUILD
				ENDIF
				#ENDIF
				
				IF bMissionName
					IF iProfanityToken = iProfanity_0_valid
						sTitle = "FMMC_KEY_TIP"
					ELSE
						sTitle = "FMMC_KEY_TIPF"
					ENDIF
					IF IS_XBOX360_VERSION()
					OR IS_XBOX_PLATFORM()
						sDesc = sTitle
						sTitle = "FMMC_T0_M19"
					ENDIF
					
					IF GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
						iNumOfChars = 21 //BC: limit the japanese string to 21 characters as that's all we have storage for. Class B 1706658
					ELSE
						iNumOfChars = 25
					ENDIF
					
					IF IS_ROCKSTAR_DEV()
						iNumOfChars = 64
					ENDIF
					
					NEXT_ONSCREEN_KEYBOARD_RESULT_WILL_DISPLAY_USING_THESE_FONTS(FONT_BIT_CURSIVE)
					
					IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionName)
						DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc,"","","","", iNumOfChars)
					ELSE
						DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc, g_FMMC_STRUCT.tl63MissionName,"","","", iNumOfChars)
					ENDIF
				ELIF bJustCustomText
					sTitle = "CST_KEY_TEXT"
					
					iNumOfChars = 63
					
					NEXT_ONSCREEN_KEYBOARD_RESULT_WILL_DISPLAY_USING_THESE_FONTS(FONT_BIT_CURSIVE)
					
					IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63BlimpMsg)
						DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc,"","","","", iNumOfChars)
					ELSE
						DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc, g_FMMC_STRUCT.tl63BlimpMsg,"","","", iNumOfChars)
					ENDIF
					
				ELIF bBlimpMessage
					IF iProfanityToken = iProfanity_0_valid
						sTitle = "FMMC_KEY_BLIMP"
					ELSE
						sTitle = "FMMC_KEY_BTIPF"
					ENDIF
					
					iNumOfChars = 60
					
					NEXT_ONSCREEN_KEYBOARD_RESULT_WILL_DISPLAY_USING_THESE_FONTS(FONT_BIT_CURSIVE)
					
					IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63BlimpMsg)
						DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc,"","","","", iNumOfChars)
					ELSE
						DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc, g_FMMC_STRUCT.tl63BlimpMsg,"","","", iNumOfChars)
					ENDIF							
				ELIF iDeathmatchHelpText != -1
					IF iProfanityToken = iProfanity_0_valid
						sTitle = "DMC_HTM_EH"
					ELSE
						sTitle = "FMMC_KEY_BTIPF"
					ENDIF
					
					iNumOfChars = 60
					
					NEXT_ONSCREEN_KEYBOARD_RESULT_WILL_DISPLAY_USING_THESE_FONTS(FONT_BIT_CURSIVE)
					
					TEXT_LABEL_63 sHTStringToUse
					sHTStringToUse = g_FMMC_STRUCT.tl63DMHelptextSaved[iDeathmatchHelpText]
					IF iDeathmatchHelpTextTeam > -1
						sHTStringToUse = g_FMMC_STRUCT.tl63DMHelptextTeams[iDeathmatchHelpText][iDeathmatchHelpTextTeam]
					ENDIF
					
					IF IS_STRING_NULL_OR_EMPTY(sHTStringToUse)
						DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc,"","","","", iNumOfChars)
					ELSE
						DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc, sHTStringToUse,"","","", iNumOfChars)
					ENDIF		
				ELIF bCustomProp
					IF iProfanityToken = iProfanity_0_valid
						sTitle = "FMMC_CDP_ADD2"
					ELSE
						sTitle = "FMMC_CDP_ADD2"
					ENDIF
					
					iNumOfChars = 60
					
					NEXT_ONSCREEN_KEYBOARD_RESULT_WILL_DISPLAY_USING_THESE_FONTS(FONT_BIT_CURSIVE)
					DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc,"","","","", iNumOfChars)
				ELIF bCustomDevModel
					
					IF iProfanityToken = iProfanity_0_valid
						sTitle = "FMMC_CDM_T"
					ELSE
						sTitle = "FMMC_CDM_F1"
					ENDIF
					
					iNumOfChars = 60
					
					NEXT_ONSCREEN_KEYBOARD_RESULT_WILL_DISPLAY_USING_THESE_FONTS(FONT_BIT_CURSIVE)
					DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc,"","","","", iNumOfChars)
				
				ELIF bCutsceneName
					IF iProfanityToken = iProfanity_0_valid
						sTitle = "FMMC_KEY_TIP"
					ELSE
						sTitle = "FMMC_KEY_TIPF"
					ENDIF
					IF IS_XBOX360_VERSION()
						sDesc = sTitle
						sTitle = "FMMC_T0_M19"
					ENDIF
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sCustomTitleForWindow)
						sTitle = sCustomTitleForWindow
						PRINTLN("RUN_ON_SCREEN_KEYBOARD - (bCutsceneName) Using sCustomTitleForWindow which is ", sCustomTitleForWindow)
					ENDIF
					
					iNumOfChars = 63
					
					NEXT_ONSCREEN_KEYBOARD_RESULT_WILL_DISPLAY_USING_THESE_FONTS(FONT_BIT_CURSIVE)
					
					IF IS_STRING_NULL_OR_EMPTY(stToChange)
						DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc,"","","","", iNumOfChars)
					ELSE
						DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc, stToChange,"","","", iNumOfChars)
					ENDIF
				ELIF bMissionTags
					IF GET_CURRENT_LANGUAGE() = LANGUAGE_ENGLISH
						iNumOfChars = 63
						IF iProfanityToken = iProfanity_0_valid
							sTitle = "FMMC_KEY_TIPT"
						ELIF iProfanityToken = iProfanity_contains_spaces
							sTitle = "FMMC_KEY_TIPTS"
						ELSE
							sTitle = "FMMC_KEY_TIPTF"
						ENDIF
					ELSE
						iNumOfChars = 63
						IF iProfanityToken = iProfanity_0_valid
							sTitle = "FMMC_KEY_ALTT"
						ELIF iProfanityToken = iProfanity_contains_spaces
							sTitle = "FMMC_KEY_ALTTS"
						ELSE
							sTitle = "FMMC_KEY_ALTTF"
						ENDIF
					ENDIF
					IF IS_XBOX360_VERSION()
					OR IS_XBOX_PLATFORM()
						sDesc = sTitle
						sTitle = "FMMC_T0_M36"
					ENDIF
					INT i
					FOR i = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION - 1)
						IF IS_STRING_NULL(g_FMMC_STRUCT.szTags.TextLabel[i])
							g_FMMC_STRUCT.szTags.TextLabel[i]   = ""
						ENDIF
					ENDFOR
					
					DISPLAY_ONSCREEN_KEYBOARD_WITH_LONGER_INITIAL_STRING(keyType, sTitle, sDesc, 
					g_FMMC_STRUCT.szTags.TextLabel[0], 
					g_FMMC_STRUCT.szTags.TextLabel[1], 
					g_FMMC_STRUCT.szTags.TextLabel[2], 
					g_FMMC_STRUCT.szTags.TextLabel[3], 
					g_FMMC_STRUCT.szTags.TextLabel[4], 
					g_FMMC_STRUCT.szTags.TextLabel[5], 
					g_FMMC_STRUCT.szTags.TextLabel[6], 
					g_FMMC_STRUCT.szTags.TextLabel[7],
					iNumOfChars)		
				ELSE
					IF bSMSMessage
						IF iProfanityToken = iProfanity_0_valid
							sTitle = "FMMC_KEY_TIP4"
						ELSE
							sTitle = "FMMC_KEY_TIP4F"
						ENDIF
						IF IS_XBOX360_VERSION()
						OR IS_XBOX_PLATFORM()
							sDesc = sTitle
							sTitle = "FMMC_SMS3"
						ENDIF
						IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sSMS[iCurrentSMS].tl63Message[0])
							DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc,"","","","", 126)
						ELSE
							INT i
							FOR i = 0 TO (FMMC_NUM_LABELS_FOR_SMS - 1)
								IF IS_STRING_NULL(g_FMMC_STRUCT.sSMS[iCurrentSMS].tl63Message[i])
									g_FMMC_STRUCT.sSMS[iCurrentSMS].tl63Message[i]	= ""
								ENDIF
							ENDFOR  
							DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc, g_FMMC_STRUCT.sSMS[iCurrentSMS].tl63Message[0], g_FMMC_STRUCT.sSMS[iCurrentSMS].tl63Message[1],"","", 126)
						ENDIF
					ELIF bSMSMessageShort
						
						iNumOfChars = 61
						
						IF bUsingLaptopEmail
						
							IF USE_SHORT_OBJECTIVE_TEXT()

								IF iProfanityToken = iProfanity_0_valid
									sTitle = "CELL_EMASH_SOD" //Short Variants.
								ELIF iProfanityToken = iProfanity_error
									sTitle = "CELL_EMASH_SODE"
								ELSE
									sTitle = "CELL_EMASH_SODF"
								ENDIF
					  
							ELSE

								IF iProfanityToken = iProfanity_0_valid
									sTitle = "CELL_EMAIL_SOD"
								ELIF iProfanityToken = iProfanity_error
									sTitle = "CELL_EMAIL_SODE"
								ELSE
									sTitle = "CELL_EMAIL_SODF"
								ENDIF

							ENDIF

							//This should fix 2104537 when using Eyefind Email.
							IF IS_XBOX360_VERSION()
							OR IS_XBOX_PLATFORM()
								sDesc = sTitle
								sTitle = "CELL_EMAIL_DESC" //Displays "description" above grey box
							ENDIF
					   
						ELSE
							
							IF USE_SHORT_OBJECTIVE_TEXT()
								IF iProfanityToken = iProfanity_0_valid
									sTitle = "FMMC_KEY_TIP8S"
								ELSE
									sTitle = "FMMC_KEY_TIP8FS"
								ENDIF
								IF IS_XBOX360_VERSION()
								OR IS_XBOX_PLATFORM()
									sDesc = sTitle
									sTitle = "FMMC_SMS4"
								ENDIF
								iNumOfChars = 21
							ELSE
								IF iProfanityToken = iProfanity_0_valid
									sTitle = "FMMC_KEY_TIP8"
								ELSE
									sTitle = "FMMC_KEY_TIP8F"
								ENDIF
								IF IS_XBOX360_VERSION()
									sDesc = sTitle
									sTitle = "FMMC_SMS4"
								ENDIF
							ENDIF
						ENDIF
						
						//Eyefind Email subject may need the "61" num of chars truncated for other languages. Will use a dedicated short variant routine if so.

						IF IS_STRING_NULL_OR_EMPTY(stToChange)
							DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc,"","","","", iNumOfChars)
						ELSE
							DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc, stToChange,"","","", iNumOfChars)
						ENDIF
					ELIF bRenameOutfit
						IF iProfanityToken = iProfanity_0_valid
							sTitle = "FMMC_KEY_TIP9"
						ELSE
							sTitle = "FMMC_KEY_TIP9F"
						ENDIF
						IF IS_XBOX360_VERSION()
							sDesc = sTitle
							sTitle = "FMMC_KEY_TIP9N"
						ENDIF
						
						IF IS_STRING_NULL_OR_EMPTY(stToChange)
							DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc,"","","","", 16)
						ELSE
							DISPLAY_ONSCREEN_KEYBOARD(keyType, sTitle, sDesc, stToChange,"","","", 16)
						ENDIF
					ELIF bCustomTeamName
						iNumOfChars = 16
						
						IF iProfanityToken = iProfanity_0_valid
							sTitle = "FMMC_KEY_TIP12"
						ELSE
							sTitle = "FMMC_KEY_TIP12F"
						ENDIF
						
						IF IS_XBOX360_VERSION()
							sDesc = sTitle
							sTitle = "FMMC_KEY_TIP12N"
						ENDIF
						
						IF IS_STRING_NULL_OR_EMPTY(stToChange)
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc,"","","","", iNumOfChars)
						ELSE
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc, stToChange,"","","", iNumOfChars)
						ENDIF
					ELIF bCustomTemplateName
						iNumOfChars = 16
						
						IF iProfanityToken = iProfanity_0_valid
							sTitle = "FMMC_KEY_TIPTN"
						ELSE
							sTitle = "FMMC_KEY_TIPTNF"
						ENDIF

						IF IS_STRING_NULL_OR_EMPTY(stToChange)
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc,"","","","", iNumOfChars)
						ELSE
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc, stToChange,"","","", iNumOfChars)
						ENDIF
					ELIF bCustomRoleName
					
						iNumOfChars = 61
						
						IF iProfanityToken = iProfanity_0_valid
							sTitle = "FMMC_KEY_TIP12"
						ELSE
							sTitle = "FMMC_KEY_TIP12F"
						ENDIF
						
						IF IS_STRING_NULL_OR_EMPTY(stToChange)
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc,"","","","", iNumOfChars)
						ELSE
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc, stToChange,"","","", iNumOfChars)
						ENDIF
					ELIF bCustomYachtName
					
						IF IS_XBOX_PLATFORM()
							iNumOfChars = 21
						ELSE
							iNumOfChars = 20
						ENDIF
						
						IF iProfanityToken = iProfanity_0_valid
							sTitle = "FMMC_KEY_TIP14S"
						ELIF iProfanityToken = iProfanity_error
							sTitle = "FMMC_KEY_TIP14ES"
						ELIF iProfanityToken = iProfanity_empty
							sTitle = "FMMC_KEY_TIP14OS"
						ELSE
							sTitle = "FMMC_KEY_TIP14FS"
						ENDIF
						
						PRINTLN("RUN_ON_SCREEN_KEYBOARD ", sTitle, ", iProfanityToken: ", iProfanityToken)
						
						IF IS_XBOX360_VERSION()
							sDesc = sTitle
							sTitle = "FMMC_KEY_TIP14N"
						ENDIF
						
						IF IS_STRING_NULL_OR_EMPTY(stToChange)
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc,"","","","", iNumOfChars)
						ELSE
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc, stToChange,"","","", iNumOfChars)
						ENDIF
					ELIF bCustomVehicleName	
						iNumOfChars = 15
						
						IF iProfanityToken = iProfanity_0_valid
							sTitle = "FMMC_KEY_TIP16S"
						ELIF iProfanityToken = iProfanity_error
							sTitle = "FMMC_KEY_TIP16ES"
						ELIF iProfanityToken = iProfanity_empty
							sTitle = "FMMC_KEY_TIP16OS"
						ELSE
							sTitle = "FMMC_KEY_TIP16FS"
						ENDIF
						
						IF  bInvalidCharacter
							sTitle = "FMMC_KEY_TIP16IS"
						ENDIF
						
						PRINTLN("RUN_ON_SCREEN_KEYBOARD ", sTitle, ", iProfanityToken: ", iProfanityToken)
						
						IF IS_XBOX360_VERSION()
							sDesc = sTitle
							sTitle = "FMMC_KEY_TIP16N"
						ENDIF
						
						NEXT_ONSCREEN_KEYBOARD_RESULT_WILL_DISPLAY_USING_THESE_FONTS(FONT_BIT_CONDENSED_NOT_GAMERNAME)
						
						IF IS_STRING_NULL_OR_EMPTY(stToChange)
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc,"","","","", iNumOfChars)
						ELSE
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc, stToChange,"","","", iNumOfChars)
						ENDIF
					ELIF iCustomOrginisation > 0
						
						IF IS_XBOX360_VERSION()
						OR IS_XBOX_PLATFORM()
							iNumOfChars = 16
						ELSE
							iNumOfChars = 15
						ENDIF
						IF bCanceled
							sTitle = "FMMC_KEY_TIP15B"
						ELIF bInUse
							sTitle = "FMMC_KEY_TIP15U"
						ELIF bTooShort 
							sTitle = "FMMC_KEY_TIP15N"
						ELIF bInvalidCharacter
							sTitle = "FMMC_KEY_TIP15I"
					   	ELIF iProfanityToken = iProfanity_0_valid
							IF iCustomOrginisation = 2
								sTitle = "FMMC_MCK_TIP15"
							ELSE
								sTitle = "FMMC_KEY_TIP15"
							ENDIF
						ELSE
							sTitle = "FMMC_KEY_TIP15F"
						ENDIF
						IF IS_STRING_NULL_OR_EMPTY(stToChange)
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc,"","","","", iNumOfChars)
						ELSE
							DISPLAY_ONSCREEN_KEYBOARD(ONSCREEN_KEYBOARD_ENGLISH, sTitle, sDesc, stToChange,"","","", iNumOfChars)
						ENDIF
					
					ELSE
					
						IF GET_CURRENT_LANGUAGE() = LANGUAGE_ENGLISH

							iNumOfChars = 501
							
							IF bUsingLaptopEmail
							
								IF USE_SHORT_OBJECTIVE_TEXT()

									IF iProfanityToken = iProfanity_0_valid
										sTitle = "CELL_EMASH_BOD" //Short variants.
									ELIF iProfanityToken = iProfanity_error
										sTitle = "CELL_EMASH_BODE"
									ELSE
										sTitle = "CELL_EMASH_BODF"
									ENDIF
					  
								ELSE

									IF iProfanityToken = iProfanity_0_valid
										sTitle = "CELL_EMAIL_BOD"
									ELIF iProfanityToken = iProfanity_error
										sTitle = "CELL_EMAIL_BODE"
									ELSE
										sTitle = "CELL_EMAIL_BODF"
									ENDIF
						   
								ENDIF

							ELSE

								IF iProfanityToken = iProfanity_0_valid
									sTitle = "FMMC_KEY_TIP3"
								ELIF iProfanityToken = iProfanity_error
									sTitle = "FMMC_KEY_TIP3E"
								ELSE
									sTitle = "FMMC_KEY_TIP3F"
								ENDIF
							
							ENDIF
							
						ELSE
							//Eyefind Email body may need the "251" num of chars truncated even further for other languages. Will use a dedicated routine if so.

							iNumOfChars = 251
							IF USE_SHORT_OBJECTIVE_TEXT()
								inumOfChars = 150
							ENDIF   
							
							IF bUsingLaptopEmail
							
								IF iProfanityToken = iProfanity_0_valid
									sTitle = "CELL_EMASH_BOD" //Short variants.
								ELIF iProfanityToken = iProfanity_error
									sTitle = "CELL_EMASH_BODE"
								ELSE
									sTitle = "CELL_EMASH_BODF"
								ENDIF
								
							ELSE
								
								IF USE_SHORT_OBJECTIVE_TEXT()
									IF iProfanityToken = iProfanity_0_valid
										sTitle = "FMMC_KEY_TIPSS"
									ELIF iProfanityToken = iProfanity_error
										sTitle = "FMMC_KEY_TIPSSE"
									ELSE
										sTitle = "FMMC_KEY_TIPSSF"
									ENDIF
								ELSE
									IF iProfanityToken = iProfanity_0_valid
										sTitle = "FMMC_KEY_TIPS"
									ELIF iProfanityToken = iProfanity_error
										sTitle = "FMMC_KEY_TIPSE"
									ELSE
										sTitle = "FMMC_KEY_TIPSF"
									ENDIF
								ENDIF

							ENDIF

						ENDIF
							
						IF IS_XBOX360_VERSION()
						OR IS_XBOX_PLATFORM()
							sDesc = sTitle
							sTitle = "FMMC_T0_M20"

							IF bUsingLaptopEmail
								sTitle = "CELL_EMAIL_DESC"
							ENDIF

						ENDIF
						
						INT i
						FOR i = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION - 1)
							IF IS_STRING_NULL(g_FMMC_STRUCT.tl63MissionDecription[i])
								g_FMMC_STRUCT.tl63MissionDecription[i]  = ""
							ENDIF
						ENDFOR	
						
						IF NOT IS_STRING_NULL_OR_EMPTY(sCustomTitleForWindow)
							sTitle = sCustomTitleForWindow
							PRINTLN("RUN_ON_SCREEN_KEYBOARD - Using sCustomTitleForWindow which is ", sCustomTitleForWindow)
						ENDIF
						
											
						IF bUsingLaptopEmail  //Fix for url:bugstar:2014583.
							
							//Updated for 2181546 to support the first two tl_63s reappearing when editing a message before it has been sent or cancelled.
							DISPLAY_ONSCREEN_KEYBOARD_WITH_LONGER_INITIAL_STRING(keyType, sTitle, sDesc, 
							stToChange, 
							stToChange2, 
							"", 
							"", 
							"", 
							"", 
							"", 
							"",
							iNumOfChars)

						ELSE
							DISPLAY_ONSCREEN_KEYBOARD_WITH_LONGER_INITIAL_STRING(keyType, sTitle, sDesc, 
							g_FMMC_STRUCT.tl63MissionDecription[0], 
							g_FMMC_STRUCT.tl63MissionDecription[1], 
							g_FMMC_STRUCT.tl63MissionDecription[2], 
							g_FMMC_STRUCT.tl63MissionDecription[3], 
							g_FMMC_STRUCT.tl63MissionDecription[4], 
							g_FMMC_STRUCT.tl63MissionDecription[5], 
							g_FMMC_STRUCT.tl63MissionDecription[6], 
							g_FMMC_STRUCT.tl63MissionDecription[7],
							iNumOfChars)

						ENDIF




					ENDIF
					
				ENDIF
			ENDIF
			g_tl63NameTemp = ""
			iProfanityToken = iProfanity_0_valid
			iCurrentstatus = OSK_STAGE_RUNNING
			PRINTLN("RUN_ON_SCREEN_KEYBOARD - iCurrentstatus = OSK_STAGE_RUNNING")
		BREAK
		//Wait for return status
		CASE OSK_STAGE_RUNNING
			oskStatus = UPDATE_ONSCREEN_KEYBOARD()
			SWITCH oskStatus
				//Still on screen
				CASE OSK_PENDING
					RETURN FALSE
					
				//Failed just return false and try again
				CASE OSK_FAILED
					iCurrentstatus  = OSK_STAGE_SET_UP				  
					oskStatus	   = OSK_PENDING
					PRINTLN("RUN_ON_SCREEN_KEYBOARD - state failed")
					iProfanityToken = iProfanity_0_valid
					RETURN FALSE
				
				//If the keyboard was canceled then return
				CASE OSK_CANCELLED
					PRINTLN("RUN_ON_SCREEN_KEYBOARD - OSK_CANCELLED")
					iProfanityToken = iProfanity_0_valid
					RETURN FALSE					
				
				//Accepted the input
				CASE OSK_SUCCESS
					PRINTLN("RUN_ON_SCREEN_KEYBOARD - RUN_ON_SCREEN_KEYBOARD")
					//If it's a mission name
					IF bMissionName 
					OR bCutsceneName
					OR bCustomVehicleName
						IF IS_STRING_NULL_OR_EMPTY(g_tl63NameTemp)
							//get the name
							g_tl63NameTemp = GET_ONSCREEN_KEYBOARD_RESULT()		 
							PRINTLN("RUN_ON_SCREEN_KEYBOARD - g_tl63NameTemp = ", g_tl63NameTemp)
							IF IS_STRING_NULL_OR_EMPTY(g_tl63NameTemp)
								PRINTLN("RUN_ON_SCREEN_KEYBOARD - g_tl63NameTemp = IS_STRING_NULL_OR_EMPTY")
								iProfanityToken = iProfanity_0_valid
								RETURN TRUE
							ENDIF
							//get the length
							iOriginalNameLength = GET_LENGTH_OF_LITERAL_STRING(GET_ONSCREEN_KEYBOARD_RESULT())
							//loop the sting removing spaces
							FOR iLoop = 0 TO (iOriginalNameLength - 1)
								IF iCurrentstatus != OSK_STAGE_SET_UP				   
									IF NOT IS_STRING_NULL_OR_EMPTY(g_tl63NameTemp)								  
										IF ARE_STRINGS_EQUAL(" ", GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(g_tl63NameTemp, 0, 1))		 
											iCurrentNameLength = GET_LENGTH_OF_LITERAL_STRING(g_tl63NameTemp)
											g_tl63NameTemp = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(g_tl63NameTemp, 1, iCurrentNameLength)
											PRINTLN("RUN_ON_SCREEN_KEYBOARD - g_tl63NameTemp = ", g_tl63NameTemp)
										ENDIF
									ELSE
										PRINTLN("RUN_ON_SCREEN_KEYBOARD - g_tl63NameTemp = IS_STRING_NULL_OR_EMPTY")
										iProfanityToken = iProfanity_0_valid
										RETURN TRUE
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
					IF IS_ROCKSTAR_DEV()
						PRINTLN("RUN_ON_SCREEN_KEYBOARD - Rockstar dev")
						iProfanityToken = iProfanity_0_valid
				  		RETURN TRUE
					ENDIF
					// ADDED TO SKIP THE PROFANITY CHECK.
//				  iProfanityToken = iProfanity_0_valid
//				  RETURN TRUE
										
					IF SC_PROFANITY_GET_CHECK_IS_VALID(iProfanityToken)
						IF NOT SC_PROFANITY_GET_CHECK_IS_PENDING(iProfanityToken)
							BUSYSPINNER_OFF()
							status = SC_PROFANITY_GET_STRING_STATUS(iProfanityToken)
							SWITCH(status)
								CASE PROFANITY_RESULT_OK
									PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " PASSED")
									IF bMissionTags
										g_tl63NameTemp = GET_ONSCREEN_KEYBOARD_RESULT()
										iOriginalNameLength = GET_LENGTH_OF_LITERAL_STRING(g_tl63NameTemp)
										//loop the sting removing spaces
										FOR iLoop = 0 TO (iOriginalNameLength - 1)
											IF iCurrentstatus != OSK_STAGE_SET_UP			
												IF NOT IS_STRING_NULL_OR_EMPTY(g_tl63NameTemp)		
													IF ARE_STRINGS_EQUAL(" ",GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(g_tl63NameTemp, iloop, iloop+1))
														iProfanityToken = iProfanity_contains_spaces
													ENDIF
												ENDIF
											ENDIF
										ENDFOR
									ENDIF
									
									IF iProfanityToken != iProfanity_contains_spaces
										iProfanityToken = iProfanity_0_valid
										PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " no spaces")
										RETURN TRUE
									ELSE
										iCurrentstatus  = OSK_STAGE_SET_UP
										oskStatus	   = OSK_PENDING
										PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " contains spaces")
										RETURN FALSE
									ENDIF
								BREAK
								CASE PROFANITY_RESULT_FAILED
									PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " FAILED")
									iCurrentstatus  = OSK_STAGE_SET_UP
									oskStatus	   = OSK_PENDING
									RETURN FALSE
								BREAK
								CASE PROFANITY_RESULT_ERROR
									PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " ERROR")
									iCurrentstatus  = OSK_STAGE_SET_UP
									oskStatus	   = OSK_FAILED
									iProfanityToken = iProfanity_0_valid
									SET_PLAYER_KICKED_TO_GO_OFFLINE_FROM_KEYBOARD(TRUE) //This is a sweeping change, only going into NG to give enough time to test. BC: 1839562 but this will happen throughout the game. 
									RETURN FALSE
								BREAK
							ENDSWITCH
							
						ELSE //Still pending

							IF bUsingLaptopEmail
								IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appInternet")) = 0   //AppInternet has been aborted...

									//215503 - Should be safe enough to force the token as passed regardless of input. As AppInternet is no longer running, the message can't be sent.
									//Putting this fix in for Next Gen only initially.
									PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " PASSED but appInternet aborted during Eyefind email profanity check.")
									BUSYSPINNER_OFF()
									iProfanityToken = iProfanity_0_valid
									RETURN TRUE
								ELSE
									PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " still pending for Eyefind Email.")
								ENDIF
							ELSE
								PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " still pending")
							ENDIF
						ENDIF
					ELSE
						//Hack to let content creation have empty strings on ghost rules - 2726464
						#IF IS_DEBUG_BUILD
						IF IS_ROCKSTAR_DEV()
							iProfanityToken = iProfanity_0_valid
							CPRINTLN(DEBUG_SYSTEM, "RUN_ON_SCREEN_KEYBOARD IS_ROCKSTAR_DEV")
							RETURN TRUE
						ENDIF
						#ENDIF
						IF bMissionName	
							PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " not longer valid \"", g_tl63NameTemp, "\"")
							IF bUGC
								SC_PROFANITY_CHECK_STRING_UGC(g_tl63NameTemp, iProfanityToken)
							ELSE
								SC_PROFANITY_CHECK_STRING(g_tl63NameTemp, iProfanityToken)
							ENDIF
						ELSE
							PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " not longer valid \"", GET_ONSCREEN_KEYBOARD_RESULT(), "\"")
							IF IS_STRING_NULL_OR_EMPTY(GET_ONSCREEN_KEYBOARD_RESULT())
								iProfanityToken = iProfanity_empty
								
								PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " NOT VALID")
								iCurrentstatus  = OSK_STAGE_SET_UP
								oskStatus	   = OSK_PENDING
								BUSYSPINNER_OFF()
								RETURN FALSE
							ELSE
								IF bUGC
									SC_PROFANITY_CHECK_STRING_UGC(GET_ONSCREEN_KEYBOARD_RESULT(), iProfanityToken)
								ELSE
									SC_PROFANITY_CHECK_STRING(GET_ONSCREEN_KEYBOARD_RESULT(), iProfanityToken)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_XBOX_PLATFORM()
							IF NOT NETWORK_IS_SIGNED_ONLINE()
							OR SCRIPT_IS_CLOUD_AVAILABLE() = FALSE
							OR IS_CLOUD_DOWN_CLOUD_LOADER() = TRUE
								iProfanityToken	= 0
								
								PRINTLN("RUN_ON_SCREEN_KEYBOARD - Durango, cloud down")
								iCurrentstatus	= OSK_STAGE_SET_UP
								oskStatus		= OSK_FAILED
								RETURN FALSE
							ENDIF
						ENDIF
						
						IF bCustomYachtName
							BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("ERROR_CHECKYACHTNAME")
							END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_SPINNER))
						ELIF bCustomVehicleName
							BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("ERROR_CHECKVEHICLENAME")
							END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_SPINNER))
						ELSE
							BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("ERROR_CHECKPROFANITY")
							END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_SPINNER))
						ENDIF
						
						IF NOT SC_PROFANITY_GET_CHECK_IS_VALID(iProfanityToken)
							IF NOT IS_STRING_NULL_OR_EMPTY(g_tl63NameTemp)
								iProfanityToken = iProfanity_error
							ELSE
								iProfanityToken = iProfanity_empty
							ENDIF
							
							PRINTLN("RUN_ON_SCREEN_KEYBOARD - Profanity token ", iProfanityToken, " NOT VALID")
							iCurrentstatus  = OSK_STAGE_SET_UP				  
							oskStatus	   = OSK_PENDING
							BUSYSPINNER_OFF()
							RETURN FALSE
						ENDIF
					ENDIF
					
					
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN FALSE

ENDFUNC


//PROC DEAL_WITH_ONSCREEN_KEYBOARD(OSK_STATUS &oskStatus, INT &iCurrentstatus, INT &iEntityCreationStatus)
//  IF RUN_ON_SCREEN_KEYBOARD(oskStatus, iCurrentstatus)
//	  iEntityCreationStatus = STAGE_ENTITY_SELECT_SAVE_SLOT
//	  iCurrentstatus = 0
//	  #IF IS_DEBUG_BUILD
//	  PRINTLN("Mission name accepted as [", GET_ONSCREEN_KEYBOARD_RESULT(), "]")
//	  PRINTLN("oskStatus = OSK_SUCCESS")
//	  #ENDIF
//	  #IF IS_DEBUG_BUILD
//	  IF GET_LENGTH_OF_LITERAL_STRING(GET_ONSCREEN_KEYBOARD_RESULT()) > 31
//		  g_FMMC_STRUCT.tl63MissionName = GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(GET_ONSCREEN_KEYBOARD_RESULT(), 31)
//	  ELSE
//		  g_FMMC_STRUCT.tl63MissionName = GET_ONSCREEN_KEYBOARD_RESULT()
//	  ENDIF
//	  #ENDIF
//	  #IF IS_FINAL_BUILD
//	  g_FMMC_STRUCT.tl63MissionName = GET_ONSCREEN_KEYBOARD_RESULT()
//	  #ENDIF
//	  oskStatus = OSK_PENDING
//  ENDIF
//  IF  oskStatus = OSK_CANCELLED
//	  oskStatus = OSK_PENDING
//	  iCurrentstatus = 0
//	  iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_END_MENU 
//	  #IF IS_DEBUG_BUILD
//	  PRINTLN("oskStatus = OSK_CANCELLED")
//	  #ENDIF
//  ENDIF
//ENDPROC














CONST_INT	   ciMAX_STRING_LENGTH 28
CONST_INT	   ciMIN_STRING_LENGTH 3
STRUCT KEYBOARDCurrecntSelection
	INT iCurrentTab
	INT iCurrentColoum
	INT iCurrentRow
ENDSTRUCT   

STRUCT MISSION_NAME_STRUCT
	INT						 iLengthOfString
	TEXT_LABEL_3				tl3Character[ciMAX_STRING_LENGTH]
	TEXT_LABEL_31			   tlMN
	BOOL						bCApsOn
	KEYBOARDCurrecntSelection   sKeySelection
ENDSTRUCT

CONST_INT   KEYBOARD_BUTTON_WIDTH   10
CONST_INT   KEYBOARD_BUTTON_MIDDLE  9
CONST_INT   KEYBOARD_BUTTON_BOTTOM  8
CONST_INT   KEYBOARD_HEIGHT		 4
CONST_INT   ciKEY_ALPHA			 210
CONST_FLOAT cfLINE_HEIGHT_KEY	   0.077
CONST_FLOAT cfKEY_SPACING		   0.047
CONST_FLOAT cfKEY_WIDTH			 0.045
CONST_FLOAT cfKEY_HEIGHT			0.074
CONST_FLOAT cfLINE_0_X			  0.289
CONST_FLOAT cfLINE_1_X			  0.289
CONST_FLOAT cfLINE_2_X			  0.3125
CONST_FLOAT cfLINE_3_X			  0.336
CONST_FLOAT cfTEXT_SPACE			0.0016
CONST_FLOAT cfKEY_LINE_Y			0.517
CONST_FLOAT cfTEXT_LINE_Y		   0.029
CONST_FLOAT cfLINE_CAPS_WIDTH	   0.068
CONST_FLOAT cfLINE_CAPS_X		   0.011
CONST_FLOAT cfBUTTON_DONE		   0.674
CONST_FLOAT cfBUTTON_ACCEPT		 0.614
CONST_FLOAT cfBUTTON_BACKSPACE	  0.47
CONST_FLOAT cfBUTTON_SPACE		  0.542
CONST_FLOAT cfBUTTON_CLEAR		  0.412
CONST_FLOAT cfBUTTON_TEXT_Y		 0.794
CONST_FLOAT cfBUTTON_TEXT_SCALE	 0.4

PROC SETUP_KEYBOARD_TEXT()
	SET_TEXT_SCALE(0.0000, 0.6800)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_CENTRE(TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
ENDPROC 

FUNC STRING GET_KEYBOARD_BUTTON_PRESSED(KEYBOARDCurrecntSelection sCurrentSelection, BOOL bCAps)
	IF sCurrentSelection.iCurrentRow = 0
			SWITCH sCurrentSelection.iCurrentColoum
				 CASE 0 RETURN "1"
				 CASE 1 RETURN "2"
				 CASE 2 RETURN "3"
				 CASE 3 RETURN "4"
				 CASE 4 RETURN "5"
				 CASE 5 RETURN "6"
				 CASE 6 RETURN "7"
				 CASE 7 RETURN "8"
				 CASE 8 RETURN "9"
				 CASE 9 RETURN "0"
			ENDSWITCH
	ENDIF
	IF bCAps
		IF sCurrentSelection.iCurrentRow = 1
			SWITCH sCurrentSelection.iCurrentColoum
				 CASE 0 RETURN "Q"
				 CASE 1 RETURN "W"
				 CASE 2 RETURN "E"
				 CASE 3 RETURN "R"
				 CASE 4 RETURN "T"
				 CASE 5 RETURN "Y"
				 CASE 6 RETURN "U"
				 CASE 7 RETURN "I"
				 CASE 8 RETURN "O"
				 CASE 9 RETURN "P"
			ENDSWITCH
		ELIF sCurrentSelection.iCurrentRow = 2
			SWITCH sCurrentSelection.iCurrentColoum
				 CASE 0 RETURN "A"
				 CASE 1 RETURN "S"
				 CASE 2 RETURN "D"
				 CASE 3 RETURN "F"
				 CASE 4 RETURN "G"
				 CASE 5 RETURN "H"
				 CASE 6 RETURN "J"
				 CASE 7 RETURN "K"
				 CASE 8 RETURN "L"
				 CASE 9 RETURN ""
			ENDSWITCH
		ELIF sCurrentSelection.iCurrentRow = 3
			SWITCH sCurrentSelection.iCurrentColoum
				 CASE 0 RETURN "Z"
				 CASE 1 RETURN "X"
				 CASE 2 RETURN "C"
				 CASE 3 RETURN "V"
				 CASE 4 RETURN "B"
				 CASE 5 RETURN "N"
				 CASE 6 RETURN "M"
				 CASE 7 RETURN ""
				 CASE 8 RETURN ""
				 CASE 9 RETURN ""
			ENDSWITCH
		ENDIF
	ELSE
		IF sCurrentSelection.iCurrentRow = 1
			SWITCH sCurrentSelection.iCurrentColoum
				 CASE 0 RETURN "q"
				 CASE 1 RETURN "w"
				 CASE 2 RETURN "e"
				 CASE 3 RETURN "r"
				 CASE 4 RETURN "t"
				 CASE 5 RETURN "y"
				 CASE 6 RETURN "u"
				 CASE 7 RETURN "i"
				 CASE 8 RETURN "o"
				 CASE 9 RETURN "p"
			ENDSWITCH
		ELIF sCurrentSelection.iCurrentRow = 2
			SWITCH sCurrentSelection.iCurrentColoum
				 CASE 0 RETURN "a"
				 CASE 1 RETURN "s"
				 CASE 2 RETURN "d"
				 CASE 3 RETURN "f"
				 CASE 4 RETURN "g"
				 CASE 5 RETURN "h"
				 CASE 6 RETURN "j"
				 CASE 7 RETURN "k"
				 CASE 8 RETURN "l"
				 CASE 9 RETURN ""
			ENDSWITCH
		ELIF sCurrentSelection.iCurrentRow = 3
			SWITCH sCurrentSelection.iCurrentColoum
				 CASE 0 RETURN "z"
				 CASE 1 RETURN "x"
				 CASE 2 RETURN "c"
				 CASE 3 RETURN "v"
				 CASE 4 RETURN "b"
				 CASE 5 RETURN "n"
				 CASE 6 RETURN "m"
				 CASE 7 RETURN ""
				 CASE 8 RETURN ""
				 CASE 9 RETURN ""
			ENDSWITCH
		ENDIF
	ENDIF
	RETURN ""   
ENDFUNC


//
//FUNC BOOL DO_KEYBOARD_MENU(MISSION_NAME_STRUCT &missionNameData)
//  INT i
//  TEXT_LABEL_15 tl15
//  TEXT_LABEL_3 tl3
//  TEXT_LABEL_7 tl7 = "FMMC_K"
//  IF missionNameData.bCApsOn = TRUE
//	  tl3 = "CAP"
//  ELSE
//	  tl3 = "NOR"
//  ENDIF
//  
//  FOR i = 0 TO 36
//	  IF i < 10
//		  tl15 = tl7
//		  tl15 += "NUM"
//		  tl15 += i+1
//		  IF i = 9
//			  tl15 = "FMMC_KNUM0"
//		  ENDIF
//		  IF missionNameData.sKeySelection.iCurrentRow = 0
//		  AND missionNameData.sKeySelection.iCurrentColoum = i
//			  DRAW_RECT(cfLINE_0_X+ (i * cfKEY_SPACING), cfKEY_LINE_Y, cfKEY_WIDTH, cfKEY_HEIGHT, 63, 63, 63, ciKEY_ALPHA)
//		  ELSE
//			  DRAW_RECT(cfLINE_0_X + (i * cfKEY_SPACING), cfKEY_LINE_Y, cfKEY_WIDTH, cfKEY_HEIGHT, 0, 0, 0, ciKEY_ALPHA)
//		  ENDIF
//		  SETUP_KEYBOARD_TEXT()
//		  DISPLAY_TEXT(cfLINE_0_X + cfTEXT_SPACE + (i * cfKEY_SPACING), cfKEY_LINE_Y - cfTEXT_LINE_Y, tl15)
//	  ELSE
//		  tl15 = tl7
//		  tl15 += tl3
//		  tl15 += (i-10)
//		  IF i < 20
//			  IF missionNameData.sKeySelection.iCurrentRow = 1
//			  AND missionNameData.sKeySelection.iCurrentColoum = i-10
//				  DRAW_RECT(cfLINE_1_X + ((i - 10) * cfKEY_SPACING), cfKEY_LINE_Y+(cfLINE_HEIGHT_KEY *1), cfKEY_WIDTH, cfKEY_HEIGHT, 63, 63, 63, ciKEY_ALPHA)
//			  ELSE
//				  DRAW_RECT(cfLINE_1_X + ((i - 10) * cfKEY_SPACING), cfKEY_LINE_Y+(cfLINE_HEIGHT_KEY *1), cfKEY_WIDTH, cfKEY_HEIGHT, 0, 0, 0, ciKEY_ALPHA)
//			  ENDIF
//			  SETUP_KEYBOARD_TEXT()
//			  DISPLAY_TEXT(cfLINE_1_X + cfTEXT_SPACE + ((i - 10) * cfKEY_SPACING), cfKEY_LINE_Y - cfTEXT_LINE_Y+(cfLINE_HEIGHT_KEY *1), tl15)
//		  ELIF i < 29
//			  IF missionNameData.sKeySelection.iCurrentRow = 2
//			  AND missionNameData.sKeySelection.iCurrentColoum = i-20
//				  DRAW_RECT(cfLINE_2_X + ((i - 20) * cfKEY_SPACING),cfKEY_LINE_Y+(cfLINE_HEIGHT_KEY *2), cfKEY_WIDTH, cfKEY_HEIGHT, 63, 63, 63, ciKEY_ALPHA)
//			  ELSE
//				  DRAW_RECT(cfLINE_2_X + ((i - 20) * cfKEY_SPACING),cfKEY_LINE_Y+(cfLINE_HEIGHT_KEY *2), cfKEY_WIDTH, cfKEY_HEIGHT, 0, 0, 0, ciKEY_ALPHA)
//			  ENDIF
//			  SETUP_KEYBOARD_TEXT()
//			  DISPLAY_TEXT(cfLINE_2_X + cfTEXT_SPACE + ((i - 20) * cfKEY_SPACING), cfKEY_LINE_Y - cfTEXT_LINE_Y+(cfLINE_HEIGHT_KEY *2), tl15)
//		  ELIF i < 36
//			  IF missionNameData.sKeySelection.iCurrentRow = 3
//			  AND missionNameData.sKeySelection.iCurrentColoum = i-29
//				  DRAW_RECT(cfLINE_3_X + ((i - 29) * cfKEY_SPACING), cfKEY_LINE_Y+(cfLINE_HEIGHT_KEY *3), cfKEY_WIDTH, cfKEY_HEIGHT, 63, 63, 63, ciKEY_ALPHA)
//			  ELSE
//				  DRAW_RECT(cfLINE_3_X + ((i - 29) * cfKEY_SPACING), cfKEY_LINE_Y+(cfLINE_HEIGHT_KEY *3), cfKEY_WIDTH, cfKEY_HEIGHT, 0, 0, 0, ciKEY_ALPHA)
//			  ENDIF
//			  SETUP_KEYBOARD_TEXT()
//			  DISPLAY_TEXT(cfLINE_3_X + cfTEXT_SPACE + ((i - 29) * cfKEY_SPACING), (cfKEY_LINE_Y - cfTEXT_LINE_Y) +(cfLINE_HEIGHT_KEY *3), tl15)
//		  ELIF i = 36
//			  IF missionNameData.sKeySelection.iCurrentRow = 3
//			  AND missionNameData.sKeySelection.iCurrentColoum = i-29
//				  DRAW_RECT(cfLINE_3_X + cfLINE_CAPS_X + ((i - 29) * cfKEY_SPACING) ,cfKEY_LINE_Y+(cfLINE_HEIGHT_KEY *3), cfLINE_CAPS_WIDTH, cfKEY_HEIGHT, 63, 63, 63, ciKEY_ALPHA)
//			  ELSE
//				  DRAW_RECT(cfLINE_3_X + cfLINE_CAPS_X + ((i - 29) * cfKEY_SPACING) , cfKEY_LINE_Y+(cfLINE_HEIGHT_KEY *3), cfLINE_CAPS_WIDTH , cfKEY_HEIGHT, 0, 0, 0, ciKEY_ALPHA)
//			  ENDIF
//			  SETUP_KEYBOARD_TEXT()
//			  DISPLAY_TEXT(cfLINE_3_X + cfLINE_CAPS_X + ((i - 29) * cfKEY_SPACING), (cfKEY_LINE_Y - cfTEXT_LINE_Y)+(cfLINE_HEIGHT_KEY *3), "FMMC_KNOR26")
//		  ENDIF
//	  ENDIF
//	  //PRINTSTRING(tl15)PRINTNL()	
//  ENDFOR
//  
//  
//  DRAW_RECT(0.547, 0.810, 0.325, 0.035,   0, 0, 0, ciKEY_ALPHA)
//  
//  SETUP_KEYBOARD_TEXT()	   
//  SET_TEXT_SCALE(0.0000, cfBUTTON_TEXT_SCALE)
//  DISPLAY_TEXT(cfBUTTON_DONE, cfBUTTON_TEXT_Y, "FMMC_MNYS")
//  
//  SETUP_KEYBOARD_TEXT()
//  SET_TEXT_SCALE(0.0000, cfBUTTON_TEXT_SCALE)
//  DISPLAY_TEXT(cfBUTTON_ACCEPT, cfBUTTON_TEXT_Y , "FMMC_KOK")
//  
//  SETUP_KEYBOARD_TEXT()
//  SET_TEXT_SCALE(0.0000, cfBUTTON_TEXT_SCALE)
//  DISPLAY_TEXT(cfBUTTON_BACKSPACE, cfBUTTON_TEXT_Y , "FMMC_KSP")
//  
//  SETUP_KEYBOARD_TEXT()
//  SET_TEXT_SCALE(0.0000, cfBUTTON_TEXT_SCALE)
//  DISPLAY_TEXT(cfBUTTON_SPACE, cfBUTTON_TEXT_Y , "FMMC_KDL")
//
//  SETUP_KEYBOARD_TEXT()
//  SET_TEXT_SCALE(0.0000, cfBUTTON_TEXT_SCALE)
//  DISPLAY_TEXT(cfBUTTON_CLEAR, cfBUTTON_TEXT_Y, "FMMC_KDLA")
//  
//  
//  //IF IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
//  IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
//	  missionNameData.sKeySelection.iCurrentRow++
//	  IF missionNameData.sKeySelection.iCurrentRow > KEYBOARD_HEIGHT-1
//		  missionNameData.sKeySelection.iCurrentRow = 0
//	  ENDIF
//	  IF missionNameData.sKeySelection.iCurrentRow = 2
//		  IF missionNameData.sKeySelection.iCurrentColoum > KEYBOARD_BUTTON_MIDDLE - 1
//			  missionNameData.sKeySelection.iCurrentColoum = KEYBOARD_BUTTON_MIDDLE - 1
//		  ENDIF
//	  ELIF missionNameData.sKeySelection.iCurrentRow = 3
//		  IF missionNameData.sKeySelection.iCurrentColoum > KEYBOARD_BUTTON_BOTTOM - 1
//			  missionNameData.sKeySelection.iCurrentColoum = KEYBOARD_BUTTON_BOTTOM - 1
//		  ENDIF
//	  ENDIF
//  ENDIF
//
//  //IF IS_BUTTON_JUST_PRESSED(PAD1, DPADUP)
//  IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
//	  missionNameData.sKeySelection.iCurrentRow--
//	  IF missionNameData.sKeySelection.iCurrentRow < 0
//		  missionNameData.sKeySelection.iCurrentRow = KEYBOARD_HEIGHT - 1
//	  ENDIF
//	  IF missionNameData.sKeySelection.iCurrentRow = 2
//		  IF missionNameData.sKeySelection.iCurrentColoum > KEYBOARD_BUTTON_MIDDLE - 1
//			  missionNameData.sKeySelection.iCurrentColoum = KEYBOARD_BUTTON_MIDDLE - 1
//		  ENDIF
//	  ELIF missionNameData.sKeySelection.iCurrentRow = 3
//		  IF missionNameData.sKeySelection.iCurrentColoum > KEYBOARD_BUTTON_BOTTOM - 1
//			  missionNameData.sKeySelection.iCurrentColoum = KEYBOARD_BUTTON_BOTTOM - 1
//		  ENDIF
//	  ENDIF
//  ENDIF
//
////	IF IS_BUTTON_JUST_PRESSED(PAD1, DPADRIGHT)
//  IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
//	  missionNameData.sKeySelection.iCurrentColoum++  
//	  IF missionNameData.sKeySelection.iCurrentRow = 0
//		  IF missionNameData.sKeySelection.iCurrentColoum > KEYBOARD_BUTTON_WIDTH - 1
//			  missionNameData.sKeySelection.iCurrentColoum = 0
//		  ENDIF
//	  ELIF missionNameData.sKeySelection.iCurrentRow = 1
//		  IF missionNameData.sKeySelection.iCurrentColoum > KEYBOARD_BUTTON_WIDTH - 1
//			  missionNameData.sKeySelection.iCurrentColoum = 0
//		  ENDIF
//	  ELIF missionNameData.sKeySelection.iCurrentRow = 2
//		  IF missionNameData.sKeySelection.iCurrentColoum > KEYBOARD_BUTTON_MIDDLE - 1
//			  missionNameData.sKeySelection.iCurrentColoum = 0
//		  ENDIF
//	  ELIF missionNameData.sKeySelection.iCurrentRow = 3
//		  IF missionNameData.sKeySelection.iCurrentColoum > KEYBOARD_BUTTON_BOTTOM - 1
//			  missionNameData.sKeySelection.iCurrentColoum = 0
//		  ENDIF
//	  ENDIF
//  ENDIF
//  //IF IS_BUTTON_JUST_PRESSED(PAD1, DPADLEFT)
//  IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
//	  missionNameData.sKeySelection.iCurrentColoum--
//	  IF missionNameData.sKeySelection.iCurrentRow = 0
//		  IF missionNameData.sKeySelection.iCurrentColoum < 0
//			  missionNameData.sKeySelection.iCurrentColoum = KEYBOARD_BUTTON_WIDTH - 1
//		  ENDIF
//	  ELIF missionNameData.sKeySelection.iCurrentRow = 1
//		  IF missionNameData.sKeySelection.iCurrentColoum < 0
//			  missionNameData.sKeySelection.iCurrentColoum = KEYBOARD_BUTTON_WIDTH - 1
//		  ENDIF
//	  ELIF missionNameData.sKeySelection.iCurrentRow = 2
//		  IF missionNameData.sKeySelection.iCurrentColoum < 0
//			  missionNameData.sKeySelection.iCurrentColoum = KEYBOARD_BUTTON_MIDDLE - 1
//		  ENDIF
//	  ELIF missionNameData.sKeySelection.iCurrentRow = 3
//		  IF missionNameData.sKeySelection.iCurrentColoum < 0
//			  missionNameData.sKeySelection.iCurrentColoum = KEYBOARD_BUTTON_BOTTOM - 1
//		  ENDIF
//	  ENDIF   
//  ENDIF
//  
//  IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) 
//	  IF IS_THIS_FLOATING_HELP_TEXT_BEING_DISPLAYED(FLOATING_HELP_TEXT_ID_2, "FMMC_KB_HELP")
//		  CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_2)
//	  ENDIF
//	  IF missionNameData.sKeySelection.iCurrentRow = 3
//	  AND missionNameData.sKeySelection.iCurrentColoum = 7
//		  IF missionNameData.bCApsOn
//			  missionNameData.bCApsOn = FALSE
//		  ELSE
//			  missionNameData.bCApsOn = TRUE
//		  ENDIF
//	  ELSE
//		  IF missionNameData.iLengthOfString < (ciMAX_STRING_LENGTH-1)
//			  missionNameData.tl3Character[missionNameData.iLengthOfString] = GET_KEYBOARD_BUTTON_PRESSED(missionNameData.sKeySelection, missionNameData.bCApsOn)
//			  missionNameData.iLengthOfString++
//		  ENDIF
//	  ENDIF
//  ENDIF
//  
//  IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)  
//	  IF IS_THIS_FLOATING_HELP_TEXT_BEING_DISPLAYED(FLOATING_HELP_TEXT_ID_2, "FMMC_KB_HELP")
//		  CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_2)
//	  ENDIF
//	  missionNameData.iLengthOfString=0
//	  FOR i = 0 to (ciMAX_STRING_LENGTH - 1)
//		  missionNameData.tl3Character[i] = ""
//	  ENDFOR
//  ENDIF
//  
//  IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)  
//	  IF IS_THIS_FLOATING_HELP_TEXT_BEING_DISPLAYED(FLOATING_HELP_TEXT_ID_2, "FMMC_KB_HELP")
//		  CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_2)
//	  ENDIF
//	  IF missionNameData.iLengthOfString < (ciMAX_STRING_LENGTH -1)
//		  missionNameData.tl3Character[missionNameData.iLengthOfString] = " "
//		  missionNameData.iLengthOfString++
//	  ENDIF
//  ENDIF
//  IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) 
//	  IF IS_THIS_FLOATING_HELP_TEXT_BEING_DISPLAYED(FLOATING_HELP_TEXT_ID_2, "FMMC_KB_HELP")
//		  CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_2)
//	  ENDIF
//	  IF missionNameData.iLengthOfString > 0
//		  missionNameData.tl3Character[missionNameData.iLengthOfString] = ""
//		  missionNameData.iLengthOfString--
//	  ENDIF
//  ENDIF
//  
//  missionNameData.tlMN=""
//  FOR i = 0 TO (missionNameData.iLengthOfString - 1)
//	  missionNameData.tlMN+= missionNameData.tl3Character[i]
//  ENDFOR
//  //Draw the mission name
//  SET_TEXT_SCALE(0.0000, 0.600)
//  SET_TEXT_COLOUR(255, 255, 255, 255)
//  SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
//  SET_TEXT_EDGE(0, 0, 0, 0, 0)
//  SET_TEXT_CENTRE(FALSE)
//  SET_TEXT_JUSTIFY(FALSE)
//  BEGIN_TEXT_COMMAND_DISPLAY_TEXT("FMMC_MN")
//	  ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(missionNameData.tlMN)
//	  SET_TEXT_CENTRE(FALSE)	  
//  END_TEXT_COMMAND_DISPLAY_TEXT(0.269, 0.426) 
//  DRAW_RECT(0.5003, 0.445 , 0.467,  0.065,   0, 0, 0, ciKEY_ALPHA)
//  
//  //Draw the input title
//  SET_TEXT_SCALE(0.0000, 0.600)
//  SET_TEXT_COLOUR(255, 255, 255, 255)
//  SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
//  SET_TEXT_EDGE(0, 0, 0, 0, 0)
//  SET_TEXT_CENTRE(FALSE)
//  SET_TEXT_JUSTIFY(FALSE)
//  DISPLAY_TEXT(0.269, 0.366, "FMMC_MNT")  
//  DRAW_RECT(0.333, 0.385 , 0.132,  0.050,   0, 0, 0, ciKEY_ALPHA)
//  
//  
//  IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT) 
//	  IF missionNameData.iLengthOfString >= ciMIN_STRING_LENGTH
//		  IF IS_THIS_FLOATING_HELP_TEXT_BEING_DISPLAYED(FLOATING_HELP_TEXT_ID_2, "FMMC_KB_HELP")
//			  CLEAR_FLOATING_HELP(FLOATING_HELP_TEXT_ID_2)
//		  ENDIF
//		  RETURN TRUE
//	  ELSE
//		  HELP_AT_SCREEN_LOCATION("FMMC_KB_HELP", 0.317, 0.347,  HELP_TEXT_SOUTH, 15000, FLOATING_HELP_AMBIENT_SLOT_1)
//	  ENDIF
//  ENDIF
//  
//  RETURN FALSE
//ENDFUNC



