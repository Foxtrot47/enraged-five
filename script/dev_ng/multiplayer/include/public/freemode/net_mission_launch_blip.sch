USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "net_mission.sch"
USING "net_common_functions.sch"
USING "net_gang_boss_launcher.sch"

USING "net_mission_launch_blip_data.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This file is for core behaviour to the Mission Launch Blip system.
// Please use "net_mission_launch_blip_data" for data/behaviour for specific blips you are adding/modifying - REB
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////
///   ACTIVE CHECKS
/////////////////////////

/// PURPOSE:
///    These are checks that prevent the system running for any of the blips, such as critical to other missions or in corona, etc.
FUNC BOOL MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS()
		
	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - bNetworkIsActivitySession")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - IS_PLAYER_IN_CORONA")			ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - !IS_SKYSWOOP_AT_GROUND")			ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_PERMANENT_TO_GANG_BOSS_MISSION()
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - GB_IS_LOCAL_PLAYER_PERMANENT_TO_GANG_BOSS_MISSION")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(MPGlobals.sFreemodeCache.PlayerIndex)
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_AN_ANIMAL()
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - IS_LOCAL_PLAYER_AN_ANIMAL")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - IS_PLAYER_ON_MP_INTRO")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_USING_ANY_RC_VEHICLE(MPGlobals.sFreemodeCache.PlayerIndex)
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - IS_PLAYER_USING_ANY_RC_VEHICLE")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_TEST_DRIVING_A_VEHICLE(MPGlobals.sFreemodeCache.PlayerIndex)
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - IS_PLAYER_TEST_DRIVING_A_VEHICLE")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.sFreemodeCache.PlayerIndex)].iCurrentMissionType = FMMC_TYPE_IMPROMPTU_DM
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - FMMC_TYPE_IMPROMPTU_DM")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(MPGlobals.sFreemodeCache.PlayerIndex, FMMC_TYPE_RACE_TO_POINT)
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - FMMC_TYPE_RACE_TO_POINT")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_TEST_DRIVING_A_VEHICLE(MPGlobals.sFreemodeCache.PlayerIndex)
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sMissionLaunchBlip.bExtraDebug	PRINTLN("[MLB] MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS - FALSE - IS_PLAYER_TEST_DRIVING_A_VEHICLE")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


/////////////////////////
///   BLIP
/////////////////////////

/// PURPOSE:
///    Creates the desired blip look using MLB_GET_BLIP_DATA
PROC MLB_CREATE_MISSION_LAUNCH_BLIP(MISSION_LAUNCH_BLIPS eMissionBlip)
	IF NOT DOES_BLIP_EXIST(MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip])
		VECTOR vCoord
		BLIP_SPRITE eSprite
		HUD_COLOURS eColour
		FLOAT fScale
		BOOL bShortRange, bFlash
		TEXT_LABEL_15 txtName
		
		IF MLB_GET_BLIP_DATA(eMissionBlip, vCoord, eSprite, eColour, fScale, bShortRange, bFlash, txtName)
			MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip] = ADD_BLIP_FOR_COORD(vCoord)
			SET_BLIP_SPRITE(MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip], eSprite)
			SET_BLIP_COLOUR(MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip], GET_BLIP_COLOUR_FROM_HUD_COLOUR(eColour))
			SET_BLIP_SCALE(MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip], fScale)
			SET_BLIP_AS_SHORT_RANGE(MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip], bShortRange)
			SET_BLIP_NAME_FROM_TEXT_FILE(MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip], txtName)
			
			IF bFlash
				SET_BLIP_FLASHES(MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip], TRUE)
				SET_BLIP_FLASH_INTERVAL(MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip], BLIP_FLASHING_TIME)
				SET_BLIP_FLASH_TIMER(MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip], FLASH_CONTRABAND_BLIP_DEFAULT_TIME)
			ENDIF
			
			MLB_SET_BIT(eMissionBlip, eMLBBITSET_CREATED_BLIP)
			PRINTLN("[", MLB_GET_DEBUG_NAME(eMissionBlip), "] MLB_CREATE_MISSION_LAUNCH_BLIP - ", vCoord, " - ", GET_BLIP_SPRITE_DEBUG_STRING(eSprite), " - ", HUD_COLOUR_AS_STRING(eColour), " - fScale: ", fScale, " - bShortRange: ", bShortRange, " - bFlash: ", bFlash, " - txtName: ", txtName)
		ENDIF
	ENDIF
ENDPROC

/////////////////////////
///   LAUNCH
/////////////////////////

FUNC BOOL MLB_IS_MISSION_LAUNCHING(MISSION_LAUNCH_BLIPS eMissionBlip)
	UNUSED_PARAMETER(eMissionBlip)
	RETURN GB_IS_PLAYER_LAUNCHING_THIS_GANG_BOSS_MISSION(MPGlobals.sFreemodeCache.PlayerIndex, MLB_GET_MISSION_LAUNCH_FMMC(eMissionBlip))
ENDFUNC

/////////////////////////
///   CORONA
/////////////////////////

/// PURPOSE:
///    Draws a corona based on MLB_GET_CORONA_DATA
PROC MLB_DRAW_MISSION_LAUNCH_CORONA(MISSION_LAUNCH_BLIPS eMissionBlip)
	INT iR, iG, iB, iA
	HUD_COLOURS eColour
	FLOAT fScale
	
	MLB_GET_CORONA_DATA(eMissionBlip, eColour, fScale)
	GET_HUD_COLOUR(eColour, iR, iG, iB, iA)	
	
	VECTOR vCoords = MLB_GET_MISSION_LAUNCH_COORD(eMissionBlip)
	vCoords.z -= 0.4
		
	DRAW_MARKER(MARKER_CYLINDER, vCoords, <<0,0,0>>, <<0,0,0>>, <<fScale, fScale, 1.0>>, iR, iG, iB, 150)
ENDPROC

////////////////////////////////////////////////
///   UNAVAILABLE REASONS (ALT HELP IN PROMPT)
////////////////////////////////////////////////

/// PURPOSE:
///    Used to implement soft restrictions that inform the player why they are currently unable to launch; things the player can change easily, such as losing wanted level or leaving a gang
FUNC BOOL MLB_HANDLE_SOFT_UNAVAILABLE_REASONS(MISSION_LAUNCH_BLIPS eMissionBlip)
	// Can't be launched with wanted level
	IF IS_PLAYER_WANTED_LEVEL_GREATER(MPGlobals.sFreemodeCache.PlayerIndex, 0) 
		PRINT_HELP("MLB_H_UNWL")
		RETURN TRUE
	ENDIF
	
	// Can't be launched as a Boss if MLB_CAN_LAUNCH_AS_BOSS is false 
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
	AND NOT MLB_CAN_LAUNCH_AS_BOSS(eMissionBlip)
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
			PRINT_HELP("MLB_H_UNMC2")
		ELSE
			PRINT_HELP("MLB_H_UNORG2")
		ENDIF
		RETURN TRUE
	ENDIF
		
	// Can't be launched as a gang member
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG(FALSE)
			PRINT_HELP("MLB_H_UNMC")
		ELSE
			PRINT_HELP("MLB_H_UNORG")
		ENDIF
		RETURN TRUE
	ENDIF
	
	// Can't be launched with passengers in current vehicle
	IF MPGlobals.sFreemodeCache.bIsPlayerInAnyVehicle
		VEHICLE_INDEX vehId = GET_VEHICLE_PED_IS_IN(MPGlobals.sFreemodeCache.PlayerPedIndex)
		IF DOES_ENTITY_EXIST(vehID)
		AND NOT IS_VEHICLE_EMPTY(vehId,TRUE,TRUE,TRUE,TRUE)
			PRINT_HELP("MLB_H_UNPASS")
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Tidying away specific unavailable reasons for each mission type. 
	// Please put mission specific reasons in the func below and leave this func for common blockers.
	IF MLB_HANDLE_CUSTOM_SOFT_UNAVAILABLE_REASONS(eMissionBlip)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/////////////////
///   CLEANUP
/////////////////

PROC MLB_CLEANUP_MISSION_LAUNCH_BLIP_PROMPT(MISSION_LAUNCH_BLIPS eMissionBlip)
	IF MLB_IS_BIT_SET(eMissionBlip, eMLBBITSET_PROMPT_DISPLAYING)
	AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(MLB_GET_LAUNCH_PROMPT_STRING(eMissionBlip))
		CLEAR_HELP()
		PRINTLN("[", MLB_GET_DEBUG_NAME(eMissionBlip), "] MLB_CLEANUP_MISSION_LAUNCH_BLIP - Clearing help.") 
	ENDIF
	MLB_CLEAR_BIT(eMissionBlip, eMLBBITSET_PROMPT_DISPLAYING)
ENDPROC

PROC MLB_CLEANUP_MISSION_LAUNCH_BLIP_INDEX(MISSION_LAUNCH_BLIPS eMissionBlip)
	IF MLB_IS_BIT_SET(eMissionBlip, eMLBBITSET_CREATED_BLIP)
	AND DOES_BLIP_EXIST(MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip])
		REMOVE_BLIP(MPGlobalsAmbience.sMissionLaunchBlip.missionBlip[eMissionBlip])
		PRINTLN("[", MLB_GET_DEBUG_NAME(eMissionBlip), "] MLB_CLEANUP_MISSION_LAUNCH_BLIP - Removing blip.") 
	ENDIF
	MLB_CLEAR_BIT(eMissionBlip, eMLBBITSET_CREATED_BLIP)
ENDPROC

/// PURPOSE:
///    Cleans up the mission launch blip system for the passed blip; removing blip, cleaning up data
PROC MLB_CLEANUP_MISSION_LAUNCH_BLIP(MISSION_LAUNCH_BLIPS eMissionBlip)
	MLB_CUSTOM_CLEANUP_BEHAVIOUR(eMissionBlip)

	MLB_CLEANUP_MISSION_LAUNCH_BLIP_INDEX(eMissionBlip)
	MLB_CLEANUP_MISSION_LAUNCH_BLIP_PROMPT(eMissionBlip)
	MLB_CLEAR_BIT(eMissionBlip, eMLBBITSET_TRIGGERED_LAUNCH)
	MLB_CLEAR_BIT(eMissionBlip, eMLBBITSET_BLIP_UPDATE_REQUIRED)
ENDPROC

PROC MLB_CLEANUP_ALL_MISSION_LAUNCH_BLIPS()
	INT iMissionBlip
	REPEAT eMLB_MAX iMissionBlip
		MLB_CLEANUP_MISSION_LAUNCH_BLIP(INT_TO_ENUM(MISSION_LAUNCH_BLIPS, iMissionBlip))
	ENDREPEAT
ENDPROC

//////////////////
///   MAINTAIN
//////////////////

/// PURPOSE:
///    A common system to allow the launching of freemode mission content from a blip in world on the map
PROC MAINTAIN_MISSION_LAUNCH_BLIPS()
	IF NOT MLB_SHOULD_RUN_MISSION_LAUNCH_BLIPS()
		MLB_CLEANUP_ALL_MISSION_LAUNCH_BLIPS()
		EXIT
	ENDIF
	
	FLOAT fDist
	BOOL bOffMission = (NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(MPGlobals.sFreemodeCache.PlayerIndex) AND NOT GB_IS_PLAYER_IN_LAUNCH_GANG(MPGlobals.sFreemodeCache.PlayerIndex))
	BOOL bMissionLaunching
	
	INT iMissionBlip
	REPEAT eMLB_MAX iMissionBlip
		MISSION_LAUNCH_BLIPS eMissionBlip = INT_TO_ENUM(MISSION_LAUNCH_BLIPS, iMissionBlip)
		bMissionLaunching = MLB_IS_MISSION_LAUNCHING(eMissionBlip)
		
		IF NOT MLB_SHOULD_BLIP_BE_ACTIVE(eMissionBlip)
		OR NOT bOffMission
		OR bMissionLaunching
		OR MLB_IS_BIT_SET(eMissionBlip, eMLBBITSET_BLIP_UPDATE_REQUIRED)
			MLB_CLEANUP_MISSION_LAUNCH_BLIP(eMissionBlip)
			RELOOP
		ENDIF
		
		IF NOT MLB_IS_BIT_SET(eMissionBlip, eMLBBITSET_CREATED_BLIP)
			MLB_CREATE_MISSION_LAUNCH_BLIP(eMissionBlip)
		ELIF MLB_SHOULD_DRAW_CORONA(eMissionBlip)
			MLB_DRAW_MISSION_LAUNCH_CORONA(eMissionBlip)
		ENDIF

		IF bOffMission
			fDist = VDIST(MPGlobals.sFreemodeCache.vPlayersLocation, MLB_GET_MISSION_LAUNCH_COORD(eMissionBlip))
			MLB_MAINTAIN_CUSTOM_RANGE_BEHAVIOUR(eMissionBlip, fDist)
			IF fDist < MLB_GET_LAUNCH_PROMPT_RANGE(eMissionBlip)
				IF MLB_HANDLE_SOFT_UNAVAILABLE_REASONS(eMissionBlip)
					RELOOP
				ENDIF
				
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
					PRINTLN("[", MLB_GET_DEBUG_NAME(eMissionBlip), "] MAINTAIN_MISSION_LAUNCH_BLIPS - INPUT_CONTEXT pressed to launch mission.") 
					MLB_SET_BIT(eMissionBlip, eMLBBITSET_TRIGGERED_LAUNCH)
					GB_SET_GLOBAL_CLIENT_BIT2(eGB_GLOBAL_CLIENT_BITSET_2_LAUNCHED_FM_MISSION_FROM_BLIP)
					MLB_ON_LAUNCH_PROMPT_BEHAVIOUR(eMissionBlip)
					MLB_CLEANUP_MISSION_LAUNCH_BLIP_PROMPT(eMissionBlip)
				ELSE
					PRINT_HELP(MLB_GET_LAUNCH_PROMPT_STRING(eMissionBlip))
					MLB_SET_BIT(eMissionBlip, eMLBBITSET_PROMPT_DISPLAYING)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
