/// FM_Maintain_Strand_Mission
/// Bobby - 27/06/2014
/// functionality for dealing with the strand missions
///    
USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_cutscene.sch"

//Sets that this is a starnd based mission
PROC SET_THIS_IS_A_STRAND_MISSION()
	PRINTLN("[TS] [MSRAND] - SET_THIS_IS_A_STRAND_MISSION")
	g_sTransitionSessionData.sStrandMissionData.bIsThisAStrandMission = TRUE
ENDPROC
//Clear that this is a strand based mission
PROC CLEAR_THIS_IS_A_STRAND_MISSION()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sStrandMissionData.bIsThisAStrandMission
		PRINTLN("[TS] [MSRAND] - CLEAR_THIS_IS_A_STRAND_MISSION")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.bIsThisAStrandMission = FALSE
ENDPROC	
//This is true from as soon as the mission is downlaoded until the last mission is over
FUNC BOOL IS_THIS_IS_A_STRAND_MISSION()
	RETURN g_sTransitionSessionData.sStrandMissionData.bIsThisAStrandMission
ENDFUNC	

//Sets that the first mission has been passed, cleaned up after use
PROC SET_FIRST_STRAND_MISSION_BEEN_PASSED()
	PRINTLN("[TS] [MSRAND] - SET_FIRST_STRAND_MISSION_BEEN_PASSED")
	g_sTransitionSessionData.sStrandMissionData.bPassedFirstMission = TRUE
ENDPROC
//Clear that the first mission has been passed, cleaned up after use
PROC CLEAR_FIRST_STRAND_MISSION_BEEN_PASSED()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sStrandMissionData.bPassedFirstMission
		PRINTLN("[TS] [MSRAND] - CLEAR_FIRST_STRAND_MISSION_BEEN_PASSED")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.bPassedFirstMission = FALSE
ENDPROC	
//has the first mission been passed, cleaned up after use
FUNC BOOL HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
	RETURN g_sTransitionSessionData.sStrandMissionData.bPassedFirstMission
ENDFUNC	

// Speirs added 2132454 ///////////////////////////////////////////////////////
//Sets that the first mission has been passed, cleaned up after use
PROC SET_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
	PRINTLN("[TS] [MSRAND] - SET_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET")
	g_sTransitionSessionData.sStrandMissionData.bPassedFirstStrandNoReset = TRUE
ENDPROC
//Clear that the first mission has been passed, cleaned up after use
PROC CLEAR_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sStrandMissionData.bPassedFirstStrandNoReset
		PRINTLN("[TS] [MSRAND] - CLEAR_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.bPassedFirstStrandNoReset = FALSE
ENDPROC	
//has the first mission been passed, cleaned up after use
FUNC BOOL HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
	RETURN g_sTransitionSessionData.sStrandMissionData.bPassedFirstStrandNoReset
ENDFUNC	
//////////////////////////////////////////////////////////////////////////////

//Set's that this is the last strand mission
PROC SET_THIS_IS_THE_LAST_STRAND_MISSION()
	PRINTLN("[TS] [MSRAND] - SET_THIS_IS_THE_LAST_STRAND_MISSION")
	g_sTransitionSessionData.sStrandMissionData.bLastMission = TRUE
ENDPROC
//Clear that this is the last strand mission
PROC CLEAR_THIS_IS_THE_LAST_STRAND_MISSION()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sStrandMissionData.bLastMission
		PRINTLN("[TS] [MSRAND] - CLEAR_THIS_IS_THE_LAST_STRAND_MISSION")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.bLastMission = FALSE
ENDPROC	
//Is this the last strand mission
FUNC BOOL IS_THIS_THE_LAST_IN_A_SERIES_OF_STRAND_MISSIONS()
	RETURN g_sTransitionSessionData.sStrandMissionData.bLastMission
ENDFUNC	

//Loops all the strrand mission content IDs to see if they are set, if not then it's the last mission in the strand
FUNC BOOL CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
	//PRINTLN("[TS] [MSRAND] - CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION")
	BOOL bLastMission = TRUE
	INT iLoop
	REPEAT FMMC_MAX_STRAND_MISSIONS iLoop
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[iLoop])
			//PRINTLN("[TS] [MSRAND] - CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION - g_FMMC_STRUCT.tl23NextContentID[", iLoop, "] = ", g_FMMC_STRUCT.tl23NextContentID[iLoop])
			bLastMission = FALSE
		ENDIF
	ENDREPEAT
	//There are no strand missions left then set that this is the last one
	RETURN bLastMission
ENDFUNC

PROC SET_FIRST_STRAND_MISSION_CONTENT_ID(STRING stFirstContentID)
	PRINTLN("[TS] [MSRAND] - SET_FIRST_STRAND_MISSION_CONTENT_ID - ", stFirstContentID)
	g_sTransitionSessionData.sStrandMissionData.tl23FirstContentID = stFirstContentID
ENDPROC	
PROC CLEAR_FIRST_STRAND_MISSION_CONTENT_ID()
	#IF IS_DEBUG_BUILD
	IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.tl23FirstContentID)
		PRINTLN("[TS] [MSRAND] - CLEAR_FIRST_STRAND_MISSION_CONTENT_ID")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.tl23FirstContentID = ""
ENDPROC
FUNC TEXT_LABEL_23 GET_FIRST_STRAND_MISSION_CONTENT_ID()
	RETURN g_sTransitionSessionData.sStrandMissionData.tl23FirstContentID
ENDFUNC

PROC SET_FIRST_STRAND_MISSION_STARTING_VECTOR(VECTOR vFirstStartPos)
	PRINTLN("[TS] [MSRAND] - SET_FIRST_STRAND_MISSION_STARTING_VECTOR - ", vFirstStartPos)
	g_sTransitionSessionData.sStrandMissionData.vFirstStartPos = vFirstStartPos
ENDPROC	
FUNC BOOL IS_FIRST_STRAND_MISSION_STARTING_VECTOR_SET()
	IF g_sTransitionSessionData.sStrandMissionData.vFirstStartPos.x	!= 0.0
	OR g_sTransitionSessionData.sStrandMissionData.vFirstStartPos.y	!= 0.0
	OR g_sTransitionSessionData.sStrandMissionData.vFirstStartPos.z	!= 0.0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
PROC CLEAR_FIRST_STRAND_MISSION_STARTING_VECTOR()
	#IF IS_DEBUG_BUILD
	IF IS_FIRST_STRAND_MISSION_STARTING_VECTOR_SET()
		PRINTLN("[TS] [MSRAND] - CLEAR_FIRST_STRAND_MISSION_STARTING_VECTOR")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.vFirstStartPos = <<0.0, 0.0, 0.0>>
ENDPROC
FUNC VECTOR GET_FIRST_STRAND_MISSION_STARTING_VECTOR()
	RETURN g_sTransitionSessionData.sStrandMissionData.vFirstStartPos
ENDFUNC


FUNC BOOL IS_CURRENTLY_LOADED_STRAND_MISSION_SET_RESTART_AT_FIRST_MISSION()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sStrandMissionData.bRestartAtStartDebug
		RETURN TRUE
	ENDIF
	#ENDIF
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciSTART_AT_FIRST_OF_STRAND)
ENDFUNC

//Sets that if we restart we start on the first mission
PROC SET_STRAND_MISSION_RESTART_ON_FIRST_MISSION(BOOL bSet)
	IF bSet
	#IF IS_DEBUG_BUILD
	OR g_sTransitionSessionData.sStrandMissionData.bRestartAtStartDebug
		g_sTransitionSessionData.sStrandMissionData.bRestartAtStartDebug = FALSE
		#ENDIF
		PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_RESTART_ON_FIRST_MISSION")
		g_sTransitionSessionData.sStrandMissionData.bRestartAtStart = TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		IF g_sTransitionSessionData.sStrandMissionData.bRestartAtStart
			PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_RESTART_ON_FIRST_MISSION")
		ENDIF
		#ENDIF
		CLEAR_FIRST_STRAND_MISSION_STARTING_VECTOR()
		CLEAR_FIRST_STRAND_MISSION_CONTENT_ID()
		g_sTransitionSessionData.sStrandMissionData.bRestartAtStart = FALSE
	ENDIF
ENDPROC
//Clear that if we restart we start on the first mission
PROC CLEAR_STRAND_MISSION_RESTART_ON_FIRST_MISSION()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sStrandMissionData.bRestartAtStart
		PRINTLN("[TS] [MSRAND] - CLEAR_STRAND_MISSION_RESTART_ON_FIRST_MISSION")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.bRestartAtStart = FALSE
ENDPROC	
//checks to see if we restart we start on the first mission
FUNC BOOL SHOULD_STRAND_MISSION_RESTART_ON_FIRST_MISSION()
	RETURN g_sTransitionSessionData.sStrandMissionData.bRestartAtStart
ENDFUNC	

//Sets that this is a starnd based mission
PROC SET_THIS_A_STRAND_MISSION_BEING_INITIALISED()
	PRINTLN("[TS] [MSRAND] - SET_THIS_A_STRAND_MISSION_BEING_INITIALISED")
	SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_A_STRAND_MISSION_BEING_INITIALISED)
ENDPROC
//Clear that this is a strand based mission
PROC CLEAR_THIS_A_STRAND_MISSION_BEING_INITIALISED()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_A_STRAND_MISSION_BEING_INITIALISED)
		PRINTLN("[TS] [MSRAND] - CLEAR_THIS_A_STRAND_MISSION_BEING_INITIALISED")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_A_STRAND_MISSION_BEING_INITIALISED)
ENDPROC	
//THis is true from when control on a mission is removed until it's returned on the next mission on a strand
//returns true when a strand mission is being set up
FUNC BOOL IS_A_STRAND_MISSION_BEING_INITIALISED()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_A_STRAND_MISSION_BEING_INITIALISED)
ENDFUNC	

//Sets that this is a starnd based mission
PROC STRAND_MISSION_SET_SKIP_INTRO_IN_MC()
	PRINTLN("[TS] [MSRAND] - STRAND_MISSION_SET_SKIP_INTRO_IN_MC")
	SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_SKIP_INTRO)
ENDPROC
//Clear that this is a strand based mission
PROC STRAND_MISSION_CLEAR_SKIP_INTRO_IN_MC()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_SKIP_INTRO)
		PRINTLN("[TS] [MSRAND] - STRAND_MISSION_CLEAR_SKIP_INTRO_IN_MC")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_SKIP_INTRO)
ENDPROC	
//THis is true from when control on a mission is removed until it's returned on the next mission on a strand
//returns true when a strand mission is being set up
FUNC BOOL STRAND_MISSION_SHOULD_SKIP_INTRO_IN_MC()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_SKIP_INTRO)
ENDFUNC	


PROC STRAND_MISSION_SET_MPH_PAC_FIN_MCS1_CUT_JUST_FINISHED()
	PRINTLN("[TS] [MSRAND] - STRAND_MISSION_SET_MPH_PAC_FIN_MCS1_CUT_JUST_FINISHED")
	SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_MPH_PAC_FIN_MCS1_CUT)
ENDPROC
PROC STRAND_MISSION_CLEAR_MPH_PAC_FIN_MCS1_CUT_JUST_FINISHED()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_MPH_PAC_FIN_MCS1_CUT)
		PRINTLN("[TS] [MSRAND] - STRAND_MISSION_CLEAR_MPH_PAC_FIN_MCS1_CUT_JUST_FINISHED")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_MPH_PAC_FIN_MCS1_CUT)
ENDPROC	
FUNC BOOL STRAND_MISSION_MPH_PAC_FIN_MCS1_CUT_JUST_FINISHED()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_MPH_PAC_FIN_MCS1_CUT)
ENDFUNC	



PROC STRAND_MISSION_SET_MPH_HUM_KEY_MCS1_CUT_PLAYING()
	PRINTLN("[TS] [MSRAND] - STRAND_MISSION_SET_MPH_HUM_KEY_MCS1_CUT_PLAYING")
	SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_MPH_HUM_KEY_MCS1_CUT)
ENDPROC
PROC STRAND_MISSION_CLEAR_MPH_HUM_KEY_MCS1_CUT_PLAYING()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_MPH_HUM_KEY_MCS1_CUT)
		PRINTLN("[TS] [MSRAND] - STRAND_MISSION_CLEAR_MPH_HUM_KEY_MCS1_CUT_PLAYING")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_MPH_HUM_KEY_MCS1_CUT)
ENDPROC	
FUNC BOOL STRAND_MISSION_MPH_HUM_KEY_MCS1_CUT_PLAYING()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_MPH_HUM_KEY_MCS1_CUT)
ENDFUNC	



//Sets that the next mission has been downloaded and set up
PROC SET_NEXT_STRAND_MISSION_HAS_BEEN_DOWNLOADED()
	PRINTLN("[TS] [MSRAND] - SET_NEXT_STRAND_MISSION_HAS_BEEN_DOWNLOADED")
	SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_HAS_BEEN_DOWNLOADED)
ENDPROC
//Clear that the next mission has been downloaded and set up
PROC CLEAR_NEXT_STRAND_MISSION_HAS_BEEN_DOWNLOADED()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_HAS_BEEN_DOWNLOADED)
		PRINTLN("[TS] [MSRAND] - CLEAR_NEXT_STRAND_MISSION_HAS_BEEN_DOWNLOADED")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_HAS_BEEN_DOWNLOADED)
ENDPROC	
//Has the next mission been downloaded and set up?
FUNC BOOL HAS_NEXT_STRAND_MISSION_HAS_BEEN_DOWNLOADED()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_HAS_BEEN_DOWNLOADED)
ENDFUNC	

//Sets that the next missionis ready to set up
PROC SET_NEXT_STRAND_MISSION_READY_TO_SET_UP()
	PRINTLN("[TS] [MSRAND] - SET_NEXT_STRAND_MISSION_READY_TO_SET_UP")
	SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_TO_SET_UP)
ENDPROC
//Clear that the next missionis ready to set up
PROC CLEAR_NEXT_STRAND_MISSION_READY_TO_SET_UP()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_TO_SET_UP)
		PRINTLN("[TS] [MSRAND] - CLEAR_NEXT_STRAND_MISSION_READY_TO_SET_UP")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_TO_SET_UP)
ENDPROC	
//Has the next missionis ready to set up
FUNC BOOL IS_NEXT_STRAND_MISSION_READY_TO_SET_UP()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_TO_SET_UP)
ENDFUNC	


//Sets that the mission controller is ready for cutscene set up
PROC SET_MISSION_CONTROLLER_READY_FOR_CUT()
	PRINTLN("[TS] [MSRAND] - SET_MISSION_CONTROLLER_READY_FOR_CUT")
	SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_CONTROLLER_READY_FOR_CUT)
ENDPROC
//Clear that the mission controller is ready for cutscene set up
PROC CLEAR_MISSION_CONTROLLER_READY_FOR_CUT()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_CONTROLLER_READY_FOR_CUT)
		PRINTLN("[TS] [MSRAND] - CLEAR_MISSION_CONTROLLER_READY_FOR_CUT")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_CONTROLLER_READY_FOR_CUT)
ENDPROC	
//Is the mission controller is ready for cutscene set up
FUNC BOOL IS_MISSION_CONTROLLER_READY_FOR_CUT()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_CONTROLLER_READY_FOR_CUT)
ENDFUNC	

//Sets that the mission controller is ready to start the cutscene
PROC SET_MISSION_CONTROLLER_READY_TO_START_CUTSCENE()
	PRINTLN("[TS] [MSRAND] - SET_MISSION_CONTROLLER_READY_TO_START_CUTSCENE")
	SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_START_CUTSCENE)
ENDPROC
//Clear that the mission controller is ready to start the cutscene
PROC CLEAR_MISSION_CONTROLLER_READY_TO_START_CUTSCENE()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_START_CUTSCENE)
		PRINTLN("[TS] [MSRAND] - CLEAR_MISSION_CONTROLLER_READY_TO_START_CUTSCENE")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_START_CUTSCENE)
ENDPROC	
//Is the mission controller ready for the cutscene to start?
FUNC BOOL IS_MISSION_CONTROLLER_READY_TO_START_CUTSCENE()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_START_CUTSCENE)
ENDFUNC	

//Sets that the mission controller is ready to start the cutscene
PROC SET_READY_TO_DELETE_STRAND_TRANSITION_CUTSCENE_ENTITIES()
	IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_READY_FOR_STRAND_CUTSCENE_TRANSITION_END)
		PRINTLN("[TS] [MSRAND] - SET_PLAYER_READY_TO_DELETE_STRAND_TRANSITION_CUTSCENE_ENTITIES")
		SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_READY_FOR_STRAND_CUTSCENE_TRANSITION_END)
	ENDIF
ENDPROC
//Clear that the mission controller is ready to start the cutscene
PROC CLEAR_READY_TO_DELETE_STRAND_TRANSITION_CUTSCENE_ENTITIES()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_READY_FOR_STRAND_CUTSCENE_TRANSITION_END)
		PRINTLN("[TS] [MSRAND] - CLEAR_PLAYER_READY_TO_DELETE_STRAND_TRANSITION_CUTSCENE_ENTITIES")
	ENDIF
	#ENDIF
	CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_READY_FOR_STRAND_CUTSCENE_TRANSITION_END)
ENDPROC	

//Is the mission controller ready for the cutscene to start?
FUNC BOOL IS_PLAYER_READY_TO_DELETE_STRAND_TRANSITION_CUTSCENE_ENTITIES(INT iPlayer)
	RETURN IS_BIT_SET(GlobalplayerBD_FM_2[iPlayer].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_READY_FOR_STRAND_CUTSCENE_TRANSITION_END)
ENDFUNC	

FUNC BOOL ARE_ALL_PLAYERS_SYNCED_FOR_CUTSCENE_END()
	
	INT iPlayer
	FOR iPlayer = 0 TO NUM_NETWORK_PLAYERS-1
		
		PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		
		IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
			
			IF IS_BIT_SET(GlobalplayerBD_FM[iPlayer].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedAsSpectator)
			OR IS_BIT_SET(GlobalplayerBD_FM[iPlayer].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
				RELOOP
			ENDIF
			
			IF GlobalplayerBD_FM[iPlayer].scoreData.iTeam = TEAM_SCTV
			OR GET_PLAYER_TEAM(piPlayer) = TEAM_SCTV
				RELOOP
			ENDIF
			
			IF NOT IS_PLAYER_READY_TO_DELETE_STRAND_TRANSITION_CUTSCENE_ENTITIES(iPlayer)
				PRINTLN("[LM][ARE_ALL_PLAYERS_SYNCED_FOR_CUTSCENE_END] Waiting for ", iPlayer, " so returning FALSE") 
				RETURN FALSE
			ENDIF		
		ENDIF
	ENDFOR
	
	PRINTLN("[LM][ARE_ALL_PLAYERS_SYNCED_FOR_CUTSCENE_END] SYNCED, returning TRUE")
	
	RETURN TRUE
ENDFUNC

//Sets that the mission controller should now continue
PROC SET_MISSION_CONTROLLER_CONTINUE_AFTER_CUTSCENE_AND_RESTART()
	PRINTLN("[TS] [MSRAND] - SET_MISSION_CONTROLLER_CONTINUE_AFTER_CUTSCENE_AND_RESTART")
	SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_CONTINUE_AFTER_RESTART)
ENDPROC
//Clears that the mission controller should now continue
PROC CLEAR_MISSION_CONTROLLER_CONTINUE_AFTER_CUTSCENE_AND_RESTART()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_CONTINUE_AFTER_RESTART)
		PRINTLN("[TS] [MSRAND] - CLEAR_MISSION_CONTROLLER_CONTINUE_AFTER_CUTSCENE_AND_RESTART")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_CONTINUE_AFTER_RESTART)
ENDPROC	
//should the mission controller now continue
FUNC BOOL SHOULD_MISSION_CONTROLLER_CONTINUE_AFTER_CUTSCENE_AND_RESTART()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_CONTINUE_AFTER_RESTART)
ENDFUNC	

//Sets that the mission controller should now continue
PROC SET_MISSION_CONTROLLER_READY_FOR_RESET()
	PRINTLN("[TS] [MSRAND] - SET_MISSION_CONTROLLER_READY_FOR_RESET")
	SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_FOR_RESET)
ENDPROC
//Clears that the mission controller should now continue
PROC CLEAR_MISSION_CONTROLLER_READY_FOR_RESET()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_FOR_RESET)
		PRINTLN("[TS] [MSRAND] - CLEAR_MISSION_CONTROLLER_READY_FOR_RESET")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_FOR_RESET)
ENDPROC	
//should the mission controller now continue
FUNC BOOL HAS_MISSION_CONTROLLER_SAID_READY_FOR_RESET()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_FOR_RESET)
ENDFUNC	



//Sets that the mission controller is ready to start the cutscene
PROC SET_MISSION_CONTROLLER_JOINED_AS_SPECTATOR_FOR_CUTSCENE()
	PRINTLN("[TS] [MSRAND] - SET_MISSION_CONTROLLER_JOINED_AS_SPECTATOR_FOR_CUTSCENE")
	SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_JOINED_AS_SPECTATOR)
ENDPROC
//Clear that the mission controller is ready to start the cutscene
PROC CLEAR_MISSION_CONTROLLER_JOINED_AS_SPECTATOR_FOR_CUTSCENE()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_JOINED_AS_SPECTATOR)
		PRINTLN("[TS] [MSRAND] - CLEAR_MISSION_CONTROLLER_JOINED_AS_SPECTATOR_FOR_CUTSCENE")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_JOINED_AS_SPECTATOR)
ENDPROC	
//Is the mission controller ready for the cutscene to start?
FUNC BOOL DID_MISSION_CONTROLLER_JOIN_AS_SPECTATOR_FOR_CUTSCENE()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_JOINED_AS_SPECTATOR)
ENDFUNC	

//Set the array pos of g_FMMC_STRUCT.tl23NextContentID[iMission] that we should be using
PROC SET_STRAND_MISSION_CONTENT_ID_ARRAY_POS(INT iArray)
	PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_CONTENT_ID_ARRAY_POS - ", iArray)
	g_sTransitionSessionData.sStrandMissionData.iArrayPos = iArray
ENDPROC	
//Clear the array pos of g_FMMC_STRUCT.tl23NextContentID[iMission] that we should be using
PROC CLEAR_STRAND_MISSION_CONTENT_ID_ARRAY_POS()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sStrandMissionData.iArrayPos != -1
		PRINTLN("[TS] [MSRAND] - CLEAR_STRAND_MISSION_CONTENT_ID_ARRAY_POS")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.iArrayPos = -1
ENDPROC
//get the array pos of g_FMMC_STRUCT.tl23NextContentID[iMission] that we should be using
FUNC INT GET_STRAND_MISSION_CONTENT_ID_ARRAY_POS()
	RETURN g_sTransitionSessionData.sStrandMissionData.iArrayPos
ENDFUNC


//Set the team that we were in on the first strand mission
PROC SET_STRAND_MISSION_FIRST_MISSION_TEAM(INT iFirstMissionTeam)
	PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_FIRST_MISSION_TEAM - ", iFirstMissionTeam)
	g_sTransitionSessionData.sStrandMissionData.iFirstMissionTeam = iFirstMissionTeam
ENDPROC	
//Clear the team that we were in on the first strand mission
PROC CLEAR_STRAND_MISSION_FIRST_MISSION_TEAM()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sStrandMissionData.iFirstMissionTeam != -1
		PRINTLN("[TS] [MSRAND] - CLEAR_STRAND_MISSION_FIRST_MISSION_TEAM")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.iFirstMissionTeam = -1
ENDPROC
//get the team that we were in on the first strand mission
FUNC INT GET_STRAND_MISSION_FIRST_MISSION_TEAM()
	RETURN g_sTransitionSessionData.sStrandMissionData.iFirstMissionTeam
ENDFUNC

//Set the array pos of g_FMMC_STRUCT.tl23NextContentID[iMission] that we have loaded
PROC SET_LOADED_MISSION_CONTENT_ID_ARRAY_POS(INT iArrayPosLoaded)
	PRINTLN("[TS] [MSRAND] - SET_LOADED_MISSION_CONTENT_ID_ARRAY_POS - ", iArrayPosLoaded)
	g_sTransitionSessionData.sStrandMissionData.iArrayPosLoaded = iArrayPosLoaded
ENDPROC	
//Clear the array pos of g_FMMC_STRUCT.tl23NextContentID[iMission] that we have loaded
PROC CLEAR_LOADED_MISSION_CONTENT_ID_ARRAY_POS()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sStrandMissionData.iArrayPosLoaded != -1
		PRINTLN("[TS] [MSRAND] - CLEAR_LOADED_MISSION_CONTENT_ID_ARRAY_POS")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.iArrayPosLoaded = -1
ENDPROC
//get the array pos of g_FMMC_STRUCT.tl23NextContentID[iMission] that we have loaded
FUNC INT GET_LOADED_MISSION_CONTENT_ID_ARRAY_POS()
	RETURN g_sTransitionSessionData.sStrandMissionData.iArrayPosLoaded
ENDFUNC

PROC STORE_STRAND_MISSION_END_CUTSCENE_TYPE(INT iEndCutscene)
	PRINTLN("[TS] [MSRAND] - STORE_STRAND_MISSION_END_CUTSCENE_TYPE iEndCutscene: ", iEndCutscene)
	g_sTransitionSessionData.sStrandMissionData.iEndCutscene = iEndCutscene
ENDPROC	
PROC CLEAR_STRAND_MISSION_END_CUTSCENE_TYPE()
	PRINTLN("[TS] [MSRAND] - CLEAR_STRAND_MISSION_END_CUTSCENE_TYPE = ciMISSION_CUTSCENE_DEFAULT")
	g_sTransitionSessionData.sStrandMissionData.iEndCutscene = ciMISSION_CUTSCENE_DEFAULT
ENDPROC
FUNC INT GET_STRAND_MISSION_END_CUTSCENE_TYPE()
	RETURN g_sTransitionSessionData.sStrandMissionData.iEndCutscene
ENDFUNC

PROC SET_STRAND_MISSION_TRANSITION_TYPE(FMMC_BRANCHING_MISSION_TRANSITION_TYPE eBranchingTransitionType)
	PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_TRANSITION_TYPE - ", ENUM_TO_INT(eBranchingTransitionType))
	g_sTransitionSessionData.sStrandMissionData.eBranchingTransitionType = eBranchingTransitionType
ENDPROC	
PROC CLEAR_STRAND_MISSION_TRANSITION_TYPE()
	PRINTLN("[TS] [MSRAND] - CLEAR_STRAND_MISSION_TRANSITION_TYPE")
	g_sTransitionSessionData.sStrandMissionData.eBranchingTransitionType = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
ENDPROC
FUNC FMMC_BRANCHING_MISSION_TRANSITION_TYPE GET_STRAND_MISSION_TRANSITION_TYPE()
	RETURN g_sTransitionSessionData.sStrandMissionData.eBranchingTransitionType
ENDFUNC

FUNC BOOL IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
	
	IF GET_STRAND_MISSION_TRANSITION_TYPE() != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK
		RETURN FALSE
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND NOT ((g_bEndOfMissionCleanUpHUD OR g_bMissionEnding) AND IS_THIS_IS_A_STRAND_MISSION())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC SET_STRAND_MISSION_CONTENT_ID(STRING stContentID)
	PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_CONTENT_ID - ", stContentID)
	g_sTransitionSessionData.sStrandMissionData.tl23NextContentID = stContentID
ENDPROC	
PROC CLEAR_STRAND_MISSION_CONTENT_ID()
	#IF IS_DEBUG_BUILD
	IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.tl23NextContentID)
		PRINTLN("[TS] [MSRAND] - CLEAR_STRAND_MISSION_CONTENT_ID")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.tl23NextContentID = ""
ENDPROC
FUNC TEXT_LABEL_23 GET_STRAND_MISSION_CONTENT_ID()
	RETURN g_sTransitionSessionData.sStrandMissionData.tl23NextContentID
ENDFUNC

PROC SET_STRAND_MISSION_CUTSCENE_NAME(STRING sMocapName)
	#IF IS_DEBUG_BUILD
	IF IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.tl23MocapName)	
	AND NOT IS_STRING_NULL_OR_EMPTY(sMocapName)	
		PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_CUTSCENE_NAME - ", sMocapName)
	ELIF NOT IS_STRING_NULL_OR_EMPTY(sMocapName)		
	AND NOT ARE_STRINGS_EQUAL(sMocapName, g_sTransitionSessionData.sStrandMissionData.tl23MocapName)		
		PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_CUTSCENE_NAME - ", sMocapName)
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.tl23MocapName = sMocapName
ENDPROC	
FUNC TEXT_LABEL_23 GET_STRAND_MISSION_CUTSCENE_NAME()
	RETURN g_sTransitionSessionData.sStrandMissionData.tl23MocapName
ENDFUNC

PROC SET_STRAND_MISSION_STARTING_VECTOR(VECTOR vStartPos)
	PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_STARTING_VECTOR - ", vStartPos)
	g_sTransitionSessionData.sStrandMissionData.vStartPos = vStartPos
ENDPROC	
FUNC BOOL IS_STRAND_MISSION_STARTING_VECTOR_SET()
	IF g_sTransitionSessionData.sStrandMissionData.vStartPos.x	!= 0.0
	OR g_sTransitionSessionData.sStrandMissionData.vStartPos.y	!= 0.0
	OR g_sTransitionSessionData.sStrandMissionData.vStartPos.z	!= 0.0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
PROC CLEAR_STRAND_MISSION_STARTING_VECTOR()
	#IF IS_DEBUG_BUILD
	IF IS_STRAND_MISSION_STARTING_VECTOR_SET()
		PRINTLN("[TS] [MSRAND] - CLEAR_STRAND_MISSION_STARTING_VECTOR")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.vStartPos = <<0.0, 0.0, 0.0>>
ENDPROC
FUNC VECTOR GET_STRAND_MISSION_STARTING_VECTOR()
	RETURN g_sTransitionSessionData.sStrandMissionData.vStartPos
ENDFUNC

PROC SET_STRAND_MISSION_TIME_OF_DAY_FOR_CUT(INT iTOD)
	g_sTransitionSessionData.sStrandMissionData.iTOD = iTOD
ENDPROC
FUNC INT GET_STRAND_MISSION_TIME_OF_DAY_FOR_CUT()
	RETURN g_sTransitionSessionData.sStrandMissionData.iTOD
ENDFUNC

// AMEC HEISTS - Added to save the start of a mission strand for use in stats upon strand completion.
PROC SET_STRAND_ROOT_CONTENT_ID_HASH(INT iHash)
	IF g_sTransitionSessionData.sStrandMissionData.iRootContentID = 0
		PRINTLN("[AMEC][HEIST_MISC] - SET_STRAND_ROOT_CONTENT_ID_HASH - ", iHash)
		g_sTransitionSessionData.sStrandMissionData.iRootContentID = iHash
	ELSE
		PRINTLN("[AMEC][HEIST_MISC] - SET_STRAND_ROOT_CONTENT_ID_HASH - g_sTransitionSessionData.sStrandMissionData.iRootContentID already set")
	ENDIF
ENDPROC	
PROC CLEAR_STRAND_ROOT_CONTENT_ID_HASH()
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.sStrandMissionData.iRootContentID != 0
		PRINTLN("[AMEC][HEIST_MISC] - CLEAR_STRAND_ROOT_CONTENT_ID_HASH")
	ENDIF
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.iRootContentID = 0
ENDPROC
FUNC INT GET_STRAND_ROOT_CONTENT_ID_HASH()
	RETURN g_sTransitionSessionData.sStrandMissionData.iRootContentID
ENDFUNC

PROC SET_MY_STRAND_MISSION_TEAM(INT iMyTeam)
	PRINTLN("[TS] [MSRAND] - SET_MY_STRAND_MISSION_TEAM - g_sTransitionSessionData.sStrandMissionData.iMyTeam = ", iMyTeam)		
	g_sTransitionSessionData.sStrandMissionData.iMyTeam = iMyTeam
ENDPROC	
FUNC INT GET_MY_STRAND_MISSION_TEAM()
	RETURN g_sTransitionSessionData.sStrandMissionData.iMyTeam
ENDFUNC


FUNC INT GET_PED_STRAND_MISSION_MASK(INT iPlayer = 0)
	RETURN g_sTransitionSessionData.sStrandMissionData.iMyMask[iPlayer]
ENDFUNC

//Sets that the next mission has been downloaded and set up
PROC SET_STRAND_MISSION_READY_TO_START_DOWNLOAD(INT iContentIdArrayPos = 0)
	IF NOT IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_TO_START_DOWNLOAD)
		IF IS_THIS_IS_A_STRAND_MISSION()
		OR IS_A_STRAND_MISSION_BEING_INITIALISED()
			IF GET_STRAND_MISSION_CONTENT_ID_ARRAY_POS() = -1
				PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_READY_TO_START_DOWNLOAD - content ID Array Pos was -1 - setting to ", iContentIdArrayPos)
				SET_STRAND_MISSION_CONTENT_ID_ARRAY_POS(iContentIdArrayPos)
			ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[GET_STRAND_MISSION_CONTENT_ID_ARRAY_POS()])
				SET_STRAND_MISSION_CONTENT_ID(g_FMMC_STRUCT.tl23NextContentID[GET_STRAND_MISSION_CONTENT_ID_ARRAY_POS()])
				SET_STRAND_MISSION_TRANSITION_TYPE(g_FMMC_STRUCT.eBranchingTransitionType[GET_STRAND_MISSION_CONTENT_ID_ARRAY_POS()])
				PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_READY_TO_START_DOWNLOAD")
				SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_TO_START_DOWNLOAD)
			ENDIF	
		ENDIF
	ENDIF
ENDPROC
//Clear that the next mission has been downloaded and set up
PROC CLEAR_STRAND_MISSION_READY_TO_START_DOWNLOAD()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_TO_START_DOWNLOAD)
		PRINTLN("[TS] [MSRAND] - CLEAR_STRAND_MISSION_READY_TO_START_DOWNLOAD")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_TO_START_DOWNLOAD)
ENDPROC	
//Has the next mission been downloaded and set up?
FUNC BOOL IS_STRAND_MISSION_READY_TO_START_DOWNLOAD()
	RETURN IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_MISSION_READY_TO_START_DOWNLOAD)
ENDFUNC	

PROC MAINTAIN_PASS_OVER_ALARMS_FROM_MC_TO_FMMC_LAUNCHER()
	INT iLoop	
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iAlarmNumber - 1)
		IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iAlarmBitSet, iLoop)
			IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.oiAlarm[iLoop])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiAlarm[iLoop])
					SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiAlarm[iLoop], FALSE, TRUE)	
					IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.oiAlarm[iLoop])
						SET_NETWORK_ID_CAN_MIGRATE(OBJ_TO_NET(g_sTransitionSessionData.sStrandMissionData.oiAlarm[iLoop]), FALSE)
					ENDIF
					CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iAlarmBitSet, iLoop)
					g_sTransitionSessionData.sStrandMissionData.iSoundId[iLoop] = -1
					PRINTLN("[TS] [MSRAND] [ALARM] - MAINTAIN_PASS_OVER_ALARMS_FROM_MC_TO_FMMC_LAUNCHER - SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiAlarm[", iLoop, "], FALSE, TRUE)")
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[TS] [MSRAND] [ALARM] - MAINTAIN_PASS_OVER_ALARMS_FROM_MC_TO_FMMC_LAUNCHER - NETWORK_HAS_CONTROL_OF_ENTITYY(g_sTransitionSessionData.sStrandMissionData.oiAlarm[", iLoop, "], FALSE, TRUE)")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDFOR
ENDPROC

PROC PASS_OVER_MISSION_MOCAP_ORIGINAL_PED_CLONE(PED_INDEX pedIndex, PLAYER_INDEX piPlayer, INT i, INT iTeam)
	g_sTransitionSessionData.piMocapOriginalPedClones[iTeam][i] = pedIndex
	g_sTransitionSessionData.piMocapOriginalPlayer[iTeam][i] = piPlayer	
ENDPROC

PROC MAINTAIN_MISSION_MOCAP_ORIGINAL_PED_CLONE()
	INT i, iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		FOR i = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS-1)	
			IF DOES_ENTITY_EXIST(g_sTransitionSessionData.piMocapOriginalPedClones[iTeam][i])
				SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.piMocapOriginalPedClones[iTeam][i], FALSE, TRUE)
			ENDIF
		ENDFOR
	ENDFOR
ENDPROC

PROC MAINTAIN_PASS_OVER_CONTROL_FROM_MC_TO_FMMC_LAUNCHER()
	INT iLoop	
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberVeh - 1)
		IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iVehBitSet, iLoop)
			IF IS_VEHICLE_DRIVEABLE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
				IF IS_ENTITY_A_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
					SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop], FALSE, TRUE)					
					PRINTLN("[TS] [MSRAND] - MAINTAIN_PASS_OVER_CONTROL_FROM_MC_TO_FMMC_LAUNCHER - SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.viCut[", iLoop, "], FALSE, TRUE)")
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[TS] [MSRAND] - MAINTAIN_PASS_OVER_CONTROL_FROM_MC_TO_FMMC_LAUNCHER - IS_ENTITY_A_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.viCut[", iLoop, "]) = FALSE")
					#ENDIF
				ENDIF
				CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iVehBitSet, iLoop)
			ENDIF
		ENDIF
	ENDFOR
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberObj - 1)
		IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iObjBitSet, iLoop)
			IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
				SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop], FALSE, TRUE)	
				CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iObjBitSet, iLoop)
				PRINTLN("[TS] [MSRAND] - MAINTAIN_PASS_OVER_CONTROL_FROM_MC_TO_FMMC_LAUNCHER - SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiCut[", iLoop, "], FALSE, TRUE)")
			ENDIF
		ENDIF	
	ENDFOR
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberPed - 1)
		IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iPedBitSet, iLoop)
			IF NOT IS_PED_INJURED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
				IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
					IF NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
						SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop], FALSE, TRUE)
						//SET_NETWORK_ID_CAN_MIGRATE(PED_TO_NET(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop]), FALSE)
						PRINTLN("[TS] [MSRAND] - MAINTAIN_PASS_OVER_CONTROL_FROM_MC_TO_FMMC_LAUNCHER - SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.piCut[", iLoop, "], FALSE, TRUE)")
						CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iPedBitSet, iLoop)
					ENDIF
				ELSE
					SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop], FALSE, TRUE)
					PRINTLN("[TS] [MSRAND] - MAINTAIN_PASS_OVER_CONTROL_FROM_MC_TO_FMMC_LAUNCHER - SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.piCut[", iLoop, "], FALSE, TRUE)")
					CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iPedBitSet, iLoop)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp - 1)
		IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iWorldPropBitSet, iLoop)
			IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop])
				SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop], FALSE, TRUE)
				PRINTLN("[TS] [MSRAND] - MAINTAIN_PASS_OVER_CONTROL_FROM_MC_TO_FMMC_LAUNCHER - SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[", iLoop, "], FALSE, TRUE)")
				CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iWorldPropBitSet, iLoop)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC HIDE_ALL_OTHER_PLAYERS_FOR_MC_MOCAP_CUT(BOOL bHideLocalPlayer = FALSE)
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(i))
			IF INT_TO_PLAYERINDEX(i) != PLAYER_ID()
			OR bHideLocalPlayer
				SET_PLAYER_INVISIBLE_LOCALLY(INT_TO_PLAYERINDEX(i))	
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
FUNC BOOL SHOULD_ENTITY_BE_PASSED_TO_CUT(STRING stPassed)		
	
	TEXT_LABEL_23 stTemp = GET_STRAND_MISSION_CUTSCENE_NAME()
		
	IF NOT IS_STRING_NULL_OR_EMPTY(stTemp)	
		IF ARE_STRINGS_EQUAL(stTemp, "MPH_HUM_KEY_MCS1")		
			//Maintain Karens car lights?
			IF ARE_STRINGS_EQUAL("Karen_Car",	stPassed)
			OR ARE_STRINGS_EQUAL("BALLER",		stPassed)
			OR ARE_STRINGS_EQUAL("EMPEROR",		stPassed)
			OR ARE_STRINGS_EQUAL("Regina_1",	stPassed)
			OR ARE_STRINGS_EQUAL("Zion_2", 		stPassed)
			OR ARE_STRINGS_EQUAL("Parked_Car",	stPassed)
			OR ARE_STRINGS_EQUAL("Zion_1", 		stPassed)
			OR ARE_STRINGS_EQUAL("Regina_2", 	stPassed)
			OR ARE_STRINGS_EQUAL("KAREN",		stPassed)
			OR ARE_STRINGS_EQUAL("Karen_Driver",stPassed)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

//Pass over Objects to the FMMC_Launcher cut scene
PROC PASS_OVER_CUT_SCENE_ALARM(OBJECT_INDEX oiAlarm, INT iSoundId, INT iAlarmSound)
	PRINTLN("[TS] [MSRAND] [ALARM] - PASS_OVER_CUT_SCENE_ALARM")
	IF g_sTransitionSessionData.sStrandMissionData.iAlarmNumber < ciTOTAL_NUMBER_OF_MC_CUT_ALARMS
		PRINTLN("[TS] [MSRAND] [ALARM] - PASS_OVER_CUT_SCENE_ALARM - g_sTransitionSessionData.sStrandMissionData.iAlarmNumber = ", g_sTransitionSessionData.sStrandMissionData.iAlarmNumber)
		PRINTLN("[TS] [MSRAND] [ALARM] - PASS_OVER_CUT_SCENE_ALARM - g_sTransitionSessionData.sStrandMissionData.iAlarmSound[", g_sTransitionSessionData.sStrandMissionData.iAlarmNumber, "] = ", iAlarmSound)
		PRINTLN("[TS] [MSRAND] [ALARM] - PASS_OVER_CUT_SCENE_ALARM - g_sTransitionSessionData.sStrandMissionData.iSoundId[", g_sTransitionSessionData.sStrandMissionData.iAlarmNumber, "] = ", iSoundId)
		g_sTransitionSessionData.sStrandMissionData.oiAlarm[g_sTransitionSessionData.sStrandMissionData.iAlarmNumber] = oiAlarm
		g_sTransitionSessionData.sStrandMissionData.iAlarmSound[g_sTransitionSessionData.sStrandMissionData.iAlarmNumber] = iAlarmSound
		g_sTransitionSessionData.sStrandMissionData.iSoundId[g_sTransitionSessionData.sStrandMissionData.iAlarmNumber] = iSoundId
		SET_BIT(g_sTransitionSessionData.sStrandMissionData.iAlarmBitSet, g_sTransitionSessionData.sStrandMissionData.iAlarmNumber)
		g_sTransitionSessionData.sStrandMissionData.iAlarmNumber++
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[TS] [MSRAND] [ALARM] - PASS_OVER_CUT_SCENE_ALARM iNumberObj >= ciTOTAL_NUMBER_OF_MC_CUT_ALARMS")		
		SCRIPT_ASSERT("[TS] [MSRAND] [ALARM] - PASS_OVER_CUT_SCENE_ALARM iNumberObj >= ciTOTAL_NUMBER_OF_MC_CUT_ALARMS")		
		#ENDIF
	ENDIF
ENDPROC

PROC PASS_OVER_CUTSCENE_WORLD_ALARM_TYPE(VECTOR vWorldAlarmPosition, INT iAlarmType)	
	
	IF g_sTransitionSessionData.sStrandMissionData.iWorldAlarmsNumber < ciTOTAL_NUMBER_OF_MC_CUT_WORLD_ALARMS
		PRINTLN("[TS] [MSRAND] [ALARM] - PASS_OVER_CUTSCENE_WORLD_ALARM_TYPE - iAlarmType: ", iAlarmType)		
		PRINTLN("[TS] [MSRAND] [ALARM] - PASS_OVER_CUTSCENE_WORLD_ALARM_TYPE - iWorldAlarmsNumber: ", g_sTransitionSessionData.sStrandMissionData.iWorldAlarmsNumber)		
		PRINTLN("[TS] [MSRAND] [ALARM] - PASS_OVER_CUTSCENE_WORLD_ALARM_TYPE - vWorldAlarmPosition: ", vWorldAlarmPosition)
		g_sTransitionSessionData.sStrandMissionData.vWorldAlarmPosition[g_sTransitionSessionData.sStrandMissionData.iWorldAlarmsNumber] = vWorldAlarmPosition
		g_sTransitionSessionData.sStrandMissionData.iWorldAlarmType[g_sTransitionSessionData.sStrandMissionData.iWorldAlarmsNumber] = iAlarmType
		g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[g_sTransitionSessionData.sStrandMissionData.iWorldAlarmsNumber] = -1		
		g_sTransitionSessionData.sStrandMissionData.iWorldAlarmsNumber++
	ELSE
		PRINTLN("[TS] [MSRAND] [ALARM] - PASS_OVER_CUTSCENE_WORLD_ALARM_TYPE - Already at max number of alarms!!  ciTOTAL_NUMBER_OF_MC_CUT_WORLD_ALARMS")
	ENDIF
	
ENDPROC

PROC PROCESS_PASSED_OVER_WORLD_ISLAND_ALARM_ONE_SHOT(INT iAlarm)
	VECTOR vPos = g_sTransitionSessionData.sStrandMissionData.vWorldAlarmPosition[iAlarm]	
	OBJECT_INDEX oiSpeaker = GET_CLOSEST_OBJECT_OF_TYPE(vPos, 125.0, INT_TO_ENUM(MODEL_NAMES, HASH("prop_out_door_speaker")), FALSE)
	
	IF NOT DOES_ENTITY_EXIST(oiSpeaker)
		oiSpeaker = GET_CLOSEST_OBJECT_OF_TYPE(vPos, 125.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Isl_Speaker_01a")), FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiSpeaker)		
		IF IS_ANY_POSITIONAL_SPEECH_PLAYING()
			IF (g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm] != -1 AND NOT HAS_SOUND_FINISHED(g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm]))
				IF NOT HAS_SOUND_FINISHED(g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm])
					PRINTLN("[TS] [MSRAND] [ALARM] - PROCESS_PASSED_OVER_WORLD_ISLAND_ALARM_ONE_SHOT - Stopping Alarm_Oneshot")
					STOP_SOUND(g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm])
				ENDIF
				g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm] = -1				
			ENDIF
		ELSE
			IF g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm] = -1
			OR HAS_SOUND_FINISHED(g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm])
				VECTOR vCoord = GET_ENTITY_COORDS(oiSpeaker)
					PRINTLN("[TS] [MSRAND] [ALARM] - PROCESS_PASSED_OVER_WORLD_ISLAND_ALARM_ONE_SHOT - Playing Alarm_Oneshot from vCoord: ", vCoord)
				
				IF g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm] = -1
					g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm] = GET_SOUND_ID()				
				ENDIF
				PLAY_SOUND_FROM_COORD(g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm], "Alarm_Oneshot" , vCoord, "DLC_H4_Island_Alarms_Sounds")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PASSED_OVER_WORLD_ISLAND_ALARM_GATE(INT iAlarm)
	VECTOR vPos = g_sTransitionSessionData.sStrandMissionData.vWorldAlarmPosition[iAlarm]
	IF g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm] = -1
	OR HAS_SOUND_FINISHED(g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm])
		IF g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm] = -1
			g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm] = GET_SOUND_ID()
		ENDIF
		PRINTLN("[TS] [MSRAND] [ALARM] - PROCESS_PASSED_OVER_WORLD_ISLAND_ALARM_GATE - Playing Alarm_Interior_Gate_Loop from vCoord: ", vPos)
		PLAY_SOUND_FROM_COORD(g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm], "Alarm_Interior_Gate_Loop", vPos, "DLC_H4_Island_Alarms_Sounds")
	ENDIF
ENDPROC

PROC MAINTAIN_PASSED_OVER_CUTSCENE_WORLD_ALARMS()
	INT iAlarm = 0
	FOR iAlarm = 0 TO g_sTransitionSessionData.sStrandMissionData.iWorldAlarmsNumber-1
		SWITCH g_sTransitionSessionData.sStrandMissionData.iWorldAlarmType[iAlarm]
			CASE ciWORLD_ALARM_TYPE_CUTSCENE_ISLAND_HEIST_ONE_SHOT
				PROCESS_PASSED_OVER_WORLD_ISLAND_ALARM_ONE_SHOT(iAlarm)
			BREAK
			CASE ciWORLD_ALARM_TYPE_CUTSCENE_ISLAND_HEIST_GATE
				PROCESS_PASSED_OVER_WORLD_ISLAND_ALARM_GATE(iAlarm)
			BREAK
		ENDSWITCH
	ENDFOR
ENDPROC

PROC CLEANUP_PASSED_OVER_CUTSCENE_WORLD_ALARMS(BOOL bInit = FALSE)
	INT iAlarm = 0
	FOR iAlarm = 0 TO ciTOTAL_NUMBER_OF_MC_CUT_WORLD_ALARMS-1		
		IF NOT bInit
			IF g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm] != -1
				IF NOT HAS_SOUND_FINISHED(g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm])
					PRINTLN("[TS] [MSRAND] [ALARM] - CLEANUP_PASSED_OVER_CUTSCENE_WORLD_ALARMS - Stopping soundID: ", g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm])
					STOP_SOUND(g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm])			
				ENDIF
				RELEASE_SOUND_ID(g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm])		
			ENDIF
		ENDIF
		g_sTransitionSessionData.sStrandMissionData.vWorldAlarmPosition[iAlarm] = <<0.0, 0.0, 0.0>>
		g_sTransitionSessionData.sStrandMissionData.iWorldAlarmType[iAlarm] = -1
		g_sTransitionSessionData.sStrandMissionData.iWorldAlarmSoundID[iAlarm] = -1
	ENDFOR
	g_sTransitionSessionData.sStrandMissionData.iWorldAlarmsNumber = 0
ENDPROC

//Pass over Objects to the FMMC_Launcher cut scene
PROC PASS_OVER_CUT_SCENE_WORLD_PROPS(OBJECT_INDEX oiCut, TEXT_LABEL_23 sWorldPropString, BOOL bDeleteOnly)
	PRINTLN("[TS] [MSRAND][WorldProp] - PASS_OVER_CUT_SCENE_WORLD_PROPS")
	
	#IF IS_DEBUG_BUILD
	IF oiCut = NULL
		PRINTLN("[TS] [MSRAND][WorldProp] - PASS_OVER_CUT_SCENE_WORLD_PROPS oiCut = NULL")	
	ENDIF
	#ENDIF
	
	IF g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp < ciTOTAL_NUMBER_OF_MC_CUT_WORLD_PROP
		PRINTLN("[TS] [MSRAND][WorldProp] - PASS_OVER_CUT_SCENE_WORLD_PROPS - g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp = ", g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp, " - sWorldPropString = ", sWorldPropString)
		g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp] = oiCut
		g_sTransitionSessionData.sStrandMissionData.sWorldPropString[g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp] = sWorldPropString
		IF NOT SHOULD_ENTITY_BE_PASSED_TO_CUT(sWorldPropString)
		OR bDeleteOnly
			SET_BIT(g_sTransitionSessionData.sStrandMissionData.iWorldPropDeleteBitSet, g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp)
			PRINTLN("[TS] [MSRAND][WorldProp] - PASS_OVER_CUT_SCENE_WORLD_PROPS - SHOULD_ENTITY_BE_PASSED_TO_CUT = FALSE - iNumberWorldProp = ", g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp, " - sWorldPropString = ", sWorldPropString)		
			g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp++
			EXIT
		ENDIF
		SET_BIT(g_sTransitionSessionData.sStrandMissionData.iWorldPropBitSet, g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp)
		g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp++
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[TS] [MSRAND][WorldProp] - PASS_OVER_CUT_SCENE_WORLD_PROPS iNumberWorldProp >= ciTOTAL_NUMBER_OF_MC_CUT_WORLD_PROP")		
		SCRIPT_ASSERT("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_WORLD_PROPS iNumberWorldProp >= ciTOTAL_NUMBER_OF_MC_CUT_WORLD_PROP")		
		#ENDIF
	ENDIF
ENDPROC

//Pass over vehicles to the FMMC_Launcher cut scene
PROC PASS_OVER_CUT_SCENE_VEHICLE(VEHICLE_INDEX viCut, TEXT_LABEL_23 sVehString)
	PRINTLN("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_VEHICLE")
	IF g_sTransitionSessionData.sStrandMissionData.iNumberVeh < ciTOTAL_NUMBER_OF_MC_CUT_VEHCILES
		PRINTLN("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_VEHICLE - g_sTransitionSessionData.sStrandMissionData.iNumberVeh = ", g_sTransitionSessionData.sStrandMissionData.iNumberVeh, " - sVehString = ", sVehString)
		g_sTransitionSessionData.sStrandMissionData.sVehicleString[g_sTransitionSessionData.sStrandMissionData.iNumberVeh] = sVehString
		g_sTransitionSessionData.sStrandMissionData.viCut[g_sTransitionSessionData.sStrandMissionData.iNumberVeh] = viCut
		IF NOT SHOULD_ENTITY_BE_PASSED_TO_CUT(sVehString)
			//SET_BIT(g_sTransitionSessionData.sStrandMissionData.iVehBitSet, g_sTransitionSessionData.sStrandMissionData.iNumberVeh)
			SET_BIT(g_sTransitionSessionData.sStrandMissionData.iVehDeleteBitSet, g_sTransitionSessionData.sStrandMissionData.iNumberVeh)
			PRINTLN("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_VEHICLE - SHOULD_ENTITY_BE_PASSED_TO_CUT = FALSE - iNumberVeh = ",	g_sTransitionSessionData.sStrandMissionData.iNumberVeh, " - sVehString = ", sVehString)		
			g_sTransitionSessionData.sStrandMissionData.iNumberVeh++
			EXIT
		ENDIF
		SET_BIT(g_sTransitionSessionData.sStrandMissionData.iVehBitSet, g_sTransitionSessionData.sStrandMissionData.iNumberVeh)
		g_sTransitionSessionData.sStrandMissionData.iNumberVeh++
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_VEHICLE iNumberVeh >= ciTOTAL_NUMBER_OF_MC_CUT_VEHCILES")		
		SCRIPT_ASSERT("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_VEHICLE iNumberVeh >= ciTOTAL_NUMBER_OF_MC_CUT_VEHCILES")		
		#ENDIF
	ENDIF
ENDPROC
//Pass over Objects to the FMMC_Launcher cut scene
PROC PASS_OVER_CUT_SCENE_OBJECT(OBJECT_INDEX oiCut, TEXT_LABEL_23 sObjString)
	PRINTLN("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_OBJECT")
	
	#IF IS_DEBUG_BUILD
	IF oiCut = NULL
		PRINTLN("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_OBJECT oiCut = NULL")	
	ENDIF
	#ENDIF
	
	IF g_sTransitionSessionData.sStrandMissionData.iNumberObj < ciTOTAL_NUMBER_OF_MC_CUT_OBJ
		PRINTLN("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_OBJECT - g_sTransitionSessionData.sStrandMissionData.iNumberObj = ", g_sTransitionSessionData.sStrandMissionData.iNumberObj, " - sVehString = ", sObjString)
		g_sTransitionSessionData.sStrandMissionData.oiCut[g_sTransitionSessionData.sStrandMissionData.iNumberObj] = oiCut
		g_sTransitionSessionData.sStrandMissionData.sObjectString[g_sTransitionSessionData.sStrandMissionData.iNumberObj] = sObjString
		IF NOT SHOULD_ENTITY_BE_PASSED_TO_CUT(sObjString)
			//SET_BIT(g_sTransitionSessionData.sStrandMissionData.iObjBitSet, g_sTransitionSessionData.sStrandMissionData.iNumberObj)
			SET_BIT(g_sTransitionSessionData.sStrandMissionData.iObjDeleteBitSet, g_sTransitionSessionData.sStrandMissionData.iNumberObj)
			PRINTLN("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_OBJECT - SHOULD_ENTITY_BE_PASSED_TO_CUT = FALSE - iNumberObj = ", g_sTransitionSessionData.sStrandMissionData.iNumberObj, " - sObjString = ", sObjString)		
			g_sTransitionSessionData.sStrandMissionData.iNumberObj++
			EXIT
		ENDIF		
		SET_BIT(g_sTransitionSessionData.sStrandMissionData.iObjBitSet, g_sTransitionSessionData.sStrandMissionData.iNumberObj)
		g_sTransitionSessionData.sStrandMissionData.iNumberObj++
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_OBJECT iNumberObj >= ciTOTAL_NUMBER_OF_MC_CUT_OBJ")		
		SCRIPT_ASSERT("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_OBJECT iNumberObj >= ciTOTAL_NUMBER_OF_MC_CUT_OBJ")		
		#ENDIF
	ENDIF
ENDPROC
//Pass over Peds to the FMMC_Launcher cut scene
PROC PASS_OVER_CUT_SCENE_PED(PED_INDEX piCut, TEXT_LABEL_23 sPedString, INT iMask = 0, PLAYER_INDEX piPlayer = NULL)
	PRINTLN("[TS] [MSRAND] [PED CUT] - PASS_OVER_CUT_SCENE_PED")
	IF g_sTransitionSessionData.sStrandMissionData.iNumberPed < ciTOTAL_NUMBER_OF_MC_CUT_PEDS
		#IF IS_DEBUG_BUILD
		IF piCut = NULL
		OR IS_PED_INJURED(piCut)
			PRINTLN("[TS] [MSRAND] [PED CUT] * - PASS_OVER_CUT_SCENE_PED - IS_PED_INJURED(piCut) = current number = ", g_sTransitionSessionData.sStrandMissionData.iNumberPed)
		ENDIF
		#ENDIF
		g_sTransitionSessionData.sStrandMissionData.piCut[g_sTransitionSessionData.sStrandMissionData.iNumberPed] = piCut
		g_sTransitionSessionData.sStrandMissionData.piPlayer[g_sTransitionSessionData.sStrandMissionData.iNumberPed] = piPlayer
		g_sTransitionSessionData.sStrandMissionData.sPlayerString[g_sTransitionSessionData.sStrandMissionData.iNumberPed] = sPedString
		g_sTransitionSessionData.sStrandMissionData.iPlayerString[g_sTransitionSessionData.sStrandMissionData.iNumberPed] = GET_HASH_KEY(sPedString)
		//Me?
		IF piCut = PLAYER_PED_ID()
			PRINTLN("[TS] [MSRAND] [PED CUT] * - PASS_OVER_CUT_SCENE_PED - Passing over me")
			g_sTransitionSessionData.sStrandMissionData.iPlayerPed = g_sTransitionSessionData.sStrandMissionData.iNumberPed
		//Not me
		ELSE
			IF NOT SHOULD_ENTITY_BE_PASSED_TO_CUT(sPedString)
				//SET_BIT(g_sTransitionSessionData.sStrandMissionData.iPedBitSet, g_sTransitionSessionData.sStrandMissionData.iNumberPed)
				SET_BIT(g_sTransitionSessionData.sStrandMissionData.iPedDeleteBitSet, g_sTransitionSessionData.sStrandMissionData.iNumberPed)
				PRINTLN("[TS] [MSRAND] [PED CUT] - PASS_OVER_CUT_SCENE_PED - SHOULD_ENTITY_BE_PASSED_TO_CUT = FALSE - iNumberPed = ", g_sTransitionSessionData.sStrandMissionData.iNumberPed, " - sPedString = ", sPedString)		
				g_sTransitionSessionData.sStrandMissionData.iNumberPed++	
				EXIT
			ENDIF		
			SET_BIT(g_sTransitionSessionData.sStrandMissionData.iPedBitSet, g_sTransitionSessionData.sStrandMissionData.iNumberPed)
		ENDIF
		g_sTransitionSessionData.sStrandMissionData.iMyMask[g_sTransitionSessionData.sStrandMissionData.iNumberPed] = iMask
		PRINTLN("[TS] [MSRAND] - SET_PED_STRAND_MISSION_MASK - g_sTransitionSessionData.sStrandMissionData.iMyMask[", g_sTransitionSessionData.sStrandMissionData.iNumberPed, "] = ", iMask)		
		PRINTLN("[PED CUT] * PASS_OVER_CUT_SCENE_PED - ped [", g_sTransitionSessionData.sStrandMissionData.iNumberPed, "] - NATIVE_TO_INT - ", NATIVE_TO_INT(g_sTransitionSessionData.sStrandMissionData.piCut[g_sTransitionSessionData.sStrandMissionData.iNumberPed]))
		g_sTransitionSessionData.sStrandMissionData.iNumberPed++
		//piCut = NULL
		PRINTLN("[TS] [MSRAND] [PED CUT] * - PASS_OVER_CUT_SCENE_PED - g_sTransitionSessionData.sStrandMissionData.iNumberPed = ", g_sTransitionSessionData.sStrandMissionData.iNumberPed, " - sPedString = ", sPedString)
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_PED iNumberPed >= ciTOTAL_NUMBER_OF_MC_CUT_PEDS")		
		SCRIPT_ASSERT("[TS] [MSRAND] - PASS_OVER_CUT_SCENE_PED iNumberPed >= ciTOTAL_NUMBER_OF_MC_CUT_PEDS")		
		#ENDIF
	ENDIF
ENDPROC

//get the number of deaths that a team has had on a strand mission
FUNC INT GET_STRAND_MISSION_TEAM_DEATHS(INT iTeam)
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
	OR (HAS_FIRST_STRAND_MISSION_BEEN_PASSED() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET())
		RETURN g_sTransitionSessionData.sStrandMissionData.iTeamDeaths[iTeam]
	ENDIF
	RETURN 0
ENDFUNC

//Store out the team deaths on a strans mission
PROC SET_STRAND_MISSION_TEAM_DEATHS(INT iTeam0 = 0 , INT iTeam1 = 0, INT iTeam2 = 0, INT iTeam3 = 0)
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_TEAM_DEATHS")		
		PRINTLN("[TS] [MSRAND] - g_sTransitionSessionData.sStrandMissionData.iTeamDeaths[0] = ", iTeam0)		
		PRINTLN("[TS] [MSRAND] - g_sTransitionSessionData.sStrandMissionData.iTeamDeaths[1] = ", iTeam1)		
		PRINTLN("[TS] [MSRAND] - g_sTransitionSessionData.sStrandMissionData.iTeamDeaths[2] = ", iTeam2)		
		PRINTLN("[TS] [MSRAND] - g_sTransitionSessionData.sStrandMissionData.iTeamDeaths[3] = ", iTeam3)		
		g_sTransitionSessionData.sStrandMissionData.iTeamDeaths[0] = iTeam0
		g_sTransitionSessionData.sStrandMissionData.iTeamDeaths[1] = iTeam1
		g_sTransitionSessionData.sStrandMissionData.iTeamDeaths[2] = iTeam2
		g_sTransitionSessionData.sStrandMissionData.iTeamDeaths[3] = iTeam3
	ENDIF
ENDPROC

//Store out the remaining thermal charges on a strand mission
PROC SET_STRAND_MISSION_TEAM_THERMAL_CHARGES_REMAINING(INT iTeam0 = 0 , INT iTeam1 = 0, INT iTeam2 = 0, INT iTeam3 = 0)
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_TEAM_THERMAL_CHARGES_REMAINING")		
		PRINTLN("[TS] [MSRAND] - g_sTransitionSessionData.sStrandMissionData.iTeamThermalCharges[0] = ", iTeam0)		
		PRINTLN("[TS] [MSRAND] - g_sTransitionSessionData.sStrandMissionData.iTeamThermalCharges[1] = ", iTeam1)		
		PRINTLN("[TS] [MSRAND] - g_sTransitionSessionData.sStrandMissionData.iTeamThermalCharges[2] = ", iTeam2)		
		PRINTLN("[TS] [MSRAND] - g_sTransitionSessionData.sStrandMissionData.iTeamThermalCharges[3] = ", iTeam3)		
		g_sTransitionSessionData.sStrandMissionData.iTeamThermalCharges[0] = iTeam0
		g_sTransitionSessionData.sStrandMissionData.iTeamThermalCharges[1] = iTeam1
		g_sTransitionSessionData.sStrandMissionData.iTeamThermalCharges[2] = iTeam2
		g_sTransitionSessionData.sStrandMissionData.iTeamThermalCharges[3] = iTeam3
	ENDIF
ENDPROC

//Store out the remaining player thermal charges on a strand mission
PROC SET_STRAND_MISSION_PLAYER_THERMAL_CHARGES_REMAINING(INT iChargesRemaining)
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		PRINTLN("[TS] [MSRAND] - SET_STRAND_MISSION_PLAYER_THERMAL_CHARGES_REMAINING")		
		PRINTLN("[TS] [MSRAND] - g_sTransitionSessionData.sStrandMissionData.iPlayerThermalCharges = ", iChargesRemaining)		
		g_sTransitionSessionData.sStrandMissionData.iPlayerThermalCharges = iChargesRemaining
	ENDIF
ENDPROC

//Cutscene specific registration
PROC REGISTER_CUTSCENE_SPECIFIC_ENTITIES(STRING tlMocapName)
	IF ARE_STRINGS_EQUAL(tlMocapName, "HS4F_MAIN_EXT")
		IF IS_BIT_SET(g_FMMC_STRUCT.sIPLOptions.iIslandIPL, ciFMMC_ISLAND_IPL_DESTROYED_DOORS)
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "External_Gate_R", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "External_Gate_L", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		ENDIF
	ENDIF
ENDPROC

//Register all the entites with the cutscene
PROC REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE(STRING tlMocapName, BOOL bDoClearArea = TRUE, BOOL bPreventCutsceneFromEnablingCollision = FALSE)
	g_sTransitionSessionData.sStrandMissionData.iVehBitSet = 0
	g_sTransitionSessionData.sStrandMissionData.iObjBitSet = 0
	g_sTransitionSessionData.sStrandMissionData.iPedBitSet = 0	
	
	VECTOR vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
	IF bDoClearArea
		PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_WITH_MC_CUTSCENE CLEAR_AREA_LEAVE_VEHICLE_HEALTH(", vPlayer, ", 100.0, FALSE, TRUE, FALSE, FALSE)")
		CLEAR_AREA_LEAVE_VEHICLE_HEALTH(vPlayer, 100.0, FALSE, TRUE, FALSE, FALSE)
	ENDIF
	
	PRINTLN("[TS] [MSRAND] - REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE")
	
	INT iLoop
	//Register all the vehicles that we stored in globals				  
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberVeh - 1)
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
		AND IS_VEHICLE_DRIVEABLE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
			
			// Might need to bring this fix to the other entity types. We'll see.
			IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
					PRINTLN("[MSRAND] - REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - vehicle [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop], " Is having it's collision with other entities disabled.")
					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop], FALSE, FALSE)	
				ELSE
					PRINTLN("[MSRAND] - REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - vehicle [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop], " Not modifying Collision we do not have Network Entity Control")
				ENDIF
			ELSE
				PRINTLN("[MSRAND] - REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - vehicle [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop], " Is having it's collision with other entities disabled Locally.")
				SET_ENTITY_COMPLETELY_DISABLE_COLLISION(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop], FALSE, FALSE)	
			ENDIF
			
			IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iVehDeleteBitSet, iLoop)
				PRINTLN("[MSRAND] - [PED CUT] * REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iVehDeleteBitSet, ", iLoop, ")")
				IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
					SET_ENTITY_VISIBLE_IN_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop], FALSE)
				ENDIF
			ELSE
				PRINTLN("[MSRAND] - REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - adding vehicle [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop])
				IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop])
				AND NOT ARE_STRINGS_EQUAL(g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop], "VISIBLE")
					REGISTER_ENTITY_FOR_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop], g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop], CU_ANIMATE_EXISTING_SCRIPT_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
				ENDIF
				IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
					SET_ENTITY_VISIBLE_IN_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop], TRUE)
				ENDIF
			ENDIF
			
			IF bPreventCutsceneFromEnablingCollision
				PRINTLN("[MSRAND] - REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - vehicle [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop], " Preventing the Cutscene from forcing collision back on.")
				NETWORK_KEEP_ENTITY_COLLISION_DISABLED_AFTER_ANIM_SCENE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop], TRUE)
			ENDIF
		ELSE
			//Dont animate NULL vehicles.
			//Don't register empty strings
			IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop])	
			AND NOT IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iVehDeleteBitSet, iLoop)
				PRINTLN("[MSRAND] - REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - NOT ANIMATING vehicle [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop])
				REGISTER_ENTITY_FOR_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop], g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop], CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
			ENDIF
		ENDIF
	ENDFOR
	//register all the objects that we stored in globals
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberObj - 1)
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
			IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iObjDeleteBitSet, iLoop)
				PRINTLN("[MSRAND] - [PED CUT] * REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iObjDeleteBitSet, ", iLoop, ")")
				IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
					SET_ENTITY_VISIBLE_IN_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop], FALSE)
				ENDIF
			ELSE
				PRINTLN("[MSRAND] - REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - adding object [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sObjectString[iLoop])
				IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.sObjectString[iLoop])
				AND NOT ARE_STRINGS_EQUAL(g_sTransitionSessionData.sStrandMissionData.sObjectString[iLoop], "VISIBLE")
					REGISTER_ENTITY_FOR_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop], g_sTransitionSessionData.sStrandMissionData.sObjectString[iLoop], CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
				ENDIF
				IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
					SET_ENTITY_VISIBLE_IN_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop], TRUE)
				ENDIF
			ENDIF
		ELSE
			//Dont animate NULL objects.
				//Don't register empty strings
			IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.sObjectString[iLoop])
			AND NOT IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iObjDeleteBitSet, iLoop)
				PRINTLN("[MSRAND] - REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - NOT ANIMATING object [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sObjectString[iLoop])
				REGISTER_ENTITY_FOR_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop], g_sTransitionSessionData.sStrandMissionData.sObjectString[iLoop], CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
			ENDIF
		ENDIF
	ENDFOR
	//Register all the peds that we stored in globals
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberPed - 1)
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
			PRINTLN("[MSRAND] - [PED CUT] * REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - ped [", iLoop, "] - NATIVE_TO_INT - ", NATIVE_TO_INT(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop]))
			//If it's me
			IF iLoop = g_sTransitionSessionData.sStrandMissionData.iPlayerPed //PLAYER_PED_ID()
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD
				IF g_sTransitionSessionData.sStrandMissionData.piCut[iLoop] != PLAYER_PED_ID()
					PRINTLN("[MSRAND] - [PED CUT] * REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - g_sTransitionSessionData.sStrandMissionData.piCut[", iLoop, "] != PLAYER_PED_ID()")
					SCRIPT_ASSERT("[PED CUT] * REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - g_sTransitionSessionData.sStrandMissionData.piCut[iLoop] != PLAYER_PED_ID()")
				ENDIF
				#ENDIF
				PRINTLN("[MSRAND] - [PED CUT] * REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - adding ped [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop], " (who is the player)")
				IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop])
				AND NOT ARE_STRINGS_EQUAL(g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop], "VISIBLE")
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop], CU_ANIMATE_EXISTING_SCRIPT_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
				ENDIF
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				SET_ENTITY_VISIBLE_IN_CUTSCENE(PLAYER_PED_ID(), TRUE)
			//A clone or a PED
			ELIF NOT IS_PED_INJURED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
				IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iPedDeleteBitSet, iLoop)
					PRINTLN("[MSRAND] - [PED CUT] * REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iPedDeleteBitSet, ", iLoop, ")")
					IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
						SET_ENTITY_VISIBLE_IN_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop], FALSE)
					ENDIF
				ELSE
					PRINTLN("[MSRAND] - [PED CUT] * REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - adding ped [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop])
					IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop])
					AND NOT ARE_STRINGS_EQUAL(g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop], "VISIBLE")
						REGISTER_ENTITY_FOR_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop], g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop], CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)						
					ENDIF
					IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
						SET_ENTITY_VISIBLE_IN_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop], TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//a NULL PED means remove the PED	
			IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop])
			AND NOT IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iPedDeleteBitSet, iLoop)
				PRINTLN("[MSRAND] - [PED CUT] * REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - NOT ANIMATING ped [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop], " - NULL")
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop], CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
			ENDIF
		ENDIF
	ENDFOR
	//Register all the world props that we stored in globals
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp - 1)
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop])								
			IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iWorldPropDeleteBitSet, iLoop)
				PRINTLN("[MSRAND][WorldProp] - [PED CUT] * REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iWorldPropDeleteBitSet, ", iLoop, ")")
				SET_ENTITY_VISIBLE(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop], FALSE)
			ELSE
				PRINTLN("[MSRAND][WorldProp] - REGISTER_ALL_ENTITIES_WITH_MC_CUTSCENE - adding World Prop [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sWorldPropString[iLoop])
				IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.sWorldPropString[iLoop])
				AND NOT ARE_STRINGS_EQUAL(g_sTransitionSessionData.sStrandMissionData.sWorldPropString[iLoop], "VISIBLE")
					REGISTER_ENTITY_FOR_CUTSCENE(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop], g_sTransitionSessionData.sStrandMissionData.sWorldPropString[iLoop], CU_ANIMATE_EXISTING_SCRIPT_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
				ENDIF
				SET_ENTITY_VISIBLE(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop], TRUE)
			ENDIF
		ENDIF
	ENDFOR
	REGISTER_CUTSCENE_SPECIFIC_ENTITIES(tlMocapName)
ENDPROC	

//Register all the entites with the cutscene
PROC MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE(BOOL bAllOffMission = FALSE)	
	PRINTLN("[MSRAND] - MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE - bAllOffMission = ", bAllOffMission)
	INT iLoop
	
	
	//Register all the peds that we stored in globals
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberPed - 1)
		IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iPedDeleteBitSet, iLoop)
		AND DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
		AND NOT IS_PED_INJURED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
			IF bAllOffMission
				IF NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
					IF NOT IS_ENTITY_A_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
						SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop], FALSE, TRUE)
						DELETE_PED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
						PRINTLN("[MSRAND] - MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE - DELETE_PED [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sPlayerString[iLoop])
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[MSRAND] - MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE - DELETE_PED [", iLoop, "] - FALSE")
						#ENDIF
					ENDIF
			//	ELSE
					//NETWORK_REQUEST_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
				ENDIF
			ELSE
				SET_ENTITY_LOCALLY_INVISIBLE(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
			ENDIF
		ENDIF
	ENDFOR	
	//register all the objects that we stored in globals
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberObj - 1)
		IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iObjDeleteBitSet, iLoop)
		AND DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
			IF bAllOffMission
				IF NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
					IF NOT IS_ENTITY_A_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
						SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop], FALSE, TRUE)
						DELETE_OBJECT(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
						PRINTLN("[MSRAND] - MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE - DELETE_OBJECT [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sObjectString[iLoop])
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[MSRAND] - MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE - DELETE_OBJECT [", iLoop, "] - FALSE")
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				SET_ENTITY_LOCALLY_INVISIBLE(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
			ENDIF
		ENDIF			
	ENDFOR
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberVeh - 1)
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
		AND IS_VEHICLE_DRIVEABLE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
			IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iVehDeleteBitSet, iLoop)
				IF bAllOffMission
					IF NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
						IF NOT IS_ENTITY_A_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
							SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop], FALSE, TRUE)
							DELETE_VEHICLE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
							PRINTLN("[MSRAND] - MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE - DELETE_VEHICLE [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop])
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[MSRAND] - MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE - DELETE_VEHICLE [", iLoop, "] - FALSE")
							#ENDIF
						ENDIF
						//NETWORK_REQUEST_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
					ENDIF
				ELSE
					SET_ENTITY_LOCALLY_INVISIBLE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
					PRINTLN("[MSRAND] - MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE - SET_ENTITY_LOCALLY_INVISIBLE - [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sVehicleString[iLoop])
				ENDIF
			ENDIF			
		ENDIF
	ENDFOR
	
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp - 1)
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop])
			IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iWorldPropDeleteBitSet, iLoop)
				IF bAllOffMission				
					IF NOT IS_ENTITY_A_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop])
						SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop], FALSE, TRUE)
						SET_OBJECT_AS_NO_LONGER_NEEDED(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop])
						PRINTLN("[MSRAND][WorldProp] - MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE - SET_ENTITY_AS_NO_LONGER_NEEDED [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sWorldPropString[iLoop])
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[MSRAND][WorldProp] - MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE - SET_ENTITY_AS_NO_LONGER_NEEDED [", iLoop, "] - FALSE")
						#ENDIF
					ENDIF
				ELSE
					SET_ENTITY_VISIBLE(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop], FALSE)
					PRINTLN("[MSRAND][WorldProp] - MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE - SET_ENTITY_VISIBLE false - [", iLoop, "] - name - ", g_sTransitionSessionData.sStrandMissionData.sWorldPropString[iLoop])
				ENDIF
			ENDIF			
		ENDIF
	ENDFOR
ENDPROC	

//Set all peds visible in the cutscene
PROC SET_ALL_STRAND_MISSION_CUT_PEDS_VISABLE()
	PRINTLN("[TS] [MSRAND] - SET_ALL_STRAND_MISSION_CUT_PEDS_VISABLE")
	INT iLoop
//	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberVeh - 1)
//		IF NOT IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iVehDeleteBitSet, iLoop)
//		AND DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
//		AND IS_VEHICLE_DRIVEABLE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
//		AND NOT NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
//			SET_ENTITY_VISIBLE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop], TRUE)
//		ENDIF
//	ENDFOR
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberPed - 1)
		IF NOT IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iPedDeleteBitSet, iLoop)
		AND DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])	
		AND NOT NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
			PRINTLN("[MSRAND] [PED CUT] * - SET_ALL_STRAND_MISSION_CUT_PEDS_VISABLE - SET_ENTITY_VISIBLE(g_sTransitionSessionData.sStrandMissionData.piCut[", iLoop, "], TRUE)")
			SET_ENTITY_VISIBLE(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop], TRUE)
		ENDIF
	ENDFOR
ENDPROC

//Shows stuff that's meant to be showen when the cut scene is playing
PROC MAINTAIN_ENTITIES_VISIBILITY_FOR_MC_CUTSCENE()
	PRINTLN("[TS] [MSRAND] - MAINTAIN_ENTITIES_VISIBILITY_FOR_MC_CUTSCENE")
	INT iLoop
	//Register all the vehicles that we stored in globals
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberVeh - 1)
		IF NOT IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iVehDeleteBitSet, iLoop)
		AND DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
		AND IS_VEHICLE_DRIVEABLE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
		AND NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
			SET_ENTITY_LOCALLY_VISIBLE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
		ENDIF
	ENDFOR
	//register all the objects that we stored in globals
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberObj - 1)
		IF NOT IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iObjDeleteBitSet, iLoop)
		AND DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])	
		AND NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
			SET_ENTITY_LOCALLY_VISIBLE(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
		ENDIF
	ENDFOR
ENDPROC	

//Rturns TRUS if a sound ID should persist on the next strand
FUNC BOOL SHOULD_THIS_SOUND_PERSIST_FOR_STRAND_MISSION(INT iSoundid)
	IF NOT IS_THIS_IS_A_STRAND_MISSION()
		RETURN FALSE
	ENDIF
	INT iLoop
	REPEAT g_sTransitionSessionData.sStrandMissionData.iAlarmNumber iLoop
		IF g_sTransitionSessionData.sStrandMissionData.iSoundId[iLoop] != -1
		AND g_sTransitionSessionData.sStrandMissionData.iSoundId[iLoop] = iSoundid
			PRINTLN("[TS] [MSRAND] [ALARM] - SHOULD_THIS_SOUND_PERSIST_FOR_STRAND_MISSION - g_sTransitionSessionData.sStrandMissionData.iSoundId[", iLoop, "] = iSoundid = ", iSoundid)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

//wipes the alarm sound data
PROC CLEANUP_STRAND_ALARM_DATA()
	PRINTLN("[TS] [MSRAND] [ALARM] - CLEANUP_STRAND_ALARM_DATA")
	g_sTransitionSessionData.sStrandMissionData.iAlarmNumber = 0
	g_sTransitionSessionData.sStrandMissionData.iAlarmBitSet = 0	
	INT iLoop
	REPEAT ciTOTAL_NUMBER_OF_MC_CUT_ALARMS iLoop
		g_sTransitionSessionData.sStrandMissionData.iAlarmSound[iLoop] = -1
		g_sTransitionSessionData.sStrandMissionData.iSoundId[iLoop] = -1
	ENDREPEAT
ENDPROC

//cleans up the alarm sounds
PROC CLEANUP_STRAND_MISSION_ALARMS(BOOL bCleanSound = TRUE)
	PRINTLN("[TS] [MSRAND] [ALARM] - CLEANUP_STRAND_MISSION_ALARMS")
	INT iLoop
	REPEAT g_sTransitionSessionData.sStrandMissionData.iAlarmNumber iLoop
		IF bCleanSound
			//Release the sound if it's valid
			IF g_sTransitionSessionData.sStrandMissionData.iSoundId[iLoop] != -1
				PRINTLN("[TS] [MSRAND] [ALARM] - g_sTransitionSessionData.sStrandMissionData.iSoundId[", iLoop, "] = ", g_sTransitionSessionData.sStrandMissionData.iSoundId[iLoop], " - STOP_SOUND(g_sTransitionSessionData.sStrandMissionData.iSoundId[", iLoop, "])")
				STOP_SOUND(g_sTransitionSessionData.sStrandMissionData.iSoundId[iLoop])
				RELEASE_SOUND_ID(g_sTransitionSessionData.sStrandMissionData.iSoundId[iLoop])
			ENDIF
		ENDIF
		//delete the entity if it's there
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.oiAlarm[iLoop])
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(g_sTransitionSessionData.sStrandMissionData.oiAlarm[iLoop]))
			DELETE_OBJECT(g_sTransitionSessionData.sStrandMissionData.oiAlarm[iLoop])
		ENDIF
	ENDREPEAT
	//Call the function to clean up the alarm data
	CLEANUP_STRAND_ALARM_DATA()
ENDPROC

//Resets all the INTs used in the strand mission pass over
PROC RESET_STRAND_MISSION_CUT_SCENE_INTS()
	PRINTLN("[TS] [MSRAND] - RESET_STRAND_MISSION_CUT_SCENE_INTS")
	//Reset the vars as we don't need them any more
	g_sTransitionSessionData.sStrandMissionData.iNumberPed = 0
	g_sTransitionSessionData.sStrandMissionData.iNumberObj = 0
	g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp = 0
	g_sTransitionSessionData.sStrandMissionData.iPlayerPed = -1
	g_sTransitionSessionData.sStrandMissionData.iNumberVeh = 0
	g_sTransitionSessionData.sStrandMissionData.iWorldPropBitSet = 0
	g_sTransitionSessionData.sStrandMissionData.iVehBitSet = 0
	g_sTransitionSessionData.sStrandMissionData.iObjBitSet = 0
	g_sTransitionSessionData.sStrandMissionData.iPedBitSet = 0
	g_sTransitionSessionData.sStrandMissionData.iWorldPropDeleteBitSet = 0
	g_sTransitionSessionData.sStrandMissionData.iVehDeleteBitSet = 0
	g_sTransitionSessionData.sStrandMissionData.iObjDeleteBitSet = 0
	g_sTransitionSessionData.sStrandMissionData.iPedDeleteBitSet = 0
	g_sTransitionSessionData.sStrandMissionData.iAlarmNumber = 0
	g_sTransitionSessionData.sStrandMissionData.iAlarmBitSet = 0	
	CLEANUP_STRAND_ALARM_DATA()
ENDPROC

PROC REMOVE_ALL_PLAYER_CLONES_AFTER_MC_CUTSCENE()
	
	PRINTLN("[TS] [MSRAND] - REMOVE_ALL_PLAYER_CLONES_AFTER_MC_CUTSCENE - Removing all player clones after cutscene.")
	
	INT iLoop
	//Remove all the peds that we stored in globals
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberPed - 1)
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
		//And it's not me
		AND g_sTransitionSessionData.sStrandMissionData.piCut[iLoop] != PLAYER_PED_ID()
			//If it's networked then make sure I've got control
			IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
					PRINTLN("[TS] [MSRAND] - REMOVE_ALL_PLAYER_CLONES_AFTER_MC_CUTSCENE - DELETE_PED ped - ", iLoop)
					DELETE_PED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
				ENDIF
			//Not networked then delete
			ELSE
				PRINTLN("[TS] [MSRAND] - REMOVE_ALL_PLAYER_CLONES_AFTER_MC_CUTSCENE - DELETE_PED ped - ", iLoop)
				DELETE_PED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//Register all the entites with the cutscene
PROC REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE(BOOL bClearArea = FALSE)
	PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE")
	
	PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE - iNumberVeh: ", g_sTransitionSessionData.sStrandMissionData.iNumberVeh)	
	
	INT iLoop
	//Remove all the vehicles that we stored in globals
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberVeh - 1)
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
			//If it's networked then make sure I've got control
			IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
					PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE - DELETE_VEHICLE vehicle - ", iLoop)
					DELETE_VEHICLE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
				ELSE
					PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE vehicle: ", iLoop, " Do not have control.")
				ENDIF
			//Not networked then delete
			ELSE
				PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE - DELETE_VEHICLE vehicle - ", iLoop)
				DELETE_VEHICLE(g_sTransitionSessionData.sStrandMissionData.viCut[iLoop])
			ENDIF
		ELSE
			PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE vehicle: ", iLoop, " Does not exist")
		ENDIF
	ENDFOR
	//Remove all the objects that we stored in globals
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberObj - 1)
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
			//If it's networked then make sure I've got control
			IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
					PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE - DELETE_OBJECT Object - ", iLoop)
					DELETE_OBJECT(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
				ENDIF
			//Not networked then delete
			ELSE
				PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE - DELETE_OBJECT Object - ", iLoop)
				DELETE_OBJECT(g_sTransitionSessionData.sStrandMissionData.oiCut[iLoop])
			ENDIF
		ENDIF
	ENDFOR
	//Remove all the peds that we stored in globals
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberPed - 1)
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
		//And it's not me
		AND g_sTransitionSessionData.sStrandMissionData.piCut[iLoop] != PLAYER_PED_ID()
			//If it's networked then make sure I've got control
			IF NETWORK_GET_ENTITY_IS_NETWORKED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
					PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE - DELETE_PED ped - ", iLoop)
					DELETE_PED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
				ENDIF
			//Not networked then delete
			ELSE
				PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE - DELETE_PED ped - ", iLoop)
				DELETE_PED(g_sTransitionSessionData.sStrandMissionData.piCut[iLoop])
			ENDIF
		ENDIF
	ENDFOR
	FOR iLoop = 0 TO (g_sTransitionSessionData.sStrandMissionData.iNumberWorldProp - 1)
		IF DOES_ENTITY_EXIST(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop])
			PRINTLN("[TS] [MSRAND][WorldProps] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE - Cleaning up World Prop: ", iLoop)
			SET_OBJECT_AS_NO_LONGER_NEEDED(g_sTransitionSessionData.sStrandMissionData.oiWorldPropCut[iLoop])
		ENDIF
	ENDFOR
	
	IF bClearArea
		VECTOR vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
		PRINTLN("[TS] [MSRAND] - REMOVE_ALL_ENTITIES_AFTER_MC_CUTSCENE CLEAR_AREA_LEAVE_VEHICLE_HEALTH(", vPlayer, ", 100.0, FALSE, TRUE, FALSE, FALSE)")
		CLEAR_AREA_LEAVE_VEHICLE_HEALTH(vPlayer, 100.0, FALSE, TRUE, FALSE, FALSE)	
	ENDIF
ENDPROC	

//Set the status of the transition joining
PROC SET_MAINTAIN_STRAND_MISSION_STATUS(INT iStatus)
	#IF IS_DEBUG_BUILD
	SWITCH iStatus
		CASE ciMAINTAIN_STRAND_MISSION_WAIT						PRINTLN("[TS] [MSRAND] - SET_MAINTAIN_STRAND_MISSION_STATUS - WAIT")					BREAK
		CASE ciMAINTAIN_STRAND_MISSION_START_CUTSCENE			PRINTLN("[TS] [MSRAND] - SET_MAINTAIN_STRAND_MISSION_STATUS - START_CUTSCENE")			BREAK
		CASE ciMAINTAIN_STRAND_MISSION_MISSION_END				PRINTLN("[TS] [MSRAND] - SET_MAINTAIN_STRAND_MISSION_STATUS - MISSION_END")				BREAK
		CASE ciMAINTAIN_STRAND_MISSION_DOWNLOAD_NEXT			PRINTLN("[TS] [MSRAND] - SET_MAINTAIN_STRAND_MISSION_STATUS - DOWNLOAD_NEXT")			BREAK
		CASE ciMAINTAIN_STRAND_MISSION_WAIT_FOR_ALL_PLAYERS		PRINTLN("[TS] [MSRAND] - SET_MAINTAIN_STRAND_MISSION_STATUS - WAIT_FOR_ALL_PLAYERS")	BREAK
		CASE ciMAINTAIN_STRAND_MISSION_SET_UP_NEXT				PRINTLN("[TS] [MSRAND] - SET_MAINTAIN_STRAND_MISSION_STATUS - SET_UP_NEXT")				BREAK
		CASE ciMAINTAIN_STRAND_MISSION_CUT_WAIT					PRINTLN("[TS] [MSRAND] - SET_MAINTAIN_STRAND_MISSION_STATUS - CUT_WAIT")				BREAK
		CASE ciMAINTAIN_STRAND_MISSION_SCTV_HOLDING				PRINTLN("[TS] [MSRAND] - SET_MAINTAIN_STRAND_MISSION_STATUS - SCTV_HOLDING")			BREAK
		CASE ciMAINTAIN_STRAND_MISSION_CORONA_WAIT				PRINTLN("[TS] [MSRAND] - SET_MAINTAIN_STRAND_MISSION_STATUS - CORONA_WAIT")				BREAK
		CASE ciMAINTAIN_STRAND_MISSION_RESET					PRINTLN("[TS] [MSRAND] - SET_MAINTAIN_STRAND_MISSION_STATUS - RESET")					BREAK
	ENDSWITCH
	#ENDIF
	g_sTransitionSessionData.sStrandMissionData.iSwitchState = iStatus
ENDPROC

//Returns true when the corona is ready
FUNC BOOL HAS_INSTANCED_CONTENT_SCRIPT_BEEN_RELAUNCHED()
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
		PRINTLN("[TS] [MSRAND] - HAS_INSTANCED_CONTENT_SCRIPT_BEEN_RELAUNCHED returning TRUE ")	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC 


PROC MAINTAIN_MPH_HUM_KEY_MCS1_SRL(INT &iStageSRL #IF IS_DEBUG_BUILD, BOOL bForce = FALSE #ENDIF )
	SWITCH iStageSRL	
		CASE 0
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<119.2551, -1091.0261, 34.6653>>, <<5.0, 5.0, 5.0>>)
			#IF IS_DEBUG_BUILD
			OR bForce
			#ENDIF 
				iStageSRL++
			ENDIF
		BREAK
		CASE 1
			PREFETCH_SRL("mph_hum_key_mcs1_Script_srl")
			SET_SRL_READAHEAD_TIMES(5, 5, 5, 5)
			SET_SRL_LONG_JUMP_MODE(TRUE)
			PRINTLN("[MAINTAIN_MPH_HUM_KEY_MCS1_SRL] PREFETCH_SRL(\"mph_hum_key_mcs1_Script_srl\")")
			iStageSRL++
		BREAK
		CASE 2
			IF IS_SRL_LOADED()
			AND GET_CUTSCENE_TOTAL_DURATION() - GET_CUTSCENE_TIME() < 1000
				BEGIN_SRL()
				PRINTLN("[MAINTAIN_MPH_HUM_KEY_MCS1_SRL] IS_SRL_LOADED")
				iStageSRL++
			ENDIF
		BREAK
		CASE 3
			SET_SRL_TIME(TO_FLOAT(GET_GAME_TIMER()))
			PRINTLN("[MAINTAIN_MPH_HUM_KEY_MCS1_SRL] SET_SRL_TIME(", GET_GAME_TIMER(), ")")
			IF NOT IS_CUTSCENE_PLAYING()
				PRINTLN("[MAINTAIN_MPH_HUM_KEY_MCS1_SRL] END_SRL")
				END_SRL()
				iStageSRL++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_WARP_PLAYER_CHECK_FOR_MPH_HUM_KEY_MCS1_CUT()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<119.682190,-1111.918701,34.165279>>, <<119.577553,-1089.630249,38.925514>>, 8.750000)
		PRINTLN("[TS] [MSRAND] - SET_ENTITY_COORDS(<<119.3599, -1090.5702, 34.6653>>, 345.1098)")	
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<119.3599, -1090.5702, 34.6653>>, FALSE)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 345.1098)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_END_OF_MISSION_HEIST_LOGIC_BE_PROCESSED_BECAUSE_OF_STRAND_MISSION()
	IF IS_A_STRAND_MISSION_BEING_INITIALISED() // AKA "No mission is chained AFTER this one"
		PRINTLN("[TS] [MSRAND] - SHOULD_END_OF_MISSION_HEIST_LOGIC_BE_PROCESSED_BECAUSE_OF_STRAND_MISSION - IS_A_STRAND_MISSION_BEING_INITIALISED")	
		RETURN FALSE
	ENDIF
	// AKA "No mission is chained AFTER this one"
	IF NOT CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
		PRINTLN("[TS] [MSRAND] - SHOULD_END_OF_MISSION_HEIST_LOGIC_BE_PROCESSED_BECAUSE_OF_STRAND_MISSION - CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION")	
		RETURN FALSE
	ENDIF
	IF IS_THIS_IS_A_STRAND_MISSION() 
	AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
		PRINTLN("[TS] [MSRAND] - SHOULD_END_OF_MISSION_HEIST_LOGIC_BE_PROCESSED_BECAUSE_OF_STRAND_MISSION - IS_THIS_IS_A_STRAND_MISSION - HAS_FIRST_STRAND_MISSION_BEEN_PASSED")	
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD

CONST_INT ciPLACEHOLDER_CUTSCENE_STRAND_MISSION					0
CONST_INT ciPLACEHOLDER_CUTSCENE_MID_STRAND_CUT					1
CONST_INT ciPLACEHOLDER_CUTSCENE_INTRO							2
CONST_INT ciPLACEHOLDER_CUTSCENE_NIGHTCLUB_SETUP				3
CONST_INT ciPLACEHOLDER_CUTSCENE_NIGHTCLUB_SOLOMUN				4
CONST_INT ciPLACEHOLDER_CUTSCENE_NIGHTCLUB_DJ_SWITCH			5
CONST_INT ciPLACEHOLDER_CUTSCENE_ARENA_MOCAP_1					6
CONST_INT ciPLACEHOLDER_CUTSCENE_ARENA_MOCAP_2					7
CONST_INT ciPLACEHOLDER_CUTSCENE_ISLAND_HEIST_INTRO				8
CONST_INT ciPLACEHOLDER_CUTSCENE_END_OF_DJ_SET					9
CONST_INT ciPLACEHOLDER_CUTSCENE_AUTO_SHOP						10
CONST_INT ciPLACEHOLDER_CUTSCENE_CAR_MEET						11
CONST_INT ciPLACEHOLDER_CUTSCENE_AUTO_SHOP_POST_SETUP			12
CONST_INT ciPLACEHOLDER_CUTSCENE_MUSIC_STUDIO_DRE_ARRIVES		13
CONST_INT ciPLACEHOLDER_CUTSCENE_MUSIC_STUDIO_MUSICIAN_ARRIVES	14
CONST_INT ciPLACEHOLDER_CUTSCENE_MUSIC_STUDIO_MUSICIAN_LEAVES	15
CONST_INT ciPLACEHOLDER_CUTSCENE_MUSIC_STUDIO_DRE_LEAVES		16

//Do a place holder cut scene
FUNC BOOL RUN_PLACEHOLDER_MO_CAP(INT &iStage, SCRIPT_TIMER &Timer, INT iCutsceneType = ciPLACEHOLDER_CUTSCENE_STRAND_MISSION)

	INT iTotalTime
	STRING strMainText, strStrapline
	
	SWITCH iCutsceneType
		CASE ciPLACEHOLDER_CUTSCENE_STRAND_MISSION
		DEFAULT
			iTotalTime = 25000
			strMainText = "PLACE HOLDER MO CAP"
			strStrapline = ""
		BREAK
		
		CASE ciPLACEHOLDER_CUTSCENE_INTRO
			iTotalTime = 10000
			strMainText = "PLACE HOLDER LOWRIDER INTRO MO CAP"
			strStrapline = ""
		BREAK
		
		CASE ciPLACEHOLDER_CUTSCENE_MID_STRAND_CUT
			iTotalTime = 10000
			strMainText = "PLACE HOLDER MID STRAND CUTSCENE"
			strStrapline = ""
		BREAK
		
		CASE ciPLACEHOLDER_CUTSCENE_NIGHTCLUB_SETUP
			iTotalTime = 5000
			strMainText = "PLACE HOLDER MO CAP"
			strStrapline = "Second Intro Cutscene"
		BREAK
		
		CASE ciPLACEHOLDER_CUTSCENE_NIGHTCLUB_SOLOMUN
			iTotalTime = 5000
			strMainText = "PLACE HOLDER MO CAP"
			strStrapline = "Solomun - Crash Landing"
		BREAK
		
		CASE ciPLACEHOLDER_CUTSCENE_NIGHTCLUB_DJ_SWITCH
			iTotalTime = 5000
			strMainText = "PLACE HOLDER MO CAP"
			strStrapline = "DJ Switch Cutscene"
		BREAK
		
		CASE ciPLACEHOLDER_CUTSCENE_ARENA_MOCAP_1
			iTotalTime = 5000
			strMainText = "PLACE HOLDER MO CAP"
			strStrapline = "INTRO 1"
		BREAK
		
		CASE ciPLACEHOLDER_CUTSCENE_ARENA_MOCAP_2
			iTotalTime = 5000
			strMainText = "PLACE HOLDER MO CAP"
			strStrapline = "INTRO 2"
		BREAK
		
		CASE ciPLACEHOLDER_CUTSCENE_ISLAND_HEIST_INTRO
			iTotalTime = 5000
			strMainText = "PLACE HOLDER MO CAP"
			strStrapline = "Heist Island - Meeting Miguel Madrazo"
		BREAK
		
		CASE ciPLACEHOLDER_CUTSCENE_END_OF_DJ_SET
			iTotalTime = 10000
			strMainText = "PLACE HOLDER CUTSCENE"
			strStrapline = "Beach Party - Time of day transitions from sunset to sunrise"
		BREAK	
		CASE ciPLACEHOLDER_CUTSCENE_AUTO_SHOP
			iTotalTime = 10000
			strMainText = "PLACE HOLDER CUTSCENE"
			strStrapline = "AUTO SHOP - SETUP CUTSCENE"
		BREAK
		CASE ciPLACEHOLDER_CUTSCENE_CAR_MEET
			iTotalTime = 10000
			strMainText = "PLACE HOLDER CUTSCENE"
			strStrapline = "CAR MEET - INTRO CUTSCENE"
		BREAK
		CASE ciPLACEHOLDER_CUTSCENE_AUTO_SHOP_POST_SETUP
			iTotalTime = 10000
			strMainText = "PLACE HOLDER CUTSCENE"
			strStrapline = "AUTO SHOP - POST SETUP CUTSCENE"
		BREAK
		CASE ciPLACEHOLDER_CUTSCENE_MUSIC_STUDIO_DRE_ARRIVES
			iTotalTime = 4000
			strMainText = "PLACEHOLDER CUTSCENE"
			strStrapline = "MUSIC STUDIO - DR DRE ARRIVES"
		BREAK
		CASE ciPLACEHOLDER_CUTSCENE_MUSIC_STUDIO_MUSICIAN_ARRIVES
			iTotalTime = 4000
			strMainText = "PLACEHOLDER CUTSCENE"
			strStrapline = "MUSIC STUDIO - MUSICIAN ARRIVES"
		BREAK
		CASE ciPLACEHOLDER_CUTSCENE_MUSIC_STUDIO_MUSICIAN_LEAVES
			iTotalTime = 4000
			strMainText = "PLACEHOLDER CUTSCENE"
			strStrapline = "MUSIC STUDIO - MUSICIAN LEAVES"
		BREAK
		CASE ciPLACEHOLDER_CUTSCENE_MUSIC_STUDIO_DRE_LEAVES
			iTotalTime = 4000
			strMainText = "PLACEHOLDER CUTSCENE"
			strStrapline = "MUSIC STUDIO - DR DRE LEAVES"
		BREAK
	ENDSWITCH
	


	SWITCH iStage
		CASE 0
			DO_SCREEN_FADE_OUT(500)
			iStage++
			PRINTLN("[TS] [MSRAND] - RUN_PLACEHOLDER_MO_CAP 0")	
		BREAK
		CASE 1
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
			ENDIF
			IF IS_SCREEN_FADED_OUT()
				#IF FEATURE_FIXER
				g_bIsMocapPlaying = TRUE
				#ENDIF
				DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0 ,255)
				DO_SCREEN_FADE_IN(0)
				START_NET_TIMER(Timer)
				PRINTLN("[TS] [MSRAND] - RUN_PLACEHOLDER_MO_CAP 1")	
				iStage++
			ENDIF
		BREAK
		CASE 2
			FLOAT fTextCenter
			FLOAT fTextScaleCut
			FLOAT fTextY
			fTextCenter = 0.5
			fTextY 		= 0.4
			fTextScaleCut 	= 0.45
			DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0 ,255)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_SCALE(0.4, fTextScaleCut)
			SET_TEXT_WRAP(0.15, 0.85)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(fTextCenter, fTextY, "STRING", strMainText)
			
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_SCALE(0.4, fTextScaleCut)
			SET_TEXT_WRAP(0.15, 0.85)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(fTextCenter, fTextY + 0.1, "STRING", strStrapline)
			
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			IF HAS_NET_TIMER_EXPIRED(Timer, iTotalTime) 
				PRINTLN("[TS] [MSRAND] - RUN_PLACEHOLDER_MO_CAP 2")	
				DO_SCREEN_FADE_OUT(0)
				iStage++
			ENDIF	
		BREAK
		CASE 3
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(0)
			ENDIF
			DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0 ,255)
			PRINTLN("[TS] [MSRAND] - RUN_PLACEHOLDER_MO_CAP 3")	
			iStage++
		BREAK
		CASE 4
			#IF FEATURE_FIXER
			g_bIsMocapPlaying = FALSE
			#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH		
	RETURN FALSE
ENDFUNC
#ENDIF

PROC CLEAR_ON_STRAND_MISSION_CUTSCENE()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_ON_STRAND_MISSION_CUTSCENE)
		PRINTLN("[TS] - CLEAR_ON_STRAND_MISSION_CUTSCENE")					
	ENDIF
	#ENDIF
	CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_ON_STRAND_MISSION_CUTSCENE)
ENDPROC
PROC SET_ON_STRAND_MISSION_CUTSCENE()
	PRINTLN("[TS] - SET_ON_STRAND_MISSION_CUTSCENE")					
	SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_ON_STRAND_MISSION_CUTSCENE)
ENDPROC
FUNC BOOL IS_PLAYER_ON_STRAND_MISSION_CUTSCENE(PLAYER_INDEX piPassed)
	RETURN IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(piPassed)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_ON_STRAND_MISSION_CUTSCENE)
ENDFUNC

#IF IS_DEBUG_BUILD
CONST_INT NUM_STRUCT_VARS 10
STRUCT FAKE_EVENTS_DATA
	INT iScriptEventToSend
	INT iFromPlayer
	INT iPlayersBitField
	INT iNumVariables = 2
	BOOL bPlayer[NUM_NETWORK_PLAYERS]
	INT iDataType[NUM_STRUCT_VARS]
	INT iIntValue[NUM_STRUCT_VARS]
	FLOAT fFloatValue[NUM_STRUCT_VARS]
	BOOL bBoolValue[NUM_STRUCT_VARS]
	BOOL bTriggerEvent
	BOOL bSpamEvent
	BOOL bIsTicker
	BOOL bIsBounty
	BOOL bForceOnToMission
ENDSTRUCT 
PROC SEND_ANY_SCRIPT_EVENT_WIDGETS(FAKE_EVENTS_DATA &sFakeEvents)
	INT iLoop
	TEXT_LABEL tl15
	START_WIDGET_GROUP(" Send Any Script Event")
		ADD_WIDGET_INT_READ_ONLY("Player Unique ID", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iUniquePlayersHash)
		ADD_WIDGET_INT_READ_ONLY("Session Unique ID 0", GlobalServerBD_FM_events.iUniqueSessionHash0)
		ADD_WIDGET_INT_READ_ONLY("Session Unique ID 1", GlobalServerBD_FM_events.iUniqueSessionHash1)
		
		ADD_WIDGET_INT_SLIDER("Script Event Hash", sFakeEvents.iScriptEventToSend, LOWEST_INT, HIGHEST_INT , 1)
		ADD_WIDGET_INT_SLIDER("From Player", sFakeEvents.iFromPlayer, 0, NUM_NETWORK_PLAYERS , 1)
		ADD_WIDGET_BOOL("Trigger ", sFakeEvents.bTriggerEvent)
		ADD_WIDGET_BOOL("Event is a Ticker", sFakeEvents.bIsTicker)
		ADD_WIDGET_BOOL("Event is a Bounty", sFakeEvents.bIsBounty)
		ADD_WIDGET_BOOL("Event is a ForceOnToMission", sFakeEvents.bForceOnToMission)
		
		ADD_WIDGET_BOOL("Spam Event ", sFakeEvents.bSpamEvent)
		START_WIDGET_GROUP("Players Bit Field")
			REPEAT NUM_NETWORK_PLAYERS iLoop
				tl15 = "Player "
				tl15+=iLoop
				ADD_WIDGET_BOOL(tl15, sFakeEvents.bPlayer[iLoop])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("STRUCT Vars")
			ADD_WIDGET_INT_SLIDER("NUM Vars in STRUCT", sFakeEvents.iNumVariables, 2, 22 , 1)
			REPEAT NUM_STRUCT_VARS iLoop
				tl15 = "Type "
				tl15+=iLoop
				ADD_WIDGET_INT_SLIDER(tl15, sFakeEvents.iDataType[iLoop], LOWEST_INT, HIGHEST_INT , 1)
				tl15 = "INT Value "
				tl15+=iLoop
				ADD_WIDGET_INT_SLIDER(tl15, sFakeEvents.iIntValue[iLoop], LOWEST_INT, HIGHEST_INT , 1)
				tl15 = "Float Value "
				tl15+=iLoop
				ADD_WIDGET_FLOAT_SLIDER(tl15, sFakeEvents.fFloatValue[iLoop], -999999999.0, 999999999.0 , 1)
				tl15 = "BOOL Value "
				tl15+=iLoop
				ADD_WIDGET_BOOL(tl15, sFakeEvents.bBoolValue[iLoop])
			ENDREPEAT
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

FUNC INT SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(FAKE_EVENTS_DATA &sFakeEvents)
	INT iReturn, iLoop
	REPEAT NUM_NETWORK_PLAYERS iLoop
		IF sFakeEvents.bPlayer[iLoop]
			SET_BIT(iReturn, iLoop)
		ENDIF
	ENDREPEAT
	IF iReturn = 0
		SET_BIT(iReturn, NATIVE_TO_INT(PLAYER_ID()))
	ENDIF
	RETURN iReturn
ENDFUNC

STRUCT STRUCT_SCRIPT_EVENT_3_VAR_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_3_VAR_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_3_VAR_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
ENDSTRUCT

PROC SEND_TU_SCRIPT_EVENT_3_VAR_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_3_VAR_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC 
PROC SEND_TU_SCRIPT_EVENT_3_VAR_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_3_VAR_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC 
PROC SEND_TU_SCRIPT_EVENT_3_VAR_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_3_VAR_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC 

PROC SEND_TU_SCRIPT_EVENT_3_VAR(FAKE_EVENTS_DATA &sFakeEvents)
	SWITCH sFakeEvents.iDataType[0]
		CASE 0
			SEND_TU_SCRIPT_EVENT_3_VAR_INT(sFakeEvents)
		BREAK
		CASE 1
			SEND_TU_SCRIPT_EVENT_3_VAR_FLOAT(sFakeEvents)
		BREAK
		CASE 2
			SEND_TU_SCRIPT_EVENT_3_VAR_BOOL(sFakeEvents)
		BREAK
	ENDSWITCH
ENDPROC


STRUCT STRUCT_SCRIPT_EVENT_4_VAR_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_4_VAR_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_4_VAR_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_4_VAR_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_4_VAR_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_4_VAR_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_4_VAR_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_4_VAR_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_4_VAR_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
ENDSTRUCT

PROC SEND_TU_SCRIPT_EVENT_4_VAR_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_4_VAR_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_4_VAR_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_4_VAR_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_4_VAR_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_4_VAR_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_4_VAR_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_4_VAR_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_4_VAR_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_4_VAR_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_4_VAR_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_4_VAR_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_4_VAR_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_4_VAR_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_4_VAR_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_4_VAR_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_4_VAR_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_4_VAR_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_4_VAR(FAKE_EVENTS_DATA &sFakeEvents)
	SWITCH sFakeEvents.iDataType[0]
		CASE 0
			SWITCH sFakeEvents.iDataType[1]
				CASE 0
					SEND_TU_SCRIPT_EVENT_4_VAR_INT_INT(sFakeEvents)
				BREAK
				CASE 1
					SEND_TU_SCRIPT_EVENT_4_VAR_INT_FLOAT(sFakeEvents)
				BREAK
				CASE 2
					SEND_TU_SCRIPT_EVENT_4_VAR_INT_BOOL(sFakeEvents)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH sFakeEvents.iDataType[1]
				CASE 0
					SEND_TU_SCRIPT_EVENT_4_VAR_FLOAT_INT(sFakeEvents)
				BREAK
				CASE 1
					SEND_TU_SCRIPT_EVENT_4_VAR_FLOAT_FLOAT(sFakeEvents)
				BREAK
				CASE 2
					SEND_TU_SCRIPT_EVENT_4_VAR_FLOAT_BOOL(sFakeEvents)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH sFakeEvents.iDataType[1]
				CASE 0
					SEND_TU_SCRIPT_EVENT_4_VAR_BOOL_INT(sFakeEvents)
				BREAK
				CASE 1
					SEND_TU_SCRIPT_EVENT_4_VAR_BOOL_FLOAT(sFakeEvents)
				BREAK
				CASE 2
					SEND_TU_SCRIPT_EVENT_4_VAR_BOOL_BOOL(sFakeEvents)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

STRUCT STRUCT_SCRIPT_EVENT_5_VAR_INT_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	INT iData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_INT_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_INT_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	BOOL bData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_INT_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_INT_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_INT_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	BOOL bData
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_5_VAR_INT_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_INT_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_INT_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	BOOL bData
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	FLOAT fData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	BOOL bData
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_5_VAR_BOOL_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_BOOL_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_BOOL_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_5_VAR_BOOL_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_BOOL_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_5_VAR_BOOL_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	BOOL bData3
ENDSTRUCT


PROC SEND_TU_SCRIPT_EVENT_5_VAR_INT_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_INT_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.iData3 = sFakeEvents.iIntValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_INT_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_INT_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_INT_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_INT_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_INT_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_INT_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC


PROC SEND_TU_SCRIPT_EVENT_5_VAR_INT_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_INT_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_INT_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_INT_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_INT_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_INT_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_INT_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_INT_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_INT_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_INT_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.fData3 = sFakeEvents.fFloatValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC


PROC SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_BOOL_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_BOOL_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_BOOL_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_BOOL_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_BOOL_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_5_VAR_BOOL_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.bData3 = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_5_VAR(FAKE_EVENTS_DATA &sFakeEvents)
	SWITCH sFakeEvents.iDataType[0]
		CASE 0
			SWITCH sFakeEvents.iDataType[1]
				CASE 0
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SEND_TU_SCRIPT_EVENT_5_VAR_INT_INT_INT(sFakeEvents)
						BREAK
						CASE 1
							SEND_TU_SCRIPT_EVENT_5_VAR_INT_INT_FLOAT(sFakeEvents)
						BREAK
						CASE 2
							SEND_TU_SCRIPT_EVENT_5_VAR_INT_INT_BOOL(sFakeEvents)
						BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SEND_TU_SCRIPT_EVENT_5_VAR_INT_FLOAT_INT(sFakeEvents)
						BREAK
						CASE 1
							SEND_TU_SCRIPT_EVENT_5_VAR_INT_FLOAT_FLOAT(sFakeEvents)
						BREAK
						CASE 2
							SEND_TU_SCRIPT_EVENT_5_VAR_INT_FLOAT_BOOL(sFakeEvents)
						BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SEND_TU_SCRIPT_EVENT_5_VAR_INT_BOOL_INT(sFakeEvents)
						BREAK
						CASE 1
							SEND_TU_SCRIPT_EVENT_5_VAR_INT_BOOL_FLOAT(sFakeEvents)
						BREAK
						CASE 2
							SEND_TU_SCRIPT_EVENT_5_VAR_INT_BOOL_BOOL(sFakeEvents)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH sFakeEvents.iDataType[1]
				CASE 0
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_INT_INT(sFakeEvents)
						BREAK
						CASE 1
							SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_INT_FLOAT(sFakeEvents)
						BREAK
						CASE 2
							SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_INT_BOOL(sFakeEvents)
						BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_INT(sFakeEvents)
						BREAK
						CASE 1
							SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_FLOAT(sFakeEvents)
						BREAK
						CASE 2
							SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_FLOAT_BOOL(sFakeEvents)
						BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_INT(sFakeEvents)
						BREAK
						CASE 1
							SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_FLOAT(sFakeEvents)
						BREAK
						CASE 2
							SEND_TU_SCRIPT_EVENT_5_VAR_FLOAT_BOOL_BOOL(sFakeEvents)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH sFakeEvents.iDataType[1]
				CASE 0
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_INT_INT(sFakeEvents)
						BREAK
						CASE 1
							SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_INT_FLOAT(sFakeEvents)
						BREAK
						CASE 2
							SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_INT_BOOL(sFakeEvents)
						BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_INT(sFakeEvents)
						BREAK
						CASE 1
							SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_FLOAT(sFakeEvents)
						BREAK
						CASE 2
							SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_FLOAT_BOOL(sFakeEvents)
						BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_BOOL_INT(sFakeEvents)
						BREAK
						CASE 1
							SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_BOOL_FLOAT(sFakeEvents)
						BREAK
						CASE 2
							SEND_TU_SCRIPT_EVENT_5_VAR_BOOL_BOOL_BOOL(sFakeEvents)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC




STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	INT iData3
	INT iData4
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	INT iData3
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	INT iData3
	BOOL bData
ENDSTRUCT


STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	FLOAT fData
	INT iData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	FLOAT fData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	FLOAT fData
	BOOL bData
ENDSTRUCT


STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	BOOL bData
	INT iData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	BOOL bData
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	BOOL bData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	INT iData2
	INT iData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	INT iData2
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	INT iData2
	BOOL bData
ENDSTRUCT


STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	FLOAT fData2
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	FLOAT fData2
	FLOAT fData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	FLOAT fData2
	BOOL bData
ENDSTRUCT


STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	BOOL bData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	BOOL bData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	FLOAT fData
	BOOL bData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	INT iData2
	INT iData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	INT iData2
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	INT iData2
	BOOL bData2
ENDSTRUCT


STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	FLOAT fData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	FLOAT fData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	FLOAT fData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	BOOL bData2
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	BOOL bData2
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	BOOL bData
	BOOL bData2
	BOOL bData3
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	INT iData2
	INT iData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	INT iData2
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	INT iData2
	BOOL bData
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	FLOAT fData2
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	FLOAT fData2
	FLOAT fData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	FLOAT fData2
	BOOL bData
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	BOOL bData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	BOOL bData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	INT iData
	BOOL bData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	INT iData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	INT iData
	FLOAT fData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	INT iData
	BOOL bData
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	FLOAT fData3
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	FLOAT fData3
	FLOAT fData4
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	FLOAT fData3
	BOOL bData
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	BOOL bData
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	BOOL bData
	FLOAT fData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	FLOAT fData2
	BOOL bData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	INT iData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	INT iData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	INT iData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	FLOAT fData2
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	FLOAT fData2
	FLOAT fData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	FLOAT fData2
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	BOOL bData2
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	BOOL bData2
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	FLOAT fData
	BOOL bData
	BOOL bData2
	BOOL bData3
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	INT iData2
	INT iData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	INT iData2
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	INT iData2
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	FLOAT fData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	FLOAT fData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	FLOAT fData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	BOOL bData2
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	BOOL bData2
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	INT iData
	BOOL bData2
	BOOL bData3
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	INT iData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	INT iData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	INT iData
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	FLOAT fData2
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	FLOAT fData2
	FLOAT fData3
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	FLOAT fData2
	BOOL bData2
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	BOOL bData2
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	BOOL bData2
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	FLOAT fData
	BOOL bData2
	BOOL bData3
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	INT iData
	INT iData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	INT iData
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	INT iData
	BOOL bData3
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	FLOAT fData
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	FLOAT fData
	FLOAT fData2
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	FLOAT fData
	BOOL bData3
ENDSTRUCT

STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_INT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	BOOL bData3
	INT iData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_FLOAT
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	BOOL bData3
	FLOAT fData
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_BOOL
	STRUCT_EVENT_COMMON_DETAILS Details 
	BOOL bData
	BOOL bData2
	BOOL bData3
	BOOL bData4
ENDSTRUCT

PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.iData3 = sFakeEvents.iIntValue[2]
	Event.iData4 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.iData3 = sFakeEvents.iIntValue[2]
	Event.fData = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.iData3 = sFakeEvents.iIntValue[2]
	Event.bData = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC



PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	Event.iData3 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	Event.fData2 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	Event.bData = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.iData3 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.fData = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	Event.iData3 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	Event.fData2 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	Event.bData = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC


PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	Event.fData3 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	Event.bData = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.fData2 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	Event.iData3 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2] 
	Event.fData = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	Event.fData2 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	Event.fData = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	Event.bData3 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	Event.iData3 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	Event.fData2 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	Event.bData = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	Event.fData3 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	Event.bData = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.fData2 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	Event.fData3 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	Event.bData = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.fData3 = sFakeEvents.fFloatValue[2]
	Event.iData = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.fData3 = sFakeEvents.fFloatValue[2]
	Event.fData4 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.fData3 = sFakeEvents.fFloatValue[2]
	Event.bData = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.iData = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.fData3 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.fData2 = sFakeEvents.fFloatValue[1]
	Event.bData = sFakeEvents.bBoolValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC


PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	Event.fData2 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	Event.iData = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	Event.fData3 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	Event.iData = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	Event.fData2 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.fData = sFakeEvents.fFloatValue[0]
	Event.bData = sFakeEvents.bBoolValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	Event.bData3 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC


PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	Event.iData3 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	Event.fData = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.iData2 = sFakeEvents.iIntValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	Event.fData2 = sFakeEvents.fFloatValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.iData = sFakeEvents.iIntValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	Event.bData3 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC


PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	Event.iData = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.fData2 = sFakeEvents.fFloatValue[2]
	Event.bData2 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	Event.iData = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.fData = sFakeEvents.fFloatValue[1]
	Event.bData2 = sFakeEvents.bBoolValue[2]
	Event.bData3 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	Event.iData2 = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.iData = sFakeEvents.iIntValue[2]
	Event.bData3 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	Event.iData = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.fData = sFakeEvents.fFloatValue[2]
	Event.bData3 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_INT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_INT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.bData3 = sFakeEvents.bBoolValue[2]
	Event.iData = sFakeEvents.iIntValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_FLOAT(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_FLOAT Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.bData3 = sFakeEvents.bBoolValue[2]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_BOOL(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_BOOL Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.bData = sFakeEvents.bBoolValue[0]
	Event.bData2 = sFakeEvents.bBoolValue[1]
	Event.bData3 = sFakeEvents.bBoolValue[2]
	Event.bData4 = sFakeEvents.bBoolValue[3]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_6_VAR(FAKE_EVENTS_DATA &sFakeEvents)
	SWITCH sFakeEvents.iDataType[0]
		CASE 0
			SWITCH sFakeEvents.iDataType[1]
				CASE 0
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_INT_INT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_INT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_INT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_INT_FLOAT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_FLOAT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_FLOAT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_INT_BOOL(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_FLOAT_BOOL(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_INT_BOOL_BOOL(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_INT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_INT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_INT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_FLOAT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_FLOAT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_FLOAT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_INT_BOOL(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_FLOAT_BOOL(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_FLOAT_BOOL_BOOL(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_INT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_INT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_INT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_FLOAT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_FLOAT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_FLOAT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_INT_BOOL(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_FLOAT_BOOL(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_INT_BOOL_BOOL_BOOL(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH sFakeEvents.iDataType[1]
				CASE 0
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_INT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_INT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_INT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_FLOAT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_FLOAT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_FLOAT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_INT_BOOL(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_FLOAT_BOOL(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_INT_BOOL_BOOL(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_INT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_INT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_INT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_FLOAT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_FLOAT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_FLOAT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_INT_BOOL(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_FLOAT_BOOL(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_FLOAT_BOOL_BOOL(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_INT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_INT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_INT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_FLOAT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_FLOAT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_FLOAT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_INT_BOOL(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_FLOAT_BOOL(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_FLOAT_BOOL_BOOL_BOOL(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH sFakeEvents.iDataType[1]
				CASE 0
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_INT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_INT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_INT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_FLOAT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_FLOAT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_FLOAT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_INT_BOOL(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_FLOAT_BOOL(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_INT_BOOL_BOOL(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_INT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_INT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_INT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_FLOAT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_FLOAT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_FLOAT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_INT_BOOL(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_FLOAT_BOOL(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_FLOAT_BOOL_BOOL(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH sFakeEvents.iDataType[2]
						CASE 0
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_INT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_INT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_INT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_FLOAT(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_FLOAT(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_FLOAT(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
						CASE 2
							SWITCH sFakeEvents.iDataType[3]
								CASE 0
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_INT_BOOL(sFakeEvents)
								BREAK
								CASE 1
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_FLOAT_BOOL(sFakeEvents)
								BREAK
								CASE 2
									SEND_TU_SCRIPT_EVENT_6_VAR_BOOL_BOOL_BOOL_BOOL(sFakeEvents)
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC
////PURPOSE: data structure for the ticker message script event
//STRUCT SCRIPT_EVENT_DATA_TICKER_MESSAGE
//	STRUCT_EVENT_COMMON_DETAILS Details 
//	TICKER_EVENTS TickerEvent
//	INT dataInt
//	INT dataInt2
//	FLOAT dataFloat
//	TEXT_LABEL_15 dataString
//	PLAYER_INDEX playerID
//	PLAYER_INDEX playerID2
//	PLAYER_INDEX playerID3
//	PLAYER_INDEX playerID4
//ENDSTRUCT
STRUCT SCRIPT_EVENT_DATA_BOUNTY_DEBUG
	STRUCT_EVENT_COMMON_DETAILS Details
	PLAYER_INDEX playerID
	GAMER_HANDLE bountyPlacer
	INT iResultBS
	INT iBountyValue
	INT iTimeSinceBountyStart
	INT iVariation
	INT iUniqueID
ENDSTRUCT
STRUCT m_structEventForcePlayerOntoMissionDebug
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_DATA				missionData				// The mission data (to ensure it is available when mission triggering needs it - in case the slot data is a bit slow getting broadcast)
	INT							bitsOptions				// BITFIELD of options to tailor the Mission Triggering based on flags supplied by Mission Controller
	INT 						UniqueId
ENDSTRUCT



STRUCT STRUCT_SCRIPT_EVENT_7_VAR
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	INT iData3
	INT iData4
	INT iData5
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_8_VAR
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	INT iData3
	INT iData4
	INT iData5
	INT iData6
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_9_VAR
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	INT iData3
	INT iData4
	INT iData5
	INT iData6
	INT iData7
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_10_VAR
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	INT iData3
	INT iData4
	INT iData5
	INT iData6
	INT iData7
	INT iData8
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_11_VAR
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	INT iData3
	INT iData4
	INT iData5
	INT iData6
	INT iData7
	INT iData8
	INT iData9
ENDSTRUCT
STRUCT STRUCT_SCRIPT_EVENT_12_VAR
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iData
	INT iData2
	INT iData3
	INT iData4
	INT iData5
	INT iData6
	INT iData7
	INT iData8
	INT iData9
	INT iData10
ENDSTRUCT

PROC SEND_TU_SCRIPT_EVENT_7_VAR(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_7_VAR Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.iData3 = sFakeEvents.iIntValue[2]
	Event.iData4 = sFakeEvents.iIntValue[3]
	Event.iData5 = sFakeEvents.iIntValue[4]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_8_VAR(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_8_VAR Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.iData3 = sFakeEvents.iIntValue[2]
	Event.iData4 = sFakeEvents.iIntValue[3]
	Event.iData5 = sFakeEvents.iIntValue[4]
	Event.iData6 = sFakeEvents.iIntValue[5]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_9_VAR(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_9_VAR Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.iData3 = sFakeEvents.iIntValue[2]
	Event.iData4 = sFakeEvents.iIntValue[3]
	Event.iData5 = sFakeEvents.iIntValue[4]
	Event.iData6 = sFakeEvents.iIntValue[5]
	Event.iData7 = sFakeEvents.iIntValue[6]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_10_VAR(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_10_VAR Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.iData3 = sFakeEvents.iIntValue[2]
	Event.iData4 = sFakeEvents.iIntValue[3]
	Event.iData5 = sFakeEvents.iIntValue[4]
	Event.iData6 = sFakeEvents.iIntValue[5]
	Event.iData7 = sFakeEvents.iIntValue[6]
	Event.iData8 = sFakeEvents.iIntValue[7]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_TU_SCRIPT_EVENT_11_VAR(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_11_VAR Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.iData3 = sFakeEvents.iIntValue[2]
	Event.iData4 = sFakeEvents.iIntValue[3]
	Event.iData5 = sFakeEvents.iIntValue[4]
	Event.iData6 = sFakeEvents.iIntValue[5]
	Event.iData7 = sFakeEvents.iIntValue[6]
	Event.iData8 = sFakeEvents.iIntValue[7]
	Event.iData9 = sFakeEvents.iIntValue[8]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC

PROC SEND_TU_SCRIPT_EVENT_12_VAR(FAKE_EVENTS_DATA &sFakeEvents)
	STRUCT_SCRIPT_EVENT_12_VAR Event
	Event.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
	Event.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
	Event.iData = sFakeEvents.iIntValue[0]
	Event.iData2 = sFakeEvents.iIntValue[1]
	Event.iData3 = sFakeEvents.iIntValue[2]
	Event.iData4 = sFakeEvents.iIntValue[3]
	Event.iData5 = sFakeEvents.iIntValue[4]
	Event.iData6 = sFakeEvents.iIntValue[5]
	Event.iData7 = sFakeEvents.iIntValue[6]
	Event.iData8 = sFakeEvents.iIntValue[7]
	Event.iData9 = sFakeEvents.iIntValue[8]
	Event.iData10 = sFakeEvents.iIntValue[9]
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
ENDPROC
PROC SEND_ANY_SCRIPT_EVENT(FAKE_EVENTS_DATA &sFakeEvents)
	IF sFakeEvents.bTriggerEvent
		IF sFakeEvents.bForceOnToMission	
			m_structEventForcePlayerOntoMissionDebug EventForce
			EventForce.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
			EventForce.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
			EventForce.UniqueID = sFakeEvents.iIntValue[0]
			EventForce.missionData.mdID.idMission		= INT_TO_ENUM(MP_MISSION, sFakeEvents.iIntValue[1])
			EventForce.missionData.mdID.idVariation		= sFakeEvents.iIntValue[2]
			EventForce.missionData.mdID.idCreator		= sFakeEvents.iIntValue[3]
			EventForce.missionData.mdID.idSharedRegID	= sFakeEvents.iIntValue[4]
			EventForce.missionData.iInstanceId	= sFakeEvents.iIntValue[5]
			EventForce.missionData.mdUniqueID	= sFakeEvents.iIntValue[6]
			EventForce.missionData.iBitSet		= sFakeEvents.iIntValue[7]
			EventForce.missionData.mdGenericInt	= sFakeEvents.iIntValue[8]
			SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, EventForce, SIZE_OF(EventForce), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
		ELIF sFakeEvents.bIsBounty
			SCRIPT_EVENT_DATA_BOUNTY_DEBUG EventBounty
			EventBounty.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
			EventBounty.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
			EventBounty.playerID = INT_TO_PLAYERINDEX(sFakeEvents.iIntValue[0])
			EventBounty.bountyPlacer = GET_GAMER_HANDLE_PLAYER(INT_TO_PLAYERINDEX(sFakeEvents.iIntValue[1]))
			EventBounty.iResultBS =  sFakeEvents.iIntValue[2]
			EventBounty.iBountyValue =  sFakeEvents.iIntValue[3]
			EventBounty.iTimeSinceBountyStart = sFakeEvents.iIntValue[4]
			EventBounty.iVariation = sFakeEvents.iIntValue[5]
			EventBounty.iUniqueID = sFakeEvents.iIntValue[6]
			SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, EventBounty, SIZE_OF(EventBounty), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
		ELIF sFakeEvents.bIsTicker
			SCRIPT_EVENT_DATA_TICKER_MESSAGE sTickerEvent
			sTickerEvent.Details.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
			sTickerEvent.Details.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
			sTickerEvent.TickerEvent = INT_TO_ENUM(TICKER_EVENTS,  sFakeEvents.iIntValue[0])
			sTickerEvent.dataInt = sFakeEvents.iIntValue[1]
			sTickerEvent.dataInt2 = sFakeEvents.iIntValue[2]
			sTickerEvent.dataFloat = sFakeEvents.fFloatValue[3]
			sTickerEvent.playerID = INT_TO_PLAYERINDEX(sFakeEvents.iIntValue[4])
			sTickerEvent.playerID2 = INT_TO_PLAYERINDEX(sFakeEvents.iIntValue[5])
			sTickerEvent.playerID3 = INT_TO_PLAYERINDEX(sFakeEvents.iIntValue[6])
			sTickerEvent.playerID4 = INT_TO_PLAYERINDEX(sFakeEvents.iIntValue[7])
			SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sTickerEvent, SIZE_OF(sTickerEvent), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
		ELSE
			STRUCT_EVENT_COMMON_DETAILS Event
			SWITCH sFakeEvents.iNumVariables
				CASE 2
					Event.Type = INT_TO_ENUM(SCRIPTED_EVENT_TYPES, sFakeEvents.iScriptEventToSend)
					Event.FromPlayerIndex = INT_TO_PLAYERINDEX(sFakeEvents.iFromPlayer)
					SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SEND_ANY_SCRIPT_EVENT_GET_PLAYER_BIT_SET(sFakeEvents))
				BREAK
				CASE 3
					SEND_TU_SCRIPT_EVENT_3_VAR(sFakeEvents)
				BREAK
				CASE 4
					SEND_TU_SCRIPT_EVENT_4_VAR(sFakeEvents)
				BREAK
				CASE 5
					SEND_TU_SCRIPT_EVENT_5_VAR(sFakeEvents)
				BREAK
				CASE 6
					SEND_TU_SCRIPT_EVENT_6_VAR(sFakeEvents)
				BREAK
				CASE 7
					SEND_TU_SCRIPT_EVENT_7_VAR(sFakeEvents)
				BREAK
				CASE 8
					SEND_TU_SCRIPT_EVENT_8_VAR(sFakeEvents)
				BREAK
				CASE 9
					SEND_TU_SCRIPT_EVENT_9_VAR(sFakeEvents)
				BREAK
				CASE 10
					SEND_TU_SCRIPT_EVENT_10_VAR(sFakeEvents)
				BREAK
				CASE 11
					SEND_TU_SCRIPT_EVENT_11_VAR(sFakeEvents)
				BREAK
				CASE 12
					SEND_TU_SCRIPT_EVENT_12_VAR(sFakeEvents)
				BREAK
			ENDSWITCH
		ENDIF
		IF NOT sFakeEvents.bSpamEvent
			sFakeEvents.bTriggerEvent = FALSE
		ENDIF
	ENDIF
ENDPROC

#ENDIF

