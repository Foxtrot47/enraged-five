///////
///    
///    QUICK MATCH HEADER
///    For all quickmatch woes
///    
///    15/06/2013 - Robert Wright!
///    
///    

USING "net_missions_at_coords.sch"
USING "fmmc_corona_controller.sch"
USING "net_garages.sch"
USING "mp_globals_common_consts.sch"
USING "net_contact_requests.sch"
USING "net_corona_v2.sch"

//PURPOSE: Returns 0 if the player is able to use the Quick Play function. Other number relate to reasons it can't be done.
FUNC INT CAN_DO_QUICK_MATCH(SCRIPT_TIMER &iGarageCheck, INT iActivity = 0)

	// KEITH 17/9/13: Most of the 'cost of play' functions now require a subtype passed in because LTS and CTF mission subtypes need charged whereas normal missions don't
	//					I 'think' we'll get off with dealing only with standard mission type here rather than pass a subtype in from all calls to this function,
	//					so setting up a default subtype of '0' to pass in to the cost of play functions
	INT defaultSubtype = 0

	IF NOT NETWORK_IS_SIGNED_IN()
		RETURN 12
	ENDIF
	
	IF NOT NETWORK_IS_SIGNED_ONLINE()
		RETURN 12
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF NETWORK_IS_SIGNED_ONLINE()
				//IF NETWORK_GET_VC_WALLET_BALANCE() < (GET_FM_ACTIVITY_COST(iActivity, defaultSubtype)+GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED))
				//IF GET_LOCAL_PLAYER_VC_AMOUNT(TRUE) < (GET_FM_ACTIVITY_COST(iActivity, defaultSubtype)+GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED))
				IF !NETWORK_CAN_SPEND_MONEY((GET_FM_ACTIVITY_COST(iActivity, defaultSubtype)+GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED)), FALSE, FALSE, TRUE)
					RETURN 1
				ENDIF
			ENDIF
		ENDIF
		

		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				RETURN 6
			ENDIF
		ELSE
			RETURN 6
		ENDIF
	
		IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)	//HAS_FM_CAR_MOD_TUT_BEEN_DONE(TRUE)
			RETURN 4
		ENDIF
		
		IF Is_There_A_MissionsAtCoords_Focus_Mission()
			RETURN 6
		ENDIF
		
		IF NETWORK_IS_SIGNED_ONLINE()
			//IF NETWORK_GET_VC_WALLET_BALANCE() < GET_FM_ACTIVITY_COST(iActivity, defaultSubtype)
			//IF GET_LOCAL_PLAYER_VC_AMOUNT(TRUE) < GET_FM_ACTIVITY_COST(iActivity, defaultSubtype)
			IF !NETWORK_CAN_SPEND_MONEY(GET_FM_ACTIVITY_COST(iActivity, defaultSubtype), FALSE, FALSE, TRUE)
				RETURN 3
			ENDIF
		ENDIF
		
		IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
			RETURN 7
		ENDIF
		
		
		IF IS_PLAYER_USING_BED_ACTIVITY()
			RETURN 7
		ENDIF
		
		IF IS_BIT_SET(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerUsingTaxi)
			RETURN 8
		ENDIF
		
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_DARTS
		OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_ARM_WRESTLING
		OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
		OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GANGHIDEOUT
		OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_IMPROMPTU_DM
		OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_RACE_TO_POINT
		OR IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL()		
		OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInHeliGunCam = TRUE
		OR IS_ENTITY_IN_GARAGE(PLAYER_PED_ID(), MP_GAR_SIMEON, FALSE, iGarageCheck)
		OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		//**TWH - CMcM - 1442921 - Added IS_PLAYER_ON_ANY_FM_MISSION to CAN_DO_QUICK_MATCH.
		OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), TRUE)
		OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GB_BOSS_DEATHMATCH
			RETURN 9
		ENDIF
		
		IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
			RETURN 10
		ENDIF
		
		IF g_bBrowserVisible = TRUE
			RETURN 11
		ENDIF
		
//		IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
//            IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_IN_A_GARAGE)
//				RETURN 12
//			ENDIF
//		ENDIF
		IF g_bV2CoronaMapBlipIsPremiumRace
		AND IS_HOVERING_OVER_MISSION_CREATOR_BLIP()
		AND IS_PAUSE_MENU_ACTIVE()
			IF g_sV2CoronaVars.iVehicleClassCount = ciCV2_OWN_NO_PERSONAL_VEHCICLE
			OR g_sV2CoronaVars.iVehicleClassCount = ciCV2_DONT_OWN_THIS_VEHCICLE
			OR g_sV2CoronaVars.iVehicleClassCount = ciCV2_DONT_OWN_VEHCICLE_IN_THIS_CLASS	
				RETURN 60 // You do not own a vehicle suitable for this Premium Race.
			ENDIF
			
			IF !g_sMPTunables.bFmCoronaProfessionalPlaylistActive
				RETURN 61
			ENDIF
			
			IF !CV2_CAN_AFFORD_PROFESSIONAL_RACE_FEE()
				RETURN 62
			ENDIF
			
			IF g_sV2CoronaVars.iVehicleClassCount = ciCV2_PERSONAL_VEHCICLE_DESTROYED
				RETURN 63
			ENDIF
			
			IF CV2_IS_PROCSSSIONAL_RACE_IN_COOL_DOWN() 
				RETURN 64
			ENDIF
		ELSE
			g_bV2CoronaMapBlipIsPremiumRace = FALSE
		ENDIF
	//Singleplayer
	ELSE
	
		
		IF HAS_IMPORTANT_STATS_LOADED()
			INT iSlot = GET_LAST_OR_VALID_SLOT(0, GAMEMODE_FM, TRUE)
			
			IF iSlot = -1
				RETURN 5
			ELSE
				//IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				//	RETURN 6
				//ENDIF
				
				IF NOT HAS_FM_INTRO_MISSION_BEEN_DONE(iSlot)
					RETURN 4
				ENDIF
				
				IF NETWORK_IS_SIGNED_ONLINE()
					//IF NETWORK_GET_VC_WALLET_BALANCE() < GET_FM_ACTIVITY_COST(iActivity, defaultSubtype)
					IF !NETWORK_CAN_SPEND_MONEY(GET_FM_ACTIVITY_COST(iActivity, defaultSubtype), FALSE, FALSE, TRUE)
						RETURN 3
					ENDIF
				ENDIF
				
				IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
					RETURN 7
				ENDIF
				
				IF IS_PLAYER_USING_BED_ACTIVITY()
					RETURN 7
				ENDIF
				
				
				
			ENDIF
		
		ELSE
			IF !IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_FM_INTRO_CAN_ACCEPT_INVITES , 0) 
			AND !IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_FM_INTRO_CAN_ACCEPT_INVITES , 1) 
				RETURN 5
			ELSE
				IF (IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_FM_INTRO_CAN_ACCEPT_INVITES , 0) AND !IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_FM_INTRO_CAN_ACCEPT_INVITES , 1))
					//SET_ACTIVE_CHARACTER_SLOT(0)
				ELIF (!IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_FM_INTRO_CAN_ACCEPT_INVITES , 0) AND IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_FM_INTRO_CAN_ACCEPT_INVITES , 1))
					//SET_ACTIVE_CHARACTER_SLOT(1)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN 0
ENDFUNC







