USING "rage_builtins.sch"
USING "globals.sch"
//Game Headers
USING "commands_network.sch"
USING "FMMC_header.sch"
USING "FMMC_Cloud_Loader.sch"
USING "Freemode_header.sch"
USING "net_common_functions.sch"






PROC SETUP_FM_CAR_GENS(FM_CAR_GENERATORS_STRUCT &sPlacedVehicle )
	// city south
	sPlacedVehicle.vPos[0]									=<<139.06,-2535.68,5.00039>>
	sPlacedVehicle.fHead[0]									=26.800190
	sPlacedVehicle.iColour[0]								=0
	sPlacedVehicle.iRespawn[0]								=0
	sPlacedVehicle.mn[0]									=landstalker
	sPlacedVehicle.iRule[0]									=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[0]								=99999
	
	sPlacedVehicle.vPos[1]									=<<-188.898,-1962.56,26.6205>>
	sPlacedVehicle.fHead[1]									=102.399498
	sPlacedVehicle.iColour[1]								=0
	sPlacedVehicle.iRespawn[1]								=0
	sPlacedVehicle.mn[1]									=landstalker
	sPlacedVehicle.iRule[1]									=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[1]								=99999
	
	sPlacedVehicle.vPos[2]									=<<98.8846,-1925.63,19.5499>>
	sPlacedVehicle.fHead[2]									=65.599358
	sPlacedVehicle.iColour[2]								=0
	sPlacedVehicle.iRespawn[2]								=0
	sPlacedVehicle.mn[2]									=landstalker
	sPlacedVehicle.iRule[2]									=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[2]								=99999
	
	sPlacedVehicle.vPos[3]									=<<-223.545,-1706.91,32.9051>>
	sPlacedVehicle.fHead[3]									=216.799194
	sPlacedVehicle.iColour[3]								=0
	sPlacedVehicle.iRespawn[3]								=0
	sPlacedVehicle.mn[3]									=landstalker
	sPlacedVehicle.iRule[3]									=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[3]								=99999
	
	sPlacedVehicle.vPos[4]									=<<-835.183,-1318.05,4.00018>>
	sPlacedVehicle.fHead[4]									=200.399414
	sPlacedVehicle.iColour[4]								=0
	sPlacedVehicle.iRespawn[4]								=0
	sPlacedVehicle.mn[4]									=landstalker
	sPlacedVehicle.iRule[4]									=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[4]								=99999
	 
	//-- Airport - Dave W
	sPlacedVehicle.vPos[5]									=<<-1045.6171, -2716.2539, 12.6723>> 
	sPlacedVehicle.fHead[5]									=241.1808
	sPlacedVehicle.iColour[5]								=0
	sPlacedVehicle.iRespawn[5]								=0
	sPlacedVehicle.mn[5]									=landstalker
	sPlacedVehicle.iRule[5]									=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[5]								=99999
	
	//-- Airport - Dave W
	sPlacedVehicle.vPos[6]									=<<-1073.9534, -2583.4207, 12.6693>>
	sPlacedVehicle.fHead[6]									=150.7360
	sPlacedVehicle.iColour[6]								=0
	sPlacedVehicle.iRespawn[6]								=0
	sPlacedVehicle.mn[6]									=TAILGATER
	sPlacedVehicle.iRule[6]									=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[6]								=99999
	
	 //-- Airport - Dave W
	sPlacedVehicle.vPos[7]									=<<-1047.3307, -2530.7358, 19.0860>>
	sPlacedVehicle.fHead[7]									=150.1163
	sPlacedVehicle.iColour[7]								=0
	sPlacedVehicle.iRespawn[7]								=0
	sPlacedVehicle.mn[7]									=TAILGATER
	sPlacedVehicle.iRule[7]									=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[7]								=99999
	
	//-- Airport - Dave W
	sPlacedVehicle.vPos[8]									=<<-1063.4192, -2712.8584, 19.0467>>
	sPlacedVehicle.fHead[8]									=233.5417
	sPlacedVehicle.iColour[8]								=0
	sPlacedVehicle.iRespawn[8]								=0
	sPlacedVehicle.mn[8]									=LANDSTALKER
	sPlacedVehicle.iRule[8]									=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[8]								=99999
	
	
	//outside lost bar
	sPlacedVehicle.vPos[9]									=<<977.8096,-113.4393,73.8448>>
	sPlacedVehicle.fHead[9]									=RAD_TO_DEG(0.42)
	sPlacedVehicle.iColour[9]								=0
	sPlacedVehicle.iRespawn[9]								=0
	sPlacedVehicle.mn[9]									=DAEMON
	sPlacedVehicle.iRule[9]									=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[9]								=99999
	
	sPlacedVehicle.vPos[10]									=<<979.4814,-111.7564,73.8451>>
	sPlacedVehicle.fHead[10]								=RAD_TO_DEG(0.39)
	sPlacedVehicle.iColour[10]								=0
	sPlacedVehicle.iRespawn[10]								=0
	sPlacedVehicle.mn[10]									=DAEMON
	sPlacedVehicle.iRule[10]								=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[10]							=99999
	
	sPlacedVehicle.vPos[11]									=<<980.9382,-110.5280,73.8404>>
	sPlacedVehicle.fHead[11]								=RAD_TO_DEG(0.42)
	sPlacedVehicle.iColour[11]								=0
	sPlacedVehicle.iRespawn[11]								=0
	sPlacedVehicle.mn[11]									=DAEMON
	sPlacedVehicle.iRule[11]								=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[11]							=99999
	
	//outside Vagos 
	sPlacedVehicle.vPos[12]									=<<964.4380,-1823.5001,30.8740>>
	sPlacedVehicle.fHead[12]								=RAD_TO_DEG(3.01)
	sPlacedVehicle.iColour[12]								=0
	sPlacedVehicle.iRespawn[12]								=0
	sPlacedVehicle.mn[12]									=CAVALCADE
	sPlacedVehicle.iRule[12]								=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[12]							=99999
	
	sPlacedVehicle.vPos[13]									=<<969.1996,-1824.0730,30.9058>>
	sPlacedVehicle.fHead[13]								=RAD_TO_DEG(3.01)
	sPlacedVehicle.iColour[13]								=0
	sPlacedVehicle.iRespawn[13]								=0
	sPlacedVehicle.mn[13]									=CAVALCADE
	sPlacedVehicle.iRule[13]								=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[13]							=99999
	
	sPlacedVehicle.vPos[14]									=<<987.1173,-1824.3915,30.9374>>
	sPlacedVehicle.fHead[14]								=RAD_TO_DEG(-0.07)
	sPlacedVehicle.iColour[14]								=0
	sPlacedVehicle.iRespawn[14]								=0
	sPlacedVehicle.mn[14]									=CAVALCADE
	sPlacedVehicle.iRule[14]								=FMMC_OBJECTIVE_LOGIC_NONE
	sPlacedVehicle.iPriority[14]							=99999
	

ENDPROC


PROC CREATE_ALL_FM_CAR_GENS(FM_CAR_GENERATORS_STRUCT &sPlacedVehicle,  BOOL &bhave_fm_car_gens_been_created)//, BOOL bOnFoot)

	
	INT i

	bhave_fm_car_gens_been_created = TRUE
	
	FOR i = 0 TO (number_of_FM_car_gens- 1)
		//HAS_SERVER_CREATED_MISSION_vg_FM_vehicle_generators(MC_serverBD)
		
		
		IF DOES_SCRIPT_VEHICLE_GENERATOR_EXIST(sPlacedVehicle.vg_FM_vehicle_generators[i])
			DELETE_SCRIPT_VEHICLE_GENERATOR(sPlacedVehicle.vg_FM_vehicle_generators[i])
		ENDIF

		
		IF NOT DOES_SCRIPT_VEHICLE_GENERATOR_EXIST(sPlacedVehicle.vg_FM_vehicle_generators[i])
	
			IF NOT IS_VECTOR_ZERO(sPlacedVehicle.vpos[i])
				IF i >= 5 AND i <= 7
					//-- High priority airport - for new players arriving in Freemode
					sPlacedVehicle.vg_FM_vehicle_generators[i] = CREATE_SCRIPT_VEHICLE_GENERATOR(sPlacedVehicle.vpos[i], sPlacedVehicle.fHead[i], 5.0,3,sPlacedVehicle.mn[i], -1, -1, -1, -1, TRUE, 0, 0,TRUE)
				ELSE
					sPlacedVehicle.vg_FM_vehicle_generators[i] = CREATE_SCRIPT_VEHICLE_GENERATOR(sPlacedVehicle.vpos[i], sPlacedVehicle.fHead[i], 5.0,3,sPlacedVehicle.mn[i], -1, -1, -1, -1, FALSE, 0, 0,TRUE)
				ENDIF
				SET_SCRIPT_VEHICLE_GENERATOR(sPlacedVehicle.vg_FM_vehicle_generators[i], 1)
				//SET_SCRIPT_VEHICLE_GENERATOR
				//CREATE_SCRIPT_VEHICLE_GENERATOR
			ENDIF
			
			
		ENDIF
		
	ENDFOR
		
	
ENDPROC


PROC REMOVE_ALL_FM_CAR_GENS(FM_CAR_GENERATORS_STRUCT &sPlacedVehicle)//, BOOL bOnFoot)
	INT i
	FOR i = 0 TO (number_of_FM_car_gens- 1)
		IF DOES_SCRIPT_VEHICLE_GENERATOR_EXIST(sPlacedVehicle.vg_FM_vehicle_generators[i])
			DELETE_SCRIPT_VEHICLE_GENERATOR(sPlacedVehicle.vg_FM_vehicle_generators[i])
		ENDIF
	ENDFOR
ENDPROC

