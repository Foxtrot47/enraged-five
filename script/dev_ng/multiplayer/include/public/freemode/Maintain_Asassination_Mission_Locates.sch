USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_missions_at_coords.sch"
USING "net_mission_locate_message.sch"
USING "net_GameDir.sch"
USING "net_missions_at_coords_public.sch"
USING "net_script_tunables_new.sch"
USING "net_hud_colour_controller.sch"
USING "net_corona_V2.sch" 
USING "net_activity_selector_groups_contact_missions.sch" 


//The number of locations
CONST_INT ciMAX_ASSASSINATION_LOCATIONS 				1
//The time interval
CONST_INT ciASSASSINATION_MISSION_TIME_INTERVAL 		600
//The number of missions in the list
CONST_INT ciASSASSINATION_MISSION_COUNT 				6

//ResetBitSet
CONST_INT ciPRINTED_MATC_DPADRIGH2 						0
CONST_INT ciPRINTED_FIFTY_METER_HELP 					1
CONST_INT ciDO_MESSAGE_COOL_DOWN 						2

//Non reset bit set
CONST_INT ci_NON_RESET_BLIP_ADDED_ONCE					0
CONST_INT ci_NON_RESET_BLIP_ADDED_ONCE_1				1
CONST_INT ci_NON_RESET_SEND_INVITE_TO_MISSION			2
CONST_INT ci_NON_RESET_SEND_INVITE_TO_MISSION_INITIAL	3
CONST_INT ci_NON_RESET_INVITE_TO_MISSION_HELP			4
CONST_INT ci_NON_RESET_MISSION_UPDATED					5
CONST_INT ci_NON_RESET_MESSAGE_SENT_ONCE				6
CONST_INT ci_NON_RESET_DO_END_OF_MISSION_MESSAGE		7
CONST_INT ci_NON_RESET_CHECK_FOR_END_OF_MISSION_MESSAGE	8

//Mission Stages
CONST_INT ciMAX_ASSASSINATION_STAGE_SET_UP 				0
CONST_INT ciMAX_ASSASSINATION_STAGE_MESSAGE 			1
CONST_INT ciMAX_ASSASSINATION_STAGE_ADD_LOCATES 		2
CONST_INT ciMAX_ASSASSINATION_STAGE_GET_TO_LOCATE		3
CONST_INT ciMAX_ASSASSINATION_STAGE_WAIT				4
CONST_INT ciMAX_ASSASSINATION_STAGE_DEAD				5

//For use with the STAT: MP_STAT_IE_OWNED_VEHICLE_0
CONST_INT ciASSASSINATION_MISSION_POST_MISSION_0		0
CONST_INT ciASSASSINATION_MISSION_POST_MISSION_1		1
CONST_INT ciASSASSINATION_MISSION_POST_MISSION_2		2

//Data for the blip locations
STRUCT ASSASSINATION_LOCATION_DATA
	BLIP_INDEX biBlip
ENDSTRUCT

//Data for controlling the missions
STRUCT ASSASSINATION_MISSION_DATA
	INT iStage
	INT iBitSet
	INT iClosest = -1
	INT iInitialPosixTime
	BOOL bProcessEveryFrame
	SCRIPT_TIMER stInitialDelay
	SCRIPT_TIMER stHelpDelay
	ASSASSINATION_LOCATION_DATA location[ciMAX_ASSASSINATION_LOCATIONS]
ENDSTRUCT

//The location of the missions
FUNC VECTOR GET_ASSASSINATION_MISSION_LOCATION(INT iLocate)
	SWITCH iLocate
//		CASE 0 RETURN <<-1022.11401, 693.60864, 160.24388>>
		CASE 0 RETURN <<1310.74243, 1188.64966, 105.93493>>
	ENDSWITCH
	RETURN <<-1022.11401, 693.60864, 160.24388>>
ENDFUNC

//Clears the help if it's being printed
PROC CLEAR_ASSASSINATION_MISSION_HELP(ASSASSINATION_MISSION_DATA &sAssassinationMissions)
	IF IS_BIT_SET(sAssassinationMissions.iBitSet, ciPRINTED_MATC_DPADRIGH2)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMASS_DPAD")
			CLEAR_HELP()
		ENDIF
		CLEAR_BIT(sAssassinationMissions.iBitSet, ciPRINTED_MATC_DPADRIGH2)
		PRINTLN("CLEAR_ASSASSINATION_MISSION_HELP")
	ENDIF
ENDPROC

//Wipes all the fata that's needed. 
PROC RESET_ASSASSINATION_MISSION(ASSASSINATION_MISSION_DATA &sAssassinationMissions)
	IF sAssassinationMissions.iStage != 0
		PRINTLN("[MAINTAINASSASSINATION_MISSION] - RESET_ASSASSINATION_MISSION")
		CLEAR_ASSASSINATION_MISSION_HELP(sAssassinationMissions)
		INT iLoop
		REPEAT ciMAX_ASSASSINATION_LOCATIONS iLoop
			IF DOES_BLIP_EXIST(sAssassinationMissions.location[iLoop].biBlip)
				REMOVE_BLIP(sAssassinationMissions.location[iLoop].biBlip)
			ENDIF
		ENDREPEAT
		sAssassinationMissions.iStage = 0
		sAssassinationMissions.iInitialPosixTime = 0
		gAssassinationMissionData.iActiveMissionArrayPos = 0
		sAssassinationMissions.iBitSet = 0
		sAssassinationMissions.iClosest = -1
//		RESET_NET_TIMER(sAssassinationMissions.stInitialDelay)
		RESET_NET_TIMER(sAssassinationMissions.stHelpDelay)
		IF !NETWORK_IS_GAME_IN_PROGRESS()
			CLEAR_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_INVITE_TO_MISSION_HELP)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_CURRENTLY_ACTIVE_ASSASSINATION_MISSION()
	RETURN ((GET_CLOUD_TIME_AS_INT()/ciASSASSINATION_MISSION_TIME_INTERVAL) % ciASSASSINATION_MISSION_COUNT)
ENDFUNC
				   
FUNC INT GET_ASSASSINATION_MISSION_ROOT_CONTENT_ID_HASH(INT iMission)
	SWITCH iMission
		CASE 0 RETURN 	1657186075		//	sjMYsr0bTUW4MA6K0bGIgQ	
		CASE 1 RETURN   -1789688675		//	zAwnaKKgc0C_E29tvKsp6w
		CASE 2 RETURN   -1433171161		//	rBVF7tsYgEas2AaAjxpklg
		CASE 3 RETURN   -1635197776		//	tQyPwyMhpUyGFCxHsZhu4g
		CASE 4 RETURN   -1509298929		//	q6Ul8EQGLk-Cr-FOxZJuMg
		CASE 5 RETURN   1367922159		//	MIRBogpLJEaMktWl1tHQCA
	ENDSWITCH
	RETURN 1657186075
ENDFUNC

FUNC BOOL IS_THIS_ROOT_CONTENT_ID_AN_ASSASSINATION_MISSION(INT iRootContentIDHash)
	INT iLoop
	REPEAT ciASSASSINATION_MISSION_COUNT iLoop
		IF iRootContentIDHash = GET_ASSASSINATION_MISSION_ROOT_CONTENT_ID_HASH(iLoop)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

//Loop all locates and set them up
PROC SET_UP_ASSASSINATION_LOCATIONS(ASSASSINATION_MISSION_DATA &sAssassinationMissions)
	INT iLoop
	REPEAT ciMAX_ASSASSINATION_LOCATIONS iLoop
		IF NOT DOES_BLIP_EXIST(sAssassinationMissions.location[iLoop].biBlip)
			sAssassinationMissions.location[iLoop].biBlip = ADD_BLIP_FOR_COORD(GET_ASSASSINATION_MISSION_LOCATION(iLoop))
			SET_BLIP_SPRITE(sAssassinationMissions.location[iLoop].biBlip, RADAR_TRACE_MARTIN_MADRAZZO)
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(sAssassinationMissions.location[iLoop].biBlip, HUD_COLOUR_YELLOW)
			SET_BLIP_NAME_FROM_TEXT_FILE(sAssassinationMissions.location[iLoop].biBlip, "BLIP_352")
			SET_BLIP_DISPLAY(sAssassinationMissions.location[iLoop].biBlip, DISPLAY_BOTH)
			SET_BLIP_AS_SHORT_RANGE(sAssassinationMissions.location[iLoop].biBlip, TRUE)
			SET_BLIP_SCALE(sAssassinationMissions.location[iLoop].biBlip, 1.0)
			IF g_sMPTunables.bENABLE_DLC_CONTENT_CONTROLLER
				SET_BLIP_AS_MISSION_CREATOR_BLIP(sAssassinationMissions.location[iLoop].biBlip, TRUE)
			ENDIF
//			IF NOT IS_BIT_SET(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_BLIP_ADDED_ONCE + iLoop )
//				SET_BLIP_FLASHES(sAssassinationMissions.location[iLoop].biBlip, TRUE)
//				SET_BLIP_FLASH_INTERVAL(sAssassinationMissions.location[iLoop].biBlip, BLIP_FLASHING_TIME)
//				SET_BLIP_FLASH_TIMER(sAssassinationMissions.location[iLoop].biBlip, 7000)
//				SET_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_BLIP_ADDED_ONCE+ iLoop)
//				PRINTLN("[MAINTAINASSASSINATION_MISSION] - SET_BIT(sLesterCallBlipData.iBitSet, ciLESTER_CALL_BLIP_ADDED_ONCE), iLoop = ", iLoop)
//			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//Launch the mission when we're ready
PROC SET_VARS_WHEN_LAUNCHING_ASSASSINATION_MISSION(STRING stPassed, INT iType, INT iMaxPlayers)
	
	SET_PLAYER_LEAVING_CORONA_VECTOR(GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID()))
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_CAN_BE_TARGETTED | NSPC_FREEZE_POSITION | NSPC_USE_PLAYER_FADE_OUT) 
	g_TransitionSessionNonResetVars.bSaveBeforeCoronaVehicle = TRUE

	iType = FMMC_TYPE_MISSION
	
	CLEAR_PAUSE_MENU_IS_USING_UGC()
	SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()	
	SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_V2_CORONA)
	
	IF (AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
	OR SHOULD_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW())
		SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()
		//If it's new VS then force it to be that mode
		g_TransitionSessionNonResetVars.sTransVars.bCleanedUpAndWait = FALSE
		PRINTLN("[TS] g_TransitionSessionNonResetVars.sTransVars.bCleanedUpAndWait = FALSE")
		SET_TRANSITION_SESSIONS_PAUSE_MENU_MAX_PLAYERS(iMaxPlayers)
		SET_TRANSITION_SESSIONS_PAUSE_MENU_QUICK_MATCH_TYPE(iType)
		SET_TRANSITION_SESSIONS_PAUSE_MENU_CONTENT_ID(stPassed)
		CLEAR_TRANSITION_SESSIONS_PAUSE_MENU_STARTING_CONTACT_MISSION()
	//If we are
	ELSE
		SET_TRANSITION_SESSIONS_QUICK_MATCH_TYPE(iType)
		SET_MY_TRANSITION_SESSION_CONTENT_ID(stPassed)
		SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(iMaxPlayers)	
		CLEAR_TRANSITION_SESSIONS_NEED_TO_SKYCAM_UP_NOT_WARP()							

		SET_TRANSITION_SESSIONS_STARTING_QUICK_MATCH()	
		SET_TRANSITION_SESSIONS_NEED_TO_WARP_TO_START_SKYCAM()
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION	
	ENDIF
ENDPROC

PROC SEND_INVITE_TO_PLAYER_USING_ARRAY_POS(INT iActiveMissionArrayPos, enumCharacterList eChar)
	Store_Invite_Details_For_Joblist_From_NPC(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iActiveMissionArrayPos].iType, 
												g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iActiveMissionArrayPos].iSubType, 
												g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iActiveMissionArrayPos].vStartPos,
												FMMC_ROCKSTAR_CREATOR_ID, eChar,
												g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iActiveMissionArrayPos].tlMissionName, "", 
												g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iActiveMissionArrayPos].tlName, DEFAULT, DEFAULT, TRUE)
ENDPROC

PROC LAUNCH_ANY_MISSION_USING_ARRAY_POS(INT iActiveMissionArrayPos)
	SET_VARS_WHEN_LAUNCHING_ASSASSINATION_MISSION(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iActiveMissionArrayPos].tlName,
													g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iActiveMissionArrayPos].iType,
													g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iActiveMissionArrayPos].iMaxPlayers)
ENDPROC

FUNC BOOL SEND_LITERAL_TEXT_MESSAGE_TO_PLAYER(STRING messagePartOne, STRING messagePartTwo, enumCharacterList eChar)
	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(eChar, "BRIEF_STRING2", TXTMSG_UNLOCKED,
                                                                    messagePartOne, -1, "", STRING_COMPONENT, TXTMSG_NOT_CRITICAL,
                                                                    TXTMSG_AUTO_UNLOCK_AFTER_READ, REPLY_IS_REQUIRED, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1, 
																	messagePartTwo)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DRAW_DASSASSINATION_LOCATE(VECTOR vDropoffCoord)
	INT iR, iG, iB, iA
	FLOAT iRadius = 1
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)   
	VECTOR vCoords = vDropoffCoord
	vCoords.z -= 0.7
	DRAW_MARKER(MARKER_CYLINDER,vCoords,<<0,0,0>>,<<0,0,0>>,<<iRadius*1.8, iRadius*1.8, 2.5 >>,iR,iG,iB,150)
ENDPROC

//Send the player a message
FUNC BOOL ASSASSINATIONSEND_TEXT_MESSAGE(STRING messagePartOne)
	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(	CHAR_MARTIN, messagePartOne, TXTMSG_UNLOCKED)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//Loop all the locates
PROC MAINTAIN_ASSASSINATION_LOCATIONS(ASSASSINATION_MISSION_DATA &sAssassinationMissions)

	sAssassinationMissions.bProcessEveryFrame = FALSE
	
	BOOL bAtLocate
	INT iLoop
	INT iClosestBlip
	FLOAT fDist
	FLOAT fDistSmallest = 10000000.0
	REPEAT ciMAX_ASSASSINATION_LOCATIONS iLoop
		fDist = VDIST2(GET_ASSASSINATION_MISSION_LOCATION(iLoop), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
		IF fDist < fDistSmallest
			fDistSmallest = fDist
			iClosestBlip = iLoop
		ENDIF
		//If we're 50 meters away then send the message about being close
		IF NOT IS_BIT_SET(sAssassinationMissions.iBitSet, ciPRINTED_FIFTY_METER_HELP)
		AND NOT IS_BIT_SET(g_sDLCContentContollerStruct.iCallBitset, ENUM_TO_INT(eDLCCONTENT_MADRAZO_DISPATCH))
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND fDist < 10000
				IF ASSASSINATIONSEND_TEXT_MESSAGE("FMASS_TXT")
					PRINT_HELP("FMASS_50MHLP")
					PRINTLN("[MAINTAINASSASSINATION_MISSION] - PRINT_HELP - GET_FIFTY_METER_HELP")
					SET_BIT(sAssassinationMissions.iBitSet, ciPRINTED_FIFTY_METER_HELP)
				ENDIF
			ENDIF
		ENDIF
		
		//Draw the marker
		VECTOR vDropoffCoord = GET_ASSASSINATION_MISSION_LOCATION(iLoop)
		IF  fDist < 10000
			sAssassinationMissions.bProcessEveryFrame = TRUE
			DRAW_DASSASSINATION_LOCATE( vDropoffCoord)
		ENDIF
		
		//If we're at the coord then tell the player to press D-Pad right
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropoffCoord, <<LOCATE_SIZE_MISSION_TRIGGER, LOCATE_SIZE_MISSION_TRIGGER, LOCATE_SIZE_MISSION_TRIGGER>>)
		AND NOT IS_BROWSER_OPEN()
		AND NOT g_bIEAppVisible
		AND NOT IS_PHONE_ONSCREEN()
			bAtLocate = TRUE
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RADIO_WHEEL_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RADIO_WHEEL_LR)
			//Tell the player to print that help
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMASS_DPAD")
				PRINTLN("GET_LOCATE_MESSAGE")
				PRINT_HELP("FMASS_DPAD", 30000)
				SET_BIT(sAssassinationMissions.iBitSet, ciPRINTED_MATC_DPADRIGH2)
			ENDIF
			
			//If we press Dpad right then launch the mission
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
				LAUNCH_ANY_MISSION_USING_ARRAY_POS(gAssassinationMissionData.iActiveMissionArrayPos)
				CLEAR_ASSASSINATION_MISSION_HELP(sAssassinationMissions)
				PRINTLN("[MAINTAINASSASSINATION_MISSION] - sAssassinationMissions.iStage = ciMAX_ASSASSINATION_STAGE_WAIT")
				sAssassinationMissions.iStage = ciMAX_ASSASSINATION_STAGE_WAIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	//We're not at a locat, then clear it
	IF !bAtLocate
		CLEAR_ASSASSINATION_MISSION_HELP(sAssassinationMissions)
	ENDIF
	
	//Maintain the closest blip
	IF sAssassinationMissions.iClosest != iClosestBlip
		sAssassinationMissions.iClosest = iClosestBlip
		PRINTLN("[MAINTAINASSASSINATION_MISSION] - sAssassinationMissions.iClosest")
//		REPEAT ciMAX_ASSASSINATION_LOCATIONS iLoop
//			IF DOES_BLIP_EXIST(sAssassinationMissions.location[iLoop].biBlip)
//				IF iLoop = sAssassinationMissions.iClosest
//					SET_BLIP_AS_SHORT_RANGE(sAssassinationMissions.location[iLoop].biBlip, FALSE)
//				ELSE
//					SET_BLIP_AS_SHORT_RANGE(sAssassinationMissions.location[iLoop].biBlip, TRUE)
//				ENDIF
//			ENDIF
//		ENDREPEAT
	ENDIF
	
ENDPROC

//Send invite message to the player
PROC ASSASSINATIONSEND_INVITE_MESSAGE()
	SEND_INVITE_TO_PLAYER_USING_ARRAY_POS(gAssassinationMissionData.iActiveMissionArrayPos, CHAR_MARTIN)
	PRINTLN("[MAINTAINASSASSINATION_MISSION] - Store_Invite_Details_For_Joblist_From_NPC")
	SET_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_SEND_INVITE_TO_MISSION)
ENDPROC

PROC MAINTAIN_ASSASSINATION_UPDATE_MESSAGES()
	//If we've got a mission array pos
	IF gAssassinationMissionData.iActiveMissionArrayPos != 0
	//And we've not messaged the player
	AND !IS_BIT_SET(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_SEND_INVITE_TO_MISSION)
		
		ASSASSINATIONSEND_INVITE_MESSAGE()
	
		IF IS_BIT_SET(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_MISSION_UPDATED)
			//Print the ticker that this has been updated
			PRINT_TICKER("FMASS_UPDT")
			PRINTLN("[MAINTAINASSASSINATION_MISSION] - PRINT_TICKER - GET_UPDATED_TICKER_MESSAGE")		
			CLEAR_BIT(gAssassinationMissionData.iBitSetNonReSet, ci_NON_RESET_MISSION_UPDATED)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ASSASSINATION_MOVE_TO_NEXT_MISSION(ASSASSINATION_MISSION_DATA &sAssassinationMissions, BOOL bSetUpDated = TRUE)
	IF GET_CLOUD_TIME_AS_INT() > sAssassinationMissions.iInitialPosixTime
		sAssassinationMissions.iInitialPosixTime = 0
		gAssassinationMissionData.iActiveMissionArrayPos = 0
		CLEAR_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_SEND_INVITE_TO_MISSION)
		IF bSetUpDated
			SET_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_MISSION_UPDATED)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ACTIVE_ON_ASSASSINATION_MISSION()
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NETWORK_IS_ACTIVITY_SESSION()
		IF!IS_BIT_SET(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_CHECK_FOR_END_OF_MISSION_MESSAGE)
		AND g_FMMC_STRUCT.iRootContentIDHash != 0
			IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
				INT iLoop
				REPEAT ciASSASSINATION_MISSION_COUNT iLoop
					IF g_FMMC_STRUCT.iRootContentIDHash = GET_ASSASSINATION_MISSION_ROOT_CONTENT_ID_HASH(iLoop)
						SET_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_DO_END_OF_MISSION_MESSAGE)
						PRINTLN("[MAINTAINASSASSINATION_MISSION] - SET_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_DO_END_OF_MISSION_MESSAGE)")
					ENDIF
				ENDREPEAT
			ENDIF
			PRINTLN("[MAINTAINASSASSINATION_MISSION] - SET_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_CHECK_FOR_END_OF_MISSION_MESSAGE)")
			SET_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_CHECK_FOR_END_OF_MISSION_MESSAGE)
		ENDIF
	ELSE
		IF IS_BIT_SET(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_CHECK_FOR_END_OF_MISSION_MESSAGE)
			CLEAR_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_CHECK_FOR_END_OF_MISSION_MESSAGE)
			PRINTLN("[MAINTAINASSASSINATION_MISSION] - CLEAR_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_CHECK_FOR_END_OF_MISSION_MESSAGE)")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ASSASSINATION_MISSION_END_OF_MISSION_HELP(ASSASSINATION_MISSION_DATA &sAssassinationMissions)
	//Check to see if we need to display the end of misison help:
	IF IS_BIT_SET(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_DO_END_OF_MISSION_MESSAGE)
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		IF !IS_SKYSWOOP_AT_GROUND()
			EXIT
		ENDIF
		IF NOT HAS_NET_TIMER_STARTED(sAssassinationMissions.stHelpDelay)
			START_NET_TIMER(sAssassinationMissions.stHelpDelay)
			PRINTLN("[MAINTAINASSASSINATION_MISSION] START_NET_TIMER(sAssassinationMissions.stHelpDelay)")
		ELIF HAS_NET_TIMER_EXPIRED(sAssassinationMissions.stHelpDelay, 5000)
			INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_IE_OWNED_VEHICLE_0)
			IF !IS_BIT_SET(iStat, ciASSASSINATION_MISSION_POST_MISSION_0)
				SET_BIT(iStat, ciASSASSINATION_MISSION_POST_MISSION_0)
				PRINTLN("[MAINTAINASSASSINATION_MISSION] SET_BIT(iStat, ciASSASSINATION_MISSION_POST_MISSION_0)")
			ELIF !IS_BIT_SET(iStat, ciASSASSINATION_MISSION_POST_MISSION_1)
				SET_BIT(iStat, ciASSASSINATION_MISSION_POST_MISSION_1)
				PRINTLN("[MAINTAINASSASSINATION_MISSION] SET_BIT(iStat, ciASSASSINATION_MISSION_POST_MISSION_1)")
			ELIF !IS_BIT_SET(iStat, ciASSASSINATION_MISSION_POST_MISSION_2)
				SET_BIT(iStat, ciASSASSINATION_MISSION_POST_MISSION_2)
				PRINTLN("[MAINTAINASSASSINATION_MISSION] SET_BIT(iStat, ciASSASSINATION_MISSION_POST_MISSION_2)")
			ELSE
				CLEAR_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_DO_END_OF_MISSION_MESSAGE)
			ENDIF
			IF IS_BIT_SET(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_DO_END_OF_MISSION_MESSAGE)
				PRINT_HELP("FMASS_PMH")
				PRINTLN("[MAINTAINASSASSINATION_MISSION] PRINT_HELP(\"FMASS_PMH\")")
				CLEAR_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_DO_END_OF_MISSION_MESSAGE)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_IE_OWNED_VEHICLE_0, iStat)
			ENDIF
		ENDIF
	ENDIF
ENDPROC



FUNC BOOL PASSED_ONE_ASSASSINATION_MISSION()
	INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_IE_OWNED_VEHICLE_0)
	RETURN IS_BIT_SET(iStat, ciASSASSINATION_MISSION_POST_MISSION_0)
		OR IS_BIT_SET(iStat, ciASSASSINATION_MISSION_POST_MISSION_1)
		OR IS_BIT_SET(iStat, ciASSASSINATION_MISSION_POST_MISSION_2)
ENDFUNC

FUNC BOOL IS_SAFE_TO_DO_ASSASSINATION_MISSION_TEXT_AND_HELP()

	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_HELP_MESSAGE_ON_SCREEN()
		PRINTLN("IS_SAFE_TO_DO_ASSASSINATION_MISSION_TEXT_AND_HELP - FALSE - Help is on screen")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("IS_SAFE_TO_DO_ASSASSINATION_MISSION_TEXT_AND_HELP - FALSE - Phone is on screen")
		RETURN FALSE
	ENDIF

	IF IS_ANY_GUNRUN_MISSION_CALL_BIT_SET()
	OR GB_IS_ANY_GUNRUNNING_BUSINESS_FLOW_CALL_BIT_SET()
	OR IS_ANY_SMUGGLER_MISSION_CALL_BIT_SET()
	OR IS_ANY_SMUGGLER_BUSINESS_FLOW_CALL_BIT_SET()
	OR IS_ANY_GANGOPS_MISSION_CALL_BIT_SET()
	OR IS_ANY_GANGOPS_BUSINESS_FLOW_CALL_BIT_SET()
	OR IS_ANY_BUSINESS_BATTLES_MISSION_CALL_BIT_SET()
	OR IS_ANY_BUSINESS_BATTLES_BUSINESS_FLOW_CALL_BIT_SET()
	OR IS_ANY_CASINO_MISSION_CALL_BIT_SET()
	OR IS_ANY_CASINO_FLOW_CALL_BIT_SET()
	#IF FEATURE_CASINO_HEIST
	OR IS_ANY_CASINO_HEIST_MISSION_CALL_BIT_SET()
	OR IS_ANY_CASINO_HEIST_FLOW_CALL_BIT_SET()
	#ENDIF
		PRINTLN("IS_SAFE_TO_DO_ASSASSINATION_MISSION_TEXT_AND_HELP - FALSE - A flow/mission phonecall is set up")
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

PROC MAINTAIN_ASSASSINATION_MISSION(ASSASSINATION_MISSION_DATA &sAssassinationMissions)

	//If we're in the dead are then stay there
	IF sAssassinationMissions.iStage = ciMAX_ASSASSINATION_STAGE_DEAD
		EXIT
	ENDIF
	
	MAINTAIN_ACTIVE_ON_ASSASSINATION_MISSION()
	
	//get out and reset
	IF !NETWORK_IS_GAME_IN_PROGRESS()
	OR NETWORK_IS_ACTIVITY_SESSION()
	OR IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
	OR NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
	OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
	OR IS_PLAYER_IN_CUTSCENE(PLAYER_ID())
	OR (GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID()) AND GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID()))
	OR GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
	OR IS_PLAYER_IN_CORONA() 
	OR IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(),FALSE)
	OR IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
	OR IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_RACE_TO_POINT)
	OR IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
	OR IS_PLAYER_IN_CASINO(PLAYER_ID())
	OR IS_LOCAL_PLAYER_AN_ANIMAL()
	OR IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
	OR IS_LOCAL_PLAYER_USING_RCBANDITO()
	OR IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
		RESET_ASSASSINATION_MISSION(sAssassinationMissions)
		EXIT
	ENDIF
	
	//Check to see if we need to reset the next time window
	IF sAssassinationMissions.iInitialPosixTime = 0
	AND GET_FM_UGC_INITIAL_HAS_FINISHED()
		sAssassinationMissions.iInitialPosixTime = CV2_GET_POSIX_ROLL_OVER_TIME(ciASSASSINATION_MISSION_TIME_INTERVAL)
		PRINTLN("[MAINTAINASSASSINATION_MISSION] sAssassinationMissions.iInitialPosixTime = ", sAssassinationMissions.iInitialPosixTime)
		gAssassinationMissionData.iActiveMissionArrayPos = GET_CURRENTLY_ACTIVE_ASSASSINATION_MISSION()
		gAssassinationMissionData.iActiveMissionArrayPos = GET_ASSASSINATION_MISSION_ROOT_CONTENT_ID_HASH(gAssassinationMissionData.iActiveMissionArrayPos)
		gAssassinationMissionData.iActiveMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(gAssassinationMissionData.iActiveMissionArrayPos)
		PRINTLN("[MAINTAINASSASSINATION_MISSION] gAssassinationMissionData.iActiveMissionArrayPos = ", gAssassinationMissionData.iActiveMissionArrayPos)
	ENDIF
	
	IF gAssassinationMissionData.iActiveMissionArrayPos = -1
		EXIT
	ENDIF
	
	//Maintain printing help after the mission
	MAINTAIN_ASSASSINATION_MISSION_END_OF_MISSION_HELP(sAssassinationMissions)
	
	SWITCH sAssassinationMissions.iStage
	
		//Do the initial set up
		CASE ciMAX_ASSASSINATION_STAGE_SET_UP			
			//If we've not finished tutorial then get out
			IF !HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_ID())
				EXIT
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunAsassinationMissionLocates")
			AND NOT IS_DEBUG_KEY_JUST_PRESSED(KEY_P, KEYBOARD_MODIFIER_NONE, "sc_RunAsassinationMissionLocates")
				EXIT
			ENDIF
			#ENDIF
			
			//If we're not at the ground then get out
			IF !IS_SKYSWOOP_AT_GROUND()
				EXIT
			ENDIF
			
			//Start timer
//			START_NET_TIMER(sAssassinationMissions.stInitialDelay)
			
//			//Move stage
//			IF !IS_BIT_SET(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_MESSAGE_SENT_ONCE)
//			AND !PASSED_ONE_ASSASSINATION_MISSION()
//				PRINTLN("[MAINTAINASSASSINATION_MISSION] - sAssassinationMissions.iStage = ciMAX_ASSASSINATION_STAGE_MESSAGE")
//				sAssassinationMissions.iStage = ciMAX_ASSASSINATION_STAGE_MESSAGE
//			ELSE
				PRINTLN("[MAINTAINASSASSINATION_MISSION] - sAssassinationMissions.iStage = ciMAX_ASSASSINATION_STAGE_ADD_LOCATES")
				sAssassinationMissions.iStage = ciMAX_ASSASSINATION_STAGE_ADD_LOCATES 
//			ENDIF
		BREAK
		
		//Send the message to the player
		CASE ciMAX_ASSASSINATION_STAGE_MESSAGE
			//Maintain the reset window
			MAINTAIN_ASSASSINATION_MOVE_TO_NEXT_MISSION(sAssassinationMissions, FALSE)
			
			IF HAS_NET_TIMER_EXPIRED(sAssassinationMissions.stInitialDelay, FLOW_TIMINGS__MARTIN_DISPATCH_MISSIONS)	// 5 mins
			AND NOT g_bBrowserVisible
			AND NOT g_bIEAppVisible
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID())
				IF IS_SAFE_TO_DO_ASSASSINATION_MISSION_TEXT_AND_HELP()
			
					//Send the message to the player
					//Message the player about the job
					IF ASSASSINATIONSEND_TEXT_MESSAGE("FMASS_TXT")
						//Print the help to say the missions are here
						PRINT_HELP("FMASS_INTH")
						PRINTLN("[MAINTAINASSASSINATION_MISSION] - PRINT_HELP_WITH_PLAYER_NAME - GET_INITIAL_HELP_MESSAGE")
						//Set that we've done it
						SET_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_MESSAGE_SENT_ONCE)
						//Move stage
						PRINTLN("[MAINTAINASSASSINATION_MISSION] - sAssassinationMissions.iStage = ciMAX_ASSASSINATION_STAGE_ADD_LOCATES")
						sAssassinationMissions.iStage = ciMAX_ASSASSINATION_STAGE_ADD_LOCATES 
						RESET_NET_TIMER(sAssassinationMissions.stInitialDelay)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciMAX_ASSASSINATION_STAGE_ADD_LOCATES
			IF gAssassinationMissionData.iActiveMissionArrayPos != 0
				//Set up the locations:
				SET_UP_ASSASSINATION_LOCATIONS(sAssassinationMissions)
				//Move stage
				PRINTLN("[MAINTAINASSASSINATION_MISSION] - sAssassinationMissions.iStage = ciMAX_ASSASSINATION_STAGE_GET_TO_LOCATE")
				sAssassinationMissions.iStage = ciMAX_ASSASSINATION_STAGE_GET_TO_LOCATE 
			ENDIF
		BREAK
		
		//Wait for the player to get to the locate
		CASE ciMAX_ASSASSINATION_STAGE_GET_TO_LOCATE
//			IF !IS_BIT_SET(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_SEND_INVITE_TO_MISSION_INITIAL)
//				IF HAS_NET_TIMER_EXPIRED(sAssassinationMissions.stInitialDelay, 60000)
//				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//					//Print the help to say that it's been updated
//					PRINT_HELP("FMASS_UPDH")
//					PRINTLN("[MAINTAINASSASSINATION_MISSION] - PRINT_HELP - GET_UPDATED_HELP_MESSAGE")
//					SET_BIT(gAssassinationMissionData.iBitSetNonReset, ci_NON_RESET_SEND_INVITE_TO_MISSION_INITIAL)
//					SET_BIT(sAssassinationMissions.iBitSet, ciDO_MESSAGE_COOL_DOWN)
//					RESET_NET_TIMER(sAssassinationMissions.stInitialDelay)
//					ASSASSINATIONSEND_INVITE_MESSAGE()
//				ENDIF
//			//Have a two minute cool down so not to fire too manay messages
//			ELIF IS_BIT_SET(sAssassinationMissions.iBitSet, ciDO_MESSAGE_COOL_DOWN)
//				IF HAS_NET_TIMER_EXPIRED(sAssassinationMissions.stInitialDelay, 1200000)
//					CLEAR_BIT(sAssassinationMissions.iBitSet, ciDO_MESSAGE_COOL_DOWN)
//					CLEAR_BIT(gAssassinationMissionData.iBitSetNonReSet, ci_NON_RESET_MISSION_UPDATED)
//					PRINTLN("[MAINTAINASSASSINATION_MISSION] - CLEAR_BIT(sAssassinationMissions.iBitSet, ciDO_MESSAGE_COOL_DOWN)")
//				ENDIF
//			//Message the player when it updates
//			ELSE
//				MAINTAIN_ASSASSINATION_UPDATE_MESSAGES()
//			ENDIF
			
			//Maintain the reset window
			MAINTAIN_ASSASSINATION_MOVE_TO_NEXT_MISSION(sAssassinationMissions)
			 
			//Maintain the indavidual locates
			MAINTAIN_ASSASSINATION_LOCATIONS(sAssassinationMissions)
		BREAK
		
		CASE ciMAX_ASSASSINATION_STAGE_WAIT
			//Dead
		BREAK
	ENDSWITCH
ENDPROC

