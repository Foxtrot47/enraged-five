


USING "net_transition_sessions.sch"



CONST_INT CORONA_NUMBER_OF_SEGMENTS		72

VECTOR vCoronaCircumferencePoints[CORONA_NUMBER_OF_SEGMENTS]
VECTOR vCoronaCircumferenceInnerPoints[CORONA_NUMBER_OF_SEGMENTS]

FLOAT fCircleAngle

PROC CALCULATE_CORONA_CIRCUMFERENCE_POINTS(VECTOR vCentre, FLOAT fRadius = 3.35)
	
	FLOAT fSegmentSize = (360.0/TO_FLOAT(CORONA_NUMBER_OF_SEGMENTS))
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	INT iSegment
	
	//PRINTLN("Segment Array size: ", CORONA_NUMBER_OF_SEGMENTS)
	//PRINTLN("fSegmentSize", fSegmentSize)
	
	FOR iSegment = 0 TO CORONA_NUMBER_OF_SEGMENTS - 1

		fCircleAngle = fSegmentSize * iSegment

		vCoronaCircumferencePoints[iSegment].x = vCentre.x + (fRadius * cos(fCircleAngle))
		vCoronaCircumferencePoints[iSegment].y = vCentre.y + (fRadius * sin(fCircleAngle))
		vCoronaCircumferencePoints[iSegment].z = vPlayerCoords.z
		
		vCoronaCircumferenceInnerPoints[iSegment].x = vCentre.x + ((fRadius - 0.5) * cos(fCircleAngle))
		vCoronaCircumferenceInnerPoints[iSegment].y = vCentre.y + ((fRadius - 0.5) * sin(fCircleAngle))
		vCoronaCircumferenceInnerPoints[iSegment].z = vPlayerCoords.z
		
		//PRINTLN("Segment: ", iSegment, " pos ", vCoronaCircumferencePoints[iSegment].x, ",", vCoronaCircumferencePoints[iSegment].y, "Angle ", fCircleAngle)
						
	ENDFOR
	
ENDPROC

#IF IS_DEBUG_BUILD

PROC DRAW_CORONA_SEGMENTS_POINTS()

	INT iSegment
	
	FOR iSegment = 0 TO 71

		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		DRAW_DEBUG_SPHERE(vCoronaCircumferencePoints[iSegment], 0.1, 255, 0, 0, 255)
		DRAW_DEBUG_SPHERE(vCoronaCircumferenceInnerPoints[iSegment], 0.1, 0, 255, 0, 255)
		
		TEXT_LABEL strDebugNum = ""
		strDebugNum += iSegment
		DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoronaCircumferencePoints[iSegment], strDebugNum, 0.0, 0, 0, 0)
		
	ENDFOR

ENDPROC

#ENDIF

FLOAT fClosestVectorDist = 9999.99

FUNC INT GET_CORONA_ENTRY_SEGMENT()
	
	INT iClosestSegment = CORONA_NUMBER_OF_SEGMENTS - 1
	INT iSegment
	
	FOR iSegment = 0 TO CORONA_NUMBER_OF_SEGMENTS - 1 
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCoronaCircumferencePoints[iSegment]) < fClosestVectorDist
			fClosestVectorDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCoronaCircumferencePoints[iSegment])
			iClosestSegment = iSegment
			
			PRINTLN("Dist to:", iSegment, "is ", fClosestVectorDist)
			PRINTLN("Current Closest Segment: ", iClosestSegment, " at ", vCoronaCircumferencePoints[iClosestSegment])
			
		ENDIF		
	ENDFOR

	PRINTLN("CLOSEST segment: ", iClosestSegment, " at ", vCoronaCircumferencePoints[iClosestSegment])
	
	//Reset
	fClosestVectorDist = 9999.99
		
	RETURN iClosestSegment

ENDFUNC

//get an off set from the center point to find a vehicle
FUNC VECTOR GET_OFF_SET_VECTOR(INT iRepeat)
	SWITCH iRepeat
		CASE 0 	RETURN <<1.0, 0.0, 0.0>>
		CASE 1 	RETURN <<-1.0, 0.0, 0.0>>
		CASE 2 	RETURN <<0.0, 1.0, 0.0>>
		CASE 3 	RETURN <<0.0, -1.0, 0.0>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC


//Hide all the vehicles in the way
PROC MAINTAIN_HIDE_VEHICLES_IN_CORONA(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)
	INT iLoop, iRepeat
	CONST_INT ciMAX_VEHCILES_TO_CHECK_IN_AREA 4
	VECTOR vTemp
	BOOL bNewVehicle
	VEHICLE_INDEX viTemp[ciMAX_VEHCILES_TO_CHECK_IN_AREA]
	PED_INDEX piTemp[ciMAX_VEHCILES_TO_CHECK_IN_AREA]
	//Loop the vehciles 
	REPEAT ciMAX_VEHCILES_TO_CHECK_IN_AREA iRepeat
		//Get a search vector
		vTemp = sLaunchMissionDetails.vStartPoint + GET_OFF_SET_VECTOR(iRepeat)
		//grab a vehcile
		viTemp[iRepeat] = GET_CLOSEST_VEHICLE(vTemp, 5.0, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING)
		//reset the flag
		bNewVehicle = TRUE
		//check to see if it's new
		FOR iLoop = 0 TO (iRepeat -1)
			IF viTemp[iLoop] = viTemp[iRepeat] 
				bNewVehicle = FALSE
			ENDIF
		ENDFOR
		//If it's new
		IF bNewVehicle
			//and it's alive
			IF IS_VEHICLE_DRIVEABLE(viTemp[iRepeat])
				//Hide the Driver
				piTemp[iRepeat] = GET_PED_IN_VEHICLE_SEAT(viTemp[iRepeat])
				IF DOES_ENTITY_EXIST(piTemp[iRepeat])
				
					IF NETWORK_GET_ENTITY_IS_NETWORKED(piTemp[iRepeat])
						SET_ENTITY_LOCALLY_INVISIBLE(piTemp[iRepeat])
					ENDIF
				ENDIF
				
				//Hide the Vehicle
				IF NETWORK_GET_ENTITY_IS_NETWORKED(viTemp[iRepeat])
					SET_ENTITY_LOCALLY_INVISIBLE(viTemp[iRepeat])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//Stages for the walk in
CONST_INT ciCORONA_WALK_IN_INT			0
CONST_INT ciCORONA_WALK_IN_WALK			1
CONST_INT ciCORONA_WALK_IN_WAIT			2
CONST_INT ciCORONA_WALK_IN_FLOW_CALL	3
CONST_INT ciCORONA_WALK_IN_DONE			4

//stages for the walk in call
CONST_INT ciCORONA_WALK_IN_CALL_INT			0
CONST_INT ciCORONA_WALK_IN_CALL_REQUEST		1
CONST_INT ciCORONA_WALK_IN_CALL_WAIT_PLAY	2
CONST_INT ciCORONA_WALK_IN_CALL_WAIT_END	3
CONST_INT ciCORONA_WALK_IN_CALL_DONE		4

//Maintain the walk in call 
FUNC BOOL MAINTAIN_FLOW_WALK_IN_PHONE_CALL(structPedsForConversation &paramConversation, INT &iFlowCallStage, INT iRootContentIdHash)
	INT phonecallModifiers
	
	SWITCH iFlowCallStage
		//Set up for the call
		CASE ciCORONA_WALK_IN_CALL_INT
			ADD_CONTACT_TO_PHONEBOOK (CHAR_LAMAR, MULTIPLAYER_BOOK, FALSE)
			ADD_PED_FOR_DIALOGUE(paramConversation, 2, NULL, "Lamar")
			iFlowCallStage = ciCORONA_WALK_IN_CALL_REQUEST
			CPRINTLN(DEBUG_MP_UNLOCKS, "[LOWFLOW][INT] [MAINTAIN_FLOW_WALK_IN_PHONE_CALL] iFlowCallStage = ciCORONA_WALK_IN_CALL_REQUEST")
		BREAK
		//wait for the call to come through
		CASE ciCORONA_WALK_IN_CALL_REQUEST
			SET_USE_DLC_DIALOGUE(TRUE)
			SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_COMMS_FORCE_ONSCREEN)
			SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_NO_HANGUP_FOR_PHONECALL)
			SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER)
			SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_NO_HANGUP_ON_PAUSEMENU)
			SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_PRIORITY_PHONECALL)
			IF Request_MP_Comms_Message(paramConversation, CHAR_LAMAR, "LOWCAAU", GET_FLOW_STRAND_MISSION_UNLOCK_CALL_FROM_ROOT_CONTENT_ID(iRootContentIdHash), phonecallModifiers)
				iFlowCallStage = ciCORONA_WALK_IN_CALL_WAIT_PLAY
				CPRINTLN(DEBUG_MP_UNLOCKS, "[LOWFLOW][INT] [MAINTAIN_FLOW_WALK_IN_PHONE_CALL] iFlowCallStage = ciCORONA_WALK_IN_CALL_WAIT_PLAY")
			ENDIF
		BREAK
		//wait for it to play
		CASE ciCORONA_WALK_IN_CALL_WAIT_PLAY
			IF IS_CELLPHONE_CONVERSATION_PLAYING() 
				SET_USE_DLC_DIALOGUE(FALSE)
				iFlowCallStage = ciCORONA_WALK_IN_CALL_WAIT_END
				CPRINTLN(DEBUG_MP_UNLOCKS, "[LOWFLOW][INT] [MAINTAIN_FLOW_WALK_IN_PHONE_CALL] iFlowCallStage = ciCORONA_WALK_IN_CALL_WAIT_END")
			ENDIF
		BREAK
		//wait for it to end
		CASE ciCORONA_WALK_IN_CALL_WAIT_END
			IF HAS_CELLPHONE_CALL_FINISHED()
				iFlowCallStage = ciCORONA_WALK_IN_CALL_DONE
				CPRINTLN(DEBUG_MP_UNLOCKS, "[LOWFLOW][INT] [MAINTAIN_FLOW_WALK_IN_PHONE_CALL] iFlowCallStage = ciCORONA_WALK_IN_CALL_DONE")
				RETURN TRUE
			ENDIF
		BREAK
		//We're done here, return TRUE
		CASE ciCORONA_WALK_IN_CALL_DONE
			RETURN TRUE

	ENDSWITCH 
	RETURN FALSE
ENDFUNC

//Deal with the walk in cam
FUNC BOOL MAINTAIN_CORONA_WALK_IN_CAM(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails, CAMERA_INDEX &initialCam, structPedsForConversation &paramConversation, INT &iFlowCallStage)
	RETURN TRUE

	VECTOR vRepositionCoords	
	INT iCameraSegment
	INT iEntrySegment
	FLOAT fEntryMoveBlend = PEDMOVE_WALK
	VECTOR vWalkInCameraPointOffset = <<-0.45, 1.25, 0.25>>
	FLOAT fHeading
	
	WEAPON_TYPE wtWalkInWeapon
				
	#IF IS_DEBUG_BUILD
//		DRAW_CORONA_SEGMENTS_POINTS()
//		iEntrySegment = GET_CORONA_ENTRY_SEGMENT() 			
//		iCameraSegment = ((iEntrySegment + (CORONA_NUMBER_OF_SEGMENTS) / 2)  - 4) % CORONA_NUMBER_OF_SEGMENTS//(iEntrySegment + CORONA_NUMBER_OF_SEGMENTS / 2) % CORONA_NUMBER_OF_SEGMENTS
//		DRAW_DEBUG_SPHERE(vCoronaCircumferencePoints[iEntrySegment], 0.2, 0, 255, 255)
//		DRAW_DEBUG_SPHERE(vCoronaCircumferencePoints[iCameraSegment], 0.2, 0, 0, 255)					
	#ENDIF

	SWITCH sLaunchMissionDetails.iCoronaWalkInCameraStatus
	
		CASE ciCORONA_WALK_IN_INT	//Init
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			iFlowCallStage = ciCORONA_WALK_IN_CALL_INT
			
			//If the player is on the activity tutorial then we don't need to do this.
			IF IS_LOCAL_PLAYER_IN_TUTORIAL_SESSION_FOR_INTRO()
				RETURN TRUE
			ENDIF
			//If the intro corona tutorial has not been done 
			IF IS_CORONA_WALK_IN_DISABLED_FOR_TUTORIAL()
				RETURN TRUE
			ENDIF
			//get out if it's a mini game.
			IF IS_THIS_A_MINI_GAME(sLaunchMissionDetails.iMissionType)
				RETURN TRUE
			ENDIF
			
			IF FM_FLOW_DISABLE_DPAD_RIGHT_AND_WALK_IN_FOR_CORONA()
				RETURN TRUE
			ENDIF
			
			
			//Or we are in a corona after a restart/random
			IF IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
				RETURN TRUE
			ENDIF
			
//			//If we should be doing a call for this mission the move to the do call stage
//			IF GET_LOADED_FLOW_STRAND_MISSION_HAS_UNLOCK_CALL_IN_CORONA(sLaunchMissionDetails.sDefaultCoronaOptions.iRootContentIdHash)
//				IF NOT (Is_Player_On_Voice_Chat_Phonecall())
//					sLaunchMissionDetails.iCoronaWalkInCameraStatus = ciCORONA_WALK_IN_FLOW_CALL
//					CPRINTLN(DEBUG_MP_UNLOCKS, "[LOWFLOW][INT] [GET_LOADED_FLOW_STRAND_MISSION_HAS_UNLOCK_CALL_IN_CORONA] GET_LOADED_FLOW_STRAND_MISSION_HAS_UNLOCK_CALL_IN_CORONA(", sLaunchMissionDetails.sDefaultCoronaOptions.iRootContentIdHash, ")")
//					RETURN FALSE
//				ENDIF
//			ENDIF
			
			//If it's a strand mission then no!
			IF sLaunchMissionDetails.iMissionType = FMMC_TYPE_MISSION
			AND sLaunchMissionDetails.iCoronaMissionSubType = FMMC_MISSION_TYPE_FLOW_MISSION
				RETURN TRUE
			ENDIF
			
			REQUEST_ANIM_DICT("move_m@GEneric")
			REQUEST_ANIM_DICT("AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@ENTER")
			REQUEST_MODEL(Prop_AMB_Phone)
			
			//Grab camera relative to the player on the edge of the corona
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				fEntryMoveBlend = GET_ENTITY_SPEED(PLAYER_PED_ID()) //GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID())
			ENDIF
						
			PRINTLN("entry movement speed:", fEntryMoveBlend)
			
			CALCULATE_CORONA_CIRCUMFERENCE_POINTS(sLaunchMissionDetails.vStartPoint)
			iEntrySegment = GET_CORONA_ENTRY_SEGMENT() 			
			iCameraSegment = ((iEntrySegment + (CORONA_NUMBER_OF_SEGMENTS) / 2)  - 4) % CORONA_NUMBER_OF_SEGMENTS
			PRINTLN("entry Segment: ", iEntrySegment, " CAMERA SEGMENT : ", iCameraSegment)
		
			vRepositionCoords = vCoronaCircumferenceInnerPoints[iEntrySegment]
			GET_GROUND_Z_FOR_3D_COORD(vRepositionCoords, vRepositionCoords.z)
			
			fHeading = GET_HEADING_BETWEEN_VECTORS_2D(vCoronaCircumferencePoints[iEntrySegment], vCoronaCircumferenceInnerPoints[iEntrySegment])
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vRepositionCoords)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
			
			IF fEntryMoveBlend < 2.5
				fEntryMoveBlend = PEDMOVE_WALK
			ELSE
				fEntryMoveBlend = PEDMOVE_RUN
			ENDIF
			
			
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWalkInWeapon)
			
						
			IF HAS_ANIM_DICT_LOADED("move_m@GEneric")	
				IF fEntryMoveBlend = PEDMOVE_WALK
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "move_m@GEneric", "wstop_l_0", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
				ELSE
//					TASK_PLAY_ANIM(PLAYER_PED_ID(), "move_m@GEneric", "rstop_l", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1)
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "move_m@GEneric", "rstop_quick_l", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
					vWalkInCameraPointOffset = <<-0.85, 1.25, 0.4>>
				ENDIF
			ENDIF
						
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			
						
			DESTROY_CAM(g_sTransitionSessionData.ciCam)
			DESTROY_CAM(initialCam)
			
			g_sTransitionSessionData.ciCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						
			initialCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
			
			IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
				SET_CAM_COORD(initialCam, vCoronaCircumferencePoints[iCameraSegment] + <<0.0, 0.0, 0.1>>)
				POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<0.0, 0.0, 0.125>>)
			ELSE
				SET_CAM_COORD(initialCam, vCoronaCircumferencePoints[iCameraSegment])
				POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<0.0, 0.0, 0.125>>)
			ENDIF
					
			//STOP_CAM_POINTING(initialCam)
			SET_CAM_FOV(initialCam, 20.0)
			SET_CAM_ACTIVE(initialCam, TRUE)
			
			IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
				SET_CAM_COORD(g_sTransitionSessionData.ciCam, vCoronaCircumferencePoints[iCameraSegment] + <<0.0, 0.0, 0.1>>)
				POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<0.0, 0.0, 0.125>>)
			ELSE
				SET_CAM_COORD(g_sTransitionSessionData.ciCam, vCoronaCircumferencePoints[iCameraSegment])
				POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<0.0, 0.0, 0.125>>)
			ENDIF
			
			
			POINT_CAM_AT_COORD(g_sTransitionSessionData.ciCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vWalkInCameraPointOffset))//vCoronaCircumferencePoints[iEntrySegment] + vWalkInCameraPointOffset)
			//STOP_CAM_POINTING(g_sTransitionSessionData.ciCam)
			SET_CAM_FOV(g_sTransitionSessionData.ciCam, 20.0)
			
			SET_CAM_ACTIVE_WITH_INTERP(g_sTransitionSessionData.ciCam, initialCam, 1500, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
			SHAKE_CAM(g_sTransitionSessionData.ciCam, "HAND_SHAKE", 0.3)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			
			
			//TASK_LOOK_AT_COORD(PLAYER_PED_ID(), vCoronaCircumferenceInnerPoints[(iCameraSegment + 4)%CORONA_NUMBER_OF_SEGMENTS], 2000, SLF_WHILE_NOT_IN_FOV)
						
			HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
						
			sLaunchMissionDetails.iTimerWalkInCamera = GET_GAME_TIMER()
			sLaunchMissionDetails.iCoronaWalkInCameraStatus = ciCORONA_WALK_IN_WALK
		BREAK
	
		CASE ciCORONA_WALK_IN_WALK	//Update
			
			HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
		
			//Retask player if they somehow dont start the sequence. 1613549
			IF (GET_GAME_TIMER() - sLaunchMissionDetails.iTimerWalkInCamera > 1000 AND fEntryMoveBlend = PEDMOVE_WALK)
			OR (GET_GAME_TIMER() - sLaunchMissionDetails.iTimerWalkInCamera > 1500 AND fEntryMoveBlend = PEDMOVE_RUN)
				
				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWalkInWeapon)
				
				IF wtWalkInWeapon = WEAPONTYPE_UNARMED
				AND HAS_ANIM_DICT_LOADED("AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@ENTER")
				AND HAS_MODEL_LOADED(Prop_AMB_Phone)
				//	TASK_START_SCENARIO_IN_PLACE(PLAYER_PED_ID(),"WORLD_HUMAN_STAND_MOBILE",0,TRUE) 
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@ENTER", "enter", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
					sLaunchMissionDetails.oiWalkInPhone = CREATE_OBJECT(Prop_AMB_Phone, GET_ENTITY_COORDS(PLAYER_PED_ID()), FALSE)
					ATTACH_ENTITY_TO_ENTITY(sLaunchMissionDetails.oiWalkInPhone, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				
				ENDIF
				sLaunchMissionDetails.iCoronaWalkInCameraStatus = ciCORONA_WALK_IN_WAIT
				
			ENDIF
		BREAK
		
		CASE ciCORONA_WALK_IN_FLOW_CALL
			//ENABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
			
			// url:bugstar:2531457 - Would we be able to enable camera control while on the forced phone calls at the new coronas?
			ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
			ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
		
			IF MAINTAIN_FLOW_WALK_IN_PHONE_CALL(paramConversation, iFlowCallStage, sLaunchMissionDetails.sDefaultCoronaOptions.iRootContentIdHash)
				sLaunchMissionDetails.iCoronaWalkInCameraStatus = ciCORONA_WALK_IN_DONE
			ENDIF
		BREAK
		
		CASE ciCORONA_WALK_IN_WAIT
		
			HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
		
			IF GET_GAME_TIMER() - sLaunchMissionDetails.iTimerWalkInCamera > 1500
				HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
//				ANIMPOSTFX_PLAY("SuccessNeutral", 0, FALSE)
//				PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")

				REMOVE_ANIM_DICT("AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@ENTER")
				SET_MODEL_AS_NO_LONGER_NEEDED(Prop_AMB_Phone)

				DESTROY_CAM(initialCam)				
				sLaunchMissionDetails.iCoronaWalkInCameraStatus = ciCORONA_WALK_IN_DONE
			ENDIF
			
		BREAK
		
		CASE ciCORONA_WALK_IN_DONE
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC
