//-- Freemode Trigger Tutorial warp / cutscene
//-- Dave W

/*
	Button prompts while in corona displayed via SET_UP_MENU_HELP in ffmc_launcher_menu.sch
	
	After race spawn position determined by
	
	x:\gta5\script\dev_network\multiplayer\include\public\freemode\fm_post_mission_cleanup.sch
	
	Print is:
		NET_PRINT("[WJK] [dsw] - FMMC_Launcher - IS_LOCAL_PLAYER_RUNNING_DM_TUTORIAL - SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE = FALSE, using tutorials near race corona loc ")
	
	If joining from skycam:
	
	x:\gta5\script\dev_network\multiplayer\scripts\transition\transition_common.sch
	
	NET_PRINT("[dsw] TRANSITION_SPAWN_PLAYER_FM - spawning at DM tutorial position ") NET_NL()	
	
*/

USING "rage_builtins.sch"
USING "globals.sch"
USING "net_scoring_common.sch"
USING "shop_public.sch"
USING "freemode_header.sch"
USING "Fmmc_launcher_menu.sch"
USING "fmmc_request_to_play_mission.sch"
USING "vehicle_public.sch"
USING "net_missions_at_coords_public_checks.sch"
//USING "net_freemode_cut.sch"

CONST_INT MAX_PLAYERS_IN_TUTORIAL_SESSION		24

INT MAX_DM_TUT_TIMEOUT = 10000 // 60000 Also set when initially storing timer 
TWEAK_INT MIN_PLAYERS_FOR_MISSION				4


CONST_INT biFmT_RunningCutscene		0
CONST_INT biFmT_ForceRenderCorona	1
CONST_INT biFmT_DoFloatingHelp		2
CONST_INT biFmt_BlockedMission		3
CONST_INT biFmt_LaunchTimeout		4
CONST_INT biFmt_WaitingForLaunch	5
CONST_INT biFmt_WarpedIntoDmCorona	6
CONST_INT biFmt_DmLaunchTimeout		7
CONST_INT biFmt_WaitingForDmLaunch	8
CONST_INT biFmt_FinishedRace		9
CONST_INT biFmt_ArrivedInVeh		10
CONST_INT biFMt_SetupInitalCam		11
CONST_INT biFMt_DoneBettingHelp		12
CONST_INT biFMt_DoWaitingScreenHelp	13
CONST_INT biFMt_DoLamarMocap		14
CONST_INT biFMt_DoneCoronaIntroHelp	15
CONST_INT biFMt_NewTutSession		16
CONST_INT biFMt_HideHudAndRadar		17
CONST_INT biFMt_FindSessionForDm	18
CONST_INT biFmt_InTutSessionForDM	19
CONST_INT biFmt_CheckForPlayControl	20
CONST_INT biFmt_DisablePauseMenu	21
CONST_INT biFmt_FrozePlyForSkyswoop 22
CONST_INT biFmt_PutPlayerOnSolo		23
CONST_INT biDMt_WalkIntoCorona		24
CONST_INT biFMt_ReserveDmSlot		25
CONST_INT biFMt_DmFinalCheckFOrPlay	26
CONST_INT biFMt_UnreservedPlayer	27
CONST_INT biFMt_DODmWaitCountdown	28
CONST_INT biFMt_LoseWanted			29
CONST_INT biFMt_RemoveFromTutSess	30
CONST_INT biFMt_SetRemoveFromTut	31

CONST_INT biFMt2_AmbientRacerCorona	0
CONST_INT biFMt2_StraightToDm		1
CONST_INT biFMt2_ToldToLoseCops		2
CONST_INT biFMt2_ToldToEnterCorona	3
CONST_INT biFmt2_AfterAmmuCutSkip	4
CONST_INT biFmt2_AfterRaceCutSkip	5
CONST_INT biFmt2_DoneAfterRaceSetup	6
CONST_INT biFmt2_WalkedAwayFromLam	7
CONST_INT biFmt2_CloudDataLoaded	8
CONST_INT biFmt2_SetupForMission	9
CONST_INT biFmt2_StartMission		10
CONST_INT biFMt2_FadeInOnMission	11
CONST_INT biFmt2_LaunchedMission	12
CONST_INT biFmt2_ReservedOnMission	13
CONST_INT biFmt2_CreateAmbRacers	14
CONST_INT biFmt2_SetMissionVehicle	15
CONST_INT biFmt2_DoAfterRaceCut		16
CONST_INT biFmt2_TempUnlockMiss		17
CONST_INT biFmt2_UnlockedRace		18
CONST_INT biFmt2_StartPutOntoMiss	19
CONST_INT biFmt2_SetMissionType		20
CONST_INT biFmt2_GotColourCombo		21
CONST_INT biFmt2_RequestedLamar		22
CONST_INT biFmt2_AttackPlayer		23
CONST_INT biFmt2_TookTransControl	24
CONST_INT biFmt2_SetupForSwoop		25
CONST_INT biFmt2_UseSoloMissionDiag	26
CONST_INT biFMt2_TryToUnlockRace	27
CONST_INT biFmt2_RenderedCams		28
CONST_INT biFmt2_OutsideRaceArea	29
CONST_INT biFmt2_DisableHudForSwoop	30
CONST_INT biFmt2_SetCloneVisible	31

CONST_INT TUT_SESSION_IN_CORONA		0
CONST_INT TUT_SESSION_FIRST_PLAYER	1
CONST_INT MAX_TUT_RACE_CAND			5


/*


STRUCT FM_TRIGGER_INTRO_STRUCT
	INT iFmTriggerIntroProg
	INT iBitset
	INT iBitset2
	INT iFloatingHelpProg
	INT iWaitingScreenHelpProg
	INT iAfterAmmuCutProg
	INT iAfterRaceCutProg
	INT iBeforeRaceCutProg
	
	structPedsForConversation 	speechFmTriggerTut
	STRING sFakeInvite
	CAMERA_INDEX cutsceneCam
	
	PED_INDEX pedLamar
	VEHICLE_INDEX vehLamar
	BLIP_INDEX blipLamar
	BLIP_INDEX blipTempRace
	
	SEQUENCE_INDEX seq
	SCRIPT_TIMER timeReserveDmSlot
	SCRIPT_TIMER timeLocalTImeOut
	SCRIPT_TIMER timeClearArea
	SCRIPT_TIMER timeTrigIntro
	
	INT iReservePlayerProg = 0
	INT reservationRequestUniqueID 
	INT iDmNumReservedPlayers
	INT iDmReservedPlayersBitset
	
	VECTOR vSafeRacePos
	VECTOR vSafeDMPos
	
	SCENARIO_BLOCKING_INDEX scenRace
	
	
	#IF IS_DEBUG_BUILD
		BOOL bForceCheckForReady
		BOOL bForceTimeOut
		BOOL bForceTimeOutDM
		BOOL bReserveDM
		BOOL bTutorialSkipped
	#ENDIF
ENDSTRUCT
*/
CONST_INT FM_TRIGGER_INTRO_INIT					0
CONST_INT FM_TRIGGER_INTRO_MESSAGE_END			1
CONST_INT FM_TRIGGER_INTRO_PHONE_AWAY			2
CONST_INT FM_TRIGGER_INTRO_FADE_OUT				3
CONST_INT FM_TRIGGER_INTRO_WARP_TO_START		4
CONST_INT FM_TRIGGER_INTRO_INIT_CUT				5
CONST_INT FM_TRIGGER_INTRO_CUT_FIRST_CAM		6
CONST_INT FM_TRIGGER_INTRO_CUT_FIRST_HELP		7
CONST_INT FM_TRIGGER_INTRO_CUT_WAIT_FOR_HELP	8
CONST_INT FM_TRIGGER_INTRO_CUT_SKIP				9
CONST_INT FM_TRIGGER_INTRO_CUT_DONE				11
CONST_INT FM_TRIGGER_INTRO_SEND_TEXT			12
CONST_INT FM_TRIGGER_INTRO_ON_RACE				13
CONST_INT FM_TRIGGER_INTRO_DONE_RACE			14
CONST_INT FM_TRIGGER_INTRO_IN_POSN				15
CONST_INT FM_TRIGGER_INTRO_CORONA_WARP			16
CONST_INT FM_TRIGGER_INTRO_CUT_WAIT1			17
CONST_INT FM_TRIGGER_INTRO_CUT_WAIT2			18
CONST_INT FM_TRIGGER_INTRO_START_DM_CUT			19
CONST_INT FM_TRIGGER_INTRO_CUT_START_DM			20
CONST_INT FM_TRIGGER_INTRO_CUT_HELP_DM			21
CONST_INT FM_TRIGGER_INTRO_CUT_FINISH_DM		22
CONST_INT FM_TRIGGER_INTRO_WARP_TO_DM			23
CONST_INT FM_TRIGGER_INTRO_IN_DM_CORONA			24
CONST_INT FM_TRIGGER_INTRO_ON_DM				25
CONST_INT FM_TRIGGER_INTRO_OFF_DM				26
CONST_INT FM_TRIGGER_INTRO_CUT_DM_SHOT2			27
CONST_INT FM_TRIGGER_INTRO_CUT_DM_SHOT_WAIT		28
CONST_INT FM_TRIGGER_INTRO_CUT_DM_SHOT_WAIT2	29
CONST_INT FM_TRIGGER_INTRO_DM_WAIT_FOR_CORONA	30
CONST_INT FM_TRIGGER_INTRO_CUT_FORCE_WARP_DM	31
CONST_INT FM_TRIGGER_INTRO_CREATE_LAMAR			32
CONST_INT FM_TRIGGER_INTRO_GET_IN_CAR			33
CONST_INT FM_TRIGGER_INTRO_DRIVE_TO_RACE		34
CONST_INT FM_TRIGGER_INTRO_PULL_OVER			35
CONST_INT FM_TRIGGER_INTRO_RACE_CUT				36
CONST_INT FM_TRIGGER_INTRO_WAIT_FOR_RACE_CUT	37
CONST_INT FM_TRIGGER_INTRO_START_MOCAP			38
CONST_INT FM_TRIGGER_INTRO_MOCAP_ACTIVE			39
CONST_INT FM_TRIGGER_INTRO_MOCAP_END			40
CONST_INT FM_TRIGGER_INTRO_SKYSWOOP_RACE		41
CONST_INT FM_TRIGGER_WARP_TO_SOLO_MISSION		42
CONST_INT FM_TRIGGER_ENTER_RACE_CORONA			45
CONST_INT FM_TRIGGER_SEND_DM_INVITE				46
CONST_INT FM_TRIGGER_NOTUT_CUT					50
CONST_INT FM_TRIGGER_INTRO_CUT_FINISH			98
CONST_INT FM_TRIGGER_INTRO_FINISHED				99

STRUCT structGetToGerald
	INT iGetToGeraldProg
	INT iGetToGeraldBitset
	SCRIPT_TIMER timeGoToGeraldStart
	BLIP_INDEX blipTempGerald
	GOTO_JOB_CUT_STRUCT gotoJobCut
	GET_UGC_CONTENT_STRUCT sGetUGC_content
	TEXT_LABEL_31 tlContent
	#IF IS_DEBUG_BUILD
		BOOL bDoGetToGeraldDebug
	#ENDIF
ENDSTRUCT

CONST_INT biGetToGerald_CoronaReadyForCut			0
CONST_INT biGetToGerald_SetupCorona					1
CONST_INT biGetToGerald_LoseCops					2
CONST_INT biGetToGerald_GetToCorona					3
CONST_INT biGetToGerald_DownloadMission				4
CONST_INT biGetToGerald_GotContentId				5
CONST_INT biGetToGerald_RaceToPointHelp				6

STRUCT structGetToSurvival
	INT iGetToSurvivalProg
	INT iGetToSurvivalBitset
	VECTOR vMySurvivalLoc
	TEXT_LABEL_15 tlObj
	SCRIPT_TIMER timeGoToSurvivalStart
	BLIP_INDEX blipTempSurvial
ENDSTRUCT

CONST_INT biGetToSurvival_DrawCorona				0
CONST_INT biGetToSurvival_LoseCops					1
CONST_INT biGetToSurvival_GetToCorona				2

#IF IS_DEBUG_BUILD
	PROC PRINT_TRIGGER_STRING(STRING sText)

		TEXT_LABEL_31 tl31
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
		ENDIF
		CPRINTLN(DEBUG_MP_TUTORIAL,tl31 ," [dsw] [TriggerTut] ", sText)
	ENDPROC

	PROC PRINT_TRIGGER_STRING_INT(STRING sText, INT iToPrint)

		TEXT_LABEL_31 tl31
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
		ENDIF
		CPRINTLN(DEBUG_MP_TUTORIAL,tl31," [dsw] [TriggerTut] ", sText, iToPrint)
	ENDPROC

	PROC PRINT_TRIGGER_STRING_FLOAT(STRING sText, FLOAT fToPrint)

		TEXT_LABEL_31 tl31
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
		ENDIF
		CPRINTLN(DEBUG_MP_TUTORIAL,tl31," [dsw] [TriggerTut] ", sText, fToPrint)
	ENDPROC
	
	PROC PRINT_TRIGGER_STRING_VECTOR(STRING sText1, VECTOR v)
	//	NET_PRINT_TIME() NET_PRINT(" [dsw] [TriggerTut] ")   NET_PRINT_STRING_VECTOR(sText1, v) NET_NL()
		TEXT_LABEL_31 tl31
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
		ENDIF
		CPRINTLN(DEBUG_MP_TUTORIAL,tl31," [dsw] [TriggerTut] ", sText1, v)
	ENDPROC
	
	PROC PRINT_TRIGGER_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player)
	//	NET_PRINT_TIME() NET_PRINT(" [dsw] [TriggerTut] ") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
		TEXT_LABEL_31 tl31
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
		ENDIF
		CPRINTLN(DEBUG_MP_TUTORIAL,tl31," [dsw] [TriggerTut] ", sText, GET_PLAYER_NAME(player))
	ENDPROC
	
	PROC PRINT_TRIGGER_STRINGS(STRING sText1, STRING sText2)
	//	NET_PRINT_TIME() NET_PRINT("[dsw] [TriggerTut]  ") NET_PRINT_STRINGS(sText1, sText2) NET_NL()
		TEXT_LABEL_31 tl31
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
		ENDIF
		CPRINTLN(DEBUG_MP_TUTORIAL,tl31," [dsw] [TriggerTut] ", sText1, sText2)
	ENDPROC
#ENDIF

//
//PROC Maintain_MissionsAtCoords_Gerald_Mission_InCorona(structGetToGerald &getToGerald)
//
//	// DAVE: STEP 1 -	CALL THIS ONCE FOR EVERY PLAYER WHEN THEY REACH THIS MISSION IN THE TUTORIAL BEFORE YOU DO ANYTHING WITH THE MISSION.
//	//					DON'T CALL IT FROM IN HERE - I'VE JUST PLACED IT HERE FOR REFERENCE - THE COORDS ARE THE COORDS OF THE MISSION.
//	
//	//Change_MissionsAtCoords_InCorona_External_Function_ID_For_Tutorial(<<-73.7516, -1462.5665, 30.5280>>, MATCICE_FM_TUTORIAL)
//
//
//	
//	// DAVE: STEP 2 -	THE NEXT TWO FUNCTION CALLS CHECK IF THE PLAYER IS IN THE CORONA OF YOUR MISSION.
//	//					PROBABLY BEST TO CALL THESE EVERY FRAME FOR EVERY PLAYER WHEN THEY REACH THIS MISSION IN THE TUTORIAL.
//	
//	
//	// Is the player in a MissionsAtCoords corona that requires InCorona Actions to be processed?
//	IF NOT (Should_External_Scripts_Process_MissionsAtCoords_InCorona_Actions())
//		// ...the player isn't in a corona that requires External Actions to be processed
//		
//		
////		#IF IS_DEBUG_BUILD	
////			
////			CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION]  Maintain_MissionsAtCoords_Gerald_Mission_InCorona Exit 1!") 
////		#ENDIF
//		EXIT
//	ENDIF
//	
//	
//						
//	
//	// InCorona Actions processing is required, so check if it is this routine that should be doing the processing
//	IF (Get_MissionsAtCoords_InCorona_Actions_ExternalID_To_Process() != MATCICE_FM_CORONA_TUT)
//		// ...it isn't this script that should be processing the current mission's InCorona Actions
//		#IF IS_DEBUG_BUILD	
//			
//			CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION]  Maintain_MissionsAtCoords_Gerald_Mission_InCorona Exit 2!") 
//		#ENDIF
//		
//		EXIT
//	ENDIF
//	
//
//	// DAVE: STEP 3 -	WHEN PROCESSING REACHES HERE IT MEANS THIS PLAYER IS IN THE CORONA OF THE TUTORIAL MISSION.
//	//					THIS IS THE START OF THE HANDSHAKING BETWEEN THIS FUNCTION AND MY CORONA CONTROL STUFF.
//	//					DO ANY INITIALISATION HERE (I DON'T THINK YOU'LL NEED TO DO ANYTHING - MAYBE TAKE PLAYER CONTROL OFF)
//	//						THEN TELL MY STUFF THAT YOU'VE FINISHED DOING INITIALISATION, ALSO CLEAR THE CLOUD-DATA LOADING CONTROL VARIABLES.
//	
//	IF (Is_MissionsAtCoords_InCorona_Stage_Initialise())
//		
//	
//		// Let Missions At Coords system know that initialisation is done and it now needs to wait for the cloud data to be downloaded
//		Set_MissionsAtCoords_InCorona_Stage_As_Initialised()
//		#IF IS_DEBUG_BUILD	
//			CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION]  Maintain_MissionsAtCoords_Gerald_Mission_InCorona Exit 3!") 
//		#ENDIF
//		
//		EXIT
//	ENDIF
//	
//	IF NOT IS_BIT_SET(getToGerald.iGetToGeraldBitset, biGetToGerald_CoronaReadyForCut)
//		SET_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_CoronaReadyForCut)
//		
//		#IF IS_DEBUG_BUILD
//		CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Maintain_MissionsAtCoords_Gerald_Mission_InCorona Set biGetToGerald_CoronaReadyForCut") 
//		#ENDIF
//		
//	ENDIF
//	
//ENDPROC

PROC DRAW_FAKE_MISSION_CORONA(VECTOR vFakeMissionLoc)
	
	VECTOR      ignoreDirection   = << 0.0, 0.0, 0.0 >>
	VECTOR      ignoreRotation    = << 0.0, 0.0, 0.0 >>

	FLOAT       theDiameter       = ciIN_LOCATE_DISTANCE * 2.0
	VECTOR      theScale          = << theDiameter, theDiameter, MATC_CORONA_INACTIVE_HEIGHT_m >>

	VECTOR      theCoords         = vFakeMissionLoc 
	theCoords.z -= 0.2

	INT         theRed            = 0
	INT         theGreen          = 0
	INT         theBlue           = 0
	INT         theAlpha          = 0
	GET_HUD_COLOUR(FM_ROCKSTAR_CREATED_CORONA_COLOUR, theRed, theGreen, theBlue, theAlpha)


	// ...draw Inactive Corona
	DRAW_MARKER(MARKER_CYLINDER, theCoords, ignoreDirection, ignoreRotation, theScale,
	            theRed, theGreen, theBlue, MATC_CORONA_INACTIVE_ALPHA)

ENDPROC

//PROC SETUP_FAKE_GERALD_CORONA_BLIP(structGetToGerald &getToGerald)
//	SET_BLIP_SCALE(getToGerald.blipTempGerald, BLIP_SIZE_NETWORK_COORD)
//	SET_BLIP_SPRITE(getToGerald.blipTempGerald, RADAR_TRACE_UGC_MISSION)
//	SHOW_HEIGHT_ON_BLIP(getToGerald.blipTempGerald, FALSE)
//	//SET_BLIP_AS_SHORT_RANGE(getToGerald.blipTempGerald, TRUE)
//	SET_BLIP_COLOUR(getToGerald.blipTempGerald, FM_ROCKSTAR_CREATED_BLIP_COLOUR)
//	SET_BLIP_PRIORITY(getToGerald.blipTempGerald, BLIPPRIORITY_LOW_LOWEST)
//	SET_BLIP_ROUTE(getToGerald.blipTempGerald, TRUE)
//	
//	BEGIN_TEXT_COMMAND_SET_BLIP_NAME("FMMC_RSTAR_MS")
//	END_TEXT_COMMAND_SET_BLIP_NAME(getToGerald.blipTempGerald)
//
//	SET_BLIP_FLASHES(getToGerald.blipTempGerald, TRUE)
//	SET_BLIP_FLASH_TIMER(getToGerald.blipTempGerald, 7000)
//ENDPROC

PROC SETUP_FAKE_CORONA_BLIP(BLIP_INDEX &blipCorona, INT iFmActivityType, BOOL bDoGps = TRUE, BOOL bDoFlash = TRUE)
	BLIP_SPRITE theSprite
	STRING theBlipName
	
	SWITCH iFmActivityType
		CASE FMMC_TYPE_MISSION
			theSprite = RADAR_TRACE_UGC_MISSION
			theBlipName = "FMMC_RSTAR_MS"
		BREAK
		
		CASE FMMC_TYPE_SURVIVAL
			theSprite = RADAR_TRACE_HORDE
			theBlipName = "FMMC_RSTAR_HM"
		BREAK
	ENDSWITCH
	
	SET_BLIP_SCALE(blipCorona, BLIP_SIZE_NETWORK_COORD)
	SET_BLIP_SPRITE(blipCorona, theSprite)
	SHOW_HEIGHT_ON_BLIP(blipCorona, FALSE)
	SET_BLIP_COLOUR(blipCorona, FM_ROCKSTAR_CREATED_BLIP_COLOUR)
	SET_BLIP_PRIORITY(blipCorona, BLIPPRIORITY_LOW_LOWEST)
	
	
	BEGIN_TEXT_COMMAND_SET_BLIP_NAME(theBlipName)
	END_TEXT_COMMAND_SET_BLIP_NAME(blipCorona)
	
	IF bDoGps
		SET_BLIP_ROUTE(blipCorona, TRUE)
	ENDIF
	
	IF bDoFlash
		SET_BLIP_FLASHES(blipCorona, TRUE)
		SET_BLIP_FLASH_TIMER(blipCorona, 7000)
	ENDIF
	
	
ENDPROC

PROC MAINTAIN_GET_TO_GERALD_MISSION_V2(structGetToGerald &getToGerald)
//	VECTOR vGeraldMission = <<898.4612,-2333.0442,29.4836 >>
	VECTOR vGeraldMission = <<897.5109,-2340.5986,29.4461>>
	SWITCH GetToGerald.iGetToGeraldProg
		CASE 0
			
			IF NOT HAS_GOTO_JOB_CUTSCENE_BEEN_DONE()
		//		IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_CUST_CAR_RACES) // Using custom cars in races help
				IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet, BI_FM_NMH_RACE_TO_POINT)
					IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CAR_MOD)
						IF HAS_FM_CAR_MOD_TUT_BEEN_DONE()
							IF g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY  <> FM_SESSION_MENU_CHOICE_JOIN_CLOSED_SOLO_PLAYABLE_SESSION
								IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
									IF NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
										IF NOT IS_PLAYER_IN_ANY_SHOP()
										
											IF NOT HAS_NET_TIMER_STARTED(getToGerald.timeGoToGeraldStart)
												START_NET_TIMER(getToGerald.timeGoToGeraldStart)
												
												#IF IS_DEBUG_BUILD 
													CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Started timeGoToGeraldStart") 
												#ENDIF
											ELSE
												IF HAS_NET_TIMER_EXPIRED(getToGerald.timeGoToGeraldStart, 15000)
													IF Request_MP_Comms_Txtmsg(CHAR_MP_GERALD, "FM_GER_TXT", DEFAULT, DEFAULT)
														RESET_NET_TIMER(getToGerald.timeGoToGeraldStart)
														
														
														SET_BIT(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_GOTO_GERALD_LTS)
														
														getToGerald.iGetToGeraldProg++
														
														#IF IS_DEBUG_BUILD 
															CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Done text message. iGetToGeraldProg = ",  GetToGerald.iGetToGeraldProg) 
														#ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELSE
											//-- In shop
											#IF IS_DEBUG_BUILD 
												IF getToGerald.bDoGetToGeraldDebug
													CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Case 0 - Waiting for IS_PLAYER_IN_ANY_SHOP ") 
												ENDIF
											#ENDIF
										ENDIF
									ELSE
										//-- Browsing shop
										#IF IS_DEBUG_BUILD 
											IF getToGerald.bDoGetToGeraldDebug
												CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Case 0 - Waiting for IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP ") 
											ENDIF
										#ENDIF
									ENDIF
								ELSE
									//-- Not ok to print help
									#IF IS_DEBUG_BUILD 
										IF getToGerald.bDoGetToGeraldDebug
											CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Case 0 - Waiting for IS_OK_TO_PRINT_FREEMODE_HELP ") 
										ENDIF
									#ENDIF
								ENDIF
							ELSE
								//-- SOlo session
								SET_GOTO_JOB_CUTSCENE_DONE_FOR_SOLO_SESSION()
							ENDIF
						ELSE
							//-- Not done car mod tut
							#IF IS_DEBUG_BUILD 
								IF getToGerald.bDoGetToGeraldDebug
									CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Case 0 - Waiting for HAS_FM_CAR_MOD_TUT_BEEN_DONE ") 
								ENDIF
							#ENDIF
						ENDIF
					ELSE
						//-- Waiting for FMMC_TYPE_CAR_MOD
						#IF IS_DEBUG_BUILD 
							IF getToGerald.bDoGetToGeraldDebug
								CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Case 0 - Waiting for FMMC_TYPE_CAR_MOD ") 
							ENDIF
						#ENDIF
					ENDIF
				ELSE
					//-- Waiting for custom car text
					#IF IS_DEBUG_BUILD 
						IF getToGerald.bDoGetToGeraldDebug
							CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Case 0 - Waiting for BI_FM_NMH3_CUST_CAR_RACES ") 
						ENDIF
					#ENDIF
				ENDIF
			ELSE
				//-- Already done cutscene
				getToGerald.iGetToGeraldProg = 100
				#IF IS_DEBUG_BUILD 
					CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Already done goto job cutscene. iGetToGeraldProg = ",  GetToGerald.iGetToGeraldProg) 
				#ENDIF
				
				IF NOT DONE_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_RACE)
					#IF IS_DEBUG_BUILD 
						CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Already done goto job cutscene, but races not unlocked") 
					#ENDIF
					SET_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_DEATHMATCH)
					SET_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_RACE)
					SET_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_MG_SHOOTING_RANGE)
					
					UNLOCK_FM_ACTIVITY(FMMC_TYPE_DEATHMATCH)
					UNLOCK_FM_ACTIVITY(FMMC_TYPE_RACE)
					UNLOCK_FM_ACTIVITY(FMMC_TYPE_MG_SHOOTING_RANGE)
					
					SET_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_MISSION)
					SET_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_MISSION_CTF)
					SET_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_IMPROMPTU_DM)
					
					UNLOCK_FM_ACTIVITY(FMMC_TYPE_MISSION)
					UNLOCK_FM_ACTIVITY(FMMC_TYPE_MISSION_CTF)
					UNLOCK_FM_ACTIVITY(FMMC_TYPE_IMPROMPTU_DM)
					
					// ChrisM - 06/01/14 - Added to ensure all mission types are unlocked
					UNLOCK_FM_ACTIVITY(FMMC_TYPE_MISSION_LTS)
					UNLOCK_FM_ACTIVITY(FMMC_TYPE_MISSION_VS)
					
					SET_FM_UNLOCKS_BIT_SET()
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_NET_TIMER_EXPIRED(getToGerald.timeGoToGeraldStart, 5000)
				Print_Objective_Text("FM_GER_GOT") // Goto the ~b~Trigger.
				SET_CORONA_WALK_IN_DISABLED_FOR_TUTORIAL(TRUE)
				Temporarily_Unlock_MissionsAtCoords_Mission_For_Tutorial(vGeraldMission, TRUE)
				Set_MissionsAtCoords_Mission_Blip_At_Coords_To_LongRange_For_Tutorial(vGeraldMission, TRUE) 
				Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_For_Tutorial(vGeraldMission, TRUE)
				Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_For_Tutorial(vGeraldMission, FALSE)
				getToGerald.blipTempGerald = ADD_BLIP_FOR_COORD(vGeraldMission)			
				SET_BLIP_SCALE(getToGerald.blipTempGerald, 0.0)
				MPGlobalsAmbience.tlQuickGPS = "FM_GER_GPS"
				SET_BLIP_COLOUR(getToGerald.blipTempGerald, FM_ROCKSTAR_CREATED_BLIP_COLOUR)
				SET_BLIP_ROUTE(getToGerald.blipTempGerald, TRUE)
		//		SETUP_FAKE_CORONA_BLIP(getToGerald.blipTempGerald, FMMC_TYPE_MISSION)
				
		//		SET_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_SetupCorona) 
				SET_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_GetToCorona)
				
				RESET_NET_TIMER(getToGerald.timeGoToGeraldStart)
				START_NET_TIMER(getToGerald.timeGoToGeraldStart)
				
				// Keith: Need to ensure this LTS is free - doing it using a global flag because it's too late to do it any other way
				Set_Player_As_On_Tutorial_LTS(vGeraldMission)
				
				getToGerald.iGetToGeraldProg++
				#IF IS_DEBUG_BUILD 
					CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Done text message. iGetToGeraldProg = ",  GetToGerald.iGetToGeraldProg) 
				#ENDIF
			ENDIF
			
		BREAK
		
		CASE 2
		
			//-- Help text about doing a race to point
			IF NOT IS_BIT_SET(getToGerald.iGetToGeraldBitset, biGetToGerald_RaceToPointHelp)
				IF IS_BIT_SET(getToGerald.iGetToGeraldBitset, biGetToGerald_GetToCorona)
					IF HAS_NET_TIMER_EXPIRED(getToGerald.timeGoToGeraldStart, 4000)
						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
							#IF IS_DEBUG_BUILD 
								CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Doing Cypress flats race to point help") 
							#ENDIF
							
							
							PRINT_HELP("FM_RTP_TRY") // Try using Impromptu Race from the Interaction Menu to challenge a player to be the first over to Cypress Flats.
							SET_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_RaceToPointHelp)
							MPGlobalsAmbience.vQuickGPS = vGeraldMission
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_LOCAL_PLAYER_RUNNING_PASSIVE_MODE_CUTSCENE()
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF NOT IS_BIT_SET(getToGerald.iGetToGeraldBitset, biGetToGerald_GetToCorona)
							Print_Objective_Text("FM_GER_GOT") // Goto the ~b~Trigger.
							getToGerald.blipTempGerald = ADD_BLIP_FOR_COORD(vGeraldMission)			
							SET_BLIP_SCALE(getToGerald.blipTempGerald, 0.0)
							SET_BLIP_COLOUR(getToGerald.blipTempGerald, FM_ROCKSTAR_CREATED_BLIP_COLOUR)	
							SET_BLIP_ROUTE(getToGerald.blipTempGerald, TRUE)
							Set_MissionsAtCoords_Mission_Blip_At_Coords_To_LongRange_For_Tutorial(vGeraldMission, TRUE)
	//						SETUP_FAKE_CORONA_BLIP(getToGerald.blipTempGerald, FMMC_TYPE_MISSION)
	//						SET_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_SetupCorona)
							SET_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_GetToCorona)
							
							CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Blip had been removed - re-adding") 
							
						ENDIF
						
						IF IS_BIT_SET(getToGerald.iGetToGeraldBitset, biGetToGerald_LoseCops) 
							CLEAR_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_LoseCops)
						ENDIF
						
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vGeraldMission) < ( ciIN_LOCATE_DISTANCE ) * ( ciIN_LOCATE_DISTANCE )
					//	AND IS_PLAYER_IN_CORONA()
							IF NOT MPGlobalsAmbience.R2Pdata.bOnRaceToPoint
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									CLEAR_HELP()
									Clear_Any_Objective_Text_From_This_Script()
									IF DOES_BLIP_EXIST(getToGerald.blipTempGerald)
										REMOVE_BLIP(getToGerald.blipTempGerald)
									ENDIF
									SET_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_CoronaReadyForCut)
									SET_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_DownloadMission)
									Set_MissionsAtCoords_Mission_Blip_At_Coords_To_LongRange_For_Tutorial(vGeraldMission, FALSE)
									
									DISABLE_KILL_YOURSELF_OPTION()

									getToGerald.iGetToGeraldProg++
									#IF IS_DEBUG_BUILD 
										CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Done text message. iGetToGeraldProg = ",  GetToGerald.iGetToGeraldProg) 
									#ENDIF
								ELSE
									IF IS_OK_TO_PRINT_FREEMODE_HELP()
										IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMMC_NAE_CAR")
											PRINT_HELP("FMMC_NAE_CAR") //Jobs can only be triggered on foot.
										ENDIF
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD 
									CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] At corona loc, but bOnRaceToPoint!",  GetToGerald.iGetToGeraldProg) 
								#ENDIF
							ENDIF
						ENDIF
					ELSE
						//-- Got a wanted level
						IF NOT IS_BIT_SET(getToGerald.iGetToGeraldBitset, biGetToGerald_LoseCops) 
							IF IS_BIT_SET(getToGerald.iGetToGeraldBitset, biGetToGerald_GetToCorona)
								CLEAR_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_GetToCorona)
							ENDIF
							IF DOES_BLIP_EXIST(getToGerald.blipTempGerald)
								REMOVE_BLIP(getToGerald.blipTempGerald)
							ENDIF
							CLEAR_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_SetupCorona)
							Print_Objective_Text("FM_IHELP_LCP") // Lose the cops
							SET_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_LoseCops) 
							CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Told to lose cops") 
						ENDIF
							
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			
			IF IS_BIT_SET(getToGerald.iGetToGeraldBitset, biGetToGerald_CoronaReadyForCut)
				IF NOT IS_LOCAL_PLAYER_RUNNING_PASSIVE_MODE_CUTSCENE()
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vGeraldMission) < 100.0
						IF DO_GOTO_JOB_CUTSCENE_V2(getToGerald.gotoJobCut,vGeraldMission, TRUE)
						
							
						//	Temporarily_Unlock_MissionsAtCoords_Mission_For_Tutorial(vGeraldMission, TRUE)
							CLEAR_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_SetupCorona)
							CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_GOTO_GERALD_LTS)
							CLEAR_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_DownloadMission)
							ENABLE_KILL_YOURSELF_OPTION()
							MPGlobalsAmbience.tlQuickGPS = ""
							getToGerald.iGetToGeraldProg++
							#IF IS_DEBUG_BUILD 
								CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] iGetToGeraldProg = ",  GetToGerald.iGetToGeraldProg) 
							#ENDIF
						ELSE
							IF IS_BIT_SET(getToGerald.gotoJobCut.iGotoJobBitset, bi_ReadyForMission)
								Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_For_Tutorial(vGeraldMission, TRUE)
								Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_For_Tutorial(vGeraldMission, FALSE)
								CLEAR_BIT(getToGerald.gotoJobCut.iGotoJobBitset, bi_ReadyForMission)
								CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Cleared bi_ReadyForMission") 
							ENDIF
						ENDIF
					ELSE
						CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Case 3, VDIST2 > 100.0") 
					ENDIF
				ELSE
					
					IF IS_BIT_SET(getToGerald.iGetToGeraldBitset, biGetToGerald_GetToCorona)
						CLEAR_BIT(getToGerald.iGetToGeraldBitset, biGetToGerald_GetToCorona)
					ENDIF
					
					getToGerald.iGetToGeraldProg = 2
					CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] Case 3, running passive cut, set getToGerald.iGetToGeraldProg = ", getToGerald.iGetToGeraldProg) 
					
				ENDIF
			ENDIF
			
			
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(getToGerald.iGetToGeraldBitset, biGetToGerald_SetupCorona)
		DRAW_FAKE_MISSION_CORONA(<<898.4612,-2333.0442,29.4836 >>)
	ENDIF
	
	
	

	
	//-- s-skip
	#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_GOTO_GERALD_LTS)
			IF getToGerald.iGetToGeraldProg < 3
				IF NOT HAS_GOTO_JOB_CUTSCENE_BEEN_DONE()
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
						Clear_Any_Objective_Text_From_This_Script()
						IF DOES_BLIP_EXIST(getToGerald.blipTempGerald)
							REMOVE_BLIP(getToGerald.blipTempGerald)
						ENDIF
						
						Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_For_Tutorial(vGeraldMission, TRUE)
						Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_For_Tutorial(vGeraldMission, FALSE)
						Set_MissionsAtCoords_Mission_Blip_At_Coords_To_LongRange_For_Tutorial(vGeraldMission, FALSE)
						SET_CORONA_WALK_IN_DISABLED_FOR_TUTORIAL(FALSE)
						INT iStatInt
						iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
						SET_BIT(iStatInt, biFmCut_GotoJobCut)
//						SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_GOTO_JOB_CUTSCENE)
						SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
						
//						REQUEST_SAVE(STAT_SAVETYPE_PROLOGUE)
						CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_GERALD_MISSION] S-skipped")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
ENDPROC

// KEITH 18/9/13: Added this flag to ensure the LTS tutorial is free, need to clear the flag at the appropriate time
PROC MAINTAIN_ON_LTS_TUTORIAL_CHECKS()

	IF NOT (IS_LOCAL_PLAYER_ON_TUTORIAL_LTS())
		EXIT
	ENDIF
	
	// Clear flag if:	Player isn't on an ambient tutorial (ie: doesn't have goto Cypress Flats objective)
	//					Player isn't in a corona
	//					Player isn't transitioning from the Invite screen
	//					Player doesn't have a focus mission
	//					Player isn't in range of cypress flats corona (give a bit of leeway so it remains free if the player accidentally walks out and then back in again)
	CONST_FLOAT	CORONA_RANGE_LEEWAY	30.0
	
	IF (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())
		EXIT
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA())
		EXIT
	ENDIF
	
	IF (IS_TRANSITION_SESSION_RESTARTING())
		EXIT
	ENDIF
	
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		EXIT
	ENDIF
	
	// Player isn't on an ambient tutorial and player isn't in a corona and a transition session isn't restarting
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
		// ...player isn't ok, so clear player as doing the LTS tutorial
		Clear_Player_As_On_Tutorial_LTS()
		
		EXIT
	ENDIF
	
	// Player is OK, so check if player is still in range of the corona
	VECTOR playerCoords = GET_PLAYER_COORDS(PLAYER_ID())
	VECTOR ltsCoords	= Get_Coords_Of_Tutorial_LTS()
	
	IF (ARE_VECTORS_ALMOST_EQUAL(playerCoords, ltsCoords, CORONA_RANGE_LEEWAY))
		// Player is still close to the corona, so don't clear the flag
		EXIT
	ENDIF
	
	// Clear the flag
	Clear_Player_As_On_Tutorial_LTS()
	
ENDPROC

FUNC INT GET_MY_PRIMARY_SURVIVAL()
	INT iMySurvival = -1
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT5)
	
	IF IS_BIT_SET(iStatInt, biNmh5_PrimarySurvBoneyard)
		iMySurvival = PRIMARY_SURVIVAL_BONEYARD
	ELIF IS_BIT_SET(iStatInt, biNmh5_PrimarySurvIndustrial)
		iMySurvival = PRIMARY_SURVIVAL_INDUSTRIAL
	ENDIF


	RETURN iMySurvival
	
ENDFUNC

FUNC BOOL MAINTAIN_GET_TO_SURVIVAL(structGetToSurvival &getToSurvival)
	/*
		Need to 
			- Block cross-session invites
			- set on ambient tutorial
			- test actually going into corona
			- delay help until after survival is done
	*/	
	
	BOOL bReturn

	VECTOR vBoneYard 	= <<2296.8977,3110.9358,46.4730>> 
	VECTOR vIndustrial 	= <<269.3056,2777.5464,42.8282>> //<<1286.3673,-3336.3889,4.9020>>
	INT iMySurvival
	
	SWITCH getToSurvival.iGetToSurvivalProg
		CASE 0
			IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_SURVIVAL)
				IF DONE_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_SURVIVAL)
					IF NOT HAS_PRIMARY_SURVIVAL_BEEN_COMPLETED()
						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
							CPRINTLN(DEBUG_MP_UNLOCKS, " ")
							CPRINTLN(DEBUG_MP_UNLOCKS, " ***************** START GET TO SURVIVAL ***************")
							CPRINTLN(DEBUG_MP_UNLOCKS, " ")
							
							iMySurvival = GET_MY_PRIMARY_SURVIVAL()
							IF iMySurvival = -1
								iMySurvival = PRIMARY_SURVIVAL_BONEYARD
								CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] Failed to get my survival! Setting to boneyard")
							ENDIF
							CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] iMySurvival = ", iMySurvival)
							iMySurvival = 1 //PRIMARY_SURVIVAL_BONEYARD
							IF iMySurvival = PRIMARY_SURVIVAL_BONEYARD
								getToSurvival.vMySurvivalLoc = vBoneYard
								getToSurvival.tlObj = "FM_SUR_OB0"
								CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] My survival is BoneYard ")
							ELSE
								getToSurvival.vMySurvivalLoc = vIndustrial
								getToSurvival.tlObj = "FM_SUR_OB1"
								CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] My survival is Industrial ")
							ENDIF
							
							START_NET_TIMER(getToSurvival.timeGoToSurvivalStart)
							
							
							getToSurvival.iGetToSurvivalProg++
							CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] Started. iGetToSurvivalProg = ",  getToSurvival.iGetToSurvivalProg) 
						ENDIF
					ELSE
						getToSurvival.iGetToSurvivalProg = 99
						CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] Already done. iGetToSurvivalProg = ",  getToSurvival.iGetToSurvivalProg)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_NET_TIMER_EXPIRED(getToSurvival.timeGoToSurvivalStart, 2000)
				getToSurvival.blipTempSurvial = ADD_BLIP_FOR_COORD(getToSurvival.vMySurvivalLoc)
				SET_BLIP_SCALE(getToSurvival.blipTempSurvial, 0.0)
				SET_BLIP_COLOUR(getToSurvival.blipTempSurvial, FM_ROCKSTAR_CREATED_BLIP_COLOUR)	
				SET_BLIP_ROUTE(getToSurvival.blipTempSurvial, TRUE)
			//	SETUP_FAKE_CORONA_BLIP(getToSurvival.blipTempSurvial, FMMC_TYPE_SURVIVAL)
				
				Print_Objective_Text(getToSurvival.tlObj) // Go to the trigger
				
				Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_For_Tutorial(getToSurvival.vMySurvivalLoc, TRUE)
				Set_MissionsAtCoords_Mission_Blip_At_Coords_To_LongRange_For_Tutorial(getToSurvival.vMySurvivalLoc, TRUE)
				
			//	SET_BIT(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_DrawCorona)
				SET_BIT(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_GetToCorona)
				getToSurvival.iGetToSurvivalProg++
				CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] iGetToSurvivalProg = ",  getToSurvival.iGetToSurvivalProg) 
				
			ENDIF
		BREAK
		
		CASE 2
			IF NOT HAS_PRIMARY_SURVIVAL_BEEN_COMPLETED()
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF IS_BIT_SET(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_LoseCops) 
							CLEAR_BIT(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_LoseCops)
						ENDIF
							
						IF NOT IS_BIT_SET(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_GetToCorona)
							getToSurvival.blipTempSurvial = ADD_BLIP_FOR_COORD(getToSurvival.vMySurvivalLoc)
							SET_BLIP_SCALE(getToSurvival.blipTempSurvial, 0.0)
							SET_BLIP_COLOUR(getToSurvival.blipTempSurvial, FM_ROCKSTAR_CREATED_BLIP_COLOUR)	
							SET_BLIP_ROUTE(getToSurvival.blipTempSurvial, TRUE)
						//	SETUP_FAKE_CORONA_BLIP(getToSurvival.blipTempSurvial, FMMC_TYPE_SURVIVAL)
							Print_Objective_Text(getToSurvival.tlObj) // Go to the trigger
							Set_MissionsAtCoords_Mission_Blip_At_Coords_To_LongRange_For_Tutorial(getToSurvival.vMySurvivalLoc, TRUE)
							SET_BIT(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_DrawCorona)
							SET_BIT(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_GetToCorona)
							CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] Re-adding blip")
						ENDIF
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), getToSurvival.vMySurvivalLoc) < ( ciIN_LOCATE_DISTANCE ) * ( ciIN_LOCATE_DISTANCE )
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF DOES_BLIP_EXIST(getToSurvival.blipTempSurvial)
								AND IS_PLAYER_IN_CORONA()
									REMOVE_BLIP(getToSurvival.blipTempSurvial)
									IF Is_This_The_Current_Objective_Text(getToSurvival.tlObj)
										Clear_Any_Objective_Text_From_This_Script()
									ENDIF
			//						NET_SET_PLAYER_CONTROL(PLAYER_INDEX playerId, bool bHasControl, BOOL bVisible = TRUE, BOOL bClearTasks = TRUE, BOOL bHasCollision = FALSE, BOOL bFreezePos = FALSE, BOOL bCanBeTargetted = FALSE, BOOL bInvincible = TRUE, BOOL bAllowDamage = FALSE, BOOL bAllowCamera = FALSE, BOOL bAllowDuringTransition=FALSE)
//									NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS )		// COmmented out due to us waiting for corona now (2414159)
									CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] Removing blip / clearing objective text as in corona")
									CLEAR_TUTORIAL_INVITES_ARE_BLOCKED()
									CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_GOTO_SURVIVAL)
									Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_For_Tutorial(getToSurvival.vMySurvivalLoc, FALSE)
									Set_MissionsAtCoords_Mission_Blip_At_Coords_To_LongRange_For_Tutorial(getToSurvival.vMySurvivalLoc, FALSE)
								//	getToSurvival.iGetToSurvivalProg = 99
								//	CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] GetToSurvivalProg = ",  getToSurvival.iGetToSurvivalProg)
						
								ENDIF
							ENDIF
						ELSE
							IF NOT DOES_BLIP_EXIST(getToSurvival.blipTempSurvial)
								SET_TUTORIAL_INVITES_ARE_BLOCKED()
								SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_GOTO_SURVIVAL)
								getToSurvival.blipTempSurvial = ADD_BLIP_FOR_COORD(getToSurvival.vMySurvivalLoc)
								SET_BLIP_SCALE(getToSurvival.blipTempSurvial, 0.0)
								SET_BLIP_COLOUR(getToSurvival.blipTempSurvial, FM_ROCKSTAR_CREATED_BLIP_COLOUR)	
								SET_BLIP_ROUTE(getToSurvival.blipTempSurvial, TRUE)
								Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_For_Tutorial(getToSurvival.vMySurvivalLoc, TRUE)
								Set_MissionsAtCoords_Mission_Blip_At_Coords_To_LongRange_For_Tutorial(getToSurvival.vMySurvivalLoc, TRUE)
							//	SETUP_FAKE_CORONA_BLIP(getToSurvival.blipTempSurvial, FMMC_TYPE_SURVIVAL)
							//	SET_BIT(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_DrawCorona)
								Print_Objective_Text(getToSurvival.tlObj) 
								CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] Player left survival corona without playing")
							ENDIF
						ENDIF
					ELSE
						//-- Got a wanted level
						IF NOT IS_BIT_SET(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_LoseCops) 
							IF IS_BIT_SET(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_GetToCorona)
								CLEAR_BIT(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_GetToCorona)
							ENDIF
							IF DOES_BLIP_EXIST(getToSurvival.blipTempSurvial)
								REMOVE_BLIP(getToSurvival.blipTempSurvial)
							ENDIF
							CLEAR_BIT(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_DrawCorona)
							Print_Objective_Text("FM_IHELP_LCP") // Lose the cops
							SET_BIT(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_LoseCops)
							CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] Told to lose cops") 
						ENDIF
					ENDIF
				ENDIF
			ELSE
				getToSurvival.iGetToSurvivalProg = 99
				CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] Case 2, primary survival completedGetToSurvivalProg = ",  getToSurvival.iGetToSurvivalProg)
			ENDIF
			bReturn = TRUE
//			IF NOT HAS_PRIMARY_SURVIVAL_BEEN_COMPLETED()
//				
//				IF IS_PLAYER_IN_CORONA()
//					IF DOES_BLIP_EXIST(getToSurvival.blipTempSurvial)
//						REMOVE_BLIP(getToSurvival.blipTempSurvial)
//						IF Is_This_The_Current_Objective_Text("FM_GER_GOT")
//							Clear_Any_Objective_Text_From_This_Script()
//						ENDIF
//						CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] Removing blip / clearing objective text as in corona")
//					ENDIF
//				ELSE
//					IF NOT DOES_BLIP_EXIST(getToSurvival.blipTempSurvial)
//						IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
//							getToSurvival.blipTempSurvial = ADD_BLIP_FOR_COORD(vBoneYard)
//							SET_BLIP_SCALE(getToSurvival.blipTempSurvial, 0.0)
//							SET_BLIP_ROUTE(getToSurvival.blipTempSurvial, TRUE)
//							Print_Objective_Text("FM_GER_GOT") // Go to the trigger
//							CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] Re-adding blip as no longer in corona")
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
		BREAK
		
		CASE 99
			IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_GOTO_SURVIVAL)
				CLEAR_TUTORIAL_INVITES_ARE_BLOCKED()
				CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_GOTO_SURVIVAL)
				CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] Clearing blocked invites as Primary survival completed")
			ENDIF
			IF DOES_BLIP_EXIST(getToSurvival.blipTempSurvial)
				REMOVE_BLIP(getToSurvival.blipTempSurvial)
				IF Is_This_The_Current_Objective_Text("FM_GER_GOT")
					Clear_Any_Objective_Text_From_This_Script()
				ENDIF
				
			ENDIF
			CLEAR_BIT(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_DrawCorona)
			getToSurvival.iGetToSurvivalProg = 100
			CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] GetToSurvivalProg = ",  getToSurvival.iGetToSurvivalProg)
				
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(getToSurvival.iGetToSurvivalBitset, biGetToSurvival_DrawCorona)
		DRAW_FAKE_MISSION_CORONA(getToSurvival.vMySurvivalLoc)
		bReturn = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD	
		IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_GOTO_SURVIVAL)
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				SET_PLAYER_HAS_COMPLETED_PRIMARY_SURVIVAL()
				Clear_Any_Objective_Text_From_This_Script()
				Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_For_Tutorial(getToSurvival.vMySurvivalLoc, FALSE)
				Set_MissionsAtCoords_Mission_Blip_At_Coords_To_LongRange_For_Tutorial(getToSurvival.vMySurvivalLoc, FALSE)
				getToSurvival.iGetToSurvivalProg = 99
				CPRINTLN(DEBUG_MP_UNLOCKS, "[dsw] [MAINTAIN_GET_TO_SURVIVAL] s-passed GetToSurvivalProg = ",  getToSurvival.iGetToSurvivalProg)
			ENDIF
		ENDIF
	#ENDIF
	
	RETURN bReturn
ENDFUNC
PROC DRAW_PLAYER_WAIT_COUNT_DOWN_TIMER( INT iCoronaTimerTemp, SCRIPT_TIMER timeServer)
/*
	timeWaitForOtherRacers
*/
	IF HAS_NET_TIMER_STARTED(timeServer)	
		INT ms = (iCoronaTimerTemp - GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),timeServer.Timer))
		IF ms >= 0
			
			DRAW_GENERIC_TIMER(ms, "FM_IHELP_TME")
									
		ENDIF
	ELSE
//		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DRAW_PLAYER_WAIT_COUNT_DOWN_TIMER] Timer not started!")#ENDIF
	ENDIF
ENDPROC
PROC CLEANUP_TRIG_TUT_CUTSCENE(FM_TRIGGER_INTRO_STRUCT &fmTrigger, BOOL bFadeIn = TRUE)	
	
	IF IS_BIT_SET(fmTrigger.iBitset,biFmT_RunningCutscene)
		CLEANUP_MP_CUTSCENE()
		CLEAR_HELP()
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		IF DOES_CAM_EXIST(fmTrigger.cutsceneCam)
			SET_CAM_ACTIVE(fmTrigger.cutsceneCam, FALSE)
		ENDIF

		DESTROY_ALL_CAMS()
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		
		IF bFadeIn
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
		ENDIF
		
	//	ENTITY_INDEX eCutPed
	//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niCutPlayer)
	//		eCutPed = NET_TO_ENT(niCutPlayer)
	//		DELETE_ENTITY(eCutPed)
	//	ENDIF
		
		
		CLEAR_BIT(fmTrigger.iBitset,biFmT_RunningCutscene)
		
		
	ENDIF
ENDPROC

FUNC MODEL_NAMES GET_AMBIENT_RACE_CAR_MODEL(INT index)
	MODEL_NAMES mCar
	SWITCH index
		CASE 0
			mCar = FUGITIVE// BUFFALO
		BREAK
		CASE 1
			mCar = RUINER
		BREAK
		CASE 2
			mCar = PRAIRIE
		BREAK
	ENDSWITCH
	
	RETURN mCar
ENDFUNC

FUNC MODEL_NAMES GET_AMBIENT_RACE_CAR_DRIVER_MODEL(INT index)
	
	MODEL_NAMES mPed
	SWITCH index
		CASE 0 mPed = A_M_Y_GENSTREET_01 BREAK
		CASE 1 mPed = A_M_M_EASTSA_02	 BREAK
		CASE 2 mPed = A_M_Y_GENSTREET_01 	BREAK
		CASE 3 mPed = A_M_Y_GENSTREET_01 BREAK
	ENDSWITCH
	
	RETURN mPed
ENDFUNC

PROC GET_CREATE_LOC_FOR_AMIENT_RACE_CAR(INT index, VECTOR &vCreate, FLOAT &fHeading)
	SWITCH index  

		 
	
		CASE 0 vCreate = <<-208.8969, 315.4391, 95.9455>> fHeading = 179.6984   BREAK
		CASE 1 vCreate = <<-205.4772, 301.4597, 95.9455>> fHeading = 2.2437		BREAK // Lhs
	//	CASE 1 vCreate = <<-209.1232, 301.3658, 95.9458>> fHeading = 357.5877		BREAK 
		CASE 2 vCreate = <<-205.5090, 314.7662, 95.9455>>  fHeading = 185.3363	BREAK
	
		
	ENDSWITCH
ENDPROC

PROC SET_AMBIENT_RACE_CAR_COLOURS(INT index, FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	IF IS_VEHICLE_DRIVEABLE(fmTrigger.vehRacers[index])
		SWITCH index
			CASE 0
				// Buffalo 
				SET_VEHICLE_COLOURS(fmTrigger.vehRacers[index], 0, 132)
				SET_VEHICLE_EXTRA_COLOURS(fmTrigger.vehRacers[index], 0, 156)
			BREAK
			
			CASE 1
				// RUINER
				SET_VEHICLE_COLOURS(fmTrigger.vehRacers[index], 0, 6)
				SET_VEHICLE_EXTRA_COLOURS(fmTrigger.vehRacers[index], 0, 156)
			BREAK
			
			CASE 2
				// PRAIRIE
				SET_VEHICLE_COLOURS(fmTrigger.vehRacers[index], 111, 0)
				SET_VEHICLE_EXTRA_COLOURS(fmTrigger.vehRacers[index], 0, 156)
			BREAK
			
			CASE 3
				// Fugitive
		//		SET_VEHICLE_COLOURS(fmTrigger.vehRacers[index], 73, 0)
		//		SET_VEHICLE_EXTRA_COLOURS(fmTrigger.vehRacers[index], 1, 0)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC
FUNC BOOL CREATE_AMBIENT_RACE_CARS(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	VECTOR vLoc
	FLOAT fHead
	INT i
	REL_GROUP_HASH relPlayer
	
	relPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
	
	REQUEST_VEHICLE_RECORDING(1, "FM_Tut")
	
	FOR i = 0 TO MAX_TRIGGER_TUT_VEH - 1
		IF NOT DOES_ENTITY_EXIST(fmTrigger.vehRacers[i])
			MODEL_NAMES mCar = GET_AMBIENT_RACE_CAR_MODEL(i)
			REQUEST_MODEL(mCar)
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "FM_Tut")
				IF HAS_MODEL_LOADED(mCar)
					GET_CREATE_LOC_FOR_AMIENT_RACE_CAR(i, vLoc, fHead)
					fmTrigger.vehRacers[i] = CREATE_VEHICLE(mCar, vLoc, fHead, FALSE, FALSE)
					SET_AMBIENT_RACE_CAR_COLOURS(i, fmTrigger)
					SET_VEHICLE_DOORS_LOCKED(fmTrigger.vehRacers[i], VEHICLELOCK_LOCKED)
					SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(fmTrigger.vehRacers[i], FALSE, relPlayer)
					IF i = 0
						SET_ENTITY_PROOFS(fmTrigger.vehRacers[0], TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
					//	FREEZE_ENTITY_POSITION(fmTrigger.vehRacers[0], TRUE)
						START_PLAYBACK_RECORDED_VEHICLE(fmTrigger.vehRacers[0], 1, "FM_Tut")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(fmTrigger.vehRacers[0], 750.0)
						PAUSE_PLAYBACK_RECORDED_VEHICLE(fmTrigger.vehRacers[0])
						SET_VEH_RADIO_STATION(fmTrigger.vehRacers[0], "OFF")
					ENDIF
					#IF IS_DEBUG_BUILD 
						TEXT_LABEL tl = ""
						PRINT_TRIGGER_STRING_INT("[CREATE_AMBIENT_RACE_CARS] Created ambient car ", i) 
						tl += i
						SET_VEHICLE_NAME_DEBUG(fmTrigger.vehRacers[i], tl)
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR i = 0 TO MAX_TRIGGER_TUT_VEH - 1
		IF NOT DOES_ENTITY_EXIST(fmTrigger.vehRacers[i])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "FM_Tut")
		#IF IS_DEBUG_BUILD 
			PRINT_TRIGGER_STRING("[CREATE_AMBIENT_RACE_CARS] False as waiting for recording")
		#ENDIF
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC GET_AMBIENT_RACE_CAR_DRIVER_POSTION(INT index, VECTOR &vPos, FLOAT &fHead)
	SWITCH index
//		CASE 0 vPos =  <<-694.8934, -982.1395, 19.3900>> fHead =  43.4950 BREAK
//		CASE 1 vPos = <<-700.1951, -982.9870, 19.3897>>  fHead = 337.0912  BREAK
//		CASE 2 vPos = <<-692.4150, -976.0198, 19.3905>>   fHead = 94.6316   BREAK
//		CASE 3 vPos = <<-701.7781, -980.4039, 19.3899>>  fHead = 341.1090  BREAK

//		CASE 0 vPos = <<-209.6734, 311.7420, 95.9469>> fHead = 193.6164  BREAK
//		CASE 1 vPos = <<-205.2586, 311.8049, 95.9467>>	  fHead = 190.6454	  	  BREAK
//		CASE 2 vPos =  <<-209.0489, 303.9464, 95.9455>>  fHead = 349.9629    BREAK
	//	CASE 3 vPos =  <<-205.5157, 304.1146, 95.9455>>  fHead = 7.6074BREAK
	
		CASE 0 vPos = <<-202.0274, 316.3006, 95.9455>>  fHead = 209.3976  BREAK
		CASE 1 vPos = <<-203.2378, 315.3547, 95.9455>>  fHead = 242.1485  BREAK
		CASE 2 vPos =  <<-203.0393, 316.6364, 95.9455>> fHead = 212.1098  BREAK
	ENDSWITCH


ENDPROC
FUNC BOOL CREATE_AMBIENT_RACE_CAR_DRIVERS(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	INT i
//	REL_GROUP_HASH relPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
	
	
	VECTOR vPos
	FLOAT fHead
	
	FOR i = 0 TO MAX_TRIGGER_TUT_VEH - 1
		IF NOT DOES_ENTITY_EXIST(fmTrigger.pedRacers[i])
			MODEL_NAMES mPed = GET_AMBIENT_RACE_CAR_DRIVER_MODEL(i)
			REQUEST_MODEL(mPed)
			IF HAS_MODEL_LOADED(mPed)
				IF IS_VEHICLE_DRIVEABLE(fmTrigger.vehRacers[i])
					GET_AMBIENT_RACE_CAR_DRIVER_POSTION(i, vPos, fHead)
					REL_GROUP_HASH relPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
					IF i = 0
						fmTrigger.pedRacers[i] = CREATE_PED_INSIDE_VEHICLE(fmTrigger.vehRacers[i], PEDTYPE_CIVMALE, mPed, DEFAULT, FALSE, FALSE)
						SET_ENTITY_INVINCIBLE(fmTrigger.pedRacers[i], TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(fmTrigger.pedRacers[i], TRUE)
						SET_ENTITY_PROOFS(fmTrigger.pedRacers[0], TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
						SET_PED_RELATIONSHIP_GROUP_HASH(fmTrigger.pedRacers[0], relPlayer)
					ELSE
						fmTrigger.pedRacers[i] = CREATE_PED(PEDTYPE_MISSION, mPed, vPos, fHead, FALSE, FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(fmTrigger.pedRacers[i], rgFM_AiDislike) //rgFM_AiLike
					ENDIF
					
				//	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(fmTrigger.pedRacers[i], TRUE)
					SET_PED_RANDOM_COMPONENT_VARIATION(fmTrigger.pedRacers[i])
					//SET_VEHICLE_ENGINE_ON(fmTrigger.vehRacers[i], TRUE, TRUE)
				//	SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(fmTrigger.pedRacers[i], FALSE, relPlayer)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(fmTrigger.pedRacers[i], PLAYER_PED_ID(), -1)
					ENDIF
					#IF IS_DEBUG_BUILD 
						PRINT_TRIGGER_STRING_INT("[CREATE_AMBIENT_RACE_CAR_DRIVERS] Created driver ", i) 	
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR i = 0 TO MAX_TRIGGER_TUT_VEH - 1
		IF NOT DOES_ENTITY_EXIST(fmTrigger.pedRacers[i])
			RETURN FALSE
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

PROC CLEANUP_AMBIENT_RACE_CARS(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	INT i
	
	FOR i = 0 TO MAX_TRIGGER_TUT_VEH - 1
		ENTITY_INDEX ent
		IF DOES_ENTITY_EXIST(fmTrigger.pedRacers[i])
			ent = fmTrigger.pedRacers[i]
			DELETE_ENTITY(ent)
		ENDIF
						
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_AMBIENT_RACE_CAR_DRIVER_MODEL(i))
		
	ENDFOR
	
	FOR i = 0 TO MAX_TRIGGER_TUT_VEH - 1
		ENTITY_INDEX ent
		IF DOES_ENTITY_EXIST(fmTrigger.vehRacers[i])
			ent = fmTrigger.vehRacers[i]
			DELETE_ENTITY(ent)
		ENDIF
						
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_AMBIENT_RACE_CAR_MODEL(i))
		
	ENDFOR
	
	REMOVE_ANIM_DICT("mp_intro_seq@ig_3_car")
	
	CLEAR_BIT(fmTrigger.iBitset2, biFmt2_CreateAmbRacers)
ENDPROC

FUNC BOOL HAS_ANY_AMBIENT_RACE_DRIVER_BEEN_DAMAGED_BY_PLAYER(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	INT i
	FOR i = 0 TO MAX_TRIGGER_TUT_VEH - 1
		IF DOES_ENTITY_EXIST(fmTrigger.pedRacers[i])
			IF NOT IS_PED_INJURED(fmTrigger.pedRacers[i])
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(fmTrigger.pedRacers[i], PLAYER_PED_ID(), TRUE)
					PRINTLN("[dsw] [HAS_ANY_AMBIENT_RACE_DRIVER_BEEN_DAMAGED_BY_PLAYER] True - damaged")
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("[dsw] [HAS_ANY_AMBIENT_RACE_DRIVER_BEEN_DAMAGED_BY_PLAYER] True - Injured")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC
PROC MANAGE_AMBIENT_RACE_CARS(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	INT i
	IF IS_BIT_SET(fmTrigger.iBitset2, biFmt2_CreateAmbRacers)
		IF CREATE_AMBIENT_RACE_CARS(fmTrigger)
			IF CREATE_AMBIENT_RACE_CAR_DRIVERS(fmTrigger)
				CLEAR_BIT(fmTrigger.iBitset2, biFmt2_CreateAmbRacers)
			ENDIF
		ENDIF
	ENDIF
//	AI_BLIP_STRUCT blipRacers[MAX_TRIGGER_TUT_VEH]
	IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_AttackPlayer)
		IF HAS_ANY_AMBIENT_RACE_DRIVER_BEEN_DAMAGED_BY_PLAYER(fmTrigger)
			FOR i = 1 TO MAX_TRIGGER_TUT_VEH - 1
				IF DOES_ENTITY_EXIST(fmTrigger.pedRacers[i])
					IF NOT IS_PED_INJURED(fmTrigger.pedRacers[i])
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						//	TASK_COMBAT_PED(fmTrigger.pedRacers[i], PLAYER_PED_ID())
							
							PRINTLN("[dsw] [MANAGE_AMBIENT_RACE_CARS] this ped is being told to attack the player ", i)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			SET_BIT(fmTrigger.iBitset2, biFmt2_AttackPlayer)
		ENDIF
	ENDIF
	
	SEQUENCE_INDEX seq
	VECTOR vPos
	FLOAT fHead
	IF GET_LOCAL_PLAYER_CORONA_POS_STATE() > 0
		IF IS_BIT_SET(fmTrigger.iBitset2, biFmt2_AttackPlayer)
			IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFMt2_AmbientRacerCorona)
				FOR i = 1 TO MAX_TRIGGER_TUT_VEH - 1
					IF DOES_ENTITY_EXIST(fmTrigger.pedRacers[i])
						IF NOT IS_PED_INJURED(fmTrigger.pedRacers[i])
							GET_AMBIENT_RACE_CAR_DRIVER_POSTION(i, vPos, fHead)
							OPEN_SEQUENCE_TASK(seq)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPos, PEDMOVE_WALK)
								TASK_TURN_PED_TO_FACE_COORD(NULL, <<-210.4620, 307.1358, 96.8026>>)
							CLOSE_SEQUENCE_TASK(seq)
								
							CLEAR_PED_TASKS(fmTrigger.pedRacers[i])	
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(fmTrigger.pedRacers[i], TRUE)
							TASK_PERFORM_SEQUENCE(fmTrigger.pedRacers[i], seq)
							
							PRINTLN("[dsw] [MANAGE_AMBIENT_RACE_CARS] this ped is being told to return to their initial pos ", i)
							
						ENDIF
					ENDIF
				ENDFOR
				
				SET_BIT(fmTrigger.iBitset2, biFMt2_AmbientRacerCorona)
				
			ENDIF
		ENDIF
	ENDIF
	
//	FOR i = 1 TO MAX_TRIGGER_TUT_VEH - 1
//		IF DOES_ENTITY_EXIST(fmTrigger.pedRacers[i])
//			IF NOT IS_PED_INJURED(fmTrigger.pedRacers[i])
//				UPDATE_AI_PED_BLIP(fmTrigger.pedRacers[i], blipRacers[i])
//			ENDIF
//		ENDIF
//	ENDFOR
ENDPROC

FUNC STRING GET_CONV_STRING_FOR_NEAR_LAMAR_DIALOGUE()
	
	STRING sText
	IF IS_PLAYER_MALE(PLAYER_ID())
		sText = "FM_RCBM"
	ELSE
		sText = "FM_RCBF"
	ENDIF
	
	RETURN sText 
ENDFUNC

PROC MAINTAIN_PLAYER_NEAR_LAMAR_DIALOGUE(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	// NEEDS A TIMER!
	
	//VECTOR vLam, vPlayer
	SEQUENCE_INDEX seq
	

	IF NOT IS_PED_INJURED(fmTrigger.pedLamar)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			//	vLam = GET_ENTITY_COORDS(fmTrigger.pedLamar)
			//	vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) 
				IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_WalkedAwayFromLam)
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-204.724716,299.263367,94.695518>>, <<-205.246490,317.749115,98.897423>>, 10.375000)
						SET_BIT(fmTrigger.iBitset2, biFmt2_WalkedAwayFromLam)
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[dsw] [MAINTAIN_PLAYER_NEAR_LAMAR_DIALOGUE] Set biFmt2_WalkedAwayFromLam") #ENDIF
					ENDIF
				ELSE
					IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-204.724716,299.263367,94.695518>>, <<-205.246490,317.749115,98.897423>>, 10.375000)
						IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-194.603256,297.555878,94.799072>>, <<-193.209076,318.511292,100.374466>>, 17.750000)
							IF NOT HAS_NET_TIMER_STARTED(fmTrigger.timeLamarDialogue)
							OR HAS_NET_TIMER_EXPIRED(fmTrigger.timeLamarDialogue, 60000)
								IF CREATE_CONVERSATION(fmTrigger.speechFmTriggerTut, "FM_1AU", "FM_LAMID2", CONV_PRIORITY_MEDIUM)
									OPEN_SEQUENCE_TASK(seq)
										TASK_PLAY_ANIM(NULL, "mp_intro_seq@ig_2_lamar_car_idle", "idle_a", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING)
										TASK_PLAY_ANIM(NULL, "mp_intro_seq@ig_2_lamar_car_idle", "loop", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(fmTrigger.pedLamar, seq)
									CLEAR_SEQUENCE_TASK(seq)
									RESET_NET_TIMER(fmTrigger.timeLamarDialogue)
									START_NET_TIMER(fmTrigger.timeLamarDialogue)
									#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[dsw] [MAINTAIN_PLAYER_NEAR_LAMAR_DIALOGUE] Doing walked away dialogue") #ENDIF
								ENDIF
							ENDIF
						ELSE
							IF HAS_NET_TIMER_STARTED(fmTrigger.timeLamarDialogue)
								RESET_NET_TIMER(fmTrigger.timeLamarDialogue)
								#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[dsw] [MAINTAIN_PLAYER_NEAR_LAMAR_DIALOGUE] Reset timeLamarDialogue as approaching corona") #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				

			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC MODEL_NAMES GET_LAMAR_VEHICLE_MODEL_FOR_TUTORIAL()
	RETURN EMPEROR
ENDFUNC

PROC REMOVE_TUTORIAL_SPLASH(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	#IF IS_DEBUG_BUILD PRINTLN("[REMOVE_TUTORIAL_SPLASH] Called....") #ENDIF
	g_bSplashScreenBigMethodCalledJoining = FALSE

	g_IsNewsFeedDisplaying = FALSE
	
	IF HAS_SCALEFORM_MOVIE_LOADED(fmTrigger.scaleSplash)
		SC_TRANSITION_NEWS_END()
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(fmTrigger.scaleSplash)
	ENDIF
	SET_JOINING_GAMETIPS_FEED_DISPLAYING(FALSE)
	THEFEED_FLUSH_QUEUE()
	 g_b_ReapplyStickySaveFailedFeed = FALSE
	THEFEED_REPORT_LOGO_OFF()

ENDPROC

PROC DO_SPLASH_SCREEN_TEST(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	SWITCH fmTrigger.iSplashProg
		CASE 0
			IF NOT GET_IS_LOADING_SCREEN_ACTIVE()
				IF NOT HAS_SCALEFORM_MOVIE_LOADED(fmTrigger.scaleSplash) 
							
					fmTrigger.scaleSplash = REQUEST_SCALEFORM_MOVIE("GTAV_ONLINE")
				//	THEFEED_REPORT_LOGO_ON()
					
					fmTrigger.iSplashProg++
				ENDIF
			ELSE
				PRINTLN("DO_SPLASH_SCREEN_TEST - GET_IS_LOADING_SCREEN_ACTIVE() = TRUE, cannot load movie GTAV_ONLINE.")
			ENDIF
		BREAK
		 
		CASE 1
			IF g_IsNewsFeedDisplaying = FALSE
				SET_JOINING_GAMETIPS_FEED_DISPLAYING(TRUE)
				
			ENDIF
			
			IF HAS_SCALEFORM_MOVIE_LOADED(fmTrigger.scaleSplash)
				DRAW_SCALEFORM_MOVIE_FULLSCREEN(fmTrigger.scaleSplash, 255, 255, 255, 255)
				
				IF g_bSplashScreenBigMethodCalledJoining = FALSE
				
					
					// Les and Andy want only one item per showing of the feed, no cycling. Passing 0 in here does this.
					IF SC_TRANSITION_NEWS_SHOW_TIMED(fmTrigger.scaleSplash, 0)
						SET_JOINING_GAMETIPS_FEED_DISPLAYING(FALSE)
						THEFEED_FLUSH_QUEUE()
						 g_b_ReapplyStickySaveFailedFeed = FALSE
						//SC_TRANSITION_NEWS_SHOW_NEXT_ITEM() Removed for 1779794
						g_IsNewsFeedDisplaying = TRUE

						g_bSplashScreenBigMethodCalledJoining = TRUE
					ENDIF
						
					
					
				ENDIF
				
				IF g_bSplashScreenBigMethodCalledJoining
					IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_TransitionSessionNonResetVars.sPostMissionCleanupData.g_aBigSplashScreenTimer, 15000)
						//SC_TRANSITION_NEWS_SHOW_NEXT_ITEM() Removed for 1779794
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Scaleform not loaded!") #ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_DRIVE_CAR_INTO_CORONA(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	#IF IS_DEBUG_BUILD
		//PRINT_TRIGGER_STRING("[MAINTAIN_DRIVE_CAR_INTO_CORONA] Running...")
	#ENDIF
	BOOL bInCar
	VECTOR vTemp
	SEQUENCE_INDEX seq

	IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_SetCloneVisible)
		IF DOES_CAM_EXIST(g_sTransitionSessionData.ciCam)
			
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				IF NOT IS_PED_INJURED(fmTrigger.pedClone)
					vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
					vTemp.z = 95.9464
					IF vTemp.z = 0.0 ENDIF
					SET_ENTITY_COORDS(fmTrigger.pedClone, <<-209.867, 306.969, 95.9464>>) //<<-209.2859, 305.5449, 95.9464>>
					SET_ENTITY_HEADING(fmTrigger.pedClone, 12.500 ) // 28.0764
					SET_ENTITY_VISIBLE(fmTrigger.pedClone, TRUE)
					SET_BIT(fmTrigger.iBitset2, biFmt2_SetCloneVisible)
					CLEAR_AREA_OF_VEHICLES(<<-210.4620, 307.1358, 96.8026>>,4.0)
					#IF IS_DEBUG_BUILD
						PRINT_TRIGGER_STRING_INT("[MAINTAIN_DRIVE_CAR_INTO_CORONA] Set biFmt2_SetCloneVisible. fmTrigger.iDriveIntoCoronaProg = ", fmTrigger.iDriveIntoCoronaProg)
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_FM_RACE_TUT_BEEN_DONE()
		SWITCH fmTrigger.iDriveIntoCoronaProg
			CASE 0
				IF IS_PLAYER_IN_CORONA()
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF IS_VEHICLE_DRIVEABLE(fmTrigger.vehRacers[0])
							IF HAS_PLAYER_ACTIVATED_TUTORIAL_CORONA_CAMERA(PLAYER_ID())
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), fmTrigger.vehRacers[0], -1)
								ENDIF
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(fmTrigger.vehRacers[0])
									SET_VEHICLE_DOORS_LOCKED(fmTrigger.vehRacers[0], VEHICLELOCK_UNLOCKED)
									FREEZE_ENTITY_POSITION(fmTrigger.vehRacers[0], FALSE)
									START_PLAYBACK_RECORDED_VEHICLE(fmTrigger.vehRacers[0], 1, "FM_Tut")
									PAUSE_PLAYBACK_RECORDED_VEHICLE(fmTrigger.vehRacers[0])
									#IF IS_DEBUG_BUILD
										PRINT_TRIGGER_STRING("[MAINTAIN_DRIVE_CAR_INTO_CORONA] Started vehicle rec")
									#ENDIF
								ENDIF
								
								IF NOT IS_PED_INJURED(fmTrigger.pedRacers[0])
									IF NOT IS_PED_SITTING_IN_VEHICLE(fmTrigger.pedRacers[0], fmTrigger.vehRacers[0])
										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK
											TASK_ENTER_VEHICLE(fmTrigger.pedRacers[0], fmTrigger.vehRacers[0], 1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED )
											#IF IS_DEBUG_BUILD
												PRINT_TRIGGER_STRING("[MAINTAIN_DRIVE_CAR_INTO_CORONA] Giving TASK_ENTER_VEHICLE")
											#ENDIF
										ENDIF
									ELSE
										bInCar = TRUE
										
									ENDIF
								ENDIF
								
								
								
								
								IF bInCar
									fmTrigger.iDriveIntoCoronaProg++
									#IF IS_DEBUG_BUILD
										PRINT_TRIGGER_STRING_INT("[MAINTAIN_DRIVE_CAR_INTO_CORONA] iDriveIntoCoronaProg = ", fmTrigger.iDriveIntoCoronaProg)
									#ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
			//	IF GET_CORONA_STATUS() >= CORONA_STATUS_INTRO
					
					IF IS_VEHICLE_DRIVEABLE(fmTrigger.vehRacers[0])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(fmTrigger.vehRacers[0])
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(fmTrigger.vehRacers[0])
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_FLOAT("Rec time... ", GET_TIME_POSITION_IN_RECORDING(fmTrigger.vehRacers[0])) #ENDIF
							SET_LOCAL_PLAYER_STARTED_TUTORIAL_CORONA_PLAYBACK(TRUE)
							RESET_NET_TIMER(fmTrigger.timeLocalTImeOut)
							fmTrigger.iDriveIntoCoronaProg++
							#IF IS_DEBUG_BUILD
								PRINT_TRIGGER_STRING_INT("[MAINTAIN_DRIVE_CAR_INTO_CORONA] iDriveIntoCoronaProg = ", fmTrigger.iDriveIntoCoronaProg)
							#ENDIF
						ENDIF
					ENDIF
			//	ENDIF
			BREAK
			
			CASE 2
				REQUEST_ANIM_DICT("mp_intro_seq@ig_3_car")
				IF IS_VEHICLE_DRIVEABLE(fmTrigger.vehRacers[0])
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(fmTrigger.vehRacers[0])
						IF NOT HAS_NET_TIMER_STARTED(fmTrigger.timeLocalTImeOut)
							START_NET_TIMER(fmTrigger.timeLocalTImeOut)
							#IF IS_DEBUG_BUILD
								PRINT_TRIGGER_STRING("Started timeLocalTImeOut")
							#ENDIF
						ELSE
							IF HAS_NET_TIMER_EXPIRED(fmTrigger.timeLocalTImeOut, 1000)
								IF NOT IS_PED_INJURED(fmTrigger.pedRacers[0])
									IF HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_3_car")
										SET_LOCAL_PLAYER_FINISHED_TUTORIAL_CORONA_PLAYBACK(TRUE)
										
										VECTOR scenePosition 
										VECTOR sceneRotation

									//	FLOAT fCarHeading

										scenePosition = << 0.000, 0.000, 0.0>> //<< -211.524, 308.305, 96.492 >>
										sceneRotation = << 0.000, 0.000, 0.0>>
										
									//	fCarHeading = GET_ENTITY_HEADING(fmTrigger.vehRacers[0]) 
									//	sceneRotation.z = fCarHeading// GET_ENTITY_HEADING(fmTrigger.pedRacers[0])
										
										g_iMyTutorialSceneId= CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation) //fmTrigger.iSceneId 
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(g_iMyTutorialSceneId, fmTrigger.vehRacers[0], GET_ENTITY_BONE_INDEX_BY_NAME(fmTrigger.vehRacers[0],"seat_dside_f"))
										
										TASK_SYNCHRONIZED_SCENE(fmTrigger.pedRacers[0], g_iMyTutorialSceneId, "mp_intro_seq@ig_3_car", "driver", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_TAG_SYNC_OUT )
										
										PLAY_SYNCHRONIZED_ENTITY_ANIM(fmTrigger.vehRacers[0], 
															g_iMyTutorialSceneId,
															"driver_car", 
															"mp_intro_seq@ig_3_car",
															INSTANT_BLEND_IN, 
															INSTANT_BLEND_OUT, 
															ENUM_TO_INT(SYNCED_SCENE_BLOCK_MOVER_UPDATE))
								//		PLAY_ENTITY_ANIM(fmTrigger.vehRacers[0], "driver_car", "mp_intro_seq@ig_3_car", INSTANT_BLEND_IN, FALSE, FALSE )
										
										#IF IS_DEBUG_BUILD
											PRINT_TRIGGER_STRING("Starting synched scene....")
											PRINT_TRIGGER_STRING_VECTOR("scenePosition... ", scenePosition)
											PRINT_TRIGGER_STRING_VECTOR("sceneRotation... ", sceneRotation)
											PRINT_TRIGGER_STRING("")
											PRINT_TRIGGER_STRING_VECTOR("Car coords.... ", GET_ENTITY_COORDS(fmTrigger.vehRacers[0]))
											PRINT_TRIGGER_STRING_FLOAT("Car heading... ", GET_ENTITY_HEADING(fmTrigger.vehRacers[0]))
											PRINT_TRIGGER_STRING("")
											PRINT_TRIGGER_STRING_VECTOR("Ped coords...", GET_ENTITY_COORDS(fmTrigger.pedRacers[0]))
											PRINT_TRIGGER_STRING_FLOAT("Ped heading...", GET_ENTITY_HEADING(fmTrigger.pedRacers[0]))
											PRINT_TRIGGER_STRING("")
										#ENDIF
										
//										OPEN_SEQUENCE_TASK(seq)
//											TASK_CLEAR_LOOK_AT(NULL)
//											TASK_PLAY_ANIM(NULL, "mp_intro_seq@ig_3_car", "player_intro")
//											TASK_PLAY_ANIM(NULL, "mp_intro_seq@ig_3_car", "player_idle", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING)
//										CLOSE_SEQUENCE_TASK(seq)
//										
//										IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//											TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
//										ENDIF
//
//										CLEAR_SEQUENCE_TASK(seq)
										RESET_NET_TIMER(fmTrigger.timeLocalTImeOut)
										START_NET_TIMER(fmTrigger.timeLocalTImeOut)
										fmTrigger.iDriveIntoCoronaProg++
										#IF IS_DEBUG_BUILD
											PRINT_TRIGGER_STRING_INT("[MAINTAIN_DRIVE_CAR_INTO_CORONA] iDriveIntoCoronaProg = ", fmTrigger.iDriveIntoCoronaProg)
										#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK	
			
			CASE 3
				IF IS_VEHICLE_DRIVEABLE(fmTrigger.vehRacers[0])
					IF NOT IS_PED_INJURED(fmTrigger.pedRacers[0])
						
						fmTrigger.iDriveIntoCoronaProg++
						#IF IS_DEBUG_BUILD
							PRINT_TRIGGER_STRING_INT("[MAINTAIN_DRIVE_CAR_INTO_CORONA] iDriveIntoCoronaProg = ", fmTrigger.iDriveIntoCoronaProg)
						#ENDIF
					ENDIF
				ENDIF
				
			BREAK
			CASE 4
				IF NOT IS_PED_INJURED(fmTrigger.pedRacers[0])
					IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iMyTutorialSceneId)
						IF GET_SYNCHRONIZED_SCENE_PHASE(g_iMyTutorialSceneId) > 0.99
							FORCE_PED_MOTION_STATE(fmTrigger.pedRacers[0], MS_ON_FOOT_WALK)
							CLEAR_PED_TASKS(fmTrigger.pedRacers[0])

							FORCE_PED_AI_AND_ANIMATION_UPDATE(fmTrigger.pedRacers[0])
							
							
							OPEN_SEQUENCE_TASK(seq)
								
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,   <<-202.3071, 314.5768, 95.9455>>, PEDMOVE_WALK)
								TASK_TURN_PED_TO_FACE_COORD(NULL, <<-210.4620, 307.1358, 96.8026>>)
							CLOSE_SEQUENCE_TASK(seq)
							SET_PED_CONFIG_FLAG(fmTrigger.pedRacers[0], PCF_PedIgnoresAnimInterruptEvents, FALSE)
							TASK_PERFORM_SEQUENCE(fmTrigger.pedRacers[0], seq)
							CLEAR_SEQUENCE_TASK(seq)
							fmTrigger.iDriveIntoCoronaProg++
							#IF IS_DEBUG_BUILD
								PRINT_TRIGGER_STRING_INT("[MAINTAIN_DRIVE_CAR_INTO_CORONA] iDriveIntoCoronaProg = ", fmTrigger.iDriveIntoCoronaProg)
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	
	
	IF NOT IS_LOCAL_PLAYER_READY_FOR_TUTORIAL_CORONA_SWOOP()
		IF HAS_NET_TIMER_STARTED(fmTrigger.timeLocalTImeOut)
			IF HAS_NET_TIMER_EXPIRED(fmTrigger.timeLocalTImeOut, 10750)
				SET_LOCAL_PLAYER_READY_TUTORIAL_CORONA_SWOOP(TRUE)
			ENDIF
		ENDIF
	ENDIF
	
//	IF HAS_LOCAL_PLAYER_DONE_RACE_TUTORIAL_CORONA_SWOOP()
//		DO_SPLASH_SCREEN_TEST(fmTrigger)
//	ENDIF
ENDPROC

FUNC BOOL ARE_PLAYER_AND_LAMAR_SITTING_IN_VEHICLE(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	IF IS_VEHICLE_DRIVEABLE(fmTrigger.vehLamar)
		IF NOT IS_PED_INJURED(fmTrigger.pedLamar)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), fmtrigger.vehLamar)
					IF IS_PED_SITTING_IN_VEHICLE(fmtrigger.pedLamar, fmtrigger.vehLamar)	
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
						
		ENDIF
	ENDIF
	RETURN FALSE
	
ENDFUNC


PROC ADD_FAKE_BLIP_FOR_RACE(FM_TRIGGER_INTRO_STRUCT &fmTrigger, VECTOR vLoc)
	IF NOT DOES_BLIP_EXIST(fmtrigger.blipTempRace)
		fmtrigger.blipTempRace = ADD_BLIP_FOR_COORD( vLoc)
	//	SET_BLIP_SCALE(fmtrigger.blipTempRace, 0.0)
		SET_BLIP_DISPLAY(fmtrigger.blipTempRace,DISPLAY_NOTHING)
		SET_BLIP_ROUTE(fmtrigger.blipTempRace, TRUE)
		SET_BLIP_COLOUR(fmtrigger.blipTempRace, BLIP_COLOUR_BLUE)
	//	SET_BIT(fmTrigger.iBitset, biFmT_DoFloatingHelp)
	ENDIF
ENDPROC

FUNC BOOL CREATE_LAMAR_VEHICLE_FOR_TUTORIAL(FM_TRIGGER_INTRO_STRUCT &fmTrigger, VECTOR vCoords, FLOAT fHeading)
	MODEL_NAMES mLamarCar = GET_LAMAR_VEHICLE_MODEL_FOR_TUTORIAL()
	REQUEST_MODEL(GET_LAMAR_VEHICLE_MODEL_FOR_TUTORIAL())
	
	IF NOT DOES_ENTITY_EXIST(fmTrigger.vehLamar)
		IF HAS_MODEL_LOADED(mLamarCar)
			fmTrigger.vehLamar = CREATE_VEHICLE(mLamarCar, vCoords, fHeading, FALSE, FALSE)
			SET_VEHICLE_COLOURS(fmTrigger.vehLamar, 0, 3)
			SET_VEHICLE_EXTRA_COLOURS(fmTrigger.vehLamar, 0, 156)
			SET_MODEL_AS_NO_LONGER_NEEDED(mLamarCar)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(fmTrigger.vehLamar)
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(fmTrigger.vehLamar)
		IF NOT HAVE_VEHICLE_MODS_STREAMED_IN(fmTrigger.vehLamar)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_WALK_TO_CORONA_LAMAR_MODELS_LOADED(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_OutsideRaceArea)
		IF NOT HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_LAMAR))
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(GET_LAMAR_VEHICLE_MODEL_FOR_TUTORIAL())
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(PROP_PHONE_ING)
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_2_lamar_car_idle")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC CLEANUP_WALK_TO_CORONA_CAM(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	IF DOES_CAM_EXIST(fmTrigger.cutsceneCam)
		IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			SET_CAM_ACTIVE(fmTrigger.cutsceneCam, FALSE)
			DESTROY_CAM(fmTrigger.cutsceneCam)
			IF NOT IS_PLAYER_IN_CORONA()
			//	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] [CLEANUP_WALK_TO_CORONA_CAM] Turned on control ") #ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] [CLEANUP_WALK_TO_CORONA_CAM] Cleaned up cam... ") #ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SCALEFORM_SETUP_CREDIT_BLOCK(STRING strName, FLOAT fX, FLOAT fY, STRING strAlign, FLOAT fFadeInDuration, FLOAT fFadeOutDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(g_sfMPCredits, "SETUP_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fX)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fY)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strAlign)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeInDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeOutDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK(STRING strName, STRING strRole, FLOAT fXOffset, STRING strColour, BOOL bUseLiteralString = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(g_sfMPCredits, "ADD_ROLE_TO_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strRole)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fXOffset)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strColour)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseLiteralString)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK(STRING strName, STRING strNames, FLOAT fXOffset, STRING strDelimiter, BOOL bUseLiteralString = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(g_sfMPCredits, "ADD_NAMES_TO_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strNames)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fXOffset)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strDelimiter)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseLiteralString)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SHOW_CREDIT_BLOCK(STRING strName, FLOAT fStepDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(g_sfMPCredits, "SHOW_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStepDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_HIDE_CREDIT_BLOCK(STRING strName, FLOAT fStepDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(g_sfMPCredits, "HIDE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStepDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC BOOL DO_WALK_TO_RACE_CORONA_CUT_V2(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
//	SEQUENCE_INDEX seq
//	VECTOR vWalkTo
	INT iTimeToWait
	
	
	SWITCH fmTrigger.iBeforeRaceCutProg
		CASE 0
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
					
				IF NOT IS_BIT_SET(fmTrigger.iBitset, biFMt_SetupInitalCam)
					IF HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_ID())
						fmTrigger.cutsceneCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 	
						SET_CAM_ACTIVE(fmTrigger.cutsceneCam, TRUE)
						
						//load credit
						IF NOT HAS_SCALEFORM_MOVIE_LOADED(g_sfMPCredits)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === g_sfMPCredits = REQUEST_SCALEFORM_MOVIE(OPENING_CREDITS)")
							#ENDIF
							g_bMPCreditsOn = TRUE
							g_sfMPCredits = REQUEST_SCALEFORM_MOVIE("OPENING_CREDITS")
						ELSE
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === g_sfMPCredits already loaded")
							#ENDIF
						ENDIF
						
					//	SET_CAM_PARAMS(fmTrigger.cutsceneCam, <<-198.3618, 299.8370, 97.4311>>, <<-6.2830, -0.0000, 53.2264>>,  50.0000)
					
						SET_CAM_PARAMS(fmTrigger.cutsceneCam, <<-199.2059, 301.0296, 97.5090>>, <<-5.3656, -0.0300, 58.4675>>, 50.0000)
						
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						SET_BIT(fmTrigger.iBitset, biFMt_SetupInitalCam)		
						SET_BIT(fmTrigger.iBitset, biFMt_HideHudAndRadar)
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] Setting up first cam because intro cut said so. ") #ENDIF
					ENDIF
				ENDIF

				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-217.952942,308.709259,95.383018>>, <<-190.133026,308.546814,99.570518>>, 22.625000)
				//	IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
					IF IS_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL() // Set by Dave G
					//	IF IS_PLAYER_OK_TO_START_MP_CUTSCENE()
							IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchTutorialFromIntroCut)
									CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchTutorialFromIntroCut)
									#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] Cleared biTrigTut_LaunchTutorialFromIntroCut") #ENDIF
								ENDIF
								Clear_Any_Objective_Text_From_This_Script()
							
								
								SET_BIT(fmtrigger.iBitset, biFMt_DoWaitingScreenHelp)

								
								
								RESET_NET_TIMER(fmTrigger.timeTrigIntro)
								START_NET_TIMER(fmTrigger.timeTrigIntro)
								
								IF DOES_CAM_EXIST(fmTrigger.cutsceneCam)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === PRE HAS_SCALEFORM_MOVIE_LOADED(g_sfMPCredits)")
									#ENDIF
									IF HAS_SCALEFORM_MOVIE_LOADED(g_sfMPCredits)
										//add credit
										#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] Director Credit") #ENDIF
										SCALEFORM_SETUP_CREDIT_BLOCK("director", 0.000, 450.000, "left", 0.33, 0.33)
										SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("director", "Designer & Director", 100.000, "HUD_COLOUR_NET_PLAYER3")
										SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("director", "LESLIE BENZIES", 175.000, "|")
										SCALEFORM_SHOW_CREDIT_BLOCK("director", 0.2)
										g_bDirectorMPCreditCleared = FALSE
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === Created credit : director")
										#ENDIF
									ENDIF
									
									SET_CAM_PARAMS(fmTrigger.cutsceneCam,<<-199.4263, 301.5534, 97.4658>>, <<-4.8559, -0.0300, 58.4675>>,  50.0000, 13000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
									SHAKE_CAM(fmTrigger.cutsceneCam,"Hand_shake", 0.2)
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								ENDIF
								fmTrigger.iBeforeRaceCutProg = 1
								#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[DO_WALK_TO_RACE_CORONA_CUT] fmTrigger.iBeforeRaceCutProg = ", fmTrigger.iBeforeRaceCutProg) #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] Player is still ragdoll... ") #ENDIF
							ENDIF
					//	ENDIF
					ELSE
						#IF IS_DEBUG_BUILD 
							IF HAS_FM_INTRO_MISSION_BEEN_DONE()
								PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] In angled area, but IS_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL not set!") 
							ENDIF
						#ENDIF
						IF NOT IS_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL()
							IF HAS_FM_INTRO_MISSION_BEEN_DONE()
							
								SET_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF HAS_FM_INTRO_MISSION_BEEN_DONE()
						ADD_FAKE_BLIP_FOR_RACE(fmtrigger, <<-210.3399,307.0503,95.9465>>)
						IF NOT IS_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL()
							SET_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL(TRUE)
						ENDIF
							
						IF NOT Is_This_The_Current_Objective_Text("FM_RTU_MAR")
						AND NOT Is_This_The_Current_Objective_Text("FM_RTU_GRA")
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-210.3399,307.0503,95.9465>>) < 5625 // 75m
								Print_Objective_Text("FM_RTU_MAR") // Walk into the Trigger to start the Race.
							ELSE
								Print_Objective_Text("FM_RTU_GRA") // Go to the Race ~b~Trigger.
							ENDIF
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] Doing walk into trigger objective as not in angled area") #ENDIF
						ENDIF
						IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_OutsideRaceArea)
							SET_BIT(fmTrigger.iBitset2, biFmt2_OutsideRaceArea)
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] Set biFmt2_OutsideRaceArea") #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] Not in angled area, but intro miss not done!") #ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_NET_TIMER_EXPIRED(fmTrigger.timeTrigIntro, 5000) //
			OR NOT HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_ID())
			#IF IS_DEBUG_BUILD	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
			
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_RIN")
				OR NOT HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_ID())
				#IF IS_DEBUG_BUILD	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())

						IF DOES_CAM_EXIST(fmTrigger.cutsceneCam)
							
							
						//	RENDER_SCRIPT_CAMS(FALSE, FALSE)
						//	RENDER_SCRIPT_CAMS(FALSE, TRUE, 2000, FALSE, TRUE)
						//	SET_CAM_ACTIVE(fmTrigger.cutsceneCam, FALSE)
						//	DESTROY_CAM(fmTrigger.cutsceneCam)
							CLEAR_BIT(fmTrigger.iBitset, biFMt_HideHudAndRadar)
						//	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							CLEANUP_MP_CUTSCENE(FALSE, FALSE)
							
							//cleanup scaleform
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_sfMPCredits)")
							#ENDIF
							SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_sfMPCredits)
							g_bMPCreditsOn = FALSE
							g_bMPCreditsLoaded = FALSE
							g_bMPCreditsIntroDraw = FALSE
							g_bDirectorMPCreditCleared = FALSE
							
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), <<-210.3399,307.0503,95.9465>>, PEDMOVE_WALK)
							IF NOT HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_ID())
								FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							ENDIF
						ENDIF
	//					IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_OutsideRaceArea)
	//						REQUEST_MODEL(GET_LAMAR_VEHICLE_MODEL_FOR_TUTORIAL())
	//						REQUEST_NPC_PED_MODEL(CHAR_LAMAR)
	//						REQUEST_MODEL(PROP_PHONE_ING)
	//						REQUEST_ANIM_DICT("mp_intro_seq@ig_2_lamar_car_idle")
	//					ENDIF

						RESET_NET_TIMER(fmTrigger.timeTrigIntro)
						START_NET_TIMER(fmTrigger.timeTrigIntro)
						
						IF NOT HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_ID())
							fmTrigger.iBeforeRaceCutProg = 99
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[DO_WALK_TO_RACE_CORONA_CUT] fmTrigger.iBeforeRaceCutProg = ", fmTrigger.iBeforeRaceCutProg) #ENDIF
						ELSE
							fmTrigger.iBeforeRaceCutProg = 2
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[DO_WALK_TO_RACE_CORONA_CUT] fmTrigger.iBeforeRaceCutProg = ", fmTrigger.iBeforeRaceCutProg) #ENDIF
						
						ENDIF
							
								
					ENDIF
				ENDIF
			ELIF HAS_NET_TIMER_EXPIRED(fmTrigger.timeTrigIntro, 3300)
				IF NOT g_bDirectorMPCreditCleared
					//clear credit
					SCALEFORM_HIDE_CREDIT_BLOCK("director", 0.2)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === Cleanup credit : director")
					#ENDIF
					g_bDirectorMPCreditCleared = TRUE
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF HAS_NET_TIMER_EXPIRED(fmTrigger.timeTrigIntro, 500)
					RENDER_SCRIPT_CAMS(FALSE, TRUE,4000, FALSE, TRUE)
					fmTrigger.iBeforeRaceCutProg = 99
				ENDIF

			ENDIF
		BREAK
		CASE 99
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF (HAS_NET_TIMER_EXPIRED(fmTrigger.timeTrigIntro, iTimeToWait)
				OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-204.628052,301.031799,95.519890>>, <<-199.666351,308.895172,98.258202>>, 1.625000))
		//		AND HAVE_WALK_TO_CORONA_LAMAR_MODELS_LOADED(fmTrigger)

	#IF IS_DEBUG_BUILD	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF

					
//					CLEAR_AREA(<<-197.9325, 301.3624, 96.0516>>, 3.0, TRUE)
//
//					IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_OutsideRaceArea)
//						IF NOT IS_AREA_OCCUPIED(<<-200.5890, 297.7068, 95.1775>>, <<-195.0197, 305.4246, 97.0983>>, FALSE, TRUE, FALSE, FALSE, FALSE)
//						//	CREATE_LAMAR_VEHICLE_FOR_TUTORIAL(fmTrigger, <<-198.4620, 301.3991, 95.9455>>, 146.9466)
//							CREATE_LAMAR_VEHICLE_FOR_TUTORIAL(fmTrigger, << -198.782, 301.486, 96.4986 >>, 181.158768)
//							CREATE_NPC_PED_INSIDE_VEHICLE(fmTrigger.pedLamar, CHAR_LAMAR, fmtrigger.vehLamar)
//							SET_VEHICLE_DOORS_LOCKED(fmtrigger.vehLamar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
//							SET_ENTITY_INVINCIBLE(fmTrigger.pedLamar, TRUE)
//							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(fmTrigger.pedLamar, TRUE)
//							SET_PED_RELATIONSHIP_GROUP_HASH(fmTrigger.pedLamar, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
//							SET_ENTITY_PROOFS(fmTrigger.pedLamar, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
//							SET_ENTITY_PROOFS(fmTrigger.vehLamar, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
//							ADD_PED_FOR_DIALOGUE(fmTrigger.speechFmTriggerTut, 1, fmTrigger.pedLamar,  "Lamar")
//							fmTrigger.oLamarPhone = CREATE_OBJECT(PROP_PHONE_ING,  <<-202.5955, 306.8250, 95.9461>>, FALSE, FALSE)
//							INT index 
//							index = GET_PED_BONE_INDEX(fmTrigger.pedLamar, BONETAG_PH_L_HAND)
//							ATTACH_ENTITY_TO_ENTITY(fmTrigger.oLamarPhone, fmTrigger.pedLamar, index, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//							TASK_PLAY_ANIM(fmTrigger.pedLamar, "mp_intro_seq@ig_2_lamar_car_idle", "loop", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING)
//							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] Created replacement Lamar and car") #ENDIF
//						ELSE
//							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] Area occupied - Not creating Lamar") #ENDIF
//						ENDIF
//					ENDIF

					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
					fmTrigger.iBeforeRaceCutProg = 100
					
					AWARD_ACHIEVEMENT(ACH30) // off the plane
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[DO_WALK_TO_RACE_CORONA_CUT] fmTrigger.iBeforeRaceCutProg = ", fmTrigger.iBeforeRaceCutProg) #ENDIF
				
				ENDIF
			ENDIF
		BREAK
		
		CASE 100
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(fmTrigger.iBitset, biFMt_HideHudAndRadar)
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		
	ENDIF
	
//	IF NOT DOES_ENTITY_EXIST(fmTrigger.pedClone)
//		fmTrigger.pedClone = CREATE_PED(PEDTYPE_MISSION, GET_ENTITY_MODEL(PLAYER_PED_ID()), <<-224.4492, 314.0460, 91.1632>>, 28.0764, FALSE, FALSE) //<<-197.8224, 316.3346, 95.9455>> , 0.0
//		CLONE_PED_TO_TARGET(PLAYER_PED_ID(), fmTrigger.pedClone)
//		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(fmTrigger.pedClone, TRUE)
//		SET_ENTITY_INVINCIBLE(fmTrigger.pedClone, TRUE)
//		SET_ENTITY_VISIBLE(fmTrigger.pedClone, FALSE)
//		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_WALK_TO_RACE_CORONA_CUT] Created ped Clone") #ENDIF
//	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MOCAP()
	
	
	INT iUniqueId = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	
	INT playerBitfield = Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission (iUniqueId)
	
	INT tempLoop = 0
	
	PLAYER_INDEX thePlayerID
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(playerBitfield, tempLoop))
			
			thePlayerID = INT_TO_PLAYERINDEX(tempLoop)
			IF (IS_NET_PLAYER_OK(thePlayerID, FALSE))
				//-- Check if player is ready for the intro mocap
				IF NOT IS_PLAYER_READY_FOR_TUTORIAL_MOCAP(thePlayerID)
					#IF IS_DEBUG_BUILD 

						PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MOCAP] This player not ready ", thePlayerID) 
						
					#ENDIF	
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD 
		PRINT_TRIGGER_STRING("[ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MOCAP] Returning True")
	#ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MISSION(#IF IS_DEBUG_BUILD SCRIPT_TIMER &timePlayerReady #ENDIF)
	
	
	INT iUniqueId = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	
	INT playerBitfield = Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission (iUniqueId)
	
	INT tempLoop = 0
	
	PLAYER_INDEX thePlayerID
	
	#IF IS_DEBUG_BUILD
		BOOL bShowDebug
		IF NOT HAS_NET_TIMER_STARTED(timePlayerReady)
		OR HAS_NET_TIMER_EXPIRED(timePlayerReady, 2000)
			PRINT_TRIGGER_STRING("ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MISSION Time debug....")
			bShowDebug = TRUE
			
			REPEAT NUM_NETWORK_PLAYERS tempLoop
				IF (IS_BIT_SET(playerBitfield, tempLoop))
					
					thePlayerID = INT_TO_PLAYERINDEX(tempLoop)
					IF (IS_NET_PLAYER_OK(thePlayerID, FALSE))
						PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MISSION] This player reserved for mission", thePlayerID)
					ENDIF
				ENDIF
			ENDREPEAT
			tempLoop = 0
			RESET_NET_TIMER(timePlayerReady)
			START_NET_TIMER(timePlayerReady)
		ENDIF
	#ENDIF
	
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(playerBitfield, tempLoop))
			
			thePlayerID = INT_TO_PLAYERINDEX(tempLoop)
			IF (IS_NET_PLAYER_OK(thePlayerID, FALSE))
			
			//	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MISSION] This player reserved for mission", thePlayerID) #ENDIF
				
				//-- Checjk if player has loaded mission from cloud
				IF NOT HAS_PLAYER_LOADED_CLOUD_DATA_FOR_TUTORIAL_MISSION(thePlayerID)
					#IF IS_DEBUG_BUILD 
						IF bShowDebug
							PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MISSION] This player hasn't loaded cloud data", thePlayerID) 
						ENDIF
					#ENDIF
					
					RETURN FALSE
				ENDIF
				
				//-- Check if player has finished watching cutscene
				IF NOT HAS_PLAYER_COMPLETED_POST_RACE_CUTSCENE(thePlayerID)
					#IF IS_DEBUG_BUILD 
						IF bShowDebug
							PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MISSION] This player hasn't finished cut", thePlayerID) 
						ENDIF
					#ENDIF	
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	PRINTLN("[dsw] ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MISSION - TRUE")
	RETURN TRUE
ENDFUNC

FUNC INT GET_NUMBER_OF_PLAYERS_READY_FOR_MISSION()
	INT iUniqueId = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	RETURN (Get_Number_Of_Players_That_Reserved_Themselves_For_The_Mission(iUniqueId))
ENDFUNC

FUNC INT GET_NUMBER_OF_PLAYERS_WHO_WERE_ON_MY_RACE()
	RETURN g_iPlayersOnMyTutorialRace
ENDFUNC

PROC GET_CREATE_POSITION_FOR_MOCAP_PED(INT index, VECTOR &vPos)
	SWITCH index
		CASE 0	vPos = <<-188.9953, 304.3564, 95.9458>> BREAK
		CASE 1	vPos = <<-188.7480, 302.6785, 95.9458>> BREAK
		CASE 2	vPos = <<-188.7535, 300.8929, 95.9458>> BREAK
		CASE 3 	vPos = <<-187.6880, 284.4852, 92.0768>> BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL CREATE_LOCAL_PLAYERS_FOR_MOCAP(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	INT iUniqueId = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	
	INT playerBitfield = Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission (iUniqueId)
	
	INT tempLoop = 0
	INT iNumPlayers = 0
//	INT iNumPlayersCreated = 0
	PLAYER_INDEX PlayersForCut[4]
	PLAYER_INDEX thePlayerID
	
	FOR tempLoop = 0 TO 3
		PlayersForCut[tempLoop] = INVALID_PLAYER_INDEX()
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		PRINT_TRIGGER_STRING("")
		PRINT_TRIGGER_STRING("********************* [CREATE_LOCAL_PLAYERS_FOR_MOCAP] *********************")
		PRINT_TRIGGER_STRING("")
		PRINT_TRIGGER_STRING("[CREATE_LOCAL_PLAYERS_FOR_MOCAP] Building list of players who will be in the mocap")
	#ENDIF
	tempLoop = 0
	
	PlayersForCut[0] =  PLAYER_ID()
	iNumPlayers++
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(playerBitfield, tempLoop))
			
			thePlayerID = INT_TO_PLAYERINDEX(tempLoop)
			IF (IS_NET_PLAYER_OK(thePlayerID, FALSE))
				IF thePlayerID <> PLAYER_ID()
					PlayersForCut[iNumPlayers] =  thePlayerID
					iNumPlayers++
					
					#IF IS_DEBUG_BUILD
						PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[CREATE_LOCAL_PLAYERS_FOR_MOCAP] This player will be in the mocap: ", thePlayerID)
					#ENDIF
				ELSE
				
					#IF IS_DEBUG_BUILD
						PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[CREATE_LOCAL_PLAYERS_FOR_MOCAP] I will be in the mocap ", PLAYER_ID())
					#ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	tempLoop = 0
	MODEL_NAMES mPlayer
	VECTOR vCreate
	FOR tempLoop = 0 TO iNumPlayers -1
		IF NOT DOES_ENTITY_EXIST(fmTrigger.pedForMocap[tempLoop])
			IF PlayersForCut[tempLoop] <> INVALID_PLAYER_INDEX()
				GET_CREATE_POSITION_FOR_MOCAP_PED(tempLoop, vCreate)
				mPlayer = GET_ENTITY_MODEL(GET_PLAYER_PED(PlayersForCut[tempLoop]))
			 	fmTrigger.pedForMocap[tempLoop]  = CREATE_PED(PEDTYPE_MISSION, mPlayer, vCreate, 0.0, FALSE, FALSE)
				CLONE_PED_TO_TARGET(GET_PLAYER_PED(PlayersForCut[tempLoop]), fmTrigger.pedForMocap[tempLoop])
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[CREATE_LOCAL_PLAYERS_FOR_MOCAP] Created this player's clone ", thePlayerID)
				#ENDIF
				
			ELSE
				//-- Invalid player for some reason
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[CREATE_LOCAL_PLAYERS_FOR_MOCAP] Trying to create temp players but player is invalid! tempLoop = ", tempLoop)
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	tempLoop = 0
	FOR tempLoop = 0 TO iNumPlayers -1
		IF NOT DOES_ENTITY_EXIST(fmTrigger.pedForMocap[tempLoop])
			#IF IS_DEBUG_BUILD
				PRINT_TRIGGER_STRING_INT("[CREATE_LOCAL_PLAYERS_FOR_MOCAP]Still waiting for someone, tempLoop = ", tempLoop)
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC CLEANUP_TUTORIAL_MOCAP_PEDS(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	INT iTemp
	ENTITY_INDEX ent
	FOR iTemp = 0 TO 2
		IF DOES_ENTITY_EXIST(fmTrigger.pedForMocap[iTemp])
			ent = fmTrigger.pedForMocap[iTemp]
			DELETE_ENTITY(ent)
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    	Don't let anyone else join my tutorial mission
PROC BLOCK_MY_TUTORIAL_MISSION()
	MP_MISSION_DATA thisMissionData
	thisMissionData.mdID.idMission      = Get_MissionID_For_This_Players_Mission(PLAYER_ID())
	thisMissionData.mdUniqueID          = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())

	Broadcast_Mission_Not_Joinable(thisMissionData)

ENDPROC

PROC RESET_TUTORIAL_VOICE_CHAT()
	#IF IS_DEBUG_BUILD 
		PRINT_TRIGGER_STRING("[RESET_TUTORIAL_VOICE_CHAT] Called...")
		PRINT_TRIGGER_STRING_FLOAT("[RESET_TUTORIAL_VOICE_CHAT] NETWORK_GET_TALKER_PROXIMITY (cleanup) ", NETWORK_GET_TALKER_PROXIMITY()) 
	#ENDIF
	NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE)
	NETWORK_SET_OVERRIDE_TUTORIAL_SESSION_CHAT(FALSE)
	NETWORK_SET_TALKER_PROXIMITY(-1.0)
ENDPROC
FUNC BOOL DO_LAMAR_AFTER_RACE_MOCAP(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	//SEQUENCE_INDEX seq
	//STRING sConv
	//fmTrigger.iAfterAmmuCutProg = -1
	
	
	SWITCH fmTrigger.iAfterRaceCutProg
		CASE 0
			IF NOT SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP()
				IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_StartPutOntoMiss)
					
					SET_BIT(fmTrigger.iBitset2, biFmt2_StartPutOntoMiss)
					#IF IS_DEBUG_BUILD 
						PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] Set biFmt2_StartPutOntoMiss")
					#ENDIF
				ENDIF
				
				IF (fmTrigger.iNumPlayersReservedOnMission  > 1 AND IS_BIT_SET(fmTrigger.iBitset, biFMt_DmFinalCheckForPlay))
				OR fmTrigger.iNumPlayersReservedOnMission >= MIN_PLAYERS_FOR_MISSION
				OR IS_BIT_SET(fmTrigger.iBitset, biFmt_PutPlayerOnSolo)
					IF NOT IS_BIT_SET(fmTrigger.iBitset, biFmt_BlockedMission)
						BLOCK_MY_TUTORIAL_MISSION()
						SET_BIT(fmTrigger.iBitset, biFmt_BlockedMission)
						
						#IF IS_DEBUG_BUILD 
							PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] Set biFmt2_StartPutOntoMiss")
						#ENDIF
						
					ENDIF
					
					SET_LOCAL_PLAYER_READY_FOR_TUTORIAL_MOCAP(TRUE)
					
					SET_LOCAL_PLAYER_CREATED_LAMAR_POST_RACE_CUT_ASSETS(TRUE) // Test
					fmTrigger.iAfterRaceCutProg++
					
					#IF IS_DEBUG_BUILD
						PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] about to launch 4-player tutorial mission. Progressing because...")
						PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] First check...")
						IF (fmTrigger.iNumPlayersReservedOnMission  > 1 AND IS_BIT_SET(fmTrigger.iBitset, biFMt_DmFinalCheckForPlay))
							
							PRINT_TRIGGER_STRING_INT("[DO_LAMAR_AFTER_RACE_MOCAP] SUCCESS! fmTrigger.iNumPlayersReservedOnMission = ", fmTrigger.iNumPlayersReservedOnMission)
							PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] And IS_BIT_SET(fmTrigger.iBitset, biFMt_DmFinalCheckForPlay)")
						ELSE
							PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] ...FAILED First check")
						ENDIF
						
						PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] Second check...")
						IF fmTrigger.iNumPlayersReservedOnMission >= MIN_PLAYERS_FOR_MISSION
							PRINT_TRIGGER_STRING_INT("[DO_LAMAR_AFTER_RACE_MOCAP] SUCCESS! fmTrigger.iNumPlayersReservedOnMission = ", fmTrigger.iNumPlayersReservedOnMission )
						ELSE
							PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] ... FAILED Second check")
						ENDIF
						
						PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] Third check...") 
						
						IF IS_BIT_SET(fmTrigger.iBitset, biFmt_PutPlayerOnSolo)
							PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] ... SUCCESS! IS_BIT_SET(fmTrigger.iBitset, biFmt_PutPlayerOnSolo")
						ELSE 
							PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] ... FAILED third check")
						ENDIF
						
						PRINT_TRIGGER_STRING_INT("[DO_LAMAR_AFTER_RACE_MOCAP] iAfterRaceCutProg = ", fmTrigger.iAfterRaceCutProg)
					#ENDIF
				ELSE
					IF NOT IS_BIT_SET(fmTrigger.iBitset, biFmt_PutPlayerOnSolo)
						IF IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_StartTutDeathmatchIfICan)
						AND fmTrigger.iNumPlayersReservedOnMission = 1
							SET_BIT(fmTrigger.iBitset, biFmt_PutPlayerOnSolo)
							#IF IS_DEBUG_BUILD 
								PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] Set biFmt_PutPlayerOnSolo") 
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//-- Joined at mission stage
				
				
				IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFmt2_TookTransControl )
					TAKE_CONTROL_OF_TRANSITION(DEFAULT, FALSE)
					SET_SKYSWOOP_STAGE(SKYSWOOP_INSKYSTATIC)
					SET_BIT(fmtrigger.iBitset2, biFmt2_TookTransControl )
			//		ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
			//		SET_SKYFREEZE_FROZEN()
					#IF IS_DEBUG_BUILD
						PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] Took control of transition")
					#ENDIF	
				ELSE
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_BIT_SET(fmTrigger.iBitset, biFmt_FrozePlyForSkyswoop)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
							SET_BIT(fmTrigger.iBitset, biFmt_FrozePlyForSkyswoop)
							#IF IS_DEBUG_BUILD
								VECTOR vTemp
								vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
								PRINT_TRIGGER_STRING_VECTOR("[DO_LAMAR_AFTER_RACE_MOCAP] Freezing player pos = ", vTemp)
								
							#ENDIF	
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFmt2_SetupForSwoop )
						IF NETWORK_IS_IN_TUTORIAL_SESSION()
							IF (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
								SET_BIT(fmtrigger.iBitset2, biFmt2_SetupForSwoop)
								SET_BIT(fmTrigger.iBitset, biFmt_PutPlayerOnSolo)
								SET_BIT(fmTrigger.iBitset2, biFmt2_StartPutOntoMiss)
								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_FinishedPostRaceCleanup)
								
								#IF IS_DEBUG_BUILD
									PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] Set biFmt2_SetupForSwoop")
								#ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] Skyswoop waiting for Is_Initial_Cloud_Loaded_Mission_Data_Ready")
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] Waiting for tutorial session - skyswoop")
							#ENDIF	
						ENDIF
					ELSE
						IF SET_SKYSWOOP_DOWN()
							IF IS_NET_PLAYER_OK(PLAYER_ID())
								ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
								SET_SKYFREEZE_FROZEN()
								SET_LOCAL_PLAYER_CREATED_LAMAR_POST_RACE_CUT_ASSETS(TRUE)
								SET_LOCAL_PLAYER_READY_FOR_TUTORIAL_MOCAP(TRUE)
								SET_OVERRIDE_WEATHER("EXTRASUNNY")
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								//	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
									SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
								ENDIF
								fmTrigger.iAfterRaceCutProg = 1
								#IF IS_DEBUG_BUILD 
									PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] Done skyswoop down and player ok!")
									PRINT_TRIGGER_STRING_INT("[DO_LAMAR_AFTER_RACE_MOCAP] (SET_SKYSWOOP_DOWN done ) fmTrigger.iAfterRaceCutProg = ", fmTrigger.iAfterRaceCutProg) 
								#ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									PRINT_TRIGGER_STRING("[DO_LAMAR_AFTER_RACE_MOCAP] Done skyswoop down but player not ok!")
								#ENDIF	
							ENDIF
						ENDIF 
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MOCAP()
				fmTrigger.iAfterRaceCutProg = 99
				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_LAMAR_AFTER_RACE_MOCAP] iAfterRaceCutProg = ", fmTrigger.iAfterRaceCutProg)
				#ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF CREATE_LOCAL_PLAYERS_FOR_MOCAP(fmTrigger)
				REQUEST_CUTSCENE("mp_intro_mcs_8_a1")
				fmTrigger.iAfterRaceCutProg++
				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_LAMAR_AFTER_RACE_MOCAP] iAfterRaceCutProg = ", fmTrigger.iAfterRaceCutProg)
				#ENDIF
			ENDIF
			
		BREAK
		
		CASE 3
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				IF NOT IS_PED_INJURED( fmTrigger.pedForMocap[0])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MP_1", fmTrigger.pedForMocap[0])
				ENDIF
				IF NOT IS_PED_INJURED(fmTrigger.pedForMocap[1])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MP_2", fmTrigger.pedForMocap[1])
				ENDIF
				IF NOT IS_PED_INJURED(fmTrigger.pedForMocap[2])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MP_3", fmTrigger.pedForMocap[2])
				ENDIF
				IF NOT IS_PED_INJURED(fmTrigger.pedForMocap[3])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MP_4", fmTrigger.pedForMocap[3])
				ENDIF
				fmTrigger.iAfterRaceCutProg++
				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_LAMAR_AFTER_RACE_MOCAP] iAfterRaceCutProg = ", fmTrigger.iAfterRaceCutProg)
				#ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF HAS_CUTSCENE_LOADED()	
				IF NOT IS_PED_INJURED(fmTrigger.pedForMocap[0])
				//	REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					REGISTER_ENTITY_FOR_CUTSCENE(fmTrigger.pedForMocap[0], "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
				IF NOT IS_PED_INJURED(fmTrigger.pedForMocap[1])
					REGISTER_ENTITY_FOR_CUTSCENE(fmTrigger.pedForMocap[1], "MP_2", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_2", CU_DONT_ANIMATE_ENTITY , DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
				IF NOT IS_PED_INJURED(fmTrigger.pedForMocap[2])
					REGISTER_ENTITY_FOR_CUTSCENE(fmTrigger.pedForMocap[2], "MP_3", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_3", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
				IF NOT IS_PED_INJURED(fmTrigger.pedForMocap[3])
					REGISTER_ENTITY_FOR_CUTSCENE(fmTrigger.pedForMocap[3], "MP_4", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_4", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
				
				START_CUTSCENE(CUTSCENE_DO_NOT_REPOSITION_PLAYER_TO_SCENE_ORIGIN)
				
				IF NOT HAS_LOCAL_PLAYER_CREATED_LAMAR_CUT_ASSETS()
					SET_LOCAL_PLAYER_CREATED_LAMAR_POST_RACE_CUT_ASSETS(TRUE)
				ENDIF
				fmTrigger.iAfterRaceCutProg++
				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_LAMAR_AFTER_RACE_MOCAP] iAfterRaceCutProg = ", fmTrigger.iAfterRaceCutProg)
				#ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF IS_CUTSCENE_PLAYING()
				fmTrigger.iAfterRaceCutProg++
				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_LAMAR_AFTER_RACE_MOCAP] iAfterRaceCutProg = ", fmTrigger.iAfterRaceCutProg)
				#ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF HAS_CUTSCENE_FINISHED()
				CLEANUP_MP_CUTSCENE()
				CLEANUP_TUTORIAL_MOCAP_PEDS(fmTrigger)
				SET_LOCAL_PLAYER_HAS_COMPLETED_POST_RACE_CUTSCENE(TRUE)
				fmTrigger.iAfterRaceCutProg = 100
				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_LAMAR_AFTER_RACE_MOCAP] iAfterRaceCutProg = ", fmTrigger.iAfterRaceCutProg)
				#ENDIF
			ENDIF	
		BREAK
		
		CASE 99
			SET_LOCAL_PLAYER_HAS_COMPLETED_POST_RACE_CUTSCENE(TRUE)
			fmTrigger.iAfterRaceCutProg = 100
			
			#IF IS_DEBUG_BUILD
				PRINT_TRIGGER_STRING_INT("[DO_LAMAR_AFTER_RACE_MOCAP] iAfterRaceCutProg = ", fmTrigger.iAfterRaceCutProg)
			#ENDIF
		BREAK
		
		CASE 100
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	
	DISABLE_FRONTEND_THIS_FRAME()
	
	
	RETURN FALSE
ENDFUNC

PROC MAKE_ALL_PLAYERS_INVISIBLE_THIS_FRAME()
	INT iPlayerCOunt
	PARTICIPANT_INDEX ParticipantID
	PLAYER_INDEX PlayerID
	FOR iPlayerCOunt= 0 TO NUM_NETWORK_PLAYERS - 1
		ParticipantID = INT_TO_NATIVE(PARTICIPANT_INDEX, iPlayerCOunt)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(ParticipantID)
			
			PlayerID = NETWORK_GET_PLAYER_INDEX(ParticipantID) 
			SET_PLAYER_INVISIBLE_LOCALLY(PlayerID)
		ENDIF
	ENDFOR
ENDPROC

PROC MAKE_ONLY_PLAYER_CAMERA_WORK(BOOL bOnlyCamera = TRUE)
	IF bOnlyCamera
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_SPECIAL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_GUN_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_GUN_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ACCELERATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_BRAKE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HEADLIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_EXIT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HANDBRAKE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HOTWIRE_LEFT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HOTWIRE_RIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_CIN_CAM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_NEXT_RADIO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PREV_RADIO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_RADIO_WHEEL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HORN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PASSENGER_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PASSENGER_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_SELECT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ROOF)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_JUMP)
	ELSE
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_SPECIAL)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_GUN_LR)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_GUN_UD)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_AIM)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ATTACK)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ATTACK2)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ACCELERATE)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_BRAKE)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HEADLIGHT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_EXIT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HANDBRAKE)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HOTWIRE_LEFT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HOTWIRE_RIGHT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_LOOK_BEHIND)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_CIN_CAM)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_NEXT_RADIO)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PREV_RADIO)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_RADIO_WHEEL)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HORN)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PASSENGER_AIM)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PASSENGER_ATTACK)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_SELECT_NEXT_WEAPON)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_SELECT_PREV_WEAPON)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ROOF)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_JUMP)
	ENDIF
ENDPROC

PROC DO_WAITING_SCREEN_HELP(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	SWITCH fmTrigger.iWaitingScreenHelpProg
		CASE 0 
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			//	PRINT_HELP("FM_IHELP_RIN") //Details of the Race are shown inside the Marker.  They include the Creator of the Race and the Race rating.
				///PRINT_HELP("FM_IHELP_RCK") 
				fmTrigger.iWaitingScreenHelpProg++
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
			//	PRINT_HELP("FM_IHELP_RN2") //The type of Job is shown within the Trigger. There are Races, Jobs for contacts and other activities, such as Golf.
				fmTrigger.iWaitingScreenHelpProg++
			ENDIF
		BREAK
		
		CASE 50
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			//	PRINT_HELP("FM_IHELP_SDM", 9000) //You can start a Deathmatch by entering a Deathmatch Trigger, marked with ~BLIP_DEATHMATCH~ on the Map.
				fmTrigger.iWaitingScreenHelpProg++
			ENDIF
		BREAK
		
		CASE 51
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				
				fmTrigger.iWaitingScreenHelpProg++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


PROC CLEANUP_LAMAR(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	ENTITY_INDEX ent
	IF DOES_ENTITY_EXIST(fmTrigger.vehLamar)
		ent = fmTrigger.vehLamar
		DELETE_ENTITY(ent)
	ENDIF
	
	IF DOES_ENTITY_EXIST(fmTrigger.pedLamar)
		ent = fmTrigger.pedLamar
		DELETE_ENTITY(ent)
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LAMAR))
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_VEH_MODEL(CHAR_LAMAR))
	
	REMOVE_ANIM_DICT("mp_intro_seq@ig_2_lamar_car_idle")
	
//	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Cleaned up Lamar") #ENDIF
ENDPROC


PROC REQUEST_LAMAR_AND_VEHICLE()
	REQUEST_NPC_PED_MODEL(CHAR_LAMAR)
	REQUEST_MODEL(GET_LAMAR_VEHICLE_MODEL_FOR_TUTORIAL())
ENDPROC

PROC PRELOAD_LAMAR_AND_VEHICLE(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_RequestedLamar)
		IF IS_LOCAL_PLAYER_READY_TO_PRELOAD_LAMAR_ASSETS() //-- Gets set in fm_intro
			REQUEST_LAMAR_AND_VEHICLE()
			SET_BIT(fmTrigger.iBitset2, biFmt2_RequestedLamar)
			#IF IS_DEBUG_BUILD 
				PRINT_TRIGGER_STRING("Requested Lamar + car prior to intro being completed") 
			#ENDIF
		ENDIF
	ENDIF
ENDPROC
//
//FUNC BOOL CREATE_LAMAR_FOR_RACE_TUT(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
//	
//	//-- See also calls to PRELOAD_LAMAR_AND_VEHICLE
//	
//	IF NOT DOES_ENTITY_EXIST(fmTrigger.vehLamar)
//		IF CREATE_LAMAR_VEHICLE_FOR_TUTORIAL(fmTrigger, <<-674.7998, -962.1434, 19.6905>>, 270.5580)
//			SET_ENTITY_PROOFS(fmTrigger.vehLamar, TRUE, TRUE, TRUE, TRUE, TRUE)
//			FREEZE_ENTITY_POSITION(fmTrigger.vehLamar, TRUE)
//			
//			SET_VEHICLE_DOORS_LOCKED(fmTrigger.vehLamar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
//			#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Created Lamar car") #ENDIF
//		ENDIF
//	/*	REQUEST_NPC_VEH_MODEL(CHAR_LAMAR)
//		IF HAS_MODEL_LOADED(GET_NPC_VEH_MODEL(CHAR_LAMAR))  
//			IF CREATE_NPC_VEHICLE(fmTrigger.vehLamar, CHAR_LAMAR, <<-674.7998, -962.1434, 19.6905>>, 270.5580) // Old ammu <<8.5347, -1125.9517, 27.3880>>, 91.1205 
//				
//				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Created Lamar car") #ENDIF
//			ENDIF
//		ENDIF */
//	ELSE	
//		IF NOT DOES_ENTITY_EXIST(fmTrigger.pedLamar)
//			REQUEST_NPC_PED_MODEL(CHAR_LAMAR)
//			IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_LAMAR))
//				IF IS_VEHICLE_DRIVEABLE(fmTrigger.vehLamar)
//					//IF CREATE_NPC_PED_INSIDE_VEHICLE(fmTrigger.pedLamar, CHAR_LAMAR,fmTrigger.vehLamar)
//					IF CREATE_NPC_PED_ON_FOOT(fmTrigger.pedLamar, CHAR_LAMAR, <<-671.8002, -962.7106, 19.8173>>, 0.8151 )
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(fmTrigger.pedLamar, TRUE)
//						SET_PED_RELATIONSHIP_GROUP_HASH(fmTrigger.pedLamar, rgFM_AiLike)
//						SET_PED_CONFIG_FLAG(fmTrigger.pedLamar, PCF_AIDriverAllowFriendlyPassengerSeatEntry, TRUE)  
//						SET_PED_CONFIG_FLAG(fmTrigger.pedLamar, PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
//						SET_PED_CAN_BE_TARGETTED(fmTrigger.pedLamar, FALSE)
//						SET_ENTITY_INVINCIBLE(fmTrigger.pedLamar, TRUE)
//						SET_ENTITY_PROOFS(fmTrigger.pedLamar, TRUE, TRUE, TRUE, TRUE, TRUE)
//						FREEZE_ENTITY_POSITION(fmTrigger.pedLamar, TRUE)
//						SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(fmTrigger.pedLamar, FALSE, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
//						ADD_PED_FOR_DIALOGUE(fmTrigger.speechFmTriggerTut, 1, fmTrigger.pedLamar,  "Lamar")
//				//		FREEZE_ENTITY_POSITION(fmTrigger.vehLamar, TRUE)
//						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Created Lamar") #ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//
//	
//	IF NOT DOES_ENTITY_EXIST(fmTrigger.VehLamar)
//	OR NOT DOES_ENTITY_EXIST(fmTrigger.pedLamar)
//		RETURN FALSE
//	ENDIF
//	
//	IF NOT IS_PED_INJURED(fmTrigger.pedLamar)
//		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(fmTrigger.pedLamar)
//			RETURN FALSE
//		ENDIF
//	ENDIF
//	RETURN TRUE
//ENDFUNC

FUNC BOOL CREATE_LAMAR_ON_PHONE_FOR_RACE_TUT(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	REQUEST_MODEL(GET_LAMAR_VEHICLE_MODEL_FOR_TUTORIAL())
	REQUEST_NPC_PED_MODEL(CHAR_LAMAR)
	REQUEST_MODEL(PROP_PHONE_ING)
	REQUEST_ANIM_DICT("mp_intro_seq@ig_2_lamar_car_idle")
	IF HAVE_WALK_TO_CORONA_LAMAR_MODELS_LOADED(fmTrigger)
		IF NOT DOES_ENTITY_EXIST(fmTrigger.vehLamar)
	//		CREATE_LAMAR_VEHICLE_FOR_TUTORIAL(fmTrigger, <<-198.4620, 301.3991, 95.9455>>, 146.9466)
			CREATE_LAMAR_VEHICLE_FOR_TUTORIAL(fmTrigger, << -198.782, 301.486, 96.4986 >>, 181.158768)
			SET_ENTITY_PROOFS(fmTrigger.vehLamar, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_VEHICLE_DOORS_LOCKED(fmtrigger.vehLamar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(fmTrigger.pedLamar)
			IF DOES_ENTITY_EXIST(fmTrigger.vehLamar)
				IF IS_VEHICLE_DRIVEABLE(fmTrigger.vehLamar)
					CREATE_NPC_PED_INSIDE_VEHICLE(fmTrigger.pedLamar, CHAR_LAMAR, fmtrigger.vehLamar)
					
					SET_ENTITY_INVINCIBLE(fmTrigger.pedLamar, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(fmTrigger.pedLamar, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(fmTrigger.pedLamar, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
					SET_ENTITY_PROOFS(fmTrigger.pedLamar, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
					
					ADD_PED_FOR_DIALOGUE(fmTrigger.speechFmTriggerTut, 1, fmTrigger.pedLamar,  "Lamar")
					fmTrigger.oLamarPhone = CREATE_OBJECT(PROP_PHONE_ING,  <<-202.5955, 306.8250, 95.9461>>, FALSE, FALSE)
					INT index 
					index = GET_PED_BONE_INDEX(fmTrigger.pedLamar, BONETAG_PH_L_HAND)
					ATTACH_ENTITY_TO_ENTITY(fmTrigger.oLamarPhone, fmTrigger.pedLamar, index, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					TASK_PLAY_ANIM(fmTrigger.pedLamar, "mp_intro_seq@ig_2_lamar_car_idle", "loop", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(fmTrigger.VehLamar)
	OR NOT DOES_ENTITY_EXIST(fmTrigger.pedLamar)
	OR NOT DOES_ENTITY_EXIST(fmTrigger.oLamarPhone)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(fmTrigger.pedLamar)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(fmTrigger.pedLamar)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC
FUNC BOOL Try_To_Reserve_Tutorial_Player_On_Joblist_Invite_Mission(FM_TRIGGER_INTRO_STRUCT &fmTrigger, VECTOR vMissionLoc)
	fmTrigger.reservationRequestUniqueID = NO_UNIQUE_ID
	/*
	VECTOR 					vDmCoords 	= <<-895.0845, -1052.6971, 1.1631>>
	
	MP_MISSION_DATA theMissionData
    theMissionData.mdID.idMission       = Convert_FM_Mission_Type_To_MissionID(FMMC_TYPE_DEATHMATCH)
    theMissionData.mdID.idVariation     = Get_MissionsAtCoords_Variation_For_Tutorial(vDmCoords)
    theMissionData.mdID.idCreator       = FMMC_ROCKSTAR_CREATOR_ID
	theMissionData.mdID.idCloudFilename	= Get_MissionsAtCoords_CloudFilename_For_Tutorial(vDmCoords)	// KGM 24/3/13: The filename is now important to ensure a match
	*/
	//VECTOR 					 vMissionLoc 		= <<-195.5058,309.4661,95.9455>> //<<-688.2597, -982.5331, 19.3901>> 
	
	MP_MISSION_DATA theMissionData
    theMissionData.mdID.idMission       = Convert_FM_Mission_Type_To_MissionID(FMMC_TYPE_MISSION)
    theMissionData.mdID.idVariation     = Get_MissionsAtCoords_Variation_For_Tutorial(vMissionLoc)
    theMissionData.mdID.idCreator       = FMMC_ROCKSTAR_CREATOR_ID
	theMissionData.mdID.idCloudFilename	= Get_MissionsAtCoords_CloudFilename_For_Tutorial(vMissionLoc)	// KGM 24/3/13: The filename is now important to ensure a match
	
	g_eMPMissionSource      theRequestSource  = MP_MISSION_SOURCE_MISSION_FLOW_BLIP
    BOOL                    hasCutscene       = Get_MissionsAtCoords_HasCutscene_For_Tutorial(vMissionLoc)
	
	
	// Broadcast the mission request to the Mission Controller, store the return value which is a unique ID for the request
    fmTrigger.reservationRequestUniqueID = Broadcast_Reserve_Mission_By_Player(theRequestSource, theMissionData, hasCutscene)
	
	// Although unlikely, it is possible that the request can return 'NO_UNIQUE_ID' so this needs to be checked
	IF (fmTrigger.reservationRequestUniqueID = NO_UNIQUE_ID)
	    #IF IS_DEBUG_BUILD
	          NET_PRINT("(Tutorial) NO_UNIQUE_ID returned when Reserving Invite Mission. Don't allow player to accept invite") NET_NL()
	          NET_NL()
	          
	          SCRIPT_ASSERT("ERROR: (Tutorial) Attempting to Reserve Mission, but NO_UNIQUE_ID returned. Invite Not Accepted. Tell Keith.")
	    #ENDIF
	    
	    // Don't allow the player to accept the invite
	    RETURN FALSE
	ENDIF

	// Request successfully issued, need to wait for a reply
	#IF IS_DEBUG_BUILD
		NET_PRINT("(Tutorial) Request to reserve place on mission has been accepted. Waiting for confirmation of reservation. [uniqueID: ")
		NET_PRINT_INT(fmTrigger.reservationRequestUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL Has_Tutorial_MP_Mission_Request_Failed(FM_TRIGGER_INTRO_STRUCT &fmTrigger)

	// Has the reservation request failed?
	IF (Has_MP_Mission_Request_Failed(fmTrigger.reservationRequestUniqueID))
		// ...mission has broadcast 'FAILED'
		#IF IS_DEBUG_BUILD
		NET_PRINT("(tutorial) Confirmation Denied when trying to reserve place on Invite Mission.") NET_NL()
		#ENDIF

		fmTrigger.reservationRequestUniqueID = NO_UNIQUE_ID

		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC Process_Stage_Tutorial_Waiting_For_Reservation_Confirmation(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	// Has the reservation request been confirmed?
	fmtrigger.iDmNumReservedPlayers  = 0
	fmtrigger.iDmReservedPlayersBitset = ALL_MISSIONS_AT_COORDS_BITS_CLEAR

	IF NOT (Has_MP_Mission_Request_Just_Broadcast_Confirm_Reservation(fmTrigger.reservationRequestUniqueID, fmtrigger.iDmNumReservedPlayers, fmtrigger.iDmReservedPlayersBitset))
		// ...confirmation still not received, so keep on waiting
		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[Process_Stage_Tutorial_Waiting_For_Reservation_Confirmation] Exit 1") #ENDIF
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[dsw] (Tutorial) Confirmation Received for Reservation Request. UniqueID: ")
		NET_PRINT_INT(fmTrigger.reservationRequestUniqueID)
	#ENDIF
	
	INT tempUniqueID = Get_MP_Mission_Request_Changed_UniqueID(fmTrigger.reservationRequestUniqueID)
	fmTrigger.reservationRequestUniqueID = tempUniqueID
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("  [Updated UniqueID: ")
		NET_PRINT_INT(tempUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF

	//fmTrigger.reservationRequestUniqueID = NO_UNIQUE_ID Moved to Case 0 in PROCESS_RESERVE_DM_SLOT
	IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_ReservedOnMission)
		SET_BIT(fmTrigger.iBitset2, biFmt2_ReservedOnMission)
		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Setting biFmt2_ReservedOnMission") #ENDIF
	ENDIF
	
	fmTrigger.iReservePlayerProg++
	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("Got confirmation fmTrigger.iReservePlayerProg = ", fmTrigger.iReservePlayerProg) #ENDIF
ENDPROC



FUNC BOOL PROCESS_RESERVE_MISSION_SLOT(FM_TRIGGER_INTRO_STRUCT &fmTrigger, VECTOR vMissionLoc)
	SWITCH fmTrigger.iReservePlayerProg
		CASE 0
			IF NOT HAS_NET_TIMER_STARTED(fmTrigger.timeReserveDmSlot)
			OR HAS_NET_TIMER_EXPIRED(fmTrigger.timeReserveDmSlot, 2000)
			OR IS_BIT_SET(fmTrigger.iBitset, biFMt_DmFinalCheckForPlay) //-- Last chance
				#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(fmTrigger.iBitset, biFMt_DmFinalCheckForPlay)
						PRINT_TRIGGER_STRING("[PROCESS_RESERVE_MISSION_SLOT] biFMt_DmFinalCheckForPlay is set!")
					ENDIF
				#ENDIF
				fmTrigger.reservationRequestUniqueID = NO_UNIQUE_ID
				IF Try_To_Reserve_Tutorial_Player_On_Joblist_Invite_Mission(fmTrigger, vMissionLoc)
					fmTrigger.iReservePlayerProg++
					
					RESET_NET_TIMER(fmTrigger.timeWaitForHostToReply)
					START_NET_TIMER(fmTrigger.timeWaitForHostToReply)
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("fmTrigger.iReservePlayerProg = ", fmTrigger.iReservePlayerProg) #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[PROCESS_RESERVE_MISSION_SLOT] Try_To_Reserve_Tutorial_Player_On_Joblist_Invite_Mission failed!") #ENDIF
					RESET_NET_TIMER(fmTrigger.timeReserveDmSlot)
					START_NET_TIMER(fmTrigger.timeReserveDmSlot)
				ENDIF
				
			ENDIF
		BREAK
		
		CASE 1
			IF NOT Has_Tutorial_MP_Mission_Request_Failed(fmTrigger)
		 		Process_Stage_Tutorial_Waiting_For_Reservation_Confirmation(fmTrigger)
				
				//-- For 1642587 - Need to handle not hearing back from host for a while when trying to get reserved on mission
				IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_ReservedOnMission)
					IF HAS_NET_TIMER_EXPIRED(fmTrigger.timeWaitForHostToReply, 60000)
						fmTrigger.iReservePlayerProg = 0
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[PROCESS_RESERVE_MISSION_SLOT] timeWaitForHostToReply expired! WIll try requesting reservation again. fmTrigger.iReservePlayerProg = ", fmTrigger.iReservePlayerProg) #ENDIF
					ENDIF
				ENDIF
			ELSE	
				fmTrigger.iReservePlayerProg = 0
				RESET_NET_TIMER(fmTrigger.timeReserveDmSlot)
				START_NET_TIMER(fmTrigger.timeReserveDmSlot)
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("MP mission request failed for some reason, resetting. fmTrigger.iReservePlayerProg = ", fmTrigger.iReservePlayerProg) #ENDIF
				
			/*	//-- Don't want to check for ever. Will check every 5 seconds for 60 seconds, then do one final check
				IF NOT IS_BIT_SET(fmTrigger.iBitset, biFMt_DmFinalCheckForPlay)
					ELSE
					//-- Failed on final check, bail
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[PROCESS_RESERVE_MISSION_SLOT] MP mission request failed for some reason, and final check is set. Bailing as only player") #ENDIF
					IF fmTrigger.reservationRequestUniqueID <> NO_UNIQUE_ID
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[PROCESS_RESERVE_MISSION_SLOT] Calling Broadcast_Cancel_Mission_Reservation_By_Player as bailing") #ENDIF
						Broadcast_Cancel_Mission_Reservation_By_Player(fmTrigger.reservationRequestUniqueID)
					ELSE	
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[PROCESS_RESERVE_MISSION_SLOT] Bailing, but NOT calling Broadcast_Cancel_Mission_Reservation_By_Player as fmTrigger.reservationRequestUniqueID = NO_UNIQUE_ID") #ENDIF
					ENDIF
					SET_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_BailAsOnlyPlayer)
				ENDIF */
			ENDIF
		BREAK
		
		CASE 2
			//-- Need at least 2 players
			IF fmtrigger.iDmNumReservedPlayers >= MIN_PLAYERS_FOR_MISSION
			OR IS_BIT_SET(fmTrigger.iBitset, biFMt_DmFinalCheckForPlay)
				//-- Success!
				fmTrigger.iReservePlayerProg = 99
				#IF IS_DEBUG_BUILD 
					PRINT_TRIGGER_STRING_INT("[PROCESS_RESERVE_MISSION_SLOT] Got confirmation fmTrigger.iReservePlayerProg = ", fmTrigger.iReservePlayerProg)
					PRINT_TRIGGER_STRING_INT("[PROCESS_RESERVE_MISSION_SLOT] Got confirmation, Num players on mission = ", fmtrigger.iDmNumReservedPlayers)
				#ENDIF
			ELSE
				//-- Didn't find anyone else, try again
				fmTrigger.iReservePlayerProg = 0
				RESET_NET_TIMER(fmTrigger.timeReserveDmSlot)
				START_NET_TIMER(fmTrigger.timeReserveDmSlot)
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[PROCESS_RESERVE_MISSION_SLOT] Not enough players for DM fmTrigger.iReservePlayerProg = ", fmTrigger.iReservePlayerProg) #ENDIF
				
			
			ENDIF
		BREAK
		
		CASE 99
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_RESERVE_SOLO_MISSION_SLOT(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	SWITCH fmTrigger.iReservePlayerProg
		CASE 0
			IF NOT HAS_NET_TIMER_STARTED(fmTrigger.timeReserveDmSlot)
			OR HAS_NET_TIMER_EXPIRED(fmTrigger.timeReserveDmSlot, 2000)
			
				#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(fmTrigger.iBitset, biFMt_DmFinalCheckForPlay)
						PRINT_TRIGGER_STRING("[PROCESS_RESERVE_SOLO_MISSION_SLOT] biFMt_DmFinalCheckForPlay is set!")
					ENDIF
				#ENDIF
				fmTrigger.reservationRequestUniqueID = NO_UNIQUE_ID
				IF Try_To_Reserve_Tutorial_Player_On_Joblist_Invite_Mission(fmTrigger,<<364.9413,278.7449,102.2490>> )
					fmTrigger.iReservePlayerProg++
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("fmTrigger.iReservePlayerProg = ", fmTrigger.iReservePlayerProg) #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[PROCESS_RESERVE_SOLO_MISSION_SLOT] Try_To_Reserve_Tutorial_Player_On_Joblist_Invite_Mission failed!") #ENDIF
					RESET_NET_TIMER(fmTrigger.timeReserveDmSlot)
					START_NET_TIMER(fmTrigger.timeReserveDmSlot)
				ENDIF
				
			ENDIF
		BREAK
		
		CASE 1
			IF NOT Has_Tutorial_MP_Mission_Request_Failed(fmTrigger)
		 		Process_Stage_Tutorial_Waiting_For_Reservation_Confirmation(fmTrigger)
			ELSE	
				fmTrigger.iReservePlayerProg = 0
				RESET_NET_TIMER(fmTrigger.timeReserveDmSlot)
				START_NET_TIMER(fmTrigger.timeReserveDmSlot)
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("MP mission request failed for some reason, resetting. fmTrigger.iReservePlayerProg = ", fmTrigger.iReservePlayerProg) #ENDIF
				
			
			ENDIF
		BREAK
		
		CASE 2
			//-- Need at least 1 players
			IF fmtrigger.iDmNumReservedPlayers = 1
			
				//-- Success!
				fmTrigger.iReservePlayerProg = 99
				#IF IS_DEBUG_BUILD 
					PRINT_TRIGGER_STRING_INT("[PROCESS_RESERVE_SOLO_MISSION_SLOT] Got confirmation fmTrigger.iReservePlayerProg = ", fmTrigger.iReservePlayerProg)
					PRINT_TRIGGER_STRING_INT("[PROCESS_RESERVE_SOLO_MISSION_SLOT] Got confirmation, Num players on mission = ", fmtrigger.iDmNumReservedPlayers)
				#ENDIF
			ELSE
				//-- Didn't find anyone else, try again
				fmTrigger.iReservePlayerProg = 0
				RESET_NET_TIMER(fmTrigger.timeReserveDmSlot)
				START_NET_TIMER(fmTrigger.timeReserveDmSlot)
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[PROCESS_RESERVE_SOLO_MISSION_SLOT] Not enough players for DM fmTrigger.iReservePlayerProg = ", fmTrigger.iReservePlayerProg) #ENDIF
				
			
			ENDIF
		BREAK
		
		CASE 99
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC DO_LAMAR_MOCAP_BACKGROUND_UPDATE(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	IF Is_This_The_Current_Objective_Text("FM_RTU_VAN")
		Clear_Any_Objective_Text_From_This_Script()
	ENDIF
	IF DOES_BLIP_EXIST(fmTrigger.blipLamar)
		REMOVE_BLIP(fmTrigger.blipLamar)
	ENDIF
	//-- Around lamar's van
	CLEAR_AREA_OF_VEHICLES(<<-674.7998, -962.1434, 19.6905>>, 300.0)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-581.5575, -1010.5590, 10.0>>, <<-573.4370, -995.0512, 25.0>>, FALSE, FALSE)
	CLEAR_AREA_OF_VEHICLES(<<-576.8201, -1004.2000, 21.1785>>, 30.0)
	fmTrigger.scenRace = ADD_SCENARIO_BLOCKING_AREA( <<-580.2809, -961.7731, 5.0>>, <<-547.3903, -1088.2806, 29.3219>>, FALSE)
	SET_ROADS_IN_ANGLED_AREA(  <<-567.693787,-982.881226,1.553646>>, <<-562.784729,-1085.963745,31.365965>>, 33.500000, FALSE, FALSE, FALSE)
	SET_PED_PATHS_IN_AREA(<<-558.6479, -969.5730, 10.0>>, <<-546.0167, -1092.4390, 25.2333>>, FALSE)
//	SET_BIT(fmtrigger.iBitset, biFMt_TimedClearArea)
	IF IS_VEHICLE_DRIVEABLE(fmtrigger.vehLamar)
		IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), fmtrigger.vehLamar)
			TASK_ENTER_VEHICLE( PLAYER_PED_ID(), fmtrigger.vehLamar, 1, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED )
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	PROC DO_TRIGGER_TUT_J_SKIPS(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
				AND NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					SWITCH fmTrigger.iFmTriggerIntroProg
						CASE FM_TRIGGER_INTRO_GET_IN_CAR
							IF IS_VEHICLE_DRIVEABLE(fmtrigger.vehLamar)
								IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), fmtrigger.vehLamar)
								//	TASK_ENTER_VEHICLE( PLAYER_PED_ID(), fmtrigger.vehLamar, 1, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED )
								ENDIF
							ENDIF
						BREAK
						
						CASE FM_TRIGGER_INTRO_START_MOCAP
					//		DO_LAMAR_MOCAP_BACKGROUND_UPDATE(fmtrigger)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-673.6775, -955.6306, 20.1033>>)
							ENDIF
						//	fmTrigger.iFmTriggerIntroProg =  FM_TRIGGER_INTRO_MOCAP_END
					//		REMOVE_CUTSCENE()
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Moving to FM_TRIGGER_INTRO_MOCAP_END from j-skip") #ENDIF
						BREAK
						
						CASE FM_TRIGGER_INTRO_DRIVE_TO_RACE
							IF IS_VEHICLE_DRIVEABLE(fmtrigger.vehLamar)
								SET_ENTITY_COORDS(fmtrigger.vehLamar, <<-576.6409, -998.7806, 21.1785>>)
								SET_ENTITY_HEADING(fmtrigger.vehLamar, 89.7010)
								fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_RACE_CUT
								IF NOT IS_PED_INJURED(fmtrigger.pedLamar)
									CLEAR_PED_TASKS(fmtrigger.pedLamar)
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

FUNC BOOL HAS_ANY_RESERVED_PLAYER_STARTED_MISSION_COUNTDOWN_TIMER()
	INT iUniqueId = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	#IF IS_DEBUG_BUILD
		PRINT_TRIGGER_STRING_INT("[HAS_ANY_RESERVED_PLAYER_STARTED_MISSION_COUNTDOWN_TIMER] iUniqueId = ", iUniqueId)  
	#ENDIF
	INT playerBitfield = Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission (iUniqueId)
	PLAYER_INDEX thePlayerID
	INT tempLoop
	INT iNumPlayers
	iNumPlayers = Get_Number_Of_Players_That_Reserved_Themselves_For_The_Mission(iUniqueId)
	#IF IS_DEBUG_BUILD
		PRINT_TRIGGER_STRING_INT("[HAS_ANY_RESERVED_PLAYER_STARTED_MISSION_COUNTDOWN_TIMER] Num reserved = ", iNumPlayers)  
	#ENDIF
	
	IF iNumPlayers > 0
		REPEAT NUM_NETWORK_PLAYERS tempLoop
			IF (IS_BIT_SET(playerBitfield, tempLoop))
				
				thePlayerID = INT_TO_PLAYERINDEX(tempLoop)
				IF (IS_NET_PLAYER_OK(thePlayerID, FALSE))
					IF thePlayerID <> PLAYER_ID()
						IF HAS_PLAYER_STARTED_WAIT_FOR_MISSION_PLAYERS_TIMEOUT(thePlayerID)
					//		SET_NET_TIMER_TO_SAME_VALUE_AS_TIMER(fmtrigger.timeLocalTImeOut, playerbd)
						//	MAX_DM_TUT_TIMEOUT = 60000 - (ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), GlobalplayerBD_FM[tempLoop].fmRaceTutorial.timeLaunchMission.Timer)))
							MAX_DM_TUT_TIMEOUT = 10000
							GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].fmRaceTutorial.timeLaunchMission  = GlobalplayerBD_FM[tempLoop].fmRaceTutorial.timeLaunchMission
							
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[HAS_ANY_RESERVED_PLAYER_STARTED_MISSION_COUNTDOWN_TIMER] Found someone reserved, setting MAX_DM_TUT_TIMEOUT = ", MAX_DM_TUT_TIMEOUT) #ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UNRESERVE_LOCAL_PLAYER_FOR_TUTORIAL_MISSION()
	INT iUniqueId = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	Broadcast_Cancel_Mission_Reservation_By_Player(iUniqueId)
	
ENDPROC

FUNC INT GET_DRIVER_OF_TUTORIAL_MISSION_VEH()
	#IF IS_DEBUG_BUILD
		PRINTLN("[dsw] [GET_DRIVER_OF_TUTORIAL_MISSION_VEH] Called... ") 
		PRINTLN("[dsw] [GET_DRIVER_OF_TUTORIAL_MISSION_VEH] GlobalplayerBD_FM[PLAYER_ID()].fmRaceTutorial.iMyTutRacePos =  ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iMyTutRacePos) 
	#ENDIF
	INT iDriver = -1
	INT iBestPos = 100
	INT iUniqueId = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	
	#IF IS_DEBUG_BUILD
		IF iUniqueId = NO_UNIQUE_ID
			 PRINTLN("[dsw] [GET_DRIVER_OF_TUTORIAL_MISSION_VEH] iUniqueId = NO_UNIQUE_ID! ") 
		ENDIF
		
	#ENDIF
	INT playerBitfield = Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission (iUniqueId)
	
	INT tempLoop = 0
	
	PLAYER_INDEX thePlayerID
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(playerBitfield, tempLoop))
			
			thePlayerID = INT_TO_PLAYERINDEX(tempLoop)
			IF (IS_NET_PLAYER_OK(thePlayerID, FALSE))
				#IF IS_DEBUG_BUILD PRINTLN("[dsw] [GET_DRIVER_OF_TUTORIAL_MISSION_VEH] Think this player is on the mission... ", GET_PLAYER_NAME(thePlayerID)) #ENDIF
				IF GlobalplayerBD_FM[tempLoop].fmRaceTutorial.iMyTutRacePos < iBestPos
					#IF IS_DEBUG_BUILD PRINTLN("[dsw] [GET_DRIVER_OF_TUTORIAL_MISSION_VEH] Players race pos is better, race pos =  ", GlobalplayerBD_FM[tempLoop].fmRaceTutorial.iMyTutRacePos) #ENDIF
					iBestPos = GlobalplayerBD_FM[tempLoop].fmRaceTutorial.iMyTutRacePos
					iDriver = tempLoop
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	
	
	IF iDriver > -1
		PRINTLN("[dsw] [GET_DRIVER_OF_TUTORIAL_MISSION_VEH] Think this player did best in the race", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iDriver)))
		PRINTLN("[dsw] [GET_DRIVER_OF_TUTORIAL_MISSION_VEH] They finished in this postion... ", iBestPos)
	ELSE	
		iDriver = NATIVE_TO_INT(PLAYER_ID())
		PRINTLN("[dsw] [GET_DRIVER_OF_TUTORIAL_MISSION_VEH] Failed to find a driver!")
	ENDIF
	
	
	RETURN iDriver
ENDFUNC

PROC SET_MISSION_AMBIENT_RACECAR_VEHICLE_MODELS(INT iDriver)
	INT iUniqueId = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	
	#IF IS_DEBUG_BUILD
		IF iUniqueId = NO_UNIQUE_ID
			 PRINTLN("[dsw] [SET_MISSION_AMBIENT_RACECAR_VEHICLE_MODELS] iUniqueId = NO_UNIQUE_ID! ") 
		ENDIF
		
	#ENDIF
	INT playerBitfield = Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission (iUniqueId)
	
	INT tempLoop = 0

	/*
		Will need to initialise  - maybe just leave them as they are?
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[1ST VEHICLE].mn = DUMMY_MODEL_FOR_SCRIPT
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[2ND VEHICLE].mn = DUMMY_MODEL_FOR_SCRIPT
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[3RD VEHICLE].mn = DUMMY_MODEL_FOR_SCRIPT
	*/
	PLAYER_INDEX thePlayerID
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(playerBitfield, tempLoop))
			IF tempLoop <> iDriver
				thePlayerID = INT_TO_PLAYERINDEX(tempLoop)
				IF (IS_NET_PLAYER_OK(thePlayerID, FALSE))
					PRINTLN("[dsw] [SET_MISSION_AMBIENT_RACECAR_VEHICLE_MODELS] Think this player is on the mission... ", GET_PLAYER_NAME(thePlayerID)) 
					
					/*
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[1ST VEHICLE].mn = DUMMY_MODEL_FOR_SCRIPT
						g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[1ST VEHICLE].mn = GlobalplayerBD_FM[tempLoop].fmRaceTutorial.modelMyRaceCar
						PRINTLN("[dsw] [SET_MISSION_AMBIENT_RACECAR_VEHICLE_MODELS] Set 1st ambient race car to model... ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GlobalplayerBD_FM[tempLoop].fmRaceTutorial.modelMyRaceCar))
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[2ND VEHICLE].mn = DUMMY_MODEL_FOR_SCRIPT
						g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[2ND VEHICLE].mn = GlobalplayerBD_FM[tempLoop].fmRaceTutorial.modelMyRaceCar
						PRINTLN("[dsw] [SET_MISSION_AMBIENT_RACECAR_VEHICLE_MODELS] Set 2nd ambient race car to model... ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GlobalplayerBD_FM[tempLoop].fmRaceTutorial.modelMyRaceCar))
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[3RD VEHICLE].mn = DUMMY_MODEL_FOR_SCRIPT
						g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[3RD VEHICLE].mn = GlobalplayerBD_FM[tempLoop].fmRaceTutorial.modelMyRaceCar
						PRINTLN("[dsw] [SET_MISSION_AMBIENT_RACECAR_VEHICLE_MODELS] Set 3rd ambient race car to model... ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GlobalplayerBD_FM[tempLoop].fmRaceTutorial.modelMyRaceCar))
					ENDIF
					*/
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	/* 
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[1ST VEHICLE].mn = DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[1ST VEHICLE].mn =
			PRINTLN("[dsw] [SET_MISSION_AMBIENT_RACECAR_VEHICLE_MODELS] Failed to set 1st ambient race car ")
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[2ND VEHICLE].mn = DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[2ND VEHICLE].mn =
			PRINTLN("[dsw] [SET_MISSION_AMBIENT_RACECAR_VEHICLE_MODELS] Failed to set 2nd ambient race car ")
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[3RD VEHICLE].mn = DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[3RD VEHICLE].mn =
			PRINTLN("[dsw] [SET_MISSION_AMBIENT_RACECAR_VEHICLE_MODELS] Failed to set 3rd ambient race car ")
		ENDIF
	*/
ENDPROC

PROC SET_VEHICLE_TO_USE_FOR_TUTORIAL_MISSION(/*FM_TRIGGER_INTRO_STRUCT &fmTrigger*/)
	INT iUniqueId = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	
	INT playerBitfield = Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission (iUniqueId)
	
	IF playerBitfield = 0
		#IF IS_DEBUG_BUILD
		PRINT_TRIGGER_STRING("[SET_VEHICLE_TO_USE_FOR_TUTORIAL_MISSION] Exit as playerBitfield = 0") 
		#ENDIF
		EXIT
	ENDIF
	
//	IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_SetMissionVehicle)
		INT iDriver = GET_DRIVER_OF_TUTORIAL_MISSION_VEH()
		MODEL_NAMES modelForMission = GlobalplayerBD_FM[iDriver].fmRaceTutorial.modelMyRaceCar
		IF modelForMission = DUMMY_MODEL_FOR_SCRIPT
			#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[SET_VEHICLE_TO_USE_FOR_TUTORIAL_MISSION] Failed to find vehicle! Setting ASEA") #ENDIF
			modelForMission = ASEA //BJXL
		ENDIF
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[0].mn = modelForMission
	//	SET_BIT(fmTrigger.iBitset2, biFmt2_SetMissionVehicle)
		#IF IS_DEBUG_BUILD 
		//	PRINT_TRIGGER_STRING("[SET_VEHICLE_TO_USE_FOR_TUTORIAL_MISSION] Set biFmt2_SetMissionVehicle") 
			PRINT_TRIGGER_STRINGS("[SET_VEHICLE_TO_USE_FOR_TUTORIAL_MISSION] g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[0].mn = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[0].mn ))
		#ENDIF
//	ENDIF
ENDPROC





// ADDED BY KEITH 2/4/13 to handle the auto-launch of the tutorial mission - TEMPLATE FUNCTION
//BOOL cloudDataLoaded	= FALSE
PROC Maintain_MissionsAtCoords_Tutorial_Mission_InCorona(FM_TRIGGER_INTRO_STRUCT &fmTrigger, BOOL bSoloMission = FALSE)

	// DAVE: STEP 1 -	CALL THIS ONCE FOR EVERY PLAYER WHEN THEY REACH THIS MISSION IN THE TUTORIAL BEFORE YOU DO ANYTHING WITH THE MISSION.
	//					DON'T CALL IT FROM IN HERE - I'VE JUST PLACED IT HERE FOR REFERENCE - THE COORDS ARE THE COORDS OF THE MISSION.
	
	//Change_MissionsAtCoords_InCorona_External_Function_ID_For_Tutorial(<<-73.7516, -1462.5665, 30.5280>>, MATCICE_FM_TUTORIAL)


	
	// DAVE: STEP 2 -	THE NEXT TWO FUNCTION CALLS CHECK IF THE PLAYER IS IN THE CORONA OF YOUR MISSION.
	//					PROBABLY BEST TO CALL THESE EVERY FRAME FOR EVERY PLAYER WHEN THEY REACH THIS MISSION IN THE TUTORIAL.
	
	
	// Is the player in a MissionsAtCoords corona that requires InCorona Actions to be processed?
	IF NOT (Should_External_Scripts_Process_MissionsAtCoords_InCorona_Actions())
		// ...the player isn't in a corona that requires External Actions to be processed
		#IF IS_DEBUG_BUILD	NET_NL() NET_PRINT("[dsw] Maintain_MissionsAtCoords_Tutorial_Mission_InCorona Exit 1!") NET_NL()#ENDIF
		EXIT
	ENDIF
	
	
						
	
	// InCorona Actions processing is required, so check if it is this routine that should be doing the processing
	IF (Get_MissionsAtCoords_InCorona_Actions_ExternalID_To_Process() != MATCICE_FM_TUTORIAL)
		// ...it isn't this script that should be processing the current mission's InCorona Actions
		#IF IS_DEBUG_BUILD	NET_NL() NET_PRINT("[dsw] Maintain_MissionsAtCoords_Tutorial_Mission_InCorona Exit 2!") NET_NL()#ENDIF
		EXIT
	ENDIF
	

	// DAVE: STEP 3 -	WHEN PROCESSING REACHES HERE IT MEANS THIS PLAYER IS IN THE CORONA OF THE TUTORIAL MISSION.
	//					THIS IS THE START OF THE HANDSHAKING BETWEEN THIS FUNCTION AND MY CORONA CONTROL STUFF.
	//					DO ANY INITIALISATION HERE (I DON'T THINK YOU'LL NEED TO DO ANYTHING - MAYBE TAKE PLAYER CONTROL OFF)
	//						THEN TELL MY STUFF THAT YOU'VE FINISHED DOING INITIALISATION, ALSO CLEAR THE CLOUD-DATA LOADING CONTROL VARIABLES.
	
	IF (Is_MissionsAtCoords_InCorona_Stage_Initialise())
		//cloudDataLoaded = FALSE
		CLEAR_BIT(fmTrigger.iBitset2, biFmt2_CloudDataLoaded)
		Clear_Cloud_Loaded_Mission_Data()
		
		// Switch player control off
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		// Let Missions At Coords system know that initialisation is done and that th eplayer can be reserved for the mission
		Set_MissionsAtCoords_InCorona_Stage_As_Initialised()
		Set_MissionsAtCoords_InCorona_Stage_As_Allow_Reservation()
		
		EXIT
	ENDIF
	
	
	
	// DAVE: STEP 4 -	INITIALISATION HAS BEEN DONE. WE WANT TO PRE-LOAD THE MISSION DATA FROM THE CLOUD.
	//					AS A SAFETY PRECAUTION I MAKE SURE THE CLOUD-LOADING SCRIPT IS RUNNING, BUT IT SHOULD BE ALREADY.
	//					WHEN THE CLOUD-LOADING FUNCTION RETURNS TRUE, TELL MY STUFF THAT THE CLOUD-DATA IS LOADED.
	
	// Load from cloud
//	IF NOT (cloudDataLoaded)
	MP_MISSION_ID_DATA	theMissionIdData 
	
	IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_CloudDataLoaded)
		// KGM 10/7/13: Wait here until the 'player reserved' handshaking from MissionsAtCoords is received - this is new and doesn't really apply here, but still need to wait for it
		IF NOT (Is_MissionsAtCoords_InCorona_Stage_Player_Reserved())
			#IF IS_DEBUG_BUILD
				PRINT_TRIGGER_STRING("Still waiting to be told the player is reserved for the mission.") NET_NL()
			#ENDIF
			
			EXIT
		ENDIF
		
		// Safety Check - relaunch the script if it isn't launched (this should never be the case)
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("net_cloud_mission_loader")) = 0)
			REQUEST_SCRIPT("net_cloud_mission_loader")
	    	IF (HAS_SCRIPT_LOADED("net_cloud_mission_loader"))
			    START_NEW_SCRIPT("net_cloud_mission_loader", FRIEND_STACK_SIZE)
			    SET_SCRIPT_AS_NO_LONGER_NEEDED("net_cloud_mission_loader")
			ELSE
				#IF IS_DEBUG_BUILD
	        		NET_PRINT("FM_Trigger_Tut: Still waiting for script to launch: net_cloud_mission_loader") NET_NL()
				#ENDIF
				
				EXIT
			ENDIF
		ENDIF

		theMissionIdData = Get_MissionsAtCoords_InCorona_Actions_MissionID_Data()
		
		// Load the mission data from the cloud
		// NOTE: KGM 23/9/13, use specific offline content - all tutorials are now offline (NOTE: longer string that a contentID)
		TEXT_LABEL_31	offlineID		= GET_CLOUD_FILE_NAME_TO_USE(FMMC_TYPE_MISSION, bSoloMission)
		BOOL			useOfflineID	= TRUE
		
		IF NOT (Load_Cloud_Loaded_Mission_Data(theMissionIdData, offlineID, useOfflineID))
			#IF IS_DEBUG_BUILD
				PRINT_TRIGGER_STRING("Still waiting for the Cloud-Loaded Mission Data to be available.") NET_NL()
			#ENDIF
			
			EXIT
		ENDIF
		
		IF NOT Did_Cloud_Loaded_Mission_Data_Load_Successfully()
			#IF IS_DEBUG_BUILD
				PRINT_TRIGGER_STRING("Did_Cloud_Loaded_Mission_Data_Load_Successfully failed!!!") NET_NL()
			#ENDIF
			
			SET_IS_CLOUD_DOWN_TRANSITION(TRUE)
			SET_QUIT_CURRENT_GAME(TRUE)

			#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("MAINTAIN_SCRIPT_BAILS - Cloud is down as Did_Cloud_Loaded_Mission_Data_Load_Successfully = FALSE - Bail ")
			#ENDIF

			IF IS_TRANSITION_ACTIVE() = FALSE
				TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)                        
			ENDIF

			EXIT
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("[dsw] FM_Trigger_Tut: Cloud-Loaded Mission Data is now available.") NET_NL()
		#ENDIF
	
		// Cloud-Load complete
		Set_MissionsAtCoords_InCorona_Stage_As_Cloud_Data_Loaded()
		SET_LOCAL_PLAYER_LOADED_CLOUD_DATA_FOR_TUTORIAL_MISSION(TRUE)
		//cloudDataLoaded = TRUE
		SET_BIT(fmTrigger.iBitset2, biFmt2_CloudDataLoaded)
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = 0
		// KGM 16/4/13: For leaderboards to work properly, need to store the filename in playerBD
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName = theMissionIdData.idCloudFilename
		
		
		//DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[Mission Launch] Set GlobalplayerBD_FM[iMyGBD].sClientCoronaData.iTeamChosen = 0")  #ENDIF
		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRINGS("[Mission Launch] Set GlobalplayerBD_FM[iMyGBD].tl31CurrentMissionFileName = ", theMissionIdData.idCloudFilename)  #ENDIF
		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[Mission Launch] Set biFmt2_CloudDataLoaded") #ENDIF
		EXIT
	ENDIF
	
	
	
	// DAVE: STEP 5 -	THE MISSION HAS BEEN LOADED FORM THE CLOUD. WHEN YOU ARE READY, TELL MY STUFF TO LAUNCH THE MISSION.
	//					ONLY ONE PLAYER NEEDS TO DO THIS - BUT ALL PLAYERS CAN DO IT IF YOU WANT - IT WON'T BREAK ANYTHING.
	

	#IF IS_DEBUG_BUILD
	IF (IS_DEBUG_KEY_JUST_PRESSED(KEY_F9, KEYBOARD_MODIFIER_SHIFT, "TuT test"))
		NET_PRINT("KGMTEMP F9-------> Start Mission") NET_NL()
		Set_MissionsAtCoords_External_InCorona_Actions_To_Start_Mission()
	ENDIF
	#ENDIF
	
	IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_LaunchedMission)
		IF NOT bSoloMission
			SET_VEHICLE_TO_USE_FOR_TUTORIAL_MISSION(/*fmTrigger*/)
			
			IF ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MISSION(#IF IS_DEBUG_BUILD fmtrigger.timeDebug #ENDIF)
				
				IF IS_BIT_SET(fmTrigger.iBitset2, biFmt2_StartMission) // set when cloud data is loaded
					#IF IS_DEBUG_BUILD NET_PRINT("KGMTEMP F9-------> Start Mission") NET_NL() #ENDIF
					Set_MissionsAtCoords_External_InCorona_Actions_To_Start_Mission()
					SET_BIT(fmtrigger.iBitset2, biFmt2_LaunchedMission)
					RESET_TUTORIAL_VOICE_CHAT()
				ELSE	
					PRINTLN("Else 7")
				ENDIF
			ELSE
			//	PRINTLN("Else 6")
			ENDIF
		ELSE
			IF ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MISSION(#IF IS_DEBUG_BUILD fmtrigger.timeDebug #ENDIF)
				SET_VEHICLE_TO_USE_FOR_TUTORIAL_MISSION(/*fmTrigger*/)
				IF IS_BIT_SET(fmTrigger.iBitset2, biFmt2_StartMission) // set when cloud data is loaded
//					IF NOT g_bFailedTutorialMission
//					OR (g_bFailedTutorialMission AND IS_SKYSWOOP_AT_GROUND())
						IF g_bFailedTutorialMission
							ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
							SET_SKYFREEZE_FROZEN()
							#IF IS_DEBUG_BUILD 
								PRINT_TRIGGER_STRING("SET_SKYFREEZE_FROZEN as g_bFailedTutorialMission")
							#ENDIF
						ENDIF
						#IF IS_DEBUG_BUILD NET_PRINT("KGMTEMP F9-------> Start SOLO Mission") NET_NL() #ENDIF
						Set_MissionsAtCoords_External_InCorona_Actions_To_Start_Mission()
						SET_BIT(fmtrigger.iBitset2, biFmt2_LaunchedMission)
						RESET_TUTORIAL_VOICE_CHAT()
			//		ENDIF
				ELSE	
					PRINTLN("Else 7")
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_SetMissionType)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						IF IS_SCREEN_FADED_IN()
							//theMissionIdData = Get_MissionsAtCoords_InCorona_Actions_MissionID_Data()
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION //Convert_MissionID_To_FM_Mission_Type(theMissionIdData.idMission)
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl23CurrentMissionOwner = "Rockstar"
							SET_BIT(fmTrigger.iBitset2, biFmt2_SetMissionType)
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[Mission Launch] Set biFmt2_SetMissionType")  #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// DAVE: STEP 6 -	AFTER THE MISSION HAS LAUNCHED (OR AFTER IT HAS BEEN PLAYED), ALLOW THE MISSION TO USE THE NORMAL IN-CORONA TRIGGERING ROUTINES AGAIN.
	//					PROBABLY DON'T CALL IT FROM IN HERE - I'VE JUST PLACED IT HERE FOR REFERENCE - THE COORDS ARE THE COORDS OF THE MISSION.
	
	//Change_MissionsAtCoords_InCorona_External_Function_ID_For_Tutorial(<<-73.7516, -1462.5665, 30.5280>>, MATCICE_FMMC_LAUNCHER)
	
ENDPROC

FUNC BOOL STORE_PLAYERS_ON_RACE()
	
	
	IF NOT HAS_LOCAL_PLAYER_STARTED_TUTORIAL_RACE() // Set in net_races.sch
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD 
		PRINT_TRIGGER_STRING("[STORE_PLAYERS_ON_RACE] Called...")
	#ENDIF
	
	PLAYER_INDEX player
	INT i
	INT iHandleIndex = 0
	
	
	//-- Initialise
	FOR i = 0 TO 7
		CLEAR_GAMER_HANDLE_STRUCT(g_handleTutorialPlayers[i])
	ENDFOR
	g_iPlayersOnMyTutorialRace = 0
	
	//-- STore the handle of everyone on my race who is on the tutorial
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		IF iHandleIndex < 8
			player = INT_TO_PLAYERINDEX(i)
			IF IS_NET_PLAYER_OK(player)
				IF IS_PLAYER_ON_ANY_RACE(player)
					IF NOT HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(player)
						#IF IS_DEBUG_BUILD 
							PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[STORE_OTHER_PLAYERS_ON_RACE] Think this player is on the race: ", player)
						#ENDIF
						g_handleTutorialPlayers[iHandleIndex] = GET_GAMER_HANDLE_PLAYER(player)
						g_iPlayersOnMyTutorialRace++
						iHandleIndex++
					ELSE
						#IF IS_DEBUG_BUILD 
							PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[STORE_OTHER_PLAYERS_ON_RACE] Think this player is on the race, but completed tutorial ", player)
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD 
						PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[STORE_OTHER_PLAYERS_ON_RACE] Think this player is in the session, but not on the race ", player)
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD 
					IF IS_NET_PLAYER_OK(player, FALSE)
						PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[STORE_OTHER_PLAYERS_ON_RACE] Think this player is in the session, but not ok ", player)
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
					
	RETURN TRUE				
ENDFUNC

FUNC BOOL DOES_HANDLE_MATCH_ANY_ON_TUT_RACE(GAMER_HANDLE &handle)
	INT i
	
	FOR i = 0 TO 7
		IF IS_GAMER_HANDLE_VALID(g_handleTutorialPlayers[i])
			#IF IS_DEBUG_BUILD 
				PRINT_TRIGGER_STRING_INT("[DOES_HANDLE_MATCH_ANY_ON_TUT_RACE] Handle valid... ", i)
			#ENDIF
			IF NETWORK_ARE_HANDLES_THE_SAME(g_handleTutorialPlayers[i], handle)
				RETURN TRUE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD 
				PRINT_TRIGGER_STRING_INT("[DOES_HANDLE_MATCH_ANY_ON_TUT_RACE] Handle not valid... ", i)
			#ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC


PROC OUTPUT_PLAYERS_ON_RACE()
	#IF IS_DEBUG_BUILD 
		PRINT_TRIGGER_STRING("[OUTPUT_PLAYERS_ON_RACE] Called...")
		PRINT_TRIGGER_STRING_INT("[OUTPUT_PLAYERS_ON_RACE] Number of players on my race = ", GET_NUMBER_OF_PLAYERS_WHO_WERE_ON_MY_RACE())
	#ENDIF
	
	INT i
	PLAYER_INDEX player
	GAMER_HANDLE handleTemp
	
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		player = INT_TO_PLAYERINDEX(i)
		IF IS_NET_PLAYER_OK(player)
			#IF IS_DEBUG_BUILD 
				PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[OUTPUT_PLAYERS_ON_RACE] Checking player... ", player)
			#ENDIF
			handleTemp = GET_GAMER_HANDLE_PLAYER(player)
			IF DOES_HANDLE_MATCH_ANY_ON_TUT_RACE( handleTemp)
				#IF IS_DEBUG_BUILD 
					PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[OUTPUT_PLAYERS_ON_RACE] Think this player was on my race... ", player)
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD 
					PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[OUTPUT_PLAYERS_ON_RACE] This player in my session, but wasn't on my race... ", player)
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC STORE_RACE_LEADERBOARD_INFO()
	#IF IS_DEBUG_BUILD 
		PRINT_TRIGGER_STRING("[STORE_RACE_LEADERBOARD_INFO] called....")
		PRINT_TRIGGER_STRING_INT("[STORE_RACE_LEADERBOARD_INFO] My player Int = ", NATIVE_TO_INT(PLAYER_ID()))
	#ENDIF
	INT i
	INT indexToUse
	BOOL bFoundHandle
	GAMER_HANDLE myHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		IF NOT bFoundHandle
			IF IS_GAMER_HANDLE_VALID(g_TransitionSessionNonResetVars.sPLLB_Globals.GamerHandle[i])
				IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sPLLB_Globals.GamerHandle[i], myHandle)
					bFoundHandle = TRUE
					indexToUse = i
					#IF IS_DEBUG_BUILD 
						PRINT_TRIGGER_STRING_INT("[STORE_RACE_LEADERBOARD_INFO] Found handle, indexToUse = ", indexToUse)
						PRINT_TRIGGER_STRING_INT("[STORE_RACE_LEADERBOARD_INFO] sPLLB_Globals.iPositionInLB... ", g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[indexToUse]) 
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD 
					PRINT_TRIGGER_STRING_INT("[STORE_RACE_LEADERBOARD_INFO] Handle not valid! i = ", i)
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF bFoundHandle
		//-- Store my race pos in BD
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iMyTutRacePos = g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[indexToUse]
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iMyTutRacePos < 1
			#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[STORE_RACE_LEADERBOARD_INFO] Setting fmRaceTutorial.iMyTutRacePos = 99 as fmRaceTutorial.iMyTutRacePos =   ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iMyTutRacePos) #ENDIF 
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iMyTutRacePos = 99
		ENDIF
	//	GET_PLAYER_RADIO_STATION_NAME
		IF SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP()
			//-- 1508212
			#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[STORE_RACE_LEADERBOARD_INFO] Setting fmRaceTutorial.iMyTutRacePos = 99 as Joined from skyswoop") #ENDIF
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iMyTutRacePos = 99
		ENDIF
		
		IF g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] <> DUMMY_MODEL_FOR_SCRIPT
			IF NOT SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP()
				IF g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] = ASEA
				OR g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] = ASEA2
				OR g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] = ASTEROPE
				OR g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] = FUGITIVE
				OR g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] = PREMIER
				OR g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] = PRIMO
				OR g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] = SCHAFTER2
				OR g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] = STANIER
				OR g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] = TAILGATER
				OR g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] = WASHINGTON
				OR g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse] = SURGE
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[STORE_RACE_LEADERBOARD_INFO] Player race car is valid") #ENDIF
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.modelMyRaceCar = g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse]
				ELSE
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRINGS("[STORE_RACE_LEADERBOARD_INFO] Player race car is not a valid sedan. Will use ASEA Model is ",
						GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[indexToUse])) #ENDIF
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.modelMyRaceCar = ASEA
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[STORE_RACE_LEADERBOARD_INFO]  Found index but player joined from skyswoop. Using ASEA") #ENDIF
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.modelMyRaceCar = ASEA //BJXL
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[STORE_RACE_LEADERBOARD_INFO]  Found index but player race car NOT valid. Using ASEA") #ENDIF
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.modelMyRaceCar = ASEA// BJXL
		ENDIF
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.modColourType = g_TransitionSessionNonResetVars.sPLLB_Globals.iCarColourtype[indexToUse]
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iLivery = g_TransitionSessionNonResetVars.sPLLB_Globals.iCarColourselection[indexToUse]
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iCrewRed = -1
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iCrewGreen = -1
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iCrewBlue = -1
		
		GET_PLAYER_CREW_COLOUR(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iCrewRed, 
							  GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iCrewGreen, 
							  GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iCrewBlue)
	ELSE	
		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Failed to find handle! Using defaults") #ENDIF
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iMyTutRacePos = 99
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.modelMyRaceCar = ASEA // BJXL
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iLivery = -1
	ENDIF
	
	#IF IS_DEBUG_BUILD 
		PRINT_TRIGGER_STRING("[STORE_RACE_LEADERBOARD_INFO] Using values....")
		PRINT_TRIGGER_STRINGS("[STORE_RACE_LEADERBOARD_INFO] fmRaceTutorial.modelMyRaceCar = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.modelMyRaceCar))
		PRINT_TRIGGER_STRING_INT("[STORE_RACE_LEADERBOARD_INFO] fmRaceTutorial.iMyTutRacePos = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iMyTutRacePos) 
		PRINT_TRIGGER_STRING_INT("[STORE_RACE_LEADERBOARD_INFO] fmRaceTutorial.modColourType = ", ENUM_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.modColourType) )
		PRINT_TRIGGER_STRING_INT("[STORE_RACE_LEADERBOARD_INFO] fmRaceTutorial.iLivery = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iLivery) 
		PRINT_TRIGGER_STRING_INT("[STORE_RACE_LEADERBOARD_INFO] fmRaceTutorial.iCrewRed = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iCrewRed) 
		PRINT_TRIGGER_STRING_INT("[STORE_RACE_LEADERBOARD_INFO] fmRaceTutorial.iCrewGreen = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iCrewGreen) 
		PRINT_TRIGGER_STRING_INT("[STORE_RACE_LEADERBOARD_INFO] fmRaceTutorial.iCrewBlue = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].fmRaceTutorial.iCrewBlue) 
	#ENDIF
	
			
ENDPROC



// Keith 13/7/13: Advance Race Setup before initial cloud refresh complete
FUNC BOOL  Trigger_Tut_Race_Advance_Setup(VECTOR paramRaceStarPos, BOOL bSetExtendedRange = TRUE)

	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("KGM MP: Trigger_Tut_Race_Advance_Setup(): Attempting advance setup of Tutorial Race") #ENDIF

	INT raceVariation = -1
	INT tempLoop = 0
	REPEAT FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED tempLoop
		IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
		AND (raceVariation = -1)
			IF (ARE_VECTORS_ALMOST_EQUAL(paramRaceStarPos, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].vStartPos, 2.0))
				raceVariation = tempLoop
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP: Found Tutorial Race at Rockstar Created Array Position ")
					NET_PRINT_INT(tempLoop)
					NET_NL()
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF (raceVariation = -1)
		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("KGM MP: Trigger_Tut_Race_Advance_Setup(): Failed to find Tutorial Race on Rockstar Created Array") #ENDIF
		SCRIPT_ASSERT("Trigger_Tut_Race_Advance_Setup(): Failed to find Tutorial Race on Rockstar Created Array. Tell Keith or Dave.")
		RETURN FALSE
	ENDIF
	
	MP_MISSION_ID_DATA theMissionIdData
	Clear_MP_MISSION_ID_DATA_Struct(theMissionIdData)
	theMissionIdData.idMission			= eFM_RACE_CLOUD
	theMissionIdData.idVariation		= raceVariation
	theMissionIdData.idCreator			= FMMC_ROCKSTAR_CREATOR_ID
	theMissionIdData.idCloudFilename	= Get_FileName_For_FM_Cloud_Loaded_Activity(theMissionIdData.idCreator, theMissionIdData.idVariation)
	theMissionIdData.idSharedRegID		= ILLEGAL_SHARED_REG_ID
	
	g_eMPMissionSource		theSource			= MP_MISSION_SOURCE_MISSION_FLOW_BLIP
	VECTOR					theCoords			= Get_Coordinates_For_FM_Cloud_Loaded_Activity(theMissionIdData)
	g_structMatCBlipMP		theBlipDetails		= Gather_Blip_Details_For_Shared_Mission(theMissionIdData)
	g_structMatCCoronaMP	theCorona			= Gather_Corona_Details_For_Shared_Mission(theMissionIdData)
	g_structMatCOptionsMP	theOptions			= Gather_Optional_Flags_For_Shared_Mission(theMissionIdData)

	INT atCoordsID = Register_MP_Mission_At_Coords(theSource, theCoords, theBlipDetails, theCorona, theOptions, theMissionIdData)
	IF (atCoordsID = ILLEGAL_AT_COORDS_ID)
		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("KGM MP: Trigger_Tut_Race_Advance_Setup(): Failed advance setup of tutorial race") #ENDIF
		SCRIPT_ASSERT("Trigger_Tut_Race_Advance_Setup(): Advance setup of Tutorial Race failed. Tell Keith or Dave.")
		RETURN FALSE
	ENDIF
	
	Temporarily_Unlock_MissionsAtCoords_Mission_For_Tutorial(theCoords, TRUE)
	IF bSetExtendedRange
		Change_MissionsAtCoords_Use_Extended_Name_Display_Range_For_Tutorial(theCoords, TRUE)
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC LOCK_NON_SOLO_TUTORIAL_MISSION(VECTOR vLoc)
	#IF IS_DEBUG_BUILD 
	CPRINTLN(DEBUG_MP_TUTORIAL, " [dsw] [TriggerTut] [LOCK_NON_SOLO_TUTORIAL_MISSION] Called  vLoc = ",vLoc)
	#ENDIF
	
	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT	tempLoop = 0
	
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), vLoc, MISSION_COORDS_TOLERANCE_m))
				Clear_MissionsAtCoords_Mission_As_Temporarily_Unlocked_Until_Played(tempLoop)			
				#IF IS_DEBUG_BUILD 
				PRINT_TRIGGER_STRING("[LOCK_NON_SOLO_TUTORIAL_MISSION] Success!")
				#ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD 
	PRINT_TRIGGER_STRING("[LOCK_NON_SOLO_TUTORIAL_MISSION] Failed to find mission!")
	#ENDIF
ENDPROC







FUNC BOOL DO_TRIGGER_TUT_CLIENT_WITH_MOCAP(FM_TRIGGER_INTRO_STRUCT &fmTrigger)
	FLOAT fFloatX = 0.28
//	FLOAT fFloatY = 0.2
	FLOAT fDistance
	
	VECTOR vDMCoronaLoc 	= <<-895.0845, -1052.6971, 1.1631>>
	VECTOR vRaceCoronaLoc	= <<-210.3399,307.0503,95.9465>> // <<-214.7080, 308.8585, 95.9455>> //<<-706.2501,-974.4005,19.3900>> //
	VECTOR vMissionLoc 		= <<367.2988,286.2505,102.3401>> // <<-688.2597, -982.5331, 19.3901>> USed in Try_To_Reserve_Tutorial_Player_On_Joblist_Invite_Mission() as well
	VECTOR vSoloMissionLoc 	= <<364.9413,278.7449,102.2490>>
	INT instance 
	/*
		IS_MENU_HOST_ALLOWED_TO_CHANGE_THIS_OPTION 
	*/
	
	/*
		Tutorial stuff in
		FMMC_PROCESS_PRE_GAME_COMMON() - Race launch
		
		GET_SHOULD_BE_DOING_RACE_TUTORIAL_CLEANUP -- Job cleanup
		
		RESET_FMMC_MISSION_VARIABLES - Race Cleanup
	
	*/
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchTutorialFromIntroCut)
	//	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Hit 1") #ENDIF
		IF NOT HAS_FM_RACE_TUT_BEEN_DONE()
		//	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Hit 2") #ENDIF
			IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_RenderedCams)
		//		RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_BIT(fmTrigger.iBitset2, biFmt2_RenderedCams)
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set biFmt2_RenderedCams") #ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_CreateAmbRacers)
				IF NOT DOES_ENTITY_EXIST(fmTrigger.vehRacers[0])
					SET_BIT(fmTrigger.iBitset2, biFmt2_CreateAmbRacers)
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set biFmt2_CreateAmbRacers as biTrigTut_LaunchTutorialFromIntroCut") #ENDIF
				ENDIF
			ENDIF
			
			IF HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_ID())
			//	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Hit 3") #ENDIF
				IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFMt2_TryToUnlockRace)
					SET_BIT(fmTrigger.iBitset2, biFMt2_TryToUnlockRace)
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set biFMt2_TryToUnlockRace as biTrigTut_LaunchTutorialFromIntroCut") #ENDIF
				ENDIF
				
				 
				IF NOT IS_BIT_SET(fmTrigger.iBitset, biFmT_ForceRenderCorona)
				//	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Hit 4") #ENDIF
					IF NOT HAS_NET_TIMER_STARTED(fmTrigger.timeEnableCorona)
						START_NET_TIMER(fmTrigger.timeEnableCorona)
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Started timeEnableCorona") #ENDIF
					ELSE
						IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01 AND HAS_NET_TIMER_EXPIRED(fmTrigger.timeEnableCorona, 13000))
						OR (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01 AND HAS_NET_TIMER_EXPIRED(fmTrigger.timeEnableCorona, 18000))
						
							FORCE_RENDER_IN_GAME_UI(TRUE)
							SET_BIT(fmTrigger.iBitset, biFmT_ForceRenderCorona)
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("FORCE_RENDER_IN_GAME_UI Set biFmT_ForceRenderCorona") #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
			//	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Else 3") #ENDIF
			ENDIF
		ELSE
		//	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Else 2") #ENDIF
		ENDIF
	ELSE
	//	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Else 1") #ENDIF
	ENDIF
	
	SWITCH fmTrigger.iFmTriggerIntroProg
		CASE FM_TRIGGER_INTRO_INIT
			IF HAS_FM_INTRO_MISSION_BEEN_DONE()
			OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchTutorialFromIntroCut)
				IF NOT HAS_FM_DEATHMATCH_TUT_BEEN_DONE()
					IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
						IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
							
							instance = NATIVE_TO_INT(PLAYER_ID()) + NUM_NETWORK_PLAYERS
							NETWORK_ALLOW_GANG_TO_JOIN_TUTORIAL_SESSION(1, instance)
							GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iFmTutorialSession = 999999
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("Putting player on tut session... ", instance) #ENDIF
						ENDIF
					ELSE
						IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPreTutorialMissionInFlow
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPreTutorialMissionInFlow = TRUE
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING(" [wjk] Tutorial launching, setting bPreTutorialMissionInFlow = TRUE ") #ENDIF
						ENDIF
							
						INTERIOR_INSTANCE_INDEX interiorPlayer
						interiorPlayer = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
						IF interiorPlayer <> NULL
//							IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_UnlockedRace)
//								Temporarily_Unlock_MissionsAtCoords_Mission_For_Tutorial(vRaceCoronaLoc, TRUE)
//								#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Temporarily unlocked race - 1") #ENDIF
//								SET_BIT(fmTrigger.iBitset2, biFmt2_UnlockedRace)
//							ENDIF
							IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFMt2_TryToUnlockRace)
								SET_BIT(fmTrigger.iBitset2, biFMt2_TryToUnlockRace)
								#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set biFMt2_TryToUnlockRace - 1") #ENDIF
							ENDIF
						ENDIF
						
						//Block all invites
						SET_TUTORIAL_INVITES_ARE_BLOCKED()
						SET_STORE_ENABLED(FALSE)
						
						SET_LOCAL_PLAYERS_FM_MISSION_DECORATORS(FMMC_TYPE_GUN_INTRO, 0)	 // For 1046941
						SET_CORONA_WALK_IN_DISABLED_FOR_TUTORIAL(TRUE)
						GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iFmTutorialSession = 999999
						SET_BIT(fmTrigger.iBitset, biFMt_DoLamarMocap)
						
						SET_COUNTRYSIDE_AMMU_AVAILABLE(FALSE)
						SET_CITY_AMMU_AVAILABLE(FALSE)
						
						//-- New intro cut
						IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFMt2_TryToUnlockRace)
							IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchTutorialFromIntroCut)
								IF NOT HAS_FM_RACE_TUT_BEEN_DONE()
									SET_BIT(fmTrigger.iBitset2, biFMt2_TryToUnlockRace)
									#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set biFMt2_TryToUnlockRace - 3") #ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						
						
						//-- Use skyswoop to preload...
						IF NOT SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_RACE_FROM_SKYSWOOP()
						OR HAS_FM_RACE_TUT_BEEN_DONE()
						OR SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP()
							IF IS_SKYSWOOP_AT_GROUND()
							OR SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP()
						//	OR HAS_FM_RACE_TUT_BEEN_DONE() Will, this is breaking it, gets stuck when re-joing after quitting at mission stage
								fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_CREATE_LAMAR
								#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("CASE FM_TRIGGER_INTRO_INIT Player in tutorial session! Moving to FM_TRIGGER_INTRO_CREATE_LAMAR") #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("CASE FM_TRIGGER_INTRO_INIT waiting for IS_SKYSWOOP_AT_GROUND") #ENDIF
							ENDIF
						ELSE
							fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_SKYSWOOP_RACE
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("CASE FM_TRIGGER_INTRO_INIT Player in tutorial session! Moving to FM_TRIGGER_INTRO_SKYSWOOP_RACE") #ENDIF
						ENDIF 
						
						
						SET_LOCAL_PLAYER_IS_RUNNING_ACTIVITY_TUTORIAL(TRUE)
						SET_LOCAL_PLAYER_IS_IN_ACTIVITY_TUTORIAL_CORONA(TRUE)
						CLEAR_AREA( <<-674.7998, -962.1434, 19.6905>>, 30.0, TRUE)
						
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Running race/dm tut, but dm tut already done! Moving to FM_TRIGGER_INTRO_OFF_DM") #ENDIF
					
					fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_OFF_DM
				ENDIF 
			ELSE
				
			ENDIF
		BREAK
		
		CASE FM_TRIGGER_INTRO_SKYSWOOP_RACE
			IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFmt2_TookTransControl )
				
				TAKE_CONTROL_OF_TRANSITION(DEFAULT, FALSE)
				SET_SKYSWOOP_STAGE(SKYSWOOP_INSKYSTATIC)
				SET_BIT(fmtrigger.iBitset2, biFmt2_TookTransControl )
				NETWORK_OVERRIDE_CLOCK_TIME(0,0,0)
				SET_OVERRIDE_WEATHER("EXTRASUNNY")
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING("FM_TRIGGER_INTRO_SKYSWOOP_RACE Took control of transition for race")
				#ENDIF	
			ELSE
				IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFmt2_SetupForSwoop )
					IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_CreateAmbRacers)
						SET_BIT(fmTrigger.iBitset2, biFmt2_CreateAmbRacers)
					ENDIF
				//	IF CREATE_LAMAR_FOR_RACE_TUT(fmTrigger)
				//	IF CREATE_LAMAR_ON_PHONE_FOR_RACE_TUT(fmTrigger)
						REQUEST_MODEL(GET_LAMAR_VEHICLE_MODEL_FOR_TUTORIAL())
						REQUEST_NPC_PED_MODEL(CHAR_LAMAR)
						REQUEST_MODEL(PROP_PHONE_ING)
						REQUEST_ANIM_DICT("mp_intro_seq@ig_2_lamar_car_idle")
	
						SET_BIT(fmtrigger.iBitset2, biFmt2_SetupForSwoop )
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("FM_TRIGGER_INTRO_SKYSWOOP_RACE set biFmt2_SetupForSwoop") #ENDIF
				//	ENDIF
				ELSE
					IF SET_SKYSWOOP_DOWN()
						CLEAR_BIT(fmtrigger.iBitset2, biFmt2_SetupForSwoop )
						CLEAR_BIT(fmtrigger.iBitset2, biFmt2_TookTransControl )
						fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_CREATE_LAMAR
						ENABLE_ALL_MP_HUD()
						DISABLE_SCRIPT_HUD(HUDPART_AWARDS, FALSE)
						DISPLAY_HUD(TRUE)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						//	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE) // 1573225
						ENDIF
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), <<-210.3399,307.0503,95.9465>>, PEDMOVE_WALK)
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Sky swoop finished Moving to FM_TRIGGER_INTRO_CREATE_LAMAR") #ENDIF
					ELSE	
						IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_DisableHudForSwoop)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
							ENDIF
							
							DISABLE_ALL_MP_HUD()
							DISABLE_SCRIPT_HUD(HUDPART_AWARDS, TRUE)
							DISPLAY_HUD(FALSE)
							SET_BIT(fmTrigger.iBitset2, biFmt2_DisableHudForSwoop)
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Case FM_TRIGGER_INTRO_SKYSWOOP_RACE set biFmt2_DisableHudForSwoop") #ENDIF
						ENDIF
						CREATE_LAMAR_ON_PHONE_FOR_RACE_TUT(fmTrigger)
					ENDIF 
				ENDIF
			ENDIF
		BREAK
		
		CASE FM_TRIGGER_INTRO_CREATE_LAMAR
			IF NOT HAS_FM_RACE_TUT_BEEN_DONE()
				IF NOT HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_ID())
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE) // 1573225
				ENDIF
					
				//-- New intro
				fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_MOCAP_ACTIVE //FM_TRIGGER_ENTER_RACE_CORONA
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Player in tutorial session! Moving to FM_TRIGGER_INTRO_MOCAP_ACTIVE") #ENDIF
				
				//-- Old intro cut
				IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_CreateAmbRacers)
					SET_BIT(fmTrigger.iBitset2, biFmt2_CreateAmbRacers)
				ENDIF
				
				fmTrigger.scenRace = ADD_SCENARIO_BLOCKING_AREA(<<-230.5390, 259.7847, 50.0>>, <<-167.0463, 330.6501, 100.3809>>)
				
				g_bPassedMyTutorialMission = FALSE
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("FM_TRIGGER_INTRO_CREATE_LAMAR g_bPassedMyTutorialMission = FALSE - 1 ") #ENDIF
				
			ELSE
				
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_JoinedAtMissionStage)
					SET_BIT(fmTrigger.iBitset2, biFMt2_StraightToDm)
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set biFMt2_StraightToDm") #ENDIF
				ENDIF
				SET_BIT(fmTrigger.iBitset, biFmt_FinishedRace)
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set biFmt_FinishedRace") #ENDIF
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Already done Race tutorial. Moving to FM_TRIGGER_INTRO_DONE_RACE") #ENDIF
				
				IF NOT SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP()
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_FLOAT("NETWORK_GET_TALKER_PROXIMITY... ", NETWORK_GET_TALKER_PROXIMITY()) #ENDIF
					NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
					NETWORK_SET_TEAM_ONLY_CHAT(FALSE)
					NETWORK_SET_TALKER_PROXIMITY(0)
					NETWORK_SET_OVERRIDE_TUTORIAL_SESSION_CHAT(TRUE)
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set NETWORK_SET_OVERRIDE_TUTORIAL_SESSION_CHAT") #ENDIF
				ENDIF
				
				g_bPassedMyTutorialMission = FALSE
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("FM_TRIGGER_INTRO_CREATE_LAMAR g_bPassedMyTutorialMission = FALSE - 2 ") #ENDIF
				fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_DONE_RACE
			ENDIF 
		BREAK
		
		CASE FM_TRIGGER_INTRO_GET_IN_CAR
		BREAK
		
		CASE FM_TRIGGER_INTRO_START_MOCAP
//			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
//				IF NOT IS_PED_INJURED(fmTrigger.pedLamar)
//					IF IS_VEHICLE_DRIVEABLE(fmtrigger.vehLamar)
//						IF IS_BIT_SET(fmtrigger.iBitset, biFMt_LoseWanted)
//							Print_Objective_Text("FM_RTU_VAN") // Get in Lamar's van
//							fmTrigger.blipLamar = ADD_BLIP_FOR_ENTITY(fmTrigger.pedLamar)
//							SET_BLIP_AS_FRIENDLY(fmTrigger.blipLamar, TRUE)
//							CLEAR_BIT(fmtrigger.iBitset, biFMt_LoseWanted)
//							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Clearing biFMt_LoseWanted as lost wanted") #ENDIF
//						ENDIF
//					//	IF HAS_CUTSCENE_LOADED() 
//							IF (IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-671.280640,-946.869873,17.546852>>, <<-655.769104,-946.756042,25.869074>>, 2.000000)
//							OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-647.647095,-956.787476,19.021814>>, <<-679.994873,-957.755127,26.983400>>, 23.000000)
//							OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-679.889465,-974.332764,25.374746>>, <<-714.023865,-974.969849,15.830793>>, 55.437500))
//							AND NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-657.590332,-945.353149,20.100964>>, <<-667.149719,-945.591919,24.353344>>, 3.562500)
//								IF Is_This_The_Current_Objective_Text("FM_RTU_VAN")
//									Clear_Any_Objective_Text_From_This_Script()
//								ENDIF
//								IF DOES_BLIP_EXIST(fmTrigger.blipLamar)
//									REMOVE_BLIP(fmTrigger.blipLamar)
//								ENDIF
//								fmTrigger.iFmTriggerIntroProg =  FM_TRIGGER_INTRO_MOCAP_ACTIVE
//								#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Moving to FM_TRIGGER_INTRO_MOCAP_ACTIVE") #ENDIF
//							ENDIF
//						
//					//	ENDIF
//					
//					ENDIF
//				ENDIF
//			ELSE	
//				IF NOT IS_BIT_SET(fmtrigger.iBitset, biFMt_LoseWanted)
//					IF DOES_BLIP_EXIST(fmTrigger.blipLamar)
//						REMOVE_BLIP(fmTrigger.blipLamar)
//					ENDIF
//					Print_Objective_Text("FM_IHELP_LCP") // Lose the cops
//					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set biFMt_LoseWanted as have wanted") #ENDIF
//					SET_BIT(fmtrigger.iBitset, biFMt_LoseWanted)
//				ENDIF
//			ENDIF
		BREAK
		
		CASE FM_TRIGGER_INTRO_MOCAP_ACTIVE
			IF DO_WALK_TO_RACE_CORONA_CUT_V2(fmtrigger)
				IF IS_BIT_SET(fmTrigger.iBitset, biFmT_ForceRenderCorona)
					FORCE_RENDER_IN_GAME_UI(FALSE)
					CLEAR_BIT(fmTrigger.iBitset, biFmT_ForceRenderCorona)
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("FORCE_RENDER_IN_GAME_UI Cleared biFmT_ForceRenderCorona") #ENDIF
				ENDIF
				
				fmtrigger.iRaceClockHours = GET_CLOCK_HOURS()
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("Storing clock hours.... ", fmtrigger.iRaceClockHours) #ENDIF
				REMOVE_SCENARIO_BLOCKING_AREA(fmTrigger.scenRace)
				fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_ENTER_RACE_CORONA // FM_TRIGGER_INTRO_MOCAP_END
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Moving to FM_TRIGGER_ENTER_RACE_CORONA") #ENDIF
			ENDIF
		BREAK
		
		CASE FM_TRIGGER_INTRO_MOCAP_END
		
			
			fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_DRIVE_TO_RACE 
			#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Moving to FM_TRIGGER_INTRO_DRIVE_TO_RACE") #ENDIF
		BREAK
		
		CASE FM_TRIGGER_INTRO_DRIVE_TO_RACE

		BREAK
		
		CASE FM_TRIGGER_INTRO_PULL_OVER

		BREAK
		
		CASE FM_TRIGGER_INTRO_RACE_CUT

		BREAK
		
		CASE FM_TRIGGER_INTRO_WAIT_FOR_RACE_CUT
	
		BREAK
		
		CASE FM_TRIGGER_ENTER_RACE_CORONA
			IF NOT IS_BIT_SET(fmtrigger.iBitset, biDMt_WalkIntoCorona)
			//	Print_Objective_Text("FM_RTU_MAR") //Walk into the yellow marker to trigger the Race. 
				SET_BIT(fmtrigger.iBitset2, biFMt2_ToldToEnterCorona)
			ENDIF
			ADD_FAKE_BLIP_FOR_RACE(fmtrigger, vRaceCoronaLoc)
		//	SET_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_OnlyPlayerOnRace)
			fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_ON_RACE
			#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Moving to FM_TRIGGER_INTRO_ON_RACE") #ENDIF
		BREAK
		
		CASE FM_TRIGGER_INTRO_ON_RACE
		//	IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
		//		UNLOCK_FM_ACTIVITY(FMMC_TYPE_RACE, FALSE)
		//	ENDIF
			IF IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
			//	REMOVE_SCENARIO_BLOCKING_AREA(fmTrigger.scenRace)
				CLEANUP_AMBIENT_RACE_CARS(fmTrigger)
				CLEANUP_LAMAR(fmTrigger)
				IF Is_This_The_Current_Objective_Text("FM_RTU_MAR")
				OR Is_This_The_Current_Objective_Text("FM_IHELP_LCP")
					Clear_Any_Objective_Text_From_This_Script()
				ENDIF
				
				IF DOES_BLIP_EXIST(fmtrigger.blipTempRace)
					REMOVE_BLIP(fmtrigger.blipTempRace)
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Removed blipTempRace") #ENDIF
				ENDIF
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-581.5575, -1010.5590, 10.0>>, <<-573.4370, -995.0512, 25.0>>, TRUE, FALSE)
				REMOVE_SCENARIO_BLOCKING_AREA(fmTrigger.scenRace)
				SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(  <<-567.693787,-982.881226,1.553646>>, <<-562.784729,-1085.963745,31.365965>>, 33.500000)
				
		//		CLEAR_BIT(fmtrigger.iBitset, biFMt_TimedClearArea) 
				CLEAR_BIT(fmtrigger.iBitset, biFMt_SetRemoveFromTut)
				CLEAR_ALL_FLOATING_HELP()
			//	SET_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL_HELP(FALSE)
				SET_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL(FALSE)
				SET_LOCAL_PLAYER_DOING_ACTIVITY_TUTORIAL_RACE(TRUE)
				SET_TUTORIAL_CORONA_INTRO_TIMER_EXPIRED(TRUE)
				CLEAR_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_PlayerInFmActivityTutorialCorona)
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerInFmActivityTutorialCorona)
				fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_DONE_RACE
				
				
				
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_DONE_RACE as race started") #ENDIF
			ELSE
				CLEANUP_WALK_TO_CORONA_CAM(fmTrigger)
				//-- Lose wanted levele
				IF GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
						IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFMt2_ToldToLoseCops)
							CLEAR_BIT(fmtrigger.iBitset2, biFMt2_ToldToEnterCorona)
							Print_Objective_Text("FM_IHELP_LCP") // lose the cops
							SET_BIT(fmtrigger.iBitset2, biFMt2_ToldToLoseCops)
							Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_For_Tutorial(vRaceCoronaLoc, FALSE)
						ENDIF
					ELSE	
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							
							IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFMt2_ToldToEnterCorona)
								CLEAR_BIT(fmtrigger.iBitset2, biFMt2_ToldToLoseCops)
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRaceCoronaLoc) > 2500
									Print_Objective_Text("FM_RTU_GRA")
								ELSE
									Print_Objective_Text("FM_RTU_MAR")
								ENDIF
								SET_BIT(fmtrigger.iBitset2, biFMt2_ToldToEnterCorona)
								Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_For_Tutorial(vRaceCoronaLoc, TRUE)
							ELSE
								IF NOT Is_This_The_Current_Objective_Text("FM_RTU_GRA")
								AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRaceCoronaLoc) > 2500
									CLEAR_BIT(fmtrigger.iBitset2, biFMt2_ToldToEnterCorona)
									#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Clearing biFMt2_ToldToEnterCorona as VIDIST2 > 2500") #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(fmtrigger.iBitset, biFMt_DoWaitingScreenHelp )	
						CLEAR_BIT(fmTrigger.iBitset, biFMt_DoWaitingScreenHelp)
						#IF IS_DEBUG_BUILD 
							PRINT_TRIGGER_STRING_INT("Clearing biFMt_DoWaitingScreenHelp because GET_LOCAL_PLAYER_CORONA_POS_STATE() = ", GET_LOCAL_PLAYER_CORONA_POS_STATE())
						#ENDIF
					ENDIF
				ENDIF
				
				IF Is_This_The_Current_Objective_Text("FM_RTU_MAR")
				OR Is_This_The_Current_Objective_Text("FM_IHELP_LCP")
				OR Is_This_The_Current_Objective_Text("FM_RTU_GRA")
					IF GET_CORONA_STATUS() > CORONA_STATUS_IDLE	
						Clear_Any_Objective_Text_From_This_Script()
						
					ENDIF
				ENDIF
				
				//-- Remove from tut session when in corona
//				IF NOT IS_BIT_SET(fmtrigger.iBitset, biFMt_SetRemoveFromTut)					
//					IF GET_CORONA_STATUS() >= CORONA_STATUS_INIT_CORONA	
//						SET_BIT(fmtrigger.iBitset, biFMt_SetRemoveFromTut)
//						SET_BIT(fmtrigger.iBitset, biFMt_RemoveFromTutSess)
//						
//						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CASE FM_TRIGGER_INTRO_ON_RACE set biFMt_RemoveFromTutSess") #ENDIF
//					ENDIF
//				ENDIF
				MAINTAIN_DRIVE_CAR_INTO_CORONA(fmTrigger)
				
				IF GET_CORONA_STATUS() = CORONA_STATUS_INTRO
				//	CLEANUP_LAMAR(fmTrigger)
					
				//	CLEANUP_AMBIENT_RACE_CARS(fmTrigger)
					IF Is_This_The_Current_Objective_Text("FM_RTU_MAR")
					OR Is_This_The_Current_Objective_Text("FM_IHELP_LCP")
						Clear_Any_Objective_Text_From_This_Script()
					ENDIF
					IF NOT IS_BIT_SET(fmTrigger.iBitset, biFMt_DoneCoronaIntroHelp)
						
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Done info screen help ") #ENDIF
						SET_BIT(fmTrigger.iBitset, biFMt_DoneCoronaIntroHelp)
						RESET_NET_TIMER(fmtrigger.timeTrigIntro)
						START_NET_TIMER(fmtrigger.timeTrigIntro)
					ELSE	
						IF HAS_NET_TIMER_EXPIRED(fmtrigger.timeTrigIntro, 12500)
						#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
							IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_TRIG_FL5")
								CLEAR_THIS_FLOATING_HELP("FM_TRIG_FL5")
							ENDIF
							IF NOT HAS_TUTORIAL_CORONA_INTRO_TIMER_EXPIRED()
								SET_TUTORIAL_CORONA_INTRO_TIMER_EXPIRED(TRUE)
							ENDIF
						ENDIF
						
					ENDIF
				ELIF GET_CORONA_STATUS() > CORONA_STATUS_INTRO
				/*	IF IS_BIT_SET(fmTrigger.iBitset, biFMt_DoneCoronaIntroHelp)
						IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_TRIG_FL5")
							CLEAR_THIS_FLOATING_HELP("FM_TRIG_FL5")
						ENDIF
					ENDIF */
					IF IS_BIT_SET(fmTrigger.iBitset, biFmT_DoFloatingHelp)
						
						IF IS_SCREEN_FADED_IN()
							IF GET_CORONA_STATUS() <> CORONA_STATUS_LEADERBOARD// NOT IS_LOCAL_PLAYER_VIEWING_LEADERBOARD()
								IF GET_CORONA_STATUS() < CORONA_STATUS_BETTING
									SWITCH fmtrigger.iFloatingHelpProg
										CASE 0
										//	IF NOT IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_OnlyPlayerOnRace)
												// You are now in game with other people. Communicate with them using a headset. 
												
										//		HELP_AT_SCREEN_LOCATION("FM_TRIG_FL0", fFloatX, fFloatY, HELP_TEXT_EAST, -1, FLOATING_HELP_MISSION_SLOT)
												RESET_NET_TIMER(fmtrigger.timeTrigIntro)
												START_NET_TIMER(fmtrigger.timeTrigIntro)
												#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Printed FM_TRIG_FL0") #ENDIF
											//ELSE	
										//		RESET_NET_TIMER(fmtrigger.timeTrigIntro)
										//		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Not printng FM_TRIG_FL0 as biTrigTut_OnlyPlayerOnRace") #ENDIF
										//	ENDIF
										//	fmtrigger.iFloatingHelpProg = 1
											fmtrigger.iFloatingHelpProg = 1
										BREAK
										
										CASE 1
											IF GET_CORONA_STATUS() = CORONA_STATUS_IN_CORONA
												// Select your vehicle and color.
											//	HELP_AT_SCREEN_LOCATION("FM_TRIG_FL4", fFloatX, fFloatY, HELP_TEXT_EAST, -1, FLOATING_HELP_MISSION_SLOT)
												HELP_AT_SCREEN_LOCATION("FM_TRIG_FL4", 0.275, 0.475,  HELP_TEXT_NORTH, -1, FLOATING_HELP_MISSION_SLOT)
												RESET_NET_TIMER(fmtrigger.timeTrigIntro)
												START_NET_TIMER(fmtrigger.timeTrigIntro)
												fmtrigger.iFloatingHelpProg++
											ENDIF
										BREAK
										
										
										CASE 2
											IF HAS_NET_TIMER_EXPIRED(fmtrigger.timeTrigIntro, 12000)
											#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
											//	When you're ready, press ~INPUT_FRONTEND_SELECT~ to continue.
											//	HELP_AT_SCREEN_LOCATION("FM_TRIG_FL6", fFloatX, fFloatY, HELP_TEXT_EAST, -1, FLOATING_HELP_MISSION_SLOT)
												RESET_NET_TIMER(fmtrigger.timeTrigIntro)
												START_NET_TIMER(fmtrigger.timeTrigIntro)
												fmtrigger.iFloatingHelpProg++
											ELSE	
												IF NOT HAS_NET_TIMER_EXPIRED(fmtrigger.timeTrigIntro, 5000)
													IF NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_TRIG_FL4")
														HELP_AT_SCREEN_LOCATION("FM_TRIG_FL4", fFloatX, 0.14, HELP_TEXT_EAST, -1, FLOATING_HELP_MISSION_SLOT)
														RESET_NET_TIMER(fmtrigger.timeTrigIntro)
														START_NET_TIMER(fmtrigger.timeTrigIntro)
													ENDIF
												ENDIF
											ENDIF 
										BREAK
										
										CASE 3
											IF HAS_NET_TIMER_EXPIRED(fmtrigger.timeTrigIntro, 11000)
											#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
										/*		// Choose your Vehicle for the race, choose a Color for your Vehicle, and select Ready Up when you're ready to Race.
												HELP_AT_SCREEN_LOCATION("FM_TRIG_FL3", fFloatX, 0.14, HELP_TEXT_EAST, -1, FLOATING_HELP_MISSION_SLOT)
												RESET_NET_TIMER(fmtrigger.timeTrigIntro)
												START_NET_TIMER(fmtrigger.timeTrigIntro)
												fmtrigger.iFloatingHelpProg++ */
											ELSE
										/*		IF NOT HAS_NET_TIMER_EXPIRED(fmtrigger.timeTrigIntro, 8000)
													IF NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_TRIG_FL6")
														HELP_AT_SCREEN_LOCATION("FM_TRIG_FL6", fFloatX, fFloatY, HELP_TEXT_EAST, -1, FLOATING_HELP_MISSION_SLOT)
														RESET_NET_TIMER(fmtrigger.timeTrigIntro)
														START_NET_TIMER(fmtrigger.timeTrigIntro)
													ENDIF
												ENDIF */
											ENDIF
										BREAK
										
										CASE 4
											IF HAS_NET_TIMER_EXPIRED(fmtrigger.timeTrigIntro, 8000)
											#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
											//	SET_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL_HELP(FALSE)
												HELP_AT_SCREEN_LOCATION("FM_TRIG_FL4", fFloatX, 0.14, HELP_TEXT_EAST, -1, FLOATING_HELP_MISSION_SLOT)
												RESET_NET_TIMER(fmtrigger.timeTrigIntro)
												START_NET_TIMER(fmtrigger.timeTrigIntro)
											//	CLEAR_ALL_FLOATING_HELP()
												SET_UP_MENU_HELP(FMMC_TYPE_RACE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
												fmtrigger.iFloatingHelpProg++
											ELSE
												IF NOT HAS_NET_TIMER_EXPIRED(fmtrigger.timeTrigIntro, 5000)
													IF NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_TRIG_FL3")
														HELP_AT_SCREEN_LOCATION("FM_TRIG_FL3", fFloatX, 0.14, HELP_TEXT_EAST, -1, FLOATING_HELP_MISSION_SLOT)
														RESET_NET_TIMER(fmtrigger.timeTrigIntro)
														START_NET_TIMER(fmtrigger.timeTrigIntro)
													ENDIF
												ENDIF
											ENDIF
										BREAK
										
										CASE 5
											IF HAS_NET_TIMER_EXPIRED(fmtrigger.timeTrigIntro, 8000)
											#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
												
												CLEAR_ALL_FLOATING_HELP()
												//SET_UP_MENU_HELP(FMMC_TYPE_RACE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
												fmtrigger.iFloatingHelpProg++
											ELSE
												IF NOT HAS_NET_TIMER_EXPIRED(fmtrigger.timeTrigIntro, 5000)
													IF NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_TRIG_FL4")
														HELP_AT_SCREEN_LOCATION("FM_TRIG_FL4", fFloatX, 0.14, HELP_TEXT_EAST, -1, FLOATING_HELP_MISSION_SLOT)
														RESET_NET_TIMER(fmtrigger.timeTrigIntro)
														START_NET_TIMER(fmtrigger.timeTrigIntro)
													ENDIF
												ENDIF
											ENDIF
										BREAK
									ENDSWITCH
								ELSE
								 
								ENDIF
							ELSE	
								
							ENDIF
							IF GET_CORONA_STATUS() = CORONA_STATUS_BETTING
								
								IF IS_BETTING_AVAILABLE_AND_ACCESSIBLE()
									IF NOT IS_BIT_SET(fmTrigger.iBitset, biFMt_DoneBettingHelp)
										CLEAR_ALL_FLOATING_HELP()
										SET_BIT(fmTrigger.iBitset, biFMt_DoneBettingHelp)
										RESET_NET_TIMER(fmtrigger.timeTrigIntro)
										START_NET_TIMER(fmtrigger.timeTrigIntro)
										//This is where you can bet on people. Bet on the person you think is going to win. You can bet on multiple players.
									//	HELP_AT_SCREEN_LOCATION("FM_IHELP_BET", 0.72, 0.24, HELP_TEXT_WEST, -1, FLOATING_HELP_MISSION_SLOT) //0.14
									//	HELP_AT_SCREEN_LOCATION("FM_IHELP_BET", 0.275, 0.475, HELP_TEXT_NORTH, -1, FLOATING_HELP_MISSION_SLOT) 
										#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Displaying betting help") #ENDIF
									ELSE	
										IF HAS_NET_TIMER_EXPIRED(fmtrigger.timeTrigIntro, 9000)
//											IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_IHELP_BET")
//												CLEAR_THIS_FLOATING_HELP("FM_IHELP_BET")
//												#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Clearing betting help as time expired") #ENDIF
//											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
//								IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_IHELP_BET")
//									CLEAR_THIS_FLOATING_HELP("FM_IHELP_BET")
//								ENDIF
								IF IS_BIT_SET(fmTrigger.iBitset, biFMt_DoneBettingHelp)
									IF NOT HAS_NET_TIMER_EXPIRED(fmtrigger.timeTrigIntro, 7000)
										CLEAR_BIT(fmTrigger.iBitset, biFMt_DoneBettingHelp)
										#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Clearing biFMt_DoneBettingHelp as wasn't displayed for long enough") #ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF GET_CORONA_STATUS() >= CORONA_STATUS_BETTING

							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE FM_TRIGGER_INTRO_DONE_RACE
			IF NOT IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
				
				
				
			//	SET_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL_HELP(FALSE)
				SET_LOCAL_PLAYER_IS_IN_ACTIVITY_TUTORIAL_CORONA(FALSE)
				
				SET_LOCAL_PLAYER_DOING_ACTIVITY_TUTORIAL_RACE(FALSE)
				SET_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL(FALSE)
				
				SET_LOCAL_PLAYER_RUNNING_DM_TUTORIAL(TRUE)
				
		//		IF NOT HAS_FM_RACE_TUT_BEEN_DONE()
		//			#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Race tut not yet marked as complete, setting") #ENDIF
		//			SET_RACE_TUTORIAL_AS_COMPLETE()
		//		ENDIF
				
				RESET_NET_TIMER(fmtrigger.timeTrigIntro)
				START_NET_TIMER(fmtrigger.timeTrigIntro)
				
				SET_BIT(fmtrigger.iBitset, biFMt_FindSessionForDm)
				fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_SEND_TEXT
				
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = 0
				
				Change_MissionsAtCoords_Use_Extended_Name_Display_Range_For_Tutorial(vRaceCoronaLoc, FALSE)
				IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0 
					SET_PLAYER_GENDER(PLAYER_ID(), TRUE)
					#IF IS_DEBUG_BUILD 	
						PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Set player male")
						
					#ENDIF
				ELIF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 1
					SET_PLAYER_GENDER(PLAYER_ID(), FALSE)
					#IF IS_DEBUG_BUILD 	
						PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Set player female")
						
					#ENDIF
				ENDIF
				
				REQUEST_MODEL(GET_LAMAR_VEHICLE_MODEL_FOR_TUTORIAL())
				REQUEST_NPC_PED_MODEL(CHAR_LAMAR)
				#IF IS_DEBUG_BUILD 	
					PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Set GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = 0")
					PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_SEND_TEXT as doing DM Tut") 
				#ENDIF
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						STRING sMystation
						sMystation = GET_PLAYER_RADIO_STATION_NAME()
						IF NOT IS_STRING_NULL_OR_EMPTY(g_sMyTutorialRadioStation)
							IF NOT IS_STRING_NULL_OR_EMPTY(sMystation)
								IF NOT ARE_STRINGS_EQUAL(g_sMyTutorialRadioStation, sMystation)
									g_sMyTutorialRadioStation = sMystation
									#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRINGS("Radio station changed, setting g_sMyTutorialRadioStation = ", sMystation) #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF g_iPlayersOnMyTutorialRace = 0
					STORE_PLAYERS_ON_RACE()
				ENDIF
			ENDIF
		BREAK
		
		CASE FM_TRIGGER_INTRO_SEND_TEXT
		//	IF NOT NETWORK_IS_ACTIVITY_SESSION()
			IF IS_BIT_SET(fmTrigger.iBitset, biFmt_FinishedRace) // WIll get set when tutorial relaunches after race is finished
				IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_DoAfterRaceCut)
					SET_BIT(fmTrigger.iBitset2, biFmt2_DoAfterRaceCut)
					#IF IS_DEBUG_BUILD 
						
						PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Set biFmt2_DoAfterRaceCut ") 
					#ENDIF
				ENDIF
				
				IF IS_SCREEN_FADED_IN()
					IF NOT IS_PAUSE_MENU_ACTIVE()
					//	IF NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
						
							STORE_RACE_LEADERBOARD_INFO()
							CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID()) 

							fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_MESSAGE_END
							SET_BIT(fmtrigger.iBitset, biFMt_ReserveDmSlot)
							#IF IS_DEBUG_BUILD 
								PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Set biFMt_ReserveDmSlot")
								PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_MESSAGE_END") 
								
							#ENDIF
					//	ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE FM_TRIGGER_INTRO_MESSAGE_END

			IF IS_BIT_SET(fmtrigger.iBitset2, biFmt2_SetupForMission)
			//	OUTPUT_PLAYERS_ON_RACE()
				fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_CUT_FINISH_DM
				#IF IS_DEBUG_BUILD 
					PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_CUT_FINISH_DM") 
					
				#ENDIF
				
				SET_CORONA_HAS_TRIGGERED_JOB_MUSIC(FALSE)
			ENDIF
		BREAK

		
		CASE FM_TRIGGER_INTRO_CUT_FINISH_DM
		//	IF IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_StartTutDeathmatchIfICan)
			IF IS_BIT_SET(fmtrigger.iBitset2, biFmt2_SetupForMission)
				IF ARE_VECTORS_EQUAL(fmTrigger.vSafeDMPos, <<0.0, 0.0, 0.0>>)
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_FinishedPostRaceCleanup)
						SET_LOCAL_PLAYER_IS_IN_ACTIVITY_TUTORIAL_CORONA(TRUE)
				//		CLEAR_PRINTS()
				//		CLEAR_BIT(fmtrigger.iBitset, biFMt_DODmWaitCountdown)

						fmTrigger.vSafeDMPos = GET_SAFE_WARP_COORD_IN_CIRCLE(NATIVE_TO_INT(PLAYER_ID()), vMissionLoc)
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_VECTOR("fmTrigger.vSafeDMPos = ",fmTrigger.vSafeDMPos) #ENDIF
					ELSE
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Case CASE FM_TRIGGER_INTRO_CUT_FINISH_DM waiting for biTrigTut_FinishedPostRaceCleanup") #ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(fmTrigger.iBitset, biFmt_WarpedIntoDmCorona)
					//	IF (NET_WARP_TO_COORD(fmTrigger.vSafeDMPos , 0.0, FALSE, FALSE))  // <<139.752, -1449.19, 28.1941>>
							SET_ENTITY_COORDS(PLAYER_PED_ID(), fmTrigger.vSafeDMPos)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
							fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),  vMissionLoc)
							#IF IS_DEBUG_BUILD
								PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] I've been warped into DM corona...")
								PRINT_TRIGGER_STRING_FLOAT("[DO_TRIGGER_TUT] I am this far from the centre of the corona... ", fDistance)
								NET_NL() NET_PRINT("My actual coords... " )NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID())) NET_NL()
								
							#ENDIF
							CLEAR_BIT(fmTrigger.iBitset, biFmt_DisablePauseMenu)
							CLEAR_BIT(fmtrigger.iBitset, biFMt_FindSessionForDm)
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CASE FM_TRIGGER_INTRO_WARP_TO_DM Cleared biFmt_DisablePauseMenu") #ENDIF
							
							
							RESET_NET_TIMER(fmtrigger.timeTrigIntro)
							START_NET_TIMER(fmtrigger.timeTrigIntro)
							SET_BIT(fmTrigger.iBitset, biFmt_WarpedIntoDmCorona)
							CLEAR_HELP()
							#IF IS_DEBUG_BUILD 
								PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CASE FM_TRIGGER_INTRO_CUT_FINISH_DM set biFmt_WarpedIntoDmCorona") 
							#ENDIF
					
					//	ENDIF
					ELSE
						//-- Warped into corona
					//	IF IS_PLAYER_ACTIVE_IN_CORONA()
						IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFmt2_StartMission)
					//	AND NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
							IF IS_BIT_SET(fmtrigger.iBitset2, biFmt2_CloudDataLoaded)
								IF ARE_ALL_PLAYERS_READY_FOR_TUTORIAL_MISSION(#IF IS_DEBUG_BUILD fmtrigger.timeDebug #ENDIF)
									SET_BIT(fmtrigger.iBitset2, biFmt2_StartMission)
									
									SET_USER_RADIO_CONTROL_ENABLED(TRUE)
								
								//	CLEANUP_MP_CUTSCENE()
									#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Set biFmt2_StartMission") 
									#ENDIF
									
								
								ENDIF
							ENDIF
						ELSE
	
							CLEAR_BIT(fmtrigger.iBitset, biFMt_ReserveDmSlot)

							
							fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_IN_DM_CORONA
							#IF IS_DEBUG_BUILD 
								PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_IN_DM_CORONA") 
							#ENDIF
							
						ENDIF
					ENDIF
					
				ENDIF
			ELSE	
			//	IF IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					CLEANUP_MP_CUTSCENE()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					IF DOES_CAM_EXIST(fmTrigger.cutsceneCam)
						SET_CAM_ACTIVE(fmTrigger.cutsceneCam, FALSE)
						DESTROY_CAM(fmTrigger.cutsceneCam)
					ENDIF
					CLEAR_BIT(fmtrigger.iBitset, biFMt_ReserveDmSlot)
				//	CLEAR_BIT(fmtrigger.iBitset2, biFmt2_SetupForMission)
					fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_ON_DM
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_ON_DM from FM_TRIGGER_INTRO_CUT_FINISH_DM as I'm on a DM") #ENDIF
				ELSE	
					IF IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_BailAsOnlyPlayer)
						IF NETWORK_IS_IN_TUTORIAL_SESSION()
							NETWORK_END_TUTORIAL_SESSION()
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Removing from tutorial session as biTrigTut_BailAsOnlyPlayer") #ENDIF
						ENDIF
						CLEANUP_MP_CUTSCENE()
						CLEAR_HELP()
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						IF DOES_CAM_EXIST(fmTrigger.cutsceneCam)
							SET_CAM_ACTIVE(fmTrigger.cutsceneCam, FALSE)
						ENDIF

						DESTROY_ALL_CAMS()
						CLEAR_BIT(fmtrigger.iBitset, biFMt_FindSessionForDm)
						fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_OFF_DM
						#IF IS_DEBUG_BUILD 
							PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_OFF_DM as biTrigTut_BailAsOnlyPlayer") 
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE FM_TRIGGER_WARP_TO_SOLO_MISSION
			IF g_bFailedTutorialMission
				IF NOT IS_BIT_SET(fmTrigger.iBitset, biFMt_HideHudAndRadar)
					SET_BIT(fmTrigger.iBitset, biFMt_HideHudAndRadar)
					#IF IS_DEBUG_BUILD 
						PRINT_TRIGGER_STRING("Setting biFMt_HideHudAndRadar as g_bFailedTutorialMission")
					#ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(fmtrigger.iBitset2, biFmt2_SetupForMission)
				IF ARE_VECTORS_EQUAL(fmTrigger.vSafeDMPos, <<0.0, 0.0, 0.0>>)
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_FinishedPostRaceCleanup)
						IF NOT g_bFailedTutorialMission
						OR (g_bFailedTutorialMission AND IS_SKYSWOOP_AT_GROUND())
							IF g_bFailedTutorialMission
								ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
								SET_SKYFREEZE_FROZEN()
								#IF IS_DEBUG_BUILD 
									PRINT_TRIGGER_STRING("SET_SKYFREEZE_FROZEN as g_bFailedTutorialMission")
								#ENDIF
							ENDIF
							SET_LOCAL_PLAYER_IS_IN_ACTIVITY_TUTORIAL_CORONA(TRUE)
				
							fmTrigger.vSafeDMPos = GET_SAFE_WARP_COORD_IN_CIRCLE(NATIVE_TO_INT(PLAYER_ID()), vSoloMissionLoc)
							Change_MissionsAtCoords_InCorona_External_Function_ID_For_Tutorial(vMissionLoc, MATCICE_NONE)
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_VECTOR("fmTrigger.vSafeDMPos = ",fmTrigger.vSafeDMPos) #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Case CASE FM_TRIGGER_WARP_TO_SOLO_MISSION waiting for biTrigTut_FinishedPostRaceCleanup") #ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(fmTrigger.iBitset, biFmt_WarpedIntoDmCorona)
					//	IF (NET_WARP_TO_COORD(fmTrigger.vSafeDMPos , 0.0, FALSE, FALSE))  // <<139.752, -1449.19, 28.1941>>
							SET_ENTITY_COORDS(PLAYER_PED_ID(), fmTrigger.vSafeDMPos)
							fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),  vSoloMissionLoc)
							#IF IS_DEBUG_BUILD
								PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] I've been warped into SOLO mission corona...")
								PRINT_TRIGGER_STRING_FLOAT("[DO_TRIGGER_TUT] I am this far from the centre of the SOLO mission corona... ", fDistance)
								NET_NL() NET_PRINT("My actual coords... " )NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID())) NET_NL()
								
							#ENDIF
							CLEAR_BIT(fmTrigger.iBitset, biFmt_DisablePauseMenu)
							CLEAR_BIT(fmtrigger.iBitset, biFMt_FindSessionForDm)
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CASE FM_TRIGGER_WARP_TO_SOLO_MISSION Cleared biFmt_DisablePauseMenu") #ENDIF
							
							
							RESET_NET_TIMER(fmtrigger.timeTrigIntro)
							START_NET_TIMER(fmtrigger.timeTrigIntro)
							SET_BIT(fmTrigger.iBitset, biFmt_WarpedIntoDmCorona)
							CLEAR_HELP()
							#IF IS_DEBUG_BUILD 
								PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CASE FM_TRIGGER_WARP_TO_SOLO_MISSION set biFmt_WarpedIntoDmCorona") 
							#ENDIF
					
					//	ENDIF
					ELSE
						//-- Warped into corona

						IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFmt2_StartMission)

							IF IS_BIT_SET(fmtrigger.iBitset2, biFmt2_CloudDataLoaded)
								
								SET_BIT(fmtrigger.iBitset2, biFmt2_StartMission)
								
								SET_USER_RADIO_CONTROL_ENABLED(TRUE)
							
								#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Set biFmt2_StartMission for SOLO mission") 
								#ENDIF

							ENDIF
						ELSE
	
							CLEAR_BIT(fmtrigger.iBitset, biFMt_ReserveDmSlot)

							
							fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_IN_DM_CORONA
							#IF IS_DEBUG_BUILD 
								PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CASE FM_TRIGGER_WARP_TO_SOLO_MISSION fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_IN_DM_CORONA") 
							#ENDIF
							
						ENDIF
					ENDIF
					
				ENDIF
			ELSE	

				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					CLEANUP_MP_CUTSCENE()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					IF DOES_CAM_EXIST(fmTrigger.cutsceneCam)
						SET_CAM_ACTIVE(fmTrigger.cutsceneCam, FALSE)
						DESTROY_CAM(fmTrigger.cutsceneCam)
					ENDIF
					CLEAR_BIT(fmtrigger.iBitset, biFMt_ReserveDmSlot)
					fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_ON_DM
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CASE FM_TRIGGER_WARP_TO_SOLO_MISSION fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_ON_DM from FM_TRIGGER_INTRO_CUT_FINISH_DM as I'm on a DM") #ENDIF
				ELSE	
					
				ENDIF
			ENDIF
		BREAK
		
		CASE FM_TRIGGER_INTRO_IN_DM_CORONA
		//	IF IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
			
			
			IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			//	IF NETWORK_IS_IN_TUTORIAL_SESSION()
			//		NETWORK_END_TUTORIAL_SESSION()
			//		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Removing from tutorial session as on mission") #ENDIF
			//	ENDIF
				fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_ON_DM
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_ON_DM") #ENDIF
			ELSE
				
				IF NOT ARE_VECTORS_EQUAL(fmTrigger.vSafeRacePos, <<0.0, 0.0, 0.0>>)
					fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),  vDMCoronaLoc)
					IF fDistance > ciIN_LOCATE_DISTANCE
						fmTrigger.vSafeDMpos = <<0.0, 0.0, 0.0>>
						fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_CUT_FINISH_DM
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Re-warping into dm corona as I'm too far away") #ENDIF
					ENDIF
				ENDIF 
				
				IF NOT IS_BIT_SET(fmtrigger.iBitset, biFMt_SetRemoveFromTut)					
					IF GET_CORONA_STATUS() >= CORONA_STATUS_INIT_CORONA	
						SET_BIT(fmtrigger.iBitset, biFMt_SetRemoveFromTut)
						SET_BIT(fmtrigger.iBitset, biFMt_RemoveFromTutSess)
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CASE FM_TRIGGER_INTRO_IN_DM_CORONA set biFMt_RemoveFromTutSess") #ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE FM_TRIGGER_INTRO_ON_DM
		//	IF IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
			IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				
							
				SET_BIT(fmtrigger.iBitset2, biFMt2_FadeInOnMission)				
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Set biFMt2_FadeInOnMission") #ENDIF
				SET_LOCAL_PLAYER_SHOULD_SPANW_AT_LAMARS_HOUSE(TRUE)
				SET_LOCAL_PLAYER_RUNNING_DM_TUTORIAL(FALSE)
				SET_LOCAL_PLAYER_IS_IN_ACTIVITY_TUTORIAL_CORONA(FALSE)
				SET_LOCAL_PLAYER_READY_FOR_TUTORIAL_DEATHMATCH(FALSE)
			//	SET_LOCAL_PLAYER_WATCHING_TUTORIAL_DM_CUT(FALSE)
				SET_LOCAL_PLAYER_IS_IN_DM_TUTORIAL_CORONA(FALSE)
				
				SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_NPC_INVITE)  // 1914215
				
				fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_OFF_DM
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_OFF_DM") #ENDIF
			ENDIF
		BREAK
		
		CASE FM_TRIGGER_INTRO_OFF_DM
		//	IF NOT IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
			IF NOT (IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			OR IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_BailAsOnlyPlayer))
			AND g_bPassedMyTutorialMission
			#IF IS_DEBUG_BUILD OR fmtrigger.bTutorialSkipped #ENDIF
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING("Cleaning up tutorial because... ")
					
					IF fmtrigger.bTutorialSkipped
						PRINT_TRIGGER_STRING("... bTutorialSkipped is set ")
					ENDIF
					IF IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_BailAsOnlyPlayer)
						PRINT_TRIGGER_STRING("... I'm the only player ready for DM ")
					ELIF NOT IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
						PRINT_TRIGGER_STRING("... I'm not on a DM ")
					ENDIF
					
					IF fmtrigger.bTutorialSkipped
						IF NETWORK_IS_IN_TUTORIAL_SESSION()
							NETWORK_END_TUTORIAL_SESSION()
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Removing from tutorial session as shift-s skipped") #ENDIF
						ENDIF
					ENDIF
				#ENDIF
					
				fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_FINISHED
// KEITH 20/6/13: Replaced with a new non-blocking type of Invite
//				Cancel_Fake_Joblist_Invite()
				Cancel_Basic_Invite_From_NPC(CHAR_LAMAR, FMMC_TYPE_HOLD_UPS)

			//	SET_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL_HELP(FALSE)
				SET_LOCAL_PLAYER_IS_RUNNING_ACTIVITY_TUTORIAL(FALSE)
				SET_LOCAL_PLAYER_IS_IN_ACTIVITY_TUTORIAL_CORONA(FALSE)
				
				SET_LOCAL_PLAYER_DOING_ACTIVITY_TUTORIAL_RACE(FALSE)
				SET_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL(FALSE)
			//	SET_LOCAL_PLAYER_WATCHING_TUTORIAL_DM_CUT(FALSE)
			//	SET_LOCAL_PLAYER_ACCEPTED_TUTORIAL_INVITE(FALSE)
				SET_LOCAL_PLAYER_READY_FOR_DM_TUTORIAL_CUT(FALSE)
				SET_LOCAL_PLAYER_READY_FOR_TUTORIAL_DEATHMATCH(FALSE)
				SET_LOCAL_PLAYER_RUNNING_DM_TUTORIAL(FALSE)
				SET_TUTORIAL_CORONA_INTRO_TIMER_EXPIRED(TRUE)
				SET_LOCAL_PLAYER_COMPLETED_ACTIVITY_TUTORIAL(TRUE)
				SET_LOCAL_PLAYER_IS_IN_TUTORIAL_SESSION_FOR_INTRO(FALSE)
				SET_CORONA_WALK_IN_DISABLED_FOR_TUTORIAL(FALSE)
				SET_STORE_ENABLED(TRUE)
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING(" SET_STORE_ENABLED True...") #ENDIF
				ADD_CONTACT_TO_PHONEBOOK (CHAR_MP_GERALD, MULTIPLAYER_BOOK, FALSE)
				
				SET_COUNTRYSIDE_AMMU_AVAILABLE(TRUE)
				SET_CITY_AMMU_AVAILABLE(TRUE)
				
				SET_TIME_OF_DAY(TIME_OFF, TRUE)
				
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING(" NETWORK_CLEAR_CLOCK_TIME_OVERRIDE...") #ENDIF
				Change_MissionsAtCoords_Use_Extended_Name_Display_Range_For_Tutorial(vRaceCoronaLoc, FALSE)
				
				
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, FALSE)
				ENDIF
				
				
				SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-652.117676,-957.778748,24.367865>>, <<-757.295715,-960.789856,12.821367>>, 22.562500)
				Clear_Any_Objective_Text_From_This_Script()
				IF DOES_BLIP_EXIST(fmTrigger.blipLamar)
					REMOVE_BLIP(fmTrigger.blipLamar)
				ENDIF
				IF DOES_BLIP_EXIST(fmtrigger.blipTempRace)
					SET_BLIP_ROUTE(fmtrigger.blipTempRace, FALSE)
					REMOVE_BLIP(fmtrigger.blipTempRace)
				ENDIF
					
				GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iFmTutorialSession = TUTORIAL_SESSION_NOT_ACTIVE
				Change_MissionsAtCoords_InCorona_External_Function_ID_For_Tutorial(vMissionLoc, MATCICE_FMMC_LAUNCHER)
				CLEAN_UP_FM_MISSION_DECORATORS() // 1046941
				
				CLEAR_BIT(fmTrigger.iBitset, biFmt_DisablePauseMenu)
				CLEAR_BIT(fmTrigger.iBitset, biFMt_DoWaitingScreenHelp)
				
//				IF IS_BIT_SET(fmTrigger.iBitset2, biFMt2_OnlyCamControls)
//					CLEAR_BIT(fmTrigger.iBitset2, biFMt2_OnlyCamControls)
//					MAKE_ONLY_PLAYER_CAMERA_WORK(FALSE)
//				ENDIF
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CASE FM_TRIGGER_INTRO_OFF_DM Cleared biFmt_DisablePauseMenu") #ENDIF
		//		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)		
				
				SET_DEATHMATCH_TUTORIAL_AS_COMPLETE()
//				INT iMyRank
//				INT iXpToGive
//				INT iMyXp 
//				iMyRank = GET_PLAYER_FM_RANK(PLAYER_ID())
//				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[DO_TRIGGER_TUT] post tut mission rank check (1), rank = ", iMyRank) #ENDIF
//				IF iMyRank  < 3
//					iMyXp = GET_PLAYER_FM_XP(PLAYER_ID())
//					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[DO_TRIGGER_TUT] post tut mission rank check (1), iMyXp = ", iMyXp) #ENDIF
//					
//					iXpToGive = 1250 - iMyXp
					
			//		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT(" Giving XP boost... ", iXpToGive) #ENDIF
			//		GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_IMPEXP", XPTYPE_SKIP_INGAME, XPCATEGORY_SKIP_INGAME_END_TUTORIAL, iXpToGive )
				
//					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Giving temp rank boost 1...") #ENDIF

//				ELSE
//					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] NOT giving temp rank boost 1...") #ENDIF
//				ENDIF
				
//				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("[DO_TRIGGER_TUT] post tut mission rank check (2), rank = ", iMyRank) #ENDIF
//				iMyRank = GET_PLAYER_FM_RANK(PLAYER_ID())
//				IF iMyRank  < 3
//					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Giving temp rank boost 2...") #ENDIF
//					GIVE_PLAYER_XP_FOR_NEXT_RANK()
//				ELSE
//					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] NOT giving temp rank boost 2...") #ENDIF
//				ENDIF

				IF IS_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA(PLAYER_ID())
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CLEAR_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA") #ENDIF
					CLEAR_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA()
				ENDIF
				
//				SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_RACETUT_DONE)
//				SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_TRIGTUT_DONE)
				SET_MP_BOOL_PLAYER_STAT(MPPLY_CAN_SPECTATE, TRUE) // 1829563
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_RACETUT_DONE, TRUE)
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_TRIGTUT_DONE, TRUE)
				GIVE_PLAYER_END_OF_MISSION_XP()
			//	REQUEST_SAVE(STAT_SAVETYPE_PROLOGUE)
				
				SET_FM_UNLOCKS_BIT_SET()
				IF IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_BailAsOnlyPlayer)
					//-- Get removed from tut session in CASE FM_TRIGGER_INTRO_CUT_FINISH_DM if biTrigTut_BailAsOnlyPlayer
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Doing only player help...") #ENDIF
					PRINT_HELP("FM_TRIG_NDM") // No other new Players were available for a Deathmatch. You can trigger a Race ~BLIP_RACE~ or a Deathmatch ~BLIP_DEATHMATCH~ by entering the Marker on the Map.
				ENDIF
				
				fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_FINISHED
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_FINISHED") #ENDIF

			ELSE
				//-- Need to cleanup lamar
				ENTITY_INDEX ent
				IF NOT HAS_NET_TIMER_STARTED(fmtrigger.timeClearArea)
					START_NET_TIMER(fmtrigger.timeClearArea)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(fmtrigger.timeClearArea, 10000)
						IF DOES_ENTITY_EXIST(fmtrigger.pedLamar)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
									ent = fmTrigger.pedLamar
									DELETE_ENTITY(ent)
									#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CASE FM_TRIGGER_INTRO_OFF_DM deleted Lamar") #ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(fmtrigger.vehLamar)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
									ent = fmTrigger.vehLamar
									DELETE_ENTITY(ent)
									#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] CASE FM_TRIGGER_INTRO_OFF_DM deleted Lamar's car") #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//-- Stop hiding radar (will only be an issue if player previously failed the mission)
				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							IF IS_BIT_SET(fmTrigger.iBitset, biFMt_HideHudAndRadar)
								CLEAR_BIT(fmTrigger.iBitset, biFMt_HideHudAndRadar)
								#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Cleared biFMt_HideHudAndRadar as on mission and player control on") #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//-- Handle launching / relaunching solo mission if player quits via phone
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					IF NOT g_bPassedMyTutorialMission
						IF NOT IS_BIT_SET(fmTrigger.iBitset, biFmt_PutPlayerOnSolo)
							SET_BIT(fmTrigger.iBitset, biFmt_PutPlayerOnSolo)
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Setting biFmt_PutPlayerOnSolo") #ENDIF
					
						ENDIF
						
						IF IS_BIT_SET(fmTrigger.iBitset, biFMt_UnreservedPlayer)	
							CLEAR_BIT(fmTrigger.iBitset, biFMt_UnreservedPlayer)
						ENDIF
						
						CLEAR_BIT(fmTrigger.iBitset2, biFmt2_LaunchedMission)
						fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_ON_DM
						#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_ON_DM - g_bPassedMyTutorialMission FALSE") #ENDIF
					ENDIF
				ENDIF
				
				
			ENDIF
		BREAK
	ENDSWITCH
	
	
	//-- Cutscene Help
	IF IS_BIT_SET(fmtrigger.iBitset, biFMt_DoWaitingScreenHelp )	
		DO_WAITING_SCREEN_HELP(fmtrigger)
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_RIN")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_RN2")
		//	CLEAR_HELP()
		ENDIF
	ENDIF
	
	//-- Disable Pause menu
	IF IS_BIT_SET(fmtrigger.iBitset, biFmt_DisablePauseMenu)
		DISABLE_FRONTEND_THIS_FRAME()
	ENDIF
	
	IF HAS_FM_RACE_TUT_BEEN_DONE()
		IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPreTutorialMissionInFlow
			HIDE_HUD_AND_RADAR_THIS_FRAME() 
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
		//	#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Hiding hud") #ENDIF
		ENDIF
	ENDIF
	
	//-- Ambient racecars
	MANAGE_AMBIENT_RACE_CARS(fmtrigger)
	
	
	
	//-- Temporarily unlock the race
	IF IS_BIT_SET(fmTrigger.iBitset2, biFMt2_TryToUnlockRace)
		IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_UnlockedRace)
//			IF (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
	//		AND NOT (Is_Cloud_Loaded_Mission_Data_Being_Refreshed())
				// KEITH 13/7/13: SPECIAL FUNCTION TO SET UP RACE CORONA AS A ONE-OFF SPECIAL
				
			//	BOOL bUseExtendedRange = IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchTutorialFromIntroCut)
				IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
				AND Trigger_Tut_Race_Advance_Setup(vRaceCoronaLoc)
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Temporarily unlocked race ") #ENDIF
					SET_BIT(fmTrigger.iBitset2, biFmt2_UnlockedRace)
				ENDIF
//			ENDIF
		ELIF GET_CORONA_STATUS() = CORONA_STATUS_WALK_OUT
		AND IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
			CLEAR_BIT(fmTrigger.iBitset2, biFmt2_UnlockedRace)
			#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Temporarily unlocked race ") #ENDIF
		ENDIF
	ENDIF
	
	//-- Remove from tutorial session once in in corona
	IF IS_BIT_SET(fmtrigger.iBitset, biFMt_RemoveFromTutSess)
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
				NETWORK_END_TUTORIAL_SESSION()
				CLEAR_BIT(fmtrigger.iBitset, biFMt_RemoveFromTutSess)
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Removing from tutorial session as biFMt_RemoveFromTutSess is set") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Tut session after race
	IF IS_BIT_SET(fmtrigger.iBitset, biFMt_FindSessionForDm)
		IF NOT IS_BIT_SET(fmtrigger.iBitset, biFmt_InTutSessionForDM)
			IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
				IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
						  
					instance = NATIVE_TO_INT(PLAYER_ID()) + NUM_NETWORK_PLAYERS
					NETWORK_ALLOW_GANG_TO_JOIN_TUTORIAL_SESSION(1, instance)
					CLEAR_BIT(fmtrigger.iBitset, biFMt_FindSessionForDm)
					SET_BIT(fmtrigger.iBitset, biFmt_InTutSessionForDM)
					#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_INT("Putting player on tut session for DM invite... ", instance) #ENDIF
					
				ENDIF
			ELSE	
				#IF IS_DEBUG_BUILD
					IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFMt2_StraightToDm) 
						// PRINT_TRIGGER_STRING("Trying to put in tut session for DM, but already in Tut session!") 
					ENDIF
				#ENDIF
			ENDIF			
		ENDIF
	ENDIF
	
	//-- After race cutscene
	IF IS_BIT_SET(fmTrigger.iBitset2, biFmt2_DoAfterRaceCut)
		IF DO_LAMAR_AFTER_RACE_MOCAP(fmTrigger)
		ENDIF
	ENDIF
	
	IF SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				DISABLE_FRONTEND_THIS_FRAME()
				#IF IS_DEBUG_BUILD 
			//		PRINT_TRIGGER_STRING("Disabling front end (SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP)") 
					
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Manage my wait-for-players time when waiting to start mission
	IF NOT HAS_NET_TIMER_STARTED(GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].fmRaceTutorial.timeLaunchMission )

		IF IS_BIT_SET(fmTrigger.iBitset2, biFmt2_ReservedOnMission)	
			IF NOT HAS_NET_TIMER_STARTED(fmtrigger.timeWaitForReserve)
				START_NET_TIMER(fmtrigger.timeWaitForReserve)
				#IF IS_DEBUG_BUILD 
					PRINT_TRIGGER_STRING("Started timeWaitForReserve") 
					
				#ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(fmtrigger.timeWaitForReserve, 1000)
					INT iNumPlayersReserved
					INT iUniqueId = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
					iNumPlayersReserved = Get_Number_Of_Players_That_Reserved_Themselves_For_The_Mission(iUniqueId)
					IF iNumPlayersReserved > 0
						IF HAS_ANY_RESERVED_PLAYER_STARTED_MISSION_COUNTDOWN_TIMER()
						ELSE	
							
							MAX_DM_TUT_TIMEOUT = 10000
						//	START_NET_TIMER(fmtrigger.timeLocalTImeOut)
							START_NET_TIMER(GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].fmRaceTutorial.timeLaunchMission)
							#IF IS_DEBUG_BUILD 
								PRINT_TRIGGER_STRING_INT("Didn' find someone reserved, setting MAX_DM_TUT_TIMEOUT = ", MAX_DM_TUT_TIMEOUT) 
							#ENDIF
						ENDIF
						
						SET_LOCAL_PLAYER_STARTED_WAIT_FOR_TUTORIAL_PLAYERS_TIMEOUT(TRUE)
						#IF IS_DEBUG_BUILD 
							PRINT_TRIGGER_STRING_INT("Waiting for other players. MAX_DM_TUT_TIMEOUT = ", MAX_DM_TUT_TIMEOUT) 
							PRINT_TRIGGER_STRING_INT("Waiting for other players. Posix Time = ", GET_CLOUD_TIME_AS_INT()) 
						#ENDIF
					//	PRINT_NOW("FM_IHELP_PWT", 60000, 1) 
						SET_BIT(fmtrigger.iBitset, biFMt_DODmWaitCountdown)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
	ENDIF
	
	//-- Only wait for other players for a finite amount of time 
//	IF HAS_NET_TIMER_STARTED(fmtrigger.timeLocalTImeOut)
	IF HAS_NET_TIMER_STARTED(GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].fmRaceTutorial.timeLaunchMission)
		IF NOT IS_BIT_SET(fmtrigger.iBitset, biFMt_DmFinalCheckFOrPlay)
	//		IF HAS_NET_TIMER_EXPIRED(fmtrigger.timeLocalTImeOut, MAX_DM_TUT_TIMEOUT)
			IF HAS_NET_TIMER_EXPIRED(GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].fmRaceTutorial.timeLaunchMission, MAX_DM_TUT_TIMEOUT)
				SET_BIT(fmtrigger.iBitset, biFMt_DmFinalCheckFOrPlay)
				#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set biFMt_DmFinalCheckFOrPlay as timeout expired") #ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(fmtrigger.iBitset, biFMt_DODmWaitCountdown)
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		//		DRAW_PLAYER_WAIT_COUNT_DOWN_TIMER(MAX_DM_TUT_TIMEOUT, GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].fmRaceTutorial.timeLaunchMission)		
			ENDIF

	
		ENDIF
	ENDIF
		
	
	//-- Try to reserve a DM mission slot
	IF HAS_FM_RACE_TUT_BEEN_DONE()

			IF fmTrigger.iFmTriggerIntroProg > FM_TRIGGER_INTRO_INIT
				IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
					IF NOT IS_BIT_SET(fmTrigger.iBitset, biFmt_PutPlayerOnSolo)
						IF IS_BIT_SET(fmTrigger.iBitset2, biFmt2_StartPutOntoMiss) // Set in Lamar after race cutscene
						
						 	// KEITH 3/5/13: Waits for the first batch of data to be ready and a no ongoing data refresh
							IF (Is_Initial_Cloud_Loaded_Mission_Data_Ready())

								IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_TempUnlockMiss)
								
									// KGM 22/9/13: For Activity Sessions I now only trigger the loaded filename, but for the tutorial mission it seems to think it
									//				is still an Activity Session. I ignore NULL filenames, so clearing the loaded filename
									CLEAR_MY_TRANSITION_SESSION_CONTENT_ID()
								
									//-- Required so that the otherwise locked mission is available for the tutorial
									Temporarily_Unlock_MissionsAtCoords_Mission_For_Tutorial(vMissionLoc, TRUE)
									
									//-- Required so that the corona screens don't activate. 
									Change_MissionsAtCoords_InCorona_External_Function_ID_For_Tutorial(vMissionLoc, MATCICE_FM_TUTORIAL)
									SET_BIT(fmTrigger.iBitset2, biFmt2_TempUnlockMiss)
									#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Temporarily unlocked mission...") #ENDIF
								ELSE
									IF IS_BIT_SET(fmtrigger.iBitset, biFMt_ReserveDmSlot)
										
										IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_ReservedOnMission) // Set in Process_Stage_Tutorial_Waiting_For_Reservation_Confirmation()
											IF PROCESS_RESERVE_MISSION_SLOT(fmTrigger, vMissionLoc)
											#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) #ENDIF
												#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("PROCESS_RESERVE_MISSION_SLOT returned True, I'm reserved on mission") #ENDIF
											ENDIF
										ELSE
											IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFmt2_SetupForMission)
												SET_BIT(fmtrigger.iBitset2, biFmt2_SetupForMission)
												#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set biFmt2_SetupForMission as I'm reserved") #ENDIF
											ELSE
												fmTrigger.iNumPlayersReservedOnMission = GET_NUMBER_OF_PLAYERS_READY_FOR_MISSION()
												IF fmTrigger.iNumPlayersReservedOnMission >= MIN_PLAYERS_FOR_MISSION
												OR IS_BIT_SET(fmTrigger.iBitset, biFMt_DmFinalCheckForPlay)
													SET_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_StartTutDeathmatchIfICan)
													
													
													CLEAR_BIT(fmtrigger.iBitset, biFMt_ReserveDmSlot)
													
												
													#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("PROCESS_RESERVE_DM_SLOT returned True, Time expired or Enough players ready") #ENDIF
											
												ENDIF
											ENDIF
										ENDIF
												
									ENDIF
								ENDIF
							ELSE	
								#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("waiting for matccInitialDataReady to be TRUE and matccRefreshInProgress to be FALSE") #ENDIF
							ENDIF
						ENDIF
					ELSE
						//-- Need to put player on 1-player mission variation
						IF NOT IS_BIT_SET(fmTrigger.iBitset, biFMt_UnreservedPlayer)	
							//-- Reset everything
							IF NOT SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP()
								UNRESERVE_LOCAL_PLAYER_FOR_TUTORIAL_MISSION()
							ENDIF
							SET_BIT(fmTrigger.iBitset, biFMt_UnreservedPlayer)
							SET_BIT(fmtrigger.iBitset, biFMt_ReserveDmSlot)
							CLEAR_BIT(fmTrigger.iBitset2, biFmt2_TempUnlockMiss)
							CLEAR_BIT(fmTrigger.iBitset2, biFmt2_ReservedOnMission)
							CLEAR_BIT(fmTrigger.iBitset2, biFmt2_SetupForMission)
							CLEAR_BIT(fmTrigger.iBitset2, biFmt2_CloudDataLoaded)
							CLEAR_BIT(fmTrigger.iBitset, biFmt_WarpedIntoDmCorona)
							
							SET_LOCAL_PLAYER_LOADED_CLOUD_DATA_FOR_TUTORIAL_MISSION(FALSE)
							
							fmTrigger.vSafeDMPos = <<0.0, 0.0, 0.0>>
							fmTrigger.iReservePlayerProg = 0
							
							fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_WARP_TO_SOLO_MISSION
							
						//	Temporarily_Unlock_MissionsAtCoords_Mission_For_Tutorial(vMissionLoc, FALSE)
							LOCK_NON_SOLO_TUTORIAL_MISSION(vMissionLoc)
							#IF IS_DEBUG_BUILD 
								PRINT_TRIGGER_STRING("") 
								PRINT_TRIGGER_STRING("********************** PUTTING PLAYER ON SOLO TUTORIAL MISSION **********************")
								PRINT_TRIGGER_STRING("")
								PRINT_TRIGGER_STRING("Unreserved player from mission as I'm only player. Set fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_WARP_TO_SOLO_MISSION") 
							#ENDIF
						ELSE
							// KEITH 3/5/13: Waits for the first batch of data to be ready and a no ongoing data refresh
							IF (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
								//-- Player removed from original mission
								IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_TempUnlockMiss)
								
									// KGM 22/9/13: For Activity Sessions I now only trigger the loaded filename, but for the tutorial mission it seems to think it
									//				is still an Activity Session. I ignore NULL filenames, so clearing the loaded filename
									CLEAR_MY_TRANSITION_SESSION_CONTENT_ID()
									
									//-- Required so that the otherwise locked mission is available for the tutorial
									Temporarily_Unlock_MissionsAtCoords_Mission_For_Tutorial(vSoloMissionLoc, TRUE)
									
									//-- Required so that the corona screens don't activate. 
									Change_MissionsAtCoords_InCorona_External_Function_ID_For_Tutorial(vSoloMissionLoc, MATCICE_FM_TUTORIAL)
									SET_BIT(fmTrigger.iBitset2, biFmt2_TempUnlockMiss)
									#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Temporarily unlocked SOLO mission...") #ENDIF
								ELSE
									IF IS_BIT_SET(fmtrigger.iBitset, biFMt_ReserveDmSlot)
											
										IF NOT IS_BIT_SET(fmTrigger.iBitset2, biFmt2_ReservedOnMission) // Set in Process_Stage_Tutorial_Waiting_For_Reservation_Confirmation()
											IF PROCESS_RESERVE_SOLO_MISSION_SLOT(fmTrigger)
											#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) #ENDIF
												#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("PROCESS_RESERVE_SOLO_MISSION_SLOT returned True, I'm reserved on mission") #ENDIF
											ENDIF
										ELSE
											IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFmt2_SetupForMission)
												SET_BIT(fmtrigger.iBitset2, biFmt2_SetupForMission)
												#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Set biFmt2_SetupForMission as I'm reserved for SOLO mission") #ENDIF
											ELSE
												fmTrigger.iNumPlayersReservedOnMission = GET_NUMBER_OF_PLAYERS_READY_FOR_MISSION()
												IF fmTrigger.iNumPlayersReservedOnMission = 1
												
													SET_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_StartTutDeathmatchIfICan)
													CLEAR_BIT(fmtrigger.iBitset, biFMt_ReserveDmSlot)
													#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Finished tutorial setup for SOLO mission...") #ENDIF
											
												ENDIF
											ENDIF
										ENDIF
												
									ENDIF
								ENDIF
							ELSE
									
								#IF IS_DEBUG_BUILD 
									PRINT_TRIGGER_STRING("waiting for Is_Initial_Cloud_Loaded_Mission_Data_Ready - 1 player") 
								#ENDIF
							
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF

	ELSE
		//-- Lamar speech when near Lamar prior to race
		MAINTAIN_PLAYER_NEAR_LAMAR_DIALOGUE(fmTrigger)
	ENDIF
	
	
	
	//-- Deal with putting players straight onto the mission
	IF IS_BIT_SET(fmtrigger.iBitset2, biFmt2_SetupForMission)
		IF NOT IS_BIT_SET(fmTrigger.iBitset, biFmt_PutPlayerOnSolo)
			Maintain_MissionsAtCoords_Tutorial_Mission_InCorona(fmtrigger)
		ELSE
			Maintain_MissionsAtCoords_Tutorial_Mission_InCorona(fmtrigger, TRUE)
		ENDIF
	ELSE
	//	PRINTLN("Else 1")
	ENDIF
	
	//-- Set random peds to flee, to stop peds attacking player
	IF NOT IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
	AND NOT IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
	AND NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		
		
	ENDIF
	
	IF NOT HAS_FM_RACE_TUT_BEEN_DONE()
		IF NOT IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
			DISABLE_FRONTEND_THIS_FRAME()
			IF IS_SOCIAL_CLUB_ACTIVE()
				CLOSE_SOCIAL_CLUB_MENU() 
			ENDIF
			#IF IS_DEBUG_BUILD 
			//	PRINT_TRIGGER_STRING("Disabling frontend") 
			#ENDIF
		ENDIF
	ENDIF
		
	//-- Fade in once on a mission
	IF IS_BIT_SET(fmtrigger.iBitset2, biFMt2_FadeInOnMission)
		//-- Fade out is is CASE FM_TRIGGER_INTRO_CUT_FINISH_DM
		IF IS_SCREEN_FADED_OUT()
			IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							CLEAR_BIT(fmtrigger.iBitset2, biFMt2_FadeInOnMission)
							#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("Cleared biFMt2_FadeInOnMission as faded in on-mission") #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	IF IS_BIT_SET(fmTrigger.iBitset2, biFMt2_OnlyCamControls)
//		IF GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
//			IF NOT IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
//				IF NOT IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
//					IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
//						MAKE_ONLY_PLAYER_CAMERA_WORK()
//						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF IS_BIT_SET(fmTrigger.iBitset, biFMt_HideHudAndRadar)
		IF g_bFailedTutorialMission
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF fmTrigger.bReserveDM
			fmTrigger.bReserveDM = FALSE
			SET_BIT(fmtrigger.iBitset, biFMt_ReserveDmSlot)
		ENDIF
		
		IF NOT IS_BIT_SET(fmtrigger.iBitset2, biFmt2_GotColourCombo)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						INT w,x,y,z
						VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						GET_VEHICLE_COLOURS(veh, w, x)
						GET_VEHICLE_EXTRA_COLOURS(veh, y, z)
						SET_BIT(fmtrigger.iBitset2, biFmt2_GotColourCombo)
						PRINTLN("[dsw] [TriggerTut] Vehicle colours... Col1: ", w, " Col2: ", x)
						PRINTLN("[dsw] [TriggerTut] Vehicle colours... Ext1: ", y, " Ext2: ", z)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT fmTrigger.bTutorialSkipped
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		//	OR IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_SHIFT, "Skip tutorials")
				IF NOT IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
				AND NOT IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
				AND NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					IF NOT g_bPlayerSkippedIntro
						IF fmTrigger.iFmTriggerIntroProg <> FM_TRIGGER_INTRO_OFF_DM
						AND fmTrigger.iFmTriggerIntroProg > FM_TRIGGER_INTRO_INIT
						AND fmTrigger.iFmTriggerIntroProg < FM_TRIGGER_INTRO_FINISHED
							GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_IMPEXP", XPTYPE_SKIP_INGAME, XPCATEGORY_SKIP_INGAME_END_TUTORIAL, 5000 )
							fmTrigger.bTutorialSkipped = TRUE
							REMOVE_CUTSCENE()
							CLEANUP_TRIG_TUT_CUTSCENE(fmTrigger, FALSE)
							PRINT_TRIGGER_STRING("[DO_TRIGGER_TUT] Trigger tut S-skipped")
							CLEAR_OVERRIDE_WEATHER()
							fmTrigger.iFmTriggerIntroProg = FM_TRIGGER_INTRO_OFF_DM
						ENDIF
					ENDIF
				ENDIF
				
				IF g_bPlayerSkippedIntro
					g_bPlayerSkippedIntro = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF HAS_FM_RACE_TUT_BEEN_DONE()
		//	PRINTLN("[dsw] [TriggerTut] iSplashProg = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iSplashProg)
		ENDIF
		DO_TRIGGER_TUT_J_SKIPS(fmtrigger)
	#ENDIF
	
	RETURN FALSE
ENDFUNC
