USING "rage_builtins.sch" 
USING "globals.sch"
USING "net_include.sch"
USING "net_rank_rewards.sch"

PROC REMOVE_ALL_PLAYERS_WEAPONS()
	DEBUG_PRINTCALLSTACK()
	GIVE_LOADOUT_TO_PED(PLAYER_PED_ID(),LOADOUT_DEFAULT)
ENDPROC

FUNC BOOL IS_WEAPON_OK_FOR_RANDOM_INVENTORY(WEAPON_TYPE wtWeaponToCheck)
	SWITCH wtWeaponToCheck
		CASE WEAPONTYPE_DLC_RAILGUN
		CASE WEAPONTYPE_UNARMED
			RETURN FALSE
	ENDSWITCH
	
	IF DOES_PLAYER_HAVE_WEAPON(wtWeaponToCheck)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_RANDOM_WEAPON_FOR_INVENTORY(WEAPON_TYPE &wtWep, INT &iAttempts, INT iMaxWeapons)
	INT iRandomInt = GET_RANDOM_INT_IN_RANGE(0, iMaxWeapons)
	WEAPON_TYPE wtRandomWeapon = GET_WEAPONTYPE_FROM_CREATOR_INDEX(iRandomInt)
	iAttempts++
	PRINTLN("GET_RANDOM_WEAPON_FOR_INVENTORY - Potential Random Weapon: ", GET_WEAPON_NAME(wtRandomWeapon), " (",iRandomInt,") attempt ", iAttempts)
	
	IF NOT IS_WEAPON_OK_FOR_RANDOM_INVENTORY(wtRandomWeapon)
	AND iAttempts <= ciFMMC_INVENTORY_RANDOM_WEAPON_ATTEMPTS
		RETURN FALSE
	ENDIF
	
	IF iAttempts > ciFMMC_INVENTORY_RANDOM_WEAPON_ATTEMPTS
		wtRandomWeapon = WEAPONTYPE_PISTOL
	ENDIF
	
	wtWep = wtRandomWeapon
	
	RETURN TRUE
ENDFUNC

PROC ADD_WEAPON_FOR_PLAYER_WEAPON_INVENTORY(FMMC_WEAPON_STRUCT &sInventoryWepStruct, BOOL bWeaponsWereRemoved = FALSE, BOOL bForceNoWeaponAddons = FALSE, BOOL bRandomWeapon = FALSE)
	WEAPON_TYPE wtWep = sInventoryWepStruct.wtWeapon
	
	IF bRandomWeapon
		INT iMaxWeapons = GET_MAX_CREATOR_WEAPONTYPES()
		INT iAttempts
		WHILE NOT GET_RANDOM_WEAPON_FOR_INVENTORY(wtWep, iAttempts, iMaxWeapons)
			PRINTLN("ADD_WEAPON_FOR_PLAYER_WEAPON_INVENTORY - Choosing random weapon")
		ENDWHILE
	ENDIF
	
	IF wtWep = WEAPONTYPE_INVALID
	OR ENUM_TO_INT(wtWep) = -1
		EXIT
	ENDIF
	
	INT iAmmo
	IF NOT IS_WEAPON_A_MELEE_WEAPON(wtWep)
		iAmmo = GET_AMMO_AMOUNT_FROM_CREATOR(wtWep, sInventoryWepStruct.iAmmo)
	ENDIF
	SET_WEAPON_DAMAGE_MODIFIER(wtWep, sInventoryWepStruct.fDamageMod)
	EQUIP_PLAYER_WITH_STARTING_MISSION_WEAPONS_AND_AMMO(wtWep, iAmmo, TRUE, bWeaponsWereRemoved, bForceNoWeaponAddons)
	
	IF bRandomWeapon
		EXIT
	ENDIF
	
	INT iComponent
	FOR iComponent = 0 TO FMMC_MAX_WEAPON_COMPONENTS-1
		WEAPONCOMPONENT_TYPE wctComponent = sInventoryWepStruct.eWeaponComponents[iComponent]
		IF wctComponent != WEAPONCOMPONENT_INVALID
		AND wctComponent != INT_TO_ENUM(WEAPONCOMPONENT_TYPE, -1)
			ADD_WEAPON_COMPONENT_FOR_STARTING_INVENTORY(wtWep, wctComponent)
			IF IS_WEAPON_MK2(wtWep)
			AND IS_WEAPON_COMP_A_CAMO_MOD(wctComponent)
			AND sInventoryWepStruct.iLiveryTint >= 0
				SET_STARTING_WEAPON_LIVERY_TINT(wtWep, wctComponent, sInventoryWepStruct.iLiveryTint)
			ENDIF
		ENDIF
	ENDFOR
	
	IF IS_WEAPON_AN_MK2_WEAPON(wtWep)
		AMMO_TYPE atAmmoType
		GET_WEAPON_AMMO_TYPE_FROM_WEAPON_TYPE(wtWep, atAmmoType)
		PRESET_INVENTORY_ADD_MK2_AMMO(wtWep, atAmmoType, iAmmo = -1, iAmmo)
	ENDIF
	
	IF sInventoryWepStruct.iTint != -1
		SET_STARTING_WEAPON_BASE_TINT(wtWep, sInventoryWepStruct.iTint)
	ENDIF
ENDPROC

PROC ADD_PARACHUTE_FOR_PLAYER_WEAPON_INVENTORY(FMMC_PARACHUTE_STRUCT &sParachuteStruct)
	
	IF sParachuteStruct.iCount = ciFMMC_PARACHUTE_COUNT_NONE
		EXIT
	ENDIF
	
	INT iNumParachutes = 1

	GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, iNumParachutes, FALSE, FALSE)
	SET_PLAYER_PARACHUTE_CANOPY(PLAYER_ID(), sParachuteStruct.iTint)
	
	IF sParachuteStruct.iCount = ciFMMC_PARACHUTE_COUNT_MAIN_RES
	OR sParachuteStruct.iCount = ciFMMC_PARACHUTE_COUNT_INFINITE
		GIVE_RESERVE_PARACHUTE_TO_PLAYER(sParachuteStruct.iTint)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_HAS_RESERVE_PARACHUTE, TRUE)
	ENDIF
	
	IF sParachuteStruct.iPackTint != -1
		SET_PLAYER_PARACHUTE_VARIATION_OVERRIDE(PLAYER_ID(), PED_COMP_HAND, GET_PARACHUTE_DRAWABLE_FOR_MP(PLAYER_PED_ID(), sParachuteStruct.iPackTint), GET_PARACHUTE_TEXTURE_FOR_MP(PLAYER_PED_ID(), sParachuteStruct.iPackTint))
		SET_MP_PARACHUTE_BAG_OVERRIDE(PLAYER_ID(), sParachuteStruct.iPackTint)
		SET_PLAYER_PARACHUTE_PACK_TINT_INDEX(PLAYER_ID(), GET_PARACHUTE_PALETTE_FOR_MP(PLAYER_PED_ID(), sParachuteStruct.iPackTint))
		EQUIP_STORED_MP_PARACHUTE(PLAYER_ID(), sParachuteStruct.iTint, sParachuteStruct.iPackTint)
	ENDIF
	
	PRINTLN("ADD_PARACHUTE_FOR_PLAYER_WEAPON_INVENTORY - Added parachute(s) tint: ", sParachuteStruct.iTint, " Pack tint: ", sParachuteStruct.iPackTint)
	
ENDPROC

PROC SET_STARTING_WEAPON_FROM_INVENTORY(FMMC_WEAPON_INVENTORY_STRUCT &sInventoryStruct, WEAPONINHAND WhichWeaponInHand = WEAPONINHAND_LASTWEAPON_BOTH)

	IF NOT IS_BIT_SET(sInventoryStruct.iInventoryBitset, ciFMMC_INVENTORY_BS_START_UNARMED)
	AND sInventoryStruct.iStartWeapon != FMMC_STARTING_WEAPON_UNARMED
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_ARMING)
			IF sInventoryStruct.iStartWeapon != FMMC_STARTING_WEAPON_CURRENT
				WEAPON_TYPE wtWep = sInventoryStruct.sWeaponStruct[sInventoryStruct.iStartWeapon].wtWeapon

				IF DOES_PLAYER_HAVE_WEAPON(wtWep)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWep, TRUE)
					gweapon_type_CurrentlyHeldWeapon = wtWep
					PRINTLN("GIVE_LOCAL_PLAYER_WEAPON_INVENTORY - SET_STARTING_WEAPON_FROM_INVENTORY - SETTING iStartingInventoryStartWep ") 
				ELSE
					PRINTLN("GIVE_LOCAL_PLAYER_WEAPON_INVENTORY - SET_STARTING_WEAPON_FROM_INVENTORY - Could not give starting weapon as player does not have it  ") 
					PUT_WEAPON_IN_HAND(WhichWeaponInHand)
				ENDIF
			ELSE
				WEAPON_TYPE wtStarting = GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())
				IF wtStarting != WEAPONTYPE_INVALID
					PRINTLN("SET_STARTING_WEAPON_FROM_INVENTORY starting : ", GET_WEAPON_NAME(wtStarting))
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtStarting, TRUE)
					gweapon_type_CurrentlyHeldWeapon = wtStarting
				ELSE
					PRINTLN("SET_STARTING_WEAPON_FROM_INVENTORY - Unable to set the current ped weapon as the weapon type was invalid!")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("GIVE_LOCAL_PLAYER_WEAPON_INVENTORY - SET_STARTING_WEAPON_FROM_INVENTORY - Set to start as unarmed")
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
	ENDIF
	
ENDPROC

PROC GIVE_PLAYER_FMMC_INVENTORY(FMMC_WEAPON_INVENTORY_STRUCT &sInventoryStruct, WEAPONINHAND WhichWeaponInHand, BOOL bWeaponsWereRemoved = FALSE)

	IF IS_BIT_SET(sInventoryStruct.iInventoryBitset, ciFMMC_INVENTORY_BS_REMOVE_WEAPONS)
		REMOVE_ALL_PLAYERS_WEAPONS()
		bWeaponsWereRemoved = TRUE
	ENDIF
	
	INT iWeapon
	
	IF IS_BIT_SET(sInventoryStruct.iInventoryBitset, ciFMMC_INVENTORY_BS_GIVE_ARMOUR)
	OR sInventoryStruct.eArmour = eFMMC_ARMOUR_GIVE
		NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
		PRINTLN("GIVE_LOCAL_PLAYER_WEAPON_INVENTORY - Given armour")
	ELIF sInventoryStruct.eArmour = eFMMC_ARMOUR_REMOVE
		SET_PED_ARMOUR(PLAYER_PED_ID(), 0)
		PRINTLN("GIVE_LOCAL_PLAYER_WEAPON_INVENTORY - Removing armour")
	ENDIF
	
	ADD_PARACHUTE_FOR_PLAYER_WEAPON_INVENTORY(sInventoryStruct.sParachute)
	
	BOOL bForceNoWeaponMods = IS_BIT_SET(sInventoryStruct.iInventoryBitset, ciFMMC_INVENTORY_BS_FORCE_NO_ADDONS)
	
	FOR iWeapon = 0 TO FMMC_MAX_INVENTORY_WEAPONS-1
		BOOL bRandomWeapon = IS_BIT_SET(sInventoryStruct.iRandomWeaponBitSet, iWeapon)
		ADD_WEAPON_FOR_PLAYER_WEAPON_INVENTORY(sInventoryStruct.sWeaponStruct[iWeapon], bWeaponsWereRemoved, bForceNoWeaponMods, bRandomWeapon)
	ENDFOR
	
	SET_STARTING_WEAPON_FROM_INVENTORY(sInventoryStruct, WhichWeaponInHand)
	
	IF sInventoryStruct.iAmmoDropTypeOverride != FMMC_PED_AMMO_DROP_TYPE_INVALID
		g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType = sInventoryStruct.iAmmoDropTypeOverride
		PRINTLN("GIVE_LOCAL_PLAYER_WEAPON_INVENTORY - Overriding g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType to ", g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType)
	ENDIF
ENDPROC
