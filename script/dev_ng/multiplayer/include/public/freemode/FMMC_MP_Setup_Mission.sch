//////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FMMC_											//
// Description: Controls the custom missions in free mode that have been created with the creator	//
// Written by:  Robert Wright																		//
// Date: 28/01/2012																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////
///    
USING "rage_builtins.sch"
USING "globals.sch"
//Game Headers
USING "commands_network.sch"
USING "script_player.sch"
//Network Headers
USING "net_events.sch"
USING "net_hud_displays.sch"
USING "net_mission_info.sch"

USING "screens_header.sch"
//USING "FMMC_header.sch"
USING "FMMC_Cloud_Loader.sch"
USING "Freemode_header.sch"
USING "mp_scaleform_functions.sch"
USING "transition_common.sch"
USING "net_transition_sessions.sch"
USING "net_mission_trigger_public.sch"
USING "FMMC_SCTV_TICKER.sch"
USING "net_clouds.sch"

STRUCT FMMC_LOCAL_DATA_STRUCT
	BLIP_INDEX 		biPedBlip[FMMC_MAX_PEDS]
	BLIP_INDEX 		biVehicleBlip[FMMC_MAX_VEHICLES]
	BLIP_INDEX 		biObject[FMMC_MAX_NUM_OBJECTS]
ENDSTRUCT

//2020 Controller
STRUCT FMMC_MISSION_SERVER_DATA_STRUCT
	
	NETWORK_INDEX 		niPed[FMMC_MAX_PEDS]
	NETWORK_INDEX 		niVehicle[FMMC_MAX_VEHICLES]
	NETWORK_INDEX 		niTrain[FMMC_MAX_TRAINS]
	NETWORK_INDEX 		niVehicleCrate[FMMC_MAX_VEHICLES]
	NETWORK_INDEX 		niNetworkedObject[FMMC_MAX_NUM_NETWORKED_OBJ_POOL]
		
	INT iDuration 	
	INT iTarget		
	INT iSpawnTime	
	INT iAim			
	INT iBlips		
	INT iTags			
	INT iVoice		
	INT iWeaponRespawnTime
	INT iVehicleRespawnTime 
	
	BOOL bIsTeam
	
	INT			 		iPlayerMissionToLoad
	INT 				iMissionVariation			= -1
	
ENDSTRUCT

//Legacy Controller - Do not change
STRUCT FMMC_SERVER_DATA_STRUCT
	NETWORK_INDEX 		niPed[FMMC_MAX_PEDS]
	NETWORK_INDEX 		niVehicle[FMMC_MAX_VEHICLES]
	NETWORK_INDEX		niObject[FMMC_MAX_NUM_OBJECTS]
	NETWORK_INDEX		niDynoProps[FMMC_MAX_NUM_DYNOPROPS]
	NETWORK_INDEX 		niVehicleCrate[FMMC_MAX_VEHICLES]
		
	INT iDuration 	
	INT iTarget		
	INT iSpawnTime	
	INT iAim			
	INT iBlips		
	INT iTags			
	INT iVoice		
	INT iWeaponRespawnTime
	INT iVehicleRespawnTime 
	
	BOOL bIsTeam
	
	INT			 		iPlayerMissionToLoad
	INT 				iMissionVariation			= -1
		
	// Legacy
	int casco_index = -1
	NETWORK_INDEX		niTrain
ENDSTRUCT

//Legacy Controller - Do not change
STRUCT sCreatedCountData

	INT iNumVehCreated[FMMC_MAX_RULES]
	INT iNumObjCreated[FMMC_MAX_RULES]
	INT iNumPedCreated[FMMC_MAX_RULES]
	INT iSkipVehBitset
	INT iSkipObjBitset
	INT iSkipPedBitset[3]
	
ENDSTRUCT

//2020 Controller
STRUCT FMMC_CREATED_COUNT_DATA

	INT iNumVehCreated[FMMC_MAX_RULES]
	INT iNumTrainsCreated
	INT iNumObjCreated[FMMC_MAX_RULES]
	INT iNumPedCreated[FMMC_MAX_RULES]
	INT iSkipVehBitset
	INT iSkipObjBitset
	INT iSkipPedBitset[3]
	
ENDSTRUCT

//Print out all the data so we can see it in debug
#IF IS_DEBUG_BUILD
FUNC STRING CONVERT_RULE_INT_TO_STRING_FOR_PRINTING(INT iRule)
	SWITCH iRule
		CASE FMMC_OBJECTIVE_LOGIC_NONE				RETURN "FMMC_OBJECTIVE_LOGIC_NONE"
		CASE FMMC_OBJECTIVE_LOGIC_PHOTO				RETURN "FMMC_OBJECTIVE_LOGIC_PHOTO"
		CASE FMMC_OBJECTIVE_LOGIC_MINIGAME			RETURN "FMMC_OBJECTIVE_LOGIC_MINIGAME"
		CASE FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER	RETURN "FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER"
		CASE FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD		RETURN "FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD"
		CASE FMMC_OBJECTIVE_LOGIC_KILL				RETURN "FMMC_OBJECTIVE_LOGIC_KILL"
		CASE FMMC_OBJECTIVE_LOGIC_DAMAGE			RETURN "FMMC_OBJECTIVE_LOGIC_DAMAGE"	
		CASE FMMC_OBJECTIVE_LOGIC_PROTECT			RETURN "FMMC_OBJECTIVE_LOGIC_PROTECT"
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO				RETURN "FMMC_OBJECTIVE_LOGIC_GO_TO"
		CASE FMMC_OBJECTIVE_LOGIC_CAPTURE			RETURN "FMMC_OBJECTIVE_LOGIC_CAPTURE"
		CASE FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS		RETURN "FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS"
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM0		RETURN "FMMC_OBJECTIVE_LOGIC_KILL_TEAM0"
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM1		RETURN "FMMC_OBJECTIVE_LOGIC_KILL_TEAM1"
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM2		RETURN "FMMC_OBJECTIVE_LOGIC_KILL_TEAM2"
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM3		RETURN "FMMC_OBJECTIVE_LOGIC_KILL_TEAM3"
		CASE FMMC_OBJECTIVE_LOGIC_GET_MASKS			RETURN "FMMC_OBJECTIVE_LOGIC_GET_MASKS"
		CASE FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL		RETURN "FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL"
		CASE FMMC_OBJECTIVE_LOGIC_CHARM				RETURN "FMMC_OBJECTIVE_LOGIC_CHARM"
		CASE FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE	RETURN "FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE"
		CASE FMMC_OBJECTIVE_LOGIC_ARREST_TEAM0		RETURN "FMMC_OBJECTIVE_LOGIC_ARREST_TEAM0"
		CASE FMMC_OBJECTIVE_LOGIC_ARREST_TEAM1		RETURN "FMMC_OBJECTIVE_LOGIC_ARREST_TEAM1"
		CASE FMMC_OBJECTIVE_LOGIC_ARREST_TEAM2		RETURN "FMMC_OBJECTIVE_LOGIC_ARREST_TEAM2"
		CASE FMMC_OBJECTIVE_LOGIC_ARREST_TEAM3		RETURN "FMMC_OBJECTIVE_LOGIC_ARREST_TEAM3"
		CASE FMMC_OBJECTIVE_LOGIC_ARREST_ALL 		RETURN "FMMC_OBJECTIVE_LOGIC_ARREST_ALL"
		CASE FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION	RETURN "FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION"
		CASE FMMC_OBJECTIVE_LOGIC_DESTROY			RETURN "FMMC_OBJECTIVE_LOGIC_DESTROY"
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM0		RETURN "FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM0"
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM1		RETURN "FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM1"
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM2		RETURN "FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM2"
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM3		RETURN "FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM3"
		CASE FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY		RETURN "FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY"
		CASE FMMC_OBJECTIVE_LOGIC_LOOT_THRESHOLD	RETURN "FMMC_OBJECTIVE_LOGIC_LOOT_THRESHOLD"
		CASE FMMC_OBJECTIVE_LOGIC_POINTS_THRESHOLD	RETURN "FMMC_OBJECTIVE_LOGIC_POINTS_THRESHOLD"		
		CASE FMMC_OBJECTIVE_LOGIC_PED_GOTO_LOCATION	RETURN "FMMC_OBJECTIVE_LOGIC_PED_GOTO_LOCATION"
		CASE FMMC_OBJECTIVE_LOGIC_HOLDING_RULE		RETURN "FMMC_OBJECTIVE_LOGIC_HOLDING_RULE"
	ENDSWITCH
	RETURN ""
ENDFUNC
FUNC STRING CONVERT_TEAM_INT_TO_STRING_FOR_PRINTING(INT iTeam)
	SWITCH iTeam
		CASE ciPED_RELATION_SHIP_HATE				RETURN "ciPED_RELATION_SHIP_HATE"	
		CASE ciPED_RELATION_SHIP_DISLIKE			RETURN "ciPED_RELATION_SHIP_DISLIKE"
		CASE ciPED_RELATION_SHIP_LIKE				RETURN "ciPED_RELATION_SHIP_LIKE"	
	ENDSWITCH
	RETURN ""
ENDFUNC
FUNC STRING CONVERT_TEAM_INT_TO_STRING_FOR_PRINTING_DM(INT iTeam)
	SWITCH iTeam
//		CASE TDM_TEAM_A			RETURN "TDM_TEAM_A"	
//		CASE TDM_TEAM_B			RETURN "TDM_TEAM_B"	
		CASE 0 RETURN "TEAM 0"	
		CASE 1 RETURN "TEAM 1"	
		CASE 2 RETURN "TEAM 2"	
		CASE 3 RETURN "TEAM 3"	
		CASE 4 RETURN "TEAM 4"	
		CASE 5 RETURN "TEAM 5"	
		CASE 6 RETURN "TEAM 6"	
		CASE 7 RETURN "TEAM 7"	
	ENDSWITCH
	RETURN "TDM_TEAM_XXXXXXXXXXXXXXXXXXXX"
ENDFUNC

FUNC STRING GET_MODEL_STING_FROM_TYPE(MODEL_NAMES mnModel)
	SWITCH mnModel
		CASE S_M_M_Armoured_01			RETURN "S_M_M_Armoured_01"	
		CASE S_F_Y_Cop_01				RETURN "S_F_Y_Cop_01"	
		CASE G_M_Y_MexGoon_02			RETURN "G_M_Y_MexGoon_02"	
		CASE G_M_M_ChiBoss_01			RETURN "G_M_M_ChiBoss_0"	
		CASE S_M_Y_Marine_01			RETURN "S_M_Y_Marine_01"	
		CASE S_F_Y_Hooker_02			RETURN "S_F_Y_Hooker_02"	
		CASE A_M_M_Hillbilly_01			RETURN "A_M_M_Hillbilly_01"	
		CASE A_F_M_BodyBuild_01			RETURN "A_F_M_BodyBuild_01"	
		CASE PROP_DRUG_PACKAGE	 		RETURN "PROP_DRUG_PACKAGE"	
		CASE PROP_DRUG_PACKAGE_02		RETURN "PROP_DRUG_PACKAGE_02"	
		CASE PROP_GOLD_BAR				RETURN "PROP_GOLD_BAR"	
		CASE PROP_BOMB_01				RETURN "PROP_BOMB_01"	
		CASE Vader						RETURN "Vader"	
		CASE Seashark					RETURN "Seashark"	
		CASE NINEF						RETURN "NINEF"	
		CASE landstalker				RETURN "landstalker"	
		CASE buzzard2					RETURN "buzzard2"	
		CASE Gauntlet					RETURN "Gauntlet"	
		CASE bjxl						RETURN "bjxl"	
		CASE police						RETURN "police"	
		CASE Prop_MP_Ramp_01			RETURN "Prop_MP_Ramp_01"	
		CASE Prop_MP_Ramp_02			RETURN "Prop_MP_Ramp_02"	
		CASE Prop_MP_Ramp_03			RETURN "Prop_MP_Ramp_03"	
		CASE Prop_RoadCone01A			RETURN "Prop_MP_Cone_01"	
		CASE Prop_RoadCone02A			RETURN "Prop_MP_Cone_02"	
		CASE PROP_ROADCONE02C			RETURN "Prop_MP_Cone_03"	
		CASE Prop_RoadPole_01a			RETURN "Prop_MP_Cone_04"	
		CASE Prop_MP_Barrier_02			RETURN "Prop_MP_Barrier_02"	
		CASE PROP_MP_ARROW_BARRIER_01	RETURN "PROP_MP_ARROW_BARRIER_01"	
		CASE Prop_MP_Barrier_01			RETURN "Prop_MP_Barrier_01"	
		CASE prop_box_wood04a			RETURN "prop_box_wood04a"	
		CASE prop_conc_sacks_02a		RETURN "prop_conc_sacks_02a"	
		CASE Prop_Contr_03b_LD			RETURN "Prop_Contr_03b_LD"	
		CASE PROP_FNCSEC_03B			RETURN "PROP_FNCSEC_03B"	
		CASE PROP_CONST_FENCE02B		RETURN "PROP_CONST_FENCE02B"	
		CASE Prop_Rub_Tyre_03			RETURN "Prop_Rub_Tyre_03"	
		CASE Prop_Pipes_conc_01			RETURN "Prop_Pipes_conc_01"	
	ENDSWITCH
	RETURN ""
ENDFUNC



FUNC  STRING GET_WEAPON_NAME_FROM_PT(PICKUP_TYPE ptPickUp) 

	ptPickUp = CONVERT_OLD_PICKUP_TYPE_TO_NEW(ptPickUp)
	
	SWITCH ptPickUp
		CASE PICKUP_ARMOUR_STANDARD				RETURN "PICKUP_ARMOUR_STANDARD"	
		CASE PICKUP_HEALTH_STANDARD				RETURN "PICKUP_HEALTH_STANDARD"	
		CASE PICKUP_WEAPON_PISTOL				RETURN "PICKUP_WEAPON_PISTOL"
		CASE PICKUP_WEAPON_DLC_PISTOL_MK2		RETURN "PICKUP_WEAPON_DLC_PISTOL_MK2"
		CASE PICKUP_WEAPON_COMBATPISTOL			RETURN "PICKUP_WEAPON_COMBATPISTOL"	
		CASE PICKUP_WEAPON_DLC_PISTOL50			RETURN "PICKUP_WEAPON_DLC_PISTOL50"	
		CASE PICKUP_WEAPON_DLC_COMPACTRIFLE		RETURN "PICKUP_WEAPON_DLC_COMPACTRIFLE"
		CASE PICKUP_WEAPON_DLC_DBSHOTGUN		RETURN "PICKUP_WEAPON_DLC_DBSHOTGUN"
		CASE PICKUP_WEAPON_DLC_MACHINEPISTOL	RETURN "PICKUP_WEAPON_DLC_MACHINEPISTOL"
		CASE PICKUP_WEAPON_APPISTOL				RETURN "PICKUP_WEAPON_APPISTOL"	
		CASE PICKUP_WEAPON_MICROSMG				RETURN "PICKUP_WEAPON_MICROSMG"	
		CASE PICKUP_WEAPON_SMG					RETURN "PICKUP_WEAPON_SMG"
		CASE PICKUP_WEAPON_DLC_SMG_MK2			RETURN "PICKUP_WEAPON_DLC_SMG_MK2"
		CASE PICKUP_WEAPON_DLC_ASSAULTSMG	   	RETURN "PICKUP_WEAPON_DLC_ASSAULTSMG"	
		CASE PICKUP_WEAPON_ASSAULTRIFLE			RETURN "PICKUP_WEAPON_ASSAULTRIFLE"	
		CASE PICKUP_WEAPON_CARBINERIFLE			RETURN "PICKUP_WEAPON_CARBINERIFLE"	
		CASE PICKUP_WEAPON_DLC_ASSAULTRIFLE_MK2	RETURN "PICKUP_WEAPON_DLC_ASSAULTRIFLE_MK2"	
		CASE PICKUP_WEAPON_DLC_CARBINERIFLE_MK2	RETURN "PICKUP_WEAPON_DLC_CARBINERIFLE_MK2"	
		CASE PICKUP_WEAPON_DLC_HEAVYRIFLE		RETURN "PICKUP_WEAPON_DLC_HEAVYRIFLE"	
		CASE PICKUP_WEAPON_ADVANCEDRIFLE		RETURN "PICKUP_WEAPON_ADVANCEDRIFLE"	
		CASE PICKUP_WEAPON_MG					RETURN "PICKUP_WEAPON_MG"	
		CASE PICKUP_WEAPON_COMBATMG				RETURN "PICKUP_WEAPON_COMBATMG"	
		CASE PICKUP_WEAPON_DLC_COMBATMG_MK2		RETURN "PICKUP_WEAPON_DLC_COMBATMG_MK2"
		CASE PICKUP_WEAPON_DLC_ASSAULTMG		RETURN "PICKUP_WEAPON_DLC_ASSAULTMG"	
		CASE PICKUP_WEAPON_PUMPSHOTGUN			RETURN "PICKUP_WEAPON_PUMPSHOTGUN"	
		CASE PICKUP_WEAPON_DLC_BULLPUPSHOTGUN	RETURN "PICKUP_WEAPON_DLC_BULLPUPSHOTGUN"	
		CASE PICKUP_WEAPON_ASSAULTSHOTGUN		RETURN "PICKUP_WEAPON_ASSAULTSHOTGUN"	
		CASE PICKUP_WEAPON_SNIPERRIFLE			RETURN "PICKUP_WEAPON_SNIPERRIFLE"	
		CASE PICKUP_WEAPON_DLC_ASSAULTSNIPER	RETURN "PICKUP_WEAPON_DLC_ASSAULTSNIPER"	
		CASE PICKUP_WEAPON_HEAVYSNIPER			RETURN "PICKUP_WEAPON_HEAVYSNIPER"	
		CASE PICKUP_WEAPON_DLC_HEAVYSNIPER_MK2	RETURN "PICKUP_WEAPON_DLC_HEAVYSNIPER_MK2"	
		CASE PICKUP_WEAPON_GRENADELAUNCHER		RETURN "PICKUP_WEAPON_GRENADELAUNCHER"	
		CASE PICKUP_WEAPON_RPG					RETURN "PICKUP_WEAPON_RPG"	
		CASE PICKUP_WEAPON_MINIGUN				RETURN "PICKUP_WEAPON_MINIGUN"	
		CASE PICKUP_WEAPON_GRENADE				RETURN "PICKUP_WEAPON_GRENADE"	
		CASE PICKUP_WEAPON_SMOKEGRENADE			RETURN "PICKUP_WEAPON_SMOKEGRENADE"	
		CASE PICKUP_WEAPON_STICKYBOMB			RETURN "PICKUP_WEAPON_STICKYBOMB"	
		CASE PICKUP_WEAPON_STUNGUN				RETURN "PICKUP_WEAPON_STUNGUN"	
		CASE PICKUP_WEAPON_RUBBERGUN			RETURN "PICKUP_WEAPON_RUBBERGUN"	
		CASE PICKUP_WEAPON_DLC_PROGRAMMABLEAR	RETURN "PICKUP_WEAPON_DLC_PROGRAMMABLEAR"	
		CASE PICKUP_WEAPON_FIREEXTINGUISHER		RETURN "PICKUP_WEAPON_FIREEXTINGUISHER"	
		CASE PICKUP_WEAPON_PETROLCAN			RETURN "PICKUP_WEAPON_PETROLCAN"	
		CASE PICKUP_WEAPON_LOUDHAILER			RETURN "PICKUP_WEAPON_LOUDHAILER"	
//		CASE PICKUP_WEAPON_LASSO				RETURN "PICKUP_WEAPON_LASSO"	
		CASE PICKUP_WEAPON_KNIFE				RETURN "PICKUP_WEAPON_KNIFE"	
		CASE PICKUP_WEAPON_NIGHTSTICK			RETURN "PICKUP_WEAPON_NIGHTSTICK"	
		CASE PICKUP_WEAPON_HAMMER				RETURN "PICKUP_WEAPON_HAMMER"	
		CASE PICKUP_WEAPON_BAT					RETURN "PICKUP_WEAPON_BAT"	
		//CASE PICKUP_WEAPON_GOLFCLUB			RETURN "PICKUP_WEAPON_GOLFCLUB"	
		CASE PICKUP_WEAPON_DLC_SWITCHBLADE		RETURN "PICKUP_WEAPON_DLC_SWITCHBLADE" 
		CASE PICKUP_WEAPON_DLC_REVOLVER			RETURN "PICKUP_WEAPON_DLC_REVOLVER" 
		CASE PICKUP_VEHICLE_HEALTH_STANDARD		RETURN "PICKUP_VEHICLE_HEALTH_STANDARD"	
		CASE PICKUP_VEHICLE_HEALTH_STANDARD_LOW_GLOW	RETURN "PICKUP_VEHICLE_HEALTH_STANDARD_LOW_GLOW"	
//		CASE PICKUP_VEHICLE_ARMOUR_STANDARD		RETURN "PICKUP_VEHICLE_ARMOUR_STANDARD"	
		CASE PICKUP_VEHICLE_WEAPON_PISTOL		RETURN "PICKUP_VEHICLE_WEAPON_PISTOL"	
		CASE PICKUP_VEHICLE_WEAPON_COMBATPISTOL	RETURN "PICKUP_VEHICLE_WEAPON_COMBATPISTOL"	
		CASE PICKUP_VEHICLE_WEAPON_DLC_PISTOL50		RETURN "PICKUP_VEHICLE_WEAPON_DLC_PISTOL50"	
		CASE PICKUP_VEHICLE_WEAPON_APPISTOL		RETURN "PICKUP_VEHICLE_WEAPON_APPISTOL"	
		CASE PICKUP_VEHICLE_WEAPON_MICROSMG		RETURN "PICKUP_VEHICLE_WEAPON_MICROSMG"	
		CASE PICKUP_VEHICLE_WEAPON_SMG			RETURN "PICKUP_VEHICLE_WEAPON_SMG"	
		CASE PICKUP_VEHICLE_WEAPON_SAWNOFF		RETURN "PICKUP_VEHICLE_WEAPON_SAWNOFFSHOTGUN"	
		CASE PICKUP_VEHICLE_WEAPON_DLC_ASSAULTSMG	RETURN "PICKUP_VEHICLE_WEAPON_DLC_ASSAULTSMG"	
		CASE PICKUP_VEHICLE_WEAPON_GRENADE		RETURN "PICKUP_VEHICLE_WEAPON_GRENADE"	
		CASE PICKUP_VEHICLE_WEAPON_SMOKEGRENADE	RETURN "PICKUP_VEHICLE_WEAPON_SMOKEGRENADE"	
		CASE PICKUP_VEHICLE_WEAPON_STICKYBOMB	RETURN "PICKUP_VEHICLE_WEAPON_STICKYBOMB"	
		CASE PICKUP_VEHICLE_WEAPON_MOLOTOV		RETURN "PICKUP_VEHICLE_WEAPON_MOLOTOV"
		CASE PICKUP_WEAPON_DLC_RAILGUN			RETURN "PICKUP_WEAPON_RAILGUN"		
		CASE PICKUP_WEAPON_DLC_SNSPISTOL_MK2	RETURN "PICKUP_WEAPON_DLC_SNSPISTOL_MK2"	
		CASE PICKUP_WEAPON_DLC_REVOLVER_MK2		RETURN "PICKUP_WEAPON_DLC_REVOLVER_MK2"	
		CASE PICKUP_WEAPON_DLC_BULLPUPRIFLE_MK2	RETURN "PICKUP_WEAPON_DLC_BULLPUPRIFLE_MK2"	
		CASE PICKUP_WEAPON_DLC_SPECIALCARBINE_MK2  	RETURN "PICKUP_WEAPON_DLC_SPECIALCARBINE_MK2" 
		CASE PICKUP_WEAPON_DLC_PUMPSHOTGUN_MK2		RETURN "PICKUP_WEAPON_DLC_PUMPSHOTGUN_MK2"	
		CASE PICKUP_WEAPON_DLC_MARKSMANRIFLE_MK2	RETURN "PICKUP_WEAPON_DLC_MARKSMANRIFLE_MK2"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC SAVE_NAMED_VECTOR_TO_NAMED_DEBUG_FILE_WITH_TABS(STRING strVectorName, VECTOR vPos, STRING strPath, STRING strFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(strVectorName, strPath, strFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	", strPath, strFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vPos.x, strPath, strFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	", strPath, strFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vPos.y, strPath, strFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	", strPath, strFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vPos.z, strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
ENDPROC

PROC DEBUG_EXPORT_ROCKSTAR_MISSIONS_DETAILS(INT iLesRating)
	PRINTLN("DEBUG_EXPORT_ROCKSTAR_MISSIONS_DETAILS")
	STRING strPath = "X:/gta5/build/dev/"
	STRING strFile = "RockstarMissionDetails.txt"
	INT iLoop
	TEXT_LABEL_23 tl23
	OPEN_NAMED_DEBUG_FILE	(strPath, strFile)	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("(", strPath, strFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iLesRating, strPath, strFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(")	", strPath, strFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(g_FMMC_STRUCT.tl63MissionName, strPath, strFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(g_FMMC_STRUCT.tl63MissionDecription[0], strPath, strFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	SAVE_NAMED_VECTOR_TO_NAMED_DEBUG_FILE_WITH_TABS("START", g_FMMC_STRUCT.vStartPos, strPath, strFile)
	IF IS_THIS_A_MISSION()
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds != 0
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("PEDS", strPath, strFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)	
			FOR iLoop = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds -1)
				tl23 = "ped "
				tl23 += iLoop
				SAVE_NAMED_VECTOR_TO_NAMED_DEBUG_FILE_WITH_TABS(tl23, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iLoop].vPos, strPath, strFile)
			ENDFOR
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles != 0
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("VEHICLES", strPath, strFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)	
			FOR iLoop = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
				tl23 = "veh "
				tl23 += iLoop
				SAVE_NAMED_VECTOR_TO_NAMED_DEBUG_FILE_WITH_TABS(tl23, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iLoop].vPos, strPath, strFile)
			ENDFOR
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfObjects != 0
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("OBJECTS", strPath, strFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)	
			FOR iLoop = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
				tl23 = "obj "
				tl23 += iLoop
				SAVE_NAMED_VECTOR_TO_NAMED_DEBUG_FILE_WITH_TABS(tl23, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iLoop].vPos, strPath, strFile)
			ENDFOR
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfProps != 0
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("PROPS", strPath, strFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)	
			FOR iLoop = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps- 1)
				tl23 = "prop "
				tl23 += iLoop
				SAVE_NAMED_VECTOR_TO_NAMED_DEBUG_FILE_WITH_TABS(tl23, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iLoop].vPos, strPath, strFile)
			ENDFOR
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons != 0
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("WEAPONS", strPath, strFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)		
			FOR iLoop = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons -1)
				tl23 = "wep "
				tl23 += iLoop
				SAVE_NAMED_VECTOR_TO_NAMED_DEBUG_FILE_WITH_TABS(tl23, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iLoop].vPos, strPath, strFile)
			ENDFOR
		ENDIF
	ELIF IS_THIS_A_RACE()	
		IF g_FMMC_STRUCT.iNumberOfCheckPoints != 0
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("WEAPONS", strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)	
			FOR iLoop = 0 TO g_FMMC_STRUCT.iNumberOfCheckPoints
				tl23 = "checkpoint "
				tl23 += iLoop
				SAVE_NAMED_VECTOR_TO_NAMED_DEBUG_FILE_WITH_TABS(tl23, g_FMMC_STRUCT.sPlacedCheckpoint[iLoop].vCheckPoint, strPath, strFile)
			ENDFOR
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons != 0
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("CHECKPOINTS", strPath, strFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)		
			FOR iLoop = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons -1)
				tl23 = "wep "
				tl23 += iLoop
				SAVE_NAMED_VECTOR_TO_NAMED_DEBUG_FILE_WITH_TABS(tl23, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iLoop].vPos, strPath, strFile)
			ENDFOR
		ENDIF
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints != 0
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("SPAWN POINTS", strPath, strFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)		
			FOR iLoop = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints -1)
				tl23 = "spawn "
				tl23 += iLoop
				SAVE_NAMED_VECTOR_TO_NAMED_DEBUG_FILE_WITH_TABS(tl23, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iLoop].vPos, strPath, strFile)
			ENDFOR
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons != 0
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("WEAPONS", strPath, strFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)		
			FOR iLoop = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons -1)
				tl23 = "wep "
				tl23 += iLoop
				SAVE_NAMED_VECTOR_TO_NAMED_DEBUG_FILE_WITH_TABS(tl23, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iLoop].vPos, strPath, strFile)
			ENDFOR
		ENDIF
	ENDIF
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	CLOSE_DEBUG_FILE()
ENDPROC

FUNC STRING GET_ci_CSBS_CUT_DEBUG_PRINT(INT i)
	SWITCH i
		CASE ci_CSBS_SkipIfTooFar  					RETURN "ci_CSBS_SkipIfTooFar           "
		CASE ci_CSBS_WarpPlayersStart    		 	RETURN "ci_CSBS_WarpPlayersStart       "    		
		CASE ci_CSBS_RemoveFromVehStart     		RETURN "ci_CSBS_RemoveFromVehStart        "  		
		CASE ci_CSBS_WarpPlayersEnd      		 	RETURN "ci_CSBS_WarpPlayersEnd         "   	
		CASE ci_CSBS_Hide_Other_Players  		 	RETURN "ci_CSBS_Hide_Other_Players     "
		CASE ci_CSBS_Hide_Your_Player    		 	RETURN "ci_CSBS_Hide_Your_Player       "
		CASE ci_CSBS_Pull_Team0_In       		 	RETURN "ci_CSBS_Pull_Team0_In          "
		CASE ci_CSBS_Pull_Team1_In       		 	RETURN "ci_CSBS_Pull_Team1_In          "
		CASE ci_CSBS_Pull_Team2_In       		 	RETURN "ci_CSBS_Pull_Team2_In          "
		CASE ci_CSBS_Pull_Team3_In       		 	RETURN "ci_CSBS_Pull_Team3_In          "
		CASE ci_CSBS_Phone_Intro				 	RETURN "ci_CSBS_Phone_Intro            "
		CASE ci_CSBS_Hint_At_Start			 		RETURN "ci_CSBS_Hint_At_Start          "	
		CASE ci_CSBS_Hacking_App_intro		  		RETURN "ci_CSBS_Hacking_App_intro      "   	
		CASE ci_CSBS_Heist_Leader_Primary		  	RETURN "ci_CSBS_Heist_Leader_Primary   "
		CASE ci_CSBS_Team0_Closest			 		RETURN "ci_CSBS_Team0_Closest          "
		CASE ci_CSBS_Team1_Closest			 		RETURN "ci_CSBS_Team1_Closest          "
		CASE ci_CSBS_Team2_Closest			 		RETURN "ci_CSBS_Team2_Closest          "
		CASE ci_CSBS_Team3_Closest			 		RETURN "ci_CSBS_Team3_Closest          "
		CASE ci_CSBS_Rolling_Start			 		RETURN "ci_CSBS_Rolling_Start          "
		CASE ci_CSBS_Heist_Leader_Secondary	 		RETURN "ci_CSBS_Heist_Leader_Secondary "
		CASE ci_CSBS_Team0_Closest_Secondary	  	RETURN "ci_CSBS_Team0_Closest_Secondary"
		CASE ci_CSBS_Team1_Closest_Secondary	  	RETURN "ci_CSBS_Team1_Closest_Secondary"
		CASE ci_CSBS_Team2_Closest_Secondary	  	RETURN "ci_CSBS_Team2_Closest_Secondary"
		CASE ci_CSBS_Team3_Closest_Secondary	  	RETURN "ci_CSBS_Team3_Closest_Secondary"
		CASE ci_CSBS_Team0_IdleAnims			  	RETURN "ci_CSBS_Team0_IdleAnims        "
		CASE ci_CSBS_Team1_IdleAnims			  	RETURN "ci_CSBS_Team1_IdleAnims        "
		CASE ci_CSBS_Team2_IdleAnims			  	RETURN "ci_CSBS_Team2_IdleAnims        "
		CASE ci_CSBS_Team3_IdleAnims			  	RETURN "ci_CSBS_Team3_IdleAnims        "
		CASE ci_CSBS_Hide_Phone_Instantly			RETURN "ci_CSBS_Hide_Phone_Instantly 	"
		CASE ci_CSBS_Lock_Player_Vehicle			RETURN "ci_CSBS_Lock_Player_Vehicle 	"
	ENDSWITCH 
	RETURN ""
ENDFUNC
FUNC STRING GET_ci_CSBS_CUT_DEBUG_PRINT_SET_2(INT i)
	SWITCH i
		CASE ci_CSBS2_Heist_Leader_Third  		RETURN "ci_CSBS2_Heist_Leader_Third  "
		CASE ci_CSBS2_Team0_Closest_Third	 	RETURN "ci_CSBS2_Team0_Closest_Third "    		
		CASE ci_CSBS2_Team1_Closest_Third	 	RETURN "ci_CSBS2_Team1_Closest_Third "  		
		CASE ci_CSBS2_Team2_Closest_Third	 	RETURN "ci_CSBS2_Team2_Closest_Third "   	
		CASE ci_CSBS2_Team3_Closest_Third	 	RETURN "ci_CSBS2_Team3_Closest_Third "
	ENDSWITCH 
	RETURN ""
ENDFUNC
FUNC STRING GET_sGotoLocationDataiLocBS_DEBUG_PRINT(INT i)
	SWITCH i
		CASE ciLoc_BS_HideBlip                		RETURN "ciLoc_BS_HideBlip                "
		CASE ciLoc_BS_FlagMarker              	 	RETURN "ciLoc_BS_FlagMarker              "    		
		CASE ciLoc_BS_HideMarker              	 	RETURN "ciLoc_BS_HideMarker              "  		
		CASE ciLoc_BS_StopVehicle             	 	RETURN "ciLoc_BS_StopVehicle             "   	
		CASE ciLoc_BS_ReturnToHeistApartment  	 	RETURN "ciLoc_BS_ReturnToHeistApartment  "
		CASE ciLoc_BS_HideCaptAreaOnMap       	 	RETURN "ciLoc_BS_HideCaptAreaOnMap       "
		CASE ciLoc_BS_RotateManual            	 	RETURN "ciLoc_BS_RotateManual            "
		CASE ciLoc_BS_HideBlipWhenInLocation  	 	RETURN "ciLoc_BS_HideBlipWhenInLocation  "
		CASE ciLoc_BS_AvoidHighwayGPS         	 	RETURN "ciLoc_BS_AvoidHighwayGPS         "
		CASE ciLoc_BS_DirectionFailObjective  	 	RETURN "ciLoc_BS_DirectionFailObjective  "
		CASE ciLoc_BS_DirectionFailMission    	 	RETURN "ciLoc_BS_DirectionFailMission    "
		CASE ciLoc_BS_PedsLeaveVehInLocation   		RETURN "ciLoc_BS_PedsLeaveVehInLocation  "	
		CASE ciLoc_BS_CustomTeams_T0_Needs_T0   	RETURN "ciLoc_BS_CustomTeams_T0_Needs_T0 "   	
		CASE ciLoc_BS_CustomTeams_T0_Needs_T1 	  	RETURN "ciLoc_BS_CustomTeams_T0_Needs_T1 "
		CASE ciLoc_BS_CustomTeams_T0_Needs_T2  		RETURN "ciLoc_BS_CustomTeams_T0_Needs_T2 "
		CASE ciLoc_BS_CustomTeams_T0_Needs_T3  		RETURN "ciLoc_BS_CustomTeams_T0_Needs_T3 "
		CASE ciLoc_BS_CustomTeams_T1_Needs_T0  		RETURN "ciLoc_BS_CustomTeams_T1_Needs_T0 "
		CASE ciLoc_BS_CustomTeams_T1_Needs_T1  		RETURN "ciLoc_BS_CustomTeams_T1_Needs_T1 "
		CASE ciLoc_BS_CustomTeams_T1_Needs_T2  		RETURN "ciLoc_BS_CustomTeams_T1_Needs_T2 "
		CASE ciLoc_BS_CustomTeams_T1_Needs_T3  		RETURN "ciLoc_BS_CustomTeams_T1_Needs_T3 "
		CASE ciLoc_BS_CustomTeams_T2_Needs_T0 	  	RETURN "ciLoc_BS_CustomTeams_T2_Needs_T0 "
		CASE ciLoc_BS_CustomTeams_T2_Needs_T1 	  	RETURN "ciLoc_BS_CustomTeams_T2_Needs_T1 "
		CASE ciLoc_BS_CustomTeams_T2_Needs_T2 	  	RETURN "ciLoc_BS_CustomTeams_T2_Needs_T2 "
		CASE ciLoc_BS_CustomTeams_T2_Needs_T3 	  	RETURN "ciLoc_BS_CustomTeams_T2_Needs_T3 "
		CASE ciLoc_BS_CustomTeams_T3_Needs_T0 	  	RETURN "ciLoc_BS_CustomTeams_T3_Needs_T0 "
		CASE ciLoc_BS_CustomTeams_T3_Needs_T1 	  	RETURN "ciLoc_BS_CustomTeams_T3_Needs_T1 "
		CASE ciLoc_BS_CustomTeams_T3_Needs_T2 	  	RETURN "ciLoc_BS_CustomTeams_T3_Needs_T2 "
		CASE ciLoc_BS_CustomTeams_T3_Needs_T3 	  	RETURN "ciLoc_BS_CustomTeams_T3_Needs_T3 "
	ENDSWITCH 
	RETURN ""
ENDFUNC

PROC PRINT_ARENA_INFO(ARENA_INFO &sArena)
	PRINTLN("[TMS][ARENA] Printing Arena Info =================================")
	PRINTLN("[TMS][ARENA] iArena_Theme: ", sArena.iArena_Theme)
	PRINTLN("[TMS][ARENA] iArena_Variation: ", sArena.iArena_Variation)
	
	PRINTLN("[TMS][ARENA] iArena_Lighting: ", sArena.iArena_Lighting)
	PRINTLN("[TMS][ARENA] iArena_RandomLightingBS: ", sArena.iArena_RandomLightingBS)
ENDPROC

PROC PRINT_TEAM_DATA_STRUCT(INT iTeam)
	PRINTLN("PRINT_TEAM_DATA_STRUCT - Team ", iTeam, "--------------------")
	PRINTLN("g_FMMC_STRUCT.sTeamData[iTeam].iStartingInventoryIndex: ", g_FMMC_STRUCT.sTeamData[iTeam].iStartingInventoryIndex)
	PRINTLN("g_FMMC_STRUCT.sTeamData[iTeam].iMidMissionInventoryIndex: ", g_FMMC_STRUCT.sTeamData[iTeam].iMidMissionInventoryIndex)
	PRINTLN("g_FMMC_STRUCT.sTeamData[iTeam].iMidMissionInventory2Index: ", g_FMMC_STRUCT.sTeamData[iTeam].iMidMissionInventory2Index)
	IF IS_BIT_SET(g_FMMC_STRUCT.sTeamData[iTeam].iTeamBitSet, ciTEAM_BS_SwapPlayerPed)
		PRINTLN("ciTEAM_BS_SwapPlayerPed is set")
		INT i
		FOR i = 0 TO FMMC_PLAYER_PED_MODELS_MAX-1
			PRINTLN("g_FMMC_STRUCT.sTeamData[iTeam].iPlayerPedModels[",i,"] = ", g_FMMC_STRUCT.sTeamData[iTeam].iPlayerPedModels[i])
		ENDFOR
	ENDIF
ENDPROC

PROC PRINT_INVENTORY_STRUCT(FMMC_WEAPON_INVENTORY_STRUCT sInventory)
	PRINTLN("sInventory.iAmmoDropTypeOverride: ", sInventory.iAmmoDropTypeOverride)
	PRINTLN("sInventory.iInventoryBitset: ", sInventory.iInventoryBitset)
	PRINTLN("sInventory.iRandomWeaponBitSet: ", sInventory.iRandomWeaponBitSet)
	PRINTLN("sInventory.iStartWeapon: ", sInventory.iStartWeapon)
	PRINTLN("sInventory.sParachute.iCount: ", sInventory.sParachute.iCount)
	PRINTLN("sInventory.sParachute.iPackTint: ", sInventory.sParachute.iPackTint)
	PRINTLN("sInventory.sParachute.iTint: ", sInventory.sParachute.iTint)
	PRINTLN("sInventory.eArmour: ", sInventory.eArmour)
	INT iWep
	FOR iWep = 0 TO FMMC_MAX_INVENTORY_WEAPONS-1
		IF sInventory.sWeaponStruct[iWep].wtWeapon != WEAPONTYPE_INVALID
			PRINTLN("sInventory.sWeaponStruct[",iWep,"].wtWeapon: ", sInventory.sWeaponStruct[iWep].wtWeapon)
			PRINTLN("sInventory.sWeaponStruct[",iWep,"].fDamageMod: ", sInventory.sWeaponStruct[iWep].fDamageMod)
			PRINTLN("sInventory.sWeaponStruct[",iWep,"].iAmmo: ", sInventory.sWeaponStruct[iWep].iAmmo)
			PRINTLN("sInventory.sWeaponStruct[",iWep,"].iLiveryTint: ", sInventory.sWeaponStruct[iWep].iLiveryTint)
			PRINTLN("sInventory.sWeaponStruct[",iWep,"].iTint: ", sInventory.sWeaponStruct[iWep].iTint)
		ENDIF
		
	ENDFOR
ENDPROC

PROC PRINT_VEHICLE_CUSTOM_MOD_SETTINGS(INT iVeh)
	PRINTLN("sPlacedVehicle[", iVeh, "].iCustomModSettingsIndex = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex = -1
		EXIT
	ENDIF
	
	INT iCustomMod = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex
	
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Bitset = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Bitset)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Spoiler = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Spoiler)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Bumper_F = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Bumper_F)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Bumper_R = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Bumper_R)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Skirt = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Skirt)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Exhaust = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Exhaust)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Chassis = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Chassis)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Grill = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Grill)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Bonnet = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Bonnet)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Wing_L = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Wing_L)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Wing_R = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Wing_R)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Roof = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Roof)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Engine = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Engine)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Brakes = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Brakes)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Gearbox = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Gearbox)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Horn = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Horn)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Suspension = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Suspension)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Armour = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Armour)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Wheels = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Wheels)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Rear_Wheels = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Rear_Wheels)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Interior1 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Interior1)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Interior2 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Interior2)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Interior3 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Interior3)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Interior4 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Interior4)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Interior5 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Interior5)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Seats = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Seats)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Steering = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Steering)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Plaque = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Plaque)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Trunk = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Trunk)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_EngineBay1 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_EngineBay1)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_EngineBay2 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_EngineBay2)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_EngineBay3 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_EngineBay3)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Chassis2 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Chassis2)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Chassis3 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Chassis3)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Chassis4 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Chassis4)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Chassis5 = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Chassis5)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Door_L = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Door_L)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Door_R = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Door_R)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Livery = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Livery)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_PLTHolder = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_PLTHolder)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_PlateText = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_PlateText)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Toggle_Turbo = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Toggle_Turbo)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Toggle_Tyre_Smoke = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Toggle_Tyre_Smoke)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_Toggle_Xenon_Lights = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Toggle_Xenon_Lights)
	PRINTLN("sPlacedVehicleCustomModSettings[", iCustomMod, "].iCustomMod_WheelRimColour = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_WheelRimColour)
ENDPROC

PROC PRINT_FMMC_DEBUGDATA(BOOL bLoading = TRUE,BOOL btestplay=FALSE, INT iLesRating = 0)	
	INT i, iTask, ii		
	
	PRINTSTRING("-------------------------------------------------------")PRINTNL()
	PRINTSTRING("-------------------------------------------------------")PRINTNL()
	PRINTSTRING("--                                                   --")PRINTNL()
	IF bLoading
		PRINTSTRING("--      FMMC MISSION DATA LOADED FROM THE CLOUD      --")PRINTNL()
	ELSE
		PRINTSTRING("--      FMMC MISSION DATA SENDING TO THE CLOUD       --")PRINTNL()
	ENDIF
	IF btestplay
		PRINTSTRING("--  FMMC MISSION DATA THIS IS LAUNCHED FROM TEST MISSION  --")PRINTNL()
	ENDIF
	PRINTSTRING("--           HERE ARE THE MISSION SETTINGS           --")PRINTNL()
	PRINTSTRING("--                                                   --")PRINTNL()	
	PRINTSTRING("-------------------------------------------------------")PRINTNL()
	PRINTSTRING("-------------------------------------------------------")PRINTNL()
	PRINTSTRING("contentIDforWidget   = ")PRINTSTRING(g_FMMC_STRUCT.tl31LoadedContentID)PRINTNL()
	PRINTSTRING("tl31MissionCreator   = ")PRINTSTRING(g_FMMC_STRUCT.tl63MissionCreator)PRINTNL()
	PRINTSTRING("tl23MissionCreatorID = ")PRINTSTRING(g_FMMC_STRUCT.tl23MissionCreatorID)PRINTNL()
	PRINTSTRING("tl31MissionName      = ")PRINTSTRING(g_FMMC_STRUCT.tl63MissionName)PRINTNL()
	FOR i = 0 TO (FMMC_MAX_STRAND_MISSIONS - 1)
		PRINTSTRING("tl23NextContentID    = ")PRINTSTRING(g_FMMC_STRUCT.tl23NextContentID[i])PRINTNL()
		PRINTSTRING("eBranchingTransitionType    = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT.eBranchingTransitionType[i]))PRINTNL()
	ENDFOR
	PRINTSTRING("tl31LoadedContentID  = ")PRINTSTRING(g_FMMC_STRUCT.tl31LoadedContentID)PRINTNL()
	
	PRINTSTRING("MissionDecription    = ")
	FOR i = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION - 1)
		PRINTSTRING(g_FMMC_STRUCT.tl63MissionDecription[i])
	ENDFOR
	PRINTNL()	
	PRINTSTRING("Search Tags    = ")
	FOR i = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION - 1)
		PRINTSTRING(g_FMMC_STRUCT.szTags.TextLabel[i])
	ENDFOR
	PRINTNL()		
	
	PRINTLN("[SST] * g_FMMC_STRUCT.iDLCRelease = ", g_FMMC_STRUCT.iDLCRelease)
	
	PRINTLN("[SST] * g_FMMC_STRUCT.iRuleOptionsBitSet = ", g_FMMC_STRUCT.iRuleOptionsBitSet)
	PRINTLN("[SST] * g_FMMC_STRUCT.iMissionType       = ", g_FMMC_STRUCT.iMissionType)
	PRINTLN("[SST] * g_FMMC_STRUCT.iMissionEndType    = ", g_FMMC_STRUCT.iMissionEndType)
	PRINTLN("[SST] * g_FMMC_STRUCT.iMissionSubType    = ", g_FMMC_STRUCT.iMissionSubType)
	PRINTLN("[SST] * g_FMMC_STRUCT.iRunAs       	  = ", g_FMMC_STRUCT.iRunAs)
	PRINTLN("[SST] * g_FMMC_STRUCT.iTeamBalance       = ", g_FMMC_STRUCT.iTeamBalance)
	PRINTLN("[SST] * g_FMMC_STRUCT.iMaxNumberOfTeams  = ", g_FMMC_STRUCT.iMaxNumberOfTeams)
	FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
		PRINTLN("[SST] * g_FMMC_STRUCT.iNumPlayersPerTeam[", i, "]       = ", g_FMMC_STRUCT.iNumPlayersPerTeam[i])
		PRINTLN("[SST] * g_FMMC_STRUCT.iMaxNumPlayersPerTeam[", i, "]       = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[i])
		PRINTLN("[SST] * g_FMMC_STRUCT.iVehicleBodyHealth[", i, "]       = ", g_FMMC_STRUCT.iVehicleBodyHealth[i])
	ENDFOR
	PRINTLN	   ("tl23CustomTimerString   = ", g_FMMC_STRUCT.tl23CustomTimerString )
	PRINTSTRING("iMissionSubType      	 = ")PRINTINT(g_FMMC_STRUCT.iMissionSubType)PRINTNL()
	
	PRINTSTRING("g_FMMC_STRUCT.iInteriorBS = ")PRINTINT(g_FMMC_STRUCT.iInteriorBS)PRINTNL()
	PRINTSTRING("g_FMMC_STRUCT.iInteriorBS2 = ")PRINTINT(g_FMMC_STRUCT.iInteriorBS2)PRINTNL()
	PRINTLN("g_FMMC_STRUCT.iInteriorBS3 = ", g_FMMC_STRUCT.iInteriorBS3)
	PRINTLN("g_FMMC_STRUCT.iIPLOptions = ", g_FMMC_STRUCT.iIPLOptions)
	PRINTLN("g_FMMC_STRUCT.sIPLOptions.iIslandIPL = ", g_FMMC_STRUCT.sIPLOptions.iIslandIPL)
	
	FOR i = 0 TO ciMETH_LAB_INSTANCE_MAX-1
		PRINTLN("g_FMMC_STRUCT.iMethLabInstanceWallTints[", i, "] = ", g_FMMC_STRUCT.iMethLabInstanceWallTints[i])			
	ENDFOR
	
	PRINTSTRING("vCameraPanPos        	 = ")PRINTVECTOR(g_FMMC_STRUCT.vCameraPanPos)PRINTNL()
	PRINTSTRING("vCameraPanRot        	 = ")PRINTVECTOR(g_FMMC_STRUCT.vCameraPanRot)PRINTNL()
	PRINTSTRING("vCameraOutroPos      	 = ")PRINTVECTOR(g_FMMC_STRUCT.vCameraOutroPos)PRINTNL()
	PRINTSTRING("vCameraOutroRot      	 = ")PRINTVECTOR(g_FMMC_STRUCT.vCameraOutroRot)PRINTNL()
	PRINTSTRING("iNumParticipants     	 = ")PRINTINT(g_FMMC_STRUCT.iNumParticipants)PRINTNL()
	PRINTSTRING("iMinNumParticipants  	 = ")PRINTINT(g_FMMC_STRUCT.iMinNumParticipants)PRINTNL()
	PRINTSTRING("iLaunchTimesBit      	 = ")PRINTINT(g_FMMC_STRUCT.iLaunchTimesBit)PRINTNL()	
	PRINTSTRING("iPhoto               	 = ")PRINTINT(g_FMMC_STRUCT.iPhoto)PRINTNL()
	PRINTSTRING("iMissionEndType      	 = ")PRINTINT(g_FMMC_STRUCT.iMissionEndType)PRINTNL()
	PRINTSTRING("iMissionTypeBitSet   	 = ")PRINTINT(g_FMMC_STRUCT.iMissionTypeBitSet)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSet   	 = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSet)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetTwo	 = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetTwo)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetThree = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetThree)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetFour  = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetFour)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetFive  = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetFive)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetSix   = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetSix)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetSeven = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetSeven )PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetEight = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetEight)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetNine  = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetNine)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetTen   = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetTen)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetEleven = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetEleven)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetTwelve = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetThirteen = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetFourteen = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetFifteen = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetSixteen = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetSeventeen = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetEighteen = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetNineteen = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen)PRINTNL()	
	PRINTSTRING("iOptionsMenuBitSetTwenty = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetTwentyOne = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetTwentyTwo = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetTwentyThree = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetTwentyFour = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetTwentyFive = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetTwentySix = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix)PRINTNL()
	PRINTSTRING("iOptionsMenuBitSetTwentySeven = ")PRINTINT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven)PRINTNL()
	PRINTLN("iOptionsMenuBitSetTwentyEight", g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight)
	PRINTLN("iOptionsMenuBitSetTwentyNine", g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine)
	PRINTLN("iOptionsMenuBitSetThirty", g_FMMC_STRUCT.iOptionsMenuBitSetThirty)
	
	FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
		PRINTLN("iTeamBitset[",i,"] 	= ", g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamBitset)
		PRINTLN("iTeamBitset2[",i,"] 	= ", g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamBitset2)
		PRINTLN("iTeamBitset3[",i,"] 	= ", g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamBitset3)
		PRINTLN("iTeamBitset4[",i,"] 	= ", g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamBitset4)
		FOR ii = 0 TO MAX_HEIST_GEAR_BITSETS-1
			PRINTLN("g_FMMC_STRUCT.biGearAvailableBitset[",i,"][",ii,"] = ", g_FMMC_STRUCT.biGearAvailableBitset[i][ii])
		ENDFOR
		PRINT_TEAM_DATA_STRUCT(i)
	ENDFOR	
	
	PRINTSTRING("iNumberOfTeams      	 = ")PRINTINT(g_FMMC_STRUCT.iNumberOfTeams)PRINTNL()
	PRINTSTRING("iMaxNumberOfTeams   	 = ")PRINTINT(g_FMMC_STRUCT.iMaxNumberOfTeams)PRINTNL()
	PRINTSTRING("iMinNumberOfTeams   	 = ")PRINTINT(g_FMMC_STRUCT.iMinNumberOfTeams)PRINTNL()
	PRINTSTRING("iNumberOfLives      	 = ")PRINTINT(g_FMMC_STRUCT.iNumberOfLives)PRINTNL()
	PRINTSTRING("iRaceType           	 = ")PRINTINT(g_FMMC_STRUCT.iRaceType)PRINTNL()
	PRINTSTRING("iRaceClass          	 = ")PRINTINT(g_FMMC_STRUCT.iRaceClass)PRINTNL()
	PRINTSTRING("iRaceClassBitSet    	 = ")PRINTINT(g_FMMC_STRUCT.iRaceClassBitSet)PRINTNL()
	PRINTSTRING("iVehicleModel       	 = ")PRINTINT(g_FMMC_STRUCT.iVehicleModel)PRINTNL()   	
	PRINTSTRING("iStartingGrid       	 = ")PRINTINT(g_FMMC_STRUCT.iStartingGrid)PRINTNL()   	
	PRINTSTRING("iRoundTime          	 = ")PRINTINT(g_FMMC_STRUCT.iRoundTime)PRINTNL()
	PRINTSTRING("iTargetScore        	 = ")PRINTINT(g_FMMC_STRUCT.iTargetScore)PRINTNL()
	PRINTSTRING("iNumLaps            	 = ")PRINTINT(g_FMMC_STRUCT.iNumLaps)PRINTNL()
	PRINTSTRING("iBlips              	 = ")PRINTINT(g_FMMC_STRUCT.iBlips)PRINTNL()
	PRINTSTRING("iTags               	 = ")PRINTINT(g_FMMC_STRUCT.iTags)PRINTNL()
	PRINTSTRING("iVoice              	 = ")PRINTINT(g_FMMC_STRUCT.iVoice)PRINTNL()
	PRINTSTRING("iContactCharEnum    	 = ")PRINTINT(g_FMMC_STRUCT.iContactCharEnum)PRINTNL()
	PRINTSTRING("iXPReward           	 = ")PRINTINT(g_FMMC_STRUCT.iXPReward)PRINTNL()
	PRINTSTRING("iCashReward         	 = ")PRINTINT(g_FMMC_STRUCT.iCashReward)PRINTNL()
	PRINTSTRING("iRespawnTime        	 = ")PRINTINT(g_FMMC_STRUCT.iRespawnTime)PRINTNL()
	PRINTSTRING("iMyTeam             	 = ")PRINTINT(g_FMMC_STRUCT.iMyTeam)PRINTNL()		
	PRINTSTRING("iMusic              	 = ")PRINTINT(g_FMMC_STRUCT.iMusic)PRINTNL()		
	PRINTSTRING("iAudioScore         	 = ")PRINTINT(g_FMMC_STRUCT.iAudioScore)PRINTNL()	
	PRINTSTRING("iNewAudioScore          = ")PRINTINT(g_FMMC_STRUCT.iNewAudioScore)PRINTNL()
	PRINTSTRING("iEndCutscene        	 = ")PRINTINT(g_FMMC_STRUCT.iEndCutscene)PRINTNL()
	
	PRINTSTRING("iAggroIndexBS_iStartMissionWithAggro        	 = ")PRINTINT(g_FMMC_STRUCT.iAggroIndexBS_iStartMissionWithAggro)PRINTNL()	
	FOR i = 0 TO ciMISSION_AGGRO_INDEX_PREREQ_TRIGGERS_MAX-1
		FOR ii = 0 TO ciPREREQ_Bitsets-1
			PRINTLN("iAggroIndexBS_ApplyAggro_PrereqIndexBS[", i, "][", ii, "]        		 = ", g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_PrereqIndexBS[i][ii])					
		ENDFOR
		PRINTLN("iAggroIndexBS_ApplyAggro_PrereqAggroBS[", i, "]        						 = ", g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_PrereqAggroBS[i])		
		PRINTLN("iAggroIndexBS_ApplyAggro_ModBS[", i, "]        								 = ", g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_ModBS[i])		
	ENDFOR
	
	PRINTLN("fStickyBombAOE = ", g_FMMC_STRUCT.fStickyBombAOE)
	
	INT i2
	FOR i2 = 0 TO FMMC_MAX_RULE_VECTOR_WARPS-1
		PRINTLN("sWarpLocationSettings.vPosition[", i2, "]: ", g_FMMC_STRUCT.sWarpLocationSettings_EndCutscene.vPosition[i2])
		PRINTLN("sWarpLocationSettings.fHeading[", i2, "]: ", g_FMMC_STRUCT.sWarpLocationSettings_EndCutscene.fHeading[i2])		
	ENDFOR
		
	FOR i2 = 0 TO FMMC_MAX_RULE_VECTOR_WARPS-1
		PRINTLN("sWarpLocationSettings_EndCutscene.iCheckPointPositionIDStart		= ", g_FMMC_STRUCT.sWarpLocationSettings_EndCutscene.iCheckPointPositionIDStart)
		PRINTLN("sWarpLocationSettings_EndCutscene.iWarpLocationBS    	 	 		= ", g_FMMC_STRUCT.sWarpLocationSettings_EndCutscene.iWarpLocationBS)
		PRINTLN("sWarpLocationSettings_EndCutscene.vPosition[", i2, "]    	 		= ", g_FMMC_STRUCT.sWarpLocationSettings_EndCutscene.vPosition[i2])
		PRINTLN("sWarpLocationSettings_EndCutscene.fHeading[", i2, "]    	 		= ", g_FMMC_STRUCT.sWarpLocationSettings_EndCutscene.fHeading[i2])
	ENDFOR
	
	PRINTLN("iEndCutsceneSpawnGroup: ", g_FMMC_STRUCT.iEndCutsceneSpawnGroup)
	FOR i2 = 0 TO FMMC_MAX_RULE_VECTOR_WARPS-1		
		PRINTLN("iEndCutsceneSpawnSubGroupBS[", i2, "]: ", g_FMMC_STRUCT.iEndCutsceneSpawnSubGroupBS[i2])		
	ENDFOR

	PRINTSTRING("iSpeedRaceCheckpointTimer = ")PRINTINT(g_FMMC_STRUCT.iSpeedRaceCheckpointTimer)PRINTNL()
	PRINTSTRING("iContinuityBitset = ")PRINTINT(g_FMMC_STRUCT.iContinuityBitset)PRINTNL()
	PRINTSTRING("eContinuitySpecialPedType = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT.eContinuitySpecialPedType))PRINTNL()
	PRINTSTRING("iContinuitySpawnGroupBitset = ")PRINTINT(g_FMMC_STRUCT.iContinuitySpawnGroupBitset)PRINTNL()
	FOR i = 0 TO (FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS - 1)
		PRINTLN("iContinuitySpawnGroupCondition[",i,"] 	= ", g_FMMC_STRUCT.iContinuitySpawnGroupCondition[i])
		PRINTLN("iContinuitySpawnGroupContinuityId[",i,"] 	= ", g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[i])
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_CUSTOM_FAIL_NAMES - 1)
		PRINTLN("tlCustomPedName[",i,"] 	= ", g_FMMC_STRUCT_ENTITIES.tlCustomPedName[i])
		PRINTLN("tlCustomVehName[",i,"] 	= ", g_FMMC_STRUCT_ENTITIES.tlCustomVehName[i])
		PRINTLN("tlCustomObjName[",i,"] 	= ", g_FMMC_STRUCT_ENTITIES.tlCustomObjName[i])
	ENDFOR
	FOR i = 0 TO (ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES - 1)
		PRINTLN("tl63ObjectiveText_SpawnGroupOverride[",i,"] = ", g_FMMC_STRUCT.tl63ObjectiveText_SpawnGroupOverride[i])
	ENDFOR
	
	FOR i = 0 TO ciYACHT_MAX_YACHTS-1		
		PRINTLN("sMissionYachtInfo[",i,"].tlYachtName 				= ", g_FMMC_STRUCT.sMissionYachtInfo[i].tlYachtName)
		PRINTLN("sMissionYachtInfo[",i,"].iMYacht_ModelIndex 		= ", g_FMMC_STRUCT.sMissionYachtInfo[i].iMYacht_ModelIndex)
		PRINTLN("sMissionYachtInfo[",i,"].iMYacht_Bitset 			= ", g_FMMC_STRUCT.sMissionYachtInfo[i].iMYacht_Bitset)
		PRINTLN("sMissionYachtInfo[",i,"].iMYacht_AreaIndex	 		= ", g_FMMC_STRUCT.sMissionYachtInfo[i].iMYacht_AreaIndex)
		PRINTLN("sMissionYachtInfo[",i,"].iMYacht_PositionIndex 	= ", g_FMMC_STRUCT.sMissionYachtInfo[i].iMYacht_PositionIndex)
		PRINTLN("sMissionYachtInfo[",i,"].iMYacht_ColourIndex 		= ", g_FMMC_STRUCT.sMissionYachtInfo[i].iMYacht_ColourIndex)
		PRINTLN("sMissionYachtInfo[",i,"].iMYacht_LightingIndex 	= ", g_FMMC_STRUCT.sMissionYachtInfo[i].iMYacht_LightingIndex)
		PRINTLN("sMissionYachtInfo[",i,"].iMYacht_FittingsIndex 	= ", g_FMMC_STRUCT.sMissionYachtInfo[i].iMYacht_FittingsIndex)
		PRINTLN("sMissionYachtInfo[",i,"].iMYacht_FlagIndex			= ", g_FMMC_STRUCT.sMissionYachtInfo[i].iMYacht_FlagIndex)
	ENDFOR
	
	PRINTLN("[AltVarsSystemSpam] - sPoolVars Start ------------------------------------------------------------------------")
	PRINTLN("[AltVarsSystemSpam] - sPoolVars.eConfig: 			= ", ENUM_TO_INT(g_FMMC_STRUCT.sPoolVars.eConfig))	
	FOR i = 0 TO ciMAX_FMMC_ALT_VAR_INTS - 1
		PRINTLN("[AltVarsSystemSpam] - sPoolVars.iInt[", i, "]: 			= ", g_FMMC_STRUCT.sPoolVars.iInt[i])
	ENDFOR
	PRINTLN("[AltVarsSystemSpam] - sPoolVars End --------------------------------------------------------------------------")
	
	FOR i = 0 TO FMMC_MAX_CUSTOM_HUDS - 1
		PRINTLN("sCustomHUDs[", i, "].iCriteria = ", g_FMMC_STRUCT.sCustomHUDs[i].iCriteria)
		FOR i2 = 0 TO ciPREREQ_Bitsets - 1
			PRINTLN("sCustomHUDs[", i, "].iPreReqs[", i2, "] = ", g_FMMC_STRUCT.sCustomHUDs[i].iPreReqs[i2])
		ENDFOR
		PRINTLN("sCustomHUDs[", i, "].iCustomStringTextLabel = ", g_FMMC_STRUCT.sCustomHUDs[i].iCustomStringTextLabel)
		PRINTLN("sCustomHUDs[", i, "].iColour = ", g_FMMC_STRUCT.sCustomHUDs[i].iColour)
		PRINTLN("sCustomHUDs[", i, "].iType = ", g_FMMC_STRUCT.sCustomHUDs[i].iType)
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_GANG_CHASE_UNIT_CONFIGS - 1
		PRINTLN("sGangChaseUnitConfigs[", i, "].mnPedModel = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].mnPedModel)
		PRINTLN("sGangChaseUnitConfigs[", i, "].mnVehicleModel = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].mnVehicleModel)
		PRINTLN("sGangChaseUnitConfigs[", i, "].wtDefaultWeapon = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].wtDefaultWeapon)
		PRINTLN("sGangChaseUnitConfigs[", i, "].wtRandomWeapon1 = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].wtRandomWeapon1)
		PRINTLN("sGangChaseUnitConfigs[", i, "].wtRandomWeapon2 = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].wtRandomWeapon2)
		PRINTLN("sGangChaseUnitConfigs[", i, "].iMaxPeds = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].iMaxPeds)
		PRINTLN("sGangChaseUnitConfigs[", i, "].eType = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].eType)
		PRINTLN("sGangChaseUnitConfigs[", i, "].fPedArmour = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].fPedArmour)
		PRINTLN("sGangChaseUnitConfigs[", i, "].bUseAlternateComponentsForPedOutfit = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].bUseAlternateComponentsForPedOutfit)
		PRINTLN("sGangChaseUnitConfigs[", i, "].iOverrideVehicleColour1 = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].iOverrideVehicleColour1)
		PRINTLN("sGangChaseUnitConfigs[", i, "].iOverrideVehicleColour2 = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].iOverrideVehicleColour2)
		PRINTLN("sGangChaseUnitConfigs[", i, "].iVehicleModPresetToUse = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].iVehicleModPresetToUse)
		PRINTLN("sGangChaseUnitConfigs[", i, "].iVehicleLiveryToUse = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].iVehicleLiveryToUse)
		PRINTLN("sGangChaseUnitConfigs[", i, "].bWeaponsDisrupted = ", g_FMMC_STRUCT.sGangChaseUnitConfigs[i].bWeaponsDisrupted)
	ENDFOR
	
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitMinObjectiveDistance = ", g_FMMC_STRUCT.fGangChaseUnitMinObjectiveDistance)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseAirSpawnMinZOffset = ", g_FMMC_STRUCT.fGangChaseAirSpawnMinZOffset)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitSpawnPointMaxOffset = ", g_FMMC_STRUCT.fGangChaseUnitSpawnPointMaxOffset)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseSeaVisibleDistance = ", g_FMMC_STRUCT.fGangChaseSeaVisibleDistance)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitSpawnDistForwardExtra = ", g_FMMC_STRUCT.fGangChaseUnitSpawnDistForwardExtra)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitSpawnDistSea = ", g_FMMC_STRUCT.fGangChaseUnitSpawnDistSea)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitSpawnDistAir = ", g_FMMC_STRUCT.fGangChaseUnitSpawnDistAir)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitSpawnDistOnFoot = ", g_FMMC_STRUCT.fGangChaseUnitSpawnDistOnFoot)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitSpawnDistLand = ", g_FMMC_STRUCT.fGangChaseUnitSpawnDistLand)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitMaxDistance = ", g_FMMC_STRUCT.fGangChaseUnitMaxDistance)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsLand = ", g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsLand)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsAir = ", g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsAir)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsSea = ", g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsSea)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsOnFoot = ", g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsOnFoot)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.iGangChaseUnitFleeDistance = ", g_FMMC_STRUCT.iGangChaseUnitFleeDistance)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.iGangChaseUnitExtraFleeDistance = ", g_FMMC_STRUCT.iGangChaseUnitExtraFleeDistance)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistLand = ", g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistLand)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistAir = ", g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistAir)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistSea = ", g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistSea)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistOnFoot = ", g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistOnFoot)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistLand = ", g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistLand)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistAir = ", g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistAir)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistSea = ", g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistSea)
	PRINTLN("[GANG_CHASE] g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistOnFoot = ", g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistOnFoot)
	
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		FOR i = 0 TO MAX_PLAYERS_PER_TEAM - 1
			PRINTLN("[CACHE VEHICLE] g_FMMC_STRUCT.iCachedVehicle[", iTeam, "][", i, "] = ", g_FMMC_STRUCT.iCachedVehicle[iTeam][i])
		ENDFOR
	ENDFOR
	
	PRINTLN("g_FMMC_STRUCT.iLDB_ShowEndTimeForTeam = ", g_FMMC_STRUCT.iLDB_ShowEndTimeForTeam)
	
	FOR i = 0 TO FMMC_MAX_AUDIO_SCENES-1
		PRINTLN("g_FMMC_STRUCT.iAudioScene[", i, "] 			= ", g_FMMC_STRUCT.iAudioScene[i])
		PRINTLN("g_FMMC_STRUCT.iAudioSceneTeam[", i, "] 		= ", g_FMMC_STRUCT.iAudioSceneTeam[i])
		PRINTLN("g_FMMC_STRUCT.iAudioSceneStartRule[", i, "] 	= ", g_FMMC_STRUCT.iAudioSceneStartRule[i])
		PRINTLN("g_FMMC_STRUCT.iAudioSceneEndRule[", i, "] 		= ", g_FMMC_STRUCT.iAudioSceneEndRule[i])
	ENDFOR
	
	FOR i = 0 TO ENUM_TO_INT(PA_MAX) - 1
		PRINTLN("sPlayerAbilities.sAbilities[", i, "].iAbilityBitset = ", g_FMMC_STRUCT.sPlayerAbilities.sAbilities[i].iAbilityBitset)
		PRINTLN("sPlayerAbilities.sAbilities[", i, "].iActiveDuration = ", g_FMMC_STRUCT.sPlayerAbilities.sAbilities[i].iActiveDuration)
		PRINTLN("sPlayerAbilities.sAbilities[", i, "].iCooldownDuration = ", g_FMMC_STRUCT.sPlayerAbilities.sAbilities[i].iCooldownDuration)
		PRINTLN("sPlayerAbilities.sAbilities[", i, "].iNumUses = ", g_FMMC_STRUCT.sPlayerAbilities.sAbilities[i].iNumUses)
		PRINTLN("sPlayerAbilities.sAbilities[", i, "].iRequiredPrerequisite = ", g_FMMC_STRUCT.sPlayerAbilities.sAbilities[i].iRequiredPrerequisite)
	ENDFOR
	
	INT iExtra
	FOR ii = 0 TO FMMC_MAX_WARP_PORTALS-1
		PRINTLN("sWarpPortals[",ii,"].vStartCoord = ", g_FMMC_STRUCT.sWarpPortals[ii].vStartCoord)
		PRINTLN("sWarpPortals[",ii,"].fStartHead = ", g_FMMC_STRUCT.sWarpPortals[ii].fStartHead)		
		FOR iExtra = 0 TO  FMMC_MAX_WARP_PORTALS_EXTRA_POSITIONS-1
			PRINTLN("sWarpPortals[",ii,"].vExtraStartCoord[",iExtra,"] = ", g_FMMC_STRUCT.sWarpPortals[ii].vExtraStartCoord[iExtra])
			PRINTLN("sWarpPortals[",ii,"].fExtraStartHead[",iExtra,"] = ", g_FMMC_STRUCT.sWarpPortals[ii].fExtraStartHead[iExtra])
		ENDFOR	
		PRINTLN("sWarpPortals[",ii,"].fStartRange = ", g_FMMC_STRUCT.sWarpPortals[ii].fStartRange)
		PRINTLN("sWarpPortals[",ii,"].iWarpPortalBS = ", g_FMMC_STRUCT.sWarpPortals[ii].iWarpPortalBS)		
		PRINTLN("sWarpPortals[",ii,"].iWarpPortalBS2 = ", g_FMMC_STRUCT.sWarpPortals[ii].iWarpPortalBS2)		
		PRINTLN("sWarpPortals[",ii,"].iAutoTriggerAfterTime = ", g_FMMC_STRUCT.sWarpPortals[ii].iAutoTriggerAfterTime)
		PRINTLN("sWarpPortals[",ii,"].fMoveForwardXMeters = ", g_FMMC_STRUCT.sWarpPortals[ii].fMoveForwardXMeters)
		PRINTLN("sWarpPortals[",ii,"].iEntryAnimType = ", g_FMMC_STRUCT.sWarpPortals[ii].iEntryAnimType)
		PRINTLN("sWarpPortals[",ii,"].vEntryAnimCoord = ", g_FMMC_STRUCT.sWarpPortals[ii].vEntryAnimCoord)
		PRINTLN("sWarpPortals[",ii,"].fEntryAnimHead = ", g_FMMC_STRUCT.sWarpPortals[ii].fEntryAnimHead)
		PRINTLN("sWarpPortals[",ii,"].vCamAnimCoord = ", g_FMMC_STRUCT.sWarpPortals[ii].vCamAnimCoord)
		PRINTLN("sWarpPortals[",ii,"].vCamAnimRot = ", g_FMMC_STRUCT.sWarpPortals[ii].vCamAnimRot)		
		INT iTeamII
		FOR iTeamII = 0 TO FMMC_MAX_TEAMS-1
			PRINTLN("sWarpPortals[",ii,"].iActiveOnRule[",iTeamII,"] = ", g_FMMC_STRUCT.sWarpPortals[ii].iActiveOnRule[iTeamII])
			PRINTLN("sWarpPortals[",ii,"].iEndOnRule[",iTeamII,"] = ", g_FMMC_STRUCT.sWarpPortals[ii].iEndOnRule[iTeamII])
		ENDFOR
		PRINTLN("sWarpPortals[",ii,"].fCamAnimFOV = ", g_FMMC_STRUCT.sWarpPortals[ii].fCamAnimFOV)
		
		FOR i = 0 TO ciAltVarsBS_MAX-1
			PRINTLN("[AltVarsSystemSpam] - sWarpPortals[", ii, "].iAltVarsBitsetWarpPortal[", i, "] = ", g_FMMC_STRUCT.sWarpPortals[ii].iAltVarsBitsetWarpPortal[i])
		ENDFOR

		FOR i = 0 TO FMMC_MAX_WARP_PORTAL_CUSTOM_PROMPT_LABEL - 1
			PRINTLN("sWarpPortals[",ii,"].iCustomLabelPrompt[",i,"] = ", g_FMMC_STRUCT.sWarpPortals[ii].iCustomPromptLabel[i])
		ENDFOR
		
		PRINTLN("sWarpPortals[",ii,"].iAttachToEntityType = ", g_FMMC_STRUCT.sWarpPortals[ii].iAttachToEntityType)
		PRINTLN("sWarpPortals[",ii,"].iAttachToEntityID = ", g_FMMC_STRUCT.sWarpPortals[ii].iAttachToEntityID)
		PRINTLN("sWarpPortals[",ii,"].iTriggerTypeUsed = ", g_FMMC_STRUCT.sWarpPortals[ii].iTriggerTypeUsed)
		PRINTLN("sWarpPortals[",ii,"].iColourType = ", g_FMMC_STRUCT.sWarpPortals[ii].iColourType)
		PRINTLN("sWarpPortals[",ii,"].fVisualRangeOverride = ", g_FMMC_STRUCT.sWarpPortals[ii].fVisualRangeOverride)
		
		PRINTLN("sWarpPortals[",ii,"].sVehicleSwap.mnVehicleModelSwap = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT.sWarpPortals[ii].sVehicleSwap.mnVehicleModelSwap))
		PRINTLN("sWarpPortals[",ii,"].sVehicleSwap.iModPresetIndex = ", g_FMMC_STRUCT.sWarpPortals[ii].sVehicleSwap.iModPresetIndex)
		PRINTLN("sWarpPortals[",ii,"].sVehicleSwap.iVehicleAirCounterCooldown = ", g_FMMC_STRUCT.sWarpPortals[ii].sVehicleSwap.iVehicleAirCounterCooldown)		
	ENDFOR
	
	PRINTLN("g_FMMC_STRUCT.iNumPlacedPTFX 		= ", g_FMMC_STRUCT.iNumPlacedPTFX)
	FOR ii = 0 TO FMMC_MAX_PLACED_PTFX-1
		PRINTLN("sPlacedPTFX[",ii,"].tl63AssetName 		= ", g_FMMC_STRUCT.sPlacedPTFX[ii].tl63AssetName)
		PRINTLN("sPlacedPTFX[",ii,"].tl63EffectName		= ", g_FMMC_STRUCT.sPlacedPTFX[ii].tl63EffectName)
		PRINTLN("sPlacedPTFX[",ii,"].tl15EvolutionName	= ", g_FMMC_STRUCT.sPlacedPTFX[ii].tl15EvolutionName)
		PRINTLN("sPlacedPTFX[",ii,"].vStartCoord 		= ", g_FMMC_STRUCT.sPlacedPTFX[ii].vStartCoord)
		PRINTLN("sPlacedPTFX[",ii,"].vStartRot 			= ", g_FMMC_STRUCT.sPlacedPTFX[ii].vStartRot)
		PRINTLN("sPlacedPTFX[",ii,"].iPTFXBS 			= ", g_FMMC_STRUCT.sPlacedPTFX[ii].iPTFXBS)
		PRINTLN("sPlacedPTFX[",ii,"].iTeam				= ", g_FMMC_STRUCT.sPlacedPTFX[ii].iTeam)
		PRINTLN("sPlacedPTFX[",ii,"].iRuleFrom 			= ", g_FMMC_STRUCT.sPlacedPTFX[ii].iRuleFrom)
		PRINTLN("sPlacedPTFX[",ii,"].iRuleTo 			= ", g_FMMC_STRUCT.sPlacedPTFX[ii].iRuleTo)
		PRINTLN("sPlacedPTFX[",ii,"].iTimeToPlayFor 	= ", g_FMMC_STRUCT.sPlacedPTFX[ii].iTimeToPlayFor)
		PRINTLN("sPlacedPTFX[",ii,"].fScale 			= ", g_FMMC_STRUCT.sPlacedPTFX[ii].fScale)
		PRINTLN("[MissionVariation]sPlacedPTFX[",ii,"].iMCVarConfig_PreReqMissionPointer 			= ", g_FMMC_STRUCT.sPlacedPTFX[ii].iMCVarConfig_PreReqMissionPointer)		
		FOR i = 0 TO ciAltVarsBS_MAX-1
			PRINTLN("[AltVarsSystemSpam] - sPlacedPTFX[", ii, "].iAltVarsBitsetPTFX[", i, "] = ", g_FMMC_STRUCT.sPlacedPTFX[ii].iAltVarsBitsetPTFX[i])
		ENDFOR
	ENDFOR	
		
	PRINTLN("g_FMMC_STRUCT_ENTITIES.iNumberOfPlacedMarkers 		= ", g_FMMC_STRUCT_ENTITIES.iNumberOfPlacedMarkers)
	FOR i = 0 TO FMMC_MAX_PLACED_MARKERS-1 // deliberately using const
		PRINTLN("sPlacedMarker[",i,"].vPosition 				= ", g_FMMC_STRUCT_ENTITIES.sPlacedMarker[i].vPosition)
		PRINTLN("sPlacedMarker[",i,"].eType 					= ", g_FMMC_STRUCT_ENTITIES.sPlacedMarker[i].eType)
		PRINTLN("sPlacedMarker[",i,"].fScale 					= ", g_FMMC_STRUCT_ENTITIES.sPlacedMarker[i].fScale)
		PRINTLN("sPlacedMarker[",i,"].fSpeed 					= ", g_FMMC_STRUCT_ENTITIES.sPlacedMarker[i].fSpeed)
		PRINTLN("sPlacedMarker[",i,"].iColour 					= ", g_FMMC_STRUCT_ENTITIES.sPlacedMarker[i].iColour)
		PRINTLN("sPlacedMarker[",i,"].fAlpha 					= ", g_FMMC_STRUCT_ENTITIES.sPlacedMarker[i].fAlpha)
		PRINTLN("sPlacedMarker[",i,"].iHideOnRulesBS 			= ", g_FMMC_STRUCT_ENTITIES.sPlacedMarker[i].iHideOnRulesBS)
		PRINTLN("sPlacedMarker[",i,"].iHideOnRulesTeam 			= ", g_FMMC_STRUCT_ENTITIES.sPlacedMarker[i].iHideOnRulesTeam)
		
		FOR ii = 0 TO ciPREREQ_Bitsets-1 
			PRINTLN("sPlacedMarker[",i,"].iShowOnPrereqSetBS[",ii,"] 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedMarker[i].iShowOnPrereqSetBS[ii])
			PRINTLN("sPlacedMarker[",i,"].iHideOnPrereqSetBS[",ii,"] 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedMarker[i].iHideOnPrereqSetBS[ii])
		ENDFOR
	ENDFOR
	
	PRINTLN("[MissionVariation] - Printing out startup globals ----------------------------------------------------------------")
	PRINTLN("sMissionVariation.iVarBSOne: ", g_FMMC_STRUCT.sMissionVariation.iVarBSOne)
	PRINTLN("sMissionVariation.eMCVarConfigEnum: ", ENUM_TO_INT(g_FMMC_STRUCT.sMissionVariation.eMCVarConfigEnum))
	INT iVar = 0
	INT iVar2 = 0 
	FOR iVar = 0 TO FMMC_MAX_MC_VAR_SIZE_ENEMY_HEALTH-1
		PRINTLN("sMissionVariation.iVarEnemyHealthPercent[",iVar,"]: ", g_FMMC_STRUCT.sMissionVariation.iVarEnemyHealthPercent[iVar])
	ENDFOR
	FOR iVar = 0 TO FMMC_MAX_MC_VAR_SIZE_ENEMY_INVENTORY-1
		PRINTLN("sMissionVariation.iVarEnemyInventoryIndex[",iVar,"]: ", g_FMMC_STRUCT.sMissionVariation.iVarEnemyInventoryIndex[iVar])
	ENDFOR
	FOR iVar = 0 TO FMMC_MAX_MC_VAR_SIZE_VEHICLE_MODEL_SWAP-1
		PRINTLN("sMissionVariation.iVarVehicleModelSwapIndex[",iVar,"]: ", g_FMMC_STRUCT.sMissionVariation.iVarVehicleModelSwapIndex[iVar])
	ENDFOR		
	FOR iVar = 0 TO FMMC_MAX_MC_VAR_SIZE_SPAWN_GROUP_FORCING_OPTIONS-1
		FOR iVar2 = 0 TO FMMC_MAX_MC_VAR_SIZE_SPAWN_GROUP_FORCING-1
			PRINTLN("sMissionVariation.iVarMissionSpawnGroupForceIndex[",iVar,"][",iVar2,"]: ", g_FMMC_STRUCT.sMissionVariation.iVarMissionSpawnGroupForceIndex[iVar][iVar2]+1) // regular spawn groups creator option start at 1 not zero. It's broken...
		ENDFOR
	ENDFOR
	FOR iVar = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
		PRINTLN("sMissionVariation.eVarMissionTimerTypeEnum[",iVar,"]: ", g_FMMC_STRUCT.sMissionVariation.eVarMissionTimerTypeEnum[iVar])
	ENDFOR
	FOR iVar = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
		PRINTLN("sMissionVariation.iVarMissionTimerRuleFrom[",iVar,"]: ", g_FMMC_STRUCT.sMissionVariation.iVarMissionTimerRuleFrom[iVar])
	ENDFOR	
	FOR iVar = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
		PRINTLN("sMissionVariation.iVarMissionTimerRuleTo[",iVar,"]: ", g_FMMC_STRUCT.sMissionVariation.iVarMissionTimerRuleTo[iVar])		
	ENDFOR
	FOR iVar = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
		PRINTLN("sMissionVariation.iVarMissionTimerZoneIndex[",iVar,"]: ", g_FMMC_STRUCT.sMissionVariation.iVarMissionTimerZoneIndex[iVar])		
	ENDFOR	
	FOR iVar = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
		FOR iVar2 = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE_SELECTION-1
			PRINTLN("sMissionVariation.iVarMissionTimerModifier[",iVar,"][",iVar2,"]: ", g_FMMC_STRUCT.sMissionVariation.iVarMissionTimerModifier[iVar][iVar2])
		ENDFOR
	ENDFOR
	FOR iVar = 0 TO FMMC_MAX_MC_VAR_SIZE_OBJECT_MODEL_SWAP-1
		IF g_FMMC_STRUCT.sMissionVariation.sObjectSwap[iVar].mnVarObjectSwap = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("g_FMMC_STRUCT.sMissionVariation.sObjectSwap[",iVar,"].mnVarObjectSwap: DUMMY_MODEL_FOR_SCRIPT")
		ELSE
			PRINTLN("g_FMMC_STRUCT.sMissionVariation.sObjectSwap[",iVar,"].mnVarObjectSwap: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT.sMissionVariation.sObjectSwap[iVar].mnVarObjectSwap))
		ENDIF
		PRINTLN("g_FMMC_STRUCT.sMissionVariation.sObjectSwap[",iVar,"].iMinigameType: ", g_FMMC_STRUCT.sMissionVariation.sObjectSwap[iVar].iMinigameType)
		PRINTLN("g_FMMC_STRUCT.sMissionVariation.sObjectSwap[",iVar,"].iMinigameSubType: ", g_FMMC_STRUCT.sMissionVariation.sObjectSwap[iVar].iMinigameSubType)
	ENDFOR
	FOR iVar = 0 TO FMMC_MAX_MC_VAR_SIZE_END_CUTSCENE_OVERRIDE-1
		PRINTLN("sMissionVariation.iVarMissionEndCutsceneOverride[",iVar,"]: ", g_FMMC_STRUCT.sMissionVariation.iVarMissionEndCutsceneOverride[iVar])		
	ENDFOR	
	
	PRINTSTRING("----------Rewards / Elite Bonuses----------")PRINTNL()
	PRINTSTRING("iHeistRewardBonusBitset		= ")PRINTINT(g_FMMC_STRUCT.iHeistRewardBonusBitset)PRINTNL()
	PRINTSTRING("iHeistRewardMMTimerTeam		= ")PRINTINT(g_FMMC_STRUCT.iHeistRewardMMTimerTeam)PRINTNL()
	PRINTSTRING("iHeistRewardMMTimerStartRule	= ")PRINTINT(g_FMMC_STRUCT.iHeistRewardMMTimerStartRule)PRINTNL()
	PRINTSTRING("iHeistRewardMMTimerEndRule		= ")PRINTINT(g_FMMC_STRUCT.iHeistRewardMMTimerEndRule)PRINTNL()
	PRINTSTRING("iHeistRewardMMTimerThreshold	= ")PRINTINT(g_FMMC_STRUCT.iHeistRewardMMTimerThreshold)PRINTNL()
	PRINTSTRING("----------Densitys and Time----------")PRINTNL()
	PRINTSTRING("iPolice               = ")PRINTINT(g_FMMC_STRUCT.iPolice)PRINTNL()
	PRINTSTRING("iPedDensity           = ")PRINTINT(g_FMMC_STRUCT.iPedDensity)PRINTNL()		
	PRINTSTRING("iVehicleDensity       = ")PRINTINT(g_FMMC_STRUCT.iVehicleDensity)PRINTNL()		
	PRINTSTRING("iTimeOfDay            = ")PRINTINT(g_FMMC_STRUCT.iTimeOfDay)PRINTNL()		
	PRINTSTRING("iTraffic              = ")PRINTINT(g_FMMC_STRUCT.iTraffic)PRINTNL()	
	PRINTSTRING("------------------------POST MISSION SPAWN LOCATION DATA---------------------------")PRINTNL()
	PRINTLN("[PED CUT] *******************************")
	
	
	PRINTSTRING("----------END MOCAP DATA----------")PRINTNL()
		
	PRINTLN("[PED CUT] * (sEndMocapSceneData) g_FMMC_STRUCT.iEndCutscene = ", g_FMMC_STRUCT.iEndCutscene)
	PRINTLN("[PED CUT] * sEndMocapSceneData.vCutscenePosition = ", g_FMMC_STRUCT.sEndMocapSceneData.vCutscenePosition)
	PRINTLN("[PED CUT] * sEndMocapSceneData.vCutsceneRotation = ", g_FMMC_STRUCT.sEndMocapSceneData.vCutsceneRotation)
	
	PRINTLN("[PED CUT] * g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneWarpRange = ", g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneWarpRange)
	PRINTLN("[PED CUT] * g_FMMC_STRUCT.sEndMocapSceneData.iCutscenePlayerInclusionRange = ", g_FMMC_STRUCT.sEndMocapSceneData.iCutscenePlayerInclusionRange)
	
	PRINTLN("[PED CUT] * sEndMocapSceneData.iCutsceneBitSet = ", g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet)
	PRINTLN("[PED CUT] * sEndMocapSceneData.iCutsceneBitSet2 = ", g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2)
	PRINTLN("[PED CUT] * sEndMocapSceneData.iCutsceneBitSet3 = ", g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet3)		
	PRINTLN("[PED CUT] * sEndMocapSceneData.iCutsceneBitSet4 = ", g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet4)
	PRINTLN("[PED CUT] * sEndMocapSceneData.iCutsceneMusicMood 	= 	"  , g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneMusicMood)
	PRINTLN("[PED CUT] * sEndMocapSceneData.fEndCutsceneTimeCycleFade 	= 	"  , g_FMMC_STRUCT.sEndMocapSceneData.fEndCutsceneTimeCycleFade)
		
	FOR i = 0 TO FMMC_MAX_CUTSCENE_HIDE_ENTITIES - 1
		PRINTLN("[PED CUT] * sEndMocapSceneData.tl31_HideCutsceneHandles[", i, "] 	= 	"  , g_FMMC_STRUCT.sEndMocapSceneData.tl31_HideCutsceneHandles[i])
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES - 1
		PRINTLN("g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[", i, "].iIndex = ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex)
		PRINTLN("g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[", i, "].iType = ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType)
		PRINTLN("g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[", i, "].tlCutsceneHandle = ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle)
	ENDFOR
				
	PRINTSTRING("--------------------------------------")PRINTNL()
	
	FOR i = 0 TO (FMMC_GET_MAX_SCRIPTED_CUTSCENES() -1)
		PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].iCutsceneBitset = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].iCutsceneBitset)		
		PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].iCutsceneBitSet2 = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].iCutsceneBitSet2)
		PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].iCutsceneBitSet3 = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].iCutsceneBitSet3)
		PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].iCutsceneBitSet4 = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].iCutsceneBitSet4)
		
		PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].iCutsceneWarpRange = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].iCutsceneWarpRange)
		PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].iCutscenePlayerInclusionRange = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].iCutscenePlayerInclusionRange)
		
		FOR ii = 0 TO FMMC_MAX_TEAMS-1
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].sCutsceneEntityWarpData.iEntityType[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].sCutsceneEntityWarpData.iEntityType[ii])
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].sCutsceneEntityWarpData.iEntityIndex[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].sCutsceneEntityWarpData.iEntityIndex[ii])
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].sCutsceneEntityWarpData.vWarpPos[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].sCutsceneEntityWarpData.vWarpPos[ii])
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].sCutsceneEntityWarpData.fWarpHead[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].sCutsceneEntityWarpData.fWarpHead[ii])
		ENDFOR
		
		PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].eExternalCutsceneSequence = ", ENUM_TO_INT(g_FMMC_STRUCT.sScriptedCutsceneData[i].eExternalCutsceneSequence))
		
		FOR ii = 0 TO FMMC_MAX_TEAMS-1
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].vPlayerStartPos[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].vPlayerStartPos[ii])
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].fPlayerStartHeading[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].fPlayerStartHeading[ii])			
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].vPlayerEndPos[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].vPlayerEndPos[ii])			
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].fPlayerEndHeading[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].fPlayerEndHeading[ii])
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].vPlayerAltEndPos[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].vPlayerAltEndPos[ii])			
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].fPlayerAltEndHeading[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].fPlayerAltEndHeading[ii])			
		ENDFOR
		
		FOR ii = 0 TO (MAX_CAMERA_SHOTS -1)
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].iCutsceneShotBitSet[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].iCutsceneShotBitSet[ii])
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].iCutsceneShotCustomTicker[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].iCutsceneShotCustomTicker[ii])			
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].iVehicleToAttachTo[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].iVehicleToAttachTo[ii])
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].vOffsetPos[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].vOffsetPos[ii])
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].vLookAtPos[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].vLookAtPos[ii])
			PRINTLN("[PED CUT] * g_FMMC_STRUCT.sScriptedCutsceneData[", i, "].iEndEarlyTime[", ii, "] = ", g_FMMC_STRUCT.sScriptedCutsceneData[i].iEndEarlyTime[ii])
		ENDFOR
	ENDFOR 
	FOR i = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
		PRINTSTRING("------------------------TEAM [")PRINTINT(i)PRINTSTRING("]---------------------------")PRINTNL() 		
		PRINTSTRING("vPostMissionPos[")PRINTINT(i)PRINTSTRING("]             	= ")PRINTVECTOR(g_FMMC_STRUCT.vPostMissionPos[i])PRINTNL()	
		PRINTSTRING("fPostMissionSpawnRadius[")PRINTINT(i)PRINTSTRING("]		= ")PRINTFLOAT(g_FMMC_STRUCT.fPostMissionSpawnRadius[i])PRINTNL()
		PRINTSTRING("vPostMissionPOI[")PRINTINT(i)PRINTSTRING("]              	= ")PRINTVECTOR(g_FMMC_STRUCT.vPostMissionPOI[i])PRINTNL()
	ENDFOR
	PRINTSTRING("iAdversaryModeType       = ")						PRINTINT(g_FMMC_STRUCT.iAdversaryModeType)PRINTNL()	
	PRINTSTRING("iRestartInCoverBitset       = ")					PRINTINT(g_FMMC_STRUCT.iRestartInCoverBitset)PRINTNL()	
	PRINTSTRING("iRestartInNearbyPlacedVehicleBitset       = ")		PRINTINT(g_FMMC_STRUCT.iRestartInNearbyPlacedVehicleBitset)PRINTNL()		
	PRINTSTRING("iHandheldDrillingMinigameSpawnGroup       = ")		PRINTINT(g_FMMC_STRUCT.iHandheldDrillingMinigameSpawnGroup)PRINTNL()		
	
	PRINTSTRING("------------------------MISSION RULE DATA---------------------------")PRINTNL()
	IF IS_THIS_A_MISSION()
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_CRATE_DROP
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_GANGHIDEOUT
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
		FOR i = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
			PRINTSTRING("------------------------TEAM [")PRINTINT(i)PRINTSTRING("]---------------------------")PRINTNL() 			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].vStartPos                  = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].vStartPos)PRINTNL()			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].biOutFitsAvailableBitset[0]=")PRINTINT(g_FMMC_STRUCT.biOutFitsAvailableBitset[i][0])PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].biOutFitsAvailableBitset[1]= ")PRINTINT(g_FMMC_STRUCT.biOutFitsAvailableBitset[i][1])PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].biOutFitsAvailableBitset[2]= ")PRINTINT(g_FMMC_STRUCT.biOutFitsAvailableBitset[i][2])PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].biOutFitsAvailableBitset[3]= ")PRINTINT(g_FMMC_STRUCT.biOutFitsAvailableBitset[i][3])PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].biStyleAvailableBitset[0]= ")PRINTINT(g_FMMC_STRUCT.biStyleAvailableBitset[i][0])PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].biStyleAvailableBitset[1]= ")PRINTINT(g_FMMC_STRUCT.biStyleAvailableBitset[i][1])PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].biMaskAvailableBitset[0]= ")PRINTINT(g_FMMC_STRUCT.biMaskAvailableBitset[i][0])PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].vCameraPos                 = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].vCameraRot            	   = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraRot)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].vDeliveryPos               = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].vDeliveryPos)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iDeliveryRadius            = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iDeliveryRadius)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].vEndPos                    = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].vEndPos)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iEndRadius                 = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iEndRadius)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].vAreaPos                   = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].vAreaPos)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iAreaRadius                = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iAreaRadius)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iAreaTime                  = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iAreaTime)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iPlayerLives               = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayerLives)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iNumberOfPlayerKillsRule   = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfPlayerKillsRule)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iNumberOfPedKillsRule      = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfPedKillsRule)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iNumberOfVehKillsRule      = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfVehKillsRule)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iNumberOfObjKillsRule      = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfObjKillsRule)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iNumberOfDeathsRule        = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfDeathsRule)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iNumberOfPedDeliveryRule   = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfPedDeliveryRule)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iNumberOfVehDeliveryRule   = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfVehDeliveryRule)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iNumberOfObjDeliveryRule   = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfObjDeliveryRule)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMissionTimeLimit          = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionTimeLimit)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMissionScoreLimit         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionScoreLimit)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMissionLapLimit           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionLapLimit)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMissionTargetScore        = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionTargetScore)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iNumberOfTeamRules         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfTeamRules)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iPointsPerKill             = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPointsPerKill)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iAggroIndexBS_TeamHackFails              	= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iAggroIndexBS_TeamHackFails)PRINTNL()		
			
			PRINTSTRING("iPedCarryLimit[")PRINTINT(i)PRINTSTRING("]                                = ")PRINTINT(g_FMMC_STRUCT.iPedCarryLimit[i])PRINTNL()
			PRINTSTRING("iOBJCarryLimit[")PRINTINT(i)PRINTSTRING("]                                = ")PRINTINT(g_FMMC_STRUCT.iOBJCarryLimit[i])PRINTNL()
			
			PRINTSTRING("iInitialPoints[")PRINTINT(i)PRINTSTRING("]                                = ")PRINTINT(g_FMMC_STRUCT.iInitialPoints[i])PRINTNL()
			PRINTSTRING("iRespawnVehicle[")PRINTINT(i)PRINTSTRING("]                                = ")PRINTINT(g_FMMC_STRUCT.iRespawnVehicle[i])PRINTNL()
			PRINTSTRING("iTeamNames[")PRINTINT(i)PRINTSTRING("]                               		= ")PRINTINT(g_FMMC_STRUCT.iTeamNames[i])PRINTNL()
			PRINTSTRING("iCriticalMinimumForTeam[")PRINTINT(i)PRINTSTRING("]                        = ")PRINTINT(g_FMMC_STRUCT.iCriticalMinimumForTeam[i])PRINTNL()
			PRINTSTRING("iTeamColourOverride[")PRINTINT(i)PRINTSTRING("]                       	 	= ")PRINTINT(g_FMMC_STRUCT.iTeamColourOverride[i])PRINTNL()
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].tl63SpecSynopsis[0]         = ")PRINTSTRING(g_FMMC_STRUCT.sFMMCEndConditions[i].tl63SpecSynopsis[0])PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].tl63SpecSynopsis[1]         = ")PRINTSTRING(g_FMMC_STRUCT.sFMMCEndConditions[i].tl63SpecSynopsis[1])PRINTNL()
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iSpawnAreaBitset           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iSpawnAreaBitset)PRINTNL()
						
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iSpawnAreaBitset, iTask)
					PRINTSTRING("SPAWN AREA ON = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
				ENDIF
			ENDFOR
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iPlayAreaBitset            = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayAreaBitset)PRINTNL()
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayAreaBitset, iTask)
					PRINTSTRING("PLAY AREA ON = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iPlayAreaTimer             = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayAreaTimer)PRINTNL()
				ENDIF
			ENDFOR
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iPlayArea2Bitset           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayArea2Bitset)PRINTNL()
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayArea2Bitset, iTask)
					PRINTSTRING("PLAY AREA 2 ON = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iPlayArea2Timer            = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayArea2Timer)PRINTNL()
				ENDIF
			ENDFOR
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iLeaveAreaBitset           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iLeaveAreaBitset)PRINTNL()
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iLeaveAreaBitset, iTask)
					PRINTSTRING("LEAVE AREA ON = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
				ENDIF
			ENDFOR
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iLeaveArea2Bitset          = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iLeaveArea2Bitset)PRINTNL()
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iLeaveArea2Bitset, iTask)
					PRINTSTRING("LEAVE AREA 2 ON = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
				ENDIF
			ENDFOR
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iWantedLevelBounds         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedLevelBounds)PRINTNL()
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedLevelBounds, iTask)
					PRINTSTRING("WANTED IN AREA ON = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
				ENDIF
			ENDFOR
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iWantedLevelBounds2        = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedLevelBounds2)PRINTNL()
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedLevelBounds2, iTask)
					PRINTSTRING("WANTED IN AREA 2 ON = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
				ENDIF
			ENDFOR
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangBackupAtMidBitset          = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupAtMidBitset)PRINTNL()
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupAtMidBitset, iTask)
					PRINTSTRING("LOSE WANTED = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
				ENDIF
			ENDFOR
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iLoseWantedBitset          = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iLoseWantedBitset)PRINTNL()
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iLoseWantedBitset, iTask)
					PRINTSTRING("LOSE WANTED = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
				ENDIF
			ENDFOR
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iTeamFailBitset            = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamFailBitset)PRINTNL()
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamFailBitset, iTask)
					PRINTSTRING("TEAM FAIL = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
				ENDIF
			ENDFOR
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRespawnInVehicleBitset     = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRespawnInVehicleBitset)PRINTNL()
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iRespawnInVehicleBitset, iTask)
					PRINTSTRING("RESPAWN VEHICLE = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
				ENDIF
			ENDFOR
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRespawnInLastMissionVehicleFromBS     = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRespawnInLastMissionVehicleFromBS)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iStartingInventory         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iStartingInventory)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iStartingInventoryTwo         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iStartingInventoryTwo)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iStartingInventoryThree         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iStartingInventoryThree)PRINTNL()
						
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMidMissionInventory         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMidMissionInventoryTwo         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventoryTwo)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMidMissionInventoryThree         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventoryThree)PRINTNL()
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMidMissionInventoryGiveOnTeam         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventoryGiveOnTeam)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMidMissionInventoryGiveOnTeamRule         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventoryGiveOnTeamRule)PRINTNL()
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMidMissionInventory2         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMidMissionInventory2Two         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2Two)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMidMissionInventory2Three         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2Three)PRINTNL()
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMidMissionInventory2GiveOnTeam         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2GiveOnTeam)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMidMissionInventory2GiveOnTeamRule         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2GiveOnTeamRule)PRINTNL()
		
						
			FOR iTask = 0 TO (31)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iStartingInventory, iTask)
					PRINTSTRING("TEAM FAIL = TRUE [")PRINTINT(iTask)PRINTSTRING("]")PRINTNL()
				ENDIF
			ENDFOR
			
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iWantedAtMidBitset = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedAtMidBitset)PRINTNL()

			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iSpawnInvincibility = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iSpawnInvincibility)PRINTNL()
			PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iTeamThermalCharges = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamThermalCharges)PRINTNL()

			FOR iTask = 0 TO (g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfTeamRules - 1)
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitset[")PRINTINT(iTask)PRINTSTRING("]                    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitset[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetTwo[")PRINTINT(iTask)PRINTSTRING("]                 = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetTwo[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetThree[")PRINTINT(iTask)PRINTSTRING("]               = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetThree[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetFour[")PRINTINT(iTask)PRINTSTRING("]                = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetFour[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetFive[")PRINTINT(iTask)PRINTSTRING("]                = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetFive[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetSix[")PRINTINT(iTask)PRINTSTRING("]                 = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetSix[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetSeven[")PRINTINT(iTask)PRINTSTRING("]               = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetSeven[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetEight[")PRINTINT(iTask)PRINTSTRING("]               = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetEight[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetNine[")PRINTINT(iTask)PRINTSTRING("]                = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetNine[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetTen[")PRINTINT(iTask)PRINTSTRING("]                 = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetTen[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetEleven[")PRINTINT(iTask)PRINTSTRING("]              = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetEleven[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetTwelve[")PRINTINT(iTask)PRINTSTRING("]              = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetTwelve[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetThirteen[")PRINTINT(iTask)PRINTSTRING("]            = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetThirteen[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetFourteen[")PRINTINT(iTask)PRINTSTRING("]            = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetFourteen[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetFifteen[")PRINTINT(iTask)PRINTSTRING("]             = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetFifteen[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleBitsetSixteen[")PRINTINT(iTask)PRINTSTRING("]             = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetSixteen[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].fPedDriveSpeedOverride[")PRINTINT(iTask)PRINTSTRING("]     = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].fPedDriveSpeedOverride[iTask])PRINTNL()	
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iProgressAnimStageForPed[")PRINTINT(iTask)PRINTSTRING("]     = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iProgressAnimStageForPed[iTask])PRINTNL()	
							
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iAggroIndexBS_CheckTeamsForAggroWanted[")PRINTINT(iTask)PRINTSTRING("]      = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iAggroIndexBS_CheckTeamsForAggroWanted[iTask])PRINTNL()		
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iAggroIndexBS_GangChase_ReqAggro[")PRINTINT(iTask)PRINTSTRING("]            = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iAggroIndexBS_GangChase_ReqAggro[iTask])PRINTNL()		
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iAggroIndexBS_Music[")PRINTINT(iTask)PRINTSTRING("]                 		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iAggroIndexBS_Music[iTask])PRINTNL()		
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iCustomHUDBS[")PRINTINT(iTask)PRINTSTRING("]                 		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iCustomHUDBS[iTask])PRINTNL()		
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iPriorityEntityHealthThresholdOverride[")PRINTINT(iTask)PRINTSTRING("]      = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPriorityEntityHealthThresholdOverride[iTask])PRINTNL()

				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRespawnOnWaterDistanceFromCoord[")PRINTINT(iTask)PRINTSTRING("]            = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRespawnOnWaterDistanceFromCoord[iTask])PRINTNL()	
	
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iSpecificPrestreamRule[")PRINTINT(iTask)PRINTSTRING("]             			= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iSpecificPrestreamRule[iTask])PRINTNL()				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].vDropOff[")PRINTINT(iTask)PRINTSTRING("]               						= ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].vDropOff[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjectiveTimeLimitRule[")PRINTINT(iTask)PRINTSTRING("]						= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveTimeLimitRule[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjectiveScore[")PRINTINT(iTask)PRINTSTRING("]        						= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveScore[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iTargetScore[")PRINTINT(iTask)PRINTSTRING("]          						= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iTargetScore[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iTakeoverTime[")PRINTINT(iTask)PRINTSTRING("]          						= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iTakeoverTime[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iReCapture[")PRINTINT(iTask)PRINTSTRING("]             						= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iReCapture[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iWantedChange[")PRINTINT(iTask)PRINTSTRING("]          						= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedChange[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iWantedDelay[")PRINTINT(iTask)PRINTSTRING("]           						= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedDelay[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iWantedDelayHUDString[")PRINTINT(iTask)PRINTSTRING("]  						= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedDelayHUDString[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].fDropOffRadius[")PRINTINT(iTask)PRINTSTRING("]         						= ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].fDropOffRadius[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjRequiredForRulePass[")PRINTINT(iTask)PRINTSTRING("]         			= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjRequiredForRulePass[iTask])PRINTNL()				
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangBackupType[")PRINTINT(iTask)PRINTSTRING("]				= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupType[iTask])PRINTNL()
				FOR ii = 0 TO FMMC_MAX_GANG_CHASE_UNIT_TYPES -1 
					PRINTLN("sFMMCEndConditions[", i, "].sMultipleGangChaseSettings[", iTask, "][", ii, "].iType = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sMultipleGangChaseSettings[iTask][ii].iType)
					PRINTLN("sFMMCEndConditions[", i, "].sMultipleGangChaseSettings[", iTask, "][", ii, "].iIntensity = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sMultipleGangChaseSettings[iTask][ii].iIntensity)
					PRINTLN("sFMMCEndConditions[", i, "].sMultipleGangChaseSettings[", iTask, "][", ii, "].iPriority = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sMultipleGangChaseSettings[iTask][ii].iPriority)
				ENDFOR				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangBackupNumber[")PRINTINT(iTask)PRINTSTRING("]			= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupNumber[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangBackupDelay[")PRINTINT(iTask)PRINTSTRING("]			= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupDelay[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangChaseRespawnDelay[")PRINTINT(iTask)PRINTSTRING("]			= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangChaseRespawnDelay[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangOverrideObjectiveTextCustomString[")PRINTINT(iTask)PRINTSTRING("]			= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangOverrideObjectiveTextCustomString[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangBackupColour[")PRINTINT(iTask)PRINTSTRING("]			= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupColour[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangMaxBackup[")PRINTINT(iTask)PRINTSTRING("]				= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangMaxBackup[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangBackupFleeDist[")PRINTINT(iTask)PRINTSTRING("]			= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupFleeDist[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangVehHealth[")PRINTINT(iTask)PRINTSTRING("]				= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangVehHealth[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangBackupAreaType[")PRINTINT(iTask)PRINTSTRING("]			= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupAreaType[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangAccuracy[")PRINTINT(iTask)PRINTSTRING("]			 	= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangAccuracy[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangChaseForwardSpawnChance[")PRINTINT(iTask)PRINTSTRING("]				= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangChaseForwardSpawnChance[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangChaseSpawnBehindChance[")PRINTINT(iTask)PRINTSTRING("]				= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangChaseSpawnBehindChance[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangNGNum[")PRINTINT(iTask)PRINTSTRING("]			 		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangNGNum[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangNGMax[")PRINTINT(iTask)PRINTSTRING("]					= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangNGMax[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangLGNum[")PRINTINT(iTask)PRINTSTRING("]					= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangLGNum[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iGangLGMax[")PRINTINT(iTask)PRINTSTRING("]			 		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iGangLGMax[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].fGangBlipNoticeRange[")PRINTINT(iTask)PRINTSTRING("]		= ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].fGangBlipNoticeRange[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].vGangTriggerPos1[")PRINTINT(iTask)PRINTSTRING("]      	= ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].vGangTriggerPos1[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].vGangTriggerPos2[")PRINTINT(iTask)PRINTSTRING("]        = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].vGangTriggerPos2[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].fGangTriggerWidth[")PRINTINT(iTask)PRINTSTRING("]       = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].fGangTriggerWidth[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicStart[")PRINTINT(iTask)PRINTSTRING("]          	= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicStart[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicSecondaryRoleMood[")PRINTINT(iTask)PRINTSTRING("] = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicSecondaryRoleMood[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicMid[")PRINTINT(iTask)PRINTSTRING("]          		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicMid[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScoreReachedMood[")PRINTINT(iTask)PRINTSTRING("]  = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScoreReachedMood[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicSecondaryRoleMood[")PRINTINT(iTask)PRINTSTRING("] = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicSecondaryRoleMood[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScoreAggroMood[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScoreAggroMood[iTask])PRINTNL()				
			
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_OnFoot[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_OnFoot[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_InAircraft[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InAircraft[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_Parachuting[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Parachuting[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_Swimming[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Swimming[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_Boating[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Boating[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_Driving[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Driving[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_InteriorSpace[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_InteriorSpace_Alt_1[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace_Alt_1[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_InteriorSpace_Alt_2[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace_Alt_2[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_IsInteracting[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_IsInteracting[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_IsBeingChasedByGang[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_IsBeingChasedByGang[iTask])PRINTNL()				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_OnFoot_Aggro[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_OnFoot_Aggro[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_InAircraft_Aggro[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InAircraft_Aggro[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_Parachuting_Aggro[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Parachuting_Aggro[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_Swimming_Aggro[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Swimming_Aggro[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_Boating_Aggro[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Boating_Aggro[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_Driving_Aggro[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Driving_Aggro[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_InteriorSpace_Aggro[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace_Aggro[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_InteriorSpace_Aggro_Alt_1[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace_Aggro_Alt_1[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_InteriorSpace_Aggro_Alt_2[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace_Aggro_Alt_2[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_IsInteracting_Aggro[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_IsInteracting_Aggro[iTask])PRINTNL()								
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_IsBeingChasedByGang_Aggro[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_IsBeingChasedByGang_Aggro[iTask])PRINTNL()								
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_WantedLevelGreaterThan1[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_WantedLevelGreaterThan1[iTask])PRINTNL()				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScore_WantedLevelGreaterThan1_Aggro[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_WantedLevelGreaterThan1_Aggro[iTask])PRINTNL()				

				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMusicScoreAggroMood[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScoreAggroMood[iTask])PRINTNL()				
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iLinkedDoor[")PRINTINT(iTask)PRINTSTRING("]          	= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iLinkedDoor[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iLinkedDoor2[")PRINTINT(iTask)PRINTSTRING("]          	= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iLinkedDoor2[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iHackScreen[")PRINTINT(iTask)PRINTSTRING("]          	= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iHackScreen[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].sBoundsStruct[")PRINTINT(iTask)PRINTSTRING("].vPos[")PRINTSTRING("]    = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].vPos)PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].sBoundsStruct[")PRINTINT(iTask)PRINTSTRING("].vPos1[")PRINTSTRING("]   = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].vPos1)PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].sBoundsStruct[")PRINTINT(iTask)PRINTSTRING("].vPos2[")PRINTSTRING("]   = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].vPos2)PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].sBoundsStruct[")PRINTINT(iTask)PRINTSTRING("].fRadius[")PRINTSTRING("] = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].fRadius)PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].sBoundsStruct[")PRINTINT(iTask)PRINTSTRING("].fWidth[")PRINTSTRING("]  = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].fWidth)PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].tl63Objective[")PRINTINT(iTask)PRINTSTRING("]           = ")PRINTSTRING(g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].tl63Objective1[")PRINTINT(iTask)PRINTSTRING("]          = ")PRINTSTRING(g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective1[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].tl63Objective2[")PRINTINT(iTask)PRINTSTRING("]          = ")PRINTSTRING(g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective2[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].tl23ObjBlip[")PRINTINT(iTask)PRINTSTRING("]           = ")PRINTSTRING(g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjBlip[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].tl23ObjSingular[")PRINTINT(iTask)PRINTSTRING("]          = ")PRINTSTRING(g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjSingular[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].tl23ObjPlural[")PRINTINT(iTask)PRINTSTRING("]          = ")PRINTSTRING(g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjPlural[iTask])PRINTNL()
								
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjectiveProgressionLogic[")PRINTINT(iTask)PRINTSTRING("][0]           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveProgressionLogic[iTask][0])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjectiveProgressionLogic[")PRINTINT(iTask)PRINTSTRING("][1]           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveProgressionLogic[iTask][1])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjectiveProgressionLogic[")PRINTINT(iTask)PRINTSTRING("][2]           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveProgressionLogic[iTask][2])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjectiveProgressionLogic[")PRINTINT(iTask)PRINTSTRING("][3]           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveProgressionLogic[iTask][3])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjectiveProgMidPointBitset[")PRINTINT(iTask)PRINTSTRING("]            = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveProgMidPointBitset[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjectiveMidPointProgressOtherTeamLogic[")PRINTINT(iTask)PRINTSTRING("][0]           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveMidPointProgressOtherTeamLogic[iTask][0])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjectiveMidPointProgressOtherTeamLogic[")PRINTINT(iTask)PRINTSTRING("][1]           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveMidPointProgressOtherTeamLogic[iTask][1])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjectiveMidPointProgressOtherTeamLogic[")PRINTINT(iTask)PRINTSTRING("][2]           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveMidPointProgressOtherTeamLogic[iTask][2])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRandomNextRuleBS[")PRINTINT(iTask)PRINTSTRING("]           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRandomNextRuleBS[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRestrictedInventory[")PRINTINT(iTask)PRINTSTRING("]                    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRestrictedInventory[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iNearestTargetThreshold[")PRINTINT(iTask)PRINTSTRING("]                  = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iNearestTargetThreshold[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleTraffic[")PRINTINT(iTask)PRINTSTRING("]                  = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleTraffic[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRulePedDensity[")PRINTINT(iTask)PRINTSTRING("]               = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRulePedDensity[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iTimeLimit[")PRINTINT(iTask)PRINTSTRING("]                    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iTimeLimit[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjCarryHUDCustomLabel[")PRINTINT(iTask)PRINTSTRING("]                    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjCarryHUDCustomLabel[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iShowTickerOnRuleStart[")PRINTINT(iTask)PRINTSTRING("]   	= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iShowTickerOnRuleStart[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iCustomObjCompTicker[")PRINTINT(iTask)PRINTSTRING("]    	= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iCustomObjCompTicker[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iCustomMidObjCompTicker[")PRINTINT(iTask)PRINTSTRING("]  	= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iCustomMidObjCompTicker[iTask])PRINTNL()
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitset[iTask], ciBS_RULE_CANNOT_EXIT_VEHICLE)
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iLockedInVehBS[")PRINTINT(iTask)PRINTSTRING("]                    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iLockedInVehBS[iTask])PRINTNL()
				ELSE
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iLockedInVehBS[")PRINTINT(iTask)PRINTSTRING("]                    = N/A, ciBS_RULE_CANNOT_EXIT_VEHICLE ISN'T SET")PRINTNL()
				ENDIF
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].OverwriteEntityType[")PRINTINT(iTask)PRINTSTRING("]                    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].OverwriteEntityType[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].OverwriteEntityId[")PRINTINT(iTask)PRINTSTRING("]                    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].OverwriteEntityId[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMinSpeed[")PRINTINT(iTask)PRINTSTRING("]                    = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMinSpeed[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMinSpeed_AddPerLap[")PRINTINT(iTask)PRINTSTRING("]          = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMinSpeed_AddPerLap[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iMinSpeed_MeterMax[")PRINTINT(iTask)PRINTSTRING("]           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iMinSpeed_MeterMax[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iFailSpeedTimer[")PRINTINT(iTask)PRINTSTRING("]              = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iFailSpeedTimer[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iPlayerLivesPerRule[")PRINTINT(iTask)PRINTSTRING("]           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayerLivesPerRule[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iObjectiveSuddenDeathTimeLimit[")PRINTINT(iTask)PRINTSTRING("] = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveSuddenDeathTimeLimit[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iDialogueToWaitFor[")PRINTINT(iTask)PRINTSTRING("]			 = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iDialogueToWaitFor[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iTeamWithPlayerPositionToDraw[")PRINTINT(iTask)PRINTSTRING("]			 = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamWithPlayerPositionToDraw[iTask])PRINTNL()
				
				INT iClearPreReqsOnRuleBSIndex = 0
				FOR iClearPreReqsOnRuleBSIndex = 0 TO ciPREREQ_Bitsets - 1
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iClearPrereqsOnRuleBS[")PRINTINT(iTask)PRINTSTRING("[")PRINTINT(iClearPreReqsOnRuleBSIndex)PRINTSTRING("]		 = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iClearPrereqsOnRuleBS[iTask][iClearPreReqsOnRuleBSIndex])PRINTNL()
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iSetPrereqsOnRuleBS[")PRINTINT(iTask)PRINTSTRING("[")PRINTINT(iClearPreReqsOnRuleBSIndex)PRINTSTRING("]		 = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iSetPrereqsOnRuleBS[iTask][iClearPreReqsOnRuleBSIndex])PRINTNL()
				ENDFOR
				
				PRINTLN("iTimerStart[", i, "] for rule ", iTask, " is ", IS_BIT_SET(g_FMMC_STRUCT.iTimerStart[i], iTask))
				PRINTLN("iTimerStop[" , i, "] for rule ", iTask, " is ", IS_BIT_SET(g_FMMC_STRUCT.iTimerStop[i]	, iTask))
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iTakeProgressionAmount[")PRINTINT(iTask)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iTakeProgressionAmount[iTask])PRINTNL()
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].fExplodeNearbyRocketsRange[")PRINTINT(iTask)PRINTSTRING("]		= ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].fExplodeNearbyRocketsRange[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].fExplodeNearbyRocketsChance[")PRINTINT(iTask)PRINTSTRING("]		= ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].fExplodeNearbyRocketsChance[iTask])PRINTNL()
				
				INT i3
				FOR i3 = 0 TO (FMMC_MAX_VEHICLES - 1)
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleVehicleWarp[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i3)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleVehicleWarp[iTask][i3])PRINTNL()
				ENDFOR
				FOR i3 = 0 TO (FMMC_MAX_PEDS - 1)
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRulePedWarp[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i3)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRulePedWarp[iTask][i3])PRINTNL()
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iPedAssGotoTask_RulePoolAssignmentIndex[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i3)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPedAssGotoTask_RulePoolAssignmentIndex[iTask][i3])PRINTNL()
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iPedAssGotoTask_RulePoolAssignmentStartingGoto[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i3)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iPedAssGotoTask_RulePoolAssignmentStartingGoto[iTask][i3])PRINTNL()					
				ENDFOR
				FOR i3 = 0 TO (FMMC_MAX_NUM_OBJECTS - 1)
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleObjWarp[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i3)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleObjWarp[iTask][i3])PRINTNL()
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iDroneAssGotoTask_RulePoolAssignmentIndex[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i3)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iDroneAssGotoTask_RulePoolAssignmentIndex[iTask][i3])PRINTNL()
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iDroneAssGotoTask_RulePoolAssignmentStartingGoto[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i3)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iDroneAssGotoTask_RulePoolAssignmentStartingGoto[iTask][i3])PRINTNL()					
				ENDFOR
				FOR i3 = 0 TO (FMMC_MAX_GO_TO_LOCATIONS - 1)
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRuleLocWarp[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i3)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleLocWarp[iTask][i3])PRINTNL()
				ENDFOR
				
				FOR i3 = 0 TO (FMMC_MAX_RESTART_CHECKPOINTS - 1)
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iRestartCheckpointBitset[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i3)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iRestartCheckpointBitset[iTask][i3])PRINTNL()
					IF g_FMMC_STRUCT.sFMMCEndConditions[i].iRestartCheckpointBitset[iTask][i3] != 0
						PRINTLN("[RESTART_CHECKPOINTS] Checkpoint Set - Rule: ", iTask, " Checkpoint: ", i3, 
						" | Set on Pass: ", IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iRestartCheckpointBitset[iTask][i3], ciRESTART_CHECKPOINT_BITSET_UPDATE_ON_PASS),
						" Set on Fail: ", IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iRestartCheckpointBitset[iTask][i3], ciRESTART_CHECKPOINT_BITSET_UPDATE_ON_FAIL),
						" Set on Midpoint: ", IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iRestartCheckpointBitset[iTask][i3], ciRESTART_CHECKPOINT_BITSET_UPDATE_ON_MIDPOINT))
					ENDIF
				ENDFOR
				
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iCameraShakeDuration[")PRINTINT(iTask)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iCameraShakeDuration[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iCameraShakeType[")PRINTINT(iTask)PRINTSTRING("]			= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iCameraShakeType[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].fCameraShakeIntensity[")PRINTINT(iTask)PRINTSTRING("]		= ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].fCameraShakeIntensity[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iSecondaryCameraShakeType[")PRINTINT(iTask)PRINTSTRING("]			= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iSecondaryCameraShakeType[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].fSecondaryCameraShakeIntensity[")PRINTINT(iTask)PRINTSTRING("]		= ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].fSecondaryCameraShakeIntensity[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iSecondaryCameraShakeInterval[")PRINTINT(iTask)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iSecondaryCameraShakeInterval[iTask])PRINTNL()
				PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iSecondaryCameraShakeDuration[")PRINTINT(iTask)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iSecondaryCameraShakeDuration[iTask])PRINTNL()
				
				INT iAlt = 0
				FOR iAlt = 0 TO ciAltVarsBS_MAX-1
					PRINTSTRING("sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iAltVarsBitsetRule[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(iAlt)PRINTSTRING("]		= ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iAltVarsBitsetRule[iTask][iAlt])PRINTNL()
				ENDFOR
				
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].iForcedMinimapInterior[",iTask,"] = ", g_FMMC_STRUCT.sFMMCEndConditions[i].iForcedMinimapInterior[iTask])
				
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].iForcedHintCamEntityType[",iTask,"] = ", g_FMMC_STRUCT.sFMMCEndConditions[i].iForcedHintCamEntityType[iTask])
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].iForcedHintCamEntityID[",iTask,"] = ", g_FMMC_STRUCT.sFMMCEndConditions[i].iForcedHintCamEntityID[iTask])
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].iForcedHintCamDuration[",iTask,"] = ", g_FMMC_STRUCT.sFMMCEndConditions[i].iForcedHintCamDuration[iTask])
				
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sOnRuleStart[",iTask,"].iOnRuleBitSet = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[iTask].iOnRuleBitSet)
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sOnRuleStart[",iTask,"].sLivesStruct.iLivesType = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[iTask].sLivesStruct.iLivesType)
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sOnRuleStart[",iTask,"].sLivesStruct.iLivesPlayersToCheck = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[iTask].sLivesStruct.iLivesPlayersToCheck)
				FOR iAlt = 0 TO ciFMMC_ON_RULE_SCALING_INCREASE_BANDS_MAX-1
					PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sOnRuleStart[",iTask,"].sLivesStruct.iLivesIncreasePlayerBands[",iAlt,"] = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[iTask].sLivesStruct.iLivesIncreasePlayerBands[iAlt])
				ENDFOR
				#IF FEATURE_DLC_1_2022
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sOnRuleStart[",iTask,"].sTimeStruct.iTimeType = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[iTask].sTimeStruct.iTimeType)
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sOnRuleStart[",iTask,"].sTimeStruct.iTimePlayersToCheck = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[iTask].sTimeStruct.iTimePlayersToCheck)
				FOR iAlt = 0 TO ciFMMC_ON_RULE_SCALING_INCREASE_BANDS_MAX-1
					PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sOnRuleStart[",iTask,"].sTimeStruct.iTimeIncreasePlayerBands[",iAlt,"] = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[iTask].sTimeStruct.iTimeIncreasePlayerBands[iAlt])
				ENDFOR
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sOnRuleStart[",iTask,"].iRagdollDuration = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[iTask].iRagdollDuration)
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sOnRuleStart[",iTask,"].iFadeOutDuration = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[iTask].iFadeOutDuration)
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sOnRuleStart[",iTask,"].iFadeInDuration = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[iTask].iFadeInDuration)
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sOnRuleStart[",iTask,"].iPlayerModelToSwapTo = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[iTask].iPlayerModelToSwapTo)
				#ENDIF
				
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sRulePlayerModelSwap[",iTask,"].iPlayerModelToSwapToOnKill = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sRulePlayerModelSwap[iTask].iPlayerModelToSwapToOnKill)
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sRulePlayerModelSwap[",iTask,"].iPlayerModelToSwapToOnRespawn = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sRulePlayerModelSwap[iTask].iPlayerModelToSwapToOnRespawn)
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].sRulePlayerModelSwap[",iTask,"].iPlayerModelSwapPrereqToComplete = ", g_FMMC_STRUCT.sFMMCEndConditions[i].sRulePlayerModelSwap[iTask].iPlayerModelSwapPrereqToComplete)
				
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].iBS_MissionTeamRelations_OnRuleSetting[",iTask,"] = ", g_FMMC_STRUCT.sFMMCEndConditions[i].iBS_MissionTeamRelations_OnRuleSetting[iTask])				
			
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].iBS_MissionTeamRelations_OnRuleEnabled for rule: ", iTask, " = ", IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iBS_MissionTeamRelations_OnRuleEnabled, iTask))
				
				PRINTLN("g_FMMC_STRUCT.sFMMCEndConditions[",i,"].iBS_MissionTeamRelations_OnModelSwapEnabled for rule: ", iTask, " = ", IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iBS_MissionTeamRelations_OnModelSwapEnabled, iTask))
				
			ENDFOR			
			
			PRINTNL()
		ENDFOR	
		
		PRINTLN("------------------------ARENA INTERIOR---------------------------")
		FOR i = 0 TO ARENA_BITSETS-1
			PRINTLN("g_FMMC_STRUCT.sArenaInfo.iArena_Floor1BS = ", g_FMMC_STRUCT.sArenaInfo.iArena_Floor1BS[i])
			PRINTLN("g_FMMC_STRUCT.sArenaInfo.iArena_Floor2BS = ", g_FMMC_STRUCT.sArenaInfo.iArena_Floor2BS[i])
			PRINTLN("g_FMMC_STRUCT.sArenaInfo.iArena_Floor3BS = ", g_FMMC_STRUCT.sArenaInfo.iArena_Floor3BS[i])
			PRINTLN("g_FMMC_STRUCT.sArenaInfo.iArena_Floor4BS = ", g_FMMC_STRUCT.sArenaInfo.iArena_Floor4BS[i])
			PRINTLN("g_FMMC_STRUCT.sArenaInfo.iArena_MiscBS = ", g_FMMC_STRUCT.sArenaInfo.iArena_MiscBS[i])
		ENDFOR
		
	ELSE
		PRINTSTRING("------------------------NOT A MISSION---------------------------")PRINTNL()
		PRINTNL()		
	ENDIF
	PRINTSTRING("----------------------GOTODATA-------------------------")PRINTNL()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]  = 0
		PRINTSTRING("***** NO GOTODATA WAS PLACED OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfGoToLocations[0] = 0")PRINTNL()
	ELSE
		PRINTSTRING("iNumberOfGoToLocations[")PRINTINT(0)PRINTSTRING("]                     = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0])PRINTNL()
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] -1)
			PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].vloC[")PRINTINT(0)PRINTSTRING("]                  = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].vLoc[0])PRINTNL()
			PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].fRadius[")PRINTINT(0)PRINTSTRING("]               = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].fRadius[0])PRINTNL()
			IF IS_THIS_A_MISSION()
			OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_CRATE_DROP
			OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_GANGHIDEOUT
			OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
				
				PRINTLN("[RCC MISSION] g_FMMC_STRUCT_ENTITIES.sGotoLocationData[", i, "].iLocBS = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS)
				FOR iTask = 0 TO 31
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS, iTask)
						PRINTLN("BIT IS SET g_FMMC_STRUCT_ENTITIES.sGotoLocationData[", i, "].iLocBS = ", iTask, " = ", GET_sGotoLocationDataiLocBS_DEBUG_PRINT(iTask), " = TRUE")
					//ELSE
						//PRINTLN("g_FMMC_STRUCT_ENTITIES.sGotoLocationData[", i, "].iLocBS = ", GET_sGotoLocationDataiLocBS_DEBUG_PRINT(i), "  = FALSE")
					ENDIF
				ENDFOR
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS, ciLoc_BS_HideBlip)
				OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sLocBlipStruct.iBlipBitset, ciBLIP_INFO_Hide_Blip)
					PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("] HIDE BLIP? = 		TRUE")PRINTNL()
				ELSE
					PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("] HIDE BLIP? = 		FALSE")PRINTNL()
				ENDIF
				FOR iTask = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
					PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iRule[")PRINTINT(iTask)PRINTSTRING("]                 = ")PRINTSTRING(CONVERT_RULE_INT_TO_STRING_FOR_PRINTING(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iRule[iTask]))PRINTSTRING(" = [")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iRule[iTask])PRINTSTRING("]")PRINTNL()
					PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iPriority[")PRINTINT(iTask)PRINTSTRING("]            = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTask])PRINTNL()
					PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iWholeTeamAtLocation[")PRINTINT(iTask)PRINTSTRING("]  = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iWholeTeamAtLocation[iTask])PRINTNL()
					PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iCaptureMultiplier[")PRINTINT(iTask)PRINTSTRING("]    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iCaptureMultiplier[iTask])PRINTNL()
					PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iArial[")PRINTINT(iTask)PRINTSTRING("]                = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iArial[iTask])PRINTNL()
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].mnVehicleNeeded[iTask] = DUMMY_MODEL_FOR_SCRIPT
						PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].mnVehicleNeeded[")PRINTINT(iTask)PRINTSTRING("]        = DUMMY_MODEL_FOR_SCRIPT") PRINTNL()
					ELSE
						PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].mnVehicleNeeded[")PRINTINT(iTask)PRINTSTRING("]        = ")PRINTSTRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].mnVehicleNeeded[iTask]))PRINTNL()
					ENDIF
				ENDFOR				
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iGPSCutOff         			    	  			  = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iGPSCutOff[0])PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iLocProcessingDelay         			    	    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocProcessingDelay)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iSpawnGroup         			        			 = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iSpawnGroup)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iSpawnSubGroup         			    		     = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iSpawnSubGroup)PRINTNL()
				PRINTLN("sGotoLocationData[", i, "].iModifySpawnGroupOnEvent										= ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iModifySpawnGroupOnEvent)				
				FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
					PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iSpawnSubGroupBS[")PRINTINT(ii)PRINTSTRING("]   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iSpawnSubGroupBS[ii])PRINTNL()
					PRINTLN("sGotoLocationData[", i, "].iModifySpawnSubGroupOnEventBS[", ii, "]			= ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iModifySpawnSubGroupOnEventBS[ii])
				ENDFOR
				PRINTLN("sGotoLocationData[", i, "].iSpawnGroupAdditionalFunctionalityBS									= ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iSpawnGroupAdditionalFunctionalityBS)				
				
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].fSecondaryRadius                  = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].fSecondaryRadius)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iLocBS2         			         = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS2)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iLocBS3         			         = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS3)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].fTeamMembersNeededWithin          = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].fTeamMembersNeededWithin)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iContinuityId        			 = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iContinuityId)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iContentContinuityType        	 = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iContentContinuityType)PRINTNL()
				PRINTSTRING("[MissionVariation] sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iLocMCVarSelectionIndex_One          = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocMCVarSelectionIndex_One)PRINTNL()
				PRINTSTRING("[MissionVariation] sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iLocMCVarSelectionIndex_Two          = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocMCVarSelectionIndex_Two)PRINTNL()
				
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iAggroIndexBS_Entity_Loc         			         = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iAggroIndexBS_Entity_Loc)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].iObjectivePrereq         			    		     = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iObjectivePrereq)PRINTNL()
								
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayAggroPass    	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sEndMissionData.iNextMissionToPlayAggroPass)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayFail	     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sEndMissionData.iNextMissionToPlayFail)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayPass	     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sEndMissionData.iNextMissionToPlayPass)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndPassMocap 			       	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sEndMissionData.iEndPassMocap)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndFailMocap 			     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sEndMissionData.iEndFailMocap)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndAggroMocap 		      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sEndMissionData.iEndAggroMocap)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iBranchScriptCutRule 	    	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sEndMissionData.iBranchScriptCutRule)PRINTNL()
				PRINTSTRING("sGotoLocationData[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iBranchScriptCutScene 	   		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sEndMissionData.iBranchScriptCutScene)PRINTNL()
				
				PRINTLN("sGotoLocationData[", i, "].sWarpLocationSettings.iCheckPointPositionIDStart							= ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sWarpLocationSettings.iCheckPointPositionIDStart)
				PRINTLN("sGotoLocationData[", i, "].sWarpLocationSettings.iWarpLocationBS    	 	 							= ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sWarpLocationSettings.iWarpLocationBS)
				PRINTLN("sGotoLocationData[", i, "].sWarpLocationSettings.iForcedRespawnLocation    						 	= ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sWarpLocationSettings.iForcedRespawnLocation)
				PRINTLN("sGotoLocationData[", i, "].sWarpLocationSettings.sWarpLocationSettings.sEntityWarpPTFX.ePTFXType  	 	= ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sWarpLocationSettings.sEntityWarpPTFX.ePTFXType)				
				INT iii = 0
				FOR iii = 0 TO FMMC_MAX_RULE_VECTOR_WARPS-1					
					PRINTLN("sGotoLocationData[", i, "].sWarpLocationSettings.vPosition[", iii, "]    	 		= ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sWarpLocationSettings.vPosition[iii])
					PRINTLN("sGotoLocationData[", i, "].sWarpLocationSettings.fHeading[", iii, "]    	 		= ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].sWarpLocationSettings.fHeading[iii])
				ENDFOR
				
				FOR iii = 0 TO ciAltVarsBS_MAX-1
					PRINTLN("[AltVarsSystemSpam] - sGotoLocationData[", i, "].iAltVarsBitsetLocation[", iii, "] = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iAltVarsBitsetLocation[iii])
				ENDFOR
			ENDIF
			PRINTNL()
		ENDFOR
	ENDIF
	
	PRINTLN("------------------------TRAINS---------------------------")
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTrains - 1
		PRINTLN("sPlacedTrains[", i, "].vPlacedTrainPos						= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].vPlacedTrainPos)
		PRINTLN("sPlacedTrains[", i, "].iPlacedTrainConfig					= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPlacedTrainConfig)
		PRINTLN("sPlacedTrains[", i, "].iPlacedTrain_StopMovingOnRuleBS		= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPlacedTrain_StopMovingOnRuleBS)
		PRINTLN("sPlacedTrains[", i, "].iPlacedTrain_StopMovingOnRuleTeam	= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPlacedTrain_StopMovingOnRuleTeam)
		PRINTLN("sPlacedTrains[", i, "].iPlacedTrainBS						= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPlacedTrainBS)
		PRINTLN("sPlacedTrains[", i, "].fPlacedTrainCruiseSpeed				= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].fPlacedTrainCruiseSpeed)
		PRINTLN("sPlacedTrains[", i, "].iPlacedTrainColour					= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPlacedTrainColour)
		PRINTLN("sPlacedTrains[", i, "].iPlacedTrainSecondaryColour			= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPlacedTrainSecondaryColour)
		
		PRINTLN("sPlacedTrains[", i, "].iAggroIndexBS_Entity_Train			= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iAggroIndexBS_Entity_Train)
		
		PRINTLN("sPlacedTrains[", i, "].iTrain_AssociatedRuleSpawnLimit		= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_AssociatedRuleSpawnLimit)
		PRINTLN("sPlacedTrains[", i, "].iTrain_AssociatedRule				= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_AssociatedRule)
		PRINTLN("sPlacedTrains[", i, "].iTrain_AssociatedTeam				= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_AssociatedTeam)
		
		PRINTLN("sPlacedTrains[", i, "].iTrain_CleanupRule					= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_CleanupRule)
		PRINTLN("sPlacedTrains[", i, "].iTrain_CleanupTeam					= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_CleanupTeam)

		PRINTLN("sPlacedTrains[", i, "].iTrain_SpawnGroup					= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_SpawnGroup)
		FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			PRINTLN("sPlacedTrains[", i, "].iTrain_SpawnSubGroupBS[", ii, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_SpawnSubGroupBS[ii])
		ENDFOR
		PRINTLN("sPlacedTrains[", i, "].iTrain_ModifySpawnGroupOnEvent					= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_ModifySpawnGroupOnEvent)
		FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			PRINTLN("sPlacedTrains[", i, "].iTrain_ModifySpawnSubGroupOnEventBS[", ii, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_ModifySpawnSubGroupOnEventBS[ii])
		ENDFOR
		PRINTLN("sPlacedTrains[", i, "].iTrain_SpawnGroupAdditionalFunctionalityBS					= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_SpawnGroupAdditionalFunctionalityBS)
		
		FOR ii = 0 TO ciPLACED_TRAIN_MAX_LOCATIONS - 1
			PRINTLN("sPlacedTrains[", i, "].sLocations[", ii, "].iLinkedZone				= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].sLocations[ii].iLinkedZone)
			PRINTLN("sPlacedTrains[", i, "].sLocations[", ii, "].iBitset					= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].sLocations[ii].iBitset)
			PRINTLN("sPlacedTrains[", i, "].sLocations[", ii, "].fCruiseSpeed				= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].sLocations[ii].fCruiseSpeed)
			PRINTLN("sPlacedTrains[", i, "].sLocations[", ii, "].iObjectiveSkip_Rule		= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].sLocations[ii].iObjectiveSkip_Rule)
			PRINTLN("sPlacedTrains[", i, "].sLocations[", ii, "].iObjectiveSkip_Team		= ", g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].sLocations[ii].iObjectiveSkip_Team)
		ENDFOR
		
	ENDFOR
	
	PRINTSTRING("------------------------DUMMY BLIPS---------------------------")PRINTNL()
	FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDummyBlips-1)
		PRINTLN("sDummyBlips[", i, "].vPos 					= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].vPos)
		PRINTLN("sDummyBlips[", i, "].iRule 				= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iRule)
		PRINTLN("sDummyBlips[", i, "].iTeam 				= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iTeam)
		PRINTLN("sDummyBlips[", i, "].iBlipSize 			= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iBlipSize)
		PRINTLN("sDummyBlips[", i, "].iEntityToUse			= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iEntityToUse)
		PRINTLN("sDummyBlips[", i, "].iBitSet 				= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iBitSet)
		PRINTLN("sDummyBlips[", i, "].iEntityType 			= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iEntityType)
		PRINTLN("sDummyBlips[", i, "].iEntityIndex 			= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iEntityIndex)
		PRINTLN("sDummyBlips[", i, "].fHideBlipRange 		= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].fHideBlipRange)
		PRINTLN("sDummyBlips[", i, "].iSpawnGroup 			= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iSpawnGroup)
		PRINTLN("sDummyBlips[", i, "].iSpawnSubGroup 		= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iSpawnSubGroup)
		
		PRINTLN("sDummyBlips[", i, "].iAggroIndexBS_Entity_DummyBlip 		= ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iAggroIndexBS_Entity_DummyBlip)
		FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			PRINTLN("sDummyBlips[", i, "].iSpawnSubGroupBS[", ii,"] = ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iSpawnSubGroupBS[ii])
		ENDFOR
		PRINTLN("sDummyBlips[", i, "].tlDBName = ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].tlDBName)		
		PRINTLN("sDummyBlips[", i, "].vOverridePosition = ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].vOverridePosition)
		
		FOR ii = 0 TO ciAltVarsBS_MAX-1
			PRINTLN("[AltVarsSystemSpam] - sDummyBlips[", i, "].iAltVarsBitsetDummyBlip[", ii, "] = ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[i].iAltVarsBitsetDummyBlip[ii])
		ENDFOR
	ENDFOR
	
	IF IS_THIS_A_MISSION()
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_CRATE_DROP
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_GANGHIDEOUT
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
	
		FOR iTask = 0 TO FMMC_GET_MAX_SCRIPTED_CUTSCENES() - 1
			//IF NOT IS_VECTOR_ZERO()
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sScriptedCutsceneData[iTask].tlName)
				PRINTNL()
				PRINTLN("----------------------CUTSCENE ", iTask, " DATA-------------------------")
				PRINTNL()
				PRINTNL()
				PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].tlName					= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].tlName)
				PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].iCutsceneBitset 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].iCutsceneBitset)
				PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].iCutsceneBitset2 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].iCutsceneBitset2)
				PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].iCutsceneBitset3 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].iCutsceneBitset3)
				PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].iCutsceneBitset4 		=	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].iCutsceneBitset4)
				PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].fCutsceneRollingStartSpeed 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].fCutsceneRollingStartSpeed)
				
				PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].fCutsceneRollingStartSpeed 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].fCutsceneRollingStartSpeed)
				
				FOR i = 0 TO FMMC_MAX_TEAMS-1
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].vPlayerStartPos[", i, "] 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].vPlayerStartPos[i])
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].vPlayerEndPos[", i, "] 			= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].vPlayerEndPos[i])
				ENDFOR
				
				PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].iAnimation 				= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].iAnimation)
				PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].iNumCamShots	 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].iNumCamShots	)
				PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].iInterpBackToGameplayTime 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].iInterpBackToGameplayTime	)
				PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].iFadeBeforeEndTime 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].iFadeBeforeEndTime	)
				FOR i = 0 TO MAX_CAMERA_SHOTS-1
					PRINTNL()
					PRINTLN(" ----- SHOT ", i, " DATA ------ ")
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].iInterpTime[",i,"] 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].iInterpTime[i]	)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].CamType[",i,"] 			= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].CamType[i]		)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].vCamStartPos[",i,"] 	= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].vCamStartPos[i]	)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].vCamEndPos[",i,"] 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].vCamEndPos[i]		)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].vCamStartRot[",i,"] 	= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].vCamStartRot[i]	)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].vCamEndRot[",i,"] 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].vCamEndRot[i]		)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].fStartCamFOV[",i,"] 	= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].fStartCamFOV[i]	)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].fEndCamFOV[",i,"] 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].fEndCamFOV[i]		)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].iEndEarlyTime[",i,"] 	= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].iEndEarlyTime[i]		)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].iCutsceneShotBitSet[",i,"] 	= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].iCutsceneShotBitSet[i]		)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].fMaxFarClipDistance[",i,"] 	= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].fMaxFarClipDistance[i]		)						
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].eDroneUIPreset[",i,"] 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].eDroneUIPreset[i]		)						
					
					PRINTLN(" ----- END OF SHOT ", i, " DATA ------ ")
					PRINTNL()
				ENDFOR
				FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].sPlayerSpecificData[",i,"].iAnimation 			= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sPlayerSpecificData[i].iAnimation)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].sPlayerSpecificData[",i,"].iPlayAnimOnShot 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sPlayerSpecificData[i].iPlayAnimOnShot)
					
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].sPlayerStartWarp[",i,"].iVehID			 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sPlayerStartWarp[i].iVehID			)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].sPlayerStartWarp[",i,"].iVehIDSecondary	 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sPlayerStartWarp[i].iVehIDSecondary	)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].sPlayerStartWarp[",i,"].iVehSeat		 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sPlayerStartWarp[i].iVehSeat		)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].sPlayerStartWarp[",i,"].iBitset1		 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sPlayerStartWarp[i].iBitset1		)
																																										  
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].sPlayerEndWarp[",i,"].iVehID			 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sPlayerEndWarp[i].iVehID			)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].sPlayerEndWarp[",i,"].iVehIDSecondary	 		= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sPlayerEndWarp[i].iVehIDSecondary	)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].sPlayerEndWarp[",i,"].iVehSeat		 			= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sPlayerEndWarp[i].iVehSeat		)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[",iTask,"].sPlayerEndWarp[",i,"].iBitset1		 			= 	", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sPlayerEndWarp[i].iBitset1		)					
				ENDFOR
				
				FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES - 1
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[", iTask, "].sCutsceneEntities[", i, "].iIndex = ", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sCutsceneEntities[i].iIndex)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[", iTask, "].sCutsceneEntities[", i, "].iType = ", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sCutsceneEntities[i].iType)
					PRINTLN("g_FMMC_STRUCT.sScriptedCutsceneData[", iTask, "].sCutsceneEntities[", i, "].tlCutsceneHandle = ", g_FMMC_STRUCT.sScriptedCutsceneData[iTask].sCutsceneEntities[i].tlCutsceneHandle)
				ENDFOR
				PRINTNL()
			ELSE
				PRINTLN("----------------------CUTSCENE ", iTask, " DATA IS EMPTY ---------------------")
			ENDIF
			
			
			
			
			//ENDIF
		ENDFOR
	
	
		FOR iTask = 0 TO MAX_MOCAP_CUTSCENES - 1
			
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iTask].tlName)
				PRINTNL()
				PRINTLN("----------------------MO CAP CUTSCENE ", iTask, " DATA-------------------------")
								
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].tlName										=   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].tlName							  				  )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vCutscenePosition = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].vCutscenePosition)	
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vCutsceneRotation = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].vCutsceneRotation)
				
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].iCutsceneBitset = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].iCutsceneBitset)
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].iCutsceneBitSet2 = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].iCutsceneBitSet2)
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].iCutsceneBitSet3 = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].iCutsceneBitSet3)
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].iCutsceneBitset4 = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].iCutsceneBitset4)
									
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].iCutsceneWarpRange = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].iCutsceneWarpRange)
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].iCutscenePlayerInclusionRange = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].iCutscenePlayerInclusionRange)
				
				FOR i = 0 TO FMMC_MAX_CUTSCENE_HIDE_ENTITIES - 1
					PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].tl31_HideCutsceneHandles[", i, "] 	= 	"  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].tl31_HideCutsceneHandles[i])
				ENDFOR
				
				FOR ii = 0 TO FMMC_MAX_TEAMS-1
					PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vPlayerStartPos[", ii, "] = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].vPlayerStartPos[ii])
					PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vPlayerEndPos[", ii, "] = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].vPlayerEndPos[ii])
				ENDFOR
				
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[",iTask,"].fCutsceneRollingStartSpeed 					= 	"  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].fCutsceneRollingStartSpeed			 			  )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[",iTask,"].iVariationBS 									= 	"  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].iVariationBS			 						  )

				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vPlayerStartPos[0]                           =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].vPlayerStartPos[0]                               )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vPlayerEndPos[0]                             =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].vPlayerEndPos[0]                                 )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].fPlayerStartHeading[0]                       =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].fPlayerStartHeading[0]                           )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].fPlayerEndHeading[0]                         =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].fPlayerEndHeading[0]                             )
				      
																		  
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vPlayerStartPos[1]                           =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].vPlayerStartPos[1]                               )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vPlayerEndPos[1]                             =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].vPlayerEndPos[1]                                 )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].fPlayerStartHeading[1]                       =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].fPlayerStartHeading[1]                           )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].fPlayerEndHeading[1]                         =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].fPlayerEndHeading[1]                             )
				  
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vPlayerStartPos[2]                           =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].vPlayerStartPos[2]                               )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vPlayerEndPos[2]                             =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].vPlayerEndPos[2]                                 )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].fPlayerStartHeading[2]                       =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].fPlayerStartHeading[2]                           )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].fPlayerEndHeading[2]                         =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].fPlayerEndHeading[2]                             )
				      
																		  
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vPlayerStartPos[3]                           =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].vPlayerStartPos[3]                               )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].vPlayerEndPos[3]                             =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].vPlayerEndPos[3]                                 )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].fPlayerStartHeading[3]                       =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].fPlayerStartHeading[3]                           )
				PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].fPlayerEndHeading[3]                         =   "  , g_FMMC_STRUCT.sMocapCutsceneData[iTask].fPlayerEndHeading[3]                             )
								
				FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
					PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[",iTask,"].sPlayerEndWarp[",i,"].iVehID			 		= 	", g_FMMC_STRUCT.sMocapCutsceneData[iTask].sPlayerEndWarp[i].iVehID			)
					PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[",iTask,"].sPlayerEndWarp[",i,"].iVehIDSecondary	 		= 	", g_FMMC_STRUCT.sMocapCutsceneData[iTask].sPlayerEndWarp[i].iVehIDSecondary	)
					PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[",iTask,"].sPlayerEndWarp[",i,"].iVehSeat		 			= 	", g_FMMC_STRUCT.sMocapCutsceneData[iTask].sPlayerEndWarp[i].iVehSeat		)
					PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[",iTask,"].sPlayerEndWarp[",i,"].iBitset1		 			= 	", g_FMMC_STRUCT.sMocapCutsceneData[iTask].sPlayerEndWarp[i].iBitset1		)					
				ENDFOR
				
				FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES - 1
					PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].sCutsceneEntities[", i, "].iIndex = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].sCutsceneEntities[i].iIndex)
					PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].sCutsceneEntities[", i, "].iType = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].sCutsceneEntities[i].iType)
					PRINTLN("g_FMMC_STRUCT.sMocapCutsceneData[", iTask, "].sCutsceneEntities[", i, "].tlCutsceneHandle = ", g_FMMC_STRUCT.sMocapCutsceneData[iTask].sCutsceneEntities[i].tlCutsceneHandle)
				ENDFOR
				
				PRINTNL()
			ELSE
				PRINTLN("----------------------MO CAP CUTSCENE ", iTask, " DATA IS EMPTY ---------------------")
			ENDIF
		ENDFOR
	
	
		FOR iTask = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
			PRINTNL()
			PRINTSTRING("----------------------KILL PLAYER DATA1-------------------------")PRINTNL()
			IF g_FMMC_STRUCT.iNumberOfPlayerRules[iTask]  = 0
				PRINTSTRING("***** NO PLAYER KILL DATA WAS PLACED OR UPLOADED *****")PRINTNL()
				PRINTSTRING("iNumberOfPlayerRules[")PRINTINT(iTask)PRINTSTRING("] = 0")PRINTNL()
			ELSE
				PRINTSTRING("iNumberOfPlayerRules[")PRINTINT(iTask)PRINTSTRING("]                     = ")PRINTINT(g_FMMC_STRUCT.iNumberOfPlayerRules[iTask])PRINTNL()
				FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfPlayerRules[iTask] -1)
					PRINTSTRING("sPlayerRuleData[")PRINTINT(i)PRINTSTRING("].iRule[")PRINTINT(iTask)PRINTSTRING("]             = ")PRINTSTRING(CONVERT_RULE_INT_TO_STRING_FOR_PRINTING(g_FMMC_STRUCT.sPlayerRuleData[i].iRule[iTask]))PRINTSTRING(" = [")PRINTINT(g_FMMC_STRUCT.sPlayerRuleData[i].iRule[iTask])PRINTSTRING("]")PRINTNL()
					PRINTSTRING("sPlayerRuleData[")PRINTINT(i)PRINTSTRING("].iPriorityi[")PRINTINT(iTask)PRINTSTRING("]        = ")PRINTINT(g_FMMC_STRUCT.sPlayerRuleData[i].iPriority[iTask])PRINTNL()
					PRINTSTRING("sPlayerRuleData[")PRINTINT(i)PRINTSTRING("].iPlayerRuleLimit[")PRINTINT(iTask)PRINTSTRING("]  = ")PRINTINT(g_FMMC_STRUCT.sPlayerRuleData[i].iPlayerRuleLimit[iTask])PRINTNL()
					PRINTSTRING("sPlayerRuleData[")PRINTINT(i)PRINTSTRING("].iBitset[")PRINTINT(iTask)PRINTSTRING("]  		   = ")PRINTINT(g_FMMC_STRUCT.sPlayerRuleData[i].iBitset[iTask])PRINTNL()
					PRINTNL()
				ENDFOR
			ENDIF
			PRINTNL()
		ENDFOR
		
		PRINTSTRING("------------------------Altitude Bar---------------------------")PRINTNL()
		FOR iTask = 0 TO FMMC_MAX_RULES-1
			FOR i = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
				PRINTLN("sFMMCEndConditions[",i,"].sAltitudeBar[",iTask,"].iBS					= ",  g_fmmc_struct.sFMMCEndConditions[i].sAltitudeBar[iTask].iBS					)
				PRINTLN("sFMMCEndConditions[",i,"].sAltitudeBar[",iTask,"].iMinimum				= ",  g_fmmc_struct.sFMMCEndConditions[i].sAltitudeBar[iTask].iMinimum				)
				PRINTLN("sFMMCEndConditions[",i,"].sAltitudeBar[",iTask,"].iThreshold			= ",  g_fmmc_struct.sFMMCEndConditions[i].sAltitudeBar[iTask].iThreshold			)
				PRINTLN("sFMMCEndConditions[",i,"].sAltitudeBar[",iTask,"].iMaximum				= ",  g_fmmc_struct.sFMMCEndConditions[i].sAltitudeBar[iTask].iMaximum				)
				PRINTLN("sFMMCEndConditions[",i,"].sAltitudeBar[",iTask,"].iSpecificVehicle		= ",  g_fmmc_struct.sFMMCEndConditions[i].sAltitudeBar[iTask].iSpecificVehicle		)
				PRINTLN("sFMMCEndConditions[",i,"].sAltitudeBar[",iTask,"].iFailTime			= ",  g_fmmc_struct.sFMMCEndConditions[i].sAltitudeBar[iTask].iFailTime				)
				PRINTLN("sFMMCEndConditions[",i,"].sAltitudeBar[",iTask,"].tl63_MainBar			= ",  g_fmmc_struct.sFMMCEndConditions[i].sAltitudeBar[iTask].tl63_MainBar			)
				PRINTLN("sFMMCEndConditions[",i,"].sAltitudeBar[",iTask,"].tl63_ConsequenceBar	= ",  g_fmmc_struct.sFMMCEndConditions[i].sAltitudeBar[iTask].tl63_ConsequenceBar	)
				PRINTLN("sFMMCEndConditions[",i,"].sAltitudeBar[",iTask,"].iBlipTime			= ",  g_fmmc_struct.sFMMCEndConditions[i].sAltitudeBar[iTask].iBlipTime				)
			ENDFOR
		ENDFOR
		
		PRINTSTRING("------------------------PLAY/LEAVE AREA BOUNDS---------------------------")PRINTNL()
		FOR iTask = 0 TO FMMC_MAX_RULES-1
			FOR i = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayAreaBitset, iTask)
				OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iLeaveAreaBitset, iTask)
				OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iSpawnAreaBitset, iTask)
					PRINTSTRING("BOUNDS team ")PRINTINT(i)PRINTSTRING(" rule ")PRINTINT(iTask)PRINTSTRING(" - Play Area = ")PRINTBOOL(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayAreaBitset, iTask))PRINTSTRING(", Leave Area = ")PRINTBOOL(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iLeaveAreaBitset, iTask))PRINTSTRING(", Spawn Area = ")PRINTBOOL(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iSpawnAreaBitset, iTask))PRINTNL()
					PRINTSTRING("sBoundsStruct.iType                       = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].iType)PRINTNL()
					PRINTSTRING("sBoundsStruct.vPos                        = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].vPos)PRINTNL()
					PRINTSTRING("sBoundsStruct.vPos1                       = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].vPos1)PRINTNL()
					PRINTSTRING("sBoundsStruct.vPos2                       = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].vPos2)PRINTNL()
					PRINTSTRING("sBoundsStruct.fWidth                      = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].fWidth)PRINTNL()
					PRINTSTRING("sBoundsStruct.fMaxHeight                  = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].fMaxHeight)PRINTNL()
					PRINTSTRING("sBoundsStruct.fMinHeight                  = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].fMinHeight)PRINTNL()
					PRINTSTRING("sBoundsStruct.fRadius                     = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].fRadius)PRINTNL()
					PRINTSTRING("sBoundsStruct.vHeadingVec                 = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].vHeadingVec)PRINTNL()
					PRINTSTRING("sBoundsStruct.MNIgnoreWantedVeh           = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].MNIgnoreWantedVeh))PRINTNL()
					PRINTSTRING("sBoundsStruct.iIgnoreWantedOutfit         = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].iIgnoreWantedOutfit)PRINTNL()
					PRINTSTRING("sBoundsStruct.iWantedToGive               = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].iWantedToGive)PRINTNL()
					PRINTSTRING("sBoundsStruct.iBoundsBS                   = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].iBoundsBS)PRINTNL()
					PRINTSTRING("sBoundsStruct.iPercentageLikelihoodToSpawnInFrontOfPlyr = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].iPercentageLikelihoodToSpawnInFrontOfPlyr)PRINTNL()
					PRINTSTRING("sBoundsStruct.iLinkedSpawnGroup           = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].iLinkedSpawnGroup)PRINTNL()
					PRINTSTRING("sBoundsStruct.iEntityType                 = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].iEntityType)PRINTNL()
					PRINTSTRING("sBoundsStruct.iEntityID                   = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].iEntityID)PRINTNL()
					PRINTSTRING("g_FMMC_STRUCT.sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iBoundsPriTimer[")PRINTINT(iTask)PRINTSTRING("] = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iBoundsPriTimer[iTask])PRINTNL()
					PRINTSTRING("sBoundsStruct.MNIgnoreLeaveAreaVeh        = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].MNIgnoreLeaveAreaVeh))PRINTNL()
					PRINTSTRING("sBoundsStruct.mnOnlyAffectVeh             = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[iTask].mnOnlyAffectVeh))PRINTNL()
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayArea2Bitset, iTask)
				OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iLeaveArea2Bitset, iTask)
				OR g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].iEntityType > 0
					PRINTSTRING("BOUNDS2 team ")PRINTINT(i)PRINTSTRING(" rule ")PRINTINT(iTask)PRINTSTRING(" - Play Area = ")PRINTBOOL(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayArea2Bitset, iTask))PRINTSTRING(", Leave Area = ")PRINTBOOL(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iLeaveArea2Bitset, iTask))PRINTNL()
					PRINTSTRING("sBoundsStruct2.iType                      = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].iType)PRINTNL()
					PRINTSTRING("sBoundsStruct2.vPos                       = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].vPos)PRINTNL()
					PRINTSTRING("sBoundsStruct2.vPos1                      = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].vPos1)PRINTNL()
					PRINTSTRING("sBoundsStruct2.vPos2                      = ")PRINTVECTOR(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].vPos2)PRINTNL()
					PRINTSTRING("sBoundsStruct2.fWidth                     = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].fWidth)PRINTNL()
					PRINTSTRING("sBoundsStruct2.fMaxHeight                 = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].fMaxHeight)PRINTNL()
					PRINTSTRING("sBoundsStruct2.fMinHeight                 = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].fMinHeight)PRINTNL()
					PRINTSTRING("sBoundsStruct2.fRadius                    = ")PRINTFLOAT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].fRadius)PRINTNL()
					PRINTSTRING("sBoundsStruct2.MNIgnoreWantedVeh          = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].MNIgnoreWantedVeh))PRINTNL()
					PRINTSTRING("sBoundsStruct2.iIgnoreWantedOutfit        = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].iIgnoreWantedOutfit)PRINTNL()
					PRINTSTRING("sBoundsStruct2.iWantedToGive              = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].iWantedToGive)PRINTNL()
					PRINTSTRING("sBoundsStruct2.iBoundsBS                  = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].iBoundsBS)PRINTNL()
					PRINTSTRING("sBoundsStruct2.iPercentageLikelihoodToSpawnInFrontOfPlyr = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].iPercentageLikelihoodToSpawnInFrontOfPlyr)PRINTNL()
					PRINTSTRING("sBoundsStruct2.iLinkedSpawnGroup          = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].iLinkedSpawnGroup)PRINTNL()
					PRINTSTRING("sBoundsStruct2.iEntityType                = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].iEntityType)PRINTNL()
					PRINTSTRING("sBoundsStruct2.iEntityID                  = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].iEntityID)PRINTNL()
					PRINTSTRING("g_FMMC_STRUCT.sFMMCEndConditions[")PRINTINT(i)PRINTSTRING("].iBoundsSecTimer[")PRINTINT(iTask)PRINTSTRING("] = ")PRINTINT(g_FMMC_STRUCT.sFMMCEndConditions[i].iBoundsSecTimer[iTask])PRINTNL()
					PRINTSTRING("sBoundsStruct2.MNIgnoreLeaveAreaVeh       = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].MNIgnoreLeaveAreaVeh))PRINTNL()
					PRINTSTRING("sBoundsStruct2.mnOnlyAffectVeh            = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[iTask].mnOnlyAffectVeh))PRINTNL()
				ENDIF
			ENDFOR		
		ENDFOR
		
		PRINTSTRING("------------------------COVER POINTS---------------------------")PRINTNL()
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfCoverPoints = 0
			PRINTSTRING("***** NO COVER POINTS WERE SET OPEN *****")PRINTNL()
			PRINTSTRING("iNumberOfCover = 0")PRINTNL()
		ELSE
			PRINTSTRING("iNumberOfCover[")PRINTINT(iTask)PRINTSTRING("]                     = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.iNumberOfCoverPoints)PRINTNL()
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfCoverPoints -1)
				PRINTSTRING("sPlacedCover[")PRINTINT(i)PRINTSTRING("].vPos                        = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos)PRINTNL()
				PRINTSTRING("sPlacedCover[")PRINTINT(i)PRINTSTRING("].fDirection                  = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].fDirection)PRINTNL()
				PRINTSTRING("sPlacedCover[")PRINTINT(i)PRINTSTRING("].iCoverUsage                 = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverUsage)PRINTNL()
				PRINTSTRING("sPlacedCover[")PRINTINT(i)PRINTSTRING("].iCoverHeight                = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverHeight)PRINTNL()
				PRINTSTRING("sPlacedCover[")PRINTINT(i)PRINTSTRING("].iCoverArc                   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverArc)PRINTNL()
			ENDFOR	
		ENDIF
		PRINTNL()
		PRINTNL()
		
		PRINTSTRING("------------------------OPEN DOORS---------------------------")PRINTNL()
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfDoors = 0
			PRINTSTRING("***** NO DOORS WERE SET OPEN *****")PRINTNL()
			PRINTSTRING("iNumberOfDoors = 0")PRINTNL()
		ELSE
			PRINTSTRING("iNumberOfDoors[")PRINTINT(iTask)PRINTSTRING("]                     = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.iNumberOfDoors)PRINTNL()
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDoors -1)
				IF i < FMMC_MAX_NUM_DOORS
					PRINTSTRING("sSelectedDoor[")PRINTINT(i)PRINTSTRING("].vPos                         = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].vPos)PRINTNL()
					PRINTSTRING("sSelectedDoor[")PRINTINT(i)PRINTSTRING("].iBitset                  	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].iBitset)PRINTNL()
					PRINTSTRING("sSelectedDoor[")PRINTINT(i)PRINTSTRING("].iContinuityId                  	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].iContinuityId)PRINTNL()
					PRINTSTRING("sSelectedDoor[")PRINTINT(i)PRINTSTRING("].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio                  	= ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio)PRINTNL()
					PRINTSTRING("sSelectedDoor[")PRINTINT(i)PRINTSTRING("].iLinkedElevator              = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].iLinkedElevator)PRINTNL()
					
					IF IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel)
					AND g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel != DUMMY_MODEL_FOR_SCRIPT					
						PRINTSTRING("sSelectedDoor[")PRINTINT(i)PRINTSTRING("].mn                          	= ")PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel))PRINTNL()
					ENDIF
				ENDIF
			ENDFOR	
		ENDIF
		PRINTNL()
		PRINTNL()
		
		PRINTSTRING("------------------------RANDOMISED SPAWN GROUPS---------------------------")PRINTNL()
		IF g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber = 0
			PRINTSTRING("***** NO SPAWN GROUPS *****")PRINTNL()
			PRINTSTRING("iRandomEntitySpawn_MaxNumber = 0")PRINTNL()
		ELSE
			PRINTSTRING("iRandomEntitySpawn_MaxNumber			= ")PRINTINT(g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber)PRINTNL()
			PRINTSTRING("iRandomEntitySpawn_RandomNumbersToUse	= ")PRINTINT(g_FMMC_STRUCT.iRandomEntitySpawn_RandomNumbersToUse)PRINTNL()
			PRINTSTRING("iRandomEntitySpawn_Bitset				= ")PRINTINT(g_FMMC_STRUCT.iRandomEntitySpawn_Bitset)PRINTNL()
			
			FOR i = 0 TO (g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber - 1)
				IF i < ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS // Safety, this should never happen
					PRINTSTRING("eRandomEntitySpawn_DefineActivation[")PRINTINT(i)PRINTSTRING("]		= ")PRINTSTRING(GET_SPAWN_GROUP_DEFINE_ACTIVATION_NAME_FOR_PRINT(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[i]))PRINTNL()
					PRINTSTRING("iRandomEntitySpawnSub_MaxNumber[")PRINTINT(i)PRINTSTRING("]			= ")PRINTINT(g_FMMC_STRUCT.iRandomEntitySpawnSub_MaxNumber[i])PRINTNL()
					PRINTSTRING("iRandomEntitySpawnSub_RandomNumbersToUse[")PRINTINT(i)PRINTSTRING("]	= ")PRINTINT(g_FMMC_STRUCT.iRandomEntitySpawnSub_RandomNumbersToUse[i])PRINTNL()
					PRINTSTRING("iRandomEntitySpawnSub_Bitset[")PRINTINT(i)PRINTSTRING("]				= ")PRINTINT(g_FMMC_STRUCT.iRandomEntitySpawnSub_Bitset[i])PRINTNL()					
					
					FOR ii = 0 TO (ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS - 1)
						PRINTSTRING("eRandomEntitySpawnSub_DefineActivation[")PRINTINT(i)PRINTSTRING("][")PRINTINT(ii)PRINTSTRING("]	= ")PRINTSTRING(GET_SPAWN_GROUP_DEFINE_ACTIVATION_NAME_FOR_PRINT(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[i][ii]))PRINTNL()
					ENDFOR
				ENDIF
			ENDFOR
		ENDIF
		PRINTNL()
		PRINTNL()
	ENDIF
		
	PRINTSTRING("------------------------SPAWN POINTS---------------------------")PRINTNL()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints = 0
		PRINTSTRING("***** NO SPAWN POINTS WERE MADE OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfSpawnPoints = 0")PRINTNL()
	ELSE
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints -1)
			IF i < FMMC_MAX_SPAWNPOINTS
				PRINTSTRING("sPlacedSpawnPoint[")PRINTINT(i)PRINTSTRING("].vPos                         		= ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos)PRINTNL()
				PRINTSTRING("sPlacedSpawnPoint[")PRINTINT(i)PRINTSTRING("].fHead                        		= ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].fHead)PRINTNL()
				PRINTSTRING("sPlacedSpawnPoint[")PRINTINT(i)PRINTSTRING("].iTeam[")PRINTINT(0)PRINTSTRING("]    = ")PRINTSTRING(CONVERT_TEAM_INT_TO_STRING_FOR_PRINTING_DM(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].iTeam))PRINTSTRING(" = [")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].iTeam)PRINTSTRING("]")PRINTNL()
				PRINTSTRING("sPlacedSpawnPoint[")PRINTINT(i)PRINTSTRING("].iTeamRestrictedBS					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].iTeamRestrictedBS)PRINTNL()
				PRINTSTRING("sPlacedSpawnPoint[")PRINTINT(i)PRINTSTRING("].iModifierRestrictedBS				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].iModifierRestrictedBS)PRINTNL()
			ENDIF
		ENDFOR	
	ENDIF
	PRINTNL()
	PRINTNL()
		
	PRINTSTRING("------------------------TEAM START POINTS---------------------------")PRINTNL()
	FOR iTask = 0 TO FMMC_MAX_TEAMS - 1
		PRINTSTRING("iNumberOfTeamSpawnPoints[")PRINTINT(iTask)PRINTSTRING("]                        = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTask])PRINTNL()
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTask] = 0
			PRINTSTRING("***** NO TEAM POINTS WERE MADE OR UPLOADED *****")PRINTNL()
		ELSE
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTask] -1)
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].vPos                         = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].vPos)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].fHead                        = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].fHead)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iSpawnBitSet                 = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iSpawnBitSet)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iValidForRules_Start         = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iValidForRules_Start)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iValidForRules_End           = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iValidForRules_End)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iSpawnPointQuantity          = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iSpawnPointQuantity)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iActivationRadius            = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iActivationRadius)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iActivationTeam              = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iActivationTeam)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iTeamRestrictedBS            = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iTeamRestrictedBS)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iLinkedContinuityEntityType  = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iLinkedContinuityEntityType)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iLinkedContinuityID          = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iLinkedContinuityID)PRINTNL()
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iSpawnBitset, ci_SpawnBS_Initial_Start_Spawn_Point)
					PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("] Flagged as Start Point.")
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iSpawnBitset, ci_SpawnBS_Treat_As_Respawn_Point)
					PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("] Flagged as Respawn Point.")
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_0)
					PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("] Flagged as Restart/Check Point 0.")
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_1)
					PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("] Flagged as Restart/Check Point 1.")
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_2)
					PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("] Flagged as Restart/Check Point 2.")
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_3)
					PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("] Flagged as Restart/Check Point 3.")
				ENDIF
				
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iMoveToEntityType          = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iMoveToEntityType)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iMoveToEntityIndex          = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iMoveToEntityIndex)PRINTNL()
				
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iVehicle    			    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iVehicle)PRINTNL()
				PRINTSTRING("sTDMSpawnPoints[")PRINTINT(iTask)PRINTSTRING("][")PRINTINT(i)PRINTSTRING("].iSeat       			    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iSeat)PRINTNL()					
				
				PRINTLN("sTDMSpawnPoints[",iTask,"][",i,"].vSpawnPositionSeparateVehicle   = ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].vSpawnPositionSeparateVehicle)
				PRINTLN("sTDMSpawnPoints[",iTask,"][",i,"].fSpawnHeadingSeparateVehicle   = ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].fSpawnHeadingSeparateVehicle)
				
				INT iFlag = 0
				FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
					PRINTLN("sTDMSpawnPoints[", iTask, "][", i,"].eSpawnConditionFlag[", iFlag,"] = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].eSpawnConditionFlag[iFlag]))
				ENDFOR
				
				FOR ii = 0 TO ciAltVarsBS_MAX-1
					PRINTLN("[AltVarsSystemSpam] - sTDMSpawnPoints[", iTask, "][", i, "].sTDMSpawnPoints[", ii, "].iAltVarsBitsetSpawnPoints = ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iAltVarsBitsetSpawnPoints[ii])
				ENDFOR
			
				PRINTLN("[AltVarsSystemSpam] - sTDMSpawnPoints[", iTask, "][", i, "].sTDMSpawnPoints[", ii, "].iSpawnGroup = ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iSpawnGroup)
				FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
					PRINTLN("[AltVarsSystemSpam] - sTDMSpawnPoints[", iTask, "][", i, "].sTDMSpawnPoints[", ii, "].iSubSpawnGroupBS = ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iSpawnSubGroupBS[ii])
				ENDFOR			
			
				FOR ii = 0 TO ciFMMC_MAX_TEAM_SPAWN_POINT_BS-1
					PRINTLN("[AltVarsSystemSpam] - sTDMSpawnPoints[", iTask, "][", i, "].sTDMSpawnPoints[", ii, "].iLinkedActivationSpawnPoints = ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTask][i].iLinkedActivationSpawnPoints[ii])
				ENDFOR
			ENDFOR	
		ENDIF
	ENDFOR
	
	PRINTNL()
	PRINTNL()
	
	
	PRINTLN("--- Custom Strings ---")
	FOR i = 0 TO ciMAX_CUSTOM_STRING_LIST_LENGTH-1
		PRINTLN("tlCustomStringList[", i, "] = ", g_FMMC_STRUCT.tlCustomStringList[i])
	ENDFOR	
	
	PRINTLN("--- Custom Variables ---")
	PRINTLN("--- Custom Variables | Player Numbers ---")
	// Systems
	FOR i = 0 TO ciMAX_CUSTOM_VARIABLE_LIST__PLAYER_NUMBERS_PRESETS-1
		FOR ii = 0 TO CUSTOM_VARIABLE_LIST__SAVED_PLAYER_NUMBER_VALUES_MAX-1
			PRINTLN("sCustomVariables.sPlayerNumbers[", i, "].sData[", ii, "].iReturnValue 					= ", g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[i].sData[ii].iReturnValue)
			PRINTLN("sCustomVariables.sPlayerNumbers[", i, "].sData[", ii, "].iComparisonValue 				= ", g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[i].sData[ii].iComparisonValue)
			PRINTLN("sCustomVariables.sPlayerNumbers[", i, "].sData[", ii, "].iComparisonType 				= ", g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[i].sData[ii].iComparisonType)
			PRINTLN("sCustomVariables.sPlayerNumbers[", i, "].sData[", ii, "].iDifficultyLevel 				= ", g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[i].sData[ii].iDifficultyLevel)
		ENDFOR
	ENDFOR
	PRINTLN("--- Custom Variables | Entity Respawns ---")
	FOR i = 0 TO ciMAX_CUSTOM_VARIABLE_LIST__ENTITY_RESPAWNED_COUNT_PRESETS-1
		FOR ii = 0 TO CUSTOM_VARIABLE_LIST__SAVED_ENTITY_RESPAWNED_VALUES_MAX-1
			PRINTLN("sCustomVariables.sEntityRespawnCount[", i, "].sData[", ii, "].iReturnValue 			= ", g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[i].sData[ii].iReturnValue)
			PRINTLN("sCustomVariables.sEntityRespawnCount[", i, "].sData[", ii, "].iComparisonValue 		= ", g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[i].sData[ii].iComparisonValue)
			PRINTLN("sCustomVariables.sEntityRespawnCount[", i, "].sData[", ii, "].iComparisonType 			= ", g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[i].sData[ii].iComparisonType)
			PRINTLN("sCustomVariables.sEntityRespawnCount[", i, "].sData[", ii, "].iDifficultyLevel 			= ", g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[i].sData[ii].iDifficultyLevel)
		ENDFOR
	ENDFOR
	PRINTLN("--- Custom Variables | Rule Timer Elapsed ---")
	FOR i = 0 TO ciMAX_CUSTOM_VARIABLE_LIST__RULE_TIME_ELAPSED_PRESETS-1
		FOR ii = 0 TO CUSTOM_VARIABLE_LIST__SAVED_RULE_TIME_ELAPSED_MAX-1
			PRINTLN("sCustomVariables.sRuleTimeElapsed[", i, "].sData[", ii, "].iReturnValue 				= ", g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[i].sData[ii].iReturnValue)
			PRINTLN("sCustomVariables.sRuleTimeElapsed[", i, "].sData[", ii, "].iComparisonValue 			= ", g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[i].sData[ii].iComparisonValue)
			PRINTLN("sCustomVariables.sRuleTimeElapsed[", i, "].sData[", ii, "].iComparisonType 			= ", g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[i].sData[ii].iComparisonType)
			PRINTLN("sCustomVariables.sRuleTimeElapsed[", i, "].sData[", ii, "].iDifficultyLevel 			= ", g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[i].sData[ii].iDifficultyLevel)
		ENDFOR
	ENDFOR
	PRINTLN("--- Custom Variables | Difficulty Level ---")
	FOR i = 0 TO ciMAX_CUSTOM_VARIABLE_LIST__DIFFICULTY_LEVEL_PRESETS-1
		FOR ii = 0 TO CUSTOM_VARIABLE_LIST__SAVED_DIFFICULTY_LEVEL_MAX-1
			PRINTLN("sCustomVariables.sDifficultyLevel[", i, "].sData[", ii, "].iReturnValue 				= ", g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[i].sData[ii].iReturnValue)
			PRINTLN("sCustomVariables.sDifficultyLevel[", i, "].sData[", ii, "].iComparisonValue 			= ", g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[i].sData[ii].iComparisonValue)
			PRINTLN("sCustomVariables.sDifficultyLevel[", i, "].sData[", ii, "].iComparisonType 			= ", g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[i].sData[ii].iComparisonType)
			PRINTLN("sCustomVariables.sDifficultyLevel[", i, "].sData[", ii, "].iDifficultyLevel 			= ", g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[i].sData[ii].iDifficultyLevel)
		ENDFOR
	ENDFOR
	// Pools
	FOR i = 0 TO ciMAX_CUSTOM_VARIABLE_POOL__WEAPON_MAX-1		
		PRINTLN("sCustomVariables.sWeaponPool[", i, "].wt_Weapon = ", GET_WEAPON_NAME(g_FMMC_STRUCT.sCustomVariables.sWeaponPool[i].wt_Weapon))
	ENDFOR
	// ---
	
	// --- Pool GOTOs ---
	PRINTLN("sPlacedPed[", i, "] --- Ped GOTOs ---")
	FOR i = 0 TO ASSOCIATED_GOTO_TASK_POOL-1
		INT j					
		FOR j = 0 TO ciENTIRE_TASK_BITSET_MAX-1
			IF IS_BIT_SET(g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.iEntireTaskBitset[j / 32], j % 32)
				PRINTLN("sAssociatedGotoTaskData.iEntireBitset Type (", j, "): ", GET_DEBUG_NAME_FOR_GOTO_ENTIRE_BITSET_TYPE(j), " - ENABLED")
			ENDIF
		ENDFOR
		PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.iNumberOfPedGotoTasks								= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.iNumberOfPedGotoTasks)					
		PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.iArrivalRadius									= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.iArrivalRadius)
		PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.fPedAccelerationCap								= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.fPedAccelerationCap)
		PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.iLoopGotoStartIndex								= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.iLoopGotoStartIndex)
		PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.iDetachCargobobOnGoto								= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.iDetachCargobobOnGoto)
		FOR ii = 0 TO MAX_ASSOCIATED_GOTO_TASKS-1
			FOR j = 0 TO PED_ASSOCIATED_GOTO_TASK_TYPE_MAX-1												
				IF IS_BIT_SET(g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].iBitset[j / 32], j % 32)
					PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iBitset Action Type (", j, "): ", GET_DEBUG_NAME_FOR_GOTO_TASK_TYPE(j), " - ENABLED")
				ENDIF
			ENDFOR
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].vPosition					= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].vPosition)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].fPedAchieveFinalHeading		= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].fPedAchieveFinalHeading)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iWaitTime					= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].iWaitTime)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iAssociatedRule				= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].iAssociatedRule)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].vPlayerActivated_Pos			= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].vPlayerActivated_Pos)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iPlayerActivated_Radius		= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].iPlayerActivated_Radius)						
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].fCustomArrivalHeight			= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].fCustomArrivalHeight)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].fCustomArrivalRadius			= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].fCustomArrivalRadius)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iVehicleGotoSpeed			= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].iVehicleGotoSpeed)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].fCustomVehArriveRadius		= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].fCustomVehArriveRadius)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iChosenVehicle				= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].iChosenVehicle)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iCompleteRuleObjectiveBS		= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].iCompleteRuleObjectiveBS)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iLoopOnRuleBS				= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].iLoopOnRuleBS)						
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].eCustomScenarioType			= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].eCustomScenarioType)
			INT iCustomScenarioInts = 0
			FOR iCustomScenarioInts = 0 TO FMMC_PED_CUSTOM_SCENARIO_GENERIC_INTS - 1
				PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iCustomScenarioInts[", iCustomScenarioInts, "]		= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].iCustomScenarioInts[iCustomScenarioInts])
			ENDFOR
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iCustomScenarioSkipCondition								= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].iCustomScenarioSkipCondition)
			PRINTLN("sAssociatedGotoPool[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iCustomScenarioSkipData									= ", g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData.sPointData[ii].iCustomScenarioSkipData)
			PRINTLN("---")
		ENDFOR
	ENDFOR
	// ---
	
	PRINTSTRING("------------------------SAS Presets---------------------------")PRINTNL()
	FOR i = 0 TO (ciPED_SAS_PRESET_MAX-1)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].iPresetIndex  					= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].iPresetIndex							)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].iBS  							= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].iBS									)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].iRuleFrom					  	= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].iRuleFrom								)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].iRuleTo						  	= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].iRuleTo								)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].fDetectionRange				  	= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].fDetectionRange						)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].fLineOfSightConeAngle		  	= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].fLineOfSightConeAngle					)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].fLineOfSightConeHeightMax	  	= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].fLineOfSightConeHeightMax				)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].fLineOfSightConeHeightMin	  	= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].fLineOfSightConeHeightMin				)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].fInstantDetectionRange		  	= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].fInstantDetectionRange					)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].fInstantLineOfSightConeAngle  	= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].fInstantLineOfSightConeAngle			)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].fInstantLineOfSightConeHeightMax = ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].fInstantLineOfSightConeHeightMax		)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].fInstantLineOfSightConeHeightMin = ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].fInstantLineOfSightConeHeightMin		)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].iStealthAggroDelay			  	= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].iStealthAggroDelay						)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[", i, "].fWeaponUsageRespondingDistance	= ", g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i].fWeaponUsageRespondingDistance			)
	ENDFOR
	
	PRINTSTRING("------------------------PEDS---------------------------")PRINTNL()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds = 0
		PRINTSTRING("***** NO PEDS WERE MADE OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfPeds = 0")PRINTNL()
	ELSE
		
		PRINTLN("g_FMMC_STRUCT_ENTITIES.iPedSpawnCap                         = ", g_FMMC_STRUCT_ENTITIES.iPedSpawnCap)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.iNumberOfPeds                        = ", g_FMMC_STRUCT_ENTITIES.iNumberOfPeds)
		PRINTLN("g_FMMC_STRUCT_ENTITIES.iPedSpawnCapOverRide			 	 = ", g_FMMC_STRUCT_ENTITIES.iPedSpawnCapOverRide)		
		
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds -1)
			IF i < FMMC_MAX_PEDS
				PRINTLN("sPlacedPed[", i, "].vPos                         = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos)
				PRINTLN("sPlacedPed[", i, "].fHead                        = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fHead)
				PRINTLN("sPlacedPed[", i, "].fRange                       = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fRange)				
				PRINTLN("sPlacedPed[", i, "].fRespawnMinRange             = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fRespawnMinRange)
				
				IF IS_THIS_A_MISSION()
				OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
					PRINTLN("sPlacedPed[", i, "].fRespawnRange                = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fRespawnRange)
					PRINTLN("sPlacedPed[", i, "].fPedRespawnRangePlayerSight  = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fPedRespawnRangePlayerSight)					
					PRINTLN("sPlacedPed[", i, "].fPedRespawnRangePlayerNear  = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fPedRespawnRangePlayerNear)					
					PRINTLN("sPlacedPed[", i, "].iPedRespawnLives             = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedRespawnLives)
					PRINTLN("sPlacedPed[", i, "].iFlee                        = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFlee)
					PRINTLN("sPlacedPed[", i, "].iCombatStyle                 = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCombatStyle)
					PRINTLN("sPlacedPed[", i, "].iAccuracy                    = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAccuracy)
					PRINTLN("sPlacedPed[", i, "].iCustomPedName               = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCustomPedName)
					
					PRINTLN("sPlacedPed[", i, "].iAssociatedObjective		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective)
					PRINTLN("sPlacedPed[", i, "].iAssociatedTeam			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam)
					PRINTLN("sPlacedPed[", i, "].iAssociatedAction			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAction)
					PRINTLN("sPlacedPed[", i, "].iAssociatedSpawn			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedSpawn)
					PRINTLN("sPlacedPed[", i, "].iSecondAssociatedObjective	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedObjective)
					PRINTLN("sPlacedPed[", i, "].iSecondAssociatedTeam		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedTeam)
					PRINTLN("sPlacedPed[", i, "].iSecondAssociatedSpawn		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedSpawn)
					PRINTLN("sPlacedPed[", i, "].iThirdAssociatedObjective	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedObjective)
					PRINTLN("sPlacedPed[", i, "].iThirdAssociatedTeam		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedTeam)
					PRINTLN("sPlacedPed[", i, "].iThirdAssociatedSpawn		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedSpawn)
					PRINTLN("sPlacedPed[", i, "].iFourthAssociatedObjective	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedObjective)
					PRINTLN("sPlacedPed[", i, "].iFourthAssociatedTeam		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedTeam)
					PRINTLN("sPlacedPed[", i, "].iFourthAssociatedSpawn		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedSpawn)
					PRINTLN("sPlacedPed[", i, "].iRespawnDelay				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRespawnDelay)										
					PRINTLN("sPlacedPed[", i, "].iForcedVehicleSeat			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iForcedVehicleSeat)
					PRINTLN("sPlacedPed[", i, "].iAggroIndexBS_Entity_Ped	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAggroIndexBS_Entity_Ped)
					
					PRINTLN("sPlacedPed[", i, "].iSpawnGroup							= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpawnGroup)
					PRINTLN("sPlacedPed[", i, "].iModifySpawnGroupOnEvent			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iModifySpawnGroupOnEvent)
					FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1					
						PRINTLN("sPlacedPed[", i, "].iSpawnSubGroupBS[", ii, "]								= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpawnSubGroupBS[ii])
						PRINTLN("sPlacedPed[", i, "].iModifySpawnSubGroupOnEventBS[", ii, "]			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iModifySpawnSubGroupOnEventBS[ii])
					ENDFOR
					PRINTLN("sPlacedPed[", i, "].iSpawnGroupAdditionalFunctionalityBS							= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpawnGroupAdditionalFunctionalityBS)
					
					PRINTLN("sPlacedPed[", i, "].iNewRelationshipGroupOnRule						= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iNewRelationshipGroupOnRule)
					PRINTLN("sPlacedPed[", i, "].iNewRelationshipGroupType							= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iNewRelationshipGroupType)
					
					PRINTLN("sPlacedPed[", i, "].iDialogueTriggerBlock								= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDialogueTriggerBlock)
					PRINTLN("sPlacedPed[", i, "].iObjectivePrereq									= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iObjectivePrereq)

					PRINTLN("sPlacedPed[", i, "].iBlipColourRelGroupFriendly						= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iBlipColourRelGroupFriendly)
					PRINTLN("sPlacedPed[", i, "].iBlipColourRelGroupNeutral							= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iBlipColourRelGroupNeutral)
					PRINTLN("sPlacedPed[", i, "].iBlipColourRelGroupHostile							= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iBlipColourRelGroupHostile)
					
					PRINTLN("sPlacedPed[", i, "].fShowBlipHeightIndicatorRange						= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fShowBlipHeightIndicatorRange)
										
					PRINTLN("sPlacedPed[", i, "].iStopTasksTeam				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopTasksTeam)
					PRINTLN("sPlacedPed[", i, "].iStopTasksRule				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopTasksRule)
					
					PRINTLN("sPlacedPed[", i, "].iSecondaryAction			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondaryAction)
					PRINTLN("sPlacedPed[", i, "].iSecondaryTeam				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondaryTeam)
					PRINTLN("sPlacedPed[", i, "].iSecondaryObjective		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondaryObjective)
					PRINTLN("sPlacedPed[", i, "].iSecondaryActionStart		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondaryActionStart)
					PRINTLN("sPlacedPed[", i, "].vSecondaryVector			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vSecondaryVector)
					
					PRINTLN("sPlacedPed[", i, "].iSecondaryTargetOverrideType				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondaryTargetOverrideType)
					PRINTLN("sPlacedPed[", i, "].iSecondaryTargetOverrideID					= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondaryTargetOverrideID)
					PRINTLN("sPlacedPed[", i, "].iSecondaryActionTriggerDistanceFromTarget	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondaryActionTriggerDistanceFromTarget)
					
					PRINTLN("sPlacedPed[", i, "].iPrimaryTargetOverrideType					= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPrimaryTargetOverrideType)
					PRINTLN("sPlacedPed[", i, "].iPrimaryTargetOverrideID					= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPrimaryTargetOverrideID)
					PRINTLN("sPlacedPed[", i, "].iPrimaryActionTriggerDistanceFromTarget	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPrimaryActionTriggerDistanceFromTarget)
					
					PRINTLN("sPlacedPed[", i, "].iAgroFailPriority[0]			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAgroFailPriority[0])
					PRINTLN("sPlacedPed[", i, "].iAgroFailPriority[1]			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAgroFailPriority[1])
					PRINTLN("sPlacedPed[", i, "].iAgroFailPriority[2]			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAgroFailPriority[2])
					PRINTLN("sPlacedPed[", i, "].iAgroFailPriority[3]			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAgroFailPriority[3])
					
					PRINTLN("sPlacedPed[", i, "].iIdleAnim       	         	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iIdleAnim)
					PRINTLN("sPlacedPed[", i, "].iIdleAnimTeam      	     	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iIdleAnimTeam)
					PRINTLN("sPlacedPed[", i, "].iIdleAnimRule     	         	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iIdleAnimRule)
					PRINTLN("sPlacedPed[", i, "].iSpecialAnim     	         	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpecialAnim)
					PRINTLN("sPlacedPed[", i, "].iSpecialAnimTeam     	     	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpecialAnimTeam)
					PRINTLN("sPlacedPed[", i, "].iSpecialAnimRule     	     	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpecialAnimRule)
					PRINTLN("sPlacedPed[", i, "].fSpecialAnimTriggerRange     	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fSpecialAnimTriggerRange)
					PRINTLN("sPlacedPed[", i, "].iSpecialAnimTriggerTeamBitset	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpecialAnimTriggerTeamBitset)										
					PRINTLN("sPlacedPed[", i, "].iIdleAnimTriggerFriendliesCount		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iIdleAnimTriggerFriendliesCount)
					PRINTLN("sPlacedPed[", i, "].fIdleAnimTriggerFriendliesRange		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fIdleAnimTriggerFriendliesRange)
					PRINTLN("sPlacedPed[", i, "].iSpecialAnimTriggerFriendliesCount		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpecialAnimTriggerFriendliesCount)
					PRINTLN("sPlacedPed[", i, "].fSpecialAnimTriggerFriendliesRange		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fSpecialAnimTriggerFriendliesRange)
					PRINTLN("sPlacedPed[", i, "].iTaskTriggerFriendliesCount			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iTaskTriggerFriendliesCount)
					PRINTLN("sPlacedPed[", i, "].fTaskTriggerFriendliesRange			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fTaskTriggerFriendliesRange)
					PRINTLN("sPlacedPed[", i, "].fAnimBreakoutRange				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fAnimBreakoutRange)
					PRINTLN("sPlacedPed[", i, "].fAnimSpecialBreakoutRange		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fAnimSpecialBreakoutRange)
					
					PRINTLN("sPlacedPed[", i, "].iAnimBreakoutTime				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAnimBreakoutTime)
					PRINTLN("sPlacedPed[", i, "].iAnimSpecialBreakoutTime		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAnimSpecialBreakoutTime)
					
					PRINTLN("sPlacedPed[", i, "].iInventoryBS	       	     	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iInventoryBS)
					PRINTLN("sPlacedPed[", i, "].iGunOnRuleTeam	       	     	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iGunOnRuleTeam)
					PRINTLN("sPlacedPed[", i, "].iGunOnRuleRule	       	     	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iGunOnRuleRule)
					PRINTLN("sPlacedPed[", i, "].wtGunOnRule		       	 	= ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].wtGunOnRule))
					
					PRINTLN("sPlacedPed[", i, "].fCombatMinimumDist		       	 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fCombatMinimumDist)
					
					PRINTLN("sPlacedPed[", i, "].iWepHolstered       	     	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iWepHolstered)
					PRINTLN("sPlacedPed[", i, "].iDialogueTeam		     	 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDialogueTeam)
					PRINTLN("sPlacedPed[", i, "].iDialogueObjective	     	 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDialogueObjective)
					PRINTLN("sPlacedPed[", i, "].iDialogueTiming		     	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDialogueTiming)
					PRINTLN("sPlacedPed[", i, "].iDialogueLine		     	 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDialogueLine)
					PRINTLN("sPlacedPed[", i, "].iDialogueRange		     	 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDialogueRange)
					PRINTLN("sPlacedPed[", i, "].iAggroTime		     	 	 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAggroTime)
					PRINTLN("sPlacedPed[", i, "].iVehicle		     	 	 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle)
					PRINTLN("sPlacedPed[", i, "].iVehicleSeat		     	 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat)
					PRINTLN("sPlacedPed[", i, "].iModelVariation		     	 = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iModelVariation)
					PRINTLN("sPlacedPed[", i, "].iAssociatedActionStart	     	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedActionStart)
					PRINTLN("sPlacedPed[", i, "].iAssociatedScoreRequired	   	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedScoreRequired)
					PRINTLN("sPlacedPed[", i, "].iAssociatedAlwaysForceSpawnOnRule   = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAlwaysForceSpawnOnRule)
					PRINTLN("sPlacedPed[", i, "].iAssociatedObjectiveEnd     = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjectiveEnd)
					PRINTLN("sPlacedPed[", i, "].iActivationRange		     	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iActivationRange)
					PRINTLN("sPlacedPed[", i, "].iDriveSpeed				 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDriveSpeed)
					PRINTLN("sPlacedPed[", i, "].iRollingStart				 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRollingStart)
					PRINTLN("sPlacedPed[", i, "].iAssociatedActionStart		 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedActionStart)	
										
					// --- Ped GOTOs ---
					PRINTLN("sPlacedPed[", i, "] --- Ped GOTOs ---")
					INT j					
					FOR j = 0 TO ciENTIRE_TASK_BITSET_MAX-1
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.iEntireTaskBitset[j / 32], j % 32)
							PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.iEntireBitset Type (", j, "): ", GET_DEBUG_NAME_FOR_GOTO_ENTIRE_BITSET_TYPE(j), " - ENABLED")
						ENDIF
					ENDFOR					
					PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.iNumberOfPedGotoTasks								= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.iNumberOfPedGotoTasks)					
					PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.iArrivalRadius										= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.iArrivalRadius)
					PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.fPedAccelerationCap								= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.fPedAccelerationCap)
					PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.iLoopGotoStartIndex								= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.iLoopGotoStartIndex)
					PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.iDetachCargobobOnGoto								= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.iDetachCargobobOnGoto)
					
					FOR ii = 0 TO MAX_ASSOCIATED_GOTO_TASKS-1
						FOR j = 0 TO PED_ASSOCIATED_GOTO_TASK_TYPE_MAX-1
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].iBitset[j / 32], j % 32)
								PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iBitset Action Type (", j, "): ", GET_DEBUG_NAME_FOR_GOTO_TASK_TYPE(j), " - ENABLED")
							ENDIF
						ENDFOR
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].vPosition						= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].vPosition)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].fPedAchieveFinalHeading		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].fPedAchieveFinalHeading)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iWaitTime						= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].iWaitTime)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iAssociatedRule				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].iAssociatedRule)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].vPlayerActivated_Pos			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].vPlayerActivated_Pos)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iPlayerActivated_Radius		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].iPlayerActivated_Radius)						
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].fCustomArrivalHeight			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].fCustomArrivalHeight)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].fCustomArrivalRadius			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].fCustomArrivalRadius)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iVehicleGotoSpeed				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].iVehicleGotoSpeed)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].fCustomVehArriveRadius		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].fCustomVehArriveRadius)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iChosenVehicle				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].iChosenVehicle)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iCompleteRuleObjectiveBS		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].iCompleteRuleObjectiveBS)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iLoopOnRuleBS					= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].iLoopOnRuleBS)						
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].eCustomScenarioType			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].eCustomScenarioType)
						INT iCustomScenarioInts = 0
						FOR iCustomScenarioInts = 0 TO FMMC_PED_CUSTOM_SCENARIO_GENERIC_INTS - 1
							PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iCustomScenarioInts[", iCustomScenarioInts, "]		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].iCustomScenarioInts[iCustomScenarioInts])
						ENDFOR
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iCustomScenarioSkipCondition								= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].iCustomScenarioSkipCondition)
						PRINTLN("sPlacedPed[", i, "].sAssociatedGotoTaskData.sPointData[", ii, "].iCustomScenarioSkipData									= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData.sPointData[ii].iCustomScenarioSkipData)
						PRINTLN("---")
					ENDFOR
					// ---
					
					PRINTLN("sPlacedPed[", i, "].mn                           	= ", GET_MODEL_STING_FROM_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn)," [", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn), " ]")
					PRINTLN("sPlacedPed[", i, "].iCleanupTeam                 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCleanupTeam)
					PRINTLN("sPlacedPed[", i, "].iCleanupObjective            	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCleanupObjective)
					PRINTLN("sPlacedPed[", i, "].iCleanupRange            		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCleanupRange)
					PRINTLN("sPlacedPed[", i, "].iFollowTeamOnRuleBS[0]       	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFollowTeamOnRuleBS[0])
					PRINTLN("sPlacedPed[", i, "].iFollowTeamOnRuleBS[1]       	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFollowTeamOnRuleBS[1])
					PRINTLN("sPlacedPed[", i, "].iFollowTeamOnRuleBS[2]       	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFollowTeamOnRuleBS[2])
					PRINTLN("sPlacedPed[", i, "].iFollowTeamOnRuleBS[3]       	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFollowTeamOnRuleBS[3])
					PRINTLN("sPlacedPed[", i, "].iPedBitset       				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitset)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetTwo     				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwo)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetThree   				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetThree)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetFour    				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFour)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetFive    				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetSix     				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSix)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetSeven   				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSeven)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetEight   				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetEight)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetNine   				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetNine)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetTen   				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTen)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetEleven  				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetEleven)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetTwelve  				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwelve)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetThirteen  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetThirteen)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetFourteen  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFourteen)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetFifteen  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFifteen)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetSixteen  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSixteen)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetSeventeen  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetSeventeen)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetEighteen  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetEighteen)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetNineteen  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetNineteen)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetTwenty	  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwenty)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetTwentyOne	  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwentyOne)
					PRINTLN("sPlacedPed[", i, "].iPedBitsetTwentyTwo	  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwentyTwo)
										
					PRINTLN("sPlacedPed[", i, "].iPedTargetForSpeedMatching  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedTargetForSpeedMatching)
					PRINTLN("sPlacedPed[", i, "].iPedTargetForSpeedMatching2  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedTargetForSpeedMatching2)
					PRINTLN("sPlacedPed[", i, "].fDistanceThreshForSpeedMatching  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fDistanceThreshForSpeedMatching)
					
					PRINTLN("sPlacedPed[", i, "].sTrainAttachmentData.iTrainIndex			 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sTrainAttachmentData.iTrainIndex		)
					PRINTLN("sPlacedPed[", i, "].sTrainAttachmentData.iCarriageIndex  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sTrainAttachmentData.iCarriageIndex  	)
					PRINTLN("sPlacedPed[", i, "].sTrainAttachmentData.vCarriageOffsetPosition  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sTrainAttachmentData.vCarriageOffsetPosition)
					PRINTLN("sPlacedPed[", i, "].sTrainAttachmentData.vCarriageOffsetRotation  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sTrainAttachmentData.vCarriageOffsetRotation)
					
					PRINTLN("sPlacedPed[", i, "].iHackFailsForAggro  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iHackFailsForAggro)
					PRINTLN("sPlacedPed[", i, "].iAssociatedAggroZone  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAggroZone)				
					PRINTLN("sPlacedPed[", i, "].fExplosionPerceptionRadius 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fExplosionPerceptionRadius)
					PRINTLN("sPlacedPed[", i, "].iPedAggroWhenCCTVCamsDestroyed = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedAggroWhenCCTVCamsDestroyed)
					PRINTLN("sPlacedPed[", i, "].iPedPointsToGive  				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedPointsToGive)
					PRINTLN("sPlacedPed[", i, "].iPedGivePointsOnRuleStart  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedGivePointsOnRuleStart)
					PRINTLN("sPlacedPed[", i, "].iPedGivePointsOnRuleEnd  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedGivePointsOnRuleEnd)
					PRINTLN("sPlacedPed[", i, "].iZoneBlockEntitySpawn  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawn)
					PRINTLN("sPlacedPed[", i, "].iZoneBlockEntitySpawnPlayerReq	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawnPlayerReq)
					PRINTLN("sPlacedPed[", i, "].iContinuityId  				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iContinuityId)
					PRINTLN("sPlacedPed[", i, "].iPedMCVarAssociatedSpawnIndex  = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedMCVarAssociatedSpawnIndex)
					PRINTLN("sPlacedPed[", i, "].iPedCopDecoyActiveTime  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedCopDecoyActiveTime)
					PRINTLN("sPlacedPed[", i, "].iPedCopDecoyLose2StarChance  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedCopDecoyLose2StarChance)
					PRINTLN("sPlacedPed[", i, "].sPedBlipStruct.fBlipHeightDifference  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sPedBlipStruct.fBlipHeightDifference)
					
					PRINTSTRING("sPlacedPed[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayAggroPass     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sEndMissionData.iNextMissionToPlayAggroPass)PRINTNL()
					PRINTSTRING("sPlacedPed[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayFail	     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sEndMissionData.iNextMissionToPlayFail)PRINTNL()
					PRINTSTRING("sPlacedPed[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayPass	     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sEndMissionData.iNextMissionToPlayPass)PRINTNL()
					PRINTSTRING("sPlacedPed[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndPassMocap 			       	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sEndMissionData.iEndPassMocap)PRINTNL()
					PRINTSTRING("sPlacedPed[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndFailMocap 			     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sEndMissionData.iEndFailMocap)PRINTNL()
					PRINTSTRING("sPlacedPed[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndAggroMocap 		      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sEndMissionData.iEndAggroMocap)PRINTNL()
					PRINTSTRING("sPlacedPed[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iBranchScriptCutRule      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sEndMissionData.iBranchScriptCutRule)PRINTNL()
					PRINTSTRING("sPlacedPed[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iBranchScriptCutScene      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sEndMissionData.iBranchScriptCutScene)PRINTNL()
					
					PRINTSTRING("sPlacedPed[")PRINTINT(i)PRINTSTRING("].sProximitySpawningData.iSpawnRange      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sProximitySpawningData.iSpawnRange)PRINTNL()
					PRINTSTRING("sPlacedPed[")PRINTINT(i)PRINTSTRING("].sProximitySpawningData.iCleanupRange      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sProximitySpawningData.iCleanupRange)PRINTNL()
					PRINTSTRING("sPlacedPed[")PRINTINT(i)PRINTSTRING("].sProximitySpawningData.iRequiredPlayers     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sProximitySpawningData.iRequiredPlayers)PRINTNL()
					
					PRINTLN("sPlacedPed[", i, "].sWarpLocationSettings.iCheckPointPositionIDStart							= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sWarpLocationSettings.iCheckPointPositionIDStart)
					PRINTLN("sPlacedPed[", i, "].sWarpLocationSettings.iWarpLocationBS    	 	 							= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sWarpLocationSettings.iWarpLocationBS)
					PRINTLN("sPlacedPed[", i, "].sWarpLocationSettings.iForcedRespawnLocation    	 						= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sWarpLocationSettings.iForcedRespawnLocation)
					PRINTLN("sPlacedPed[", i, "].sWarpLocationSettings.sWarpLocationSettings.sEntityWarpPTFX.ePTFXType    	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sWarpLocationSettings.sEntityWarpPTFX.ePTFXType)										
					INT iii = 0
					FOR iii = 0 TO FMMC_MAX_RULE_VECTOR_WARPS-1						
						PRINTLN("sPlacedPed[", i, "].sWarpLocationSettings.vPosition[", iii, "]    	 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sWarpLocationSettings.vPosition[iii])
						PRINTLN("sPlacedPed[", i, "].sWarpLocationSettings.fHeading[", iii, "]    	 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sWarpLocationSettings.fHeading[iii])
					ENDFOR
		
					INT iFlag = 0
					FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
						PRINTLN("sPlacedPed[", i,"].eSpawnConditionFlag[", iFlag,"] = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].eSpawnConditionFlag[iFlag]))
					ENDFOR
					
					FOR iii = 0 TO ciAltVarsBS_MAX-1
						PRINTLN("[AltVarsSystemSpam] - sPlacedPed[", i, "].iAltVarsBitsetPed[", iii, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAltVarsBitsetPed[iii])
					ENDFOR
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitset, ciPED_BS_CanMoveBeforeCombat)
						PRINTLN("sPlacedPed[", i, "] can move before combat ciPED_BS_CanMoveBeforeCombat is SET")
					ELSE
						PRINTLN("sPlacedPed[", i, "] cant move before combat ciPED_BS_CanMoveBeforeCombat is NOT SET")
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitset, ciPED_BS_FreeMovementInCombat)
						PRINTLN("sPlacedPed[", i, "] can move freely in combat ciPED_BS_FreeMovementInCombat is SET")
					ELSE
						PRINTLN("sPlacedPed[", i, "] cant move freely in combat ciPED_BS_FreeMovementInCombat is NOT SET")
					ENDIF					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitset, ciPED_BS_PlayerNumberCreated)
						PRINTLN("sPlacedPed[", i, "] is created depending on player numbers ciPED_BS_PlayerNumberCreated is SET")
					ELSE
						PRINTLN("sPlacedPed[", i, "] is created normally ciPED_BS_PlayerNumberCreated is NOT SET")
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitset, ciPED_BS_AssActionWalk)
						PRINTLN("sPlacedPed[", i, "] will walk when performnig their associated action")
					ELSE
						PRINTLN("sPlacedPed[", i, "] will run when performnig their associated action")
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_DisableReact)
						PRINTLN("sPlacedPed[", i, "] has disabled reactions")
					ENDIF
					
					PRINTLN("sPlacedPed[", i, "].sSelfDestructSettings.iTeam  							= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sSelfDestructSettings.iTeam									)
					PRINTLN("sPlacedPed[", i, "].sSelfDestructSettings.iRuleStart  						= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sSelfDestructSettings.iRuleStart								)
					PRINTLN("sPlacedPed[", i, "].sSelfDestructSettings.iRuleEnd  						= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sSelfDestructSettings.iRuleEnd								)
					PRINTLN("sPlacedPed[", i, "].sSelfDestructSettings.iDelayMiliseconds  				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sSelfDestructSettings.iDelayMiliseconds						)
					PRINTLN("sPlacedPed[", i, "].sSelfDestructSettings.iMustBeBelowHealthPercentage  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sSelfDestructSettings.iMustBeBelowHealthPercentage)
					PRINTLN("sPlacedPed[", i, "].sSelfDestructSettings.iDestructBS  					= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sSelfDestructSettings.iDestructBS								)
					
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.iPresetIndex  					= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.iPresetIndex							)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.iBS  							= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.iBS									)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.iBS2  							= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.iBS2									)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.iRuleFrom					  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.iRuleFrom								)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.iRuleTo						  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.iRuleTo								)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.fDetectionRange				  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.fDetectionRange						)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.fLineOfSightConeAngle		  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.fLineOfSightConeAngle					)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.fLineOfSightConeHeightMax		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.fLineOfSightConeHeightMax				)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.fLineOfSightConeHeightMin	  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.fLineOfSightConeHeightMin				)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.fInstantDetectionRange		  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.fInstantDetectionRange					)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.fInstantLineOfSightConeAngle  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.fInstantLineOfSightConeAngle			)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.fInstantLineOfSightConeHeightMax= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.fInstantLineOfSightConeHeightMax		)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.fInstantLineOfSightConeHeightMin= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.fInstantLineOfSightConeHeightMin		)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.iStealthAggroDelay			  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.iStealthAggroDelay						)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.fWeaponUsageRespondingDistance	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.fWeaponUsageRespondingDistance			)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.iVehiclePartialDisguiseBS	= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.iVehiclePartialDisguiseBS			)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.iVehicleFullDisguiseBS		= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.iVehicleFullDisguiseBS				)
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.mnVehiclePartialDiguise		  	= ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.mnVehiclePartialDiguise))
					PRINTLN("sPlacedPed[", i, "].sStealthAndAggroSystem.mnVehicleFullDiguise			= ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sStealthAndAggroSystem.mnVehicleFullDiguise))
										
					PRINTLN("sPlacedPed[", i, "].sCompanionSettings.iBS  								= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sCompanionSettings.iBS			)
					PRINTLN("sPlacedPed[", i, "].sCompanionSettings.iTeam  								= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sCompanionSettings.iTeam			)
					PRINTLN("sPlacedPed[", i, "].sCompanionSettings.iRuleStart_ForceState	 			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sCompanionSettings.iRuleStart_ForceState			)
					PRINTLN("sPlacedPed[", i, "].sCompanionSettings.iRuleEnd_ForceState	 				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sCompanionSettings.iRuleEnd_ForceState			)
					PRINTLN("sPlacedPed[", i, "].sCompanionSettings.fGroupJoinRange	 					= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sCompanionSettings.fGroupJoinRange			)
					PRINTLN("sPlacedPed[", i, "].sCompanionSettings.fGroupSeparationRange	 			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sCompanionSettings.fGroupSeparationRange			)
					PRINTLN("sPlacedPed[", i, "].sCompanionSettings.fSeparationWarpRange	 			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sCompanionSettings.fSeparationWarpRange			)
					
					PRINTLN("sPlacedPed[", i, "].iDropPickupChance  				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDropPickupChance  )
					PRINTLN("sPlacedPed[", i, "].iDropPickupIndex					= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDropPickupIndex	)
						
					//Blip specific settings.
					CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] sPlacedPed[", i, "]  Blip off:", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitset, ciPED_BS_BlipOff), "T", "F"), "  On follow:", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwo, ciPED_BSTwo_BlipOnFollow), "T", "F"), "  After combat:", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_Blip_After_Combat), "T", "F"), "  AI only: ", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_AiBlipsOnly), "T", "F"), "  Dis. heli:", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_DisableHelicopterBlip), "T", "F"), "  Show height:", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_ShowBlipHeightIndicator), "T", "F"))
					
					FOR iTask = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
						PRINTLN("sPlacedPed[", i, "].iRule[", iTask,"]                    	 		= ", CONVERT_RULE_INT_TO_STRING_FOR_PRINTING(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule[iTask])," = [", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule[iTask], "]")
						PRINTLN("sPlacedPed[", i, "].iPriority[", iTask,"]                 			= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTask])
						PRINTLN("sPlacedPed[", i, "].iTeam[", iTask,"]                     			= ", CONVERT_TEAM_INT_TO_STRING_FOR_PRINTING(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iTeam[iTask])," = [", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iTeam[iTask], "]")
						PRINTLN("sPlacedPed[", i, "] is Mission Critical for Team ", iTask," 		= ", GET_STRING_FROM_BOOL(IS_BIT_SET(g_FMMC_STRUCT.iMissionCriticalPedDoubleBS[iTask][i/31], i - 31*(i/31))))
						PRINTLN("sPlacedPed[", i, "].iMissionCriticalRemovalRule[", iTask,"]        = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iMissionCriticalRemovalRule[iTask])
						PRINTLN("iCriticalGoToPedDoubleBS[", iTask, "][", i,"]       				= ", GET_STRING_FROM_BOOL(IS_BIT_SET(g_FMMC_STRUCT.iCriticalGoToPedDoubleBS[iTask][GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))))
						
						PRINTLN("sPlacedPed[", i, "].iJumpToObjectivePass[", iTask, "]				= ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iJumpToObjectivePass[iTask])
						
					ENDFOR
				ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
					FOR iTask = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
						IF iTask < FMMC_MAX_TEAMS
							PRINTLN("sPlacedPed[", i, "].iTeam[", 0,"]                     = ", CONVERT_TEAM_INT_TO_STRING_FOR_PRINTING_DM(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iTeam[iTask]), " = [", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iTeam[iTask], "]")
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDFOR	
		IF IS_THIS_A_MISSION()
		OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
			PRINTSTRING("Max Ped for Excel")
			PRINTNL()
			PRINTNL()
			PRINTNL()
			PRINTSTRING("					")PRINTSTRING("Ass Obj 1")PRINTSTRING("	")PRINTSTRING("Ass Team 1")PRINTSTRING("	")PRINTSTRING("Ass Spawn 1")
			IF g_FMMC_STRUCT.iMaxNumberOfTeams > 1
				PRINTSTRING("	")PRINTSTRING("	")PRINTSTRING("Ass Obj 2")PRINTSTRING("	")PRINTSTRING("Ass Team 2")PRINTSTRING("	")PRINTSTRING("Ass Spawn 2")
				IF g_FMMC_STRUCT.iMaxNumberOfTeams > 2
					PRINTSTRING("	")PRINTSTRING("	")PRINTSTRING("Ass Obj 3")PRINTSTRING("	")PRINTSTRING("Ass Team 3")PRINTSTRING("	")PRINTSTRING("Ass Spawn 3")
					IF g_FMMC_STRUCT.iMaxNumberOfTeams > 3
						PRINTSTRING("	")PRINTSTRING("	")PRINTSTRING("Ass Obj 4")PRINTSTRING("	")PRINTSTRING("Ass Team 4")PRINTSTRING("	")PRINTSTRING("Ass Spawn 4")
					ENDIF
				ENDIF
			ENDIF
			PRINTSTRING("	")PRINTSTRING("	")PRINTSTRING("Cln Up Obj")PRINTSTRING("	")PRINTSTRING("Cln Up Team")PRINTSTRING("	")PRINTSTRING("Cln Up MidP")
			PRINTNL()
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds -1)
				IF i < FMMC_MAX_PEDS
					PRINTSTRING("					")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective)PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam)PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedSpawn)
					IF g_FMMC_STRUCT.iMaxNumberOfTeams > 1
						PRINTSTRING("	")PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedObjective)PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedTeam)PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedSpawn)
						IF g_FMMC_STRUCT.iMaxNumberOfTeams > 2
							PRINTSTRING("	")PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedObjective)PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedTeam)PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedSpawn)
							IF g_FMMC_STRUCT.iMaxNumberOfTeams > 3
								PRINTSTRING("	")PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedObjective)PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedTeam)PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedSpawn)
							ENDIF
						ENDIF
					ENDIF
					PRINTSTRING("		")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCleanupObjective)PRINTSTRING("	")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCleanupTeam)PRINTSTRING("	")PRINTBOOL(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitset,ciPED_BS_CleanupAtMidpoint))
					PRINTNL()
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	PRINTNL()
	PRINTNL()
	IF IS_THIS_A_MISSION()
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
		PRINTSTRING("----------------------HIGH PRIORITY PEDS---------------------------")PRINTNL()
		FOR i = 0 TO (FMMC_MAX_PRIORITY_PEDS - 1)
			PRINTSTRING("iHighPriorityPed[")PRINTINT(i)PRINTSTRING("]                           = ")PRINTINT(g_FMMC_STRUCT.iHighPriorityPed[i])PRINTNL()
		ENDFOR
	ENDIF
	PRINTNL()
	PRINTNL()
	PRINTSTRING("----------------------VEHICLES---------------------------")PRINTNL()
	PRINTSTRING("iNumberOfVehicleCrates = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates)PRINTNL()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles = 0
		PRINTSTRING("***** NO VEHICLES WERE MADE OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfVehicles = 0")PRINTNL()
	ELSE
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
			IF i < FMMC_MAX_VEHICLES
				
				
				PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].vPos                       = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos)PRINTNL()
				PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].fHead                      = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fHead)PRINTNL()
				PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iColour                    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iColour)PRINTNL()
				PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iVehicleRespawnLives                   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehicleRespawnLives)PRINTNL()
				PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iHealth                    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iHealth)PRINTNL()
				PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].fEngineHealth              = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fEngineHealth)PRINTNL()
				PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iHealthBarInRange          = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iHealthBarInRange)PRINTNL()
				PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iHealthBarCustomTitleStringIndex          = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iHealthBarCustomTitleStringIndex)PRINTNL()
				PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].fPetrolHealth              = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fPetrolHealth)PRINTNL()
				PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].fRespawnRange              = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fRespawnRange)PRINTNL()
				PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].fRespawnMinRange           = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fRespawnMinRange)PRINTNL()
				
				FOR ii = 0 TO (FMMC_MAX_RESTART_CHECKPOINTS - 1)				
					PRINTLN("sPlacedVehicle[", i, "].vRestartPos[", ii, "]           		= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vRestartPos[ii])
					PRINTLN("sPlacedVehicle[", i, "].fRestartHeading[", ii, "]           	= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fRestartHeading[ii])
				ENDFOR
				
				IF IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
				AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].mn                         = ")PRINTSTRING( GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))PRINTSTRING(" [")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))PRINTSTRING(" ]")PRINTNL()
				ENDIF
				IF IS_THIS_A_MISSION()
				OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_CRATE_DROP	
				OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iLivery   			   		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iLivery)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iModPreset 			   		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iModPreset)PRINTNL()
					
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iAssociatedObjective   		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iAssociatedTeam  	   		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iAssociatedSpawn 	   		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedSpawn)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedObjective	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedObjective)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedTeam		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedTeam)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedSpawn		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedSpawn)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedObjective	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedObjective)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedTeam		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedTeam)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedSpawn		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedSpawn)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedObjective	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedObjective)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedTeam		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedTeam)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedSpawn		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedSpawn)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iRespawnDelay				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnDelay)PRINTNL()										
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iSpawnNearType				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSpawnNearType)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iSpawnNearEntityID			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSpawnNearEntityID)PRINTNL()	
					
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iSpawnGroup						= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSpawnGroup)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iModifySpawnGroupOnEvent	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iModifySpawnGroupOnEvent)PRINTNL()
					FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
						PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iSpawnSubGroupBS[")PRINTINT(ii)PRINTSTRING("]   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSpawnSubGroupBS[ii])PRINTNL()
						PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iModifySpawnSubGroupOnEventBS[")PRINTINT(ii)PRINTSTRING("]   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iModifySpawnSubGroupOnEventBS[ii])PRINTNL()
					ENDFOR
					PRINTLN("sPlacedVehicle[", i, "].iSpawnGroupAdditionalFunctionalityBS							= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSpawnGroupAdditionalFunctionalityBS)
					
					PRINTLN("sPlacedVehicle[",i,"].iVehBitset 	      	    	= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitset)
					PRINTLN("sPlacedVehicle[",i,"].iVehBitsetTwo 	      	    = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetTwo)
					PRINTLN("sPlacedVehicle[",i,"].iVehBitsetThree 	      	    = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetThree)
					PRINTLN("sPlacedVehicle[",i,"].iVehBitsetFour 	      	    = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFour)
					PRINTLN("sPlacedVehicle[",i,"].iVehBitsetFive 	      	    = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive)
					PRINTLN("sPlacedVehicle[",i,"].iVehBitsetSix 	      	    = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSix)
					PRINTLN("sPlacedVehicle[",i,"].iVehBitsetSeven 	      	    = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven)
					PRINTLN("sPlacedVehicle[",i,"].iVehBitsetNine 	      	    = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetNine)
					PRINTLN("sPlacedVehicle[",i,"].iVehBitsetTen 	      	    = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetTen)
					
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iAggroIndexBS_Entity_Veh    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAggroIndexBS_Entity_Veh)PRINTNL()
				
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetThree, ciFMMC_VEHICLE3_UseSearchLightInDark)
						PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("] IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetThree, ciFMMC_VEHICLE3_UseSearchLightInDark)")
					ENDIF
					
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iCleanupTeam           = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCleanupTeam)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iCleanupObjective      = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCleanupObjective)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iCleanupDelay   	   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCleanupDelay)PRINTNL()
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitSet, ciFMMC_VEHICLE_PLAYER_NUM_CREATED)
						PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("] is created depending on player numbers ciFMMC_VEHICLE_PLAYER_NUM_CREATED is SET")PRINTNL()
					ELSE
						PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("] is created normally ciFMMC_VEHICLE_PLAYER_NUM_CREATED is NOT SET")PRINTNL()
					ENDIF
					FOR iTask = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
						PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iRule[")PRINTINT(iTask)PRINTSTRING("]                   = ")PRINTSTRING(CONVERT_RULE_INT_TO_STRING_FOR_PRINTING(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule[iTask]))PRINTSTRING(" = [")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule[iTask])PRINTSTRING("]")PRINTNL()
						PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iPriority[")PRINTINT(iTask)PRINTSTRING("]               = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTask])PRINTNL()
						PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("] is Mission Critical	for Team ")PRINTINT(iTask)PRINTSTRING(" = ") PRINTBOOL(IS_BIT_SET(g_FMMC_STRUCT.iMissionCriticalVehBS[iTask], i))PRINTNL()
						PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iMissionCriticalRemovalRule[")PRINTINT(iTask)PRINTSTRING("] = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iMissionCriticalRemovalRule[iTask])PRINTNL()
						PRINTLN("sPlacedVehicle[",i,"].iTeamSeatPreference[",iTask,"]  = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iTeamSeatPreference[iTask])
					ENDFOR
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iMissionCriticalLinkedVeh  = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iMissionCriticalLinkedVeh)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iVehTeamHorn   	   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehTeamHorn)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iAttachParentType    	   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iAttachParent	    	   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].vDropOffOverride    	   = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vDropOffOverride)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].fVehDropOffRadius    	   = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fVehDropOffRadius)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].fVehDropOffVisualRadius    	   = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fVehDropOffVisualRadius)PRINTNL()
					
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iBurnRule    	  			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iBurnRule)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iBurnTeam    	   			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iBurnTeam)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iBurningLifetime    	   	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iBurningLifetime)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].fFirePutOutTime    	  		= ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fFirePutOutTime)PRINTNL()
					PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[",i,"].iBurningVehicleBS				= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iBurningVehicleBS)
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.iVehicleTireBitSet, i)
						PRINTSTRING("TIRES UNBURSTABLE = TRUE [")PRINTINT(i)PRINTSTRING("]")PRINTNL()
					ENDIF
					
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iVehBecomesUnstuckOnRule    	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBecomesUnstuckOnRule)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iVehBecomesLockedOnRule    	    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBecomesLockedOnRule)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iSeatBitset    	  				  = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSeatBitset)PRINTNL()					
					
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iInvincibleOnRules    	    	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iInvincibleOnRules)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iInvincibleOnRulesTeam    	    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iInvincibleOnRulesTeam)PRINTNL()
									
					PRINTLN("sPlacedVehicle[", i, "].sWarpLocationSettings.iCheckPointPositionIDStart							= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sWarpLocationSettings.iCheckPointPositionIDStart)
					PRINTLN("sPlacedVehicle[", i, "].sWarpLocationSettings.iWarpLocationBS    	 	 							= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sWarpLocationSettings.iWarpLocationBS)
					PRINTLN("sPlacedVehicle[", i, "].sWarpLocationSettings.iForcedRespawnLocation    	 						= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sWarpLocationSettings.iForcedRespawnLocation)
					PRINTLN("sPlacedVehicle[", i, "].sWarpLocationSettings.sWarpLocationSettings.sEntityWarpPTFX.ePTFXType    	= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sWarpLocationSettings.sEntityWarpPTFX.ePTFXType)					
					INT iii = 0
					FOR iii = 0 TO FMMC_MAX_RULE_VECTOR_WARPS-1						
						PRINTLN("sPlacedVehicle[", i, "].sWarpLocationSettings.vPosition[", iii, "]    	 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sWarpLocationSettings.vPosition[iii])
						PRINTLN("sPlacedVehicle[", i, "].sWarpLocationSettings.fHeading[", iii, "]    	 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sWarpLocationSettings.fHeading[iii])
					ENDFOR
					
					FOR iii = 0 TO ciAltVarsBS_MAX-1
						PRINTLN("[AltVarsSystemSpam] - sPlacedVehicle[", i, "].iAltVarsBitsetVeh[", iii, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAltVarsBitsetVeh[iii])
					ENDFOR
					
					FOR iii = 0 TO ciPREREQ_Bitsets-1
						PRINTLN("sPlacedVehicle[", i, "].iSetPrereqsOnEnterBS[", iii, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSetPrereqsOnEnterBS[iii])
					ENDFOR
					
					PRINTSTRING("[MissionVariation] sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iVehMCVarAssociatedSpawnIndex      = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehMCVarAssociatedSpawnIndex)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].iContinuityID      = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityID)PRINTNL()
					PRINTLN("sPlacedVehicle[", i, "].iZoneBlockEntitySpawn  					= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawn)
					PRINTLN("sPlacedVehicle[", i, "].iZoneBlockEntitySpawnPlayerReq  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawnPlayerReq)
					
					//Custom points
					PRINTLN("sPlacedVehicle[", i, "].iVehPointsToGive  				= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehPointsToGive)
					PRINTLN("sPlacedVehicle[", i, "].iVehGivePointsOnRuleStart  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehGivePointsOnRuleStart)
					PRINTLN("sPlacedVehicle[", i, "].iVehGivePointsOnRuleEnd  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehGivePointsOnRuleEnd)
					
					//Blip specific settings.
					CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] sPlacedVehicle[", i, "]  Blip off: ", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitSet, ciFMMC_VEHICLE_BLIP_OFF), "T", "F"), "  Show height:", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetThree, ciFMMC_VEHICLE3_ShowBlipHeightIndicator), "T", "F"))
					
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayAggroPass     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sEndMissionData.iNextMissionToPlayAggroPass)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayFail	     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sEndMissionData.iNextMissionToPlayFail)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayPass	     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sEndMissionData.iNextMissionToPlayPass)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndPassMocap 			       	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sEndMissionData.iEndPassMocap)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndFailMocap 			     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sEndMissionData.iEndFailMocap)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndAggroMocap 		      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sEndMissionData.iEndAggroMocap)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iBranchScriptCutRule 	   		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sEndMissionData.iBranchScriptCutRule)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iBranchScriptCutScene	   		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sEndMissionData.iBranchScriptCutScene)PRINTNL()
					
					PRINTLN("sPlacedVehicle[", i, "].sSelfDestructSettings.iTeam  						= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sSelfDestructSettings.iTeam)
					PRINTLN("sPlacedVehicle[", i, "].sSelfDestructSettings.iRuleStart  					= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sSelfDestructSettings.iRuleStart)
					PRINTLN("sPlacedVehicle[", i, "].sSelfDestructSettings.iRuleEnd  					= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sSelfDestructSettings.iRuleEnd)
					PRINTLN("sPlacedVehicle[", i, "].sSelfDestructSettings.iDelayMiliseconds  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sSelfDestructSettings.iDelayMiliseconds)
					PRINTLN("sPlacedVehicle[", i, "].sSelfDestructSettings.iMustBeBelowHealthPercentage = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sSelfDestructSettings.iMustBeBelowHealthPercentage)
					PRINTLN("sPlacedVehicle[", i, "].sSelfDestructSettings.iDestructBS  				= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sSelfDestructSettings.iDestructBS)
					
					PRINTLN("sPlacedVehicle[", i, "].iCargo = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCargo)
					PRINTLN("sPlacedVehicle[", i, "].iVehCargoExplodeTeam = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehCargoExplodeTeam)
					PRINTLN("sPlacedVehicle[", i, "].iVehCargoExplodeRule = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehCargoExplodeRule)
					PRINTLN("sPlacedVehicle[", i, "].iVehCargoBeepTime = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehCargoBeepTime)
					
					PRINTLN("sPlacedVehicle[", i, "].iObjectivePrereq = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iObjectivePrereq)
				
					INT iFlag = 0
					FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
						PRINTLN("sPlacedVehicle[", i,"].eSpawnConditionFlag[", iFlag,"] = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].eSpawnConditionFlag[iFlag]))
					ENDFOR
					
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].sProximitySpawningData.iSpawnRange      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sProximitySpawningData.iSpawnRange)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].sProximitySpawningData.iCleanupRange      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sProximitySpawningData.iCleanupRange)PRINTNL()
					PRINTSTRING("sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].sProximitySpawningData.iRequiredPlayers     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sProximitySpawningData.iRequiredPlayers)PRINTNL()
										
					PRINTLN("sPlacedVehicle[", i, "].sTrainAttachmentData.iTrainIndex			 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sTrainAttachmentData.iTrainIndex		)
					PRINTLN("sPlacedVehicle[", i, "].sTrainAttachmentData.iCarriageIndex  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sTrainAttachmentData.iCarriageIndex  	)
					PRINTLN("sPlacedVehicle[", i, "].sTrainAttachmentData.vCarriageOffsetPosition  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sTrainAttachmentData.vCarriageOffsetPosition)
					PRINTLN("sPlacedVehicle[", i, "].sTrainAttachmentData.vCarriageOffsetRotation  	= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sTrainAttachmentData.vCarriageOffsetRotation)
					
					PRINTLN("sPlacedVehicle[", i, "].iKinematicPhysicsTeam    	= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iKinematicPhysicsTeam)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iKinematicPhysicsBS = 0
						PRINTLN("sPlacedVehicle[", i, "].iKinematicPhysicsBS - All bits are set to FALSE. This vehicle is not using Kinematic Physics on any rule")
					ELSE
						PRINTLN("sPlacedVehicle[", i, "].iKinematicPhysicsBS - Some bits are set to TRUE. This vehicle is using Kinematic Physics on a rule")
						INT iRule = 0
						FOR iRule = 0 TO FMMC_MAX_RULES - 1
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iKinematicPhysicsBS, iRule)
								PRINTLN("sPlacedVehicle[", i, "].iKinematicPhysicsBS - BIT_", iRule, " = TRUE")
							ELSE
								PRINTLN("sPlacedVehicle[", i, "].iKinematicPhysicsBS - BIT_", iRule, " = FALSE")
							ENDIF
						ENDFOR
					ENDIF
					
					PRINT_VEHICLE_CUSTOM_MOD_SETTINGS(i)
					
				ENDIF
				PRINTNL()
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTNL()
	PRINTNL()
	PRINTSTRING("-----------------------OBJECTS--------------------------")PRINTNL()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfObjects = 0
		PRINTSTRING("***** NO OBJECTS WERE MADE OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfObjects = 0")PRINTNL()
	ELSE
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
			IF i < FMMC_MAX_NUM_OBJECTS
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].vPos                 = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].vRot                 = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vRot)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].fHead                = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fHead)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iObjectRespawnLives  = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectRespawnLives)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].sObjBlipStruct.iBlipStyle           = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjBlipStruct.iBlipStyle)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].sObjBlipStruct.fBlipRange           = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjBlipStruct.fBlipRange)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].mn                   = ")PRINTSTRING(GET_MODEL_STING_FROM_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))PRINTSTRING(" [")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))PRINTSTRING(" ]")PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iObjectHealth        = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectHealth)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iObjectColour		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectColour)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iLegacyMiniGameBitSet      = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLegacyMiniGameBitSet)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iCrateContents       = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCrateContents)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iCrateValue		    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCrateValue)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iInterior		    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iInterior)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iRoomKey		    	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRoomKey)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].vInteriorCoords		= ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vInteriorCoords)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iInteriorTypeHash	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iInteriorTypeHash)PRINTNL()
				PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iPlacedMarkerIndex	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPlacedMarkerIndex)PRINTNL()
				
				PRINTLN("sPlacedObject[", i, "].sObjectTrainAttachmentData.iTrainIndex			 			= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.iTrainIndex		)
				PRINTLN("sPlacedObject[", i, "].sObjectTrainAttachmentData.iCarriageIndex  					= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.iCarriageIndex  	)
				PRINTLN("sPlacedObject[", i, "].sObjectTrainAttachmentData.vCarriageOffsetPosition  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.vCarriageOffsetPosition)
				PRINTLN("sPlacedObject[", i, "].sObjectTrainAttachmentData.vCarriageOffsetRotation  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData.vCarriageOffsetRotation)
				
				IF IS_THIS_A_MISSION()
				OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_CRATE_DROP
				OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iAssociatedObjective   	 	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iAssociatedTeam  	  	 	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iAssociatedSpawn 	  	 	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedSpawn)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iAssociatedScoreRequired 	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedScoreRequired)PRINTNL()

					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedObjective	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedObjective)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedTeam		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedTeam)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedSpawn		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedSpawn)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedScoreRequired = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedScoreRequired)PRINTNL()
					
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedObjective	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedObjective)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedTeam			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedTeam)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedSpawn		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedSpawn)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedScoreRequired = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedScoreRequired)PRINTNL()
					
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedObjective	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedObjective)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedTeam		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedTeam)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedSpawn		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedSpawn)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedScoreRequired = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedScoreRequired)PRINTNL()
					
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iRespawnDelay					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRespawnDelay)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iSpawnGroup					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnGroup)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iModifySpawnGroupOnEvent					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iModifySpawnGroupOnEvent)PRINTNL()
					FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
						PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iSpawnSubGroupBS[")PRINTINT(ii)PRINTSTRING("]   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnSubGroupBS[ii])PRINTNL()
						PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iModifySpawnSubGroupOnEventBS[")PRINTINT(ii)PRINTSTRING("]   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iModifySpawnSubGroupOnEventBS[ii])PRINTNL()
					ENDFOR
					PRINTLN("sPlacedObject[", i, "].iSpawnGroupAdditionalFunctionalityBS							= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnGroupAdditionalFunctionalityBS)
					
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iCleanupTeam           		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCleanupTeam)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iCleanupObjective       		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCleanupObjective)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iObjectAttachmentPed			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed)PRINTNL()
					
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iContinuityId				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iContinuityId)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iContentContinuityType		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iContentContinuityType)PRINTNL()
					PRINTLN("sPlacedObject[", i, "].iZoneBlockEntitySpawn  								= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawn)
					PRINTLN("sPlacedObject[", i, "].iZoneBlockEntitySpawnPlayerReq  					= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawnPlayerReq)
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iClothesChangeOutfit			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iClothesChangeOutfit)PRINTNL()
					
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iPaintingIndex				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPaintingIndex)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iLinkedDoor1					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor1)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iLinkedDoor2					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor2)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iNumberOfObstructions		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iNumberOfObstructions)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iNumberOfObstructions_Hard		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iNumberOfObstructions_Hard)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].fDifficultyOfObstructions	= ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fDifficultyOfObstructions)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].fDifficultyOfObstructions_Hard	= ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fDifficultyOfObstructions_Hard)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].fDifficultyOfObstructionsLaser	= ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fDifficultyOfObstructionsLaser)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].fDifficultyOfObstructionsLaser_Hard	= ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fDifficultyOfObstructionsLaser_Hard)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iPatternsRequired			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPatternsRequired)PRINTNL()
					
					PRINTSTRING("[MissionVariation] sPlacedObject[")PRINTINT(i)PRINTSTRING("].iObjMCVarSelectionIndex_One		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjMCVarSelectionIndex_One)PRINTNL()
					PRINTSTRING("[MissionVariation] sPlacedObject[")PRINTINT(i)PRINTSTRING("].iObjMCVarSelectionIndex_Two		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjMCVarSelectionIndex_Two)PRINTNL()					
					
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iAggroIndexBS_Entity_Obj = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj)PRINTNL()					
					
					INT iFlag = 0
					FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
						PRINTLN("sPlacedObject[", i,"].eSpawnConditionFlag[", iFlag,"] = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].eSpawnConditionFlag[iFlag]))
					ENDFOR
					
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iObjectBitSet					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iObjectBitSetTwo					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetTwo)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iObjectBitSetThree				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetThree)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iObjectBitSetFour				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetFour)PRINTNL()					
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iObjectBitSetFive				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetFive)PRINTNL()					
								
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_PlayerNumCreated)
						PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("] is created depending on player numbers cibsOBJ_PlayerNumCreated is SET")PRINTNL()
					ELSE
						PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("] is created normally cibsOBJ_PlayerNumCreated is NOT SET")PRINTNL()
					ENDIF
					FOR iTask = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
						PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iRule[")PRINTINT(iTask)PRINTSTRING("]             = ")PRINTSTRING(CONVERT_RULE_INT_TO_STRING_FOR_PRINTING(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[iTask]))PRINTSTRING(" = [")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[iTask])PRINTSTRING("]")PRINTNL()
						PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].iPriority[")PRINTINT(iTask)PRINTSTRING("]         = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTask])PRINTNL()
						PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("] is Mission Critical for Team ")PRINTINT(iTask)PRINTSTRING(" = ") PRINTBOOL(IS_BIT_SET(g_FMMC_STRUCT.iMissionCriticalObjBS[iTask], i))PRINTNL()
					ENDFOR
					
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayAggroPass     		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEndMissionData.iNextMissionToPlayAggroPass)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayFail	     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEndMissionData.iNextMissionToPlayFail)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iNextMissionToPlayPass	     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEndMissionData.iNextMissionToPlayPass)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndPassMocap 			       	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEndMissionData.iEndPassMocap)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndFailMocap 			     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEndMissionData.iEndFailMocap)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iEndAggroMocap 		      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEndMissionData.iEndAggroMocap)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iBranchScriptCutRule 		    = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEndMissionData.iBranchScriptCutRule)PRINTNL()
					PRINTSTRING("sPlacedObject[")PRINTINT(i)PRINTSTRING("].sEndMissionData.iBranchScriptCutScene      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEndMissionData.iBranchScriptCutScene)PRINTNL()
					
					//Custom points
					PRINTLN("sPlacedObject[", i, "].iObjPointsToGive  				= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjPointsToGive)
					PRINTLN("sPlacedObject[", i, "].iObjGivePointsOnRuleStart  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjGivePointsOnRuleStart)
					PRINTLN("sPlacedObject[", i, "].iObjGivePointsOnRuleEnd  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjGivePointsOnRuleEnd)
									
					PRINTLN("sPlacedObject[", i, "].sSelfDestructSettings.iTeam  						= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sSelfDestructSettings.iTeam)
					PRINTLN("sPlacedObject[", i, "].sSelfDestructSettings.iRuleStart  					= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sSelfDestructSettings.iRuleStart)
					PRINTLN("sPlacedObject[", i, "].sSelfDestructSettings.iRuleEnd  					= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sSelfDestructSettings.iRuleEnd)
					PRINTLN("sPlacedObject[", i, "].sSelfDestructSettings.iDelayMiliseconds  			= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sSelfDestructSettings.iDelayMiliseconds)
					PRINTLN("sPlacedObject[", i, "].sSelfDestructSettings.iMustBeBelowHealthPercentage  = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sSelfDestructSettings.iMustBeBelowHealthPercentage)
					PRINTLN("sPlacedObject[", i, "].sSelfDestructSettings.iDestructBS  					= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sSelfDestructSettings.iDestructBS)
					
					PRINTLN("sPlacedObject[", i, "].iPhotoRange  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPhotoRange)
					PRINTLN("sPlacedObject[", i, "].iCaptureRange  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCaptureRange)
					PRINTLN("sPlacedObject[", i, "].iSecuroHackCustomString  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecuroHackCustomString)
					
					FOR iTask = 0 TO ciPREREQ_Bitsets - 1
						PRINTLN("sPlacedObject[", i, "].iPreReqRequiredForMinigame[",iTask,"]  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPreReqRequiredForMinigame[iTask])
					ENDFOR
					PRINTLN("sPlacedObject[", i, "].iPreReqRequiredForMinigameHelpText  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPreReqRequiredForMinigameHelpText)
					FOR iTask = 0 TO ciPREREQ_Bitsets - 1
						PRINTLN("sPlacedObject[", i, "].iPreReqsRequiredToUnblock[",iTask,"]  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPreReqsRequiredToUnblock[iTask])
					ENDFOR
					
					PRINTLN("sPlacedObject[", i, "].sWarpLocationSettings.iCheckPointPositionIDStart						= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings.iCheckPointPositionIDStart)
					PRINTLN("sPlacedObject[", i, "].sWarpLocationSettings.iWarpLocationBS    	 	 						= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings.iWarpLocationBS)
					PRINTLN("sPlacedObject[", i, "].sWarpLocationSettings.iForcedRespawnLocation    					 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings.iForcedRespawnLocation)
					PRINTLN("sPlacedObject[", i, "].sWarpLocationSettings.sWarpLocationSettings.sEntityWarpPTFX.ePTFXType   = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings.sEntityWarpPTFX.ePTFXType)					
					INT iii = 0
					FOR iii = 0 TO FMMC_MAX_RULE_VECTOR_WARPS-1						
						PRINTLN("sPlacedObject[", i, "].sWarpLocationSettings.vPosition[", iii, "]    	 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings.vPosition[iii])
						PRINTLN("sPlacedObject[", i, "].sWarpLocationSettings.fHeading[", iii, "]    	 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings.fHeading[iii])						
					ENDFOR
					
					FOR iii = 0 TO ciAltVarsBS_MAX-1
						PRINTLN("[AltVarsSystemSpam] - sPlacedObject[", i, "].iAltVarsBitsetObj[", iii, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAltVarsBitsetObj[iii])
					ENDFOR
					
					FOR iii = 0 TO ciPREREQ_Bitsets-1
						PRINTLN("sPlacedObject[", i, "].iSetPrereqsOnCollectionBS[", iii, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSetPrereqsOnCollectionBS[iii])
						PRINTLN("sPlacedObject[", i, "].iSetPrereqsOnCollectionNoResetBS[", iii, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSetPrereqsOnCollectionNoResetBS[iii])
					ENDFOR
										
					PRINTLN("sPlacedObject[", i, "].iInvincibleOnRules    	    					= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iInvincibleOnRules)
					PRINTLN("sPlacedObject[", i, "].iInvincibleOnRulesTeam    	   					= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iInvincibleOnRulesTeam)
					
					PRINTLN("sPlacedObject[", i, "].sEntAudioData.iLoop 							= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntAudioData.iLoop)
					PRINTLN("sPlacedObject[", i, "].sEntAudioData.iLoopSoundSet						= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntAudioData.iLoopSoundSet)
					PRINTLN("sPlacedObject[", i, "].sEntAudioData.iPlayOnCreate						= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntAudioData.iPlayOnCreate)
					PRINTLN("sPlacedObject[", i, "].sEntAudioData.iPlayOnCreateSoundSet				= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntAudioData.iPlayOnCreateSoundSet)
					PRINTLN("sPlacedObject[", i, "].sEntAudioData.iPlayOnDestroy					= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntAudioData.iPlayOnDestroy)
					PRINTLN("sPlacedObject[", i, "].sEntAudioData.iPlayOnDestroySoundSet			= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntAudioData.iPlayOnDestroySoundSet)
					
					PRINTLN("sPlacedObject[", i, "].iObjectivePrereq								= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectivePrereq)
					
					PRINTLN("sPlacedObject[", i, "].sObjDroneData.iBS								= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.iBS)
					PRINTLN("sPlacedObject[", i, "].sObjDroneData.iAssociatedPoolStartingIndex		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.iAssociatedPoolStartingIndex)
					PRINTLN("sPlacedObject[", i, "].sObjDroneData.fMinimumHoverHeight				= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.fMinimumHoverHeight)
					PRINTLN("sPlacedObject[", i, "].sObjDroneData.fDroneSpeedMultiplier				= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.fDroneSpeedMultiplier)					
					PRINTLN("sPlacedObject[", i, "].sObjDroneData.fMinimumCombatDistanceToTarget	= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.fMinimumCombatDistanceToTarget)
					PRINTLN("sPlacedObject[", i, "].sObjDroneData.fDefensiveRadius	= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.fDefensiveRadius)
					PRINTLN("sPlacedObject[", i, "].sObjDroneData.iStartingAction					= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.iStartingAction)
					PRINTLN("sPlacedObject[", i, "].sObjDroneData.iAssociatedRuleAction				= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.iAssociatedRuleAction)
					PRINTLN("sPlacedObject[", i, "].sObjDroneData.iHealthThresholdAction			= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.iHealthThresholdAction)
					PRINTLN("sPlacedObject[", i, "].sObjDroneData.iHealthThresholdPercentage		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.iHealthThresholdPercentage)											
					FOR iii = 0 TO ciMAX_FMMC_DRONE_WEAPONS - 1							
						PRINTLN("sPlacedObject[", i, "].sObjDroneData.sWeaponData[", iii, "].iWeaponType			= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.sWeaponData[iii].iWeaponType)
						PRINTLN("sPlacedObject[", i, "].sObjDroneData.sWeaponData[", iii, "].iShotAccuracy			= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.sWeaponData[iii].iShotAccuracy)
						PRINTLN("sPlacedObject[", i, "].sObjDroneData.sWeaponData[", iii, "].iShotReloadTime		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.sWeaponData[iii].iShotReloadTime)
						PRINTLN("sPlacedObject[", i, "].sObjDroneData.sWeaponData[", iii, "].iShotIntervalTime		= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.sWeaponData[iii].iShotIntervalTime)
						PRINTLN("sPlacedObject[", i, "].sObjDroneData.sWeaponData[", iii, "].iAmmoMax				= ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData.sWeaponData[iii].iAmmoMax)		
					ENDFOR	
				ENDIF
				PRINTNL()
			ENDIF
		ENDFOR	
	ENDIF
	PRINTNL()
	PRINTNL()
	PRINTSTRING("-----------------------INTERACTABLES--------------------------")PRINTNL()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables = 0
		PRINTSTRING("***** NO INTERACTABLES WERE MADE OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfInteractables = 0")PRINTNL()
	ELSE
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables - 1)
			PRINTLN("sPlacedInteractable[", i, "].vInteractable_Position		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].vInteractable_Position)
			PRINTLN("sPlacedInteractable[", i, "].vInteractable_Rotation		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].vInteractable_Rotation)
			PRINTLN("sPlacedInteractable[", i, "].mnInteractable_Model		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model)
			
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_InteractionType		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_InteractionType)
			PRINTLN("sPlacedInteractable[", i, "].iInteractableBS		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractableBS)
			PRINTLN("sPlacedInteractable[", i, "].fInteractable_InteractionDistance		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].fInteractable_InteractionDistance)
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_RequirePlayerInVehicleIndex		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_RequirePlayerInVehicleIndex)
			
			PRINTLN("sPlacedInteractable[", i, "].sInteractableTrainAttachmentData.iTrainIndex			 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sInteractableTrainAttachmentData.iTrainIndex		)
			PRINTLN("sPlacedInteractable[", i, "].sInteractableTrainAttachmentData.iCarriageIndex  				= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sInteractableTrainAttachmentData.iCarriageIndex  	)
			PRINTLN("sPlacedInteractable[", i, "].sInteractableTrainAttachmentData.vCarriageOffsetPosition  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sInteractableTrainAttachmentData.vCarriageOffsetPosition)
			PRINTLN("sPlacedInteractable[", i, "].sInteractableTrainAttachmentData.vCarriageOffsetRotation  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sInteractableTrainAttachmentData.vCarriageOffsetRotation)
			
			INT iPreReqRequired
			FOR iPreReqRequired = 0 TO ciInteractable_PreReqsRequired_Max - 1
				PRINTLN("sPlacedInteractable[", i, "].iInteractable_PreReqsRequired[", iPreReqRequired, "]		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_PreReqsRequired[iPreReqRequired])
			ENDFOR
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_PreReqsRequiredCount		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_PreReqsRequiredCount)
			
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_Health		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_Health)
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_PromptStringIndex		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_PromptStringIndex)
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_ProximityObjectiveTextIndex_Standard		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_ProximityObjectiveTextIndex_Standard)
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_ProximityObjectiveTextIndex_WaitForTeam		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_ProximityObjectiveTextIndex_WaitForTeam)
			PRINTLN("sPlacedInteractable[", i, "].fInteractable_ProximityObjectiveTextDistance		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].fInteractable_ProximityObjectiveTextDistance)
			
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_AssociatedTeam		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_AssociatedTeam)
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_AssociatedRule		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_AssociatedRule)
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_AssociatedRuleSpawnLimit		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_AssociatedRuleSpawnLimit)
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_CleanupTeam		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_CleanupTeam)
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_CleanupRule		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_CleanupRule)
			
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_BlockInteractionRuleBS[0]		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_BlockInteractionRuleBS[0])
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_BlockInteractionRuleBS[1]		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_BlockInteractionRuleBS[1])
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_BlockInteractionRuleBS[2]		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_BlockInteractionRuleBS[2])
			PRINTLN("sPlacedInteractable[", i, "].iInteractable_BlockInteractionRuleBS[3]		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_BlockInteractionRuleBS[3])
			
			PRINTLN("sPlacedInteractable[", i, "].iAggroIndexBS_Entity_Interactable		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iAggroIndexBS_Entity_Interactable)
			
			PRINTLN("sPlacedInteractable[", i, "].iPlacedMarkerIndex							= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iPlacedMarkerIndex)
			
			INT iFlag = 0
			FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
				PRINTLN("sPlacedInteractable[", i,"].eSpawnConditionFlag[", iFlag,"] = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].eSpawnConditionFlag[iFlag]))
			ENDFOR
			
			FOR ii = 0 TO ciAltVarsBS_MAX-1
				PRINTLN("[AltVarsSystemSpam] - sPlacedInteractable[", i, "].iAltVarsBitsetInteractable[", ii, "] 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iAltVarsBitsetInteractable[ii])
			ENDFOR
	
			PRINTLN("sPlacedInteractable[", i, "].iSpawnGroup							= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_SpawnGroup)		
			PRINTLN("sPlacedInteractable[", i, "].iModifySpawnGroupOnEvent			= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iModifySpawnGroupOnEvent)				
			FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1					
				PRINTLN("sPlacedInteractable[", i, "].iSpawnSubGroupBS[", ii, "]							= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_SpawnSubGroupBS[ii])
				PRINTLN("sPlacedInteractable[", i, "].iModifySpawnSubGroupOnEventBS[", ii, "]			= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iModifySpawnSubGroupOnEventBS[ii])
			ENDFOR			
			PRINTLN("sPlacedInteractable[", i, "].iSpawnGroupAdditionalFunctionalityBS							= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iSpawnGroupAdditionalFunctionalityBS)
					
			PRINTLN("[CONTINUITY] - sPlacedInteractable[", i, "].iContinuityId				= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iContinuityId)
			PRINTLN("[CONTINUITY] - sPlacedInteractable[", i, "].iContentContinuityType		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iContentContinuityType)
			
			PRINTLN("sPlacedInteractable[", i, "].sEntAudioData.iLoop 							= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sEntAudioData.iLoop)
			PRINTLN("sPlacedInteractable[", i, "].sEntAudioData.iLoopSoundSet					= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sEntAudioData.iLoopSoundSet)
			PRINTLN("sPlacedInteractable[", i, "].sEntAudioData.iPlayOnCreate					= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sEntAudioData.iPlayOnCreate)
			PRINTLN("sPlacedInteractable[", i, "].sEntAudioData.iPlayOnCreateSoundSet			= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sEntAudioData.iPlayOnCreateSoundSet)
			PRINTLN("sPlacedInteractable[", i, "].sEntAudioData.iPlayOnDestroy					= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sEntAudioData.iPlayOnDestroy)
			PRINTLN("sPlacedInteractable[", i, "].sEntAudioData.iPlayOnDestroySoundSet			= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sEntAudioData.iPlayOnDestroySoundSet)
			
			PRINTLN("sPlacedInteractable[", i, "].sWarpLocationSettings.iCheckPointPositionIDStart							= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sWarpLocationSettings.iCheckPointPositionIDStart)
			PRINTLN("sPlacedInteractable[", i, "].sWarpLocationSettings.iWarpLocationBS    	 	 							= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sWarpLocationSettings.iWarpLocationBS)
			PRINTLN("sPlacedInteractable[", i, "].sWarpLocationSettings.iForcedRespawnLocation    						 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sWarpLocationSettings.iForcedRespawnLocation)			
			PRINTLN("sPlacedInteractable[", i, "].sWarpLocationSettings.sWarpLocationSettings.sEntityWarpPTFX.ePTFXType    	= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sWarpLocationSettings.sEntityWarpPTFX.ePTFXType)
			INT iii = 0
			FOR iii = 0 TO FMMC_MAX_RULE_VECTOR_WARPS-1				
				PRINTLN("sPlacedInteractable[", i, "].sWarpLocationSettings.vPosition[", iii, "]    	 	= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sWarpLocationSettings.vPosition[iii])
				PRINTLN("sPlacedInteractable[", i, "].sWarpLocationSettings.fHeading[", iii, "]    	 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sWarpLocationSettings.fHeading[iii])
			ENDFOR
		ENDFOR
	ENDIF
	PRINTNL()
	PRINTNL()
	PRINTSTRING("-----------------------PROPS--------------------------")PRINTNL()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfProps = 0
		PRINTSTRING("***** NO PROPS WERE MADE OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfProps = 0")PRINTNL()
	ELSE
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps- 1)
			//IF i < GET_FMMC_MAX_NUM_PROPS()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].vPos        = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].vRot        = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vRot)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].fHead       = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].fHead)PRINTNL()
				
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].mn 		 = ")PRINTSTRING(FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))PRINTSTRING(" [")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))PRINTSTRING(" ]")PRINTNL()
				
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iAssociatedTeam   				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAssociatedTeam)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iAssociatedObjective   		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAssociatedObjective)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iAssociatedSpawn       		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAssociatedSpawn)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedObjective		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iSecondAssociatedObjective)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedTeam			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iSecondAssociatedTeam)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedSpawn			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iSecondAssociatedSpawn)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedObjective		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iThirdAssociatedObjective)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedTeam			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iThirdAssociatedTeam)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedSpawn			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iThirdAssociatedSpawn)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedObjective		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iFourthAssociatedObjective)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedTeam			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iFourthAssociatedTeam)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedSpawn			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iFourthAssociatedSpawn)PRINTNL()
				
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iPropBitSet					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropBitSet)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iPropBitSet2					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropBitSet2)PRINTNL()
				
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iSpawnGroup        			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iSpawnGroup)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iSpawnSubGroup        			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iSpawnSubGroup)PRINTNL()
				
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iAggroIndexBS_Entity_Prop        = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAggroIndexBS_Entity_Prop)PRINTNL()
				
				FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
					PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iSpawnSubGroupBS[")PRINTINT(ii)PRINTSTRING("]   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iSpawnSubGroupBS[ii])PRINTNL()
				ENDFOR
				
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iCleanupTeam         		  	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iCleanupTeam)PRINTNL()
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iCleanupObjective      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iCleanupObjective)PRINTNL()
				
				PRINTSTRING("sPlacedProp[")PRINTINT(i)PRINTSTRING("].iPropPaintingIndex      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropPaintingIndex)PRINTNL()				
				
				INT iFlag = 0
				FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
					PRINTLN("sPlacedProp[", i,"].eSpawnConditionFlag[", iFlag,"] = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].eSpawnConditionFlag[iFlag]))
				ENDFOR
				
				PRINTNL()
			//ENDIF
		ENDFOR	
	ENDIF
	PRINTNL()
	PRINTNL()
	PRINTSTRING("-----------------------DYNAMIC PROPS--------------------------")PRINTNL()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps = 0
		PRINTSTRING("***** NO DYNOPROPS WERE MADE OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfDynoProps = 0")PRINTNL()
	ELSE
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps- 1)
			PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].vPos    								    = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].vPos)PRINTNL()
			PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].vRot   								    = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].vRot)PRINTNL()
			PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].fHead  								    = ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].fHead)PRINTNL()
			PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].mn     								    = ")PRINTSTRING(GET_MODEL_STING_FROM_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn))PRINTSTRING(" [")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn))PRINTSTRING(" ]")PRINTNL()
				
			PRINTLN("sPlacedDynoProp[", i, "].sDynopropTrainAttachmentData.iTrainIndex			 		= ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sDynopropTrainAttachmentData.iTrainIndex		)
			PRINTLN("sPlacedDynoProp[", i, "].sDynopropTrainAttachmentData.iCarriageIndex  				= ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sDynopropTrainAttachmentData.iCarriageIndex  	)
			PRINTLN("sPlacedDynoProp[", i, "].sDynopropTrainAttachmentData.vCarriageOffsetPosition  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sDynopropTrainAttachmentData.vCarriageOffsetPosition)
			PRINTLN("sPlacedDynoProp[", i, "].sDynopropTrainAttachmentData.vCarriageOffsetRotation  		= ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sDynopropTrainAttachmentData.vCarriageOffsetRotation)
			
			PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].iDynopropBitset    						 = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iDynopropBitset)PRINTNL()
				
			PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].iSpawnGroup        						= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSpawnGroup)PRINTNL()
			PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].iSpawnSubGroup        						= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSpawnSubGroup)PRINTNL()
			
			PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].iAggroIndexBS_Entity_DynoProp				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAggroIndexBS_Entity_DynoProp)PRINTNL()
			
			FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
				PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].iSpawnSubGroupBS[")PRINTINT(ii)PRINTSTRING("]   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSpawnSubGroupBS[ii])PRINTNL()
			ENDFOR
			FOR ii = 0 TO ciAltVarsBS_MAX-1
				PRINTLN("[AltVarsSystemSpam] - sPlacedDynoProp[", i, "].iAltVarsBitsetDynoProp[", ii, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAltVarsBitsetDynoProp[ii])
			ENDFOR
			
			INT iFlag = 0
			FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
				PRINTLN("sPlacedDynoProp[", i,"].eSpawnConditionFlag[", iFlag,"] = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].eSpawnConditionFlag[iFlag]))
			ENDFOR
			
			PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].sProximitySpawningData.iSpawnRange      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sProximitySpawningData.iSpawnRange)PRINTNL()
			PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].sProximitySpawningData.iCleanupRange      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sProximitySpawningData.iCleanupRange)PRINTNL()
			PRINTSTRING("sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].sProximitySpawningData.iRequiredPlayers     	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sProximitySpawningData.iRequiredPlayers)PRINTNL()
			
			PRINTNL()
		ENDFOR	
	ENDIF
	PRINTNL()
	PRINTNL()
	PRINTSTRING("---------------------- DIALOGUE TRIGGERS --------------")PRINTNL()
	IF g_FMMC_STRUCT.iNumberOfDialogueTriggers = 0
		PRINTSTRING("***** NO Dialogue triggers WERE MADE OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfDialogueTriggers = 0")PRINTNL()
	ELSE
		FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfDialogueTriggers -1)
			IF i < FMMC_MAX_DIALOGUES 
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iDialogueID		= ", g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueID	)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].tlDialogueTriggerAdditionalHelpText	= ", g_FMMC_STRUCT.sDialogueTriggers[i].tlDialogueTriggerAdditionalHelpText	)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iCustomStringTicker	= ", g_FMMC_STRUCT.sDialogueTriggers[i].iCustomStringTicker	)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].tlCustomVoice = ", g_FMMC_STRUCT.sDialogueTriggers[i].tlCustomVoice)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iRule			= ", g_FMMC_STRUCT.sDialogueTriggers[i].iRule			)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iTeam			= ", g_FMMC_STRUCT.sDialogueTriggers[i].iTeam			)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].vPosition		= ", g_FMMC_STRUCT.sDialogueTriggers[i].vPosition		)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].fRadius			= ", g_FMMC_STRUCT.sDialogueTriggers[i].fRadius		)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iTimeDelay 		= ", g_FMMC_STRUCT.sDialogueTriggers[i].iTimeDelay	)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].tlRoot			= ", g_FMMC_STRUCT.sDialogueTriggers[i].tlRoot	)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].tlBlock			= ", g_FMMC_STRUCT.sDialogueTriggers[i].tlBlock	)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iDialogueBitset	 = ", g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset	)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iDialogueBitset2 = ", g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2	)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iDialogueBitset3 = ", g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset3	)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iDialogueBitset4 = ", g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset4	)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iDialogueBitset5 = ", g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset5	)	
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iDialogueBitset6 = ", g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset6)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iPlayForRoleBS	= ", g_FMMC_STRUCT.sDialogueTriggers[i].iPlayForRoleBS )
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iPrerequisite	= ", g_FMMC_STRUCT.sDialogueTriggers[i].iPrerequisite	 )						
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].tl3AppendOnAggro = ", g_FMMC_STRUCT.sDialogueTriggers[i].tl3AppendOnAggro)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iPedsKillRequired = ", g_FMMC_STRUCT.sDialogueTriggers[i].iPedsKillRequired)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iObjectToCollect = ", g_FMMC_STRUCT.sDialogueTriggers[i].iObjectToCollect)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iTeamToCollectObject = ", g_FMMC_STRUCT.sDialogueTriggers[i].iTeamToCollectObject)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iContinuityID = ", g_FMMC_STRUCT.sDialogueTriggers[i].iContinuityID)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iLinkedObjectContinuityId = ", g_FMMC_STRUCT.sDialogueTriggers[i].iLinkedObjectContinuityId)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iLinkedLocationContinuityId = ", g_FMMC_STRUCT.sDialogueTriggers[i].iLinkedLocationContinuityId)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iHintCamEntityType = ", g_FMMC_STRUCT.sDialogueTriggers[i].iHintCamEntityType)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iHintCamEntityIndex = ", g_FMMC_STRUCT.sDialogueTriggers[i].iHintCamEntityIndex)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iHintCamTime = ", g_FMMC_STRUCT.sDialogueTriggers[i].iHintCamTime)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iInteractWithObj = ", g_FMMC_STRUCT.sDialogueTriggers[i].iInteractWithObj)
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iCamShotTrigger = ", g_FMMC_STRUCT.sDialogueTriggers[i].iCamShotTrigger)
				
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iAggroIndexBS_Entity_Dialog = ", g_FMMC_STRUCT.sDialogueTriggers[i].iAggroIndexBS_Entity_Dialog)
								
				INT k = 0
				FOR k = 0 TO ciDIALOGUE_PED_IN_RANGE_MAX-1
					PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iPedInRangeTrigger[", k, "] = ", g_FMMC_STRUCT.sDialogueTriggers[i].iPedInRangeTrigger[k])
				ENDFOR
				
				FOR k = 0 TO ciMAX_DIALOGUE_BIT_SETS-1
					PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iBlockingTriggers[", k, "] = ", g_FMMC_STRUCT.sDialogueTriggers[i].iBlockingTriggers[k])
				ENDFOR
				
				INT iFlag = 0
				FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
					PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].eSpawnConditionFlag[", iFlag,"] = ", ENUM_TO_INT(g_FMMC_STRUCT.sDialogueTriggers[i].eSpawnConditionFlag[iFlag]))
				ENDFOR
				
				PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iSpawnGroup = ", g_FMMC_STRUCT.sDialogueTriggers[i].iSpawnGroup )
				FOR k = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
					PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iSpawnSubGroupBS[", k, "] = ", g_FMMC_STRUCT.sDialogueTriggers[i].iSpawnSubGroupBS[k] )
				ENDFOR
				
				FOR k = 0 TO DT_CASINO_REQ_MAX-1
					PRINTLN("g_FMMC_STRUCT.sDialogueTriggers[",i,"].iVarRequirement[",k,"] = ", g_FMMC_STRUCT.sDialogueTriggers[i].iVarRequirement[k])
				ENDFOR
				FOR k = 0 TO ciAltVarsBS_MAX_DialogueTriggers-1
					PRINTLN("[AltVarsSystemSpam] - sDialogueTriggers[", i, "].iAltVarsBitsetDialogueTrigger[", k, "] = ", g_FMMC_STRUCT.sDialogueTriggers[i].iAltVarsBitsetDialogueTrigger[k])
				ENDFOR
			ENDIF
		ENDFOR
		FOR i = 0 TO 31
			PRINTLN("g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[0] ", i, " set to ", IS_BIT_SET(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[0], i))
		ENDFOR
		FOR i = 0 TO 31
			PRINTLN("g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[1] ", i, " set to ", IS_BIT_SET(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[1], i))
		ENDFOR		
	ENDIF
	
	FOR i = 0 TO FMMC_MAX_SPEAKERS-1
		PRINTLN("g_FMMC_STRUCT.sSpeakers[",i,"]	= ", g_FMMC_STRUCT.sSpeakers[i]	 )
	ENDFOR
	
	
	
	PRINTNL()
	PRINTNL()
	PRINTSTRING("----------------------WEAPONS---------------------------")PRINTNL()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
		PRINTSTRING("***** NO Weapon WERE MADE OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfWeapons = 0")PRINTNL()
	ELSE
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons -1)
			IF i < FMMC_MAX_WEAPONS
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].vPos     = ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].pt       = ")PRINTSTRING(GET_WEAPON_NAME_FROM_PT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt))PRINTSTRING(" - [")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt))PRINTSTRING(" ]")PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iSubType = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSubType)PRINTSTRING(" - [")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSubType)PRINTSTRING(" ]")PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iClips = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iClips)PRINTSTRING(" - [")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iClips)PRINTSTRING(" ]")PRINTNL()
				PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[",i,"].iCustomPickupType = ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType) 
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iPlacedBitset   				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iAssociatedTeam   			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedTeam)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iAssociatedObjective   		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedObjective)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iAssociatedSpawn       		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedSpawn)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedObjective	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSecondAssociatedObjective)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedTeam			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSecondAssociatedTeam)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iSecondAssociatedSpawn		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSecondAssociatedSpawn)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedObjective		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iThirdAssociatedObjective)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedTeam			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iThirdAssociatedTeam)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iThirdAssociatedSpawn			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iThirdAssociatedSpawn)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedObjective	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iFourthAssociatedObjective)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedTeam			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iFourthAssociatedTeam)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iFourthAssociatedSpawn		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iFourthAssociatedSpawn)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iCleanupTeam           		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCleanupTeam)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iCleanupObjective      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCleanupObjective)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iContinuityID      			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iContinuityID)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iPlacedMarkerIndex      		= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedMarkerIndex)PRINTNL()
				
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iAggroIndexBS_Entity_Weapon   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAggroIndexBS_Entity_Weapon)PRINTNL()					
				
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iSpawnGroup  	    			= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSpawnGroup)PRINTNL()

				FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
					PRINTSTRING("sPlacedWeapon[")PRINTINT(i)PRINTSTRING("].iSpawnSubGroupBS[")PRINTINT(ii)PRINTSTRING("]   = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSpawnSubGroupBS[ii])PRINTNL()
				ENDFOR
				
				FOR ii = 0 TO ciAltVarsBS_MAX-1
					PRINTLN("[AltVarsSystemSpam] - sPlacedWeapon[", i, "].iAltVarsBitsetWeapon[", ii, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAltVarsBitsetWeapon[ii])
				ENDFOR
				
				FOR ii = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
					PRINTLN("sPlacedWeapon[", i, "].eSpawnConditionFlag[", ii,"] = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].eSpawnConditionFlag[ii]))
				ENDFOR
			ENDIF
		ENDFOR
	ENDIF
	PRINTNL()
	PRINTNL()
	
	PRINTSTRING("----------------------ZONES---------------------------")PRINTNL()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfZones = 0
		PRINTSTRING("***** NO ZONES WERE MADE OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfZones = 0")PRINTNL()
	ELSE
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones -1)
			IF i < FMMC_MAX_NUM_ZONES
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].vPos[0]				= ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0])PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].vPos[1]				= ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iType					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iZoneBS				= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneBS)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].fZoneValue		= ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].fZoneValue2		= ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue2)PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iZoneStartPriority[0]	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneStartPriority[0])PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iZoneEndPriority[0]	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneEndPriority[0])PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iZoneStartPriority[1]	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneStartPriority[1])PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iZoneEndPriority[1]	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneEndPriority[1])PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iZoneStartPriority[2]	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneStartPriority[2])PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iZoneEndPriority[2]	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneEndPriority[2])PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iZoneStartPriority[3]	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneStartPriority[3])PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iZoneEndPriority[3]	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneEndPriority[3])PRINTNL()
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iAreaType					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType)PRINTNL()
				
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedZones[")PRINTINT(i)PRINTSTRING("].iAggroIndexBS_Entity_Zone = ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAggroIndexBS_Entity_Zone)PRINTNL()
				
				
				PRINTNL()
				
				FOR ii = 0 TO ciAltVarsBS_MAX-1
					PRINTLN("[AltVarsSystemSpam] - sZones[", i, "].iAltVarsBitsetZone[", ii, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAltVarsBitsetZone[ii])
				ENDFOR
				
				FOR ii = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
					PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedZones[", i, "].eSpawnConditionFlag[", ii,"] = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].eSpawnConditionFlag[ii]))
				ENDFOR
				
				PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedZones[",i,"].iSoundType = ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iSoundType)
				PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedZones[",i,"].iZoneChance = ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneChance)
				PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedZones[",i,"].iZoneResetTimerLength = ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneResetTimerLength)
				PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedZones[",i,"].fBlipRange = ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fBlipRange)
			ENDIF
		ENDFOR
	ENDIF
	PRINTNL()
	PRINTNL()
	
	PRINTSTRING("--------------------CHECKPOINTS-------------------------")PRINTNL()
	IF g_FMMC_STRUCT.iNumberOfCheckPoints = 0
		PRINTSTRING("***** NO CHECKPOINTS WERE MADE OR UPLOADED *****")PRINTNL()
		PRINTSTRING("iNumberOfCheckPoints = 0")PRINTNL()
	ELSE
		FOR i = 0 TO g_FMMC_STRUCT.iNumberOfCheckPoints
			IF i < GET_FMMC_MAX_NUM_CHECKPOINTS()
				PRINTSTRING("vCheckPoint[")PRINTINT(i)PRINTSTRING("]        = ")PRINTVECTOR(g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint)PRINTNL()
				PRINTSTRING("vCheckpointSecondary[")PRINTINT(i)PRINTSTRING("]        = ")PRINTVECTOR(g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckpointSecondary)PRINTNL()
				PRINTSTRING("fCheckPointRespawn[")PRINTINT(i)PRINTSTRING("] = ")PRINTFLOAT(g_FMMC_STRUCT.sPlacedCheckpoint[i].fCheckPointRespawn)PRINTNL()
				PRINTSTRING("fCheckPointScale[")PRINTINT(i)PRINTSTRING("] = ")PRINTFLOAT(g_FMMC_STRUCT.sPlacedCheckpoint[i].fCheckPointScale)PRINTNL()
				PRINTSTRING("fCheckPointScaleSecondary[")PRINTINT(i)PRINTSTRING("] = ")PRINTFLOAT(g_FMMC_STRUCT.sPlacedCheckpoint[i].fCheckPointScaleSecondary)PRINTNL()
				PRINTSTRING("fCheckPointVisualScale[")PRINTINT(i)PRINTSTRING("] = ")PRINTFLOAT(g_FMMC_STRUCT.sPlacedCheckpoint[i].fCheckPointVisualScale)PRINTNL()
				PRINTSTRING("iCheckpointWrongWayTime[")PRINTINT(i)PRINTSTRING("] = ")PRINTINT(g_FMMC_STRUCT.sPlacedCheckpoint[i].iCheckpointWrongWayTime)PRINTNL()
				PRINTSTRING("sCheckpointRespawn[")PRINTINT(i)PRINTSTRING("].vRespawnPos[0] = ")PRINTVECTOR(g_FMMC_STRUCT.sPlacedCheckpoint[i].sCheckpointRespawn.vRespawnPos[0])PRINTNL()
				PRINTSTRING("sCheckpointRespawn[")PRINTINT(i)PRINTSTRING("].vRespawnPos[1] = ")PRINTVECTOR(g_FMMC_STRUCT.sPlacedCheckpoint[i].sCheckpointRespawn.vRespawnPos[1])PRINTNL()
				PRINTSTRING("sCheckpointRespawn[")PRINTINT(i)PRINTSTRING("].vRespawnPos[2] = ")PRINTVECTOR(g_FMMC_STRUCT.sPlacedCheckpoint[i].sCheckpointRespawn.vRespawnPos[2])PRINTNL()
					
			ENDIF
		ENDFOR	
	ENDIF
	PRINTNL()
	PRINTNL()
	
	IF IS_THIS_A_MISSION()
		PRINTSTRING("------------------EXTRA OBJECTIVE ENTITIES---------------------")PRINTNL()
		INT j,k
		FOR i = 0 TO MAX_NUM_EXTRA_OBJECTIVE_ENTITIES - 1
			PRINTSTRING("*** EXTRA OBJECTIVE ENTITY #")PRINTINT(i)
			IF g_FMMC_STRUCT.iExtraObjectiveEntityID[i] = -1
				PRINTSTRING(" (INACTIVE!)")
			ENDIF
			PRINTSTRING(" ***")
			PRINTNL()
			PRINTSTRING("g_FMMC_STRUCT.iExtraObjectiveEntityType[")PRINTINT(i)PRINTSTRING("] 		= ")PRINTINT(g_FMMC_STRUCT.iExtraObjectiveEntityType[i])PRINTNL()
			PRINTSTRING("g_FMMC_STRUCT.iExtraObjectiveEntityID[")PRINTINT(i)PRINTSTRING("] 			= ")PRINTINT(g_FMMC_STRUCT.iExtraObjectiveEntityID[i])PRINTNL()
			FOR k = 0 TO FMMC_MAX_TEAMS - 1
				PRINTSTRING("TEAM ")PRINTINT(k+1)PRINTSTRING(":")PRINTNL()
				FOR j = 0 TO MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1
					PRINTSTRING("g_FMMC_STRUCT.iExtraObjectivePriority[")PRINTINT(i)PRINTSTRING("][")PRINTINT(j)PRINTSTRING("][")PRINTINT(k)PRINTSTRING("] 	= ")PRINTINT(g_FMMC_STRUCT.iExtraObjectivePriority[i][j][k])PRINTNL()
					PRINTSTRING("g_FMMC_STRUCT.iExtraObjectiveRule[")PRINTINT(i)PRINTSTRING("][")PRINTINT(j)PRINTSTRING("][")PRINTINT(k)PRINTSTRING("] 		= ")PRINTINT(g_FMMC_STRUCT.iExtraObjectiveRule[i][j][k])PRINTNL()
				ENDFOR
			ENDFOR
			PRINTNL()
		ENDFOR
		PRINTNL()
		
		PRINTSTRING("------------------DOORS & DUMMY DOORS---------------------")PRINTNL()
		FOR i = 0 TO FMMC_MAX_NUM_DOORS - 1			
			PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sSelectedDoor.vPos[")PRINTINT(i)PRINTSTRING("] 					= ")PRINTVECTOR(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].vPos)PRINTNL()
			PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sSelectedDoor.mn[")PRINTINT(i)PRINTSTRING("]				        = ")PRINTSTRING(GET_MODEL_STING_FROM_TYPE(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel))PRINTSTRING(" [")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel))PRINTSTRING(" ]")PRINTNL()
			IF IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel)
			AND g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel != DUMMY_MODEL_FOR_SCRIPT					
				PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sSelectedDoor.mn[")PRINTINT(i)PRINTSTRING("]                 = ")PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel))PRINTNL()
			ENDIF
			PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sSelectedDoor.fOpenRatio[")PRINTINT(i)PRINTSTRING("] 					= ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio)PRINTNL()
			PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sSelectedDoor.sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio[")PRINTINT(i)PRINTSTRING("] 		= ")PRINTFLOAT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio)PRINTNL()
			PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sSelectedDoor.iUpdateRule[")PRINTINT(i)PRINTSTRING("] 					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].iUpdateRule)PRINTNL()
			PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sSelectedDoor.iUpdateTeam[")PRINTINT(i)PRINTSTRING("] 					= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].iUpdateTeam)PRINTNL()
			PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sSelectedDoor.iBitSet[")PRINTINT(i)PRINTSTRING("] 						= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].iBitSet)PRINTNL()
			PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sSelectedDoor.iAggroIndexBS_Entity_Door[")PRINTINT(i)PRINTSTRING("] 	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].iAggroIndexBS_Entity_Door)PRINTNL()
			PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sSelectedDoor.iDoorAnimDynopropIndex[")PRINTINT(i)PRINTSTRING("] 	= ")PRINTINT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].iDoorAnimDynopropIndex)PRINTNL()
			
			FOR ii = 0 TO ciAltVarsBS_MAX-1
				PRINTLN("[AltVarsSystemSpam] - sSelectedDoor[", i, "].iAltVarsBitsetDoor[", ii, "] = ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].iAltVarsBitsetDoor[ii])
			ENDFOR
		ENDFOR
	
		PRINTSTRING("------------------SPEAKER LIST---------------------")PRINTNL()
		FOR i = 0 TO FMMC_MAX_SPEAKERS-1
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sSpeakers[i])
				PRINTLN("Speaker		[", i ,"] = ", g_FMMC_STRUCT.sSpeakers[i])
				PRINTLN("iSpeakerPlayers[", i ,"] = ", g_FMMC_STRUCT.iSpeakerPlayers[i])
				PRINTNL()
			ENDIF
		ENDFOR
		PRINTNL()
	
	ENDIF
	
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		INT j,k
		FOR j = 0 TO FMMC_MAX_RESTART_CHECKPOINTS - 1
			FOR k = 0 TO FMMC_MAX_RESTART_POINTS_PER_RESTART - 1
				PRINTSTRING("g_FMMC_STRUCT.vTeamRestartPoint[")PRINTINT(i)PRINTSTRING("][")PRINTINT(j)PRINTSTRING("][")PRINTINT(k)PRINTSTRING("] = ")PRINTVECTOR(g_FMMC_STRUCT.vTeamRestartPoint[i][j][k])PRINTNL()
			ENDFOR
		ENDFOR
		PRINTSTRING("g_FMMC_STRUCT.iTeamParachuteTint[")PRINTINT(i)PRINTSTRING("] = ")PRINTINT(g_FMMC_STRUCT.iTeamParachuteTint[i])PRINTNL()		
	ENDFOR
	
	PRINTLN("------------------Player Weapon Inventories---------------------")
	FOR i = 0 TO FMMC_MAX_INVENTORIES-1
		PRINTLN("PRINT_INVENTORY_STRUCTS iInventoryIndex: ", i, " -----------------")
		PRINT_INVENTORY_STRUCT(g_FMMC_STRUCT.sPlayerWeaponInventories[i])
	ENDFOR
	
	PRINTLN("g_FMMC_STRUCT.sCasinoApartment.iCasinoApartArcade = ", g_FMMC_STRUCT.sCasinoApartment.iCasinoApartArcade)
	PRINTLN("g_FMMC_STRUCT.sCasinoApartment.iCasinoApartPattern = ", g_FMMC_STRUCT.sCasinoApartment.iCasinoApartPattern)
	PRINTLN("g_FMMC_STRUCT.sCasinoApartment.iCasinoApartColour = ", g_FMMC_STRUCT.sCasinoApartment.iCasinoApartColour)
	PRINTLN("g_FMMC_STRUCT.sCasinoApartment.iCasinoApartParty = ", g_FMMC_STRUCT.sCasinoApartment.iCasinoApartParty)
	PRINTLN("g_FMMC_STRUCT.sCasinoApartment.iCasinoPartyRadio = ", g_FMMC_STRUCT.sCasinoApartment.iCasinoPartyRadio)
	
	PRINT_ARENA_INFO(g_FMMC_STRUCT.sArenaInfo)
	
	IF g_bDebugPrintMissionDetailsToFile
		DEBUG_EXPORT_ROCKSTAR_MISSIONS_DETAILS(iLesRating)
	ENDIF
ENDPROC
#ENDIF

/// PURPOSE: Checks to see if we are on mission creator pause menu. Function will reset bool if we are not on pause menu
FUNC BOOL IS_MISSION_CREATOR_PAUSE_MENU_ACTIVE()

	IF IS_PAUSE_MENU_ACTIVE()
		IF g_bMissionCreatorPauseMenuActive
			RETURN TRUE						// We need to prioritise data loading for pause menu
		ENDIF
	ELSE
		IF g_bMissionCreatorPauseMenuActive
		
			PRINTLN("[JA@PAUSEMENU] Resetting bool g_bMissionCreatorPauseMenuActive since pause menu no longer active")
		
			g_bMissionCreatorPauseMenuActive = FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                  //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


// Added by Kevin> This willl need to be removed
FUNC BOOL CHECK_IF_THIS_IS_A_CLOUD_RACE()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	
		IF 	g_bturn_on_basejumps_cloud 		= FALSE
			
				#IF IS_DEBUG_BUILD
				
					NET_NL() NET_PRINT("g_bturn_on_basejumps_cloud 		= FALSE ")	
			
				#ENDIF
				RETURN FALSE
		ELSE
				#IF IS_DEBUG_BUILD
				
					NET_NL() NET_PRINT("g_bturn_on_basejumps_cloud 		= TRUE ")	
			
				#ENDIF
				RETURN TRUE
		
		ENDIF
			
		
	ENDIF
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_DM_CUSTOM_GAME_SETTINGS()
	INT iLoop, iArrayLoop
	
	PRINTLN("PRINT_DM_CUSTOM_GAME_SETTINGS")
	
	PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.iFFALives: ", g_FMMC_STRUCT.sDMCustomSettings.iFFALives)
	FOR iLoop = 0 TO g_FMMC_STRUCT.iMaxNumberOfTeams-1
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.iTeamTargetScore[",iLoop,"]: ", g_FMMC_STRUCT.sDMCustomSettings.iTeamTargetScore[iLoop])
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.iTeamLives[",iLoop,"]: ", g_FMMC_STRUCT.sDMCustomSettings.iTeamLives[iLoop])
	ENDFOR
	
	PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.iWinBiasTeam: ", g_FMMC_STRUCT.sDMCustomSettings.iWinBiasTeam)
	
	PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings: ", g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings)
	
	FOR iLoop = 0 TO g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings-1
		PRINTLN("PRINT_DM_CUSTOM_GAME_SETTINGS - Modifier set ", iLoop, "-------------------------------------------------")
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iModifierSetBS: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].iModifierSetBS)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.eWhoType: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.eWhoType)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iTeamBS: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iTeamBS)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.eConditionType: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.eConditionType)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.eScoreCondition: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.eScoreCondition)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iScoreConditionToCheck: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iScoreConditionToCheck)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iScorePercentage: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iScorePercentage)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iTimerConditionLength: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iTimerConditionLength)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iTimerPercentage: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iTimerPercentage)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.eApplyType: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.eApplyType)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iKill: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iKill)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iHeadshot: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iHeadshot)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iMelee: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iMelee)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iVehicle: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iVehicle)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iPositionFirst: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iPositionFirst)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iPositionLast: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iPositionLast)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iFriendlyKill: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iFriendlyKill)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iSuicide: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iSuicide)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iOutOfBounds: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iOutOfBounds)
		
		FOR iArrayLoop = 0 TO g_FMMC_STRUCT.iMaxNumberOfTeams-1
			PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iSpecificTeam: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iSpecificTeam[iArrayLoop])
		ENDFOR
		
		FOR iArrayLoop = 0 TO ciDM_WEAPON_GROUP_MODIFIER_MAX-1
			IF iArrayLoop != ciDM_WEAPON_GROUP_MODIFIER_MELEE
				PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iSpecificWeaponGroup: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierScoring.iSpecificWeaponGroup[iArrayLoop])
			ENDIF
			PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].iWeaponGroupDamageModifier: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].iWeaponGroupDamageModifier[iArrayLoop])
		ENDFOR
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].eInfiniteAmmoType: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].eInfiniteAmmoType)
		
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].iHealthScale: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].iHealthScale)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].iStartingHealthPercentage: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].iStartingHealthPercentage)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].eHealthMaxRegen: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].eHealthMaxRegen)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].eHealthRegenSpeed: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].eHealthRegenSpeed)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].eExplosiveResistance: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].eExplosiveResistance)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierBlips.eBlipType: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierBlips.eBlipType)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierBlips.iCampTimer: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierBlips.iCampTimer)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierMovement.eMovementRestriction: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierMovement.eMovementRestriction)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierMovement.eMovementSpeed: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierMovement.eMovementSpeed)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnKill.iExtraHealth: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnKill.iExtraHealth)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnKill.iExtraArmour: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnKill.iExtraArmour)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnKill.eAmmoType: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnKill.eAmmoType)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnKill.iGiveAmmoTo: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnKill.iGiveAmmoTo)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnKill.iAmmoQuantity: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnKill.iAmmoQuantity)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnHit.iExtraHealth: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnHit.iExtraHealth)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnHit.iExtraArmour: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnHit.iExtraArmour)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnHit.eAmmoType: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnHit.eAmmoType)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnHit.iGiveAmmoTo: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnHit.iGiveAmmoTo)
		PRINTLN("g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnHit.iAmmoQuantity: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierOnHit.iAmmoQuantity)
		
		//Keep this at the bottom it dumps a lot of info
		PRINT_INVENTORY_STRUCT(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierInventory)
		PRINTLN("-------------------------------------------------")
	ENDFOR
ENDPROC
#ENDIF


FUNC BOOL FMMC_LOAD_MISSION_DATA(	GET_UGC_CONTENT_STRUCT	&sGetUGC_content, 
									STRING 					szContentID, 
									INT 					&iBitSet, 
									INT 					iMissionType = -1, 
									BOOL				 	bOnPlayList = FALSE,
									BOOL				 	bGetPhoto = FALSE,
									BOOL bDoRockStarUGCCheck = FALSE,
									BOOL bDefaultOptionsLoaded = FALSE,
									BOOL bDoPauseMenuCheck = TRUE, 
									BOOL bForceUseOfflineContent = FALSE, 
									BOOL bLatest = FALSE, 
									BOOL bClearNextContentID = TRUE, 
									BOOL bStrandLoad = FALSE,
									BOOL bStaggered = FALSE)
	
	#IF IS_DEBUG_BUILD
	IF TRANSITION_SESSION_FORCE_LATEST_VERSION_OF_MISSION()
		bLatest = TRUE
	ENDIF
	#ENDIF
	
	//If it is a mini game then there is no data to load from the cloud
	IF iMissionType > FMMC_TYPE_MG
	AND iMissionType <= FMMC_TYPE_MG_ARM_WRESTLING
	OR iMissionType = FMMC_TYPE_MG_PILOT_SCHOOL
		sGetUGC_content.bSuccess = TRUE
		SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
		IF NOT IS_BIT_SET(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
			PRINTLN("FMMC_LOAD_MISSION_DATA - iMissionType > FMMC_TYPE_MG - ciFMMC_MISSION_DATA_BitSet_ReadyToVote")			
		ENDIF		
		SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
		RETURN TRUE
	ENDIF
	
	//The transition session is restarting
	IF (IS_TRANSITION_SESSION_RESTARTING()
	//And the mission was loaded before the launch
	AND HAS_TRANSITION_SESSION_MISSION_LOADED_BEFORE_LAUNCH())
	
	//OR The mission is loaded Already
	OR HAS_TRANSITION_SESSIONS_MISSIONLOADED()
		IF NOT IS_BIT_SET(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
			PRINTLN("FMMC_LOAD_MISSION_DATA - HAS_TRANSITION_SESSIONS_MISSIONLOADED - HAS_TRANSITION_SESSION_MISSION_LOADED_BEFORE_LAUNCH - HAS_TRANSITION_SESSIONS_MISSIONLOADED - ciFMMC_MISSION_DATA_BitSet_ReadyToVote")			
		ENDIF		
		SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
		SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
		SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
		#IF IS_DEBUG_BUILD
		IF IS_TRANSITION_SESSION_RESTARTING()
			PRINTLN("FMMC_LOAD_MISSION_DATA - IS_TRANSITION_SESSION_RESTARTING")
		ENDIF		
		IF HAS_TRANSITION_SESSIONS_MISSIONLOADED()
			PRINTLN("FMMC_LOAD_MISSION_DATA - HAS_TRANSITION_SESSIONS_MISSIONLOADED")
		ENDIF
		IF HAS_TRANSITION_SESSION_MISSION_LOADED_BEFORE_LAUNCH()
			PRINTLN("FMMC_LOAD_MISSION_DATA - HAS_TRANSITION_SESSION_MISSION_LOADED_BEFORE_LAUNCH")
		ENDIF
		#ENDIF
		sGetUGC_content.bSuccess = TRUE
		RETURN TRUE
	ENDIF
	
	//If we are loading the default options
	IF bDefaultOptionsLoaded = TRUE
		//If the falg to say we are ready to vote is not set
		IF NOT IS_BIT_SET(iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
			SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
		ENDIF		
//		//And the mission data is not loaded
//		IF NOT IS_BIT_SET(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
//			//Clear out any old messy data
//			IF NOT IS_BIT_SET(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
//				PRINTLN("FMMC_LOAD_MISSION_DATA - CLEAR_FMMC_STRUCT - bDefaultOptionsLoaded = TRUE")
//				CLEAR_FMMC_STRUCT()
//				SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
//				RESET_UGC_LOAD_VARS(sGetUGC_content)
//			ENDIF
//		ENDIF
		RETURN TRUE
	ENDIF
	
	
	//don't download if on a playlist. 
	IF bOnPlayList
		IF NOT HAS_PLAYLIST_MISSION_HAS_LOADED()
			PRINTLN("FMMC_LOAD_MISSION_DATA - HAS_PLAYLIST_MISSION_HAS_LOADED = FALSE")
			RETURN FALSE
		ENDIF
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		IF iMissionType = FMMC_TYPE_BASE_JUMP
//			CHECK_IF_THIS_IS_A_CLOUD_RACE()
//			IF g_bturn_on_basejumps_cloud = FALSE
//						
//				#IF IS_DEBUG_BUILD
//				
//						NET_NL() NET_PRINT("FMMC_LOAD_MISSION_DATA g_bturn_on_basejumps_cloud = FALSE ")	
//			
//				#ENDIF
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	#ENDIF
	//Reset the loading Icon BOOL
	sGetUGC_content.bRunLoadingIcon = FALSE
	
	IF bDoPauseMenuCheck
		IF IS_MISSION_CREATOR_PAUSE_MENU_ACTIVE()
			IF IS_BIT_SET(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
				SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
				RETURN TRUE
			ELSE
				PRINTLN("FMMC_LOAD_MISSION_DATA - IS_MISSION_CREATOR_PAUSE_MENU_ACTIVE = TRUE")
				RETURN FALSE			
			ENDIF
		ENDIF	
	ENDIF	

	
	//to stop this load stoping over the rockstar content.
	IF bDoRockStarUGCCheck
		IF IS_CLOUD_REFRESH_IS_USING_UGC()
			PRINTLN("FMMC_LOAD_MISSION_DATA - GET_FM_UGC_HAS_FINISHED = FALSE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	//If the data has not been loaded
	IF NOT IS_BIT_SET(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
	
		//Clear out any old messy data
		IF NOT IS_BIT_SET(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
			PRINTSTRING("FMMC_LOAD_MISSION_DATA...  CLEAR_FMMC_STRUCT")PRINTNL()
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] FMMC_LOAD_MISSION_DATA...  CLEAR_FMMC_STRUCT 1")#ENDIF
			IF NOT bStrandLoad
				CLEAR_FMMC_STRUCT(DEFAULT, DEFAULT, bClearNextContentID)
			ENDIF
			SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
			RESET_UGC_LOAD_VARS(sGetUGC_content)
			sGetUGC_content.bSuccess = FALSE
		ENDIF
		
		//Load all the lovely new FMMC data in to the globals
		IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, szContentID, UGC_TYPE_GTA5_MISSION, TRUE, FALSE, bGetPhoto, FALSE, FALSE, TRUE, bForceUseOfflineContent, bLatest, bClearNextContentID, FALSE, bStrandLoad, bStaggered)		
			PRINTLN("[TS] *****************************************************************")
			PRINTLN("[TS] * MISSION DATA - ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_FMMC_MISSION_TYPE_STRING(g_FMMC_STRUCT.iMissionType)), " - ", g_FMMC_STRUCT.tl31LoadedContentID, " - ", g_FMMC_STRUCT.tl63MissionName, " T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * MISSION DATA - ")#ENDIF
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] - g_FMMC_STRUCT.vStartPos = ", g_FMMC_STRUCT.vStartPos)#ENDIF
			PRINTLN("[TS] *****************************************************************")
			PRINTSTRING("SET_UP_FMMC_MISSION...  Data Loaded")PRINTNL()
			PRINTSTRING("iMissionType 		= ")PRINTINT(g_FMMC_STRUCT.iMissionType)PRINTNL()
			//Set the correct globals up depending on what type was created
			IF IS_THIS_A_MISSION()
			OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_CRATE_DROP
			OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_GANGHIDEOUT
			OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
			
				IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
					g_FMMC_STRUCT.vStartPos = GANGOPS_GET_GANG_OPS_BOARD_START_POS()
					PRINTLN("[TS][GangOps] - FMMC_LOAD_MISSION_DATA - GangOps mission, clobbering location - g_FMMC_STRUCT.vStartPos = ", g_FMMC_STRUCT.vStartPos)
				ENDIF
				#IF FEATURE_CASINO_HEIST
				IF IS_THIS_MISSION_A_CASINO_HEIST_INTRO_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
					g_FMMC_STRUCT.vStartPos = GET_CASINO_HEIST_BOARD_START_POS()
					PRINTLN("[TS][CASINO_HEIST] - FMMC_LOAD_MISSION_DATA - Casino Heist mission, clobbering location - g_FMMC_STRUCT.vStartPos = ", g_FMMC_STRUCT.vStartPos)
				ENDIF
				#ENDIF
				#IF FEATURE_HEIST_ISLAND
				IF IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
					g_FMMC_STRUCT.vStartPos = GET_HEIST_ISLAND_BOARD_START_POS()
					PRINTLN("[TS][HEIST_ISLAND] - FMMC_LOAD_MISSION_DATA - Heist Island mission, clobbering location - g_FMMC_STRUCT.vStartPos = ", g_FMMC_STRUCT.vStartPos)
				ENDIF
				#ENDIF
				IF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)
					g_FMMC_STRUCT.vStartPos = GET_TUNER_BOARD_START_POS()
					PRINTLN("[TS][HEIST_ISLAND] - FMMC_LOAD_MISSION_DATA - Tuner Robbery mission, clobbering location - g_FMMC_STRUCT.vStartPos = ", g_FMMC_STRUCT.vStartPos)
				ENDIF
				IF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ()
					g_FMMC_STRUCT.vStartPos = GET_FIXER_STORY_START_POS()
					PRINTLN("[TS][FIXER] - FMMC_LOAD_MISSION_DATA - Fixer mission, clobbering location - g_FMMC_STRUCT.vStartPos = ", g_FMMC_STRUCT.vStartPos)
				ENDIF
				IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					g_FMMC_STRUCT.vStartPos = GET_CAYO_PERICO_SERIES_START_POS()
					PRINTLN("[TS][CAYO] - FMMC_LOAD_MISSION_DATA - Cayo perico series, clobbering location - g_FMMC_STRUCT.vStartPos = ", g_FMMC_STRUCT.vStartPos)
				ENDIF
				IF IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
					g_FMMC_STRUCT.vStartPos = GET_ULP_MISSION_LOCATION()
					PRINTLN("[TS][CAYO] - FMMC_LOAD_MISSION_DATA - ULP Mission, clobbering location - g_FMMC_STRUCT.vStartPos = ", g_FMMC_STRUCT.vStartPos)
				ENDIF
				
				#IF FEATURE_DLC_2_2022
				IF IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
					g_FMMC_STRUCT.vStartPos = GET_XMAS22_STORY_START_POS()
					PRINTLN("[TS][CAYO] - FMMC_LOAD_MISSION_DATA - Xmas22 Story Mission, clobbering location - g_FMMC_STRUCT.vStartPos = ", g_FMMC_STRUCT.vStartPos)
				ENDIF
				#ENDIF
				
				IF g_FMMC_STRUCT.iMaxNumberOfTeams 	< 1
					g_FMMC_STRUCT.iMaxNumberOfTeams	= 1
				ENDIF
				IF g_FMMC_STRUCT.iNumberOfTeams 	< 1
					g_FMMC_STRUCT.iNumberOfTeams	= 1
				ENDIF
				IF NOT ARE_ALL_TEAMS_EVEN()
					CLEAR_LAST_MISSION_WAS_TEAM()
				ENDIF
				//GlobalplayerBD_FM[iMyGBDslot].iCurrentMissionType = FMMC_TYPE_MISSION
				PRINTLN("-------------------------------------------------------")
				PRINTLN("-------------------------------------------------------")
				PRINTLN("--                                                   --")
				PRINTLN("--                   MISSION DATA                    --")
				PRINTLN("--                                                   --")
				PRINTLN("-------------------------------------------------------")
				PRINTLN("-------------------------------------------------------")
			//Set the deathmatch global 
			ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
				//GlobalplayerBD_FM[iMyGBDslot].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
				g_sDM_SB_CoronaOptions.iLocation 			= FMMC_LOCATION						 	
				g_sDM_SB_CoronaOptions.iTarget				= g_FMMC_STRUCT.iTargetScore		
				g_sDM_SB_CoronaOptions.iWeapons				= g_FMMC_STRUCT_ENTITIES.iWeaponPallet	
//				g_sDM_SB_CoronaOptions.iAim	 				= g_FMMC_STRUCT.iAutoAim	
				g_sDM_SB_CoronaOptions.iPolice 				= g_FMMC_STRUCT.iPolice		
				g_sDM_SB_CoronaOptions.iBlips				= g_FMMC_STRUCT.iBlips				
				g_sDM_SB_CoronaOptions.iTags					= g_FMMC_STRUCT.iTags				
				g_sDM_SB_CoronaOptions.iRadio				= g_FMMC_STRUCT.iRadio				
				g_sDM_SB_CoronaOptions.iVoice				= g_FMMC_STRUCT.iVoice	
				g_sDM_SB_CoronaOptions.iSpawnTime			= g_FMMC_STRUCT.iRespawnTime				  		
				g_sDM_SB_CoronaOptions.iDuration 			= g_FMMC_STRUCT.iRoundTime				
//				g_sDM_SB_CoronaOptions.iAim					= g_FMMC_STRUCT.iNumLaps				
				g_sDM_SB_CoronaOptions.iPeds					= g_FMMC_STRUCT.iDMPedRule		
				PRINTLN("-------------------------------------------------------")
				PRINTLN("-------------------------------------------------------")
				PRINTLN("--                                                   --")
				PRINTLN("--                 DEATHMATCH DATA                   --")
				PRINTLN("--                                                   --")
				PRINTLN("-------------------------------------------------------")
				PRINTLN("-------------------------------------------------------")
				PRINTLN(" g_sDM_SB_CoronaOptions.iTypeOfDeathmatch   		= ",  g_sDM_SB_CoronaOptions.iTypeOfDeathmatch)
				PRINTLN(" g_sDM_SB_CoronaOptions.iNumberOfTeams   			= ",  g_sDM_SB_CoronaOptions.iNumberOfTeams)
				PRINTLN(" g_sDM_SB_CoronaOptions.bIsTeamDM  				= ",  g_sDM_SB_CoronaOptions.bIsTeamDM)
				PRINTLN(" g_sDM_SB_CoronaOptions.bOnePlayerDeathmatch   	= ",  g_sDM_SB_CoronaOptions.bOnePlayerDeathmatch)
				PRINTLN(" g_sDM_SB_CoronaOptions.bResetDeathmatchOptions   	= ",  g_sDM_SB_CoronaOptions.bResetDeathmatchOptions)
				PRINTLN(" g_sDM_SB_CoronaOptions.iBalanceTeams   			= ",  g_sDM_SB_CoronaOptions.iBalanceTeams)
				PRINTLN(" g_sDM_SB_CoronaOptions.iHealthBar   				= ",  g_sDM_SB_CoronaOptions.iHealthBar)
				PRINTLN(" g_sDM_SB_CoronaOptions.iBlips   					= ",  g_sDM_SB_CoronaOptions.iBlips)
				PRINTLN(" g_sDM_SB_CoronaOptions.iStartWeapon   			= ",  g_sDM_SB_CoronaOptions.iStartWeapon)
								
				PRINTLN(" g_sDM_SB_CoronaOptions.iLocation   = ",  g_sDM_SB_CoronaOptions.iLocation)
				PRINTLN(" g_sDM_SB_CoronaOptions.iTarget     = ",  g_sDM_SB_CoronaOptions.iTarget)
				PRINTLN(" g_sDM_SB_CoronaOptions.iWeapons    = ",  g_sDM_SB_CoronaOptions.iWeapons)
//				PRINTLN(" g_sDM_SB_CoronaOptions.iAim        = ",  g_sDM_SB_CoronaOptions.iAim)
				PRINTLN(" g_sDM_SB_CoronaOptions.iPolice     = ",  g_sDM_SB_CoronaOptions.iPolice)
				PRINTLN(" g_sDM_SB_CoronaOptions.iBlips      = ",  g_sDM_SB_CoronaOptions.iBlips)
				PRINTLN(" g_sDM_SB_CoronaOptions.iTags       = ",  g_sDM_SB_CoronaOptions.iTags)
				PRINTLN(" g_sDM_SB_CoronaOptions.iRadio      = ",  g_sDM_SB_CoronaOptions.iRadio)
				PRINTLN(" g_sDM_SB_CoronaOptions.iVoice      = ",  g_sDM_SB_CoronaOptions.iVoice)
				PRINTLN(" g_sDM_SB_CoronaOptions.iSpawnTime  = ",  g_sDM_SB_CoronaOptions.iSpawnTime)
				PRINTLN(" g_sDM_SB_CoronaOptions.iDuration   = ",  g_sDM_SB_CoronaOptions.iDuration)
//				PRINTLN(" g_sDM_SB_CoronaOptions.iAim        = ",  g_sDM_SB_CoronaOptions.iAim)
				PRINTLN(" g_sDM_SB_CoronaOptions.iPeds       = ",  g_sDM_SB_CoronaOptions.iPeds)
				
				#IF IS_DEBUG_BUILD
				PRINT_DM_CUSTOM_GAME_SETTINGS()
				#ENDIF
				
			//For the race
			ELIF IS_THIS_A_RACE()
				//GlobalplayerBD_FM[iMyGBDslot].iCurrentMissionType = FMMC_TYPE_RACE
//				g_sRC_SB_CoronaOptions.iNumRacers			= g_FMMC_STRUCT.iNumParticipants
				//g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles 		= FMMC_MAX_VEHICLES
				g_sRC_SB_CoronaOptions.iRaceTrack 		= FMMC_RACE
				g_sRC_SB_CoronaOptions.iLaps				= (g_FMMC_STRUCT.iNumLaps + 1)
				IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartingGrid)
					PRINTLN("IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartingGrid) = TRUE")
					g_FMMC_STRUCT.vStartingGrid = g_FMMC_STRUCT.vStartPos
				ENDIF
				PRINTLN("-------------------------------------------------------")
				PRINTLN("-------------------------------------------------------")
				PRINTLN("--                                                   --")
				PRINTLN("--                 RACE DATA                         --")
				PRINTLN("--                                                   --")
				PRINTLN("-------------------------------------------------------")
				PRINTLN("-------------------------------------------------------")
//				PRINTLN(" g_sRC_SB_CoronaOptions.iNumRacers     = ", g_sRC_SB_CoronaOptions.iNumRacers)
				PRINTLN(" g_FMMC_STRUCT.iRaceType             = ", g_FMMC_STRUCT.iRaceType)
				PRINTLN(" g_FMMC_STRUCT.iRaceClass            = ", g_FMMC_STRUCT.iRaceClass)
				PRINTLN(" g_FMMC_STRUCT.iRaceClassBitSet      = ", g_FMMC_STRUCT.iRaceClassBitSet)

				
				PRINTLN(" g_sRC_SB_CoronaOptions.iRaceTrack  = ", g_sRC_SB_CoronaOptions.iRaceTrack)
				PRINTLN(" g_sRC_SB_CoronaOptions.iLaps 	     = ", g_sRC_SB_CoronaOptions.iLaps)
				PRINTLN(" g_FMMC_STRUCT.iNumberOfCheckPoints = ", g_FMMC_STRUCT.iNumberOfCheckPoints)
				PRINTLN(" g_FMMC_STRUCT.vStartPos 	         = ", g_FMMC_STRUCT.vStartPos)
				PRINTLN(" g_FMMC_STRUCT.vStartingGrid 	     = ", g_FMMC_STRUCT.vStartingGrid)
				PRINTLN(" g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles    = ", g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles )
			ENDIF
			
			//Print out all the data so we can see it in debug
			#IF IS_DEBUG_BUILD
				PRINT_FMMC_DEBUGDATA(TRUE, FALSE, 0)
			#ENDIF
			
			//Set the bit that the mission has loaded 
			SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
			//Clear the bit to say that we cleand out the struct so it can be cleaned next time we use this proc
			CLEAR_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
			SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
			IF sGetUGC_content.bSuccess
				#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] - sGetUGC_content.bSuccess = TRUE")#ENDIF
				SET_TRANSITION_SESSION_MISSION_LOADED_BEFORE_LAUNCH()
				SET_TRANSITION_SESSIONS_MISSION_HAS_LOADED()
				//If the mission has loaded and the transition session is good to go. 
				IF IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
					//If we're not set up as a head to head then set us up
					TRANSITION_SESSIONS_SET_UP_FOR_HEAD_2_HEAD(g_FMMC_STRUCT.iNumParticipants)				
				ENDIF	
				// KGM 29/5/13: Store that the most recent load was successful
				g_FMMC_STRUCT.bMostRecentLoadSuccessful = TRUE
				PRINTLN("FMMC_LOAD_MISSION_DATA - ...KGM MP: Setting g_FMMC_STRUCT.bMostRecentLoadSuccessful = TRUE (used by Mission Triggerer's safety cloud load request)")
			ELSE
				CLEAR_TRANSITION_SESSIONS_MISSION_HAS_LOADED()
				#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] - sGetUGC_content.bSuccess = FALSE")#ENDIF
			ENDIF
			PRINTLN("FMMC_LOAD_MISSION_DATA - MISSION DATA LOADED")
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] - FMMC_LOAD_MISSION_DATA - MISSION DATA LOADED")#ENDIF
			//if there are more missions in the strand then set the flag that this is going to be a strand mission
			IF NOT CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
				SET_THIS_IS_A_STRAND_MISSION()
			ENDIF
			RETURN TRUE
		ELSE
			sGetUGC_content.bRunLoadingIcon = TRUE
		ENDIF
	ELSE
	//Everything done
		SET_BIT(iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
		// KGM 29/5/13: If the last load was successful then set the bSuccess flag too
		IF (g_FMMC_STRUCT.bMostRecentLoadSuccessful)
			sGetUGC_content.bSuccess = TRUE
			PRINTLN("\nFMMC_LOAD_MISSION_DATA - ...KGM MP: Most Recent Load was successful so set the sGetUGC_content.bSuccess flag (as used by Mission Triggerer's safety cloud load request)")
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] - FMMC_LOAD_MISSION_DATA - ...KGM MP: Most Recent Load was successful so set the sGetUGC_content.bSuccess flag (as used by Mission Triggerer's safety cloud load r")#ENDIF
		ELSE
			PRINTLN("\nFMMC_LOAD_MISSION_DATA - ...KGM MP: Most Recent Load must have failed so not setting the sGetUGC_content.bSuccess flag (as used by Mission Triggerer's safety cloud load request)")
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] - FMMC_LOAD_MISSION_DATA - ...KGM MP: Most Recent Load must have failed so not setting the sGetUGC_content.bSuccess flag (as used by Mission T")#ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL FMMC_LOAD_MISSION_DATA_FOR_TRANSITION(GET_UGC_CONTENT_STRUCT	&sGetUGC_content, 
									STRING 					szContentID, 
									INT 					&iBitSet, 
									INT 					iMissionType = -1, 
									BOOL				 	bOnPlayList = FALSE,
									BOOL				 	bGetPhoto = FALSE,
									BOOL					bDoRockStarUGCCheck = FALSE,
									BOOL					bDefaultOptionsLoaded = FALSE,
									BOOL					bDoPauseMenuCheck = TRUE, 
									BOOL					bForceUseOfflineContent = FALSE, 
									BOOL					bLatest = FALSE, 
									BOOL					bClearNextContentID = TRUE,
									BOOL					bStrandLoad = FALSE,
									BOOL					bStaggered = FALSE)
	RETURN FMMC_LOAD_MISSION_DATA(sGetUGC_content, szContentID, iBitSet, iMissionType, bOnPlayList, bGetPhoto, bDoRockStarUGCCheck, bDefaultOptionsLoaded, bDoPauseMenuCheck, bForceUseOfflineContent, bLatest, bClearNextContentID, bStrandLoad, bStaggered)
ENDFUNC			


//PROC SET_FM_LAUNCHER_PLAYER_GAME_STATE(PlayerBroadcastData &PBDpassed[], MAIN_SERVER_MISSION_STAGES msmsPassed)
//	#IF IS_DEBUG_BUILD
//		IF msmsPassed = GAME_STATE_CREATE_ENTITIES
//			NET_PRINT("BWW... SET_FM_LAUNCHER_PLAYER_GAME_STATE - GAME_STATE_CREATE_ENTITIES") NET_NL()
//		ELIF msmsPassed = GAME_STATE_WARP
//			NET_PRINT("BWW... SET_FM_LAUNCHER_PLAYER_GAME_STATE - GAME_STATE_WARP") NET_NL()
//		ELIF msmsPassed = GAME_STATE_COUNTDOWN
//			NET_PRINT("BWW... SET_FM_LAUNCHER_PLAYER_GAME_STATE - GAME_STATE_COUNTDOWN") NET_NL()
//		ELIF msmsPassed = GAME_STATE_GAME_IN_PROGRESS
//			NET_PRINT("BWW... SET_FM_LAUNCHER_PLAYER_GAME_STATE - GAME_STATE_GAME_IN_PROGRESS") NET_NL()
//		ELIF msmsPassed = GAME_STATE_RESULTS
//			NET_PRINT("BWW... SET_FM_LAUNCHER_PLAYER_GAME_STATE - GAME_STATE_RESULTS") NET_NL()
//		ELIF msmsPassed = GAME_STATE_FAILED
//			NET_PRINT("BWW... SET_FM_LAUNCHER_PLAYER_GAME_STATE - GAME_STATE_FAILED") NET_NL()
//		ENDIF
//	#ENDIF
//	PBDpassed[PARTICIPANT_ID_TO_INT()].GameState = msmsPassed
//ENDPROC


PROC CLEAN_UP_FMMC_ENTITIES(FMMC_SERVER_DATA_STRUCT &sSDSpassed)
	INT i 
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
		IF NETWORK_DOES_NETWORK_ID_EXIST(sSDSpassed.niPed[i])		
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSDSpassed.niPed[i])
				CLEANUP_NET_ID(sSDSpassed.niPed[i])
			ENDIF	
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
		IF NETWORK_DOES_NETWORK_ID_EXIST(sSDSpassed.niVehicle[i])		
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSDSpassed.niVehicle[i])
				CLEANUP_NET_ID(sSDSpassed.niVehicle[i])
			ENDIF	
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects 	 i
		IF NETWORK_DOES_NETWORK_ID_EXIST(sSDSpassed.niObject[i])		
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSDSpassed.niObject[i])
				CLEANUP_NET_ID(sSDSpassed.niObject[i])
			ENDIF	
		ENDIF
	ENDREPEAT
ENDPROC


//Cleanup Mission Data
PROC FMMC_SCRIPT_CLEANUP(BOOL bReset = TRUE)
	//INT i
	//For missions
	IF IS_THIS_A_MISSION()
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_CRATE_DROP
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_GANGHIDEOUT
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
		//REMOVE_RELATIONSHIP_GROUPS(sRGH)
	//For deathmatch
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH

	//For the race
	ELIF IS_THIS_A_RACE()
	
	ENDIF
	IF bReset = TRUE
		RESET_FMMC_MISSION_VARIABLES()
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

FUNC INT NUM_ACTIVE_PLAYERS()
	INT i 
	INT iCount
	INT iPlayer[NUM_NETWORK_PLAYERS]
	// Adding players to leaderboard
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer[i]))
			iCount++
		ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC


						
//Check that this capture is not a hack or has an exploit
PROC DO_CAPTURE_EXPLOIT_CHECK()
	//If it's a mission and a CTF raid
	FLOAT fRad
	INT iTeam, iOtherTeam, iCollectEntity
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		PRINTLN("DO_CAPTURE_EXPLOIT_CHECK g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION")	
		//Get the diffrent types
		BOOL bContend	= IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_CONTEND)		
		BOOL bGta		= IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_GTA)	
		BOOL bHold		= IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_HOLD)
		BOOL bRaid		= IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_RAID)
		PRINTLN("DO_CAPTURE_EXPLOIT_CHECK bContend	= ", bContend)	
		PRINTLN("DO_CAPTURE_EXPLOIT_CHECK bGta		= ", bGta)	
		PRINTLN("DO_CAPTURE_EXPLOIT_CHECK bHold		= ", bHold)	
		PRINTLN("DO_CAPTURE_EXPLOIT_CHECK bRaid		= ", bRaid)	
		
		//If it's a contend
		IF bHold
			//Loop all teams
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				//If the drop off is valid
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0])
					//Get the pick up vector based on type
					fRad = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[0] + 2.0 
					//Loop all capture entities
					FOR iCollectEntity = 0 TO (FMMC_MAX_NUM_CAP_ENTS - 1)						
						//If the rule is to collect it
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCollectEntity].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
							//If the vector is valid
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCollectEntity].vPos)
								//And the distance is less than the radius plus a safe distance of 
								IF VDIST(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0], g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCollectEntity].vPos) < fRad
								//And it's not in side the base as that's allowed
								AND VDIST(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0], g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCollectEntity].vPos) > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[0]
									PRINTLN("DO_CAPTURE_EXPLOIT_CHECK iTeam = ", iTeam, " iCollectEntity = ", iCollectEntity, " - ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0], ", ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCollectEntity].vPos, ") < ", fRad, " - Flaging")
									//then flag it for delete
									g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE		
									PRINTLN("DO_CAPTURE_EXPLOIT_CHECK g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE")
									SCRIPT_ASSERT("DO_CAPTURE_EXPLOIT_CHECK g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE")
									//get out
									EXIT
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR	
			
		ELIF bContend
			//Loop all teams
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				//If the drop off is valid
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0])
					//Get the pick up vector based on type
					fRad = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[0] + 2.0 
					//Loop all capture entities
					FOR iCollectEntity = 0 TO (FMMC_MAX_NUM_CAP_ENTS - 1)						
						//If the rule is to collect it
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCollectEntity].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							//If the vector is valid
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCollectEntity].vPos)
								//And the distance is less than the radius plus a safe distance of 
								IF VDIST(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0], g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCollectEntity].vPos) < fRad
									PRINTLN("DO_CAPTURE_EXPLOIT_CHECK ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0], ", ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCollectEntity].vPos, ") < ", fRad, " - Flaging")
									//then flag it for delete
									g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE		
									PRINTLN("DO_CAPTURE_EXPLOIT_CHECK g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE")
									SCRIPT_ASSERT("DO_CAPTURE_EXPLOIT_CHECK g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE")
									//get out
									EXIT
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR		
			
		//If it's a GTA
		ELIF bGta
			//Loop all teams
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				//If the drop off is valid
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0])
					//Get the safe radius
					fRad = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[0] + 4.0 
					//Loop all capture entities
					FOR iCollectEntity = 0 TO (FMMC_MAX_NUM_CAP_ENTS - 1)
						//If the rule is to collect it
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCollectEntity].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							//If the vector is valid
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCollectEntity].vPos)
								//And the distance is less than the radius plus a safe distance of 
								IF VDIST(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCollectEntity].vPos) < fRad
									PRINTLN("DO_CAPTURE_EXPLOIT_CHECK ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0], ", ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCollectEntity].vPos, ") < ", fRad, " - Flaging")
									//then flag it for delete
									g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE		
									PRINTLN("DO_CAPTURE_EXPLOIT_CHECK g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE")
									SCRIPT_ASSERT("DO_CAPTURE_EXPLOIT_CHECK g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE")
									//get out
									EXIT
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
			
		//If it's a raid
		ELIF bRaid
			PRINTLN("DO_CAPTURE_EXPLOIT_CHECK called")
			//Loop all teams drop offs
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				//If it has a value
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0])
					//If the pick up has a coordinate
					FOR iOtherTeam = 0 TO (FMMC_MAX_TEAMS - 1)
						//And it's not me
						IF iTeam != iOtherTeam
							//That's not 0
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iOtherTeam].vDropOff[0])
								//Calculate a safe radius
								fRad = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[0]
								fRad += 1.0
								fRad += g_FMMC_STRUCT.sFMMCEndConditions[iOtherTeam].fDropOffRadius[0]
								//And it's less than the safe distance
								IF VDIST(g_FMMC_STRUCT.sFMMCEndConditions[iOtherTeam].vDropOff[0], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0]) < fRad
									PRINTLN("DO_CAPTURE_EXPLOIT_CHECK VDIST(", g_FMMC_STRUCT.sFMMCEndConditions[iOtherTeam].fDropOffRadius[0], ", ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[0], ") < ", fRad, " - Flaging")
									//then flag it for delete
									g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE		
									PRINTLN("DO_CAPTURE_EXPLOIT_CHECK g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE")
									SCRIPT_ASSERT("DO_CAPTURE_EXPLOIT_CHECK g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = TRUE")
									//get out
									EXIT
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

// Clear NJVS globals because we moved the structs from NJVS into globals to prevent stack overflows (1744107)
PROC CLEAR_NJVS_STRUCTS()
	STRUCT_IMAGES_FOR_NEXT_JOB_SCREEN_VARS tempStruct
	g_sNextJobImages = tempStruct
	CACHED_MISSION_DESCRIPTION_LOAD_VARS otherTempStruct
	
	INT i
	REPEAT NUM_JOBS_TO_GRAB i
		g_sCMDLvar[i] = otherTempStruct
		PRINTLN(" [CS_JOB] CLEAR_NJVS_STRUCTS, i = ", i)
		g_sNextJobImages.sDownLoadPhotoVars[i].iTextureDownloadHandle = -1
	ENDREPEAT
ENDPROC
#IF IS_DEBUG_BUILD
FUNC BOOL SHOULD_SET_AS_HEIST_BECAUSE_OF_Z_MENU_LAUNCH()
	PLAYER_INDEX playerId 
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		playerId = INT_TO_PLAYERINDEX(iPlayer)
		IF NETWORK_IS_PLAYER_ACTIVE(playerId)
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_DONT_USE_END_VOTE_SCREEN)
				PRINTLN("SHOULD_SET_AS_HEIST_BECAUSE_OF_Z_MENU_LAUNCH() -  IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_DONT_USE_END_VOTE_SCREEN)")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC
#ENDIF

//If a mission is not rockstar created or verified then checl 
PROC DO_CASH_CAP_PAYOUT_CHECK_FOR_NONE_ROCKSTAR_CREATED()
	PRINTLN("[RCC MISSION] DO_CASH_CAP_PAYOUT_CHECK_FOR_NONE_ROCKSTAR_CREATED")
	#IF IS_DEBUG_BUILD 
	IF IS_ROCKSTAR_DEV()
		PRINTLN("[RCC MISSION] [RCC MISSION] DO_CASH_CAP_PAYOUT_CHECK_FOR_NONE_ROCKSTAR_CREATED IS_ROCKSTAR_DEV")
		EXIT
	ENDIF
	#ENDIF
	//If we're on a mission 
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	//that's paying out more than the tunable cap
	AND g_FMMC_STRUCT.iCashReward > g_sMPTunables.iMaxCashPayOutNoneRockstar//FMMC_CASH_BAND_K
		//If it's not rockstar created
		IF g_FMMC_STRUCT.sUgcCategory != UGC_CATEGORY_ROCKSTAR_CREATED
		AND g_FMMC_STRUCT.sUgcCategory != UGC_CATEGORY_ROCKSTAR_CREATED_CANDIDATE
		AND g_FMMC_STRUCT.sUgcCategory != UGC_CATEGORY_ROCKSTAR_VERIFIED
		AND g_FMMC_STRUCT.sUgcCategory != UGC_CATEGORY_ROCKSTAR_VERIFIED_CANDIDATE
		AND g_FMMC_STRUCT.sUgcCategory != UGC_CATEGORY_ROCKSTAR_COMMUNITY
		AND g_FMMC_STRUCT.sUgcCategory != UGC_CATEGORY_ROCKSTAR_COMMUNITY_CANDIDATE
			//then cap to the tunable
			PRINTLN("[RCC MISSION] g_FMMC_STRUCT.iCashReward = g_sMPTunables.iMaxCashPayOutNoneRockstar = ", g_sMPTunables.iMaxCashPayOutNoneRockstar)
			g_FMMC_STRUCT.iCashReward = g_sMPTunables.iMaxCashPayOutNoneRockstar//FMMC_CASH_BAND_K
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_HELICOPTER_GRID_POSITION(INT iPos, FLOAT fHeading)

	INT iColumns = FLOOR((g_FMMC_STRUCT.fGridWidth*2)/g_FMMC_STRUCT.fLR_GridSpacing)
	INT iRows	 = FLOOR((g_FMMC_STRUCT.fGridLength)/g_FMMC_STRUCT.fUD_GridSpacing)
	INT iCurrentColumn, iCurrentRow
	
	//cap the min
	IF iColumns = 0
		iColumns = 1
	ENDIF
	IF iRows = 0
		iRows = 1
	ENDIF
	
	iCurrentColumn 	= (iPos)%iColumns
	iCurrentRow		= (iPos)/iColumns
	
	PRINTLN("iColumns = ", iColumns, " iRows = ", iRows )
	PRINTLN("iPos = ", iPos, " iCurrentColumn = ", iCurrentColumn, " iCurrentRow = ", iCurrentRow)
	
	FLOAT fXOffset 	= (-g_FMMC_STRUCT.fLR_GridSpacing*(TO_FLOAT(iColumns)/2.0)) + (g_FMMC_STRUCT.fLR_GridSpacing * TO_FLOAT(iCurrentColumn)) + (g_FMMC_STRUCT.fLR_GridSpacing *0.5)
	RETURN 			GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT.sPlacedCheckpoint[0].vCheckPoint, fHeading, 
						<<fXOffset, -4.5 -g_FMMC_STRUCT.fUD_GridSpacing * iCurrentRow, 1.0>>)
ENDFUNC

//Do necessary pre game start ini.
PROC FMMC_PROCESS_PRE_GAME_COMMON(	MP_MISSION_DATA &fmmcMissionData, 
									INT 			&iPlayerMissionToLoad, 
									INT 			&iMissionVariation, 
									INT 			iNumberOfPacticipants 		= NUM_NETWORK_PLAYERS, 
									BOOL 			bDoReservePeds 				= TRUE, 
									BOOL 			bDoReserveVehicles 			= TRUE, 
									BOOL 			bTurnOffInvincibility 		= TRUE,
									INT				iContainerBitset			= 0 )
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[TS] *****************************************************************")
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_BASE_JUMP
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL
		PRINTLN("[TS] * **  STARTING - ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_FMMC_MISSION_TYPE_STRING(g_FMMC_STRUCT.iMissionType))," - ", g_FMMC_STRUCT.tl31LoadedContentID, " - ", g_FMMC_STRUCT.tl63MissionName, " T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " ** *")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * **  STARTING - ", g_FMMC_STRUCT.iMissionType)," - ", g_FMMC_STRUCT.tl31LoadedContentID, " - ", g_FMMC_STRUCT.tl63MissionName, " T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " ** *")#ENDIF
	ELSE
		PRINTLN("[TS] * **  STARTING - ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_FMMC_MISSION_TYPE_STRING(g_FMMC_STRUCT.iMissionType))," ** *")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * **  STARTING - ", g_FMMC_STRUCT.iMissionType," ** *")#ENDIF
	ENDIF
	IF IS_THIS_IS_A_STRAND_MISSION()
		PRINTLN("[TS] *                      STRAND MISSION *")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] *                      STRAND MISSION *")#ENDIF
	ENDIF
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds != 0
		PRINTLN("[TS] *                      ROUNDS MISSION - iNumberOfRounds = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds, " - iNumberOfRoundsPlayed = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed)
		#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] *                      ROUNDS MISSION - iNumberOfRounds = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds, " - iNumberOfRoundsPlayed = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed)#ENDIF
	ENDIF
	PRINTLN("[TS] *****************************************************************")
	#ENDIF
	
	#IF FEATURE_GEN9_STANDALONE
		SET_ACTIVITY_SCRIPT_ROUTING_ENABLED(FALSE)
	#ENDIF //FEATURE_GEN9_STANDALONE
	
	STORE_PREFERRED_CAMERA()	
	
	CLEAR_PED_HEADSHOTS_LBD()
	
	g_bCelebrationPodiumIsActive = FALSE
	
	// Clear NJVS structs
	CLEAR_NJVS_STRUCTS()
	
	// Fade the clouds back in 2193949
	SET_CLOUD_STATE(ENUMCLOUDSTATE_FADING_IN)
	
	//Clear the old posix time
	g_iCurrentPosixforNJVS = 0
	
	//If I'm starting an on call and we're not in an activity session then clean it up
	IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()	
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
		SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()	
	ENDIF
	
	//Clear that we're ready to start the intro animations
	CLEAR_READY_TO_START_INTRO_ANIMS()
	
	//clear that we want the rounds LB
	CLEAR_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
	
	//Clean that the mission needs flagged for delete
	g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = FALSE		
	
	// This marks the script as a net script, and handles any instancing setup. 
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(iNumberOfPacticipants, FALSE, fmmcMissionData.iInstanceId)   
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
	AND DID_I_JOIN_MISSION_AS_SPECTATOR()
	AND NOT SHOULD_SPECTATOR_BE_ROAMING_AT_START()
		PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX, GET_TRANSITION_SESSION_JOINED_PLAYER_GBD())
		IF piPlayer != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_ACTIVE(piPlayer)
			IF DOES_ENTITY_EXIST(GET_PLAYER_PED(piPlayer))
				SET_IN_SPECTATOR_MODE(TRUE, GET_PLAYER_PED(piPlayer))
				CPRINTLN(DEBUG_SPECTATOR, "Joining session, set to spectate ", GET_PLAYER_NAME(piPlayer)) 
			ENDIF
		ENDIF
	ENDIF
	
	//Clear the SCTV Ticker feed
	CLEAR_SCTV_TICKER_FEED_STRUCT()
	
	PRINTLN("[TEL] RESET_FMMC_MISSION_VARIABLES - NETWORK_BLOCK_JOIN_QUEUE_INVITES(TRUE)")
	NETWORK_BLOCK_JOIN_QUEUE_INVITES(TRUE)
	
	//Clear that we joined a launched mission
	IF SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()
		CLEAR_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()
	ENDIF
	
	//Wrapped up because of this assert: url:bugstar:4392650
	PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX, GET_TRANSITION_SESSION_JOINED_PLAYER_GBD())
	IF piPlayer != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_ACTIVE(piPlayer)	
		PRINTLN("RESET_FMMC_MISSION_VARIABLES - NETWORK_SET_PLAYER_IS_PASSIVE(FALSE)")
		NETWORK_SET_PLAYER_IS_PASSIVE(FALSE)
	ENDIF
	
	//Clear the swap to smallest flag
	CLEAR_THAT_I_WON_LAST_SWAP_TO_SMALLEST_MISSION()
	CLEAR_THAT_I_DIDNT_KILL_BEAST_LAST_MISSION()
	CLEAR_IM_ABOUT_TO_JIP_PLAYLIST_MISSION()
	CLEAR_LOCAL_PLAYER_ON_MISSION_LB()
	
	#IF IS_DEBUG_BUILD
	
	//Set broad cast data if launched via Z menu
	#IF IS_DEBUG_BUILD
		IF g_bLaunchedViaZmenu
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_DONT_USE_END_VOTE_SCREEN)
			g_bLaunchedViaZmenu = FALSE
		ENDIF
	#ENDIF
	
	//Clear that the mission was debug launched
	g_bDebugLaunchedMission = FALSE
		IF SHOULD_SET_AS_HEIST_BECAUSE_OF_Z_MENU_LAUNCH()
			PRINTLN("g_FMMC_STRUCT.iRootContentIDHash = ", g_FMMC_STRUCT.iRootContentIDHash)
			IF TEMP_Debug_Is_This_A_Heist_Planning_Mission(g_FMMC_STRUCT.iRootContentIDHash)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_HEIST_PLANNING
				PRINTLN("[RCC MISSION] SATMP_HEIST_PLANNING launched via Z Menu")
				PRINTLN("[TS] GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_HEIST_PLANNING")
				g_bDebugLaunchedMission = TRUE
				//Set that the corona can progress
				SET_CORONA_HEIST_IS_READY_TO_START_JOB(TRUE)
				SET_CORONA_IS_READY_TO_START_JOB(TRUE)
			ENDIF
			IF TEMP_Debug_Is_This_A_Heist_Finale_Mission(g_FMMC_STRUCT.iRootContentIDHash)
				PRINTLN("[RCC MISSION] SATMP_HEIST launched via Z Menu")
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_HEIST
				PRINTLN("[TS] GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_HEIST")
				g_bDebugLaunchedMission = TRUE
				//Set that the corona can progress
				SET_CORONA_HEIST_IS_READY_TO_START_JOB(TRUE)
				SET_CORONA_IS_READY_TO_START_JOB(TRUE)
			ENDIF
		ENDIF
	#ENDIF
	
	//Clear that we are a JIP player
	CLEAR_LOCAL_PLAYER_JOINED_IN_PROGRESS_MATCH()
	
	//Cap the cash payout for rockstar created
	DO_CASH_CAP_PAYOUT_CHECK_FOR_NONE_ROCKSTAR_CREATED()
	
	// Check for already being in a tutorial session due to being on the race / dm tutorial
	BOOL bInTutorialSessionForRaceOrDMTtutorial = FALSE
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			//bInTutorialSessionForRaceOrDMTtutorial = TRUE
		//	#IF IS_DEBUG_BUILD
		//		PRINTLN("[dsw] [FMMC_PROCESS_PRE_GAME_COMMON] bInTutorialSessionForRaceOrDMTtutorial = TRUE")
		//	#ENDIF
		ENDIF
	ENDIF
	
	CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
	PRINTLN("FMMC_PROCESS_PRE_GAME_COMMON - CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)")
	
	//if it's not an activity session then make sure to turn the store off
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		SET_STORE_ENABLED(FALSE)
		PRINTLN("[TS] RESET_FMMC_MISSION_VARIABLES - SET_STORE_ENABLED(FALSE)")
	ENDIF
	
	// Clear on leaderboard global
	SET_ON_LBD_GLOBAL(FALSE)
	g_i_Headshots = 0
	
	//CLEAR_ALL_BUDDY_EFFECTS()
	//Set that we've unlocked base jumps
	#IF IS_DEBUG_BUILD
		IF NOT g_bBlockParachuting
	#ENDIF
	IF DONE_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_BASE_JUMP)
		PRINTLN(" [CS JOB] FMMC_PROCESS_PRE_GAME_COMMON -GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_UNLOCKED_PARA")
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_UNLOCKED_PARA)
	ENDIF
	#IF IS_DEBUG_BUILD
		ENDIF
	#ENDIF
	//set tht we've unlocked survival
	#IF IS_DEBUG_BUILD
		IF NOT g_bBlockSurvival
	#ENDIF
	IF HAS_PRIMARY_SURVIVAL_BEEN_COMPLETED()
		PRINTLN(" [CS JOB] FMMC_PROCESS_PRE_GAME_COMMON -GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_UNLOCKED_SURVIVAL")
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_UNLOCKED_SURVIVAL)
	ENDIF
	#IF IS_DEBUG_BUILD
		ENDIF
	#ENDIF
	
	//clear the left at the very end of a mission
	CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()

	CLEAR_LAST_NUM_TEAMS_FOR_RESTART()
	
	IF  IS_PLAYER_SCTV(PLAYER_ID())
	OR DID_I_JOIN_MISSION_AS_SPECTATOR()
	OR SHOULD_SPECTATOR_BE_ROAMING_AT_START()
		DISABLE_MP_PASSIVE_MODE(PMER_NON_PARTICIPANT_OF_MATCH, FALSE)
	ELSE
		DISABLE_MP_PASSIVE_MODE(PMER_START_OF_A_MATCH, FALSE)
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_IMPROMPTU_DM
		AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_GANGHIDEOUT
		AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_RACE_TO_POINT
		AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_GB_BOSS_DEATHMATCH
			CLEAR_CHAT_TO_PLAYER_OVERRIDE_RESTRICTIONS()
		ENDIF
	
		//No vehicles/pickups or in IMPROMPTU DM /MINIGAMES
		IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_IMPROMPTU_DM
		OR iPlayerMissionToLoad != FMMC_MINI_GAME_CREATOR_ID
			IF bDoReserveVehicles
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > 0
				OR g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles > 0
					INT iVehiclesToReserve = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
					iVehiclesToReserve += g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles
					
					IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
						iVehiclesToReserve += 1
					ENDIF
					
					PRINTLN("RESERVE_NETWORK_MISSION_VEHICLES(", iVehiclesToReserve, ")")
					RESERVE_NETWORK_MISSION_VEHICLES(iVehiclesToReserve)
				ENDIF
			ENDIF
			IF bDoReservePeds
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > 0
				OR g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds > 0
					IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
						PRINTLN("RESERVE_NETWORK_MISSION_PEDS not called because this is a FMMC_TYPE_DEATHMATCH")
					ELSE
						INT iNumPeds = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
						IF g_FMMC_STRUCT.iEndCutscene != ciMISSION_CUTSCENE_DEFAULT
							iNumPeds += 1
						ENDIF
						iNumPeds += g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds
						
						IF iNumPeds > 50
							PRINTLN("RESERVE_NETWORK_MISSION_PEDS - Limiting to 50")
							iNumPeds = 50
						ENDIF
						
						RESERVE_NETWORK_MISSION_PEDS(iNumPeds)
						PRINTLN("RESERVE_NETWORK_MISSION_PEDS(", iNumPeds, ")")
					ENDIF
				ENDIF
			ENDIF
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfObjects > 0
			OR g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps > 0
			OR g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates > 0
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciENABLE_BMB_MODE)
			OR g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects > 0
				PRINTLN("RESERVE_NETWORK_MISSION_OBJECTS(", (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects + g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps + g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates + g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects), ")")
				
				INT iExtraObjects = g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
				
				iExtraObjects += COUNT_SET_BITS( iContainerBitSet ) * 3
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciENABLE_BMB_MODE)
					PRINTLN( "[JJT][BMB] Requesting additional object space for Bomber Mode: ", ciBMB_MODE_ADDITIONAL_PROP_REQUEST )
					iExtraObjects += ciBMB_MODE_ADDITIONAL_PROP_REQUEST
				ENDIF
				
				RESERVE_NETWORK_MISSION_OBJECTS((iExtraObjects + g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps + g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates + g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects))
			ENDIF
		ENDIF
		IF NOT bInTutorialSessionForRaceOrDMTtutorial
			//IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_ARM_WRESTLING
			IF (IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() AND HAS_FM_RACE_TUT_BEEN_DONE()) // Tutorial mission
	//			PRINTLN("g_FMMC_STRUCT.iMissionType = FMMC_TYPE_IMPROMPTU_DM")
	//		ELIF NOT IS_AREA_TRIGGERED_MISSION(g_FMMC_STRUCT.iMissionType)
	//			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
					PRINTLN("NETWORK_ALLOW_GANG_TO_JOIN_TUTORIAL_SESSION - Starting tutorial session with team 1 and iInstancePassed = to ", fmmcMissionData.iInstanceId)
					PRINTLN("MAKING A CALL TO:")
					PRINTLN("          NETWORK_ALLOW_GANG_TO_JOIN_TUTORIAL_SESSION(", 1, ", ", fmmcMissionData.iInstanceId, ")")
					PRINTLN("  ")
					PRINTLN("  ")
					NETWORK_ALLOW_GANG_TO_JOIN_TUTORIAL_SESSION(1, fmmcMissionData.iInstanceId)
	//			ELSE
	//				PRINTLN("IS_FAKE_MULTIPLAYER_MODE_SET()")
	//			ENDIF
	//		ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_GANGHIDEOUT
	//			PRINTLN("FMMC_PROCESS_PRE_GAME_COMMON - g_FMMC_STRUCT.iMissionType = FMMC_TYPE_GANGHIDEOUT")	
	//		ELSE
	//			PRINTLN("IS_AREA_TRIGGERED_MISSION = TRUE")
	//			PRINTLN("SET_FM_MISSION_AS_NOT_JOINABLE CALLED")
	//			SET_FM_MISSION_AS_NOT_JOINABLE()
			ENDIF
		ELSE
			PRINTLN("bInTutorialSessionForRaceOrDMTtutorial")
		ENDIF
	ENDIF
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	IF NOT IS_TRANSITION_SESSION_ACTIVE_ON_A_MISSION()
		SET_TRANSITION_SESSION_ACTIVE_ON_A_MISSION()
	ENDIF
	IF NOT IS_TRANSITION_SESSION_VOTE_STATUS_CLEAR() 
		CLEAR_TRANSITION_SESSION_VOTE_STATUS() 
	ENDIF
	//If I joined a private session then set some broadcast data to tell everone about it!
	IF DID_I_JOIN_A_PRIVATE_SESSION()
		SET_ME_AS_GOING_PRIVATE_FOR_DO_TRANSITION_TO_FREEMODE()
	ENDIF
	//Host things
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF fmmcMissionData.iInstanceId != DEFAULT_MISSION_INSTANCE
			iPlayerMissionToLoad 	 = fmmcMissionData.mdID.idCreator
			iMissionVariation		 = fmmcMissionData.mdID.idVariation
			#IF IS_DEBUG_BUILD
				PRINTLN("-------------------------------------")
				PRINTLN("-------------------------------------")
				PRINTLN("Mission details are:")PRINTNL()
				IF iPlayerMissionToLoad = FMMC_ROCKSTAR_CREATOR_ID
					PRINTLN("Creator :", "FMMC_ROCKSTAR_CREATOR_ID")PRINTNL()
				ELIF iPlayerMissionToLoad = FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID
					PRINTLN("Creator :", "FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID")PRINTNL()
				ELIF iPlayerMissionToLoad = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
					PRINTLN("Creator :", "FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID")PRINTNL()
				ELIF iPlayerMissionToLoad = FMMC_MINI_GAME_CREATOR_ID
					PRINTLN("Creator :", "MINIGAME")PRINTNL()					
				ELSE
					IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
					AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_IMPROMPTU_DM
					AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_GB_BOSS_DEATHMATCH
						PRINTLN("Creator :", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerMissionToLoad)))PRINTNL()
					ENDIF
				ENDIF
				PRINTLN("iMissionVariation :", iMissionVariation)PRINTNL()
				NET_PRINT_TIME() NET_PRINT_STRINGS("Loading the save from ", "FM_Mission_Controller.sc") NET_NL()
			#ENDIF
		ELSE
			iPlayerMissionToLoad = NATIVE_TO_INT(PLAYER_ID())
			iMissionVariation = 0
		ENDIF
		#IF IS_DEBUG_BUILD
		NET_PRINT_TIME() NET_PRINT_STRINGS("Setting serverBD.piPlayerMissionToLoad I'm the host ", "FM_Mission_Controller.sc") NET_NL()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		NET_PRINT_TIME() NET_PRINT_STRINGS("I'm NOT THE host ", "FM_Mission_Controller.sc") NET_NL()
		#ENDIF
	ENDIF
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	//Reserve Assets
	REGISTER_SCRIPT_WITH_AUDIO(TRUE)
	
	PRINTLN("SET_EVERYONE_IGNORE_PLAYER FALSE")
	SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
	
	SET_FM_MISSION_LAUNCHED_SUCESS(fmmcMissionData.mdID.idCreator, fmmcMissionData.mdID.idVariation, fmmcMissionData.iInstanceId)
	
	IF bTurnOffInvincibility
		PRINTLN("FMMC_PROCESS_PRE_GAME_COMMON - SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)")
		SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
	ENDIF
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR() //Don't clear the area if I joined as a spectator as this means I'm coming in late
		AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() // DOn't clear area on tutorial mission
				CLEAR_AREA(<<0.0, 0.0, 0.0>>, 16000.0, TRUE, FALSE, FALSE, FALSE)
				PRINTLN("FMMC_PROCESS_PRE_GAME_COMMON - CLEAR_AREA(<<0.0, 0.0, 0.0>>, 16000.0, TRUE, FALSE, FALSE, FALSE)")
			ENDIF
		ENDIF
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_GANGHIDEOUT
		SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_WALK_IN)
	ENDIF
	
	IF DID_I_JOIN_MISSION_AS_SPECTATOR()
		PRINTLN("[TS] FMMC_PROCESS_PRE_GAME_COMMON - DID_I_JOIN_MISSION_AS_SPECTATOR - SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)")
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
		IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_RACE
		AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_BASE_JUMP
		AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION
		AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_DEATHMATCH
		AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_SURVIVAL
			SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
			CLEAR_IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA()
			SET_MG_READY_FOR_TRANSITION_STATE_FM_SWOOP_DOWN()
			PRINTLN("[TS] FMMC_PROCESS_PRE_GAME_COMMON - DID_I_JOIN_MISSION_AS_SPECTATOR - SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()")
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_ARM_WRESTLING
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_DARTS
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_GOLF
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_HUNTING
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_RANGE_COVERED
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_RANGE_GRID
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_RANGE_RANDOM
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_TENNIS	
//		INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_MINIGAME_START)
//		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_START, INCREMENT_STARTING_ACTIVITY_CHEAT_STAT_BY())
//		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_STARTED, 1)
		IF INCREMENT_STARTING_ACTIVITY_CHEAT_STAT_BY() > 0
			SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_MINIGAME_STARTED)
		ENDIF
	ENDIF
	
	
	//Set the players mission decarators
	IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		SET_LOCAL_PLAYERS_FM_MISSION_DECORATORS(g_FMMC_STRUCT.iMissionType, fmmcMissionData.mdID.idVariation)
	ELSE
		PRINTLN("SET_LOCAL_PLAYERS_FM_MISSION_DECORATORS = FALSE because IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL")
	ENDIF
	
	//Clean up the mission 
	g_sFMMCEOM.bDoWarp 	= FALSE
	g_sFMMCEOM.vWarp 	= <<0.0, 0.0, 0.0>>
	
	// Clock the time when the mission started so the player vehicle can determine which time of spawn to do.
	MPGlobals.VehicleData.iMissionStartTime = GET_CLOUD_TIME_AS_INT()
	//CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_START_OF_MISSION_SPAWN)
	
	//Check that this capture is not a hack or has an exploit
	DO_CAPTURE_EXPLOIT_CHECK()

ENDPROC



FUNC STRING GET_FMMC_LAUNCH_SCRIPT(INT iMissionType)

	// KGM 12/11/12: FM Missions now use mission data
	SWITCH iMissionType
		CASE FMMC_TYPE_MG_ARM_WRESTLING		// eFM_ARM_WRESTLING
		CASE FMMC_TYPE_MG_DARTS				// eFM_DARTS
		CASE FMMC_TYPE_MG_GOLF				// eFM_GOLF
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	// eFM_SHOOTING_RANGE
		CASE FMMC_TYPE_MG_TENNIS			// eFM_TENNIS
		CASE FMMC_TYPE_BASE_JUMP			// eFM_BASEJUMP_CLOUD
		CASE FMMC_TYPE_DEATHMATCH			// eFM_DEATHMATCH_CLOUD
		CASE FMMC_TYPE_IMPROMPTU_DM			// eFM_DEATHMATCH_CLOUD
		CASE FMMC_TYPE_GANGHIDEOUT			// eFM_GANG_ATTACK_CLOUD
		CASE FMMC_TYPE_SURVIVAL				// eFM_SURVIVAL_CLOUD
		CASE FMMC_TYPE_MISSION				// eFM_MISSION_CLOUD
		CASE FMMC_TYPE_RACE					// eFM_RACE_CLOUD
		CASE FMMC_TYPE_MG_PILOT_SCHOOL		// eFM_PILOT_SCHOOL
		CASE FMMC_TYPE_GB_BOSS_DEATHMATCH
			RETURN (GET_MP_MISSION_NAME(Convert_FM_Mission_Type_To_MissionID(iMissionType)))
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: GET_FMMC_LAUNCH_SCRIPT() - ERROR: FMMC_TYPE_ not recognised.") NET_NL()
		SCRIPT_ASSERT("GET_FMMC_LAUNCH_SCRIPT(): ERROR - FMMC_TYPE_ not recognised. Add to Switch Statement. See Console Log. Returning fm_missions_controler. Tell Keith.")
	#ENDIF
	
	RETURN GET_FM_MISSION_CONTROLLER_SCRIPT_NAME()
	
ENDFUNC

// Speirs changed 28/06/12 to fix 556700
FUNC INT GET_PLAYER_FMMC_MISSION_STACKSIZE(INT iMissionType)

	// KGM 12/11/12: FM Missions now use mission data
	SWITCH iMissionType
		CASE FMMC_TYPE_MG_ARM_WRESTLING		// eFM_ARM_WRESTLING
		CASE FMMC_TYPE_MG_DARTS				// eFM_DARTS
		CASE FMMC_TYPE_MG_GOLF				// eFM_GOLF
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	// eFM_SHOOTING_RANGE
		CASE FMMC_TYPE_MG_TENNIS			// eFM_TENNIS
		CASE FMMC_TYPE_BASE_JUMP			// eFM_BASEJUMP_CLOUD
		CASE FMMC_TYPE_DEATHMATCH			// eFM_DEATHMATCH_CLOUD
		CASE FMMC_TYPE_IMPROMPTU_DM			// eFM_DEATHMATCH_CLOUD
		CASE FMMC_TYPE_GANGHIDEOUT			// eFM_GANG_ATTACK_CLOUD
		CASE FMMC_TYPE_SURVIVAL				// eFM_SURVIVAL_CLOUD
		CASE FMMC_TYPE_MISSION				// eFM_MISSION_CLOUD
		CASE FMMC_TYPE_RACE					// eFM_RACE_CLOUD
		CASE FMMC_TYPE_MG_PILOT_SCHOOL					// eFM_PILOT_SCHOOL
		CASE FMMC_TYPE_GB_BOSS_DEATHMATCH
			RETURN (GET_MP_MISSION_STACK_SIZE(Convert_FM_Mission_Type_To_MissionID(iMissionType)))
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: GET_PLAYER_FMMC_MISSION_STACKSIZE() - ERROR: FMMC_TYPE_ not recognised.") NET_NL()
		SCRIPT_ASSERT("GET_PLAYER_FMMC_MISSION_STACKSIZE(): ERROR - FMMC_TYPE_ not recognised. Add to Switch Statement. See Console Log. Returning FREEMODE_STACK_SIZE. Tell Keith.")
	#ENDIF
	
	RETURN MULTIPLAYER_FREEMODE_STACK_SIZE
	
ENDFUNC

FUNC BLIP_SPRITE BLIP_TO_USE_FOR_START_LOCATION(INT iStartNumber)
	IF iStartNumber < 11
		IF iStartNumber = 4
			PRINTLN("BLIP_TO_USE_FOR_START_LOCATION is REturning RADAR_TRACE_MISSION_2TO4",iStartNumber)
			RETURN RADAR_TRACE_MISSION_2TO4
		ELIF iStartNumber = 8
			PRINTLN("BLIP_TO_USE_FOR_START_LOCATION is REturning RADAR_TRACE_MISSION_2TO8",iStartNumber)
			RETURN RADAR_TRACE_MISSION_2TO8
		ELSE
			#IF IS_DEBUG_BUILD	
				PRINTLN("BLIP_TO_USE_FOR_START_LOCATION is REturning RADAR_TRACE_NUMBERED_",iStartNumber)
			#ENDIF
			RETURN INT_TO_ENUM(BLIP_SPRITE, ENUM_TO_INT(RADAR_TRACE_NUMBERED_1) +iStartNumber - 1)
		ENDIF
	ELIF iStartNumber <= 16
		IF iStartNumber = 12
			PRINTLN("BLIP_TO_USE_FOR_START_LOCATION is REturning RADAR_TRACE_MISSION_2TO12",iStartNumber)
			RETURN RADAR_TRACE_MISSION_2TO12
		ELIF iStartNumber = 16
			PRINTLN("BLIP_TO_USE_FOR_START_LOCATION is REturning RADAR_TRACE_MISSION_2TO16",iStartNumber)
			RETURN RADAR_TRACE_MISSION_2TO16
		ELSE
			#IF IS_DEBUG_BUILD	
				PRINTLN("BLIP_TO_USE_FOR_START_LOCATION is REturning RADAR_TRACE_NUMBERED_",iStartNumber)
			#ENDIF
			RETURN INT_TO_ENUM(BLIP_SPRITE, ENUM_TO_INT(RADAR_TRACE_NUMBERED_11) + (iStartNumber - 11))
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD	
			PRINTLN("BLIP_TO_USE_FOR_START_LOCATION is REturning RADAR_TRACE_OBJECTIVE" )
		#ENDIF
		RETURN RADAR_TRACE_NUMBERED_16
	ENDIF
ENDFUNC

FUNC BOOL JOIN_FM_MISSION_IN_PROGRESS_AS_SPECTATOR(GET_UGC_CONTENT_STRUCT &sGetUGC_content, STRING szContentID, INT iCreatorID, INT iInstanceId, INT iMissionVariation,  INT iMissionType, BOOL bSkipDownLoad = FALSE, BOOL bDoFade = FALSE)//, STRING stUserName = NULL
	//Get MY GBD
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	STRING				stLaunchScript			= GET_FMMC_LAUNCH_SCRIPT(iMissionType)
	
	//Set up my data
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
		IF NETWORK_IS_SCRIPT_ACTIVE(stLaunchScript, iInstanceId, TRUE)
			PRINTLN("[TS] JOIN_FM_MISSION_IN_PROGRESS_AS_SPECTATOR - NETWORK_IS_SCRIPT_ACTIVE(", stLaunchScript, ", ", iInstanceId, ") = TRUE, waiting")
			RETURN FALSE
		ENDIF
		PRINTLN("[TS] JOIN_FM_MISSION_IN_PROGRESS_AS_SPECTATOR setting up")
		GlobalplayerBD_FM_2[iMyGBD].missionVariation 					= iMissionVariation
		GlobalplayerBD_FM_2[iMyGBD].missionType	 					= iMissionType
		GlobalplayerBD_FM[iMyGBD].iCreatorIDOfMIssionIwantToStart	= iCreatorID
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
	ENDIF
	
	//Load the mission data
	IF bSkipDownLoad 
	OR FMMC_LOAD_MISSION_DATA(	sGetUGC_content, 
								szContentID,
								GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, 
								GlobalplayerBD_FM_2[iMyGBD].missionType, 
								IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()), 
								FALSE, 
								FALSE,
								FALSE,
								FALSE)
		INT iMissionTypeToLaunch = g_FMMC_STRUCT.iMissionType 					
		INT iStackSizeToUse = GET_PLAYER_FMMC_MISSION_STACKSIZE(iMissionTypeToLaunch)
		IF GlobalplayerBD_FM_2[iMyGBD].missionType >=  FMMC_TYPE_MG	
			iMissionTypeToLaunch = GlobalplayerBD_FM_2[iMyGBD].missionType
		ENDIF
		//Set up a struct with all the data about the mission
		MP_MISSION_DATA	fmmcMissionDataTemp
		fmmcMissionDataTemp.mdID.idCreator 	= GlobalplayerBD_FM[iMyGBD].iCreatorIDOfMIssionIwantToStart
		fmmcMissionDataTemp.iInstanceId 	= iInstanceId
		//Launch the script
		IF NET_LOAD_AND_LAUNCH_SCRIPT_FM(stLaunchScript, iStackSizeToUse, fmmcMissionDataTemp, bDoFade, TRUE, TRUE)
			SET_SCTV_READY_FOR_NEXT_MISSION()
			GlobalplayerBD_FM_2[iMyGBD].missionVariation 					= -1
			GlobalplayerBD_FM_2[iMyGBD].missionType						= -1
			GlobalplayerBD_FM[iMyGBD].iCreatorIDOfMIssionIwantToStart	= -1
			CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
			CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
			CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
			CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biStartToLoadData)
			SET_I_JOIN_MISSION_AS_SPECTATOR()			
			GlobalplayerBD_FM[iMyGBD].currentMissionData.mdPrimaryCoords 	= g_FMMC_STRUCT.vStartPos
			GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idVariation	= iMissionVariation
			GlobalplayerBD_FM[iMyGBD].tl31CurrentMissionFileName 			= g_FMMC_STRUCT.tl31LoadedContentID
			GlobalplayerBD_FM[iMyGBD].tl23CurrentMissionOwner				= g_FMMC_STRUCT.tl23MissionCreatorID
			GlobalplayerBD_MissionName[iMyGBD].tl63MissionName				= g_FMMC_STRUCT.tl63MissionName
			PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM = TRUE")			
			RETURN TRUE
		ELSE
			PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM = FALSE")			
		ENDIF		
	ELSE
		PRINTLN("FMMC_LOAD_MISSION_DATA = FALSE")
	ENDIF				
	RETURN FALSE
ENDFUNC

//Launches a playlist with the given instance, Nice.
FUNC BOOL LAUNCH_FM_PLAYLIST_SCRIPT(INT iInstanceId, INT iMissionToStart = -1)
	//Launch the script
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FMMC_PlayList_Controller")) = 0
		IF NOT (NETWORK_IS_SCRIPT_ACTIVE("FMMC_PlayList_Controller", iInstanceId, TRUE))
			REQUEST_SCRIPT("FMMC_PlayList_Controller")
  			IF HAS_SCRIPT_LOADED("FMMC_PlayList_Controller")
				PRINTLN("[PLC] iInstanceId = ", iInstanceId)
				
				//Set my playlist instance
				SET_PLAYERS_PLAYLIST_INSTANCE(iInstanceId)
				
				MP_MISSION_DATA fmmcMissionData
				fmmcMissionData.iInstanceId = iInstanceId
				fmmcMissionData.mdID.idCreator = iMissionToStart
				PRINTLN("[PLC] fmmcMissionData.iInstanceId    = ", fmmcMissionData.iInstanceId)
				PRINTLN("[PLC] fmmcMissionData.mdID.idCreator = ", fmmcMissionData.mdID.idCreator)
				START_NEW_SCRIPT_WITH_ARGS("FMMC_PlayList_Controller", fmmcMissionData, SIZE_OF(fmmcMissionData), SCRIPT_XML_STACK_SIZE)
   				SET_SCRIPT_AS_NO_LONGER_NEEDED("FMMC_PlayList_Controller")
				PRINTLN("[PLC] BWW...  FMMC_Launcher Starting FMMC_PlayList_Controller. ")
				RETURN TRUE
			ELSE
				PRINTLN("[PLC] LAUNCH_FM_PLAYLIST_SCRIPT, WAITING ON HAS_SCRIPT_LOADED")
			ENDIF
		ELSE
			PRINTLN("[PLC] (NETWORK_IS_SCRIPT_ACTIVE(FMMC_PlayList_Controller, ", iInstanceId, ", TRUE))")
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// Joins a mission inprogress via the phone
FUNC BOOL JOIN_FM_MISSION_IN_PROGRESS_VIA_PHONE(GET_UGC_CONTENT_STRUCT &sGetUGC_content, STRING szContentID, INT iCreatorID, INT iInstanceId, INT iMissionVariation,  INT iMissionType, INT iPlayerGBD, STRING stUserName = NULL)
	//Get MY GBD
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	
	
	IF IS_PLAYER_ON_A_PLAYLIST_INT(iPlayerGBD)
		PRINTLN("IS_PLAYER_ON_A_PLAYLIST_INT(iPlayerGBD)")
		SET_PLAYER_ON_A_PLAYLIST(TRUE)
		//IF LAUNCH_FM_PLAYLIST_SCRIPT(GlobalplayerBD_FM[iPlayerGBD].sPlaylistVars.iPlayListInstance) 
			RETURN TRUE
		//ENDIF
	ELSE
		//Set up my data
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
			PRINTLN("stUserName = ", stUserName)
			PRINTLN("szContentID = ", szContentID)
			PRINTLN("ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator SETTING")
			GlobalplayerBD_FM_2[iMyGBD].missionVariation 					= iMissionVariation
			GlobalplayerBD_FM_2[iMyGBD].missionType	 					= iMissionType
			GlobalplayerBD_FM[iMyGBD].iCreatorIDOfMIssionIwantToStart	= iCreatorID
			SET_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
			PRINTLN("GlobalplayerBD_FM_2[iMyGBD].missionVariation                                 = ", iMissionVariation)
	        PRINTLN("GlobalplayerBD_FM_2[iMyGBD].missionType                                     = ", iMissionType)
	        PRINTLN("GlobalplayerBD_FM[iMyGBD].iCreatorIDOfMIssionIwantToStart = ", iCreatorID)
		ENDIF
		
		//Load the mission data
		IF FMMC_LOAD_MISSION_DATA(sGetUGC_content, szContentID, GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, GlobalplayerBD_FM_2[iMyGBD].missionType)//GlobalplayerBD_FM[iMyGBD].iCreatorIDOfMIssionIwantToStart,  GlobalplayerBD_FM_2[iMyGBD].missionVariation,  stUserName
			
			INT iMissionTypeToLaunch = g_FMMC_STRUCT.iMissionType 					
			IF GlobalplayerBD_FM_2[iMyGBD].missionType >=  FMMC_TYPE_MG	
				iMissionTypeToLaunch = GlobalplayerBD_FM_2[iMyGBD].missionType
			ENDIF
			STRING				stLaunchScript			= GET_FMMC_LAUNCH_SCRIPT(iMissionTypeToLaunch)
			INT 				iStackSizeToUse			= GET_PLAYER_FMMC_MISSION_STACKSIZE(iMissionTypeToLaunch)
			//Set up a struct with all the data about the mission
			MP_MISSION_DATA	fmmcMissionDataTemp
			fmmcMissionDataTemp.mdID.idCreator 	= GlobalplayerBD_FM[iMyGBD].iCreatorIDOfMIssionIwantToStart
			fmmcMissionDataTemp.iInstanceId 	= iInstanceId
			fmmcMissionDataTemp.mdID.idVariation    = iMissionVariation
			
			//Launch the script
			IF NET_LOAD_AND_LAUNCH_SCRIPT_FM(stLaunchScript, iStackSizeToUse, fmmcMissionDataTemp, TRUE, TRUE, TRUE)
				PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM RETURNING TRUE")
				GlobalplayerBD_FM_2[iMyGBD].missionVariation 					= -1
				GlobalplayerBD_FM_2[iMyGBD].missionType						= -1
				GlobalplayerBD_FM[iMyGBD].iCreatorIDOfMIssionIwantToStart	= -1
				CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
				CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biStartToLoadData) 
				PRINTLN("fmmcMissionDataTemp.mdID.idCreator          = ", fmmcMissionDataTemp.mdID.idCreator)
	            PRINTLN("fmmcMissionDataTemp.iInstanceId           = ", fmmcMissionDataTemp.iInstanceId)
	            PRINTLN("fmmcMissionDataTemp.mdID.idVariation  = ", fmmcMissionDataTemp.mdID.idVariation)

				GlobalplayerBD_FM[iMyGBD].currentMissionData.mdPrimaryCoords 	= g_FMMC_STRUCT.vStartPos
				GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idVariation	= iMissionVariation
				GlobalplayerBD_FM[iMyGBD].tl23CurrentMissionOwner	 			= stUserName
				GlobalplayerBD_FM[iMyGBD].tl31CurrentMissionFileName 			= szContentID
				PRINTLN("GlobalplayerBD_FM[iMyGBD].currentMissionData.mdPrimaryCoords = ", GlobalplayerBD_FM[iMyGBD].currentMissionData.mdPrimaryCoords)
				PRINTLN("GlobalplayerBD_FM[iMyGBD].tl23CurrentMissionOwner            = ", GlobalplayerBD_FM[iMyGBD].tl23CurrentMissionOwner)
				PRINTLN("GlobalplayerBD_FM[iMyGBD].tl31CurrentMissionFileName         = ", GlobalplayerBD_FM[iMyGBD].tl31CurrentMissionFileName)
				RETURN TRUE
			ENDIF				
		ENDIF				
	ENDIF				
	RETURN FALSE
ENDFUNC
//Join a specific player on a mission as a spectator
FUNC BOOL JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AS_SPECTATOR(GET_UGC_CONTENT_STRUCT &sGetUGC_content, PLAYER_INDEX piPlayer, BOOL bDoFade = FALSE)
	
	//Set up the data
	INT 	iPlayerGBD 			= NATIVE_TO_INT(piPlayer)
	INT 	iCreatorID 			= GlobalplayerBD_FM[iPlayerGBD].currentMissionData.mdID.idCreator 		
	INT 	iMissionVariation	= GlobalplayerBD_FM[iPlayerGBD].currentMissionData.mdID.idVariation 	
	INT 	iInstanceId			= GlobalplayerBD_FM[iPlayerGBD].currentMissionData.iInstanceId	
	INT 	iMissionType		= GlobalplayerBD_FM[iPlayerGBD].iCurrentMissionType	
	
	//Call the join function
	RETURN JOIN_FM_MISSION_IN_PROGRESS_AS_SPECTATOR(sGetUGC_content, GlobalplayerBD_FM[iPlayerGBD].tl31CurrentMissionFileName, iCreatorID, iInstanceId, iMissionVariation,  iMissionType, FALSE, bDoFade)//, 
	
ENDFUNC

//Join a specific player on a mission as a spectator
FUNC BOOL JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AS_SPECTATOR_GBD(GET_UGC_CONTENT_STRUCT &sGetUGC_content, INT iPlayerGBD, BOOL bSkipDownLoad = FALSE, BOOL bDoFade = FALSE)
	
		//Set up the data
		INT 	iCreatorID 			= GlobalplayerBD_FM[iPlayerGBD].currentMissionData.mdID.idCreator 		
		INT 	iMissionVariation	= GlobalplayerBD_FM[iPlayerGBD].currentMissionData.mdID.idVariation 	
		INT 	iInstanceId			= GlobalplayerBD_FM[iPlayerGBD].currentMissionData.iInstanceId	
		INT 	iMissionType		= GlobalplayerBD_FM[iPlayerGBD].iCurrentMissionType	
		
		IF iInstanceId = -1
			PRINTLN("[TS] JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AS_SPECTATOR_GBD iInstanceId = -1")		
			RETURN FALSE
		ENDIF
		
		//Call the join function
		//PRINTLN("JOIN_FM_MISSION_IN_PROGRESS_AS_SPECTATOR")
		RETURN JOIN_FM_MISSION_IN_PROGRESS_AS_SPECTATOR(sGetUGC_content, GlobalplayerBD_FM[iPlayerGBD].tl31CurrentMissionFileName, iCreatorID, iInstanceId, iMissionVariation,  iMissionType, bSkipDownLoad, bDoFade)//, 
ENDFUNC

CONST_INT ciMAX_JIP_FAIL_COUNT 20
//Join a specific player on a mission as a spectator
FUNC BOOL JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AFTER_JIP(SCRIPT_TIMER &sTimerPassed, INT &iJoinFailCount)

	//MAT not ready bail and print
	IF NOT Is_Initial_Cloud_Loaded_Mission_Data_Ready()
		#IF IS_DEBUG_BUILD				
			IF (GET_GAME_TIMER() % 10000) < 50
				PRINTLN("[TS] JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AFTER_JIP - Is_Initial_Cloud_Loaded_Mission_Data_Ready = FALSE")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	//Set up the data
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
		PLAYER_INDEX 	playerOnMission = INT_TO_NATIVE(PLAYER_INDEX, GET_TRANSITION_SESSION_JOINED_PLAYER_GBD())
		INT 			uniqueID 		= Get_UniqueID_For_This_Players_Mission(playerOnMission)
		IF NETWORK_IS_PLAYER_ACTIVE(playerOnMission)
			PRINTLN("[TS] JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AFTER_JIP - Broadcast_Request_Join_Existing_Mission called")		
			PRINTLN("[TS] JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AFTER_JIP - playerOnMission = ", GET_PLAYER_NAME(playerOnMission))
			PRINTLN("[TS] JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AFTER_JIP - uniqueID        = ", uniqueID)		
			IF uniqueID = NO_UNIQUE_ID
				iJoinFailCount++
				PRINTLN("[TS] JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AFTER_JIP - iJoinFailCount        = ", iJoinFailCount)		
				IF iJoinFailCount > ciMAX_JIP_FAIL_COUNT
					PRINTLN("[TS] JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AFTER_JIP - iJoinFailCount        = ciMAX_JIP_FAIL_COUNT = ", ciMAX_JIP_FAIL_COUNT)		
					//Get out as we're going to get stuck
					RESET_TRANSITION_SESSION_NON_RESET_VARS()
					CLEAR_TRANSITION_SESSIONS_BITSET()
					SET_TRANSITION_SESSIONS_JIP_FAILED()
					SET_MY_TRANSITION_SESSION_CONTENT_ID(GlobalplayerBD_FM[NATIVE_TO_INT(playerOnMission)].tl31CurrentMissionFileName)					
					BUSYSPINNER_OFF()
					NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_FM_MISSION_IN_PROGRESS_AFTER_JIP_NO_DATA))	
				ENDIF
			ENDIF
			Broadcast_Request_Join_Existing_Mission(uniqueID, MP_MISSION_SOURCE_JOBLIST)
			RESET_NET_TIMER(sTimerPassed)
		ELSE
			PRINTLN("[TS] JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AFTER_JIP - NETWORK_IS_PLAYER_ACTIVE = TRUE")		
			RETURN FALSE
		ENDIF
	ENDIF
	
	//When we are active, return TRUE
	IF Is_Mission_Triggering_State_Mission_Active()
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
		PRINTLN("[TS] JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AFTER_JIP - Is_Mission_Triggering_State_Mission_Active")
		SET_I_JOIN_MISSION_AS_SPECTATOR()	
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl23CurrentMissionOwner = g_FMMC_STRUCT.tl23MissionCreatorID
		RETURN TRUE
	//If 913 seconds have passed then try again
	ELIF HAS_NET_TIMER_EXPIRED(sTimerPassed, 913)
		PRINTLN("[TS] JOIN_PLAYER_ON_FM_MISSION_IN_PROGRESS_AFTER_JIP - HAS_NET_TIMER_EXPIRED = TRUE clearing flag and trying again")		
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
		RESET_NET_TIMER(sTimerPassed)		
	ENDIF

	RETURN FALSE
ENDFUNC


PROC CLEAN_UP_WHEN_ON_A_MISSION_COMMON()
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	
	g_vStartingPointWarppedToWithVote = <<0.0, 0.0, 0.0>>
	IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FMMC_RCHL_0")
		CLEAR_THIS_FLOATING_HELP("FMMC_RCHL_0")
	ENDIF
	
	CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIwantToVote)
	CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biPlayerWarpingToCorona)
	CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIsDontDisplayVoteOverHead)		
	CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biStartToLoadData) 
	CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteHasPassed)
	CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteSetUp)
	CLEAR_ME_AS_VOTED(iMyGBD, TRUE)
	SET_BIT  (GlobalplayerBD_FM_2[iMyGBD].iBitSet, biILeftTheArea)
	
	SCRIPT_TIMER sCoronaTimerTemp
	GlobalplayerBD_FM_2[iMyGBD].sCoronaTimer = sCoronaTimerTemp
	GlobalplayerBD_FM_2[iMyGBD].sCoronaTimer2 = sCoronaTimerTemp
	GlobalplayerBD_FM_2[iMyGBD].bHasPlayerVoted					= FALSE
	GlobalplayerBD_FM_2[iMyGBD].missionToStart  					= ciNULL_MISSION
	GlobalplayerBD_FM_2 [iMyGBD].missionVariation					= ciNULL_VARIATION				
	GlobalplayerBD_FM_2[iMyGBD].iVoteToUse 						= -1	
	
//	CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Controlled_TurnedOff_For_Launch)		// KGM 18/12/12: looks to be obsolete, checked with Bobby
	CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_SKIP_JOINABLE_CHECKS)	
	CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
	CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)		
	CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
	CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator)
	
	GlobalplayerBD_FM[iMyGBD].iInstanceToUse					= DEFAULT_MISSION_INSTANCE
	GlobalplayerBD_FM[iMyGBD].iCreatorIDOfMIssionIwantToStart	= -1
	GlobalplayerBD_FM[iMyGBD].sMenuSelection.bForceStart		= FALSE
	
	IF IS_SELECTOR_DISABLED()
		PRINTLN("CLEAN_UP_WHEN_ON_A_MISSION - ENABLE_SELECTOR")
		ENABLE_SELECTOR()
	ENDIF
	SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
	PRINTLN("FMMC_LAUNCHER - CLEAN_UP_WHEN_ON_A_MISSION - SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)")
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	PRINTSTRING("...BWW MANTAIN_START_LOCATIONS Clean up the vote varaiables as player is now on that mission")PRINTNL()
ENDPROC











