
USING "commands_stats.sch"
USING "commands_network.sch"
USING "achievements_enum.sch"
USING "gamemodes_enum.sch"
USING "script_network.sch"
USING "script_player.sch"
USING "net_prints.sch"
USING "net_stat_system.sch"
USING "MP_SkyCam.sch"
USING "net_spawn.sch"
USING "selector_public.sch"
USING "help_at_location.sch"
USING "net_cutscene.sch"
USING "cam_recording_public.sch"
//USING "net_custom_garage.sch"
//USING "tattoo_shop_private.sch"
using "commands_ped.sch"

/*// total should not exceed 225
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					62
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					1	
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				5	
// total should not exceed 12
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK 		8   
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		4					
// this should be fine
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		6
USING "traffic.sch"*/

#IF IS_DEBUG_BUILD
INT iWdScriptProgress = 0//used to follow where the script currently is (was used to figure out how to decrease the length of time at the start of the cutscene)
#ENDIF

/// PURPOSE: List of Intro Cutscene Stage IDs
ENUM INTRO_CUT_STAGE
	INTRO_CUT_INIT,
	INTRO_CUT_START,
	INTRO_CUT_SHOT1_START,
	INTRO_CUT_SHOT1_START2,
	INTRO_CUT_SHOT1_START3,
	INTRO_CUT_SHOT1_START4,
	INTRO_CUT_SHOT1,
	
	INTRO_CUT_CITY_SHOT_1_START,
	INTRO_CUT_CITY_SHOT_1,
	
	INTRO_CUT_CITY_SHOT_2_START,
	INTRO_CUT_CITY_SHOT_2,
	
	INTRO_CUT_RACE_START,
	INTRO_CUT_RACE_WAIT_FOR_LOAD,
	INTRO_CUT_RACE,
	INTRO_CUT_RACE_1,
	INTRO_CUT_RACE_2,
	INTRO_CUT_RACE_3,
	INTRO_CUT_RACE_4,
	INTRO_CUT_RACE_5,
	INTRO_CUT_RACE_6,
	INTRO_CUT_RACE_HELI,
	INTRO_CUT_RACE_SHOT2,
	INTRO_CUT_START_GARAGE,
	
	INTRO_CUT_GARAGE_2_START,
	INTRO_CUT_GARAGE_2,
	INTRO_CUT_GARAGE_3_START,
	INTRO_CUT_GARAGE_3,
	INTRO_CUT_GARAGE_4_START,
	INTRO_CUT_GARAGE_4,
	
	INTRO_CUT_MOCAP_START,
	INTRO_CUT_MOCAP_CONFIRM_PLAYING,
	INTRO_CUT_MOCAP_PLAYING,
	
	INTRO_CUT_LAMAR_CAR_DRIVE_START,
	INTRO_CUT_LAMAR_CAR_DRIVE_01,
	INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01,
	INTRO_CUT_LAMAR_CAR_DRIVE_03,
	INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02,
	INTRO_CUT_LAMAR_CAR_DRIVE_05,
	INTRO_CUT_LAMAR_CAR_DRIVE_07,
	INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03,
	INTRO_CUT_LAMAR_CAR_DRIVE_10,
	INTRO_CUT_LAMAR_CAR_DRIVE_12,
	INTRO_CUT_LAMAR_CAR_DRIVE_14,
	
	INTRO_CUT_LAMAR_CAR_DRIVE_17,//
	
	INTRO_CUT_LAMAR_CAR_DRIVE_19,
	INTRO_CUT_LAMAR_CAR_DRIVE_20,
	INTRO_CUT_LAMAR_CAR_DRIVE_21,
	INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04,
	INTRO_CUT_LAMAR_CAR_DRIVE_24,
	INTRO_CUT_LAMAR_CAR_DRIVE_25,
	INTRO_CUT_LAMAR_CAR_DRIVE_26,
	
	INTRO_CUT_DRIVE_END_MOCAP_START,
	INTRO_CUT_DRIVE_END_MOCAP_CONFIRM_PLAYING,
	INTRO_CUT_DRIVE_END_MOCAP_PLAYING,
	
	INTRO_CUT_TEST,
	INTRO_CUT_SKIP_START,
	INTRO_CUT_SKIP_DAVE,
	INTRO_CUT_FINISH,
	INTRO_CUT_LAUNCH_GUN_INTRO,
	INTO_CUT_DONE
ENDENUM

CONST_INT MAX_CUT_ENTITIES				12
CONST_INT NUMBER_OF_GARAGE_VEHICLES 	8
CONST_INT NUMBER_OF_JET_POPULATION		7
CONST_INT NUMBER_OF_AMBIENT_CARS		15
CONST_INT NUMBER_OF_AMBIENT_VAGOS		10
CONST_INT NUMBER_OF_AIRPORT_JETS		1

CONST_INT SRL_READAHEAD_LIGHT_PRESTREAM_MAP		8
CONST_INT SRL_READAHEAD_LIGHT_PRESTREAM_ASSETS	8
CONST_INT SRL_READAHEAD_LIGHT_PLAYBACK_MAP		8
CONST_INT SRL_READAHEAD_LIGHT_PLAYBACK_ASSETS	8

CONST_INT SRL_READAHEAD_NORMAL_PRESTREAM_MAP	9
CONST_INT SRL_READAHEAD_NORMAL_PRESTREAM_ASSETS	9
CONST_INT SRL_READAHEAD_NORMAL_PLAYBACK_MAP		9
CONST_INT SRL_READAHEAD_NORMAL_PLAYBACK_ASSETS	9

CONST_INT SRL_READAHEAD_NORMAL_SP_PRESTREAM_MAP		9
CONST_INT SRL_READAHEAD_NORMAL_SP_PRESTREAM_ASSETS	12
CONST_INT SRL_READAHEAD_NORMAL_SP_PLAYBACK_MAP		9
CONST_INT SRL_READAHEAD_NORMAL_SP_PLAYBACK_ASSETS 	12

CONST_INT SRL_READAHEAD_HEAVY_PRESTREAM_MAP		9
CONST_INT SRL_READAHEAD_HEAVY_PRESTREAM_ASSETS	13
CONST_INT SRL_READAHEAD_HEAVY_PLAYBACK_MAP		9
CONST_INT SRL_READAHEAD_HEAVY_PLAYBACK_ASSETS	13

CONST_INT MP_INTRO_START_OF_MOCAP_TIME			52618
CONST_INT MP_INTRO_END_OF_MOCAP_TIME			134784

/// PURPOSE: Data struct of variables required when running the intro cutscene
STRUCT FM_CUT_STRUCT
	INT iBitset
	INT iSpeechBitset
	INT iGarageCarBitset
	INT iLoadBitset
	INT iCreditBitset
	INT iCurrentCredit

	INTRO_CUT_STAGE introCutStage
	CAMERA_INDEX cutsceneCam
	CAMERA_INDEX animCam
	INT iSceneID = -1
	INT iMechanicSceneID = -1
	INT iLamarSceneID = -1
	INT iPlayerSceneID = -1
	INT iCutStartTime
	INT iDeleteStagger = 0
	SCRIPT_TIMER timeWaitForSkycam
	SCRIPT_TIMER iStreamVolTimer
	VECTOR vSkyCamPos
	VECTOR vSkyCamRot
	FLOAT fSkyCamFOV
	INT iStartOfMocapTime = 0
	INT iStartOfMocapSRLAdjust = 0
	INT iEndOfMocapTime = 0
	INT iEndOfMocapSRLAdjust = 0
	INT iStartOfDriveEndMocapTime = 0
	INT iMocapTimeLastFrame = 0
	INTERIOR_INSTANCE_INDEX interiorGarage
	OBJECT_INDEX objPlane
	VEHICLE_INDEX vehLamar
	PED_INDEX pedIGLamar
	PED_INDEX pedCSLamar
	PED_INDEX pedVagos[NUMBER_OF_AMBIENT_VAGOS]
	VEHICLE_INDEX vehHeli
	VEHICLE_INDEX vehGarage[NUMBER_OF_GARAGE_VEHICLES]
	VEHICLE_INDEX vehAmbient[NUMBER_OF_AMBIENT_CARS]
	PED_INDEX pedAmbient[NUMBER_OF_AMBIENT_CARS]
	INT iAmbientCarStagger = 0
	PED_INDEX pedMechanic
	VEHICLE_INDEX vehRacers[5]
	PED_INDEX pedRacers[5]
	PED_INDEX pedJet[NUMBER_OF_JET_POPULATION]
	INT iJetCloneSourcePlayer[NUMBER_OF_JET_POPULATION]
	PTFX_ID ptfxPlane0
	PTFX_ID ptfxPlane1
	SCENARIO_BLOCKING_INDEX scenBlockCut
	structPedsForConversation sSpeech
	BOOL bLaunchFmIntro = FALSE
	FLOAT fLastSRLTimePassed = 0.0
	FLOAT fLastSRLTimeAdjust = 0.0
	BOOL bStallSRLTimerUpdate = FALSE
	#IF IS_DEBUG_BUILD
		INT iCutsceneTimeUpCount = 0
	#ENDIF
	
	BOOL bSRLOn = FALSE
	BOOL bRecordOn = FALSE
	BOOL bPTFXOn = FALSE
	BOOL bRegPlayerOn = FALSE
	
	BOOL bLoadedRoadModel = FALSE
ENDSTRUCT


//iBitset
CONST_INT biCleanupDone						0
CONST_INT biCutWasSkipped					1
CONST_INT biIntroHelp						2
CONST_INT biSkipFinalScene					3
CONST_INT biStartedFirstScene				4
CONST_INT biDoneFirstRaceHelp				5
CONST_INT biDoneSecondRaceHelp				6
CONST_INT biDoMusicCues						7
CONST_INT biSetGarageRoom					8
CONST_INT biAddedToTutSession				9
CONST_INT biStartRaceLoadScene				10
CONST_INT biDoSRLTime						11
CONST_INT biRunSplashStart					12
CONST_INT biBlockSettingFocus				13
CONST_INT biFocusSet						14
CONST_INT biDelayCam						15
CONST_INT biSkipping						16
CONST_INT biFadedOutFinalSkip				17
CONST_INT biDisplayFMIntroHelp				18
CONST_INT biLamarCoronaConvoStart			29
CONST_INT biDriveEndMocapLoaded				20
CONST_INT biNeedToCallMusicDriveEnd			21
CONST_INT biTimeSetToNightInCutscene		22
CONST_INT biSetPlayerExitState				23
CONST_INT biMusicStarted					24
CONST_INT biAirportJetsStarted				25
CONST_INT biScreenFrozen					26
CONST_INT biTyreSqueal0						27
CONST_INT biTyreSqueal1						28
CONST_INT biDrawTennisLights				29
CONST_INT biLogoFXPlayed					30
CONST_INT biPinnedGarageInterior			31



//iLoadBitset
CONST_INT loadBiCreatedCars					0
CONST_INT loadBiModsSetOnRacer				1
CONST_INT loadBiPlaneCreated				2
CONST_INT loadBiMocapLoaded					3
CONST_INT loadBiAmbientCarsCreated			4
CONST_INT loadBiVagosCreated				5
CONST_INT loadBiEntitiesForcedIntoInterior	6
CONST_INT loadBiUberCreated					7
CONST_INT loadBiHeliFlypastCreated			8
CONST_INT loadBiLamarCreated				9
CONST_INT loadBiCSLamarCreated				10
CONST_INT loadBiAirportJetsCreated			11
CONST_INT loadBiCredits						12




//iGarageCarBitset
CONST_INT GCbiGarageCarsComplete		31


CONST_INT CREATE_TAXI_FOR_INTRO	0
CONST_INT WALK_PLAYER_INTO_TAXI 1
CONST_INT PLAYER_SAT_IN_TAXI	2

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Prints a string to the console log prefaced with an identifier for the intro cutscene
/// PARAMS:
///    sText - string to print
	PROC PRINT_INTRO_STRING(STRING sText)
		NET_NL()
		NET_PRINT(" [dsw] [IntroCut] ")
		NET_PRINT(sText)
		NET_NL()
	ENDPROC

/// PURPOSE:
///    Prints a string and int to the console log prefaced with an identifier for the intro cutscene
/// PARAMS:
///    sText - string to print
///    iToPrint - int to print
	PROC PRINT_INTRO_STRING_INT(STRING sText, INT iToPrint)
		NET_NL()
		NET_PRINT(" [dsw] [IntroCut] ")
		NET_PRINT(sText)
		NET_PRINT_INT(iToPrint)
		NET_NL()
	ENDPROC

/// PURPOSE:
///    Converts a Intro Cutscene Stage ID into a string
/// PARAMS:
///    thisStage - Intro Cutscene Stage ID requested
/// RETURNS:
///    INTRO_CUT_STAGE as a string
	FUNC STRING GET_INTRO_CUT_STAGE_NAME(INTRO_CUT_STAGE thisStage)
		
		SWITCH thisStage
			CASE INTRO_CUT_INIT									RETURN "INTRO_CUT_INIT"									BREAK
			CASE INTRO_CUT_START								RETURN "INTRO_CUT_START"								BREAK
			CASE INTRO_CUT_SHOT1_START							RETURN "INTRO_CUT_SHOT1_START"							BREAK
			CASE INTRO_CUT_SHOT1_START2							RETURN "INTRO_CUT_SHOT1_START2"							BREAK
			CASE INTRO_CUT_SHOT1_START3							RETURN "INTRO_CUT_SHOT1_START3"							BREAK
			CASE INTRO_CUT_SHOT1_START4							RETURN "INTRO_CUT_SHOT1_START4"							BREAK
			CASE INTRO_CUT_SHOT1								RETURN "INTRO_CUT_SHOT1"								BREAK
			
			CASE INTRO_CUT_CITY_SHOT_1_START					RETURN "INTRO_CUT_CITY_SHOT_1_START"					BREAK
			CASE INTRO_CUT_CITY_SHOT_1							RETURN "INTRO_CUT_CITY_SHOT_1"							BREAK
			
			CASE INTRO_CUT_CITY_SHOT_2_START					RETURN "INTRO_CUT_CITY_SHOT_2_START"					BREAK
			CASE INTRO_CUT_CITY_SHOT_2							RETURN "INTRO_CUT_CITY_SHOT_2"							BREAK
			
			CASE INTRO_CUT_RACE_START							RETURN "INTRO_CUT_RACE_START"							BREAK
			CASE INTRO_CUT_RACE_WAIT_FOR_LOAD					RETURN "INTRO_CUT_RACE_WAIT_FOR_LOAD"					BREAK
			CASE INTRO_CUT_RACE									RETURN "INTRO_CUT_RACE"									BREAK
			CASE INTRO_CUT_RACE_1								RETURN "INTRO_CUT_RACE_1"								BREAK
			CASE INTRO_CUT_RACE_2								RETURN "INTRO_CUT_RACE_2"								BREAK
			CASE INTRO_CUT_RACE_3								RETURN "INTRO_CUT_RACE_3"								BREAK
			CASE INTRO_CUT_RACE_4								RETURN "INTRO_CUT_RACE_4"								BREAK
			CASE INTRO_CUT_RACE_5								RETURN "INTRO_CUT_RACE_5"								BREAK
			CASE INTRO_CUT_RACE_6								RETURN "INTRO_CUT_RACE_6"								BREAK
			CASE INTRO_CUT_RACE_HELI							RETURN "INTRO_CUT_RACE_HELI"							BREAK
			CASE INTRO_CUT_RACE_SHOT2							RETURN "INTRO_CUT_RACE_SHOT2"							BREAK
			CASE INTRO_CUT_START_GARAGE							RETURN "INTRO_CUT_START_GARAGE"							BREAK
			
			CASE INTRO_CUT_GARAGE_2_START						RETURN "INTRO_CUT_GARAGE_2_START"						BREAK
			CASE INTRO_CUT_GARAGE_2								RETURN "INTRO_CUT_GARAGE_2"								BREAK
			CASE INTRO_CUT_GARAGE_3_START						RETURN "INTRO_CUT_GARAGE_3_START"						BREAK
			CASE INTRO_CUT_GARAGE_3								RETURN "INTRO_CUT_GARAGE_3"								BREAK
			CASE INTRO_CUT_GARAGE_4_START						RETURN "INTRO_CUT_GARAGE_4_START"						BREAK
			CASE INTRO_CUT_GARAGE_4								RETURN "INTRO_CUT_GARAGE_4"								BREAK
			
			CASE INTRO_CUT_MOCAP_START							RETURN "INTRO_CUT_MOCAP_START"							BREAK
			CASE INTRO_CUT_MOCAP_CONFIRM_PLAYING				RETURN "INTRO_CUT_MOCAP_CONFIRM_PLAYING"				BREAK
			CASE INTRO_CUT_MOCAP_PLAYING						RETURN "INTRO_CUT_MOCAP_PLAYING"						BREAK
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_START				RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_START"				BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_01					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_01"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01				RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01"				BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_03					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_03"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02				RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02"				BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_05					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_05"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_07					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_07"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03				RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03"				BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_10					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_10"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_12					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_12"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_14					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_14"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_17					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_17"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_19					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_19"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_20					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_20"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_21					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_21"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04				RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04"				BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_24					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_24"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_25					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_25"					BREAK
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_26					RETURN "INTRO_CUT_LAMAR_CAR_DRIVE_26"					BREAK
			
			CASE INTRO_CUT_DRIVE_END_MOCAP_START				RETURN "INTRO_CUT_DRIVE_END_MOCAP_START"				BREAK
			CASE INTRO_CUT_DRIVE_END_MOCAP_CONFIRM_PLAYING		RETURN "INTRO_CUT_DRIVE_END_MOCAP_CONFIRM_PLAYING"		BREAK
			CASE INTRO_CUT_DRIVE_END_MOCAP_PLAYING				RETURN "INTRO_CUT_DRIVE_END_MOCAP_PLAYING"				BREAK
			
			CASE INTRO_CUT_TEST									RETURN "INTRO_CUT_TEST"									BREAK
			CASE INTRO_CUT_SKIP_START							RETURN "INTRO_CUT_SKIP_START"							BREAK
			CASE INTRO_CUT_SKIP_DAVE							RETURN "INTRO_CUT_SKIP_DAVE"							BREAK
			CASE INTRO_CUT_FINISH								RETURN "INTRO_CUT_FINISH"								BREAK
			CASE INTRO_CUT_LAUNCH_GUN_INTRO						RETURN "INTRO_CUT_LAUNCH_GUN_INTRO"						BREAK
			CASE INTO_CUT_DONE									RETURN "INTO_CUT_DONE"									BREAK
		ENDSWITCH
		
		RETURN ""
	ENDFUNC
	
#ENDIF

/// PURPOSE:
///    Returns if the player is female (in debug mode, will return a set answer for debug reasons)
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    true/false
FUNC BOOL IS_INTRO_FEMALE(FM_CUT_STRUCT &FMCutData)
	
	IF FMCutData.bRegPlayerOn
		RETURN IS_PLAYER_FEMALE()
	ELSE
		RETURN FALSE		//Setting for debug play
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the model for the race drive in the intro
/// RETURNS:
///    MODEL_NAMES of the race driver model
FUNC MODEL_NAMES GET_RACE_DRIVER_MODEL()
	RETURN GET_ENTITY_MODEL(PLAYER_PED_ID())
ENDFUNC

/// PURPOSE:
///    Returns the model for the mechanic in the intro
/// RETURNS:
///    MODEL_NAMES of the mechanic
FUNC MODEL_NAMES GET_INTRO_MECHANIC_MODEL()
	RETURN S_M_Y_XMECH_02
ENDFUNC

/// PURPOSE:
///    Returns the animation dictionary for the mechanic in the intro
/// RETURNS:
///    string ID of the animation dictionary
FUNC STRING GET_MECHANIC_ANIM_DICT()
	RETURN "MP_INTRO_SEQ@"
ENDFUNC

/// PURPOSE:
///    Returns a model for a car in the garage based on a car ID
/// PARAMS:
///    iIndex - ID of the garage car
/// RETURNS:
///    MODEL_NAMES of the garage car
FUNC MODEL_NAMES GET_INTRO_GARAGE_MODEL(INT iIndex)
	SWITCH iIndex
		//FMMC_VEHICLE_CLASS_MUSCLE
		CASE 0 RETURN PENUMBRA BREAK		//mechanic
		//FMMC_VEHICLE_CLASS_SEDANS  
		CASE 1 RETURN FELTZER2 BREAK		//black cardick
		//FMMC_VEHICLE_CLASS_SUV
		CASE 2 RETURN COQUETTE BREAK		//green sports
		//FMMC_VEHICLE_CLASS_COUPES
		CASE 3 RETURN FELON2 BREAK			//plain black car
		//FMMC_VEHICLE_CLASS_CLASSICS
		CASE 4 RETURN SABREGT BREAK			//focus
		// FMMC_VEHICLE_CLASS_COMPACTS
		CASE 5 RETURN DUBSTA BREAK			//grey SUV
		//FMMC_VEHICLE_CLASS_OFF_ROAD 
		CASE 6 RETURN TORNADO BREAK			//red 50's
		//FMMC_VEHICLE_CLASS_SPORTS 
		CASE 7 RETURN Monroe BREAK
	ENDSWITCH
	RETURN TAILGATER
ENDFUNC

/// PURPOSE:
///    Requests all of the garage car models
PROC REQUEST_INTRO_GARAGE_MODELS()
	INT i
	REPEAT NUMBER_OF_GARAGE_VEHICLES i
		REQUEST_MODEL(GET_INTRO_GARAGE_MODEL(i))
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Cleans up the model, mod kits and vehicle ID for a garage car
/// PARAMS:
///    FMCutData - 
///    iCarID - ID of the garage car
PROC CLEANUP_INTRO_CUT_GARAGE_CAR(FM_CUT_STRUCT &FMCutData, INT iCarID)
	//INT i
	
	#IF IS_DEBUG_BUILD
		PRINT_INTRO_STRING_INT("[CLEANUP_INTRO_CUT_GARAGE_CAR] Called with iCarId = ", iCarID)
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_INTRO_GARAGE_MODEL(iCarID))
	IF DOES_ENTITY_EXIST(FMCutData.vehGarage[iCarID])
		IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[iCarID])
		ENDIF
		// Removing mod removal - not needed!
		/*REPEAT (ENUM_TO_INT(MOD_REAR_WHEELS)+1) i
			IF IS_TOGGLE_MOD_ON(FMCutData.vehGarage[iCarID], INT_TO_ENUM(MOD_TYPE, i))
				TOGGLE_VEHICLE_MOD(FMCutData.vehGarage[iCarID], INT_TO_ENUM(MOD_TYPE, i), FALSE)
			ENDIF
		ENDREPEAT
		REMOVE_VEHICLE_MOD_KIT(FMCutData.vehGarage[iCarID])*/
		
		#IF IS_DEBUG_BUILD
			PRINT_INTRO_STRING("[CLEANUP_INTRO_CUT_GARAGE_CAR] ...vehicle exists, cleaning up mods...")
		#ENDIF
		
		RELEASE_PRELOAD_MODS(FMCutData.vehGarage[iCarID])
		DELETE_VEHICLE(FMCutData.vehGarage[iCarID])
	ELSE
		#IF IS_DEBUG_BUILD
			PRINT_INTRO_STRING("[CLEANUP_INTRO_CUT_GARAGE_CAR] ...vehicle doesn't exist!")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up all of the data loaded for the garage cars, as well as the car mechanic
/// PARAMS:
///    FMCutData - 
PROC CLEANUP_INTRO_CUT_CUSTOM_GARAGE_MODELS(FM_CUT_STRUCT &FMCutData)
	#IF IS_DEBUG_BUILD
		PRINT_INTRO_STRING("[CLEANUP_INTRO_CUT_CUSTOM_GARAGE_MODELS] Called. Callstack...")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	INT i
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_INTRO_MECHANIC_MODEL())
	REPEAT NUMBER_OF_GARAGE_VEHICLES i
		CLEANUP_INTRO_CUT_GARAGE_CAR(FMCutData, i)
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(FMCutData.pedMechanic)
		DELETE_PED(FMCutData.pedMechanic)
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the position and heading for a garage car ID
/// PARAMS:
///    iCar - car ID information required
///    vPos - store the vector position of the car
///    fHead - stores the float heading of the car
PROC GET_INTRO_GARAGE_POS_AND_HEAD(INT iCar, VECTOR &vPos, FLOAT &fHead)
	SWITCH iCar
		CASE 0
			vPos 	= 	<<224.478, -986.548, -99.9999>>
			fHead	=	240.399
		BREAK
		
		CASE 1
			vPos 	= 	<<224.305, -991.848, -99.9999>>
			fHead	=	240.399
		BREAK
		
		CASE 2
			vPos 	= 	<<224.535, -997.348, -99.9999>>
			fHead	=	240.399
		BREAK
		
		CASE 3
			vPos 	=	<<232.847, -986.818, -99.9999>> 
			fHead	=	118.309
		BREAK
		
		CASE 4
			vPos 	=	<<232.847, -980.848, -99.9999>>
			fHead	=	118.309
		BREAK
		
		CASE 5
			vPos 	= 	<<224.535, -980.848, -99.9999>>
			fHead	=  	240.399
		BREAK
		
		CASE 6
			vPos 	=  <<232.779, -992.840, -99.9999>>
			fHead	=	118.309
		BREAK
		
		CASE 7
			vPos 	=	<<232.709, -997.348, -99.9999>> 
			fHead	=	118.309
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Sets the provided garage car ID's colour setup
/// PARAMS:
///    FMCutData - 
///    iCar - car ID to set the colours of
PROC SET_INTRO_GARAGE_COLOURS(FM_CUT_STRUCT &FMCutData, INT iCar)
	
	SWITCH iCar
	
		CASE 0
			// PENUMBRA, 2nd on left
			SET_VEHICLE_COLOURS(FMCutData.vehGarage[iCar], 83, 134)
			SET_VEHICLE_EXTRA_COLOURS(FMCutData.vehGarage[iCar], 134, 134)
		BREAK
	
		CASE 1	//black cardick
			SET_VEHICLE_COLOURS(FMCutData.vehGarage[iCar], 0, 0) // Primary Colour - Classic - Black
			SET_VEHICLE_EXTRA_COLOURS(FMCutData.vehGarage[iCar], 0, 156)// 
		BREAK
	
		CASE 2
			//--COQUETTE , green sports, mechanic
			SET_VEHICLE_COLOURS(FMCutData.vehGarage[iCar], 49, 0) // Classic Dark green
			SET_VEHICLE_EXTRA_COLOURS(FMCutData.vehGarage[iCar], 0, 156)//  
		BREAK
	
		CASE 3
			// FELON2, plain black car
			SET_VEHICLE_COLOURS(FMCutData.vehGarage[iCar], 0, 0) // Metallic Dark Steel
			SET_VEHICLE_EXTRA_COLOURS(FMCutData.vehGarage[iCar], 0, 156)//  
		BREAK
		
		CASE 4
			//-- SABREGT, les request car, gets full screen shot
			SET_VEHICLE_COLOURS(FMCutData.vehGarage[iCar], 43, 5)
			SET_VEHICLE_EXTRA_COLOURS(FMCutData.vehGarage[iCar], 0, 156)
		BREAK
	
		CASE 5	//grey SUV
			SET_VEHICLE_COLOURS(FMCutData.vehGarage[iCar], 3, 3) // Metallic Dark Steel
			SET_VEHICLE_EXTRA_COLOURS(FMCutData.vehGarage[iCar], 6, 156)//  
		BREAK
		
		CASE 6
			//--TORNADO , red 50's
			SET_VEHICLE_COLOURS(FMCutData.vehGarage[iCar], 35, 0) // Primary Colour - Classic - Candy Red
			SET_VEHICLE_EXTRA_COLOURS(FMCutData.vehGarage[iCar], 0, 156)// 
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Preloads the mods for a specified garage car ID
/// PARAMS:
///    FMCutData - 
///    iCar - garage car ID that we want to preload the mods for
PROC PRELOAD_VEHICLE_MODS_FOR_INTRO_GARAGE(FM_CUT_STRUCT &FMCutData, INT iCar)

	SWITCH iCar
	
		CASE 0
			// PENUMBRA, 2nd on left mechanic
			SET_VEHICLE_MOD_KIT(FMCutData.vehGarage[iCar], 0) 
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SPOILER, 	1)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_F, 	0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_R, 	1)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SKIRT, 		1)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_EXHAUST, 	1)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_CHASSIS, 	0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BONNET, 		0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_ROOF, 		0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WHEELS, 		11)
			#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Requested penumbra mods!")#ENDIF
		BREAK
	
		CASE 1	//black cardick
			SET_VEHICLE_MOD_KIT(FMCutData.vehGarage[iCar], 0) 
			#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Requested feltzer2 mods!")#ENDIF
		BREAK
	
		CASE 2
			//--COQUETTE , green sports
			SET_VEHICLE_MOD_KIT(FMCutData.vehGarage[iCar], 0) 
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SPOILER, 	0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_EXHAUST, 	0)
			#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Requested COQUETTE mods!")#ENDIF
		BREAK
	
		CASE 3
			// FELON2, plain black car
			SET_VEHICLE_MOD_KIT(FMCutData.vehGarage[iCar], 0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WHEELS,		11)
		BREAK
	
		CASE 4
			//-- SABREGT, les request car, gets full screen shot
			SET_VEHICLE_MOD_KIT(FMCutData.vehGarage[iCar], 0) 
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_F, 	0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_R, 	0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_EXHAUST, 	1)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BONNET, 		1)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WHEELS,		5)
			#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Requested sabregt mods!")#ENDIF
		BREAK
	
		CASE 5	//grey SUV
			SET_VEHICLE_MOD_KIT(FMCutData.vehGarage[iCar], 0) 
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SPOILER, 	0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_F, 	0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_R, 	0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SKIRT,    	0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_EXHAUST,		1)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_GRILL,		2)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_ROOF,		3)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WHEELS,		5)
			#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Requested dubsta mods!")#ENDIF
		BREAK
		
		CASE 6
			//--TORNADO , red 50's
			SET_VEHICLE_MOD_KIT(FMCutData.vehGarage[iCar], 0) 
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_F, 	1)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_EXHAUST,		1)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WING_L,		0)
			PRELOAD_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WHEELS,		0)
			#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Requested tornado mods!")#ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Sets the vehicle mods for a specified garage car ID
/// PARAMS:
///    FMCutData - 
///    iCar - ID of the garage car to set the vehicle mods for
/// RETURNS:
///    true if successful
FUNC BOOL SET_INTRO_GARAGE_VEHICLE_MODS(FM_CUT_STRUCT &FMCutData, INT iCar)
	
	SWITCH iCar
		
		CASE 0
			//-- PENUMBRA, 2nd on right, mechanic
			IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[iCar])
				IF GET_NUM_MOD_KITS(FMCutData.vehGarage[iCar]) > 0
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SPOILER, 	1)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_F, 	0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_R, 	1)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SKIRT, 		1)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_EXHAUST, 	1)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_CHASSIS, 	0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BONNET, 		0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_ROOF, 		0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BRAKES, 		0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_HORN,	 	0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SUSPENSION,	4)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WHEELS, 		11)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[iCar])
				IF GET_NUM_MOD_KITS(FMCutData.vehGarage[iCar]) > 0
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BRAKES, 		0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SUSPENSION, 	3)
					TOGGLE_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_TOGGLE_XENON_LIGHTS, TRUE)
					SET_VEHICLE_TYRES_CAN_BURST(FMCutData.vehGarage[iCar], FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			//-- COQUETTE, green sports
			IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[iCar])
				IF GET_NUM_MOD_KITS(FMCutData.vehGarage[iCar]) > 0
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SPOILER, 	0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_EXHAUST, 	0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_HORN,		5)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SUSPENSION,	3)
					TOGGLE_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_TOGGLE_XENON_LIGHTS, TRUE)
					SET_VEHICLE_TYRES_CAN_BURST(FMCutData.vehGarage[iCar], FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			//-- FELON2, plain black car
			IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[iCar])
				IF GET_NUM_MOD_KITS(FMCutData.vehGarage[iCar]) > 0
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SUSPENSION,	3)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WHEELS,		11)
					TOGGLE_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_TOGGLE_XENON_LIGHTS, TRUE)
					SET_VEHICLE_TYRES_CAN_BURST(FMCutData.vehGarage[iCar], FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			//-- SABREGT, les request car, gets full screen shot
			IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[iCar])
				IF GET_NUM_MOD_KITS(FMCutData.vehGarage[iCar]) > 0
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_F, 	0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_R, 	0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_EXHAUST, 	1)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BONNET, 		1)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_ENGINE, 		3)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BRAKES, 		2)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_GEARBOX, 	2)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SUSPENSION,	3)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_ARMOUR,		4)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WHEELS,		5)
					TOGGLE_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_TOGGLE_XENON_LIGHTS, TRUE)
					SET_VEHICLE_TYRES_CAN_BURST(FMCutData.vehGarage[iCar], FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			//grey SUV
			IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[iCar])
				IF GET_NUM_MOD_KITS(FMCutData.vehGarage[iCar]) > 0
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SPOILER, 		0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_F, 		0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_R, 		0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SKIRT,    		0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_EXHAUST,		1)		
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_GRILL,			2)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_ROOF,			3)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_HORN,			7)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_SUSPENSION,	0)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WHEELS,		5)
					TOGGLE_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_TOGGLE_XENON_LIGHTS, TRUE)
					SET_VEHICLE_TYRES_CAN_BURST(FMCutData.vehGarage[iCar], FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			//-- TORNADO, red 50's
			IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[iCar])
				IF GET_NUM_MOD_KITS(FMCutData.vehGarage[iCar]) > 0
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_BUMPER_F, 		1)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_EXHAUST,		1)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WING_L,		0)	
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_HORN,			7)
					SET_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_WHEELS,		0)
					TOGGLE_VEHICLE_MOD(FMCutData.vehGarage[iCar], MOD_TOGGLE_XENON_LIGHTS, TRUE)
					SET_VEHICLE_TYRES_CAN_BURST(FMCutData.vehGarage[iCar], FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		DEFAULT 
			RETURN TRUE
		BREAK
	ENDSWITCH
		
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Forces the car that the mechanic is working on's bonnet open
/// PARAMS:
///    FMCutData - 
PROC FORCE_CAR_BONNET_OPEN(FM_CUT_STRUCT &FMCutData)
	IF DOES_ENTITY_EXIST(FMCutData.vehGarage[2])
		IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[2])
			SET_VEHICLE_DOOR_CONTROL(FMCutData.vehGarage[2], SC_DOOR_BONNET, DT_DOOR_INTACT, 1.0)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Forces the cars near the camera to be high detail in the garage mechanic shot
/// PARAMS:
///    FMCutData - 
PROC FORCE_HIGH_DETAIL_CARS(FM_CUT_STRUCT &FMCutData)
	IF DOES_ENTITY_EXIST(FMCutData.vehGarage[2])
		IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[2])
			REQUEST_VEHICLE_HIGH_DETAIL_MODEL(FMCutData.vehGarage[2])
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(FMCutData.vehGarage[4])
		IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[4])
			REQUEST_VEHICLE_HIGH_DETAIL_MODEL(FMCutData.vehGarage[4])
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads the GTAO logo scaleform
/// RETURNS:
///    true once loaded
FUNC BOOL LOAD_GTAO_LOGO()
	IF GET_IS_LOADING_SCREEN_ACTIVE()
		PRINTLN("LOAD_GTAO_LOGO - GET_IS_LOADING_SCREEN_ACTIVE() = TRUE, cannot load GTAV_ONLINE movie.")
		RETURN FALSE
	ENDIF
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(g_SplashScreenMovieBIG) 
		PRINTLN("LOAD_GTAO_LOGO - HAS_SCALEFORM_MOVIE_LOADED() = FALSE, loading GTAV_ONLINE movie.")
		g_SplashScreenMovieBIG = REQUEST_SCALEFORM_MOVIE("GTAV_ONLINE")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Draws the GTAO logo scaleform to the centre of the screen
PROC DRAW_GTAO_LOGO()
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(g_SplashScreenMovieBIG, 255, 255, 255, 255)
	
	IF g_bSplashScreenBigMethodCalled = FALSE
		BEGIN_SCALEFORM_MOVIE_METHOD(g_SplashScreenMovieBIG, "SET_BIG_LOGO_VISIBLE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
		g_bSplashScreenBigMethodCalled = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Hides and cleans up the scaleform of the GTAO logo
PROC CLEANUP_GTAO_LOGO()
	g_bSplashScreenBigMethodCalled = FALSE
	IF HAS_SCALEFORM_MOVIE_LOADED(g_SplashScreenMovieBIG) 
	
		BEGIN_SCALEFORM_MOVIE_METHOD(g_SplashScreenMovieBIG, "SET_BIG_LOGO_VISIBLE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
	
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_SplashScreenMovieBIG)
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads, creates and sets up the properties of the cars in the garage scene
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    true once all cars are entirely ready for the shot
FUNC BOOL CREATE_GARAGE_CARS(FM_CUT_STRUCT &FMCutData)
	MODEL_NAMES theModel
	VECTOR vPos
	FLOAT fHead
	INT i
	
	IF NOT IS_BIT_SET(FMCutData.iGarageCarBitset, GCbiGarageCarsComplete)
		
		REQUEST_INTRO_GARAGE_MODELS()
		
		//Loop through to create and set up
		REPEAT NUMBER_OF_GARAGE_VEHICLES i
			GET_INTRO_GARAGE_POS_AND_HEAD(i, vPos, fHead)
			IF NOT ARE_VECTORS_EQUAL(vPos,<<0,0,0>>)
				IF NOT DOES_ENTITY_EXIST(FMCutData.vehGarage[i])
					theModel = GET_INTRO_GARAGE_MODEL(i)
					IF IS_MODEL_IN_CDIMAGE(theModel)
						IF HAS_MODEL_LOADED(theModel)
							FMCutData.vehGarage[i] = CREATE_VEHICLE(theModel,vPos,fHead, FALSE, FALSE)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(FMCutData.vehGarage[i], TRUE)
							SET_VEHICLE_DIRT_LEVEL(FMCutData.vehGarage[i], 0.0)
							SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(FMCutData.vehGarage[i], FALSE)
							IF i = 2
								SET_VEHICLE_DOOR_OPEN(FMCutData.vehGarage[i], SC_DOOR_BONNET, FALSE, TRUE)
								SET_VEHICLE_DOOR_CONTROL(FMCutData.vehGarage[i], SC_DOOR_BONNET, DT_DOOR_INTACT, 1.0)
							ENDIF
							SET_INTRO_GARAGE_COLOURS(FMCutData, i)
							PRELOAD_VEHICLE_MODS_FOR_INTRO_GARAGE(FMCutData, i)
							
							SET_MODEL_AS_NO_LONGER_NEEDED(theModel)
							
							#IF IS_DEBUG_BUILD 
								PRINT_INTRO_STRING_INT("[CREATE_GARAGE_CARS] Created vehicle.... ", i) 
								TEXT_LABEL_15 tl = ""
								tl += "Veh "
								tl += i
								SET_VEHICLE_NAME_DEBUG(FMCutData.vehGarage[i], tl)
							#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD PRINT_INTRO_STRING_INT("[CREATE_GARAGE_CARS] Waiting for model to load.... ", i) #ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Loop through to check they're set up correctly
		REPEAT NUMBER_OF_GARAGE_VEHICLES i
			IF NOT DOES_ENTITY_EXIST(FMCutData.vehGarage[i])
				#IF IS_DEBUG_BUILD PRINT_INTRO_STRING_INT("[CREATE_GARAGE_CARS] this vehicle still not created.... ", i) #ENDIF
				RETURN FALSE
			ENDIF
			
			IF NOT IS_BIT_SET(FMCutData.iGarageCarBitset, i)
				IF SET_INTRO_GARAGE_VEHICLE_MODS(FMCutData, i)
					SET_BIT(FMCutData.iGarageCarBitset, i)
				ELSE
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING_INT("[CREATE_GARAGE_CARS] this vehicle still waitng for mods.... ", i) #ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(FMCutData.vehGarage[i])
				IF NOT HAVE_VEHICLE_MODS_STREAMED_IN(FMCutData.vehGarage[i])
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING_INT("[CREATE_GARAGE_CARS] this vehicle still waitng for mods to stream.... ", i) #ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
			
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("[CREATE_GARAGE_CARS] CREATE_GARAGE_CARS = TRUE") #ENDIF
		
		SET_BIT(FMCutData.iGarageCarBitset, GCbiGarageCarsComplete)
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Request the mechanic ped and animation dictionary
PROC REQUEST_MECHANIC_PED()
	REQUEST_MODEL(GET_INTRO_MECHANIC_MODEL())
	REQUEST_ANIM_DICT(GET_MECHANIC_ANIM_DICT())
ENDPROC

/// PURPOSE:
///    Loads, creates and sets up the mechanic ped
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    true once complete
FUNC BOOL CREATE_MECHANIC_PED(FM_CUT_STRUCT &FMCutData)
	MODEL_NAMES theModel = GET_INTRO_MECHANIC_MODEL()
	IF NOT DOES_ENTITY_EXIST(FMCutData.pedMechanic)
		IF DOES_ENTITY_EXIST(FMCutData.vehGarage[2])
			IF IS_MODEL_IN_CDIMAGE(theModel)
				IF HAS_MODEL_LOADED(theModel)
					IF HAS_ANIM_DICT_LOADED("MP_INTRO_SEQ@")			
						IF NOT IS_ENTITY_DEAD(FMCutData.vehGarage[2])
							FMCutData.iMechanicSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iMechanicSceneID, FMCutData.vehGarage[2], 0)
							
							FMCutData.pedMechanic = CREATE_PED(PEDTYPE_CIVMALE, theModel,<<226.6826, -998.5228, -99.9999>>, 63.8130, FALSE, FALSE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FMCutData.pedMechanic, TRUE)
							
							TASK_SYNCHRONIZED_SCENE(FMCutData.pedMechanic, FMCutData.iMechanicSceneID, "MP_INTRO_SEQ@", "MP_Mech_Fix", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)
							SET_SYNCHRONIZED_SCENE_LOOPED(FMCutData.iMechanicSceneID, TRUE)
							
							FREEZE_ENTITY_POSITION(FMCutData.pedMechanic, TRUE)
							SET_ENTITY_COLLISION(FMCutData.pedMechanic, FALSE)
							REMOVE_WEAPON_FROM_PED(FMCutData.pedMechanic, GADGETTYPE_PARACHUTE)
							
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_HEAD, 1, 1)
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_BERD, 0, 0)
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_HAIR, 0, 2)
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_TORSO, 0, 2)
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_LEG, 1, 0)
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_HAND, 0, 0)
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_FEET, 0, 0)
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_TEETH, 0, 0)
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_SPECIAL, 0, 0)
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_SPECIAL2, 0, 0)
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_DECL, 0, 0)
							SET_PED_COMPONENT_VARIATION(FMCutData.pedMechanic, PED_COMP_JBIB, 0, 0)

							CLEAR_PED_PROP(FMCutData.pedMechanic, ANCHOR_HEAD)
							CLEAR_PED_PROP(FMCutData.pedMechanic, ANCHOR_EYES)
							CLEAR_PED_PROP(FMCutData.pedMechanic, ANCHOR_EARS)
							CLEAR_PED_PROP(FMCutData.pedMechanic, ANCHOR_MOUTH)
							CLEAR_PED_PROP(FMCutData.pedMechanic, ANCHOR_LEFT_HAND)
							CLEAR_PED_PROP(FMCutData.pedMechanic, ANCHOR_RIGHT_HAND)
							CLEAR_PED_PROP(FMCutData.pedMechanic, ANCHOR_LEFT_WRIST)
							CLEAR_PED_PROP(FMCutData.pedMechanic, ANCHOR_RIGHT_WRIST)
							CLEAR_PED_PROP(FMCutData.pedMechanic, ANCHOR_HIP)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(FMCutData.pedMechanic)
		#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("[CREATE_MECHANIC_PED] DOES_ENTITY_EXIST(FMCutData.pedMechanic) = FALSE") #ENDIF
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("[CREATE_MECHANIC_PED] CREATE_MECHANIC_PED = TRUE") #ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Manages putting the player into a tutorial session for the duration of the intro cutscene
/// PARAMS:
///    FMCutData - 
PROC MAINTAIN_ADD_TO_TUTORIAL_SESSION(FM_CUT_STRUCT &FMCutData)
	INT instance  
	//-- Check process_pre_game in FM_Intro as well
	IF NOT IS_BIT_SET(FMCutData.iBitset, biAddedToTutSession)
		IF FMCutData.introCutStage > INTRO_CUT_INIT
			IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
				IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
					
					instance = NATIVE_TO_INT(PLAYER_ID()) + 32
					NETWORK_ALLOW_GANG_TO_JOIN_TUTORIAL_SESSION(1, instance)
					SET_LOCAL_PLAYER_IS_IN_TUTORIAL_SESSION_FOR_INTRO(TRUE) 
					SET_BIT(FMCutData.iBitset, biAddedToTutSession)
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING_INT("[MAINTAIN_ADD_TO_TUTORIAL_SESSION] Putting player on tut session... ", instance) #ENDIF
				ENDIF
			ELSE	
				SET_LOCAL_PLAYER_IS_IN_TUTORIAL_SESSION_FOR_INTRO(TRUE) 
				SET_BIT(FMCutData.iBitset, biAddedToTutSession)
				#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("[MAINTAIN_ADD_TO_TUTORIAL_SESSION] Already in tut session... ") #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Begins creating a scaleform credit block
/// PARAMS:
///    strName - string ID for the block, needs to be short and unique
///    fX - X position of the block
///    fY - Y position of the block
///    strAlign - alignment of the text ("left" or "right")
///    fFadeInDuration - number of seconds to fade in credit block
///    fFadeOutDuration - number of seconds to fade out credit block
PROC SCALEFORM_SETUP_CREDIT_BLOCK(STRING strName, FLOAT fX, FLOAT fY, STRING strAlign, FLOAT fFadeInDuration, FLOAT fFadeOutDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(g_sfMPCredits, "SETUP_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fX)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fY)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strAlign)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeInDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeOutDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a role to a scaleform credit block
/// PARAMS:
///    strName - string ID of the block to add to
///    strRole - string of the role
///    fXOffset - offset on the X axis
///    strColour - colour of the role text
///    bUseLiteralString - use the string literally
PROC SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK(STRING strName, STRING strRole, FLOAT fXOffset, STRING strColour, BOOL bUseLiteralString = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(g_sfMPCredits, "ADD_ROLE_TO_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strRole)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fXOffset)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strColour)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseLiteralString)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Adds a name to a scaleform credit block
/// PARAMS:
///    strName - string ID of the block to add to
///    strNames - string of the name
///    fXOffset - offset on the X axis
///    strDelimiter - colour of the name text
///    bUseLiteralString - use the string literally
PROC SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK(STRING strName, STRING strNames, FLOAT fXOffset, STRING strDelimiter, BOOL bUseLiteralString = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(g_sfMPCredits, "ADD_NAMES_TO_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strNames)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fXOffset)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strDelimiter)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseLiteralString)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Finishes editing the scaleform credit block and begins it displaying it on screen
/// PARAMS:
///    strName - string ID of the block to add to
///    fStepDuration - time between each line of the block in seconds
PROC SCALEFORM_SHOW_CREDIT_BLOCK(STRING strName, FLOAT fStepDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(g_sfMPCredits, "SHOW_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStepDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Removes the scaleform credit block from being displayed
/// PARAMS:
///    strName - string ID of the block to add to
///    fStepDuration - time between each line of the block in seconds
PROC SCALEFORM_HIDE_CREDIT_BLOCK(STRING strName, FLOAT fStepDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(g_sfMPCredits, "HIDE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStepDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Returns if the in-jet ped is a player
/// PARAMS:
///    FMCutData - 
///    iIndex - ID of the in-jet ped to check
/// RETURNS:
///    true/false
FUNC BOOL IS_JET_PED_A_PLAYER(FM_CUT_STRUCT &FMCutData, INT iIndex)
	IF FMCutData.bRegPlayerOn
	AND FMCutData.iJetCloneSourcePlayer[iIndex] <> -1
	AND IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(FMCutData.iJetCloneSourcePlayer[iIndex]), FALSE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if the in-jet ped is female
/// PARAMS:
///    FMCutData - 
///    iIndex - ID of the in-jet ped to check
/// RETURNS:
///    true/false
FUNC BOOL IS_JET_PED_FEMALE(FM_CUT_STRUCT &FMCutData, INT iIndex)
	IF IS_JET_PED_A_PLAYER(FMCutData, iIndex)
		RETURN IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(FMCutData.iJetCloneSourcePlayer[iIndex]))
	ELSE
		IF iIndex = 3
		OR iIndex = 4
		OR iIndex = 5
		OR iIndex = 6
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the in-jet ped Cutscene scene handle 
/// PARAMS:
///    iIndex - ID of the in-jet ped to check
/// RETURNS:
///    string of the scene handle
FUNC STRING GET_JET_PED_SCENE_HANDLE(INT iIndex)
	
	SWITCH iIndex
		CASE 0	RETURN "MP_Plane_Passenger_1"	BREAK
		CASE 1	RETURN "MP_Plane_Passenger_2"	BREAK
		CASE 2	RETURN "MP_Plane_Passenger_3"	BREAK
		CASE 3	RETURN "MP_Plane_Passenger_4"	BREAK
		CASE 4	RETURN "MP_Plane_Passenger_5"	BREAK
		CASE 5	RETURN "MP_Plane_Passenger_6"	BREAK
		CASE 6	RETURN "MP_Plane_Passenger_7"	BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Returns the ID of an in-jet ped to use next, depending on the ID passed in. The ID returned is in order of visibility in the shot.
/// PARAMS:
///    iIndex - ID of how important the entry is (starting with 0 as most important)
/// RETURNS:
///    the in-jet ped ID relevant to how important the iIndex is
FUNC INT GET_JET_PED_PRIORITY_INDEX(INT iIndex)
	SWITCH iIndex
		CASE 0	RETURN 5 /*Nearest female, right in shot with player*/				BREAK
		CASE 1	RETURN 6 /*Female Beside nearest female, next down camera line*/	BREAK
		CASE 2	RETURN 0 /*Nearest male, behind player*/							BREAK
		CASE 3	RETURN 4 /*Last second female*/										BREAK
		CASE 4	RETURN 1 /*Male in front of player*/								BREAK
		CASE 5	RETURN 3 /*Hidden female, seat beside seat in front of player*/		BREAK
		CASE 6	RETURN 2 /*Male hidden front left*/									BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Loops through the list of players and places them into an array if they are suitable to be used in the in-jet scene
/// PARAMS:
///    FMCutData - 
PROC CREATE_JET_CLONE_SOURCE_LIST(FM_CUT_STRUCT &FMCutData)
	INT iNextToRegister = 0
	INT i
	
	REPEAT NUMBER_OF_JET_POPULATION i
		FMCutData.iJetCloneSourcePlayer[i] = -1
	ENDREPEAT
	
	PLAYER_INDEX playerID
	REPEAT NUM_NETWORK_PLAYERS i
		playerID = INT_TO_PLAYERINDEX(i)
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			IF PLAYER_ID() <> playerID
				IF iNextToRegister < 4
					FMCutData.iJetCloneSourcePlayer[GET_JET_PED_PRIORITY_INDEX(iNextToRegister)] = i
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === FMCutData.iJetCloneSourcePlayer[", GET_JET_PED_PRIORITY_INDEX(iNextToRegister), 
												"] = ", GET_PLAYER_NAME(playerID))
					#ENDIF
					iNextToRegister++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Applies the ped component variations of the non-player in-jet peds that may be used in the jet scene
/// PARAMS:
///    FMCutData - 
///    iIndex - ID of the in-jet ped to setup the component variations of
PROC APPLY_JET_PED_VARIATION(FM_CUT_STRUCT &FMCutData, INT iIndex)

	SWITCH iIndex
		CASE 0//Nearest male, behind player
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HEAD, 21, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAIR, 9, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TORSO, 1, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_LEG, 9, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_FEET, 4, 8)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL, 15, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_DECL, 0, 0)
	//		SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_JBIB, 2, 5)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_JBIB, 10, 0)
			
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HEAD)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EYES)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EARS)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_MOUTH)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HIP)
		BREAK
		CASE 1//Male in front of player
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HEAD, 13, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAIR, 5, 4)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TORSO, 1, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_LEG, 10, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_FEET, 10, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TEETH, 11, 2)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL, 13, 6)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_JBIB, 10, 0)

			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HEAD)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EYES)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EARS)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_MOUTH)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HIP)
		BREAK
		CASE 2//Male hidden front left
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HEAD, 15, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAIR, 1, 4)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TORSO, 1, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_LEG, 0, 1)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_FEET, 1, 7)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL, 2, 9)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_JBIB, 6, 0)

			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HEAD)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EYES)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EARS)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_MOUTH)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HIP)
		BREAK
		CASE 3//Hidden female, seat beside seat in front of player
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HEAD, 14, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAIR, 5, 3)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TORSO, 3, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_LEG, 1, 6)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_FEET, 11, 5)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL, 2, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_JBIB, 3, 12)

			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HEAD)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EYES)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EARS)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_MOUTH)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HIP)
		BREAK
		CASE 4//Last second female
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HEAD, 18, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAIR, 15, 3)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TORSO, 15, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_LEG, 2, 5)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_FEET, 4, 6)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TEETH, 4, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL, 3, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_JBIB, 4, 0)

			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HEAD)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EYES)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EARS)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_MOUTH)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HIP)
		BREAK
		CASE 5//Nearest female, right in shot with player
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HEAD, 27, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAIR, 7, 3)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TORSO, 11, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_LEG, 4, 8)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_FEET, 13, 14)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TEETH, 5, 3)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL, 3, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_JBIB, 2, 7)

			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HEAD)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EYES)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EARS)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_MOUTH)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HIP)
		BREAK
		CASE 6//Female Beside nearest female, next down camera line
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HEAD, 16, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAIR, 15, 1)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TORSO, 3, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_LEG, 5, 6)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_FEET, 2, 8)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL, 2, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(FMCutData.pedJet[iIndex], PED_COMP_JBIB, 3, 7)

			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HEAD)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EYES)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_EARS)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_MOUTH)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_HAND)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_LEFT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_RIGHT_WRIST)
			CLEAR_PED_PROP(FMCutData.pedJet[iIndex], ANCHOR_HIP)
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Loads and creates the resources and indices required for the jet cutscene to run.
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    true when complete
FUNC BOOL CREATE_JET(FM_CUT_STRUCT &FMCutData)
	INT i
	VECTOR vCreate = <<-1200.000, -1490.000, 142.385>>
	
	IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiPlaneCreated)
	
		REQUEST_MODEL(P_CS_MP_JET_01_S)
		REQUEST_MODEL(MP_M_FREEMODE_01)
		REQUEST_MODEL(MP_F_FREEMODE_01)
		IF FMCutData.bPTFXOn
			REQUEST_PTFX_ASSET()
		ENDIF
					
		IF HAS_MODEL_LOADED(P_CS_MP_JET_01_S)
		AND HAS_MODEL_LOADED(MP_M_FREEMODE_01)
		AND HAS_MODEL_LOADED(MP_F_FREEMODE_01)
		AND ( (NOT FMCutData.bPTFXOn) OR HAS_PTFX_ASSET_LOADED())
		
			FMCutData.objPlane = CREATE_OBJECT(P_CS_MP_JET_01_S, vCreate, FALSE, FALSE, FALSE)
			
			SET_ENTITY_LOD_DIST(FMCutData.objPlane, 3000)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(FMCutData.objPlane, FALSE)
			SET_ENTITY_VISIBLE(FMCutData.objPlane, FALSE)
			
			IF FMCutData.bPTFXOn
				FMCutData.ptfxPlane0 = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_mp_intro_plane_exhaust", FMCutData.objPlane, <<-5.403, -8.000, -2.200>>, <<0.0,0.0,0.0>>)
				FMCutData.ptfxPlane1 = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_mp_intro_plane_exhaust", FMCutData.objPlane, <<6.629, -8.000, -2.200>>, <<0,0,0>>)
			ENDIF
			
			IF FMCutData.bRegPlayerOn
				CREATE_JET_CLONE_SOURCE_LIST(FMCutData)
			ENDIF
			
			REPEAT NUMBER_OF_JET_POPULATION i
				IF IS_JET_PED_FEMALE(FMCutData, i)
					FMCutData.pedJet[i] = CREATE_PED(PEDTYPE_MISSION, MP_F_FREEMODE_01, <<-1117.7778, -1557.6249, 3.3819>>, 0, FALSE, FALSE)
				ELSE
					FMCutData.pedJet[i] = CREATE_PED(PEDTYPE_MISSION, MP_M_FREEMODE_01, <<-1117.7778, -1557.6249, 3.3819>>, 0, FALSE, FALSE)
				ENDIF
				
				IF IS_JET_PED_A_PLAYER(FMCutData, i)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CLONE_PED_TO_TARGET(GET_PLAYER_PED(FMCutData.iJetCloneSourcePlayer[", i, "]), FMCutData.pedJet[", i, "])")
					#ENDIF
					IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(INT_TO_PLAYERINDEX(FMCutData.iJetCloneSourcePlayer[i])))
					ENDIF
					CLONE_PED_TO_TARGET(GET_PLAYER_PED(INT_TO_PLAYERINDEX(FMCutData.iJetCloneSourcePlayer[i])), FMCutData.pedJet[i])
					FINALIZE_HEAD_BLEND(FMCutData.pedJet[i])
				ELSE
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === APPLY_JET_PED_VARIATION(FMCutData, ", i, ")")
					#ENDIF
					APPLY_JET_PED_VARIATION(FMCutData, i)
				ENDIF
				
			ENDREPEAT
			
			SET_MODEL_AS_NO_LONGER_NEEDED(P_CS_MP_JET_01_S)
			
			SET_BIT(FMCutData.iLoadBitset, loadBiPlaneCreated)
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Cleans up and unloads the resources and indices used by the jet and in-jet peds
/// PARAMS:
///    FMCutData - 
PROC CLEANUP_JET(FM_CUT_STRUCT &FMCutData)
	INT i
	IF IS_BIT_SET(FMCutData.iLoadBitset, loadBiPlaneCreated)
		SET_MODEL_AS_NO_LONGER_NEEDED(P_CS_MP_JET_01_S)
		IF DOES_ENTITY_EXIST(FMCutData.objPlane)
			DELETE_OBJECT(FMCutData.objPlane)
		ENDIF
		IF FMCutData.bPTFXOn
			IF DOES_PARTICLE_FX_LOOPED_EXIST(FMCutData.ptfxPlane0)
				REMOVE_PARTICLE_FX(FMCutData.ptfxPlane0)
			ENDIF
			IF DOES_PARTICLE_FX_LOOPED_EXIST(FMCutData.ptfxPlane1)
				REMOVE_PARTICLE_FX(FMCutData.ptfxPlane1)
			ENDIF
			IF HAS_PTFX_ASSET_LOADED()
				REMOVE_PTFX_ASSET()
			ENDIF
		ENDIF
		
		REPEAT NUMBER_OF_JET_POPULATION i
			IF DOES_ENTITY_EXIST(FMCutData.pedJet[i])
				DELETE_PED(FMCutData.pedJet[i])
			ENDIF
		ENDREPEAT
		
		CLEAR_BIT(FMCutData.iLoadBitset, loadBiPlaneCreated)
	ENDIF
ENDPROC

/// PURPOSE:
///    Readies the in-jet peds to be used in the jet scene
PROC SET_CUTSCENE_COMPONENT_VARIATION_FOR_JET_POPULATION()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === SET_CUTSCENE_COMPONENT_VARIATION_FOR_JET_POPULATION")
	#ENDIF
	INT i
	REPEAT NUMBER_OF_JET_POPULATION i
		SET_CUTSCENE_ENTITY_STREAMING_FLAGS(GET_JET_PED_SCENE_HANDLE(i), DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
		//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(GET_JET_PED_SCENE_HANDLE(i), FMCutData.pedJet[i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Registers the in-jet peds with the cutscene
/// PARAMS:
///    FMCutData - 
PROC REGISTER_JET_POPULATION_FOR_CUTSCENE(FM_CUT_STRUCT &FMCutData)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === REGISTER_JET_POPULATION_FOR_CUTSCENE")
	#ENDIF
	INT i
	REPEAT NUMBER_OF_JET_POPULATION i
		IF NOT IS_ENTITY_DEAD(FMCutData.pedJet[i])
			FINALIZE_HEAD_BLEND(FMCutData.pedJet[i])
			REGISTER_ENTITY_FOR_CUTSCENE(	FMCutData.pedJet[i], GET_JET_PED_SCENE_HANDLE(i), CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT,
											CEO_IGNORE_MODEL_NAME)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Loads, creates, starts playback and pauses the helicopter that flies in the background of the second in-city shot
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    true when complete
FUNC BOOL CREATE_HELI_FLYPAST(FM_CUT_STRUCT &FMCutData)
	IF IS_BIT_SET(FMCutData.iLoadBitset, loadBiHeliFlypastCreated)
		RETURN TRUE
	ELSE
		MODEL_NAMES mHeli = FROGGER
		REQUEST_MODEL(mHeli)
		REQUEST_VEHICLE_RECORDING(8, "FMIntro")	
		IF HAS_MODEL_LOADED(mHeli)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(8, "FMIntro")
			IF NOT DOES_ENTITY_EXIST(FMCutData.vehHeli)
				FMCutData.vehHeli = CREATE_VEHICLE(mHeli, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(8, 0.0, "FMIntro"), 0.0, FALSE, FALSE)
				
				START_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehHeli, 8, "FMIntro")
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehHeli, 5000)
				PAUSE_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehHeli)
				
				//SET_ENTITY_LOD_DIST(FMCutData.vehHeli, 1000)
				
				SET_ENTITY_VISIBLE(FMCutData.vehHeli, FALSE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(mHeli)
				
				#IF IS_DEBUG_BUILD
					PRINT_INTRO_STRING("Created Heli")
				#ENDIF
			ENDIF
			
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(FMCutData.vehHeli)
			RETURN FALSE
		ENDIF
		
		SET_BIT(FMCutData.iLoadBitset, loadBiHeliFlypastCreated)
		
		RETURN TRUE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Deletes and unloads the helicopter used in the second city shot
/// PARAMS:
///    FMCutData - 
PROC CLEANUP_HELI_FLYPAST(FM_CUT_STRUCT &FMCutData)
	SET_MODEL_AS_NO_LONGER_NEEDED(FROGGER)
	REMOVE_VEHICLE_RECORDING(8, "FMIntro")
	IF DOES_ENTITY_EXIST(FMCutData.vehHeli)
		DELETE_VEHICLE(FMCutData.vehHeli)
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads, creates, starts playback and pauses the jets that are scene taxiing as the player's airliner lands
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    true when complete
FUNC BOOL CREATE_AIRPORT_JETS(FM_CUT_STRUCT &FMCutData)
	IF IS_BIT_SET(FMCutData.iLoadBitset, loadBiAirportJetsCreated)
		RETURN TRUE
	ELSE
		
		INT i
		VECTOR vSpawnCoords
		FLOAT fSpawnHeading
		MODEL_NAMES mnJet = SHAMAL
	
		REQUEST_MODEL(mnJet)
		REQUEST_MODEL(GET_RACE_DRIVER_MODEL())
		
		IF HAS_MODEL_LOADED(mnJet)
			REPEAT NUMBER_OF_AIRPORT_JETS i
				IF NOT DOES_ENTITY_EXIST(FMCutData.vehRacers[i])
				
					SWITCH i
						CASE 0
							vSpawnCoords = <<-1358.9728, -2361.9199, 12.9502>>
							fSpawnHeading = 149.9339
						BREAK
						CASE 1
							vSpawnCoords = <<-1302.0040, -2279.3777, 12.9468>>
							fSpawnHeading = 239.9712
						BREAK
					ENDSWITCH
				
					FMCutData.vehRacers[i] = CREATE_VEHICLE(mnJet, vSpawnCoords, fSpawnHeading, FALSE, FALSE)
					
					SET_VEHICLE_ON_GROUND_PROPERLY(FMCutData.vehRacers[i])
					
					FREEZE_ENTITY_POSITION(FMCutData.vehRacers[i], TRUE)
					
					SET_VEHICLE_LIGHTS(FMCutData.vehRacers[i], FORCE_VEHICLE_LIGHTS_ON)
					
					#IF IS_DEBUG_BUILD
						PRINT_INTRO_STRING("Created vehRacers")
					#ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		REPEAT NUMBER_OF_AIRPORT_JETS i
			IF NOT DOES_ENTITY_EXIST(FMCutData.vehRacers[i])
				RETURN FALSE
			ENDIF
		ENDREPEAT
		
		IF HAS_MODEL_LOADED(GET_RACE_DRIVER_MODEL())
			REPEAT NUMBER_OF_AIRPORT_JETS i
				IF NOT DOES_ENTITY_EXIST(FMCutData.pedRacers[i])
					IF DOES_ENTITY_EXIST(FMCutData.vehRacers[i])
						IF IS_VEHICLE_DRIVEABLE(FMCutData.vehRacers[i])
							FMCutData.pedRacers[i] = CREATE_PED_INSIDE_VEHICLE(FMCutData.vehRacers[i], PEDTYPE_CIVMALE,GET_RACE_DRIVER_MODEL(),VS_DRIVER,FALSE, FALSE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FMCutData.pedRacers[i], TRUE)
							SET_VEHICLE_ENGINE_ON(FMCutData.vehRacers[i], TRUE, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF		
		
		SET_MODEL_AS_NO_LONGER_NEEDED(mnJet)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_RACE_DRIVER_MODEL())
		
		SET_BIT(FMCutData.iLoadBitset, loadBiAirportJetsCreated)
		
		RETURN TRUE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Unfreezes the airport jets that taxi around as the player lands
/// PARAMS:
///    FMCutData - 
PROC START_AIRPORT_JETS(FM_CUT_STRUCT &FMCutData)
	IF NOT IS_BIT_SET(FMCutData.iBitset, biAirportJetsStarted)
		INT i
		VECTOR vDriveTo
	
		REPEAT NUMBER_OF_AIRPORT_JETS i
		
			SWITCH i
				CASE 0
					vDriveTo = <<-1403.4198, -2437.4473, 12.9496>>
				BREAK
				CASE 1
					vDriveTo = <<-1245.7578, -2311.9790, 12.9445>>
				BREAK
			ENDSWITCH
		
			IF NOT IS_PED_INJURED(FMCutData.pedRacers[i])
			AND IS_VEHICLE_DRIVEABLE(FMCutData.vehRacers[i])
				FREEZE_ENTITY_POSITION(FMCutData.vehRacers[i], FALSE)
				SET_VEHICLE_FORWARD_SPEED(FMCutData.vehRacers[i], 10.0)
				TASK_VEHICLE_DRIVE_TO_COORD(FMCutData.pedRacers[i], FMCutData.vehRacers[i], vDriveTo, 5.0, DRIVINGSTYLE_STRAIGHTLINE,
											GET_ENTITY_MODEL(FMCutData.vehRacers[i]), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
			ENDIF
		ENDREPEAT
		
		SET_BIT(FMCutData.iBitset, biAirportJetsStarted)
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes and unloads the taxiing airport jets seen as the player's jet lands
/// PARAMS:
///    FMCutData - 
PROC CLEANUP_AIRPORT_JETS(FM_CUT_STRUCT &FMCutData)

	INT i
	REPEAT NUMBER_OF_AIRPORT_JETS i
		IF DOES_ENTITY_EXIST(FMCutData.vehRacers[i])
			DELETE_VEHICLE(FMCutData.vehRacers[i])
		ENDIF
		IF DOES_ENTITY_EXIST(FMCutData.pedRacers[i])
			DELETE_PED(FMCutData.pedRacers[i])
		ENDIF
	ENDREPEAT
	
	SET_MODEL_AS_NO_LONGER_NEEDED(SHAMAL)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_RACE_DRIVER_MODEL())
ENDPROC

/// PURPOSE:
///    Loads and creates Lamar and the car that he drives in.
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    true when complete
FUNC BOOL CREATE_LAMAR_AND_VAN(FM_CUT_STRUCT &FMCutData)
	MODEL_NAMES mnLamar = IG_LAMARDAVIS
	MODEL_NAMES mnLamarCar = EMPEROR
	
	IF IS_BIT_SET(FMCutData.iLoadBitset, loadBiLamarCreated)
		RETURN TRUE
	ELSE
				
		REQUEST_MODEL(mnLamar)
		REQUEST_MODEL(mnLamarCar)
		REQUEST_VEHICLE_RECORDING(5, "FMINTRO")
		REQUEST_VEHICLE_RECORDING(6, "FMINTRO")
		REQUEST_VEHICLE_RECORDING(7, "FMINTRO")
		REQUEST_ANIM_DICT("mp_intro_seq@")
		IF HAS_MODEL_LOADED(mnLamar)
		AND HAS_MODEL_LOADED(mnLamarCar)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(5, "FMINTRO")
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(6, "FMINTRO")
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(7, "FMINTRO")
		AND HAS_ANIM_DICT_LOADED("mp_intro_seq@")
			IF NOT DOES_ENTITY_EXIST(FMCutData.vehLamar)
				CLEAR_AREA(<<-1034.0347, -2730.4849, 19.0565>>, 3.0, TRUE)
				FMCutData.vehLamar = CREATE_VEHICLE(mnLamarCar, <<-1032.3224, -2731.1919, 19.0658>>, 239.2169, FALSE, FALSE)
				SET_ENTITY_ALWAYS_PRERENDER(FMCutData.vehLamar, TRUE)
				SET_VEHICLE_RADIO_ENABLED(FMCutData.vehLamar, FALSE)
				SET_ENTITY_PROOFS(FMCutData.vehLamar, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
				#IF IS_DEBUG_BUILD
					PRINT_INTRO_STRING("Created Van")
				#ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(FMCutData.pedIGLamar)
					IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
						FMCutData.pedIGLamar = CREATE_PED(PEDTYPE_MISSION, mnLamar, << -1031.600, -2730.150, 20.150 >>, 143.6341, FALSE, FALSE)
						TASK_ENTER_VEHICLE(FMCutData.pedIGLamar, FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FMCutData.pedIGLamar, TRUE)
						SET_ENTITY_INVINCIBLE(FMCutData.vehLamar, TRUE)
						SET_ENTITY_INVINCIBLE(FMCutData.pedIGLamar, TRUE)
						SET_PED_RELATIONSHIP_GROUP_HASH(FMCutData.pedIGLamar, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
						SET_ENTITY_PROOFS(FMCutData.pedIGLamar, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
						
						#IF IS_DEBUG_BUILD
							PRINT_INTRO_STRING("Created IG Lamar")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(FMCutData.vehLamar)
		OR NOT DOES_ENTITY_EXIST(FMCutData.pedIGLamar)
			RETURN FALSE
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(mnLamar)
		SET_MODEL_AS_NO_LONGER_NEEDED(mnLamarCar)
		
		SET_BIT(FMCutData.iLoadBitset, loadBiLamarCreated)
		RETURN TRUE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Loads and creates the cutscene ped for Lamar. Readies him for dialogue
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    true when complete
FUNC BOOL CREATE_CS_LAMAR(FM_CUT_STRUCT &FMCutData)
	MODEL_NAMES mnLamar = CS_LAMARDAVIS
	
	IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiCSLamarCreated)
		
		REQUEST_MODEL(mnLamar)
		IF HAS_MODEL_LOADED(mnLamar)
			IF NOT DOES_ENTITY_EXIST(FMCutData.pedCSLamar)
				FMCutData.pedCSLamar = CREATE_PED(PEDTYPE_MISSION, mnLamar, <<-976.6968, -2679.8606, 35.6049>>, 138.0620, FALSE, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FMCutData.pedCSLamar, TRUE)
				SET_ENTITY_INVINCIBLE(FMCutData.pedCSLamar, TRUE)
				SET_ENTITY_VISIBLE(FMCutData.pedCSLamar, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(FMCutData.pedCSLamar, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
				SET_ENTITY_PROOFS(FMCutData.pedCSLamar, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
				
				ADD_PED_FOR_DIALOGUE(FMCutData.sSpeech, 1, FMCutData.pedCSLamar, "Lamar")
				#IF IS_DEBUG_BUILD
					PRINT_INTRO_STRING("Created CS Lamar")
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(FMCutData.pedCSLamar)
			RETURN FALSE
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(mnLamar)
		SET_BIT(FMCutData.iLoadBitset, loadBiCSLamarCreated)
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Loads the animation dictionaries required for the lamar drive scene based on the player's gender
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    true when complete
FUNC BOOL LOAD_DRIVE_ANIMS(FM_CUT_STRUCT &FMCutData)
	
	IF IS_INTRO_FEMALE(FMCutData)
		REQUEST_ANIM_DICT("mp_intro_seq@ig_1_lamar_drive_female")
		REQUEST_ANIM_DICT("mp_intro_seq@ig_1_lamar_drive_radio")
		IF HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_1_lamar_drive_female")
		AND HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_1_lamar_drive_radio")
			RETURN TRUE
		ENDIF
	ELSE
		REQUEST_ANIM_DICT("mp_intro_seq@ig_1_lamar_drive_male")
		REQUEST_ANIM_DICT("mp_intro_seq@ig_1_lamar_drive_radio")
		IF HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_1_lamar_drive_male")
		AND HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_1_lamar_drive_radio")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Loads and creates Lamar and his car in a position ready for skipping to the end of the intro
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    true when complete
FUNC BOOL CREATE_LAMAR_AND_VAN_FOR_SKIP(FM_CUT_STRUCT &FMCutData)
	MODEL_NAMES mnLamar = IG_LAMARDAVIS
	MODEL_NAMES mnLamarCar = EMPEROR
	
	REQUEST_MODEL(mnLamar)
	REQUEST_MODEL(mnLamarCar)
	IF HAS_MODEL_LOADED(mnLamar)
	AND HAS_MODEL_LOADED(mnLamarCar)
		IF NOT DOES_ENTITY_EXIST(FMCutData.vehLamar)
			FMCutData.vehLamar = CREATE_VEHICLE(mnLamarCar, <<-198.2210, 301.8606, 95.9455>>, 148.1586, FALSE, FALSE)
			SET_ENTITY_ALWAYS_PRERENDER(FMCutData.vehLamar, TRUE)
			SET_VEHICLE_RADIO_ENABLED(FMCutData.vehLamar, FALSE)
			SET_ENTITY_PROOFS(FMCutData.vehLamar, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_VEHICLE_LIGHTS(FMCutData.vehLamar, FORCE_VEHICLE_LIGHTS_ON)
			#IF IS_DEBUG_BUILD
				PRINT_INTRO_STRING("Created Van")
			#ENDIF
		ELSE
			IF NOT DOES_ENTITY_EXIST(FMCutData.pedIGLamar)
				IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
					
					FMCutData.pedIGLamar = CREATE_PED(PEDTYPE_MISSION, mnLamar, <<-195.2000, 307.1825, 95.9455>>, 143.6341, FALSE, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FMCutData.pedIGLamar, TRUE)
					SET_ENTITY_INVINCIBLE(FMCutData.vehLamar, TRUE)
					SET_ENTITY_INVINCIBLE(FMCutData.pedIGLamar, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(FMCutData.pedIGLamar, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
					SET_ENTITY_PROOFS(FMCutData.pedIGLamar, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
					
					#IF IS_DEBUG_BUILD
						PRINT_INTRO_STRING("Created Skip Lamar")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(FMCutData.vehLamar)
	OR NOT DOES_ENTITY_EXIST(FMCutData.pedIGLamar)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Converts a rotation to a direction vector
/// PARAMS:
///    vRot - vector of rotation
/// RETURNS:
///    vector of direction
FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

/// PURPOSE:
///    Runs a synchronised scene animated camera based on a passed origin coordinate
/// PARAMS:
///    FMCutData - 
///    animName - ID of the animation
///    animDict - ID of animation dictionary
///    vOrigin - coordinates of the origin to play the animation around
///    vRot - rotation of the animation origin
///    fStartPhase - phase (0.0 to 1.0) to begin playing from
///    fRate - rate to play the animation at
PROC RUN_SYNC_SCENE_CAM_ORIGIN(FM_CUT_STRUCT &FMCutData, STRING animName, STRING animDict, VECTOR vOrigin, VECTOR vRot, FLOAT fStartPhase = 0.0, FLOAT fRate = 1.0)

	IF NOT DOES_CAM_EXIST(FMCutData.animCam)
		FMCutData.animCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE) 
	ENDIF

	FMCutData.iSceneID = -1
	
	FMCutData.iSceneId = CREATE_SYNCHRONIZED_SCENE(vOrigin, vRot)

	PLAY_SYNCHRONIZED_CAM_ANIM(FMCutData.animCam, FMCutData.iSceneId, animName, animDict)
	
	SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iSceneID, fStartPhase)
	
	SET_SYNCHRONIZED_SCENE_RATE(FMCutData.iSceneID, fRate)
	
	SET_CAM_ACTIVE(FMCutData.animCam, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
		
ENDPROC

/// PURPOSE:
///    Runs a synchronised scene animated camera attached to an entity
/// PARAMS:
///    FMCutData - 
///    animName - ID of the animation
///    animDict - ID of the animation dictionary
///    entAttach - entity to play the animation attached to
///    vOrigin - offset from the entity to base the origin on
///    vRot - rotation offset from the entity to base the origin on
PROC RUN_SYNC_SCENE_CAM_ATTACH(FM_CUT_STRUCT &FMCutData, STRING animName, STRING animDict, ENTITY_INDEX entAttach, VECTOR vOrigin, VECTOR vRot)
	
	IF DOES_ENTITY_EXIST(entAttach)
		
		IF IS_ENTITY_DEAD(entAttach)
		ENDIF
	
		IF NOT DOES_CAM_EXIST(FMCutData.animCam)
			FMCutData.animCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE) 
		ENDIF
		
		FMCutData.iSceneID = -1
		
		FMCutData.iSceneId = CREATE_SYNCHRONIZED_SCENE(vOrigin, vRot)
		
		ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iSceneId, entAttach, 0)
		
		PLAY_SYNCHRONIZED_CAM_ANIM(FMCutData.animCam, FMCutData.iSceneId, animName, animDict)
		
		SET_CAM_ACTIVE(FMCutData.animCam, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
				
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Returns a safe coordinate near to the cutscene to hide the cars used in the ambient traffic
/// PARAMS:
///    iCarID - ID of the ambient traffic car
///    vCoords - vector to store the safe coordinate
///    fHeading - heading to store the safe coordinate's heading
PROC GET_AMBIENT_CAR_SAFE_COORD(INT iCarID, VECTOR &vCoords, FLOAT &fHeading)
	vCoords = <<0,0,0>>
	fHeading = 0.0
	
	SWITCH iCarID
		CASE 0		vCoords = <<-285.8592, -914.3868, 30.0800>>		fHeading = 77.3101		BREAK
		CASE 1		vCoords = <<-303.4057, -910.4340, 30.0797>>		fHeading = 77.3122		BREAK
		CASE 2		vCoords = <<-328.6838, -905.0965, 30.0783>>		fHeading = 74.4975		BREAK
		CASE 3		vCoords = <<-353.6149, -896.1278, 30.0707>>		fHeading = 358.6274		BREAK
		CASE 4		vCoords = <<-354.2628, -915.5025, 30.0800>>		fHeading = 358.0417		BREAK
		CASE 5		vCoords = <<-354.6489, -935.3478, 30.0800>>		fHeading = 0.5678		BREAK
		CASE 6		vCoords = <<-354.4816, -953.4573, 30.0800>>		fHeading = 0.4572		BREAK
		CASE 7		vCoords = <<-343.5193, -967.0103, 30.0800>>		fHeading = 247.6961		BREAK
		CASE 8		vCoords = <<-324.7872, -974.6605, 30.0800>>		fHeading = 247.7728		BREAK
		CASE 9		vCoords = <<-301.3824, -977.9114, 30.0800>>		fHeading = 249.2065		BREAK
		CASE 10		vCoords = <<-342.4198, -1019.4559, 29.3849>>	fHeading = 250.0953		BREAK
		CASE 11		vCoords = <<-317.1607, -1025.8585, 29.3849>>	fHeading = 250.7381		BREAK
		CASE 12		vCoords = <<-302.6580, -1022.4009, 29.3850>>	fHeading = 244.0013		BREAK
		CASE 13		vCoords = <<-283.3195, -1029.4685, 29.3850>>	fHeading = 252.7933		BREAK
		CASE 14		vCoords = <<-276.4286, -1013.5339, 29.3850>>	fHeading = 339.4764		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Loads and creates the ambient traffic cars used in the lamar drive sequence
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    true when complete
FUNC BOOL CREATE_AMBIENT_CARS_FOR_CUTSCENE(FM_CUT_STRUCT &FMCutData)
	INT iCount
	MODEL_NAMES mnCar
	VECTOR vCoords
	FLOAT fHeading
	
	IF IS_BIT_SET(FMCutData.iLoadBitset, loadBiAmbientCarsCreated)
		RETURN TRUE
	ELSE
		REQUEST_MODEL(ASTEROPE)
		REQUEST_MODEL(SENTINEL)
		REQUEST_MODEL(GET_RACE_DRIVER_MODEL())
					
		IF HAS_MODEL_LOADED(ASTEROPE)
		AND HAS_MODEL_LOADED(SENTINEL)
		AND HAS_MODEL_LOADED(GET_RACE_DRIVER_MODEL())
			REPEAT NUMBER_OF_AMBIENT_CARS iCount
				IF NOT DOES_ENTITY_EXIST(FMCutData.vehAmbient[iCount])
					IF iCount < (NUMBER_OF_AMBIENT_CARS/2)
						mnCar = ASTEROPE
					ELSE
						mnCar = SENTINEL
					ENDIF
					
					GET_AMBIENT_CAR_SAFE_COORD(iCount, vCoords, fHeading)
					
					FMCutData.vehAmbient[iCount] = CREATE_VEHICLE(mnCar, vCoords, fHeading, FALSE, FALSE)
					SET_VEHICLE_COLOUR_COMBINATION(FMCutData.vehAmbient[iCount], iCount)
					SET_ENTITY_INVINCIBLE(FMCutData.vehAmbient[iCount], TRUE)
					SET_VEHICLE_LIGHTS(FMCutData.vehAmbient[iCount], FORCE_VEHICLE_LIGHTS_ON)
					FREEZE_ENTITY_POSITION(FMCutData.vehAmbient[iCount], TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(FMCutData.vehAmbient[iCount])
					IF NOT DOES_ENTITY_EXIST(FMCutData.pedAmbient[iCount])
						//FMCutData.pedAmbient[iCount] = CREATE_RANDOM_PED_AS_DRIVER(FMCutData.vehAmbient[iCount])
						FMCutData.pedAmbient[iCount] = CREATE_PED_INSIDE_VEHICLE(FMCutData.vehAmbient[iCount], PEDTYPE_CIVMALE, GET_RACE_DRIVER_MODEL(),VS_DRIVER,FALSE, FALSE)
						//TASK_ENTER_VEHICLE(FMCutData.pedAmbient[iCount], FMCutData.vehAmbient[iCount], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
						SET_ENTITY_INVINCIBLE(FMCutData.pedAmbient[iCount], TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FMCutData.pedAmbient[iCount], TRUE)
					ENDIF
				ENDIF
				
			ENDREPEAT
		ENDIF
				
		REPEAT NUMBER_OF_AMBIENT_CARS iCount
			IF NOT DOES_ENTITY_EXIST(FMCutData.vehAmbient[iCount])
			OR NOT DOES_ENTITY_EXIST(FMCutData.pedAmbient[iCount])
				RETURN FALSE
			ENDIF
		ENDREPEAT
		
		SET_MODEL_AS_NO_LONGER_NEEDED(ASTEROPE)
		SET_MODEL_AS_NO_LONGER_NEEDED(SENTINEL)
		
		SET_BIT(FMCutData.iLoadBitset, loadBiAmbientCarsCreated)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Cleans up a car used in the ambient traffic of the lamar drive
/// PARAMS:
///    FMCutData - 
///    iCarID - ID of the car to delete
PROC CLEANUP_CAR_FOR_AMBIENT(FM_CUT_STRUCT &FMCutData, INT iCarID)
	IF DOES_ENTITY_EXIST(FMCutData.vehAmbient[iCarID])
		DELETE_VEHICLE(FMCutData.vehAmbient[iCarID])
	ENDIF
	IF DOES_ENTITY_EXIST(FMCutData.pedAmbient[iCarID])
		DELETE_PED(FMCutData.pedAmbient[iCarID])
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up all the cars used in the ambient traffic of the lamar drive
/// PARAMS:
///    FMCutData - 
PROC CLEANUP_CARS_FOR_AMBIENT(FM_CUT_STRUCT &FMCutData)
	INT iCount
	REPEAT NUMBER_OF_AMBIENT_CARS iCount
		CLEANUP_CAR_FOR_AMBIENT(FMCutData, iCount)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Places an ambient car at a defined position ready to start driving in the background of a scene
/// PARAMS:
///    FMCutData - 
///    iCarID - ID of ambient car to move
///    vCoords - coordinates to move the car to
///    fHeading - heading to match the coords
///    iBitset - bitset of the ambient cars used to mark which cars are being used
///    bFreeway - defines the speed that the car will drive at
PROC SETUP_CAR_FOR_AMBIENT(FM_CUT_STRUCT &FMCutData, INT iCarID, VECTOR vCoords, FLOAT fHeading, INT &iBitset, BOOL bFreeway = FALSE)
	
	IF IS_VEHICLE_DRIVEABLE(FMCutData.vehAmbient[iCarID])
	AND NOT IS_PED_INJURED(FMCutData.pedAmbient[iCarID])
		FREEZE_ENTITY_POSITION(FMCutData.vehAmbient[iCarID], FALSE)
		SET_ENTITY_COLLISION(FMCutData.vehAmbient[iCarID], TRUE)
		SET_ENTITY_COORDS(FMCutData.vehAmbient[iCarID], vCoords)
		SET_ENTITY_HEADING(FMCutData.vehAmbient[iCarID], fHeading)
		SET_VEHICLE_ON_GROUND_PROPERLY(FMCutData.vehAmbient[iCarID])
		SET_VEHICLE_FIXED(FMCutData.vehAmbient[iCarID])
		IF bFreeway
			TASK_VEHICLE_MISSION(FMCutData.pedAmbient[iCarID], FMCutData.vehAmbient[iCarID], NULL, MISSION_CRUISE, 35.0, DRIVINGMODE_STOPFORCARS, 5.0, 5.0)
			SET_VEHICLE_FORWARD_SPEED(FMCutData.vehAmbient[iCarID], 30.0)
		ELSE
			TASK_VEHICLE_MISSION(FMCutData.pedAmbient[iCarID], FMCutData.vehAmbient[iCarID], NULL, MISSION_CRUISE, 15.0, DRIVINGMODE_STOPFORCARS, 5.0, 5.0)
			SET_VEHICLE_FORWARD_SPEED(FMCutData.vehAmbient[iCarID], 10.0)
		ENDIF
		SET_PED_KEEP_TASK(FMCutData.pedAmbient[iCarID], TRUE)
		
		SET_ENTITY_VISIBLE(FMCutData.vehAmbient[iCarID], TRUE)
		SET_ENTITY_VISIBLE(FMCutData.pedAmbient[iCarID], TRUE)
		
		SET_BIT(iBitset, iCarID)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Returns the ambient traffic car to a safe coordinate to hide out of shot, but remain near to the cutscene
/// PARAMS:
///    FMCutData - 
///    iCarID - ID of the car to hide
PROC HIDE_CAR_FOR_AMBIENT(FM_CUT_STRUCT &FMCutData, INT iCarID)
	VECTOR vCoords
	FLOAT fHeading
	
	IF DOES_ENTITY_EXIST(FMCutData.vehAmbient[iCarID])
		IF IS_ENTITY_DEAD(FMCutData.vehAmbient[iCarID])
		ENDIF
		
		GET_AMBIENT_CAR_SAFE_COORD(iCarID, vCoords, fHeading)
		
		SET_ENTITY_COORDS(FMCutData.vehAmbient[iCarID], vCoords)
		SET_ENTITY_HEADING(FMCutData.vehAmbient[iCarID], fHeading)
		FREEZE_ENTITY_POSITION(FMCutData.vehAmbient[iCarID], TRUE)
		SET_VEHICLE_FIXED(FMCutData.vehAmbient[iCarID])
		SET_ENTITY_COLLISION(FMCutData.vehAmbient[iCarID], FALSE)
		SET_ENTITY_VISIBLE(FMCutData.vehAmbient[iCarID], FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(FMCutData.pedAmbient[iCarID])
		IF IS_ENTITY_DEAD(FMCutData.pedAmbient[iCarID])
		ENDIF
		
		SET_ENTITY_VISIBLE(FMCutData.pedAmbient[iCarID], FALSE)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Grabs any ambient cars that have fallen through the map and freezes them at a safe coordinate, rather than letting them pop up to ground level
/// PARAMS:
///    FMCutData - 
PROC STAGGER_CATCH_FALLING_AMBIENT_CARS(FM_CUT_STRUCT &FMCutData)
	
	IF DOES_ENTITY_EXIST(FMCutData.vehAmbient[FMCutData.iAmbientCarStagger])
		IF IS_ENTITY_DEAD(FMCutData.vehAmbient[FMCutData.iAmbientCarStagger])
		ENDIF
		VECTOR vTestAmbientHeight = GET_ENTITY_COORDS(FMCutData.vehAmbient[FMCutData.iAmbientCarStagger])
		IF vTestAmbientHeight.z < -50.0
			HIDE_CAR_FOR_AMBIENT(FMCutData, FMCutData.iAmbientCarStagger)
		ENDIF
	ENDIF
	
	FMCutData.iAmbientCarStagger++
	IF FMCutData.iAmbientCarStagger >= NUMBER_OF_AMBIENT_CARS
		FMCutData.iAmbientCarStagger = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Based on the cutscene stage passed, moves a selection of ambient cars into the correct position to drive in the background of the shot
/// PARAMS:
///    FMCutData - 
///    thisStage - stage to base the car positions on
PROC SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FM_CUT_STRUCT &FMCutData, INTRO_CUT_STAGE thisStage)
	INT iCount
	INT iCarUsedBitset
	
	SWITCH thisStage
		CASE INTRO_CUT_RACE_1
			SETUP_CAR_FOR_AMBIENT(FMCutData, 0, <<-2997.1738, 330.9234, 14.2341>>, -15.97, iCarUsedBitset, TRUE)
			//SETUP_CAR_FOR_AMBIENT(FMCutData, 2, <<-2996.9377, 138.7189, 13.9606>>, 34.5345, iCarUsedBitset, TRUE)
			//SETUP_CAR_FOR_AMBIENT(FMCutData, 9, <<-2983.9001, 423.7414, 14.4122>>, -5.25, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 2, <<-2983.9001, 423.7414, 14.4122>>, -5.25, iCarUsedBitset, TRUE)
		BREAK
		CASE INTRO_CUT_RACE_2
			SETUP_CAR_FOR_AMBIENT(FMCutData, 1, <<-2622.2075, -109.3386, 19.2643>>, 39.34, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 3, <<-2583.9497, -143.6469, 20.6224>>, 48.89, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 7, <<-2552.1448, -169.0495, 19.8664>>, 60.04, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 8, <<-2531.4744, -174.4581, 19.2269>>, 58.09, iCarUsedBitset, TRUE)
		BREAK
		CASE INTRO_CUT_RACE_3
			
		BREAK
		CASE INTRO_CUT_RACE_4
			SETUP_CAR_FOR_AMBIENT(FMCutData, 0, <<-2420.6177, -232.2668, 15.3525>>, 57.71, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 2, <<-2399.7092, -249.2471, 14.9551>>, 58.19, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 4, <<-2568.4990, -153.4655, 20.4075>>, 54.10, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 5, <<-2485.3921, -198.0737, 17.5783>>, 63.83, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 6, <<-2469.1389, -206.5518, 17.0201>>, 60.64, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 9, <<-2408.7327, -238.8328, 15.2055>>, 59.84, iCarUsedBitset, TRUE)
		BREAK
		CASE INTRO_CUT_RACE_5
			SETUP_CAR_FOR_AMBIENT(FMCutData, 1, <<-2146.3840, -323.4933, 12.0429>>, 14.8813, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 3, <<-2075.4441, -374.1310, 11.5560>>, 56.66, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 7, <<-2160.2520, -294.0829, 12.5163>>, 163.95, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 8, <<-2191.4287, -335.8769, 12.7225>>, 84.71, iCarUsedBitset, TRUE)
		BREAK
		CASE INTRO_CUT_RACE_6
			SETUP_CAR_FOR_AMBIENT(FMCutData, 0, <<-1833.8702, -561.7711, 11.0989>>, 48.11, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 1, <<-1630.5872, -685.6821, 16.5351>>, 58.23, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 2, <<-1668.7380, -662.7587, 14.7925>>, 60.52, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 3, <<-1564.9460, -738.6324, 10.7672>>, 68.32, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 4, <<-1706.5074, -720.8959, 9.6281>>, -132.94, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 5, <<-1626.7560, -769.9745, 10.2367>>, -128.80, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 6, <<-1782.7637, -610.5726, 10.3008>>, 52.34, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 7, <<-1595.2581, -733.5870, 10.8576>>, 68.39, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 8, <<-1751.8918, -682.1017, 9.6795>>, -134.89, iCarUsedBitset, TRUE)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 9, <<-1742.0983, -642.5658, 9.8169>>, 48.19, iCarUsedBitset, TRUE)
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_01
			SETUP_CAR_FOR_AMBIENT(FMCutData, 0, <<-934.4687, -2716.0378, 12.7365>>, 309.2312, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 5, <<-975.6617, -2733.5168, 12.8172>>, 267.4944, iCarUsedBitset)
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01
			
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_03
			
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02
			SETUP_CAR_FOR_AMBIENT(FMCutData, 6, <<-850.1183, -2615.2710, 14.1979>>, 327.7461, iCarUsedBitset)
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_05
			
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_07
			SETUP_CAR_FOR_AMBIENT(FMCutData, 0, <<-712.0925, -1983.5159, 25.6880>>, -167.99, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 1, <<-533.9934, -2004.7274, 26.9085>>, 58.71, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 2, <<-653.3551, -2047.7823, 27.3566>>, -112.14, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 3, <<-663.8210, -1934.8499, 27.1417>>, -121.65, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 4, <<-656.6017, -2009.0784, 30.0188>>, -106.53, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 5, <<-502.6960, -2023.0684, 26.2376>>, 60.2827, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 6, <<-460.1447, -2044.0320, 26.2371>>, 63.9022, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 7, <<-624.7944, -1960.9753, 26.2900>>, 237.3848, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 8, <<-463.0545, -1998.2552, 36.6533>>, 27.85, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 9, <<-473.4099, -1950.2133, 24.7576>>, 37.16, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData,10, <<-574.5834, -1994.9000, 26.2871>>, 236.0571, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData,11, <<-546.7894, -2012.7808, 26.3137>>, 235.9205, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData,12, <<-584.9290, -1970.3323, 26.2437>>, 55.8826, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData,13, <<-633.4936, -1937.6824, 26.2413>>, 54.9911, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData,14, <<-601.2188, -1941.4187, 17.4986>>, 148.6622, iCarUsedBitset)
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03
			SETUP_CAR_FOR_AMBIENT(FMCutData, 4, <<-499.2335, -1906.5316, 16.3534>>, 133.5751, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 9, <<-518.8654, -1918.1439, 16.3355>>, 134.6305, iCarUsedBitset)
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_10
			SETUP_CAR_FOR_AMBIENT(FMCutData, 3, <<-270.2197, -1499.7521, 29.4309>>, -13.07, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 7, <<-313.1461, -1559.5889, 24.2966>>, 145.66, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 8, <<-301.9485, -1550.0525, 25.5595>>, 146.95, iCarUsedBitset)
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_12
			
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_14
			
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_17
			SETUP_CAR_FOR_AMBIENT(FMCutData, 1, <<246.1231, -238.5137, 52.9482>>, 251.5966, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 2, <<214.2755, -202.6486, 53.5033>>, 70.72, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 6, <<263.1977, -115.1292, 69.0366>>, 155.02, iCarUsedBitset)
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_19
			SETUP_CAR_FOR_AMBIENT(FMCutData, 0, <<392.6941, 239.8680, 102.5932>>, 159.15, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 7, <<375.2224, 192.3160, 102.0397>>, 160.4247, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 12, <<377.4185, 166.3668, 102.0407>>, 337.9135, iCarUsedBitset)
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_20
			SETUP_CAR_FOR_AMBIENT(FMCutData, 1, <<267.8363, 181.3352, 103.7037>>, 71.7724, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 3, <<216.3894, 211.8832, 105.2308>>, -19.82, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 4, <<290.3365, 172.6194, 103.7313>>, 67.09, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 5, <<283.6507, 155.5740, 103.2640>>, 252.0678, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 8, <<309.5128, 156.7439, 103.3183>>, -112.52, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 9, <<249.0189, 168.3015, 104.5421>>, -112.05, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 11, <<313.7221, 144.8591, 102.6542>>, 249.0527, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 13, <<171.2032, 216.4137, 105.2411>>, 69.1778, iCarUsedBitset)
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_21
			
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04
			SETUP_CAR_FOR_AMBIENT(FMCutData, 2, <<226.2312, 176.6035, 104.3217>>, 252.5577, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 6, <<247.4283, 168.6965, 103.9378>>, 250.2977, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 7, <<211.3750, 181.7446, 104.5610>>, 250.2252, iCarUsedBitset)
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_24
			
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_25
			SETUP_CAR_FOR_AMBIENT(FMCutData, 2, <<-192.4192, 249.7100, 91.7534>>, -99.42, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 3, <<-45.3246, 256.5581, 105.0516>>, 272.9104, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 4, <<-24.1551, 272.3286, 106.4563>>, 96.6005, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 5, <<6.5526, 270.3987, 108.2680>>, 84.5591, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 6, <<-124.1576, 245.0871, 96.1105>>, -89.91, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 7, <<-140.9613, 262.5320, 94.5308>>, 87.51, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 9, <<-83.4766, 247.5941, 99.4880>>, 275.7796, iCarUsedBitset)
			SETUP_CAR_FOR_AMBIENT(FMCutData, 10, <<-215.5077, 252.4543, 91.0469>>, 264.7260, iCarUsedBitset)
		BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_26
			
		BREAK
		
	ENDSWITCH
	
	REPEAT NUMBER_OF_AMBIENT_CARS iCount
		IF NOT IS_BIT_SET(iCarUsedBitset, iCount)
			HIDE_CAR_FOR_AMBIENT(FMCutData, iCount)		//hide the cars unused this shot
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Creates the road blocking areas for the intro cutscene
PROC SETUP_ROAD_NODE_BLOCKING()
	SET_ROADS_IN_ANGLED_AREA(<<-533.667419,-1961.787720,16.826660>>, <<-429.507385,-1860.309692,22.796881>>, 20.5, FALSE, FALSE, FALSE)
	SET_ROADS_IN_ANGLED_AREA(<<224.497803,-274.769104,48.879707>>, <<263.158142,-170.510651,63.187721>>, 21.0, FALSE, FALSE, FALSE)
ENDPROC

/// PURPOSE:
///    Destroys the road blocking areas for the intro cutscene
PROC CLEAR_ROAD_NODE_BLOCKING()
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-533.667419,-1961.787720,16.826660>>, <<-429.507385,-1860.309692,22.796881>>, 20.5)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<224.497803,-274.769104,48.879707>>, <<263.158142,-170.510651,63.187721>>, 21.0)
ENDPROC

/// PURPOSE:
///    Creates the vagos peds who stand in the underpass shot in the Lamar drive
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    True when complete
FUNC BOOL CREATE_VAGOS_FOR_CUTSCENE(FM_CUT_STRUCT &FMCutData)
	INT iCount
	
	IF IS_BIT_SET(FMCutData.iLoadBitset, loadBiVagosCreated)
		RETURN TRUE
	ELSE
		REQUEST_MODEL(G_M_Y_MexGoon_02)
		REQUEST_ANIM_DICT("amb@world_human_leaning@male@wall@back@foot_up@idle_a")
		REQUEST_ANIM_DICT("amb@world_human_hang_out_street@male_a@base")
		
		IF HAS_MODEL_LOADED(G_M_Y_MexGoon_02)
		AND HAS_ANIM_DICT_LOADED("amb@world_human_leaning@male@wall@back@foot_up@idle_a")
		AND HAS_ANIM_DICT_LOADED("amb@world_human_hang_out_street@male_a@base")
			REPEAT NUMBER_OF_AMBIENT_VAGOS iCount
				IF NOT DOES_ENTITY_EXIST(FMCutData.pedVagos[iCount])
					FMCutData.pedVagos[iCount] = CREATE_PED(PEDTYPE_CIVMALE, G_M_Y_MexGoon_02, << -85.925, -1233.407, 27.759 >>, 0.0, FALSE, FALSE)
					SET_ENTITY_INVINCIBLE(FMCutData.pedVagos[iCount], TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FMCutData.pedVagos[iCount], TRUE)
					
					SWITCH iCount
						CASE 0
							TASK_PLAY_ANIM_ADVANCED(FMCutData.pedVagos[iCount], "amb@world_human_leaning@male@wall@back@foot_up@idle_a", "idle_a",
													<< -85.925, -1233.407, 27.759 >>, << 0.000, 0.000, 97.750 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
													AF_LOOPING|AF_OVERRIDE_PHYSICS)
						BREAK
						CASE 1
							TASK_PLAY_ANIM_ADVANCED(FMCutData.pedVagos[iCount], "amb@world_human_hang_out_street@male_a@base", "base",
													<< -86.487, -1232.985, 28.759 >>, << 0.000, 0.000, -110.500 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
													AF_LOOPING)
						BREAK
						CASE 2
							TASK_PLAY_ANIM_ADVANCED(FMCutData.pedVagos[iCount], "amb@world_human_leaning@male@wall@back@foot_up@idle_a", "idle_a",
													<< -82.139, -1203.525, 26.574 >>, << 0.000, 0.000, 90.750 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
													AF_LOOPING)
						BREAK
						CASE 3
							TASK_PLAY_ANIM_ADVANCED(FMCutData.pedVagos[iCount], "amb@world_human_hang_out_street@male_a@base", "base",
													<< -83.539, -1203.537, 27.549 >>, << 0.000, 0.000, -85.250 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
													AF_LOOPING)
						BREAK
						CASE 4
							TASK_PLAY_ANIM_ADVANCED(FMCutData.pedVagos[iCount], "amb@world_human_leaning@male@wall@back@foot_up@idle_a", "idle_a",
													<< -83.750, -1197.136, 26.471 >>, << 0.000, 0.000, -174.500 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
													AF_LOOPING)
						BREAK
						CASE 5
							SET_ENTITY_COORDS(FMCutData.pedVagos[iCount], <<-82.0542, -1199.2643, 26.5920>>)
							SET_ENTITY_HEADING(FMCutData.pedVagos[iCount], 76.8852)
							GIVE_WEAPON_TO_PED(FMCutData.pedVagos[iCount], WEAPONTYPE_ASSAULTRIFLE, 100, TRUE, TRUE)
							FREEZE_ENTITY_POSITION(FMCutData.pedVagos[iCount], TRUE)
						BREAK
						CASE 6
							TASK_PLAY_ANIM_ADVANCED(FMCutData.pedVagos[iCount], "amb@world_human_leaning@male@wall@back@foot_up@idle_a", "idle_a",
													<< -86.000, -1222.397, 27.417 >>, << 0.000, 0.000, 98.750 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
													AF_LOOPING)
						BREAK
						CASE 7
							SET_ENTITY_COORDS(FMCutData.pedVagos[iCount], <<-85.5161, -1205.7815, 26.5847>>)
							SET_ENTITY_HEADING(FMCutData.pedVagos[iCount], 87.5786)
							GIVE_WEAPON_TO_PED(FMCutData.pedVagos[iCount], WEAPONTYPE_ASSAULTRIFLE, 100, TRUE, TRUE)
							FREEZE_ENTITY_POSITION(FMCutData.pedVagos[iCount], TRUE)
						BREAK
						CASE 8
							TASK_PLAY_ANIM_ADVANCED(FMCutData.pedVagos[iCount], "amb@world_human_hang_out_street@male_a@base", "base",
													<< -87.100, -1221.762, 28.367 >>, << 0.000, 0.000, -85.250 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
													AF_LOOPING)
						BREAK
						CASE 9
							TASK_PLAY_ANIM_ADVANCED(FMCutData.pedVagos[iCount], "amb@world_human_hang_out_street@male_a@base", "base",
													<< -87.063, -1222.918, 28.417 >>, << 0.000, 0.000, -45.500 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
													AF_LOOPING)
						BREAK
					ENDSWITCH
					
				ENDIF
				
			ENDREPEAT
		ENDIF
				
		REPEAT NUMBER_OF_AMBIENT_VAGOS iCount
			IF NOT DOES_ENTITY_EXIST(FMCutData.pedVagos[iCount])
				RETURN FALSE
			ENDIF
		ENDREPEAT
		
		SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_MexGoon_02)
		REMOVE_ANIM_DICT("amb@world_human_leaning@male@wall@back@foot_up@idle_a")
		REMOVE_ANIM_DICT("amb@world_human_hang_out_street@male_a@base")
		
		SET_BIT(FMCutData.iLoadBitset, loadBiVagosCreated)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Cleans up the vagos peds that stand in the underpass during the Lamar drive
/// PARAMS:
///    FMCutData - 
PROC CLEANUP_VAGOS_FOR_CUTSCENE(FM_CUT_STRUCT &FMCutData)
	INT iCount
	REPEAT NUMBER_OF_AMBIENT_VAGOS iCount
		IF DOES_ENTITY_EXIST(FMCutData.pedVagos[iCount])
			DELETE_PED(FMCutData.pedVagos[iCount])
		ENDIF
	ENDREPEAT
	
	SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_MexGoon_02)
	REMOVE_ANIM_DICT("amb@world_human_leaning@male@wall@back@foot_up@idle_a")
	REMOVE_ANIM_DICT("amb@world_human_hang_out_street@male_a@base")
	
ENDPROC

/// PURPOSE:
///    Requests the models, vehicle recordings and anims required for the race shots at the starts of the cutscene
PROC REQUEST_UBER_ASSETS()
	REQUEST_MODEL(CHEETAH)
	REQUEST_MODEL(monroe)
	REQUEST_MODEL(ENTITYXF)
	REQUEST_MODEL(FELTZER2)
	REQUEST_MODEL(GET_RACE_DRIVER_MODEL())
	REQUEST_VEHICLE_RECORDING(100, "FM_Intro_uber")
	REQUEST_VEHICLE_RECORDING(101, "FM_Intro_uber")
	REQUEST_VEHICLE_RECORDING(102, "FM_Intro_uber")
	REQUEST_VEHICLE_RECORDING(103, "FM_Intro_uber")
	REQUEST_VEHICLE_RECORDING(104, "FM_Intro_uber")
	REQUEST_ANIM_DICT("mp_intro_seq@")
ENDPROC

/// PURPOSE:
///    Checks if the models, vehicle recordings and anims have been loaded
/// RETURNS:
///    True/False
FUNC BOOL HAVE_UBER_ASSSETS_LOADED()
	REQUEST_UBER_ASSETS()
	IF HAS_MODEL_LOADED(GET_RACE_DRIVER_MODEL())
		IF HAS_MODEL_LOADED(CHEETAH)
			IF HAS_MODEL_LOADED(monroe)
				IF HAS_MODEL_LOADED(ENTITYXF)
					IF HAS_MODEL_LOADED(FELTZER2)
						IF HAS_VEHICLE_RECORDING_BEEN_LOADED(100, "FM_Intro_uber")
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "FM_Intro_uber")
								IF HAS_VEHICLE_RECORDING_BEEN_LOADED(102, "FM_Intro_uber")
									IF HAS_VEHICLE_RECORDING_BEEN_LOADED(103, "FM_Intro_uber")
										IF HAS_VEHICLE_RECORDING_BEEN_LOADED(104, "FM_Intro_uber")
											IF HAS_ANIM_DICT_LOADED("mp_intro_seq@")
												RETURN TRUE
											ELSE
												#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === HAVE_UBER_ASSSETS_LOADED() = FALSE, HAS_ANIM_DICT_LOADED(mp_intro_seq@)")
												#ENDIF
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === HAVE_UBER_ASSSETS_LOADED() = FALSE, HAS_VEHICLE_RECORDING_BEEN_LOADED(104, FM_Intro_uber)")
											#ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === HAVE_UBER_ASSSETS_LOADED() = FALSE, HAS_VEHICLE_RECORDING_BEEN_LOADED(103, FM_Intro_uber)")
										#ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === HAVE_UBER_ASSSETS_LOADED() = FALSE, HAS_VEHICLE_RECORDING_BEEN_LOADED(102, FM_Intro_uber)")
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === HAVE_UBER_ASSSETS_LOADED() = FALSE, HAS_VEHICLE_RECORDING_BEEN_LOADED(101, FM_Intro_uber)")
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === HAVE_UBER_ASSSETS_LOADED() = FALSE, HAS_VEHICLE_RECORDING_BEEN_LOADED(100, FM_Intro_uber)")
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === HAVE_UBER_ASSSETS_LOADED() = FALSE, HAS_MODEL_LOADED(FELTZER2)")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === HAVE_UBER_ASSSETS_LOADED() = FALSE, HAS_MODEL_LOADED(ENTITYXF)")
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === HAVE_UBER_ASSSETS_LOADED() = FALSE, HAS_MODEL_LOADED(monroe)")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === HAVE_UBER_ASSSETS_LOADED() = FALSE, HAS_MODEL_LOADED(CHEETAH)")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === HAVE_UBER_ASSSETS_LOADED() = FALSE, HAS_MODEL_LOADED(GET_RACE_DRIVER_MODEL())")
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Creatse the racing cars and drives for the race scene at the start of the cutscene
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    True when complete
FUNC BOOL CREATE_MAIN_UBER_REC_CAR(FM_CUT_STRUCT &FMCutData)

	BOOL bFinished = TRUE
	
	IF IS_BIT_SET(FMCutData.iLoadBitset, loadBiUberCreated)
		RETURN TRUE
	ELSE

		REQUEST_UBER_ASSETS()

		IF NOT DOES_ENTITY_EXIST(FMCutData.vehRacers[0])
			IF HAS_MODEL_LOADED(CHEETAH)
				FMCutData.vehRacers[0] = CREATE_VEHICLE(CHEETAH, <<-3007.9121, 370.6909, 13.7517>>, 184.1746, FALSE, FALSE)
				SET_VEHICLE_RADIO_ENABLED(FMCutData.vehRacers[0], FALSE)
				SET_VEHICLE_TYRES_CAN_BURST(FMCutData.vehRacers[0], FALSE)			//black sleek cheetah, 1st
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CREATE_MAIN_UBER_REC_CAR() = FALSE, HAS_MODEL_LOADED(CHEETAH)")
				#ENDIF
				bFinished = FALSE
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(FMCutData.vehRacers[1])
			IF HAS_MODEL_LOADED(entityxf)
				FMCutData.vehRacers[1] = CREATE_VEHICLE(entityxf,<<-2999.3831, 404.3465, 14.3213>>, 184.1746, FALSE, FALSE)
				SET_VEHICLE_RADIO_ENABLED(FMCutData.vehRacers[1], FALSE)
				SET_ENTITY_QUATERNION(FMCutData.vehRacers[1], -0.0001,  -0.0020, 0.9978, 0.0661)
				SET_VEHICLE_COLOUR_COMBINATION(FMCutData.vehRacers[1], 8)
				SET_VEHICLE_TYRES_CAN_BURST(FMCutData.vehRacers[1], FALSE)			//lime green ferarri, 2nd
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CREATE_MAIN_UBER_REC_CAR() = FALSE, HAS_MODEL_LOADED(entityxf)")
				#ENDIF
				bFinished = FALSE
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(FMCutData.vehRacers[2])
			IF HAS_MODEL_LOADED(monroe)	
				FMCutData.vehRacers[2] =CREATE_VEHICLE(monroe,<<-2994.4165, 435.5293, 14.2780>>, 184.1746, FALSE, FALSE)
				SET_VEHICLE_RADIO_ENABLED(FMCutData.vehRacers[2], FALSE)
				SET_ENTITY_QUATERNION(FMCutData.vehRacers[2], 0.0072,  -0.0028, 0.9981, 0.0612)
				SET_VEHICLE_COLOUR_COMBINATION(FMCutData.vehRacers[2], 2)
				SET_VEHICLE_TYRES_CAN_BURST(FMCutData.vehRacers[2], FALSE)			//red monroe, 4th
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CREATE_MAIN_UBER_REC_CAR() = FALSE, HAS_MODEL_LOADED(monroe)")
				#ENDIF
				bFinished = FALSE
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(FMCutData.vehRacers[3])
			IF HAS_MODEL_LOADED(cheetah)		
				FMCutData.vehRacers[3] = CREATE_VEHICLE(cheetah, <<-2992.2593, 473.8287, 14.5044>>, 184.1746, FALSE, FALSE)
				SET_VEHICLE_RADIO_ENABLED(FMCutData.vehRacers[3], FALSE)
				SET_ENTITY_QUATERNION(FMCutData.vehRacers[3], 0.0071,  -0.0058,0.9993, 0.0375)
				SET_VEHICLE_COLOUR_COMBINATION(FMCutData.vehRacers[3], 15)
				SET_VEHICLE_TYRES_CAN_BURST(FMCutData.vehRacers[3], FALSE)			//yellow cheetah, 5th
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CREATE_MAIN_UBER_REC_CAR() = FALSE, HAS_MODEL_LOADED(cheetah)")
				#ENDIF
				bFinished = FALSE
			ENDIF
		ENDIF
				
		IF NOT DOES_ENTITY_EXIST(FMCutData.vehRacers[4])
			IF HAS_MODEL_LOADED(FELTZER2)		
				FMCutData.vehRacers[4] = CREATE_VEHICLE(FELTZER2, <<-2988.2461, 425.2664, 14.3870>>, 184.1746, FALSE, FALSE)
				SET_VEHICLE_RADIO_ENABLED(FMCutData.vehRacers[4], FALSE)
				SET_ENTITY_QUATERNION(FMCutData.vehRacers[4], 0.0026, -0.0014, 0.9980, 0.0637)
				SET_VEHICLE_COLOURS(FMCutData.vehRacers[4], 0, 0)
				SET_VEHICLE_EXTRA_COLOURS(FMCutData.vehRacers[4], 0, 156) 	//Black feltzer2, cardick, 3rd
				SET_VEHICLE_MOD_KIT(FMCutData.vehRacers[4], 0)
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CREATE_MAIN_UBER_REC_CAR() = FALSE, HAS_MODEL_LOADED(FELTZER2)")
				#ENDIF
				bFinished = FALSE
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(FMCutData.vehRacers[4])
				IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiModsSetOnRacer)
					IF GET_NUM_MOD_KITS(FMCutData.vehRacers[4]) > 0
						TOGGLE_VEHICLE_MOD(FMCutData.vehRacers[4], MOD_TOGGLE_XENON_LIGHTS, TRUE)
						SET_VEHICLE_TYRES_CAN_BURST(FMCutData.vehRacers[4], FALSE)
						SET_BIT(FMCutData.iLoadBitset, loadBiModsSetOnRacer)
					ELSE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CREATE_MAIN_UBER_REC_CAR() = FALSE, GET_NUM_MOD_KITS(FMCutData.vehRacers[4]) <= 0")
						#ENDIF
						bFinished = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		INT i
		IF HAS_MODEL_LOADED(GET_RACE_DRIVER_MODEL())
			FOR i = 0 TO 4
				IF NOT DOES_ENTITY_EXIST(FMCutData.pedRacers[i])
					IF DOES_ENTITY_EXIST(FMCutData.vehRacers[i])
						IF IS_VEHICLE_DRIVEABLE(FMCutData.vehRacers[i])
							FMCutData.pedRacers[i] = CREATE_PED_INSIDE_VEHICLE(FMCutData.vehRacers[i], PEDTYPE_CIVMALE,GET_RACE_DRIVER_MODEL(),VS_DRIVER,FALSE, FALSE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FMCutData.pedRacers[i], TRUE)
							SET_VEHICLE_ENGINE_ON(FMCutData.vehRacers[i], TRUE, TRUE)
							#IF IS_DEBUG_BUILD 
								PRINT_INTRO_STRING_INT("Created race driver ", i) 
								TEXT_LABEL_15 tl15 = ""
								tl15 += i
								SET_PED_NAME_DEBUG(FMCutData.pedRacers[i], tl15)
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CREATE_MAIN_UBER_REC_CAR() = FALSE, HAS_MODEL_LOADED(GET_RACE_DRIVER_MODEL())")
			#ENDIF
			bFinished = FALSE
		ENDIF
		
		IF bFinished
			SET_MODEL_AS_NO_LONGER_NEEDED(CHEETAH)
			SET_MODEL_AS_NO_LONGER_NEEDED(MONROE)
			SET_MODEL_AS_NO_LONGER_NEEDED(ENTITYXF)
			SET_MODEL_AS_NO_LONGER_NEEDED(FELTZER2)
			
			SET_BIT(FMCutData.iLoadBitset, loadBiUberCreated)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Destroys and unloads the resources required for the racing scene at the start of the cutscene
/// PARAMS:
///    FMCutData - 
PROC CLEANUP_UBER_ASSETS(FM_CUT_STRUCT &FMCutData)
	ENTITY_INDEX eTemp
	INT i
	FOR i = 0 TO 4
		IF DOES_ENTITY_EXIST(FMCutData.vehRacers[i])
			eTemp = FMCutData.vehRacers[i]
			DELETE_ENTITY(eTemp)
		ENDIF
		IF DOES_ENTITY_EXIST(FMCutData.pedRacers[i])
			eTemp = FMCutData.pedRacers[i]
			DELETE_ENTITY(eTemp)
		ENDIF
	ENDFOR
	
	SET_MODEL_AS_NO_LONGER_NEEDED(CHEETAH)
	SET_MODEL_AS_NO_LONGER_NEEDED(MONROE)
	SET_MODEL_AS_NO_LONGER_NEEDED(ENTITYXF)
	SET_MODEL_AS_NO_LONGER_NEEDED(FELTZER2)
	REMOVE_VEHICLE_RECORDING(100, "FM_Intro_uber")
	REMOVE_VEHICLE_RECORDING(101, "FM_Intro_uber")
	REMOVE_VEHICLE_RECORDING(102, "FM_Intro_uber")
	REMOVE_VEHICLE_RECORDING(103, "FM_Intro_uber")
	REMOVE_VEHICLE_RECORDING(104, "FM_Intro_uber")
	REMOVE_ANIM_DICT("mp_intro_seq@")
ENDPROC

/// PURPOSE:
///    Jumps a vehicle to a defined time in a vehicle recording
/// PARAMS:
///    thisVehicle - vehicle to play the vehicle recording on
///    FileNumber - vehicle recording ID number
///    sRecordingName - vehicle recording ID name
///    fSkip - time to skip through the vehicle recording
///    bForceStart - forces this vehicle recording to be played, even if vehicle is already playing recording
PROC SKIP_THIS_CAR_TO_TIME(VEHICLE_INDEX thisVehicle, INT FileNumber, STRING sRecordingName, FLOAT fSkip, BOOL bForceStart = FALSE)
	IF IS_VEHICLE_DRIVEABLE(thisVehicle)
		
		IF bForceStart
			STOP_PLAYBACK_RECORDED_VEHICLE(thisVehicle)
			START_PLAYBACK_RECORDED_VEHICLE(thisVehicle, FileNumber, sRecordingName)
		ELSE
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(thisVehicle)
				START_PLAYBACK_RECORDED_VEHICLE(thisVehicle, FileNumber, sRecordingName)
				SET_VEHICLE_ACTIVE_DURING_PLAYBACK(thisVehicle, TRUE)
			ELSE
				RECORDING_ID thisRecording = GET_VEHICLE_RECORDING_ID(FileNumber, sRecordingName)
				IF GET_CURRENT_PLAYBACK_FOR_VEHICLE(thisVehicle) <> thisRecording
					START_PLAYBACK_RECORDED_VEHICLE(thisVehicle, FileNumber, sRecordingName)
					SET_VEHICLE_ACTIVE_DURING_PLAYBACK(thisVehicle, TRUE)
				ENDIF
			ENDIF
		ENDIF
			
		SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(thisVehicle, fSKip - GET_TIME_POSITION_IN_RECORDING(thisVehicle))
	ENDIF
ENDPROC

/// PURPOSE:
///    Skips all the cars in the race scene at the start of the cutscene to a specified time in their vehicle recordings
/// PARAMS:
///    FMCutData - 
///    fSKip - time to skip all the cars to
///    fCar2Offset - special time offset for the third car
PROC SKIP_ALL_CARS(FM_CUT_STRUCT &FMCutData, FLOAT fSKip, FLOAT fCar2Offset = 0.0)
	SKIP_THIS_CAR_TO_TIME(FMCutData.vehRacers[0], 100, "FM_Intro_uber", fSKip)
	SKIP_THIS_CAR_TO_TIME(FMCutData.vehRacers[1], 101, "FM_Intro_uber", fSKip)
	SKIP_THIS_CAR_TO_TIME(FMCutData.vehRacers[2], 102, "FM_Intro_uber", fSKip + fCar2Offset)
	SKIP_THIS_CAR_TO_TIME(FMCutData.vehRacers[3], 103, "FM_Intro_uber", fSKip)
	SKIP_THIS_CAR_TO_TIME(FMCutData.vehRacers[4], 104, "FM_Intro_uber", fSKip)
ENDPROC	

/// PURPOSE:
///    Skips Lamar's car through the vehicle recording in the Lamar drive scene
/// PARAMS:
///    FMCutData - 
///    fSKip - time to skip forward
PROC SKIP_LAMAR_CAR(FM_CUT_STRUCT &FMCutData, FLOAT fSKip)
	SKIP_THIS_CAR_TO_TIME(FMCutData.vehLamar, 5, "FMINTRO", fSKip)
ENDPROC

/// PURPOSE:
///    Skips Lamar's car through the vehicle recording in the Lamar drive scene at the end when Lamar is turning into the car park
/// PARAMS:
///    FMCutData - 
///    fSKip - time to skip forward
PROC SKIP_LAMAR_CAR_TURN_RAMP_RECORDING(FM_CUT_STRUCT &FMCutData, FLOAT fSKip)
	SKIP_THIS_CAR_TO_TIME(FMCutData.vehLamar, 6, "FMINTRO", fSKip, TRUE)
ENDPROC

/// PURPOSE:
///    Skips Lamar's car through the vehicle recording in the Lamar drive scene at the end when Lamar is turning into the car parking space
/// PARAMS:
///    FMCutData - 
///    fSKip - time to skip forward
PROC SKIP_LAMAR_CAR_TURN_PARK_RECORDING(FM_CUT_STRUCT &FMCutData, FLOAT fSKip)
	SKIP_THIS_CAR_TO_TIME(FMCutData.vehLamar, 7, "FMINTRO", fSKip, TRUE)
ENDPROC

/// PURPOSE:
///    Turns on or off ambient peds and vehicles
/// PARAMS:
///    bOnOff - switch on or off
PROC SWITCH_AMBIENT_PEDS_AND_VEHICLES(BOOL bOnOff)
	IF bOnOff
		SET_ALL_VEHICLE_GENERATORS_ACTIVE()
		SET_NUMBER_OF_PARKED_VEHICLES(-1)
		SET_GARBAGE_TRUCKS(TRUE)
		SET_ALL_LOW_PRIORITY_VEHICLE_GENERATORS_ACTIVE(TRUE)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
		SET_VEHICLE_POPULATION_BUDGET(3)
		SET_PED_POPULATION_BUDGET(3)
	ELSE
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-9000.0, -9000.0, -1000.0>>, <<9000.0, 9000.0, 1500.0>>, FALSE)
		SET_NUMBER_OF_PARKED_VEHICLES(0)
		SET_GARBAGE_TRUCKS(FALSE)
		SET_ALL_LOW_PRIORITY_VEHICLE_GENERATORS_ACTIVE(FALSE)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_VEHICLE_POPULATION_BUDGET(0)
		SET_PED_POPULATION_BUDGET(0)
	ENDIF
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//											MISC FUNCTIONS													//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    
//PURPOSE: Cleans up the Freemode Intro Cutscene

/// PURPOSE:
///    Cleans up the Freemode Intro Cutscene
/// PARAMS:
///    FMCutData - 
///    bTurnOffPlayerControl - Sets the player control off as cutscene ends
///    bEmergencyTermination - Cleans up elements that are usually left at the end of the cutscene when playing in full at the start of the game
PROC CLEANUP_FREEMODE_INTRO_CUTSCENE(FM_CUT_STRUCT &FMCutData, BOOL bTurnOffPlayerControl = FALSE, BOOL bEmergencyTermination = FALSE)
	IF NOT IS_BIT_SET(FMCutData.iBitset, biCleanupDone)
		#IF IS_DEBUG_BUILD
			PRINT_INTRO_STRING("Cleaning up intro cut. Callstack...")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
		
		IF IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE()
		ENDIF
		
		DISABLE_METRO_SYSTEM(FALSE)
		
		//SRL end the Streaming Request List
		IF FMCutData.bSRLOn
			SET_SRL_LONG_JUMP_MODE(FALSE)
			END_SRL()
			CLEAR_BIT(fmcutdata.iBitset, biDoSRLTime)
		ENDIF
		
		IF IS_BIT_SET(FMCutData.iBitset, biFocusSet)
			CLEAR_FOCUS()
			CLEAR_BIT(FMCutData.iBitset, biFocusSet)
		ENDIF
		
		CLEAR_ROAD_NODE_BLOCKING()
		
		CLEANUP_JET(FMCutData)
		
		CLEANUP_HELI_FLYPAST(FMCutData)
		
		CLEANUP_AIRPORT_JETS(FMCutData)
		
		CLEANUP_GTAO_LOGO()
		
		CLEANUP_INTRO_CUT_CUSTOM_GARAGE_MODELS(FMCutData)
		
		CLEANUP_UBER_ASSETS(FMCutData)
		
		CLEANUP_CARS_FOR_AMBIENT(FMCutData)
		
		CLEANUP_VAGOS_FOR_CUTSCENE(FMCutData)
		
		REMOVE_SCENARIO_BLOCKING_AREAS()
		
		IF bEmergencyTermination
			IF DOES_ENTITY_EXIST(FMCutData.vehLamar)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(FMCutData.vehLamar)
			ENDIF
			IF DOES_ENTITY_EXIST(FMCutData.pedIGLamar)
				SET_PED_AS_NO_LONGER_NEEDED(FMCutData.pedIGLamar)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(FMCutData.pedCSLamar)
			DELETE_PED(FMCutData.pedCSLamar)
		ENDIF
		
		IF NOT FMCutData.bLaunchFmIntro
			HIDE_ALL_SHOP_BLIPS(FALSE)		
		ENDIF
		Unpause_Objective_Text()
		Enable_MP_Comms()

		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		SET_WIDESCREEN_BORDERS(FALSE, -1)
		SET_USER_RADIO_CONTROL_ENABLED(TRUE)
		
		IF NETWORK_IS_GAME_IN_PROGRESS() // John G handles cleaning up if Network game isn't running. See 892498
			IF NETWORK_IS_IN_MP_CUTSCENE() // Wrapped in check to prevent John gurney's assert that fires if NETWORK_SET_IN_MP_CUTSCENE with the same value for it's agrument more than once.
				NETWORK_SET_IN_MP_CUTSCENE(FALSE)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
			TRIGGER_MUSIC_EVENT("FM_INTRO_DRIVE_END")
			#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("[IntroCue] Playing cue FM_INTRO_DRIVE_END") #ENDIF
			CLEAR_BIT(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
		ENDIF
		
		NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
		SWITCH_AMBIENT_PEDS_AND_VEHICLES(TRUE)
		
		IF IS_BIT_SET(FMCutData.iBitset, biScreenFrozen)
			TOGGLE_RENDERPHASES(TRUE)
			CLEAR_BIT(FMCutData.iBitset, biScreenFrozen)
		ENDIF
		
		MPGlobals.bStartedMPCutscene = FALSE 
		
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			SET_RENDER_HD_ONLY(FALSE)
			#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Cleanup -  SET_RENDER_HD_ONLY(FALSE)") #ENDIF
		ELSE
			#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Not calling SET_RENDER_HD_ONLY(FALSE)") #ENDIF
		ENDIF
		SET_GAME_PAUSES_FOR_STREAMING(TRUE)
		IF bTurnOffPlayerControl
		//	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS )
		//	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ELSE	
		//	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		SET_XP_BAR_ACTIVE_IN_CUTSCENE(FALSE)
		SET_DPADDOWN_ACTIVE(TRUE)
		
		MPGlobalsAmbience.bFMIntroCutDemandingDpadDownActive = FALSE
		
		MPGlobalsAmbience.bRunningFmIntroCut = FALSE
		
		IF IS_AUDIO_SCENE_ACTIVE("MP_INTRO_RACE_SCENE")
			STOP_AUDIO_SCENE("MP_INTRO_RACE_SCENE")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("MP_INTRO_LAMAR_DRIVE_SCENE")
			STOP_AUDIO_SCENE("MP_INTRO_LAMAR_DRIVE_SCENE")
		ENDIF
				
		SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEL, TRUE)
		
		SET_MAX_WANTED_LEVEL(5)
		
		UNLOAD_CLOUD_HAT("CONTRAILS")
		
		REMOVE_VEHICLE_RECORDING(5, "FMINTRO")
		REMOVE_VEHICLE_RECORDING(6, "FMINTRO")
		REMOVE_VEHICLE_RECORDING(7, "FMINTRO")
		REMOVE_ANIM_DICT("mp_intro_seq@")
		
		REMOVE_ANIM_DICT("mp_intro_seq@ig_1_lamar_drive_female")
		REMOVE_ANIM_DICT("mp_intro_seq@ig_1_lamar_drive_radio")
		REMOVE_ANIM_DICT("mp_intro_seq@ig_1_lamar_drive_male")
		
		IF DOES_BLIP_EXIST(GET_MAIN_PLAYER_BLIP_ID())
			SET_BLIP_SCALE(GET_MAIN_PLAYER_BLIP_ID(), BLIP_SIZE_NETWORK_PED)
			NET_PRINT("FREEMODE INTRO CUTSCENE -  PLAYER BLIP MADE VISIBLE")NET_NL()
		ENDIF
			
		SET_RADAR_ZOOM(0)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		
		//-- Outside airport
		IF NETWORK_IS_GAME_IN_PROGRESS()
			REMOVE_NAVMESH_REQUIRED_REGIONS()
			#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Called NETWORK_SET_VOICE_ACTIVE(TRUE)") #ENDIF 
			NETWORK_SET_VOICE_ACTIVE(TRUE)
		ENDIF
		
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		
		IF NOT IS_SCREEN_FADED_IN()
		AND NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		SET_BIT(FMCutData.iBitset, biCleanupDone)
	ELSE
		#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Running CLEANUP_FREEMODE_INTRO_CUTSCENE but bit already set") #ENDIF 
	ENDIF
ENDPROC

/// PURPOSE:
///    Disables local player mouth being moved by microphone noise
/// PARAMS:
///    playerID - player to disable the mouth movement of
PROC MANAGE_PLAYER_FACE_ANIMATION(PLAYER_INDEX playerID)
	//IF NETWORK_PLAYER_HAS_HEADSET(playerID)				
	//	IF NETWORK_IS_PLAYER_TALKING(playerID)
			SET_PED_RESET_FLAG(GET_PLAYER_PED(playerID), PRF_DisableVoiceDrivenMouthMovement, TRUE)
	//	ENDIF
	//ENDIF
ENDPROC

/// PURPOSE:
///    Gets the length of each cutscene shot as stored in the Streaming Request List. This is different to the lenght of each actual cutscene short due to the cutscene changing after the Streaming Request List was locked into being on the disc.
/// PARAMS:
///    thisStage - stage to get the length of
/// RETURNS:
///    the shot length as in INT in ms
FUNC INT GET_SRL_SHOT_LENGTH(INTRO_CUT_STAGE thisStage)
	
	SWITCH thisStage
		//old SRL pre-27/08/2013
		/*CASE INTRO_CUT_SHOT1						RETURN 5533				BREAK
		CASE INTRO_CUT_CITY_SHOT_1					RETURN 5333				BREAK
		CASE INTRO_CUT_CITY_SHOT_2					RETURN 5233				BREAK
		CASE INTRO_CUT_RACE_1						RETURN 7767				BREAK
		CASE INTRO_CUT_RACE_2						RETURN 4300				BREAK
		CASE INTRO_CUT_RACE_3						RETURN 1800				BREAK
		CASE INTRO_CUT_RACE_4						RETURN 1800				BREAK
		CASE INTRO_CUT_RACE_5						RETURN 3367				BREAK
		CASE INTRO_CUT_RACE_6						RETURN 9200				BREAK
		CASE INTRO_CUT_GARAGE_2_START				RETURN 2933				BREAK
		CASE INTRO_CUT_GARAGE_2						RETURN 2200				BREAK
		CASE INTRO_CUT_GARAGE_4						RETURN 2967				BREAK
		
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_01			RETURN 6200				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01		RETURN 2300				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_03			RETURN 3933				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02		RETURN 8467				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_05			RETURN 4000				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_07			RETURN 7600				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03		RETURN 4700				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_10			RETURN 6167				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_12			RETURN 5466				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_14			RETURN 6233				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_17			RETURN 4800				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_19			RETURN 3667				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_20			RETURN 2433				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_21			RETURN 4539				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04		RETURN 2900				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_24			RETURN 3800				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_25			RETURN 9285				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_26			RETURN 2014				BREAK
		CASE INTRO_CUT_FINISH						RETURN 0				BREAK*/
		
		//new srl
		CASE INTRO_CUT_SHOT1						RETURN 5239				BREAK
		CASE INTRO_CUT_CITY_SHOT_1					RETURN 5314				BREAK
		CASE INTRO_CUT_CITY_SHOT_2					RETURN 5274				BREAK
		CASE INTRO_CUT_RACE_1						RETURN 7827				BREAK
		CASE INTRO_CUT_RACE_2						RETURN 2661				BREAK
		CASE INTRO_CUT_RACE_3						RETURN 2669				BREAK
		CASE INTRO_CUT_RACE_4						RETURN 2491				BREAK
		CASE INTRO_CUT_RACE_5						RETURN 3453				BREAK
		CASE INTRO_CUT_RACE_6						RETURN 11245			BREAK
		CASE INTRO_CUT_GARAGE_2_START				RETURN 2557				BREAK
		CASE INTRO_CUT_GARAGE_2						RETURN 2059				BREAK
		CASE INTRO_CUT_GARAGE_4						RETURN 1746				BREAK
		
		/*CASE INTRO_CUT_SHOT1						RETURN 1000				BREAK
		CASE INTRO_CUT_CITY_SHOT_1					RETURN 1000				BREAK
		CASE INTRO_CUT_CITY_SHOT_2					RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_1						RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_2						RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_3						RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_4						RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_5						RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_6						RETURN 1000				BREAK
		CASE INTRO_CUT_GARAGE_2_START				RETURN 1000				BREAK
		CASE INTRO_CUT_GARAGE_2						RETURN 1000				BREAK
		CASE INTRO_CUT_GARAGE_4						RETURN 1000				BREAK*/
		
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_01			RETURN 6200				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01		RETURN 2300				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_03			RETURN 3933				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02		RETURN 8467				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_05			RETURN 4000				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_07			RETURN 7600				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03		RETURN 4700				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_10			RETURN 5421				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_12			RETURN 4487				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_14			RETURN 4578				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_17			RETURN 4664				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_19			RETURN 2247				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_20			RETURN 2336				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_21			RETURN 3521				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04		RETURN 2510				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_24			RETURN 3455				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_25			RETURN 9318				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_26			RETURN 2014				BREAK
		CASE INTRO_CUT_FINISH						RETURN 0				BREAK
		
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the length of a shot in the cutscene
/// PARAMS:
///    thisStage - shot to get the length of
/// RETURNS:
///    length as INT in ms
FUNC INT GET_CUTSCENE_SHOT_LENGTH(INTRO_CUT_STAGE thisStage)
	
	SWITCH thisStage
		CASE INTRO_CUT_SHOT1						RETURN 5239				BREAK
		CASE INTRO_CUT_CITY_SHOT_1					RETURN 5314				BREAK
		CASE INTRO_CUT_CITY_SHOT_2					RETURN 5274				BREAK
		CASE INTRO_CUT_RACE_1						RETURN 7827				BREAK
		CASE INTRO_CUT_RACE_2						RETURN 2661				BREAK
		CASE INTRO_CUT_RACE_3						RETURN 2669				BREAK
		CASE INTRO_CUT_RACE_4						RETURN 2491				BREAK
		CASE INTRO_CUT_RACE_5						RETURN 3453				BREAK
		CASE INTRO_CUT_RACE_6						RETURN 11245			BREAK
		CASE INTRO_CUT_GARAGE_2_START				RETURN 2557				BREAK
		CASE INTRO_CUT_GARAGE_2						RETURN 2059				BREAK
		CASE INTRO_CUT_GARAGE_4						RETURN 1746				BREAK
		
		/*CASE INTRO_CUT_SHOT1						RETURN 1000				BREAK
		CASE INTRO_CUT_CITY_SHOT_1					RETURN 1000				BREAK
		CASE INTRO_CUT_CITY_SHOT_2					RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_1						RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_2						RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_3						RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_4						RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_5						RETURN 1000				BREAK
		CASE INTRO_CUT_RACE_6						RETURN 1000				BREAK
		CASE INTRO_CUT_GARAGE_2_START				RETURN 1000				BREAK
		CASE INTRO_CUT_GARAGE_2						RETURN 1000				BREAK
		CASE INTRO_CUT_GARAGE_4						RETURN 1000				BREAK*/
		
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_01			RETURN 6200				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01		RETURN 2300				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_03			RETURN 3933				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02		RETURN 8467				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_05			RETURN 4000				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_07			RETURN 7600				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03		RETURN 4700				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_10			RETURN 5421				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_12			RETURN 4487				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_14			RETURN 4578				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_17			RETURN 4664				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_19			RETURN 2247				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_20			RETURN 2336				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_21			RETURN 3521				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04		RETURN 2510				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_24			RETURN 3455				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_25			RETURN 9318				BREAK
		CASE INTRO_CUT_LAMAR_CAR_DRIVE_26			RETURN 2014				BREAK
		CASE INTRO_CUT_FINISH						RETURN 0				BREAK
		
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the time of the cutscene that the specified stage should end
/// PARAMS:
///    FMCutData - 
///    thisStage - stage to get end time for
///    bSRL - get end time in the SRL timeline, rather than the cutscene timeline
/// RETURNS:
///    end time as INT in ms
FUNC INT GET_CUTSCENE_TIME_FOR_STAGE(FM_CUT_STRUCT &FMCutData, INTRO_CUT_STAGE thisStage, BOOL bSRL = FALSE)
	INT i
	INT iTime
	
	INTRO_CUT_STAGE startStage
	INTRO_CUT_STAGE endStage
	INTRO_CUT_STAGE loopStage
	
	IF thisStage < INTRO_CUT_MOCAP_PLAYING
		iTime = 0
		startStage = INTRO_CUT_SHOT1
		endStage = INTRO_CUT_GARAGE_4
	ELSE
		IF bSRL
			iTime = FMCutData.iEndOfMocapSRLAdjust
		ELSE
			iTime = FMCutData.iEndOfMocapTime
		ENDIF
		startStage = INTRO_CUT_LAMAR_CAR_DRIVE_01
		endStage = INTRO_CUT_FINISH
	ENDIF
	
	REPEAT ((ENUM_TO_INT(endStage) - ENUM_TO_INT(startStage))+1) i
		loopStage = startStage + INT_TO_ENUM(INTRO_CUT_STAGE, i)
		IF bSRL
			iTime += GET_SRL_SHOT_LENGTH(loopStage)
		ELSE
			iTime += GET_CUTSCENE_SHOT_LENGTH(loopStage)
		ENDIF
		IF thisStage = loopStage
			RETURN iTime 
		ENDIF
	ENDREPEAT
	
	RETURN iTime
ENDFUNC

/// PURPOSE:
///    Gets the progress through the current cutscene shot as a phase
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    FLOAT phase of the progress through the current cutscene shot (between 0.0 to 1.0)
FUNC FLOAT GET_CURRENT_CUTSCENE_SHOT_PHASE(FM_CUT_STRUCT &FMCutData)
	INT iShotStart = GET_CUTSCENE_TIME_FOR_STAGE(FMCutData, FMCutData.introCutStage) - GET_CUTSCENE_SHOT_LENGTH(FMCutData.introCutStage)
	INT iCurrentTime = ROUND(FMCutData.fLastSRLTimePassed)
	
	RETURN TO_FLOAT(iCurrentTime - iShotStart) / TO_FLOAT(GET_CUTSCENE_SHOT_LENGTH(FMCutData.introCutStage))
	
ENDFUNC

CONST_INT FEMALE_MOCAP_START 	134735
CONST_INT FEMALE_MOCAP_END		187368
CONST_INT FEMALE_MOCAP_LENGTH 	FEMALE_MOCAP_END - FEMALE_MOCAP_START
CONST_INT MALE_MOCAP_START		83986
CONST_INT MALE_MOCAP_END	 	134701
CONST_INT MALE_MOCAP_LENGTH 	MALE_MOCAP_END - MALE_MOCAP_START

/// PURPOSE:
///    Due to the SRL recording being out of date to the final version of the cutscene (as the recording was locked on the disc and could not be patched), this function was made to get a modified version to be passed into the SRL system, but it now returns the actual SRL time
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    SRL time as a Float
FUNC FLOAT GET_CURRENT_SRL_ADJUST(FM_CUT_STRUCT &FMCutData)
	//INT iShotStart = GET_CUTSCENE_TIME_FOR_STAGE(FMCutData, FMCutData.introCutStage, TRUE) - GET_SRL_SHOT_LENGTH(FMCutData.introCutStage)
	
	FLOAT fMocapValue
	FLOAT fPhase
	
	IF FMCutData.introCutStage >= INTRO_CUT_MOCAP_PLAYING
	AND FMCutData.introCutStage < INTRO_CUT_LAMAR_CAR_DRIVE_01
		
		IF IS_INTRO_FEMALE(FMCutData)
			fMocapValue = TO_FLOAT(FMCutData.iStartOfMocapSRLAdjust + GET_CUTSCENE_TIME()) //Adjust for cutscene
			
			IF GET_CUTSCENE_CONCAT_SECTION_PLAYING() > 2
				
				fPhase = (fMocapValue - FEMALE_MOCAP_START) / TO_FLOAT(FEMALE_MOCAP_LENGTH)
				
				fMocapValue = MALE_MOCAP_START + (fPhase * MALE_MOCAP_LENGTH)
				
			ENDIF
			
		ELSE
			fMocapValue = TO_FLOAT(FMCutData.iStartOfMocapSRLAdjust + GET_CUTSCENE_TIME()) //Adjust for cutscene
		ENDIF
		
		RETURN fMocapValue
	ENDIF
	
	//RETURN (GET_CURRENT_CUTSCENE_SHOT_PHASE(FMCutData) * TO_FLOAT(GET_SRL_SHOT_LENGTH(FMCutData.introCutStage))) + iShotStart
	RETURN FMCutData.fLastSRLTimePassed
ENDFUNC

/// PURPOSE:
///    Stores the curret game time as the cutscene's start time
/// PARAMS:
///    FMCutData - 
PROC RESET_CUTSCENE_TIME(FM_CUT_STRUCT &FMCutData)
	FMCutData.iCutStartTime = GET_GAME_TIMER()
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Debug: Resets the player as jskipping, ready to jskip again
/// PARAMS:
///    FMCutData - 
PROC RESET_JSKIP(FM_CUT_STRUCT &FMCutData)
	IF IS_BIT_SET(FMCutData.iBitset, biSkipping)
		CLEAR_BIT(FMCutData.iBitset, biSkipping)
		PRINT_INTRO_STRING("CLEAR_BIT(FMCutData.iBitset, biSkipping)")
	ENDIF
	FMCutData.iCutsceneTimeUpCount = 0
ENDPROC
#ENDIF

/// PURPOSE:
///    Returns the current progress time of the cutscene
/// PARAMS:
///    FMCutData - 
/// RETURNS:
///    current cutscene time as an INT
FUNC INT GET_ASSET_TIMER(FM_CUT_STRUCT &FMCutData)
	IF FMCutData.introCutStage < INTRO_CUT_MOCAP_PLAYING				//If before the mocap
		RETURN GET_MUSIC_PLAYTIME()										//return time through the curret music (so cuts always hit the beat)
	ELIF FMCutData.introCutStage < INTRO_CUT_LAMAR_CAR_DRIVE_01			//If during the mocap cutscene
		RETURN (FMCutData.iStartOfMocapTime + GET_CUTSCENE_TIME())		//Return the start of the mocap time, plus current mocap time
	ELSE																//If beyond the mocap cutscene
		RETURN (FMCutData.iEndOfMocapTime + GET_MUSIC_PLAYTIME())		//Return the end of the mocap plus the current music time
	ENDIF
ENDFUNC

/// PURPOSE:
///    Checks if the current shot has ended and needs to move on
/// PARAMS:
///    FMCutData - 
///    thisStage - stage to check
///    iModifier - modifier to default shot length
/// RETURNS:
///    True when the supplied shot is complete
FUNC BOOL IS_CUTSCENE_TIME_UP(FM_CUT_STRUCT &FMCutData, INTRO_CUT_STAGE thisStage, INT iModifier = 0)
	INT iTime = GET_CUTSCENE_TIME_FOR_STAGE(FMCutData, thisStage)
	
	IF FMCutData.bRecordOn
		IF (FMCutData.iCutStartTime + iTime + iModifier) <= GET_GAME_TIMER()		//when recording
			RETURN TRUE
		ENDIF
	ELSE
		IF (iTime + iModifier) <= GET_ASSET_TIMER(FMCutData)						//actual playback, check for debug skip
		#IF IS_DEBUG_BUILD
		OR IS_BIT_SET(FMCutData.iBitset, biSkipping)
		
			IF IS_BIT_SET(FMCutData.iBitset, biSkipping)
				//FMCutData.iCutStartTime = GET_ASSET_TIMER(FMCutData) - (iTime + iModifier)
				FMCutData.iCutStartTime = GET_GAME_TIMER() - (iTime + iModifier)
				FMCutData.bRecordOn = TRUE		//move onto "recording" timekeeping method as we've skipped
				CLEAR_BIT(FMCutData.iBitset, biSkipping)
			ENDIF
		
		#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Turns on the flag for the SRL progress to stall if a shot reaches it's end but the next resources haven't loaded in time. Allows the cutscene to lurch a bit in poor streaming but repair itself next shot
/// PARAMS:
///    FMCutData - 
///    thisStage - stage to check
PROC MANAGE_CUTSCENE_TIME_DELAY(FM_CUT_STRUCT &FMCutData, INTRO_CUT_STAGE thisStage)
	IF IS_CUTSCENE_TIME_UP(FMCutData, thisStage)
		IF FMCutData.bRecordOn
			FMCutData.iCutStartTime = GET_GAME_TIMER() - GET_CUTSCENE_TIME_FOR_STAGE(FMCutData, thisStage)
		ELSE
			FMCutData.bStallSRLTimerUpdate = TRUE
		ENDIF
	
		#IF IS_DEBUG_BUILD
		FMCutData.iCutsceneTimeUpCount++
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === IS_CUTSCENE_TIME_UP(", GET_INTRO_CUT_STAGE_NAME(thisStage), 
									") = TRUE, iCutsceneTimeUpCount = ", FMCutData.iCutsceneTimeUpCount)
		/*IF FMCutData.bRecordOn
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === BUG BUG BUG IS_CUTSCENE_TIME_UP ", GET_INTRO_CUT_STAGE_NAME(thisStage), 
									" : ", FMCutData.fLastSRLTimePassed)
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === BUG BUG BUG IS_CUTSCENE_TIME_UP ", GET_INTRO_CUT_STAGE_NAME(thisStage), 
									" : ", FMCutData.fLastSRLTimeAdjust)
		ENDIF*/
									
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads the mocap and sets up the entities involved to prepare for the lamar pickup cutscene to begin
/// PARAMS:
///    FMCutData - 
PROC LOAD_AND_SETUP_MOCAP(FM_CUT_STRUCT &FMCutData)
	IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiMocapLoaded)
		
		PRELOAD_CLOUD_HAT("CONTRAILS")
		
		IF IS_INTRO_FEMALE(FMCutData)
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("MP_INTRO_CONCAT", CS_SECTION_1|CS_SECTION_2|CS_SECTION_3|CS_SECTION_6|CS_SECTION_7)
		ELSE
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("MP_INTRO_CONCAT", CS_SECTION_1|CS_SECTION_2|CS_SECTION_3|CS_SECTION_4|CS_SECTION_5)
		ENDIF
		
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()")
			#ENDIF
			IF FMCutData.bRegPlayerOn
				IF IS_INTRO_FEMALE(FMCutData)
					SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_Female_Character", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
					//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MP_Female_Character", PLAYER_PED_ID())
				ELSE
					SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_Male_Character", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
					//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MP_Male_Character", PLAYER_PED_ID())
				ENDIF
			ENDIF
			SET_CUTSCENE_COMPONENT_VARIATION_FOR_JET_POPULATION()
		ENDIF
		
		IF HAS_CUTSCENE_LOADED()
		
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === SET_BIT(FMCutData.iLoadBitset, loadBiMocapLoaded)")
			#ENDIF
			SET_BIT(FMCutData.iLoadBitset, loadBiMocapLoaded)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Requests and creates the cars and mechanic ped for the garage scene
/// PARAMS:
///    FMCutData - 
PROC SETUP_GARAGE_ENTITIES(FM_CUT_STRUCT &FMCutData)
	
	IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiCreatedCars)
		REQUEST_INTRO_GARAGE_MODELS()
		REQUEST_MECHANIC_PED()
		IF CREATE_GARAGE_CARS(FMCutData)
			IF CREATE_MECHANIC_PED(FMCutData)
			
				IF NOT DOES_CAM_EXIST(FMCutData.cutsceneCam)
					FMCutData.cutsceneCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)		//pre create camera inside garage
				ENDIF
				SET_CAM_PARAMS(	FMCutData.cutsceneCam,
								<<228.1892, -1006.7584, -96.8311>>, <<-13.0682, 0.0572, 0.7306>>, 
								40.0330)
				
				INT i
				REPEAT NUMBER_OF_GARAGE_VEHICLES i
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_INTRO_GARAGE_MODEL(i))
				ENDREPEAT
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_INTRO_MECHANIC_MODEL())
				
				SET_BIT(FMCutData.iLoadBitset, loadBiCreatedCars)
				#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Set biCreatedCar") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Draws the light inside lamar's car during the up-close car shots
/// PARAMS:
///    FMCutData - 
PROC MANAGE_CAR_LIGHT(FM_CUT_STRUCT &FMCutData)
	IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_01
	AND FMCutData.introCutStage < INTRO_CUT_DRIVE_END_MOCAP_START
		IF DOES_ENTITY_EXIST(FMCutData.vehLamar)
			IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
				DRAW_LIGHT_WITH_RANGE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(FMCutData.vehLamar, <<0.0, 0.25, 0.6>>), 255, 255, 255, 1.5, 0.5)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains drawing the lights in the tennis scene required to cover up some flickering left in the on-disk mocap
/// PARAMS:
///    FMCutData - 
PROC MANAGE_TENNIS_LIGHTS(FM_CUT_STRUCT &FMCutData)
	IF IS_BIT_SET(FMCutData.iBitset, biDrawTennisLights)
		
		DRAW_SPOT_LIGHT(<<-1160.9423828125, -1657.68591308594, 5.12962102890015>>, 
						<<0.891254603862762, -0.398292809724808, -0.216859892010689>>, 
						226, 130, 78, 
						3.99999904632568, 6.59999990463257, 1.0, 30.0, 1.0)
						
		DRAW_SPOT_LIGHT(<<-1157.32360839844, -1659.87841796875, 5.44926404953003>>, 
						<<-0.840350747108459, 0.398467868566513, -0.367469877004623>>, 
						226, 130, 78, 
						3.90000200271606, 8.80000019073486, 1.0, 30.0, 1.0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Forcibly loads a piece of road that was commonly not streaming in on time on the HDD build
/// PARAMS:
///    FMCutData - 
PROC MAINTAIN_ROAD_MODEL_REQUEST(FM_CUT_STRUCT &FMCutData)
	IF NOT FMCutData.bLoadedRoadModel
		INT iHash
		MODEL_NAMES tempModel
		iHash = GET_HASH_KEY("ss1_rd1_03")
		tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
		IF IS_MODEL_IN_CDIMAGE(tempModel)
			REQUEST_LOAD_MODEL(tempModel)//1633396
			#IF IS_DEBUG_BUILD
			PRINTLN("CDM- Intro forcably requesting road for intro cutscene")
			#ENDIF
			FMCutData.bLoadedRoadModel = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: List of intro credit IDs
ENUM MP_INTRO_CREDITS
	MP_INTRO_CREDIT_ART_DIRECTOR=0,
	MP_INTRO_CREDIT_TECHNICAL_DIRECTOR,
	MP_INTRO_CREDIT_ASSOCIATE_ART_DIRECTORS,
	MP_INTRO_CREDIT_ASSOCIATE_TECHNICAL_DIRECTORS,
	MP_INTRO_CREDIT_LEAD_PROGRAMMERS,
	MP_INTRO_CREDIT_SENIOR_PROGRAMMERS,
	MP_INTRO_CREDIT_PROGRAMMERS,
	MP_INTRO_CREDIT_SOCIAL_CLUB,
	MP_INTRO_CREDIT_AUDIO,
	MP_INTRO_CREDIT_SCRIPT_LEADS,
	MP_INTRO_CREDIT_SCRIPTERS1,
	MP_INTRO_CREDIT_SCRIPTERS2,
	MP_INTRO_CREDIT_VISUAL_DESIGN,
	MP_INTRO_CREDIT_UI_DESIGN_AND_DEVELOPMENT,
	MP_INTRO_CREDIT_CONTENT_CREATION,
	MP_INTRO_CREDIT_DEVELOPMENT_ASSISTANTS,
	MP_INTRO_CREDIT_QA_SUPERVISOR_AND_LEAD_TEST_ANALYST,
	MP_INTRO_CREDIT_SENIOR_TEST_ANALYSTS,
	MP_INTRO_CREDIT_ASSISTANT_PRODUCER,
	MP_INTRO_CREDIT_ANIMATION_DIRECTOR,
	MP_INTRO_CREDIT_PRODUCER,
	MP_INTRO_CREDIT_VICE_PRESIDENT_OF_CREATIVE,
	MP_INTRO_CREDIT_EXECUTIVE_PRODUCER,
	
	NUMBER_OF_MP_INTRO_CREDITS
ENDENUM

CONST_FLOAT MPINTRO_WIPE_IN 		0.3//0.33
CONST_FLOAT MPINTRO_WIPE_OUT 		0.3//0.33
CONST_FLOAT MPINTRO_WIPE_GAP		0.16//0.2

CONST_FLOAT MPINTRO_INSTANT_IN 		0.0
CONST_FLOAT MPINTRO_INSTANT_OUT 	0.0
CONST_FLOAT MPINTRO_INSTANT_GAP		0.0

/// PURPOSE:
///    Manages setup and cleanup of intro credit names based on progress through the cutscene
/// PARAMS:
///    FMCutData - 
PROC MANAGE_MP_INTRO_CREDITS(FM_CUT_STRUCT &FMCutData)
	IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiCredits)
		/*IF FMCutData.introCutStage > INTRO_CUT_MOCAP_CONFIRM_PLAYING
			//g_sfMPCredits = REQUEST_SCALEFORM_MOVIE("OPENING_CREDITS")
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === SET_BIT(FMCutData.iLoadBitset, loadBiCredits)")
			#ENDIF*/
			
			
			
			SET_BIT(FMCutData.iLoadBitset, loadBiCredits)
		//ENDIF
	ELSE
		
		IF FMCutData.introCutStage > INTRO_CUT_MOCAP_CONFIRM_PLAYING
			IF NOT g_bMPCreditsOn
				g_bMPCreditsOn = TRUE
				g_bMPCreditsIntroDraw = TRUE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === g_bMPCreditsOn = TRUE")
				#ENDIF
			ENDIF
		ENDIF
	
		IF FMCutData.introCutStage > INTRO_CUT_MOCAP_PLAYING
			IF HAS_SCALEFORM_MOVIE_LOADED(g_sfMPCredits)
			
				IF FMCutData.iCurrentCredit < ENUM_TO_INT(NUMBER_OF_MP_INTRO_CREDITS)
					SWITCH INT_TO_ENUM(MP_INTRO_CREDITS, FMCutData.iCurrentCredit)
						
						CASE MP_INTRO_CREDIT_ART_DIRECTOR
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_01, -6780)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_01
									
									IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
										SCALEFORM_SETUP_CREDIT_BLOCK("artdir", 0.000, 420.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									ELSE
										SCALEFORM_SETUP_CREDIT_BLOCK("artdir", 0.000, 450.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									ENDIF
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("artdir", "Art Director", 175.000, "HUD_COLOUR_FRIENDLY")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("artdir", "AARON GARBUT", 210.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("artdir", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit artdir")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_01, -3390)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("artdir", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit artdir")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_TECHNICAL_DIRECTOR
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_01, -2940)
									
									SCALEFORM_SETUP_CREDIT_BLOCK("techdir", 0.000, 30.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("techdir", "Technical Director", 100.000, "HUD_COLOUR_NET_PLAYER1")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("techdir", "ADAM FOWLER", 185.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("techdir", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit techdir")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_01, -450)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("techdir", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit techdir")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_ASSOCIATE_ART_DIRECTORS
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_03, -3090)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_03
									
									SCALEFORM_SETUP_CREDIT_BLOCK("assartdir", 0.000, 100.000, "left", MPINTRO_WIPE_IN, MPINTRO_INSTANT_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("assartdir", "Associate Art Directors", 20.000, "HUD_COLOUR_NET_PLAYER2")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("assartdir", "ADAM COCHRANE|MICHAEL KANE", 100.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("assartdir", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit assartdir")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_03, -960)
								IF FMCutData.introCutStage > INTRO_CUT_LAMAR_CAR_DRIVE_03
									SCALEFORM_HIDE_CREDIT_BLOCK("assartdir", MPINTRO_INSTANT_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit assartdir")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_ASSOCIATE_TECHNICAL_DIRECTORS
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02, -3090)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02
									
									IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
										SCALEFORM_SETUP_CREDIT_BLOCK("asstecdir", 0.000, 370.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									ELSE
										SCALEFORM_SETUP_CREDIT_BLOCK("asstecdir", 0.000, 400.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									ENDIF
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("asstecdir", "Associate Technical Directors", 0.000, "HUD_COLOUR_NET_PLAYER3")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("asstecdir", "KLAAS SCHILSTRA|PHIL HOOKER", 150.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("asstecdir", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit asstecdir")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02, -4740)//-4440)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("asstecdir", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit asstecdir")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_LEAD_PROGRAMMERS
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02, -3810)
									
									SCALEFORM_SETUP_CREDIT_BLOCK("leadprog", 30.000, 20.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("leadprog", "Lead Programmers", 0.000, "HUD_COLOUR_FRIENDLY")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("leadprog", "DANIEL YELLAND|ROBERT TRICKEY", 145.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("leadprog", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit leadprog")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02, -600)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("leadprog", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit leadprog")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_SENIOR_PROGRAMMERS
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_05, -3030)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_05
									
									IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
										SCALEFORM_SETUP_CREDIT_BLOCK("senprog", 0.000, 200.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									ELSE
										SCALEFORM_SETUP_CREDIT_BLOCK("senprog", 0.000, 225.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									ENDIF
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("senprog", "Senior Programmers", 20.000, "HUD_COLOUR_NET_PLAYER1")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("senprog", "JOHN HYND|JOHN GURNEY|DAN CONTI|ROSS CHILDS|STEPHEN LaVALLEY", 75.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("senprog", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit senprog")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_05, -1350)//-1050)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("senprog", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit senprog")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_PROGRAMMERS
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_07, -2940)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_07
									
									SCALEFORM_SETUP_CREDIT_BLOCK("prog", 0.000, 100.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("prog", "Programmers", 35.000, "HUD_COLOUR_NET_PLAYER2")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("prog", "MIGUEL FREITAS|STUART ROSKELL|ALLAN WALTON|PAU AGUILAR FRUTO", 100.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("prog", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit prog")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_07, -4320)//-4020)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("prog", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit prog")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_SOCIAL_CLUB
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_07, -3240)
									
									IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
									OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
										SCALEFORM_SETUP_CREDIT_BLOCK("socclub", 40.000, 305.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									ELSE
										SCALEFORM_SETUP_CREDIT_BLOCK("socclub", 40.000, 325.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									ENDIF
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("socclub", "Social Club", 0.000, "HUD_COLOUR_NET_PLAYER3")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("socclub", "IAN McFARLAND|KEVIN BACA", 40.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("socclub", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit socclub")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_07, -870)//-570)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("socclub", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit socclub")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_AUDIO
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03, -3960)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03
									
									SCALEFORM_SETUP_CREDIT_BLOCK("audio", 0.000, 10.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("audio", "Audio", 0.000, "HUD_COLOUR_FRIENDLY")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("audio", "MATTHEW SMITH|CRAIG CONNER|ALASTAIR MacGREGOR|JEFFREY WHITCHER", 35.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("audio", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit audio")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03, -1620)//-1020)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("audio", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit audio")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_SCRIPT_LEADS
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_10, -3150)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_10
									
									SCALEFORM_SETUP_CREDIT_BLOCK("scrlead", 50.000, 85.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("scrlead", "Script Leads", 0.000, "HUD_COLOUR_NET_PLAYER1")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("scrlead", "BOBBY WRIGHT|KEITH McLEMAN|BRENDA CAREY|ROWAN COCKCROFT|NEIL FERGUSON|RYAN BAKER", 60.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("scrlead", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit scrlead")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_10, -1470)//-1170)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("scrlead", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit scrlead")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_SCRIPTERS1
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_12, -2970)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_12
									
									SCALEFORM_SETUP_CREDIT_BLOCK("script1", 0.000, 10.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("script1", "Scripters", 20.000, "HUD_COLOUR_NET_PLAYER2")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("script1", "CONOR McGUIRE|JAMES ADWICK|DAVID WATSON|NEIL BEGGS|WILLIAM KENNEDY", 80.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("script1", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit script1")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_12, -1200)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("script1", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit script1")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_SCRIPTERS2
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_14, -2070)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_14
									
									SCALEFORM_SETUP_CREDIT_BLOCK("script2", 0.000, 10.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("script2", "Scripters", 20.000, "HUD_COLOUR_NET_PLAYER3")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("script2", "DAVID GENTLES|BEN HINCHLIFFE|KEVIN WONG|CHRISTOPHER SPEIRS|CHRIS McMAHON", 80.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("script2", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit script2")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_14, -1320)//-1020)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("script2", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit script2")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_VISUAL_DESIGN
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_17, -3090)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_17
									
									SCALEFORM_SETUP_CREDIT_BLOCK("visdes", 0.000, 45.000, "left", 0.2, 0.2)//MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("visdes", "Visual Design", 30.000, "HUD_COLOUR_FRIENDLY")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("visdes", "THOMAS DIAKOMICHALIS|KARMEN COKER|LUKE HOWARD| | |", 67.000, "|")
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("visdes", "Character Creator", 100.000, "HUD_COLOUR_FRIENDLY")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("visdes", "STUART HANSELL|STEWART WRIGHT|FLAVIUS ALECU", 147.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("visdes", 0.05)//MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit visdes")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_17, -1320)//-1020)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("visdes", 0.05)//MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit visdes")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_UI_DESIGN_AND_DEVELOPMENT
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_19, -2850)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_19
									
									SCALEFORM_SETUP_CREDIT_BLOCK("uides", 40.000, 20.000, "right", MPINTRO_INSTANT_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("uides", "UI Design & Development", 0.000, "HUD_COLOUR_NET_PLAYER1")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("uides", "STUART PETRI|STEVE WALSH|GARETH EVANS|JEFFREY KHAN|CHRIS EDWARDS", 110.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("uides", MPINTRO_INSTANT_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit uides")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_20, -1650)//-1350)//-1050)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("uides", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit uides")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_CONTENT_CREATION
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_21, -7740)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_21
									
									SCALEFORM_SETUP_CREDIT_BLOCK("concre", 0.000, 20.000, "left", MPINTRO_INSTANT_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("concre", "Content Creation", 40.000, "HUD_COLOUR_NET_PLAYER2")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("concre", "CALLUM MUNGALL|CHRIS McCALLUM|JESSE HOLCROFT|JOE STAFFORD", 80.000, "|")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("concre", "JOSHUA MATTYASOVSZKY|MARTIN LOGAN|STEFAN WEBSTER", 80.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("concre", MPINTRO_INSTANT_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit concre")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_21, -1950)//-1650)//-1350)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("concre", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit concre")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_DEVELOPMENT_ASSISTANTS
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04, -1830)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04
									
									SCALEFORM_SETUP_CREDIT_BLOCK("devass", 0.000, 400.000, "right", MPINTRO_INSTANT_IN, MPINTRO_INSTANT_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("devass", "Development Assistants", 0.000, "HUD_COLOUR_NET_PLAYER3")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("devass", "FIONN WRIGHT|KATIE PICA", 100.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("devass", MPINTRO_INSTANT_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit devass")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04, -1830)
								IF FMCutData.introCutStage > INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04
								
									SCALEFORM_HIDE_CREDIT_BLOCK("devass", MPINTRO_INSTANT_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit devass")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_QA_SUPERVISOR_AND_LEAD_TEST_ANALYST
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_24, -1830)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_24
									
									SCALEFORM_SETUP_CREDIT_BLOCK("qasup", 20.000, 0.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("qasup", "QA Supervisor", 0.000, "HUD_COLOUR_FRIENDLY")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("qasup", "ADAM WHITING| ", 50.000, "|")
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("qasup", "Lead Test Analyst", 50.000, "HUD_COLOUR_FRIENDLY")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("qasup", "CHRIS THOMSON", 160.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("qasup", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit qasup")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_24, -1320)//-1020)//-720)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("qasup", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit qasup")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_SENIOR_TEST_ANALYSTS
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_25, -1830)
								IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_25
									
									SCALEFORM_SETUP_CREDIT_BLOCK("senta", 0.000, 25.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("senta", "Senior Test Analysts", 30.000, "HUD_COLOUR_NET_PLAYER1")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("senta", "MICHAEL BURTON|ROSS PARKER|SEAN CASEY", 120.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("senta", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit senta")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_25, -5070)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("senta", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit senta")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_ASSISTANT_PRODUCER
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_25, -3750)
									
									SCALEFORM_SETUP_CREDIT_BLOCK("asspro", 20.000, 350.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
									SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("asspro", "Assistant Producer", 0.000, "HUD_COLOUR_NET_PLAYER2")
									SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("asspro", "JOSH NEEDLEMAN", 75.000, "|")
									SCALEFORM_SHOW_CREDIT_BLOCK("asspro", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit asspro")
									#ENDIF
									
									SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								ENDIF
							ELSE
								IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_25, -720)//-420)
									
									SCALEFORM_HIDE_CREDIT_BLOCK("asspro", MPINTRO_WIPE_GAP)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit asspro")
									#ENDIF
									
									FMCutData.iCurrentCredit++
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_ANIMATION_DIRECTOR
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								IF IS_INTRO_FEMALE(FMCutData)		//***Female***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 2468
										
										SCALEFORM_SETUP_CREDIT_BLOCK("animdir", 0.000, 95.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
										SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("animdir", "Animation Director", 110.000, "HUD_COLOUR_NET_PLAYER3")
										SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("animdir", "ROB NELSON", 200.000, "|")
										SCALEFORM_SHOW_CREDIT_BLOCK("animdir", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit animdir")
										#ENDIF
										
										SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
									ENDIF
								ELSE								//***Male***
									IF FMCutData.introCutStage >= INTRO_CUT_LAMAR_CAR_DRIVE_26
										
										SCALEFORM_SETUP_CREDIT_BLOCK("animdir", 50.000, 90.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
										SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("animdir", "Animation Director", 0.000, "HUD_COLOUR_NET_PLAYER3")
										SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("animdir", "ROB NELSON", 90.000, "|")
										SCALEFORM_SHOW_CREDIT_BLOCK("animdir", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit animdir")
										#ENDIF
										
										SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
									ENDIF
								ENDIF
							ELSE
								IF IS_INTRO_FEMALE(FMCutData)		//***Female***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 6123
										
										SCALEFORM_HIDE_CREDIT_BLOCK("animdir", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit animdir")
										#ENDIF
										
										FMCutData.iCurrentCredit++
									ENDIF
								ELSE								//***Male***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 1898
										
										SCALEFORM_HIDE_CREDIT_BLOCK("animdir", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit animdir")
										#ENDIF
										
										FMCutData.iCurrentCredit++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_PRODUCER
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								IF IS_INTRO_FEMALE(FMCutData)		//***Female***
									IF IS_CUTSCENE_PLAYING()
									//AND GET_CUTSCENE_TIME() > 7416
									AND HAS_CUTSCENE_CUT_THIS_FRAME()
										
										SCALEFORM_SETUP_CREDIT_BLOCK("producer", 65.000, 55.000, "right", MPINTRO_INSTANT_IN, MPINTRO_INSTANT_OUT)
										SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("producer", "Producer", 0.000, "HUD_COLOUR_FRIENDLY")
										SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("producer", "ANDY DUTHIE", 20.000, "|")
										SCALEFORM_SHOW_CREDIT_BLOCK("producer", MPINTRO_INSTANT_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit producer")
										#ENDIF
										
										SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
									ENDIF
								ELSE								//***Male***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 2452
										
										SCALEFORM_SETUP_CREDIT_BLOCK("producer", 0.000, 100.000, "left", MPINTRO_WIPE_IN, MPINTRO_INSTANT_OUT)
										SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("producer", "Producer", 80.000, "HUD_COLOUR_FRIENDLY")
										SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("producer", "ANDY DUTHIE", 110.000, "|")
										SCALEFORM_SHOW_CREDIT_BLOCK("producer", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit producer")
										#ENDIF
										
										SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
									ENDIF
								ENDIF
							ELSE
								IF IS_INTRO_FEMALE(FMCutData)		//***Female***
									IF IS_CUTSCENE_PLAYING()
									//AND GET_CUTSCENE_TIME() > 10967
									AND HAS_CUTSCENE_CUT_THIS_FRAME()
										
										SCALEFORM_HIDE_CREDIT_BLOCK("producer", MPINTRO_INSTANT_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit producer")
										#ENDIF
										
										FMCutData.iCurrentCredit++
									ENDIF
								ELSE								//***Male***
									IF IS_CUTSCENE_PLAYING()
									//AND GET_CUTSCENE_TIME() > 5668
									AND HAS_CUTSCENE_CUT_THIS_FRAME()
										
										SCALEFORM_HIDE_CREDIT_BLOCK("producer", MPINTRO_INSTANT_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit producer")
										#ENDIF
										
										FMCutData.iCurrentCredit++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_VICE_PRESIDENT_OF_CREATIVE
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								IF IS_INTRO_FEMALE(FMCutData)		//***Female***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 13265
										
										SCALEFORM_SETUP_CREDIT_BLOCK("vpcre", 65.000, 55.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
										SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("vpcre", "VP of Creative", 0.000, "HUD_COLOUR_NET_PLAYER1")
										SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("vpcre", "DAN HOUSER", 80.000, "|")
										SCALEFORM_SHOW_CREDIT_BLOCK("vpcre", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit vpcre")
										#ENDIF
										
										SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
									ENDIF
								ELSE								//***Male***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 6921
										
										SCALEFORM_SETUP_CREDIT_BLOCK("vpcre", 65.000, 80.000, "right", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
										SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("vpcre", "VP of Creative", 0.000, "HUD_COLOUR_NET_PLAYER1")
										SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("vpcre", "DAN HOUSER", 80.000, "|")
										SCALEFORM_SHOW_CREDIT_BLOCK("vpcre", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit vpcre")
										#ENDIF
										
										SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
									ENDIF
								ENDIF
							ELSE
								IF IS_INTRO_FEMALE(FMCutData)		//***Female***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 16955
										
										SCALEFORM_HIDE_CREDIT_BLOCK("vpcre", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit vpcre")
										#ENDIF
										
										FMCutData.iCurrentCredit++
									ENDIF
								ELSE								//***Male***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 10513
										
										SCALEFORM_HIDE_CREDIT_BLOCK("vpcre", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit vpcre")
										#ENDIF
										
										FMCutData.iCurrentCredit++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE MP_INTRO_CREDIT_EXECUTIVE_PRODUCER
							IF NOT IS_BIT_SET(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
								IF IS_INTRO_FEMALE(FMCutData)		//***Female***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 18077
										g_bMPCreditsIntroDraw = FALSE
										SCALEFORM_SETUP_CREDIT_BLOCK("execpro", 0.000, 375.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
										SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("execpro", "Executive Producer", 70.000, "HUD_COLOUR_NET_PLAYER2")
										SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("execpro", "SAM HOUSER", 165.000, "|")
										SCALEFORM_SHOW_CREDIT_BLOCK("execpro", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit execpro")
										#ENDIF
										
										SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
									ENDIF
								ELSE								//***Male***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 12358
										g_bMPCreditsIntroDraw = FALSE
										SCALEFORM_SETUP_CREDIT_BLOCK("execpro", 0.000, 340.000, "left", MPINTRO_WIPE_IN, MPINTRO_WIPE_OUT)
										SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("execpro", "Executive Producer", 100.000, "HUD_COLOUR_NET_PLAYER2")
										SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("execpro", "SAM HOUSER", 190.000, "|")
										SCALEFORM_SHOW_CREDIT_BLOCK("execpro", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Setup Credit execpro")
										#ENDIF
										
										SET_BIT(FMCutData.iCreditBitset, FMCutData.iCurrentCredit)
									ENDIF
								ENDIF
							ELSE
								IF IS_INTRO_FEMALE(FMCutData)		//***Female***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 21785
										
										SCALEFORM_HIDE_CREDIT_BLOCK("execpro", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit execpro")
										#ENDIF
										
										FMCutData.iCurrentCredit++
									ENDIF
								ELSE								//***Male***
									IF IS_CUTSCENE_PLAYING()
									AND GET_CUTSCENE_TIME() > 15333
										
										SCALEFORM_HIDE_CREDIT_BLOCK("execpro", MPINTRO_WIPE_GAP)
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Cleanup Credit execpro")
										#ENDIF
										
										FMCutData.iCurrentCredit++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
					ENDSWITCH
				ENDIF
				IF g_bMPCreditsIntroDraw
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(g_sfMPCredits, 255, 255, 255, 255)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls the MP Intro Cutscene
/// PARAMS:
///    FMCutData - 
///    iBitset - bitset for controlling the cutscene
/// RETURNS:
///    True when cutscene is complete
FUNC BOOL RUN_FREEMODE_INTRO_CUTSCENE(FM_CUT_STRUCT &FMCutData, INT &iBitset)

	FLOAT fSkip
	VECTOR vDaveTemp
	INT i
	#IF IS_DEBUG_BUILD
		IF g_ShouldShiftingTutorialsBeSkipped = TRUE
			RETURN TRUE
		ENDIF
		
		IF IS_SCREEN_FADED_IN()
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				IF FMCutData.introCutStage < INTRO_CUT_LAMAR_CAR_DRIVE_26
				AND FMCutData.introCutStage > INTRO_CUT_SHOT1_START4
					//SRL end the Streaming Request List
					IF FMCutData.bSRLOn
						END_SRL()
						CLEAR_BIT(fmcutdata.iBitset, biDoSRLTime)
					ENDIF
					FMCutData.introCutStage = INTRO_CUT_SKIP_START
				ENDIF
				
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				IF FMCutData.introCutStage < INTRO_CUT_LAMAR_CAR_DRIVE_26
				AND FMCutData.introCutStage > INTRO_CUT_SHOT1_START4
					//SRL end the Streaming Request List
					IF FMCutData.bSRLOn
						END_SRL()
						CLEAR_BIT(fmcutdata.iBitset, biDoSRLTime)
					ENDIF
					FMCutData.introCutStage = INTRO_CUT_SKIP_DAVE
				ENDIF
				
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				IF NOT IS_BIT_SET(FMCutData.iBitset, biSkipping)
					//SRL end the Streaming Request List
					IF FMCutData.bSRLOn
						END_SRL()
						CLEAR_BIT(fmcutdata.iBitset, biDoSRLTime)
					ENDIF
					SET_BIT(FMCutData.iBitset, biSkipping)
					PRINT_INTRO_STRING("SET_BIT(FMCutData.iBitset, biSkipping)")
				ENDIF
			ENDIF
		ENDIF
		
	#ENDIF
	
	//If not flagging otherwise, move the game focus to the camera's position
	IF NOT IS_BIT_SET(FMCutData.iBitset, biBlockSettingFocus)
		IF DOES_CAM_EXIST(GET_RENDERING_CAM())
			VECTOR vCamPos = GET_CAM_COORD(GET_RENDERING_CAM())
			IF vCamPos.x <> 0.0
			OR vCamPos.y <> 0.0
			OR vCamPos.z <> 0.0
				SET_FOCUS_POS_AND_VEL(vCamPos, <<0,0,0>>)
				IF NOT IS_BIT_SET(FMCutData.iBitset, biFocusSet)
					SET_BIT(FMCutData.iBitset, biFocusSet)
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("SET_BIT(FMCutData.iBitset, biFocusSet)") #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(FMCutData.iBitset, biFocusSet)
			CLEAR_FOCUS()
			CLEAR_BIT(FMCutData.iBitset, biFocusSet)
			#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("CLEAR_BIT(FMCutData.iBitset, biFocusSet)") #ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_AllowHeadPropInVehicle, TRUE)
	ENDIF
	
	MANAGE_CAR_LIGHT(FMCutData)
	
	OVERRIDE_LODSCALE_THIS_FRAME(1.0) //set LOD to disregard FOV
	
	MANAGE_MP_INTRO_CREDITS(FMCutData)
	
	MANAGE_PLAYER_FACE_ANIMATION(PLAYER_ID())
	
	IF NOT IS_BIT_SET(FMCutData.iBitset, biPinnedGarageInterior)
		IF FMCutData.introCutStage > INTRO_CUT_INIT
			FMCutData.interiorGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<234.0331, -1005.2032, -98.4626>>, "hei_dlc_garage_high_new")
			IF FMCutData.interiorGarage <> NULL
				PIN_INTERIOR_IN_MEMORY(FMCutData.interiorGarage)
				SET_BIT(FMCutData.iBitset, biPinnedGarageInterior)
				#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("PINNED GARAGE INTERIOR ") #ENDIF
			ELSE
				#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("TRYING TO FIND GARAGE INTERIOR! ") #ENDIF
			ENDIF
		ENDIF
	ENDIF
				
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
		
		SWITCH FMCutData.introCutStage
		
			// Fade out
			CASE INTRO_CUT_INIT
				
				#IF IS_DEBUG_BUILD	
				iWdScriptProgress = 1	//=========Script Progress===========
				#ENDIF
				
				FMCutData.bSRLOn = TRUE //turn on or off SRL
				
				FMCutData.bRecordOn = FALSE //turn on or off Using game timer
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_RecordMPIntroSRL")
					FMCutData.bRecordOn = TRUE
				ENDIF
				#ENDIF
				
				FMCutData.bRegPlayerOn = FALSE
				FMCutData.bPTFXOn = FALSE
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Intro")) > 0
					FMCutData.bRegPlayerOn = TRUE	//we're totally legit!
					FMCutData.bPTFXOn = TRUE
				ELIF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(FREEMODE_HASH()) > 0
					FMCutData.bRegPlayerOn = TRUE	//we're partially legit.
				ENDIF
				
				IF NOT MPGlobalsAmbience.bRunningFmIntroCut
					MPGlobalsAmbience.bRunningFmIntroCut = TRUE
					NETWORK_SET_VOICE_ACTIVE(FALSE)
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Called NETWORK_SET_VOICE_ACTIVE(FALSE)") #ENDIF
				ENDIF
				IF NOT IS_LOCAL_PLAYER_RUNNING_FM_INTRO_CUT()
					SET_LOCAL_PLAYER_RUNNING_FM_INTRO_CUT(TRUE)
				ENDIF
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				ENDIF
				IF NOT HAS_NET_TIMER_STARTED(FMCutData.timeWaitForSkycam)
					START_NET_TIMER(FMCutData.timeWaitForSkycam)
				ENDIF
				
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_sfMPCredits)
				g_bMPCreditsOn = FALSE
				g_bMPCreditsLoaded = FALSE
				g_bMPCreditsIntroDraw = FALSE
				g_bDirectorMPCreditCleared = FALSE
				
				FMCutData.vSkyCamPos = << 0.0, 0.0, 0.0>>
				IF IS_SCREEN_FADED_OUT()	

					#IF IS_DEBUG_BUILD	
					iWdScriptProgress = 2	//=========Script Progress===========
					#ENDIF

					#IF IS_DEBUG_BUILD

						IF HAS_NET_TIMER_EXPIRED(FMCutData.timeWaitForSkycam, 10000)
							PRINT_INTRO_STRING("CASE INTRO_CUT_INIT timeWaitForSkycam expired!")
						ENDIF
						
						IF NOT GET_SKYCAM_POSITIONING(FMCutData.vSkyCamPos, FMCutData.vSkyCamRot, FMCutData.fSkyCamFOV)
							PRINT_INTRO_STRING("CASE INTRO_CUT_INIT Failed to get skycam!")
						ELSE
							PRINT_INTRO_STRING("CASE INTRO_CUT_INIT Got skycam ok!")
						ENDIF
					#ENDIF
					
					FMCutData.iBitset = 0
					FMCutData.iCreditBitset = 0
					FMCutData.iCurrentCredit = 0
					
					MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, FALSE, TRUE, TRUE, TRUE)//, FALSE)
					SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE)
					HIDE_ALL_SHOP_BLIPS(TRUE)
					
					DISABLE_METRO_SYSTEM(TRUE)
					
					SET_OVERRIDE_WEATHER("EXTRASUNNY")
					//weather
					NETWORK_OVERRIDE_CLOCK_TIME(19, 00, 00)
					NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
					SWITCH_AMBIENT_PEDS_AND_VEHICLES(FALSE)
					
					SET_GAME_PAUSES_FOR_STREAMING(FALSE)
					
					TOGGLE_RENDERPHASES(TRUE)		//make sure the game is not frozen
					
					TAKE_CONTROL_OF_TRANSITION()
					
					//SRL start loading Streaming Request List
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Intro")) < 1	//if this is not a proper game
					OR NOT IS_TUTORIAL_CUTSCENE_GOING_TO_RUN()											//or we've not come from character creator
						IF FMCutData.bSRLOn
							IF IS_SRL_TUTORIAL_ACTIVE() = FALSE
								PREFETCH_SRL("GTAO_INTRO_MALE")
								SET_SRL_LONG_JUMP_MODE(TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEL, FALSE)
					
					FMCutData.introCutStage = INTRO_CUT_START
					
					#IF IS_DEBUG_BUILD
						PRINT_INTRO_STRING("MOVE TO CASE INTRO_CUT_START - GOT SKYCAM INFO")
					#ENDIF
					
					FMCutData.iBitset = 0
					CLEAR_BIT(FMCutData.iBitset, biCleanupDone)
					CLEAR_BIT(FMCutData.iBitset, biCutWasSkipped)
					CLEAR_BIT(FMCutData.iBitset, biIntroHelp)
					CLEAR_BIT(FMCutData.iBitset, biAddedToTutSession) 
					 
					SET_BIT(FMCutData.iBitset, biDoMusicCues)
					
					//-- front of airport
					FMCutData.scenBlockCut = ADD_SCENARIO_BLOCKING_AREA(<<-1012.7869, -2747.5974,15.0>>, <<-1052.0299, -2716.1438, 25.0>>)
					
				ENDIF
				
			BREAK
			
			
			// Warp player during fade, deal with hud
			CASE INTRO_CUT_START
				
				#IF IS_DEBUG_BUILD	
				iWdScriptProgress = 3	//=========Script Progress===========
				#ENDIF
				
				REQUEST_MODEL(FROGGER)
				IF HAS_MODEL_LOADED(FROGGER)
					IF NOT FMCutData.bSRLOn
					OR IS_SRL_LOADED()//SRL wait until Streaming Request List loaded
						#IF IS_DEBUG_BUILD	
						iWdScriptProgress = 4	//=========Script Progress===========
						#ENDIF
						IF SET_SKYBLUR_CLEAR()
							#IF IS_DEBUG_BUILD	
							iWdScriptProgress = 5	//=========Script Progress===========
							#ENDIF
							// These coords are also used for setting the skycam pos if GET_SKYCAM_POSITIONING return FALse in CASE INTRO_CUT_INIT
							
							SET_ALL_MAPDATA_CULLED(FALSE)
							
							START_MP_CUTSCENE(TRUE)
							
							DONT_RENDER_IN_GAME_UI(FALSE)
							NET_NL()NET_PRINT(" RUN_FREEMODE_INTRO_CUTSCENE: DONT_RENDER_IN_GAME_UI(FALSE) ")
							
							CLEAR_HELP()
								
							//Temp Fix for MP HUD Quit enabling the phone
								IF NOT Is_MP_Comms_Disabled()
									Disable_MP_Comms()
								ENDIF
								
							SET_DPADDOWN_ACTIVE(FALSE)
							
							SETUP_ROAD_NODE_BLOCKING()
							
							#IF IS_DEBUG_BUILD PRINT_INTRO_STRING ("Using g_vFreemodeCutStartPos...") #ENDIF
							SET_ENTITY_COORDS(PLAYER_PED_ID(),g_vFreemodeCutStartPos)
							SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
							SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
							SET_MAX_WANTED_LEVEL(0)
						
							IF DOES_BLIP_EXIST(GET_MAIN_PLAYER_BLIP_ID())
								SET_BLIP_SCALE(GET_MAIN_PLAYER_BLIP_ID(), 0)
							ENDIF
							
						//	REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
							
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
							
							FMCutData.introCutStage = INTRO_CUT_SHOT1_START
													
							#IF IS_DEBUG_BUILD
								PRINT_INTRO_STRING("MOVE TO CASE INTRO_CUT_SHOT1_START ")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE INTRO_CUT_SHOT1_START
				
				IF (NOT IS_PLAYER_SWITCH_IN_PROGRESS() )
					#IF IS_DEBUG_BUILD	
					iWdScriptProgress = 6	//=========Script Progress===========
					#ENDIF
					IF NOT IS_BIT_SET(FMCutData.iBitset, biStartedFirstScene)
						#IF IS_DEBUG_BUILD	
						iWdScriptProgress = 7	//=========Script Progress===========
						#ENDIF
						IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL
						OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_TERMINATE_MAINTRANSITION
							#IF IS_DEBUG_BUILD	
							iWdScriptProgress = 8	//=========Script Progress===========
							#ENDIF
							CLEAR_FOCUS()
							NEW_LOAD_SCENE_START(<<754.2219, 1226.8309, 356.5081>>,  CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<-14.3670, -0.0000, 157.3524>>), 100.0)
							RESET_NET_TIMER(FMCutData.iStreamVolTimer)
							START_NET_TIMER(FMCutData.iStreamVolTimer)	
							
							SET_BIT(FMCutData.iBitset, biStartedFirstScene)
							
							#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Started First load scene") #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								// BC: in logs shows transition debug
								PRINT_INTRO_STRING_INT("Case INTRO_CUT_SHOT1_START3 Waiting for GET_CURRENT_TRANSITION_STATE, current state... ", ENUM_TO_INT(GET_CURRENT_TRANSITION_STATE()))
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD	
						iWdScriptProgress = 9	//=========Script Progress===========
						#ENDIF
						IF PREPARE_MUSIC_EVENT("FM_INTRO_START")
						AND (IS_NEW_LOAD_SCENE_LOADED()
							OR HAS_NET_TIMER_EXPIRED(FMCutData.iStreamVolTimer, 5000))
							
							IF NOT IS_BIT_SET(FMCutData.iBitset, biMusicStarted)
								IF IS_BIT_SET(FMCutData.iBitset, biDoMusicCues)
									TRIGGER_MUSIC_EVENT("FM_INTRO_START")
									#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("[IntroCue] Playing cue FM_INTRO_START") #ENDIF
									SET_BIT(FMCutData.iBitset, biMusicStarted)
									SET_BIT(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
								ENDIF
							ELSE
								IF GET_MUSIC_PLAYTIME() > 0
									#IF IS_DEBUG_BUILD	
									iWdScriptProgress = 10	//=========Script Progress===========
									#ENDIF
									
								 	NEW_LOAD_SCENE_STOP()
									RESET_ADAPTATION() 	
									
									IF NOT DOES_CAM_EXIST(FMCutData.cutsceneCam)
										FMCutData.cutsceneCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
										SET_CAM_ACTIVE(FMCutData.cutsceneCam, TRUE)
									ENDIF
									
									SET_CAM_PARAMS(	FMCutData.cutsceneCam, 
													<<754.2219, 1226.8309, 356.5081>>, <<-14.3670, -0.0000, 157.3524>>, 
													42.2442)
									SET_CAM_PARAMS(	FMCutData.cutsceneCam,  
													<<740.7797, 1193.9226, 351.1997>>, <<-9.6114, 0.0000, 157.8659>>,
													44.8314, 6500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
													
									SHAKE_CAM(FMCutData.cutsceneCam, "HAND_SHAKE", 0.15)
									RENDER_SCRIPT_CAMS(TRUE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
									
									RESET_CUTSCENE_TIME(FMCutData)	//The only RESET_CUTSCENE_TIMER
									SET_BIT(fmcutdata.iBitset, biDoSRLTime)
									#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
									
									//SRL start the Streaming Request List
									IF FMCutData.bSRLOn
										SET_SRL_READAHEAD_TIMES(SRL_READAHEAD_NORMAL_PRESTREAM_MAP,
																SRL_READAHEAD_NORMAL_PRESTREAM_ASSETS,
																SRL_READAHEAD_NORMAL_PLAYBACK_MAP,
																SRL_READAHEAD_NORMAL_PLAYBACK_ASSETS)
										BEGIN_SRL()
									ENDIF
									
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
									
									IF IS_SCREEN_FADED_OUT()
										DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
									ENDIF
									
									FMCutData.introCutStage = INTRO_CUT_SHOT1 
									#IF IS_DEBUG_BUILD
										PRINT_INTRO_STRING("MOVE TO CASE INTRO_CUT_SHOT1 ")
									#ENDIF
								ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								PRINT_INTRO_STRING("INTRO_CUT_SHOT1_START3: Waiting for IS_NEW_LOAD_SCENE_LOADED")
							#ENDIF
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINT_INTRO_STRING("INTRO_CUT_SHOT1_START3: Waiting for IS_PLAYER_SWITCH_IN_PROGRESS")
					#ENDIF
				ENDIF
					
			BREAK
			
			CASE INTRO_CUT_SHOT1 //3
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_SHOT1)
				
				CREATE_HELI_FLYPAST(FMCutData)//preload the chopper as it causes problems
				
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_SHOT1)
				#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)#ENDIF
					
					CLEAR_HELP()
		
					#IF IS_DEBUG_BUILD
						PRINT_INTRO_STRING("MOVE TO CASE INTRO_CUT_RACE_START ")
					#ENDIF
										
					FMCutData.introCutStage = INTRO_CUT_CITY_SHOT_1_START
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_CITY_SHOT_1_START
			
				IF NOT IS_BIT_SET(FMCutData.iBitset, biIntroHelp)
					CLEAR_HELP()
					PRINT_HELP_NO_SOUND("FMIC_INTRO2") //Freemode lets you explore GTA V with other players.
					SET_BIT(FMCutData.iBitset, biIntroHelp)
				ENDIF
				
				SET_CAM_PARAMS(	FMCutData.cutsceneCam, 
								<<-259.3686, -553.8571, 142.6048>>, <<13.2752, -0.5186, -143.3378>>, 
								44.9959)
				SET_CAM_PARAMS(	FMCutData.cutsceneCam,  
								<<-277.7789, -569.9620, 142.8625>>, <<12.4479, -0.5186, -134.9992>>,
								44.9959, 6000, GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
								
				#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
				
				SET_SRL_READAHEAD_TIMES(SRL_READAHEAD_HEAVY_PRESTREAM_MAP,
										SRL_READAHEAD_HEAVY_PRESTREAM_ASSETS,
										SRL_READAHEAD_HEAVY_PLAYBACK_MAP,
										SRL_READAHEAD_HEAVY_PLAYBACK_ASSETS)
				
				FMCutData.introCutStage = INTRO_CUT_CITY_SHOT_1
				
			BREAK
			
			
			CASE INTRO_CUT_CITY_SHOT_1
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_CITY_SHOT_1)
				
				IF CREATE_HELI_FLYPAST(FMCutData)
					CREATE_AMBIENT_CARS_FOR_CUTSCENE(FMCutData)
					IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_CITY_SHOT_1)
						FMCutData.introCutStage = INTRO_CUT_CITY_SHOT_2_START
					ENDIF
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_CITY_SHOT_2_START
				
				IF NOT IS_BIT_SET(fmcutdata.iBitset, biStartRaceLoadScene)
							
					//-- Start load scene for race shot , 
					REQUEST_UBER_ASSETS()
					
					SET_BIT(fmcutdata.iBitset, biStartRaceLoadScene)
					#IF IS_DEBUG_BUILD
						PRINT_INTRO_STRING("Starte race load scene - 1")
					#ENDIF
					
				ENDIF
				
				SET_CAM_PARAMS(	FMCutData.cutsceneCam, 
								<<-4.6668, -900.9736, 184.8870>>, <<-1.6106, -0.5186, 78.3786>>, 
								45.0069)
				SET_CAM_PARAMS(	FMCutData.cutsceneCam,  
								<<-23.0087, -896.4288, 184.1939>>, <<-2.8529, -0.5186, 81.8262>>,
								45.0069, 8000, GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
								
				#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF				
				
				IF IS_VEHICLE_DRIVEABLE(FMCutData.vehHeli)
					UNPAUSE_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehHeli)
					SET_ENTITY_VISIBLE(FMCutData.vehHeli, TRUE)
					SET_HELI_BLADES_FULL_SPEED(FMCutData.vehHeli)
				ENDIF
				
				FMCutData.introCutStage = INTRO_CUT_CITY_SHOT_2
			
			BREAK
			
			
			CASE INTRO_CUT_CITY_SHOT_2
				IF IS_VEHICLE_DRIVEABLE(FMCutData.vehHeli)
					SET_HELI_BLADES_FULL_SPEED(FMCutData.vehHeli)
				ENDIF
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_CITY_SHOT_2)
				
				IF HAVE_UBER_ASSSETS_LOADED()
					IF CREATE_MAIN_UBER_REC_CAR(FMCutData)
						IF CREATE_AMBIENT_CARS_FOR_CUTSCENE(FMCutData)
							IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_CITY_SHOT_2)
							
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-2870.8044, 81.4979, 13.1957>>)
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE) 
								ENDIF
								
								fSkip = 5000.0
								IF IS_VEHICLE_DRIVEABLE(FMCutData.vehRacers[0])
									START_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehRacers[0], 100, "FM_Intro_uber")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehRacers[0], fSkip)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(FMCutData.vehRacers[1])
									START_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehRacers[1], 101, "FM_Intro_uber")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehRacers[1], fSkip)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(FMCutData.vehRacers[2])
									START_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehRacers[2], 102, "FM_Intro_uber")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehRacers[2], fSkip)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(FMCutData.vehRacers[3])
									START_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehRacers[3], 103, "FM_Intro_uber")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehRacers[3], fSkip)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(FMCutData.vehRacers[4])
									START_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehRacers[4], 104, "FM_Intro_uber")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehRacers[4], fSkip)
								ENDIF
								
								SET_BIT(FMCutData.iBitset, biDelayCam)
								
								#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
								
								DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
								
								FMCutData.introCutStage = INTRO_CUT_RACE_1
								#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("MOVE TO CASE INTRO_CUT_RACE_1 ") #ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			BREAK
			
			
			CASE INTRO_CUT_RACE_1
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					NETWORK_OVERRIDE_CLOCK_TIME(16, 00, 00)
					CLEANUP_HELI_FLYPAST(FMCutData)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "race_camera_1", "mp_intro_seq@", <<-2237.39, -333.52, 12.47>>, <<0.0, 0.0, 0.0>>, 0.1982)
					
					STOP_CAM_SHAKING(FMCutData.cutsceneCam, TRUE)
					DETACH_CAM(FMCutData.cutsceneCam)
					DESTROY_CAM(FMCutData.cutsceneCam)
					
					START_AUDIO_SCENE("MP_INTRO_RACE_SCENE")
					
					SET_SRL_READAHEAD_TIMES(SRL_READAHEAD_NORMAL_SP_PRESTREAM_MAP,
											SRL_READAHEAD_NORMAL_SP_PRESTREAM_ASSETS,
											SRL_READAHEAD_NORMAL_SP_PLAYBACK_MAP,
											SRL_READAHEAD_NORMAL_SP_PLAYBACK_ASSETS)
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_RACE_1)
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
				
				IF NOT IS_BIT_SET(FMCutData.iBitset, biDoneFirstRaceHelp)
					PRINT_HELP_NO_SOUND("FMIC_RACE1")
					SET_BIT(FMCutData.iBitset, biDoneFirstRaceHelp)
				ENDIF
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_RACE_1)
				
				
				
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_RACE_1)
				
//					#IF FEATURE_HEIST_PLANNING
//					FMCutData.interiorGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<234.0331, -1005.2032, -98.4626>>, "hei_dlc_garage_high_new")
//					#ENDIF
//					#IF NOT FEATURE_HEIST_PLANNING
//					FMCutData.interiorGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<234.0331, -1005.2032, -98.4626>>, "v_garagel")
//					#ENDIF
//
//					//FMCutData.interiorGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<234.0331, -1005.2032, -98.4626>>, "v_garagel")
//					
//					IF FMCutData.interiorGarage <> NULL
//						PIN_INTERIOR_IN_MEMORY(FMCutData.interiorGarage)
//					ELSE
//						#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Garage interior is NULL! ") #ENDIF
//					ENDIF
				
					SKIP_ALL_CARS(FMCutData, 23539.0)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					NETWORK_OVERRIDE_CLOCK_TIME(16, 00, 00)
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_RACE_2
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("MOVE TO CASE INTRO_CUT_RACE_2 ") #ENDIF
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_RACE_2
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "race_camera_2", "mp_intro_seq@", <<-2237.39, -333.52, 12.47>>, <<0.0, 0.0, 0.0>>, 0.39)
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_RACE_2)
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiCreatedCars)
					SETUP_GARAGE_ENTITIES(FMCutData)
				ELSE
					IF CREATE_LAMAR_AND_VAN(FMCutData)
					AND CREATE_JET(FMCutData)
						LOAD_AND_SETUP_MOCAP(FMCutData)
					ENDIF
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_RACE_2)
				
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_RACE_2)
					SKIP_ALL_CARS(FMCutData, 28833.330, 75.0)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					NETWORK_OVERRIDE_CLOCK_TIME(16, 00, 00)
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_RACE_3
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("MOVE TO CASE INTRO_CUT_RACE_3 ") #ENDIF
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_RACE_3
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ATTACH(FMCutData, "race_camera_3a", "mp_intro_seq@", FMCutData.vehRacers[4], <<0.0, 0.0, -0.1>>, <<0.0, 0.0, 0.0>>)
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_RACE_3)
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
				
				IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiCreatedCars)
					SETUP_GARAGE_ENTITIES(FMCutData)
				ELSE
					IF CREATE_LAMAR_AND_VAN(FMCutData)
					AND CREATE_JET(FMCutData)
						LOAD_AND_SETUP_MOCAP(FMCutData)
					ENDIF
				ENDIF
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_RACE_3)
				
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_RACE_3)
					//SKIP_ALL_CARS(FMCutData, 30000.0)
					SKIP_ALL_CARS(FMCutData, 29700.0)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					NETWORK_OVERRIDE_CLOCK_TIME(16, 00, 00)
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_RACE_4
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("MOVE TO CASE INTRO_CUT_RACE_4 ") #ENDIF
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_RACE_4
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "race_camera_3b", "mp_intro_seq@", <<-2237.39, -333.52, 12.47>>, <<0.0, 0.0, 0.0>>)
					SET_SYNCHRONIZED_SCENE_RATE(FMCutData.iSceneID, 0.70)
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_RACE_4)
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				IF NOT IS_BIT_SET(FMCutData.iBitset, biDoneSecondRaceHelp)
					SET_BIT(FMCutData.iBitset, biDoneSecondRaceHelp)
					PRINT_HELP_NO_SOUND("FMIC_RACE2") // You can challenge others with content you have created yourself or bet on players to win and make cash.
				ENDIF
				
				IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiCreatedCars)
					SETUP_GARAGE_ENTITIES(FMCutData)
				ELSE
					IF CREATE_LAMAR_AND_VAN(FMCutData)
					AND CREATE_JET(FMCutData)
						LOAD_AND_SETUP_MOCAP(FMCutData)
					ENDIF
				ENDIF
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_RACE_4)
				
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_RACE_4)
					SKIP_ALL_CARS(FMCutData, 36450.0)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					NETWORK_OVERRIDE_CLOCK_TIME(16, 00, 00)
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_RACE_5
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("MOVE TO CASE INTRO_CUT_RACE_5 ") #ENDIF
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_RACE_5
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "race_camera_4", "mp_intro_seq@", <<-2237.39, -333.52, 12.47>>, <<0.0, 0.0, 0.0>>)
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_RACE_5)
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
				
				IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiCreatedCars)
					SETUP_GARAGE_ENTITIES(FMCutData)
				ELSE
					IF CREATE_LAMAR_AND_VAN(FMCutData)
					AND CREATE_JET(FMCutData)
						LOAD_AND_SETUP_MOCAP(FMCutData)
					ENDIF
				ENDIF
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_RACE_5)
				
				IF LOAD_GTAO_LOGO()
				AND IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_RACE_5)
					SKIP_ALL_CARS(FMCutData, 43650.0)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					NETWORK_OVERRIDE_CLOCK_TIME(16, 00, 00)
					SET_BIT(FMCutData.iBitset, biDelayCam)
					
					FMCutData.introCutStage = INTRO_CUT_RACE_6
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("MOVE TO CASE INTRO_CUT_RACE_6 ") #ENDIF
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_RACE_6
				
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
				
					//Delete race cars that crash into each other at this point
					IF DOES_ENTITY_EXIST(FMCutData.vehRacers[2])
						DELETE_VEHICLE(FMCutData.vehRacers[2])
					ENDIF
					IF DOES_ENTITY_EXIST(FMCutData.vehRacers[3])
						DELETE_VEHICLE(FMCutData.vehRacers[3])
					ENDIF
					IF DOES_ENTITY_EXIST(FMCutData.vehRacers[4])
						DELETE_VEHICLE(FMCutData.vehRacers[4])
					ENDIF
				
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "race_camera_5", "mp_intro_seq@", <<-2237.39, -333.52, 12.47>>, <<0.0, 0.0, 0.0>>)
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_RACE_6)
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
				
				IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiCreatedCars)
					SETUP_GARAGE_ENTITIES(FMCutData)
				ELSE
					IF NOT IS_BIT_SET(FMCutData.iLoadBitset, loadBiEntitiesForcedIntoInterior)
						IF FMCutData.interiorGarage <> NULL
							IF IS_INTERIOR_READY(FMCutData.interiorGarage)
								IF NOT IS_ENTITY_DEAD(FMCutData.pedMechanic)
									FORCE_ROOM_FOR_ENTITY(FMCutData.pedMechanic, FMCutData.interiorGarage, GET_HASH_KEY("GtaMloRoom001"))
								ENDIF
								SET_STATIC_EMITTER_ENABLED("SE_MP_GARAGE_L_RADIO", FALSE)
								REPEAT NUMBER_OF_GARAGE_VEHICLES i
									IF NOT IS_ENTITY_DEAD(FMCutData.vehGarage[i])
										FORCE_ROOM_FOR_ENTITY(FMCutData.vehGarage[i], FMCutData.interiorGarage, GET_HASH_KEY("GtaMloRoom001"))
									ENDIF
								ENDREPEAT
								SET_BIT(FMCutData.iLoadBitset, loadBiEntitiesForcedIntoInterior)
							ENDIF
						ENDIF
					ENDIF
					
					IF CREATE_LAMAR_AND_VAN(FMCutData)
					AND CREATE_JET(FMCutData)
						LOAD_AND_SETUP_MOCAP(FMCutData)
					ENDIF
					
				ENDIF
				
				FORCE_CAR_BONNET_OPEN(FMCutData)
				FORCE_HIGH_DETAIL_CARS(FMCutData)
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_RACE_6)
				
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_RACE_6)
				AND IS_BIT_SET(FMCutData.iLoadBitset, loadBiCreatedCars)
					IF IS_BIT_SET(FMCutData.iBitset, biRunSplashStart)
						CLEAR_BIT(FMCutData.iBitset, biRunSplashStart)
					ENDIF
					
					FMCutData.introCutStage = INTRO_CUT_START_GARAGE
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("MOVE TO CASE INTRO_CUT_START_GARAGE ") #ENDIF
					
				/*ELIF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_RACE_6, -1000)
					IF NOT IS_BIT_SET(FMCutData.iBitset, biScreenFrozen)
						TOGGLE_RENDERPHASES(FALSE)
						SET_BIT(FMCutData.iBitset, biScreenFrozen)
					ENDIF*/
				
				ELIF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_RACE_6, -4968)
					IF NOT IS_BIT_SET(FMCutData.iBitset, biRunSplashStart)
						CLEAR_HELP(TRUE)
						SET_BIT(FMCutData.iBitset, biRunSplashStart)
						#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Set biRunSplashStart ") #ENDIF
					ENDIF
				ELIF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_RACE_6, -5618)
					IF NOT IS_BIT_SET(FMCutData.iBitset, biLogoFXPlayed)
						ANIMPOSTFX_PLAY("MP_intro_logo", 0, FALSE)
						SET_BIT(FMCutData.iBitset, biLogoFXPlayed)
						#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Set biLogoFXPlayed ") #ENDIF
					ENDIF
				ENDIF
			
			BREAK
			
			
			CASE INTRO_CUT_START_GARAGE
			
				FORCE_CAR_BONNET_OPEN(FMCutData)
				FORCE_HIGH_DETAIL_CARS(FMCutData)
			
				IF IS_BIT_SET(FMCutData.iLoadBitset, loadBiCreatedCars)
					IF NOT DOES_CAM_EXIST(FMCutData.cutsceneCam)
						FMCutData.cutsceneCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
					ENDIF
					SET_CAM_ACTIVE(FMCutData.cutsceneCam, TRUE)
					SET_CAM_PARAMS(	FMCutData.cutsceneCam,
									<<228.1892, -1006.7584, -96.8311>>, <<-13.0682, 0.0572, 0.7306>>, 
									40.0330)
					SET_CAM_PARAMS(	FMCutData.cutsceneCam, 
									<<228.2493, -1006.1546, -98.1976>>, <<-2.3097, 0.0572, 0.7306>>, 
									40.0330, 6000, GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
					SHAKE_CAM(FMCutData.cutsceneCam, "HAND_SHAKE", 0.1)
					
					FORCE_ROOM_FOR_GAME_VIEWPORT(FMCutData.interiorGarage, GET_HASH_KEY("GtaMloRoom001"))
					SET_STATIC_EMITTER_ENABLED("SE_MP_GARAGE_L_RADIO", FALSE)
					
					RESET_ADAPTATION(0)
					
					SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iMechanicSceneID, 0.0)
					IF NOT IS_PED_INJURED(FMCutData.pedMechanic)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(FMCutData.pedMechanic)
					ENDIF
					
					IF IS_AUDIO_SCENE_ACTIVE("MP_INTRO_RACE_SCENE")
						STOP_AUDIO_SCENE("MP_INTRO_RACE_SCENE")
					ENDIF
					
					SET_SRL_READAHEAD_TIMES(SRL_READAHEAD_HEAVY_PRESTREAM_MAP,
											SRL_READAHEAD_HEAVY_PRESTREAM_ASSETS,
											SRL_READAHEAD_HEAVY_PLAYBACK_MAP,
											SRL_READAHEAD_HEAVY_PLAYBACK_ASSETS)
					
					//CLEANUP_UBER_PLAYBACK()
					CLEANUP_UBER_ASSETS(FMCutData)
					CLEANUP_GTAO_LOGO()
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					PRINT_HELP_NO_SOUND("FMIC_GAR") // You can store your own collection of cars and bikes.
					FMCutData.introCutStage =  INTRO_CUT_GARAGE_2_START
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Moving to case INTRO_CUT_GARAGE_2_START") #ENDIF
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_GARAGE_2_START
				
				FORCE_CAR_BONNET_OPEN(FMCutData)
				FORCE_HIGH_DETAIL_CARS(FMCutData)
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_GARAGE_2_START)
				
				IF CREATE_LAMAR_AND_VAN(FMCutData)
				AND CREATE_JET(FMCutData)
					LOAD_AND_SETUP_MOCAP(FMCutData)
				ENDIF
				
				/*IF IS_BIT_SET(FMCutData.iBitset, biScreenFrozen)
					IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_RACE_6, 1000)
						TOGGLE_RENDERPHASES(TRUE)
						
						RESET_ADAPTATION(0)
						
						CLEAR_BIT(FMCutData.iBitset, biScreenFrozen)
					ENDIF
				ENDIF*/
				
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_GARAGE_2_START)
					SET_CAM_PARAMS(	FMCutData.cutsceneCam, 
									<<222.9166, -999.9128, -99.4285>>, <<5.5453, 0.0572, -57.3736>>, 
									31.9442)
					SET_CAM_PARAMS(	FMCutData.cutsceneCam,  
									<<223.1394, -999.8619, -99.4078>>, <<5.5453, 0.0572, -56.2437>>,
									31.9442, 3500, GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
					
					SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iMechanicSceneID, 0.0)
					IF NOT IS_PED_INJURED(FMCutData.pedMechanic)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(FMCutData.pedMechanic)
					ENDIF
					
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF				
					
					CLEANUP_INTRO_CUT_GARAGE_CAR(FMCutData, 0)
					CLEANUP_INTRO_CUT_GARAGE_CAR(FMCutData, 1)
					CLEANUP_INTRO_CUT_GARAGE_CAR(FMCutData, 5)
					CLEANUP_INTRO_CUT_GARAGE_CAR(FMCutData, 6)
					
					FMCutData.introCutStage = INTRO_CUT_GARAGE_2
				ENDIF
			
			BREAK
			
			
			CASE INTRO_CUT_GARAGE_2
				
				FORCE_CAR_BONNET_OPEN(FMCutData)
				FORCE_HIGH_DETAIL_CARS(FMCutData)
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_GARAGE_2)
				
				IF CREATE_LAMAR_AND_VAN(FMCutData)
				AND CREATE_JET(FMCutData)
					LOAD_AND_SETUP_MOCAP(FMCutData)
				ENDIF
				
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_GARAGE_2)
					FMCutData.introCutStage = INTRO_CUT_GARAGE_4_START
				ENDIF
			BREAK
			
			
			CASE INTRO_CUT_GARAGE_4_START
				
				FORCE_CAR_BONNET_OPEN(FMCutData)
				FORCE_HIGH_DETAIL_CARS(FMCutData)
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1148.285645,-1639.737061,5.106698>>)
				SET_BIT(FMCutData.iBitset, biBlockSettingFocus)
				
				/*SET_CAM_PARAMS(	FMCutData.cutsceneCam, 
								<<226.5056, -979.8753, -98.8740>>, <<-3.0848, 0.0347, -115.3629>>, 
								25.2446)
				SET_CAM_PARAMS(	FMCutData.cutsceneCam, 
								<<226.4608, -978.0541, -98.8740>>, <<-3.0848, 0.0347, -115.3629>>, 
								25.2446, 4500, GRAPH_TYPE_DECEL,GRAPH_TYPE_DECEL)*/
				/*SET_CAM_PARAMS(	FMCutData.cutsceneCam, 
								<<226.4720, -978.5094, -98.8740>>, <<-3.0848, 0.0347, -115.3629>>, 
								25.2446)
				SET_CAM_PARAMS(	FMCutData.cutsceneCam, 
								<<226.4608, -978.0541, -98.8740>>, <<-3.0848, 0.0347, -115.3629>>, 
								25.2446, 1000, GRAPH_TYPE_DECEL,GRAPH_TYPE_DECEL)*/
				SET_CAM_PARAMS(	FMCutData.cutsceneCam, 
								<<226.4832, -978.9647, -98.8740>>, <<-3.0848, 0.0347, -115.3629>>, 
								25.2446)
				SET_CAM_PARAMS(	FMCutData.cutsceneCam, 
								<<226.4608, -978.0541, -98.8740>>, <<-3.0848, 0.0347, -115.3629>>, 
								25.2446, 1500, GRAPH_TYPE_DECEL,GRAPH_TYPE_DECEL)
				
				NETWORK_OVERRIDE_CLOCK_TIME(18, 00, 00)
				
				#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF				
				
				CLEANUP_INTRO_CUT_GARAGE_CAR(FMCutData, 2)
				CLEANUP_INTRO_CUT_GARAGE_CAR(FMCutData, 7)
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_INTRO_MECHANIC_MODEL())
				IF DOES_ENTITY_EXIST(FMCutData.pedMechanic)
					DELETE_PED(FMCutData.pedMechanic)
				ENDIF
				
				SET_BIT(FMCutData.iBitset, biDrawTennisLights)//start drawing lights to hide flicker
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === SET_BIT(FMCutData.iBitset, biDrawTennisLights)")
				#ENDIF
				
				FMCutData.introCutStage = INTRO_CUT_GARAGE_4
			
			BREAK
			
			
			CASE INTRO_CUT_GARAGE_4
			
				FORCE_CAR_BONNET_OPEN(FMCutData)
				FORCE_HIGH_DETAIL_CARS(FMCutData)
			
				IF NOT IS_BIT_SET(FMCutData.iBitset, biSetGarageRoom)
					SET_BIT(FMCutData.iBitset, biSetGarageRoom)
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Set game viewport room") #ENDIF
				ENDIF
				
				IF CREATE_LAMAR_AND_VAN(FMCutData)
				AND CREATE_JET(FMCutData)
					LOAD_AND_SETUP_MOCAP(FMCutData)
				ENDIF
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_GARAGE_4)
				
				IF PREPARE_MUSIC_EVENT("FM_INTRO_DRIVE_START")
				AND IS_BIT_SET(FMCutData.iLoadBitset, loadBiMocapLoaded)
				AND IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_GARAGE_4)  
				#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)#ENDIF
					
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					
					FMCutData.introCutStage =  INTRO_CUT_MOCAP_START
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Moving to case INTRO_CUT_MOCAP_START") #ENDIF
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_MOCAP_START
			
				IF FMCutData.bRegPlayerOn
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Mocap starting for real")
					#ENDIF
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					
					
						IF IS_INTRO_FEMALE(FMCutData)
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_Male_Character", CU_DONT_ANIMATE_ENTITY, MP_M_FREEMODE_01)
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_Female_Character", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_Male_Character", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_Female_Character", CU_DONT_ANIMATE_ENTITY, MP_F_FREEMODE_01)
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Mocap starting as fake")
					#ENDIF
					IF IS_INTRO_FEMALE(FMCutData)
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_Male_Character", CU_DONT_ANIMATE_ENTITY, MP_M_FREEMODE_01)
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_Female_Character", CU_DONT_ANIMATE_ENTITY, MP_F_FREEMODE_01)
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(FMCutData.pedIGLamar)
					REGISTER_ENTITY_FOR_CUTSCENE(FMCutData.pedIGLamar, "LAMAR", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF NOT IS_ENTITY_DEAD(FMCutData.vehLamar)
					REGISTER_ENTITY_FOR_CUTSCENE(FMCutData.vehLamar, "MP_Lamar_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF NOT IS_ENTITY_DEAD(FMCutData.objPlane)
					SET_ENTITY_VISIBLE(FMCutData.objPlane, TRUE)
					REGISTER_ENTITY_FOR_CUTSCENE(FMCutData.objPlane, "MP_Plane", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				REGISTER_JET_POPULATION_FOR_CUTSCENE(FMCutData)
			
				LOAD_CLOUD_HAT("CONTRAILS") 
			
				SET_SRL_READAHEAD_TIMES(SRL_READAHEAD_NORMAL_PRESTREAM_MAP,
										SRL_READAHEAD_NORMAL_PRESTREAM_ASSETS,
										SRL_READAHEAD_NORMAL_PLAYBACK_MAP,
										SRL_READAHEAD_NORMAL_PLAYBACK_ASSETS)
			
				PRINT_HELP_NO_SOUND("FMIC_TENN")//~s~Challenge others to tennis, golf and many other activities.
				START_CUTSCENE(CUTSCENE_DO_NOT_REPOSITION_PLAYER_TO_SCENE_ORIGIN)
	            SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				FMCutData.introCutStage = INTRO_CUT_MOCAP_CONFIRM_PLAYING
			BREAK
			
			CASE INTRO_CUT_MOCAP_CONFIRM_PLAYING
				IF IS_CUTSCENE_PLAYING()
					IF FMCutData.interiorGarage <> NULL
						UNPIN_INTERIOR(FMCutData.interiorGarage)
						#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Case INTRO_CUT_TENNIS_MOCAP_CONFIRM_PLAYING unpinned interior") #ENDIF
					ENDIF
					CLEANUP_INTRO_CUT_CUSTOM_GARAGE_MODELS(FMCutData)
					
					IF FMCutData.bRecordOn
						FMCutData.iStartOfMocapTime = GET_GAME_TIMER() - FMCutData.iCutStartTime
					ELSE
						FMCutData.iStartOfMocapTime = MP_INTRO_START_OF_MOCAP_TIME
						FMCutData.iStartOfMocapSRLAdjust = MP_INTRO_START_OF_MOCAP_TIME
					ENDIF
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === FMCutData.iStartOfMocapTime = ", FMCutData.iStartOfMocapTime)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === FMCutData.iStartOfMocapSRLAdjust = ", FMCutData.iStartOfMocapSRLAdjust)
					#ENDIF
					
					FMCutData.introCutStage = INTRO_CUT_MOCAP_PLAYING
				ELSE
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Waiting for IS_CUTSCENE_PLAYING") #ENDIF
				ENDIF
			BREAK
			
			CASE INTRO_CUT_MOCAP_PLAYING
				IF FMCutData.bRegPlayerOn
					IF IS_INTRO_FEMALE(FMCutData)
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_Female_Character")
							//Do stuff.
							IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								
								
									IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										TASK_ENTER_VEHICLE(PLAYER_PED_ID(), FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
									ENDIF
								ENDIF
							ENDIF
		                ENDIF
					ELSE
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_Male_Character")
							//Do stuff.
							IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())

								
									IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										TASK_ENTER_VEHICLE(PLAYER_PED_ID(), FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
									ENDIF
								ENDIF
							ENDIF
		                ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("LAMAR")
						//Do stuff.
						IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
							IF NOT IS_PED_INJURED(FMCutData.pedIGLamar)
								IF NOT IS_PED_IN_ANY_VEHICLE(FMCutData.pedIGLamar)
									TASK_ENTER_VEHICLE(FMCutData.pedIGLamar, FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(FMCutData.pedIGLamar)
								ENDIF
							ENDIF
						ENDIF
	                ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_Lamar_Car")
						//Do stuff.
						IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
							SET_VEHICLE_ENGINE_ON(FMCutData.vehLamar, TRUE, TRUE)
							SET_ENTITY_COORDS(FMCutData.vehLamar, <<-1028.4063, -2732.7822, 19.5931>>)
							SET_ENTITY_HEADING(FMCutData.vehLamar, -106.11)//-1.85
							SKIP_LAMAR_CAR(FMCutData, 3099.997)
							SET_VEHICLE_DOORS_SHUT(FMCutData.vehLamar, TRUE)
						ENDIF
					ENDIF
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					NETWORK_OVERRIDE_CLOCK_TIME(21, 00, 00)
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_01)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_01", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
				ENDIF
				
				IF IS_BIT_SET(FMCutData.iBitset, biDrawTennisLights)	//clear tennis lights on first cut
					IF HAS_CUTSCENE_CUT_THIS_FRAME()
					OR GET_CUTSCENE_TIME() > 3254
						CLEAR_BIT(FMCutData.iBitset, biDrawTennisLights)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CLEAR_BIT(FMCutData.iBitset, biDrawTennisLights)")
						#ENDIF
					ENDIF
				ENDIF
				
				IF CREATE_CS_LAMAR(FMCutData)
				AND LOAD_DRIVE_ANIMS(FMCutData)
				AND NOT IS_CUTSCENE_PLAYING()
					IF FMCutData.bRecordOn
						FMCutData.iEndOfMocapTime = GET_GAME_TIMER() - FMCutData.iCutStartTime
					ELSE
						
						FMCutData.iEndOfMocapSRLAdjust = MP_INTRO_END_OF_MOCAP_TIME
						
						IF IS_INTRO_FEMALE(FMCutData)
							FMCutData.iEndOfMocapTime = MP_INTRO_END_OF_MOCAP_TIME	//use adjusted value for female so can share SRL
						ELSE
							FMCutData.iEndOfMocapTime = MP_INTRO_END_OF_MOCAP_TIME
						ENDIF
						
					ENDIF
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === FMCutData.iEndOfMocapTime = ", FMCutData.iEndOfMocapTime)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === FMCutData.iEndOfMocapSRLAdjust = ", FMCutData.iEndOfMocapSRLAdjust)
					#ENDIF
					
					DISABLE_CINEMATIC_VEHICLE_IDLE_MODE_THIS_UPDATE()
					INVALIDATE_CINEMATIC_VEHICLE_IDLE_MODE()
					
					IF IS_BIT_SET(FMCutData.iBitset, biDoMusicCues)
						TRIGGER_MUSIC_EVENT("FM_INTRO_DRIVE_START")
						#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("[IntroCue] Playing cue FM_INTRO_DRIVE_START") #ENDIF
						SET_BIT(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
					ENDIF
				
					IF NOT HAS_CUTSCENE_FINISHED()
						STOP_CUTSCENE()
					ENDIF
					
					IF NOT IS_BIT_SET(iBitset, WALK_PLAYER_INTO_TAXI)
						SET_BIT(iBitset, WALK_PLAYER_INTO_TAXI)
						#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Set WALK_PLAYER_INTO_TAXI 1") #ENDIF
					ENDIF
					
					CLEANUP_JET(FMCutData)
					
					CLEANUP_AIRPORT_JETS(FMCutData)
					
				    SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
		
					CLEAR_BIT(FMCutData.iBitset, biBlockSettingFocus)
					
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_01
					
				ELSE
				
					#IF IS_DEBUG_BUILD
					IF NOT CREATE_CS_LAMAR(FMCutData)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === NOT CREATE_CS_LAMAR")
					ENDIF
					IF NOT LOAD_DRIVE_ANIMS(FMCutData)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === NOT LOAD_DRIVE_ANIMS")
					ENDIF
					#ENDIF
				
					IF FMCutData.bRecordOn
						FMCutData.iCutStartTime = GET_GAME_TIMER() - (FMCutData.iStartOfMocapTime + GET_CUTSCENE_TIME())
					ENDIF
							
					
					IF NOT IS_BIT_SET(FMCutData.iBitset, biTimeSetToNightInCutscene)
						IF GET_CUTSCENE_CONCAT_SECTION_PLAYING() = 1
							NETWORK_OVERRIDE_CLOCK_TIME(19, 59, 00)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === SET_BIT(FMCutData.iBitset, biPlaneLandFarclip)")
							#ENDIF
							SET_BIT(FMCutData.iBitset, biTimeSetToNightInCutscene)
						ENDIF
					ENDIF
					
					IF CREATE_AIRPORT_JETS(FMCutData)
						IF GET_CUTSCENE_CONCAT_SECTION_PLAYING() = 2
							
							IF NOT IS_BIT_SET(FMCutData.iBitset, biDisplayFMIntroHelp)
								#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("SET_BIT(FMCutData.iBitset, biDisplayFMIntroHelp)") #ENDIF
								SET_BIT(FMCutData.iBitset, biDisplayFMIntroHelp)
							ENDIF
							
							IF GET_CUTSCENE_TIME() > 23146
								IF NOT IS_BIT_SET(FMCutData.iBitset, biTyreSqueal0)
									/*IF*/ START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(	"scr_mp_plane_landing_tyre_smoke", FMCutData.objPlane, //right
																				<<-2.508, -3.666, -3.584>>, <<0.0,0.0,-90.0>>)
										SET_BIT(FMCutData.iBitset, biTyreSqueal0)
									//ENDIF
								ENDIF
								IF NOT IS_BIT_SET(FMCutData.iBitset, biTyreSqueal1)
									/*IF*/ START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(	"scr_mp_plane_landing_tyre_smoke", FMCutData.objPlane, //left
																				<<3.508, -3.666, -3.584>>, <<0.0,0.0,-90.0>>)
										SET_BIT(FMCutData.iBitset, biTyreSqueal1)
									//ENDIF
								ENDIF
							ENDIF
						
							SET_CUTSCENE_CAM_FAR_CLIP_THIS_UPDATE(4950.0)
							IF IS_BIT_SET(FMCutData.iBitset, biTimeSetToNightInCutscene)
								NETWORK_OVERRIDE_CLOCK_TIME(21, 00, 00)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CLEAR_BIT(FMCutData.iBitset, biPlaneLandFarclip)")
								#ENDIF
								CLEAR_BIT(FMCutData.iBitset, biTimeSetToNightInCutscene)
							ENDIF
							
							IF GET_CUTSCENE_TIME() > 23254
								START_AIRPORT_JETS(FMCutData)
							ENDIF
							
						ENDIF
					ENDIF
					
					IF GET_CUTSCENE_CONCAT_SECTION_PLAYING() > 2
						CLEANUP_AIRPORT_JETS(FMCutData)
					
						IF NOT IS_BIT_SET(FMCutData.iBitset, biTimeSetToNightInCutscene)
							NETWORK_OVERRIDE_CLOCK_TIME(21, 00, 00)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === SET_BIT(FMCutData.iBitset, biTimeSetToNightInCutscene)")
							#ENDIF
							SET_BIT(FMCutData.iBitset, biTimeSetToNightInCutscene)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
							REQUEST_VEHICLE_HIGH_DETAIL_MODEL(FMCutData.vehLamar)
							SET_VEHICLE_LIGHTS(FMCutData.vehLamar, FORCE_VEHICLE_LIGHTS_ON)
						ENDIF
					ENDIF
					
					FMCutData.iMocapTimeLastFrame = GET_CUTSCENE_TIME()
					
				ENDIF
				
			BREAK
						
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_01
				IF IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 1)
						IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_01, -5450)
							//~z~Say baby, it is a gang of opportunities in Los Santos.
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM2", "FM_LAM2_1", CONV_PRIORITY_VERY_HIGH)
		               			SET_BIT(FMCutData.iSpeechBitset, 1)
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 2)
						IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_01, -5450)
							//~z~I mean, that's if you got the nuts... or the... you know...
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM2", "FM_LAM2_3", CONV_PRIORITY_VERY_HIGH)
		               			SET_BIT(FMCutData.iSpeechBitset, 2)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 1)
						IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_01, -5450)
							//~z~Man, dog, I'm glad you here, man. There's all kinds of opportunities in Los Santos,
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM1", "FM_LAM1_1", CONV_PRIORITY_VERY_HIGH)
		               			SET_BIT(FMCutData.iSpeechBitset, 1)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					
					START_AUDIO_SCENE("MP_INTRO_LAMAR_DRIVE_SCENE")
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_01)
				
				CREATE_VAGOS_FOR_CUTSCENE(FMCutData)
				
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_01)
					//SKIP_LAMAR_CAR(FMCutData, 9966.65)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01
			
				IF NOT IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 2)
						//~z~I mean, that's if you got the nuts to pull 'em off.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM1", "FM_LAM1_3", CONV_PRIORITY_VERY_HIGH)
	               			SET_BIT(FMCutData.iSpeechBitset, 2)
						ENDIF
					ENDIF
				ENDIF
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					//RUN_SYNC_SCENE_CAM_ATTACH(FMCutData, "LAMAR_CAR_DRIVE_06", "mp_intro_seq@", FMCutData.vehLamar, <<0.0, -2.0, 0.4>>, <<0.0, 0.0, 0.0>>)
					IF NOT IS_ENTITY_DEAD(FMCutData.vehLamar)
						ATTACH_CAM_TO_ENTITY(FMCutData.cutsceneCam, FMCutData.vehLamar, <<0.8056, 2.2245, 0.5533>>)
						POINT_CAM_AT_ENTITY(FMCutData.cutsceneCam, FMCutData.vehLamar, <<-0.4552, -0.4977, 0.5449>>)
						SET_CAM_FOV(FMCutData.cutsceneCam, 33.3479)
						SHAKE_CAM(FMCutData.cutsceneCam, "ROAD_VIBRATION_SHAKE", 2.0)
						SET_CAM_ACTIVE(FMCutData.cutsceneCam, TRUE)
						
						IF NOT IS_ENTITY_DEAD(FMCutData.pedIGLamar)
							SET_ENTITY_VISIBLE(FMCutData.pedIGLamar, FALSE)
						ENDIF
						IF NOT IS_ENTITY_DEAD(FMCutData.pedCSLamar)
							SET_ENTITY_VISIBLE(FMCutData.pedCSLamar, TRUE)
						ENDIF
						
						FMCutData.iLamarSceneID = -1
						FMCutData.iLamarSceneId = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iLamarSceneId, FMCutData.vehLamar, 
															GET_ENTITY_BONE_INDEX_BY_NAME(FMCutData.vehLamar, "seat_dside_f"))
						
						IF IS_INTRO_FEMALE(FMCutData)
							//female synch scene needs to be at front right seat
							FMCutData.iPlayerSceneID = -1
							FMCutData.iPlayerSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iPlayerSceneID, FMCutData.vehLamar, 
																GET_ENTITY_BONE_INDEX_BY_NAME(FMCutData.vehLamar, "seat_pside_f"))
						
							TASK_SYNCHRONIZED_SCENE(FMCutData.pedCSLamar, FMCutData.iLamarSceneID, "mp_intro_seq@ig_1_lamar_drive_female", 
													"ig_1_lamar_drive_female_lamar", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), FMCutData.iPlayerSceneID, "mp_intro_seq@ig_1_lamar_drive_female", 
													"ig_1_lamar_drive_female_fem", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iLamarSceneID, 0.129)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iPlayerSceneID, 0.129)
						ELSE
							//male synch scene needs to be at vehicle origin
							FMCutData.iPlayerSceneID = -1
							FMCutData.iPlayerSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iPlayerSceneID, FMCutData.vehLamar, 0)
							
							TASK_SYNCHRONIZED_SCENE(FMCutData.pedCSLamar, FMCutData.iLamarSceneID, "mp_intro_seq@ig_1_lamar_drive_male", 
													"ig_1_lamar_drive_male_lamar", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), FMCutData.iPlayerSceneID, "mp_intro_seq@ig_1_lamar_drive_male", 
													"ig_1_lamar_drive_male_male", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iLamarSceneID, 0.145)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iPlayerSceneID, 0.145)
						ENDIF
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(FMCutData.pedCSLamar)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01)
			
				CREATE_VAGOS_FOR_CUTSCENE(FMCutData)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01)
					SKIP_LAMAR_CAR(FMCutData, 18966.65)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_03
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_03
			
				IF IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 3)
						IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01, 1100)
							//~z~Your boy know people all over the city.
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM2", "FM_LAM2_5", CONV_PRIORITY_VERY_HIGH)
		               			SET_BIT(FMCutData.iSpeechBitset, 3)
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 4)
						IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01, 1100)
							//~z~Like my boy Hao, he, like literally the best mechanic in town.
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM2", "FM_LAM2_7", CONV_PRIORITY_VERY_HIGH)
		               			SET_BIT(FMCutData.iSpeechBitset, 4)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF NOT IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 3)
						IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_01, 1000)
							//~z~And don't even trip, man, your boy know people all over the city. Like, the boy Hao, 
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM1", "FM_LAM1_5", CONV_PRIORITY_VERY_HIGH)
		               			SET_BIT(FMCutData.iSpeechBitset, 3)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_03", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					
					IF NOT IS_ENTITY_DEAD(FMCutData.pedIGLamar)
						SET_ENTITY_VISIBLE(FMCutData.pedIGLamar, TRUE)
					ENDIF
					IF NOT IS_ENTITY_DEAD(FMCutData.pedCSLamar)
						SET_ENTITY_VISIBLE(FMCutData.pedCSLamar, FALSE)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					AND NOT IS_ENTITY_DEAD(FMCutData.vehLamar)
						TASK_ENTER_VEHICLE(	PLAYER_PED_ID(), FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, 
											ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_03)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_03)
			
				CREATE_VAGOS_FOR_CUTSCENE(FMCutData)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_03)
					//SKIP_LAMAR_CAR(FMCutData, 43150.960)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02
					
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02
			
				IF IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 5)
						//~z~And my boy, Simeon, you know, he the best bullshit artist in town, with his bullshitting ass. 
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM2", "FM_LAM2_9", CONV_PRIORITY_VERY_HIGH)
	               			SET_BIT(FMCutData.iSpeechBitset, 5)
						ENDIF
					ENDIF
				ENDIF
			
				IF NOT IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 4)
						//~z~He is like hands down the best mechanic in town.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM1", "FM_LAM1_7", CONV_PRIORITY_VERY_HIGH)
		           			SET_BIT(FMCutData.iSpeechBitset, 4)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 5)
						IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02, -3750)
							//~z~Or the other boy, Simeon, he's literally the best bullshit artist in town.
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM1", "FM_LAM1_9", CONV_PRIORITY_VERY_HIGH)
			           			SET_BIT(FMCutData.iSpeechBitset, 5)
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					//RUN_SYNC_SCENE_CAM_ATTACH(FMCutData, "LAMAR_CAR_DRIVE_06", "mp_intro_seq@", FMCutData.vehLamar, <<0.0, -2.0, 0.4>>, <<0.0, 0.0, 0.0>>)
					IF NOT IS_ENTITY_DEAD(FMCutData.vehLamar)
						ATTACH_CAM_TO_ENTITY(FMCutData.cutsceneCam, FMCutData.vehLamar, <<0.8056, 2.2245, 0.5533>>)
						POINT_CAM_AT_ENTITY(FMCutData.cutsceneCam, FMCutData.vehLamar, <<-0.4552, -0.4977, 0.5449>>)
						SET_CAM_FOV(FMCutData.cutsceneCam, 33.3479)
						SHAKE_CAM(FMCutData.cutsceneCam, "ROAD_VIBRATION_SHAKE", 2.0)
						SET_CAM_ACTIVE(FMCutData.cutsceneCam, TRUE)
						
						IF NOT IS_ENTITY_DEAD(FMCutData.pedIGLamar)
							SET_ENTITY_VISIBLE(FMCutData.pedIGLamar, FALSE)
						ENDIF
						IF NOT IS_ENTITY_DEAD(FMCutData.pedCSLamar)
							SET_ENTITY_VISIBLE(FMCutData.pedCSLamar, TRUE)
						ENDIF
						
						FMCutData.iLamarSceneID = -1
						FMCutData.iLamarSceneId = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iLamarSceneId, FMCutData.vehLamar, 
															GET_ENTITY_BONE_INDEX_BY_NAME(FMCutData.vehLamar, "seat_dside_f"))
						
						IF IS_INTRO_FEMALE(FMCutData)
							//female synch scene needs to be at front right seat
							FMCutData.iPlayerSceneID = -1
							FMCutData.iPlayerSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iPlayerSceneID, FMCutData.vehLamar, 
																GET_ENTITY_BONE_INDEX_BY_NAME(FMCutData.vehLamar, "seat_pside_f"))
						
							TASK_SYNCHRONIZED_SCENE(FMCutData.pedCSLamar, FMCutData.iLamarSceneID, "mp_intro_seq@ig_1_lamar_drive_female", 
													"ig_1_lamar_drive_female_lamar", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), FMCutData.iPlayerSceneID, "mp_intro_seq@ig_1_lamar_drive_female", 
													"ig_1_lamar_drive_female_fem", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iLamarSceneID, 0.363)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iPlayerSceneID, 0.363)
						ELSE
							//male synch scene needs to be at vehicle origin
							FMCutData.iPlayerSceneID = -1
							FMCutData.iPlayerSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iPlayerSceneID, FMCutData.vehLamar, 0)
							
							TASK_SYNCHRONIZED_SCENE(FMCutData.pedCSLamar, FMCutData.iLamarSceneID, "mp_intro_seq@ig_1_lamar_drive_male", 
													"ig_1_lamar_drive_male_lamar", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), FMCutData.iPlayerSceneID, "mp_intro_seq@ig_1_lamar_drive_male", 
													"ig_1_lamar_drive_male_male", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iLamarSceneID, 0.298)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iPlayerSceneID, 0.298)
						ENDIF
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(FMCutData.pedCSLamar)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02)
			
				CREATE_VAGOS_FOR_CUTSCENE(FMCutData)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02)
					SKIP_LAMAR_CAR(FMCutData, 42700.960)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_05
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_05
				
				IF IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 6)
						//~z~And my boy G, or any of the homies from Families., baby, you straight.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM2", "FM_LAM2_11", CONV_PRIORITY_VERY_HIGH)
	               			SET_BIT(FMCutData.iSpeechBitset, 6)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 6)
						IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_02, 1000)
							//~z~Or we can go fuck with the homie G, or any of the homies from families for that matter. You straight.
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM1", "FM_LAM1_11", CONV_PRIORITY_VERY_HIGH)
			           			SET_BIT(FMCutData.iSpeechBitset, 6)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_05", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					
					IF NOT IS_ENTITY_DEAD(FMCutData.pedIGLamar)
						SET_ENTITY_VISIBLE(FMCutData.pedIGLamar, TRUE)
					ENDIF
					IF NOT IS_ENTITY_DEAD(FMCutData.pedCSLamar)
						SET_ENTITY_VISIBLE(FMCutData.pedCSLamar, FALSE)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					AND NOT IS_ENTITY_DEAD(FMCutData.vehLamar)
						TASK_ENTER_VEHICLE(	PLAYER_PED_ID(), FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, 
											ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_05)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_05)
			
				CREATE_VAGOS_FOR_CUTSCENE(FMCutData)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_05)
					SKIP_LAMAR_CAR(FMCutData, 51033.280)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_07
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_07
			
				IF IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 7)
						//~z~Matter of fact I'ma fun you to Hao's right now cause he might get you a little race or something. Know what I'm talkin' about? You like cars right?
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM2", "FM_LAM2_13", CONV_PRIORITY_VERY_HIGH)
	               			SET_BIT(FMCutData.iSpeechBitset, 7)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 8)
						//~z~Yeah. Put up a little wager, you know. I got my money on you baby. I got a lot of shit on you.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM2", "FM_LAM2_15", CONV_PRIORITY_VERY_HIGH)
	               			SET_BIT(FMCutData.iSpeechBitset, 8)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 9)
						//~z~But...
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM2", "FM_LAM2_17", CONV_PRIORITY_VERY_HIGH)
	               			SET_BIT(FMCutData.iSpeechBitset, 9)
						ENDIF
					ENDIF
				ENDIF
			
				IF NOT IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 7)
						IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_05, 2500)
							//~z~Matter of fact, we gonna fall through on Hao right now, see if we can get us a race, you know.
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM1", "FM_LAM1_13", CONV_PRIORITY_VERY_HIGH)
			           			SET_BIT(FMCutData.iSpeechBitset, 7)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_07", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_07)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_07)
			
				CREATE_VAGOS_FOR_CUTSCENE(FMCutData)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_07)
					//SKIP_LAMAR_CAR(FMCutData, 116366.500)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03
					
				ENDIF
				
			BREAK
			

			CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03
			
				IF NOT IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 8)
						//~z~You with the business, we can make us a little wager.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM1", "FM_LAM1_15", CONV_PRIORITY_VERY_HIGH)
		           			SET_BIT(FMCutData.iSpeechBitset, 8)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 9)
						//~z~lolololololol
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAM1", "FM_LAM1_17", CONV_PRIORITY_VERY_HIGH)
		           			SET_BIT(FMCutData.iSpeechBitset, 9)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 31)
						//~z~Wait, that's my shit.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAMRAD", "FM_LAMRAD_1", CONV_PRIORITY_VERY_HIGH)
		           			SET_BIT(FMCutData.iSpeechBitset, 31)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_INTRO_FEMALE(FMCutData)
					IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 31)
						//~z~Wait, that's my shit.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAMRAD", "FM_LAMRAD_1", CONV_PRIORITY_VERY_HIGH)
		           			SET_BIT(FMCutData.iSpeechBitset, 31)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					//RUN_SYNC_SCENE_CAM_ATTACH(FMCutData, "LAMAR_CAR_DRIVE_06", "mp_intro_seq@", FMCutData.vehLamar, <<0.0, -2.0, 0.4>>, <<0.0, 0.0, 0.0>>)
					IF NOT IS_ENTITY_DEAD(FMCutData.vehLamar)
						ATTACH_CAM_TO_ENTITY(FMCutData.cutsceneCam, FMCutData.vehLamar, <<0.8056, 2.2245, 0.5533>>)
						POINT_CAM_AT_ENTITY(FMCutData.cutsceneCam, FMCutData.vehLamar, <<-0.4552, -0.4977, 0.5449>>)
						SET_CAM_FOV(FMCutData.cutsceneCam, 33.3479)
						SHAKE_CAM(FMCutData.cutsceneCam, "ROAD_VIBRATION_SHAKE", 2.0)
						SET_CAM_ACTIVE(FMCutData.cutsceneCam, TRUE)
						
						IF NOT IS_ENTITY_DEAD(FMCutData.pedIGLamar)
							SET_ENTITY_VISIBLE(FMCutData.pedIGLamar, FALSE)
						ENDIF
						IF NOT IS_ENTITY_DEAD(FMCutData.pedCSLamar)
							SET_ENTITY_VISIBLE(FMCutData.pedCSLamar, TRUE)
						ENDIF
						
						FMCutData.iLamarSceneID = -1
						FMCutData.iLamarSceneId = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iLamarSceneId, FMCutData.vehLamar, 
															GET_ENTITY_BONE_INDEX_BY_NAME(FMCutData.vehLamar, "seat_dside_f"))
						
						IF IS_INTRO_FEMALE(FMCutData)
							//female synch scene needs to be at front right seat
							FMCutData.iPlayerSceneID = -1
							FMCutData.iPlayerSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iPlayerSceneID, FMCutData.vehLamar, 
																GET_ENTITY_BONE_INDEX_BY_NAME(FMCutData.vehLamar, "seat_pside_f"))
						
							TASK_SYNCHRONIZED_SCENE(FMCutData.pedCSLamar, FMCutData.iLamarSceneID, "mp_intro_seq@ig_1_lamar_drive_female", 
													"ig_1_lamar_drive_female_lamar", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), FMCutData.iPlayerSceneID, "mp_intro_seq@ig_1_lamar_drive_female", 
													"ig_1_lamar_drive_female_fem", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iLamarSceneID, 0.811)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iPlayerSceneID, 0.811)
						ELSE
							//male synch scene needs to be at vehicle origin
							FMCutData.iPlayerSceneID = -1
							FMCutData.iPlayerSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iPlayerSceneID, FMCutData.vehLamar, 0)
							
							TASK_SYNCHRONIZED_SCENE(FMCutData.pedCSLamar, FMCutData.iLamarSceneID, "mp_intro_seq@ig_1_lamar_drive_male", 
													"ig_1_lamar_drive_male_lamar", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), FMCutData.iPlayerSceneID, "mp_intro_seq@ig_1_lamar_drive_male", 
													"ig_1_lamar_drive_male_male", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iLamarSceneID, 0.791)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iPlayerSceneID, 0.791)
						ENDIF
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(FMCutData.pedCSLamar)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03)
			
				CREATE_VAGOS_FOR_CUTSCENE(FMCutData)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_03)
					SKIP_LAMAR_CAR(FMCutData, 85050.000)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_10
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_10
			
				IF NOT IS_BIT_SET(FMCutData.iSpeechBitset, 31)
					//~z~Wait, that's my shit.
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(FMCutData.sSpeech, "FM_1AU", "FM_LAMRAD", "FM_LAMRAD_1", CONV_PRIORITY_VERY_HIGH)
	           			SET_BIT(FMCutData.iSpeechBitset, 31)
					ENDIF
				ENDIF
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_10", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					
					SET_SRL_READAHEAD_TIMES(SRL_READAHEAD_NORMAL_SP_PRESTREAM_MAP,
											SRL_READAHEAD_NORMAL_SP_PRESTREAM_ASSETS,
											SRL_READAHEAD_NORMAL_SP_PLAYBACK_MAP,
											SRL_READAHEAD_NORMAL_SP_PLAYBACK_ASSETS)
					
					IF NOT IS_ENTITY_DEAD(FMCutData.pedIGLamar)
						SET_ENTITY_VISIBLE(FMCutData.pedIGLamar, TRUE)
					ENDIF
					IF NOT IS_ENTITY_DEAD(FMCutData.pedCSLamar)
						SET_ENTITY_VISIBLE(FMCutData.pedCSLamar, FALSE)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					AND NOT IS_ENTITY_DEAD(FMCutData.vehLamar)
						TASK_ENTER_VEHICLE(	PLAYER_PED_ID(), FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, 
											ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_10)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_10)
			
				IF CREATE_VAGOS_FOR_CUTSCENE(FMCutData)
				AND IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_10)
					
					
					SKIP_LAMAR_CAR(FMCutData, 116366.500)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_12
					
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_12
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					NETWORK_OVERRIDE_CLOCK_TIME(00, 00, 00)
					//RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_12", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_12", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>, 0.235, 0.75)
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_12)
					UNLOAD_CLOUD_HAT("CONTRAILS")
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_12)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_12)
					RENDER_SHADOWED_LIGHTS_WITH_NO_SHADOWS(TRUE)
					SKIP_LAMAR_CAR(FMCutData, 127166.500)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_14
					
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_14
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					CLEANUP_VAGOS_FOR_CUTSCENE(FMCutData)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_14", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_14)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_14)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_14)
					RENDER_SHADOWED_LIGHTS_WITH_NO_SHADOWS(FALSE)
					SKIP_LAMAR_CAR(FMCutData, 170800.000)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_17
					
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_17
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_17", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_17)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_17)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_17)
					
					SET_SRL_READAHEAD_TIMES(SRL_READAHEAD_NORMAL_SP_PRESTREAM_MAP,
											SRL_READAHEAD_NORMAL_SP_PRESTREAM_ASSETS,
											SRL_READAHEAD_NORMAL_SP_PLAYBACK_MAP,
											SRL_READAHEAD_NORMAL_SP_PLAYBACK_ASSETS)
				
					SKIP_LAMAR_CAR(FMCutData, 191799.800)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_19
					
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_19
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_19", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_19)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_19)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_19)
					SKIP_LAMAR_CAR(FMCutData, 196633.100)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_20
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_20
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_20", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_20)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_20)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_20)
					SKIP_LAMAR_CAR(FMCutData, 201466.500)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_21
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_21
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_21", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_21)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_21)
				
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_21)
					//SKIP_LAMAR_CAR(FMCutData, 204099.8)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04
				MAINTAIN_ROAD_MODEL_REQUEST(FMCutData)
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					//RUN_SYNC_SCENE_CAM_ATTACH(FMCutData, "LAMAR_CAR_DRIVE_06", "mp_intro_seq@", FMCutData.vehLamar, <<0.0, -2.0, 0.4>>, <<0.0, 0.0, 0.0>>)
					
					
					
					IF NOT IS_ENTITY_DEAD(FMCutData.vehLamar)
						ATTACH_CAM_TO_ENTITY(FMCutData.cutsceneCam, FMCutData.vehLamar, <<0.8056, 2.2245, 0.5533>>)
						POINT_CAM_AT_ENTITY(FMCutData.cutsceneCam, FMCutData.vehLamar, <<-0.4552, -0.4977, 0.5449>>)
						SET_CAM_FOV(FMCutData.cutsceneCam, 33.3479)
						SHAKE_CAM(FMCutData.cutsceneCam, "ROAD_VIBRATION_SHAKE", 2.0)
						SET_CAM_ACTIVE(FMCutData.cutsceneCam, TRUE)
						
						IF NOT IS_ENTITY_DEAD(FMCutData.pedIGLamar)
							SET_ENTITY_VISIBLE(FMCutData.pedIGLamar, FALSE)
						ENDIF
						IF NOT IS_ENTITY_DEAD(FMCutData.pedCSLamar)
							SET_ENTITY_VISIBLE(FMCutData.pedCSLamar, TRUE)
						ENDIF
						
						FMCutData.iLamarSceneID = -1
						FMCutData.iLamarSceneId = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iLamarSceneId, FMCutData.vehLamar, 
															GET_ENTITY_BONE_INDEX_BY_NAME(FMCutData.vehLamar, "seat_dside_f"))
						
						IF IS_INTRO_FEMALE(FMCutData)
							//female synch scene needs to be at front right seat
							FMCutData.iPlayerSceneID = -1
							FMCutData.iPlayerSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iPlayerSceneID, FMCutData.vehLamar, 
																GET_ENTITY_BONE_INDEX_BY_NAME(FMCutData.vehLamar, "seat_pside_f"))
						
							TASK_SYNCHRONIZED_SCENE(FMCutData.pedCSLamar, FMCutData.iLamarSceneID, "mp_intro_seq@ig_1_lamar_drive_radio", 
													"mp_intro_ig_1_p2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), FMCutData.iPlayerSceneID, "mp_intro_seq@ig_1_lamar_drive_female", 
													"ig_1_lamar_drive_female_fem", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iLamarSceneID, 0.726)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iPlayerSceneID, 0.740)
						ELSE
							//male synch scene needs to be at vehicle origin
							FMCutData.iPlayerSceneID = -1
							FMCutData.iPlayerSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(FMCutData.iPlayerSceneID, FMCutData.vehLamar, 0)
							
							TASK_SYNCHRONIZED_SCENE(FMCutData.pedCSLamar, FMCutData.iLamarSceneID, "mp_intro_seq@ig_1_lamar_drive_radio", 
													"mp_intro_ig_1_p2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), FMCutData.iPlayerSceneID, "mp_intro_seq@ig_1_lamar_drive_male", 
													"ig_1_lamar_drive_male_male", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iLamarSceneID, 0.726)
							SET_SYNCHRONIZED_SCENE_PHASE(FMCutData.iPlayerSceneID, 0.815)
						ENDIF
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(FMCutData.pedCSLamar)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04)
						
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_ANIM_04)
					SKIP_LAMAR_CAR(FMCutData, 213066.400)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_24
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_24
				MAINTAIN_ROAD_MODEL_REQUEST(FMCutData)
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_24", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					
					IF NOT IS_ENTITY_DEAD(FMCutData.pedIGLamar)
						SET_ENTITY_VISIBLE(FMCutData.pedIGLamar, TRUE)
					ENDIF
					IF NOT IS_ENTITY_DEAD(FMCutData.pedCSLamar)
						SET_ENTITY_VISIBLE(FMCutData.pedCSLamar, FALSE)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					AND NOT IS_ENTITY_DEAD(FMCutData.vehLamar)
						TASK_ENTER_VEHICLE(	PLAYER_PED_ID(), FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, 
											ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_24)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_24)
			
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_24)
					
					//move to second recording
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchTutorialFromIntroCut)
					
					SKIP_LAMAR_CAR_TURN_RAMP_RECORDING(FMCutData, 10177.750)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_25
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_25
				MAINTAIN_ROAD_MODEL_REQUEST(FMCutData)
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_25", "mp_intro_seq@", <<-1038.78, -2732.61, 19.08>>, <<0.0, 0.0, 0.0>>)
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_25)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_25)
				
				IF PREPARE_MUSIC_EVENT("FM_INTRO_DRIVE_END")
				AND IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_25)
					SKIP_LAMAR_CAR_TURN_PARK_RECORDING(FMCutData, 4500.000)
					#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
					SET_BIT(FMCutData.iBitset, biDelayCam)
					FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_26
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAMAR_CAR_DRIVE_26
				MAINTAIN_ROAD_MODEL_REQUEST(FMCutData)
				IF NOT IS_SCREEN_FADED_IN()
					IF NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
					ENDIF
				ENDIF
			
				IF IS_BIT_SET(FMCutData.iBitset, biDelayCam)
					RUN_SYNC_SCENE_CAM_ORIGIN(FMCutData, "LAMAR_CAR_DRIVE_26", "mp_intro_seq@", <<-197.900, 301.000, 96.400>>, <<0.0, 0.0, 180.0>>)
					
					/*IF IS_BIT_SET(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
						TRIGGER_MUSIC_EVENT("FM_INTRO_DRIVE_END")
						#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("[IntroCue] Playing cue FM_INTRO_DRIVE_END") #ENDIF
						CLEAR_BIT(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
					ENDIF*/
					
					//SRL end the Streaming Request List
					IF FMCutData.bSRLOn
						END_SRL()
						CLEAR_BIT(fmcutdata.iBitset, biDoSRLTime)
					ENDIF
					
					SETUP_AMBIENT_CARS_FOR_CUTSCENE_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_26)
					
					CLEAR_BIT(FMCutData.iBitset, biDelayCam)
				ENDIF
			
				IF NOT IS_BIT_SET(FMCutData.iBitset, biDriveEndMocapLoaded)
					
					IF IS_INTRO_FEMALE(FMCutData)
						REQUEST_CUTSCENE("MP_INT_MCS_5_ALT2")
					ELSE
						REQUEST_CUTSCENE("MP_INT_MCS_5_ALT1")
					ENDIF
					
					IF FMCutData.bRegPlayerOn
						IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
							IF IS_INTRO_FEMALE(FMCutData)
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_Female_Character", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
								//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MP_Female_Character", PLAYER_PED_ID())
							ELSE
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
								//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MP_1", PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
					
	       			IF HAS_CUTSCENE_LOADED()
						SET_BIT(FMCutData.iBitset, biDriveEndMocapLoaded)
					ENDIF
				ENDIF
			
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_26)
				
				IF NOT IS_ENTITY_DEAD(FMCutData.vehLamar)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(FMCutData.vehLamar)
						IF GET_ENTITY_SPEED(FMCutData.vehLamar) < 1
							STOP_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehLamar)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_26)
				AND IS_BIT_SET(FMCutData.iBitset, biDriveEndMocapLoaded)
					
					IF IS_BIT_SET(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
						TRIGGER_MUSIC_EVENT("FM_INTRO_DRIVE_END")
						#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("[IntroCue] Playing cue FM_INTRO_DRIVE_END") #ENDIF
						CLEAR_BIT(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
					ENDIF
					STOP_PLAYBACK_RECORDED_VEHICLE(FMCutData.vehLamar)
					FMCutData.introCutStage = INTRO_CUT_DRIVE_END_MOCAP_START
				/*ELIF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_26, -2000)
					IF NOT IS_BIT_SET(FMCutData.iBitset, biLamarCoronaConvoStart)
						IF CREATE_CONVERSATION (FMCutData.sSpeech, "FM_1AU", "FM_IEND", CONV_PRIORITY_MEDIUM)
	               			SET_BIT(FMCutData.iBitset, biLamarCoronaConvoStart)
						ENDIF
					ENDIF*/
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_DRIVE_END_MOCAP_START
			
				IF FMCutData.bRegPlayerOn
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					
					
						IF IS_INTRO_FEMALE(FMCutData)
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_Female_Character", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(FMCutData.pedIGLamar)
					REGISTER_ENTITY_FOR_CUTSCENE(FMCutData.pedIGLamar, "LAMAR", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF NOT IS_ENTITY_DEAD(FMCutData.vehLamar)
					REGISTER_ENTITY_FOR_CUTSCENE(FMCutData.vehLamar, "MP_Lamar_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
			
				SET_BIT(FMCutData.iBitset, biBlockSettingFocus)
				
				IF IS_AUDIO_SCENE_ACTIVE("MP_INTRO_LAMAR_DRIVE_SCENE")
					STOP_AUDIO_SCENE("MP_INTRO_LAMAR_DRIVE_SCENE")
				ENDIF
				
				START_CUTSCENE(CUTSCENE_DO_NOT_REPOSITION_PLAYER_TO_SCENE_ORIGIN)
	            SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				FMCutData.introCutStage = INTRO_CUT_DRIVE_END_MOCAP_CONFIRM_PLAYING
			BREAK
			
			CASE INTRO_CUT_DRIVE_END_MOCAP_CONFIRM_PLAYING
				IF IS_CUTSCENE_PLAYING()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					IF FMCutData.bRecordOn
						FMCutData.iStartOfDriveEndMocapTime = GET_GAME_TIMER() - FMCutData.iCutStartTime
					ELSE
						FMCutData.iStartOfDriveEndMocapTime = GET_ASSET_TIMER(FMCutData)
					ENDIF
					
					FMCutData.iDeleteStagger = 0
					
					FMCutData.introCutStage = INTRO_CUT_DRIVE_END_MOCAP_PLAYING
				ENDIF
			BREAK
			
			CASE INTRO_CUT_DRIVE_END_MOCAP_PLAYING
				IF FMCutData.bRegPlayerOn
					IF IS_INTRO_FEMALE(FMCutData)
						IF NOT IS_BIT_SET(FMCutData.iBitset, biSetPlayerExitState)
							IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_Female_Character")
								//Do stuff.
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								
								

									SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<-200.8225, 302.9014, 95.9455>>)
									vDaveTemp = (<<-210.3399,307.0503,95.9465>> - <<-200.8225, 302.9014, 95.9455>>)
									SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(vDaveTemp.x, vDaveTemp.y))
									

//									TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), <<-210.3399,307.0503,95.9465>>, PEDMOVE_WALK)
//									FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
//									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									
									SET_GAMEPLAY_CAM_RELATIVE_HEADING()
									SET_GAMEPLAY_CAM_RELATIVE_PITCH()
									SET_BIT(FMCutData.iBitset, biSetPlayerExitState)
									#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Set exit state") #ENDIF
								ENDIF
			                ENDIF
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(FMCutData.iBitset, biSetPlayerExitState)
							IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
								//Do stuff.
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									
									
									SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<-200.8225, 302.9014, 95.9455>>)
									vDaveTemp = (<<-210.3399,307.0503,95.9465>> - <<-200.8225, 302.9014, 95.9455>>)
									SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(vDaveTemp.x, vDaveTemp.y))
									

								

//									TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), <<-210.3399,307.0503,95.9465>>, PEDMOVE_WALK)
//									FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
//									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									
									SET_GAMEPLAY_CAM_RELATIVE_HEADING()
									SET_GAMEPLAY_CAM_RELATIVE_PITCH()
									
									SET_BIT(FMCutData.iBitset, biSetPlayerExitState)
									#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Set exit state") #ENDIF
								ENDIF
			                ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("LAMAR")
					//Do stuff.
					IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
						IF NOT IS_PED_INJURED(FMCutData.pedIGLamar)
							IF NOT IS_PED_IN_ANY_VEHICLE(FMCutData.pedIGLamar)
								TASK_ENTER_VEHICLE(FMCutData.pedIGLamar, FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
							ENDIF
						ENDIF
					ENDIF
                ENDIF		
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_Lamar_Car")
					IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
						SET_VEHICLE_ON_GROUND_PROPERLY(FMCutData.vehLamar)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL(TRUE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
				
				IF NOT HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_ID())
					PRINTLN("{dsw] Intro cut about to call SET_LOCAL_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT - 1")
					//-- Let tutorial know it should set up first camera
					SET_LOCAL_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(TRUE)
				ENDIF
			
				IF IS_CUTSCENE_PLAYING()
					IF FMCutData.bRecordOn
						FMCutData.iCutStartTime = GET_GAME_TIMER() - (FMCutData.iStartOfDriveEndMocapTime + GET_CUTSCENE_TIME())
					ENDIF
					
					//Final delete stagger (as deleting everything at the end causes slowdown)
					
					SWITCH FMCutData.iDeleteStagger
						CASE 0
							CLEANUP_CAR_FOR_AMBIENT(FMCutData, 0)
						BREAK
						CASE 10
							CLEANUP_CAR_FOR_AMBIENT(FMCutData, 1)
						BREAK
						CASE 20
							CLEANUP_CAR_FOR_AMBIENT(FMCutData, 2)
						BREAK
						CASE 30
							CLEANUP_CAR_FOR_AMBIENT(FMCutData, 3)
						BREAK
						CASE 40
							CLEANUP_CAR_FOR_AMBIENT(FMCutData, 4)
						BREAK
						CASE 50
							CLEANUP_CAR_FOR_AMBIENT(FMCutData, 5)
						BREAK
						CASE 60
							CLEANUP_CAR_FOR_AMBIENT(FMCutData, 6)
						BREAK
						CASE 70
							CLEANUP_CAR_FOR_AMBIENT(FMCutData, 7)
						BREAK
						CASE 80
							CLEANUP_CAR_FOR_AMBIENT(FMCutData, 8)
						BREAK
						CASE 90
							CLEANUP_CAR_FOR_AMBIENT(FMCutData, 9)
						BREAK
						CASE 100
							IF DOES_ENTITY_EXIST(FMCutData.pedCSLamar)
								DELETE_PED(FMCutData.pedCSLamar)
							ENDIF
						BREAK
					ENDSWITCH
					FMCutData.iDeleteStagger++
					
					//End final delete stagger
					
				ELSE
					
					DISABLE_CINEMATIC_VEHICLE_IDLE_MODE_THIS_UPDATE()
					INVALIDATE_CINEMATIC_VEHICLE_IDLE_MODE()
					
					IF NOT HAS_CUTSCENE_FINISHED()
						STOP_CUTSCENE()
					ENDIF
					
				    SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					CLEAR_BIT(FMCutData.iBitset, biBlockSettingFocus)
					
					FMCutData.introCutStage = INTRO_CUT_FINISH
				ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_SKIP_START
			
				IF IS_CUTSCENE_PLAYING()
					//STOP_CUTSCENE()
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === STOP_CUTSCENE_IMMEDIATELY()")
					#ENDIF
					STOP_CUTSCENE_IMMEDIATELY()
				ELSE
					IF NOT IS_BIT_SET(FMCutData.iBitset, biFadedOutFinalSkip)
						IF IS_SCREEN_FADED_OUT()
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							SET_BIT(FMCutData.iBitset, biBlockSettingFocus)
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-195.2000, 307.1825, 95.9455>>)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
							
							//SRL end the Streaming Request List
							IF FMCutData.bSRLOn
								END_SRL()
								CLEAR_BIT(fmcutdata.iBitset, biDoSRLTime)
							ENDIF
							
							RESET_NET_TIMER(FMCutData.iStreamVolTimer)
							START_NET_TIMER(FMCutData.iStreamVolTimer)
							SET_BIT(FMCutData.iBitset, biFadedOutFinalSkip)
						ELSE
							IF NOT IS_SCREEN_FADING_OUT()
								DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
							ENDIF
						ENDIF
					ELSE
					
						IF HAS_NET_TIMER_EXPIRED(FMCutData.iStreamVolTimer, 10000)
							IF CREATE_LAMAR_AND_VAN_FOR_SKIP(FMCutData)
								
								CLEANUP_HELI_FLYPAST(FMCutData)
								
								CLEANUP_AIRPORT_JETS(FMCutData)
								
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
									IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
										
										SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<-200.8225, 302.9014, 95.9455>>)
										SET_ENTITY_HEADING(PLAYER_PED_ID(), 61.3164)
										
										//IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
										//	TASK_ENTER_VEHICLE(PLAYER_PED_ID(), FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
										//ENDIF
									ENDIF
								ENDIF
								
								IF NOT IS_PED_INJURED(FMCutData.pedIGLamar)
									IF NOT IS_PED_IN_ANY_VEHICLE(FMCutData.pedIGLamar)
										IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
											TASK_ENTER_VEHICLE(FMCutData.pedIGLamar, FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
										ENDIF
									ENDIF
								ENDIF
								
								SET_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL(TRUE)
								
								IF NOT HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_ID())
									//-- Let tutorial know it should set up first camera
									SET_LOCAL_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(TRUE)
								ENDIF
								
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
								
								NETWORK_OVERRIDE_CLOCK_TIME(00, 00, 00)
								
								#IF IS_DEBUG_BUILD
								g_bPlayerSkippedIntro = TRUE
								#ENDIF
								
								IF IS_BIT_SET(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
									TRIGGER_MUSIC_EVENT("FM_INTRO_DRIVE_END")
									#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("[IntroCue] Playing cue FM_INTRO_DRIVE_END") #ENDIF
									CLEAR_BIT(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
								ENDIF
								
								SET_BIT(FMCutData.iBitset, biCutWasSkipped)
								SET_BIT(FMCutData.iBitset, biDelayCam)
								FMCutData.introCutStage = INTRO_CUT_FINISH
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
						
			CASE INTRO_CUT_SKIP_DAVE
			
				IF IS_CUTSCENE_PLAYING()
					//STOP_CUTSCENE()
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === STOP_CUTSCENE_IMMEDIATELY()")
					#ENDIF
					STOP_CUTSCENE_IMMEDIATELY()
				ELSE
					IF NOT IS_BIT_SET(FMCutData.iBitset, biFadedOutFinalSkip)
						IF IS_SCREEN_FADED_OUT()
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							SET_BIT(FMCutData.iBitset, biBlockSettingFocus)
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-195.2000, 307.1825, 95.9455>>)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
							
							//SRL end the Streaming Request List
							IF FMCutData.bSRLOn
								END_SRL()
								CLEAR_BIT(fmcutdata.iBitset, biDoSRLTime)
							ENDIF
							
							RESET_NET_TIMER(FMCutData.iStreamVolTimer)
							START_NET_TIMER(FMCutData.iStreamVolTimer)
							SET_BIT(FMCutData.iBitset, biFadedOutFinalSkip)
						ELSE
							IF NOT IS_SCREEN_FADING_OUT()
								DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
							ENDIF
						ENDIF
					ELSE
					
						IF HAS_NET_TIMER_EXPIRED(FMCutData.iStreamVolTimer, 10000)
							IF CREATE_LAMAR_AND_VAN(FMCutData)
								
								CLEANUP_HELI_FLYPAST(FMCutData)
								
								CLEANUP_AIRPORT_JETS(FMCutData)
								
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
									IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
										
										SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<-200.8225, 302.9014, 95.9455>>)
										SET_ENTITY_HEADING(PLAYER_PED_ID(), 61.3164)
										
										IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
											TASK_ENTER_VEHICLE(PLAYER_PED_ID(), FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK, ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
										ENDIF
									ENDIF
								ENDIF
								
								IF NOT IS_PED_INJURED(FMCutData.pedIGLamar)
									IF NOT IS_PED_IN_ANY_VEHICLE(FMCutData.pedIGLamar)
										IF IS_VEHICLE_DRIVEABLE(FMCutData.vehLamar)
											TASK_ENTER_VEHICLE(FMCutData.pedIGLamar, FMCutData.vehLamar, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_WARP_PED|ECF_BLOCK_SEAT_SHUFFLING)
										ENDIF
									ENDIF
								ENDIF
								
								//SET_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL(TRUE)
								
								IF NOT HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_ID())
									//-- Let tutorial know it should set up first camera
									//SET_LOCAL_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(TRUE)
								ENDIF
								
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
								
								#IF IS_DEBUG_BUILD
								g_bPlayerSkippedIntro = TRUE
								#ENDIF
								
								IF IS_BIT_SET(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
									TRIGGER_MUSIC_EVENT("FM_INTRO_DRIVE_END")
									#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("[IntroCue] Playing cue FM_INTRO_DRIVE_END") #ENDIF
									CLEAR_BIT(FMCutData.iBitset, biNeedToCallMusicDriveEnd)
								ENDIF
								
								DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
								
								NETWORK_OVERRIDE_CLOCK_TIME(00, 00, 00)
								//move to second recording
								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchTutorialFromIntroCut)
								
								FMCutData.iCutStartTime = GET_GAME_TIMER() - GET_CUTSCENE_TIME_FOR_STAGE(FMCutData, INTRO_CUT_LAMAR_CAR_DRIVE_24)
								FMCutData.bRecordOn = TRUE
								
								SKIP_LAMAR_CAR_TURN_RAMP_RECORDING(FMCutData, 10177.750)
								#IF IS_DEBUG_BUILD RESET_JSKIP(FMCutData) #ENDIF
								SET_BIT(FMCutData.iBitset, biDelayCam)
								FMCutData.introCutStage = INTRO_CUT_LAMAR_CAR_DRIVE_25
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
						
			// Clean up cutscene it's finishing now
			CASE INTRO_CUT_FINISH
				
				MANAGE_CUTSCENE_TIME_DELAY(FMCutData, INTRO_CUT_FINISH)
			
				//IF IS_CUTSCENE_TIME_UP(FMCutData, INTRO_CUT_FINISH)
				//OR IS_BIT_SET(FMCutData.iBitset, biCutWasSkipped)
						
					BOOL bTurnOffPlayerControl 
					bTurnOffPlayerControl = (NOT(FMCutData.bLaunchFmIntro))
					
					#IF IS_DEBUG_BUILD 
						IF IS_BIT_SET(FMCutData.iBitset, biSkipFinalScene)
							bTurnOffPlayerControl = FALSE
							PRINT_INTRO_STRING("biSkipFinalScene is set!")
						ENDIF
						IF bTurnOffPlayerControl
							PRINT_INTRO_STRING("About to call CLEANUP_FREEMODE_INTRO_CUTSCENE, bTurnOffPlayerControl is True ") 
						ELSE
							PRINT_INTRO_STRING("About to call CLEANUP_FREEMODE_INTRO_CUTSCENE, bTurnOffPlayerControl is False ") 
						ENDIF
					#ENDIF 
					CLEANUP_FREEMODE_INTRO_CUTSCENE(FMCutData, bTurnOffPlayerControl)
				//	CLEANUP_MP_CUTSCENE(FALSE)
					#IF IS_DEBUG_BUILD 
						IF IS_BIT_SET(FMCutData.iBitset, biSkipFinalScene)
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							PRINT_INTRO_STRING("Clearing player tasks as biSkipFinalScene is set ") 
						ENDIF
					#ENDIF
					
					//SRL end the Streaming Request List
					IF FMCutData.bSRLOn
						END_SRL()
						CLEAR_BIT(fmcutdata.iBitset, biDoSRLTime)
					ENDIF
					
					IF NOT IS_PED_INJURED(FMCutData.pedIGLamar)
						TASK_STAND_STILL(FMCutData.pedIGLamar, -1)
						SET_PED_KEEP_TASK(FMCutData.pedIGLamar, TRUE)
					ENDIF
					NETWORK_SET_VOICE_ACTIVE(TRUE)
					#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Called NETWORK_SET_VOICE_ACTIVE(TRUE)") #ENDIF 
					IF NOT FMCutData.bLaunchFmIntro
						#IF IS_DEBUG_BUILD
							PRINT_INTRO_STRING("RUN_FREEMODE_INTRO_CUTSCENE: MP_STAT_FM_INTRO_CUT_DONE = TRUE") 
							PRINT_INTRO_STRING(" Intro cut Moving to INTO_CUT_DONE")
						#ENDIF
						SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_CUT_DONE, TRUE)
//						SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_INTRO_CUT_DONE)
						
						RETURN TRUE
					ENDIF
				//ENDIF
				
			BREAK
			
			
			CASE INTRO_CUT_LAUNCH_GUN_INTRO
				
			BREAK
			
			
			CASE INTRO_CUT_TEST
				
			BREAK
			
			
			CASE INTO_CUT_DONE
				RETURN TRUE
			BREAK
	
	
		ENDSWITCH
		
		MANAGE_TENNIS_LIGHTS(FMCutData)
		
		//SRL update the Streaming Request List Timer
		IF NOT FMCutData.bStallSRLTimerUpdate
			IF FMCutData.bRecordOn
				FMCutData.fLastSRLTimePassed = TO_FLOAT(GET_GAME_TIMER() - FMCutData.iCutStartTime)
			ELSE
				FMCutData.fLastSRLTimePassed = TO_FLOAT(GET_ASSET_TIMER(FMCutData))
			ENDIF
			
			FMCutData.fLastSRLTimeAdjust = GET_CURRENT_SRL_ADJUST(FMCutData)	//Adjust for old SRL recording. redundant now as we're getting a new recording.
			
		ENDIF
		FMCutData.bStallSRLTimerUpdate = FALSE
		
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT()%15 = 0
				CPRINTLN(DEBUG_NET_AMBIENT, "=|=|============================ DAMN YOU SRL ==================================")
				CPRINTLN(DEBUG_NET_AMBIENT, "=|=|= SET_SRL_TIME(", FMCutData.fLastSRLTimePassed, ")")
				CPRINTLN(DEBUG_NET_AMBIENT, "=|=|= introCutStage = ", GET_INTRO_CUT_STAGE_NAME(FMCutData.introCutStage))
				CPRINTLN(DEBUG_NET_AMBIENT, "=|=|= GET_CUTSCENE_TIME_FOR_STAGE = ", GET_CUTSCENE_TIME_FOR_STAGE(FMCutData, FMCutData.introCutStage))
				CPRINTLN(DEBUG_NET_AMBIENT, "=|=|= iCutStartTime = ", FMCutData.iCutStartTime)
				CPRINTLN(DEBUG_NET_AMBIENT, "=|=|= iStartOfMocapTime = ", FMCutData.iStartOfMocapTime)
				CPRINTLN(DEBUG_NET_AMBIENT, "=|=|= iStartOfMocapSRLAdjust = ", FMCutData.iStartOfMocapSRLAdjust)
				CPRINTLN(DEBUG_NET_AMBIENT, "=|=|= iEndOfMocapTime = ", FMCutData.iEndOfMocapTime)
				CPRINTLN(DEBUG_NET_AMBIENT, "=|=|= iEndOfMocapSRLAdjust = ", FMCutData.iEndOfMocapSRLAdjust)
				IF DOES_CAM_EXIST(GET_RENDERING_CAM())
					VECTOR vSRLCAMPos = GET_CAM_COORD(GET_RENDERING_CAM())
					VECTOR vSRLCAMRot = GET_CAM_ROT(GET_RENDERING_CAM())
					FLOAT fSRLCAMFov = GET_CAM_FOV(GET_RENDERING_CAM())
					CPRINTLN(DEBUG_NET_AMBIENT, "=|=|= CAMERA : ", vSRLCAMPos, ", ", vSRLCAMRot, ", ", fSRLCAMFov)
				ENDIF
				CPRINTLN(DEBUG_NET_AMBIENT, "=|=|============================ YOU WILL PAY ==================================")
			ENDIF
		#ENDIF
		
		IF FMCutData.bSRLOn
		AND IS_BIT_SET(FMCutData.iBitset, biDoSRLTime)
			IF FMCutData.bRecordOn
				SET_SRL_TIME(FMCutData.fLastSRLTimePassed)
			ELSE
				SET_SRL_TIME(FMCutData.fLastSRLTimeAdjust)
			ENDIF
		ENDIF
		
		//1649401
		STAGGER_CATCH_FALLING_AMBIENT_CARS(FMCutData)
		
		//-- Splash start, 1325792
		IF IS_BIT_SET(FMCutData.iBitset, biRunSplashStart)
			IF LOAD_GTAO_LOGO()
				DRAW_GTAO_LOGO()
			ENDIF
		ENDIF
				
		IF FMCutData.introCutStage > INTRO_CUT_INIT
		AND FMCutData.introCutStage <= INTRO_CUT_FINISH
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS|SPC_REMOVE_PROJECTILES|SPC_REMOVE_FIRES|SPC_CLEAR_TASKS)
					#IF IS_DEBUG_BUILD
						PRINT_INTRO_STRING(" Turning player control off")
					#ENDIF
				ENDIF
			ENDIF
			
			MAINTAIN_ADD_TO_TUTORIAL_SESSION(FMCutData)
			
			DISABLE_FRONTEND_THIS_FRAME() 
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
		ENDIF
				
		IF MPGlobalsAmbience.bFMIntroCutDemandingDpadDownActive
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		ENDIF
		
	ELSE	
		#IF IS_DEBUG_BUILD
			PRINT_INTRO_STRING("Player injured!")
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

