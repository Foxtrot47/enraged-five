USING "rage_builtins.sch"
USING "globals.sch"
USING "net_gang_boss_common.sch"


//╔═════════════════════════════════════════════════════════════════════════════╗
//║				AVENGER WANTED RATINGS											║

// iAVEBitSet
CONST_INT ciAVE_WANTED_NEEDS_CLEARED 	0
CONST_INT ciAVE_WANTED_HAD_WANTED		1

STRUCT AVE_WANTED_VARS
	SCRIPT_TIMER stAVEWantedTimer
	SCRIPT_TIMER stAVEBustedTimer
	INT iBustedWarpProgress
	INT iRemoteWantedLevel
	BOOL bWipeAVEWanted
	INT iAVEBitSet
	INT iAVEWipeBitSet
	INT iParticipantCabDriver = -1
	INT iTurretBit	
	#IF IS_DEBUG_BUILD
	BOOL bSpamPrints
	#ENDIF
ENDSTRUCT

AVE_WANTED_VARS sAveVars

STRUCT AVE_WANTED_VARS_NO_RESET
	INT iAVENRBitSet 
	SCRIPT_TIMER stAVEFailsafeTimer
	BOOL bResettingInProgress
ENDSTRUCT

AVE_WANTED_VARS_NO_RESET sNoResetAveVars

// iAVENRBitSet
CONST_INT ciAVE_WANTED_SWAT_CONFIG_FLAG 0

PROC START_AVENGER_BUSTED_TIMER()
	IF NOT HAS_NET_TIMER_STARTED(sAveVars.stAVEBustedTimer)
		START_NET_TIMER(sAveVars.stAVEBustedTimer)
		PRINTLN("    ->    [AVE_WANTED] [BUSTED_TIMER] START_AVENGER_BUSTED_TIMER ")
	ENDIF
ENDPROC

PROC RESET_AVENGER_BUSTED_TIMER()
	IF HAS_NET_TIMER_STARTED(sAveVars.stAVEBustedTimer)
		RESET_NET_TIMER(sAveVars.stAVEBustedTimer)
		PRINTLN("  [AVE_WANTED] [BUSTED_TIMER] RESET_AVENGER_BUSTED_TIMER ")
	ENDIF
ENDPROC

PROC WIPE_AVENGER_WANTED_RATINGS()
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			PRINTLN("  [AVE_WANTED] [BUSTED_TIMER] WIPE_AVENGER_WANTED_RATINGS ")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ANY_LOAD_SCENE_LOADED()

	IF IS_NEW_LOAD_SCENE_ACTIVE()
		IF IS_NEW_LOAD_SCENE_LOADED()
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE // No load scene active, return true
	ENDIF
	
	RETURN FALSE //If load scene is active but hasn't loaded, it will return false here

ENDFUNC

PROC HANDLE_AVENGER_BUSTED_WARP()

	//PRINTLN("  4283246 HANDLE_AVENGER_BUSTED_WARP, STUCK ", sAveVars.iBustedWarpProgress)

	SWITCH sAveVars.iBustedWarpProgress
		CASE 0
			IF HAS_NET_TIMER_STARTED(sAveVars.stAVEBustedTimer)
			AND HAS_NET_TIMER_EXPIRED(sAveVars.stAVEBustedTimer, 30000)
			AND IS_SCREEN_FADED_IN()
				IF IS_LOCAL_PLAYER_USING_DRONE()
					RETURN_DRONE_TO_PLAYER(TRUE)
				ENDIF	
				IF IS_LOCAL_PLAYER_USING_VEHICLE_WEAPON()
					SET_KILL_VEHICLE_WEAPON_SCRIPT(TRUE)
				ENDIF
				
				IF NOT IS_LOCAL_PLAYER_USING_VEHICLE_WEAPON()
				AND NOT IS_LOCAL_PLAYER_USING_DRONE()
					g_bBustedWarpInProgress = TRUE
			
					IF SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_CUSTOM_FAILED, INVALID_PLAYER_INDEX(), -1, "", "GB_SHARD_BUST", DEFAULT, DEFAULT, DEFAULT, BIG_MESSAGE_BIT_BUSTED)					
						sAveVars.iBustedWarpProgress++
						PRINTLN("  [AVE_WANTED] [BUSTED_TIMER] - HANDLE_AVENGER_BUSTED_WARP, > ", sAveVars.iBustedWarpProgress)
					
					ENDIF
				ENDIF	
			ENDIF
		
		BREAK
		
		CASE 1
			IF IS_SCREEN_FADED_IN()
			AND IS_BIG_MESSAGE_BEING_DRAWN()
				DO_SCREEN_FADE_OUT(500)
				sAveVars.iBustedWarpProgress++
				PRINTLN("  [AVE_WANTED] [BUSTED_TIMER] - HANDLE_AVENGER_BUSTED_WARP, > ", sAveVars.iBustedWarpProgress)
			ENDIF
		BREAK
		
		CASE 2
			IF IS_SCREEN_FADED_OUT()
				IF HAS_ANY_LOAD_SCENE_LOADED()
					RESET_NET_TIMER(sNoResetAveVars.stAVEFailsafeTimer)
					IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
						CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_CUSTOM_FAILED)
					ENDIF
					WIPE_AVENGER_WANTED_RATINGS()
					IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_NEAREST_POLICE_STATION)
						DO_SCREEN_FADE_IN(500)
						sAveVars.iBustedWarpProgress++
						
						PRINTLN("  [AVE_WANTED] [BUSTED_TIMER] - HANDLE_AVENGER_BUSTED_WARP, > ", sAveVars.iBustedWarpProgress)
					ENDIF
				ELSE
					// url:bugstar:3649025
					IF NOT HAS_NET_TIMER_STARTED(sNoResetAveVars.stAVEFailsafeTimer)
						START_NET_TIMER(sNoResetAveVars.stAVEFailsafeTimer)
						PRINTLN("  [AVE_WANTED] [BUSTED_TIMER] - HANDLE_AVENGER_BUSTED_WARP, START_NET_TIMER(sNoResetAveVars.stAVEFailsafeTimer) ")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sNoResetAveVars.stAVEFailsafeTimer, 20000)
							PRINTLN("  [AVE_WANTED] [BUSTED_TIMER] - HANDLE_AVENGER_BUSTED_WARP, CLEANUP_LOAD_SCENE ")
							CLEAR_FOCUS()
							NEW_LOAD_SCENE_STOP()
						ENDIF
					ENDIF
				ENDIF
			ELIF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
			ENDIF
		BREAK
		
		CASE 3
		
			RESET_AVENGER_BUSTED_TIMER()
			WIPE_AVENGER_WANTED_RATINGS()
			
			sAveVars.iBustedWarpProgress = 0
			START_AVE_COOLDOWN_TIMER()
			g_bBustedWarpInProgress = FALSE
			
			PRINTLN("  [AVE_WANTED] [BUSTED_TIMER] - HANDLE_AVENGER_BUSTED_WARP, DONE ", sAveVars.iBustedWarpProgress)
		BREAK

	ENDSWITCH
ENDPROC

CONST_INT ciCAB_ID 		0
CONST_INT ciTRAILER_ID 	1

FUNC VEHICLE_INDEX GET_AVE_CAB_ID(BOOL bAvenger, BOOL bHackerTruck)
	IF bAvenger 
		RETURN g_viAvengerRemoteVehIndex
	ENDIF
	IF bHackerTruck 
		RETURN g_viHackerTruckRemoteVehIndex
	ENDIF
	
	RETURN INT_TO_NATIVE(VEHICLE_INDEX, -1)
ENDFUNC

PROC SET_AVE_WANTED_CLEARED_FLAG(INT iTempWanted, INT iPlayer)
	IF iTempWanted <> -1
		IF iTempWanted = 0
//		AND g_iInteriorTurretSeat = -1 // Not using turret
			IF IS_BIT_SET(sAveVars.iAVEWipeBitSet, iPlayer)
				CLEAR_BIT(sAveVars.iAVEWipeBitSet, iPlayer)
				sAveVars.bWipeAVEWanted = TRUE
				PRINTLN("    ->     [3672063] GRAB_REMOTE_AVE_WANTED_RATINGS, SET_AVE_WANTED_CLEARED_FLAG - sAveVars.bWipeAVEWanted = TRUE ")
				PRINTLN("3924326,sAveVars.bWipeAVEWanted = TRUE 1 ")
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(sAveVars.iAVEWipeBitSet, iPlayer)
				SET_BIT(sAveVars.iAVEWipeBitSet, iPlayer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL LOCAL_PLAYER_IN_FRONT_CAB(BOOL bAvenger, BOOL bHackerTruck)
	
	VEHICLE_INDEX pedVeh = GET_AVE_CAB_ID(bAvenger, bHackerTruck)
	
	IF IS_ENTITY_ALIVE(pedVeh)
		IF IS_ENTITY_A_VEHICLE(pedVeh)
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), pedVeh)
			RETURN TRUE
		ENDIF	
	ENDIF

	RETURN FALSE
ENDFUNC

//url:bugstar:4105176
PROC TOGGLE_RESET_FLAG(BOOL bOn)
	IF bOn
		IF !sNoResetAveVars.bResettingInProgress
			sNoResetAveVars.bResettingInProgress = TRUE
			PRINTLN("  [AVE_WANTED] TOGGLE_RESET_FLAG, sNoResetAveVars.bResettingInProgress = TRUE ")
		ENDIF
	ELSE
		IF sNoResetAveVars.bResettingInProgress
			sNoResetAveVars.bResettingInProgress = FALSE
			PRINTLN("  [AVE_WANTED] TOGGLE_RESET_FLAG, sNoResetAveVars.bResettingInProgress = TRUE ")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL LOCAL_PLAYER_USING_AVENGER()
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR LOCAL_PLAYER_IN_FRONT_CAB(TRUE, FALSE)
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL LOCAL_PLAYER_USING_HACKER_TRUCK()
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	OR LOCAL_PLAYER_IN_FRONT_CAB(FALSE, TRUE)
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC GRAB_REMOTE_AVE_WANTED_RATINGS(PLAYER_INDEX PlayerID)

	INT iTempWanted = -1
	INT iPlayer
	
	IF LOCAL_PLAYER_USING_AVENGER()
		
			iPlayer = NATIVE_TO_INT(PlayerID)

			IF iPlayer != -1

				IF NOT sNoResetAveVars.bResettingInProgress
				AND !IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner)
				
					IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PlayerID)	
					
						IF g_ownerOfArmoryAircraftPropertyIAmIn = globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner //url:bugstar:4095675
						OR GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(GET_AVE_CAB_ID(TRUE, FALSE)) = globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner

							iTempWanted = GET_PLAYER_WANTED_LEVEL(PlayerId)
							
							SET_AVE_WANTED_CLEARED_FLAG(iTempWanted, iPlayer)
							
							#IF IS_DEBUG_BUILD
							PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS, IS_PLAYER_IN_ARMORY_AIRCRAFT - iTempWanted = ", iTempWanted, " PlayerId = ", 
									GET_PLAYER_NAME(PlayerId), " GET_AVE_CAB_ID(TRUE, FALSE) = ", NATIVE_TO_INT(GET_AVE_CAB_ID(TRUE, FALSE)))
							IF IS_NET_PLAYER_OK(g_ownerOfArmoryAircraftPropertyIAmIn, FALSE)
								PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS, IS_PLAYER_IN_ARMORY_AIRCRAFT - g_ownerOfArmoryAircraftPropertyIAmIn = ", GET_PLAYER_NAME(g_ownerOfArmoryAircraftPropertyIAmIn))
							ENDIF
							IF IS_NET_PLAYER_OK(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner, FALSE)
								PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS, IS_PLAYER_IN_ARMORY_AIRCRAFT - GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT = ", GET_PLAYER_NAME(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner))
							ENDIF
							IF IS_NET_PLAYER_OK(GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(GET_AVE_CAB_ID(TRUE, FALSE)))
								PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS, IS_PLAYER_IN_ARMORY_AIRCRAFT - GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT = ", GET_PLAYER_NAME(GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(GET_AVE_CAB_ID(TRUE, FALSE))))
							ENDIF
							#ENDIF
		//				ELSE
		//
		//					PRINTLN("    ->     !!!GRAB_REMOTE_AVE_WANTED_RATINGS, g_ownerOfArmoryAircraftPropertyIAmIn = ", NATIVE_TO_INT(g_ownerOfArmoryAircraftPropertyIAmIn))
		//					PRINTLN("    ->     !!!GRAB_REMOTE_AVE_WANTED_RATINGS, propertyOwner = ", NATIVE_TO_INT(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner))
						ENDIF
				
					ENDIF
					
					IF DOES_ENTITY_EXIST(GET_AVE_CAB_ID(TRUE, FALSE))
						IF NOT IS_ENTITY_DEAD(GET_AVE_CAB_ID(TRUE, FALSE))
							IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(PlayerID), GET_AVE_CAB_ID(TRUE, FALSE))
								iTempWanted = GET_PLAYER_WANTED_LEVEL(PlayerId)
								PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS - iTempWanted = ", iTempWanted)
								
								SET_AVE_WANTED_CLEARED_FLAG(iTempWanted, iPlayer)
								
								sAveVars.iParticipantCabDriver = NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX(PlayerId) )
								PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS - sAveVars.iParticipantCabDriver = ", sAveVars.iParticipantCabDriver)
							ENDIF
						ENDIF
					ENDIF
					
					IF iTempWanted > 0
					AND sAveVars.iRemoteWantedLevel <> iTempWanted 
						sAveVars.iRemoteWantedLevel = iTempWanted
						
						PRINTLN("  [AVE_WANTED] GRAB_REMOTE_AVE_WANTED_RATINGS - sAveVars.iRemoteWantedLevel = ", sAveVars.iRemoteWantedLevel, " from ", GET_PLAYER_NAME(PlayerID))
					ENDIF
				
				ELSE
					IF sAveVars.iRemoteWantedLevel <> 0
						sAveVars.iRemoteWantedLevel = 0
						PRINTLN("  [AVE_WANTED] GRAB_REMOTE_AVE_WANTED_RATINGS - bResettingInProgress, sAveVars.iRemoteWantedLevel = ", sAveVars.iRemoteWantedLevel)
					ENDIF
					
					IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner)
						PRINTLN("  [AVE_WANTED] GRAB_REMOTE_AVE_WANTED_RATINGS - IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE = ", GET_PLAYER_NAME(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner))
					ENDIF
				
					PRINTLN("  [AVE_WANTED] GRAB_REMOTE_AVE_WANTED_RATINGS - sNoResetAveVars.bResettingInProgress = ", sNoResetAveVars.bResettingInProgress)
				ENDIF
			
			ENDIF
	ELIF LOCAL_PLAYER_USING_HACKER_TRUCK()
		
			iPlayer = NATIVE_TO_INT(PlayerID)

			IF iPlayer != -1

				IF NOT sNoResetAveVars.bResettingInProgress
				AND !IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner)
				
					IF IS_PLAYER_IN_HACKER_TRUCK(PlayerID)	
					
						IF g_OwnerOfHackerTruckPropertyIAmIn = globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner //url:bugstar:4095675
						OR GET_OWNER_OF_PERSONAL_HACKER_TRUCK(GET_AVE_CAB_ID(TRUE, FALSE)) = globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner

							iTempWanted = GET_PLAYER_WANTED_LEVEL(PlayerId)
							
							SET_AVE_WANTED_CLEARED_FLAG(iTempWanted, iPlayer)
							
							#IF IS_DEBUG_BUILD
							PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS, IS_PLAYER_IN_ARMORY_AIRCRAFT - iTempWanted = ", iTempWanted, " PlayerId = ", 
									GET_PLAYER_NAME(PlayerId), " GET_AVE_CAB_ID(FALSE, TRUE) = ", NATIVE_TO_INT(GET_AVE_CAB_ID(FALSE, TRUE)))
							IF IS_NET_PLAYER_OK(g_OwnerOfHackerTruckPropertyIAmIn, FALSE)
								PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS, IS_PLAYER_IN_ARMORY_AIRCRAFT - g_OwnerOfHackerTruckPropertyIAmIn = ", GET_PLAYER_NAME(g_OwnerOfHackerTruckPropertyIAmIn))
							ENDIF
							IF IS_NET_PLAYER_OK(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner, FALSE)
								PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS, IS_PLAYER_IN_ARMORY_AIRCRAFT - GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT = ", GET_PLAYER_NAME(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner))
							ENDIF
							IF IS_NET_PLAYER_OK(GET_OWNER_OF_PERSONAL_HACKER_TRUCK(GET_AVE_CAB_ID(FALSE, TRUE)))
								PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS, IS_PLAYER_IN_ARMORY_AIRCRAFT - GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT = ", GET_PLAYER_NAME(GET_OWNER_OF_PERSONAL_HACKER_TRUCK(GET_AVE_CAB_ID(FALSE, TRUE))))
							ENDIF
							#ENDIF
		//				ELSE
		//
		//					PRINTLN("    ->     !!!GRAB_REMOTE_AVE_WANTED_RATINGS, g_OwnerOfHackerTruckPropertyIAmIn = ", NATIVE_TO_INT(g_OwnerOfHackerTruckPropertyIAmIn))
		//					PRINTLN("    ->     !!!GRAB_REMOTE_AVE_WANTED_RATINGS, propertyOwner = ", NATIVE_TO_INT(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner))
						ENDIF
				
					ENDIF
					
					IF DOES_ENTITY_EXIST(GET_AVE_CAB_ID(FALSE, TRUE))
						IF NOT IS_ENTITY_DEAD(GET_AVE_CAB_ID(FALSE, TRUE))
							IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(PlayerID), GET_AVE_CAB_ID(FALSE, TRUE))
								iTempWanted = GET_PLAYER_WANTED_LEVEL(PlayerId)
								PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS - iTempWanted = ", iTempWanted)
								
								SET_AVE_WANTED_CLEARED_FLAG(iTempWanted, iPlayer)
								
								sAveVars.iParticipantCabDriver = NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX(PlayerId) )
								PRINTLN("    ->     GRAB_REMOTE_AVE_WANTED_RATINGS - sAveVars.iParticipantCabDriver = ", sAveVars.iParticipantCabDriver)
							ENDIF
						ENDIF
					ENDIF
					
					IF iTempWanted > 0
					AND sAveVars.iRemoteWantedLevel <> iTempWanted 
						sAveVars.iRemoteWantedLevel = iTempWanted
						
						PRINTLN("  [AVE_WANTED] GRAB_REMOTE_AVE_WANTED_RATINGS - sAveVars.iRemoteWantedLevel = ", sAveVars.iRemoteWantedLevel, " from ", GET_PLAYER_NAME(PlayerID))
					ENDIF
				
				ELSE
					IF sAveVars.iRemoteWantedLevel <> 0
						sAveVars.iRemoteWantedLevel = 0
						PRINTLN("  [AVE_WANTED] GRAB_REMOTE_AVE_WANTED_RATINGS - bResettingInProgress, sAveVars.iRemoteWantedLevel = ", sAveVars.iRemoteWantedLevel)
					ENDIF
					
					IF IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner)
						PRINTLN("  [AVE_WANTED] GRAB_REMOTE_AVE_WANTED_RATINGS - IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB = ", GET_PLAYER_NAME(globalPlayerBD[iPlayer].SimpleInteriorBD.propertyOwner))
					ENDIF
				
					PRINTLN("  [AVE_WANTED] GRAB_REMOTE_AVE_WANTED_RATINGS - sNoResetAveVars.bResettingInProgress = ", sNoResetAveVars.bResettingInProgress)
				ENDIF
			
			ENDIF
	ELSE
		IF sAveVars.iRemoteWantedLevel <> 0
			sAveVars.iRemoteWantedLevel = 0
			PRINTLN("  [AVE_WANTED] GRAB_REMOTE_AVE_WANTED_RATINGS - !LOCAL_PLAYER_USING_AVENGER, sAveVars.iRemoteWantedLevel = ", sAveVars.iRemoteWantedLevel)
		ENDIF
	ENDIF
ENDPROC


PROC RESET_AVE_WANTED_TIMER()
	IF HAS_NET_TIMER_STARTED(sAveVars.stAVEWantedTimer)
		RESET_NET_TIMER(sAveVars.stAVEWantedTimer)
		PRINTLN("  [AVE_WANTED] RESET_AVE_WANTED_TIMER, stAVEWantedTimer ")
	ENDIF
ENDPROC

PROC TOGGLE_FLASH_FLAG(BOOL bFlashing)
	IF bFlashing
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].iGeneralBitSet, ciWANTED_FLASHING)
			PRINTLN("  [AVE_WANTED] TOGGLE_FLASH_FLAG, TRUE ")
			SET_BIT(GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].iGeneralBitSet, ciWANTED_FLASHING)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].iGeneralBitSet, ciWANTED_FLASHING)
			PRINTLN("  [AVE_WANTED] TOGGLE_FLASH_FLAG, FALSE ")
			CLEAR_BIT(GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].iGeneralBitSet, ciWANTED_FLASHING)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FLASHING_STARS_FLAG()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		TOGGLE_FLASH_FLAG(ARE_PLAYER_FLASHING_STARS_ABOUT_TO_DROP(PLAYER_ID()))
	ENDIF
ENDPROC

FUNC BOOL SHOULD_FLASH_AVE_STARS()

	IF sAveVars.iParticipantCabDriver <> -1
	AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(sAveVars.iParticipantCabDriver)) 
	AND IS_BIT_SET(GlobalplayerBD_FM[sAveVars.iParticipantCabDriver].iGeneralBitSet, ciWANTED_FLASHING)
	
		RETURN TRUE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sAveVars.stAVEWantedTimer)
	AND HAS_NET_TIMER_EXPIRED(sAveVars.stAVEWantedTimer, g_sMPTunables.IH2_AVENGER_TIME_NEEDED_TO_LOSE_COPS - 10000)
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC TOGGLE_AVE_SWAT_HELI_FLAG(BOOL bSet)

	IF bSet
		IF NOT IS_BIT_SET(sNoResetAveVars.iAVENRBitSet, ciAVE_WANTED_SWAT_CONFIG_FLAG)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_SwatHeliSpawnWithinLastSpottedLocation, TRUE)
			SET_BIT(sNoResetAveVars.iAVENRBitSet, ciAVE_WANTED_SWAT_CONFIG_FLAG)
			PRINTLN("  [AVE_WANTED] TOGGLE_AVE_SWAT_HELI_FLAG, TRUE ")
		ENDIF
	ELSE
		IF IS_BIT_SET(sNoResetAveVars.iAVENRBitSet, ciAVE_WANTED_SWAT_CONFIG_FLAG)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_SwatHeliSpawnWithinLastSpottedLocation, FALSE)
			CLEAR_BIT(sNoResetAveVars.iAVENRBitSet, ciAVE_WANTED_SWAT_CONFIG_FLAG)
			PRINTLN("  [AVE_WANTED] TOGGLE_AVE_SWAT_HELI_FLAG, FALSE ")
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_VEHICLE_IN_RESTRICTED_AREA(BOOL bAvenger, BOOL bHackerTruck)

	IF DOES_ENTITY_EXIST(GET_AVE_CAB_ID(bAvenger, bHackerTruck))
		IF NOT IS_ENTITY_DEAD(GET_AVE_CAB_ID(bAvenger, bHackerTruck))
		
			VECTOR vPos = GET_ENTITY_COORDS(GET_AVE_CAB_ID(bAvenger, bHackerTruck))

			RETURN IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA(vPos)
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

// Expensive so only suitable for brief use
PROC MAINTAIN_TURRET_IN_USE_FLAG(PLAYER_INDEX playerID) 

	INT iPlayer = NATIVE_TO_INT(playerID)

	IF (GlobalplayerBD_FM[iPlayer].iTurretSeat <> -1
	AND IS_BIT_SET(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam))
	OR IS_BIT_SET(GlobalplayerBD_FM[iPlayer].propertyDetails.iHackerTruckModBS, PROPERTY_BROADCAST_HACKER_TRUCK_USING_MISSILE_LAUNCHER)
	OR IS_BIT_SET(GlobalplayerBD_FM[iPlayer].propertyDetails.iHackerTruckModBS, PROPERTY_BROADCAST_HACKER_TRUCK_USING_DRONE)
		IF NOT IS_BIT_SET(sAveVars.iTurretBit, iPlayer)
			SET_BIT(sAveVars.iTurretBit, iPlayer)
			PRINTLN("MAINTAIN_TURRET_IN_USE_FLAG, TURRET IN USE BY ", GET_PLAYER_NAME(playerID))
		ENDIF
	ELSE
		IF IS_BIT_SET(sAveVars.iTurretBit, iPlayer)
			CLEAR_BIT(sAveVars.iTurretBit, iPlayer)
			PRINTLN("MAINTAIN_TURRET_IN_USE_FLAG, TURRET NO LONGER IN USE BY ", GET_PLAYER_NAME(playerID))
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL TURRET_IN_USE()
	RETURN ( sAveVars.iTurretBit != 0 )
ENDFUNC

FUNC VEHICLE_INDEX GET_VEHICLE_INDEX_OF_IAM_IN(BOOL bAvenger, BOOL bHackerTruck)
	IF bAvenger 
		RETURN g_viAvengerIamIn
	ENDIF
	
	IF bHackerTruck
		RETURN g_viHackerTruckIamIn
	ENDIF
	
	RETURN INT_TO_NATIVE(VEHICLE_INDEX, -1)
ENDFUNC 

FUNC BOOL MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS()

	
	FLOAT fHeightAboveGround
	BOOL bReturn
	
	// Reset everything and start a cooldown to avoid players receiving a wanted rating shortly after losing one
	IF HAS_NET_TIMER_STARTED(stAVECooldown)
		IF HAS_NET_TIMER_EXPIRED(stAVECooldown, 5000)
			RESET_NET_TIMER(stAVECooldown)
			AVE_WANTED_VARS tempStruct
			sAveVars = tempStruct
			TOGGLE_RESET_FLAG(FALSE)
			PRINTLN("  [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS - RESET ALL! ")
		ELSE
			TOGGLE_RESET_FLAG(TRUE)
		ENDIF	
		
//		PRINTLN("4242385 1 ")
	ELSE
		
		TOGGLE_RESET_FLAG(FALSE)
	
		INT iLocalWantedRating
		
		IF NOT g_sMPTunables.BH2_AVENGER_DISABLE_BUSTED_BY_COPS_INSIDE_AVENGER                                                
			// BUSTED, handle warp to police tation
			HANDLE_AVENGER_BUSTED_WARP()
			IF g_bBustedWarpInProgress
				bReturn = TRUE
			ENDIF
		ENDIF
		
		BOOL bIsPlayerInAvenger = IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		BOOL bIsPlayerInHackerTruck = IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		
		IF bIsPlayerInAvenger
		OR bIsPlayerInHackerTruck
			
//			PRINTLN("4242385 2 ")

			IF IS_SCREEN_FADED_IN() // ensure player is properly inside the truck
			
				iLocalWantedRating = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
			
				BOOL bCabOccupied
				BOOL bCabUseless
				VEHICLE_INDEX EiCabId = GET_AVE_CAB_ID(bIsPlayerInAvenger, bIsPlayerInHackerTruck)
				IF DOES_ENTITY_EXIST(EiCabId)
				AND NOT IS_ENTITY_DEAD(EiCabId)
					IF NOT IS_VEHICLE_SEAT_FREE(EiCabId)
						bCabOccupied = TRUE
					ENDIF
				ELSE
					bCabUseless = TRUE
					PRINTLN("  [AVE_WANTED] bCabUseless = TRUE ")
				ENDIF	

				IF iLocalWantedRating > 0
				
					IF bCabOccupied
					OR TURRET_IN_USE()
						// - If someone else wanted enters in that time the timer resets to 60 seconds
						RESET_AVE_WANTED_TIMER()
					ELSE
						// - If no one is in the cab the cops should be lost after 60 seconds
						IF NOT HAS_NET_TIMER_STARTED(sAveVars.stAVEWantedTimer)
							START_NET_TIMER(sAveVars.stAVEWantedTimer)
							PRINTLN("    ->    [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS - START_NET_TIMER(sAveVars.stAVEWantedTimer) ")
						ELSE
							IF HAS_NET_TIMER_EXPIRED(sAveVars.stAVEWantedTimer, g_sMPTunables.IH2_AVENGER_TIME_NEEDED_TO_LOSE_COPS)
								sAveVars.bWipeAVEWanted = TRUE
								PRINTLN("    ->     MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS - HAS_NET_TIMER_EXPIRED(sAveVars.stAVEWantedTimer, g_sMPTunables.IH2_AVENGER_TIME_NEEDED_TO_LOSE_COPS) ", g_sMPTunables.IH2_AVENGER_TIME_NEEDED_TO_LOSE_COPS)
							ENDIF
						ENDIF
					ENDIF
				
					// Tell cops where to go
					IF DOES_ENTITY_EXIST(GET_VEHICLE_INDEX_OF_IAM_IN(bIsPlayerInAvenger, bIsPlayerInHackerTruck))
					AND NOT IS_ENTITY_DEAD(GET_VEHICLE_INDEX_OF_IAM_IN(bIsPlayerInAvenger, bIsPlayerInHackerTruck))
					
						VECTOR vCoords = GET_ENTITY_COORDS(GET_VEHICLE_INDEX_OF_IAM_IN(bIsPlayerInAvenger, bIsPlayerInHackerTruck))
						
						SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), vCoords)
						PRINTLN("  [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS - SET_PLAYER_WANTED_CENTRE_POSITION, vCoords = ", vCoords)
	
						TOGGLE_AVE_SWAT_HELI_FLAG(TRUE)
						
						fHeightAboveGround = GET_ENTITY_HEIGHT_ABOVE_GROUND(GET_VEHICLE_INDEX_OF_IAM_IN(bIsPlayerInAvenger, bIsPlayerInHackerTruck)) 
					ELSE
						PRINTLN("  [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS - SET_PLAYER_WANTED_CENTRE_POSITION, ERROR, GET_VEHICLE_INDEX_OF_IAM_IN(bIsPlayerInAvenger, bIsPlayerInHackerTruck) = ", NATIVE_TO_INT(GET_VEHICLE_INDEX_OF_IAM_IN(bIsPlayerInAvenger, bIsPlayerInHackerTruck)))
					ENDIF
					
					// - If no one is in the cab the cops should be lost after 60 seconds
					IF sAveVars.bWipeAVEWanted
						WIPE_AVENGER_WANTED_RATINGS()	
						START_AVE_COOLDOWN_TIMER()
						RESET_AVE_WANTED_TIMER()
						IF sAveVars.iBustedWarpProgress = 0
							RESET_AVENGER_BUSTED_TIMER()
						ENDIF
						PRINTLN("  [AVE_WANTED]  MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS - IS_PLAYER_IN_ARMORY_AIRCRAFT, WIPE_AVENGER_WANTED_RATINGS ")
					ELSE
						IF SHOULD_FLASH_AVE_STARS()
							FORCE_START_HIDDEN_EVASION(PLAYER_ID())
						ELSE
							SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
						ENDIF
					ENDIF
					
//					PRINTLN("fHeightAboveGround = ", fHeightAboveGround)
					
					IF NOT bCabOccupied 
					AND iLocalWantedRating > 4
					AND NOT IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
					AND fHeightAboveGround < 5 
						START_AVENGER_BUSTED_TIMER()
						PRINTLN("  [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS - SHOULD BUST PLAYER ")
					ELSE
						IF sAveVars.iBustedWarpProgress = 0
							RESET_AVENGER_BUSTED_TIMER()
						ENDIF
					ENDIF
				ELSE
					
					TOGGLE_AVE_SWAT_HELI_FLAG(FALSE)
				ENDIF

				// - If a player in the cab gains a Wanted Level those in the trailer will too
				IF NOT sAveVars.bWipeAVEWanted
				OR IS_LOCAL_PLAYER_USING_DRONE()
				OR IS_LOCAL_PLAYER_USING_VEHICLE_WEAPON()
					IF sAveVars.iRemoteWantedLevel > 0
					AND iLocalWantedRating < sAveVars.iRemoteWantedLevel
					AND NOT bCabUseless
					AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != sAveVars.iRemoteWantedLevel
						IF GET_CLOUD_TIME_AS_INT() < MPGlobalsAmbience.sMagnateGangBossData.iTimeOfLastWantedClear + POST_WANTED_CLEAR_TIME
							PRINTLN("  [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS, skip setting wanted level mission is clearing wanted level")
						ELSE
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), sAveVars.iRemoteWantedLevel)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							PRINTLN("  [AVE_WANTED]  MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS - IS_PLAYER_IN_ARMORY_AIRCRAFT, SET_PLAYER_WANTED_LEVEL >>> ", sAveVars.iRemoteWantedLevel)
						ENDIF	
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(sAveVars.iAVEBitSet, ciAVE_WANTED_NEEDS_CLEARED)
					CLEAR_BIT(sAveVars.iAVEBitSet, ciAVE_WANTED_NEEDS_CLEARED)
				ENDIF
				
			ELSE
				IF NOT IS_BIT_SET(sAveVars.iAVEBitSet, ciAVE_WANTED_NEEDS_CLEARED)
					INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
					IF iPlayer <> -1
					AND GlobalplayerBD[iPlayer].SimpleInteriorBD.eCurrentWalkingInOrOutOfSimpleInterior <> SIMPLE_INTERIOR_INVALID
					AND (GET_SIMPLE_INTERIOR_TYPE(GlobalplayerBD[iPlayer].SimpleInteriorBD.eCurrentWalkingInOrOutOfSimpleInterior) = SIMPLE_INTERIOR_TYPE_ARMORY_AIRCRAFT OR GET_SIMPLE_INTERIOR_TYPE(GlobalplayerBD[iPlayer].SimpleInteriorBD.eCurrentWalkingInOrOutOfSimpleInterior) = SIMPLE_INTERIOR_TYPE_HACKER_TRUCK)
					AND IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
					AND NOT IS_VEHICLE_IN_RESTRICTED_AREA(bIsPlayerInAvenger, bIsPlayerInHackerTruck)
					AND NOT TURRET_IN_USE() 
					
						// - If you enter the trailer with a Wanted Level it is cleared
						WIPE_AVENGER_WANTED_RATINGS()	
						SET_BIT(sAveVars.iAVEBitSet, ciAVE_WANTED_NEEDS_CLEARED)
						sAveVars.bWipeAVEWanted = FALSE
						PRINTLN("  [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS - WIPE_AVENGER_WANTED_RATINGS (entering hold) ")
					ELSE
						// url:bugstar:3672041
						// Prevent players losing the wanted rating during the warp outside the trailer
						IF NOT SHOULD_FLASH_AVE_STARS()
							IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
									PRINTLN("  [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS - SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME, 2 ")
						
									SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			TOGGLE_FLASH_FLAG(FALSE)
			
			bReturn = TRUE
			
		ELSE
			RESET_AVE_WANTED_TIMER()
			
			IF sAveVars.iBustedWarpProgress = 0
				RESET_AVENGER_BUSTED_TIMER()
			ENDIF
			
			TOGGLE_AVE_SWAT_HELI_FLAG(FALSE)
			
//			PRINTLN("4242385 6 ")
			
		ENDIF
		
		// In front CAB
		IF LOCAL_PLAYER_IN_FRONT_CAB(FALSE, TRUE)
		OR LOCAL_PLAYER_IN_FRONT_CAB(TRUE, FALSE)
		
//			PRINTLN("4242385 3 ")
					
			MAINTAIN_FLASHING_STARS_FLAG()
			
			iLocalWantedRating = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
			
			IF NOT IS_BIT_SET(sAveVars.iAVEBitSet, ciAVE_WANTED_HAD_WANTED)
				IF iLocalWantedRating > 0
					SET_BIT(sAveVars.iAVEBitSet, ciAVE_WANTED_HAD_WANTED)
					PRINTLN("  [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS, LOCAL_PLAYER_IN_FRONT_CAB, ciAVE_WANTED_HAD_WANTED ")
				ENDIF
			ELSE
				IF iLocalWantedRating = 0
					WIPE_AVENGER_WANTED_RATINGS()	
					START_AVE_COOLDOWN_TIMER()
					sAveVars.bWipeAVEWanted = TRUE
					PRINTLN("  [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS, LOCAL_PLAYER_IN_FRONT_CAB, WIPE_AVENGER_WANTED_RATINGS, sAveVars.bWipeAVEWanted = TRUE ")
				ENDIF
			ENDIF

			IF sAveVars.bWipeAVEWanted = FALSE
				// - This will transfer a Wanted Level to those in/or who enter the cab
				IF sAveVars.iRemoteWantedLevel > 0
				AND iLocalWantedRating < sAveVars.iRemoteWantedLevel
				AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != sAveVars.iRemoteWantedLevel
					IF GET_CLOUD_TIME_AS_INT() < MPGlobalsAmbience.sMagnateGangBossData.iTimeOfLastWantedClear + POST_WANTED_CLEAR_TIME
						PRINTLN("  [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS, skip setting wanted level mission is clearing wanted ")
					ELSE
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), sAveVars.iRemoteWantedLevel)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())					
						PRINTLN("  [AVE_WANTED] MAINTAIN_AVENGER_TRUCK_WANTED_RATINGS, LOCAL_PLAYER_IN_FRONT_CAB, , SET_PLAYER_WANTED_LEVEL to ", sAveVars.iRemoteWantedLevel)
					ENDIF	
				ELSE
					#IF IS_DEBUG_BUILD
					IF sAveVars.bSpamPrints 
						PRINTLN("  [AVE_WANTED] LOCAL_PLAYER_IN_FRONT_CAB, iLocalWantedRating = ", iLocalWantedRating)
						PRINTLN("  [AVE_WANTED] LOCAL_PLAYER_IN_FRONT_CAB, sAveVars.iRemoteWantedLevel = ", sAveVars.iRemoteWantedLevel)
					ENDIF
					#ENDIF
				ENDIF
			ELSE
			
				WIPE_AVENGER_WANTED_RATINGS()	
				START_AVE_COOLDOWN_TIMER()
				RESET_AVE_WANTED_TIMER()
				IF sAveVars.iBustedWarpProgress = 0
					RESET_AVENGER_BUSTED_TIMER()
				ENDIF
				PRINTLN("  [AVE_WANTED] LOCAL_PLAYER_IN_FRONT_CAB, RESET ")
			ENDIF
			bReturn = TRUE
		ELSE
			TOGGLE_FLASH_FLAG(FALSE)
			
			IF IS_BIT_SET(sAveVars.iAVEBitSet, ciAVE_WANTED_HAD_WANTED)
				CLEAR_BIT(sAveVars.iAVEBitSet, ciAVE_WANTED_HAD_WANTED)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF sAveVars.bSpamPrints 
				PRINTLN("  [AVE_WANTED] LOCAL_PLAYER_IN_FRONT_CAB, FALSE  iLocalWantedRating = ", iLocalWantedRating)
				PRINTLN("  [AVE_WANTED] LOCAL_PLAYER_IN_FRONT_CAB, FALSE  sAveVars.iRemoteWantedLevel = ", sAveVars.iRemoteWantedLevel)
				PRINTLN("  [AVE_WANTED] LOCAL_PLAYER_IN_FRONT_CAB, FALSE ")
			ENDIF
			#ENDIF
		ENDIF
		RETURN bReturn
	ENDIF
	RETURN bReturn
ENDFUNC

 // #IF FEATURE_GANG_OPS
