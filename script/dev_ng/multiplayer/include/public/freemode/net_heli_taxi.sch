//////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_heli_taxi.sc														//
// Description: Player calls for a heli taxi when stranded in the water.				//
// Written by:  Ryan Baker																//
// Date: 04/01/2013																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
//USING "script_player.sch"
//USING "commands_path.sch"
//USING "building_control_public.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_mission.sch"
USING "mp_scaleform_functions.sch"

//PURPOSE: Retursn TRUE if the player is allowed to call for a Heli Pickup
FUNC INT CAN_CALL_FOR_HELI_PICKUP()
	//IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 5.0
	//	RETURN FALSE
	//ENDIF
	
	IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HELI_PICKUP)
		RETURN 3
	ENDIF
	
	/*IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
		RETURN 1
	ENDIF*/
	
	IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		RETURN 2
	ENDIF
	
//	#IF FEATURE_EXECUTIVE
//	// if on roof of office
//	IF IS_PLAYER_ON_ANY_OFFICE_ROOF(PLAYER_ID())
//		RETURN 2
//	ENDIF
//	#ENDIF
	
	
	RETURN 0
ENDFUNC

//PURPOSE: Returns TRUE if the player is allowed to call for a Heli Pickup
FUNC INT CAN_CALL_FOR_PERSONAL_ASSISTANT_HELI_PICKUP()
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_HELI_TAXI")) > 0
		RETURN 3
	ENDIF
	
	IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		RETURN 2
	ENDIF
	
	IF (FM_EVENT_GET_SESSION_ACTIVE_FM_EVENT() = FMMC_TYPE_CRIMINAL_DAMAGE)
		RETURN 1
	ENDIF
	
	// Check cooldown period has elapsed since killing the pilot
	INT iDiff = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobalsAmbience.tKilledPAPilot))
	IF (iDiff < g_sMPTunables.ipa_pilot_kill_cooldown)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MP_CONTACT_REQUESTS,	"CAN_CALL_FOR_PERSONAL_ASSISTANT_HELI_PICKUP - cooldown time ", iDiff)
		#ENDIF
		RETURN 1
	ENDIF
	
	
//	// if on roof of office
//	IF IS_PLAYER_ON_ANY_OFFICE_ROOF(PLAYER_ID())
//		RETURN 2
//	ENDIF	
	RETURN 0
	
ENDFUNC

//PURPOSE: Controls the launching of the Heli Taxi
FUNC BOOL MAINTAIN_HELI_TAXI_CLIENT()//HELI_TAXI_STRUCT &HeliTaxiData)
	MP_MISSION_DATA amHeliTaxiLaunch
				
	IF MPGlobalsAmbience.bLaunchHeliPickup = TRUE
		//IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_HELI_TAXI")) <= 0
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_HELI_TAXI", DEFAULT_MISSION_INSTANCE, TRUE)
			//IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
							
				//IF HeliTaxiData.bLaunchHeliTaxi = TRUE
				amHeliTaxiLaunch.mdID.idMission = eAM_HELI_TAXI
				amHeliTaxiLaunch.iInstanceId = DEFAULT_MISSION_INSTANCE	//NATIVE_TO_INT(PLAYER_ID())
				IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amHeliTaxiLaunch)
					#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_HELI_TAXI_CLIENT - LAUNCHED SCRIPT AM_HELI_TAXI.sc - INSTANCE = ") NET_PRINT_INT(amHeliTaxiLaunch.iInstanceId) NET_NL() #ENDIF
					//SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(HeliTaxiData.buttonMovie)
					//RESET_NET_TIMER(HeliTaxiData.iLaunchTimer)
					MPGlobalsAmbience.bLaunchHeliPickup = FALSE
				ENDIF
			
			//ENDIF
			
		ELSE
			IF MPGlobalsAmbience.bLaunchHeliPickup = TRUE
				//SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(HeliTaxiData.buttonMovie)
				//RESET_NET_TIMER(HeliTaxiData.iLaunchTimer)
				MPGlobalsAmbience.bLaunchHeliPickup = FALSE
				#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_HELI_TAXI_CLIENT - SCRIPT ALREADY RUNNING AM_HELI_TAXI.sc") NET_NL() #ENDIF
			ENDIF
		ENDIF
	
	//LAUNCH IF SOMEBODY ELSE IS RUNNING THE SCRIPT
	/*ELSE
		IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.iHeliLaunchCheckDelay, 2000)
			IF NETWORK_IS_SCRIPT_ACTIVE("AM_HELI_TAXI", DEFAULT_MISSION_INSTANCE)
				//IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_HELI_TAXI")) <= 0
				IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_HELI_TAXI", DEFAULT_MISSION_INSTANCE, TRUE)
					amHeliTaxiLaunch.mdID.idMission = eAM_HELI_TAXI
					amHeliTaxiLaunch.iInstanceId = DEFAULT_MISSION_INSTANCE	//NATIVE_TO_INT(PLAYER_ID())
					
					//IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
						IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amHeliTaxiLaunch)
							#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_HELI_TAXI_CLIENT - SOMEONE ELSE STARTED - LAUNCHED SCRIPT AM_HELI_TAXI.sc - INSTANCE = ") NET_PRINT_INT(amHeliTaxiLaunch.iInstanceId) NET_NL() #ENDIF
							//MPGlobalsAmbience.bInitMpTaxiLaunch = TRUE
						ENDIF
					//ENDIF
				ENDIF
			ENDIF
			RESET_NET_TIMER(MPGlobalsAmbience.iHeliLaunchCheckDelay)
		ENDIF*/
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
