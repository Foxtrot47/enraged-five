USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "net_mission.sch"
USING "net_common_functions.sch"
USING "net_gang_boss_launcher.sch"

#IF FEATURE_DLC_2_2022

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This file is for core behaviour of the Street Dealer launching
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////
///   HELPERS
/////////////////////////

FUNC INT GET_ACTIVE_STREET_DEALER_LOCATION(INT iDealer)
	RETURN MPGlobalsAmbience.sStreetDealers.iActiveLocations[iDealer]
ENDFUNC

FUNC STRING GET_STREET_DEALER_SCRIPT_NAME()
	RETURN "fm_street_dealer"
ENDFUNC

/////////////////////////
///   ACTIVE CHECKS
/////////////////////////

/// PURPOSE:
///    These are checks that prevent the system running, such as critical to other missions or in corona, etc.
FUNC BOOL SHOULD_RUN_STREET_DEALERS(BOOL bCheckActive)
		
	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - bNetworkIsActivitySession")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - IS_PLAYER_IN_CORONA")			ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - !IS_SKYSWOOP_AT_GROUND")			ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_PERMANENT_TO_GANG_BOSS_MISSION()
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - GB_IS_LOCAL_PLAYER_PERMANENT_TO_GANG_BOSS_MISSION")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(MPGlobals.sFreemodeCache.PlayerIndex)
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_AN_ANIMAL()
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - IS_LOCAL_PLAYER_AN_ANIMAL")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - IS_PLAYER_ON_MP_INTRO")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_USING_ANY_RC_VEHICLE(MPGlobals.sFreemodeCache.PlayerIndex)
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - IS_PLAYER_USING_ANY_RC_VEHICLE")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_TEST_DRIVING_A_VEHICLE(MPGlobals.sFreemodeCache.PlayerIndex)
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - IS_PLAYER_TEST_DRIVING_A_VEHICLE")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.sFreemodeCache.PlayerIndex)].iCurrentMissionType = FMMC_TYPE_IMPROMPTU_DM
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - FMMC_TYPE_IMPROMPTU_DM")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(MPGlobals.sFreemodeCache.PlayerIndex, FMMC_TYPE_RACE_TO_POINT)
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - FMMC_TYPE_RACE_TO_POINT")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF bCheckActive
	AND MPGlobalsAmbience.sStreetDealers.iActiveScriptLocation != (-1)
		#IF IS_DEBUG_BUILD	IF MPGlobalsAmbience.sStreetDealers.bExtraDebug	PRINTLN("[DEALERS] SHOULD_RUN_STREET_DEALERS - FALSE - iActiveScriptLocation = ", MPGlobalsAmbience.sStreetDealers.iActiveScriptLocation)	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC FLOAT SDL_GET_STREET_DEALER_LAUNCH_RANGE()
	RETURN 200.0
ENDFUNC

FUNC VECTOR GET_STREET_DEALER_LOCATION_COORD(INT iLocation)
	SWITCH iLocation
		CASE 0		RETURN <<550.8953, -1774.5175, 28.3121>>
		CASE 1		RETURN <<-154.9240, 6434.4282, 30.9160>>
		CASE 2		RETURN <<316.3949, 2613.4805, 43.4671>>
		CASE 3		RETURN <<1533.8459, 3796.8369, 33.4560>>
		CASE 4		RETURN <<-1666.6420, -1080.0201, 12.1537>>
		CASE 5		RETURN <<-1560.6105, -413.3221, 37.1001>>
		CASE 6		RETURN <<819.1757, -2989.5164, 5.0210>>
		CASE 7		RETURN <<1001.7010, -2162.4480, 29.5670>>
		CASE 8		RETURN <<1368.2760, -1518.8810, 56.6990>>
		CASE 9		RETURN <<-3054.5740, 556.7110, 0.6610>>
		CASE 10		RETURN <<-72.8903, 80.7170, 70.6161>>
		CASE 11		RETURN <<198.6676, -167.0663, 55.3187>>
		CASE 12		RETURN <<814.6360, -280.1090, 65.4630>>
		CASE 13		RETURN <<-237.0040, -256.5130, 38.1220>>
		CASE 14		RETURN <<-493.6540, -720.7340, 22.9210>>
		CASE 15		RETURN <<156.1586, 6656.5249, 30.5882>>
		CASE 16		RETURN <<1986.8673, 3786.9358, 31.2792>>
		CASE 17		RETURN <<-685.5629, 5762.8706, 16.5110>>
		CASE 18		RETURN <<1707.7030, 4924.3110, 41.0780>>
		CASE 19		RETURN <<1195.3047, 2630.4685, 36.8100>>
		CASE 20		RETURN <<-30.7640, 1943.7429, 189.1861>>
		CASE 21		RETURN <<2724.0076, 1483.0660, 23.5007>>
		CASE 22		RETURN <<1594.5344, 6452.9546, 24.3172>>
		CASE 23		RETURN <<-2177.3970, 4275.9448, 48.1200>>
		CASE 24		RETURN <<-2521.2490, 2311.7939, 32.2160>>
		CASE 25		RETURN <<-3161.2869, 1152.0717, 20.0540>>
		CASE 26		RETURN <<-1145.0260, -2048.4661, 12.2180>>
		CASE 27		RETURN <<-1304.3210, -1318.8480, 3.8800>>
		CASE 28		RETURN <<-946.7270, 322.0810, 70.3570>>
		CASE 29		RETURN <<-895.1120, -776.6240, 14.9100>>
		CASE 30		RETURN <<-250.6140, -1527.6169, 30.5610>>
		CASE 31		RETURN <<-601.6390, -1026.4900, 21.5500>>
		CASE 32		RETURN <<2713.1013, 4324.4136, 44.8521>>
		CASE 33		RETURN <<726.7720, 4169.1011, 39.7090>>
		CASE 34		RETURN <<182.0660, 3087.3979, 42.2853>>
		CASE 35		RETURN <<2351.5920, 2524.2490, 46.6940>>
		CASE 36		RETURN <<388.9941, 799.6882, 186.6764>>
		CASE 37		RETURN <<2546.4163, 385.6190, 107.6182>>
		CASE 38		RETURN <<830.2875, -1052.7747, 27.6666>>
		CASE 39		RETURN <<-759.6620, -208.3960, 36.2710>>
		CASE 40		RETURN <<-135.9990, -2032.8580, 21.9560>>
		CASE 41		RETURN <<124.0200, -1039.8840, 28.2130>>
		CASE 42		RETURN <<477.7250, -569.5540, 27.5000>>
		CASE 43		RETURN <<959.6700, 3619.0359, 31.6680>>
		CASE 44		RETURN <<2375.8994, 3162.9954, 47.2087>>
		CASE 45		RETURN <<-1505.6870, 1526.5580, 114.2570>>
		CASE 46		RETURN <<645.7370, 242.1730, 101.1530>>
		CASE 47		RETURN <<1173.1378, -388.2896, 70.5896>>
		CASE 48		RETURN <<-1801.8500, 172.4900, 67.7710>>
		CASE 49		RETURN <<3803.2920, 4439.1309, 3.1230>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

////////////////////////////////////
///   LOCATION SELECTION
////////////////////////////////////

FUNC FLOAT GET_STREET_DEALER_MIN_DIST()
	RETURN 1000.0
ENDFUNC

FUNC BOOL IS_STREET_DEALER_LOCATION_VALID(INT iLocation)
	INT iIndex
	REPEAT STREET_DEALER_MAX_ACTIVE iIndex
		IF MPGlobalsAmbience.sStreetDealers.iActiveLocations[iIndex] = iLocation
			RETURN FALSE
		ENDIF
		
		IF MPGlobalsAmbience.sStreetDealers.iActiveLocations[iIndex] != (-1)
		AND VDIST_2D(GET_STREET_DEALER_LOCATION_COORD(MPGlobalsAmbience.sStreetDealers.iActiveLocations[iIndex]), GET_STREET_DEALER_LOCATION_COORD(iLocation)) < GET_STREET_DEALER_MIN_DIST()
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC INIT_STREET_DEALER_LOCATIONS()
	INT iLocation
	REPEAT STREET_DEALER_MAX_ACTIVE iLocation
		MPGlobalsAmbience.sStreetDealers.iActiveLocations[iLocation] = -1
	ENDREPEAT
	PRINTLN("[DEALERS] INIT_STREET_DEALER_LOCATIONS - About to select dealers for the day, setting active to -1.")
ENDPROC

PROC POPULATE_DAILY_STREET_DEALERS()
	IF MPGlobalsAmbience.sDailyReset.iSeed = (-1)
		EXIT
	ENDIF
	
	INIT_STREET_DEALER_LOCATIONS()
	
	INT iTempLocationArray[STREET_DEALER_MAX_LOCATIONS]

	INT iIndex, iIndex2
	REPEAT STREET_DEALER_MAX_LOCATIONS iIndex
		iTempLocationArray[iIndex] = iIndex
	ENDREPEAT
	
	NETWORK_SEED_RANDOM_NUMBER_GENERATOR(MPGlobalsAmbience.sDailyReset.iSeed)
	
	iIndex = 0
	FOR iIndex = STREET_DEALER_MAX_LOCATIONS - 1 TO 1 STEP -1
		iIndex2 = NETWORK_GET_RANDOM_INT_RANGED(0, iIndex)
		COMMON_SWAP_INT(iTempLocationArray[iIndex], iTempLocationArray[iIndex2])
		//PRINTLN("[DEALERS] POPULATE_DAILY_STREET_DEALERS - Swap iIndex ", iIndex, " with iIndex2 ",  iIndex2)
	ENDFOR
	
	INT iValidLocation
	REPEAT STREET_DEALER_MAX_LOCATIONS iIndex
		IF iValidLocation = STREET_DEALER_MAX_ACTIVE
			BREAKLOOP
		ENDIF
		
		IF IS_STREET_DEALER_LOCATION_VALID(iTempLocationArray[iIndex])
			MPGlobalsAmbience.sStreetDealers.iActiveLocations[iValidLocation] = iTempLocationArray[iIndex]
			PRINTLN("[DEALERS] POPULATE_DAILY_STREET_DEALERS - Location #", iTempLocationArray[iIndex], " selected for dealer #", iValidLocation)
			iValidLocation++
		ENDIF
	ENDREPEAT
	
	NETWORK_SEED_RANDOM_NUMBER_GENERATOR(GET_CLOUD_TIME_AS_INT())
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_STREET_DEALER_DEBUG_BLIP_NAME(INT iDealer)
	SWITCH iDealer
		CASE 0		RETURN "XM22DBG_DLR1"
		CASE 1		RETURN "XM22DBG_DLR2"
		CASE 2		RETURN "XM22DBG_DLR3"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC MAINTAIN_STREET_DEALER_DEBUG(INT iLocation, INT iDealer, VECTOR vCoord)
	IF MPGlobalsAmbience.sStreetDealers.bDrawDebugBlips
	AND iLocation != (-1)
		IF NOT DOES_BLIP_EXIST(MPGlobalsAmbience.sStreetDealers.blipDebugActive[iDealer])
			MPGlobalsAmbience.sStreetDealers.blipDebugActive[iDealer] = ADD_BLIP_FOR_COORD(vCoord)
			SET_BLIP_NAME_FROM_TEXT_FILE(MPGlobalsAmbience.sStreetDealers.blipDebugActive[iDealer], GET_STREET_DEALER_DEBUG_BLIP_NAME(iDealer))
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.sStreetDealers.blipDebugActive[iDealer], HUD_COLOUR_BLUE)
		ELIF NOT ARE_VECTORS_EQUAL(vCoord, GET_BLIP_COORDS(MPGlobalsAmbience.sStreetDealers.blipDebugActive[iDealer]))
			SET_BLIP_COORDS(MPGlobalsAmbience.sStreetDealers.blipDebugActive[iDealer], vCoord)
		ENDIF
	ELIF DOES_BLIP_EXIST(MPGlobalsAmbience.sStreetDealers.blipDebugActive[iDealer])
		REMOVE_BLIP(MPGlobalsAmbience.sStreetDealers.blipDebugActive[iDealer])
	ENDIF
ENDPROC

PROC CLEANUP_ALL_STREET_DEALER_DEBUG_BLIPS()
	INT iDealer
	REPEAT STREET_DEALER_MAX_ACTIVE iDealer
		IF DOES_BLIP_EXIST(MPGlobalsAmbience.sStreetDealers.blipDebugActive[iDealer])
			REMOVE_BLIP(MPGlobalsAmbience.sStreetDealers.blipDebugActive[iDealer])
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

//////////////////
///   MAINTAIN
//////////////////

/// PURPOSE:
///    A common system to allow the launching of freemode mission content from a blip in world on the map
PROC MAINTAIN_STREET_DEALER_LAUNCH()
	IF NOT SHOULD_RUN_STREET_DEALERS(TRUE)
		CLEANUP_ALL_STREET_DEALER_DEBUG_BLIPS()
		EXIT
	ENDIF
	
	FLOAT fDist
	VECTOR vCoord
	INT iDealer, iLocation
	REPEAT STREET_DEALER_MAX_ACTIVE iDealer
		iLocation = GET_ACTIVE_STREET_DEALER_LOCATION(iDealer)
		IF iLocation != (-1)
			vCoord = GET_STREET_DEALER_LOCATION_COORD(iLocation)

			fDist = VDIST(MPGlobals.sFreemodeCache.vPlayersLocation, vCoord)
			IF fDist <= SDL_GET_STREET_DEALER_LAUNCH_RANGE()
				IF NET_LOAD_AND_LAUNCH_SCRIPT(GET_STREET_DEALER_SCRIPT_NAME(), DEFAULT_STACK_SIZE)
					MPGlobalsAmbience.sStreetDealers.iActiveScriptLocation = iLocation
					PRINTLN("[DEALERS] MAINTAIN_STREET_DEALER_LAUNCH - Launching script. In range (", SDL_GET_STREET_DEALER_LAUNCH_RANGE(), "m) of dealer #", iDealer, " at location #", iLocation, " - ", vCoord)
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_STREET_DEALER_DEBUG(iLocation, iDealer, vCoord)
		#ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

