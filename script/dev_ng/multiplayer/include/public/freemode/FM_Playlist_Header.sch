//////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FM_Playlist_header											                        //
// Written by:  Robert Wright																	//
// Date: 15/11/2012																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////
USING "rage_builtins.sch"
USING "globals.sch"
USING "net_mission.sch"
USING "Menu_Public.sch"
USING "FMMC_Cloud_loader.sch"
USING "fm_maintain_mission_rounds.sch"
USING "mp_skycam.sch"
//USING "leader_board_common.sch"

STRUCT PLAYLIST_LB_VARIABLES
	//bitsets
	INT iNotBestPlayer
	INT iAlreadyUsedPlayer

	INT iBestPlayerOrder[NUM_NETWORK_PLAYERS]
//	INT iPlayerSortStage
	INT iCurrentPlayerToCheck
	INT iCurrentLBPosition
	BOOL bWinnerFound
ENDSTRUCT


//SWITCHING INTS
CONST_INT ciEND_OF_MISSION_INI		 		0
CONST_INT ciEND_OF_MISSION_SET_UP 			1
CONST_INT ciEND_OF_MISSION_RATE_JOB			2
CONST_INT ciEND_OF_MISSION_DO_VOTE 			3
CONST_INT ciEND_OF_MISSION_VIEW_SCLB		4
CONST_INT ciEND_OF_MISSION_CLEAR_BUTTONS	5
CONST_INT ciEND_OF_MISSION_DONE				6
CONST_INT ciEND_OF_MISSION_HORDE			7
CONST_INT ciEND_OF_MISSION_RACE				8
//For the bookmarks
CONST_INT LBD_BOOKMARK_INITIAL				0
CONST_INT LBD_BOOKMARK_CONFIRM_SCREEN 		1
CONST_INT LBD_BOOKLMARK_WAIT_FOR_CLEAR		2
CONST_INT LBD_BOOKMARK_ADD			 		3
CONST_INT LBD_BOOKMARK_CONFIRM		 		4
CONST_INT LBD_BOOKMARK_ERROR		 		5
CONST_INT LBD_BOOKMARK_DONE			 		6
//For the quitting
CONST_INT LBD_QUIT_INITIAL					0
CONST_INT LBD_QUIT_CONFIRM_SCREEN 			1
CONST_INT LBD_QUIT_LBD			 			2
// For Freemode confirm
CONST_INT LBD_FM_INITIAL					0	
CONST_INT LBD_FM_CONFIRM					1
CONST_INT LBD_FM_QUIT		 				2

//Struct containing all the details
STRUCT FMMC_EOM_DETAILS
	INT 	iProgress					= ciEND_OF_MISSION_INI
	INT 	iSavedProgress				= ciEND_OF_MISSION_INI
	BOOL 	bOnlyDoQuit 				= FALSE
	BOOL	bBeingKickedFromSurvival	= FALSE
	BOOL 	bActiveSCLeaderboard 		= FALSE
	BOOL 	bAllowedToVoteLocal			= FALSE
	BOOL	bSetupWithSpectate 			= FALSE
	BOOL	bSpectateButtonPressed 		= FALSE
	BOOL 	bShowPlayerCard				= FALSE
	BOOL 	bPlayercardTriggered 		= FALSE
	BOOL 	bResetWithOutTimer	 		= FALSE
	BOOL 	bEnoughCash 		 		= FALSE
	BOOL 	bShowQuickRestart	 		= FALSE
	BOOL 	bShowQuickRestartSwap 		= FALSE
	BOOL 	bAddBookMark 				= TRUE
	BOOL 	bShowCoDriver				= FALSE
	BOOL 	bRallyRace					= FALSE
	BOOL 	bIsHeist					= FALSE
	BOOL 	bMiniGame					= FALSE
	BOOL	bIsContactMission			= FALSE
	BOOL	bRefresh					= FALSE
	BOOL	bTimerWasNull				= FALSE
	BOOL	bHidePlayerCard				= FALSE
	BOOL	bTimerShown					= FALSE
	BOOL	bHideQuickRestart			= FALSE
	BOOL	bPlaylistTimerZero			= FALSE
	BOOL	bTelemetryCalled			= FALSE
	INT 	iThumbStage					= 0
	INT 	iQuitProgress
	INT 	iAddToPlaylistProgress
	INT 	iRaceStage
	INT		iFMProgress					
	
	UGC_ERROR	iCloudFailStatus
	SCRIPT_TIMER sTimeDelayLbdSkip
ENDSTRUCT

CONST_INT ciCONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_SET_UP		0
CONST_INT ciCONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_MAINTAIN	1

FUNC BOOL IS_LOCAL_PLAYER_SPECTATOR()
	IF g_b_IsRaceSpectator
	OR g_b_IsDMSpectator
	OR IS_PLAYER_SCTV(PLAYER_ID())
	OR DID_I_JOIN_MISSION_AS_SPECTATOR()
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_PROFILE_BUTTON_NAME()
	STRING sProfileButtonName

	IF IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM()
		sProfileButtonName = "HUD_XBXCRD" // Gamer Card
	ELSE
		sProfileButtonName = "HUD_PS3CRD" // Profile
	ENDIF
	
	RETURN sProfileButtonName
ENDFUNC

CONST_INT ciONE_WHOLE_DAY 60*60*24

//Check to see if we're able to play more challenges today
FUNC BOOL AM_I_ABLE_TO_PLAY_MORE_CHALLENGES_TODAY_PLEASE()
	
	//If the number is set to 0 then no, get out please!
	IF g_sMPTunables.inumber_of_challenges_per_day = 0
		PRINTLN("[PLC] - CHALLENGE RESTRICTION - AM_I_ABLE_TO_PLAY_MORE_CHALLENGES_TODAY_PLEASE - g_sMPTunables.inumber_of_challenges_per_day")
		RETURN FALSE
	ENDIF
	
	//get the last challenge compleation time
	INT iChallengeCompleationTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHALLENGE_LAUNCH_TIME)
	
	//If the challenge launch time is 0 then we're ok
	IF iChallengeCompleationTime = 0
		PRINTLN("[PLC] - CHALLENGE RESTRICTION - AM_I_ABLE_TO_PLAY_MORE_CHALLENGES_TODAY_PLEASE iChallengeCompleationTime = 0")
		RETURN TRUE
	ENDIF
	
	//If the time out has expired then we are good to go
	IF GET_CLOUD_TIME_AS_INT() >= iChallengeCompleationTime + ciONE_WHOLE_DAY
		PRINTLN("[PLC] - CHALLENGE RESTRICTION - AM_I_ABLE_TO_PLAY_MORE_CHALLENGES_TODAY_PLEASE iChallengeCompleationTime >= GET_CLOUD_TIME_AS_INT() - ", iChallengeCompleationTime, " >= ", GET_CLOUD_TIME_AS_INT())
		//Reset and return TRUE
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CHALLENGE_LAUNCH_TIME, 0)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHALLENGES_TODAY, 0)
		PRINTLN("[PLC] - CHALLENGE RESTRICTION - AM_I_ABLE_TO_PLAY_MORE_CHALLENGES_TODAY_PLEASE SET_MP_INT_CHARACTER_STAT(MP_STAT_CHALLENGE_LAUNCH_TIME, 0)")
		PRINTLN("[PLC] - CHALLENGE RESTRICTION - AM_I_ABLE_TO_PLAY_MORE_CHALLENGES_TODAY_PLEASE SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHALLENGES_TODAY, 0)")
		RETURN TRUE
	ENDIF
	
	//If we are down here the timer is not 0 and it's not expired 
	//Get the number that we've played in the last 24 hours
	INT iNumberInLastTwentyFourHours = GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHALLENGES_TODAY)
	
	//If the number that we've played is less than the 
	IF iNumberInLastTwentyFourHours < g_sMPTunables.inumber_of_challenges_per_day
		PRINTLN("[PLC] - CHALLENGE RESTRICTION - AM_I_ABLE_TO_PLAY_MORE_CHALLENGES_TODAY_PLEASE iNumberInLastTwentyFourHours < g_sMPTunables.inumber_of_challenges_per_day - ", iNumberInLastTwentyFourHours, " < ", g_sMPTunables.inumber_of_challenges_per_day)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC




//Have I voted
FUNC INT GET_CURRENT_PLAYLIST_MAX_PLAYERS()
	RETURN (g_sCurrentPlayListDetails.iMaxNumber)
ENDFUNC
FUNC INT GET_CURRENT_PLAYLIST_MIN_PLAYERS()
	RETURN (g_sCurrentPlayListDetails.iMinNumber)
ENDFUNC
//Have I voted
FUNC BOOL FM_PLAY_LIST_HAVE_I_VOTED()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_Voted)
ENDFUNC

//Set me at the end of the mission
PROC FM_PLAY_LIST_SET_AT_END_OF_MISSION()
	PRINTLN("[PLC] - FMMC_EOM - FM_PLAY_LIST_SET_AT_END_OF_MISSION()")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_PlaylistAtEndOfMission)
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_Voted)
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestart)
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantNext)
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuickRestart)
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuickRestartSwap)
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToContinue)
ENDPROC

//Set me as voted
PROC FM_PLAY_LIST_SET_ME_AS_VOTED()
	PRINTLN("[PLC] - FMMC_EOM - FM_PLAY_LIST_SET_ME_AS_VOTED()")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_Voted)
ENDPROC

//Set I want to restart
PROC FM_PLAY_LIST_I_WANT_TO_RESTART()
	PRINTLN("[PLC] - FMMC_EOM - FM_PLAY_LIST_I_WANT_TO_RESTART()")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestart)
	FM_PLAY_LIST_SET_ME_AS_VOTED()
ENDPROC

//Set I want to restart
PROC FM_PLAY_LIST_I_WANT_TO_CONTINUE()
	PRINTLN("[PLC] - FMMC_EOM - FM_PLAY_LIST_I_WANT_TO_CONTINUE()")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToContinue)
	FM_PLAY_LIST_SET_ME_AS_VOTED()
ENDPROC

//Set I want to Quick restart
PROC FM_PLAY_LIST_I_WANT_TO_QUICK_RESTART()
	PRINTLN("[PLC] - FMMC_EOM - FM_PLAY_LIST_I_WANT_TO_QUICK_RESTART()")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuickRestart)
	FM_PLAY_LIST_SET_ME_AS_VOTED()
ENDPROC

//Set I want to Quick restart SwapTeams
PROC FM_PLAY_LIST_I_WANT_TO_QUICK_RESTART_SWAP()
	PRINTLN("[PLC] - FMMC_EOM - FM_PLAY_LIST_I_WANT_TO_QUICK_RESTART_SWAP()")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuickRestartSwap)
	FM_PLAY_LIST_SET_ME_AS_VOTED()
ENDPROC

//Set me as wanting to play next
PROC FM_PLAY_LIST_I_WANT_TO_PLAY_NEXT()
	PRINTLN("[PLC] - FMMC_EOM - FM_PLAY_LIST_I_WANT_TO_PLAY_NEXT()")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantNext)
	FM_PLAY_LIST_SET_ME_AS_VOTED()
ENDPROC

FUNC BOOL IS_PLAYER_READY_FOR_JOB_VOTES(PLAYER_INDEX piPlayer)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_IM_GOING_TO_JOB_VOTES)
ENDFUNC

FUNC BOOL HAS_PLAYER_VOTED_TO_CONTINUE(PLAYER_INDEX piPlayer)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToContinue)
ENDFUNC
FUNC BOOL HAS_PLAYER_VOTED_TO_PLAY_NEXT_MISSION(PLAYER_INDEX piPlayer)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantNext)
ENDFUNC
FUNC BOOL HAS_PLAYER_VOTED_TO_RESTART_MISSION(PLAYER_INDEX piPlayer)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestart)
ENDFUNC
FUNC BOOL HAS_PLAYER_VOTED_AT_END_OF_MISSION(PLAYER_INDEX piPlayer)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_Voted)
ENDFUNC

//Set me as wanting to quit
PROC FM_PLAY_LIST_I_WANT_TO_QUIT()
	PRINTLN("[PLC] - FMMC_EOM - FM_PLAY_LIST_I_WANT_TO_QUIT()")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuit)
ENDPROC

//Clean the control vars for the playlist.
PROC FM_PLAY_LIST_CLEAN_VARS()
	INT iMySlot = NATIVE_TO_INT(PLAYER_ID())
	PRINTLN("[PLC] - FMMC_EOM - FM_PLAY_LIST_CLEAN_VARS")
	CLEAR_BIT(GlobalplayerBD_FM[iMySlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_PlaylistAtEndOfMission)
	CLEAR_BIT(GlobalplayerBD_FM[iMySlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_Voted)
	CLEAR_BIT(GlobalplayerBD_FM[iMySlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestart)
	CLEAR_BIT(GlobalplayerBD_FM[iMySlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantNext)
	CLEAR_BIT(GlobalplayerBD_FM[iMySlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuit)
	CLEAR_BIT(GlobalplayerBD_FM[iMySlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn)
	CLEAR_BIT(GlobalplayerBD_FM[iMySlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_Voted_On_Menu)	
	CLEAR_BIT(GlobalplayerBD_FM[iMySlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuickRestart)
	CLEAR_BIT(GlobalplayerBD_FM[iMySlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuickRestartSwap)
ENDPROC

FUNC BOOL HAS_DEV_CONTINUE_BUTTON_BEEN_PRESSED()
		
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_DEV_CONTINUE_BUTTON_BEEN_PRESSED                            --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_MISSION_END_RESTART_BUTTON_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)			
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_END_RESTART_BUTTON_BEEN_PRESSED       	             --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_END_CONTINUE_BEEN_PRESSED()
		
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_END_CONTINUE_BEEN_PRESSED                            --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_END_QUIT_BEEN_PRESSED()
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_END_QUIT_BEEN_PRESSED                                --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_END_BACK_BEEN_PRESSED()
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_END_BACK_BEEN_PRESSED                                --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_END_BACK_BEEN_RELEASED()
	
	IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_END_BACK_BEEN_RELEASED                                --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_END_ADD_TO_PLAYLIST_BEEN_PRESSED()
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_END_ADD_TO_PLAYLIST_BEEN_PRESSED                     --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_END_QUIT_PLAYLIST_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_END_QUIT_PLAYLIST_BEEN_PRESSED                        --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_NO_BUTTON_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_NO_BUTTON_BEEN_PRESSED	                        			 --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// DON'T CHANGE THIS 
FUNC BOOL HAS_END_JOB_QUIT_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_END_JOB_QUIT_BEEN_PRESSED                        			 --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_YES_BUTTON_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_YES_BUTTON_BEEN_PRESSED	                        			 --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_QUICK_RESTART_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM -    HAS_MISSION_QUICK_RESTART_BEEN_PRESSED                        --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_QUICK_RESTART_SWAP_TEAMS_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_QUICK_RESTART_SWAP_TEAMS_BEEN_PRESSED                --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_END_RANDOM_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_END_RANDOM_BEEN_PRESSED                              --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_END_SHOW_SC_LB_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_END_SHOW_SC_LB_BEEN_PRESSED                          --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_END_SHOW_SC_LB_CO_DRIVER_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_END_SHOW_SC_LB_CO_DRIVER_BEEN_PRESSED                --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LEADERBOARD_RANDOM_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM -  HAS_LEADERBOARD_RANDOM_BEEN_PRESSED                             --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL HAS_LEADERBOARD_POST_TO_FACEBOOK_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM -  HAS_LEADERBOARD_POST_TO_FACEBOOK_BEEN_PRESSED                   --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_END_SPECTATE_BUTTON_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)			
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_MISSION_END_SPECTATE_BUTTON_BEEN_PRESSED       	             --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_END_VOTE_PLAYER_LIST_BUTTON_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)			
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_END_VOTE_PLAYER_LIST_BUTTON_BEEN_PRESSED       	             --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LEADERBOARD_VOTE_UP_BEEN_PRESSED()
	IF (NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y))	
	OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - LBD_COMMON - HAS_MISSION_END_VOTE_UP_BEEN_PRESSED                			 --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LEADERBOARD_VOTE_DOWN_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - LBD_COMMON - HAS_MISSION_END_VOTE_DOWN_BEEN_PRESSED                		 --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LEADERBOARD_NO_VOTE_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - LBD_COMMON - HAS_MISSION_END_NO_VOTE_BEEN_PRESSED                			 --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC WRITE_MISSION_LB_DATA_TO_GLOBALS(PLAYER_INDEX playerID, INT teamId,INT iPositionInLB,INT iscore,INT itime, INT ikills,INT ideaths,INT iHeadShots,MODEL_NAMES carmodel = DUMMY_MODEL_FOR_SCRIPT,MOD_COLOR_TYPE ecolourtype = MCT_METALLIC, INT colourselection = 0, INT iChallengeScore = 0)
	//Don't do this if it's a rounds mission
	IF IS_THIS_A_ROUNDS_MISSION()
	AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
	
		PRINTLN(" [PLC] WRITING MISSION DATA checking valid for PLAYER: ", NATIVE_TO_INT(playerID), " not called because IS_THIS_A_ROUNDS_MISSION")
		EXIT
	ENDIF
	
	NET_PRINT(" [PLY_BUG] WRITING MISSION DATA checking valid for PLAYER: ") NET_PRINT_INT(NATIVE_TO_INT(playerID)) NET_NL()
	INT i
	IF NOT g_TransitionSessionNonResetVars.sPLLB_Globals.binitalise
		FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
			g_TransitionSessionNonResetVars.sPLLB_Globals.PlayerID[i] = INVALID_PLAYER_INDEX()
			g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[i] = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("[PLC] - [PLY_BUG]  [LINK] WRITE_MISSION_LB_DATA_TO_GLOBALS, INVALID_PLAYER_INDEX ", i)
		ENDFOR
		g_TransitionSessionNonResetVars.sPLLB_Globals.binitalise = TRUE
	ENDIF
	
	IF playerID <> INVALID_PLAYER_INDEX()
		INT iplayer = NATIVE_TO_INT(playerID)
		IF IS_NET_PLAYER_OK(playerID,FALSE)
			g_TransitionSessionNonResetVars.sPLLB_Globals.PlayerID[iplayer] = playerID
			NET_PRINT(" [PLY_BUG] [LINK] WRITING MISSION DATA TO LB GLOBALS FOR PLAYER: ") NET_PRINT_INT(iplayer) NET_NL()
			g_TransitionSessionNonResetVars.sPLLB_Globals.GamerHandle[iplayer] = GET_GAMER_HANDLE_PLAYER(g_TransitionSessionNonResetVars.sPLLB_Globals.PlayerID[iplayer])
			g_TransitionSessionNonResetVars.sPLLB_Globals.iteamId[iplayer] = teamId
			NET_PRINT(" WRITING MISSION DATA TO LB GLOBALS iteam: ") NET_PRINT_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.iteamId[iplayer]) NET_NL()
			g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[iplayer] = iPositionInLB
			NET_PRINT(" WRITING MISSION DATA TO LB GLOBALS iPositionInLB: ") NET_PRINT_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[iplayer]) NET_NL()
			g_TransitionSessionNonResetVars.sPLLB_Globals.ikills[iplayer] = ikills
			NET_PRINT(" WRITING MISSION DATA TO LB GLOBALS ikills: ") NET_PRINT_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.ikills[iplayer]) NET_NL()
			g_TransitionSessionNonResetVars.sPLLB_Globals.ideaths[iplayer] = ideaths
			NET_PRINT(" WRITING MISSION DATA TO LB GLOBALS ideaths: ") NET_PRINT_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.ideaths[iplayer]) NET_NL()
			g_TransitionSessionNonResetVars.sPLLB_Globals.iHeadShots[iplayer] = iHeadShots
			NET_PRINT(" WRITING MISSION DATA TO LB GLOBALS iHeadShots: ") NET_PRINT_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.iHeadShots[iplayer]) NET_NL()
			g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[iplayer] = carmodel
			NET_PRINT(" WRITING MISSION DATA TO LB GLOBALS carmodel: ") NET_PRINT_INT(ENUM_TO_INT(carmodel)) NET_NL()
			g_TransitionSessionNonResetVars.sPLLB_Globals.iCarColourtype[iplayer] = ecolourtype
			NET_PRINT(" WRITING MISSION DATA TO LB GLOBALS colourtype: ") NET_PRINT_INT(ENUM_TO_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.iCarColourType[iplayer])) NET_NL()
			g_TransitionSessionNonResetVars.sPLLB_Globals.iCarColourselection[iplayer] = colourselection
			NET_PRINT(" WRITING MISSION DATA TO LB GLOBALS iCarColourselection: ") NET_PRINT_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.iCarColourSelection[iplayer]) NET_NL()
			g_TransitionSessionNonResetVars.sPLLB_Globals.iEventscore[iplayer] = iscore
			NET_PRINT(" WRITING MISSION DATA TO LB GLOBALS iEventscore : ") NET_PRINT_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.iEventscore[iplayer]) NET_NL()
			g_TransitionSessionNonResetVars.sPLLB_Globals.iEventtime[iplayer] = itime
			NET_PRINT(" WRITING MISSION DATA TO LB GLOBALS iEventtime: ") NET_PRINT_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.iEventtime[iplayer]) NET_NL()
			g_TransitionSessionNonResetVars.sPLLB_Globals.iChallengeScore[iplayer] = iChallengeScore
			NET_PRINT(" WRITING MISSION DATA TO LB GLOBALS iChallengeScore: ") NET_PRINT_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.iChallengeScore[iplayer]) NET_NL()
			
		ENDIF
	ENDIF

ENDPROC

CONST_INT ciMIN_NUM_PLAYERS_FOR_EXTRA_JP (NUM_NETWORK_PLAYERS/2)

FUNC INT GET_POINTS_FOR_POSITION(INT iposition, INT iLocalPlayer = -1)

	BOOL bExtraPoints // 2035121
	
	IF g_iNumOnPlaylist = -1
		g_iNumOnPlaylist = NETWORK_GET_NUM_PARTICIPANTS()
		PRINTLN("GET_POINTS_FOR_POSITION, g_iNumOnPlaylist = ", g_iNumOnPlaylist)
	ENDIF
	
	IF (g_iNumOnPlaylist > ciMIN_NUM_PLAYERS_FOR_EXTRA_JP)
		bExtraPoints = TRUE
	ENDIF

	INT iscore

	SWITCH iposition
 	
		CASE 1
			IF bExtraPoints
				iscore = 30
			ELSE
				iscore = 15
			ENDIF
		BREAK
		CASE 2
			IF bExtraPoints
				iscore = 25
			ELSE
				iscore = 12
			ENDIF
		BREAK
		CASE 3
			IF bExtraPoints
				iscore = 22
			ELSE
				iscore = 10
			ENDIF
		BREAK
		CASE 4
			IF bExtraPoints
				iscore = 20
			ELSE
				iscore = 8
			ENDIF
		BREAK
		CASE 5
			IF bExtraPoints
				iscore = 18
			ELSE
				iscore = 7
			ENDIF
		BREAK	
		CASE 6
			IF bExtraPoints
				iscore = 16
			ELSE
				iscore = 6
			ENDIF
		BREAK	
		CASE 7
			IF bExtraPoints
				iscore = 14
			ELSE
				iscore = 5
			ENDIF
		BREAK
		CASE 8
			IF bExtraPoints
				iscore = 13
			ELSE
				iscore = 4
			ENDIF
		BREAK
		CASE 9
			IF bExtraPoints
				iscore = 12
			ELSE
				iscore = 3
			ENDIF
		BREAK
		CASE 10
			IF bExtraPoints
				iscore = 11
			ELSE
				iscore = 2
			ENDIF
		BREAK
		CASE 11
			IF bExtraPoints
				iscore = 10
			ELSE
				iscore = 1
			ENDIF
		BREAK
		CASE 12
			IF bExtraPoints
				iscore = 9
			ELSE
				iscore = 1
			ENDIF
		BREAK
		CASE 13
			IF bExtraPoints
				iscore = 8
			ELSE
				iscore = 1
			ENDIF
		BREAK
		CASE 14
			IF bExtraPoints
				iscore = 7
			ELSE
				iscore = 1
			ENDIF
		BREAK
		CASE 15
			IF bExtraPoints
				iscore = 6
			ELSE
				iscore = 1
			ENDIF
		BREAK
		CASE 16
			IF bExtraPoints
				iscore = 5
			ELSE
				iscore = 1
			ENDIF
		BREAK
		CASE 17
			IF bExtraPoints
				iscore = 4
			ELSE
				iscore = 1
			ENDIF
		BREAK
		CASE 18
			IF bExtraPoints
				iscore = 3
			ELSE
				iscore = 1
			ENDIF
		BREAK
		CASE 19
			IF bExtraPoints
				iscore = 2
			ELSE
				iscore = 1
			ENDIF
		BREAK
		CASE 20
			iscore = 1
		BREAK
		CASE 21
			iscore = 1
		BREAK
		CASE 22
			iscore = 1
		BREAK
		CASE 23
			iscore = 1
		BREAK
		CASE 24
			iscore = 1
		BREAK
		CASE 25
			iscore = 1
		BREAK
		CASE 26
			iscore = 1
		BREAK
		CASE 27
			iscore = 0
		BREAK
		CASE 28 
			iscore = 0
		BREAK
		CASE 29
			iscore = 0
		BREAK
		CASE 30
			iscore = 0
		BREAK
		DEFAULT
			iscore = 0
		BREAK
		
	ENDSWITCH
	
	// MVP bonus point
	IF iLocalPlayer <> -1
		IF iLocalPlayer = GlobalplayerBD_FM[iLocalPlayer].sPlaylistVars.iMVPThisJob
			iscore = (iscore +1)
		ENDIF
	ENDIF
	
	PRINTLN("GET_POINTS_FOR_POSITION, iscore = ", iscore)

	RETURN iscore
ENDFUNC

PROC CLIENT_CLEAR_LB_DATA_START_OF_MISSION(PLAYLIST_LB_VARIABLES& sPLLB_Vars)
	
	NET_PRINT(" [PLY_BUG] [LINK] CLIENT_CLEAR_LB_DATA_START_OF_MISSION ") NET_NL()
	
	PLAYLIST_LB_VARIABLES sPLLB_VarsEmpty
	INT i
	
	sPLLB_Vars = sPLLB_VarsEmpty
	
	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
		g_TransitionSessionNonResetVars.sPLLB_Globals.PlayerID[i] = INVALID_PLAYER_INDEX()
		g_TransitionSessionNonResetVars.sPLLB_Globals.iteamId[i] = 0
		g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[i] = 0
		g_TransitionSessionNonResetVars.sPLLB_Globals.ikills[i] = 0
		g_TransitionSessionNonResetVars.sPLLB_Globals.ideaths[i] = 0
		g_TransitionSessionNonResetVars.sPLLB_Globals.iHeadShots[i] = 0
		g_TransitionSessionNonResetVars.sPLLB_Globals.Carmodel[i] = DUMMY_MODEL_FOR_SCRIPT
	ENDFOR
	g_TransitionSessionNonResetVars.sPLLB_Globals.binitalise = FALSE
	
ENDPROC

////////////////////////////////////////////////////
///    
///    Mission loaded
///    
//SEt mission has loaded
PROC SET_PLAYLIST_MISSION_HAS_LOADED()
	PRINTLN("[PLC] - SET_PLAYLIST_MISSION_HAS_LOADED")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_BIT_SET_LOADED)
ENDPROC
//Clear mission has loaded
PROC CLEAR_PLAYLIST_MISSION_HAS_LOADED()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_MISSION_HAS_LOADED")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_BIT_SET_LOADED)
ENDPROC
//Get mission has loaded status
FUNC  BOOL HAS_PLAYLIST_MISSION_HAS_LOADED()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_BIT_SET_LOADED)
ENDFUNC

PROC SET_PLAYLIST_IS_RESTARTING()
	PRINTLN("[PLC] - SET_PLAYLIST_IS_RESTARTING")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_CURRENTLY_RESTARTING)
ENDPROC
PROC CLEAR_PLAYLIST_IS_RESTARTING()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_IS_RESTARTING")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_CURRENTLY_RESTARTING)
ENDPROC
FUNC  BOOL IS_PLAYLIST_IS_RESTARTING()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_CURRENTLY_RESTARTING)
ENDFUNC

PROC SET_SCTV_SAID_TO_CONTINUE_PLAYLIST()
	PRINTLN("[PLC] - SET_SCTV_SAID_TO_CONTINUE_PLAYLIST")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_SCTV_SAID_TO_CONTINUE_PLAYLIST)
ENDPROC
PROC CLEAR_SCTV_SAID_TO_CONTINUE_PLAYLIST()
	PRINTLN("[PLC] - CLEAR_SCTV_SAID_TO_CONTINUE_PLAYLIST")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_SCTV_SAID_TO_CONTINUE_PLAYLIST)
ENDPROC
FUNC  BOOL HAS_SCTV_SAID_TO_CONTINUE_PLAYLIST()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_SCTV_SAID_TO_CONTINUE_PLAYLIST)
ENDFUNC



//Set that this is the last mission of a playlist
PROC SET_THAT_THIS_IS_THE_LAST_PLAYLIST_MISSION()
	PRINTLN("[PLC] - SET_THAT_THIS_IS_THE_LAST_PLAYLIST_MISSION")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_THE_LAST_PLAYLIST_MISSION)
ENDPROC
//Clear that this is the last mission of a playlist
PROC CLEAR_THAT_THIS_IS_THE_LAST_PLAYLIST_MISSION()
	PRINTLN("[PLC] - CLEAR_THAT_THIS_IS_THE_LAST_PLAYLIST_MISSION")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_THE_LAST_PLAYLIST_MISSION)
ENDPROC
//Is this the last mission of a playlist
FUNC BOOL IS_THIS_THE_LAST_PLAYLIST_MISSION()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_THE_LAST_PLAYLIST_MISSION)
ENDFUNC

//Set that the playlist should save stats
PROC SET_PLAYLIST_SAVE_STATS()
	PRINTLN("[PLC] - SET_PLAYLIST_SAVE_STATS")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_CALL_SAVE_STATS)
ENDPROC
//Clear that the playlist should save stats
PROC CLEAR_PLAYLIST_SAVE_STATS()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_SAVE_STATS")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_CALL_SAVE_STATS)
ENDPROC
//Should the playlist save stats
FUNC BOOL SHOULD_PLAYLIST_SAVE_STATS()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_CALL_SAVE_STATS)
ENDFUNC




PROC SET_PLAYLIST_WAIT_FOR_LEADER_BOARD_TO_FINISH()
	PRINTLN("[PLC] - SET_PLAYLIST_WAIT_FOR_LEADER_BOARD_TO_FINISH")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WaitForLB)
ENDPROC
PROC CLEAR_PLAYLIST_WAIT_FOR_LEADER_BOARD_TO_FINISH()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_WAIT_FOR_LEADER_BOARD_TO_FINISH")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WaitForLB)
ENDPROC
FUNC  BOOL SHOULD_WAIT_FOR_PLAYLIST_LEADER_BOARD_TO_FINISH()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WaitForLB)
ENDFUNC
FUNC BOOL SHOULD_THIS_PLAYER_WAIT_FOR_PLAYLIST_LEADER_BOARD_TO_FINISH(PLAYER_INDEX piPassed)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPassed)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WaitForLB)
ENDFUNC


PROC SET_PLAYLIST_JIPPED_WHEN_ON_PLAYLIST_LB()
	PRINTLN("[PLC] - SET_PLAYLIST_JIPPED_WHEN_ON_PLAYLIST_LB")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_JIPED_ON_LB)
ENDPROC
PROC CLEAR_PLAYLIST_JIPPED_WHEN_ON_PLAYLIST_LB()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_JIPPED_WHEN_ON_PLAYLIST_LB")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_JIPED_ON_LB)
ENDPROC
FUNC BOOL DID_THIS_PLAYER_PLAYLIST_JIPPED_WHEN_ON_PLAYLIST_LB(PLAYER_INDEX piPassed)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPassed)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_JIPED_ON_LB)
ENDFUNC


PROC SET_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
	PRINTLN("[PLC] - [PLC] -SET_PLAYLIST_HAS_DONE_INITIAL_TRANSITION")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_HAS_DONE_INITIAL_TRANSITION)
	
	// Always set our global for bad sport detection
	SET_TRANSITION_SESSIONS_PLAYLIST_VALID_FOR_BAD_SPORT()
ENDPROC
PROC CLEAR_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
	PRINTLN("[PLC] - [PLC] -CLEAR_PLAYLIST_HAS_DONE_INITIAL_TRANSITION")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_HAS_DONE_INITIAL_TRANSITION)
ENDPROC
FUNC  BOOL HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_HAS_DONE_INITIAL_TRANSITION)
ENDFUNC

PROC SET_PLAYLIST_JIPPED_INTO_CORONA()
	PRINTLN("[PLC] - [PLC] -SET_PLAYLIST_JIPPED_INTO_CORONA")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_JIPPED_INTO_CORONA)
ENDPROC
PROC CLEAR_PLAYLIST_JIPPED_INTO_CORONA()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_JIPPED_INTO_CORONA")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_JIPPED_INTO_CORONA)
ENDPROC
FUNC  BOOL PLAYLIST_JIPPED_INTO_CORONA()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_JIPPED_INTO_CORONA)
ENDFUNC

PROC SET_PLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN()
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN)
		PRINTLN("[PLC] - SET_PLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN")
	ENDIF
	#ENDIF
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN)
ENDPROC
PROC CLEAR_PLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN)
		PRINTLN("[PLC] - CLEAR_PLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN)
ENDPROC
FUNC  BOOL PLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN)
ENDFUNC

PROC SET_PLAYLIST_LEADERBOARD_BEING_SETTING_UP()
	PRINTLN("[PLC] -SET_PLAYLIST_LEADERBOARD_BEING_SETTING_UP")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_LEADERBOARD_BEING_SETTING_UP)
ENDPROC
PROC CLEAR_PLAYLIST_LEADERBOARD_BEING_SETTING_UP()
	PRINTLN("[PLC] -CLEAR_PLAYLIST_LEADERBOARD_BEING_SETTING_UP")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_LEADERBOARD_BEING_SETTING_UP)
ENDPROC
FUNC  BOOL IS_PLAYLIST_LEADERBOARD_BEING_SETTING_UP()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_LEADERBOARD_BEING_SETTING_UP)
ENDFUNC

PROC SET_PLAYLIST_BETWEEN_LEADERBOARDS()
	PRINTLN("[PLC] -SET_PLAYLIST_BETWEEN_LEADERBOARDS")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_BETWEEN_LEADERBOARDS)
ENDPROC
PROC CLEAR_PLAYLIST_BETWEEN_LEADERBOARDS()
	IF IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_BETWEEN_LEADERBOARDS)
		PRINTLN("[PLC] -CLEAR_PLAYLIST_BETWEEN_LEADERBOARDS")
		CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_BETWEEN_LEADERBOARDS)
	ENDIF
ENDPROC
FUNC  BOOL IS_PLAYLIST_BETWEEN_LEADERBOARDS()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_BETWEEN_LEADERBOARDS)
ENDFUNC

PROC SET_PLAYLIST_MISSION_FAILED_TO_LOAD()
	PRINTLN("[PLC] -SET_PLAYLIST_MISSION_FAILED_TO_LOAD")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_MISSION_FAILED_TO_LOAD)
ENDPROC
PROC CLEAR_PLAYLIST_MISSION_FAILED_TO_LOAD()
	PRINTLN("[PLC] -CLEAR_PLAYLIST_MISSION_FAILED_TO_LOAD")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_MISSION_FAILED_TO_LOAD)
ENDPROC
FUNC  BOOL DID_PLAYLIST_MISSION_FAIL_TO_LOAD()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_MISSION_FAILED_TO_LOAD)
ENDFUNC


PROC SET_PLAYLIST_HAS_BEEN_SETUP()
	PRINTLN("[PLC] -SET_PLAYLIST_HAS_BEEN_SETUP")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_HAS_BEEN_SET_UP)
ENDPROC
//Clear mission has loaded
PROC CLEAR_PLAYLIST_HAS_BEEN_SETUP()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_HAS_BEEN_SETUP")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_HAS_BEEN_SET_UP)
ENDPROC
//Get mission has loaded status
FUNC  BOOL HAS_PLAYLIST_HAS_BEEN_SETUP()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_HAS_BEEN_SET_UP)
ENDFUNC



//Set the local player on a challenge
PROC SET_JOINED_PLAYLIST_AFTER_INITILISATION()
	PRINTLN("[PLC] - SET_JOINED_PLAYLIST_AFTER_INITILISATION")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_JOINED_AFTER_LB_INITILISATION)
ENDPROC
//Clear the local player on a challenge
PROC CLEAR_JOINED_PLAYLIST_AFTER_INITILISATION()
	PRINTLN("[PLC] - CLEAR_JOINED_PLAYLIST_AFTER_INITILISATION")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_JOINED_AFTER_LB_INITILISATION)
ENDPROC
//Retunrs if a player is on a Challenge or not!
FUNC BOOL DID_PLAYER_JOIN_A_PLAYLIST_AFTER_INITILISATION(PLAYER_INDEX playerID)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_JOINED_AFTER_LB_INITILISATION)
ENDFUNC
////////////////////////////////////////////////////
///    
///    Qualifing playlist
///    
//Set that this is a Qualifing playlist
PROC SET_PLAYLIST_IS_A_QUALIFYING_PLAYLIST()
	PRINTLN("[PLC] - SET_PLAYLIST_IS_A_QUALIFYING_PLAYLIST")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_QUALIFYING_PLAYLIS)
ENDPROC
//Clear that this is a Qualifing playlist
PROC CLEAR_PLAYLIST_IS_A_QUALIFYING_PLAYLIST()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_IS_A_QUALIFYING_PLAYLIST")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_QUALIFYING_PLAYLIS)
ENDPROC
//Is this a Qualifing playlist
FUNC  BOOL IS_PLAYLIST_IS_A_QUALIFYING_PLAYLIST()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_QUALIFYING_PLAYLIS)
ENDFUNC

////////////////////////////////////////////////////
///    
///    SCTV Controlled playlist
///    
//Set that this is a SCTV Controlled playlist
PROC SET_PLAYLIST_IS_A_SCTV_CONTROLLED_PLAYLIST()
	PRINTLN("[PLC] - SET_PLAYLIST_IS_A_SCTV_CONTROLLED_PLAYLIST")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_SCTV_CONTROLLED_PLAYLIST)
ENDPROC
//Clear that this is a SCTV Controlled playlist
PROC CLEAR_PLAYLIST_IS_A_SCTV_CONTROLLED_PLAYLIST()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_IS_A_SCTV_CONTROLLED_PLAYLIST")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_SCTV_CONTROLLED_PLAYLIST)
ENDPROC
//Is this a SCTV Controlled playlist
FUNC  BOOL IS_PLAYLIST_IS_A_SCTV_CONTROLLED_PLAYLIST()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_SCTV_CONTROLLED_PLAYLIST)
ENDFUNC

////////////////////////////////////////////////////
///    
///    Tournament playlist
///    
//Set that this is a Tournament playlist
PROC SET_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
	PRINTLN("[PLC] - SET_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_TOURNAMENT_PLAYLIS)
ENDPROC
//Clear that this is a Tournament playlist
PROC CLEAR_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_TOURNAMENT_PLAYLIS)
ENDPROC
//Is this a Tournament playlist
FUNC  BOOL IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_TOURNAMENT_PLAYLIS)
ENDFUNC
////////////////////////////////////////////////////
///    
///    LiveStreaming playlist
///    
//Set that this is a LiveStreaming playlist
PROC SET_PLAYLIST_IS_A_LIVESTREAM_PLAYLIST()
	PRINTLN("[PLC] - SET_PLAYLIST_IS_A_LIVESTREAM_PLAYLIST")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_LIVESTREAM_PLAYLIS)
ENDPROC
//Clear that this is a LiveStreaming playlist
PROC CLEAR_PLAYLIST_IS_A_LIVESTREAM_PLAYLIST()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_IS_A_LIVESTREAM_PLAYLIST")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_LIVESTREAM_PLAYLIS)
ENDPROC
//Is this a LiveStreaming playlist
FUNC  BOOL IS_PLAYLIST_IS_A_LIVESTREAM_PLAYLIST()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_IS_A_LIVESTREAM_PLAYLIS)
ENDFUNC

////////////////////////////////////////////////////
///    
///    Setting challenge
///    
//Set challenge setting time
PROC SET_PLAYLIST_SETTING_CHALLENGE_TIME()
	PRINTLN("[PLC] - SET_PLAYLIST_SETTING_CHALLENGE_TIME")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_SETTING_CHALANGE_TIME)
ENDPROC
//Clear challenge setting time
PROC CLEAR_PLAYLIST_SETTING_CHALLENGE_TIME()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_SETTING_CHALLENGE_TIME")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_SETTING_CHALANGE_TIME)
ENDPROC
//Get challenge setting time status
FUNC  BOOL IS_PLAYLIST_SETTING_CHALLENGE_TIME()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_SETTING_CHALANGE_TIME)
ENDFUNC

////////////////////////////////////////////////////
///    
///   Trtying to beat challenge
///    
//set doing challenge status
PROC SET_PLAYLIST_DOING_CHALLENGE()
	PRINTLN("[PLC] - SET_PLAYLIST_DOING_CHALLENGE")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DOING_CHALLENGE)
ENDPROC
//Clear doing challenge status
PROC CLEAR_PLAYLIST_DOING_CHALLENGE()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_DOING_CHALLENGE")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DOING_CHALLENGE)
ENDPROC
//Get doing challenge status
FUNC  BOOL IS_PLAYLIST_DOING_CHALLENGE()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DOING_CHALLENGE)
ENDFUNC

////////////////////////////////////////////////////
///    
///   PLAYING HEAD TO HEAD
///    
//set doing challenge status
PROC SET_PLAYLIST_DOING_HEAD_TO_HEAD()
	PRINTLN("[PLC] - SET_PLAYLIST_DOING_HEAD_TO_HEAD")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DOING_HEAD_TO_HEAD)
ENDPROC
//Clear doing HEAD_TO_HEAD status
PROC CLEAR_PLAYLIST_DOING_HEAD_TO_HEAD()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_DOING_HEAD_TO_HEAD")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DOING_HEAD_TO_HEAD)
ENDPROC
//Get doing HEAD_TO_HEAD status
FUNC BOOL IS_PLAYLIST_DOING_HEAD_TO_HEAD()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DOING_HEAD_TO_HEAD)
ENDFUNC
//Set My playlist team
PROC SET_MY_PLAYLIST_TEAM(INT iTeam)
	PRINTLN("[PLC] - SET_MY_PLAYLIST_TEAM - ", iTeam)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlaylistTeam = iTeam
ENDPROC
//clear My playlist team
PROC CLEAR_MY_PLAYLIST_TEAM()
	PRINTLN("[PLC] - CLEAR_MY_PLAYLIST_TEAM - ")
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlaylistTeam = -1
ENDPROC
//Get My pals playlist team
FUNC INT GET_PLAYERS_PLAYLIST_TEAM(PLAYER_INDEX piPlayer)
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].sPlaylistVars.iPlaylistTeam
ENDFUNC
//Get My playlist team
FUNC INT GET_MY_PLAYLIST_TEAM()
	RETURN GET_PLAYERS_PLAYLIST_TEAM(PLAYER_ID())
ENDFUNC

////////////////////////////////////////////////////
///    
///   Trtying to beat challenge
///    
//set doing challenge status
PROC SET_PLAYLIST_DONE_FIRST_MISSION()
	PRINTLN("[PLC] - SET_PLAYLIST_DONE_FIRST_MISSION")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DONE_FIRST_MISSION)
ENDPROC
//Clear doing challenge status
PROC CLEAR_PLAYLIST_DONE_FIRST_MISSION()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_DONE_FIRST_MISSION")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DONE_FIRST_MISSION)
ENDPROC
//Get doing challenge status
FUNC  BOOL HAVE_DONE_FIRST_PLAYLIST_MISSION()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DONE_FIRST_MISSION)
ENDFUNC

PROC SET_PLAYLIST_LAUNCHING_CONTROLLER_SCRIPT()
	#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_LAUNCHING_CONTROLLER_SCRIPT)
			PRINTLN("[PLC] - SET_PLAYLIST_LAUNCHING_CONTROLLER_SCRIPT")
		ENDIF
	#ENDIF
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_LAUNCHING_CONTROLLER_SCRIPT)
ENDPROC
PROC CLEAR_PLAYLIST_LAUNCHING_CONTROLLER_SCRIPT()
	#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_LAUNCHING_CONTROLLER_SCRIPT)
			PRINTLN("[PLC] - CLEAR_PLAYLIST_LAUNCHING_CONTROLLER_SCRIPT")
		ENDIF
	#ENDIF
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_LAUNCHING_CONTROLLER_SCRIPT)
ENDPROC
FUNC  BOOL IS_PLAYLIST_LAUNCHING_CONTROLLER_SCRIPT()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_LAUNCHING_CONTROLLER_SCRIPT)
ENDFUNC

////////////////////////////////////////////////////
///    
///   Setting the playlist shuffeled.
///   
//Set playlist shuffeled status
PROC SET_PLAYLIST_IS_SHUFFELED()
	PRINTLN("[PLC] - SET_PLAYLIST_IS_SHUFFELED")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_BIT_SET_SHUFFELED)
ENDPROC
//Clear playlist shuffeled status
PROC CLEAR_PLAYLIST_IS_SHUFFELED()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_IS_SHUFFELED")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_BIT_SET_SHUFFELED)
ENDPROC
//Get playlist shuffeled status
FUNC  BOOL IS_PLAYLIST_SHUFFELED()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_BIT_SET_SHUFFELED)
ENDFUNC

//Make the bit set 0!
PROC CLEAR_PLAYLIST_BITSET()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_BITSET")
	g_sCurrentPlayListDetails.iPlayListBitSet = 0
ENDPROC

/// PURPOSE: Returns TRUE if you can quit the playlist without abandoning players
FUNC BOOL IS_TRANSITION_SESSIONS_PLAYLIST_SAFE_TO_QUIT()
	
	IF IS_TRANSITION_SESSIONS_PLAYLIST_VALID_FOR_BAD_SPORT()
		IF NOT HAS_TRANSITION_SESSIONS_PLAYLIST_FINISHED()
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				RETURN FALSE
			ENDIF
			IF IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
			AND NETWORK_GET_ACTIVITY_PLAYER_NUM(TRUE) <= 0
				RETURN TRUE
			ENDIF
			
			IF NETWORK_GET_ACTIVITY_PLAYER_NUM(FALSE) > 1
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HANDLE_THUMB_VOTE_INPUT(INT& iVoteBitset, BOOL bTimeOut)
	
	IF NOT IS_WARNING_MESSAGE_ACTIVE()
	
		IF HAS_LEADERBOARD_VOTE_UP_BEEN_PRESSED()
			SET_BIT(iVoteBitset, ciRATINGS_VOTED)
			SET_BIT(iVoteBitset, ciRATINGS_VOTED_UP)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP) // for counting votes
			PRINTLN("[THUMBS], HAS_THUMB_VOTED, ESSRV_THUMBS_UP")
			PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
			RETURN TRUE
		ENDIF
		
		IF HAS_LEADERBOARD_VOTE_DOWN_BEEN_PRESSED()
			SET_BIT(iVoteBitset, ciRATINGS_VOTED)
			CLEAR_BIT(iVoteBitset, ciRATINGS_VOTED_UP)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN) // for counting votes
			PRINTLN("[THUMBS], HAS_THUMB_VOTED, ESSRV_THUMBS_DOWN")
			PLAY_SOUND_FRONTEND(-1,"ERROR","HUD_FRONTEND_DEFAULT_SOUNDSET")
			RETURN TRUE
		ENDIF
		
		IF HAS_LEADERBOARD_NO_VOTE_BEEN_PRESSED()
		OR bTimeout
			SET_BIT(iVoteBitset, ciRATINGS_VOTED)
			SET_BIT(iVoteBitset, ciRATINGS_NO_VOTE)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
			PRINTLN("[THUMBS] ,HAS_THUMB_VOTED, ESSRV_TIMEOUT / ESSRV_ABSTAIN")
			PLAY_SOUND_FRONTEND(-1,"TOGGLE_ON","HUD_FRONTEND_DEFAULT_SOUNDSET")
			RETURN TRUE
		ENDIF
		
	ELSE
		PRINTLN("[THUMBS] ,HAS_THUMB_VOTED, WARNING SCREEN IS SHOWING")
	ENDIF
	
	IF bTimeout
		SET_BIT(iVoteBitset, ciRATINGS_VOTED)
		SET_BIT(iVoteBitset, ciRATINGS_NO_VOTE)
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
		PRINTLN("[THUMBS] ,HAS_THUMB_VOTED, ESSRV_TIMEOUT / ESSRV_ABSTAIN")
		PLAY_SOUND_FRONTEND(-1,"TOGGLE_ON","HUD_FRONTEND_DEFAULT_SOUNDSET")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_VOTE_RP_AWARD()
	RETURN g_sMPTunables.ixp_tuneable_vote_for_content
ENDFUNC

FUNC INT PRINT_VOTE_TICKER(INT iReward)

    INT iReturn = -1

    BEGIN_TEXT_COMMAND_THEFEED_POST("LBD_VOTE_TICK")        
		ADD_TEXT_COMPONENT_INTEGER(iReward)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER_WITH_TOKENS(FALSE)

	PRINTLN("[PLC] - PRINT_VOTE_TICKER LBD_VOTE_TICK VOTE_RP_AWARD = ", iReward)
    
    RETURN(iReturn)
ENDFUNC

PROC REWARD_RP_FOR_VOTING()
	INT iPlayer
	iPlayer = NATIVE_TO_INT(PLAYER_ID())
	XP_TYPE eXpType = eXPTYPE_STANDARD
	INT iReward = GET_VOTE_RP_AWARD()
	
	IF GET_CURRENT_GAMEMODE() <> GAMEMODE_CREATOR
		IF iReward > 0
			IF iPlayer <> -1
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayer].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
				OR IS_BIT_SET(GlobalplayerBD_FM[iPlayer].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
					PRINT_VOTE_TICKER(iReward)
					GIVE_LOCAL_PLAYER_XP(eXpType, "REWVOTEXP", XPTYPE_SOCIALCLUB, XPCATEGORY_VOTE_REWARD, iReward, 1)
					PRINTLN("[PLC] - REWARD_RP_FOR_VOTING ", iReward)
				ELSE
					PRINTLN("[PLC] - REWARD_RP_FOR_VOTING, DID_NOT_VOTE ")
				ENDIF
			ELSE
				PRINTLN("[PLC] - REWARD_RP_FOR_VOTING, iPlayer_INVALID ")
			ENDIF
		ELSE
			PRINTLN("[PLC] - REWARD_RP_FOR_VOTING, iReward =  ", iReward)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BYPASS_LIKE_DISLIKE_LOGIC()
	// 1865664
		IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
		
			RETURN TRUE
		ENDIF

	IF IS_PLAYER_SPECTATING(PLAYER_ID())
	
		RETURN TRUE
	ENDIF

	IF IS_THIS_A_ROUNDS_MISSION()
	AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

CONST_INT tvsWaitForVote 	0
CONST_INT tvsRefreshHelp 	1
CONST_INT tvsWaitForSave 	2
CONST_INT tvsComplete 		3

PROC DO_THUMB_VOTING(SAVE_OUT_UGC_PLAYER_DATA_VARS &sSaveOutVars, FMMC_EOM_DETAILS &sEndOfMission, BOOL bDoneSaveAlready, BOOL bTimerExpired, BOOL bPlaylistVate = FALSE)

	IF IS_LOCAL_PLAYER_SPECTATOR()
	
		EXIT
	ENDIF
	
	BOOL bBypass = SHOULD_BYPASS_LIKE_DISLIKE_LOGIC()

	SWITCH sEndOfMission.iThumbStage
		
		CASE tvsWaitForVote
			IF sEndOfMission.iProgress = ciEND_OF_MISSION_RATE_JOB
			OR bPlaylistVate
				IF bBypass
					IF bTimerExpired
						PRINTLN("[UGC] DO_THUMB_VOTING, bBypass = TRUE, ciRATINGS_NO_VOTE ")
						SET_BIT(sSaveOutVars.iBitSet, ciRATINGS_RUN_UP_LOAD)
						SET_BIT(sSaveOutVars.iBitSet, ciRATINGS_NO_VOTE)
						sEndOfMission.iThumbStage = tvsRefreshHelp
					ELSE
						PRINTLN("[UGC] DO_THUMB_VOTING, bTimerExpired = FALSE ")
					ENDIF
				ELSE
					IF HANDLE_THUMB_VOTE_INPUT(sSaveOutVars.iBitSet, bTimerExpired)
						SET_BIT(sSaveOutVars.iBitSet, ciRATINGS_RUN_UP_LOAD)
						PRINTLN("[PLC] - FMMC EOM - DO_THUMB_VOTING tvsWaitForVote iEomProgress = ciEND_OF_MISSION_RATE_JOB HANDLE_THUMB_VOTE_INPUT returned true")
						sEndOfMission.iThumbStage = tvsRefreshHelp
						THEFEED_FORCE_RENDER_ON()
						REWARD_RP_FOR_VOTING()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE tvsWaitForSave
			IF bDoneSaveAlready
			OR SAVE_OUT_UGC_PLAYER_DATA(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName, sSaveOutVars)
				#IF IS_DEBUG_BUILD
				PRINTLN("[UGC] DO_THUMB_VOTING, DONE ")
				IF bDoneSaveAlready
					PRINTLN("[UGC] DO_THUMB_VOTING, bDoneSaveAlready ")
				ENDIF
				#ENDIF
				sEndOfMission.iThumbStage = tvsComplete
			ENDIF
		BREAK
		DEFAULT
			//PRINTLN("[PLC] - FMMC EOM - DO_THUMB_VOTING DEFAULT")
		BREAK	
	ENDSWITCH
ENDPROC

CONST_INT ciMINIMUM_LBD_TIMEOUT 5000
FUNC BOOL ON_BOOKMARK_ERROR_SCREEN(FMMC_EOM_DETAILS& sEndOfMission)
	RETURN (sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_ERROR)
ENDFUNC
FUNC BOOL ON_BOOKMARK_CONFIRM_SCREEN(FMMC_EOM_DETAILS& sEndOfMission)
	RETURN (sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_CONFIRM_SCREEN)
ENDFUNC

PROC MAINTAIN_ADD_BOOKMARK_BUTTON_PLAYLIST(FMMC_EOM_DETAILS& sEndOfMission)
	SWITCH sEndOfMission.iAddToPlaylistProgress
		CASE LBD_BOOKMARK_INITIAL
			//And if the button has just been pressed
			IF HAS_MISSION_END_ADD_TO_PLAYLIST_BEEN_PRESSED()
				sEndOfMission.iRaceStage = 0
				PRINTLN("[LB] [BOOK] sEndOfMission.iAddToPlaylistProgress = HAS_MISSION_END_ADD_TO_PLAYLIST_BEEN_PRESSED ")						
				sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_CONFIRM_SCREEN	
				PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
				PRINTLN("[LB] [BOOK] sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_CONFIRM_SCREEN ")
			ENDIF									   
		BREAK
		CASE LBD_BOOKMARK_CONFIRM_SCREEN
			// Draw confirm black screen
			SET_WARNING_MESSAGE_WITH_HEADER("FMMC_CLDEAD", "FMMC_ADDBKQ", (FE_WARNING_YES | FE_WARNING_NO))
			IF HAS_YES_BUTTON_BEEN_PRESSED()
				PRINTLN("[LB] [BOOK] MAINTAIN_ADD_BOOKMARK_BUTTON - HAS_YES_BUTTON_BEEN_PRESSED")						
				//ADD_CURRENT_MISSION_TO_PLAYLIST_FRONT_END()
				PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FREEMODE_SOUNDSET")
				sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKLMARK_WAIT_FOR_CLEAR	
				sEndOfMission.bAddBookMark = FALSE	//TRUE
				sEndOfMission.bRefresh = TRUE
				PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
				PRINTLN("[LB] [BOOK] sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_ADD ")
			ENDIF
			IF HAS_NO_BUTTON_BEEN_PRESSED()
				PRINTLN("[LB] [BOOK] sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_INITIAL ")
				PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET")
				sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_INITIAL
			ENDIF
		BREAK
		//Wait till it's finshed.
		CASE LBD_BOOKLMARK_WAIT_FOR_CLEAR
			IF NOT UGC_IS_MODIFYING()
				sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_ADD	
				PRINTLN("[LB] [BOOK] sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_ADD ")
			ELSE
				PRINTLN("[LB] [BOOK] LBD_BOOKLMARK_WAIT_FOR_CLEAR - UGC_IS_MODIFYING")
			ENDIF
		BREAK
		//Add it
		CASE LBD_BOOKMARK_ADD
			IF UGC_SET_BOOKMARKED(g_FMMC_STRUCT.tl31LoadedContentID, TRUE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION))
				sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_CONFIRM
				PRINTLN("[LB] [BOOK] sEndOfMission.iAddToPlaylistProgress = UGC_SET_BOOKMARKED = TRUE - LBD_BOOKMARK_CONFIRM")
			ELSE
				PRINTLN("[LB] [BOOK] sEndOfMission.iAddToPlaylistProgress - UGC_SET_BOOKMARKED = FALSE")						
			ENDIF
		BREAK
		//Wait for confirm
		CASE LBD_BOOKMARK_CONFIRM
			IF UGC_HAS_MODIFY_FINISHED()
				IF UGC_DID_MODIFY_SUCCEED()		
				#IF IS_DEBUG_BUILD
				AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_FailAddBookMark")
				#ENDIF
					//sEndOfMission.bAddBookMark = FALSE	//TRUE
					//sEndOfMission.bRefresh = TRUE
					sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_DONE
					PRINTLN("[LB] [BOOK] sEndOfMission.iAddToPlaylistProgress = UGC_SET_BOOKMARKED = TRUE - LBD_BOOKMARK_DONE")			
				ELSE
					sEndOfMission.iCloudFailStatus = UGC_GET_MODIFY_RESULT()							
					PRINTLN("[LB] [BOOK] sEndOfMission.iAddToPlaylistProgress = sEndOfMission.iCloudFailStatus = ", GET_UGC_QUERY_RESULT_AS_A_STRING(sEndOfMission.iCloudFailStatus))							
					sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_ERROR
					PRINTLN("[LB] [BOOK] sEndOfMission.iAddToPlaylistProgress = UGC_SET_BOOKMARKED = TRUE - LBD_BOOKMARK_ERROR")							
				ENDIF
			ENDIF
		BREAK
		//deal with the error if we failed
		CASE LBD_BOOKMARK_ERROR
			IF sEndOfMission.iCloudFailStatus = UGC_ERROR_UNEXPECTED_RESULT
			OR sEndOfMission.iCloudFailStatus = UGC_ERROR_NOTALLOWED_MAXCREATED
				SET_WARNING_MESSAGE_WITH_HEADER("FMMC_CLDEAD", "FMMC_ADDBKQT", FE_WARNING_OK)
			ELSE 
				SET_WARNING_MESSAGE_WITH_HEADER("FMMC_CLDEAD", "FMMC_ADDBKQF", FE_WARNING_OK)
			ENDIF
			IF HAS_YES_BUTTON_BEEN_PRESSED()	
				sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_DONE
				PRINTLN("[LB] [BOOK] sEndOfMission.iAddToPlaylistProgress = UGC_SET_BOOKMARKED = TRUE - LBD_BOOKMARK_DONE")				
				//sEndOfMission.bAddBookMark = FALSE	//TRUE
				//sEndOfMission.bRefresh = TRUE
			ENDIF
		BREAK
		//We are done here
		CASE LBD_BOOKMARK_DONE
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_UP_PLAYLIST_INITIAL_LB_BUTTONS(FMMC_EOM_DETAILS 	&sEndOfMission, INT iMyGBD, BOOL bSCTV, BOOL bSpectate)						
	PRINTLN("[LB] SET_UP_PLAYLIST_INITIAL_LB_BUTTONS")
	IF IS_BIT_SET(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
	AND NOT SHOULD_BYPASS_LIKE_DISLIKE_LOGIC()
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL,		"FMMC_NOVOTE")		//NO VOTE
		
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X,		"FMMC_DISLIKE")		//DISLIKE
		
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT,	"FMMC_LIKE")		//LIKE
		ELSE
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y,		"FMMC_LIKE")		//LIKE
		ENDIF
		PRINTLN("[PLC] [LB] ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) - bShowVote")
		PRINTLN("[PLC] [LB] ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X) - bShowVote")
		PRINTLN("[PLC] [LB] ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y) - bShowVote")
		sEndOfMission.iThumbStage = tvsWaitForVote
	ELSE
		IF NOT bSCTV
		AND NOT SHOULD_BYPASS_LIKE_DISLIKE_LOGIC()
			IF bSpectate
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y,		"FMMC_END_SPEC")
				SET_BIT(GlobalplayerBD_FM[iMyGBD].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_SpectateButtonOn)
			ELSE
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT,		"FMMC_END_CONT")
				CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_SpectateButtonOn)
			ENDIF
		ENDIF
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL,		"FMMC_END_QUIT")
	ENDIF

	STRING sProfile = GET_PROFILE_BUTTON_NAME()
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_SELECT, sProfile)		//PLAYER CARD

	IF sEndOfMission.bAddBookMark
	AND sEndOfMission.iAddToPlaylistProgress <	LBD_BOOKMARK_ADD
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LB,		"FMMC_ADDBK")
		PRINTLN("[PLC] [LB] ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LB) - sEndOfMission.bAddBookMark")
	ENDIF
ENDPROC

PROC SET_UP_PLAYLIST_LB_BUTTONS_AFTER_VOTE(FMMC_EOM_DETAILS &sEndOfMission, INT iTimer)		
	PRINTLN("[LB] SET_UP_PLAYLIST_LB_BUTTONS_AFTER_VOTE")
	IF iTimer > 0
		ADD_MENU_HELP_KEY("", 	"END_LBD_WAITPN", iTimer)
	ENDIF
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL,		"FMMC_END_QUIT")
	STRING sProfile = GET_PROFILE_BUTTON_NAME()
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_SELECT, sProfile)		//PLAYER CARD
	IF sEndOfMission.bAddBookMark
	AND sEndOfMission.iAddToPlaylistProgress <	LBD_BOOKMARK_ADD
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LB,		"FMMC_ADDBK")
		PRINTLN("[PLC] [LB] ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LB) - sEndOfMission.bAddBookMark")
	ENDIF
ENDPROC

PROC SET_UP_PLAYLIST_LB_BUTTONS_THUMB_VOTING_ALLOWED(FMMC_EOM_DETAILS &sEndOfMission, INT iTimer)		
	PRINTLN("[LB] SET_UP_PLAYLIST_LB_BUTTONS_THUMB_VOTING_ALLOWED")
	IF iTimer > 0
		ADD_MENU_HELP_KEY("", 	"END_LBD_WAITPN", iTimer)
	ENDIF
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, 		"FMMC_END_QUIT")
	STRING sProfile = GET_PROFILE_BUTTON_NAME()
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_SELECT, sProfile)		//PLAYER CARD
	IF sEndOfMission.bAddBookMark
	AND sEndOfMission.iAddToPlaylistProgress <	LBD_BOOKMARK_ADD
		PRINTLN("[PLC] [LB] ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LB) - sEndOfMission.bAddBookMark")
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LB,		"FMMC_ADDBK")
	ENDIF
ENDPROC

FUNC BOOL OK_TO_CHECK_FOR_QUIT(FMMC_EOM_DETAILS &sEndOfMission)

	IF IS_WARNING_MESSAGE_ACTIVE()
	
		RETURN FALSE
	ENDIF

	IF ON_BOOKMARK_ERROR_SCREEN(sEndOfMission)
	
		RETURN FALSE
	ENDIF
	
	IF ON_BOOKMARK_CONFIRM_SCREEN(sEndOfMission)
	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_DEV_SPECTATOR_PRESENT()

	INT iDevSpectateCount
	INT i
	PLAYER_INDEX playerId
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_NET_PLAYER_OK(playerId, FALSE)
				IF IS_PLAYER_DEV_SPECTATOR(playerId)
					PRINTLN("6532784, DEV_SPECTATOR = ", GET_PLAYER_NAME(playerId))
					iDevSpectateCount++
				ENDIF
			ENDIF
		ENDIF	
	ENDREPEAT
	
	IF iDevSpectateCount > 0
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL NO_PARTICIPANTS_LEFT_ON_SCRIPT()

	INT iActivePlayerCount
	INT i
	PLAYER_INDEX playerId
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_NET_PLAYER_OK(playerId, FALSE)
				IF NOT IS_PLAYER_SCTV(playerId)
				
					iActivePlayerCount++
				ENDIF
			ENDIF
		ENDIF	
	ENDREPEAT
	
	IF iActivePlayerCount < 1
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_HOLD_FOR_DEV_SPECTATOR()
	IF IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
		IF IS_PLAYLIST_IS_A_SCTV_CONTROLLED_PLAYLIST()
			IF g_b_DevSpecLbdContinue = FALSE
			AND IS_DEV_SPECTATOR_PRESENT()
				PRINTLN("[DEV_SPC] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, g_b_DevSpectateContinue = FALSE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

//Control the end of mission options when on a playlist. 
FUNC BOOL CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS(	INT 				&iRestartProgress,  
															FMMC_EOM_DETAILS 	&sEndOfMission,
															TIME_DATATYPE 		Timer				= NULL,
															INT 				iOverTimeOut		= 0,  
															STRING 				stTimer				= NULL,
															BOOL 				bSpectate			= FALSE,
															BOOL 				bskiplb				= FALSE, 
															BOOL				bNotUsedRemove		= FALSE,
															BOOL 				bRaceTimeOut		= FALSE)
															
	IF SHOULD_HOLD_FOR_DEV_SPECTATOR()
		PRINTLN("[DEV_SPC] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, SHOULD_HOLD_FOR_DEV_SPECTATOR")
		RETURN FALSE
	ENDIF				
															
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID()) 
	INT iTimer
	bNotUsedRemove = bNotUsedRemove
	
	//Id there is a count down timer then calculate is as seconds 
	IF Timer != NULL
		//Get the timer as seconds
		iTimer = (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Timer)) // /1000 
		// Count downwards 
		iTimer = (iOverTimeOut - iTimer) //1000
		IF iTimer <= 0
			iTimer = -1

			IF NOT sEndOfMission.bPlaylistTimerZero 
				sEndOfMission.bRefresh = TRUE
				sEndOfMission.bPlaylistTimerZero = TRUE
			ENDIF
		ELSE
			IF sEndOfMission.bPlaylistTimerZero 
				sEndOfMission.bPlaylistTimerZero = FALSE
			ENDIF
		ENDIF
	ELSE
		iTimer = -1
	ENDIF
	
	INT iScreenX, iScreenY
	
	sEndOfMission.bShowPlayerCard = sEndOfMission.bShowPlayerCard
	
	BOOL bSCTV = IS_PLAYER_SCTV(PLAYER_ID())
	
	SWITCH iRestartProgress
		//Do set up
		CASE ciCONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_SET_UP
			IF LOAD_MENU_ASSETS()
				CLOSE_SOCIAL_CLUB_MENU()
				SET_FRONTEND_ACTIVE(FALSE)
				REMOVE_MENU_HELP_KEYS()
				IF iTimer > 0
					ADD_MENU_HELP_KEY("", 		stTimer, iTimer)
					sEndOfMission.bTimerShown = TRUE
				ENDIF
				
				//Set book mark flag to TRUE
				sEndOfMission.bAddBookMark = TRUE
				
				//If the mission is bookmarked then we can't book mark it again
				IF g_FMMC_STRUCT.bBookMarked = TRUE
				OR NOT g_FMMC_STRUCT.bMissionIsPublished
				OR NOT IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE) 
				OR IS_PLAYER_SCTV(PLAYER_ID())
					sEndOfMission.bAddBookMark = FALSE
					sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_DONE
				ENDIF
				SET_UP_PLAYLIST_INITIAL_LB_BUTTONS(sEndOfMission, iMyGBD, bSCTV, bSpectate)				
				g_b_RestartButtons = TRUE
				g_b_PlayAgain = TRUE
				FM_PLAY_LIST_SET_AT_END_OF_MISSION()
				iRestartProgress = ciCONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_MAINTAIN
				PRINTLN("[LB] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS - ciCONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_MAINTAIN")
			ENDIF
		BREAK
		
		//Do maintain
		CASE ciCONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_MAINTAIN
		
			PRINTLN("[CS_TEST_SPEC] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS - ciCONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_MAINTAIN, iQuitProgress = ", sEndOfMission.iQuitProgress, " iThumbStage = ", sEndOfMission.iThumbStage)
			
			//If we're the only one an 
			IF (IS_PLAYER_SPECTATING(PLAYER_ID())
			OR DID_I_JOIN_MISSION_AS_SPECTATOR())
			AND NETWORK_GET_NUM_PARTICIPANTS() = 1
				 IF NOT HAS_NET_TIMER_STARTED(sEndOfMission.sTimeDelayLbdSkip)
					START_NET_TIMER(sEndOfMission.sTimeDelayLbdSkip)
					PRINTLN("[LB] IS_PLAYER_SPECTATING - Start timer")
				ELIF HAS_NET_TIMER_EXPIRED(sEndOfMission.sTimeDelayLbdSkip, ciMINIMUM_LBD_TIMEOUT)
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn)
					PRINTLN("[LB] IS_PLAYER_SPECTATING - Setting ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn")
					PRINTLN("[LB] IS_PLAYER_SPECTATING - bRaceTimeOut waited 5 seconds, RETURN TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
			
			//I have voted
			IF FM_PLAY_LIST_HAVE_I_VOTED()
			OR IS_PLAYLIST_LEADERBOARD_BEING_SETTING_UP()
				IF IS_BIT_SET(GlobalplayerBD_FM[iMyGBD].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn)
				OR (IS_THIS_A_ROUNDS_MISSION() AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD())
					IF bRaceTimeOut
						// 1743204
						IF NOT HAS_NET_TIMER_STARTED(sEndOfMission.sTimeDelayLbdSkip)
							START_NET_TIMER(sEndOfMission.sTimeDelayLbdSkip)
						ELIF HAS_NET_TIMER_EXPIRED(sEndOfMission.sTimeDelayLbdSkip, ciMINIMUM_LBD_TIMEOUT)
							PRINTLN("[LB] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS - bRaceTimeOut waited 5 seconds, RETURN TRUE")
							
							RETURN TRUE
						ENDIF
					ELSE
						PRINTLN("[LB] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS - IF IS_BIT_SET(GlobalplayerBD_FM[iMyGBD].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn)")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF		
			
			//We are spectating
			IF bSpectate
				//We are spectating
				IF IS_BIT_SET(GlobalplayerBD_FM[iMyGBD].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_SpectateButtonOn)
					IF HAS_MISSION_END_SPECTATE_BUTTON_BEEN_PRESSED()
						SET_BIT(GlobalplayerBD_FM[iMyGBD].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToSpectate)
						PRINTLN("[LB] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS - ciFMMC_MISSION_DATA_BitSet_Playlist_SpectateButtonOn ciCONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_SET_UP(bSpectate)1 ")
						iRestartProgress = ciCONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_SET_UP
					ENDIF
				ENDIF
			ELSE
				//Quit progress
				SWITCH sEndOfMission.iQuitProgress
					CASE LBD_QUIT_INITIAL
					
						MAINTAIN_ADD_BOOKMARK_BUTTON_PLAYLIST(sEndOfMission)
						
						IF IS_THIS_A_ROUNDS_MISSION()
							IF OK_TO_CHECK_FOR_QUIT(sEndOfMission)	
								IF HAS_END_JOB_QUIT_BEEN_PRESSED()
									PRINTLN("[PLC] - PLAYLIST CONTROLLER -- sEndOfMission.iQuitProgress = LBD_QUIT_CONFIRM_SCREEN ")
									sEndOfMission.iQuitProgress = LBD_QUIT_CONFIRM_SCREEN
								ENDIF
							ENDIF
						ENDIF
						
						IF (IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
						OR IS_THIS_A_ROUNDS_MISSION())
						AND NOT bskiplb
							IF sEndOfMission.bRefresh
								PRINTLN("[LB] sEndOfMission.bRefresh  ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED - Out Of Switch")
								sEndOfMission.bRefresh = FALSE
								IF sEndOfMission.iThumbStage != tvsRefreshHelp
									REMOVE_MENU_HELP_KEYS()
									IF NOT FM_PLAY_LIST_HAVE_I_VOTED()
										SET_UP_PLAYLIST_INITIAL_LB_BUTTONS(sEndOfMission, iMyGBD, bSCTV, bSpectate)
									ELSE
										SET_UP_PLAYLIST_LB_BUTTONS_AFTER_VOTE(sEndOfMission, iTimer)	
									ENDIF
								ENDIF
							ENDIF
							SWITCH sEndOfMission.iThumbStage
								CASE tvsRefreshHelp
									REMOVE_MENU_HELP_KEYS()									
									SET_UP_PLAYLIST_LB_BUTTONS_THUMB_VOTING_ALLOWED(sEndOfMission, iTimer)										
									FM_PLAY_LIST_I_WANT_TO_PLAY_NEXT()
									sEndOfMission.iThumbStage = tvsWaitForSave
								BREAK
								CASE tvsWaitForSave
									IF sEndOfMission.bRefresh
										PRINTLN("[LB] sEndOfMission.bRefresh  ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED")
										sEndOfMission.bRefresh = FALSE
										REMOVE_MENU_HELP_KEYS()									
										SET_UP_PLAYLIST_LB_BUTTONS_THUMB_VOTING_ALLOWED(sEndOfMission, iTimer)										
									ENDIF
								BREAK
								CASE tvsComplete
									CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								BREAK
							ENDSWITCH
						ELSE
							//and we've pressed to continue
							IF NOT bSCTV
								IF sEndOfMission.bRefresh
									PRINTLN("[LB] sEndOfMission.bRefresh  bSCTV")
									sEndOfMission.bRefresh = FALSE
									REMOVE_MENU_HELP_KEYS()
									IF NOT FM_PLAY_LIST_HAVE_I_VOTED()
										SET_UP_PLAYLIST_INITIAL_LB_BUTTONS(sEndOfMission, iMyGBD, bSCTV, bSpectate)
									ELSE
										SET_UP_PLAYLIST_LB_BUTTONS_AFTER_VOTE(sEndOfMission, iTimer)	
									ENDIF
								ENDIF
								IF (NOT ON_BOOKMARK_ERROR_SCREEN(sEndOfMission)
								AND NOT ON_BOOKMARK_CONFIRM_SCREEN(sEndOfMission))
								OR bskiplb
									IF HAS_MISSION_END_CONTINUE_BEEN_PRESSED()
									OR bskiplb
										REMOVE_MENU_HELP_KEYS()
										SET_UP_PLAYLIST_LB_BUTTONS_AFTER_VOTE(sEndOfMission, iTimer)	
										FM_PLAY_LIST_I_WANT_TO_PLAY_NEXT()
										PRINTLN("[LB] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS - HAS_MISSION_END_CONTINUE_BEEN_PRESSED")
									ENDIF
								ENDIF
							ENDIF
							IF NOT sEndOfMission.bTimerShown
								IF iTimer > 0
									ADD_MENU_HELP_KEY("", 		stTimer, iTimer)
									sEndOfMission.bTimerShown = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						//Make sure I'm set on the end
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
							IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyGBD].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_PlaylistAtEndOfMission)
								PRINTLN("[LB] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS - SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_PlaylistAtEndOfMission)")
								SET_BIT(GlobalplayerBD_FM[iMyGBD].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_PlaylistAtEndOfMission)
							ENDIF
							
							IF OK_TO_CHECK_FOR_QUIT(sEndOfMission)	
								IF HAS_END_JOB_QUIT_BEEN_PRESSED()
									PRINTLN("[PLC] - PLAYLIST CONTROLLER -- sEndOfMission.iQuitProgress = LBD_QUIT_CONFIRM_SCREEN ")
									sEndOfMission.iQuitProgress = LBD_QUIT_CONFIRM_SCREEN
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE LBD_QUIT_CONFIRM_SCREEN
						// Draw confirm black screen
						
						IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
						AND NOT IS_TRANSITION_SESSIONS_PLAYLIST_SAFE_TO_QUIT()
						AND NOT IS_PLAYER_SCTV(PLAYER_ID())
						AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
							SET_WARNING_MESSAGE_WITH_HEADER("FM_CSC_QUIT", "FM_CSC_QUIT3", (FE_WARNING_YES | FE_WARNING_NO))
						ELSE							
							SET_WARNING_MESSAGE_WITH_HEADER("FM_CSC_QUIT", "FM_LBD_QUIT", (FE_WARNING_YES | FE_WARNING_NO))
						ENDIF
						
						IF HAS_YES_BUTTON_BEEN_PRESSED()
							PRINTLN("[LB] sEndOfMission.iQuitProgress = LBD_QUIT_LBD ")
							PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET")
							sEndOfMission.iQuitProgress = LBD_QUIT_LBD
							
							// Set global for bad sport behaviour
							SET_TRANSITION_SESSIONS_PLAYER_QUIT_FROM_HUD()
						ENDIF
						IF HAS_NO_BUTTON_BEEN_PRESSED()
							PRINTLN("[LB] sEndOfMission.iQuitProgress = LBD_QUIT_INITIAL ")
							PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
							sEndOfMission.iQuitProgress = LBD_QUIT_INITIAL
						ENDIF
					BREAK
					CASE LBD_QUIT_LBD				
						PRINTLN("[LB] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS RETURN TRUE, I PRESSED QUIT")
						FM_PLAY_LIST_I_WANT_TO_QUIT()
						SET_PLAYER_ON_A_PLAYLIST(FALSE)
						CLEAR_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
						CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
						PRINTLN("[LB] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS - HAS_MISSION_END_QUIT_PLAYLIST_BEEN_PRESSED")
						//Clean up the strand mission data
						RESET_STRAND_MISSION_DATA()
						RESET_ROUND_MISSION_DATA()
						
						RETURN TRUE
				ENDSWITCH
		
				IF IS_BIT_SET(GlobalplayerBD_FM[iMyGBD].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_SpectateButtonOn)
					PRINTLN("[LB] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS - ciFMMC_MISSION_DATA_BitSet_Playlist_SpectateButtonOn ciCONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_SET_UP")
					iRestartProgress = ciCONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_SET_UP
				ENDIF
			ENDIF
			
			//Draw the menu help
			GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
			DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, iTimer, FALSE, TRUE, TRUE)
		BREAK
	ENDSWITCH
	//If we are skipping then get out
	IF bskiplb
		IF IS_BIT_SET(GlobalplayerBD_FM[iMyGBD].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Reset the playlist min and max values
PROC RESET_PLAYLIST_MIN_AND_MAX()
	PRINTLN("[PLC] - RESET_PLAYLIST_MIN_AND_MAX")
	g_sMenuPlayListDetails.iMinNumber = 1
	g_sMenuPlayListDetails.iMaxNumber = NUM_NETWORK_PLAYERS
ENDPROC

//If this mission compatable with a playlist that is being created!
FUNC  BOOL CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST(INT iMissionType, INT iMissionMin, INT iMissionMax, INT iRank=0, BOOL bIsPlaylist=TRUE, INT iSubtype = -1, STRING tlName = NULL)
	//If the max number on the play list is smaller than the min
	IF bIsPlaylist
		IF g_sMenuPlayListDetails.iMaxNumber < iMissionMin
			PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST false g_sMenuPlayListDetails.iMaxNumber < iMissionMin ")
			PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST  iMissionMin = ", iMissionMin )
			PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST  g_sMenuPlayListDetails.iMaxNumber = ", g_sMenuPlayListDetails.iMaxNumber )
			PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST tlName =  ", tlName)
			RETURN FALSE
		ENDIF	
		//If the min number on the play list is greater than the max
		IF g_sMenuPlayListDetails.iMinNumber > iMissionMax
			PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST false g_sMenuPlayListDetails.iMinNumber > iMissionMax ")
			PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST  iMissionMin = ", iMissionMin )
			PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST  iMissionMax = ", iMissionMax )
			PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST  g_sMenuPlayListDetails.iMinNumber = ", g_sMenuPlayListDetails.iMinNumber )
			PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST tlName =  ", tlName)
			RETURN FALSE
		ENDIF	
		
		IF GET_PLAYER_RANK(PLAYER_ID()) < iRank 
			PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST false iRank ")
			RETURN FALSE
		ENDIF
		
		IF g_sMenuPlayListDetails.iLength = FMMC_MAX_PLAY_LIST_LENGTH
			PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST false FMMC_MAX_PLAY_LIST_LENGTH ")
			RETURN FALSE
		ENDIF
		
		IF iSubtype > -1
			IF iMissionType = FMMC_TYPE_MISSION
			AND iSubtype = FMMC_MISSION_TYPE_RANDOM
			//OR iSubtype = FMMC_MISSION_TYPE_CONTACT
				PRINTLN("CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST false FMMC_MISSION_TYPE_RANDOM ")
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Disallow duplicate missions
		INT i
		IF NOT IS_STRING_NULL_OR_EMPTY(tlName)
			CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST - tlName = ", tlName)
			FOR i = 0 TO (g_sMenuPlayListDetails.iLength)
				CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST - i = ", i, ", g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlName = ", g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlName)
				IF ARE_STRINGS_EQUAL(g_sMenuPlayListDetails.sLoadedMissionDetails[i].tlName, tlName)
					CPRINTLN(DEBUG_PAUSE_MENU, "[CMcM@PAUSEMENU] CAN_THIS_MISSION_BE_ADDED_TO_THIS_PLAYLIST - No! It's already there.")
					RETURN FALSE
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//Update the new min max values, return TRUE is the values have changed. 
FUNC BOOL UPDATE_PLAYLIST_NEW_MIN_AND_MAX(INT iMissionMin, INT iMissionMax)
	PRINTLN("[PLC] - UPDATE_PLAYLIST_NEW_MIN_AND_MAX")
	BOOL bReturn
	
	IF iMissionMin > g_sMenuPlayListDetails.iMinNumber
		g_sMenuPlayListDetails.iMinNumber = iMissionMin
		bReturn = TRUE
	ENDIF
	
	
	// If the mission max is less than the playlist max cap the playlist max
	IF iMissionMax < g_sMenuPlayListDetails.iMaxNumber
	OR g_sMenuPlayListDetails.iMaxNumber = 0
		g_sMenuPlayListDetails.iMaxNumber = iMissionMax
		bReturn = TRUE
	ENDIF	
	
	PRINTLN("[PLC] - g_sCurrentPlayListDetails.iMaxNumber = ", g_sMenuPlayListDetails.iMaxNumber)
	PRINTLN("[PLC] - g_sCurrentPlayListDetails.iMinNumber = ", g_sMenuPlayListDetails.iMinNumber)
	RETURN bReturn
ENDFUNC 

//Consts for the stages
CONST_INT ciSET_UP_PLAYLIST_INT									0
CONST_INT ciSET_UP_PLAYLIST_SKY_CAM_UP							1
CONST_INT ciSET_UP_PLAYLIST_LOAD_FROM_CLOUD						2
CONST_INT ciSET_UP_PLAYLIST_SET_UP_PLAYER_DATA					3
CONST_INT  ciSET_UP_PLAYLIST_WAIT_FOR_MISSION_AT_COORDS 		4
CONST_INT  ciSET_UP_PLAYLIST_SKY_CAM_DOWN 						5
CONST_INT ciSET_UP_PLAYLIST_DONE								6

//Set the status of the transition joining
PROC SET_SET_UP_JOINING_PLAYLIST_STATUS(INT &iSwitchINT, INT iStatus)
	#IF IS_DEBUG_BUILD
	SWITCH iStatus
		CASE ciSET_UP_PLAYLIST_INT							PRINTLN("[PLC] - SET_SET_UP_JOINING_PLAYLIST_STATUS - ciSET_UP_PLAYLIST_INT")							BREAK
		CASE ciSET_UP_PLAYLIST_SKY_CAM_UP					PRINTLN("[PLC] - SET_SET_UP_JOINING_PLAYLIST_STATUS - ciSET_UP_PLAYLIST_SKY_CAM_UP")					BREAK
		CASE ciSET_UP_PLAYLIST_LOAD_FROM_CLOUD				PRINTLN("[PLC] - SET_SET_UP_JOINING_PLAYLIST_STATUS - ciSET_UP_PLAYLIST_LOAD_FROM_CLOUD")				BREAK
		CASE ciSET_UP_PLAYLIST_SET_UP_PLAYER_DATA			PRINTLN("[PLC] - SET_SET_UP_JOINING_PLAYLIST_STATUS - ciSET_UP_PLAYLIST_SET_UP_PLAYER_DATA")			BREAK
		CASE ciSET_UP_PLAYLIST_WAIT_FOR_MISSION_AT_COORDS	PRINTLN("[PLC] - SET_SET_UP_JOINING_PLAYLIST_STATUS - ciSET_UP_PLAYLIST_WAIT_FOR_MISSION_AT_COORDS")	BREAK
		CASE ciSET_UP_PLAYLIST_SKY_CAM_DOWN					PRINTLN("[PLC] - SET_SET_UP_JOINING_PLAYLIST_STATUS - ciSET_UP_PLAYLIST_SKY_CAM_DOWN")				BREAK
		CASE ciSET_UP_PLAYLIST_DONE							PRINTLN("[PLC] - SET_SET_UP_JOINING_PLAYLIST_STATUS - ciSET_UP_PLAYLIST_DONE")						BREAK
	ENDSWITCH
	#ENDIF
	iSwitchINT = iStatus
ENDPROC
//Joining vars
STRUCT SET_UP_JOINING_PLAYLIST_VARS
	INT 							iSwitchINT
	SWITCH_TYPE						sSwitchType	= SWITCH_TYPE_LONG
ENDSTRUCT

//Reset up the playlist loader
PROC RESET_SET_UP_JOINING_PLAYLIST_VARS(SET_UP_JOINING_PLAYLIST_VARS 	&sJoiningPlaylistVars, 
										GET_UGC_CONTENT_STRUCT 			&sGetUGC_content,
										SAVE_OUT_UGC_PLAYER_DATA_VARS	&sSaveOutVars)
	PRINTLN("[PLC] - RESET_SET_UP_JOINING_PLAYLIST_VARS")									
	SET_UP_JOINING_PLAYLIST_VARS 	sJoiningPlaylistVarsTemp
	SAVE_OUT_UGC_PLAYER_DATA_VARS	sSaveOutVarsTemp		
	RESET_UGC_LOAD_VARS(sGetUGC_content, TRUE, FALSE)
	sJoiningPlaylistVars	= sJoiningPlaylistVarsTemp
	sSaveOutVars			= sSaveOutVarsTemp		
ENDPROC

//// PURPOSE:Get the current playlist header details
///    iCurrentPlayListPosition - the current position in a playlist
/// RETURNS:the current playlist header details
FUNC CLOUD_LOADED_MISSION_HEADER_DETAILS GET_CURRENT_PLAYLIST_MISSION_HEADER_DETAILS(INT iCurrentPlayListPosition)
	CLOUD_LOADED_MISSION_HEADER_DETAILS sMissionHeaderVars
	
	//Set the STRUC CLOUD_LOADED_MISSION_HEADER_DETAILS to equal the playlist one	
	sMissionHeaderVars.tlName 			= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].tlName
	sMissionHeaderVars.tlOwner 			= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].tlOwner
	sMissionHeaderVars.tlMissionName 	= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].tlMissionName
	sMissionHeaderVars.tlMissionDec 	= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].tlMissionDec
	sMissionHeaderVars.vStartPos		= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].vStartPos
	sMissionHeaderVars.iType 			= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].iType
	sMissionHeaderVars.iMinPlayers 		= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].iMinPlayers
	sMissionHeaderVars.iRank 			= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].iRank
	sMissionHeaderVars.iMaxPlayers 		= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].iMaxPlayers
	sMissionHeaderVars.iRating 			= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].iRating
	sMissionHeaderVars.iSubType 		= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].iSubType
	sMissionHeaderVars.vCamPos 			= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].vCamPos
	sMissionHeaderVars.vCamHead 		= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].vCamHead
	sMissionHeaderVars.iBitSet 			= g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPlayListPosition].iBitSet

	RETURN 	sMissionHeaderVars
ENDFUNC

//Set that the playlist wants the mission at coords to set it up
PROC SET_PLAYLIST_READY_FOR_MISSION_AT_COORDS()
	PRINTLN("[PLC] - SET_PLAYLIST_READY_FOR_MISSION_AT_COORDS")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_READY_FOR_MISSION_AT_COORDS)
ENDPROC
//Clear that the playlist wants the mission at coords to set it up
PROC CLEAR_PLAYLIST_READY_FOR_MISSION_AT_COORDS()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_READY_FOR_MISSION_AT_COORDS")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_READY_FOR_MISSION_AT_COORDS)
ENDPROC
//has that the playlist said that it wants the mission at coords to set it up
FUNC  BOOL IS_PLAYLIST_READY_FOR_MISSION_AT_COORDS()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_READY_FOR_MISSION_AT_COORDS)
ENDFUNC

//Set that the next mission has been set up 
PROC SET_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP()
	PRINTLN("[PLC] - SET_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP)
ENDPROC
//Clear that the next mission has been set up 
PROC CLEAR_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP)
ENDPROC
//has the next mission has been set up 
FUNC  BOOL HAS_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP)
ENDFUNC

//Set that the next mission has been set up 
PROC SET_PLAYLIST_CHECK_FOR_CONTAT_MISSION()
	PRINTLN("[PLC] - SET_PLAYLIST_CHECK_FOR_CONTAT_MISSION")
	SET_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_CHECK_FOR_CONTAT_MISSION)
ENDPROC
//Clear that the next mission has been set up 
PROC CLEAR_PLAYLIST_CHECK_FOR_CONTAT_MISSION()
	PRINTLN("[PLC] - CLEAR_PLAYLIST_CHECK_FOR_CONTAT_MISSION")
	CLEAR_BIT(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_CHECK_FOR_CONTAT_MISSION)
ENDPROC
//has the next mission has been set up 
FUNC  BOOL SHOULD_PLAYLIST_CHECK_FOR_CONTAT_MISSION()
	RETURN IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_CHECK_FOR_CONTAT_MISSION)
ENDFUNC



//set up the vars needed when joining a playlist
FUNC BOOL SET_UP_JOINING_PLAYLIST(	SET_UP_JOINING_PLAYLIST_VARS 	&sJoiningPlaylistVars, 
									GET_UGC_CONTENT_STRUCT 			&sGetUGC_content,
									SAVE_OUT_UGC_PLAYER_DATA_VARS	&sSaveOutVars,
									TEXT_LABEL_23 					tl23PlayListToPlay, 
									VECTOR 							vStartPos, 
									UGC_TYPE						nType, 
									BOOL 							bUseFmmcSTructStar = FALSE, 
									BOOL 							bDoneInitialSpawn = TRUE, 
									BOOL 							bReSetControl = TRUE,
									BOOL 							bReSetLoadedVars = FALSE, 
									BOOL 							bHoldForCorona = FALSE)
	BOOL bVectorIsZero
	
	IF bUseFmmcSTructStar 
		bVectorIsZero = FALSE
	ELSE
		bVectorIsZero = IS_VECTOR_ZERO(vStartPos)
	ENDIF	
	
	//Have done the first mission
	IF (HAVE_DONE_FIRST_PLAYLIST_MISSION())
		PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - HAVE_DONE_FIRST_PLAYLIST_MISSION = TRUE")
		RETURN TRUE
	ENDIF
	
	//Have we loaded?
	IF HAS_PLAYLIST_HAS_BEEN_SETUP()
		PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - HAS_PLAYLIST_HAS_BEEN_SETUP = TRUE")
		RETURN TRUE
	ENDIF
	
	BOOL bCameraIsDone = FALSE
	
	//Switch this stuss
	SWITCH sJoiningPlaylistVars.iSwitchINT
	
		//Set stuff up
		CASE ciSET_UP_PLAYLIST_INT			
			IF bReSetControl
			AND NOT IS_PLAYER_SCTV(PLAYER_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
			ENDIF
			IF bReSetLoadedVars
				//Clear that the mission has loaded
				CLEAR_TRANSITION_SESSIONS_MISSION_HAS_LOADED()
				CLEAR_TRANSITION_SESSION_MISSION_LOADED_BEFORE_LAUNCH()
			ENDIF
			RESET_UGC_LOAD_VARS(sGetUGC_content, TRUE, FALSE)
			CLEAR_PLAYLIST_MISSION_FAILED_TO_LOAD()
			SET_SET_UP_JOINING_PLAYLIST_STATUS(sJoiningPlaylistVars.iSwitchINT, ciSET_UP_PLAYLIST_SKY_CAM_UP)
			SET_PLAYER_ON_A_PLAYLIST(TRUE)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				sJoiningPlaylistVars.sSwitchType = GET_SWITCH_TYPE_FOR_START_AND_END_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vStartPos) 	
				IF sJoiningPlaylistVars.sSwitchType = SWITCH_TYPE_SHORT
					sJoiningPlaylistVars.sSwitchType = SWITCH_TYPE_MEDIUM
				ENDIF
			ENDIF
		BREAK   
		
		//Cam up if needed
		CASE ciSET_UP_PLAYLIST_SKY_CAM_UP			
			IF bVectorIsZero
			OR NOT bDoneInitialSpawn
				SET_SET_UP_JOINING_PLAYLIST_STATUS(sJoiningPlaylistVars.iSwitchINT, ciSET_UP_PLAYLIST_LOAD_FROM_CLOUD)			
			ELSE
				IF SET_SKYSWOOP_UP(FALSE, TRUE, TRUE, sJoiningPlaylistVars.sSwitchType, FALSE, vStartPos.x, vStartPos.y, vStartPos.z)
					PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - START_FM_MISSION_PLAYLIST - iSkyCamStage = ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_MOVE_PED")
					SET_SET_UP_JOINING_PLAYLIST_STATUS(sJoiningPlaylistVars.iSwitchINT, ciSET_UP_PLAYLIST_LOAD_FROM_CLOUD)
				//ELSE
					//PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - START_FM_MISSION_PLAYLIST - IF SET_SKYSWOOP_UP(TRUE) = FALSE")
				ENDIF
			ENDIF
		BREAK
		
		//Load the playlist from the cloud
		CASE ciSET_UP_PLAYLIST_LOAD_FROM_CLOUD
			IF NOT HAS_PLAYLIST_MISSION_HAS_LOADED()
				IF NOT IS_STRING_NULL_OR_EMPTY(tl23PlayListToPlay)
					IF GET_FM_UGC_HAS_FINISHED()
						IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, tl23PlayListToPlay, nType, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE)		
							IF sGetUGC_content.bSuccess 
								RESET_UGC_LOAD_VARS(sGetUGC_content, TRUE, FALSE)
								SET_SET_UP_JOINING_PLAYLIST_STATUS(sJoiningPlaylistVars.iSwitchINT, ciSET_UP_PLAYLIST_SET_UP_PLAYER_DATA)
							ELSE
								SET_PLAYLIST_MISSION_FAILED_TO_LOAD()
								PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - GET_UGC_BY_CONTENT_ID - IF sGetUGC_content.bSuccess = FALSE")
								RETURN TRUE
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - GET_FM_UGC_HAS_FINISHED = FALSE")						
						#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - tl23PlayListToPlay = NULL")
					//SCRIPT_ASSERT("SET_UP_JOINING_PLAYLIST - tl23PlayListToPlay = NULL")
				#ENDIF
				ENDIF
			ELSE
				PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - HAS_PLAYLIST_MISSION_HAS_LOADED = TRUE and bVectorIsZero = FALSE")
				SET_SET_UP_JOINING_PLAYLIST_STATUS(sJoiningPlaylistVars.iSwitchINT, ciSET_UP_PLAYLIST_SET_UP_PLAYER_DATA)
			ENDIF
		BREAK
		
		//Set up the player data
		CASE ciSET_UP_PLAYLIST_SET_UP_PLAYER_DATA
			IF NOT IS_STRING_NULL_OR_EMPTY(tl23PlayListToPlay)
				//IF SAVE_OUT_UGC_PLAYER_DATA(tl23PlayListToPlay, sSaveOutVars, nType)
					//don't warp the player
					IF bVectorIsZero
						//Wait till we have a vlaid start pos
						IF g_sCurrentPlayListDetails.iCurrentPlayListPosition != -1
							PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - SAVE_OUT_UGC_PLAYER_DATA - IF bVectorIsZero = TRUE ")
							SET_PLAYLIST_READY_FOR_MISSION_AT_COORDS()
							SET_PLAYLIST_CHECK_FOR_CONTAT_MISSION()
							SET_SET_UP_JOINING_PLAYLIST_STATUS(sJoiningPlaylistVars.iSwitchINT, ciSET_UP_PLAYLIST_WAIT_FOR_MISSION_AT_COORDS)
						ENDIF
					//warp the player
					ELSE
						IF bUseFmmcSTructStar = TRUE
							vStartPos = g_sCurrentPlayListDetails.sLoadedMissionDetails[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iCurrentPlayListPosition].vStartPos
						ENDIF
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - vStartPos - ", vStartPos)
								SET_ENTITY_COORDS(PLAYER_PED_ID(), vStartPos)	
								//if I'm not going to take part in a corona
								IF IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA()
									PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA = TRUE, Setting g_vInitialSpawnLocation from ", g_vInitialSpawnLocation, " to ", vStartPos)
									g_vInitialSpawnLocation = vStartPos
									PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA = TRUE, not clearing g_vInitialSpawnLocation - ", g_vInitialSpawnLocation)
								ELSE
									g_vInitialSpawnLocation = << 0.0, 0.0, 0.0 >>
									PRINTLN("[PLC] - g_vInitialSpawnLocation = ", g_vInitialSpawnLocation)
								ENDIF
								SET_ENTITY_HEADING(PLAYER_PED_ID(), 0.0)
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
								SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
								//Set that the playlist want the mission at coords to set this up
								SET_PLAYLIST_READY_FOR_MISSION_AT_COORDS()
								//move stage
								SET_SET_UP_JOINING_PLAYLIST_STATUS(sJoiningPlaylistVars.iSwitchINT, ciSET_UP_PLAYLIST_WAIT_FOR_MISSION_AT_COORDS)
							ENDIF
						ENDIF
					ENDIF
				//ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST - ciSET_UP_PLAYLIST_SET_UP_PLAYER_DATA - tl23PlayListToPlay = NULL")
				SCRIPT_ASSERT("SET_UP_JOINING_PLAYLIST - ciSET_UP_PLAYLIST_SET_UP_PLAYER_DATA - tl23PlayListToPlay = NULL")
			#ENDIF
			ENDIF
		BREAK	
		
		//Wait for the firs mission to be set up
		CASE ciSET_UP_PLAYLIST_WAIT_FOR_MISSION_AT_COORDS
			//Has the mission at coords set this up.
			IF HAS_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP()
				//Clear the wait flags
				CLEAR_PLAYLIST_READY_FOR_MISSION_AT_COORDS()
				CLEAR_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP()
				CLEAR_PLAYLIST_CHECK_FOR_CONTAT_MISSION()
				//move stage
				IF bVectorIsZero
				OR NOT bDoneInitialSpawn
					SET_SET_UP_JOINING_PLAYLIST_STATUS(sJoiningPlaylistVars.iSwitchINT, ciSET_UP_PLAYLIST_DONE)
					SET_PLAYLIST_MISSION_HAS_LOADED()
					SET_PLAYLIST_HAS_BEEN_SETUP()
					RESET_SET_UP_JOINING_PLAYLIST_VARS(sJoiningPlaylistVars, sGetUGC_content, sSaveOutVars)
				ELSE
					SET_SET_UP_JOINING_PLAYLIST_STATUS(sJoiningPlaylistVars.iSwitchINT, ciSET_UP_PLAYLIST_SKY_CAM_DOWN)
				ENDIF
			ENDIF
		BREAK
		
		//cam back down
		CASE ciSET_UP_PLAYLIST_SKY_CAM_DOWN 
			 
			bCameraIsDone = FALSE 
			IF IS_PLAYER_SCTV(PLAYER_ID())
				IF SET_SKYSWOOP_DOWN_PROPERTY(DEFAULT, FALSE, DEFAULT, FALSE, TRUE)
					bCameraIsDone = TRUE
				ENDIF
				
				IF IS_SKYCAM_ON_LAST_CUT()
					SET_SKYFREEZE_FROZEN()
				ENDIF
				
			ELSE
				IF bHoldForCorona
					IF NOT IS_PLAYER_IN_CORONA()
						RETURN FALSE
					ENDIF
				ENDIF
				IF SET_SKYSWOOP_DOWN(NULL, FALSE, FALSE)	
					bCameraIsDone = TRUE
				ENDIF
			ENDIF
			 
			IF bCameraIsDone
				PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST = TRUE")
				SET_PLAYLIST_MISSION_HAS_LOADED()
				SET_PLAYLIST_HAS_BEEN_SETUP()
				SET_SET_UP_JOINING_PLAYLIST_STATUS(sJoiningPlaylistVars.iSwitchINT, ciSET_UP_PLAYLIST_DONE)
				RESET_SET_UP_JOINING_PLAYLIST_VARS(sJoiningPlaylistVars, sGetUGC_content, sSaveOutVars)
				//Done
				RETURN TRUE
			ENDIF
		BREAK
		
		//Done
		CASE ciSET_UP_PLAYLIST_DONE
			PRINTLN("[PLC] - SET_UP_JOINING_PLAYLIST = ciSET_UP_PLAYLIST_DONE")
			RETURN TRUE	
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: returns is a mission sub type is score or time based
///    iMissionType - the mission type to check
FUNC  BOOL IS_THIS_A_SCORE_BASED_MISSION(INT iMissionType)
	SWITCH iMissionType
		CASE FMMC_TYPE_SURVIVAL
		CASE FMMC_TYPE_BASE_JUMP
			RETURN TRUE
		BREAK
			
		CASE FMMC_TYPE_MISSION	
			IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME	
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
			
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//Get the current score to beat
FUNC INT GET_CURRENT_MISSION_TIME_SCORE_TO_BEAT()
	//Get the position
	INT iPos = g_sCurrentPlayListDetails.iCurrentPlayListPosition
	//return based on type
	IF IS_THIS_A_SCORE_BASED_MISSION(g_sCurrentPlayListDetails.sLoadedMissionDetails[iPos].iType)
		RETURN g_sCurrentPlayListDetails.sLoadedMissionDetails[iPos].iBestScore
	ELSE
		RETURN g_sCurrentPlayListDetails.sLoadedMissionDetails[iPos].iBestTime
	ENDIF
	//default
	RETURN g_sCurrentPlayListDetails.sLoadedMissionDetails[iPos].iBestTime
ENDFUNC

// SHOULD_BYPASS_LIKE_DISLIKE_LOGIC








