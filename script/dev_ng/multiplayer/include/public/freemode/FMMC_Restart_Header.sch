//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FMMC_Restart_hearer.sch																				//
// Description: Controls the Vote to see if a custom mission should restart or we should move on to a random one	//
// Written by:  Robert Wright																						//
// Date: 17/01/2013																									//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
USING "Rage_Builtins.sch"
USING "Globals.sch"
USING "FM_Playlist_Header.sch"
USING "net_common_functions.sch"
USING "leader_board_common.sch"
USING "fm_in_corona_header.sch"
USING "net_spectator_hud.sch"

USING "fm_mission_flow.sch"

//Clear the back up menu selection
PROC CLEAR_EOM_GLOBALS()
	#IF IS_DEBUG_BUILD
	PRINTLN("FMMC EOM - CLEAR_EOM_GLOBALS")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	FMMC_END_OF_MISSION_VARS g_sFMMCEOMTemp
	g_sFMMCEOM = g_sFMMCEOMTemp
ENDPROC

//Current Player counts struct.
STRUCT PLAYER_CURRENT_STATUS_COUNTS
	INT iNumberOnMission
	INT iNumberContinue
	INT iNumberRestart
	INT iNumberRandom
	INT iNumberAtEndOfMission
	INT iNumberVoted
	INT iNumberQuickRestart
	INT iNumberQuickRestartSwap
	INT iNumberJobVotes
	INT iPlayerRank
ENDSTRUCT

//Server broadcast data needed for the EOM
STRUCT SERVER_EOM_VARS
	INT 			iVoteStatus 	= ciFMMC_EOM_VOTE_STATUS_INVALID
	BOOL 			bVotePassed
	BOOL 			bJobVotesYes
	INT				iMaxPlayerRank
	BOOL 			bAllowedToVote 	= TRUE	
	VECTOR 			vWarpPos
	TEXT_LABEL_23	contentID	// KGM 2/7/13: Stores cloud filename when Random Next Job chosen so that I don't have to look it up based on the warpPos
	#IF IS_DEBUG_BUILD
	PLAYER_CURRENT_STATUS_COUNTS sDebugCounts
	#ENDIF
ENDSTRUCT

FUNC BOOL SHOULD_BLOCK_AMMO_ARMOUR()

	IF IS_PLAYER_SPECTATING(PLAYER_ID())
		
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
	
		RETURN TRUE
	ENDIF
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
		IF iTeam < 0
		OR iTeam >= FMMC_MAX_TEAMS
			iTeam = 0
		ENDIF
		
		IF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(iTeam)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


//Set up the help for the end of mission stuff.
PROC SET_UP_MISSION_END_HELP(	FMMC_EOM_DETAILS &sEndOfMission, 
								LBD_VARS& lbdVars, 
								BOOL 	bRestart, 						//	FALSE, 
								BOOL 	bRandom, 						//	FALSE, 
								BOOL 	bContinue, 						//	FALSE, 
								BOOL 	bQuit, 							//	FALSE, 
								BOOL 	bPlayerCard 		= TRUE, 	//	TRUE, 
								BOOL 	bSpectate 			= TRUE, 	//	FALSE, 
								BOOL 	bShowMove 			= TRUE,		//	TRUE,
								BOOL 	bShowFacebook 		= TRUE, 	//	FALSE,
								BOOL 	bSCLBAvailable		= TRUE,		//	TRUE,
								BOOL 	bShowSCLB 			= FALSE,	//	sEndOfMission.bActiveSCLeaderboard,
								INT 	iTimer				= -1, 		//	iTimer, 
								STRING 	stTimer				= NULL,		//	stEnd,
								BOOL 	bQuickRestart		= TRUE, 	//	FALSE,
								BOOL 	bQuickRestartSwap	= TRUE, 	//	FALSE,
								BOOL 	bAddBookMark		= FALSE,	//	FALSE,
								BOOL 	bShowCoDriver		= FALSE,	//	sEndOfMission.bShowCoDriver,
								BOOL 	bRallyRace			= FALSE,    //	sEndOfMission.bRallyRace)
								BOOL 	bKickedFromSurvival = FALSE,
								BOOL 	bShowVote			= FALSE,
								BOOL    bQuitEarly			= FALSE)
								
								
	IF SHOULD_BYPASS_LIKE_DISLIKE_LOGIC()
	
		bShowVote = FALSE
	ENDIF
			
	PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP")
	REMOVE_MENU_HELP_KEYS()
	//CLEAR_MENU_DATA()
	
	IF iTimer != -1
		ADD_MENU_HELP_KEY("", 		stTimer, iTimer)		//Spinner
		PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - ADD_MENU_HELP_KEY(ICON_INVALID, 		stTimer, iTimer)")
	ENDIF
	IF bShowVote
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL,		"FMMC_NOVOTE")		//NO VOTE
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X,		"FMMC_DISLIKE")		//DISLIKE
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			PRINTLN("7674940 SET_UP_MISSION_END_HELP, FRONTEND_CONTROL")
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT,		"FMMC_LIKE")		//LIKE		WORLD LEADERBOARD uses INPUT_FRONTEND_LEADERBOARD which is tab, so for keyboard controls use Enter to "Like" the job
		ELSE
			PRINTLN("7674940 SET_UP_MISSION_END_HELP, INPUT_FRONTEND_Y")
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y,		"FMMC_LIKE")		//LIKE
		ENDIF
		PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) - bShowVote")
		PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X) - bShowVote")
		PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y) - bShowVote")
	ENDIF
	IF bContinue
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID()) 
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
				ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT,		"FM_NXT_GTA")	//Freemode 
				PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) - bContinue")
			ENDIF
		ELSE
			ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT,		"FMMC_END_CONT")	//CONTINUE
			PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) - bContinue")
		ENDIF
	ENDIF
	IF bRandom
		//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL,		"FMMC_END_RAND")	//RANDOM
	ENDIF
	IF bSpectate
		//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_Y,		"HUD_INPUT28")		//SPECTATE
	ENDIF
	IF bRestart
		//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_X,		"FMMC_END_RST")		//RESTART
	ENDIF	
	//as per bug 1485237 (and others relating to this scroll option is being removed so taken this out.
	IF bShowMove
//		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD,		"HUD_INPUT1")		//MOVE
	ENDIF
	IF bQuit
		IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
			IF NOT g_bPassedMyTutorialMission
			AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
				ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT,		"FMMC_END_RST")	//RESTART
			ELSE
				IF NOT FM_PLAY_LIST_HAVE_I_VOTED()
					ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT,		"FMMC_END_CONT")	//CONTINUE
				ENDIF
			ENDIF
		ELIF bKickedFromSurvival
			ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT,		"FMMC_END_CONT")	//CONTINUE
		ELSE
			IF bQuitEarly
				ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_CANCEL,		"HUD_QUIT")		//QUIT	
			ENDIF
		ENDIF
	ENDIF
	
	IF bPlayerCard
		STRING sProfile = GET_PROFILE_BUTTON_NAME()
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_SELECT, sProfile)		//PLAYER CARD
	ENDIF
	IF bShowFacebook
		ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_LS,	"FMMC_ALLPL")	//POST RESULTS TO FACEBOOK
	ENDIF
	IF bSCLBAvailable
		IF NOT bShowSCLB
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LEADERBOARD,	"FMMC_END_SCLB1")	//WORLD LEADERBOARD
		ELSE
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, 	"HUD_INPUT34")
//			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_LEADERBOARD,	"FMMC_END_SCLB2")	//GAME LEADERBOARD
			IF bRallyRace
				IF bShowCoDriver	
//					ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_Y,	"FMMC_COR_SCLB4")	//Co_driver
//				ELSE
//					ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_Y,	"FMMC_COR_SCLB3")	//DRIVER
				ENDIF
			ENDIF
//			IF NOT NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
//				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT,"IB_SOCIAL_CLUB")
//			ELSE
//				IF NOT NETWORK_ARE_SOCIAL_CLUB_POLICIES_CURRENT()
//					ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT,"SCLB_SIGNIN")
//				ENDIF
//			ENDIF
			
		ENDIF
	ENDIF	
	IF bAddBookMark
	AND g_FMMC_STRUCT.bBookMarked = FALSE	
	AND g_FMMC_STRUCT.bMissionIsPublished = TRUE
	AND IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE) 
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LB,		"FMMC_ADDBK")	//Bookmark Job
	ENDIF
	IF SHOULD_SHOW_QUICK_RESTART_OPTION()
		IF bQuickRestart
			ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_RT,		"HUD_QREST")		//QUICK RESTART	
			PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RT) - HUD_QREST")
		ENDIF
		IF bQuickRestartSwap
			ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_LT,		"HUD_QRESTST")		//QUICK RESTART SWAP TEAMS	
			PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - ADD_MENU_HELP_KEY GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LT) - HUD_QRESTST")
		ENDIF
	ENDIF
	
	// 1757460 add buttons for armour/ammo purchase
	BOOL bShowAmmo
	BOOL bShowArmour
	
	IF NOT SHOULD_BLOCK_AMMO_ARMOUR()
	AND NOT sEndOfMission.bActiveSCLeaderboard
	
		COUNT_AMMO_COST(lbdVars.sAmmoArmour)
		INITIAL_ARMOUR_CHECK(lbdVars.sAmmoArmour)
	
		bShowAmmo = AMMO_AVAILABLE(lbdVars.sAmmoArmour)
		bShowArmour = ARMOUR_AVAILABLE(lbdVars.sAmmoArmour)
		STRING sAmmo
			
		IF bShowArmour
			sAmmo = GET_MP_ARMOR_NAME(GET_MP_MAX_UNLOCKED_ARMOR_LEVEL())
			IF NOT IS_STRING_NULL_OR_EMPTY(sAmmo)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RS, sAmmo)			// Armour
				PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - ADD_MENU_HELP_KEY ICON_RSTICK_ALL - FMMC_END_ARMR")
			ELSE
				PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - IS_STRING_NULL_OR_EMPTY(sAmmo), GET_MP_MAX_UNLOCKED_ARMOR_LEVEL  = ", GET_MP_MAX_UNLOCKED_ARMOR_LEVEL())
			ENDIF
		ENDIF
		
		IF bShowAmmo
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LS, "FMMC_END_AMMO")		// Ammo
			PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - ADD_MENU_HELP_KEY ICON_LSTICK_ALL1 - FMMC_END_AMMO")
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[ammo/armour] fmmc_restart_header.sch SHOULD_BLOCK_AMMO_ARMOUR ")
	#ENDIF	
	ENDIF
	
	IF GET_IS_WIDESCREEN()
		SET_MENU_HELP_CLEAR_SPACE(25)
	ELSE
		SET_MENU_HELP_CLEAR_SPACE(40)
	ENDIF
	//SET_MENU_HELP_KEYS_STACKED(TRUE)
	
ENDPROC
//Set the status
PROC SET_END_OF_MISSION_STATUS(FMMC_EOM_DETAILS &sEndOfMission, INT iNewStatus)
	#IF IS_DEBUG_BUILD	
	IF iNewStatus = ciEND_OF_MISSION_INI
		PRINTLN("FMMC EOM - SET_END_OF_MISSION_STATUS - ciEND_OF_MISSION_INI")
	ELIF iNewStatus = ciEND_OF_MISSION_SET_UP
		PRINTLN("FMMC EOM - SET_END_OF_MISSION_STATUS - ciEND_OF_MISSION_SET_UP")
	ELIF iNewStatus = ciEND_OF_MISSION_DONE
		PRINTLN("FMMC EOM - SET_END_OF_MISSION_STATUS - ciEND_OF_MISSION_DONE")
	ELIF iNewStatus = ciEND_OF_MISSION_DO_VOTE
		PRINTLN("FMMC EOM - SET_END_OF_MISSION_STATUS - ciEND_OF_MISSION_DO_VOTE")
	ELIF iNewStatus = ciEND_OF_MISSION_RATE_JOB
		PRINTLN("FMMC EOM - SET_END_OF_MISSION_STATUS - ciEND_OF_MISSION_RATE_JOB")
	ELIF iNewStatus = ciEND_OF_MISSION_RACE
		PRINTLN("FMMC EOM - SET_END_OF_MISSION_STATUS - ciEND_OF_MISSION_RACE")
	ENDIF
	#ENDIF
	sEndOfMission.iProgress = iNewStatus
ENDPROC

//FUNC BOOL MOVE_TO_PLAYER_CARD_WHEN_BUTTON_PRESSED(LEADERBOARD_PLACEMENT_TOOLS &placement )

/*
PROC RENDER_PLAYER_CARD(PLAYER_INDEX PlayerCardPlayer,LEADERBOARD_PLACEMENT_TOOLS &placement, BIG_RACE_STRUCT &bigRaceVarsPassed )
 
	INT iScreenX, iScreenY
	//player card variables
	SCALEFORM_INDEX siPlayerCardMovie
	SCALEFORM_PLAYER_CARD ScaleformPlayerCardStruct

	IF IS_NET_PLAYER_OK(PlayerCardPlayer,FALSE)
		FMMC_DISPLAY_PLAYER_STATS_CARD(ScaleformPlayerCardStruct, siPlayerCardMovie, PlayerCardPlayer)
	ELSE
		g_b_AddPlayerCardExit = FALSE
		bigRaceVarsPassed.bShowPlayerCard =FALSE
		REMOVE_MENU_HELP_KEYS()
	ENDIF
	IF g_b_AddPlayerCardExit = FALSE
		IF LOAD_MENU_ASSETS()
			REMOVE_MENU_HELP_KEYS()
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL,"FMMC_DM_MENU_Y")                                
			g_b_AddPlayerCardExit = TRUE
		ENDIF
	ELSE
		IF NOT IS_SPECTATOR_HUD_HIDDEN()
			//Draw the menu help
			GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
			DRAW_MENU_HELP_SCALEFORM(iScreenX)
		ENDIF
	ENDIF
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		g_b_AddPlayerCardExit = FALSE
		bigRaceVarsPassed.bShowPlayerCard =FALSE
		REMOVE_MENU_HELP_KEYS()
		REFRESH_SCALEFORM_BUTTON_SLOTS(placement)
	ENDIF
					
ENDPROC*/


//If the player is not on the quit confirm screen
FUNC BOOL IS_PLAYER_ON_QUIT_CONFIRM_SCREEN(FMMC_EOM_DETAILS &sEndOfMission)
	RETURN (sEndOfMission.iQuitProgress = LBD_QUIT_CONFIRM_SCREEN)
ENDFUNC

//If the player is on on the add bookmark confirm screen
FUNC BOOL IS_PLAYER_ON_BOOKMARK_SCREEN(FMMC_EOM_DETAILS &sEndOfMission)	
	RETURN ((sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_CONFIRM_SCREEN) OR (sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_ADD) OR (sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_CONFIRM) OR (sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_ERROR))
ENDFUNC

//returns true if we are allowed random
FUNC BOOL SHOULD_EOM_RANDOM_BE_DISPLAYED(FMMC_EOM_DETAILS 	&sEndOfMission, SERVER_EOM_VARS 	&sServerFMMC_EOM, BOOL bIsJobVote)
	RETURN FALSE
	IF sServerFMMC_EOM.bAllowedToVote 
	AND sEndOfMission.bEnoughCash 
	AND NOT sEndOfMission.bIsHeist 
	AND NOT sEndOfMission.bIsContactMission
	AND NOT sEndOfMission.bMiniGame
	AND NOT FM_PLAY_LIST_HAVE_I_VOTED()
	AND NOT bIsJobVote
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


//returns true if we are allowed random
FUNC BOOL SHOULD_EOM_CONTINUE_BE_DISPLAYED(SERVER_EOM_VARS 	&sServerFMMC_EOM, BOOL bAllowContinue)
	IF sServerFMMC_EOM.bAllowedToVote	
	AND bAllowContinue
	AND NOT FM_PLAY_LIST_HAVE_I_VOTED()

		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
//Should we show the stqandard restart option
FUNC BOOL SHOULD_EOM_RESTART_BE_DISPLAYED(FMMC_EOM_DETAILS 	&sEndOfMission, SERVER_EOM_VARS 	&sServerFMMC_EOM, BOOL bIsJobVote)

	IF bIsJobVote
		RETURN FALSE
	ENDIF
	
	IF sEndOfMission.bShowQuickRestart
		RETURN FALSE
	ENDIF
	
	//We don't show restart if this is a contact mission and they've not failed it.
	IF sEndOfMission.bIsContactMission
	AND NOT SHOULD_SHOW_QUICK_RESTART_OPTION()
		RETURN FALSE
	ENDIF
	
	//not a failed contact then show these options.
	IF sServerFMMC_EOM.bAllowedToVote 
	AND sEndOfMission.bEnoughCash
	AND NOT sEndOfMission.bIsHeist
	AND NOT FM_PLAY_LIST_HAVE_I_VOTED()
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO(g_FMMC_STRUCT.iRootContentIDHash)

		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_HIDE_SHOW_QUICK_RESTART_OPTION_BECAUSE_OF_SPECTATOR()
	INT				iPartLoop
	PLAYER_INDEX	playerID
	
	// 2013525
	IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
	OR IS_LOADED_MISSION_TYPE_FLOW_MISSION()	
		RETURN FALSE
	ENDIF
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPartLoop
		//If they are active
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
			//Get player ID
			playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))	
			IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_REFRESH_BUTTONS(FMMC_EOM_DETAILS& sEndOfMission, SERVER_EOM_VARS &sServerFMMC_EOM)

	IF IS_PC_VERSION()
		IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF sEndOfMission.bAllowedToVoteLocal = sServerFMMC_EOM.bAllowedToVote
		
		RETURN FALSE
	ENDIF
	
	IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
	
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL DO_THUMB_VOTE(FMMC_EOM_DETAILS& sEndOfMission)
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
	AND sEndOfMission.iThumbStage = tvsWaitForVote
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

//Reset up the help if the server says so
PROC RESET_UP_HELP_IF_VOTE_STATUS_CHANGE(LBD_VARS& lbdVars, FMMC_EOM_DETAILS& sEndOfMission, SERVER_EOM_VARS &sServerFMMC_EOM, BOOL bIsJobVote, BOOL bAllowContinue, BOOL bPlayerCard, BOOL bSpectate, BOOL bShowMove, BOOL bfacebook, BOOL bSCLB, STRING stEnd, INT iTimer, BOOL bQuitEarly)
	
	IF SHOULD_REFRESH_BUTTONS(sEndOfMission, sServerFMMC_EOM)
	
		PRINTLN("FMMC EOM - sEndOfMission.bAllowedToVoteLocal != sServerFMMC_EOM.bAllowedToVote, RESET_UP_HELP_IF_VOTE_STATUS_CHANGE ")
		sEndOfMission.bAllowedToVoteLocal = sServerFMMC_EOM.bAllowedToVote
		PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - sEndOfMission.bAllowedToVoteLocal != sServerFMMC_EOM.bAllowedToVote")						
		PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - iTimer = ", iTimer)						
		PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - stEnd  = ", stEnd)		
		BOOL bAddContinue
		IF DO_THUMB_VOTE(sEndOfMission)
			bAddContinue = FALSE
		ELSE
			bAddContinue = SHOULD_EOM_CONTINUE_BE_DISPLAYED(sServerFMMC_EOM, bAllowContinue)
		ENDIF
		SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars,
								SHOULD_EOM_RESTART_BE_DISPLAYED(sEndOfMission, sServerFMMC_EOM, bIsJobVote), 
								FALSE, 
								bAddContinue, 
								TRUE, 
								bPlayerCard, 
								bSpectate, 
								bShowMove, 
								bfacebook, 
								bSCLB, 
								sEndOfMission.bActiveSCLeaderboard,
								iTimer,
								stEnd,
								sServerFMMC_EOM.bAllowedToVote AND sEndOfMission.bShowQuickRestart, 
								sServerFMMC_EOM.bAllowedToVote AND sEndOfMission.bShowQuickRestartSwap,
								sEndOfMission.bAddBookMark,
								DEFAULT,
								DEFAULT,
								DEFAULT,
								DO_THUMB_VOTE(sEndOfMission),
								bQuitEarly)
	ENDIF
	
ENDPROC

//Check for quick restart
PROC MAINTAIN_MISSION_QUICK_RESTART(LBD_VARS& lbdVars,FMMC_EOM_DETAILS &sEndOfMission, BOOL bMiniGame, BOOL bPlayerCard, BOOL bSpectate, BOOL bShowMove, BOOL bfacebook, BOOL bSCLB, STRING stEnd, INT iTimer, BOOL bAllowedToVote)
	IF HAS_MISSION_QUICK_RESTART_BEEN_PRESSED()
	AND NOT bMiniGame
		IF sEndOfMission.bShowQuickRestart
		AND NOT FM_PLAY_LIST_HAVE_I_VOTED()
		AND bAllowedToVote
		
		#IF IS_DEBUG_BUILD
		OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceQuicRestart")
		#ENDIF
		
			IF (NETWORK_GET_NUM_PARTICIPANTS() >= g_FMMC_STRUCT.iMinNumParticipants)
			
				#IF IS_DEBUG_BUILD
				OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceQuicRestart")
				#ENDIF
			
				CLEAR_REPLAY_JOB_HELP()
				FM_PLAY_LIST_SET_ME_AS_VOTED()
				SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART) 
				PRINTLN("FMMC EOM [QUICK_RESTART] - SET_UP_MISSION_END_HELP - HAS_MISSION_QUICK_RESTART_BEEN_PRESSED")						
				PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
				SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark)
			ELSE
				PRINTLN("FMMC EOM - [QUICK_RESTART] sEndOfMission.bShowQuickRestart = FALSE - HAS_MISSION_QUICK_RESTART_BEEN_PRESSED, NETWORK_GET_NUM_PARTICIPANTS = ", NETWORK_GET_NUM_PARTICIPANTS(), " iNumParticipants = ", g_FMMC_STRUCT.iNumParticipants)		
			ENDIF
		ELSE
			PRINTLN("FMMC EOM - [QUICK_RESTART] sEndOfMission.bShowQuickRestart = FALSE - HAS_MISSION_QUICK_RESTART_BEEN_PRESSED")						
		ENDIF
	ENDIF 
ENDPROC

//Check for quick restart with a wee team swap
PROC MAINTAIN_MISSION_QUICK_RESTART_SWAP_TEAMS(LBD_VARS& lbdVars, FMMC_EOM_DETAILS &sEndOfMission, BOOL bMiniGame, BOOL bPlayerCard, BOOL bSpectate, BOOL bShowMove, BOOL bfacebook, BOOL bSCLB, STRING stEnd, INT iTimer, BOOL bAllowedToVote)
	IF HAS_MISSION_QUICK_RESTART_SWAP_TEAMS_BEEN_PRESSED()
	AND NOT bMiniGame
		IF sEndOfMission.bShowQuickRestartSwap
		AND NOT FM_PLAY_LIST_HAVE_I_VOTED()
		AND bAllowedToVote
			IF (NETWORK_GET_NUM_PARTICIPANTS() >= g_FMMC_STRUCT.iMinNumParticipants)
				CLEAR_REPLAY_JOB_HELP()
				FM_PLAY_LIST_SET_ME_AS_VOTED()
				SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP) 
				PRINTLN("FMMC EOM - [QUICK_RESTART] SET_UP_MISSION_END_HELP - HAS_MISSION_QUICK_RESTART_SWAP_TEAMS_BEEN_PRESSED")						
				PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
				SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark)
			ELSE
				PRINTLN("FMMC EOM - [QUICK_RESTART] sEndOfMission.bShowQuickRestart = FALSE - MAINTAIN_MISSION_QUICK_RESTART_SWAP_TEAMS, NETWORK_GET_NUM_PARTICIPANTS = ", NETWORK_GET_NUM_PARTICIPANTS(), " iNumParticipants = ", g_FMMC_STRUCT.iNumParticipants)		
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("FMMC EOM - [QUICK_RESTART] sEndOfMission.bShowQuickRestartSwap = FALSE - HAS_MISSION_QUICK_RESTART_SWAP_TEAMS_BEEN_PRESSED")						
		#ENDIF
		ENDIF	
	ENDIF
ENDPROC

// check for showing SC LB
PROC MAINTAIN_SELECTING_WORLD_LEADERBOARD(FMMC_EOM_DETAILS &sEndOfMission, BOOL bSCLB, BOOL &bResetButtons)
	IF bSCLB
	AND NOT IS_PLAYER_ON_BOOKMARK_SCREEN(sEndOfMission)
	AND NOT IS_PLAYER_ON_QUIT_CONFIRM_SCREEN(sEndOfMission)
		IF HAS_MISSION_END_SHOW_SC_LB_BEEN_PRESSED()
			sEndOfMission.iRaceStage = 0
			sEndOfMission.iSavedProgress = sEndOfMission.iProgress
			sEndOfMission.iProgress = ciEND_OF_MISSION_VIEW_SCLB
			PRINTLN("FMMC EOM - SET_END_OF_MISSION_STATUS - sEndOfMission.iProgress = ciEND_OF_MISSION_VIEW_SCLB")	
			PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
			CLEAR_REPLAY_JOB_HELP()
			bResetButtons = TRUE
			sEndOfMission.bRefresh = TRUE
			sEndOfMission.bActiveSCLeaderboard = !sEndOfMission.bActiveSCLeaderboard
		ENDIF
		
		IF sEndOfMission.bActiveSCLeaderboard
			IF HAS_MISSION_END_SHOW_SC_LB_CO_DRIVER_BEEN_PRESSED()
				bResetButtons = TRUE
				sEndOfMission.bRefresh = TRUE
				sEndOfMission.bShowCoDriver = !sEndOfMission.bShowCoDriver
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Maintain spectate
PROC MAINTAIN_SETUP_FOR_SPECTATE(LBD_VARS& lbdVars,FMMC_EOM_DETAILS &sEndOfMission, SERVER_EOM_VARS &sServerFMMC_EOM, BOOL bIsJobVote, BOOL bAllowContinue, BOOL bResetButtons, BOOL bPlayerCard, BOOL bSpectate, BOOL bShowMove, BOOL bfacebook, BOOL bSCLB, STRING stEnd, INT iTimer)
	IF NOT IS_PLAYER_ON_QUIT_CONFIRM_SCREEN(sEndOfMission)  // Not on the confirm screen
	AND NOT IS_PLAYER_ON_BOOKMARK_SCREEN(sEndOfMission)
		IF sEndOfMission.bSetupWithSpectate <> bSpectate	//update if highlighted spectator was not the same as last highlighted spectator
			//If you are only able to quit
			IF sEndOfMission.bOnlyDoQuit
				PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - sEndOfMission.bSetupWithSpectate <> bSpectate - sEndOfMission.bOnlyDoQuit")						
				SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, FALSE, bfacebook, bSCLB, FALSE, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark)
			//If the serversays we can't vote
			ELIF sServerFMMC_EOM.bAllowedToVote = FALSE
				PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - sEndOfMission.bSetupWithSpectate <> bSpectate - sServerFMMC_EOM.bAllowedToVote = FALSE")						
				SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, SHOULD_EOM_RESTART_BE_DISPLAYED(sEndOfMission, sServerFMMC_EOM, bIsJobVote), 
										FALSE, 
										SHOULD_EOM_CONTINUE_BE_DISPLAYED(sServerFMMC_EOM, bAllowContinue), 
										TRUE, bPlayerCard, bSpectate, 
										bShowMove, bfacebook, bSCLB, FALSE, iTimer, stEnd, sServerFMMC_EOM.bAllowedToVote AND sEndOfMission.bShowQuickRestart, sServerFMMC_EOM.bAllowedToVote AND sEndOfMission.bShowQuickRestartSwap, sEndOfMission.bAddBookMark)
			ELSE
				PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - sEndOfMission.bSetupWithSpectate <> bSpectate")						
				SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, SHOULD_EOM_RESTART_BE_DISPLAYED(sEndOfMission, sServerFMMC_EOM, bIsJobVote), 
										FALSE, 
										SHOULD_EOM_CONTINUE_BE_DISPLAYED(sServerFMMC_EOM, bAllowContinue), TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, FALSE, iTimer, stEnd, sEndOfMission.bShowQuickRestart, sEndOfMission.bShowQuickRestartSwap, sEndOfMission.bAddBookMark)
			ENDIF
			sEndOfMission.bSetupWithSpectate = bSpectate
		ENDIF
		
		IF sEndOfMission.bSetupWithSpectate
		AND HAS_MISSION_END_SPECTATE_BUTTON_BEEN_PRESSED()
			PRINTLN("FMMC EOM - SET_END_OF_MISSION_STATUS - HAS_MISSION_END_SPECTATE_BUTTON_BEEN_PRESSED")						
			SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_SET_UP)
			sEndOfMission.bSpectateButtonPressed = TRUE
		ELSE
			sEndOfMission.bSpectateButtonPressed = FALSE
		ENDIF
		
		
		IF bResetButtons
		OR sEndOfMission.bRefresh
		OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
			IF sEndOfMission.bRefresh
				sEndOfMission.bRefresh = FALSE
			ENDIF
			IF sEndOfMission.iProgress = ciEND_OF_MISSION_VIEW_SCLB
				PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bResetButtons - only SCLB buttons")	
				SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, TRUE,FALSE,TRUE,sEndOfMission.bActiveSCLeaderboard,iTimer, stEnd,FALSE,FALSE,sEndOfMission.bAddBookMark,sEndOfMission.bShowCoDriver,sEndOfMission.bRallyRace)
			ELSE
				IF sEndOfMission.bOnlyDoQuit
					IF NOT sEndOfMission.bActiveSCLeaderboard
					OR NOT bSCLB
						PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bResetButtons - sEndOfMission.bOnlyDoQuit - IF NOT sEndOfMission.bActiveSCLeaderboard")						
						SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, FALSE, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark,sEndOfMission.bShowCoDriver,sEndOfMission.bRallyRace)
					ENDIF
				ELSE
					PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bResetButtons - sEndOfMission.bOnlyDoQuit = FALSE")						
					SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, SHOULD_EOM_RESTART_BE_DISPLAYED(sEndOfMission, sServerFMMC_EOM, bIsJobVote), 
											FALSE, 
											SHOULD_EOM_CONTINUE_BE_DISPLAYED(sServerFMMC_EOM, bAllowContinue), 
											TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer,stEnd, !FM_PLAY_LIST_HAVE_I_VOTED() AND sEndOfMission.bShowQuickRestart, !FM_PLAY_LIST_HAVE_I_VOTED() AND sEndOfMission.bShowQuickRestartSwap, sEndOfMission.bAddBookMark)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Maintain the add book mark button
PROC MAINTAIN_ADD_BOOKMARK_BUTTON(FMMC_EOM_DETAILS& sEndOfMission, BOOL &bResetButtons)

	//If the mission is bookmarked then we can't book mark it again
	IF g_FMMC_STRUCT.bBookMarked = TRUE
		EXIT
	ENDIF
	//If the mission is not published then get out
	IF g_FMMC_STRUCT.bMissionIsPublished = FALSE
		EXIT
	ENDIF
	//if the player doesn't have a social club account
	IF NOT IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE) 
		EXIT
	ENDIF
	IF IS_PC_VERSION()
	AND NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_ON_QUIT_CONFIRM_SCREEN(sEndOfMission)  // Not on the confirm screen
		IF sEndOfMission.bAddBookMark = TRUE
		AND sEndOfMission.bOnlyDoQuit = FALSE
			SWITCH sEndOfMission.iAddToPlaylistProgress
				CASE LBD_BOOKMARK_INITIAL
					//And if the button has just been pressed
					IF HAS_MISSION_END_ADD_TO_PLAYLIST_BEEN_PRESSED()
					
						sEndOfMission.iRaceStage = 0
						PRINTLN("FMMC EOM - sEndOfMission.iAddToPlaylistProgress = HAS_MISSION_END_ADD_TO_PLAYLIST_BEEN_PRESSED ")						
						sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_CONFIRM_SCREEN	
						PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
						PRINTLN("FMMC EOM - sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_CONFIRM_SCREEN")
					ENDIF									   
				BREAK
				CASE LBD_BOOKMARK_CONFIRM_SCREEN
					// Draw confirm black screen
					SET_WARNING_MESSAGE_WITH_HEADER("FMMC_CLDEAD", "FMMC_ADDBKQ", (FE_WARNING_YES | FE_WARNING_NO))
					IF HAS_YES_BUTTON_BEEN_PRESSED()
						PRINTLN("FMMC EOM - MAINTAIN_ADD_BOOKMARK_BUTTON")						
						//ADD_CURRENT_MISSION_TO_PLAYLIST_FRONT_END()
						PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FREEMODE_SOUNDSET")
						bResetButtons = TRUE
						sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_ADD	
						PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
						PRINTLN("FMMC EOM - sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_ADD ")
					ENDIF
					IF HAS_NO_BUTTON_BEEN_PRESSED()
						PRINTLN("FMMC EOM - sEndOfMission.iAddToPlaylistProgress = LBD_QUIT_INITIAL ")
						PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET")
						sEndOfMission.iAddToPlaylistProgress = LBD_BOOKMARK_INITIAL
					ENDIF
				BREAK
				CASE LBD_BOOKMARK_ADD
					IF UGC_SET_BOOKMARKED(g_FMMC_STRUCT.tl31LoadedContentID, TRUE, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_MISSION))
						sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_CONFIRM
						PRINTLN("FMMC EOM - sEndOfMission.iAddToPlaylistProgress = UGC_SET_BOOKMARKED = TRUE - LBD_BOOKMARK_CONFIRM")
					ELSE
						PRINTLN("FMMC EOM - sEndOfMission.iAddToPlaylistProgress - UGC_SET_BOOKMARKED = FALSE")						
					ENDIF
				BREAK
				//Wait for confirm
				CASE LBD_BOOKMARK_CONFIRM
					IF UGC_HAS_MODIFY_FINISHED()
						IF UGC_DID_MODIFY_SUCCEED()		
						#IF IS_DEBUG_BUILD
						AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_FailAddBookMark")
						#ENDIF
							sEndOfMission.bAddBookMark = FALSE	//TRUE
							sEndOfMission.bRefresh = TRUE
							bResetButtons = TRUE
							sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_DONE
							PRINTLN("FMMC EOM - sEndOfMission.iAddToPlaylistProgress = UGC_SET_BOOKMARKED = TRUE - LBD_BOOKMARK_DONE")			
						ELSE
							sEndOfMission.iCloudFailStatus = UGC_GET_MODIFY_RESULT()							
							PRINTLN("FMMC EOM - sEndOfMission.iAddToPlaylistProgress = sEndOfMission.iCloudFailStatus = ", GET_UGC_QUERY_RESULT_AS_A_STRING(sEndOfMission.iCloudFailStatus))							
							sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_ERROR
							PRINTLN("FMMC EOM - sEndOfMission.iAddToPlaylistProgress = UGC_SET_BOOKMARKED = TRUE - LBD_BOOKMARK_ERROR")							
						ENDIF
					ENDIF
				BREAK
				//deal with the error if we failed
				CASE LBD_BOOKMARK_ERROR
					IF sEndOfMission.iCloudFailStatus = UGC_ERROR_NOTALLOWED_MAXCREATED
						SET_WARNING_MESSAGE_WITH_HEADER("FMMC_CLDEAD", "FMMC_ADDBKQT", FE_WARNING_OK)
					ELSE
						SET_WARNING_MESSAGE_WITH_HEADER("FMMC_CLDEAD", "FMMC_ADDBKQF", FE_WARNING_OK)
					ENDIF
					IF HAS_YES_BUTTON_BEEN_PRESSED()	
						sEndOfMission.iAddToPlaylistProgress = 	LBD_BOOKMARK_DONE
						PRINTLN("FMMC EOM - sEndOfMission.iAddToPlaylistProgress = UGC_SET_BOOKMARKED = TRUE - LBD_BOOKMARK_DONE")				
						sEndOfMission.bAddBookMark = FALSE	//TRUE
						sEndOfMission.bRefresh = TRUE
						bResetButtons = TRUE
					ENDIF
				BREAK
				//We are done here
				CASE LBD_BOOKMARK_DONE
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

//Maintain the player quiting
FUNC BOOL MAINTAIN_LEADERBOARD_QUIT_BUTTON(FMMC_EOM_DETAILS& sEndOfMission)
	IF NOT IS_PLAYER_ON_BOOKMARK_SCREEN(sEndOfMission)
	
		IF IS_PC_VERSION()
		AND NETWORK_TEXT_CHAT_IS_TYPING()
			RETURN FALSE
		ENDIF
	
		SWITCH sEndOfMission.iQuitProgress
			CASE LBD_QUIT_INITIAL
				IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()	
				AND NOT sEndOfMission.bBeingKickedFromSurvival
					IF HAS_MISSION_END_BACK_BEEN_PRESSED()		
					AND NOT IS_WARNING_MESSAGE_ACTIVE()
						PRINTLN("FMMC EOM - sEndOfMission.iQuitProgress = LBD_QUIT_CONFIRM_SCREEN ")						
						PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
						sEndOfMission.iQuitProgress = LBD_QUIT_CONFIRM_SCREEN
					ENDIF
				ENDIF
			BREAK
			CASE LBD_QUIT_CONFIRM_SCREEN
				//Bail if on the book mark fail screen
				IF ON_BOOKMARK_ERROR_SCREEN(sEndOfMission)
					RETURN FALSE
				ENDIF
				// Draw confirm black screen
				INT i
				INT iCount
				PLAYER_INDEX PlayerID
				
				REPEAT NUM_NETWORK_PLAYERS i
					PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
					IF NOT (PlayerID = PLAYER_ID())
						IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
							IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerID)
								iCount += 1
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
				AND NOT IS_TRANSITION_SESSIONS_PLAYLIST_SAFE_TO_QUIT()
				AND NOT IS_PLAYER_SCTV(PLAYER_ID())
				AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
					SET_WARNING_MESSAGE_WITH_HEADER("FM_CSC_QUIT", "FM_CSC_QUIT3", (FE_WARNING_YES | FE_WARNING_NO))
				ELSE
					IF iCount > 1
						SET_WARNING_MESSAGE_WITH_HEADER("FM_CSC_QUIT", "FM_LBD_QUIT", (FE_WARNING_YES | FE_WARNING_NO))
					ELSE
						SET_WARNING_MESSAGE_WITH_HEADER("FM_CSC_QUIT", "FM_LBD_1QUIT", (FE_WARNING_YES | FE_WARNING_NO))
					ENDIF
				ENDIF
				
				IF HAS_YES_BUTTON_BEEN_PRESSED()
					PRINTLN("FMMC EOM - sEndOfMission.iQuitProgress = LBD_QUIT_LBD ")
					PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
					sEndOfMission.iQuitProgress = LBD_QUIT_LBD					
					CLEAR_REPLAY_JOB_HELP()
					PRINTLN("FMMC EOM - MAINTAIN_END_OF_MISSION_SCREEN RETURN TRUE, I PRESSED QUIT")
					//Clear that I left with everyone else because I pressed quit
					CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
					
					// Set global for bad sport behaviour
					SET_TRANSITION_SESSIONS_PLAYER_QUIT_FROM_HUD()
					
					g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUIT			
					//Set the transition session vote status
					SET_TRANSITION_SESSION_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUIT)
					RETURN TRUE
				ENDIF
				IF HAS_NO_BUTTON_BEEN_PRESSED()
					PRINTLN("FMMC EOM - sEndOfMission.iQuitProgress = LBD_QUIT_INITIAL ")
					PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET")
					sEndOfMission.iQuitProgress = LBD_QUIT_INITIAL
				ENDIF
			BREAK
			CASE LBD_QUIT_LBD						
				CLEAR_REPLAY_JOB_HELP()
				PRINTLN("FMMC EOM - MAINTAIN_END_OF_MISSION_SCREEN RETURN TRUE, I PRESSED QUIT")
				//Clear that I left with everyone else because I pressed quit
				CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
				g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUIT			
				//Set the transition session vote status
				SET_TRANSITION_SESSION_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUIT)
				RETURN TRUE
		ENDSWITCH
	ENDIF
	RETURN FALSE
ENDFUNC 

// Maintain the player quiting to Freemode (2080160)
FUNC BOOL MAINTAIN_LEADERBOARD_FREEMODE_BUTTON(FMMC_EOM_DETAILS& sEndOfMission)
	IF NOT IS_PLAYER_ON_BOOKMARK_SCREEN(sEndOfMission)
		SWITCH sEndOfMission.iFMProgress
		
			CASE LBD_FM_INITIAL
			
				IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()	
				AND NOT sEndOfMission.bBeingKickedFromSurvival
				AND NOT IS_PLAYER_SCTV(PLAYER_ID())
					IF HAS_MISSION_END_CONTINUE_BEEN_PRESSED()	
					AND NOT IS_WARNING_MESSAGE_ACTIVE()
						PRINTLN("[FM_QUIT] FMMC EOM - MAINTAIN_LEADERBOARD_FREEMODE_BUTTON, HAS_MISSION_END_CONTINUE_BEEN_PRESSED ")						
						PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
						sEndOfMission.iFMProgress = LBD_FM_CONFIRM
					ENDIF
				ENDIF
				
			BREAK
			
			CASE LBD_FM_CONFIRM
			
				// Bail if on the book mark fail screen
				IF ON_BOOKMARK_ERROR_SCREEN(sEndOfMission)
				
					RETURN FALSE
				ENDIF

				// "Are you sure you want to quit the Heist and return to GTA Online?"
				SET_WARNING_MESSAGE_WITH_HEADER("FM_CSC_QUIT", "FM_CSC_QUIT6", (FE_WARNING_YES | FE_WARNING_NO))
				
				IF HAS_YES_BUTTON_BEEN_PRESSED()
				
					PRINTLN("[FM_QUIT] FMMC EOM - MAINTAIN_LEADERBOARD_FREEMODE_BUTTON, HAS_YES_BUTTON_BEEN_PRESSED ")
//					PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
					sEndOfMission.iFMProgress = LBD_FM_QUIT					
					
					CLEAR_REPLAY_JOB_HELP()
					FM_PLAY_LIST_SET_ME_AS_VOTED()
					SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 			
					
					//RETURN TRUE
				ENDIF
				
				IF HAS_NO_BUTTON_BEEN_PRESSED()
					PRINTLN("[FM_QUIT] FMMC EOM - MAINTAIN_LEADERBOARD_FREEMODE_BUTTON, HAS_NO_BUTTON_BEEN_PRESSED ")
					PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET")
					sEndOfMission.iFMProgress = LBD_FM_INITIAL
				ENDIF
				
			BREAK
			
			CASE LBD_FM_QUIT		
//			
//				CLEAR_REPLAY_JOB_HELP()
//				FM_PLAY_LIST_SET_ME_AS_VOTED()
//				SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 			
//
//				RETURN TRUE
			BREAK
			
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CALL_END_OF_MISSION_VOTE_TELEMETRY(FMMC_EOM_DETAILS &sEndOfMission, SERVER_EOM_VARS &sServerFMMC_EOM, BOOL bQuit = FALSE, INT iVoteStatusCache = -1, INT iServerVoteStatusCache = -1)
	
	//Only call this once
	IF sEndOfMission.bTelemetryCalled
		EXIT
	ENDIF
	
	sEndOfMission.bTelemetryCalled = TRUE
	
	DEBUG_PRINTCALLSTACK()
	
	//Net to be connected to call this
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - NETWORK_IS_GAME_IN_PROGRESS = FALSE, bail")					
		EXIT
	ENDIF	
	
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())	
	
	//Call the quit if we're quitting the mode
	IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[iPlayerID].bQuitJob
		PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - Player is skipping, skip vote telemerty")		
		EXIT
	ENDIF
	
	IF GlobalplayerBD_FM[iPlayerID].iEOM_voteStatus = ciFMMC_EOM_VOTE_STATUS_QUIT
	OR bQuit
		PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - Player is skipping, skip vote telemerty because ciFMMC_EOM_VOTE_STATUS_QUIT")					
		CALL_END_OF_MISSION_QUIT_TELEMETRY()
		EXIT
	ENDIF
	
	IF iPlayerID = -1
		PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - iPlayerID = -1, bail")					
		EXIT
	ENDIF
		
	IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION
		PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION = ", g_FMMC_STRUCT.iMissionType , " - bail")					
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.iAdversaryModeType != 0
		PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - g_FMMC_STRUCT.iAdversaryModeType != 0 = ", g_FMMC_STRUCT.iAdversaryModeType , " - bail")					
		EXIT
	ENDIF
	
	STRUCT_MISSION_VOTE sMissionVoteStruct
	sMissionVoteStruct.sMid = g_FMMC_STRUCT.tl31LoadedContentID
	IF iVoteStatusCache = -1
		sMissionVoteStruct.voteChoice = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iEOM_voteStatus
	ELSE
		PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - iVoteStatusCache != -1. sMissionVoteStruct.voteChoice = iVoteStatusCache = ", iVoteStatusCache)					
		sMissionVoteStruct.voteChoice = iVoteStatusCache
	ENDIF
	IF iServerVoteStatusCache = -1
		sMissionVoteStruct.result = sServerFMMC_EOM.iVoteStatus
	ELSE
		PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - iServerVoteStatusCache != -1. sMissionVoteStruct.result = iServerVoteStatusCache = ", iServerVoteStatusCache)					
		sMissionVoteStruct.result = iServerVoteStatusCache
	ENDIF
	
	sMissionVoteStruct.numPlayers = NETWORK_GET_NUM_CONNECTED_PLAYERS()
	sMissionVoteStruct.timeOnline = 0
	sMissionVoteStruct.voteDiff = (sMissionVoteStruct.voteChoice != sMissionVoteStruct.result)

	//Get the status of the end board
	SWITCH sMissionVoteStruct.result
		CASE ciFMMC_EOM_VOTE_STATUS_CONTINUE
		CASE ciFMMC_EOM_VOTE_STATUS_RANDOM
		CASE ciFMMC_EOM_VOTE_STATUS_QUIT
		CASE ciFMMC_EOM_VOTE_STATUS_JOB_VOTE
			PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - ndBoard = TRUE - sMissionVoteStruct.result = ", sMissionVoteStruct.result)					
			sMissionVoteStruct.endBoard = TRUE
		BREAK
		CASE ciFMMC_EOM_VOTE_STATUS_RESTART
		CASE ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
		CASE ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP
		CASE ciFMMC_EOM_VOTE_STATUS_STRAND_MISSION
		CASE ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION
		CASE ciFMMC_EOM_VOTE_STATUS_INVALID
		CASE ciFMMC_EOM_VOTE_STATUS_HEIST_CUT
			PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - ndBoard = FALSE - sMissionVoteStruct.result = ", sMissionVoteStruct.result)					
			sMissionVoteStruct.endBoard = FALSE
		BREAK
	ENDSWITCH

	PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - sMissionVoteStruct.sMid = ", sMissionVoteStruct.sMid)					
	PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - sMissionVoteStruct.voteChoice = ", sMissionVoteStruct.voteChoice)					
	PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - sMissionVoteStruct.result = ", sMissionVoteStruct.result)					
	PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - sMissionVoteStruct.numPlayers = ", sMissionVoteStruct.numPlayers)					
	PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - sMissionVoteStruct.timeOnline = ", sMissionVoteStruct.timeOnline)					
	PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - sMissionVoteStruct.voteDiff = ", sMissionVoteStruct.voteDiff)					
	PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - iVoteStatusCache = ", iVoteStatusCache)
	PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - sMissionVoteStruct.endBoard = ", sMissionVoteStruct.endBoard)
	PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - calling PLAYSTATS_MISSION_VOTE")

	PLAYSTATS_MISSION_VOTE(sMissionVoteStruct)
ENDPROC

//This set's the vars if the vote passes
FUNC BOOL MAINTAIN_SETTING_VARS_ON_VOTE_PASSING(FMMC_EOM_DETAILS& sEndOfMission, SERVER_EOM_VARS &sServerFMMC_EOM)
	IF (sEndOfMission.bOnlyDoQuit = FALSE
	OR IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL())
	AND NOT sEndOfMission.bBeingKickedFromSurvival
		//Check to see if the vote has passed and set a 
		IF sServerFMMC_EOM.bVotePassed
		#IF IS_DEBUG_BUILD
		OR g_sFMMCEOM.bForceThroughVote = TRUE
			g_sFMMCEOM.bForceThroughVote = FALSE
		#ENDIF
			IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
				RETURN TRUE
			ENDIF
			INT iVoteStatusCache = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iEOM_voteStatus
			SET_VARS_ON_FMMC_EOM_VOTE_PASSING(sServerFMMC_EOM.iVoteStatus, sServerFMMC_EOM.vWarpPos, sServerFMMC_EOM.contentID)
			CALL_END_OF_MISSION_VOTE_TELEMETRY(sEndOfMission, sServerFMMC_EOM, DEFAULT, iVoteStatusCache)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DID_PLAYER_QUIT_JOB()

	INT iSlot = NATIVE_TO_INT(PLAYER_ID())
	
	IF iSlot <> -1
		IF IS_BIT_SET(GlobalplayerBD_FM[iSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob) = TRUE	
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_AMMO_AND_ARMOUR(LBD_VARS& lbdVars)
	IF NOT SHOULD_BLOCK_AMMO_ARMOUR()
		HANDLE_BUYING_ARMOUR(lbdVars.sAmmoArmour)
		HANDLE_BUYING_AMMO(lbdVars.sAmmoArmour)
	ENDIF
ENDPROC

CONST_INT ciLBD_TIMEOUT 5000

FUNC BOOL SVM_SKIP_QUICK_RESTART()
	
	IF NOT DOES_CURRENT_MISSION_USE_GANG_BOSS()
		PRINTLN("[SVMFLOW] SVM_SKIP_QUICK_RESTART - NOT DOES_CURRENT_MISSION_USE_GANG_BOSS()")					
		RETURN FALSE
	ENDIF
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceQuicRestart")
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_IgnoreSvmWvmBossCheck")
		PRINTLN("[SVMFLOW] SVM_SKIP_QUICK_RESTART - IF GET_COMMANDLINE_PARAM_EXISTS(\"sc_ForceQuicRestart\")")					
		RETURN FALSE
	ENDIF
	IF g_bLaunchedViaZmenu
		PRINTLN("[SVMFLOW] SVM_SKIP_QUICK_RESTART - g_bLaunchedViaZmenu")					
		RETURN FALSE
	ENDIF
	#ENDIF
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
		PRINTLN("[SVMFLOW] SVM_SKIP_QUICK_RESTART - GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()")					
		RETURN TRUE
	ENDIF
	IF NOT NETWORK_IS_PLAYER_ACTIVE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		PRINTLN("[SVMFLOW] SVM_SKIP_QUICK_RESTART - NOT NETWORK_IS_PLAYER_ACTIVE(GB_GET_LOCAL_PLAYER_GANG_BOSS())")					
		RETURN TRUE
	ENDIF
	PRINTLN("[SVMFLOW] SVM_SKIP_QUICK_RESTART - RETURN FALSE")					
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SHOW_QUICK_RESTART_TEAM_SWAP()
	IF NOT IS_THIS_A_TWO_TEAM_MISSION()
		RETURN FALSE
	ENDIF
	
	IF DOES_CURRENT_MISSION_USE_GANG_BOSS()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisableQuickRestartTeamSwap)
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
	
ENDFUNC

//Control the end of mission vote and help text.
FUNC  BOOL MAINTAIN_END_OF_MISSION_SCREEN(	LBD_VARS& lbdVars,
											FMMC_EOM_DETAILS 	&sEndOfMission, 
											SERVER_EOM_VARS 	&sServerFMMC_EOM, 
											BOOL 				bTimerExpired 	= TRUE, 
											BOOL 				bPlayerCard 	= TRUE, 
											BOOL 				bSpectate 		= TRUE, 
											BOOL 				bShowMove 		= TRUE, 
											BOOL 				bfacebook 		= TRUE, 
											BOOL 				bSCLB 			= TRUE,
											TIME_DATATYPE 		Timer			= NULL,
											INT 				iOverTimeOut	= ciLBD_TIMEOUT, 
											STRING 				stEnd			= NULL,
											BOOL 				bMiniGame		= FALSE,
											BOOL 				bAllowContinue	= TRUE,
											BOOL 				bIsJobVote		= FALSE,
											BOOL				bMissionPassed  = FALSE, 
											BOOL 				bHidePlayerCard = FALSE,
											BOOL 				bQuitEarly 		= FALSE)
											
	IF SHOULD_HOLD_FOR_DEV_SPECTATOR()
		PRINTLN("[DEV_SPC] MAINTAIN_END_OF_MISSION_SCREEN, SHOULD_HOLD_FOR_DEV_SPECTATOR")
		RETURN FALSE
	ENDIF
	
	IF g_FinalRoundsLbd
		PRINTLN("[DEV_SPC] MAINTAIN_END_OF_MISSION_SCREEN, g_FinalRoundsLbd")
		RETURN FALSE
	ENDIF
											
	//If this is a strnad mission then get out
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
		PRINTLN("[TS] [MSRAND] - MAINTAIN_END_OF_MISSION_SCREEN - IS_THIS_A_STRAND_MISSION = TRUE - returning TRUE")
		RETURN TRUE
	ENDIF
	
	//If we should get out because the NJVS is being skipped
	IF SHOULD_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
		PRINTLN("[TS] MAINTAIN_END_OF_MISSION_SCREEN - SHOULD_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS - skipping")
		TEXT_LABEL_23 contentID
		IF SHOULD_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE()
			SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_HEIST_CUT, <<0.0, 0.0, 0.0>>, contentID)
		ELSE
			SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, contentID)
		ENDIF
		RETURN TRUE
	ENDIF
											
	INT iTimer
	BOOL bResetButtons	= FALSE
	//Id there is a count down timer then calculate is as seconds 
	IF Timer != NULL
		//Get the timer as seconds
		iTimer = (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Timer)) // /1000 
		// Count downwards 
		iTimer = (iOverTimeOut - iTimer) //1000
		IF iTimer < 0
			iTimer = -1
		ENDIF
	ELSE
		iTimer = -1
	ENDIF
	
	IF iTimer = -1
		sEndOfMission.bTimerWasNull = TRUE
	ELSE
		IF sEndOfMission.bTimerWasNull
			sEndOfMission.bTimerWasNull = FALSE
			bResetButtons = TRUE
		ENDIF
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF NOT IS_BIT_SET(lbdVars.sAmmoArmour.iBitSet, LBD_BOOL_WAS_USING_KEYBOARD)
			SET_BIT(lbdVars.sAmmoArmour.iBitSet, LBD_BOOL_WAS_USING_KEYBOARD)
			PRINTLN("[TS] MAINTAIN_END_OF_MISSION_SCREEN - LBD_BOOL_WAS_USING_KEYBOARD - SET ")
		ENDIF
	ELSE
		IF IS_BIT_SET(lbdVars.sAmmoArmour.iBitSet, LBD_BOOL_WAS_USING_KEYBOARD)
			CLEAR_BIT(lbdVars.sAmmoArmour.iBitSet, LBD_BOOL_WAS_USING_KEYBOARD)
			SET_BIT(lbdVars.sAmmoArmour.iBitSet, LBD_BOOL_REFRESH_NEEDED)
			PRINTLN("[TS] MAINTAIN_END_OF_MISSION_SCREEN - LBD_BOOL_WAS_USING_KEYBOARD - CLEAR - SET LBD_BOOL_REFRESH_NEEDED ")
		ENDIF		
	ENDIF
	
	// Refresh ammo/armour
	IF IS_BIT_SET(lbdVars.sAmmoArmour.iBitSet, LBD_BOOL_REFRESH_NEEDED)
		bResetButtons = TRUE
		CLEAR_BIT(lbdVars.sAmmoArmour.iBitSet, LBD_BOOL_REFRESH_NEEDED)
	ENDIF
	
	INT iScreenX, iScreenY
	
	// PT Fix 1428861 (Pull this from the leaderboard until we work out the deal.)
//	IF NOT FACEBOOK_CAN_POST_TO_FACEBOOK()
//	OR g_sFMMCEOM.bPostToFacebook
		bfacebook = FALSE
//	ENDIF
	
	IF bPlayerCard 	= TRUE
//		CHECK_IS_GAMER_TAG_IS_ACTIVATED(sEndOfMission.bShowPlayerCard, sEndOfMission.bPlayercardTriggered) // added by Kevin. to show gamer card #1006218
	ENDIF
	
	IF g_bOnHorde
//	AND NOT sEndOfMission.bBeingKickedFromSurvival
	AND sEndOfMission.iProgress != ciEND_OF_MISSION_HORDE
		SET_ME_AS_READY_FOR_JOB_VOTES()
		SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE)
		FM_PLAY_LIST_SET_ME_AS_VOTED()
		FM_PLAY_LIST_SET_AT_END_OF_MISSION()
		sEndOfMission.iProgress = ciEND_OF_MISSION_HORDE
	ENDIF
	
	
	BOOL bShowQuit = FALSE
	
	//Set the quit flags if we're on a heist
	IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
		bQuitEarly = TRUE
		bShowQuit = TRUE
	ENDIF
	
	SWITCH sEndOfMission.iProgress
	
		//Load stuff needed and set up the menu
		CASE ciEND_OF_MISSION_INI
			CLOSE_SOCIAL_CLUB_MENU()
			CLEANUP_MENU_ASSETS()
			CLEAR_MENU_DATA()
			SET_FRONTEND_ACTIVE(FALSE)
			RESET_PAUSED_RENDERPHASES()
			PRINTLN("FMMC EOM - SET_FRONTEND_ACTIVE(FALSE) called")
			SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_SET_UP)
		BREAK
		
		CASE ciEND_OF_MISSION_SET_UP
		
			IF LOAD_MENU_ASSETS()
			
				SET_LOCAL_PLAYER_ON_MISSION_LB()
				
				IF CONTENT_IS_USING_ARENA()
					IF DOES_PLAYER_OWN_AN_ARENA_GARAGE(PLAYER_ID())
					AND g_TransitionSessionNonResetVars.bStartedFirstArenaEvent
					AND (NOT IS_THIS_A_ROUNDS_MISSION() OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD())
						PRINTLN("[ARENA TUT] Skipping NJVS for spawn in the arena workshop - ciEND_OF_MISSION_SET_UP")
						SET_LOCAL_PLAYER_DOING_ARENA_TUTORIAL()
					ENDIF
				ENDIF
				
				//If the mission is bookmarked then we can't book mark it again
				IF g_FMMC_STRUCT.bBookMarked = TRUE
				OR Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
				OR Is_Player_Currently_On_MP_Random_Event(PLAYER_ID())
				OR Is_Player_Currently_On_MP_Heist(PLAYER_ID()) 
				OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
				OR CV2_WAS_STUNT_RACE_LAUNCHED_AS_PROFESSIONAL()
					sEndOfMission.bAddBookMark = FALSE
				ENDIF
				sEndOfMission.bEnoughCash = FALSE				 
				IF CAN_I_AFFORD_TO_PLAY_THIS_ACTIVITY(g_FMMC_STRUCT.iMissionType, g_FMMC_STRUCT.iMissionSubType)
					PRINTLN("FMMC EOM - sEndOfMission.bEnoughCash = TRUE")					
					sEndOfMission.bEnoughCash = TRUE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("FMMC EOM - sEndOfMission.bEnoughCash = FALSE")					
				#ENDIF
				ENDIF
				
				sEndOfMission.bHideQuickRestart = SHOULD_HIDE_SHOW_QUICK_RESTART_OPTION_BECAUSE_OF_SPECTATOR()
				IF NOT IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
				AND NOT IS_LOADED_MISSION_TYPE_FLOW_MISSION()
				AND(sEndOfMission.bHideQuickRestart
				OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
				OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
				OR IS_THIS_A_ROUNDS_MISSION()
				OR IS_PLAYER_SCTV(PLAYER_ID())
				OR IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()
				OR SVM_SKIP_QUICK_RESTART())
					#IF IS_DEBUG_BUILD
					IF sEndOfMission.bHideQuickRestart					PRINTLN("FMMC EOM - [QUICK_RESTART] sEndOfMission.bHideQuickRestart				 = TRUE") ENDIF
					IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()			PRINTLN("FMMC EOM - [QUICK_RESTART] IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()		 = TRUE") ENDIF
					IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()		PRINTLN("FMMC EOM - [QUICK_RESTART] SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD() = TRUE") ENDIF
					IF IS_THIS_A_ROUNDS_MISSION()						PRINTLN("FMMC EOM - [QUICK_RESTART] IS_THIS_A_ROUNDS_MISSION()					 = TRUE") ENDIF
					IF IS_PLAYER_SCTV(PLAYER_ID())						PRINTLN("FMMC EOM - [QUICK_RESTART] IS_PLAYER_SCTV(PLAYER_ID())					 = TRUE") ENDIF
					IF IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()				PRINTLN("FMMC EOM - [QUICK_RESTART] IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()			 = TRUE") ENDIF
					#ENDIF
					
					PRINTLN("FMMC EOM - [QUICK_RESTART] sEndOfMission.bHideQuickRestart = FALSE")					
					CLEAR_SHOW_QUICK_RESTART_OPTION()
					sEndOfMission.bShowQuickRestart = FALSE
					sEndOfMission.bShowQuickRestartSwap = FALSE
					CLEAR_SHOW_QUICK_RESTART_OPTION()
				ELSE
					sEndOfMission.bShowQuickRestart 	= SHOULD_SHOW_QUICK_RESTART_OPTION()
					PRINTLN("FMMC EOM - [QUICK_RESTART] sEndOfMission.bHideQuickRestart = SHOULD_SHOW_QUICK_RESTART_OPTION = ", sEndOfMission.bShowQuickRestart)					
					sEndOfMission.bShowQuickRestartSwap = SHOULD_SHOW_QUICK_RESTART_TEAM_SWAP()
				ENDIF
				
				sEndOfMission.bIsHeist 				= (Is_Player_Currently_On_MP_Heist(PLAYER_ID()) OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))	// KGM 5/3/14: Heist Planning added
				sEndOfMission.bIsContactMission 	= Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
				sEndOfMission.bMiniGame				= bMiniGame
				sEndOfMission.bHidePlayerCard 		= bHidePlayerCard
				//If it's a heist don't show the swap teams thing
				IF sEndOfMission.bIsHeist
				OR SHOULD_WINNER_SWAP_TO_SMALLEST_TEAM()
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciALWAYS_SWAP_SMALL_TEAM)
				OR IS_LOADED_MISSION_TYPE_FLOW_MISSION()
					PRINTLN("FMMC EOM - [QUICK_RESTART] sEndOfMission.bShowQuickRestartSwap = FALSE")					
					sEndOfMission.bShowQuickRestartSwap = FALSE
				ENDIF
				#IF IS_DEBUG_BUILD
				IF sEndOfMission.bShowQuickRestart
					PRINTLN("FMMC EOM [QUICK_RESTART] - sEndOfMission.bShowQuickRestart = TRUE")	
				ELSE
					PRINTLN("FMMC EOM [QUICK_RESTART] - sEndOfMission.bShowQuickRestart = FALSE")					
				ENDIF
				IF sEndOfMission.bShowQuickRestartSwap
					PRINTLN("FMMC EOM [QUICK_RESTART] - sEndOfMission.bShowQuickRestartSwap	= TRUE")	
				ELSE
					PRINTLN("FMMC EOM [QUICK_RESTART] - sEndOfMission.bShowQuickRestartSwap	= FALSE")					
				ENDIF				
				IF NOT sEndOfMission.bIsContactMission
					PRINTLN("FMMC EOM - THIS IS NOT A CONTACT MISSION")
				ELSE
					PRINTLN("FMMC EOM - THIS IS A CONTACT MISSION")
				ENDIF
				IF sEndOfMission.bIsHeist 
					PRINTLN("FMMC EOM - sEndOfMission.bIsHeist	= TRUE")	
				ELSE
					PRINTLN("FMMC EOM - sEndOfMission.bIsHeist	= FALSE")					
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_FMMC_EOMbIsHeist")
						PRINTLN("FMMC EOM - sEndOfMission.bIsHeist	= sc_FMMC_EOMbIsHeist")					
						sEndOfMission.bIsHeist = TRUE
					ENDIF
				ENDIF	
				#ENDIF
				
				IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() 
				OR DID_PLAYER_QUIT_JOB()
					sEndOfMission.bOnlyDoQuit = TRUE
					PRINTLN("FMMC EOM - sEndOfMission.bOnlyDoQuit	= TRUE")
				ENDIF
				
				sEndOfMission.bAllowedToVoteLocal = sServerFMMC_EOM.bAllowedToVote
				sEndOfMission.bSetupWithSpectate = bSpectate
				
				
				IF NOT SHOULD_ALLOW_THUMB_VOTE()
				OR sEndOfMission.bMiniGame
				OR sEndOfMission.bOnlyDoQuit
				OR ((sEndOfMission.bIsContactMission OR Is_Player_Currently_On_MP_Random_Event(PLAYER_ID())) AND NOT bMissionPassed)
				
					//If you are only able to quit
					IF sEndOfMission.bOnlyDoQuit
					OR sEndOfMission.bBeingKickedFromSurvival
						PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - sEndOfMission.bOnlyDoQuit	= TRUE")
						IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, FALSE, FALSE, bfacebook, bSCLB, FALSE, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark, FALSE, FALSE, sEndOfMission.bBeingKickedFromSurvival)
						ELSE
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, FALSE, FALSE, bfacebook, FALSE, FALSE, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark, FALSE, FALSE, sEndOfMission.bBeingKickedFromSurvival)
						ENDIF
						
					//If the serversays we can't vote
					ELIF sServerFMMC_EOM.bAllowedToVote = FALSE
						PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - sEndOfMission.bAllowedToVote	= FALSE")
						IF sEndOfMission.bIsContactMission OR Is_Player_Currently_On_MP_Random_Event(PLAYER_ID())
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, TRUE, TRUE, bPlayerCard, FALSE, bShowMove, bfacebook, bSCLB, FALSE, iTimer, 
													stEnd, sEndOfMission.bShowQuickRestart, sEndOfMission.bShowQuickRestartSwap, sEndOfMission.bAddBookMark)
						ELSE
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, SHOULD_EOM_RESTART_BE_DISPLAYED(sEndOfMission, sServerFMMC_EOM, bIsJobVote), FALSE,
													SHOULD_EOM_CONTINUE_BE_DISPLAYED(sServerFMMC_EOM, bAllowContinue), TRUE, bPlayerCard, FALSE, bShowMove, bfacebook, bSCLB, FALSE, iTimer, stEnd, 
													sServerFMMC_EOM.bAllowedToVote AND sEndOfMission.bShowQuickRestart, 
													sServerFMMC_EOM.bAllowedToVote AND sEndOfMission.bShowQuickRestartSwap, sEndOfMission.bAddBookMark, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bQuitEarly)
						ENDIF
					ELSE
						PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - ciEND_OF_MISSION_SET_UP")
						IF sEndOfMission.bIsContactMission OR Is_Player_Currently_On_MP_Random_Event(PLAYER_ID())
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, TRUE, TRUE, bPlayerCard, FALSE, bShowMove, bfacebook, bSCLB, FALSE, iTimer, 
													stEnd, sEndOfMission.bShowQuickRestart, sEndOfMission.bShowQuickRestartSwap, sEndOfMission.bAddBookMark, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bQuitEarly)
						ELSE
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, SHOULD_EOM_RESTART_BE_DISPLAYED(sEndOfMission, sServerFMMC_EOM, bIsJobVote), FALSE,
												SHOULD_EOM_CONTINUE_BE_DISPLAYED(sServerFMMC_EOM, bAllowContinue), TRUE, bPlayerCard, FALSE, bShowMove, bfacebook, bSCLB, FALSE, iTimer, stEnd, 
												sEndOfMission.bShowQuickRestart, sEndOfMission.bShowQuickRestartSwap, sEndOfMission.bAddBookMark, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bQuitEarly)
						ENDIF
					ENDIF
					
					
					PRINTLN("FMMC EOM - MAINTAIN_END_OF_MISSION_SCREEN - moving ciEND_OF_MISSION_DO_VOTE ")
					//make use save the save stuff
					sEndOfMission.iThumbStage = tvsWaitForSave
					SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_DO_VOTE)
				ELSE
					// 2019009
					IF SHOULD_BYPASS_LIKE_DISLIKE_LOGIC()
					
						SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, 
												TRUE, 
												FALSE, 
												NOT DID_I_JOIN_MISSION_AS_SPECTATOR(), 
												bShowQuit, 
												bPlayerCard, 
												FALSE, 
												bShowMove, 
												bfacebook,
												bSCLB, 
												FALSE, 
												iTimer, 
												stEnd, 
												sEndOfMission.bShowQuickRestart AND sEndOfMission.bIsHeist,// AND sServerFMMC_EOM.bAllowedToVote, 
												FALSE, 			
												sEndOfMission.bAddBookMark, 
												FALSE, 
												FALSE, 
												sEndOfMission.bBeingKickedFromSurvival, 
												NOT DID_I_JOIN_MISSION_AS_SPECTATOR(), bQuitEarly)
					
						SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_DO_VOTE)
						
						PRINTLN("FMMC EOM - MAINTAIN_END_OF_MISSION_SCREEN - SHOULD_BYPASS_LIKE_DISLIKE_LOGIC, TRUE (skip voting)")
					ELSE
						PRINTLN("FMMC EOM - MAINTAIN_END_OF_MISSION_SCREEN - SHOULD_BYPASS_LIKE_DISLIKE_LOGIC, FALSE")						
						SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, 
												TRUE, 
												FALSE, 
												DID_I_JOIN_MISSION_AS_SPECTATOR(), 
												bShowQuit, 
												bPlayerCard,
												FALSE, 
												bShowMove,
												bfacebook,
												bSCLB, 
												FALSE, 
												iTimer, 
												stEnd, 
												sEndOfMission.bShowQuickRestart AND sEndOfMission.bIsHeist AND sServerFMMC_EOM.bAllowedToVote, 
												FALSE, 
												sEndOfMission.bAddBookMark, 
												FALSE, 
												FALSE, 
												sEndOfMission.bBeingKickedFromSurvival, 
												NOT DID_I_JOIN_MISSION_AS_SPECTATOR(), bQuitEarly)
												
						SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
						
						SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_RATE_JOB)
					ENDIF
				ENDIF
				
				FM_PLAY_LIST_SET_AT_END_OF_MISSION()
				
			ELSE
			
				PRINTLN("FMMC EOM - LOAD_MENU_ASSETS = FALSE")	
				
			ENDIF
	
		BREAK
		
		CASE ciEND_OF_MISSION_RATE_JOB
		
			DO_AMMO_AND_ARMOUR(lbdVars)
			
			IF sEndOfMission.bAllowedToVoteLocal != sServerFMMC_EOM.bAllowedToVote
				IF sEndOfMission.bResetWithOutTimer
					sEndOfMission.bResetWithOutTimer = FALSE
				ENDIF
			ENDIF
			
			IF sEndOfMission.bResetWithOutTimer = FALSE
				IF Timer != NULL
					IF iTimer < 1
						sEndOfMission.bResetWithOutTimer = TRUE
						bResetButtons = TRUE
						iTimer = -1
					ENDIF
				ENDIF
			ELSE
				iTimer = -1
			ENDIF
			
			IF LOAD_MENU_ASSETS()
				GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
				DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, iTimer, TRUE, TRUE, TRUE, DEFAULT, DEFAULT)//, TRUE)
			ENDIF
			
			IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
			
				IF NOT IS_PLAYER_ON_BOOKMARK_SCREEN(sEndOfMission)
				AND NOT IS_PLAYER_ON_QUIT_CONFIRM_SCREEN(sEndOfMission)
				
					//Deal with the quick restart
					IF NOT FM_PLAY_LIST_HAVE_I_VOTED()
						IF sEndOfMission.bOnlyDoQuit = FALSE
							IF SHOULD_SHOW_QUICK_RESTART_OPTION()
								RESET_UP_HELP_IF_VOTE_STATUS_CHANGE(lbdVars, sEndOfMission, sServerFMMC_EOM, bIsJobVote, bAllowContinue, bPlayerCard, FALSE, bShowMove, bfacebook, bSCLB, stEnd, iTimer, bQuitEarly)
								//If the server says we can vote
								IF sServerFMMC_EOM.bAllowedToVote
								AND sEndOfMission.bEnoughCash
									IF NOT sEndOfMission.bHideQuickRestart
										//Check for quick restart
										MAINTAIN_MISSION_QUICK_RESTART(lbdVars, sEndOfMission, bMiniGame, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, stEnd, iTimer, sServerFMMC_EOM.bAllowedToVote)
										//Check for quick restart with a wee team swap
										MAINTAIN_MISSION_QUICK_RESTART_SWAP_TEAMS(lbdVars, sEndOfMission, bMiniGame, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, stEnd, iTimer, sServerFMMC_EOM.bAllowedToVote)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					
				//Deal with the thumb vote
				SWITCH sEndOfMission.iThumbStage					
					CASE tvsRefreshHelp
						IF NOT sEndOfMission.bIsContactMission 
						AND NOT Is_Player_Currently_On_MP_Random_Event(PLAYER_ID())
							SET_ME_AS_READY_FOR_JOB_VOTES()
							CLEAR_REPLAY_JOB_HELP()
							//FM_PLAY_LIST_SET_ME_AS_VOTED()
							PRINTLN("FMMC EOM - ciEND_OF_MISSION_RATE_JOB - PLAYER HAS THUMB VOTED")
						ENDIF
						sEndOfMission.iThumbStage = tvsWaitForSave
						sEndOfMission.bRefresh = TRUE
					BREAK
					
					CASE tvsComplete
						PRINTLN("FMMC EOM - tvscomplete")
						IF sEndOfMission.bIsContactMission
						OR Is_Player_Currently_On_MP_Random_Event(PLAYER_ID())
						OR Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
							SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_DO_VOTE)
							//sEndOfMission.bShowQuickRestart = TRUE
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, TRUE, FALSE, TRUE, TRUE, bPlayerCard, FALSE, bShowMove, bfacebook, bSCLB, FALSE, iTimer, stEnd, 
												sServerFMMC_EOM.bAllowedToVote AND sEndOfMission.bShowQuickRestart, 
												sServerFMMC_EOM.bAllowedToVote AND sEndOfMission.bShowQuickRestartSwap, sEndOfMission.bAddBookMark)
							CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
						ELSE
							IF bQuitEarly
								sEndOfMission.bRefresh = TRUE
								PRINTLN("FMMC EOM - ciEND_OF_MISSION_RACE")
								SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, TRUE, bShowMove, bfacebook, bSCLB, FALSE, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark, FALSE, FALSE, FALSE, FALSE, bQuitEarly)
								SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_RACE)
								sEndOfMission.bRefresh = FALSE
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				//Spectator
				SET_ME_AS_READY_FOR_JOB_VOTES()
				CLEAR_REPLAY_JOB_HELP()
				FM_PLAY_LIST_SET_ME_AS_VOTED()
				PRINTLN("FMMC EOM - HAS_MISSION_END_CONTINUE_BEEN_PRESSED IN RATE JOB AS SPECTATOR")	
				
				bPlayerCard = TRUE
				SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, bShowQuit, bPlayerCard, FALSE, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark)
			ENDIF
			
//			PRINTLN("FMMC EOM - iTimer ... ", iTimer)
			
			// check for showing SC LB
			MAINTAIN_SELECTING_WORLD_LEADERBOARD(sEndOfMission, bSCLB, bResetButtons)
			//Maintain the add to playlist button
			MAINTAIN_ADD_BOOKMARK_BUTTON(sEndOfMission, bResetButtons)

			IF MAINTAIN_SETTING_VARS_ON_VOTE_PASSING(sEndOfMission, sServerFMMC_EOM)	
			
				IF NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
					SET_VARS_ON_FMMC_EOM_VOTE_PASSING(sServerFMMC_EOM.iVoteStatus, sServerFMMC_EOM.vWarpPos, sServerFMMC_EOM.contentID)
				ENDIF
				SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_DONE)
			ENDIF
			
			IF sEndOfMission.iProgress = ciEND_OF_MISSION_RATE_JOB
				IF sEndOfMission.bRefresh
				OR bResetButtons
				OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
					PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP SET_UP_MISSION_END_HELP SET_UP_MISSION_END_HELP SET_UP_MISSION_END_HELP >>>>>>>> ")	
					SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, FALSE, bShowMove, bfacebook, bSCLB, FALSE, iTimer, stEnd, 
											FALSE, FALSE, sEndOfMission.bAddBookMark, FALSE, FALSE, sEndOfMission.bBeingKickedFromSurvival, sEndOfMission.iThumbStage = tvsWaitForVote AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR())
					sEndOfMission.bRefresh = FALSE
				ENDIF
			ENDIF

		BREAK
		
		// 1670077 Allow players to quit early instead of hanging around waiting for 50% to finish the race
		CASE ciEND_OF_MISSION_RACE
			IF bQuitEarly
				SWITCH sEndOfMission.iRaceStage
					CASE 0
						sEndOfMission.bRefresh = TRUE
						SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, TRUE, bShowMove, bfacebook, bSCLB, FALSE, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark, FALSE, FALSE, FALSE, FALSE, bQuitEarly)
						sEndOfMission.bRefresh = FALSE
						
						PRINTLN("FMMC EOM - ciEND_OF_MISSION_RACE, sEndOfMission.iRaceStage = 1")
						sEndOfMission.iRaceStage = 1
					BREAK
					CASE 1
						IF MAINTAIN_LEADERBOARD_QUIT_BUTTON(sEndOfMission)
							CALL_END_OF_MISSION_VOTE_TELEMETRY(sEndOfMission, sServerFMMC_EOM, TRUE)
							RETURN TRUE
						ENDIF
						
						// check for showing SC LB
						MAINTAIN_SELECTING_WORLD_LEADERBOARD(sEndOfMission, bSCLB, bResetButtons)
						//Maintain the add to playlist button
						MAINTAIN_ADD_BOOKMARK_BUTTON(sEndOfMission, bResetButtons)
							
						IF LOAD_MENU_ASSETS()
							GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
							DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, iTimer, TRUE, TRUE, TRUE, DEFAULT, DEFAULT)//, TRUE)
						ENDIF
					BREAK	
				ENDSWITCH
			ELSE
				SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_DO_VOTE)
			ENDIF
		BREAK
		
		//Wait for the vote
		CASE ciEND_OF_MISSION_DO_VOTE
		
			DO_AMMO_AND_ARMOUR(lbdVars)
		
//			PRINTLN("sEndOfMission.bAllowedToVoteLocal = ",	sEndOfMission.bAllowedToVoteLocal	)
//			PRINTLN("sServerFMMC_EOM.bAllowedToVote = ",	sServerFMMC_EOM.bAllowedToVote 		)
//			PRINTLN("sEndOfMission.bResetWithOutTimer = ",	sEndOfMission.bResetWithOutTimer	)
//			PRINTLN("sEndOfMission.bAddBookMark = ",		sEndOfMission.bAddBookMark			)
			
			IF sEndOfMission.bAllowedToVoteLocal != sServerFMMC_EOM.bAllowedToVote
				IF sEndOfMission.bResetWithOutTimer
					sEndOfMission.bResetWithOutTimer = FALSE
				ENDIF
			ENDIF
			
			IF sEndOfMission.bResetWithOutTimer = FALSE
				IF Timer != NULL
					IF iTimer < 1
						sEndOfMission.bResetWithOutTimer = TRUE
						PRINTLN("FMMC EOM - sEndOfMission.bResetWithOutTimer = TRUE")	
						bResetButtons = TRUE
						iTimer = -1
					ENDIF
				ENDIF
			ELSE
				iTimer = -1
			ENDIF
			
			//Draw the menu help
			IF LOAD_MENU_ASSETS()
				//If there is more than one player in my session then always draw
				IF NETWORK_GET_TOTAL_NUM_PLAYERS() > 1				
					GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
					DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, iTimer, TRUE, TRUE, TRUE, DEFAULT, DEFAULT)//, TRUE)
				ELSE
					//if i've voted then don't draw to avoid the buttons popping. 
					IF NOT HAS_PLAYER_VOTED_AT_END_OF_MISSION(PLAYER_ID()) 
						GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
						DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, iTimer, TRUE, TRUE, TRUE, DEFAULT, DEFAULT)//, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF bIsJobVote
			AND NOT sEndOfMission.bBeingKickedFromSurvival
			AND NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
			
			
				PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - TOP ")
			
				IF bResetButtons
					IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
						PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - sEndOfMission.bOnlyDoQuit	= TRUE")
						SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, FALSE, FALSE, bfacebook, FALSE, FALSE, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark, FALSE, FALSE, sEndOfMission.bBeingKickedFromSurvival)
					ELSE
						IF sEndOfMission.bIsContactMission
						OR Is_Player_Currently_On_MP_Random_Event(PLAYER_ID())
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, SHOULD_EOM_RESTART_BE_DISPLAYED(sEndOfMission, sServerFMMC_EOM, bIsJobVote), FALSE,
												SHOULD_EOM_CONTINUE_BE_DISPLAYED(sServerFMMC_EOM, bAllowContinue), TRUE, bPlayerCard, 
												FALSE, bShowMove, bfacebook, bSCLB, FALSE, iTimer, stEnd, 
												sServerFMMC_EOM.bAllowedToVote AND sEndOfMission.bShowQuickRestart, 
												sServerFMMC_EOM.bAllowedToVote AND sEndOfMission.bShowQuickRestartSwap, sEndOfMission.bAddBookMark)
						ENDIF
					ENDIF
				ENDIF
				
//				IF NOT IS_PLAYER_ON_BOOKMARK_SCREEN(sEndOfMission)
//				AND NOT IS_PLAYER_ON_QUIT_CONFIRM_SCREEN(sEndOfMission)  // Not on the confirm screen
				IF NOT IS_WARNING_MESSAGE_ACTIVE()
					IF SHOULD_EOM_CONTINUE_BE_DISPLAYED(sServerFMMC_EOM, bAllowContinue)	
					
						// Vote for end Jobs
						IF HAS_MISSION_END_CONTINUE_BEEN_PRESSED()	
							SET_ME_AS_READY_FOR_JOB_VOTES()
							CLEAR_REPLAY_JOB_HELP()
							FM_PLAY_LIST_SET_ME_AS_VOTED()
							PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - HAS_MISSION_END_CONTINUE_BEEN_PRESSED (1) ")						
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, FALSE, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark)
						ENDIF
						
					ENDIF
				ENDIF
					
				// Time out pass vote
				IF bTimerExpired
					SET_ME_AS_READY_FOR_JOB_VOTES()
				ENDIF
				
				// Vote passes move on
				IF sServerFMMC_EOM.bJobVotesYes = TRUE
					CALL_END_OF_MISSION_VOTE_TELEMETRY(sEndOfMission, sServerFMMC_EOM, DEFAULT, ciFMMC_EOM_VOTE_STATUS_CONTINUE, ciFMMC_EOM_VOTE_STATUS_CONTINUE)
					RETURN TRUE
				ENDIF
								
				//Reset up the help if the server says so
				RESET_UP_HELP_IF_VOTE_STATUS_CHANGE(lbdVars, sEndOfMission, sServerFMMC_EOM, bIsJobVote, bAllowContinue, bPlayerCard, FALSE, bShowMove, bfacebook, bSCLB, stEnd, iTimer, bQuitEarly)
				
				//Check vote the vote presses
				IF NOT FM_PLAY_LIST_HAVE_I_VOTED()
					IF sEndOfMission.bOnlyDoQuit = FALSE
						//If the server says we can vote
						IF sServerFMMC_EOM.bAllowedToVote
						AND sEndOfMission.bEnoughCash
							IF NOT IS_PLAYER_ON_QUIT_CONFIRM_SCREEN(sEndOfMission)  // Not on the confirm screen
							AND NOT IS_PLAYER_ON_BOOKMARK_SCREEN(sEndOfMission)
							AND NOT sEndOfMission.bHideQuickRestart
							AND SHOULD_SHOW_QUICK_RESTART_OPTION()
								//Check for quick restart
								MAINTAIN_MISSION_QUICK_RESTART(lbdVars, sEndOfMission, bMiniGame, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, stEnd, iTimer, sServerFMMC_EOM.bAllowedToVote)
								//Check for quick restart with a wee team swap
								MAINTAIN_MISSION_QUICK_RESTART_SWAP_TEAMS(lbdVars, sEndOfMission, bMiniGame, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, stEnd, iTimer, sServerFMMC_EOM.bAllowedToVote)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT sEndOfMission.bIsContactMission 
//				AND Is_Player_Currently_On_MP_Random_Event(PLAYER_ID())
					PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - MAINTAIN_ADD_BOOKMARK_BUTTON ")
					//Maintain the add to playlist button
					MAINTAIN_ADD_BOOKMARK_BUTTON(sEndOfMission, bResetButtons)
				ELSE
					PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - MAINTAIN_ADD_BOOKMARK_BUTTON, bIsContactMission ")
				ENDIF
				
				//Maintain the spectate stuff
				MAINTAIN_SETUP_FOR_SPECTATE(lbdVars, sEndOfMission, sServerFMMC_EOM, bIsJobVote, bAllowContinue, bResetButtons, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, stEnd, iTimer)
				
				// check for showing SC LB
				MAINTAIN_SELECTING_WORLD_LEADERBOARD(sEndOfMission, bSCLB, bResetButtons)
				
				//This set's the vars if the vote passes
				IF MAINTAIN_SETTING_VARS_ON_VOTE_PASSING(sEndOfMission, sServerFMMC_EOM)
					SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_DONE)
				ENDIF
				
				IF IS_PLAYER_SCTV(PLAYER_ID())
				AND NOT IS_VALID_SPECTATE_TARGET_IN_SESSION()
				
					PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - IS_VALID_SPECTATE_TARGET_IN_SESSION, RETURN TRUE ")		
					RETURN TRUE
				ENDIF
				
			ELSE
				PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - BOTTOM ")
				
				//If the player is on the intro race tutorial
				IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
				OR sEndOfMission.bBeingKickedFromSurvival
					
					IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
						IF sEndOfMission.bHidePlayerCard != bHidePlayerCard
							sEndOfMission.bHidePlayerCard = bHidePlayerCard
							bResetButtons = TRUE
						ENDIF											
						IF bResetButtons
						OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, NOT FM_PLAY_LIST_HAVE_I_VOTED(), bPlayerCard AND (bHidePlayerCard = FALSE), bSpectate, bShowMove, bfacebook, FALSE, FALSE, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark, FALSE, FALSE, sEndOfMission.bBeingKickedFromSurvival)
						ENDIF
					ENDIF
					
					IF NOT FM_PLAY_LIST_HAVE_I_VOTED()
						IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() 
							IF bTimerExpired 
								CLEAR_REPLAY_JOB_HELP()
								FM_PLAY_LIST_SET_ME_AS_VOTED()
								//If we passed the intro mission
								IF sEndOfMission.bBeingKickedFromSurvival								
									PRINTLN("FMMC EOM - g_bPassedMyTutorialMission - TRUE")	
									SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 
								ELIF NOT g_bPassedMyTutorialMission
								AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
									PRINTLN("FMMC EOM - g_bPassedMyTutorialMission - FALSE")	
									
									//Set that we are in a corona after a restart/random
									SET_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
									SET_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
									SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_RESTART) 
								//We failed the intro mission
								ELSE
									PRINTLN("FMMC EOM - g_bPassedMyTutorialMission - TRUE")	
									SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 								
								ENDIF
								
								PRINTLN("FMMC EOM - //// - bTimerExpired")	
							ENDIF
							MAINTAIN_SELECTING_WORLD_LEADERBOARD(sEndOfMission, bSCLB, bResetButtons)
						ENDIF
						
						IF HAS_MISSION_END_CONTINUE_BEEN_PRESSED()
							CLEAR_REPLAY_JOB_HELP()
							FM_PLAY_LIST_SET_ME_AS_VOTED()
							//We failed the intro mission
							IF sEndOfMission.bBeingKickedFromSurvival							
								PRINTLN("FMMC EOM - g_bPassedMyTutorialMission - TRUE")	
								SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 
							//If we didn't pass the intro mission
							ELIF NOT g_bPassedMyTutorialMission
							AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
								PRINTLN("FMMC EOM - g_bPassedMyTutorialMission - FALSE")	
								SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_RESTART) 
							//We passe the race or mission
							ELSE							
								PRINTLN("FMMC EOM - g_bPassedMyTutorialMission - TRUE")	
								SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 
							ENDIF

							PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - HAS_MISSION_END_CONTINUE_BEEN_PRESSED (2)")						
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, FALSE, FALSE, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark, FALSE, FALSE, sEndOfMission.bBeingKickedFromSurvival)
							IF sEndOfMission.bBeingKickedFromSurvival
								CLEAR_REPLAY_JOB_HELP()
								PRINTLN("FMMC EOM - MAINTAIN_END_OF_MISSION_SCREEN RETURN TRUE, I PRESSED QUIT")
								//Clear that I left with everyone else because I pressed quit
								CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
								sEndOfMission.iQuitProgress = LBD_QUIT_INITIAL
								g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_INVALID			
								//Set the transition session vote status
								SET_TRANSITION_SESSION_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUIT)
								CALL_END_OF_MISSION_VOTE_TELEMETRY(sEndOfMission, sServerFMMC_EOM)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
				
					PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - 3 ")
				
					// check for showing SC LB
					MAINTAIN_SELECTING_WORLD_LEADERBOARD(sEndOfMission, bSCLB, bResetButtons)
					
					//Check vote the vote presses
					IF NOT FM_PLAY_LIST_HAVE_I_VOTED()
					AND NOT sEndOfMission.bBeingKickedFromSurvival
						PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - 3 - NOT FM_PLAY_LIST_HAVE_I_VOTED")
					
						//If we should show quit and we're on a heist then maintain it
						IF bShowQuit AND sEndOfMission.bIsHeist
							IF MAINTAIN_LEADERBOARD_QUIT_BUTTON(sEndOfMission)
								CALL_END_OF_MISSION_VOTE_TELEMETRY(sEndOfMission, sServerFMMC_EOM, TRUE)
								RETURN TRUE
							ENDIF
							
							MAINTAIN_LEADERBOARD_FREEMODE_BUTTON(sEndOfMission)
							
						ENDIF
						
						IF bTimerExpired 
							CLEAR_REPLAY_JOB_HELP()
							FM_PLAY_LIST_SET_ME_AS_VOTED()
							IF sEndOfMission.bIsHeist
							OR DOES_CURRENT_MISSION_USE_GANG_BOSS()
							OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_DefaultForceQuickRestart) 
								//If wuick restart is active
								IF (sEndOfMission.bShowQuickRestart
								//And there's enough players
								AND (NETWORK_GET_NUM_PARTICIPANTS() >= g_FMMC_STRUCT.iMinNumParticipants))
								#IF IS_DEBUG_BUILD
								OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceQuicRestart")
								#ENDIF
									#IF IS_DEBUG_BUILD
									IF GET_COMMANDLINE_PARAM_EXISTS("sc_disableAutoQuickRestart")
										SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 
									ELSE
									#ENDIF
										//Set me to restat
										SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART) 
									#IF IS_DEBUG_BUILD
									ENDIF
									#ENDIF
								ELSE
									//No, set me to continue									
									SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 
								ENDIF
								PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bTimerExpired//")						
								SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark)
							ELIF bMiniGame
							OR sEndOfMission.bIsContactMission
								SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 
								PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bTimerExpired//")						
								SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark)
							ELIF sEndOfMission.bOnlyDoQuit = FALSE
								SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 
								PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bTimerExpired///")						
								SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark)
							ENDIF
						ENDIF
						
						IF sEndOfMission.bOnlyDoQuit = FALSE
							PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - 3 - bOnlyDoQuit")
						
							//Reset up the help if the server says so							
							RESET_UP_HELP_IF_VOTE_STATUS_CHANGE(lbdVars, sEndOfMission, sServerFMMC_EOM, bIsJobVote, bAllowContinue, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, stEnd, iTimer, bQuitEarly)
							
							//If the server says we can vote
							IF sServerFMMC_EOM.bAllowedToVote
							AND sEndOfMission.bEnoughCash
								PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - 3 - bEnoughCash")
								IF NOT DONE_REPLAY_JOB_HELP()
									IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
									OR sEndOfMission.bBeingKickedFromSurvival
										IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DO_REPLAY_HELP)
											SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DO_REPLAY_HELP)
											PRINTLN("FMMC EOM - [dsw] Set BI_FM_NMH4_DO_REPLAY_HELP")
										ENDIF
									ENDIF
								ENDIF
								
								//Check for ready to continue
								IF NOT IS_WARNING_MESSAGE_ACTIVE()
									PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - 3 - IS_WARNING_MESSAGE_ACTIVE")
									IF SHOULD_EOM_CONTINUE_BE_DISPLAYED(sServerFMMC_EOM, bAllowContinue)
									AND HAS_MISSION_END_CONTINUE_BEEN_PRESSED()
									AND NOT sEndOfMission.bIsHeist // (See MAINTAIN_LEADERBOARD_FREEMODE_BUTTON)
										CLEAR_REPLAY_JOB_HELP()
										FM_PLAY_LIST_SET_ME_AS_VOTED()
										SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 
										PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - HAS_MISSION_END_CONTINUE_BEEN_PRESSED (3)")						
										SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark)
									ENDIF
									
//									//Check for random
//									IF SHOULD_EOM_RANDOM_BE_DISPLAYED(sEndOfMission, sServerFMMC_EOM, bIsJobVote)
//									AND HAS_MISSION_END_RANDOM_BEEN_PRESSED()
//									AND NOT bIsJobVote
//										CLEAR_REPLAY_JOB_HELP()
//										FM_PLAY_LIST_SET_ME_AS_VOTED()
//										SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_RANDOM) 
//										PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - HAS_MISSION_END_RANDOM_BEEN_PRESSED")						
//										SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark)
//										PRINTLN("FMMC EOM - sEndOfMission.bActiveSCLeaderboard = FALSE - HAS_MISSION_END_RANDOM_BEEN_PRESSED")						
//									ENDIF
								
									//Check for restart
//									IF SHOULD_EOM_RESTART_BE_DISPLAYED(sEndOfMission, sServerFMMC_EOM, bIsJobVote)
//									AND HAS_MISSION_END_RESTART_BUTTON_BEEN_PRESSED() 
//									AND NOT bIsJobVote
//										CLEAR_REPLAY_JOB_HELP()
//										FM_PLAY_LIST_SET_ME_AS_VOTED()
//										SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_RESTART) 
//										PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - HAS_MISSION_END_RESTART_BUTTON_BEEN_PRESSED")						
//										SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark)
//									ENDIF
									
									IF NOT sEndOfMission.bHideQuickRestart
									AND SHOULD_SHOW_QUICK_RESTART_OPTION()
										PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - 3 - SHOULD_SHOW_QUICK_RESTART_OPTION")
										//Check for quick restart
										MAINTAIN_MISSION_QUICK_RESTART(lbdVars, sEndOfMission, bMiniGame, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, stEnd, iTimer, sServerFMMC_EOM.bAllowedToVote)
										
										//Check for quick restart with a wee team swap
										MAINTAIN_MISSION_QUICK_RESTART_SWAP_TEAMS(lbdVars, sEndOfMission, bMiniGame, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, stEnd, iTimer, sServerFMMC_EOM.bAllowedToVote)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//Maintain the spectate stuff
						MAINTAIN_SETUP_FOR_SPECTATE(lbdVars, sEndOfMission, sServerFMMC_EOM, bIsJobVote, bAllowContinue, bResetButtons, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, stEnd, iTimer)
						
						
						PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - 4 ")
					ELSE
					
						IF bResetButtons
						OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
							IF sEndOfMission.iProgress = ciEND_OF_MISSION_VIEW_SCLB
								PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bResetButtons - only SCLB buttons")	
								SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, bShowQuit, TRUE, FALSE, TRUE,FALSE,TRUE,sEndOfMission.bActiveSCLeaderboard,iTimer, stEnd,FALSE,FALSE,sEndOfMission.bAddBookMark,sEndOfMission.bShowCoDriver,sEndOfMission.bRallyRace)
							ELSE
								IF sEndOfMission.bOnlyDoQuit
									IF NOT sEndOfMission.bActiveSCLeaderboard
									OR NOT bSCLB
										PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bResetButtons - sEndOfMission.bOnlyDoQuit - IF NOT sEndOfMission.bActiveSCLeaderboard")						
										SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, FALSE, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, sEndOfMission.bAddBookMark,sEndOfMission.bShowCoDriver,sEndOfMission.bRallyRace)
									ENDIF
								ELSE
									PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bResetButtons - sEndOfMission.bOnlyDoQuit = FALSE")						
									SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, SHOULD_EOM_RESTART_BE_DISPLAYED(sEndOfMission, sServerFMMC_EOM, bIsJobVote), 
															FALSE, 
															SHOULD_EOM_CONTINUE_BE_DISPLAYED(sServerFMMC_EOM, bAllowContinue), 
															TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer,stEnd, !FM_PLAY_LIST_HAVE_I_VOTED() AND sEndOfMission.bShowQuickRestart, !FM_PLAY_LIST_HAVE_I_VOTED() AND sEndOfMission.bShowQuickRestartSwap, sEndOfMission.bAddBookMark)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//If we are allowed to add this as a bookmark
					MAINTAIN_ADD_BOOKMARK_BUTTON(sEndOfMission, bResetButtons)
					
					PRINTLN("FMMC EOM - ciEND_OF_MISSION_DO_VOTE - 5 ")
					
				ENDIF
				
				//Maintain the setting of the vars if the vote passes
				IF MAINTAIN_SETTING_VARS_ON_VOTE_PASSING(sEndOfMission, sServerFMMC_EOM)		
					SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_DONE)
				ENDIF
			ENDIF
		BREAK
		
		//handle viewing LB 
		CASE ciEND_OF_MISSION_VIEW_SCLB
		
			PRINTLN("FMMC EOM - In ciEND_OF_MISSION_VIEW_SCLB sServerFMMC_EOM.bVotePassed = ", sServerFMMC_EOM.bVotePassed)
			
			IF sEndOfMission.bResetWithOutTimer = FALSE
				IF Timer != NULL
					IF iTimer < 1
						sEndOfMission.bResetWithOutTimer = TRUE
						bResetButtons = TRUE
						iTimer = -1
					ENDIF
				ENDIF
			ELSE
				iTimer = -1
			ENDIF
			
			//Draw the menu help
			GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
			DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, iTimer, TRUE, TRUE, TRUE, DEFAULT, DEFAULT)//, TRUE)
			
			// check for showing SC LB
			IF HAS_MISSION_END_BACK_BEEN_PRESSED()
				IF sEndOfMission.iSavedProgress = ciEND_OF_MISSION_RATE_JOB
					SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_CLEAR_BUTTONS)
				ELSE
					SET_END_OF_MISSION_STATUS(sEndOfMission, sEndOfMission.iSavedProgress)
				ENDIF
				PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_DEFAULT_SOUNDSET")
				CLEAR_REPLAY_JOB_HELP()
				bResetButtons = TRUE
				sEndOfMission.bActiveSCLeaderboard = !sEndOfMission.bActiveSCLeaderboard
			ENDIF
			
			IF sEndOfMission.bActiveSCLeaderboard
				IF HAS_MISSION_END_SHOW_SC_LB_CO_DRIVER_BEEN_PRESSED()
					bResetButtons = TRUE
					sEndOfMission.bShowCoDriver = !sEndOfMission.bShowCoDriver
				ENDIF
			ENDIF
			
			IF bResetButtons
			OR sEndOfMission.bRefresh
			OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
				IF sEndOfMission.bRefresh
					sEndOfMission.bRefresh = FALSE
				ENDIF
				IF sEndOfMission.iProgress = ciEND_OF_MISSION_VIEW_SCLB
					PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bResetButtons - only SCLB buttons")	
					SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, TRUE,FALSE,TRUE,sEndOfMission.bActiveSCLeaderboard,iTimer, stEnd,FALSE,FALSE,FALSE,sEndOfMission.bShowCoDriver,sEndOfMission.bRallyRace)
				ELSE
					IF sEndOfMission.bOnlyDoQuit
						IF NOT sEndOfMission.bActiveSCLeaderboard
						OR NOT bSCLB
							PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bResetButtons - sEndOfMission.bOnlyDoQuit - IF NOT sEndOfMission.bActiveSCLeaderboard")						
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, FALSE, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, FALSE,sEndOfMission.bShowCoDriver,sEndOfMission.bRallyRace)
						ENDIF
					ELSE
						IF sEndOfMission.iSavedProgress = ciEND_OF_MISSION_RATE_JOB
							sEndOfMission.bRefresh = TRUE
							//SET_UP_MISSION_END_HELP(FALSE, FALSE, FALSE, FALSE, bPlayerCard, bSpectate, bShowMove, bfacebook, TRUE, FALSE, iTimer, stEnd, FALSE, FALSE, TRUE, FALSE, FALSE, sEndOfMission.bBeingKickedFromSurvival, TRUE)
						ELSE
							PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bResetButtons - sEndOfMission.bOnlyDoQuit = FALSE")						
							SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars, SHOULD_EOM_RESTART_BE_DISPLAYED(sEndOfMission, sServerFMMC_EOM, bIsJobVote), 
													FALSE, 
													SHOULD_EOM_CONTINUE_BE_DISPLAYED(sServerFMMC_EOM, bAllowContinue), TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer,stEnd, !FM_PLAY_LIST_HAVE_I_VOTED() AND sEndOfMission.bShowQuickRestart, !FM_PLAY_LIST_HAVE_I_VOTED() AND sEndOfMission.bShowQuickRestartSwap, FALSE)
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			IF MAINTAIN_SETTING_VARS_ON_VOTE_PASSING(sEndOfMission, sServerFMMC_EOM)	
				SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_DONE)
			ENDIF
			
			IF bTimerExpired
			//Check vote the vote presses
			AND NOT FM_PLAY_LIST_HAVE_I_VOTED()	
				// If the timer has expired we want to move on from the Social Club leaderboard
				SET_ME_AS_READY_FOR_JOB_VOTES()
				
				
				//Chris's mess!
				IF bIsJobVote
				AND NOT sEndOfMission.bBeingKickedFromSurvival
				AND NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
					// Time out pass vote
						SET_ME_AS_READY_FOR_JOB_VOTES()
					//Not Chris's mess
				ELSE
					CLEAR_REPLAY_JOB_HELP()
					FM_PLAY_LIST_SET_ME_AS_VOTED()
					IF NOT FM_PLAY_LIST_HAVE_I_VOTED()
					AND IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() 
						IF NOT g_bPassedMyTutorialMission
						AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
							PRINTLN("FMMC EOM - g_bPassedMyTutorialMission - FALSE")	
							SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_RESTART) 
						//We failed the intro mission
						ELSE
							PRINTLN("FMMC EOM - g_bPassedMyTutorialMission - TRUE")	
							SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 								
						ENDIF					
					ELIF bMiniGame
					OR sEndOfMission.bIsHeist 
					OR sEndOfMission.bIsContactMission
						SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 
						PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bTimerExpired, A")						
						SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars,FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, FALSE)
					ELIF sEndOfMission.bOnlyDoQuit = FALSE
						SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE) 
						PRINTLN("FMMC EOM - SET_UP_MISSION_END_HELP - bTimerExpired, B")						
						SET_UP_MISSION_END_HELP(sEndOfMission, lbdVars,FALSE, FALSE, FALSE, TRUE, bPlayerCard, bSpectate, bShowMove, bfacebook, bSCLB, sEndOfMission.bActiveSCLeaderboard, iTimer, stEnd, FALSE, FALSE, FALSE)
					ENDIF
				ENDIF
			ENDIF			
		BREAK
		
		CASE ciEND_OF_MISSION_CLEAR_BUTTONS
			IF NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				SET_END_OF_MISSION_STATUS(sEndOfMission, ciEND_OF_MISSION_RATE_JOB)
			ENDIF
		BREAK
		
		CASE ciEND_OF_MISSION_HORDE
			IF MAINTAIN_SETTING_VARS_ON_VOTE_PASSING(sEndOfMission, sServerFMMC_EOM)
			OR DID_PLAYER_QUIT_JOB()
			OR sEndOfMission.bBeingKickedFromSurvival
			OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
			OR IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WAS_LAST_DEACTIVATED_BY_QUIT_SCREEN)
				CALL_END_OF_MISSION_VOTE_TELEMETRY(sEndOfMission, sServerFMMC_EOM)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciEND_OF_MISSION_DONE
			RETURN TRUE
		BREAK
				
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_MINI_GAME_END_OF_MISSION_SCREEN(FMMC_EOM_DETAILS &sEndOfMission, SERVER_EOM_VARS &sServerFMMC_EOM, BOOL bTimerExpired 	= TRUE, TIME_DATATYPE Timer = NULL, INT iOverTimeOut	= 0, STRING stEnd = NULL)
	RETURN MAINTAIN_END_OF_MISSION_SCREEN(sEndOfMission, sServerFMMC_EOM, bTimerExpired, FALSE, FALSE, FALSE, FALSE, FALSE, Timer, iOverTimeOut, stEnd, TRUE)
ENDFUNC
//This resets the player count at the start of the frame
PROC FMMC_EOM_SERVER_RESET_PLAYER_COUNTS(PLAYER_CURRENT_STATUS_COUNTS &sPlayerCounts)
	PLAYER_CURRENT_STATUS_COUNTS sPlayerCountsBlank
	sPlayerCounts = sPlayerCountsBlank
ENDPROC

//This needs to be called from with in the player loop. 
//itempPart is the participant index
PROC FMMC_EOM_SERVER_PLAYER_PROCESSING(PLAYER_CURRENT_STATUS_COUNTS &sPlayerCounts, INT itempPart)

	PARTICIPANT_INDEX temppart
	INT iRank
	temppart = INT_TO_PARTICIPANTINDEX(itempPart)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
		PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
		IF NOT IS_PLAYER_ON_A_PLAYLIST(playerId)
		AND NOT IS_PLAYER_SCTV(playerId)
//			IF NOT IS_PLAYER_SCTV(playerId)
//			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
			
				INT iPlayerSlot = NATIVE_TO_INT(playerId)
				iRank = GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(playerId))
				IF sPlayerCounts.iPlayerRank < iRank
					sPlayerCounts.iPlayerRank = iRank
				ENDIF
				sPlayerCounts.iNumberOnMission++
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_PlaylistAtEndOfMission)
					sPlayerCounts.iNumberAtEndOfMission++
					IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_Voted)
						sPlayerCounts.iNumberVoted++
						SWITCH GlobalplayerBD_FM[iPlayerSlot].iEOM_voteStatus
							CASE ciFMMC_EOM_VOTE_STATUS_CONTINUE			
								sPlayerCounts.iNumberContinue++
							BREAK
							CASE ciFMMC_EOM_VOTE_STATUS_RANDOM			
								sPlayerCounts.iNumberRandom++
							BREAK
							CASE ciFMMC_EOM_VOTE_STATUS_RESTART			
								sPlayerCounts.iNumberRestart++
							BREAK
							CASE ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART		
								sPlayerCounts.iNumberQuickRestart++
							BREAK
							CASE ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP
								sPlayerCounts.iNumberQuickRestartSwap++
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_IM_GOING_TO_JOB_VOTES)
					sPlayerCounts.iNumberJobVotes++
				ENDIF
//			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
//Pint out changes to the counts of players
PROC PRINT_PLAYER_COUNT_CHANGES_FORDEBUG(PLAYER_CURRENT_STATUS_COUNTS &sPlayerCounts, SERVER_EOM_VARS &sServerFMMC_EOM)
	IF sServerFMMC_EOM.sDebugCounts.iNumberOnMission != sPlayerCounts.iNumberOnMission
		sServerFMMC_EOM.sDebugCounts.iNumberOnMission = sPlayerCounts.iNumberOnMission
		PRINTLN("FMMC EOM - sServerFMMC_EOM.sDebugCounts.iNumberOnMission = ", sServerFMMC_EOM.sDebugCounts.iNumberOnMission)
	ENDIF
	IF sServerFMMC_EOM.sDebugCounts.iNumberAtEndOfMission != sPlayerCounts.iNumberAtEndOfMission
		sServerFMMC_EOM.sDebugCounts.iNumberAtEndOfMission = sPlayerCounts.iNumberAtEndOfMission
		PRINTLN("FMMC EOM - sServerFMMC_EOM.sDebugCounts.iNumberAtEndOfMission = ", sServerFMMC_EOM.sDebugCounts.iNumberAtEndOfMission)
	ENDIF
	IF sServerFMMC_EOM.sDebugCounts.iNumberVoted != sPlayerCounts.iNumberVoted
		sServerFMMC_EOM.sDebugCounts.iNumberVoted = sPlayerCounts.iNumberVoted
		PRINTLN("FMMC EOM - sServerFMMC_EOM.sDebugCounts.iNumberVoted = ", sServerFMMC_EOM.sDebugCounts.iNumberVoted)
	ENDIF
	IF sServerFMMC_EOM.sDebugCounts.iNumberContinue != sPlayerCounts.iNumberContinue
		sServerFMMC_EOM.sDebugCounts.iNumberContinue = sPlayerCounts.iNumberContinue
		PRINTLN("FMMC EOM - sServerFMMC_EOM.sDebugCounts.iNumberContinue = ", sServerFMMC_EOM.sDebugCounts.iNumberContinue)
	ENDIF
	IF sServerFMMC_EOM.sDebugCounts.iNumberRestart != sPlayerCounts.iNumberRestart
		sServerFMMC_EOM.sDebugCounts.iNumberRestart = sPlayerCounts.iNumberRestart
		PRINTLN("FMMC EOM - sServerFMMC_EOM.sDebugCounts.iNumberRestart = ", sServerFMMC_EOM.sDebugCounts.iNumberRestart)
	ENDIF
	IF sServerFMMC_EOM.sDebugCounts.iNumberRandom != sPlayerCounts.iNumberRandom
		sServerFMMC_EOM.sDebugCounts.iNumberRandom = sPlayerCounts.iNumberRandom
		PRINTLN("FMMC EOM - sServerFMMC_EOM.sDebugCounts.iNumberRandom = ", sServerFMMC_EOM.sDebugCounts.iNumberRandom)
	ENDIF
	IF sServerFMMC_EOM.sDebugCounts.iNumberQuickRestart != sPlayerCounts.iNumberQuickRestart
		sServerFMMC_EOM.sDebugCounts.iNumberQuickRestart = sPlayerCounts.iNumberQuickRestart
		PRINTLN("FMMC EOM - sServerFMMC_EOM.sDebugCounts.iNumberQuickRestart = ", sServerFMMC_EOM.sDebugCounts.iNumberQuickRestart)
	ENDIF
	IF sServerFMMC_EOM.sDebugCounts.iNumberQuickRestartSwap != sPlayerCounts.iNumberQuickRestartSwap
		sServerFMMC_EOM.sDebugCounts.iNumberQuickRestartSwap = sPlayerCounts.iNumberQuickRestartSwap
		PRINTLN("FMMC EOM - sServerFMMC_EOM.sDebugCounts.iNumberQuickRestartSwap = ", sServerFMMC_EOM.sDebugCounts.iNumberQuickRestartSwap)
	ENDIF
	IF sServerFMMC_EOM.sDebugCounts.iNumberJobVotes != sPlayerCounts.iNumberJobVotes
		sServerFMMC_EOM.sDebugCounts.iNumberJobVotes = sPlayerCounts.iNumberJobVotes
		PRINTLN("FMMC EOM - sServerFMMC_EOM.sDebugCounts.iNumberJobVotes = ", sServerFMMC_EOM.sDebugCounts.iNumberJobVotes)
	ENDIF
ENDPROC
#ENDIF


//This is called after the player loop, or after the staggered has finished.
PROC FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING(PLAYER_CURRENT_STATUS_COUNTS &sPlayerCounts, SERVER_EOM_VARS &sServerFMMC_EOM, INT iMissionType, INT iPreviousVariation = -1, FLOAT fPassRatio = 0.5, BOOL bNeedsToBeTeam = FALSE)

	//If the vote has passed then get out of here. 
	IF sServerFMMC_EOM.bVotePassed
		EXIT
	ENDIF
	
	IF sPlayerCounts.iNumberOnMission = 0
		EXIT
	ENDIF
	
	FLOAT fPassRatioQuickRestart = fPassRatio
	
	//If they are two people then 100% of people need to vote
	IF sPlayerCounts.iNumberOnMission = 2
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
	OR IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		fPassRatio = 1.0 //setting pass ratio to 1 
	ENDIF
	
	//Set the rank to select!
	IF sServerFMMC_EOM.iMaxPlayerRank < sPlayerCounts.iPlayerRank
		sServerFMMC_EOM.iMaxPlayerRank = sPlayerCounts.iPlayerRank
		PRINTLN("FMMC EOM - FORCING THROUGH VOTE - sServerFMMC_EOM.iMaxPlayerRank = ", sServerFMMC_EOM.iMaxPlayerRank )
	ENDIF
	
	//If everybody is at the end of the mission
	IF sPlayerCounts.iNumberOnMission = sPlayerCounts.iNumberAtEndOfMission
	
		//Tell people if the server if throwing the vote
		#IF IS_DEBUG_BUILD
		IF sServerFMMC_EOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_INVALID
			PRINTLN("FMMC EOM - FORCING THROUGH VOTE - sServerFMMC_EOM.iVoteStatus = ", GET_EMO_STATUS_FOR_DEBUG_PRINT(sServerFMMC_EOM.iVoteStatus))
		ENDIF
		PRINT_PLAYER_COUNT_CHANGES_FORDEBUG(sPlayerCounts, sServerFMMC_EOM)
		#ENDIF
		
		// Speirs Job votes	
		//Vote has passed for Quick Restart
		IF (TO_FLOAT(sPlayerCounts.iNumberQuickRestart)/TO_FLOAT(sPlayerCounts.iNumberOnMission))>= fPassRatioQuickRestart
		OR sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
			PRINTLN("FMMC EOM - FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING - VOTE PASSED FOR QUICK RESTART")
			sServerFMMC_EOM.bVotePassed = TRUE
			sServerFMMC_EOM.vWarpPos 	= g_vFMMC_MissionStartPos
			sServerFMMC_EOM.contentID	= ""	// Added by Keith for 'Random' 2/7/13 - wasn't used for 'Quick Restart' so NULLifying to prevent confusion
			sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART			
			PRINTLN("FMMC EOM - sServerFMMC_EOM.vWarpPos  = ", sServerFMMC_EOM.vWarpPos)
			PRINTLN("FMMC EOM - sServerFMMC_EOM.contentID = ", sServerFMMC_EOM.contentID)
		//Vote has passed for Quick Restart Swap Teams 
		ELIF (TO_FLOAT(sPlayerCounts.iNumberQuickRestartSwap)/TO_FLOAT(sPlayerCounts.iNumberOnMission))>= fPassRatioQuickRestart
		OR sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP
			PRINTLN("FMMC EOM - FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING - VOTE PASSED FOR QUICK RESTART RESTART")
			sServerFMMC_EOM.bVotePassed = TRUE
			sServerFMMC_EOM.vWarpPos 	= g_vFMMC_MissionStartPos
			sServerFMMC_EOM.contentID	= ""	// Added by Keith for 'Random' 2/7/13 - wasn't used for 'Quick Restart Swap Teams' so NULLifying to prevent confusion
			sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP			
			PRINTLN("FMMC EOM - sServerFMMC_EOM.vWarpPos = ", sServerFMMC_EOM.vWarpPos)	
			PRINTLN("FMMC EOM - sServerFMMC_EOM.contentID = ", sServerFMMC_EOM.contentID)
		ELIF (TO_FLOAT(sPlayerCounts.iNumberJobVotes)/TO_FLOAT(sPlayerCounts.iNumberOnMission))>= fPassRatio
			PRINTLN("FMMC EOM - FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING - sServerFMMC_EOM.bJobVotesYes = TRUE ")
			sServerFMMC_EOM.bVotePassed = TRUE
			sServerFMMC_EOM.bJobVotesYes = TRUE		
		ELSE
			//Check to see if the vote has passed
			//Vote has passed for Continue
			IF (TO_FLOAT(sPlayerCounts.iNumberContinue)/TO_FLOAT(sPlayerCounts.iNumberOnMission))>= fPassRatio
				PRINTLN("FMMC EOM - FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING - VOTE PASSED FOR CONTINUE")
				sServerFMMC_EOM.bVotePassed = TRUE
				sServerFMMC_EOM.vWarpPos 	= <<0.0, 0.0, 0.0>>
				sServerFMMC_EOM.contentID	= ""	// Added by Keith for 'Random' 2/7/13 - wasn't used for 'Continue' so NULLifying to prevent confusion
				sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_CONTINUE			
				PRINTLN("FMMC EOM - sServerFMMC_EOM.vWarpPos  = ", sServerFMMC_EOM.vWarpPos)
				PRINTLN("FMMC EOM - sServerFMMC_EOM.contentID = ", sServerFMMC_EOM.contentID)
			//Vote has passed for RANDOM
			ELIF (TO_FLOAT(sPlayerCounts.iNumberRandom)/TO_FLOAT(sPlayerCounts.iNumberOnMission))>= fPassRatio
			OR sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_RANDOM
				PRINTLN("FMMC EOM - FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING - VOTE PASSED FOR RANDOM")
				INT iReturnArrayPos
				BOOL bRockstarCreated
				IF GET_NEXT_FM_MISSION_TO_LOAD(iMissionType, iReturnArrayPos, bRockstarCreated, iPreviousVariation, sPlayerCounts.iNumberOnMission, bNeedsToBeTeam, sServerFMMC_EOM.iMaxPlayerRank)
					IF bRockstarCreated
						sServerFMMC_EOM.vWarpPos 	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iReturnArrayPos].vStartPos
						sServerFMMC_EOM.contentID	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iReturnArrayPos].tlName	// Added by Keith 2/7/13
					ELSE
						sServerFMMC_EOM.vWarpPos 	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iReturnArrayPos].vStartPos
						sServerFMMC_EOM.contentID	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iReturnArrayPos].tlName	// Added by Keith 2/7/13
					ENDIF
					sServerFMMC_EOM.bVotePassed = TRUE
					sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_RANDOM
					PRINTLN("FMMC EOM - sServerFMMC_EOM.vWarpPos  = ", sServerFMMC_EOM.vWarpPos)
					PRINTLN("FMMC EOM - sServerFMMC_EOM.contentID = ", sServerFMMC_EOM.contentID)
				ENDIF
				
			//Vote has passed for RESTART
			ELIF (TO_FLOAT(sPlayerCounts.iNumberRestart)/TO_FLOAT(sPlayerCounts.iNumberOnMission))>= fPassRatio
			OR sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_RESTART
				PRINTLN("FMMC EOM - FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING - VOTE PASSED FOR RESTART")
				sServerFMMC_EOM.bVotePassed = TRUE
				sServerFMMC_EOM.vWarpPos 	= g_vFMMC_MissionStartPos
				sServerFMMC_EOM.contentID	= ""	// Added by Keith for 'Random' 2/7/13 - wasn't used for 'Restart' so NULLifying to prevent confusion
				sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_RESTART			
				PRINTLN("FMMC EOM - sServerFMMC_EOM.vWarpPos = ", sServerFMMC_EOM.vWarpPos)
				PRINTLN("FMMC EOM - sServerFMMC_EOM.contentID = ", sServerFMMC_EOM.contentID)
			ENDIF
		ENDIF
		
		//If the vote hasn't passed
		IF sServerFMMC_EOM.bVotePassed = FALSE
			//If the number of people voted is everybody on the mission then we need to swing the vote based on the hosts choice
			IF sPlayerCounts.iNumberVoted = sPlayerCounts.iNumberOnMission
				PRINTLN("FMMC EOM - sPlayerCounts.iNumberVoted = sPlayerCounts.iNumberOnMission")
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iEOM_voteStatus = ciFMMC_EOM_VOTE_STATUS_INVALID
					IF Is_Player_Currently_On_MP_Heist(PLAYER_ID()) 
					OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
					OR DOES_CURRENT_MISSION_USE_GANG_BOSS()
						GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iEOM_voteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
						PRINTLN("FMMC EOM - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iEOM_voteStatus = ciFMMC_EOM_VOTE_STATUS_INVALID - setting to ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART")
					ELSE
						GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iEOM_voteStatus = ciFMMC_EOM_VOTE_STATUS_CONTINUE
						PRINTLN("FMMC EOM - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iEOM_voteStatus = ciFMMC_EOM_VOTE_STATUS_INVALID - setting to ciFMMC_EOM_VOTE_STATUS_CONTINUE")
					ENDIF
				ENDIF
				sServerFMMC_EOM.iVoteStatus = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iEOM_voteStatus
				PRINTLN("FMMC EOM - sServerFMMC_EOM.iVoteStatus = ", GET_EMO_STATUS_FOR_DEBUG_PRINT(sServerFMMC_EOM.iVoteStatus))
				sServerFMMC_EOM.vWarpPos 	= g_vFMMC_MissionStartPos
				PRINTLN("FMMC EOM - sServerFMMC_EOM.vWarpPos  = ", sServerFMMC_EOM.vWarpPos)
				sServerFMMC_EOM.bVotePassed = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// SHOULD_BYPASS_LIKE_DISLIKE_LOGIC
