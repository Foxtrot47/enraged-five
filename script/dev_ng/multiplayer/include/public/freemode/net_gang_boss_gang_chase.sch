
//-- Dave W - 'Gang Chase' functionality similar to mission controller which can be used in freemode scripts

//----------------------
//	INCLUDES
//----------------------
USING "globals.sch"
USING "net_prints.sch"
USING "net_gang_boss_common.sch"
USING "net_include.sch"
USING "net_blips.sch"

//----------------------
//	CONSTANTS
//----------------------
CONST_INT MAX_PEDS_PER_GANG_CHASE_UNIT					4
CONST_INT MAX_GANG_CHASE								5
CONST_INT MAX_GANG_CHASE_PER_TARGET						5
CONST_INT MAX_GANG_CHASE_PER_WAVE						5
CONST_FLOAT MAX_GANG_CHASE_CLEANUP_DIST2				122500.0 //350m

CONST_INT GANG_CHASE_UNIT_CREATED			0

CONST_INT GANG_CHASE_STATE_CREATED				0
CONST_INT GANG_CHASE_STATE_CHASE_PLAYER			1
CONST_INT GANG_CHASE_STATE_FLEE					2
CONST_INT GANG_CHASE_STATE_CHASE_AI_PED				3
CONST_INT GANG_CHASE_STATE_ATTACK_COORD			4
CONST_INT GANG_CHASE_STATE_CHASE_PLAYER_IN_HELI	5

CONST_INT GANG_CHASE_SERVER_INIT												0
CONST_INT GANG_CHASE_SERVER_ALLOW_GANG_CHASE									1
CONST_INT GANG_CHASE_SERVER_CREATED_ALL_LAND_UNITS_FOR_CURRENT_WAVE				2
CONST_INT GANG_CHASE_SERVER_CREATED_ALL_SEA_UNITS_FOR_CURRENT_WAVE				3
CONST_INT GANG_CHASE_SERVER_CURRENT_WAVE_CLEARED								4


CONST_INT GANG_CHASE_CLIENT_INIT					0
CONST_INT GANG_CHASE_CLIENT_VALID_CHASE_TARGET		1
CONST_INT GANG_CHASE_CLIENT_VALID_CHASE_TARGET_SEA	2

CONST_INT GANG_CHASE_TYPE_LAND						0
CONST_INT GANG_CHASE_TYPE_SEA						1
CONST_INT GANG_CHASE_TYPE_HELI						2
CONST_INT GANG_CHASE_TYPE_MAX						3

CONST_INT GANG_CHASE_OPTIONS_DONT_JACK_PLAYER		0
CONST_INT GANG_CHASE_OPTIONS_ALWAYS_CHASE_PLAYER	1
CONST_INT GANG_CHASE_OPTIONS_DONT_LEAVE_VEHICLE		2

//----------------------
//	STRUCT
//----------------------
STRUCT STRUCT_GANG_CHASE_OPTIONS
	INT iTotalNumberOfWaves							// The total number of gang chase 'waves' to spawn
	INT iTimeBetweenWaves							// The time delay, in ms, between the previous gang chase wave cleaning up, and the new one spawning
	INT iNumPedsInEachVeh							// The number of peds in each gang chase vehicle.
	INT iNumVehInEachWave							// The number of vehicles in each wave. Each vehicle will spawn with iNumPedsInEachVeh inside
	INT iPedAccuracy								// The accuracy of the gang chase peds
	INT iDifficulty									// The mission difficulty. Used to set the ped accuracy etc. See GB_BIKER_CLUBHOUSE_MISSION_DIFFICULTY_ CONSTs
	 
	VECTOR vDropffCoord								// World coordinate which the gang chase will flee from if they get within fFleeDistance. Won't flee if left as the origin
	FLOAT fFleeDistance								// The distance from vDropffCoord that the gang chase will begin to flee (200m by default).
	
	VECTOR vCoordTarget[MAX_GANG_CHASE_PER_WAVE]	// A fixed location to attack, rather than a player / ped
	VECTOR vOverrideDirectionVector					// The ideal ddirection from the fixed location that the gang chase should come from.
	MODEL_NAMES mGangChaseVeh						// The Vehicle model to use for each gang chase unit (Land)
	MODEL_NAMES mGangChaseVehSea					// The Vehicle model to use for each gang chase unit (Sea)
	MODEL_NAMES mGangChasePed						// The ped model to use for each gang chase ped created inside the vehicle
	NETWORK_INDEX niPedToAttack						// Gang chase will chase the ped with this network index if specified, rather than a player
	NETWORK_INDEX niVehToAttack						// Gang chase will chase and sapwn near this specific veh, rather than a player
	
	REL_GROUP_HASH relGangChase						// The relationship group of the gang chase. Will default to rgFM_AiHate if not specified
	
	MODEL_NAMES mGangChaseVehCustom[MAX_GANG_CHASE_PER_WAVE]
	MODEL_NAMES mGangChasePedCustom[MAX_PEDS_PER_GANG_CHASE_UNIT]
	
	INT iNumPedsInEachVehCustom[MAX_GANG_CHASE_PER_WAVE]
	
	INT iGangChaseOptionsBitset
	
	WEAPON_TYPE weaponGangChasePed
	BOOL bDriversUsePistols = FALSE
	
	BOOL bSetVehicleColours[MAX_GANG_CHASE_PER_WAVE]
	BOOL bSetVehicleExtraColours[MAX_GANG_CHASE_PER_WAVE]
	BOOL bSetVehicleExtraColour5[MAX_GANG_CHASE_PER_WAVE]
	BOOL bSetVehicleExtraColour6[MAX_GANG_CHASE_PER_WAVE]
	
	INT iVehicleColour1[MAX_GANG_CHASE_PER_WAVE]
	INT iVehicleColour2[MAX_GANG_CHASE_PER_WAVE]
	INT iVehicleColour3[MAX_GANG_CHASE_PER_WAVE]
	INT iVehicleColour4[MAX_GANG_CHASE_PER_WAVE]
	INT iVehicleColour5[MAX_GANG_CHASE_PER_WAVE]
	INT iVehicleColour6[MAX_GANG_CHASE_PER_WAVE]
	
	BOOL bLockVehicleDoors = FALSE
	
ENDSTRUCT



STRUCT STRUCT_GANG_CHASE_UNIT
	NETWORK_INDEX niGCPed[MAX_PEDS_PER_GANG_CHASE_UNIT]
	NETWORK_INDEX niGCVeh
	INT iGangChaseState[MAX_PEDS_PER_GANG_CHASE_UNIT]
	INT iGangChaseUnitBitset
	INT iGangChaseUnitType
	INT iTarget
	VECTOR vTargetCoord
	AI_BLIP_STRUCT gangChasePedBlipData[MAX_PEDS_PER_GANG_CHASE_UNIT]
	SCRIPT_TIMER timeCreation	
	
ENDSTRUCT

STRUCT STRUCT_GANG_CHASE_SERVER
	STRUCT_GANG_CHASE_UNIT structGangChaseUnit[MAX_GANG_CHASE_PER_WAVE]
	INT iNumberOfUnitsCreated
	INT iNumberOfWavesCreated[GANG_CHASE_TYPE_MAX]
	INT iGangChaseTargetPart = -1
	INT iNextGangChaseIndex = -1
	INT iGangChaseFleeBitset
	INT iServerGangChaseBitset
	INT iGangChaseType
	INT iDifficulty = -1
	INT iNumberOfWavesCleared
	NETWORK_INDEX niTargetPed
	NETWORK_INDEX niTargetVeh
	VECTOR vOverrideDirectionVector
//	VECTOR vCoordTarget
	VECTOR vDropOffToAvoid
	FLOAT fFleeDistance
	VECTOR vLastSpawnCoords
	INT iGangChaseOptionBitset
	SCRIPT_TIMER timeGangChaseWaveCleanedUp[GANG_CHASE_TYPE_MAX]
	
//	STRUCT_GANG_CHASE_OPTIONS options
	MODEL_NAMES mCustomVeh
	MODEL_NAMES mGangChaseVeh
	#IF IS_DEBUG_BUILD
	BOOL bWdToggleOnScreenDebug
	#ENDIF
ENDSTRUCT



STRUCT STRUCT_GANG_CHASE_LOCAL
	VECTOR vTempGangChaseSpawnCoords
	VECTOR vGangChaseSpawnDir
	VECTOR vSpawnDirection
	INT iGangChaseSpawnCoordRotCount
	INT iSpawnAttempts
	SCRIPT_TIMER timeHeliGoto[MAX_GANG_CHASE]
	SCRIPT_TIMER timeHeliAttack[MAX_GANG_CHASE]
	#IF IS_DEBUG_BUILD
	BOOL bWdToggleOnScreenDebug
	#ENDIF
ENDSTRUCT

//----------------------
//	FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD
FUNC TEXT_LABEL_31 GET_GANG_CHASE_STATE_NAME(INT iState)
	TEXT_LABEL_31 tl31State = "Invalid!"
	SWITCH iState
		CASE GANG_CHASE_STATE_CREATED				tl31State =  "GANG_CHASE_STATE_CREATED"			BREAK
		CASE GANG_CHASE_STATE_CHASE_PLAYER			tl31State =  "GANG_CHASE_STATE_CHASE_PLAYER"	BREAK
		CASE GANG_CHASE_STATE_FLEE					tl31State = "GANG_CHASE_STATE_FLEE"				BREAK
		CASE GANG_CHASE_STATE_CHASE_AI_PED				tl31State = "GANG_CHASE_STATE_CHASE_AI_PED"			BREAK
		CASE GANG_CHASE_STATE_ATTACK_COORD			tl31State = "GANG_CHASE_STATE_ATTACK_COORD"		BREAK
		CASE GANG_CHASE_STATE_CHASE_PLAYER_IN_HELI	tl31State = "GANG_CHASE_STATE_CH_PL_IN_HELI"		BREAK
	ENDSWITCH
	
	RETURN tl31State
ENDFUNC

PROC DISPLAY_GB_GANG_CHASE_STRING_AND_NUMBER(FLOAT fX, FLOAT &fY, STRING sDisplay, INT iDisplay)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
	
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_SCALE(0.0000, 0.5)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fX, fY, "STRING", sDisplay)
	
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_SCALE(0.0000, 0.5)
	DISPLAY_TEXT_WITH_NUMBER((fX + 0.4), fY, "NUMBER", iDisplay)	
	fY += 0.05
ENDPROC

FUNC TEXT_LABEL_63 GET_GANG_CHASE_OPTION_NAME(INT iOption)
	TEXT_LABEL_63 tl63Option = "Invalid!"
	SWITCH iOption
		CASE GANG_CHASE_OPTIONS_DONT_JACK_PLAYER	tl63Option = "GANG_CHASE_OPTIONS_DONT_JACK_PLAYER"	BREAK
		CASE GANG_CHASE_OPTIONS_ALWAYS_CHASE_PLAYER	tl63Option = "GANG_CHASE_OPTIONS_ALWAYS_CHASE_PLAYER" BREAK
		CASE GANG_CHASE_OPTIONS_DONT_LEAVE_VEHICLE tl63Option = "GANG_CHASE_OPTIONS_DONT_LEAVE_VEHICLE" BREAK
	ENDSWITCH
	
	RETURN tl63Option 
ENDFUNC
#ENDIF

FUNC INT GET_TOTAL_NUMBER_OF_GANG_CHASE_WAVES(STRUCT_GANG_CHASE_OPTIONS &gangChaseOptions)
	RETURN gangChaseOptions.iTotalNumberOfWaves
ENDFUNC

FUNC INT GET_MAX_NUMBER_OF_GANG_CHASE_VEHS_PER_WAVE(STRUCT_GANG_CHASE_OPTIONS &gangChaseOptions)
	RETURN gangChaseOptions.iNumVehInEachWave
ENDFUNC
FUNC INT GET_TIME_BETWEEN_GANG_CHASE_WAVES_FOR_VARIATION(STRUCT_GANG_CHASE_OPTIONS &gangChaseOptions)
	RETURN gangChaseOptions.iTimeBetweenWaves
ENDFUNC

FUNC VECTOR GET_DROP_OFF_COORDS_TO_AVOID(STRUCT_GANG_CHASE_OPTIONS &gangChaseOptions)
	RETURN gangChaseOptions.vDropffCoord
ENDFUNC

FUNC FLOAT GET_FLEE_DISTANCE(STRUCT_GANG_CHASE_OPTIONS &gangChaseOptions)
	RETURN gangChaseOptions.fFleeDistance
ENDFUNC

FUNC NETWORK_INDEX GET_GANG_CHASE_VEHICLE_NI(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase)
	RETURN serverGangChaseData.structGangChaseUnit[iGangChase].niGCVeh
ENDFUNC

FUNC VEHICLE_INDEX GET_GANG_CHASE_VEHICLE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase)
	RETURN NET_TO_VEH(GET_GANG_CHASE_VEHICLE_NI(serverGangChaseData, iGangChase))
ENDFUNC
FUNC NETWORK_INDEX GET_GANG_CHASE_PED_NI(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase, INT iPed)
	RETURN serverGangChaseData.structGangChaseUnit[iGangChase].niGCPed[iPed]
ENDFUNC

FUNC PED_INDEX GET_GANG_CHASE_PED(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase, INT iPed)
	PED_INDEX ped
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase, iPed))
		ped = NET_TO_PED(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase, iPed))
	ENDIF
	
	RETURN ped
ENDFUNC

FUNC BOOL GANG_CHASE_IS_THIS_PED_A_GANG_CHASE_PED(PED_INDEX pedID, STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
	INT iGangChaseLoop
	INT iPedLoop
	
	REPEAT MAX_GANG_CHASE_PER_WAVE iGangChaseLoop
		REPEAT MAX_PEDS_PER_GANG_CHASE_UNIT iPedLoop
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.structGangChaseUnit[iGangChaseLoop].niGCPed[iPedLoop])
				IF NET_TO_PED(serverGangChaseData.structGangChaseUnit[iGangChaseLoop].niGCPed[iPedLoop]) = pedID
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_GANG_CHASE_STATE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase, INT iPed)
	RETURN serverGangChaseData.structGangChaseUnit[iGangChase].iGangChaseState[iPed]
ENDFUNC

PROC SET_GANG_CHASE_PED_STATE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase, INT iPed, INT iState)
	#IF IS_DEBUG_BUILD
	INT iCurrentSate = GET_GANG_CHASE_STATE(serverGangChaseData, iGangChase, iPed)
	TEXT_LABEL_31 tl31OldState = GET_GANG_CHASE_STATE_NAME(iCurrentSate)
	TEXT_LABEL_31 tl31NewState = GET_GANG_CHASE_STATE_NAME(iState)
	PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [SET_GANG_CHASE_PED_STATE] Gang Chase ", iGangChase, " Ped ", iPed, " ", tl31OldState, " -> ", tl31NewState) 
	#ENDIF
	
	serverGangChaseData.structGangChaseUnit[iGangChase].iGangChaseState[iPed] = iState
ENDPROC

PROC SET_GANG_CHASE_OPTION(STRUCT_GANG_CHASE_OPTIONS &options, INT iOption)
	IF NOT IS_BIT_SET(options.iGangChaseOptionsBitset, iOption)
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tl63OptionName = GET_GANG_CHASE_OPTION_NAME(iOption)
		PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [SET_GANG_CHASE_OPTION] Option: ", tl63OptionName)
		#ENDIF
		SET_BIT(options.iGangChaseOptionsBitset, iOption)
	ENDIF
ENDPROC

PROC CLEAR_GANG_CHASE_OPTION(STRUCT_GANG_CHASE_OPTIONS &options, INT iOption)
	IF NOT IS_BIT_SET(options.iGangChaseOptionsBitset, iOption)
		#IF IS_DEBUG_BUILD
		PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [SET_GANG_CHASE_OPTION] Option: ", GET_GANG_CHASE_OPTION_NAME(iOption))
		#ENDIF
		SET_BIT(options.iGangChaseOptionsBitset, iOption)
	ENDIF
ENDPROC

FUNC BOOL IS_GANG_CHASE_OPTION_SET(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iOption)
	RETURN IS_BIT_SET(serverGangChaseData.iGangChaseOptionBitset, iOption)
ENDFUNC

PROC SET_GANG_CHASE_WONT_JACK_PLAYERS(STRUCT_GANG_CHASE_OPTIONS &options)
	SET_GANG_CHASE_OPTION(options, GANG_CHASE_OPTIONS_DONT_JACK_PLAYER)
ENDPROC

PROC SET_GANG_CHASE_ALWAYS_CHASE_PLAYER(STRUCT_GANG_CHASE_OPTIONS &options)
	SET_GANG_CHASE_OPTION(options, GANG_CHASE_OPTIONS_ALWAYS_CHASE_PLAYER)
ENDPROC

PROC SET_GANG_CHASE_DONT_LEAVE_VEHICLE(STRUCT_GANG_CHASE_OPTIONS &options)
	SET_GANG_CHASE_OPTION(options, GANG_CHASE_OPTIONS_DONT_LEAVE_VEHICLE)
ENDPROC

PROC RESET_GANG_CHASE_SERVER(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase)
	IF IS_BIT_SET(serverGangChaseData.structGangChaseUnit[iGangChase].iGangChaseUnitBitset, GANG_CHASE_UNIT_CREATED)
		
		
		serverGangChaseData.structGangChaseUnit[iGangChase].iGangChaseUnitBitset = 0
		serverGangChaseData.structGangChaseUnit[iGangChase].iTarget = -1
		serverGangChaseData.structGangChaseUnit[iGangChase].iGangChaseState[0] = GANG_CHASE_STATE_CREATED
		serverGangChaseData.structGangChaseUnit[iGangChase].iGangChaseState[1] = GANG_CHASE_STATE_CREATED
		serverGangChaseData.structGangChaseUnit[iGangChase].iGangChaseState[2] = GANG_CHASE_STATE_CREATED
		serverGangChaseData.structGangChaseUnit[iGangChase].iGangChaseState[3] = GANG_CHASE_STATE_CREATED
		
		PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [RESET_GANG_CHASE_SERVER] resetting gang chase ", iGangChase)
		RESET_NET_TIMER(serverGangChaseData.structGangChaseUnit[iGangChase].timeCreation)
		
	ENDIF
ENDPROC

FUNC BOOL SHOULD_GANG_CHASE_UNIT_FLEE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase)
	RETURN IS_BIT_SET(serverGangChaseData.iGangChaseFleeBitset, iGangChase)
ENDFUNC

PROC PROCESS_GANG_CHASE_BRAIN(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase)
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.structGangChaseUnit[iGangChase].niGCVeh)
		IF IS_BIT_SET(serverGangChaseData.structGangChaseUnit[iGangChase].iGangChaseUnitBitset, GANG_CHASE_UNIT_CREATED)
			RESET_GANG_CHASE_SERVER(serverGangChaseData, iGangChase)
		ENDIF
		
		EXIT
	ENDIF
	
	INT i
	INT iState
	
	PED_INDEX ped
	VEHICLE_INDEX veh
	REPEAT MAX_PEDS_PER_GANG_CHASE_UNIT i
		iState = GET_GANG_CHASE_STATE(serverGangChaseData, iGangChase, i)
		SWITCH iState
			CASE GANG_CHASE_STATE_CREATED
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase, i))
					ped = GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)
					veh = GET_GANG_CHASE_VEHICLE(serverGangChaseData, iGangChase)
					IF ARE_VECTORS_EQUAL(serverGangChaseData.structGangChaseUnit[iGangChase].vTargetCoord, << 0.0, 0.0, 0.0 >>)
						IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetPed)
							IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetVeh)
								PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BRAIN] Gang Chase ", iGangChase, " ped ", i, " told to chase player vector zero")
								IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(veh))
									IF GET_PED_IN_VEHICLE_SEAT(veh) = ped
										SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_CHASE_PLAYER_IN_HELI)
									ELSE
										SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_CHASE_PLAYER)
									ENDIF
								ELSE
									SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_CHASE_PLAYER)
								ENDIF
							ELSE
								SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_CHASE_PLAYER)
							ENDIF
						ELSE
							SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_CHASE_AI_PED)
						ENDIF
					ELSE
						//-- Attack a fixed coord
						IF i = 0
							//-- Driver
							PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BRAIN] Gang Chase ", iGangChase, " ped ", i, " told to attack coord as driver")
							SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_ATTACK_COORD)
						ELSE
							PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BRAIN] Gang Chase ", iGangChase, " ped ", i, " told to chase player as not driver")
							SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_CHASE_PLAYER)
						ENDIF
						
					ENDIF
				ENDIF
			BREAK
			
			CASE GANG_CHASE_STATE_ATTACK_COORD
				ped = GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)
				VECTOR vPos
				IF NOT IS_PED_INJURED(ped)
					IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(ped)
						SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_CHASE_PLAYER)
						PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BRAIN] Gang Chase ", iGangChase, " ped ", i, " told to attack as not in a vehicle")
					ELSE
						vPos = GET_ENTITY_COORDS(ped)
						IF NOT ARE_VECTORS_EQUAL(serverGangChaseData.structGangChaseUnit[iGangChase].vTargetCoord, << 0.0, 0.0, 0.0 >>)
							IF VDIST2(vPos, serverGangChaseData.structGangChaseUnit[iGangChase].vTargetCoord) < 8.0 * 8.0
								PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BRAIN] Gang Chase ", iGangChase, " ped ", i, " told to attack as near attack coords ")
								SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_CHASE_PLAYER)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			
			CASE GANG_CHASE_STATE_CHASE_PLAYER
				IF SHOULD_GANG_CHASE_UNIT_FLEE(serverGangChaseData, iGangChase)
					SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_FLEE)
				ENDIF
			BREAK
			
			CASE GANG_CHASE_STATE_CHASE_AI_PED
				IF SHOULD_GANG_CHASE_UNIT_FLEE(serverGangChaseData, iGangChase)
					SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_FLEE)
				ENDIF
			BREAK
			
			CASE GANG_CHASE_STATE_CHASE_PLAYER_IN_HELI
				IF SHOULD_GANG_CHASE_UNIT_FLEE(serverGangChaseData, iGangChase)
					SET_GANG_CHASE_PED_STATE(serverGangChaseData, iGangChase, i, GANG_CHASE_STATE_FLEE)
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

PROC CLEANUP_GANG_CHASE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase)
	PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [CLEANUP_GANG_CHASE] Called with iGangChase = ", iGangChase)
	
	NETWORK_INDEX ni
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GANG_CHASE_VEHICLE_NI(serverGangChaseData,iGangChase))
		ni = GET_GANG_CHASE_VEHICLE_NI(serverGangChaseData, iGangChase)
		CLEANUP_NET_ID(ni)
	ENDIF
	
	INT i
	REPEAT MAX_PEDS_PER_GANG_CHASE_UNIT i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase, i))
			ni = GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase, i)
			CLEANUP_NET_ID(ni)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_PART_GANG_CHASE_IS_CHASING(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase)
	RETURN serverGangChaseData.structGangChaseUnit[iGangChase].iTarget
ENDFUNC

FUNC BOOL ARE_ALL_GANG_CHASE_UNIT_PEDS_INJURED(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase)
	INT i
	REPEAT MAX_PEDS_PER_GANG_CHASE_UNIT i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase, i))
			IF NOT IS_PED_INJURED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase, i))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_CLEANUP_CHECKS_FOR_GANG_CHASE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase)
	INT iPartChasing = GET_PART_GANG_CHASE_IS_CHASING(serverGangChaseData, iGangChase)
	VECTOR vTargetCoords
	VECTOR vGangChaseCoords
	VECTOR vDropOffCoords
	IF iPartChasing = PARTICIPANT_ID_TO_INT()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GANG_CHASE_VEHICLE_NI(serverGangChaseData, iGangChase))
				IF ARE_ALL_GANG_CHASE_UNIT_PEDS_INJURED(serverGangChaseData, iGangChase)
					PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_CLEANUP_CHECKS_FOR_GANG_CHASE] Cleaning up gang chase unit ", iGangChase, " as ARE_ALL_GANG_CHASE_UNIT_PEDS_INJURED")
					CLEANUP_GANG_CHASE(serverGangChaseData, iGangChase)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(GET_GANG_CHASE_VEHICLE(serverGangChaseData, iGangChase))
						IF HAS_NET_TIMER_EXPIRED(serverGangChaseData.structGangChaseUnit[iGangChase].timeCreation, 2000)
							PRINTLN("[BIKER1]     ----------> [MAINTAIN_CLEANUP_CHECKS_FOR_GANG_CHASE] Gang chase ",iGangChase, " veh drivable")
							
							IF ARE_VECTORS_EQUAL(serverGangChaseData.structGangChaseUnit[iGangChase].vTargetCoord, << 0.0, 0.0, 0.0 >>)
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetPed)
									vTargetCoords = GET_ENTITY_COORDS(NET_TO_PED(serverGangChaseData.niTargetPed), FALSE)
								ELIF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetVeh)
									vTargetCoords = GET_ENTITY_COORDS(NET_TO_PED(serverGangChaseData.niTargetVeh), FALSE)
								ELSE
									vTargetCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
								ENDIF
							ELSE
								//-- Target is a fixed coord
								vTargetCoords = serverGangChaseData.structGangChaseUnit[iGangChase].vTargetCoord
							ENDIF
							
							vGangChaseCoords = GET_ENTITY_COORDS(GET_GANG_CHASE_VEHICLE(serverGangChaseData, iGangChase))
							FLOAT fDist2 = VDIST2(vTargetCoords, vGangChaseCoords)
							IF fDist2 > MAX_GANG_CHASE_CLEANUP_DIST2
								PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]  [MAINTAIN_CLEANUP_CHECKS_FOR_GANG_CHASE] Cleaning up gang chase unit ", iGangChase, " as Dist squared = ", fDist2)
								CLEANUP_GANG_CHASE(serverGangChaseData, iGangChase)
							ELSE
								//-- flee drop off
								IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.gangChaseUnitClient.iUnitFleeBitset, iGangChase)
									vDropOffCoords = serverGangChaseData.vDropOffToAvoid
									IF NOT IS_VECTOR_ZERO(vDropOffCoords)
										fDist2 = GET_DISTANCE_BETWEEN_COORDS(vDropOffCoords, vGangChaseCoords)
										IF fDist2 < serverGangChaseData.fFleeDistance
											SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.gangChaseUnitClient.iUnitFleeBitset, iGangChase)
											PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]  [MAINTAIN_CLEANUP_CHECKS_FOR_GANG_CHASE] Set gang chase unit ", iGangChase, " should flee drop off at coords ", vDropOffCoords, " as fdist2 = ", fDist2, " < ", serverGangChaseData.fFleeDistance)
										ENDIF
									ENDIF

								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_CLEANUP_CHECKS_FOR_GANG_CHASE] Cleaning up gang cahse unit ", iGangChase, " as veh not drivable")
						CLEANUP_GANG_CHASE(serverGangChaseData, iGangChase)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_BLIP_GANG_CHASE_PED(PED_INDEX ped)
	REL_GROUP_HASH relPlayer
	REL_GROUP_HASH relGangPed
	RELATIONSHIP_TYPE relType
	
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(ped)
			relPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())

			relGangPed = GET_PED_RELATIONSHIP_GROUP_HASH(ped)
			
			relType = GET_RELATIONSHIP_BETWEEN_GROUPS(relGangPed, relPlayer)
			IF relType != ACQUAINTANCE_TYPE_PED_LIKE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HOST_TAKE_CONTROL_OF_GANG_CHASE_NETWORK_ID(NETWORK_INDEX niEntity)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niEntity)
		RETURN FALSE
	ENDIF
	
	IF TAKE_CONTROL_OF_NET_ID(niEntity)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_OWNERSHIP_OF_GANG_CHASE_NETWORK_ID(NETWORK_INDEX niEntity)

	IF IS_NETWORK_ID_OWNED_BY_PARTICIPANT(niEntity)
		RETURN TRUE
	ENDIF
	
	IF HOST_TAKE_CONTROL_OF_GANG_CHASE_NETWORK_ID(niEntity)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
PROC PROCESS_GANG_CHASE_BODY(STRUCT_GANG_CHASE_LOCAL &gangChaseLocal, STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase)
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.structGangChaseUnit[iGangChase].niGCVeh)
		EXIT
	ENDIF
	
	UNUSED_PARAMETER(gangChaseLocal)
	
	#IF IS_DEBUG_BUILD
	MODEL_NAMES mVehBlips
	VEHICLE_INDEX vehBlips
	PED_INDEX pedBlips
	TEXT_LABEL_63 tl63VehName, tl63StateNameBlips
		
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_GangChaseBlips") 
		PRINTLN("[BIKER1]     ---------->   [sc_GangChaseBlips] ")
		PRINTLN("[BIKER1]     ---------->   [sc_GangChaseBlips] ****************************************") 
		PRINTLN("[BIKER1]     ---------->   [sc_GangChaseBlips] PROCESS_GANG_CHASE_BODY Called for GC unit ", iGangChase) 
	ENDIF
	#ENDIF
	
	
	INT i
	INT iState
	SCRIPTTASKSTATUS status
	
	VECTOR vFleeCoords
//	VECTOR vTargetCoords
	BOOL bForceBlip
	BOOL bShouldBlip
	//BOOL bFleeing
	PED_INDEX ped
//	PED_INDEX pedTargetPlayer
	VEHICLE_INDEX veh
//	PARTICIPANT_INDEX partChasing
//	PLAYER_INDEX playerChasing
	//VEHICLE_INDEX veh
	REPEAT MAX_PEDS_PER_GANG_CHASE_UNIT i
		iState = GET_GANG_CHASE_STATE(serverGangChaseData, iGangChase, i)
		SWITCH iState
			CASE GANG_CHASE_STATE_CREATED
			BREAK
			
			CASE GANG_CHASE_STATE_CHASE_PLAYER
				MAINTAIN_CLEANUP_CHECKS_FOR_GANG_CHASE(serverGangChaseData, iGangChase)
				
				IF NOT IS_PED_INJURED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i))
					status = GET_SCRIPT_TASK_STATUS(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
					IF status != PERFORMING_TASK
					AND status != WAITING_TO_START_TASK
						IF MAINTAIN_OWNERSHIP_OF_GANG_CHASE_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase,i))
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase,i))
					//			IF SET_CURRENT_PED_VEHICLE_WEAPON(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), FALSE)
									TASK_COMBAT_HATED_TARGETS_IN_AREA(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), GET_ENTITY_COORDS(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)), 299.9)
									PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] I have given gang chase ", iGangChase, " ped ", i, " TASK_COMBAT_HATED_TARGETS_IN_AREA")
						//		ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				bShouldBlip = SHOULD_BLIP_GANG_CHASE_PED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)) // TRUE
				IF bShouldBlip
					bForceBlip = TRUE
				ENDIF
			BREAK
			
			CASE GANG_CHASE_STATE_CHASE_AI_PED
				MAINTAIN_CLEANUP_CHECKS_FOR_GANG_CHASE(serverGangChaseData, iGangChase)
				
				IF NOT IS_PED_INJURED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i))
					
					status = GET_SCRIPT_TASK_STATUS(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), SCRIPT_TASK_COMBAT)
					IF status != PERFORMING_TASK
					AND status != WAITING_TO_START_TASK
						IF MAINTAIN_OWNERSHIP_OF_GANG_CHASE_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase,i))
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase,i))
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetPed)
									ped = GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)
									
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, FALSE)
									TASK_COMBAT_PED(ped, NET_TO_PED(serverGangChaseData.niTargetPed))
								//	TASK_VEHICLE_MISSION_PED_TARGET(ped, veh, NET_TO_PED(serverGangChaseData.niTargetPed),MISSION_ATTACK, 30.0, DRIVINGMODE_AVOIDCARS,1.0, 1.0)
									PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] I have given gang chase ", iGangChase, " ped ", i, " TASK_COMBAT_PED")

								ELSE
									PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] GANG_CHASE_STATE_CHASE_AI_PED Target NI doesn't exist!")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				
				bShouldBlip = SHOULD_BLIP_GANG_CHASE_PED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)) // TRUE
				IF bShouldBlip
					bForceBlip = TRUE
				ENDIF
			BREAK
			
			CASE GANG_CHASE_STATE_ATTACK_COORD
				MAINTAIN_CLEANUP_CHECKS_FOR_GANG_CHASE(serverGangChaseData, iGangChase)
				
				IF NOT IS_PED_INJURED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i))
					status = GET_SCRIPT_TASK_STATUS(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), SCRIPT_TASK_VEHICLE_MISSION)
					IF status != PERFORMING_TASK
					AND status != WAITING_TO_START_TASK
						IF MAINTAIN_OWNERSHIP_OF_GANG_CHASE_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase,i))
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase,i))
								IF NOT ARE_VECTORS_EQUAL(serverGangChaseData.structGangChaseUnit[iGangChase].vTargetCoord, << 0.0, 0.0, 0.0 >>)
									ped = GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)
									IF IS_PED_SITTING_IN_ANY_VEHICLE(ped)
										veh = GET_VEHICLE_PED_IS_IN(ped)
										TASK_VEHICLE_MISSION_COORS_TARGET(ped, 
																			veh,
																			serverGangChaseData.structGangChaseUnit[iGangChase].vTargetCoord,
																			MISSION_GOTO,
																			25.0,
																			DRIVINGMODE_AVOIDCARS,
																			3.0,
																			10.0)
										PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] I have given gang chase ", iGangChase, " ped ", i, " TASK_VEHICLE_MISSION_COORS_TARGET to coords ", serverGangChaseData.structGangChaseUnit[iGangChase].vTargetCoord)
									ELSE
										PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] Want to give gang chase ", iGangChase, " ped ", i, " TASK_VEHICLE_MISSION_COORS_TARGET but not in a vehicle!")
									ENDIF
								ELSE
									PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] Want to give gang chase ", iGangChase, " ped ", i, " TASK_VEHICLE_MISSION_COORS_TARGET but coords ", serverGangChaseData.structGangChaseUnit[iGangChase].vTargetCoord)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				bShouldBlip = SHOULD_BLIP_GANG_CHASE_PED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)) // TRUE
				
				IF bShouldBlip
					bForceBlip = TRUE
				ENDIF
			BREAK
			
			CASE GANG_CHASE_STATE_CHASE_PLAYER_IN_HELI
				ped 		= GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)
				veh 		= GET_GANG_CHASE_VEHICLE(serverGangChaseData, iGangChase)
//				partChasing = INT_TO_PARTICIPANTINDEX(GET_PART_GANG_CHASE_IS_CHASING(serverGangChaseData, iGangChase))
//				INT iMinHeight
//				IF NOT IS_PED_INJURED(ped)
//					IF IS_VEHICLE_DRIVEABLE(veh)
//						IF IS_PED_SITTING_IN_VEHICLE(ped, veh)
//							IF NOT HAS_NET_TIMER_STARTED(gangChaseLocal.timeHeliGoto[iGangChase]) 
//							OR HAS_NET_TIMER_EXPIRED(gangChaseLocal.timeHeliGoto[iGangChase], 10)
//								IF NETWORK_IS_PARTICIPANT_ACTIVE(partChasing)
//									IF MAINTAIN_OWNERSHIP_OF_GANG_CHASE_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase,i))
//										IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase,i))
//											IF SET_CURRENT_PED_VEHICLE_WEAPON(ped, WEAPONTYPE_VEHICLE_SPACE_ROCKET)
//												START_NET_TIMER(gangChaseLocal.timeHeliGoto[iGangChase])
//												playerChasing = NETWORK_GET_PLAYER_INDEX(partChasing)
//												pedTargetPlayer = GET_PLAYER_PED(playerChasing)
//												IF NOT IS_PED_INJURED(pedTargetPlayer)
//												
//													IF HAS_NET_TIMER_EXPIRED(gangChaseLocal.timeHeliAttack[iGangChase], 2500)
//														VECTOR vHeliOffset
//														vHeliOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh, GET_ENTITY_COORDS(pedTargetPlayer))
//														
//														IF vHeliOffset.y >= 10
//															INT iRocketRNG
//															iRocketRNG = GET_RANDOM_INT_IN_RANGE(0, 100)
//															
//															IF iRocketRNG > 60
//																SET_VEHICLE_SHOOT_AT_TARGET(ped, pedTargetPlayer, <<0,0,0>>)
//																CPRINTLN(DEBUG_NET_MAGNATE, "=== GB_FRAGILE_GOODS === [MAINTAIN_AI_BODY] - PED_AI_STATE_ATTACK_IN_HELI - Firing on target at driver.")
//		                                                    ELSE
//																VECTOR vOffset
//																vOffset = <<0.0, 0.0, 0.0>>
//																vOffset.x = GET_RANDOM_FLOAT_IN_RANGE(5, 10)
//			                                                    vOffset.y = GET_RANDOM_FLOAT_IN_RANGE(5, 10)
//			                                                    vOffset.z = 0.0 //GET_RANDOM_FLOAT_IN_RANGE(5, 10)
//																
//			                                                    IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
//																	vOffset.x *= -1
//			                                                    ENDIF
//			                                                    IF GET_RANDOM_INT_IN_RANGE(0,3) = 0
//			                                                        vOffset.y *= -1
//			                                                    ENDIF
//															
//		                                                        SET_VEHICLE_SHOOT_AT_TARGET(ped, NULL, GET_ENTITY_COORDS(pedTargetPlayer) + vOffset)
//																PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] I have given gang chase ", iGangChase, " ped ", i, " SET_VEHICLE_SHOOT_AT_TARGET, chasing player ", GET_PLAYER_NAME(playerChasing))
//		                                                    ENDIF
//		                                                ENDIF
//														RESET_NET_TIMER(gangChaseLocal.timeHeliAttack[iGangChase])
//														START_NET_TIMER(gangChaseLocal.timeHeliAttack[iGangChase])
//													ENDIF
//												
//													vTargetCoords = GET_ENTITY_COORDS(pedTargetPlayer)
//													iMinHeight = (ROUND(vTargetCoords.z) + 10)
//													TASK_HELI_MISSION(ped, veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedTargetPlayer, <<10.0*iGangChase, -30.0, 30.0>>) , MISSION_GOTO, 70.0, 0.1, GET_ENTITY_HEADING(pedTargetPlayer), iMinHeight, 20, -1)
//													RESET_NET_TIMER(gangChaseLocal.timeHeliGoto[iGangChase])
//											//		PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] I have given gang chase ", iGangChase, " ped ", i, " TASK_HELI_MISSION, chasing player ", GET_PLAYER_NAME(playerChasing))
//												ENDIF
//											ELSE
//												PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] WAiting for SET_CURRENT_PED_VEHICLE_WEAPON")
//											ENDIF
//										ENDIF
//									ENDIF
//								ELSE
//									PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] GANG_CHASE_STATE_CHASE_PLAYER_IN_HELI Part not active!")
//								ENDIF 
//							ENDIF
//						ENDIF
//					ENDIF
							
//				ENDIF

				IF NOT IS_PED_INJURED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i))
					status = GET_SCRIPT_TASK_STATUS(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
					IF status != PERFORMING_TASK
					AND status != WAITING_TO_START_TASK
						IF MAINTAIN_OWNERSHIP_OF_GANG_CHASE_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, -iGangChase,i))
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase,i))
								IF SET_CURRENT_PED_VEHICLE_WEAPON(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), FALSE)
									SET_PED_COMBAT_ATTRIBUTES(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), CA_USE_VEHICLE_ATTACK,TRUE)
									SET_PED_COMBAT_ATTRIBUTES(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,TRUE)
							
									SET_PED_FIRING_PATTERN(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), FIRING_PATTERN_BURST_FIRE_HELI)
									
									TASK_COMBAT_HATED_TARGETS_IN_AREA(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), GET_ENTITY_COORDS(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)), 299.9)
									PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] I have given gang chase ", iGangChase, " ped ", i, " TASK_COMBAT_HATED_TARGETS_IN_AREA in heli")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				MAINTAIN_CLEANUP_CHECKS_FOR_GANG_CHASE(serverGangChaseData, iGangChase)
				
				bShouldBlip = SHOULD_BLIP_GANG_CHASE_PED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)) // TRUE
				
				IF bShouldBlip
					bForceBlip = TRUE
				ENDIF
			BREAK
			
			CASE GANG_CHASE_STATE_FLEE
				IF NOT IS_PED_INJURED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i))
					status = GET_SCRIPT_TASK_STATUS(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), SCRIPT_TASK_SMART_FLEE_POINT)
					IF status != PERFORMING_TASK
					AND status != WAITING_TO_START_TASK
						IF MAINTAIN_OWNERSHIP_OF_GANG_CHASE_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase,i))
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase,i))
								vFleeCoords = serverGangChaseData.vDropOffToAvoid
								IF NOT ARE_VECTORS_EQUAL(vFleeCoords, << 0.0, 0.0, 0.0 >>)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), TRUE)
									SET_PED_COMBAT_ATTRIBUTES(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), CA_ALWAYS_FIGHT, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), CA_ALWAYS_FLEE, TRUE)
									SET_PED_FLEE_ATTRIBUTES(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), FA_DISABLE_COWER,TRUE)
									SET_PED_FLEE_ATTRIBUTES(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i),  FA_COWER_INSTEAD_OF_FLEE,FALSE)
									TASK_SMART_FLEE_COORD(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i), vFleeCoords, 10000.0, 999999)
									PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] Gang chase unit ", iGangChase, " - Ped ", i, " tasked with TASK_SMART_FLEE_COORD (ePEDSTATE_FLEE_DROP_OFF) from coord ", vFleeCoords)
								ELSE
									PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [PROCESS_GANG_CHASE_BODY] Gang chase unit ", iGangChase, "- Ped ", i, " can't flee because flee coords are zero!")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			//	bForceBlip = SHOULD_FORCE_BLIP_BLIP_GANG_CHASE_PED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)) // TRUE
				bShouldBlip = SHOULD_BLIP_GANG_CHASE_PED(GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)) 
				bForceBlip = FALSE
			//	bFleeing = TRUE
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_GangChaseBlips")
			tl63StateNameBlips = GET_GANG_CHASE_STATE_NAME(iState)
			pedBlips = GET_GANG_CHASE_PED(serverGangChaseData, iGangChase,i)
			PRINTLN("[BIKER1]     ---------->   [sc_GangChaseBlips] Checking ped... ", i)
			IF IS_PED_INJURED(pedBlips)
				PRINTLN("[BIKER1]     ---------->   [sc_GangChaseBlips] 	Injured!")
			ELSE
				IF IS_PED_SITTING_IN_ANY_VEHICLE(pedBlips)
					vehBlips = GET_VEHICLE_PED_IS_IN(pedBlips)
					mVehBlips = GET_ENTITY_MODEL(vehBlips)
					tl63VehName = GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mVehBlips)
					PRINTLN("[BIKER1]     ---------->   [sc_GangChaseBlips]			Veh Model 	= ", tl63VehName)
				ENDIF
				PRINTLN("[BIKER1]     ---------->   [sc_GangChaseBlips]			State 		= ", tl63StateNameBlips)	
				PRINTLN("[BIKER1]     ---------->   [sc_GangChaseBlips]			bShouldBlip	= ", bShouldBlip)
				PRINTLN("[BIKER1]     ---------->   [sc_GangChaseBlips]			bForceBlip	= ", bForceBlip)
			ENDIF
			PRINTLN("[BIKER1]     ---------->   [sc_GangChaseBlips] ")
		ENDIF
		#ENDIF
		
		
		
		
		IF bShouldBlip
			UPDATE_ENEMY_NET_PED_BLIP(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase, i), serverGangChaseData.structGangChaseUnit[iGangChase].gangChasePedBlipData[i], DEFAULT, DEFAULT, bForceBlip, FALSE)
			IF NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
				IF DOES_BLIP_EXIST(serverGangChaseData.structGangChaseUnit[iGangChase].gangChasePedBlipData[i].BlipID)
					SET_BLIP_AS_SHORT_RANGE(serverGangChaseData.structGangChaseUnit[iGangChase].gangChasePedBlipData[i].BlipID, TRUE)
				ENDIF
				IF DOES_BLIP_EXIST(serverGangChaseData.structGangChaseUnit[iGangChase].gangChasePedBlipData[i].VehicleBlipID)
					SET_BLIP_AS_SHORT_RANGE(serverGangChaseData.structGangChaseUnit[iGangChase].gangChasePedBlipData[i].VehicleBlipID, TRUE)
				ENDIF
			ENDIF
		ELSE
//			IF bFleeing
//				UPDATE_ENEMY_NET_PED_BLIP(GET_GANG_CHASE_PED_NI(serverGangChaseData, iGangChase, i), serverGangChaseData.structGangChaseUnit[iGangChase].gangChasePedBlipData[i], DEFAULT, DEFAULT, FALSE, FALSE)
//			ENDIF
		ENDIF
	ENDREPEAT
	
	
	
ENDPROC

PROC ADD_ROTATION_FOR_SPAWN_CHECK(STRUCT_GANG_CHASE_LOCAL &gangChaseLocal)
	
	gangChaseLocal.iGangChaseSpawnCoordRotCount++
		
	IF gangChaseLocal.iGangChaseSpawnCoordRotCount < 5 
		//0 = 0 degrees in front, 1 = +10, 2 = +20, 3 = -10, 4 = -20
		
		SWITCH gangChaseLocal.iGangChaseSpawnCoordRotCount
			CASE 1
			CASE 2
				gangChaseLocal.vSpawnDirection = gangChaseLocal.vSpawnDirection + <<0.0,0.0,10.0>>
			BREAK
			
			CASE 3
				gangChaseLocal.vSpawnDirection = gangChaseLocal.vSpawnDirection - <<0.0,0.0,30.0>>
			BREAK
			
			CASE 4
				gangChaseLocal.vSpawnDirection = gangChaseLocal.vSpawnDirection - <<0.0,0.0,10.0>>
			BREAK
		ENDSWITCH
		
		IF gangChaseLocal.vSpawnDirection.z > 360.0
			gangChaseLocal.vSpawnDirection.z = (gangChaseLocal.vSpawnDirection.z - 360.0)
		ENDIF
	ELSE
		gangChaseLocal.iGangChaseSpawnCoordRotCount = 0
		gangChaseLocal.vSpawnDirection = gangChaseLocal.vSpawnDirection + <<0.0,0.0,20.0>> // Back to 0 degrees in front
	ENDIF
	
	gangChaseLocal.iSpawnAttempts++
ENDPROC

FUNC INT GET_NUMBER_OF_GANG_CHASE_WAVES_CREATED(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iType = 0)
	RETURN serverGangChaseData.iNumberOfWavesCreated[iType]
ENDFUNC

FUNC INT GET_NUMBER_OF_ACTIVE_GANG_CHASE_UNITS_OF_TYPE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iType)
	INT i
	INT iCount
	
	REPEAT MAX_GANG_CHASE_PER_WAVE i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GANG_CHASE_VEHICLE_NI(serverGangChaseData, i))
			IF serverGangChaseData.structGangChaseUnit[i].iGangChaseUnitType = iType
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iCount
	
ENDFUNC

FUNC INT GET_NUMBER_OF_GANG_CHASE_CHASING_PART(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iPart)
	INT i
	INT iCount
	REPEAT MAX_GANG_CHASE_PER_WAVE i
		IF serverGangChaseData.structGangChaseUnit[i].iTarget = iPart
			iCount++
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

FUNC INT GET_NEXT_FREE_GANG_CHASE_INDEX(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
	INT i
	INT iFree = -1
	REPEAT MAX_GANG_CHASE_PER_WAVE i
		IF iFree = -1
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.structGangChaseUnit[i].niGCVeh)
				IF NOT IS_BIT_SET(serverGangChaseData.structGangChaseUnit[i].iGangChaseUnitBitset, GANG_CHASE_UNIT_CREATED)
					iFree = i
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iFree
ENDFUNC

FUNC BOOL IS_GANG_CHASE_CLIENT_BIT_SET(INT iPart, INT iBitToCheck)
	IF iPart >= 0
		PLAYER_INDEX player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
		RETURN IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(player)].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, iBitToCheck)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_GANG_CHASE_CLIENT_BIT(INT iBit)
	SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, iBit)
ENDPROC

FUNC BOOL IS_GANG_CHASE_SERVER_BIT_SET(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iBitToCheck)
	RETURN IS_BIT_SET(serverGangChaseData.iServerGangChaseBitset, iBitToCheck)
ENDFUNC

PROC SET_GANG_CHASE_SERVER_BIT(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iBit)
	IF NOT IS_BIT_SET(serverGangChaseData.iServerGangChaseBitset, iBit)
		SET_BIT(serverGangChaseData.iServerGangChaseBitset, iBit)
	ENDIF
ENDPROC

PROC CLEAR_GANG_CHASE_SERVER_BIT(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iBit)
	IF IS_BIT_SET(serverGangChaseData.iServerGangChaseBitset, iBit)
		CLEAR_BIT(serverGangChaseData.iServerGangChaseBitset, iBit)
	ENDIF
ENDPROC

PROC SET_GANG_CHASE_ALLOWED_TO_TRIGGER_SERVER(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
	IF NOT IS_BIT_SET(serverGangChaseData.iServerGangChaseBitset, GANG_CHASE_SERVER_ALLOW_GANG_CHASE)
		SET_BIT(serverGangChaseData.iServerGangChaseBitset, GANG_CHASE_SERVER_ALLOW_GANG_CHASE)
		#IF IS_DEBUG_BUILD
		PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [SET_GANG_CHASE_ALLOWED_TO_TRIGGER_SERVER] Set GANG_CHASE_SERVER_ALLOW_GANG_CHASE Callstack...")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
ENDPROC

PROC CLEAR_GANG_CHASE_ALLOWED_TO_TRIGGER_SERVER(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
	IF IS_BIT_SET(serverGangChaseData.iServerGangChaseBitset, GANG_CHASE_SERVER_ALLOW_GANG_CHASE)
		CLEAR_BIT(serverGangChaseData.iServerGangChaseBitset, GANG_CHASE_SERVER_ALLOW_GANG_CHASE)
		#IF IS_DEBUG_BUILD
		PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [CLEAR_GANG_CHASE_ALLOWED_TO_TRIGGER_SERVER] Clear GANG_CHASE_SERVER_ALLOW_GANG_CHASE Callstack...")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_GANG_CHASE_ALLOWED_TO_TRIGGER(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
	RETURN (IS_GANG_CHASE_SERVER_BIT_SET(serverGangChaseData, GANG_CHASE_SERVER_ALLOW_GANG_CHASE))
ENDFUNC

PROC SET_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET(BOOL bAllowLand = TRUE, BOOL bAllowSea = FALSE)
	IF bAllowLand
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, GANG_CHASE_CLIENT_VALID_CHASE_TARGET)
			SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, GANG_CHASE_CLIENT_VALID_CHASE_TARGET)
			#IF IS_DEBUG_BUILD
			PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [SET_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET Set I'm a valid target for gang chase - land. Callstack...")
			DEBUG_PRINTCALLSTACK()
			#ENDIF
		ENDIF
	ENDIF
	
	IF bAllowSea
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, GANG_CHASE_CLIENT_VALID_CHASE_TARGET_SEA)
			SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, GANG_CHASE_CLIENT_VALID_CHASE_TARGET_SEA)
			PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [SET_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET] Set I'm a valid target for gang chase - sea")
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET()
	IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, GANG_CHASE_CLIENT_VALID_CHASE_TARGET)
		CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, GANG_CHASE_CLIENT_VALID_CHASE_TARGET)
		PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [CLEAR_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET Set I'm NOT a valid target for gang chase")
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, GANG_CHASE_CLIENT_VALID_CHASE_TARGET_SEA)
		CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, GANG_CHASE_CLIENT_VALID_CHASE_TARGET_SEA)
		PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [CLEAR_LOCAL_PLAYER_IS_VALID_GANG_CHASE_TARGET Set I'm NOT a valid target for gang chase - sea")
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_A_VALID_GANG_CHASE_TARGET(PLAYER_INDEX playerId, BOOL bCheckLand = TRUE, BOOL bCheckSea = TRUE)
	PARTICIPANT_INDEX part
	
	IF playerId != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerId)
			
			part = NETWORK_GET_PARTICIPANT_INDEX(playerId)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
				IF NOT IS_PED_INJURED(GET_PLAYER_PED(playerId))
				//	PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [IS_PLAYER_A_VALID_GANG_CHASE_TARGET]  Bitset for player ", GET_PLAYER_NAME(playerId), " = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset)
					IF bCheckSea
						IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, GANG_CHASE_CLIENT_VALID_CHASE_TARGET_SEA)
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF bCheckLand
						IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset, GANG_CHASE_CLIENT_VALID_CHASE_TARGET)
							RETURN TRUE
						ENDIF
					ENDIF
					
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
			
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_VALID_GANG_CHASE_TARGET(BOOL bCheckLand = TRUE, BOOL bCheckSea = TRUE)
	RETURN IS_PLAYER_A_VALID_GANG_CHASE_TARGET(PLAYER_ID(), bCheckLand, bCheckSea)
ENDFUNC




FUNC BOOL IS_PARTICIPANT_VALID_TARGET(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iPart, BOOL bCheckLand = TRUE, BOOL bCheckSea = TRUE)
	IF iPart > -1
		IF GET_NUMBER_OF_GANG_CHASE_CHASING_PART(serverGangChaseData,iPart) <= MAX_GANG_CHASE_PER_TARGET
			RETURN (IS_PLAYER_A_VALID_GANG_CHASE_TARGET(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)), bCheckLand, bCheckSea))
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    For chasing AI peds, figure out which participant is closest to the target so the participant can figure out spawn coords
/// PARAMS:
///    serverGangChaseData - 
/// RETURNS:
///    
FUNC INT GET_CLOSEST_PART_TO_GANG_CHASE_TARGET_AI_PED(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
	INT i
	INT iClosestPart = -1
	
	PARTICIPANT_INDEX part
	PLAYER_INDEX player
	FLOAT fClosest2 = 9999999999.0
	FLOAT fDist2
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		part = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			player = NETWORK_GET_PLAYER_INDEX(part)
			fDist2 = VDIST2(GET_ENTITY_COORDS(GET_PLAYER_PED(player), FALSE), GET_ENTITY_COORDS(NET_TO_PED(serverGangChaseData.niTargetPed), FALSE))
			IF fDist2 < fClosest2
				fClosest2 = fDist2
				iClosestPart = i
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iClosestPart
ENDFUNC

/// PURPOSE:
///    For chasing specific vehs, figure out which participant is closest to the target veh so the participant can figure out spawn coords
/// PARAMS:
///    serverGangChaseData - 
/// RETURNS:
///    
FUNC INT GET_CLOSEST_PART_TO_GANG_CHASE_TARGET_VEH(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
	INT i
	INT iClosestPart = -1
	
	PARTICIPANT_INDEX part
	PLAYER_INDEX player
	FLOAT fClosest2 = 9999999999.0
	FLOAT fDist2
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		part = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			player = NETWORK_GET_PLAYER_INDEX(part)
			fDist2 = VDIST2(GET_ENTITY_COORDS(GET_PLAYER_PED(player), FALSE), GET_ENTITY_COORDS(NET_TO_VEH(serverGangChaseData.niTargetVeh), FALSE))
			IF fDist2 < fClosest2
				fClosest2 = fDist2
				iClosestPart = i
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iClosestPart
ENDFUNC

/// PURPOSE:
///    For chasing AI peds, figure out which participant is closest to the fixed coord so the participant can figure out spawn coords
/// PARAMS:
///    serverGangChaseData - 
/// RETURNS:
///    
FUNC INT GET_CLOSEST_PART_TO_FIXED_COORD_TARGET(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
	INT i
	INT iClosestPart = -1
	
	
	
	PARTICIPANT_INDEX part
	PLAYER_INDEX player
	FLOAT fClosest2 = 9999999999.0
	FLOAT fDist2
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		part = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			player = NETWORK_GET_PLAYER_INDEX(part)
			fDist2 = VDIST2(GET_ENTITY_COORDS(GET_PLAYER_PED(player), FALSE), serverGangChaseData.structGangChaseUnit[0].vTargetCoord)
			IF fDist2 < fClosest2
				fClosest2 = fDist2
				iClosestPart = i
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iClosestPart
ENDFUNC
//
//FUNC INT GET_PART_TO_CHASE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
//	INT i
//	INT iChase = -1
//	
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetPed)
//		iChase = GET_CLOSEST_PART_TO_GANG_CHASE_TARGET_AI_PED(serverGangChaseData)
//		PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [GET_PART_TO_CHASE] Want to chase AI target, this part is closest ", iChase, " who is player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iChase))))
//	ELSE
//		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
//			IF iChase = -1
//				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
//					IF IS_PARTICIPANT_VALID_TARGET(i)
//						IF GET_NUMBER_OF_GANG_CHASE_CHASING_PART(serverGangChaseData,i) <= MAX_GANG_CHASE_PER_TARGET
//							iChase = i
//							PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [GET_PART_TO_CHASE] Going to chase part ", i, " who is player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))))
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
//	RETURN iChase
//ENDFUNC


PROC SET_CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iType)
	PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [SET_CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE] Called with Type ", iType)
	
	SWITCH iType
		CASE GANG_CHASE_TYPE_LAND 	SET_GANG_CHASE_SERVER_BIT(serverGangChaseData, GANG_CHASE_SERVER_CREATED_ALL_LAND_UNITS_FOR_CURRENT_WAVE) 	BREAK
		CASE GANG_CHASE_TYPE_SEA	SET_GANG_CHASE_SERVER_BIT(serverGangChaseData, GANG_CHASE_SERVER_CREATED_ALL_SEA_UNITS_FOR_CURRENT_WAVE)	BREAK 	
	ENDSWITCH
ENDPROC

PROC CLEAR_CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iType)
	PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [CLEAR_CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE] Called with Type ", iType)
	
	SWITCH iType
		CASE GANG_CHASE_TYPE_LAND 	CLEAR_GANG_CHASE_SERVER_BIT(serverGangChaseData, GANG_CHASE_SERVER_CREATED_ALL_LAND_UNITS_FOR_CURRENT_WAVE) 	BREAK
		CASE GANG_CHASE_TYPE_SEA	CLEAR_GANG_CHASE_SERVER_BIT(serverGangChaseData, GANG_CHASE_SERVER_CREATED_ALL_SEA_UNITS_FOR_CURRENT_WAVE)	BREAK 	
	ENDSWITCH
ENDPROC

FUNC BOOL CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iType)
	SWITCH iType
		CASE GANG_CHASE_TYPE_LAND 	RETURN IS_GANG_CHASE_SERVER_BIT_SET(serverGangChaseData, GANG_CHASE_SERVER_CREATED_ALL_LAND_UNITS_FOR_CURRENT_WAVE)
		CASE GANG_CHASE_TYPE_SEA	RETURN IS_GANG_CHASE_SERVER_BIT_SET(serverGangChaseData, GANG_CHASE_SERVER_CREATED_ALL_SEA_UNITS_FOR_CURRENT_WAVE)	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_GANG_CHASE_TYPE_FROM_MODEL(MODEL_NAMES model)
	IF IS_THIS_MODEL_A_HELI(model)
		RETURN GANG_CHASE_TYPE_HELI
	ELIF IS_THIS_MODEL_A_BOAT(model)
	OR IS_THIS_MODEL_A_JETSKI(model)
		RETURN GANG_CHASE_TYPE_SEA
	ENDIF
	
	RETURN GANG_CHASE_TYPE_LAND
ENDFUNC

FUNC INT GET_GANG_CHASE_TYPE_FROM_TARGET(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)

	INT iTargetPart = serverGangChaseData.iGangChaseTargetPart
	
	IF IS_PARTICIPANT_VALID_TARGET(serverGangChaseData, iTargetPart, FALSE, TRUE)
		RETURN GANG_CHASE_TYPE_SEA
	ENDIF
	
	RETURN GANG_CHASE_TYPE_LAND
ENDFUNC

FUNC INT GET_GANG_CHASE_TYPE_FROM_PART(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iPart)
	INT iType
	
	IF IS_PARTICIPANT_VALID_TARGET(serverGangChaseData, iPart, TRUE, FALSE)
		iType = GANG_CHASE_TYPE_LAND
	ELIF IS_PARTICIPANT_VALID_TARGET(serverGangChaseData, iPart, FALSE, TRUE)
		iType = GANG_CHASE_TYPE_SEA
	ENDIF
	
	RETURN iType
ENDFUNC

/// PURPOSE:
///    Figure out whic player has the least number of gang chase currently chasing them
/// PARAMS:
///    serverGangChaseData - 
/// RETURNS:
///    
FUNC INT GET_VALID_TARGET_TO_CHASE_WITH_LEAST_CURRENTLY_CHASING(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
	INT iLeast = -1
	INT iNumberChasing = 99
	INT iNumChasingCurrent
	INT i
	
	BOOL bFoundPart
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NOT bFoundPart
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				IF IS_PARTICIPANT_VALID_TARGET(serverGangChaseData, i)
					iNumChasingCurrent = GET_NUMBER_OF_GANG_CHASE_CHASING_PART(serverGangChaseData, i)
					IF iNumChasingCurrent = 0
						bFoundPart = TRUE
						iLeast = i
						PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [GET_VALID_TARGET_TO_CHASE_WITH_LEAST_CURRENTLY_CHASING] Part ", i, " who is player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))), " as they have 0 GC currently chasing them")
					ELSE
						IF iNumChasingCurrent < iNumberChasing
							iLeast = i
							iNumberChasing = iNumChasingCurrent
							IF IS_PARTICIPANT_VALID_TARGET(serverGangChaseData, i)
								PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [GET_VALID_TARGET_TO_CHASE_WITH_LEAST_CURRENTLY_CHASING] Part ", i, " who is player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))), " has fewest chasing so far = ", iNumChasingCurrent)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iLeast
ENDFUNC

PROC INIT_PART_TO_CHASE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
	INT i
	INT iChase = -1
	INT iType
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetPed)
		iChase = GET_CLOSEST_PART_TO_GANG_CHASE_TARGET_AI_PED(serverGangChaseData)
		PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [INIT_PART_TO_CHASE] Want to chase AI target, this part is closest ", iChase, " who is player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iChase))))
	ELIF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetVeh)
		iChase = GET_CLOSEST_PART_TO_GANG_CHASE_TARGET_VEH(serverGangChaseData)
		PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [INIT_PART_TO_CHASE] Want to chase specific veh target, this part is closest ", iChase, " who is player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iChase))))
	ELIF NOT ARE_VECTORS_EQUAL(serverGangChaseData.structGangChaseUnit[0].vTargetCoord, << 0.0, 0.0, 0.0 >>)
		iChase = GET_CLOSEST_PART_TO_FIXED_COORD_TARGET(serverGangChaseData)
		PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [INIT_PART_TO_CHASE] Want to attacked fixed coord, this part is closest ", iChase, " who is player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iChase))))
	ELSE
		i = GET_VALID_TARGET_TO_CHASE_WITH_LEAST_CURRENTLY_CHASING(serverGangChaseData)
		IF i > -1
			IF iChase = -1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					IF IS_PARTICIPANT_VALID_TARGET(serverGangChaseData, i)
						iType = GET_GANG_CHASE_TYPE_FROM_PART(serverGangChaseData, i)
						IF NOT CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE(serverGangChaseData, iType)
							iChase = i
							PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [INIT_PART_TO_CHASE] Going to chase part ", i, " who is player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	serverGangChaseData.iGangChaseTargetPart = iChase
	serverGangChaseData.iGangChaseType = GET_GANG_CHASE_TYPE_FROM_TARGET(serverGangChaseData)
	
	IF serverGangChaseData.iGangChaseTargetPart > -1
		PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [INIT_PART_TO_CHASE] Chase iPart ", serverGangChaseData.iGangChaseTargetPart, " with type ", serverGangChaseData.iGangChaseType)
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Gets a random vector within a donut centred at vPos with a radius of fRadius and a height of fHeight.
/// PARAMS:
///    vPos - The sphere disc
///    fInnerRadius - Radius of the hole in the middle
///    fOuterRadius - Radius of the donut
///    fHeight - Height of the disc (goes both above and below vPos)
/// RETURNS:
///    A random position vector within fRadius of vPos.
FUNC VECTOR GET_POINT_IN_DONUT(VECTOR vPos, FLOAT fInnerRadius, FLOAT fOuterRadius, FLOAT fHeight)
 
	// Pick a random unit direction vector.
	VECTOR vDir = << GET_RANDOM_FLOAT_IN_RANGE(-1,1) , GET_RANDOM_FLOAT_IN_RANGE(-1,1), 0 >>
	FLOAT fHalfHeight = fHeight / 2.0
	
	// Project this vector into the disc some random distance.
	vDir = GET_VECTOR_OF_LENGTH(vDir, GET_RANDOM_FLOAT_IN_RANGE(fInnerRadius, fOuterRadius))
	
	// Grab a random height and add it on.
	vDir.z = GET_RANDOM_FLOAT_IN_RANGE(-fHalfHeight, fHalfHeight)
	
	RETURN vPos + vDir
ENDFUNC

PROC GET_BOAT_CREATION_POINT(ENTITY_INDEX vTargetVeh, VECTOR &vPos, FLOAT &fHeading, BOOL bSearchAroundVehicleCoords = FALSE)
	VECTOR vCentre
	IF bSearchAroundVehicleCoords
		vPos = GET_POINT_IN_DONUT(GET_ENTITY_COORDS(vTargetVeh), 50.0, 50.0*1.5, 10.0)
	ELSE
		vCentre = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vTargetVeh), GET_ENTITY_HEADING(vTargetVeh), <<0.0, 120, 0.0>>)
		vPos = GET_POINT_IN_DONUT(vCentre, 50.0/2, 50.0, 10.0)
	ENDIF
	
	fHeading = GET_HEADING_BETWEEN_VECTORS_2D(vPos, GET_ENTITY_COORDS(vTargetVeh))
ENDPROC


PROC CLEANUP_GANG_CHASE_CLIENT()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_GANG_CHASE_CLIENT_BIT_SET(PARTICIPANT_ID_TO_INT(), GANG_CHASE_CLIENT_INIT)
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset = 0
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords = << 0.0, 0.0, 0.0>>
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnRot = << 0.0, 0.0, 0.0>>
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.gangChaseUnitClient.iUnitFleeBitset = 0
			PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [CLEANUP_GANG_CAHSE_CLIENT] Reset")
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_GANG_CHASE_CLIENT(STRUCT_GANG_CHASE_LOCAL &gangChaseLocal, STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
//	VEHICLE_INDEX tempVeh
	ENTITY_INDEX ent
	
	INT iGangChaseType
	INT index
	FLOAT fSpanwDistFromContra
	FLOAT fBoatHeading
	IF NOT IS_GANG_CHASE_CLIENT_BIT_SET(PARTICIPANT_ID_TO_INT(), GANG_CHASE_CLIENT_INIT)
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.iGangChaseClientBitset = 0
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords = << 0.0, 0.0, 0.0>>
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnRot = << 0.0, 0.0, 0.0>>
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.gangChaseUnitClient.iUnitFleeBitset = 0
		PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] Client init")
		SET_GANG_CHASE_CLIENT_BIT(GANG_CHASE_CLIENT_INIT)
	ENDIF
//	PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] vGangChaseSpawnCoords = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords)
//	PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] vGangChasePreviousSpawnCoords = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChasePreviousSpawnCoords)
//	PRINTLN("[BIKER1]     ----------> [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] serverGangChaseData.vLastSpawnCoords = ", serverGangChaseData.vLastSpawnCoords)
	IF serverGangChaseData.iGangChaseTargetPart = PARTICIPANT_ID_TO_INT()
		IF ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChasePreviousSpawnCoords, << 0.0, 0.0, 0.0 >>)
		OR (NOT ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChasePreviousSpawnCoords ))
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) // Chasing me
				OR NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetPed) // Chasing AI ped
				OR NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetVeh) // Chasing specific mission veh
				OR NOT ARE_VECTORS_EQUAL(serverGangChaseData.structGangChaseUnit[0].vTargetCoord, << 0.0, 0.0, 0.0 >>) // Chasing fixed coord
				OR IS_GANG_CHASE_OPTION_SET(serverGangChaseData, GANG_CHASE_OPTIONS_ALWAYS_CHASE_PLAYER)
					IF ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords, << 0.0, 0.0, 0.0 >>)
						IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetPed)
							IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetVeh)
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									ent = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								ELSE
									ent = PLAYER_PED_ID()
								ENDIF
							ELSE
								ent = NET_TO_VEH(serverGangChaseData.niTargetVeh)
							ENDIF
						ELSE
							ent = NET_TO_PED(serverGangChaseData.niTargetPed)
						ENDIF
						iGangChaseType = serverGangChaseData.iGangChaseType
						index = serverGangChaseData.iNextGangChaseIndex
						IF index >= 0
							
							IF serverGangChaseData.mCustomVeh != DUMMY_MODEL_FOR_SCRIPT
								iGangChaseType = GET_GANG_CHASE_TYPE_FROM_MODEL(serverGangChaseData.mCustomVeh)
								PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] Custom model iGangChaseType: ", iGangChaseType)
							ELIF iGangChaseType = GANG_CHASE_TYPE_LAND
								IF serverGangChaseData.mGangChaseVeh != DUMMY_MODEL_FOR_SCRIPT
									IF IS_THIS_MODEL_A_HELI(serverGangChaseData.mGangChaseVeh)
										iGangChaseType = GANG_CHASE_TYPE_HELI
										PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] Model is a heli iGangChaseType: ", iGangChaseType)
									ENDIF
								ENDIF
							ENDIF
							
							SWITCH iGangChaseType
								CASE GANG_CHASE_TYPE_LAND
									IF IS_VECTOR_ZERO(gangChaseLocal.vGangChaseSpawnDir)
										IF ARE_VECTORS_EQUAL(serverGangChaseData.vOverrideDirectionVector, << 0.0, 0.0, 0.0 >>)
											gangChaseLocal.vGangChaseSpawnDir = GET_ENTITY_FORWARD_VECTOR(ent)
										ELSE gangChaseLocal.vGangChaseSpawnDir = serverGangChaseData.vOverrideDirectionVector
											PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] - serverBD.vSpawnDirection = GET_ENTITY_FORWARD_VECTOR = ", gangChaseLocal.vGangChaseSpawnDir)
										ENDIF
									ELSE
										FLOAT fSpawnDistance
										FLOAT fMaxXOffset
										PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] trying to find spawn coords for GC Unit ", index, ". Attempt number ", gangChaseLocal.iSpawnAttempts, " # Waves so far ", GET_NUMBER_OF_GANG_CHASE_WAVES_CREATED(serverGangChaseData))
										
										IF (gangChaseLocal.vGangChaseSpawnDir.z >= 0.0 AND gangChaseLocal.vGangChaseSpawnDir.z < 90.0)
										OR (gangChaseLocal.vGangChaseSpawnDir.z >= 270.0 AND gangChaseLocal.vGangChaseSpawnDir.z < 360.0)
											fSpawnDistance = 220
										ELSE
											fSpawnDistance = 120
										ENDIF
										BOOL bSpawn
										
										VECTOR vOffsetFromContra
										VECTOR vInitialCoords
										
										IF ARE_VECTORS_EQUAL(serverGangChaseData.structGangChaseUnit[0].vTargetCoord, << 0.0, 0.0, 0.0 >>)
											vInitialCoords = GET_ENTITY_COORDS(ent)
										ELSE
											vInitialCoords = serverGangChaseData.structGangChaseUnit[0].vTargetCoord
										ENDIF
										bSpawn = TRUE 
										IF FIND_SPAWN_POINT_IN_DIRECTION(vInitialCoords, gangChaseLocal.vGangChaseSpawnDir, fSpawnDistance, gangChaseLocal.vTempGangChaseSpawnCoords)
											fSpanwDistFromContra = GET_DISTANCE_BETWEEN_COORDS(gangChaseLocal.vTempGangChaseSpawnCoords, vInitialCoords)
											
											IF ARE_VECTORS_EQUAL(serverGangChaseData.structGangChaseUnit[0].vTargetCoord, << 0.0, 0.0, 0.0 >>)
												vOffsetFromContra = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(ent, gangChaseLocal.vTempGangChaseSpawnCoords)
											ENDIF
											
											//-- Too far from the player?
											IF fSpanwDistFromContra > 290.0
												bSpawn = FALSE
												PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] - rejecting  spawn coords too far from player ", gangChaseLocal.vTempGangChaseSpawnCoords, " dist = ", fSpanwDistFromContra)
											ENDIF
											
											//-- Location ok?
											IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(gangChaseLocal.vTempGangChaseSpawnCoords, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 190)
												bSpawn = FALSE
												PRINTLN("[BIKER1]     ---------->   -  [MAINTAIN_GANG_CHASE_CLIENT] - gangChaseClient.vGangChaseSpawnCoords not okay for net entity creation, setting back to zero so we search again")
											ENDIF
											
											IF gangChaseLocal.iSpawnAttempts <= 10
												fMaxXOffset = 100.0
											ELIF gangChaseLocal.iSpawnAttempts <= 40
												fMaxXOffset = 110.0
											ELIF gangChaseLocal.iSpawnAttempts <= 60
												fMaxXOffset = 125.0
											ELIF gangChaseLocal.iSpawnAttempts <= 100
												fMaxXOffset = 150.0
												
											ELIF gangChaseLocal.iSpawnAttempts <= 200
												fMaxXOffset = 300
											ELSE
												fMaxXOffset = 500
											ENDIF
											
											IF ABSF(vOffsetFromContra.x) > fMaxXOffset
												bSpawn = FALSE
												gangChaseLocal.iSpawnAttempts++
												PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] REJECTING COORDS ", gangChaseLocal.vTempGangChaseSpawnCoords ," As X offset from vehicle = ", vOffsetFromContra.x, " fMaxXOffset = ", fMaxXOffset)
											ENDIF
											
											IF NOT bSpawn
												gangChaseLocal.vTempGangChaseSpawnCoords = << 0.0, 0.0, 0.0 >>
											ELSE
												GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords = gangChaseLocal.vTempGangChaseSpawnCoords 
												GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnRot.z = GET_HEADING_BETWEEN_VECTORS_2D(gangChaseLocal.vTempGangChaseSpawnCoords, GET_ENTITY_COORDS(ent))
												GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChasePreviousSpawnCoords  = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords
												PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] - gangChaseClient.vGangChaseSpawnCoords is okay for net entity creation. gangChaseClient.vGangChaseSpawnCoords = ", gangChaseLocal.vTempGangChaseSpawnCoords, "Dist = ",fSpanwDistFromContra, " Tried to spawn at least this far ", fSpawnDistance)
												PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] - .gangChaseClient.vGangChaseSpawnRotg = GET_HEADING_BETWEEN_VECTORS_2D = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnRot)
											
											ENDIF
											
											
										ELSE
											PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] -FIND_SPAWN_POINT_IN_DIRECTION = FALSE. Adding rotation to coords with ADD_ROTATION_FOR_SPAWN_CHECK")
											ADD_ROTATION_FOR_SPAWN_CHECK(gangChaseLocal)
										ENDIF
									ENDIF
								BREAK
								
								CASE GANG_CHASE_TYPE_SEA 
									GET_BOAT_CREATION_POINT(ent, gangChaseLocal.vTempGangChaseSpawnCoords, fBoatHeading)
									
									IF TEST_PROBE_AGAINST_WATER(gangChaseLocal.vTempGangChaseSpawnCoords+<<0, 0, 2>>, gangChaseLocal.vTempGangChaseSpawnCoords-<<0, 0, 10.0>>, gangChaseLocal.vTempGangChaseSpawnCoords)
										IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(gangChaseLocal.vTempGangChaseSpawnCoords, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 190)
											IF IS_LOCATION_ABOVE_GROUND(gangChaseLocal.vTempGangChaseSpawnCoords)
												GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords = gangChaseLocal.vTempGangChaseSpawnCoords
												GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnRot.z = fBoatHeading
												fSpanwDistFromContra = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords)
												GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChasePreviousSpawnCoords  = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords
												PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] Point ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords, " Ok! Dist = ", fSpanwDistFromContra)
											ELSE
												PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] Point ", gangChaseLocal.vTempGangChaseSpawnCoords, " failed IS_LOCATION_ABOVE_GROUND, will try again ")
												gangChaseLocal.vTempGangChaseSpawnCoords = << 0.0, 0.0, 0.0 >>
											ENDIF
										ELSE
											PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] Point ", gangChaseLocal.vTempGangChaseSpawnCoords, " failed IS_POINT_OK_FOR_NET_ENTITY_CREATION, will try again ")
											gangChaseLocal.vTempGangChaseSpawnCoords = << 0.0, 0.0, 0.0 >>
										ENDIF
									ELSE
										PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] Point ", gangChaseLocal.vTempGangChaseSpawnCoords, " failed water probe, will try again ")
										gangChaseLocal.vTempGangChaseSpawnCoords = << 0.0, 0.0, 0.0 >>
									ENDIF
								BREAK
								
								CASE GANG_CHASE_TYPE_HELI
									IF IS_VECTOR_ZERO(gangChaseLocal.vGangChaseSpawnDir)
										IF ARE_VECTORS_EQUAL(serverGangChaseData.vOverrideDirectionVector, << 0.0, 0.0, 0.0 >>)
											gangChaseLocal.vGangChaseSpawnDir = GET_ENTITY_FORWARD_VECTOR(ent)
										ELSE gangChaseLocal.vGangChaseSpawnDir = serverGangChaseData.vOverrideDirectionVector
											PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] - [MAINTAIN_GANG_CHASE_CLIENT] [GANG_CHASE_TYPE_HELI] serverBD.vSpawnDirection = GET_ENTITY_FORWARD_VECTOR = ", gangChaseLocal.vGangChaseSpawnDir)
										ENDIF
									ELSE
										FLOAT fSpawnDistance
										
										IF (gangChaseLocal.vGangChaseSpawnDir.z >= 0.0 AND gangChaseLocal.vGangChaseSpawnDir.z < 90.0)
										OR (gangChaseLocal.vGangChaseSpawnDir.z >= 270.0 AND gangChaseLocal.vGangChaseSpawnDir.z < 360.0)
											fSpawnDistance = 220
										ELSE
											fSpawnDistance = 120
										ENDIF
										BOOL bSpawn
										
										VECTOR vOffsetFromContra
										VECTOR vInitialCoords
										
										IF ARE_VECTORS_EQUAL(serverGangChaseData.structGangChaseUnit[0].vTargetCoord, << 0.0, 0.0, 0.0 >>)
											vInitialCoords = GET_ENTITY_COORDS(ent)
										ELSE
											vInitialCoords = serverGangChaseData.structGangChaseUnit[0].vTargetCoord
										ENDIF
										bSpawn = TRUE 
										IF FIND_SPAWN_POINT_IN_DIRECTION(vInitialCoords, gangChaseLocal.vGangChaseSpawnDir, fSpawnDistance, gangChaseLocal.vTempGangChaseSpawnCoords)
											fSpanwDistFromContra = GET_DISTANCE_BETWEEN_COORDS(gangChaseLocal.vTempGangChaseSpawnCoords, vInitialCoords)
											gangChaseLocal.vTempGangChaseSpawnCoords.z += 75.0
											
											IF ARE_VECTORS_EQUAL(serverGangChaseData.structGangChaseUnit[0].vTargetCoord, << 0.0, 0.0, 0.0 >>)
												vOffsetFromContra = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(ent, gangChaseLocal.vTempGangChaseSpawnCoords)
											ENDIF
											
											//-- Too far from the player?
											IF fSpanwDistFromContra > 290.0
												bSpawn = FALSE
												PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] [GANG_CHASE_TYPE_HELI] - rejecting  spawn coords too far from player ", gangChaseLocal.vTempGangChaseSpawnCoords, " dist = ", fSpanwDistFromContra)
											ENDIF
											
											//-- Location ok?
											IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(gangChaseLocal.vTempGangChaseSpawnCoords, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 500)
												bSpawn = FALSE
												PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] - [GANG_CHASE_TYPE_HELI] -gangChaseClient.vGangChaseSpawnCoords not okay for net entity creation, setting back to zero so we search again")
											ENDIF
											
											IF ABSF(vOffsetFromContra.x) > 100.0
												bSpawn = FALSE
												PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] [GANG_CHASE_TYPE_HELI] REJECTING COORDS ", gangChaseLocal.vTempGangChaseSpawnCoords ," As X offset from vehicle = ", vOffsetFromContra.x)
											ENDIF
											
											IF NOT bSpawn
												gangChaseLocal.vTempGangChaseSpawnCoords = << 0.0, 0.0, 0.0 >>
											ELSE
												IF ARE_VECTORS_EQUAL(gangChaseLocal.vTempGangChaseSpawnCoords, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChasePreviousSpawnCoords )
													PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] New spawn coord same as last sapwn coord, but passed all spawning checks")
													gangChaseLocal.vTempGangChaseSpawnCoords.z += 1.0
												ENDIF
												GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords = gangChaseLocal.vTempGangChaseSpawnCoords 
												GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnRot.z = GET_HEADING_BETWEEN_VECTORS_2D(gangChaseLocal.vTempGangChaseSpawnCoords, GET_ENTITY_COORDS(ent))
												GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChasePreviousSpawnCoords  = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords
												PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] - gangChaseClient.vGangChaseSpawnCoords is okay for net entity creation. gangChaseClient.vGangChaseSpawnCoords = ", gangChaseLocal.vTempGangChaseSpawnCoords, "Dist = ",fSpanwDistFromContra, " Tried to spawn at least this far ", fSpawnDistance)
												PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] - gangChaseClient.vGangChaseSpawnRotg = GET_HEADING_BETWEEN_VECTORS_2D = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnRot)
											
											ENDIF
											
											
										ELSE
											PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] [GANG_CHASE_TYPE_HELI] -FIND_SPAWN_POINT_IN_DIRECTION = FALSE. Adding rotation to coords with ADD_ROTATION_FOR_SPAWN_CHECK")
											ADD_ROTATION_FOR_SPAWN_CHECK(gangChaseLocal)
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						ELSE
				//			PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] MAINTAIN_GANG_CHASE_CLIENT waiting for next gang chase index!")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT ARE_VECTORS_EQUAL(serverGangChaseData.vLastSpawnCoords, << 0.0, 0.0, 0.0 >>)
				IF ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords, serverGangChaseData.vLastSpawnCoords) 
				
					PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] Reset my gang chase spawn data as my last spawn coords match servers - need to find new coords")
					gangChaseLocal.vGangChaseSpawnDir = << 0.0, 0.0, 0.0 >>
					gangChaseLocal.vTempGangChaseSpawnCoords = << 0.0, 0.0, 0.0>>
					gangChaseLocal.iGangChaseSpawnCoordRotCount = 0
					gangChaseLocal.iSpawnAttempts = 0
					GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords = << 0.0, 0.0, 0.0>>
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT ARE_VECTORS_EQUAL(gangChaseLocal.vTempGangChaseSpawnCoords, << 0.0, 0.0, 0.0 >>)
			PRINTLN("[BIKER1]     ---------->   - [GANG_CHASE] [MAINTAIN_GANG_CHASE_CLIENT] Reset my gang chase spawn data as serverBD.iGangChaseTargetPart = ", serverGangChaseData.iGangChaseTargetPart)
			gangChaseLocal.vGangChaseSpawnDir = << 0.0, 0.0, 0.0 >>
			gangChaseLocal.vTempGangChaseSpawnCoords = << 0.0, 0.0, 0.0>>
			gangChaseLocal.iGangChaseSpawnCoordRotCount = 0
			gangChaseLocal.iSpawnAttempts = 0
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords = << 0.0, 0.0, 0.0>>
		ENDIF
	ENDIF
	
	INT iGangChase
	REPEAT MAX_GANG_CHASE iGangChase
		PROCESS_GANG_CHASE_BODY(gangChaseLocal, serverGangChaseData, iGangChase)
	ENDREPEAT
	
	
ENDPROC


PROC MAINTAIN_GANG_CHASE_MAX_PARTICIPANT_CHECK_SERVER(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, INT iGangChase)
	
	
//	IF NOT IS_GANG_CHASE_ALLOWED_TO_TRIGGER(serverGangChaseData)
//		EXIT
//	ENDIF
	
	BOOL bAllowedToTrigger = FALSE
	
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(iPlayer)
		IF NETWORK_IS_PLAYER_ACTIVE(PlayerID)
			IF NOT IS_PLAYER_SCTV(PlayerId)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerID)
					//-- Allowed to trigger?
					IF NOT bAllowedToTrigger
						IF IS_PLAYER_A_VALID_GANG_CHASE_TARGET(PlayerID)
						//	PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] [MAINTAIN_GANG_CHASE_MAX_PARTICIPANT_CHECK_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " is a valid gang chase target")
							bAllowedToTrigger = TRUE
						ENDIF
					ENDIF
					
					//-- Handle fleeing from the drop off
					IF NOT IS_BIT_SET(serverGangChaseData.iGangChaseFleeBitset, iGangChase)
						IF IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.gangChaseClient.gangChaseUnitClient.iUnitFleeBitset, iGangChase)
							SET_BIT(serverGangChaseData.iGangChaseFleeBitset, iGangChase)
							
							#IF IS_DEBUG_BUILD
							PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] [MAINTAIN_GANG_CHASE_MAX_PARTICIPANT_CHECK_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " thinks this gang chase unit ", iGangChase, " should flee")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bAllowedToTrigger
		IF NOT IS_GANG_CHASE_ALLOWED_TO_TRIGGER(serverGangChaseData)
			SET_GANG_CHASE_ALLOWED_TO_TRIGGER_SERVER(serverGangChaseData)
		ENDIF
	ELSE
		IF IS_GANG_CHASE_ALLOWED_TO_TRIGGER(serverGangChaseData)
			CLEAR_GANG_CHASE_ALLOWED_TO_TRIGGER_SERVER(serverGangChaseData)
		ENDIF
	ENDIF
ENDPROC

PROC SET_GANG_CHASE_PED_COMPONENT_VARIATION(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, MODEL_NAMES mPed, INT iGcUnit, INT iGcPed)
	PED_INDEX ped = NET_TO_PED(serverGangChaseData.structGangChaseUnit[iGcUnit].niGCPed[iGcPed])
	PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]	[CREATE_GANG_CHASE_PED_AND_VEHICLE] [SET_GANG_CHASE_PED_COMPONENT_VARIATION] Unit ", iGcUnit, " iGcPed ", iGcPed)
	IF mPed = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
		//-- 5 variations
		SWITCH iGcUnit % 5
			CASE 0
				SWITCH iGcPed
					CASE 0
					 	// Var 1 
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 2)
						SET_PED_PROP_INDEX(ped, ANCHOR_HEAD, 0, 0)
					BREAK
					
					CASE 1
						// Var 2
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 1, 0)
						SET_PED_PROP_INDEX(ped, ANCHOR_EYES, 0, 0)
					BREAK
					
					DEFAULT
						// Var 3
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 2, 0)
					BREAK
				ENDSWITCH
			BREAK
			
			CASE 1
				SWITCH iGcPed
					CASE 0
						// Var 3
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 2, 0)
					 	
					BREAK
					
					CASE 1
						// Var 4
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 3, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 3, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 3, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 3, 0)
						
					BREAK
					
					DEFAULT
						// Var 5
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 4, 0)
						SET_PED_PROP_INDEX(ped, ANCHOR_HEAD, 1, 0)
					BREAK
				ENDSWITCH
			BREAK
			
			
			CASE 2
				SWITCH iGcPed
					CASE 0
						// Var 5
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 4, 0)
						SET_PED_PROP_INDEX(ped, ANCHOR_HEAD, 1, 0)
						
					 	
					BREAK
					
					CASE 1
						// Var 1 
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 2)
						SET_PED_PROP_INDEX(ped, ANCHOR_HEAD, 0, 0)
						
					BREAK
					
					DEFAULT
						// Var 2
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 1, 0)
						SET_PED_PROP_INDEX(ped, ANCHOR_EYES, 0, 0)
					BREAK
				ENDSWITCH
			BREAK
			
			
			
			CASE 3
				SWITCH iGcPed
					CASE 0
						// Var 2
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 1, 0)
						SET_PED_PROP_INDEX(ped, ANCHOR_EYES, 0, 0)
						
					 	
					BREAK
					
					CASE 1
						// Var 3
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 2, 0)
						
					BREAK
					
					DEFAULT
						// Var 4
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 3, 2)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 3, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 3, 2)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 3, 0)
						
					BREAK
				ENDSWITCH
			BREAK
			
			CASE 4
				SWITCH iGcPed
					CASE 0
						// Var 4
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 3, 2)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 3, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 3, 2)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 3, 0)
						
					 	
					BREAK
					
					CASE 1
						// Var 1 
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 2)
						SET_PED_PROP_INDEX(ped, ANCHOR_HEAD, 0, 0)
						
						
					BREAK
					
					DEFAULT
						// Var 5
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 4, 0)
						SET_PED_PROP_INDEX(ped, ANCHOR_HEAD, 1, 0)
						
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ELIF mPed = INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
		//-- 5 variations
		SWITCH iGcUnit % 5
			CASE 0
				SWITCH iGcPed 
					CASE 0
					 	// Var 1 
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 0, 0)
					BREAK
					
					CASE 1
						// Var 2
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 1, 1)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 1, 0)
					BREAK
					
					DEFAULT
						// Var 3
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 2, 0)
					BREAK
				ENDSWITCH
			BREAK
			
			CASE 1
				SWITCH iGcPed
					CASE 0
						// Var 3
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 2, 0)
					 	
					BREAK
					
					CASE 1
						// Var 4
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 3, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 3, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 3, 3)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 3, 3)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 3, 0)
						
					BREAK
					
					DEFAULT
						// Var 5
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 4, 1)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 4, 2)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 4, 0)
					BREAK
				ENDSWITCH
			BREAK
			
			CASE 2
				SWITCH iGcPed
					CASE 0
						// Var 5
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 4, 1)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 4, 2)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 4, 0)
						
					 	
					BREAK
					
					CASE 1
						// Var 1 
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 0, 0)
						
					BREAK
					
					DEFAULT
						// Var 2
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 1, 1)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 1, 0)
					BREAK
				ENDSWITCH
			BREAK
			
			CASE 3
				SWITCH iGcPed
					CASE 0
						// Var 2
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 1, 1)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 1, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 1, 0)
						
					 	
					BREAK
					
					CASE 1
						// Var 3
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 2, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 2, 0)
						
					BREAK
					
					DEFAULT
						// Var 4
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 3, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 3, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 3, 3)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 3, 3)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 3, 0)
						
					BREAK
				ENDSWITCH
			BREAK
			
			
			CASE 4
				SWITCH iGcPed
					CASE 0
						// Var 4
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 3, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 3, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 3, 3)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 3, 3)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 3, 0)
						
					 	
					BREAK
					
					CASE 1
						// Var 1 
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 0, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 0, 0)
						
						
					BREAK
					
					DEFAULT
						// Var 5
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, 4, 0)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO, 4, 1)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG, 4, 2)
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 4, 0)
						
					BREAK
				ENDSWITCH
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC




/// PURPOSE:
///    Create a new gang chase vehicle with associated peds inside
/// PARAMS:
///    index - An identifier for the gang chase 'unit'
///    vCoord - Where to create
///    fHeading - Heading when created
///    iNumberOfPeds - Number of peds to create inside the gang chase vehicle
///    mVeh - Vehicle model
///    mPed - Ped model
/// RETURNS:
///    
FUNC BOOL CREATE_GANG_CHASE_PED_AND_VEHICLE(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, STRUCT_GANG_CHASE_OPTIONS &gangChaseOptions, VECTOR vCoord, FLOAT fHeading, INT iNumberOfPeds, MODEL_NAMES mVeh, MODEL_NAMES mPed)
	INT i
	PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]	[CREATE_GANG_CHASE_PED_AND_VEHICLE] Called With...")
	PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]	[CREATE_GANG_CHASE_PED_AND_VEHICLE]		vCoord = ", vCoord)
	PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]	[CREATE_GANG_CHASE_PED_AND_VEHICLE]		fHeading = ", fHeading)
	PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]	[CREATE_GANG_CHASE_PED_AND_VEHICLE]		iNumberOfPeds = ", iNumberOfPeds)
	PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]	[CREATE_GANG_CHASE_PED_AND_VEHICLE]		mVeh = ", mVeh)
	PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]	[CREATE_GANG_CHASE_PED_AND_VEHICLE]		mPed = ", mPed)
	PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]	[CREATE_GANG_CHASE_PED_AND_VEHICLE]		iAccuracy = ", gangChaseOptions.iPedAccuracy)
	PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]	[CREATE_GANG_CHASE_PED_AND_VEHICLE]		Rel Group = ", ENUM_TO_INT(gangChaseOptions.relGangChase))
	REPEAT MAX_PEDS_PER_GANG_CHASE_UNIT i
		IF gangChaseOptions.mGangChasePedCustom[i] != DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]	[CREATE_GANG_CHASE_PED_AND_VEHICLE]		mGangChasePedCustom[", i, "] = ", gangChaseOptions.mGangChasePedCustom[i])
		ENDIF
	ENDREPEAT
	PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]	[CREATE_GANG_CHASE_PED_AND_VEHICLE] ")
	
	VEHICLE_INDEX vehId
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tl15Name
	#ENDIF
	
	i = 0
	INT index = serverGangChaseData.iNextGangChaseIndex
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.structGangChaseUnit[index].niGCVeh)
		IF CAN_REGISTER_MISSION_VEHICLES(1)
			IF REQUEST_LOAD_MODEL(mVeh)
				IF CREATE_NET_VEHICLE(serverGangChaseData.structGangChaseUnit[index].niGCVeh, mVeh, vCoord, fHeading, TRUE, TRUE, TRUE, FALSE, TRUE)
					vehId = NET_TO_VEH(serverGangChaseData.structGangChaseUnit[index].niGCVeh)
					SET_VEHICLE_TYRES_CAN_BURST(vehId, TRUE)
    				SET_VEHICLE_HAS_STRONG_AXLES(vehId, FALSE)
					SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, TRUE)
					SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehId)
					
					SET_VEHICLE_FORWARD_SPEED(vehId, 20.0)
					
					IF mVeh != SEASHARK
						IF gangChaseOptions.bLockVehicleDoors
							SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED)
						ENDIF
					ENDIF
					
					SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehId, TRUE)
					SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
					
					IF gangChaseOptions.bSetVehicleColours[index]
						SET_VEHICLE_COLOURS(vehId, gangChaseOptions.iVehicleColour1[index], gangChaseOptions.iVehicleColour2[index])
						PRINTLN("[BIKER1]    ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] Setting colours of vehicle ", index, " to ", gangChaseOptions.iVehicleColour1[index], " and ", gangChaseOptions.iVehicleColour2[index])
					ENDIF
					IF gangChaseOptions.bSetVehicleExtraColours[index]
						SET_VEHICLE_EXTRA_COLOURS(vehId, gangChaseOptions.iVehicleColour3[index], gangChaseOptions.iVehicleColour4[index])
						PRINTLN("[BIKER1]    ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] Setting extra colours of vehicle ", index, " to ", gangChaseOptions.iVehicleColour3[index], " and ", gangChaseOptions.iVehicleColour4[index])
					ENDIF
					IF gangChaseOptions.bSetVehicleExtraColour5[index]
						SET_VEHICLE_EXTRA_COLOUR_5(vehId, gangChaseOptions.iVehicleColour5[index])
						PRINTLN("[BIKER1]    ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] Setting colour 5 of vehicle ", index, " to ", gangChaseOptions.iVehicleColour5[index])
					ENDIF
					IF gangChaseOptions.bSetVehicleExtraColour6[index]
						SET_VEHICLE_EXTRA_COLOUR_5(vehId, gangChaseOptions.iVehicleColour6[index])
						PRINTLN("[BIKER1]    ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] Setting colour 6 of vehicle ", index, " to ", gangChaseOptions.iVehicleColour6[index])
					ENDIF
					
					IF !gangChaseOptions.bSetVehicleColours[index]
						IF mVeh = BATI
						OR mVeh = FUGITIVE
							SET_VEHICLE_COLOURS(vehId, 0, 0) 
						ENDIF
					ENDIF
					
					SET_VEHICLE_MODS_FOR_IMP_EXP_GANG_CHASE_UNIT(vehId, index)
					
					IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
				    	DECOR_SET_INT(vehId, "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
						PRINTLN("[BIKER1]    ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] - Vehicle ", index, " set as 'Not_Allow_As_Saved_Veh'.")
					ENDIF
					
					IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
						INT iDecoratorValue
						IF DECOR_EXIST_ON(vehId, "MPBitset")
							iDecoratorValue = DECOR_GET_INT(vehId, "MPBitset")
						ENDIF
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
						DECOR_SET_INT(vehId, "MPBitset", iDecoratorValue)
						
						PRINTLN("[BIKER1]    ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] - Vehicle ", index, " set as 'MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE'.")
						PRINTLN("[BIKER1]    ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] - Vehicle ", index, " set as 'MP_DECORATOR_BS_NON_MODDABLE_VEHICLE'.")
					ENDIF
					
					PRINTLN("[BIKER1]    ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] Created gang chase veh ", index, " at coord", vCoord, " and heading ", fHeading)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PED_INDEX pedId
	VEHICLE_SEAT vsSeatToUse
	MODEL_NAMES mTemp = mPed
	WEAPON_TYPE weaponToUse = WEAPONTYPE_MICROSMG
	
	FLOAT fBreakLockAngle
	FLOAT fBreakLockAngleClose
	FLOAT fBreakLockCloseDistance
	FLOAT fTurnRateModifier
	
	REPEAT iNumberOfPeds i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.structGangChaseUnit[index].niGCPed[i]) 
			IF CAN_REGISTER_MISSION_PEDS(1)
				IF gangChaseOptions.mGangChasePedCustom[i] != DUMMY_MODEL_FOR_SCRIPT
					mPed = gangChaseOptions.mGangChasePedCustom[i]
				ELSE
					mPed = mTemp
				ENDIF
				
				IF REQUEST_LOAD_MODEL(mPed)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.structGangChaseUnit[index].niGCVeh)
						IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverGangChaseData.structGangChaseUnit[index].niGCVeh)
							NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverGangChaseData.structGangChaseUnit[index].niGCVeh)
						ENDIF
						
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverGangChaseData.structGangChaseUnit[index].niGCVeh)
							vsSeatToUse = INT_TO_ENUM(VEHICLE_SEAT, (i-1))
							IF CREATE_NET_PED_IN_VEHICLE(serverGangChaseData.structGangChaseUnit[index].niGCPed[i],
														serverGangChaseData.structGangChaseUnit[index].niGCVeh,
														PEDTYPE_CRIMINAL,
														mPed,
														vsSeatToUse)
								
								pedId = NET_TO_PED(serverGangChaseData.structGangChaseUnit[index].niGCPed[i])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
								SET_PED_DIES_WHEN_INJURED(pedId, TRUE)
								
								IF IS_THIS_MODEL_A_BOAT(mVeh)
								//	SET_PED_DIES_INSTANTLY_IN_WATER(pedId, TRUE)
								ENDIF
								
								IF vsSeatToUse = VS_DRIVER
								AND gangChaseOptions.bDriversUsePistols
									weaponToUse = WEAPONTYPE_PISTOL
								ELSE
									IF gangChaseOptions.weaponGangChasePed != WEAPONTYPE_INVALID
										weaponToUse = gangChaseOptions.weaponGangChasePed
									ELSE
										weaponToUse = WEAPONTYPE_MICROSMG
									ENDIF
								ENDIF
								GIVE_WEAPON_TO_PED(pedId , weaponToUse, 999999999, TRUE)
								
								SET_PED_CONFIG_FLAG(pedId, PCF_DontInfluenceWantedLevel, TRUE)
								SET_PED_HIGHLY_PERCEPTIVE(pedId, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(pedId, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
								SET_PED_TARGET_LOSS_RESPONSE(pedId, TLR_NEVER_LOSE_TARGET)
								SET_PED_CONFIG_FLAG(pedId, PCF_DisableLadderClimbing, TRUE)
								SET_PED_KEEP_TASK(pedId, TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(pedId, gangChaseOptions.relGangChase)		
								SET_PED_COMBAT_ATTRIBUTES(pedId, CA_DO_DRIVEBYS, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(pedId, CA_DISABLE_BLOCK_FROM_PURSUE_DURING_VEHICLE_CHASE,FALSE)
								SET_PED_COMBAT_ATTRIBUTES(pedId, CA_DISABLE_CRUISE_IN_FRONT_DURING_BLOCK_DURING_VEHICLE_CHASE,FALSE)
								SET_PED_COMBAT_ATTRIBUTES(pedId, CA_USE_VEHICLE,TRUE)
								SET_PED_COMBAT_ATTRIBUTES(pedId, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,TRUE)
								SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedId,KNOCKOFFVEHICLE_HARD)
								
								IF IS_GANG_CHASE_OPTION_SET(serverGangChaseData, GANG_CHASE_OPTIONS_DONT_JACK_PLAYER)
									SET_PED_CONFIG_FLAG(pedId, PCF_NotAllowedToJackAnyPlayers, TRUE)
								ENDIF
								
								IF IS_GANG_CHASE_OPTION_SET(serverGangChaseData, GANG_CHASE_OPTIONS_DONT_LEAVE_VEHICLE)
									SET_PED_COMBAT_ATTRIBUTES(pedId, CA_LEAVE_VEHICLES, FALSE)
									PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] GANG_CHASE_OPTIONS_DONT_LEAVE_VEHICLE ", i)
								ENDIF
								
								IF serverGangChaseData.iDifficulty <= 0
									PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] Set accuracy for ped ", i, " = ", gangChaseOptions.iPedAccuracy)
									SET_PED_ACCURACY(pedId, gangChaseOptions.iPedAccuracy)
								ELSE
									GB_BIKER_SET_PED_ATTRIBUTES_FOR_DIFFICULTY(pedId, serverGangChaseData.iDifficulty)
								ENDIF
								
								IF IS_THIS_MODEL_A_HELI(mVeh)
									IF vsSeatToUse = VS_DRIVER
									OR vsSeatToUse = VS_FRONT_RIGHT
										IF gangChaseOptions.iPedAccuracy > 10
											SET_PED_ACCURACY(pedId, 10)
											PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] Set accuracy for ped ", i, " = 10 as heli pilot / passenger")
										ENDIF
										
										SET_PED_SHOOT_RATE(pedId, 80  )
										
										fBreakLockAngle				= g_sMPTunables.furbanw_homing_rocket_break_lock_angle 			
										fBreakLockAngleClose		= g_sMPTunables.furbanw_homing_rocket_break_lock_angle_close		
										fBreakLockCloseDistance		= g_sMPTunables.furbanw_homing_rocket_break_lock_close_distance	
										fTurnRateModifier			= g_sMPTunables.furbanw_homing_rocket_turn_rate_modifier    		
										
										SET_COMBAT_FLOAT(pedId, CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE, fBreakLockAngle)
										SET_COMBAT_FLOAT(pedId, CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE, fBreakLockAngleClose)
										SET_COMBAT_FLOAT(pedId, CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE, fBreakLockCloseDistance)
										SET_COMBAT_FLOAT(pedId, CCF_HOMING_ROCKET_TURN_RATE_MODIFIER, fTurnRateModifier)
										
									ENDIF
								ENDIF
								
								IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.niTargetPed)
									SET_COMBAT_FLOAT(pedId, CCF_AUTOMOBILE_SPEED_MODIFIER, 2.0)
									SET_DRIVER_AGGRESSIVENESS(pedId, 1.0)
								ENDIF
								
								SET_GANG_CHASE_PED_COMPONENT_VARIATION(serverGangChaseData, mPed, index, i)
								
								#IF IS_DEBUG_BUILD
								tl15Name = ""
								tl15Name += "GC: "
								tl15Name += index
								tl15Name += " P: "
								tl15Name += i
								SET_PED_NAME_DEBUG(pedId, tl15Name)
								#ENDIF
								PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]   [CREATE_GANG_CHASE_PED_AND_VEHICLE] Created gang chase ped ", index, " in seat ", ENUM_TO_INT(vsSeatToUse))						
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.structGangChaseUnit[index].niGCVeh)
		RETURN FALSE
	ENDIF
	
	REPEAT iNumberOfPeds i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverGangChaseData.structGangChaseUnit[index].niGCPed[i]) 
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC MODEL_NAMES GET_GANG_CHASE_VEHICLE_MODEL(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, STRUCT_GANG_CHASE_OPTIONS &gangChaseOptions)
	INT index
	SWITCH serverGangChaseData.iGangChaseType
		CASE GANG_CHASE_TYPE_LAND		
			index = serverGangChaseData.iNextGangChaseIndex
			IF gangChaseOptions.mGangChaseVehCustom[index] != DUMMY_MODEL_FOR_SCRIPT
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					IF serverGangChaseData.mCustomVeh != gangChaseOptions.mGangChaseVehCustom[index]
						serverGangChaseData.mCustomVeh = gangChaseOptions.mGangChaseVehCustom[index]
						PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] serverGangChaseData.mCustomVeh updated to ", serverGangChaseData.mCustomVeh)
					ENDIF
				ENDIF
				
				RETURN gangChaseOptions.mGangChaseVehCustom[index]
			ELSE
				RETURN gangChaseOptions.mGangChaseVeh		
			ENDIF
		BREAK
		CASE GANG_CHASE_TYPE_SEA		RETURN gangChaseOptions.mGangChaseVehSea	
	ENDSWITCH
	
	RETURN gangChaseOptions.mGangChaseVeh
ENDFUNC

FUNC INT GET_NUMBER_OF_PEDS_IN_GANG_CHASE_VEH(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, STRUCT_GANG_CHASE_OPTIONS &gangChaseOptions)
	INT index = serverGangChaseData.iNextGangChaseIndex
	IF gangChaseOptions.iNumPedsInEachVehCustom[index] > 0
		RETURN gangChaseOptions.iNumPedsInEachVehCustom[index]
	ENDIF
	
	
	RETURN gangChaseOptions.iNumPedsInEachVeh
ENDFUNC
/// PURPOSE:
///    Server copy of the gang chase options. Update them when the options change
/// PARAMS:
///    serverGangChaseData - 
///    gangChaseOptions - 
PROC MAINTAIN_GANG_CHASE_OPTIONS_SERVER(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, STRUCT_GANG_CHASE_OPTIONS &gangChaseOptions)
	INT i
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(gangChaseOptions.niPedToAttack)
		IF serverGangChaseData.niTargetPed != gangChaseOptions.niPedToAttack
			serverGangChaseData.niTargetPed = gangChaseOptions.niPedToAttack
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]		niTargetPed PED_INDEX ", NATIVE_TO_INT(NET_TO_PED(gangChaseOptions.niPedToAttack)))	
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(gangChaseOptions.niVehToAttack)
		IF serverGangChaseData.niTargetVeh != gangChaseOptions.niVehToAttack
			serverGangChaseData.niTargetVeh = gangChaseOptions.niVehToAttack
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]		niTargetVeh VEH_INDEX ", NATIVE_TO_INT(NET_TO_VEH(gangChaseOptions.niVehToAttack)))	
		ENDIF
	ENDIF
	
	REPEAT MAX_GANG_CHASE_PER_WAVE i
		IF NOT ARE_VECTORS_EQUAL(gangChaseOptions.vCoordTarget[i], << 0.0, 0.0, 0.0>>)
			IF NOT ARE_VECTORS_EQUAL(gangChaseOptions.vCoordTarget[i], serverGangChaseData.structGangChaseUnit[i].vTargetCoord)	
				serverGangChaseData.structGangChaseUnit[i].vTargetCoord = gangChaseOptions.vCoordTarget[i]
				PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]		serverGangChaseData.structGangChaseUnit[", i, "].vTargetCoord = ", serverGangChaseData.structGangChaseUnit[i].vTargetCoord)
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	IF NOT ARE_VECTORS_EQUAL(gangChaseOptions.vOverrideDirectionVector, << 0.0, 0.0, 0.0>>)
		IF NOT ARE_VECTORS_EQUAL(gangChaseOptions.vOverrideDirectionVector, serverGangChaseData.vOverrideDirectionVector)	
			serverGangChaseData.vOverrideDirectionVector = gangChaseOptions.vOverrideDirectionVector
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]		serverGangChaseData.vOverrideDirectionVector = ", serverGangChaseData.vOverrideDirectionVector)
		ENDIF
	ENDIF
	
	IF gangChaseOptions.iDifficulty > 0
		IF serverGangChaseData.iDifficulty != gangChaseOptions.iDifficulty
			serverGangChaseData.iDifficulty = gangChaseOptions.iDifficulty
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE]		serverGangChaseData.iDifficulty = ", serverGangChaseData.iDifficulty)
		ENDIF
	ENDIF
	
	IF gangChaseOptions.mGangChaseVeh != DUMMY_MODEL_FOR_SCRIPT
		IF serverGangChaseData.mGangChaseVeh != gangChaseOptions.mGangChaseVeh
			serverGangChaseData.mGangChaseVeh = gangChaseOptions.mGangChaseVeh
		ENDIF
	ENDIF
	
	IF serverGangChaseData.iGangChaseOptionBitset != gangChaseOptions.iGangChaseOptionsBitset
		serverGangChaseData.iGangChaseOptionBitset = gangChaseOptions.iGangChaseOptionsBitset
	ENDIF
	
	IF serverGangChaseData.fFleeDistance != gangChaseOptions.fFleeDistance
		serverGangChaseData.fFleeDistance = gangChaseOptions.fFleeDistance
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS(STRUCT_GANG_CHASE_SERVER &serverGangChaseData)
	INT i, j
	VECTOR vCoords
	
	PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS]")
	PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS] ....")
	PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS]")
	REPEAT MAX_GANG_CHASE_PER_TARGET i
		PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS] 		Unit ", i)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GANG_CHASE_VEHICLE_NI(serverGangChaseData, i))
			vCoords = GET_ENTITY_COORDS(GET_GANG_CHASE_VEHICLE(serverGangChaseData, i), FALSE)
			IF IS_VEHICLE_DRIVEABLE(GET_GANG_CHASE_VEHICLE(serverGangChaseData, i))
				PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS] 		Veh drivable! Coords = ", vCoords)
			ELSE
				PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS] 		Veh NOT drivable! Coords = ", vCoords)
			ENDIF
		ELSE
			PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS] 		Veh doesn't exist!")
		ENDIF
		
		REPEAT MAX_PEDS_PER_GANG_CHASE_UNIT j
			PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS] 			Checking ped ", j)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GANG_CHASE_PED_NI(serverGangChaseData, i, j))
				vCoords = GET_ENTITY_COORDS(GET_GANG_CHASE_PED(serverGangChaseData, i, j), FALSE)
				IF NOT IS_PED_INJURED(GET_GANG_CHASE_PED(serverGangChaseData, i, j))
					PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS] 				Ped is Ok! Coords = ", vCoords)
				ELSE
					PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS] 				Ped is injured! Coords = ", vCoords)
				ENDIF
			ELSE
				PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS] 				Ped NI doesn't exist!")
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS] ")
	PRINTLN("[BIKER1]     ---------->   [GANG_CHASE_EXTRA_DEBUG] [DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS] DONE ")
ENDPROC
#ENDIF

/// PURPOSE:
///    Handle the server side tracking of the gang chase. Handles the creation of the gang chase vehs and peds and
///    determines when they should spawn
PROC MAINTAIN_GANG_CHASE_SERVER(STRUCT_GANG_CHASE_SERVER &serverGangChaseData, STRUCT_GANG_CHASE_OPTIONS &gangChaseOptions)
	
	INT iTemp
	INT i
	
	//-- Initialise
	IF NOT IS_GANG_CHASE_SERVER_BIT_SET(serverGangChaseData, GANG_CHASE_SERVER_INIT)
		
		REPEAT MAX_GANG_CHASE_PER_TARGET i
			serverGangChaseData.structGangChaseUnit[i].iTarget = -1
		ENDREPEAT
		
		SET_GANG_CHASE_SERVER_BIT(serverGangChaseData, GANG_CHASE_SERVER_INIT)
		
		PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] ")
		PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] Initialised gang chase with options....")
		IF gangChaseOptions.iTotalNumberOfWaves = 0
			gangChaseOptions.iTotalNumberOfWaves = 3
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		iTotalNumberOfWaves = ", gangChaseOptions.iTotalNumberOfWaves, " (DEFAULT)")
		ELSE
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		iTotalNumberOfWaves = ", gangChaseOptions.iTotalNumberOfWaves)
		ENDIF
		
		IF gangChaseOptions.iNumVehInEachWave = 0
			gangChaseOptions.iNumVehInEachWave = 2
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		iNumVehInEachWave = ", gangChaseOptions.iNumVehInEachWave, " (DEFAULT)")
		ELSE
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		iNumVehInEachWave = ", gangChaseOptions.iNumVehInEachWave)
		ENDIF
		
		IF gangChaseOptions.iNumPedsInEachVeh = 0
			gangChaseOptions.iNumPedsInEachVeh = 2
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		iNumPedsInEachVeh = ", gangChaseOptions.iNumPedsInEachVeh, " (DEFAULT)")
		ELSE
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		iNumPedsInEachVeh = ", gangChaseOptions.iNumPedsInEachVeh)
		ENDIF
		IF gangChaseOptions.iTimeBetweenWaves = 0
			gangChaseOptions.iTimeBetweenWaves = 10000
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		iTimeBetweenWaves = ", gangChaseOptions.iTimeBetweenWaves, " (DEFAULT)")
		ELSE
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		iTimeBetweenWaves = ", gangChaseOptions.iTimeBetweenWaves)
		ENDIF

		IF gangChaseOptions.mGangChaseVeh = DUMMY_MODEL_FOR_SCRIPT
			gangChaseOptions.mGangChaseVeh = BATI
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		mGangChaseVeh = ", ENUM_TO_INT(BATI), " (DEFAULT - BATI)")
		ELSE
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		mGangChaseVeh = ", ENUM_TO_INT(gangChaseOptions.mGangChaseVeh))
		ENDIF
		IF gangChaseOptions.mGangChasePed = DUMMY_MODEL_FOR_SCRIPT
			gangChaseOptions.mGangChasePed = G_M_Y_Korean_01
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		mGangChasePed = ", ENUM_TO_INT(G_M_Y_Korean_01), " (DEFAULT - mGangChasePed)")
		ELSE
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		mGangChasePed = ", ENUM_TO_INT(gangChaseOptions.mGangChasePed))
		ENDIF
		
		IF gangChaseOptions.fFleeDistance = 0
			gangChaseOptions.fFleeDistance = 200
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		fFleeDistance = ", gangChaseOptions.fFleeDistance, " (DEFAULT)")
		ELSE
			PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		fFleeDistance = ", gangChaseOptions.fFleeDistance)
		ENDIF
		
		
		i = 0
		REPEAT MAX_GANG_CHASE_PER_WAVE i
			IF gangChaseOptions.iNumPedsInEachVehCustom[i] = 0
				gangChaseOptions.iNumPedsInEachVehCustom[i] = -1
				PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		gangChaseOptions.iNumPedsInEachVehCustom[", i, "] = ", gangChaseOptions.iNumPedsInEachVehCustom[i]) 
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] 		Callstack....")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] ")
	ENDIF
	
	
	MAINTAIN_GANG_CHASE_OPTIONS_SERVER(serverGangChaseData, gangChaseOptions)
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
		SET_GANG_CHASE_ALLOWED_TO_TRIGGER_SERVER(serverGangChaseData)
	ENDIF
	#ENDIF
		
	IF IS_GANG_CHASE_ALLOWED_TO_TRIGGER(serverGangChaseData)
		IF NOT ARE_VECTORS_EQUAL(serverGangChaseData.vDropOffToAvoid, gangChaseOptions.vDropffCoord)
			IF NOT ARE_VECTORS_EQUAL(gangChaseOptions.vDropffCoord, << 0.0, 0.0, 0.0 >> )
				serverGangChaseData.vDropOffToAvoid = gangChaseOptions.vDropffCoord
				PRINTLN("[BIKER1]     ---------->  [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] vDropOffToAvoid = ", serverGangChaseData.vDropOffToAvoid)
			ENDIF
		ENDIF
		
		//-- Figure out which player to chase
		INT iPartToChase 	= -1
		INT iGangChaseType 	= -1
		
		IF serverGangChaseData.iGangChaseTargetPart < 0
			INIT_PART_TO_CHASE(serverGangChaseData)
		ENDIF

		
		iPartToChase 	= serverGangChaseData.iGangChaseTargetPart	
		iGangChaseType 	= serverGangChaseData.iGangChaseType
		
		PLAYER_INDEX playerToChase
		INT iPlayer
		
		MODEL_NAMES mVeh
		MODEL_NAMES mPed
		
		IF iPartToChase > -1
			IF NOT CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE(serverGangChaseData, iGangChaseType)
				//-- Only create a gang chase if not all of the current wave haven't been created 
				IF serverGangChaseData.iNumberOfWavesCreated[iGangChaseType] < GET_TOTAL_NUMBER_OF_GANG_CHASE_WAVES(gangChaseOptions)
				OR GET_TOTAL_NUMBER_OF_GANG_CHASE_WAVES(gangChaseOptions) = -1
				
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToChase))
						IF GET_NUMBER_OF_GANG_CHASE_CHASING_PART(serverGangChaseData, iPartToChase) <= MAX_GANG_CHASE_PER_TARGET
							IF serverGangChaseData.iNextGangChaseIndex = -1
								iTemp = GET_NEXT_FREE_GANG_CHASE_INDEX(serverGangChaseData)
								IF iTemp > -1
									serverGangChaseData.iNextGangChaseIndex = iTemp
									PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] serverBD.iNextGangChaseIndex = ", serverGangChaseData.iNextGangChaseIndex)
								ELSE
									PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] [GET_NEXT_FREE_GANG_CHASE_INDEX] <= -1!")
								ENDIF
							ELSE
								playerToChase = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToChase))
								iPlayer = NATIVE_TO_INT(playerToChase)
								
								IF NOT ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords, << 0.0, 0.0, 0.0>>)
									IF ARE_VECTORS_EQUAL(serverGangChaseData.vLastSpawnCoords, << 0.0, 0.0, 0.0 >>)
									OR (NOT ARE_VECTORS_EQUAL(serverGangChaseData.vLastSpawnCoords, GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords))
										
										//-- Spawn location and heading is calculated client side by whichever client the gang chase is going to chase
										VECTOR vSpawn = GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnCoords
										FLOAT fHeading = GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.gangChaseClient.vGangChaseSpawnRot.z
										
										//-- Ped veh model and number of peds per vehicle are set via the Options struct
										mVeh = GET_GANG_CHASE_VEHICLE_MODEL(serverGangChaseData, gangChaseOptions)
										mPed = gangChaseOptions.mGangChasePed 
										INT iPedsPerVeh = GET_NUMBER_OF_PEDS_IN_GANG_CHASE_VEH(serverGangChaseData, gangChaseOptions)

										IF gangChaseOptions.iPedAccuracy = 0
											gangChaseOptions.iPedAccuracy = 10
										ENDIF
										
										IF ENUM_TO_INT(gangChaseOptions.relGangChase) = 0
											PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] gangChaseOptions.relGangChase = NULL")
											gangChaseOptions.relGangChase = rgFM_AiHate
										ELSE
											PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] gangChaseOptions.relGangChase != NULL = ", ENUM_TO_INT(gangChaseOptions.relGangChase))
										ENDIF
										IF CREATE_GANG_CHASE_PED_AND_VEHICLE(serverGangChaseData, gangChaseOptions, vSpawn, fHeading, iPedsPerVeh, mVeh,  mPed)
											serverGangChaseData.iNumberOfUnitsCreated++
											serverGangChaseData.vLastSpawnCoords = vSpawn
											serverGangChaseData.mCustomVeh = DUMMY_MODEL_FOR_SCRIPT
											serverGangChaseData.structGangChaseUnit[serverGangChaseData.iNextGangChaseIndex].iGangChaseUnitType = iGangChaseType
											SET_BIT(serverGangChaseData.structGangChaseUnit[serverGangChaseData.iNextGangChaseIndex].iGangChaseUnitBitset, GANG_CHASE_UNIT_CREATED)
											CLEAR_BIT(serverGangChaseData.iGangChaseFleeBitset, serverGangChaseData.iNextGangChaseIndex)
											serverGangChaseData.structGangChaseUnit[serverGangChaseData.iNextGangChaseIndex].iTarget = serverGangChaseData.iGangChaseTargetPart 
											
											PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] Created gang chase index ", serverGangChaseData.iNextGangChaseIndex, " at coords ", vSpawn, " Type = ", iGangChaseType, " Number created in total = ",serverGangChaseData.iNumberOfUnitsCreated , " Timestamp: ", GET_CLOUD_TIME_AS_INT())
											PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] Set serverGangChaseData.structGangChaseUnit[", serverGangChaseData.iNextGangChaseIndex, "].iTarget = ", serverGangChaseData.iGangChaseTargetPart)
											
											
											START_NET_TIMER(serverGangChaseData.structGangChaseUnit[serverGangChaseData.iNextGangChaseIndex].timeCreation)
											serverGangChaseData.iGangChaseTargetPart = -1
											serverGangChaseData.iNextGangChaseIndex = -1
											
											INT iMaxVehPerWave = GET_MAX_NUMBER_OF_GANG_CHASE_VEHS_PER_WAVE(gangChaseOptions)
											INT iNumActiveOfType = GET_NUMBER_OF_ACTIVE_GANG_CHASE_UNITS_OF_TYPE(serverGangChaseData, iGangChaseType)
											IF  iNumActiveOfType = iMaxVehPerWave
												//-- All of the current wave has been created
												serverGangChaseData.iNumberOfWavesCreated[iGangChaseType]++
												PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] SET GANG_CHASE_SERVER_CREATED_ALL_LAND_UNITS_FOR_CURRENT_WAVE as ", iMaxVehPerWave, " currently active. iNumberOfWavesCreated[", iGangChaseType, "] =  ", serverGangChaseData.iNumberOfWavesCreated[iGangChaseType])
										//		SET_BIT(serverGangChaseData.iServerGangChaseBitset, GANG_CHASE_SERVER_CREATED_ALL_LAND_UNITS_FOR_CURRENT_WAVE)
												SET_CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE(serverGangChaseData, iGangChaseType) 
											ELSE
												IF iGangChaseType = 0
													PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] Not updating number of waves created as iNumActiveOfType = ", iNumActiveOfType, " != iMaxVehPerWave  = ", iMaxVehPerWave)
												ENDIF
											ENDIF
										ENDIF
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] Chosen part to chase ", iPartToChase, " not active! Will reset...")
						serverGangChaseData.iGangChaseTargetPart = -1
						serverGangChaseData.iNextGangChaseIndex = -1
					ENDIF
				ELSE
					PRINTLN("gangchasespam - serverGangChaseData.iNumberOfWavesCreated[iGangChaseType] >= GET_TOTAL_NUMBER_OF_GANG_CHASE_WAVES(gangChaseOptions)")
					PRINTLN("gangchasespam - OR GET_TOTAL_NUMBER_OF_GANG_CHASE_WAVES(gangChaseOptions) != -1")
				ENDIF
			ELSE
				PRINTLN("gangchasespam - CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE(serverGangChaseData, iGangChaseType)")
			ENDIF
		ELSE
			PRINTLN("gangchasespam - part to chase -1")
		ENDIF
		
		i = 0
		
		REPEAT GANG_CHASE_TYPE_MAX i
			//-- Time between waves
			IF NOT HAS_NET_TIMER_STARTED(serverGangChaseData.timeGangChaseWaveCleanedUp[i])
				IF CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE(serverGangChaseData, i)
				
					//-- Created all units in current wave
					IF GET_NUMBER_OF_ACTIVE_GANG_CHASE_UNITS_OF_TYPE(serverGangChaseData, i) = 0
						serverGangChaseData.iNumberOfWavesCleared++
						START_NET_TIMER(serverGangChaseData.timeGangChaseWaveCleanedUp[i])
						SET_BIT(serverGangChaseData.iServerGangChaseBitset, GANG_CHASE_SERVER_CURRENT_WAVE_CLEARED)
						PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] Started timeGangChaseWaveCleanedUp[", i, "]")
					ENDIF
				ENDIF
			ELSE
				INT iTimeBetweenWaves = GET_TIME_BETWEEN_GANG_CHASE_WAVES_FOR_VARIATION(gangChaseOptions)
				IF HAS_NET_TIMER_EXPIRED(serverGangChaseData.timeGangChaseWaveCleanedUp[i], iTimeBetweenWaves)
					RESET_NET_TIMER(serverGangChaseData.timeGangChaseWaveCleanedUp[i])
				//	CLEAR_BIT(serverGangChaseData.iServerGangChaseBitset, GANG_CHASE_SERVER_CREATED_ALL_LAND_UNITS_FOR_CURRENT_WAVE)
					CLEAR_CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE(serverGangChaseData, i)
					CLEAR_BIT(serverGangChaseData.iServerGangChaseBitset, GANG_CHASE_SERVER_CURRENT_WAVE_CLEARED)
					PRINTLN("[BIKER1]     ---------->   [GANG_CHASE] [MAINTAIN_GANG_CHASE_SERVER] Clear CLEAR_CREATED_ALL_UNITS_FOR_CURRENT_WAVE_TYPE for type ", i, " as ", iTimeBetweenWaves, " seconds expired")
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	INT iGangChase
	REPEAT MAX_GANG_CHASE iGangChase
		MAINTAIN_GANG_CHASE_MAX_PARTICIPANT_CHECK_SERVER(serverGangChaseData, iGangChase)
		PROCESS_GANG_CHASE_BRAIN(serverGangChaseData, iGangChase)
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		//INT iDebug
		FLOAT fY = 0.2
		INT iDbNumActiveUnits[3]
	//	INT iDbMaxVehPerWave = GET_MAX_NUMBER_OF_GANG_CHASE_VEHS_PER_WAVE(gangChaseOptions)
		INT iDbTotalNumWaves = GET_TOTAL_NUMBER_OF_GANG_CHASE_WAVES(gangChaseOptions)
		iDbNumActiveUnits[0] = GET_NUMBER_OF_ACTIVE_GANG_CHASE_UNITS_OF_TYPE(serverGangChaseData, 0)
		iDbNumActiveUnits[1] = GET_NUMBER_OF_ACTIVE_GANG_CHASE_UNITS_OF_TYPE(serverGangChaseData, 1)
		iDbNumActiveUnits[2] = GET_NUMBER_OF_ACTIVE_GANG_CHASE_UNITS_OF_TYPE(serverGangChaseData, 2)
		IF NOT serverGangChaseData.bWdToggleOnScreenDebug
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1, KEYBOARD_MODIFIER_SHIFT, "SKIP")
				serverGangChaseData.bWdToggleOnScreenDebug = TRUE
			ENDIF
		ELSE
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1, KEYBOARD_MODIFIER_SHIFT, "SKIP")
				serverGangChaseData.bWdToggleOnScreenDebug = FALSE
			ENDIF
		ENDIF
		
		IF serverGangChaseData.bWdToggleOnScreenDebug 
			DEBUG_GB_GANG_CHASE_OUTPUT_ALL_ACTIVE_UNITS(serverGangChaseData)
			
		//	DISPLAY_GB_GANG_CHASE_STRING_AND_NUMBER(0.1, fY, "Max Units Per Wave", iDbMaxVehPerWave)
			DISPLAY_GB_GANG_CHASE_STRING_AND_NUMBER(0.1, fY, "Total num waves ", iDbTotalNumWaves)
			DISPLAY_GB_GANG_CHASE_STRING_AND_NUMBER(0.1, fY, "Units Created", serverGangChaseData.iNumberOfUnitsCreated)
			
			
			DISPLAY_GB_GANG_CHASE_STRING_AND_NUMBER(0.1, fY, "# Land Waves Created", serverGangChaseData.iNumberOfWavesCreated[0])
			DISPLAY_GB_GANG_CHASE_STRING_AND_NUMBER(0.1, fY, "# Sea Waves Created", serverGangChaseData.iNumberOfWavesCreated[1])
			DISPLAY_GB_GANG_CHASE_STRING_AND_NUMBER(0.1, fY, "# Air Waves Created", serverGangChaseData.iNumberOfWavesCreated[2])
			DISPLAY_GB_GANG_CHASE_STRING_AND_NUMBER(0.1, fY, "# Land Unit Active", iDbNumActiveUnits[0])
			DISPLAY_GB_GANG_CHASE_STRING_AND_NUMBER(0.1, fY, "# Sea Unit Active", iDbNumActiveUnits[1])
			DISPLAY_GB_GANG_CHASE_STRING_AND_NUMBER(0.1, fY, "# Air Unit active", iDbNumActiveUnits[2])
			
		ENDIF
	#ENDIF
ENDPROC
 // FEATURE_BIKER
