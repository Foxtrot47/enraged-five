//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////        Deals with the Corona in FM	   		/////////////////////////////
//////////////////////////////           	  James Adwick        	    	//////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "FMMC_Corona_Anim_Scene.sch"
USING "net_simple_interior.sch"

CONST_FLOAT cfCORONA_CAM_BESPOKE_FOV			36.777779
CONST_FLOAT cfCORONA_CAM_BESPOKE_NO_TEAM_FOV	34.866665
CONST_FLOAT cfCORONA_CAM_FOV					45.0

// Light Rig Adjustments
TWEAK_FLOAT	tfCORONA_BESPOKE_LIGHT_RIG_ROTATION			0.0
TWEAK_FLOAT tfCORONA_BESPOKE_LIGHT_RIG_COORD_X			0.0
TWEAK_FLOAT tfCORONA_BESPOKE_LIGHT_RIG_COORD_Y			0.0
TWEAK_FLOAT tfCORONA_BESPOKE_LIGHT_RIG_COORD_Z			0.0

CONST_INT ciCORONA_VISIBLE_PEDS	5

CONST_FLOAT cfCORONA_ARENA_WARS_SCENE_ROTATION			270.0

// **************************************
// 		NEW CORONA PLAYER POSITIONING
// **************************************

/// PURPOSE: Returns where our corona placement begins (based on where player actually is)
FUNC FLOAT GET_PLAYER_CORONA_ORIGIN_OFFSET(INT iNumSlots)
	RETURN GET_CORONA_SLOT_ANGLE(MPGlobals.coronaPosData.iCoronaPosition, iNumSlots)
ENDFUNC

/// PURPOSE: Sets the team the player has been assigned   
PROC SET_MY_TEAM_IN_CORONA(INT iTeam)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = iTeam
ENDPROC

/// PURPOSE:
///    Wrapper for setting the team in the corona menu data. Also updates our cloned structure
PROC SET_PLAYER_CORONA_TEAM(CORONA_MENU_DATA &coronaMenuData, INT iSlot, INT iTeam, INT iPlayerIndex)
	
	PRINTLN("[CORONA] SET_PLAYER_CORONA_TEAM - Setting slot: ", iSlot, " to team: ", iTeam, ", player: ", iPlayerIndex)
	
	coronaMenuData.lobbyPlayers[iSlot].iTeam = iTeam
	
	g_CoronaClonePeds.piPlayersPed[iPlayerIndex].iClonedTeam = iTeam
ENDPROC

/// PURPOSE: Returns the players slot in the corona    
FUNC INT GET_MY_CORONA_SLOT(CORONA_MENU_DATA &coronaMenuData)
	RETURN coronaMenuData.iLocalPlayerSlot
ENDFUNC

/// PURPOSE: Returns the players slot in the corona    
PROC SET_MY_CORONA_SLOT(CORONA_MENU_DATA &coronaMenuData, INT iSlot)

	PRINTLN("[CORONA] SET_MY_CORONA_SLOT - Setting my slot to: ", iSlot)
	DEBUG_PRINTCALLSTACK()

	coronaMenuData.iLocalPlayerSlot = iSlot
ENDPROC

/// PURPOSE: Returns the INT value of who the player is looking at
FUNC INT GET_WHO_PLAYER_IS_LOOKING_AT_IN_CORONA(PLAYER_INDEX playerID)
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sClientCoronaData.iLookAtSlot		
ENDFUNC

FUNC BOOL IS_CORONA_A_BOAT_OR_AIR_RACE()
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
		IF IS_BOAT_RACE(g_FMMC_STRUCT.iRaceType)
		OR IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns an evaluated centre position of corona based on type 
FUNC VECTOR GET_CORONA_CENTRE_POSITION(VECTOR vStartLoc, VECTOR vAltStartLoc)

	IF IS_CORONA_A_BOAT_OR_AIR_RACE()
		RETURN vAltStartLoc
	ENDIF

	RETURN vStartLoc
ENDFUNC


FUNC INT GET_CORONA_SLOT_FOR_TEAM(CORONA_MENU_DATA &coronaMenuData, INT iTeam)

	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF coronaMenuData.iCoronaSlotLayout[i] = iTeam
			RETURN i
		ENDIF
		
	ENDREPEAT

	PRINTLN("[CORONA] GET_CORONA_SLOT_FOR_TEAM - NOT KNOWN WHICH SLOT TEAM: ", iTeam, " IS HELD IN!")
	RETURN 0
ENDFUNC


/// PURPOSE: Returns the name to display for the current viewing slot
FUNC STRING GET_CORONA_NAME_FOR_SLOT(CORONA_MENU_DATA &coronaMenuData, INT &iAnInt)

	iAnInt = -1

	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		IF IS_PLAYER_VIEWING_TUTORIAL_PED(coronaMenuData)
			RETURN GET_TUTORIAL_PED_NAME()
		ENDIF
	ENDIF
	
	IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
	AND NOT SHOULD_CORONA_TEAM_MISSION_SIMULATE_SINGLE_PLAYER_TEAMS()
	
		iAnInt = coronaMenuData.iCoronaSlotTarget + 1
		
		INT iTeam = GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData)) 
		
		RETURN GET_CORONA_NAME_FOR_TEAM(coronaMenuData, iAnInt, iTeam)
	ENDIF
	
	// Get player held in slot we are viewing
	RETURN GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[coronaMenuData.iCoronaSlotTarget].playerID)
ENDFUNC

/// PURPOSE:
///    Resets the positioning of the players gamertags so we get new x,y next iteration
PROC RESET_CORONA_GAMERTAG_POSITION(CORONA_PLAYER_DETAILS &playerDetails)	
	playerDetails.fNameX = -1.0
	playerDetails.fNameY = -1.0
ENDPROC


//FUNC INT GET_BETTING_INDEX_FOR_SLOT(CORONA_MENU_DATA &coronaMenuData)
//	
//	IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
//		RETURN GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData))
//	ENDIF	
//	
//	PLAYER_INDEX playerID = coronaMenuData.lobbyPlayers[GET_MY_CURRENT_TARGET_SLOT(coronaMenuData)].playerID
//	
//	RETURN NATIVE_TO_INT(playerID)
//ENDFUNC

/// PURPOSE: Returns the number of slots around the corona we will have for current Job
FUNC INT GET_NUM_CORONA_SLOTS(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)
	
	#IF IS_DEBUG_BUILD
		IF bDebugClonesForCoronaScene
			RETURN iDebugNumberClonesForScene+1
		ENDIF
	#ENDIF
	
	// Is the Job a team corona?
	IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)

		//...yes, return the correct amount based on the mission type
		IF sLaunchMissionDetails.iMissionType = FMMC_TYPE_DEATHMATCH
			RETURN (g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_NUM_TEAMS] + 2)		// Host setting
		ELIF sLaunchMissionDetails.iMissionType = FMMC_TYPE_MISSION
			IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSELECTABLE_TEAM_NUMBERS)
			AND g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS] > 0
				RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS]
			ELSE
				RETURN g_FMMC_STRUCT.iNumberOfTeams												// Specified by creator
			ENDIF
		ELIF sLaunchMissionDetails.iMissionType = FMMC_TYPE_RACE
			RETURN CEIL(TO_FLOAT(sLaunchMissionDetails.iMaxParticipants) * 0.5)				// Rally (max part / 2)
		ENDIF

	ENDIF
	
	INT iTotalSlot = GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION() //sLaunchMissionDetails.iMaxParticipants
	
	// ******** TUTORIAL SPECIAL CASE ********
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		iTotalSlot++
	ENDIF
	
	RETURN iTotalSlot																			// Max number of participants
ENDFUNC

/// PURPOSE: Returning the FOV of the camera for the corona
FUNC FLOAT GET_CORONA_CAMERA_FOV()
	
	IF IS_CORONA_BIT_SET(CORONA_HEIST_PLANNING_CAMERA)
		RETURN cf_HEIST_DEFAULT_CAMERA_FOV
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF IS_CORONA_BIT_SET(CORONA_GLOBAL_CASINO_HEIST_FINALE_FLAG)
		RETURN 22.0808
	ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_CORONA_BIT_SET(CORONA_GLOBAL_GENERIC_HEIST_FINALE_FLAG)
		RETURN 45.5
	ENDIF
	#ENDIF
	
	IF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ()
		RETURN 50.0
	ENDIF
	
	// Should we use a bespoke camera FOV
	IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
	
		// If one team, use adjusted FOC
		IF g_FMMC_STRUCT.iNumberOfTeams = 1
			RETURN cfCORONA_CAM_BESPOKE_NO_TEAM_FOV
		ENDIF
		
		RETURN cfCORONA_CAM_BESPOKE_FOV
	ENDIF
	
	RETURN cfCORONA_CAM_FOV
ENDFUNC

FUNC VECTOR CALC_SLOT_CENTRE_POSITION(VECTOR vCoronaCentre, FLOAT fHeading, FLOAT fRadius, BOOL bIgnoreMode = FALSE)

	VECTOR vTemp
	FLOAT fGroundZ

	IF (GET_CORONA_STATUS() = CORONA_STATUS_IN_CORONA OR GET_CORONA_STATUS() = CORONA_STATUS_BALANCE_AND_LOAD)
	AND g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
	AND NOT bIgnoreMode
		
		IF IS_BOAT_RACE(g_FMMC_STRUCT.iRaceType)
			vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaCentre, fHeading, <<0.0, 0.0, 2.0>>)
			
			IF TEST_VERTICAL_PROBE_AGAINST_ALL_WATER(vTemp, SCRIPT_INCLUDE_MOVER, fGroundZ) = SCRIPT_WATER_TEST_RESULT_WATER
				PRINTLN("[CORONA] CALC_SLOT_CENTRE_POSITION - fGroundZ for WATER = ", fGroundZ)
				vTemp.z = fGroundZ
			ENDIF
			
		ELIF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
			PRINTLN("[CORONA] CALC_SLOT_CENTRE_POSITION - just calculate position for air race")
			vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaCentre, fHeading, <<0.0, 0.0, 0.0>>)
		ELSE
			vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaCentre, fHeading, <<0.0, 0.0, 2.0>>)
			
			IF GET_GROUND_Z_FOR_3D_COORD(vTemp, fGroundZ)
				vTemp.z = fGroundZ
			ENDIF
		ENDIF
	ELSE
	
		IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
			PRINTLN("[CORONA] CALC_SLOT_CENTRE_POSITION - Team corona, use centre position: ", vCoronaCentre)
			vTemp = vCoronaCentre
		ELSE
			vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaCentre, fHeading, <<0.0, fRadius, 2.0>>)
			
			IF GET_GROUND_Z_FOR_3D_COORD(vTemp, fGroundZ)
				vTemp.z = fGroundZ
			ENDIF
			
			PRINTLN("[CORONA] CALC_SLOT_CENTRE_POSITION - Normal corona, get slot vector: ", vTemp)
		ENDIF
	ENDIF

	RETURN vTemp
ENDFUNC

FUNC INT CORONA_INT_SUM(INT iInt)
	INT i = 0
	INT iTotal = 0
	FOR i = 0 TO iInt
		iTotal += i
	ENDFOR
	
	RETURN iTotal
ENDFUNC

FUNC VECTOR GET_TEAM_CORONA_POSITION(VECTOR vCentralPos, INT &iRow, INT iCount, FLOAT fAngle, INT &iTeamPos)
	
	PRINTNL()
	PRINTLN("[CORONA] GET_TEAM_CORONA_POSITION - vCentralPos: ", vCentralPos, " iRow: ", iRow, " iCount: ", iCount)
	
	// We have switched 0 and 1 indexes around in array so no longer need to hard code the lookup of the partners position
//	IF IS_THIS_A_RALLY_RACE_TEMP(g_sTransitionSessionOptions)
//	OR IS_THIS_CORONA_A_GTA_TEAM_RACE(g_sTransitionSessionOptions)
//		IF iCount = 1
//			PRINTLN("[CORONA] GET_TEAM_CORONA_POSITION - Shift player by 1 so they are standing correct side")
//			iCount = 2
//		ENDIF
//	ENDIF
	
	IF iCount != 0	

		// Calc. the row we should be on
		INT iRowSum = CORONA_INT_SUM(iRow)
		INT iCalcRow = iCount - iRowSum			
		
		IF iCalcRow > iRow
			iRow++
			iRowSum = CORONA_INT_SUM(iRow)
			PRINTLN("[CORONA] 		 New Row: ", iRow)
		ENDIF		
		
		// Calculate number of slots on this row
		INT iNumberPos = (iRow + 1)								
		
		PRINTLN("[CORONA] 		 # Pos: ", iNumberPos)
		
		// Calculate our offsets in y, x directions
		FLOAT fYOffset = 0
		FLOAT fXOffset = -1.0		// Default options to make sure we are out of view from camera if our team exceeds 15
		
		IF iCount < 15
			fYOffset = g_vCoronaPedPositions[iCount-1].y //iRow * yCoronaTeamOffset
			fXOffset = g_vCoronaPedPositions[iCount-1].x //((iNumberPos - 1) * xCoronaTeamOffset) - ((iCount - iRowSum) * (2*xCoronaTeamOffset))
		ENDIF
		
		PRINTLN("[CORONA] 		 xOffset: ", fXOffset, " yOffset: ", fYOffset)
		
		
		IF (iCount - iRowSum) = 0
		OR (iCount - iRowSum) = (iNumberPos - 1)
		
			PRINTLN("[CORONA] 		 Team Row Pos: ", (iCount - iRowSum), ", Player is at CORONA_PLAYER_TEAM_POS_AT_EDGE")
		
			iTeamPos = CORONA_PLAYER_TEAM_POS_AT_EDGE
		ENDIF
		
																									// + GET_RANDOM_FLOAT_IN_RANGE(-0.22, 0.22)  + GET_RANDOM_FLOAT_IN_RANGE(-0.24, 0.24)
		vCentralPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCentralPos, fAngle, <<fXOffset, fYOffset, 2.0>>)
	
	ELSE
		iTeamPos = CORONA_PLAYER_TEAM_POS_AT_EDGE
		vCentralPos.z += 2.0
	ENDIF
	
	FLOAT fGroundZ
	IF GET_GROUND_Z_FOR_3D_COORD(vCentralPos, fGroundZ)
		vCentralPos.z = fGroundZ
	ENDIF
	
	PRINTLN("[CORONA] 		 New Pos: ", vCentralPos)
	PRINTNL()
	
	
	RETURN vCentralPos
ENDFUNC

PROC CALC_TEAM_CORONA_FORMATION_OFFSET(INT &iRow, INT iCount, FLOAT &fXOffset, FLOAT &fYOffset)
	
	INT iRowSum = CORONA_INT_SUM(iRow)
	INT iCalcRow = iCount - iRowSum	
	
	IF iCalcRow > iRow
		iRow++
		iRowSum = CORONA_INT_SUM(iRow)
		PRINTLN("[CORONA] 		 New Row: ", iRow)
	ENDIF		
	
	// Calculate number of slots on this row
	INT iNumberPos = (iRow + 1)								
	
	PRINTLN("[CORONA] 		 # Pos: ", iNumberPos)
	
	// Calculate our offsets in y, x directions
	fYOffset = iRow * yCoronaTeamOffset
	fXOffset = ((iNumberPos - 1) * xCoronaTeamOffset) - ((iCount - iRowSum) * (2*xCoronaTeamOffset))
ENDPROC

/// PURPOSE: Processes the data passed to calculate the centre position and heading of corona slot
/// PARAMS:
///    vPosition - Vector updated to slot position
///    fHeading - Heading of from slot position to camera
///    vCentrePosition - Centre of current corona
///    iMissionType - Type of mission
///    iNumSlots - Total number of slots in corona
PROC GET_CORONA_SLOT_POSITION_AND_ANGLE(VECTOR &vPosition, FLOAT &fHeading, INT iSlot, VECTOR vCentrePosition, INT iMissionType, INT iNumSlots, BOOL bIgnoreMode = FALSE)
	
	FLOAT fAngleDelta = GET_PLAYER_CORONA_ORIGIN_OFFSET(iNumSlots) + GET_CORONA_SLOT_ANGLE(iSlot, iNumSlots)
	FLOAT fRadius = GET_RADIUS_OF_CORONA(iMissionType)
	
	// Calculate our position
	vPosition = CALC_SLOT_CENTRE_POSITION(vCentrePosition, fAngleDelta, fRadius, bIgnoreMode)
	fHeading = GET_HEADING_FROM_VECTOR_2D(vCentrePosition.x - vPosition.x, vCentrePosition.y - vPosition.y)
	
	IF bIgnoreMode
		vPosition = CALC_SLOT_CENTRE_POSITION(vCentrePosition, fAngleDelta, fRadius)
	ENDIF
	
ENDPROC

PROC HIDE_PLAYERS_CLOSE_TO_SELECTION_CAMERA(CORONA_MENU_DATA &coronaMenuData)
	EXIT
	
	// Only apply the visibility if we are using the new layout of peds
	IF coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_SYNC_SCENE

		PED_INDEX aPed
		INT i
		
		SET_USE_HI_DOF()
			
		BOOL bPlayerValid
			
		//Hide players that intersect the camera
		FOR i = 0 TO (NUM_NETWORK_PLAYERS-1) STEP 1	
		
			bPlayerValid = FALSE
		
			IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
				bPlayerValid = TRUE
				aPed = g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF bDebugClonesForCoronaScene
				bPlayerValid = FALSE
			
				IF CORONA_SCENE_CHECK_VALID_PED(g_CoronaClonePeds.piPlayersPed[i].pedID)
					bPlayerValid = TRUE
					aPed = g_CoronaClonePeds.piPlayersPed[i].pedID
				ENDIF
			ENDIF
			#ENDIF

			IF bPlayerValid
				
				IF DOES_ENTITY_EXIST(aPed)
				AND NOT IS_ENTITY_DEAD(aPed)
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(aPed, GET_FINAL_RENDERED_CAM_COORD()) < 1.0
						
						IF coronaMenuData.lobbyPlayers[i].iTeam != GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData))
								
							SET_ENTITY_VISIBLE(aPed, FALSE)
							PRINTLN("HIDE_PLAYERS_CLOSE_TO_SELECTION_CAMERA - SET_ENTITY_VISIBLE(FALSE) - ", i)
						ENDIF
					ELSE
						SET_ENTITY_VISIBLE(aPed, TRUE)
						PRINTLN("HIDE_PLAYERS_CLOSE_TO_SELECTION_CAMERA - SET_ENTITY_VISIBLE(TRUE) - ", i)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF

ENDPROC

/// PURPOSE: Own clamping function for getting slot in range
FUNC INT CORONA_CAP_SLOT_IN_RANGE(INT iSlot, INT iMinSlot, INT iMaxSlot)

	IF iSlot >= iMinSlot
	AND iSlot < iMaxSlot
		RETURN iSlot
	ENDIF
	
	IF iSlot < iMinSlot
		RETURN (iMaxSlot + iSlot)
	ELIF iSlot >= iMaxSlot
		RETURN (iMinSlot + (iSlot - iMaxSlot))
	ENDIF

	RETURN iSlot
ENDFUNC

// Calculates the slots that we should be showing in the corona
PROC SET_CORONA_VISIBLE_PEDS(INT iSlot, INT iNumSlots, INT &iVisibleRange[])
	
	iVisibleRange[0] = iSlot
	iVisibleRange[1] = CORONA_CAP_SLOT_IN_RANGE(iSlot+1, 0, iNumSlots)
	iVisibleRange[2] = CORONA_CAP_SLOT_IN_RANGE(iSlot+2, 0, iNumSlots)
	iVisibleRange[3] = CORONA_CAP_SLOT_IN_RANGE(iSlot-1, 0, iNumSlots)
	iVisibleRange[4] = CORONA_CAP_SLOT_IN_RANGE(iSlot-2, 0, iNumSlots)
ENDPROC

// Checks to see if the player in the slot should be visible
FUNC BOOL IS_CORONA_PED_SLOT_IN_RANGE(INT iSlot, INT &iVisibleRange[])

	INT i
	REPEAT ciCORONA_VISIBLE_PEDS i
		IF iSlot = iVisibleRange[i]
			RETURN TRUE
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

/// PURPOSE: Sets the visibility of the player peds in the betting screen based off current slot
/// PARAMS:
///    iSlot - Slot currently being observed
PROC SET_CORONA_PED_VISIBILITY_FOR_SLOT(CORONA_MENU_DATA &coronaMenuData, INT iSlot, INT iNumberOfSlots)
	
	// Early exit if we do not need to worry about larger numbers
//	IF iNumberOfSlots < CORONA_MAX_PLAYER_ROWS
//		EXIT
//	ENDIF
	
	INT iPed
	INT iVisibleRange[ciCORONA_VISIBLE_PEDS]
	PLAYER_INDEX playerID
	SET_CORONA_VISIBLE_PEDS(iSlot, iNumberOfSlots, iVisibleRange)
	
	// ** DEBUG FUNCTIONALITY **
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		
		// Debug functionality
		IF g_bDebugCoronaLayoutGreaterThan16
			REPEAT NUM_NETWORK_PLAYERS iPed
		
				IF CORONA_SCENE_CHECK_VALID_PED(g_CoronaClonePeds.piPlayersPed[iPed].pedID)
					IF IS_CORONA_PED_SLOT_IN_RANGE(iPed, iVisibleRange)
						PRINTLN("[CORONA] SET_CORONA_PED_VISIBILITY_FOR_SLOT - g_bDebugCoronaLayoutGreaterThan16 - SET_ENTITY_VISIBLE(TRUE) - Visible for ped - ", iPed)
						SET_ENTITY_VISIBLE(g_CoronaClonePeds.piPlayersPed[iPed].pedID, TRUE)				
					ELSE
						PRINTLN("[CORONA] SET_CORONA_PED_VISIBILITY_FOR_SLOT - g_bDebugCoronaLayoutGreaterThan16 - SET_ENTITY_VISIBLE(FALSE) - NOT visible for ped - ", iPed)
						SET_ENTITY_VISIBLE(g_CoronaClonePeds.piPlayersPed[iPed].pedID, FALSE)	
					ENDIF
				ENDIF
			ENDREPEAT
			
			EXIT
		ENDIF
	#ENDIF
	
	PRINTLN("[CORONA] SET_CORONA_PED_VISIBILITY_FOR_SLOT - In Range slots: iVisibleRange[0] = ", iVisibleRange[0])
	PRINTLN("[CORONA] SET_CORONA_PED_VISIBILITY_FOR_SLOT - In Range slots: iVisibleRange[1] = ", iVisibleRange[1])
	PRINTLN("[CORONA] SET_CORONA_PED_VISIBILITY_FOR_SLOT - In Range slots: iVisibleRange[2] = ", iVisibleRange[2])
	PRINTLN("[CORONA] SET_CORONA_PED_VISIBILITY_FOR_SLOT - In Range slots: iVisibleRange[3] = ", iVisibleRange[3])
	PRINTLN("[CORONA] SET_CORONA_PED_VISIBILITY_FOR_SLOT - In Range slots: iVisibleRange[4] = ", iVisibleRange[4])
	
	// Loop over all players in our lobby array
	REPEAT NUM_NETWORK_PLAYERS iPed
	
		playerID = coronaMenuData.lobbyPlayers[iPed].playerID
		IF playerID != INVALID_PLAYER_INDEX()
			
			// If they have a ped generated
			IF CORONA_SCENE_CHECK_VALID_PED(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(playerID)].pedID)
			
				// If the ped is in a slot that is valid then we should set them to visible, else hide
				IF IS_CORONA_PED_SLOT_IN_RANGE(coronaMenuData.lobbyPlayers[iPed].iSlotAssigned, iVisibleRange)
					PRINTLN("[CORONA] SET_CORONA_PED_VISIBILITY_FOR_SLOT - SET_ENTITY_VISIBLE(TRUE) - Visible for ped - ", NATIVE_TO_INT(playerID), " iSlotAssigned = ", coronaMenuData.lobbyPlayers[iPed].iSlotAssigned)
					SET_ENTITY_VISIBLE(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(playerID)].pedID, TRUE)				
				ELSE
					PRINTLN("[CORONA] SET_CORONA_PED_VISIBILITY_FOR_SLOT - SET_ENTITY_VISIBLE(FALSE) - NOT Visible for ped - ", NATIVE_TO_INT(playerID), " iSlotAssigned = ", coronaMenuData.lobbyPlayers[iPed].iSlotAssigned)
					SET_ENTITY_VISIBLE(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(playerID)].pedID, FALSE)
				ENDIF
			ENDIF
		ENDIF		
	ENDREPEAT
	
ENDPROC

//PURPOSE: Sets the selected Team as visible
PROC SET_SELECTED_TEAM_VISIBLE(CORONA_MENU_DATA &coronaMenuData, MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails, BOOL bAllInvisible = FALSE)

	// Disable visibility flipping for heist finales.
	IF IS_CORONA_BIT_SET(CORONA_HEIST_SINGLE_TEAM_OUTFITS)
		EXIT
	ENDIF

	IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
	
		DEBUG_PRINTCALLSTACK()
	
		INT i, iCounter
		REPEAT NUM_NETWORK_PLAYERS i
			IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			AND IS_NET_PLAYER_OK(coronaMenuData.lobbyPlayers[i].playerID, FALSE, TRUE)
			
				IF coronaMenuData.lobbyPlayers[i].iTeam = GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData))
				AND NOT bAllInvisible
				AND NOT IS_BIT_SET(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].iCoronaCloneFlags, ciCORONA_CLONE_INVISIBLE)
				AND iCounter <= 15
					SET_ENTITY_VISIBLE(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID, TRUE)
					PRINTLN("[CORONA] SET_SELECTED_TEAM_VISIBLE - SET_ENTITY_VISIBLE(TRUE) -Visible for ped - ", NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID))
					iCounter++
				ELSE
					SET_ENTITY_VISIBLE(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID, FALSE)
					PRINTLN("[CORONA] SET_SELECTED_TEAM_VISIBLE - SET_ENTITY_VISIBLE(FALSE) - NOT Visible for ped - ", NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID))
				ENDIF
			ENDIF
		ENDREPEAT
		
	//Do Non-Team just incase
	ELSE
		SET_CORONA_PED_VISIBILITY_FOR_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData), GET_NUM_CORONA_SLOTS(sLaunchMissionDetails))
	ENDIF
	
ENDPROC

/// PURPOSE: Command for placement of players clone within the corona
PROC PLACE_PED_IN_CORONA_POSITION(	CORONA_MENU_DATA &coronaMenuData, PED_INDEX &pedIndex, INT &iTeamPos, INT iCount, VECTOR vCentrePosition, INT iMissionType, INT iNumPlayers, INT &iTeamMemberCount, 
									INT &iTeamRow, INT iTeam, INT iTeamPosIndex, BOOL bForcePedUpdate = TRUE, BOOL bRefreshAnim = TRUE, PED_INDEX aPedForAnimRefresh = NULL, BOOL bUseGenericAnim = FALSE) //, BOOL bLocalPlayer = FALSE)

	// Reset this before placing
	iTeamPos = CORONA_PLAYER_TEAM_POS_INSIDE
	
	IF DOES_ENTITY_EXIST(pedIndex)
		IF NOT IS_PED_INJURED(pedIndex)
			
			// Always want to get player origin offset (everything relative to where you are 'effectively' standing)
			FLOAT fAngleDelta = GET_PLAYER_CORONA_ORIGIN_OFFSET(iNumPlayers) + GET_CORONA_SLOT_ANGLE(iCount, iNumPlayers) //   (-iCount * (360.0 / iNumPlayers))
			FLOAT fRadius = GET_RADIUS_OF_CORONA(iMissionType)
			
			VECTOR vPosition 
			FLOAT fHeading
			
			IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
				vPosition = vCentrePosition	//CALC_SLOT_CENTRE_POSITION(vCentrePosition, fAngleDelta, 0)	//NO OFFSET
				fHeading = (fAngleDelta - 180)
				PRINTLN("[CORONA] 	- NO OFFSET FOR TEAM CENTRE")
			ELSE
				vPosition = CALC_SLOT_CENTRE_POSITION(vCentrePosition, fAngleDelta, fRadius)
				fHeading = GET_HEADING_FROM_VECTOR_2D(vCentrePosition.x - vPosition.x, vCentrePosition.y - vPosition.y)
			ENDIF
			
			INT iPlayerTeam
			
			IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
				
				iTeamPosIndex = iTeamPosIndex
				
				// If we are running a bespoke corona placement system, grab the coordinates based on team and slot
				IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
					
					iPlayerTeam = iTeam
					IF IS_CORONA_BIT_SET(CORONA_NO_TEAM_OUTFITS)
						PRINTLN("[CORONA] 	- No teams for bespoke corona, Set to -1")
						iPlayerTeam = -1
					ENDIF
				
					PRINTLN("[CORONA] - Property - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
					PRINTLN("[CORONA] - iTeamPosIndex = ", iTeamPosIndex)				
				
					vPosition 	= GET_CORONA_PED_POSITION_FOR_TEAM(iPlayerTeam, iTeamPosIndex, fHeading)
				ELSE
				
					vPosition = GET_TEAM_CORONA_POSITION(vPosition, iTeamRow, iTeamMemberCount, fAngleDelta, iTeamPos)
				
				ENDIF
			ENDIF
			
			#IF FEATURE_CASINO_HEIST
			IF IS_CORONA_BIT_SET(CORONA_GLOBAL_CASINO_HEIST_FINALE_FLAG)
				GET_CASINO_HEIST_PED_POSITION(iTeamPosIndex, vPosition, fHeading)
				PRINTLN("[CORONA] [CASINO_HEIST] PLACE_PED_IN_CORONA_POSITION - grabbing casino heist positions:	iSlot = ", iCount, ", position = ", vPosition, ", fHeading = ", fHeading)
			ENDIF
			#ENDIF
			
			#IF FEATURE_HEIST_ISLAND
			IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_MISSION_FINALE()
				GET_HEIST_ISLAND_PED_POSITION(iTeamPosIndex, vPosition, fHeading)
				PRINTLN("[CORONA] [CASINO_HEIST] PLACE_PED_IN_CORONA_POSITION - grabbing heist island positions:	iSlot = ", iCount, ", position = ", vPosition, ", fHeading = ", fHeading)
			ENDIF
			#ENDIF
			
			IF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ()
				GET_FIXER_STORY_PED_POSITION(iTeamPosIndex, vPosition, fHeading)
			ENDIF
			
			PRINTLN("[CORONA] 	- iSlot = ", iCount, ", position = ", vPosition, ", fHeading = ", fHeading)
						
			SET_ENTITY_COORDS(pedIndex, vPosition, DEFAULT, TRUE)
			SET_ENTITY_HEADING(pedIndex, fHeading)

			SET_ENTITY_COLLISION(pedIndex, FALSE)
			FREEZE_ENTITY_POSITION(pedIndex, TRUE)
			
			// Disable visibility flipping for heist finales.
			IF NOT IS_CORONA_BIT_SET(CORONA_HEIST_SINGLE_TEAM_OUTFITS)
				IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
					IF iTeam = GET_MY_TEAM_IN_CORONA()
					AND iTeamMemberCount <= 15
						SET_ENTITY_VISIBLE(pedIndex, TRUE)
						PRINTLN("[CORONA] 	- SET_ENTITY_VISIBLE(TRUE) - THIS PLAYER VISIBLE")
					ELSE
						SET_ENTITY_VISIBLE(pedIndex, FALSE)
						PRINTLN("[CORONA] 	- SET_ENTITY_VISIBLE(FALSE) - THIS PLAYER NOT VISIBLE")
					ENDIF
				ELSE
					SET_ENTITY_VISIBLE(pedIndex, TRUE)
				ENDIF
			ELSE
				SET_ENTITY_VISIBLE(pedIndex, TRUE)
				PRINTLN("[CORONA] 	- SET_ENTITY_VISIBLE(TRUE) - THIS PLAYER VISIBLE - NOT TEAM")
			ENDIF
			
			FINALIZE_HEAD_BLEND(pedIndex)
			IF bForcePedUpdate
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIndex, TRUE)
			ENDIF
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
			
			
			//IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
			//	IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
					IF bRefreshAnim
					OR pedIndex = aPedForAnimRefresh
						
						SET_CORONA_PED_ANIMATION(coronaMenuData, pedIndex, iPlayerTeam, iTeamPosIndex, bUseGenericAnim)
						SET_CORONA_BIT(CORONA_PEDS_HAVE_ANIMATION_PLAYING)
					ENDIF
			//	ENDIF
			//ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Clear all the idle movement before we make the switch. Should hide this in the camera frame.
PROC CLEAR_PEDS_OF_CORONA_IDLE_MOVEMENT(CORONA_MENU_DATA &coronaMenuData)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
	
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
		
			IF CORONA_SCENE_CHECK_VALID_PED(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID)
				CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID, AAT_IDLE, INSTANT_BLEND_OUT)
				FORCE_PED_MOTION_STATE(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID, MS_ON_FOOT_IDLE, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID)
			ENDIF
		ENDIF
	
	ENDREPEAT
	
ENDPROC

PROC RESET_LOCAL_PLAYER_CORONA_PED()
		
	PRINTLN("[CORONA] RESET_LOCAL_PLAYER_CORONA_PED - SET_ENTITY_VISIBLE(ped, FALSE)")
		
	IF DOES_ENTITY_EXIST(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID)
		IF NOT IS_PED_INJURED(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID)
			SET_ENTITY_VISIBLE(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID, FALSE)
		ENDIF
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC DUMMY_OUR_TEAM_CORONA_LAYOUT()

	INT i
	VECTOR vPlayerPos 
	FLOAT fPlayerHeading
	
	IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE
		vPlayerPos = GET_ENTITY_COORDS(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID)
		fPlayerHeading = GET_ENTITY_HEADING(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID) + 180
	ELSE
		vPlayerPos = GET_ENTITY_COORDS(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID) //GET_PLAYER_COORDS(PLAYER_ID())
		fPlayerHeading = GET_ENTITY_HEADING(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID) //+ 180 //GET_ENTITY_HEADING(PLAYER_PED_ID())
	ENDIF
	
	TEXT_LABEL_63 tlPedName
	INT iCount
	FOR i = 0 TO (iDebugNumberClonesForScene) STEP 1
	
		IF NOT DOES_ENTITY_EXIST(g_CoronaClonePeds.piPlayersPed[i].pedID)
			PRINTLN("[CORONA] DUMMY_OUR_TEAM_CORONA_LAYOUT - Creating clone for slot ", i)
			tlPedName = "clone_"
			tlPedName += iCount
		
			g_CoronaClonePeds.piPlayersPed[i].pedID = CLONE_PED(PLAYER_PED_ID(), FALSE, FALSE, TRUE)
			
			//SETUP_MP_SPAWN_OUTFIT_MALE(g_CoronaClonePeds.piPlayersPed[i], INT_TO_ENUM(MP_SPAWN_OUTFIT_TYPE, GET_RANDOM_INT_IN_RANGE(0, 6)))
			
			iCount++
			
			SET_PED_NAME_DEBUG(g_CoronaClonePeds.piPlayersPed[i].pedID, tlPedName)
		ENDIF

	ENDFOR
	
	VECTOR vTeamPos = vPlayerPos
	INT iRow = 0
	INT iTeamPos
	iCount = 0
	
	GET_TEAM_CORONA_POSITION(vPlayerPos, iRow, iCount, fPlayerHeading, iTeamPos)
	iCount++
	
	FOR i = 0 TO (iDebugNumberClonesForScene) STEP 1
	
		IF i != NATIVE_TO_INT(PLAYER_ID())
	
			vTeamPos = GET_TEAM_CORONA_POSITION(vPlayerPos, iRow, iCount, fPlayerHeading, iTeamPos)
			
			
			SET_ENTITY_COORDS(g_CoronaClonePeds.piPlayersPed[i].pedID, vTeamPos)
			CLEAR_PED_TASKS_IMMEDIATELY(g_CoronaClonePeds.piPlayersPed[i].pedID)
			SET_ENTITY_COLLISION(g_CoronaClonePeds.piPlayersPed[i].pedID, FALSE)
			FREEZE_ENTITY_POSITION(g_CoronaClonePeds.piPlayersPed[i].pedID, TRUE)
			SET_ENTITY_VISIBLE(g_CoronaClonePeds.piPlayersPed[i].pedID, TRUE)
			PRINTLN("DUMMY_OUR_TEAM_CORONA_LAYOUT - SET_ENTITY_VISIBLE(TRUE) ", i)
			FINALIZE_HEAD_BLEND(g_CoronaClonePeds.piPlayersPed[i].pedID)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_CoronaClonePeds.piPlayersPed[i].pedID, TRUE)
			
			SET_ENTITY_HEADING(g_CoronaClonePeds.piPlayersPed[i].pedID, (fPlayerHeading + g_fCoronaPedHeadings[iCount-1]) - 180.0)
		
			iCount++
		ENDIF
	ENDFOR
ENDPROC

PROC DUMMY_DELETE_TEAM_CORONA_LAYOUT()

	INT i
	
	FOR i = 0 TO g_CoronaNumberOfPlayers STEP 1
		DELETE_PED(g_CoronaClonePeds.piPlayersPed[i].pedID)
	ENDFOR
ENDPROC

PROC DUMMY_REPOSITION_CORONA_TEAM_PEDS()

	INT i
	VECTOR vPlayerPos 
	FLOAT fPlayerHeading
	
	vPlayerPos = GET_ENTITY_COORDS(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID)
	fPlayerHeading = GET_ENTITY_HEADING(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID) + 180

	VECTOR vTeamPos = vPlayerPos
	INT iRow = 0
	INT iTeamPos
	INT iCount
	
	GET_TEAM_CORONA_POSITION(vPlayerPos, iRow, iCount, fPlayerHeading, iTeamPos)
	iCount++
	
	FOR i = 0 TO (iDebugNumberClonesForScene) STEP 1
	
		IF i != NATIVE_TO_INT(PLAYER_ID())
	
			vTeamPos = GET_TEAM_CORONA_POSITION(vPlayerPos, iRow, iCount, fPlayerHeading, iTeamPos)
						
			SET_ENTITY_COORDS(g_CoronaClonePeds.piPlayersPed[i].pedID, vTeamPos)
			SET_ENTITY_COLLISION(g_CoronaClonePeds.piPlayersPed[i].pedID, FALSE)
			
			#IF IS_DEBUG_BUILD
			IF NOT bDebugRefreshCoronaClones
			#ENDIF
				SET_ENTITY_VISIBLE(g_CoronaClonePeds.piPlayersPed[i].pedID, TRUE)
				PRINTLN("DUMMY_REPOSITION_CORONA_TEAM_PEDS - SET_ENTITY_VISIBLE(TRUE)", i)
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF

			SET_ENTITY_HEADING(g_CoronaClonePeds.piPlayersPed[i].pedID, (fPlayerHeading + g_fCoronaPedHeadings[iCount-1]) - 180)
			iCount++
		ENDIF
	ENDFOR

ENDPROC


#ENDIF


FUNC PLAYER_INDEX GET_CORONA_PLAYER_IN_SLOT(CORONA_MENU_DATA &coronaMenuData, INT iSlot)

	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			
			IF coronaMenuData.lobbyPlayers[i].iSlotAssigned = iSlot
			
				PRINTLN("[CORONA] GET_CORONA_PLAYER_IN_SLOT - found player in new slot: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID), " i = ", i)
				RETURN coronaMenuData.lobbyPlayers[i].playerID
			ENDIF
		ENDIF
		
	ENDREPEAT

	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

/// PURPOSE: Updates our broadcast data with value we are looking at
PROC SET_CORONA_LOOK_AT_BROADCAST_DATA(CORONA_MENU_DATA &coronaMenuData)

	IF NOT IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iLookAtSlot = NATIVE_TO_INT(GET_CORONA_PLAYER_IN_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData)))
		PRINTLN("[CORONA] SET_CORONA_LOOK_AT_BROADCAST_DATA - Viewing player: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iLookAtSlot)
	ELSE
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iLookAtSlot = GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData))
		PRINTLN("[CORONA] SET_CORONA_LOOK_AT_BROADCAST_DATA - Viewing player: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iLookAtSlot)
	ENDIF

	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iClientSyncID++

ENDPROC

/// PURPOSE: Places the peds in correct place around the corona
/// PARAMS:
///    coronaMenuData - Corona struct
///    vCentrePosition - Centre position of the corona
///    iMissionType - Type of Job
///    iNumPlayers - Max players that could be involved in this Job
PROC POSITION_CORONA_PEDS(CORONA_MENU_DATA &coronaMenuData, VECTOR vCentrePosition, INT iMissionType, INT iNumSlots, BOOL bForceUpdate = TRUE)

	PRINTLN("[CORONA] POSITION_CORONA_PEDS - called...")
	PRINTLN("[CORONA] 				vCentrePosition - ", vCentrePosition)
	PRINTLN("[CORONA] 				iMissionType - ", iMissionType)
	PRINTLN("[CORONA] 				iNumSlots - ", iNumSlots)
	
	INT i
	INT iCount
	
	INT iMaxSlot
	INT iCurrentTeam = -1
	INT iBS_TeamPlaced = 0
	INT iCurrentTeamSlot = -1
		
	INT iTeamMemberCount = -1
	INT iTeamRow = 0
	
	coronaMenuData.iCoronaSlotsActive = 0
	
	BOOL bTeamSlotStored
	
	#IF IS_DEBUG_BUILD
	IF bDebugClonesForCoronaScene
		
		INT iDummyTeam
		iTeamMemberCount = 0
		REPEAT NUM_NETWORK_PLAYERS i
			IF CORONA_SCENE_CHECK_VALID_PED(g_CoronaClonePeds.piPlayersPed[i].pedID)
			
				PLACE_PED_IN_CORONA_POSITION(coronaMenuData, g_CoronaClonePeds.piPlayersPed[i].pedID, iDummyTeam,
										 iCount, vCentrePosition, iMissionType, iNumSlots, iTeamMemberCount, iTeamRow, iCurrentTeam, iCount, bForceUpdate)
										 
				iCount++
			ENDIF
		ENDREPEAT

		EXIT
	ENDIF
	#ENDIF
	
	
	BOOL bUseGenericAnim
	
	REPEAT NUM_NETWORK_PLAYERS i
		
		// Check to see if player in list is valid
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(coronaMenuData.lobbyPlayers[i].playerID, FALSE, TRUE)
			
			bTeamSlotStored = FALSE					
			
			// If this is a team corona
			IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
				
				// Increment the member count we are doing for current team
				iTeamMemberCount++
			
//				IF NOT IS_CORONA_BIT_SET(CORONA_HEIST_SINGLE_TEAM_OUTFITS)
				// If teams do not match then reset our values and increment our team slot count
				IF iCurrentTeam != coronaMenuData.lobbyPlayers[i].iTeam
					iCurrentTeam = coronaMenuData.lobbyPlayers[i].iTeam
					
					iCurrentTeamSlot++
					iTeamMemberCount = 0
					iTeamRow = 0								
				ENDIF
//				ELSE
//					coronaMenuData.lobbyPlayers[i].iTeam = 0
//					iCurrentTeamSlot++
//					coronaMenuData.lobbyPlayers[i].iTeamMembers = (iTeamMemberCount + 1)
//				ENDIF
			
				// Update our slot 
				iCount = iCurrentTeamSlot
				iMaxSlot = iCurrentTeamSlot
				
				// Update our slot layout
				coronaMenuData.iCoronaSlotLayout[iCurrentTeamSlot] = iCurrentTeam
				SET_BIT(iBS_TeamPlaced, iCurrentTeam)
				bTeamSlotStored = TRUE
				
				PRINTLN("[CORONA] 	- ped placed in team: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID), 
				" for Team ", iCurrentTeam, " for Slot: ", iCurrentTeamSlot, " and memberCount ", iTeamMemberCount)						
		
			ENDIF
			
			IF NOT coronaMenuData.lobbyPlayers[i].bPlaced
		
				// Attempt to place ped in position
				//IF NOT bIgnorePlacement
				
					INT iTeamPos					
					
					iTeamPos = coronaMenuData.lobbyPlayers[i].iTeamPosIndex
					bUseGenericAnim = FALSE
					
//					#IF FEATURE_HEIST_PLANNING
//					IF iTeamPos = 0
//					AND g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].iCloneOutfit = ENUM_TO_INT(OUTFIT_HEIST_PRISONER_0)
//						bUseGenericAnim = TRUE
//					ENDIF
//					#ENDIF
								
					PLACE_PED_IN_CORONA_POSITION(coronaMenuData, g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID, coronaMenuData.lobbyPlayers[i].iTeamPos,
												 iCount, vCentrePosition, iMissionType, iNumSlots, iTeamMemberCount, iTeamRow, iCurrentTeam, iTeamPos, bForceUpdate, DEFAULT, DEFAULT, bUseGenericAnim)
				//ENDIF
				
				coronaMenuData.lobbyPlayers[i].iTeamRow = iTeamRow
				
				// We need to use the count for the team otherwise the player index
				IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
					coronaMenuData.lobbyPlayers[i].iSlotAssigned = iCount
				ELSE
					coronaMenuData.lobbyPlayers[i].iSlotAssigned = i
				ENDIF
				
				coronaMenuData.lobbyPlayers[i].bPlaced = TRUE
				
				PRINTLN("[CORONA] 	- ped placed for player ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID), " in slot ", coronaMenuData.lobbyPlayers[i].iSlotAssigned)
			ENDIF
			
			IF NOT bTeamSlotStored
				coronaMenuData.iCoronaSlotLayout[iCount] = iCount
			ENDIF
			
			// Set that this slot is now active
			IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
				SET_BIT(coronaMenuData.iCoronaSlotsActive, iCount)
			ELSE
				SET_BIT(coronaMenuData.iCoronaSlotsActive, i)
			ENDIF
			
			// Store our slot position
			IF coronaMenuData.lobbyPlayers[i].playerID = PLAYER_ID()
			OR (IS_PLAYER_SCTV(PLAYER_ID()) AND coronaMenuData.lobbyPlayers[i].playerID = coronaMenuData.piRacePartner)
				SET_MY_CORONA_SLOT(coronaMenuData, coronaMenuData.lobbyPlayers[i].iSlotAssigned)				
			ENDIF
			
			PRINTLN("[CORONA] 	- setting corona slot: ", coronaMenuData.lobbyPlayers[i].iSlotAssigned, " to ACTIVE")
			PRINTNL()			
			
			//...success, increment slot number (peds placed left to right)
			iCount++
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[CORONA] 	- ped is not in corona: ", i)
		#ENDIF
		
		ENDIF
		
	ENDREPEAT
	

	// Keep record of where we are placing teams
	IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)		
		FOR i = 0 TO (iNumSlots-1) STEP 1
			
			IF NOT IS_BIT_SET(iBS_TeamPlaced, i)
				iMaxSlot++
				coronaMenuData.iCoronaSlotLayout[iMaxSlot] = i
				
				SET_BIT(iBS_TeamPlaced, i)
			ENDIF
		ENDFOR
		
	ENDIF
		
	// ******** TUTORIAL SPECIAL CASE ********
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		
		PRINTLN("[CORONA] 	POSITION_CORONA_PEDS - ped placed for player Lamar (TUTORIAL) in slot ", iCount)
		INT iTempInt
		//IF NOT bIgnorePlacement
			PLACE_PED_IN_CORONA_POSITION(coronaMenuData, coronaMenuData.tutorialPedIndex, iTempInt, iCount, vCentrePosition, iMissionType, iNumSlots, iTeamMemberCount, iTeamRow, 0, 0, bForceUpdate)
		//ENDIF
		coronaMenuData.iTutorialPedSlot = iCount
	ENDIF
	
	// Set our default camera position for this screen (always us)
	coronaMenuData.iCoronaSlotTarget = GET_MY_CORONA_SLOT(coronaMenuData)
	
	// Broadcast who we are looking at so everyone knows
	SET_CORONA_LOOK_AT_BROADCAST_DATA(coronaMenuData)
	
ENDPROC


PROC REPOSITION_PEDS_FOR_TEAM(CORONA_MENU_DATA &coronaMenuData, INT iTeam, VECTOR vCentrePosition, INT iMissionType, INT iNumSlots, PLAYER_INDEX aPlayerThatChanged)

	PRINTLN("[CORONA] REPOSITION_PEDS_FOR_TEAM - called...")
	
	INT i
	INT iCount
	INT iCurrentTeam = -1
	INT iTeamMemberCount = 0
	INT iTeamRow = 0
	
	PED_INDEX aPedThatChanged
	IF aPlayerThatChanged != INVALID_PLAYER_INDEX()
		aPedThatChanged = g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(aPlayerThatChanged)].pedID
	ENDIF
	
	REPEAT NUM_NETWORK_PLAYERS i
	
		// Check to see if player in list is valid
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(coronaMenuData.lobbyPlayers[i].playerID, FALSE, TRUE)
			
			
			// If team corona, move players about correctly
			IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
				
				iTeamMemberCount++
				
				IF iCurrentTeam != coronaMenuData.lobbyPlayers[i].iTeam
					iCurrentTeam = coronaMenuData.lobbyPlayers[i].iTeam
					iCount = GET_CORONA_SLOT_FOR_TEAM(coronaMenuData, coronaMenuData.lobbyPlayers[i].iTeam)
					
					iTeamRow = 0
					iTeamMemberCount = 0
				ENDIF
				
				PRINTLN("[CORONA] 	REPOSITION_PEDS_FOR_TEAM - ped placed in team: ", 
				GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID), " for Team ", iCurrentTeam, 
				" for Slot: ", iCount, " and memberCount ", iTeamMemberCount)						
			ENDIF
			
			IF coronaMenuData.lobbyPlayers[i].iTeam = iTeam
				
				// Attempt to place ped in position
				PLACE_PED_IN_CORONA_POSITION(coronaMenuData, g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID, coronaMenuData.lobbyPlayers[i].iTeamPos,
											iCount, vCentrePosition, iMissionType, iNumSlots, iTeamMemberCount, iTeamRow, iTeam, coronaMenuData.lobbyPlayers[i].iTeamPosIndex, FALSE, FALSE, aPedThatChanged)
				
				coronaMenuData.lobbyPlayers[i].iTeamRow = iTeamRow
				coronaMenuData.lobbyPlayers[i].iSlotAssigned = iCount
				coronaMenuData.lobbyPlayers[i].bPlaced = TRUE
				
				IF coronaMenuData.lobbyPlayers[i].playerID = PLAYER_ID()
					SET_MY_CORONA_SLOT(coronaMenuData, iCount)				
				ENDIF
				
				PRINTLN("[CORONA] 	REPOSITION_PEDS_FOR_TEAM - ped placed for player ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID), " in slot ", iCount)
			ENDIF
			
			//...success, increment slot number (peds placed left to right)
			//iCount++
			
		#IF IS_DEBUG_BUILD
		ELSE
			//PRINTLN("[CORONA] 	REPOSITION_PEDS_FOR_TEAM - ped is not in corona: ", i)
		#ENDIF
		
		ENDIF
		
	ENDREPEAT
	
		
	
ENDPROC


PROC UPDATE_CORONA_TEAM_ARRAY_FOR_PLAYER(PLAYER_INDEX playerID, INT iTeam, CORONA_MENU_DATA &coronaMenuData)

	PRINTLN("[CORONA] UPDATE_CORONA_TEAM_ARRAY_FOR_PLAYER - ")
	PRINTLN("[CORONA] 			- Player: ", GET_PLAYER_NAME(playerID))
	PRINTLN("[CORONA] 			- New Team: ", iTeam)

	INT i
	
	INT iOldTeam = -1
	INT iTeamCount = 0
	BOOL bInsertAtFront = (playerID = PLAYER_ID())
	
	// Remove the player
	BOOL bTeamExists = FALSE
	BOOL bShiftDown = FALSE
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF bShiftDown
			coronaMenuData.lobbyPlayers[i-1] = coronaMenuData.lobbyPlayers[i]
			RESET_CORONA_GAMERTAG_POSITION(coronaMenuData.lobbyPlayers[i-1])
		ENDIF
	
		IF coronaMenuData.lobbyPlayers[i].playerID = playerID		
			iOldTeam = coronaMenuData.lobbyPlayers[i].iTeam			
			bShiftDown = TRUE
			
			PRINTLN("[CORONA] 			- Old Team: ", iOldTeam, " Index: ", i)
		ELSE
			IF coronaMenuData.lobbyPlayers[i].iTeam = iTeam
				bTeamExists = TRUE
				iTeamCount = coronaMenuData.lobbyPlayers[i].iTeamMembers
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Empty out the last slot.
	RESET_CORONA_PLAYER_DETAILS(coronaMenuData.lobbyPlayers[NUM_NETWORK_PLAYERS-1])
	
	IF bTeamExists

		BOOL bInserted
		BOOL bFoundTeam
		
		CORONA_PLAYER_DETAILS playerDetails
		playerDetails.playerID = playerID
		playerDetails.iTeam = iTeam
		playerDetails.iTeamMembers = iTeamCount + 1
		playerDetails.bPlaced = TRUE
		
		// Insert player in new position
		FOR i = (NUM_NETWORK_PLAYERS-2) TO 0 STEP -1
		
			// Deduct one if player moved from this team
			IF coronaMenuData.lobbyPlayers[i].iTeam = iOldTeam
				coronaMenuData.lobbyPlayers[i].iTeamMembers--
			ENDIF
			
			// Increment if the team is same as new team
			IF coronaMenuData.lobbyPlayers[i].iTeam = iTeam
				coronaMenuData.lobbyPlayers[i].iTeamMembers++
			ENDIF
		
			// Process while we have not inserted player
			IF NOT bInserted
				
				IF bInsertAtFront
				
					IF NOT bFoundTeam
						IF coronaMenuData.lobbyPlayers[i].iTeam = iTeam
							bFoundTeam = TRUE
						ENDIF
						// Otherwise shift everybody up
						coronaMenuData.lobbyPlayers[i+1] = coronaMenuData.lobbyPlayers[i]
						coronaMenuData.lobbyPlayers[i] = playerDetails
						
						RESET_CORONA_GAMERTAG_POSITION(coronaMenuData.lobbyPlayers[i+1])
					ELSE
						IF coronaMenuData.lobbyPlayers[i].iTeam != iTeam
							bInserted = TRUE
							PRINTLN("[CORONA] 			- Found Team, inserted at index (FRONT): ", i+1)
						ENDIF
					ENDIF
					
				ELSE
					IF coronaMenuData.lobbyPlayers[i].iTeam = iTeam
						coronaMenuData.lobbyPlayers[i+1] = playerDetails
						SET_PLAYER_CORONA_TEAM(coronaMenuData, i+1, iTeam, NATIVE_TO_INT(playerID))
						bInserted = TRUE
						
						PRINTLN("[CORONA] 			- Found Team, inserted at index (BACK): ", i+1)
					ELSE
						coronaMenuData.lobbyPlayers[i+1] = coronaMenuData.lobbyPlayers[i]
						RESET_CORONA_GAMERTAG_POSITION(coronaMenuData.lobbyPlayers[i+1])
					ENDIF
				ENDIF

			ENDIF
		
		ENDFOR
	ELSE
		// If team doesn't exist, insert player at end
		REPEAT NUM_NETWORK_PLAYERS i
			
			// Deduct one if player moved from this team
			IF coronaMenuData.lobbyPlayers[i].iTeam = iOldTeam
				coronaMenuData.lobbyPlayers[i].iTeamMembers--
			ENDIF
			
			// Increment if the team is same as new team
			IF coronaMenuData.lobbyPlayers[i].iTeam = iTeam
				coronaMenuData.lobbyPlayers[i].iTeamMembers++
			ENDIF
			
			IF coronaMenuData.lobbyPlayers[i].playerID = INVALID_PLAYER_INDEX()
				
				PRINTLN("[CORONA] 			- Empty team, insert at end: ", i)
				
				coronaMenuData.lobbyPlayers[i].playerID = playerID
				
				SET_PLAYER_CORONA_TEAM(coronaMenuData, i, iTeam, NATIVE_TO_INT(playerID))

				coronaMenuData.lobbyPlayers[i].iTeamMembers = 1
				coronaMenuData.lobbyPlayers[i].bPlaced = TRUE
				
				i = NUM_NETWORK_PLAYERS
			ENDIF
		ENDREPEAT
	ENDIF
	
//	INT iCurrentTeam = -1
//	INT iCurrentTeamSlot = -1
//	
//	// Correct our corona slot layout
//	REPEAT NUM_NETWORK_PLAYERS i
//	
//		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
//		AND IS_NET_PLAYER_OK(coronaMenuData.lobbyPlayers[i].playerID, FALSE, TRUE)
//	
//			IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
//				
//				IF iCurrentTeam != coronaMenuData.lobbyPlayers[i].iTeam
//					iCurrentTeam = coronaMenuData.lobbyPlayers[i].iTeam
//					
//					iCurrentTeamSlot++							
//				ENDIF
//		
//				coronaMenuData.iCoronaSlotLayout[iCurrentTeamSlot] = iCurrentTeam
//				
//				PRINTLN("[CORONA] 	- Correct our corona slot layout: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID), " for Team ", iCurrentTeam, " for Slot: ", iCurrentTeamSlot)						
//			ENDIF
//		ENDIF
//	ENDREPEAT
	
	
	// Display our final array.
	#IF IS_DEBUG_BUILD
		
		PRINTLN(" ")
		PRINTLN("[CORONA] ********** REVISED ARRAY AFTER TEAM SWAP **********")
		
		REPEAT NUM_NETWORK_PLAYERS i
			
			PRINTLN(" Slot: ", i)
			
			IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
				PRINTLN("	- Player: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID))
				PRINTLN("	- Team: ", coronaMenuData.lobbyPlayers[i].iTeam)
				PRINTLN("	- No. of Members: ", coronaMenuData.lobbyPlayers[i].iTeamMembers)
				PRINTLN("	- fNameX: ", coronaMenuData.lobbyPlayers[i].fNameX)
				PRINTLN("	- fNameY: ", coronaMenuData.lobbyPlayers[i].fNameY)
			ELSE
				PRINTLN("	- Player: INVALID_PLAYER_INDEX()")
			ENDIF			
		ENDREPEAT
		
		PRINTLN("[CORONA] ********************************************")
		PRINTLN(" ")
	#ENDIF
	

ENDPROC

FUNC BOOL GET_SIMPLE_INTERIOR_CORONA_HOST(PLAYER_INDEX playerId)
	
	IF NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
	AND NOT IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
	#IF FEATURE_HEIST_ISLAND
	AND NOT IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
	#ENDIF
		PRINTLN("[CORONA] GET_SIMPLE_INTERIOR_CORONA_HOST - Not gangops flow or casino heist or heist island")
	
		RETURN FALSE
	ENDIF
	
	PRINTLN("[CORONA] GET_SIMPLE_INTERIOR_CORONA_HOST - Check: ", GET_PLAYER_NAME(playerId))
	
	
	INT iLoop
	GAMER_HANDLE ghLeader
	REPEAT NUM_NETWORK_PLAYERS iLoop
		IF IS_THIS_TRANSITION_SESSION_PLAYER_THE_HOST(iLoop)
			CDEBUG1LN(DEBUG_GANG_OPS, "[CORONA] GET_SIMPLE_INTERIOR_CORONA_HOST - host is in array position ", iLoop)
			ghLeader = g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].hGamer
			BREAKLOOP
		ENDIF
	ENDREPEAT 
	
	PLAYER_INDEX gangOpsHost = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(ghLeader)
	
	PRINTLN("[CORONA] GET_SIMPLE_INTERIOR_CORONA_HOST - Check: ", GET_PLAYER_NAME(playerId), " against: ", GET_PLAYER_NAME(gangOpsHost))
	
	RETURN gangOpsHost = playerID
ENDFUNC

/// PURPOSE: Finds an available team position index for a player in the team they have joined
PROC UPDATE_CORONA_PLAYER_TEAM_POSITION_INDEX(PLAYER_INDEX playerID, CORONA_MENU_DATA &coronaMenuData, INT iTeam)

	IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)		

		INT i
		INT iPlayerSlot
		INT iBSSlotsTaken
		
		// Loop over the players to find the team and available slots
		REPEAT NUM_NETWORK_PLAYERS i
			
			IF coronaMenuData.lobbyPlayers[i].playerID = playerID
				iPlayerSlot = i			
			ELSE
				IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
					IF coronaMenuData.lobbyPlayers[i].iTeam = iTeam
						SET_BIT(iBSSlotsTaken, coronaMenuData.lobbyPlayers[i].iTeamPosIndex)
					ENDIF
				ENDIF
			ENDIF
		
		ENDREPEAT

		// If this is the unique heist behaviour then put us at the front
		IF IS_CORONA_BIT_SET(CORONA_NO_TEAM_OUTFITS)
		
			IF g_ownerOfPropertyIAmIn = playerID
			OR GET_SIMPLE_INTERIOR_CORONA_HOST(playerID)

				coronaMenuData.lobbyPlayers[iPlayerSlot].iTeamPosIndex = 0
				PRINTLN("[CORONA] UPDATE_CORONA_PLAYER_TEAM_POSITION_INDEX - Player is owner of property, put them at the front: ", GET_PLAYER_NAME(playerID))
				EXIT
			ELSE
				// Slot 0 will always be the leader
				INT iPos = 1
				PLAYER_INDEX playerInSlot
				
				// Loop over our players to find the slot we should be in
				REPEAT NUM_NETWORK_PLAYERS i
					
					playerInSlot = INT_TO_PLAYERINDEX(i)
					
					// If the player is valid
					IF playerInSlot != INVALID_PLAYER_INDEX()
					AND IS_NET_PLAYER_OK(playerInSlot, FALSE)
					AND NOT IS_PLAYER_SCTV(playerInSlot)
						//...yes, are they the owner of the property
						IF playerInSlot != g_ownerOfPropertyIAmIn
						AND NOT GET_SIMPLE_INTERIOR_CORONA_HOST(playerInSlot)
							
							//...no, is it the player we are looking for?
							IF playerInSlot = playerID
							
								//...yes, save out out position
								coronaMenuData.lobbyPlayers[iPlayerSlot].iTeamPosIndex = iPos
								PRINTLN("[CORONA] UPDATE_CORONA_PLAYER_TEAM_POSITION_INDEX - No team bespoke corona layout. Player [", GET_PLAYER_NAME(playerID), "], iTeamPosIndex = ", iPos)
								
								EXIT
							ELSE
								//...no, increment our position
								iPos++
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			
				EXIT
			ENDIF
		ENDIF

		// Loop over the bitset to find next available 
		REPEAT NUM_NETWORK_PLAYERS i
			
			IF NOT IS_BIT_SET(iBSSlotsTaken, i)
				PRINTLN("[CORONA] UPDATE_CORONA_PLAYER_TEAM_POSITION_INDEX - Found available team position index for player: ", i)
				coronaMenuData.lobbyPlayers[iPlayerSlot].iTeamPosIndex = i
				i = NUM_NETWORK_PLAYERS
			ENDIF
		ENDREPEAT


	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[CORONA] UPDATE_CORONA_PLAYER_TEAM_POSITION_INDEX - Corona is not bespoke layout. Do not process loops")
	#ENDIF
	ENDIF
ENDPROC


// ******************************************
// 		NEW CORONA PLAYER LIST PROCESSING
// ******************************************

STRUCT CORONA_TEAM_DETAILS
	INT iTeamIndex = -1
	INT iTeamTotal = 0
ENDSTRUCT

/// PURPOSE: Shifts all players up one place from iTeamIndex. Correct team indexes
PROC MOVE_ALL_PLAYERS_IN_CORONA_ARRAY(INT iTeamIndex, CORONA_MENU_DATA &coronaMenuData, CORONA_TEAM_DETAILS &teamIndexArray[])
	
	INT i
	
	// Push every player up one
	FOR i = (NUM_NETWORK_PLAYERS-2) TO iTeamIndex STEP -1
		coronaMenuData.lobbyPlayers[i+1] = coronaMenuData.lobbyPlayers[i]	
	ENDFOR
	
	// Update our knowledge of where every team finishes in the array
	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1) STEP 1
		IF teamIndexArray[i].iTeamIndex >= iTeamIndex
			teamIndexArray[i].iTeamIndex++
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE: Populates the passed slot with players details
PROC ADD_PLAYER_DETAILS_TO_CORONA_ARRAY(INT iPosition, PLAYER_INDEX playerID, INT iTeam, CORONA_MENU_DATA &coronaMenuData, INT iCurrentTeamTotal)
	
	coronaMenuData.lobbyPlayers[iPosition].playerID 		= playerID

	// Update the team and clone struct
	SET_PLAYER_CORONA_TEAM(coronaMenuData, iPosition, iTeam, NATIVE_TO_INT(playerID))
	
	coronaMenuData.lobbyPlayers[iPosition].iTeamPosIndex 	= iCurrentTeamTotal
	
	UPDATE_PLAYER_CORONA_VOICE_STATE(coronaMenuData.lobbyPlayers[iPosition])
	
	
	IF IS_CORONA_BIT_SET(CORONA_NO_TEAM_OUTFITS)
		
		IF g_ownerOfPropertyIAmIn = playerID
		OR GET_SIMPLE_INTERIOR_CORONA_HOST(playerID)
			coronaMenuData.lobbyPlayers[iPosition].iTeamPosIndex = 0
			PRINTLN("[CORONA] ADD_PLAYER_DETAILS_TO_CORONA_ARRAY - Player is owner of property, put them at the front: ", GET_PLAYER_NAME(playerID))
			EXIT
		ELSE
			// Slot 0 will always be the leader
			INT i
			INT iPos = 1
			PLAYER_INDEX playerInSlot
			
			// Loop over our players to find the slot we should be in
			REPEAT NUM_NETWORK_PLAYERS i
				
				playerInSlot = INT_TO_PLAYERINDEX(i)
				
				// If the player is valid
				IF playerInSlot != INVALID_PLAYER_INDEX()
				AND IS_NET_PLAYER_OK(playerInSlot, FALSE)
				AND NOT IS_PLAYER_SCTV(playerInSlot)
				
					//...yes, are they the owner of the property
					IF playerInSlot != g_ownerOfPropertyIAmIn
					AND NOT GET_SIMPLE_INTERIOR_CORONA_HOST(playerInSlot)
						//...no, is it the player we are looking for?
						IF playerInSlot = playerID
						
							//...yes, save out out position
							coronaMenuData.lobbyPlayers[iPosition].iTeamPosIndex = iPos
							PRINTLN("[CORONA] ADD_PLAYER_DETAILS_TO_CORONA_ARRAY - No team bespoke corona layout. Player [", GET_PLAYER_NAME(playerID), "], iTeamPosIndex = ", iPos)
							
							EXIT
						ELSE
							//...no, increment our position
							iPos++
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		
			EXIT
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_TEAM_INDEX_IN_CORONA(CORONA_MENU_DATA &coronaMenuData, INT iTeam)

	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			
			IF coronaMenuData.lobbyPlayers[i].iTeam > iTeam
				PRINTLN("		- Getting team index (found team higher then me, stop): ", i)	
				RETURN i
			ENDIF
		ELSE
			PRINTLN("		- Getting team index (end of list): ", i)		
			RETURN i
		ENDIF
		
	ENDREPEAT

	RETURN i
ENDFUNC


/// PURPOSE: Adds a player to the corona lobby array within their team
PROC ADD_PLAYER_TO_CORONA_ARRAY(PLAYER_INDEX playerID, CORONA_MENU_DATA &coronaMenuData, CORONA_TEAM_DETAILS &teamIndexArray[])
	
	// We do not add a player if they are SCTV
	IF IS_PLAYER_SCTV(playerID)
		PRINTLN("	- New player ", GET_PLAYER_NAME(playerID), " is SCTV. Do not add.")
		EXIT
	ENDIF
	
	// Get players team they have been assigned
	INT iTeamAssigned = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sClientCoronaData.iTeamChosen
	
	PRINTLN("	- New player added: ", GET_PLAYER_NAME(playerID))
	PRINTLN("		- Player is in team: ", iTeamAssigned)
	
	IF iTeamAssigned = -1
	OR IS_CORONA_BIT_SET(CORONA_HEIST_SINGLE_TEAM_OUTFITS)
		iTeamAssigned = 0
		PRINTLN("[CORONA] ADD_PLAYER_TO_CORONA_ARRAY - Player has team -1. REVERT TO 0")
		//SCRIPT_ASSERT("ADD_PLAYER_TO_CORONA_ARRAY - GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sClientCoronaData.iTeamChosen = -1. Player in invalid team.")
	ENDIF	
	
	// Get the index currently used for same team
	INT iTeamIndex = teamIndexArray[iTeamAssigned].iTeamIndex
	
	// If the current index for the team is invalid, add at end
	IF iTeamIndex = -1
	
		PRINTLN("		- Player's team not already placed (iTeamIndex = -1)")
	
		IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
			iTeamIndex = GET_TEAM_INDEX_IN_CORONA(coronaMenuData, iTeamAssigned)
			
			// Move everyone out of the way
			MOVE_ALL_PLAYERS_IN_CORONA_ARRAY(iTeamIndex, coronaMenuData, teamIndexArray)
		ELSE
			//...correct our team index
			iTeamIndex = coronaMenuData.iCoronaPedIndex
		ENDIF
			
		//...update the array with details of player
		ADD_PLAYER_DETAILS_TO_CORONA_ARRAY(iTeamIndex, playerID, iTeamAssigned, coronaMenuData, teamIndexArray[iTeamAssigned].iTeamTotal)
	ELSE
		
		PRINTLN("		- Player's team is at index: ", iTeamIndex, " insert player at (iTeamIndex+1): ", (iTeamIndex+1))
		
		//...correct team index for insertion
		iTeamIndex++
		
		//...add player to array (correcting everyone else)
		MOVE_ALL_PLAYERS_IN_CORONA_ARRAY(iTeamIndex, coronaMenuData, teamIndexArray)
		ADD_PLAYER_DETAILS_TO_CORONA_ARRAY(iTeamIndex, playerID, iTeamAssigned, coronaMenuData, teamIndexArray[iTeamAssigned].iTeamTotal)
	ENDIF
	
	// Save out the index of our spectated player
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF playerID = coronaMenuData.piRacePartner
			PRINTLN("		- SCTV Dev. This is our player: ", iTeamIndex)
			coronaMenuData.iDevSpectatorIndex = iTeamIndex
		ENDIF
	ENDIF
	
	PRINTLN("		- Player's team position index: ", teamIndexArray[iTeamAssigned].iTeamTotal)
	
	// Update our knowledge of where the last player for current team has been added and how many we have
	teamIndexArray[iTeamAssigned].iTeamIndex = iTeamIndex
	teamIndexArray[iTeamAssigned].iTeamTotal++
	
	PRINTLN("		- Players team has ", teamIndexArray[iTeamAssigned].iTeamTotal, " members now")
	
	// Record player being placed
	SET_BIT(coronaMenuData.iBS_PlayersInCorona, NATIVE_TO_INT(playerID))
	
	// Increment our corona player index after adding another player
	coronaMenuData.iCoronaPedIndex++
	
	PRINTLN("		- Player added to slot: ", iTeamIndex)	
ENDPROC


/// PURPOSE: Considers players teams and places them in suitable order for corona
///    		 Should only be called after all team balancing has been completed
/// PARAMS:
///    coronaMenuData - Holds all data relevant to corona
PROC INIT_CORONA_PLAYER_ORDER(CORONA_MENU_DATA &coronaMenuData)

	PRINTLN("[CORONA] INIT_CORONA_PLAYER_ORDER - setting up our corona order of peds")
	
	// Create temp array of team index
	CORONA_TEAM_DETAILS teamIndexArray[NUM_NETWORK_PLAYERS]
	
	// ...loop over arrays, clearing all data
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		RESET_CORONA_PLAYER_DETAILS(coronaMenuData.lobbyPlayers[i])
	ENDREPEAT
	
	// ...clear bit set of players involved
	coronaMenuData.iBS_PlayersInCorona = 0
	coronaMenuData.iLobbyPlayerToCheck = 0
	coronaMenuData.iCoronaPedIndex = 0
	coronaMenuData.piViewedPlayer = GET_CORONA_DEFAULT_TARGET_PED()
	
	
	PLAYER_INDEX playerID
	
	// Add ourselves to the corona array
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		ADD_PLAYER_TO_CORONA_ARRAY(PLAYER_ID(), coronaMenuData, teamIndexArray)
	ENDIF
	
	// ...loop over remaining players and add to initial list
	REPEAT NUM_NETWORK_PLAYERS i
		
		playerID = INT_TO_PLAYERINDEX(i)
		
		//...check player is not me
		IF playerID != PLAYER_ID() 
		
			//...check player is ok and is on same vote
			IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
			AND NOT IS_THIS_PLAYER_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA(playerID)

				//...add player to lobby list details					
				ADD_PLAYER_TO_CORONA_ARRAY(playerID, coronaMenuData, teamIndexArray)
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	// Update every players team totals (used in right hand menu)
	REPEAT NUM_NETWORK_PLAYERS i
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			coronaMenuData.lobbyPlayers[i].iTeamMembers = teamIndexArray[coronaMenuData.lobbyPlayers[i].iTeam].iTeamTotal
			
			IF coronaMenuData.lobbyPlayers[i].playerID = PLAYER_ID()
				coronaMenuData.iCoronaStatPlayer = i
				PRINTLN("[CORONA] INIT_CORONA_PLAYER_ORDER - Setting coronaMenuData.iCoronaStatPlayer = ", coronaMenuData.iCoronaStatPlayer, " Name: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID), " Slot: ", coronaMenuData.lobbyPlayers[i].iSlotAssigned, " and Target Slot: ", GET_MY_CURRENT_TARGET_SLOT(coronaMenuData))
			ENDIF
		ENDIF
	ENDREPEAT
	
	// If we need to make sure a specific player is at the front then do so (slightly hardcoded right now)
	IF IS_CORONA_BIT_SET(CORONA_NO_TEAM_OUTFITS)
	
		INT iLeaderSlot
		INT iPlayerSlot0
		REPEAT NUM_NETWORK_PLAYERS i
			
			// Find both the leader and the player in slot 0
			IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
				
				IF coronaMenuData.lobbyPlayers[i].playerID = g_ownerOfPropertyIAmIn
					iLeaderSlot = i
				ENDIF
				
				IF coronaMenuData.lobbyPlayers[i].iTeamPosIndex = 0
					iPlayerSlot0 = i
				ENDIF
			ENDIF
			
		ENDREPEAT
		
		// Switch them around
		coronaMenuData.lobbyPlayers[iPlayerSlot0].iTeamPosIndex = coronaMenuData.lobbyPlayers[iLeaderSlot].iTeamPosIndex
		coronaMenuData.lobbyPlayers[iLeaderSlot].iTeamPosIndex = 0		
	ENDIF
	
	// Update how many players we believe are in corona
	UPDATE_TOTAL_PLAYER_IN_CORONA(coronaMenuData, coronaMenuData.iCoronaPedIndex)
	SET_CORONA_BIT(CORONA_PLAYER_TEAMS_HAVE_BEEN_PROCESSED)
	
	coronaMenuData.iTotalPlayersJoinedTeamDMScreen = coronaMenuData.iTotalPlayersInCorona
	PRINTLN("[CORONA] iTotalPlayersJoinedTeamDMScreen = ", coronaMenuData.iTotalPlayersJoinedTeamDMScreen)
	
	// Display our final array.
	#IF IS_DEBUG_BUILD
		
		PRINTLN(" ")
		PRINTLN("[CORONA] ********** SUMMARY OF CORONA PEDS **********")
		
		REPEAT NUM_NETWORK_PLAYERS i
			
			PRINTLN("[CORONA] Slot: ", i)
			
			IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
				PRINTLN("[CORONA]	- Player: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID))
				PRINTLN("[CORONA]	- Team: ", coronaMenuData.lobbyPlayers[i].iTeam)
				PRINTLN("[CORONA]	- No. of Members: ", coronaMenuData.lobbyPlayers[i].iTeamMembers)
			ELSE
				PRINTLN("[CORONA]	- Player: INVALID_PLAYER_INDEX()")
			ENDIF			
		ENDREPEAT
		
		PRINTLN("[CORONA] ********************************************")
		PRINTLN(" ")
	#ENDIF
		
	// We now have our initial order of peds (place peds!)
ENDPROC

/// PURPOSE: Need to maintain player team counts as players leave
PROC UPDATE_TEAM_TOTALS_FOR_TEAM(INT iTeam, CORONA_MENU_DATA &coronaMenuData)

	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			IF coronaMenuData.lobbyPlayers[i].iTeam = iTeam
			
				coronaMenuData.lobbyPlayers[i].iTeamMembers--
				PRINTLN("[CORONA] UPDATE_TEAM_TOTALS_FOR_TEAM - reduce team count for player: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID), " to: ", coronaMenuData.lobbyPlayers[i].iTeamMembers)
							
			ENDIF
		ENDIF
		
	ENDREPEAT

ENDPROC

FUNC INT GET_CORONA_PLAYER_TEAM(CORONA_MENU_DATA &coronaMenuData, PLAYER_INDEX playerID)

	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF coronaMenuData.lobbyPlayers[i].playerID = playerID
			RETURN coronaMenuData.lobbyPlayers[i].iTeam
		ENDIF
		
	ENDREPEAT

	RETURN 0
ENDFUNC

FUNC BOOL DOES_CORONA_TEAM_HAVE_MEMBERS(CORONA_MENU_DATA &coronaMenuData)

	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			
			IF coronaMenuData.lobbyPlayers[i].iSlotAssigned = GET_MY_CURRENT_TARGET_SLOT(coronaMenuData)
			
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDREPEAT

	RETURN FALSE
ENDFUNC

/// PURPOSE: Find a valid player index to display stats for
PROC UPDATE_CORONA_STAT_PLAYER_FOR_SLOT(CORONA_MENU_DATA &coronaMenuData)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			
			IF coronaMenuData.lobbyPlayers[i].iSlotAssigned = GET_MY_CURRENT_TARGET_SLOT(coronaMenuData)
			
				PRINTLN("[CORONA] UPDATE_CORONA_STAT_PLAYER_FOR_SLOT - found new player in new slot: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID), " array pos: ", i)
				coronaMenuData.iCoronaStatPlayer = i
				i = NUM_NETWORK_PLAYERS
			ENDIF
		ENDIF
		
	ENDREPEAT
	
ENDPROC

/// PURPOSE: Find a valid player index to display stats for
PROC UPDATE_CORONA_PLAYER_INDEX_FOR_TEAM(CORONA_MENU_DATA &coronaMenuData, INT &iIntToInit)
	
	INT i
	iIntToInit = -1
	
		BOOL bFoundPlayer
	
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			
			IF coronaMenuData.lobbyPlayers[i].iSlotAssigned = GET_MY_CURRENT_TARGET_SLOT(coronaMenuData)
			AND iIntToInit = -1
			
				PRINTLN("[CORONA] UPDATE_CORONA_STAT_PLAYER_FOR_SLOT - found new player in new slot: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID), " array pos: ", i)
				iIntToInit = i				
					bFoundPlayer = TRUE
			ELSE
				IF coronaMenuData.lobbyPlayers[i].playerID = PLAYER_ID()
				AND coronaMenuData.lobbyPlayers[i].iSlotAssigned = GET_MY_CURRENT_TARGET_SLOT(coronaMenuData)
				
					PRINTLN("[CORONA] UPDATE_CORONA_STAT_PLAYER_FOR_SLOT - found ourselves in new slot: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID), " array pos: ", i)
				
					iIntToInit = i
					i = NUM_NETWORK_PLAYERS
					
						bFoundPlayer = TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	IF NOT bFoundPlayer
		PRINTLN("[CORONA][AMEC] UPDATE_CORONA_STAT_PLAYER_FOR_SLOT - Couldn't find player, setting iIntToInit to INVALID_HEIST_DATA.")
		iIntToInit = INVALID_HEIST_DATA
	ENDIF
	
ENDPROC



/// PURPOSE: Increments our pointer to a new player in the same team we are currently viewing
FUNC BOOL UPDATE_CORONA_PLAYER_INDEX_FOR_TEAM_PLAYERS(CORONA_MENU_DATA &coronaMenuData, INT &iIntToIncrement, INT iIncrement = 1, BOOL bUseLocalPlayer = FALSE)

	INT iOldPlayer = iIntToIncrement
	BOOL bFound
	BOOL bFoundPlayer
	
	PRINTLN("[CORONA] UPDATE_CORONA_PLAYER_INDEX_FOR_TEAM_PLAYERS - Find next player for SLOT: ", GET_MY_CURRENT_TARGET_SLOT(coronaMenuData), ", iOldPlayer: ", iOldPlayer)
	
	WHILE (NOT bFound)
	
		iIntToIncrement += iIncrement
		
		IF iIntToIncrement >= NUM_NETWORK_PLAYERS
			iIntToIncrement = 0
		ENDIF
		
		IF iIntToIncrement < 0
			iIntToIncrement = (NUM_NETWORK_PLAYERS - 1)
		ENDIF
		
		IF iOldPlayer = iIntToIncrement
			PRINTLN("[CORONA] UPDATE_CORONA_PLAYER_INDEX_FOR_TEAM_PLAYERS - Done entire loop, no valid player? ", GET_MY_CURRENT_TARGET_SLOT(coronaMenuData))
			bFound = TRUE
			bFoundPlayer = FALSE
		ELSE
		
			IF coronaMenuData.lobbyPlayers[iIntToIncrement].playerID != INVALID_PLAYER_INDEX()
			
				IF NOT bUseLocalPlayer
				
					IF coronaMenuData.lobbyPlayers[iIntToIncrement].iSlotAssigned = GET_MY_CURRENT_TARGET_SLOT(coronaMenuData)
					
					OR IS_CORONA_BIT_SET(CORONA_HEIST_SINGLE_TEAM_OUTFITS)
//					OR IS_CORONA_BIT_SET(CORONA_HEIST_SINGLE_TEAM_OUTFITS_PREP)
						IF IS_CORONA_BIT_SET(CORONA_HEIST_SINGLE_TEAM_OUTFITS)
//						OR IS_CORONA_BIT_SET(CORONA_HEIST_SINGLE_TEAM_OUTFITS_PREP)
							PRINTLN("[CORONA][AMEC] UPDATE_CORONA_PLAYER_INDEX_FOR_TEAM_PLAYERS - Bypassing team check for heist finale/prep outfit fudge. Pointing at player: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[iIntToIncrement].playerID)) 
						ELSE
						
							PRINTLN("[CORONA] UPDATE_CORONA_PLAYER_INDEX_FOR_TEAM_PLAYERS - Found player at: ", iIntToIncrement, " Name: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[iIntToIncrement].playerID))
						
						ENDIF
						
						bFound = TRUE
						bFoundPlayer = TRUE
					ENDIF
				
				ELSE
				
					IF coronaMenuData.lobbyPlayers[iIntToIncrement].playerID = PLAYER_ID()
						PRINTLN("[CORONA][AMEC] UPDATE_CORONA_PLAYER_INDEX_FOR_TEAM_PLAYERS - Found local player: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[iIntToIncrement].playerID), " at index: ", iIntToIncrement) 
						bFound = TRUE
						bFoundPlayer = TRUE
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDWHILE

	RETURN bFoundPlayer
ENDFUNC


// Maintain the player list on right hand side (this is mainly now for players leaving)
FUNC BOOL MAINTAIN_CORONA_PLAYER_ORDER(CORONA_MENU_DATA &coronaMenuData, INT iOverrideCheck = -1)

	// If we are overriding the value to check, update our corona data
	IF iOverrideCheck != -1
		PRINTLN("[CORONA] MAINTAIN_CORONA_PLAYER_ORDER - overriding player to check: ", iOverrideCheck)
		coronaMenuData.iLobbyPlayerToCheck = iOverrideCheck
	ENDIF
	
	PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(coronaMenuData.iLobbyPlayerToCheck)	

	INT iIndex
	BOOL bListUpdated
	
	// Is player active and in my vote?
	IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
	//AND IS_THIS_PLAYER_ACTIVE_IN_CORONA(playerID)

		IF IS_BIT_SET(coronaMenuData.iBS_PlayersInCorona, coronaMenuData.iLobbyPlayerToCheck)
			iIndex = GET_INDEX_OF_CORONA_PLAYER(coronaMenuData, playerID)
			
			IF iIndex != -1
				UPDATE_PLAYER_CORONA_VOICE_STATE(coronaMenuData.lobbyPlayers[iIndex])
			ENDIF
		ENDIF
	ELSE
		//...no, do we think they are part of lobby?
		IF IS_BIT_SET(coronaMenuData.iBS_PlayersInCorona, coronaMenuData.iLobbyPlayerToCheck)
			
			PRINTLN("[CORONA] MAINTAIN_CORONA_PLAYER_ORDER - player no longer in corona: ", coronaMenuData.iLobbyPlayerToCheck)
			
			// Were we viewing that player as they left
			iIndex = GET_INDEX_OF_CORONA_PLAYER(coronaMenuData, playerID)
			
			IF iIndex != -1
			
				INT iTeam = coronaMenuData.lobbyPlayers[iIndex].iTeam
							
				// Update how many players we believe are in corona
				UPDATE_TOTAL_PLAYER_IN_CORONA(coronaMenuData, (coronaMenuData.iTotalPlayersInCorona-1))
				
				// Blank the array position of this player
				coronaMenuData.lobbyPlayers[iIndex].playerID = INVALID_PLAYER_INDEX()
				
				// If we were the only one in this team then blank the active slot
				IF coronaMenuData.lobbyPlayers[iIndex].iTeamMembers = 1
				
					IF GET_CORONA_STATUS() = CORONA_STATUS_BETTING
						PRINTLN("[CORONA] MAINTAIN_CORONA_PLAYER_ORDER - CLEAR_BIT(coronaMenuData.iCoronaActiveSlots) SLOT: ", coronaMenuData.lobbyPlayers[iIndex].iSlotAssigned)
						CLEAR_BIT(coronaMenuData.iCoronaSlotsActive, coronaMenuData.lobbyPlayers[iIndex].iSlotAssigned)
					
					
						IF coronaMenuData.iCoronaSlotTarget = coronaMenuData.lobbyPlayers[iIndex].iSlotAssigned
							PRINTLN("[CORONA] MAINTAIN_CORONA_PLAYER_ORDER - we were viewing player. Move back to us")
						
							//...yes, default back to us.
							coronaMenuData.iCoronaSlotTarget = GET_MY_CORONA_SLOT(coronaMenuData)
							coronaMenuData.piViewedPlayer = GET_CORONA_DEFAULT_TARGET_PED()
							UPDATE_CORONA_STAT_PLAYER_FOR_SLOT(coronaMenuData)
															
							// Refresh our bet amount for selected player
							UPDATE_CASH_BET_ON_CORONA_PLAYER(GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData)))
							
							SET_CORONA_BIT(CORONA_PLAYER_LEFT_REQUIRES_CAM_UPDATE)
						ENDIF
					ENDIF
				ELSE
					IF coronaMenuData.iCoronaStatPlayer = iIndex
						UPDATE_CORONA_STAT_PLAYER_FOR_SLOT(coronaMenuData)
					ENDIF
				ENDIF
				
				UPDATE_TEAM_TOTALS_FOR_TEAM(iTeam, coronaMenuData)
			ENDIF
			
			CLEAR_BIT(coronaMenuData.iBS_PlayersInCorona, coronaMenuData.iLobbyPlayerToCheck)
			
			bListUpdated = TRUE
		ENDIF
	ENDIF

	
	// Increment so we check the next player
	coronaMenuData.iLobbyPlayerToCheck++
	
	// We have looped through everyone, lets summarise and reset
	IF coronaMenuData.iLobbyPlayerToCheck >= NUM_NETWORK_PLAYERS
		//...go again
		coronaMenuData.iLobbyPlayerToCheck = 0
	ENDIF
	
	RETURN bListUpdated
ENDFUNC

/// PURPOSE: Checks each player in the current corona. (This should only be used occasionally as does a full player loop)
FUNC BOOL VERIFY_CORONA_PLAYER_LIST(CORONA_MENU_DATA &coronaMenuData)
	
	INT iPlayerToVerify
	BOOL bUpdated = FALSE
	
	REPEAT NUM_NETWORK_PLAYERS iPlayerToVerify
		IF MAINTAIN_CORONA_PLAYER_ORDER(coronaMenuData, iPlayerToVerify)
			bUpdated = TRUE
		ENDIF
	ENDREPEAT
	
	RETURN bUpdated
ENDFUNC

#IF IS_DEBUG_BUILD
	
	FUNC STRING PRINT_CORONA_CAMERA_MODE_DEBUG(CORONA_CAMERA_MODE camMode)
		SWITCH camMode
			CASE CORONA_CAMERA_MODE_INDIVIDUAL		RETURN "CORONA_CAMERA_MODE_INDIVIDUAL"
			CASE CORONA_CAMERA_MODE_AIR				RETURN "CORONA_CAMERA_MODE_AIR"
			CASE CORONA_CAMERA_MODE_WATER			RETURN "CORONA_CAMERA_MODE_WATER"
			CASE CORONA_CAMERA_MODE_LARGE_VEHICLE	RETURN "CORONA_CAMERA_MODE_LARGE_VEHICLE"
			CASE CORONA_CAMERA_MODE_STANDARD_CAR	RETURN "CORONA_CAMERA_MODE_STANDARD_CAR"
			CASE CORONA_CAMERA_MODE_TEAM			RETURN "CORONA_CAMERA_MODE_TEAM"
			CASE CORONA_CAMERA_MODE_BIKE			RETURN "CORONA_CAMERA_MODE_BIKE"
			CASE CORONA_CAMERA_MODE_AIR_HELI		RETURN "CORONA_CAMERA_MODE_AIR_HELI"
			CASE CORONA_CAMERA_MODE_SYNC_SCENE		RETURN "CORONA_CAMERA_MODE_SYNC_SCENE"
		ENDSWITCH
		
		RETURN "UNKNOWN"
	ENDFUNC	
	
#ENDIF


// ************************************
// 		NEW CORONA CAMERA CONTROL
// ************************************

FUNC CORONA_CAMERA_MODE GET_CAMERA_MODE_FOR_MINIGAME(INT iMissionType)
	IF iMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
	OR iMissionType = FMMC_TYPE_MG_DARTS
	OR iMissionType = FMMC_TYPE_MG_ARM_WRESTLING
		RETURN CORONA_CAMERA_MODE_MINIGAME
	ELSE
		RETURN CORONA_CAMERA_MODE_SYNC_SCENE
	ENDIF
ENDFUNC

FUNC CORONA_CAMERA_MODE GET_CORONA_CAMERA_MODE(CORONA_MENU_DATA &coronaMenuData)
	RETURN coronaMenuData.coronaCamMode
ENDFUNC

PROC SET_CORONA_CAMERA_MODE(CORONA_MENU_DATA &coronaMenuData, CORONA_CAMERA_MODE camMode)
	PRINTLN("[CORONA] SET_CORONA_CAMERA_MODE - from: ", PRINT_CORONA_CAMERA_MODE_DEBUG(coronaMenuData.coronaCamMode), " to: ", PRINT_CORONA_CAMERA_MODE_DEBUG(camMode))
	
	coronaMenuData.coronaCamMode = camMode
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

FUNC BOOL IS_CORONA_MODEL_CONSIDERED_LARGE(MODEL_NAMES mnVeh)
	IF ((mnVeh = PATRIOT) OR (mnVeh = SANDKING) OR (mnVeh = SANDKING2)
	OR (mnVeh = DLOADER) OR (mnVeh = BALLER) OR (mnVeh = BALLER2)  OR (mnVeh = RALLYTRUCK) )
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_CORONA_CAMERA_MODE_NEED_CHANGING(CORONA_MENU_DATA &coronaMenuData, MODEL_NAMES mnVeh)

	IF IS_CORONA_CLASS_OFF_ROAD(g_sTransitionSessionOptions)
		IF coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_STANDARD_CAR
			IF IS_CORONA_MODEL_CONSIDERED_LARGE(mnVeh)
		
				SET_CORONA_CAMERA_MODE(coronaMenuData, CORONA_CAMERA_MODE_LARGE_VEHICLE)
				RETURN TRUE
			ENDIF
		ELSE
			IF NOT IS_CORONA_MODEL_CONSIDERED_LARGE(mnVeh)
				SET_CORONA_CAMERA_MODE(coronaMenuData, CORONA_CAMERA_MODE_STANDARD_CAR)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SET_CORONA_CAMERA_MODE_FOR_RACE(CORONA_MENU_DATA &coronaMenuData) //, INT iClass)
	
	IF IS_BOAT_RACE(g_FMMC_STRUCT.iRaceType)
		SET_CORONA_CAMERA_MODE(coronaMenuData, CORONA_CAMERA_MODE_WATER)
	ELIF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
		IF IS_CORONA_CLASS_HELIS(g_sTransitionSessionOptions)
			SET_CORONA_CAMERA_MODE(coronaMenuData, CORONA_CAMERA_MODE_AIR_HELI)
		ELSE
			SET_CORONA_CAMERA_MODE(coronaMenuData, CORONA_CAMERA_MODE_AIR)
		ENDIF
	ELSE
		IF IS_CORONA_CLASS_BIKE(g_sTransitionSessionOptions)
			SET_CORONA_CAMERA_MODE(coronaMenuData, CORONA_CAMERA_MODE_BIKE)
		ELIF IS_CORONA_CLASS_SUVS(g_sTransitionSessionOptions)
		OR IS_CORONA_CLASS_OFF_ROAD(g_sTransitionSessionOptions)
		OR (IS_ARENA_WARS_JOB() AND NOT IS_THIS_MISSION_ROCKSTAR_CREATED())
			SET_CORONA_CAMERA_MODE(coronaMenuData, CORONA_CAMERA_MODE_LARGE_VEHICLE)
		ELSE
			IF NOT IS_THIS_A_RALLY_RACE_TEMP(g_sTransitionSessionOptions)
			AND NOT IS_THIS_CORONA_A_GTA_TEAM_RACE(g_sTransitionSessionOptions)
			AND NOT IS_TARGET_ASSAULT_RACE()
				SET_CORONA_CAMERA_MODE(coronaMenuData, CORONA_CAMERA_MODE_SYNC_SCENE)
			ELSE
				SET_CORONA_CAMERA_MODE(coronaMenuData, CORONA_CAMERA_MODE_STANDARD_CAR)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_CORONA_CAMERA_VECTORS(CORONA_MENU_DATA &coronaMenuData, VECTOR &vCamPos, VECTOR &vCamRot, FLOAT &fNearDOF, FLOAT &fFarDOF, BOOL &bPointAt, FLOAT &fHeadingOffset, FLOAT &fFOV)
	
	#IF IS_DEBUG_BUILD
		IF bTweakCoronaCam
			vCamPos = <<fCoronaCamX, fCoronaCamY, fCoronaCamZ>>
			vCamRot = <<fCoronaRotX, fCoronaRotY, fCoronaRotZ>>
			fFarDOF = fCoronaDOF
			fNearDOF = fCoronaNearDOF
			bPointAt = bCoronaCamPoint
			fHeadingOffset = fCoronaHeadingOff
			fFOV = fCoronaVehicleFOV
			EXIT
		ENDIF
	#ENDIF
	
	SWITCH coronaMenuData.coronaCamMode
	
		CASE CORONA_CAMERA_MODE_INDIVIDUAL
			vCamPos = <<0.115, -3.95, 1.48>>
			vCamRot = <<0.0, 0.0, 15.0>>
			fFarDOF = 2.5
			fNearDOF = 0.0
			bPointAt = FALSE
			fHeadingOffset = 0.0
		BREAK
		
		CASE CORONA_CAMERA_MODE_MINIGAME
			vCamPos = <<0.115, -2.72, 1.48>>
			vCamRot = <<0.0, 0.0, 15.0>>
			fFarDOF = 2.5
			fNearDOF = 0.0
			bPointAt = FALSE
			fHeadingOffset = 0.0
		BREAK
		
		CASE CORONA_CAMERA_MODE_TEAM		
			vCamPos = <<0.0, -4.3, 1.9>> // <<-0.0538, -2.1142, 0.0211>>
			vCamRot = <<-12, 0.0, 11.0>> // <<-0.1398, 0.8236, 0.6227>>
			fFarDOF = 4.5
			fNearDOF = 1.0
			bPointAt = FALSE
			fHeadingOffset = 0.0
		BREAK
		
		CASE CORONA_CAMERA_MODE_STANDARD_CAR
			vCamPos = <<0.0, 4.78, 1.2>> //<<0.0962, 1.6831, 0.2997>>
			vCamRot = <<-0.595, 0.0, 1.11>> //<<0.2827, -1.2954, 0.6065>> //<<-0.595, 0.0, 1.11>>
			fFarDOF = 6.0
			fNearDOF = 2.0
			bPointAt = TRUE
			fHeadingOffset = 0.0
		BREAK
		
		CASE CORONA_CAMERA_MODE_SYNC_SCENE
		
			IF IS_CORONA_BIT_SET(CORONA_USE_ZOOMED_OUT_CAMERA_FOR_VEHICLES)
				vCamPos = <<0, 4.7, 2.2>>
				vCamRot = <<-0.9, 0.0, 1.1>>
				fFOV = 70.0
			ELSE
				vCamPos = <<0.0, 4.749, 1.200>>
				vCamRot = <<-0.4, 0.0, 1.055>>
				fFOV = 45.5
			ENDIF
			
			fFarDOF = 6.0
			fNearDOF = 1.0
			bPointAt = TRUE
			fHeadingOffset = 30.0
			
			IF IS_ARENA_WARS_JOB()
//				vCamPos.z += 1.0
//				vCamRot.z += 1.0
				fHeadingOffset = cfCORONA_ARENA_WARS_SCENE_ROTATION+30.0
			ENDIF
		BREAK
		
		CASE CORONA_CAMERA_MODE_BIKE
			vCamPos = <<0.0, 4.2, 1.55>>
			vCamRot = <<-0.42, 0.0, 1.1>>
			fFarDOF = 5.0
			fNearDOF = 2.0
			bPointAt = TRUE
			fHeadingOffset = 0.0
			
			IF IS_ARENA_WARS_JOB()
//				vCamPos.z += 1.0
//				vCamRot.z += 1.0
				fHeadingOffset = cfCORONA_ARENA_WARS_SCENE_ROTATION+30.0
			ENDIF
		BREAK
		
		CASE CORONA_CAMERA_MODE_WATER
			vCamPos = <<0.0, 8.0, 3.0>>
			vCamRot = <<-1.2, 0.0, 1.2>>
			fFarDOF = 12.5
			fNearDOF = 6.0
			bPointAt = TRUE
			fHeadingOffset = 0.0
		BREAK
		
		CASE CORONA_CAMERA_MODE_AIR
			vCamPos = <<0.0, 16.0, 5.5>>
			vCamRot = <<-2.5, 0.0, 0.0>>//1.5, 3.0>>
			fFarDOF = 30.0
			fNearDOF = 8.0
			bPointAt = TRUE
			fHeadingOffset = 0.0
		BREAK
		
		CASE CORONA_CAMERA_MODE_AIR_HELI
			vCamPos = <<0.0, 15, 3.0>>
			vCamRot = <<-2.2, 0.0, 1.0>>
			fFarDOF = 30.0
			fNearDOF = 8.0
			bPointAt = TRUE
			fHeadingOffset = 8
		BREAK		
		
		CASE CORONA_CAMERA_MODE_LARGE_VEHICLE
			vCamPos = <<0.0, 6.0, 2.0>>
			vCamRot = <<-0.81, 0.0, 1.1>>
			fFarDOF = 10.5
			fNearDOF = 5.0
			bPointAt = TRUE
			fHeadingOffset = 0.0
			
			IF IS_ARENA_WARS_JOB()
//				vCamPos.z += 1.0
//				vCamRot.z += 1.0
				fHeadingOffset = cfCORONA_ARENA_WARS_SCENE_ROTATION+30.0
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		fCoronaCamX = vCamPos.x
		fCoronaCamY = vCamPos.y
		fCoronaCamZ = vCamPos.z
		fCoronaRotX = vCamRot.x
		fCoronaRotY = vCamRot.y
		fCoronaRotZ = vCamRot.z
		fCoronaDOF = fFarDOF
		fCoronaNearDOF = fNearDOF
		bCoronaCamPoint = bPointAt
//		fCoronaHeadingOff = fHeadingOffset
		fCoronaVehicleFOV = fFOV
	#ENDIF
	
ENDPROC

/// PURPOSE: Sets the camera to point at corona vehicle and allow rotation of camera as player views vehicle
PROC SET_CORONA_CAMERA_FOR_VEHICLE(CORONA_MENU_DATA &coronaMenuData)
	
	IF IS_CORONA_BIT_SET(CORONA_GENERIC_JOB_RECREATE_CAMERA)
		IF DOES_CAM_EXIST(coronaMenuData.ciCoronaCam)
			DESTROY_CAM(coronaMenuData.ciCoronaCam)
			PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - Destroyed cam")
		ELSE
			PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - Cam didnt exist, so we'll need to make a new one")
		ENDIF
		CLEAR_CORONA_BIT(CORONA_GENERIC_JOB_RECREATE_CAMERA)
	ENDIF
	
	IF NOT DOES_CAM_EXIST(coronaMenuData.ciCoronaCam)
	
		PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - Creating corona cam for vehicle")
	
		coronaMenuData.ciCoronaCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
	ELSE
		PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - Cam already exists")
	ENDIF
	
	IF NOT IS_CAM_ACTIVE(coronaMenuData.ciCoronaCam)
		SET_CAM_ACTIVE(coronaMenuData.ciCoronaCam, TRUE)
		PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - Setting cam active")
	ELSE
		PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - Cam already active")
	ENDIF
	
	VECTOR vCameraOffset
	VECTOR vCameraRotOffset
	FLOAT fCameraNearDOFValue
	FLOAT fCameraDOFValue
	FLOAT fCameraHeadingOffset
	FLOAT fCameraFOV = 45.5
	BOOL bPointAt
	
	SET_CORONA_CAMERA_VECTORS(coronaMenuData, vCameraOffset, vCameraRotOffset, fCameraNearDOFValue, fCameraDOFValue, bPointAt, fCameraHeadingOffset, fCameraFOV)
	
	PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - Camera info for camera mode ", PRINT_CORONA_CAMERA_MODE_DEBUG(coronaMenuData.coronaCamMode),":")
	PRINTNL()
	PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - 		vCameraOffset: ", vCameraOffset)
	PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - 		vCameraRotOffset: ", vCameraRotOffset)
	PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - 		fCameraNearDOFValue: ", fCameraNearDOFValue)
	PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - 		fCameraDOFValue: ", fCameraDOFValue)
	PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - 		bPointAt: ", bPointAt)
	PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - 		fCameraHeadingOffset: ", fCameraHeadingOffset)
	PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - 		fCameraFOV: ", fCameraFOV)
	PRINTNL()
	PRINTLN("[CORONA] SET_CORONA_CAMERA_FOR_VEHICLE - End of camera info")
	
	FLOAT fHeading = (fCameraHeadingOffset + coronaMenuData.fCameraRotationX)
		
	IF coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_AIR
	OR coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_AIR_HELI		
		vCameraOffset.z = vCameraOffset.z + SIN(TO_FLOAT((GET_GAME_TIMER()/100)%360))
	ENDIF
	
	
	// Calculate the camera position. fCameraHeadingOffset is initial heading position of camera. fCameraRotationX is the amount of rotation from input
	VECTOR vCameraPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vVehicleVector, fHeading, <<vCameraOffset.x, vCameraOffset.y, vCameraOffset.z + coronaMenuData.fCameraRotationY>>)
	VECTOR vPointAt = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vVehicleVector, fHeading - 180.0,  vCameraRotOffset)

	
	//ATTACH_CAM_TO_ENTITY(cam, entity, <<0.0962, 1.6831, 0.2997>>)
	//POINT_CAM_AT_ENTITY(cam, entity, <<0.2827, -1.2954, 0.6065>>)
	//SET_CAM_FOV(cam, 40.0000)
	
	SET_CAM_COORD(coronaMenuData.ciCoronaCam, vCameraPos)
	POINT_CAM_AT_COORD(coronaMenuData.ciCoronaCam, vPointAt)
	
	SET_CAM_FOV(coronaMenuData.ciCoronaCam, fCameraFOV)
	
	SET_CAM_MOTION_BLUR_STRENGTH(coronaMenuData.ciCoronacam, 0.1)
	
	SET_USE_HI_DOF()
	SET_CAM_USE_SHALLOW_DOF_MODE(coronaMenuData.ciCoronaCam, FALSE)
	SET_CAM_DOF_STRENGTH(coronaMenuData.ciCoronaCam, 1.0)
	SET_CAM_DOF_PLANES(coronaMenuData.ciCoronaCam, 0.0, fCameraNearDOFValue, fCameraDOFValue, fCameraDOFValue+25.0)
			
//	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
//		IF NOT IS_CAM_ACTIVE(coronaMenuData.ciCoronaCam)
//			IF DOES_CAM_EXIST(g_sTransitionSessionData.ciPedCam)
//				SET_CAM_ACTIVE_WITH_INTERP(coronaMenuData.ciCoronaCam,g_sTransitionSessionData.ciPedCam,1000,GRAPH_TYPE_SIN_ACCEL_DECEL)
//				SET_CAM_MOTION_BLUR_STRENGTH(coronaMenuData.ciCoronaCam,1.0)
//				IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
//					ANIMPOSTFX_STOP("MinigameTransitionIn")
//				ENDIF
//				ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, false)
//			ELSE
//				PRINTLN("[dsw] [SET_CORONA_CAMERA_FOR_VEHICLE] g_sTransitionSessionData.ciPedCam doesn't exist")
//			ENDIF
//
//		ENDIF
//	ENDIF	
ENDPROC

/// PURPOSE: Returns the target position of the camera we are currently looking for iSlot
FUNC VECTOR GET_CORONA_CAMERA_TARGET_VECTOR(CORONA_MENU_DATA &coronaMenuData, INT iSlot, VECTOR vCoronaCentre, FLOAT fCameraHeading, FLOAT fRadius)

	// Are we in a bespoke corona with exact positioning
	IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
		INT iTeam = GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, iSlot)
		
		// Override team if we are faking team positioning
		IF IS_CORONA_BIT_SET(CORONA_NO_TEAM_OUTFITS)
			iTeam = -1
		ENDIF
		
		RETURN GET_BESPOKE_CORONA_CAMERA_PLACEMENT(iTeam)
	ENDIF

	VECTOR vTargetPos = CALC_SLOT_CENTRE_POSITION(vCoronaCentre, fCameraHeading, fRadius)

	IF coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_SYNC_SCENE
	OR coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_TEAM
	OR coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_MINIGAME
	
		IF coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_TEAM
			vTargetPos.z += 1.0
		ENDIF
	ENDIF

	// Return normal offset from central position
	RETURN vTargetPos
ENDFUNC

/// PURPOSE: Returns the target heading of the camera we are currently looking for iSlot
FUNC FLOAT GET_CORONA_CAMERA_TARGET_HEADING(CORONA_MENU_DATA &coronaMenuData, INT iSlot, VECTOR vCoronaCentre, VECTOR vfocusPos)
	
	// Are we in a bespoke corona with exact positioning
	IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
		INT iTeam = GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, iSlot)
		
		// Override team if we are faking team positioning
		IF IS_CORONA_BIT_SET(CORONA_NO_TEAM_OUTFITS)
			iTeam = -1
		ENDIF
		
		VECTOR vCamRot = GET_BESPOKE_CORONA_CAMERA_ROTATION(iTeam)
		RETURN vCamRot.z
	ENDIF


	RETURN GET_HEADING_BETWEEN_VECTORS_2D(vfocusPos, vCoronaCentre)
ENDFUNC

/// PURPOSE: Returns the target rotation for the camera (Used only for bespoke coronas currently)
FUNC VECTOR GET_CORONA_CAMERA_TARGET_ROTATION(CORONA_MENU_DATA &coronaMenuData, INT iSlot)
	
	// Are we in a bespoke corona with exact positioning
	IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
		INT iTeam = GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, iSlot)
		
		// Override team if we are faking team positioning
		IF IS_CORONA_BIT_SET(CORONA_NO_TEAM_OUTFITS)
			iTeam = -1
		ENDIF
		
		RETURN GET_BESPOKE_CORONA_CAMERA_ROTATION(iTeam)
	ENDIF

	// Standard team coronas will be ignored
	RETURN <<0, 0, 0>>
ENDFUNC

// Returns an adjusted heading for the camera
FUNC FLOAT UPDATE_CORONA_CAMERA_HEADING(FLOAT fHeading, VECTOR vRotation)
	
	IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
		RETURN vRotation.z
	ENDIF
	
	RETURN fHeading + 180.0
ENDFUNC

PROC SET_CORONA_CAMERA_TO_SLOT(CORONA_MENU_DATA &coronaMenuData, INT iSlot, INT iNumSlots, INT iMissionType, VECTOR vCoronaCentre  , BOOL bPlanningBoard = FALSE )
	
	FLOAT fRadius, fCameraHeading
	VECTOR focusPos, vTargetCameraLoc, vTargetCameraRot, vPlanningBoardLoc
	
	IF bPlanningBoard
	
		GET_PLAYER_PROPERTY_HEIST_LOCATION(vTargetCameraLoc, MP_PROP_ELEMENT_HEIST_CAM_POS_BOARD, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		GET_PLAYER_PROPERTY_HEIST_ROTATION(vTargetCameraRot, MP_PROP_ELEMENT_HEIST_CAM_POS_OVERVIEW, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		GET_PLAYER_PROPERTY_HEIST_LOCATION(vPlanningBoardLoc, MP_PROP_ELEMENT_HEIST_BOARD_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	
		// Calculate the key values of the corona
		fCameraHeading = vTargetCameraRot.Z
		focusPos = vTargetCameraLoc
		SET_CORONA_BIT(CORONA_HEIST_PLANNING_CAMERA)
		coronaMenuData.iMovingLeft = -1
		
		PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ********** Setting up new camera position to view heist planning board: ")
		PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ...  vTargetCameraLoc: 	", vTargetCameraLoc)
		PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ...  vTargetCameraRot: 	", vTargetCameraRot)
		PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ...  focusPos: 			", focusPos)
		PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ...  fCameraHeading: 		", fCameraHeading)
		PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ...  coronaCamMode: 		", coronaMenuData.coronaCamMode)
		
	ELSE
	
	// Calculate the key values of the corona
	fCameraHeading = GET_PLAYER_CORONA_ORIGIN_OFFSET(iNumSlots) + GET_CORONA_SLOT_ANGLE(iSlot, iNumSlots)
	fRadius = GET_RADIUS_OF_CORONA(iMissionType)
	focusPos = GET_CORONA_CAMERA_TARGET_VECTOR(coronaMenuData, iSlot, vCoronaCentre, fCameraHeading, fRadius)
	
	IF IS_CORONA_BIT_SET(CORONA_HEIST_PLANNING_CAMERA)
		coronaMenuData.iMovingLeft = 0
		CLEAR_CORONA_BIT(CORONA_HEIST_PLANNING_CAMERA)
	ENDIF
	
	ENDIF
	
	// If we are in sync scene mode then we should activate new animated camera
	IF coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_SYNC_SCENE
	OR coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_TEAM
	OR coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_MINIGAME
	
		IF coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_TEAM
			
			PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT - Triggering camera movement to next team")
			
			VECTOR vCameraRotation
		
			IF bPlanningBoard
				vCameraRotation = vTargetCameraRot
			ELSE
		
			// Get the target rotation and heading for the upcoming camera
			VECTOR vTempFocusPos
			vTempFocusPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaCentre, fCameraHeading, <<0, 2.0, 0.0>>)
			fCameraHeading = GET_CORONA_CAMERA_TARGET_HEADING(coronaMenuData, iSlot, vCoronaCentre, vTempFocusPos)
			vCameraRotation = GET_CORONA_CAMERA_TARGET_ROTATION(coronaMenuData, iSlot)
						
			IF IS_VECTOR_ZERO(coronaMenuData.vTargetCamVector)
				PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT - Defaulting our vector as first time we have prcessed this: ", focusPos)
				coronaMenuData.vTargetCamVector = focusPos
				coronaMenuData.fTargetCamHeading = fCameraHeading
				coronaMenuData.vTargetCamRotation = vCameraRotation
			ENDIF
			
			ENDIF
			
			IF NOT IS_CORONA_BIT_SET(CORONA_HEIST_CAMERA_FROM_BOARD)
			
				coronaMenuData.vCurrentCamVector = coronaMenuData.vTargetCamVector
				coronaMenuData.vCurrentCamRotation = coronaMenuData.vTargetCamRotation
				coronaMenuData.fCurrentCamHeading = UPDATE_CORONA_CAMERA_HEADING(coronaMenuData.fTargetCamHeading, coronaMenuData.vTargetCamRotation) 
			
			ELSE
			
				coronaMenuData.vCurrentCamVector = g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.vInitCamPos
				coronaMenuData.vCurrentCamRotation = g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.vInitCamRot
				coronaMenuData.fCurrentCamHeading = g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.vInitCamRot.Z
			
			ENDIF
			
			coronaMenuData.vTargetCamVector = focusPos
			coronaMenuData.fTargetCamHeading = fCameraHeading
			coronaMenuData.vTargetCamRotation = vCameraRotation
			
			PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT - Moving camera to position: ")
			PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ...  vCurrentCamVector: 	", coronaMenuData.vCurrentCamVector)
			PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ...  vCurrentCamRotation: ", coronaMenuData.vCurrentCamRotation)
			PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ...  fCurrentCamHeading: 	", coronaMenuData.fCurrentCamHeading)
			PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ...  vTargetCamVector: 	", coronaMenuData.vTargetCamVector)
			PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ...  vTargetCamRotation: 	", coronaMenuData.vTargetCamRotation)
			PRINTLN("[CORONA][AMEC] SET_CORONA_CAMERA_TO_SLOT ...  fTargetCamHeading: 	", coronaMenuData.fTargetCamHeading)
			
			coronaMenuData.fCamPointAtOffset = 0.0
			IF coronaMenuData.piViewedPlayer != INVALID_PLAYER_INDEX()
			AND IS_PED_WEARING_HIGH_HEELS(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.piViewedPlayer)].pedID)
				PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT : CORONA_CAMERA_MODE_TEAM - Ped wearing heels, increase z coord")
				coronaMenuData.vTargetCamVector.z = coronaMenuData.vTargetCamVector.z + 0.1
				coronaMenuData.fCamPointAtOffset = 0.1
			ENDIF
			
			IF coronaMenuData.iSelectionCameraSwitchStage > 2
				PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT - Camera already transitioning, reset so correct vectors will be used")
				coronaMenuData.iSelectionCameraSwitchStage = 0
			ENDIF
							
		ELSE
			PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT - Triggering camera movement to next player")
			PED_INDEX aTargetPed
			
			IF coronaMenuData.piViewedPlayer = INVALID_PLAYER_INDEX()
				SCRIPT_ASSERT("SET_CORONA_CAMERA_TO_SLOT - coronaMenuData.piViewedPlayer = INVALID_PLAYER_INDEX(). No one to move camera to")
				EXIT
			ENDIF
			
			IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA() AND IS_PLAYER_VIEWING_TUTORIAL_PED(coronaMenuData)
				aTargetPed = coronaMenuData.tutorialPedIndex
			ELSE
				aTargetPed = g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.piViewedPlayer)].pedID
			ENDIF
			
			IF CORONA_SCENE_CHECK_VALID_PED(aTargetPed)
			
				IF IS_VECTOR_ZERO(coronaMenuData.vTargetCamVector)
					coronaMenuData.vTargetCamVector = GET_ENTITY_COORDS(aTargetPed)
					coronaMenuData.fTargetCamHeading = GET_ENTITY_HEADING(aTargetPed)
					
					PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT - Defaulting our vector as first time we have prcessed this: ", coronaMenuData.vTargetCamVector)
				ENDIF
				
				coronaMenuData.vCurrentCamVector = coronaMenuData.vTargetCamVector
				coronaMenuData.fCurrentCamHeading = coronaMenuData.fTargetCamHeading
				
				coronaMenuData.vTargetCamVector = GET_ENTITY_COORDS(aTargetPed)
				coronaMenuData.fTargetCamHeading = GET_ENTITY_HEADING(aTargetPed)
				
				coronaMenuData.fCamPointAtOffset = 0.0
				IF IS_PED_WEARING_HIGH_HEELS(aTargetPed)
					PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT - Ped wearing heels, increase z coord")
					coronaMenuData.vTargetCamVector.z = coronaMenuData.vTargetCamVector.z + 0.1
					coronaMenuData.fCamPointAtOffset = 0.1
				ENDIF
				
				SET_CORONA_PED_VISIBILITY_FOR_SLOT(coronaMenuData, iSlot, iNumSlots)
				
				PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT - coronaMenuData.vCurrentCamVector: ", coronaMenuData.vCurrentCamVector, " and Heading: ", coronaMenuData.fCurrentCamHeading)
				PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT - coronaMenuData.vTargetCamVector: ", coronaMenuData.vTargetCamVector, " and Heading: ", coronaMenuData.fTargetCamHeading)
			ENDIF
			
			IF coronaMenuData.iSelectionCameraSwitchStage > 2
				PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT - Camera already transitioning, reset so correct vectors will be used")
				coronaMenuData.iSelectionCameraSwitchStage = 0
			ENDIF
		ENDIF
		
		SET_CORONA_BIT(CORONA_PROCESS_SLOT_SELECTION_CAMERA)
	ELSE
	
		// Process the normal behaviour of our corona cameras
		IF NOT DOES_CAM_EXIST(coronaMenuData.ciCoronaCam)
			coronaMenuData.ciCoronaCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
			
			PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT - Creating corona cam for ped normal")
		ENDIF
		
		IF NOT IS_CAM_ACTIVE(coronaMenuData.ciCoronaCam)
			SET_CAM_ACTIVE(coronaMenuData.ciCoronaCam, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		ENDIF
		
		IF IS_VECTOR_ZERO(coronaMenuData.vVehicleVector)
			coronaMenuData.vVehicleVector = focusPos
		ENDIF

		IF coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_AIR
		OR coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_AIR_HELI
			focusPos = coronaMenuData.vVehicleVector
		ENDIF

		VECTOR vCameraOffset
		VECTOR vCameraRotOffset
		FLOAT fCameraNearDOFValue
		FLOAT fCameraDOFValue
		FLOAT fCameraHeadingOffset
		FLOAT fCameraFOV = 45.5
		BOOL bPointAt
		
		coronaMenuData.fCamPointAtOffset = 0.0
		// Get our correct camera settings
		SET_CORONA_CAMERA_VECTORS(coronaMenuData, vCameraOffset, vCameraRotOffset, fCameraNearDOFValue, fCameraDOFValue, bPointAt, fCameraHeadingOffset, fCameraFOV)
		IF coronaMenuData.piViewedPlayer != INVALID_PLAYER_INDEX()
		AND IS_PED_WEARING_HIGH_HEELS(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.piViewedPlayer)].pedID)
			PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT : CORONA_CAMERA_MODE_TEAM - Ped wearing heels, increase z coord")
			coronaMenuData.vTargetCamVector.z = coronaMenuData.vTargetCamVector.z  + 0.1
			coronaMenuData.fCamPointAtOffset = 0.1
			SET_CORONA_BIT(CORONA_HEIST_HEELS_ADJUST_CAM_MOVEMENT)
		ENDIF
				
		fCameraHeading += fCameraHeadingOffset


		VECTOR vCameraPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(focusPos, fCameraHeading, <<0.0 + vCameraOffset.x, GET_RADIUS_OF_CORONA(iMissionType) + vCameraOffset.y,  vCameraOffset.z>>)
		
		SET_CAM_COORD(coronaMenuData.ciCoronaCam, vCameraPos)
		
		IF bPointAt
			POINT_CAM_AT_COORD(coronaMenuData.ciCoronaCam, <<focusPos.x + vCameraRotOffset.x, focusPos.y + vCameraRotOffset.y, focusPos.z + vCameraRotOffset.z>>)
		ELSE
			STOP_CAM_POINTING(coronaMenuData.ciCoronaCam)
			SET_CAM_ROT(coronaMenuData.ciCoronaCam, <<vCameraRotOffset.x, vCameraRotOffset.y, fCameraHeading + vCameraRotOffset.z>>)
		ENDIF
		
		SET_CAM_FOV(coronaMenuData.ciCoronaCam, fCameraFOV)
		
		SET_USE_HI_DOF()

		SET_CAM_USE_SHALLOW_DOF_MODE(coronaMenuData.ciCoronaCam, FALSE)
		SET_CAM_DOF_STRENGTH(coronaMenuData.ciCoronaCam, 1.0)
		SET_CAM_DOF_PLANES(coronaMenuData.ciCoronaCam, 0.0, fCameraNearDOFValue, fCameraDOFValue, fCameraDOFValue+25.0)
		
//		SHAKE_CAM(coronaMenuData.ciCoronaCam, "HAND_SHAKE", 0.3)
		
		// Update who we see visible
		SET_CORONA_PED_VISIBILITY_FOR_SLOT(coronaMenuData, iSlot, iNumSlots)
		
		// Store our heading when we create this camera
		coronaMenuData.fCameraRotationX = fCameraHeading
		coronaMenuData.fCameraRotationY = 0.0
		
		#IF IS_DEBUG_BUILD
		
		IF NOT bTweakCoronaCam
			PRINTLN()
			PRINTLN("[CORONA] SET_CORONA_CAMERA_TO_SLOT - ")
			PRINTLN("			- iSlot: ", iSlot)
			PRINTLN("			- iNumSlots: ", iNumSlots)
			PRINTLN("			- vCoronaCentre: ", vCoronaCentre)
			PRINTLN("			- fCameraHeading: ", fCameraHeading)
			PRINTLN("			- vCameraPos: ", vCameraPos)
			PRINTLN("			- fCameraNearDOFValue: ", fCameraNearDOFValue)
			PRINTLN("			- fCameraDOFValue: ", fCameraDOFValue)
			PRINTLN()
		ENDIF
		
		#ENDIF
	ENDIF
	
ENDPROC



PROC CLAMP_CORONA_VEHICLE_CAMERA(CORONA_MENU_DATA &coronaMenuData, FLOAT &fYLimitLow, FLOAT &fYLimitHigh)


	#IF IS_DEBUG_BUILD
		IF bTweakCoronaCam
//				IF fYpos < fCoronaYLimitDown
//					fYpos = fCoronaYLimitDown
//				ENDIF
//				
//				IF fYpos > fCoronaYLimitUp
//					fYpos = fCoronaYLimitUp
//				ENDIF
			EXIT
		ENDIF
	#ENDIF

	SWITCH coronaMenuData.coronaCamMode

		CASE CORONA_CAMERA_MODE_STANDARD_CAR
			fYLimitLow = -0.5
			fYLimitHigh = 4.0
		BREAK
		
		CASE CORONA_CAMERA_MODE_SYNC_SCENE
			fYLimitLow = -0.5
			fYLimitHigh = 2.0
		BREAK
		
		CASE CORONA_CAMERA_MODE_BIKE
			fYLimitLow = -0.5
			fYLimitHigh = 2.2
		BREAK
		
		CASE CORONA_CAMERA_MODE_WATER
			fYLimitLow = -1.25
			fYLimitHigh = 5.0
		BREAK
		
		CASE CORONA_CAMERA_MODE_AIR
		CASE CORONA_CAMERA_MODE_AIR_HELI
			fYLimitLow = -10.0
			fYLimitHigh = 6.5
		BREAK
		
		CASE CORONA_CAMERA_MODE_LARGE_VEHICLE
			fYLimitLow = -1.0
			fYLimitHigh = 2.8
		BREAK
	ENDSWITCH

ENDPROC

CONST_INT I_CORONA_SF_PIXEL_WIDTH					868
CONST_INT I_CORONA_SF_PIXEL_HEIGHT					430

CONST_INT I_CORONA_SF_ROW_PIXEL_HEIGHT				25
CONST_INT I_CORONA_SF_NUM_ROWS						16
CONST_INT I_CORONA_SF_ROW_GAP_PIXEL_HEIGHT			2
CONST_INT I_CORONA_SF_NUM_ROW_GAPS					15	

FUNC BOOL IS_CORONA_SCREEN_SPACE_COORD_VALID(VECTOR vCoord2D, VECTOR vElementDimensions2D)

//	// Is over lapping the left hand column menu
//	IF vCoord2D.x - vElementDimensions2D.x <= ( I_CORONA_SF_PIXEL_WIDTH / 3 )
//	AND vCoord2D.y - vElementDimensions2D.y <= coronaMenuData.iLeftColumnHeight
//		RETURN FALSE
//	ENDIF


		#IF IS_DEBUG_BUILD
		BOOL bDebugPrint = GET_COMMANDLINE_PARAM_EXISTS("sc_debugPrintPlayerCoronaTitles")
		IF bDebugPrint
			PRINTLN("[JA@TAGS]  vCoord2D.x = ", vCoord2D.x)
			PRINTLN("[JA@TAGS]  vCoord2D.y = ", vCoord2D.y)
			PRINTLN("[JA@TAGS]  vElementDimensions2D.x = ", vElementDimensions2D.x)
			PRINTLN("[JA@TAGS]  vElementDimensions2D.y = ", vElementDimensions2D.y)
			PRINTLN("[JA@TAGS]  vCoord2D.y + vElementDimensions2D.y = ", vCoord2D.y + vElementDimensions2D.y)
			PRINTLN("[JA@TAGS]  vCoord2D.x - (vElementDimensions2D.x * 0.5) ", vCoord2D.x - (vElementDimensions2D.x * 0.5))
			PRINTLN("[JA@TAGS]  vCoord2D.x + (vElementDimensions2D.x * 0.5) ", vCoord2D.x + (vElementDimensions2D.x * 0.5))
		ENDIF
		#ENDIF
	
	//PRINTLN("[JA@TAGS]  vCoord2D.y + vElementDimensions2D.y = ", vCoord2D.y + vElementDimensions2D.y)
	
	// do checks to cull the player titles at the lower section of the screen for the large fancy text info
	IF vCoord2D.y + vElementDimensions2D.y > 330
	
//		// Races vehicle selection screen
//		IF GET_CORONA_STATUS() = CORONA_STATUS_IN_CORONA
//		AND g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
//			
//			// Hide for the lower section of the last column
//			IF vCoord2D.x + vElementDimensions2D.x > ( I_CORONA_SF_PIXEL_WIDTH / 3 ) * 2.0
//				RETURN FALSE
//			ENDIF
//			
//		// All other screens have their large fancy text centred - growing to the right so cull the left & centre column
//		ELSE
//			// Hide for the lower section of the first 2 columns - left & centre
//			IF vCoord2D.x - vElementDimensions2D.x <= ( I_CORONA_SF_PIXEL_WIDTH / 3 ) * 2.0
//			AND vCoord2D.x + vElementDimensions2D.x > ( I_CORONA_SF_PIXEL_WIDTH / 3 )
//				RETURN FALSE
//			ENDIF
//		ENDIF
		RETURN FALSE
		
	ENDIF
	
	// Check the far left
	IF vCoord2D.x - (vElementDimensions2D.x * 0.5) < 10
		RETURN FALSE
	ENDIF
	
	// Check the far right only in non widescreen
	IF NOT GET_IS_WIDESCREEN()
		IF vCoord2D.x + (vElementDimensions2D.x * 0.5) >= (I_CORONA_SF_PIXEL_WIDTH-10)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC UPDATE_CORONA_TEAM_MEMBER_PLAYER_TITLES(CORONA_MENU_DATA &coronaMenuData)	
	
	// Exit early if pause menu is not active since it will be initialised elsewhere.
	IF NOT IS_PAUSE_MENU_ACTIVE()
		EXIT
	ENDIF
	
	INT iCurrentTeam
	IF GET_CORONA_STATUS() = CORONA_STATUS_IN_CORONA
		iCurrentTeam = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
	ELSE
		iCurrentTeam = GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData))
	ENDIF
	
	INT i, iNumOnThisTeam
	REPEAT NUM_NETWORK_PLAYERS i
		IF coronaMenuData.lobbyPlayers[i].iTeam = iCurrentTeam
			iNumOnThisTeam++
		ENDIF
	ENDREPEAT
	
	IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)	
	AND NOT IS_CORONA_BIT_SET(CORONA_PROCESS_SLOT_SELECTION_CAMERA)	
	AND NOT IS_CORONA_BIT_SET(CORONA_DISPLAYING_THIRD_DETAIL_PANEL)
	AND coronaMenuData.coronaSummaryCardState = CORONA_SUMMARY_CARD_HIDE
	AND GET_CORONA_STATUS() != CORONA_STATUS_IN_GENERIC_JOB_CORONA
		
		i = 0
		PLAYER_INDEX playerId
		VECTOR vPlayerPedPos
		TEXT_LABEL_63 strPlayerName	//, strPlayerTitle
		TEXT_LABEL_15 strPlayerTitle = ""
		HUD_COLOURS eColPlayerName	= HUD_COLOUR_WHITE
		FLOAT fX, fY
		INT iResX, iResY
		INT iInitialRow
		INT iTeam
		GET_SCREEN_RESOLUTION(iResX, iResY)
		
		SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
		
		CLEAR_FRONTEND_DATA_SLOT(CORONA_COLUMN_EIGHT)
		
		INT iCounter
		
		BOOL bInitialisedPos = FALSE
		FLOAT fIntialYPos
		BOOL bForceRender
		
		#IF IS_DEBUG_BUILD
		BOOL bDebugPrint = GET_COMMANDLINE_PARAM_EXISTS("sc_debugPrintPlayerCoronaTitles")
		IF bDebugPrint
			PRINTLN("[JA@TAGS] ************************************************")
		ENDIF
		#ENDIF
		
		REPEAT NUM_NETWORK_PLAYERS i
			
			playerId = coronaMenuData.lobbyPlayers[i].playerID
			
			IF playerId != INVALID_PLAYER_INDEX()
				IF IS_NET_PLAYER_OK(playerId)

					IF DOES_ENTITY_EXIST(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(playerId)].pedID)
					AND NOT IS_ENTITY_DEAD(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(playerId)].pedID)
				
						#IF IS_DEBUG_BUILD
						IF bDebugPrint
							PRINTLN("[JA@TAGS] Player = ", GET_PLAYER_NAME(playerId), ", coronaMenuData.lobbyPlayers[i].iTeamPos ", coronaMenuData.lobbyPlayers[i].iTeamPos, ", coronaMenuData.lobbyPlayers[i].iTeamRow", coronaMenuData.lobbyPlayers[i].iTeamRow, ", coronaMenuData.lobbyPlayers[i].iTeam = ", coronaMenuData.lobbyPlayers[i].iTeam, ", iCurrentTeam = ", iCurrentTeam, ", coronaMenuData.lobbyPlayers[i].iTeamPosIndex = ", coronaMenuData.lobbyPlayers[i].iTeamPosIndex)
						ENDIF
						#ENDIF
				
						IF coronaMenuData.lobbyPlayers[i].iTeamPos = CORONA_PLAYER_TEAM_POS_AT_EDGE
						OR coronaMenuData.lobbyPlayers[i].iTeamRow <= 1
							IF coronaMenuData.lobbyPlayers[i].iTeam = iCurrentTeam
								
								iTeam = coronaMenuData.lobbyPlayers[i].iTeam
								
								IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FINALE_FLAG)
								OR IS_CORONA_BIT_SET(CORONA_NO_TEAM_OUTFITS)
									iTeam = -1
								ENDIF
								
								bForceRender = FALSE
									
								// Get player poz with offset
								vPlayerPedPos = GET_WORLD_POSITION_OF_ENTITY_BONE(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(playerId)].pedID, GET_PED_BONE_INDEX(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(playerId)].pedID, BONETAG_NECK))
								
								#IF IS_DEBUG_BUILD
								IF bDebugPrint
									PRINTLN("[JA@TAGS] ***************** ", GET_PLAYER_NAME(playerId), " *************")
									PRINTLN("[JA@TAGS] vPlayerPedPos = ", vPlayerPedPos)
								ENDIF
								#ENDIF
								
								// Offset it to lowest value
								vPlayerPedPos.z += fTeamDMOffsetZ
								
								#IF IS_DEBUG_BUILD
								IF bDebugPrint
									PRINTLN("[JA@TAGS] vPlayerPedPos += fTeamDMOffsetZ = ", vPlayerPedPos)
								ENDIF
								#ENDIF
								
								// Get screen space coord
								fX = 0.0
								fY = 0.0
								GET_SCREEN_COORD_FROM_WORLD_COORD(vPlayerPedPos, fX, fY)
								
								#IF IS_DEBUG_BUILD
								IF bDebugPrint
									PRINTLN("[JA@TAGS] Before: fX = ", fX, ", fY = ", fY)
								ENDIF
								#ENDIF
								
								IF bInitialisedPos
									fY = fIntialYPos
								ELSE
									fIntialYPos = fY
									bInitialisedPos = TRUE
									
									IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
										iInitialRow = GET_CORONA_PED_POSITION_TAG_OFFSET(iTeam, coronaMenuData.lobbyPlayers[i].iTeamPosIndex)
										
										#IF IS_DEBUG_BUILD
										IF bDebugPrint
											PRINTLN("[JA@TAGS] iInitialRow: ", iInitialRow)
										ENDIF
										#ENDIF
									ENDIF
								ENDIF
																
								// If we have already saved out our gamertag position, use this (so we don't get large jumps)
								IF coronaMenuData.lobbyPlayers[i].fNameX != -1.0
								AND coronaMenuData.lobbyPlayers[i].fNameY != 1.0
								AND NOT IS_CORONA_BIT_SET(CORONA_REFRESH_PLAYER_TITLES)
									fX = coronaMenuData.lobbyPlayers[i].fNameX 
									fY = coronaMenuData.lobbyPlayers[i].fNameY
								ELSE
									// Else, save out these values so we will use them going forward
									coronaMenuData.lobbyPlayers[i].fNameX = fX 
									coronaMenuData.lobbyPlayers[i].fNameY = FY
									
									#IF IS_DEBUG_BUILD
									IF bDebugPrint
										PRINTLN("[JA@TAGS] Save out the positions to lobbyPlayers:")
										PRINTLN("[JA@TAGS] 					- coronaMenuData.lobbyPlayers[i].fNameX = ", coronaMenuData.lobbyPlayers[i].fNameX)
										PRINTLN("[JA@TAGS] 					- coronaMenuData.lobbyPlayers[i].fNameY = ", coronaMenuData.lobbyPlayers[i].fNameY)
									ENDIF
									#ENDIF
								ENDIF
								
								// IF we are in a bespoke placement with 1 team, force the 4th gamer on screen (to overcome the scaleform limit)
								IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
									IF (g_FMMC_STRUCT.iNumberOfTeams = 1
									OR IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FINALE_FLAG))
									AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
									AND NOT IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
									#IF FEATURE_HEIST_ISLAND
									AND NOT IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
									#ENDIF
									
										// Check we are the 4th ped (on the left hand side)
										IF coronaMenuData.lobbyPlayers[i].iTeamPosIndex = 3
											
											#IF IS_DEBUG_BUILD
											IF bDebugPrint
												PRINTLN("[JA@TAGS] Adjust our gamertag position for 4th position")
											ENDIF
											#ENDIF
											
											fX = g_fCoronaTeam4ThPosX
											fY = g_fCoronaTeam4ThPosY
											
											bForceRender = TRUE
										ENDIF
									ENDIF
								ENDIF
								
								#IF IS_DEBUG_BUILD
								IF bDebugPrint
									PRINTLN("[JA@TAGS] After: fX = ", fX, ", fY = ", fY)
								ENDIF
								#ENDIF
								
								// Get scaleform screen space coord
								INT iSFPixelPosX, iSFPixelPosY 			//TEMP REMOVED CLAMP FOR BUG 1691095
								iSFPixelPosX = IMAX(0, (( ROUND(fX * iResX) ) - ( ( iResX - I_CORONA_SF_PIXEL_WIDTH )/2 )))	//CLAMP_INT( ( ROUND(fX * iResX) ) - ( ( iResX - I_CORONA_SF_PIXEL_WIDTH )/2 ), 0, I_CORONA_SF_PIXEL_WIDTH)
								iSFPixelPosY = CLAMP_INT( ( ROUND(fY * iResY) ) - ( ( iResY - I_CORONA_SF_PIXEL_HEIGHT )/2 ), 0, I_CORONA_SF_PIXEL_HEIGHT)
								
								#IF IS_DEBUG_BUILD
								IF bDebugPrint
									PRINTLN("[JA@TAGS] Before: iSFPixelPosX = ", iSFPixelPosX, ", iSFPixelPosY = ", iSFPixelPosY)
									PRINTLN("[JA@TAGS] Before: iSFPixelPosX = ", iSFPixelPosX, ", iSFPixelPosY = ", iSFPixelPosY)
								ENDIF
								#ENDIF
								
								// Finally offset our position by the row we were on.								
								IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
									iSFPixelPosY -= ROUND((GET_CORONA_PED_POSITION_TAG_OFFSET(iTeam, coronaMenuData.lobbyPlayers[i].iTeamPosIndex) - iInitialRow) * (fTeamDMOffsetRows * iResY))
								
									#IF IS_DEBUG_BUILD
									IF bDebugPrint
										PRINTLN("[JA@TAGS] GET_CORONA_PED_POSITION_TAG_OFFSET(iTeam, iTeamPosIndex) = ", GET_CORONA_PED_POSITION_TAG_OFFSET(iTeam, coronaMenuData.lobbyPlayers[i].iTeamPosIndex))
									ENDIF
									#ENDIF
								ELSE
									iSFPixelPosY -= ROUND(coronaMenuData.lobbyPlayers[i].iTeamRow * (fTeamDMOffsetRows * iResY))
								ENDIF
								
								#IF IS_DEBUG_BUILD
								IF bDebugPrint
									PRINTLN("[JA@TAGS] After: iSFPixelPosX = ", iSFPixelPosX, ", iSFPixelPosY = ", iSFPixelPosY)
								ENDIF
								#ENDIF								
								
								// Gamertag
								strPlayerName 	= GET_PLAYER_NAME(playerId)	
								INT iNameWidth 	= CEIL( GET_LENGTH_OF_LITERAL_STRING(strPlayerName) / 2.0 ) * iTeamNameWidthPerCharacter
								
								IF IS_ENTITY_VISIBLE(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(playerId)].pedID)	
								
									// We need to validate the position on the screen to check we are not intercepting any of the UI
									IF IS_CORONA_SCREEN_SPACE_COORD_VALID(<<iSFPixelPosX, iSFPixelPosY, 0>>, <<iNameWidth, 18, 0>>)
									OR bForceRender
									
										//PRINTLN("[JA@TAGS] VALID: DRAW")
									
										// Status
										TEXT_LABEL_15 strReadyStatus
										HUD_COLOURS eColReadyStatus
										
										// Only show ready status when there is more than 1 player on a team
										IF iNumOnThisTeam > 1
											IF Has_Player_Voted(playerId)
												strReadyStatus	= "FMMC_MENU_RED"
												eColReadyStatus = HUD_COLOUR_GREEN
											ELSE
												strReadyStatus	= "FMMC_MENU_UNR"
												eColReadyStatus = HUD_COLOUR_RED
											ENDIF	
										ELSE
											strReadyStatus	= ""
										ENDIF
										
										MP_ICONS_ENUMS eIcon
										IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
											IF GlobalPlayerBD_FM[NATIVE_TO_INT(playerId)].sClientCoronaData.iPreferredRole = 0
												eIcon = LOBBY_DRIVER
											ELIF GlobalPlayerBD_FM[NATIVE_TO_INT(playerId)].sClientCoronaData.iPreferredRole = 1
												eIcon = LOBBY_CODRIVER
											ENDIF
										ELSE
											eIcon = ICON_EMPTY
										ENDIF
										
	//									// Optional title only displayed for the first 2 rows
	//									IF coronaMenuData.lobbyPlayers[i].iTeamRow < 2
	//										strPlayerTitle 	= GET_MP_PLAYER_STATS_TITLE_STRING(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].sClientCoronaData.playerTitle)
	//									ELSE
	//										strPlayerTitle	= ""
	//									ENDIF
	//									
	//									SWITCH coronaMenuData.lobbyPlayers[i].iTeamRow
	//										CASE 0
	//											iAlpha = 100
	//										BREAK
	//										CASE 1
	//											iAlpha = 75
	//										BREAK
	//										CASE 2
	//										CASE 3
	//										CASE 4
	//											iAlpha = 50
	//										BREAK
	//									ENDSWITCH
											
										eColPlayerName = CORONA_GET_SUMMARY_HIGHLIGHT_COLOUR(coronaMenuData, i, g_FMMC_STRUCT.iMissionType)
				
										IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FINALE_FLAG)
											IF GlobalPlayerBD_FM[NATIVE_TO_INT(playerId)].sClientCoronaData.iTeamChosen != -1
												strPlayerTitle = GET_CORONA_TEAM_NAME(GlobalPlayerBD_FM[NATIVE_TO_INT(playerId)].sClientCoronaData.iTeamChosen)
												//PRINTLN("[TITLES@CORONA] UPDATE_CORONA_TEAM_MEMBER_PLAYER_TITLES - Getting player team for Finale = ", strPlayerTitle)
											ENDIF
										ENDIF
										
										IF IS_CORONA_MISSION_SUB_ROLE_ACTIVE()
										AND GET_PLAYER_CHOSEN_MISSION_TEAM_ROLE(playerID) != -1
											strPlayerTitle = GET_CORONA_MISSION_SUB_ROLE_NAME(GlobalPlayerBD_FM[NATIVE_TO_INT(playerId)].sClientCoronaData.iTeamChosen, GET_PLAYER_CHOSEN_MISSION_TEAM_ROLE(playerID))				
										ENDIF
											
										IF coronaMenuData.iPlayerTitlesAlpha = 0
										
											coronaMenuData.lobbyPlayers[i].fNameX = -1.0
											coronaMenuData.lobbyPlayers[i].fNameY = -1.0
										
											CLEAR_FRONTEND_DATA_SLOT(CORONA_COLUMN_EIGHT)
											SET_FRONTEND_DISPLAY_COLUMN(CORONA_COLUMN_EIGHT, FALSE)
										ELSE								
											// Call the scaleform method to update the UI
											SET_FRONTEND_SMALL_FANCY_TEXT_DETAILS(iCounter, iSFPixelPosX, iSFPixelPosY,
												strPlayerName, eColPlayerName, strPlayerTitle, strReadyStatus, eColReadyStatus, "", HUD_COLOUR_WHITE, "", HUD_COLOUR_WHITE, coronaMenuData.iPlayerTitlesAlpha, eIcon)
												
											iCounter++
										ENDIF
									ELSE
										// Wipe these values if not safe to show (hidden so can recalculate next time)
										coronaMenuData.lobbyPlayers[i].fNameX = -1.0
										coronaMenuData.lobbyPlayers[i].fNameY = -1.0
									ENDIF
								ELSE
									coronaMenuData.lobbyPlayers[i].fNameX = -1.0
									coronaMenuData.lobbyPlayers[i].fNameY = -1.0
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[TITLES@CORONA] UPDATE_CORONA_TEAM_MEMBER_PLAYER_TITLES - IS NOT AT THE EDGE player = ", i)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDREPEAT
		
		// Clear this flag as will only ever want to do this once after clearing the player titles
		CLEAR_CORONA_BIT(CORONA_REFRESH_PLAYER_TITLES)
		
		DISPLAY_FRONTEND_DATA_SLOT(CORONA_COLUMN_EIGHT)
			
		SET_FRONTEND_DISPLAY_COLUMN(CORONA_COLUMN_EIGHT, TRUE)
		
		SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	
	// Turn off player titles
	ELSE		
		coronaMenuData.iPlayerTitlesAlpha = 0 // Reset the alpha, so when they come back on they fade in
	
		CLEAR_FRONTEND_DATA_SLOT(CORONA_COLUMN_EIGHT)
		SET_FRONTEND_DISPLAY_COLUMN(CORONA_COLUMN_EIGHT, FALSE)
		
		IF NOT IS_CORONA_BIT_SET(CORONA_REFRESH_PLAYER_TITLES)
		
			SET_CORONA_BIT(CORONA_REFRESH_PLAYER_TITLES)
		ENDIF
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE				
//			PRINTLN("[CORONA] ------>>>>> IS A RACE")
//		ENDIF
//		IF NOT IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)		
//			PRINTLN("[CORONA] ------>>>>> IS NOT A TEAM CORONA")
//		ENDIF
//		IF IS_CORONA_BIT_SET(CORONA_PROCESS_SLOT_SELECTION_CAMERA)	
//			PRINTLN("[CORONA] ------>>>>> CORONA CAMERA ANIMATING")
//		ENDIF
//		IF IS_CORONA_BIT_SET(CORONA_DISPLAYING_THIRD_DETAIL_PANEL)
//			PRINTLN("[CORONA] ------>>>>> CORONA_DISPLAYING_THIRD_DETAIL_PANEL")
//		ENDIF
//		IF coronaMenuData.coronaSummaryCardState != CORONA_SUMMARY_CARD_HIDE
//			PRINTLN("[CORONA] ------>>>>> coronaSummaryCardState != CORONA_SUMMARY_CARD_HIDE - real value = ", enum_to_int(coronaMenuData.coronaSummaryCardState))
//		ENDIF
//	#ENDIF

ENDPROC

/// PURPOSE: Listen for user control of camera
PROC MAINTAIN_CORONA_CAMERA_CONTROL(CORONA_MENU_DATA &coronaMenuData)

	CONST_FLOAT F_ALPHA_DEAD_ZONE				0.1
	CONST_INT	I_APLHA_MIN						0
	CONST_INT	I_APLHA_FADE_RATE				400 // amount change per second (200 = 0.5s to go from full to zero)
	CONST_FLOAT F_CAMERA_DEVIATION_TOLERENCE	0.01

	//Lukes desired offset/position
	//ATTACH_CAM_TO_ENTITY(cam, entity, <<0.0962, 1.6831, 0.2997>>)
	//POINT_CAM_AT_ENTITY(cam, entity, <<0.2827, -1.2954, 0.6065>>)
	//SET_CAM_FOV(cam, 40.0000)
	

	// Allow player to move the camera
	INT iLeftX, iLeftY, iRightX, iRightY
	BOOL bUpdateCamera
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
	iRightX *= -1
	IF IS_LOOK_INVERTED()
		iRightY	*= -1
	ENDIF
	
	FLOAT fXLimitLeft = -15.0
	FLOAT fXLimitRight = 110.0
	
	UPDATE_MENU_CURSOR_GLOBALS()
		
	// Keyboard and mouse camera movement
	IF IS_USING_KEYBOARD_AND_MOUSE( FRONTEND_CONTROL)
			
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_X)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_Y)
		
		IF PAUSE_MENU_GET_MOUSE_HOVER_INDEX() != -1
		OR IS_MOUSE_ROLLED_OVER_INSTRUCTIONAL_BUTTONS()
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
		ELSE
	
			IF g_fMenuCursorX > 0.40
			AND g_fMenuCursorY < 0.8
			
				IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
					
					//IF g_fMenuCursorX != 0
					//AND g_fMenuCursorY != 0
						bUpdateCamera = TRUE
					//ENDIF
					
					SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_GRAB)
					
				ELSE
					SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_OPEN)
				ENDIF
			ELSE

				SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
			ENDIF
			
		ENDIF
		
	ENDIF
		
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		///////// Original gamepad camera /////////
	
		// We allow them full cycle of the vehicle
		IF ((iRightX < -16) OR (iRightX > 16))
		AND NOT (iRightX < 0 AND coronaMenuData.fCameraRotationX < fXLimitLeft)
		AND NOT (iRightX > 0 AND coronaMenuData.fCameraRotationX > fXLimitRight)
			bUpdateCamera = TRUE		
			
			IF ABSF(coronaMenuData.desiredXRot) < 25.0
				coronaMenuData.desiredXRot += (iRightX*0.025)
			ELSE
				IF coronaMenuData.desiredXRot > 25.0
					coronaMenuData.desiredXRot = 25.0
				ELIF coronaMenuData.desiredXRot < -25.0
					coronaMenuData.desiredXRot = -25.0
				ENDIF
			ENDIF
		ELSE
			bUpdateCamera = TRUE
			
			IF (iRightX < 0 AND coronaMenuData.fCameraRotationX < fXLimitLeft)
			OR (iRightX > 0 AND coronaMenuData.fCameraRotationX > fXLimitRight)
				coronaMenuData.desiredXRot = coronaMenuData.desiredXRot * (0.5)	//Hit the limit, decel quickly.
			ELSE
				coronaMenuData.desiredXRot = coronaMenuData.desiredXRot * (0.7)	//Just let go of stick decel slowly.
			ENDIF
		ENDIF
		
	ENDIF
	
	FLOAT fYLimitLow, fYLimitHigh
	
	// Get clamp values for Y axis.
	CLAMP_CORONA_VEHICLE_CAMERA(coronaMenuData, fYLimitLow, fYLimitHigh)

	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		///////// Original gamepad camera /////////
	
		IF ((iRightY < -16) OR (iRightY > 16))
		AND NOT (iRightY < 0 AND coronaMenuData.fCameraRotationY < fYLimitLow)
		AND NOT (iRightY > 0 AND coronaMenuData.fCameraRotationY > fYLimitHigh)
//			CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - 1")
			bUpdateCamera = TRUE				
			IF ABSF(coronaMenuData.desiredYRot) < 6.5
				coronaMenuData.desiredYRot += (iRightY*0.025)
//				CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - 2")
			ELSE
//				CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - 3")
				
				IF iRightY > 0 AND coronaMenuData.desiredYRot < 0
					coronaMenuData.desiredYRot = coronaMenuData.desiredYRot + (ABSF(coronaMenuData.desiredYRot)*2)
					CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - iRightY > 0 (down) AND coronaMenuData.desiredYRot < 0 (upward direction) - Corrected")
				ELIF iRightY < 0 AND coronaMenuData.desiredYRot > 0
					coronaMenuData.desiredYRot = coronaMenuData.desiredYRot - (ABSF(coronaMenuData.desiredYRot)*2)
					CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - iRightY < 0 (up) AND coronaMenuData.desiredYRot > 0 (downward direction) - Corrected")
				ENDIF
				
				IF coronaMenuData.desiredYRot > 6.5
					coronaMenuData.desiredYRot = 6.5
				ELIF coronaMenuData.desiredYRot < -6.5
					coronaMenuData.desiredYRot = -6.5
				ENDIF
			ENDIF
		ELSE
//			CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - 4")
			bUpdateCamera = TRUE
			IF (iRightY < 0 AND coronaMenuData.fCameraRotationY < fYLimitLow)
			OR (iRightY > 0 AND coronaMenuData.fCameraRotationY > fYLimitHigh)
//				CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - 5")
				coronaMenuData.desiredYRot = coronaMenuData.desiredYRot * (0.3)	//Hit the limit, decel quickly.
			ELSE
//				CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - 6")
				coronaMenuData.desiredYRot = coronaMenuData.desiredYRot * (0.6)	//Just let go of stick decel slowly.
			ENDIF
		ENDIF
		
//		CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - coronaMenuData.desiredYRot = ", coronaMenuData.desiredYRot)
//		CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - coronaMenuData.fCameraRotationY = ", coronaMenuData.fCameraRotationY)
//		CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - fYLimitLow = ", fYLimitLow)
//		CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - fYLimitHigh = ", fYLimitHigh)
//		CDEBUG1LN(DEBUG_GANG_OPS, "MAINTAIN_CORONA_CAMERA_CONTROL - iRightY = ", iRightY)
	
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_BUTTON_PRESSED(PAD1, LEFTSHOULDER2)
			TEXT_LABEL_15 debugText 
			debugText += ROUND(coronaMenuData.desiredXRot)
			debugText += " "
			debugText += iRightX
			debugText += " "
			debugText += ROUND(coronaMenuData.fCameraRotationX)
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(),debugText, 0.2)
		ENDIF
	#ENDIF
	
	// Mouse camera
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)

		IF bUpdateCamera
			coronaMenuData.desiredXRot = -(g_fMenuCursorXMoveDistance * 150)
			coronaMenuData.fCameraRotationX = coronaMenuData.fCameraRotationX + coronaMenuData.desiredXRot
			coronaMenuData.fCameraRotationX = CLAMP(coronaMenuData.fCameraRotationX, fXLimitLeft, fXLimitRight)
			
			coronaMenuData.desiredYRot = (g_fMenuCursorYMoveDistance * 15)
			coronaMenuData.fCameraRotationY = coronaMenuData.fCameraRotationY + coronaMenuData.desiredYRot
			coronaMenuData.fCameraRotationY = CLAMP(coronaMenuData.fCameraRotationY, fYLimitLow, fYLimitHigh)
		ENDIF

	ELSE
	// Gamepad camera update
		coronaMenuData.fCameraRotationX = coronaMenuData.fCameraRotationX +@ ((2.75 * coronaMenuData.desiredXRot))
		coronaMenuData.fCameraRotationY = coronaMenuData.fCameraRotationY +@ ((0.5 * coronaMenuData.desiredYRot))  
	ENDIF
		
	FLOAT fCamRotSpeed = VMAG(<<coronaMenuData.desiredXRot, coronaMenuData.desiredYRot, 0.0>>)

	// Chase alpha up
	IF fCamRotSpeed <= F_ALPHA_DEAD_ZONE
		IF coronaMenuData.iPlayerTitlesAlpha < 100
			coronaMenuData.iPlayerTitlesAlpha += ROUND( TIMESTEP() * I_APLHA_FADE_RATE )
			coronaMenuData.iPlayerTitlesAlpha = CLAMP_INT(coronaMenuData.iPlayerTitlesAlpha, I_APLHA_MIN, 100)
		ENDIF
		
	// Chase alpha down
	ELSE
		IF coronaMenuData.iPlayerTitlesAlpha > I_APLHA_MIN
			coronaMenuData.iPlayerTitlesAlpha -= ROUND( TIMESTEP() * I_APLHA_FADE_RATE )
			coronaMenuData.iPlayerTitlesAlpha = CLAMP_INT(coronaMenuData.iPlayerTitlesAlpha, I_APLHA_MIN, 100)
		ENDIF
	ENDIF
	
	IF bUpdateCamera
	AND coronaMenuData.coronaCamMode != CORONA_CAMERA_MODE_AIR
	AND coronaMenuData.coronaCamMode != CORONA_CAMERA_MODE_AIR_HELI
		SET_CORONA_CAMERA_FOR_VEHICLE(coronaMenuData)
	ENDIF
	
	IF coronaMenuData.iPlayerTitlesAlpha < 100
	OR fCamRotSpeed > F_CAMERA_DEVIATION_TOLERENCE		
		UPDATE_CORONA_TEAM_MEMBER_PLAYER_TITLES(coronaMenuData)
	ENDIF
	
//	IF NOT IS_CAM_SHAKING(coronaMenuData.ciCoronaCam)
//	AND ABSF(coronaMenuData.desiredXRot) < 0.1
//	AND ABSF(coronaMenuData.desiredYRot) < 0.1
////		SHAKE_CAM(coronaMenuData.ciCoronaCam, "HAND_SHAKE", 0.3)
//	ENDIF
	
ENDPROC

/// PURPOSE: Cycles through corona player list and finds next valid slot position to view
FUNC BOOL FIND_NEXT_VALID_CORONA_SLOT(CORONA_MENU_DATA &coronaMenuData, INT iIncrement, INT &iNewSlot)

	iNewSlot = coronaMenuData.iCoronaSlotTarget
	
	WHILE (TRUE)
	
		// Change the slot we are looking at
		iNewSlot += iIncrement
		
		// Have we reached left hand edge of list
		IF iNewSlot < 0
			PRINTLN("[CORONA] FIND_NEXT_VALID_CORONA_SLOT - iNewSlot < 0: loop to end of list")
			
			iNewSlot = (NUM_NETWORK_PLAYERS-1)
		ENDIF
		
		// Have we reached right hand edge of list
		IF iNewSlot >= NUM_NETWORK_PLAYERS
			PRINTLN("[CORONA] FIND_NEXT_VALID_CORONA_SLOT - iNewSlot >= NUM_NETWORK_PLAYERS, iNewSlot back to 0")
		
			iNewSlot = 0
		ENDIF
		
		// If player is on tutorial and we could point to tutorial ped, return
		IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
			IF iNewSlot = coronaMenuData.iTutorialPedSlot
				PRINTLN("[CORONA] FIND_NEXT_VALID_CORONA_SLOT - found valid player: Lamar (TUTORIAL) in slot: ", iNewSlot)
						
				RETURN TRUE
			ENDIF
		ENDIF
		
		// We have looped so nothing there for us to move to
		IF iNewSlot = coronaMenuData.iCoronaSlotTarget
			RETURN FALSE
		ENDIF
		
		// Otherwise, check to see if the slot has valid player
		IF IS_BIT_SET(coronaMenuData.iCoronaSlotsActive, iNewSlot)//coronaMenuData.lobbyPlayers[iNewSlot].playerID != INVALID_PLAYER_INDEX()
			PRINTLN("[CORONA] FIND_NEXT_VALID_CORONA_SLOT - found valid slot: ", iNewSlot)
			
			IF NOT IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
			
				coronaMenuData.piViewedPlayer = GET_CORONA_PLAYER_IN_SLOT(coronaMenuData, iNewSlot)
				PRINTLN("[CORONA] FIND_NEXT_VALID_CORONA_SLOT - not a team corona so update piViewedPlayer: ", GET_PLAYER_NAME(coronaMenuData.piViewedPlayer))
			ENDIF
			
			//...yes, return our new slot index
			RETURN TRUE
		ENDIF
			
		// Loop
	ENDWHILE

	RETURN FALSE
ENDFUNC

/// PURPOSE: Loops to find the next valid player the dev can spectate
FUNC BOOL FIND_NEXT_DEV_SPECTATOR_PLAYER(CORONA_MENU_DATA &coronaMenuData, INT iIncrement)
	
	INT iCurrentSpec = coronaMenuData.iDevSpectatorIndex
	
	BOOL bFound
	BOOL bFoundNew
	
	WHILE NOT bFound
	
		// Change the slot we are looking at
		iCurrentSpec += iIncrement
		
		// Have we reached left hand edge of list
		IF iCurrentSpec < 0
			PRINTLN("[CORONA] FIND_NEXT_DEV_SPECTATOR_PLAYER - iCurrentSpec < 0: loop to end of list")
			
			iCurrentSpec = (NUM_NETWORK_PLAYERS-1)
		ENDIF
		
		// Have we reached right hand edge of list
		IF iCurrentSpec >= NUM_NETWORK_PLAYERS
			PRINTLN("[CORONA] FIND_NEXT_DEV_SPECTATOR_PLAYER - iCurrentSpec >= NUM_NETWORK_PLAYERS, iCurrentSpec back to 0")
		
			iCurrentSpec = 0
		ENDIF
				
		// We have looped so nothing there for us to move to
		IF iCurrentSpec = coronaMenuData.iDevSpectatorIndex
			bFound = TRUE
			bFoundNew = FALSE
		ELSE
			IF coronaMenuData.lobbyPlayers[iCurrentSpec].playerID != INVALID_PLAYER_INDEX()
				bFound = TRUE
				bFoundNew = TRUE
			ENDIF
		ENDIF
					
		// Loop
	ENDWHILE
	
	coronaMenuData.iDevSpectatorIndex = iCurrentSpec
	
	RETURN bFoundNew	
ENDFUNC

FUNC BOOL HAS_VIEW_TEAMMATE_BUTTON_BEEN_PRESSED()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_TEAM_TO_VIEW_FROM_MY_TEAM()
	SWITCH GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
		CASE ciCORONA_JUG_ATTACK_TEAM_1	RETURN ciCORONA_JUG_JUGGERNAUT_TEAM_1
		CASE ciCORONA_JUG_ATTACK_TEAM_2	RETURN ciCORONA_JUG_JUGGERNAUT_TEAM_2
		CASE ciCORONA_JUG_JUGGERNAUT_TEAM_1	RETURN ciCORONA_JUG_ATTACK_TEAM_1
		CASE ciCORONA_JUG_JUGGERNAUT_TEAM_2	RETURN ciCORONA_JUG_ATTACK_TEAM_2
	ENDSWITCH

	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
ENDFUNC

FUNC INT GET_NEW_SLOT_FROM_CURRENT_SELECTION(INT iCurrentSlot, INT iIncrement, INT iBackupNewSlot)
	INT iToReturn = iBackupNewSlot

	SWITCH iCurrentSlot
		CASE ciCORONA_JUG_ATTACK_TEAM_1
			IF iIncrement = 1
				iToReturn = ciCORONA_JUG_JUGGERNAUT_TEAM_1
			ELSE
				iToReturn  = ciCORONA_JUG_JUGGERNAUT_TEAM_2
			ENDIF
		BREAK
		
		CASE ciCORONA_JUG_ATTACK_TEAM_2
			IF iIncrement = 1
				iToReturn = ciCORONA_JUG_JUGGERNAUT_TEAM_2
			ELSE
				iToReturn  = ciCORONA_JUG_JUGGERNAUT_TEAM_1
			ENDIF
		BREAK
		
		CASE ciCORONA_JUG_JUGGERNAUT_TEAM_1
			IF iIncrement = 1
				iToReturn = ciCORONA_JUG_ATTACK_TEAM_2
			ELSE
				iToReturn  = ciCORONA_JUG_ATTACK_TEAM_1
			ENDIF
		BREAK
		
		CASE ciCORONA_JUG_JUGGERNAUT_TEAM_2
			IF iIncrement = 1
				iToReturn = ciCORONA_JUG_ATTACK_TEAM_1
			ELSE
				iToReturn  = ciCORONA_JUG_ATTACK_TEAM_2
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN iToReturn
ENDFUNC

/// PURPOSE: Listens to see if the player is moving left and right in the corona. Finds next valid slot.
FUNC BOOL PROCESS_CORONA_SLOT_SELECTION(CORONA_MENU_DATA &coronaMenuData)


	INT iIncrement
	BOOL bButtonPressed
	
	IF SHOULD_SELECTION_BE_INCREMENTED(coronaMenuData.lobbyOptionTimer, iIncrement, bButtonPressed, TRUE, TRUE, -1, FALSE, coronaMenuData.iCurrentSelection)

		INT iNewSlot
		IF FIND_NEXT_VALID_CORONA_SLOT(coronaMenuData, iIncrement, iNewSlot)
			coronaMenuData.iMovingLeft = iIncrement
		
			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		
			// Update our global corona data
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
				coronaMenuData.iCoronaSlotTarget = GET_NEW_SLOT_FROM_CURRENT_SELECTION(coronaMenuData.iCoronaSlotTarget, iIncrement, iNewSlot)
			
				IF coronaMenuData.iCoronaSlotTarget = GET_TEAM_TO_VIEW_FROM_MY_TEAM()
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_VIEW_TEAMMATES"))
					PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CORONA_VIEW_MYTEAM"))
				ELSE
					PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_VIEW_MYTEAM"))
					PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CORONA_VIEW_TEAMMATES"))
				ENDIF
			ELSE
				coronaMenuData.iCoronaSlotTarget = iNewSlot
			ENDIF
		
			// Refresh our bet amount for selected player
			UPDATE_CASH_BET_ON_CORONA_PLAYER(GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData)))

			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Listen for the dev spectator cycling through the players on vehicle screen 
FUNC BOOL PROCESS_CORONA_DEV_SPECTATOR_PLAYER_SELECTION(CORONA_MENU_DATA &coronaMenuData, BOOL bForceChange = FALSE)
	
	INT iIncrement = 1
	BOOL bButtonPressed
	
	// Listen for if the option has been scrolled
	IF SHOULD_SELECTION_BE_INCREMENTED(coronaMenuData.lobbyOptionTimer, iIncrement, bButtonPressed, TRUE, TRUE, -1, FALSE, coronaMenuData.iCurrentSelection)
	OR bForceChange

		// Find a valid player that we can spectate while on the vehicle screen
		IF FIND_NEXT_DEV_SPECTATOR_PLAYER(coronaMenuData, iIncrement)
			
			coronaMenuData.piRacePartner = coronaMenuData.lobbyPlayers[coronaMenuData.iDevSpectatorIndex].playerId
			PRINTLN("[CORONA] PROCESS_CORONA_DEV_SPECTATOR_PLAYER_SELECTION - New player to observe: ", GET_PLAYER_NAME(coronaMenuData.piRacePartner))
			
			IF NOT bForceChange
				PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			ENDIF
		
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

/// PURPOSE: Maintains the player the dev spectator is viewing and ensure they are still valid
FUNC BOOL MAINTAIN_CORONA_DEV_SPECTATOR_PLAYER_VALID(CORONA_MENU_DATA &coronaMenuData)
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	AND IS_ROCKSTAR_DEV()
	
		IF coronaMenuData.piRacePartner = INVALID_PLAYER_INDEX()
		OR NOT IS_NET_PLAYER_OK(coronaMenuData.piRacePartner, FALSE, TRUE)
			
			PRINTLN("[CORONA] MAINTAIN_CORONA_DEV_SPECTATOR_PLAYER_VALID - Our spectated player is no longer valid, switch.")
			
			IF PROCESS_CORONA_DEV_SPECTATOR_PLAYER_SELECTION(coronaMenuData, TRUE)
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_CORONA_SLOTS_ACTIVE(CORONA_MENU_DATA &coronaMenuData, INT iNumSlots)

	INT i
	REPEAT iNumSlots i

		IF NOT IS_BIT_SET(coronaMenuData.iCoronaSlotsActive, i)
			PRINTLN("[CORONA] UPDATE_CORONA_SLOTS_ACTIVE - setting slot as active: ", i)			
			
			SET_BIT(coronaMenuData.iCoronaSlotsActive, i)		
		ENDIF
		
	ENDREPEAT
	
	
ENDPROC

//VECTOR vOffsetFromOriginalPosition

FUNC BOOL GRACEFULLY_REMOVE_VEHICLE(CORONA_MENU_DATA &coronaMenuData)

	DELETE_VEHICLE(coronaMenuData.vehDisplayedVehicle)
	
	RETURN TRUE

//	VECTOR vVehicleCoords
//	FLOAT fVehicleHeading
//	
//	//IF NOT IS_ENTITY_ON_SCREEN(coronaMenuData.vehDisplayedVehicle)
//	IF vOffsetFromOriginalPosition.y > 15.0
//		//CLEANUP_CORONA_VEHICLE(coronaMenuData, TRUE, FALSE)
//		RETURN TRUE
//	ELSE
//	
//		IF NOT IS_ENTITY_DEAD(coronaMenuData.vehDisplayedVehicle)
//			SET_ENTITY_COLLISION(coronaMenuData.vehDisplayedVehicle, FALSE)
//			
//			IF VMAG(vVehicleCoords) = 0.0
//				vVehicleCoords = GET_ENTITY_COORDS(coronaMenuData.vehDisplayedVehicle)
//				fVehicleHeading = GET_ENTITY_HEADING(coronaMenuData.vehDisplayedVehicle)
//			ENDIF
//			
//			//SET_ENTITY_ALPHA
//			
//			vOffsetFromOriginalPosition.y = vOffsetFromOriginalPosition.y +@ 10.5
//			
//			
////			vVehicleCoords.z = vVehicleCoords.z -@ 3.5
//		
//			SET_ENTITY_COORDS(coronaMenuData.vehDisplayedVehicle, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vVehicleCoords, fVehicleHeading , vOffsetFromOriginalPosition))
//		ENDIF
//		
//	ENDIF
//	
//	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Creates a light rig used throughout the corona (vehicle, betting screens)
PROC CREATE_CORONA_LIGHT_RIG(CORONA_MENU_DATA &coronaMenuData, MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails, MODEL_NAMES mnLightRig)
	
	IF IS_THIS_MISSION_OF_TYPE_HEIST(	sLaunchMissionDetails.iMissionType, 
										sLaunchMissionDetails.bIsHeistMission 
										 , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
	OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
	#IF FEATURE_CASINO_HEIST
	OR IS_THIS_MISSION_A_CASINO_HEIST_MISSION(sLaunchMissionDetails.iRootContentIDHash)
	#ENDIF
	
		PRINTLN("[CORONA] CREATE_CORONA_LIGHT_RIG - Do not setup the light rig for Heists")
		EXIT											
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(coronaMenuData.vehLightRig)
		IF REQUEST_LOAD_MODEL(mnLightRig)
	
			VECTOR vVehPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			PRINTLN("[CORONA] CREATE_CORONA_LIGHT_RIG - Creating a light rig for the corona")
			
			coronaMenuData.vehLightRig = CREATE_OBJECT(mnLightRig, vVehPos, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnLightRig)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Allows us to attach a light rig to the corona player
PROC CORONA_ATTACH_LIGHT_RIG_TO_PLAYER(CORONA_MENU_DATA &coronaMenuData, MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)

	// Currently bail for heists
	IF IS_THIS_MISSION_OF_TYPE_HEIST(	sLaunchMissionDetails.iMissionType, 
										sLaunchMissionDetails.bIsHeistMission 
										 , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
										
		PRINTLN("[CORONA] CREATE_CORONA_LIGHT_RIG - Do not setup the light rig for Heists")
		EXIT											
	ENDIF

	// Create the light rig
	CREATE_CORONA_LIGHT_RIG(coronaMenuData, sLaunchMissionDetails, LIGHT_CAR_RIG)

	PED_INDEX aPed
	
	// Find the relevant ped for either team mode or single betting
	IF coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_TEAM
		
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			
			IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			AND coronaMenuData.lobbyPlayers[i].iTeam = GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData))
				aPed = g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID
				i = NUM_NETWORK_PLAYERS
			ENDIF
		ENDREPEAT		
	ELSE
		IF coronaMenuData.piViewedPlayer != INVALID_PLAYER_INDEX()
			aPed = g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.piViewedPlayer)].pedID
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(aPed)
	AND NOT IS_ENTITY_DEAD(aPed)
		IF DOES_ENTITY_EXIST(coronaMenuData.vehLightRig)
			PRINTLN("[CORONA] CORONA_ATTACH_LIGHT_RIG_TO_PLAYER - Set the light rig to player: ", GET_PLAYER_NAME(coronaMenuData.piViewedPlayer))
			
			VECTOR vPlayerCoords = GET_ENTITY_COORDS(aPed, FALSE)
			SET_ENTITY_COORDS(coronaMenuData.vehLightRig, vPlayerCoords, FALSE)
			
			VECTOR vPlayerRot = GET_ENTITY_ROTATION(aPed)
			SET_ENTITY_ROTATION(coronaMenuData.vehLightRig, <<0, 0, (vPlayerRot.z - 180.0)>>)
			
			PRINTLN("[CORONA] CORONA_ATTACH_LIGHT_RIG_TO_PLAYER - Set the light rig to player: ", GET_PLAYER_NAME(coronaMenuData.piViewedPlayer), " vPlayerCoords = ", vPlayerCoords, " vPlayerRot = ", vPlayerRot)
			
			//ATTACH_ENTITY_TO_ENTITY(coronaMenuData.vehLightRig, aPed, 0, <<0, 0, 0>>, <<0, 0, 0>>)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the: Model name, property location, coordinates & roation of the corona light rig depending on which team is currently viewed
PROC GET_CORONA_LIGHT_RIG_PROPERTIES(INT &iCurrentViewedTeam, MODEL_NAMES &sLightRigModel, INT &iLightRigPropertyLoc, OBJECT_INDEX &vehLightRig, VECTOR &vObjCoord, VECTOR &vObjRot)
	
	SWITCH iCurrentViewedTeam
		CASE 0	
			sLightRigModel = HEI_MPH_SELECTCLOTHSLRIG_01  		// Light rig for team 0 in the apartment
			iLightRigPropertyLoc = MP_PROP_ELEMENT_BESPOKE_CORONA_LIGHT_RIG_TEAM_0
			PRINTLN("[CORONA] SETUP_CORONA_BESPOKE_LIGHT_RIG - Choosing light rig = HEI_MPH_SELECTCLOTHSLRIG")
		BREAK
		CASE 1
			sLightRigModel = HEI_MPH_SELECTCLOTHSLRIG			// Light rig for team 1 in the apartment
			iLightRigPropertyLoc = MP_PROP_ELEMENT_BESPOKE_CORONA_LIGHT_RIG_TEAM_1
			PRINTLN("[CORONA] SETUP_CORONA_BESPOKE_LIGHT_RIG - Choosing light rig = HEI_MPH_SELECTCLOTHSLRIG_01")
		BREAK
		CASE 2
			sLightRigModel = HEI_MPH_SELECTCLOTHSLRIG_03  		// Light rig for team 2 in the apartment
			iLightRigPropertyLoc = MP_PROP_ELEMENT_BESPOKE_CORONA_LIGHT_RIG_TEAM_2
			PRINTLN("[CORONA] SETUP_CORONA_BESPOKE_LIGHT_RIG - Choosing light rig = HEI_MPH_SELECTCLOTHSLRIG_03")
		BREAK
		CASE 3				
			sLightRigModel = HEI_MPH_SELECTCLOTHSLRIG_02 		// Light rig for team 3 in the apartment
			iLightRigPropertyLoc = MP_PROP_ELEMENT_BESPOKE_CORONA_LIGHT_RIG_TEAM_3
			PRINTLN("[CORONA] SETUP_CORONA_BESPOKE_LIGHT_RIG - Choosing light rig = HEI_MPH_SELECTCLOTHSLRIG_02")
		BREAK
	ENDSWITCH
	
	// Special case for the invalid team (Finale) lighting
	IF IS_CORONA_BIT_SET(CORONA_NO_TEAM_OUTFITS)
		sLightRigModel = HEI_MPH_SELECTCLOTHSLRIG_04  			// Light rig for finale setup (no team Heists)
		iLightRigPropertyLoc = MP_PROP_ELEMENT_BESPOKE_CORONA_LIGHT_RIG_FINALE
		PRINTLN("[CORONA] SETUP_CORONA_BESPOKE_LIGHT_RIG - Choosing light rig = HEI_MPH_SELECTCLOTHSLRIG_04")
	ENDIF
	
	// Coords and rotation
	#IF IS_DEBUG_BUILD
	IF NOT g_bDebugLightRigUpdate
	#ENDIF
	
		IF DOES_ENTITY_EXIST(vehLightRig)
			DELETE_OBJECT(vehLightRig)
		ENDIF
	
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	VECTOR vInitialCoord, vInitialRot
	// Always grab the default position / rotation so the light gains a source
	GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vInitialCoord, vInitialRot, MP_PROP_ELEMENT_BESPOKE_CORONA_LIGHT_RIG_TEAM_0)
	GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vObjCoord, vObjRot, iLightRigPropertyLoc)
	
	IF HAS_MODEL_LOADED(sLightRigModel)
		PRINTLN("[CORONA] SETUP_CORONA_BESPOKE_LIGHT_RIG - Model for light rig has been loaded")
		
		vObjCoord.x += tfCORONA_BESPOKE_LIGHT_RIG_COORD_X
		vObjCoord.y += tfCORONA_BESPOKE_LIGHT_RIG_COORD_Y
		vObjCoord.z += tfCORONA_BESPOKE_LIGHT_RIG_COORD_Z
		
		IF NOT DOES_ENTITY_EXIST(vehLightRig)
			vehLightRig = CREATE_OBJECT_NO_OFFSET(sLightRigModel, vInitialCoord, FALSE, FALSE)
		ELSE
			SET_ENTITY_COORDS_NO_OFFSET(vehLightRig, vObjCoord)
		ENDIF
	
		IF DOES_ENTITY_EXIST(vehLightRig)
			
			vObjRot.z += tfCORONA_BESPOKE_LIGHT_RIG_ROTATION
			
			PRINTLN("[CORONA] SETUP_CORONA_BESPOKE_LIGHT_RIG - Creating light rig: vInitialCoord = ", vInitialCoord, ", vInitialRot = ", vInitialRot)
			
			SET_ENTITY_ROTATION(vehLightRig, vInitialRot)
			
			PRINTLN("[CORONA] SETUP_CORONA_BESPOKE_LIGHT_RIG - Updating position to: vObjCoord = ", vObjCoord, ", vObjRot = ", vObjRot)
		
			// Not update to the correct position
			SET_ENTITY_COORDS_NO_OFFSET(vehLightRig, vObjCoord)
			SET_ENTITY_ROTATION(vehLightRig, vObjRot)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up the bespoke light rig for the heist coronas
PROC SETUP_CORONA_BESPOKE_LIGHT_RIG(CORONA_MENU_DATA &coronaMenuData, MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)
	
	IF IS_THIS_MISSION_OF_TYPE_HEIST(sLaunchMissionDetails.iMissionType, 
										sLaunchMissionDetails.bIsHeistMission 
										 , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  ) 
		
		// Only next gen
		IF NOT IS_PS3_VERSION()
		AND NOT IS_XBOX360_VERSION()	
			INT iCurrentViewedTeam = GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData))
		
			// Check the team is valid for the player
			IF iCurrentViewedTeam < -1
			OR iCurrentViewedTeam >= FMMC_MAX_TEAMS 
				PRINTLN("[CORONA] SETUP_CORONA_BESPOKE_LIGHT_RIG - Invalid team: ", iCurrentViewedTeam, ". Set to 0.")
				iCurrentViewedTeam = 0
			ENDIF
	
			
			MODEL_NAMES mnLightRig
			INT iPropertyLocation
			VECTOR vObjCoord
			VECTOR vObjRot
	
			GET_CORONA_LIGHT_RIG_PROPERTIES(iCurrentViewedTeam, mnLightRig, iPropertyLocation, coronaMenuData.vehLightRig, vObjCoord, vObjRot)
			
		ENDIF
	ENDIF
ENDPROC

PROC GRACEFULLY_REMOVE_PLANE(CORONA_MENU_DATA &coronaMenuData)
	IF coronaMenuData.iSelectionCameraSwitchStage = 1
		IF NOT IS_ENTITY_DEAD(coronaMenuData.vehDisplayedVehicle)
			FREEZE_ENTITY_POSITION(coronaMenuData.vehDisplayedVehicle, TRUE)
			SET_ENTITY_COLLISION(coronaMenuData.vehDisplayedVehicle, FALSE)
			SET_ENTITY_VISIBLE(coronaMenuData.vehDisplayedVehicle, FALSE)
			PRINTLN("GRACEFULLY_REMOVE_PLANE - SET_ENTITY_VISIBLE(TRUE)")
			SET_VEHICLE_ENGINE_ON(coronaMenuData.vehDisplayedVehicle, FALSE, TRUE)
			SET_ENTITY_PROOFS(coronaMenuData.vehDisplayedVehicle, TRUE, TRUE, TRUE, TRUE, TRUE)
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL PROCESS_CORONA_SLOT_SELECTION_FANCY(CORONA_MENU_DATA &coronaMenuData, VECTOR vCurrentVector, VECTOR vTargetVector, 
											  FLOAT fCurrentHeading, FLOAT fTargetHeading, BOOL bWhipLeft, MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails) //PED_INDEX piCurrentPlayer, PED_INDEX piTargetPlayer, BOOL bWhipLeft, VECTOR vStartPos)

	VECTOR vCameraSlideFrom	
		
	VECTOR vCameraOffset = <<0.1481, 1.7047, 0.4819>>//<<0.0963, 1.6826, 0.4>>//0.3049>>//<<0.0962, 1.6831, 0.2997>>//<<0.1, 1.45, 0.3>>
	//VECTOR vCameraPointOffset = <<0.3419, -1.2704, 0.5514>>//= <<0.0334, -1.2923, 0.5518>>//<<-0.0459, -1.2991, 0.6046>>//<<0.2827, -1.2954, 0.6065>>//<<0.25, 0.0, 0.65>>		
	
	VECTOR vCameraWhipOffOffset

	VECTOR vInitialCameraCoord,vInitialCameraRot
	FLOAT fInitialCameraFOV
	
	PED_INDEX aPed
	
	INT i
	
	SET_USE_HI_DOF()
	
	IF NOT IS_CORONA_A_BOAT_OR_AIR_RACE()
		GRACEFULLY_REMOVE_VEHICLE(coronaMenuData)	
	ELSE
		// Rob B - handle cleanup of plane so doesn't crash in background
		IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
			IF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
				GRACEFULLY_REMOVE_PLANE(coronaMenuData)
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH coronaMenuData.iSelectionCameraSwitchStage

		//Get cam offset from current player
		CASE 0


			//Dont allow target coords/heading to update after initialisation of move.
			coronaMenuData.vGrabbedTargetCoords = vTargetVector
			coronaMenuData.fGrabbedTargetHeading = fTargetHeading

			// Set our bit so we will update the light rig as we switch
			CLEAR_CORONA_BIT(CORONA_UPDATE_LIGHT_RIG_ON_SWITCH)

			IF coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_TEAM
//				vCameraOffset = <<-0.0538, -2.1142, 0.1211>>
//				vCameraPointOffset = <<-0.1398, 0.8236, 0.6227>>				
			ENDIF

	
			FOR i = 0 TO (NUM_NETWORK_PLAYERS-1) STEP 1		
				IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
				AND coronaMenuData.lobbyPlayers[i].playerID != PLAYER_ID()
					IF coronaMenuData.lobbyPlayers[i].iTeam = GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, GET_MY_CURRENT_TARGET_SLOT(coronaMenuData))
						aPed = g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID
						
						IF CORONA_SCENE_CHECK_VALID_PED(aPed)
							SET_ENTITY_MOTION_BLUR(aPed, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			DESTROY_ALL_CAMS(TRUE)
			DESTROY_CAM(g_sTransitionSessionData.ciPedCam)
			DESTROY_CAM(coronaMenuData.ciCoronaCam)
			
			g_sTransitionSessionData.ciPedCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")						
			coronaMenuData.ciCoronaCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
			
			PRINTLN("[CORONA] PROCESS_CORONA_SLOT_SELECTION_FANCY - Creating ciPedCam: ", 
				NATIVE_TO_INT(g_sTransitionSessionData.ciPedCam), 
				", ciCoronaCam: ", 
				NATIVE_TO_INT(coronaMenuData.ciCoronaCam))
			
			// Ensure the DOF is set correctly.
			SET_CAM_USE_SHALLOW_DOF_MODE(coronaMenuData.ciCoronaCam, FALSE)
			SET_CAM_DOF_STRENGTH(coronaMenuData.ciCoronaCam, 1.0)
			SET_CAM_DOF_PLANES(coronaMenuData.ciCoronaCam, 0.6, 0.8, 1.7, 5.0)
			
			SET_CAM_USE_SHALLOW_DOF_MODE(g_sTransitionSessionData.ciPedCam, FALSE)
			SET_CAM_DOF_STRENGTH(g_sTransitionSessionData.ciPedCam, 1.0)
			SET_CAM_DOF_PLANES(g_sTransitionSessionData.ciPedCam, 0.6, 0.8, 1.7, 5.0)


			IF coronaMenuData.bFirstTransitionFlag = FALSE //iSelectionCameraTimer = -1
			//AND g_FMMC_STRUCT.iMissionType <> FMMC_TYPE_RACE
			
				PRINTLN("[CORONA] PROCESS_CORONA_SLOT_SELECTION_FANCY - Initial camera transition, no swoop")
			
				IF IS_CORONA_A_BOAT_OR_AIR_RACE()
					PLAY_SOUND_FRONTEND(-1, "BOATS_PLANES_HELIS_BOOM" , "MP_LOBBY_SOUNDS")
				ELSE
					PLAY_SOUND_FRONTEND(-1, "CAR_BIKE_WHOOSH" , "MP_LOBBY_SOUNDS")
				ENDIF
				
				coronaMenuData.vGrabbedTargetCoords = vTargetVector
				coronaMenuData.fGrabbedTargetHeading = fTargetHeading
				
				coronaMenuData.vCameraPointOffset = <<0.3419, -1.2704, 0.5514 - coronaMenuData.fCamPointAtOffset>>	//Default to 
//				vCameraPointOffset  = <<0.3419, -1.2704, 0.5514>>
				
				// If we are handling bespoke then use exact values
				IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
					SET_CAM_COORD(g_sTransitionSessionData.ciPedCam, coronaMenuData.vGrabbedTargetCoords)
					SET_CAM_ROT(g_sTransitionSessionData.ciPedCam, coronaMenuData.vTargetCamRotation)
					
					PRINTLN("[CORONA] CAMERA: pos = ", coronaMenuData.vGrabbedTargetCoords, ", rot = ", coronaMenuData.vTargetCamRotation)
				ELSE
					SET_CAM_COORD(g_sTransitionSessionData.ciPedCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vGrabbedTargetCoords, coronaMenuData.fGrabbedTargetHeading, vCameraOffset))
					POINT_CAM_AT_COORD(g_sTransitionSessionData.ciPedCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vGrabbedTargetCoords, coronaMenuData.fGrabbedTargetHeading, coronaMenuData.vCameraPointOffset))
				ENDIF
				
				SET_CAM_FOV(g_sTransitionSessionData.ciPedCam, GET_CORONA_CAMERA_FOV())
				
				// Set up standard properties of camera
				SET_CAM_ACTIVE(g_sTransitionSessionData.ciPedCam, TRUE)			
				SET_CAM_MOTION_BLUR_STRENGTH(g_sTransitionSessionData.ciPedCam, 0.25)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)

				coronaMenuData.iSelectionCameraTimer = GET_GAME_TIMER()		
				coronaMenuData.bFirstTransitionFlag = TRUE
				coronaMenuData.bPedCoordsGrabbed = FALSE
				coronaMenuData.iSelectionCameraSwitchStage = 0
				RETURN TRUE
				
			ELSE
				IF bWhipLeft
					PLAY_SOUND_FRONTEND(-1,"Whoosh_1s_R_to_L","MP_LOBBY_SOUNDS")
				ELSE
					PLAY_SOUND_FRONTEND(-1,"Whoosh_1s_L_to_R","MP_LOBBY_SOUNDS")
				ENDIF
			ENDIF
			
			PRINTLN("[CORONA] CAMERA - A")
			
			IF (coronaMenuData.coronaSummaryCardState != CORONA_SUMMARY_CARD_HIDE)
				coronaMenuData.vCameraPointOffset = <<-0.0962, -1.2845, 0.5518 - coronaMenuData.fCamPointAtOffset>>//<<0.0334, -1.2923, 0.5518>>//Maybe make these constants		
			ELSE		
				coronaMenuData.vCameraPointOffset = <<0.3419, -1.2704, 0.5514 - coronaMenuData.fCamPointAtOffset>>
			ENDIF
		
			vInitialCameraCoord = GET_FINAL_RENDERED_CAM_COORD()
			vInitialCameraRot   = GET_FINAL_RENDERED_CAM_ROT()
			fInitialCameraFOV   = GET_FINAL_RENDERED_CAM_FOV()
					
			SET_CAM_COORD(coronaMenuData.ciCoronaCam, vInitialCameraCoord)
			SET_CAM_ROT(coronaMenuData.ciCoronaCam,vInitialCameraRot)
			SET_CAM_FOV(coronaMenuData.ciCoronaCam, fInitialCameraFOV)
			
			SET_CAM_ACTIVE(coronaMenuData.ciCoronaCam, TRUE)
			
			IF NOT IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
			
				PRINTLN("[CORONA] PROCESS_CORONA_SLOT_SELECTION_FANCY - Not a team corona, normal interp")
			
				coronaMenuData.vGrabbedTargetCoords = vTargetVector
				coronaMenuData.fGrabbedTargetHeading = fTargetHeading
				
				SET_CAM_COORD(g_sTransitionSessionData.ciPedCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vGrabbedTargetCoords, coronaMenuData.fGrabbedTargetHeading, vCameraOffset))
				POINT_CAM_AT_COORD(g_sTransitionSessionData.ciPedCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vGrabbedTargetCoords, coronaMenuData.fGrabbedTargetHeading, coronaMenuData.vCameraPointOffset))
				SET_CAM_FOV(g_sTransitionSessionData.ciPedCam, cfCORONA_CAM_FOV)
			
				SET_CAM_ACTIVE_WITH_INTERP(g_sTransitionSessionData.ciPedCam, coronaMenuData.ciCoronaCam, 350, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
			
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_CAM_MOTION_BLUR_STRENGTH(g_sTransitionSessionData.ciPedCam, 0.25)
				
				ANIMPOSTFX_PLAY("MP_corona_switch", 0, FALSE)
				
				coronaMenuData.iSelectionCameraTimer = GET_GAME_TIMER()
				coronaMenuData.iSelectionCameraSwitchStage = 5
				
			ELSE
				PRINTLN("[CORONA] PROCESS_CORONA_SLOT_SELECTION_FANCY - Team corona, special swoop")
			
				IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
					
					SET_CAM_COORD(g_sTransitionSessionData.ciPedCam, coronaMenuData.vCurrentCamVector)
					
					IF bWhipLeft = TRUE
						vCameraWhipOffOffset = <<coronaMenuData.vCurrentCamRotation.x, coronaMenuData.vCurrentCamRotation.y, coronaMenuData.vCurrentCamRotation.z - g_fCoronaBespokeCameraRotOffset>>
					ELSE		
						vCameraWhipOffOffset = <<coronaMenuData.vCurrentCamRotation.x, coronaMenuData.vCurrentCamRotation.y, coronaMenuData.vCurrentCamRotation.z + g_fCoronaBespokeCameraRotOffset>>
					ENDIF 
					
					SET_CAM_ROT(g_sTransitionSessionData.ciPedCam, vCameraWhipOffOffset)
				ELSE
			
					vCameraOffset.y = coronaMenuData.vCameraPointOffset.y
					SET_CAM_COORD(g_sTransitionSessionData.ciPedCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCurrentVector, fCurrentHeading, vCameraOffset))
						
					IF bWhipLeft = TRUE
						vCameraWhipOffOffset = <<1.25, coronaMenuData.vCameraPointOffset.y, coronaMenuData.vCameraPointOffset.z>>
					ELSE		
						vCameraWhipOffOffset = <<-1.25, coronaMenuData.vCameraPointOffset.y, coronaMenuData.vCameraPointOffset.z>>
					ENDIF
								
					POINT_CAM_AT_COORD(g_sTransitionSessionData.ciPedCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCurrentVector, fCurrentHeading, vCameraWhipOffOffset))
				ENDIF
				
				SET_CAM_FOV(g_sTransitionSessionData.ciPedCam, GET_CORONA_CAMERA_FOV())
				
				SET_CAM_ACTIVE_WITH_INTERP(g_sTransitionSessionData.ciPedCam, coronaMenuData.ciCoronaCam, 175, GRAPH_TYPE_ACCEL, GRAPH_TYPE_ACCEL)			
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_CAM_MOTION_BLUR_STRENGTH(g_sTransitionSessionData.ciPedCam, 0.25)
									
				
				ANIMPOSTFX_PLAY("MP_corona_switch", 0, FALSE)
							
				coronaMenuData.iSelectionCameraTimer = GET_GAME_TIMER()
				coronaMenuData.iSelectionCameraSwitchStage++
			ENDIF
		BREAK
		
		//Zoom in
		CASE 1
		
//			IF GET_GAME_TIMER() - coronaMenuData.iSelectionCameraTimer > 100
//				SET_SELECTED_TEAM_VISIBLE(coronaMenuData, sLaunchMissionDetails, TRUE)
//			ENDIF
		
			IF GET_GAME_TIMER() - coronaMenuData.iSelectionCameraTimer > 175
								//PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
				
				SETUP_CORONA_BESPOKE_LIGHT_RIG(coronaMenuData, sLaunchMissionDetails)
				
				IF GET_CORONA_STATUS() = CORONA_STATUS_BETTING
					CORONA_ATTACH_LIGHT_RIG_TO_PLAYER(coronaMenuData, sLaunchMissionDetails)
				ENDIF
				
				coronaMenuData.iSelectionCameraSwitchStage++
			ENDIF
		BREAK
		
		//Blur?
		CASE 2
					
			//IN case target chagnes during...
			coronaMenuData.vGrabbedTargetCoords = vTargetVector
			coronaMenuData.fGrabbedTargetHeading = fTargetHeading
			
			HIDE_PLAYERS_CLOSE_TO_SELECTION_CAMERA(coronaMenuData)
							
			// Always set us as an offset					
			IF bWhipLeft = TRUE
				vCameraSlideFrom = <<-0.75, vCameraOffset.y, vCameraOffset.z>>//1.6831, 0.2997>>
			ELSE
				vCameraSlideFrom = <<0.75, vCameraOffset.y, vCameraOffset.z>>//1.6831, 0.2997>>
			ENDIF
						
			IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
				
				// Null our other offsets, we just displace left or right
				vCameraSlideFrom.y = 0.0
				vCameraSlideFrom.z = 0.0
				vCameraSlideFrom.x *= -1
			
				SET_CAM_COORD(coronaMenuData.ciCoronaCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vGrabbedTargetCoords, coronaMenuData.fGrabbedTargetHeading, vCameraSlideFrom))
				SET_CAM_ROT(coronaMenuData.ciCoronaCam, coronaMenuData.vTargetCamRotation)
			ELSE
				SET_CAM_COORD(coronaMenuData.ciCoronaCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vGrabbedTargetCoords, coronaMenuData.fGrabbedTargetHeading, vCameraSlideFrom))
			
				IF bWhipLeft = TRUE
					POINT_CAM_AT_COORD(coronaMenuData.ciCoronaCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vGrabbedTargetCoords, coronaMenuData.fGrabbedTargetHeading, <<-0.75, coronaMenuData.vCameraPointOffset.y, coronaMenuData.vCameraPointOffset.z>>))
				ELSE
					POINT_CAM_AT_COORD(coronaMenuData.ciCoronaCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vGrabbedTargetCoords, coronaMenuData.fGrabbedTargetHeading, <<0.75, coronaMenuData.vCameraPointOffset.y, coronaMenuData.vCameraPointOffset.z>>))
				ENDIF
			ENDIF
			
			SET_CAM_FOV(coronaMenuData.ciCoronaCam, GET_CORONA_CAMERA_FOV())
			SET_CAM_ACTIVE(coronaMenuData.ciCoronaCam, TRUE)
			
			IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
				SET_CAM_COORD(g_sTransitionSessionData.ciPedCam, coronaMenuData.vGrabbedTargetCoords)
				SET_CAM_ROT(g_sTransitionSessionData.ciPedCam, coronaMenuData.vTargetCamRotation)
				
				PRINTLN("[CORONA] CAMERA: pos = ", coronaMenuData.vGrabbedTargetCoords, ", rot = ", coronaMenuData.vTargetCamRotation)
			ELSE
				SET_CAM_COORD(g_sTransitionSessionData.ciPedCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vGrabbedTargetCoords, coronaMenuData.fGrabbedTargetHeading, vCameraOffset))
				POINT_CAM_AT_COORD(g_sTransitionSessionData.ciPedCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vGrabbedTargetCoords, coronaMenuData.fGrabbedTargetHeading, coronaMenuData.vCameraPointOffset))
			ENDIF
			
			SET_CAM_FOV(g_sTransitionSessionData.ciPedCam, GET_CORONA_CAMERA_FOV())
			
			SET_CAM_ACTIVE_WITH_INTERP(g_sTransitionSessionData.ciPedCam, coronaMenuData.ciCoronaCam, 175, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)			
			
			SET_CAM_MOTION_BLUR_STRENGTH(g_sTransitionSessionData.ciPedCam, 0.25)
			
			SET_SELECTED_TEAM_VISIBLE(coronaMenuData, sLaunchMissionDetails, DEFAULT)
			
			//SHAKE_CAM(g_sTransitionSessionData.ciPedCam, "HAND_SHAKE", 0.15)
									
			coronaMenuData.iSelectionCameraTimer = GET_GAME_TIMER()		
			coronaMenuData.iSelectionCameraSwitchStage++
		BREAK
		
		//Reposition to new player
		CASE 3
		
			HIDE_PLAYERS_CLOSE_TO_SELECTION_CAMERA(coronaMenuData)
		
			//Update which players are visible
			SET_SELECTED_TEAM_VISIBLE(coronaMenuData, sLaunchMissionDetails, DEFAULT)
		
			IF GET_GAME_TIMER() - coronaMenuData.iSelectionCameraTimer > 175
				
//				STOP_CAM_POINTING(coronaMenuData.ciCoronaCam)
//				STOP_CAM_POINTING(g_sTransitionSessionData.ciPedCam)
				
				coronaMenuData.iSelectionCameraSwitchStage++
			ENDIF
		BREAK
		
		//Zoom out
		CASE 4
		
			HIDE_PLAYERS_CLOSE_TO_SELECTION_CAMERA(coronaMenuData)
			
			//Update which players are visible
			SET_SELECTED_TEAM_VISIBLE(coronaMenuData, sLaunchMissionDetails, DEFAULT)
			
			//SET_CAM_ACTIVE(g_sTransitionSessionData.ciPedCam, TRUE)
			
			FOR i = 0 TO (NUM_NETWORK_PLAYERS-1) STEP 1		
				IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
				AND coronaMenuData.lobbyPlayers[i].playerID != PLAYER_ID()
					aPed = g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID
					
					IF CORONA_SCENE_CHECK_VALID_PED(aPed)			
						SET_ENTITY_MOTION_BLUR(aPed, FALSE)
					ENDIF
				ENDIF
			ENDFOR
			
			IF IS_CORONA_BIT_SET(CORONA_HEIST_CAMERA_FROM_BOARD)
				PRINTLN("[CORONA][AMEC] - PROCESS_CORONA_SLOT_SELECTION_FANCY - Clearing heist camera from board bit.")
				SET_CORONA_BIT(CORONA_HEIST_CAMERA_FROM_BOARD)
			ENDIF
			
			coronaMenuData.desiredXRotPed = 0.0
			coronaMenuData.desiredYRotPed = 0.0
			coronaMenuData.fCameraRotationXPed = 0.0//1
			coronaMenuData.fCameraRotationYPed = 0.0//1
			coronaMenuData.fCamMovementSpeed = 0.0
			coronaMenuData.fCamReturnSpeed = 0.0
			coronaMenuData.fCamMovementSpeedY = 0.0
			coronaMenuData.fCamReturnSpeedY = 0.0
	
			
			coronaMenuData.bPedCoordsGrabbed = FALSE
			coronaMenuData.iSelectionCameraSwitchStage = 0
			RETURN TRUE
		BREAK
		
		CASE 5
		
			// As we switch, we should update the light rig on the players
			IF GET_GAME_TIMER() - coronaMenuData.iSelectionCameraTimer > 150
				IF NOT IS_CORONA_BIT_SET(CORONA_UPDATE_LIGHT_RIG_ON_SWITCH)
					IF GET_CORONA_STATUS() = CORONA_STATUS_BETTING
					
						PRINTLN("[CORONA] PROCESS_CORONA_SLOT_SELECTION_FANCY - Switch the light rig as we move to the new player")
					
						CORONA_ATTACH_LIGHT_RIG_TO_PLAYER(coronaMenuData, sLaunchMissionDetails)
						SET_CORONA_BIT(CORONA_UPDATE_LIGHT_RIG_ON_SWITCH)
					ENDIF
				ENDIF
			ENDIF
		
			IF GET_GAME_TIMER() - coronaMenuData.iSelectionCameraTimer > 350
				
//				STOP_CAM_POINTING(coronaMenuData.ciCoronaCam)
//				STOP_CAM_POINTING(g_sTransitionSessionData.ciPedCam)
				
				coronaMenuData.iSelectionCameraSwitchStage = 4
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE: Listen for user control of camera
FUNC FLOAT MAINTAIN_BETTING_CAMERA_CONTROL(CORONA_MENU_DATA &coronaMenuData,  MISSION_TO_LAUNCH_DETAILS sLaunchMissionDetails)
	
//	IF DOES_CAM_EXIST(g_sTransitionSessionData.ciPedCam)
//	AND IS_CAM_ACTIVE(g_sTransitionSessionData.ciPedCam)
//	AND NOT IS_CAM_INTERPOLATING(g_sTransitionSessionData.ciPedCam)
	IF NOT IS_CORONA_BIT_SET(CORONA_PROCESS_SLOT_SELECTION_CAMERA)
		HIDE_PLAYERS_CLOSE_TO_SELECTION_CAMERA(coronaMenuData)
		
//		SCRIPT_ASSERT("maintaining camera control")

		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_X)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_Y)
		ENDIF
		
		// Allow player to move the camera
		INT iLeftX, iLeftY, iRightX, iRightY
		
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
		
		iRightX *= -1
		IF IS_LOOK_INVERTED()
			iRightY	*= -1
		ENDIF
		
		FLOAT fXLimitLeft = -0.5
		FLOAT fXLimitRight = 0.5
				
		// Get clamp values for Y axis.
		FLOAT fYLimitLow = -0.25
		FLOAT fYLimitHigh = 0.3
		FLOAT fdesiredYRotDegree = 128.0
		
		// Reduce Y axis movement in heist corona (more noticable due to heist camera angles)
		IF IS_THIS_MISSION_OF_TYPE_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistMission, sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene)
			
			fYLimitHigh -= 0.05
			fdesiredYRotDegree = 70.0
			
			// Single team
			IF IS_CORONA_BIT_SET(CORONA_NO_TEAM_OUTFITS)
			OR g_FMMC_STRUCT.iNumberOfTeams = 1 
				IF IS_CORONA_BIT_SET(CORONA_HEIST_HEELS_ADJUST_CAM_MOVEMENT)
					fYLimitLow -= 0.110
				ELSE
					fdesiredYRotDegree = 45.0
				ENDIF
			ELSE
				// Multiple teams
				IF GET_MY_CURRENT_TARGET_SLOT(coronaMenuData) = 0 
					fYLimitLow -= 0.120
				ELIF GET_MY_CURRENT_TARGET_SLOT(coronaMenuData) = 1
					fYLimitLow -= 0.090
				ELIF GET_MY_CURRENT_TARGET_SLOT(coronaMenuData) = 2
					fYLimitLow -= 0.075
				ELIF GET_MY_CURRENT_TARGET_SLOT(coronaMenuData) = 3
					fYLimitLow -= 0.110
				ENDIF
			ENDIF
		ENDIF
		
		IF iRightX < 0
			coronaMenuData.desiredXRotPed = (TO_FLOAT(iRightX)/-128.0) * fXLimitLeft
		ELIF iRightX > 0
			coronaMenuData.desiredXRotPed = (TO_FLOAT(iRightX)/128.0) * fXLimitRight 
		ENDIF
		
		IF iRightX = 0
			coronaMenuData.desiredXRotPed = 0.0
		ENDIF
												
		IF ABSF(coronaMenuData.fCameraRotationXPed -  coronaMenuData.desiredXRotPed) > 0.01
			coronaMenuData.fCameraRotationXPed = LERP_FLOAT(coronaMenuData.fCameraRotationXPed, coronaMenuData.desiredXRotPed, coronaMenuData.fXOffsetLerp)
			IF coronaMenuData.fXOffsetLerp < 1.0
				coronaMenuData.fXOffsetLerp = coronaMenuData.fXOffsetLerp +@ CLAMP((VDIST(<<coronaMenuData.fCameraRotationXPed, 0.0, 0.0>>, <<coronaMenuData.desiredXRotPed, 0.0, 0.0>>) * 2.5), -5.0, 5.0)
			ENDIF
		ELSE
			coronaMenuData.fXOffsetLerp = 0.0
		ENDIF
		
		IF iRightY < 0
			coronaMenuData.desiredYRotPed = (TO_FLOAT(iRightY)/-128.0) * fYLimitHigh
		ELIF iRightY > 0
			coronaMenuData.desiredYRotPed = (TO_FLOAT(iRightY)/fdesiredYRotDegree) * fYLimitLow
		ENDIF
		
		IF iRightY = 0
			coronaMenuData.desiredYRotPed = 0.0
		ENDIF
												
		IF ABSF(coronaMenuData.fCameraRotationYPed -  coronaMenuData.desiredYRotPed) > 0.01
			coronaMenuData.fCameraRotationYPed = LERP_FLOAT(coronaMenuData.fCameraRotationYPed, coronaMenuData.desiredYRotPed, coronaMenuData.fYOffsetLerp)	
			IF coronaMenuData.fYOffsetLerp < 1.0
				coronaMenuData.fYOffsetLerp = coronaMenuData.fYOffsetLerp +@ CLAMP((VDIST(<<coronaMenuData.fCameraRotationYPed, 0.0, 0.0>>, <<coronaMenuData.desiredYRotPed, 0.0, 0.0>>) * 2.5), -5.0, 5.0)
			ENDIF
		ELSE
			coronaMenuData.fYOffsetLerp = 0.0
		ENDIF
				
		//End of camera movement control
		
		
		IF NOT coronaMenuData.bPedCoordsGrabbed
			coronaMenuData.vTargetPedCoords = coronaMenuData.vTargetCamVector   //GET_ENTITY_COORDS(aTargetPed)
			coronaMenuData.fTargetPedHeading = coronaMenuData.fTargetCamHeading //GET_ENTITY_HEADING(aTargetPed)						
			coronaMenuData.bPedCoordsGrabbed = TRUE
		ENDIF
		
		// ********** HANDLE ZOOM **********
		
		CONST_FLOAT fCameraOffsetY 1.7047
		VECTOR vDesiredPointOffset 	= <<0.3419, -1.2704, 0.5514 - coronaMenuData.fCamPointAtOffset>>
		VECTOR vCameraOffset 		= <<0.1481, fCameraOffsetY, 0.4819>>	//<<0.0963, 1.6826, 0.4>>
		
		IF coronaMenuData.fCoronaCurrentZoom = 0.0
			coronaMenuData.fCoronaCurrentZoom = vCameraOffset.y
		ENDIF		
		
		FLOAT fTargetYPos = vCameraOffset.y

		// Only listen for the zoom if we are not on a heist
		IF NOT IS_THIS_MISSION_OF_TYPE_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistMission 
									 , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  ) 

			// Get our target zoom based on control pressed
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
				fTargetYPos = CORONA_CAM_ZOOM_MIN
				
					IF IS_THIS_MISSION_A_PRE_PLANNING_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistPlanning)
						fTargetYPos -= CORONA_CAM_ZOOM_ADJUST
					ENDIF
				
			ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
				fTargetYPos = CORONA_CAM_ZOOM_MAX
			ENDIF
		ENDIF
		
		// If we haven't yet reached the target zoom
		IF ABSF(coronaMenuData.fCoronaCurrentZoom - fTargetYPos) > 0.01
		
			// Calculate the current zoom we should be at
			coronaMenuData.fCoronaCurrentZoom = LERP_FLOAT(coronaMenuData.fCoronaCurrentZoom, fTargetYPos, coronaMenuData.fCoronaZoomLerp)
			
			IF coronaMenuData.fCoronaZoomLerp < 1.0
				coronaMenuData.fCoronaZoomLerp = coronaMenuData.fCoronaZoomLerp +@ CLAMP((ABSF(coronaMenuData.fCoronaCurrentZoom - fTargetYPos)), -CORONA_CAM_ZOOM_RATE, CORONA_CAM_ZOOM_RATE)	
			ENDIF
		ELSE
			coronaMenuData.fCoronaZoomLerp = 0.0
		ENDIF
		
		vCameraOffset.y = coronaMenuData.fCoronaCurrentZoom
	
		// ********** HANDLE ZOOM END **********
				
		//First time run no transition
		IF IS_VECTOR_ZERO(coronaMenuData.vCameraPointOffset)
			coronaMenuData.vCameraPointOffset = <<0.3419, -1.2704, 0.5514 - coronaMenuData.fCamPointAtOffset>>
			vDesiredPointOffset = coronaMenuData.vCameraPointOffset			
			coronaMenuData.fPointOffsetLerp = 0.0
		ELSE	
			IF sLaunchMissionDetails.iBettingStatus = FM_MISSION_BETTING_SCREEN_MAINTAIN			
			OR (GET_CORONA_STATUS() = CORONA_STATUS_TEAM_DM
				AND sLaunchMissionDetails.iInCoronaStatus = FM_MISSION_IN_CORONA_SCREEN_MAINTAIN)//FM_MISSION_IN_CORONA_SCREEN_MAINTAIN)

				//Else adjust depending on Status view
				IF (coronaMenuData.coronaSummaryCardState != CORONA_SUMMARY_CARD_HIDE)
					vDesiredPointOffset = <<-0.0962, -1.2845, 0.5518 - coronaMenuData.fCamPointAtOffset>>//<<0.0334, -1.2923, 0.5518>>//Maybe make these constants		
				ELSE		
					vDesiredPointOffset = <<0.3419, -1.2704, 0.5514 - coronaMenuData.fCamPointAtOffset>>
				ENDIF
			ENDIF
			
		ENDIF				
								
		IF NOT ARE_VECTORS_ALMOST_EQUAL(coronaMenuData.vCameraPointOffset, vDesiredPointOffset, 0.02)		
			coronaMenuData.vCameraPointOffset = LERP_VECTOR(coronaMenuData.vCameraPointOffset, vDesiredPointOffset, coronaMenuData.fPointOffsetLerp)		
			IF coronaMenuData.fPointOffsetLerp < 1.0
				coronaMenuData.fPointOffsetLerp = coronaMenuData.fPointOffsetLerp +@ VDIST(coronaMenuData.vCameraPointOffset, vDesiredPointOffset) * 5.0
			ENDIF
		ELSE
			coronaMenuData.fPointOffsetLerp = 0.0
		ENDIF

		FLOAT fCameraNearDOFValue	
		FLOAT fCameraDOFValue
		
		VECTOR vCameraPos
		VECTOR vCameraPosRest
		
		// Calculate the camera position. fCameraHeadingOffset is initial heading position of camera. fCameraRotationXPed is the amount of rotation from input	
		IF IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
		
			// Calculate our rotations based as a fraction of the offset from the centre
			FLOAT fNewRotationX
			FLOAT fNewRotationY
		
			fNewRotationX = (coronaMenuData.fCameraRotationXPed / fXLimitLeft) * g_fCoronaBespokeCameraRotVal
			
			IF coronaMenuData.fCameraRotationYPed < 0
				fNewRotationY = (coronaMenuData.fCameraRotationYPed / fYLimitLow) * g_fCoronaBespokeCameraRotValY
			ELSE
				fNewRotationY = (coronaMenuData.fCameraRotationYPed / (-fYLimitHigh)) * g_fCoronaBespokeCameraRotValY
			ENDIF
			
			FLOAT fNewCameraZoom = fCameraOffsetY - vCameraOffset.y
			
			vCameraPos 		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vGrabbedTargetCoords, coronaMenuData.fTargetCamHeading, <<coronaMenuData.fCameraRotationXPed*-1, fNewCameraZoom, coronaMenuData.fCameraRotationYPed>>)
			vCameraPosRest	= coronaMenuData.vGrabbedTargetCoords
			
			#IF IS_DEBUG_BUILD
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
				PRINTLN("[H_CAM] ************************")
				PRINTLN("[H_CAM] coronaMenuData.vGrabbedTargetCoords = ", coronaMenuData.vGrabbedTargetCoords)
				PRINTLN("[H_CAM] vCameraPos = ", vCameraPos)
				PRINTLN("[H_CAM] coronaMenuData.fCameraRotationXPed = ", coronaMenuData.fCameraRotationXPed)
				PRINTLN("[H_CAM] coronaMenuData.desiredXRotPed = ", coronaMenuData.desiredXRotPed)
				PRINTLN("[H_CAM] coronaMenuData.vTargetCamRotation = ", coronaMenuData.vTargetCamRotation)
				VECTOR vNewRot = <<coronaMenuData.vTargetCamRotation.x + fNewRotationY, coronaMenuData.vTargetCamRotation.y, coronaMenuData.vTargetCamRotation.z + fNewRotationX>>
				PRINTLN("[H_CAM] coronaMenuData.vTargetCamRotation + rotation = ", vNewRot)
			ENDIF
			#ENDIF
			
			SET_CAM_COORD(g_sTransitionSessionData.ciPedCam, vCameraPos)
			SET_CAM_ROT(g_sTransitionSessionData.ciPedCam, <<coronaMenuData.vTargetCamRotation.x + fNewRotationY, coronaMenuData.vTargetCamRotation.y, coronaMenuData.vTargetCamRotation.z + fNewRotationX>>)

		ELSE
			vCameraPos 		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vTargetPedCoords, coronaMenuData.fTargetPedHeading, <<vCameraOffset.x + coronaMenuData.fCameraRotationXPed, vCameraOffset.y, vCameraOffset.z + coronaMenuData.fCameraRotationYPed>>)
			vCameraPosRest	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vTargetPedCoords, coronaMenuData.fTargetPedHeading, <<vCameraOffset.x, fCameraOffsetY, vCameraOffset.z>>)
			VECTOR vPointAt = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(coronaMenuData.vTargetPedCoords, coronaMenuData.fTargetPedHeading, <<(coronaMenuData.vCameraPointOffset.x - ( 0.2 * coronaMenuData.fCameraRotationXPed)), (coronaMenuData.vCameraPointOffset.y), (coronaMenuData.vCameraPointOffset.z - (0.5 * coronaMenuData.fCameraRotationYPed))>> )
		
			SET_CAM_COORD(g_sTransitionSessionData.ciPedCam, vCameraPos)
			POINT_CAM_AT_COORD(g_sTransitionSessionData.ciPedCam, vPointAt)
		ENDIF
		
		SET_CAM_MOTION_BLUR_STRENGTH(g_sTransitionSessionData.ciPedCam, 0.1)
		
		IF MAINTAIN_IN_CORONA_DOF_SETTINGS()

			SET_CAM_USE_SHALLOW_DOF_MODE(g_sTransitionSessionData.ciPedCam, FALSE)
			SET_CAM_DOF_STRENGTH(g_sTransitionSessionData.ciPedCam, 1.0)
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
				SET_CAM_DOF_PLANES(g_sTransitionSessionData.ciPedCam, 0.0, fCameraNearDOFValue, fCameraDOFValue+(g_tfCameraDOFFar-1.0), fCameraDOFValue+g_tfCameraDOFFar)
			ELSE
				SET_CAM_DOF_PLANES(g_sTransitionSessionData.ciPedCam, 0.0, fCameraNearDOFValue, fCameraDOFValue, fCameraDOFValue+25.0)
			ENDIF
		ENDIF

//		#IF IS_DEBUG_BUILD
		
			//IF IS_BUTTON_PRESSED(PAD1, LEFTSHOULDER2)
		
//				TEXT_LABEL_31 debugText 
//				debugText += "X: "
//				debugText += ROUND(coronaMenuData.desiredXRotPed * 100.0)
//				debugText += " "
//				debugText += iRightX
//				debugText += " "
//				debugText += ROUND(coronaMenuData.fCameraRotationXPed * 100.0)
//				debugText += " "
//				debugText += ROUND(coronaMenuData.fCamMovementSpeed * 100.0)
//				debugText += " "
//				debugText += ROUND(coronaMenuData.fCamReturnSpeed * 100.0)
//				debugText += "/ Y:"
//				debugText += ROUND(coronaMenuData.desiredYRotPed * 100.0)
//				debugText += " "
//				debugText += iRightY
//				debugText += " "
//				debugText += ROUND(coronaMenuData.fCameraRotationYPed * 100.0)
//				
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//				DRAW_DEBUG_TEXT_ABOVE_COORDS(vPointAt + <<0.0, 0.0, 1.0>>,debugText, 0.0)
			
			//ENDIF
//		#ENDIF		
		
		RETURN ABSF( VDIST(coronaMenuData.vCameraPointOffset, vDesiredPointOffset) ) + ABSF( VDIST(vCameraPosRest, vCameraPos) )

	ELSE
		IF coronaMenuData.fCoronaCurrentZoom != 0.0
			coronaMenuData.fCoronaCurrentZoom = 0.0
		ENDIF
		
		IF coronaMenuData.fCoronaZoomLerp != 0.0
			coronaMenuData.fCoronaZoomLerp = 0.0
		ENDIF
	ENDIF
	
	RETURN 0.0
ENDFUNC


/// PURPOSE: This maintains the switching of the camera between corona slot position (new effects / camera positioning by Ross)
PROC MAINTAIN_CORONA_SLOT_SELECTION_CAMERA(CORONA_MENU_DATA &coronaMenuData, MISSION_TO_LAUNCH_DETAILS sLaunchMissionDetails)

	CONST_FLOAT F_ALPHA_DEAD_ZONE				0.2
	CONST_INT	I_APLHA_FADE_RATE				400 // amount change per second (200 = 0.5s to go from full to zero)
	CONST_FLOAT F_CAMERA_DEVIATION_TOLERENCE	0.05

	BOOL bIsFancySelectionRunning		= FALSE
	BOOL bForceAnUpdate					= FALSE

	// Do we need to process camera switch	
	IF IS_CORONA_BIT_SET(CORONA_PROCESS_SLOT_SELECTION_CAMERA)
		
		BOOL bWhipLeft = (coronaMenuData.iMovingLeft < 0)
		
		IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
			IF coronaMenuData.iSelectionCameraSwitchStage = 0
				bWhipLeft = !bWhipLeft
			ENDIF
		ENDIF
		
//		IF coronaMenuData.iSelectionCameraSwitchStage = 0
//			POSITION_CORONA_PEDS(coronaMenuData, sLaunchMissionDetails.vStartPoint, sLaunchMissionDetails.iMissionType, GET_NUM_CORONA_SLOTS(sLaunchMissionDetails), (coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_SYNC_SCENE))	
//		ENDIF
		
		// Perform camera switch until complete
		IF NOT PROCESS_CORONA_SLOT_SELECTION_FANCY(coronaMenuData, coronaMenuData.vCurrentCamVector, coronaMenuData.vTargetCamVector, coronaMenuData.fCurrentCamHeading, coronaMenuData.fTargetCamHeading, bWhipLeft, sLaunchMissionDetails)
			bIsFancySelectionRunning = TRUE
		ELSE
			bIsFancySelectionRunning 			= FALSE
			bForceAnUpdate						= TRUE	// finished slot selection camera, force it to update
			coronaMenuData.iPlayerTitlesAlpha	= 100	// reset alpha to full
			
			CLEAR_CORONA_BIT(CORONA_PROCESS_SLOT_SELECTION_CAMERA)	// Finished clear bit
		ENDIF
				
		IF NOT IS_CORONA_BIT_SET(CORONA_GENERIC_JOB_RECREATE_CAMERA)
			SET_CORONA_BIT(CORONA_GENERIC_JOB_RECREATE_CAMERA)
		ENDIF
	ENDIF
	
	//Movement stuff here	
	FLOAT fCamDeviation = MAINTAIN_BETTING_CAMERA_CONTROL(coronaMenuData, sLaunchMissionDetails)

	// Chase alpha up
	IF fCamDeviation <= F_ALPHA_DEAD_ZONE
		IF coronaMenuData.iPlayerTitlesAlpha < 100
			coronaMenuData.iPlayerTitlesAlpha += ROUND( TIMESTEP() * I_APLHA_FADE_RATE )
			coronaMenuData.iPlayerTitlesAlpha = CLAMP_INT(coronaMenuData.iPlayerTitlesAlpha, 0, 100)
		ENDIF
		
	// Chase alpha down
	ELSE
		IF coronaMenuData.iPlayerTitlesAlpha > 0
			coronaMenuData.iPlayerTitlesAlpha -= ROUND( TIMESTEP() * I_APLHA_FADE_RATE )
			coronaMenuData.iPlayerTitlesAlpha = CLAMP_INT(coronaMenuData.iPlayerTitlesAlpha, 0, 100)
		ENDIF
	ENDIF
	
	IF NOT bIsFancySelectionRunning	
	AND (bForceAnUpdate
	OR coronaMenuData.iPlayerTitlesAlpha < 100
	OR fCamDeviation > F_CAMERA_DEVIATION_TOLERENCE)
		// If we are in a corona status that is valid for the player titles, update the positions
		IF NOT (GET_CORONA_STATUS() = CORONA_STATUS_TEAM_DM AND sLaunchMissionDetails.iInCoronaStatus = FM_MISSION_IN_CORONA_VIEW_PLANNING)
			UPDATE_CORONA_TEAM_MEMBER_PLAYER_TITLES(coronaMenuData)
		ENDIF
	ENDIF
	
	
	// [SAR] Deals with looping synced main anim loop B* 2151275 - needed here for update
	IF IS_CORONA_BIT_SET(CORONA_HEIST_PLAY_LOOPING_ANIM)
		IF GET_SYNCHRONIZED_SCENE_PHASE(coronaMenuData.iSyncedSceneID) >= 1.0
			IF IS_ENTITY_ALIVE(coronaMenuData.pTempPed)
				TASK_SYNCHRONIZED_SCENE(coronaMenuData.pTempPed, coronaMenuData.iSyncedSceneID, "ANIM@HEISTS@HEIST_CORONA@SINGLE_TEAM", "Single_team_loop_one", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(coronaMenuData.oPistol, coronaMenuData.iSyncedSceneID, "single_team_loop_gun", "ANIM@HEISTS@HEIST_CORONA@SINGLE_TEAM", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET))
				PLAY_SYNCHRONIZED_ENTITY_ANIM(coronaMenuData.oAmmoClip, coronaMenuData.iSyncedSceneID, "single_team_loop_clip", "ANIM@HEISTS@HEIST_CORONA@SINGLE_TEAM", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET))
				FORCE_PED_AI_AND_ANIMATION_UPDATE(coronaMenuData.pTempPed)		
				SET_SYNCHRONIZED_SCENE_LOOPED(coronaMenuData.iSyncedSceneID, TRUE)
				PRINTLN("[CORONA] Begin loop synced animation")
			ENDIF
			IF IS_SYNCHRONIZED_SCENE_RUNNING(coronaMenuData.iSyncedSceneID)
				PRINTLN("[CORONA] The loop synced anim is running")
			ELSE
				PRINTLN("[CORONA] The loop synced anim is not runnning")
			ENDIF
			CLEAR_CORONA_BIT(CORONA_HEIST_PLAY_LOOPING_ANIM)
		ENDIF
	ENDIF
	
ENDPROC




/// PURPOSE:
///    Complete cleanup of any corona data related to ped placement in coronas
PROC CLEANUP_CORONA_PED_PLACEMENT_VARS(CORONA_MENU_DATA &coronaMenuData, BOOL bTurnOffScriptCams = TRUE)
	
	PRINTLN("[CORONA] CLEANUP_CORONA_PED_PLACEMENT_VARS - cleaning up betting camera")
	
	coronaMenuData.iCoronaSlotTarget = 0
	coronaMenuData.iCoronaPedIndex = 0
	
	IF DOES_CAM_EXIST(coronaMenuData.ciCoronaCam)
		PRINTLN("[CORONA] CLEANUP_CORONA_PED_PLACEMENT_VARS - Destroying ciCoronaCam: ", NATIVE_TO_INT(coronaMenuData.ciCoronaCam))
		DESTROY_CAM(coronaMenuData.ciCoronaCam)
	ENDIF
	
	IF DOES_CAM_EXIST(g_sTransitionSessionData.ciPedCam)
		PRINTLN("[CORONA] CLEANUP_CORONA_PED_PLACEMENT_VARS - Destroying ciPedCam: ", NATIVE_TO_INT(g_sTransitionSessionData.ciPedCam))
		DESTROY_CAM(g_sTransitionSessionData.ciPedCam)
	ENDIF
	
	IF bTurnOffScriptCams
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(coronaMenuData.oPistol)
		DELETE_OBJECT(coronaMenuData.oPistol)
	ENDIF
	IF DOES_ENTITY_EXIST(coronaMenuData.oAmmoClip)
		DELETE_OBJECT(coronaMenuData.oAmmoClip)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_PISTOL_CLIP_01))
	IF DOES_ENTITY_EXIST(coronaMenuData.pTempPed)
		DELETE_PED(coronaMenuData.pTempPed)
	ENDIF
	
	CLEAR_CORONA_BIT(CORONA_HEIST_HEELS_ADJUST_CAM_MOVEMENT)
ENDPROC




