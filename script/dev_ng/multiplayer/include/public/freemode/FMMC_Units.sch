
USING "FMMC_Vars.sch"

CONST_INT UNIT_TYPE_POLICE_ROADBLOCK		0
CONST_INT UNIT_TYPE_POLICE_ROADBLOCK_SINGLE 1
CONST_INT UNIT_TYPE_CROWD_ROADBLOCK			2
CONST_INT UNIT_TYPE_CROWD_ROADBLOCK_SINGLE	3
CONST_INT UNIT_TYPE_SINGLE_VEHICLE			4
CONST_INT UNIT_TYPE_GRID_GIRL				5
CONST_INT UNIT_TYPE_MAX						6

CONST_INT ciCROWD_ROADBLOCK_RETASK_TIME		3000

FUNC INT GET_NUMBER_OF_UNIT_VARIATIONS(INT iUnitType)
	SWITCH iUnitType
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			RETURN 10
	ENDSWITCH
	
	RETURN 0
ENDFUNC


FUNC INT GET_NUMBER_OF_UNIT_VEHICLES(INT iUnitType)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK
		CASE UNIT_TYPE_CROWD_ROADBLOCK
			RETURN 2
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE 
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
		CASE UNIT_TYPE_SINGLE_VEHICLE
			RETURN 1
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC VECTOR GET_CROWD_ROADBLOCK_SINGLE_VEHICLE_OFFSET(INT iVariation)
	
	IF iVariation != -1
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
		
	
	RETURN <<1.25, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_OFFSET_FOR_UNIT_VEHICLE(INT iUnitType, INT iVehicle, INT iVariation)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK
			SWITCH iVehicle
				CASE 0	RETURN <<2.0, 2.9, 0.0>>  //Left
				CASE 1	RETURN <<1.6, -2.8, 0.0>> //Right
			ENDSWITCH
		BREAK
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE
			RETURN <<0.0, 0.0, 0.0>> //Single vehicle
		BREAK
		CASE UNIT_TYPE_CROWD_ROADBLOCK
			SWITCH iVehicle
				CASE 0	RETURN <<1.25, 0.0, 0.0>>
				CASE 1	RETURN <<-2.5, -1.5, 0.0>>
			ENDSWITCH
		BREAK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			RETURN GET_CROWD_ROADBLOCK_SINGLE_VEHICLE_OFFSET(iVariation)
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_CROWD_ROADBLOCK_SINGLE_VEHICLE_HEADING(INT iVariation)
	SWITCH iVariation
		CASE 0	RETURN 300.0
		CASE 1	RETURN 60.0
		CASE 2	RETURN 105.0
		CASE 3	RETURN 240.0
		CASE 4	RETURN 185.0
		CASE 5	RETURN 185.0
		CASE 6	RETURN 185.0
		CASE 7	RETURN 100.0
		CASE 8	RETURN 100.0
		CASE 9	RETURN 185.0
	ENDSWITCH
	
	RETURN -60.0
ENDFUNC

FUNC FLOAT GET_HEADING_FOR_UNIT_VEHICLE(INT iUnitType, INT iVehicle, INT iVariation)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK
			SWITCH iVehicle
				CASE 0	RETURN  -85.0 //Left
				CASE 1	RETURN  169.0//Right
			ENDSWITCH
		BREAK
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE
			RETURN 169.0
		BREAK
		CASE UNIT_TYPE_CROWD_ROADBLOCK
			SWITCH iVehicle
				CASE 0	RETURN -60.0  //Left
				CASE 1	RETURN 15.0 //Right
			ENDSWITCH
		BREAK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			RETURN GET_CROWD_ROADBLOCK_SINGLE_VEHICLE_HEADING(iVariation)
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC MODEL_NAMES GET_DEFAULT_MODEL_NAME_FOR_UNIT_VEHICLE(INT iUnitType)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK 
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE 
			RETURN POLICE3
		CASE UNIT_TYPE_CROWD_ROADBLOCK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			RETURN CALICO
	ENDSWITCH
	
	RETURN DILETTANTE
ENDFUNC

FUNC INT GET_CROWD_ROADBLOCK_SINGLE_NUMBER_OF_PEDS(INT iVariation)
	SWITCH iVariation
		CASE 7
		CASE 8
		CASE 9
			RETURN 2
	ENDSWITCH
	
	RETURN 1
ENDFUNC

FUNC INT GET_NUMBER_OF_UNIT_PEDS(INT iUnitType, INT iVariation)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK
		CASE UNIT_TYPE_CROWD_ROADBLOCK
			RETURN 2
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE
			RETURN 1
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			RETURN GET_CROWD_ROADBLOCK_SINGLE_NUMBER_OF_PEDS(iVariation)
		CASE UNIT_TYPE_GRID_GIRL
			RETURN 1
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC VECTOR GET_CROWD_ROADBLOCK_SINGLE_PED_OFFSET(INT iVariation, INT iPed)
	SWITCH iVariation
		CASE 0		RETURN <<1.5, -1.0, 0.0>>
		CASE 1		RETURN <<-1.5, -1.0, 0.0>>
		CASE 2		RETURN <<-1.5, 1.0, 0.0>>
		CASE 3		RETURN <<1.8, 0.5, 0.0>>
		CASE 4		RETURN <<1.6, -0.8, 0.0>>
		CASE 5		RETURN <<0.9, -2.9, 0.0>>
		CASE 6		RETURN <<-0.4, -2.9, 0.0>>
		CASE 7
			SWITCH iPed
				CASE 0 RETURN <<0.9, -1.4, 0.0>>
				CASE 1 RETURN <<0.0, -1.5, 0.0>>
			ENDSWITCH
		BREAK
		CASE 8
			SWITCH iPed
				CASE 0 RETURN <<-1.5, 1.0, 0.0>>
				CASE 1 RETURN <<1.0, 1.5, 0.0>>
			ENDSWITCH
		BREAK
		CASE 9
			SWITCH iPed
				CASE 0 RETURN <<-1.5, -0.9, 0.0>>
				CASE 1 RETURN <<1.6, -0.8, 0.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.75, 0.0>>
ENDFUNC

FUNC VECTOR GET_OFFSET_FOR_UNIT_PED(INT iUnitType, INT iPed, INT iVariation)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK
			SWITCH iPed
				CASE 0	RETURN <<-1.3, 2.6, 0.0>>  //Left
				CASE 1	RETURN <<0.2, -1.1, 0.0>> //Right
			ENDSWITCH
		BREAK
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE
			RETURN <<-1.4, 1.7, 0.0>>
		BREAK
		CASE UNIT_TYPE_CROWD_ROADBLOCK
			SWITCH iPed
				CASE 0	RETURN <<0.0, 0.75, 0.0>>  //Left
				CASE 1	RETURN <<-1.5, 1.0, 0.0>> //Right
			ENDSWITCH
		BREAK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			RETURN GET_CROWD_ROADBLOCK_SINGLE_PED_OFFSET(iVariation, iPed)
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_CROWD_ROADBLOCK_SINGLE_PED_HEADING(INT iVariation, INT iPed)
	SWITCH iVariation
		CASE 0	RETURN 180.0
		CASE 1	RETURN 175.0
		CASE 2	RETURN 180.0
		CASE 3	RETURN 175.0
		CASE 4	RETURN 175.0
		CASE 5
		CASE 6	RETURN 180.0
		CASE 7
			SWITCH iPed
				CASE 0 RETURN 180.0
				CASE 1 RETURN 175.0
			ENDSWITCH
		BREAK
		CASE 8
			SWITCH iPed
				CASE 0 RETURN 180.0
				CASE 1 RETURN 185.0
			ENDSWITCH
		BREAK
		CASE 9
			SWITCH iPed
				CASE 0 RETURN 170.0
				CASE 1 RETURN 175.0
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_HEADING_FOR_UNIT_PED(INT iUnitType, INT iPed, INT iVariation)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK
			SWITCH iPed
				CASE 0	RETURN  -90.0
				CASE 1	RETURN  -90.0
			ENDSWITCH
		BREAK
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE
			RETURN -90.0
		BREAK
		CASE UNIT_TYPE_CROWD_ROADBLOCK
			SWITCH iPed
				CASE 0	RETURN 0.0  //Left
				CASE 1	RETURN 15.0 //Right
			ENDSWITCH
		BREAK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			RETURN GET_CROWD_ROADBLOCK_SINGLE_PED_HEADING(iVariation, iPed)
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC MODEL_NAMES GET_DEFAULT_MODEL_NAME_FOR_UNIT_PED(INT iUnitType)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK 
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE 
			RETURN S_M_Y_Cop_01
		CASE UNIT_TYPE_CROWD_ROADBLOCK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_CarClub_01"))
		CASE UNIT_TYPE_GRID_GIRL
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_CarClub_01"))
	ENDSWITCH
	
	RETURN S_M_Y_Cop_01
ENDFUNC

FUNC INT GET_NUMBER_OF_UNIT_DYNOPROPS(INT iUnitType)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK 		RETURN 3
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE	RETURN 2
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC VECTOR GET_OFFSET_FOR_UNIT_DYNOPROP(INT iUnitType, INT iDynoProp)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK
			SWITCH iDynoProp
				CASE 0	RETURN <<4.8, 3.6, 1.0>>
				CASE 1	RETURN <<3.88, -0.75, 1.0>>
				CASE 2	RETURN <<3.95, -4.5, 1.0>>
			ENDSWITCH
		BREAK
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE
			SWITCH iDynoProp
				CASE 0	RETURN <<1, -2.1, 1.0>>
				CASE 1	RETURN <<1.91, 1.64, 1.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 1.0>>
ENDFUNC

FUNC FLOAT GET_HEADING_FOR_UNIT_DYNOPROP(INT iUnitType, INT iDynoProp)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK
			SWITCH iDynoProp
				CASE 0	RETURN  33.0
				CASE 1	RETURN  75.0
				CASE 3	RETURN  12.0
			ENDSWITCH
		BREAK
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE
			SWITCH iDynoProp
				CASE 0	RETURN  33.0
				CASE 1	RETURN  75.0
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC MODEL_NAMES GET_DEFAULT_MODEL_NAME_FOR_UNIT_DYNOPROP(INT iUnitType)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK 
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE 
			RETURN PROP_ROADCONE02A
	ENDSWITCH
	
	RETURN PROP_ROADCONE02A
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Peds
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_PED_UNIT_STATE(FMMC_SINGLE_UNIT_RUNTIME_VARS &sUnitRuntime, INT iPed, FMMC_PED_UNIT_STATE eNewState)
	IF sUnitRuntime.sPedStruct[iPed].ePedUnitState != eNewState
		sUnitRuntime.sPedStruct[iPed].ePedUnitState = eNewState
		PRINTLN("[Units] SET_UNIT_STATE - Ped ",iPed ," Unit ",sUnitRuntime.iUnitNumber," now in state ", sUnitRuntime.sPedStruct[iPed].ePedUnitState)
	ENDIF
ENDPROC

   
STRUCT SCRIPT_EVENT_DATA_FMMC_SET_PED_UNIT_STATE
	STRUCT_EVENT_COMMON_DETAILS Details
	
	INT iUnitIndex
	INT iPed
	FMMC_PED_UNIT_STATE eNewState
ENDSTRUCT

PROC BROADCAST_FMMC_SET_PED_UNIT_STATE(INT iUnitIndex, INT iPed, FMMC_PED_UNIT_STATE eNewState)
	SCRIPT_EVENT_DATA_FMMC_SET_PED_UNIT_STATE Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_SET_PED_UNIT_STATE
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.iUnitIndex = iUnitIndex
	Event.iPed = iPed
	Event.eNewState = eNewState
	
	NET_PRINT("-----------------BROADCAST_FMMC_SET_PED_UNIT_STATE---------------------------") NET_NL()
	PRINTLN("Event.iUnitIndex - ", Event.iUnitIndex)
	PRINTLN("Event.iPed - ", Event.iPed)
	PRINTLN("Event.eNewState - ", Event.eNewState)
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_SET_PED_UNIT_STATE - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
ENDPROC

   
PROC PROCESS_SCRIPT_EVENT_FMMC_SET_PED_UNIT_STATE(INT iCount, FMMC_UNIT_RUNTIME_VARS &sUnitsRuntime)
	SCRIPT_EVENT_DATA_FMMC_SET_PED_UNIT_STATE EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	OR EventData.Details.Type != SCRIPT_EVENT_FMMC_SET_PED_UNIT_STATE
		EXIT
	ENDIF
	
	SET_PED_UNIT_STATE(sUnitsRuntime.sUnitVars[EventData.iUnitIndex], EventData.iPed, EventData.eNewState)
	
ENDPROC

FUNC BOOL SHOULD_PED_GO_INTO_COMBAT(FMMC_UNITS_STRUCT &sUnit, INT iPed)
	UNUSED_PARAMETER(sUnit)
	UNUSED_PARAMETER(iPed)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_START_ANIMATION(FMMC_UNITS_STRUCT &sUnit, INT iPed)
	UNUSED_PARAMETER(sUnit)
	UNUSED_PARAMETER(iPed)
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PED_RETURN_TO_ORIGIN(FMMC_UNITS_STRUCT &sUnit, INT iPed)
	UNUSED_PARAMETER(sUnit)
	UNUSED_PARAMETER(iPed)
	RETURN FALSE
ENDFUNC

FUNC INT GET_MAX_ANIM_INDEX_FOR_UNIT_PED(INT iUnitType)
	SWITCH iUnitType
		CASE UNIT_TYPE_CROWD_ROADBLOCK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			RETURN 5
	ENDSWITCH
	RETURN 1
ENDFUNC

FUNC STRING GET_ANIM_DICT_FOR_CROWD_UNIT(INT iAnimIndex)
	SWITCH iAnimIndex
		CASE 1 RETURN "amb@world_human_window_shop@male@idle_a"
		CASE 2 RETURN "amb@world_human_stand_impatient@male@no_sign@idle_a"
		CASE 3 RETURN "WORLD_HUMAN_MOBILE_FILM_SHOCKING" //Scenario
		CASE 4 RETURN "WORLD_HUMAN_DRINKING" //Scenario
	ENDSWITCH
	
	RETURN "random@street_race"
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_CROWD_UNIT(INT iAnimIndex)
	SWITCH iAnimIndex
		CASE 1 RETURN "browse_a"
		CASE 2 RETURN "idle_a"
		CASE 3 RETURN "" //Scenario
		CASE 4 RETURN "" //Scenario
	ENDSWITCH
	
	RETURN "_car_b_lookout"
ENDFUNC

FUNC STRING GET_UNIT_PED_IDLE_ANIM_DICT(INT iUnitType, INT iAnimIndex)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK 
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE 
			RETURN "amb@world_human_cop_idles@male@idle_a"
		CASE UNIT_TYPE_CROWD_ROADBLOCK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			RETURN GET_ANIM_DICT_FOR_CROWD_UNIT(iAnimIndex)
		CASE UNIT_TYPE_GRID_GIRL
			RETURN "random@street_race"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIMATION_LETTER_APPEND(INT iVariation)
	SWITCH iVariation
		CASE 0 RETURN "a"
		CASE 1 RETURN "b"
		CASE 2 RETURN "c"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC TEXT_LABEL_23 GET_UNIT_PED_IDLE_ANIM_NAME(INT iUnitType, INT iAnimIndex)
	TEXT_LABEL_23 tlAnimName
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK 
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE
			tlAnimName = "idle_"
			INT iRand
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			tlAnimName += GET_ANIMATION_LETTER_APPEND(iRand)
			RETURN tlAnimName
		BREAK
		CASE UNIT_TYPE_CROWD_ROADBLOCK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			tlAnimName = GET_ANIM_NAME_FOR_CROWD_UNIT(iAnimIndex)
		BREAK
		CASE UNIT_TYPE_GRID_GIRL
			tlAnimName = "grid_girl_race_start"
		BREAK
	ENDSWITCH
	
	RETURN tlAnimName
ENDFUNC

FUNC BOOL IS_UNIT_TYPE_READY_TO_HAVE_PED_PROCESSED(FMMC_UNIT_RUNTIME_VARS &sUnitsRuntime, INT iUnitIndex)
	SWITCH sUnitsRuntime.sUnitVars[iUnitIndex].iUnitType
		CASE UNIT_TYPE_GRID_GIRL
			RETURN IS_BIT_SET(sUnitsRuntime.iUnitRuntimeBS, ciUNIT_RUNTIME_BS_RACE_INTRO_STARTED)
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_UNIT_PED_VALID_TO_PROCESS(PED_INDEX &piPassed)
	IF NOT DOES_ENTITY_EXIST(piPassed)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(piPassed)
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(piPassed)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_UP_UNIT_PED_ROADBLOCK(PED_INDEX &piPassed)

	GIVE_DELAYED_WEAPON_TO_PED(piPassed, WEAPONTYPE_PISTOL, 25000, TRUE)
	SET_CURRENT_PED_WEAPON(piPassed, WEAPONTYPE_PISTOL, TRUE)
	
	SET_PED_AS_COP(piPassed, TRUE)
	SET_PED_CONFIG_FLAG(piPassed,PCF_CanAttackNonWantedPlayerAsLaw, FALSE)
	SET_PED_CONFIG_FLAG(piPassed,PCF_OnlyUpdateTargetWantedIfSeen, TRUE)
	
	SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_COWER, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_ALL_RANDOMS_FLEE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_PREFER_KNOWN_TARGETS_WHEN_COMBAT_CLOSEST_TARGET, TRUE)
	SET_PED_COMBAT_ABILITY(piPassed, CAL_POOR)
	SET_PED_ACCURACY(piPassed, 5)
	SET_PED_COMBAT_RANGE(piPassed, CR_NEAR)
	
	SET_PED_TARGET_LOSS_RESPONSE(piPassed, TLR_EXIT_TASK)
	
	VECTOR vPedCoord = GET_ENTITY_COORDS(piPassed)
	SET_PED_SPHERE_DEFENSIVE_AREA(piPassed, vPedCoord, 20.0)
	
ENDPROC

PROC SET_UP_UNIT_PED_CROWD_ROADBLOCK(PED_INDEX &piPassed)
	
	SET_PED_FLEE_ATTRIBUTES(piPassed, FA_PREFER_PAVEMENTS, TRUE)
	SET_PED_FLEE_ATTRIBUTES(piPassed, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, TRUE)
	SET_PED_FLEE_ATTRIBUTES(piPassed, FA_USE_VEHICLE, FALSE)
	
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FLEE, TRUE)
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(piPassed)
		NETWORK_INDEX niPed = NETWORK_GET_NETWORK_ID_FROM_ENTITY(piPassed)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niPed)
			SET_NETWORK_ID_CAN_MIGRATE(niPed, FALSE)
			SET_NETWORK_ID_CAN_BE_REASSIGNED(niPed, TRUE)
		ENDIF
	ENDIF
		
ENDPROC

PROC SET_UP_UNIT_PED(INT iUnitType, PED_INDEX &piPassed)
	
	IF IS_ENTITY_DEAD(piPassed)
		EXIT
	ENDIF
	
	IF NOT IS_UNIT_PED_VALID_TO_PROCESS(piPassed)
		EXIT
	ENDIF
	
	SET_PED_DIES_WHEN_INJURED(piPassed, TRUE)
	SET_PED_KEEP_TASK(piPassed, TRUE)
	SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPassed, FALSE)
	
	//Unit specific set ups
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK 
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE
			SET_UP_UNIT_PED_ROADBLOCK(piPassed)
		BREAK
		CASE UNIT_TYPE_CROWD_ROADBLOCK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			SET_UP_UNIT_PED_CROWD_ROADBLOCK(piPassed)
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_UP_UNIT_PEDS(FMMC_SINGLE_UNIT_RUNTIME_VARS &sUnitRuntime)
	INT iPed
	FOR iPed = 0 TO GET_NUMBER_OF_UNIT_PEDS(sUnitRuntime.iUnitType, sUnitRuntime.iUnitVariation)-1
		PRINTLN("[Units] SET_UP_UNIT_PEDS - Setting up ped ", iPed, " for unit ", sUnitRuntime.iUnitNumber)
		SET_UP_UNIT_PED(sUnitRuntime.iUnitType, sUnitRuntime.sUnitEntities.piPeds[iPed])
	ENDFOR
ENDPROC

FUNC BOOL IS_UNIT_PED_ANIMATION_A_ONESHOT(INT iUnitType)
	SWITCH iUnitType
		CASE UNIT_TYPE_GRID_GIRL
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_GRID_GIRL_ONE_SHOT_ANIM(PED_INDEX piPassed, TEXT_LABEL_63 tlAnimDict, TEXT_LABEL_23 tlAnimName)
	
	FLOAT fStartPhase = 0.0
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		fStartPhase = 0.29
	ENDIF
	
	SEQUENCE_INDEX siGridGirl
	OPEN_SEQUENCE_TASK(siGridGirl)
		TASK_PLAY_ANIM(NULL, tlAnimDict, tlAnimName, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_BLENDOUT_WRT_LAST_FRAME, fStartPhase)
		TASK_WANDER_STANDARD(NULL, GET_ENTITY_HEADING(piPassed))
	CLOSE_SEQUENCE_TASK(siGridGirl)
	TASK_PERFORM_SEQUENCE(piPassed, siGridGirl)
	CLEAR_SEQUENCE_TASK(siGridGirl)
ENDPROC

PROC PROCESS_UNIT_PED_TASK_ONE_SHOT(INT iUnitType, PED_INDEX piPassed, TEXT_LABEL_63 tlAnimDict, TEXT_LABEL_23 tlAnimName, FLOAT fStartPhase)
	SWITCH iUnitType
		CASE UNIT_TYPE_GRID_GIRL
			PROCESS_GRID_GIRL_ONE_SHOT_ANIM(piPassed, tlAnimDict, tlAnimName)
		BREAK
		DEFAULT
			TASK_PLAY_ANIM(piPassed, tlAnimDict, tlAnimName, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, fStartPhase)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_PROCESS_ANIM(STRING tlAnimDict, STRING tlAnimName)
	IF NOT IS_STRING_NULL_OR_EMPTY(tlAnimName)
		REQUEST_ANIM_DICT(tlAnimDict)
		RETURN HAS_ANIM_DICT_LOADED(tlAnimDict)		
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL TASK_UNIT_PED(FMMC_SINGLE_UNIT_RUNTIME_VARS &sUnitRuntime, PED_INDEX piPassed, INT iAnimIndex)
	IF NOT IS_PED_PERFORMING_SCRIPT_TASK(piPassed, SCRIPT_TASK_PLAY_ANIM)
	AND NOT IS_PED_PERFORMING_SCRIPT_TASK(piPassed, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
		TEXT_LABEL_63 tlAnimDict = GET_UNIT_PED_IDLE_ANIM_DICT(sUnitRuntime.iUnitType, iAnimIndex)
		TEXT_LABEL_23 tlAnimName = GET_UNIT_PED_IDLE_ANIM_NAME(sUnitRuntime.iUnitType, iAnimIndex)
		
		IF SHOULD_PROCESS_ANIM(tlAnimDict, tlAnimName)
			FLOAT fStartPhase
			IF IS_UNIT_PED_ANIMATION_A_ONESHOT(sUnitRuntime.iUnitType)
				PROCESS_UNIT_PED_TASK_ONE_SHOT(sUnitRuntime.iUnitType, piPassed, tlAnimDict, tlAnimName, fStartPhase)
			ELSE
				IF IS_STRING_NULL_OR_EMPTY(tlAnimName)
					TASK_START_SCENARIO_IN_PLACE(piPassed, tlAnimDict, DEFAULT, TRUE)
				ELSE
					fStartPhase = GET_RANDOM_FLOAT_IN_RANGE()
					TASK_PLAY_ANIM(piPassed, tlAnimDict, tlAnimName, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, fStartPhase)
				ENDIF
			ENDIF
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_START_UNIT_PED_ANIMS(FMMC_SINGLE_UNIT_RUNTIME_VARS &sUnitRuntime, INT iPed)

	IF IS_UNIT_PED_VALID_TO_PROCESS(sUnitRuntime.sUnitEntities.piPeds[iPed])
		PRINTLN("[Units] PROCESS_START_UNIT_PED_ANIMS - Starting anim for ped ", iPed, " for unit ", sUnitRuntime.iUnitNumber)
		IF NOT TASK_UNIT_PED(sUnitRuntime, sUnitRuntime.sUnitEntities.piPeds[iPed], sUnitRuntime.sPedStruct[iPed].iAnim)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_POLICE_ROAD_BLOCK_PEDS(FMMC_SINGLE_UNIT_RUNTIME_VARS &sUnitRuntime, INT iPed)
	IF IS_PED_IN_COMBAT(sUnitRuntime.sUnitEntities.piPeds[iPed])
		SET_BIT(sUnitRuntime.sPedStruct[iPed].iPedBitSet, ciUNIT_PED_BS__BEEN_IN_COMBAT)
		
	ELSE
		IF IS_BIT_SET(sUnitRuntime.sPedStruct[iPed].iPedBitSet, ciUNIT_PED_BS__BEEN_IN_COMBAT)
			CLEAR_BIT(sUnitRuntime.sPedStruct[iPed].iPedBitSet, ciUNIT_PED_BS__BEEN_IN_COMBAT)
			SET_PED_UNIT_STATE(sUnitRuntime, iPed, PED_UNIT_STATE_START_ANIM)
			BROADCAST_FMMC_SET_PED_UNIT_STATE(sUnitRuntime.iUnitNumber, iPed, PED_UNIT_STATE_START_ANIM)
			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CROWD_ROAD_BLOCK_PEDS(FMMC_SINGLE_UNIT_RUNTIME_VARS &sUnitRuntime, INT iPed)
	IF IS_PED_FLEEING(sUnitRuntime.sUnitEntities.piPeds[iPed])
		SET_PED_AS_NO_LONGER_NEEDED(sUnitRuntime.sUnitEntities.piPeds[iPed])
		RESET_NET_TIMER(sUnitRuntime.sPedStruct[iPed].tdReTaskTimer)
	ELSE
		IF NOT IS_PED_PERFORMING_SCRIPT_TASK(sUnitRuntime.sUnitEntities.piPeds[iPed], SCRIPT_TASK_PLAY_ANIM)
		AND NOT IS_PED_PERFORMING_SCRIPT_TASK(sUnitRuntime.sUnitEntities.piPeds[iPed], SCRIPT_TASK_START_SCENARIO_IN_PLACE)
		AND NOT IS_PED_GETTING_UP(sUnitRuntime.sUnitEntities.piPeds[iPed])
		AND NOT IS_PED_RAGDOLL(sUnitRuntime.sUnitEntities.piPeds[iPed])
			IF NOT HAS_NET_TIMER_STARTED(sUnitRuntime.sPedStruct[iPed].tdReTaskTimer)
				REINIT_NET_TIMER(sUnitRuntime.sPedStruct[iPed].tdReTaskTimer)
			ELIF HAS_NET_TIMER_EXPIRED(sUnitRuntime.sPedStruct[iPed].tdReTaskTimer, ciCROWD_ROADBLOCK_RETASK_TIME)
				SET_PED_UNIT_STATE(sUnitRuntime, iPed, PED_UNIT_STATE_START_ANIM)
				BROADCAST_FMMC_SET_PED_UNIT_STATE(sUnitRuntime.iUnitNumber, iPed, PED_UNIT_STATE_START_ANIM)
				RESET_NET_TIMER(sUnitRuntime.sPedStruct[iPed].tdReTaskTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_GRID_GIRL_PED(FMMC_SINGLE_UNIT_RUNTIME_VARS &sUnitRuntime, INT iPed, INT iUnitRuntimeBS)
	IF NOT IS_PED_PERFORMING_SCRIPT_TASK(sUnitRuntime.sUnitEntities.piPeds[iPed], SCRIPT_TASK_PERFORM_SEQUENCE)
	OR (IS_BIT_SET(iUnitRuntimeBS, ciUNIT_RUNTIME_BS_RACE_COUNTDOWN)
	AND IS_BIT_SET(iUnitRuntimeBS, ciUNIT_RUNTIME_BS_RACE_ONGOING))
		SET_PED_AS_NO_LONGER_NEEDED(sUnitRuntime.sUnitEntities.piPeds[iPed])
	ELSE
		IF IS_BIT_SET(iUnitRuntimeBS, ciUNIT_RUNTIME_BS_RACE_INTRO_HOLD)
		AND NOT IS_BIT_SET(iUnitRuntimeBS, ciUNIT_RUNTIME_BS_RACE_COUNTDOWN)
			SET_ANIM_PHASE(sUnitRuntime.sUnitEntities.piPeds[iPed], cfGRID_GIRL_HOLD_PHASE)
			PRINTLN("PROCESS_GRID_GIRL_PED - Holding anim")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_UNIT_PED_BEHAVIOUR(FMMC_SINGLE_UNIT_RUNTIME_VARS &sUnitRuntime, INT iPed, INT iUnitRuntimeBS)
	SWITCH sUnitRuntime.iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE
			PROCESS_POLICE_ROAD_BLOCK_PEDS(sUnitRunTime, iPed)
		BREAK
		CASE UNIT_TYPE_CROWD_ROADBLOCK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			PROCESS_CROWD_ROAD_BLOCK_PEDS(sUnitRunTime, iPed)
		BREAK
		CASE UNIT_TYPE_GRID_GIRL
			PROCESS_GRID_GIRL_PED(sUnitRunTime, iPed, iUnitRuntimeBS)
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_INDIVIDUAL_UNIT_PED(FMMC_SINGLE_UNIT_RUNTIME_VARS &sUnitRuntime, INT iPed, INT iUnitRuntimeBS)

	IF NOT IS_UNIT_PED_VALID_TO_PROCESS(sUnitRuntime.sUnitEntities.piPeds[iPed])
		PRINTLN("PROCESS_INDIVIDUAL_UNIT_PED - Ped ", iPed, " sUnitRuntime.iUnitNumber: ", sUnitRuntime.iUnitNumber, " invalid")
		EXIT
	ENDIF
	
	SWITCH sUnitRuntime.sPedStruct[iPed].ePedUnitState
		CASE PED_UNIT_STATE_INIT
			SET_PED_UNIT_STATE(sUnitRuntime, iPed, PED_UNIT_STATE_SETUP)
			BROADCAST_FMMC_SET_PED_UNIT_STATE(sUnitRuntime.iUnitNumber, iPed, PED_UNIT_STATE_SETUP)
		BREAK
		CASE PED_UNIT_STATE_SETUP
			SET_UP_UNIT_PEDS(sUnitRuntime)
			
			SET_PED_UNIT_STATE(sUnitRuntime, iPed, PED_UNIT_STATE_START_ANIM)
			BROADCAST_FMMC_SET_PED_UNIT_STATE(sUnitRuntime.iUnitNumber, iPed, PED_UNIT_STATE_START_ANIM)
		BREAK
		CASE PED_UNIT_STATE_START_ANIM
			IF PROCESS_START_UNIT_PED_ANIMS(sUnitRuntime, iPed)
				SET_PED_UNIT_STATE(sUnitRuntime, iPed, PED_UNIT_STATE_PROCESSING)
				BROADCAST_FMMC_SET_PED_UNIT_STATE(sUnitRuntime.iUnitNumber, iPed, PED_UNIT_STATE_PROCESSING)
			ENDIF
		BREAK
		CASE PED_UNIT_STATE_PROCESSING
			PROCESS_UNIT_PED_BEHAVIOUR(sUnitRuntime, iPed, iUnitRuntimeBS)
		BREAK
		CASE PED_UNIT_STATE_END_ANIM
			
		BREAK
		CASE PED_UNIT_STATE_CLEANUP
			
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_FMMC_UNIT_PEDS(FMMC_UNIT_RUNTIME_VARS &sUnitsRuntime, INT iUnitIndex)
	INT iPed
	FOR iPed = 0 TO GET_NUMBER_OF_UNIT_PEDS(sUnitsRuntime.sUnitVars[iUnitIndex].iUnitType, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitIndex].iVariation)-1
		PROCESS_INDIVIDUAL_UNIT_PED(sUnitsRuntime.sUnitVars[iUnitIndex], iPed, sUnitsRuntime.iUnitRuntimeBS)
	ENDFOR
ENDPROC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicles
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_UNIT_VEHICLE_LIGHTS(VEHICLE_INDEX &viVehicle)
	INT iHour = GET_CLOCK_HOURS()
	IF iHour > 20
	OR iHour < 6
		SET_VEHICLE_LIGHTS(viVehicle, FORCE_VEHICLE_LIGHTS_ON)
	ENDIF
ENDPROC

PROC SET_UP_UNIT_VEHICLE_SHARED(VEHICLE_INDEX &viVehicle, INT iVeh, INT iUnitNumber)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].sUnitVeh[iVeh].iVehicleBitSet, ciUNIT_VEH_FROZEN)
		FREEZE_ENTITY_POSITION(viVehicle, TRUE)
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viVehicle, TRUE)
		PRINTLN("[Units][Unit ",iUnitNumber,"][Vehicle ",iVeh,"] SET_UP_UNIT_VEHICLE_SHARED - Vehicle frozen (and locking doors)")
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnitNumber].sUnitVeh[iVeh].iVehicleBitSet, ciUNIT_VEH_DOORS_UNLOCKED)
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viVehicle, TRUE)
		PRINTLN("[Units][Unit ",iUnitNumber,"][Vehicle ",iVeh,"] SET_UP_UNIT_VEHICLE_SHARED - Locking doors")
	ENDIF
ENDPROC

PROC SET_UP_UNIT_POLICE_CAR(VEHICLE_INDEX &viVehicle)
	SET_VEHICLE_SIREN(viVehicle, TRUE)
	SET_SIREN_WITH_NO_DRIVER(viVehicle, TRUE)
	TRIGGER_SIREN_AUDIO(viVehicle)
ENDPROC

PROC SET_UP_UNIT_VEHICLE(INT iUnitType, VEHICLE_INDEX &viVehicle, INT iVeh, INT iUnitNumber)
	SWITCH iUnitType
		CASE UNIT_TYPE_POLICE_ROADBLOCK
		CASE UNIT_TYPE_POLICE_ROADBLOCK_SINGLE
			SET_UP_UNIT_POLICE_CAR(viVehicle)
			SET_UNIT_VEHICLE_LIGHTS(viVehicle)
		BREAK
		
		CASE UNIT_TYPE_CROWD_ROADBLOCK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			SET_UNIT_VEHICLE_LIGHTS(viVehicle)
		BREAK
	ENDSWITCH
	
	SET_UP_UNIT_VEHICLE_SHARED(viVehicle, iVeh, iUnitNumber)
ENDPROC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Initial Anim Set Up
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_UNIT_TYPE_REQUIRE_ANIM_STARTED_IMMEDIATELY(INT iUnitType)
	SWITCH iUnitType
		CASE UNIT_TYPE_GRID_GIRL
		CASE UNIT_TYPE_CROWD_ROADBLOCK
		CASE UNIT_TYPE_CROWD_ROADBLOCK_SINGLE
			RETURN TRUE
	ENDSWITCH	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_UNIT_PEDS_IN_ANIM_STATE_PROCESSING(FMMC_SINGLE_UNIT_RUNTIME_VARS &sUnitRuntime, INT iUnit)
	
	INT iPed
	FOR iPed = 0 TO GET_NUMBER_OF_UNIT_PEDS(sUnitRuntime.iUnitType, g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].iVariation)-1
		IF sUnitRuntime.sPedStruct[iPed].ePedUnitState < PED_UNIT_STATE_PROCESSING
			PRINTLN("[Units][Peds][Ped ", iPed,"] ARE_ALL_UNIT_PEDS_IN_ANIM_STATE_PROCESSING - ped not ready for staggered processing")
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL RUN_INITIAL_ANIMATION_SET_UP_FOR_UNITS(FMMC_UNIT_RUNTIME_VARS &sUnitsRuntime)
	INT iUnit
	FOR iUnit = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced-1
		IF DOES_UNIT_TYPE_REQUIRE_ANIM_STARTED_IMMEDIATELY(sUnitsRuntime.sUnitVars[iUnit].iUnitType)
			
			IF NOT IS_UNIT_TYPE_READY_TO_HAVE_PED_PROCESSED(sUnitsRuntime, iUnit)
				RETURN FALSE
			ENDIF
			
			PROCESS_FMMC_UNIT_PEDS(sUnitsRuntime, iUnit)
			IF NOT ARE_ALL_UNIT_PEDS_IN_ANIM_STATE_PROCESSING(sUnitsRuntime.sUnitVars[iUnit], iUnit)
				PRINTLN("[Units][Unit ", iUnit,"] RUN_INITIAL_ANIMATION_SET_UP_FOR_UNITS - Unit not ready for staggered processing")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_FMMC_UNITS(FMMC_UNIT_RUNTIME_VARS &sUnitsRuntime)
	
	IF NOT IS_BIT_SET(sUnitsRuntime.iUnitRuntimeBS, ciUNIT_RUNTIME_BS_CACHED)
		EXIT
	ENDIF
		
	PRINTLN("PROCESS_FMMC_UNITS - Running")
	
		
	IF NOT IS_BIT_SET(sUnitsRuntime.iUnitRuntimeBS, ciUNIT_RUNTIME_BS_INITIAL_ANIM_SET_UP)
		IF RUN_INITIAL_ANIMATION_SET_UP_FOR_UNITS(sUnitsRuntime)
			SET_BIT(sUnitsRuntime.iUnitRuntimeBS, ciUNIT_RUNTIME_BS_INITIAL_ANIM_SET_UP)
			PRINTLN("[Units] PROCESS_FMMC_UNITS - ciUNIT_RUNTIME_BS_INITIAL_ANIM_SET_UP is now set")
		ENDIF
		
		EXIT
	ENDIF
		
	IF IS_UNIT_TYPE_READY_TO_HAVE_PED_PROCESSED(sUnitsRuntime, sUnitsRuntime.iStaggeredUnitsIterator)	
		PROCESS_FMMC_UNIT_PEDS(sUnitsRuntime, sUnitsRuntime.iStaggeredUnitsIterator)
	ENDIF
	
	sUnitsRuntime.iStaggeredUnitsIterator++
	IF sUnitsRuntime.iStaggeredUnitsIterator >= g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced
		sUnitsRuntime.iStaggeredUnitsIterator = 0
	ENDIF
ENDPROC

FUNC BOOL IS_PED_ANIMATION_UNIQUE_IN_UNIT(FMMC_UNIT_RUNTIME_VARS &sUnitsRuntime, INT iUnit, INT iPedToCheck)
	
	//Unit doesn't have enough unique animations for each ped
	IF iPedToCheck >= GET_MAX_ANIM_INDEX_FOR_UNIT_PED(sUnitsRuntime.sUnitVars[iUnit].iUnitType)
		PRINTLN("[Units][Unit ",iUnit,"] IS_PED_ANIMATION_UNIQUE_IN_UNIT - Ped ", iPedToCheck, " has an index >= to the number of animations for this unit type")
		RETURN TRUE
	ENDIF
	
	INT iPed
	FOR iPed = 0 TO GET_NUMBER_OF_UNIT_PEDS(sUnitsRuntime.sUnitVars[iUnit].iUnitType, sUnitsRuntime.sUnitVars[iUnit].iUnitVariation)-1
		IF iPedToCheck = iPed
			RELOOP
		ENDIF
		
		IF sUnitsRuntime.sUnitVars[iUnit].sPedStruct[iPedToCheck].iAnim = -1
			RETURN FALSE
		ENDIF
		
		IF sUnitsRuntime.sUnitVars[iUnit].sPedStruct[iPedToCheck].iAnim = sUnitsRuntime.sUnitVars[iUnit].sPedStruct[iPed].iAnim
			PRINTLN("[Units][Unit ",iUnit,"] IS_PED_ANIMATION_UNIQUE_IN_UNIT - Peds ", iPedToCheck, " & ", iPed, " have the same animations")
			PRINTLN("[Units][Unit ",iUnit,"] IS_PED_ANIMATION_UNIQUE_IN_UNIT - iPedToCheck anim: ", sUnitsRuntime.sUnitVars[iUnit].sPedStruct[iPedToCheck].iAnim, " & iPed Anim: ", sUnitsRuntime.sUnitVars[iUnit].sPedStruct[iPed].iAnim)
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC INIT_FMMC_UNIT_ENTITIES(FMMC_UNIT_RUNTIME_VARS &sUnitsRuntime, UNIT_NETWORK_ENTITY_STRUCT &sUnitNetworkEntities[])
	
	IF IS_BIT_SET(sUnitsRuntime.iUnitRuntimeBS, ciUNIT_RUNTIME_BS_CACHED)
		EXIT
	ENDIF
	
	INT iUnit
	PRINTLN("[Units] INIT_FMMC_UNIT_ENTITIES - Setting up start data")
	FOR iUnit = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced-1
		
		INT iVeh, iPed, iObj
		
		sUnitsRuntime.sUnitVars[iUnit].iUnitNumber = iUnit
		sUnitsRuntime.sUnitVars[iUnit].iUnitType = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].iUnitType
		sUnitsRuntime.sUnitVars[iUnit].iUnitVariation = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].iVariation
		
		FOR iVeh = 0 TO GET_NUMBER_OF_UNIT_VEHICLES(sUnitsRuntime.sUnitVars[iUnit].iUnitType)-1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sUnitNetworkEntities[iUnit].viVehicles[iVeh])
				PRINTLN("[Units][Vehicles][Vehicle ",iVeh,"] INIT_FMMC_UNIT_ENTITIES - Caching net veh ", iVeh, " for unit ", iUnit)
				sUnitsRuntime.sUnitVars[iUnit].sUnitEntities.viVehicles[iVeh] = NET_TO_VEH(sUnitNetworkEntities[iUnit].viVehicles[iVeh])
			ENDIF
		ENDFOR
		
		FOR iPed = 0 TO GET_NUMBER_OF_UNIT_PEDS(sUnitsRuntime.sUnitVars[iUnit].iUnitType, sUnitsRuntime.sUnitVars[iUnit].iUnitVariation)-1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sUnitNetworkEntities[iUnit].piPeds[iPed])
				PRINTLN("[Units][Peds][Ped ",iPed,"] INIT_FMMC_UNIT_ENTITIES - Caching net ped ", iPed, " for unit ", iUnit)
				sUnitsRuntime.sUnitVars[iUnit].sUnitEntities.piPeds[iPed] = NET_TO_PED(sUnitNetworkEntities[iUnit].piPeds[iPed])
				WHILE NOT IS_PED_ANIMATION_UNIQUE_IN_UNIT(sUnitsRuntime, iUnit, iPed)
					sUnitsRuntime.sUnitVars[iUnit].sPedStruct[iPed].iAnim = GET_RANDOM_INT_IN_RANGE(0, GET_MAX_ANIM_INDEX_FOR_UNIT_PED(sUnitsRuntime.sUnitVars[iUnit].iUnitType))
					PRINTLN("[Units][Peds][Ped ",iPed,"] INIT_FMMC_UNIT_ENTITIES - Anim ", sUnitsRuntime.sUnitVars[iUnit].sPedStruct[iPed].iAnim)
				ENDWHILE
			ENDIF
		ENDFOR
		
		FOR iObj = 0 TO GET_NUMBER_OF_UNIT_DYNOPROPS(sUnitsRuntime.sUnitVars[iUnit].iUnitType)-1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sUnitNetworkEntities[iUnit].oiProps[iObj])
				PRINTLN("[Units][Objects][Object ",iObj,"] INIT_FMMC_UNIT_ENTITIES - Caching net obj ", iObj, " for unit ", iUnit)
				sUnitsRuntime.sUnitVars[iUnit].sUnitEntities.oiProps[iObj] = NET_TO_OBJ(sUnitNetworkEntities[iUnit].oiProps[iObj])
			ENDIF
		ENDFOR

	ENDFOR
	
	SET_BIT(sUnitsRuntime.iUnitRuntimeBS, ciUNIT_RUNTIME_BS_CACHED)
	
ENDPROC

PROC INIT_TEST_UNIT_ENTITIES(FMMC_UNIT_RUNTIME_VARS &sUnitsRuntime, UNIT_PLACEMENT_STRUCT &sEntities[])
	
	IF IS_BIT_SET(sUnitsRuntime.iUnitRuntimeBS, ciUNIT_RUNTIME_BS_CACHED)
		EXIT
	ENDIF

	INT iUnit
	PRINTLN("[Units] INIT_TEST_UNIT_ENTITIES - Setting up start data")
	FOR iUnit = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced-1
		INT iVeh, iPed, iObj
		
		sUnitsRuntime.sUnitVars[iUnit].iUnitNumber = iUnit
		sUnitsRuntime.sUnitVars[iUnit].iUnitType = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].iUnitType
		sUnitsRuntime.sUnitVars[iUnit].iUnitVariation = g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].iVariation
		
		FOR iVeh = 0 TO GET_NUMBER_OF_UNIT_VEHICLES(sUnitsRuntime.sUnitVars[iUnit].iUnitType)-1
			IF DOES_ENTITY_EXIST(sEntities[iUnit].viVehicles[iVeh])
				PRINTLN("[Units][Vehicles][Vehicle ",iVeh,"] INIT_TEST_UNIT_ENTITIES - Caching veh")
				sUnitsRuntime.sUnitVars[iUnit].sUnitEntities.viVehicles[iVeh] = sEntities[iUnit].viVehicles[iVeh]
			ENDIF
		ENDFOR
		FOR iPed = 0 TO GET_NUMBER_OF_UNIT_PEDS(sUnitsRuntime.sUnitVars[iUnit].iUnitType, sUnitsRuntime.sUnitVars[iUnit].iUnitVariation)-1
			IF DOES_ENTITY_EXIST(sEntities[iUnit].piPeds[iPed])
				PRINTLN("[Units][Peds][Ped ",iPed,"] INIT_TEST_UNIT_ENTITIES - Caching net ped")
				sUnitsRuntime.sUnitVars[iUnit].sUnitEntities.piPeds[iPed] = sEntities[iUnit].piPeds[iPed]
				WHILE NOT IS_PED_ANIMATION_UNIQUE_IN_UNIT(sUnitsRuntime, iUnit, iPed)
					sUnitsRuntime.sUnitVars[iUnit].sPedStruct[iPed].iAnim = GET_RANDOM_INT_IN_RANGE(0, GET_MAX_ANIM_INDEX_FOR_UNIT_PED(sUnitsRuntime.sUnitVars[iUnit].iUnitType))
					PRINTLN("[Units][Peds][Ped ",iPed,"] INIT_TEST_UNIT_ENTITIES - Anim ", sUnitsRuntime.sUnitVars[iUnit].sPedStruct[iPed].iAnim)
				ENDWHILE
			ENDIF
		ENDFOR
		FOR iObj = 0 TO GET_NUMBER_OF_UNIT_DYNOPROPS(sUnitsRuntime.sUnitVars[iUnit].iUnitType)-1
			IF DOES_ENTITY_EXIST(sEntities[iUnit].oiProps[iObj])
				PRINTLN("[Units][Objects][Object ",iObj,"] INIT_TEST_UNIT_ENTITIES - Caching net obj")
				sUnitsRuntime.sUnitVars[iUnit].sUnitEntities.oiProps[iObj] = sEntities[iUnit].oiProps[iObj]
			ENDIF
		ENDFOR
	ENDFOR
	
	SET_BIT(sUnitsRuntime.iUnitRuntimeBS, ciUNIT_RUNTIME_BS_CACHED)
ENDPROC

PROC CLEAR_UNITS_RUNTIME_STRUCT(FMMC_UNIT_RUNTIME_VARS &sUnitsRuntime)
	FMMC_UNIT_RUNTIME_VARS sEmptyStruct
	sUnitsRuntime = sEmptyStruct
ENDPROC
