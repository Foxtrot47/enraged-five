//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_casino.sch																							//
// Description: This is the implementation of the casino chips and playtime tracking									//
// Written by:  Online Technical Design: Mark Richardson																//
// Date:  		27/02/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_include.sch"
USING "menu_public.sch"
USING "shop_public.sch"

#IF FEATURE_CASINO
ENUM CASINO_CHIPS_UPDATE
	CCU_HOUSE_PURCHASE,		//The player has purchased chips from the casino store
	CCU_HOUSE_SELL,			//The player has sold chips for cash at the casino store
	CCU_HOUSE_BET,			//The player has placed a bet at one of the table games
	CCU_HOUSE_PAYOUT,		//The player has won a reward from a casino game
	CCU_COLLECTABLES,		//The player has collected one or more of the secret hidden items
	CCU_MEBERSHIP_BONUS,	//The player has purchased a membership to the casino
	CCU_MISSION_REWARD,		//The player is awarded chips for completing casino management missions
	CCU_DAILY_AWARD,		//The player is allowed to collect 1000 (Tuneable) chips for free per gameday
	CCU_BET_RETURN,			//The player can cancel their bet in some games
	CCU_PURCHASE_BONUS,		//A bonus given for purchasing selected items on a website
	CCU_PURCHASE_SHOP_ITEM,	//The player has purchased a item from casino shop
	CCU_BIG_MISSION_REWARD	//The player is awarded chips for completing casino DJ missions
	#IF IS_DEBUG_BUILD
	,CCU_DEBUG_ADD
	,CCU_DEBUG_REMOVE
	#ENDIF
ENDENUM

ENUM CASINO_BAN_REASON
	CBR_WON_TOO_MUCH,
	CBR_LOST_TOO_MUCH,
	CBR_TIME_SPENT_PLAYING,
	
	CBR_NOT_BANNED
ENDENUM

#IF IS_DEBUG_BUILD
STRUCT CASINO_DEBUG_WIDGET_DATA
	//Chip restrictions
	INT iChipsToAddRemove
	BOOL bAddChips
	BOOL bRemoveChips
	BOOL bResetRestrictions
	BOOL bDisplayChips
	
	//Inside track
	BOOL bDrawDebugInsideTrackBet
	BOOL bDrawDebugInsideTrackHostMainEvent
	BOOL bDrawDebugInsideTrackRewardMainEvent
	
	//Gambling time restrictions
	BOOL bStartGamblingTracking 		= FALSE
	BOOL bStopGamblingTracking 			= FALSE
	BOOL bResetGamblingTimeTracking 	= FALSE
	BOOL bMoveGamblingTrackingToNextDay = FALSE
	BOOL bSimulateTrackDayEnd			= FALSE
	BOOL bBanPlayer						= FALSE
	BOOL bReduceGamblingBan				= FALSE
	
	INT iTimeToSimulateUntilEOD			= 1
	INT iAddMinutesToTime = 1
	BOOL bAddMinutesToTime
	BOOL bAddMinutesToTimeGMBL
	
	//Debug display
	BOOL bEnableDisplayRepositioning 	= FALSE
	BOOL bDebugDrawTrackingData 		= FALSE
	BOOL bResetDisplayPosition 			= FALSE
	BOOL bDebugDisplayTakeSC			= FALSE
	
	//Transaction
	GENERIC_TRANSACTION_STATE eBuyTransactionState = TRANSACTION_STATE_DEFAULT
	GENERIC_TRANSACTION_STATE eSellTransactionState = TRANSACTION_STATE_DEFAULT
ENDSTRUCT

FUNC STRING GET_DEBUG_STRING_CHIPS_TOTAL_UPDATE_REASON(CASINO_CHIPS_UPDATE eUpdateReason)
	SWITCH eUpdateReason
		CASE CCU_HOUSE_PURCHASE 	RETURN "CCU_HOUSE_PURCHASE"		BREAK
		CASE CCU_HOUSE_SELL			RETURN "CCU_HOUSE_SELL"			BREAK
		CASE CCU_HOUSE_BET 			RETURN "CCU_HOUSE_BET"			BREAK
		CASE CCU_HOUSE_PAYOUT 		RETURN "CCU_HOUSE_PAYOUT"		BREAK
		CASE CCU_DEBUG_ADD			RETURN "CCU_DEBUG_ADD"			BREAK
		CASE CCU_DEBUG_REMOVE		RETURN "CCU_DEBUG_REMOVE"		BREAK
		CASE CCU_COLLECTABLES		RETURN "CCU_COLLECTABLES"		BREAK
		CASE CCU_MISSION_REWARD		RETURN "CCU_MISSION_REWARD"		BREAK
		CASE CCU_BIG_MISSION_REWARD	RETURN "CCU_BIG_MISSION_REWARD"	BREAK
		CASE CCU_MEBERSHIP_BONUS	RETURN "CCU_MEBERSHIP_BONUS"	BREAK
		CASE CCU_DAILY_AWARD		RETURN "CCU_DAILY_AWARD"		BREAK
		CASE CCU_BET_RETURN			RETURN "CCU_BET_RETURN"			BREAK
		CASE CCU_PURCHASE_BONUS		RETURN "CCU_PURCHASE_BONUS"		BREAK
		CASE CCU_PURCHASE_SHOP_ITEM	RETURN "CCU_PURCHASE_SHOP_ITEM"	BREAK
	ENDSWITCH
	
	RETURN "****INVALID****"
ENDFUNC

FUNC STRING GET_DEBUG_STRING_CASINO_GAME_BAN_REASON(CASINO_BAN_REASON eUpdateReason)
	SWITCH eUpdateReason
		CASE CBR_WON_TOO_MUCH 		RETURN "CBR_WON_TOO_MUCH"		BREAK
		CASE CBR_LOST_TOO_MUCH		RETURN "CBR_LOST_TOO_MUCH"		BREAK
		CASE CBR_TIME_SPENT_PLAYING RETURN "CBR_TIME_SPENT_PLAYING"	BREAK
		CASE CBR_NOT_BANNED			RETURN "CBR_NOT_BANNED"			BREAK
	ENDSWITCH
	
	RETURN "****INVALID****"
ENDFUNC

FUNC STRING GET_CASINO_GAME_DEBUG_STRING(CASINO_GAMES eGame)
	SWITCH eGame
		CASE CG_BLACKJACK 		RETURN "CG_BLACKJACK"		BREAK
		CASE CG_3CARDPOKER		RETURN "CG_3CARDPOKER"		BREAK
		CASE CG_SLOTS 			RETURN "CG_SLOTS"			BREAK
		CASE CG_ROULETTE 		RETURN "CG_ROULETTE"		BREAK
		CASE CG_BIGWHEEL		RETURN "CG_BIGWHEEL"		BREAK
		CASE CG_INSIDE_TRACK	RETURN "CG_INSIDE_TRACK"	BREAK
		#IF IS_DEBUG_BUILD
		CASE CG_DEBUG			RETURN "CG_DEBUG"			BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN "****INVALID****"
ENDFUNC

DEBUGONLY PROC SAVE_CASINO_DEBUG_TIME_TRACKING_SCREENSHOT()
	IF g_bEnableCasinoDisplayScreenshots
		DEBUG_PRINTCALLSTACK()
		TEXT_LABEL_63 tl23FileName = "CASINO_GMBL_TIME_SC_"
		tl23FileName += g_iCasinoGMBLTimeScreenshots
		tl23FileName += "_"
		tl23FileName += GET_CLOUD_TIME_AS_INT()
		g_iCasinoGMBLTimeScreenshots ++
		
		PRINTLN("[GAMBLING_TIME] SAVE_CASINO_DEBUG_TIME_TRACKING_SCREENSHOT - Saving screenshot: ", tl23FileName)
		SAVE_SCREENSHOT(tl23FileName)
	ELSE
		PRINTLN("[GAMBLING_TIME] SAVE_CASINO_DEBUG_TIME_TRACKING_SCREENSHOT - Screenshots disabled")
	ENDIF
ENDPROC
#ENDIF	//IS_DEBUG_BUILD

FUNC BOOL IS_PLAYER_PLAYING_A_CASINO_MINIGAME(PLAYER_INDEX player)
	IF player = INVALID_PLAYER_INDEX()
		PRINTLN("[CASINO_CHIPS] IS_PLAYER_PLAYING_A_CASINO_MINIGAME - INVALID_PLAYER!")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(player)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_BLACKJACK)
	OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(player)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_THREE_CARD_POKER)
	OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(player)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_ROULETTE)
	OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(player)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_LUCKY_WHEEL)
	OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(player)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_SLOTS)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].propertyDetails.sCasinoPropertyData.iBSCasino, PROPERTY_BROADCAST_BS_CASINO_INSIDE_TRACK_MINIGAME_ACTIVE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the number of house chips the player owns
FUNC INT GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL()
	#IF IS_DEBUG_BUILD
		IF g_bEnableChipsBypass
			RETURN 50000
		ENDIF
	#ENDIF
	
	RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_CASINO_CHIPS)
ENDFUNC

FUNC INT GET_NUMBER_OF_CHIPS_FOR_VISITOR_BONUS()
	RETURN g_sMPTunables.iVC_CASINO_CHIP_VISITOR
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ House chips HUD ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

DEBUGONLY FUNC STRING GET_CHIPS_HUD_STATE_NAME(CHIPS_HUD_STATE eState)
	SWITCH eState
		CASE CHS_UNLOADED	RETURN "CHS_UNLOADED"	BREAK
		CASE CHS_LOADING	RETURN "CHS_LOADING"	BREAK
		CASE CHS_LOADED		RETURN "CHS_LOADED"		BREAK
		CASE CHS_DRAWING	RETURN "CHS_DRAWING"	BREAK
		CASE CHS_CLEANUP	RETURN "CHS_CLEANUP"	BREAK
	ENDSWITCH
	
	RETURN "**Invalid**"
ENDFUNC

CONST_INT HUD_CHIPS_BS_REQUEST_CLEANUP				0	//Cleanup the scaleform movie
CONST_INT HUD_CHIPS_BS_CHIPS_TOTAL_UPDATE			1	//Update the scaleform movie with the new chips total
CONST_INT HUD_CHIPS_BS_PAUSE_CHIPS_TOTAL_UPDATE		2	//Pause the update of the chips total - Useful for games that need to make win/loss decisions before displaying the result to the player e.g. slots
CONST_INT HUD_CHIPS_BS_HIDE_CHIPS_HUD				3	//Activley hide the chips HUD
CONST_INT HUD_CHIPS_BS_HIDE_CHIPS_HUD_THIS_FRAME	4	//Activley hide the chips HUD for one frame only
CONST_INT HUD_CHIPS_BS_REQUEST_DISPLAY				5	//Request script start displaying the chips HUD
CONST_INT HUD_CHIPS_BS_REQUEST_CHIP_CHANGE_DISPLAY	6	//Request script start displaying the chips change HUD
CONST_INT HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT			7	//Has SF been sent a house chips value to display by script
CONST_INT HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT_CHANGE	8	//Has SF been sent a house chips value to display by script
CONST_INT HUD_CHIPS_BS_CHIPS_CHANGE_UPDATE			9	//Update the scaleform movie with the new chips +/-

FUNC BOOL HAS_CASINO_CHIPS_HUD_DISPLAY_BEEN_REQUESTED()
	RETURN (IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_CHIP_CHANGE_DISPLAY) OR IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_DISPLAY))
ENDFUNC

FUNC BOOL IS_CASINO_CHIPS_HUD_UPDATE_PAUSED()
	RETURN IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_PAUSE_CHIPS_TOTAL_UPDATE)
ENDFUNC

FUNC BOOL _IS_CASINO_CHIPS_TOTAL_DISPLAY_PENDING_UPDATE()
	RETURN IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_CHIPS_TOTAL_UPDATE)
ENDFUNC

PROC _REQUEST_CHIPS_HUD_TOTAL_UPDATE()
	IF HAS_CASINO_CHIPS_HUD_DISPLAY_BEEN_REQUESTED()
		PRINTLN("[HUD_CHIPS] _REQUEST_CHIPS_HUD_TOTAL_UPDATE - Will Update SF with new chip total")
		SET_BIT(g_iChipsHudBS, HUD_CHIPS_BS_CHIPS_TOTAL_UPDATE)
	ELSE
		PRINTLN("[HUD_CHIPS] _REQUEST_CHIPS_HUD_TOTAL_UPDATE - Display is not active. Ignoring request")
	ENDIF
ENDPROC

FUNC BOOL _IS_CASINO_CHIPS_CHANGE_DISPLAY_PENDING_UPDATE()
	RETURN IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_CHIPS_CHANGE_UPDATE)
ENDFUNC

PROC _REQUEST_CHIPS_CHANGE_HUD_UPDATE()
	IF IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_CHIP_CHANGE_DISPLAY)
		PRINTLN("[HUD_CHIPS] _REQUEST_CHIPS_CHANGE_HUD_UPDATE - Will Update SF with new +/- value")
		SET_BIT(g_iChipsHudBS, HUD_CHIPS_BS_CHIPS_CHANGE_UPDATE)
		REINIT_NET_TIMER(g_stChipChangeHUDDisplay)
	ELSE
		PRINTLN("[HUD_CHIPS] _REQUEST_CHIPS_CHANGE_HUD_UPDATE - Display is not active. Ignoring request")
	ENDIF
ENDPROC

/// PURPOSE:
///    Hide the casino chips HUD temporarily - Must be called with (FALSE) to allow the display to begin again
PROC SET_CASINO_CHIPS_HUD_HIDDEN(BOOL bSet)
	DEBUG_PRINTCALLSTACK()
	IF bSet
		#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_HIDE_CHIPS_HUD)
				PRINTLN("[CASINO_CHIPS][HUD_CHIPS] SET_CASINO_CHIPS_HUD_HIDDEN - TRUE")
			ENDIF
		#ENDIF
		
		SET_BIT(g_iChipsHudBS, HUD_CHIPS_BS_HIDE_CHIPS_HUD)
	ELSE
		#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_HIDE_CHIPS_HUD)
				PRINTLN("[CASINO_CHIPS][HUD_CHIPS] SET_CASINO_CHIPS_HUD_HIDDEN - FALSE")
			ENDIF
		#ENDIF
		
		CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_HIDE_CHIPS_HUD)
	ENDIF
ENDPROC

/// PURPOSE:
///    Per frame variant of SET_CASINO_CHIPS_HUD_HIDDEN
PROC HIDE_CASINO_CHIPS_HUD_THIS_FRAME()
	PRINTLN("[HUD_CHIPS] HIDE_CASINO_CHIPS_HUD_THIS_FRAME - Will prevent HUD draw next update")
	SET_BIT(g_iChipsHudBS, HUD_CHIPS_BS_HIDE_CHIPS_HUD_THIS_FRAME)
ENDPROC

/// PURPOSE:
///    Request that script activates the casino chips HUD display
PROC REQUEST_CASINO_CHIPS_HUD_DISPLAY()
	PRINTLN("[HUD_CHIPS] REQUEST_CASINO_CHIPS_HUD_DISPLAY - Called")
	SET_BIT(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_DISPLAY)
ENDPROC

/// PURPOSE:
///    Request that script activates the casino chip +/- HUD display
PROC REQUEST_CASINO_CHIPS_CHANGE_HUD_DISPLAY(INT iAmount, BOOL bIncrement)
	IF iAmount < 0
		SCRIPT_ASSERT("REQUEST_CASINO_CHIPS_CHANGE_HUD_DISPLAY - Given invalid amount")
		EXIT
	ENDIF
	
	PRINTLN("[HUD_CHIPS] REQUEST_CASINO_CHIPS_CHANGE_HUD_DISPLAY - Called. iAmount: ", iAmount, " bIncrement: ", bIncrement)
	SET_BIT(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_CHIP_CHANGE_DISPLAY)
	g_iChipsHUDChangeAmount 	= iAmount
	g_bChipsHUDChangeIncrement 	= bIncrement
ENDPROC

/// PURPOSE:
///    Removes the casino chips HUD display if it is active
PROC SET_CASINO_CHIPS_HUD_DISPLAY_AS_NO_LONGER_NEEDED()
	PRINTLN("[HUD_CHIPS] SET_CASINO_CHIPS_HUD_DISPLAY_AS_NO_LONGER_NEEDED - Called")
	CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_DISPLAY)
	SET_CASINO_CHIPS_HUD_HIDDEN(FALSE)
ENDPROC

/// PURPOSE:
///    Prevent the casino chips HUD from updating SF with the latest chip amount.
/// PARAMS:
///    bSet - Should the update be paused
PROC SET_PAUSE_CASINO_CHIPS_HUD_UPDATE(BOOL bSet)
	IF bSet
		#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_PAUSE_CHIPS_TOTAL_UPDATE)
				PRINTLN("[CASINO_CHIPS][HUD_CHIPS] SET_PAUSE_CASINO_CHIPS_HUD_UPDATE - TRUE")
			ENDIF
		#ENDIF
		
		SET_BIT(g_iChipsHudBS, HUD_CHIPS_BS_PAUSE_CHIPS_TOTAL_UPDATE)
	ELSE
		#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_PAUSE_CHIPS_TOTAL_UPDATE)
				PRINTLN("[CASINO_CHIPS][HUD_CHIPS] SET_PAUSE_CASINO_CHIPS_HUD_UPDATE - FALSE")
			ENDIF
		#ENDIF
		
		CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_PAUSE_CHIPS_TOTAL_UPDATE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns true if the hide or per frame hide command is active
FUNC BOOL IS_CASINO_CHIPS_HUD_HIDDEN_BY_SCRIPT()
	IF IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_HIDE_CHIPS_HUD)
	OR IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_HIDE_CHIPS_HUD_THIS_FRAME)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Are we in a situation where it is suitable to display the chips HUD
FUNC BOOL IS_SAFE_TO_DISLAY_CASINO_CHIPS_HUD()
	IF IS_SCREEN_FADED_OUT()
		RETURN FALSE
	ENDIF 
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		RETURN FALSE
	ENDIF

	IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
	AND !IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_CLOTHES)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Requests the script managed chips HUD be cleaned up and the SF movie unloaded
PROC CLEANUP_CASINO_CHIPS_HUD()
	PRINTLN("[HUD_CHIPS] CLEANUP_CASINO_CHIPS_HUD")
	SET_BIT(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_CLEANUP)
ENDPROC

/// PURPOSE:
///    State of the chips display
PROC SET_CASINO_CHIPS_HUD_STATE(CHIPS_HUD_STATE eNewState)
	IF eNewState != g_eChipsHudState
		PRINTLN("[HUD_CHIPS] SET_CASINO_CHIPS_HUD_STATE - Change state from: ", GET_CHIPS_HUD_STATE_NAME(g_eChipsHudState), " to: ", GET_CHIPS_HUD_STATE_NAME(eNewState))
		g_eChipsHudState = eNewState
	ENDIF
ENDPROC

/// PURPOSE:
///    State of the chips +/- change display
PROC SET_CASINO_CHIPS_CHANGE_HUD_STATE(CHIPS_HUD_STATE eNewState)
	IF eNewState != g_eChipsChangeHudState
		PRINTLN("[HUD_CHIPS][CHANGE] SET_CASINO_CHIPS_CHANGE_HUD_STATE - Change state from: ", GET_CHIPS_HUD_STATE_NAME(g_eChipsChangeHudState), " to: ", GET_CHIPS_HUD_STATE_NAME(eNewState))
		g_eChipsChangeHudState = eNewState
	ENDIF
ENDPROC

/// PURPOSE:
///    Sends the latest script chips total to scaleform
PROC _UPDATE_SF_MOVIE_HUD_CHIPS()
	IF HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_CHIP)
	 
        BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(HUD_CHIP,"SET_PLAYER_CHIPS")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_iLocalPlayerCasinoChips)
        END_SCALEFORM_MOVIE_METHOD()
		
		SET_BIT(g_iChipsHudBS, HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT)
		PRINTLN("[HUD_CHIPS] UPDATE_SF_MOVIE_HUD_CHIPS - Num chips = ", g_iLocalPlayerCasinoChips)
    ELSE
        REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_CHIP)
		PRINTLN("[HUD_CHIPS] UPDATE_SF_MOVIE_HUD_CHIPS - Request movie")
    ENDIF
ENDPROC

/// PURPOSE:
///    Sends the latest script chips =/- change to scaleform
PROC _UPDATE_SF_MOVIE_HUD_CHIPS_CHANGE(INT iChipsChange, BOOL bIncrement)
	IF HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_CHIP_CHANGE)
	 
        BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(HUD_CHIP_CHANGE, "SET_PLAYER_CHIP_CHANGE")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iChipsChange)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIncrement)
        END_SCALEFORM_MOVIE_METHOD()
		
		SET_BIT(g_iChipsHudBS, HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT_CHANGE)
		PRINTLN("[HUD_CHIPS][CHANGE] UPDATE_SF_MOVIE_HUD_CHIPS_CHANGE - Called iChipsChange = ", iChipsChange, " bIncrement = ", bIncrement)
    ELSE
        REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_CHIP_CHANGE)
		PRINTLN("[HUD_CHIPS][CHANGE] UPDATE_SF_MOVIE_HUD_CHIPS_CHANGE - Request movie")
    ENDIF
ENDPROC

/// PURPOSE:
///    To be used only when the HUD is requested otherwise it wont display
///    For times when using HIDE_HUD_AND_RADAR_THIS_FRAME but you need the 
///    chips UI to still be displayed.
PROC SHOW_CHIPS_HUD_THIS_FRAME()
	IF NOT IS_CASINO_CHIPS_HUD_HIDDEN_BY_SCRIPT()
		SHOW_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_CHIP)
		SHOW_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_CHIP_CHANGE)
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_TO_LOAD_AND_USE_CHIPS_HUD()
	IF NOT NETWORK_IS_SESSION_ACTIVE()
	OR NOT IS_SKYSWOOP_AT_GROUND()
	OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Manages the loading, unloading and display requests for the script HUD component HUD_CHIP
PROC UPDATE_CASINO_CHIPS_HUD_STATE()
	SWITCH g_eChipsHudState
		CASE CHS_UNLOADED
			IF IS_SAFE_TO_LOAD_AND_USE_CHIPS_HUD()
				IF HAS_CASINO_CHIPS_HUD_DISPLAY_BEEN_REQUESTED()
					PRINTLN("[HUD_CHIPS] - UPDATE_CASINO_CHIPS_HUD_STATE - Active in freemode. Request HUD_CHIP")
					REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_CHIP)
					SET_CASINO_CHIPS_HUD_STATE(CHS_LOADING)
					CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT)
				ENDIF
			ENDIF
		BREAK
		CASE CHS_LOADING
			IF HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_CHIP)
				PRINTLN("[HUD_CHIPS] - UPDATE_CASINO_CHIPS_HUD_STATE - HUD_CHIP Loaded.")
				SET_CASINO_CHIPS_HUD_STATE(CHS_LOADED)
			ENDIF
		BREAK
		CASE CHS_LOADED
			IF IS_SAFE_TO_DISLAY_CASINO_CHIPS_HUD()
			AND NOT IS_CASINO_CHIPS_HUD_HIDDEN_BY_SCRIPT()
			AND NOT IS_CASINO_CHIPS_HUD_UPDATE_PAUSED()
				//Wait until we can actually display the HUD so we get the fade in
				IF NOT IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT)
					_UPDATE_SF_MOVIE_HUD_CHIPS()
					CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_CHIPS_TOTAL_UPDATE)
				ELSE
					SET_CASINO_CHIPS_HUD_STATE(CHS_DRAWING)
				ENDIF
			ELSE
				IF IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT)
					SET_CASINO_CHIPS_HUD_STATE(CHS_DRAWING)
				ENDIF
				
				PRINTLN("[HUD_CHIPS] - HIDE HUD_CHIP")
				HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_CHIP)
			ENDIF
		BREAK
		CASE CHS_DRAWING
			//Use the the command hide script HUD component this frame to prevent this being displayed when we don't need it
			IF IS_CASINO_CHIPS_HUD_HIDDEN_BY_SCRIPT()
			OR NOT IS_SAFE_TO_DISLAY_CASINO_CHIPS_HUD()
				PRINTLN("[HUD_CHIPS] - HIDE HUD_CHIP")
				HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_CHIP)
			ELSE
				SHOW_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_CHIP)
			ENDIF
			
			IF HAS_CASINO_CHIPS_HUD_DISPLAY_BEEN_REQUESTED()
				IF _IS_CASINO_CHIPS_TOTAL_DISPLAY_PENDING_UPDATE()
				AND NOT IS_CASINO_CHIPS_HUD_UPDATE_PAUSED()
					_UPDATE_SF_MOVIE_HUD_CHIPS()
					CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_CHIPS_TOTAL_UPDATE)
				ENDIF
			ELSE
				SET_CASINO_CHIPS_HUD_STATE(CHS_CLEANUP)
			ENDIF
		BREAK
		CASE CHS_CLEANUP
			PRINTLN("[HUD_CHIPS] UPDATE_CASINO_CHIPS_HUD_STATE - Calling REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_CHIP)")
			REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_CHIP)
			SET_CASINO_CHIPS_HUD_STATE(CHS_UNLOADED)
			SET_CASINO_CHIPS_HUD_DISPLAY_AS_NO_LONGER_NEEDED()
			CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Manages the loading, unloading and display requests for the script HUD component HUD_CHIP_CHANGE
PROC UPDATE_CASINO_CHIPS_CHANGE_HUD_STATE()
	SWITCH g_eChipsChangeHudState
		CASE CHS_UNLOADED
			IF IS_SAFE_TO_LOAD_AND_USE_CHIPS_HUD()
				IF IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_CHIP_CHANGE_DISPLAY)
					PRINTLN("[HUD_CHIPS] - UPDATE_CASINO_CHIPS_CHANGE_HUD_STATE - Active in freemode. Request HUD_CHIP_CHANGE")
					REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_CHIP_CHANGE)
					SET_CASINO_CHIPS_CHANGE_HUD_STATE(CHS_LOADING)
					CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT_CHANGE)
				ENDIF
			ENDIF
		BREAK
		CASE CHS_LOADING
			IF HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_CHIP_CHANGE)
				PRINTLN("[HUD_CHIPS] - UPDATE_CASINO_CHIPS_CHANGE_HUD_STATE - HUD_CHIP_CHANGE Loaded.")
				SET_CASINO_CHIPS_CHANGE_HUD_STATE(CHS_LOADED)
			ENDIF
		BREAK
		CASE CHS_LOADED
			IF IS_SAFE_TO_DISLAY_CASINO_CHIPS_HUD()
			AND NOT IS_CASINO_CHIPS_HUD_HIDDEN_BY_SCRIPT()
			AND NOT IS_CASINO_CHIPS_HUD_UPDATE_PAUSED()
				//Wait until we can actually display the HUD so we get the fade in
				IF NOT IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT_CHANGE)
					_UPDATE_SF_MOVIE_HUD_CHIPS_CHANGE(g_iChipsHUDChangeAmount, g_bChipsHUDChangeIncrement)
					CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_CHIPS_CHANGE_UPDATE)
				ELSE
					REINIT_NET_TIMER(g_stChipChangeHUDDisplay)
					SET_CASINO_CHIPS_CHANGE_HUD_STATE(CHS_DRAWING)
				ENDIF
			ELSE
				IF IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT_CHANGE)
					REINIT_NET_TIMER(g_stChipChangeHUDDisplay)
					SET_CASINO_CHIPS_CHANGE_HUD_STATE(CHS_DRAWING)
				ENDIF
				
				PRINTLN("[HUD_CHIPS] - HIDE HUD_CHIP_CHANGE")
				HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_CHIP_CHANGE)
			ENDIF
		BREAK
		CASE CHS_DRAWING
			//Use the the command hide script HUD component this frame to prevent this being displayed when we don't need it
			IF IS_CASINO_CHIPS_HUD_HIDDEN_BY_SCRIPT()
			OR NOT IS_SAFE_TO_DISLAY_CASINO_CHIPS_HUD()
				PRINTLN("[HUD_CHIPS] - HIDE HUD_CHIP_CHANGE")
				HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_CHIP_CHANGE)
			ELSE
				SHOW_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_CHIP_CHANGE)
			ENDIF
			
			IF IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_CHIP_CHANGE_DISPLAY)
				IF _IS_CASINO_CHIPS_CHANGE_DISPLAY_PENDING_UPDATE()
				AND NOT IS_CASINO_CHIPS_HUD_UPDATE_PAUSED()
					PRINTLN("[HUD_CHIPS] UPDATE_CASINO_CHIPS_CHANGE_HUD_STATE - Updating display before time expired")
					REINIT_NET_TIMER(g_stChipChangeHUDDisplay)
					_UPDATE_SF_MOVIE_HUD_CHIPS_CHANGE(g_iChipsHUDChangeAmount, g_bChipsHUDChangeIncrement)
					CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_CHIPS_CHANGE_UPDATE)
				ENDIF
			ELSE
				SET_CASINO_CHIPS_CHANGE_HUD_STATE(CHS_CLEANUP)
			ENDIF
		BREAK
		CASE CHS_CLEANUP
			PRINTLN("[HUD_CHIPS] UPDATE_CASINO_CHIPS_CHANGE_HUD_STATE - Calling REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_CHIP_CHANGE)")
			REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_CHIP_CHANGE)
			SET_CASINO_CHIPS_CHANGE_HUD_STATE(CHS_UNLOADED)
			RESET_NET_TIMER(g_stChipChangeHUDDisplay)
			CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_SETUP_DISPLAY_AMOUNT_CHANGE)
			CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_CHIP_CHANGE_DISPLAY)
		BREAK
	ENDSWITCH
ENDPROC

PROC MANAGE_CASINO_CHIPS_HUD()
	
	IF IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_CLEANUP)
	OR NOT IS_SAFE_TO_LOAD_AND_USE_CHIPS_HUD()
		IF g_eChipsHudState != CHS_UNLOADED
			SET_CASINO_CHIPS_HUD_STATE(CHS_CLEANUP)
		ENDIF
		IF g_eChipsChangeHudState != CHS_UNLOADED
			SET_CASINO_CHIPS_CHANGE_HUD_STATE(CHS_CLEANUP)
		ENDIF
		CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_CLEANUP)
	ENDIF
	
	UPDATE_CASINO_CHIPS_HUD_STATE()
	UPDATE_CASINO_CHIPS_CHANGE_HUD_STATE()
	
	//The chip change HUD should only be active for a limited time
	IF IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_CHIP_CHANGE_DISPLAY)
		IF HAS_NET_TIMER_STARTED(g_stChipChangeHUDDisplay)
		AND HAS_NET_TIMER_EXPIRED(g_stChipChangeHUDDisplay, 5000)
			PRINTLN("[HUD_CHIPS][CHANGE] MANAGE_CASINO_CHIPS_HUD - 5 second display timer expired")
			SET_CASINO_CHIPS_CHANGE_HUD_STATE(CHS_CLEANUP)
			RESET_NET_TIMER(g_stChipChangeHUDDisplay)
		ELIF IS_SCREEN_FADED_OUT()
			PRINTLN("[HUD_CHIPS][CHANGE] HUD_CHIPS_BS_REQUEST_CHIP_CHANGE_DISPLAY - Screen faded out so reinit timer to display after")
			REINIT_NET_TIMER(g_stChipChangeHUDDisplay)
		ENDIF
	ENDIF
	
	CLEAR_BIT(g_iChipsHudBS, HUD_CHIPS_BS_HIDE_CHIPS_HUD_THIS_FRAME)
ENDPROC

PROC INIT_CASINO_CHIPS_HUD_DATA()
	PRINTLN("[HUD_CHIPS] INIT_CASINO_CHIPS_HUD_DATA")
	g_iChipsHudBS = 0
	g_eChipsHudState = CHS_UNLOADED
	g_eChipsChangeHudState = CHS_UNLOADED
	RESET_NET_TIMER(g_stChipChangeHUDDisplay)
ENDPROC

/// PURPOSE:
///    Returns true if the reason given for updating house chips is valid for an increasing in house chip count
FUNC BOOL IS_CASINO_CHIPS_UPDATE_REASON_AN_EARN(CASINO_CHIPS_UPDATE eReason)
	IF eReason = CCU_HOUSE_SELL
	OR eReason = CCU_HOUSE_BET
	OR eReason = CCU_PURCHASE_SHOP_ITEM
	#IF IS_DEBUG_BUILD
	OR eReason = CCU_DEBUG_REMOVE
	#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_COLLECT_DAILY_FREE_CHIPS()
	IF g_iPOSIXTimeLastCollectedChips = -1
		RETURN TRUE
	ENDIF
	
	RETURN ((GET_CLOUD_TIME_AS_INT() - g_iPOSIXTimeLastCollectedChips) > g_sMPTunables.iVC_CASINO_CHIP_FREE_COOLDOWN)
ENDFUNC

PROC SET_PLAYER_COLLECTED_DAILY_HOUSE_CHIPS()
	g_iPOSIXTimeLastCollectedChips = GET_CLOUD_TIME_AS_INT()
	SET_MP_INT_PLAYER_STAT(MPPLY_CHIPS_COL_TIME, g_iPOSIXTimeLastCollectedChips)
	PRINTLN("[CASINO_CHIPS] SET_PLAYER_COLLECTED_DAILY_HOUSE_CHIPS - Time set to ", g_iPOSIXTimeLastCollectedChips)
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════╡ House chips purchase tracking ╞════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Returns the number of chips a player is allowed to purchase in a singe game day
FUNC INT GET_NUM_CHIPS_PURCHASABLE_PER_GAMEDAY()
	IF DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
		RETURN g_sMPTunables.iVC_CASINO_CHIP_MAX_BUY_PENTHOUSE
	ENDIF
	
	RETURN g_sMPTunables.iVC_CASINO_CHIP_MAX_BUY
ENDFUNC

/// PURPOSE:
///    Returns the number of chips the local player has purchased in the last game day
FUNC INT GET_NUM_CHIPS_LP_HAS_BOUGHT_THIS_GAME_DAY()
	RETURN GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_PUR_GD)
ENDFUNC

/// PURPOSE:
///    Returns the time at which we started tracking casino chips purchasing (game day tracking not lifetime)
FUNC INT GET_CASINO_CHIPS_PURCHASE_TIME()
	RETURN GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_PURTIM)
ENDFUNC

FUNC INT GET_NUMBER_OF_CHIPS_PLAYER_CAN_AFFORD()
	
	INT iMaxPurchaseableChips = GET_NUM_CHIPS_PURCHASABLE_PER_GAMEDAY()
	INT iNumChips
	
	IF iMaxPurchaseableChips <= 0
		RETURN 0
	ENDIF
	
	IF NETWORK_CASINO_CAN_BUY_CHIPS_PVC()
		IF NETWORK_CAN_SPEND_MONEY(iMaxPurchaseableChips, FALSE, TRUE, FALSE)
			iNumChips = iMaxPurchaseableChips
		ELSE
			iNumChips = (NETWORK_GET_VC_BANK_BALANCE() + NETWORK_GET_VC_WALLET_BALANCE())
		ENDIF
	ELSE
		IF NETWORK_CAN_SPEND_MONEY(iMaxPurchaseableChips, FALSE, TRUE, FALSE, -1, TRUE)
			iNumChips = iMaxPurchaseableChips
		ELSE
			iNumChips = NETWORK_GET_EVC_BALANCE()
		ENDIF
	ENDIF
	
	RETURN iNumChips
ENDFUNC

FUNC BOOL CAN_AFFORD_CASINO_CHIPS(INT iAmount)
	RETURN (GET_NUMBER_OF_CHIPS_PLAYER_CAN_AFFORD() >= iAmount)
ENDFUNC

/// PURPOSE:
///    Returns the remaining chips a player can buy based on GET_NUM_CHIPS_PURCHASABLE_PER_GAMEDAY()
FUNC INT GET_NUM_CHIPS_LP_CAN_BUY_THIS_GAME_DAY()
	INT iGameDayLimit 	= (GET_NUM_CHIPS_PURCHASABLE_PER_GAMEDAY() - GET_NUM_CHIPS_LP_HAS_BOUGHT_THIS_GAME_DAY())
	INT iNumChipsToMax 	= (g_sMPTunables.iVC_CASINO_CHIP_MAX_HELD - GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL())
	INT iMaxAffordable	= GET_NUMBER_OF_CHIPS_PLAYER_CAN_AFFORD()
	
	IF iGameDayLimit > iNumChipsToMax
		iGameDayLimit = iNumChipsToMax
	ENDIF
	
	IF iGameDayLimit > iMaxAffordable
		iGameDayLimit = iMaxAffordable
	ENDIF
	
	RETURN iGameDayLimit
ENDFUNC

/// PURPOSE:
///    Returns true if the player can buy any more house chips
FUNC BOOL CAN_LOCAL_PLAYER_PURCHASE_CASINO_CHIPS()
	RETURN GET_NUM_CHIPS_LP_CAN_BUY_THIS_GAME_DAY() = 0
ENDFUNC

/// PURPOSE:
///    Sets the current posix time as a stat so we can track the number of chips purchased this past gameday
PROC START_HOUSE_CHIPS_PURCHASE_TIMER_FOR_LOCAL_PLAYER()
	PRINTLN("[CASINO_CHIPS] START_HOUSE_CHIPS_PURCHASE_TIMER_FOR_LOCAL_PLAYER - Set timer")
	SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_PURTIM, GET_CLOUD_TIME_AS_INT())
ENDPROC

PROC RESTART_HOUSE_CHIPS_PURCHASE_TIMER_FOR_LOCAL_PLAYER()
	PRINTLN("[CASINO_CHIPS] RESTART_HOUSE_CHIPS_PURCHASE_TIMER_FOR_LOCAL_PLAYER - Set timer")
	START_HOUSE_CHIPS_PURCHASE_TIMER_FOR_LOCAL_PLAYER()
ENDPROC

/// PURPOSE:
///    Tracking function to limit the player to GET_NUM_CHIPS_PURCHASABLE_PER_GAMEDAY() in given gameday
PROC ADD_TO_CASINO_CHIPS_PURCHASED(INT iNumChipsToAdd)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableGamblingRestrictions")
		PRINTLN("[CASINO_CHIPS] ADD_TO_CASINO_CHIPS_PURCHASED Ignoring command for sc_DisableGamblingRestrictions")
		EXIT
	ENDIF
	#ENDIF
	
	INT iChipsPurchaseTime = GET_CASINO_CHIPS_PURCHASE_TIME()
	INT iCurrentTime = GET_CLOUD_TIME_AS_INT()
	INT iPreviousTotal = GET_NUM_CHIPS_LP_HAS_BOUGHT_THIS_GAME_DAY()
	
	IF iChipsPurchaseTime = 0
		START_HOUSE_CHIPS_PURCHASE_TIMER_FOR_LOCAL_PLAYER()
	ELIF (iChipsPurchaseTime - iCurrentTime) >= g_sMPTunables.iVC_CASINO_CHIP_MAX_BUY_COOLDOWN
		//Should never reach heare as the maintain function should take care of this, but adding as a precaution
		RESTART_HOUSE_CHIPS_PURCHASE_TIMER_FOR_LOCAL_PLAYER()
		iPreviousTotal = 0
	ELSE
		PRINTLN("[CASINO_CHIPS] ADD_TO_CASINO_CHIPS_PURCHASED - Time expired since timer started (seconds): ", (iCurrentTime - iChipsPurchaseTime))
	ENDIF
	
	IF (iNumChipsToAdd + iPreviousTotal) > GET_NUM_CHIPS_PURCHASABLE_PER_GAMEDAY()
		PRINTLN("[CASINO_CHIPS] ADD_TO_CASINO_CHIPS_PURCHASED - We are now at the threshold for max number of chips purchasable in a gameday!")
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_PUR_GD, GET_NUM_CHIPS_PURCHASABLE_PER_GAMEDAY())
		RESTART_HOUSE_CHIPS_PURCHASE_TIMER_FOR_LOCAL_PLAYER()
//		PRINTLN("[CASINO_CHIPS][CASINO_SAVE] ADD_TO_CASINO_CHIPS_PURCHASED - Requesting minor save")
//		g_b_CasinoSave = TRUE
	ELSE
		PRINTLN("[CASINO_CHIPS] ADD_TO_CASINO_CHIPS_PURCHASED - Adding ", iNumChipsToAdd, " chips")
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_PUR_GD, (iPreviousTotal + iNumChipsToAdd))
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡ Betting losses tracking ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Returns the number of chips the local player has lost in the last game day
FUNC INT GET_CHIPS_WIN_LOSS_TOTAL_GD_FOR_LOCAL_PLAYER()
	RETURN GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WON_GD)
ENDFUNC

/// PURPOSE:
///    Returns the time at which we started tracking player losses via betting against the house (Gameday tracking)
FUNC INT GET_TRACKING_TIME_FOR_CHIPS_WIN_LOSS_TO_CASINO()
	RETURN GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WONTIM)
ENDFUNC

/// PURPOSE:
///    Returns the remaining chips a player can lose through betting based on g_sMPTunables.iVC_CASINO_CHIP_MAX_LOSS_DAILY
FUNC INT GET_NUM_CHIPS_LP_CAN_LOSE_THIS_GAME_DAY()
	RETURN (g_sMPTunables.iVC_CASINO_CHIP_MAX_LOSS_DAILY + GET_CHIPS_WIN_LOSS_TOTAL_GD_FOR_LOCAL_PLAYER())
ENDFUNC

/// PURPOSE:
///    Returns true if the player can lose any more house chips in the current gameday
FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_TO_LOSE_ANY_MORE_BETS_THIS_GAMEDAY()
	RETURN GET_NUM_CHIPS_LP_CAN_LOSE_THIS_GAME_DAY() > 0
ENDFUNC

/// PURPOSE:
///    Sets the current posix time as a stat so we can track the number of chips won this past gameday
PROC START_HOUSE_CHIPS_WON_LOSS_TIMER_FOR_LOCAL_PLAYER()
	PRINTLN("[CASINO_CHIPS] START_HOUSE_CHIPS_WON_LOSS_TIMER_FOR_LOCAL_PLAYER - Set timer")
	SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WONTIM, GET_CLOUD_TIME_AS_INT())
ENDPROC

PROC RESTART_HOUSE_CHIPS_WON_LOSS_TIMER_FOR_LOCAL_PLAYER()
	PRINTLN("[CASINO_CHIPS] RESTART_HOUSE_CHIPS_WON_LOSS_TIMER_FOR_LOCAL_PLAYER - Set timer")
	START_HOUSE_CHIPS_WON_LOSS_TIMER_FOR_LOCAL_PLAYER()
ENDPROC
/// PURPOSE:
///    Tracking function to limit the player to losing g_sMPTunables.iVC_CASINO_CHIP_MAX_LOSS_DAILY in given gameday
PROC ADD_TO_CASINO_CHIPS_LOST_ON_BETTING_THIS_GAMEDAY(INT iNumChipsToAdd)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableGamblingRestrictions")
		PRINTLN("[CASINO_CHIPS] ADD_TO_CASINO_CHIPS_LOST_ON_BETTING_THIS_GAMEDAY Ignoring command for sc_DisableGamblingRestrictions")
		EXIT
	ENDIF
	#ENDIF
	
	INT iChipsLossTime = GET_TRACKING_TIME_FOR_CHIPS_WIN_LOSS_TO_CASINO()
	INT iCurrentTime = GET_CLOUD_TIME_AS_INT()
	INT iPreviousTotal = GET_CHIPS_WIN_LOSS_TOTAL_GD_FOR_LOCAL_PLAYER()
	
	IF iChipsLossTime = 0
		START_HOUSE_CHIPS_WON_LOSS_TIMER_FOR_LOCAL_PLAYER()
	ELIF (iChipsLossTime - iCurrentTime) >= g_sMPTunables.iVC_CASINO_CHIP_MAX_WIN_LOSS_COOLDOWN
		//Should never reach heare as the maintain function should take care of this, but adding as a precaution
		RESTART_HOUSE_CHIPS_WON_LOSS_TIMER_FOR_LOCAL_PLAYER()
		iPreviousTotal = 0
	ELSE
		PRINTLN("[CASINO_CHIPS] ADD_TO_CASINO_CHIPS_LOST_ON_BETTING_THIS_GAMEDAY - Time expired since timer started (seconds): ", (iCurrentTime - iChipsLossTime))
	ENDIF
	
	IF (iPreviousTotal - iNumChipsToAdd) >= g_sMPTunables.iVC_CASINO_CHIP_MAX_LOSS_DAILY
		PRINTLN("ADD_TO_CASINO_CHIPS_LOST_ON_BETTING_THIS_GAMEDAY - Threshold reached. We are now at tha maximum allowable loss in this gameday")
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WON_GD, g_sMPTunables.iVC_CASINO_CHIP_MAX_LOSS_DAILY)
		RESTART_HOUSE_CHIPS_WON_LOSS_TIMER_FOR_LOCAL_PLAYER()
		PRINTLN("[CASINO_CHIPS][CASINO_SAVE] ADD_TO_CASINO_CHIPS_LOST_ON_BETTING_THIS_GAMEDAY - Requesting minor save")
		g_b_CasinoSave = TRUE
	ELSE
		PRINTLN("[CASINO_CHIPS] ADD_TO_CASINO_CHIPS_LOST_ON_BETTING_THIS_GAMEDAY - Adding ", iNumChipsToAdd, " chips")
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WON_GD, (iPreviousTotal - iNumChipsToAdd))
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ Betting win tracking ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Returns the remaining chips a player can win through betting based on g_sMPTunables.iVC_CASINO_CHIP_MAX_WIN_DAILY
FUNC INT GET_NUM_CHIPS_LP_CAN_WIN_THIS_GAME_DAY()
	RETURN (g_sMPTunables.iVC_CASINO_CHIP_MAX_WIN_DAILY - GET_CHIPS_WIN_LOSS_TOTAL_GD_FOR_LOCAL_PLAYER())
ENDFUNC

/// PURPOSE:
///    Returns true if the player can win any more house chips in the current gameday
FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_TO_WIN_ANY_MORE_BETS_THIS_GAMEDAY()
	RETURN GET_NUM_CHIPS_LP_CAN_WIN_THIS_GAME_DAY() > 0
ENDFUNC

/// PURPOSE:
///    Tracking function to limit the player to g_sMPTunables.iVC_CASINO_CHIP_MAX_WIN_DAILY in given gameday
PROC ADD_TO_CASINO_CHIPS_WON_ON_BETTING_THIS_GAMEDAY(INT iNumChipsToAdd)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableGamblingRestrictions")
		PRINTLN("[CASINO_CHIPS] ADD_TO_CASINO_CHIPS_WON_ON_BETTING_THIS_GAMEDAY Ignoring command for sc_DisableGamblingRestrictions")
		EXIT
	ENDIF
	#ENDIF
	
	INT iChipsWonTime = GET_TRACKING_TIME_FOR_CHIPS_WIN_LOSS_TO_CASINO()
	INT iCurrentTime = GET_CLOUD_TIME_AS_INT()
	INT iPreviousTotal = GET_CHIPS_WIN_LOSS_TOTAL_GD_FOR_LOCAL_PLAYER()
	
	IF iChipsWonTime = 0
		START_HOUSE_CHIPS_WON_LOSS_TIMER_FOR_LOCAL_PLAYER()
	ELIF (iChipsWonTime - iCurrentTime) >= g_sMPTunables.iVC_CASINO_CHIP_MAX_WIN_LOSS_COOLDOWN
		//Should never reach heare as the maintain function should take care of this, but adding as a precaution
		RESTART_HOUSE_CHIPS_WON_LOSS_TIMER_FOR_LOCAL_PLAYER()
		iPreviousTotal = 0
	ELSE
		PRINTLN("[CASINO_CHIPS] ADD_TO_CASINO_CHIPS_WON_ON_BETTING_THIS_GAMEDAY - Time expired since timer started (seconds): ", (iCurrentTime - iChipsWonTime))
	ENDIF
	
	IF (iNumChipsToAdd + iPreviousTotal) >= g_sMPTunables.iVC_CASINO_CHIP_MAX_WIN_DAILY
		PRINTLN("[CASINO_CHIPS] ADD_TO_CASINO_CHIPS_WON_ON_BETTING_THIS_GAMEDAY - Threshold reached. We are now at tha maximum allowable wins in this gameday")
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WON_GD, g_sMPTunables.iVC_CASINO_CHIP_MAX_WIN_DAILY)
		RESTART_HOUSE_CHIPS_WON_LOSS_TIMER_FOR_LOCAL_PLAYER()
		PRINTLN("[CASINO_CHIPS][CASINO_SAVE] ADD_TO_CASINO_CHIPS_WON_ON_BETTING_THIS_GAMEDAY - Requesting minor save")
		g_b_CasinoSave = TRUE
	ELSE
		PRINTLN("[CASINO_CHIPS] ADD_TO_CASINO_CHIPS_WON_ON_BETTING_THIS_GAMEDAY - Adding ", iNumChipsToAdd, " chips")
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WON_GD, (iPreviousTotal + iNumChipsToAdd))
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ transactions ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC INT GET_CCU_UPDATE_REASON_CATALOGUE_HASH(CASINO_CHIPS_UPDATE eUpdateReason)
	INT iReturn = 0
	
	SWITCH eUpdateReason
		CASE CCU_HOUSE_PURCHASE		iReturn = HASH("CCUR_BUY")					BREAK
		CASE CCU_HOUSE_SELL			iReturn = HASH("CCUR_SELL")					BREAK
		CASE CCU_HOUSE_BET			iReturn = HASH("CCUR_BET")					BREAK
		CASE CCU_HOUSE_PAYOUT		iReturn = HASH("CCUR_PAYOUT")				BREAK
		CASE CCU_COLLECTABLES		iReturn = HASH("CCUR_COLLECTIBLE_REWARD")	BREAK
		CASE CCU_MEBERSHIP_BONUS	iReturn = HASH("CCUR_MEMBERSHIP_PURCHASE")	BREAK
		CASE CCU_MISSION_REWARD		iReturn = HASH("CCUR_MISSION_REWARD")		BREAK
		CASE CCU_BIG_MISSION_REWARD	iReturn = HASH("CCUR_MISSION_REWARD")		BREAK
		CASE CCU_DAILY_AWARD		iReturn = HASH("CCUR_DAILY_BONUS")			BREAK
		CASE CCU_BET_RETURN			iReturn = HASH("CCUR_PAYOUT")				BREAK
		CASE CCU_PURCHASE_BONUS		iReturn = HASH("CCUR_VEH_PURCHASE_REWARD")	BREAK
		CASE CCU_PURCHASE_SHOP_ITEM	iReturn = HASH("CCU_PURCHASE_SHOP_ITEM")	BREAK
		#IF IS_DEBUG_BUILD
		CASE CCU_DEBUG_ADD			iReturn = HASH("CCUR_BUY")					BREAK
		CASE CCU_DEBUG_REMOVE		iReturn = HASH("CCUR_SELL")					BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN iReturn
ENDFUNC

FUNC BOOL SHOULD_CASINO_CHIPS_UPDATE_REASON_INVOLVE_CASH(CASINO_CHIPS_UPDATE eUpdateReason)
	SWITCH eUpdateReason
		CASE CCU_HOUSE_PURCHASE
		CASE CCU_HOUSE_SELL
		CASE CCU_PURCHASE_SHOP_ITEM
		#IF IS_DEBUG_BUILD
		CASE CCU_DEBUG_ADD
		CASE CCU_DEBUG_REMOVE
		#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_CASINO_CHIPS_TRANSACTION(GENERIC_TRANSACTION_STATE &eTransactionResult, CASINO_CHIPS_UPDATE eUpdateReason, INT iNumChips)
	
	/// SellCasinoChipsTransaction is used to Add casino chips to the player's inventory
    ///     Expected basket as follows
    ///         0: CATEGORY_CASINO_CHIPS item, price: 1 or 0 (depending on reason), value: quantity of chips 
    ///         1: CATEGORY_CASINO_CHIP_REASON item to identify the reason for the transaction, price: 0, quantity: 0
	 	
	/// BuyCasinoChipsTransaction is used to Add casino chips to the player's inventory
    ///     Expected basket as follows
    ///         0: CATEGORY_CASINO_CHIPS item, price: 1 or 0 (depending on reason), value: quantity of chips 
    ///         1: CATEGORY_CASINO_CHIP_REASON item to identify the reason for the transaction, price: 0, quantity: 0
	
	INT iCost = iNumChips
	INT iValue = 1
	BOOL bTransactionInvolvesCash = SHOULD_CASINO_CHIPS_UPDATE_REASON_INVOLVE_CASH(eUpdateReason)
	
	IF NOT bTransactionInvolvesCash
		iCost = 0
		iValue = 0
	ENDIF
	
	IF eTransactionResult = TRANSACTION_STATE_FAILED
	OR eTransactionResult = TRANSACTION_STATE_SUCCESS
		eTransactionResult = TRANSACTION_STATE_DEFAULT
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		IF eTransactionResult = TRANSACTION_STATE_DEFAULT
			
			IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
				PRINTLN("PROCESS_CASINO_ADD_CHIPS_TRANSACTION - Hold up for transaction in progress")
				RETURN FALSE
			ENDIF
			
			INT iInventoryItem	= HASH("CASINO_CHIPS_v0")
			INT iReasonHash 	= GET_CCU_UPDATE_REASON_CATALOGUE_HASH(eUpdateReason)
			ITEM_ACTION_TYPES eShopAction = NET_SHOP_ACTION_SELL_CASINO_CHIPS
			
			IF IS_CASINO_CHIPS_UPDATE_REASON_AN_EARN(eUpdateReason)
				PRINTLN("PROCESS_CASINO_CHIPS_TRANSACTION - Update reason = BUY_CASINO_CHIPS")
				eShopAction = NET_SHOP_ACTION_BUY_CASINO_CHIPS
			ELSE
				PRINTLN("PROCESS_CASINO_CHIPS_TRANSACTION - Update reason = SELL_CASINO_CHIPS")
			ENDIF
			
			//Add the inventory item
			IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CASINO_CHIPS, iInventoryItem, eShopAction, iNumChips, iValue, 0, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
				
				PRINTLN("PROCESS_CASINO_ADD_CHIPS_TRANSACTION - Added chip inventory item to basket")
				
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CASINO_CHIP_REASON, iReasonHash, eShopAction, 1, 0, 0, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
					
					PRINTLN("PROCESS_CASINO_ADD_CHIPS_TRANSACTION - Added reason item to basket")
					
					IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
			           PRINTLN("PROCESS_CASINO_ADD_CHIPS_TRANSACTION - basket checkout started")			
						eTransactionResult = TRANSACTION_STATE_PENDING
			        ELSE
			           PRINTLN("PROCESS_CASINO_ADD_CHIPS_TRANSACTION - failed to start basket checkout")
			            eTransactionResult = TRANSACTION_STATE_FAILED
			        ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "PROCESS_CASINO_ADD_CHIPS_TRANSACTION - failed to add item to basket")
		        	eTransactionResult = TRANSACTION_STATE_FAILED
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "PROCESS_CASINO_ADD_CHIPS_TRANSACTION - failed to add item to basket")
		        eTransactionResult = TRANSACTION_STATE_FAILED
			ENDIF
		ELIF eTransactionResult = TRANSACTION_STATE_PENDING
			IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
		        IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
		           PRINTLN("PROCESS_CASINO_ADD_CHIPS_TRANSACTION - Basket transaction finished, success!")
					NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
					IF IS_CASINO_CHIPS_UPDATE_REASON_AN_EARN(eUpdateReason)
						NETWORK_CASINO_BUY_CHIPS(iCost, iNumChips)
					ELSE
						NETWORK_CASINO_SELL_CHIPS(iCost, iNumChips)
					ENDIF
		            eTransactionResult = TRANSACTION_STATE_SUCCESS
		        ELSE
		           PRINTLN("PROCESS_CASINO_ADD_CHIPS_TRANSACTION - Basket transaction finished, failed!")
		            eTransactionResult = TRANSACTION_STATE_FAILED
		        ENDIF
				
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
		    ENDIF
		ENDIF
	ELSE
		IF iCost > 0
		OR NETWORK_CAN_SPEND_MONEY(iCost, FALSE, TRUE, FALSE)
			IF IS_CASINO_CHIPS_UPDATE_REASON_AN_EARN(eUpdateReason)
				PRINTLN("PROCESS_CASINO_ADD_CHIPS_TRANSACTION - Call NETWORK_CASINO_BUY_CHIPS(",iCost, ", ", iNumChips, ")")
				NETWORK_CASINO_BUY_CHIPS(iCost, iNumChips)
			ELSE
				PRINTLN("PROCESS_CASINO_ADD_CHIPS_TRANSACTION - Call NETWORK_CASINO_SELL_CHIPS(",iCost, ", ", iNumChips, ")")
				NETWORK_CASINO_SELL_CHIPS(iCost, iNumChips)
			ENDIF
			
			eTransactionResult = TRANSACTION_STATE_SUCCESS
		ELSE
			eTransactionResult = TRANSACTION_STATE_FAILED
		ENDIF
	ENDIF
	
	RETURN eTransactionResult != TRANSACTION_STATE_PENDING
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡ General helper functions ╞══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC INT GET_CASINO_CHIPS_TRANSACTION_POINT(CASINO_CHIPS_UPDATE eUpdateReason, CASINO_GAMES eGame, INT iExtraInt = -1)
	INT iID
		
	IF eGame = CG_INVALID
		SWITCH eUpdateReason	
			CASE CCU_HOUSE_PURCHASE
				IF iExtraInt > -1
					iID = (19 + iExtraInt)
				ELSE
					iID = 19
				ENDIF
			BREAK
			CASE CCU_HOUSE_SELL					iID = 21	BREAK
			CASE CCU_COLLECTABLES				iID = 22	BREAK
			CASE CCU_MEBERSHIP_BONUS			iID = 23	BREAK
			CASE CCU_MISSION_REWARD				iID = 24	BREAK
			CASE CCU_DAILY_AWARD				iID = 25	BREAK
			CASE CCU_PURCHASE_BONUS				iID = 26	BREAK
			CASE CCU_PURCHASE_SHOP_ITEM
				IF iExtraInt > -1
					iID = (27 + iExtraInt)
				ELSE
					iID = 27
				ENDIF
			BREAK
			CASE CCU_BIG_MISSION_REWARD			iID = 29	BREAK
		ENDSWITCH
	ELSE
		SWITCH eGame
			CASE CG_SLOTS
				IF eUpdateReason = CCU_HOUSE_BET
					iID = 1
				ELIF eUpdateReason = CCU_HOUSE_PAYOUT
					iID = 2
				ELIF eUpdateReason = CCU_BET_RETURN
					iID = 3
				ENDIF
			BREAK
			CASE CG_BLACKJACK
				IF eUpdateReason = CCU_HOUSE_BET
					iID = 4
				ELIF eUpdateReason = CCU_HOUSE_PAYOUT
					iID = 5
				ELIF eUpdateReason = CCU_BET_RETURN
					iID = 6
				ENDIF
			BREAK
			CASE CG_3CARDPOKER
				IF eUpdateReason = CCU_HOUSE_BET
					iID = 7
				ELIF eUpdateReason = CCU_HOUSE_PAYOUT
					iID = 8
				ELIF eUpdateReason = CCU_BET_RETURN
					iID = 9
				ENDIF
			BREAK
			CASE CG_ROULETTE
				IF eUpdateReason = CCU_HOUSE_BET
					iID = 10
				ELIF eUpdateReason = CCU_HOUSE_PAYOUT
					iID = 11
				ELIF eUpdateReason = CCU_BET_RETURN
					iID = 12
				ENDIF
			BREAK
			CASE CG_BIGWHEEL
				IF eUpdateReason = CCU_HOUSE_BET
					iID = 13
				ELIF eUpdateReason = CCU_HOUSE_PAYOUT
					iID = 14
				ELIF eUpdateReason = CCU_BET_RETURN
					iID = 15
				ENDIF
			BREAK
			CASE CG_INSIDE_TRACK
				IF eUpdateReason = CCU_HOUSE_BET
					iID = 16
				ELIF eUpdateReason = CCU_HOUSE_PAYOUT
					iID = 17
				ELIF eUpdateReason = CCU_BET_RETURN
					iID = 18
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
		
	RETURN iID
ENDFUNC

DEBUGONLY PROC PRINT_CASINO_CHIP_PLAYSTATS(STRUCT_CASINOCHIPMETRIC data)
	PRINTLN("PRINT_CASINO_CHIP_PLAYSTATS - m_action: ", data.m_action,
			" m_transactionID: ", data.m_transactionID,
			" m_cashAmount", data.m_cashAmount,
			" m_chipsAmount", data.m_chipsAmount,
			" m_reason", data.m_reason,
			" m_cashBalance", data.m_cashBalance,
			" m_chipBalance", data.m_chipBalance,
			" m_item", data.m_item)
ENDPROC

/// PURPOSE:
///    Adds the given number of chips to the local players stat total
///    PARAMS:
///    	iExtraInt - For telemetry if a chip update reason has multiple purchase points
FUNC BOOL ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS(INT iChipsToAdd, CASINO_CHIPS_UPDATE eUpdateReason, GENERIC_TRANSACTION_STATE &eTransactionResult, CASINO_GAMES eGame = CG_INVALID, INT iExtraInt = -1)
	
	PRINTLN("[CASINO_CHIPS] ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS - Attempting to add ", iChipsToAdd, " chips. Reason: ", GET_DEBUG_STRING_CHIPS_TOTAL_UPDATE_REASON(eUpdateReason))
	
	IF iChipsToAdd <= 0
		SCRIPT_ASSERT("[CASINO_CHIPS] ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS - Failed to add chips! Invalid number of chips to add.")
	ELIF NOT IS_CASINO_CHIPS_UPDATE_REASON_AN_EARN(eUpdateReason)
	#IF IS_DEBUG_BUILD
	AND eUpdateReason != CCU_DEBUG_ADD
	#ENDIF
		SCRIPT_ASSERT("[CASINO_CHIPS] ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS - Failed to add chips! Invalid update reason.")
	ELSE
		INT iCurrentChips 	= GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL()
		INT iChipsToLimit 	= (g_sMPTunables.iVC_CASINO_CHIP_MAX_HELD - iCurrentChips)
		INT iNewTotal 		= 0
		
		IF eUpdateReason = CCU_HOUSE_PURCHASE
			INT iPurchaseLimit = GET_NUM_CHIPS_LP_CAN_BUY_THIS_GAME_DAY()
			
			IF iChipsToLimit > iPurchaseLimit
				iChipsToLimit = iPurchaseLimit
			ENDIF
		ENDIF
		
		PRINTLN("[CASINO_CHIPS] ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS - Additional chips limited to ", iChipsToLimit)
		
		IF iChipsToAdd > iChipsToLimit
		#IF IS_DEBUG_BUILD
		AND eUpdateReason != CCU_DEBUG_ADD
		AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_DisableGamblingRestrictions")
		#ENDIF
			iNewTotal = iChipsToLimit + iCurrentChips
		ELSE
			iNewTotal = iChipsToAdd + iCurrentChips
		ENDIF
		
		IF PROCESS_CASINO_CHIPS_TRANSACTION(eTransactionResult, eUpdateReason, (iNewTotal - iCurrentChips))
			
			INT iChipsAdded = (GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL() - g_iLocalPlayerCasinoChips)
			
			REQUEST_CASINO_CHIPS_CHANGE_HUD_DISPLAY(iChipsAdded, TRUE)
			_REQUEST_CHIPS_HUD_TOTAL_UPDATE()
			_REQUEST_CHIPS_CHANGE_HUD_UPDATE()
			g_iLocalPlayerCasinoChips = (g_iLocalPlayerCasinoChips + iChipsAdded)
			
			//Update gambling trackers
			IF eUpdateReason = CCU_HOUSE_PURCHASE
			
				ADD_TO_CASINO_CHIPS_PURCHASED(iChipsAdded)
				PRINTLN("[CASINO_CHIPS][CASINO_SAVE] ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS - Requesting minor save")
				g_b_CasinoSave = TRUE
				
			ELIF eUpdateReason = CCU_HOUSE_PAYOUT
				ADD_TO_CASINO_CHIPS_WON_ON_BETTING_THIS_GAMEDAY(iChipsAdded)
				
				IF iChipsAdded >= g_sMPTunables.iVC_CASINO_SAVE_MAJOR_WIN_THRESHOLD
					PRINTLN("[CASINO_CHIPS][CASINO_SAVE] ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS - Requesting major save")
					g_b_CasinoPrioritySave = TRUE
				ELIF iChipsAdded >= g_sMPTunables.iVC_CASINO_SAVE_BIG_WIN_THRESHOLD
					PRINTLN("[CASINO_CHIPS][CASINO_SAVE] ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS - Requesting minor save.")
					g_b_CasinoSave = TRUE
				ENDIF
			ENDIF
			
			STRUCT_CASINOCHIPMETRIC data
			data.m_action 			= GET_CCU_UPDATE_REASON_CATALOGUE_HASH(eUpdateReason)
		    data.m_transactionID	= GET_CASINO_CHIPS_TRANSACTION_POINT(eUpdateReason, eGame, iExtraInt)
			IF SHOULD_CASINO_CHIPS_UPDATE_REASON_INVOLVE_CASH(eUpdateReason)
		    	data.m_cashAmount	= iChipsAdded
			ELSE
				data.m_cashAmount	= 0
			ENDIF
		    data.m_chipsAmount 		= iChipsAdded
		    data.m_reason 			= ENUM_TO_INT(eUpdateReason)
		    data.m_cashBalance		= (NETWORK_GET_VC_WALLET_BALANCE() + NETWORK_GET_VC_BANK_BALANCE())
		    data.m_chipBalance		= g_iLocalPlayerCasinoChips
			
			PRINT_CASINO_CHIP_PLAYSTATS(data)
			PLAYSTATS_CASINO_CHIP(data) 
			PRINTLN("[CASINO_CHIPS] ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS - Player chips total updated to ", iNewTotal)
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CASINO_PAYOUT_REWARD_TO_LOCAL_PLAYER(INT iPayout, GENERIC_TRANSACTION_STATE &eTransactionResult, CASINO_GAMES eGame)
	RETURN ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS(iPayout, CCU_HOUSE_PAYOUT, eTransactionResult, eGame)
ENDFUNC

FUNC BOOL CASINO_PAYOUT_HOUSE_CHIPS_MISSION_REWARD(INT iNumChips, GENERIC_TRANSACTION_STATE &eTransactionResult)
	RETURN ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS(iNumChips, CCU_MISSION_REWARD, eTransactionResult)
ENDFUNC

/// PURPOSE:
///    Same as CASINO_PAYOUT_HOUSE_CHIPS_MISSION_REWARD but allows for a bigger payout of up to 25000 chips
FUNC BOOL CASINO_PAYOUT_HOUSE_CHIPS_LARGE_MISSION_REWARD(INT iNumChips, GENERIC_TRANSACTION_STATE &eTransactionResult)
	RETURN ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS(iNumChips, CCU_BIG_MISSION_REWARD, eTransactionResult)
ENDFUNC

FUNC BOOL CASINO_PAYOUT_HOUSE_CHIPS_DAILY_COLLECTION(GENERIC_TRANSACTION_STATE &eTransactionResult)
	IF CAN_PLAYER_COLLECT_DAILY_FREE_CHIPS()
		IF ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS(GET_NUMBER_OF_CHIPS_FOR_VISITOR_BONUS(), CCU_DAILY_AWARD, eTransactionResult)
			SET_PLAYER_COLLECTED_DAILY_HOUSE_CHIPS()
			RETURN TRUE
		ELSE
			//Transaction pending...
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Return chips used to place a bet to the player
FUNC BOOL RETURN_BET_TO_PLAYER(INT iNumChips, GENERIC_TRANSACTION_STATE &eTransactionResult)
	RETURN ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS(iNumChips, CCU_BET_RETURN, eTransactionResult)
ENDFUNC

/// PURPOSE:
///    Give the player a special reward for purchasing select items online 
FUNC BOOL GIVE_PLAYER_CHIPS_BONUS_FOR_PURCHASE(INT iNumChips, GENERIC_TRANSACTION_STATE &eTransactionResult)
	RETURN ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS(iNumChips, CCU_PURCHASE_BONUS, eTransactionResult)
ENDFUNC

/// PURPOSE:
///    Removes the given number of chips to the local players stat total
///    PARAMS:
///    	iExtraInt - For telemetry if a chip update reason has multiple purchase points
FUNC BOOL REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER(INT iChipsToRemove, CASINO_CHIPS_UPDATE eUpdateReason, GENERIC_TRANSACTION_STATE &eTransactionResult, CASINO_GAMES eGame = CG_INVALID, INT iExtraInt = -1, INT iExtraIntTwo = -1)
	PRINTLN("[CASINO_CHIPS] REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER - Attempting to remove ", iChipsToRemove, " chips. Reason: ", GET_DEBUG_STRING_CHIPS_TOTAL_UPDATE_REASON(eUpdateReason))
	
	IF iChipsToRemove <= 0
		SCRIPT_ASSERT("[CASINO_CHIPS] REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER - Failed to remove chips! Invalid number of chips to remove.")
	ELIF IS_CASINO_CHIPS_UPDATE_REASON_AN_EARN(eUpdateReason)
	#IF IS_DEBUG_BUILD
	AND eUpdateReason != CCU_DEBUG_REMOVE
	#ENDIF
		SCRIPT_ASSERT("[CASINO_CHIPS] REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER - Failed to remove chips! Invalid update reason.")
	ELSE
		INT iCurrentChips 	= GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL()
		INT iNewTotal 		= iCurrentChips - iChipsToRemove
		
		IF iNewTotal < 0
		AND (NOT USE_SERVER_TRANSACTIONS() OR eTransactionResult != TRANSACTION_STATE_PENDING)
			SCRIPT_ASSERT("REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER - Attepmpting to remove more chips than the player has available!")
			iNewTotal 		= iCurrentChips
			iChipsToRemove 	= iCurrentChips
		ENDIF
		
		IF PROCESS_CASINO_CHIPS_TRANSACTION(eTransactionResult, eUpdateReason, iChipsToRemove)
			
			INT iChipsRemoved = (g_iLocalPlayerCasinoChips - GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL())
			PRINTLN("[CASINO_CHIPS] REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER Removing chips - ", iChipsRemoved, " = ", g_iLocalPlayerCasinoChips, " - ", GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL())
			
			IF eUpdateReason = CCU_HOUSE_SELL
				PRINTLN("[CASINO_CHIPS][CASINO_SAVE] REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER - Requesting Major save for selling chips")
				g_b_CasinoPrioritySave = TRUE
			ELIF iChipsRemoved >= g_sMPTunables.iVC_CASINO_SAVE_LOSS_THRESHOLD
				PRINTLN("[CASINO_CHIPS][CASINO_SAVE] REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER - Requesting minor save")
				g_b_CasinoSave = TRUE
			ENDIF
			
			REQUEST_CASINO_CHIPS_CHANGE_HUD_DISPLAY(iChipsRemoved, FALSE)
			_REQUEST_CHIPS_HUD_TOTAL_UPDATE()
			_REQUEST_CHIPS_CHANGE_HUD_UPDATE()
			g_iLocalPlayerCasinoChips = (g_iLocalPlayerCasinoChips - iChipsRemoved)
			
			STRUCT_CASINOCHIPMETRIC data
			data.m_action 			= GET_CCU_UPDATE_REASON_CATALOGUE_HASH(eUpdateReason)
		    data.m_transactionID	= GET_CASINO_CHIPS_TRANSACTION_POINT(eUpdateReason, eGame, iExtraInt)
			IF SHOULD_CASINO_CHIPS_UPDATE_REASON_INVOLVE_CASH(eUpdateReason)
		    	data.m_cashAmount	= iChipsRemoved
			ELSE
				data.m_cashAmount	= 0
			ENDIF
		    data.m_chipsAmount 		= iChipsRemoved
		    data.m_reason 			= ENUM_TO_INT(eUpdateReason)
		    data.m_cashBalance		= (NETWORK_GET_VC_WALLET_BALANCE() + NETWORK_GET_VC_BANK_BALANCE())
		    data.m_chipBalance		= g_iLocalPlayerCasinoChips
			data.m_item				= iExtraIntTwo
			
			PRINT_CASINO_CHIP_PLAYSTATS(data)			
			PLAYSTATS_CASINO_CHIP(data) 
			PRINTLN("[CASINO_CHIPS] REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER - Player chips total updated to ", iNewTotal, " iChipsToRemove = ", iChipsToRemove)
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER_FOR_SHOP_ITEM(INT iItemCost, GENERIC_TRANSACTION_STATE &eTransactionResult, INT iPurchasePoint = 0, INT iExtraIntTwo = -1)
	RETURN REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER(iItemCost, CCU_PURCHASE_SHOP_ITEM, eTransactionResult, CG_INVALID, iPurchasePoint, iExtraIntTwo)
ENDFUNC

FUNC BOOL IS_LP_BANNED_FROM_BETTING_DUE_TO_TIME_SPENT_GAMBLING()
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableGamblingRestrictions")
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN g_iGamblingBanTime != 0
ENDFUNC

FUNC CASINO_BAN_REASON GET_CASINO_GAME_BAN_REASON()	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableGamblingRestrictions")
		RETURN CBR_NOT_BANNED
	ENDIF
	#ENDIF
		
	IF GET_NUM_CHIPS_LP_CAN_WIN_THIS_GAME_DAY() <= 0
		PRINTLN("[CASINO_CHIPS] GET_CASINO_GAME_BAN_REASON - TRUE:  Chips won today: ", GET_CHIPS_WIN_LOSS_TOTAL_GD_FOR_LOCAL_PLAYER(), " > ", g_sMPTunables.iVC_CASINO_CHIP_MAX_WIN_DAILY)
		RETURN CBR_WON_TOO_MUCH
	ENDIF
	
	IF GET_NUM_CHIPS_LP_CAN_LOSE_THIS_GAME_DAY() <= 0
		PRINTLN("[CASINO_CHIPS] GET_CASINO_GAME_BAN_REASON - TRUE:  Chips lost today: ", (GET_CHIPS_WIN_LOSS_TOTAL_GD_FOR_LOCAL_PLAYER() * -1), " > ", g_sMPTunables.iVC_CASINO_CHIP_MAX_LOSS_DAILY)
		RETURN CBR_LOST_TOO_MUCH
	ENDIF
	
	IF IS_LP_BANNED_FROM_BETTING_DUE_TO_TIME_SPENT_GAMBLING()
		PRINTLN("[CASINO_CHIPS] GET_CASINO_GAME_BAN_REASON - TRUE: g_iGamblingBanTime != 0")
		RETURN CBR_TIME_SPENT_PLAYING
	ENDIF
	
	RETURN CBR_NOT_BANNED
ENDFUNC

/// PURPOSE:
///    Returns true if the local player has exceded the limits on wins or losses in the current gameday
FUNC BOOL IS_LOCAL_PLAYER_BANNED_FROM_BETTING()
	RETURN (GET_CASINO_GAME_BAN_REASON() != CBR_NOT_BANNED)
ENDFUNC

/// PURPOSE:
///    Returns true if the player can place a bet of the given amount
///    Checks betting win/loss limits and chip count
FUNC BOOL CAN_LOCAL_PLAYER_PLACE_A_BET(INT iBetAmount)
	IF IS_LOCAL_PLAYER_BANNED_FROM_BETTING()
		RETURN FALSE
	ENDIF	
	
	INT iNumHouseChips = GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL()
	
	IF iNumHouseChips < iBetAmount
		PRINTLN("[CASINO_CHIPS] CAN_LOCAL_PLAYER_PLACE_A_BET - FALSE:  iNumHouseChips < iBetAmount: ", iNumHouseChips, " < ", iBetAmount)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Removes the given number of chips from the player
/// PARAMS:
///    iBet - The amount to take from the player
///    bResult - Returns if the game was able to remove chils fromt the player
///    bCheckIfCanBet - Should this function check if the player is able to place a bet of the given amount? If not this should be checked by whatever script is calling this
/// RETURNS:
///    TRUE when the transaction is complete
FUNC BOOL LOCAL_PLAYER_PLACE_CASINO_BET(INT iBet, GENERIC_TRANSACTION_STATE &eTransactionResult, CASINO_GAMES eGame, BOOL bCheckIfCanBet = FALSE)
	IF USE_SERVER_TRANSACTIONS() 
	AND eTransactionResult = TRANSACTION_STATE_PENDING
		bCheckIfCanBet = FALSE
	ENDIF
	IF NOT bCheckIfCanBet
	OR CAN_LOCAL_PLAYER_PLACE_A_BET(iBet)
		RETURN REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER(iBet, CCU_HOUSE_BET, eTransactionResult, eGame)
	ELSE
		eTransactionResult = TRANSACTION_STATE_FAILED
	ENDIF
	
	RETURN TRUE
ENDFUNC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// Time spent gambling tracking //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CONST_INT CASINO_GAMBLING_HISTORY_NUM_HOURS		24
CONST_INT CASINO_GAMBLING_HISTORY_NUM_DAYS		28
CONST_INT CASINO_GAMBLING_TIME_SAVE_INTERVAL	60		//Interval in seconds
CONST_INT GTAO_PLAY_TIME_SAVE_INTERVAL			60		//Interval in seconds
CONST_INT MINUTES_PER_DAY 						1440
CONST_INT SECONDS_PER_HOUR						3600
CONST_INT SECONDS_PER_DAY 						86400
CONST_INT SEVEN_DAYS_IN_MINUTES					10080
CONST_INT SEVEN_DAYS_IN_SECONDS					604800
CONST_INT TEN_HOURS_IN_SECONDS					36000	//The number of seconds in 10 hours
CONST_INT GAMBLE_HISTORY_DAYS_IN_SECONDS		((CASINO_GAMBLING_HISTORY_NUM_DAYS * 24) * 60 * 60)
CONST_INT BITS_TO_REPRESENT_MINUTES_PER_DAY		11
CONST_INT STATS_TO_REPRESENT_28_DAY_TRACKING	10		//CEIL((11 * 28)/32)
CONST_INT BITS_TO_REPRESENT_SECONDS_PER_HOUR	12
CONST_INT STATS_TO_REPRESENT_24_HOUR_TRACKING	9		//(11 * 24)/32
CONST_INT CASINO_GAMBLING_BAN_CHECK_THRESHOLD	2400	//Threshold number of minutes for time gambling at which point we check total playtime over the last up to 28 days

FUNC MPPLY_INT_STATS GET_CASINO_TIME_GAMBLING_PER_DAY_STAT(INT iIndex)
	TEXT_LABEL_23 tlStatName = "MPPLY_CAS_TIME_GMBLNG_"
	tlStatName += iIndex
	
	RETURN INT_TO_ENUM(MPPLY_INT_STATS, GET_HASH_KEY(tlStatName))
ENDFUNC

FUNC MPPLY_INT_STATS GET_CASINO_TIME_PLAYING_PER_DAY_STAT(INT iIndex)
	TEXT_LABEL_23 tlStatName = "MPPLY_CAS_TIME_PLYING_"
	tlStatName += iIndex
	
	RETURN INT_TO_ENUM(MPPLY_INT_STATS, GET_HASH_KEY(tlStatName))
ENDFUNC

FUNC MPPLY_INT_STATS GET_CASINO_TIME_GAMBLING_PER_HOUR_STAT(INT iIndex)
	TEXT_LABEL_23 tlStatName = "MPPLY_CAS_GMBLNG_L24"
	
	//Added like this due to the change in requirements of how we track the last 24 hours
	IF iIndex > 0
		tlStatName += "_"
		tlStatName += iIndex
	ENDIF
	
	RETURN INT_TO_ENUM(MPPLY_INT_STATS, GET_HASH_KEY(tlStatName))
ENDFUNC

/// PURPOSE:
///    Similar to SET_PACKED_BITFIELD_VALUE_IN_ARRAY, but this function allows for bit widths not wholly divisible by 32
/// PARAMS:
///    iBitFieldArr - The array of integers
///    iElementNum - The element to set a value for
///    iDataBitWidth - the number of bits required
///    iValue - The value to set
PROC SET_PACKED_BITFIELD_VALUE_IN_ARRAY_ELEMENTS( INT &iBitFieldArr[], INT iElementNum, INT iDataBitWidth, INT iValue)
	INT i, j
	INT iElementEnd		= ((iElementNum + 1) * iDataBitWidth)
	INT iElementID		= (iElementEnd - iDataBitWidth)
	INT iArrayID 		= FLOOR(TO_FLOAT(iElementID) / 32.0)
	//Is this item stored in more than one array element
	INT iNumElements	= CEIL(TO_FLOAT(((iElementID % 32) + iDataBitWidth) - 32) / 32) + 1
	
	IF ((iArrayID - 1) + iNumElements) >= COUNT_OF(iBitFieldArr)
		PRINTLN("[GAMBLING_TIME] SET_PACKED_BITFIELD_VALUE_IN_ARRAY_ELEMENTS - iElementNum: ", iElementNum, " iDataBitWidth: ", iDataBitWidth, " iArrayID: ", iArrayID, " iNumElements: ", iNumElements, " array size: ", COUNT_OF(iBitFieldArr))
		SCRIPT_ASSERT( "SET_PACKED_BITFIELD_VALUE_IN_ARRAY_ELEMENTS - iBitFieldArr[] is not large enough to have an element of that bit width stored in it.")
		EXIT
	ENDIF
	
	INT iBitsUsed = 0
	INT iStartBit = (iElementID % 32)
	
	REPEAT iNumElements i
		INT iElementBitWidth = (32 - iStartBit)
		
		IF iElementBitWidth > (iDataBitWidth - iBitsUsed)
			iElementBitWidth = (iDataBitWidth - iBitsUsed)
		ENDIF
		
		FOR j = iStartBit TO ((iStartBit + iElementBitWidth) - 1)
			IF IS_BIT_SET(iValue, iBitsUsed)
				SET_BIT(iBitFieldArr[iArrayID + i], j)
			ELSE
				CLEAR_BIT(iBitFieldArr[iArrayID + i], j)
			ENDIF
			
			iBitsUsed ++
		ENDFOR
		
		IF iStartBit > 0
			iStartBit = 0
		ELSE
			iStartBit += iElementBitWidth
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Similar to GET_PACKED_BITFIELD_VALUE_IN_ARRAY, but this function allows for bit widths not wholly divisible by 32
FUNC INT GET_PACKED_BITFIELD_VALUE_FROM_ARRAY_ELEMENTS(INT &iBitFieldArr[], INT iElementNum, INT iDataBitWidth)
	
	INT i, j, iReturn
	INT iElementEnd		= ((iElementNum + 1) * iDataBitWidth)
	INT iElementID		= (iElementEnd - iDataBitWidth)
	INT iArrayID 		= FLOOR(TO_FLOAT(iElementID) / 32.0)
	//Is this item stored in more than one array element
	INT iNumElements	= CEIL(TO_FLOAT(((iElementID % 32) + iDataBitWidth) - 32) / 32) + 1
	
	IF ((iArrayID - 1) + iNumElements) >= COUNT_OF(iBitFieldArr)
		PRINTLN("[GAMBLING_TIME] GET_PACKED_BITFIELD_VALUE_FROM_ARRAY_ELEMENTS - iElementNum: ", iElementNum, " iDataBitWidth: ", iDataBitWidth, " iArrayID: ", iArrayID, " iNumElements: ", iNumElements, " array size: ", COUNT_OF(iBitFieldArr))
		SCRIPT_ASSERT( "GET_PACKED_BITFIELD_VALUE_FROM_ARRAY_ELEMENTS - iBitFieldArr[] is not large enough to have an element of that bit width stored in it.")
		RETURN 0
	ENDIF
	
	INT iBitsUsed = 0
	INT iStartBit = (iElementID % 32)
	
	REPEAT iNumElements i
		INT iElementBitWidth = (32 - iStartBit)
		
		IF iElementBitWidth > (iDataBitWidth - iBitsUsed)
			iElementBitWidth = (iDataBitWidth - iBitsUsed)
		ENDIF
		
		FOR j = iStartBit TO ((iStartBit + iElementBitWidth) - 1)
			IF IS_BIT_SET(iBitFieldArr[iArrayID + i], j)
				SET_BIT(iReturn, iBitsUsed)
			ENDIF
			
			iBitsUsed ++
		ENDFOR
		
		IF iStartBit > 0
			iStartBit = 0
		ELSE
			iStartBit += iElementBitWidth
		ENDIF
	ENDREPEAT
	
	RETURN iReturn
ENDFUNC

/// PURPOSE:
///    Sets the the posix time on which we last updated casino gambling stats
PROC SET_GAMBLING_TRACKING_UPDATED_TIME()
	PRINTLN("[GAMBLING_TIME] SET_GAMBLING_TRACKING_UPDATED_TIME - Time updated")
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_CUR_GMBLNG_PX, GET_CLOUD_TIME_AS_INT())
ENDPROC

PROC SET_CASINO_GAMBLING_EOD_POSIX(INT iTimetoSet)
	g_iGamblingEndOfDayPOSIX = iTimetoSet
	PRINTLN("[GAMBLING_TIME] SET_CASINO_GAMBLING_EOD_POSIX - New time: ", iTimetoSet)
ENDPROC

PROC START_GAMBLING_TIME_TRACKING()
	PRINTLN("[GAMBLING_TIME] START_GAMBLING_TIME_TRACKING - Time started")
	SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_GMBLNG_GD, GET_CLOUD_TIME_AS_INT())
	SET_GAMBLING_TRACKING_UPDATED_TIME()
	SET_CASINO_GAMBLING_EOD_POSIX(GET_CLOUD_TIME_AS_INT() + SECONDS_PER_DAY)
	g_bCasinoGamblingTrackingStarted = TRUE
ENDPROC

PROC RESET_GAMBLING_TIME_TRACKING()
	PRINTLN("[GAMBLING_TIME] RESET_GAMBLING_TIME_TRACKING - Time reset")
	SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_GMBLNG_GD, 0)
ENDPROC

FUNC BOOL HAS_GAMBLING_TIME_TRACKING_STARTED()
	RETURN (GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_GMBLNG_GD) != 0)
ENDFUNC

FUNC INT GET_TIME_LP_STARTED_GAMLING()
	RETURN GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_GMBLNG_GD)
ENDFUNC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    Hourly tracking - 24 hour moving track
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC INT GAMBLING_TIME_24HR_GET_CURRENT_TRACK_HOUR()
	RETURN GET_MP_INT_PLAYER_STAT(MPPLY_CAS_CUR_GMBLNG_HR)
ENDFUNC

/// PURPOSE:
///    Sets the the posix time on which we last updated casino gambling stats
PROC SET_24HR_GAMBLING_TRACKING_UPDATED_TIME()
	PRINTLN("[GAMBLING_TIME] SET_24HR_GAMBLING_TRACKING_UPDATED_TIME - Time updated")
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_24H_GMBLNG_PX, GET_CLOUD_TIME_AS_INT())
ENDPROC

/// PURPOSE:
///    Returns the time gambling in a given hour in the 24 hour tracking cycle
/// PARAMS:
///    iHour - The hour we want to check time for
FUNC INT GAMBLING_TIME_24HR_GET_TIME_SPENT_FOR_HOUR(INT iHour)
	INT i
	INT iStatsArr[STATS_TO_REPRESENT_24_HOUR_TRACKING]
	
	INT iStart 				= (((iHour + 1) * BITS_TO_REPRESENT_SECONDS_PER_HOUR) - BITS_TO_REPRESENT_SECONDS_PER_HOUR)
	INT iNumStatsToRepValue	= 1 
	
	IF ((iStart % 32) + BITS_TO_REPRESENT_SECONDS_PER_HOUR) > 32
		//The max number of stats a given hour could need to set e.g. at 12 bits per hour the 3rd hour will be represented in stat 1 and 2
		iNumStatsToRepValue = 2
	ENDIF
	
	INT iArrayIndex	= FLOOR(TO_FLOAT(iStart) / 32.0)
	
	IF iArrayIndex < 0
		PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_24HR_GET_TIME_SPENT_FOR_HOUR - Array size is negative")
		SCRIPT_ASSERT("[GAMBLING_TIME] GAMBLING_TIME_24HR_GET_TIME_SPENT_FOR_HOUR - Array size is negative")
		RETURN 0
	ENDIF
	
	//Get the current stat value(s)
	REPEAT iNumStatsToRepValue i
		IF (iArrayIndex + i) >= STATS_TO_REPRESENT_24_HOUR_TRACKING
			PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_24HR_GET_TIME_SPENT_FOR_HOUR - Array size not large enough! Required size >= ", (iArrayIndex + i))
			SCRIPT_ASSERT("GAMBLING_TIME_24HR_GET_TIME_SPENT_FOR_HOUR - Array size not large enough!")
			RETURN 0
		ENDIF
		
		MPPLY_INT_STATS	eStat = GET_CASINO_TIME_GAMBLING_PER_HOUR_STAT(iArrayIndex + i)		
		iStatsArr[iArrayIndex + i] = GET_MP_INT_PLAYER_STAT(eStat)
	ENDREPEAT
	
	//Get the values from the bits that represent the day we're interested in
	RETURN GET_PACKED_BITFIELD_VALUE_FROM_ARRAY_ELEMENTS(iStatsArr, iHour, BITS_TO_REPRESENT_SECONDS_PER_HOUR)
ENDFUNC

/// PURPOSE:
///    Adds to the time gambling for a given hour in the 24 hour tracking cycle
/// PARAMS:
///    iHour - The hour we want to check time for
///    iSecondsToAdd - The time to add
PROC GAMBLING_TIME_24HR_ADD_TO_TIME_PLAYING_THIS_HOUR(INT iHour, INT iSecondsToAdd)
	INT i
	INT iStatsArr[STATS_TO_REPRESENT_24_HOUR_TRACKING]
	
	INT iStart 				= (((iHour + 1) * BITS_TO_REPRESENT_SECONDS_PER_HOUR) - BITS_TO_REPRESENT_SECONDS_PER_HOUR)
	INT iNumStatsToRepValue	= 1 
	
	IF ((iStart % 32) + BITS_TO_REPRESENT_SECONDS_PER_HOUR) > 32
		//The max number of stats a given hour could need to set e.g. at 12 bits per hour the 3rd hour will be represented in stat 1 and 2
		iNumStatsToRepValue = 2
	ENDIF
	
	INT iArrayIndex	= FLOOR(TO_FLOAT(iStart) / 32.0)
	
	IF iArrayIndex < 0
		PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_24HR_ADD_TO_TIME_PLAYING_THIS_HOUR - Array size is negative")
		SCRIPT_ASSERT("[GAMBLING_TIME] GAMBLING_TIME_24HR_ADD_TO_TIME_PLAYING_THIS_HOUR - Array size is negative")
		EXIT
	ENDIF
	
	//Get the current stat value(s) to add to
	REPEAT iNumStatsToRepValue i
		IF (iArrayIndex + i) >= STATS_TO_REPRESENT_24_HOUR_TRACKING
			PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_24HR_ADD_TO_TIME_PLAYING_THIS_HOUR - Array size not large enough! Required size >= ", (iArrayIndex + i))
			SCRIPT_ASSERT("GAMBLING_TIME_24HR_ADD_TO_TIME_PLAYING_THIS_HOUR - Array size not large enough!")
			EXIT
		ENDIF
		
		MPPLY_INT_STATS	eStat = GET_CASINO_TIME_GAMBLING_PER_HOUR_STAT(iArrayIndex + i)		
		iStatsArr[iArrayIndex + i] = GET_MP_INT_PLAYER_STAT(eStat)
	ENDREPEAT
	
	//Get the values from the bits that represent the day we're interested in
	INT iCurrentValue = GET_PACKED_BITFIELD_VALUE_FROM_ARRAY_ELEMENTS(iStatsArr, iHour, BITS_TO_REPRESENT_SECONDS_PER_HOUR)	
	
	PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_24HR_ADD_TO_TIME_PLAYING_THIS_HOUR - Hour: ", iHour, " bit set start: ", iStart, " Current time: ", iCurrentValue, " adding: ", iSecondsToAdd, " num stats to represent this day = ", iNumStatsToRepValue)
	
	//Add the time
	iCurrentValue += iSecondsToAdd
	
	//Set the relevant bits
	SET_PACKED_BITFIELD_VALUE_IN_ARRAY_ELEMENTS(iStatsArr, iHour, BITS_TO_REPRESENT_SECONDS_PER_HOUR, iCurrentValue)
	
	//Set the new stat values
	REPEAT iNumStatsToRepValue i
		MPPLY_INT_STATS	eStat = GET_CASINO_TIME_GAMBLING_PER_HOUR_STAT(iArrayIndex + i)		
		SET_MP_INT_PLAYER_STAT(eStat, iStatsArr[iArrayIndex + i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Sets the time gambling for a given hour in the 24 hour tracking cycle
/// PARAMS:
///    iHour - The hour we want to check time for
///    iTimeToSet - The time to set (seconds)
PROC GAMBLING_TIME_24HR_SET_TIME_PLAYING_FOR_HOUR(INT iHour, INT iTimeToSet)
	INT i
	INT iStatsArr[STATS_TO_REPRESENT_24_HOUR_TRACKING]
	
	INT iStart 				= (((iHour + 1) * BITS_TO_REPRESENT_SECONDS_PER_HOUR) - BITS_TO_REPRESENT_SECONDS_PER_HOUR)
	INT iNumStatsToRepValue	= 1 
	
	IF ((iStart % 32) + BITS_TO_REPRESENT_SECONDS_PER_HOUR) > 32
		//The max number of stats a given hour could need to set e.g. at 12 bits per hour the 3rd hour will be represented in stat 1 and 2
		iNumStatsToRepValue = 2
	ENDIF
	
	INT iArrayIndex	= FLOOR(TO_FLOAT(iStart) / 32.0)
	
	IF iArrayIndex < 0
		PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_24HR_SET_TIME_PLAYING_FOR_HOUR - Array size is negative")
		SCRIPT_ASSERT("[GAMBLING_TIME] GAMBLING_TIME_24HR_SET_TIME_PLAYING_FOR_HOUR - Array size is negative")
		EXIT
	ENDIF
	
	//Get the current stat value(s) to add to
	REPEAT iNumStatsToRepValue i
		IF (iArrayIndex + i) >= STATS_TO_REPRESENT_24_HOUR_TRACKING
			PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_24HR_SET_TIME_PLAYING_FOR_HOUR - Array size not large enough! Required size >= ", (iArrayIndex + i))
			SCRIPT_ASSERT("GAMBLING_TIME_24HR_SET_TIME_PLAYING_FOR_HOUR - Array size not large enough!")
			EXIT
		ENDIF
		
		MPPLY_INT_STATS	eStat = GET_CASINO_TIME_GAMBLING_PER_HOUR_STAT(iArrayIndex + i)		
		iStatsArr[iArrayIndex + i] = GET_MP_INT_PLAYER_STAT(eStat)
	ENDREPEAT
	
	PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_24HR_SET_TIME_PLAYING_FOR_HOUR - Hour: ", iHour, " bit set start: ", iStart, " Setting to: ", iTimeToSet, " num stats to represent this day = ", iNumStatsToRepValue)
	
	//Set the relevant bits
	SET_PACKED_BITFIELD_VALUE_IN_ARRAY_ELEMENTS(iStatsArr, iHour, BITS_TO_REPRESENT_SECONDS_PER_HOUR, iTimeToSet)
	
	//Set the new stat values
	REPEAT iNumStatsToRepValue i
		MPPLY_INT_STATS	eStat = GET_CASINO_TIME_GAMBLING_PER_HOUR_STAT(iArrayIndex + i)
		
		SET_MP_INT_PLAYER_STAT(eStat, iStatsArr[iArrayIndex + i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    A seperate set of tracking stats the purpose of which is to increment a counter stat when x number of hours has been spent gambling in a 24 hour period of time
FUNC BOOL HAS_24HR_GAMBLING_TIME_TRACKING_STARTED()
	RETURN g_bCasinoGambling24HourTrackingStarted
ENDFUNC

FUNC INT GET_TIME_24HR_GAMBLING_TIME_TRACKING_STARTED()
	RETURN GET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_PX)
ENDFUNC

PROC RESET_CASINO_24_HOUR_TIME_TRACKING_STATS()
	PRINTLN("[GAMBLING_TIME] RESET_CASINO_24_HOUR_TIME_TRACKING_STATS - Called")
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_1, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_2, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_3, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_4, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_5, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_6, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_7, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_8, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_CUR_GMBLNG_HR, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_24H_GMBLNG_PX, 0)
ENDPROC

/// PURPOSE:
///    A seperate set of tracking stats the purpose of which is to increment a counter stat when x number of hours has been spent gambling in a 24 hour period of time
///    Timer start - POSIX
PROC START_24HR_GAMBLING_TIME_TRACKING()
	#IF IS_DEBUG_BUILD
		IF HAS_24HR_GAMBLING_TIME_TRACKING_STARTED()
			PRINTLN("[GAMBLING_TIME] START_24HR_GAMBLING_TIME_TRACKING - Time restarted")
		ELSE
			PRINTLN("[GAMBLING_TIME] START_24HR_GAMBLING_TIME_TRACKING - Time started")
		ENDIF
	#ENDIF
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_PX, GET_CLOUD_TIME_AS_INT())
	RESET_CASINO_24_HOUR_TIME_TRACKING_STATS()
	SET_24HR_GAMBLING_TRACKING_UPDATED_TIME()
	g_bCasinoGambling24HourTrackingStarted 	= TRUE
	g_iGamblingTimeInlastDay				= 0
ENDPROC

/// PURPOSE:
///    Adds to time tracking stats for the last 24 hours - seperate tracking to GAMBLING_TIME_ADD_TO_MINUTES_SPENT_TODAY
///    This time is a moving track
PROC GAMBLING_TIME_ADD_TO_TIME_SPENT_IN_THE_LAST_24HOURS(INT iSecondsToAdd)
	INT iCurrentHour = GAMBLING_TIME_24HR_GET_CURRENT_TRACK_HOUR()
	g_iGamblingTimeInlastDay += iSecondsToAdd
	GAMBLING_TIME_24HR_ADD_TO_TIME_PLAYING_THIS_HOUR(iCurrentHour, iSecondsToAdd)
	SET_24HR_GAMBLING_TRACKING_UPDATED_TIME()
ENDPROC

PROC STOP_24HR_GAMBLING_TIME_TRACKING()
	PRINTLN("[GAMBLING_TIME] START_24HR_GAMBLING_TIME_TRACKING - Time stopped")
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_PX, 0)
	RESET_CASINO_24_HOUR_TIME_TRACKING_STATS()
	
	g_bCasinoGambling24HourTrackingStarted 	= FALSE
	g_iGamblingTimeInlastDay 				= 0
ENDPROC

/// PURPOSE:
///    Enables gambling tracking to shift so we're always tracking the last 24 hours of gambling time
PROC MOVE_24HR_GAMBLING_TRACKING_FORWARD_ONE_HOUR()
	INT iCurrentStat = GET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_PX)	
	PRINTLN("[GAMBLING_TIME] MOVE_24HR_GAMBLING_TRACKING_FORWARD_ONE_HOUR - Time moved forward ", SECONDS_PER_HOUR, " seconds. New value = ", (iCurrentStat + SECONDS_PER_HOUR))
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_PX, iCurrentStat + SECONDS_PER_HOUR)
ENDPROC

/// PURPOSE:
///    Gets the current day we are tracking gambling for
FUNC INT GET_CURRENT_GAMBLING_24HR_TRACK_HOUR()
	RETURN GET_MP_INT_PLAYER_STAT(MPPLY_CAS_CUR_GMBLNG_HR)
ENDFUNC

/// PURPOSE:
///    Sets the current hour we are tracking gambling for
PROC SET_CURRENT_GAMBLING_24HR_TRACK_HOUR(INT iHour)
	PRINTLN("[GAMBLING_TIME] SET_CURRENT_GAMBLING_24HR_TRACK_HOUR - iHour: ", iHour)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_CUR_GMBLNG_HR, iHour)
ENDPROC

/// PURPOSE:
///    Gets the posix time on which we last updated casino gambling stats
FUNC INT GET_GAMBLING_24HR_TRACKING_UPDATED_TIME()
	RETURN GET_MP_INT_PLAYER_STAT(MPPLY_CAS_24H_GMBLNG_PX)
ENDFUNC

PROC INCREMENT_24_HOUR_GAMBLING_THRESHOLD_REACHED_STAT()
	INT iCurrentValue = GET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_TRHSLD)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_TRHSLD, (iCurrentValue + 1))
ENDPROC

/// PURPOSE:
///    Returns the real world day we are currently tracking gambling statistics for
FUNC INT GET_NUM_DAYS_SINCE_GAMBLING_TRACKING_BEGAN()
	INT iGamblingTrackStart = GET_TIME_LP_STARTED_GAMLING()
	INT iGamblingTimeDiff 	= (GET_CLOUD_TIME_AS_INT() - iGamblingTrackStart)
	
	IF iGamblingTrackStart = 0
		RETURN -1
	ENDIF
	
	RETURN FLOOR(TO_FLOAT(iGamblingTimeDiff)/TO_FLOAT(SECONDS_PER_DAY))
ENDFUNC

FUNC INT GET_NUM_DAYS_FROM_GMBL_TRACK_START_TO_TIME(INT iTime)
	INT iGamblingTrackStart = GET_TIME_LP_STARTED_GAMLING()
	INT iTimeDiff 	= (iTime - iGamblingTrackStart)
	
	IF iGamblingTrackStart = 0
		RETURN 0
	ENDIF
	
	RETURN FLOOR(TO_FLOAT(iTimeDiff)/TO_FLOAT(SECONDS_PER_DAY))
ENDFUNC

/// PURPOSE:
///    Gets the current day we are tracking gambling for
FUNC INT GET_CURRENT_GAMBLING_TRACK_DAY()
	RETURN GET_MP_INT_PLAYER_STAT(MPPLY_CAS_CUR_GMBLNG_GD)
ENDFUNC

/// PURPOSE:
///    Sets the current day we are tracking gambling for
PROC SET_CURRENT_GAMBLING_TRACK_DAY(INT iDay)
	PRINTLN("[GAMBLING_TIME] SET_CURRENT_GAMBLING_TRACK_DAY - Day: ", iDay)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_CUR_GMBLNG_GD, iDay)
ENDPROC

/// PURPOSE:
///    Gets the posix time on which we last updated casino gambling stats
FUNC INT GET_GAMBLING_TRACKING_UPDATED_TIME()
	RETURN GET_MP_INT_PLAYER_STAT(MPPLY_CAS_CUR_GMBLNG_PX)
ENDFUNC

/// PURPOSE:
///    Gets the number of days we have tracked time for in the current run
///    Includes days where no gambling has taken place or time skipped e.g. 3 days not playing GTAO
FUNC INT GET_NUM_GAMBLING_DAYS_TRACKED_IN_CURRENT_RUN()
	RETURN GET_PACKED_STAT_INT(CASINO_CONSECUTIVE_TIME_TRACKING_DAYS, 0)
ENDFUNC

/// PURPOSE:
///    Adds days the number of days we have tracked time for in the current run
///    Includes days where no gambling has taken place or time skipped e.g. 3 days not playing GTAO
PROC ADD_TO_NUM_GAMBLING_DAYS_TRACKED_IN_CURRENT_RUN(INT iDaysToAdd)
	INT iCurrentrun = GET_NUM_GAMBLING_DAYS_TRACKED_IN_CURRENT_RUN()
	INT iNewTotal = (iDaysToAdd + iCurrentrun)
	
	IF iNewTotal <= 255
		SET_PACKED_STAT_INT(CASINO_CONSECUTIVE_TIME_TRACKING_DAYS, iNewTotal, 0)
		PRINTLN("[GAMBLING_TIME] ADD_TO_NUM_GAMBLING_DAYS_TRACKED_IN_CURRENT_RUN - Incremented to ", iNewTotal)
	ELSE
		SET_PACKED_STAT_INT(CASINO_CONSECUTIVE_TIME_TRACKING_DAYS, 255, 0)
		PRINTLN("[GAMBLING_TIME] ADD_TO_NUM_GAMBLING_DAYS_TRACKED_IN_CURRENT_RUN - skip - New total > 255")
	ENDIF
ENDPROC

/// PURPOSE:
///    Enables gambling tracking to shift so we're always tracking the last 28 days of gambling time
PROC MOVE_GAMBLING_TRACKING_FORWARD_ONE_DAY()
	INT iCurrentStat = GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_GMBLNG_GD)	
	PRINTLN("[GAMBLING_TIME] MOVE_GAMBLING_TRACKING_FORWARD_ONE_DAY - Time moved forward ", SECONDS_PER_DAY, " seconds. New value = ", (iCurrentStat + SECONDS_PER_DAY))
	SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_GMBLNG_GD, iCurrentStat + SECONDS_PER_DAY)
	ADD_TO_NUM_GAMBLING_DAYS_TRACKED_IN_CURRENT_RUN(1)
ENDPROC

FUNC INT GET_TIME_IN_SECONDS_SINCE_GAMBLING_TRACKING_BEGAN()
	RETURN (GET_CLOUD_TIME_AS_INT() - GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_GMBLNG_GD))
ENDFUNC

/// PURPOSE:
///    Returns in seconds how far through the current day we are in terms of when we started tracking time spent gambling
FUNC INT GET_TIME_THROUGH_CURRENT_GAMBLING_TRACK_DAY()
	INT iTotalTimeElapsed 	= GET_TIME_IN_SECONDS_SINCE_GAMBLING_TRACKING_BEGAN()
	INT iNumDays 			= GET_NUM_DAYS_SINCE_GAMBLING_TRACKING_BEGAN()
	
	IF iNumDays > 0
		INT iDaysEleapsedInSeconds = (iNumDays * SECONDS_PER_DAY)
		
		RETURN (iTotalTimeElapsed - iDaysEleapsedInSeconds)
	ENDIF
	
	RETURN iTotalTimeElapsed
ENDFUNC

/// PURPOSE:
///    Start tracking the local players time gambling on a casino game
PROC SET_PLAYER_IS_PLAYING_CASINO_GAME(CASINO_GAMES eGame)
	
	#IF IS_DEBUG_BUILD
		IF g_eCasinoGameInUse != CG_INVALID
		AND g_eCasinoGameInUse != CG_DEBUG
			PRINTLN("SET_PLAYER_IS_PLAYING_CASINO_GAME - value already set to ", GET_CASINO_GAME_DEBUG_STRING(g_eCasinoGameInUse), " when trying to set: ", eGame)
			SCRIPT_ASSERT("SET_PLAYER_IS_PLAYING_CASINO_GAME - Setting when we are already playing another game!") 
		ENDIF
	#ENDIF
	
	g_iGamblingTime 	= GET_CLOUD_TIME_AS_INT()
	g_eCasinoGameInUse	= eGame
	
	PRINTLN("[GAMBLING_TIME] SET_PLAYER_IS_PLAYING_CASINO_GAME - Cloud time: ", g_iGamblingTime, " game: ",eGame)
	
	IF NOT HAS_GAMBLING_TIME_TRACKING_STARTED()
		//This is the point at which we start tracking player time gambing stats
		START_GAMBLING_TIME_TRACKING()
	ENDIF
	
	IF NOT HAS_24HR_GAMBLING_TIME_TRACKING_STARTED()
		//This is the point at which we start tracking the number of hours gambling in the last 24 hours
		START_24HR_GAMBLING_TIME_TRACKING()
	ENDIF
ENDPROC

/// PURPOSE:
///    Call to end the time tracking of the current casino game
PROC SET_PLAYER_FINISHED_PLAYING_CASINO_GAME()
	IF g_eCasinoGameInUse != CG_INVALID
		//Update 24 hour time tracking
		INT iTimeSpentGambling 	= (GET_CLOUD_TIME_AS_INT() - g_iGamblingTime)
		INT iTimeSinceLastSave	= (GET_CLOUD_TIME_AS_INT() - g_iGamblingTimeLastSave)
		
		IF g_iGamblingTimeLastSave = -1
			GAMBLING_TIME_ADD_TO_TIME_SPENT_IN_THE_LAST_24HOURS(iTimeSpentGambling)
		ELSE
			GAMBLING_TIME_ADD_TO_TIME_SPENT_IN_THE_LAST_24HOURS(iTimeSinceLastSave)
		ENDIF
		
		PRINTLN("[GAMBLING_TIME] SET_PLAYER_FINISHED_PLAYING_CASINO_GAME - Total gambling time: ", (GET_CLOUD_TIME_AS_INT() - g_iGamblingTime), " seconds")
		SET_GAMBLING_TRACKING_UPDATED_TIME()
		SET_24HR_GAMBLING_TRACKING_UPDATED_TIME()
		g_iGamblingTime			= -1
		g_iGamblingTimeLastSave = -1
		g_eCasinoGameInUse 		= CG_INVALID
	ELSE
		PRINTLN("[GAMBLING_TIME] SET_PLAYER_FINISHED_PLAYING_CASINO_GAME - Called whilst not in game")
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    Daily tracking - 28 day moving track
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Returns the time gambling on a given day in the 28 day tracking cycle
/// PARAMS:
///    iDay - The day we want to check time for
///    bTimeGambling - Time gambling or time not gambling
FUNC INT GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY(INT iDay, BOOL bTimeGambling)
	INT i
	INT iStatsArr[STATS_TO_REPRESENT_28_DAY_TRACKING]
	
	INT iStart 				= (((iDay + 1) * BITS_TO_REPRESENT_MINUTES_PER_DAY) - BITS_TO_REPRESENT_MINUTES_PER_DAY)
	INT iNumStatsToRepValue	= 1 
	
	IF ((iStart % 32) + BITS_TO_REPRESENT_MINUTES_PER_DAY) > 32
		//The max number of stats a given day could need to set e.g. at 11 bits per day the 3rd day will be represented in stat 1 and 2
		iNumStatsToRepValue = 2
	ENDIF
	
	INT iArrayIndex	= FLOOR(TO_FLOAT(iStart) / 32.0)
	
	IF iArrayIndex < 0
		PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY - Array size is negative")
		SCRIPT_ASSERT("[GAMBLING_TIME] GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY - Array size is negative")
		RETURN 0
	ENDIF
	
	//Get the current stat value(s)
	REPEAT iNumStatsToRepValue i
		IF (iArrayIndex + i) >= STATS_TO_REPRESENT_28_DAY_TRACKING
			PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY - Array size not large enough! Required size >= ", (iArrayIndex + i))
			SCRIPT_ASSERT("GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY - Array size not large enough!")
			RETURN 0
		ENDIF
		
		MPPLY_INT_STATS	eStat
		
		IF bTimeGambling
			eStat = GET_CASINO_TIME_GAMBLING_PER_DAY_STAT(iArrayIndex + i)
		ELSE
			eStat = GET_CASINO_TIME_PLAYING_PER_DAY_STAT(iArrayIndex + i)
		ENDIF
		
		iStatsArr[iArrayIndex + i] = GET_MP_INT_PLAYER_STAT(eStat)
	ENDREPEAT
	
	//Get the values from the bits that represent the day we're interested in
	RETURN GET_PACKED_BITFIELD_VALUE_FROM_ARRAY_ELEMENTS(iStatsArr, iDay, BITS_TO_REPRESENT_MINUTES_PER_DAY)
ENDFUNC

/// PURPOSE:
///    Sets the time gambling or playing GTAO on a given day in the 28 day tracking cycle
/// PARAMS:
///    iDay - The day we want to check time for
///    iTimeToSet - The time to set
///    bTimeGambling - Time gambling or time not gambling
PROC GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY(INT iDay, INT iTimeToSet, BOOL bTimeGambling)
	INT i
	INT iStatsArr[STATS_TO_REPRESENT_28_DAY_TRACKING]
	
	INT iStart 				= (((iDay + 1) * BITS_TO_REPRESENT_MINUTES_PER_DAY) - BITS_TO_REPRESENT_MINUTES_PER_DAY)
	INT iNumStatsToRepValue	= 1 
	
	IF ((iStart % 32) + BITS_TO_REPRESENT_MINUTES_PER_DAY) > 32
		//The max number of stats a given day could need to set e.g. at 11 bits per day the 3rd day will be represented in stat 1 and 2
		iNumStatsToRepValue = 2
	ENDIF
	
	INT iArrayIndex	= FLOOR(TO_FLOAT(iStart) / 32.0)
	
	IF iArrayIndex < 0
		PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY - Array size is negative")
		SCRIPT_ASSERT("[GAMBLING_TIME] GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY - Array size is negative")
		EXIT
	ENDIF
	
	//Get the current stat value(s) to add to
	REPEAT iNumStatsToRepValue i
		IF (iArrayIndex + i) >= STATS_TO_REPRESENT_28_DAY_TRACKING
			PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY - Array size not large enough! Required size >= ", (iArrayIndex + i))
			SCRIPT_ASSERT("GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY - Array size not large enough!")
			EXIT
		ENDIF
		
		MPPLY_INT_STATS	eStat
		
		IF bTimeGambling
			eStat = GET_CASINO_TIME_GAMBLING_PER_DAY_STAT(iArrayIndex + i)
		ELSE
			eStat = GET_CASINO_TIME_PLAYING_PER_DAY_STAT(iArrayIndex + i)
		ENDIF
		
		iStatsArr[iArrayIndex + i] = GET_MP_INT_PLAYER_STAT(eStat)
	ENDREPEAT
	
	PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY - Day: ", iDay, " bit set start: ", iStart, " Setting to: ", iTimeToSet, " num stats to represent this day = ", iNumStatsToRepValue, " bTimeGambling: ", bTimeGambling)
	
	//Set the relevant bits
	SET_PACKED_BITFIELD_VALUE_IN_ARRAY_ELEMENTS(iStatsArr, iDay, BITS_TO_REPRESENT_MINUTES_PER_DAY, iTimeToSet)
	
	//Set the new stat values
	REPEAT iNumStatsToRepValue i
		IF (iArrayIndex + i) >= STATS_TO_REPRESENT_28_DAY_TRACKING
			PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY - Array size not large enough! Required size >= ", (iArrayIndex + i))
			SCRIPT_ASSERT("GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY - Array size not large enough!")
			EXIT
		ENDIF
		
		MPPLY_INT_STATS	eStat
		
		IF bTimeGambling
			eStat = GET_CASINO_TIME_GAMBLING_PER_DAY_STAT(iArrayIndex + i)
		ELSE
			eStat = GET_CASINO_TIME_PLAYING_PER_DAY_STAT(iArrayIndex + i)
		ENDIF
		
		SET_MP_INT_PLAYER_STAT(eStat, iStatsArr[iArrayIndex + i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Adds to the time gambling on a given day in the 28 day tracking cycle
/// PARAMS:
///    iDay - The day we want to check time for
///    iMinutesToAdd - The time to set
///    bTimeGambling - Time gambling or time not gambling
PROC GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY(INT iDay, INT iMinutesToAdd, BOOL bTimeGambling)
	INT i
	INT iStatsArr[STATS_TO_REPRESENT_28_DAY_TRACKING]
	
	INT iStart 				= (((iDay + 1) * BITS_TO_REPRESENT_MINUTES_PER_DAY) - BITS_TO_REPRESENT_MINUTES_PER_DAY)
	INT iNumStatsToRepValue	= 1 
	
	IF ((iStart % 32) + BITS_TO_REPRESENT_MINUTES_PER_DAY) > 32
		//The max number of stats a given day could need to set e.g. at 11 bits per day the 3rd day will be represented in stat 1 and 2
		iNumStatsToRepValue = 2
	ENDIF
	
	INT iArrayIndex	= FLOOR(TO_FLOAT(iStart) / 32.0)
	
	IF iArrayIndex < 0
		PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY - Array size is negative")
		SCRIPT_ASSERT("[GAMBLING_TIME] GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY - Array size is negative")
		EXIT
	ENDIF
	
	//Get the current stat value(s) to add to
	REPEAT iNumStatsToRepValue i
		IF (iArrayIndex + i) >= STATS_TO_REPRESENT_28_DAY_TRACKING
			PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY - Array size not large enough! Required size >= ", (iArrayIndex + i))
			SCRIPT_ASSERT("GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY - Array size not large enough!")
			EXIT
		ENDIF
		
		MPPLY_INT_STATS	eStat
		
		IF bTimeGambling
			eStat = GET_CASINO_TIME_GAMBLING_PER_DAY_STAT(iArrayIndex + i)
		ELSE
			eStat = GET_CASINO_TIME_PLAYING_PER_DAY_STAT(iArrayIndex + i)
		ENDIF
		
		iStatsArr[iArrayIndex + i] = GET_MP_INT_PLAYER_STAT(eStat)
	ENDREPEAT
	
	//Get the values from the bits that represent the day we're interested in
	INT iCurrentValue = GET_PACKED_BITFIELD_VALUE_FROM_ARRAY_ELEMENTS(iStatsArr, iDay, BITS_TO_REPRESENT_MINUTES_PER_DAY)	
	
	PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY - Day: ", iDay, " bit set start: ", iStart, " Current time: ", iCurrentValue, " adding: ", iMinutesToAdd, " num stats to represent this day = ", iNumStatsToRepValue, " bTimeGambling: ", bTimeGambling)
	
	//Add the time
	iCurrentValue += iMinutesToAdd
	
	//Set the relevant bits
	SET_PACKED_BITFIELD_VALUE_IN_ARRAY_ELEMENTS(iStatsArr, iDay, BITS_TO_REPRESENT_MINUTES_PER_DAY, iCurrentValue)
	
	//Set the new stat values
	REPEAT iNumStatsToRepValue i
		MPPLY_INT_STATS	eStat
		
		IF bTimeGambling
			eStat = GET_CASINO_TIME_GAMBLING_PER_DAY_STAT(iArrayIndex + i)
		ELSE
			eStat = GET_CASINO_TIME_PLAYING_PER_DAY_STAT(iArrayIndex + i)
		ENDIF
		
		SET_MP_INT_PLAYER_STAT(eStat, iStatsArr[iArrayIndex + i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Adds to time tracking stats for the relevant days
/// PARAMS:
///    iMinutesToAdd - How many minutes to add
///    bTimeGambling - Is this time gambling or time playing GTAO
PROC GAMBLING_TIME_ADD_TO_MINUTES_SPENT_TODAY(INT iMinutesToAdd, BOOL bTimeGambling)
	//Looped track of gambling time so determine which day we need to add time to
	INT iDayToAddTo 			= GET_CURRENT_GAMBLING_TRACK_DAY()
	//Look at how far through this day we currently are in real world time
	INT iCurrentDayElapsedTime	= GET_TIME_THROUGH_CURRENT_GAMBLING_TRACK_DAY()
	
	PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_ADD_TO_MINUTES_SPENT_TODAY - Adding ", iMinutesToAdd, " minutes. Day: ", iDayToAddTo, " iCurrentDayElapsedTime: ", iCurrentDayElapsedTime, " seconds")
	
	//Check if the current time + the time to add will take us into a new day
	IF iMinutesToAdd > 1
	AND (iCurrentDayElapsedTime + (iMinutesToAdd * 60)) > SECONDS_PER_DAY
		//We need to add time to multiple days
		INT iNumDays = CEIL(TO_FLOAT(iCurrentDayElapsedTime + (iMinutesToAdd * 60)) / TO_FLOAT(SECONDS_PER_DAY))
		INT i
		
		PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_ADD_TO_MINUTES_SPENT_TODAY - Adding time for ", iNumDays, " days")
		
		REPEAT iNumDays i
			GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY((iDayToAddTo + i), iMinutesToAdd, bTimeGambling)
		ENDREPEAT
	ELSE
		GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY(iDayToAddTo, iMinutesToAdd, bTimeGambling)
	ENDIF
	
	IF bTimeGambling
		g_iTotalGamblingTime += iMinutesToAdd
		PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_ADD_TO_MINUTES_SPENT_TODAY - Time gambling Global = ", g_iTotalGamblingTime, " minutes")
	ELSE
		g_iTotalGTAOTime += iMinutesToAdd
		PRINTLN("[GAMBLING_TIME] GAMBLING_TIME_ADD_TO_MINUTES_SPENT_TODAY - Time playing GTAO Global = ", g_iTotalGTAOTime, " minutes")
	ENDIF
	
	SET_GAMBLING_TRACKING_UPDATED_TIME()
ENDPROC

/// PURPOSE:
///    Returns true if one of the casino games has set the player as gambling in the casino
FUNC BOOL IS_PLAYER_GAMBLING_IN_THE_CASINO()
	RETURN g_iGamblingTime != -1
ENDFUNC

PROC CLEANUP_ALL_CASINO_TIME_GAMBLING_STATS(BOOL bCleanupBanTime)
	PRINTLN("[GAMBLING_TIME] CLEANUP_ALL_CASINO_TIME_GAMBLING_STATS - Called")
	RESET_GAMBLING_TIME_TRACKING()
	
	//Time gambling per day
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_GMBLNG_0, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_GMBLNG_1, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_GMBLNG_2, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_GMBLNG_3, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_GMBLNG_4, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_GMBLNG_5, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_GMBLNG_6, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_GMBLNG_7, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_GMBLNG_8, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_GMBLNG_9, 0)
	
	//Time playing GTAO per day
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_PLYING_0, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_PLYING_1, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_PLYING_2, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_PLYING_3, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_PLYING_4, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_PLYING_5, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_PLYING_6, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_PLYING_7, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_PLYING_8, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_TIME_PLYING_9, 0)
	
	IF bCleanupBanTime
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_BAN_TIME, 0)
	ENDIF
	
	//24 hour tracking
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24, 0)
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_L24_PX, 0)
	RESET_CASINO_24_HOUR_TIME_TRACKING_STATS()
	
	//Current day we're tracking
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_CUR_GMBLNG_GD, 0)	
	SET_MP_INT_PLAYER_STAT(MPPLY_CAS_CUR_GMBLNG_PX, 0)
	SET_PACKED_STAT_INT(CASINO_CONSECUTIVE_TIME_TRACKING_DAYS, 0, 0)
	SET_CASINO_GAMBLING_EOD_POSIX(-1)
	
	g_iTotalGamblingTime	= 0
	g_iTotalGTAOTime		= 0
	g_iGamblingTime 		= -1
	g_iGamblingTimeLastSave = -1
	g_iGTAOPlayTime			= -1
	g_iGTAOPlayTimeLastSave	= -1
	g_iGamblingBanTime		= 0
	g_iGamblingTimeInlastDay 				= 0
	g_bCasinoGamblingTrackingStarted 		= FALSE
	g_bCasinoGambling24HourTrackingStarted 	= FALSE
ENDPROC

/// PURPOSE:
///    Checks the cumulative playtime tracking to ban the player from gambling if thresholds have been reached
PROC MAINTAIN_GAMBLING_PLAYTIME_THRESHOLD_CHECKS(INT iCloudTime)
	IF NOT IS_LP_BANNED_FROM_BETTING_DUE_TO_TIME_SPENT_GAMBLING()
	AND g_iTotalGamblingTime > CASINO_GAMBLING_BAN_CHECK_THRESHOLD
		FLOAT fPercentageOfTimeInGTAO = (TO_FLOAT(g_iTotalGamblingTime) / TO_FLOAT(g_iTotalGTAOTime))
		
		PRINTLN("[GAMBLING_TIME] MAINTAIN_GAMBLING_PLAYTIME_THRESHOLD_CHECKS - Gambling playtime currently: ", (g_iTotalGamblingTime/60), " percentage of total playtime: ", fPercentageOfTimeInGTAO)
		
		IF fPercentageOfTimeInGTAO > 0.5
			//Ban the player for one week
			CLEANUP_ALL_CASINO_TIME_GAMBLING_STATS(FALSE)
			SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_BAN_TIME, iCloudTime)
			g_iGamblingBanTime = iCloudTime
			
			//Request a save
			PRINTLN("[GAMBLING_TIME][CASINO_SAVE] MAINTAIN_GAMBLING_PLAYTIME_THRESHOLD_CHECKS - Requesting major save")
			g_b_CasinoSave = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    If the player has been banned then check for the ban expiring
PROC MAINTAIN_GAMBLING_BAN_EXPIRATION(INT iCloudTime)
	IF IS_LP_BANNED_FROM_BETTING_DUE_TO_TIME_SPENT_GAMBLING()
		IF (iCloudTime - g_iGamblingBanTime) > SEVEN_DAYS_IN_SECONDS
			PRINTLN("[GAMBLING_TIME][CASINO_SAVE] MAINTAIN_GAMBLING_BAN_EXPIRATION - Player gambling ban expired. Cleanup. Ban POSIX time: ", g_iGamblingBanTime)
			//Ban is over. Cleanup stats to enable tracking again
			CLEANUP_ALL_CASINO_TIME_GAMBLING_STATS(TRUE)
			
			//Not a Major save as this will be hit again if we don't save the first time
			g_b_CasinoSave = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Moves the tracking forward one day - Used before we have looped back to the start of the 28 day track
PROC _GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_PRE_28_DAY()
	INT iCurrentDay = GET_CURRENT_GAMBLING_TRACK_DAY()
	iCurrentDay ++
	
	IF iCurrentDay >= CASINO_GAMBLING_HISTORY_NUM_DAYS
		iCurrentDay = 0
	ENDIF
	
	SET_CASINO_GAMBLING_EOD_POSIX(g_iGamblingEndOfDayPOSIX + SECONDS_PER_DAY)
	
	PRINTLN("[GAMBLING_TIME] _GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_PRE_28_DAY - 24 hours have passed. Moving current day to: ", iCurrentDay, " New POSIX target: ", g_iGamblingEndOfDayPOSIX)
	
	//Reset the time gambling & playing GTAO for this day to 0
	GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY(iCurrentDay, 0, TRUE)
	GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY(iCurrentDay, 0, FALSE)
	//Move the current day forward - Loop 0 - CASINO_GAMBLING_HISTORY_NUM_DAYS
	SET_CURRENT_GAMBLING_TRACK_DAY(iCurrentDay)
	ADD_TO_NUM_GAMBLING_DAYS_TRACKED_IN_CURRENT_RUN(1)			
	
	#IF IS_DEBUG_BUILD
		SAVE_CASINO_DEBUG_TIME_TRACKING_SCREENSHOT()
	#ENDIF
ENDPROC

/// PURPOSE:
///    Moves the tracking forward one day - Used After we have looped back to the start of the 28 day track
PROC _GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_POST_28_DAY(BOOL bFreemodeInit = FALSE)
	INT iLastUpdatePOSIX		= GET_GAMBLING_TRACKING_UPDATED_TIME()
	INT iDaysSinceLastUpdate	= FLOOR(TO_FLOAT(GET_CLOUD_TIME_AS_INT() - iLastUpdatePOSIX)/TO_FLOAT(SECONDS_PER_DAY))
	
	IF iDaysSinceLastUpdate >= CASINO_GAMBLING_HISTORY_NUM_DAYS
		PRINTLN("[GAMBLING_TIME] _GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_POST_28_DAY - 28 day gambling history exceeded. No activity in the last 28 days. Cleanup")
		CLEANUP_ALL_CASINO_TIME_GAMBLING_STATS(TRUE)
		EXIT
	ENDIF
	
	PRINTLN("[GAMBLING_TIME] _GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_POST_28_DAY - 28 day gambling history exceeded. Moving tracking forward by a day")

	INT iCurrentDay = GET_CURRENT_GAMBLING_TRACK_DAY()
	iCurrentDay ++
	
	IF iCurrentDay >= CASINO_GAMBLING_HISTORY_NUM_DAYS
		iCurrentDay = 0
	ENDIF
	
	SET_CASINO_GAMBLING_EOD_POSIX(g_iGamblingEndOfDayPOSIX + SECONDS_PER_DAY)
	
	IF NOT bFreemodeInit
		INT iGamblingTimeToRemove 	= GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY(iCurrentDay, TRUE)
		INT iGTAOTimeToRemove 		= GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY(iCurrentDay, FALSE)
		
		g_iTotalGamblingTime -= iGamblingTimeToRemove
		g_iTotalGTAOTime -= iGTAOTimeToRemove
		
		PRINTLN("[GAMBLING_TIME] _GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_POST_28_DAY - Removed ", iGamblingTimeToRemove, " from total gambling time. Time remaining: ", g_iTotalGamblingTime)
		PRINTLN("[GAMBLING_TIME] _GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_POST_28_DAY - Removed ", iGTAOTimeToRemove, " from total GTAO time. Time remaining: ", g_iTotalGTAOTime)
		
		#IF IS_DEBUG_BUILD
			IF g_iTotalGamblingTime < 0
				SCRIPT_ASSERT("_GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_POST_28_DAY - Invalid total time for g_iTotalGamblingTime")
			ENDIF
		#ENDIF
		
		IF g_iTotalGTAOTime <= 0
			#IF IS_DEBUG_BUILD
				IF g_iTotalGTAOTime < 0
					SCRIPT_ASSERT("_GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_POST_28_DAY - Invalid total time for g_iTotalGTAOTime")
				ENDIF
			#ENDIF
			
			CLEANUP_ALL_CASINO_TIME_GAMBLING_STATS(FALSE)
		ENDIF
	ENDIF
	
	//Reset the time gambling & time playing GTAO for this day to 0
	GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY(iCurrentDay, 0, TRUE)
	GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY(iCurrentDay, 0, FALSE)
	//Move the current day forward - Loop 0 - CASINO_GAMBLING_HISTORY_NUM_DAYS
	SET_CURRENT_GAMBLING_TRACK_DAY(iCurrentDay)
	//Move the time tracking started forward
	MOVE_GAMBLING_TRACKING_FORWARD_ONE_DAY()
	
	#IF IS_DEBUG_BUILD
		SAVE_CASINO_DEBUG_TIME_TRACKING_SCREENSHOT()
	#ENDIF
ENDPROC

PROC MAINTAIN_GAMBLING_28_DAY_TRACKING(INT iCloudTime)
	IF HAS_GAMBLING_TIME_TRACKING_STARTED()
		INT iTimeSinceTrackingBegan = (iCloudTime - GET_TIME_LP_STARTED_GAMLING())
	
		IF iTimeSinceTrackingBegan > (GAMBLE_HISTORY_DAYS_IN_SECONDS - SECONDS_PER_DAY)
			
			IF iCloudTime >= g_iGamblingEndOfDayPOSIX
			
				//Update for when we have reached more than 28 days of tracking
				_GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_POST_28_DAY()
				
			ENDIF
			
		ELIF iCloudTime >= g_iGamblingEndOfDayPOSIX
			
			_GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_PRE_28_DAY()
			
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GAMBLING_24_HOUR_TRACKING(INT iCloudTime)
	IF HAS_24HR_GAMBLING_TIME_TRACKING_STARTED()
		INT iTimeExpired = (iCloudTime - GET_TIME_24HR_GAMBLING_TIME_TRACKING_STARTED())
		
		IF iTimeExpired >= SECONDS_PER_DAY
		OR ((iTimeExpired % 60) = 0
		AND (FLOOR(TO_FLOAT(iTimeExpired) / 60.0)) = GET_CURRENT_GAMBLING_24HR_TRACK_HOUR()
		AND iTimeExpired > 0)
			PRINTLN("[GAMBLING_TIME] MAINTAIN_GAMBLING_24_HOUR_TRACKING - time to move onto next hour. Checking if we should restart tracking. iTimeExpired = ", iTimeExpired)
			
			IF IS_PLAYER_GAMBLING_IN_THE_CASINO()
				INT iTimeSpentGambling 	= (iCloudTime - g_iGamblingTime)
				INT iTimeSinceLastSave	= (iCloudTime - g_iGamblingTimeLastSave)
				
				IF g_iGamblingTimeLastSave = -1
					IF (iTimeSpentGambling + g_iGamblingTimeInlastDay) > TEN_HOURS_IN_SECONDS
						PRINTLN("[GAMBLING_TIME] MAINTAIN_GAMBLING_24_HOUR_TRACKING - Threshold reached for time gambling as 24 hour timer expired! - 1")
						INCREMENT_24_HOUR_GAMBLING_THRESHOLD_REACHED_STAT()
						START_24HR_GAMBLING_TIME_TRACKING()
						
						#IF IS_DEBUG_BUILD
							SAVE_CASINO_DEBUG_TIME_TRACKING_SCREENSHOT()
						#ENDIF
					ENDIF
				ELSE
					IF (iTimeSinceLastSave + g_iGamblingTimeInlastDay) > TEN_HOURS_IN_SECONDS
						PRINTLN("[GAMBLING_TIME] MAINTAIN_GAMBLING_24_HOUR_TRACKING - Threshold reached for time gambling as 24 hour timer expired! - 2")
						INCREMENT_24_HOUR_GAMBLING_THRESHOLD_REACHED_STAT()
						START_24HR_GAMBLING_TIME_TRACKING()
						
						#IF IS_DEBUG_BUILD
							SAVE_CASINO_DEBUG_TIME_TRACKING_SCREENSHOT()
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			INT iCurrentHour = (GET_CURRENT_GAMBLING_24HR_TRACK_HOUR() + 1)
			
			IF iCurrentHour >= CASINO_GAMBLING_HISTORY_NUM_HOURS
				iCurrentHour = 0
			ENDIF
			
			//Check if rolling over to the next hour will mean 0 hours gambling time
			INT iPreviousHourTime = GAMBLING_TIME_24HR_GET_TIME_SPENT_FOR_HOUR(iCurrentHour)
			g_iGamblingTimeInlastDay -= iPreviousHourTime
			
			IF g_iGamblingTimeInlastDay <= 0
				#IF IS_DEBUG_BUILD
					IF g_iGamblingTimeInlastDay < 0
						SCRIPT_ASSERT("MAINTAIN_GAMBLING_24_HOUR_TRACKING - Time spent reduced to < 0")
					ENDIF
				#ENDIF
				
				PRINTLN("[GAMBLING_TIME] MAINTAIN_GAMBLING_24_HOUR_TRACKING - 24 hours reached. 0 seconds spent gambling in the last 24. Stopping tracking")
				STOP_24HR_GAMBLING_TIME_TRACKING()
			ELSE
				PRINTLN("[GAMBLING_TIME] MAINTAIN_GAMBLING_24_HOUR_TRACKING - 24 hours reached. ", g_iGamblingTimeInlastDay, " seconds spent gambling in the last 24.")
				GAMBLING_TIME_24HR_SET_TIME_PLAYING_FOR_HOUR(iCurrentHour, 0)				
				SET_CURRENT_GAMBLING_24HR_TRACK_HOUR(iCurrentHour)
				
				IF iTimeExpired >= SECONDS_PER_DAY
					MOVE_24HR_GAMBLING_TRACKING_FORWARD_ONE_HOUR()
					#IF IS_DEBUG_BUILD
						SAVE_CASINO_DEBUG_TIME_TRACKING_SCREENSHOT()
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			IF g_iGamblingTimeInlastDay >= TEN_HOURS_IN_SECONDS
				PRINTLN("[GAMBLING_TIME] MAINTAIN_GAMBLING_24_HOUR_TRACKING - Threshold reached for time gambling in the last 24 hours!")
				INCREMENT_24_HOUR_GAMBLING_THRESHOLD_REACHED_STAT()
				
				IF IS_PLAYER_GAMBLING_IN_THE_CASINO()
					START_24HR_GAMBLING_TIME_TRACKING()
				ELSE
					STOP_24HR_GAMBLING_TIME_TRACKING()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GAMBLING_TIME_TRACKING()
	INT iCloudTime = GET_CLOUD_TIME_AS_INT()
	
	//28 day rolling track - url:bugstar:5619422 (1)
	MAINTAIN_GAMBLING_28_DAY_TRACKING(iCloudTime)
	//24hr threshold time - url:bugstar:5619422 (2)
	MAINTAIN_GAMBLING_24_HOUR_TRACKING(iCloudTime)
	
	//General GTAO play time - Save once per minute
	IF g_bCasinoGamblingTrackingStarted
		IF g_iGTAOPlayTime = -1
			g_iGTAOPlayTime = iCloudTime
			g_iGTAOPlayTimeLastSave	= -1
			PRINTLN("[GAMBLING_TIME] MAINTAIN_GAMBLING_TIME_TRACKING - Started tracking GTAO playtime")
		ELSE
			INT iTimeSpentPlaying 	= (iCloudTime - g_iGTAOPlayTime)
			INT iTimeSinceLastSave	= (iCloudTime - g_iGTAOPlayTimeLastSave)
			
			IF g_iGTAOPlayTimeLastSave = -1
				IF iTimeSpentPlaying >= GTAO_PLAY_TIME_SAVE_INTERVAL
					g_iGTAOPlayTimeLastSave = iCloudTime
					GAMBLING_TIME_ADD_TO_MINUTES_SPENT_TODAY(FLOOR(TO_FLOAT(iTimeSpentPlaying)/60.0), FALSE)
				ENDIF
			ELSE
				IF iTimeSinceLastSave >= GTAO_PLAY_TIME_SAVE_INTERVAL
					g_iGTAOPlayTimeLastSave = iCloudTime
					GAMBLING_TIME_ADD_TO_MINUTES_SPENT_TODAY(FLOOR(TO_FLOAT(iTimeSinceLastSave)/60.0), FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Gambling time - Save once per minute
	IF IS_PLAYER_GAMBLING_IN_THE_CASINO()
		INT iTimeSpentGambling 	= (iCloudTime - g_iGamblingTime)
		INT iTimeSinceLastSave	= (iCloudTime - g_iGamblingTimeLastSave)
		
		IF g_iGamblingTimeLastSave = -1
			IF iTimeSpentGambling >= CASINO_GAMBLING_TIME_SAVE_INTERVAL
				g_iGamblingTimeLastSave = iCloudTime
				GAMBLING_TIME_ADD_TO_MINUTES_SPENT_TODAY(FLOOR(TO_FLOAT(iTimeSpentGambling)/60.0), TRUE)
				GAMBLING_TIME_ADD_TO_TIME_SPENT_IN_THE_LAST_24HOURS(iTimeSpentGambling)
			ENDIF
		ELSE
			IF iTimeSinceLastSave >= CASINO_GAMBLING_TIME_SAVE_INTERVAL
				g_iGamblingTimeLastSave = iCloudTime
				GAMBLING_TIME_ADD_TO_MINUTES_SPENT_TODAY(FLOOR(TO_FLOAT(iTimeSinceLastSave)/60.0), TRUE)
				GAMBLING_TIME_ADD_TO_TIME_SPENT_IN_THE_LAST_24HOURS(iTimeSinceLastSave)
			ENDIF
		ENDIF
		
		IF iTimeSpentGambling > 0
		AND (iTimeSpentGambling % g_sMPTunables.iVC_CASINO_SAVE_FREQUENCY) = 0
		AND g_iGamblingTimeLastSaveRequest != GET_CLOUD_TIME_AS_INT()
			g_iGamblingTimeLastSaveRequest = GET_CLOUD_TIME_AS_INT()
			PRINTLN("[GAMBLING_TIME][CASINO_SAVE] MAINTAIN_GAMBLING_TIME_TRACKING - Requesting save: POSIX: ", g_iGamblingTimeLastSaveRequest)
			g_b_CasinoSave = TRUE
		ENDIF
	ENDIF
	
	//Threshold checks
	MAINTAIN_GAMBLING_PLAYTIME_THRESHOLD_CHECKS(iCloudTime)
	
	//Are we banned
	MAINTAIN_GAMBLING_BAN_EXPIRATION(iCloudTime)
ENDPROC

PROC CLEAR_GAMBLING_AND_GTAO_TIME_FOR_X_NUM_DAYS(INT iStartDay, INT iNumDays)
	INT i
	
	PRINTLN("[GAMBLING_TIME] CLEAR_GAMBLING_AND_GTAO_TIME_FOR_X_NUM_DAYS - Clearing time for ", iNumDays, " starting at day: ", iNumDays)
	
	REPEAT iNumDays i
		INT iDay = (iStartDay + i)
		
		IF iDay >= CASINO_GAMBLING_HISTORY_NUM_DAYS
			iDay = (iDay - CASINO_GAMBLING_HISTORY_NUM_DAYS)
		ENDIF
		
		GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY(iDay, 0, TRUE)
		GAMBLING_TIME_SET_TIME_PLAYING_FOR_DAY(iDay, 0, FALSE)
	ENDREPEAT
ENDPROC

PROC CLEAR_GAMBLING_TIME_FOR_X_NUM_HOURS(INT iStartHour, INT iNumHours)
	INT i
	
	IF iNumHours > CASINO_GAMBLING_HISTORY_NUM_HOURS
		SCRIPT_ASSERT("CLEAR_GAMBLING_TIME_FOR_X_NUM_HOURS - Clearing time for more than 24 hours!")
		iNumHours = CASINO_GAMBLING_HISTORY_NUM_HOURS
	ENDIF
	
	PRINTLN("[GAMBLING_TIME] CLEAR_GAMBLING_TIME_FOR_X_NUM_HOURS - Clearing time for ", iNumHours, " starting at hour: ", iStartHour)
	
	REPEAT iNumHours i
		INT iHour = (iStartHour + i)
		
		IF iHour >= CASINO_GAMBLING_HISTORY_NUM_HOURS
			iHour = (iHour - CASINO_GAMBLING_HISTORY_NUM_HOURS)
		ENDIF
		
		GAMBLING_TIME_24HR_SET_TIME_PLAYING_FOR_HOUR(iHour, 0)
	ENDREPEAT
ENDPROC

PROC INIT_CASINO_24HR_GAMBLING_TIME_TRACK()
	INT iCloudTime = GET_CLOUD_TIME_AS_INT()
	INT i24HourTrackingStart = GET_TIME_24HR_GAMBLING_TIME_TRACKING_STARTED()
	INT iTimeExpired = (iCloudTime - GET_TIME_24HR_GAMBLING_TIME_TRACKING_STARTED())
	
	IF iTimeExpired >= SECONDS_PER_DAY
		PRINTLN("[GAMBLING_TIME] INIT_CASINO_24HR_GAMBLING_TIME_TRACK - iTimeExpired >= SECONDS_PER_DAY")
		STOP_24HR_GAMBLING_TIME_TRACKING()
		EXIT
	ELIF i24HourTrackingStart > 0
		INT iLastHourUpdated 		= GET_GAMBLING_24HR_TRACKING_UPDATED_TIME()
		INT iHoursSinceLastUpdate 	= FLOOR(TO_FLOAT(iCloudTime - iLastHourUpdated)/TO_FLOAT(SECONDS_PER_HOUR))
		INT iCurrentSavedTrackHour 	= GET_CURRENT_GAMBLING_24HR_TRACK_HOUR()
		INT iHoursSinceStart 		= FLOOR(TO_FLOAT(iCloudTime - i24HourTrackingStart)/TO_FLOAT(SECONDS_PER_HOUR))
		INT iHoursSinceSave			= FLOOR(TO_FLOAT(iLastHourUpdated - i24HourTrackingStart)/TO_FLOAT(SECONDS_PER_HOUR))
		
		PRINTLN("[GAMBLING_TIME] INIT_CASINO_24HR_GAMBLING_TIME_TRACK - iLastHourUpdated: ", 			iLastHourUpdated,
																		" iHoursSinceLastUpdate: ", 	iHoursSinceLastUpdate,
																		" iCurrentSavedTrackHour: ", 	iCurrentSavedTrackHour,
																		" iHoursSinceStart: ", 			iHoursSinceStart,
																		" iHoursSinceSave: ", 			iHoursSinceSave)
		
		IF iHoursSinceLastUpdate > 1
			CLEAR_GAMBLING_TIME_FOR_X_NUM_HOURS((iCurrentSavedTrackHour + 1), (iHoursSinceLastUpdate - 1))
			
			IF (iCurrentSavedTrackHour + iHoursSinceLastUpdate) >= CASINO_GAMBLING_HISTORY_NUM_HOURS
				//Loop back to hour 0
				INT iNewHour = (iCurrentSavedTrackHour + iHoursSinceLastUpdate) - CASINO_GAMBLING_HISTORY_NUM_HOURS
				SET_CURRENT_GAMBLING_24HR_TRACK_HOUR(iNewHour)
			ELSE
				SET_CURRENT_GAMBLING_24HR_TRACK_HOUR(iCurrentSavedTrackHour + iHoursSinceLastUpdate)
			ENDIF
			
			SET_24HR_GAMBLING_TRACKING_UPDATED_TIME()
		ELSE
			PRINTLN("[GAMBLING_TIME] INIT_CASINO_24HR_GAMBLING_TIME_TRACK - iHoursSinceLastUpdate = 0. iHoursSinceStart = ", iHoursSinceStart, " iHoursSinceSave = ", iHoursSinceSave)
			
			IF iHoursSinceStart != iHoursSinceSave
				INT iCurrentHour = (iCurrentSavedTrackHour + 1)
				
				IF iCurrentHour >= CASINO_GAMBLING_HISTORY_NUM_HOURS
					iCurrentHour = 0
				ENDIF
				
				GAMBLING_TIME_24HR_SET_TIME_PLAYING_FOR_HOUR(iCurrentHour, 0)				
				SET_CURRENT_GAMBLING_24HR_TRACK_HOUR(iCurrentHour)
				
				IF iTimeExpired >= SECONDS_PER_DAY
					PRINTLN("[GAMBLING_TIME] INIT_CASINO_24HR_GAMBLING_TIME_TRACK - iTimeExpired >= SECONDS_PER_DAY")
					MOVE_24HR_GAMBLING_TRACKING_FORWARD_ONE_HOUR()
					#IF IS_DEBUG_BUILD
						SAVE_CASINO_DEBUG_TIME_TRACKING_SCREENSHOT()
					#ENDIF
				ENDIF
				
				SET_24HR_GAMBLING_TRACKING_UPDATED_TIME()
			ENDIF
		ENDIF
		
		PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING - 24HR - Resumed")
		
		g_bCasinoGambling24HourTrackingStarted = TRUE
	ELSE
		PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING - 24HR - No tracking has started yet")
		STOP_24HR_GAMBLING_TIME_TRACKING()
		EXIT
	ENDIF
	
	INT i
	
	REPEAT CASINO_GAMBLING_HISTORY_NUM_HOURS i
		g_iGamblingTimeInlastDay += GAMBLING_TIME_24HR_GET_TIME_SPENT_FOR_HOUR(i)
	ENDREPEAT
	
	IF g_iGamblingTimeInlastDay <= 0
		PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING - 24HR - No time in the last 24 hours. Reset.")
		STOP_24HR_GAMBLING_TIME_TRACKING()
	ELSE
		PRINTLN("[GAMBLING_TIME] INIT_CASINO_24HR_GAMBLING_TIME_TRACK - Done. Time in the last 24 hours: ", g_iGamblingTimeInlastDay, " seconds")
	ENDIF
ENDPROC

PROC INIT_GAMBLING_TRACKING()
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CASINO_MEMBERSHIP_PURCHASED)
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCasinoPropertyData.iBSCasino, PROPERTY_BROADCAST_BS_CASINO_PLAYER_PURCHASED_CASINO_MEMBERSHIP)
	ENDIF
	
	INT iDaysTrackingGambling 		= GET_NUM_DAYS_SINCE_GAMBLING_TRACKING_BEGAN()
	INT iLastUpdatePOSIX			= GET_GAMBLING_TRACKING_UPDATED_TIME()
	INT iDaysSinceLastUpdate		= FLOOR(TO_FLOAT(GET_CLOUD_TIME_AS_INT() - iLastUpdatePOSIX)/TO_FLOAT(SECONDS_PER_DAY))
	INT iCurrentSavedTrackDay		= GET_CURRENT_GAMBLING_TRACK_DAY()
	INT iCurrentDayElapsedTime		= GET_TIME_THROUGH_CURRENT_GAMBLING_TRACK_DAY()
	INT iTimeUntilEOD 				= (SECONDS_PER_DAY - iCurrentDayElapsedTime)
	INT iDaysFromStartToLastSave 	= GET_NUM_DAYS_FROM_GMBL_TRACK_START_TO_TIME(iLastUpdatePOSIX)
	
	//Reset global variables
	g_iGamblingTime 						= -1
	g_iGamblingTimeLastSave 				= -1
	g_iGTAOPlayTime							= -1
	g_iGTAOPlayTimeLastSave					= -1
	g_iGamblingBanTime						= GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_BAN_TIME)
	g_iTotalGamblingTime					= 0
	g_iTotalGTAOTime						= 0
	g_iGamblingTimeInlastDay 				= 0
	g_bCasinoGamblingTrackingStarted 		= FALSE
	g_bCasinoGambling24HourTrackingStarted 	= FALSE
	
	PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING - iDaysTrackingGambling: ", 	iDaysTrackingGambling,
													" iLastUpdatePOSIX: ", 			iLastUpdatePOSIX,
													" iDaysSinceLastUpdate: ", 		iDaysSinceLastUpdate,
													" iCurrentSavedTrackDay: ", 	iCurrentSavedTrackDay,
													" iCurrentDayElapsedTime: ", 	iCurrentDayElapsedTime,
													" iTimeUntilEOD: ", 			iTimeUntilEOD,
													" iDaysFromStartToLastSave: ",	iDaysFromStartToLastSave,
													" Time since tracking began: ",	GET_TIME_IN_SECONDS_SINCE_GAMBLING_TRACKING_BEGAN())
	
	//Are we currently banned
	IF IS_LP_BANNED_FROM_BETTING_DUE_TO_TIME_SPENT_GAMBLING()
	
		PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING Local player is banned from gambling. Ban time: ", g_iGamblingBanTime)
		
	//Have we started any tracking
	ELIF iDaysTrackingGambling > -1 
		IF iDaysSinceLastUpdate < CASINO_GAMBLING_HISTORY_NUM_DAYS
			IF iDaysSinceLastUpdate > 1
				
				PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING - iDaysSinceLastUpdate = ", iDaysSinceLastUpdate)
				
				//Cear the time for these days as they may be leftover from the previous loop as we do a 28 day moving track
				CLEAR_GAMBLING_AND_GTAO_TIME_FOR_X_NUM_DAYS((iCurrentSavedTrackDay + 1), (iDaysSinceLastUpdate - 1))
				ADD_TO_NUM_GAMBLING_DAYS_TRACKED_IN_CURRENT_RUN(iDaysSinceLastUpdate)
				
				IF (iCurrentSavedTrackDay + iDaysSinceLastUpdate) >= CASINO_GAMBLING_HISTORY_NUM_DAYS
					//Loop back to day 0
					INT iNewDay = (iCurrentSavedTrackDay + iDaysSinceLastUpdate) - CASINO_GAMBLING_HISTORY_NUM_DAYS
					SET_CURRENT_GAMBLING_TRACK_DAY(iNewDay)
				ELSE
					SET_CURRENT_GAMBLING_TRACK_DAY(iCurrentSavedTrackDay + iDaysSinceLastUpdate)
				ENDIF
				
				SET_GAMBLING_TRACKING_UPDATED_TIME()
			ELSE
				PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING - 0 Days since last update. Time since = ", (GET_CLOUD_TIME_AS_INT() - iLastUpdatePOSIX), " seconds. Current day Elapsed time: ", iCurrentDayElapsedTime)
				
				IF iDaysTrackingGambling != iDaysFromStartToLastSave
					PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING Moving forward one day")
					IF iDaysFromStartToLastSave >= CASINO_GAMBLING_HISTORY_NUM_DAYS
						_GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_POST_28_DAY(TRUE)
					ELSE
						_GAMBLING_28_DAY_MOVE_FORWARD_ONE_DAY_PRE_28_DAY()
					ENDIF
					
					SET_GAMBLING_TRACKING_UPDATED_TIME()
				ELSE
					PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING - No update necessary to current gameday")
				ENDIF
			ENDIF
			
			//Update the 24 hour tracker
			INIT_CASINO_24HR_GAMBLING_TIME_TRACK()
			
			//Grab the total time gambling/playing GTAO in the last 28 days from stats
			INT i
			
			REPEAT CASINO_GAMBLING_HISTORY_NUM_DAYS i
				g_iTotalGamblingTime += GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY(i, TRUE)
				g_iTotalGTAOTime += GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY(i, FALSE)
			ENDREPEAT
			
			PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING - Time gambling in casino: ", g_iTotalGamblingTime, " minutes. Time playing GTAO: ", g_iTotalGTAOTime, " minutes")
						
			SET_CASINO_GAMBLING_EOD_POSIX(GET_CLOUD_TIME_AS_INT() + iTimeUntilEOD)
			g_bCasinoGamblingTrackingStarted = TRUE
		ELSE
			//We have not tracked anything for the last 28 days. Cleanup tracking.
			CLEANUP_ALL_CASINO_TIME_GAMBLING_STATS(TRUE)
		ENDIF
	ELSE
		PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING - No tracking has started yet")
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_EnableGMBLAutoScreenshot")
			PRINTLN("[GAMBLING_TIME] INIT_GAMBLING_TRACKING - sc_EnableGMBLAutoScreenshot = TRUE")
			g_bEnableCasinoDisplayScreenshots = TRUE
		ENDIF
	#ENDIF
	
	g_iPOSIXTimeLastCollectedChips = GET_MP_INT_PLAYER_STAT(MPPLY_CHIPS_COL_TIME)
	
	INIT_CASINO_CHIPS_HUD_DATA()
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////// Main update ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Updates timers that trigger blocking of casino activities
PROC UPDATE_CASINO_DATA(GENERIC_TRANSACTION_STATE &eCasinoMemBonusTransaction)
	//Update once every 60 frames
	IF ((GET_FRAME_COUNT() % 60) = 0)
		//Reset the timer for gameday purchases of casino chips
		INT iPurchaseTimeTracker		= GET_CASINO_CHIPS_PURCHASE_TIME()
		INT iTimeElapsedOnPurchasing 	= (GET_CLOUD_TIME_AS_INT() - iPurchaseTimeTracker)
		
		IF iPurchaseTimeTracker > 0
		AND iTimeElapsedOnPurchasing > g_sMPTunables.iVC_CASINO_CHIP_MAX_BUY_COOLDOWN
			PRINTLN("[CASINO_CHIPS] UPDATE_CASINO_DATA - Reset MP_CASINO_CHIPS_PURTIM")
			SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_PURTIM, 0)
			SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_PUR_GD, 0)
		ENDIF
		
		//Reset the timer for gameday chip winnings on betting
		INT iWinLossTimeTracker			= GET_TRACKING_TIME_FOR_CHIPS_WIN_LOSS_TO_CASINO()
		INT iTimeElapsedOnBetWins 	= (GET_CLOUD_TIME_AS_INT() - iWinLossTimeTracker)
		
		IF iWinLossTimeTracker > 0
		AND iTimeElapsedOnBetWins > g_sMPTunables.iVC_CASINO_CHIP_MAX_WIN_LOSS_COOLDOWN
			PRINTLN("[CASINO_CHIPS] UPDATE_CASINO_DATA - Reset MP_CASINO_CHIPS_WONTIM")
			SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WONTIM, 0)
			SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WON_GD, 0)
		ENDIF
	ENDIF
	
	IF g_bGivePlayerCasinoMemebershipBonus
		IF NOT NETWORK_CASINO_CAN_BUY_CHIPS_PVC()
			g_bGivePlayerCasinoMemebershipBonus = FALSE
		ELSE
			IF GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_MEM_BONUS) <= 2
				IF ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS(5000, CCU_MEBERSHIP_BONUS, eCasinoMemBonusTransaction)
					PRINTLN("[CASINO_CHIPS] UPDATE_CASINO_DATA - Done adding casino chips for CCU_MEBERSHIP_BONUS")
					eCasinoMemBonusTransaction = TRANSACTION_STATE_DEFAULT
					g_bGivePlayerCasinoMemebershipBonus = FALSE
				ELSE
					PRINTLN("[CASINO_CHIPS] UPDATE_CASINO_DATA - Doing transaction for CCU_MEBERSHIP_BONUS")
				ENDIF
			ELSE
				g_bGivePlayerCasinoMemebershipBonus = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	MAINTAIN_GAMBLING_TIME_TRACKING()
ENDPROC

/// PURPOSE:
///    Check to see if player is able to play a particular casino game
/// PARAMS:
///    theGame - CASINO_GAMES enum list of games
///    iReason - int representing a reason for being blocked in case we need help text.
/// RETURNS:
///    TRUE if player is blocked
FUNC BOOL IS_PLAYER_BLOCKED_FROM_CASINO_GAME(CASINO_GAMES theGame, INT &iReason)
	iReason = -1 //in case we need to add reasons
	//whatever block is in place (world location/gambling rules etc)
	INT iHash
	SWITCH theGame
		CASE CG_SLOTS iHash = HASH("GTAO_CASINO_SLOTS") BREAK
		CASE CG_BLACKJACK iHash = HASH("GTAO_CASINO_BLACKJACK") BREAK
		CASE CG_3CARDPOKER iHash = HASH("GTAO_CASINO_3CARDPOKER") BREAK
		CASE CG_INSIDE_TRACK iHash = HASH("GTAO_CASINO_INSIDETRACK") BREAK
		CASE CG_ROULETTE iHash = HASH("GTAO_CASINO_ROULETTE") BREAK
		CASE CG_BIGWHEEL iHash = HASH("GTAO_CASINO_LUCKYWHEEL") BREAK
	ENDSWITCH
	IF NOT NETWORK_CASINO_CAN_BET(iHash)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GAMBLING_APPROVED_REGION()
	INT iReason
	INT i
	CASINO_GAMES CasinoGame
	REPEAT 6 i
		CasinoGame = INT_TO_ENUM(CASINO_GAMES, i)
		IF IS_PLAYER_BLOCKED_FROM_CASINO_GAME(CasinoGame, iReason)
			PRINTLN("IS_GAMBLING_APPROVED_REGION - returning FALSE, game ", i, ", iReason = ", iReason)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	PRINTLN("IS_GAMBLING_APPROVED_REGION - returning TRUE.")
	RETURN TRUE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ Inside Track race data ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC CLONE_INSIDE_TRACK_RACE_DATA(INSIDE_TRACK_RACE_DATA &sOrigin, INSIDE_TRACK_RACE_DATA &sDestination)
	PRINTLN("[INSIDE_TRACK] CLONE_INSIDE_TRACK_RACE_DATA")
	
	INT i
	
	sDestination.iBS						= sOrigin.iBS
	sDestination.eGameState					= sOrigin.eGameState
	REPEAT iCONST_INSIDE_TRACK_LANE_COUNT i
		sDestination.eHorsesRacing[i]		= sOrigin.eHorsesRacing[i]
		sDestination.iHorseTimes[i]			= sOrigin.iHorseTimes[i]
	ENDREPEAT
	sDestination.iWinningSegment			= sOrigin.iWinningSegment
	sDestination.iSeed						= sOrigin.iSeed
	sDestination.iDuration					= sOrigin.iDuration
	sDestination.iPositionList				= sOrigin.iPositionList
	REPEAT INSIDE_TRACK_RACE_TIMER_COUNT i
		sDestination.stTimer[i].StartTime	= sOrigin.stTimer[i].StartTime
		sDestination.stTimer[i].PauseTime	= sOrigin.stTimer[i].PauseTime
		sDestination.stTimer[i].TimerBits	= sOrigin.stTimer[i].TimerBits
	ENDREPEAT
ENDPROC

PROC RESET_INSIDE_TRACK_RACE_DATA(INSIDE_TRACK_RACE_DATA &sRaceData)
	PRINTLN("[INSIDE_TRACK] RESET_INSIDE_TRACK_RACE_DATA")
	
	INT i
	
	sRaceData.iBS							= 0
	sRaceData.eGameState					= INSIDE_TRACK_GAME_STATE_SETUP_NEW_GAME
	REPEAT iCONST_INSIDE_TRACK_LANE_COUNT i
		sRaceData.eHorsesRacing[i]			= INSIDE_TRACK_HORSE_TYPE_INVALID
		sRaceData.iHorseTimes[i]			= 0
	ENDREPEAT
	sRaceData.iWinningSegment				= -1
	sRaceData.iSeed							= 0
	sRaceData.iDuration						= 0
	sRaceData.iPositionList					= -1
	REPEAT INSIDE_TRACK_RACE_TIMER_COUNT i
		CANCEL_TIMER(sRaceData.stTimer[i])
	ENDREPEAT
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ Debug functions ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC SETUP_CASINO_FM_DEBUG_WIDGETS(CASINO_DEBUG_WIDGET_DATA &sData)
	START_WIDGET_GROUP("Casino")
		ADD_WIDGET_BOOL("g_bBypassAllDecorRestrictions",g_bBypassAllDecorRestrictions)
		START_WIDGET_GROUP("House chips management")
			ADD_WIDGET_INT_SLIDER("Casino chips to add/remove", sData.iChipsToAddRemove, 0, 20000000, 1)
			ADD_WIDGET_BOOL("Add chips", sData.bAddChips)
			ADD_WIDGET_BOOL("Remove chips", sData.bRemoveChips)
			ADD_WIDGET_BOOL("Reset chip purchase/win/loss Restrictions", sData.bResetRestrictions)
			ADD_WIDGET_BOOL("Display chips on screen", sData.bDisplayChips)
			ADD_WIDGET_BOOL("Bypass chips total stat", g_bEnableChipsBypass)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Freemode - Inside Track")
			IF GET_DRAW_DEBUG_COMMANDLINE_PARAM_EXISTS("inside_track")
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				sData.bDrawDebugInsideTrackBet = TRUE
				sData.bDrawDebugInsideTrackHostMainEvent = TRUE
				sData.bDrawDebugInsideTrackRewardMainEvent = TRUE
			ENDIF
			
			ADD_WIDGET_BOOL("Draw debug bet info", sData.bDrawDebugInsideTrackBet)
			ADD_WIDGET_BOOL("Draw debug Host Main Event", sData.bDrawDebugInsideTrackHostMainEvent)
			ADD_WIDGET_BOOL("Draw debug Reward Main Event", sData.bDrawDebugInsideTrackRewardMainEvent)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Playing time management")
			ADD_WIDGET_BOOL("Start gambling", sData.bStartGamblingTracking)
			ADD_WIDGET_BOOL("Stop gambling", sData.bStopGamblingTracking)
			ADD_WIDGET_INT_SLIDER("Minutes to add to time spent", sData.iAddMinutesToTime, 1, 1440, 1)
			ADD_WIDGET_BOOL("Add minutes to time playing GTAO today", sData.bAddMinutesToTime)
			ADD_WIDGET_BOOL("Add minutes to time gambling today", sData.bAddMinutesToTimeGMBL)
			ADD_WIDGET_BOOL("Reset time gambling tracking", sData.bResetGamblingTimeTracking)
			ADD_WIDGET_BOOL("Move time tracking to next day", sData.bMoveGamblingTrackingToNextDay)
			ADD_WIDGET_INT_SLIDER("Time to simulat before EOD", sData.iTimeToSimulateUntilEOD, 0, 1200, 1)
			ADD_WIDGET_BOOL("Simulate day end", sData.bSimulateTrackDayEnd)
			ADD_WIDGET_BOOL("Ban local player", sData.bBanPlayer)
			ADD_WIDGET_BOOL("Reduce ban to 60 seconds", sData.bReduceGamblingBan)
			START_WIDGET_GROUP("Debug display")
				ADD_WIDGET_BOOL("Draw gambling tracking data", sData.bDebugDrawTrackingData)
				ADD_WIDGET_BOOL("Enable debug display repositioning", sData.bEnableDisplayRepositioning)
				ADD_WIDGET_BOOL("Reset display position", sData.bResetDisplayPosition)
				ADD_WIDGET_BOOL("Enable Auto screenshot", g_bEnableCasinoDisplayScreenshots)
				ADD_WIDGET_STRING("To reposition the display, Enable using 'Allow debug display positioning' and follow the instructions below: ")
				IF IS_PC_VERSION()
					ADD_WIDGET_STRING("Click on screen where you'd like it positioned.")
				ELSE
					ADD_WIDGET_STRING("Use the rag mousepad to select a position")
				ENDIF
				ADD_WIDGET_STRING("Deselect 'Allow x display positioning' when done")
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Casino Nightclub Light Rig")
			ADD_WIDGET_BOOL("Override Light Rig Start Time", g_bOverrideCasinoNightClubLightRigTime)
			ADD_WIDGET_INT_SLIDER("Second", g_iCasinoNightClubLightRigSecond, 0, 59, 1)
			ADD_WIDGET_INT_SLIDER("Minute", g_iCasinoNightClubLightRigMinute, 0, 59, 1)
			ADD_WIDGET_INT_SLIDER("Hour", g_iCasinoNightClubLightRigHour, 0, 23, 1)
			ADD_WIDGET_INT_SLIDER("Day", g_iCasinoNightClubLightRigDay, 1, 31, 1)
			ADD_WIDGET_INT_SLIDER("Month", g_iCasinoNightClubLightRigMonth, 1, 12, 1)
			ADD_WIDGET_INT_SLIDER("Year", g_iCasinoNightClubLightRigYear, 0, 3000, 1)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

PROC DRAW_BASIC_CASINO_TIME_DEBUG_DISPLAY(FLOAT fSelectedX, FLOAT fSelectedY)
	FLOAT fOffsetX = (fSelectedX - 0.154)
	FLOAT fOffsetY = (fSelectedY - 0.1075)
	
	//BACKGROUND RECTANGLE
	IF fSelectedX = -1
		DRAW_RECT(0.15, 0.1055, 0.26, 0.15, 0, 0, 0, 200)
		fOffsetX = 0.0
		fOffsetY = 0.0
	ELSE
		DRAW_RECT(fSelectedX, fSelectedY , 0.26, 0.15, 0, 0, 0, 200)
	ENDIF
	
	//Title
    SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(0, 255, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, 0.0295 + fOffsetY, "STRING", "Gambling time stats")

	SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	
	IF IS_LP_BANNED_FROM_BETTING_DUE_TO_TIME_SPENT_GAMBLING()			
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, 0.0895 + fOffsetY, "STRING", "Banned from casino games")
		
		SET_TEXT_SCALE(0.4, 0.4)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, 0.1295 + fOffsetY, "STRING", "Ban time remaining: ")
		
		INT iTime = (SEVEN_DAYS_IN_SECONDS - (GET_CLOUD_TIME_AS_INT() - g_iGamblingBanTime)) * 1000
		TEXT_LABEL_15 tlTime = GET_TIME_AS_TEXT_LABEL(iTime)
		
		SET_TEXT_SCALE(0.4, 0.4)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1475 + fOffsetX, 0.1295 + fOffsetY, "STRING", tlTime)
	ELSE
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, 0.0895 + fOffsetY, "STRING", "No tracking active.")
	ENDIF
ENDPROC

PROC DRAW_CASINO_GAMBLING_HISTORY(INT iStartDay, INT iNumDays, FLOAT fYPos, FLOAT fStartXPos)
	
	CONST_FLOAT fXSpacing 0.05
	
	INT i
	FLOAT fXPos = (fStartXPos - 0.044)
	TEXT_LABEL_15 tlTimeSplit
	
	FOR i = iStartDay TO (iNumDays - 1)
		tlTimeSplit = GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY(i, TRUE)
		tlTimeSplit += ":"
		tlTimeSplit += GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY(i, FALSE)
		fXPos += fXSpacing
		
		//Day
		SET_TEXT_SCALE(0.3, 0.3)
		IF GET_CURRENT_GAMBLING_TRACK_DAY() = i
			SET_TEXT_COLOUR(0, 255, 0, 255)
		ELSE
			SET_TEXT_COLOUR(255, 255, 255, 255)
		ENDIF
		DISPLAY_TEXT_WITH_NUMBER(fXPos, fYPos, "NUMBER", (i + 1))
		
		SET_TEXT_SCALE(0.3, 0.3)
		IF GET_CURRENT_GAMBLING_TRACK_DAY() = i
			SET_TEXT_COLOUR(0, 255, 0, 255)
		ELSE
			SET_TEXT_COLOUR(255, 255, 255, 255)
		ENDIF
		DISPLAY_TEXT_WITH_LITERAL_STRING(fXPos, fYPos + 0.015, "STRING", tlTimeSplit)
	ENDFOR
ENDPROC

PROC DRAW_CASINO_GAMBLING_DATA(FLOAT fSelectedX, FLOAT fSelectedY)
	CONST_FLOAT fBasicRowSpacing 0.04
	FLOAT fCurRowY = 0.0295
	
	FLOAT fOffsetX = (fSelectedX - 0.20)
	FLOAT fOffsetY = (fSelectedY - 0.25)
	INT iCloudTime = GET_CLOUD_TIME_AS_INT()
		
	//Basic displays - No tracking active - Banned/not played
	IF GET_NUM_DAYS_SINCE_GAMBLING_TRACKING_BEGAN() <= -1
		DRAW_BASIC_CASINO_TIME_DEBUG_DISPLAY(fSelectedX, fSelectedY)
		EXIT
	ENDIF
	
	//BACKGROUND RECTANGLE
	IF fSelectedX = -1
		DRAW_RECT(0.2, 0.25, 0.35, 0.45, 0, 0, 0, 200)
		fOffsetX = 0.0
		fOffsetY = 0.0
	ELSE
		DRAW_RECT(fSelectedX, fSelectedY , 0.35, 0.45, 0, 0, 0, 200)
	ENDIF
	
	//Title
    SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(0, 255, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Gambling Time Stats")
	
	//Gambling status + time
	fCurRowY += 0.04755
    SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	IF IS_PLAYER_GAMBLING_IN_THE_CASINO()
		TEXT_LABEL_63 tlStatus = "Status: Gambling: "
		tlStatus += GET_CASINO_GAME_DEBUG_STRING(g_eCasinoGameInUse)
		tlStatus += ":"
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, fCurRowY + fOffsetY, "STRING", tlStatus)
		INT iTime = ((iCloudTime - g_iGamblingTime) * 1000)
		TEXT_LABEL_15 TLGamblingTime
		TLGamblingTime = GET_TIME_AS_TEXT_LABEL(iTime)
		
		SET_TEXT_SCALE(0.4, 0.4)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.26 + fOffsetX, fCurRowY + fOffsetY, "STRING", TLGamblingTime)
	ELSE
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.142 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Status: Not Gambling")
	ENDIF
	
	//Tracking run data
	fCurRowY += fBasicRowSpacing
    SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(51, 204, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Current tracking run:")
	
	fCurRowY += 0.03
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Day: ")
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.0475 + fOffsetX, fCurRowY + fOffsetY, "NUMBER", (GET_CURRENT_GAMBLING_TRACK_DAY() + 1))
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0675 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Days tracked: ")
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.13 + fOffsetX, fCurRowY + fOffsetY, "NUMBER", GET_NUM_GAMBLING_DAYS_TRACKED_IN_CURRENT_RUN())
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.145 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Gambling time (m): ")
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.23 + fOffsetX, fCurRowY + fOffsetY, "NUMBER", g_iTotalGamblingTime)
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.265 + fOffsetX, fCurRowY + fOffsetY, "STRING", "GTAO time (m): ")
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.335 + fOffsetX, fCurRowY + fOffsetY, "NUMBER", g_iTotalGTAOTime)
	
	//General info:
	fCurRowY += 0.0275
    SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Day current time: ")
	
	INT iCurrentDayElapsedTime	= GET_TIME_THROUGH_CURRENT_GAMBLING_TRACK_DAY()
	TEXT_LABEL_15 TLDayTime
	TLDayTime = GET_TIME_AS_TEXT_LABEL(iCurrentDayElapsedTime * 1000)
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.105 + fOffsetX, fCurRowY + fOffsetY, "STRING", TLDayTime)
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.15 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Time gambling as %: ")
	
	FLOAT fPercentageOfTimeInGTAO = (TO_FLOAT(g_iTotalGamblingTime) / TO_FLOAT(g_iTotalGTAOTime)) * 100
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_FLOAT(0.24 + fOffsetX, fCurRowY + fOffsetY, "NUMBER", fPercentageOfTimeInGTAO, 3)
	
	//28 day history:
	fCurRowY += 0.0325
    SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(51, 204, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, fCurRowY + fOffsetY, "STRING", "28 day history:")
	
	fCurRowY += 0.03525
	DRAW_CASINO_GAMBLING_HISTORY(0, 7, fCurRowY + fOffsetY, 0.0275 + fOffsetX)
	fCurRowY += 0.0325
	DRAW_CASINO_GAMBLING_HISTORY(7, 14, fCurRowY + fOffsetY, 0.0275 + fOffsetX)
	fCurRowY += 0.0325
	DRAW_CASINO_GAMBLING_HISTORY(14, 21, fCurRowY + fOffsetY, 0.0275 + fOffsetX)
	fCurRowY += 0.0325
	DRAW_CASINO_GAMBLING_HISTORY(21, 28, fCurRowY + fOffsetY, 0.0275 + fOffsetX)
	
	//24 hour tracking:
	fCurRowY += 0.0325
    SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(51, 204, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, fCurRowY + fOffsetY, "STRING", "24 hour tracking:")
	
	fCurRowY += 0.025
	
	IF HAS_24HR_GAMBLING_TIME_TRACKING_STARTED()
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Hour:")
		
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_NUMBER(0.055 + fOffsetX, fCurRowY + fOffsetY, "NUMBER", (GET_CURRENT_GAMBLING_24HR_TRACK_HOUR() + 1))
		
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Time gambling in the last 24 hours:")
		
		iCurrentDayElapsedTime = g_iGamblingTimeInlastDay * 1000
		TLDayTime = GET_TIME_AS_TEXT_LABEL(iCurrentDayElapsedTime)
		
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.257 + fOffsetX, fCurRowY + fOffsetY, "STRING", TLDayTime)
	ELSE
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Tracking not started")
	ENDIF
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.305 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Count:")
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.34 + fOffsetX, fCurRowY + fOffsetY, "NUMBER", GET_MP_INT_PLAYER_STAT(MPPLY_CAS_GMBLNG_TRHSLD))
	
	fCurRowY += 0.0275
    SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(51, 204, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Gameday bans:")
	
	fCurRowY += 0.025
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Win/Loss:")
		
	IF GET_CHIPS_WIN_LOSS_TOTAL_GD_FOR_LOCAL_PLAYER() >= g_sMPTunables.iVC_CASINO_CHIP_MAX_WIN_DAILY
		iCurrentDayElapsedTime = (g_sMPTunables.iVC_CASINO_CHIP_MAX_WIN_LOSS_COOLDOWN - (iCloudTime - GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WONTIM))) * 1000
		TLDayTime = GET_TIME_AS_TEXT_LABEL(iCurrentDayElapsedTime)
		
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.075 + fOffsetX, fCurRowY + fOffsetY, "STRING", TLDayTime)
	ELSE
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_NUMBER(0.075 + fOffsetX, fCurRowY + fOffsetY, "NUMBER", GET_CHIPS_WIN_LOSS_TOTAL_GD_FOR_LOCAL_PLAYER())
	ENDIF
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.218 + fOffsetX, fCurRowY + fOffsetY, "STRING", "Purchasing:")
		
	IF GET_NUM_CHIPS_LP_HAS_BOUGHT_THIS_GAME_DAY() >= GET_NUM_CHIPS_PURCHASABLE_PER_GAMEDAY()
		iCurrentDayElapsedTime = (g_sMPTunables.iVC_CASINO_CHIP_MAX_BUY_COOLDOWN - (iCloudTime - GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_PURTIM))) * 1000
		TLDayTime = GET_TIME_AS_TEXT_LABEL(iCurrentDayElapsedTime)
		
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.275 + fOffsetX, fCurRowY + fOffsetY, "STRING", TLDayTime)
	ELSE
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_NUMBER(0.275 + fOffsetX, fCurRowY + fOffsetY, "NUMBER", GET_NUM_CHIPS_LP_HAS_BOUGHT_THIS_GAME_DAY())
	ENDIF
	
ENDPROC

PROC DRAW_INSIDE_TRACK_RACE_DATA(INSIDE_TRACK_RACE_DATA &sRaceData, HUD_COLOURS eHudColour, INT &iRow,
			FLOAT cfDISPLAY_AT_X = 0.850,
			FLOAT cfDISPLAY_AT_Y_A = 0.150,
			FLOAT cfDISPLAY_AT_Y_B = 0.018)

	INT red, green, blue, alpha_value, i
	FLOAT fAlphaMult = 1.0
	TEXT_LABEL_63 tl63Literal = ""
	IF sRaceData.iBS != 0
		GET_HUD_COLOUR(eHudColour, red, green, blue, alpha_value)
	ELSE
		IF (eHudColour = HUD_COLOUR_RED)
			GET_HUD_COLOUR(HUD_COLOUR_REDLIGHT, red, green, blue, alpha_value)
		ELIF (eHudColour = HUD_COLOUR_BLUE)
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, red, green, blue, alpha_value)
		ELSE
			GET_HUD_COLOUR(HUD_COLOUR_GREYLIGHT, red, green, blue, alpha_value)
		ENDIF
		alpha_value = ROUND(TO_FLOAT(alpha_value)*0.5)
	ENDIF
	
	//
	IF sRaceData.iBS != 0
		REPEAT 32 i
			IF IS_BIT_SET(sRaceData.iBS, i)
				tl63Literal  = "iBS["
				tl63Literal += i
				tl63Literal += "]: set"
				SET_TEXT_SCALE(0.30, 0.30)
				SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
				DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
						cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
						"STRING", tl63Literal) iRow++
			ENDIF
		ENDREPEAT
	ELSE
		tl63Literal  = "iBS: "
		tl63Literal += sRaceData.iBS
		SET_TEXT_SCALE(0.30, 0.30)
		SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
		DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
				cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
				"STRING", tl63Literal) iRow++
	ENDIF
	
	//
	tl63Literal  = "eGameState:"
	SWITCH sRaceData.eGameState
		CASE INSIDE_TRACK_GAME_STATE_SETUP_NEW_GAME		tl63Literal += "setup game" BREAK
		CASE INSIDE_TRACK_GAME_STATE_RACE_COUNTDOWN		tl63Literal += "countdown" BREAK
		CASE INSIDE_TRACK_GAME_STATE_START_THE_RACE		tl63Literal += "start race" BREAK
		CASE INSIDE_TRACK_GAME_STATE_RANDOMISE_REWARD	tl63Literal += "randomise" BREAK
		CASE INSIDE_TRACK_GAME_STATE_RACE_IN_PROGRESS	tl63Literal += "in progress" BREAK
		CASE INSIDE_TRACK_GAME_STATE_REWARD				tl63Literal += "reward" BREAK
		CASE INSIDE_TRACK_GAME_STATE_POST_RACE			tl63Literal += "post race" BREAK
		CASE INSIDE_TRACK_GAME_STATE_RACE_RESULTS		tl63Literal += "race results" BREAK
		CASE INSIDE_TRACK_GAME_STATE_CLEANUP			tl63Literal += "cleanup" BREAK
		
		DEFAULT
			tl63Literal += "unknown_"
			tl63Literal += ENUM_TO_INT(sRaceData.eGameState)
		BREAK
	ENDSWITCH
	SET_TEXT_SCALE(0.30, 0.30)
	SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
	DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
			cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
			"STRING", tl63Literal) iRow++
	
	//
	REPEAT iCONST_INSIDE_TRACK_LANE_COUNT i
		tl63Literal  = "eHorsesRacing["
		tl63Literal += i
		tl63Literal += "]:"
		tl63Literal += ENUM_TO_INT(sRaceData.eHorsesRacing[i])
		tl63Literal += ", "
		tl63Literal += sRaceData.iHorseTimes[i]
		SET_TEXT_SCALE(0.30, 0.30)
		SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
		DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
				cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
				"STRING", tl63Literal) iRow++
	ENDREPEAT
	
	//
	tl63Literal  = "iWinningSegment:"
	tl63Literal += sRaceData.iWinningSegment
	SET_TEXT_SCALE(0.30, 0.30)
	SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
	DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
			cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
			"STRING", tl63Literal) iRow++
	
	//
	tl63Literal  = "iSeed:"
	tl63Literal += sRaceData.iSeed
	SET_TEXT_SCALE(0.30, 0.30)
	SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
	DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
			cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
			"STRING", tl63Literal) iRow++
	
	//
	tl63Literal  = "iDuration:"
	tl63Literal += sRaceData.iDuration
	SET_TEXT_SCALE(0.30, 0.30)
	SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
	DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
			cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
			"STRING", tl63Literal) iRow++
	
	//
	tl63Literal  = "iPositionList:"
	tl63Literal += sRaceData.iPositionList
	SET_TEXT_SCALE(0.30, 0.30)
	SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
	DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
			cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
			"STRING", tl63Literal) iRow++
	
	//
	REPEAT INSIDE_TRACK_RACE_TIMER_COUNT i
		IF IS_TIMER_STARTED(sRaceData.stTimer[i])
			tl63Literal  = ""
			SWITCH INT_TO_ENUM(INSIDE_TRACK_RACE_TIMER_TYPE, i)
				CASE INSIDE_TRACK_RACE_TIMER_RaceInsideTrack		tl63Literal  = "RaceInsideTrack" BREAK
				CASE INSIDE_TRACK_RACE_TIMER_RaceUpdatePositions	tl63Literal  = "RaceUpdatePositions" BREAK
				CASE INSIDE_TRACK_RACE_TIMER_RaceToStart			tl63Literal  = "RaceToStart" BREAK
				CASE INSIDE_TRACK_RACE_TIMER_ResultsScreen			tl63Literal  = "ResultsScreen" BREAK
				CASE INSIDE_TRACK_RACE_TIMER_RaceCountdown			tl63Literal  = "RaceCountdown" BREAK
				
				DEFAULT
					tl63Literal  = "stTimer["
					tl63Literal += i
					tl63Literal += "]"
				BREAK
			ENDSWITCH
			tl63Literal += ":"
			tl63Literal += GET_STRING_FROM_FLOAT(GET_TIMER_IN_SECONDS(sRaceData.stTimer[i]))
			SET_TEXT_SCALE(0.30, 0.30)
			SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
			DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
					cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
					"STRING", tl63Literal) iRow++
		ENDIF
	ENDREPEAT
ENDPROC

PROC UPDATE_CASINO_FM_DEBUG_WIDGETS(CASINO_DEBUG_WIDGET_DATA &sData)
	
	//Take a screenshot of the display
	IF sData.bDebugDisplayTakeSC != sData.bDebugDrawTrackingData
		sData.bDebugDisplayTakeSC = sData.bDebugDrawTrackingData	
		SAVE_CASINO_DEBUG_TIME_TRACKING_SCREENSHOT()
	ENDIF
	
	IF sData.bDebugDrawTrackingData
		DRAW_CASINO_GAMBLING_DATA(g_fCasinoDebugDisplayPosX, g_fCasinoDebugDisplayPosY)
	ENDIF
	
	IF sData.bEnableDisplayRepositioning
		IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			IF NOT IS_PC_VERSION()
				GET_MOUSE_POSITION(g_fCasinoDebugDisplayPosX, g_fCasinoDebugDisplayPosY)
			ELSE
				g_fCasinoDebugDisplayPosX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
				g_fCasinoDebugDisplayPosY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
			ENDIF
		ENDIF
	ENDIF
	
	IF sData.bResetDisplayPosition
		g_fCasinoDebugDisplayPosX 	= -1.0
		g_fCasinoDebugDisplayPosY 	= -1.0
		sData.bResetDisplayPosition = FALSE
	ENDIF
	
	IF sData.bAddChips
		IF ADD_TO_LOCAL_PLAYERS_CASINO_CHIPS(sData.iChipsToAddRemove, CCU_DEBUG_ADD, sData.eBuyTransactionState)
			sData.bAddChips = FALSE
			sData.eBuyTransactionState = TRANSACTION_STATE_DEFAULT
		ENDIF
	ENDIF
	
	IF sData.bRemoveChips
		IF REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER(sData.iChipsToAddRemove, CCU_DEBUG_REMOVE, sData.eSellTransactionState)
			sData.bRemoveChips = FALSE
			sData.eSellTransactionState = TRANSACTION_STATE_DEFAULT
		ENDIF
	ENDIF
	
	IF sData.bDisplayChips
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.85, "STRING", GET_STRING_FROM_INT(g_iLocalPlayerCasinoChips))
	ENDIF
	
	IF sData.bSimulateTrackDayEnd
		PRINTLN("[GAMBLING_TIME][DEBUG] bSimulateTrackDayEnd")
		INT iStat 					= GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_GMBLNG_GD)
		INT iCurrentDayElapsedTime	= GET_TIME_THROUGH_CURRENT_GAMBLING_TRACK_DAY()
		INT iTimeUntilEOD 			= (SECONDS_PER_DAY - iCurrentDayElapsedTime)
		
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_GMBLNG_GD, iStat - (iTimeUntilEOD - sData.iTimeToSimulateUntilEOD))
		SET_CASINO_GAMBLING_EOD_POSIX((GET_CLOUD_TIME_AS_INT() + sData.iTimeToSimulateUntilEOD))
		sData.bSimulateTrackDayEnd = FALSE
	ENDIF
	
	IF sData.bBanPlayer
		PRINTLN("[GAMBLING_TIME][DEBUG] bBanPlayer")
		IF NOT IS_LP_BANNED_FROM_BETTING_DUE_TO_TIME_SPENT_GAMBLING()
			//Ban the player for one week
			CLEANUP_ALL_CASINO_TIME_GAMBLING_STATS(TRUE)
			SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_BAN_TIME, GET_CLOUD_TIME_AS_INT())
			g_iGamblingBanTime = GET_CLOUD_TIME_AS_INT()
			
			//Request a save
			g_b_CasinoPrioritySave = TRUE
		ENDIF
		sData.bBanPlayer = FALSE
	ENDIF
	
	IF sData.bReduceGamblingBan
		PRINTLN("[GAMBLING_TIME][DEBUG] bReduceGamblingBan")
		IF IS_LP_BANNED_FROM_BETTING_DUE_TO_TIME_SPENT_GAMBLING()
			INT iTime = (GET_CLOUD_TIME_AS_INT() - SEVEN_DAYS_IN_SECONDS) + 60
			
			IF iTime > 60
				SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_BAN_TIME, iTime)
				g_iGamblingBanTime = iTime			
				//Request a save
				g_b_CasinoPrioritySave = TRUE
			ENDIF
		ENDIF
		
		sData.bReduceGamblingBan = FALSE
	ENDIF
	
	IF sData.bResetRestrictions
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_PURTIM, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_PUR_GD, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WONTIM, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_CHIPS_WON_GD, 0)
		
		PRINTLN("[CASINO_CHIPS][DEBUG] UPDATE_CASINO_FM_DEBUG_WIDGETS - Reset chip restricitons")
		
		sData.bResetRestrictions = FALSE
	ENDIF
	
	//Playing time tracking
	IF sData.bStartGamblingTracking
		PRINTLN("[GAMBLING_TIME][DEBUG] bStartGamblingTracking")
		IF NOT IS_PLAYER_GAMBLING_IN_THE_CASINO()
			SET_PLAYER_IS_PLAYING_CASINO_GAME(CG_DEBUG)
		ENDIF
		sData.bStartGamblingTracking = FALSE
	ENDIF
	
	IF sData.bStopGamblingTracking
		PRINTLN("[GAMBLING_TIME][DEBUG] bStopGamblingTracking")
		IF IS_PLAYER_GAMBLING_IN_THE_CASINO()
			SET_PLAYER_FINISHED_PLAYING_CASINO_GAME()
		ENDIF
		sData.bStopGamblingTracking = FALSE
	ENDIF
	
	IF sData.bAddMinutesToTime
	OR sData.bAddMinutesToTimeGMBL
		PRINTLN("[GAMBLING_TIME][DEBUG] bAddMinutesToTime")
		INT iTrackingDay = GET_CURRENT_GAMBLING_TRACK_DAY()
		
		IF (GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY(iTrackingDay, sData.bAddMinutesToTimeGMBL) + sData.iAddMinutesToTime) < MINUTES_PER_DAY
			GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY(iTrackingDay, sData.iAddMinutesToTime, sData.bAddMinutesToTimeGMBL)
			
			IF sData.bAddMinutesToTimeGMBL
				g_iTotalGamblingTime += sData.iAddMinutesToTime
			ELSE
				g_iTotalGTAOTime += sData.iAddMinutesToTime
			ENDIF
		ELSE
			INT iTimeToAdd = (MINUTES_PER_DAY - GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY(iTrackingDay, sData.bAddMinutesToTimeGMBL))
			
			IF iTimeToAdd > 0
				GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY(iTrackingDay, iTimeToAdd, sData.bAddMinutesToTimeGMBL)
				
				IF sData.bAddMinutesToTimeGMBL
					g_iTotalGamblingTime += iTimeToAdd
				ELSE
					g_iTotalGTAOTime += iTimeToAdd
				ENDIF
			ENDIF
		ENDIF
		
		IF sData.bAddMinutesToTimeGMBL
			IF (GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY(iTrackingDay, FALSE) + sData.iAddMinutesToTime) < MINUTES_PER_DAY
				GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY(iTrackingDay, sData.iAddMinutesToTime, FALSE)
				g_iTotalGTAOTime += sData.iAddMinutesToTime
			ELSE
				INT iTimeToAdd = (MINUTES_PER_DAY - GAMBLING_TIME_GET_TIME_SPENT_FOR_DAY(iTrackingDay, FALSE))
				
				IF iTimeToAdd > 0
					GAMBLING_TIME_ADD_TO_TIME_PLAYING_TODAY(iTrackingDay, iTimeToAdd, FALSE)
					g_iTotalGTAOTime += iTimeToAdd
				ENDIF
			ENDIF
		ENDIF
		
		sData.bAddMinutesToTimeGMBL 	= FALSE
		sData.bAddMinutesToTime 		= FALSE
	ENDIF
	
	IF sData.bMoveGamblingTrackingToNextDay
		INT iCurrentStat = GET_MP_INT_PLAYER_STAT(MPPLY_CASINO_GMBLNG_GD)	
		PRINTLN("[GAMBLING_TIME][DEBUG] bMoveGamblingTrackingToNextDay - Initial time moved back ", SECONDS_PER_DAY, " seconds. New value = ", (iCurrentStat - SECONDS_PER_DAY))
		SET_MP_INT_PLAYER_STAT(MPPLY_CASINO_GMBLNG_GD, iCurrentStat - SECONDS_PER_DAY)
		SET_CURRENT_GAMBLING_TRACK_DAY(GET_CURRENT_GAMBLING_TRACK_DAY() + 1)
		ADD_TO_NUM_GAMBLING_DAYS_TRACKED_IN_CURRENT_RUN(1)
		sData.bMoveGamblingTrackingToNextDay = FALSE
	ENDIF
	
	IF sData.bResetGamblingTimeTracking
		PRINTLN("[GAMBLING_TIME][DEBUG] bResetGamblingTimeTracking")
		CLEANUP_ALL_CASINO_TIME_GAMBLING_STATS(TRUE)
		sData.bResetGamblingTimeTracking = FALSE
	ENDIF
	
	//Inside track
	CONST_FLOAT cfDISPLAY_AT_X		0.850	//0.85
	CONST_FLOAT cfDISPLAY_AT_Y_A	0.150	//0.75
	CONST_FLOAT cfDISPLAY_AT_Y_B	0.018	//0.02
	INT iRow = 0
	IF sData.bDrawDebugInsideTrackBet
		INT red, green, blue, alpha_value
		FLOAT fAlphaMult = 1.0
		TEXT_LABEL_63 tl63Literal = ""
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_INSIDETRACK_BET_HORSEID) > 0
			GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, red, green, blue, alpha_value)
		ELSE
			GET_HUD_COLOUR(HUD_COLOUR_GREYLIGHT, red, green, blue, alpha_value)
			alpha_value = ROUND(TO_FLOAT(alpha_value)*0.5)
		ENDIF
		
		//
		tl63Literal  = "HORSEID:"
		tl63Literal += GET_MP_INT_CHARACTER_STAT(MP_STAT_INSIDETRACK_BET_HORSEID)
		tl63Literal += " ("
		tl63Literal += GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCasinoPropertyData.bdMainEventBet.iSelection
		tl63Literal += " \""
		tl63Literal += GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCasinoPropertyData.bdMainEventBet.tl63Gamertag
		tl63Literal += "\")"
//		tl63Literal += ", RESULT:"
//		tl63Literal += GET_MP_INT_CHARACTER_STAT(MP_STAT_INSIDETRACK_BET_useless)
		SET_TEXT_SCALE(0.30, 0.30)
		SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
		DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
				cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
				"STRING", tl63Literal) iRow++
		
		//
		tl63Literal  = "CHIPS:"
		tl63Literal += GET_MP_INT_CHARACTER_STAT(MP_STAT_INSIDETRACK_BET_CHIPS)
		tl63Literal += " ("
		tl63Literal += GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCasinoPropertyData.bdMainEventBet.iStake
		tl63Literal += ")"
		SET_TEXT_SCALE(0.30, 0.30)
		SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
		DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
				cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
				"STRING", tl63Literal) iRow++
		
//		//
//		tl63Literal  = "FINISHTIME:"
//		tl63Literal += GET_MP_INT_CHARACTER_STAT(MP_STAT_INSIDETRACK_BET_useless)
//		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_INSIDETRACK_BET_useless) != 0
//			tl63Literal += " ("
//			tl63Literal += GET_STRING_FROM_MILISECONDS(GET_MP_INT_CHARACTER_STAT(MP_STAT_INSIDETRACK_BET_useless) - GET_GAME_TIMER())
//			tl63Literal += ")"
//		ELSE
//			//
//		ENDIF
//		SET_TEXT_SCALE(0.30, 0.30)
//		SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
//		DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
//				cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
//				"STRING", tl63Literal) iRow++
		
		//
		tl63Literal  = "GAMESEED:"
		tl63Literal += GET_MP_INT_CHARACTER_STAT(MP_STAT_INSIDETRACK_BET_GAMESEED)
		tl63Literal += " ("
		tl63Literal += g_iInsideTrackMainEventSeed
		tl63Literal += ")"
		SET_TEXT_SCALE(0.30, 0.30)
		SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
		DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
				cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
				"STRING", tl63Literal) iRow++
		
		//
		tl63Literal  = "TelemetryPlayersAtTable:"
		tl63Literal += g_iInsideTrackTelemetryPlayersAtTable
		SET_TEXT_SCALE(0.30, 0.30)
		SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
		DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
				cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
				"STRING", tl63Literal) iRow++
		
		//
		tl63Literal  = "TelemetryTableID:"
		tl63Literal += g_iInsideTrackTelemetryTableID
		SET_TEXT_SCALE(0.30, 0.30)
		SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
		DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
				cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
				"STRING", tl63Literal) iRow++
		
		//
		tl63Literal  = "TelemetryDurationGameTime:"
		tl63Literal += g_iInsideTrackTelemetryDurationGameTime
		SET_TEXT_SCALE(0.30, 0.30)
		SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(alpha_value) * fAlphaMult))
		DISPLAY_TEXT_WITH_LITERAL_STRING(cfDISPLAY_AT_X,
				cfDISPLAY_AT_Y_A+(cfDISPLAY_AT_Y_B*TO_FLOAT(iRow)),
				"STRING", tl63Literal) iRow++
	ENDIF
	IF sData.bDrawDebugInsideTrackRewardMainEvent
		DRAW_INSIDE_TRACK_RACE_DATA(g_rdTriggerRaceFinishData, HUD_COLOUR_BLUE, iRow, cfDISPLAY_AT_X, cfDISPLAY_AT_Y_A, cfDISPLAY_AT_Y_B)
	ENDIF
ENDPROC
#ENDIF //Debug build


#ENDIF	//Feature casino
