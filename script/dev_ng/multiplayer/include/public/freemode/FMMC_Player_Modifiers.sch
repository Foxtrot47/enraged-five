USING "FMMC_Player_Modifier_Vars.sch"
USING "FMMC_Player_Inventory.sch"
USING "FMMC_Shared_Common.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Modifier Context Help Text
// ##### Description: Functions for contextual help text for the player modifier system
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(INT iContext)
	SET_BIT(iContextHelpTextRequiredBS, iContext)
ENDPROC

FUNC STRING GET_TEXT_LABEL_FOR_PLAYER_MODIFIER_HELP_TEXT(INT iContext)
	SWITCH iContext
		CASE ciPLAYER_MODIFIER_HELP_NO_HEADSHOTS 		RETURN "PM_CTH_NH"
		CASE ciPLAYER_MODIFIER_HELP_NO_HEADSHOTS_VIC 	RETURN "PM_CTH_NHV"
		CASE ciPLAYER_MODIFIER_HELP_ON_KILL_EFFECT		RETURN "PM_CTH_OK"
		CASE ciPLAYER_MODIFIER_HELP_WEP_DMG_CHANGED 	RETURN "PM_CTH_WDM"
		CASE ciPLAYER_MODIFIER_HELP_HEALTH_CHANGED		RETURN "PM_CTH_HSC"
		CASE ciPLAYER_MODIFIER_HELP_HANDBRAKE_DISABLED 	RETURN "DM_DISACT_2HLP"
		CASE ciPLAYER_MODIFIER_HELP_FRIENDLY_FIRE_ON	RETURN "PM_CTH_FFE"
		CASE ciPLAYER_MODIFIER_HELP_MOD_SET_ON_SPAWN	RETURN "PM_CTH_MSOS"
		CASE ciPLAYER_MODIFIER_HELP_BLOCK_VEH_ENTER		RETURN "PM_CTH_DENT"
		CASE ciPLAYER_MODIFIER_HELP_BLOCK_VEH_EXIT		RETURN "PM_CTH_DEXT"
		CASE ciPLAYER_MODIFIER_HELP_FRIENDLY_SCORE		RETURN "PM_CTH_FFS"
		CASE ciPLAYER_MODIFIER_HELP_OOB_SCORE			RETURN "PM_CTH_OOBS"
		CASE ciPLAYER_MODIFIER_HELP_SUICIDE_SCORE		RETURN "PM_CTH_SUIS"
		CASE ciPLAYER_MODIFIER_HELP_HIGH_HEALTH			RETURN "PM_CTH_HH"
		CASE ciPLAYER_MODIFIER_HELP_HIGH_HEALTH_VIC		RETURN "PM_CTH_HHV"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL HAS_PLAYER_MODIFIER_CONTEXT_HELP_PREREQ_BEEN_COMPLETED(INT iContext)
	
	SWITCH iContext
		CASE ciPLAYER_MODIFIER_HELP_HANDBRAKE_DISABLED
			IF NOT IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
				RETURN FALSE
			ENDIF
		BREAK
		CASE ciPLAYER_MODIFIER_HELP_BLOCK_VEH_ENTER
			IF NOT IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				RETURN FALSE
			ENDIF
		BREAK
		CASE ciPLAYER_MODIFIER_HELP_BLOCK_VEH_EXIT
			IF NOT IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN TRUE
		
ENDFUNC

PROC PROCESS_PLAYER_MODIFIER_CONTEXT_HELP_TEXT()
	
	INT iContext
	FOR iContext = 0 TO ciPLAYER_MODIFIER_HELP_MAX-1
		IF NOT IS_BIT_SET(iContextHelpTextShownBS, iContext)
		AND IS_BIT_SET(iContextHelpTextRequiredBS, iContext)
		AND HAS_PLAYER_MODIFIER_CONTEXT_HELP_PREREQ_BEEN_COMPLETED(iContext)
			TEXT_LABEL_15 tlContext = GET_TEXT_LABEL_FOR_PLAYER_MODIFIER_HELP_TEXT(iContext)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP(tlContext)
			ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlContext)
				SET_BIT(iContextHelpTextShownBS, iContext)
			ENDIF
		ENDIF
	ENDFOR
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Helpers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_CURRENT_MODIFIER_SET_VALID(INT iCurrentModSet)
	RETURN iCurrentModSet >= 0 AND iCurrentModSet < ciDM_MAX_MODIFIER_SETS
ENDFUNC

FUNC BOOL IS_WEAPON_GROUP_VALID(INT iWeaponGroup)
	RETURN iWeaponGroup >= 0 AND iWeaponGroup < ciDM_WEAPON_GROUP_MODIFIER_MAX
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Weapon Modifiers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_WEAPON_MODIFIER_ONE_HIT_KILL(INT iCurrentModSet, WEAPON_TYPE wtWeapon)
	INT iWeaponGroup = GET_WEAPON_GROUP(wtWeapon)
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModSet)
		PRINTLN("IS_WEAPON_MODIFIER_ONE_HIT_KILL - IS_CURRENT_MODIFIER_SET_VALID is not valid")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_WEAPON_GROUP_VALID(iWeaponGroup)
		PRINTLN("IS_WEAPON_GROUP_VALID - IS_CURRENT_MODIFIER_SET_VALID is not valid")
		RETURN FALSE
	ENDIF
	
	RETURN g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iWeaponGroupDamageModifier[iWeaponGroup] = ciDM_WEAPON_DAMAGE_MODIFIER_ONE_HIT
ENDFUNC

FUNC FLOAT GET_PLAYER_MODIFIERS_WEAPON_DAMAGE_FOR_WEAPON_TYPE(INT iCurrentModSet, WEAPON_TYPE wtWeapon)
	
	FLOAT fWeaponDamageModifier = 100.0
	
	INT iWeaponGroup = GET_WEAPON_GROUP(wtWeapon)
	IF IS_WEAPON_GROUP_VALID(iWeaponGroup)
		IF IS_WEAPON_MODIFIER_ONE_HIT_KILL(iCurrentModSet, wtWeapon)
			fWeaponDamageModifier = ciDM_WEAPON_DAMAGE_MODIFIER_ONE_HIT_SCALE
		ELSE
			fWeaponDamageModifier = TO_FLOAT(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iWeaponGroupDamageModifier[iWeaponGroup])
		ENDIF
	ENDIF
	
	RETURN fWeaponDamageModifier / 100
	
ENDFUNC

PROC SET_PLAYER_MODIFIER_WEAPON_DAMAGE_MODIFIER_HELP_TEXT_REQUIRED(FLOAT fModifier)
	IF fModifier = 1.0
		EXIT
	ENDIF
	SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_WEP_DMG_CHANGED)
ENDPROC

PROC PROCESS_PLAYER_MODIFIERS__WEAPON_DAMAGE(INT iCurrentModSet)
	
	INT iWeaponLoop
	PED_WEAPONS_MP_STRUCT sPedWeapons
	FLOAT fModifier
	GET_PED_WEAPONS_MP(PLAYER_PED_ID(), sPedWeapons)
	
	FOR iWeaponLoop = 0 TO NUM_PLAYER_PED_WEAPON_SLOTS-1
		IF sPedWeapons.sWeaponInfo[iWeaponLoop].eWeaponType = WEAPONTYPE_INVALID
			RELOOP
		ELSE
			fModifier = GET_PLAYER_MODIFIERS_WEAPON_DAMAGE_FOR_WEAPON_TYPE(iCurrentModSet, sPedWeapons.sWeaponInfo[iWeaponLoop].eWeaponType)
			PRINTLN("[Player Modifiers] PROCESS_PLAYER_MODIFIERS__WEAPON_DAMAGE - Setting non DLC weapon ", GET_WEAPON_NAME(sPedWeapons.sWeaponInfo[iWeaponLoop].eWeaponType), " modifier to ", fModifier)
			SET_WEAPON_DAMAGE_MODIFIER(sPedWeapons.sWeaponInfo[iWeaponLoop].eWeaponType, fModifier)
			SET_PLAYER_MODIFIER_WEAPON_DAMAGE_MODIFIER_HELP_TEXT_REQUIRED(fModifier)
		ENDIF
	ENDFOR
	
	FOR iWeaponLoop = 0 TO NUMBER_OF_DLC_WEAPONS_MP-1
		IF sPedWeapons.sDLCWeaponInfo[iWeaponLoop].eWeaponType = WEAPONTYPE_INVALID
			RELOOP
		ELSE
			fModifier = GET_PLAYER_MODIFIERS_WEAPON_DAMAGE_FOR_WEAPON_TYPE(iCurrentModSet, sPedWeapons.sDLCWeaponInfo[iWeaponLoop].eWeaponType)
			PRINTLN("[Player Modifiers] PROCESS_PLAYER_MODIFIERS__WEAPON_DAMAGE - Setting DLC weapon ", GET_WEAPON_NAME(sPedWeapons.sDLCWeaponInfo[iWeaponLoop].eWeaponType), " modifier to ", fModifier)
			SET_WEAPON_DAMAGE_MODIFIER(sPedWeapons.sDLCWeaponInfo[iWeaponLoop].eWeaponType, fModifier)
			SET_PLAYER_MODIFIER_WEAPON_DAMAGE_MODIFIER_HELP_TEXT_REQUIRED(fModifier)
		ENDIF
	ENDFOR
	
	FOR iWeaponLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
		WEAPON_TYPE ePickupWeapon = GET_WEAPON_TYPE_FROM_PICKUP_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponLoop].pt)
		IF IS_WEAPON_VALID(ePickupWeapon)
			fModifier = GET_PLAYER_MODIFIERS_WEAPON_DAMAGE_FOR_WEAPON_TYPE(iCurrentModSet, ePickupWeapon)
			PRINTLN("[Player Modifiers] PROCESS_PLAYER_MODIFIERS__WEAPON_DAMAGE - Setting pickup weapon ", GET_WEAPON_NAME(ePickupWeapon), " modifier to ", fModifier)
			SET_WEAPON_DAMAGE_MODIFIER(ePickupWeapon, fModifier)
			SET_PLAYER_MODIFIER_WEAPON_DAMAGE_MODIFIER_HELP_TEXT_REQUIRED(fModifier)
		ENDIF
	ENDFOR

ENDPROC

PROC PROCESS_PLAYER_MODIFIERS__INFINITE_WEAPON_AMMO(INT iCurrentModSet)
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModSet)
		EXIT
	ENDIF
	
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	
	SWITCH g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].eInfiniteAmmoType
		CASE DM_INFINITE_AMMO_OFF
			SET_PED_INFINITE_AMMO(piPlayerPed, FALSE)
			SET_PED_INFINITE_AMMO_CLIP(piPlayerPed, FALSE)
			PRINTLN("[Player Modifiers] PROCESS_PLAYER_MODIFIERS__INFINITE_WEAPON_AMMO - Removing any infinite ammo")
		BREAK
		CASE DM_INFINITE_AMMO_RELOAD
			SET_PED_INFINITE_AMMO(piPlayerPed, TRUE)
			SET_PED_INFINITE_AMMO_CLIP(piPlayerPed, FALSE)
			PRINTLN("[Player Modifiers] PROCESS_PLAYER_MODIFIERS__INFINITE_WEAPON_AMMO - Setting infinite ammo with reloads")
		BREAK
		CASE DM_INFINITE_AMMO_NO_RELOAD
			SET_PED_INFINITE_AMMO(piPlayerPed, FALSE)
			SET_PED_INFINITE_AMMO_CLIP(piPlayerPed, TRUE)
			PRINTLN("[Player Modifiers] PROCESS_PLAYER_MODIFIERS__INFINITE_WEAPON_AMMO - Setting infinite ammo with no reloads")
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_PLAYER_MODIFIERS__WEAPONS(INT iCurrentModSet)
	
	PROCESS_PLAYER_MODIFIERS__WEAPON_DAMAGE(iCurrentModSet)
	
	PROCESS_PLAYER_MODIFIERS__INFINITE_WEAPON_AMMO(iCurrentModSet)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Scoring modifiers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_CAP_INDIVIDUAL_TDM_SCORE_AT_ZERO(INT &iScore)
	
	IF g_bFM_ON_TEAM_DEATHMATCH
	AND (g_iMyDMScore + iScore) < 0
		iScore = -g_iMyDMScore
		PRINTLN("[SCR_MODS] PROCESS_CAP_INDIVIDUAL_TDM_SCORE_AT_ZERO - Capping player score at 0! Updated score: ", iScore)
	ENDIF
	
ENDPROC

PROC PROCESS_SCORING_MODIFIERS_ADD_FLOATING_SCORE(TARGET_FLOATING_SCORE &sTargetFloatingScores[], INT iScore)

	IF iScore != 0
		STRING stTextLabel = PICK_STRING((iScore > 0), "DM_IND_SCR", "NUMBER")
		TARGET_ADD_FLOATING_SCORE(iScore, GET_CENTER_OF_SCREEN(), HUD_COLOUR_WHITE, sTargetFloatingScores, stTextLabel, DEFAULT, DEFAULT, TARGET_SCORE_SCALE_SMALL, TARGET_SCORE_SPACING_LARGE, TARGET_SCORE_LIFETIME_LONG, TRUE)
	ENDIF
	
ENDPROC

FUNC INT GET_PLAYER_MODIFIERS_SCORING_WEAPON_GROUP_SCORE(INT iCurrentModset, WEAPON_TYPE wtWeapon)

	INT iWeaponGroupScore
	
	INT iWeaponGroup = GET_WEAPON_GROUP(wtWeapon)
	IF IS_WEAPON_GROUP_VALID(iWeaponGroup)
	
		// Melee scoring is handled seperately
		IF iWeaponGroup = ciDM_WEAPON_GROUP_MODIFIER_MELEE
			RETURN 0
		ENDIF
		
		iWeaponGroupScore = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierScoring.iSpecificWeaponGroup[iWeaponGroup]
		
	ENDIF
	
	RETURN iWeaponGroupScore
	
ENDFUNC

FUNC INT GET_PLAYER_MODIFIERS__SCORING_FRIENDLY_KILL(TARGET_FLOATING_SCORE &sTargetFloatingScores[], INT iCurrentModset)
	
	INT iTotalScore
		
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModset)
		PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_FRIENDLY_KILL - Invalid modset")
		RETURN iTotalScore
	ENDIF
	
	// Friendly kill score
	iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierScoring.iFriendlyKill
	PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_FRIENDLY_KILL - Friendly Kill. Total Score: ", iTotalScore)

	PROCESS_CAP_INDIVIDUAL_TDM_SCORE_AT_ZERO(iTotalScore)
	PROCESS_SCORING_MODIFIERS_ADD_FLOATING_SCORE(sTargetFloatingScores, iTotalScore)
	
	RETURN iTotalScore
	
ENDFUNC

FUNC INT GET_PLAYER_MODIFIERS__SCORING_SUICIDE(TARGET_FLOATING_SCORE &sTargetFloatingScores[], INT iCurrentModset)
	
	INT iTotalScore
		
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModset)
		PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_SUICIDE - Invalid modset")
		RETURN iTotalScore
	ENDIF
	
	// Suicide score
	iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierScoring.iSuicide
	PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_SUICIDE - Suicide. Total Score: ", iTotalScore)

	PROCESS_CAP_INDIVIDUAL_TDM_SCORE_AT_ZERO(iTotalScore)
	PROCESS_SCORING_MODIFIERS_ADD_FLOATING_SCORE(sTargetFloatingScores, iTotalScore)
	
	RETURN iTotalScore
	
ENDFUNC

FUNC INT GET_PLAYER_MODIFIERS__SCORING_OUT_OF_BOUNDS(TARGET_FLOATING_SCORE &sTargetFloatingScores[], INT iCurrentModset)

	INT iTotalScore
		
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModset)
		PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_OUT_OF_BOUNDS - Invalid modset")
		RETURN iTotalScore
	ENDIF
	
	// Out of bounds score
	iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierScoring.iOutOfBounds
	PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_OUT_OF_BOUNDS - Out Of Bounds. Total Score: ", iTotalScore)

	PROCESS_CAP_INDIVIDUAL_TDM_SCORE_AT_ZERO(iTotalScore)
	PROCESS_SCORING_MODIFIERS_ADD_FLOATING_SCORE(sTargetFloatingScores, iTotalScore)
	
	RETURN iTotalScore
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Inventory
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC GIVE_PLAYER_MODIFIER_INVENTORY_TO_LOCAL_PLAYER(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iCurrentModSet, BOOL bModSetChanged = FALSE)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierInventory.iInventoryBitset, ciFMMC_INVENTORY_BS_DM_MODSET_USING_INVENTORY)
		EXIT
	ENDIF
	
	PRINTLN("GIVE_PLAYER_MODIFIER_INVENTORY_TO_LOCAL_PLAYER - Giving mod set inventory to player")
	
	GIVE_PLAYER_FMMC_INVENTORY(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierInventory, WEAPONINHAND_LASTWEAPON_BOTH)
	SET_BIT(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_INVENTORY_GIVEN)
	SET_BIT(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_INVENTORY_GIVEN_NO_RESET)
	
	IF bModSetChanged
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_BIT_SET(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_INITIAL_MOD_SET_APPLICATION_DONE)
		FLOAT fR, fG, fB
		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_FRIENDLY, iR, iG, iB, iA)
		fR = TO_FLOAT(iR)/255.0
		fG = TO_FLOAT(iG)/255.0
		fB = TO_FLOAT(iB)/255.0
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(fR, fG, fB)
		SET_PARTICLE_FX_NON_LOOPED_ALPHA(0.8)
		USE_PARTICLE_FX_ASSET("scr_bike_adversary")
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_adversary_gunsmith_weap_change", PLAYER_PED_ID(), <<0, 0, 0.0>>, <<0, 0, 0>>, BONETAG_PH_R_HAND, 0.6)
		PLAY_SOUND_FRONTEND(-1, "Weapon_Force_Change_Smoke", "Deathmatch_Sounds")
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Movement Modifiers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PLAYER_MODIFIERS__MOVEMENT_RESTRICTION(INT iCurrentModset)

	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModSet)
		EXIT
	ENDIF
	
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	IF IS_PED_INJURED(piPlayerPed)
		EXIT
	ENDIF
	
	SWITCH g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierMovement.eMovementRestriction
		CASE DM_MOVEMENT_RESTRICTION__DEFAULT
			SET_PED_MAX_MOVE_BLEND_RATIO(piPlayerPed, PEDMOVEBLENDRATIO_SPRINT)
		BREAK
		CASE DM_MOVEMENT_RESTRICTION__BLOCK_SPRINT
			SET_PED_MAX_MOVE_BLEND_RATIO(piPlayerPed, PEDMOVEBLENDRATIO_RUN)
		BREAK
		CASE DM_MOVEMENT_RESTRICTION__BLOCK_RUN
			SET_PED_MAX_MOVE_BLEND_RATIO(piPlayerPed, PEDMOVEBLENDRATIO_WALK)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_PLAYER_MODIFIERS__MOVEMENT_SPEED(INT iCurrentModset)

	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModSet)
		EXIT
	ENDIF
	
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	IF IS_PED_INJURED(piPlayerPed)
		EXIT
	ENDIF
	
	FLOAT fMovementSpeed
	SWITCH g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierMovement.eMovementSpeed
		CASE DM_MOVEMENT_SPEED__SLOW
			fMovementSpeed = ciDM_MOVEMENT_SPEED_SLOW
		BREAK
		CASE DM_MOVEMENT_SPEED__DEFAULT
			fMovementSpeed = ciDM_MOVEMENT_SPEED_DEFAULT
		BREAK
		CASE DM_MOVEMENT_SPEED__FAST
			fMovementSpeed = ciDM_MOVEMENT_SPEED_FAST
		BREAK
		CASE DM_MOVEMENT_SPEED__LUDICROUS
			fMovementSpeed = ciDM_MOVEMENT_SPEED_LUDICROUS
		BREAK
	ENDSWITCH
	
	IF fMovementSpeed >= 0.0
		SET_PED_MOVE_RATE_OVERRIDE(piPlayerPed, fMovementSpeed)
		SET_PED_MOVE_RATE_IN_WATER_OVERRIDE(piPlayerPed, fMovementSpeed)
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_MODIFIERS__MOVEMENT_OPTIONS(INT iCurrentModset)
	
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	IF IS_PED_INJURED(piPlayerPed)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_InfiniteStamina)
		RESET_PLAYER_STAMINA(PLAYER_ID())
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_BlockClimbing)
		SET_PED_RESET_FLAG(piPlayerPed, PRF_DisablePlayerVaulting, TRUE)
		SET_PED_RESET_FLAG(piPlayerPed, PRF_DisablePlayerAutoVaulting, TRUE)
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_BlockJumping)
		SET_PED_RESET_FLAG(piPlayerPed, PRF_DisablePlayerJumping, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_BlockCombatRoll)
		SET_PED_RESET_FLAG(piPlayerPed, PRF_DisablePlayerCombatRoll, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_BlockActionMode)
		SET_PED_RESET_FLAG(piPlayerPed, PRF_DisableActionMode, TRUE)
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_MODIFIERS__MOVEMENT(INT iCurrentModSet)
	
	PROCESS_PLAYER_MODIFIERS__MOVEMENT_RESTRICTION(iCurrentModset)
	
	PROCESS_PLAYER_MODIFIERS__MOVEMENT_SPEED(iCurrentModset)
	
	PROCESS_PLAYER_MODIFIERS__MOVEMENT_OPTIONS(iCurrentModset)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Health Regen
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC FLOAT GET_HEALTH_MAX_REGEN_PERCENT(INT iCurrentModSet)
	
	SWITCH g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].eHealthMaxRegen
		CASE DM_HEALTH_MAX_REGEN_25	RETURN 0.25
		CASE DM_HEALTH_MAX_REGEN_50	RETURN 0.5
		CASE DM_HEALTH_MAX_REGEN_75	RETURN 0.75
		CASE DM_HEALTH_MAX_REGEN_100 RETURN 1.0
	ENDSWITCH
	
	RETURN 0.5
	
ENDFUNC

FUNC FLOAT GET_HEALTH_REGEN_SPEED(INT iCurrentModSet)

	SWITCH g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].eHealthRegenSpeed
		CASE DM_HEALTH_REGEN_SPEED_VERY_SLOW RETURN 0.25
		CASE DM_HEALTH_REGEN_SPEED_SLOW RETURN 0.5
		CASE DM_HEALTH_REGEN_SPEED_DEFAULT RETURN 1.0
		CASE DM_HEALTH_REGEN_SPEED_FAST RETURN 1.5
		CASE DM_HEALTH_REGEN_SPEED_VERY_FAST RETURN 2.0
	ENDSWITCH
	
	RETURN 1.0

ENDFUNC

PROC PROCESS_PLAYER_MODIFIERS__HEALTH_REGEN(INT iCurrentModSet)
		
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModSet)
		EXIT
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_BlockHealthRegen)
		SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(PLAYER_ID(), 0.0)
		SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), 0.0)
		EXIT
	ENDIF

	SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(PLAYER_ID(), GET_HEALTH_MAX_REGEN_PERCENT(iCurrentModSet))
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), GET_HEALTH_REGEN_SPEED(iCurrentModSet))

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: On Kill
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Any changes to this will need to be reflected in PROCESS_CUSTOM_DM_GIVE_LIVES_TEST_MODE
PROC PROCESS_CUSTOM_DM_GIVE_LIVES(INT &iExtraLives, INT iCurrentModset, TARGET_FLOATING_SCORE &sTargetFloatingScores[])
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModset)
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].iModifierSetBS, ciDM_ModifierBS_GiveLives)
		EXIT
	ENDIF
	
	INT iTeam = GET_PLAYER_TEAM(PLAYER_ID())
	INT iAdditionalLives = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierOnKill.iExtraLives

	IF IS_TEAM_DEATHMATCH()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_SharedTeamLives)
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_CUSTOM_DM_GIVE_LIVES - Broadcasting give team lives event!")
		BROADCAST_DM_GIVE_TEAM_LIVES(iTeam, iAdditionalLives)
	ELSE		
		iExtraLives += iAdditionalLives
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_CUSTOM_DM_GIVE_LIVES - Player ", PARTICIPANT_ID_TO_INT(), " recevied extra lives! Additional lives: ", iAdditionalLives, " | Total Extra Lives: ", iExtraLives)
	ENDIF
	
	SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_ON_KILL_EFFECT)
	
//	url:bugstar:7776049
	UNUSED_PARAMETER(sTargetFloatingScores)	
//	TARGET_ADD_FLOATING_SCORE(iAdditionalLives, GET_CENTER_OF_SCREEN(), HUD_COLOUR_GREENLIGHT, sTargetFloatingScores, "DM_IND_LIV", DEFAULT, DEFAULT, TARGET_SCORE_SCALE_SMALL, TARGET_SCORE_SPACING_LARGE, TARGET_SCORE_LIFETIME_LONG, TRUE)
	
ENDPROC

PROC PROCESS_CUSTOM_DM_GIVE_HEALTH(FLOAT fDamage, INT iCurrentModset, TARGET_FLOATING_SCORE &sTargetFloatingScores[], BOOL bKillEvent = TRUE)
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModset)
		EXIT
	ENDIF
	
	INT iSelection
	IF bKillEvent
		iSelection = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierOnKill.iExtraHealth
	ELSE
		iSelection = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierOnHit.iExtraHealth
	ENDIF
	
	INT iMaxHealth = GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID())
	INT iExtraHealth = GET_DM_EXTRA_HEALTH_FROM_SELECTION(fDamage, iMaxHealth, iSelection)
		
	IF iExtraHealth > 0
		
		ADD_HEALTH_TO_PLAYER(iExtraHealth)
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_CUSTOM_DM_GIVE_HEALTH - Player ", NETWORK_PLAYER_ID_TO_INT(), " received extra health! | iExtraHealth: ", iExtraHealth, " | iTotalHealth: ", GET_ENTITY_HEALTH(PLAYER_PED_ID()))
		
		IF bKillEvent
//			url:bugstar:7776049
			UNUSED_PARAMETER(sTargetFloatingScores)
//  		TARGET_ADD_FLOATING_SCORE(0, GET_CENTER_OF_SCREEN(), HUD_COLOUR_REDLIGHT, sTargetFloatingScores, "DM_IND_HLT", "MPHud", "mp_anim_ammo", TARGET_SCORE_SCALE_SMALL, TARGET_SCORE_SPACING_LARGE, TARGET_SCORE_LIFETIME_LONG, TRUE)
			SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_ON_KILL_EFFECT)
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_CUSTOM_DM_GIVE_ARMOUR(INT iCurrentModset, TARGET_FLOATING_SCORE &sTargetFloatingScores[], BOOL bKillEvent = TRUE)

	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModset)
		EXIT
	ENDIF
	
	INT iMaxArmour = GET_PLAYER_MAX_ARMOUR(PLAYER_ID())
	INT iExtraArmour
	IF bKillEvent
		iExtraArmour = ROUND(iMaxArmour * (g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierOnKill.iExtraArmour / 10.0))
	ELSE
		iExtraArmour = ROUND(iMaxArmour * (g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierOnHit.iExtraArmour / 10.0))
	ENDIF
	
	IF iExtraArmour > 0
	
		ADD_ARMOUR_TO_PED(PLAYER_PED_ID(), iExtraArmour)
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_CUSTOM_DM_GIVE_ARMOUR - Player ", NETWORK_PLAYER_ID_TO_INT(), " received extra armour! | iExtraArmour: ", iExtraArmour, " | iTotalArmour: ", GET_PED_ARMOUR(PLAYER_PED_ID()))
		
		IF bKillEvent
//			url:bugstar:7776049
			UNUSED_PARAMETER(sTargetFloatingScores)
//			TARGET_ADD_FLOATING_SCORE(0, GET_CENTER_OF_SCREEN(), HUD_COLOUR_BLUELIGHT, sTargetFloatingScores, "DM_IND_ARM", "MPHud", "mp_anim_ammo", TARGET_SCORE_SCALE_SMALL, TARGET_SCORE_SPACING_LARGE, TARGET_SCORE_LIFETIME_LONG, TRUE)
			SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_ON_KILL_EFFECT)
		ENDIF
		
	ENDIF

ENDPROC

PROC PROCESS_CUSTOM_DM_GIVE_AMMO(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iCurrentModSet, TARGET_FLOATING_SCORE &sTargetFloatingScores[], BOOL bKillEvent = TRUE)

	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModset)
		EXIT
	ENDIF

	DM_AMMO_TYPE eAmmoType
	INT iWeaponGroup, iAmmoQuantity
	
	IF bKillEvent
		eAmmoType = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierOnKill.eAmmoType
		iWeaponGroup = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierOnKill.iGiveAmmoTo
		iAmmoQuantity = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierOnKill.iAmmoQuantity
	ELSE
		eAmmoType = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierOnHit.eAmmoType
		iWeaponGroup = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierOnHit.iGiveAmmoTo
		iAmmoQuantity = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierOnHit.iAmmoQuantity
	ENDIF
	
	IF iWeaponGroup = ciDM_GIVE_AMMO_TO_OFF
		EXIT
	ENDIF
		
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	WEAPON_TYPE wtWeapon = GET_PEDS_CURRENT_WEAPON(piPlayerPed)
	IF GET_AMMO_IN_PED_WEAPON(piPlayerPed, wtWeapon) = 0
		START_NET_TIMER(sPlayerModifierData.stBlockFiringTimer)
	ENDIF
	
	BOOL bBullets = FALSE
	IF eAmmoType = DM_AMMO_TYPE__BULLETS
		bBullets = TRUE
	ENDIF
		
	PROCESS_GIVE_PLAYER_AMMO(iAmmoQuantity, iWeaponGroup, bBullets)
	
	IF bKillEvent
//		url:bugstar:7776049
		UNUSED_PARAMETER(sTargetFloatingScores)
//		TARGET_ADD_FLOATING_SCORE(iAmmoQuantity, GET_CENTER_OF_SCREEN(), HUD_COLOUR_YELLOWLIGHT, sTargetFloatingScores, DEFAULT, "MPHud", "mp_anim_ammo", TARGET_SCORE_SCALE_SMALL, TARGET_SCORE_SPACING_LARGE, TARGET_SCORE_LIFETIME_LONG, TRUE)
		SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_ON_KILL_EFFECT)
	ENDIF
	
ENDPROC

PROC PROCESS_CUSTOM_DM_BLOCK_FIRING(PLAYER_MODIFIER_DATA &sPlayerModifierData)

	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_BlockWeaponFire, TRUE)
	
	WEAPON_TYPE wtWeapon = GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())
	IF NOT CAN_AMMO_BE_AWARDED_TO_WEAPON(wtWeapon)
		RESET_NET_TIMER(sPlayerModifierData.stBlockFiringTimer)
		PRINTLN("PROCESS_CUSTOM_DM_BLOCK_FIRING - Invalid weapon! Exiting function. Weapon: ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(wtWeapon))
		EXIT
	ENDIF
	
	FLOAT iBlockTime = GET_WEAPON_TIME_BETWEEN_SHOTS(wtWeapon) * 1000
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sPlayerModifierData.stBlockFiringTimer, CEIL(iBlockTime))
		
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_BlockWeaponFire, FALSE)
		RESET_NET_TIMER(sPlayerModifierData.stBlockFiringTimer)
		
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Defensive Modifiers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_STARTING_HEALTH_AMOUNT(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iCurrentModSet, PED_INDEX piPlayerPed)
	
	FLOAT fHealth = TO_FLOAT(GET_PED_MAX_HEALTH(piPlayerPed)) - 100
	
	IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iStartingHealthPercentage != ciDM_HEALTH_MODIFIER_DEFAULT
		FLOAT fMultiplier = TO_FLOAT(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iStartingHealthPercentage) / 100
		fHealth *= fMultiplier
	ELSE
		IF IS_BIT_SET(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_INITIAL_MOD_SET_APPLICATION_DONE)
		AND NOT IS_PED_INJURED(piPlayerPed)
			IF GET_PED_MAX_HEALTH(piPlayerPed) != GET_ENTITY_HEALTH(piPlayerPed)
				PRINTLN("SET_STARTING_HEALTH_AMOUNT - Not changing health as starting health percentage is default and not at maximum health!")
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	fHealth += 100
	
	SET_ENTITY_HEALTH(piPlayerPed, ROUND(fHealth))
	
ENDPROC

PROC PROCESS_PLAYER_MODIFIER__SET_PLAYER_MAX_HEALTH(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iCurrentModSet, PED_INDEX piPlayerPed)
	
	IF sPlayerModifierData.iStartingHealth = -1
		sPlayerModifierData.iStartingHealth = GET_PED_MAX_HEALTH(piPlayerPed)
	ENDIF
	
	PRINTLN("PROCESS_PLAYER_MODIFIERS__COMBAT_MODIFIERS - Starting health: ", sPlayerModifierData.iStartingHealth)
	
	IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iHealthScale != ciDM_HEALTH_MODIFIER_DEFAULT
		PRINTLN("PROCESS_PLAYER_MODIFIERS__COMBAT_MODIFIERS - g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[",iCurrentModSet,"].iHealthScale: ", g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iHealthScale)
		FLOAT fHealth = TO_FLOAT(sPlayerModifierData.iStartingHealth-100)
		FLOAT fMultiplier = TO_FLOAT(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iHealthScale) / 100
		fHealth *= fMultiplier
		fHealth += 100.0
		
		SET_PED_MAX_HEALTH(piPlayerPed, ROUND(fHealth))
		SET_STARTING_HEALTH_AMOUNT(sPlayerModifierData, iCurrentModSet, piPlayerPed)
		PRINTLN("PROCESS_PLAYER_MODIFIERS__COMBAT_MODIFIERS - player max health is now: ", GET_PED_MAX_HEALTH(piPlayerPed))
		
		SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_HEALTH_CHANGED)
		SET_BIT(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_MAX_HEALTH_CHANGED)
	ELSE
		IF sPlayerModifierData.iStartingHealth != -1
			SET_PED_MAX_HEALTH(piPlayerPed, sPlayerModifierData.iStartingHealth)
			SET_STARTING_HEALTH_AMOUNT(sPlayerModifierData, iCurrentModSet, piPlayerPed)
		ENDIF		
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SET_PED_HEALTH_PENDING_LAST_DAMAGE_EVENT_OVERRIDE_FLAG(TRUE)
	ENDIF
ENDPROC

FUNC FLOAT GET_EXPLOSION_RESISTANCE_MULTIPLIER(INT iModSet)
	
	SWITCH g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSet].eExplosiveResistance
		CASE DM_EXPR_VERY_HIGH	RETURN 0.1
		CASE DM_EXPR_HIGH		RETURN 0.25
		CASE DM_EXPR_MEDIUM		RETURN 0.5
		CASE DM_EXPR_LOW		RETURN 0.75
	ENDSWITCH
	
	RETURN 1.0
ENDFUNC

PROC PROCESS_PLAYER_MODIFIER__EXPLOSION_RESISTANCE(INT iModSet)
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iModSet)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSet].eExplosiveResistance = DM_EXPR_OFF
		PRINTLN("PROCESS_PLAYER_MODIFIER__EXPLOSION_RESISTANCE - No explosion resistance")
		SET_PLAYER_MAX_EXPLOSIVE_DAMAGE(PLAYER_ID())
		EXIT
	ENDIF
	
	FLOAT fMaxExplosiveDamage = GET_PED_MAX_HEALTH(PLAYER_PED_ID()) * GET_EXPLOSION_RESISTANCE_MULTIPLIER(iModSet)
	
	SET_PLAYER_MAX_EXPLOSIVE_DAMAGE(PLAYER_ID(), fMaxExplosiveDamage)
	PRINTLN("PROCESS_PLAYER_MODIFIER__EXPLOSION_RESISTANCE - Set to ", fMaxExplosiveDamage)
	
ENDPROC

PROC PROCESS_PLAYER_MODIFIERS__COMBAT_MODIFIERS(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iCurrentModSet)
	
	PED_INDEX piPlayerPed = PLAYER_PED_ID()

	PROCESS_PLAYER_MODIFIER__SET_PLAYER_MAX_HEALTH(sPlayerModifierData, iCurrentModSet, piPlayerPed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_BlockHeadshots)
		SET_PED_SUFFERS_CRITICAL_HITS(piPlayerPed, FALSE)
		PRINTLN("PROCESS_PLAYER_MODIFIERS__COMBAT_MODIFIERS - Blocking headshots")
	ELSE
		SET_PED_SUFFERS_CRITICAL_HITS(piPlayerPed, TRUE)
		PRINTLN("PROCESS_PLAYER_MODIFIERS__COMBAT_MODIFIERS - Headshots not blocked")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_BlockTakeDowns)
		SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(PLAYER_ID(), 0.125)
		PRINTLN("PROCESS_PLAYER_MODIFIERS__COMBAT_MODIFIERS - Reducing weapon takedown damage modifier to prevent take downs")
	ELSE
		SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(PLAYER_ID(), 1)
		PRINTLN("PROCESS_PLAYER_MODIFIERS__COMBAT_MODIFIERS - Weapon takedown damage modifier not reduced")
	ENDIF
	
	PROCESS_PLAYER_MODIFIER__EXPLOSION_RESISTANCE(iCurrentModSet)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_DisableDropWepOnDeath)
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPlayerPed, FALSE)
		PRINTLN("PROCESS_PLAYER_MODIFIERS__COMBAT_MODIFIERS - Blocking weapon drops on death")
	ELSE
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPlayerPed, TRUE)
		PRINTLN("PROCESS_PLAYER_MODIFIERS__COMBAT_MODIFIERS - Weapon drops on death not blocked")
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Respawn Vehicle Modifier
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PLAYER_MODIFIERS__SPAWN_VEHICLE(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iCurrentModSet)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_SpawnInVehicle)
		EXIT
	ENDIF
	
	sPlayerModifierData.mnRespawnVehicle = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].mnRespawnVehicle
	g_bDMUsingSpawnVehicle = TRUE
	PRINTLN("PROCESS_PLAYER_MODIFIERS__SPAWN_VEHICLE - Setting respawn vehicle to: ", GET_MODEL_NAME_FOR_DEBUG(sPlayerModifierData.mnRespawnVehicle))
	
ENDPROC

PROC PROCESS_PLAYER_MODIFIERS__START_VEHICLE_TRANSFORM(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sPlayerModifierData.sVehicleSwap.ptfxID)
		REMOVE_PARTICLE_FX(sPlayerModifierData.sVehicleSwap.ptfxID)
		PRINTLN("[VehSwap][Player Modifiers] - PROCESS_PLAYER_MODIFIERS__START_VEHICLE_TRANSFORM - Clearing up the smokey effect because we are swapping now!")
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		REMOVE_PARTICLE_FX_FROM_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		PRINTLN("[VehSwap][Player Modifiers] - PROCESS_PLAYER_MODIFIERS__START_VEHICLE_TRANSFORM - Removing particle FX from the vehicle the player is currently in")
	ENDIF
	
	REMOVE_PARTICLE_FX_FROM_ENTITY(PLAYER_PED_ID())
	PRINTLN("[VehSwap][Player Modifiers] - PROCESS_PLAYER_MODIFIERS__START_VEHICLE_TRANSFORM - Removing particle FX from the local player's ped")
	
	SET_VEHICLE_SWAP_STATE(sPlayerModifierData.sVehicleSwap.eVehicleSwapState, eVehicleSwapState_INITIALISE)
ENDPROC

PROC PROCESS_PLAYER_MODIFIERS__TRANSFORM_VEHICLE_SETUP(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	
	IF IS_BIT_SET(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_TRANSFORM_SETUP_DONE)
		EXIT
	ENDIF
	
	IF (sPlayerModifierData.ePreTransformState = eVehicleSwapState_CREATE
	OR sPlayerModifierData.ePreTransformState = eVehicleSwapState_INITIALISE)
	AND sPlayerModifierData.sVehicleSwap.eVehicleSwapState != eVehicleSwapState_CREATE
	AND sPlayerModifierData.sVehicleSwap.eVehicleSwapState != eVehicleSwapState_INITIALISE
		
		IF NOT DOES_ENTITY_EXIST(sPlayerModifierData.sVehicleSwap.viNewVeh)
			EXIT
		ENDIF
		
		PRINTLN("[VehSwap] PROCESS_PLAYER_MODIFIERS__TRANSFORM_VEHICLE_SETUP - Setting up transform vehicle")		

		IF sPlayerModifierData.sVehicleSwap.eVehicleSwapState = eVehicleSwapState_APPLY_FORCES
			VS_PROCESS_APPLY_FORCES(sPlayerModifierData.sVehicleSwap, sPlayerModifierData.mnModelToSwapTo)
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			PRINTLN("[VehSwap] - PROCESS_PLAYER_MODIFIERS__TRANSFORM_VEHICLE_SETUP - Playing transform sound from vehicle")
			PLAY_SOUND_FROM_ENTITY(-1, "Vehicle_Transform", GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), "DLC_Air_Race_Sounds_Player",  NOT IS_FAKE_MULTIPLAYER_MODE_SET(), 250)
		ELSE
			PRINTLN("[VehSwap] - PROCESS_PLAYER_MODIFIERS__TRANSFORM_VEHICLE_SETUP - Playing transform sound from ped")
			PLAY_SOUND_FROM_ENTITY(-1, "Vehicle_Transform", PLAYER_PED_ID(), "DLC_Air_Race_Sounds_Player", NOT IS_FAKE_MULTIPLAYER_MODE_SET(), 250)
		ENDIF
		
		SET_BIT(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_TRANSFORM_SETUP_DONE)
		SET_BIT(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_TRANSFORM_VEH_INIT_REQUESTED)
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_MODIFIERS__TRANSFORM_ONE_FRAME_CLEANUP(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	
	IF (sPlayerModifierData.ePreTransformState = eVehicleSwapState_CREATE
	OR sPlayerModifierData.ePreTransformState = eVehicleSwapState_INITIALISE)
	AND sPlayerModifierData.sVehicleSwap.eVehicleSwapState = eVehicleSwapState_CLEANUP
		PRINTLN("[VehSwap] PROCESS_PLAYER_MODIFIERS__TRANSFORM_ONE_FRAME_CLEANUP - Attempting single frame cleanup")
		PROCESS_VEHICLE_SWAP(sPlayerModifierData.sVehicleSwap, sPlayerModifierData.mnModelToSwapTo)
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_MODIFIERS__TRANSFORM(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	sPlayerModifierData.ePreTransformState = sPlayerModifierData.sVehicleSwap.eVehicleSwapState
	PROCESS_VEHICLE_SWAP(sPlayerModifierData.sVehicleSwap, sPlayerModifierData.mnModelToSwapTo)
	PROCESS_PLAYER_MODIFIERS__TRANSFORM_VEHICLE_SETUP(sPlayerModifierData)
	PROCESS_PLAYER_MODIFIERS__TRANSFORM_ONE_FRAME_CLEANUP(sPlayerModifierData)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Disable Actions Modifier
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PLAYER_MODIFIERS__DISABLE_ACTIONS(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iCurrentModSet)
	
	PRINTLN("PROCESS_PLAYER_MODIFIERS__DISABLE_ACTIONS - Processing...")
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_DisableActionEnterVehicle)
		PRINTLN("PROCESS_PLAYER_MODIFIERS__DISABLE_ACTIONS - Disabling Enter Vehicle")
		SET_BIT(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_ENTER_VEHICLE)
	ELSE
		PRINTLN("PROCESS_PLAYER_MODIFIERS__DISABLE_ACTIONS - Enabling Enter Vehicle")
		CLEAR_BIT(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_ENTER_VEHICLE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_DisableActionExitVehicle)
		PRINTLN("PROCESS_PLAYER_MODIFIERS__DISABLE_ACTIONS - Disabling Exit Vehicle")
		SET_BIT(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_EXIT_VEHICLE)
	ELSE
		PRINTLN("PROCESS_PLAYER_MODIFIERS__DISABLE_ACTIONS - Enabling Exit Vehicle")
		CLEAR_BIT(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_EXIT_VEHICLE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_DisableActionVehicleHandbrake)
		PRINTLN("PROCESS_PLAYER_MODIFIERS__DISABLE_ACTIONS - Disabling Vehicle Handbrake")
		SET_BIT(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_VEHICLE_HANDBRAKE)
		SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_HANDBRAKE_DISABLED)
	ELSE
		PRINTLN("PROCESS_PLAYER_MODIFIERS__DISABLE_ACTIONS - Enabling Vehicle Handbrake")
		CLEAR_BIT(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_VEHICLE_HANDBRAKE)
	ENDIF
	
	IF IS_CURRENT_MODIFIER_SET_VALID(iCurrentModSet)
		SWITCH g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].eInfiniteAmmoType
			CASE DM_INFINITE_AMMO_OFF
				PRINTLN("[Player Modifiers] PROCESS_PLAYER_MODIFIERS__DISABLE_ACTIONS - Enabling ammo drop on wheel")
				CLEAR_BIT(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_DROP_AMMO)
			BREAK 			
			CASE DM_INFINITE_AMMO_RELOAD
			CASE DM_INFINITE_AMMO_NO_RELOAD
				PRINTLN("[Player Modifiers] PROCESS_PLAYER_MODIFIERS__DISABLE_ACTIONS - Disabling ammo drop on wheel due to infinite ammo being used")
				SET_BIT(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_DROP_AMMO)
			BREAK
		ENDSWITCH
	ENDIF

ENDPROC

PROC PROCESS_DISABLED_CONTROL_ACTIONS(PLAYER_MODIFIER_DATA& sPlayerModifierData)
	
	IF IS_BIT_SET(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_ENTER_VEHICLE)
		PRINTLN("PROCESS_DISABLED_CONTROL_ACTIONS - Disabling INPUT_ENTER")
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	ENDIF
	
	IF IS_BIT_SET(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_EXIT_VEHICLE)
		PRINTLN("PROCESS_DISABLED_CONTROL_ACTIONS - Disabling INPUT_VEH_EXIT")
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	
	IF IS_BIT_SET(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_VEHICLE_HANDBRAKE)
		PRINTLN("PROCESS_DISABLED_CONTROL_ACTIONS - Disabling INPUT_VEH_HANDBRAKE")
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	ENDIF
	
	IF IS_BIT_SET(sPlayerModifierData.iDisableActionsBS, PLAYER_MODIFIER_DISABLE_ACTIONS_DROP_AMMO)
		PRINTLN("[Player Modifiers] PROCESS_DISABLED_CONTROL_ACTIONS - Disabling ammo drop on wheel due to infinite ammo being used. Calling DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_AMMO)")
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_AMMO)
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Bounds Modifier
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PLAYER_MODIFIERS__BOUNDS(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iCurrentModSet)
	
	IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iBoundsIndex = -1
		EXIT
	ENDIF
	
	PRINTLN("PROCESS_PLAYER_MODIFIERS__BOUNDS - Processing...")
	
	sPlayerModifierData.iBoundsIndex = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iBoundsIndex
	PRINTLN("PROCESS_PLAYER_MODIFIERS__BOUNDS - Bounds index set to: ", sPlayerModifierData.iBoundsIndex)

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Requesting and processing applicability updates
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC REQUEST_APPLICABILITY_UPDATE(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	
	IF IS_THIS_LEGACY_DM_CONTENT()
		EXIT
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	SET_BIT(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_REQUESTED)
	PRINTLN("[Player Modifiers] - REQUEST_APPLICABILITY_UPDATE called! ciPLAYERMODIFIER_REQUESTED Set")
ENDPROC

PROC CLEAR_APPLICABILITY_UPDATE_REQUEST(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	CLEAR_BIT(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_REQUESTED)
	PRINTLN("[Player Modifiers] - CLEAR_APPLICABILITY_UPDATE_REQUEST called! ciPLAYERMODIFIER_REQUESTED Cleared")
ENDPROC

FUNC BOOL HAS_APPLICABILITY_UPDATE_BEEN_REQUESTED(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	RETURN IS_BIT_SET(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_REQUESTED)
ENDFUNC

PROC CLEAR_PENDING_MODIFIER_DATA(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT &iPendingModSet)
	iPendingModSet = -1
	sPlayerModifierData.iPendingApplyBlockerBS = 0
	PRINTLN("[Player Modifiers] - CLEAR_PENDING_MODIFIER_DATA - Clearing data! ")
ENDPROC

PROC CLEAR_MODIFIER_SET_SORT_DATA(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	sPlayerModifierData.iModifierSetSortCount = 0
	INITIALISE_INT_ARRAY(sPlayerModifierData.iModifierSetIndex, -1)
	INITIALISE_FLOAT_ARRAY(sPlayerModifierData.fModifierSetScore, 0.0)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Score Eligibility
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_SCORE_TO_CHECK_FOR_MODIFIER_SCORING(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iScore)
	sPlayerModifierData.iScoreToCheck = iScore
	PRINTLN("[Player Modifiers] SET_SCORE_TO_CHECK_FOR_MODIFIER_SCORING - sPlayerModifierData.iScoreToCheck set to ", sPlayerModifierData.iScoreToCheck)
ENDPROC

PROC PROCESS_KEEP_PLAYER_MODIFIER_SCORES_TO_CHECK_UP_TO_DATE(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iIndividualScore, INT iTeamScore)
	sPlayerModifierData.iIndividualScore = iIndividualScore
	sPlayerModifierData.iTeamScore = iTeamScore
ENDPROC

FUNC INT GET_SCORE_REQUIREMENT_FROM_CONDITION_TYPE(INT iTargetScore, INT iModSetToCheck)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSetToCheck].iModifierSetBS, ciDM_ModifierBS_UsePercentageComparison)
		RETURN FLOOR(iTargetScore * g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSetToCheck].sModifierSetup.iScorePercentage * 0.01)
	ENDIF
	
	RETURN g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSetToCheck].sModifierSetup.iScoreConditionToCheck
	
ENDFUNC

FUNC BOOL DOES_PLAYER_MEET_SCORE_REQUIREMENT_FOR_MODIFIER_SET(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iModSetToCheck)
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSetToCheck].iModifierSetBS, ciDM_ModifierBS_CheckTeamScore)
		SET_SCORE_TO_CHECK_FOR_MODIFIER_SCORING(sPlayerModifierData, sPlayerModifierData.iTeamScore)
	ELSE
		SET_SCORE_TO_CHECK_FOR_MODIFIER_SCORING(sPlayerModifierData, sPlayerModifierData.iIndividualScore)
	ENDIF
	
	INT iScoreRequirement = GET_SCORE_REQUIREMENT_FROM_CONDITION_TYPE(sPlayerModifierData.iTargetScore, iModSetToCheck)

	#IF IS_DEBUG_BUILD
	PRINTLN("[Player Modifiers] DOES_PLAYER_MEET_SCORE_REQUIREMENT_FOR_MODIFIER_SET - Check Type: ", GET_SCORE_CONDITION_TYPE_NAME(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSetToCheck].sModifierSetup.eScoreCondition), " Checking ", sPlayerModifierData.iScoreToCheck, " against ", iScoreRequirement)
	#ENDIF
	
	SWITCH g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSetToCheck].sModifierSetup.eScoreCondition
		CASE DM_SCORE_EQUAL
			RETURN sPlayerModifierData.iScoreToCheck = iScoreRequirement
		BREAK
		CASE DM_SCORE_GREATER_THAN_EQUAL_TO
			RETURN sPlayerModifierData.iScoreToCheck >= iScoreRequirement
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC SORT_GREATER_THAN_SCORE_CONDITIONS(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	
	INITIALISE_INT_ARRAY(sPlayerModifierData.iGreaterThanIndex, -1)
	INITIALISE_INT_ARRAY(sPlayerModifierData.iGreaterThanSort, -1)
	
	INT iLoop
	FOR iLoop = 0 TO ciDM_MAX_MODIFIER_SETS-1
		IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.eScoreCondition = DM_SCORE_GREATER_THAN_EQUAL_TO
			sPlayerModifierData.iGreaterThanIndex[sPlayerModifierData.iGreaterThanSortCount] = iLoop			
			sPlayerModifierData.iGreaterThanSort[sPlayerModifierData.iGreaterThanSortCount] = GET_SCORE_REQUIREMENT_FROM_CONDITION_TYPE(sPlayerModifierData.iTargetScore, iLoop)
			sPlayerModifierData.iGreaterThanSortCount++
		ENDIF
	ENDFOR
	
	PRINTLN("SORT_GREATER_THAN_SCORE_CONDITIONS ---------- Sort Greater Than")
	QUICK_SORT_INT_WITH_INDEXES(sPlayerModifierData.iGreaterThanSort, sPlayerModifierData.iGreaterThanIndex, 0, sPlayerModifierData.iGreaterThanSortCount-1, TRUE)
	FOR iLoop = 0 TO sPlayerModifierData.iGreaterThanSortCount-1
		PRINTLN("Greater Than Modset Index: ", sPlayerModifierData.iGreaterThanIndex[iLoop], " Value: ", sPlayerModifierData.iGreaterThanSort[iLoop])
	ENDFOR
	PRINTLN("SORT_GREATER_THAN_SCORE_CONDITIONS ----------")
	
ENDPROC

FUNC INT GET_INDEX_FROM_SORTED_CONDITION(INT &iIndexArray[], INT iModifierSetIndexToFind)
	INT iLoop
	FOR iLoop = 0 TO ciDM_MAX_MODIFIER_SETS-1
		IF iModifierSetIndexToFind = iIndexArray[iLoop]
			RETURN iLoop
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC FLOAT GET_SCORE_FOR_MODIFIER_SET_BASED_ON_SCORE_REQUIREMENT(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iModSetToCheck)
	
	FLOAT fScore
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSetToCheck].iModifierSetBS, ciDM_ModifierBS_CheckTeamScore)
		fScore += 25.0
		PRINTLN("GET_SCORE_FOR_MODIFIER_SET_BASED_ON_SCORE_REQUIREMENT - Adding a small bonus for individual score")
	ENDIF
	
	INT iSortedIndex
	
	SWITCH g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSetToCheck].sModifierSetup.eScoreCondition
		CASE DM_SCORE_EQUAL
			fScore += 300.0
			RETURN fScore
		BREAK
		CASE DM_SCORE_GREATER_THAN_EQUAL_TO
			fScore += 200.0
			iSortedIndex = GET_INDEX_FROM_SORTED_CONDITION(sPlayerModifierData.iGreaterThanIndex, iModSetToCheck)
			IF iSortedIndex != -1
				fScore += (ciDM_MAX_MODIFIER_SETS - iSortedIndex)
				PRINTLN("GET_SCORE_FOR_MODIFIER_SET_BASED_ON_SCORE_REQUIREMENT - Adding extra to score based on Greater Than sort")
			ENDIF
			RETURN fScore
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Timer Eligibility
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL HAS_MODIFIER_SET_TIMER_REQUIREMENT_BEEN_MET(INT iTimeStamp, INT iTimerLength)

	IF iTimeStamp = 0
		RETURN FALSE
	ENDIF
	
	RETURN (NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) - iTimeStamp) >= iTimerLength
	
ENDFUNC

FUNC INT GET_TIMER_DURATION_FROM_CONDITION_TYPE(INT iDuration, INT iModSetToCheck)

	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSetToCheck].iModifierSetBS, ciDM_ModifierBS_UsePercentageComparison)
		RETURN FLOOR(iDuration * g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSetToCheck].sModifierSetup.iTimerPercentage * 0.01)
	ENDIF
	
	RETURN g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSetToCheck].sModifierSetup.iTimerConditionLength
	
ENDFUNC

PROC SORT_TIMER_CONDITIONS(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	
	INITIALISE_INT_ARRAY(sPlayerModifierData.iTimerSortLength, -1)
	INITIALISE_INT_ARRAY(sPlayerModifierData.iTimerSortIndex, -1)
	
	INT iLoop
	FOR iLoop = 0 TO ciDM_MAX_MODIFIER_SETS-1
	
		IF (IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].iModifierSetBS, ciDM_ModifierBS_UsePercentageComparison)
		AND g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iTimerPercentage > 0)
		OR (NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].iModifierSetBS, ciDM_ModifierBS_UsePercentageComparison) 
		AND g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierSetup.iTimerConditionLength > 0)
				
			sPlayerModifierData.iTimerSortIndex[sPlayerModifierData.iTimerSortCount] = iLoop			
			sPlayerModifierData.iTimerSortLength[sPlayerModifierData.iTimerSortCount] = GET_TIMER_DURATION_FROM_CONDITION_TYPE(sPlayerModifierData.iDuration, iLoop)
			sPlayerModifierData.iTimerSortCount++
			
		ENDIF
	ENDFOR
	
	PRINTLN("SORT_TIMER_CONDITIONS ---------- Sort Timers")
	QUICK_SORT_INT_WITH_INDEXES(sPlayerModifierData.iTimerSortLength, sPlayerModifierData.iTimerSortIndex, 0, sPlayerModifierData.iTimerSortCount-1, TRUE)
	FOR iLoop = 0 TO sPlayerModifierData.iTimerSortCount-1
		PRINTLN("Timer Condition Modset Index: ", sPlayerModifierData.iTimerSortIndex[iLoop], " Value: ", sPlayerModifierData.iTimerSortLength[iLoop])
	ENDFOR
	PRINTLN("SORT_TIMER_CONDITIONS ----------")
	
ENDPROC

PROC SET_MODIFIER_SET_TIMER_REQUIREMENT_TIME_STAMP(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iOffset = 0)
	IF sPlayerModifierData.iTimerConditionTimeStamp = 0
		sPlayerModifierData.iTimerConditionTimeStamp = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) + iOffset
		PRINTLN("SET_MODIFIER_SET_TIMER_REQUIREMENT_TIME_STAMP - Start of DM time: ", sPlayerModifierData.iTimerConditionTimeStamp)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Position Eligibility
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_KEEP_PLAYER_MODIFIER_POSITION_TO_CHECK_UP_TO_DATE(PLAYER_MODIFIER_DATA &sPlayerModifierData, BOOL bWinning, BOOL bLosing)
	
	IF bWinning
	AND NOT bLosing
		#IF IS_DEBUG_BUILD
		IF sPlayerModifierData.eMyPosition != DM_POSITION__FIRST
			PRINTLN("[Player Modifiers] PROCESS_KEEP_PLAYER_MODIFIER_POSITION_TO_CHECK_UP_TO_DATE - Setting position to DM_POSITION__FIRST")
		ENDIF
		#ENDIF		
		sPlayerModifierData.eMyPosition = DM_POSITION__FIRST
		
	ELIF bLosing
	AND NOT bWinning
		#IF IS_DEBUG_BUILD
		IF sPlayerModifierData.eMyPosition != DM_POSITION__LAST
			PRINTLN("[Player Modifiers] PROCESS_KEEP_PLAYER_MODIFIER_POSITION_TO_CHECK_UP_TO_DATE - Setting position to DM_POSITION__LAST")
		ENDIF
		#ENDIF	
		sPlayerModifierData.eMyPosition = DM_POSITION__LAST
	ELSE
		#IF IS_DEBUG_BUILD
		IF sPlayerModifierData.eMyPosition != DM_POSITION__ANY
			PRINTLN("[Player Modifiers] PROCESS_KEEP_PLAYER_MODIFIER_POSITION_TO_CHECK_UP_TO_DATE - Setting position to DM_POSITION__ANY")
		ENDIF
		#ENDIF	
		sPlayerModifierData.eMyPosition = DM_POSITION__ANY
	ENDIF
	
ENDPROC

FUNC BOOL DOES_MODIFIER_SET_POSITION_MATCH_MY_POSITION(PLAYER_MODIFIER_DATA &sPlayerModifierData, DM_MODIFIER_POSITION ePositionToCheck)
	
	PRINTLN("DOES_MODIFIER_SET_POSITION_MATCH_MY_POSITION - sPlayerModifierData.eMyPosition: ", GET_POSITION_TYPE_NAME(sPlayerModifierData.eMyPosition), " ePositionToCheck: ", GET_POSITION_TYPE_NAME(ePositionToCheck))
	
	RETURN sPlayerModifierData.eMyPosition = ePositionToCheck
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Modifier Set Scoring
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE: Give a score based on relevance to the local player. The lower the score the better!
PROC SCORE_MODIFIER_SETS(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iTeam = -2)
	
	INT iModifierSetLoop
	
	IF iTeam = -2
		iTeam = GET_PLAYER_TEAM(PLAYER_ID())
	ENDIF
	
	FOR iModifierSetLoop = 0 TO g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings-1
		
		PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Calculating score for modifier ", iModifierSetLoop, " ____________________")
		PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Current Player Team: ", iTeam)
		FLOAT fSetScore = 0.0
		IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModifierSetLoop].sModifierSetup.eWhoType = DM_WHO__TEAMS
			IF iTeam != -1
			AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModifierSetLoop].sModifierSetup.iTeamBS, iTeam)
				fSetScore -= 1000.0
				PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Current Score: ",fSetScore," - Reducing score for team match")
			ELSE
				PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Current Score: ",fSetScore," - Not adding this modifier set to the sort array as isn't relevant to my team.")
				RELOOP
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModifierSetLoop].sModifierSetup.ePositionType != DM_POSITION__ANY
			IF DOES_MODIFIER_SET_POSITION_MATCH_MY_POSITION(sPlayerModifierData, g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModifierSetLoop].sModifierSetup.ePositionType)
				fSetScore -= 500.0
				PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Current Score: ",fSetScore," - Reducing score for position match")
			ELSE
				PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Current Score: ",fSetScore," - Not adding this modifier set to the sort array as isn't relevant to my position.")
				RELOOP
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModifierSetLoop].sModifierSetup.eConditionType = DM_CONDITION__SCORE
			IF DOES_PLAYER_MEET_SCORE_REQUIREMENT_FOR_MODIFIER_SET(sPlayerModifierData, iModifierSetLoop)
				fSetScore -= GET_SCORE_FOR_MODIFIER_SET_BASED_ON_SCORE_REQUIREMENT(sPlayerModifierData, iModifierSetLoop)
				PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Current Score: ",fSetScore," - Reducing score by ", GET_SCORE_FOR_MODIFIER_SET_BASED_ON_SCORE_REQUIREMENT(sPlayerModifierData, iModifierSetLoop), " for score requirement met")
			ELSE
				PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Current Score: ",fSetScore," - Not adding this modifier set to the sort array as I don't meet the score requirement.")
				RELOOP
			ENDIF
		ELIF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModifierSetLoop].sModifierSetup.eConditionType = DM_CONDITION__TIMER
			
			INT iTimerLength = GET_TIMER_DURATION_FROM_CONDITION_TYPE(sPlayerModifierData.iDuration, iModifierSetLoop)
			
			IF HAS_MODIFIER_SET_TIMER_REQUIREMENT_BEEN_MET(sPlayerModifierData.iTimerConditionTimeStamp, iTimerLength)
				fSetScore -= 50.0
				FLOAT fBonus = TO_FLOAT(ciDM_MAX_MODIFIER_SETS - GET_INDEX_FROM_SORTED_CONDITION(sPlayerModifierData.iTimerSortIndex, iModifierSetLoop))
				fSetScore -= fBonus
				PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Current Score: ",fSetScore," - Reducing score by 50 for timer requirement met and extra ",fBonus," for sorted array position")
			ELSE
				PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Current Score: ",fSetScore," - Not adding this modifier set to the sort array as I don't meet the timer requirement.")
				RELOOP
			ENDIF
		ENDIF
		
		sPlayerModifierData.fModifierSetScore[sPlayerModifierData.iModifierSetSortCount] = fSetScore
		sPlayerModifierData.iModifierSetIndex[sPlayerModifierData.iModifierSetSortCount] = iModifierSetLoop
		PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Modifier ", sPlayerModifierData.iModifierSetIndex[sPlayerModifierData.iModifierSetSortCount]," has a score of ", sPlayerModifierData.fModifierSetScore[sPlayerModifierData.iModifierSetSortCount])
		sPlayerModifierData.iModifierSetSortCount++
		PRINTLN("[Player Modifiers] SCORE_MODIFIER_SETS - Number of modifier sets in the sort array is now ", sPlayerModifierData.iModifierSetSortCount)
		PRINTLN("______________________")
	ENDFOR

ENDPROC

#IF IS_DEBUG_BUILD
PROC PRINT_MODIFIER_SET_ARRAYS(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	INT iLoop
	FOR iLoop = 0 TO sPlayerModifierData.iModifierSetSortCount-1
		PRINTLN("[Player Modifiers] GET_RELEVANT_MODIFIER_SET_INDEX index ", iLoop,": ", sPlayerModifierData.iModifierSetIndex[iLoop], " | ", sPlayerModifierData.fModifierSetScore[iLoop])
	ENDFOR
ENDPROC
#ENDIF

PROC SORT_MODIFIER_SETS(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[Player Modifiers] GET_RELEVANT_MODIFIER_SET_INDEX PRE SORT")
	PRINT_MODIFIER_SET_ARRAYS(sPlayerModifierData)
	#ENDIF
	
	QUICK_SORT_FLOAT_WITH_INDEXES(sPlayerModifierData.fModifierSetScore, sPlayerModifierData.iModifierSetIndex, 0, sPlayerModifierData.iModifierSetSortCount-1)
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[Player Modifiers] GET_RELEVANT_MODIFIER_SET_INDEX POST SORT")
	PRINT_MODIFIER_SET_ARRAYS(sPlayerModifierData)
	#ENDIF
	
ENDPROC

FUNC INT GET_RELEVANT_MODIFIER_SET_INDEX(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iTeam = -2)
	
	CLEAR_MODIFIER_SET_SORT_DATA(sPlayerModifierData)
	
	SCORE_MODIFIER_SETS(sPlayerModifierData, iTeam)
	
	SORT_MODIFIER_SETS(sPlayerModifierData)
	
	RETURN sPlayerModifierData.iModifierSetIndex[0]
	
ENDFUNC

PROC SET_APPLICABILITY_APPLY_CONDITION_RESPAWNING(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	IF IS_BIT_SET(sPlayerModifierData.iPendingApplyBlockerBS, ciPLAYERMODIFIER_APPLY_BLOCKER__SPAWN)
		CLEAR_BIT(sPlayerModifierData.iPendingApplyBlockerBS, ciPLAYERMODIFIER_APPLY_BLOCKER__SPAWN)
	ENDIF
ENDPROC

FUNC INT GET_MODSET_APPLY_BLOCKER_BIT(DM_MODIFIER_APPLY eApplyType)
	SWITCH eApplyType
		CASE DM_APPLY__SPAWN 
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				RETURN ciPLAYERMODIFIER_APPLY_BLOCKER__SPAWN
			ENDIF
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

PROC SET_MODSET_APPLY_CONDITIONS(INT iNewModset, PLAYER_MODIFIER_DATA &sPlayerModifierData)
	
	//Creator Set Conditions
	IF GET_MODSET_APPLY_BLOCKER_BIT(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iNewModSet].sModifierSetup.eApplyType) > -1
		SET_BIT(sPlayerModifierData.iPendingApplyBlockerBS, GET_MODSET_APPLY_BLOCKER_BIT(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iNewModSet].sModifierSetup.eApplyType))
		PRINTLN("[Player Modifiers] - SET_MODSET_APPLY_CONDITION - Setting Apply condition ", GET_APPLY_TYPE_NAME(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iNewModSet].sModifierSetup.eApplyType))
		
		IF IS_BIT_SET(sPlayerModifierData.iPendingApplyBlockerBS, ciPLAYERMODIFIER_APPLY_BLOCKER__SPAWN)
			SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_MOD_SET_ON_SPAWN)
		ENDIF
		
	ENDIF
	
	IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iNewModSet].sModifierSetup.eApplyType != DM_APPLY__SPAWN
		//Vehicle Swap
		IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iNewModset].iModifierSetBS, ciDM_ModifierBS_SpawnInVehicle)
		AND sPlayerModifierData.mnRespawnVehicle != g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iNewModset].mnRespawnVehicle
			SET_BIT(sPlayerModifierData.iPendingApplyBlockerBS, ciPLAYERMODIFIER_APPLY_BLOCKER__VEHICLE_ASSETS)
			PRINTLN("[Player Modifiers] - SET_MODSET_APPLY_CONDITION - Setting Apply condition - Prep swap to vehicle")
		ENDIF
		
		//Weapon Swap
		IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iNewModset].sModifierInventory.iInventoryBitset, ciFMMC_INVENTORY_BS_DM_MODSET_USING_INVENTORY)
		AND IS_BIT_SET(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_INITIAL_MOD_SET_APPLICATION_DONE)
			SET_BIT(sPlayerModifierData.iPendingApplyBlockerBS, ciPLAYERMODIFIER_APPLY_BLOCKER__WEAPON_ASSETS)
			PRINTLN("[Player Modifiers] - SET_MODSET_APPLY_CONDITION - Setting Apply condition - Prep swap to weapons")
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_APPLY_CONDITION_BEEN_MET(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iPendingModset)

	IF sPlayerModifierData.iPendingApplyBlockerBS = 0
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPlayerModifierData.iPendingApplyBlockerBS, ciPLAYERMODIFIER_APPLY_BLOCKER__WEAPON_ASSETS)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		
			REQUEST_NAMED_PTFX_ASSET("scr_bike_adversary")
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_bike_adversary")
				WEAPON_TYPE eStartingWeapon = WEAPONTYPE_INVALID
				IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iPendingModset].sModifierInventory.iInventoryBitset, ciFMMC_INVENTORY_BS_START_UNARMED)
				OR g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iPendingModset].sModifierInventory.iStartWeapon = FMMC_STARTING_WEAPON_UNARMED
					eStartingWeapon = WEAPONTYPE_UNARMED
				ELIF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iPendingModset].sModifierInventory.iStartWeapon != FMMC_STARTING_WEAPON_CURRENT
					eStartingWeapon = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iPendingModset].sModifierInventory.sWeaponStruct[g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iPendingModset].sModifierInventory.iStartWeapon].wtWeapon
				ENDIF
				IF eStartingWeapon = WEAPONTYPE_INVALID
					CLEAR_BIT(sPlayerModifierData.iPendingApplyBlockerBS, ciPLAYERMODIFIER_APPLY_BLOCKER__WEAPON_ASSETS)
					PRINTLN("[Player Modifiers] - HAS_APPLY_CONDITION_BEEN_MET - Weapon Asset Not Needed")
				ELSE
					REQUEST_WEAPON_ASSET(eStartingWeapon, ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS))
					IF HAS_WEAPON_ASSET_LOADED(eStartingWeapon)
						CLEAR_BIT(sPlayerModifierData.iPendingApplyBlockerBS, ciPLAYERMODIFIER_APPLY_BLOCKER__WEAPON_ASSETS)
						PRINTLN("[Player Modifiers] - HAS_APPLY_CONDITION_BEEN_MET - Weapon Asset Loaded")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Player Modifiers] - HAS_APPLY_CONDITION_BEEN_MET - Weapon swap will happen on spawn, clearing.")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sPlayerModifierData.iPendingApplyBlockerBS, ciPLAYERMODIFIER_APPLY_BLOCKER__VEHICLE_ASSETS)
		
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			sPlayerModifierData.mnModelToSwapTo = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iPendingModset].mnRespawnVehicle
			IF sPlayerModifierData.mnModelToSwapTo != DUMMY_MODEL_FOR_SCRIPT
				REQUEST_MODEL(sPlayerModifierData.mnModelToSwapTo)
				PRELOAD_VEHICLE_AUDIO_BANK(ENUM_TO_INT(sPlayerModifierData.mnModelToSwapTo))
			ENDIF
			
			REQUEST_NAMED_PTFX_ASSET("scr_as_trans")
				
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_as_trans")
			AND (sPlayerModifierData.mnModelToSwapTo != DUMMY_MODEL_FOR_SCRIPT AND HAS_MODEL_LOADED(sPlayerModifierData.mnModelToSwapTo))
			
				//Check valid to swap right now. Only do these if there are no other blockers. We need the transform to fire as soon as this passes
				IF sPlayerModifierData.iPendingApplyBlockerBS = POW(2.0, ciPLAYERMODIFIER_APPLY_BLOCKER__VEHICLE_ASSETS)
				
					DO_TRANSFORM_SHAPETEST(sPlayerModifierData.sShapeTest, GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_ROTATION(PLAYER_PED_ID()))
					
					VEHICLE_INDEX viCurrentVeh = NULL
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						viCurrentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					ENDIF
					
					IF sPlayerModifierData.sShapeTest.iResult != 1
					AND (viCurrentVeh = NULL OR NOT IS_VEHICLE_FUCKED_MP(viCurrentVeh))
					AND (viCurrentVeh = NULL OR IS_VEHICLE_IN_VALID_POSITION_FOR_TRANSFORM(viCurrentVeh))
						CLEAR_BIT(sPlayerModifierData.iPendingApplyBlockerBS, ciPLAYERMODIFIER_APPLY_BLOCKER__VEHICLE_ASSETS)
						PRINTLN("[Player Modifiers] - HAS_APPLY_CONDITION_BEEN_MET - Safe to swap vehicle")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Player Modifiers] - HAS_APPLY_CONDITION_BEEN_MET - Vehicle swap will happen on spawn, clearing.")
		ENDIF
	ENDIF
	
	PRINTLN("[Player Modifiers] - HAS_APPLY_CONDITION_BEEN_MET - Blockers: ", sPlayerModifierData.iPendingApplyBlockerBS)
	
	RETURN sPlayerModifierData.iPendingApplyBlockerBS = 0

ENDFUNC

PROC INIT_APPLY_MOD_SET_DATA(PLAYER_MODIFIER_DATA &sPlayerModifierData, BOOL bModSetChanged = FALSE)
	IF bModSetChanged
		sPlayerModifierData.mnRespawnVehicle = DUMMY_MODEL_FOR_SCRIPT
		g_bDMUsingSpawnVehicle = FALSE
	ENDIF
	CLEAR_BIT(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_INVENTORY_GIVEN)
	CLEAR_BIT(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_TRANSFORM_SETUP_DONE)
ENDPROC

PROC SET_CURRENT_PLAYER_MODIFIER_SET(INT &iCurrentModSet, INT iNewModSet)
	iCurrentModSet = iNewModSet
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iDMModSet = iCurrentModSet
	PRINTLN("[Player Modifiers] SET_CURRENT_PLAYER_MODIFIER_SET - Player modifier set is now set to ", iCurrentModSet)
	#ENDIF
ENDPROC

PROC APPLY_MOD_SET_TO_LOCAL_PLAYER(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT &iCurrentModSet, BOOL bModSetChanged = FALSE)
	
	IF IS_THIS_LEGACY_DM_CONTENT()
		EXIT
	ENDIF
	
	IF iCurrentModSet = -1
		SET_CURRENT_PLAYER_MODIFIER_SET(iCurrentModSet, GET_RELEVANT_MODIFIER_SET_INDEX(sPlayerModifierData))
	ENDIF
	
	INIT_APPLY_MOD_SET_DATA(sPlayerModifierData, bModSetChanged)
	PRINTLN("[Player Modifiers] - APPLY_MOD_SET_TO_LOCAL_PLAYER - Applying mod set: ", iCurrentModSet, " Mod Set Changed: ", bModSetChanged)
	DEBUG_PRINTCALLSTACK()
	
	IF bModSetChanged
		
		INT iNextTimer
		FOR iNextTimer = sPlayerModifierData.iTimerSortCount-1 TO 0 STEP -1
		
			IF (sPlayerModifierData.iNextTimeToRefresh = -1
			OR sPlayerModifierData.iNextTimeToRefresh > sPlayerModifierData.iTimerSortLength[iNextTimer])
			AND sPlayerModifierData.iTimerSortLength[iNextTimer] > 0
			AND NOT HAS_MODIFIER_SET_TIMER_REQUIREMENT_BEEN_MET(sPlayerModifierData.iTimerConditionTimeStamp, sPlayerModifierData.iTimerSortLength[iNextTimer])
				sPlayerModifierData.iNextTimeToRefresh = sPlayerModifierData.iTimerSortLength[iNextTimer]
				PRINTLN("[Player Modifiers] - APPLY_MOD_SET_TO_LOCAL_PLAYER - Setting next refresh time to ", sPlayerModifierData.iNextTimeToRefresh)
			ENDIF
			
		ENDFOR
		
		PROCESS_PLAYER_MODIFIERS__SPAWN_VEHICLE(sPlayerModifierData, iCurrentModSet)
		
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
	OR IS_PED_INJURED(PLAYER_PED_ID())
		PRINTLN("[Player Modifiers] - APPLY_MOD_SET_TO_LOCAL_PLAYER - Exiting due to death from applying mod set: ", iCurrentModSet)
		EXIT
	ENDIF
	
	GIVE_PLAYER_MODIFIER_INVENTORY_TO_LOCAL_PLAYER(sPlayerModifierData, iCurrentModSet, bModSetChanged)

	PROCESS_PLAYER_MODIFIERS__WEAPONS(iCurrentModSet)
	
	PROCESS_PLAYER_MODIFIERS__COMBAT_MODIFIERS(sPlayerModifierData, iCurrentModSet)
		
	PROCESS_PLAYER_MODIFIERS__DISABLE_ACTIONS(sPlayerModifierData, iCurrentModSet)
	
	PROCESS_PLAYER_MODIFIERS__BOUNDS(sPlayerModifierData, iCurrentModSet)
	
	IF sPlayerModifierData.mnModelToSwapTo != DUMMY_MODEL_FOR_SCRIPT
	AND bModSetChanged
		PROCESS_PLAYER_MODIFIERS__START_VEHICLE_TRANSFORM(sPlayerModifierData)
	ENDIF
	
ENDPROC

/// PURPOSE: Processing for things that need to be applied or set every frame such as reset flags.
PROC PROCESS_APPLY_APPLICABILITY_EVERY_FRAME_FUNCTIONALITY(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iCurrentModSet)
	
	IF iCurrentModSet = -1
		EXIT
	ENDIF
	
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iModifierSetBS, ciDM_ModifierBS_BlockTakeDowns)
		SET_PED_RESET_FLAG(piPlayerPed, PRF_PreventAllStealthKills, TRUE)
		SET_PED_RESET_FLAG(piPlayerPed, PRF_PreventAllMeleeTakedowns, TRUE)
	ENDIF
	
	PROCESS_PLAYER_MODIFIERS__MOVEMENT(iCurrentModSet)
	
	PROCESS_PLAYER_MODIFIERS__HEALTH_REGEN(iCurrentModSet)
		
	IF HAS_NET_TIMER_STARTED(sPlayerModifierData.stBlockFiringTimer)
		PROCESS_CUSTOM_DM_BLOCK_FIRING(sPlayerModifierData)
	ENDIF
	
	PROCESS_PLAYER_MODIFIERS__TRANSFORM(sPlayerModifierData)

ENDPROC

PROC PROCESS_APPLICABILITY_CHANGES(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT &iCurrentModSet, INT &iPendingModSet)
		
	IF HAS_APPLICABILITY_UPDATE_BEEN_REQUESTED(sPlayerModifierData)
		
		INT iNewModSet = GET_RELEVANT_MODIFIER_SET_INDEX(sPlayerModifierData)
		IF iNewModSet != iCurrentModSet
		AND iNewModSet != iPendingModSet
			SET_MODSET_APPLY_CONDITIONS(iNewModSet, sPlayerModifierData)
			IF HAS_APPLY_CONDITION_BEEN_MET(sPlayerModifierData, iNewModSet)
				SET_CURRENT_PLAYER_MODIFIER_SET(iCurrentModSet, iNewModSet)
				APPLY_MOD_SET_TO_LOCAL_PLAYER(sPlayerModifierData, iCurrentModSet, TRUE)
				CLEAR_PENDING_MODIFIER_DATA(sPlayerModifierData, iPendingModSet)
			ELSE
				iPendingModSet = iNewModSet
			ENDIF
		ELSE
			PRINTLN("[Player Modifiers] - PROCESS_APPLICABILITY_CHANGES - modset is the same as we are currently using or swapping to (new: ", iNewModSet, ", pending: ", iPendingModSet, ", current: ", iCurrentModSet, ")")
		ENDIF
		
		CLEAR_APPLICABILITY_UPDATE_REQUEST(sPlayerModifierData)
	ENDIF
	
	IF iPendingModSet != -1
		IF HAS_APPLY_CONDITION_BEEN_MET(sPlayerModifierData, iPendingModSet)
			SET_CURRENT_PLAYER_MODIFIER_SET(iCurrentModSet, iPendingModSet)
			APPLY_MOD_SET_TO_LOCAL_PLAYER(sPlayerModifierData, iCurrentModSet, TRUE)
			CLEAR_PENDING_MODIFIER_DATA(sPlayerModifierData, iPendingModSet)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_APPLICABILITY_EVERY_FRAME(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT &iCurrentModSet, INT &iPendingModSet)
	
	PROCESS_APPLICABILITY_CHANGES(sPlayerModifierData, iCurrentModSet, iPendingModSet)
	
	PROCESS_APPLY_APPLICABILITY_EVERY_FRAME_FUNCTIONALITY(sPlayerModifierData, iCurrentModSet)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Init
// ##### Description: Init function for the very start of the mode
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_DEATHMATCH_INIT_SCORE_AND_TIME(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT iTargetScore, INT iDuration)

	sPlayerModifierData.iTargetScore = iTargetScore
	sPlayerModifierData.iDuration = iDuration

ENDPROC

PROC PROCESS_APPLICABILITY_INIT(PLAYER_MODIFIER_DATA &sPlayerModifierData, INT &iCurrentModSet, INT &iPendingModSet)
	
	SORT_GREATER_THAN_SCORE_CONDITIONS(sPlayerModifierData)
	SORT_TIMER_CONDITIONS(sPlayerModifierData)
	
	APPLY_MOD_SET_TO_LOCAL_PLAYER(sPlayerModifierData, iCurrentModSet, TRUE)
	CLEAR_PENDING_MODIFIER_DATA(sPlayerModifierData, iPendingModSet)
	
	SET_BIT(sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_INITIAL_MOD_SET_APPLICATION_DONE)
	PRINTLN("[Player Modifiers] - PROCESS_APPLICABILITY_INIT - ciPLAYERMODIFIER_INITIAL_MOD_SET_APPLICATION_DONE is now set")
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Clean Up
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_PLAYER_MODIFIER_CLEANUP(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	
	PRINTLN("PROCESS_PLAYER_MODIFIER_CLEANUP - Cleaning up player modifier data and functionality")
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	IF sPlayerModifierData.iStartingHealth != -1
		IF NOT IS_PED_INJURED(piPlayerPed)
			SET_ENTITY_HEALTH(piPlayerPed, sPlayerModifierData.iStartingHealth)
		ENDIF
		SET_PED_MAX_HEALTH(piPlayerPed, sPlayerModifierData.iStartingHealth)
	ENDIF
	
	SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(PLAYER_ID(), 0.5)
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), 1.0)
	SET_PLAYER_MAX_EXPLOSIVE_DAMAGE(PLAYER_ID(), -1)
	
	SET_PED_SUFFERS_CRITICAL_HITS(piPlayerPed, TRUE)
	SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPlayerPed, TRUE)
	
	SET_PED_INFINITE_AMMO(piPlayerPed, FALSE)
	SET_PED_INFINITE_AMMO_CLIP(piPlayerPed, FALSE)
	
	SET_PED_HEALTH_PENDING_LAST_DAMAGE_EVENT_OVERRIDE_FLAG(FALSE)
	
	#IF IS_DEBUG_BUILD
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iDMModSet = 0
	#ENDIF

	PLAYER_MODIFIER_DATA sEmpty
	COPY_SCRIPT_STRUCT(sPlayerModifierData, sEmpty, SIZE_OF(PLAYER_MODIFIER_DATA))
	
	g_bDMUsingSpawnVehicle = FALSE
	
	PRINTLN("PROCESS_PLAYER_MODIFIER_CLEANUP - Cleaning up player modifier data and functionality")
	
	PRINTLN("PROCESS_PLAYER_MODIFIER_CLEANUP - sPlayerModifierData.iBoundsIndex: ", sPlayerModifierData.iBoundsIndex)
ENDPROC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Test Mode Bots
// ##### Description: Hopefully temporary method of applying modifier sets to bots in DM test mode
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC GIVE_TEST_PED_MODIFIER_SET_INVENTORY(PED_INDEX &piPed, INT iCurrentModifierSet)
	INT iAmmo
	WEAPON_TYPE wtWeaponToGive
	INT iStartingWeapon = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModifierSet].sModifierInventory.iStartWeapon
	IF iStartingWeapon > FMMC_STARTING_WEAPON_CURRENT
		wtWeaponToGive = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModifierSet].sModifierInventory.sWeaponStruct[iStartingWeapon].wtWeapon
		IF NOT IS_WEAPON_A_MELEE_WEAPON(wtWeaponToGive)
			iAmmo = GET_AMMO_AMOUNT_FROM_CREATOR(wtWeaponToGive, g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModifierSet].sModifierInventory.sWeaponStruct[iStartingWeapon].iAmmo)
		ENDIF
	ELSE
		INT iWeapon, iWeaponArrayIndex
		WEAPON_TYPE weapons[FMMC_MAX_INVENTORY_WEAPONS_DM]
		INT iWeaponAmmo[FMMC_MAX_INVENTORY_WEAPONS_DM]
		FOR iWeapon = 0 TO FMMC_MAX_INVENTORY_WEAPONS_DM-1
			IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModifierSet].sModifierInventory.sWeaponStruct[iWeapon].wtWeapon != WEAPONTYPE_INVALID
				weapons[iWeaponArrayIndex] = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModifierSet].sModifierInventory.sWeaponStruct[iWeapon].wtWeapon
				iWeaponAmmo[iWeaponArrayIndex] = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModifierSet].sModifierInventory.sWeaponStruct[iWeapon].iAmmo
				iWeaponArrayIndex++
			ENDIF
		ENDFOR
		INT iRandomWep = GET_RANDOM_INT_IN_RANGE(0, iWeaponArrayIndex)
		wtWeaponToGive = weapons[iRandomWep]
		IF wtWeaponToGive != WEAPONTYPE_INVALID
		AND NOT IS_WEAPON_A_MELEE_WEAPON(wtWeaponToGive)
			iAmmo = GET_AMMO_AMOUNT_FROM_CREATOR(wtWeaponToGive, iWeaponAmmo[iRandomWep])
		ENDIF
	ENDIF
	
	IF wtWeaponToGive != WEAPONTYPE_INVALID
		GIVE_WEAPON_TO_PED(piPed, wtWeaponToGive, iAmmo, TRUE)
		PRINTLN("GIVE_TEST_PED_MODIFIER_SET_INVENTORY - Giving ped ", GET_WEAPON_NAME(wtWeaponToGive))
	ENDIF
	
	IF iStartingWeapon = FMMC_STARTING_WEAPON_UNARMED
		SET_CURRENT_PED_WEAPON(piPed, WEAPONTYPE_UNARMED)
		PRINTLN("GIVE_TEST_PED_MODIFIER_SET_INVENTORY - Setting ped as unarmed to start")
	ENDIF
	
ENDPROC

PROC APPLY_MOD_SET_TO_TEST_MODE_BOT(PED_INDEX &piPed, INT iCurrentModifierSet)
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModifierSet)
		EXIT
	ENDIF
	
	GIVE_TEST_PED_MODIFIER_SET_INVENTORY(piPed, iCurrentModifierSet)
	
ENDPROC


