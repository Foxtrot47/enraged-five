
//-- Dave W - Header for Biker Defend missions. 

//----------------------
//	INCLUDES
//----------------------
USING "globals.sch"
USING "net_prints.sch"
USING "net_gang_boss_common.sch"
USING "net_include.sch"

/*
	FACTORY_ID_WEED_1 - Paleto
	FACTORY_ID_WEED_2 - City
	FACTORY_ID_WEED_3 - Countryside
	FACTORY_ID_WEED_4 - Docks
	
	
	RETRIEVAL - METH
	SHOOTOUT - WEED
	GETAWAY - FAKEID
	SNITCH - COKE
	CRASH - Cash
*/


FUNC MODEL_NAMES GET_GB_BIKER_DEFEND_PED_MODEL(INT iVariation, FACTORY_ID factoryId, INT iPed)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1
					SWITCH iPed
						CASE 0	RETURN G_M_Y_Lost_01
						CASE 1	RETURN G_F_Y_Lost_01
						CASE 2	RETURN G_F_Y_Lost_01
						CASE 3	RETURN G_M_Y_Lost_01
						CASE 4	RETURN G_M_Y_Lost_01
						CASE 5	RETURN G_F_Y_Lost_01
						CASE 6	RETURN G_M_Y_Lost_01
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_2
					SWITCH iPed
						CASE 0	RETURN G_M_Y_Lost_01
						CASE 1	RETURN G_F_Y_Lost_01
						CASE 2	RETURN G_F_Y_Lost_01
						CASE 3	RETURN G_M_Y_Lost_01
						CASE 4	RETURN G_M_Y_Lost_01
						CASE 5	RETURN G_F_Y_Lost_01
						CASE 6	RETURN G_M_Y_Lost_01
					ENDSWITCH
				BREAK
				
				
				
				CASE FACTORY_ID_WEED_3
					SWITCH iPed
						CASE 0	RETURN G_M_Y_Lost_01
						CASE 1	RETURN G_F_Y_Lost_01
						CASE 2	RETURN G_F_Y_Lost_01
						CASE 3	RETURN G_M_Y_Lost_01
						CASE 4	RETURN G_M_Y_Lost_01
						CASE 5	RETURN G_F_Y_Lost_01
						CASE 6	RETURN G_M_Y_Lost_01
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_4
					SWITCH iPed
						CASE 0	RETURN G_M_Y_MexGoon_02
						CASE 1	RETURN G_F_Y_Vagos_01
						CASE 2	RETURN G_F_Y_Vagos_01
						CASE 3	RETURN G_F_Y_Vagos_01
						CASE 4	RETURN G_M_Y_MexGoon_02
						CASE 5	RETURN G_M_Y_MexGoon_02
						CASE 6	RETURN G_M_Y_MexGoon_02
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1
					SWITCH iPed
						CASE 0	RETURN G_M_Y_Lost_01
						CASE 1	RETURN G_M_Y_Lost_01 // G_F_Y_Lost_01 remove for script memor
						CASE 2	RETURN G_M_Y_Lost_01
						CASE 3	RETURN G_M_Y_Lost_01
						CASE 4	RETURN G_M_Y_Lost_01
						CASE 5	RETURN G_M_Y_Lost_01
						CASE 6	RETURN G_M_Y_Lost_01
						CASE 7	RETURN G_M_Y_Lost_01
						CASE 8	RETURN G_M_Y_Lost_01
						CASE 9	RETURN G_M_Y_Lost_01
						CASE 10	RETURN G_M_Y_Lost_01
						CASE 11	RETURN G_M_Y_Lost_01
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_2
					SWITCH iPed
						CASE 0	RETURN G_M_Y_BallaOrig_01
						CASE 1	RETURN G_M_Y_BallaOrig_01
						CASE 2	RETURN G_M_Y_BallaOrig_01
						CASE 3	RETURN G_M_Y_BallaOrig_01
						CASE 4	RETURN G_M_Y_BallaOrig_01
						CASE 5	RETURN G_M_Y_BallaOrig_01
						CASE 6	RETURN G_M_Y_BallaOrig_01
						CASE 7	RETURN G_M_Y_BallaOrig_01
						CASE 8	RETURN G_M_Y_BallaOrig_01
						CASE 9	RETURN G_M_Y_BallaOrig_01
						CASE 10	RETURN G_M_Y_BallaOrig_01
						CASE 11	RETURN G_M_Y_BallaOrig_01
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_3
					SWITCH iPed
						CASE 0	RETURN A_M_M_Hillbilly_01
						CASE 1	RETURN A_M_M_Hillbilly_02
						CASE 2	RETURN A_M_M_Hillbilly_01
						CASE 3	RETURN A_M_M_Hillbilly_02
						CASE 4	RETURN A_M_M_Hillbilly_01
						CASE 5	RETURN A_M_M_Hillbilly_02
						CASE 6	RETURN A_M_M_Hillbilly_01
						CASE 7	RETURN A_M_M_Hillbilly_02
						CASE 8	RETURN A_M_M_Hillbilly_01
						CASE 9	RETURN A_M_M_Hillbilly_02
						CASE 10	RETURN A_M_M_Hillbilly_01
						CASE 11	RETURN A_M_M_Hillbilly_02
					ENDSWITCH
				BREAK
				
				 
				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iPed
						CASE 0	RETURN mp_g_m_pros_01
						CASE 1	RETURN mp_g_m_pros_01
						CASE 2	RETURN mp_g_m_pros_01
						CASE 3	RETURN mp_g_m_pros_01
						CASE 4	RETURN mp_g_m_pros_01
						CASE 5	RETURN mp_g_m_pros_01
						CASE 6	RETURN mp_g_m_pros_01
						CASE 7	RETURN mp_g_m_pros_01
						CASE 8	RETURN mp_g_m_pros_01
						CASE 9	RETURN mp_g_m_pros_01
						CASE 10	RETURN mp_g_m_pros_01
						CASE 11	RETURN mp_g_m_pros_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK

		
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryID
				CASE FACTORY_ID_METH_1 
					SWITCH iPed
						CASE 0			RETURN G_M_Y_Lost_01
						CASE 1			RETURN G_M_Y_Lost_01
						CASE 2			RETURN G_F_Y_Lost_01
						CASE 3			RETURN G_F_Y_Lost_01
						CASE 4			RETURN G_F_Y_Lost_01
						CASE 5			RETURN G_M_Y_Lost_01
						CASE 6			RETURN G_M_Y_Lost_01

					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_2
					SWITCH iPed
						CASE 0			RETURN G_M_Y_BallaOrig_01
						CASE 1			RETURN G_M_Y_BallaOrig_01
						CASE 2			RETURN G_M_Y_BallaOrig_01
						CASE 3			RETURN G_M_Y_BallaOrig_01
						CASE 4			RETURN G_M_Y_BallaOrig_01
						CASE 5			RETURN G_M_Y_BallaOrig_01
						CASE 6			RETURN G_M_Y_BallaOrig_01


					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_3 
					SWITCH iPed
						CASE 0			RETURN A_M_M_Hillbilly_01
						CASE 1			RETURN A_M_M_Hillbilly_02
						CASE 2			RETURN A_M_M_Hillbilly_02
						CASE 3			RETURN A_M_M_Hillbilly_02
						CASE 4			RETURN A_M_M_Hillbilly_01
						CASE 5			RETURN A_M_M_Hillbilly_01
						CASE 6			RETURN A_M_M_Hillbilly_02



					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_4
					SWITCH iPed
						CASE 0			RETURN G_M_M_ArmGoon_01
						CASE 1			RETURN G_M_M_ArmGoon_01
						CASE 2			RETURN G_M_M_ArmGoon_01
						CASE 3			RETURN G_M_M_ArmGoon_01
						CASE 4			RETURN G_M_M_ArmBoss_01
						CASE 5			RETURN G_M_M_ArmGoon_01
						CASE 6			RETURN G_M_M_ArmGoon_01


					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					SWITCH iPed
						CASE 0	RETURN A_M_M_Hillbilly_02
						CASE 1	RETURN S_M_Y_Sheriff_01
						CASE 2	RETURN S_M_Y_Sheriff_01
						CASE 3	RETURN S_F_Y_Sheriff_01
						CASE 4	RETURN S_F_Y_Sheriff_01
						CASE 5	RETURN S_M_Y_Sheriff_01
						CASE 6	RETURN S_M_Y_Sheriff_01
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_CRACK_2
					SWITCH iPed
						CASE 0	RETURN s_m_y_robber_01
						CASE 1	RETURN S_M_Y_Cop_01
						CASE 2	RETURN S_M_Y_Cop_01
						CASE 3	RETURN S_F_Y_Cop_01
						CASE 4	RETURN S_F_Y_Cop_01
						CASE 5	RETURN S_M_Y_Cop_01
						CASE 6	RETURN S_M_Y_Cop_01
					ENDSWITCH
				BREAK
				
				
				CASE FACTORY_ID_CRACK_3
					SWITCH iPed
						CASE 0	RETURN s_m_y_robber_01
						CASE 1	RETURN S_M_Y_Sheriff_01
						CASE 2	RETURN S_M_Y_Sheriff_01
						CASE 3	RETURN S_F_Y_Sheriff_01
						CASE 4	RETURN S_F_Y_Sheriff_01
						CASE 5	RETURN S_M_Y_Sheriff_01
						CASE 6	RETURN S_M_Y_Sheriff_01
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_4
					SWITCH iPed
						CASE 0	RETURN A_M_Y_BusiCas_01
						CASE 1	RETURN S_M_Y_Cop_01
						CASE 2	RETURN S_M_Y_Cop_01
						CASE 3	RETURN S_M_Y_Cop_01
						CASE 4	RETURN S_M_Y_Cop_01
						CASE 5	RETURN S_M_Y_Cop_01
						CASE 6	RETURN S_M_Y_Cop_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK



		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iPed
						CASE 0	RETURN mp_g_m_pros_01
						CASE 1	RETURN mp_g_m_pros_01
						CASE 2	RETURN mp_g_m_pros_01
						CASE 3	RETURN mp_g_m_pros_01
						CASE 4	RETURN G_M_M_ChiBoss_01
						CASE 5	RETURN G_M_M_ChiGoon_02
						CASE 6	RETURN G_M_M_ChiGoon_02
						CASE 7	RETURN G_M_M_ChiGoon_02
						CASE 8	RETURN G_M_M_ChiGoon_02

					ENDSWITCH
				BREAK
				
				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iPed
						CASE 0	RETURN mp_g_m_pros_01
						CASE 1	RETURN mp_g_m_pros_01
						CASE 2	RETURN mp_g_m_pros_01
						CASE 3	RETURN mp_g_m_pros_01
						CASE 4	RETURN G_M_M_ChiBoss_01
						CASE 5	RETURN G_M_M_ChiGoon_02
						CASE 6	RETURN G_M_M_ChiGoon_02
						CASE 7	RETURN G_M_M_ChiGoon_02
						CASE 8	RETURN G_M_M_ChiGoon_02

					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iPed
						CASE 0			RETURN G_M_Y_Lost_01
						CASE 1			RETURN G_F_Y_Lost_01
						CASE 2			RETURN G_M_Y_Lost_01
						CASE 3			RETURN G_M_Y_Lost_01
						CASE 4			RETURN G_F_Y_Lost_01
						CASE 5			RETURN A_M_M_Hillbilly_02
						CASE 6			RETURN A_M_M_Hillbilly_01
						CASE 7			RETURN A_M_M_Hillbilly_01
						CASE 8			RETURN A_M_M_Hillbilly_02
						CASE 9			RETURN A_M_M_Hillbilly_02
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iPed
						CASE 0			RETURN mp_g_m_pros_01
						CASE 1			RETURN mp_g_m_pros_01
						CASE 2			RETURN mp_g_m_pros_01
						CASE 3			RETURN mp_g_m_pros_01
						CASE 4			RETURN mp_g_m_pros_01
						CASE 5			RETURN G_M_Y_Korean_01
						CASE 6			RETURN G_M_Y_Korean_01
						CASE 7			RETURN G_M_Y_Korean_01
						CASE 8			RETURN G_M_M_Korboss_01
						CASE 9			RETURN G_M_Y_Korean_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK



		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					SWITCH iPed
						CASE 0			RETURN S_M_M_CIASec_01		
						CASE 1			RETURN S_M_M_CIASec_01
						CASE 2			RETURN S_M_M_CIASec_01
						CASE 3			RETURN S_M_M_CIASec_01
						CASE 4			RETURN S_M_M_CIASec_01
						CASE 5			RETURN S_M_M_CIASec_01
						CASE 6			RETURN S_M_M_CIASec_01
						CASE 7			RETURN S_M_M_CIASec_01		
						CASE 8			RETURN S_M_M_CIASec_01
						CASE 9			RETURN S_M_M_CIASec_01

					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_GB_BIKER_DEFEND_PLAYER_VEH_MODEL(INT iVariation, FACTORY_ID factoryId, INT iVeh)
	SWITCH iVariation

		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1
				
					SWITCH iVeh
						CASE 0	RETURN GBURRITO
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_2
					SWITCH iVeh
						CASE 0	RETURN rumpo3
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_3
					SWITCH iVeh
						CASE 0	RETURN BODHI2
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_4
					SWITCH iVeh
						CASE 0	RETURN schafter5
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_GB_BIKER_DEFEND_VEH_MODEL(INT iVariation, FACTORY_ID factoryId, INT iVeh)
	//CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     GB_BIKER_DEFEND [CREATE_ENEMY_VEHICLE] GET_GB_BIKER_DEFEND_VEH_MODEL iVariation = ", iVariation, " factoryId = ", ENUM_TO_INT(factoryId)," iVeh = ", iVeh) 
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1
					SWITCH iVeh
						CASE 0	RETURN GBURRITO
						CASE 1	RETURN GBURRITO

					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_2
					SWITCH iVeh
						CASE 0	RETURN GBURRITO
						CASE 1	RETURN GBURRITO

					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_3
					SWITCH iVeh
						CASE 0	RETURN GBURRITO
						CASE 1	RETURN GBURRITO

					ENDSWITCH
				BREAK
				CASE FACTORY_ID_WEED_4
					SWITCH iVeh
						CASE 0	RETURN cavalcade2
						CASE 1	RETURN cavalcade2

					ENDSWITCH
				BREAK
				
				
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1
					SWITCH iVeh
						CASE 0		RETURN GBURRITO
						CASE 1		RETURN DAEMON
						CASE 2		RETURN DAEMON
						
						CASE 3		RETURN GBURRITO
						CASE 4		RETURN DAEMON
						CASE 5		RETURN DAEMON
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_2
					SWITCH iVeh
						CASE 0		RETURN BALLER
						CASE 1		RETURN RUFFIAN
						CASE 2		RETURN RUFFIAN
						
						CASE 3		RETURN BALLER
						CASE 4		RETURN RUFFIAN
						CASE 5		RETURN RUFFIAN
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_3
					SWITCH iVeh
						CASE 0		RETURN BODHI2
						CASE 1		RETURN SANCHEZ
						CASE 2		RETURN SANCHEZ
						
						CASE 3		RETURN BODHI2
						CASE 4		RETURN SANCHEZ
						CASE 5		RETURN SANCHEZ
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iVeh
						CASE 0		RETURN SPEEDO
						CASE 1		RETURN BATI
						CASE 2		RETURN BATI
						
						CASE 3		RETURN SPEEDO
						CASE 4		RETURN BATI
						CASE 5		RETURN BATI
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
//		CASE GB_BIKER_DEFEND_METH
//			SWITCH factoryID
//				CASE FACTORY_ID_METH_1 
//					SWITCH iVeh
//						CASE 0			RETURN GBURRITO
//						CASE 1			RETURN GBURRITO
//					ENDSWITCH
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					SWITCH iVeh
						CASE 0			RETURN SHERIFF
						CASE 1			RETURN SHERIFF
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_2
					SWITCH iVeh
						CASE 0			RETURN police3
						CASE 1			RETURN police3
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_3
					SWITCH iVeh
						CASE 0			RETURN SHERIFF
						CASE 1			RETURN SHERIFF
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_4
					SWITCH iVeh
						CASE 0			RETURN police3
						CASE 1			RETURN police3
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iVeh
						CASE 0			RETURN GBURRITO
						CASE 1			RETURN DINGHY
					ENDSWITCH
				BREAK
				
				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iVeh
						CASE 0			RETURN SPEEDO
						CASE 1			RETURN GBURRITO2
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iVeh
						CASE 0			RETURN  GBURRITO
						CASE 1			RETURN  REBEL
						CASE 2			RETURN  REBEL
						CASE 3			RETURN  GBURRITO
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iVeh
						CASE 0			RETURN KURUMA
						CASE 1			RETURN SULTAN
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					SWITCH iVeh
						CASE 0			RETURN  police4
						CASE 1			RETURN  police4
						CASE 2			RETURN  POLMAV
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC




FUNC MODEL_NAMES GET_GB_BIKER_DEFEND_PROP_MODEL(INT iVariation, FACTORY_ID factoryId, INT iProp)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
					SWITCH iProp
						CASE 0		RETURN prop_box_wood04a
						CASE 1		RETURN prop_box_wood04a
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_FAKE_ID
			RETURN prop_box_wood01a 

		
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryID
				CASE FACTORY_ID_METH_1 
					SWITCH iProp
						CASE 0			RETURN prop_box_wood04a
						CASE 1			RETURN prop_box_wood04a
						CASE 2			RETURN prop_boxpile_02b
						CASE 3			RETURN prop_boxpile_02b
						CASE 4			RETURN prop_box_wood01a
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_METH_2
					SWITCH iProp
						CASE 0			RETURN prop_box_wood04a
						CASE 1			RETURN prop_box_wood04a
						CASE 2			RETURN prop_boxpile_02b
						CASE 3			RETURN prop_boxpile_02b
						CASE 4			RETURN Prop_Box_Ammo03A
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_METH_3
					SWITCH iProp
						CASE 0			RETURN prop_hayb_st_01_cr
						CASE 1			RETURN prop_boxpile_04a
						CASE 2			RETURN prop_boxpile_04a
						CASE 3			RETURN prop_box_wood01a
						
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_METH_4
					SWITCH iProp
						CASE 0			RETURN prop_conc_blocks01a
						CASE 1			RETURN prop_cratepile_07a
						CASE 2			RETURN prop_conc_blocks01a
						CASE 3			RETURN prop_cratepile_07a
						CASE 4			RETURN prop_box_wood01a
						
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK


		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iProp
						CASE 0			RETURN prop_box_wood04a
						CASE 1			RETURN prop_box_wood04a
						CASE 2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))
						CASE 3			RETURN prop_box_wood04a
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))
						CASE 1			RETURN prop_gun_case_01			
						CASE 2			RETURN prop_gun_case_01
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))
						CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))
						CASE 2			RETURN  prop_gun_case_01
						CASE 3			RETURN  prop_gun_case_01
						CASE 4			RETURN  prop_gun_case_01

					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iProp
						CASE 0			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))
						CASE 1			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))
						CASE 2			RETURN prop_gun_case_01
						CASE 3			RETURN prop_gun_case_01
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					SWITCH iProp
						CASE 0			RETURN prop_barrier_work05
						CASE 1			RETURN prop_barrier_work05

					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_GB_BIKER_DEFEND_PACKAGE_MODEL(INT iVariation, FACTORY_ID factoryId, INT iPackage)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iPackage
						CASE 0		RETURN prop_drug_package

					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					SWITCH iPackage
						CASE 0		RETURN hei_prop_hei_timetable
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("bkr_Prop_Duffel_Bag_01a"))
	ENDSWITCH
	
	RETURN prop_drug_package
ENDFUNC



PROC INIT_GB_BIKER_DEFEND_PED_SPAWN_COORDS(INT iVariation, FACTORY_ID factoryId, INT iPed, VECTOR &vLoc, FLOAT &fHeading)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1
					SWITCH iPed
						CASE 0	vLoc = <<406.1125, 6495.7207, 26.9215>> 		fHeading = 172.4475		BREAK
						CASE 1	vLoc = <<410.2993, 6497.3364, 26.8923>> 		fHeading = 214.4474		BREAK
						CASE 2	vLoc = <<408.6697, 6488.7598, 27.5795>> 		fHeading = 344.2470		BREAK
						CASE 3	vLoc = <<414.4818, 6488.7813, 27.4802>> 		fHeading = 340.0468		BREAK
						CASE 4	vLoc = <<417.9521, 6521.2271, 26.7249>> 		fHeading = 340.0468		BREAK
						CASE 5	vLoc = <<417.7648, 6516.0376, 26.6688>> 		fHeading = 350.8466		BREAK
						CASE 6	vLoc = <<422.9774, 6521.3691, 26.7053>> 		fHeading = 350.8466		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_2
					SWITCH iPed
						CASE 0	vLoc = <<115.0835, 170.2639, 111.4567>> 		fHeading = 86.5977		BREAK
						CASE 1	vLoc = <<94.4770, 177.5729, 103.6162>> 			fHeading = 271.5472		BREAK
						CASE 2	vLoc = <<91.7610, 162.8384, 103.6210>> 			fHeading = 319.9465		BREAK
						CASE 3	vLoc = <<119.2783, 165.6519, 103.7327>> 		fHeading = 204.5453		BREAK
						CASE 4	vLoc = <<92.7969, 163.4393, 103.6075>> 			fHeading = 118.9450		BREAK
						CASE 5	vLoc = <<95.7130, 179.3221, 103.5720>> 			fHeading = 118.9450		BREAK
						CASE 6	vLoc = <<127.8838, 165.2367, 103.7433>> 		fHeading = 176.3443		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_3
					SWITCH iPed
						CASE 0	vLoc = <<2873.7815, 4459.6699, 47.5223>> 		fHeading = 295.5985		BREAK
						CASE 1	vLoc = <<2883.6121, 4460.2183, 47.5083>> 		fHeading = 114.9981		BREAK
						CASE 2	vLoc = <<2875.0369, 4455.4614, 47.5496>> 		fHeading = 259.5975		BREAK
						CASE 3	vLoc = <<2845.1592, 4446.1401, 47.5638>> 		fHeading = 122.9972		BREAK
						CASE 4	vLoc = <<2846.7024, 4451.9224, 47.5083>> 		fHeading = 128.5972		BREAK
						CASE 5	vLoc = <<2848.4697, 4447.9717, 47.5237>> 		fHeading = 128.5972		BREAK
						CASE 6	vLoc = <<2878.9834, 4461.6821, 47.4620>> 		fHeading = 124.9971		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_4
					SWITCH iPed
						CASE 0	vLoc = <<136.4867, -2472.6943, 5.0000>> 		fHeading = 240.5958		BREAK
						CASE 1	vLoc = <<133.6829, -2480.3855, 5.0000>> 		fHeading = 259.5956		BREAK
						CASE 2	vLoc = <<144.5504, -2483.4900, 5.0000>> 		fHeading = 247.5966		BREAK
						CASE 3	vLoc = <<147.4689, -2481.7668, 5.0000>> 		fHeading = 241.5966		BREAK
						CASE 4	vLoc = <<146.6130, -2486.8486, 5.0000>> 		fHeading = 246.1969		BREAK
						CASE 5	vLoc = <<142.6747, -2476.3577, 5.0000>> 		fHeading = 213.5970		BREAK
						CASE 6	vLoc = <<139.6597, -2479.7397, 5.0000>> 		fHeading = 244.3955		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1
					SWITCH iPed
						CASE 0	vLoc =  <<-127.4177, 6392.2749, 30.4881>>  		fHeading = 32.6140		BREAK
						CASE 1	vLoc =  <<-189.6366, 6358.6128, 30.4765>>   	fHeading = 256.5182		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK

		
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryID
				CASE FACTORY_ID_METH_1 
					SWITCH iPed
						CASE 0	vLoc = <<1690.3516, 3603.4648, 34.4254>> 		fHeading = 83.3991		BREAK
						CASE 1	vLoc = <<1694.8308, 3608.7844, 34.3479>> 		fHeading = 324.3990		BREAK
						CASE 2	vLoc = <<1695.6193, 3609.8040, 34.3329>> 		fHeading = 163.3984		BREAK
						CASE 3	vLoc = <<1690.4680, 3597.5781, 34.5488>> 		fHeading = 307.7974		BREAK
						CASE 4	vLoc = <<1691.8453, 3598.5286, 34.5507>> 		fHeading = 113.7969		BREAK
						CASE 5	vLoc = <<1696.1779, 3597.7695, 34.6025>> 		fHeading = 124.7968		BREAK
						CASE 6	vLoc = <<1699.5643, 3604.4275, 34.4282>> 		fHeading = 179.3996		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_2 
					SWITCH iPed
						CASE 0	vLoc = <<247.7620, 129.5270, 112.7010>> 		fHeading = 210.5980		BREAK
						CASE 1	vLoc = <<236.2360, 126.7850, 101.6000>> 		fHeading = 34.5980		BREAK
						CASE 2	vLoc = <<228.1990, 128.5990, 101.6000>> 		fHeading = 308.1970		BREAK
						CASE 3	vLoc = <<229.2470, 129.5670, 101.6000>> 		fHeading = 115.7970		BREAK
						CASE 4	vLoc = <<244.6905, 126.7374, 101.5993>> 		fHeading = 131.1988		BREAK
						CASE 5	vLoc = <<243.8857, 126.1607, 101.5993>> 		fHeading = 294.9992		BREAK
						CASE 6	vLoc = <<240.1743, 115.6268, 101.6225>> 		fHeading = 216.9994		BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_METH_3 
					SWITCH iPed
						CASE 0	vLoc = <<2400.8735, 5027.4287, 45.0084>> 		fHeading = 196.19806	BREAK
						CASE 1	vLoc = <<2405.3359, 5026.2871, 45.0220>> 		fHeading = 64.79757		BREAK
						CASE 2	vLoc = <<2404.3191, 5026.6279, 44.9931>> 		fHeading = 248.99736	BREAK
						CASE 3	vLoc = <<2407.7192, 5033.1670, 44.9933>> 		fHeading = 104.79710	BREAK
						CASE 4	vLoc = <<2398.4707, 5030.6577, 44.9951>> 		fHeading = 135.39706	BREAK
						CASE 5	vLoc = <<2403.4412, 5034.2363, 44.9932>> 		fHeading = 122.99656	BREAK
						CASE 6	vLoc = <<2402.3516, 5033.1372, 44.9932>> 		fHeading = 302.3962		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_4 
					SWITCH iPed
						CASE 0	vLoc = <<-1124.1759, -1453.8003, 7.3482>> 		fHeading = 58.7996	BREAK
						CASE 1	vLoc = <<-1130.2506, -1452.5344, 3.9380>> 		fHeading = 193.3994	BREAK
						CASE 2	vLoc = <<-1131.6995, -1437.3601, 3.9093>> 		fHeading = 300.9989	BREAK
						CASE 3	vLoc = <<-1137.5421, -1448.7032, 3.7750>> 		fHeading = 247.3954	BREAK
						CASE 4	vLoc = <<-1136.3081, -1449.2018, 3.7523>> 		fHeading = 58.1954	BREAK
						CASE 5	vLoc = <<-1143.3463, -1445.1925, 3.6430>> 		fHeading = 123.5981	BREAK
						CASE 6	vLoc = <<-1126.3807, -1447.5673, 4.0529>> 		fHeading = 215.9979	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK

		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					SWITCH iPed
						CASE 0	vLoc = <<-687.1215, 5760.5044, 16.5110>> 		fHeading = 125.79836	BREAK
						CASE 1	vLoc = <<-688.3340, 5760.0098, 16.5110>> 		fHeading = 294.99787	BREAK
						CASE 2	vLoc = <<-689.9678, 5772.7573, 16.3310>> 		fHeading = 76.99716		BREAK
						CASE 3	vLoc = <<-692.9852, 5769.0820, 16.3310>> 		fHeading = 167.39630	BREAK
						CASE 4	vLoc = <<-693.3242, 5767.9321, 16.3310>> 		fHeading = 348.59576	BREAK
						CASE 5	vLoc = <<-704.3570, 5772.3516, 16.3309>> 		fHeading = 60.79926		BREAK
						CASE 6	vLoc = <<-690.1846, 5764.6875, 16.3310>> 		fHeading = 15.5955		BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_CRACK_2
					SWITCH iPed
						CASE 0	vLoc = <<344.3741, -204.9075, 57.0242>> 		fHeading = 140.3993	BREAK
						CASE 1	vLoc = <<343.7755, -205.6453, 57.0242>> 		fHeading = 318.5989	BREAK
						CASE 2	vLoc = <<333.5367, -211.8548, 53.0863>> 		fHeading = 32.3983	BREAK
						CASE 3	vLoc = <<333.0363, -211.1229, 53.0863>> 		fHeading = 218.9983	BREAK
						CASE 4	vLoc = <<342.1429, -209.2703, 57.0242>> 		fHeading = 74.8469	BREAK
						CASE 5	vLoc = <<333.1700, -220.8350, 55.2750>> 		fHeading = 43.5980	BREAK
						CASE 6	vLoc = <<325.0602, -222.0730, 53.0851>> 		fHeading = 151.6474	BREAK
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_CRACK_3
					SWITCH iPed
						CASE 0	vLoc = <<1141.8840, 2642.7561, 37.1490>> 		fHeading = 63.4000	BREAK
						CASE 1	vLoc = <<1140.7469, 2643.2251, 37.1490>> 		fHeading = 256.4000	BREAK
						CASE 2	vLoc = <<1131.0630, 2661.7681, 36.9980>> 		fHeading = 135.5990	BREAK
						CASE 3	vLoc = <<1124.0990, 2650.8711, 36.9970>> 		fHeading = 6.6000	BREAK
						CASE 4	vLoc = <<1130.1379, 2660.6111, 36.9970>> 		fHeading = 332.3990	BREAK
						CASE 5	vLoc = <<1130.2290, 2642.8279, 37.1490>> 		fHeading = 17.5990	BREAK
						CASE 6	vLoc = <<1138.5750, 2645.7400, 36.9960>> 		fHeading = 33.9990	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_4
					SWITCH iPed
						CASE 0	vLoc = <<561.1011, -1747.8079, 32.4476>> 		fHeading = 206.3958		BREAK
						CASE 1	vLoc = <<565.3523, -1750.1691, 28.2839>> 		fHeading = 339.5988		BREAK
						CASE 2	vLoc = <<564.3752, -1776.4705, 30.6977>> 		fHeading = 331.5970		BREAK
						CASE 3	vLoc = <<558.6710, -1756.2892, 32.4476>> 		fHeading = 252.9974		BREAK
						CASE 4	vLoc = <<561.5383, -1748.9618, 32.4476>> 		fHeading = 16.1977		BREAK
						CASE 5	vLoc = <<561.5959, -1770.3324, 28.3567>> 		fHeading = 267.9964		BREAK
						CASE 6	vLoc = <<562.7568, -1770.0918, 28.3572>> 		fHeading = 109.7960		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK

		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iPed
						CASE 0	vLoc = <<1309.1121, 4229.9199, 32.9200>> 		fHeading = 1.3990		BREAK
						CASE 1	vLoc = <<1302.9410, 4234.7852, 32.9140>> 		fHeading = 339.19700	BREAK
						CASE 2	vLoc = <<1305.7640, 4233.1631, 32.9140>> 		fHeading = 39.39900		BREAK
						CASE 3	vLoc = <<1303.0850, 4231.3579, 32.9140>> 		fHeading = 343.7960		BREAK
						CASE 4	vLoc = <<1303.2290, 4236.7212, 32.9140>> 		fHeading = 168.59400	BREAK
						CASE 5	vLoc = <<1301.6360, 4238.9082, 32.9140>> 		fHeading = 194.5940		BREAK
						CASE 6	vLoc = <<1305.5200, 4239.9131, 32.9140>> 		fHeading = 154.1940		BREAK
						CASE 7	vLoc = <<1304.6580, 4256.1948, 32.9140>> 		fHeading = 318.9980		BREAK
						CASE 8	vLoc = <<1303.4740, 4242.2261, 32.9140>> 		fHeading = 173.1930		BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iPed
						CASE 0	vLoc = <<1657.5980, 4.2860, 165.1180>>	 		fHeading = 223.7990		BREAK
						CASE 1	vLoc = <<1661.8160, 3.1630, 165.1180>>	 		fHeading = 180.99800	BREAK
						CASE 2	vLoc = <<1663.6899, 4.1100, 165.1180>>	 		fHeading = 150.3980		BREAK
						CASE 3	vLoc = <<1662.2970, 7.6410, 165.0680>>	 		fHeading = 8.3980		BREAK
						CASE 4	vLoc = <<1661.8130, 0.9190, 165.1180>>	 		fHeading = 350.79800	BREAK
						CASE 5	vLoc = <<1661.5090, -1.8150, 165.1180>>	 		fHeading = 21.3970		BREAK
						CASE 6	vLoc = <<1663.6320, -0.6020, 165.1180>>	 		fHeading = 0.9970		BREAK
						CASE 7	vLoc = <<1664.0031, -5.8700, 165.1180>>	 		fHeading = 202.3970		BREAK
						CASE 8	vLoc = <<1659.8800, 0.6000, 165.1180>>	 		fHeading = 342.1970		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iPed
						CASE 0	vLoc = <<2354.2161, 3132.5549, 47.2090>> 		fHeading = 78.6000	BREAK
						CASE 1	vLoc = <<2357.6230, 3127.4399, 47.2090>> 		fHeading = 64.1990	BREAK
						CASE 2	vLoc = <<2356.5430, 3134.5229, 47.2090>> 		fHeading = 97.7990	BREAK
						CASE 3	vLoc = <<2362.1021, 3133.8660, 47.2090>> 		fHeading = 85.3990	BREAK
						CASE 4	vLoc = <<2367.2180, 3131.6570, 47.2490>> 		fHeading = 260.9990	BREAK
						CASE 5	vLoc = <<2338.9417, 3136.3328, 47.2066>> 		fHeading = 69.5990	BREAK
						CASE 6	vLoc = <<2351.3430, 3133.2209, 47.2087>> 		fHeading = 255.5984	BREAK
						CASE 7	vLoc = <<2349.5381, 3131.6614, 47.2087>> 		fHeading = 269.9975	BREAK
						CASE 8	vLoc = <<2350.2964, 3135.5115, 47.2087>> 		fHeading = 233.3977	BREAK
						CASE 9	vLoc = <<2349.2595, 3134.8220, 47.2087>> 		fHeading = 254.3975	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iPed
						CASE 0	vLoc = <<-440.8068, -983.9050, 24.9031>> 		fHeading = 198.7998		BREAK
						CASE 1	vLoc = <<-442.9531, -990.5599, 22.8330>> 		fHeading = 374.99850	BREAK
						CASE 2	vLoc = <<-443.5239, -985.6843, 22.8178>> 		fHeading = 390.1984		BREAK
						CASE 3	vLoc = <<-449.6893, -976.2077, 24.9032>> 		fHeading = 3172.5990	BREAK
						CASE 4	vLoc = <<-445.1724, -987.8193, 22.7599>> 		fHeading = 189.39860	BREAK
						CASE 5	vLoc = <<-460.1397, -983.6872, 22.5453>> 		fHeading = 155.9955		BREAK
						CASE 6	vLoc = <<-449.7830, -986.0192, 22.6078>> 		fHeading = 1257.9952	BREAK
						CASE 7	vLoc = <<-449.5731, -988.4716, 22.6117>> 		fHeading = 3288.5952	BREAK
						CASE 8	vLoc = <<-447.8551, -987.6537, 22.6703>> 		fHeading = 1253.7949	BREAK
						CASE 9	vLoc = <<-455.6019, -987.0480, 22.5453>> 		fHeading = 1324.3943	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			
		BREAK


		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
					SWITCH iPed
						CASE 0	vLoc = <<51.5610, 6337.9663, 30.3812>> 		fHeading = 17.1981	BREAK
						CASE 1	vLoc = <<38.1361, 6351.1631, 30.2398>> 		fHeading = 205.5979	BREAK
						CASE 2	vLoc = <<58.1027, 6342.3667, 30.2260>> 		fHeading = 83.9977	BREAK
						CASE 3	vLoc = <<43.9142, 6334.4160, 30.3668>> 		fHeading = 352.9974	BREAK
						CASE 4	vLoc = <<35.6928, 6379.9688, 88.3791>> 		fHeading = 203.3974	BREAK
						CASE 5	vLoc = <<36.3193, 6379.2822, 88.1211>> 		fHeading = 203.3974	BREAK
						CASE 6	vLoc = <<36.3193, 6379.2822, 88.1211>> 		fHeading = 203.3974	BREAK
						CASE 7	vLoc = <<74.8904, 6377.7236, 30.2394>> 		fHeading = 269.9975	BREAK
						CASE 8	vLoc = <<79.6583, 6354.1938, 30.3759>> 		fHeading = 233.3977	BREAK
						CASE 9	vLoc = <<67.5955, 6321.0674, 30.3710>> 		fHeading = 254.3975	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_2
					SWITCH iPed
						CASE 0	vLoc = <<1449.6958, -1652.8284, 64.8268>>	 		fHeading = 1.7984				BREAK
						CASE 1	vLoc = <<1465.2133, -1643.5917, 67.6959>>	 		fHeading = 29.7980				BREAK
						CASE 2	vLoc = <<1434.6486, -1654.2078, 62.1111>>	 		fHeading = 314.1978				BREAK
						CASE 3	vLoc = <<1457.3384, -1650.4194, 66.0953>>	 		fHeading = 47.1976				BREAK
						CASE 4	vLoc = <<1424.4821, -1616.7404, 124.0498>>	 		fHeading = 213.5477				BREAK
						CASE 5	vLoc = <<1423.3065, -1614.8804, 124.1826>>	 		fHeading = 213.5477				BREAK
						CASE 6	vLoc = <<1423.3065, -1614.8804, 124.1826>>	 		fHeading = 213.5477				BREAK
						CASE 7	vLoc = <<1424.1571, -1646.7726, 60.5027>>	 		fHeading = 52.9999				BREAK
						CASE 8	vLoc = <<1474.0764, -1638.5048, 68.6667>>	 		fHeading = 340.9997				BREAK
						CASE 9	vLoc = <<1440.9353, -1669.9475, 65.6504>>	 		fHeading = 31.9983				BREAK
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_METH_3
					SWITCH iPed
						CASE 0	vLoc =  <<203.3479, 2462.1074, 54.7058>>	 		fHeading = 175.7996			BREAK
						CASE 1	vLoc =  <<209.1309, 2467.3513, 54.6871>>	 		fHeading = 240.7995			BREAK
						CASE 2	vLoc =  <<208.0104, 2447.2959, 56.8825>>	 		fHeading = 282.7993			BREAK
						CASE 3	vLoc =  <<204.1095, 2452.3474, 56.1404>>	 		fHeading = 353.3970			BREAK
						CASE 4	vLoc =  <<263.7463, 2451.2295, 124.7385>> 			fHeading = 68.5968			BREAK
						CASE 5	vLoc =  <<264.6442, 2450.8806, 125.0143>> 			fHeading = 68.5968			BREAK
						CASE 6	vLoc =  <<264.6442, 2450.8806, 125.0143>> 			fHeading = 68.5968			BREAK
						CASE 7	vLoc =  <<219.3545, 2484.9033, 54.1257>>	 		fHeading = -2.6013			BREAK
						CASE 8	vLoc =  <<224.0474, 2438.7590, 57.4561>>	 		fHeading = 154.7982			BREAK
						CASE 9	vLoc =  <<210.4050, 2463.1848, 54.9640>>	 		fHeading = 120.5980			BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_4
					SWITCH iPed
						CASE 0	vLoc =  <<1180.2961, -3112.3293, 5.0280>>	 		fHeading = 99.5988			BREAK
						CASE 1	vLoc =  <<1175.6754, -3126.6543, 5.0280>>	 		fHeading = 61.5985			BREAK
						CASE 2	vLoc =  <<1176.8580, -3104.4612, 5.0280>>	 		fHeading = 183.3982			BREAK
						CASE 3	vLoc =  <<1181.1052, -3118.0916, 5.0280>>	 		fHeading = 43.7978			BREAK
						CASE 4	vLoc =  <<1141.3502, -3110.5366, 75.9948>>			fHeading = 282.5974			BREAK
						CASE 5	vLoc =  <<1140.0980, -3110.3132, 76.3252>>			fHeading = 282.5974			BREAK
						CASE 6	vLoc =  <<1140.0980, -3110.3132, 76.3252>>			fHeading = 282.5974			BREAK
						CASE 7	vLoc =  <<1159.8777, -3121.3843, 4.9003>>	 		fHeading = 89.1999			BREAK
						CASE 8	vLoc =  <<1177.1815, -3108.0916, 5.0280>>	 		fHeading = 25.5989			BREAK
						CASE 9	vLoc =  <<1185.1003, -3108.3811, 5.0280>>	 		fHeading = 233.1984			BREAK
					ENDSWITCH
				BREAK

			
				CASE FACTORY_ID_WEED_1
					SWITCH iPed
						CASE 0	vLoc =  <<416.7668, 6519.5659, 26.6965>>	 		fHeading = 292.5990			BREAK
						CASE 1	vLoc =  <<433.8810, 6536.5786, 27.0296>>	 		fHeading = 102.1971			BREAK
						CASE 2	vLoc =  <<429.2975, 6504.0576, 27.3953>>	 		fHeading = 65.9970			BREAK
						CASE 3	vLoc =  <<417.7043, 6539.6333, 26.6770>>	 		fHeading = 318.9962			BREAK
						CASE 4	vLoc =  <<442.7939, 6549.2690, 72.1564>>			fHeading = 129.1956			BREAK
						CASE 5	vLoc =  <<442.7939, 6549.2690, 72.1564>>			fHeading = 129.1956			BREAK
						CASE 6	vLoc =  <<442.7939, 6549.2690, 72.1564>>			fHeading = 129.1956			BREAK
						CASE 7	vLoc =  <<434.6517, 6552.5220, 26.5632>>	 		fHeading = 353.7996			BREAK
						CASE 8	vLoc =  <<426.8210, 6498.4683, 27.0759>>	 		fHeading = 23.3985			BREAK
						CASE 9	vLoc =  <<416.3445, 6541.6479, 26.5680>>	 		fHeading = 199.7977			BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_2
					SWITCH iPed
						CASE 0	vLoc =  <<102.3849, 173.3189, 103.5867>>		 		fHeading = 151.9972			BREAK
						CASE 1	vLoc =  <<111.6505, 168.0158, 103.7048>>		 		fHeading = 233.1969			BREAK
						CASE 2	vLoc =  <<114.6820, 170.9016, 111.4567>>		 		fHeading = 162.3936			BREAK
						CASE 3	vLoc =  <<93.4121, 175.6396, 103.6263>>		 			fHeading = 58.3930			BREAK
						CASE 4	vLoc =  <<69.2631, 137.0533, 166.6731>>					fHeading = 318.9926			BREAK
						CASE 5	vLoc =  <<69.2631, 137.0532, 166.6730>>					fHeading = 318.9926			BREAK
						CASE 6	vLoc =  <<69.2631, 137.0532, 166.6730>>					fHeading = 318.9926			BREAK
						CASE 7	vLoc =  <<72.0584, 184.2480, 103.8951>>		 			fHeading = 62.0000			BREAK
						CASE 8	vLoc =  <<130.8619, 163.1978, 103.6192>>		 		fHeading = 267.1991			BREAK
						CASE 9	vLoc =  <<111.0224, 171.1221, 103.6295>>		 		fHeading = 124.9986			BREAK
					ENDSWITCH
				BREAK
			
				CASE FACTORY_ID_WEED_3
					SWITCH iPed
						CASE 0	vLoc =  <<2847.8220, 4451.4663, 47.5091>>		 		fHeading = 138.3970			BREAK
						CASE 1	vLoc =  <<2845.4841, 4479.9580, 47.5654>>		 		fHeading = 138.3970			BREAK
						CASE 2	vLoc =  <<2857.8848, 4445.6348, 47.5394>>		 		fHeading = 132.7970			BREAK
						CASE 3	vLoc =  <<2850.3164, 4433.4971, 47.8439>>	 			fHeading = 31.5963			BREAK
						CASE 4	vLoc =  <<2827.9231, 4404.1890, 94.1047>>				fHeading = 320.7959			BREAK
						CASE 5	vLoc =  <<2827.9231, 4404.1890, 94.1047>>				fHeading = 320.7959			BREAK
						CASE 6	vLoc =  <<2827.9231, 4404.1890, 94.1047>>				fHeading = 320.7959			BREAK
						CASE 7	vLoc =  <<2840.6731, 4483.1875, 47.8268>>	 			fHeading = 44.7995			BREAK
						CASE 8	vLoc =  <<2858.3335, 4436.0640, 47.7770>>		 		fHeading = 27.3991			BREAK
						CASE 9	vLoc =  <<2874.7458, 4451.4248, 47.5535>>		 		fHeading = 307.1983			BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_4
					SWITCH iPed
						CASE 0	vLoc =  <<137.5898, -2472.2100, 5.0000>>			 		fHeading = 220.1976			BREAK
						CASE 1	vLoc =  <<144.4460, -2464.1628, 5.0000>>			 		fHeading = 187.1977			BREAK
						CASE 2	vLoc =  <<152.6100, -2476.9783, 4.9916>>			 		fHeading = 246.9975			BREAK
						CASE 3	vLoc =  <<130.4017, -2479.5364, 5.0000>>		 			fHeading = 246.1971			BREAK
						CASE 4	vLoc =  <<175.2240, -2491.9353, 49.1284>>					fHeading = 48.1967			BREAK
						CASE 5	vLoc =  <<175.2241, -2491.9348, 49.1285>>					fHeading = 48.1967			BREAK
						CASE 6	vLoc =  <<175.2241, -2491.9348, 49.1285>>					fHeading = 48.1967			BREAK
						CASE 7	vLoc =  <<151.7093, -2491.9199, 4.9901>>		 			fHeading = 233.5982			BREAK
						CASE 8	vLoc =  <<146.9291, -2466.1941, 5.0000>>			 		fHeading = 94.5980			BREAK
						CASE 9	vLoc =  <<119.6780, -2470.1802, 5.0000>>			 		fHeading = 107.7948			BREAK
					ENDSWITCH
				BREAK

			
				CASE FACTORY_ID_CRACK_1
					SWITCH iPed
						CASE 0	vLoc = <<51.2343, 6486.9248, 30.4253>>			 		fHeading = 238.9962			BREAK
						CASE 1	vLoc = <<60.4884, 6475.1582, 30.4253>>			 		fHeading = 238.9962			BREAK
						CASE 2	vLoc = <<61.7692, 6464.7998, 30.3324>>			 		fHeading = 298.9960			BREAK
						CASE 3	vLoc = <<43.0875, 6497.8027, 30.4253>>		 			fHeading = 21.9955			BREAK
						CASE 4	vLoc = <<124.7746, 6481.6621, 79.0894>>					fHeading = 94.1954			BREAK
						CASE 5	vLoc = <<124.7746, 6481.6621, 79.0894>>					fHeading = 94.1954			BREAK
						CASE 6	vLoc = <<124.7746, 6481.6621, 79.0894>>					fHeading = 94.1954			BREAK
						CASE 7	vLoc = <<71.2958, 6471.7671, 30.2259>>		 			fHeading = 254.5995			BREAK
						CASE 8	vLoc = <<58.4544, 6467.6919, 30.4253>>			 		fHeading = 349.3968			BREAK
						CASE 9	vLoc = <<52.8278, 6467.9893, 30.4252>>			 		fHeading = 256.3959			BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_2
					SWITCH iPed
						CASE 0	vLoc = <<-1461.2284, -381.3546, 37.8007>>		 		fHeading = 229.1984			BREAK
						CASE 1	vLoc = <<-1474.3870, -399.5579, 36.7078>>		 		fHeading = 149.9983			BREAK
						CASE 2	vLoc = <<-1444.4481, -374.1057, 42.4503>>		 		fHeading = 329.5978			BREAK
						CASE 3	vLoc = <<-1459.0367, -376.1592, 37.9571>>	 			fHeading = 28.1967			BREAK
						CASE 4	vLoc = <<-1429.4989, -381.0566, 78.3396>>				fHeading = 100.7965			BREAK
						CASE 5	vLoc = <<-1429.4989, -381.0566, 78.3396>>				fHeading = 100.7965			BREAK
						CASE 6	vLoc = <<-1429.1761, -381.0665, 78.3471>>				fHeading = 100.7965			BREAK
						CASE 7	vLoc = <<-1444.7719, -377.4260, 37.3412>>	 			fHeading = 285.1988			BREAK
						CASE 8	vLoc = <<-1476.5850, -405.3120, 36.3396>>		 		fHeading = 349.3979			BREAK
						CASE 9	vLoc = <<-1465.0547, -394.1414, 37.2545>>		 		fHeading = 8.3974			BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_CRACK_3
					SWITCH iPed
						CASE 0	vLoc = <<388.8037, 3585.3972, 32.2922>>		 		fHeading = 35.3988		BREAK
						CASE 1	vLoc = <<399.9077, 3584.1377, 32.2922>>		 		fHeading = 278.3985		BREAK
						CASE 2	vLoc = <<374.2202, 3596.4548, 32.4686>>		 		fHeading = 277.1985		BREAK
						CASE 3	vLoc = <<379.4160, 3586.1611, 32.2922>>	 			fHeading = 315.1983		BREAK
						CASE 4	vLoc = <<373.0628, 3627.0146, 75.3474>>				fHeading = 201.7981		BREAK
						CASE 5	vLoc = <<373.0628, 3627.0146, 75.3474>>				fHeading = 201.7981		BREAK
						CASE 6	vLoc = <<373.0628, 3627.0146, 75.3474>>				fHeading = 201.7981		BREAK
						CASE 7	vLoc = <<402.2075, 3592.7793, 32.2218>>	 			fHeading = 284.9995		BREAK
						CASE 8	vLoc = <<420.5654, 3584.5891, 32.2386>>		 		fHeading = 67.5991		BREAK
						CASE 9	vLoc = <<390.8926, 3559.9868, 32.3198>>		 		fHeading = 131.3983		BREAK
					ENDSWITCH
				BREAK
		
				CASE FACTORY_ID_CRACK_4
					SWITCH iPed
						CASE 0	vLoc =  <<-253.8698, -2592.1277, 5.0006>>			 		fHeading = 44.1989		BREAK
						CASE 1	vLoc =  <<-266.1535, -2573.4844, 5.0006>>			 		fHeading = 198.1980		BREAK
						CASE 2	vLoc =  <<-260.3382, -2601.9590, 5.0006>>			 		fHeading = 43.7978		BREAK
						CASE 3	vLoc =  <<-255.2566, -2580.1770, 5.0006>>		 			fHeading = 132.5972		BREAK
						CASE 4	vLoc =  <<-295.0537, -2574.4639, 52.2955>>					fHeading = 232.1968		BREAK
						CASE 5	vLoc =  <<-295.0537, -2574.4639, 52.2955>>					fHeading = 232.1968		BREAK
						CASE 6	vLoc =  <<-295.0537, -2574.4639, 52.2955>>					fHeading = 232.1968		BREAK
						CASE 7	vLoc =  <<-267.8810, -2606.4268, 5.0272>>		 			fHeading = 158.9996		BREAK
						CASE 8	vLoc =  <<-283.8436, -2573.2925, 5.0006>>			 		fHeading = 259.7993		BREAK
						CASE 9	vLoc =  <<-247.5822, -2600.4370, 5.0007>>			 		fHeading = 351.5983		BREAK
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iPed
						CASE 0	vLoc =  <<-412.4442, 6171.2266, 30.4782>>		 		fHeading = 318.3984		BREAK
						CASE 1	vLoc =  <<-403.1873, 6161.6357, 30.4788>>		 		fHeading = 318.3984		BREAK
						CASE 2	vLoc =  <<-416.9733, 6181.8330, 30.4782>>		 		fHeading = 242.1979		BREAK
						CASE 3	vLoc =  <<-425.3268, 6166.2339, 30.4782>>	 			fHeading = 357.5974		BREAK
						CASE 4	vLoc =  <<-387.9090, 6193.3936, 77.6257>>				fHeading = 127.3972		BREAK
						CASE 5	vLoc =  <<-386.5088, 6194.1421, 77.9916>>				fHeading = 127.3972		BREAK
						CASE 6	vLoc =  <<-386.5088, 6194.1421, 77.9916>>				fHeading = 127.3972		BREAK
						CASE 7	vLoc =  <<-401.1820, 6146.8584, 30.4748>>	 			fHeading = 207.5995		BREAK
						CASE 8	vLoc =  <<-396.5336, 6169.4790, 30.5090>>		 		fHeading = 334.1993		BREAK
						CASE 9	vLoc =  <<-431.9058, 6159.1167, 30.4782>>		 		fHeading = 51.7987		BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iPed
						CASE 0	vLoc =  <<-1171.1522, -1379.0782, 3.9771>>			 		fHeading = 113.7987		BREAK
						CASE 1	vLoc =  <<-1180.5691, -1401.5040, 3.6172>>			 		fHeading = 319.3982		BREAK
						CASE 2	vLoc =  <<-1184.4054, -1388.3064, 3.6343>>			 		fHeading = 292.1980		BREAK
						CASE 3	vLoc =  <<-1166.2944, -1393.2129, 3.9352>>		 			fHeading = 102.9975		BREAK
						CASE 4	vLoc =  <<-1204.0920, -1377.1592, 46.4974>>					fHeading = 249.7971		BREAK
						CASE 5	vLoc =  <<-1204.0920, -1377.1592, 46.4974>>					fHeading = 249.7971		BREAK
						CASE 6	vLoc =  <<-1204.0920, -1377.1592, 46.4974>>					fHeading = 249.7971		BREAK
						CASE 7	vLoc =  <<-1192.7694, -1374.0294, 3.5586>>		 			fHeading = 28.3993		BREAK
						CASE 8	vLoc =  <<-1167.8699, -1402.7803, 3.8078>>			 		fHeading = 229.5984		BREAK
						CASE 9	vLoc =  <<-1174.5472, -1373.1361, 3.9705>>			 		fHeading = 173.3973		BREAK
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iPed
						CASE 0	vLoc =  <<635.9548, 2784.2163, 41.0173>>		 		fHeading = 257.5985		BREAK
						CASE 1	vLoc =  <<649.1716, 2781.2434, 40.9011>>		 		fHeading = 231.1980		BREAK
						CASE 2	vLoc =  <<644.4341, 2767.3770, 41.0139>>		 		fHeading = 231.1980		BREAK
						CASE 3	vLoc =  <<636.2225, 2772.5273, 41.0095>>	 			fHeading = 341.9977		BREAK
						CASE 4	vLoc =  <<672.4673, 2760.1914, 82.1292>>				fHeading = 54.5973		BREAK
						CASE 5	vLoc =  <<672.4673, 2760.1914, 82.1292>>				fHeading = 54.5973		BREAK
						CASE 6	vLoc =  <<672.4673, 2760.1914, 82.1292>>				fHeading = 54.5973		BREAK
						CASE 7	vLoc =  <<652.8533, 2740.6221, 40.8902>>	 			fHeading = 216.5995		BREAK
						CASE 8	vLoc =  <<634.1667, 2738.2380, 40.9690>>		 		fHeading = 136.5993		BREAK
						CASE 9	vLoc =  <<642.7129, 2797.2190, 40.9764>>		 		fHeading = 224.7989		BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iPed
						CASE 0	vLoc =  <<671.1136, -2668.7546, 5.0812>>			 		fHeading =  95.5983			BREAK
						CASE 1	vLoc =  <<661.8442, -2662.0747, 5.0812>>			 		fHeading =  95.5983			BREAK
						CASE 2	vLoc =  <<644.2212, -2674.3711, 5.0969>>			 		fHeading =  122.7981		BREAK
						CASE 3	vLoc =  <<670.4280, -2677.5754, 5.0812>>		 			fHeading =  52.7976			BREAK
						CASE 4	vLoc =  <<629.5627, -2713.3940, 45.6524>>					fHeading =  325.5972		BREAK
						CASE 5	vLoc =  <<629.5627, -2713.3940, 45.6524>>					fHeading =  325.5972		BREAK
						CASE 6	vLoc =  <<629.5627, -2713.3940, 45.6524>>					fHeading =  325.5972		BREAK
						CASE 7	vLoc =  <<651.1802, -2693.8162, 5.0993>>		 			fHeading =  152.7994		BREAK
						CASE 8	vLoc =  <<632.0514, -2651.8970, 5.0995>>			 		fHeading =  84.3991			BREAK
						CASE 9	vLoc =  <<678.1410, -2693.5081, 6.6105>>			 		fHeading =  59.7990			BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_ID_1
					SWITCH iPed
						CASE 0	vLoc =  <<-161.6216, 6333.8940, 30.5808>>		 		fHeading =  4.7989			BREAK
						CASE 1	vLoc =  <<-152.4815, 6325.8936, 30.5907>>		 		fHeading =  31.3987			BREAK
						CASE 2	vLoc =  <<-156.2353, 6351.9976, 30.5808>>		 		fHeading =  171.9985		BREAK
						CASE 3	vLoc =  <<-167.2912, 6341.3384, 30.5754>>	 			fHeading =  270.5974		BREAK
						CASE 4	vLoc =  <<-126.7401, 6370.3359, 93.3578>>				fHeading =  101.5969		BREAK
						CASE 5	vLoc =  <<-126.7401, 6370.3359, 93.3578>>				fHeading =  101.5969		BREAK
						CASE 6	vLoc =  <<-126.7401, 6370.3359, 93.3578>>				fHeading =  101.5969		BREAK
						CASE 7	vLoc =  <<-148.1060, 6315.3848, 30.4102>>	 			fHeading =  317.9995		BREAK
						CASE 8	vLoc =  <<-154.4275, 6330.1455, 30.5657>>		 		fHeading =  223.5986		BREAK
						CASE 9	vLoc =  <<-152.2743, 6348.4189, 30.5853>>		 		fHeading =  127.3985		BREAK
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_FAKE_ID_2
					SWITCH iPed
						CASE 0	vLoc =  <<332.0761, -773.8383, 28.2737>>			 		fHeading =  72.7982			BREAK
						CASE 1	vLoc =  <<302.1832, -760.0439, 28.3148>>			 		fHeading =  285.9979		BREAK
						CASE 2	vLoc =  <<305.7425, -752.4186, 28.3677>>			 		fHeading =  225.7975		BREAK
						CASE 3	vLoc =  <<297.8272, -772.4958, 28.3151>>		 			fHeading =  305.1971		BREAK
						CASE 4	vLoc =  <<354.5495, -740.3669, 99.6289>>					fHeading =  118.1953		BREAK
						CASE 5	vLoc =  <<355.9252, -739.1375, 100.1210>>					fHeading =  	118.1953		BREAK
						CASE 6	vLoc =  <<355.8687, -739.3969, 100.1210>>					fHeading =  118.1953		BREAK
						CASE 7	vLoc =  <<296.5152, -784.4351, 28.3119>>		 			fHeading =  186.9996		BREAK
						CASE 8	vLoc =  <<309.9944, -743.7558, 28.3119>>			 		fHeading =  310.7983		BREAK
						CASE 9	vLoc =  <<330.5505, -781.3486, 28.2726>>			 		fHeading =  21.3979			BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_ID_3
					SWITCH iPed
						CASE 0	vLoc =  <<1657.9570, 4853.1763, 40.9714>>		 		fHeading =  255.9986		BREAK
						CASE 1	vLoc =  <<1659.5781, 4841.2036, 41.0390>>		 		fHeading =  342.9984		BREAK
						CASE 2	vLoc =  <<1678.1139, 4856.2305, 41.0668>>		 		fHeading =  101.9980		BREAK
						CASE 3	vLoc =  <<1658.7139, 4861.4546, 40.9960>>	 			fHeading =  221.9974		BREAK
						CASE 4	vLoc =  <<1687.9186, 4864.4824, 91.5886>>				fHeading =  135.5966		BREAK
						CASE 5	vLoc =  <<1689.4391, 4865.2769, 91.9096>>				fHeading =  	135.5966		BREAK
						CASE 6	vLoc =  <<1689.4391, 4865.2769, 91.9096>>				fHeading =  135.5966		BREAK
						CASE 7	vLoc =  <<1662.6288, 4822.5630, 41.0603>>	 			fHeading =  248.5995		BREAK
						CASE 8	vLoc =  <<1655.3368, 4867.3179, 41.0057>>		 		fHeading =  272.3994		BREAK
						CASE 9	vLoc =  <<1678.5094, 4861.9385, 41.0557>>		 		fHeading =  115.7986		BREAK
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iPed
						CASE 0	vLoc =  <<-332.9190, -2776.7849, 4.1402>>		 		fHeading =  59.1977			BREAK
						CASE 1	vLoc =  <<-347.0241, -2785.2170, 4.0002>>		 		fHeading =  337.9976		BREAK
						CASE 2	vLoc =  <<-333.1073, -2765.0420, 4.0002>>		 		fHeading =  119.3970		BREAK
						CASE 3	vLoc =  <<-353.0453, -2764.9814, 5.0352>>	 			fHeading =  31.7966			BREAK
						CASE 4	vLoc =  <<-387.7456, -2768.0669, 59.2421>>				fHeading =  292.1962		BREAK
						CASE 5	vLoc =  <<-389.6833, -2768.5303, 59.0641>>				fHeading =  	292.1962		BREAK
						CASE 6	vLoc =  <<-389.6833, -2768.5303, 59.0641>>				fHeading =  292.1962		BREAK
						CASE 7	vLoc =  <<-354.4687, -2742.0037, 5.0456>>	 			fHeading =  38.0000			BREAK
						CASE 8	vLoc =  <<-368.0629, -2761.9563, 5.0121>>		 		fHeading =  72.7999			BREAK
						CASE 9	vLoc =  <<-332.2912, -2787.0784, 4.1441>>		 		fHeading =  7.7998			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC INIT_GB_BIKER_DEFEND_PED_DEFENSIVE_COORDS(INT iVariation, FACTORY_ID factoryId, INT iPed, VECTOR &vLoc, FLOAT &fSize)
	FLOAT fDefaultSize
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryID
				CASE FACTORY_ID_METH_1 
					SWITCH iPed
						CASE 0	vLoc = <<1690.3516, 3603.4648, 34.4254>> 		fSize = 3.0		BREAK
						CASE 1	vLoc = <<1694.8308, 3608.7844, 34.3479>> 		fSize = 3.0		BREAK
						CASE 2	vLoc = <<1695.6193, 3609.8040, 34.3329>> 		fSize = 3.0		BREAK
						CASE 3	vLoc = <<1690.4680, 3597.5781, 34.5488>> 		fSize = 3.0		BREAK
						CASE 4	vLoc = <<1691.8453, 3598.5286, 34.5507>> 		fSize = 3.0		BREAK
						CASE 5	vLoc = <<1696.1779, 3597.7695, 34.6025>> 		fSize = 3.0		BREAK
						CASE 6	vLoc = <<1699.5643, 3604.4275, 34.4282>> 		fSize = 3.0		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_2 
					SWITCH iPed
						CASE 0	vLoc = <<247.7620, 129.5270, 112.7010>> 		fSize = 3.0		BREAK
						CASE 1	vLoc = <<236.2360, 126.7850, 101.6000>> 		fSize = 3.0		BREAK
						CASE 2	vLoc = <<228.1990, 128.5990, 101.6000>> 		fSize = 3.0		BREAK
						CASE 3	vLoc = <<229.2470, 129.5670, 101.6000>> 		fSize = 3.0		BREAK
						CASE 4	vLoc = <<244.6905, 126.7374, 101.5993>> 		fSize = 3.0		BREAK
						CASE 5	vLoc = <<243.8857, 126.1607, 101.5993>> 		fSize = 3.0		BREAK
						CASE 6	vLoc = <<240.1743, 115.6268, 101.6225>> 		fSize = 3.0		BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_METH_3 
					SWITCH iPed
						CASE 0	vLoc = <<2400.8735, 5027.4287, 45.0084>> 		fSize = 3.0	BREAK
						CASE 1	vLoc = <<2405.3359, 5026.2871, 45.0220>> 		fSize = 3.0	BREAK
						CASE 2	vLoc = <<2404.3191, 5026.6279, 44.9931>> 		fSize = 3.0	BREAK
						CASE 3	vLoc = <<2407.7192, 5033.1670, 44.9933>> 		fSize = 3.0	BREAK
						CASE 4	vLoc = <<2398.4707, 5030.6577, 44.9951>> 		fSize = 3.0	BREAK
						CASE 5	vLoc = <<2403.4412, 5034.2363, 44.9932>> 		fSize = 3.0	BREAK
						CASE 6	vLoc = <<2402.3516, 5033.1372, 44.9932>> 		fSize = 3.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_4 
					SWITCH iPed
						CASE 0	vLoc = <<-1124.1759, -1453.8003, 7.3482>> 		fSize = 3.0	BREAK
						CASE 1	vLoc = <<-1130.2506, -1452.5344, 3.9380>> 		fSize = 3.0	BREAK
						CASE 2	vLoc = <<-1131.6995, -1437.3601, 3.9093>> 		fSize = 3.0	BREAK
						CASE 3	vLoc = <<-1137.5421, -1448.7032, 3.7750>> 		fSize = 3.0	BREAK
						CASE 4	vLoc = <<-1136.3081, -1449.2018, 3.7523>> 		fSize = 3.0	BREAK
						CASE 5	vLoc = <<-1143.3463, -1445.1925, 3.6430>> 		fSize = 3.0	BREAK
						CASE 6	vLoc = <<-1126.3807, -1447.5673, 4.0529>> 		fSize = 3.0	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					SWITCH iPed
						CASE 0	vLoc = <<-687.1215, 5760.5044, 16.5110>> 		fSize = 3.0	BREAK
						CASE 1	vLoc = <<-688.3340, 5760.0098, 16.5110>> 		fSize = 3.0	BREAK
						CASE 2	vLoc = <<-689.9678, 5772.7573, 16.3310>> 		fSize = 3.0	BREAK
						CASE 3	vLoc = <<-692.9852, 5769.0820, 16.3310>> 		fSize = 3.0	BREAK
						CASE 4	vLoc = <<-693.3242, 5767.9321, 16.3310>> 		fSize = 3.0	BREAK
						CASE 5	vLoc = <<-704.3570, 5772.3516, 16.3309>> 		fSize = 3.0	BREAK
						CASE 6	vLoc = <<-690.1846, 5764.6875, 16.3310>> 		fSize = 3.0	BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_CRACK_2
					SWITCH iPed
						CASE 0	vLoc = <<344.3741, -204.9075, 57.0242>> 		fSize = 3.0	BREAK
						CASE 1	vLoc = <<343.7755, -205.6453, 57.0242>> 		fSize = 3.0	BREAK
						CASE 2	vLoc = <<333.5367, -211.8548, 53.0863>> 		fSize = 3.0	BREAK
						CASE 3	vLoc = <<333.0363, -211.1229, 53.0863>> 		fSize = 3.0	BREAK
						CASE 4	vLoc = <<342.1429, -209.2703, 57.0242>> 		fSize = 3.0	BREAK
						CASE 5	vLoc = <<333.1700, -220.8350, 55.2750>> 		fSize = 3.0	BREAK
						CASE 6	vLoc = <<325.0602, -222.0730, 53.0851>> 		fSize = 3.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_3
					SWITCH iPed
						CASE 0	vLoc = <<1141.8840, 2642.7561, 37.1490>> 		fSize = 3.0	BREAK
						CASE 1	vLoc = <<1140.7469, 2643.2251, 37.1490>> 		fSize = 3.0	BREAK
						CASE 2	vLoc = <<1131.0630, 2661.7681, 36.9980>> 		fSize = 3.0	BREAK
						CASE 3	vLoc = <<1124.0990, 2650.8711, 36.9970>> 		fSize = 3.0	BREAK
						CASE 4	vLoc = <<1130.1379, 2660.6111, 36.9970>> 		fSize = 3.0	BREAK
						CASE 5	vLoc = <<1130.2290, 2642.8279, 37.1490>> 		fSize = 3.0	BREAK
						CASE 6	vLoc = <<1138.5750, 2645.7400, 36.9960>> 		fSize = 3.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_4
					SWITCH iPed
						CASE 0	vLoc = <<561.1011, -1747.8079, 32.4476>> 		fSize = 3.0	BREAK
						CASE 1	vLoc = <<565.3523, -1750.1691, 28.2839>> 		fSize = 3.0	BREAK
						CASE 2	vLoc = <<564.3752, -1776.4705, 30.6977>> 		fSize = 3.0	BREAK
						CASE 3	vLoc = <<558.6710, -1756.2892, 32.4476>> 		fSize = 3.0	BREAK
						CASE 4	vLoc = <<561.5383, -1748.9618, 32.4476>> 		fSize = 3.0	BREAK
						CASE 5	vLoc = <<561.5959, -1770.3324, 28.3567>> 		fSize = 3.0	BREAK
						CASE 6	vLoc = <<562.7568, -1770.0918, 28.3572>> 		fSize = 3.0	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iPed
						CASE 0	vLoc = <<1309.1121, 4229.9199, 32.9200>> 		fSize = 3.0		BREAK
						CASE 1	vLoc = <<1302.9410, 4234.7852, 32.9140>> 		fSize = 3.00	BREAK
						CASE 2	vLoc = <<1305.7640, 4233.1631, 32.9140>> 		fSize = 3.0		BREAK
						CASE 3	vLoc = <<1303.0850, 4231.3579, 32.9140>> 		fSize = 3.0		BREAK
						CASE 4	vLoc = <<1303.2290, 4236.7212, 32.9140>> 		fSize = 3.00	BREAK
						CASE 5	vLoc = <<1301.6360, 4238.9082, 32.9140>> 		fSize = 3.0		BREAK
						CASE 6	vLoc = <<1305.5200, 4239.9131, 32.9140>> 		fSize = 3.0		BREAK
						CASE 7	vLoc = <<1304.6580, 4256.1948, 32.9140>> 		fSize = 3.0		BREAK
						CASE 8	vLoc = <<1303.4740, 4242.2261, 32.9140>> 		fSize = 3.0		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iPed
						CASE 0	vLoc = <<1657.5980, 4.2860, 165.1180>>	 		fSize = 3.0		BREAK
						CASE 1	vLoc = <<1661.8160, 3.1630, 165.1180>>	 		fSize = 3.00	BREAK
						CASE 2	vLoc = <<1663.6899, 4.1100, 165.1180>>	 		fSize = 3.0		BREAK
						CASE 3	vLoc = <<1662.2970, 7.6410, 165.0680>>	 		fSize = 3.0		BREAK
						CASE 4	vLoc = <<1661.8130, 0.9190, 165.1180>>	 		fSize = 3.00	BREAK
						CASE 5	vLoc = <<1661.5090, -1.8150, 165.1180>>	 		fSize = 3.0		BREAK
						CASE 6	vLoc = <<1663.6320, -0.6020, 165.1180>>	 		fSize = 3.0		BREAK
						CASE 7	vLoc = <<1664.0031, -5.8700, 165.1180>>	 		fSize = 3.0		BREAK
						CASE 8	vLoc = <<1659.8800, 0.6000, 165.1180>>	 		fSize = 3.0		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iPed
						CASE 0	vLoc = <<2354.2161, 3132.5549, 47.2090>> 		fSize = 3.0	BREAK
						CASE 1	vLoc = <<2357.6230, 3127.4399, 47.2090>> 		fSize = 3.0	BREAK
						CASE 2	vLoc = <<2356.5430, 3134.5229, 47.2090>> 		fSize = 3.0	BREAK
						CASE 3	vLoc = <<2362.1021, 3133.8660, 47.2090>> 		fSize = 3.0	BREAK
						CASE 4	vLoc = <<2367.2180, 3131.6570, 47.2490>> 		fSize = 3.0	BREAK
						CASE 5	vLoc = <<2338.9417, 3136.3328, 47.2066>> 		fSize = 3.0	BREAK
						CASE 6	vLoc = <<2351.3430, 3133.2209, 47.2087>> 		fSize = 3.0	BREAK
						CASE 7	vLoc = <<2349.5381, 3131.6614, 47.2087>> 		fSize = 3.0	BREAK
						CASE 8	vLoc = <<2350.2964, 3135.5115, 47.2087>> 		fSize = 3.0	BREAK
						CASE 9	vLoc = <<2349.2595, 3134.8220, 47.2087>> 		fSize = 3.0	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iPed
						CASE 0	vLoc = <<-440.8068, -983.9050, 24.9031>> 		fSize = 3.0	BREAK
						CASE 1	vLoc = <<-442.9531, -990.5599, 22.8330>> 		fSize = 3.0	BREAK
						CASE 2	vLoc = <<-443.5239, -985.6843, 22.8178>> 		fSize = 3.0	BREAK
						CASE 3	vLoc = <<-449.6893, -976.2077, 24.9032>> 		fSize = 3.0	BREAK
						CASE 4	vLoc = <<-445.1724, -987.8193, 22.7599>> 		fSize = 3.0	BREAK
						CASE 5	vLoc = <<-460.1397, -983.6872, 22.5453>> 		fSize = 3.0	BREAK
						CASE 6	vLoc = <<-449.7830, -986.0192, 22.6078>> 		fSize = 3.0	BREAK
						CASE 7	vLoc = <<-449.5731, -988.4716, 22.6117>> 		fSize = 3.0	BREAK
						CASE 8	vLoc = <<-447.8551, -987.6537, 22.6703>> 		fSize = 3.0	BREAK
						CASE 9	vLoc = <<-455.6019, -987.0480, 22.5453>> 		fSize = 3.0	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			fDefaultSize = 10.0
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
					SWITCH iPed
						CASE 0	vLoc = <<51.5610, 6337.9663, 30.3812>> 		fSize = fDefaultSize	BREAK
						CASE 1	vLoc = <<38.1361, 6351.1631, 30.2398>> 		fSize = fDefaultSize	BREAK
						CASE 2	vLoc = <<58.1027, 6342.3667, 30.2260>> 		fSize = fDefaultSize	BREAK
						CASE 3	vLoc = <<43.9142, 6334.4160, 30.3668>> 		fSize = fDefaultSize	BREAK
						CASE 4	vLoc = <<35.6928, 6379.9688, 88.3791>> 		fSize = fDefaultSize	BREAK
						CASE 5	vLoc = <<36.3193, 6379.2822, 88.1211>> 		fSize = fDefaultSize	BREAK
						CASE 6	vLoc = <<36.3193, 6379.2822, 88.1211>> 		fSize = fDefaultSize	BREAK
						CASE 7	vLoc = <<74.8904, 6377.7236, 30.2394>> 		fSize = fDefaultSize	BREAK
						CASE 8	vLoc = <<79.6583, 6354.1938, 30.3759>> 		fSize = fDefaultSize	BREAK
						CASE 9	vLoc = <<67.5955, 6321.0674, 30.3710>> 		fSize = fDefaultSize	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_2
					SWITCH iPed
						CASE 0	vLoc = <<1449.6958, -1652.8284, 64.8268>>	 		fSize = fDefaultSize			BREAK
						CASE 1	vLoc = <<1465.2133, -1643.5917, 67.6959>>	 		fSize = fDefaultSize			BREAK
						CASE 2	vLoc = <<1434.6486, -1654.2078, 62.1111>>	 		fSize = fDefaultSize			BREAK
						CASE 3	vLoc = <<1457.3384, -1650.4194, 66.0953>>	 		fSize = fDefaultSize			BREAK
						CASE 4	vLoc = <<1424.4821, -1616.7404, 124.0498>>	 		fSize = fDefaultSize			BREAK
						CASE 5	vLoc = <<1423.3065, -1614.8804, 124.1826>>	 		fSize = fDefaultSize			BREAK
						CASE 6	vLoc = <<1423.3065, -1614.8804, 124.1826>>	 		fSize = fDefaultSize			BREAK
						CASE 7	vLoc = <<1424.1571, -1646.7726, 60.5027>>	 		fSize = fDefaultSize			BREAK
						CASE 8	vLoc = <<1474.0764, -1638.5048, 68.6667>>	 		fSize = fDefaultSize			BREAK
						CASE 9	vLoc = <<1440.9353, -1669.9475, 65.6504>>	 		fSize = fDefaultSize			BREAK
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_METH_3
					SWITCH iPed
						CASE 0	vLoc =  <<203.3479, 2462.1074, 54.7058>>	 		fSize = fDefaultSize			BREAK
						CASE 1	vLoc =  <<209.1309, 2467.3513, 54.6871>>	 		fSize = fDefaultSize			BREAK
						CASE 2	vLoc =  <<208.0104, 2447.2959, 56.8825>>	 		fSize = fDefaultSize			BREAK
						CASE 3	vLoc =  <<204.1095, 2452.3474, 56.1404>>	 		fSize = fDefaultSize			BREAK
						CASE 4	vLoc =  <<263.7463, 2451.2295, 124.7385>> 			fSize = fDefaultSize			BREAK
						CASE 5	vLoc =  <<264.6442, 2450.8806, 125.0143>> 			fSize = fDefaultSize			BREAK
						CASE 6	vLoc =  <<264.6442, 2450.8806, 125.0143>> 			fSize = fDefaultSize			BREAK
						CASE 7	vLoc =  <<219.3545, 2484.9033, 54.1257>>	 		fSize = fDefaultSize			BREAK
						CASE 8	vLoc =  <<224.0474, 2438.7590, 57.4561>>	 		fSize = fDefaultSize			BREAK
						CASE 9	vLoc =  <<210.4050, 2463.1848, 54.9640>>	 		fSize = fDefaultSize			BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_4
					SWITCH iPed
						CASE 0	vLoc =  <<1180.2961, -3112.3293, 5.0280>>	 		fSize = fDefaultSize			BREAK
						CASE 1	vLoc =  <<1175.6754, -3126.6543, 5.0280>>	 		fSize = fDefaultSize			BREAK
						CASE 2	vLoc =  <<1176.8580, -3104.4612, 5.0280>>	 		fSize = fDefaultSize			BREAK
						CASE 3	vLoc =  <<1181.1052, -3118.0916, 5.0280>>	 		fSize = fDefaultSize			BREAK
						CASE 4	vLoc =  <<1141.3502, -3110.5366, 75.9948>>			fSize = fDefaultSize			BREAK
						CASE 5	vLoc =  <<1140.0980, -3110.3132, 76.3252>>			fSize = fDefaultSize			BREAK
						CASE 6	vLoc =  <<1140.0980, -3110.3132, 76.3252>>			fSize = fDefaultSize			BREAK
						CASE 7	vLoc =  <<1159.8777, -3121.3843, 4.9003>>	 		fSize = fDefaultSize			BREAK
						CASE 8	vLoc =  <<1177.1815, -3108.0916, 5.0280>>	 		fSize = fDefaultSize			BREAK
						CASE 9	vLoc =  <<1185.1003, -3108.3811, 5.0280>>	 		fSize = fDefaultSize			BREAK
					ENDSWITCH
				BREAK

			
				CASE FACTORY_ID_WEED_1
					SWITCH iPed
						CASE 0	vLoc =  <<416.7668, 6519.5659, 26.6965>>	 		fSize = fDefaultSize		BREAK
						CASE 1	vLoc =  <<433.8810, 6536.5786, 27.0296>>	 		fSize = fDefaultSize		BREAK
						CASE 2	vLoc =  <<429.2975, 6504.0576, 27.3953>>	 		fSize = fDefaultSize		BREAK
						CASE 3	vLoc =  <<417.7043, 6539.6333, 26.6770>>	 		fSize = fDefaultSize		BREAK
						CASE 4	vLoc =  <<442.7939, 6549.2690, 72.1564>>			fSize = fDefaultSize		BREAK
						CASE 5	vLoc =  <<442.7939, 6549.2690, 72.1564>>			fSize = fDefaultSize		BREAK
						CASE 6	vLoc =  <<442.7939, 6549.2690, 72.1564>>			fSize = fDefaultSize		BREAK
						CASE 7	vLoc =  <<434.6517, 6552.5220, 26.5632>>	 		fSize = fDefaultSize		BREAK
						CASE 8	vLoc =  <<426.8210, 6498.4683, 27.0759>>	 		fSize = fDefaultSize		BREAK
						CASE 9	vLoc =  <<416.3445, 6541.6479, 26.5680>>	 		fSize = fDefaultSize		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_2
					SWITCH iPed
						CASE 0	vLoc =  <<102.3849, 173.3189, 103.5867>>		 		fSize = fDefaultSize			BREAK
						CASE 1	vLoc =  <<111.6505, 168.0158, 103.7048>>		 		fSize = fDefaultSize			BREAK
						CASE 2	vLoc =  <<114.6820, 170.9016, 111.4567>>		 		fSize = fDefaultSize			BREAK
						CASE 3	vLoc =  <<93.4121, 175.6396, 103.6263>>		 			fSize = fDefaultSize			BREAK
						CASE 4	vLoc =  <<69.2631, 137.0533, 166.6731>>					fSize = fDefaultSize			BREAK
						CASE 5	vLoc =  <<69.2631, 137.0532, 166.6730>>					fSize = fDefaultSize			BREAK
						CASE 6	vLoc =  <<69.2631, 137.0532, 166.6730>>					fSize = fDefaultSize			BREAK
						CASE 7	vLoc =  <<72.0584, 184.2480, 103.8951>>		 			fSize = fDefaultSize			BREAK
						CASE 8	vLoc =  <<130.8619, 163.1978, 103.6192>>		 		fSize = fDefaultSize			BREAK
						CASE 9	vLoc =  <<111.0224, 171.1221, 103.6295>>		 		fSize = fDefaultSize			BREAK
					ENDSWITCH
				BREAK
			
				CASE FACTORY_ID_WEED_3
					SWITCH iPed
						CASE 0	vLoc =  <<2847.8220, 4451.4663, 47.5091>>		 		fSize = fDefaultSize		BREAK
						CASE 1	vLoc =  <<2845.4841, 4479.9580, 47.5654>>		 		fSize = fDefaultSize		BREAK
						CASE 2	vLoc =  <<2857.8848, 4445.6348, 47.5394>>		 		fSize = fDefaultSize		BREAK
						CASE 3	vLoc =  <<2850.3164, 4433.4971, 47.8439>>	 			fSize = fDefaultSize		BREAK
						CASE 4	vLoc =  <<2827.9231, 4404.1890, 94.1047>>				fSize = fDefaultSize		BREAK
						CASE 5	vLoc =  <<2827.9231, 4404.1890, 94.1047>>				fSize = fDefaultSize		BREAK
						CASE 6	vLoc =  <<2827.9231, 4404.1890, 94.1047>>				fSize = fDefaultSize		BREAK
						CASE 7	vLoc =  <<2840.6731, 4483.1875, 47.8268>>	 			fSize = fDefaultSize		BREAK
						CASE 8	vLoc =  <<2858.3335, 4436.0640, 47.7770>>		 		fSize = fDefaultSize		BREAK
						CASE 9	vLoc =  <<2874.7458, 4451.4248, 47.5535>>		 		fSize = fDefaultSize		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_4
					SWITCH iPed
						CASE 0	vLoc =  <<137.5898, -2472.2100, 5.0000>>			 		fSize = fDefaultSize			BREAK
						CASE 1	vLoc =  <<144.4460, -2464.1628, 5.0000>>			 		fSize = fDefaultSize			BREAK
						CASE 2	vLoc =  <<152.6100, -2476.9783, 4.9916>>			 		fSize = fDefaultSize			BREAK
						CASE 3	vLoc =  <<130.4017, -2479.5364, 5.0000>>		 			fSize = fDefaultSize			BREAK
						CASE 4	vLoc =  <<175.2240, -2491.9353, 49.1284>>					fSize = fDefaultSize			BREAK
						CASE 5	vLoc =  <<175.2241, -2491.9348, 49.1285>>					fSize = fDefaultSize			BREAK
						CASE 6	vLoc =  <<175.2241, -2491.9348, 49.1285>>					fSize = fDefaultSize			BREAK
						CASE 7	vLoc =  <<151.7093, -2491.9199, 4.9901>>		 			fSize = fDefaultSize			BREAK
						CASE 8	vLoc =  <<146.9291, -2466.1941, 5.0000>>			 		fSize = fDefaultSize			BREAK
						CASE 9	vLoc =  <<119.6780, -2470.1802, 5.0000>>			 		fSize = fDefaultSize			BREAK
					ENDSWITCH
				BREAK

			
				CASE FACTORY_ID_CRACK_1
					SWITCH iPed
						CASE 0	vLoc = <<51.2343, 6486.9248, 30.4253>>			 		fSize = fDefaultSize			BREAK
						CASE 1	vLoc = <<60.4884, 6475.1582, 30.4253>>			 		fSize = fDefaultSize			BREAK
						CASE 2	vLoc = <<61.7692, 6464.7998, 30.3324>>			 		fSize = fDefaultSize			BREAK
						CASE 3	vLoc = <<43.0875, 6497.8027, 30.4253>>		 			fSize = fDefaultSize			BREAK
						CASE 4	vLoc = <<124.7746, 6481.6621, 79.0894>>					fSize = fDefaultSize			BREAK
						CASE 5	vLoc = <<124.7746, 6481.6621, 79.0894>>					fSize = fDefaultSize			BREAK
						CASE 6	vLoc = <<124.7746, 6481.6621, 79.0894>>					fSize = fDefaultSize			BREAK
						CASE 7	vLoc = <<71.2958, 6471.7671, 30.2259>>		 			fSize = fDefaultSize			BREAK
						CASE 8	vLoc = <<58.4544, 6467.6919, 30.4253>>			 		fSize = fDefaultSize			BREAK
						CASE 9	vLoc = <<52.8278, 6467.9893, 30.4252>>			 		fSize = fDefaultSize			BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_2
					SWITCH iPed
						CASE 0	vLoc = <<-1461.2284, -381.3546, 37.8007>>		 		fSize = fDefaultSize		BREAK
						CASE 1	vLoc = <<-1474.3870, -399.5579, 36.7078>>		 		fSize = fDefaultSize		BREAK
						CASE 2	vLoc = <<-1444.4481, -374.1057, 42.4503>>		 		fSize = fDefaultSize		BREAK
						CASE 3	vLoc = <<-1459.0367, -376.1592, 37.9571>>	 			fSize = fDefaultSize		BREAK
						CASE 4	vLoc = <<-1429.4989, -381.0566, 78.3396>>				fSize = fDefaultSize		BREAK
						CASE 5	vLoc = <<-1429.4989, -381.0566, 78.3396>>				fSize = fDefaultSize		BREAK
						CASE 6	vLoc = <<-1429.1761, -381.0665, 78.3471>>				fSize = fDefaultSize		BREAK
						CASE 7	vLoc = <<-1444.7719, -377.4260, 37.3412>>	 			fSize = fDefaultSize		BREAK
						CASE 8	vLoc = <<-1476.5850, -405.3120, 36.3396>>		 		fSize = fDefaultSize		BREAK
						CASE 9	vLoc = <<-1465.0547, -394.1414, 37.2545>>		 		fSize = fDefaultSize		BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_CRACK_3
					SWITCH iPed
						CASE 0	vLoc = <<388.8037, 3585.3972, 32.2922>>		 		fSize = fDefaultSize	BREAK
						CASE 1	vLoc = <<399.9077, 3584.1377, 32.2922>>		 		fSize = fDefaultSize	BREAK
						CASE 2	vLoc = <<374.2202, 3596.4548, 32.4686>>		 		fSize = fDefaultSize	BREAK
						CASE 3	vLoc = <<379.4160, 3586.1611, 32.2922>>	 			fSize = fDefaultSize	BREAK
						CASE 4	vLoc = <<373.0628, 3627.0146, 75.3474>>				fSize = fDefaultSize	BREAK
						CASE 5	vLoc = <<373.0628, 3627.0146, 75.3474>>				fSize = fDefaultSize	BREAK
						CASE 6	vLoc = <<373.0628, 3627.0146, 75.3474>>				fSize = fDefaultSize	BREAK
						CASE 7	vLoc = <<402.2075, 3592.7793, 32.2218>>	 			fSize = fDefaultSize	BREAK
						CASE 8	vLoc = <<420.5654, 3584.5891, 32.2386>>		 		fSize = fDefaultSize	BREAK
						CASE 9	vLoc = <<390.8926, 3559.9868, 32.3198>>		 		fSize = fDefaultSize	BREAK
					ENDSWITCH
				BREAK
		
				CASE FACTORY_ID_CRACK_4
					SWITCH iPed
						CASE 0	vLoc =  <<-253.8698, -2592.1277, 5.0006>>			 		fSize = fDefaultSize	BREAK
						CASE 1	vLoc =  <<-266.1535, -2573.4844, 5.0006>>			 		fSize = fDefaultSize	BREAK
						CASE 2	vLoc =  <<-260.3382, -2601.9590, 5.0006>>			 		fSize = fDefaultSize	BREAK
						CASE 3	vLoc =  <<-255.2566, -2580.1770, 5.0006>>		 			fSize = fDefaultSize	BREAK
						CASE 4	vLoc =  <<-295.0537, -2574.4639, 52.2955>>					fSize = fDefaultSize	BREAK
						CASE 5	vLoc =  <<-295.0537, -2574.4639, 52.2955>>					fSize = fDefaultSize	BREAK
						CASE 6	vLoc =  <<-295.0537, -2574.4639, 52.2955>>					fSize = fDefaultSize	BREAK
						CASE 7	vLoc =  <<-267.8810, -2606.4268, 5.0272>>		 			fSize = fDefaultSize	BREAK
						CASE 8	vLoc =  <<-283.8436, -2573.2925, 5.0006>>			 		fSize = fDefaultSize	BREAK
						CASE 9	vLoc =  <<-247.5822, -2600.4370, 5.0007>>			 		fSize = fDefaultSize	BREAK
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iPed
						CASE 0	vLoc =  <<-412.4442, 6171.2266, 30.4782>>		 		fSize = fDefaultSize	BREAK
						CASE 1	vLoc =  <<-403.1873, 6161.6357, 30.4788>>		 		fSize = fDefaultSize	BREAK
						CASE 2	vLoc =  <<-416.9733, 6181.8330, 30.4782>>		 		fSize = fDefaultSize	BREAK
						CASE 3	vLoc =  <<-425.3268, 6166.2339, 30.4782>>	 			fSize = fDefaultSize	BREAK
						CASE 4	vLoc =  <<-387.9090, 6193.3936, 77.6257>>				fSize = fDefaultSize	BREAK
						CASE 5	vLoc =  <<-386.5088, 6194.1421, 77.9916>>				fSize = fDefaultSize	BREAK
						CASE 6	vLoc =  <<-386.5088, 6194.1421, 77.9916>>				fSize = fDefaultSize	BREAK
						CASE 7	vLoc =  <<-401.1820, 6146.8584, 30.4748>>	 			fSize = fDefaultSize	BREAK
						CASE 8	vLoc =  <<-396.5336, 6169.4790, 30.5090>>		 		fSize = fDefaultSize	BREAK
						CASE 9	vLoc =  <<-431.9058, 6159.1167, 30.4782>>		 		fSize = fDefaultSize	BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iPed
						CASE 0	vLoc =  <<-1171.1522, -1379.0782, 3.9771>>			 		fSize = fDefaultSize	BREAK
						CASE 1	vLoc =  <<-1180.5691, -1401.5040, 3.6172>>			 		fSize = fDefaultSize	BREAK
						CASE 2	vLoc =  <<-1184.4054, -1388.3064, 3.6343>>			 		fSize = fDefaultSize	BREAK
						CASE 3	vLoc =  <<-1166.2944, -1393.2129, 3.9352>>		 			fSize = fDefaultSize	BREAK
						CASE 4	vLoc =  <<-1204.0920, -1377.1592, 46.4974>>					fSize = fDefaultSize	BREAK
						CASE 5	vLoc =  <<-1204.0920, -1377.1592, 46.4974>>					fSize = fDefaultSize	BREAK
						CASE 6	vLoc =  <<-1204.0920, -1377.1592, 46.4974>>					fSize = fDefaultSize	BREAK
						CASE 7	vLoc =  <<-1192.7694, -1374.0294, 3.5586>>		 			fSize = fDefaultSize	BREAK
						CASE 8	vLoc =  <<-1167.8699, -1402.7803, 3.8078>>			 		fSize = fDefaultSize	BREAK
						CASE 9	vLoc =  <<-1174.5472, -1373.1361, 3.9705>>			 		fSize = fDefaultSize	BREAK
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iPed
						CASE 0	vLoc =  <<635.9548, 2784.2163, 41.0173>>		 		fSize = fDefaultSize		BREAK
						CASE 1	vLoc =  <<649.1716, 2781.2434, 40.9011>>		 		fSize = fDefaultSize		BREAK
						CASE 2	vLoc =  <<644.4341, 2767.3770, 41.0139>>		 		fSize = fDefaultSize		BREAK
						CASE 3	vLoc =  <<636.2225, 2772.5273, 41.0095>>	 			fSize = fDefaultSize		BREAK
						CASE 4	vLoc =  <<672.4673, 2760.1914, 82.1292>>				fSize = fDefaultSize		BREAK
						CASE 5	vLoc =  <<672.4673, 2760.1914, 82.1292>>				fSize = fDefaultSize		BREAK
						CASE 6	vLoc =  <<672.4673, 2760.1914, 82.1292>>				fSize = fDefaultSize		BREAK
						CASE 7	vLoc =  <<652.8533, 2740.6221, 40.8902>>	 			fSize = fDefaultSize		BREAK
						CASE 8	vLoc =  <<634.1667, 2738.2380, 40.9690>>		 		fSize = fDefaultSize		BREAK
						CASE 9	vLoc =  <<642.7129, 2797.2190, 40.9764>>		 		fSize = fDefaultSize		BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iPed
						CASE 0	vLoc =  <<671.1136, -2668.7546, 5.0812>>			 		fSize = fDefaultSize		BREAK
						CASE 1	vLoc =  <<661.8442, -2662.0747, 5.0812>>			 		fSize = fDefaultSize		BREAK
						CASE 2	vLoc =  <<644.2212, -2674.3711, 5.0969>>			 		fSize = fDefaultSize		BREAK
						CASE 3	vLoc =  <<670.4280, -2677.5754, 5.0812>>		 			fSize = fDefaultSize		BREAK
						CASE 4	vLoc =  <<629.5627, -2713.3940, 45.6524>>					fSize = fDefaultSize		BREAK
						CASE 5	vLoc =  <<629.5627, -2713.3940, 45.6524>>					fSize = fDefaultSize		BREAK
						CASE 6	vLoc =  <<629.5627, -2713.3940, 45.6524>>					fSize = fDefaultSize		BREAK
						CASE 7	vLoc =  <<651.1802, -2693.8162, 5.0993>>		 			fSize = fDefaultSize		BREAK
						CASE 8	vLoc =  <<632.0514, -2651.8970, 5.0995>>			 		fSize = fDefaultSize		BREAK
						CASE 9	vLoc =  <<678.1410, -2693.5081, 6.6105>>			 		fSize = fDefaultSize		BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_ID_1
					SWITCH iPed
						CASE 0	vLoc =  <<-161.6216, 6333.8940, 30.5808>>		 		fSize = fDefaultSize		BREAK
						CASE 1	vLoc =  <<-152.4815, 6325.8936, 30.5907>>		 		fSize = fDefaultSize		BREAK
						CASE 2	vLoc =  <<-156.2353, 6351.9976, 30.5808>>		 		fSize = fDefaultSize		BREAK
						CASE 3	vLoc =  <<-167.2912, 6341.3384, 30.5754>>	 			fSize = fDefaultSize		BREAK
						CASE 4	vLoc =  <<-126.7401, 6370.3359, 93.3578>>				fSize = fDefaultSize		BREAK
						CASE 5	vLoc =  <<-126.7401, 6370.3359, 93.3578>>				fSize = fDefaultSize		BREAK
						CASE 6	vLoc =  <<-126.7401, 6370.3359, 93.3578>>				fSize = fDefaultSize		BREAK
						CASE 7	vLoc =  <<-148.1060, 6315.3848, 30.4102>>	 			fSize = fDefaultSize		BREAK
						CASE 8	vLoc =  <<-154.4275, 6330.1455, 30.5657>>		 		fSize = fDefaultSize		BREAK
						CASE 9	vLoc =  <<-152.2743, 6348.4189, 30.5853>>		 		fSize = fDefaultSize		BREAK
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_FAKE_ID_2
					SWITCH iPed
						CASE 0	vLoc =  <<332.0761, -773.8383, 28.2737>>			 		fSize = fDefaultSize	BREAK
						CASE 1	vLoc =  <<302.1832, -760.0439, 28.3148>>			 		fSize = fDefaultSize	BREAK
						CASE 2	vLoc =  <<305.7425, -752.4186, 28.3677>>			 		fSize = fDefaultSize	BREAK
						CASE 3	vLoc =  <<297.8272, -772.4958, 28.3151>>		 			fSize = fDefaultSize	BREAK
						CASE 4	vLoc =  <<354.5495, -740.3669, 99.6289>>					fSize = fDefaultSize	BREAK
						CASE 5	vLoc =  <<355.9252, -739.1375, 100.1210>>					fSize = fDefaultSize		BREAK
						CASE 6	vLoc =  <<355.8687, -739.3969, 100.1210>>					fSize = fDefaultSize	BREAK
						CASE 7	vLoc =  <<296.5152, -784.4351, 28.3119>>		 			fSize = fDefaultSize	BREAK
						CASE 8	vLoc =  <<309.9944, -743.7558, 28.3119>>			 		fSize = fDefaultSize	BREAK
						CASE 9	vLoc =  <<330.5505, -781.3486, 28.2726>>			 		fSize = fDefaultSize	BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_ID_3
					SWITCH iPed
						CASE 0	vLoc =  <<1657.9570, 4853.1763, 40.9714>>		 		fSize = fDefaultSize	BREAK
						CASE 1	vLoc =  <<1659.5781, 4841.2036, 41.0390>>		 		fSize = fDefaultSize	BREAK
						CASE 2	vLoc =  <<1678.1139, 4856.2305, 41.0668>>		 		fSize = fDefaultSize	BREAK
						CASE 3	vLoc =  <<1658.7139, 4861.4546, 40.9960>>	 			fSize = fDefaultSize	BREAK
						CASE 4	vLoc =  <<1687.9186, 4864.4824, 91.5886>>				fSize = fDefaultSize	BREAK
						CASE 5	vLoc =  <<1689.4391, 4865.2769, 91.9096>>				fSize = fDefaultSize		BREAK
						CASE 6	vLoc =  <<1689.4391, 4865.2769, 91.9096>>				fSize = fDefaultSize	BREAK
						CASE 7	vLoc =  <<1662.6288, 4822.5630, 41.0603>>	 			fSize = fDefaultSize	BREAK
						CASE 8	vLoc =  <<1655.3368, 4867.3179, 41.0057>>		 		fSize = fDefaultSize	BREAK
						CASE 9	vLoc =  <<1678.5094, 4861.9385, 41.0557>>		 		fSize = fDefaultSize	BREAK
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iPed
						CASE 0	vLoc =  <<-332.9190, -2776.7849, 4.1402>>		 		fSize = fDefaultSize	BREAK
						CASE 1	vLoc =  <<-347.0241, -2785.2170, 4.0002>>		 		fSize = fDefaultSize	BREAK
						CASE 2	vLoc =  <<-333.1073, -2765.0420, 4.0002>>		 		fSize = fDefaultSize	BREAK
						CASE 3	vLoc =  <<-353.0453, -2764.9814, 5.0352>>	 			fSize = fDefaultSize	BREAK
						CASE 4	vLoc =  <<-387.7456, -2768.0669, 59.2421>>				fSize = fDefaultSize	BREAK
						CASE 5	vLoc =  <<-389.6833, -2768.5303, 59.0641>>				fSize = fDefaultSize		BREAK
						CASE 6	vLoc =  <<-389.6833, -2768.5303, 59.0641>>				fSize = fDefaultSize	BREAK
						CASE 7	vLoc =  <<-354.4687, -2742.0037, 5.0456>>	 			fSize = fDefaultSize	BREAK
						CASE 8	vLoc =  <<-368.0629, -2761.9563, 5.0121>>		 		fSize = fDefaultSize	BREAK
						CASE 9	vLoc =  <<-332.2912, -2787.0784, 4.1441>>		 		fSize = fDefaultSize	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		
	ENDSWITCH
ENDPROC



FUNC TEXT_LABEL_63 GET_GB_BIKER_DEFEND_PED_SCENARIO(INT iVariation, FACTORY_ID factoryId, INT iPed)
	TEXT_LABEL_63 tl63Scen
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryID
				CASE FACTORY_ID_METH_1 
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_WINDOW_SHOP_BROWSE"						BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET" 						BREAK
						CASE 5 	tl63Scen = "CODE_HUMAN_MEDIC_KNEEL"								BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_SMOKING"								BREAK 
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_2
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_SMOKING"								BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_WINDOW_SHOP_BROWSE"						BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"							BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET" 						BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"							BREAK
						CASE 6 	tl63Scen = "CODE_HUMAN_MEDIC_KNEEL"								BREAK 
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_3
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_WINDOW_SHOP_BROWSE"						BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_WINDOW_SHOP_BROWSE"						BREAK
						CASE 4	tl63Scen = "CODE_HUMAN_MEDIC_KNEEL" 							BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK 
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_4
					SWITCH iPed
						CASE 0	tl63Scen = "CODE_HUMAN_PATROL_2H"								BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_WINDOW_SHOP_BROWSE"						BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET" 						BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_SMOKING"								BREAK 
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK



		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 1	tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH"						BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET" 						BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 6 	tl63Scen = "CODE_HUMAN_PATROL_1H"								BREAK 
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_2
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 1	tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH"						BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_GUARD_STAND" 							BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 6 	tl63Scen = "CODE_HUMAN_PATROL_1H"								BREAK 
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_3
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 1	tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH"						BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET" 						BREAK
						CASE 5 	tl63Scen = "CODE_HUMAN_PATROL_1H"								BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK 
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_4
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_WINDOW_SHOP_BROWSE"						BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 3 	tl63Scen = "CODE_HUMAN_PATROL_1H"								BREAK
						CASE 4	tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH" 					BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK 
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK


		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT"						BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_DRUG_DEALER"							BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_DRUG_DEALER_HARD" 						BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK 
						CASE 7 	tl63Scen = "WORLD_HUMAN_SMOKING"								BREAK
						CASE 8 	tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT_UPRIGHT"				BREAK
					ENDSWITCH
				BREAK
				
				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT"						BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_DRUG_DEALER"							BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_DRUG_DEALER_HARD" 						BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK 
						CASE 7 	tl63Scen = "WORLD_HUMAN_SMOKING"								BREAK
						CASE 8 	tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT_UPRIGHT"				BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_DRUG_DEALER"							BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT"						BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 3 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_SMOKING" 								BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_DRUG_DEALER_HARD"						BREAK 
						CASE 7 	tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT_UPRIGHT"				BREAK
						CASE 8 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 9 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_SMOKING"								BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT"						BREAK
						CASE 2 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 3 	tl63Scen = "CODE_HUMAN_PATROL_2H"								BREAK
						CASE 4	tl63Scen = "WORLD_HUMAN_DRUG_DEALER" 							BREAK
						CASE 5 	tl63Scen = "WORLD_HUMAN_GUARD_STAND"							BREAK
						CASE 6 	tl63Scen = "WORLD_HUMAN_STAND_IMPATIENT_UPRIGHT"				BREAK 
						CASE 7 	tl63Scen = "WORLD_HUMAN_HANG_OUT_STREET"						BREAK
						CASE 8 	tl63Scen = "WORLD_HUMAN_DRUG_DEALER_HARD"						BREAK
						CASE 9 	tl63Scen = "WORLD_HUMAN_SMOKING"								BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					SWITCH iPed
						CASE 0	tl63Scen = "WORLD_HUMAN_GUARD_STAND"			BREAK
						CASE 1	tl63Scen = "WORLD_HUMAN_COP_IDLES"				BREAK
						CASE 2	tl63Scen = "WORLD_HUMAN_COP_IDLES"				BREAK
						CASE 3	tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH"		BREAK			
						CASE 7	tl63Scen = "WORLD_HUMAN_GUARD_STAND"			BREAK
						CASE 8	tl63Scen = "WORLD_HUMAN_COP_IDLES"				BREAK
						CASE 9	tl63Scen = "CODE_HUMAN_MEDIC_TIME_OF_DEATH"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl63Scen
ENDFUNC

FUNC INT GET_GB_BIKER_DEFEND_VEH_PED_SHOULD_USE(INT iVariation, FACTORY_ID factoryId, INT iPed)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iPed
						CASE 0
						CASE 1
						CASE 4
						CASE 5
							RETURN 0 // van

						
						CASE 2		RETURN 1 // bike
						CASE 3		RETURN 2 // bike
						
						CASE 6
						CASE 7
						CASE 8
						CASE 9
							RETURN 3 // Van
						
						CASE 10
							RETURN 4 // Bike
						
						CASE 11
							RETURN 5 // Bike
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					SWITCH iPed
						CASE 4
						CASE 5
						CASE 6
							RETURN 2
						CASE 10
							RETURN 0
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC INT GET_GB_BIKER_DEFEND_VEH_PED_SHOULD_ESCORT(INT iVariation, FACTORY_ID factoryId, INT iPed)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iPed
						CASE 2 RETURN 0
						CASE 3 RETURN 0
						
						CASE 10
						CASE 11
							RETURN 3
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC INT GET_GB_DEFEND_DRIVER_OF_ENEMY_VEH(INT iVariation, FACTORY_ID factoryId, INT iVeh)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iVeh
						CASE 0		RETURN 0 // Van
						CASE 1		RETURN 2 // bike
						CASE 2		RETURN 3 // bike
						
						CASE 3		RETURN 6 // Van
						CASE 4		RETURN 10 // bike
						CASE 5		RETURN 11 // bike
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					SWITCH iVeh
						CASE 2
							RETURN 4
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC


PROC INIT_GB_BIKER_DEFEND_AMBIENT_VEH_SPAWN_COORDS(INT iVariation, FACTORY_ID factoryId, INT iVeh, VECTOR &vLoc, FLOAT &fHeading)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1
					SWITCH iVeh
						CASE 0	vLoc =  <<420.4612, 6518.7144, 26.7371>> 		fHeading = 141.1983	BREAK
						CASE 1	vLoc =  <<412.1498, 6490.4575, 27.3356>> 		fHeading = 123.5980	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_2
					SWITCH iVeh
						CASE 0	vLoc =  <<92.1398, 165.1692, 103.5768>> 		fHeading = 101.9993	BREAK
						CASE 1	vLoc =  <<123.5997, 164.8985, 103.6558>> 		fHeading = 274.1986	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_3
					SWITCH iVeh
						CASE 0	vLoc =  <<2846.7644, 4444.4043, 47.5685>> 		fHeading = 336.0000	BREAK
						CASE 1	vLoc =  <<2881.7446, 4461.1416, 47.4672>> 		fHeading = 161.7989	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_4
					SWITCH iVeh
						CASE 0	vLoc =  <<148.0495, -2484.1375, 4.9982>> 		fHeading = 111.1952	BREAK
						CASE 1	vLoc =  <<144.4489, -2477.7046, 5.0000>> 		fHeading = 339.7963	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			
		BREAK
		
		
//		CASE GB_BIKER_DEFEND_METH
//			SWITCH factoryID
//				CASE FACTORY_ID_METH_1 
//					SWITCH iVeh
//						CASE 0	vLoc =  <<164.9940, 6414.1108, 30.1680>> 		fHeading = 274.0000	BREAK
//						CASE 1	vLoc =  <<167.6190, 6433.5518, 30.3000>> 		fHeading = 245.1940	BREAK
//					ENDSWITCH
//				BREAK
//			ENDSWITCH
//		BREAK	


		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					SWITCH iVeh
						CASE 0	vLoc =  <<-701.5576, 5770.4360, 16.3310>> 		fHeading = 234.5993	BREAK
						CASE 1	vLoc =  <<-688.4068, 5772.0835, 16.3309>> 		fHeading = 331.7997	BREAK
					ENDSWITCH
				BREAK

			
				CASE FACTORY_ID_CRACK_2
					SWITCH iVeh
						CASE 0	vLoc =  <<335.6994, -213.9058, 53.0863>> 		fHeading = 249.3986	BREAK
						CASE 1	vLoc =  <<326.4712, -220.5476, 53.0863>> 		fHeading = 271.2477	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_3
					SWITCH iVeh
						CASE 0	vLoc =  <<1132.7852, 2662.0032, 37.0065>> 		fHeading = 180.0001	BREAK
						CASE 1	vLoc =  <<1124.1130, 2647.7542, 36.9964>> 		fHeading = 180.8006	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_4
					SWITCH iVeh
						CASE 0	vLoc =  <<565.9390, -1768.6160, 28.1440>> 		fHeading = 65.1990	BREAK
						CASE 1	vLoc =  <<563.4702, -1753.7192, 28.1566>> 		fHeading = 153.9997	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
 
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iVeh
						CASE 0	vLoc =  <<1308.1060, 4309.1230, 36.5660>> 		fHeading = 148.2000	BREAK
						CASE 1	vLoc =  <<1310.0150, 4226.2451, 29.6250>> 		fHeading = 78.3990	BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iVeh
						CASE 0	vLoc =  <<1653.3340, 35.6000, 171.8810>>	 	fHeading = 166.20000	BREAK
						CASE 1	vLoc =  <<1669.4110, -18.3920, 172.7750>> 		fHeading = 16.3990		BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iVeh
						CASE 0	vLoc =  <<2364.6250, 3130.8611, 47.2090>> 		fHeading = 37.0000	BREAK
						CASE 1	vLoc =  <<2331.0090, 3134.6870, 47.1690>> 		fHeading = 217.7990	BREAK
						CASE 2	vLoc =  <<2335.9031, 3138.5610, 47.1910>> 		fHeading = 277.9990	BREAK
						CASE 3	vLoc =  <<2368.6328, 3134.8896, 47.3275>> 		fHeading = 70.5998	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iVeh
						CASE 0	vLoc =  <<-456.8816, -987.8931, 22.5453>> 		fHeading = 209.9958	BREAK
						CASE 1	vLoc =  <<-454.2304, -981.0823, 22.5453>> 		fHeading = 284.9960	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
					SWITCH iVeh
						CASE 0	vLoc =  <<44.8598, 6337.4541, 30.2269>> 		fHeading = 285.3988	BREAK
						CASE 1	vLoc =  <<40.3153, 6350.6826, 30.2398>> 		fHeading = 121.3984	BREAK
						
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_METH_2
					SWITCH iVeh
						CASE 0	vLoc =  <<1464.1119, -1642.1450, 67.6167>> 		fHeading = 297.1994	BREAK
						CASE 1	vLoc =  <<1436.8616, -1651.0448, 62.3306>> 		fHeading = 251.9990	BREAK
						
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_METH_3
					SWITCH iVeh
						CASE 0	vLoc =  <<210.6828, 2468.0945, 54.6319>> 		fHeading = 194.7996	BREAK
						CASE 1	vLoc =  <<209.6650, 2448.6548, 56.6416>> 		fHeading = 184.7997	BREAK
						
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_METH_4
					SWITCH iVeh
						CASE 0	vLoc =  <<1174.3517, -3123.0225, 5.0280>> 		fHeading = 358.5992	BREAK
						CASE 1	vLoc =  <<1175.0662, -3106.4766, 5.0280>> 		fHeading = 360.3989	BREAK
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_1
					SWITCH iVeh
						CASE 0	vLoc =  <<431.7915, 6534.3662, 26.8913>> 		fHeading = 167.1974	BREAK
						CASE 1	vLoc =  <<428.0981, 6505.8315, 27.0259>> 		fHeading = 165.1974	BREAK
						
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_WEED_2
					SWITCH iVeh
						CASE 0	vLoc =  <<108.2768, 169.3776, 103.5976>> 		fHeading = 60.9996	BREAK
						CASE 1	vLoc =  <<91.4248, 175.1380, 103.5171>> 		fHeading = 52.7974	BREAK
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_3
					SWITCH iVeh
						CASE 0	vLoc =  <<2854.4858, 4434.3257, 47.8032>>		fHeading = 84.9977	BREAK
						CASE 1	vLoc =  <<2844.2014, 4483.4321, 47.7325>>		fHeading = 250.5970	BREAK
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_4
					SWITCH iVeh
						CASE 0	vLoc =  <<144.2524, -2466.5398, 5.0399>>		fHeading = 44.7984	BREAK
						CASE 1	vLoc =  <<153.0709, -2480.2361, 4.9875>>		fHeading = 332.3980	BREAK
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_1
					SWITCH iVeh
						CASE 0	vLoc =  <<62.6541, 6463.3184, 30.2790>>		fHeading = 314.5989	BREAK
						CASE 1	vLoc =  <<59.9557, 6468.7554, 30.4253>>		fHeading = 14.7967	BREAK
						
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_CRACK_2
					SWITCH iVeh
						CASE 0	vLoc =  <<-1462.5432, -391.5512, 37.2335>>		fHeading = 298.5990		BREAK
						CASE 1	vLoc =  <<-1473.1418, -406.7394, 36.0244>>		fHeading = 20.5987		BREAK
							
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_CRACK_3
					SWITCH iVeh
						CASE 0	vLoc =  <<403.2658, 3585.9207, 32.2130>>		fHeading = 166.9994	BREAK
						CASE 1	vLoc =  <<377.4701, 3597.4551, 32.5180>>		fHeading = 86.9989	BREAK
							
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_CRACK_4
					SWITCH iVeh
						CASE 0	vLoc =  <<-257.0101, -2599.9409, 5.0006>>		fHeading = 272.9997	BREAK
						CASE 1	vLoc =  <<-268.5474, -2574.4556, 5.0006>>		fHeading = 328.9992	BREAK
							
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iVeh
						CASE 0	vLoc =  <<-415.4477, 6180.1260, 30.4782>>		fHeading = 93.5991	BREAK
						CASE 1	vLoc =  <<-403.3076, 6165.1187, 30.5200>>		fHeading = 174.7989	BREAK
							
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iVeh
						CASE 0	vLoc =  <<-1182.8167, -1386.9044, 3.6780>>		fHeading = 38.5988	BREAK
						CASE 1	vLoc =  <<-1178.6821, -1400.9579, 3.6539>>		fHeading = 38.1988	BREAK
							
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iVeh
						CASE 0	vLoc =  <<651.0166, 2778.5940, 40.7991>>		fHeading = 6.1992	BREAK
						CASE 1	vLoc =  <<646.1464, 2765.4939, 41.0057>>		fHeading = 350.5991	BREAK
					ENDSWITCH
				BREAK
			
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iVeh
						CASE 0	vLoc =  <<640.9509, -2673.5842, 5.0995>>		fHeading = 24.3993	BREAK
						CASE 1	vLoc =  <<660.1951, -2664.7800, 5.0812>>		fHeading = 358.5990	BREAK			
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_1
					SWITCH iVeh
						CASE 0	vLoc =  <<-160.1379, 6334.8423, 30.5854>>		fHeading = 272.1996	BREAK
						CASE 1	vLoc =  <<-164.1193, 6338.4482, 30.5672>>		fHeading = 21.7994	BREAK			
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_2
					SWITCH iVeh
						CASE 0	vLoc =   <<298.2625, -774.7745, 28.3136>>		fHeading = 118.9991	BREAK
						CASE 1	vLoc =   <<330.5925, -777.4604, 28.2740>>		fHeading = 339.3969	BREAK			
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_3
					SWITCH iVeh
						CASE 0	vLoc =   <<1662.6133, 4846.6245, 40.9127>>		fHeading = 184.5990	BREAK
						CASE 1	vLoc =   <<1676.0900, 4859.0615, 41.0724>>		fHeading = 6.5989	BREAK			
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iVeh
						CASE 0	vLoc =   <<-344.4102, -2777.2861, 4.0002>>		fHeading = 168.1990	BREAK
						CASE 1	vLoc =   <<-335.7935, -2764.7883, 4.0002>>		fHeading = 239.7977	BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC


PROC INIT_GB_BIKER_DEFEND_PLAYER_VEH_SPAWN_COORDS(INT iVariation, FACTORY_ID factoryId, INT iVeh, VECTOR &vLoc, FLOAT &fHeading)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1
					SWITCH iVeh
						CASE 0	vLoc =  <<1686.5471, 3603.9878, 34.4096>>  		fHeading =  81.5997	BREAK 
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_2
					SWITCH iVeh
						CASE 0	vLoc =   <<233.3469, 129.9368, 101.5997>>  		fHeading =  42.3988	BREAK 
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_3
					SWITCH iVeh
						CASE 0	vLoc =   <<2402.0601, 5023.5938, 45.0621>>  		fHeading =  196.8002	BREAK 
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_4
					SWITCH iVeh
						CASE 0	vLoc =   <<-1129.4659, -1455.7531, 3.9260>>  		fHeading =  194.1990	BREAK 
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC INIT_GB_BIKER_DEFEND_ENEMY_VEH_SPAWN_COORDS(INT iVariation, FACTORY_ID factoryId, INT iVeh, VECTOR &vLoc, FLOAT &fHeading)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1
					SWITCH iVeh
						CASE 0	vLoc =  <<-111.3476, 6419.3032, 30.4354>>  		fHeading = 315.5944	BREAK // Van
						CASE 1	vLoc =  <<-118.8317, 6408.9048, 30.3944>>   	fHeading = 318.8315	BREAK // Escort Biker
						CASE 2	vLoc =  <<-122.8714, 6410.1118, 30.4141>> 		fHeading = 311.2798	BREAK // Escort biker
						
						CASE 3	vLoc =  <<-170.4088, 6213.2241, 30.1815>>  		fHeading = 133.3307	BREAK // Van
						CASE 4	vLoc =  <<-165.9438, 6222.9863, 30.2173>>   	fHeading = 134.2735	BREAK // Escort Biker
						CASE 5	vLoc =  <<-158.0645, 6219.8472, 30.1980>> 		fHeading = 138.7301	BREAK // Escort biker
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_2
					SWITCH iVeh
						CASE 0	vLoc =  <<398.7870, -981.6014, 28.4083>>  		fHeading = 179.7737	BREAK // Van
						CASE 1	vLoc =  <<401.2792, -973.0635, 28.3816>>   		fHeading = 180.0338	BREAK // Escort Biker
						CASE 2	vLoc =  <<397.1577, -971.6725, 28.2945>> 		fHeading = 176.2522	BREAK // Escort biker
						
						CASE 3	vLoc =  <<-71.6871, -593.0323, 35.3148>>  		fHeading = 341.0485	BREAK // Van
						CASE 4	vLoc =  <<-73.0525, -602.5574, 35.3150>>  	 	fHeading = 343.0564	BREAK // Escort Biker
						CASE 5	vLoc =  <<-76.9337, -601.3236, 35.3148>> 		fHeading = 343.7657	BREAK // Escort biker
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_3
					SWITCH iVeh
						CASE 0	vLoc =  <<1806.3810, 4570.5293, 35.3520>>  		fHeading = 277.4597	BREAK // Van
						CASE 1	vLoc =  <<1797.5181, 4571.2603, 35.7099>>   	fHeading = 276.3600	BREAK // Escort Biker
						CASE 2	vLoc =  <<1797.5057, 4567.7061, 35.7042>> 		fHeading = 278.8102	BREAK // Escort biker
						
						CASE 3	vLoc =  <<2295.0513, 5031.6992, 43.0186>>  		fHeading = 315.2822	BREAK // Van
						CASE 4	vLoc =  <<2289.5996, 5024.0210, 42.7022>>   	fHeading = 313.2981	BREAK // Escort Biker
						CASE 5	vLoc =  <<2286.7188, 5026.1216, 42.7125>> 		fHeading = 318.4515	BREAK // Escort biker
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iVeh
						CASE 0	vLoc =   <<-113.9498, -2625.6519, 5.0007>>  	fHeading = 271.1869	BREAK // Van
						CASE 1	vLoc =   <<-122.3716, -2627.0032, 5.0007>>   	fHeading = 269.6436	BREAK // Escort Biker
						CASE 2	vLoc =   <<-122.3787, -2623.9382, 5.0007>> 		fHeading = 273.6377	BREAK // Escort biker
						
						CASE 3	vLoc =  <<335.7822, -2474.5771, 4.7181>>  		fHeading = 353.4243	BREAK // Van
						CASE 4	vLoc =  <<336.7179, -2482.0464, 4.5784>>   		fHeading = 354.5690	BREAK // Escort Biker
						CASE 5	vLoc =  <<333.2179, -2481.5439, 4.5833>> 		fHeading = 356.8736	BREAK // Escort biker
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
					SWITCH iVeh
						CASE 2	vLoc =    <<35.6453, 6379.8315, 85.4362>> 		fHeading = 203.3974	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_2
					SWITCH iVeh
						CASE 2	vLoc =    <<1423.6899, -1614.8666, 121.7332>>		fHeading = 213.5477	BREAK
						
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_METH_3
					SWITCH iVeh
						CASE 2	vLoc =    <<264.8855, 2449.8015, 123.3579>> 		fHeading = 68.5968	BREAK
						
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_METH_4
					SWITCH iVeh
					
						CASE 2	vLoc =    <<1139.7706, -3110.7109, 73.6126>> 		fHeading =  282.5974	BREAK
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_1
					SWITCH iVeh
						CASE 2	vLoc =    <<444.4752, 6549.4985, 70.3524>> 		fHeading = 129.1956	BREAK
						
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_WEED_2
					SWITCH iVeh
						CASE 2	vLoc =    <<69.4694, 136.7592, 164.1646>> 		fHeading = 318.9926	BREAK
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_3
					SWITCH iVeh
						CASE 2	vLoc =    <<2827.8391, 4402.7920, 91.9611>> 		fHeading = 320.7959	BREAK
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_4
					SWITCH iVeh
						CASE 2	vLoc =     <<175.3683, -2492.4749, 46.4167>> 		fHeading = 48.1967	BREAK
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_1
					SWITCH iVeh
						CASE 2	vLoc =    <<123.0383, 6481.6953, 76.9569>> 		fHeading =  94.1954	BREAK
						
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_CRACK_2
					SWITCH iVeh
						CASE 2	vLoc =    <<-1427.8998, -380.5865, 75.9665>> 		fHeading = 100.7965	BREAK
							
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_CRACK_3
					SWITCH iVeh
						CASE 2	vLoc =    <<373.0848, 3626.5007, 72.6360>> 		fHeading = 201.7981	BREAK
							
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_CRACK_4
					SWITCH iVeh
						CASE 2	vLoc =    <<-295.0657, -2574.5847, 49.1612>> 		fHeading = 232.1968	BREAK
							
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iVeh
						CASE 2	vLoc =    <<-386.6624, 6194.0889, 75.2413>>		fHeading = 127.3972	BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iVeh
						CASE 2	vLoc =   <<-1203.0531, -1377.2582, 44.2625>> 		fHeading = 249.7971	BREAK
							
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iVeh
						CASE 2	vLoc =    <<671.9219, 2760.8440, 79.4217>> 		fHeading =  54.5973	BREAK
					ENDSWITCH
				BREAK
			
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iVeh
						CASE 2	vLoc =     <<629.8040, -2713.9761, 43.4253>> 		fHeading =  325.5972	BREAK	
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_1
					SWITCH iVeh
						CASE 2	vLoc =    <<-126.0931, 6369.8242, 91.1849>> 		fHeading =  101.5969	BREAK		
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_2
					SWITCH iVeh
						CASE 2	vLoc =    <<356.3581, -738.9581, 97.4097>> 		fHeading =  118.1953	BREAK			
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_3
					SWITCH iVeh
						CASE 2	vLoc =    <<1689.4490, 4865.8145, 89.2562>> 		fHeading = 135.5966	BREAK		
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iVeh
						CASE 2	vLoc =    <<-389.6134, -2769.5044, 57.1299>>		fHeading =  292.1962	BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC




PROC INIT_GB_BIKER_DEFEND_PROP_SPAWN_COORDS(INT iVariation, FACTORY_ID factoryId, INT iProp, VECTOR &vLoc, FLOAT &fHeading)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1
					SWITCH iProp
						CASE 0	vLoc = <<405.3802, 6494.2075, 27.0403>> 		fHeading = 322.0000	BREAK
						CASE 1	vLoc = <<418.7116, 6522.6357, 26.7398>> 		fHeading = 335.7992	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_2
					SWITCH iProp
						CASE 0	vLoc = <<97.6693, 166.7286, 103.5447>> 			fHeading = 260.1995	BREAK
						CASE 1	vLoc = <<118.2530, 164.1741, 103.5544>> 		fHeading = 11.3979	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_3
					SWITCH iProp
						CASE 0	vLoc = <<2845.2964, 4450.7939, 47.5164>> 		fHeading = 336.0000	BREAK
						CASE 1	vLoc = <<2875.2263, 4461.1021, 47.5351>> 		fHeading = 161.7989	BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_4
					SWITCH iProp
						CASE 0	vLoc = <<135.9397, -2480.5591, 5.0027>> 		fHeading = 258.9976	BREAK
						CASE 1	vLoc = <<139.1387, -2474.0254, 5.0027>> 		fHeading = 46.3974	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK



		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryID
				CASE FACTORY_ID_METH_1 
					SWITCH iProp
						CASE 0	vLoc = <<1697.7975, 3608.5864, 34.3528>> 		fHeading = 56.3996	BREAK
						CASE 1	vLoc = <<1695.9088, 3606.9880, 34.3861>> 		fHeading = 25.7995	BREAK
						CASE 2	vLoc = <<1692.5194, 3601.5139, 34.6073>> 		fHeading = 185.5998	BREAK
						CASE 3	vLoc = <<1694.9180, 3597.3601, 34.7430>> 		fHeading = 118.7990	BREAK
						CASE 4	vLoc = <<1686.5471, 3603.9878, 36.4096>> 		fHeading = 118.7990	BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_METH_2 
					SWITCH iProp
						CASE 0	vLoc = <<242.3710, 115.9980, 101.6296>> 		fHeading = 85.1985	BREAK
						CASE 1	vLoc = <<234.7437, 133.4675, 101.6024>> 		fHeading = 247.7977	BREAK
						CASE 2	vLoc = <<246.3067, 125.6538, 101.7239>> 		fHeading = 56.5982	BREAK
						CASE 3	vLoc = <<230.8449, 127.4170, 101.7243>> 		fHeading = 321.5981	BREAK
						CASE 4	vLoc = <<233.3469, 129.9368, 103.5997>> 		fHeading = 118.7990	BREAK
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_METH_3
					SWITCH iProp
						CASE 0	vLoc = <<2406.7170, 5027.9927, 45.0998>> 		fHeading = 227.1999	BREAK
						CASE 1	vLoc = <<2397.4866, 5029.5498, 44.9932>> 		fHeading = 137.9983	BREAK
						CASE 2	vLoc = <<2406.1240, 5032.3799, 44.9932>> 		fHeading = 21.1980	BREAK
						CASE 3	vLoc =  <<2402.0601, 5023.5938, 47.0621>>  		fHeading =  196.8002	BREAK 

					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_METH_4 
					SWITCH iProp
						CASE 0	vLoc = <<-1130.9890, -1445.7111, 4.1410>> 		fHeading = 125.3980	BREAK
						CASE 1	vLoc = <<-1137.1429, -1447.0229, 4.1170>> 		fHeading = 201.1960	BREAK
						CASE 2	vLoc = <<-1139.1165, -1438.6584, 3.9887>> 		fHeading = 303.5997	BREAK
						CASE 3	vLoc = <<-1127.2850, -1448.9509, 4.3397>> 		fHeading = 304.9997	BREAK
						CASE 4	vLoc = <<-1129.4659, -1455.7531, 5.9260>> 		fHeading = 304.9997	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK


		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iProp
						CASE 0	vLoc = <<1312.6670, 4272.5278, 32.9160>>		fHeading = 349.5990		BREAK
						CASE 1	vLoc = <<1307.6970, 4232.8301, 32.9230>>		fHeading = 187.5990		BREAK
						CASE 2	vLoc = <<1301.5640, 4232.3579, 32.9140>>		fHeading = 304.5960		BREAK
						CASE 3	vLoc = <<1302.5160, 4244.7139, 32.9163>>		fHeading = 350.2000		BREAK
					
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iProp
						CASE 0	vLoc = <<1659.4766, 3.0453, 165.066>>		fHeading = 348.1996		BREAK
						CASE 1	vLoc = <<1659.5912, 1.8871, 165.1702>>		fHeading = 0.0000		BREAK
						CASE 2	vLoc = <<1661.2211, 4.9775, 165.1702>>		fHeading = 87.0000		BREAK

					
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iProp
						CASE 0	vLoc = <<2357.6670, 3130.5291, 47.2090>>		fHeading = 20.7990		BREAK
						CASE 1	vLoc = <<2356.0911, 3128.7085, 47.2087>>		fHeading = 88.7998		BREAK	
						CASE 2	vLoc = <<2357.7473, 3129.2952, 47.2608>>		fHeading = 36.3997		BREAK
						CASE 3	vLoc = <<2355.5730, 3131.2981, 47.2610>>		fHeading = 319.1980		BREAK	
						CASE 4	vLoc = <<2353.1899, 3128.9456, 47.2608>>		fHeading = 278.7999		BREAK	
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iProp
						CASE 0	vLoc = <<-445.1510, -983.9860, 22.7630>>		fHeading = 180.7980		BREAK
						CASE 1	vLoc = <<-446.4732, -991.1110, 22.7137>>		fHeading = 295.7999		BREAK	
						CASE 2	vLoc = <<-445.3309, -986.3227, 22.8086>>		fHeading = 194.1998		BREAK
						CASE 3	vLoc = <<-446.4166, -989.4883, 22.7682>>		fHeading = 219.1995		BREAK	
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK


		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
					SWITCH iProp
						CASE 0	vLoc = <<54.5086, 6340.1064, 30.3811>>		fHeading = 11.7994		BREAK
						CASE 1	vLoc = <<51.4362, 6339.7983, 30.2305>>		fHeading = 32.5993		BREAK	
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_METH_2
					SWITCH iProp
						CASE 0	vLoc = <<1455.0144, -1647.9817, 65.7651>> 		fHeading = 6.8000	BREAK
						CASE 1	vLoc = <<1452.2568, -1649.2120, 65.2253>> 		fHeading = 51.9999	BREAK
						
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_METH_3
					SWITCH iProp
						CASE 0	vLoc =  <<201.0682, 2460.0491, 54.6985>> 		fHeading = 346.5999	BREAK
						CASE 1	vLoc =  <<204.0793, 2461.2610, 54.7406>> 		fHeading = 35.9998	BREAK
						
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_METH_4
					SWITCH iProp
						CASE 0	vLoc =  <<1178.8654, -3112.8711, 5.0326>> 		fHeading = 248.3997	BREAK
						CASE 1	vLoc =  <<1179.1432, -3115.5833, 5.0326>> 		fHeading = 302.1993	BREAK
						
					ENDSWITCH
				BREAK
			
				CASE FACTORY_ID_WEED_1
					SWITCH iProp
						CASE 0	vLoc =  <<418.4595, 6522.8394, 26.7418>> 		fHeading = 300.5996	BREAK
						CASE 1	vLoc =  <<418.1917, 6519.4038, 26.7029>> 		fHeading = 227.7991	BREAK
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_2
					SWITCH iProp
						CASE 0	vLoc =  <<99.9754, 172.7944, 103.5831>> 		fHeading = 330.3997	BREAK
						CASE 1	vLoc =  <<103.1075, 171.8262, 103.5847>>		fHeading = 10.5997	BREAK
						
					ENDSWITCH
				BREAK

	
				CASE FACTORY_ID_WEED_3
					SWITCH iProp
						CASE 0	vLoc =  <<2846.2759, 4450.9561, 47.5169>>		fHeading = 77.3990	BREAK
						CASE 1	vLoc =  <<2847.8354, 4448.2271, 47.5273>>		fHeading = 150.9987	BREAK
						
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_WEED_4
					SWITCH iProp
						CASE 0	vLoc =  <<138.5471, -2471.6526, 5.0046>>		fHeading = 94.1990	BREAK
						CASE 1	vLoc =  <<137.0719, -2474.2146, 5.0046>>		fHeading = 12.9989	BREAK
						
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_CRACK_1
					SWITCH iProp
						CASE 0	vLoc = <<53.4699, 6485.7031, 30.4412>>		fHeading = 273.59939	BREAK
						CASE 1	vLoc = <<51.2500, 6487.7759, 30.4299>>		fHeading = 155.9991		BREAK
						
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_CRACK_2
					SWITCH iProp
						CASE 0	vLoc =  <<-1462.3510, -383.8051, 37.7350>>		fHeading = 21.5998	BREAK
						CASE 1	vLoc =  <<-1460.1718, -381.3084, 37.7662>>		fHeading = 97.1997	BREAK
							
					ENDSWITCH
				BREAK


				
				CASE FACTORY_ID_CRACK_3
					SWITCH iProp
						CASE 0	vLoc = <<386.4111, 3586.0801, 32.2969>>		fHeading = 27.20004	BREAK
						CASE 1	vLoc = <<390.0789, 3586.0095, 32.2969>>		fHeading = 316.9999	BREAK
							
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_CRACK_4
					SWITCH iProp
						CASE 0	vLoc =  <<-255.7577, -2591.1316, 5.0053>>		fHeading = 266.5997	BREAK
						CASE 1	vLoc =  <<-253.9852, -2588.7761, 5.0053>>		fHeading = 188.5998	BREAK
							
					ENDSWITCH
				BREAK
	
				
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iProp
						CASE 0	vLoc =  <<-413.8132, 6173.6948, 30.4828>>		fHeading = -3.0004	BREAK
						CASE 1	vLoc =  <<-411.7779, 6171.5322, 30.4828>>		fHeading = -73.2004	BREAK
							
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iProp
						CASE 0	vLoc =  <<-1174.0181, -1380.6810, 3.9413>>		fHeading = 305.1999	BREAK
						CASE 1	vLoc =  <<-1172.3479, -1378.5668, 3.9732>>		fHeading = 212.1997	BREAK
							
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iProp
						CASE 0	vLoc =  <<635.9418, 2782.9858, 41.0262>>		fHeading = 0.0000	BREAK
						CASE 1	vLoc =  <<638.7815, 2784.4395, 41.0086>>		fHeading = 74.7999	BREAK
					ENDSWITCH
				BREAK

			
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iProp
						CASE 0	vLoc =  <<669.8441, -2666.4895, 5.0858>>		fHeading = 238.2000	BREAK
						CASE 1	vLoc =  <<670.0934, -2669.3149, 5.0858>>		fHeading = 321.3994	BREAK			
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_ID_1
					SWITCH iProp
						CASE 0	vLoc =  <<-160.1379, 6334.8423, 30.5854>>		fHeading = 272.1996	BREAK
						CASE 1	vLoc =  <<-164.1193, 6338.4482, 30.5672>>		fHeading = 21.7994	BREAK			
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_2
					SWITCH iProp
						CASE 0	vLoc =   <<301.5947, -764.3118, 28.3184>>		fHeading = 12.1988	BREAK
						CASE 1	vLoc =   <<304.5190, -760.4872, 28.3165>>		fHeading = 113.7985	BREAK			
					ENDSWITCH
				BREAK

				
				CASE FACTORY_ID_FAKE_ID_3
					SWITCH iProp
						CASE 0	vLoc =   <<1659.0114, 4854.1348, 40.9520>>		fHeading = 130.5971	BREAK
						CASE 1	vLoc =   <<1659.3823, 4850.4619, 40.9512>>		fHeading = 77.3968	BREAK			
					ENDSWITCH
				BREAK


				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iProp
						CASE 0	vLoc =   <<-334.4207, -2773.8306, 4.0049>>		fHeading = 25.9982	BREAK
						CASE 1	vLoc =   <<-336.6407, -2776.9546, 4.0049>>		fHeading = 75.7981	BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC VECTOR GET_GB_BIKER_DEFEND_PACKAGE_SPAWN_COORDS(INT iVariation, FACTORY_ID factoryId, INT iPackage)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1
					SWITCH iPackage
						CASE 0		RETURN <<-111.3476, 6419.3032, 40.4354>> 
						CASE 1		RETURN <<-170.4088, 6213.2241, 40.1815>>
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_2
					SWITCH iPackage
						CASE 0		RETURN <<398.7870, -981.6014, 38.4083>> 
						CASE 1		RETURN <<-71.6871, -593.0323, 45.3148>>
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_3
					SWITCH iPackage
						CASE 0		RETURN  <<1806.3810, 4570.5293, 45.3520>> 
						CASE 1		RETURN <<2295.0513, 5031.6992, 53.0186>>
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iPackage
						CASE 0		RETURN <<-113.9498, -2625.6519, 15.0007>> 
						CASE 1		RETURN <<335.7822, -2474.5771, 14.7181>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					SWITCH iPackage
						CASE 0		RETURN <<-687.1215, 5760.5044, 26.5110>>
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_2
					SWITCH iPackage
						CASE 0		RETURN <<344.3741, -204.9075, 62.0242>>
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_3
					SWITCH iPackage
						CASE 0		RETURN <<1141.8840, 2642.7561, 47.1490>>
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_4
					SWITCH iPackage
						CASE 0		RETURN <<561.1011, -1747.8079, 42.4476>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK

		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iPackage
						CASE 0		RETURN <<1303.4489, 4235.9424, 33.0478>>
						CASE 1		RETURN <<1305.1309, 4239.2612, 33.0478>>
					ENDSWITCH
				BREAK

				//<<1661.9718, 2.6619, 165.1181>>
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iPackage
						CASE 0		RETURN  <<1662.0168, 1.8361, 165.1181>>
						CASE 1		RETURN  <<1660.6086, 0.5445, 165.1181>>
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iPackage
						CASE 0		RETURN <<2351.8130, 3132.0610, 47.3430>>
						CASE 1		RETURN <<2352.4641, 3133.3059, 47.3430>>
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iPackage
						CASE 0		RETURN <<-449.3700, -986.4580, 22.7550>>
						CASE 1		RETURN <<-449.3740, -988.9070, 22.7520>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN << 0.0, 0.0, 0.0 >>
ENDFUNC

FUNC INT GET_GB_BIKER_DEFEND_NUMBER_OF_INITIAL_PEDS_FOR_VARIATION(INT iVariation, FACTORY_ID factoryId, INT iNumPackages = 1)
	
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1		RETURN 7
				CASE FACTORY_ID_WEED_2		RETURN 7
				CASE FACTORY_ID_WEED_3		RETURN 7
				CASE FACTORY_ID_WEED_4		RETURN 7
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1	
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iNumPackages
						CASE 1	RETURN 6
						CASE 2	RETURN 12
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryID
				CASE FACTORY_ID_METH_1 
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
					RETURN 7
				
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					RETURN 7
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					RETURN 9
				CASE FACTORY_ID_FAKE_CASH_2
					RETURN 9
				CASE FACTORY_ID_FAKE_CASH_4
					RETURN 10
				CASE FACTORY_ID_FAKE_CASH_3
					RETURN 10
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					RETURN 10
				
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_GB_BIKER_DEFEND_NUMBER_OF_INITIAL_AMBIENT_VEHS_FOR_VARIATION(INT iVariation, FACTORY_ID factoryId)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1		RETURN 2
				CASE FACTORY_ID_WEED_2		RETURN 2
				CASE FACTORY_ID_WEED_3		RETURN 2
				CASE FACTORY_ID_WEED_4		RETURN 2
			ENDSWITCH
		BREAK
		
//		CASE GB_BIKER_DEFEND_METH
//			SWITCH factoryID
//				CASE FACTORY_ID_METH_1 
//					RETURN 2
//			ENDSWITCH
//		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
					RETURN 2
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					RETURN 2
				CASE FACTORY_ID_FAKE_CASH_4
					RETURN 2
				CASE FACTORY_ID_FAKE_CASH_3
					RETURN 4
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					RETURN 2
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN 0	
ENDFUNC

FUNC INT GET_GB_BIKER_DEFEND_NUMBER_OF_INITIAL_ENEMY_VEHS_FOR_VARIATION(INT iVariation, FACTORY_ID factoryId, INT iNumPackages = 1)
	SWITCH iVariation

		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1	
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
					SWITCH iNumPackages
						CASE 1	RETURN 3
						CASE 2	RETURN 6
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					RETURN 1
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN 0	
ENDFUNC

FUNC INT GET_ENEMY_VEHICLE_INT_FOR_VEHICLE(INT iVariation, FACTORY_ID factoryId, INT iVeh)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					SWITCH iVeh
						CASE 0
							RETURN 2
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
		
	RETURN iVeh				
ENDFUNC

FUNC INT GET_GB_BIKER_DEFEND_NUMBER_OF_INITIAL_PLAYER_VEHS_FOR_VARIATION(INT iVariation, FACTORY_ID factoryId)
	SWITCH iVariation

		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
					RETURN 1
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC
			
FUNC INT GET_GB_BIKER_DEFEND_PED_TO_ATTACH_PACKAGE_TO(INT iVariation, FACTORY_ID factoryId, INT iPackage)
	SWITCH iVariation

		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1	
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				
					SWITCH iPackage
						CASE 0 RETURN 0
						CASE 1 RETURN 6
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					RETURN 0
			ENDSWITCH
		BREAK
		
	ENDSWITCH

	RETURN -1	
ENDFUNC

FUNC INT GET_GB_BIKER_DEFEND_NUMBER_OF_INITIAL_PROPS_FOR_VARIATION(INT iVariation, FACTORY_ID factoryId, INT iNumPackages = 1)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1		RETURN 2
				CASE FACTORY_ID_WEED_2		RETURN 2
				CASE FACTORY_ID_WEED_3		RETURN 2
				CASE FACTORY_ID_WEED_4		RETURN 2
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_FAKE_ID
			RETURN iNumPackages
		
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryID
				CASE FACTORY_ID_METH_1 
					RETURN 5
				CASE FACTORY_ID_METH_2
					RETURN 5
				CASE FACTORY_ID_METH_3
					RETURN 4
				CASE FACTORY_ID_METH_4
					RETURN 4
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					RETURN 4
				CASE FACTORY_ID_FAKE_CASH_2
					RETURN 3
				CASE FACTORY_ID_FAKE_CASH_3
					RETURN 3
				CASE FACTORY_ID_FAKE_CASH_4
					RETURN 4
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					RETURN 2
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_GB_BIKER_DEFEND_VEHICLE_TO_ATTACH_CRATE_TO(INT iVariation, FACTORY_ID factoryId, INT iCrate)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4 
					SWITCH iCrate
						CASE 0
							RETURN 0
						CASE 1
							RETURN 3
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC INT GET_GB_BIKER_DEFEND_CRATE_TO_ATTACH_PROP_INDEX(INT iVariation, FACTORY_ID factoryId, INT iCrate)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_FAKE_ID
			RETURN iCrate
			
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1 
					RETURN 4
				CASE FACTORY_ID_METH_2
					RETURN 4
				CASE FACTORY_ID_METH_3
					RETURN 3
				CASE FACTORY_ID_METH_4
					RETURN 4
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC BOOL DOES_GB_BIKER_DEFEND_VARIATION_REQUIRE_CRATES_IN_BACK_OF_VAN(INT iVariation, FACTORY_ID factoryId)

	SWITCH iVariation
		CASE GB_BIKER_DEFEND_FAKE_ID
			RETURN FALSE
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_GB_BIKER_DEFEND_NUMBER_OF_PACKAGES_FOR_VARIATION(INT iVariation, FACTORY_ID factoryId)
	INT iNumInGang = 1
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1	
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
					iNumInGang += GB_GET_NUM_GOONS_IN_PLAYER_GANG(PLAYER_ID())
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_BIKER_DEFEND [GET_GB_BIKER_DEFEND_NUMBER_OF_PACKAGES_FOR_VARIATION] iNumInGang = ", iNumInGang)
					
					IF iNumInGang = 1
						RETURN 1
					ENDIF
					
					RETURN 2
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					RETURN 1
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
					iNumInGang += GB_GET_NUM_GOONS_IN_PLAYER_GANG(PLAYER_ID())
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     GB_BIKER_DEFEND [GET_GB_BIKER_DEFEND_NUMBER_OF_PACKAGES_FOR_VARIATION] iNumInGang = ", iNumInGang)
					
					IF iNumInGang = 1
						RETURN 1
					ENDIF
					
					RETURN 2
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC
FUNC INT GET_NUMBER_OF_WAVES_FOR_GB_BIKER_DEFEND(INT iVariation, FACTORY_ID factoryId)
	SWITCH iVariation
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1		RETURN 3
				CASE FACTORY_ID_WEED_2		RETURN 3
				CASE FACTORY_ID_WEED_3		RETURN 3
				CASE FACTORY_ID_WEED_4		RETURN 3
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_NUMBER_OF_PEDS_IN_DEFEND_GANG_CHASE_VEH(INT iVar, FACTORY_ID factoryId, INT iWave, INT iVeh)
	SWITCH iVar
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
					/*
					"3 waves of enemies
					Wave 0 : 4 peds on 4 bikes
					Wave 1: 6 peds in 1 cavalcade and 2 bikes
					Wave 2: Is a Maverick helicopoter with 4 vagos with rifle and 4 peds on 4 bikes"

					*/
					SWITCH iWave
						CASE 1
							SWITCH iVeh
								CASE 0	RETURN 4	// Cavalcade
								CASE 1	RETURN 1	// Bike
								CASE 2	RETURN 1	// Bike
							ENDSWITCH
						BREAK
						
						CASE 2
							SWITCH iVeh
								CASE 0	RETURN 4	// Cavalcade
								CASE 1	RETURN 1	// Bike
								CASE 2	RETURN 1	// Bike
								CASE 3	RETURN 1	// Bike
								CASE 4	RETURN 1	// Bike
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1
					RETURN 1
					
				CASE FACTORY_ID_METH_2
					RETURN 1
					
				CASE FACTORY_ID_METH_3
					RETURN 1
				CASE FACTORY_ID_METH_4
					RETURN 1
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_CASH_1
					RETURN 1
				CASE FACTORY_ID_FAKE_CASH_2
					RETURN 1
				CASE FACTORY_ID_FAKE_CASH_3
					RETURN 1
				CASE FACTORY_ID_FAKE_CASH_4
					RETURN 1
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC MODEL_NAMES GET_VEHICLE_MODEL_FOR_DEFEND_GANG_CHASE_VEH(INT iVar, FACTORY_ID factoryId, INT iWave, INT iVeh)
	SWITCH iVar
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
			//	CASE FACTORY_ID_WEED_4
					/*
					"3 waves of enemies
					Wave 0 : 4 peds on 4 bikes
					Wave 1: 6 peds in 1 cavalcade and 2 bikes
					Wave 2: Is a Maverick helicopoter with 4 vagos with rifle and 4 peds on 4 bikes"

					*/
					SWITCH iWave
						CASE 1
							SWITCH iVeh
								CASE 0	RETURN GBURRITO	// Cavalcade
								CASE 1	RETURN DAEMON	// Bike
								CASE 2	RETURN DAEMON	// Bike
							ENDSWITCH
						BREAK
						
						CASE 2
							SWITCH iVeh
								CASE 0	RETURN MAVERICK	// Maverick
								CASE 1	RETURN DAEMON	// Bike
								CASE 2	RETURN DAEMON	// Bike
								CASE 3	RETURN DAEMON	// Bike
								CASE 4	RETURN DAEMON	// Bike
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_4
					SWITCH iWave
						CASE 1
							SWITCH iVeh
								CASE 0	RETURN CAVALCADE	// Cavalcade
								CASE 1	RETURN BATI	// Bike
								CASE 2	RETURN BATI	// Bike
							ENDSWITCH
						BREAK
						
						CASE 2
							SWITCH iVeh
								CASE 0	RETURN MAVERICK	// Maverick
								CASE 1	RETURN BATI	// Bike
								CASE 2	RETURN BATI	// Bike
								CASE 3	RETURN BATI	// Bike
								CASE 4	RETURN BATI	// Bike
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1
					RETURN DAEMON
					
				CASE FACTORY_ID_METH_2
					RETURN BATI
				
				CASE FACTORY_ID_METH_3
					RETURN SANCHEZ
				CASE FACTORY_ID_METH_4
					RETURN BATI
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_CASH_1
					RETURN DAEMON
				CASE FACTORY_ID_FAKE_CASH_2
					RETURN BATI
				CASE FACTORY_ID_FAKE_CASH_3
					RETURN SANCHEZ
				CASE FACTORY_ID_FAKE_CASH_4
					RETURN BATI
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC


FUNC MODEL_NAMES GET_PED_MODEL_FOR_DEFEND_GANG_CHASE(INT iVar, FACTORY_ID factoryId)
	SWITCH iVar
		
		
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1
					RETURN  G_M_Y_Lost_01
					
				CASE FACTORY_ID_METH_2
					RETURN G_M_Y_BallaOrig_01
				
				CASE FACTORY_ID_METH_3
					RETURN  A_M_M_Hillbilly_01
					
				CASE FACTORY_ID_METH_4
					RETURN  G_M_M_ArmGoon_01
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_CASH_1 
					RETURN G_M_Y_Lost_01
				
				CASE FACTORY_ID_FAKE_CASH_2
					RETURN G_M_M_ChiGoon_02
					
				CASE FACTORY_ID_FAKE_CASH_3
					RETURN A_M_M_Hillbilly_02
					
				CASE FACTORY_ID_FAKE_CASH_4
					RETURN G_M_Y_Korean_01
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryID
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
					RETURN G_M_Y_Lost_01
					
				CASE FACTORY_ID_WEED_4
					RETURN G_M_Y_MexGoon_02
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC SETUP_GB_BIKER_DEFEND_CUSTOM_SPAWNS(INT iVar, FACTORY_ID factoryId)
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BIKER_CONTRA_DEFEND - [MAINTAIN_DEFEND_CONTRABAND_PLAYER_SPAWNING] Caleld with Var ", iVar, " and factoryId ", ENUM_TO_INT(factoryId))
	
	SWITCH iVar
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1	
					ADD_CUSTOM_SPAWN_POINT( <<446.4323, 6503.0796, 28.3633>>	,	179.3996)
					ADD_CUSTOM_SPAWN_POINT( <<445.7750, 6510.7295, 28.0500>>	,	360.9990)
					ADD_CUSTOM_SPAWN_POINT( <<424.6255, 6459.0332, 27.7887>>	,  	52.1989)
					ADD_CUSTOM_SPAWN_POINT( <<449.2216, 6460.0342, 28.1724>>	,	325.1988)
					ADD_CUSTOM_SPAWN_POINT( <<439.9934, 6449.7568, 28.1983>>	,	133.1982)
					ADD_CUSTOM_SPAWN_POINT( <<336.6075, 6482.7090, 28.4298>>	,  	335.9978)
					ADD_CUSTOM_SPAWN_POINT( <<335.6669, 6460.5923, 29.4889>>	,  	229.9972)
					ADD_CUSTOM_SPAWN_POINT( <<422.2368, 6460.2861, 27.9069>>	, 	45.5966)
				BREAK
				
				CASE FACTORY_ID_WEED_2	
					ADD_CUSTOM_SPAWN_POINT(<<143.3918, 184.3316, 105.0267>>		,	183.3435)
					ADD_CUSTOM_SPAWN_POINT(<<133.4269, 136.1507, 99.7704>>		,	80.7432)
					ADD_CUSTOM_SPAWN_POINT(<<129.1446, 137.8446, 99.7704>>		,  	80.7432)
					ADD_CUSTOM_SPAWN_POINT(<<36.9802, 198.6169, 105.0246>>		,	268.5427)
					ADD_CUSTOM_SPAWN_POINT(<<23.5788, 193.6709, 103.4859>>		,	320.3424)
					ADD_CUSTOM_SPAWN_POINT(<<27.9585, 205.5291, 105.7943>>		,  	190.3419)
					ADD_CUSTOM_SPAWN_POINT(<<176.0768, 151.0649, 101.1403>>		,  	142.9417)
					ADD_CUSTOM_SPAWN_POINT(<<170.3011, 138.1934, 98.1415>>		, 	48.3414)
				BREAK
				
				CASE FACTORY_ID_WEED_3	
					ADD_CUSTOM_SPAWN_POINT(<<2841.6790, 4396.5791, 48.0014>>	,	25.5993)
					ADD_CUSTOM_SPAWN_POINT(<<2887.2087, 4399.9780, 49.2967>>	,	4.9989)
					ADD_CUSTOM_SPAWN_POINT(<<2903.3687, 4414.4253, 47.8703>>	,  	15.9989)
					ADD_CUSTOM_SPAWN_POINT(<<2927.5725, 4471.8496, 46.9237>>	,	121.5987)
					ADD_CUSTOM_SPAWN_POINT(<<2913.7473, 4496.5063, 47.1427>>	,	148.9985)
					ADD_CUSTOM_SPAWN_POINT(<<2876.4871, 4506.8374, 46.8740>>	,  	62.9982)
					ADD_CUSTOM_SPAWN_POINT(<<2891.3662, 4503.7944, 47.0903>>	,  	147.1980)
					ADD_CUSTOM_SPAWN_POINT(<<2892.8872, 4487.6313, 47.1529>>	, 	118.9978)
				BREAK
				
				CASE FACTORY_ID_WEED_4	
					ADD_CUSTOM_SPAWN_POINT(<<145.0080, -2409.1050, 5.0010>>		,	294.7990)
					ADD_CUSTOM_SPAWN_POINT(<<142.5547, -2409.0447, 5.0005>>		,	318.3989)
					ADD_CUSTOM_SPAWN_POINT(<<181.2223, -2437.2581, 5.0290>>		,  	143.9971)
					ADD_CUSTOM_SPAWN_POINT(<<180.4789, -2439.9072, 5.0291>>		,	155.9969)
					ADD_CUSTOM_SPAWN_POINT(<<193.1970, -2506.2644, 4.9961>>		,	91.3965)
					ADD_CUSTOM_SPAWN_POINT(<<189.5415, -2505.9524, 4.9961>>		,  	91.3965)
					ADD_CUSTOM_SPAWN_POINT(<<88.4229, -2518.5344, 5.0006>>		,  	319.9957)
					ADD_CUSTOM_SPAWN_POINT(<<86.3593, -2516.6726, 5.0022>>		, 	324.7948)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL DOES_VARIATION_USE_CUSTOM_SPAWN_POINTS(INT iVar)
	SWITCH iVar
		CASE GB_BIKER_DEFEND_SHOOTOUT
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC WEAPON_TYPE GET_WEAPON_FOR_INITIAL_TARGET(INT iPed, INT iVar, FACTORY_ID factoryId)
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BIKER_CONTRA_DEFEND - [GET_WEAPON_FOR_INITIAL_TARGET] Called with Var ", iVar, " and factoryId ", ENUM_TO_INT(factoryId))
	
	SWITCH iVar
		CASE GB_BIKER_DEFEND_SHOOTOUT
			SWITCH factoryId
				CASE FACTORY_ID_WEED_1
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 1	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 2	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 3	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 4	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 5	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 6	RETURN WEAPONTYPE_ASSAULTRIFLE
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_2	
					SWITCH iPed
						CASE 0	RETURN  WEAPONTYPE_ASSAULTRIFLE
						CASE 1	RETURN  WEAPONTYPE_PUMPSHOTGUN
						CASE 2	RETURN  WEAPONTYPE_ASSAULTRIFLE
						CASE 3	RETURN  WEAPONTYPE_PUMPSHOTGUN
						CASE 4	RETURN  WEAPONTYPE_ASSAULTRIFLE
						CASE 5	RETURN  WEAPONTYPE_PUMPSHOTGUN
						CASE 6	RETURN  WEAPONTYPE_ASSAULTRIFLE
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_3	
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 1	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 2	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 3	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 4	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 5	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 6	RETURN WEAPONTYPE_ASSAULTRIFLE
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_WEED_4	
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_MICROSMG
						CASE 1	RETURN WEAPONTYPE_SMG
						CASE 2	RETURN WEAPONTYPE_MICROSMG
						CASE 3	RETURN WEAPONTYPE_SMG
						CASE 4	RETURN WEAPONTYPE_MICROSMG
						CASE 5	RETURN WEAPONTYPE_SMG
						CASE 6	RETURN WEAPONTYPE_MICROSMG
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_FAKE_ID
			SWITCH factoryId
				CASE FACTORY_ID_FAKE_ID_1	
					SWITCH iPed
						CASE 0 RETURN WEAPONTYPE_ASSAULTRIFLE
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryID
				CASE FACTORY_ID_METH_1 
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 1	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 2	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 3	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 4	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 5	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 6	RETURN WEAPONTYPE_ASSAULTRIFLE
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_2 
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 1	RETURN WEAPONTYPE_MICROSMG
						CASE 2	RETURN WEAPONTYPE_ASSAULTSHOTGUN
						CASE 3	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 4	RETURN WEAPONTYPE_MICROSMG
						CASE 5	RETURN WEAPONTYPE_ASSAULTSHOTGUN
						CASE 6	RETURN WEAPONTYPE_ASSAULTRIFLE
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_3
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 1	RETURN WEAPONTYPE_MICROSMG
						CASE 2	RETURN WEAPONTYPE_ASSAULTSHOTGUN
						CASE 3	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 4	RETURN WEAPONTYPE_MICROSMG
						CASE 5	RETURN WEAPONTYPE_ASSAULTSHOTGUN
						CASE 6	RETURN WEAPONTYPE_ASSAULTRIFLE
						
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_METH_4
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 1	RETURN WEAPONTYPE_MICROSMG
						CASE 2	RETURN WEAPONTYPE_ASSAULTSHOTGUN
						CASE 3	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 4	RETURN WEAPONTYPE_MICROSMG
						CASE 5	RETURN WEAPONTYPE_ASSAULTSHOTGUN
						CASE 6	RETURN WEAPONTYPE_ASSAULTRIFLE
						
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_UNARMED
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_PISTOL
						CASE 3	RETURN WEAPONTYPE_PISTOL
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_PISTOL
						CASE 6	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_2
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_UNARMED
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_PISTOL
						CASE 3	RETURN WEAPONTYPE_PISTOL
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_PISTOL
						CASE 6	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_CRACK_3
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_UNARMED
						CASE 1	RETURN WEAPONTYPE_PISTOL
						CASE 2	RETURN WEAPONTYPE_PISTOL
						CASE 3	RETURN WEAPONTYPE_PISTOL
						CASE 4	RETURN WEAPONTYPE_PISTOL
						CASE 5	RETURN WEAPONTYPE_PISTOL
						CASE 6	RETURN WEAPONTYPE_PISTOL
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_DLC_SPECIALCARBINE
						CASE 1	RETURN WEAPONTYPE_MICROSMG
						CASE 2	RETURN WEAPONTYPE_MICROSMG
						CASE 3	RETURN WEAPONTYPE_DLC_SPECIALCARBINE
						CASE 4	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 5	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 6	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 7	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 8	RETURN WEAPONTYPE_ASSAULTRIFLE
					ENDSWITCH
				BREAK
				
				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_DLC_SPECIALCARBINE
						CASE 1	RETURN WEAPONTYPE_MICROSMG
						CASE 2	RETURN WEAPONTYPE_MICROSMG
						CASE 3	RETURN WEAPONTYPE_DLC_SPECIALCARBINE
						CASE 4	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 5	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 6	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 7	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 8	RETURN WEAPONTYPE_ASSAULTRIFLE
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_SMG
						CASE 1	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 2	RETURN WEAPONTYPE_SMG
						CASE 3	RETURN WEAPONTYPE_SMG
						CASE 4	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 5	RETURN WEAPONTYPE_ASSAULTSHOTGUN
						CASE 6	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 7	RETURN WEAPONTYPE_ASSAULTSHOTGUN
						CASE 8	RETURN WEAPONTYPE_ASSAULTRIFLE
						CASE 9	RETURN WEAPONTYPE_ASSAULTSHOTGUN
					ENDSWITCH
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iPed
						CASE 0	RETURN WEAPONTYPE_DLC_HEAVYPISTOL
						CASE 1	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 2	RETURN WEAPONTYPE_DLC_HEAVYPISTOL
						CASE 3	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 4	RETURN WEAPONTYPE_CARBINERIFLE
						CASE 5	RETURN WEAPONTYPE_MICROSMG
						CASE 6	RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 7	RETURN WEAPONTYPE_MICROSMG
						CASE 8	RETURN WEAPONTYPE_MICROSMG
						CASE 9	RETURN WEAPONTYPE_PUMPSHOTGUN
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					SWITCH iPed
						CASE 0			RETURN WEAPONTYPE_PUMPSHOTGUN		
						CASE 1			RETURN WEAPONTYPE_PISTOL
						CASE 2			RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 3			RETURN WEAPONTYPE_PISTOL
						CASE 4			RETURN WEAPONTYPE_UNARMED
						CASE 5			RETURN WEAPONTYPE_CARBINERIFLE
						CASE 6			RETURN WEAPONTYPE_CARBINERIFLE
						CASE 7			RETURN WEAPONTYPE_PUMPSHOTGUN
						CASE 8			RETURN WEAPONTYPE_PISTOL
						CASE 9			RETURN WEAPONTYPE_PISTOL
						CASE 10			RETURN WEAPONTYPE_SMG
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN WEAPONTYPE_MICROSMG	
ENDFUNC

FUNC TEXT_LABEL_15 GET_NAME_FOR_DEFEND_PICKUP_BLIP(INT iVar)
	TEXT_LABEL_15 tl15Name
	
	SWITCH iVar
		CASE GB_BIKER_DEFEND_FAKE_ID
			tl15Name = "DBR_BLPPRO" // Product
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			tl15Name = "DBR_BLPTST" // Testimony
		BREAK
		
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			tl15Name = "DBR_BLPCSH" // Cash
		BREAK
	ENDSWITCH
	
	RETURN tl15Name
ENDFUNC

FUNC TEXT_LABEL_15 GET_NAME_FOR_DEFEND_PACKAGE_HUD_TITLE(INT iVar)
	TEXT_LABEL_15 tl15Name
	
	SWITCH iVar
		CASE GB_BIKER_DEFEND_FAKE_ID
			tl15Name = "DBR_HUDPRO" // Product
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			tl15Name = "DBR_HUDTST" // Testimony
		BREAK
		
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			tl15Name = "DBR_HUDCSH" // Cash
		BREAK
	ENDSWITCH
	
	RETURN tl15Name
ENDFUNC

FUNC TEXT_LABEL_15 GET_NAME_FOR_ACTION_AREA_BLIP(INT iVar, FACTORY_ID factoryID)
	TEXT_LABEL_15 tl15Obj
	
	SWITCH iVar
		CASE GB_BIKER_DEFEND_SHOOTOUT
			tl15Obj = "DBR_BLBUS" // Business
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			tl15Obj = "DBR_BLBUS" // Business
		BREAK
		
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					tl15Obj = "DBR_BLMFS" // Millar's Fishery
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_2
					tl15Obj = "DBR_BLIPLA" // Land Act Dam
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					tl15Obj = "DBR_BLIPGSD" // Grand Senora Desert
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					tl15Obj = "DBR_BLIPLSE" // Little Seoul
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1	
					tl15Obj = "DBR_BLPSS"		// Sandy Shores
				BREAK 
				
				CASE FACTORY_ID_METH_2	
					tl15Obj = "DBR_BLIPDV"		// Downtown Vinewood
				BREAK
				
				CASE FACTORY_ID_METH_3	
					tl15Obj = "DBR_BLIPGR"		// Grapeseed
				BREAK
				
				CASE FACTORY_ID_METH_4	
					tl15Obj = "DBR_BLIPLP"		// La Puerta
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_FAKE_ID
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					tl15Obj = "DBR_BLIPBV"			//Bayview Lodge.
				BREAK
				
				CASE FACTORY_ID_CRACK_2
					tl15Obj = "DBR_BLIPPC"			//Pink Cage Motel.
				BREAK
				
				CASE FACTORY_ID_CRACK_3
					tl15Obj = "DBR_BLIPMM"			//Motor Motel
				BREAK
				CASE FACTORY_ID_CRACK_4
					tl15Obj = "DBR_BLIPBM"			//Bilingsgate motel
				BREAK
			ENDSWITCH
		BREAK
		
		
	ENDSWITCH
	
	RETURN tl15Obj
ENDFUNC

FUNC TEXT_LABEL_15 GET_GB_BIKER_DEFEND_VEHICLE_BLIP_NAME(INT iVar, FACTORY_ID factoryId)
	TEXT_LABEL_15 tl15Veh
	
	SWITCH iVar
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1	
					tl15Veh = "DBR_BLPVAN"		// van
				BREAK
				
				CASE FACTORY_ID_METH_2	
					tl15Veh = "DBR_BLPVAN"		// Van
				BREAK
				
				CASE FACTORY_ID_METH_3	
					tl15Veh = "DBR_BLPTRK"		// truck
				BREAK
				
				CASE FACTORY_ID_METH_4	
					tl15Veh = "DBR_BLPCAR"		// car
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			tl15Veh = "DBR_BLPVAN"
		BREAK
	ENDSWITCH
	RETURN tl15Veh
ENDFUNC

FUNC TEXT_LABEL_15 GET_GB_BIKER_DEFEND_OBJECTIVE_TEXT_FOR_APPROACH_ACTION(INT iVar, FACTORY_ID factoryId)
	TEXT_LABEL_15 tl15Obj
	
	SWITCH iVar
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1	
					tl15Obj = "DBR_GOTOL"		// Go to the ~y~Lost Hideout.
				BREAK 
				
				CASE FACTORY_ID_METH_2	
					tl15Obj = "DBR_GOTOA"		// Go to the ~y~Armenian Hideout.
				BREAK
				
				CASE FACTORY_ID_METH_3	
					tl15Obj = "DBR_GOTOR"		// Go to the ~y~Redneck's Hideout.
				BREAK
				
				CASE FACTORY_ID_METH_4	
					tl15Obj = "DBR_GOTOP"		// Go to the ~y~Redneck's Hideout.
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					tl15Obj = "DBR_GOTOBV"			//Go to ~y~Bayview Lodge.
				BREAK
				
				CASE FACTORY_ID_CRACK_2
					tl15Obj = "DBR_GOTOPC"			//Go to ~y~Bayview Lodge.
				BREAK
				
				CASE FACTORY_ID_CRACK_3
					tl15Obj = "DBR_GOTOMM"			//Go to ~y~Bayview Lodge.
				BREAK
				CASE FACTORY_ID_CRACK_4
					tl15Obj = "DBR_GOTOBL"			//Go to ~y~Bayview Lodge.
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					tl15Obj = "DBR_GOTOMFS"			//Go to Millar's Fishery
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_2
					tl15Obj = "DBR_GOTOLA"			//Go to the ~y~Land Act Dam
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_3
					tl15Obj = "DBR_GOTOGSD"			//Go to grand senora desert
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					tl15Obj = "DBR_GOTOLSE"			//Go to Millar's Fishery
				BREAK
				
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					tl15Obj = "DBR_GOTO1"	 // go to the business
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
	
ENDFUNC

FUNC TEXT_LABEL_15 GET_GB_BIKER_DEFEND_VEHICLE_TEXT_FOR_STOLEN_GOODS(INT iVar, FACTORY_ID factoryId)
	TEXT_LABEL_15 tl15Obj
	
	SWITCH iVar
		CASE GB_BIKER_DEFEND_METH
			SWITCH factoryId
				CASE FACTORY_ID_METH_1	
					tl15Obj = "DBR_OBJVAN"		// van
				BREAK
				
				CASE FACTORY_ID_METH_2	
					tl15Obj = "DBR_OBJVAN"		// Van
				BREAK
				
				CASE FACTORY_ID_METH_3	
					tl15Obj = "DBR_OBJTRK"		// truck
				BREAK
				
				CASE FACTORY_ID_METH_4	
					tl15Obj = "DBR_OBJCAR"		// car
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
	
ENDFUNC

FUNC TEXT_LABEL_15 GET_GB_BIKER_DEFEND_OPENING_TEXT_MSG(INT iVar, FACTORY_ID factoryId)
	TEXT_LABEL_15 tl15Obj
	
	SWITCH iVar
//		CASE GB_BIKER_DEFEND_METH
//			SWITCH factoryId
//				CASE FACTORY_ID_METH_1	
//					tl15Obj = "DBR_TXTMTH1"		
//				BREAK
//				
//				CASE FACTORY_ID_METH_2	
//					tl15Obj = "DBR_TXTMTH2"		
//				BREAK
//				
//				
//				CASE FACTORY_ID_METH_3	
//					tl15Obj = "DBR_TXTMTH3"		
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					//tl15Obj = "DBR_TXTCK1"			
				BREAK
				
//				CASE FACTORY_ID_CRACK_2
//					tl15Obj = "DBR_TXTCK4" 
//				BREAK
//				
//				CASE FACTORY_ID_CRACK_3
//					tl15Obj = "DBR_TXTCK3"
//				BREAK
//				
//				CASE FACTORY_ID_CRACK_4
//					tl15Obj = "DBR_TXTCK2"
//				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
	
ENDFUNC

FUNC TEXT_LABEL_15 GET_GB_BIKER_DEFEND_WARN_LOSE_BUSINESS_HELP(INT iVar, FACTORY_ID factoryId)
	TEXT_LABEL_15 tl15Obj
	
	SWITCH iVar
		CASE GB_BIKER_DEFEND_GLOBAL
			SWITCH factoryID
				CASE FACTORY_ID_METH_1
				CASE FACTORY_ID_METH_2
				CASE FACTORY_ID_METH_3
				CASE FACTORY_ID_METH_4
					tl15Obj = "DBR_HLP_MTH2"
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_1
				CASE FACTORY_ID_FAKE_CASH_2
				CASE FACTORY_ID_FAKE_CASH_3
				CASE FACTORY_ID_FAKE_CASH_4
					tl15Obj = "DBR_HLP_CSH2"
				BREAK
				
				CASE FACTORY_ID_WEED_1
				CASE FACTORY_ID_WEED_2
				CASE FACTORY_ID_WEED_3
				CASE FACTORY_ID_WEED_4
					tl15Obj = "DBR_HLP_WFM2"
				BREAK
				
				CASE FACTORY_ID_FAKE_ID_1
				CASE FACTORY_ID_FAKE_ID_2
				CASE FACTORY_ID_FAKE_ID_3
				CASE FACTORY_ID_FAKE_ID_4
					tl15Obj = "DBR_HLP_FGY2"
				BREAK
				
				CASE FACTORY_ID_CRACK_1
				CASE FACTORY_ID_CRACK_2
				CASE FACTORY_ID_CRACK_3
				CASE FACTORY_ID_CRACK_4
					tl15Obj = "DBR_HLP_CNE2"	
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tl15Obj
ENDFUNC


FUNC INT GET_GB_BIKER_DEFEND_NUMBER_OF_SCENARIO_BLOCKING_AREAS(INT iVar, FACTORY_ID factoryId)
	SWITCH iVar
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					RETURN 1
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					RETURN 1
				CASE FACTORY_ID_FAKE_CASH_4
					RETURN 1
				CASE FACTORY_ID_FAKE_CASH_3
					RETURN 1
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC


PROC GET_GB_DEFEND_SCENARIO_BLOCKING_AREA_DETAILS(INT iVar, FACTORY_ID factoryID, INT iScenBlock, VECTOR &vMin, VECTOR &vMax)
	SWITCH iVar
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					SWITCH iScenBlock
						CASE 0
							vMin = <<-692.961426,5778.409668,18.455952>> - <<24.875000,38.062500,2.562500>>
							vMin.z -= 2.0
							
							vMax = <<-692.961426,5778.409668,18.455952>> + <<24.875000,38.062500,2.562500>>
							vMin.z += 2.0
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					SWITCH iScenBlock
						CASE 0
							vMin =  <<1311.883789,4278.740723,40.413673>> - <<87.937500,87.375000,10.500000>>
							vMin.z -= 2.0
							
							vMax =  <<1311.883789,4278.740723,40.413673>> + <<87.937500,87.375000,10.500000>>
							vMin.z += 2.0
						BREAK
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_FAKE_CASH_4
					SWITCH iScenBlock
						CASE 0
							vMin =  <<-467.920227,-981.108948,27.170263>> - <<43.250000,30.125000,6.937500>>
							vMin.z -= 2.0
							
							vMax =  <<-467.920227,-981.108948,27.170263>> + <<43.250000,30.125000,6.937500>>
							vMin.z += 2.0
						BREAK
					ENDSWITCH
				BREAK

				CASE FACTORY_ID_FAKE_CASH_3
					SWITCH iScenBlock
						CASE 0
							vMin =  <<2351.924561,3133.575439,47.583725>> - <<22.500000,26.437500,1.000000>>
							vMin.z -= 5.0
							
							vMax =  <<2351.924561,3133.575439,47.583725>> + <<22.500000,26.437500,1.000000>>
							vMin.z += 5.0
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GB_DEFEND_ANGLED_AREA_FOR_SPOOKED_CHECK(INT iVar, FACTORY_ID factoryID, INT iAngled, VECTOR &vMin, VECTOR &vMax, FLOAT &fDefendWidth)
	SWITCH iVar
		CASE GB_BIKER_DEFEND_COKE
			SWITCH factoryID
				CASE FACTORY_ID_CRACK_1
					vMin = <<-683.949829,5765.671387,15.761013>>
					vMax = <<-703.894165,5775.584473,20.330959>> 
					fDefendWidth = 21.625000
				BREAK

				CASE FACTORY_ID_CRACK_2
					vMin 			= 	<<320.778961,-227.479355,52.086807>>		//	<<320.407898,-223.768433,51.999619>>
					vMax 			= 	<<334.153687,-191.494690,70.101456>>		//	<<329.416321,-202.267242,61.586292>> 
					fDefendWidth 	= 	31.875000									//	34.687500
				BREAK
				
				CASE FACTORY_ID_CRACK_3
					vMin 			= <<1140.214844,2654.576172,36.246906>>
					vMax 			= <<1105.762329,2652.743896,41.145905>> 
					fDefendWidth 	= 21.625000
				BREAK
				
				CASE FACTORY_ID_CRACK_4
					vMin 			= <<556.562134,-1776.717651,27.389490>>
					vMax 			= <<566.113037,-1749.880737,32.286449>> 
					fDefendWidth 	= 21.625000
				BREAK
				
			ENDSWITCH
		BREAK
	
		CASE GB_BIKER_DEFEND_COUNTERFEIT
			SWITCH factoryID
				CASE FACTORY_ID_FAKE_CASH_1
					vMin 			= <<1343.890259,4228.709473,27.134155>>
					vMax 			= <<1300.388794,4237.671387,36.726173>> 
					fDefendWidth 	= 38.312500
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_2
					SWITCH iAngled
						CASE 0
							vMin 			= <<1666.841309,-10.196686,165.580765>>
							vMax 			= <<1656.955933,5.569890,170.118103>>  
							fDefendWidth 	= 4.062500
						BREAK
						
						CASE 1
							vMin 			= <<1662.324585,7.686837,165.067719>>
							vMax 			= <<1660.893188,21.303038,170.039124>>  
							fDefendWidth 	= 4.062500
						BREAK
					ENDSWITCH
				BREAK
				
				
				CASE FACTORY_ID_FAKE_CASH_3
					vMin 			= <<2333.489990,3137.608154,46.555958>>
					vMax 			= <<2371.310303,3131.123047,53.904095>> 
					fDefendWidth 	= 22.437500	
				BREAK
				
				CASE FACTORY_ID_FAKE_CASH_4
					vMin 			= <<-435.027161,-985.867615,29.842325>>
					vMax 			= <<-465.276855,-984.259338,21.982761>> 
					fDefendWidth 	= 17.375000		
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
		
ENDPROC

FUNC INT GET_GB_DEFEND_POLICE_RAID_RIOT_VAN_VEHICLE_INT(FACTORY_ID factoryID)
	SWITCH factoryID
		CASE FACTORY_ID_METH_1
		CASE FACTORY_ID_METH_2
		CASE FACTORY_ID_METH_3
		CASE FACTORY_ID_METH_4
		CASE FACTORY_ID_FAKE_CASH_1
		CASE FACTORY_ID_FAKE_CASH_2
		CASE FACTORY_ID_FAKE_CASH_3
		CASE FACTORY_ID_FAKE_CASH_4
		CASE FACTORY_ID_WEED_1
		CASE FACTORY_ID_WEED_2
		CASE FACTORY_ID_WEED_3
		CASE FACTORY_ID_WEED_4
		CASE FACTORY_ID_FAKE_ID_1
		CASE FACTORY_ID_FAKE_ID_2
		CASE FACTORY_ID_FAKE_ID_3
		CASE FACTORY_ID_FAKE_ID_4
		CASE FACTORY_ID_CRACK_1
		CASE FACTORY_ID_CRACK_2
		CASE FACTORY_ID_CRACK_3
		CASE FACTORY_ID_CRACK_4
			RETURN 3
	ENDSWITCH
	
	RETURN -1
ENDFUNC


FUNC INT GET_GB_DEFEND_POLICE_RAID_RIOT_VAN_DRIVER_INT(FACTORY_ID factoryID)
	SWITCH factoryID
		CASE FACTORY_ID_METH_1
		CASE FACTORY_ID_METH_2
		CASE FACTORY_ID_METH_3
		CASE FACTORY_ID_METH_4
		CASE FACTORY_ID_FAKE_CASH_1
		CASE FACTORY_ID_FAKE_CASH_2
		CASE FACTORY_ID_FAKE_CASH_3
		CASE FACTORY_ID_FAKE_CASH_4
		CASE FACTORY_ID_WEED_1
		CASE FACTORY_ID_WEED_2
		CASE FACTORY_ID_WEED_3
		CASE FACTORY_ID_WEED_4
		CASE FACTORY_ID_FAKE_ID_1
		CASE FACTORY_ID_FAKE_ID_2
		CASE FACTORY_ID_FAKE_ID_3
		CASE FACTORY_ID_FAKE_ID_4
		CASE FACTORY_ID_CRACK_1
		CASE FACTORY_ID_CRACK_2
		CASE FACTORY_ID_CRACK_3
		CASE FACTORY_ID_CRACK_4
			RETURN 10
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC VECTOR GET_GB_DEFEND_POLICE_RAID_RIOT_VAN_CREATE_COORDS(FACTORY_ID factoryID)
	SWITCH factoryID
		CASE FACTORY_ID_METH_1					RETURN <<-482.0600, 5877.7842, 32.4518>>
		CASE FACTORY_ID_METH_2					RETURN <<1238.4711, -1159.3434, 36.4193>>
		CASE FACTORY_ID_METH_3					RETURN <<887.3482, 2219.7493, 47.5606>>
		CASE FACTORY_ID_METH_4					RETURN <<752.7949, -2500.0999, 18.9858>>
		CASE FACTORY_ID_FAKE_CASH_1				RETURN <<-700.1161, 5545.2686, 36.7214>>
		CASE FACTORY_ID_FAKE_CASH_2				RETURN <<-786.5857, -1069.8104, 10.3975>>
		CASE FACTORY_ID_FAKE_CASH_3				RETURN <<895.2108, 2216.5920, 47.5661>>
		CASE FACTORY_ID_FAKE_CASH_4				RETURN <<764.4866, -2300.5156, 26.8079>>
		CASE FACTORY_ID_WEED_1					RETURN <<1605.3066, 6387.9312, 26.0916>>
		CASE FACTORY_ID_WEED_2					RETURN <<113.2487, -56.8197, 66.3282>>
		CASE FACTORY_ID_WEED_3					RETURN <<2877.6833, 3683.4011, 51.6637>>
		CASE FACTORY_ID_WEED_4					RETURN <<434.7159, -2126.3533, 18.6572>>
		CASE FACTORY_ID_FAKE_ID_1				RETURN <<-686.4249, 5554.5776, 37.1782>>
		CASE FACTORY_ID_FAKE_ID_2				RETURN <<74.3208, -982.2161, 28.4045>>
		CASE FACTORY_ID_FAKE_ID_3				RETURN <<2174.5037, 5217.9658, 59.6483>>
		CASE FACTORY_ID_FAKE_ID_4				RETURN <<241.5291, -2547.3425, 4.8500>>
		CASE FACTORY_ID_CRACK_1					RETURN <<1011.7293, 6481.5342, 19.9807>>
		CASE FACTORY_ID_CRACK_2					RETURN <<-1066.8434, -382.9635, 36.4517>>
		CASE FACTORY_ID_CRACK_3					RETURN <<215.9028, 3075.4343, 41.2866>>
		CASE FACTORY_ID_CRACK_4					RETURN <<343.1183, -2450.1123, 6.3881>>


	ENDSWITCH
	
	RETURN << 0.0, 0.0, 0.0 >>
ENDFUNC

FUNC MODEL_NAMES GET_FRIENDLY_PED_MODEL_FOR_GLOBAL_VARIATION(FACTORY_ID factoryID, INT iPed)
	SWITCH factoryID
		CASE FACTORY_ID_METH_1
		CASE FACTORY_ID_METH_2
		CASE FACTORY_ID_METH_3
		CASE FACTORY_ID_METH_4
			SWITCH iped
				CASE 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
				CASE 1
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_meth_01"))
			ENDSWITCH
		BREAK
		
		
		CASE FACTORY_ID_FAKE_ID_1
		CASE FACTORY_ID_FAKE_ID_2
		CASE FACTORY_ID_FAKE_ID_3
		CASE FACTORY_ID_FAKE_ID_4
			SWITCH iped
				CASE 0
					RETURN  INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_forgery_01"))
				CASE 1
					RETURN  INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_forgery_01"))
			ENDSWITCH
		BREAK
		
		CASE FACTORY_ID_WEED_1
		CASE FACTORY_ID_WEED_2
		CASE FACTORY_ID_WEED_3
		CASE FACTORY_ID_WEED_4
			SWITCH iped
				CASE 0
					RETURN  INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
				CASE 1
					RETURN  INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_weed_01"))
			ENDSWITCH
		BREAK
		
		CASE FACTORY_ID_CRACK_1
		CASE FACTORY_ID_CRACK_2
		CASE FACTORY_ID_CRACK_3
		CASE FACTORY_ID_CRACK_4
			SWITCH iped
				CASE 0
					RETURN  INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
				CASE 1
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_meth_01"))
			ENDSWITCH
		BREAK
		
		CASE FACTORY_ID_FAKE_CASH_1
		CASE FACTORY_ID_FAKE_CASH_2
		CASE FACTORY_ID_FAKE_CASH_3
		CASE FACTORY_ID_FAKE_CASH_4
			SWITCH iped
				CASE 0 
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
				CASE 1
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_counterfeit_01"))
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC


PROC GET_DROP_OFF_GOTO_AND_HEADING_FOR_FRIENDLY_PED(FACTORY_ID factoryID, INT iPed, VECTOR &vGoto, FLOAT &fHead)
	SWITCH iPed
		CASE 0
			SWITCH factoryID
				CASE FACTORY_ID_METH_1					vGoto =  <<44.0637, 6303.1880, 30.2205>> 		fHead =  25.7565			BREAK
				CASE FACTORY_ID_METH_2					vGoto =  <<1452.2788, -1681.9351, 64.9426>>		fHead = 189.0157			BREAK
				CASE FACTORY_ID_METH_3					vGoto =  <<191.6493, 2458.4006, 54.6935>>		fHead =  51.3878			BREAK
				CASE FACTORY_ID_METH_4					vGoto =  <<1190.2146, -3109.6328, 4.5280>>		fHead = 189.015			BREAK
				CASE FACTORY_ID_FAKE_CASH_1				vGoto =   <<-438.0900, 6148.1016, 30.4782>>		fHead = 315.9032			BREAK
				CASE FACTORY_ID_FAKE_CASH_2				vGoto =  <<-1168.9359, -1388.4149, 3.9377>>		fHead = 312.3426			BREAK
				CASE FACTORY_ID_FAKE_CASH_3				vGoto =  <<642.2122, 2790.8735, 40.9772>>		fHead = 91.497			BREAK
				CASE FACTORY_ID_FAKE_CASH_4				vGoto =  <<671.9888, -2671.5615, 5.3063>>		fHead = 267.4432		BREAK
				CASE FACTORY_ID_WEED_1					vGoto =  <<407.2768, 6498.2593, 26.7549>>		fHead = 355.0736		BREAK
				CASE FACTORY_ID_WEED_2					vGoto =  <<114.7995, 168.2941, 103.8011>>		fHead = 354.114			BREAK
				CASE FACTORY_ID_WEED_3					vGoto =  <<2873.4290, 4457.3242, 47.5266>>		fHead = 116.297			BREAK
				CASE FACTORY_ID_WEED_4					vGoto =  <<134.3603, -2476.4712, 5.0000>>		fHead = 50.0421			BREAK
				CASE FACTORY_ID_FAKE_ID_1				vGoto =  <<-167.4548, 6312.0137, 30.4771>>		fHead = 328.3768			BREAK
				CASE FACTORY_ID_FAKE_ID_2				vGoto =  <<298.5593, -759.6779, 28.3927>>		fHead = 64.8253			BREAK
				CASE FACTORY_ID_FAKE_ID_3				vGoto =  <<1646.7516, 4840.2339, 41.0293>>		fHead = 278.8109			BREAK
				CASE FACTORY_ID_FAKE_ID_4				vGoto =   <<-330.4659, -2779.4170, 4.3323>>		fHead = 271.7462			BREAK
				CASE FACTORY_ID_CRACK_1					vGoto =  <<2.5463, 6447.0020, 30.4253>>			fHead = 316.765			BREAK
				CASE FACTORY_ID_CRACK_2					vGoto =  <<-1462.8885, -381.7709, 37.8462>>		fHead = 36.108			BREAK
				CASE FACTORY_ID_CRACK_3					vGoto =  <<379.5887, 3584.2742, 32.2922>>		fHead = 233.852			BREAK
				CASE FACTORY_ID_CRACK_4					vGoto =  <<-252.8833, -2585.8779, 5.0006>>		fHead = 281.4969			BREAK
			ENDSWITCH
		BREAK
		
		CASE 1
			SWITCH factoryID
				CASE FACTORY_ID_METH_1					vGoto =  <<45.4917, 6304.2393, 30.2170>> 		fHead = 39.0334				BREAK
				CASE FACTORY_ID_METH_2					vGoto = <<1455.2026, -1680.7335, 65.0509>>		fHead = 173.274				BREAK
				CASE FACTORY_ID_METH_3					vGoto =  <<191.6493, 2458.4006, 54.6935>>		fHead = 40.2315			BREAK
				CASE FACTORY_ID_METH_4					vGoto =  <<1188.5238, -3109.7185, 4.5280>>		fHead = 39.0334			BREAK
				CASE FACTORY_ID_FAKE_CASH_1				vGoto = <<-437.1454, 6147.1880, 30.4782>>		fHead = 310.5616			BREAK
				CASE FACTORY_ID_FAKE_CASH_2				vGoto = <<-1168.2230, -1388.9894, 3.9439>>		fHead = 307.7668			BREAK
				CASE FACTORY_ID_FAKE_CASH_3				vGoto = <<642.1058, 2792.4641, 40.9803>>		fHead = 104.8706			BREAK
				CASE FACTORY_ID_FAKE_CASH_4				vGoto = <<671.9273, -2672.9973, 5.3010>>		fHead = 269.4843			BREAK
				CASE FACTORY_ID_WEED_1					vGoto =  <<409.1617, 6498.2188, 26.7750>>		fHead = 2.5185			BREAK
				CASE FACTORY_ID_WEED_2					vGoto =  <<116.9202, 167.7725, 103.8181>>		fHead = 331.524			BREAK
				CASE FACTORY_ID_WEED_3					vGoto =  <<2873.0959, 4458.5581, 47.5190>>		fHead = 86.0931			BREAK
				CASE FACTORY_ID_WEED_4					vGoto =  <<134.7300, -2474.8987, 5.0000>>		fHead = 58.3085			BREAK
				CASE FACTORY_ID_FAKE_ID_1				vGoto =  <<-167.2002, 6310.9004, 30.4081>>		fHead = 342.3936			BREAK
				CASE FACTORY_ID_FAKE_ID_2				vGoto =  <<298.8705, -758.5872, 28.3927>>		fHead = 271.7462			BREAK
				CASE FACTORY_ID_FAKE_ID_3				vGoto =  <<1647.2589, 4839.2573, 41.0293>>		fHead = 273.2348			BREAK
				CASE FACTORY_ID_FAKE_ID_4				vGoto =  <<-330.6907, -2778.4841, 4.1449>>		fHead = 249.4719			BREAK
				CASE FACTORY_ID_CRACK_1					vGoto =  <<3.7215, 6446.1724, 30.4253>>			fHead = 312.4161			BREAK
				CASE FACTORY_ID_CRACK_2					vGoto =  <<-1461.6837, -382.2304, 37.7777>>		fHead = 43.5136			BREAK
				CASE FACTORY_ID_CRACK_3					vGoto =  <<379.2895, 3583.0747, 32.2922>>		fHead = 271.6733			BREAK
				CASE FACTORY_ID_CRACK_4					vGoto =  <<-252.6560, -2587.1372, 5.0006>>		fHead = 263.584			BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

ENDPROC







PROC GET_INITIAL_GOTO_FOR_GLOBAL_DEFEND_VEHICLE(FACTORY_ID factory, VECTOR &vGoto)
	SWITCH factory
		CASE FACTORY_ID_METH_1					vGoto =  <<-791.6460, 5488.8853, 33.2443>>			BREAK
		CASE FACTORY_ID_METH_2					vGoto = <<1191.6097, -670.0583, 60.3647>>			BREAK
		CASE FACTORY_ID_METH_3					vGoto = <<1161.6700, 1806.8818, 73.2495>>			BREAK
		CASE FACTORY_ID_METH_4					vGoto = <<797.8190, -2002.7351, 28.2616>>			BREAK
		
		CASE FACTORY_ID_FAKE_CASH_1				vGoto = <<-1133.6599, 5302.8638, 50.4578>>		BREAK
		CASE FACTORY_ID_FAKE_CASH_2				vGoto = <<-1113.7341, -706.5634, 19.6723>>		BREAK
		CASE FACTORY_ID_FAKE_CASH_3				vGoto = <<1164.5784, 1803.8578, 73.5321>>		BREAK
		CASE FACTORY_ID_FAKE_CASH_4				vGoto = <<830.1542, -1806.2761, 28.1088>>		BREAK
		
		CASE FACTORY_ID_WEED_1					vGoto = <<2015.5868, 6110.7803, 46.5750>>			BREAK
		CASE FACTORY_ID_WEED_2					vGoto = <<-61.1848, -521.4447, 39.4230>>			BREAK
		CASE FACTORY_ID_WEED_3					vGoto = <<2689.6997, 3223.6042, 53.0982>>			BREAK
		CASE FACTORY_ID_WEED_4					vGoto = <<915.5909, -2083.8674, 29.5698>>			BREAK
		
		
		CASE FACTORY_ID_FAKE_ID_1				vGoto = <<-1117.8311, 5315.1973, 49.1137>>		BREAK
		CASE FACTORY_ID_FAKE_ID_2				vGoto = <<-396.2242, -838.6695, 30.6225>>		BREAK
		CASE FACTORY_ID_FAKE_ID_3				vGoto = <<2521.5818, 5506.9194, 43.6777>>		BREAK
		CASE FACTORY_ID_FAKE_ID_4				vGoto = <<716.3234, -2490.2175, 19.2447>>		BREAK
		
		CASE FACTORY_ID_CRACK_1					vGoto = <<1505.3260, 6429.1660, 21.7667>>		BREAK
		CASE FACTORY_ID_CRACK_2					vGoto = <<-573.8163, -377.1201, 33.9229>>		BREAK
		CASE FACTORY_ID_CRACK_3					vGoto = <<300.4578, 2588.8625, 43.3964>>		BREAK
		CASE FACTORY_ID_CRACK_4					vGoto = <<632.5452, -2057.8911, 28.3333>>		BREAK
	ENDSWITCH
ENDPROC

