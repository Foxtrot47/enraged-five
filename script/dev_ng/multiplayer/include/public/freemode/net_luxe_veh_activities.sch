USING "vehicle_public.sch"
USING "context_control_public.sch"
USING "website_public.sch"
USING "freemode_header.sch"
USING "net_script_timers.sch"
USING "net_interactions_public.sch"
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      CREATED         :   Philip O'D
//      DESCRIPTION     :   
//								
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      CONSTS
// ===========================================================================================================

ENUM LUXE_ACT_CLIENT_REQUEST_CHAMP_STATE
CR_CHAMP_IDLE = 0,
CR_CHAMP_BUSY,
CR_REQUESTING,
CR_PLAY_CHAMP
ENDENUM

ENUM SERVER_CHAMP_CHECK_STATE
SR_CHECK_FOR_REQUESTS = 0,
SR_CHECK_IF_CLIENT_FINISHED
ENDENUM

ENUM LUXE_ACT_SCRIPT_STATE
CL_LUXE_ACT_CHECK_IN_VEH = 0,
CL_LUXE_ACT_LAUNCH_SCRIPT,
CL_LUXE_ACT_SCRIPT_LAUNCHED
ENDENUM

ENUM LUXE_ACT_STATE
CL_LUXE_ACT_INIT = 0,
CL_LUXE_ACT_IN_VEH,
CL_LUXE_ACT_IN_VEH_IN_FRONT, // AS in not passenger
CL_LUXE_ACT_IN_VEH_PLAY_CHAMP,
CL_LUXE_ACT_IN_VEH_PROMPT_ACT,
CL_LUXE_ACT_GET_SERVER_CHAMP_RESPONSE,
CL_LUXE_ACT_PICK_UP_BOTTLE,
CL_LUXE_ACT_PICK_UP_FLUTE,
CL_LUXE_ACT_RELEASE_CHAMP, 
CL_LUXE_ACT_POLL_ANIM_FOR_TAGS,
CL_LUXE_ACT_POLL_ANIM_FOR_TAGS_CIGAR,
CL_LUXE_ACT_PICK_UP_CIGAR, 
CL_LUXE_CREATE_CIGAR_PROPS,
CL_LUXE_ACT_IN_VEH_PLAY_CIGAR_SYNC,
CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC,
CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC,
CL_LUXE_ACT_RELEASE_CIGAR,
CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC,
CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC, 
CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC,
CL_LUXE_ACT_IN_VEH_PLAY_CIGAR,
CL_LUXE_ACT_LAUNCH_INTERNET,
CL_LUXE_ACT_BROWSE_INTERNET,
CL_LUXE_ACT_CHANGING_SEAT, 
CL_LUXE_ACT_CLEANING_UP,
CL_LUXE_ACT_CLEANED_UP
ENDENUM

ENUM LUXE_SERVER_ACT_SCRIPT_STATE
SR_LUXE_INIT = 0,
SR_LUXE_LOAD_PROPS,
SR_LUXE_CREATE_PROPS,
SR_LUXE_RUNNING,
SR_LUXE_CLEANING_UP,
SR_LUXE_CLEANED_UP
ENDENUM

ENUM LUXE_ACT_GEN_STATE
CHECK_PLAYER_IS_IN_LUXE_ACTIVITY_VEH = 0,
CHECK_FOR_VEH_WITH_INSTANCE_IDS,
REQUEST_INSTANCE_ID,
LAUNCH_LUX_VEH_SCRIPT,
VERIFY_LAUNCH_LUX_VEH_SCRIPT,
CHECK_FOR_LUX_VEH_RESET,
RUN_VEH_ACTIVITY
ENDENUM


BOOL bCigarExhale
BOOL bLighterStart
BOOL bSparkStart
BOOL bSecondSpark 
BOOL bWeaponSet

INT iNumberOfSeatsToClean

WEAPON_TYPE storedWeapon

SCRIPT_TIMER appInternetCloseDelay

// TODO: We may want to make this a tunable if used for critical timeouts
CONST_INT ciLUXE_ACT_ANIM_SAFETY_TIMEOUT		30000

CONST_INT ciLUXE_INSTANCE_RECYCLE_ATTEMPTS		3

// ===========================================================================================================
//     Structs
//		
// ===========================================================================================================

STRUCT SEAT_DATA
//	STRING sSeat
	TEXT_LABEL_15 sSeat
	

	
	NETWORK_INDEX propChampFluteAnim
	ENTITY_INDEX propChampFluteIdle
	
	NETWORK_INDEX propChampBotAnim
	ENTITY_INDEX propChampBotIdle
	
	NETWORK_INDEX propCigarAnim
	ENTITY_INDEX  propCigarIdle
	
	ENTITY_INDEX  propAshtray
	NETWORK_INDEX propLiquid
	NETWORK_INDEX propLighter
	
	VECTOR vFlutePropPos
	VECTOR vChampBotPropPos
	VECTOR vCigarPropPos
	VECTOR vAshTrayPropPos
	
	VECTOR vFlutePropRot
	VECTOR vChampBotPropRot
	VECTOR vCigarPropRot
	VECTOR vAshTrayPropRot
	
	INT vehicleSeatID
	BOOL bObjectsReservedForSeat
	
	PLAYER_INDEX piSeatedPlayer
ENDSTRUCT

STRUCT TABLE_DATA
INT iTable
PLAYER_INDEX champIDPlayerChosen
BOOL bPropsNeedToBeCreated
BOOL bPropsNeedToBeDestroyed
ENDSTRUCT

STRUCT SERVER_BROADCAST_DATA
	LUXE_SERVER_ACT_SCRIPT_STATE luxeServerActScriptState
	SERVER_CHAMP_CHECK_STATE serverChampCheckStage

	NETWORK_INDEX niVehIndex

	SEAT_DATA activitySeats[6]

//	BOOL bCheckPlayerFinished
//	BOOL bChampClientPicked
	
	#IF IS_DEBUG_BUILD
	BOOL bDebugNetworkVisibiltyOfSwiftNetworkProps = TRUE
	INT iDebugClipVariation
	BOOL bMakeSwift2NetworkPropsOnlyExistForParticipants = FALSE
	BOOL bMakeLuxor2NetworkPropsOnlyExistForParticipants = TRUE
	#ENDIF
//	INT iClipVariation
	TABLE_DATA tablesData[3]
	#IF IS_DEBUG_BUILD
//	BOOL bMoveSeat
	INT iSeatWidget
	#ENDIF
ENDSTRUCT

STRUCT PLAYER_BROADCAST_DATA
	BOOL bRequestChampAct
//	BOOL bPlayerHasFinishedChampAct
//	BOOL bPlayerStartedChampAct
	BOOL bRequestCigarNetProp
	BOOL bCleaningUp
	INT iTableNumber
	INT iSeatID = -1// this is the index of the seat in the serverBD.activitySeats array
	LUXE_ACT_CLIENT_REQUEST_CHAMP_STATE clientRequestStage = CR_REQUESTING
	
	BOOL bIsClientFinishedCigar
//	NETWORK_INDEX propLiquid
//	NETWORK_INDEX propLighter
	
	INT iActivityVariation
	#IF IS_DEBUG_BUILD

	#ENDIF
ENDSTRUCT

CONST_INT OBJ_RES_CHAMP		0
CONST_INT OBJ_RES_CIGAR		1

CONST_INT MAX_LUXE_SEATS	6

STRUCT LUXE_ACT_SERVER_STRUCT
	
//	BOOL bObjectsReservedForSeat[NUM_NETWORK_PLAYERS]
//	BOOL bPropsNeedToBeCreated[NUM_NETWORK_PLAYERS]
//	BOOL bPropsNeedToBeDestroyed[NUM_NETWORK_PLAYERS]
	
	INT iObjectsReservedForSeatBitSet[2]
	INT iPropsNeedToBeCreatedBitSet
	INT iPropsNeedToBeDestroyedBitSet
ENDSTRUCT

STRUCT LUXE_ACT_STRUCT
	LUXE_ACT_STATE luxeActivityState = CL_LUXE_ACT_INIT
	LUXE_ACT_SCRIPT_STATE luxeActivityScriptState = CL_LUXE_ACT_CHECK_IN_VEH
	BOOL bPropsNeedToAlpha
	INT iNetSceneid
	INT iPromptContextIntention
	INT iLuxeVehInstanceID = -1
	INT iLightState = -1
	INT iParticipantAttempts = 0
	STRING sPromptMessage
	
	VEHICLE_INDEX vehicleIndex
	BOOL bObjectsReservedForSeat
	BOOL bObjectsReservedForPlayer
//	OBJECT_INDEX oVeh
	BOOL bNetChampVisible[3] // Prop visibility for Champagne activity per table
	
	FLOAT fPickUpBottlePhase
	FLOAT fPutDownBottlePhase
	
	FLOAT fPickUpFluteBackPhase
	FLOAT fPutDownFluteBackPhase
	
//	FLOAT fPickUpCigarBackPhase
//	FLOAT fPutDownCigarBackPhase
//	
//	FLOAT fPickUpBottleBackPhase
//	FLOAT fPutDownBottleBackPhase
//	
//	FLOAT fPickUpFluteBackPhase
//	FLOAT fPutDownFluteBackPhase
//	
//	FLOAT fPickUpBottleFrontPhase
//	FLOAT fPutDownBottleFrontPhase
//	
//	FLOAT fPickUpFluteFrontPhase
//	FLOAT fPickUpCigarFrontPhase
//	FLOAT fPutDownCigarFrontPhase
//	FLOAT fPutDownFluteFrontPhase
//	
//	FLOAT fPutDownBottleDriverSidePhase
//	FLOAT fPickUpBottleDriverSidePhase
//			
//	FLOAT fPickUpFluteDriverSidePhase
//	FLOAT fPutDownFluteDriverSidePhase
//		
//	FLOAT fPutDownBottlePasSidePhase
//	FLOAT fPickUpBottlePasSidePhase
//			
//	FLOAT fPickUpFlutePasSidePhase
//	FLOAT fPutDownFlutePasSidePhase
	
	FLOAT fEndPhaseTime
	
//	FLOAT fPickUpCigarPhase
//	FLOAT fPutDownCigarPhase 
//	
//	FLOAT fPutDownBottlePhase
//	FLOAT fPickUpBottlePhase 
//	
//	FLOAT fPickUpFlutePhase 
//	FLOAT fPutDownFlutePhase
	
	
	STRING sChampAnimBackDict
	STRING sChampAnimFrontDict
	STRING sCigarAnimBackDict
	STRING sCigarAnimFrontDict
	STRING sChampAnimDriverSideDict
	STRING sChampAnimPassengerSideDict
	
	
	NETWORK_INDEX niVehNetID
	
	//ANIM_DATA none
//	ANIM_DATA playAnim
//	ANIM_DATA pourGlassAnim
//	ANIM_DATA pourBottleAnim
	
	MODEL_NAMES vehModel
	BOOL bCigarAttached
	BOOL bRequestChamp
	STRING sSeat
	INT iSeatID = -1
	
	STRING sCigarAnimDict
	STRING sChampAnimDict
	
	STRING sCigarClip
	STRING sCigarLighterClip
	STRING sCigarCigarClip
	
	STRING sPourClip
	STRING sFluteClip
	STRING sChampClip
	STRING sBottleClip
	
	BOOL bEmptyPropsHidden
	
	NETWORK_INDEX propLiquid

	
	BOOL bDebugIgnoreInVeh
//	SCRIPT_TIMER cigarAudioTimer
//	SCRIPT_TIMER champAudioTimer
	SCRIPT_TIMER animSafetyTimeout		// Timer used currently in CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC to timeout when grabbing props
	
	INT iClipVariation
	
	INT iSwitchFluteLocalVisibilityBitSet
	INT iSwitchBottleLocalVisibilityBitSet
	INT iSwitchCigarLocalVisibilityBitSet
	INT iSwitchLighterLocalVisibilityBitSet
	
//	BOOL bSwitchFluteLocalVisibility[NUM_NETWORK_PLAYERS]
//	BOOL bSwitchBottleLocalVisibility[NUM_NETWORK_PLAYERS]
//	BOOL bSwitchCigarLocalVisibility[NUM_NETWORK_PLAYERS]
	
	PTFX_ID ptfxIDCigar
	PTFX_ID ptfxIDCigarIdle
	PTFX_ID ptfxIDLighter
//	PTFX_ID ptfxIDCigarExhale
	SCRIPT_TIMER scriptCleanupTimer
	BOOL bScriptCleanupTimerStarted
	#IF IS_DEBUG_BUILD
	BOOL bActivateLuxeVehActivities
	BOOL bCleanupScriptDebug
	BOOL bDisableDrunkDebug = FALSE
	#ENDIF
	
	BOOL bNeedToLightCigar = TRUE
	//SCRIPT_TIMER cigarBurnOutTimer
	SCRIPT_TIMER timerPromptDelay
	BOOL bCigarIdleSmokeFXStarted
	BOOL bCigarAnimSmokeFXStarted
	//INT iTimeToBurnOut = 5000
	FLOAT fAmberLightIntensity
	BOOL bClosingSwiftRearDoors
	//BOOL bChampPropsNetworkVisible
	INT iLocalHelpCounter
	
	INT iLoadPropFail
	INT iCreationSeatNum
	SEAT_DATA activitySeats[6]
	BOOL bInhaling
	BOOL bWaitForBDSync
	
	BOOL bAudioBanksRequested
	
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD

BOOL debug_bForceHelpText = FALSE

FUNC STRING LuxeScriptActString(LUXE_ACT_SCRIPT_STATE iLuxeActState)

	SWITCH iLuxeActState
		CASE CL_LUXE_ACT_CHECK_IN_VEH					RETURN "CL_LUXE_ACT_CHECK_IN_VEH"						
		CASE CL_LUXE_ACT_LAUNCH_SCRIPT					RETURN "CL_LUXE_ACT_LAUNCH_SCRIPT"
		CASE CL_LUXE_ACT_SCRIPT_LAUNCHED				RETURN "CL_LUXE_ACT_SCRIPT_LAUNCHED"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING LuxeActString(LUXE_ACT_STATE iLuxeActState)

	SWITCH iLuxeActState
		CASE CL_LUXE_ACT_INIT                     		RETURN "CL_LUXE_ACT_INIT"
		CASE CL_LUXE_ACT_IN_VEH                     	RETURN "CL_LUXE_ACT_IN_VEH"
		CASE CL_LUXE_ACT_IN_VEH_PLAY_CHAMP              RETURN "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP"
		CASE CL_LUXE_ACT_IN_VEH_IN_FRONT               	RETURN "CL_LUXE_ACT_IN_VEH_IN_FRONT"
		CASE CL_LUXE_ACT_IN_VEH_PROMPT_ACT   			RETURN "CL_LUXE_ACT_IN_VEH_PROMPT_ACT"	
		CASE CL_LUXE_ACT_GET_SERVER_CHAMP_RESPONSE      RETURN "CL_LUXE_ACT_GET_SERVER_CHAMP_RESPONSE"	
		CASE CL_LUXE_ACT_PICK_UP_BOTTLE     			RETURN "CL_LUXE_ACT_PICK_UP_BOTTLE"		
		CASE CL_LUXE_ACT_PICK_UP_FLUTE                  RETURN "CL_LUXE_ACT_PICK_UP_FLUTE"	
		CASE CL_LUXE_ACT_RELEASE_CHAMP					RETURN "CL_LUXE_ACT_RELEASE_CHAMP"	
		CASE CL_LUXE_ACT_POLL_ANIM_FOR_TAGS             RETURN "CL_LUXE_ACT_POLL_ANIM_FOR_TAGS"
		CASE CL_LUXE_ACT_PICK_UP_CIGAR                  RETURN "CL_LUXE_ACT_PICK_UP_CIGAR"
		CASE CL_LUXE_ACT_IN_VEH_PLAY_CIGAR_SYNC         RETURN "CL_LUXE_ACT_IN_VEH_PLAY_CIGAR_SYNC"
		CASE CL_LUXE_CREATE_CIGAR_PROPS		            RETURN "CL_LUXE_CREATE_CIGAR_PROPS"
		CASE CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC          RETURN "CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC"
		CASE CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC       RETURN "CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC"
		CASE CL_LUXE_ACT_RELEASE_CIGAR                  RETURN "CL_LUXE_ACT_RELEASE_CIGAR"
		CASE CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC         RETURN "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC"
		CASE CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC          RETURN "CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC"
		CASE CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC       RETURN "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC"
		CASE CL_LUXE_ACT_POLL_ANIM_FOR_TAGS_CIGAR       RETURN "CL_LUXE_ACT_POLL_ANIM_FOR_TAGS_CIGAR"
		CASE CL_LUXE_ACT_IN_VEH_PLAY_CIGAR              RETURN "CL_LUXE_ACT_IN_VEH_PLAY_CIGAR"
		CASE CL_LUXE_ACT_CHANGING_SEAT                  RETURN "CL_LUXE_ACT_CHANGING_SEAT"
		CASE CL_LUXE_ACT_CLEANING_UP                    RETURN "CL_LUXE_ACT_CLEANING_UP"
		CASE CL_LUXE_ACT_CLEANED_UP						RETURN "CL_LUXE_ACT_CLEANED_UP"						
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING LuxeServerActString(LUXE_SERVER_ACT_SCRIPT_STATE iLuxeActState)

	SWITCH iLuxeActState
		CASE SR_LUXE_INIT							RETURN "SR_LUXE_INIT"
		CASE SR_LUXE_CREATE_PROPS					RETURN "SR_LUXE_CREATE_PROPS"
		CASE SR_LUXE_LOAD_PROPS						RETURN "SR_LUXE_LOAD_PROPS"
		CASE SR_LUXE_RUNNING                     	RETURN "SR_LUXE_RUNNING"						
		CASE SR_LUXE_CLEANING_UP                    RETURN "SR_LUXE_CLEANING_UP"						
		CASE SR_LUXE_CLEANED_UP 					RETURN "SR_LUXE_CLEANED_UP"	
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING LuxeClientRequestChampString(LUXE_ACT_CLIENT_REQUEST_CHAMP_STATE iLuxeRequestChampState)

	SWITCH iLuxeRequestChampState
		CASE CR_REQUESTING							RETURN "CR_REQUESTING"
		CASE CR_CHAMP_BUSY							RETURN "CR_CHAMP_BUSY"
		CASE CR_PLAY_CHAMP							RETURN "CR_PLAY_CHAMP"
		CASE CR_CHAMP_IDLE                          RETURN "CR_CHAMP_IDLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING LuxeServerChampCheckString(SERVER_CHAMP_CHECK_STATE iLuxeChampCheckState)

	SWITCH iLuxeChampCheckState
		CASE SR_CHECK_FOR_REQUESTS                  RETURN "SR_CHECK_FOR_REQUESTS"
		CASE SR_CHECK_IF_CLIENT_FINISHED            RETURN "SR_CHECK_IF_CLIENT_FINISHED"
	ENDSWITCH
	
	RETURN ""
ENDFUNC


FUNC STRING LuxeGenString(LUXE_ACT_GEN_STATE iLuxeState)

	SWITCH iLuxeState
		CASE CHECK_PLAYER_IS_IN_LUXE_ACTIVITY_VEH      RETURN "CHECK_PLAYER_IS_IN_LUXE_ACTIVITY_VEH"
		CASE CHECK_FOR_VEH_WITH_INSTANCE_IDS           RETURN "CHECK_FOR_VEH_WITH_INSTANCE_IDS"
		CASE REQUEST_INSTANCE_ID            		   RETURN "REQUEST_INSTANCE_ID"
		CASE LAUNCH_LUX_VEH_SCRIPT            		   RETURN "LAUNCH_LUX_VEH_SCRIPT"
		CASE VERIFY_LAUNCH_LUX_VEH_SCRIPT			   RETURN "VERIFY_LAUNCH_LUX_VEH_SCRIPT"
		CASE RUN_VEH_ACTIVITY            		   	   RETURN "RUN_VEH_ACTIVITY"
		CASE CHECK_FOR_LUX_VEH_RESET				   RETURN "CHECK_FOR_LUX_VEH_RESET"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

#ENDIF

PROC SetGenActStage(LUXE_ACT_GEN_STATE &luxeGenPrevious, LUXE_ACT_GEN_STATE luxeGenEnum)
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SetGenActStage  - from ", LuxeGenString(luxeGenPrevious), " to ", LuxeGenString(luxeGenEnum))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	luxeGenPrevious = luxeGenEnum
ENDPROC

PROC SetLuxScriptActStage(LUXE_ACT_STRUCT &luxeActStruct, LUXE_ACT_SCRIPT_STATE iLuxeActState)
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SetLuxScriptActStage  - from ", LuxeScriptActString(luxeActStruct.luxeActivityScriptState), " to ", LuxeScriptActString(iLuxeActState))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	luxeActStruct.luxeActivityScriptState = iLuxeActState
ENDPROC

PROC SetLuxeActStage(LUXE_ACT_STRUCT &luxeActStruct, LUXE_ACT_STATE iLuxeActState)
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SetLuxeActStage - from ", LuxeActString(luxeActStruct.luxeActivityState), " to ", LuxeActString(iLuxeActState))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	luxeActStruct.luxeActivityState = iLuxeActState
ENDPROC

PROC SetServerLuxeActStage(SERVER_BROADCAST_DATA &luxeServerActStruct, LUXE_SERVER_ACT_SCRIPT_STATE iLuxeActState)
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SetServerLuxeActStage - from ", LuxeServerActString(luxeServerActStruct.luxeServerActScriptState), " to ", LuxeServerActString(iLuxeActState))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	luxeServerActStruct.luxeServerActScriptState = iLuxeActState
ENDPROC

PROC SetClientRequestChampStage(LUXE_ACT_CLIENT_REQUEST_CHAMP_STATE &previousRequestChampState, LUXE_ACT_CLIENT_REQUEST_CHAMP_STATE newRequestChampState)
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SetClientRequestChampStage - from ", LuxeClientRequestChampString(previousRequestChampState), " to ", LuxeClientRequestChampString(newRequestChampState))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	previousRequestChampState = newRequestChampState
ENDPROC

PROC SetServerChampCheckStage(SERVER_BROADCAST_DATA &serverBD, SERVER_CHAMP_CHECK_STATE iLuxeChampCheckState)
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LuxeServerChampCheckString - from ", LuxeServerChampCheckString(serverBD.serverChampCheckStage), " to ", LuxeServerChampCheckString(iLuxeChampCheckState))
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	serverBD.serverChampCheckStage = iLuxeChampCheckState
ENDPROC


#IF IS_DEBUG_BUILD
PROC CREATE_LUXE_ACT_WIDGETS(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActState)
	START_WIDGET_GROUP("LUXE_VEHICLE_ACTIVITIES")
	
		START_WIDGET_GROUP("Prompts")
			ADD_WIDGET_BOOL("debug_bForceHelpText", debug_bForceHelpText)
		STOP_WIDGET_GROUP()

		//#IF IS_DEBUG_BUILD
//		#IF SCRIPT_PROFILER_ACTIVE
//		CREATE_SCRIPT_PROFILER_WIDGET()
//		#ENDIF
		//#ENDIF
		//ADD_WIDGET_FLOAT_SLIDER("APPARTMENT_MOVE_BLEND_RATIO", APPARTMENT_MOVE_BLEND_RATIO, 0.0, 3.0, 0.01)

//		ADD_WIDGET_INT_SLIDER("Player Seat: ",serverBD.iSeatWidget, -1, 8, 1)
		
		ADD_WIDGET_INT_SLIDER("Anim Variation: ",serverBD.iDebugClipVariation, -1, 2, 1)
		
		ADD_WIDGET_BOOL("bDebugNetworkVisibiltyOfSwiftNetworkProps: ", serverBD.bDebugNetworkVisibiltyOfSwiftNetworkProps)
		ADD_WIDGET_BOOL("bMakeLuxor2NetworkPropsOnlyExistForParticipants: ", serverBD.bMakeLuxor2NetworkPropsOnlyExistForParticipants)
		ADD_WIDGET_BOOL("bMakeSwift2NetworkPropsOnlyExistForParticipants: ", serverBD.bMakeSwift2NetworkPropsOnlyExistForParticipants)
		ADD_WIDGET_BOOL("CLEANUP SCRIPT: ", luxeActState.bCleanupScriptDebug)
		ADD_WIDGET_BOOL("Disable drunk effect: ", luxeActState.bDisableDrunkDebug)
		
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vFlutePropPos", serverBD.activitySeats[0].vFlutePropPos, -10, 10, 0.01)
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vFlutePropRot", serverBD.activitySeats[0].vFlutePropRot, -360, 360, 0.01)
//		
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vChampBotPropPos", serverBD.activitySeats[0].vChampBotPropPos, -10, 10, 0.01)
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vChampBotPropRot", serverBD.activitySeats[0].vChampBotPropRot, -360, 360, 0.01)
//		
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vCigarPropPos", serverBD.activitySeats[0].vCigarPropPos, -10, 10, 0.01)
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vCigarPropRot", serverBD.activitySeats[0].vCigarPropRot, -360, 360, 0.01)
//		
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vAshTrayPropPos", serverBD.activitySeats[0].vAshTrayPropPos, -10, 10, 0.01)
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vAshTrayPropRot", serverBD.activitySeats[0].vAshTrayPropRot, -360, 360, 0.01)
//		
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vFlutePropPos1", serverBD.activitySeats[1].vFlutePropPos, -10, 10, 0.01)
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vFlutePropRot1", serverBD.activitySeats[1].vFlutePropRot, -360, 360, 0.01)
//		
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vChampBotPropPos1", serverBD.activitySeats[1].vChampBotPropPos, -10, 10, 0.01)
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vChampBotPropRot1", serverBD.activitySeats[1].vChampBotPropRot, -360, 360, 0.01)
		
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vCigarPropPos1", serverBD.activitySeats[1].vCigarPropPos, -10, 10, 0.01)
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vCigarPropRot1", serverBD.activitySeats[1].vCigarPropRot, -360, 360, 0.01)
		
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vAshTrayPropPos1", serverBD.activitySeats[1].vAshTrayPropPos, -10, 10, 0.01)
//		ADD_WIDGET_VECTOR_SLIDER("TEST: vAshTrayPropRot1", serverBD.activitySeats[1].vAshTrayPropRot, -360, 360, 0.01)
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

//		IF ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_dside_r") // done
//		OR ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r")
//		OR ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r2")
//			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "IS_BACK_FACING_SEAT: seat: ", activitySeat.sSeat, " is back facing")
//			RETURN TRUE
//		ELIF ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_dside_r1") // hack
//		OR ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r1") // hack
//		OR ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r3")
//			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "IS_BACK_FACING_SEAT: seat: ", activitySeat.sSeat, " is front facing")
//			RETURN FALSE
//		ENDIF

FUNC BOOL IS_PLAYER_IN_LUX_JET_VEHICLE(MODEL_NAMES mnVehicleModels)
	SWITCH mnVehicleModels
		CASE LUXOR2
		CASE NIMBUS
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_LUX_HELI_VEHICLE(MODEL_NAMES mnVehicleModels)
	SWITCH mnVehicleModels
		CASE SWIFT2
		CASE SUPERVOLITO
		CASE SUPERVOLITO2
		CASE VOLATUS
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC
		

FUNC BOOL IS_BACK_FACING_SEAT(LUXE_ACT_STRUCT &luxeActStruct, SEAT_DATA &activitySeat)
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
		IF ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_dside_r1") // done
		OR ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r1")
		OR ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r2")
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "IS_BACK_FACING_SEAT: seat: ", activitySeat.sSeat, " is back facing")
			RETURN TRUE
		ELIF ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_dside_r")
		OR ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r")
		OR ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r3")
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "IS_BACK_FACING_SEAT: seat: ", activitySeat.sSeat, " is front facing")
			RETURN FALSE
		ENDIF
	ELIF IS_PLAYER_IN_LUX_HELI_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
		RETURN FALSE // The Swift heli has no back facing seats
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_DRIVER_SIDE_SEAT(LUXE_ACT_STRUCT &luxeActStruct, SEAT_DATA &activitySeat)
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
//		IF ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r")
//		OR ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r3")
//		OR ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_dside_r")
//			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "IS_BACK_FACING_SEAT: seat: ", activitySeat.sSeat, " is front facing")
			RETURN FALSE
//		ELSE
//			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "IS_BACK_FACING_SEAT: seat: ", activitySeat.sSeat, " is back facing")
//			RETURN TRUE
//		ENDIF
//	ELIF SWIFT2 = GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
//		RETURN FALSE // The Swift heli has no back facing seats
	ELSE
//		IF ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r")
//		OR ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_pside_r3")
		IF ARE_STRINGS_EQUAL(activitySeat.sSeat, "seat_dside_r")
			RETURN TRUE
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC



PROC CIGAR_MAKE_INVISIBLE_START_SYNC(LUXE_ACT_STRUCT &luxeActStruct, SERVER_BROADCAST_DATA &serverBD, INT iSeatID)

		CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CIGAR_MAKE_INVISIBLE_START_SYNC: forcing local anim prop to be visible")
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.activitySeats[iSeatID].propCigarAnim)
			SET_ENTITY_LOCALLY_VISIBLE(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propCigarAnim))
		ENDIF
		
//		IF NETWORK_DOES_NETWORK_ID_EXIST(seat.propCigarIdle)
//			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CIGAR_MAKE_INVISIBLE_START_SYNC: forcing local idle prop to be invisible")
//			SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_OBJ(seat.propCigarIdle))
//		ENDIF

		IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[iSeatID].propCigarIdle)
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CIGAR_MAKE_INVISIBLE_START_SYNC: forcing local idle prop to be invisible")
			SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeatID].propCigarIdle, FALSE)
		ENDIF
//		SET_ENTITY_LOCALLY_VISIBLE(propLiquid)
//		SET_ENTITY_VISIBLE(NET_TO_OBJ(luxeActStruct.propLiquid), TRUE)
				

ENDPROC

PROC CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC(LUXE_ACT_STRUCT &luxeActStruct, SERVER_BROADCAST_DATA &serverBD, INT iSeatID)

		CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: forcing network anim prop to be visible")
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.activitySeats[iSeatID].propChampBotAnim)
			
			SET_ENTITY_LOCALLY_VISIBLE(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propChampBotAnim)) // set local anim prop visibile
			IF GET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propChampBotAnim)) < 255
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: prop alpha is: ", GET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propChampBotAnim)), " so forcing alpha")
//				SCRIPT_ASSERT("IF bottle fades in, then we still have a problem, if it didn't, then we've just fixed the problem")
				SET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propChampBotAnim), 255, FALSE)
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: after setting alpha, prop alpha is: ", GET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propChampBotAnim)), " so forcing alpha")
			ENDIF
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: network anim prop exists so making visible locally")
			
			IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[iSeatID].propChampBotIdle)
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: forcing local idle prop to be invisible")
				SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeatID].propChampBotIdle, FALSE) // an set idle one to invisibilt, i.e. swap
			ELSE
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: forcing local idle prop to be invisible but prop doesn't exist: iSeatID: ", iSeatID)
			ENDIF
		ELSE // but if network anim prop doesn't exist yet, just keep making the idle prop visible to prevent the bottle from disappearing
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: network anim prop DOES NOT exists so making local idle visibile")
			IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[iSeatID].propChampBotIdle)
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: forcing local idle prop to be invisible")
				SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeatID].propChampBotIdle, TRUE)
			ELSE
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: forcing local idle prop to be invisible but prop doesn't exist: iSeatID: ", iSeatID)
			ENDIF
		ENDIF
//		SET_ENTITY_LOCALLY_VISIBLE(propLiquid)
//		SET_ENTITY_VISIBLE(NET_TO_OBJ(luxeActStruct.propLiquid), TRUE)
				

ENDPROC

PROC CIGAR_LIGHTER_MAKE_INVISIBLE_START_SYNC(SERVER_BROADCAST_DATA &serverBD, INT iSeatID)

		CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "POD: CIGAR_LIGHTER_MAKE_INVISIBLE_START_SYNC: force local anim prop to be visible")
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.activitySeats[iSeatID].propLighter)
			SET_ENTITY_LOCALLY_VISIBLE(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propLighter))
			IF GET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propLighter)) < 255
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CIGAR_LIGHTER_MAKE_INVISIBLE_START_SYNC: prop alpha is: ", GET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propLighter)), " so forcing alpha")
//				SCRIPT_ASSERT("IF bottle fades in, then we still have a problem, if it didn't, then we've just fixed the problem")
				RESET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propLighter))
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CIGAR_LIGHTER_MAKE_INVISIBLE_START_SYNC: after setting alpha, prop alpha is: ", GET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propLighter)), " so forcing alpha")
			ENDIF
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_FLUTE_MAKE_INVISIBLE_START_SYNC: network anim prop exists so making visible locally")//, ENTITY ALPHA: ", GET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propLighter)))
//			IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[iSeatID].propChampFluteIdle)
//				SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeatID].propChampFluteIdle, FALSE)
//			ELSE
//				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: forcing local idle prop to be invisible but prop doesn't exist: iSeatID: ", iSeatID)
//			ENDIF
		ELSE
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_FLUTE_MAKE_INVISIBLE_START_SYNC: network anim prop DOES NOT exist so making local idle visible")
//			IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[iSeatID].propChampFluteIdle)
//				SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeatID].propChampFluteIdle, TRUE)
//			ELSE
//				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: forcing local idle prop to be invisible but prop doesn't exist: iSeatID: ", iSeatID)
//			ENDIF
		ENDIF				

ENDPROC

PROC CHAMP_FLUTE_MAKE_INVISIBLE_START_SYNC(LUXE_ACT_STRUCT &luxeActStruct, SERVER_BROADCAST_DATA &serverBD, INT iSeatID, ENTITY_INDEX propLiquid)

		CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_FLUTE_MAKE_INVISIBLE_START_SYNC: force network anim prop to be visible")
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.activitySeats[iSeatID].propChampFluteAnim)
			SET_ENTITY_LOCALLY_VISIBLE(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propChampFluteAnim))
			IF GET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propChampFluteAnim)) < 255
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_FLUTE_MAKE_INVISIBLE_START_SYNC: prop alpha is: ", GET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propChampFluteAnim)), " so forcing alpha")
//				SCRIPT_ASSERT("IF bottle fades in, then we still have a problem, if it didn't, then we've just fixed the problem")
//				RESET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propChampFluteAnim))
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_FLUTE_MAKE_INVISIBLE_START_SYNC: after setting alpha, prop alpha is: ", GET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propChampFluteAnim)), " so forcing alpha")
			ENDIF
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_FLUTE_MAKE_INVISIBLE_START_SYNC: network anim prop exists so making visible locally")//, ENTITY ALPHA: ", GET_ENTITY_ALPHA(NET_TO_OBJ(serverBD.activitySeats[iSeatID].propChampFluteAnim)))
			IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[iSeatID].propChampFluteIdle)
				SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeatID].propChampFluteIdle, FALSE)
			ELSE
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC: forcing local idle prop to be invisible but prop doesn't exist: iSeatID: ", iSeatID)
			ENDIF
		ELSE
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_FLUTE_MAKE_INVISIBLE_START_SYNC: network anim prop DOES NOT exist so making local idle visible")
			IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[iSeatID].propChampFluteIdle)
				SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeatID].propChampFluteIdle, TRUE)
			ELSE
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "POD: CHAMP_FLUTE_MAKE_INVISIBLE_START_SYNC: forcing local idle prop to be invisible but prop doesn't exist: iSeatID: ", iSeatID)
			ENDIF
		ENDIF
		SET_ENTITY_LOCALLY_VISIBLE(propLiquid)
//		SET_ENTITY_VISIBLE(NET_TO_OBJ(luxeActStruct.propLiquid), TRUE)
				

ENDPROC


PROC RESERVE_PLAYER_SPECIFIC_PROPS_FOR_CREATION(LUXE_ACT_STRUCT &luxeActStruct)
	IF luxeActStruct.bObjectsReservedForPlayer = FALSE
		IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 2,false,true)
				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 2)
	//			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(luxeActStruct.propLiquid)
	//			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(luxeActStruct.propLighter)
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RESERVE_PLAYER_SPECIFIC_PROPS_FOR_CREATION: ALL PROPS reserved for Luxor2, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())
				luxeActStruct.bObjectsReservedForPlayer = TRUE	
			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RESERVE_PLAYER_SPECIFIC_PROPS_FOR_CREATION: CANNOT reserved PROPS for Luxor2, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())
			ENDIF		
				
			
		ELSE
			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1,false,true)
				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1)
		//		NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(luxeActStruct.propLiquid)
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RESERVE_PLAYER_SPECIFIC_PROPS_FOR_CREATION: ALL PROPS Reserved for swift2, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())
				luxeActStruct.bObjectsReservedForPlayer = TRUE
			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RESERVE_PLAYER_SPECIFIC_PROPS_FOR_CREATION: CANNOT reserved PROPS for swift2, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UNRESERVE_PLAYER_SPECIFIC_PROPS_FOR_CREATION(LUXE_ACT_STRUCT &luxeActStruct)
	
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
		
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 2)
//			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(luxeActStruct.propLiquid)
//			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(luxeActStruct.propLighter)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "UNRESERVE_PLAYER_SPECIFIC_PROPS_FOR_CREATION: ALL PROPS reserved for Luxor2, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())
			luxeActStruct.bObjectsReservedForPlayer = FALSE	
		
	ELSE
		IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1,false,true)
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
	//		NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(luxeActStruct.propLiquid)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "UNRESERVE_PLAYER_SPECIFIC_PROPS_FOR_CREATION: ALL PROPS Reserved for swift2, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())
			luxeActStruct.bObjectsReservedForPlayer = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL RESERVE_NETWORK_PROPS_FOR_CHAMP()
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RESERVE_NETWORK_PROPS_FOR_CHAMP: number of reserved mission objects = ", GET_NUM_RESERVED_MISSION_OBJECTS())
		IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 3,false,true)
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 3)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RESERVE_NETWORK_PROPS_FOR_CHAMP: Reserved new mission objects, now reserved mission objects = ", GET_NUM_RESERVED_MISSION_OBJECTS())	
			RETURN TRUE
//							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
//							
//							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
//							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLiquid)
							
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: 3 Props registered for seat: name: ", activitySeat.sSeat, " ID: , GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RESERVE_NETWORK_PROPS_FOR_CHAMP: cannot reserve more mission objects")
		ENDIF
	RETURN FALSE		
ENDFUNC

FUNC BOOL RESERVE_NETWORK_PROPS_FOR_CIGAR()
		
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RESERVE_NETWORK_PROPS_FOR_CIGAR: number of reserved mission objects = ", GET_NUM_RESERVED_MISSION_OBJECTS())		
		IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 2,false,true)
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 2)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RESERVE_NETWORK_PROPS_FOR_CIGAR: Reserved new mission objects, now reserved mission objects = ", GET_NUM_RESERVED_MISSION_OBJECTS())		
//							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
//							
//							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
//							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLiquid)
							
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: 2 Props registered for seat: name: ", activitySeat.sSeat, " ID: , GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RESERVE_NETWORK_PROPS_FOR_CIGAR: cannot reserve more mission objects")		
		ENDIF
	RETURN FALSE		
ENDFUNC

PROC UNRESERVE_NETWORK_PROPS_FOR_CHAMP(SEAT_DATA &activitySeat)
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: unregistering for seat: name: ", activitySeat.sSeat, " ID: , GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())		
	RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 3)
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
//		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
//		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLiquid)
							
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: 3 Props registered for seat: name: ", activitySeat.sSeat, " ID: , GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
		

ENDPROC

//PROC RESERVE_PROPS_FOR_CREATION(LUXE_ACT_STRUCT &luxeActStruct, SEAT_DATA &activitySeat)
//	IF luxeActStruct.bObjectsReservedForSeat = FALSE
//		IF LUXOR2 = GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
//			IF NOT IS_BACK_FACING_SEAT(luxeActStruct, activitySeat)
//				IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 7,false,true)
//					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 7)
//				
////							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propAshtray)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarIdle)
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: 7 Props registered for front facing seat: name: ", activitySeat.sSeat, " ID: , GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
//					luxeActStruct.bObjectsReservedForSeat = TRUE
//				ENDIF
//			ELSE
//				IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 4,false,true)
//					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 4)
////							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarIdle)
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: 4 Props registered for back facing seat: name: ", activitySeat.sSeat, " ID: , GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
//					luxeActStruct.bObjectsReservedForSeat = TRUE
//				ENDIF
//			ENDIF
//		ELSE
//			IF NOT IS_PLAYER_IN_DRIVER_SIDE_SEAT(luxeActStruct, activitySeat)
//				IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 4,false,true)
//					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 4)
////								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
////								AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
////								AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
////								AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
//					luxeActStruct.bObjectsReservedForSeat = TRUE
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: 4 Props registered for driver side seat:  name: ", activitySeat.sSeat, " ID:, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
//				ENDIF
//					
//			ELSE
//				IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 2,false,true)
//					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 2)
////								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: 2 Props registered for driver passenger seat:  name: ", activitySeat.sSeat, " ID: GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
//					luxeActStruct.bObjectsReservedForSeat = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//
//ENDPROC

//PROC UNRESERVE_PROPS_FOR_CREATION(LUXE_ACT_STRUCT &luxeActStruct, SEAT_DATA &activitySeat)
//	IF LUXOR2 = GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
//		IF NOT IS_BACK_FACING_SEAT(luxeActStruct, activitySeat)
//			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 7)
//		
////							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propAshtray)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarIdle)
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: 7 Props unregistered for front facing seat: name: ", activitySeat.sSeat, " ID:  GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
//			luxeActStruct.bObjectsReservedForSeat = FALSE
//
//			
//		ELSE
//			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 4)
////							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarIdle)
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: 4 Props unregistered for front facing seat:  name: ", activitySeat.sSeat, " ID:  GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
//			luxeActStruct.bObjectsReservedForSeat = FALSE
//		ENDIF
//	ELSE
//		IF NOT IS_PLAYER_IN_DRIVER_SIDE_SEAT(luxeActStruct, activitySeat)
//			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 4)
////								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
////								AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
////								AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
////								AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: 4 Props unregistered for front facing seat:  name: ", activitySeat.sSeat, " ID: GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
//			luxeActStruct.bObjectsReservedForSeat = FALSE
//		ELSE
//			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 2)
////								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
////							AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: 2 Props unregistered for front facing seat:  name: ", activitySeat.sSeat, " ID:  GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS())	
//			luxeActStruct.bObjectsReservedForSeat = FALSE
//		ENDIF
//	ENDIF
//	
//
//ENDPROC

FUNC MODEL_NAMES GET_ASHTRAY_MODEL(LUXE_ACT_STRUCT &luxeActStruct)

	SWITCH GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
		CASE LUXOR2
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_ASHTRAY_LUXE_01"))
		BREAK
		CASE NIMBUS
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("EX_prop_ashtray_LUXE_02"))
		BREAK
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC		

		

FUNC BOOL LOAD_NEW_LUXE_ACTIVITY_PROPS_LOOP(LUXE_ACT_STRUCT &luxeActStruct)

		
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Lux_Prop_Champ_Flute_LUXE")))					
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: Lux_Prop_Champ_Flute_LUXE Loaded")
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: Lux_Prop_Champ_Flute_LUXE not Loaded yet")
		ENDIF
	
	
		
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_P_CHAMP_FLUTE_S")))
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: LUX_P_CHAMP_FLUTE_S Loaded")
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: LUX_P_CHAMP_FLUTE_S not Loaded yet")
		ENDIF

		
		
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")))
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: LUX_PROP_CHAMP_01_LUXE Loaded")
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: LUX_PROP_CHAMP_01_LUXE not Loaded yet")
		ENDIF
	
		
		
		IF SWIFT2 != GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
		AND NOT IS_PLAYER_IN_LUX_HELI_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
		
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CIGAR_01_LUXE")))
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: PROP_CIGAR_01 loaded ")
			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: PROP_CIGAR_01 not loaded yet")
			ENDIF


		
			
			IF REQUEST_LOAD_MODEL(GET_ASHTRAY_MODEL(luxeActStruct))

			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: LUX_PROP_ASHTRAY_LUXE_01 not loaded yet")
			ENDIF

		ENDIF
		
		IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))	
			
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")))
			AND REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_P_CHAMP_FLUTE_S")))
			AND REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Lux_Prop_Champ_Flute_LUXE")))		
			AND REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CIGAR_01_LUXE")))
			AND REQUEST_LOAD_MODEL(GET_ASHTRAY_MODEL(luxeActStruct))
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: ALL MODELS LOADED for LUXOR2")

				
				RETURN TRUE
			ENDIF
			
		ELSE
			
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")))
			AND REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_P_CHAMP_FLUTE_S")))
			AND REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Lux_Prop_Champ_Flute_LUXE")))	
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: ALL MODELS LOADED for SWIFT2")

				RETURN TRUE
			ENDIF
			
		ENDIF

	
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: NOT ALL MODELS LOADED")
	RETURN FALSE
ENDFUNC

//FUNC BOOL CREATE_NEW_LUXE_ACTIVITY_PROPS_LOOP(LUXE_ACT_STRUCT &luxeActStruct, SEAT_DATA &activitySeat)
//	IF NOT IS_ENTITY_DEAD(luxeActStruct.vehicleIndex)
//		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: REQUESTING propChampFluteIdle")
//			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Lux_Prop_Champ_Flute_LUXE")))					
//				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//				
//					activitySeat.propChampFluteIdle = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("Lux_Prop_Champ_Flute_LUXE")), activitySeat.vFlutePropPos))
//
//					ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propChampFluteIdle), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vFlutePropPos, activitySeat.vFlutePropRot)
//					SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propChampFluteIdle), TRUE)
//					SET_DISABLE_FRAG_DAMAGE(NET_TO_ENT(activitySeat.propChampFluteIdle), TRUE)
//
//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: CREATING propChampFluteIdle, position: ", activitySeat.vFlutePropPos, " for seat: ", activitySeat.sSeat)
//				ELSE
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: propChampFluteIdle - CAN_REGISTER_MISSION_ENTITIES = FALSE")
//	    		ENDIF
//
//			ELSE
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: PROP_CHAMP_FLUTE not Loaded yet")
//			ENDIF
//		ENDIF
//		
//		
//		
//		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
//
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: REQUESTING propChampFluteAnim")
//			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_P_CHAMP_FLUTE_S")))
//
//				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//
//					activitySeat.propChampFluteAnim = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_P_CHAMP_FLUTE_S")), activitySeat.vFlutePropPos))
//
//					ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propChampFluteAnim), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vFlutePropPos, activitySeat.vFlutePropRot)
//					SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propChampFluteAnim), TRUE)
//					SET_ENTITY_VISIBLE(NET_TO_ENT(activitySeat.propChampFluteAnim), FALSE)
//
//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: CREATING propChampFluteAnim, position: ", activitySeat.vFlutePropPos, " for seat: ", activitySeat.sSeat)
//				ELSE
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: propChampFluteAnim - CAN_REGISTER_MISSION_ENTITIES = FALSE")
//	    		ENDIF
//
//			ELSE
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: PROP_CHAMP_FLUTE not Loaded yet")
//			ENDIF
//
//		ENDIF
//		
//		IF SWIFT2 != GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
//			
//			
//			
//			IF NOT IS_BACK_FACING_SEAT(luxeActStruct, activitySeat)
//				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: REQUESTING propChampBotIdle")
//					IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")))
//
//						IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//						
//							activitySeat.propChampBotIdle = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")), activitySeat.vChampBotPropPos))
//
//							ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propChampBotIdle), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vChampBotPropPos, activitySeat.vChampBotPropRot)
//							SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propChampBotIdle), TRUE)
//							SET_DISABLE_FRAG_DAMAGE(NET_TO_ENT(activitySeat.propChampBotIdle), TRUE)
//
//							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: CREATING propChampBotIdle, position: ", activitySeat.vChampBotPropPos, " for seat: ", activitySeat.sSeat)
//						ELSE
//							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: propChampBotIdle - CAN_REGISTER_MISSION_ENTITIES = FALSE")
//			    		ENDIF
//
//					ELSE
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: PROP_CHAMP_FLUTE not Loaded yet")
//					ENDIF
//
//				ENDIF
//			
//				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
//
//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: REQUESTING propChampBotAnim")
//					IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")))
//
//						IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//							VECTOR vTemp = GET_WORLD_POSITION_OF_ENTITY_BONE(luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat))
//							
//							activitySeat.propChampBotAnim = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")), activitySeat.vChampBotPropPos))
//							vTemp = GET_ENTITY_COORDS(NET_TO_ENT(activitySeat.propChampBotAnim))
//							ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propChampBotAnim), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vChampBotPropPos, activitySeat.vChampBotPropRot)
//							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: vChampBotPropPos: ", activitySeat.vChampBotPropPos, ", champ prop position: ", vTemp)
//							SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propChampBotAnim), TRUE)
//							SET_DISABLE_FRAG_DAMAGE(NET_TO_ENT(activitySeat.propChampBotAnim), TRUE)
//							SET_ENTITY_VISIBLE(NET_TO_ENT(activitySeat.propChampBotAnim), FALSE)
//
//							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: CREATING propChampBotAnim, position: ", activitySeat.vChampBotPropPos)
//						ELSE
//							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: propChampBotAnim - CAN_REGISTER_MISSION_ENTITIES = FALSE")
//			    		ENDIF
//
//					ELSE
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: PROP_CHAMP_01A not loaded yet")
//					ENDIF
//
//				ENDIF
//			ELSE
//				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: Seat is not back facing, not creating Champagne bottle")
//			ENDIF
//		ELSE
//			IF NOT IS_PLAYER_IN_DRIVER_SIDE_SEAT(luxeActStruct, activitySeat)
//				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
//
//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: REQUESTING propChampBotIdle")
//					IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")))
//
//						IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//						
//							activitySeat.propChampBotIdle = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")), activitySeat.vFlutePropPos))
//
//
//							ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propChampBotIdle), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vChampBotPropPos, activitySeat.vChampBotPropRot)
//							SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propChampBotIdle), TRUE)
//							SET_DISABLE_FRAG_DAMAGE(NET_TO_ENT(activitySeat.propChampBotIdle), TRUE)
//
//
//							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: CREATING propChampBotIdle, position: ", activitySeat.vChampBotPropPos, " for seat: ", activitySeat.sSeat)
//						ELSE
//							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: propChampBotIdle - CAN_REGISTER_MISSION_ENTITIES = FALSE")
//			    		ENDIF
//
//					ELSE
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: PROP_CHAMP_FLUTE not Loaded yet")
//					ENDIF
//
//				ENDIF
//			
//				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
//
//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: REQUESTING propChampBotAnim")
//					IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")))
//
//						IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//						
//							activitySeat.propChampBotAnim = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")), activitySeat.vFlutePropPos))
//
//							ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propChampBotAnim), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vChampBotPropPos, activitySeat.vChampBotPropRot)
//							SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propChampBotAnim), TRUE)
//							SET_DISABLE_FRAG_DAMAGE(NET_TO_ENT(activitySeat.propChampBotAnim), TRUE)
//							SET_ENTITY_VISIBLE(NET_TO_ENT(activitySeat.propChampBotAnim), FALSE)
//
//
//							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: CREATING propChampBotAnim, position: ", activitySeat.vChampBotPropPos)
//						ELSE
//							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: propChampBotAnim - CAN_REGISTER_MISSION_ENTITIES = FALSE")
//			    		ENDIF
//
//					ELSE
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: PROP_CHAMP_01A not loaded yet")
//					ENDIF
//
//				ENDIF
//			ELSE
//				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: Seat is not back facing, not creating Champagne bottle")
//			ENDIF
//		ENDIF
//		
//		IF SWIFT2 != GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
//		
//			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
//
//				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: REQUESTING propCigarAnim") 
//				IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CIGAR_01_LUXE")))
//
//					IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//					
//						activitySeat.propCigarAnim = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CIGAR_01_LUXE")), activitySeat.vCigarPropPos))
//
//						ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propCigarAnim), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vCigarPropPos, activitySeat.vCigarPropRot)
//						SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propCigarAnim), TRUE)
//						SET_ENTITY_VISIBLE(NET_TO_ENT(activitySeat.propCigarAnim), FALSE)
//
//
//						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: CREATING propCigarAnim, position: ", activitySeat.vCigarPropPos)
//					ELSE
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: propCigarAnim - CAN_REGISTER_MISSION_ENTITIES = FALSE")
//		    		ENDIF
//
//				ELSE
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: PROP_CIGAR_01 not loaded yet")
//				ENDIF
//
//			ENDIF
//			
//			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarIdle)
//
//				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: REQUESTING propCigarIdle") 
//				IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CIGAR_01_LUXE")))
//
//					IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//					
//						activitySeat.propCigarIdle = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CIGAR_01_LUXE")), activitySeat.vCigarPropPos))
//
//						ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propCigarIdle), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vCigarPropPos, activitySeat.vCigarPropRot)
//						SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propCigarIdle), TRUE)
//
//
//						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: CREATING propCigarIdle, position: ", activitySeat.vCigarPropPos)
//					ELSE
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: propCigarIdle - CAN_REGISTER_MISSION_ENTITIES = FALSE")
//		    		ENDIF
//
//				ELSE
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: PROP_CIGAR_01 not loaded yet")
//				ENDIF
//
//			ENDIF
//			
//			IF NOT IS_BACK_FACING_SEAT(luxeActStruct, activitySeat)
//				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propAshtray)
//
//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: REQUESTING propAshtray")
//						IF REQUEST_LOAD_MODEL(GET_ASHTRAY_MODEL(luxeActStruct))
//
//						IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//						
//							activitySeat.propAshtray = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_ASHTRAY_LUXE_01")), activitySeat.vAshtrayPropPos))
//
//							ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propAshtray), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vAshtrayPropPos, activitySeat.vAshtrayPropRot)
//							SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propAshtray), TRUE)
//
//
//							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: CREATING propAshtray, position: ", activitySeat.vAshtrayPropPos)
//						ELSE
//							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: propAshtray - CAN_REGISTER_MISSION_ENTITIES = FALSE")
//			    		ENDIF
//
//					ELSE
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: LUX_PROP_ASHTRAY_LUXE_01 not loaded yet")
//					ENDIF
//
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF LUXOR2 = GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
//		
//			IF NOT IS_BACK_FACING_SEAT(luxeActStruct, activitySeat)
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propAshtray)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarIdle)
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: ALL PROPS CREATED for Luxor2")
//
//					
//					RETURN TRUE
//				ENDIF
//			ELSE
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarIdle)
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: ALL PROPS CREATED for Luxor2")
//
//					
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		ELSE
//			IF NOT IS_PLAYER_IN_DRIVER_SIDE_SEAT(luxeActStruct, activitySeat)
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: PASS_SIDE: ALL PROPS CREATED for Luxor2")
//
//					RETURN TRUE
//				ENDIF
//			ELSE
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
//				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: DRIVE_SIDE: ALL PROPS CREATED for Luxor2")
//
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		
//	ENDIF
//	
//	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "LOAD_NEW_LUXE_ACTIVITY_PROPS: NOT ALL PROPS CREATED")
//	RETURN FALSE
//ENDFUNC

FUNC BOOL DO_NETWORK_CIGAR_EXIST(SEAT_DATA &activitySeat)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLighter)
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "DO_NETWORK_CIGAR_EXIST: ALL PROPS CREATED for Luxor2")

		RETURN TRUE
	ELSE
		CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "DO_NETWORK_CIGAR_EXIST: Not all props created")
		
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL HAVE_CHAMP_SERVER_PROPS_BEEN_CREATED(SEAT_DATA &activitySeat)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLiquid)
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: ALL PROPS CREATED for Luxor2")

		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_NETWORK_OBJECTS_FOR_CHAMP(SEAT_DATA &activitySeat, LUXE_ACT_STRUCT &luxeActStruct #IF IS_DEBUG_BUILD , SERVER_BROADCAST_DATA &serverBD #ENDIF)
//	IF NOT IS_ENTITY_DEAD(luxeActStruct.vehicleIndex)
			
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)

			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: REQUESTING propChampFluteAnim")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_P_CHAMP_FLUTE_S")))

				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)

					activitySeat.propChampFluteAnim = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_P_CHAMP_FLUTE_S")), <<0.1, 0.1, 0.1>>))

//					SET_ENTITY_NO_COLLISION_ENTITY(NET_TO_ENT(activitySeat.propChampFluteAnim), luxeActStruct.vehicleIndex, FALSE)
//					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_ENT(activitySeat.propChampFluteAnim), TRUE, TRUE)
					// TODO tunable range
					
					SET_ENTITY_COLLISION(NET_TO_ENT(activitySeat.propChampFluteAnim), FALSE)
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propChampFluteAnim), TRUE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(activitySeat.propChampFluteAnim), FALSE)
					IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
					#IF IS_DEBUG_BUILD
					AND serverBD.bMakeLuxor2NetworkPropsOnlyExistForParticipants = TRUE
					#ENDIF
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(activitySeat.propChampFluteAnim), TRUE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF (luxeActStruct.vehModel = SWIFT2
					OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel))
					AND serverBD.bMakeSwift2NetworkPropsOnlyExistForParticipants
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(activitySeat.propChampFluteAnim), TRUE)
					ENDIF
					#ENDIF
					NETWORK_SET_OBJECT_SCOPE_DISTANCE(NET_TO_OBJ(activitySeat.propChampFluteAnim), 30)
//					SET_NETWORK_ID_CAN_MIGRATE(activitySeat.propChampFluteAnim, FALSE)

//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: CREATING propChampFluteAnim, position: ", <<0.1, 0.1, 0.1>>, " for seat: ", activitySeat.sSeat)
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: propChampFluteAnim - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: PROP_CHAMP_FLUTE not Loaded yet")
			ENDIF

		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLiquid)

			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: REQUESTING propChampFluteAnim")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_P_Pour_champagne_LUXE")))

				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)

					activitySeat.propLiquid = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_P_Pour_champagne_LUXE")), <<0.1, 0.1, 0.1>>))


//					SET_ENTITY_NO_COLLISION_ENTITY(NET_TO_ENT(activitySeat.propLiquid), luxeActStruct.vehicleIndex, FALSE)
//					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_ENT(activitySeat.propLiquid), TRUE, TRUE)
					
					SET_ENTITY_COLLISION(NET_TO_ENT(activitySeat.propLiquid), FALSE)
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propLiquid), TRUE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(activitySeat.propLiquid), FALSE)
//					SET_NETWORK_ID_CAN_MIGRATE(activitySeat.propLiquid, FALSE)
					IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
					#IF IS_DEBUG_BUILD
					AND serverBD.bMakeLuxor2NetworkPropsOnlyExistForParticipants = TRUE
					#ENDIF
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(activitySeat.propLiquid), TRUE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF (luxeActStruct.vehModel = SWIFT2
					OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel))
					AND serverBD.bMakeSwift2NetworkPropsOnlyExistForParticipants
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(activitySeat.propLiquid), TRUE)
					ENDIF
					#ENDIF
					NETWORK_SET_OBJECT_SCOPE_DISTANCE(NET_TO_OBJ(activitySeat.propLiquid), 30)
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: CREATING .propLiquid, position: ", <<0.1, 0.1, 0.1>>, " for seat: ", activitySeat.sSeat)
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: .propLiquid - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: .propLiquid not Loaded yet")
			ENDIF

		ENDIF
		
		
	
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)

			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: REQUESTING propChampBotAnim")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")))

				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					
					activitySeat.propChampBotAnim = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")), <<0.1, 0.1, 0.1>>))
//							vTemp = GET_ENTITY_COORDS(NET_TO_ENT(activitySeat.propChampBotAnim))
//							ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propChampBotAnim), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vChampBotPropPos, activitySeat.vChampBotPropRot)
//					SET_ENTITY_NO_COLLISION_ENTITY(NET_TO_ENT(activitySeat.propChampBotAnim), luxeActStruct.vehicleIndex, FALSE)
//					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_ENT(activitySeat.propChampBotAnim), TRUE, TRUE)
					
					SET_ENTITY_COLLISION(NET_TO_ENT(activitySeat.propChampBotAnim), FALSE)
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propChampBotAnim), TRUE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(activitySeat.propChampBotAnim), FALSE)
					
					IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
					#IF IS_DEBUG_BUILD
					AND serverBD.bMakeLuxor2NetworkPropsOnlyExistForParticipants = TRUE
					#ENDIF
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(activitySeat.propChampBotAnim), TRUE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF (luxeActStruct.vehModel = SWIFT2
					OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel))
					AND serverBD.bMakeSwift2NetworkPropsOnlyExistForParticipants
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(activitySeat.propChampBotAnim), TRUE)
					ENDIF
					#ENDIF
					NETWORK_SET_OBJECT_SCOPE_DISTANCE(NET_TO_OBJ(activitySeat.propChampBotAnim), 30)
//					SET_NETWORK_ID_CAN_MIGRATE(activitySeat.propChampBotAnim, FALSE)
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: CREATING propChampBotAnim, position: ", <<0.1, 0.1, 0.1>>)
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: propChampBotAnim - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: PROP_CHAMP_01A not loaded yet")
			ENDIF

		ENDIF
		
		
				


		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLiquid)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: ALL PROPS CREATED for Luxor2")

			RETURN TRUE
		ENDIF
			
//	ENDIF
	
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: NOT ALL PROPS CREATED")
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_CIGAR_NETWORK_OBJECTS_EXIST(SEAT_DATA &activitySeat)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLighter)
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "DO_CIGAR_NETWORK_OBJECTS_EXIST: ALL PROPS CREATED for Luxor2")

		RETURN TRUE
	ENDIF
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "DO_CIGAR_NETWORK_OBJECTS_EXIST: NOT ALL PROPS CREATED for Luxor2")
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_NETWORK_OBJECTS_FOR_CIGAR(SEAT_DATA &activitySeat, LUXE_ACT_STRUCT &luxeActStruct)
//	IF NOT IS_ENTITY_DEAD(luxeActStruct.vehicleIndex)
			
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)

			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CIGAR: REQUESTING propCigarAnim")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CIGAR_01_LUXE")))

				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)

					activitySeat.propCigarAnim = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CIGAR_01_LUXE")), <<0.1, 0.1, 0.1>>))// doesn't matter where they're placed they're going straight into a sync scene

//					ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propCigarAnim), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vFlutePropPos, activitySeat.vFlutePropRot)	
					
//					SET_ENTITY_NO_COLLISION_ENTITY(NET_TO_ENT(activitySeat.propCigarAnim), luxeActStruct.vehicleIndex, FALSE)
//					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_ENT(activitySeat.propCigarAnim), TRUE, TRUE)
					
					SET_ENTITY_COLLISION(NET_TO_ENT(activitySeat.propCigarAnim), FALSE)
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propCigarAnim), TRUE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(activitySeat.propCigarAnim), FALSE)
					IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(activitySeat.propCigarAnim), TRUE)
					ENDIF
//					SET_NETWORK_ID_CAN_MIGRATE(activitySeat.propCigarAnim, FALSE)

//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CIGAR: CREATING propCigarAnim, position: ", <<0.1, 0.1, 0.1>>, " for seat: ", activitySeat.sSeat)
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CIGAR: propCigarAnim - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CIGAR: PROP_CHAMP_FLUTE not Loaded yet")
			ENDIF

		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLighter)

			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CIGAR: REQUESTING propLighter")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_LIGHTER_LUXE")))

				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)

					activitySeat.propLighter = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_LIGHTER_LUXE")), <<0.1, 0.1, 0.1>>))

//					ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(activitySeat.propLighter), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vFlutePropPos, activitySeat.vFlutePropRot)
//					SET_ENTITY_NO_COLLISION_ENTITY(NET_TO_ENT(activitySeat.propLighter), luxeActStruct.vehicleIndex, FALSE)
//					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_ENT(activitySeat.propLighter), TRUE, TRUE)
					SET_ENTITY_COLLISION(NET_TO_ENT(activitySeat.propLighter), FALSE)
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(activitySeat.propLighter), TRUE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(activitySeat.propLighter), FALSE)
//					SET_NETWORK_ID_CAN_MIGRATE(activitySeat.propLighter, FALSE)
					IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(activitySeat.propLighter), TRUE)
					ENDIF
//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CIGAR: CREATING .propLighter, position: ", <<0.1, 0.1, 0.1>>, " for seat: ", activitySeat.sSeat)
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CIGAR: .propLighter - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CIGAR: .propLighter not Loaded yet")
			ENDIF

		ENDIF

		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLighter)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CIGAR: ALL PROPS CREATED for Luxor2")

			RETURN TRUE
		ENDIF
			
//	ENDIF
	
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CIGAR: NOT ALL PROPS CREATED")
	RETURN FALSE
ENDFUNC

FUNC BOOL DESTROY_NETWORK_OBJECTS_FOR_CIGAR(SEAT_DATA &activitySeat)

		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propCigarAnim)
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CIGAR: propCigarAnim")
					
					DELETE_NET_ID(activitySeat.propCigarAnim)
					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
				ELSE
					TAKE_CONTROL_OF_NET_ID(activitySeat.propCigarAnim)
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CIGAR: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propCigarAnim")
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLighter)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLighter)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propLighter)
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CIGAR: propLighter")
					
					DELETE_NET_ID(activitySeat.propLighter)
					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
				ELSE
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CIGAR: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propLighter")
					TAKE_CONTROL_OF_NET_ID(activitySeat.propLighter)
				ENDIF
			ENDIF
		ENDIF
		

	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
	AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLighter)
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CIGAR: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " all props cleaned up")
		RETURN TRUE
	ENDIF
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: NOT ALL PROPS DESTROYED")
	RETURN FALSE
ENDFUNC


FUNC BOOL DESTROY_NETWORK_OBJECTS_FOR_CHAMP(SEAT_DATA &activitySeat)

			
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propChampFluteAnim)
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CHAMP: propChampFluteAnim")
					
					DELETE_NET_ID(activitySeat.propChampFluteAnim)
					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
				ELSE
					TAKE_CONTROL_OF_NET_ID(activitySeat.propChampFluteAnim)
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CHAMP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propChampFluteAnim")
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propChampBotAnim)
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CHAMP: propChampBotAnim")
					
					DELETE_NET_ID(activitySeat.propChampBotAnim)
					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
				ELSE
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CHAMP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propChampBotAnim")
					TAKE_CONTROL_OF_NET_ID(activitySeat.propChampBotAnim)
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLiquid)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLiquid)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propLiquid)
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CHAMP: propLiquid")
					
					DELETE_NET_ID(activitySeat.propLiquid)
					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
				ELSE
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CHAMP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propLiquid")
					TAKE_CONTROL_OF_NET_ID(activitySeat.propLiquid)
				ENDIF
			ENDIF
		ENDIF

	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
	AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
	AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propLiquid)
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: DESTROY_NETWORK_OBJECTS_FOR_CHAMP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " all props cleaned up")
		RETURN TRUE
	ENDIF
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_NETWORK_OBJECTS_FOR_CHAMP: NOT ALL PROPS DESTROYED")
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_LOCAL_DUMMY_PROPS(LUXE_ACT_STRUCT &luxeActStruct, SEAT_DATA &activitySeat)
	IF DOES_ENTITY_EXIST(luxeActStruct.vehicleIndex)
		IF NOT DOES_ENTITY_EXIST(activitySeat.propChampFluteIdle)
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: REQUESTING propChampFluteIdle")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Lux_Prop_Champ_Flute_LUXE")))					
				
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: Creating propChampFluteIdle, activitySeat.sSeat: ", activitySeat.sSeat, ", position: ", activitySeat.vFlutePropPos)
					activitySeat.propChampFluteIdle = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("Lux_Prop_Champ_Flute_LUXE")), activitySeat.vFlutePropPos, FALSE, FALSE)

					ATTACH_ENTITY_TO_ENTITY(activitySeat.propChampFluteIdle, luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vFlutePropPos, activitySeat.vFlutePropRot)
					SET_ENTITY_INVINCIBLE(activitySeat.propChampFluteIdle, TRUE)
					SET_DISABLE_FRAG_DAMAGE(activitySeat.propChampFluteIdle, TRUE)

					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: CREATING propChampFluteIdle, position: ", activitySeat.vFlutePropPos, " for seat: ", activitySeat.sSeat)
				

			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: PROP_CHAMP_FLUTE not Loaded yet")
			ENDIF
		ENDIF
		
		
		
		
		
		IF SWIFT2 != GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
		AND NOT IS_PLAYER_IN_LUX_HELI_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
			
			
			
			IF NOT IS_BACK_FACING_SEAT(luxeActStruct, activitySeat)
				IF NOT DOES_ENTITY_EXIST(activitySeat.propChampBotIdle)
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: REQUESTING propChampBotIdle")
					IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")))

					
							activitySeat.propChampBotIdle = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")), activitySeat.vChampBotPropPos, FALSE, FALSE)

							ATTACH_ENTITY_TO_ENTITY(activitySeat.propChampBotIdle, luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vChampBotPropPos, activitySeat.vChampBotPropRot)
							SET_ENTITY_INVINCIBLE(activitySeat.propChampBotIdle, TRUE)
							SET_DISABLE_FRAG_DAMAGE(activitySeat.propChampBotIdle, TRUE)

							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: CREATING propChampBotIdle, position: ", activitySeat.vChampBotPropPos, " for seat: ", activitySeat.sSeat)
						
					ELSE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: PROP_CHAMP_FLUTE not Loaded yet")
					ENDIF

				ENDIF
			
			ELSE
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: Seat is not back facing, not creating Champagne bottle")
			ENDIF
		ELSE
			IF NOT IS_PLAYER_IN_DRIVER_SIDE_SEAT(luxeActStruct, activitySeat)
				IF NOT DOES_ENTITY_EXIST(activitySeat.propChampBotIdle)

					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: REQUESTING propChampBotIdle")
					IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")))

						
						
							activitySeat.propChampBotIdle = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CHAMP_01_LUXE")), activitySeat.vChampBotPropPos, FALSE, FALSE)


							ATTACH_ENTITY_TO_ENTITY(activitySeat.propChampBotIdle, luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vChampBotPropPos, activitySeat.vChampBotPropRot)
							SET_ENTITY_INVINCIBLE(activitySeat.propChampBotIdle, TRUE)
							SET_DISABLE_FRAG_DAMAGE(activitySeat.propChampBotIdle, TRUE)


							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: CREATING propChampBotIdle, position: ", activitySeat.vChampBotPropPos, " for seat: ", activitySeat.sSeat)
						

					ELSE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: PROP_CHAMP_FLUTE not Loaded yet")
					ENDIF

				ENDIF
			
				
			ELSE
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: Seat is not back facing, not creating Champagne bottle")
			ENDIF
		ENDIF
		
		IF SWIFT2 != GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
		AND NOT IS_PLAYER_IN_LUX_HELI_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
			
			IF NOT DOES_ENTITY_EXIST(activitySeat.propCigarIdle)

				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: REQUESTING propCigarIdle") 
				IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CIGAR_01_LUXE")))

					
					
						activitySeat.propCigarIdle = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_PROP_CIGAR_01_LUXE")), activitySeat.vCigarPropPos, FALSE, FALSE)

						ATTACH_ENTITY_TO_ENTITY(activitySeat.propCigarIdle, luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vCigarPropPos, activitySeat.vCigarPropRot)
						SET_ENTITY_INVINCIBLE(activitySeat.propCigarIdle, TRUE)


						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: CREATING propCigarIdle, position: ", activitySeat.vCigarPropPos)
					

				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: PROP_CIGAR_01 not loaded yet")
				ENDIF

			ENDIF
			
			IF NOT IS_BACK_FACING_SEAT(luxeActStruct, activitySeat)
				IF NOT DOES_ENTITY_EXIST(activitySeat.propAshtray)

					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: REQUESTING propAshtray")
					IF REQUEST_LOAD_MODEL(GET_ASHTRAY_MODEL(luxeActStruct))
						
						activitySeat.propAshtray = CREATE_OBJECT_NO_OFFSET(GET_ASHTRAY_MODEL(luxeActStruct), activitySeat.vAshtrayPropPos, FALSE, FALSE)

						ATTACH_ENTITY_TO_ENTITY(activitySeat.propAshtray, luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat), activitySeat.vAshtrayPropPos, activitySeat.vAshtrayPropRot)
						SET_ENTITY_INVINCIBLE(activitySeat.propAshtray, TRUE)


						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: CREATING propAshtray, position: ", activitySeat.vAshtrayPropPos)
						

					ELSE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: LUX_PROP_ASHTRAY_LUXE_01 not loaded yet")
					ENDIF

				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
		
			IF NOT IS_BACK_FACING_SEAT(luxeActStruct, activitySeat)
				IF DOES_ENTITY_EXIST(activitySeat.propChampFluteIdle)
				AND DOES_ENTITY_EXIST(activitySeat.propChampBotIdle)
				AND DOES_ENTITY_EXIST(activitySeat.propAshtray)
				AND DOES_ENTITY_EXIST(activitySeat.propCigarIdle)
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: ALL PROPS CREATED for Luxor2")

					RETURN TRUE
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(activitySeat.propChampFluteIdle)
				AND DOES_ENTITY_EXIST(activitySeat.propCigarIdle)
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: ALL PROPS CREATED for Luxor2")

					
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PLAYER_IN_DRIVER_SIDE_SEAT(luxeActStruct, activitySeat)
				IF DOES_ENTITY_EXIST(activitySeat.propChampFluteIdle)
				AND DOES_ENTITY_EXIST(activitySeat.propChampBotIdle)
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: PASS_SIDE: ALL PROPS CREATED for Luxor2")

					RETURN TRUE
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(activitySeat.propChampFluteIdle)
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: DRIVE_SIDE: ALL PROPS CREATED for Luxor2")

					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		
	ENDIF
	
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CREATE_LOCAL_DUMMY_PROPS: NOT ALL PROPS CREATED")
	RETURN FALSE
ENDFUNC

FUNC BOOL DESTROY_LOCAL_DUMMY_PROPS( SEAT_DATA &activitySeat)
	
		
	
	IF DOES_ENTITY_EXIST(activitySeat.propChampFluteIdle)
		CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: propChampFluteIdle")
		DELETE_ENTITY(activitySeat.propChampFluteIdle)
	ENDIF
	
	IF DOES_ENTITY_EXIST(activitySeat.propCigarIdle)
		CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: propCigarIdle")
		DELETE_ENTITY(activitySeat.propCigarIdle)
	ENDIF
	
	IF DOES_ENTITY_EXIST(activitySeat.propAshtray)
		CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: propAshtray")
		DELETE_ENTITY(activitySeat.propAshtray)
	ENDIF
	
	IF DOES_ENTITY_EXIST(activitySeat.propChampBotIdle)
		CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: propChampBotIdle")
		DELETE_ENTITY(activitySeat.propChampBotIdle)
	ENDIF
		
	
	IF NOT DOES_ENTITY_EXIST(activitySeat.propChampFluteIdle)
	AND NOT DOES_ENTITY_EXIST(activitySeat.propChampBotIdle)
	AND NOT DOES_ENTITY_EXIST(activitySeat.propAshtray)
	AND NOT DOES_ENTITY_EXIST(activitySeat.propCigarIdle)
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " all props cleaned up")
		RETURN TRUE
	ELSE
		IF NOT DOES_ENTITY_EXIST(activitySeat.propChampFluteIdle)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: activitySeat.propChampFluteIdle not cleaned up ")
		ENDIF
		IF NOT DOES_ENTITY_EXIST(activitySeat.propChampBotIdle)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: activitySeat.propChampBotIdle not cleaned up ")
		ENDIF
		IF NOT DOES_ENTITY_EXIST(activitySeat.propAshtray)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: activitySeat.propAshtray not cleaned up ")
		ENDIF
		IF NOT DOES_ENTITY_EXIST(activitySeat.propCigarIdle)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: activitySeat.propCigarIdle not cleaned up ")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP(LUXE_ACT_STRUCT &luxeActStruct, SEAT_DATA &activitySeat) 
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
		IF NOT IS_BACK_FACING_SEAT(luxeActStruct, activitySeat)
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propAshtray)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarIdle)
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " all props cleaned up")
				RETURN TRUE
			ENDIF
		ELSE
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
	//		AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
	//		AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propAshtray)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarIdle)
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " all props cleaned up")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BACK_FACING_SEAT(luxeActStruct, activitySeat)
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " all props cleaned up")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteAnim)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propChampFluteAnim)
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: propChampFluteAnim")
				
				DELETE_NET_ID(activitySeat.propChampFluteAnim)
				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(activitySeat.propChampFluteAnim)
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propChampFluteAnim")
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampFluteIdle)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propChampFluteIdle)
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: propChampFluteIdle")
				
				DELETE_NET_ID(activitySeat.propChampFluteIdle)
				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(activitySeat.propChampFluteIdle)
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propChampFluteIdle")
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarAnim)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propCigarAnim)
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: propCigarAnim")
				
				DELETE_NET_ID(activitySeat.propCigarAnim)
				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(activitySeat.propCigarAnim)
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propCigarAnim")
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarIdle)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propCigarIdle)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propCigarIdle)
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: propCigarIdle")
				
				DELETE_NET_ID(activitySeat.propCigarIdle)
				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(activitySeat.propCigarIdle)
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propCigarIdle")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BACK_FACING_SEAT(luxeActStruct, activitySeat)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propAshtray)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propAshtray)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propAshtray)
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: propAshtray")
					
					DELETE_NET_ID(activitySeat.propAshtray)
					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
				ELSE
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propAshtray")
					TAKE_CONTROL_OF_NET_ID(activitySeat.propAshtray)
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotAnim)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propChampBotAnim)
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: propChampBotAnim")
					
					DELETE_NET_ID(activitySeat.propChampBotAnim)
					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
				ELSE
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propChampBotAnim")
					TAKE_CONTROL_OF_NET_ID(activitySeat.propChampBotAnim)
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activitySeat.propChampBotIdle)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activitySeat.propChampBotIdle)
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: propChampBotIdle")
					
					DELETE_NET_ID(activitySeat.propChampBotIdle)
					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
				ELSE
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT: CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propChampBotIdle")
					TAKE_CONTROL_OF_NET_ID(activitySeat.propChampBotIdle)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//FUNC BOOL CLEANUP_NEW_LUSE_ACTIVITY_PROPS(SERVER_BROADCAST_DATA &serverBD) 
//	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propChampFluteAnim)
//	AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propChampFluteIdle)
//	AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propChampBotAnim)
//	AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propAshtray)
//	AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propCigarAnim)
//		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_NEW_ACTIVITY_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " all props cleaned up")
//		RETURN TRUE
//	ENDIF
//	
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propChampFluteAnim)
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propChampFluteAnim)
//			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.propChampFluteAnim)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_NEW_ACTIVITY_PROPS: propChampFluteAnim")
//				
//				DELETE_NET_ID(serverBD.propChampFluteAnim)
//				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
//			ELSE
//				TAKE_CONTROL_OF_NET_ID(serverBD.propChampFluteAnim)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_NEW_ACTIVITY_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propChampFluteAnim")
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propChampFluteIdle)
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propChampFluteIdle)
//			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.propChampFluteIdle)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_NEW_ACTIVITY_PROPS: propChampFluteIdle")
//				
//				DELETE_NET_ID(serverBD.propChampFluteIdle)
//				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
//			ELSE
//				TAKE_CONTROL_OF_NET_ID(serverBD.propChampFluteIdle)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_NEW_ACTIVITY_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propChampFluteAnim")
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propCigarAnim)
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propCigarAnim)
//			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.propCigarAnim)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_NEW_ACTIVITY_PROPS: propCigarAnim")
//				
//				DELETE_NET_ID(serverBD.propCigarAnim)
//				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
//			ELSE
//				TAKE_CONTROL_OF_NET_ID(serverBD.propCigarAnim)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_NEW_ACTIVITY_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propCigarAnim")
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propAshtray)
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propAshtray)
//			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.propAshtray)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_NEW_ACTIVITY_PROPS: propAshtray")
//				
//				DELETE_NET_ID(serverBD.propAshtray)
//				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
//			ELSE
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_NEW_ACTIVITY_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propAshtray")
//				TAKE_CONTROL_OF_NET_ID(serverBD.propAshtray)
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propChampBotAnim)
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.propChampBotAnim)
//			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.propChampBotAnim)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_NEW_ACTIVITY_PROPS: propChampBotAnim")
//				
//				DELETE_NET_ID(serverBD.propChampBotAnim)
//				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()- 1)
//			ELSE
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_NEW_ACTIVITY_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of propChampBotAnim")
//				TAKE_CONTROL_OF_NET_ID(serverBD.propChampBotAnim)
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

//FUNC BOOL GET_PLAYER_VEH_CONTROL()
//	
//	IF NETWORK_HAS_CONTROL_OF_ENTITY(luxeActStruct.vehicleIndex)
//		IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(luxeActStruct.vehicleIndex)
//			IF LUXOR2 = GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
//			OR SWIFT2 = GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
//																				
////					luxeActStruct.oVeh = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_FROM_PED_OR_VEHICLE(luxeActStruct.vehicleIndex))
//				
//				//luxeActStruct.niVehIndex = VEH_TO_NET(luxeActStruct.vehicleIndex)
////					luxeActStruct.vFlutePropPos = <<1,1,1>>
//				RETURN TRUE
//			ENDIF
//		ELSE
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: registering vehicle with this script")
//			SET_ENTITY_AS_MISSION_ENTITY(luxeActStruct.vehicleIndex, FALSE, TRUE)
//		ENDIF
//	ELSE
//		NETWORK_REQUEST_CONTROL_OF_ENTITY(luxeActStruct.vehicleIndex)
//		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: requesting vehicle control")
//	ENDIF
//	RETURN FALSE
//ENDFUNC


//FUNC BOOL GRAB_VEHICLE(VEHICLE_INDEX vehicle)
//	IF NETWORK_HAS_CONTROL_OF_ENTITY(vehicle)
//		IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(luxeActStruct.vehicleIndex)
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: vehicle has been registered with this script")
//			RETURN TRUE
//		ELSE
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: registering vehicle with this script")
//			SET_ENTITY_AS_MISSION_ENTITY(vehicle, FALSE, TRUE)
//		ENDIF
//	ELSE
//		NETWORK_REQUEST_CONTROL_OF_ENTITY(vehicle)
//		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: requesting vehicle control")
//	ENDIF	
//	RETURN FALSE
//ENDFUNC

//FUNC BOOL CHECK_IF_PLAYER_IS_IN_LUXE_ACT_VEH2(NETWORK_INDEX niLuxor2)//LUXE_ACT_STRUCT &luxeActStruct)
//
//	IF playerBD[NATIVE_TO_INT(PLAYER_ID())].niLuxor2 != NULL
//		IF NET_TO_VEH(playerBD[NATIVE_TO_INT(PLAYER_ID())].niLuxor2) = luxeActStruct.vehicleIndex
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

//FUNC BOOL CHECK_IF_PLAYER_IS_IN_LUXE_ACT_VEH()//LUXE_ACT_STRUCT &luxeActStruct)
//	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
//		VEHICLE_INDEX vehicle = luxeActStruct.vehicleIndex
//		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehicle)
//			
//			IF LUXOR2 = GET_ENTITY_MODEL(vehicle)
//				
//				RETURN TRUE
//			ELIF SWIFT2 = GET_ENTITY_MODEL(vehicle)
//				
//
//				RETURN TRUE
//			ENDIF
//			
//		ELSE
//			NETWORK_REQUEST_CONTROL_OF_ENTITY(vehicle)
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: requesting vehicle control")
//		ENDIF
//	ELSE
////		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: player is not in any vehicle or entering any vehicle")
//	ENDIF
//
//	RETURN FALSE
//ENDFUNC

//IF DECOR_EXIST_ON(NET_TO_VEH(playerBD[NATIVE_TO_INT(PLAYER_ID())].niLuxor2), "LUXE_VEH_INSTANCE_ID")
//	//RETURN DECOR_GET_INT(NET_TO_VEH(playerBD[NATIVE_TO_INT(PLAYER_ID())].niLuxor2), "LUXE_VEH_INSTANCE_ID")
//	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "AM_VEHICLE_SPAWN: Decor has instance id: ", DECOR_GET_INT(NET_TO_VEH(playerBD[NATIVE_TO_INT(PLAYER_ID())].niLuxor2), "LUXE_VEH_INSTANCE_ID"))
//ELSE
//	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "AM_VEHICLE_SPAWN: doesn't have instance id")
//	//RETURN -1
//ENDIF

//FUNC INT CHECK_FOR_LUXE_VEH_INSTANCE_ID(LUXE_ACT_STRUCT &luxeActStruct)
//	
//		//luxeActStruct.niVehNetID = VEH_TO_NET(luxeActStruct.vehicleIndex)
//						
//		IF DECOR_EXIST_ON(NET_TO_VEH(luxeActStruct.niVehNetID), "LUXE_VEH_INSTANCE_ID")
//			RETURN DECOR_GET_INT(NET_TO_VEH(luxeActStruct.niVehNetID), "LUXE_VEH_INSTANCE_ID")
//		ELSE
//			RETURN -1
//		ENDIF
//		
//ENDFUNC


/// PURPOSE:
///    Check if the passed in vehicle has the decorator "LUXE_VEH_INSTANCE_ID", and if so, get the value attached.
/// RETURNS:
///    INT - Value stored in decor, -1 if decor doesn't exist.
//FUNC INT CHECK_FOR_LUXE_VEH_INSTANCE_ID(VEHICLE_INDEX vehicleIndex)
//	
//	//luxeActStruct.niVehNetID = VEH_TO_NET(luxeActStruct.vehicleIndex)
//					
//	IF DECOR_EXIST_ON(vehicleIndex, "LUXE_VEH_INSTANCE_ID")
//		RETURN DECOR_GET_INT(vehicleIndex, "LUXE_VEH_INSTANCE_ID")
//	ELSE
//		RETURN -1
//	ENDIF
//		
//ENDFUNC
//
//
///// PURPOSE:
/////    Locate and remove the decorator "LUXE_VEH_INSTANCE_ID" from the passed in vehicle index.
//PROC CLEAR_LUXE_VEH_INSTANCE_ID(VEHICLE_INDEX vehicleIndex)
//					
//	IF DECOR_EXIST_ON(vehicleIndex, "LUXE_VEH_INSTANCE_ID")
//		DECOR_REMOVE(vehicleIndex, "LUXE_VEH_INSTANCE_ID")
//		CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CLEAR_LUXE_VEH_INSTANCE_ID: Removed decorator: LUXE_VEH_INSTANCE_ID, from vehicle idx: ", NATIVE_TO_INT(vehicleIndex))
//	ELSE
//		CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CLEAR_LUXE_VEH_INSTANCE_ID: No decorator found for removal. Vehicle idx: ", NATIVE_TO_INT(vehicleIndex))
//	ENDIF
//		
//ENDPROC

// check if we need  a new instance id
PROC MANAGE_LUXE_JET_SCRIPT(LUXE_ACT_STRUCT &luxeActStruct)

	SWITCH luxeActStruct.luxeActivityScriptState
		CASE CL_LUXE_ACT_CHECK_IN_VEH	
//			IS_NETWORK_ID_OWNED_BY_PARTICIPANT(luxeActStruct.niVehNetID)
			
			//CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "MAINTAIN_LUXE_VEH_ACTIVITIES: Launching script with niVehNetID: ", NATIVE_TO_INT(luxeActStruct.niVehNetID))
			luxeActStruct.vehModel = GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
			SetLuxScriptActStage(luxeActStruct, CL_LUXE_ACT_LAUNCH_SCRIPT)
						

		BREAK
		CASE CL_LUXE_ACT_LAUNCH_SCRIPT
			REQUEST_SCRIPT("luxe_veh_activity")

			IF HAS_SCRIPT_LOADED("luxe_veh_activity")

			    START_NEW_SCRIPT_WITH_ARGS("luxe_veh_activity", luxeActStruct, SIZE_OF(luxeActStruct), DEFAULT_STACK_SIZE)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "MAINTAIN_LUXE_VEH_ACTIVITIES: Launching script with niVehNetID: ", NATIVE_TO_INT(luxeActStruct.niVehNetID))
			    SET_SCRIPT_AS_NO_LONGER_NEEDED("luxe_veh_activity")
				SetLuxScriptActStage(luxeActStruct, CL_LUXE_ACT_SCRIPT_LAUNCHED)
			ENDIF
		BREAK
		CASE CL_LUXE_ACT_SCRIPT_LAUNCHED
			IF NOT NETWORK_IS_SCRIPT_ACTIVE("luxe_veh_activity", luxeActStruct.iLuxeVehInstanceID)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "AM_VEHICLE_SPAWN: RUN_VEH_ACTIVITY: Player has exited vehicle, going to CHECK_FOR_VEH_WITH_INSTANCE_IDS")
//				luxeActStruct.iLuxeVehInstanceID = -1
//				luxeActStruct.luxeActivityScriptState = CL_LUXE_ACT_CHECK_IN_VEH
////							myLuxeVehInstanceID = -1
//				playerBD[NATIVE_TO_INT(PLAYER_ID())].playerHasTakenLuxeVehID = FALSE
//				luxActGenerationState = CHECK_FOR_VEH_WITH_INSTANCE_IDS
//				
//				SetLuxScriptActStage(luxeActStruct, CL_LUXE_ACT_CHECK_IN_VEH)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC STRING GET_SEAT_NAME_FROM_ENUM(VEHICLE_SEAT seat, LUXE_ACT_STRUCT &luxeActStruct)
	
	STRING sReturn
	
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)

		SWITCH seat 
			CASE VS_DRIVER
				sReturn = "seat_dside_f"
			BREAK
			CASE VS_FRONT_RIGHT
				sReturn = "seat_pside_f"
			BREAK
			CASE VS_BACK_LEFT
				sReturn = "seat_dside_r"
			BREAK
			CASE VS_BACK_RIGHT
				sReturn = "seat_pside_r1"
			BREAK
			CASE VS_EXTRA_LEFT_1
				sReturn = "seat_dside_r1"
			BREAK
			CASE VS_EXTRA_RIGHT_1
				sReturn = "seat_pside_r"
			BREAK
	//		CASE VS_EXTRA_LEFT_2
	////			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: Hack, thinks we're in VS_EXTRA_LEFT_2 returning seat_pside_r2")
	//			sReturn = "seat_pside_r2"
	//		BREAK
			CASE VS_EXTRA_RIGHT_2
				sReturn = "seat_pside_r2"
			BREAK
			CASE VS_EXTRA_RIGHT_3
				sReturn = "seat_pside_r3"
			BREAK
			DEFAULT
				sReturn = "NULL"
			BREAK
		ENDSWITCH
	ELSE

		SWITCH seat 
			CASE VS_DRIVER
				sReturn = "seat_dside_f"
			BREAK
			CASE VS_FRONT_RIGHT
				sReturn = "seat_pside_f"
			BREAK
			CASE VS_BACK_LEFT
				sReturn = "seat_dside_r"
			BREAK
			CASE VS_BACK_RIGHT
				sReturn = "seat_pside_r"
			BREAK
			CASE VS_EXTRA_LEFT_1
				sReturn = "seat_dside_r1"
			BREAK
			CASE VS_EXTRA_RIGHT_1
				sReturn = "seat_pside_r1"
			BREAK
	//		CASE VS_EXTRA_LEFT_2
	////			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: Hack, thinks we're in VS_EXTRA_LEFT_2 returning seat_pside_r2")
	//			sReturn = "seat_pside_r2"
	//		BREAK
			CASE VS_EXTRA_RIGHT_2
				sReturn = "seat_pside_r2"
			BREAK
			CASE VS_EXTRA_RIGHT_3
				sReturn = "seat_pside_r3"
			BREAK
			DEFAULT
				sReturn = "NULL"
			BREAK
		ENDSWITCH
	ENDIF
	CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "GET_SEAT_NAME_FROM_ENUM: returning: ", sReturn)
	RETURN sReturn
ENDFUNC
FUNC VEHICLE_SEAT GET_VEHICLE_SEAT(LUXE_ACT_STRUCT &luxeActStruct)
	INT iSeat
	BOOL bSeatFound = FALSE
//	seat = VS_BACK_LEFT 
	VEHICLE_SEAT returnVehSeat
	returnVehSeat = INT_TO_ENUM(VEHICLE_SEAT, -3)
	iSeat = ENUM_TO_INT(VS_DRIVER)
	WHILE iSeat < ENUM_TO_INT(VS_EXTRA_RIGHT_3) 
	AND bSeatFound = FALSE
		
		IF PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
			returnVehSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat)
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: player is in VEHICLE_SEAT seat = ", iSeat, ", which is seat bone: ", GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, iSeat), luxeActStruct))
//			luxeActStruct.sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, iSeat))
			bSeatFound = TRUE
		ENDIF
		iSeat++
	ENDWHILE
//	CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: player is in seat = ", ENUM_TO_INT(returnVehSeat))
	RETURN returnVehSeat
ENDFUNC
FUNC BOOL IS_PLAYER_IN_ACTIVITY_SEAT(LUXE_ACT_STRUCT &luxeActStruct)
	SWITCH GET_VEHICLE_SEAT(luxeActStruct)  
		CASE VS_ANY_PASSENGER
		CASE VS_DRIVER
		CASE VS_FRONT_RIGHT
		DEFAULT
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_IN_ACTIVITY_SEAT: player is not in an activity seat")
			RETURN FALSE
		 
		CASE VS_BACK_LEFT
		CASE VS_BACK_RIGHT
		CASE VS_EXTRA_LEFT_1
		CASE VS_EXTRA_RIGHT_1
		CASE VS_EXTRA_LEFT_2
		CASE VS_EXTRA_RIGHT_2
		CASE VS_EXTRA_LEFT_3
		CASE VS_EXTRA_RIGHT_3
			RETURN TRUE
			
	ENDSWITCH
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_PLAYER_IN_COCKPIT(LUXE_ACT_STRUCT &luxeActStruct)
	SWITCH GET_VEHICLE_SEAT(luxeActStruct)  
		CASE VS_ANY_PASSENGER
		CASE VS_DRIVER
		CASE VS_FRONT_RIGHT 
		DEFAULT
			RETURN TRUE
			
		CASE VS_BACK_LEFT
		CASE VS_BACK_RIGHT
		CASE VS_EXTRA_LEFT_1
		CASE VS_EXTRA_RIGHT_1
		CASE VS_EXTRA_LEFT_2
		CASE VS_EXTRA_RIGHT_2
		CASE VS_EXTRA_LEFT_3
		CASE VS_EXTRA_RIGHT_3
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_IN_COCKPIT: player is not in a cockpit seat")
			RETURN FALSE
			
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC INT GET_TABLE_NUMBER(LUXE_ACT_STRUCT &luxeActStruct, VEHICLE_SEAT vehicleSeat)
	
	INT iTableReturn
	iTableReturn = -1
	
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
		SWITCH vehicleSeat
			CASE VS_ANY_PASSENGER
			CASE VS_DRIVER
			CASE VS_FRONT_RIGHT 
			DEFAULT
				iTableReturn = -1
			BREAK
			CASE VS_BACK_LEFT
			CASE VS_EXTRA_LEFT_1
				iTableReturn = 0
			BREAK
			CASE VS_BACK_RIGHT
			CASE VS_EXTRA_RIGHT_1
				iTableReturn = 1
			BREAK
			CASE VS_EXTRA_RIGHT_2
			CASE VS_EXTRA_RIGHT_3
				iTableReturn = 2
			BREAK 
		ENDSWITCH
	ELSE
		SWITCH vehicleSeat
			CASE VS_ANY_PASSENGER
			CASE VS_DRIVER
			CASE VS_FRONT_RIGHT 
			DEFAULT
				iTableReturn = -1
			BREAK
			CASE VS_BACK_LEFT
			CASE VS_EXTRA_LEFT_1
			CASE VS_BACK_RIGHT
			CASE VS_EXTRA_RIGHT_1
			CASE VS_EXTRA_RIGHT_2
			CASE VS_EXTRA_RIGHT_3
				iTableReturn = 0
			BREAK 
		ENDSWITCH
		
	ENDIF
	CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "GET_TABLE_NUMBER: returning table: ", iTableReturn)
	RETURN iTableReturn
ENDFUNC
FUNC INT GET_NUMBER_OF_PASSENGER_SEATS(LUXE_ACT_STRUCT &luxeActStruct) 
	INT iMaxSeats = -1
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
		iMaxSeats = 6 
	ELSE
		iMaxSeats = 2 
	ENDIF
	RETURN iMaxSeats
ENDFUNC
FUNC INT GET_LAST_VEHICLE_SEAT(LUXE_ACT_STRUCT &luxeActStruct) 
	INT iMaxSeats = -1
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
		iMaxSeats = ENUM_TO_INT(VS_EXTRA_RIGHT_3) 
	ELSE
		iMaxSeats = ENUM_TO_INT(VS_EXTRA_RIGHT_3) 
	ENDIF
	RETURN iMaxSeats
ENDFUNC
PROC SET_ALL_PLAYERS_SEAT_DATA(LUXE_ACT_STRUCT &luxeActStruct)
	INT iSeat
//	seat = VS_BACK_LEFT 
	iSeat = ENUM_TO_INT(VS_DRIVER)
	INT iMaxSeat
	STRING sSeatString
	iMaxSeat = GET_LAST_VEHICLE_SEAT(luxeActStruct)

	WHILE iSeat < iMaxSeat 
		
		IF GET_PED_IN_VEHICLE_SEAT(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat)) != INT_TO_NATIVE(PED_INDEX, 0)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_ALL_PLAYERS_SEAT_DATA: GET_PED_IN_VEHICLE_SEAT: seat: ", iSeat, ", player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat)))))
			
			VEHICLE_SEAT vehicleSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat)
			IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
				IF INT_TO_ENUM(VEHICLE_SEAT, iSeat) = VS_EXTRA_LEFT_2
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_ALL_PLAYERS_SEAT_DATA: player thinks there in left_2, switching to right_2")
					vehicleSeat = VS_EXTRA_RIGHT_2
				ELIF INT_TO_ENUM(VEHICLE_SEAT, iSeat) = VS_EXTRA_RIGHT_2
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_ALL_PLAYERS_SEAT_DATA: player thinks there in right_2, switching to right_3")
					vehicleSeat = VS_EXTRA_RIGHT_3
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_ALL_PLAYERS_SEAT_DATA: no hack necessary")
				ENDIF
			ENDIF
			
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: player is in seat = ", iSeat, ", which is seat bone: ", GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, iSeat)))
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SET_ALL_PLAYERS_SEAT_DATA: player is in seat = ", iSeat, ", which is seat bone: ", GET_SEAT_NAME_FROM_ENUM(vehicleSeat, luxeActStruct))
			IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
				sSeatString = GET_SEAT_NAME_FROM_ENUM(vehicleSeat, luxeActStruct)
			ELSE
				IF iSeat = 2
					sSeatString = "seat_pside_r"
				ELSE
					sSeatString = GET_SEAT_NAME_FROM_ENUM(vehicleSeat, luxeActStruct)
				ENDIF
			ENDIF
			
			INT iSeatCounter = 0				
			IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
				WHILE iSeatCounter < GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct)
					IF NOT IS_STRING_NULL_OR_EMPTY(sSeatString)
					AND NOT IS_STRING_NULL_OR_EMPTY(luxeActStruct.activitySeats[iSeatCounter].sSeat)
						
						IF ARE_STRINGS_EQUAL(sSeatString, luxeActStruct.activitySeats[iSeatCounter].sSeat)
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_ALL_PLAYERS_SEAT_DATA: strings are not null sSeatString: ", sSeatString, ", player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat)))),", activitySeats[", iSeatCounter,"].sSeat = ", luxeActStruct.activitySeats[iSeatCounter].sSeat)	
							luxeActStruct.activitySeats[iSeatCounter].piSeatedPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat)))

							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_ALL_PLAYERS_SEAT_DATA: strings are equal, iSeatID: ", luxeActStruct.iSeatID)
						ELSE
		//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: strings are not equal")
						ENDIF
					ELSE
						//CASSERTLN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: NULL: luxeActStruct.sSeat = ", luxeActStruct.sSeat, ", activitySeats[", iSeat,"].sSeat = ", serverBD.activitySeats[iSeat].sSeat)
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_ALL_PLAYERS_SEAT_DATA: NULL: luxeActStruct.sSeat = ", luxeActStruct.sSeat, ", luxeActStruct.activitySeats[", iSeat,"].sSeat = ", luxeActStruct.activitySeats[iSeat].sSeat)
					ENDIF
					iSeatCounter++
				ENDWHILE
			ELSE
				IF ARE_STRINGS_EQUAL(sSeatString, "seat_dside_r")
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_ALL_PLAYERS_SEAT_DATA: strings are not null sSeatString: ", sSeatString, ", player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat)))),", activitySeats[", iSeatCounter,"].sSeat = seat_dside_r")	
					luxeActStruct.activitySeats[0].piSeatedPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat)))
//					luxeActStruct.iSeatID = 0
//					playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID = luxeActStruct.iSeatID
//					bAssignedSeat = TRUE
				ELIF ARE_STRINGS_EQUAL(sSeatString, "seat_pside_r")
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_ALL_PLAYERS_SEAT_DATA: strings are not null sSeatString: ", sSeatString, ", player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat)))),", activitySeats[", iSeatCounter,"].sSeat = seat_pside_r")	
					luxeActStruct.activitySeats[1].piSeatedPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat)))
//					luxeActStruct.iSeatID = 1
//					playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID = luxeActStruct.iSeatID
//					bAssignedSeat = TRUE
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_ALL_PLAYERS_SEAT_DATA: strings are not null, can't match luxeActStruct.sSeat: ", luxeActStruct.sSeat)
				ENDIF
			ENDIF
			sSeatString = ""
		ENDIF
		iSeat++
	ENDWHILE
ENDPROC


PROC SET_PLAYER_SEAT_AND_TABLE_DATA(LUXE_ACT_STRUCT &luxeActStruct, PLAYER_BROADCAST_DATA &playerBD[])

	INT iSeat
	BOOL bSeatFound = FALSE
//	seat = VS_BACK_LEFT 
	iSeat = ENUM_TO_INT(VS_DRIVER)
	INT iMaxSeat
	
	iMaxSeat = GET_LAST_VEHICLE_SEAT(luxeActStruct)
//	IF luxeActStruct.vehModel = LUXOR2
		WHILE iSeat < iMaxSeat 
		AND bSeatFound = FALSE
			
			IF PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
				VEHICLE_SEAT vehicleSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat)
				IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
					IF INT_TO_ENUM(VEHICLE_SEAT, iSeat) = VS_EXTRA_LEFT_2
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: player thinks there in left_2, switching to right_2")
						vehicleSeat = VS_EXTRA_RIGHT_2
					ELIF INT_TO_ENUM(VEHICLE_SEAT, iSeat) = VS_EXTRA_RIGHT_2
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: player thinks there in right_2, switching to right_3")
						vehicleSeat = VS_EXTRA_RIGHT_3
					ELSE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: no hack necessary")
					ENDIF
				ENDIF
				
				playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber = GET_TABLE_NUMBER(luxeActStruct, vehicleSeat)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: player is in seat = ", iSeat, ", which is seat bone: ", GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, iSeat)))
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: player is in seat = ", iSeat, ", which is seat bone: ", GET_SEAT_NAME_FROM_ENUM(vehicleSeat, luxeActStruct), ", table: ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber)
				IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
					luxeActStruct.sSeat = GET_SEAT_NAME_FROM_ENUM(vehicleSeat, luxeActStruct)
				ELSE
					IF iSeat = 2
						luxeActStruct.sSeat = "seat_pside_r"
					ELSE
						luxeActStruct.sSeat = GET_SEAT_NAME_FROM_ENUM(vehicleSeat, luxeActStruct)
					ENDIF
				ENDIF
				bSeatFound = TRUE
			ENDIF
			iSeat++
		ENDWHILE

	iSeat = 0
	BOOL bAssignedSeat
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
		WHILE iSeat < GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct)
			IF NOT IS_STRING_NULL_OR_EMPTY(luxeActStruct.sSeat)
			AND NOT IS_STRING_NULL_OR_EMPTY(luxeActStruct.activitySeats[iSeat].sSeat)
				
				IF ARE_STRINGS_EQUAL(luxeActStruct.sSeat, luxeActStruct.activitySeats[iSeat].sSeat)
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: strings are not null luxeActStruct.sSeat: ", luxeActStruct.sSeat, ", activitySeats[", iSeat,"].sSeat = ", luxeActStruct.activitySeats[iSeat].sSeat)	
					luxeActStruct.iSeatID = iSeat
					playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID = luxeActStruct.iSeatID
					bAssignedSeat = TRUE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: strings are equal, iSeatID: ", luxeActStruct.iSeatID)
				ELSE
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: strings are not equal")
				ENDIF
			ELSE
				//CASSERTLN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: NULL: luxeActStruct.sSeat = ", luxeActStruct.sSeat, ", activitySeats[", iSeat,"].sSeat = ", serverBD.activitySeats[iSeat].sSeat)
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: NULL: luxeActStruct.sSeat = ", luxeActStruct.sSeat, ", luxeActStruct.activitySeats[", iSeat,"].sSeat = ", luxeActStruct.activitySeats[iSeat].sSeat)
			ENDIF
			iSeat++
		ENDWHILE
	ELSE
		IF ARE_STRINGS_EQUAL(luxeActStruct.sSeat, "seat_dside_r")
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: strings are not null luxeActStruct.sSeat: ", luxeActStruct.sSeat, ", activitySeats[", 0,"].sSeat = seat_dside_r")
			luxeActStruct.iSeatID = 0
			playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID = luxeActStruct.iSeatID
			bAssignedSeat = TRUE
		ELIF ARE_STRINGS_EQUAL(luxeActStruct.sSeat, "seat_pside_r")
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: strings are not null luxeActStruct.sSeat: ", luxeActStruct.sSeat, ", activitySeats[", 1,"].sSeat = seat_pside_r")
			luxeActStruct.iSeatID = 1
			playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID = luxeActStruct.iSeatID
			bAssignedSeat = TRUE
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: strings are not null, can't match luxeActStruct.sSeat: ", luxeActStruct.sSeat)
		ENDIF
	ENDIF
	IF NOT bAssignedSeat
		luxeActStruct.iSeatID = -1
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID = luxeActStruct.iSeatID
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: no seat assigned reset to: ", luxeActStruct.iSeatID)
	ENDIF
ENDPROC

PROC START_SMOKE_EFFECT(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct)
	
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim)
		IF luxeActStruct.bCigarAnimSmokeFXStarted = FALSE			
	//			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_bong_smoke", PLAYER_PED_ID(), << -0.025, 0.13, 0.0 >>, <<0,0,0>>, BONETAG_HEAD)	
			USE_PARTICLE_FX_ASSET("scr_mp_cig_plane")
			luxeActStruct.ptfxIDCigar = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("ent_anim_cig_smoke_plane", NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim), <<0.004,0,0>>, <<0,0,0>>, GET_ENTITY_BONE_INDEX_BY_NAME(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim), "VFX_Emitter"))
			FORCE_PARTICLE_FX_IN_VEHICLE_INTERIOR(luxeActStruct.ptfxIDCigar , TRUE)
//			SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "light_intensity", 1.0)
//			SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "smoke_fade", 1.0)
	//			START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sh_cig_smoke", GET_SYNCHED_SCENE_OBJECT(), vCigSmokePos, <<0,0,0>>)
			//START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_bong_smoke", PLAYER_PED_ID(), << -0.025, 0.13, 0.0 >>, <<0,0,0>>, BONETAG_HEAD)	
			luxeActStruct.bCigarAnimSmokeFXStarted = TRUE
		
		ENDIF
	ELSE
		IF luxeActStruct.bCigarAnimSmokeFXStarted = TRUE	
			IF DOES_PARTICLE_FX_LOOPED_EXIST(luxeActStruct.ptfxIDCigar)
				STOP_PARTICLE_FX_LOOPED(luxeActStruct.ptfxIDCigar)
				luxeActStruct.bCigarAnimSmokeFXStarted = FALSE
			ENDIF
		ENDIF
		CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "START_SMOKE_EFFECT: can't find anim cigar for seatID: ", luxeActStruct.iSeatID)
	ENDIF
	
	IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[luxeActStruct.iSeatID].propCigarIdle)
		IF luxeActStruct.bCigarIdleSmokeFXStarted = FALSE			
	//			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_bong_smoke", PLAYER_PED_ID(), << -0.025, 0.13, 0.0 >>, <<0,0,0>>, BONETAG_HEAD)	
			USE_PARTICLE_FX_ASSET("scr_mp_cig_plane")
			luxeActStruct.ptfxIDCigarIdle = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("ent_anim_cig_smoke_plane", luxeActStruct.activitySeats[luxeActStruct.iSeatID].propCigarIdle, <<0.004,0,0>>, <<0,0,0>>, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.activitySeats[luxeActStruct.iSeatID].propCigarIdle, "VFX_Emitter"))
			FORCE_PARTICLE_FX_IN_VEHICLE_INTERIOR(luxeActStruct.ptfxIDCigarIdle , TRUE)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigarIdle , "light_intensity", 1.0)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigarIdle , "smoke_fade", 1.0)
	//			START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sh_cig_smoke", GET_SYNCHED_SCENE_OBJECT(), vCigSmokePos, <<0,0,0>>)
			//START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sh_bong_smoke", PLAYER_PED_ID(), << -0.025, 0.13, 0.0 >>, <<0,0,0>>, BONETAG_HEAD)	
			luxeActStruct.bCigarIdleSmokeFXStarted = TRUE
		ELSE
			
		ENDIF
	ELSE
		IF luxeActStruct.bCigarIdleSmokeFXStarted = TRUE		
			IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[luxeActStruct.iSeatID].propCigarIdle)
				IF DOES_PARTICLE_FX_LOOPED_EXIST(luxeActStruct.ptfxIDCigarIdle)
					STOP_PARTICLE_FX_LOOPED(luxeActStruct.ptfxIDCigarIdle)
				ENDIF
				luxeActStruct.bCigarIdleSmokeFXStarted = FALSE
			ENDIF
		ENDIF
		CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "START_SMOKE_EFFECT: can't find Idle cigar for seatID: ", luxeActStruct.iSeatID)
	ENDIF
	
ENDPROC

//PROC SMOKE_REACT_BEGINE_INHALE(LUXE_ACT_STRUCT &luxeActStruct)
//	IF bCigarAnimSmokeFXStarted = TRUE
//		SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "light_intensity", 0.0)
//		SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "smoke_fade", 1.0)
//	ENDIF
//ENDPROC
//
//PROC SMOKE_REACT_END_INHALE(LUXE_ACT_STRUCT &luxeActStruct)
//	IF bCigarAnimSmokeFXStarted = TRUE
//		SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "light_intensity", 0.5)
//		SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "smoke_fade", 0.0)
//	ENDIF
//ENDPROC


PROC SMOKE_REACT_INHALE(LUXE_ACT_STRUCT &luxeActStruct)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(luxeActStruct.ptfxIDCigar)
		IF luxeActStruct.bCigarAnimSmokeFXStarted = TRUE
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SMOKE_REACT_INHALE: amber intensity 1")
			SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "light_intensity", 0.0)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "smoke_fade", 1.0)
		ENDIF
	ENDIF
ENDPROC

PROC SMOKE_REACT_EXHALE(LUXE_ACT_STRUCT &luxeActStruct)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(luxeActStruct.ptfxIDCigar)
		IF luxeActStruct.bCigarAnimSmokeFXStarted = TRUE
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SMOKE_REACT_INHALE: amber intensity 0")
			SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "light_intensity", 0.5)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "smoke_fade", 0.0)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL RELEASE_CHAMPAGNE_PROPS(SERVER_BROADCAST_DATA &serverBD, INT iSeatID)
	IF iSeatID > -1
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.activitySeats[iSeatID].propChampFluteAnim)
	//	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activitySeats[iSeatID].propChampFluteIdle)
		AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.activitySeats[iSeatID].propChampBotAnim)
		AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.activitySeats[iSeatID].propLiquid)
	//	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activitySeats[iSeatID].propChampBotIdle)
			
			SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[iSeatID].propChampFluteAnim, TRUE)
	//		SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[iSeatID].propChampFluteIdle, TRUE)
			SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[iSeatID].propChampBotAnim, TRUE)
	//		SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[iSeatID].propChampBotIdle, TRUE)
			SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[iSeatID].propLiquid, TRUE)
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RELEASE_CHAMPAGNE_PROPS: NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = FALSE, iSeatID: ", iSeatID)
		ENDIF
	ELSE
		CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "RELEASE_CHAMPAGNE_PROPS: INVALID, iSeatID ")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL GRAB_CIGAR_PROPS(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct)
	IF luxeActStruct.iSeatID > -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim)
				IF TAKE_CONTROL_OF_NET_ID(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter)
					IF TAKE_CONTROL_OF_NET_ID(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim)
					
						SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim, FALSE)
						SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter, FALSE)
	//					SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarIdle, FALSE)
						RETURN TRUE
					ELSE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CIGAR_PROPS: propCigarAnim TAKE_CONTROL_OF_NET_ID = FALSE ")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CIGAR_PROPS: propLighter TAKE_CONTROL_OF_NET_ID = FALSE ")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CIGAR_PROPS: propCigarAnim NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = FALSE ")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CIGAR_PROPS: propLighter NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = FALSE ")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CIGAR_PROPS: Invalid seatID")
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RELEASE_CIGAR_PROPS(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct)
	IF luxeActStruct.iSeatID > -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter))
			AND NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim))
		//		SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarIdle, TRUE)
				SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim, TRUE)
				SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter, TRUE)
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RELEASE_CIGAR_PROPS: cigar props set to migrate")
		//		RETURN TRUE
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RELEASE_CIGAR_PROPS: Cigar props don't exist anymore ")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RELEASE_CIGAR_PROPS: Invalid seatID")
	ENDIF
//	RETURN FALSE
ENDPROC

//PROC CREATE_LOCAL_LIQUID_OBJECT(LUXE_ACT_STRUCT &luxeActStruct)
//	luxeActStruct.propLiquid = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_P_Pour_champagne_LUXE")), <<0.1,0.1,0.1>>)
//	SET_ENTITY_INVINCIBLE(NET_TO_ENT(luxeActStruct.propLiquid), TRUE)
//	SET_NETWORK_ID_CAN_MIGRATE(luxeActStruct.propLiquid, FALSE)
//	
//	SET_ENTITY_VISIBLE(NET_TO_ENT(luxeActStruct.propLiquid), FALSE)
//	SET_ENTITY_COLLISION(NET_TO_OBJ(luxeActStruct.propLiquid), FALSE)
//ENDPROC
//
//PROC DESTROY_LOCAL_LIQUID_OBJECT(LUXE_ACT_STRUCT &luxeActStruct)
//	DELETE_OBJECT(luxeActStruct.propLiquid)
////	luxeActStruct.propLiquid = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("LUX_P_Pour_champagne_LUXE")), <<0.1,0.1,0.1>>)
//ENDPROC

FUNC BOOL GRAB_CHAMPAGNE_PROPS(SERVER_BROADCAST_DATA &serverBD, INT iSeatID)
	
	IF iSeatID > -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activitySeats[iSeatID].propChampFluteAnim)
	//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activitySeats[iSeatID].propChampFluteIdle)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activitySeats[iSeatID].propChampBotAnim)
	//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activitySeats[iSeatID].propChampBotIdle)
	//					IF TAKE_CONTROL_OF_NET_ID(serverBD.activitySeats[iSeatID].propChampBotIdle)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activitySeats[iSeatID].propLiquid)
								IF TAKE_CONTROL_OF_NET_ID(serverBD.activitySeats[iSeatID].propChampFluteAnim)
		//							IF TAKE_CONTROL_OF_NET_ID(serverBD.activitySeats[iSeatID].propChampFluteIdle)
										IF TAKE_CONTROL_OF_NET_ID(serverBD.activitySeats[iSeatID].propChampBotAnim)
											IF TAKE_CONTROL_OF_NET_ID(serverBD.activitySeats[iSeatID].propLiquid)
												SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[iSeatID].propChampFluteAnim, FALSE)
			//									SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[iSeatID].propChampFluteIdle, FALSE)
												SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[iSeatID].propChampBotAnim, FALSE)
			//									SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[iSeatID].propChampBotIdle, FALSE)
												SET_NETWORK_ID_CAN_MIGRATE(serverBD.activitySeats[iSeatID].propLiquid, FALSE)
												RETURN TRUE
											ELSE
												CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CHAMPAGNE_PROPS: TAKE_CONTROL_OF_NET_ID: propChampBotAnim = FALSE ", serverBD.activitySeats[iSeatID].sSeat)
											ENDIF
										ELSE
											CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CHAMPAGNE_PROPS: TAKE_CONTROL_OF_NET_ID: propChampBotAnim = FALSE ", serverBD.activitySeats[iSeatID].sSeat)
										ENDIF
		//							ELSE
		//								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CHAMPAGNE_PROPS: TAKE_CONTROL_OF_NET_ID: propChampFluteIdle = FALSE ", serverBD.activitySeats[iSeatID].sSeat)
		//							ENDIF 

								ELSE
									CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CHAMPAGNE_PROPS: TAKE_CONTROL_OF_NET_ID: propChampFluteAnim = FALSE ", serverBD.activitySeats[iSeatID].sSeat)
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CHAMPAGNE_PROPS: NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID: propLiquid = FALSE ", serverBD.activitySeats[iSeatID].sSeat, " seatID: ", iSeatID)
							ENDIF
	//					ELSE
	//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CHAMPAGNE_PROPS: TAKE_CONTROL_OF_NET_ID: propChampBotIdle = FALSE ", serverBD.activitySeats[iSeatID].sSeat)
	//					ENDIF
	//				ELSE
	//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CHAMPAGNE_PROPS: NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID: propChampBotIdle = FALSE ", serverBD.activitySeats[iSeatID].sSeat, " seatID: ", iSeatID)
	//				ENDIF 
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CHAMPAGNE_PROPS: NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID: propChampBotAnim = FALSE ", serverBD.activitySeats[iSeatID].sSeat, " seatID: ", iSeatID)
				ENDIF 
	//		ELSE
	//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CHAMPAGNE_PROPS: NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID: propChampFluteIdle = FALSE ", serverBD.activitySeats[iSeatID].sSeat)
	//		ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CHAMPAGNE_PROPS: NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID: propChampFluteAnim = FALSE, sSeat ", serverBD.activitySeats[iSeatID].sSeat)
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GRAB_CHAMPAGNE_PROPS: Invalid seatID")
	ENDIF
	RETURN FALSE
ENDFUNC

//       bAreAllPlayersFinished = TRUE
//       REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
//          IF NETWORK_IS_PLAYER_ACTIVE(PLAYERS_ID_FROM_INT(i))
//             // Checks for what the player is up to go here
//             IF NOT IS_PLAYER_IN_FINISHED_STATE(i)
//                bAreAllPlayersFinished = FALSE
//             ENDIF
//          ENDIF
//       ENDREPEAT
//      PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
//      PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)

PROC HANDLE_SYNC_SCENE_CLEANUP_IF_DEAD(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct)
	IF IS_PLAYER_DEAD(PLAYER_ID())
//	AND (luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC
//		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC
//		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_PLAY_CIGAR_SYNC)
		RELEASE_CHAMPAGNE_PROPS(serverBD, luxeActStruct.iSeatID)
//		RELEASE_CIGAR_PROPS(serverBD, luxeActStruct.iSeatID)
	ENDIF
ENDPROC

//INFO: 
//PARAM NOTES: viSpecificVehicleInstance checks to see if player is trying to enter or already in a specific vehicle
//PURPOSE: Checks to see if player is in, entering or trying to enter a luxe vehicle
FUNC BOOL IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH(LUXE_ACT_STRUCT &luxeActStruct, VEHICLE_INDEX viSpecificVehicleInstance, BOOL &bCleanupSwiftImmediately)
	INT iMissionType = GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID())
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND iMissionType != FMMC_TYPE_RACE  
	AND NOT NETWORK_IS_IN_MP_CUTSCENE()
	#IF FEATURE_HEIST_ISLAND
	AND NOT HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
	#ENDIF
		IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())) // If we are in or attemping to get into a vehicle
		OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		
			IF viSpecificVehicleInstance != NULL
				IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
					IF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) != viSpecificVehicleInstance
//						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH: player not entering/trying to enter the specified vehicle ID")
//						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "							- GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) 	= ", NATIVE_TO_INT(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
//						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "							- viSpecificVehicleInstance 					= ", NATIVE_TO_INT(viSpecificVehicleInstance))
						IF luxeActStruct.vehModel = SWIFT2
						OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel)
							//CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH: setting CleanupSwiftImmediately to true")
							bCleanupSwiftImmediately = TRUE
						ENDIF
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF 
		
			IF viSpecificVehicleInstance != NULL
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE) != viSpecificVehicleInstance
//						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH: player not in the specified vehicle ID ")
//						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "							- GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE) 	= ", NATIVE_TO_INT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
//						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "							- viSpecificVehicleInstance 					= ", NATIVE_TO_INT(viSpecificVehicleInstance))
						IF luxeActStruct.vehModel = SWIFT2
						OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel)
							//CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH: setting CleanupSwiftImmediately to true")
							bCleanupSwiftImmediately = TRUE
						ENDIF
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF 
			
			IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
				luxeActStruct.vehicleIndex = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
				//CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH: player is entering vehicle idx: ", NATIVE_TO_INT(luxeActStruct.vehicleIndex), ", model enum: ", ENUM_TO_INT(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)))
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				luxeActStruct.vehicleIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
				//CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH: player is in a vehicle: ", NATIVE_TO_INT(luxeActStruct.vehicleIndex))
			ELSE
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, " player is not in any vehicle")
			ENDIF
		
			MODEL_NAMES vehIdxModel = DUMMY_MODEL_FOR_SCRIPT
			IF DOES_ENTITY_EXIST(luxeActStruct.vehicleIndex) 
				vehIdxModel = GET_ENTITY_MODEL(luxeActStruct.vehicleIndex) 
			ENDIF
			
			// Check if it's a luxe activity vehicle 
			IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel) 
			OR vehIdxModel = SWIFT2
			OR IS_PLAYER_IN_LUX_HELI_VEHICLE(vehIdxModel)
				luxeActStruct.vehModel = vehIdxModel
				RETURN TRUE	
			ELSE
				luxeActStruct.vehModel = vehIdxModel
				RETURN FALSE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


PROC SET_ALL_LUXE_PROPS_INVISIBLE_THIS_FRAME(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct)

	INT iSeat
	REPEAT MAX_LUXE_SEATS iSeat
	
		// handle cigar and lighter
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.activitySeats[iSeat].propCigarAnim)
			SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_OBJ(serverBD.activitySeats[iSeat].propCigarAnim))
		ENDIF
		
		IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[iSeat].propCigarIdle)
			IF IS_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeat].propCigarIdle)
				SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeat].propCigarIdle, FALSE)
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.activitySeats[iSeat].propLighter)
			SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_OBJ(serverBD.activitySeats[iSeat].propLighter))
		ENDIF
		
		// handle bottle and flute
		IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[iSeat].propChampBotIdle)
			IF IS_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeat].propChampBotIdle)
				SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeat].propChampBotIdle, FALSE) // an set idle one to invisibilt, i.e. swap
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.activitySeats[iSeat].propChampBotAnim)
			SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_OBJ(serverBD.activitySeats[iSeat].propChampBotAnim)) // set local anim prop visibile
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.activitySeats[iSeat].propChampFluteAnim)
			SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_OBJ(serverBD.activitySeats[iSeat].propChampFluteAnim))
		ENDIF
		
		IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[iSeat].propChampFluteIdle)
			IF IS_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeat].propChampFluteIdle)
				SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[iSeat].propChampFluteIdle, FALSE)
			ENDIF
		ENDIF
		
	ENDREPEAT

ENDPROC

PROC HANDLE_PROP_VISIBILITY_SWAP(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct, PLAYER_BROADCAST_DATA &playerBD[])
	BOOL bTempBool
//	INT iTableCounter = 0
//	 
// 
//	REPEAT 3 iTableCounter
//		IF serverBD.tablesData[iTableCounter].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1) // looping through tables to check to see if visibility is turned on, because if we loop through players, a player may have disconnected and therefore we can't find out which table they were on
////			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
////				IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX(i) )
////					PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
//					IF luxeActStruct.bNetChampVisible[iTableCounter] = TRUE // We've found a table where the chosen player of the animation is set to null, but the table is forcing anim objects to be visible, 
//						//so reset the table visibility variable
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: INVALID Table visibility on table: ", iTableCounter," chosen player is set to -1 so resetting bNetChampVisible back to false")
//						luxeActStruct.bNetChampVisible[iTableCounter] = FALSE
//					ENDIF
////				ENDIF
////			ENDREPEAT
//		ENDIF 
//	ENDREPEAT

	// If we are locally in a cutscene, hide all props
	IF NETWORK_IS_IN_MP_CUTSCENE()
		SET_ALL_LUXE_PROPS_INVISIBLE_THIS_FRAME(serverBD, luxeActStruct)		
		
		EXIT
	ENDIF



	INT i
	REPEAT NUM_NETWORK_PLAYERS i 
		PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(i) 
		
//		IF IS_NET_PLAYER_OK(playerID, FALSE) 
				

//		IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX(i) )
//			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: player: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", table: ", playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber)
	
			
//			IF playerBD[NATIVE_TO_INT(PlayerId)].iSeatID > -1
				IF IS_NET_PLAYER_OK(playerID, FALSE) 
				AND NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerId)
				AND NOT IS_ENTITY_DEAD(GET_PLAYER_PED(PlayerId))
				AND playerBD[NATIVE_TO_INT(PlayerId)].bCleaningUp = FALSE
				AND NOT IS_PLAYER_SCTV(PlayerId)
				AND NOT IS_PED_BEING_JACKED(GET_PLAYER_PED(PlayerId))
				AND ((IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH(luxeActStruct, NULL, bTempBool)
					AND IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel))
						OR luxeActStruct.vehModel = SWIFT2
						OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel) )
					PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
					
//					IF HAS_ANIM_EVENT_FIRED (PlayerPedId, HASH("MAKE_ANIMGLASS_VISIBLE")) 
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: anim event MAKE_ANIMGLASS_VISIBLE fired for player: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
//						SET_BIT(luxeActStruct.iSwitchFluteLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
//	//					luxeActStruct.bSwitchFluteLocalVisibility[NATIVE_TO_INT(PlayerId)] = TRUE
//					ENDIF
//					
//
					IF playerBD[NATIVE_TO_INT(PlayerId)].iActivityVariation = 2 // the third variation only uses the bottle, so bits need to be reset when the bottle is put back down
						IF HAS_ANIM_EVENT_FIRED (PlayerPedId, HASH("PUT_DOWN_BOTTLE")) 
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: anim event PUT_DOWN_FLUTE fired for player: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
		//					luxeActStruct.bSwitchFluteLocalVisibility[NATIVE_TO_INT(PlayerId)] = FALSE
							CLEAR_BIT(luxeActStruct.iSwitchFluteLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
							CLEAR_BIT(luxeActStruct.iSwitchBottleLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
							luxeActStruct.bNetChampVisible[playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber] = FALSE
		//					CHAMP_PROP_MAKE_VISIBLE_END_SYNC(serverBD.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID])
						ENDIF
					ENDIF
					IF playerBD[NATIVE_TO_INT(PlayerId)].iActivityVariation != 2
						IF HAS_ANIM_EVENT_FIRED (PlayerPedId, HASH("PUT_DOWN_FLUTE")) 
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: anim event PUT_DOWN_FLUTE fired for player: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
		//					luxeActStruct.bSwitchFluteLocalVisibility[NATIVE_TO_INT(PlayerId)] = FALSE
							CLEAR_BIT(luxeActStruct.iSwitchFluteLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
							CLEAR_BIT(luxeActStruct.iSwitchBottleLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
							luxeActStruct.bNetChampVisible[playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber] = FALSE
		//					CHAMP_PROP_MAKE_VISIBLE_END_SYNC(serverBD.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID])
						ENDIF
					ENDIF
					
					IF HAS_ANIM_EVENT_FIRED (PlayerPedId, HASH("PICK_UP_BOTTLE")) 
						IF NOT IS_BIT_SET(luxeActStruct.iSwitchBottleLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
						OR NOT IS_BIT_SET(luxeActStruct.iSwitchFluteLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
						OR NOT luxeActStruct.bNetChampVisible[playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber]
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: anim event PICK_UP_BOTTLE fired for player: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", setting props to be visibile for iTableNumber: ", playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber)
							luxeActStruct.bNetChampVisible[playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber] = TRUE					
							
		//					luxeActStruct.bSwitchBottleLocalVisibility[NATIVE_TO_INT(PlayerId)] = TRUE
							SET_BIT(luxeActStruct.iSwitchBottleLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
							
							IF playerBD[NATIVE_TO_INT(PlayerId)].iActivityVariation != 2
								SET_BIT(luxeActStruct.iSwitchFluteLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
							ENDIF
		//					CHAMP_PROP_MAKE_VISIBLE_END_SYNC(serverBD.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID])
						ENDIF
					ENDIF
					

					
					IF HAS_ANIM_EVENT_FIRED (PlayerPedId, HASH("PICK_UP_CIGAR")) 
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: anim event PICK_UP_CIGAR fired for player: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
	//					luxeActStruct.bSwitchCigarLocalVisibility[NATIVE_TO_INT(PlayerId)] = TRUE
						SET_BIT(luxeActStruct.iSwitchCigarLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
	//					CHAMP_PROP_MAKE_VISIBLE_END_SYNC(serverBD.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID])
					ENDIF
					
					IF HAS_ANIM_EVENT_FIRED (PlayerPedId, HASH("PUT_DOWN_CIGAR")) 
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: anim event PUT_DOWN_CIGAR fired for player: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
	//					luxeActStruct.bSwitchCigarLocalVisibility[NATIVE_TO_INT(PlayerId)] = FALSE
						CLEAR_BIT(luxeActStruct.iSwitchCigarLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
	//					CHAMP_PROP_MAKE_VISIBLE_END_SYNC(serverBD.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID])
					ENDIF

					IF HAS_ANIM_EVENT_FIRED (PlayerPedId, HASH("CREATE_LIGHTER")) 
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: anim event CREATE_LIGHTER fired for player: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
	//					luxeActStruct.bSwitchCigarLocalVisibility[NATIVE_TO_INT(PlayerId)] = FALSE
						SET_BIT(luxeActStruct.iSwitchLighterLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
	//					CHAMP_PROP_MAKE_VISIBLE_END_SYNC(serverBD.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID])
					ENDIF
					
					IF HAS_ANIM_EVENT_FIRED (PlayerPedId, HASH("DESTROY_LIGHTER")) 
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: anim event DESTROY_LIGHTER fired for player: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
	//					luxeActStruct.bSwitchCigarLocalVisibility[NATIVE_TO_INT(PlayerId)] = FALSE
						CLEAR_BIT(luxeActStruct.iSwitchLighterLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
	//					CHAMP_PROP_MAKE_VISIBLE_END_SYNC(serverBD.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID])
					ENDIF
					
					
					
					
				ELSE
				
					// PLAYER IS INVALID
					IF IS_BIT_SET(luxeActStruct.iSwitchFluteLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
					OR IS_BIT_SET(luxeActStruct.iSwitchBottleLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
					OR IS_BIT_SET(luxeActStruct.iSwitchCigarLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
						IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerId)
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: INVALID PLAYER: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", because player is not a participant")
						ENDIF
						IF IS_ENTITY_DEAD(GET_PLAYER_PED(PlayerId))
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: INVALID PLAYER: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", because player is not alive")
						ENDIF
						IF playerBD[NATIVE_TO_INT(PlayerId)].bCleaningUp = TRUE
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: INVALID PLAYER: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", because player is cleaning up")
						ENDIF
					ENDIF
					
					IF NOT((IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH(luxeActStruct, NULL, bTempBool)
							AND IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel))
								OR luxeActStruct.vehModel = SWIFT2
								OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel) )
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: INVALID PLAYER: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", because player is not in a Luxor2 or trying to get into one, or is not 25 meters away from a swift2")
					ENDIF
					// TODO flags are reset but funcitons aren't going to get hit
					IF IS_BIT_SET(luxeActStruct.iSwitchCigarLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
	//				IF luxeActStruct.bSwitchCigarLocalVisibility[NATIVE_TO_INT(PlayerId)] = TRUE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP:  INVALID PLAYER: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", resetting clearing bit iSwitchCigarLocalVisibilityBitSet")
						CLEAR_BIT(luxeActStruct.iSwitchCigarLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
	//					luxeActStruct.bSwitchCigarLocalVisibility[NATIVE_TO_INT(PlayerId)] = FALSE
					ENDIF 
					
					IF IS_BIT_SET(luxeActStruct.iSwitchBottleLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
	//				IF luxeActStruct.bSwitchBottleLocalVisibility[NATIVE_TO_INT(PlayerId)] = TRUE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP:  INVALID PLAYER: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", resetting clearing bit iSwitchBottleLocalVisibilityBitSet")
						CLEAR_BIT(luxeActStruct.iSwitchBottleLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
						
						// Networked champagne bottle no longer needs to be visible
						luxeActStruct.bNetChampVisible[playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber] = FALSE
						
	//					luxeActStruct.bSwitchBottleLocalVisibility[NATIVE_TO_INT(PlayerId)] = FALSE
					ENDIF
					
					IF IS_BIT_SET(luxeActStruct.iSwitchFluteLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
	//				IF luxeActStruct.bSwitchFluteLocalVisibility[NATIVE_TO_INT(PlayerId)] = TRUE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP:  INVALID PLAYER: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", resetting clearing bit iSwitchFluteLocalVisibilityBitSet")
						CLEAR_BIT(luxeActStruct.iSwitchFluteLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
	//					luxeActStruct.bSwitchFluteLocalVisibility[NATIVE_TO_INT(PlayerId)] = FALSE
					ENDIF 
					
//					IF luxeActStruct.bNetChampVisible[playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber] = TRUE
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: INVALID PLAYER: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", resetting bNetChampVisible back to false")
//						luxeActStruct.bNetChampVisible[playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber] = FALSE
//					ENDIF
				ENDIF
				
				IF playerBD[NATIVE_TO_INT(PlayerId)].iSeatID > -1
					IF IS_BIT_SET(luxeActStruct.iSwitchCigarLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
		//				IF luxeActStruct.bSwitchCigarLocalVisibility[NATIVE_TO_INT(PlayerId)] = TRUE
						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: CIGAR: is true ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
						CIGAR_MAKE_INVISIBLE_START_SYNC(luxeActStruct, serverBD, playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
					ELSE
						IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propCigarIdle)
						AND NOT IS_ENTITY_VISIBLE(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propCigarIdle)
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: making idle cigar glass visible again")
							SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propCigarIdle, TRUE)
						ENDIF
					ENDIF 
					
					IF IS_BIT_SET(luxeActStruct.iSwitchBottleLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
		//				IF luxeActStruct.bSwitchBottleLocalVisibility[NATIVE_TO_INT(PlayerId)] = TRUE
						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: BOTTLE: is true ", GET_PLAYER_NAME(PlayerId), ", ID: ", NATIVE_TO_INT(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
						CHAMP_BOTTLE_MAKE_INVISIBLE_START_SYNC(luxeActStruct, serverBD, playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
					ELSE
						IF luxeActStruct.bNetChampVisible[playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber] = FALSE
		//					IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propChampBotIdle)
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: making idle bottle visible but player: ", GET_PLAYER_NAME(PlayerId),", ID: ", NATIVE_TO_INT(PlayerId), " is using, on table: ", playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber)
							IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propChampBotIdle)
							AND NOT IS_ENTITY_VISIBLE(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propChampBotIdle)
								SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propChampBotIdle, TRUE)
							ENDIF
						ELSE
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: tried to make idle bottle visible but other player is using, on table: ", playerBD[NATIVE_TO_INT(PlayerId)].iTableNumber)
						ENDIF
					ENDIF 
					
					
					
					IF IS_BIT_SET(luxeActStruct.iSwitchFluteLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
		//				IF luxeActStruct.bSwitchFluteLocalVisibility[NATIVE_TO_INT(PlayerId)] = TRUE
						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: FLUTE: is true ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", PlayerId: ", NATIVE_TO_INT(PlayerId))
						
		//					IF DOES_ENTITY_EXIST(NET_TO_OBJ(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propLiquid))
						IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propLiquid)
							CHAMP_FLUTE_MAKE_INVISIBLE_START_SYNC(luxeActStruct, serverBD, playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, NET_TO_OBJ(serverBD.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propLiquid))
						ENDIF
					ELSE
						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: FLUTE: is false ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID, ", PlayerId: ", NATIVE_TO_INT(PlayerId))
		//					IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propChampFluteIdle)
						IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propChampFluteIdle)
						AND NOT IS_ENTITY_VISIBLE(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propChampFluteIdle)
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: making idle flute glass visible again, for player: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
							SET_ENTITY_VISIBLE(luxeActStruct.activitySeats[playerBD[NATIVE_TO_INT(PlayerId)].iSeatID].propChampFluteIdle, TRUE)
						ENDIF
						
					ENDIF 
					
					IF IS_BIT_SET(luxeActStruct.iSwitchLighterLocalVisibilityBitSet, NATIVE_TO_INT(PlayerId))
		//				IF luxeActStruct.bSwitchFluteLocalVisibility[NATIVE_TO_INT(PlayerId)] = TRUE
						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: FLUTE: is true ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
						
						CIGAR_LIGHTER_MAKE_INVISIBLE_START_SYNC(serverBD, playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
						
					ENDIF
				ELSE
					CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: Not doing any visibility swapping for ", GET_PLAYER_NAME(PlayerId)," because seatID is less then 0 ")
				ENDIF
//		ELSE
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_PROP_VISIBILITY_SWAP: IS_NET_PLAYER_OK = FALSE for player: ", GET_PLAYER_NAME(PlayerId), " on seatID: ", playerBD[NATIVE_TO_INT(PlayerId)].iSeatID)
//		ENDIF
	ENDREPEAT
	
ENDPROC


PROC CLEANUP_VFX()
	REMOVE_NAMED_PTFX_ASSET("scr_mp_cig_plane")
	CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_VFX")
ENDPROC
PROC CLEANUP_AUDIO(LUXE_ACT_STRUCT &luxeActStruct)
	IF luxeActStruct.bAudioBanksRequested
		IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_LUXE/LUXE_SMOKE_CIGAR")
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_LUXE/LUXE_DRINK_CHAMPAGNE")
		ELSE
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_LUXE/LUXE_DRINK_CHAMPAGNE")
		ENDIF
		
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLEANUP_AUDIO: done")
	ENDIF
	
	luxeActStruct.bAudioBanksRequested = FALSE
ENDPROC

PROC RUN_IDLE_CIGAR_PROP_EFFECTS(LUXE_ACT_STRUCT &luxeActStruct)

	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(luxeActStruct.ptfxIDCigarIdle)
		IF DOES_ENTITY_EXIST(luxeActStruct.activitySeats[luxeActStruct.iSeatID].propCigarIdle)
			IF IS_ENTITY_VISIBLE(luxeActStruct.activitySeats[luxeActStruct.iSeatID].propCigarIdle)
				IF luxeActStruct.bCigarIdleSmokeFXStarted = TRUE
					SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigarIdle , "light_intensity", 0.0)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigarIdle , "smoke_fade", 0.0)
				ENDIF
			ELSE
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "START_SMOKE_EFFECT: idle prop not visibile: ", luxeActStruct.iSeatID)
				
				IF luxeActStruct.bCigarIdleSmokeFXStarted = TRUE
					SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigarIdle , "light_intensity", 1.0) // if the entity isn't visible, then don't show the particle effects
					SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigarIdle , "smoke_fade", 1.0)
				ENDIF
			ENDIF
		ELSE
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "START_SMOKE_EFFECT: can't find idle cigar for seatID: ", luxeActStruct.iSeatID)
		ENDIF
	ENDIF

	
ENDPROC 
PROC RUN_ANIM_CIGAR_PROP_EFFECTS(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(luxeActStruct.ptfxIDCigar)
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim)
			IF luxeActStruct.bCigarAnimSmokeFXStarted = TRUE
				SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "light_intensity", 0.0)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "smoke_fade", 0.0)	
			ENDIF
		ELSE
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "START_SMOKE_EFFECT: can't find anim cigar for seatID: ", luxeActStruct.iSeatID)
		ENDIF
	ENDIF
	
ENDPROC 

PROC RUN_VFX_FOR_SYNC(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct)
	IF (luxeActStruct.vehModel != SWIFT2
	AND NOT IS_PLAYER_IN_LUX_HELI_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)) )
	AND luxeActStruct.iSeatID > -1
		// Manages when to start particle effects on anim and idle cigars
		START_SMOKE_EFFECT(serverBD, luxeActStruct)
		
		// Manages the burnout timer
		
//		IF HAS_NET_TIMER_STARTED(luxeActStruct.cigarBurnOutTimer)
//			IF NOT HAS_NET_TIMER_EXPIRED(luxeActStruct.cigarBurnOutTimer, 50000, TRUE)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX_FOR_SYNC: cigarBurnOutTimer: ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), luxeActStruct.cigarBurnOutTimer.Timer))
//				
//				luxeActStruct.fAmberLightIntensity = ABSF((TO_FLOAT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), luxeActStruct.cigarBurnOutTimer.Timer))/TO_FLOAT(50000))-1)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX_FOR_SYNC: fAmberLightIntensity: ", luxeActStruct.fAmberLightIntensity)
//			ELSE
//				RESET_NET_TIMER(luxeActStruct.cigarBurnOutTimer)
//				CPRINTLN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX_FOR_SYNC: cigarBurnOutTimer expired, raising bNeedToLightCigar flag")
//				luxeActStruct.bNeedToLightCigar = TRUE
//			ENDIF
//		ENDIF
		
		
		RUN_IDLE_CIGAR_PROP_EFFECTS(luxeActStruct)
		RUN_ANIM_CIGAR_PROP_EFFECTS(serverBD, luxeActStruct)
		
//		SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "light_intensity", luxeActStruct.fAmberLightIntensity)
//		SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "smoke_fade", luxeActStruct.fAmberLightIntensity)
		
//		IF luxeActStruct.bCigarAnimSmokeFXStarted = TRUE
//			IF luxeActStruct.bNeedToLightCigar = FALSE
//				IF HAS_NET_TIMER_STARTED(luxeActStruct.cigarBurnOutTimer)
////					IF NOT HAS_NET_TIMER_EXPIRED(luxeActStruct.cigarBurnOutTimer, luxeActStruct.iTimeToBurnOut)
//					IF NOT HAS_NET_TIMER_EXPIRED(luxeActStruct.cigarBurnOutTimer, 50000, TRUE)
//						luxeActStruct.fAmberLightIntensity = ABSF((TO_FLOAT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), luxeActStruct.cigarBurnOutTimer.Timer))/TO_FLOAT(50000))-1)
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX_FOR_SYNC: fAmberLightIntensity: ", luxeActStruct.fAmberLightIntensity)
//						SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "light_intensity", luxeActStruct.fAmberLightIntensity)
//						SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "smoke_fade", luxeActStruct.fAmberLightIntensity)
//						RUN_IDLE_CIGAR_PROP_EFFECTS(luxeActStruct)
//					ELSE
//						RESET_NET_TIMER(luxeActStruct.cigarBurnOutTimer)
//						CPRINTLN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX_FOR_SYNC: cigarBurnOutTimer expired, raising bNeedToLightCigar flag")
//						luxeActStruct.bNeedToLightCigar = TRUE
//					ENDIF
//				ELSE
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX_FOR_SYNC: HAS_NET_TIMER_STARTED: FALSE")
//				ENDIF
//			ELSE
//				SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "light_intensity", luxeActStruct.fAmberLightIntensity)
//				SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "smoke_fade", luxeActStruct.fAmberLightIntensity)
//				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX_FOR_SYNC: bNeedToLightCigar: ", luxeActStruct.bNeedToLightCigar)
//			ENDIF
//		ENDIF
		IF luxeActStruct.bInhaling = TRUE
			SMOKE_REACT_INHALE(luxeActStruct)		
		ENDIF
		
		IF luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND DOES_PARTICLE_FX_LOOPED_EXIST(luxeActStruct.ptfxIDCigar)
			IF !bCigarExhale
				IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BEGIN_EXHALE")) 
					USE_PARTICLE_FX_ASSET("scr_mp_cig_plane")
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "BEGIN_EXHALE")
					START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("ent_anim_cig_exhale_mth_plane", PLAYER_PED_ID(), << -0.025, 0.13, 0.0 >>, <<0,0,0>>, BONETAG_HEAD)
					//SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "light_intensity", luxeActStruct.fAmberLightIntensity)
					luxeActStruct.bInhaling = FALSE
					bCigarExhale = TRUE
				ENDIF
			ELSE
				IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BEGIN_INHALE")) 
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "BEGIN_INHALE")
					SET_PARTICLE_FX_LOOPED_EVOLUTION(luxeActStruct.ptfxIDCigar , "light_intensity", luxeActStruct.fAmberLightIntensity*1.5)
					luxeActStruct.bInhaling = TRUE
					bCigarExhale = FALSE
				ENDIF
			ENDIF
			
			IF luxeActStruct.iClipVariation = 0
			OR luxeActStruct.iClipVariation = 1
				FLOAT fLighterTimeStart 
				FLOAT fLighterTimeEnd 
				FLOAT fSecondSparkTime
				IF IS_BACK_FACING_SEAT(luxeActStruct, serverBD.activitySeats[luxeActStruct.iSeatID])
					fLighterTimeStart = 0.13
					fLighterTimeEnd = 0.24
					fSecondSparkTime = 0.2
				ELSE
					fLighterTimeStart = 0.28
					fLighterTimeEnd = 0.32
					fSecondSparkTime = 0.31
				ENDIF
	//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX: Anim phase ", GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), luxeActStruct.sCigarAnimDict, luxeActStruct.sCigarClip))
				INT localCigarScene 
				localCigarScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(luxeActStruct.iNetSceneid)
				
				IF bLighterStart = FALSE
					IF localCigarScene  != -1
						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX: Anim phase: ", GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene))
						IF GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene ) >= fLighterTimeStart
						AND GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene ) < fLighterTimeEnd
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX: fLighterTimeStart Anim phase > ", fLighterTimeStart)
							USE_PARTICLE_FX_ASSET("scr_mp_cig_plane")
							luxeActStruct.ptfxIDLighter = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("ent_anim_lighter_flame_plane", NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter), <<0.0,0,0.06>>, <<0,0,0>>, GET_ENTITY_BONE_INDEX_BY_NAME(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter), "VFX_Emitter"))
							FORCE_PARTICLE_FX_IN_VEHICLE_INTERIOR(luxeActStruct.ptfxIDLighter, TRUE)
							bLighterStart = TRUE
						ENDIF
					ENDIF
				ELSE
					IF localCigarScene  != -1
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX: Anim phase: ", GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene))
						CDEBUG3LN(DEBUG_ACT_CELEB, "GET_SYNCHRONIZED_SCENE_PHASE: ", GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene))
						IF GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene ) >= fLighterTimeEnd
							IF DOES_PARTICLE_FX_LOOPED_EXIST(luxeActStruct.ptfxIDLighter)
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RUN_VFX: fLighterTimeEnd Anim phase > ", fLighterTimeEnd)
								STOP_PARTICLE_FX_LOOPED(luxeActStruct.ptfxIDLighter)
								bLighterStart = FALSE
								bSparkStart = FALSE
								bSecondSpark = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bSparkStart = FALSE
					IF localCigarScene  != -1
						CDEBUG3LN(DEBUG_ACT_CELEB, "GET_SYNCHRONIZED_SCENE_PHASE: ", GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene))
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene ) >= fLighterTimeStart
						AND GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene ) < fLighterTimeEnd
							USE_PARTICLE_FX_ASSET("scr_mp_cig_plane")
							START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("ent_anim_lighter_sparks_plane", NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter), <<0.0,0,0.06>>, <<0,0,0>>)
							bSparkStart = TRUE
						ENDIF
					ENDIF
				ELSE
					IF bSecondSpark = FALSE
						IF localCigarScene  != -1
							CDEBUG3LN(DEBUG_ACT_CELEB, "GET_SYNCHRONIZED_SCENE_PHASE: ", GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene))
							IF GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene ) >= fSecondSparkTime
							AND GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene ) < fLighterTimeEnd
								USE_PARTICLE_FX_ASSET("scr_mp_cig_plane")
								START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("ent_anim_lighter_sparks_plane", NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter), <<0.0,0,0.06>>, <<0,0,0>>)
								bSecondSpark = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF luxeActStruct.iSeatID < 0
			CDEBUG3LN(DEBUG_ACT_CELEB, "RUN_VFX_FOR_SYNC: seatID is less then 0 ")
		ENDIF
	ENDIF
	
ENDPROC


PROC SET_SEAT_ANIMS_DATA(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct, PLAYER_BROADCAST_DATA &playerBD[])
	SET_PLAYER_SEAT_AND_TABLE_DATA(luxeActStruct, playerBD)
	IF luxeActStruct.iSeatID > -1
		IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
			IF IS_BACK_FACING_SEAT(luxeActStruct, serverBD.activitySeats[luxeActStruct.iSeatID])
				luxeActStruct.sCigarAnimDict = "ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@SMOKE@"
				luxeActStruct.sChampAnimDict = "ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@DRINK@"
				
	//			luxeActStruct.fPickUpCigarPhase = luxeActStruct.fPickUpCigarBackPhase
	//			luxeActStruct.fPutDownCigarPhase = luxeActStruct.fPutDownCigarBackPhase
	//			
	//			luxeActStruct.fPutDownBottlePhase = luxeActStruct.fPutDownBottleBackPhase
	//			luxeActStruct.fPickUpBottlePhase = luxeActStruct.fPickUpBottleBackPhase 
	//			
	//			luxeActStruct.fPickUpFlutePhase = luxeActStruct.fPickUpFluteBackPhase
	//			luxeActStruct.fPutDownFlutePhase = luxeActStruct.fPutDownFluteBackPhase
			ELSE
				luxeActStruct.sCigarAnimDict = "ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_FRONT@SMOKE@"
				luxeActStruct.sChampAnimDict = "ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_FRONT@DRINK@"
				
	//			luxeActStruct.fPickUpCigarPhase = luxeActStruct.fPickUpCigarFrontPhase
	//			luxeActStruct.fPutDownCigarPhase = luxeActStruct.fPutDownCigarFrontPhase
	//			
	//			luxeActStruct.fPutDownBottlePhase = luxeActStruct.fPutDownBottleFrontPhase 
	//			luxeActStruct.fPickUpBottlePhase = luxeActStruct.fPickUpBottleFrontPhase
	//			
	//			luxeActStruct.fPickUpFlutePhase = luxeActStruct.fPickUpFluteFrontPhase
	//			luxeActStruct.fPutDownFlutePhase = luxeActStruct.fPutDownFluteFrontPhase
			ENDIF
		ELSE
			IF IS_PLAYER_IN_DRIVER_SIDE_SEAT(luxeActStruct, serverBD.activitySeats[luxeActStruct.iSeatID])
				
				luxeActStruct.sChampAnimDict = luxeActStruct.sChampAnimDriverSideDict
				
	//			luxeActStruct.fPutDownBottlePhase = luxeActStruct.fPutDownBottleDriverSidePhase
	//			luxeActStruct.fPickUpBottlePhase = luxeActStruct.fPickUpBottleDriverSidePhase
	//			
	//			luxeActStruct.fPickUpFlutePhase = luxeActStruct.fPickUpFluteDriverSidePhase
	//			luxeActStruct.fPutDownFlutePhase = luxeActStruct.fPutDownFluteDriverSidePhase
			ELSE
				luxeActStruct.sChampAnimDict = luxeActStruct.sChampAnimPassengerSideDict
				
	//			luxeActStruct.fPutDownBottlePhase = luxeActStruct.fPutDownBottlePasSidePhase
	//			luxeActStruct.fPickUpBottlePhase = luxeActStruct.fPickUpBottlePasSidePhase
	//			
	//			luxeActStruct.fPickUpFlutePhase = luxeActStruct.fPickUpFlutePasSidePhase
	//			luxeActStruct.fPutDownFlutePhase = luxeActStruct.fPutDownFlutePasSidePhase
			ENDIF
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_SEAT_ANIMS_DATA: Invalid seatID")
	ENDIF
	
ENDPROC


FUNC BOOL HAS_SERVER_CREATED_NETWORK_CIGAR_PROPS(SERVER_BROADCAST_DATA &serverBD, PLAYER_BROADCAST_DATA &playerBD[], LUXE_ACT_STRUCT &luxeActStruct)
	IF luxeActStruct.iSeatID > -1
		IF playerBD[NATIVE_TO_INT(PLAYER_ID())].bRequestCigarNetProp = FALSE
			playerBD[NATIVE_TO_INT(PLAYER_ID())].bRequestCigarNetProp = TRUE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HAS_SERVER_CREATED_NETWORK_CIGAR_PROPS: client has requested cigar props to be created")
		ENDIF
		
		IF DO_NETWORK_CIGAR_EXIST(serverBD.activitySeats[luxeActStruct.iSeatID])
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HAS_SERVER_CREATED_NETWORK_CIGAR_PROPS: network cigar props have been found by client, resetting request and returning true")
			playerBD[NATIVE_TO_INT(PLAYER_ID())].bRequestCigarNetProp = FALSE
			RETURN TRUE
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HAS_SERVER_CREATED_NETWORK_CIGAR_PROPS: Invalid seatID")
	ENDIF
	RETURN FALSE
ENDFUNC


PROC SERVER_MANAGE_NETWORK_CIGAR_PROPS(SERVER_BROADCAST_DATA &serverBD, PLAYER_BROADCAST_DATA &playerBD[], LUXE_ACT_SERVER_STRUCT &luxeActServerStruct, LUXE_ACT_STRUCT &luxeActStruct)
	INT iPlayerCount 
	iPlayerCount = 0
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPlayerCount
		IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX(iPlayerCount) )
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))
						
			IF playerBD[iPlayerCount].iSeatID > -1
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerId)
				AND NOT IS_ENTITY_DEAD(GET_PLAYER_PED(PlayerId))
				AND playerBD[iPlayerCount].bCleaningUp = FALSE
	
					IF playerBD[iPlayerCount].bRequestCigarNetProp = TRUE
					AND playerBD[iPlayerCount].bIsClientFinishedCigar = FALSE // checking this because if it is true, it means that cigar props already exist and haven't been destroyed yet, which needs to happen to prevent more props being created that haven't been reserved
					AND NOT DO_CIGAR_NETWORK_OBJECTS_EXIST(serverBD.activitySeats[playerBD[iPlayerCount].iSeatID])
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_MANAGE_NETWORK_CIGAR_PROPS: Server found Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))), " requesting cigar props for seat: ", playerBD[iPlayerCount].iSeatID)
						IF NOT IS_BIT_SET(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CIGAR], iPlayerCount)
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_MANAGE_NETWORK_CIGAR_PROPS: Reserve bit not set for player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))), " requesting to do activity, iPlayerCount: ", iPlayerCount)
							IF RESERVE_NETWORK_PROPS_FOR_CIGAR()
								SET_BIT(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CIGAR], iPlayerCount)
							ELSE
								CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_MANAGE_NETWORK_CIGAR_PROPS: waiting for RESERVE_NETWORK_PROPS_FOR_CIGAR")
							ENDIF
						ELSE
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_MANAGE_NETWORK_CIGAR_PROPS: Reserve bit is set for player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))), " requesting to do activity, iPlayerCount: ", iPlayerCount)
						ENDIF
						IF IS_BIT_SET(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CIGAR], iPlayerCount)
							IF CREATE_NETWORK_OBJECTS_FOR_CIGAR(serverBD.activitySeats[playerBD[iPlayerCount].iSeatID], luxeActStruct)
//								playerBD[iPlayerCount].bRequestCigarNetProp = FALSE
								CLEAR_BIT(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CIGAR], iPlayerCount)
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SERVER_MANAGE_NETWORK_CIGAR_PROPS: Server created network objects for Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))), " requesting champ act for seat: ", playerBD[iPlayerCount].iSeatID)
							ELSE
								CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_MANAGE_NETWORK_CIGAR_PROPS: Server attempting to make network objects for Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))), " requesting champ act for seat: ", playerBD[iPlayerCount].iSeatID, " but failing for some reason")
								// TODO - FAILSAFE
							ENDIF
						ELSE
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_MANAGE_NETWORK_CIGAR_PROPS: Server attempting to make network objects for Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))), " requesting champ act for seat: ", playerBD[iPlayerCount].iSeatID, " but can't reserve space for some reason")
						ENDIF
					ENDIF
					
					IF playerBD[iPlayerCount].bIsClientFinishedCigar = TRUE
						IF DESTROY_NETWORK_OBJECTS_FOR_CIGAR(serverBD.activitySeats[playerBD[iPlayerCount].iSeatID])
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SERVER_MANAGE_NETWORK_CIGAR_PROPS: Server destroyed network objects for Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))), "  for seat: ", playerBD[iPlayerCount].iSeatID)
				
							CLEAR_BIT(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CIGAR], iPlayerCount)
						ELSE
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_MANAGE_NETWORK_CIGAR_PROPS: Server attempting to destroy objects for Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))), " requesting champ act for seat: ", playerBD[iPlayerCount].iSeatID, " but failing for some reason")
						ENDIF
					ENDIF
				ELSE
					
					IF IS_BIT_SET(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CIGAR], iPlayerCount)
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SERVER_MANAGE_NETWORK_CIGAR_PROPS: Found Invalid player in seatID:", playerBD[iPlayerCount].iSeatID, ", with iObjectsReservedForSeatBitSet set to true, so resetting")
						CLEAR_BIT(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CIGAR], iPlayerCount)
					ENDIF
				ENDIF
			ELSE
				IF playerBD[iPlayerCount].iSeatID < 0
					
					IF IS_BIT_SET(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CIGAR], iPlayerCount)
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SERVER_MANAGE_NETWORK_CIGAR_PROPS: Found Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))), " with Invalid seatID:", playerBD[iPlayerCount].iSeatID, ", with iObjectsReservedForSeatBitSet set to true, so resetting")
						CLEAR_BIT(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CIGAR], iPlayerCount)
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC CLIENT_REQUEST_CHAMP_ACT(SERVER_BROADCAST_DATA &serverBD, PLAYER_BROADCAST_DATA &playerBD[], LUXE_ACT_STRUCT &luxeActStruct)
		
	SWITCH playerBD[NATIVE_TO_INT(PLAYER_ID())].clientRequestStage
		CASE CR_CHAMP_IDLE
			IF luxeActStruct.bRequestChamp 
			AND luxeActStruct.iSeatID > -1
				luxeActStruct.bRequestChamp = FALSE
				SetClientRequestChampStage(playerBD[NATIVE_TO_INT(PLAYER_ID())].clientRequestStage, CR_REQUESTING)
			ENDIF
		BREAK
		
		CASE CR_REQUESTING
			IF (playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber > -1
			AND playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber < 3)
			AND (serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen = PLAYER_ID()
			OR serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1))
			AND playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID > -1
			
				IF playerBD[NATIVE_TO_INT(PLAYER_ID())].bRequestChampAct = FALSE
					playerBD[NATIVE_TO_INT(PLAYER_ID())].bRequestChampAct = TRUE
				ENDIF
				
				IF serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen = PLAYER_ID()
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT_REQUEST_CHAMP_ACT: server has picked this client to play activity")
					IF HAVE_CHAMP_SERVER_PROPS_BEEN_CREATED(serverBD.activitySeats[luxeActStruct.iSeatID])
			//			myLuxeVehInstanceID = ServerBD.luxeVehInstanceID
						//playerBD[NATIVE_TO_INT(PLAYER_ID())].bPlayerStartedChampAct = TRUE
						playerBD[NATIVE_TO_INT(PLAYER_ID())].bRequestChampAct = FALSE
						SetClientRequestChampStage(playerBD[NATIVE_TO_INT(PLAYER_ID())].clientRequestStage, CR_PLAY_CHAMP)	
					ELSE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT_REQUEST_CHAMP_ACT: server has picked this client to play activity, but server props haven't been created")
					ENDIF
				ELSE
					IF serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen != INT_TO_NATIVE(PLAYER_INDEX, -1)
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CLIENT_REQUEST_CHAMP_ACT: server is not allowing us to use activity, instead allowing client: ", GET_PLAYER_NAME(serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen))
					ELSE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT_REQUEST_CHAMP_ACT: champIDPlayerChosen = NULL")
					ENDIF
				ENDIF
			ELSE
				playerBD[NATIVE_TO_INT(PLAYER_ID())].bRequestChampAct = FALSE
				SetClientRequestChampStage(playerBD[NATIVE_TO_INT(PLAYER_ID())].clientRequestStage, CR_CHAMP_BUSY)
			ENDIF
			
		BREAK
		
		CASE CR_CHAMP_BUSY
			IF serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1)
			OR playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID < 0
				SetClientRequestChampStage(playerBD[NATIVE_TO_INT(PLAYER_ID())].clientRequestStage, CR_CHAMP_IDLE)
			ENDIF
		BREAK
		
		CASE CR_PLAY_CHAMP
			IF luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_PROMPT_ACT
			OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_CLEANED_UP			// Or we are cleaning up, reset our request stage to IDLE
				#IF IS_DEBUG_BUILD
				IF luxeActStruct.luxeActivityState = CL_LUXE_ACT_CLEANED_UP			
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CLIENT_REQUEST_CHAMP_ACT: CR_PLAY_CHAMP: we are cleaning up, reset our request stage to IDLE")
				ENDIF
				#ENDIF
				playerBD[NATIVE_TO_INT(PLAYER_ID())].bRequestChampAct = FALSE
				SetClientRequestChampStage(playerBD[NATIVE_TO_INT(PLAYER_ID())].clientRequestStage, CR_CHAMP_IDLE)
			ENDIF
//			playerBD[NATIVE_TO_INT(PLAYER_ID())].bPlayerStartedChampAct = TRUE
//			playerBD[NATIVE_TO_INT(PLAYER_ID())].bPlayerStartedChampAct = TRUE
		BREAK
	ENDSWITCH
ENDPROC
// TODO - FADE in props for swift
//PROC FADE_IN_PROPS_FOR_SWIFT(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct)
//	IF luxeActStruct.bPropsNeedToAlpha = TRUE
//		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "FADE_IN_PROPS_FOR_SWIFT: Fading in")
//		NETWORK_FADE_IN_ENTITY(NET_TO_OBJ(serverBD.activitySeats[0].propChampBotAnim), TRUE)
//		NETWORK_FADE_IN_ENTITY(NET_TO_OBJ(serverBD.activitySeats[0].propChampFluteIdle), TRUE)
//		NETWORK_FADE_IN_ENTITY(NET_TO_OBJ(serverBD.activitySeats[1].propChampFluteIdle), TRUE)
//		luxeActStruct.bPropsNeedToAlpha = FALSE
//	ENDIF
//ENDPROC

PROC SERVER_CHECK_CHAMP_ACT(SERVER_BROADCAST_DATA &serverBD, PLAYER_BROADCAST_DATA &playerBD[], LUXE_ACT_SERVER_STRUCT &luxeActServerStruct, LUXE_ACT_STRUCT &luxeActStruct)
//	IF serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen != INT_TO_NATIVE(PLAYER_INDEX, -1)
//		CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Player: ", GET_PLAYER_NAME(serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen), " is playing champAct")
//	ELSE
//		CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Player: is NULL")
//	ENDIF
	SWITCH serverBD.serverChampCheckStage
		CASE SR_CHECK_FOR_REQUESTS
			
			// loops through players checking for requests and setting table players if free
			INT iPlayerCount 
			
			iPlayerCount = 0
			REPEAT NUM_NETWORK_PLAYERS iPlayerCount
				IF playerBD[iPlayerCount].bRequestChampAct = TRUE
				AND playerBD[iPlayerCount].iSeatID > -1
					#IF IS_DEBUG_BUILD
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerCount))
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))), " requesting champ act for table: ", playerBD[iPlayerCount].iTableNumber)
					ELSE
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Player not an active participant: ", iPlayerCount, " requesting champ act for table: ", playerBD[iPlayerCount].iTableNumber)
					ENDIF
					#ENDIF
					IF serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1)	
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: table not in use so setting table request to player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerCount)))
						// table not in use, so pick player
//						luxeActServerStruct.bPropsNeedToBeCreated[iPlayerCount] = TRUE
						SET_BIT(luxeActServerStruct.iPropsNeedToBeCreatedBitSet, iPlayerCount)
						serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].champIDPlayerChosen = INT_TO_PLAYERINDEX(iPlayerCount)
//						serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].bPropsNeedToBeCreated = TRUE
					ELSE
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: table already in use by player: ",  GET_PLAYER_NAME(serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].champIDPlayerChosen))
						// table already in use, do nothing, or start timer to make sure player picked is actually doing activity
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF playerBD[iPlayerCount].bRequestChampAct = TRUE
						IF playerBD[iPlayerCount].iSeatID < 0
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))), " requesting champ act for table: ", playerBD[iPlayerCount].iTableNumber, ", but their seatID: ", playerBD[iPlayerCount].iSeatID )
						ENDIF
					ENDIF
					#ENDIF
				ENDIF
			ENDREPEAT

			iPlayerCount = 0
			REPEAT NUM_NETWORK_PLAYERS iPlayerCount
//				IF luxeActServerStruct.bPropsNeedToBeCreated[iPlayerCount] = TRUE
				IF IS_BIT_SET(luxeActServerStruct.iPropsNeedToBeCreatedBitSet, iPlayerCount)
				AND playerBD[iPlayerCount].iSeatID > -1
//				IF serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].bPropsNeedToBeCreated = TRUE
//					IF serverBD.activitySeats[playerBD[iPlayerCount].iSeatID].bObjectsReservedForSeat = FALSE
					IF NOT IS_BIT_SET(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CHAMP], iPlayerCount)
//					IF luxeActServerStruct.bObjectsReservedForSeat[iPlayerCount] = FALSE
						IF RESERVE_NETWORK_PROPS_FOR_CHAMP()
//							luxeActServerStruct.bObjectsReservedForSeat[iPlayerCount] = TRUE
							SET_BIT(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CHAMP], iPlayerCount)
						ELSE
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: waiting for RESERVE_NETWORK_PROPS_FOR_CHAMP")
						ENDIF
					ENDIF
					IF IS_BIT_SET(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CHAMP], iPlayerCount)
					AND playerBD[iPlayerCount].iSeatID > -1
						
						IF CREATE_NETWORK_OBJECTS_FOR_CHAMP(serverBD.activitySeats[playerBD[iPlayerCount].iSeatID], luxeActStruct #IF IS_DEBUG_BUILD , serverBD #ENDIF)
//							serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].champIDPlayerChosen = INT_TO_PLAYERINDEX(iPlayerCount)
//							luxeActServerStruct.bPropsNeedToBeCreated[iPlayerCount] = FALSE
							CLEAR_BIT(luxeActServerStruct.iPropsNeedToBeCreatedBitSet, iPlayerCount)
//							serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].bPropsNeedToBeCreated = FALSE			
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Server created network objects for Player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerCount)), " requesting champ act for seat: ", playerBD[iPlayerCount].iSeatID)
						ELSE
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Server attempting to make network objects for Player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerCount)), " requesting champ act for seat: ", playerBD[iPlayerCount].iSeatID, " but failing for some reason")
							// TODO - FAILSAFE
						ENDIF
						
					ELSE
						IF IS_BIT_SET(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CHAMP], iPlayerCount)
						AND playerBD[iPlayerCount].iSeatID < 0
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Server attempting to make network objects for Player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerCount)), " requesting champ act for seat: ", playerBD[iPlayerCount].iSeatID, " but can't because seat is -1")
							CLEAR_BIT(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CHAMP], iPlayerCount)
							CLEAR_BIT(luxeActServerStruct.iPropsNeedToBeCreatedBitSet, iPlayerCount)
							serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1)	
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(luxeActServerStruct.iPropsNeedToBeCreatedBitSet, iPlayerCount)
					AND playerBD[iPlayerCount].iSeatID < 0
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Server attempting to make network objects for Player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerCount)), " requesting champ act for seat: ", playerBD[iPlayerCount].iSeatID, " but seatid is -1")
						CLEAR_BIT(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CHAMP], iPlayerCount)
						CLEAR_BIT(luxeActServerStruct.iPropsNeedToBeCreatedBitSet, iPlayerCount)
						serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1)	
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(luxeActServerStruct.iPropsNeedToBeDestroyedBitSet, iPlayerCount)
				AND playerBD[iPlayerCount].iSeatID > -1
//				IF luxeActServerStruct.bPropsNeedToBeDestroyed[iPlayerCount] = TRUE
//				IF serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].bPropsNeedToBeDestroyed = TRUE
					IF DESTROY_NETWORK_OBJECTS_FOR_CHAMP(serverBD.activitySeats[playerBD[iPlayerCount].iSeatID])
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Server destroyed network objects for Player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerCount)), "  for seat: ", playerBD[iPlayerCount].iSeatID)
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Resetting champIDPlayerChosen to NULL. iPlayerCount: ", iPlayerCount, ", iTableNumber: ", playerBD[iPlayerCount].iTableNumber)
						serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1)
						
//						luxeActServerStruct.bObjectsReservedForSeat[iPlayerCount] = FALSE
						CLEAR_BIT(luxeActServerStruct.iObjectsReservedForSeatBitSet[OBJ_RES_CHAMP], iPlayerCount)
						
//						serverBD.activitySeats[playerBD[iPlayerCount].iSeatID].bObjectsReservedForSeat = FALSE
//						luxeActServerStruct.bPropsNeedToBeDestroyed[iPlayerCount] = FALSE
						CLEAR_BIT(luxeActServerStruct.iPropsNeedToBeDestroyedBitSet, iPlayerCount)
//						serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].bPropsNeedToBeDestroyed = FALSE
					ELSE
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Server attempting to destroy objects for Player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerCount)), " requesting champ act for seat: ", playerBD[iPlayerCount].iSeatID, " but failing for some reason")
					ENDIF
				ELSE
					IF IS_BIT_SET(luxeActServerStruct.iPropsNeedToBeDestroyedBitSet, iPlayerCount)
					AND playerBD[iPlayerCount].iSeatID < 0
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Server attempting to destroy network objects for Player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerCount)), " requesting champ act for seat: ", playerBD[iPlayerCount].iSeatID, " but seatid is -1")
						CLEAR_BIT(luxeActServerStruct.iPropsNeedToBeDestroyedBitSet, iPlayerCount)
						serverBD.tablesData[playerBD[iPlayerCount].iTableNumber].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1)	
					ENDIF
				ENDIF
			ENDREPEAT
			
			// loops through tables checking if in use, resetting if player is in idle state, 
			INT iTableCount
			iTableCount = 0
			REPEAT 3 iTableCount
				IF serverBD.tablesData[iTableCount].champIDPlayerChosen != INT_TO_NATIVE(PLAYER_INDEX, -1)
					IF playerBD[NATIVE_TO_INT(serverBD.tablesData[iTableCount].champIDPlayerChosen)].iSeatID > -1
						IF playerBD[NATIVE_TO_INT(serverBD.tablesData[iTableCount].champIDPlayerChosen)].clientRequestStage = CR_CHAMP_IDLE
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Client: ", GET_PLAYER_NAME(serverBD.tablesData[iTableCount].champIDPlayerChosen)," has finished animation on table: ", serverBD.tablesData[iTableCount].iTable, ", playerID: ", NATIVE_TO_INT(serverBD.tablesData[iTableCount].champIDPlayerChosen))
							
	//						luxeActServerStruct.bPropsNeedToBeDestroyed[NATIVE_TO_INT(serverBD.tablesData[iTableCount].champIDPlayerChosen)] = TRUE
							SET_BIT(luxeActServerStruct.iPropsNeedToBeDestroyedBitSet, NATIVE_TO_INT(serverBD.tablesData[iTableCount].champIDPlayerChosen))
	//						serverBD.tablesData[iTableCount].bPropsNeedToBeDestroyed = TRUE
							
						ELIF playerBD[NATIVE_TO_INT(serverBD.tablesData[iTableCount].champIDPlayerChosen)].clientRequestStage != CR_CHAMP_IDLE
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Client: ", GET_PLAYER_NAME(serverBD.tablesData[iTableCount].champIDPlayerChosen)," has NOT finished animation on table: ", serverBD.tablesData[iTableCount].iTable, " is currently on stage: ", LuxeClientRequestChampString(playerBD[NATIVE_TO_INT(serverBD.tablesData[iTableCount].champIDPlayerChosen)].clientRequestStage))
							// possibly set timer to time out incase player has hogged table for too long
							// TODO - TIME out
						ENDIF
									
						// TODO - server has to check if player who has requested to use activity has disconnected
						IF serverBD.tablesData[iTableCount].champIDPlayerChosen != INT_TO_NATIVE(PLAYER_INDEX, -1)
						
							// If the player is no longer active on script / dead or SCTV then clean up the player index who is using the champagne. This releases it for others.
							IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD.tablesData[iTableCount].champIDPlayerChosen)
							OR IS_PLAYER_DEAD(serverBD.tablesData[iTableCount].champIDPlayerChosen)
							OR IS_PLAYER_SCTV(serverBD.tablesData[iTableCount].champIDPlayerChosen)
							
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Player Index: ", NATIVE_TO_INT(serverBD.tablesData[iTableCount].champIDPlayerChosen), " is now invalid for table: ", serverBD.tablesData[iTableCount].iTable, ". Clean up champIDPlayerChosen to free up activity.")
							
								SET_BIT(luxeActServerStruct.iPropsNeedToBeDestroyedBitSet, NATIVE_TO_INT(serverBD.tablesData[iTableCount].champIDPlayerChosen))
								serverBD.tablesData[iTableCount].champIDPlayerChosen = INVALID_PLAYER_INDEX()
							ENDIF
						

						ENDIF
					ELSE
						IF playerBD[NATIVE_TO_INT(serverBD.tablesData[iTableCount].champIDPlayerChosen)].iSeatID < 0
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: Server attempting to check if something needs to be destroy for Player: ", GET_PLAYER_NAME(serverBD.tablesData[iTableCount].champIDPlayerChosen), "  seatid is -1")
							CLEAR_BIT(luxeActServerStruct.iPropsNeedToBeDestroyedBitSet, NATIVE_TO_INT(serverBD.tablesData[iTableCount].champIDPlayerChosen))
							serverBD.tablesData[iTableCount].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1)	
						ENDIF
					ENDIF
					
				ELSE
					CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SERVER_CHECK_CHAMP_ACT: No one is using table: ", serverBD.tablesData[iTableCount].iTable)
				ENDIF
			
			ENDREPEAT
		BREAK
		
		
	ENDSWITCH
	

ENDPROC
FUNC BOOL IS_PLAYER_PLAYING_ANIMATION(LUXE_ACT_STRUCT &luxeActStruct)
	IF luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC
	OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC
	OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC
	OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC
		//CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_PLAYING_ANIMATION: Player is in stage: ", LuxeActString(luxeActStruct.luxeActivityState), ", so returning TRUE")
		RETURN TRUE
	ELSE
		//CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_PLAYING_ANIMATION: Player is in stage: ", LuxeActString(luxeActStruct.luxeActivityState), ", so returning FALSE")
		RETURN FALSE
	ENDIF
ENDFUNC
PROC TERMINATE_NET_LUX_SCRIPT()
	CLEAR_HELP()
	TERMINATE_THIS_THREAD()
ENDPROC

// Clean Up
PROC LUXE_SCRIPT_CLEANUP(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct, PLAYER_BROADCAST_DATA &playerBD[])

	DEBUG_PRINTCALLSTACK()
	
	// Check we have populated the prompt before querying it
	IF NOT IS_STRING_NULL_OR_EMPTY(luxeActStruct.sPromptMessage)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(luxeActStruct.sPromptMessage)
			CLEAR_HELP()
		ENDIF
	ENDIF	
	
	IF playerBD[NATIVE_TO_INT(PLAYER_ID())].bCleaningUp = FALSE
		playerBD[NATIVE_TO_INT(PLAYER_ID())].bCleaningUp = TRUE
	ENDIF
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		IF NETWORK_GET_NUM_PARTICIPANTS() = 1 // If we are the last participant of the script, clean up the props.
			IF serverBD.luxeServerActScriptState != SR_LUXE_CLEANING_UP
			AND serverBD.luxeServerActScriptState != SR_LUXE_CLEANED_UP
			AND NOT IS_PLAYER_PLAYING_ANIMATION(luxeActStruct)
				SetServerLuxeActStage(serverBD, SR_LUXE_CLEANING_UP)
			ENDIF
		ENDIF
		
		
		
		IF (luxeActStruct.luxeActivityState != CL_LUXE_ACT_CLEANED_UP
			AND luxeActStruct.luxeActivityState != CL_LUXE_ACT_CLEANING_UP)
		AND NOT IS_PLAYER_PLAYING_ANIMATION(luxeActStruct)// If the player is playing an animation, it'll clean itself up, for every other case force the script to cleanup
			SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_CLEANING_UP)
		ENDIF
	
		IF NETWORK_GET_NUM_PARTICIPANTS() < 2 // If we are the last participant of the script, clean up the props.	
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: Last participant, so cleaning up server objects")
			IF serverBD.luxeServerActScriptState = SR_LUXE_CLEANED_UP
			AND luxeActStruct.luxeActivityState = CL_LUXE_ACT_CLEANED_UP
			OR NOT NETWORK_IS_GAME_IN_PROGRESS()
			
				// Allow peds to be left behind again now we are leaving the activity
				
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: mark player to leave ped behind (host): SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE) (+ network)")
						SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
						NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)
					ENDIF
					
					SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE, FALSE)
				ENDIF
				
				IF IS_BIT_SET(g_iLuxeBootVerifyBS, luxeActStruct.iLuxeVehInstanceID)
					CLEAR_BIT(g_iLuxeBootVerifyBS, luxeActStruct.iLuxeVehInstanceID)
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: 1 Cleared boot bit index: ", luxeActStruct.iLuxeVehInstanceID, ", bitset value: ", g_iLuxeBootVerifyBS)
				ENDIF
			
				TERMINATE_NET_LUX_SCRIPT()
			ELSE
				IF serverBD.luxeServerActScriptState != SR_LUXE_CLEANED_UP
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: waiting for server to clean up props")
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: luxeServerActScriptState: ", LuxeServerActString(serverBD.luxeServerActScriptState))
				ENDIF
				
				IF luxeActStruct.luxeActivityState != CL_LUXE_ACT_CLEANED_UP
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: waiting for client to clean up props")
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: luxeActivityState: ", LuxeActString(luxeActStruct.luxeActivityState))
				ENDIF
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: Not last participant: ", NETWORK_GET_NUM_PARTICIPANTS()," so not cleaning up server objects")
			IF luxeActStruct.luxeActivityState = CL_LUXE_ACT_CLEANED_UP
//			OR NOT NETWORK_IS_GAME_IN_PROGRESS()
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: all props cleaned up, terminating thread")
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					// Allow peds to be left behind again now we are leaving the activity
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: mark player to leave ped behind: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE) (+ network)")
						SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
						NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)
					ENDIF
					
					SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE, FALSE)
				ENDIF
				
				IF IS_BIT_SET(g_iLuxeBootVerifyBS, luxeActStruct.iLuxeVehInstanceID)
					CLEAR_BIT(g_iLuxeBootVerifyBS, luxeActStruct.iLuxeVehInstanceID)
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: 2 Cleared boot bit index: ", luxeActStruct.iLuxeVehInstanceID, ", bitset value: ", g_iLuxeBootVerifyBS)
				ENDIF
				
				TERMINATE_NET_LUX_SCRIPT()
			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: waiting for client to clean up props")
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: luxeActivityState: ", LuxeActString(luxeActStruct.luxeActivityState))
			ENDIF
		ENDIF
	ELSE
		
		IF (luxeActStruct.luxeActivityState != CL_LUXE_ACT_CLEANED_UP
			AND luxeActStruct.luxeActivityState != CL_LUXE_ACT_CLEANING_UP)
		AND NOT IS_PLAYER_PLAYING_ANIMATION(luxeActStruct)// If the player is playing an animation, it'll clean itself up, for every other case force the script to cleanup
			SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_CLEANING_UP)
		ENDIF
		
		IF luxeActStruct.luxeActivityState = CL_LUXE_ACT_CLEANED_UP
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: all props cleaned up, terminating thread")
//		OR NOT NETWORK_IS_GAME_IN_PROGRESS()

			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				// Allow peds to be left behind again now we are leaving the activity
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: mark player to leave ped behind: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE) (+ network)")
					SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
					NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)
				ENDIF
				
				SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE, FALSE)
			ENDIF
			
			IF IS_BIT_SET(g_iLuxeBootVerifyBS, luxeActStruct.iLuxeVehInstanceID)
				CLEAR_BIT(g_iLuxeBootVerifyBS, luxeActStruct.iLuxeVehInstanceID)
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: 3 Cleared boot bit index: ", luxeActStruct.iLuxeVehInstanceID, ", bitset value: ", g_iLuxeBootVerifyBS)
			ENDIF
			
			TERMINATE_NET_LUX_SCRIPT()
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CHECK_IN_VEH: waiting for client to clean up")
		ENDIF
	ENDIF
	
ENDPROC



PROC GET_RANDOM_CIGAR_CLIP_VARIATION(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct)
	INT iRandomNum = GET_RANDOM_INT_IN_RANGE(0, 2)
	IF luxeActStruct.bNeedToLightCigar = TRUE // if the cigar needs to be relight, then randomly choose between variation 0 - 1
		iRandomNum = iRandomNum
	ELSE // if it doesn't, then choose randomly between variation 2 - 3
		iRandomNum += 2
	ENDIF
	#IF IS_DEBUG_BUILD
		IF serverBD.iDebugClipVariation != -1
			iRandomNum = serverBD.iDebugClipVariation 
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CIGAR_CLIP_VARIATION - Overriding iRandomNum to ", iRandomNum)
		ENDIF
	#ENDIF
	
	
	
	luxeActStruct.iClipVariation = iRandomNum
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CIGAR_CLIP_VARIATION - luxeActStruct.iClipVariation ", luxeActStruct.iClipVariation)
	UNUSED_PARAMETER(serverBD)
	
	SWITCH iRandomNum
		CASE 0
			luxeActStruct.sCigarClip = "cigar_a"
			luxeActStruct.sCigarCigarClip = "cigar_a_cigar"
			luxeActStruct.sCigarLighterClip = "cigar_a_lighter"
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CIGAR_CLIP_VARIATION: setting variation A")
		BREAK
		CASE 1
			luxeActStruct.sCigarClip = "cigar_a2"
			luxeActStruct.sCigarCigarClip = "cigar_a2_cigar"
			luxeActStruct.sCigarLighterClip = "cigar_a2_lighter"
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CIGAR_CLIP_VARIATION: setting variation A2")
		BREAK
		CASE 2
			luxeActStruct.sCigarClip = "cigar_b"
			luxeActStruct.sCigarCigarClip = "cigar_b_cigar"
			luxeActStruct.sCigarLighterClip = "cigar_b_lighter"
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CIGAR_CLIP_VARIATION: setting variation B")
		BREAK
		CASE 3
			luxeActStruct.sCigarClip = "cigar_c"
			luxeActStruct.sCigarCigarClip = "cigar_c_cigar"
			luxeActStruct.sCigarLighterClip = "cigar_c_lighter"
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CIGAR_CLIP_VARIATION: setting variation C")
		BREAK
	ENDSWITCH
	
	
ENDPROC

PROC GET_RANDOM_CHAMP_CLIP_VARIATION(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct)
	INT iMaxRange
	IF GET_PEDS_DRUNK_ALCOHOL_HIT_COUNT(PLAYER_PED_ID()) > 2
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CHAMP_CLIP_VARIATION - PED is drunk, allowing for drunk variation ")
		iMaxRange = 3
	ELSE
		iMaxRange = 2
	ENDIF

	INT iRandomNum = GET_RANDOM_INT_IN_RANGE(0, iMaxRange)
	#IF IS_DEBUG_BUILD
		IF serverBD.iDebugClipVariation != -1
			iRandomNum = serverBD.iDebugClipVariation 
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CHAMP_CLIP_VARIATION - Overriding iRandomNum to ", iRandomNum)
		ENDIF
	#ENDIF
	UNUSED_PARAMETER(serverBD)
//		IF iForceHeistCam != -1
//			iRandomNum = iForceHeistCam
//			PRINTLN("GET_RANDOM_HEIST_EXIT_CAM - Overriding iRandomNum to ", iForceHeistAnim)
//		ENDIF

	luxeActStruct.iClipVariation = iRandomNum
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CIGAR_CLIP_VARIATION - luxeActStruct.iClipVariation ", luxeActStruct.iClipVariation)
	
	SWITCH iRandomNum
		CASE 0
			luxeActStruct.sChampClip = "champagne_a"
			luxeActStruct.sFluteClip = "champagne_a_flute"
			luxeActStruct.sPourClip = "champagne_a_pour"
			luxeActStruct.sBottleClip = "champagne_a_bottle"
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CHAMP_CLIP_VARIATION: setting variation a")
		BREAK
		CASE 1
			luxeActStruct.sChampClip = "champagne_b"
			luxeActStruct.sFluteClip = "champagne_b_flute"
			luxeActStruct.sPourClip = "champagne_b_pour"
			luxeActStruct.sBottleClip = "champagne_b_bottle"
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CHAMP_CLIP_VARIATION: setting variation b")
		BREAK
		CASE 2
			luxeActStruct.sChampClip = "champagne_c"
			luxeActStruct.sFluteClip = "champagne_c_flute"
			luxeActStruct.sPourClip = "champagne_c_pour"
			luxeActStruct.sBottleClip = "champagne_c_bottle"
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CHAMP_CLIP_VARIATION: setting variation c")
//			luxeActStruct.sChampClip = "champagne_c"
//			luxeActStruct.sFluteClip = "champagne_c_flute"
//			luxeActStruct.sPourClip = "champagne_c_pour"
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "GET_RANDOM_CHAMP_CLIP_VARIATION: setting variation c")
		BREAK
	ENDSWITCH
	
	
	
ENDPROC
PROC SET_ANIM_DICTS_AND_CLIPS(SERVER_BROADCAST_DATA &serverBD, LUXE_ACT_STRUCT &luxeActStruct)
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
		luxeActStruct.sChampAnimBackDict = "ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@DRINK@"
		luxeActStruct.sChampAnimFrontDict = "ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_FRONT@DRINK@"
		
		luxeActStruct.sCigarAnimBackDict = "ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@SMOKE@"
		luxeActStruct.sCigarAnimFrontDict = "ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_FRONT@SMOKE@"
	ELSE
		luxeActStruct.sChampAnimDriverSideDict = "ANIM@MP_PLAYER_INTVEH@HELICOPTER@SWIFT2@RDS@DRINK@"
		luxeActStruct.sChampAnimPassengerSideDict = "ANIM@MP_PLAYER_INTVEH@HELICOPTER@SWIFT2@RPS@DRINK@"
	ENDIF
	GET_RANDOM_CHAMP_CLIP_VARIATION(serverBD, luxeActStruct)
	GET_RANDOM_CIGAR_CLIP_VARIATION(serverBD, luxeActStruct)
ENDPROC

FUNC BOOL REQUEST_ANIM_DICTS_AND_GET_TAGS(LUXE_ACT_STRUCT &luxeActStruct)
	REQUEST_NAMED_PTFX_ASSET("scr_mp_cig_plane")
				
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
		REQUEST_ANIM_DICT(luxeActStruct.sChampAnimBackDict)
		REQUEST_ANIM_DICT(luxeActStruct.sCigarAnimBackDict)
		
		REQUEST_ANIM_DICT(luxeActStruct.sChampAnimFrontDict)
		REQUEST_ANIM_DICT(luxeActStruct.sCigarAnimFrontDict)
		
		IF HAS_ANIM_DICT_LOADED(luxeActStruct.sChampAnimBackDict)
		AND HAS_ANIM_DICT_LOADED(luxeActStruct.sCigarAnimBackDict)
		AND HAS_ANIM_DICT_LOADED(luxeActStruct.sChampAnimFrontDict)
		AND HAS_ANIM_DICT_LOADED(luxeActStruct.sCigarAnimFrontDict)
		
			
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "MAINTAIN_LUXE_VEH_ACTIVITIES: anims have loaded ")
//			IF FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimBackDict, "CHAMPAGNE_A", "PICK_UP_BOTTLE", luxeActStruct.fPickUpBottleBackPhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimBackDict, "CHAMPAGNE_A", "PUT_DOWN_BOTTLE", luxeActStruct.fPutDownBottleBackPhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimFrontDict, "CHAMPAGNE_A", "PICK_UP_BOTTLE", luxeActStruct.fPickUpBottleFrontPhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimFrontDict, "CHAMPAGNE_A", "PUT_DOWN_BOTTLE", luxeActStruct.fPutDownBottleFrontPhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimBackDict, "CHAMPAGNE_A", "PICK_UP_FLUTE", luxeActStruct.fPickUpFluteBackPhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimBackDict, "CHAMPAGNE_A", "PUT_DOWN_FLUTE", luxeActStruct.fPutDownFluteBackPhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimFrontDict, "CHAMPAGNE_A", "PICK_UP_FLUTE", luxeActStruct.fPickUpFluteFrontPhase , luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimFrontDict, "CHAMPAGNE_A", "PUT_DOWN_FLUTE", luxeActStruct.fPutDownFluteFrontPhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sCigarAnimBackDict, "CIGAR_A", "PICK_UP_CIGAR", luxeActStruct.fPickUpCigarBackPhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sCigarAnimFrontDict, "CIGAR_A", "PICK_UP_CIGAR", luxeActStruct.fPickUpCigarFrontPhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sCigarAnimBackDict, "CIGAR_A", "PUT_DOWN_CIGAR", luxeActStruct.fPutDownCigarBackPhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sCigarAnimFrontDict, "CIGAR_A", "PUT_DOWN_CIGAR", luxeActStruct.fPutDownCigarFrontPhase, luxeActStruct.fEndPhaseTime)
//			ENDIF
//			
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPickUpBottleBackPhase ", luxeActStruct.fPickUpBottleBackPhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPutDownBottleBackPhase ", luxeActStruct.fPutDownBottleBackPhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPickUpBottleFrontPhase ", luxeActStruct.fPickUpBottleFrontPhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPutDownBottleFrontPhase ", luxeActStruct.fPutDownBottleFrontPhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPickUpFluteBackPhase ", luxeActStruct.fPickUpFluteBackPhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPutDownFluteBackPhase ", luxeActStruct.fPutDownFluteBackPhase)					
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPickUpFluteFrontPhase ", luxeActStruct.fPickUpFluteFrontPhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPutDownFluteFrontPhase ", luxeActStruct.fPutDownFluteFrontPhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPickUpCigarBackPhase ", luxeActStruct.fPickUpCigarBackPhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPickUpCigarFrontPhase ", luxeActStruct.fPickUpCigarFrontPhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPutDownCigarBackPhase ", luxeActStruct.fPutDownCigarBackPhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPutDownCigarFrontPhase ", luxeActStruct.fPutDownCigarFrontPhase)
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: Requesting Anims ")
			RETURN FALSE
		ENDIF
	ELSE
		REQUEST_ANIM_DICT(luxeActStruct.sChampAnimPassengerSideDict)
		REQUEST_ANIM_DICT(luxeActStruct.sChampAnimDriverSideDict)
		
		IF HAS_ANIM_DICT_LOADED(luxeActStruct.sChampAnimPassengerSideDict)
		AND HAS_ANIM_DICT_LOADED(luxeActStruct.sChampAnimDriverSideDict)
			
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "MAINTAIN_LUXE_VEH_ACTIVITIES: anims have loaded ")
//			IF FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimPassengerSideDict, "CHAMPAGNE_A", "PICK_UP_BOTTLE", luxeActStruct.fPickUpBottlePasSidePhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimPassengerSideDict, "CHAMPAGNE_A", "PUT_DOWN_BOTTLE", luxeActStruct.fPutDownBottlePasSidePhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimPassengerSideDict, "CHAMPAGNE_A", "PICK_UP_FLUTE", luxeActStruct.fPickUpFlutePasSidePhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimPassengerSideDict, "CHAMPAGNE_A", "PUT_DOWN_FLUTE", luxeActStruct.fPutDownFlutePasSidePhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimDriverSideDict, "CHAMPAGNE_A", "PICK_UP_BOTTLE", luxeActStruct.fPickUpBottleDriverSidePhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimDriverSideDict, "CHAMPAGNE_A", "PUT_DOWN_BOTTLE", luxeActStruct.fPutDownBottleDriverSidePhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimDriverSideDict, "CHAMPAGNE_A", "PICK_UP_FLUTE", luxeActStruct.fPickUpFluteDriverSidePhase, luxeActStruct.fEndPhaseTime)
//			AND FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimDriverSideDict, "CHAMPAGNE_A", "PUT_DOWN_FLUTE", luxeActStruct.fPutDownFluteDriverSidePhase, luxeActStruct.fEndPhaseTime)
//			ENDIF
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPickUpBottlePasSidePhase ", luxeActStruct.fPickUpBottlePasSidePhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPutDownBottlePasSidePhase ", luxeActStruct.fPutDownBottlePasSidePhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPickUpFlutePasSidePhase ", luxeActStruct.fPickUpFlutePasSidePhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPutDownFlutePasSidePhase ", luxeActStruct.fPutDownFlutePasSidePhase)
//			
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPickUpBottleDriverSidePhase ", luxeActStruct.fPickUpBottleDriverSidePhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPutDownBottleDriverSidePhase ", luxeActStruct.fPutDownBottleDriverSidePhase)					
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPickUpFluteDriverSidePhase ", luxeActStruct.fPickUpFluteDriverSidePhase)
//			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: fPutDownFluteDriverSidePhase ", luxeActStruct.fPutDownFluteDriverSidePhase)

			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: Requesting Anims ")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_1ST_PERSON_ACTIVITY_PROMPT(LUXE_ACT_STRUCT &luxeActStruct)
	INT iGlobalHelpCounter = GET_PACKED_STAT_INT(PACKED_MP_ACTIVITY_HELP_COUNTER)
	
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_VEH_ACT_1ST")
	OR NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_ACT_1ST_SW")
		IF NOT g_OfficeHeliDockData.bDoingCutscene
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_VEH_ACT_1ST")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_ACT_1ST_SW")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_INIT_HELP0")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_INIT_HELP1")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_INIT_HELP2")
					CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_1ST_PERSON_ACTIVITY_PROMPT: Clearing help messages for 3rd person view.")
					
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF iGlobalHelpCounter < 3
				IF luxeActStruct.iLocalHelpCounter = iGlobalHelpCounter
					IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
						PRINT_HELP_FOREVER("LUX_VEH_ACT_1ST")
					ELSE
						PRINT_HELP_FOREVER("LUX_ACT_1ST_SW")
					ENDIF
					
					iGlobalHelpCounter++
					
					SET_PACKED_STAT_INT(PACKED_MP_ACTIVITY_HELP_COUNTER, iGlobalHelpCounter)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF g_OfficeHeliDockData.bDoingCutscene
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_CHAM_CIGAR")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_CHAM_WEB")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_UNKNOWN")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_CIGAR")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_WEB")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_VEH_ACT_1ST")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_ACT_1ST_SW")
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC


// Removed by Alastair C. 07/05/15
// Luxe vehicle lighting is moving to code.

//FUNC BOOL SHOULD_LIGHTS_BE_ON()
//	INT iTime = GET_CLOCK_HOURS()
//	BOOL toReturn = FALSE
//	
//	IF iTime >= 21 OR iTime < 6
//		toReturn = TRUE
//	ENDIF
//	
//	RETURN toReturn
//ENDFUNC
//
//
///// PURPOSE:
/////    Manage the light state of the vehicle.
///// PARAMS:
/////    luxeActStruct - ByREF
/////    bCleanup - Force a light cleanup this frame.
//PROC MAINTAIN_LUXOR_LIGHTS(LUXE_ACT_STRUCT &luxeActStruct, BOOL bCleanup = FALSE)
//	IF luxeActStruct.vehModel = LUXOR2
//	OR luxeActStruct.vehModel = SWIFT2
//		IF NOT IS_ENTITY_DEAD(luxeActStruct.vehicleIndex)
//			IF SHOULD_LIGHTS_BE_ON()
//			AND NOT bCleanup
//				IF luxeActStruct.iLightState != ENUM_TO_INT(SET_VEHICLE_LIGHTS_ON)
//					CPRINTLN(DEBUG_MP_MINIGAME_ACT, "[AMEC] MAINTAIN_LUXOR_LIGHTS - Enabling interior lights for current vehicle.")
//					luxeActStruct.iLightState = ENUM_TO_INT(SET_VEHICLE_LIGHTS_ON)
//				ENDIF
//				SET_VEHICLE_LIGHTS(luxeActStruct.vehicleIndex, SET_VEHICLE_LIGHTS_ON)
//			ELSE
//				IF luxeActStruct.iLightState != ENUM_TO_INT(SET_VEHICLE_LIGHTS_OFF)
//					CPRINTLN(DEBUG_MP_MINIGAME_ACT, "[AMEC] MAINTAIN_LUXOR_LIGHTS - Disabling interior lights for current vehicle.")
//					luxeActStruct.iLightState = ENUM_TO_INT(SET_VEHICLE_LIGHTS_OFF)
//				ENDIF
//				SET_VEHICLE_LIGHTS(luxeActStruct.vehicleIndex, SET_VEHICLE_LIGHTS_OFF)
//				IF (bCleanup) luxeActStruct.iLightState = -1 ENDIF // Cleanup should only be called on exit, reset local data.
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC


/// PURPOSE:
///    Wrapper for removing all clothing items that can interfere with the drinking/smoking animations.
/// PARAMS:
///    pedTarget - Target ped to remove items from.
PROC CLEANUP_PED_CLOTHING_FOR_LUXE_ACTIVITY(PED_INDEX pedTarget)

	CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "[AMEC] CLEANUP_PED_CLOTHING_FOR_ACTIVITY - Removing mask/hood for ped idx: ", NATIVE_TO_INT(pedTarget))

	IF IS_PED_WEARING_PILOT_SUIT(pedTarget, PED_COMP_TEETH)
		CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "[AMEC] CLEANUP_PED_CLOTHING_FOR_ACTIVITY - Attempting to remove oxy mask.")
		SET_PED_COMPONENT_VARIATION(pedTarget, PED_COMP_TEETH, 0, 0)
	ENDIF 
	REMOVE_MP_HEIST_GEAR(pedTarget, HEIST_GEAR_NIGHTVISION)
	CLEAR_PED_PROP(pedTarget, ANCHOR_HEAD)
	SET_PED_HELMET(pedTarget, FALSE)
	REMOVE_PED_HELMET(pedTarget, TRUE)
	SET_PED_COMPONENT_VARIATION(pedTarget, PED_COMP_BERD, 0, 0)
	SET_HOODED_JACKET_STATE(pedTarget, JACKET_HOOD_DOWN)
ENDPROC


/// PURPOSE:
///    Check if my current vehicle is actually in the air, and also has a pilot.
/// RETURNS:
///    TRUE if the player is not in a state to continue the luxe activities.
FUNC BOOL IS_THE_PARTY_OVER(LUXE_ACT_STRUCT& luxeActStruct)

	IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) AND IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		// Yes the party is over, there is not a driver (pilot).
		IF !IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_ACT_BAIL")
			IF luxeActStruct.iLocalHelpCounter != 99
				luxeActStruct.iLocalHelpCounter = 99
				PRINT_HELP("LUX_ACT_BAIL")
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "[AMEC] IS_THE_PARTY_OVER - Party is over, pilot left. Printing help: LUX_ACT_BAIL")
			ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC
PROC REMOVE_PED_WEAPON()
	WEAPON_TYPE eCurrentWeapon
	IF bWeaponSet = FALSE
		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon)
		    IF eCurrentWeapon != WEAPONTYPE_OBJECT
	          PRINTLN("Holstering weapon ", eCurrentWeapon)
			  GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)
	          SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED,TRUE)
			  bWeaponSet = TRUE
	          CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "REMOVE_PED_WEAPON: player set to unarmed")
	          IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
	                FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), FALSE, FALSE)
	          ENDIF
		    ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC RESET_PED_WEAPON
	IF bWeaponSet
		IF NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
		AND NOT GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsClosingVehicleDoor)
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "RESET_PED_WEAPON: Resetting ped weapon")
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)
			bWeaponSet = FALSE
		ENDIF
	ENDIF
ENDPROC
FUNC BOOL SHOULD_VEHICLE_SCRIPT_CLEANUP(LUXE_ACT_STRUCT &luxeActStruct)
	BOOL bCleanupSwiftImmediately = FALSE
	
	IF NOT DOES_ENTITY_EXIST(luxeActStruct.vehicleIndex)
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: DOES_ENTITY_EXIST: FALSE")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_DEEP_WATER(luxeActStruct.vehicleIndex)
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: IS_ENTITY_IN_DEEP_WATER: TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_DEAD(luxeActStruct.vehicleIndex)
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: luxeActStruct.vehicleIndex is no longer driveable")
		RETURN TRUE
	ENDIF

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: NETWORK_IS_GAME_IN_PROGRESS = TRUE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: IS_SKYSWOOP_AT_GROUND = FALSE")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
		RETURN TRUE
	ENDIF

	IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
		IF IS_PLAYER_DEAD(PLAYER_ID())
		OR IS_PED_BEING_JACKED(PLAYER_PED_ID())
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: IS_PLAYER_DEAD = TRUE")
			RETURN TRUE
		ENDIF
	ENDIF

	IF MPGlobalsAmbience.g_bForceFromVehicle = TRUE
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: g_bForceFromVehicle = TRUE")
		RETURN TRUE
	ENDIF

	#IF IS_DEBUG_BUILD
	IF luxeActStruct.bCleanupScriptDebug
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: bCleanupScriptDebug = TRUE")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF NOT IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH(luxeActStruct, luxeActStruct.vehicleIndex, bCleanupSwiftImmediately)
		RESET_PED_WEAPON()
		IF luxeActStruct.vehModel = SWIFT2
		OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel) 
			
			
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), luxeActStruct.vehicleIndex) > 25
			OR g_iLuxeLatestScriptInstanceID != luxeActStruct.iLuxeVehInstanceID
			OR bCleanupSwiftImmediately = TRUE
				IF bCleanupSwiftImmediately = TRUE
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH = FALSE, (bCleanupSwiftImmediately = TRUE)")
				ENDIF
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), luxeActStruct.vehicleIndex) > 25
					CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: Player is greater than 25 metres away from swift")
				ENDIF
				IF g_iLuxeLatestScriptInstanceID != luxeActStruct.iLuxeVehInstanceID
					CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: g_iLuxeLatestScriptInstanceID: ", g_iLuxeLatestScriptInstanceID, ", doesn't equal luxeActStruct.iLuxeVehInstanceID: ", luxeActStruct.iLuxeVehInstanceID)
				ENDIF
				RETURN TRUE
			ELSE
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), luxeActStruct.vehicleIndex) < 25
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: Player is " , GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), luxeActStruct.vehicleIndex), " metres away from swift")
				ENDIF
				IF g_iLuxeLatestScriptInstanceID = luxeActStruct.iLuxeVehInstanceID
					CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: g_iLuxeLatestScriptInstanceID: ", g_iLuxeLatestScriptInstanceID, ", is equal luxeActStruct.iLuxeVehInstanceID: ", luxeActStruct.iLuxeVehInstanceID)
				ENDIF
			ENDIF
		ELSE
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: IS_PLAYER_IN_OR_TRYING_TO_ENTER_LUXE_VEH: = FALSE")
			RETURN TRUE
		ENDIF
		
	ELSE
		REMOVE_PED_WEAPON()
		// Part of 2335971 fix.
		IF luxeActStruct.luxeActivityState = CL_LUXE_ACT_CLEANED_UP	 
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: luxeActivityState = CL_LUXE_ACT_CLEANED_UP, moving to thread kill.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: IS_PLAYER_SCTV = TRUE, cleaning up")
		RETURN TRUE
	ENDIF
	

//	CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_VEHICLE_SCRIPT_CLEANUP: No reason to clean up, therefore = FALSE")

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Wrapper to check all luxe activity prompt messages to see if they are being displayed.
/// RETURNS:
///    TRUE when luxe activity prompt is up.
FUNC BOOL IS_LUXE_HELP_MESSAGE_BEING_DISPLAYED()

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_CHAM_CIGAR")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_CHAM_WEB")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_UNKNOWN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_CIGAR")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_WEB")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Check if the gameplay camera is within the view cone of the luxe activities.
/// RETURNS:
///    TRUE if view is valid.
FUNC BOOL CAN_PLAYER_TRIGGER_LUXE_ACTIVITY(LUXE_ACT_STRUCT &luxeActStruct)

	BOOL bLuxeHelpActive = IS_LUXE_HELP_MESSAGE_BEING_DISPLAYED()
	INT iCorrection
	FLOAT fCamHeading
	
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
	AND luxeActStruct.iSeatID > -1
		IF ARE_STRINGS_EQUAL(luxeActStruct.activitySeats[luxeActStruct.iSeatID].sSeat, "seat_dside_r")
		OR ARE_STRINGS_EQUAL(luxeActStruct.activitySeats[luxeActStruct.iSeatID].sSeat, "seat_pside_r1")
		OR ARE_STRINGS_EQUAL(luxeActStruct.activitySeats[luxeActStruct.iSeatID].sSeat, "seat_pside_r2")
			iCorrection = 180
		ENDIF
	ENDIF
	
	IF luxeActStruct.iSeatID < 0
		
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CAN_PLAYER_TRIGGER_LUXE_ACTIVITY: Invalid seatID")
	
	ENDIF
	
	fCamHeading = ABSF(GET_GAMEPLAY_CAM_RELATIVE_HEADING())
	// Make sure that the player's viewing cone is within acceptable range.
	IF ABSF(fCamHeading - iCorrection) > 45.0
		IF bLuxeHelpActive 
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CAN_PLAYER_TRIGGER_LUXE_ACTIVITY: Player camera outside view cone, disabling help.")
			CLEAR_HELP() 
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Wrapper to include all conditions before a luxe activity can be triggered.
/// PARAMS:
///    eInput - PLAYER_CONTROL input type.
/// RETURNS:
///    TRUE when control is pressed, and conditions allow.
FUNC BOOL IS_LUXE_ACTIVITY_CONTROL_PRESSED(CONTROL_ACTION eInput, LUXE_ACT_STRUCT &luxeActStruct)

	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, eInput)
	AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND (IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING() OR IS_BONNET_CINEMATIC_CAM_RENDERING())
	AND CAN_PLAYER_TRIGGER_LUXE_ACTIVITY(luxeActStruct)
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Check if any existing luxe activity prompts are getting in the way of an update. Also maintains
///    a timer system to prevent single frame help-text popping.
/// RETURNS:
///    TRUE if it is safe to force a help-text update.
FUNC BOOL SHOULD_LUXE_HELP_MESSAGE_BE_DISPLAYED(LUXE_ACT_STRUCT &luxeActStruct)

	IF luxeActStruct.bWaitForBDSync
		CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_LUXE_HELP_MESSAGE_BE_DISPLAYED: bWaitForBDSync = TRUE, waiting for BD to sync before popping help-text up.")
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_PLAYER_TRIGGER_LUXE_ACTIVITY(luxeActStruct)
		RETURN FALSE
	ENDIF
	
	BOOL bLuxeHelpActive
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		IF IS_LUXE_HELP_MESSAGE_BEING_DISPLAYED()
			bLuxeHelpActive = TRUE
		ELSE
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_ACT_1ST_SW")
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_VEH_ACT_1ST")
				RETURN FALSE
			ELSE
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_LUXE_HELP_MESSAGE_BE_DISPLAYED: Luxe FP cam prompts are active, forcing update.")
			ENDIF
		ENDIF
	ENDIF

	IF IS_VEHICLE_IN_WATER(luxeActStruct.vehicleIndex)
		IF bLuxeHelpActive
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_LUXE_HELP_MESSAGE_BE_DISPLAYED: Vehicle in water and luxe help-text prompt is active, disabling help.")
			CLEAR_HELP()
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Don't hammer the help-text system with dupes.
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(luxeActStruct.sPromptMessage)
		RETURN FALSE
	ENDIF
	
	IF bLuxeHelpActive
		// Maintain a timer to prevent rapid fire help-text popping.
		
		IF NOT HAS_NET_TIMER_STARTED(luxeActStruct.timerPromptDelay)
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_LUXE_HELP_MESSAGE_BE_DISPLAYED: Starting new prompt timer.")
			START_NET_TIMER(luxeActStruct.timerPromptDelay)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(luxeActStruct.timerPromptDelay, 2000)
			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_LUXE_HELP_MESSAGE_BE_DISPLAYED: Prompt delay timer has expired, force update.")
			RESET_NET_TIMER(luxeActStruct.timerPromptDelay)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
		
	ENDIF

	RETURN TRUE

ENDFUNC

PROC INIT_SEAT_DATA(LUXE_ACT_STRUCT &luxeActStruct, SEAT_DATA &activitySeat)
//	VECTOR vTemp = GET_WORLD_POSITION_OF_ENTITY_BONE(luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, activitySeat.sSeat))
//	CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: vTemp : ", vTemp )
	IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
		IF IS_BACK_FACING_SEAT(luxeActStruct, activitySeat) // 

			
			activitySeat.vFlutePropPos = GET_ANIM_INITIAL_OFFSET_POSITION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@DRINK@", luxeActStruct.sFluteClip, <<0,0,0>>, <<0,0,0>>)

			activitySeat.vChampBotPropPos = GET_ANIM_INITIAL_OFFSET_POSITION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@DRINK@", luxeActStruct.sBottleClip, <<0,0,0>>, <<0,0,0>>)

			activitySeat.vCigarPropPos = GET_ANIM_INITIAL_OFFSET_POSITION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@SMOKE@", luxeActStruct.sCigarCigarClip, <<0,0,0>>, <<0,0,0>>)

			activitySeat.vAshTrayPropPos = <<0.1108, 0.8483, 0.1573>>
		
			activitySeat.vFlutePropRot = GET_ANIM_INITIAL_OFFSET_ROTATION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@DRINK@", luxeActStruct.sFluteClip, <<0,0,0>>, <<0,0,0>>)
			activitySeat.vChampBotPropRot = GET_ANIM_INITIAL_OFFSET_ROTATION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@DRINK@", luxeActStruct.sBottleClip, <<0,0,0>>, <<0,0,0>>)

			activitySeat.vCigarPropRot = GET_ANIM_INITIAL_OFFSET_ROTATION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@SMOKE@", luxeActStruct.sCigarCigarClip, <<0,0,0>>, <<0,0,0>>)
			activitySeat.vAshTrayPropRot = <<0, 0, 40.4759>>

//			
			
		ELSE  // 
			
			activitySeat.vFlutePropPos = GET_ANIM_INITIAL_OFFSET_POSITION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_FRONT@DRINK@", luxeActStruct.sFluteClip, <<0,0,0>>, <<0,0,0>>)

			activitySeat.vChampBotPropPos = GET_ANIM_INITIAL_OFFSET_POSITION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_FRONT@DRINK@", luxeActStruct.sBottleClip, <<0,0,0>>, <<0,0,0>>)

			activitySeat.vCigarPropPos = GET_ANIM_INITIAL_OFFSET_POSITION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_FRONT@SMOKE@", luxeActStruct.sCigarCigarClip, <<0,0,0>>, <<0,0,0>>)
			activitySeat.vAshTrayPropPos = <<-0.0647, 0.7970, 0.1612>>
			
			activitySeat.vFlutePropRot = GET_ANIM_INITIAL_OFFSET_ROTATION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_FRONT@DRINK@", luxeActStruct.sFluteClip, <<0,0,0>>, <<0,0,0>>)
			activitySeat.vChampBotPropRot = GET_ANIM_INITIAL_OFFSET_ROTATION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_FRONT@DRINK@", luxeActStruct.sBottleClip, <<0,0,0>>, <<0,0,0>>)

			activitySeat.vCigarPropRot = GET_ANIM_INITIAL_OFFSET_ROTATION("ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_FRONT@SMOKE@", luxeActStruct.sCigarCigarClip, <<0,0,0>>, <<0,0,0>>)
			activitySeat.vAshTrayPropRot = <<0, 0, 40.4759>>
			

		ENDIF
	ELSE
		IF IS_PLAYER_IN_DRIVER_SIDE_SEAT(luxeActStruct, activitySeat) // 
			
			activitySeat.vFlutePropPos = GET_ANIM_INITIAL_OFFSET_POSITION("ANIM@MP_PLAYER_INTVEH@HELICOPTER@SWIFT2@RDS@DRINK@", luxeActStruct.sFluteClip, <<0,0,0>>, <<0,0,0>>)

			activitySeat.vChampBotPropPos = GET_ANIM_INITIAL_OFFSET_POSITION("ANIM@MP_PLAYER_INTVEH@HELICOPTER@SWIFT2@RDS@DRINK@", luxeActStruct.sBottleClip, <<0,0,0>>, <<0,0,0>>)

		
			activitySeat.vFlutePropRot = GET_ANIM_INITIAL_OFFSET_ROTATION("ANIM@MP_PLAYER_INTVEH@HELICOPTER@SWIFT2@RDS@DRINK@", luxeActStruct.sFluteClip, <<0,0,0>>, <<0,0,0>>)
			activitySeat.vChampBotPropRot = GET_ANIM_INITIAL_OFFSET_ROTATION("ANIM@MP_PLAYER_INTVEH@HELICOPTER@SWIFT2@RDS@DRINK@", luxeActStruct.sBottleClip, <<0,0,0>>, <<0,0,0>>)



		ELSE   	
			
			
			activitySeat.vFlutePropPos = GET_ANIM_INITIAL_OFFSET_POSITION("ANIM@MP_PLAYER_INTVEH@HELICOPTER@SWIFT2@RPS@DRINK@", luxeActStruct.sFluteClip, <<0,0,0>>, <<0,0,0>>)


			activitySeat.vChampBotPropPos = GET_ANIM_INITIAL_OFFSET_POSITION("ANIM@MP_PLAYER_INTVEH@HELICOPTER@SWIFT2@RPS@DRINK@", luxeActStruct.sBottleClip, <<0,0,0>>, <<0,0,0>>)

		
			activitySeat.vFlutePropRot = GET_ANIM_INITIAL_OFFSET_ROTATION("ANIM@MP_PLAYER_INTVEH@HELICOPTER@SWIFT2@RPS@DRINK@", luxeActStruct.sFluteClip, <<0,0,0>>, <<0,0,0>>)
			activitySeat.vChampBotPropRot = GET_ANIM_INITIAL_OFFSET_ROTATION("ANIM@MP_PLAYER_INTVEH@HELICOPTER@SWIFT2@RPS@DRINK@", luxeActStruct.sBottleClip, <<0,0,0>>, <<0,0,0>>)


		ENDIF
//	activitySeat.vFlutePropPos = activitySeat.vFlutePropPos*0.010 
//	activitySeat.vChampBotPropPos = activitySeat.vChampBotPropPos*0.010 
//	activitySeat.vCigarPropPos = activitySeat.vCigarPropPos*0.010 
//	activitySeat.vAshTrayPropPos = activitySeat.vAshTrayPropPos*0.010 
	ENDIF
ENDPROC

PROC MAINTAIN_LUXE_VEH_ACTIVITIES(SERVER_BROADCAST_DATA &serverBD, PLAYER_BROADCAST_DATA &playerBD[], LUXE_ACT_STRUCT &luxeActStruct)	
//	#IF IS_DEBUG_BUILD
//	IF luxeActStruct.luxeActivityState > CL_LUXE_ACT_INIT
//		SET_SEAT_ANIMS_DATA(serverBD, luxeActStruct)
//	ENDIF
//	#ENDIF

	#IF IS_DEBUG_BUILD
	IF debug_bForceHelpText
		debug_bForceHelpText = FALSE
		PRINT_HELP("MISSING")
	ENDIF
	#ENDIF

	ENABLE_INTERACTION_MENU()
	IF DOES_ENTITY_EXIST(luxeActStruct.vehicleIndex)
		IF luxeActStruct.luxeActivityState != CL_LUXE_ACT_IN_VEH_IN_FRONT
			// Added activity seat check to counter the cleanup range of the Swift
			IF IS_PLAYER_IN_ACTIVITY_SEAT(luxeActStruct)
				IF luxeActStruct.luxeActivityState != CL_LUXE_ACT_IN_VEH_PROMPT_ACT
					DISABLE_SELECTOR_THIS_FRAME()
					DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
					IF luxeActStruct.luxeActivityState != CL_LUXE_ACT_CLEANING_UP
					AND luxeActStruct.luxeActivityState != CL_LUXE_ACT_CLEANED_UP
						DISABLE_INTERACTION_MENU()
					ENDIF
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_PASSENGER_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_LOOK_BEHIND)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_SELECT_PREV_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_EXIT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_CIN_CAM)
				ENDIF
			
				// Disable stuff when in the back of a Luxe vehicle, regardless of activity state.
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_DROP_PROJECTILE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_SPECIAL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_DETONATE)
				DISABLE_INTERACTIONS_THIS_FRAME()
				
//				// Extend use of HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE for when a player is animating
//				IF IS_PLAYER_PLAYING_ANIMATION(luxeActStruct)
//					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
//				ELSE
//					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//				ENDIF
			ENDIF
		ENDIF
		
		iNumberOfSeatsToClean = GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct)

		IF luxeActStruct.luxeActivityState > CL_LUXE_ACT_INIT
		AND luxeActStruct.luxeActivityState != CL_LUXE_ACT_CLEANING_UP
		AND luxeActStruct.luxeActivityState != CL_LUXE_ACT_CLEANED_UP
		//		SET_ALL_PLAYERS_SEAT_DATA(luxeActStruct)
			HANDLE_PROP_VISIBILITY_SWAP(serverBD, luxeActStruct, playerBD)
		ENDIF

		

		//	HANDLE_SYNC_SCENE_CLEANUP_IF_DEAD(serverBD, luxeActStruct)
		IF luxeActStruct.luxeActivityState = CL_LUXE_ACT_GET_SERVER_CHAMP_RESPONSE
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC
		OR luxeActStruct.luxeActivityState = CL_LUXE_CREATE_CIGAR_PROPS
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_PLAY_CIGAR_SYNC
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC

			CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_GET_SERVER_CHAMP_RESPONSE: disbling exit")
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_EXIT)
		ENDIF

		IF luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_PROMPT_ACT
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_PLAY_CIGAR_SYNC
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_GET_SERVER_CHAMP_RESPONSE
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_LAUNCH_INTERNET
		OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_BROWSE_INTERNET
			//TODO - FX
			RUN_VFX_FOR_SYNC(serverBD, luxeActStruct)
		ENDIF

		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), luxeActStruct.vehicleIndex, TRUE)
			IF IS_MP_PASSIVE_MODE_ENABLED()
				CPRINTLN(DEBUG_MP_MINIGAME_ACT, "MAINTAIN_LUXE_VEH_ACTIVITIES: Disabling passive mode & printing warning.")
				PRINT_HELP("HELP_PASSIVE", 4000)
				IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
				OR GET_ENTITY_MODEL(luxeActStruct.vehicleIndex) = AVENGER
					DISABLE_MP_PASSIVE_MODE(PMER_AVENGER_PASSIVE_MODE, FALSE)
				ELSE
					DISABLE_MP_PASSIVE_MODE(PMER_VEHICLE_PASSIVE_MODE, FALSE)
				ENDIF
			ENDIF
			
			IF luxeActStruct.luxeActivityState = CL_LUXE_ACT_CLEANING_UP
			OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_CLEANED_UP
				TURN_OFF_PASSIVE_MODE_OPTION(FALSE)
			ELSE
				TURN_OFF_PASSIVE_MODE_OPTION(TRUE)
			ENDIF
		ELSE
			TURN_OFF_PASSIVE_MODE_OPTION(FALSE)
		ENDIF

		CLIENT_REQUEST_CHAMP_ACT(serverBD, playerBD, luxeActStruct)
	ENDIF
	
	playerBD[NATIVE_TO_INT(PLAYER_ID())].iActivityVariation = luxeActStruct.iClipVariation
	SWITCH luxeActStruct.luxeActivityState
		CASE CL_LUXE_ACT_INIT
			IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
			OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel) 

//				luxeActStruct.clientRequestStage = CR_REQUESTING
				playerBD[NATIVE_TO_INT(PLAYER_ID())].clientRequestStage = CR_CHAMP_IDLE
				
				playerBD[NATIVE_TO_INT(PLAYER_ID())].bRequestChampAct = FALSE
				
////				luxeActStruct.iPromptContextIntention = NEW_CONTEXT_INTENTION
				IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
					luxeActStruct.sPromptMessage = "POD_CHAM_CIGAR"
				ELSE
					luxeActStruct.sPromptMessage = "POD_CHAM_WEB"
				ENDIF
				
//				luxeActStruct.playAnim.type = APT_SINGLE_ANIM
					
							
							
							
				IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
					luxeActStruct.activitySeats[0].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 1), luxeActStruct)
					luxeActStruct.activitySeats[0].vehicleSeatID = 1
					
					luxeActStruct.activitySeats[1].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 2), luxeActStruct)
					luxeActStruct.activitySeats[1].vehicleSeatID = 2
					
					luxeActStruct.activitySeats[2].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 3), luxeActStruct)
					luxeActStruct.activitySeats[2].vehicleSeatID = 3
					
					luxeActStruct.activitySeats[3].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 4), luxeActStruct)
					luxeActStruct.activitySeats[3].vehicleSeatID = 4
					
					luxeActStruct.activitySeats[4].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 6), luxeActStruct)
					luxeActStruct.activitySeats[4].vehicleSeatID = 6
					
					luxeActStruct.activitySeats[5].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 8), luxeActStruct)
					luxeActStruct.activitySeats[5].vehicleSeatID = 8
					
				ELSE
					luxeActStruct.activitySeats[0].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 1), luxeActStruct)
					luxeActStruct.activitySeats[0].vehicleSeatID = 1
					
					luxeActStruct.activitySeats[1].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 2), luxeActStruct) 
					luxeActStruct.activitySeats[1].vehicleSeatID = 2
				ENDIF
				
//				INT jSeat
//				jSeat = 0
//				
//				WHILE jSeat < GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct)
//					
//					INIT_SEAT_DATA(luxeActStruct, luxeActStruct.activitySeats[jSeat])
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_INIT: INIT_SEAT_DATA: ", jSeat, " ", serverBD.activitySeats[jSeat].sSeat)
//					jSeat++	
//				ENDWHILE			
							
							
							
				
//				IF HAS_NET_TIMER_STARTED(luxeActStruct.champAudioTimer)
//					
//					luxeActStruct.champAudioTimer
//				ENDIF
				
				IF IS_PLAYER_IN_LUX_JET_VEHICLE(luxeActStruct.vehModel)
				
					IF REQUEST_AMBIENT_AUDIO_BANK("DLC_LUXE/LUXE_DRINK_CHAMPAGNE", FALSE)
					AND REQUEST_AMBIENT_AUDIO_BANK("DLC_LUXE/LUXE_SMOKE_CIGAR", FALSE)
						luxeActStruct.bAudioBanksRequested = TRUE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "MAINTAIN_LUXE_VEH_ACTIVITIES: requested audio")
					ELSE
						EXIT
					ENDIF
				
					/*
					IF NOT REQUEST_AMBIENT_AUDIO_BANK("DLC_LUXE/LUXE_DRINK_CHAMPAGNE", FALSE)
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: failed to load audio bank: LUXE_DRINK_CHAMPAGNE")
//						IF NOT HAS_NET_TIMER_STARTED(luxeActStruct.champAudioTimer)
//							START_NET_TIMER(luxeActStruct.champAudioTimer,TRUE)
//							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: champAudioTimer started")
//						ELSE
//							IF HAS_NET_TIMER_EXPIRED_READ_ONLY(luxeActStruct.champAudioTimer,4000,TRUE)
//								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: champAudioTimer expired")
//								LUXE_SCRIPT_CLEANUP(serverBD, luxeActStruct)
//							ENDIF	
//						ENDIF
						EXIT
					ELSE
//						IF HAS_NET_TIMER_STARTED(luxeActStruct.champAudioTimer)
//							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: champAudioTimer reset")
//							RESET_NET_TIMER(luxeActStruct.champAudioTimer)
//						ENDIF
					ENDIF
				
					IF NOT REQUEST_AMBIENT_AUDIO_BANK("DLC_LUXE/LUXE_SMOKE_CIGAR", FALSE)
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: failed to load audio bank: LUXE_SMOKE_CIGAR")
//						IF NOT HAS_NET_TIMER_STARTED(luxeActStruct.cigarAudioTimer)
//							START_NET_TIMER(luxeActStruct.cigarAudioTimer,TRUE)
//							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: cigarAudioTimer started")
//						ELSE
//							IF HAS_NET_TIMER_EXPIRED_READ_ONLY(luxeActStruct.cigarAudioTimer,4000,TRUE)
//								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: cigarAudioTimer expired")
//								LUXE_SCRIPT_CLEANUP(serverBD, luxeActStruct)
//							ENDIF	
//						ENDIF
						EXIT
					ELSE
//						IF HAS_NET_TIMER_STARTED(luxeActStruct.cigarAudioTimer)
//							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: cigarAudioTimer reset")
//							RESET_NET_TIMER(luxeActStruct.cigarAudioTimer)
//						ENDIF
					ENDIF
					*/
					
				ELSE
					IF REQUEST_AMBIENT_AUDIO_BANK("DLC_LUXE/LUXE_DRINK_CHAMPAGNE", FALSE)
						luxeActStruct.bAudioBanksRequested = TRUE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "MAINTAIN_LUXE_VEH_ACTIVITIES: requested audio")
					ELSE
						EXIT
					ENDIF
				ENDIF
				
				REQUEST_NAMED_PTFX_ASSET("scr_mp_cig_plane")
				
				SET_ANIM_DICTS_AND_CLIPS(serverBD, luxeActStruct)
				IF REQUEST_ANIM_DICTS_AND_GET_TAGS(luxeActStruct)
					IF luxeActStruct.iCreationSeatNum < GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct) // should be 6
						INIT_SEAT_DATA(luxeActStruct, luxeActStruct.activitySeats[luxeActStruct.iCreationSeatNum])
						IF CREATE_LOCAL_DUMMY_PROPS(luxeActStruct, luxeActStruct.activitySeats[luxeActStruct.iCreationSeatNum])
							IF IS_BACK_FACING_SEAT(luxeActStruct, serverBD.activitySeats[luxeActStruct.iCreationSeatNum])
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: Props created for back facing seat: ", luxeActStruct.iCreationSeatNum, " name: ", serverBD.activitySeats[luxeActStruct.iCreationSeatNum].sSeat, " ID: ", luxeActStruct.iCreationSeatNum)	
							ELSE
								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: Props created for front facing seat: ", luxeActStruct.iCreationSeatNum, " name: ", serverBD.activitySeats[luxeActStruct.iCreationSeatNum].sSeat, " ID: ", luxeActStruct.iCreationSeatNum)	
							ENDIF
							luxeActStruct.iLoadPropFail = 0
							luxeActStruct.iCreationSeatNum++	
						ELSE
							luxeActStruct.iLoadPropFail++
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: Failed to create props for seat : ", luxeActStruct.iCreationSeatNum,", attempt: ", luxeActStruct.iLoadPropFail)
						ENDIF
						
					ENDIF
					
					IF luxeActStruct.iLoadPropFail = 100
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: Failed to create props for seat after 100 attemps, CLEANUPING UP SCRIPT")
						LUXE_SCRIPT_CLEANUP(serverBD, luxeActStruct, playerBD)
					ENDIF
					
					IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: serverBD.activitySeats[1].propChampBotAnim = serverBD.activitySeats[2].propChampBotAnim ")	

						luxeActStruct.activitySeats[2].propChampBotIdle = luxeActStruct.activitySeats[0].propChampBotIdle 

						luxeActStruct.activitySeats[1].propChampBotIdle = luxeActStruct.activitySeats[3].propChampBotIdle
						
						luxeActStruct.activitySeats[4].propChampBotIdle = luxeActStruct.activitySeats[5].propChampBotIdle

					ELSE
						luxeActStruct.activitySeats[0].propChampBotIdle = luxeActStruct.activitySeats[1].propChampBotIdle
					ENDIF
					
					
					
					IF luxeActStruct.iCreationSeatNum = GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct) // should be 6
						SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH)
					ENDIF 
				ENDIF
			ENDIF
		BREAK
				
		CASE CL_LUXE_ACT_IN_VEH		
			
			IF NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
				IF ENUM_TO_INT(GET_VEHICLE_SEAT(luxeActStruct)) != -3
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH: player is in seat: ", ENUM_TO_INT(GET_VEHICLE_SEAT(luxeActStruct)))
					IF IS_PLAYER_IN_ACTIVITY_SEAT(luxeActStruct)
						SET_PLAYER_SEAT_AND_TABLE_DATA(luxeActStruct, playerBD)
						SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_PROMPT_ACT)
					ELIF IS_PLAYER_IN_COCKPIT(luxeActStruct)
						SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_IN_FRONT)
					ELSE	
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH: player is not in any seat")
					ENDIF
					
				ELSE
					CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH: player is not in any seat yet")
				ENDIF
			ELSE
//				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH: player is not in any seat yet because player is exiting")
				IF g_bInLuxeVehicle
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH: Player isn't in lux vehicle, g_bInLuxeVehicle to FALSE.")
					g_bInLuxeVehicle = FALSE
				ENDIF
				luxeActStruct.iSeatID = -1
				playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID = luxeActStruct.iSeatID
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "SET_PLAYER_SEAT_AND_TABLE_DATA: no seat assigned reset to: ", luxeActStruct.iSeatID)
			ENDIF
		BREAK
		
		CASE CL_LUXE_ACT_IN_VEH_PROMPT_ACT
			SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE, FALSE)
			
//			ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(serverBD.activitySeats[1].propCigar), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, "seat_pside_r1"), serverBD.activitySeats[1].vCigarPropPos, serverBD.activitySeats[1].vCigarPropRot)
//			IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
				IF IS_PLAYER_IN_ACTIVITY_SEAT(luxeActStruct)
				AND NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
				AND NOT GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsClosingVehicleDoor)
					IF NOT IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_HUNT_THE_BEAST)
						IF NOT g_bInLuxeVehicle
							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH: Setting g_bInLuxeVehicle to TRUE becuase player is in passenger seat.")
							g_bInLuxeVehicle = TRUE 
						ENDIF
					
						IF IS_BROWSER_OPEN()
							CPRINTLN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: g_bBrowserVisible = TRUE, exit prompt state, move to browse.")
							SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_BROWSE_INTERNET)
							EXIT
						ENDIF
						
						luxeActStruct.bWaitForBDSync = FALSE
						
						IF serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen != INT_TO_NATIVE(PLAYER_INDEX, -1)
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: champ activity for table: ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber, " has been assigned to: ", GET_PLAYER_NAME(serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen))
							
							IF serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen = PLAYER_ID()
								CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: champ activity has been asigned to me, setting bWaitForBDSync to TRUE.")
								luxeActStruct.bWaitForBDSync = TRUE
							ENDIF
							
						ELIF serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1)
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: champ activity champIDPlayerChosen has been asigned to = -1")
						ELSE
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: champ activity for table : ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber, " has been assigned to: ", GET_PLAYER_NAME(serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen))
						ENDIF
						
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						AND serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1)
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: in Champ/Cigar(or just champ for Swift) stage ")
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: Champ/Cigar serverBD.serverChampCheckStage: ", LuxeServerChampCheckString(serverBD.serverChampCheckStage))
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: Champ/Cigar - Player cam view mode: ", ENUM_TO_INT(GET_FOLLOW_PED_CAM_VIEW_MODE()))

							IF NOT IS_THE_PARTY_OVER(luxeActStruct)
								IF IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING()
								OR IS_BONNET_CINEMATIC_CAM_RENDERING()
								//OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_FIRST_PERSON
									
									IF IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel)
										luxeActStruct.sPromptMessage = "POD_CHAM_WEB" 
									ENDIF
									IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
										luxeActStruct.sPromptMessage = "POD_CHAM_CIGAR"
									ENDIF
								
									IF IS_PHONE_ONSCREEN()
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(luxeActStruct.sPromptMessage)
											CLEAR_HELP() // Hide prompt when phone is up ONLY for this state's prompt type.
										ELSE
											// Don't clear help as we have no idea what help it is. Comment out if this gets too spammy.
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: IS_PHONE_ONSCREEN = TRUE, UNKNOWN helptext is active, do nothing.")
										ENDIF
									ELSE
										IF SHOULD_LUXE_HELP_MESSAGE_BE_DISPLAYED(luxeActStruct)
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: Update prompt label: ", luxeActStruct.sPromptMessage)
											CLEAR_HELP()
											PRINT_HELP_FOREVER(luxeActStruct.sPromptMessage)
										ELSE
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: Cannot update prompt, different help text is blocking.")
										ENDIF
									ENDIF
									
								ELSE
									HANDLE_1ST_PERSON_ACTIVITY_PROMPT(luxeActStruct)
								ENDIF
							ENDIF
							
							IF IS_LUXE_ACTIVITY_CONTROL_PRESSED(INPUT_CONTEXT, luxeActStruct)
								CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_GET_SERVER_CHAMP_RESPONSE: disbling exit")
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_EXIT)
								DISABLE_INTERACTION_MENU()
								
	//							SET_PED_WEAPONS_MP()
								SET_SEAT_ANIMS_DATA(serverBD, luxeActStruct, playerBD)
								
								SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE, FALSE)
								
								CLEANUP_PED_CLOTHING_FOR_LUXE_ACTIVITY(PLAYER_PED_ID())
	//							luxeActStruct.playAnim.dictionary0 = luxeActStruct.sChampAnimDict
								GET_RANDOM_CHAMP_CLIP_VARIATION(serverBD, luxeActStruct)
	//							GET_CLIP_TAG_PHASE(serverBD, luxeActStruct)
	//							luxeActStruct.playAnim.anim0 = luxeActStruct.sChampClip
								
								IF IS_LUXE_HELP_MESSAGE_BEING_DISPLAYED()
									CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: CHAMP - Calling CLEAR_HELP for visible help text label: ", luxeActStruct.sPromptMessage)
									CLEAR_HELP()
								ENDIF
								
	//							RELEASE_CONTEXT_INTENTION(luxeActStruct.iPromptContextIntention)
								luxeActStruct.bRequestChamp = TRUE
								SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_GET_SERVER_CHAMP_RESPONSE)	
							ENDIF
							IF IS_PLAYER_IN_LUX_JET_VEHICLE( luxeActStruct.vehModel)
								IF IS_LUXE_ACTIVITY_CONTROL_PRESSED(INPUT_CONTEXT_SECONDARY, luxeActStruct)
									CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_GET_SERVER_CHAMP_RESPONSE: disbling exit")
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_EXIT)
									DISABLE_INTERACTION_MENU()
									SET_SEAT_ANIMS_DATA(serverBD, luxeActStruct, playerBD)
									
									SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE, FALSE)
									
									CLEANUP_PED_CLOTHING_FOR_LUXE_ACTIVITY(PLAYER_PED_ID())
				//					luxeActStruct.playAnim.dictionary0 = "ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@SMOKE@"	
				//					luxeActStruct.playAnim.anim0 = "CIGAR_A"
	//								luxeActStruct.playAnim.dictionary0 = luxeActStruct.sCigarAnimDict
									GET_RANDOM_CIGAR_CLIP_VARIATION(serverBD, luxeActStruct)
	//								GET_CLIP_TAG_PHASE(serverBD, luxeActStruct)
	//								luxeActStruct.playAnim.anim0 = luxeActStruct.sCigarClip
									CLEANUP_PED_CLOTHING_FOR_LUXE_ACTIVITY(PLAYER_PED_ID())
	//								RELEASE_CONTEXT_INTENTION(luxeActStruct.iPromptContextIntention)
									
									IF IS_LUXE_HELP_MESSAGE_BEING_DISPLAYED()
										CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: LUXOR2 - Calling CLEAR_HELP for visible help text label: ", luxeActStruct.sPromptMessage)
										CLEAR_HELP()
									ENDIF
									
									SetLuxeActStage(luxeActStruct, CL_LUXE_CREATE_CIGAR_PROPS)															
								ENDIF
							ENDIF
							IF luxeActStruct.vehModel = SWIFT2
							OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel) 
								IF IS_LUXE_ACTIVITY_CONTROL_PRESSED(INPUT_CONTEXT_SECONDARY, luxeActStruct)
									DISABLE_INTERACTION_MENU()
									SET_SEAT_ANIMS_DATA(serverBD, luxeActStruct, playerBD)
									
									SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE, FALSE)
									
									CLEANUP_PED_CLOTHING_FOR_LUXE_ACTIVITY(PLAYER_PED_ID())
				//					luxeActStruct.playAnim.dictionary0 = "ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@SMOKE@"	
				//					luxeActStruct.playAnim.anim0 = "CIGAR_A"
		//							luxeActStruct.playAnim.dictionary0 = luxeActStruct.sCigarAnimDict
		//							luxeActStruct.playAnim.anim0 = "CIGAR_A"
									
	//								RELEASE_CONTEXT_INTENTION(luxeActStruct.iPromptContextIntention)

									IF IS_LUXE_HELP_MESSAGE_BEING_DISPLAYED()
										CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: SWIFT2 - Calling CLEAR_HELP for visible help text label: ", luxeActStruct.sPromptMessage)
										CLEAR_HELP()
									ENDIF
									
									SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_LAUNCH_INTERNET)
								ENDIF
							ENDIF
							
						ELIF luxeActStruct.vehModel != SWIFT2
						AND NOT IS_PLAYER_IN_LUX_HELI_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						AND serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen != INT_TO_NATIVE(PLAYER_INDEX, -1)
						AND serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen != PLAYER_ID()
							
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: In cigar prompt stage.")

							IF NOT IS_THE_PARTY_OVER(luxeActStruct)
								IF IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING()
								OR IS_BONNET_CINEMATIC_CAM_RENDERING()
								//OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_FIRST_PERSON
								
									// Entering this state means that the only help-text getting displayed needs to be
									// the one that offers only the cigar.
									luxeActStruct.sPromptMessage = "POD_CIGAR"
								
									IF IS_PHONE_ONSCREEN()
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(luxeActStruct.sPromptMessage)
											CLEAR_HELP() // Hide prompt when phone is up ONLY for this state's prompt type.
										ELSE
											// Don't clear help as we have no idea what help it is. Comment out if this gets too spammy.
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: IS_PHONE_ONSCREEN = TRUE, UNKNOWN helptext is active, do nothing.")
										ENDIF
									ELSE
										IF SHOULD_LUXE_HELP_MESSAGE_BE_DISPLAYED(luxeActStruct)
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: Update prompt label: ", luxeActStruct.sPromptMessage)
											CLEAR_HELP()
											PRINT_HELP_FOREVER(luxeActStruct.sPromptMessage)
										ELSE
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: Cannot update prompt, different help text is blocking.")
										ENDIF
									ENDIF
									
								ELSE
									HANDLE_1ST_PERSON_ACTIVITY_PROMPT(luxeActStruct)
								ENDIF
							ENDIF
							
							IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
								IF IS_LUXE_ACTIVITY_CONTROL_PRESSED(INPUT_CONTEXT_SECONDARY, luxeActStruct)
									CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_GET_SERVER_CHAMP_RESPONSE: disbling exit")
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_EXIT)
									DISABLE_INTERACTION_MENU()
									
									SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE, FALSE)
									
									CLEANUP_PED_CLOTHING_FOR_LUXE_ACTIVITY(PLAYER_PED_ID())
									SET_SEAT_ANIMS_DATA(serverBD, luxeActStruct, playerBD)
	//								luxeActStruct.playAnim.dictionary0 = luxeActStruct.sCigarAnimDict
									GET_RANDOM_CIGAR_CLIP_VARIATION(serverBD, luxeActStruct)
	//								GET_CLIP_TAG_PHASE(serverBD, luxeActStruct)
	//								luxeActStruct.playAnim.anim0 = luxeActStruct.sCigarClip
									
									IF IS_LUXE_HELP_MESSAGE_BEING_DISPLAYED()
										CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: !SWIFT2 - Calling CLEAR_HELP for visible help text label: ", luxeActStruct.sPromptMessage)
										CLEAR_HELP()
									ENDIF
									
	//								CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: releasing context intention")
									SetLuxeActStage(luxeActStruct, CL_LUXE_CREATE_CIGAR_PROPS)															
								ENDIF
							ENDIF
							
						ELIF (luxeActStruct.vehModel = SWIFT2
						OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel)
						)
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK // if we're in the swift and someone else has just used the champgane
						AND serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1)
						
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: swift, champ & web ")

							IF NOT IS_THE_PARTY_OVER(luxeActStruct)
								IF IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING()
								OR IS_BONNET_CINEMATIC_CAM_RENDERING()
								//OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_FIRST_PERSON
									
									// Entering this state means that the only help-text getting displayed needs to be
									// the one that offers both champ and web at once.
									luxeActStruct.sPromptMessage = "POD_CHAM_WEB"
								
									IF IS_PHONE_ONSCREEN()
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(luxeActStruct.sPromptMessage)
											CLEAR_HELP() // Hide prompt when phone is up ONLY for this state's prompt type.
										ELSE
											// Don't clear help as we have no idea what help it is. Comment out if this gets too spammy.
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: IS_PHONE_ONSCREEN = TRUE, UNKNOWN helptext is active, do nothing.")
										ENDIF
									ELSE
										IF SHOULD_LUXE_HELP_MESSAGE_BE_DISPLAYED(luxeActStruct)
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: Update prompt label: ", luxeActStruct.sPromptMessage)
											CLEAR_HELP()
											PRINT_HELP_FOREVER(luxeActStruct.sPromptMessage)
										ELSE
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: Cannot update prompt, different help text is blocking.")
										ENDIF
									ENDIF
									
								ELSE
									HANDLE_1ST_PERSON_ACTIVITY_PROMPT(luxeActStruct)
								ENDIF
							ENDIF
							
							IF IS_LUXE_ACTIVITY_CONTROL_PRESSED(INPUT_CONTEXT_SECONDARY, luxeActStruct)
								
								DISABLE_INTERACTION_MENU()
								
								SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE, FALSE)
								
								CLEANUP_PED_CLOTHING_FOR_LUXE_ACTIVITY(PLAYER_PED_ID())
								SET_SEAT_ANIMS_DATA(serverBD, luxeActStruct, playerBD)
								
	//								RELEASE_CONTEXT_INTENTION(luxeActStruct.iPromptContextIntention)
								
								IF IS_LUXE_HELP_MESSAGE_BEING_DISPLAYED()
									CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: SWIFT2 CHAMP - Calling CLEAR_HELP for visible help text label: ", luxeActStruct.sPromptMessage)
									CLEAR_HELP()
								ENDIF
								
								SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_LAUNCH_INTERNET)
							ENDIF
						ELIF (luxeActStruct.vehModel = SWIFT2
						OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel) 
						)
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK // if we're in the swift and someone else has just used the champgane
						AND serverBD.tablesData[playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableNumber].champIDPlayerChosen != INT_TO_NATIVE(PLAYER_INDEX, -1)
						
							CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: swift web ")
						
							IF NOT IS_THE_PARTY_OVER(luxeActStruct)
								IF IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING()
								OR IS_BONNET_CINEMATIC_CAM_RENDERING()
								//OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_FIRST_PERSON	
									// Entering this state means that the only help-text getting displayed needs to be
									// the one that offers both champ and web at once.
									luxeActStruct.sPromptMessage = "POD_WEB"
								
									IF IS_PHONE_ONSCREEN()
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(luxeActStruct.sPromptMessage)
											CLEAR_HELP() // Hide prompt when phone is up ONLY for this state's prompt type.
										ELSE
											// Don't clear help as we have no idea what help it is. Comment out if this gets too spammy.
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: IS_PHONE_ONSCREEN = TRUE, UNKNOWN helptext is active, do nothing.")
										ENDIF
									ELSE
										IF SHOULD_LUXE_HELP_MESSAGE_BE_DISPLAYED(luxeActStruct)
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: Update prompt label: ", luxeActStruct.sPromptMessage)
											CLEAR_HELP()
											PRINT_HELP_FOREVER(luxeActStruct.sPromptMessage)
										ELSE
											CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: Cannot update prompt, different help text is blocking.")
										ENDIF
									ENDIF
									
								ELSE
									HANDLE_1ST_PERSON_ACTIVITY_PROMPT(luxeActStruct)
								ENDIF
							ENDIF
							
							IF IS_LUXE_ACTIVITY_CONTROL_PRESSED(INPUT_CONTEXT_SECONDARY, luxeActStruct)
							
								DISABLE_INTERACTION_MENU()
								
								SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE, FALSE)
								
								CLEANUP_PED_CLOTHING_FOR_LUXE_ACTIVITY(PLAYER_PED_ID())
								SET_SEAT_ANIMS_DATA(serverBD, luxeActStruct, playerBD)
								
	//								RELEASE_CONTEXT_INTENTION(luxeActStruct.iPromptContextIntention)
								
								IF IS_LUXE_HELP_MESSAGE_BEING_DISPLAYED()
									CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: SWIFT2 INTERNET - Calling CLEAR_HELP for visible help text label: ", luxeActStruct.sPromptMessage)
									CLEAR_HELP()
								ENDIF
								
								SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_LAUNCH_INTERNET)
							ENDIF
						ELSE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(luxeActStruct.sPromptMessage)
								CLEAR_HELP()
							ENDIF
						ENDIF
					ELSE
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP()
						ENDIF
					ENDIF
				ELSE
					
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_PED_ID(), FMMC_TYPE_HUNT_THE_BEAST) = TRUE")
						CLEAR_HELP()
					ENDIF
					SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH)
				ENDIF
//			ELSE
//				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PROMPT_ACT: ped is shooting")
//			ENDIF
		BREAK
		
		CASE CL_LUXE_ACT_GET_SERVER_CHAMP_RESPONSE
			
			IF playerBD[NATIVE_TO_INT(PLAYER_ID())].clientRequestStage = CR_PLAY_CHAMP
			
				RESET_NET_TIMER(luxeActStruct.animSafetyTimeout)
			
				setLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC)	
			ELIF playerBD[NATIVE_TO_INT(PLAYER_ID())].clientRequestStage = CR_CHAMP_BUSY
				setLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_PROMPT_ACT)	
			ENDIF
		BREAK
		
//		CASE CL_LUXE_ACT_IN_VEH_PLAY_CIGAR
//			
//			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
//			AND GRAB_CIGAR_PROPS(serverBD, luxeActStruct)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CIGAR: Starting anim")
//				
//	//			playAnim.seq
//				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CIGAR: dictionary0: ", luxeActStruct.playAnim.dictionary0, ", anim0: ", luxeActStruct.playAnim.anim0)
//				SEQUENCE_INDEX animSeq
//				OPEN_SEQUENCE_TASK(animSeq)	
//					TASK_SCRIPTED_ANIMATION(NULL, luxeActStruct.playAnim, luxeActStruct.none, luxeActStruct.none)		
////					TASK_PLAY_ANIM(NULL, "ANIM@MP_PLAYER_INTVEH@PLANE@LUXOR2@REAR_BACK@DRINK@", "CHAMPAGNE_A", DEFAULT, DEFAULT, DEFAULT, AF_UPPERBODY)
//				CLOSE_SEQUENCE_TASK(animSeq)
//				
//				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), animSeq)
//				CLEAR_SEQUENCE_TASK(animSeq)
//				
//				luxeActStruct.bFinishUsingProps = FALSE
//				SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_POLL_ANIM_FOR_TAGS_CIGAR)
//			
//			ENDIF
//		BREAK
		
		CASE CL_LUXE_ACT_LAUNCH_INTERNET
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(luxeActStruct.sPromptMessage)
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "BROWSE INTERNET: Calling CLEAR_HELP for visible help text label: ", luxeActStruct.sPromptMessage)
			ELIF NOT IS_STRING_NULL_OR_EMPTY(luxeActStruct.sPromptMessage)
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "BROWSE INTERNET: Calling CLEAR_HELP for active help text label: ", luxeActStruct.sPromptMessage)
			ELSE
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "BROWSE INTERNET: Calling CLEAR_HELP for empty help text label.")
			ENDIF
			CLEAR_HELP()
			
			CPRINTLN(DEBUG_MP_MINIGAME_ACT, "BROWSE INTERNET - <0> starting browser...")
			
			START_BROWSER()
			
			CPRINTLN(DEBUG_MP_MINIGAME_ACT, "BROWSE INTERNET - <1> waiting for browser...")
			
			IF IS_BROWSER_OPEN() OR g_bBrowserQuitMessage
				SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_BROWSE_INTERNET)
			ENDIF
		BREAK
		
		CASE CL_LUXE_ACT_BROWSE_INTERNET
		
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(luxeActStruct.sPromptMessage)
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "BROWSE INTERNET: Calling CLEAR_HELP for visible help text label: ", luxeActStruct.sPromptMessage)
				CLEAR_HELP()
			ENDIF
			
			IF IS_BROWSER_OPEN()
				CPRINTLN(DEBUG_MP_MINIGAME_ACT, "BROWSE INTERNET - <3> browser visible...")
				START_NET_TIMER(appInternetCloseDelay)
			ELSE
				CPRINTLN(DEBUG_MP_MINIGAME_ACT, "BROWSE INTERNET - <3.5> running 1 sec delay...")
				IF HAS_NET_TIMER_EXPIRED(appInternetCloseDelay, 1000)
					CPRINTLN(DEBUG_MP_MINIGAME_ACT, "BROWSE INTERNET - <4> browser not visible...")
					SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_PROMPT_ACT)
				ENDIF
			ENDIF

		BREAK
		
//		CASE CL_LUXE_ACT_POLL_ANIM_FOR_TAGS_CIGAR
//			IF TAKE_CONTROL_OF_NET_ID(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim)
//				IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), luxeActStruct.playAnim.dictionary0, luxeActStruct.playAnim.anim0) >= luxeActStruct.fPickUpCigarPhase
//				AND NOT luxeActStruct.bFinishUsingProps
//					SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_PICK_UP_CIGAR)
//				ELSE
//					CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "playAnim.Phase: ", GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), luxeActStruct.playAnim.dictionary0, luxeActStruct.playAnim.anim0), ", fPickUpCigarPhase: ", luxeActStruct.fPickUpCigarPhase)
//				ENDIF
//			ELSE
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "Don't have control of Cigar")
//			ENDIF
//		BREAK

//		CASE CL_LUXE_ACT_PICK_UP_CIGAR
//			IF NOT IS_ENTITY_DEAD(luxeActStruct.vehicleIndex)
//				CIGAR_PROP_VISIBILITY_SWAP(TRUE, luxeActStruct)
//				
//				IF NOT luxeActStruct.bCigarAttached
//					ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim), PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE) 
//					luxeActStruct.bCigarAttached = TRUE
//				ELIF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), luxeActStruct.playAnim.dictionary0, luxeActStruct.playAnim.anim0) >= luxeActStruct.fPutDownCigarPhase
//					CIGAR_PROP_VISIBILITY_SWAP(FALSE, luxeActStruct)
//					CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "playAnim.Phase: ", GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), luxeActStruct.playAnim.dictionary0, luxeActStruct.playAnim.anim0), ", fPutDownCigarBackPhase: ", luxeActStruct.fPutDownCigarPhase)
//					DETACH_ENTITY(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim))
//					ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, serverBD.activitySeats[luxeActStruct.iSeatID].sSeat), serverBD.activitySeats[luxeActStruct.iSeatID].vCigarPropPos, serverBD.activitySeats[luxeActStruct.iSeatID].vCigarPropRot)
//					luxeActStruct.bCigarAttached = FALSE
//					luxeActStruct.bFinishUsingProps = TRUE
//					SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_RELEASE_CIGAR)
//				ENDIF
//			ENDIF
//		BREAK
		
//		CASE CL_LUXE_ACT_RELEASE_CIGAR
//			bLighterStart = FALSE
//			bSparkStart = FALSE
//			bSecondSpark = FALSE
//			IF RELEASE_CIGAR_PROPS(serverBD, luxeActStruct)
//				SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_PROMPT_ACT)
//			ENDIF
//		BREAK

		CASE CL_LUXE_CREATE_CIGAR_PROPS
			IF HAS_SERVER_CREATED_NETWORK_CIGAR_PROPS(serverBD, playerBD, luxeActStruct)
				SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_PLAY_CIGAR_SYNC)
			ELSE
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_CREATE_CIGAR_PROPS: server props haven't been created yet ")
			ENDIF
		BREAK
		CASE CL_LUXE_ACT_IN_VEH_PLAY_CIGAR_SYNC
			IF GRAB_CIGAR_PROPS(serverBD, luxeActStruct)
			//and load lighter prop
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP: CHAMP_PROP_VISIBILITY_SWAP: seatID: ", luxeActStruct.iSeatID)
				
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP: CHAMP_PROP_VISIBILITY_SWAP")
				
				
				luxeActStruct.vehicleIndex = luxeActStruct.vehicleIndex
				luxeActStruct.iNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(<<0,0,0>>, <<0,0,0>>, DEFAULT, FALSE)
				
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP: luxeActStruct.sCigarAnimDict: ", luxeActStruct.sCigarAnimDict, ", sCigarClip: ", luxeActStruct.sCigarClip, ", sCigarCigarClip: ", luxeActStruct.sCigarCigarClip, ", sCigarLighterClip: ", luxeActStruct.sCigarLighterClip, ", on seat: ", luxeActStruct.iSeatID)
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), luxeActStruct.iNetSceneid, luxeActStruct.sCigarAnimDict, luxeActStruct.sCigarClip, 6.0, WALK_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT, INSTANT_BLEND_IN)
				IF luxeActStruct.iSeatID > -1
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter), luxeActStruct.iNetSceneid, luxeActStruct.sCigarAnimDict, luxeActStruct.sCigarLighterClip, 6.0, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START)
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim), luxeActStruct.iNetSceneid, luxeActStruct.sCigarAnimDict, luxeActStruct.sCigarCigarClip, 6.0, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START)
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CIGAR_SYNC: Invalid seatID")
				ENDIF
				NETWORK_ATTACH_SYNCHRONISED_SCENE_TO_ENTITY(luxeActStruct.iNetSceneid, luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, luxeActStruct.activitySeats[luxeActStruct.iSeatID].sSeat))
				NETWORK_START_SYNCHRONISED_SCENE(luxeActStruct.iNetSceneid)
				
	
				SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC)
			ENDIF
		BREAK
		
		CASE CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC
			INT localCigarScene 
			localCigarScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(luxeActStruct.iNetSceneid)
			
			
			
			IF localCigarScene  != -1
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC: GET_SYNCHRONIZED_SCENE_PHASE: ", GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene))
//				CDEBUG3LN(DEBUG_ACT_CELEB, "GET_SYNCHRONIZED_SCENE_PHASE: ", GET_SYNCHRONIZED_SCENE_PHASE(localScene))
				
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(localCigarScene ) >= 0.990
				OR IS_PLAYER_DEAD(PLAYER_ID())
				OR SHOULD_VEHICLE_SCRIPT_CLEANUP(luxeActStruct)
				OR IS_PED_BEING_JACKED(PLAYER_PED_ID())
					NETWORK_STOP_SYNCHRONISED_SCENE(luxeActStruct.iNetSceneid)
					
					IF luxeActStruct.iSeatID > -1
						SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propCigarAnim), FALSE)
						SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLighter), FALSE)
					ELSE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC: Invalid seatID")
					ENDIF
					
					IF IS_PLAYER_DEAD(PLAYER_ID())
					OR SHOULD_VEHICLE_SCRIPT_CLEANUP(luxeActStruct)
					OR IS_PED_BEING_JACKED(PLAYER_PED_ID())
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC: Scene finishing early because player is dead, jacked or script needs to cleanup")
					ENDIF
					
//					TASK_ENTER_VEHICLE(PLAYER_PED_ID(),luxeActStruct.vehicleIndex, 1, INT_TO_ENUM(VEHICLE_SEAT, serverBD.activitySeats[luxeActStruct.iSeatID].vehicleSeatID), DEFAULT, ECF_WARP_PED)
//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC: TASK_ENTER_VEHICLE = ", serverBD.activitySeats[luxeActStruct.iSeatID].vehicleSeatID)
//					TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(),luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, serverBD.activitySeats[luxeActStruct.iSeatID].vehicleSeatID))
//					IF HAS_NET_TIMER_STARTED(luxeActStruct.cigarBurnOutTimer)
//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC: resetting net timer ")
//					RESET_NET_TIMER(luxeActStruct.cigarBurnOutTimer)
////					ENDIF
//					START_NET_TIMER(luxeActStruct.cigarBurnOutTimer, TRUE)
//					luxeActStruct.cigarBurnOutTimer.Timer = INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())
//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC: time difference: ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), luxeActStruct.cigarBurnOutTimer.Timer))
					
					
					IF luxeActStruct.iClipVariation = 0
					OR luxeActStruct.iClipVariation = 1
						IF luxeActStruct.bNeedToLightCigar = TRUE
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC: Lighter variation was played when needed, resetting bNeedToLightCigar flag")
							luxeActStruct.bNeedToLightCigar = FALSE
						ELSE
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC: Lighter variation was played even though lighter wasn't needed?")
						ENDIF
					ENDIF
					SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC)
				ENDIF
			ELSE
				// Player has been cleaned up from the sync scene (usually by external system calling clear tasks)
				// We need to reset this scene
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CIGAR_SYNC: localCigarScene = ", localCigarScene, " resetting state of script")
				
				RELEASE_CIGAR_PROPS(serverBD, luxeActStruct)
				SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_PROMPT_ACT)
			ENDIF
		BREAK
		
		CASE CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC
	
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC: GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = TRUE")
			IF luxeActStruct.vehicleIndex = luxeActStruct.vehicleIndex
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC: luxeActStruct.vehicleIndex = TRUE")
			ELSE
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC: GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(, TRUE)) = FALSE")
			ENDIF
			
			RELEASE_CIGAR_PROPS(serverBD, luxeActStruct)
				
			VEHICLE_SEAT vehicleSeat
			playerBD[NATIVE_TO_INT(PLAYER_ID())].bIsClientFinishedCigar = TRUE
			vehicleSeat = GET_VEHICLE_SEAT(luxeActStruct)
			IF vehicleSeat = VS_EXTRA_RIGHT_2
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC: HACKING right2 seat to right3 seat")
				vehicleSeat = VS_EXTRA_RIGHT_3
			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC: NOT HACKING right2 ")
			ENDIF
			IF vehicleSeat = VS_EXTRA_LEFT_2// player is in seat extra left, which is actually extra right 
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC: HACKING left2 seat to right2 seat")
				vehicleSeat = VS_EXTRA_RIGHT_2
			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC: NOT HACKING left2 ")
			ENDIF
			
			
			IF NOT SHOULD_VEHICLE_SCRIPT_CLEANUP(luxeActStruct)
				IF luxeActStruct.iSeatID > -1
					IF NOT DO_NETWORK_CIGAR_EXIST(serverBD.activitySeats[luxeActStruct.iSeatID])
						
						IF vehicleSeat = INT_TO_ENUM(VEHICLE_SEAT, serverBD.activitySeats[luxeActStruct.iSeatID].vehicleSeatID)
							playerBD[NATIVE_TO_INT(PLAYER_ID())].bIsClientFinishedCigar = FALSE
							SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_PROMPT_ACT)
						ELSE
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC: GET_VEHICLE_SEAT = ", ENUM_TO_INT(GET_VEHICLE_SEAT(luxeActStruct)), " != seat player should be trying to get into: ", serverBD.activitySeats[luxeActStruct.iSeatID].vehicleSeatID)
						ENDIF
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC: Invalid seatID")
				ENDIF
			ELSE

				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CIGAR_SYNC: going to cleanup because this script has to cleanup")
				SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_CLEANING_UP)

			ENDIF

			
		BREAK
		
		CASE CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC
			IF GRAB_CHAMPAGNE_PROPS(serverBD, luxeActStruct.iSeatID)
				
//				CREATE_LOCAL_LIQUID_OBJECT(luxeActStruct)
				BOOL bNetworkVisibiltyOfNetworkProps
				bNetworkVisibiltyOfNetworkProps = TRUE
				#IF IS_DEBUG_BUILD
					bNetworkVisibiltyOfNetworkProps = serverBD.bDebugNetworkVisibiltyOfSwiftNetworkProps
				#ENDIF
				IF bNetworkVisibiltyOfNetworkProps  = TRUE
				AND( luxeActStruct.vehModel = SWIFT2
				OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel)
				)
					IF FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimDict, luxeActStruct.sChampClip, "PICK_UP_BOTTLE", luxeActStruct.fPickUpBottlePhase, luxeActStruct.fEndPhaseTime)
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC: FIND_ANIM_EVENT_PHASE, luxeActStruct.fPickUpBottlePhase: ", luxeActStruct.fPickUpBottlePhase)
					ELSE
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC: FIND_ANIM_EVENT_PHASE, could NOT find: luxeActStruct.fPickUpBottlePhase, bad things will happen to the visibility of the network props!")
					ENDIF
					IF FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimDict, luxeActStruct.sChampClip, "PUT_DOWN_BOTTLE", luxeActStruct.fPutDownBottlePhase, luxeActStruct.fEndPhaseTime)
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC: FIND_ANIM_EVENT_PHASE, luxeActStruct.fPutDownBottlePhase: ", luxeActStruct.fPutDownBottlePhase)
					ELSE
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC: FIND_ANIM_EVENT_PHASE, could NOT find: luxeActStruct.fPutDownBottlePhase, bad things will happen to the visibility of the network props!")
					ENDIF
					IF FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimDict, luxeActStruct.sChampClip, "PICK_UP_FLUTE", luxeActStruct.fPickUpFluteBackPhase, luxeActStruct.fEndPhaseTime)
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC: FIND_ANIM_EVENT_PHASE, luxeActStruct.fPickUpFluteBackPhase: ", luxeActStruct.fPickUpFluteBackPhase)
					ELSE
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC: FIND_ANIM_EVENT_PHASE, could NOT find: luxeActStruct.fPickUpFluteBackPhase, bad things will happen to the visibility of the network props!")
					ENDIF
					IF FIND_ANIM_EVENT_PHASE(luxeActStruct.sChampAnimDict, luxeActStruct.sChampClip, "PUT_DOWN_FLUTE", luxeActStruct.fPutDownFluteBackPhase, luxeActStruct.fEndPhaseTime)
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC: FIND_ANIM_EVENT_PHASE, luxeActStruct.fPutDownFluteBackPhase: ", luxeActStruct.fPutDownFluteBackPhase)
					ELSE
						CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC: FIND_ANIM_EVENT_PHASE, could NOT find: luxeActStruct.fPutDownFluteBackPhase, bad things will happen to the visibility of the network props!")
					ENDIF
				ENDIF
				
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP: CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC: seatID: ", luxeActStruct.iSeatID)
				


//				SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteAnim), FALSE)
//				SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampBotAnim), FALSE)
//				SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLiquid), FALSE)

				luxeActStruct.vehicleIndex = luxeActStruct.vehicleIndex
				luxeActStruct.iNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(<<0,0,0>>, <<0,0,0>>, DEFAULT, FALSE)
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC: luxeActStruct.sChampAnimDict: ", luxeActStruct.sChampAnimDict, ", sChampClip: ", luxeActStruct.sChampClip, ", sPourClip: ", luxeActStruct.sPourClip, ", sFluteClip: ", luxeActStruct.sFluteClip, ", sBottleClip: ", luxeActStruct.sBottleClip, ", luxeActStruct.iSeatID: ", luxeActStruct.iSeatID)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), luxeActStruct.iNetSceneid, luxeActStruct.sChampAnimDict, luxeActStruct.sChampClip, 6.0, WALK_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT, RBF_PLAYER_IMPACT)
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLiquid), luxeActStruct.iNetSceneid, luxeActStruct.sChampAnimDict, luxeActStruct.sPourClip, 6.0, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START | SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteAnim), luxeActStruct.iNetSceneid, luxeActStruct.sChampAnimDict, luxeActStruct.sFluteClip, 6.0, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START | SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampBotAnim), luxeActStruct.iNetSceneid, luxeActStruct.sChampAnimDict, luxeActStruct.sBottleClip, 6.0, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START | SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)

				NETWORK_ATTACH_SYNCHRONISED_SCENE_TO_ENTITY(luxeActStruct.iNetSceneid, luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, luxeActStruct.activitySeats[luxeActStruct.iSeatID].sSeat))
				NETWORK_START_SYNCHRONISED_SCENE(luxeActStruct.iNetSceneid)
				SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC)
				
			ELSE
				// Wait Xs before timing out and resetting ourselves (prevents: 2341532)
				IF HAS_NET_TIMER_EXPIRED(luxeActStruct.animSafetyTimeout, ciLUXE_ACT_ANIM_SAFETY_TIMEOUT)
					
					// Throw an assert as we still want to catch instances of this while we should always be able to grab the props
					SCRIPT_ASSERT("CL_LUXE_ACT_IN_VEH_PLAY_CHAMP: GRAB_CHAMPAGNE_PROPS(serverBD, luxeActStruct.iSeatID) = FALSE")
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_PLAY_CHAMP: GRAB_CHAMPAGNE_PROPS(serverBD, luxeActStruct.iSeatID) = FALSE. Hit timeout of: ", ciLUXE_ACT_ANIM_SAFETY_TIMEOUT)
					
					// if we time out, reset our state back to the prompt so they can try again.
					SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_PROMPT_ACT)
				ENDIF
			ENDIF
		BREAK
		
		CASE CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC
			INT localScene 
			localScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(luxeActStruct.iNetSceneid)
			
			IF localScene != -1
				CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC: GET_SYNCHRONIZED_SCENE_PHASE: ", GET_SYNCHRONIZED_SCENE_PHASE(localScene))
				CDEBUG2LN(DEBUG_ACT_CELEB, "GET_SYNCHRONIZED_SCENE_PHASE: ", GET_SYNCHRONIZED_SCENE_PHASE(localScene))



				BOOL bNetworkVisibiltyOfNetworkProps
				bNetworkVisibiltyOfNetworkProps = TRUE
				#IF IS_DEBUG_BUILD
					bNetworkVisibiltyOfNetworkProps = serverBD.bDebugNetworkVisibiltyOfSwiftNetworkProps
				#ENDIF
				IF bNetworkVisibiltyOfNetworkProps  = TRUE
				AND luxeActStruct.vehModel = SWIFT2
				OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel) 
					IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= (luxeActStruct.fPickUpBottlePhase + 0.04)
					AND NOT (GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= (luxeActStruct.fPutDownFluteBackPhase - 0.04))
						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC: Network bottle and liquid objects Networking Visibility to on, frame: ", (luxeActStruct.fPickUpBottlePhase + 0.04))
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampBotAnim), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLiquid), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteAnim), TRUE)
					ENDIF
					
												
					IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= (luxeActStruct.fPutDownFluteBackPhase - 0.04)
						CDEBUG3LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC: Network flute glass object Networking Visibility to off, frame: ", (luxeActStruct.fPutDownFluteBackPhase - 0.04))
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampBotAnim), FALSE)
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLiquid), FALSE)
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteAnim), FALSE)
					ENDIF					
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 0.990
				OR IS_PLAYER_DEAD(PLAYER_ID())
				OR SHOULD_VEHICLE_SCRIPT_CLEANUP(luxeActStruct)
				OR IS_PED_BEING_JACKED(PLAYER_PED_ID()) 
					#IF IS_DEBUG_BUILD
					IF SHOULD_VEHICLE_SCRIPT_CLEANUP(luxeActStruct)
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC: SHOULD_VEHICLE_SCRIPT_CLEANUP = TRUE, cleaningup sync scene early")
					ENDIF
					#ENDIF
					NETWORK_STOP_SYNCHRONISED_SCENE(luxeActStruct.iNetSceneid)
					
					SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteAnim), FALSE)
					SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampBotAnim), FALSE)
					SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propLiquid), FALSE)
//					DESTROY_LOCAL_LIQUID_OBJECT(luxeActStruct)
					#IF IS_DEBUG_BUILD
					IF NOT luxeActStruct.bDisableDrunkDebug
					#ENDIF
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC: PLAYER_TAKES_ALCOHOL_HIT")
					PLAYER_TAKES_ALCOHOL_HIT(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD
					ELSE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "DEBUG WIDGET: Ignoring drunk reg")
					ENDIF
					#ENDIF
					
					IF IS_PLAYER_DEAD(PLAYER_ID())
					OR SHOULD_VEHICLE_SCRIPT_CLEANUP(luxeActStruct)
					OR IS_PED_BEING_JACKED(PLAYER_PED_ID())
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC: Scene finishing early because player is dead, jacked or script needs to cleanup")
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteAnim))					
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteAnim), FALSE)
					ENDIF
					
					
					// TODO - NETWORK VISIBILITY
//					IF NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteIdle))
//					AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteAnim))
//						IF NOT IS_ENTITY_DEAD(luxeActStruct.vehicleIndex)
//							ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteIdle), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, serverBD.activitySeats[luxeActStruct.iSeatID].sSeat), serverBD.activitySeats[luxeActStruct.iSeatID].vFlutePropPos, serverBD.activitySeats[luxeActStruct.iSeatID].vFlutePropRot)
//							ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteAnim), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, serverBD.activitySeats[luxeActStruct.iSeatID].sSeat), serverBD.activitySeats[luxeActStruct.iSeatID].vFlutePropPos, serverBD.activitySeats[luxeActStruct.iSeatID].vFlutePropRot)
//						ENDIF
//						
//						IF NOT IS_ENTITY_DEAD(NET_TO_OBJ(luxeActStruct.propLiquid))
//							SET_ENTITY_VISIBLE(NET_TO_OBJ(luxeActStruct.propLiquid), FALSE)
//						ENDIF
//						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteAnim), FALSE)
//						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.activitySeats[luxeActStruct.iSeatID].propChampFluteIdle), TRUE)
//					ENDIF
					//				IS_ENTITY_ATTACHED_TO_ENTITY
//					VECTOR vTemp  
//					vTemp = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampBotAnim))
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC: vChampBotPropPos: ", serverBD.activitySeats[luxeActStruct.iSeatID].vChampBotPropPos, ", champ prop position: ", vTemp , ", seatID: ", luxeActStruct.iSeatID)
//					ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(serverBD.activitySeats[luxeActStruct.iSeatID].propChampBotAnim), luxeActStruct.vehicleIndex, GET_ENTITY_BONE_INDEX_BY_NAME(luxeActStruct.vehicleIndex, serverBD.activitySeats[luxeActStruct.iSeatID].sSeat), serverBD.activitySeats[luxeActStruct.iSeatID].vChampBotPropPos, serverBD.activitySeats[luxeActStruct.iSeatID].vChampBotPropRot)

					

					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC: activitySeats[luxeActStruct.iSeatID].vehicleSeatID = ", serverBD.activitySeats[luxeActStruct.iSeatID].vehicleSeatID)

					SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC)
				ENDIF
			ELSE
			
				IF RELEASE_CHAMPAGNE_PROPS(serverBD, luxeActStruct.iSeatID)
					// Player has been cleaned up from the sync scene (usually by external system calling clear tasks)
					// We need to reset this scene
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC: localScene = ", localScene, " resetting state of script")
					
					SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_PROMPT_ACT)
					
				#IF IS_DEBUG_BUILD
				ELSE
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC: RELEASE_CHAMPAGNE_PROPS = FALSE")
				#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC

			//luxeActStruct.bChampPropsNetworkVisible = FALSE
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC: GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = TRUE")
			IF luxeActStruct.vehicleIndex = luxeActStruct.vehicleIndex
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC: luxeActStruct.vehicleIndex = TRUE")
			ELSE
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC: GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(, TRUE)) = FALSE")
			ENDIF
			
			IF RELEASE_CHAMPAGNE_PROPS(serverBD, luxeActStruct.iSeatID)
//					AND luxeActStruct.vehModel = LUXOR2
				VEHICLE_SEAT vehicleSeat2
				vehicleSeat2 = GET_VEHICLE_SEAT(luxeActStruct)
				IF vehicleSeat2 = VS_EXTRA_RIGHT_2
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC: HACKING right2 seat to right3 seat")
					vehicleSeat2 = VS_EXTRA_RIGHT_3
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC: NOT HACKING right2 ")
				ENDIF
				IF vehicleSeat2 = VS_EXTRA_LEFT_2// player is in seat extra left, which is actually extra right 
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC: HACKING left2 seat to right2 seat")
					vehicleSeat2 = VS_EXTRA_RIGHT_2
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC: NOT HACKING left2 ")
				ENDIF
				
				IF NOT SHOULD_VEHICLE_SCRIPT_CLEANUP(luxeActStruct)
					IF luxeActStruct.iSeatID > -1
						IF vehicleSeat2 = INT_TO_ENUM(VEHICLE_SEAT, serverBD.activitySeats[luxeActStruct.iSeatID].vehicleSeatID)
							SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH_PROMPT_ACT)
						ELSE
							CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC: GET_VEHICLE_SEAT = ", ENUM_TO_INT(GET_VEHICLE_SEAT(luxeActStruct)), " != seat player should be trying to get into: ", serverBD.activitySeats[luxeActStruct.iSeatID].vehicleSeatID)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC: Invalid seatID")
					ENDIF
				ELSE
					
					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC: going to cleanup because this script has to cleanup")
					SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_CLEANING_UP)
				ENDIF
			ELSE
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC: RELEASE_CHAMPAGNE_PROPS = FALSE")
			ENDIF

		BREAK
		
		CASE CL_LUXE_ACT_IN_VEH_IN_FRONT
			IF IS_PLAYER_IN_COCKPIT(luxeActStruct)
				IF g_bInLuxeVehicle
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH: Player is in cockpit g_bInLuxeVehicle to TRUE.")
					g_bInLuxeVehicle = FALSE
				ENDIF	
				
				IF luxeActStruct.iSeatID != -1
					luxeActStruct.iSeatID = -1
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_IN_FRONT: player is in cockpit setting seat id = ", luxeActStruct.iSeatID)
				ENDIF
				
				IF playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID != -1
					playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID = -1
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_IN_VEH_IN_FRONT: player is in cockpit setting playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID = ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID)
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF serverBD.iSeatWidget != -1
				AND NOT IS_ENTITY_DEAD(luxeActStruct.vehicleIndex)
					IF IS_VEHICLE_SEAT_FREE(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, serverBD.iSeatWidget))
					AND PLAYER_PED_ID() != GET_PED_IN_VEHICLE_SEAT(luxeActStruct.vehicleIndex, INT_TO_ENUM(VEHICLE_SEAT, serverBD.iSeatWidget))
						TASK_ENTER_VEHICLE(PLAYER_PED_ID(), luxeActStruct.vehicleIndex, DEFAULT, INT_TO_ENUM(VEHICLE_SEAT, serverBD.iSeatWidget), DEFAULT, ECF_WARP_PED)
					ELSE
						SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH)
	//					CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "WIDGET: Seat is already occupied or player is already in seat")
					ENDIF
				ENDIF
				#ENDIF
			ELSE
				SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_IN_VEH)
			ENDIF
		BREAK
		
		CASE CL_LUXE_ACT_CHANGING_SEAT
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
				luxeActStruct.bDebugIgnoreInVeh = FALSE
				SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_INIT)
			ENDIF
		BREAK
		
		CASE CL_LUXE_ACT_CLEANING_UP
			CLEANUP_VFX()
			CLEANUP_AUDIO(luxeActStruct)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_PED_IN_PARACHUTE_FREE_FALL(PLAYER_PED_ID())
				SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE, FALSE)
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CLEANING_UP: Hiding ped weapon because ped isn't freefalling")
			ELSE
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CLEANING_UP: Not hiding ped weapon because ped is freefalling")
			ENDIF
			TURN_OFF_PASSIVE_MODE_OPTION(FALSE)
			luxeActStruct.bWaitForBDSync = FALSE
			IF GET_PEDS_DRUNK_ALCOHOL_HIT_COUNT(PLAYER_PED_ID()) > 2
				CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CLEANING_UP: Ped should sober up withen 30 seconds.")
//				Quit_Drunk_Camera_Immediately()
				QUIT_DRUNK_CAMERA_GRADUALLY()
				Make_Ped_Sober(PLAYER_PED_ID())
			ENDIF 
			IF g_bInLuxeVehicle
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CLEANING_UP: Setting g_bInLuxeVehicle to FALSE.")
				g_bInLuxeVehicle = FALSE
			ENDIF
			
			INT i
			REPEAT iNumberOfSeatsToClean i
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_CLEANING_UP: DESTROY_LOCAL_DUMMY_PROPS i = ", i)
				DESTROY_LOCAL_DUMMY_PROPS(luxeActStruct.activitySeats[i])
			ENDREPEAT
			
			

			SetLuxeActStage(luxeActStruct, CL_LUXE_ACT_CLEANED_UP)

		BREAK
		
		
		
		CASE CL_LUXE_ACT_CLEANED_UP
		BREAK
		
		
 	ENDSWITCH
	
ENDPROC
/// PURPOSE: Do necessary pre game start ini
PROC PROCESS_PRE_GAME_LUXE(SERVER_BROADCAST_DATA &serverBD, PLAYER_BROADCAST_DATA &playerBD[NUM_NETWORK_PLAYERS], LUXE_ACT_STRUCT &luxeActStruct) //
	
	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "PROCESS_PRE_GAME_LUXE: script instance: ", luxeActStruct.iLuxeVehInstanceID)
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, luxeActStruct.iLuxeVehInstanceID)

	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	Wait_For_First_Network_Broadcast()
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// Allow peds to be left behind again now we are leaving the activity
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "PROCESS_PRE_GAME_LUXE: mark player to NOT leave a ped behind: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE) (+ network)")
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
		NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
	ENDIF
	
	//TODO, is the decorator already set
	
	IF DOES_ENTITY_EXIST(luxeActStruct.vehicleIndex)
		DECOR_SET_INT(luxeActStruct.vehicleIndex , "LUXE_VEH_INSTANCE_ID",luxeActStruct.iLuxeVehInstanceID)
		CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "CL_LUXE_ACT_INIT: setting LUXE_VEH_INSTANCE_ID decor on vehicle to: ", luxeActStruct.iLuxeVehInstanceID)
	ENDIF

	luxeActStruct.iLocalHelpCounter = GET_PACKED_STAT_INT(PACKED_MP_ACTIVITY_HELP_COUNTER)
	
	IF NOT IS_BIT_SET(g_iLuxeBootVerifyBS, luxeActStruct.iLuxeVehInstanceID)
		SET_BIT(g_iLuxeBootVerifyBS, luxeActStruct.iLuxeVehInstanceID)
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "PROCESS_PRE_GAME_LUXE: Setting boot verify bit: ", luxeActStruct.iLuxeVehInstanceID)
	ELSE
		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "PROCESS_PRE_GAME_LUXE: ERROR! Boot verify bit: ", luxeActStruct.iLuxeVehInstanceID, " was already set. Was there a problem cleaning up? Bitset: ", g_iLuxeBootVerifyBS)
		SCRIPT_ASSERT("PROCESS_PRE_GAME_LUXE: ERROR! Boot verify bit was already set. Was there a problem cleaning up? Enter a bug for Philip O.D.")
	ENDIF

	PRINTNL()
	
ENDPROC


// Mission Script -----------------------------------------//




PROC HANDLE_SERVER_OBJ_LUXE_VEH_FUNCTIONALITY(SERVER_BROADCAST_DATA &serverBD, PLAYER_BROADCAST_DATA &playerBD[], LUXE_ACT_STRUCT &luxeActStruct, LUXE_ACT_SERVER_STRUCT &luxeActServerStruct) //, LUXE_ACT_STRUCT &luxeActStruct)
	
	SWITCH serverBD.luxeServerActScriptState
		CASE SR_LUXE_INIT

//				INT jSeat
//				jSeat = 0
//				WHILE jSeat < 8
//					IF jSeat != 4
//					AND jSeat != 6
//						serverBD.activitySeats[jSeat].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, jSeat+2))
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_INIT: sSeat: ", jSeat, " = ", serverBD.activitySeats[jSeat].sSeat)
//					ENDIF
//				ENDWHILE
				#IF IS_DEBUG_BUILD
				serverBD.iDebugClipVariation = -1
				#ENDIF
				
				IF IS_PLAYER_IN_LUX_JET_VEHICLE(GET_ENTITY_MODEL(luxeActStruct.vehicleIndex))
					serverBD.activitySeats[0].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 1), luxeActStruct)
					serverBD.activitySeats[0].vehicleSeatID = 1
					
					serverBD.activitySeats[1].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 2), luxeActStruct)
					serverBD.activitySeats[1].vehicleSeatID = 2
					
					serverBD.activitySeats[2].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 3), luxeActStruct)
					serverBD.activitySeats[2].vehicleSeatID = 3
					
					serverBD.activitySeats[3].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 4), luxeActStruct)
					serverBD.activitySeats[3].vehicleSeatID = 4
					
					serverBD.activitySeats[4].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 6), luxeActStruct)
					serverBD.activitySeats[4].vehicleSeatID = 6
					
					serverBD.activitySeats[5].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 8), luxeActStruct)
					serverBD.activitySeats[5].vehicleSeatID = 8
					
				ELSE
					serverBD.activitySeats[0].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 1), luxeActStruct)
					serverBD.activitySeats[0].vehicleSeatID = 1
					
					serverBD.activitySeats[1].sSeat = GET_SEAT_NAME_FROM_ENUM(INT_TO_ENUM(VEHICLE_SEAT, 2), luxeActStruct) 
					serverBD.activitySeats[1].vehicleSeatID = 2
				ENDIF
				
				INT iTableCount
				iTableCount = 0
				REPEAT 3 iTableCount
					serverBD.tablesData[iTableCount].iTable = iTableCount
					serverBD.tablesData[iTableCount].champIDPlayerChosen = INT_TO_NATIVE(PLAYER_INDEX, -1)
				ENDREPEAT
				
//				jSeat = 0
//				
//				WHILE jSeat < GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct)
//					
//					INIT_SEAT_DATA(luxeActStruct, serverBD.activitySeats[jSeat])
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_INIT: INIT_SEAT_DATA: ", jSeat, " ", serverBD.activitySeats[jSeat].sSeat)
//					jSeat++	
//				ENDWHILE
//				
//				IF jSeat = GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct)
					
					

						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "Vehicle ", NATIVE_TO_INT(serverBD.niVehIndex), ", ", GET_ENTITY_MODEL(luxeActStruct.vehicleIndex), " needs props")
						SetServerLuxeActStage(serverBD, SR_LUXE_LOAD_PROPS)
//					
//				ELSE
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_INIT: jSeat != 6, : ", jSeat)
//				ENDIF
//			ENDIF
		BREAK
		
		CASE SR_LUXE_LOAD_PROPS
//			IF LOAD_NEW_LUXE_ACTIVITY_PROPS_LOOP(luxeActStruct)
				SetServerLuxeActStage(serverBD, SR_LUXE_CREATE_PROPS)
//			ENDIF
		BREAK
		
		CASE SR_LUXE_CREATE_PROPS
		

//			IF luxeActStruct.iCreationSeatNum < GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct) // should be 6
//				RESERVE_PROPS_FOR_CREATION(luxeActStruct, serverBD.activitySeats[luxeActStruct.iCreationSeatNum])
//				IF luxeActStruct.bObjectsReservedForSeat = TRUE
//					IF CREATE_NEW_LUXE_ACTIVITY_PROPS_LOOP(luxeActStruct, serverBD.activitySeats[luxeActStruct.iCreationSeatNum])
//						IF IS_BACK_FACING_SEAT(luxeActStruct, serverBD.activitySeats[luxeActStruct.iCreationSeatNum])
//							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: Props created for back facing seat: ", luxeActStruct.iCreationSeatNum, " name: ", serverBD.activitySeats[luxeActStruct.iCreationSeatNum].sSeat, " ID: ", luxeActStruct.iCreationSeatNum)	
//						ELSE
//							CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: Props created for front facing seat: ", luxeActStruct.iCreationSeatNum, " name: ", serverBD.activitySeats[luxeActStruct.iCreationSeatNum].sSeat, " ID: ", luxeActStruct.iCreationSeatNum)	
//						ENDIF
//						luxeActStruct.bObjectsReservedForSeat = FALSE
//						luxeActStruct.iLoadPropFail = 0
//						luxeActStruct.iCreationSeatNum++	
//					ELSE
//						luxeActStruct.iLoadPropFail++
//						CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: Failed to create props for seat : ", luxeActStruct.iCreationSeatNum,", attempt: ", luxeActStruct.iLoadPropFail)
//					ENDIF
//				ELSE
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: RESERVE_PROPS_FOR_CREATION couldn't reserve enough objects")
//					//iLoadPropFail = 15
//				ENDIF
//			ENDIF
//			
//			IF luxeActStruct.iLoadPropFail = 100
//			AND luxeActStruct.bObjectsReservedForSeat = TRUE
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: Failed to create props for seat after 100 attemps, CLEANUPING UP SCRIPT")
//				LUXE_SCRIPT_CLEANUP(serverBD, luxeActStruct, playerBD)
//			ENDIF
			
			
			IF luxeActStruct.vehModel = SWIFT2
			OR IS_PLAYER_IN_LUX_HELI_VEHICLE(luxeActStruct.vehModel) 
				IF IS_VEHICLE_DOOR_FULLY_OPEN(luxeActStruct.vehicleIndex, SC_DOOR_REAR_LEFT)
				OR IS_VEHICLE_DOOR_FULLY_OPEN(luxeActStruct.vehicleIndex, SC_DOOR_REAR_RIGHT)
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: doors are open flagging props to fade in.")
					luxeActStruct.bPropsNeedToAlpha = TRUE
				ELSE
					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: None of the doors are fully open.")
				ENDIF
			ENDIF
			
//			IF LUXOR2 = GET_ENTITY_MODEL(luxeActStruct.vehicleIndex)
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: serverBD.activitySeats[1].propChampBotAnim = serverBD.activitySeats[2].propChampBotAnim ")	
//				serverBD.activitySeats[2].propChampBotAnim = serverBD.activitySeats[0].propChampBotAnim 
//				serverBD.activitySeats[2].propChampBotIdle = serverBD.activitySeats[0].propChampBotIdle 
//				
//				serverBD.activitySeats[1].propChampBotAnim = serverBD.activitySeats[3].propChampBotAnim
//				serverBD.activitySeats[1].propChampBotIdle = serverBD.activitySeats[3].propChampBotIdle
//				
//				serverBD.activitySeats[4].propChampBotAnim = serverBD.activitySeats[5].propChampBotAnim
//				serverBD.activitySeats[4].propChampBotIdle = serverBD.activitySeats[5].propChampBotIdle
//
//			ELSE
//				serverBD.activitySeats[0].propChampBotAnim = serverBD.activitySeats[1].propChampBotAnim
//				serverBD.activitySeats[0].propChampBotIdle = serverBD.activitySeats[1].propChampBotIdle
//			ENDIF
			

			
//			IF luxeActStruct.iCreationSeatNum = GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct)
				SetServerLuxeActStage(serverBD, SR_LUXE_RUNNING)
//			ELSE
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: iSeat != 6, :", luxeActStruct.iCreationSeatNum)
//			ENDIF

		BREAK
		
		CASE SR_LUXE_RUNNING

//			FADE_IN_PROPS_FOR_SWIFT(serverBD, luxeActStruct)
			SERVER_CHECK_CHAMP_ACT(serverBD, playerBD, luxeActServerStruct, luxeActStruct)
			SERVER_MANAGE_NETWORK_CIGAR_PROPS(serverBD, playerBD, luxeActServerStruct, luxeActStruct)
		BREAK
		
		CASE SR_LUXE_CLEANING_UP
		
//			INT kSeat
//			INT kCleanupPropFail
//			kCleanupPropFail = 0
//			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CLEANING_UP: resetting fail attempts ")	
//			kSeat = 0
//			
//			WHILE kSeat < GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct) // should be 6
//			AND kCleanupPropFail < 15
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CLEANING_UP: trying to clean up seat")
//				IF CLEANUP_NEW_LUXE_ACTIVITY_PROPS_LOOP(luxeActStruct, serverBD.activitySeats[kSeat])
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: Props cleaned up for seat: ", kSeat)	
//					kCleanupPropFail = 0
//					kSeat++	
//				ELSE
//					
//					kCleanupPropFail++
//					CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: failed to cleanup attempt: ", kCleanupPropFail)	
//				ENDIF
//			ENDWHILE
			
//			CLEAR_LUXE_VEH_INSTANCE_ID(luxeActStruct.vehicleIndex)
			
//			IF kSeat = GET_NUMBER_OF_PASSENGER_SEATS(luxeActStruct)
				SetServerLuxeActStage(serverBD, SR_LUXE_CLEANED_UP)
//			ELSE
//				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SR_LUXE_CREATE_PROPS: kSeat != 6, :", kSeat, ", kCleanupPropFail: ", kCleanupPropFail)
//			ENDIF

		BREAK
		CASE SR_LUXE_CLEANED_UP
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_PLAYER_IN_SYNC_ACTIVITY(LUXE_ACT_STRUCT &luxeActStruct)
	SWITCH luxeActStruct.luxeActivityState
		CASE CL_LUXE_ACT_IN_VEH_RUN_CHAMP_SYNC
		CASE CL_LUXE_ACT_IN_VEH_PLAY_CHAMP_SYNC
		CASE CL_LUXE_ACT_IN_VEH_FINISH_CHAMP_SYNC
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_IN_SYNC_ACTIVITY: ", LuxeActString(luxeActStruct.luxeActivityState), ", therefore = TRUE")
			RETURN TRUE
		BREAK
	ENDSWITCH
//	CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "IS_PLAYER_IN_SYNC_ACTIVITY: ", LuxeActString(luxeActStruct.luxeActivityState), ", therefore = FALSE")
	RETURN FALSE
ENDFUNC



FUNC BOOL HAS_CLEANUP_TIMER_EXPIRED(LUXE_ACT_STRUCT &luxeActStruct)
	IF HAS_NET_TIMER_STARTED(luxeActStruct.scriptCleanupTimer)
//	AND luxeActStruct.bScriptCleanupTimerStarted = TRUE
//		IF HAS_NET_TIMER_EXPIRED(luxeActStruct.scriptCleanupTimer, g_sMPTunables.iactivity_celebratiion_transition_timer, TRUE) // celeb act tuneables
		IF HAS_NET_TIMER_EXPIRED(luxeActStruct.scriptCleanupTimer, 10000, TRUE) // celeb act tuneables
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HAS_NET_TIMER_EXPIRED = TRUE")
			RESET_NET_TIMER(luxeActStruct.scriptCleanupTimer)
			RETURN TRUE
//			luxeActStruct.bScriptCleanupTimerStarted = FALSE
		ELSE
			CDEBUG2LN(DEBUG_MP_MINIGAME_ACT, "HAS_NET_TIMER_EXPIRED = FALSE")
			
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_TIMER_IF_NOT_ALREADY_STARTED(LUXE_ACT_STRUCT &luxeActStruct)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	AND NETWORK_GET_NUM_PARTICIPANTS() < 2 // If we are the last participant of the script	
		IF NOT HAS_NET_TIMER_STARTED(luxeActStruct.scriptCleanupTimer)
//		AND NOT luxeActStruct.bScriptCleanupTimerStarted = TRUE
	//	#IF IS_DEBUG_BUILD
	//		IF iDebug_activity_celebratiion_transition_timer != -1
	//			g_sMPTunables.iactivity_celebratiion_transition_timer = iDebug_activity_celebratiion_transition_timer*1000
	//			CWARNINGLN(DEBUG_ACT_CELEB, "Overriding iactivity_celebratiion_transition_timer to ", iDebug_activity_celebratiion_transition_timer*1000)
	//		ENDIF
	//	#ENDIF
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SET_TIMER_IF_NOT_ALREADY_STARTED: setting timer")
			luxeActStruct.bScriptCleanupTimerStarted = TRUE
			START_NET_TIMER(luxeActStruct.scriptCleanupTimer, TRUE)
		ENDIF
	ENDIF
ENDPROC






PROC HANDLE_OBJ_LUXE_VEH_FUNCTIONALITY(SERVER_BROADCAST_DATA &serverBD, PLAYER_BROADCAST_DATA &playerBD[], LUXE_ACT_STRUCT &luxeActStruct, LUXE_ACT_SERVER_STRUCT &luxeActServerStruct)

	
	
		#IF IS_DEBUG_BUILD
		IF luxeActStruct.bDisableDrunkDebug
		AND GET_PEDS_DRUNK_ALCOHOL_HIT_COUNT(PLAYER_PED_ID()) > 0
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "DEBUG WIDGET: RESET_ALL_DRUNK_VARIABLES_AND_TERMINATE_ALL_UNNEEDED_SCRIPTS")
			RESET_ALL_DRUNK_VARIABLES_AND_TERMINATE_ALL_UNNEEDED_SCRIPTS() 
		ENDIF 
		#ENDIF
		
//		CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_OBJ_LUXE_VEH_FUNCTIONALITY: NETWORK_GET_NUM_PARTICIPANTS: ", NETWORK_GET_NUM_PARTICIPANTS())
		IF SHOULD_VEHICLE_SCRIPT_CLEANUP(luxeActStruct)
			LUXE_SCRIPT_CLEANUP(serverBD, luxeActStruct, playerBD)
			//If the script should die becuse MP is going then we must kill it
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - Calling TERMINATE_NET_LUX_SCRIPT")
				#ENDIF
				TERMINATE_NET_LUX_SCRIPT()
			ENDIF
			PRINTNL()
		ENDIF
		
		IF serverBD.luxeServerActScriptState > SR_LUXE_CREATE_PROPS
			IF DOES_ENTITY_EXIST(luxeActStruct.vehicleIndex)
			OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_CLEANING_UP
			OR luxeActStruct.luxeActivityState = CL_LUXE_ACT_CLEANED_UP
				MAINTAIN_LUXE_VEH_ACTIVITIES(serverBD, playerBD, luxeActStruct)
			ELSE
				CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_OBJ_LUXE_VEH_FUNCTIONALITY: DOES_ENTITY_EXIST = FALSE")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_OBJ_LUXE_VEH_FUNCTIONALITY: luxeServerActScriptState <= SR_LUXE_CREATE_PROPS")
			CDEBUG1LN(DEBUG_MP_MINIGAME_ACT, "HANDLE_OBJ_LUXE_VEH_FUNCTIONALITY: luxeServerActScriptState = ", LuxeServerActString(serverBD.luxeServerActScriptState))
		ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			HANDLE_SERVER_OBJ_LUXE_VEH_FUNCTIONALITY(serverBD, playerBD, luxeActStruct, luxeActServerStruct) //, luxeActState)
		ENDIF


ENDPROC
