USING "rage_builtins.sch"
USING "globals.sch"
USING "FMMC_Vars.sch"
USING "net_team_info.sch"
USING "commands_water.sch"
USING "net_race_common_functions.sch"
USING "net_common_functions.sch"

/// PURPOSE:
///    Maintains the players position behind the camera in the sky.
/// PARAMS:
///    sCamDataPassed - The camera to use for players position.
PROC MAINTAIN_PLAYER_COORDS(FMMC_LOCAL_STRUCT &sFMMCdataPassed, CAMERA_INDEX sCamDataPassed)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		VECTOR vCamRot = GET_CAM_ROT(sCamDataPassed)
		
		IF IS_PLAYER_SCTV(PLAYER_ID())
			sFMMCdataPassed.vPlayerPos = GET_CAM_COORD(sCamDataPassed)
			//sFMMCdataPassed.vPlayerPos.z = -100.0
		ELSE
			sFMMCdataPassed.vPlayerPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_CAM_COORD(sCamDataPassed), vCamRot.z, <<0.0, 0.0, 0.0>>) //<<0.0, 0.0, 2.0>>)
		ENDIF
		
		SET_ENTITY_COORDS(PLAYER_PED_ID(), sFMMCdataPassed.vPlayerPos, DEFAULT, DEFAULT, DEFAULT, FALSE)	
		SET_ENTITY_ROTATION(PLAYER_PED_ID(), vCamRot)
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_PED_MAP_WARPED_WITH_FRONT_END(FMMC_LOCAL_STRUCT &sFMMCdataPassed, VECTOR &vPlayerPos)

	IF IS_PUBLIC_ARENA_CREATOR()
	AND NOT IS_SCREEN_FADED_OUT()
		PRINTLN("[ARENA] HAS_PLAYER_PED_MAP_WARPED_WITH_FRONT_END - Returning FALSE due to IS_PUBLIC_ARENA_CREATOR")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		IF NOT IS_VECTOR_ZERO(sFMMCdataPassed.vPlayerPos)
			IF GET_DISTANCE_BETWEEN_COORDS(sFMMCdataPassed.vPlayerPos, vPlayerPos) > 50
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_CAM_HEADING()
	FLOAT fHead
	VECTOR vRot = GET_FINAL_RENDERED_CAM_ROT(EULER_XYZ)
	fHead = ATAN(vRot.y/vRot.x)
	//PRINTLN("fHead = [", fHead, "]")
	RETURN fHead
ENDFUNC

FUNC BOOL IS_UNDERWATER_PLACEMENT_ALLOWED(structFMMC_MENU_ITEMS& sFMMCmenu)
	IF sFMMCmenu.iEntityCreation = CREATION_TYPE_CHECKPOINT
	AND NOT (IS_TRANSFORM_RACE() OR IS_SPECIAL_VEHICLE_RACE())
		PRINTLN("IS_UNDERWATER_PLACEMENT_ALLOWED - False due to placing a checkpoint we can't change the height of")
		RETURN FALSE
	ENDIF
	
	RETURN g_sMPTunables.bENABLE_AC_UNDERWATER_CHECKPOINTS
ENDFUNC

FUNC BOOL IS_CORONA_UNDERWATER()
	RETURN IS_BIT_SET(g_iUnderwaterEditModeBS, ciUnderwaterEditMode_Active)
ENDFUNC

PROC GET_PROP_CORNER_POINTS(OBJECT_INDEX oiPassed, VECTOR &vPoints[])
	VECTOR vMin, vmax, vDim
	IF COUNT_OF(vPoints) < 8
		SCRIPT_ASSERT("Vector array argument passed to GET_PROP_CORNER_POINTS requires a size of at least 8")
		EXIT
	ENDIF
	IF DOES_ENTITY_EXIST(oiPassed)
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(oiPassed), vMin, vMax)
		vDim = vMax - vMin
		vDim.x/=2.0
		vDim.y/=2.0
		vPoints[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiPassed, <<-vDim.x, -vDim.y, -vDim.z>>) //bottom left rear
		vPoints[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiPassed, <<-vDim.x,  vDim.y, -vDim.z>>) //bottom left front
		vPoints[2] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiPassed, << vDim.x,  vDim.y, -vDim.z>>) //bottom right front
		vPoints[3] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiPassed, << vDim.x, -vDim.y, -vDim.z>>) //bottom right rear
		vPoints[4] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiPassed, <<-vDim.x, -vDim.y, vDim.z>>) //top left rear
		vPoints[5] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiPassed, <<-vDim.x,  vDim.y, vDim.z>>) //top left front
		vPoints[6] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiPassed, << vDim.x,  vDim.y, vDim.z>>) //top right front
		vPoints[7] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiPassed, << vDim.x, -vDim.y, vDim.z>>) //top right rear
	ENDIF
ENDPROC

FUNC FLOAT GET_LOWEST_POINT_OF_PROP(OBJECT_INDEX oiPassed)
	IF DOES_ENTITY_EXIST(oiPassed)
		VECTOR vPoints[8]
		GET_PROP_CORNER_POINTS(oiPassed, vPoints)
		INT i, iLowest
		FLOAT fLowestVal = 99999
		FOR i = 0 TO COUNT_OF(vPoints)-1
			IF vPoints[i].z < fLowestVal
				fLowestVal = vPoints[i].z
				iLowest = i
			ENDIF
		ENDFOR
		RETURN vPoints[iLowest].z
	ENDIF
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_HIGHEST_POINT_OF_PROP(OBJECT_INDEX oiPassed)
	IF DOES_ENTITY_EXIST(oiPassed)
		VECTOR vPoints[8]
		GET_PROP_CORNER_POINTS(oiPassed, vPoints)
		INT i, iHighest
		FLOAT fHighestVal = -99999
		FOR i = 0 TO COUNT_OF(vPoints)-1
			IF vPoints[i].z > fHighestVal
				fHighestVal = vPoints[i].z
				iHighest = i
			ENDIF
		ENDFOR
		RETURN vPoints[iHighest].z
	ENDIF
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_HEIGHT_OF_HIGHEST_PROP_NEAR_VECTOR(VECTOR vInput, FLOAT &fNearestPropSize)
	FLOAT fHighestHeight = -99999
	INT i
	VECTOR vMin, vMax
	FLOAT fRange
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
		GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn, vMin, vMax)
		fRange = VDIST(vMax, vMin)
		fRange += 10.0
		IF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos, vInput, FALSE) < fRange + 5.0
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos.z) >= fHighestHeight
				fHighestHeight = g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos.z
				fNearestPropSize = fRange
			ENDIF
		ENDIF
	ENDFOR
	RETURN fHighestHeight
ENDFUNC

//this is to check if a prop is in the 2d radius of the camera when placed
FUNC BOOL IS_PROP_WITHIN_RANGE_2D(VECTOR vPoint, OBJECT_INDEX oiPassed, FLOAT fThreshold)

	INT i
	VECTOR vPoints[8]
	//get the points of the shape as world coords
	GET_PROP_CORNER_POINTS(oiPassed, vPoints)
	
	//set each of the points to the height of the test point
	REPEAT COUNT_OF(vPoints) i
		vPoints[i].z = vPoint.z
	ENDREPEAT
	
	//right to left
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[0], vPoints[3]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 1") RETURN TRUE ENDIF
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[1], vPoints[2]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 2") RETURN TRUE ENDIF
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[4], vPoints[7]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 3") RETURN TRUE ENDIF
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[5], vPoints[6]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 4") RETURN TRUE ENDIF
	//back to front
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[4], vPoints[5]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 5") RETURN TRUE ENDIF
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[7], vPoints[6]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 6") RETURN TRUE ENDIF
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[0], vPoints[1]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 7") RETURN TRUE ENDIF
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[3], vPoints[2]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 8") RETURN TRUE ENDIF
	//bottom to otp
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[0], vPoints[4]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 9") RETURN TRUE ENDIF
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[3], vPoints[7]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 10") RETURN TRUE ENDIF
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[1], vPoints[5]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 11") RETURN TRUE ENDIF
	IF VDIST2(GET_CLOSEST_POINT_ON_LINE(vPoint, vPoints[2], vPoints[6]), vPoint) < fThreshold*fThreshold PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 12") RETURN TRUE ENDIF
	
	IF IS_POINT_IN_ANGLED_AREA(vPoint, vPoints[4] + ((vPoints[5] - vPoints[4]) / 2.0), vPoints[6] + ((vPoints[7] - vPoints[6]) / 2.0), GET_DISTANCE_BETWEEN_COORDS(vPoints[4], vPoints[5]), DEFAULT, FALSE) PRINTLN("[IS_PROP_WITHIN_RANGE_2D] 13") RETURN TRUE ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SET_CAMERA_ABOVE_PLACED_PROP_TEMPLATE(FMMC_CAM_DATA &sCamDataPassed, OBJECT_INDEX &objArray[], BOOL bCheckOverPlayer = FALSE)
	FLOAT fHighestPoint, fLowestPoint
	VECTOR vCamCoord = GET_CAM_COORD(sCamDataPassed.cam)
	IF bCheckOverPlayer
		vCamCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	INT i
	FOR i = 0 TO COUNT_OF(objArray)-1
		IF DOES_ENTITY_EXIST(objArray[i])
			IF IS_PROP_WITHIN_RANGE_2D(vCamCoord, objArray[i], 3.0)
				fHighestPoint = GET_HIGHEST_POINT_OF_PROP(objArray[i])
				fLowestPoint = GET_LOWEST_POINT_OF_PROP(objArray[i])
				IF fHighestPoint > vCamCoord.z - 3.0
				AND fLowestPoint < vCamCoord.z + 3.0
					vCamCoord.z = fHighestPoint + 3.0
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	IF (NOT bCheckOverPlayer AND NOT ARE_VECTORS_EQUAL(vCamCoord, GET_CAM_COORD(sCamDataPassed.cam)))
	OR (bCheckOverPlayer AND NOT ARE_VECTORS_EQUAL(vCamCoord, GET_ENTITY_COORDS(PLAYER_PED_ID())))
		PRINTLN("[SET_CAMERA_ABOVE_PLACED_PROP_TEMPLATE] Setting Camera to <<", vCamCoord.x, ", ", vCamCoord.y, ", ", fHighestPoint + 3.0, ">>")
		sCamDataPassed.camPos = <<vCamCoord.x, vCamCoord.y, fHighestPoint + 3.0>>
		SET_CAM_COORD(sCamDataPassed.cam, sCamDataPassed.camPos)
	ENDIF
ENDPROC

PROC SET_CAMERA_ABOVE_PLACED_PROP(FMMC_CAM_DATA &sCamDataPassed, OBJECT_INDEX objPlaced)
	FLOAT fHighestPoint, fLowestPoint
	VECTOR vCamCoord = GET_CAM_COORD(sCamDataPassed.cam)
	IF DOES_ENTITY_EXIST(objPlaced)
		IF IS_PROP_WITHIN_RANGE_2D(vCamCoord, objPlaced, 3.0)
			fHighestPoint = GET_HIGHEST_POINT_OF_PROP(objPlaced)
			fLowestPoint = GET_LOWEST_POINT_OF_PROP(objPlaced)
			IF fHighestPoint >= vCamCoord.z - 3.0
			AND fLowestPoint < vCamCoord.z + 3.0
				sCamDataPassed.camPos = <<vCamCoord.x, vCamCoord.y, fHighestPoint+3.0>>
				SET_CAM_COORD(sCamDataPassed.cam, sCamDataPassed.camPos)
				SET_FLY_CAM_COORD_AND_CONSTRAIN (sCamDataPassed.cam, sCamDataPassed.camPos)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_CAMERA_IN_A_PROP(FMMC_CAM_DATA &sCamDataPassed)
	
	IF NOT DOES_CAM_EXIST(sCamDataPassed.cam)
		RETURN FALSE
	ENDIF
	
	OBJECT_INDEX oiProp
	VECTOR vMin, vMax
	VECTOR vCamPos = GET_CAM_COORD(sCamDataPassed.cam)
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
		GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn, vMin, vMax)
		IF VDIST(vCamPos, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos) < VMAG(vMax-vMin)/2
			oiProp = CREATE_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos, FALSE)
			SET_ENTITY_ROTATION(oiProp, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vRot)
			IF IS_PROP_WITHIN_RANGE_2D(vCamPos, oiProp, 2)
				DELETE_OBJECT(oiProp)
				RETURN TRUE
			ENDIF
			DELETE_OBJECT(oiProp)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps-1
		GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn, vMin, vMax)
		
		IF VDIST(vCamPos, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].vPos) < VMAG(vMax-vMin)
			oiProp = CREATE_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].vPos, FALSE)
			SET_ENTITY_ROTATION(oiProp, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].vRot)
			IF IS_PROP_WITHIN_RANGE_2D(vCamPos, oiProp, 3.5)
				DELETE_OBJECT(oiProp)
				RETURN TRUE
			ELSE
			ENDIF
			
			DELETE_OBJECT(oiProp)
		ENDIF
	ENDFOR

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_IN_AXIS_ALIGNED_AREA(VECTOR vPoint, VECTOR vMin, VECTOR vMax, BOOL checkZ = TRUE)
//	PRINTLN("IS_CAMERA_IN_AXIS_ALIGNED_AREA - checking ", vPoint, " is between ", vMin, " and ", vMax)
	IF vPoint.x > vMax.x
		RETURN FALSE
	ELIF vPoint.x < vMin.x
		RETURN FALSE
	ELIF vPoint.y > vMax.y
		RETURN FALSE
	ELIF vPoint.y < vMin.y
		RETURN FALSE
	ENDIF
	IF checkZ
		IF vPoint.z > vMax.z
			RETURN FALSE
		ELIF vPoint.z < vMin.z
			RETURN FALSE
		ENDIF
	ENDIF
	PRINTLN("IS_CAMERA_IN_AXIS_ALIGNED_AREA - TRUE ")
	RETURN TRUE
ENDFUNC

FUNC FLOAT GET_NEARBY_GROUND_LEVEL(VECTOR vPos)
	
	FLOAT fZ
	INT i
	
	REPEAT 5 i
		vPos += <<1.0,1.0,0.0>>
		GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vPos + <<0.0,0.0,100.0>>, fZ)
		IF fz > 0.0
			RETURN fZ
		ENDIF
	ENDREPEAT
	
	PRINTLN("GET_NEARBY_GROUND_LEVEL couldn't find a proper ground z! Returning windmill ground lowpoint of 44.0")
	RETURN 44.0
	
ENDFUNC

PROC IS_CAMERA_IN_A_MAP_ESCAPE(FMMC_CAM_DATA &sCamDataPassed)
	
	FLOAT fZ
	VECTOR vCamPos = GET_CAM_COORD(sCamDataPassed.cam)
	
//	PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE vCamPos = ",vCamPos)
	
	//Windmills
	IF IS_PLAYER_IN_AXIS_ALIGNED_AREA(vCamPos, <<1908.0,1257.9,0.0>>, <<2471.0,2638.7,250.5>>)
		GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vCamPos, fZ)
		PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Windmills norm z = ",fZ)
		IF fZ <= 0.0
			fz = GET_NEARBY_GROUND_LEVEL(vCamPos)
			PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Windmills near z = ",fZ)
			IF vCamPos.z < fz
				VECTOR vNewPos = vCamPos + <<1.0,0.0,fz + 25.0>>
				SET_CAM_COORD(sCamDataPassed.cam, vNewPos)
				PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Windmills new pos = ",vNewPos)
			ENDIF
		ENDIF
	ENDIF
	
	//Hawick Building
	IF IS_PLAYER_IN_AXIS_ALIGNED_AREA(vCamPos, <<359.0,-54.0,0.0>>, <<424.0,7.5,155.0>>)
		GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vCamPos, fZ)
		PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Hawick norm z = ",fZ)
		IF fZ <= 0.0
			fz = GET_NEARBY_GROUND_LEVEL(vCamPos)
			PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Hawick near z = ",fZ)
			IF vCamPos.z < fz
				VECTOR vNewPos = vCamPos + <<1.0,0.0,fz + 25.0>>
				SET_CAM_COORD(sCamDataPassed.cam, vNewPos)
				PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Hawick new pos = ",vNewPos)
			ENDIF
		ENDIF
	ENDIF
	
	//Ko Casual
	IF IS_PLAYER_IN_AXIS_ALIGNED_AREA(vCamPos, <<-690.1, -943.1, 0.0>>, <<-660.3, -926.2, 42.2>>)
		GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vCamPos, fZ)
		PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Ko Casual norm z = ",fZ)
		IF fZ <= 0.0
			fz = GET_NEARBY_GROUND_LEVEL(vCamPos)
			PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Ko Casual near z = ",fZ)
			IF vCamPos.z < fz
				VECTOR vNewPos = vCamPos + <<1.0,0.0,fz + 25.0>>
				SET_CAM_COORD(sCamDataPassed.cam, vNewPos)
				PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Ko Casual new pos = ",vNewPos)
			ENDIF
		ENDIF
	ENDIF
	
	//Hoard House
	IF IS_PLAYER_IN_AXIS_ALIGNED_AREA(vCamPos, <<-674.1, -885.8, 29.4>>, <<-657.3, -857.5, 42.9>>)
		GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vCamPos, fZ)
		PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Hoard House norm z = ",fZ)
		IF fZ <= 0.0
			fz = GET_NEARBY_GROUND_LEVEL(vCamPos)
			PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Hoard House near z = ",fZ)
			IF vCamPos.z < fz
				VECTOR vNewPos = vCamPos + <<1.0,0.0,fz + 25.0>>
				SET_CAM_COORD(sCamDataPassed.cam, vNewPos)
				PRINTLN("aIS_CAMERA_IN_A_MAP_ESCAPE Hoard House new pos = ",vNewPos)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//This checks if all 4 corner points of a vehicle have no ground z below them.
//This should only be used if the vehicle is within certain areas otherwise if the vehicle is
//too high in the air, or there is no collision loaded this will return true which it shouldn't
FUNC BOOL DOES_VEHICLE_APPEAR_TO_BE_ENTIRELY_UNDERGROUND(ENTITY_INDEX eiCar)
	
	MODEL_NAMES mnCar
	VECTOR vMin, vMax
	VECTOR CarCorner[4]
	FLOAT fz
	INT i
	
	mnCar = GET_ENTITY_MODEL(eiCar)
	GET_MODEL_DIMENSIONS(mnCar, vMin, vMax)
	
	CarCorner[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiCar, <<vMin.x,vMin.y,0.0>>)
	CarCorner[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiCar, <<vMin.x,vMax.y,0.0>>)
	CarCorner[2] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiCar, <<vMax.x,vMin.y,0.0>>)
	CarCorner[3] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiCar, <<vMax.x,vMax.y,0.0>>)
	
	FOR i = 0 TO 3
		GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(CarCorner[i], fZ)
		IF fZ != 0.0
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_PLAYER_IN_A_MAP_ESCAPE(ENTITY_INDEX eiPlayer)
	
	FLOAT fZ
	VECTOR vPlayerPos
	
	IF DOES_ENTITY_EXIST(eiPlayer)
	AND NOT IS_ENTITY_DEAD(eiPlayer)
		vPlayerPos = GET_ENTITY_COORDS(eiPlayer)
	ENDIF
	
//	PRINTLN("IS_PLAYER_IN_A_MAP_ESCAPE vCamPos = ",vPlayerPos)
	
	IF g_bInCreatorMapEscape
		PRINTLN("IS_PLAYER_IN_A_MAP_ESCAPE - g_bInCreatorMapEscape is TRUE from BG Script")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vPlayerPos)
	AND DOES_ENTITY_EXIST(eiPlayer)
	AND NOT IS_ENTITY_DEAD(eiPlayer) //Extra checks to prevent assert
	#IF IS_DEBUG_BUILD
	AND NOT IS_ROCKSTAR_DEV()		// To allow fm creator access to interiors
	#ENDIF
		
		//Life Invader
		IF IS_PLAYER_IN_AXIS_ALIGNED_AREA(vPlayerPos, <<-1098.8,-265.5,0.0>>, <<-1034.4,-219.8,85.9>>)
			PRINTLN("IS_PLAYER_IN_A_MAP_ESCAPE Player near Life Invader!")
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1044.921509,-229.959946,43.169811>>, <<-1049.988159,-239.923309,50.786922>>, 11.500000)
				PRINTLN("IS_PLAYER_IN_A_MAP_ESCAPE Player near Life Invader!")
				RETURN TRUE
			ENDIF
		ENDIF
		
		//Windmills
		IF IS_PLAYER_IN_AXIS_ALIGNED_AREA(vPlayerPos, <<1908.0,1257.9,0.0>>, <<2471.0,2638.7,250.5>>)
			IF vPlayerPos.z < 135.0
				GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vPlayerPos, fZ)
				IF fZ = 0.0
					PRINTLN("IS_PLAYER_IN_A_MAP_ESCAPE Player is under map!")
					IF DOES_VEHICLE_APPEAR_TO_BE_ENTIRELY_UNDERGROUND(eiPlayer)
						PRINTLN("IS_PLAYER_IN_A_MAP_ESCAPE Player vehicle looks to be fully underground!")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Under the map in general
		IF vPlayerPos.z < 0.0
			GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vPlayerPos, fZ)
			IF fZ = 0.0
				PRINTLN("IS_PLAYER_IN_A_MAP_ESCAPE Player is under map!")
				IF NOT IS_ENTITY_IN_WATER(eiPlayer)
					PRINTLN("IS_PLAYER_IN_A_MAP_ESCAPE Player is not in water!")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		//Casino Penthouse
		IF IS_ENTITY_IN_ANGLED_AREA(eiPlayer, <<967.2, 78.97, 0.0>>, <<990.275, 64.182, 121.3>>, 19.0)
		OR IS_ENTITY_IN_ANGLED_AREA(eiPlayer, <<986.4, 75.88, 0.0>>, <<939.108, -0.32, 118.2>>, 19.0)
			
			PRINTLN("IS_PLAYER_IN_A_MAP_ESCAPE Casino penthouse check")
			
			RETURN TRUE
				
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC KEEP_CAMERA_WITHIN_WORLD_BOUNDS(FMMC_CAM_DATA &sCamDataPassed)
	// Keep inside bounds
	IF IS_SKYSWOOP_AT_GROUND()
	AND DOES_CAM_EXIST(sCamDataPassed.cam)
		FLOAT fMaxDist = 9999.0
		
		IF VDIST2(GET_CAM_COORD(sCamDataPassed.cam), <<0,0,0>>) > fMaxDist * fMaxDist
			VECTOR vPos = GET_CAM_COORD(sCamDataPassed.cam)
			vPos = NORMALISE_VECTOR(vPos) * fMaxDist
			SET_CAM_COORD(sCamDataPassed.cam, vPos)
			PRINTLN("KEEP_CAMERA_WITHIN_WORLD_BOUNDS - Keeping camera within bounds - size ", fMaxDist)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FMMC_CAMERA(FMMC_LOCAL_STRUCT &sFMMCdataPassed, FMMC_CAM_DATA &sCamDataPassed, INT iMenuState, FLOAT fHeight = 80.0, BOOL bDisableMapWarp = FALSE)
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
			IF GET_FRAME_COUNT() % 10 = 0
				CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === MAINTAIN_FMMC_CAMERA - running ")
			ENDIF
		ENDIF
	#ENDIF
	
	IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	AND NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL))
	OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	AND IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_SUB_DESCEND))
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_FLY_CAM_VERTICAL_RESPONSE(sCamDataPassed.cam, 160, 160)
		ENDIF
	ELSE
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_FLY_CAM_VERTICAL_RESPONSE(sCamDataPassed.cam)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sFMMCdataPassed.iBitSet, bCameraActive)
	
		IF IS_SKYSWOOP_AT_GROUND() = FALSE
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			g_IsMainCreatorCameraRunning = TRUE	
			PRINTLN("Setting g_IsMainCreatorCameraRunning to TRUE (1)")
		ELSE
			IF g_IsMainCreatorCameraRunning
				g_IsMainCreatorCameraRunning = FALSE
				PRINTLN("Setting g_IsMainCreatorCameraRunning to FALSE (2)")
			ENDIF
		ENDIF
		
		IF NOT DOES_CAM_EXIST(sCamDataPassed.cam)
		
			HIDE_HUD_AND_RADAR_THIS_FRAME()
				
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === NOT DOES_CAM_EXIST(sCamDataPassed.cam) ")
					ENDIF
				ENDIF
			#ENDIF
			
			sCamDataPassed.inTerp = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
			SET_CAM_PARAMS(sCamDataPassed.inTerp, GET_FINAL_RENDERED_CAM_COORD(), 			GET_FINAL_RENDERED_CAM_ROT(), 		GET_FINAL_RENDERED_CAM_FOV())
			SET_CAM_ACTIVE(sCamDataPassed.inTerp, TRUE)			
			
			sCamDataPassed.cam 	= CREATE_CAM("DEFAULT_SCRIPTED_FLY_CAMERA", FALSE)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
				IF NOT IS_VECTOR_ZERO(sCamDataPassed.camPos)					
					PRINTLN("5")
					SET_CAM_COORD(sCamDataPassed.cam, sCamDataPassed.camPos)
					SET_CAM_ROT(sCamDataPassed.cam, <<-89.5,0,0>>)
				ELSE
					PRINTLN("6")
					VECTOR vHeight
					FLOAT fHigh
					vHeight = GET_ENTITY_COORDS(PLAYER_PED_ID())
					fHigh = GET_APPROX_HEIGHT_FOR_POINT(vHeight.x, vHeight.y)
					PRINTLN("PLAYER IS THIS HIGH ", fHigh)
					IF fHeight > 0
					ENDIF
					vHeight.z = fHigh + 40
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vHeight, DEFAULT, DEFAULT, DEFAULT, FALSE)
					SET_CAM_COORD(sCamDataPassed.cam, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					SET_CAM_ROT(sCamDataPassed.cam, <<-89.5,0,0>>)
					SET_SKYSWOOP_STAGE(SKYSWOOP_INSKYSTATIC)
					
					IF DOES_CAM_EXIST(g_SkyCamData.camSky)
						IF IS_CAM_ACTIVE(g_SkyCamData.camSky)
							SET_CAM_PARAMS(sCamDataPassed.inTerp, GET_CAM_COORD(g_SkyCamData.camSky), 		GET_CAM_ROT(g_SkyCamData.camSky), 	GET_CAM_FOV(g_SkyCamData.camSky))
							SET_CAM_PARAMS(sCamDataPassed.inTerp, GET_CAM_COORD(sCamDataPassed.cam), 		GET_CAM_ROT(sCamDataPassed.cam), 	GET_CAM_FOV(sCamDataPassed.cam), 1000)
						ELSE
							SET_CAM_PARAMS(sCamDataPassed.inTerp, GET_FINAL_RENDERED_CAM_COORD(), 			GET_FINAL_RENDERED_CAM_ROT(), 		GET_FINAL_RENDERED_CAM_FOV())
							SET_CAM_PARAMS(sCamDataPassed.inTerp, GET_CAM_COORD(sCamDataPassed.cam), 		GET_CAM_ROT(sCamDataPassed.cam),	GET_CAM_FOV(sCamDataPassed.cam), 1000)
						ENDIF
					ENDIF
				ENDIF
				
				SET_CAM_ACTIVE(sCamDataPassed.cam, FALSE)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)

				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
				SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
				SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				
			ENDIF
		ELSE	
		
			IF sCamDataPassed.fCamInterping < 1.0
				IF NOT sCamDataPassed.bClearToInterp
					INT iShapeResult
					VECTOR vShapePos
					VECTOR vShapeNormal
					ENTITY_INDEX hitEntity
		
					IF sCamDataPassed.siSnapInterpCheck = NULL
						sCamDataPassed.siSnapInterpCheck = START_SHAPE_TEST_LOS_PROBE(sCamDataPassed.vCachedCamPos,sCamDataPassed.vTargetCamPos+sCamDataPassed.vCachedCamOffset,SCRIPT_INCLUDE_ALL)
					ELSE
						SHAPETEST_STATUS eShapeResult = GET_SHAPE_TEST_RESULT(sCamDataPassed.siSnapInterpCheck, iShapeResult, vShapePos, vShapeNormal, hitEntity)
						IF eShapeResult = SHAPETEST_STATUS_RESULTS_READY
							IF iShapeResult = 0
								sCamDataPassed.bClearToInterp = TRUE
							ELSE
								RELEASE_SCRIPT_GUID_FROM_ENTITY(hitEntity)
								sCamDataPassed.fCamInterping = 1.0
							ENDIF
							sCamDataPassed.siSnapInterpCheck = NULL
						ELIF eShapeResult = SHAPETEST_STATUS_NONEXISTENT
							RELEASE_SCRIPT_GUID_FROM_ENTITY(hitEntity)
							sCamDataPassed.siSnapInterpCheck = NULL
						ENDIF
					ENDIF
				ENDIF
				IF sCamDataPassed.bClearToInterp
					sCamDataPassed.bIsCamInterping = TRUE
					PRINTLN("sCamDataPassed.fCamInterping = ", sCamDataPassed.fCamInterping)
					sCamDataPassed.fCamInterping += 0.1
					VECTOR vInterpPos = INTERP_VECTOR(sCamDataPassed.vCachedCamPos, sCamDataPassed.vTargetCamPos+sCamDataPassed.vCachedCamOffset, sCamDataPassed.fCamInterping, INTERPTYPE_COSINE)
					SET_CAM_COORD(sCamDataPassed.cam, vInterpPos)
				ENDIF
			ELSE
				IF sCamDataPassed.bIsCamInterping
					sCamDataPassed.bIsCamInterping = FALSE
					sCamDataPassed.bFinishedInterp = TRUE
				ENDIF
				sCamDataPassed.bClearToInterp = FALSE
				STOP_CAM_POINTING(sCamDataPassed.cam)
			ENDIF
		
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === DOES_CAM_EXIST(sCamDataPassed.cam) ")
					ENDIF
				ENDIF
			#ENDIF	
			
			IF CONTENT_IS_USING_ARENA()
				VECTOR vPos = GET_CAM_COORD(sCamDataPassed.cam)
				
				IF (vPos.x > 2782.2 AND vPos.x < 2813.3)
				AND (vPos.y < -3782.2 AND vPos.y > -3819.3)
				AND NOT HAS_NET_TIMER_STARTED(sCamDataPassed.stStuckTimer)
					SET_FLY_CAM_MAX_HEIGHT(sCamDataPassed.cam, 188.0)
				ELSE
					SET_FLY_CAM_MAX_HEIGHT(sCamDataPassed.cam, 215.0)
				ENDIF
			ELSE
				SET_FLY_CAM_MAX_HEIGHT(sCamDataPassed.cam, cfCameraMaxHeight)
			ENDIF
			
			IS_CAMERA_IN_A_MAP_ESCAPE(sCamDataPassed)
			
			IF IS_CREATOR_MENU_TRIGGERED() = FALSE
			AND (IS_SKYSWOOP_AT_GROUND()
			OR SET_SKYSWOOP_DOWN(sCamDataPassed.cam, TRUE, TRUE))
				
				#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
						IF GET_FRAME_COUNT() % 10 = 0
							CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === IS_SKYSWOOP_AT_GROUND = TRUE ")
						ENDIF
					ENDIF
				#ENDIF
				
				IF NOT IS_CAM_ACTIVE(sCamDataPassed.cam)
					IF DOES_CAM_EXIST(sCamDataPassed.inTerp)
						IF NOT IS_CAM_INTERPOLATING(sCamDataPassed.inTerp)//g_SkyCamData.camID
							DESTROY_CAM(sCamDataPassed.inTerp)
							SET_CAM_ACTIVE(sCamDataPassed.cam, TRUE)
							SET_BIT(sFMMCdataPassed.iBitSet, biCameraNotTerping)
							PRINTLN("DESTROY INTERP CAM")
						ELSE
							CLEAR_BIT(sFMMCdataPassed.iBitSet, biCameraNotTerping)
						ENDIF	
					ELSE
						SET_CAM_ACTIVE(sCamDataPassed.cam, TRUE)
						PRINTLN("sCamDataPassed.cam IS ACTIVE")
					ENDIF	
				ENDIF	
				VECTOR vPlayerPos
				IF HAS_PLAYER_PED_MAP_WARPED_WITH_FRONT_END(sFMMCdataPassed, vPlayerPos)
				AND NOT bDisableMapWarp
					#IF IS_DEBUG_BUILD
						IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
							IF GET_FRAME_COUNT() % 10 = 0
								CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === HAS_PLAYER_PED_MAP_WARPED_WITH_FRONT_END(sFMMCdataPassed, vPlayerPos) ")
							ENDIF
						ENDIF
					#ENDIF	
					
					PRINTLN("HAS_PLAYER_PED_MAP_WARPED_WITH_FRONT_END = TRUE")
					
					IF NOT IS_BIT_SET(sFMMCdataPassed.iBitSet, bSetCameraAbovePlacedPropTemplate)
						SET_CAM_COORD(sCamDataPassed.cam, vPlayerPos + <<0.0, 0.0, 1.0>>)
					ENDIF
					SET_CAM_ROT(sCamDataPassed.cam, sCamDataPassed.camRot)
					VECTOR newRot
					newRot = GET_CAM_ROT(sCamDataPassed.cam)
					newRot.x = -89.5
					SET_CAM_ROT(sCamDataPassed.cam, newRot)
					sFMMCdataPassed.vPlayerPos = vPlayerPos
					
					IF iMenuState = MENU_STATE_DEFAULT
						DO_SCREEN_FADE_OUT(0)
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							NEW_LOAD_SCENE_STOP()
						ENDIF
						sCamDataPassed.bLoadingArea = TRUE
						PRINTLN("Player Warped - iMenuState = ", iMenuState)
					ELSE
						PRINTLN("No Warp - iMenuState = ", iMenuState)
					ENDIF
				ELIF sCamDataPassed.bLoadingArea
				
					#IF IS_DEBUG_BUILD
						IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
							IF GET_FRAME_COUNT() % 10 = 0
								CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === sCamDataPassed.bLoadingArea ")
							ENDIF
						ENDIF
					#ENDIF
				
					VECTOR loadRot, loadPos
					IF GET_RENDERING_CAM() != NULL
						loadRot = GET_CAM_ROT(GET_RENDERING_CAM())
						loadPos = GET_CAM_COORD(GET_RENDERING_CAM())
					ENDIF						
					PRINTLN("PLAYER WARPED AND WE ARE LOADING THE AREA - <<", loadPos.x, ", ", loadPos.y, ", ", loadPos.z, ">>")
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_START(loadPos, loadRot, 200)
						PRINTLN("START THE NEW LOAD SCENE")
					ELSE
						PRINTLN("LOADING")
						IF IS_NEW_LOAD_SCENE_LOADED()
							DO_SCREEN_FADE_IN(500)
							NEW_LOAD_SCENE_STOP()
							sCamDataPassed.bLoadingArea = FALSE
							PRINTLN("FINISHED THE LOAD SCENE")
						ENDIF
					ENDIF
				ELSE	
				
					#IF IS_DEBUG_BUILD
						IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
							IF GET_FRAME_COUNT() % 10 = 0
								CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === ELSE ")
							ENDIF
						ENDIF
					#ENDIF
					
					IF WAS_FLY_CAM_CONSTRAINED_ON_PREVIOUS_UDPATE(sCamDataPassed.cam)
						SET_BIT(sFMMCdataPassed.iBitSet, biShakeCam)
					ENDIF
					
					//Get the height of the camera off the ground. 
					VECTOR vCamPos = GET_CAM_COORD(sCamDataPassed.cam)
					FLOAT zGroundHeight
					
					IF (GET_WATER_HEIGHT_NO_WAVES(vCamPos, zGroundHeight)
					OR GET_GROUND_Z_FOR_3D_COORD(vCamPos, zGroundHeight))
					AND NOT IS_CORONA_UNDERWATER()
						//get the fdiffrence in height
						sFMMCdataPassed.fLastZ = zGroundHeight
						zGroundHeight = vCamPos.z - zGroundHeight
						FLOAT fSpeed = (zGroundHeight * tfSpeedMultiplier)
						FLOAT fAcceleration = (zGroundHeight * tfAccelerationMultiplier)
						IF fSpeed < 40
							fSpeed = 40
						ENDIF
						IF fAcceleration < 40
							fAcceleration = 40
						ENDIF
						
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
							IF (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_SUB_DESCEND))
							OR (NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) 
							AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y))
								fSpeed = 200
								fAcceleration = 200
							ENDIF
						ENDIF
						
						sFMMCdataPassed.bGotGroundLastFrame = TRUE
						SET_FLY_CAM_HORIZONTAL_RESPONSE(sCamDataPassed.cam, fSpeed, fAcceleration)
					ELSE
						//Didn't get z
						//Use the last known z and then have a guess at the speed if the last z is not 0
						IF sFMMCdataPassed.fLastZ != 0.0
							//get the fdiffrence in height
							zGroundHeight = sFMMCdataPassed.fLastZ
							zGroundHeight = vCamPos.z - zGroundHeight					
							FLOAT fSpeed = (zGroundHeight * tfSpeedMultiplier)
							FLOAT fAcceleration = (zGroundHeight * tfAccelerationMultiplier)
							IF fSpeed < 40
								fSpeed = 40
							ENDIF
							IF fAcceleration < 40
								fAcceleration = 40
							ENDIF
							
							IF GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
								IF (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
								AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_SUB_DESCEND))
								OR (NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) 
								AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y))
									fSpeed = 200
									fAcceleration = 200
								ENDIF
							ENDIF
							
							SET_FLY_CAM_HORIZONTAL_RESPONSE(sCamDataPassed.cam, fSpeed, fAcceleration)
						ELSE
							FLOAT fSpeed = 40
							FLOAT fAcceleration = 40
							IF GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR								
								IF (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
								AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_SUB_DESCEND))
								OR (NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) 
								AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y))
									fSpeed = 200
									fAcceleration = 200
								ENDIF							
							ENDIF
							
							//Set to default
							SET_FLY_CAM_HORIZONTAL_RESPONSE(sCamDataPassed.cam, fSpeed, fAcceleration)
						ENDIF
						//Didn't get the height
						sFMMCdataPassed.bGotGroundLastFrame = FALSE
					ENDIF
					
					IF (g_bUpdateSpectatorPosition)
						MAINTAIN_PLAYER_COORDS(sFMMCdataPassed, sCamDataPassed.cam)
					ENDIF
				ENDIF
				
				IF WAS_FLY_CAM_CONSTRAINED_ON_PREVIOUS_UDPATE(sCamDataPassed.cam)
					IF NOT HAS_NET_TIMER_STARTED(sCamDataPassed.stStuckTimer)
						REINIT_NET_TIMER(sCamDataPassed.stStuckTimer)
					ELSE						
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCamDataPassed.stStuckTimer) > 1000
							IF IS_CAMERA_IN_A_PROP(sCamDataPassed)
								SET_CAM_COORD(sCamDataPassed.cam, GET_CAM_COORD(sCamDataPassed.cam) + <<0.0, 0.0, 0.2>>)
								PRINTLN("Moving creator camera up because it was stuck in a prop!")
							ENDIF
							
							VECTOR vCamPosNow = GET_CAM_COORD(sCamDataPassed.cam)
							IF IS_PUBLIC_ARENA_CREATOR()
							AND vCamPosNow.z > 185.0
								SET_CAM_COORD(sCamDataPassed.cam, GET_ARENA_REFRESH_CAM_POS(TRUE))
								PRINTLN("[ARENA] Moving camera to refresh pos because it got stuck!")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					RESET_NET_TIMER(sCamDataPassed.stStuckTimer)
				ENDIF
				
				IF IS_BIT_SET(sFMMCdataPassed.iBitSet, bSetCameraAbovePlacedPropTemplate)
					PRINTLN("[HAS_PLAYER_PED_MAP_WARPED_WITH_FRONT_END] - CLEAR_BIT(sFMMCdataPassed.iBitSet, bSetCameraAbovePlacedPropTemplate)")	
					CLEAR_BIT(sFMMCdataPassed.iBitSet, bSetCameraAbovePlacedPropTemplate)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(sFMMCdataPassed.iBitSet, biShakeCam)
			IF IS_PLAYER_SCTV(PLAYER_ID())						//No shake in SCTV
				CLEAR_BIT(sFMMCdataPassed.iBitSet, biShakeCam)
			ELSE
				IF sFMMCdataPassed.iCamShakeTimer = 0
					sFMMCdataPassed.iCamShakeTimer = GET_GAME_TIMER() + 500
					SHAKE_CAM(sCamDataPassed.cam, "VIBRATE_SHAKE", 0.5)
					PLAY_SOUND_FRONTEND(-1, "HUD_FREEMODE_CANCEL_MASTER")
				ELSE
					IF GET_GAME_TIMER() > sFMMCdataPassed.iCamShakeTimer
						sFMMCdataPassed.iCamShakeTimer = 0
						SHAKE_CAM(sCamDataPassed.cam, "VIBRATE_SHAKE", 0)
						CLEAR_BIT(sFMMCdataPassed.iBitSet, biShakeCam)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
		
	ELSE 
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			RENDER_SCRIPT_CAMS(FALSE, TRUE, 500)
			DESTROY_CAM(sCamDataPassed.cam)
			PRINTSTRING("CAMERA KILLED")PRINTNL()
		ENDIF
		IF DOES_CAM_EXIST(sCamDataPassed.inTerp)
			RENDER_SCRIPT_CAMS(FALSE, TRUE, 500)
			DESTROY_CAM(sCamDataPassed.inTerp)
			PRINTSTRING("CAMERA inTerp KILLED")PRINTNL()
		ENDIF
	ENDIF
ENDPROC

PROC CLEAN_UP_FMMC_CAMERA(BOOL CollisionProof = TRUE, BOOL SetVisibility = TRUE)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		IF (g_bUpdateSpectatorPosition)
			VECTOR vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
			FLOAT fTemp
			IF GET_GROUND_Z_FOR_3D_COORD(vTemp, ftemp)
				vTemp.Z = ftemp
			ENDIF
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vTemp, DEFAULT, DEFAULT, DEFAULT, FALSE)
		ENDIF
		IF SetVisibility
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, CollisionProof, FALSE)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC ROTATE_VECTOR_FMMC(VECTOR &InVec, VECTOR rotation)
      
      FLOAT CosAngle
      FLOAT SinAngle
      VECTOR ReturnVec

      // Rotation about the x axis 
      CosAngle = COS(rotation.x)
      SinAngle = SIN(rotation.x)
      ReturnVec.x = InVec.x
      ReturnVec.y = (CosAngle * InVec.y) - (SinAngle * InVec.z)
      ReturnVec.z = (SinAngle * InVec.y) + (CosAngle * InVec.z)
      InVec = ReturnVec

      // Rotation about the y axis
      CosAngle = COS(rotation.y)
      SinAngle = SIN(rotation.y)
      ReturnVec.x = (CosAngle * InVec.x) + (SinAngle * InVec.z)
      ReturnVec.y = InVec.y
      ReturnVec.z = (CosAngle * InVec.z) - (SinAngle * InVec.x) 
      InVec = ReturnVec
      
      // Rotation about the z axis 
      CosAngle = COS(rotation.z)
      SinAngle = SIN(rotation.z)
      ReturnVec.x = (CosAngle * InVec.x) - (SinAngle * InVec.y)
      ReturnVec.y = (SinAngle * InVec.x) + (CosAngle * InVec.y)
      ReturnVec.z = InVec.z
      InVec = ReturnVec
ENDPROC

FUNC VECTOR GET_VECTOR_INFRONT_OF_CURRENT_RENDERING_CAM(VECTOR vStart, FLOAT fDistance = 1000.0)
	VECTOR vEnd
	//end vector - pointing north
	vEnd	= <<0.0, 1.0, 0.0>>
	//point it in the same direction as the camera
	ROTATE_VECTOR_FMMC(vEnd, GET_FINAL_RENDERED_CAM_ROT())
	//Make the normilised roted vector 400 times larger
	vEnd.x *= fDistance
	vEnd.y *= fDistance
	vEnd.z *= fDistance
	//add it on to the start vector to get the end vector coordinates
	vEnd += vStart
	RETURN vEnd
ENDFUNC

 FUNC  BOOL IS_VECTOR_IN_WORLD_BOUNDS(VECTOR vLoc)
 	IF vLoc.X < 4500.0
 	AND vLoc.X > -4500.0
 	AND vLoc.Y < 7800.0
 	AND vLoc.Y > -5000.0
 	AND vLoc.Z < 3000.0
 	AND vLoc.Z > -1400.0
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_ROCKSTAR_DEV() //Could be repalced with an Island Loaded check if we let the public use the Island
		IF vLoc.X < 8000.0
	 	AND vLoc.X > 1000.0
	 	AND vLoc.Y < -1500.0
	 	AND vLoc.Y > -7500.0
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	RETURN FALSE
 ENDFUNC

PROC MAINTAIN_SCREEN_CENTER_COORD(VECTOR vCamRot, INVISIBLE_OBJECT_STRUCT &sInvisibleObjects, CREATION_VARS_STRUCT &sCurrentVarsStructPassed, structFMMC_MENU_ITEMS& sFMMCmenu, BOOL bDraw, HUD_COLOURS hcCurrentCoronaColour, FLOAT &fIncrease, BOOL bDrawShadow = FALSE, BOOL bIsBoat = FALSE, BOOL bOnGround = FALSE, BOOL bDoingDropOff = FALSE, BOOL bIgnoreVehicles = FALSE, BOOL bCanBeUnderwater = FALSE, BOOL bForceOnGroundCoronaToGround = TRUE, BOOL bMeldProps = FALSE, INT iDistFail = 500, FLOAT fLastCamDist = 15.0)
	
//	PRINTLN("MAINTAIN_SCREEN_CENTER_COORD START")
	
	FLOAT fDistanceForLine = 2400
	IF vCamRot.x >= -10 AND NOT bOnGround
		EXIT
	ENDIF
	
	INT iTemp	
	//The cameras position, note this result is a frame behind
	VECTOR vStart 		= GET_FINAL_RENDERED_CAM_COORD()
	BOOL bUseSphereCast = (sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS AND NOT bOnGround) AND NOT IS_ROCKSTAR_DEV()
	FLOAT fSphereCastSize = 1.75
	
	if bDrawShadow
	AND bDraw
		IF bOnGround
			fDistanceForLine = 700
		ELSE
			fDistanceForLine = (vStart.z - fIncrease)/sin(ABSF(vCamRot.x))
		ENDIF

		IF fDistanceForLine <= 5.0
			fDistanceForLine = 5.0
		ENDIF
		
		IF vStart.z < fIncrease
		AND sCurrentVarsStructPassed.iWaterState = ciCoronaAboveWater
			fIncrease = vStart.z-fDistanceForLine
			EXIT
		ENDIF		

	ENDIF
	
	//End of line start
	VECTOR vEnd 		= GET_VECTOR_INFRONT_OF_CURRENT_RENDERING_CAM(vStart, fDistanceForLine)
	//The return vector
	VECTOR vReturnTemp
	//the normal vector
	VECTOR vNormal
	
	ENTITY_INDEX ignoreEntity = PLAYER_PED_ID()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		ignoreEntity = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ENDIF
	
	IF bUseSphereCast
		sCurrentVarsStructPassed.stiScreenCentreLOS = START_SHAPE_TEST_SWEPT_SPHERE(vStart, vEnd, fSphereCastSize, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_RIVER|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_VEHICLE, ignoreEntity, SCRIPT_SHAPETEST_OPTION_IGNORE_NO_COLLISION)
	ELSE
		IF bIgnoreVehicles
			sCurrentVarsStructPassed.stiScreenCentreLOS = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vStart, vEnd,  SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_RIVER|SCRIPT_INCLUDE_OBJECT, ignoreEntity, SCRIPT_SHAPETEST_OPTION_IGNORE_NO_COLLISION)
		ELIF bMeldProps
			sCurrentVarsStructPassed.stiScreenCentreLOS = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vStart, vEnd,  SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_RIVER|SCRIPT_INCLUDE_VEHICLE, ignoreEntity)
		ELSE
			sCurrentVarsStructPassed.stiScreenCentreLOS = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vStart, vEnd,  SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_RIVER|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_VEHICLE, ignoreEntity, SCRIPT_SHAPETEST_OPTION_IGNORE_NO_COLLISION)
		ENDIF
	ENDIF
	
	ENTITY_INDEX hitEntity
	
//	PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - vStart = ", vStart)
//	PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - vEnd = ", vEnd)
	
	IF GET_SHAPE_TEST_RESULT(sCurrentVarsStructPassed.stiScreenCentreLOS, iTemp, vReturnTemp, vNormal, hitEntity) = SHAPETEST_STATUS_RESULTS_READY
		//Set to null
		sCurrentVarsStructPassed.stiScreenCentreLOS = NULL
//		PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - GET_SHAPE_TEST_RESULT returned VPos as ", vReturnTemp)
	ELSE
		EXIT
	ENDIF
	
	IF sCurrentVarsStructPassed.vCoronaHitEntity <> NULL
	AND sCurrentVarsStructPassed.vCoronaHitEntity <> hitEntity
		RELEASE_SCRIPT_GUID_FROM_ENTITY(sCurrentVarsStructPassed.vCoronaHitEntity)
	ENDIF
	
	IF IS_AN_ENTITY(hitEntity)
		IF DOES_ENTITY_EXIST(hitEntity)
		AND IS_ENTITY_AN_OBJECT(hitEntity)
		AND (sFMMCmenu.iEntityCreation = CREATION_TYPE_PROPS OR sFMMCmenu.iEntityCreation = CREATION_TYPE_DYNOPROPS)
		AND NOT (sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES OR sFMMCmenu.sActiveMenu = efmmc_PROP_TEMPLATE_BASE)
			IF IS_MODEL_AN_ARENA_PROP_OR_TRAP(GET_ENTITY_MODEL(hitEntity))
			AND NOT IS_CURRENT_PROP_LIBRARY_AN_ARENA_LIBRARY(sFMMCmenu)
//				PRINTLN("[TMS] MAINTAIN_SCREEN_CENTER_COORD - Ignoring this arena prop we're hovering over")
				hitEntity = null
			ELIF NOT IS_MODEL_AN_ARENA_PROP_OR_TRAP(GET_ENTITY_MODEL(hitEntity))
			AND IS_CURRENT_PROP_LIBRARY_AN_ARENA_LIBRARY(sFMMCmenu)
//				PRINTLN("[TMS] MAINTAIN_SCREEN_CENTER_COORD - Ignoring this standard prop we're hovering over")
				hitEntity = null
			ENDIF
		ENDIF
	ENDIF
	
	sCurrentVarsStructPassed.vCoronaHitEntity = hitEntity
	sCurrentVarsStructPassed.vCoronaHitNormal = vNormal
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
		IF DOES_ENTITY_EXIST(hitEntity)
		AND IS_ENTITY_AN_OBJECT(hitEntity)
			STRING sEntName = GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(hitEntity))
			
			TEXT_LABEL_63 tlEntityDebug = "Ent: "
			tlEntityDebug += sEntName
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(hitEntity, tlEntityDebug, 0.1)
		ENDIF
	ENDIF
	#ENDIF
	
	//Cap Max Corona Increase Height
	sCurrentVarsStructPassed.bitsetOkToPlace = BS_CAN_PLACE
	sCurrentVarsStructPassed.bLOSHitGround = TRUE
	
	IF bDrawShadow
	AND bDraw
		IF IS_VECTOR_ZERO(vReturnTemp)
//			PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - vReturnTemp = zero")
			vReturnTemp = vEnd
		ELSE
			fIncrease = vReturnTemp.z
		ENDIF
	ENDIF
		
	VECTOR vTempReturnVec
	
	SCRIPT_WATER_TEST_RESULT eResult = TEST_PROBE_AGAINST_ALL_WATER(vStart,vEnd,SCRIPT_INCLUDE_RIVER,vTempReturnVec)

	IF eResult = SCRIPT_WATER_TEST_RESULT_WATER
	AND sCurrentVarsStructPassed.iWaterState != ciCoronaUnderWater
//	AND NOT IS_CORONA_UNDERWATER()
	AND VDIST2(vTempReturnVec, vStart) < VDIST2(vReturnTemp, vStart)
//		PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - TEST_PROBE_AGAINST_ALL_WATER - SCRIPT_WATER_TEST_RESULT_WATER - ", vTempReturnVec)
		vReturnTemp = vTempReturnVec
		sCurrentVarsStructPassed.bCoronaOnWater = TRUE
		sCurrentVarsStructPassed.fLastCamDistance = GET_DISTANCE_BETWEEN_COORDS(vStart, vReturnTemp)
	ELSE
//		PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - TEST_PROBE_AGAINST_ALL_WATER - NOT ON WATER - ", vTempReturnVec)
		INT iObj
		FOR iObj = 0 TO IMAX_INVISIBLE_OBJECTS - 1
			IF DOES_ENTITY_EXIST(sInvisibleObjects.oiInvObj[iObj])
				if GET_ENTITY_FROM_PED_OR_VEHICLE(sInvisibleObjects.oiInvObj[iObj]) = hitEntity
					//vReturnTemp = vTempReturnVec
				ENDIF
			ENDIF
		ENDFOR
		sCurrentVarsStructPassed.fLastCamDistance = GET_DISTANCE_BETWEEN_COORDS(vStart, vReturnTemp)
		sCurrentVarsStructPassed.bCoronaOnWater = FALSE
	ENDIF
	
	IF NOT bOnGround
		IF IS_VECTOR_ZERO(vReturnTemp)
		
			VECTOR vEndGround = vEnd
			vEndGround.z = GET_APPROX_FLOOR_FOR_POINT(vEnd.x, vEnd.y)
			FLOAT fAngle = 90 - ABSF(vCamRot.x)
			FLOAT fTempDist = fDistanceForLine - (vEndGround.z - vEnd.z)/COS(fAngle)
			VECTOR vTempCollPos
			vTempCollPos = GET_VECTOR_INFRONT_OF_CURRENT_RENDERING_CAM(vStart, fTempDist)
//				DRAW_DEBUG_SPHERE(vTempCollPos, 2, 0, 0, 255)
//				PRINTLN("1ST BLUE! End pos - << ", vEnd.x, ", ", vEnd.y, ", ", vEnd.z, ">>")
//				PRINTLN("		Ground above it  - << ", vEndGround.x, ", ", vEndGround.y, ", ", vEndGround.z, ">>")
//				PRINTLN("		Use my crazy system fTempDist - ", fTempDist,", fAngle - ", fAngle,", << ", vTempCollPos.x, ", ", vTempCollPos.y, ", ", vTempCollPos.z, ">>")
				
			vEndGround = vTempCollPos
			vEndGround.z = GET_APPROX_FLOOR_FOR_POINT(vTempCollPos.x, vTempCollPos.y)			
			fTempDist += (vTempCollPos.z - vEndGround.z)/COS(fAngle)
			vTempCollPos = GET_VECTOR_INFRONT_OF_CURRENT_RENDERING_CAM(vStart, fTempDist)
//				DRAW_DEBUG_SPHERE(vTempCollPos, 5, 255, 0, 0)
//				PRINTLN("RED! 	Ground below it  - << ", vEndGround.x, ", ", vEndGround.y, ", ", vEndGround.z, ">>")
//				PRINTLN("		Use my crazy system fTempDist - ", fTempDist,", fAngle - ", fAngle,", << ", vTempCollPos.x, ", ", vTempCollPos.y, ", ", vTempCollPos.z, ">>")
			
			vEndGround = vTempCollPos
			vEndGround.z = GET_APPROX_FLOOR_FOR_POINT(vTempCollPos.x, vTempCollPos.y)			
			fTempDist -= (vEndGround.z - vTempCollPos.z)/COS(fAngle)
			IF fTempDist > fDistanceForLine
				fTempDist = fDistanceForLine
			ENDIF
			vTempCollPos = GET_VECTOR_INFRONT_OF_CURRENT_RENDERING_CAM(vStart, fTempDist)
//				DRAW_DEBUG_SPHERE(vTempCollPos, 3.5, 0, 255, 0)
//				PRINTLN("GREEN! Ground above it  - << ", vEndGround.x, ", ", vEndGround.y, ", ", vEndGround.z, ">>")
//				PRINTLN("		Use my crazy system fTempDist - ", fTempDist,", fAngle - ", fAngle,", << ", vTempCollPos.x, ", ", vTempCollPos.y, ", ", vTempCollPos.z, ">>")
							
//				IF fTempDist = fDistanceForLine
//					PRINTLN("******** FURTHEST POSITION - ", fDistanceForLine, " ********")					
//				ENDIF
			
			vReturnTemp = vTempCollPos
			sCurrentVarsStructPassed.fLastCamDistance = fTempDist

			sCurrentVarsStructPassed.bLOSHitGround = FALSE
			sCurrentVarsStructPassed.bitsetOkToPlace = BS_CANT_PLACE__DIST_FAIL
		ELSE
			IF GET_DISTANCE_BETWEEN_COORDS(vStart, vReturnTemp) > iDistFail
				sCurrentVarsStructPassed.bitsetOkToPlace = BS_CANT_PLACE__DIST_FAIL
				IF GET_DISTANCE_BETWEEN_COORDS(vStart, vReturnTemp) > 800
					sCurrentVarsStructPassed.bLOSHitGround = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF sCurrentVarsStructPassed.bCoronaOnWater = FALSE
	OR IS_CORONA_UNDERWATER()
//		PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - sCurrentVarsStructPassed.bCoronaOnWater = FALSE")
		//vector is not zero update the control vector (last sucessful result)
		sCurrentVarsStructPassed.vSaveVector = vReturnTemp
	ELSE
//		PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - sCurrentVarsStructPassed.bCoronaOnWater = TRUE")
		FLOAT fWaveHeight
		IF GET_WATER_HEIGHT(vReturnTemp + <<0.0, 0.0, 5.0>> ,fWaveHeight)
//			PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - ON WATER 2")
			sCurrentVarsStructPassed.vSaveVector = <<vReturnTemp.x, vReturnTemp.y, fWaveHeight>>
		ELSE
//			PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - ON WATER 3")
			sCurrentVarsStructPassed.vSaveVector = vReturnTemp
		ENDIF
	ENDIF
	
	IF bOnGround		
		IF sCurrentVarsStructPassed.fLastCamDistance > fLastCamDist
			sCurrentVarsStructPassed.fLastCamDistance = fLastCamDist
		ENDIF
	ENDIF
	
	//Get the hud colour as an INT
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(hcCurrentCoronaColour,iR, iG, iB, iA)
	
	//Get the coordainate in the center of the screen based on the last distance from the ground i.e. ignore then Z for just now
	//This is where the corona will be drawn
	sCurrentVarsStructPassed.vCoronaPos = GET_VECTOR_INFRONT_OF_CURRENT_RENDERING_CAM(vStart, sCurrentVarsStructPassed.fLastCamDistance)
	
	IF bOnGround
		
		IF sCurrentVarsStructPassed.bCoronaOnWater = FALSE
		AND bForceOnGroundCoronaToGround
			FLOAT zGround
			GET_GROUND_Z_FOR_3D_COORD(sCurrentVarsStructPassed.vCoronaPos + <<0,0,0.1>>, zGround)
			sCurrentVarsStructPassed.vCoronaPos = <<sCurrentVarsStructPassed.vCoronaPos.x, sCurrentVarsStructPassed.vCoronaPos.y, zGround>>
			FLOAT fTempWaveHeight
			IF GET_WATER_HEIGHT(sCurrentVarsStructPassed.vCoronaPos + <<0.0, 0.0, 0.1>> ,fTempWaveHeight)
			AND NOT bCanBeUnderwater
			AND NOT (IS_CORONA_UNDERWATER() AND IS_UNDERWATER_PLACEMENT_ALLOWED(sFMMCmenu))
				sCurrentVarsStructPassed.bCoronaOnWater = TRUE
				sCurrentVarsStructPassed.vCoronaPos = <<sCurrentVarsStructPassed.vCoronaPos.x, sCurrentVarsStructPassed.vCoronaPos.y, fTempWaveHeight>>
//				PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - 2 On WATER")
			ELSE
				sCurrentVarsStructPassed.bCoronaOnWater = FALSE
//				PRINTLN("MAINTAIN_SCREEN_CENTER_COORD - 2 On LAND")
			ENDIF
		ENDIF
		
		sCurrentVarsStructPassed.vSaveVector = sCurrentVarsStructPassed.vCoronaPos
		
	ENDIF
	
	IF NOT IS_VECTOR_IN_WORLD_BOUNDS(sCurrentVarsStructPassed.vCoronaPos)
		sCurrentVarsStructPassed.bitsetOkToPlace = BS_CANT_PLACE__MAP_FAIL
	ENDIF
	
	//If the corona should be drawn
	IF bDraw
		// Can do height on drop offs.
		IF bDoingDropOff
		AND NOT bOnGround
			bDrawShadow = TRUE
		ENDIF
		//If it's to go on water.
		IF bIsBoat = TRUE
		AND fIncrease = 0.0
			FLOAT fTemp
			IF GET_WATER_HEIGHT(sCurrentVarsStructPassed.vCoronaPos + <<0.0, 0.0, 5.0>> ,fTemp)
			AND NOT (IS_CORONA_UNDERWATER() AND IS_UNDERWATER_PLACEMENT_ALLOWED(sFMMCMenu))
//				PRINTSTRING("ON WATER 3")PRINTNL()
				sCurrentVarsStructPassed.vCoronaPos = <<sCurrentVarsStructPassed.vCoronaPos.x, sCurrentVarsStructPassed.vCoronaPos.y, fTemp>>
			ENDIF
		ENDIF
	ENDIF
	
	sFMMCMenu.vCachedCoronaPos = sCurrentVarsStructPassed.vCoronaPos
//	PRINTLN("MAINTAIN_SCREEN_CENTER_COORD END")
	
ENDPROC






//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////       Deal With Changing camera ANGLE        //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
FUNC BOOL SHOULD_CAMERA_ANGLE_CHANGE_BE_ACTIVE(structFMMC_MENU_ITEMS &sFMMCmenu)
	
	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CAMERA_ROTATE(INT iStickPos, FLOAT fMOvementThreshold)
	
	// PC mouse doesn't need dead-zone
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN TRUE
	ENDIF
	
	IF iStickPos < -fMOvementThreshold
		RETURN TRUE
	ELIF iStickPos > fMOvementThreshold
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC UPDATE_INTRO_CAM_ROTATION_AND_FOV(CAMERA_INDEX ciPassed, BOOL &bInitialStart, BOOL bFOV, BOOL &bResetUpHelp)

	ALLOW_MISSION_CREATOR_WARP(FALSE)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		INT iLx, iLy, iRx, iRy
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLx, iLy, iRx, iRy)
		
		IF NOT IS_LOOK_INVERTED()
			iLy = -iLy
			iRy = -iRy
		ENDIF
		
		/*
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			iRx = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_LOOK_LR) * 127.0)
			iRy = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_LOOK_UD) * 127.0)
		ENDIF
		*/
		
		VECTOR 		vCamSetRot 
		CONST_INT 	ciMOVE_THRESHOLD 30
		CONST_FLOAT	cfMULTIPLYER 	0.01
		CONST_FLOAT	cfMIN 			-89.9
		
		IF DOES_CAM_EXIST(ciPassed)
			vCamSetRot = GET_CAM_ROT(ciPassed)
			
			IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				Set_Fly_Cam_Vertical_Controls_This_Update(ciPassed)
			ELSE
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CREATOR_LT) OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CREATOR_RT) 
					Set_Fly_Cam_Vertical_Controls_This_Update(ciPassed)
				ENDIF
			ENDIF
			
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_CREATOR_LS_BUTTON())
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_CREATOR_RS_BUTTON())
			
			IF NOT bInitialStart				
				
				IF bFOV
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_RESET_FOV_BUTTON())
						SET_CAM_FOV(ciPassed, 40)
						bResetUpHelp = TRUE
					ENDIF
					
					IF GET_CAM_FOV(ciPassed) > 30
						IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_RS_BUTTON())
							IF GET_CAM_FOV(ciPassed) > 39.6
							AND GET_CAM_FOV(ciPassed) < 40.0
								bResetUpHelp = TRUE
							ENDIF
							FLOAT fov
							fov = GET_CAM_FOV(ciPassed) - 0.2
							IF fov <= 30
								fov = 30
							ENDIF
							SET_CAM_FOV(ciPassed, fov)
						ENDIF
					ENDIF
					IF GET_CAM_FOV(ciPassed) < 65
						IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_LS_BUTTON())
							IF GET_CAM_FOV(ciPassed) > 40
							AND GET_CAM_FOV(ciPassed) < 40.4
								bResetUpHelp = TRUE
							ENDIF
							FLOAT fov
							fov = GET_CAM_FOV(ciPassed) + 0.2
							IF fov >= 65
								fov = 65
							ENDIF
							SET_CAM_FOV(ciPassed, fov)
						ENDIF
					ENDIF
				ENDIF
			
				IF vCamSetRot.x < -30.0
					vCamSetRot.x += 3
					IF vCamSetRot.x > -30.0
						vCamSetRot.x = -30.0
					ENDIF
				ELIF vCamSetRot.x > 20.0
					vCamSetRot.x -= 3
					IF vCamSetRot.x < 20.0
						vCamSetRot.x = 20.0
					ENDIF
				ELSE						
					IF SHOULD_CAMERA_ROTATE(iRy, ciMOVE_THRESHOLD)
						vCamSetRot += <<iRy*cfMULTIPLYER, 0.0, 0.0 >>
						IF vCamSetRot.x < -30
							vCamSetRot.x = -30
						ENDIF
						IF vCamSetRot.x > 20
							vCamSetRot.x = 20
						ENDIF	
						PRINTLN("SHOULD_CAMERA_ROTATE = ", vCamSetRot.x)	
					ENDIF								
				ENDIF
				
				IF SHOULD_CAMERA_ROTATE(iRx, ciMOVE_THRESHOLD)
					vCamSetRot -= <<0.0, 0.0, iRx*cfMULTIPLYER * 2>>
				ENDIF	
			ELSE
			
				PRINTLN("INTIAL ROATE")
				
				IF vCamSetRot.x < 0.0
					vCamSetRot.x += 3
					IF vCamSetRot.x > 0.0
						vCamSetRot.x = 0.0						
					ENDIF
				ELIF vCamSetRot.x > 0.0
					vCamSetRot.x -= 3
					IF vCamSetRot.x < 0.0
						vCamSetRot.x = 0.0
					ENDIF
				ENDIF
				
				FLOAT fov
				fov = GET_CAM_FOV(ciPassed)
				IF fov > 40
					fov -= 1
					IF fov <= 40
						fov =  40					
					ENDIF
				ENDIF
				IF fov < 40
					fov += 1
					IF fov >= 40
						fov =  40					
					ENDIF
				ENDIF
				SET_CAM_FOV(ciPassed, fov)
				
				IF vCamSetRot.x = 0.0
				AND fov = 40
					PRINTLN("INTIAL ROATE DONE")
					bInitialStart = FALSE
					bResetUpHelp = TRUE
				ENDIF
			ENDIF
			
			SET_CAM_ROT(ciPassed, vCamSetRot)
		ENDIF
	ENDIF
ENDPROC

PROC CHANGE_SCRIPT_FLY_CAM_ROTATION(CAMERA_INDEX ciPassed, FLOAT fRotAddition = 0.0)
	IF NOT IS_PAUSE_MENU_ACTIVE()
	
			//Get the postions of the analouge sticks
			
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
			OR g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_CTF
				ALLOW_MISSION_CREATOR_WARP(NOT IS_PUBLIC_ARENA_CREATOR())
				//PRINTLN("CHANGE_SCRIPT_FLY_CAM_ROTATION - ALLOW_MISSION_CREATOR_WARP - TRUE")
			ENDIF
			
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
			
			INT iLx, iLy, iRx, iRy
			VECTOR vCamSetRot 
			GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLx, iLy, iRx, iRy)
			IF NOT IS_LOOK_INVERTED()
				iLy = -iLy
				iRy = -iRy
			ENDIF
			
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				iRx = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_LOOK_LR) * 127.0) * 2
				iRy = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_LOOK_UD) * 127.0) * -1
			ENDIF
			
			CONST_INT 	ciMOVE_THRESHOLD 30
			CONST_FLOAT	cfMULTIPLYER 	0.01
			CONST_FLOAT	cfMIN 			-89.9
			FLOAT 		cfMAX 
			
			IF IS_PLAYER_SCTV(PLAYER_ID())
				Set_Fly_Cam_Vertical_Controls_This_Update(ciPassed)
				cfMAX = 0.0
			ELIF IS_ROCKSTAR_DEV()
				cfMAX = 79.0
			ELSE
				IF !g_sMPTunables.bSM_YAW_ADJUST
					cfMAX = -35.0
				ELSE
					cfMAX = -11.5
				ENDIF
			ENDIF
			
			IF DOES_CAM_EXIST(ciPassed)
				vCamSetRot = GET_CAM_ROT(ciPassed)
				
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
					IF GET_CAM_FOV(ciPassed) < 45
						FLOAT fov
						fov = GET_CAM_FOV(ciPassed) + 1
						IF fov >= 45
							fov = 45
						ENDIF
						SET_CAM_FOV(ciPassed, fov)			
					ELIF GET_CAM_FOV(ciPassed) > 45
						FLOAT fov
						fov = GET_CAM_FOV(ciPassed) - 1
						IF fov <= 45
							fov = 45
						ENDIF
						SET_CAM_FOV(ciPassed, fov)
					ENDIF
				ENDIF
				IF vCamSetRot.x < cfMIN
					vCamSetRot.x += 3
					IF vCamSetRot.x > cfMIN
						vCamSetRot.x = cfMIN + 0.01
					ENDIF
				ELIF vCamSetRot.x > cfMAX + 0.01
					vCamSetRot.x -= 3
					IF vCamSetRot.x < cfMAX
						vCamSetRot.x = cfMAX
					ENDIF
				ELSE
					IF SHOULD_CAMERA_ROTATE(iRy, ciMOVE_THRESHOLD)
						vCamSetRot += <<iRy*(cfMULTIPLYER+fRotAddition), 0.0, 0.0 >>
						IF vCamSetRot.x < cfMIN
							vCamSetRot.x = cfMIN
						ENDIF
						IF vCamSetRot.x > cfMAX
							vCamSetRot.x = cfMAX
						ENDIF	
					ENDIF								
				ENDIF
				
				IF SHOULD_CAMERA_ROTATE(iRx, ciMOVE_THRESHOLD)
					vCamSetRot -= <<0.0, 0.0, iRx*(cfMULTIPLYER+fRotAddition) * 2>>
					SET_CAM_ROT(ciPassed, vCamSetRot)
				ENDIF	
				SET_CAM_ROT(ciPassed, vCamSetRot)
			ENDIF

	ENDIF
ENDPROC

PROC MAINTAIN_CAMERA_ROTATION_LIMITS(FMMC_CAM_DATA &sCamData, eFMMC_MENU_ENUM eFMMCActiveMenu, CREATION_VARS_STRUCT &sCurrentVarsStruct, BOOL &bInitialIntroCamSetup, BOOL &bInitialIntroCamWarp, INT iEntityCreation = -1)
	IF sCurrentVarsStruct.iEntityCreationStatus < STAGE_ENTITY_PLACEMENT_END_MENU
		IF eFMMCActiveMenu = eFmmc_PAN_CAM_BASE
		OR eFMMCActiveMenu = eFmmc_PHOTO_CAM_BASE
		OR eFMMCActiveMenu = eFmmc_OUT_CAM_BASE
		OR eFMMCActiveMenu = eFmmc_SET_ELEVATOR_CAM
		OR eFMMCActiveMenu = efMMC_CUTSCENE_SET_CAM_1
		OR eFMMCActiveMenu = efMMC_CUTSCENE_SET_CAM_2
		OR eFMMCActiveMenu = efMMC_CUTSCENE_SET_CAM_3
		OR eFMMCActiveMenu = efMMC_TRIP_SKIP_CAM
		OR eFMMCActiveMenu = eFmmc_FINISH_CAM_BASE
			IF bInitialIntroCamWarp
				IF IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					AND NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
						VECTOR vCoords = g_FMMC_STRUCT.vStartPos
						
						IF eFMMCActiveMenu = eFmmc_FINISH_CAM_BASE
							IF IS_RACE_P2P()
								vCoords = g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints - 1].vCheckPoint
							ELSE
								vCoords = g_FMMC_STRUCT.sPlacedCheckpoint[0].vCheckPoint
							ENDIF
						ENDIF
						IF IS_POINT_OBSCURED_BY_A_MISSION_ENTITY(vCoords, <<5,5,5>>)
							INT i
							FOR i = 0 TO 26 //Max height in the creator is 1300
								vCoords.z += 50
								IF NOT IS_POINT_OBSCURED_BY_A_MISSION_ENTITY(vCoords, <<5,5,5>>)
									PRINTLN("MAINTAIN_CAMERA_ROTATION_LIMITS - Increasing z as we are colliding with a prop")
									BREAKLOOP
								ENDIF
							ENDFOR
							
						ENDIF
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vCoords, DEFAULT, DEFAULT, DEFAULT, FALSE)
						SET_ENTITY_ROTATION(PLAYER_PED_ID(), <<0,0,0>>)
						PRINTLN("MAINTAIN_CAMERA_ROTATION_LIMITS - SET_ENTITY_COORDS")
					ENDIF
					IF DOES_CAM_EXIST(sCamData.cam)
						sCamData.camRot = GET_CAM_ROT(sCamData.cam)
						sCamData.camRot.x = 0
						SET_CAM_ROT(sCamData.cam, sCamData.camRot)
						PRINTLN("MAINTAIN_CAMERA_ROTATION_LIMITS - SET_CAM_ROT")
					ENDIF
					bInitialIntroCamWarp = FALSE
					bInitialIntroCamSetup = FALSE
				ELSE
					IF NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(500)
						PRINTLN("MAINTAIN_CAMERA_ROTATION_LIMITS - DO_SCREEN_FADE_OUT")
					ENDIF
				ENDIF
			ELSE
				IF bInitialIntroCamSetup AND eFMMCActiveMenu = eFmmc_PAN_CAM_BASE
					IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraPanPos)
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), g_FMMC_STRUCT.vStartPos) > 100
							bInitialIntroCamWarp = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF bInitialIntroCamSetup AND eFMMCActiveMenu = eFmmc_FINISH_CAM_BASE
					IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraFinishPos)
						VECTOR vCoords
						
						IF IS_RACE_P2P()
							vCoords = g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints - 1].vCheckPoint
						ELSE
							vCoords = g_FMMC_STRUCT.sPlacedCheckpoint[0].vCheckPoint
						ENDIF
						
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCoords) > 100
							bInitialIntroCamWarp = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bInitialIntroCamWarp
					IF sCurrentVarsStruct.iMenuState != MENU_STATE_PAN_CAM
						BOOL bFov = (eFMMCActiveMenu = eFmmc_PHOTO_CAM_BASE) OR (eFMMCActiveMenu = eFmmc_FINISH_CAM_BASE)
						
						UPDATE_INTRO_CAM_ROTATION_AND_FOV(sCamData.cam, bInitialIntroCamSetup, bFOV, sCurrentVarsStruct.bResetUpHelp)
						IF sCurrentVarsStruct.iMenuState != MENU_STATE_SWITCH_CAM
						AND sCurrentVarsStruct.iMenuState != MENU_STATE_WARP_TO_INTERIOR
						AND iEntityCreation =  -1
							IF IS_SCREEN_FADED_OUT()
							AND NOT IS_SCREEN_FADING_OUT()
								PRINTLN("MAINTAIN_CAMERA_ROTATION_LIMITS - DO_SCREEN_FADE_IN - ", sCurrentVarsStruct.iMenuState)
								DO_SCREEN_FADE_IN(500)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			
			IF sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
				//PD 2147000 - When resuming form suspend the camera can freeze because the skyfreeze stage is set to SKYFREEZE_FROZEN
				IF GET_SKYFREEZE_STAGE() = SKYFREEZE_FROZEN
					SET_SKYFREEZE_CLEAR(TRUE)
				ENDIF
			ENDIF
			
			CHANGE_SCRIPT_FLY_CAM_ROTATION(sCamData.cam)
			bInitialIntroCamSetup = TRUE
			bInitialIntroCamWarp = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SWAP_CAMERAS(FMMC_CAM_DATA &sCamData, FMMC_LOCAL_STRUCT &sFMMCdata, structFMMC_MENU_ITEMS &sFMMCMenu, CREATION_VARS_STRUCT &sCurrentVarsStruct, BOOL bPlaceOnGround, BOOL BUSeStartPos = FALSE, BOOL bDoFade = FALSE, BOOL bIsRaceType = FALSE)
	
	PRINTLN("SWAP_CAMERAS has been called! bCameraActive: ", GET_STRING_FROM_BOOL(IS_BIT_SET(sFMMCdata.iBitSet, bCameraActive)))
	
	IF IS_BIT_SET(sFMMCdata.iBitSet, bCameraActive)
		IF bDoFade
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
			ENDIF
			IF NOT IS_SCREEN_FADED_OUT()
				RETURN FALSE
			ENDIF
		ENDIF
		CLEAR_BIT(sFMMCdata.iBitSet, bCameraActive)
		SET_BIT(sFMMCdata.iBitSet, bOnTheGround)
		CLEAR_BIT(sFMMCMenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_STACK)
		IF IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
			CLEAR_BIT(sFMMCdata.iBitSet, bResetMenu)
		ENDIF
		
		IF IS_RACE()
			SET_CREATOR_MENU_SECTION(sFMMCMenu, MENU_SECTION_DEFAULT)
		ENDIF
		
		IF DOES_CAM_EXIST(sCamData.cam)
			sCamData.camPos = GET_CAM_COORD(sCamData.cam)
			sCamData.camRot = GET_CAM_ROT(sCamData.cam)
			IF IS_CAM_ACTIVE(sCamData.cam)
				SET_CAM_ACTIVE(sCamData.cam, FALSE)
			ENDIF
			DESTROY_CAM(sCamData.cam, FALSE)
		ENDIF		
		IF DOES_CAM_EXIST(sCamData.inTerp)
			IF IS_CAM_ACTIVE(sCamData.inTerp)
				SET_CAM_ACTIVE(sCamData.inTerp, FALSE)
				DESTROY_CAM(sCamData.inTerp, FALSE)
			ENDIF
			DESTROY_CAM(sCamData.inTerp, FALSE)
		ENDIF	
		IF bDoFade
			RENDER_SCRIPT_CAMS(FALSE, FALSE, 0, TRUE, TRUE)
		ELSE
			RENDER_SCRIPT_CAMS(FALSE, TRUE, 500, TRUE, TRUE)
		ENDIF
		PRINTSTRING("RENDER_SCRIPT_CAMS FALSE")PRINTNL()		
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, TRUE)
			REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
			SET_PED_DIES_IN_SINKING_VEHICLE(PLAYER_PED_ID(), FALSE)
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE)
			SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
			IF bIsRaceType = TRUE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				ENDIF
			ELSE
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			IF bPlaceOnGround
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				AND BUSeStartPos
					SET_ENTITY_COORDS(PLAYER_PED_ID(), g_FMMC_STRUCT.vStartPos, DEFAULT, DEFAULT, DEFAULT, FALSE)
				ELIF NOT IS_VECTOR_ZERO(sCurrentVarsStruct.vCoronaPos)
					IF sCurrentVarsStruct.bCoronaOnWater = TRUE
						SET_ENTITY_COORDS(PLAYER_PED_ID(), sCurrentVarsStruct.vCoronaPos+<<0.0,0.0,0.1>>)
					ELSE
						float fGroundZ
						if  GET_GROUND_Z_FOR_3D_COORD(sCurrentVarsStruct.vCoronaPos + <<0.0,0.0,0.5>>, fGroundZ)
							SET_ENTITY_COORDS(PLAYER_PED_ID(),<<sCurrentVarsStruct.vCoronaPos.x, sCurrentVarsStruct.vCoronaPos.y, fGroundZ>>, DEFAULT, DEFAULT, DEFAULT, FALSE)
						ELSE
							SET_ENTITY_COORDS(PLAYER_PED_ID(), sCurrentVarsStruct.vCoronaPos+<<0.0,0.0,0.1>>, DEFAULT, DEFAULT, DEFAULT, FALSE)
						ENDIF	
					ENDIF
				ELSE
					VECTOR VTemp
					IF GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(PLAYER_PED_ID()), VTemp)
						PRINTLN("SET TO VEHICLE NODE 1")
						SET_ENTITY_COORDS(PLAYER_PED_ID(), VTemp+<<0.0,0.0,0.1>>, DEFAULT, DEFAULT, DEFAULT, FALSE)
					ENDIF
				ENDIF
				sFMMCdata.vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			ENDIF
			g_bInCreatorMapEscape = FALSE
		ENDIF
		
		REFRESH_MENU_ASAP(sFMMCMenu)
		RETURN TRUE
	ELSE	
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, TRUE)
			REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			SET_PED_DIES_IN_SINKING_VEHICLE(PLAYER_PED_ID(), FALSE)
			SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE)	
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) = FALSE
				IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
					DETACH_ENTITY(PLAYER_PED_ID())
				ENDIF
			ENDIF
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
		ENDIF
		IF DOES_CAM_EXIST(sCamData.cam)
			IF NOT IS_CAM_ACTIVE(sCamData.cam)
				SET_CAM_ACTIVE(sCamData.cam, TRUE)
			ENDIF
		ENDIF
		SET_BIT(sFMMCdata.iBitSet, bSetCameraAbovePlacedPropTemplate)
		IF IS_BIT_SET(sFMMCdata.iBitSet, bSetCameraAbovePlacedPropTemplate)
			PRINTLN("[MAINTAIN_CAMERA] - SET_BIT(sFMMCdata.iBitSet, bSetCameraAbovePlacedPropTemplate)")
		ENDIF
		IF DOES_CAM_EXIST(sCamData.inTerp)
			DESTROY_CAM(sCamData.inTerp)
		ENDIF
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10000, TRUE)
		ENDIF
		IF IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
			CLEAR_BIT(sFMMCdata.iBitSet, bResetMenu)
		ENDIF
		CLEAR_BIT(sFMMCdata.iBitSet, bTestHelpTextDone)
		SET_BIT(sFMMCdata.iBitSet, bCameraActive)
		CLEAR_BIT(sFMMCdata.iBitSet, bOnTheGround)
		sCurrentVarsStruct.bSwitching = FALSE
		sCurrentVarsStruct.stiScreenCentreLOS = NULL
		
		g_bInCreatorMapEscape = FALSE
	ENDIF
	
	REFRESH_MENU_ASAP(sFMMCMenu)
	RETURN FALSE
ENDFUNC

/// PURPOSE:Warps the scripted fly cam to a given coordinate and points at a given coordinat, snaps to the minimum height on the Z map
///    NEVER ZOOMS IN
/// PARAMS:
///    vCamGoToCoord 	- The new camera position
///    vPointAtCoord 	- the new camera point at location
///    sCamDataPassed 	- the camera struct passed
///    fHeight 			- The z height of the ground where the camera is going.
/// RETURNS:TRUE WHEN IT@S DONE
FUNC BOOL POINT_SCRIPT_FLY_CAM_AT_COORD(VECTOR vCamGoToCoord, VECTOR vPointAtCoord, FMMC_CAM_DATA &sCamDataPassed, FLOAT fHeight = 0.0)
	IF  NOT IS_VECTOR_ZERO(vCamGoToCoord)
	AND NOT IS_VECTOR_ZERO(vPointAtCoord)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			IF sCamDataPassed.bDonePoint = FALSE
				//Try to get a ground Z
				FLOAT fTempZ
				FLOAT fZtoUse
				VECTOR vCamPos = GET_CAM_COORD(sCamDataPassed.cam)
				IF fHeight != 0.0
					IF GET_GROUND_Z_FOR_3D_COORD(vCamPos, fTempZ)
						PRINTLN("DEAL_WITH_SKY_CAM_SWITCH - GET_GROUND_Z_FOR_3D_COORD = TRUE")
						PRINTLN("DEAL_WITH_SKY_CAM_SWITCH - fTempZ = ", fTempZ)
						//Calculate the height diffrence
						fTempZ = (vCamPos.z - fTempZ)
						PRINTLN("DEAL_WITH_SKY_CAM_SWITCH - fTempZ = ", fTempZ)
						fZtoUse = fHeight + fTempZ
					ELSE
						PRINTLN("DEAL_WITH_SKY_CAM_SWITCH - GET_GROUND_Z_FOR_3D_COORD = FALSE")
						fZtoUse = vCamGoToCoord.z 
					ENDIF
				ELSE
					fZtoUse = vCamGoToCoord.z
				ENDIF
				
				FLOAT fHighestPropSize = 0.0
				FLOAT fZOfNearbyHighestProp = GET_HEIGHT_OF_HIGHEST_PROP_NEAR_VECTOR(<<vCamGoToCoord.x, vCamGoToCoord.y, fZtoUse>>, fHighestPropSize)
				PRINTLN("DEAL_WITH_SKY_CAM_SWITCH - fZOfNearbyHighestProp = ", fZOfNearbyHighestProp)
				IF fZOfNearbyHighestProp >= fZtoUse
					fZtoUse = fZOfNearbyHighestProp + fHighestPropSize + 10.0
				ENDIF
				PRINTLN("DEAL_WITH_SKY_CAM_SWITCH - fZtoUse = ", fZtoUse)
				SET_CAM_COORD(sCamDataPassed.cam, <<vCamGoToCoord.x, vCamGoToCoord.y, fZtoUse>>)
				POINT_CAM_AT_COORD(sCamDataPassed.cam, vPointAtCoord - <<0,0,5>>)
				//DRAW_DEBUG_LINE(GET_CAM_COORD(sCamDataPassed.cam), vPointAtCoord)
				sCamDataPassed.bDonePoint = TRUE
			ELIF sCamDataPassed.bStopLookAt = FALSE
				POINT_CAM_AT_COORD(sCamDataPassed.cam, vPointAtCoord - <<0,0,5>>)
				sCamDataPassed.bStopLookAt = TRUE
			ELSE
				sCamDataPassed.bDonePoint 	= FALSE
				sCamDataPassed.bStopLookAt 	= FALSE
				//DRAW_DEBUG_LINE(GET_CAM_COORD(sCamDataPassed.cam), vPointAtCoord)
				STOP_CAM_POINTING(sCamDataPassed.cam)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
