USING "rage_builtins.sch"
USING "globals.sch"
USING "MP_Globals_FM.sch"
USING "MP_globals_UGC.sch"

USING "net_activity_selector_public.sch"		// For Contact Character Hash conversion
USING "fm_maintain_strand_mission.sch"			// For Strand Mission function calls (ie: Heist Missions with Multiple Parts)
USING "net_transition_sessions_private.sch"

USING "net_prints.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   FM_Mission_Info.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Stores any Freemode-Specific Mission Info functions.
//		NOTES			:	This header is used while the Freemode Mission Data loaded from the cloud is spread over multiple arrays.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************

// ===========================================================================================================
//      FM STANDARD COLOURS
// ===========================================================================================================

// KGM 16/2/14: Standard Freemode Blip, Corona, and Corona Text colours
// NOTE:	Confirmed by written note from Stu 27/3/13
CONST_INT	FM_MINIGAME_BLIP_COLOUR				BLIP_COLOUR_WHITE
CONST_INT	FM_ROCKSTAR_CREATED_BLIP_COLOUR		BLIP_COLOUR_BLUE
CONST_INT	FM_ROCKSTAR_VERIFIED_BLIP_COLOUR	BLIP_COLOUR_BLUE
CONST_INT	FM_MY_UGC_BLIP_COLOUR				BLIP_COLOUR_WHITE
CONST_INT	FM_OTHER_UGC_BLIP_COLOUR			BLIP_COLOUR_YELLOW
CONST_INT	FM_GANG_ATTACK_BLIP_COLOUR			BLIP_COLOUR_RED

// KEITH 28/7/13: DON'T ADD NEW HUD_COLOURS HERE - THEY BREAK THE SP BINARY SIGNATURE
//				  (I'll add them instead to: mp_globals_missions_at_coords_definitions_TU.sch)
HUD_COLOURS	FM_MINIGAME_CORONA_COLOUR			= HUD_COLOUR_WHITE
HUD_COLOURS	FM_ROCKSTAR_CREATED_CORONA_COLOUR	= HUD_COLOUR_NORTH_BLUE
HUD_COLOURS	FM_ROCKSTAR_VERIFIED_CORONA_COLOUR	= HUD_COLOUR_NORTH_BLUE
HUD_COLOURS	FM_MY_UGC_CORONA_COLOUR				= HUD_COLOUR_WHITE
HUD_COLOURS	FM_OTHER_UGC_CORONA_COLOUR			= HUD_COLOUR_WHITE

HUD_COLOURS	FM_MINIGAME_TEXT_COLOUR				= HUD_COLOUR_WHITE
HUD_COLOURS	FM_ROCKSTAR_CREATED_TEXT_COLOUR		= HUD_COLOUR_NORTH_BLUE
HUD_COLOURS	FM_ROCKSTAR_VERIFIED_TEXT_COLOUR	= HUD_COLOUR_WHITE
HUD_COLOURS	FM_MY_UGC_TEXT_COLOUR				= HUD_COLOUR_YELLOW
HUD_COLOURS	FM_OTHER_UGC_TEXT_COLOUR			= HUD_COLOUR_YELLOW




// ===========================================================================================================
//      Mission ID Conversion Functions
//		NOTE: Converts between MP_MISSION (Mission Controller) and FMMC_TYPE IDs (FMMC_Launcher)
// ===========================================================================================================

FUNC BOOL IS_CONTENT_IN_OR_AFTER_DLC(INT iDLC)
	RETURN g_FMMC_STRUCT.iDLCRelease >= iDLC
ENDFUNC

FUNC BOOL IS_CONTENT_AFTER_DLC(INT iDLC)
	RETURN g_FMMC_STRUCT.iDLCRelease > iDLC
ENDFUNC

FUNC BOOL IS_THIS_LEGACY_FMMC_CONTENT()
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_DLC_1_2020)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC STRING GET_FM_MISSION_CONTROLLER_SCRIPT_NAME()

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceUse2020Controller")
		RETURN "fm_mission_controller_2020"
	ENDIF
	#ENDIF
	
	IF IS_THIS_LEGACY_FMMC_CONTENT()
		RETURN "fm_mission_controller"
	ENDIF
	
	RETURN "fm_mission_controller_2020"
	
ENDFUNC

FUNC INT GET_FM_MISSION_CONTROLLER_SCRIPT_NAME_HASH()

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceUse2020Controller")
		RETURN HASH("fm_mission_controller_2020")
	ENDIF
	#ENDIF
	
	IF IS_THIS_LEGACY_FMMC_CONTENT()
		RETURN HASH("fm_mission_controller")
	ENDIF
	
	RETURN HASH("fm_mission_controller_2020")
	
ENDFUNC

FUNC BOOL IS_THIS_LEGACY_DM_CONTENT()
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_DLC_1_2022)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

// PURPOSE:	Get a string from a text label
FUNC STRING Get_String_From_TextLabel(STRING paramTextLabel)
	RETURN(paramTextLabel)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: KGM: Convert an FM Mission Type to a MissionID
//
// INPUT PARAMS:		paramFMMissionType			FM Mission Type
// RETURN VALUE:		MP_MISSION					An MP Mission ID
FUNC MP_MISSION Convert_FM_Mission_Type_To_MissionID(INT paramFMMissionType)

	SWITCH (paramFMMissionType)
		CASE FMMC_TYPE_MG_ARM_WRESTLING				RETURN eFM_ARM_WRESTLING
		CASE FMMC_TYPE_MG_DARTS						RETURN eFM_DARTS
		CASE FMMC_TYPE_MG_PILOT_SCHOOL				RETURN eFM_PILOT_SCHOOL
		CASE FMMC_TYPE_MG_GOLF						RETURN eFM_GOLF
		CASE FMMC_TYPE_MG_SHOOTING_RANGE			RETURN eFM_SHOOTING_RANGE
		CASE FMMC_TYPE_MG_TENNIS					RETURN eFM_TENNIS
		CASE FMMC_TYPE_BASE_JUMP					RETURN eFM_BASEJUMP_CLOUD
		CASE FMMC_TYPE_DEATHMATCH					RETURN eFM_DEATHMATCH_CLOUD
		CASE FMMC_TYPE_IMPROMPTU_DM					RETURN eFM_IMPROMPTU_DM
		CASE FMMC_TYPE_GANGHIDEOUT					RETURN eFM_GANG_ATTACK_CLOUD
		CASE FMMC_TYPE_SURVIVAL						RETURN eFM_SURVIVAL_CLOUD
		CASE FMMC_TYPE_MISSION						RETURN eFM_MISSION_CLOUD
		CASE FMMC_TYPE_RACE							RETURN eFM_RACE_CLOUD
		//CASE FMMC_TYPE_GB_BOSS_CHALLENGES			RETURN eAM_BOSS_CHALLENGES
		CASE FMMC_TYPE_GB_BOSS_DEATHMATCH			RETURN eAM_BOSS_VS_BOSS_DM
		CASE FMMC_TYPE_GB_TERMINATE		 			RETURN eAM_TERMINATE
		CASE FMMC_TYPE_GB_YACHT_ROBBERY				RETURN eAM_YACHT_ROBBERY
		CASE FMMC_TYPE_GB_BELLY_BEAST				RETURN eAM_BELLY_OF_THE_BEAST
		CASE FMMC_TYPE_GB_CHAL_FIVESTAR				RETURN eAM_CHAL_FIVESTAR
		CASE FMMC_TYPE_GB_CHAL_ROB_SHOP				RETURN eAM_CHAL_ROB_SHOP
		CASE FMMC_TYPE_GB_CHAL_COLLECT_MONEY		RETURN eAM_CHAL_COLLECT_MONEY
//		CASE FMMC_TYPE_GB_CHAL_INFIGHTING			RETURN eAM_CHAL_INFIGHTING
		CASE FMMC_TYPE_GB_ASSAULT					RETURN eAM_BOSS_ASSAULT
		CASE FMMC_TYPE_GB_CHAL_P2P					RETURN eAM_BOSS_POINT_TO_POINT
//		CASE FMMC_TYPE_GB_VEHICLE_SURVIVAL			RETURN eAM_BOSS_VEH_SURVIVAL
		CASE FMMC_TYPE_GB_SIGHTSEER					RETURN eAM_BOSS_SIGHTSEER
	//	CASE FMMC_TYPE_GB_FLYING_IN_STYLE			RETURN eAM_BOSS_FLYING_IN_STYLE
	//	CASE FMMC_TYPE_GB_BOSS_DEATHMATCH			RETURN eAM_BOSS_VS_BOSS_DM
		CASE FMMC_TYPE_GB_CHAL_FINDERS_KEEPERS		RETURN eAM_BOSS_FINDERSKEEPERS
		CASE FMMC_TYPE_GB_HUNT_THE_BOSS				RETURN eAM_BOSS_HUNT_THE_BOSS
		CASE FMMC_TYPE_GB_CHAL_CARJACKING			RETURN eAM_CARJACKING
		CASE FMMC_TYPE_GB_HEADHUNTER				RETURN eAM_BOSS_HEADHUNTER
		CASE FMMC_TYPE_GB_CONTRABAND_BUY			RETURN eAM_CONTRABAND_BUY
		CASE FMMC_TYPE_GB_CONTRABAND_SELL			RETURN eAM_CONTRABAND_SELL
		CASE FMMC_TYPE_GB_CONTRABAND_DEFEND			RETURN eAM_CONTRABAND_DEFEND
		CASE FMMC_TYPE_GB_AIRFREIGHT				RETURN eAM_BOSS_AIRFREIGHT
		CASE FMMC_TYPE_GB_CASHING_OUT				RETURN eAM_CHAL_CASHOUT
		CASE FMMC_TYPE_GB_SALVAGE					RETURN eAM_CHAL_SALVAGE
		CASE FMMC_TYPE_GB_FRAGILE_GOODS				RETURN eAM_BOSS_FRAGILE_GOODS
		CASE FMMC_TYPE_VEHICLE_EXPORT_BUY			RETURN eAM_VEHICLE_EXPORT_BUY
		CASE FMMC_TYPE_VEHICLE_EXPORT_SELL			RETURN eAM_VEHICLE_EXPORT_SELL
		CASE FMMC_TYPE_IMPEXP_PLOUGHED				RETURN eAM_IMPEXP_PLOUGHED
		CASE FMMC_TYPE_IMPEXP_FULLY_LOADED			RETURN eAM_IMPEXP_FULLY_LOADED
		CASE FMMC_TYPE_IMPEXP_AMPHIBIOUS_ASSAULT	RETURN eAM_IMPEXP_AMPHIBIOUS_ASSAULT
		CASE FMMC_TYPE_IMPEXP_TRANSPORTER			RETURN eAM_IMPEXP_TRANSPORTER
		CASE FMMC_TYPE_IMPEXP_FORTIFIED				RETURN eAM_IMPEXP_FORTIFIED
		CASE FMMC_TYPE_IMPEXP_VELOCITY				RETURN eAM_IMPEXP_VELOCITY
		CASE FMMC_TYPE_IMPEXP_RAMPED_UP				RETURN eAM_IMPEXP_RAMPED_UP
		CASE FMMC_TYPE_IMPEXP_STOCKPILING			RETURN eAM_IMPEXP_STOCKPILING	
		CASE FMMC_TYPE_BIKER_JOUST					RETURN eAM_BIKER_JOUST
		CASE FMMC_TYPE_BIKER_RACE_P2P				RETURN eAM_BIKER_RACE_P2P
		CASE FMMC_TYPE_BIKER_UNLOAD_WEAPONS			RETURN eAM_BIKER_UNLOAD_WEAPONS
		CASE FMMC_TYPE_BIKER_DEAL_GONE_WRONG		RETURN eAM_BIKER_DEAL_GONE_WRONG
		CASE FMMC_TYPE_BIKER_RESCUE_CONTACT			RETURN eAM_BIKER_RESCUE_CONTACT
		CASE FMMC_TYPE_BIKER_LAST_RESPECTS			RETURN eAM_BIKER_LAST_RESPECTS
		CASE FMMC_TYPE_BIKER_CONTRACT_KILLING		RETURN eAM_BIKER_CONTRACT_KILLING
//		CASE FMMC_TYPE_BIKER_FIGHT_OR_FLIGHT		RETURN eAM_BIKER_FIGHT_OR_FLIGHT
		CASE FMMC_TYPE_BIKER_CONTRABAND_SELL		RETURN eAM_BIKER_SELL
		CASE FMMC_TYPE_BIKER_CONTRABAND_DEFEND		RETURN eAM_BIKER_DEFEND
		CASE FMMC_TYPE_BIKER_BUY					RETURN eAM_BIKER_BUY
		CASE FMMC_TYPE_BIKER_DRIVEBY_ASSASSIN		RETURN eAM_BIKER_DRIVEBY_ASSASSIN
		CASE FMMC_TYPE_BIKER_CRIMINAL_MISCHIEF		RETURN eAM_BIKER_CRIMINAL_MISCHIEF
		CASE FMMC_TYPE_BIKER_RIPPIN_IT_UP			RETURN eAM_BIKER_RIPPIN_IT_UP
		CASE FMMC_TYPE_BIKER_FREE_PRISONER			RETURN eAM_BIKER_FREE_PRISONER
		CASE FMMC_TYPE_BIKER_SAFECRACKER			RETURN eAM_BIKER_SAFECRACKER
		CASE FMMC_TYPE_BIKER_STEAL_BIKES			RETURN eAM_BIKER_STEAL_BIKES
		CASE FMMC_TYPE_BIKER_SEARCH_AND_DESTROY		RETURN eAM_BIKER_SEARCH_AND_DESTROY
		CASE FMMC_TYPE_BIKER_CAGED_IN				RETURN eAM_BIKER_CAGED_IN
		CASE FMMC_TYPE_BIKER_STAND_YOUR_GROUND		RETURN eAM_BIKER_STAND_YOUR_GROUND
		CASE FMMC_TYPE_BIKER_DESTROY_VANS			RETURN eAM_BIKER_DESTROY_VANS		
		CASE FMMC_TYPE_BIKER_BURN_ASSETS			RETURN eAM_BIKER_BURN_ASSETS	
		CASE FMMC_TYPE_BIKER_SHUTTLE				RETURN eAM_BIKER_SHUTTLE		
		CASE FMMC_TYPE_BIKER_WHEELIE_RIDER  		RETURN eAM_BIKER_WHEELIE_RIDER
		CASE FMMC_TYPE_GUNRUNNING_BUY				RETURN eAM_GUNRUNNING_BUY	
		CASE FMMC_TYPE_GUNRUNNING_SELL				RETURN eAM_GUNRUNNING_SELL
		CASE FMMC_TYPE_GUNRUNNING_DEFEND			RETURN eAM_GUNRUNNING_DEFEND
		CASE FMMC_TYPE_SMUGGLER_BUY					RETURN eAM_SMUGGLER_BUY	
		CASE FMMC_TYPE_SMUGGLER_SELL				RETURN eAM_SMUGGLER_SELL
		CASE FMMC_TYPE_FM_GANGOPS					RETURN eAM_FM_GANGOPS
		CASE FMMC_TYPE_FMBB_SELL					RETURN eAM_FMBB_SELL				
		CASE FMMC_TYPE_FMBB_DEFEND					RETURN eAM_FMBB_DEFEND	
		CASE FMMC_TYPE_FMBB_PHONECALL				RETURN eAM_FMBB_PHONECALL
		CASE FMMC_TYPE_FMBB_SECURITY_VAN			RETURN eAM_FMBB_SECURITY_VAN		
		CASE FMMC_TYPE_FMBB_TARGET_PURSUIT			RETURN eAM_FMBB_TARGET_PURSUIT	
		CASE FMMC_TYPE_FMBB_JEWEL_STORE_GRAB		RETURN eAM_FMBB_JEWEL_STORE_GRAB	
		CASE FMMC_TYPE_FMBB_BANK_JOB				RETURN eAM_FMBB_BANK_JOB			
		CASE FMMC_TYPE_FMBB_DATA_HACK				RETURN eAM_FMBB_DATA_HACK		
		CASE FMMC_TYPE_FMBB_INFILTRATION			RETURN eAM_FMBB_INFILTRATION
		CASE FMMC_TYPE_FMBB_CLUB_MANAGEMENT			RETURN eAM_FMBB_CLUB_MANAGEMENT
		CASE FMMC_TYPE_GB_CASINO 					RETURN eAM_GB_CASINO
		#IF FEATURE_CASINO_HEIST
		CASE FMMC_TYPE_GB_CASINO_HEIST				RETURN eAM_GB_CASINO_HEIST
		CASE FMMC_TYPE_MISSION_PLACEHOLDER			RETURN eAM_MISSION_PLACEHOLDER
		#ENDIF
		CASE FMMC_TYPE_SUPER_BUSINESS_BATTLES		RETURN eAM_FMC_BUSINESS_BATTLES
		#IF FEATURE_COPS_N_CROOKS
		CASE FMMC_TYPE_DISPATCH						RETURN eAM_DISPATCH
		#ENDIF
		CASE FMMC_TYPE_DRUG_VEHICLE					RETURN eAM_DRUG_VEHICLE
		CASE FMMC_TYPE_MOVIE_PROPS					RETURN eAM_MOVIE_PROPS
		#IF FEATURE_HEIST_ISLAND
		CASE FMMC_TYPE_ISLAND_HEIST_PREP 			RETURN eAM_ISLAND_HEIST_PREP
		CASE FMMC_TYPE_ISLAND_DJ_MISSION			RETURN eAM_ISLAND_DJ_MISSION
		CASE FMMC_TYPE_GOLDEN_GUN					RETURN eAM_GOLDEN_GUN
		#ENDIF
		#IF FEATURE_TUNER
		CASE FMMC_TYPE_TUNER_ROBBERY 			RETURN eAM_TUNER_ROBBERY
		CASE FMMC_TYPE_VEHICLE_LIST_EVENT 		RETURN eAM_VEHICLE_LIST_EVENT
		CASE FMMC_TYPE_SANDBOX_ACTIVITY			RETURN eAM_SANDBOX_ACTIVITY
		CASE FMMC_TYPE_TUNER_CLIENT_DELIVERY	RETURN eAM_TUNER_CLIENT_DELIVERY
		#ENDIF
		#IF FEATURE_FIXER	
		CASE FMMC_TYPE_FIXER_PAYPHONE		RETURN eAM_FIXER_PAYPHONE			
		CASE FMMC_TYPE_FIXER_SECURITY		RETURN eAM_FIXER_SECURITY	    		
		CASE FMMC_TYPE_FIXER_VIP			RETURN eAM_FIXER_VIP	    		
		CASE FMMC_TYPE_METAL_DETECTOR		RETURN eAM_METAL_DETECTOR
		#ENDIF
		#IF FEATURE_HALLOWEEN_2021
		CASE FMMC_TYPE_PHANTOM_CAR			RETURN eAM_PHANTOM_CAR
		CASE FMMC_TYPE_SLASHER				RETURN eAM_SLASHER
		CASE FMMC_TYPE_SIGHTSEEING			RETURN eAM_SIGHTSEEING
		#ENDIF
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE FMMC_TYPE_HSW_TIME_TRIAL		RETURN eAM_HSW_TIME_TRIAL
		CASE FMMC_TYPE_GEN9_MP_INTRO		RETURN eAM_GEN9_MP_INTRO
		CASE FMMC_TYPE_HSW_SETUP			RETURN eAM_HSW_SETUP
		#ENDIF
		#IF FEATURE_DLC_1_2022
		CASE FMMC_TYPE_SMUGGLER_TRAIL		RETURN eAM_SMUGGLER_TRAIL
		CASE FMMC_TYPE_SKYDIVING_CHALLENGE	RETURN eAM_SKYDIVING_CHALLENGE
		CASE FMMC_TYPE_SMUGGLER_PLANE		RETURN eAM_SMUGGLER_PLANE
		CASE FMMC_TYPE_CERBERUS				RETURN eAM_CERBERUS
		CASE FMMC_TYPE_PARACHUTER			RETURN eAM_PARACHUTER
		CASE FMMC_TYPE_CRIME_SCENE			RETURN eAM_CRIME_SCENE
		CASE FMMC_TYPE_BAR_RESUPPLY				RETURN eAM_BAR_RESUPPLY
		CASE FMMC_TYPE_BIKE_SHOP_DELIVERY		RETURN eAM_BIKE_SHOP_DELIVERY
		CASE FMMC_TYPE_CLUBHOUSE_CONTRACTS		RETURN eAM_CLUBHOUSE_CONTRACTS
		CASE FMMC_TYPE_SPECIAL_CARGO			RETURN eAM_SPECIAL_CARGO
		CASE FMMC_TYPE_EXPORT_CARGO				RETURN eAM_EXPORT_CARGO
		CASE FMMC_TYPE_GUNRUNNING_AMMUNATION	RETURN eAM_GUNRUNNING_AMMUNATION
		CASE FMMC_TYPE_GUNRUNNING_RESUPPLY		RETURN eAM_GUNRUNNING_RESUPPLY
		CASE FMMC_TYPE_SOURCE_RESEARCH			RETURN eAM_SOURCE_RESEARCH
		CASE FMMC_TYPE_CLUB_MANAGEMENT			RETURN eAM_CLUB_MANAGEMENT
		CASE FMMC_TYPE_CLUB_ODD_JOBS			RETURN eAM_CLUB_ODD_JOBS
		CASE FMMC_TYPE_CLUB_SOURCE				RETURN eAM_CLUB_SOURCE
		#ENDIF
		#IF FEATURE_DLC_2_2022
		CASE FMMC_TYPE_GANG_CONVOY			RETURN eAM_GANG_CONVOY
		CASE FMMC_TYPE_ROBBERY				RETURN eAM_ROBBERY
		CASE FMMC_TYPE_ACID_LAB_SETUP		RETURN eAM_ACID_LAB_SETUP
		CASE FMMC_TYPE_ACID_LAB_SOURCE		RETURN eAM_ACID_LAB_SOURCE
		CASE FMMC_TYPE_ACID_LAB_SELL		RETURN eAM_ACID_LAB_SELL
		CASE FMMC_TYPE_DRUG_LAB_WORK		RETURN eAM_DRUG_LAB_WORK
		CASE FMMC_TYPE_STASH_HOUSE			RETURN eAM_STASH_HOUSE
		CASE FMMC_TYPE_TAXI_DRIVER			RETURN eAM_TAXI_DRIVER
		CASE FMMC_TYPE_XMAS_MUGGER			RETURN eAM_XMAS_MUGGER
		CASE FMMC_TYPE_BANK_SHOOTOUT		RETURN eAM_BANK_SHOOTOUT
		#ENDIF
		#IF IS_DEBUG_BUILD
		CASE FMMC_TYPE_FMC_TEST				RETURN eAM_FMC_TEST
		#ENDIF
	ENDSWITCH

	// Need to add new Details for this activity
	NET_PRINT("...KGM MP [MissionInfo]: Convert_FM_Mission_Type_To_MissionID - ERROR: Need to add mission ID conversion details for this FM Mission Type (as an Int): ")
	NET_PRINT_INT(paramFMMissionType)
	NET_NL()
	SCRIPT_ASSERT("Convert_FM_Mission_Type_To_MissionID(): ERROR - Need to add mission ID conversion details for FM Activity. See Console Log. Tell Keith.")
	
	RETURN eNULL_MISSION

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To convert an MP_MISSION to its FM Mission Type equivalent
//
// INPUT PARAMS:			paramMissionID		The missionID
// RETURN VALUE:			INT					The FM Mission Type
FUNC INT Convert_MissionID_To_FM_Mission_Type(MP_MISSION paramMissionID, BOOL bDoAssert = TRUE)

	SWITCH (paramMissionID)
		CASE eFM_ARM_WRESTLING		RETURN FMMC_TYPE_MG_ARM_WRESTLING
		CASE eFM_BASEJUMP_CLOUD		RETURN FMMC_TYPE_BASE_JUMP
		CASE eFM_DARTS				RETURN FMMC_TYPE_MG_DARTS
		CASE eFM_PILOT_SCHOOL		RETURN FMMC_TYPE_MG_PILOT_SCHOOL
		CASE eFM_DEATHMATCH_CLOUD	RETURN FMMC_TYPE_DEATHMATCH
		CASE eFM_IMPROMPTU_DM		RETURN FMMC_TYPE_IMPROMPTU_DM
		CASE eFM_GANG_ATTACK_CLOUD	RETURN FMMC_TYPE_GANGHIDEOUT
		CASE eFM_GOLF				RETURN FMMC_TYPE_MG_GOLF
		CASE eFM_MISSION_CLOUD		RETURN FMMC_TYPE_MISSION
		CASE eFM_RACE_CLOUD			RETURN FMMC_TYPE_RACE
		CASE eFM_SHOOTING_RANGE		RETURN FMMC_TYPE_MG_SHOOTING_RANGE
		CASE eFM_SURVIVAL_CLOUD		RETURN FMMC_TYPE_SURVIVAL
		CASE eFM_TENNIS				RETURN FMMC_TYPE_MG_TENNIS
		CASE eAM_BOSS_VS_BOSS_DM		RETURN FMMC_TYPE_GB_BOSS_DEATHMATCH		
		CASE eAM_TERMINATE				RETURN FMMC_TYPE_GB_TERMINATE		 		
		CASE eAM_YACHT_ROBBERY			RETURN FMMC_TYPE_GB_YACHT_ROBBERY			
		CASE eAM_BELLY_OF_THE_BEAST		RETURN FMMC_TYPE_GB_BELLY_BEAST			
		CASE eAM_CHAL_FIVESTAR			RETURN FMMC_TYPE_GB_CHAL_FIVESTAR			
		CASE eAM_CHAL_ROB_SHOP			RETURN FMMC_TYPE_GB_CHAL_ROB_SHOP			
		CASE eAM_CHAL_COLLECT_MONEY		RETURN FMMC_TYPE_GB_CHAL_COLLECT_MONEY		
		CASE eAM_BOSS_ASSAULT			RETURN FMMC_TYPE_GB_ASSAULT				
		CASE eAM_BOSS_POINT_TO_POINT	RETURN FMMC_TYPE_GB_CHAL_P2P						
		CASE eAM_BOSS_SIGHTSEER			RETURN FMMC_TYPE_GB_SIGHTSEER					
		CASE eAM_BOSS_FINDERSKEEPERS	RETURN FMMC_TYPE_GB_CHAL_FINDERS_KEEPERS	
		CASE eAM_BOSS_HUNT_THE_BOSS		RETURN FMMC_TYPE_GB_HUNT_THE_BOSS			
		CASE eAM_CARJACKING				RETURN FMMC_TYPE_GB_CHAL_CARJACKING		
		CASE eAM_BOSS_HEADHUNTER		RETURN FMMC_TYPE_GB_HEADHUNTER
		CASE eAM_CONTRABAND_BUY			RETURN FMMC_TYPE_GB_CONTRABAND_BUY	
		CASE eAM_CONTRABAND_SELL		RETURN FMMC_TYPE_GB_CONTRABAND_SELL	
		CASE eAM_CONTRABAND_DEFEND		RETURN FMMC_TYPE_GB_CONTRABAND_DEFEND
		CASE eAM_BOSS_AIRFREIGHT		RETURN FMMC_TYPE_GB_AIRFREIGHT	
		CASE eAM_CHAL_CASHOUT			RETURN FMMC_TYPE_GB_CASHING_OUT	
		CASE eAM_CHAL_SALVAGE			RETURN FMMC_TYPE_GB_SALVAGE		
		CASE eAM_BOSS_FRAGILE_GOODS		RETURN FMMC_TYPE_GB_FRAGILE_GOODS
		CASE eAM_VEHICLE_EXPORT_BUY			RETURN FMMC_TYPE_VEHICLE_EXPORT_BUY
		CASE eAM_VEHICLE_EXPORT_SELL		RETURN FMMC_TYPE_VEHICLE_EXPORT_SELL
		CASE eAM_IMPEXP_PLOUGHED			RETURN FMMC_TYPE_IMPEXP_PLOUGHED			
		CASE eAM_IMPEXP_FULLY_LOADED        RETURN FMMC_TYPE_IMPEXP_FULLY_LOADED		
		CASE eAM_IMPEXP_AMPHIBIOUS_ASSAULT  RETURN FMMC_TYPE_IMPEXP_AMPHIBIOUS_ASSAULT	
		CASE eAM_IMPEXP_TRANSPORTER         RETURN FMMC_TYPE_IMPEXP_TRANSPORTER			
		CASE eAM_IMPEXP_FORTIFIED           RETURN FMMC_TYPE_IMPEXP_FORTIFIED			
		CASE eAM_IMPEXP_VELOCITY            RETURN FMMC_TYPE_IMPEXP_VELOCITY			
		CASE eAM_IMPEXP_RAMPED_UP           RETURN FMMC_TYPE_IMPEXP_RAMPED_UP			
		CASE eAM_IMPEXP_STOCKPILING	        RETURN FMMC_TYPE_IMPEXP_STOCKPILING			

		CASE eAM_BIKER_JOUST			RETURN FMMC_TYPE_BIKER_JOUST		
		CASE eAM_BIKER_RACE_P2P			RETURN FMMC_TYPE_BIKER_RACE_P2P		
		CASE eAM_BIKER_UNLOAD_WEAPONS	RETURN FMMC_TYPE_BIKER_UNLOAD_WEAPONS			
		CASE eAM_BIKER_DEAL_GONE_WRONG	RETURN FMMC_TYPE_BIKER_DEAL_GONE_WRONG
		CASE eAM_BIKER_RESCUE_CONTACT	RETURN FMMC_TYPE_BIKER_RESCUE_CONTACT	
		CASE eAM_BIKER_LAST_RESPECTS	RETURN FMMC_TYPE_BIKER_LAST_RESPECTS
		CASE eAM_BIKER_CONTRACT_KILLING	RETURN FMMC_TYPE_BIKER_CONTRACT_KILLING
//		CASE eAM_BIKER_FIGHT_OR_FLIGHT	RETURN FMMC_TYPE_BIKER_FIGHT_OR_FLIGHT
		CASE eAM_BIKER_SELL				RETURN FMMC_TYPE_BIKER_CONTRABAND_SELL		
		CASE eAM_BIKER_DEFEND			RETURN FMMC_TYPE_BIKER_CONTRABAND_DEFEND
		CASE eAM_BIKER_BUY				RETURN FMMC_TYPE_BIKER_BUY
		CASE eAM_BIKER_DRIVEBY_ASSASSIN	RETURN FMMC_TYPE_BIKER_DRIVEBY_ASSASSIN
		CASE eAM_BIKER_CRIMINAL_MISCHIEF RETURN FMMC_TYPE_BIKER_CRIMINAL_MISCHIEF
		CASE eAM_BIKER_RIPPIN_IT_UP		RETURN FMMC_TYPE_BIKER_RIPPIN_IT_UP
		CASE eAM_BIKER_FREE_PRISONER 	RETURN FMMC_TYPE_BIKER_FREE_PRISONER
		CASE eAM_BIKER_SAFECRACKER 		RETURN FMMC_TYPE_BIKER_SAFECRACKER
		CASE eAM_BIKER_SEARCH_AND_DESTROY RETURN FMMC_TYPE_BIKER_SEARCH_AND_DESTROY
		CASE eAM_BIKER_CAGED_IN			RETURN FMMC_TYPE_BIKER_CAGED_IN
		CASE eAM_BIKER_STAND_YOUR_GROUND	RETURN FMMC_TYPE_BIKER_STAND_YOUR_GROUND
		CASE eAM_BIKER_DESTROY_VANS		RETURN FMMC_TYPE_BIKER_DESTROY_VANS			
		CASE eAM_BIKER_BURN_ASSETS		RETURN FMMC_TYPE_BIKER_BURN_ASSETS			
		CASE eAM_BIKER_SHUTTLE			RETURN FMMC_TYPE_BIKER_SHUTTLE				
		CASE eAM_BIKER_WHEELIE_RIDER	RETURN FMMC_TYPE_BIKER_WHEELIE_RIDER		
		CASE eAM_BIKER_STEAL_BIKES		RETURN FMMC_TYPE_BIKER_STEAL_BIKES
		CASE eAM_GUNRUNNING_BUY			RETURN FMMC_TYPE_GUNRUNNING_BUY
		CASE eAM_GUNRUNNING_SELL		RETURN FMMC_TYPE_GUNRUNNING_SELL
		CASE eAM_GUNRUNNING_DEFEND		RETURN FMMC_TYPE_GUNRUNNING_DEFEND
		CASE eAM_SMUGGLER_BUY			RETURN FMMC_TYPE_SMUGGLER_BUY
		CASE eAM_SMUGGLER_SELL			RETURN FMMC_TYPE_SMUGGLER_SELL
		CASE eAM_FM_GANGOPS				RETURN FMMC_TYPE_FM_GANGOPS
		CASE eAM_FMBB_SELL				RETURN FMMC_TYPE_FMBB_SELL				
		CASE eAM_FMBB_DEFEND			RETURN FMMC_TYPE_FMBB_DEFEND			
		CASE eAM_FMBB_SECURITY_VAN		RETURN FMMC_TYPE_FMBB_SECURITY_VAN		
		CASE eAM_FMBB_TARGET_PURSUIT	RETURN FMMC_TYPE_FMBB_TARGET_PURSUIT	
		CASE eAM_FMBB_JEWEL_STORE_GRAB	RETURN FMMC_TYPE_FMBB_JEWEL_STORE_GRAB	
		CASE eAM_FMBB_BANK_JOB			RETURN FMMC_TYPE_FMBB_BANK_JOB			
		CASE eAM_FMBB_DATA_HACK			RETURN FMMC_TYPE_FMBB_DATA_HACK			
		CASE eAM_FMBB_INFILTRATION		RETURN FMMC_TYPE_FMBB_INFILTRATION
		CASE eAM_FMBB_PHONECALL			RETURN FMMC_TYPE_FMBB_PHONECALL
		CASE eAM_FMBB_CLUB_MANAGEMENT	RETURN FMMC_TYPE_FMBB_CLUB_MANAGEMENT	
		CASE eAM_GB_CASINO				RETURN FMMC_TYPE_GB_CASINO
		#IF FEATURE_CASINO_HEIST
		CASE eAM_GB_CASINO_HEIST		RETURN FMMC_TYPE_GB_CASINO_HEIST
		CASE eAM_MISSION_PLACEHOLDER	RETURN FMMC_TYPE_MISSION_PLACEHOLDER
		#ENDIF
		#IF FEATURE_COPS_N_CROOKS
		CASE eAM_SERVE_AND_PROTECT		RETURN FMMC_TYPE_SERVE_AND_PROTECT
		CASE eAM_DISPATCH 				RETURN FMMC_TYPE_DISPATCH
		#ENDIF
		CASE eAM_FMC_BUSINESS_BATTLES	RETURN FMMC_TYPE_SUPER_BUSINESS_BATTLES
		CASE eAM_DRUG_VEHICLE			RETURN FMMC_TYPE_DRUG_VEHICLE
		CASE eAM_MOVIE_PROPS			RETURN FMMC_TYPE_MOVIE_PROPS
		#IF FEATURE_HEIST_ISLAND
		CASE eAM_ISLAND_HEIST_PREP		RETURN FMMC_TYPE_ISLAND_HEIST_PREP
		CASE eAM_ISLAND_DJ_MISSION		RETURN FMMC_TYPE_ISLAND_DJ_MISSION
		CASE eAM_GOLDEN_GUN				RETURN FMMC_TYPE_GOLDEN_GUN
		#ENDIF
		#IF FEATURE_TUNER
		CASE eAM_TUNER_ROBBERY			RETURN FMMC_TYPE_TUNER_ROBBERY
		CASE eAM_VEHICLE_LIST_EVENT		RETURN FMMC_TYPE_VEHICLE_LIST_EVENT
		CASE eAM_SANDBOX_ACTIVITY		RETURN FMMC_TYPE_SANDBOX_ACTIVITY
		CASE eAM_TUNER_CLIENT_DELIVERY	RETURN FMMC_TYPE_TUNER_CLIENT_DELIVERY
		#ENDIF
		#IF FEATURE_FIXER
		CASE eAM_FIXER_PAYPHONE			RETURN FMMC_TYPE_FIXER_PAYPHONE	
		CASE eAM_FIXER_SECURITY	        RETURN FMMC_TYPE_FIXER_SECURITY	
		CASE eAM_FIXER_VIP	        	RETURN FMMC_TYPE_FIXER_VIP
		CASE eAM_METAL_DETECTOR			RETURN FMMC_TYPE_METAL_DETECTOR
		#ENDIF
		#IF FEATURE_HALLOWEEN_2021
		CASE eAM_PHANTOM_CAR			RETURN FMMC_TYPE_PHANTOM_CAR
		CASE eAM_SLASHER				RETURN FMMC_TYPE_SLASHER
		CASE eAM_SIGHTSEEING			RETURN FMMC_TYPE_SIGHTSEEING
		#ENDIF
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE eAM_HSW_TIME_TRIAL			RETURN FMMC_TYPE_HSW_TIME_TRIAL
		CASE eAM_GEN9_MP_INTRO			RETURN FMMC_TYPE_GEN9_MP_INTRO
		CASE eAM_HSW_SETUP				RETURN FMMC_TYPE_HSW_SETUP
		#ENDIF
		#IF FEATURE_DLC_1_2022
		CASE eAM_SMUGGLER_TRAIL			RETURN FMMC_TYPE_SMUGGLER_TRAIL
		CASE eAM_SKYDIVING_CHALLENGE	RETURN FMMC_TYPE_SKYDIVING_CHALLENGE
		CASE eAM_SMUGGLER_PLANE			RETURN FMMC_TYPE_SMUGGLER_PLANE
		CASE eAM_CRIME_SCENE	 		RETURN FMMC_TYPE_CRIME_SCENE
		CASE eAM_CERBERUS				RETURN FMMC_TYPE_CERBERUS
		CASE eAM_BAR_RESUPPLY			RETURN FMMC_TYPE_BAR_RESUPPLY
		CASE eAM_BIKE_SHOP_DELIVERY		RETURN FMMC_TYPE_BIKE_SHOP_DELIVERY
		CASE eAM_CLUBHOUSE_CONTRACTS	RETURN FMMC_TYPE_CLUBHOUSE_CONTRACTS
		CASE eAM_SPECIAL_CARGO			RETURN FMMC_TYPE_SPECIAL_CARGO
		CASE eAM_EXPORT_CARGO			RETURN FMMC_TYPE_EXPORT_CARGO
		CASE eAM_GUNRUNNING_AMMUNATION	RETURN FMMC_TYPE_GUNRUNNING_AMMUNATION
		CASE eAM_GUNRUNNING_RESUPPLY	RETURN FMMC_TYPE_GUNRUNNING_RESUPPLY
		CASE eAM_SOURCE_RESEARCH		RETURN FMMC_TYPE_SOURCE_RESEARCH
		CASE eAM_CLUB_MANAGEMENT		RETURN FMMC_TYPE_CLUB_MANAGEMENT
		CASE eAM_CLUB_ODD_JOBS			RETURN FMMC_TYPE_CLUB_ODD_JOBS
		CASE eAM_CLUB_SOURCE			RETURN FMMC_TYPE_CLUB_SOURCE
		#ENDIF
		#IF FEATURE_DLC_2_2022
		CASE eAM_GANG_CONVOY			RETURN FMMC_TYPE_GANG_CONVOY
		CASE eAM_ROBBERY				RETURN FMMC_TYPE_ROBBERY
		CASE eAM_ACID_LAB_SETUP			RETURN FMMC_TYPE_ACID_LAB_SETUP
		CASE eAM_ACID_LAB_SOURCE		RETURN FMMC_TYPE_ACID_LAB_SOURCE
		CASE eAM_ACID_LAB_SELL			RETURN FMMC_TYPE_ACID_LAB_SELL
		CASE eAM_DRUG_LAB_WORK			RETURN FMMC_TYPE_DRUG_LAB_WORK
		CASE eAM_STASH_HOUSE			RETURN FMMC_TYPE_STASH_HOUSE
		CASE eAM_TAXI_DRIVER			RETURN FMMC_TYPE_TAXI_DRIVER
		CASE eAM_XMAS_MUGGER			RETURN FMMC_TYPE_XMAS_MUGGER
		CASE eAM_BANK_SHOOTOUT			RETURN FMMC_TYPE_BANK_SHOOTOUT
		#ENDIF
		#IF IS_DEBUG_BUILD
		CASE eAM_FMC_TEST				RETURN FMMC_TYPE_FMC_TEST
		#ENDIF
	ENDSWITCH

	// Need to add new Details for this activity
	IF bDoAssert
		NET_PRINT("...KGM MP [MissionInfo]: Convert_MissionID_To_FM_Mission_Type - ERROR: Need to add FM Mission Type conversion details for this Mission ID (as an INT): ")
		NET_PRINT_INT(ENUM_TO_INT(paramMissionID))
		NET_NL()
		SCRIPT_ASSERT("Convert_MissionID_To_FM_Mission_Type(): ERROR - Need to add FM Mission Type conversion details for MP MISSION ID. See Console Log. Tell Keith.")
	ENDIF
	
	RETURN (FMMC_TYPE_MAX)

ENDFUNC




// ===========================================================================================================
//      Generated Cloud Header Data Functions
//		(cross-transition cloud header data gathered from the full mission data loaded for other player's UGC)
// ===========================================================================================================

#IF IS_DEBUG_BUILD
// INPUT PARAMS:			paramOnHoursBitfield		Bitfield where the first 24 bits represent the 24 hours of the day
// RETURN VALUE:			TEXT_LABEL_63				A string description of the 'on' hours
FUNC TEXT_LABEL_63 Convert_Hours_From_Bitfield_To_TL(INT paramOnHoursBitfield)

	TEXT_LABEL_63 theReturnTimes = ""

	// All Day?
	IF ((paramOnHoursBitfield & MATC_ALL_DAY) = MATC_ALL_DAY)
		theReturnTimes = "All Day"
		RETURN (theReturnTimes)
	ENDIF
	
	// Not Open?
	IF ((paramOnHoursBitfield & MATC_ALL_DAY) = 0)
		theReturnTimes = "No On Hours Set"
		RETURN (theReturnTimes)
	ENDIF
	
	// Build up a string describing the On Hours
	CONST_INT	HOURS_IN_A_DAY		24
	
	INT		tempLoop		= 0
	
	REPEAT HOURS_IN_A_DAY tempLoop
		IF (IS_BIT_SET(paramOnHoursBitfield, tempLoop))
			theReturnTimes += "1"
		ELSE
			theReturnTimes += "0"
		ENDIF
	ENDREPEAT
	
	RETURN (theReturnTimes)

ENDFUNC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
// PURPOSE:	Output the full details of the Generated Header Data struct
PROC Debug_Output_Generated_Header_Data()

	TEXT_LABEL_63 theHoursDescription = Convert_Hours_From_Bitfield_To_TL(g_sGeneratedCloudHeaderData.gchdHeaderData.iLaunchTimesBit)
	
	NET_PRINT("      Cloud Filename  : ")	NET_PRINT(g_sGeneratedCloudHeaderData.gchdHeaderData.tlName)					NET_NL()
	NET_PRINT("      Owner           : ")	NET_PRINT(g_sGeneratedCloudHeaderData.gchdHeaderData.tlOwner )					NET_NL()
	NET_PRINT("      Mission Name    : ")	NET_PRINT(g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionName)				NET_NL()
	NET_PRINT("      Mission Desc    : ")	NET_PRINT(g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionDec)				NET_NL()
	NET_PRINT("      Start Pos       : ")	NET_PRINT_VECTOR(g_sGeneratedCloudHeaderData.gchdHeaderData.vStartPos)			NET_NL()
	NET_PRINT("      Camera Pos      : ")	NET_PRINT_VECTOR(g_sGeneratedCloudHeaderData.gchdHeaderData.vCamPos)			NET_NL()
	NET_PRINT("      Camera Heading  : ")	NET_PRINT_VECTOR(g_sGeneratedCloudHeaderData.gchdHeaderData.vCamHead)			NET_NL()
	NET_PRINT("      FM Type         : ")	NET_PRINT_INT(g_sGeneratedCloudHeaderData.gchdHeaderData.iType)					NET_NL()
	NET_PRINT("      FM Subtype      : ")	NET_PRINT_INT(g_sGeneratedCloudHeaderData.gchdHeaderData.iSubtype)				NET_NL()
	NET_PRINT("      Min Players     : ")	NET_PRINT_INT(g_sGeneratedCloudHeaderData.gchdHeaderData.iMinPlayers)			NET_NL()
	NET_PRINT("      Rank            : ")	NET_PRINT_INT(g_sGeneratedCloudHeaderData.gchdHeaderData.iRank)					NET_NL()
	NET_PRINT("      Max Players     : ")	NET_PRINT_INT(g_sGeneratedCloudHeaderData.gchdHeaderData.iMaxPlayers)			NET_NL()
	NET_PRINT("      Rating          : ")	NET_PRINT_INT(g_sGeneratedCloudHeaderData.gchdHeaderData.iRating)				NET_NL()
	NET_PRINT("      Les Rating      : ")	NET_PRINT_INT(g_sGeneratedCloudHeaderData.gchdHeaderData.iLesRating)			NET_NL()
	NET_PRINT("      Contact (as INT): ")	NET_PRINT_INT(g_sGeneratedCloudHeaderData.gchdHeaderData.iContactCharEnum)		NET_NL()
	NET_PRINT("      Bitset          : ")	NET_PRINT_INT(g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet)				NET_NL()
	NET_PRINT("      On Hours        : ")	NET_PRINT(theHoursDescription)													NET_NL()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Generated Cloud Header Data struct
PROC Clear_Generated_Cloud_Header_Data()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Clear the Generated Cloud Header Data struct") NET_NL()
	#ENDIF
	
	g_structGeneratedCloudHeaderData emptyGeneratedCloudHeaderData
	g_sGeneratedCloudHeaderData = emptyGeneratedCloudHeaderData
	
	g_sGeneratedCloudHeaderData.gchdInUse							= FALSE
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlName				= ""
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlOwner				= ""
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionName		= ""
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionDec			= ""
	g_sGeneratedCloudHeaderData.gchdHeaderData.vStartPos			= << 0.0, 0.0, 0.0 >>
	g_sGeneratedCloudHeaderData.gchdHeaderData.vCamPos				= << 0.0, 0.0, 0.0 >>
	g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet				= ALL_MISSIONS_SHARED_BITS_CLEAR
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Create header data for the mission from the full mission data
//
// INPUT PARAMS:		paramCloudFilename			The cloud filename passed with the transition
//
// NOTES:	This is needed because the header data for another player's UGC is not available during a cross-session transition
PROC Generate_Cloud_Header_Data_From_Full_Mission_Data(TEXT_LABEL_23 paramCloudFilename)

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [MissionInfo]: Generate Cloud Header Data from the full mission data for Cloud Filename: ", paramCloudFilename)
	#ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramCloudFilename))
		paramCloudFilename = g_FMMC_STRUCT.tl31LoadedContentID
		
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [MissionInfo]: Passed-in Filename is NULL or EMPTY, so replacing with Loaded Filename: ", paramCloudFilename)
		#ENDIF
	ENDIF
	
	// Races subtype comes from a different field
	INT theSubtype = g_FMMC_STRUCT.iMissionSubType
	IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE)
		theSubtype = g_FMMC_STRUCT.iRaceType
	ENDIF
	
	// On Hours may need swapped to be 'all hours'
	INT onHours = g_FMMC_STRUCT.iLaunchTimesBit
	IF (g_FMMC_STRUCT.iLaunchTimesBit = 0)
		onHours = MATC_ALL_DAY
	ENDIF
	
	// If the Player is on an activity session and this is a heist-related mission, then there must have been a contentID change
	// We need to move the heist coords to the correct apartment
	VECTOR coronaCoords = g_FMMC_STRUCT.vStartPos
	IF (NETWORK_IS_ACTIVITY_SESSION())
		IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION)
			IF (theSubtype = FMMC_MISSION_TYPE_HEIST)
			OR (theSubtype = FMMC_MISSION_TYPE_PLANNING)
				// This is a Heist-related mission that hasn't been found in the rockstar-created array during an activity session,
				//		so assuming a contentID change and moving the startPos to the Player's Pos.
				coronaCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [MissionInfo][Heist]: Generating Heist-related Mission. Assuming ContentID change. Updating Corona coords from: ", g_FMMC_STRUCT.vStartPos, " to ", coronaCoords)
				#ENDIF
				
				IF (HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
					PRINTLN(".KGM [MissionInfo][Heist]: ...this is a Part Two mission, so also updating the mission restart coords")
					SET_STRAND_MISSION_STARTING_VECTOR(coronaCoords)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		// FEATURE_HEIST_PLANNING
	
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlName				= paramCloudFilename
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlOwner				= g_FMMC_STRUCT.tl63MissionCreator
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionName		= g_FMMC_STRUCT.tl63MissionName
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionDec			= g_FMMC_STRUCT.tl63MissionDecription[0]
	g_sGeneratedCloudHeaderData.gchdHeaderData.vStartPos			= coronaCoords
	g_sGeneratedCloudHeaderData.gchdHeaderData.vCamPos				= g_FMMC_STRUCT.vCameraPanPos
	g_sGeneratedCloudHeaderData.gchdHeaderData.vCamHead				= g_FMMC_STRUCT.vCameraPanRot
	g_sGeneratedCloudHeaderData.gchdHeaderData.iType				= g_FMMC_STRUCT.iMissionType
	g_sGeneratedCloudHeaderData.gchdHeaderData.iSubType				= theSubtype
	g_sGeneratedCloudHeaderData.gchdHeaderData.iMinPlayers			= g_FMMC_STRUCT.iMinNumParticipants
	g_sGeneratedCloudHeaderData.gchdHeaderData.iRank				= g_FMMC_STRUCT.iRank
	g_sGeneratedCloudHeaderData.gchdHeaderData.iMaxPlayers			= g_FMMC_STRUCT.iNumParticipants
	g_sGeneratedCloudHeaderData.gchdHeaderData.iRating				= -1
	g_sGeneratedCloudHeaderData.gchdHeaderData.iLesRating			= -1
	g_sGeneratedCloudHeaderData.gchdHeaderData.iContactCharEnum		= g_FMMC_STRUCT.iContactCharEnum
	g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet				= ALL_MISSION_INFO_BITS_CLEAR
	g_sGeneratedCloudHeaderData.gchdHeaderData.iLaunchTimesBit		= onHours
	g_sGeneratedCloudHeaderData.gchdInUse							= TRUE
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Generated_Header_Data()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Create header data for the mission from the full mission data
//
// INPUT PARAMS:		paramCloudFilename			The cloud filename passed with the transition
//
// NOTES:	This is needed because the header data for another player's UGC is not available during a cross-session transition
PROC Generate_Cloud_Header_Data_For_PauseMenu_Mission(CLOUD_LOADED_MISSION_HEADER_DETAILS &sHeaderVarsPassed)

	g_sGeneratedCloudHeaderData.gchdHeaderData.tlName				= sHeaderVarsPassed.tlName
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlOwner				= sHeaderVarsPassed.tlOwner
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionName		= sHeaderVarsPassed.tlMissionName
//	g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionDec			= sHeaderVarsPassed.tlMissionDec
	g_sGeneratedCloudHeaderData.gchdHeaderData.iMissionDecHash		= sHeaderVarsPassed.iMissionDecHash
	g_sGeneratedCloudHeaderData.gchdHeaderData.vStartPos			= sHeaderVarsPassed.vStartPos
	g_sGeneratedCloudHeaderData.gchdHeaderData.vCamPos				= sHeaderVarsPassed.vCamPos
	g_sGeneratedCloudHeaderData.gchdHeaderData.vCamHead				= sHeaderVarsPassed.vCamHead
	g_sGeneratedCloudHeaderData.gchdHeaderData.iType				= sHeaderVarsPassed.iType
	g_sGeneratedCloudHeaderData.gchdHeaderData.iSubType				= sHeaderVarsPassed.iSubType
	g_sGeneratedCloudHeaderData.gchdHeaderData.iMinPlayers			= sHeaderVarsPassed.iMinPlayers
	g_sGeneratedCloudHeaderData.gchdHeaderData.iRank				= sHeaderVarsPassed.iRank
	g_sGeneratedCloudHeaderData.gchdHeaderData.iMaxPlayers			= sHeaderVarsPassed.iMaxPlayers
	g_sGeneratedCloudHeaderData.gchdHeaderData.iRating				= sHeaderVarsPassed.iRating
	g_sGeneratedCloudHeaderData.gchdHeaderData.iContactCharEnum		= sHeaderVarsPassed.iContactCharEnum
	g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet				= sHeaderVarsPassed.iBitSet
	g_sGeneratedCloudHeaderData.gchdHeaderData.iLaunchTimesBit		= sHeaderVarsPassed.iLaunchTimesBit
	g_sGeneratedCloudHeaderData.gchdInUse							= TRUE
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Generated_Header_Data()
	#ENDIF

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Create header data for the copy of the player's last registered Shared Mission Data
// NOTES:	This is needed because the header data for another player's UGC may not be available if the other player doesn't play the mission
PROC Generate_Cloud_Header_Data_From_Copy_Shared_Mission_Data()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Generate Cloud Header Data from the copy of the player's last registered Shared Mission Data") NET_NL()
		NET_PRINT("      Last Registered Shared Mission has this filename: ") NET_PRINT(g_sCopyRegSharedMissionData.crsmdHeaderData.tlName) NET_NL()
	#ENDIF
	
	g_sGeneratedCloudHeaderData.gchdHeaderData						= g_sCopyRegSharedMissionData.crsmdHeaderData
	g_sGeneratedCloudHeaderData.gchdInUse							= TRUE
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Generated_Header_Data()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Copy header data from the current Playlist Mission Header Data
//
// REFERENCE PARAMS:		CLOUD_LOADED_MISSION_HEADER_DETAILS			Playlist Mission Header data
//
// NOTES:	This is needed because the playlist may be using an old version of the mission
PROC Generate_Cloud_Header_Data_From_Current_Playlist_Mission_Header_Data(CLOUD_LOADED_MISSION_HEADER_DETAILS &refPlaylistMissionHeader)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Generate Cloud Header Data from the current playlist mission's header data") NET_NL()
	#ENDIF
	
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlName				= refPlaylistMissionHeader.tlName
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlOwner				= refPlaylistMissionHeader.tlOwner
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionName		= refPlaylistMissionHeader.tlMissionName
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionDec			= refPlaylistMissionHeader.tlMissionName
	g_sGeneratedCloudHeaderData.gchdHeaderData.vStartPos			= refPlaylistMissionHeader.vStartPos
	g_sGeneratedCloudHeaderData.gchdHeaderData.vCamPos				= refPlaylistMissionHeader.vCamPos
	g_sGeneratedCloudHeaderData.gchdHeaderData.vCamHead				= refPlaylistMissionHeader.vCamHead
	g_sGeneratedCloudHeaderData.gchdHeaderData.iType				= refPlaylistMissionHeader.iType
	g_sGeneratedCloudHeaderData.gchdHeaderData.iSubType				= refPlaylistMissionHeader.iSubType
	g_sGeneratedCloudHeaderData.gchdHeaderData.iMinPlayers			= refPlaylistMissionHeader.iMinPlayers
	g_sGeneratedCloudHeaderData.gchdHeaderData.iRank				= refPlaylistMissionHeader.iRank
	g_sGeneratedCloudHeaderData.gchdHeaderData.iMaxPlayers			= refPlaylistMissionHeader.iMaxPlayers
	g_sGeneratedCloudHeaderData.gchdHeaderData.iRating				= refPlaylistMissionHeader.iRating
	g_sGeneratedCloudHeaderData.gchdHeaderData.iLesRating			= refPlaylistMissionHeader.iLesRating
	g_sGeneratedCloudHeaderData.gchdHeaderData.iContactCharEnum		= refPlaylistMissionHeader.iContactCharEnum
	g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet				= refPlaylistMissionHeader.iBitSet
	g_sGeneratedCloudHeaderData.gchdHeaderData.iLaunchTimesBit		= refPlaylistMissionHeader.iLaunchTimesBit
	g_sGeneratedCloudHeaderData.gchdInUse							= TRUE
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Generated_Header_Data()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gather the MissionId Data from the Generated Cloud Header Data
//
// REFERENCE PARAMS:		refMissionIdData			The MissionID Data
// RETURN VALUE:			BOOL						TRUE if the missionID Data is being returned, otherwise FALSE
FUNC BOOL Retrieve_MissionIdData_From_Generated_Cloud_Header_Data(MP_MISSION_ID_DATA &refMissionIdData)

	IF NOT (g_sGeneratedCloudHeaderData.gchdInUse)
		RETURN FALSE
	ENDIF
	
	// Gather the data - store as a fake variationID of the player's UGC
	// NOTE: this is a workaround that may prove to be nasty - but the functions above should interpret this before checking the player's UGC
	//		 The proper fix is possibly to create a new CreatorID for generated content
	refMissionIdData.idMission			= Convert_FM_Mission_Type_To_MissionID(g_sGeneratedCloudHeaderData.gchdHeaderData.iType)
	refMissionIdData.idVariation		= GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION
	refMissionIdData.idCreator			= NATIVE_TO_INT(PLAYER_ID())
	refMissionIdData.idCloudFilename	= g_sGeneratedCloudHeaderData.gchdHeaderData.tlName
	refMissionIdData.idSharedRegID		= ILLEGAL_SHARED_REG_ID
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this data refers to the Generated Header Data
//
// INPUT PARAMS:			paramVariation				The Variation
// RETURN VALUE:			BOOL						TRUE if the MissionId Data refers to the generated cloud header data, otherwise FALSE
FUNC BOOL Is_This_Generated_Cloud_Header_Data(INT paramVariation)
	RETURN (paramVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the passed in mission ID data matches the current generated cloud header data
//
// REFERENCE PARAMS:		refMissionIdData			The MissionID Data
// RETURN VALUE:			BOOL						TRUE if the MissionId Data matches the generated cloud header data, otherwise FALSE
FUNC BOOL Does_MissionId_Data_Match_Generated_Cloud_Header_Data(MP_MISSION_ID_DATA &refMissionIdData)

	// ERROR CHECKING - This should only get called if the MissionIdData refers to Generated Header Data
	#IF IS_DEBUG_BUILD
		IF NOT (Is_This_Generated_Cloud_Header_Data(refMissionIdData.idVariation))
			SCRIPT_ASSERT("Does_MissionId_Data_Match_Generated_Cloud_Header_Data() - Data passed in does not refer to Generated Cloud Header Data. Tell Keith")
			RETURN FALSE
		ENDIF
	#ENDIF

	// Does it match?
	IF (g_sGeneratedCloudHeaderData.gchdInUse)
		IF (ARE_STRINGS_EQUAL(g_sGeneratedCloudHeaderData.gchdHeaderData.tlName, refMissionIdData.idCloudFilename))
			// ...the generated cloud header data is in use and the filename matches
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: This is generated data but the filenames don't match. [Generated = ")
		NET_PRINT(g_sGeneratedCloudHeaderData.gchdHeaderData.tlName)
		NET_PRINT("]  [MissionID Data = ")
		NET_PRINT(refMissionIdData.idCloudFilename)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	RETURN FALSE
		
ENDFUNC





// ===========================================================================================================
//      Data Gathering Functions
// ===========================================================================================================

// PURPOSE:	Checks if the data is stored as a shared activity
//
// RETURN PARAMS:		paramMissionIdData			MissionID Data
// RETURN VALUE:		BOOL						TRUE if the shared mission data is in use and correct, FALSE if not in use or an error
//
// NOTES:	I should use access functions instead of using the Shared Missions Globals directly, but I can't at the moment because of cyclic header issues - needs a slight re-structure
FUNC BOOL Is_Cloud_Data_Stored_As_A_Shared_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT sharedArrayPos = paramMissionIdData.idSharedRegID

	// Is there a registered shared array position?
	IF (sharedArrayPos = ILLEGAL_SHARED_REG_ID)
		RETURN FALSE
	ENDIF

	// There is a registered shared array position, so check if the data in use
	IF NOT (GlobalServerBD_MissionsShared.cloudDetails[sharedArrayPos].mscInUse)
		#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_cloudsafespam"))
				NET_PRINT("...KGM MP [MissionInfo]: Is_Cloud_Data_Stored_As_A_Shared_Activity(): The array position passed as parameter is not in use: ") NET_PRINT_INT(sharedArrayPos) NET_NL()
				DEBUG_PRINTCALLSTACK()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// There is valid data in the registered shared array position, so ensure it is for the expected mission
	IF NOT (ARE_STRINGS_EQUAL(GlobalServerBD_MissionsShared.cloudDetails[sharedArrayPos].mscHeaderData.tlName, paramMissionIdData.idCloudFilename))
		#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_cloudsafespam"))
				NET_PRINT("...KGM MP [MissionInfo]: Is_Cloud_Data_Stored_As_A_Shared_Activity(): The array position passed as parameter has a different mission: ") NET_PRINT_INT(sharedArrayPos) NET_NL()
				NET_PRINT("          Filename In Array: ") NET_PRINT(GlobalServerBD_MissionsShared.cloudDetails[sharedArrayPos].mscHeaderData.tlName) NET_NL()
				NET_PRINT("          Expected Filename: ") NET_PRINT(paramMissionIdData.idCloudFilename) NET_NL()
				DEBUG_PRINTCALLSTACK()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Has the shared array received all the mission data, or is it still in the process of retrieving the data
	IF NOT (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[sharedArrayPos].mscBitflags, MSCLOUD_BITFLAG_ALL_DATA_RECEIVED))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Is_Cloud_Data_Stored_As_A_Shared_Activity(): The array position passed as parameter is still gathering data : ") NET_PRINT_INT(sharedArrayPos) NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// Everything ok - so use the shared missions data
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the data is stored as a shared activity
//
// RETURN PARAMS:		paramMissionIdData			MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:		INT							The array position of the cloud-loaded header data, of ILLEGAL_ARRAY_POSITION if not found
FUNC INT Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(MP_MISSION_ID_DATA &paramMissionIdData)

	// Check Filename consistency
	IF (IS_STRING_NULL_OR_EMPTY(paramMissionIdData.idCloudFilename))
		#IF IS_DEBUG_BUILD
			IF (paramMissionIdData.idCreator != FMMC_MINI_GAME_CREATOR_ID)
				NET_PRINT("...KGM MP [MissionInfo]: Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): The passed in filename is empty") NET_NL()
				SCRIPT_ASSERT("Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): The passed in filename is empty. Tell Keith")
			ENDIF
		#ENDIF
		
		RETURN ILLEGAL_ARRAY_POSITION
	ENDIF
	
	// Check for Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		// ...generated header data
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			// ...the generated cloud header data is in use and the filename matches, so return the fake variation as the array pos
			RETURN GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION
		ENDIF
		
		// The generated cloud header data doesn't match
		RETURN ILLEGAL_ARRAY_POSITION
	ENDIF

	INT tempLoop = 0

	SWITCH (paramMissionIdData.idCreator)
		// Rockstar Created Data
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// Is the missionID Data still valid?
			IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
				IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramMissionIdData.idVariation].tlName, paramMissionIdData.idCloudFilename))
					RETURN (paramMissionIdData.idVariation)
				ENDIF
			ENDIF
			
			// No longer valid
			#IF IS_DEBUG_BUILD
				IF (GET_COMMANDLINE_PARAM_EXISTS("sc_cloudsafespam"))
					NET_PRINT("...KGM MP [MissionInfo]: Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): The passed in variation doesn't point to the correct Created data. Starting long-winded search.") NET_NL()
					IF NOT (g_sAtCoordsControlsMP.matccRefreshInProgress)
						DEBUG_PRINTCALLSTACK()
					ENDIF
				ENDIF
			#ENDIF
			
			// If a cloud refresh is in progress and the array position is no longer in use then don't do a long-winded search - the array may have been cleared prior to getting refreshed
			IF NOT (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
				IF (g_sAtCoordsControlsMP.matccRefreshInProgress)
					#IF IS_DEBUG_BUILD
						IF (GET_COMMANDLINE_PARAM_EXISTS("sc_cloudsafespam"))
							NET_PRINT("      Refresh in progress and mission's array position is 'not in use' - so array may have been cleared and getting refilled - don't do long-winded search.")
							NET_NL()
						ENDIF
					#ENDIF
					
					RETURN (ILLEGAL_ARRAY_POSITION)
				ENDIF
			ENDIF
		
			// Do a long-winded search to find the new array position of the data
			REPEAT FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED tempLoop
				IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
					IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].tlName, paramMissionIdData.idCloudFilename))
						#IF IS_DEBUG_BUILD
							IF (GET_COMMANDLINE_PARAM_EXISTS("sc_cloudsafespam"))
								NET_PRINT("      Found the data at Rockstar Created array position: ")
								NET_PRINT_INT(tempLoop)
								NET_PRINT(" - UPDATING THE PASSED IN VARIATION")
								NET_NL()
							ENDIF
						#ENDIF
						
						// Update the passed in variation to match the new array position
						paramMissionIdData.idVariation = tempLoop
						
						RETURN (tempLoop)
					ENDIF
				ENDIF
			ENDREPEAT
			
			// Failed to find the data
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MissionInfo]: Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): Failed to find the filename on the Rockstar Created array: ")
				NET_PRINT(paramMissionIdData.idCloudFilename)
				IF (g_sAtCoordsControlsMP.matccRefreshInProgress)
					NET_PRINT("  - NOTE: Cloud refresh in progress")
				ENDIF
				NET_NL()
// KGM 9/11/14: No longer asserting - bugs that cause the assert are most likely ignorable. If I find any that aren't I'll re-instatte the assert.
//				IF NOT (g_sAtCoordsControlsMP.matccRefreshInProgress)
//					SCRIPT_ASSERT("Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): Failed to find the filename in the Rockstar Created array. Tell Keith")
//				ENDIF
			#ENDIF
			
			RETURN ILLEGAL_ARRAY_POSITION
		
		// Rockstar Verified Data
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// Is the missionID Data still valid?
			IF (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
				IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramMissionIdData.idVariation].tlName, paramMissionIdData.idCloudFilename))
					RETURN (paramMissionIdData.idVariation)
				ENDIF
			ENDIF
			
			// No longer valid
			#IF IS_DEBUG_BUILD
				IF (GET_COMMANDLINE_PARAM_EXISTS("sc_cloudsafespam"))
					NET_PRINT("...KGM MP [MissionInfo]: Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): The passed in variation doesn't point to the correct Verified data. Starting long-winded search.") NET_NL()
					IF NOT (g_sAtCoordsControlsMP.matccRefreshInProgress)
						DEBUG_PRINTCALLSTACK()
					ENDIF
				ENDIF
			#ENDIF
		
			// If a cloud refresh is in progress and the array position is no longer in use then don't do a long-winded search - the array may have been cleared prior to getting refreshed
			IF NOT (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
				IF (g_sAtCoordsControlsMP.matccRefreshInProgress)
					#IF IS_DEBUG_BUILD
						IF (GET_COMMANDLINE_PARAM_EXISTS("sc_cloudsafespam"))
							NET_PRINT("      Refresh in progress and mission's array position is 'not in use' - so array may have been cleared and getting refilled - don't do long-winded search.")
							NET_NL()
						ENDIF
					#ENDIF
					
					RETURN (ILLEGAL_ARRAY_POSITION)
				ENDIF
			ENDIF
		
			// Do a long-winded search to find the new array position of the data
			REPEAT FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED tempLoop
				IF (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
					IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[tempLoop].tlName, paramMissionIdData.idCloudFilename))
						#IF IS_DEBUG_BUILD
							IF (GET_COMMANDLINE_PARAM_EXISTS("sc_cloudsafespam"))
								NET_PRINT("      Found the data at Rockstar Verified array position: ")
								NET_PRINT_INT(tempLoop)
								NET_PRINT(" - UPDATING THE PASSED IN VARIATION")
								NET_NL()
							ENDIF
						#ENDIF
						
						// Update the passed in variation to match the new array position
						paramMissionIdData.idVariation = tempLoop
						
						RETURN (tempLoop)
					ENDIF
				ENDIF
			ENDREPEAT
			
			// Failed to find the data
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MissionInfo]: Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): Failed to find the filename on the Rockstar Verified array: ")
				NET_PRINT(paramMissionIdData.idCloudFilename)
				IF (g_sAtCoordsControlsMP.matccRefreshInProgress)
					NET_PRINT("  - NOTE: Cloud refresh in progress")
				ENDIF
				NET_NL()
				IF NOT (g_sAtCoordsControlsMP.matccRefreshInProgress)
					SCRIPT_ASSERT("Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): Failed to find the filename in the Rockstar Verified array. Tell Keith")
				ENDIF
			#ENDIF
			
			RETURN ILLEGAL_ARRAY_POSITION
			
		// MiniGame Data - all hardcoded, so just return the same variation
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (paramMissionIdData.idVariation)
			
		// UGC and unrecognised data
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				// Is the missionID Data still valid?
				IF (IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
					IF (ARE_STRINGS_EQUAL(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[paramMissionIdData.idVariation].tlName, paramMissionIdData.idCloudFilename))
						RETURN (paramMissionIdData.idVariation)
					ENDIF
				ENDIF
				
				// No longer valid
				#IF IS_DEBUG_BUILD
					IF (GET_COMMANDLINE_PARAM_EXISTS("sc_cloudsafespam"))
						NET_PRINT("...KGM MP [MissionInfo]: Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): The passed in variation doesn't point to the correct Player UGC data. Starting long-winded search.") NET_NL()
						IF NOT (g_sAtCoordsControlsMP.matccRefreshInProgress)
							DEBUG_PRINTCALLSTACK()
						ENDIF
					ENDIF
				#ENDIF
			
				// If a cloud refresh is in progress and the array position is no longer in use then don't do a long-winded search - the array may have been cleared prior to getting refreshed
				IF (IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
					IF (g_sAtCoordsControlsMP.matccRefreshInProgress)
						#IF IS_DEBUG_BUILD
							IF (GET_COMMANDLINE_PARAM_EXISTS("sc_cloudsafespam"))
								NET_PRINT("      Refresh in progress and mission's array position is 'not in use' - so array may have been cleared and getting refilled - don't do long-winded search.")
								NET_NL()
							ENDIF
						#ENDIF
						
						RETURN (ILLEGAL_ARRAY_POSITION)
					ENDIF
				ENDIF
			
				// Do a long-winded search to find the new array position of the data
				REPEAT MAX_NUMBER_UGC_AND_BOOKMARKED tempLoop
					IF (IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
						IF (ARE_STRINGS_EQUAL(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[tempLoop].tlName, paramMissionIdData.idCloudFilename))
							#IF IS_DEBUG_BUILD
								IF (GET_COMMANDLINE_PARAM_EXISTS("sc_cloudsafespam"))
									NET_PRINT("      Found the data at Player's UGC and Bookmarked array position: ")
									NET_PRINT_INT(tempLoop)
									NET_PRINT(" - UPDATING THE PASSED IN VARIATION")
									NET_NL()
								ENDIF
							#ENDIF
							
							// Update the passed in variation to match the new array position
							paramMissionIdData.idVariation = tempLoop
							
							RETURN (tempLoop)
						ENDIF
					ENDIF
				ENDREPEAT
				
				// Failed to find the data
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): Failed to find the filename on the Player's UGC and Bookmarked array: ")
					NET_PRINT(paramMissionIdData.idCloudFilename)
					IF (g_sAtCoordsControlsMP.matccRefreshInProgress)
						NET_PRINT("  - NOTE: Cloud refresh in progress")
					ENDIF
					NET_NL()
					IF NOT (g_sAtCoordsControlsMP.matccRefreshInProgress)
						SCRIPT_ASSERT("Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): Failed to find the filename in the Player's UGC and Bookmarked array. Tell Keith")
					ENDIF
				#ENDIF
				
				RETURN ILLEGAL_ARRAY_POSITION
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Another player's UGC - so there will be no data on a local array if the data isn't already shared
				RETURN ILLEGAL_ARRAY_POSITION
			ENDIF
			BREAK
	ENDSWITCH
	
	// Failed to find the creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): Unknown creatorID: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		SCRIPT_ASSERT("Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(): Unknown CreatorID. Tell Keith")
	#ENDIF
	
	RETURN ILLEGAL_ARRAY_POSITION
	
ENDFUNC



// ===========================================================================================================
//      Freemode-Specific versions of net_mission_info functions
//		(Gathers the data from the appropriate global array)
// ===========================================================================================================

// PURPOSE:	Check if the cloud details passed as parameter are locally stored on this machine
//
// REFERENCE PARAMS:		refMissionIdData		The MissionID Data of the mission being searched for
// RETURN VALUE:			BOOL					TRUE if the cloud header details are stored locally, FALSE if not
//
// NOTES:	IF the details are stored but found in a different array position, then the 'variation' parameter of the return struct will be updated to the correct array position
FUNC BOOL Are_These_FM_Cloud_Loaded_Activity_Details_Available_Locally(MP_MISSION_ID_DATA &refMissionIdData)

	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(refMissionIdData)
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN FALSE
	ENDIF
	
	// Found the details (the variation parameter should already have been updated if the position is different)
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this data is still in same Shared Array position, or is still Generated Header data, or in same place on Global cloud array
//
// REFERENCE PARAMS:		refMissionIdData		The MissionID Data of the mission being searched for
// RETURN VALUE:			BOOL					TRUE if the cloud header details are stored locally, FALSE if not
//
// NOTES:	This was added to prevent Mission At Coords searching the arrays for debug output after the mission had been deleted
FUNC BOOL Is_This_Cloud_Loaded_Data_In_Correct_ArrayPos(MP_MISSION_ID_DATA &refMissionIdData)

	// Shared Mission data?
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(refMissionIdData))
		RETURN TRUE
	ENDIF
	
	// Check for Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(refMissionIdData.idVariation))
		// ...generated header data
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(refMissionIdData))
			// ...the generated cloud header data is in use and the filename matches, so return the fake variation as the array pos
			RETURN TRUE
		ENDIF
		
		// The generated header data doesn't match, but need to quit the function to avoid falling through to the function below with a fake missionVariation that causes an array overrun
		RETURN FALSE
	ENDIF

	// Check for in the same place in the global arrays
	SWITCH (refMissionIdData.idCreator)
		// Rockstar Created Data
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// Is the missionID Data still valid?
			IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[refMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
				IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[refMissionIdData.idVariation].tlName, refMissionIdData.idCloudFilename))
					RETURN TRUE
				ENDIF
			ENDIF
			RETURN FALSE
		
		// Rockstar Verified Data
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// Is the missionID Data still valid?
			IF (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[refMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
				IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[refMissionIdData.idVariation].tlName, refMissionIdData.idCloudFilename))
					RETURN TRUE
				ENDIF
			ENDIF
			RETURN FALSE
			
		// MiniGame Data - all hardcoded
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN TRUE
			
		// UGC and unrecognised data
		DEFAULT
			IF (refMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				// Is the missionID Data still valid?
				IF (IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[refMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
					IF (ARE_STRINGS_EQUAL(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[refMissionIdData.idVariation].tlName, refMissionIdData.idCloudFilename))
						RETURN TRUE
					ENDIF
				ENDIF
				RETURN FALSE
				
			ELIF (refMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Another player's UGC - so there will be no data on a local array if the data isn't already shared
				RETURN FALSE
			ENDIF
			BREAK
	ENDSWITCH
	
	// Failed to find the creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_This_Cloud_Loaded_Data_In_Correct_ArrayPos(): Unknown creatorID: ")
		NET_PRINT_INT(refMissionIdData.idCreator)
		NET_NL()
		SCRIPT_ASSERT("Is_This_Cloud_Loaded_Data_In_Correct_ArrayPos(): Unknown CreatorID. Tell Keith")
	#ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the FM Mission Type required for the specified activity.
//
// INPUT PARAMS:			paramCreatorID			The Creator ID (which indicates which cloud global array to access)
//							paramVariation			The Variation (which is the array position on the cloud global array)
// RETURN VALUE:			INT						The mission type for this mission.
//
// NOTES:	The Variation is used by Cloud-Loaded missions - the header data gets stored on a global array and the variation provides the array position of the data.
//			This does no consistency checking because it is used to check if the array position is in use.
FUNC BOOL Is_FM_Cloud_Loaded_Array_Position_In_Use(INT paramCreatorID, INT paramVariation)

	// Check for Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramVariation))
		// ...generated header data
		RETURN (g_sGeneratedCloudHeaderData.gchdInUse)
	ENDIF
	
	// Check for Loaded Cloud Header Data
	SWITCH (paramCreatorID)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
			
		DEFAULT
			IF (paramCreatorID = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[paramVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
			ELIF (paramCreatorID < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Array_Position_In_Use(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramCreatorID)))
					NET_PRINT("] - RETURNING FALSE")
					NET_NL()
				#ENDIF
				
				RETURN (FALSE)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Array_Position_In_Use() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramCreatorID)
		NET_NL()
		
		SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Array_Position_In_Use: ERROR - Unknown Creator ID. See Console Log. Returning 0. Tell Keith")
	#ENDIF
	
	RETURN (FALSE)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the FM Mission Type required for the specified activity.
//
// INPUT PARAMS:			paramCreatorID			The Creator ID (which indicates which cloud global array to access)
//							paramVariation			The Variation (which is the array position on the cloud global array)
// RETURN VALUE:			INT						The mission type for this mission.
//
// NOTES:	The Variation is used by Cloud-Loaded missions - the header data gets stored on a global array and the variation provides the array position of the data.
//			This does no consistency checking because it is used to retrieve the MissionID used for consistency checking.
FUNC INT Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity(INT paramCreatorID, INT paramVariation)

	// Check for Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramVariation))
		IF (g_sGeneratedCloudHeaderData.gchdInUse)
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.iType)
		ENDIF
		
		// Error - Generated Cloud Header Data not in use
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity() ERROR - Generated Cloud Header Data is not in use.")
			
			SCRIPT_ASSERT("Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity: ERROR - Generated Cloud Header Data is not in use. Tell Keith")
		#ENDIF
		
		RETURN (FMMC_TYPE_MAX)
	ENDIF
	
	// Return the details from the correct global cloud data array based on the CreatorID and Variation passed to the function
	// This was originally added for eFM_RACE_CLOUD
	SWITCH (paramCreatorID)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramVariation].iType)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramVariation].iType)
			
		DEFAULT
			IF (paramCreatorID = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[paramVariation].iType)
			ELIF (paramCreatorID < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramCreatorID)))
					NET_PRINT("] - RETURNING FMMC_TYPE_MAX")
					NET_NL()
				#ENDIF
				
				RETURN (FMMC_TYPE_MAX)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramCreatorID)
		NET_NL()
		
		SCRIPT_ASSERT("Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 0. Tell Keith")
	#ENDIF
	
	RETURN (FMMC_TYPE_MAX)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the FileName as stored in the cloud for the specified activity.
//
// INPUT PARAMS:			paramCreatorID			The Creator ID (which indicates which cloud global array to access)
//							paramVariation			The Variation (which is the array position on the cloud global array)
// RETURN VALUE:			TEXT_LABEL_23			The FileName stored for this mission.
//
// NOTES:	This function should only be called once when the data is guaranteed to be InSync just after being loaded - the filename should then get passed around
FUNC TEXT_LABEL_23 Get_FileName_For_FM_Cloud_Loaded_Activity(INT paramCreatorID, INT paramVariation)

	// Assumes this function gets called for a mission as the mission has just been loaded, so all the details are InSync
	// Normally the filename is used to verify the data, but this can't be done when we are actually grabbing the filename in the first place to act as the verification data

	TEXT_LABEL_23 fileNameToReturn = "UNKNOWN"

	// Check for Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramVariation))
		IF (g_sGeneratedCloudHeaderData.gchdInUse)
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.tlName)
		ENDIF
		
		// Error - Generated Cloud Header Data not in use
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_FileName_For_FM_Cloud_Loaded_Activity() ERROR - Generated Cloud Header Data is not in use.")
			
			SCRIPT_ASSERT("Get_FileName_For_FM_Cloud_Loaded_Activity: ERROR - Generated Cloud Header Data is not in use. Tell Keith")
		#ENDIF
		
		RETURN (fileNameToReturn)
	ENDIF
	
	// Return the details from the correct global cloud data array based on the CreatorID and Variation passed to the function
	// NOTE: This is partly a copy of the switch above, but the switch above is intended to be temporary for additional debug
	// This was originally added for eFM_RACE_CLOUD
	SWITCH (paramCreatorID)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			fileNameToReturn = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramVariation].tlName
			RETURN (fileNameToReturn)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			fileNameToReturn = g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramVariation].tlName
			RETURN (fileNameToReturn)
			
		DEFAULT
			IF (paramCreatorID = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[paramVariation].tlName)
			ELIF (paramCreatorID < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_FileName_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramCreatorID)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT(fileNameToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (fileNameToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_FileName_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramCreatorID)
		NET_NL()
		NET_PRINT("          Passed in Variation for Cloud Global Array to use: ")
		NET_PRINT_INT(paramVariation)
		NET_NL()
		
		SCRIPT_ASSERT("Get_FileName_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Tell Keith")
	#ENDIF
	
	RETURN (fileNameToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the minimum players required for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT						The minimum number of players required for this mission.
FUNC INT Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT minPlayers = 1

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iMinPlayers)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.iMinPlayers)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (minPlayers)
	ENDIF
	
	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (minPlayers)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iMinPlayers)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iMinPlayers)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iMinPlayers)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_INT(minPlayers)
					NET_NL()
				#ENDIF
				
				RETURN (minPlayers)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 1. Tell Keith")
	#ENDIF
	
	RETURN (minPlayers)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the maximum players required for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT						The maximum number of players required for this mission.
FUNC INT Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT maxPlayers = NUM_NETWORK_PLAYERS

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iMaxPlayers)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.iMaxPlayers)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (maxPlayers)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (maxPlayers)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iMaxPlayers)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iMaxPlayers)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iMaxPlayers)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_INT(maxPlayers)
					NET_NL()
				#ENDIF
				
				RETURN (maxPlayers)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning MaxPlayers. Tell Keith")
	#ENDIF
	
	RETURN (maxPlayers)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the User Name as stored in the cloud for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			TEXT_LABEL_23			The FileName stored for this mission.
FUNC TEXT_LABEL_63 Get_User_Name_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	TEXT_LABEL_63 userNameToReturn = "[UNKNOWN]"

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		userNameToReturn = GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.tlOwner
		RETURN (userNameToReturn)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			userNameToReturn = g_sGeneratedCloudHeaderData.gchdHeaderData.tlOwner
			RETURN (userNameToReturn)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_User_Name_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (userNameToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (userNameToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].tlOwner)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			userNameToReturn = g_FMMC_ROCKSTAR_VERIFIED.tlOwnerPretty[arrayPos]
			RETURN userNameToReturn //(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].tlOwner)
			
		// MiniGames don't have a User Name
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (userNameToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				#IF IS_DEBUG_BUILD
					IF (IS_STRING_NULL_OR_EMPTY(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].tlOwner))
						RETURN (userNameToReturn)
					ENDIF
				#ENDIF
				
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].tlOwner)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_User_Name_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT(userNameToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (userNameToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_User_Name_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_User_Name_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'UNKNOWN'. Tell Keith")
	#ENDIF
	
	RETURN (userNameToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Mission Name as stored in the cloud for the specified activity.
//
// INPUT PARAMS:			paramCheckLocalOnly		[DEFAULT = FALSE] TRUE means check local data only, FALSE means check all data including Shared and Generated
// RETURN PARAMS:			paramMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			TEXT_LABEL_23			The Mission Name stored for this mission.
FUNC TEXT_LABEL_63 Get_Mission_Name_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData, BOOL paramCheckLocalOnly = FALSE)
	
	TEXT_LABEL_63 missionNameToReturn = "UNKNOWN"
	
	IF (paramCheckLocalOnly)
		missionNameToReturn = ""
	ENDIF

	IF NOT (paramCheckLocalOnly)
		// Check if this cloud-loaded header data is on the shared missions cloud data array
		IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
			// If the local player has processed the Shared Mission data, then return the local version of the Mission Name (this prevents returning a version in a different language)
			IF (IS_BIT_SET(g_MSCloudClientControl.mscccdMissions[paramMissionIdData.idSharedRegID].msccdBitfield, MSCCLIENT_BITFLAG_MISSION_PROCESSED))
				// ...local player has processed the Shared Mission, so return the local copy of the Mission Name
				RETURN (g_MSCLocalLanguageText.msclldtMissionName[paramMissionIdData.idSharedRegID])
			ENDIF
			
			RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.tlMissionName)
		ENDIF
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionName)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Name_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (missionNameToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (missionNameToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].tlMissionName)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].tlMissionName)
			
		// MiniGames don't have a Mission Name
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (missionNameToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].tlMissionName)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Name_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT(missionNameToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (missionNameToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Name_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Mission_Name_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'UNKNOWN'. Tell Keith")
	#ENDIF
	
	RETURN (missionNameToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Mission Description as stored in the cloud for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			TEXT_LABEL_23			The Mission Description stored for this mission.
FUNC TEXT_LABEL_63 Get_Mission_Description_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)
	
	TEXT_LABEL_63 missionDescriptionToReturn = "NO DESCRIPTION"

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.tlMissionDec)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionDec)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Description_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (missionDescriptionToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (missionDescriptionToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].tlMissionDec)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].tlMissionDec)
			
		// MiniGames don't have a Mission Description
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (missionDescriptionToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].tlMissionDec)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Description_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT(missionDescriptionToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (missionDescriptionToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Description_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Mission_Description_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'UNKNOWN'. Tell Keith")
	#ENDIF
	
	RETURN (missionDescriptionToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Mission Coordinates as stored in the cloud for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			VECTOR					The Mission Coordinates stored for this mission.
FUNC VECTOR Get_Coordinates_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	VECTOR coordsToReturn = << 0.0, 0.0, 0.0 >>

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.vStartPos)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.vStartPos)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Coordinates_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (coordsToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (coordsToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].vStartPos)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].vStartPos)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].vStartPos)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Coordinates_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_VECTOR(coordsToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (coordsToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Coordinates_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Coordinates_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning '<< 0.0, 0.0, 0.0 >>'. Tell Keith")
	#ENDIF
	
	RETURN (coordsToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the User Rating for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT						The user rating for this mission.
FUNC INT Get_User_Rating_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT userRatingToReturn	= 0

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iRating)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.iRating)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_User_Rating_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (userRatingToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (userRatingToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iRating)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iRating)
			
		// MiniGames don;t store a User Rating
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (userRatingToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iRating)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_User_Rating_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_INT(userRatingToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (userRatingToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_User_Rating_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_User_Rating_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning '0'. Tell Keith")
	#ENDIF
	
	RETURN (userRatingToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the vector for the starting cam for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			VECTOR						cam vector for this mission.
FUNC VECTOR Get_Cam_Vector_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	VECTOR camPosToReturn	= << 0.0, 0.0, 0.0 >>

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.vCamPos)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.vCamPos)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Cam_Vector_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (camPosToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (camPosToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].vCamPos)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].vCamPos)
			
		// MiniGames don't return a camera position
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (camPosToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].vCamPos)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Cam_Vector_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_VECTOR(camPosToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (camPosToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Cam_Vector_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Cam_Vector_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning '<< 0.0, 0.0, 0.0 >>'. Tell Keith")
	#ENDIF
	
	RETURN (camPosToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the vector for the starting cam for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			VECTOR						cam vector for this mission.
FUNC CLOUD_LOADED_DEFAULT_OPTIONS Get_Default_Corona_Options_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	CLOUD_LOADED_DEFAULT_OPTIONS sDefaultCoronaOptions

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (sDefaultCoronaOptions)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[arrayPos])
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sDefaultCoronaOptions[arrayPos])
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Default_Corona_Options_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Default_Corona_Options_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'sDefaultCoronaOptions'. Tell Bobby.")
	#ENDIF
	
	RETURN (sDefaultCoronaOptions)

ENDFUNC

FUNC INT Get_Mission_Photo_Path_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT iDefaultImagePath = -1
//
//	// Check if this cloud-loaded header data is on the shared missions cloud data array
//	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
//		NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Photo_Path_For_FM_Cloud_Loaded_Activity():Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))a") NET_NL()
//		RETURN -1 //(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iPhotoPath)
//	ENDIF
//
//	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
//	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
//		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
//			NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Photo_Path_For_FM_Cloud_Loaded_Activity():Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData)") NET_NL()
//			RETURN -1 //(g_sGeneratedCloudHeaderData.gchdHeaderData.iPhotoPath)
//		ENDIF
//		
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Photo_Path_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
//		#ENDIF
//	
//		RETURN (iDefaultImagePath)
//	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
	OR (arrayPos = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
		RETURN (iDefaultImagePath)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Photo_Path_For_FM_Cloud_Loaded_Activity():g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iPhotoPath") NET_NL()
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iPhotoPath)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iPhotoPath)
			
		// MiniGames don;t store a User Rating
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (iDefaultImagePath)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iPhotoPath)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Photo_Path_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_INT(iDefaultImagePath)
					NET_NL()
				#ENDIF
				
				RETURN (iDefaultImagePath)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Photo_Path_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Mission_Photo_Path_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning '0'. Tell Keith")
	#ENDIF
	
	RETURN (iDefaultImagePath)

ENDFUNC


FUNC BOOL Get_Is_Sc_NickName_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN FALSE
	ENDIF
			
	IF paramMissionIdData.idCreator =  FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
	AND arrayPos < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED 
		RETURN g_FMMC_ROCKSTAR_VERIFIED.bScNickName[arrayPos]
	ENDIF
	
	RETURN FALSE
ENDFUNC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the rootContentIDHash for the specified activity.
//
// RETURN PARAMS:			AdversaryModeType			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							RootContentID Hash for this mission.
FUNC INT Get_AdversaryModeType_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &refMissionIdData)

	INT								defaultReturnValue = 0

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(refMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (defaultReturnValue)
	ENDIF
	
	// Return the correct data
	SWITCH (refMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iAdversaryModeType)
			
		// KGM 5/3/14: Only Rockstar Created missions supply the RootContentID
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MissionInfo]: Get_AdversaryModeType_For_FM_Cloud_Loaded_Activity() requested for a Verified mission. Ignoring. Returning 0.")
			#ENDIF
			
			RETURN (defaultReturnValue)
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_AdversaryModeType_For_FM_Cloud_Loaded_Activity() ERROR - Unsupported Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(refMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_AdversaryModeType_For_FM_Cloud_Loaded_Activity: ERROR - Unsupported Creator ID. See Console Log. Returning 0. Tell Keith.")
	#ENDIF
	
	RETURN (defaultReturnValue)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the rootContentIDHash for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							RootContentID Hash for this mission.
FUNC INT Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &refMissionIdData)

	CLOUD_LOADED_DEFAULT_OPTIONS	sDefaultCoronaOptions
	INT								defaultReturnValue = 0
	
	// Only handle rootContentIdHash requests for rockstar created missions - the data doesn't get stored as Generated or Shared)
//	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(refMissionIdData))
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [MissionInfo]: Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity() ERROR - Ignoring request for Shared Data")
//			NET_NL()
//			
//			SCRIPT_ASSERT("Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity: ERROR - Ignoring request for Shared Data. Returning 0. Tell Keith.")
//		#ENDIF
//		
//		RETURN defaultReturnValue
//	ENDIF
//	
//	IF (Is_This_Generated_Cloud_Header_Data(refMissionIdData.idVariation))
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [MissionInfo]: Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity() ERROR - Ignoring request for Generated Data")
//			NET_NL()
//			
//			SCRIPT_ASSERT("Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity: ERROR - Ignoring request for Generated Data. Returning 0. Tell Keith.")
//		#ENDIF
//		
//		RETURN defaultReturnValue
//	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(refMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (defaultReturnValue)
	ENDIF
	
	// Return the correct data
	SWITCH (refMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			sDefaultCoronaOptions = Get_Default_Corona_Options_For_FM_Cloud_Loaded_Activity(refMissionIdData)
			RETURN (sDefaultCoronaOptions.iRootContentIdHash)
			
		// KGM 5/3/14: Only Rockstar Created missions supply the RootContentID
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MissionInfo]: Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity() requested for a Verified mission. Ignoring. Returning 0.")
			#ENDIF
			
			RETURN (defaultReturnValue)
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity() ERROR - Unsupported Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(refMissionIdData.idCreator)
		NET_NL()
		
		//SCRIPT_ASSERT("Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity: ERROR - Unsupported Creator ID. See Console Log. Returning 0. Tell Keith.")
	#ENDIF
	
	RETURN (defaultReturnValue)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the iMinGangRank for the specified activity - Currently CNC-SPECIFIC
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							iMinGangRank for this mission.
FUNC INT Get_Gang_Rank_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &refMissionIdData)

	CLOUD_LOADED_DEFAULT_OPTIONS	sDefaultCoronaOptions
	INT								defaultReturnValue = 8999
	
	// Only handle iMinGangRank requests for rockstar created missions
	// (Currently only used by CnC Missions - FOR NOW: Ignore Shared and Generated data, but I'll probably need to handle this)
//	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(refMissionIdData))
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [MissionInfo]: Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity() ERROR - Ignoring request for Shared Data")
//			NET_NL()
//			
//			SCRIPT_ASSERT("Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity: ERROR - Ignoring request for Shared Data. Returning 0. Tell Keith.")
//		#ENDIF
//		
//		RETURN defaultReturnValue
//	ENDIF
//	
//	IF (Is_This_Generated_Cloud_Header_Data(refMissionIdData.idVariation))
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [MissionInfo]: Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity() ERROR - Ignoring request for Generated Data")
//			NET_NL()
//			
//			SCRIPT_ASSERT("Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity: ERROR - Ignoring request for Generated Data. Returning 0. Tell Keith.")
//		#ENDIF
//		
//		RETURN defaultReturnValue
//	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(refMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (defaultReturnValue)
	ENDIF
	
	// Return the correct data
	SWITCH (refMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			sDefaultCoronaOptions = Get_Default_Corona_Options_For_FM_Cloud_Loaded_Activity(refMissionIdData)
			RETURN (sDefaultCoronaOptions.iMinGangRank)
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Gang_Rank_For_FM_Cloud_Loaded_Activity() ERROR - Unsupported Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(refMissionIdData.idCreator)
		NET_NL()
		
	//	SCRIPT_ASSERT("Get_Gang_Rank_For_FM_Cloud_Loaded_Activity: ERROR - Unsupported Creator ID. See Console Log. Returning 0. Tell Keith.")
	#ENDIF
	
	RETURN (defaultReturnValue)

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Temporary function for marking CnC missions as requiring a minimum gang hate stat value- Currently CNC-SPECIFIC
//
// INPUT PARAMS:			hashRootContentID			The hash of the root content id for the cloud loaded activity
// RETURN VALUE:			INT							Minimum gang hate stat value required
FUNC INT TEMP_Get_Hardcoded_MinimumGangHate_For_Mission (INT hashRootContentID)

	
	IF (hashRootContentID = 0)
		RETURN 0
	ENDIF

//	SWITCH (hashRootContentID)
//		CASE 1438689793       RETURN 10 // CnC - Escort Boss
//
//	ENDSWITCH

	//-- Most missions won't have a minimum gang hate stat requirement
	RETURN 0 

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the iMinGangHate for the specified activity - Currently CNC-SPECIFIC
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							iMinGangHate for this mission
FUNC INT Get_Minimum_Gang_Hate_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &refMissionIdData)

	CLOUD_LOADED_DEFAULT_OPTIONS	sDefaultCoronaOptions
	INT								defaultReturnValue = 0
//	INT								theRootContentIdHash = 0
	

	// Only handle iMinGangRank requests for rockstar created missions
	// (Currently only used by CnC Missions - FOR NOW: Ignore Shared and Generated data, but I'll probably need to handle this)
//	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(refMissionIdData))
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [MissionInfo]: Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity() ERROR - Ignoring request for Shared Data")
//			NET_NL()
//			
//			SCRIPT_ASSERT("Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity: ERROR - Ignoring request for Shared Data. Returning 0. Tell Keith.")
//		#ENDIF
//		
//		RETURN defaultReturnValue
//	ENDIF
//	
//	IF (Is_This_Generated_Cloud_Header_Data(refMissionIdData.idVariation))
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [MissionInfo]: Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity() ERROR - Ignoring request for Generated Data")
//			NET_NL()
//			
//			SCRIPT_ASSERT("Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity: ERROR - Ignoring request for Generated Data. Returning 0. Tell Keith.")
//		#ENDIF
//		
//		RETURN defaultReturnValue
//	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(refMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (defaultReturnValue)
	ENDIF
	
	// Return the correct data
	SWITCH (refMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// KGM 16/4/14: COMMENTED OUT UNTIL WE GET MINIMUM GANG HATE FROM THE CLOUD
			sDefaultCoronaOptions = Get_Default_Corona_Options_For_FM_Cloud_Loaded_Activity(refMissionIdData)
			RETURN (sDefaultCoronaOptions.iCnCBitset)

			// KGM 16/4/14: TEMP HARDCODED REPLACEMENT FOR THE ABOVE
		//	theRootContentIdHash = Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(refMissionIdData)
		//	RETURN (TEMP_Get_Hardcoded_MinimumGangHate_For_Mission(theRootContentIdHash))
			
		// Only Rockstar Created content should have a CnC minimum Gang Hate value for the mission
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (defaultReturnValue)

	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Minimum_Gang_Hate_For_FM_Cloud_Loaded_Activity() ERROR - Unsupported Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(refMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Minimum_Gang_Hate_For_FM_Cloud_Loaded_Activity: ERROR - Unsupported Creator ID. See Console Log. Returning 0. Tell Keith.")
	#ENDIF
	
	RETURN (defaultReturnValue)

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Hard code which CnC missions each team can or can't join (through invites, as opposed to flow)
//
// INPUT PARAMS:			hashRootContentID			The hash of the root content id for the cloud loaded activity
// INPUT PARAMS:			INT							
// RETURN VALUE:			BOOL						IF the mission can be joined by the CnC team
FUNC BOOL TEMP_Get_Hardcoded_CnC_Mission_Joinable_By_Team (INT hashRootContentID, INT iCnCTeam)
	// Added by Dave W
	
	IF iCnCTeam = 0 // Cops
		SWITCH hashRootContentID
			//-- Cops can't join Showroom
			CASE 1443412552		RETURN FALSE
			
			//-- ...or Showroom 2
			CASE -1866883004	RETURN FALSE
		ENDSWITCH
	ENDIF
	
	
	RETURN TRUE

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Determine if a member of the CnC Team specified can join a CnC mission
//
// INPUT PARAMS:			INT								The CnC Team to check against
// RETURN PARAMS:			paramMissionIdData				The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL							iMinGangHate for this mission
FUNC BOOL Get_Is_CnC_Mission_Joinable_By_Team(MP_MISSION_ID_DATA &refMissionIdData, INT iCnCTeam)
	// Added by Dave W
	
	
//	CLOUD_LOADED_DEFAULT_OPTIONS	sDefaultCoronaOptions
	BOOL							defaultReturnValue = TRUE
	INT								theRootContentIdHash = 0

	
	IF iCnCTeam < 0
		//-- Invalid team
		RETURN FALSE
	ENDIF
	
	
	
	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(refMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (defaultReturnValue)
	ENDIF
	
	// Return the correct data
	SWITCH (refMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// KGM 23/08/14: COMMENTED OUT UNTIL WE GET JOINABLE MISSION BITSET FROM THE CLOUD
		//	sDefaultCoronaOptions = Get_Default_Corona_Options_For_FM_Cloud_Loaded_Activity(refMissionIdData)
		//	RETURN (sDefaultCoronaOptions.iCnCBitset)

			// KGM 23/08/14: TEMP HARDCODED REPLACEMENT FOR THE ABOVE
			theRootContentIdHash = Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(refMissionIdData)
			RETURN (TEMP_Get_Hardcoded_CnC_Mission_Joinable_By_Team(theRootContentIdHash, iCnCTeam))
			
		// Only Rockstar Created content should have a CnC minimum Gang Hate value for the mission
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (defaultReturnValue)

	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Is_CnC_Mission_Joinable_By_Team() ERROR - Unsupported Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(refMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Is_CnC_Mission_Joinable_By_Team: ERROR - Unsupported Creator ID. See Console Log. Returning 0. Tell Keith.")
	#ENDIF
	
	RETURN (defaultReturnValue)

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

FUNC BOOL Get_Have_Played_Before_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL bPlayedAlready

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		NET_PRINT("...KGM MP [MissionInfo]: bPlayedAlready - Get_Have_Played_Before_For_FM_Cloud_Loaded_Activity() IF (arrayPos = ILLEGAL_ARRAY_POSITION)")
		RETURN (bPlayedAlready)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			NET_PRINT("...KGM MP [MissionInfo]: bPlayedAlready - Get_Have_Played_Before_For_FM_Cloud_Loaded_Activity() FMMC_ROCKSTAR_CREATOR_ID")
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			NET_PRINT("...KGM MP [MissionInfo]: bPlayedAlready - Get_Have_Played_Before_For_FM_Cloud_Loaded_Activity() FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID")
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED))
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: bPlayedAlready - Get_Have_Played_Before_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Have_Played_Before_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'sDefaultCoronaOptions'. Tell Bobby.")
	#ENDIF
	
	RETURN (bPlayedAlready)

ENDFUNC


FUNC INT Get_Default_Corona_Options_For_FM_Cloud_Loaded_Activity_HASH(MP_MISSION_ID_DATA &paramMissionIdData)
	INT iDefaultCoronaOptionsHash = 0

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (iDefaultCoronaOptionsHash)
	ENDIF
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[arrayPos].iHash)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sDefaultCoronaOptions[arrayPos].iHash)
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Default_Corona_Options_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Default_Corona_Options_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'iDefaultCoronaOptionsHash'. Tell Bobby.")
	#ENDIF
	
	RETURN (iDefaultCoronaOptionsHash)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the heading for the starting cam for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			VECTOR						cam heading for this mission.
FUNC VECTOR Get_Cam_Heading_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	VECTOR camHeadingToReturn	= << 0.0, 0.0, 0.0 >>

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.vCamHead)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.vCamHead)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Cam_Heading_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (camHeadingToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (camHeadingToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].vCamHead)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].vCamHead)
			
		// MiniGames don't return a camera heading
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (camHeadingToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].vCamHead)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Cam_Heading_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_VECTOR(camHeadingToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (camHeadingToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Cam_Heading_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Cam_Heading_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning '0.0'. Tell Keith")
	#ENDIF
	
	RETURN (camHeadingToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Les Rating for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							The Les Rating for this mission.
#IF IS_DEBUG_BUILD
FUNC INT Get_Les_Rating_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT LesRatingToReturn	= 0

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iLesRating)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.iLesRating)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Les_Rating_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (LesRatingToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (LesRatingToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iLesRating)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iLesRating)
			
		// MiniGames don't return a Les Rating
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (LesRatingToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - doesn't have a Les Rating
				RETURN (LesRatingToReturn)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Les_Rating_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_INT(LesRatingToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (LesRatingToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Les_Rating_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Les_Rating_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning '0'. Tell Keith")
	#ENDIF
	
	RETURN (LesRatingToReturn)

ENDFUNC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the handel for the mission description for the specified activity.
//
// INPUT PARAMS:			paramCheckLocalOnly			[DEFAULT = FALSE] TRUE means check local data only, FALSE means check all data including Shared and Generated
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							The handel for the mission description for this mission.
FUNC INT Get_Mission_Dec_Hash_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData, BOOL paramCheckLocalOnly = FALSE)

	INT iMissionDecHash	= 0

	IF NOT (paramCheckLocalOnly)
		// Check if this cloud-loaded header data is on the shared missions cloud data array
		IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
			// If the local player has processed the Shared Mission data, then return the local version of the Shared DescriptionHash)
			IF (IS_BIT_SET(g_MSCloudClientControl.mscccdMissions[paramMissionIdData.idSharedRegID].msccdBitfield, MSCCLIENT_BITFLAG_MISSION_PROCESSED))
				// ...local player has processed the Shared Mission, so return the shared copy of the Description
				RETURN (g_MSCLocalLanguageText.msclldtDescHash[paramMissionIdData.idSharedRegID])
			ENDIF
			
			// Otherwise let it fall through to try to find the data locally - if not available it will return 0
		ENDIF
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.iMissionDecHash)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Dec_Hash_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (iMissionDecHash)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (0)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iMissionDecHash)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iMissionDecHash)
			
		// MiniGames don't return a description
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (0)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iMissionDecHash)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array			
				RETURN (iMissionDecHash)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Mission_Dec_Hash_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Mission_Dec_Hash_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning '0'. Tell Keith")
	#ENDIF
	
	RETURN (iMissionDecHash)

ENDFUNC
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Rank for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							The Rank for this mission.
FUNC INT Get_Rank_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT rankToReturn	= 0

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iRank)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.iRank)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Rank_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (rankToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (rankToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iRank)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iRank)
			
		// MiniGames don't return a rank
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (rankToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iRank)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Rank_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_INT(rankToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (rankToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Rank_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Rank_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning '0'. Tell Keith")
	#ENDIF
	
	RETURN (rankToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Options Bitset for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							The Options Bitfield
FUNC INT Get_Options_Bitfield_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT bitfieldToReturn	= ALL_MISSION_INFO_BITS_CLEAR

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Options_Bitfield_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (bitfieldToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (bitfieldToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet)
			
		// MiniGames don't return an options bitfield
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (bitfieldToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iBitSet)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Options_Bitfield_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_INT(bitfieldToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (bitfieldToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Options_Bitfield_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Options_Bitfield_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'ALL_MISSION_INFO_BITS_CLEAR'. Tell Keith")
	#ENDIF
	
	RETURN (bitfieldToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the 'Played' status for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this mission been played at any time, FALSE if never played
FUNC BOOL Get_Played_Status_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL playedStatusToReturn	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (IS_BIT_SET(g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Played_Status_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (playedStatusToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (playedStatusToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED))
			
		// MiniGames don't return a played status
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (playedStatusToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - doesn't have a played status
				RETURN (playedStatusToReturn)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Played_Status_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(playedStatusToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (playedStatusToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Played_Status_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Played_Status_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'ALL_MISSION_INFO_BITS_CLEAR'. Tell Keith")
	#ENDIF
	
	RETURN (playedStatusToReturn)

ENDFUNC

// PURPOSE:	Gets whether the mission is a pushbike only mission (used for races)
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this mission is a bike only race, FALSE otherwise
FUNC BOOL Is_FM_Cloud_Loaded_Activity_A_Bicycle_Race(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL isMissionABikeOnlyRace	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciROCKSTAR_CREATED_MISSION_IS_PUSH_BIKE_ONLY))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (IS_BIT_SET(g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet, ciROCKSTAR_CREATED_MISSION_IS_PUSH_BIKE_ONLY))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Bicycle_Race(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (isMissionABikeOnlyRace)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (isMissionABikeOnlyRace)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_IS_PUSH_BIKE_ONLY))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_IS_PUSH_BIKE_ONLY))
			
		// MiniGames don't require push bikes
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (isMissionABikeOnlyRace)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - can't get this info yet
				RETURN (isMissionABikeOnlyRace)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Bicycle_Race(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(isMissionABikeOnlyRace)
					NET_NL()
				#ENDIF
				
				RETURN (isMissionABikeOnlyRace)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Bicycle_Race() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_Bicycle_Race: ERROR - Unknown Creator ID. See Console Log. Returning 'ALL_MISSION_INFO_BITS_CLEAR'. Tell Keith")
	#ENDIF
	
	RETURN (isMissionABikeOnlyRace)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the 'Bookmarked' status for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this mission is a bookmark, FALSE if not
FUNC BOOL Get_Bookmarked_Status_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL bookmarkStatusToReturn	= FALSE

	// Ignore Shared Missions Data when checking for bookmarked missions because the 'bookmarked' flag will have been shared but will be wrong for the majority of players

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			// Generated Header Data won't be bookmarked (the data should be locally available)
			RETURN (bookmarkStatusToReturn)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Bookmarked_Status_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (bookmarkStatusToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (bookmarkStatusToReturn)
	ENDIF
	
	// Return the correct data - Only UGC will be bookmarked (Rockstar missions that have been bookmarked will come in as UGC too)
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (bookmarkStatusToReturn)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (bookmarkStatusToReturn)
			
		// MiniGames don't return a played status
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (bookmarkStatusToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - check the Bookmarked
				RETURN (IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_IS_A_BOOKMARK))
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Bookmarked_Status_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(bookmarkStatusToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (bookmarkStatusToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Bookmarked_Status_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Bookmarked_Status_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'ALL_MISSION_INFO_BITS_CLEAR'. Tell Keith")
	#ENDIF
	
	RETURN (bookmarkStatusToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Locally update the mission data 'played' flag so it can be re-used immediately after a transition without waiting for the cloud turn-around time
//
// REFERENCE PARAMS:		paramMissionIdData		The MissionIdData
//
// NOTES:	This will only update if the contentID matches for the passed in array position - if not then it's safer to leave it to the usual process
PROC Instant_Local_Update_Of_Played_Status_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	// Ignore for generated header data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Instant_Local_Update_Of_Played_Status_For_FM_Cloud_Loaded_Activity(): IGNORE - Data is Generated Header Data.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Ignore if the passed in cloud filename is not set
	IF (IS_STRING_NULL_OR_EMPTY(paramMissionIdData.idCloudFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Instant_Local_Update_Of_Played_Status_For_FM_Cloud_Loaded_Activity(): IGNORE - Passed in ContentID is NULL or EMPTY.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Only deal with Rockstar Created and Rockstar Verified data, ignore everything else
	SWITCH (paramMissionIdData.idCreator)
		// Rockstar Created Data
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// Is the missionID Data still valid?
			IF (paramMissionIdData.idVariation < FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED)
				IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
					IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramMissionIdData.idVariation].tlName, paramMissionIdData.idCloudFilename))
						
						IF NOT IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED)
						
							PRINTLN("[BIGFEED] Instant Local 'Played' state updated. Increment our count for this Job type played (R* Created): ", paramMissionIdData.idCloudFilename)
							
							#IF IS_DEBUG_BUILD
								IF g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionType != -1
									SCRIPT_ASSERT("BIG_FEED Instant_Local_Update_Of_Played_Status_For_FM_Cloud_Loaded_Activity - Trying to update a mission when one already needs to be processed")
								ENDIF
							#ENDIF
							
							g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionType = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramMissionIdData.idVariation].iType
							g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionSubType = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramMissionIdData.idVariation].iSubType
							g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iRank = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramMissionIdData.idVariation].iRank
							g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iContactInt = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramMissionIdData.idVariation].iContactCharEnum
							g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.bVerified = FALSE
						ENDIF
						
						// Found a match, mission is still in the same location, so update
						SET_BIT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED)
						
						#IF IS_DEBUG_BUILD
							NET_PRINT("...KGM MP [MissionInfo]: Instant Local 'Played' status update setting Played for Rockstar Created mission: ")
							NET_PRINT(paramMissionIdData.idCloudFilename)
							NET_NL()
						#ENDIF
		
						EXIT
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MissionInfo]: Instant_Local_Update_Of_Played_Status_For_FM_Cloud_Loaded_Activity(): IGNORE - Rockstar Created data didn't match.") NET_NL()
			#ENDIF
		
			EXIT
			
		// Rockstar verified data
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// Is the missionID Data still valid?
			IF (paramMissionIdData.idVariation < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED)
				IF (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
					IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramMissionIdData.idVariation].tlName, paramMissionIdData.idCloudFilename))
						
						IF NOT IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED)
						
							PRINTLN("[BIGFEED] Instant Local 'Played' state updated. Increment our count for this Job type played (R* Verified)")
						
							#IF IS_DEBUG_BUILD
								IF g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionType != -1
									SCRIPT_ASSERT("BIG_FEED Instant_Local_Update_Of_Played_Status_For_FM_Cloud_Loaded_Activity - Trying to update a mission when one already needs to be processed")
								ENDIF
							#ENDIF
						
							g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionType = g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramMissionIdData.idVariation].iType
							g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionSubType = g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramMissionIdData.idVariation].iSubType
							g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iRank = g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramMissionIdData.idVariation].iRank
							g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iContactInt = g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramMissionIdData.idVariation].iContactCharEnum
							g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.bVerified = TRUE
						ENDIF
						
						// Found a match, mission is still in the same location, so update
						SET_BIT(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[paramMissionIdData.idVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED)
						
						#IF IS_DEBUG_BUILD
							NET_PRINT("...KGM MP [MissionInfo]: Instant Local 'Played' status update setting Played for Rockstar Verified mission: ")
							NET_PRINT(paramMissionIdData.idCloudFilename)
							NET_NL()
						#ENDIF
		
						EXIT
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MissionInfo]: Instant_Local_Update_Of_Played_Status_For_FM_Cloud_Loaded_Activity(): IGNORE - Rockstar Verified data didn't match.") NET_NL()
			#ENDIF
		
			EXIT
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Instant_Local_Update_Of_Played_Status_For_FM_Cloud_Loaded_Activity(): IGNORE - don't update data for this Creator ID.") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the 'Secondary Unlock' status for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this mission should be a secondary unlock, FALSE if primary unlock
//
// NOTES:	Added for Survival where the player must play one of the two Primary Unlock survivals before the secondary unlock survivals become available
FUNC BOOL Get_Secondary_Unlock_Status_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL secondaryUnlockToReturn	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciMISSION_OPTION_BS_SECONDARY_UNLOCK))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (IS_BIT_SET(g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet, ciMISSION_OPTION_BS_SECONDARY_UNLOCK))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Secondary_Unlock_Status_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (secondaryUnlockToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (secondaryUnlockToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_SECONDARY_UNLOCK))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_SECONDARY_UNLOCK))
			
		// MiniGames don't return a played status
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (secondaryUnlockToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - doesn't have a played status
				RETURN (secondaryUnlockToReturn)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Secondary_Unlock_Status_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(secondaryUnlockToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (secondaryUnlockToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Secondary_Unlock_Status_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Secondary_Unlock_Status_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'FALSE'. Tell Keith")
	#ENDIF
	
	RETURN (secondaryUnlockToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the 'One Play' status for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this mission is a One Play mission only, FALSE if the mission can be replayed if there are no CMs available at the current rank
FUNC BOOL Get_One_Play_Status_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL OnePlayStatusToReturn	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciMISSION_OPTION_BS_ONE_PLAY))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (IS_BIT_SET(g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet, ciMISSION_OPTION_BS_ONE_PLAY))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_One_Play_Status_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (OnePlayStatusToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (OnePlayStatusToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_ONE_PLAY))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_ONE_PLAY))
			
		// MiniGames don't return a played status
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (OnePlayStatusToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - doesn't have a played status
				RETURN (OnePlayStatusToReturn)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_One_Play_Status_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(OnePlayStatusToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (OnePlayStatusToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_One_Play_Status_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_One_Play_Status_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'FALSE'. Tell Keith")
	#ENDIF
	
	RETURN (OnePlayStatusToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the 'Forced At Rank' status for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this mission is A CM Forced At A Specific Rank only, FALSE if the mission is not a CM forced at a specific rank
FUNC BOOL Get_Forced_At_Rank_Status_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL forcedAtRankStatusToReturn	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciMISSION_OPTION_BS_FORCED_AT_RANK))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (IS_BIT_SET(g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet, ciMISSION_OPTION_BS_FORCED_AT_RANK))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Forced_At_Rank_Status_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (forcedAtRankStatusToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (forcedAtRankStatusToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_FORCED_AT_RANK))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_FORCED_AT_RANK))
			
		// MiniGames don't return a played status
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (forcedAtRankStatusToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - doesn't have a played status
				RETURN (forcedAtRankStatusToReturn)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Forced_At_Rank_Status_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(forcedAtRankStatusToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (forcedAtRankStatusToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Forced_At_Rank_Status_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Forced_At_Rank_Status_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'FALSE'. Tell Keith")
	#ENDIF
	
	RETURN (forcedAtRankStatusToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Subtype of the Mission Type for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							The Subtype for this mission (ie: air race, land race, sea race, etc).
FUNC INT Get_SubType_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT subtypeToReturn	= -1

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iSubType)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.iSubType)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_SubType_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (subtypeToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (subtypeToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iSubType)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iSubType)
			
		// MiniGames don't return a subtype
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (subtypeToReturn)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iSubType)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_SubType_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_INT(subtypeToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (subtypeToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_SubType_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_SubType_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning '-1'. Tell Keith")
	#ENDIF
	
	RETURN (subtypeToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Convert the contactCharEnum value (coming in as a Hash of a contact string) to the enumCharacterList array position of the contact (as an INT)
//
// INPUT PARAMS:			paramContactFromCloud			The value read from the cloud
// RETURN VALUE:			INT								The INT version of the enumCharacterList converted from the data read from the cloud
//
// TEMP: Supports th eold way (INT version of enumCharacterList stored on the cloud) and the new safer enum-safe way (Hash value of a string associated with the contact)
FUNC INT Get_enumCharacterListAsInt_From_ContactAsInt(INT paramContactFromCloud)

	// 0 means no character
	IF (paramContactFromCloud = 0)
		RETURN (0)
	ENDIF
	
	// If the contactEnum INT is less than this max then treat it as an old style contactEnum INT, otherwise treat as the stringHash version
	CONST_INT	TEMP_FIXED_MAX_CHARS_FOR_OLD_METHOD_CONVERSION	200
	
	// TEMP: The old way was to store the enumCharacterList array position as the cloud data - this wasn't very safe, but still needs catered for short-term
	//		until we are fully changed over to the Hash value method (Hash values are usually very big (positive or negative))
	IF (paramContactFromCloud > 0)
	AND (paramContactFromCloud <= ENUM_TO_INT(TEMP_FIXED_MAX_CHARS_FOR_OLD_METHOD_CONVERSION))
		// ...the INT is small, so treat as the old way - an enumCharacterList array position
		// Ensure a value greater than the last MP character is treated as NO_CHARACTER
		IF (paramContactFromCloud > (ENUM_TO_INT(CHAR_MP_JULIO)))
			RETURN (0)
		ENDIF
		
		// Check for specific INTs and convert them to the correct characters
		SWITCH (paramContactFromCloud)
			CASE 10			RETURN (ENUM_TO_INT(CHAR_LESTER))
			CASE 17			RETURN (ENUM_TO_INT(CHAR_LAMAR))
			CASE 18			RETURN (ENUM_TO_INT(CHAR_RON))
			CASE 28			RETURN (ENUM_TO_INT(CHAR_MARTIN))
			CASE 75			RETURN (ENUM_TO_INT(CHAR_LESTER))	// 75 used to be CHAR_MP_LESTER
		ENDSWITCH
		
		// Finally, just return what came in
		RETURN (paramContactFromCloud)
	ENDIF
	// END TEMP
	
	// KGM 9/5/13: The new way is to use a Hash of a string that can be converted to an enumCharacterList character
	enumCharacterList theCharacterListID = Get_enumCharacterList_From_Contact_Hash(paramContactFromCloud)
	
	IF (theCharacterListID = NO_CHARACTER)
		RETURN (0)
	ENDIF
	
	RETURN (ENUM_TO_INT(theCharacterListID))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Convert the contactCharEnum value (coming in as a Hash of a contact string) to the enumCharacterList array position of the contact
//
// INPUT PARAMS:			paramContactFromCloud			The value read from the cloud
// RETURN VALUE:			enumCharacterList				The enumCharacterList converted from the data read from the cloud
//
// TEMP: Supports the old way (INT version of enumCharacterList stored on the cloud) and the new safer enum-safe way (Hash value of a string associated with the contact)
FUNC enumCharacterList Get_enumCharacterList_From_ContactAsInt(INT paramContactFromCloud)

	INT characterEnumAsInt = Get_enumCharacterListAsInt_From_ContactAsInt(paramContactFromCloud)
	
	IF (characterEnumAsInt = 0)
		RETURN (NO_CHARACTER)
	ENDIF
	
	RETURN (INT_TO_ENUM(enumCharacterList, characterEnumAsInt))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Mission Contact as stored in the cloud for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							The Contact INT value as directly stored on the cloud for this mission (with no conversion to enumCharacterList)
FUNC INT Get_Contact_Value_As_Read_From_Cloud_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT contactValueFromCloud = 0

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iContactCharEnum)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.iContactCharEnum)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_Contact_Value_As_Read_From_Cloud_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (contactValueFromCloud)
	ENDIF

	// The data isn't stored as a shared activity, so get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (contactValueFromCloud)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iContactCharEnum)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iContactCharEnum)
			
		// MiniGames don't return a contact
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (contactValueFromCloud)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iContactCharEnum)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_Contact_Value_As_Read_From_Cloud_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_INT(contactValueFromCloud)
					NET_NL()
				#ENDIF
				
				RETURN (contactValueFromCloud)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Contact_Value_As_Read_From_Cloud_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_Contact_Value_As_Read_From_Cloud_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'NO_CHARACTER (as Int)'. Tell Keith")
	#ENDIF
	
	RETURN (contactValueFromCloud)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Mission Contact as stored in the cloud for the specified activity.
//
// RETURN PARAMS:			refMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							The Contact Character (as an INT) for this mission, or NO_CHARACTER (as INT).
//
// NOTES:	This data isn't yet available so I'm making it up
FUNC INT Get_ContactAsInt_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &refMissionIdData)
	INT contactValueOnCloud = Get_Contact_Value_As_Read_From_Cloud_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	RETURN (Get_enumCharacterListAsInt_From_ContactAsInt(contactValueOnCloud))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Mission Contact as stored in the cloud for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			enumCharacterList		The Contact Character for this mission, or NO_CHARACTER.
FUNC enumCharacterList Get_Contact_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT	contactAsInt = Get_ContactAsInt_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
	
	IF (contactAsInt = 0)
		RETURN (NO_CHARACTER)
	ENDIF
	
	enumCharacterList theContact = INT_TO_ENUM(enumCharacterList, contactAsInt)
	
	RETURN (theContact)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	When using offline content check if this is a Heist based on MissionID and MissionSubtype
// This will be superseded by the bit in teh bitfield being set
FUNC BOOL TEMP_Check_If_Heist_Using_Mission_Subtype(MP_MISSION_ID_DATA &refMissionIdData)
	IF (refMissionIdData.idMission != eFM_MISSION_CLOUD)
		RETURN FALSE
	ENDIF
	RETURN (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) = FMMC_MISSION_TYPE_HEIST)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the activity is a Heist.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this is a Heist mission, FALSE if not
FUNC BOOL Is_FM_Cloud_Loaded_Activity_A_Heist(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL isHeist	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		// KGM 30/7/14: Only use the subtype now
		RETURN (TEMP_Check_If_Heist_Using_Mission_Subtype(paramMissionIdData))
//		IF (g_b_HasEntered_ReducedContent)
//			RETURN (TEMP_Check_If_Heist_Using_Mission_Subtype(paramMissionIdData))
//		ENDIF
//		RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciMISSION_OPTION_BS_HEIST_HIGH_END))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (TEMP_Check_If_Heist_Using_Mission_Subtype(paramMissionIdData))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Heist(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (isHeist)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (isHeist)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			IF (TEMP_Check_If_Heist_Using_Mission_Subtype(paramMissionIdData))
				// KGMTEMPHEIST VERY TEMP - All Missions tagged as Heist come from Lester if no contact
				IF (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iContactCharEnum = 0)
					g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iContactCharEnum = ENUM_TO_INT(CHAR_LESTER)
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_Heist(): NO CONTACT ADDED FOR HEIST. Heists need a ContactID. Tell Keith or the Heist Creator. Assuming Lester.")
					#ENDIF
				ENDIF
				// END VERY TEMP
				RETURN TRUE
			ENDIF
			RETURN FALSE
//			IF (g_b_HasEntered_ReducedContent)
//				IF (TEMP_Check_If_Heist_Using_Mission_Subtype(paramMissionIdData))
//					// KGMTEMPHEIST VERY TEMP - All Missions tagged as Heist come from Lester if no contact
//					IF (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iContactCharEnum = 0)
//						g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iContactCharEnum = ENUM_TO_INT(CHAR_LESTER)
//						#IF IS_DEBUG_BUILD
//							SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_Heist(): NO CONTACT ADDED FOR HEIST. Heists need a ContactID. Tell Keith or the Heist Creator. Assuming Lester.")
//						#ENDIF
//					ENDIF
//					// END VERY TEMP
//					RETURN TRUE
//				ENDIF
//				RETURN FALSE
//			ENDIF
//			IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_HEIST_HIGH_END))
//				// KGMTEMPHEIST VERY TEMP - All Missions tagged as Heist come from Lester if no contact
//				IF (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iContactCharEnum = 0)
//					g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iContactCharEnum = ENUM_TO_INT(CHAR_LESTER)
//					#IF IS_DEBUG_BUILD
//						SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_Heist(): NO CONTACT ADDED FOR HEIST. Heists need a ContactID. Tell Keith or the Heist Creator. Assuming Lester.")
//					#ENDIF
//				ENDIF
//				// END VERY TEMP
//				RETURN TRUE
//			ENDIF
//			RETURN FALSE
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_Heist_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_Heist_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_HEIST_HIGH_END))
			
		// MiniGames aren't Heists
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (isHeist)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - Heists aren't UGC
				RETURN (isHeist)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Heist(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(isHeist)
					NET_NL()
				#ENDIF
				
				RETURN (isHeist)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Heist() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_Heist: ERROR - Unknown Creator ID. See Console Log. Returning FALSE. Tell Keith")
	#ENDIF
	
	RETURN (isHeist)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	When using offline content check if this is a Heist Planning mission based on MissionID and MissionSubtype
// This will be superseded by the bit in the bitfield being set
FUNC BOOL TEMP_Check_If_Heist_Planning_Using_Mission_Subtype(MP_MISSION_ID_DATA &refMissionIdData)
	IF (refMissionIdData.idMission != eFM_MISSION_CLOUD)
		RETURN FALSE
	ENDIF
	RETURN (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) = FMMC_MISSION_TYPE_PLANNING)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the activity is a Heist Planning mission.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this is a Heist Planning mission, FALSE if not
FUNC BOOL Is_FM_Cloud_Loaded_Activity_Heist_Planning(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL isHeistPlanning	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		// KGM 30/7/14: Only use the subtype now
		RETURN (TEMP_Check_If_Heist_Planning_Using_Mission_Subtype(paramMissionIdData))
//		IF (g_b_HasEntered_ReducedContent)
//			RETURN (TEMP_Check_If_Heist_Planning_Using_Mission_Subtype(paramMissionIdData))
//		ENDIF
//		RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciMISSION_OPTION_BS_HEIST_PLANNING))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (TEMP_Check_If_Heist_Planning_Using_Mission_Subtype(paramMissionIdData))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_Heist_Planning(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (isHeistPlanning)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (isHeistPlanning)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_Heist_Planning_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				IF (TEMP_Check_If_Heist_Planning_Using_Mission_Subtype(paramMissionIdData))
//					RETURN TRUE
//				ENDIF
//				RETURN FALSE
//			ENDIF
//			IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_HEIST_PLANNING))
//				RETURN TRUE
//			ENDIF
//			RETURN FALSE
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_Heist_Planning_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_Heist_Planning_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_HEIST_PLANNING))
			
		// MiniGames aren't Heist Planning
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (isHeistPlanning)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - Heist Planning missions aren't UGC
				RETURN (isHeistPlanning)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_Heist_Planning(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(isHeistPlanning)
					NET_NL()
				#ENDIF
				
				RETURN (isHeistPlanning)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_Heist_Planning() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_Heist_Planning: ERROR - Unknown Creator ID. See Console Log. Returning FALSE. Tell Keith")
	#ENDIF
	
	RETURN (isHeistPlanning)

ENDFUNC


FUNC BOOL TEMP_Check_If_Flow_Using_Mission_Subtype(MP_MISSION_ID_DATA &refMissionIdData)
	IF (refMissionIdData.idMission != eFM_MISSION_CLOUD)
		RETURN FALSE
	ENDIF
	RETURN (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) = FMMC_MISSION_TYPE_FLOW_MISSION)
ENDFUNC
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the activity is a Flow Mission.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this is a Contact Mission, FALSE if not
FUNC BOOL Is_FM_Cloud_Loaded_Activity_A_Flow_Mission(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL isFlow	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (TEMP_Check_If_Flow_Using_Mission_Subtype(paramMissionIdData))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (TEMP_Check_If_Flow_Using_Mission_Subtype(paramMissionIdData))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Flow_Mission(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (isFlow)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (isFlow)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN (TEMP_Check_If_Flow_Using_Mission_Subtype(paramMissionIdData))
			
		DEFAULT
			RETURN FALSE
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Flow_Mission() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_Flow_Mission: ERROR - Unknown Creator ID. See Console Log. Returning FALSE. Tell Keith")
	#ENDIF
	
	RETURN (isFlow)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	When using offline content check if this is a Contact Mission based on MissionID and MissionSubtype
// This will be superseded by the bit in teh bitfield being set
FUNC BOOL TEMP_Check_If_Contact_Mission_Using_Mission_Subtype(MP_MISSION_ID_DATA &refMissionIdData)
	// TEMP: Any mission with a contact that isn't a Heist is a contact mission
	IF (Get_Contact_For_FM_Cloud_Loaded_Activity(refMissionIdData) != NO_CHARACTER)
	AND NOT (TEMP_Check_If_Heist_Using_Mission_Subtype(refMissionIdData))
	AND NOT (TEMP_Check_If_Heist_Planning_Using_Mission_Subtype(refMissionIdData))
		RETURN TRUE
	ENDIF
	RETURN FALSE	// TEMP: Anything else isn't a contact mission
//	IF (refMissionIdData.idMission != eFM_MISSION_CLOUD)
//		RETURN FALSE
//	ENDIF
//	RETURN (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) = FMMC_MISSION_TYPE_CONTACT)
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the activity is a Contact Mission.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this is a Contact Mission, FALSE if not
FUNC BOOL Is_FM_Cloud_Loaded_Activity_A_Contact_Mission(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL isContactMission	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		// KGM 30/7/14: Only use the subtype now
		RETURN (TEMP_Check_If_Contact_Mission_Using_Mission_Subtype(paramMissionIdData))
//		IF (g_b_HasEntered_ReducedContent)
//			RETURN (TEMP_Check_If_Contact_Mission_Using_Mission_Subtype(paramMissionIdData))
//		ENDIF
//		RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciMISSION_OPTION_BS_CONTACT_MISSION))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			// KGM 3/5/14: Just use the Subtype for Generated Data rather than the Bitset - hopefully we'll be able to ditch the bitset subtypes and rely fully on subtype INTs again
			RETURN (TEMP_Check_If_Contact_Mission_Using_Mission_Subtype(paramMissionIdData))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Contact_Mission(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (isContactMission)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (isContactMission)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_Contact_Mission_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_Contact_Mission_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_CONTACT_MISSION))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_Contact_Mission_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_Contact_Mission_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_CONTACT_MISSION))
			
		// MiniGames don't return a played status
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (isContactMission)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - Contact Missions aren't UGC
				RETURN (isContactMission)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Contact_Mission(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(isContactMission)
					NET_NL()
				#ENDIF
				
				RETURN (isContactMission)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Contact_Mission() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_Contact_Mission: ERROR - Unknown Creator ID. See Console Log. Returning FALSE. Tell Keith")
	#ENDIF
	
	RETURN (isContactMission)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	When using offline content check if this is a Random Event based on MissionID and MissionSubtype
// This will be superseded by the bit in teh bitfield being set
FUNC BOOL TEMP_Check_If_Random_Event_Using_Mission_Subtype(MP_MISSION_ID_DATA &refMissionIdData)
	IF (refMissionIdData.idMission != eFM_MISSION_CLOUD)
		RETURN FALSE
	ENDIF
	RETURN (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) = FMMC_MISSION_TYPE_RANDOM)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the activity is a Contact Mission.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this is a Random Event, FALSE if not
FUNC BOOL Is_FM_Cloud_Loaded_Activity_A_Random_Event(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL isRandomEvent	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		// KGM 30/7/14: Only use the subtype now
		RETURN (TEMP_Check_If_Random_Event_Using_Mission_Subtype(paramMissionIdData))
//		IF (g_b_HasEntered_ReducedContent)
//			RETURN (TEMP_Check_If_Random_Event_Using_Mission_Subtype(paramMissionIdData))
//		ENDIF
//		RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciMISSION_OPTION_BS_RANDOM_EVENT))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			// KGM 30/7/14: Only use the subtype now
			RETURN (TEMP_Check_If_Random_Event_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_Random_Event_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet, ciMISSION_OPTION_BS_RANDOM_EVENT))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Random_Event(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (isRandomEvent)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (isRandomEvent)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_Random_Event_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_Random_Event_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_RANDOM_EVENT))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_Random_Event_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_Random_Event_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_RANDOM_EVENT))
			
		// MiniGames don't return a played status
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (isRandomEvent)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - Random Events aren't UGC
				RETURN (isRandomEvent)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Random_Event(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(isRandomEvent)
					NET_NL()
				#ENDIF
				
				RETURN (isRandomEvent)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Random_Event() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_Random_Event: ERROR - Unknown Creator ID. See Console Log. Returning FALSE. Tell Keith")
	#ENDIF
	
	RETURN (isRandomEvent)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	When using offline content check if this is a Versus Mission based on MissionID and MissionSubtype
// This will be superseded by the bit in teh bitfield being set
FUNC BOOL TEMP_Check_If_Versus_Mission_Using_Mission_Subtype(MP_MISSION_ID_DATA &refMissionIdData)
	IF (refMissionIdData.idMission != eFM_MISSION_CLOUD)
		RETURN FALSE
	ENDIF
	RETURN (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) = FMMC_MISSION_TYPE_VERSUS)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	When using offline content check if this is a Coop Mission based on MissionID and MissionSubtype
// This will be superseded by the bit in teh bitfield being set
FUNC BOOL TEMP_Check_If_Coop_Mission_Using_Mission_Subtype(MP_MISSION_ID_DATA &refMissionIdData)
	IF (refMissionIdData.idMission != eFM_MISSION_CLOUD)
		RETURN FALSE
	ENDIF
	RETURN (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) = FMMC_MISSION_TYPE_COOP)
ENDFUNC
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the activity is a Versus Mission.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this is a Versus Mission, FALSE if not
FUNC BOOL Is_FM_Cloud_Loaded_Activity_A_Versus_Mission(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL isVersusMission	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		// KGM 30/7/14: Only use the subtype now
		RETURN (TEMP_Check_If_Versus_Mission_Using_Mission_Subtype(paramMissionIdData))
//		IF (g_b_HasEntered_ReducedContent)
//			RETURN (TEMP_Check_If_Versus_Mission_Using_Mission_Subtype(paramMissionIdData))
//		ENDIF
//		RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			// KGM 30/7/14: Only use the subtype now
			RETURN (TEMP_Check_If_Versus_Mission_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_Versus_Mission_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Random_Event(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (isVersusMission)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (isVersusMission)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_Versus_Mission_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_Versus_Mission_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_Versus_Mission_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_Versus_Mission_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS))
			
		// MiniGames don't return a played status
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (isVersusMission)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC - Versus missions aren't UGC
				RETURN (isVersusMission)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Random_Event(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(isVersusMission)
					NET_NL()
				#ENDIF
				
				RETURN (isVersusMission)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_Random_Event() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_Random_Event: ERROR - Unknown Creator ID. See Console Log. Returning FALSE. Tell Keith")
	#ENDIF
	
	RETURN (isVersusMission)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	When using offline content check if this is a LTS (Last Team Standing) Mission based on MissionID and MissionSubtype
// This will be superseded by the bit in teh bitfield being set
FUNC BOOL TEMP_Check_If_LTS_Mission_Using_Mission_Subtype(MP_MISSION_ID_DATA &refMissionIdData)
	IF (refMissionIdData.idMission != eFM_MISSION_CLOUD)
		RETURN FALSE
	ENDIF
	RETURN (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) = FMMC_MISSION_TYPE_LTS)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the activity is a LTS Mission.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this is a LTS Mission, FALSE if not
FUNC BOOL Is_FM_Cloud_Loaded_Activity_A_LTS_Mission(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL isLTSMission	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		// KGM 30/7/14: Only use the subtype now
		RETURN (TEMP_Check_If_LTS_Mission_Using_Mission_Subtype(paramMissionIdData))
//		IF (g_b_HasEntered_ReducedContent)
//			RETURN (TEMP_Check_If_LTS_Mission_Using_Mission_Subtype(paramMissionIdData))
//		ENDIF
//		RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciMISSION_OPTION_BS_LTS))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			// KGM 30/7/14: Only use the subtype now
			RETURN (TEMP_Check_If_LTS_Mission_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_LTS_Mission_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet, ciMISSION_OPTION_BS_LTS))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_LTS_Mission(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (isLTSMission)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (isLTSMission)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_LTS_Mission_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_LTS_Mission_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_LTS))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_LTS_Mission_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_LTS_Mission_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_LTS))
			
		// MiniGames don't return a played status
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (isLTSMission)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				IF (g_b_HasEntered_ReducedContent)
					RETURN (TEMP_Check_If_LTS_Mission_Using_Mission_Subtype(paramMissionIdData))
				ENDIF
				RETURN (IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_LTS))
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_LTS_Mission(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(isLTSMission)
					NET_NL()
				#ENDIF
				
				RETURN (isLTSMission)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_LTS_Mission() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_LTS_Mission: ERROR - Unknown Creator ID. See Console Log. Returning FALSE. Tell Keith")
	#ENDIF
	
	RETURN (isLTSMission)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	When using offline content check if this is a CTF (Capture the Flag) Mission based on MissionID and MissionSubtype
// This will be superseded by the bit in the bitfield being set
FUNC BOOL TEMP_Check_If_CTF_Mission_Using_Mission_Subtype(MP_MISSION_ID_DATA &refMissionIdData)
	IF (refMissionIdData.idMission != eFM_MISSION_CLOUD)
		RETURN FALSE
	ENDIF
	RETURN (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) = FMMC_MISSION_TYPE_CTF)
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the activity is a CTF Mission.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this is a CTF Mission, FALSE if not
FUNC BOOL Is_FM_Cloud_Loaded_Activity_A_CTF_Mission(MP_MISSION_ID_DATA &paramMissionIdData)

	BOOL isCTFMission	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		// KGM: 25/3/14: Using Subtype rather than bitfield because generated data doesn't generate the bitfield data
		// NOTE: This is to fix 1793161 where playing another player's UGC is using Generated data and is being treated as a generic mission rather than a Capture
		//IF (g_b_HasEntered_ReducedContent)
		RETURN (TEMP_Check_If_CTF_Mission_Using_Mission_Subtype(paramMissionIdData))
		//ENDIF
		//RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iBitSet, ciMISSION_OPTION_BS_CTF))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			// KGM: 25/3/14: Using Subtype rather than bitfield because generated data doesn't generate the bitfield data
			//IF (g_b_HasEntered_ReducedContent)
			RETURN (TEMP_Check_If_CTF_Mission_Using_Mission_Subtype(paramMissionIdData))
			//ENDIF
			//RETURN (IS_BIT_SET(g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet, ciMISSION_OPTION_BS_CTF))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_CTF_Mission(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (isCTFMission)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (isCTFMission)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_CTF_Mission_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_CTF_Mission_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_CTF))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_CTF_Mission_Using_Mission_Subtype(paramMissionIdData))
//			IF (g_b_HasEntered_ReducedContent)
//				RETURN (TEMP_Check_If_CTF_Mission_Using_Mission_Subtype(paramMissionIdData))
//			ENDIF
//			RETURN (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_CTF))
			
		// MiniGames don't return a played status
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (isCTFMission)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				// KGM: 25/3/14: Using Subtype rather than bitfield because generated data doesn't generate the bitfield data
				//IF (g_b_HasEntered_ReducedContent)
					RETURN (TEMP_Check_If_CTF_Mission_Using_Mission_Subtype(paramMissionIdData))
				//ENDIF
				//RETURN (IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[arrayPos].iBitSet, ciMISSION_OPTION_BS_CTF))
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_CTF_Mission(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(isCTFMission)
					NET_NL()
				#ENDIF
				
				RETURN (isCTFMission)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_CTF_Mission() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_CTF_Mission: ERROR - Unknown Creator ID. See Console Log. Returning FALSE. Tell Keith")
	#ENDIF
	
	RETURN (isCTFMission)

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

FUNC BOOL TEMP_Check_If_King_of_the_Hill_Using_Mission_Subtype(MP_MISSION_ID_DATA &refMissionIdData, BOOL &bIsTeam)
	IF (refMissionIdData.idMission != eFM_DEATHMATCH_CLOUD)
		RETURN FALSE
	ENDIF
	IF (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) = FMMC_KING_OF_THE_HILL_TEAM)
		bIsTeam = TRUE
		RETURN TRUE
	ENDIF
	bIsTeam = FALSE
	RETURN (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) = FMMC_KING_OF_THE_HILL)
ENDFUNC

// PURPOSE:	Check if the activity is a King of the Hill.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			BOOL						TRUE if this is a CTF Mission, FALSE if not
FUNC BOOL Is_FM_Cloud_Loaded_Activity_A_King_Of_The_Hill(MP_MISSION_ID_DATA &paramMissionIdData, BOOL &bIsTeam)

	BOOL isKotH	= FALSE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		// KGM: 25/3/14: Using Subtype rather than bitfield because generated data doesn't generate the bitfield data
		// NOTE: This is to fix 1793161 where playing another player's UGC is using Generated data and is being treated as a generic mission rather than a Capture
		RETURN (TEMP_Check_If_King_of_the_Hill_Using_Mission_Subtype(paramMissionIdData, bIsTeam))
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			// KGM: 25/3/14: Using Subtype rather than bitfield because generated data doesn't generate the bitfield data
			RETURN (TEMP_Check_If_King_of_the_Hill_Using_Mission_Subtype(paramMissionIdData, bIsTeam))
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_King_Of_The_Hill(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (isKotH)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (isKotH)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_King_of_the_Hill_Using_Mission_Subtype(paramMissionIdData, bIsTeam))
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// KGM 3/6/15: Only use the subtype now, ignore the bitset
			RETURN (TEMP_Check_If_King_of_the_Hill_Using_Mission_Subtype(paramMissionIdData, bIsTeam))
			
		// MiniGames don't return a played status
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (isKotH)
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				// KGM: 25/3/14: Using Subtype rather than bitfield because generated data doesn't generate the bitfield data
				RETURN (TEMP_Check_If_King_of_the_Hill_Using_Mission_Subtype(paramMissionIdData, bIsTeam))
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_King_Of_The_Hill(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_BOOL(isKotH)
					NET_NL()
				#ENDIF
				
				RETURN (isKotH)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Is_FM_Cloud_Loaded_Activity_A_King_Of_The_Hill() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Is_FM_Cloud_Loaded_Activity_A_King_Of_The_Hill: ERROR - Unknown Creator ID. See Console Log. Returning FALSE. Tell Keith")
	#ENDIF
	
	RETURN (isKotH)

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the On Hours for the specified activity.
//
// RETURN PARAMS:			paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT							A Bitfield where the first 24 bits represent the hours - if the hour is set then the mission is available.
FUNC INT Get_On_Hours_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &paramMissionIdData)

	INT onHoursToReturn	= MATC_ALL_DAY

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(paramMissionIdData))
		RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramMissionIdData.idSharedRegID].mscHeaderData.iLaunchTimesBit)
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	IF (Is_This_Generated_Cloud_Header_Data(paramMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(paramMissionIdData))
			RETURN (g_sGeneratedCloudHeaderData.gchdHeaderData.iLaunchTimesBit)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Get_On_Hours_For_FM_Cloud_Loaded_Activity(): The passed-in data doesn't match the stored Generated Cloud Header Data") NET_NL()
		#ENDIF
	
		RETURN (onHoursToReturn)
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(paramMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN (onHoursToReturn)
	ENDIF
	
	// Return the correct data
	SWITCH (paramMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			// KGMTEMPHOURS: Until the real data is available
			IF ((g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iLaunchTimesBit & MATC_ALL_DAY) = 0)
				g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iLaunchTimesBit = MATC_ALL_DAY
			ENDIF
			// END KGMTEMPHOURS
			RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[arrayPos].iLaunchTimesBit)
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			// KGMTEMPHOURS: Until the real data is available
			IF ((g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iLaunchTimesBit & MATC_ALL_DAY) = 0)
				g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iLaunchTimesBit = MATC_ALL_DAY
			ENDIF
			// END KGMTEMPHOURS
			RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[arrayPos].iLaunchTimesBit)
			
		// MiniGames don't return a subtype
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN (onHoursToReturn)
			
		DEFAULT
			// UGC doesn't have a launch times bitset
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC
				RETURN (onHoursToReturn)
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other Player's UGC - so the data won't be on a local stored array
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MissionInfo]: Get_On_Hours_For_FM_Cloud_Loaded_Activity(): Can't retrieve UGC data for another player [")
					NET_PRINT(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(paramMissionIdData.idCreator)))
					NET_PRINT("] - RETURNING ")
					NET_PRINT_INT(onHoursToReturn)
					NET_NL()
				#ENDIF
				
				RETURN (onHoursToReturn)
			ENDIF
			BREAK
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_On_Hours_For_FM_Cloud_Loaded_Activity() ERROR - Unknown Creator ID. Add it to the SWITCH statement.")
		NET_NL()
		NET_PRINT("          Passed in CreatorID for Cloud Global Array to use: ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_NL()
		
		SCRIPT_ASSERT("Get_On_Hours_For_FM_Cloud_Loaded_Activity: ERROR - Unknown Creator ID. See Console Log. Returning 'MATC_ALL_DAY'. Tell Keith")
	#ENDIF
	
	RETURN (onHoursToReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the creator is Rockstar
//
// INPUT PARAMS:			paramCreatorID			The CreatorID
// RETURN VALUE:			BOOL					TRUE if the creator is Rockstar, otherwise FALSE
FUNC BOOL Is_CreatorID_Rockstar(INT paramCreatorID)

	// Check for a Rockstar Creator ID
	SWITCH (paramCreatorID)
		CASE FMMC_ROCKSTAR_CREATOR_ID
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
		CASE FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID
		CASE FMMC_MINI_GAME_CREATOR_ID
			// For Rockstar Created Missions, return 'Rockstar' as a literal string (so do a converstion from a text label)
			RETURN TRUE
	ENDSWITCH
	
	// Not Rockstar
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get a literal string version of the Creator Name.
//
// REFERENCE PARAMS:		refMissionIdData		The Mission Id Data
// RETURN VALUE:			STRING					The Creator Name String
FUNC STRING Convert_CreatorID_To_Creator_Name_For_FM_Cloud_Loaded_Activity(MP_MISSION_ID_DATA &refMissionIdData)

	// Check for a Rockstar Creator ID
	IF (Is_CreatorID_Rockstar(refMissionIdData.idCreator))
		// For Rockstar Created Missions, return 'Rockstar' as a literal string (so do a converstion from a text label)
		RETURN (GET_FILENAME_FOR_AUDIO_CONVERSATION("FM_LOC_ROCKS"))
	ENDIF
	
	// Check for an existing player
	IF (refMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
	AND NOT (Is_This_Generated_Cloud_Header_Data(refMissionIdData.idVariation))
		PLAYER_INDEX thisPlayer = INT_TO_PLAYERINDEX(refMissionIdData.idCreator)
		IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
			RETURN (GET_PLAYER_NAME(thisPlayer))
		ENDIF
	ENDIF
	
	// For anything else, use the cloud-loaded user name
	TEXT_LABEL_23	theUserName		= Get_User_Name_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	STRING			theReturnString	= ""
	
	// If the user name is NULL or empty, return a space to ensure we don't get asserts matching ~a~
	IF (IS_STRING_NULL_OR_EMPTY(theUserName))
		theReturnString = " "
	ELSE
		theReturnString = Get_String_From_TextLabel(theUserName)
	ENDIF
	
	RETURN (theReturnString)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

//Get the correct race blip from the race sub type.
FUNC BLIP_SPRITE GET_FM_RACE_BLIP_TYPE_FROM_SUB_TYPE(INT iRaceSubType)

	SWITCH iRaceSubType
		CASE FMMC_RACE_TYPE_STANDARD
		CASE FMMC_RACE_TYPE_P2P
			RETURN RADAR_TRACE_RACE_LAND
			
		CASE FMMC_RACE_TYPE_STUNT
		CASE FMMC_RACE_TYPE_STUNT_P2P
			RETURN RADAR_TRACE_RACE_STUNT
			
		CASE FMMC_RACE_TYPE_TARGET
		CASE FMMC_RACE_TYPE_TARGET_P2P
			RETURN RADAR_TRACE_ACSR_RACE_TARGET
			
		CASE FMMC_RACE_TYPE_ON_FOOT
		CASE FMMC_RACE_TYPE_ON_FOOT_P2P
			RETURN RADAR_TRACE_RACE_FOOT
			
		CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE		
		CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE_P2P
			RETURN RADAR_TRACE_RACE_BIKE
			
		CASE FMMC_RACE_TYPE_BOAT
		CASE FMMC_RACE_TYPE_BOAT_P2P
			RETURN RADAR_TRACE_RACE_SEA
			
		CASE FMMC_RACE_TYPE_AIR
		CASE FMMC_RACE_TYPE_AIR_P2P
			RETURN RADAR_TRACE_RACE_AIR
			
		CASE FMMC_RACE_TYPE_ARCADE_RACE
		CASE FMMC_RACE_TYPE_ARCADE_RACE_P2P
		CASE FMMC_RACE_TYPE_DESTRUCTION_DERBY
		CASE FMMC_RACE_TYPE_DESTRUCTION_DERBY_P2P
			RETURN RADAR_TRACE_RACE_WARS
			
		CASE FMMC_RACE_TYPE_OPEN_WHEEL		
		CASE FMMC_RACE_TYPE_OPEN_WHEEL_P2P
			RETURN RADAR_TRACE_RACE_OPEN_WHEEL
			
		CASE FMMC_RACE_TYPE_PURSUIT
			RETURN RADAR_TRACE_PURSUIT_SERIES
			
		CASE FMMC_RACE_TYPE_STREET
			RETURN RADAR_TRACE_STREET_RACE_SERIES
			
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE FMMC_RACE_TYPE_HSW
		CASE FMMC_RACE_TYPE_HSW_P2P
			RETURN RADAR_TRACE_HSW_RACE_SERIES
		#ENDIF
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: GET_FM_RACE_BLIP_TYPE_FROM_SUB_TYPE() - ERROR: Unknown Race Subtype: ") NET_PRINT_INT(iRaceSubType) NET_NL()
		SCRIPT_ASSERT("GET_FM_RACE_BLIP_TYPE_FROM_SUB_TYPE(): ERROR - Unknown Race Subtype. Add to switch statement. Returning RADAR_TRACE_RACE. Tell Keith or Bobby.")
	#ENDIF
	
	RETURN RADAR_TRACE_RACE
	
ENDFUNC


//Get the correct Mission blip from the Mission sub type.
FUNC BLIP_SPRITE GET_FM_MISSION_BLIP_TYPE_FROM_SUB_TYPE(INT paramSubtype, INT iStrand = 0)

	SWITCH (paramSubtype)
		CASE FMMC_MISSION_TYPE_HEIST
	        RETURN RADAR_TRACE_HEIST
			
		CASE FMMC_MISSION_TYPE_PLANNING
	        RETURN RADAR_TRACE_HEIST 	// HEIST PLANNING, also return 'HEIST'
			
		CASE FMMC_MISSION_TYPE_LTS
			RETURN RADAR_TRACE_LAST_TEAM_STANDING
			
		CASE FMMC_MISSION_TYPE_CTF
			RETURN RADAR_TRACE_CAPTURE_THE_FLAG
			
		CASE FMMC_MISSION_TYPE_CONTACT
		CASE FMMC_MISSION_TYPE_RANDOM
		CASE FMMC_MISSION_TYPE_STANDARD
		CASE FMMC_MISSION_TYPE_VERSUS
		CASE FMMC_MISSION_TYPE_COOP
		CASE FMMC_TYPE_MISSION_NEW_VS
			// ...the others
			RETURN RADAR_TRACE_UGC_MISSION
			
		CASE FMMC_MISSION_TYPE_FLOW_MISSION
			IF iStrand = ciFLOW_STRAND_LOW_RIDER
				RETURN RADAR_TRACE_MP_LAMAR
			ELSE
				RETURN RADAR_TRACE_UGC_MISSION
  			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: GET_FM_MISSION_BLIP_TYPE_FROM_SUB_TYPE() - ERROR: Unknown Mission Subtype: ") NET_PRINT_INT(paramSubtype) NET_NL()
		SCRIPT_ASSERT("GET_FM_MISSION_BLIP_TYPE_FROM_SUB_TYPE(): ERROR - Unknown Mission Subtype. Add to switch statement. Returning RADAR_TRACE_UGC_MISSION. Tell Keith or Bobby.")
	#ENDIF
	
	RETURN RADAR_TRACE_UGC_MISSION
	
ENDFUNC

FUNC TEXT_LABEL_23 Get_FM_Mission_Blip_Name_From_Subtype(INT paramSubtype, BOOL paramIsRockstar, BOOL paramIsBookmarked = FALSE, INT iStrand = 0)
	
	TEXT_LABEL_23 theReturn = ""
	
	SWITCH (paramSubtype)
		CASE FMMC_MISSION_TYPE_HEIST
	        theReturn = "FMMC_RSTAR_MHST"
			RETURN (theReturn)
			
		CASE FMMC_MISSION_TYPE_PLANNING
	        theReturn = "FMMC_RSTAR_HP" // HEIST PLANNING, also return 'HEIST'
			RETURN (theReturn)
			
		CASE FMMC_MISSION_TYPE_LTS
			// KGM 19/3/14: Until the LTS creator is ready, treat non-Rockstar as Rockstar so that LTS Creator text isn't released prematurely to the public
			IF (paramIsBookmarked)
		        theReturn =  "FMMC_BM_LTS"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				// Treat non-Rockstar as Rockstar for now
				theReturn =  "FMMC_RSTAR_MLTS"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_LTS"
			RETURN (theReturn)
		CASE FMMC_MISSION_TYPE_CTF
			IF (paramIsBookmarked)
		        theReturn =  "FMMC_BM_CTF"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
		        theReturn = "FMMC_RSTAR_MCTF"
				RETURN (theReturn)
			ENDIF
			theReturn = "FMMC_PL_CTF"
			RETURN (theReturn)
			
		CASE FMMC_MISSION_TYPE_FLOW_MISSION
			IF iStrand = ciFLOW_STRAND_LOW_RIDER
				theReturn = "FMMC_RSTAR_MSL"
			ELSE
				theReturn =  "FMMC_RSTAR_MS"
  			ENDIF	
			RETURN (theReturn)
			
		CASE FMMC_TYPE_MISSION_NEW_VS
			theReturn = "FMMC_RSTAR_MSA"
			RETURN (theReturn)
			
		CASE FMMC_MISSION_TYPE_CONTACT
		CASE FMMC_MISSION_TYPE_RANDOM
		CASE FMMC_MISSION_TYPE_STANDARD
		CASE FMMC_MISSION_TYPE_VERSUS
		CASE FMMC_MISSION_TYPE_COOP
			// ...the others
			// NOTE: If  it isn't Bookmarked then treat as 'isRockstar' regardless of the passed-in parameter
			// 			This will need addressed when the public is able to create other types of missions
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_MISS"
			ELSE
				theReturn = "FMMC_RSTAR_MS"
			ENDIF
			RETURN (theReturn)
  	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Mission_Blip_Name_From_Subtype() - ERROR: Unknown Mission Subtype: ") NET_PRINT_INT(paramSubtype) NET_NL()
		SCRIPT_ASSERT("Get_FM_Mission_Blip_Name_From_Subtype(): ERROR - Unknown Mission Subtype. Add to switch statement. Returning MISSION. Tell Keith or Bobby.")
	#ENDIF
	
	theReturn = "FMMC_RSTAR_MS"
	RETURN (theReturn)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To get racetype specific blip names allowing an inserted string if not Rockstar
//
// INPUT PARAMS:		paramSubtype		The Race subtype
//						paramIsRockstar		TRUE for Rockstar Blip Names, FALSE for blip names that allow another string to be inserted
//						paramIsBookmarked	TRUE for a Bookmarked Blip Name, otherwise FALSE
// RETURN VALUE:		TEXT_LABEL_23		The text label containing the names
FUNC TEXT_LABEL_23 Get_FM_Race_Blip_Name_From_Subtype(INT paramSubtype, BOOL paramIsRockstar, BOOL paramIsBookmarked = FALSE)

	TEXT_LABEL_23 theReturn = ""

	SWITCH (paramSubtype)
		CASE FMMC_RACE_TYPE_STANDARD
		CASE FMMC_RACE_TYPE_P2P
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_LR"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn = "FMMC_RSTAR_LR"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_LR"
			RETURN (theReturn)
			
		CASE FMMC_RACE_TYPE_ON_FOOT
		CASE FMMC_RACE_TYPE_ON_FOOT_P2P
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_OFR"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn =  "FMMC_RSTAR_OFR"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_OFR"
			RETURN (theReturn)
			
		CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE		
		CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE_P2P
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_BR"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn =  "FMMC_RSTAR_BR"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_BR"
			RETURN (theReturn)
		
		CASE FMMC_RACE_TYPE_STUNT
		CASE FMMC_RACE_TYPE_STUNT_P2P
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_SR"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn =  "FMMC_RSTAR_STR"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_SR"
			RETURN (theReturn)
			
		CASE FMMC_RACE_TYPE_TARGET
		CASE FMMC_RACE_TYPE_TARGET_P2P
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_TAR"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn =  "FMMC_RSTAR_TAR"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_TAR"
			RETURN (theReturn)
			
		CASE FMMC_RACE_TYPE_BOAT
		CASE FMMC_RACE_TYPE_BOAT_P2P
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_WR"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn =  "FMMC_RSTAR_WR"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_WR"
			RETURN (theReturn)
			
		CASE FMMC_RACE_TYPE_AIR
		CASE FMMC_RACE_TYPE_AIR_P2P
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_AR"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn =  "FMMC_RSTAR_AR"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_AR"
			RETURN (theReturn)
			
		CASE FMMC_RACE_TYPE_ARCADE_RACE
		CASE FMMC_RACE_TYPE_ARCADE_RACE_P2P
		CASE FMMC_RACE_TYPE_DESTRUCTION_DERBY
		CASE FMMC_RACE_TYPE_DESTRUCTION_DERBY_P2P
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_AW"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn =  "FMMC_RSTARAWR"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_AW"
			RETURN (theReturn)
			
		CASE FMMC_RACE_TYPE_OPEN_WHEEL
		CASE FMMC_RACE_TYPE_OPEN_WHEEL_P2P
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_OW"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn = "FMMC_RSTAR_OW"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_OW"
			RETURN (theReturn)
			
		CASE FMMC_RACE_TYPE_PURSUIT
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_PU"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn = "FMMC_RSTAR_PU"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_PU"
			RETURN (theReturn)
			
		CASE FMMC_RACE_TYPE_STREET
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_ST"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn = "FMMC_RSTAR_ST"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_ST"
			RETURN (theReturn)
			
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE FMMC_RACE_TYPE_HSW
		CASE FMMC_RACE_TYPE_HSW_P2P
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_HSW"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn = "FMMC_RSTAR_HSW"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_HSW"
			RETURN (theReturn)
		#ENDIF
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Race_Blip_Name_From_Subtype() - ERROR: Unknown Race Subtype: ") NET_PRINT_INT(paramSubtype) NET_NL()
		SCRIPT_ASSERT("Get_FM_Race_Blip_Name_From_Subtype(): ERROR - Unknown Race Subtype. Add to switch statement. Returning RACE.")
	#ENDIF
	
	theReturn = "FMMC_RSTAR_LR"
	RETURN (theReturn)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// Get the correct deathmatch blip from the deathmatch sub type.
//
// INPUT PARAMS:		paramSubtype			The Deathmatch Subtype
// RETURN VALUE:		BLIP_SPRITE				The Deathmatch Blip Sprite
FUNC BLIP_SPRITE Get_FM_Deathmatch_Blip_From_Subtype(INT paramSubtype)

	SWITCH paramSubtype
		CASE FMMC_DEATHMATCH_STANDARD
			RETURN RADAR_TRACE_DEATHMATCH
			
		CASE FMMC_DEATHMATCH_TEAM
			RETURN RADAR_TRACE_TEAM_DEATHMATCH
			
		CASE FMMC_DEATHMATCH_VEHICLE
			RETURN RADAR_TRACE_VEHICLE_DEATHMATCH
			
		CASE FMMC_KING_OF_THE_HILL
			RETURN RADAR_TRACE_KING_OF_THE_HILL
			
		CASE FMMC_KING_OF_THE_HILL_TEAM
			RETURN RADAR_TRACE_KING_OF_THE_HILL_TEAMS
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Deathmatch_Blip_From_Subtype() - ERROR: Unknown Deathmatch Subtype: ") NET_PRINT_INT(paramSubtype) NET_NL()
		SCRIPT_ASSERT("Get_FM_Deathmatch_Blip_From_Subtype(): ERROR - Unknown Deathmatch Subtype. Add to switch statement. Returning RADAR_TRACE_DEATHMATCH. Tell Keith or Bobby.")
	#ENDIF
	
	RETURN RADAR_TRACE_DEATHMATCH
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To get deathmatch type specific blip names allowing an inserted string if not Rockstar
//
// INPUT PARAMS:		paramSubtype		The Deathmatch subtype
//						paramIsRockstar		TRUE for Rockstar Blip Names, FALSE for blip names that allow another string to be inserted
//						paramIsBookmarked	TRUE for a Bookmarked Blip Name, otherwise FALSE
// RETURN VALUE:		TEXT_LABEL_23		The text label containing the names
FUNC TEXT_LABEL_23 Get_FM_Deathmatch_Blip_Name_From_Subtype(INT paramSubtype, BOOL paramIsRockstar, BOOL paramIsBookmarked = FALSE)

	TEXT_LABEL_23 theReturn = ""

	SWITCH (paramSubtype)
		CASE FMMC_DEATHMATCH_STANDARD
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_DM"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn = "FMMC_RSTAR_DM"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_DM"
			RETURN (theReturn)
			
		CASE FMMC_DEATHMATCH_TEAM
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_TDM"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn =  "FMMC_RSTAR_TDM"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_TDM"
			RETURN (theReturn)
			
		CASE FMMC_DEATHMATCH_VEHICLE
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_VDM"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn =  "FMMC_RSTAR_VDM"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_VDM"
			RETURN (theReturn)
		
		CASE FMMC_KING_OF_THE_HILL
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_KOTH"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn = "FMMC_RSTAR_KOTH"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_KOTH"
			RETURN (theReturn)
			
		CASE FMMC_KING_OF_THE_HILL_TEAM
			IF (paramIsBookmarked)
				theReturn = "FMMC_BM_TKOTH"
				RETURN (theReturn)
			ENDIF
			IF (paramIsRockstar)
				theReturn = "FMMC_RSTR_TKOTH"
				RETURN (theReturn)
			ENDIF
			theReturn =  "FMMC_PL_TKOTH"
			RETURN (theReturn)
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Deathmatch_Blip_Name_From_Subtype() - ERROR: Unknown DM Subtype: ") NET_PRINT_INT(paramSubtype) NET_NL()
		SCRIPT_ASSERT("Get_FM_Deathmatch_Blip_Name_From_Subtype(): ERROR - Unknown DM Subtype. Add to switch statement. Returning DEATHMATCH. Tell Keith or Bobby.")
	#ENDIF
	
	theReturn = "FMMC_RSTAR_DM"
	RETURN (theReturn)
	
ENDFUNC


FUNC BLIP_SPRITE LOW_FLOW_GET_BLIP_SPRITE_FROM_FLOW_POS(INT iStrand, INT iMission)
	SWITCH iStrand
		CASE ciFLOW_STRAND_LOW_RIDER
			SWITCH iMission
				CASE ciLOW_FLOW_MISSION_DRIVE_BY_SHOOTOUT		RETURN RADAR_TRACE_CORNER_NUMBER_1
				CASE ciLOW_FLOW_MISSION_TRANSPORT_LOWRIDERS 	RETURN RADAR_TRACE_CORNER_NUMBER_2
				CASE ciLOW_FLOW_MISSION_LOWRIDERS_RESCUE	 	RETURN RADAR_TRACE_CORNER_NUMBER_3
				CASE ciLOW_FLOW_MISSION_FUNERAL_SHOOTOUT		RETURN RADAR_TRACE_CORNER_NUMBER_4
				CASE ciLOW_FLOW_MISSION_BLOW_UP_LOWRIDERS		RETURN RADAR_TRACE_CORNER_NUMBER_5
				CASE ciLOW_FLOW_MISSION_PHOTO_CARS				RETURN RADAR_TRACE_CORNER_NUMBER_6
				CASE ciLOW_FLOW_MISSION_LOWRIDER_ASSASSINATION	RETURN RADAR_TRACE_CORNER_NUMBER_7
				CASE ciLOW_FLOW_MISSION_LOWRIDER_FINALE			RETURN RADAR_TRACE_CORNER_NUMBER_8
			ENDSWITCH 
			PRINTLN("[LOWFLOW] LOW_FLOW_GET_BLIP_SPRITE_FROM_FLOW_POS iMission - iStrand = ", iStrand, " - iMission = ", iMission)
			SCRIPT_ASSERT("LOW_FLOW_GET_BLIP_SPRITE_FROM_FLOW_POS() - RADAR_TRACE_MP_LAMAR - unknown mission")
			RETURN RADAR_TRACE_MP_LAMAR
	ENDSWITCH 
	
	PRINTLN("[LOWFLOW] LOW_FLOW_GET_BLIP_SPRITE_FROM_FLOW_POS strand - iStrand = ", iStrand, " - iMission = ", iMission)
	SCRIPT_ASSERT("LOW_FLOW_GET_BLIP_SPRITE_FROM_FLOW_POS() - RADAR_TRACE_MP_LAMAR - unknown strand")
	RETURN RADAR_TRACE_MP_LAMAR
ENDFUNC

FUNC BLIP_SPRITE LOW_FLOW_OVER_RIDE_BLIP_SPRITE_BASED_ON_CONTENT_ID(INT iRootContentIdHash, BLIP_SPRITE bsReturn)	

	INT iLoop
	REPEAT ciMAX_ROCKSTAR_FLOW_MISSIONS iLoop
		//If it's one of the lowrider missions
		IF iRootContentIdHash = g_sMPTunables.iLowFlowMission[iLoop]
		OR iRootContentIdHash = GET_HASH_KEY(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[g_FMMC_ROCKSTAR_CREATED.sFlowMissionDetails[ciFLOW_STRAND_LOW_RIDER].iRockstarArrayMissionPos[iLoop]].tlName)
			RETURN LOW_FLOW_GET_BLIP_SPRITE_FROM_FLOW_POS(ciFLOW_STRAND_LOW_RIDER, iLoop)
		ENDIF
	ENDREPEAT
	RETURN bsReturn
ENDFUNC

// ===========================================================================================================
//      TEMPORARY FUNCTIONS TO GATHER FM BLIP AND CORONA DETAILS
// ===========================================================================================================

// PURPOSE:	Get the blip specifics for this activity
//
// RETURN PARAMS:		paramMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
//						paramBlipSprite			The Blip Sprite
//						paramBlipPriority		The Blip Priority
//						paramBlipName			The Blip Name (or "" if using default)
//						paramCreatorName		The Creator Name (or "" if using default)
//						paramBlipColour			The Blip Colour
//						paramFromCreator		TRUE if this is a mission creator blip, otherwise FALSE
//
// NOTES:	KGM TEMP FUNCTION: Should become obsolete when these sprite details are stored as Mission Data
PROC TEMP_Get_FM_Activity_Blip_Details(	MP_MISSION_ID_DATA		&paramMissionIdData,
										BLIP_SPRITE				&paramBlipSprite,
										BLIP_PRIORITY			&paramBlipPriority,
										TEXT_LABEL_23			&paramBlipName,
										TEXT_LABEL_63			&paramCreatorName,
										INT						&paramBlipColour,
										BOOL					&paramFromCreator, 
										BOOL					&paramForceLongRange)

	// Setup some defaults
	paramBlipSprite		= RADAR_TRACE_NUMBERED_1
	paramBlipPriority	= BLIPPRIORITY_LOWEST
	paramBlipName		= ""
	paramCreatorName	= ""
	paramBlipColour		= BLIP_COLOUR_STEALTH_GREY
	paramFromCreator	= FALSE
	
	// Bookmarked activity?
	BOOL isBookmarked = FALSE
	IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
		// My UGC, so check if UGC or BOOKMARKED
		IF (Get_Bookmarked_Status_FM_Cloud_Loaded_Activity(paramMissionIdData))
			isBookmarked = TRUE
		ENDIF
	ENDIF
	
	// Standard Colour details
	SWITCH (paramMissionIdData.idCreator)
		// MiniGames
		CASE FMMC_MINI_GAME_CREATOR_ID
			paramBlipColour			= FM_MINIGAME_BLIP_COLOUR
			BREAK
			
		// Rockstar Created Activities
		CASE FMMC_ROCKSTAR_CREATOR_ID
			paramBlipColour 		= FM_ROCKSTAR_CREATED_BLIP_COLOUR
			BREAK
			
		// Rockstar Verified Activities
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			paramBlipColour 		= FM_ROCKSTAR_VERIFIED_BLIP_COLOUR
			BREAK
			
		DEFAULT
			IF (paramMissionIdData.idCreator = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC, so check if UGC or BOOKMARKED
				IF (Get_Bookmarked_Status_FM_Cloud_Loaded_Activity(paramMissionIdData))
					paramBlipColour = FM_BOOKMARKED_BLIP_COLOUR
				ELSE
					paramBlipColour = FM_MY_UGC_BLIP_COLOUR
				ENDIF
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				// Other UGC
				paramBlipColour = FM_OTHER_UGC_BLIP_COLOUR
			ENDIF
			BREAK
	ENDSWITCH

	// Other Blip Details
	SWITCH (paramMissionIdData.idMission)
		CASE eFM_ARM_WRESTLING
			paramBlipSprite			= RADAR_TRACE_ARM_WRESTLING
			paramBlipPriority		= BLIPPRIORITY_LOWEST
			EXIT
			
		CASE eFM_BASEJUMP_CLOUD
			IF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CREATOR_ID)
				paramBlipSprite		= RADAR_TRACE_BASE_JUMP
				paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				paramBlipName		= "FMMC_RSTAR_BJ"
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
				paramBlipSprite		= RADAR_TRACE_BASE_JUMP
				paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				paramBlipName		= "FMMC_RSTAR_BJ"
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				paramBlipSprite		= RADAR_TRACE_BASE_JUMP
				paramBlipPriority	= BLIPPRIORITY_LOW
				paramFromCreator	= TRUE
				IF (isBookmarked)
					paramBlipName		= "FMMC_BM_BASE"
				ELSE
					paramBlipName		= "FMMC_PL_BASE"
					paramCreatorName	= Convert_CreatorID_To_Creator_Name_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
				ENDIF
			ENDIF
			EXIT

		CASE eFM_DARTS
			paramBlipSprite			= RADAR_TRACE_DARTS
			paramBlipPriority		= BLIPPRIORITY_LOWEST
			EXIT

		CASE eFM_PILOT_SCHOOL
			paramBlipSprite			= RADAR_TRACE_FLIGHT_SCHOOL
			paramBlipPriority		= BLIPPRIORITY_LOWEST
			paramBlipName			= "HUD_MG_PILOT"
			EXIT
			
		CASE eFM_DEATHMATCH_CLOUD
			paramBlipSprite			= Get_FM_Deathmatch_Blip_From_Subtype(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData))
			IF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CREATOR_ID)
				paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				paramBlipName		= Get_FM_Deathmatch_Blip_Name_From_Subtype(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData), TRUE)
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
				paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				paramBlipName		= Get_FM_Deathmatch_Blip_Name_From_Subtype(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData), TRUE)
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				paramBlipPriority	= BLIPPRIORITY_LOW
				paramBlipName		= Get_FM_Deathmatch_Blip_Name_From_Subtype(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData), FALSE, isBookmarked)
				paramCreatorName	= Convert_CreatorID_To_Creator_Name_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
				paramFromCreator	= TRUE
			ENDIF
			EXIT
			
		// KGM 24/8/13: Gang Attacks use an invisible blip to get the tick oriented correctly, but the missionsAtCoords will automatically add an area-triggered radius blip
		CASE eFM_GANG_ATTACK_CLOUD
			paramBlipColour			= FM_GANG_ATTACK_BLIP_COLOUR
			IF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CREATOR_ID)
				paramBlipSprite		= RADAR_TRACE_INVISIBLE
				paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				paramBlipName		= "FMMC_RSTAR_GA"
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
				paramBlipSprite		= RADAR_TRACE_INVISIBLE
				paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				paramBlipName		= "FMMC_RSTAR_GA"
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				paramBlipSprite		= RADAR_TRACE_INVISIBLE
				paramBlipPriority	= BLIPPRIORITY_LOW
				paramFromCreator	= TRUE
				IF (isBookmarked)
					paramBlipName		= "FMMC_BM_GH"
				ELSE
					paramBlipName		= "FMMC_PL_GH"
					paramCreatorName	= Convert_CreatorID_To_Creator_Name_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
				ENDIF
			ENDIF
			EXIT
			
		CASE eFM_GOLF
			paramBlipSprite			= RADAR_TRACE_GOLF
			paramBlipPriority		= BLIPPRIORITY_LOWEST
			EXIT
			
		CASE eFM_MISSION_CLOUD
			paramBlipSprite			= GET_FM_MISSION_BLIP_TYPE_FROM_SUB_TYPE(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData))
			IF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CREATOR_ID)
				IF Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData) = FMMC_MISSION_TYPE_FLOW_MISSION
					paramBlipSprite 	= LOW_FLOW_OVER_RIDE_BLIP_SPRITE_BASED_ON_CONTENT_ID(GET_HASH_KEY(paramMissionIdData.idCloudFilename), paramBlipSprite)
					paramBlipColour 	= BLIP_COLOUR_HUDCOLOUR_YELLOW //HUD_COLOUR_LOW_FLOW BLIP_COLOUR_FRANKLIN
					paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
					paramForceLongRange = FALSE
				ELSE
					paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				ENDIF
				//paramBlipName		= "FMMC_RSTAR_MS"
				paramBlipName		= Get_FM_Mission_Blip_Name_From_Subtype(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData), TRUE)
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
				paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				//paramBlipName		= "FMMC_RSTAR_MS"
				paramBlipName		= Get_FM_Mission_Blip_Name_From_Subtype(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData), TRUE)
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				paramBlipPriority	= BLIPPRIORITY_LOW
				paramFromCreator	= TRUE
				paramCreatorName	= Convert_CreatorID_To_Creator_Name_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
				paramBlipName		= Get_FM_Mission_Blip_Name_From_Subtype(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData), FALSE, isBookmarked)
			ENDIF
			EXIT
			
		CASE eFM_RACE_CLOUD
			paramBlipSprite			= GET_FM_RACE_BLIP_TYPE_FROM_SUB_TYPE(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData))
			IF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CREATOR_ID)
				paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				paramBlipName		= Get_FM_Race_Blip_Name_From_Subtype(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData), TRUE)
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
				paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				paramBlipName		= Get_FM_Race_Blip_Name_From_Subtype(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData), TRUE)
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				paramBlipPriority	= BLIPPRIORITY_LOW
				paramBlipName		= Get_FM_Race_Blip_Name_From_Subtype(Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData), FALSE, isBookmarked)
				paramCreatorName	= Convert_CreatorID_To_Creator_Name_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
				paramFromCreator	= TRUE
			ENDIF
			EXIT
			
		CASE eFM_SHOOTING_RANGE
			paramBlipSprite			= RADAR_TRACE_SHOOTING_RANGE
			paramBlipPriority		= BLIPPRIORITY_LOWEST
			EXIT
			
		CASE eFM_SURVIVAL_CLOUD
			IF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CREATOR_ID)
				paramBlipSprite		= RADAR_TRACE_HORDE
				paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				paramBlipName		= "FMMC_RSTAR_HM"
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
				paramBlipSprite		= RADAR_TRACE_HORDE
				paramBlipPriority	= BLIPPRIORITY_LOW_LOWEST
				paramBlipName		= "FMMC_RSTAR_HM"
				paramFromCreator	= TRUE
			ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
				paramBlipSprite		= RADAR_TRACE_HORDE
				paramBlipPriority	= BLIPPRIORITY_LOW
				paramFromCreator	= TRUE
				IF (isBookmarked)
					paramBlipName		= "FMMC_BM_HM"
				ELSE
					paramBlipName		= "FMMC_PL_HM"
					paramCreatorName	= Convert_CreatorID_To_Creator_Name_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
				ENDIF
			ENDIF
			EXIT
			
		CASE eFM_TENNIS	
			paramBlipSprite			= RADAR_TRACE_TENNIS
			paramBlipPriority		= BLIPPRIORITY_LOWEST
			EXIT
	ENDSWITCH
	
	// Need to add new Blip Details for this activity
	NET_PRINT("...KGM MP [MissionInfo]: TEMP_Get_FM_Activity_Blip_Details - ERROR: Need to add details for this MP MISSION (as INT): ")
	NET_PRINT_INT(ENUM_TO_INT(paramMissionIdData.idMission))
	NET_NL()
	SCRIPT_ASSERT("TEMP_Get_FM_Activity_Blip_Details(): ERROR - Need to add blip details for FM Activity. See Console Log. Tell Keith.")
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Get the Corona Colour based on the CreatorID
//
// INPUT PARAMS:		paramCreatorID			The Creator ID - used to colour the coronas appropriately
//						paramIsBookmarked		TRUE if this is a bookmarked mission, otherwise FALSE
// RETURN VALUE:		HUD_COLOURS				The Corona Colour for this Creator
FUNC HUD_COLOURS Get_Corona_Colour_For_This_CreatorID(INT paramCreatorID, BOOL paramIsBookmarked)

	// KGM 5/1/13: Creator IDs will be added as they start to be referenced (currently only required for eFM_RACE_CLOUD)
	
	SWITCH (paramCreatorID)
		// MiniGames
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN FM_MINIGAME_CORONA_COLOUR
			
		// Rockstar Created
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN FM_ROCKSTAR_CREATED_CORONA_COLOUR
			
		// Rockstar Verified
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN FM_ROCKSTAR_VERIFIED_CORONA_COLOUR
			
		DEFAULT
			IF (paramCreatorID = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC, or BOOKMARKED
				IF (paramIsBookmarked)
					RETURN FM_BOOKMARKED_CORONA_COLOUR
				ENDIF
				
				RETURN FM_MY_UGC_CORONA_COLOUR
			ELIF (paramCreatorID < NUM_NETWORK_PLAYERS)
				// Other UGC
				RETURN FM_OTHER_UGC_CORONA_COLOUR
			ENDIF
			BREAK
	ENDSWITCH
	
	// Need to add new Corona Colour for this CreatorID
	NET_PRINT("...KGM MP [MissionInfo]: Get_Corona_Colour_For_This_CreatorID - ERROR: Need to add Corona Colour for this CreatorID: ")
	NET_PRINT_INT(paramCreatorID)
	NET_NL()
	SCRIPT_ASSERT("Get_Corona_Colour_For_This_CreatorID(): ERROR - Need to add Corona Colour for FM Creator ID. See Console Log. Tell Keith.")
	
	// Return defaults
	RETURN HUD_COLOUR_GREY
							
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Text Colour based on the CreatorID
//
// INPUT PARAMS:		paramCreatorID			The Creator ID - used to colour the InCorona Text appropriately
//						paramIsBookmarked		TRUE if this is a bookmarked mission, otherwise FALSE
// RETURN VALUE:		HUD_COLOURS				The Text Colour for this Creator
FUNC HUD_COLOURS Get_Text_Colour_For_This_CreatorID(INT paramCreatorID, BOOL paramIsBookmarked)

	// KGM 5/1/13: Creator IDs will be added as they start to be referenced (currently only required for eFM_RACE_CLOUD)
	
	SWITCH (paramCreatorID)
		// MiniGames
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN FM_MINIGAME_TEXT_COLOUR
			
		// Rockstar Created
		CASE FMMC_ROCKSTAR_CREATOR_ID
			RETURN FM_ROCKSTAR_CREATED_TEXT_COLOUR
			
		// Rockstar Verified
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			RETURN FM_ROCKSTAR_VERIFIED_TEXT_COLOUR
			
		DEFAULT
			IF (paramCreatorID = NATIVE_TO_INT(PLAYER_ID()))
				// My UGC, or BOOKMARKED
				IF (paramIsBookmarked)
					RETURN FM_BOOKMARKED_TEXT_COLOUR
				ENDIF
				
				RETURN FM_MY_UGC_TEXT_COLOUR
			ELIF (paramCreatorID < NUM_NETWORK_PLAYERS)
				// Other UGC
				RETURN FM_OTHER_UGC_TEXT_COLOUR
			ENDIF
			BREAK
	ENDSWITCH
	
	// Need to add new Corona Colour for this CreatorID
	NET_PRINT("...KGM MP [MissionInfo]: Get_Text_Colour_For_This_CreatorID - ERROR: Need to add Text Colour for this CreatorID: ")
	NET_PRINT_INT(paramCreatorID)
	NET_NL()
	SCRIPT_ASSERT("Get_Text_Colour_For_This_CreatorID(): ERROR - Need to add Text Colour for FM Creator ID. See Console Log. Tell Keith.")
	
	// Return defaults
	RETURN HUD_COLOUR_GREY
							
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the race-specific corona icon based on the race subtype
//
// RETURN PARAMS:		paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:		eMP_TAG_SCRIPT				The Icon
//
// NOTES:	The Race Subtypes are copied from this function: GET_FM_RACE_BLIP_TYPE_FROM_SUB_TYPE
FUNC eMP_TAG_SCRIPT Get_FM_Race_Corona_Icon_From_Subtype(MP_MISSION_ID_DATA &paramMissionIdData)

	// Get the subtype
	INT theSubtype = Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
	
	SWITCH (theSubtype)
		CASE FMMC_RACE_TYPE_STANDARD
		CASE FMMC_RACE_TYPE_P2P
		CASE FMMC_RACE_TYPE_STUNT
		CASE FMMC_RACE_TYPE_STUNT_P2P
			RETURN MP_TAG_SCRIPT_RACE_LAND
		CASE FMMC_RACE_TYPE_TARGET
		CASE FMMC_RACE_TYPE_TARGET_P2P
			RETURN MP_TAG_SCRIPT_TARGET_ASSAULT_SERIES
			
		CASE FMMC_RACE_TYPE_ON_FOOT
		CASE FMMC_RACE_TYPE_ON_FOOT_P2P
			RETURN MP_TAG_SCRIPT_RACE_FOOT
			
		CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE		
		CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE_P2P
			RETURN MP_TAG_SCRIPT_RACE_BIKE
			
		CASE FMMC_RACE_TYPE_BOAT
		CASE FMMC_RACE_TYPE_BOAT_P2P
			RETURN MP_TAG_SCRIPT_RACE_BOAT
			
		CASE FMMC_RACE_TYPE_AIR
		CASE FMMC_RACE_TYPE_AIR_P2P
			RETURN MP_TAG_SCRIPT_RACE_AIR
			
		CASE FMMC_RACE_TYPE_ARCADE_RACE		
		CASE FMMC_RACE_TYPE_ARCADE_RACE_P2P	
		CASE FMMC_RACE_TYPE_DESTRUCTION_DERBY	
		CASE FMMC_RACE_TYPE_DESTRUCTION_DERBY_P2P
			RETURN MP_TAG_SCRIPT_ARENA_SERIES

		CASE FMMC_RACE_TYPE_OPEN_WHEEL		
		CASE FMMC_RACE_TYPE_OPEN_WHEEL_P2P	
			RETURN MP_TAG_SCRIPT_OPEN_WHEEL_SERIES
			
		CASE FMMC_RACE_TYPE_PURSUIT
			RETURN MP_TAG_SCRIPT_PURSUIT_SERIES
			
		CASE FMMC_RACE_TYPE_STREET
			RETURN MP_TAG_SCRIPT_STREET_RACE_SERIES
			
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE FMMC_RACE_TYPE_HSW
		CASE FMMC_RACE_TYPE_HSW_P2P
			RETURN MP_TAG_SCRIPT_HSW_RACE_SERIES
		#ENDIF
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Race_Corona_Icon_From_Subtype() - ERROR: Unknown Race Subtype: ") NET_PRINT_INT(theSubtype) NET_NL()
		SCRIPT_ASSERT("Get_FM_Race_Corona_Icon_From_Subtype(): ERROR - Unknown Race Subtype. Add to switch statement. Returning MP_TAG_SCRIPT_RACE_OFFROAD. Tell Keith or Bobby.")
	#ENDIF
	
	RETURN MP_TAG_SCRIPT_RACE_OFFROAD

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the deathmatch-specific corona icon based on the deathmatch subtype
//
// RETURN PARAMS:		paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:		eMP_TAG_SCRIPT				The Icon
FUNC eMP_TAG_SCRIPT Get_FM_Deathmatch_Corona_Icon_From_Subtype(MP_MISSION_ID_DATA &paramMissionIdData)

	// Get the subtype
	INT theSubtype = Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
	
	SWITCH (theSubtype)
		CASE FMMC_DEATHMATCH_STANDARD
			RETURN MP_TAG_SCRIPT_DEATHMATCH
			
		CASE FMMC_DEATHMATCH_TEAM
			RETURN MP_TAG_SCRIPT_TEAM_DM
			
		CASE FMMC_DEATHMATCH_VEHICLE
			RETURN MP_TAG_SCRIPT_VEHICLE_DM
		
		CASE FMMC_KING_OF_THE_HILL
			RETURN MP_TAG_SCRIPT_KING_OF_THE_HILL
			
		CASE FMMC_KING_OF_THE_HILL_TEAM
			RETURN MP_TAG_SCRIPT_KING_OF_THE_HILL_TEAM			
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Deathmatch_Corona_Icon_From_Subtype() - ERROR: Unknown Deathmatch Subtype: ") NET_PRINT_INT(theSubtype) NET_NL()
		SCRIPT_ASSERT("Get_FM_Deathmatch_Corona_Icon_From_Subtype(): ERROR - Unknown Deathmatch Subtype. Add to switch statement. Returning MP_TAG_SCRIPT_DEATHMATCH. Tell Keith or Bobby.")
	#ENDIF
	
	RETURN MP_TAG_SCRIPT_DEATHMATCH

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the mission-specific corona icon based on the Mission subtype
//
// RETURN PARAMS:		paramMissionIdData			The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:		eMP_TAG_SCRIPT				The Icon
FUNC eMP_TAG_SCRIPT Get_FM_Mission_Corona_Icon_From_Subtype(MP_MISSION_ID_DATA &paramMissionIdData)

	IF Is_FM_Cloud_Loaded_Activity_A_LTS_Mission(paramMissionIdData)
		RETURN (MP_TAG_SCRIPT_LTS)
	ENDIF
	
  	IF Is_FM_Cloud_Loaded_Activity_A_CTF_Mission(paramMissionIdData)
		RETURN (MP_TAG_SCRIPT_CTF)
  	ENDIF
	
	RETURN (MP_TAG_SCRIPT_CUSTOM_MISSION)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the corona trigger radius from MissionID
//
// INPUT PARAMS:			paramMissionID			The MissionID
// RETURN VALUE:			FLOAT					The trigger radius
FUNC FLOAT Get_Corona_Trigger_Radius_From_MissionID(MP_MISSION paramMissionID)

	// The switch statement contains any exceptions
	SWITCH paramMissionID
		CASE eFM_SHOOTING_RANGE
		CASE eFM_DARTS
		CASE eFM_ARM_WRESTLING
		CASE eFM_PILOT_SCHOOL
			RETURN ciIN_LOCATE_DISTANCE_INSIDE
	ENDSWITCH
	
	RETURN ciIN_LOCATE_DISTANCE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the corona trigger radius from MissionID
//
// INPUT PARAMS:			refMissionIdData		The MissionID Data
// RETURN VALUE:			FLOAT					The trigger radius
//
// NOTES:	Heists should use a smaller trigger radius
FUNC FLOAT Get_Corona_Trigger_Radius_From_MissionIdData(MP_MISSION_ID_DATA &refMissionIdData)
	
	// Heists use smaller trigger radius
	IF (Is_FM_Cloud_Loaded_Activity_A_Heist(refMissionIdData))
	OR (Is_FM_Cloud_Loaded_Activity_Heist_Planning(refMissionIdData))
	OR GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(refMissionIdData))
		RETURN ciIN_LOCATE_DISTANCE_INSIDE
	ENDIF
	
	// Use usual rules apply
	RETURN (Get_Corona_Trigger_Radius_From_MissionID(refMissionIdData.idMission))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the corona On Hours from MissionID
//
// INPUT PARAMS:			refMissionIdData		The MissionID Data
// RETURN VALUE:			INT						The on hours to be passed to the Corona
//
// NOTES:	Contact Missions should be setup as ALL_DAY and the invite will use the 'on hours' as the time control
FUNC INT Get_Corona_On_Hours_From_MissionIdData(MP_MISSION_ID_DATA &refMissionIdData)

	// Contact Missions are available ALL_DAY (the invite will control the on hours)
	// KGM 5/9/14: Ensure Heist coronas don't have specific TimeOfDay settings. This is controlled from within the mission itself with a different variable.
	IF (Is_FM_Cloud_Loaded_Activity_A_Contact_Mission(refMissionIdData))
	OR (Is_FM_Cloud_Loaded_Activity_A_Heist(refMissionIdData))
	OR (Is_FM_Cloud_Loaded_Activity_Heist_Planning(refMissionIdData))
		RETURN MATC_ALL_DAY
	ENDIF
	
	// Use usual rules apply
	RETURN (Get_On_Hours_For_FM_Cloud_Loaded_Activity(refMissionIdData))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Hud Colours and Icon to use for this activity
//
// REFERENCE PARAMS:	refMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
//						refCoronaColour			The Corona Colour for the activity
//						refTextColour			The Text Colour for the activity
//						refIcon					The Icon to display at the corona
//						refCoronaRadius			The Corona trigger and draw radius
//						refOnHours				The On Hours when the corona is available
PROC Fill_Corona_Details_For_FM_Activity(	MP_MISSION_ID_DATA		&refMissionIdData,
											HUD_COLOURS 			&refCoronaColour,
											HUD_COLOURS 			&refTextColour,
											eMP_TAG_SCRIPT			&refIcon,
											FLOAT					&refCoronaRadius,
											INT						&refOnHours)

	// Icon
	SWITCH (refMissionIdData.idMission)
		CASE eFM_ARM_WRESTLING
			refIcon = MP_TAG_SCRIPT_ARM_WRESTLING
			BREAK
			
		CASE eFM_DARTS
			refIcon = MP_TAG_SCRIPT_DARTS
			BREAK
		
		CASE eFM_PILOT_SCHOOL
			refIcon = MP_TAG_SCRIPT_PILOT_SCHOOL
			BREAK
			
		CASE eFM_GOLF
			refIcon = MP_TAG_SCRIPT_GOLF
			BREAK
			
		CASE eFM_SHOOTING_RANGE
			refIcon = MP_TAG_SCRIPT_SHOOTING_RANGE
			BREAK
			
		CASE eFM_TENNIS
			refIcon = MP_TAG_SCRIPT_TENNIS
			BREAK
			
		CASE eFM_BASEJUMP_CLOUD
			refIcon = MP_TAG_SCRIPT_BASEJUMP
			BREAK
			
		CASE eFM_DEATHMATCH_CLOUD
			refIcon = Get_FM_Deathmatch_Corona_Icon_From_Subtype(refMissionIdData)
			BREAK
			
		CASE eFM_GANG_ATTACK_CLOUD
			refIcon = MP_TAG_SCRIPT_GANG_ATTACK
			BREAK
			
		CASE eFM_MISSION_CLOUD
			refIcon = Get_FM_Mission_Corona_Icon_From_Subtype(refMissionIdData)
			BREAK
			
		CASE eFM_RACE_CLOUD
			refIcon = Get_FM_Race_Corona_Icon_From_Subtype(refMissionIdData)
			BREAK
			
		CASE eFM_SURVIVAL_CLOUD
			refIcon = MP_TAG_SCRIPT_SURVIVAL
			BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MissionInfo]: Fill_Corona_Details_For_FM_Activity - ERROR: Need to add Corona and Text Colours for this MP MISSION (as INT): ")
				NET_PRINT_INT(ENUM_TO_INT(refMissionIdData.idMission))
				NET_NL()
				SCRIPT_ASSERT("Fill_Corona_Details_For_FM_Activity(): ERROR - Need to add Corona and Text Colour for FM Activity. See Console Log. Tell Keith.")
			#ENDIF
			
			refIcon = MP_TAG_SCRIPT_NONE
			BREAK
	ENDSWITCH
	
	BOOL isBookmarked = Get_Bookmarked_Status_FM_Cloud_Loaded_Activity(refMissionIdData)
	
	// Colours
	refCoronaColour		= Get_Corona_Colour_For_This_CreatorID(refMissionIdData.idCreator, isBookmarked)
	refTextColour		= Get_Text_Colour_For_This_CreatorID(refMissionIdData.idCreator, isBookmarked)
	refCoronaRadius		= Get_Corona_Trigger_Radius_From_MissionIdData(refMissionIdData)
	refOnHours			= Get_Corona_On_Hours_From_MissionIdData(refMissionIdData)
							
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Ground Projection Texture Name that matches the passed-in Augmented Icon
//
// INPUT PARAMS:			paramIcon				The Augmented Icon
// RETURN VALUE:			TEXT_LABEL_23			The texture name
FUNC STRING Get_Ground_Projection_Texture_That_Matches_Corona_Icon(eMP_TAG_SCRIPT paramIcon)

	SWITCH (paramIcon)
		CASE MP_TAG_SCRIPT_ARM_WRESTLING
			RETURN ("Arm_Wrestling_Icon")

		CASE MP_TAG_SCRIPT_DARTS
			RETURN ("Darts_Icon")
			
		CASE MP_TAG_SCRIPT_PILOT_SCHOOL
			RETURN ("Pilot_School_Icon")
			
		CASE MP_TAG_SCRIPT_GOLF
			RETURN ("Golf_Icon")

		CASE MP_TAG_SCRIPT_SHOOTING_RANGE
			RETURN ("Shooting_Range_Icon")

		CASE MP_TAG_SCRIPT_TENNIS
			RETURN ("Tennis_Icon")

		CASE MP_TAG_SCRIPT_BASEJUMP
			RETURN ("BaseJump_Icon")

		CASE MP_TAG_SCRIPT_DEATHMATCH
			RETURN ("Deathmatch_Marker_256")
			
		CASE MP_TAG_SCRIPT_TEAM_DM
			RETURN ("TeamDeathmatch_Icon")
			
		CASE MP_TAG_SCRIPT_VEHICLE_DM
			RETURN ("VehicleDeathmatch_Icon")

		CASE MP_TAG_SCRIPT_GANG_ATTACK
			RETURN ("GangAttack_Icon")

		CASE MP_TAG_SCRIPT_CUSTOM_MISSION
			RETURN ("Custom_Icon")

		CASE MP_TAG_SCRIPT_SURVIVAL
			RETURN ("Survival_Icon")

		CASE MP_TAG_SCRIPT_RACE_LAND
			RETURN ("Land_Race_Icon")

		CASE MP_TAG_SCRIPT_RACE_BOAT
			RETURN ("Sea_Race_Icon")

		CASE MP_TAG_SCRIPT_RACE_AIR
			RETURN ("Air_Race_Icon")
			
		CASE MP_TAG_SCRIPT_RACE_BIKE
			RETURN ("Bike_Race_Icon")
			
		CASE MP_TAG_SCRIPT_RACE_FOOT
			RETURN ("Foot_Race_Icon")
			
		CASE MP_TAG_SCRIPT_CTF
			RETURN ("Capture_The_Flag_Icon")
			
		CASE MP_TAG_SCRIPT_LTS
			RETURN ("Last_Team_Standing_Icon")
			
		CASE MP_TAG_SCRIPT_STUNT_PLAYLIST
			RETURN "Stunt_Mode_Icon"
			
		CASE MP_TAG_SCRIPT_ADVERSARY_PLAYLIST
			RETURN "Featured_Series"
			
		CASE MP_TAG_SCRIPT_ADVERSARY_PLAYLIST_4
			RETURN "Adversary_4"
		CASE MP_TAG_SCRIPT_ADVERSARY_PLAYLIST_8
			RETURN "Adversary_8"
		CASE MP_TAG_SCRIPT_ADVERSARY_PLAYLIST_10
			RETURN "Adversary_10"
		CASE MP_TAG_SCRIPT_ADVERSARY_PLAYLIST_12
			RETURN "Adversary_12"
		CASE MP_TAG_SCRIPT_ADVERSARY_PLAYLIST_16
			RETURN "Adversary_16"

		CASE MP_TAG_SCRIPT_STUNT_PREMIUM
			RETURN "Stunt_Premium_Icon"
			
		CASE MP_TAG_SCRIPT_SPECIAL_VEHICLE_RACE
			RETURN "Special_Vehicle_Race_Series"
			
		CASE MP_TAG_SCRIPT_CHALLENGE_SERIES
			RETURN "Challenge_Series"

		CASE MP_TAG_SCRIPT_BUNKER_SERIES
			RETURN "Adversary_Bunker"

		CASE MP_TAG_SCRIPT_TF_SERIES
			RETURN "Transform_Race_Icon"
			
		CASE MP_TAG_SCRIPT_TARGET_ASSAULT_SERIES
			RETURN "Target_race"

		CASE MP_TAG_SCRIPT_HOTRING_SERIES
			RETURN "Hotring_circuit"
			
		CASE MP_TAG_SCRIPT_ARENA_SERIES
			RETURN "Arena_Series"
			
		CASE MP_TAG_SCRIPT_RACE_SERIES
			RETURN "Land_Race_Icon"
			
		CASE MP_TAG_SCRIPT_SURVIVAL_SERIES
			RETURN "Survival_Icon"
			
		// Add new projections for KOTH when they're available
		// url:bugstar:5664503 - Has a new ground projection for king of the hill jobs been requested?
			
		CASE MP_TAG_SCRIPT_KING_OF_THE_HILL
			RETURN ("King_Of_The_Hill")
			
		CASE MP_TAG_SCRIPT_KING_OF_THE_HILL_TEAM
			RETURN ("King_Of_The_Hill_Teams")
			
		CASE MP_TAG_SCRIPT_OPEN_WHEEL_SERIES
			RETURN "Open_Wheel_Race"
		
		CASE MP_TAG_SCRIPT_PURSUIT_SERIES
			RETURN "Pursuit_Series"
			
		CASE MP_TAG_SCRIPT_STREET_RACE_SERIES
			RETURN "Street_Race_Series"
			
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE MP_TAG_SCRIPT_HSW_RACE_SERIES
			RETURN "HSW_Race_Series"
		#ENDIF

		CASE MP_TAG_SCRIPT_COMMUNITY_SERIES
			RETURN "Community_Series_Icon"
			
		CASE MP_TAG_SCRIPT_CAYO_PERICO_SERIES
			RETURN "Cayo_Perico_Icon"

	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_Ground_Projection_Texture_That_Matches_Corona_Icon - ERROR: Failed to Find Corona Icon ID (as INT): ")
		NET_PRINT_INT(ENUM_TO_INT(paramIcon))
		NET_NL()
		SCRIPT_ASSERT("Get_Ground_Projection_Texture_That_Matches_Corona_Icon(): ERROR - Need to add Ground Projection Texture for FM Activity. See Console Log. Tell Keith.")
	#ENDIF
	
	RETURN ("Off_Road_Race_Icon")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the passed-in mission details should, be default, only display a blip on the minimap
//
// REFERENCE PARAMS:		refMissionIdData			MissionID Data
// RETURN VALUE:			BOOL						TRUE if the blip for these details should only appear on the minimap, FALSE if standard (minimap and frontend)
//
// NOTES:	KGM 1/5/13: Random Event missions should only display the blip on the minimap
//			KGM 19/5/13: Gang Attacks should only appear on minimap
FUNC BOOL Should_FM_Activity_Display_Blip_Only_On_Minimap(MP_MISSION_ID_DATA &refMissionIdData)

	IF (Is_FM_Cloud_Loaded_Activity_A_Random_Event(refMissionIdData))
	OR (refMissionIdData.idMission = eFM_GANG_ATTACK_CLOUD)
		RETURN TRUE
	ENDIF
	
	// Display blip on both minimap and FrontEnd map
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_HIDE_JOB_BLIP_AND_CORONA_BY_ROOT_CONTENT_ID(INT iRootContentIDHash)
	SWITCH iRootContentIDHash
		CASE TDM_VINEWOOD_KILLS_ROOT_CONTENT_ID_HASH
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

// PURPOSE:	Check if the passed-in mission details should, be default, not display a blip
//
// REFERENCE PARAMS:		refMissionIdData			MissionID Data
// RETURN VALUE:			BOOL						TRUE if this mission should NOT display a blip, FALSE if standard (display blip)
//
// NOTES:	KGM 1/5/13: Contact Missions should not display a blip
//			KGM 2/5/13: Heists should not display a blip
FUNC BOOL Should_FM_Activity_Not_Display_A_Blip(MP_MISSION_ID_DATA &refMissionIdData)

	IF (Is_FM_Cloud_Loaded_Activity_A_Contact_Mission(refMissionIdData))
	OR (Is_FM_Cloud_Loaded_Activity_A_Heist(refMissionIdData))
	OR (Is_FM_Cloud_Loaded_Activity_Heist_Planning(refMissionIdData))
		// TRUE - DON'T display a blip
		RETURN TRUE
	ENDIF
	
	IF SHOULD_HIDE_JOB_BLIP_AND_CORONA_BY_ROOT_CONTENT_ID(Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(refMissionIdData))
		PRINTLN("[TS] ...KGM MP - Should_FM_Activity_Not_Display_A_Blip - SHOULD_HIDE_JOB_BLIP_AND_CORONA_BY_ROOT_CONTENT_ID - RETURN TRUE")
		RETURN TRUE
	ENDIF
	
	// Display blip
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the passed-in mission details should, be default, not display a corona
//
// REFERENCE PARAMS:		refMissionIdData			MissionID Data
// RETURN VALUE:			BOOL						TRUE if this mission should NOT display a corona, FALSE if standard (display corona)
//
// NOTES:	KGM 1/5/13: Contact Missions should not display a corona
//			KGM 19/5/13: Gang Attack should not display a corona
FUNC BOOL Should_FM_Activity_Not_Display_A_Corona(MP_MISSION_ID_DATA &refMissionIdData)

	IF (Is_FM_Cloud_Loaded_Activity_A_Contact_Mission(refMissionIdData))
	OR (refMissionIdData.idMission = eFM_GANG_ATTACK_CLOUD)
		// TRUE - DON'T display a corona
		RETURN TRUE
	ENDIF
	
	IF SHOULD_HIDE_JOB_BLIP_AND_CORONA_BY_ROOT_CONTENT_ID(Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(refMissionIdData))
		PRINTLN("[TS] ...KGM MP - Should_FM_Activity_Not_Display_A_Corona - SHOULD_HIDE_JOB_BLIP_AND_CORONA_BY_ROOT_CONTENT_ID - RETURN TRUE")
		RETURN TRUE
	ENDIF
	
	// Display corona
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the passed-in mission details should, be default, quick Launch without using an InCorona Sequence
//
// REFERENCE PARAMS:		refMissionIdData			MissionID Data
// RETURN VALUE:			BOOL						TRUE if this mission should Quick Launch, FALSE if it should use the standard launch sequence
//
// NOTES:	KGM 12/5/13: Gang Attacks should Quick Launch
FUNC BOOL Should_FM_Activity_Quick_Launch(MP_MISSION_ID_DATA &refMissionIdData)

	SWITCH (refMissionIdData.idMission)
		CASE eFM_GANG_ATTACK_CLOUD
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the passed-in mission details should, be default, await activation again after being played
//
// REFERENCE PARAMS:		refMissionIdData			MissionID Data
// RETURN VALUE:			BOOL						TRUE if this mission should Await Activation after play, FALSE if it can become available again immediately
//
// NOTES:	KGM 12/5/13: Gang Attacks should Await Activation by the server again after being played to avoid immediate re-triggering
FUNC BOOL Should_FM_Activity_Await_Activation_After_Play(MP_MISSION_ID_DATA &refMissionIdData)

	SWITCH (refMissionIdData.idMission)
		CASE eFM_GANG_ATTACK_CLOUD
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the passed-in mission details should, be default, keep the mission open (ignore closing hour) if the mission is active in the session
//
// REFERENCE PARAMS:		refMissionIdData			MissionID Data
// RETURN VALUE:			BOOL						TRUE if this mission should Remain Open If Active, FALSE if it should obey the normal closing hour even if active
//
// NOTES:	KGM 7/7/13: Gang Attacks should Remain Open If Active so that players can still join the mission after closing time if there is another player already on it
FUNC BOOL Should_FM_Activity_Remain_Open_If_Active(MP_MISSION_ID_DATA &refMissionIdData)

	SWITCH (refMissionIdData.idMission)
		CASE eFM_GANG_ATTACK_CLOUD
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the passed-in mission details should, be default, Launch Immediately when the mission is unlocked
//
// REFERENCE PARAMS:		refMissionIdData			MissionID Data
// RETURN VALUE:			BOOL						TRUE if this mission should Launch Immediately, FALSE if it should launch when player in a corona, as normal
//
// NOTES:	KGM 9/6/13: Contact Missions should Launch Immediately
FUNC BOOL Should_FM_Activity_Launch_Immediately_When_Unlocked(MP_MISSION_ID_DATA &refMissionIdData)

	IF (Is_FM_Cloud_Loaded_Activity_A_Contact_Mission(refMissionIdData))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the passed-in mission details are for an Invite Only mission
//
// REFERENCE PARAMS:		refMissionIdData			MissionID Data
// RETURN VALUE:			BOOL						TRUE if this mission is Invite Only, FALSE if it can launch when a player walks into a corona, as normal
//
// NOTES:	KGM 9/6/13: Contact Missions are Invite Only
FUNC BOOL Is_FM_Activity_Invite_Only(MP_MISSION_ID_DATA &refMissionIdData)

	IF (Is_FM_Cloud_Loaded_Activity_A_Contact_Mission(refMissionIdData))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the FM Activity should wait for a Secondary Unlock of the activity type before unlocking
//
// REFERENCE PARAMS:		refMissionIdData			MissionID Data
// RETURN VALUE:			BOOL						TRUE if this mission should wait for a secondary unlock, FALSE if it can launch with the primary unlock, as normal
//
// NOTES:	KGM 28/7/13: Survivals use a primary unlock to unlock two initial ones, followed by a secondary unlock when one of those two have been played to unlock the rest
FUNC BOOL Should_FM_Activity_Wait_For_Secondary_Unlock(MP_MISSION_ID_DATA &refMissionIdData)

	SWITCH (refMissionIdData.idMission)
		CASE eFM_SURVIVAL_CLOUD
			// KGM 10/7/13: When all survivals have the flag in, uncomment the line below and delete the others and the TEMP_ function
			RETURN (Get_Secondary_Unlock_Status_FM_Cloud_Loaded_Activity(refMissionIdData))
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the ID of the routine that Missions At Coords will pass control over to when the player walks into a corona
//
// REFERENCE PARAMS:		refMissionIdData			MissionID Data
// RETURN VALUE:			g_eMatCInCoronaExternalID	The MissionsAtCoords External Processing Routine ID
//
// NOTES:	KGM 12/5/13: Almost everything uses FMMC_Launcher, but Gang Attacks don't use an InCorona sequence
FUNC g_eMatCInCoronaExternalID Get_ExternalID_To_Use_For_InCorona_Processing(MP_MISSION_ID_DATA &refMissionIdData)

	SWITCH (refMissionIdData.idMission)
		CASE eFM_GANG_ATTACK_CLOUD
			RETURN (MATCICE_NONE)
	ENDSWITCH
	
	// Everything else uses the FMMC_Launcher InCorona processing routines
	RETURN MATCICE_FMMC_LAUNCHER

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Details of any missions that should launch when teh player walks into an area
//
// REFERENCE PARAMS:		refMissionIdData			MissionID Data
//							refOptionalFlags			The Options Strunct - this will be updated directly if required
PROC Get_FM_Activity_Area_Triggered_Option_Details(MP_MISSION_ID_DATA &refMissionIdData, g_structMatCOptionsMP &refOptions)

	// Should this mission use area triggering?
	SWITCH (refMissionIdData.idMission)
		// These use Area Triggering
		CASE eFM_GANG_ATTACK_CLOUD
			BREAK
			
		// The rest don't
		DEFAULT
			EXIT
	ENDSWITCH
	
	// Gang Attacks - these use the subtype field as an index into some angled area storage
	IF (refMissionIdData.idMission = eFM_GANG_ATTACK_CLOUD)
		INT areaIndex = Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData)
		
		// Check for validity
		IF (areaIndex < 0)
		OR (areaIndex >= g_FMMC_ROCKSTAR_GANG_ATTACKS.iNumberLoaded)
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_31 missionName = Get_Mission_Name_For_FM_Cloud_Loaded_Activity(refMissionIdData)
				NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Activity_Area_Triggered_Option_Details() - ERROR: areaIndex is illegal: ")
				NET_PRINT_INT(areaIndex)
				NET_PRINT(" number of Gang Attacks areas: ")
				NET_PRINT_INT(g_FMMC_ROCKSTAR_GANG_ATTACKS.iNumberLoaded)
				NET_PRINT(" [")
				NET_PRINT(missionName)
				NET_PRINT("]")
				NET_NL()
				SCRIPT_ASSERT("Get_FM_Activity_Area_Triggered_Option_Details(): ERROR - Illegal Area Index into Gang Attack Angled Area array. See Console Log. Tell Keith.")
			#ENDIF
			
			EXIT
		ENDIF
		
		// Gather the details
		VECTOR	theMin		= g_FMMC_ROCKSTAR_GANG_ATTACKS.vMin[areaIndex]
		VECTOR	theMax		= g_FMMC_ROCKSTAR_GANG_ATTACKS.vMax[areaIndex]
		FLOAT	theWidth	= g_FMMC_ROCKSTAR_GANG_ATTACKS.fRadius[areaIndex]
		
		// Do some consistency checking on the details
		VECTOR	theOrigin			= << 0.0, 0.0, 0.0 >>
		FLOAT	widthToleranceMax	= 0.1
		FLOAT	widthToleranceMin	= -0.1
		
		IF (ARE_VECTORS_ALMOST_EQUAL(theMin, theOrigin))
		OR (ARE_VECTORS_ALMOST_EQUAL(theMax, theOrigin))
		OR (ARE_VECTORS_ALMOST_EQUAL(theMin, theMax, widthToleranceMax))
		OR ((theWidth > widthToleranceMin) AND (theWidth < widthToleranceMax))
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_31 missionName = Get_Mission_Name_For_FM_Cloud_Loaded_Activity(refMissionIdData)
				NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Activity_Area_Triggered_Option_Details() - ERROR: Retrieved area details are illegal: ")
				NET_PRINT_INT(areaIndex)
				NET_PRINT(" [")
				NET_PRINT(missionName)
				NET_PRINT("]")
				NET_NL()
				NET_PRINT("      Area (Min): ")	NET_PRINT_VECTOR(theMin)	NET_NL()
				NET_PRINT("      Area (Max): ")	NET_PRINT_VECTOR(theMax)	NET_NL()
				NET_PRINT("      Width     : ")	NET_PRINT_FLOAT(theWidth)	NET_NL()
				SCRIPT_ASSERT("Get_FM_Activity_Area_Triggered_Option_Details(): ERROR - Retrieved Gang Attack Angled Area details are illegal. See Console Log. Tell Keith.")
			#ENDIF
			
			EXIT
		ENDIF
		
		// All seems good
		refOptions.matcoUseAngledArea	= TRUE
		refOptions.matcoAngledAreaMin	= theMin
		refOptions.matcoAngledAreaMax	= theMax
		refOptions.matcoAngledAreaWidth	= theWidth
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_FM_Activity_Area_Triggered_Option_Details() - ERROR: Need to deal with a new Activity Type that uses Angled Area Triggering")
		NET_NL()
		SCRIPT_ASSERT("Get_FM_Activity_Area_Triggered_Option_Details(): ERROR - Need to add a handler for an activity type that uses Angled Area triggering. See Console Log. Tell Keith.")
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Gather the general options for a mission
//
// REFERENCE PARAMS:	refMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
//						refOptions				The struct containing the optional flags
PROC Gather_Standard_Optional_Flags_For_Missions(MP_MISSION_ID_DATA &refMissionIdData, g_structMatCOptionsMP &refOptions)

	// NOTES:	Launch Immediately means the mission is triggered immediately it is unlocked - but triggering may still require the player to be reserved for the mission first (ie: Contact Mission)
	//			Quick Launch means the player doesn't get reserved for the mission but instead starts a new mission or joins an active mission immediately (ie: Gang Attack)
	
	refOptions.matcoInCoronaExternalID	= Get_ExternalID_To_Use_For_InCorona_Processing(refMissionIdData)
	refOptions.matcoShareCloudData		= TRUE
	refOptions.matcoMinimapBlipOnly		= Should_FM_Activity_Display_Blip_Only_On_Minimap(refMissionIdData)
	refOptions.matcoDisplayNoBlip		= Should_FM_Activity_Not_Display_A_Blip(refMissionIdData)
	refOptions.matcoDisplayNoCorona		= Should_FM_Activity_Not_Display_A_Corona(refMissionIdData)
	refOptions.matcoLaunchImmediately	= Should_FM_Activity_Launch_Immediately_When_Unlocked(refMissionIdData)
	refOptions.matcoQuickLaunch			= Should_FM_Activity_Quick_Launch(refMissionIdData)
	refOptions.matcoInactiveAfterPlay	= Should_FM_Activity_Await_Activation_After_Play(refMissionIdData)
	refOptions.matcoInviteOnly			= Is_FM_Activity_Invite_Only(refMissionIdData)
	refOptions.matcoRemainOpenIfActive	= Should_FM_Activity_Remain_Open_If_Active(refMissionIdData)
	refOptions.matcoSecondaryUnlock		= Should_FM_Activity_Wait_For_Secondary_Unlock(refMissionIdData)
	
	// KGM 15/6/13: Area-Triggered Mission additional options
	Get_FM_Activity_Area_Triggered_Option_Details(refMissionIdData, refOptions)
	
ENDPROC





// ===========================================================================================================
//      FUNCTIONS USED WHEN CHANGING TRANSITION SESSIONS
// ===========================================================================================================

// PURPOSE:	Search through all cloud-loaded header data looking for the specified cloud filename and retrieve the missionID data if found
//
// INPUT PARAMS:			paramCheckExtra				[DEFAULT = TRUE] TRUE to also check any extra R* Created mission slots from 500+, FALSE to ignore those
// REFERENCE PARAM:			refCloudFilename			The cloud filename being searched for
//							refMissionIdData			The MissionId Data being filled out and returned
// RETURN VALUE:			BOOL						TRUE if the cloud filename was found, otherwise FALSE
FUNC BOOL Search_For_And_Retrieve_MissionIdData_From_Cloud_Filename(TEXT_LABEL_23 &refCloudFilename, MP_MISSION_ID_DATA &refMissionIdData, BOOL paramCheckExtra = TRUE)

	IF NOT (NETWORK_IS_GAME_IN_PROGRESS()) //Added by Steve T with Bobby / Keith direction.
        PRINTNL()
        PRINTSTRING("...KGM MP / SP crossover - Search_For_And_Retrieve_MissionIdData_From_Cloud_Filename exiting and returning FALSE as player in SP")
        PRINTNL()

        RETURN FALSE
		
    ENDIF




	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Search_For_And_Retrieve_MissionIdData_From_Cloud_Filename() - Starting Search for filename: ")
		NET_PRINT(refCloudFilename)
		NET_NL()
	#ENDIF
	
	// Ensure the filename is valid
	IF (IS_STRING_NULL_OR_EMPTY(refCloudFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MissionInfo]: Search_For_And_Retrieve_MissionIdData_From_Cloud_Filename() - ERROR: Filename is NULL or EMPTY")
			NET_NL()
			SCRIPT_ASSERT("Search_For_And_Retrieve_MissionIdData_From_Cloud_Filename() - ERROR: Trying to search for a cloud filename that is NULL or EMPTY. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	INT tempLoop		= 0
	INT FMMissionType	= -1
	
	// Search the Rockstar Created header details
	INT maxCheck = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED
	IF NOT (paramCheckExtra)
		maxCheck = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD
	ENDIF

	#IF IS_DEBUG_BUILD
		NET_PRINT("      Searching Rockstar Created header details [")
		IF (paramCheckExtra)
			NET_PRINT("Includes ")
		ELSE
			NET_PRINT("Doesn't include ")
		ENDIF
		NET_PRINT_INT(FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD)
		NET_PRINT(" - ")
		NET_PRINT_INT((FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED - 1))
		NET_PRINT("]: ")
	#ENDIF
	
	REPEAT maxCheck tempLoop
		IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
			IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].tlName, refCloudFilename))

				#IF IS_DEBUG_BUILD
					NET_PRINT("SUCCESS: Found the data at Rockstar Created array position: ")
					NET_PRINT_INT(tempLoop)
					NET_NL()
				#ENDIF
				
				// Gather the MissionID data details
				FMMissionType						= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iType
				
				refMissionIdData.idMission			= Convert_FM_Mission_Type_To_MissionID(FMMissionType)
				refMissionIdData.idVariation		= tempLoop
				refMissionIdData.idCreator			= FMMC_ROCKSTAR_CREATOR_ID
				refMissionIdData.idCloudFilename	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].tlName
				refMissionIdData.idSharedRegID		= ILLEGAL_SHARED_REG_ID
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("NOT FOUND")
		NET_NL()
	#ENDIF
	
	// Search the Rockstar Created header details
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Searching Rockstar Verified header details: ")
	#ENDIF

	REPEAT FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED tempLoop
		IF (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
			IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[tempLoop].tlName, refCloudFilename))
				#IF IS_DEBUG_BUILD
					NET_PRINT("SUCCESS: Found the data at Rockstar Verified array position: ")
					NET_PRINT_INT(tempLoop)
					NET_NL()
				#ENDIF
				
				// Gather the MissionID data details
				FMMissionType						= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[tempLoop].iType
				
				refMissionIdData.idMission			= Convert_FM_Mission_Type_To_MissionID(FMMissionType)
				refMissionIdData.idVariation		= tempLoop
				refMissionIdData.idCreator			= FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
				refMissionIdData.idCloudFilename	= refCloudFilename
				refMissionIdData.idSharedRegID		= ILLEGAL_SHARED_REG_ID
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("NOT FOUND")
		NET_NL()
	#ENDIF
	
	// Search the Players UGC header details
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Searching Players UGC and Bookmarked header details: ")
	#ENDIF

	REPEAT MAX_NUMBER_UGC_AND_BOOKMARKED tempLoop
		IF (IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
			IF (ARE_STRINGS_EQUAL(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[tempLoop].tlName, refCloudFilename))
				#IF IS_DEBUG_BUILD
					NET_PRINT("      Found the data at Player's UGC and Bookmarked array position: ")
					NET_PRINT_INT(tempLoop)
					NET_NL()
				#ENDIF
				
				// Gather the MissionID data details
				FMMissionType						= g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[tempLoop].iType
				
				refMissionIdData.idMission			= Convert_FM_Mission_Type_To_MissionID(FMMissionType)
				refMissionIdData.idVariation		= tempLoop
				refMissionIdData.idCreator			= NATIVE_TO_INT(PLAYER_ID())
				refMissionIdData.idCloudFilename	= refCloudFilename
				refMissionIdData.idSharedRegID		= ILLEGAL_SHARED_REG_ID
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("NOT FOUND")
		NET_NL()
		NET_PRINT("      NO MORE HEADER DATA TO SEARCH - Cloud Filename is not available as local header data")
		NET_NL()
	#ENDIF
	
	RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      TEMPORARY FUNCTIONS USED FOR ACCESSING CACHED 255-LENGTH DESCRIPTIONS
// ===========================================================================================================

// PURPOSE:	TEMP: Return the DescriptionID to be passed to code for where Rockstar Created Descriptions are stored
//
// INPUT PARAMS:		paramCreatorID			The CreatorID
// RETURN VALUE:		INT						The descriptionID used by UGC_GET_CACHED_CONTENT_DESCRIPTION(), or ILLEGAL_ARRAY_POSITION if none for this creator
//
// NOTES:	THIS IS REALLY NASTY AN TEMP - We need to store the actual ID returned by code cloud access functions like: UGC_GET_GET_BY_CATEGORY()
FUNC INT TEMP_Get_DescriptionID_For_CreatorID(INT paramCreatorID)

	SWITCH (paramCreatorID)
		CASE FMMC_ROCKSTAR_CREATOR_ID				RETURN 8			// Dodgy hardcoded value - see NOTES
	ENDSWITCH
	
	RETURN ILLEGAL_ARRAY_POSITION
			
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	TEMP: From the info available to the Joblist, find the cached description ID
//
// INPUT PARAMS:		paramCreatorID			The CreatorID
//						paramMissionName		The Mission Name
// RETURN VALUE:		INT						The Cached Description ID, or ILLEGAL_ARRAY_POSITION
//
// NOTES:	For Rockstar Created content only, search the array looking for this mission name and return the array position
FUNC INT TEMP_Get_CachedDescId_From_Creator_And_MissionName(INT paramCreatorID, STRING paramMissionName)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: TEMP_Get_CachedDescId_From_Creator_And_MissionName() - Search for CreatorID: ")
		NET_PRINT_INT(paramCreatorID)
		NET_PRINT("  Mission Name: ")
		NET_PRINT(paramMissionName)
		NET_NL()
	#ENDIF

	IF (paramCreatorID != FMMC_ROCKSTAR_CREATOR_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("      BUT: CreatorID is not FMMC_ROCKSTAR_CREATOR_ID, so return ILLEGAL_ARRAY_POSITION") NET_NL()
		#ENDIF
	
		RETURN (ILLEGAL_ARRAY_POSITION)
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramMissionName))
		#IF IS_DEBUG_BUILD
			NET_PRINT("      BUT: MissionName is NULL or EMPTY, so return ILLEGAL_ARRAY_POSITION") NET_NL()
		#ENDIF
	
		RETURN (ILLEGAL_ARRAY_POSITION)
	ENDIF
	
	// Search the Rockstar Created array looking for this mission name
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Starting Search...")
	#ENDIF
	
	INT tempLoop = 0
	REPEAT FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED tempLoop
		IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
			IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].tlMissionName, paramMissionName))
				#IF IS_DEBUG_BUILD
					NET_PRINT(" Found the data at Rockstar Created array position: ")
					NET_PRINT_INT(tempLoop)
					NET_NL()
				#ENDIF
				
				RETURN (tempLoop)
			ENDIF
		ENDIF
	ENDREPEAT

	#IF IS_DEBUG_BUILD
		NET_PRINT(" FAILED TO FIND MISSION NAME, so returning ILLEGAL_ARRAY_POSITION")
	#ENDIF
	
	RETURN (ILLEGAL_ARRAY_POSITION)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	From the info available to the Joblist, find the mission description hash
//
// INPUT PARAMS:		paramCreatorID			The CreatorID
//						paramContentID			The ContentID (cloud filename)
// RETURN VALUE:		INT						The mission description hash, or 0
FUNC INT Get_MissionDescHash_From_Creator_And_ContentID(INT paramCreatorID, STRING paramContentID)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Get_MissionDescHash_From_Creator_And_ContentID() - Search for CreatorID: ")
		NET_PRINT_INT(paramCreatorID)
		NET_PRINT("  ContentID: ")
		NET_PRINT(paramContentID)
		NET_NL()
	#ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramContentID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("      BUT: ContentID is NULL or EMPTY, so return 0") NET_NL()
		#ENDIF
	
		RETURN (ILLEGAL_ARRAY_POSITION)
	ENDIF
	
	// Search the array looking for this contentID
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Starting Search...")
	#ENDIF
	
	INT tempLoop = 0
	
	SWITCH (paramCreatorID)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			REPEAT FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED tempLoop
				IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
					IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].tlName, paramContentID))
						#IF IS_DEBUG_BUILD
							NET_PRINT(" Found the data at Rockstar Created array position: ")
							NET_PRINT_INT(tempLoop)
							NET_PRINT("  Hash: ")
							NET_PRINT_INT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iMissionDecHash)
							NET_NL()
						#ENDIF
						
						RETURN (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iMissionDecHash)
					ENDIF
				ENDIF
			ENDREPEAT
			BREAK
			
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			REPEAT FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED tempLoop
				IF (IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
					IF (ARE_STRINGS_EQUAL(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[tempLoop].tlName, paramContentID))
						#IF IS_DEBUG_BUILD
							NET_PRINT(" Found the data at Rockstar Verified array position: ")
							NET_PRINT_INT(tempLoop)
							NET_PRINT("  Hash: ")
							NET_PRINT_INT(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[tempLoop].iMissionDecHash)
							NET_NL()
						#ENDIF
						
						RETURN (g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[tempLoop].iMissionDecHash)
					ENDIF
				ENDIF
			ENDREPEAT
			BREAK
			
		CASE FMMC_MINI_GAME_CREATOR_ID
			RETURN 0
			
		DEFAULT
			IF (paramCreatorID = NATIVE_TO_INT(PLAYER_ID()))
				// Player's UGC and Bookmarked missions
				REPEAT MAX_NUMBER_UGC_AND_BOOKMARKED tempLoop
					IF (IS_BIT_SET(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[tempLoop].iBitSet , ciROCKSTAR_CREATED_MISSION_IN_USE))
						IF (ARE_STRINGS_EQUAL(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[tempLoop].tlName, paramContentID))
							#IF IS_DEBUG_BUILD
								NET_PRINT(" Found the data at Players UGC and Bookmarked array position: ")
								NET_PRINT_INT(tempLoop)
								NET_PRINT("  Hash: ")
								NET_PRINT_INT(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[tempLoop].iMissionDecHash)
								NET_NL()
							#ENDIF
							
							RETURN (g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[tempLoop].iMissionDecHash)
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			BREAK
	ENDSWITCH
	

	#IF IS_DEBUG_BUILD
		NET_PRINT("      ...FAILED TO FIND CONTENT ID, so returning 0")
	#ENDIF
	
	RETURN (0)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Fill the Generated Data struct using a copy of locally available data
//
// RETURN PARAMS:		refMissionIdData		The MissionIdData of the local data
//
// NOTES:	KGM 2/9/14 [BUG: 2004116] - Playing rounds of a UGC LTS causes many unnecessary output errors and potential data problems.
//				Part of the issue is that the owner of the UGC may not be the first player to get reserved for the next round of the
//				LTS and so the mission controller gets setup using 'generated' data. Because the mission controller passes this data
//				around, the UGC owner sometimes tries to access data using the 'generated' identifier resulting in an output error because
//				the local player doesn't have the data stored as generated data. This will copy the local data into the generated data
//				struct under specific conditions (ie: local player's focus mission is the UGC LTS but wasn't first to be reserved for the mission)
//				which will allow accesses using the 'generated data' identifier to work.
PROC Generate_Cloud_Header_Data_Using_Local_Data(MP_MISSION_ID_DATA &refMissionIdData)

	// Check if the copy is needed
	IF NOT (IS_STRING_NULL_OR_EMPTY(g_sGeneratedCloudHeaderData.gchdHeaderData.tlName))
		IF (ARE_STRINGS_EQUAL(g_sGeneratedCloudHeaderData.gchdHeaderData.tlName, refMissionIdData.idCloudFilename))
			// ...already stored, so not required
			EXIT
		ENDIF
	ENDIF
	
	// Do the copy
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MissionInfo]: Storing Generate Cloud Header Data from local data. CreatorID: ")
		NET_PRINT_INT(refMissionIdData.idCreator)
		NET_PRINT("  Variation: ")
		NET_PRINT_INT(refMissionIdData.idVariation)
		NET_PRINT("  [")
		NET_PRINT(refMissionIdData.idCloudFilename)
		NET_PRINT("]")
		NET_NL()
		IF NOT (IS_STRING_NULL_OR_EMPTY(g_sGeneratedCloudHeaderData.gchdHeaderData.tlName))
			PRINTLN("...KGM MP [MissionInfo]: Existing Data Being Overwritten:")
			Debug_Output_Generated_Header_Data()
		ENDIF
	#ENDIF
	
	Clear_Generated_Cloud_Header_Data()
	
	// Make a copy of the MissionIdData that ignores the Shared Data value so that local data is retrieved
	#IF IS_DEBUG_BUILD
		IF (refMissionIdData.idSharedRegID != ILLEGAL_SHARED_REG_ID)
			PRINTLN("...KGM MP [MissionInfo]: Storing Generate Cloud Header Data from local data. IGNORING SHARED REGID: ", refMissionIdData.idSharedRegID)
		ENDIF
	#ENDIF
	
	MP_MISSION_ID_DATA localMissionIdData = refMissionIdData
	localMissionIdData.idSharedRegID = ILLEGAL_SHARED_REG_ID
	
	INT lesRating = 0
	#IF IS_DEBUG_BUILD
		lesRating = Get_Les_Rating_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	#ENDIF
	
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlName				= refMissionIdData.idCloudFilename
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlOwner				= Get_User_Name_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionName		= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.tlMissionDec			= Get_Mission_Description_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.vStartPos			= Get_Coordinates_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.vCamPos				= Get_Cam_Vector_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.vCamHead				= Get_Cam_Heading_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.iType				= Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity(localMissionIdData.idCreator, localMissionIdData.idVariation)
	g_sGeneratedCloudHeaderData.gchdHeaderData.iSubType				= Get_SubType_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.iMinPlayers			= Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.iRank				= Get_Rank_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.iMaxPlayers			= Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.iRating				= Get_User_Rating_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.iLesRating			= lesRating
	g_sGeneratedCloudHeaderData.gchdHeaderData.iContactCharEnum		= Get_ContactAsInt_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.iBitSet				= Get_Options_Bitfield_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdHeaderData.iLaunchTimesBit		= Get_On_Hours_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sGeneratedCloudHeaderData.gchdInUse							= TRUE
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Generated_Header_Data()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Heist Unlock Rewards on Completion - Public Access Functions
// ===========================================================================================================

// PURPOSE:	Check if this is a Heist Reward Versus Mission
//
// REFERENCE PARAMS:		refMissionIdData		The missionId Data for the mission being checked
// RETURN VALUE:			BOOL					TRUE if this is a Heist Reward Versus Mission
FUNC BOOL Is_This_A_Heist_Reward_Versus_Mission(MP_MISSION_ID_DATA &refMissionIdData)

	IF (refMissionIdData.idMission != eFM_MISSION_CLOUD)
		RETURN FALSE
	ENDIF
	
	IF (Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData) != FMMC_MISSION_TYPE_VERSUS)
		RETURN FALSE
	ENDIF
	
	INT rootContentIdHash = Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	
	IF (Get_Heist_Reward_Versus_Mission_GroupID(rootContentIdHash) = NO_HEIST_REWARD_GROUP)
		// ...not a Heist Reward Versus Mission GroupID
		RETURN FALSE
	ENDIF
	
	// This is a Heist Reward Versus Mission GroupID
	RETURN TRUE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To check if this Heist Reward (Invite to) Versus mission is unlocked
//
// REFERENCE PARAMS:		refMissionIdData		The missionId Data for the mission being checked
// RETURN VALUE:			BOOL					TRUE if this is Heist Reward Versus Mission is unlocked, FALSE if not
//
// NOTES:	Assumes the pre-check has been done to ensure this is a Heist Reward Versus Mission
//			KGM 4/10/14: These missions no longer 'unlock' on heist strand completion, they unlock at rank 12 as normal, but the 'invite' to these missions unlocks
FUNC BOOL Is_This_Heist_Reward_Versus_Mission_Unlocked(MP_MISSION_ID_DATA &refMissionIdData)

	#IF IS_DEBUG_BUILD
		IF NOT (Is_This_A_Heist_Reward_Versus_Mission(refMissionIdData))
			PRINTLN("...KGM MP [ActSelect][Heist]: Is_This_Heist_Reward_Versus_Mission_Unlocked() - ERROR: This is not a Heist Reward Versus Mission: ", refMissionIdData.idCloudFilename)
			SCRIPT_ASSERT("Is_This_Heist_Reward_Versus_Mission_Unlocked() - ERROR: This is not a Heist Reward Versus Mission. Use Pre-Check. Tell Keith.")
			RETURN FALSE
		ENDIF
	#ENDIF
	
	INT rootContentIdHash = Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	
	g_eHeistRewardGroupID rewardGroupID = Get_Heist_Reward_Versus_Mission_GroupID(rootContentIdHash)
	
	#IF IS_DEBUG_BUILD
		IF (rewardGroupID = NO_HEIST_REWARD_GROUP)
			PRINTLN("...KGM MP [ActSelect][Heist]: Is_This_Heist_Reward_Versus_Mission_Unlocked() - ERROR: This mission isn't in a Heist Reward Group: ", refMissionIdData.idCloudFilename)
			SCRIPT_ASSERT("Is_This_Heist_Reward_Versus_Mission_Unlocked() - ERROR: This mission isn't in a Heist Reward Group. Tell Keith.")
			RETURN FALSE
		ENDIF
	#ENDIF
	
	RETURN (Is_This_Heist_Reward_Group_Unlocked(rewardGroupID))

ENDFUNC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Strand Mission Part One Header Data - Public Access Functions
// ===========================================================================================================

// PURPOSE:	Clear any stored Strand Mission Part One Header Data
PROC Clear_Strand_Mission_Part_One_Header_Data()
	
	g_structStrandMissionP1HeaderData emptyStrandMissionPartOneHeaderData
	g_sStrandMissionPartOne = emptyStrandMissionPartOneHeaderData
	
	g_sStrandMissionPartOne.smp1hdInUse							= FALSE
	g_sStrandMissionPartOne.smp1hdHeaderData.tlName				= ""
	g_sStrandMissionPartOne.smp1hdHeaderData.tlOwner			= ""
	g_sStrandMissionPartOne.smp1hdHeaderData.tlMissionName		= ""
	g_sStrandMissionPartOne.smp1hdHeaderData.tlMissionDec		= ""
	g_sStrandMissionPartOne.smp1hdHeaderData.vStartPos			= << 0.0, 0.0, 0.0 >>
	g_sStrandMissionPartOne.smp1hdHeaderData.vCamPos			= << 0.0, 0.0, 0.0 >>
	g_sStrandMissionPartOne.smp1hdHeaderData.iBitSet			= ALL_MISSIONS_SHARED_BITS_CLEAR
	
	PRINTLN(".KGM MP [ActSelect][Heist]: Strand Mission Part One Header Data Cleared.")
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
// PURPOSE:	Output the full details of the Strand Mission Part One Header Data struct
PROC Debug_Output_Strand_Mission_Part_One_Header_Data()

	TEXT_LABEL_63 theHoursDescription = Convert_Hours_From_Bitfield_To_TL(g_sStrandMissionPartOne.smp1hdHeaderData.iLaunchTimesBit)
	
	NET_PRINT(".KGM [ActSelect][Heist]: Strand Mission Part One Header Data:")	NET_NL()
	
	NET_PRINT("      Cloud Filename  : ")	NET_PRINT(g_sStrandMissionPartOne.smp1hdHeaderData.tlName)						NET_NL()
	NET_PRINT("      Owner           : ")	NET_PRINT(g_sStrandMissionPartOne.smp1hdHeaderData.tlOwner )					NET_NL()
	NET_PRINT("      Mission Name    : ")	NET_PRINT(g_sStrandMissionPartOne.smp1hdHeaderData.tlMissionName)				NET_NL()
	NET_PRINT("      Mission Desc    : ")	NET_PRINT(g_sStrandMissionPartOne.smp1hdHeaderData.tlMissionDec)				NET_NL()
	NET_PRINT("      Start Pos       : ")	NET_PRINT_VECTOR(g_sStrandMissionPartOne.smp1hdHeaderData.vStartPos)			NET_NL()
	NET_PRINT("      Camera Pos      : ")	NET_PRINT_VECTOR(g_sStrandMissionPartOne.smp1hdHeaderData.vCamPos)				NET_NL()
	NET_PRINT("      Camera Heading  : ")	NET_PRINT_VECTOR(g_sStrandMissionPartOne.smp1hdHeaderData.vCamHead)				NET_NL()
	NET_PRINT("      FM Type         : ")	NET_PRINT_INT(g_sStrandMissionPartOne.smp1hdHeaderData.iType)					NET_NL()
	NET_PRINT("      FM Subtype      : ")	NET_PRINT_INT(g_sStrandMissionPartOne.smp1hdHeaderData.iSubtype)				NET_NL()
	NET_PRINT("      Min Players     : ")	NET_PRINT_INT(g_sStrandMissionPartOne.smp1hdHeaderData.iMinPlayers)				NET_NL()
	NET_PRINT("      Rank            : ")	NET_PRINT_INT(g_sStrandMissionPartOne.smp1hdHeaderData.iRank)					NET_NL()
	NET_PRINT("      Max Players     : ")	NET_PRINT_INT(g_sStrandMissionPartOne.smp1hdHeaderData.iMaxPlayers)				NET_NL()
	NET_PRINT("      Rating          : ")	NET_PRINT_INT(g_sStrandMissionPartOne.smp1hdHeaderData.iRating)					NET_NL()
	NET_PRINT("      Les Rating      : ")	NET_PRINT_INT(g_sStrandMissionPartOne.smp1hdHeaderData.iLesRating)				NET_NL()
	NET_PRINT("      Contact (as INT): ")	NET_PRINT_INT(g_sStrandMissionPartOne.smp1hdHeaderData.iContactCharEnum)		NET_NL()
	NET_PRINT("      Bitset          : ")	NET_PRINT_INT(g_sStrandMissionPartOne.smp1hdHeaderData.iBitSet)					NET_NL()
	NET_PRINT("      On Hours        : ")	NET_PRINT(theHoursDescription)													NET_NL()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Fill the stored Strand Mission Part One Header Data from the locally available data
//
// INPUT PARAMS:		paramStartCoords		The focus mission's start position for the mission
// RETURN PARAMS:		refMissionIdData		The MissionIdData of the local data
PROC Fill_Strand_Mission_Part_One_Details_From_Local_Data(VECTOR paramStartCoords, MP_MISSION_ID_DATA &refMissionIdData)
	
	Clear_Strand_Mission_Part_One_Header_Data()
	
	// Make a copy of the MissionIdData that ignores the Shared Data value so that local data is retrieved
	#IF IS_DEBUG_BUILD
		IF (refMissionIdData.idSharedRegID != ILLEGAL_SHARED_REG_ID)
			PRINTLN("...KGM MP [MissionInfo]: Storing Generate Cloud Header Data from local data. IGNORING SHARED REGID: ", refMissionIdData.idSharedRegID)
		ENDIF
	#ENDIF
	
	MP_MISSION_ID_DATA localMissionIdData = refMissionIdData
	localMissionIdData.idSharedRegID = ILLEGAL_SHARED_REG_ID
	
	INT lesRating = 0
	#IF IS_DEBUG_BUILD
		lesRating = Get_Les_Rating_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	#ENDIF
	
	g_sStrandMissionPartOne.smp1hdHeaderData.tlName					= refMissionIdData.idCloudFilename
	g_sStrandMissionPartOne.smp1hdHeaderData.tlOwner				= Get_User_Name_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.tlMissionName			= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.tlMissionDec			= Get_Mission_Description_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.vStartPos				= paramStartCoords
	g_sStrandMissionPartOne.smp1hdHeaderData.vCamPos				= Get_Cam_Vector_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.vCamHead				= Get_Cam_Heading_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.iType					= Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity(localMissionIdData.idCreator, localMissionIdData.idVariation)
	g_sStrandMissionPartOne.smp1hdHeaderData.iSubType				= Get_SubType_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.iMinPlayers			= Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.iRank					= Get_Rank_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.iMaxPlayers			= Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.iRating				= Get_User_Rating_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.iLesRating				= lesRating
	g_sStrandMissionPartOne.smp1hdHeaderData.iContactCharEnum		= Get_ContactAsInt_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.iBitSet				= Get_Options_Bitfield_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdHeaderData.iLaunchTimesBit		= Get_On_Hours_For_FM_Cloud_Loaded_Activity(localMissionIdData)
	g_sStrandMissionPartOne.smp1hdInUse								= TRUE
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Strand_Mission_Part_One_Header_Data()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store a copy of the Strand Mission Part One Header Data from the correct source
//
// INPUT PARAMS:			paramStartCoords		The focus mission's start position for the mission
// REFERENCE PARAMS:		refMissionIdData		The missionId Data for the mission being checked
PROC Store_Strand_Mission_Part_One_Header_Data(VECTOR paramStartCoords, MP_MISSION_ID_DATA &refMissionIdData)

	g_sStrandMissionPartOne.smp1hdInUse	= TRUE

	// Check if this cloud-loaded header data is on the shared missions cloud data array
	// ...shared data should already have the correct start coords
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(refMissionIdData))
		g_sStrandMissionPartOne.smp1hdHeaderData = GlobalServerBD_MissionsShared.cloudDetails[refMissionIdData.idSharedRegID].mscHeaderData
		
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [ActSelect][Heist]: Strand Mission Part One Header Data Stored from Shared Mission Data. ContentID: ", g_sStrandMissionPartOne.smp1hdHeaderData.tlName)
			Debug_Output_Strand_Mission_Part_One_Header_Data()
		#ENDIF
		
		EXIT
	ENDIF

	// The data isn't stored as a shared activity, so check if it is Generated Cloud Header Data
	// ...generated data should already have the correct start coords
	IF (Is_This_Generated_Cloud_Header_Data(refMissionIdData.idVariation))
		IF (Does_MissionId_Data_Match_Generated_Cloud_Header_Data(refMissionIdData))
			g_sStrandMissionPartOne.smp1hdHeaderData = g_sGeneratedCloudHeaderData.gchdHeaderData
		
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [ActSelect][Heist]: Strand Mission Part One Header Data Stored from Generated Header Data. ContentID: ", g_sStrandMissionPartOne.smp1hdHeaderData.tlName)
				Debug_Output_Strand_Mission_Part_One_Header_Data()
			#ENDIF
			
			EXIT
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [ActSelect][Heist]: Store_Strand_Mission_Part_One_Header_Data() - ERROR: This is Generated Data but the filenames don't match. NO DATA STORED.")
			PRINTLN(".KGM [ActSelect][Heist]: ...Generated ContentID: ", g_sGeneratedCloudHeaderData.gchdHeaderData.tlName, ". Passed ContentID: ", refMissionIdData.idCloudFilename)
			SCRIPT_ASSERT("Store_Strand_Mission_Part_One_Header_Data() - ERROR: This is Generated Data but the filenames don't match. NO DATA STORED. Tell Keith.")
		#ENDIF
	
		g_sStrandMissionPartOne.smp1hdInUse	= FALSE
		EXIT
	ENDIF

	// Get the array position for the data on the global cloud data arrays
	// NOTE: This will perform a search through all data if the data is out of sync or missing
	INT arrayPos = Get_ArrayPos_Of_Cloud_Loaded_Activity_Data(refMissionIdData)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [ActSelect][Heist]: Store_Strand_Mission_Part_One_Header_Data() - ERROR: Failed to find the Mission in the Arrays. NO DATA STORED.")
			PRINTLN(".KGM [ActSelect][Heist]: ...Passed ContentID: ", refMissionIdData.idCloudFilename)
			SCRIPT_ASSERT("Store_Strand_Mission_Part_One_Header_Data() - ERROR: Failed to find the Mission in the Arrays. NO DATA STORED. Tell Keith.")
		#ENDIF
	
		g_sStrandMissionPartOne.smp1hdInUse	= FALSE
		EXIT
	ENDIF
	
	// Store the correct data
	// NOTE: Assume only Heists can be Strand Missions for now, so assume only on Rockstar Created array
	SWITCH (refMissionIdData.idCreator)
		CASE FMMC_ROCKSTAR_CREATOR_ID
			IF !SHOULD_STRAND_MISSION_RESTART_ON_FIRST_MISSION()
			OR IS_STRING_NULL_OR_EMPTY(g_sStrandMissionPartOne.smp1hdHeaderData.tlName)
				Fill_Strand_Mission_Part_One_Details_From_Local_Data(paramStartCoords, refMissionIdData)
				PRINTLN(".KGM [ActSelect][Heist]: Strand Mission Part One Header Data Stored from Rockstar Created Array: ", arrayPos, ". ContentID: ", g_sStrandMissionPartOne.smp1hdHeaderData.tlName)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN(".KGM [ActSelect][Heist]: Strand Mission Part One Header Data Not Updating, keeping as ContentID: ", g_sStrandMissionPartOne.smp1hdHeaderData.tlName)
				#ENDIF
			ENDIF
			EXIT
	ENDSWITCH
	
	// Error - Unknown or Unsupported Creator ID
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: Store_Strand_Mission_Part_One_Header_Data() - ERROR: Illegal Creator ID. NO DATA STORED.")
		PRINTLN(".KGM [Heist]: ...Passed in Creator ID: ", refMissionIdData.idCreator)
	#ENDIF
	
	g_sStrandMissionPartOne.smp1hdInUse	= FALSE
	EXIT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Create Generated header data from teh Strand Mission Part One Header Data
PROC Generate_Cloud_Header_Data_From_Strand_Mission_Part_One_Header_Data()
	
	g_sGeneratedCloudHeaderData.gchdHeaderData						= g_sStrandMissionPartOne.smp1hdHeaderData
	g_sGeneratedCloudHeaderData.gchdInUse							= TRUE
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Generated_Header_Data()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the start coords for the stored Strand Mission Part One
FUNC VECTOR Get_Stored_Strand_Mission_Part_One_Start_Coords()

	VECTOR theReturn = << 0.0, 0.0, 0.0 >>
	
	IF NOT (g_sStrandMissionPartOne.smp1hdInUse)
		PRINTLN(".KGM [ActSelect][Heist]: Get_Stored_Strand_Mission_Part_One_Start_Coords() - ERROR: There is no stored data. Returning: ", theReturn)
		RETURN (theReturn)
	ENDIF
	
	theReturn = g_sStrandMissionPartOne.smp1hdHeaderData.vStartPos
	
	PRINTLN(".KGM [Heist]: Get_Stored_Strand_Mission_Part_One_Start_Coords() - Returning: ", theReturn)
	RETURN (theReturn)
	
ENDFUNC




// ===========================================================================================================
//      Copy Registered Data From Full Mission Data - Public Access Functions
// ===========================================================================================================

// PURPOSE:	Store the Copy of Registered Data from Full Mission Data
//
// REFERENCE PARAMS:		refMissionIdData		The missionId Data for the mission being checked
//
// NOTES:	This was added for SCTV players so that they woul dbe able to 'replay' another player's UGC
PROC Store_Copy_Registered_Data_From_Full_Mission_Data(MP_MISSION_ID_DATA &refMissionIdData)

	// First, Generate the data from the Full Mission Data
	Generate_Cloud_Header_Data_From_Full_Mission_Data(refMissionIdData.idCloudFilename)

	g_sCopyRegSharedMissionData.crsmdMissionID	= refMissionIdData.idMission
	g_sCopyRegSharedMissionData.crsmdVariation	= GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION
	g_sCopyRegSharedMissionData.crsmdCreatorID	= NATIVE_TO_INT(PLAYER_ID())

	// Then, copy the generated data into the Copy Reg Shared data
	PRINTLN(".KGM [MShared][CloudData]: Filling the Copy Shared Mission data struct based on the Full Mission Details.")
	
	g_sCopyRegSharedMissionData.crsmdHeaderData		= g_sGeneratedCloudHeaderData.gchdHeaderData
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Mission ID      : ")	NET_PRINT_INT(ENUM_TO_INT(g_sCopyRegSharedMissionData.crsmdMissionID))					NET_NL()
		NET_PRINT("      Variation       : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdVariation)								NET_NL()
		NET_PRINT("      Creator         : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdCreatorID)								NET_NL()
		
		TEXT_LABEL_63 theHoursDescription = Convert_Hours_From_Bitfield_To_TL(g_sCopyRegSharedMissionData.crsmdHeaderData.iLaunchTimesBit)
		
		NET_PRINT("      Cloud Filename  : ")	NET_PRINT(g_sCopyRegSharedMissionData.crsmdHeaderData.tlName)							NET_NL()
		NET_PRINT("      Owner           : ")	NET_PRINT(g_sCopyRegSharedMissionData.crsmdHeaderData.tlOwner )							NET_NL()
		NET_PRINT("      Mission Name    : ")	NET_PRINT(g_sCopyRegSharedMissionData.crsmdHeaderData.tlMissionName)					NET_NL()
		NET_PRINT("      Mission Desc    : ")	NET_PRINT(g_sCopyRegSharedMissionData.crsmdHeaderData.tlMissionDec)						NET_NL()
		NET_PRINT("      Start Pos       : ")	NET_PRINT_VECTOR(g_sCopyRegSharedMissionData.crsmdHeaderData.vStartPos)					NET_NL()
		NET_PRINT("      Camera Pos      : ")	NET_PRINT_VECTOR(g_sCopyRegSharedMissionData.crsmdHeaderData.vCamPos)					NET_NL()
		NET_PRINT("      Camera Heading  : ")	NET_PRINT_VECTOR(g_sCopyRegSharedMissionData.crsmdHeaderData.vCamHead)					NET_NL()
		NET_PRINT("      FM Type         : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdHeaderData.iType)						NET_NL()
		NET_PRINT("      FM Subtype      : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdHeaderData.iSubtype)						NET_NL()
		NET_PRINT("      Min Players     : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdHeaderData.iMinPlayers)					NET_NL()
		NET_PRINT("      Rank            : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdHeaderData.iRank)						NET_NL()
		NET_PRINT("      Max Players     : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdHeaderData.iMaxPlayers)					NET_NL()
		NET_PRINT("      Rating          : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdHeaderData.iRating)						NET_NL()
		NET_PRINT("      Les Rating      : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdHeaderData.iLesRating)					NET_NL()
		NET_PRINT("      Contact (as INT): ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdHeaderData.iContactCharEnum)				NET_NL()
		NET_PRINT("      Bitset          : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdHeaderData.iBitSet)						NET_NL()
		NET_PRINT("      On Hours        : ")	NET_PRINT(theHoursDescription)															NET_NL()
	#ENDIF

ENDPROC








