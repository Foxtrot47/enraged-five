USING "globals.sch"
USING "net_prints.sch"
USING "net_gang_boss_common.sch"
USING "net_include.sch"
USING "net_gang_boss_drops.sch"
USING "net_gang_boss_limo.sch"
USING "leader_board_common.sch"
USING "net_spectator_cam.sch"
USING "net_spectator_cam_common.sch"
USING "net_gang_boss_common.sch"
USING "net_gang_boss.sch"
USING "dm_leaderboard.sch"
USING "am_common_ui.sch"

///    Declair:
///    GB_BIKER_RACE_DATA sBikerRace
///    
///    Add to server BD:
///    GB_BIKER_RACE_SERVER_DATA sRaceServerVars
///    
///    Call in pre game:
///    GB_BIKER_RACE_PROCESS_PREGAME()
///    
///    Call in clean up:
///    GB_BIKER_RACE_CLEAN_UP()
///    
///    Call is server Loop:
///    GB_BIKER_RACE_SERVER_MAINTAIN(serverBD.sRaceServerVars, iParticipant)
///    
///    Add a call to this once when the blip has been added with the destination blip:
///    GB_BIKER_RACE_SET_UP(sBikerRace, vector, blipDriveToLocation)
///    
///    Call somewhere in your maintain:
///    GB_BIKER_RACE_MAINTAIN(sBikerRace, serverBD.sRaceServerVars)
///    When this returns TRUE, the race is over

CONST_INT ciCOUNT_DOWN_BIKER_RACE 30000

CONST_INT ciBIKER_RACE_HELP 			0
CONST_INT ciBIKER_RACE_WINNER_TICKER	1
CONST_INT ciBIKER_RACE_SET_ROUTE		2
/// Local vars
STRUCT GB_BIKER_RACE_LB_DATA
	FLOAT fDist
	PLAYER_INDEX PlayerId
ENDSTRUCT 
STRUCT GB_BIKER_RACE_DATA
	INT iBitSet
	BLIP_INDEX DestinationBlip
	VECTOR vDestination
	SCRIPT_TIMER stTimerHelp
	GB_BIKER_RACE_LB_DATA sLbData[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
ENDSTRUCT

STRUCT GB_BIKER_RACE_SERVER_DATA
	BOOL bMultiRace
	BOOL bSetUp = FALSE
	INT iWinnerPart[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
ENDSTRUCT

//Set up, called by all before registering as BD data
PROC GB_BIKER_RACE_PROCESS_PREGAME(GB_BIKER_RACE_SERVER_DATA &sServerVars)
	CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] GB_BIKER_RACE_PROCESS_PREGAME")
	INT iLoop
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS iLoop
		sServerVars.iWinnerPart[iLoop] = -1
	ENDREPEAT
	GB_CLEAR_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_KICKED_OFF_RACE)
	GB_CLEAR_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_FINISHED_RACE)
ENDPROC

//Clean up, call at end of script
PROC GB_BIKER_RACE_CLEAN_UP()
	CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] GB_BIKER_RACE_CLEAN_UP")
	GB_CLEAR_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_KICKED_OFF_RACE)
	GB_CLEAR_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_FINISHED_RACE)
ENDPROC
PROC GB_BIKER_RACE_SET_OVER_DUE_TO_INCOMPATIBLE_VARIATION(GB_BIKER_RACE_SERVER_DATA &sServerVars)
	CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] GB_BIKER_RACE_SET_OVER_DUE_TO_INCOMPATIBLE_VARIATION")
	sServerVars.iWinnerPart[0] = -5
ENDPROC
PROC GB_BIKER_RACE_SET_UP_BLIP(GB_BIKER_RACE_DATA &sBikerRace, BLIP_INDEX DestinationBlip)
	CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] GB_BIKER_RACE_SET_UP_BLIP")
	sBikerRace.DestinationBlip = DestinationBlip
ENDPROC
PROC GB_BIKER_RACE_SET_UP(GB_BIKER_RACE_DATA &sBikerRace, GB_BIKER_RACE_SERVER_DATA &sServerVars, VECTOR vDestination, BLIP_INDEX DestinationBlip = NULL)
	CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] GB_BIKER_RACE_SET_UP - sBikerRace.vDestination = ", vDestination)
	sBikerRace.vDestination = vDestination
	GB_BIKER_RACE_SET_UP_BLIP(sBikerRace, DestinationBlip)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		sServerVars.bSetUp = TRUE
		IF NOT sServerVars.bMultiRace
			PLAYER_INDEX bossID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			IF NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(bossID, eGB_GLOBAL_CLIENT_BITSET_1_RACE_2_POINT_ENABLED)
			OR g_sMPTunables.bBIKER_DISABLE_RACE_IN_MISSION
				sServerVars.iWinnerPart[0] = -2
				CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] GB_BIKER_RACE_SET_UP -  NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(bossID, eGB_GLOBAL_CLIENT_BITSET_1_RACE_2_POINT_ENABLED) - sServerVars.iWinnerPart[0] = ", sServerVars.iWinnerPart[0])
			//If there's only one player then there's no race
			ELIF GB_GET_NUM_GOONS_IN_PLAYER_GANG(bossID) <= 0
				sServerVars.iWinnerPart[0] = -3
				CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] GB_BIKER_RACE_SET_UP -  GB_GET_NUM_GOONS_IN_PLAYER_GANG = ", GB_GET_NUM_GOONS_IN_PLAYER_GANG(bossID), ",  - sServerVars.iWinnerPart[0] = ", sServerVars.iWinnerPart[0])
			//If there's no road cptain set then clean up
			ELIF NOT GB_BIKER_RACE_IS_THERE_A_ROAD_CAPTAIN(bossID)
				sServerVars.iWinnerPart[0] = -4
				CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] GB_BIKER_RACE_SET_UP -  NOT GB_BIKER_RACE_IS_THERE_A_ROAD_CAPTAIN() - sServerVars.iWinnerPart[0] = ", sServerVars.iWinnerPart[0])
			ENDIF
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_IgnoreGBMissionLaunchChecksP2p")
				sServerVars.iWinnerPart[0] = -1
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL GB_BIKER_CORRECT_DISTANCE_BASED_ON_PLACE(FLOAT &fDist, INT iParticipant, INT &iWinnerPart[])
	INT iLoop
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS iLoop
		IF iParticipant = iWinnerPart[iLoop]
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBP2P] - CORRECT_DISTANCE_BASED_ON_PLACE - ", iParticipant, " = iWinnerPart[", iLoop, "]")
			fDist = 0.001 * (iLoop+1)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_BIKER_RACE_OKAY_TO_SEND_REACHED_DESTINATION_EVENT(PED_INDEX passedInPed)
	IF IS_PED_IN_ANY_VEHICLE(passedInPed)
		VEHICLE_INDEX thisVeh  = GET_VEHICLE_PED_IS_IN(passedInPed)
		IF IS_VEHICLE_DRIVEABLE(thisVeh)
			PED_INDEX thisPed
			INT i
			REPEAT (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(thisVeh)+1) i
				thisPed = GET_PED_IN_VEHICLE_SEAT(thisVeh, INT_TO_ENUM(VEHICLE_SEAT, i-1))
				IF DOES_ENTITY_EXIST(thisPed)
					IF NOT IS_PED_INJURED(thisPed)
						IF IS_PED_A_PLAYER(thisPed)
							
							PLAYER_INDEX l_player = NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed)
							IF l_player != NETWORK_GET_PLAYER_INDEX_FROM_PED(passedInPed)
								IF NETWORK_IS_PLAYER_A_PARTICIPANT(l_player)
									IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(l_player))
										CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - OKAY_TO_SEND_REACHED_DESTINATION_EVENT - FALSE - NOT FIRST PARTICIPANT IN VEHICLE")
										RETURN FALSE
									ENDIF
								ENDIF
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - OKAY_TO_SEND_REACHED_DESTINATION_EVENT - TRUE - FIRST PARTICIPANT IN VEHICLE")
								RETURN TRUE
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - OKAY_TO_SEND_REACHED_DESTINATION_EVENT - TRUE")
	RETURN TRUE
ENDFUNC


PROC GB_BIKER_RACE_SET_IS_MULTI_RACE(GB_BIKER_RACE_SERVER_DATA &sServerVars)
	CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - GB_BIKER_RACE_SET_IS_MULTI_RACE")
	sServerVars.bMultiRace = TRUE
ENDPROC
FUNC BOOL GB_BIKER_RACE_IS_RACE_OVER(GB_BIKER_RACE_SERVER_DATA &sServerVars)
	RETURN sServerVars.iWinnerPart[0] != -1
ENDFUNC

PROC GB_BIKER_RACE_PARRT_POS(INT &iWinnerPart[], INT iParticipant)
	INT iLoop
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS iLoop
		IF iWinnerPart[iLoop] = iParticipant
			EXIT
		ENDIF
		IF iWinnerPart[iLoop] = -1
			iWinnerPart[iLoop] = iParticipant
			CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - iWinnerPart[", iLoop, "] = ", iParticipant)
			#IF IS_DEBUG_BUILD
			IF iLoop = 0
				CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - WINNER PLAYER  = ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iParticipant)))
			ENDIF
			#ENDIF
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC
PROC GB_BIKER_RACE_SERVER_MAINTAIN(GB_BIKER_RACE_SERVER_DATA &sServerVars, INT iParticipant)
	IF NOT GB_BIKER_RACE_IS_RACE_OVER(sServerVars)
	OR sServerVars.bMultiRace
		PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
		IF GB_IS_GLOBAL_CLIENT_BIT1_SET(PlayerId, eGB_GLOBAL_CLIENT_BITSET_1_FINISHED_RACE)
		AND NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PlayerId, eGB_GLOBAL_CLIENT_BITSET_1_KICKED_OFF_RACE)
		AND GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(PlayerId, GB_GET_LOCAL_PLAYER_MISSION_HOST())
			CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - OKAY_TO_SEND_REACHED_DESTINATION_EVENT - sServerVars.iWinnerPart = ", iParticipant)
			GB_BIKER_RACE_PARRT_POS(sServerVars.iWinnerPart, iParticipant)
		ENDIF
	ENDIF
ENDPROC

PROC GB_BIKER_RACE_MAINTAIN_PRINT_WINNING_TICKER(GB_BIKER_RACE_DATA &sBikerRace, GB_BIKER_RACE_SERVER_DATA &sServerVars)
	IF sServerVars.iWinnerPart[0] >= 0
	AND NOT IS_BIT_SET(sBikerRace.iBitSet, ciBIKER_RACE_WINNER_TICKER)
		CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - MAINTAIN_PRINT_WINNING_TICKER")
		PLAYER_INDEX piWinningPlayer
		PARTICIPANT_INDEX piWinnerPart = INT_TO_PARTICIPANTINDEX(sServerVars.iWinnerPart[0])
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piWinnerPart)
			CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - NETWORK_IS_PARTICIPANT_ACTIVE(serverBD.iWinnerPart[0])")
			piWinningPlayer = NETWORK_GET_PLAYER_INDEX(piWinnerPart)
			IF NETWORK_IS_PLAYER_ACTIVE(piWinningPlayer)
				CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - NETWORK_IS_PLAYER_ACTIVE(serverBD.iWinnerPart) - player = ", GET_PLAYER_NAME(piWinningPlayer), " - PRINT_TICKER_WITH_PLAYER_NAME called")
				PRINT_TICKER_WITH_PLAYER_NAME("GB_P2P_WT0", piWinningPlayer)
				PLAY_SOUND_FRONTEND(-1, "Boss_Message_Orange", "GTAO_Biker_FM_Soundset", FALSE)
			ENDIF	
		ENDIF	
		IF sServerVars.iWinnerPart[0] = NATIVE_TO_INT(PARTICIPANT_ID())
			CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION")
			NEXT_RP_ADDITION_SHOW_RANKBAR()
			GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "", XPTYPE_SKILL,XPCATEGORY_COLLECT_CHECKPOINT, g_sMPTunables.iBIKER_RACE_RP_REWARD)
		ENDIF
		SET_BIT(sBikerRace.iBitSet, ciBIKER_RACE_WINNER_TICKER)
	ENDIF	
ENDPROC


//Call till it returns TRUE
FUNC BOOL GB_BIKER_RACE_MAINTAIN(GB_BIKER_RACE_DATA &sBikerRace, GB_BIKER_RACE_SERVER_DATA &sServerVars, FLOAT fRadius = 50.0)

	IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(PLAYER_ID(), GB_GET_LOCAL_PLAYER_MISSION_HOST())
		RETURN TRUE
	ENDIF

	//Local to funtion
	INT iParticipant, iPartCount, i, j
	PED_INDEX PlayerPedId, thisPed
	GB_BIKER_RACE_LB_DATA sLbDataTemp
	VEHICLE_INDEX thisVeh
	PLAYER_INDEX PlayerIdTemp
	
	IF sServerVars.bMultiRace
		GB_BIKER_RACE_MAINTAIN_PRINT_WINNING_TICKER(sBikerRace, sServerVars)
	ELIF GB_BIKER_RACE_IS_RACE_OVER(sServerVars)
		GB_BIKER_RACE_MAINTAIN_PRINT_WINNING_TICKER(sBikerRace, sServerVars)
		RETURN TRUE
	ENDIF
	
	IF NOT sServerVars.bSetUp
		RETURN FALSE
	ENDIF 
	
	IF g_sBIK_Telemetry_data.m_RaceToLocationLaunched != 1
		CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - g_sBIK_Telemetry_data.m_RaceToLocationLaunched = 1")
		g_sBIK_Telemetry_data.m_RaceToLocationLaunched = 1
	ENDIF
	
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS i
		sBikerRace.sLbData[i].PlayerId = INVALID_PLAYER_INDEX()
	ENDREPEAT

	IF NOT sServerVars.bMultiRace
		IF NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_FINISHED_RACE)
		AND NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_KICKED_OFF_RACE)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sBikerRace.vDestination) < (fRadius * fRadius)
					IF GB_BIKER_RACE_OKAY_TO_SEND_REACHED_DESTINATION_EVENT(PLAYER_PED_ID())
						GB_SET_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_FINISHED_RACE)
						CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - PROCESS_REACHING_DESTINATION - REACHED DESTINATION - ", sBikerRace.vDestination)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(sBikerRace.iBitSet, ciBIKER_RACE_HELP)
		IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_HELP_MESSAGE_ON_SCREEN()
		AND MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
			IF HAS_NET_TIMER_EXPIRED(sBikerRace.stTimerHelp, 500)
				IF sServerVars.bMultiRace
					PRINT_HELP_NO_SOUND("SBCONTRA_R1H")
				ELSE
					PRINT_HELP_NO_SOUND("BG_P2P_GOD4")
				ENDIF
				GB_SET_GANG_BOSS_HELP_BACKGROUND()
				SET_BIT(sBikerRace.iBitSet, ciBIKER_RACE_HELP)
			ENDIF
		ENDIF
	ENDIF
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		//If they are active
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PlayerIdTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF NOT GB_IS_THIS_PLAYER_USING_SPECTATE(PlayerIdTemp)
			AND NOT IS_PLAYER_SCTV(PlayerIdTemp)
			AND NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PlayerIdTemp, eGB_GLOBAL_CLIENT_BITSET_1_KICKED_OFF_RACE)	//playerBD[iParticipant].iClientBitSet, biP_OffBikeTooLong)
			AND GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(PlayerIdTemp, GB_GET_LOCAL_PLAYER_MISSION_HOST())
				sBikerRace.sLbData[iPartCount].PlayerId = PlayerIdTemp
				PlayerPedId = GET_PLAYER_PED(sBikerRace.sLbData[iPartCount].PlayerId)
				//And not injured
				IF NOT IS_PED_INJURED(PlayerPedId)
					IF NOT GB_BIKER_CORRECT_DISTANCE_BASED_ON_PLACE(sBikerRace.sLbData[iPartCount].fDist, iParticipant, sServerVars.iWinnerPart)
						VECTOR vPos = GET_ENTITY_COORDS(PlayerPedId)
						IF IS_PLAYER_IN_PROPERTY(sBikerRace.sLbData[iPartCount].PlayerId, TRUE, TRUE)
							vPos = GET_PLAYER_PERCEIVED_COORDS(sBikerRace.sLbData[iPartCount].PlayerId)
						ELIF vPos.z < -40.0
						AND sBikerRace.sLbData[iPartCount].PlayerId != PLAYER_ID()
							 vPos =  NETWORK_GET_LAST_PLAYER_POS_RECEIVED_OVER_NETWORK(sBikerRace.sLbData[iPartCount].PlayerId)
						ENDIF
						sBikerRace.sLbData[iPartCount].fDist = GET_DISTANCE_BETWEEN_COORDS(vPos, sBikerRace.vDestination)
						//If we're in a car
						IF IS_PED_IN_ANY_VEHICLE(PlayerPedId)
							//Get the car
							thisVeh  = GET_VEHICLE_PED_IS_IN(PlayerPedId)
							//If you can drive it
							IF IS_VEHICLE_DRIVEABLE(thisVeh)
								//Get the driver
								thisPed = GET_PED_IN_VEHICLE_SEAT(thisVeh, VS_DRIVER)
								//If the driver is not the PED we're looking at
								IF thisPed != PlayerPedId
									//If they are ok
									IF DOES_ENTITY_EXIST(thisPed)
									AND NOT IS_PED_INJURED(thisPed)
										//If they are are player
										IF IS_PED_A_PLAYER(thisPed)
											PLAYER_INDEX l_player = NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed)
											//And they are a participant
											IF NETWORK_IS_PLAYER_A_PARTICIPANT(l_player)
											AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(l_player))
												//CPRINTLN(DEBUG_NET_AMBIENT, " [BIKER_RACE] - MAINTTAIN_FIRST_PLACE_UI - fSameCarDisadvantage = 1.0")
												sBikerRace.sLbData[iPartCount].fDist += 0.001		
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					iPartCount++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	FOR i = 0 TO GB_MAX_GANG_SIZE_INCLUDING_BOSS-1 STEP 1
		FOR j = i TO GB_MAX_GANG_SIZE_INCLUDING_BOSS-1 STEP 1
			IF sBikerRace.sLbData[i].fDist > sBikerRace.sLbData[j].fDist
			AND	sBikerRace.sLbData[j].fDist != 0
				sLbDataTemp = sBikerRace.sLbData[i]
				sBikerRace.sLbData[i] = sBikerRace.sLbData[j]
				sBikerRace.sLbData[j] = sLbDataTemp
			ENDIF
		ENDFOR
	ENDFOR

	DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, g_GBLeaderboardStruct.fmDpadStruct)
	IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
		IF sBikerRace.sLbData[0].PlayerId != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_ACTIVE(sBikerRace.sLbData[0].PlayerId)

			IF NOT IS_BIT_SET(sBikerRace.iBitSet, ciBIKER_RACE_SET_ROUTE)
				IF DOES_BLIP_EXIST(sBikerRace.DestinationBlip)
					SET_BLIP_ROUTE(sBikerRace.DestinationBlip, TRUE)
					SET_BIT(sBikerRace.iBitSet, ciBIKER_RACE_SET_ROUTE)
				ENDIF
			ENDIF

			
			//Draw the bottom right UI
			STRING NumberString = "HUD_METNUM"
			FLOAT fDist[GB_MAX_GANG_SIZE_INCLUDING_BOSS]
			IF SHOULD_USE_METRIC_MEASUREMENTS()
				REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS i
					fDist[i] = sBikerRace.sLbData[i].fDist
				ENDREPEAT
			ELSE
				REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS i
					fDist[i] = CONVERT_METERS_TO_FEET(sBikerRace.sLbData[i].fDist)
				ENDREPEAT
				NumberString = "HUD_FTNUM"
			ENDIF
			BOOL bForceRebuildNames
			INT iPlayer = -1
			BOOL bDisPlay4
			REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS i
				IF i < 3
					IF sBikerRace.sLbData[i].PlayerId = PLAYER_ID()
						bDisPlay4 = TRUE
					ENDIF
				ELIF sBikerRace.sLbData[i].PlayerId = PLAYER_ID()
					iPlayer = i
				ENDIF
			ENDREPEAT
			IF bDisPlay4
			AND iPlayer != -1
				BOTTOM_RIGHT_UI_1STFLOAT_2NDFLOAT_3RDFLOAT_4THFLOAT_ATTEMPT_LOCALFLOAT_TIMER(	sBikerRace.sLbData[0].PlayerId, 		//PLAYER_INDEX First_Player_Index, 
																								sBikerRace.sLbData[1].PlayerId, 		//PLAYER_INDEX Second_Player_Index, 
																								sBikerRace.sLbData[2].PlayerId, 		//PLAYER_INDEX Third_Player_Index, 
																								sBikerRace.sLbData[iPlayer].PlayerId, 	//PLAYER_INDEX Third_Player_Index, 
																								fDist[0], 								//FLOAT First_Player_Score, 
																								fDist[1], 								//FLOAT Second_Player_Score, 
																								fDist[2], 								//FLOAT Third_Player_Score, 
																								fDist[iPlayer], 						//FLOAT Attempt_Display, 
																								0.0, 									//FLOAT iLocal_Score_Display, 
																								0.0, 									//FLOAT iLocal_Score_Display, 
																								-10, 									//INT Event_Timer_Display,
																								NumberString,							//STRING UnitString,
																								bForceRebuildNames,						//BOOL& bForceRebuildNames,
																								HUD_COLOUR_WHITE, 						//HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																								FALSE,									//BOOL bDisplayAttempt = TRUE, 
																								DEFAULT,								//INT FlashBackgroundtimerMS = 0,
																								"GB_CHAL_END",							//STRING ModeName = NULL, 
																								FMEVENT_SCORETITLE_NONE)				//FMEVENT_SCORETITLE LocalDisplayType = FMEVENT_SCORETITLE_GAMERTAG)
			ELSE
				BOTTOM_RIGHT_UI_1STFLOAT_2NDFLOAT_3RDFLOAT_ATTEMPT_LOCALFLOAT_TIMER(	sBikerRace.sLbData[0].PlayerId, 	//PLAYER_INDEX First_Player_Index, 
																						sBikerRace.sLbData[1].PlayerId, 	//PLAYER_INDEX Second_Player_Index, 
																						sBikerRace.sLbData[2].PlayerId, 	//PLAYER_INDEX Third_Player_Index, 
																						fDist[0], 							//FLOAT First_Player_Score, 
																						fDist[1], 							//FLOAT Second_Player_Score, 
																						fDist[2], 							//FLOAT Third_Player_Score, 
																						0.0, 								//FLOAT iLocal_Score_Display, 
																						0.0, 								//FLOAT iLocal_Score_Display, 
																						-10, 								//INT Event_Timer_Display,
																						NumberString,						//STRING UnitString,
																						bForceRebuildNames,					//BOOL& bForceRebuildNames,
																						HUD_COLOUR_WHITE, 					//HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																						FALSE,								//BOOL bDisplayAttempt = TRUE, 
																						DEFAULT,							//INT FlashBackgroundtimerMS = 0,
																						"",									//STRING ModeName = NULL, 
																						FMEVENT_SCORETITLE_NONE)			//FMEVENT_SCORETITLE LocalDisplayType = FMEVENT_SCORETITLE_GAMERTAG)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



