USING "globals.sch"
USING "net_include.sch"
USING "net_script_timers.sch"
USING "mp_scaleform_functions.sch"
USING "net_common_functions.sch"
USING "fmmc_restart_header.sch"
USING "net_missions_at_coords_public.sch"
USING "MP_globals.sch"
USING "net_corona_V2.sch"
USING "net_script_tunables_new.sch"
USING "Maintain_Asassination_Mission_Locates.sch"
USING "net_arena_wars_career.sch"

//For adding adversary modes update functionality where you see this:
//ADD NEW ADVERSARY MODES STUFF HERE!
//CHECK_IF_VARIATION_PASSES_NEW_VS_CHECK
//GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION
//GET_NEXT_VS_ROOT_CONTENT_ID

//USING "gunclub_shop_private.sch"

CONST_INT JOB_PANELS				9
CONST_INT NUM_JOBS_ON_ONE_PAGE		6
CONST_INT MIN_VOTES					0
CONST_INT MAX_VOTES					1
CONST_INT MAX_TOTAL_VOTES			(NUM_NETWORK_PLAYERS -2)
//CONST_INT VOTING_TIMEOUT 			60000 - replaced with - g_sMPTunables.iNjvsTimeOut
//CONST_INT SAFE_MAJORITY_TIMEOUT	10000 - replaced with - g_sMPTunables.iNjvsMajorityTimeOut
CONST_INT SHOW_RESULTS_TIME			4000

CONST_INT VEH_DM_KILL_TIME_LIMIT	60000
CONST_INT DM_KILL_TIME_LIMIT		30000

CONST_INT ciMAX_RESTART_LOOP 		60

CONST_INT JOB_PANEL_ZERO 			0
CONST_INT JOB_PANEL_ONE				1
CONST_INT JOB_PANEL_TWO 			2
CONST_INT JOB_PANEL_THREE 			3
CONST_INT JOB_PANEL_FOUR			4
CONST_INT JOB_PANEL_FIVE			5 

CONST_INT JOB_PANEL_SIX				6 
CONST_INT JOB_PANEL_SEVEN			7 
CONST_INT JOB_PANEL_EIGHT			8 
CONST_INT JOB_PANEL_NINE			9 
CONST_INT JOB_PANEL_TEN				10
CONST_INT JOB_PANEL_ELEVEN			11

CONST_INT JOB_PANEL_TWELVE 			12	
CONST_INT JOB_PANEL_THIRTEEN		13	
CONST_INT JOB_PANEL_FOURTEEN		14	
CONST_INT JOB_PANEL_FIFTEEN			15
CONST_INT JOB_PANEL_SIXTEEN			16
CONST_INT JOB_PANEL_SEVENTEEN		17	

CONST_INT JOB_PANEL_REPLAY			18
CONST_INT JOB_PANEL_REFRESH			19
CONST_INT JOB_PANEL_EXIT			20
CONST_INT JOB_PANEL_RANDOM			21
CONST_INT JOB_PANEL_NO_VOTE			22
CONST_INT JOB_PANEL_INVALID			23

CONST_INT PRESSED_UP				0
CONST_INT PRESSED_DOWN				1
CONST_INT PRESSED_LEFT				2
CONST_INT PRESSED_RIGHT				3

CONST_INT JOB_GET_ASSETS			0
CONST_INT JOB_VOTE_REQUEST_ASSETS 	1
CONST_INT JOB_VOTE_DO_VOTING		2
CONST_INT JOB_VOTE_REFRESHING		3
CONST_INT JOB_VOTE_END				4

CONST_INT VALID_VOTE				1
CONST_INT INVALID_VOTE				0

CONST_INT ciCHECK_PLAYERS_JOBS_INT 		0
CONST_INT ciPLAYERS_SET_ON_END_OF_JOB	1
	
// iImageBoolsBitSet	
CONST_INT ciIMAGE_GOT_IMAGES 				0
CONST_INT ciIMAGE_GOT_SECOND_SET_IMAGES 	1
CONST_INT ciIMAGE_RELEASED_FIRST_TEXTURES	2

CONST_INT CLIENT_BOOL_UPDATE			0
CONST_INT CLIENT_BOOL_PLAYER_LIST_ON	1
CONST_INT CLIENT_BOOL_GOT_DESCRIPTIONS  2
CONST_INT CLIENT_BOOL_QUIT_SCREEN_ON	3
CONST_INT CLIENT_BOOL_QUIT_SOUND_ONCE	4
CONST_INT CLIENT_BOOL_CLOUD_BAIL		5
CONST_INT CLIENT_BOOL_READY_TO_GO		6
CONST_INT CLIENT_BOOL_SET_BLOCKING		7
CONST_INT CLIENT_BOOL_QUIT_NJVS			8
CONST_INT CLIENT_BOOL_CALL_SET_UPDATE	9
//CONST_INT CLIENT_BOOL_BLOCK_ARMOUR		10
//CONST_INT CLIENT_BOOL_BLOCK_AMMO		11
//CONST_INT CLIENT_BOOL_COUNT_AMMO_COST	12
//CONST_INT CLIENT_BOOL_AMMO_WARNING_ON	13
CONST_INT CLIENT_BOOL_INIT_SCROLL		14
CONST_INT CLIENT_BOOL_LOBBY_LIST		15
CONST_INT CLIENT_BOOL_LOBBY_INIT		16
CONST_INT CLIENT_BOOL_LOBBY_INIT_DONE	17
CONST_INT CLIENT_BOOL_DISPLAY_VIEW		18
	
CONST_INT REFRESH_STAGE_TIME			2000
										
STRUCT NEXT_JOB_STRUCT

	MP_MISSION_ID_DATA missionIdData
	
	GET_UGC_CONTENT_STRUCT sGetUGC_content
	// Moved to globals
//	CACHED_MISSION_DESCRIPTION_LOAD_VARS sCMDLvar[NUM_JOBS_TO_GRAB]
//	STRUCT_IMAGES_FOR_NEXT_JOB_SCREEN_VARS sNextJobImages

	SCALEFORM_INDEX scaleformJobsMovie
	
	INT iArrayPos[NUM_JOBS_TO_GRAB]
	INT iJobPanelSelected
	INT iOffset = 0
	INT iRefreshValueLocal
	INT iClientJobBitSet
	INT iFoundArrayPosCounter
	INT iReleaseCount
	INT iNumClientRefreshes
	INT iNotFoundBitSet
	INT iNotFoundCount
	INT iPlayerActiveBitSet
	//INT iImageToDisplay = 1
	INT iTotalCostToFillAmmo
	INT	iWeaponSlot	
	BOOL bSecondProcess
	
	SCRIPT_TIMER timerShowResult
	SCRIPT_TIMER timerFlash
	SCRIPT_TIMER sTimerDpad
	SCRIPT_TIMER sImageTimer
	
	SCRIPT_TIMER sDLImagesTimeout1
	SCRIPT_TIMER sDLImagesTimeout2
	
	TIME_DATATYPE timerScrollDelay
	
	// Help buttons
	INT iJobVoteButtonProgress
	INT iUpdateTimer
	SCALEFORM_INDEX scaleformHelp
	SCALEFORM_INSTRUCTIONAL_BUTTONS scaleformStruct
	
	// Scroll
	INT iRowSelected = 0
	INT iPlayerListCount 
	
	// Mouse
	INT iCurrentMouseHoverHighlightId
ENDSTRUCT

CONST_INT SERVER_BOOL_EVERYONE_READY 			0
CONST_INT SERVER_BOOL_GRABBED_JOBS				1
CONST_INT SERVER_BOOL_SECOND_PAGE				2
CONST_INT SERVER_BOOL_ALL_CLIENTS_REF			3
CONST_INT SERVER_BOOL_GET_SAFE_JOBS				4
CONST_INT SERVER_BOOL_LAST_PAGE					5
CONST_INT SERVER_BOOL_ALL_CLIENT_VOTING 		6
CONST_INT SERVER_BOOL_FIRST_JOB_IS_RANDOM 		7
CONST_INT SERVER_BOOL_IS_CORONA_FLOW_V2 		8
CONST_INT SERVER_BOOL_IS_ADVERSARY_SERIES_V2 	9
CONST_INT SERVER_BOOL_IS_BUNKER_SERIES_V2 		10
CONST_INT SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT	11
CONST_INT SERVER_BOOL_IS_TARGET_SERIES			12
CONST_INT SERVER_BOOL_IS_HOTRING_SERIES			13
CONST_INT SERVER_BOOL_IS_ASSASSINATION_MISSION 	14
CONST_INT SERVER_BOOL_IS_ARENA_SERIES			15
CONST_INT SERVER_BOOL_IS_RACE_SERIES			16
CONST_INT SERVER_BOOL_IS_SURVIVAL_SERIES		17
CONST_INT SERVER_BOOL_IS_KOTH					18
CONST_INT SERVER_BOOL_IS_OPEN_WHEEL_SERIES		19
CONST_INT SERVER_BOOL_IS_STREET_RACE_SERIES		20
CONST_INT SERVER_BOOL_IS_PURSUIT_SERIES			21
#IF FEATURE_GEN9_EXCLUSIVE
CONST_INT SERVER_BOOL_IS_HSW_RACE_SERIES		22
#ENDIF
CONST_INT SERVER_BOOL_IS_COMMUNITY_SERIES		23
CONST_INT SERVER_BOOL_IS_CAYO_PERICO_SERIES		24

PROC RESET_SERVER_NEXT_JOB_VARS()
	PRINTLN(" [CS_JOB] [2001613] RESET_SERVER_NEXT_JOB_VARS - g_sMC_serverBDEndJob.iNumberCompatable before = ", g_sMC_serverBDEndJob.iNumberCompatable)
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	INT iLoop
	SCRIPT_TIMER timerVotingEnds
	VECTOR vRestartPos 
	TEXT_LABEL_23 tlFileName
	TOTAL_COUNT_VARS sCounts
	PLAYER_INDEX playerCurrentJobHost
	
	
	FOR iLoop = 0 TO (JOB_PANEL_TOTAL - 1)
		g_sMC_serverBDEndJob.iJobVotes[iLoop]	= 0	
	ENDFOR
	FOR iLoop = 0 TO (NUM_JOBS_TO_GRAB - 1)
		//g_sMC_serverBDEndJob.iArrayPos[iLoop]	= 	0
		g_sMC_serverBDEndJob.iStoredRandom[iLoop]	= 	0
		g_sMC_serverBDEndJob.tlFileName[iLoop]	= tlFileName	
	ENDFOR
	FOR iLoop = 0 TO (ciEOJV_TYPE_MAX - 1)
		g_sMC_serverBDEndJob.sCounts[iLoop]	= sCounts	
	ENDFOR
	FOR iLoop = 0 TO (MAX_CLOUD_MISSION_TYPES - 1)
		g_sMC_serverBDEndJob.sTypeCounts.iType[iLoop] = 0
	ENDFOR
	FOR iLoop = 0 TO (ciNUMBER_OF_VERIFIED_TYPES - 1)
		g_sMC_serverBDEndJob.sTypeCounts.iTypeVerifiedType[iLoop] = 0
	ENDFOR
	FOR iLoop = 0 TO (ciMAX_RECENT_LOCAL_CONTENT_ID_HISTORY - 1)
		g_sMC_serverBDEndJob.sTypeCounts.iAttempts[iLoop] = 0
	ENDFOR
	FOR iLoop = 0 TO (ciNUMBER_OF_LTS_TO_GRAB - 1)
		g_sMC_serverBDEndJob.sTypeCounts.iMissionThatIsLTS[iLoop] = 0
	ENDFOR
	FOR iLoop = 0 TO (ciNUMBER_OF_NEW_VS_TO_GRAB - 1)
		g_sMC_serverBDEndJob.sTypeCounts.iMissionThatIsNewVs[iLoop] = 0
		g_sMC_serverBDEndJob.sTypeCounts.iNewVsHash[iLoop] = 0
	ENDFOR
	
	g_sMC_serverBDEndJob.sTypeCounts.bUseRandom = TRUE
	g_sMC_serverBDEndJob.sTypeCounts.bUseRandomKing = FALSE
	g_sMC_serverBDEndJob.sTypeCounts.iLastMissionArrayPos = 0
	g_sMC_serverBDEndJob.sTypeCounts.iNumSameAsLastToFind = 0
	g_sMC_serverBDEndJob.sTypeCounts.iNumNewVsFound = 0
	g_sMC_serverBDEndJob.sTypeCounts.iNumLtsFound = 0
		
	g_sMC_serverBDEndJob.playerCurrentJobHost 	= playerCurrentJobHost
	g_sMC_serverBDEndJob.timerVotingEnds 		= timerVotingEnds
	g_sMC_serverBDEndJob.timerServerRefresh 	= timerVotingEnds
	
	g_sMC_serverBDEndJob.iJobVoteWinner 		= -1
	g_sMC_serverBDEndJob.iSecondryJobVoteWinner = -1
	g_sMC_serverBDEndJob.iTeamMissionsFound		= 0
	g_sMC_serverBDEndJob.iFoundJobOfTypeBitset	= 0
	g_sMC_serverBDEndJob.iAttempts				= 0
	g_sMC_serverBDEndJob.iNumRefreshed			= 0
	g_sMC_serverBDEndJob.iMatchTypeUnlockBitSet	= 0
	g_sMC_serverBDEndJob.iCheckPlayersHaveJobsBitSet = 0
	g_sMC_serverBDEndJob.iHostMigrateBitSet		= 0
	g_sMC_serverBDEndJob.iMajorityVoteTotal		= 0
	g_sMC_serverBDEndJob.iNumberOnJob			= 0
	g_sMC_serverBDEndJob.iRefreshValue			= 0
	g_sMC_serverBDEndJob.iHighestPlayerRank		= 0
	g_sMC_serverBDEndJob.iAverageRank			= 0
	g_sMC_serverBDEndJob.iRandomProgress		= 0
	g_sMC_serverBDEndJob.iVoteTotal				= 0
	g_sMC_serverBDEndJob.iFound					= 0
	g_sMC_serverBDEndJob.iServerLogicProgress	= 0
	g_sMC_serverBDEndJob.iServerJobBitSet		= 0
	g_sMC_serverBDEndJob.vRestartPos			= vRestartPos//<<0.0, 0.0, 0.0>>
	g_sMC_serverBDEndJob.iVerifiedBitSet 		= 0
	g_sMC_serverBDEndJob.iNumberCompatable 		= -1
	g_sMC_serverBDEndJob.iCustomVehicleNameBS 	= 0
	PRINTLN(" [CS_JOB] [2001613] RESET_SERVER_NEXT_JOB_VARS - g_sMC_serverBDEndJob.iNumberCompatable after = ", g_sMC_serverBDEndJob.iNumberCompatable)
	#IF IS_DEBUG_BUILD
	g_sMC_serverBDEndJob.bForceVoteForFreemode = FALSE
	g_SkipCelebAndLbd = FALSE
	#ENDIF
ENDPROC

FUNC BOOL SHOULD_SKIP_NJVS()

	//If we should get out because the NJVS is being skipped
	IF SHOULD_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
		//PRINTLN("[TS] DO_END_JOB_VOTING_SCREEN - SHOULD_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS - skipping")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_FM_FLOW_MISSION_SKIP_NJVS()
		//PRINTLN("[TS] DO_END_JOB_VOTING_SCREEN - SHOULD_FM_FLOW_MISSION_SKIP_NJVS - skipping")
		RETURN TRUE
	ENDIF
	
	IF CV2_WAS_STUNT_RACE_LAUNCHED_AS_PROFESSIONAL()
		RETURN TRUE
	ENDIF
		
	//If we should get out because it's a strand mission
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
	OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		//PRINTLN("[TS] DO_END_JOB_VOTING_SCREEN - IS_THIS_A_STRAND_MISSION - skipping")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())	
		RETURN TRUE
	ENDIF
	
	IF DOES_CURRENT_MISSION_USE_GANG_BOSS()
	OR DOES_CURRENT_MISSION_USE_GANG_BOSS_FLOW()
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_DOING_ARENA_TUTORIAL(PLAYER_ID())
	//OR (CONTENT_IS_USING_ARENA() AND IS_ORIGINAL_ARENA_OWNER_DOING_ARENA_TUTORIAL()) - don't think this is needed here, it would spam too much
		PRINTLN("[ARENA TUT] Skipping NJVS for spawn in the arena workshop - SHOULD_SKIP_NJVS")
		g_TransitionSessionNonResetVars.bFinishedFirstArenaEvent = TRUE			
		RETURN TRUE
	ENDIF
	
	IF VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
	OR IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
	#IF FEATURE_DLC_2_2022
	OR IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
	#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GOT_SECOND_BATCH_OF_IMAGES()
	RETURN IS_BIT_SET(g_sNextJobImages.iImageBoolsBitSet, ciIMAGE_GOT_SECOND_SET_IMAGES)
ENDFUNC

FUNC BOOL ON_SECOND_PAGE_REFRESHED(SERVER_NEXT_JOB_VARS& serverVars)
	RETURN IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_SECOND_PAGE)
ENDFUNC

FUNC BOOL ON_LAST_PAGE_REFRESHED(SERVER_NEXT_JOB_VARS& serverVars)
	RETURN IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_LAST_PAGE)
ENDFUNC

// 1633817 Don't let players refresh until the images are ready
FUNC BOOL CANT_REFRESH_YET(SERVER_NEXT_JOB_VARS& serverVars)
	IF ON_SECOND_PAGE_REFRESHED(serverVars)
	AND NOT GOT_SECOND_BATCH_OF_IMAGES()
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL DID_I_VOTE_FOR_THIS_JOB(INT iJob)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, iJob)
ENDFUNC

FUNC BOOL IS_PLAYER_LIST_ON(NEXT_JOB_STRUCT& nextJobStruct)
	RETURN IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_PLAYER_LIST_ON)
ENDFUNC

FUNC BOOL IS_QUIT_SCREEN_ON(NEXT_JOB_STRUCT& nextJobStruct)
	RETURN IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_QUIT_SCREEN_ON)
ENDFUNC

FUNC BOOL DID_QUIT_END_JOB_VOTING(NEXT_JOB_STRUCT& nextJobStruct)
	RETURN IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_QUIT_NJVS)
ENDFUNC

PROC SET_PLAYER_QUIT_JOB(NEXT_JOB_STRUCT& nextJobStruct)
	PRINTLN("FMMC_EOM -  [CS_JOB] SET_PLAYER_QUIT_JOB ")
	SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_QUIT_NJVS)
ENDPROC

FUNC BOOL HAVE_GRABBED_FINAL_IMAGES()
	RETURN IS_BIT_SET(g_sNextJobImages.iImageBoolsBitSet, ciIMAGE_GOT_SECOND_SET_IMAGES)
ENDFUNC

FUNC INT GET_JOB_VOTE_STAGE()
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	RETURN GlobalplayerBD_FM[iPlayer].iClientJobLogicStage
ENDFUNC

//FUNC BOOL HAS_END_JOB_BUY_AMMO_BEEN_PRESSED()
//	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)		
//	
//		PRINTLN(" ------------------------------------------------------------------------------")
//		PRINTLN(" --                                                                          --")
//		PRINTLN(" - FMMC_EOM - HAS_END_JOB_BUY_AMMO_BEEN_PRESSED                     		  --")
//		PRINTLN(" --                                                                          --")
//		PRINTLN(" ------------------------------------------------------------------------------")
//	
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC
//
//FUNC BOOL HAS_END_JOB_BUY_ARMOUR_BEEN_PRESSED()
//	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)		
//	
//		PRINTLN(" ------------------------------------------------------------------------------")
//		PRINTLN(" --                                                                          --")
//		PRINTLN(" - FMMC_EOM - HAS_END_JOB_BUY_ARMOUR_BEEN_PRESSED                     		  --")
//		PRINTLN(" --                                                                          --")
//		PRINTLN(" ------------------------------------------------------------------------------")
//	
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC
//
//PROC TURN_ON_AMMO_WARNING(NEXT_JOB_STRUCT& nextJobStruct, BOOL bTurnOn)
//	IF bTurnOn
//		PRINTLN("[CS_JOB] [ammo/armour] TURN_ON_AMMO_WARNING, TRUE ")
//		SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_AMMO_WARNING_ON)
//	ELSE
//		PRINTLN("[CS_JOB] [ammo/armour] TURN_ON_AMMO_WARNING, FALSE ")
//		CLEAR_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_AMMO_WARNING_ON)
//	ENDIF
//ENDPROC
//
//FUNC BOOL IS_AMMO_WARNING_SCREEN_ACTIVE(NEXT_JOB_STRUCT& nextJobStruct)
//
//	RETURN IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_AMMO_WARNING_ON)
//ENDFUNC
//
//FUNC BOOL WARN_ABOUT_AMMO_COST(NEXT_JOB_STRUCT& nextJobStruct)
//
//	IF nextJobStruct.iTotalCostToFillAmmo >= CORONA_PURCHASE_AMMO_WARNING_LIMIT
//	
//		RETURN TRUE
//	ENDIF
//
//	RETURN FALSE
//ENDFUNC
//
//FUNC BOOL AMMO_AVAILABLE(NEXT_JOB_STRUCT& nextJobStruct)
//
//	IF IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_BLOCK_AMMO)
//	
//		RETURN FALSE
//	ENDIF
//
//	RETURN TRUE
//ENDFUNC
//
//FUNC BOOL ARMOUR_AVAILABLE(NEXT_JOB_STRUCT& nextJobStruct)
//
//	IF IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_BLOCK_ARMOUR)
//	
//		RETURN FALSE
//	ENDIF
//
//	RETURN TRUE
//ENDFUNC

PROC REFRESH_BUTTONS(NEXT_JOB_STRUCT& nextJobStruct)
	nextJobStruct.iJobVoteButtonProgress = 0
ENDPROC

PROC DO_JOB_VOTE_SCREEN_BUTTONS(NEXT_JOB_STRUCT& nextJobStruct, TIME_DATATYPE iTimer)

	INT ms = g_sMPTunables.iNjvsTimeOut - ABSI(GET_TIME_DIFFERENCE(iTimer, GET_NETWORK_TIME()))
	INT sec = ROUND(ms * 0.001)

	STRING stTimer = "END_LBD_CONT"
	SPRITE_PLACEMENT aSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	IF GET_JOB_VOTE_STAGE() > JOB_VOTE_DO_VOTING
	OR NETWORK_TEXT_CHAT_IS_TYPING() 
	
		nextJobStruct.iJobVoteButtonProgress = 0
		EXIT
	ENDIF
	
	PRINTLN("FMMC EOM - DO_JOB_VOTE_SCREEN_BUTTONS, iJobVoteButtonProgress =  ", nextJobStruct.iJobVoteButtonProgress, " ms = ", ms, " sec = ", sec, " iNjvsTimeOut = ", g_sMPTunables.iNjvsTimeOut)

	STRING sProfile = GET_PROFILE_BUTTON_NAME()
	
	SWITCH nextJobStruct.iJobVoteButtonProgress
	
		CASE 0		
			IF sec > 0
				PRINTLN("FMMC EOM - DO_JOB_VOTE_SCREEN_BUTTONS, nextJobStruct.iJobVoteButtonProgress = 1 ")		
				PRINTLN("FMMC EOM - DO_JOB_VOTE_SCREEN_BUTTONS - BUSYSPINNER_OFF()")		
				BUSYSPINNER_OFF()
				nextJobStruct.iJobVoteButtonProgress = 1
			ELSE
				PRINTLN("[1943902] FMMC EOM - DO_JOB_VOTE_SCREEN_BUTTONS (sec < 0) ")
			ENDIF
		BREAK
		
		CASE 1
			IF LOAD_MENU_ASSETS()
				REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(nextJobStruct.scaleformStruct)
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT_AS_TIME("", stTimer, ms, nextJobStruct.scaleformStruct) 	// time
				IF IS_PLAYER_LIST_ON(nextJobStruct)
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT, sProfile, nextJobStruct.scaleformStruct, TRUE)				// Profile select
				ELSE
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "FMMC_END_VOT", nextJobStruct.scaleformStruct, TRUE)		// VOTE CROSS
					ENDIF
				ENDIF
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HUD_QUIT", nextJobStruct.scaleformStruct, TRUE)					// Quit CIRCLE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, "FMMC_END_LST", nextJobStruct.scaleformStruct, TRUE)				// Player list TRIANGLE
				
//				IF AMMO_AVAILABLE(nextJobStruct)
//					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LT), "FMMC_END_AMMO", nextJobStruct.scaleformStruct)			// Ammo
//				ENDIF
//				IF ARMOUR_AVAILABLE(nextJobStruct)
//					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RT), "FMMC_END_ARMR", nextJobStruct.scaleformStruct)			// Armour
//				ENDIF

				nextJobStruct.iUpdateTimer = sec
				PRINTLN("FMMC EOM - DO_JOB_VOTE_SCREEN_BUTTONS, nextJobStruct.iJobVoteButtonProgress = 2 ")					
				nextJobStruct.iJobVoteButtonProgress = 2
			ENDIF
		BREAK
		
		CASE 2
			IF HAS_END_VOTE_PLAYER_LIST_BUTTON_BEEN_PRESSED() 
				PRINTLN("FMMC EOM - DO_JOB_VOTE_SCREEN_BUTTONS, HAS_END_VOTE_PLAYER_LIST_BUTTON_BEEN_PRESSED, nextJobStruct.iJobVoteButtonProgress = 0 ")	
				nextJobStruct.iJobVoteButtonProgress = 0
			ENDIF
			
			IF LOAD_MENU_ASSETS()
				IF sec >= 0
					IF nextJobStruct.iUpdateTimer <> sec
						UPDATE_SCALEFORM_INSTRUCTION_BUTTON_SPINNER(nextJobStruct.scaleformHelp, ms, nextJobStruct.scaleformStruct)
						nextJobStruct.iUpdateTimer = sec
					ELSE
						PRINTLN("FMMC EOM - DO_JOB_VOTE_SCREEN_BUTTONS, nextJobStruct.iUpdateTimer <> sec ")	
					ENDIF
				ELSE
					PRINTLN("FMMC EOM - DO_JOB_VOTE_SCREEN_BUTTONS, nextJobStruct.iJobVoteButtonProgress = 3 ")	
					nextJobStruct.iJobVoteButtonProgress = 3
				ENDIF
				
				PRINTLN("FMMC EOM - DO_JOB_VOTE_SCREEN_BUTTONS, RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS, CASE 2 ")	
				
				RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(nextJobStruct.scaleformHelp, aSprite, nextJobStruct.scaleformStruct, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(nextJobStruct.scaleformStruct)) 
			ENDIF
		BREAK
		
		CASE 3
			IF LOAD_MENU_ASSETS()
				REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(nextJobStruct.scaleformStruct)
				IF IS_PLAYER_LIST_ON(nextJobStruct)
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT, sProfile, nextJobStruct.scaleformStruct)				// Profile select
				ELSE
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "FMMC_END_VOT", nextJobStruct.scaleformStruct)		// VOTE CROSS
					ENDIF
				ENDIF
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HUD_QUIT", nextJobStruct.scaleformStruct)					// Quit CIRCLE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, "FMMC_END_LST", nextJobStruct.scaleformStruct)				// Player list TRIANGLE
				PRINTLN("FMMC EOM - DO_JOB_VOTE_SCREEN_BUTTONS, nextJobStruct.iJobVoteButtonProgress = 4 ")					
				nextJobStruct.iJobVoteButtonProgress = 4
			ENDIF
		BREAK
		
		CASE 4
			IF LOAD_MENU_ASSETS()
				RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(nextJobStruct.scaleformHelp, aSprite, nextJobStruct.scaleformStruct, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(nextJobStruct.scaleformStruct)) 
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT GET_NUMBER_OF_TIMES_REFRESHED(SERVER_NEXT_JOB_VARS& serverVars)
	IF ON_SECOND_PAGE_REFRESHED(serverVars)
		RETURN 1
	ELIF ON_LAST_PAGE_REFRESHED(serverVars)
		RETURN 2
	ENDIF
	RETURN 0
ENDFUNC

FUNC BOOL HAS_SERVER_CHECKED_CLIENTS_READY_FOR_REFRESH(SERVER_NEXT_JOB_VARS& serverVars)
	RETURN IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENTS_REF)
ENDFUNC

FUNC INT GET_ARRAY_SHIFT(SERVER_NEXT_JOB_VARS& serverVars)
	IF ON_LAST_PAGE_REFRESHED(serverVars)

		RETURN (NUM_JOBS_ON_ONE_PAGE * 2)
	ELIF ON_SECOND_PAGE_REFRESHED(serverVars)
	
		RETURN NUM_JOBS_ON_ONE_PAGE
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL HAS_VOTING_TIMED_OUT(SERVER_NEXT_JOB_VARS& serverVars)

	#IF IS_DEBUG_BUILD
	IF g_bNeverTimeOut = TRUE
	
		RETURN FALSE
	ENDIF
	#ENDIF

	IF HAS_NET_TIMER_STARTED(serverVars.timerVotingEnds)
		IF HAS_NET_TIMER_EXPIRED(serverVars.timerVotingEnds, g_sMPTunables.iNjvsTimeOut)
		
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ENOUGH_TIME_PASSED_FOR_MAJORITY(SERVER_NEXT_JOB_VARS& serverVars)
	IF HAS_NET_TIMER_STARTED(serverVars.timerVotingEnds)
		IF HAS_NET_TIMER_EXPIRED(serverVars.timerVotingEnds, g_sMPTunables.iNjvsMajorityTimeOut)
		
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT GET_JOB_MOST_VOTES(SERVER_NEXT_JOB_VARS& serverVars, BOOL bSecondMost = FALSE)

	INT iJob
	INT iJobMostVotes 
	INT iSecondJobMostVotes 
	
	REPEAT JOB_PANEL_TOTAL iJob
		IF serverVars.iJobVotes[iJob] >= serverVars.iJobVotes[iJobMostVotes]
			iJobMostVotes = iJob
		ENDIF
	ENDREPEAT
	
	IF bSecondMost
		REPEAT JOB_PANEL_TOTAL iJob
			IF serverVars.iJobVotes[iJob] >= serverVars.iJobVotes[iSecondJobMostVotes]
			AND iJob != iJobMostVotes
				iSecondJobMostVotes = iJob
			ENDIF
		ENDREPEAT
		RETURN iSecondJobMostVotes 		
	ENDIF
	
	RETURN iJobMostVotes
ENDFUNC

FUNC BOOL DOES_SECONDRY_JOB_HAVE_ANY_VOTES(SERVER_NEXT_JOB_VARS& serverVars)
	INT iJob
	REPEAT JOB_PANEL_TOTAL iJob
		IF serverVars.iJobVotes[iJob] > 0
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

CONST_FLOAT VOTE_MAJORITY 0.5

FUNC BOOL VOTE_MAJORITY_PASSED(SERVER_NEXT_JOB_VARS& serverVars)

	FLOAT fNumOnJob = TO_FLOAT(serverVars.iNumberOnJob)
	BOOL bSafeTimeElapsed = HAS_ENOUGH_TIME_PASSED_FOR_MAJORITY(serverVars)
	INT iJobMostVotes 		= GET_JOB_MOST_VOTES(serverVars)
	FLOAT fNumTotalValidVotes 	= TO_FLOAT(serverVars.iMajorityVoteTotal)
	
//	// 1816729 /////////////////////////
//	#IF IS_DEBUG_BUILD
//	IF bSafeTimeElapsed
//		PRINTLN("[CS_JOB] VOTE_MAJORITY_PASSED, bSafeTimeElapsed YES ")
//	ELSE
//		PRINTLN("[CS_JOB] VOTE_MAJORITY_PASSED, bSafeTimeElapsed NO ")
//	ENDIF
//	PRINTLN("[CS_JOB] VOTE_MAJORITY_PASSED, iJobMostVotes = ", iJobMostVotes)
//	PRINTLN("[CS_JOB] VOTE_MAJORITY_PASSED, fNumOnJob = ", fNumOnJob)
//	PRINTLN("[CS_JOB] VOTE_MAJORITY_PASSED, (fNumTotalValidVotes / fNumOnJob) = ", (fNumTotalValidVotes / fNumOnJob))
//	#ENDIF
//	////////////////////////////////////

	// Vote majority if enough time has passed
	IF serverVars.iNumberOnJob > 2
	AND bSafeTimeElapsed
	AND iJobMostVotes <> JOB_PANEL_EXIT // 1765604 Don't pass on majority if the winner is return to Freemode
		IF (fNumTotalValidVotes / fNumOnJob) >= VOTE_MAJORITY
			
			RETURN TRUE			
		ENDIF
	ELSE
		// Wait for everyone to vote
		IF serverVars.iNumberOnJob = serverVars.iVoteTotal
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_VOTE_PASSED(SERVER_NEXT_JOB_VARS& serverVars)
	
	IF VOTE_MAJORITY_PASSED(serverVars)
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_VOTING_ENDED(SERVER_NEXT_JOB_VARS& serverVars)
	IF HAS_VOTING_TIMED_OUT(serverVars)
	
		PRINTLN(" [CS_JOB] HAS_VOTING_ENDED, HAS_VOTING_TIMED_OUT  ")

		RETURN TRUE
	ENDIF
	
	IF HAS_VOTE_PASSED(serverVars)
	
		PRINTLN(" [CS_JOB] HAS_VOTING_ENDED, HAS_VOTE_PASSED  iNumberOnJob = ", serverVars.iNumberOnJob, "   iVoteTotal = ", serverVars.iVoteTotal)

		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


//╔═════════════════════════════════════════════════════════════════════════════╗
//║		SERVER																	║
//╚═════════════════════════════════════════════════════════════════════════════╝	

PROC SERVER_STARTS_VOTE_TIMER(SERVER_NEXT_JOB_VARS& serverVars, BOOL bReset)
	IF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENT_VOTING)
	OR bReset
		IF NOT HAS_NET_TIMER_STARTED(serverVars.timerVotingEnds)
			PRINTLN(" [CS_JOB] SERVER_STARTS_VOTE_TIMER  ")
			serverVars.iRefreshValue++
			START_NET_TIMER(serverVars.timerVotingEnds)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_SERVER_GRABBED_JOBS(SERVER_NEXT_JOB_VARS& serverVars)
	RETURN IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_GRABBED_JOBS)
ENDFUNC

CONST_INT RANDOM_STAGE_INIT 	0
CONST_INT RANDOM_STAGE_COUNT	1
CONST_INT RANDOM_STAGE_GRAB 	2
CONST_INT RANDOM_STAGE_END		3

//get the vars for the mission based on type
FUNC TEXT_LABEL_23 GET_EOJV_MISSION_CONTENT_ID(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iJobToCheck].tlName
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].tlName
	ENDIF
ENDFUNC
FUNC TEXT_LABEL_63 GET_EOJV_MISSION_NAME(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iJobToCheck].tlMissionName
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].tlMissionName
	ENDIF
ENDFUNC
FUNC INT GET_EOJV_MISSION_BIT_SET(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iJobToCheck].iBitSet
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].iBitSet
	ENDIF
ENDFUNC
FUNC INT GET_EOJV_MISSION_BIT_SET_TWO(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN 0
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].iBitSetTwo
	ENDIF
ENDFUNC

FUNC INT GET_EOJV_MISSION_ROOT_ID_HASH(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN 0
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iJobToCheck].iRootContentIdHash
	ENDIF
ENDFUNC

FUNC INT GET_EOJV_MISSION_iAdversaryModeType(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN 0
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].iAdversaryModeType
	ENDIF
ENDFUNC


FUNC INT GET_EOJV_MISSION_MAX_PLAYERS(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iJobToCheck].iMaxPlayers
	ELSE
		IF g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iJobToCheck].iCoronaFlowPlaylist != -1
		AND g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMax[g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iJobToCheck].iCoronaFlowPlaylist] != -1
			RETURN g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMax[g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iJobToCheck].iCoronaFlowPlaylist]
		ENDIF
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].iMaxPlayers
	ENDIF
ENDFUNC
FUNC INT GET_EOJV_MISSION_MIN_PLAYERS(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iJobToCheck].iMinPlayers
	ELSE
		IF g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iJobToCheck].iCoronaFlowPlaylist != -1
		AND g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMin[g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iJobToCheck].iCoronaFlowPlaylist] != -1
			RETURN g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMin[g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iJobToCheck].iCoronaFlowPlaylist]
		ENDIF
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].iMinPlayers
	ENDIF
ENDFUNC
FUNC INT GET_EOJV_MISSION_MAX_NUMBER_OF_TEAMS(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iJobToCheck].iMaxNumberOfTeams
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].iMaxNumberOfTeams
	ENDIF
ENDFUNC
FUNC INT GET_EOJV_MISSION_RANK(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iJobToCheck].iRank
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].iRank
	ENDIF
ENDFUNC
FUNC INT GET_EOJV_MISSION_TYPE(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iJobToCheck].iType
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].iType
	ENDIF
ENDFUNC

FUNC INT GET_EOJV_MISSION_SUB_TYPE_FOR_COMPAT(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN -1
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].iSubType
	ENDIF
ENDFUNC

FUNC VECTOR GET_EOJV_MISSION_START_LOCATION(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iJobToCheck].vStartPos
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].vStartPos
	ENDIF
ENDFUNC
FUNC INT GET_EOJV_MISSION_DESCRIPTION_HASH(INT iJobToCheck, BOOL bIsRockstarVerified = FALSE)
	IF bIsRockstarVerified
	AND iJobToCheck < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iJobToCheck].iMissionDecHash
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iJobToCheck].iMissionDecHash
	ENDIF
ENDFUNC




//Get the details for displaying using Chris's functions
FUNC INT GET_JOB_HASH(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iMissionDecHash
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iMissionDecHash		
	ENDIF
ENDFUNC
FUNC INT GET_JOB_PHOTO_VERSION(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iPhotoVersion
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iPhotoVersion		
	ENDIF
ENDFUNC
FUNC INT GET_JOB_PHOTO_PATH(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iPhotoPath
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iPhotoPath		
	ENDIF
ENDFUNC
FUNC TEXT_LABEL_23 GET_JOB_CREATOR(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.tlOwnerPretty[nextJobStruct.iArrayPos[iJob]]
	ELSE
		TEXT_LABEL_23 tlReturn = GET_ROCKSTAR_CREATED_USER_STRING()
		RETURN tlReturn
	ENDIF
ENDFUNC
FUNC INT GET_JOB_TYPE(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iType
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iType
	ENDIF
ENDFUNC
FUNC TEXT_LABEL_23 GET_JOB_CONTENT_ID(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].tlName
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].tlName
	ENDIF
ENDFUNC
FUNC INT GET_JOB_SUB_TYPE(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iSubType
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iSubType
	ENDIF
ENDFUNC
FUNC INT GET_JOB_RATING(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iRating
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iRating
	ENDIF
ENDFUNC
FUNC BOOL GET_JOB_PLAYED_STATUS(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED)
	ELSE
		RETURN IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED)
	ENDIF
ENDFUNC
FUNC BOOL IS_THIS_A_PUSHBIKE_ONLY_RACE(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iBitSet, ciROCKSTAR_CREATED_MISSION_IS_PUSH_BIKE_ONLY)
	ELSE
		RETURN IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iBitSet, ciROCKSTAR_CREATED_MISSION_IS_PUSH_BIKE_ONLY)
	ENDIF
ENDFUNC
FUNC INT GET_JOB_MIN_PLAYERS(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iMinPlayers
	ELSE
		IF g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[nextJobStruct.iArrayPos[iJob]].iCoronaFlowPlaylist != -1
		AND g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMin[g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[nextJobStruct.iArrayPos[iJob]].iCoronaFlowPlaylist] != -1
			RETURN g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMin[g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[nextJobStruct.iArrayPos[iJob]].iCoronaFlowPlaylist]
		ENDIF
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iMinPlayers
	ENDIF
ENDFUNC
FUNC INT GET_JOB_MAX_PLAYERS(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iMaxPlayers
	ELSE
		IF g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[nextJobStruct.iArrayPos[iJob]].iCoronaFlowPlaylist != -1
		AND g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMax[g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[nextJobStruct.iArrayPos[iJob]].iCoronaFlowPlaylist] != -1
			RETURN g_sMPTunables.iFmCoronaPlaylistProffesionalNumberMax[g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[nextJobStruct.iArrayPos[iJob]].iCoronaFlowPlaylist]
		ENDIF
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iMaxPlayers
	ENDIF
ENDFUNC
FUNC VECTOR GET_JOB_START_POS(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].vStartPos
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].vStartPos
	ENDIF
ENDFUNC
FUNC INT GET_JOB_START_ROOT_ID(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN 0
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[nextJobStruct.iArrayPos[iJob]].iRootContentIdHash
	ENDIF
ENDFUNC

FUNC INT GET_JOB_START_ADVERSARY_MODE_TYPE(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN 0
	ELSE
		RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iAdversaryModeType
	ENDIF
ENDFUNC

// FALSE = R* verified
FUNC BOOL IS_JOB_ROCKSTAR_CREATED(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob)
	IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
	AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_EOJV_MISSION_SUB_TYPE(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iJob, BOOL bForBadge = FALSE)

	IF GET_JOB_TYPE(serverVars, nextJobStruct, iJob) = FMMC_TYPE_RACE	
	OR GET_JOB_TYPE(serverVars, nextJobStruct, iJob) = FMMC_TYPE_DEATHMATCH	
		IF IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
		AND nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
			RETURN g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iSubType
		ELSE
			RETURN g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iSubType
		ENDIF
	ENDIF
	
	IF GET_JOB_TYPE(serverVars, nextJobStruct, iJob) != FMMC_TYPE_MISSION
		RETURN 0
	ENDIF
	
	IF NOT IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)
		IF IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[nextJobStruct.iArrayPos[iJob]].iRootContentIdHash, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iAdversaryModeType)
			RETURN FMMC_TYPE_MISSION_NEW_VS
		ELIF g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[nextJobStruct.iArrayPos[iJob]].iSubType = FMMC_MISSION_TYPE_CONTACT
			RETURN FMMC_MISSION_TYPE_CONTACT
		ENDIF
	ENDIF
	
	IF bForBadge
		IF IS_BIT_SET(GET_EOJV_MISSION_BIT_SET(nextJobStruct.iArrayPos[iJob], IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)), ciMISSION_OPTION_BS_LTS) 
			RETURN FMMC_MISSION_TYPE_LTS
		ELIF IS_BIT_SET(GET_EOJV_MISSION_BIT_SET(nextJobStruct.iArrayPos[iJob], IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)), ciMISSION_OPTION_BS_CTF)
			RETURN FMMC_MISSION_TYPE_CTF
		ENDIF
	ELSE
		IF IS_BIT_SET(GET_EOJV_MISSION_BIT_SET(nextJobStruct.iArrayPos[iJob], IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)), ciMISSION_OPTION_BS_LTS) 
			RETURN FMMC_TYPE_MISSION_LTS
		ELIF IS_BIT_SET(GET_EOJV_MISSION_BIT_SET(nextJobStruct.iArrayPos[iJob], IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)), ciMISSION_OPTION_BS_CTF)
			RETURN FMMC_TYPE_MISSION_CTF
		ELIF IS_BIT_SET(GET_EOJV_MISSION_BIT_SET(nextJobStruct.iArrayPos[iJob], IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)), ciMISSION_OPTION_BS_COOP)
			RETURN FMMC_TYPE_MISSION_COOP
		ELIF IS_BIT_SET(GET_EOJV_MISSION_BIT_SET(nextJobStruct.iArrayPos[iJob], IS_BIT_SET(serverVars.iVerifiedBitSet, iJob)), ciMISSION_OPTION_BS_CONTACT_MISSION)
			RETURN FMMC_TYPE_MISSION_CONTACT
		ENDIF
	ENDIF
	RETURN 0
ENDFUNC

FUNC BOOL IS_SOCIAL_CLUB_CREATOR(NEXT_JOB_STRUCT& nextJobStruct, INT iJob, BOOL bIsReplay)
	IF NOT bIsReplay
		IF nextJobStruct.iArrayPos[iJob] < FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
			RETURN g_FMMC_ROCKSTAR_VERIFIED.bScNickName[nextJobStruct.iArrayPos[iJob]]
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT.bScNickName
ENDFUNC

FUNC BOOL IS_NEW_RANDOM_JOB(SERVER_NEXT_JOB_VARS& serverVars, INT iJobToCheck, BOOL bIsRockstarVerified = FALSE, BOOL bCoronaFlowPlay = FALSE)
	#IF IS_DEBUG_BUILD
	IF serverVars.iFound = 0
	AND GET_COMMANDLINE_PARAM_EXISTS("sc_NJVSskipRandom")
		RETURN TRUE
	ENDIF
	#ENDIF
	// Don't want the Job we just done
	IF NOT bCoronaFlowPlay
		TEXT_LABEL_63 tl63 = GET_EOJV_MISSION_CONTENT_ID(iJobToCheck, bIsRockstarVerified)
		IF ARE_STRINGS_EQUAL(tl63, g_FMMC_STRUCT.tl31LoadedContentID)
			RETURN FALSE
		ENDIF
	ENDIF

	INT i
	REPEAT NUM_JOBS_TO_GRAB i
		IF iJobToCheck = serverVars.iStoredRandom[i]
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	//PRINTLN(" [CS_JOB] HAVE_ALL_JOBS_BEEN_DISHED_OUT ")
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_ALL_JOBS_BEEN_DISHED_OUT(SERVER_NEXT_JOB_VARS& serverVars)
	INT i
	REPEAT NUM_JOBS_TO_GRAB i
		IF serverVars.iStoredRandom[i] = -1

			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC INITIALISE_SERVER_VARS(SERVER_NEXT_JOB_VARS& serverVars)
	INT i
	REPEAT NUM_JOBS_TO_GRAB i
		serverVars.iStoredRandom[i] = -1
	ENDREPEAT
	
	serverVars.vRestartPos = g_vFMMC_MissionStartPos
	PRINTLN("[CS_JOB]  - INITIALISE_SERVER_VARS - serverVars.vRestartPos = ", serverVars.vRestartPos)
ENDPROC

FUNC TEXT_LABEL_63 GET_JOB_NAME(NEXT_JOB_STRUCT& nextJobStruct, INT iJob, BOOL bIsRockstarVerified)
	TEXT_LABEL_63 tlJob
	
	tlJob = GET_EOJV_MISSION_NAME(nextJobStruct.iArrayPos[iJob], bIsRockstarVerified)
	
	RETURN tlJob
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DRAW_JOB_NAME_DEBUG(NEXT_JOB_STRUCT& nextJobStruct, INT iJob, BOOL bIsRockstarVerified)
	TEXT_LABEL_63 tlJob[NUM_JOBS_TO_GRAB] 
	tlJob[iJob] = GET_JOB_NAME(nextJobStruct, iJob, bIsRockstarVerified)			
	PRINTLN(" [CS_JOB] DRAW_JOB_NAME_DEBUG = ", tlJob[iJob])
ENDPROC
#ENDIF

FUNC BOOL RETURN_SAFE_JOB(SERVER_NEXT_JOB_VARS& serverVars)
	RETURN IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_GET_SAFE_JOBS)
ENDFUNC

FUNC INT GET_JOB_TYPE_TO_GRAB(SERVER_NEXT_JOB_VARS& serverVars, BOOL bRockStarCreated)

	INT iSet = NETWORK_GET_RANDOM_INT_RANGED(0, 1000)
	
	//Get only very save jobs
	IF NOT bRockStarCreated
		IF iSet < 500		
			RETURN FMMC_TYPE_RACE
		ELSE
			RETURN FMMC_TYPE_DEATHMATCH
		ENDIF
	ELIF RETURN_SAFE_JOB(serverVars)
		IF iSet < 333		
			RETURN FMMC_TYPE_MISSION			
		ELIF iSet < 666
			RETURN FMMC_TYPE_DEATHMATCH
		ELSE
			RETURN FMMC_TYPE_RACE
		ENDIF
	//Get a job that is unlocked
	ELSE
		INT iLoop
		//get a mission that is unlocked
		WHILE iLoop < 25
			IF iSet < 250
				IF IS_BIT_SET(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_MISSION)
					RETURN FMMC_TYPE_MISSION	
				ENDIF
			ELIF iSet < 500
				IF IS_BIT_SET(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_RACE)
					RETURN FMMC_TYPE_RACE			
				ENDIF
			ELIF iSet < 750
				IF IS_BIT_SET(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_DEATHMATCH)
					RETURN FMMC_TYPE_DEATHMATCH			
				ENDIF
			ELIF iSet < 850
				IF IS_BIT_SET(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_SURVIVAL)
					RETURN FMMC_TYPE_SURVIVAL
				ENDIF
			ELSE
				IF IS_BIT_SET(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_BASE_JUMP)
					RETURN FMMC_TYPE_BASE_JUMP
				ENDIF
			ENDIF
			iSet = NETWORK_GET_RANDOM_INT_RANGED(0, 1000)
			iLoop++	
		ENDWHILE
	ENDIF
	//Fall back random
	IF iSet < 333		
		RETURN FMMC_TYPE_MISSION			
	ELIF iSet < 666
		RETURN FMMC_TYPE_DEATHMATCH
	ELSE
		RETURN FMMC_TYPE_RACE
	ENDIF
	//last case	
	RETURN FMMC_TYPE_RACE
ENDFUNC

FUNC BOOL SHOULD_PICK_TEAM(SERVER_NEXT_JOB_VARS& serverVars)

	IF g_sMC_serverBDEndJob.iTeamMissionsFound < serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfTeamMissionsFound
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_THE_NUMBER_OF_PLAYERS_OK(INT iMinPlayers, INT iNumberOfPlayersOnScript, BOOL bAdVersaryFlow)
	IF iMinPlayers <= iNumberOfPlayersOnScript
	OR bAdVersaryFlow
		RETURN TRUE
	ENDIF
	
	//If this is not a solo session
	IF NOT IS_THIS_A_SOLO_SESSION()
		IF iNumberOfPlayersOnScript = 1
		 	RETURN TRUE
		ENDIF
	ENDIF
	 
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_IF_VARIATION_PASSES_LTS_CHECK(SERVER_NEXT_JOB_VARS& serverVars, BOOL bLTS,  INT iLoop, BOOL bIsRockstarVerified = FALSE)
	IF bLTS = FALSE
	OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ARENA_SERIES)
		RETURN TRUE
	ENDIF
	
	INT iBitSet = GET_EOJV_MISSION_BIT_SET(iLoop, bIsRockstarVerified)
	
	IF IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_LTS)
	OR IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS)
	OR IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_CTF)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL CHECK_IF_VARIATION_PASSES_NEW_VS_CHECK(SERVER_NEXT_JOB_VARS& serverVars, INT iNewVSBitSet,  INT iLoop, BOOL bIsRockstarVerified = FALSE, BOOL bMinPlayerOK = TRUE)
	IF (g_FMMC_STRUCT.iAdversaryModeType = 0 OR g_FMMC_STRUCT.iAdversaryModeType = ciNEWVS_NEW_SURVIVAL)
	AND iNewVSBitSet = 0
		RETURN TRUE
	ENDIF
	
	IF bIsRockstarVerified
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_BUNKER_SERIES_V2)
	AND CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_BUNKER_SERIES(GET_EOJV_MISSION_ROOT_ID_HASH(iLoop, bIsRockstarVerified))
		RETURN TRUE
	ENDIF
	
	
	IF(IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ARENA_SERIES))
	AND CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(GET_EOJV_MISSION_ROOT_ID_HASH(iLoop, bIsRockstarVerified))
		RETURN TRUE
	ENDIF
	
	
	INT iBitSet = GET_EOJV_MISSION_BIT_SET(iLoop, bIsRockstarVerified)
	INT iBitSetTwo = GET_EOJV_MISSION_BIT_SET_TWO(iLoop, bIsRockstarVerified)
	INT iAdversaryModeType = GET_EOJV_MISSION_iAdversaryModeType(iLoop, bIsRockstarVerified)
	
	//If we were on a SM then only take MS
	IF iAdversaryModeType != 0
	AND g_FMMC_STRUCT.iAdversaryModeType != 0
		//ADD NEW ADVERSARY MODES STUFF HERE!
		IF g_FMMC_STRUCT.iAdversaryModeType = iAdversaryModeType
			RETURN TRUE
		ENDIF
	ENDIF
	IF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_LOST_DAMNED)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(iAdversaryModeType)
			RETURN TRUE
		ENDIF		
		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_VEHICLE_VENDETTA)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(iAdversaryModeType)
			RETURN TRUE
		ENDIF	
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_POINTLESS)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POINTLESS(iAdversaryModeType)
			RETURN TRUE
		ENDIF	
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_TURF_WAR)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(iAdversaryModeType)
			RETURN TRUE
		ENDIF	
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_JUGGERNAUT)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(iAdversaryModeType)
			RETURN TRUE
		ENDIF		
		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EBC)
	OR IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUNNINGBACK)
		IF IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EBC)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUNNINGBACK)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY)
			RETURN TRUE
		ENDIF
		
	
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD)
	OR IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE)
	OR IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY)
		IF IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY)
			PRINTLN("[NEWVS] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - CHECK_IF_VARIATION_PASSES_NEW_VS_CHECK")
			RETURN TRUE
		ENDIF
	
	
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_HUNTDARK)
		IF IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_HUNTDARK)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY)
			RETURN TRUE
		ENDIF
		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BE_MY_VALENTIN)
		IF IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BE_MY_VALENTIN)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUGBY)
		IF IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUGBY)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_IN_AND_OUT)
		IF IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_IN_AND_OUT)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_ENTOURAGE)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ENTOURAGE(iAdversaryModeType)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_POWER_PLAY)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(iAdversaryModeType)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RATG)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RAGE_AGAINST_THE_CAMPERS(iAdversaryModeType)
			RETURN TRUE
		ENDIF		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAWN_RAID)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(iAdversaryModeType)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DIRTY_SANCHEZ)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DIRTY_SANCHEZ(iAdversaryModeType)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_GRASS_GREENER)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GRASS_GREENER(iAdversaryModeType)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_SUMO_ZONE)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_ZONE(iAdversaryModeType)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_GUNSMITH)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(iAdversaryModeType)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_TOUR_DE_FORCE)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE(iAdversaryModeType)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_DONT_CROSS_THE_LINE)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(iAdversaryModeType)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAVID_AND_GOLIATH)
		IF IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAVID_AND_GOLIATH)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BE_MY_VALENTIN)
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SUMO)
		IF IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SUMO)
		AND bMinPlayerOK
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DROP_ZONE)
	OR IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BVS1)
	OR IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EXTRACTION)
		IF IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DROP_ZONE)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BVS1)
		OR IS_BIT_SET(iBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EXTRACTION)
			RETURN TRUE
		ENDIF

		
	//If we were on a SM then only take MS
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HuntingPack)
	OR IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_CrossTheLine)
		IF IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HuntingPack)
		OR IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_CrossTheLine)
			RETURN TRUE
		ENDIF
	//If we were on a SM then only take MS
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
		IF IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
			RETURN TRUE
		ENDIF
	//If we're on a hasta la vista then pick hasta la vista and SM
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista)
		IF IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista)
		OR IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
			RETURN TRUE
		ENDIF
	//If we're on a Come Out to play then pick Come Out to play, hasta la vista and SM
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_ComeOutToPlay)
		IF IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_ComeOutToPlay)
		OR IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista)
		OR IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL CHECK_IF_VARIATION_PASSES_SAME_AS_LAST_CHECK(BOOL bSameAsLast,  INT iLoop, INT iMissionType, BOOL bAssassination)
	//Doesn't need to be the same as the last type
	IF bSameAsLast = FALSE
		RETURN TRUE
	ENDIF
	
	//If it's NOT a mission and type is the same then get out
	IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION
		IF g_FMMC_STRUCT.iMissionType = iMissionType						
			RETURN TRUE
		ENDIF

	//If it's a mission then get the sub type
	ELSE
		INT iBitSet = GET_EOJV_MISSION_BIT_SET(iLoop, FALSE)
		IF bAssassination
			IF IS_THIS_ROOT_CONTENT_ID_AN_ASSASSINATION_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
				RETURN TRUE
			ENDIF
		ELIF Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
		AND IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_CONTACT_MISSION)
			RETURN TRUE
		ENDIF		
		IF Is_Player_Currently_On_MP_Random_Event(PLAYER_ID())
		AND IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_RANDOM_EVENT)	
			RETURN TRUE
		ENDIF		
		IF Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())	
		AND IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS)		
			RETURN TRUE
		ENDIF		
		IF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())	
		AND IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_LTS)	
			RETURN TRUE
		ENDIF		
		IF Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
		AND IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_CTF)	
			RETURN TRUE
		ENDIF
		IF Is_Player_Currently_On_MP_Coop_Mission(PLAYER_ID())
		AND IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_COOP)	
			RETURN TRUE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC 

PROC REMOVE_MISSION_FROM_RECENT_HISTORY(INT iNextMission)
	INT iMoveLoop
	//Clear it
	g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iNextMission].tlName = ""
	//move the array down one
	FOR iMoveLoop = iNextMission TO (ciMAX_RECENT_LOCAL_CONTENT_ID_HISTORY - 2) 
		g_sRecentMissionHistory.g_tl23RecentMissionHistory[iMoveLoop] = g_sRecentMissionHistory.g_tl23RecentMissionHistory[iMoveLoop + 1]
		PRINTLN("[CONTENT_ID_HISTORY] g_sRecentMissionHistory.g_tl23RecentMissionHistory[", iMoveLoop, "] = ", g_sRecentMissionHistory.g_tl23RecentMissionHistory[iMoveLoop])
	ENDFOR
	//reduce the number in the loop
	g_sRecentMissionHistory.iStorageCount--
	PRINTLN("[CONTENT_ID_HISTORY] g_sRecentMissionHistory.iStorageCount = ", g_sRecentMissionHistory.iStorageCount)
ENDPROC

CONST_INT ciMAX_RECENT_ATTEMPTS 5
//Check to see if the mission 
//FUNC BOOL IS_MISSION_IN_RECENT_HISTORY(INT iNextMission, MISSION_TYPE_COUNT &sTypeCounts)
//	INT iLoop
//	//Loop all them missions
//	REPEAT g_sRecentMissionHistory.iStorageCount iLoop
//		//If it is in recent history
//		IF ARE_STRINGS_EQUAL(g_sRecentMissionHistory.g_tl23RecentMissionHistory[iLoop], g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iNextMission].tlName)
//			PRINTLN("[CONTENT_ID_HISTORY] IS_MISSION_IN_RECENT_HISTORY - g_sRecentMissionHistory.g_tl23RecentMissionHistory[", iLoop, "] = ", g_sRecentMissionHistory.g_tl23RecentMissionHistory[iLoop])
//			sTypeCounts.iAttempts[iLoop]++
//			IF sTypeCounts.iAttempts[iLoop] > ciMAX_RECENT_ATTEMPTS
//				 sTypeCounts.iAttempts[iLoop] = 0
//				 REMOVE_MISSION_FROM_RECENT_HISTORY(iNextMission)
//			ENDIF
//			//get out
//			RETURN TRUE
//		ENDIF
//	ENDREPEAT
//	//No
//	RETURN FALSE
//ENDFUNC

//Add a mission to the recent history
PROC ADD_MISSION_TO_RECENT_HISTORY()
	PRINTLN("[CONTENT_ID_HISTORY] ADD_MISSION_TO_RECENT_HISTORY")
	INT iLoop, iInverse
	BOOL bAdd = TRUE
	//Seee if we are to add it
	REPEAT g_sRecentMissionHistory.iStorageCount iLoop
		IF ARE_STRINGS_EQUAL(g_sRecentMissionHistory.g_tl23RecentMissionHistory[iLoop], g_FMMC_STRUCT.tl31LoadedContentID)	
			PRINTLN("[CONTENT_ID_HISTORY] ADD_MISSION_TO_RECENT_HISTORY - IF ARE_STRINGS_EQUAL(g_sRecentMissionHistory.g_tl23RecentMissionHistory[", iLoop, "], g_FMMC_STRUCT.tl31LoadedContentID)")
			bAdd = FALSE
		ENDIF
	ENDREPEAT
	//Yess lets add it
	IF bAdd
		//Move all the old missions down
		FOR iLoop = 0 TO (ciMAX_RECENT_LOCAL_CONTENT_ID_HISTORY - 2) 
			iInverse = ciMAX_RECENT_LOCAL_CONTENT_ID_HISTORY - 2 - iLoop
			g_sRecentMissionHistory.g_tl23RecentMissionHistory[iInverse+1] = g_sRecentMissionHistory.g_tl23RecentMissionHistory[iInverse]		
			PRINTLN("[CONTENT_ID_HISTORY] g_sRecentMissionHistory.g_tl23RecentMissionHistory[", iInverse, "] = ", g_sRecentMissionHistory.g_tl23RecentMissionHistory[iInverse])
		ENDFOR
		//Add it
		g_sRecentMissionHistory.g_tl23RecentMissionHistory[0] = g_FMMC_STRUCT.tl31LoadedContentID
		//Up the number that we've stored
		g_sRecentMissionHistory.iStorageCount++
		PRINTLN("[CONTENT_ID_HISTORY] g_sRecentMissionHistory.iStorageCount = ", g_sRecentMissionHistory.iStorageCount)
		IF g_sRecentMissionHistory.iStorageCount > ciMAX_RECENT_LOCAL_CONTENT_ID_HISTORY
			g_sRecentMissionHistory.iStorageCount = ciMAX_RECENT_LOCAL_CONTENT_ID_HISTORY
			PRINTLN("[CONTENT_ID_HISTORY] g_sRecentMissionHistory.iStorageCount = ciMAX_RECENT_LOCAL_CONTENT_ID_HISTORY")
		ENDIF
		//Boom, done
	ENDIF	
ENDPROC
FUNC BOOL IS_MISSION_COMPATABLE(SERVER_NEXT_JOB_VARS& serverVars, INT iNumberOfPlayersOnScript, BOOL bRockStarCreated, INT iMissionType, INT iLoop, BOOL bNeedsToBeTeam, INT iAverageRank, BOOL bLTS, BOOL bSameAsLast, INT iNewVSBitSet, BOOL bAdVersaryFlow, BOOL bAssassination)
	
	#IF IS_DEBUG_BUILD
	IF g_iCS_JOB_ForceMaxPlayers > 0
		iNumberOfPlayersOnScript = g_iCS_JOB_ForceMaxPlayers
		iAverageRank = g_iCS_JOB_ForceRank
	ENDIF
	TEXT_LABEL_23 tl23
	#ENDIF
	TEXT_LABEL_63 tl63  
	

	INT iType						= GET_EOJV_MISSION_TYPE(iLoop, !bRockStarCreated)
	INT iSubTypeType				= GET_EOJV_MISSION_SUB_TYPE_FOR_COMPAT(iLoop, !bRockStarCreated)


	BOOL bPassesSubType = TRUE
	
	IF iType = FMMC_TYPE_MISSION
		IF iSubTypeType = FMMC_MISSION_TYPE_FLOW_MISSION
		OR bRockStarCreated AND SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iLoop].iRootContentIdHash)
			bPassesSubType = FALSE
		ENDIF
	ENDIF
	
	IF iType = FMMC_TYPE_DEATHMATCH
		IF IS_CONTENT_KING_OF_THE_HILL(iType, iSubTypeType) != IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_KOTH)
			PRINTLN("IS_MISSION_COMPATABLE - NO - IS_CONTENT_KING_OF_THE_HILL != SERVER_BOOL_IS_KOTH")
			bPassesSubType = FALSE
		ENDIF
	ENDIF
	
	BOOL bMinPlayerOK 
	BOOL bMaxPlayerOk 
	
	IF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ARENA_SERIES)
		bMaxPlayerOk = TRUE
	ELSE
		bMaxPlayerOk = GET_EOJV_MISSION_MAX_PLAYERS(iLoop, !bRockStarCreated) >=  iNumberOfPlayersOnScript
	ENDIF
	
	IF bAdVersaryFlow
	OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)
	OR bAssassination
		bMinPlayerOK = TRUE
	ELSE
		bMinPlayerOK = ARE_THE_NUMBER_OF_PLAYERS_OK(GET_EOJV_MISSION_MIN_PLAYERS(iLoop, !bRockStarCreated), iNumberOfPlayersOnScript, bAdVersaryFlow)
	ENDIF
	
	IF bMinPlayerOK
	AND iType != FMMC_TYPE_GANGHIDEOUT
	AND bPassesSubType
	AND bMaxPlayerOk
	AND GET_EOJV_MISSION_RANK(iLoop, !bRockStarCreated) <= iAverageRank
	AND (bAssassination OR !CHECK_IF_VARIATION_IS_A_SPECIALISED_MISSION_TYPE(bRockStarCreated, iMissionType, iLoop, FALSE, TRUE))
	AND CHECK_IF_VARIATION_IS_TEAM_COMPATIBLE(bRockStarCreated, iMissionType, iLoop, bNeedsToBeTeam)
	AND CHECK_IF_VARIATION_PASSES_LTS_CHECK(serverVars, bLTS, iLoop, !bRockStarCreated)
	AND CHECK_IF_VARIATION_PASSES_SAME_AS_LAST_CHECK(bSameAsLast, iLoop, iMissionType, bAssassination)
	AND CHECK_IF_VARIATION_PASSES_NEW_VS_CHECK(serverVars, iNewVSBitSet, iLoop, !bRockStarCreated, bMinPlayerOK)
		// Don't want the Job we just done
		tl63 = GET_EOJV_MISSION_CONTENT_ID(iLoop, !bRockStarCreated)
		IF ARE_STRINGS_EQUAL(tl63, g_FMMC_STRUCT.tl31LoadedContentID)
			#IF IS_DEBUG_BUILD
			tl23 = GET_EOJV_MISSION_CONTENT_ID(iLoop, !bRockStarCreated)
			tl63 = GET_EOJV_MISSION_NAME(iLoop, !bRockStarCreated)
			PRINTLN("IS_MISSION_COMPATABLE - NO - ARE_STRINGS_EQUAL - g_FMMC_STRUCT.tl31LoadedContentID -[", iLoop , "].tlName = ", tl23, " - tlMissionName = ", tl63)
			#ENDIF
			RETURN FALSE
		ENDIF
		//PRINTLN("iLoop = ", iLoop)
		#IF IS_DEBUG_BUILD
		//IF g_bPrintMissionNames
			tl23 = GET_EOJV_MISSION_CONTENT_ID(iLoop, !bRockStarCreated)
			tl63 = GET_EOJV_MISSION_NAME(iLoop, !bRockStarCreated)
			PRINTLN("IS_MISSION_COMPATABLE - YES - [", iLoop , "].tlName = ", tl23, " - tlMissionName = ", tl63)
		//ENDIF
		#ENDIF
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		//IF g_bPrintMissionNames
			tl23 = GET_EOJV_MISSION_CONTENT_ID(iLoop, !bRockStarCreated)
			tl63 = GET_EOJV_MISSION_NAME(iLoop, !bRockStarCreated)
			PRINTLN("IS_MISSION_COMPATABLE - NO - [", iLoop , "].tlName = ", tl23, " - tlMissionName = ", tl63)
			PRINTLN("bMinPlayerOK = ", bMinPlayerOK)
			PRINTLN("iType != FMMC_TYPE_GANGHIDEOUT = ", iType != FMMC_TYPE_GANGHIDEOUT)
			PRINTLN("bPassesSubType = ", bPassesSubType)
			PRINTLN("GET_EOJV_MISSION_MAX_PLAYERS(iLoop, !bRockStarCreated) >=  iNumberOfPlayersOnScript = ", GET_EOJV_MISSION_MAX_PLAYERS(iLoop, !bRockStarCreated) >=  iNumberOfPlayersOnScript)
			PRINTLN("GET_EOJV_MISSION_RANK(iLoop, !bRockStarCreated) <= iAverageRank = ", GET_EOJV_MISSION_RANK(iLoop, !bRockStarCreated) <= iAverageRank)
			PRINTLN("NOT (CHECK_IF_VARIATION_IS_A_SPECIALISED_MISSION_TYPE(bRockStarCreated, iMissionType, iLoop, FALSE, TRUE)) = ", !(CHECK_IF_VARIATION_IS_A_SPECIALISED_MISSION_TYPE(bRockStarCreated, iMissionType, iLoop, FALSE, TRUE)))
			PRINTLN("CHECK_IF_VARIATION_IS_TEAM_COMPATIBLE(bRockStarCreated, iMissionType, iLoop, bNeedsToBeTeam) = ", CHECK_IF_VARIATION_IS_TEAM_COMPATIBLE(bRockStarCreated, iMissionType, iLoop, bNeedsToBeTeam))
			PRINTLN("CHECK_IF_VARIATION_PASSES_LTS_CHECK(bLTS, iLoop, !bRockStarCreated) = ", CHECK_IF_VARIATION_PASSES_LTS_CHECK(serverVars, bLTS, iLoop, !bRockStarCreated))
			PRINTLN("CHECK_IF_VARIATION_PASSES_SAME_AS_LAST_CHECK(bSameAsLast, iLoop, iMissionType) = ", CHECK_IF_VARIATION_PASSES_SAME_AS_LAST_CHECK(bSameAsLast, iLoop, iMissionType, bAssassination))
			PRINTLN("CHECK_IF_VARIATION_PASSES_NEW_VS_CHECK(iNewVSBitSet, iLoop, !bRockStarCreated, bMinPlayerOK) = ", CHECK_IF_VARIATION_PASSES_NEW_VS_CHECK(serverVars, iNewVSBitSet, iLoop, !bRockStarCreated, bMinPlayerOK))
		//ENDIF
		#ENDIF
		//IF (isSpecialisedMissionType)
			//PRINTLN("[CS_JOB] - CONVERT_VAR_NUMBER_TO_ARRAY_POSTION - IGNORING MISSION - IT IS A SPECIALISED MISSION TYPE (Heist, Contact Mission, Random Event)")
		//ELSE
			//PRINTLN("[CS_JOB] - CONVERT_VAR_NUMBER_TO_ARRAY_POSTION - IGNORING MISSION -MORE PEOPLE ARE ON THE SCRIPT THAT THE RANDOM MISSION I WANT TO LOAD HAS BEEN SET UP FOR")
		//ENDIF
		
		RETURN FALSE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT CONVERT_VAR_NUMBER_TO_ARRAY_POSTION(	SERVER_NEXT_JOB_VARS& serverVars, 
												INT 	iMissionType, 
												INT 	iNextMission, 
												INT 	iNumberOfPlayersOnScript, 
												BOOL 	bNeedsToBeTeam 				= FALSE, 
												INT 	iAverageRank 				= 1000, 
												BOOL 	bLTS 						= FALSE, 
												BOOL 	bRockStarCreated 			= TRUE, 
												BOOL 	bSameAsLast 				= FALSE, 
												INT 	iMissionTypeBitSet 			= 0, 
												BOOL 	bAdVersaryFlow				= FALSE, 
												BOOL 	bAssassination 				= FALSE)
	INT iLoop
	INT iLoopCount
	INT iLoopMax
	IF bRockStarCreated
		iLoopMax = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD
	ELSE
		iLoopMax = FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
	ENDIF
	
	FOR iLoop = 0 TO (iLoopMax - 1)
		IF IS_BIT_SET(GET_EOJV_MISSION_BIT_SET(iLoop, !bRockStarCreated), ciROCKSTAR_CREATED_MISSION_IN_USE)
		AND GET_EOJV_MISSION_TYPE(iLoop, !bRockStarCreated) = iMissionType
			IF IS_MISSION_COMPATABLE(serverVars, iNumberOfPlayersOnScript, bRockStarCreated, iMissionType, iLoop, bNeedsToBeTeam, iAverageRank, bLTS, bSameAsLast, iMissionTypeBitSet, bAdVersaryFlow, bAssassination)	
				iLoopCount++
				IF iLoopCount = iNextMission
					RETURN iLoop
				ENDIF
			ENDIF			
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

FUNC INT GET_ONLY_COMPATABLE_JOBS(	SERVER_NEXT_JOB_VARS &serverVars,
									MISSION_TYPE_COUNT &sTypeCounts, 
									INT iNumberOfPlayersOnScript, 
									INT iAverageRank = 1000,
									BOOL bTeam = FALSE, 
									BOOL bLTS = FALSE,
									BOOL bKingFailSafe = FALSE,
									BOOL bRockStarCreated = TRUE, 
									BOOL bAdVersaryFlow = FALSE, 
									BOOL bAssassination = FALSE)
	INT iLoop, iLoopMax
	IF bRockStarCreated
		iLoopMax = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD
	ELSE
		iLoopMax = FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
	ENDIF
	//get a random job
	INT iPosToGet = NETWORK_GET_RANDOM_INT_RANGED(0, serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iTotalNumberCompatable)
	PRINTLN(" [CS_JOB] GET_ONLY_COMPATABLE_JOBS NETWORK_GET_RANDOM_INT_RANGED(0, ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iTotalNumberCompatable, ") = ", iPosToGet)
	
	INT iCurrentPos
	//Loop through till we get that compatable one
	FOR iLoop = 0 TO (iLoopMax - 1)
		IF IS_BIT_SET(GET_EOJV_MISSION_BIT_SET(iLoop, !bRockStarCreated), ciROCKSTAR_CREATED_MISSION_IN_USE)
			IF iLoop != sTypeCounts.iLastMissionArrayPos
				IF IS_MISSION_COMPATABLE(serverVars, iNumberOfPlayersOnScript, bRockStarCreated, GET_EOJV_MISSION_TYPE(iLoop, !bRockStarCreated), iLoop, bTeam, iAverageRank, bLTS, FALSE, 0, bAdVersaryFlow, bAssassination)
					IF iCurrentPos = iPosToGet
					OR bKingFailSafe
						IF IS_NEW_RANDOM_JOB(serverVars, iLoop)
							PRINTLN(" [CS_JOB] GET_ONLY_COMPATABLE_JOBS iLoop = ", iLoop)
							RETURN iLoop
						ENDIF		
					ENDIF		
					iCurrentPos++
				ENDIF		
			ENDIF		
		ENDIF		
	ENDFOR
	RETURN -1
ENDFUNC

FUNC INT COUNT_NUMBER_OF_MISSIONS_THAT_ARE_COMPATABLE(	SERVER_NEXT_JOB_VARS& serverVars, 
														MISSION_TYPE_COUNT &sTypeCounts, 
														INT iMatchTypeUnlockBitSet, 
														INT iNumberOfPlayersOnScript, 
														INT iAverageRank = 1000, 
														BOOL bTeam = FALSE, 
														BOOL bLTS = FALSE, 
														BOOL bDoTypeCount = FALSE, 
														BOOL bRockStarCreated = TRUE,
														BOOL bSameAsLast = FALSE, 
														INT iMissionTypeBitSet = 0, 
														BOOL bAdVersaryFlow = FALSE, 
														BOOL bAssassination = FALSE)
	INT iCount, iLoop, iType, iLoopMax
	IF bRockStarCreated
		iLoopMax = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD
	ELSE
		iLoopMax = FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED
	ENDIF
	TEXT_LABEL_23 tl23
	FOR iLoop = 0 TO (iLoopMax - 1)
		//If it's active
		IF IS_BIT_SET(GET_EOJV_MISSION_BIT_SET(iLoop, !bRockStarCreated), ciROCKSTAR_CREATED_MISSION_IN_USE)
			iType = GET_EOJV_MISSION_TYPE(iLoop, !bRockStarCreated)
			//if it's unclocked
			IF IS_BIT_SET(iMatchTypeUnlockBitSet, iType)
				//If it's not the last mission we played
				tl23 = GET_EOJV_MISSION_CONTENT_ID(iLoop, !bRockStarCreated)
				IF NOT ARE_STRINGS_EQUAL(tl23, g_FMMC_STRUCT.tl31LoadedContentID)
					IF IS_MISSION_COMPATABLE(serverVars, iNumberOfPlayersOnScript, bRockStarCreated, iType, iLoop, bTeam, iAverageRank, bLTS, bSameAsLast, iMissionTypeBitSet, bAdVersaryFlow, bAssassination)
						IF bDoTypeCount
							IF bRockStarCreated
								IF iType < MAX_CLOUD_MISSION_TYPES
									sTypeCounts.iType[iType]++
								ENDIF	
							ELSE
								IF iType = FMMC_TYPE_RACE
									sTypeCounts.iTypeVerifiedType[ciNUMBER_OF_VERIFIED_TYPE_RACE]++
								ELIF iType = FMMC_TYPE_DEATHMATCH
									sTypeCounts.iTypeVerifiedType[ciNUMBER_OF_VERIFIED_TYPE_DM]++								
								ENDIF	
							ENDIF
						ENDIF	
						iCount++
					ENDIF	
				ELSE
					sTypeCounts.iLastMissionArrayPos = iLoop
					PRINTLN(" [CS_JOB] COUNT_NUMBER_OF_MISSIONS_THAT_ARE_COMPATABLE sTypeCounts.iLastMissionArrayPos = ", sTypeCounts.iLastMissionArrayPos)
				ENDIF		
			ENDIF	
		ENDIF		
	ENDFOR
	RETURN iCount
ENDFUNC

FUNC INT GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(INT iType, INT iAdversaryModeType = -1)
	INT iMode
	INT iArrayPos = NETWORK_GET_RANDOM_INT_RANGED(0, 5 - 1)
	IF iAdversaryModeType != -1
		SWITCH iAdversaryModeType
			//ADD NEW ADVERSARY MODES STUFF HERE!
			CASE ciNEWVS_TINYRACERS			RETURN g_sMPTunables.iTopdown[iArrayPos]
			CASE ciNEWVS_TURF_WARZONE		RETURN g_sMPTunables.iLandGrab[iArrayPos]
			CASE ciNEWVS_RESURRECTION		RETURN g_sMPTunables.iResurrection[iArrayPos]
			CASE ciNEWVS_OVERTIME_TURNS		RETURN g_sMPTunables.iOvertime[iArrayPos]
			CASE ciNEWVS_OVERTIME_RUMBLE	RETURN g_sMPTunables.iOvertimeRumble[iArrayPos]
			CASE ciNEWVS_POWER_MAD 			RETURN g_sMPTunables.iPowerMad[iArrayPos]
			CASE ciNEWVS_VEHICULAR_WARFARE	RETURN g_sMPTunables.iVehicleWarfare[iArrayPos]
			CASE ciNEWVS_BOMBUSHKA			RETURN g_sMPTunables.iBombushkaRun[iArrayPos]
			CASE ciNEWVS_STOCKPILE			RETURN g_sMPTunables.iStockpile[iArrayPos]
			CASE ciNEWVS_CONDEMNED			RETURN g_sMPTunables.iCondemned[iArrayPos]
			CASE ciNEWVS_AIRSHOOTOUT		RETURN g_sMPTunables.iAirShootout[iArrayPos]
			CASE ciNEWVS_HARD_TARGET		RETURN g_sMPTunables.iHardTarget[iArrayPos]	
			CASE ciNEWVS_SLASHERS			RETURN g_sMPTunables.iSlashers[iArrayPos]
			CASE ciNEWVS_HOSTILE_TAKEOVER	RETURN g_sMPTunables.iHostileTakeover[iArrayPos] 
			CASE ciNEWVS_AIRQUOTA			RETURN g_sMPTunables.iAirQuota[iArrayPos]
			CASE ciNEWVS_VENETIAN_JOB		RETURN g_sMPTunables.iVenetianJob[iArrayPos]
			CASE ciNEWVS_TRAP_DOOR			RETURN g_sMPTunables.iTrapDoor[iArrayPos]
			CASE ciNEWVS_SUMO_RUN					RETURN g_sMPTunables.iSumoRemix[iArrayPos]
			CASE ciNEWVS_STUNTING_PACK				RETURN g_sMPTunables.iHuntingPackRemix[iArrayPos]
			CASE ciNEWVS_TRADING_PLACES_REMIX		RETURN g_sMPTunables.iTradingPlacesRemix[iArrayPos]
			CASE ciNEWVS_RUNNING_BACK_REMIX			RETURN g_sMPTunables.iRunningBackRemix[iArrayPos]
		ENDSWITCH
	ENDIF
	SWITCH iType
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY
			iMode = NETWORK_GET_RANDOM_INT_RANGED(0, 1)
			IF iMode = 0
				RETURN g_sMPTunables.iShepherd[iArrayPos]
			ELSE
				RETURN g_sMPTunables.iSpeedRace[iArrayPos]
			ENDIF
		BREAK
		
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAVID_AND_GOLIATH
			iMode = NETWORK_GET_RANDOM_INT_RANGED(0, 1)
			IF iMode = 0
				RETURN g_sMPTunables.iDavidAndGoliath[iArrayPos]
			ELSE
				RETURN g_sMPTunables.iBeMyValentine[iArrayPos]
			ENDIF
		BREAK
		
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD
			iMode = NETWORK_GET_RANDOM_INT_RANGED(0, 1)
			IF iMode = 0
				RETURN g_sMPTunables.iRelay[iArrayPos]
			ELSE
				RETURN g_sMPTunables.iSpeedRace[iArrayPos]
			ENDIF
		BREAK
		
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE
			iMode = NETWORK_GET_RANDOM_INT_RANGED(0, 1)
			IF iMode = 0
				RETURN g_sMPTunables.iRelay[iArrayPos]
			ELSE
				RETURN g_sMPTunables.iShepherd[iArrayPos]
			ENDIF
		BREAK
		
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_HUNTDARK
			iMode = NETWORK_GET_RANDOM_INT_RANGED(0, 1)
			IF iMode = 0
				RETURN g_sMPTunables.iShepherd[iArrayPos]
			ELIF iMode = 1	
				RETURN g_sMPTunables.iSpeedRace[iArrayPos]
			ELSE
				RETURN g_sMPTunables.iRelay[iArrayPos]
			ENDIF
		BREAK
		
		
		CASE ciROCKSTAR_CREATED_2_NEW_VS_JUGGERNAUT
			RETURN g_sMPTunables.iJuggernaut[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_TURF_WAR
			RETURN g_sMPTunables.iTurfWars[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_POINTLESS
			RETURN g_sMPTunables.iPointless[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_VEHICLE_VENDETTA
			RETURN g_sMPTunables.iVehicleVendetta[iArrayPos]
		BREAK
		
		
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BE_MY_VALENTIN
			RETURN g_sMPTunables.iBeMyValentine[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SUMO
			RETURN g_sMPTunables.iSumo[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUGBY
			RETURN g_sMPTunables.iINCH_BY_INCH[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_POWER_PLAY
			RETURN g_sMPTunables.iPowerPlay[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_ENTOURAGE
			RETURN g_sMPTunables.iEntourage[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_IN_AND_OUT
			RETURN g_sMPTunables.iInAndOut[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAWN_RAID
			RETURN g_sMPTunables.iDawnRaid[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_DONT_CROSS_THE_LINE
			RETURN g_sMPTunables.iDeadline[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_LOST_DAMNED
			RETURN g_sMPTunables.iLostDamned[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_GUNSMITH
			RETURN g_sMPTunables.iGunsmith[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_TOUR_DE_FORCE
			RETURN g_sMPTunables.iTourDeForce[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DIRTY_SANCHEZ
			RETURN g_sMPTunables.iDirtySanchez[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RATG
			RETURN g_sMPTunables.iRageAgainstTheCampers[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_GRASS_GREENER
			RETURN g_sMPTunables.iGrassGreener[iArrayPos]
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_SUMO_ZONE
			RETURN g_sMPTunables.iSumoZone[iArrayPos]
		BREAK
		
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DROP_ZONE
			iMode = NETWORK_GET_RANDOM_INT_RANGED(0, 1)
			IF iMode = 0
				RETURN g_sMPTunables.iBeastVsSlasherV1[iArrayPos]
			ELSE
				RETURN g_sMPTunables.iExtraction[iArrayPos]
			ENDIF
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BVS1
			iMode = NETWORK_GET_RANDOM_INT_RANGED(0, 1)
			IF iMode = 0
				RETURN g_sMPTunables.iDropZone[iArrayPos]
			ELSE
				RETURN g_sMPTunables.iExtraction[iArrayPos]
			ENDIF
		BREAK
		CASE ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EXTRACTION
			iMode = NETWORK_GET_RANDOM_INT_RANGED(0, 1)
			IF iMode = 0
				RETURN g_sMPTunables.iDropZone[iArrayPos]
			ELSE
				RETURN g_sMPTunables.iBeastVsSlasherV1[iArrayPos]
			ENDIF
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC


//Get the root content ID of the mission that should appear next
FUNC INT GET_NEXT_VS_ROOT_CONTENT_ID(INT iNewVSBitSet, INT iMission)
	//If we're on a Come Out to play then pick Come Out to play then SM then hasta la vista and 
	//ADD NEW ADVERSARY MODES STUFF HERE!
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_SUMO_REMIX
			RETURN g_sMPTunables.iSumoRemix[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_HUNTING_PACK_REMIX
			RETURN g_sMPTunables.iHuntingPackRemix[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_TRADING_PLACES_REMIX
			RETURN g_sMPTunables.iTradingPlacesRemix[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_RUNNING_BACK_REMIX
			RETURN g_sMPTunables.iRunningBackRemix[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_WARFARE(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_VEHICULAR_WARFARE
			RETURN g_sMPTunables.iVehicleWarfare[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_BOMBUSHKA
			RETURN g_sMPTunables.iBombushkaRun[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VENETIAN_JOB(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_VENETIAN_JOB
			RETURN g_sMPTunables.iVenetianJob[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRAP_DOOR(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_TRAP_DOOR
			RETURN g_sMPTunables.iTrapDoor[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)	
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_STOCKPILE
			RETURN g_sMPTunables.iStockpile[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_CONDEMNED(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_CONDEMNED
			RETURN g_sMPTunables.iCondemned[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_AIRSHOOTOUT
			RETURN g_sMPTunables.iAirShootout[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_TINY_RACERS
			RETURN g_sMPTunables.iTopdown[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_TURF_WARS
			RETURN g_sMPTunables.iLandGrab[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_RESURRECTION
			RETURN g_sMPTunables.iResurrection[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
		
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_TURNS(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_OVERTIME
			RETURN g_sMPTunables.iOvertime[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_RUMBLE(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_OVERTIME_RUMBLE 
			RETURN g_sMPTunables.iOvertimeRumble[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
		IF iMission >= 0
		AND iMission < ciMax_POWER_MAD
			RETURN g_sMPTunables.iPowerMad[iMission]
		ENDIF
		RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(-1, g_FMMC_STRUCT.iAdversaryModeType)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iJuggernaut[0]
			CASE 1  RETURN g_sMPTunables.iJuggernaut[1]
			CASE 2  RETURN g_sMPTunables.iJuggernaut[2]
			CASE 3  RETURN g_sMPTunables.iJuggernaut[3]
			CASE 4  RETURN g_sMPTunables.iJuggernaut[4]
			CASE 5  RETURN g_sMPTunables.iJuggernaut[5]
			CASE 6  RETURN g_sMPTunables.iJuggernaut[6]
			CASE 7  RETURN g_sMPTunables.iJuggernaut[7]
			CASE 8  RETURN g_sMPTunables.iJuggernaut[8]
			CASE 9  RETURN g_sMPTunables.iJuggernaut[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_JUGGERNAUT)
		ENDSWITCH
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iTurfWars[0]
			CASE 1  RETURN g_sMPTunables.iTurfWars[1]
			CASE 2  RETURN g_sMPTunables.iTurfWars[2]
			CASE 3  RETURN g_sMPTunables.iTurfWars[3]
			CASE 4  RETURN g_sMPTunables.iTurfWars[4]
			CASE 5  RETURN g_sMPTunables.iTurfWars[5]
			CASE 6  RETURN g_sMPTunables.iTurfWars[6]
			CASE 7  RETURN g_sMPTunables.iTurfWars[7]
			CASE 8  RETURN g_sMPTunables.iTurfWars[8]
			CASE 9  RETURN g_sMPTunables.iTurfWars[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_TURF_WAR)
		ENDSWITCH
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POINTLESS(g_FMMC_STRUCT.iAdversaryModeType)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iPointless[0]
			CASE 1  RETURN g_sMPTunables.iPointless[1]
			CASE 2  RETURN g_sMPTunables.iPointless[2]
			CASE 3  RETURN g_sMPTunables.iPointless[3]
			CASE 4  RETURN g_sMPTunables.iPointless[4]
			CASE 5  RETURN g_sMPTunables.iPointless[5]
			CASE 6  RETURN g_sMPTunables.iPointless[6]
			CASE 7  RETURN g_sMPTunables.iPointless[7]
			CASE 8  RETURN g_sMPTunables.iPointless[8]
			CASE 9  RETURN g_sMPTunables.iPointless[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_POINTLESS)
		ENDSWITCH
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iVehicleVendetta[0]
			CASE 1  RETURN g_sMPTunables.iVehicleVendetta[1]
			CASE 2  RETURN g_sMPTunables.iVehicleVendetta[2]
			CASE 3  RETURN g_sMPTunables.iVehicleVendetta[3]
			CASE 4  RETURN g_sMPTunables.iVehicleVendetta[4]
			CASE 5  RETURN g_sMPTunables.iVehicleVendetta[5]
			CASE 6  RETURN g_sMPTunables.iVehicleVendetta[6]
			CASE 7  RETURN g_sMPTunables.iVehicleVendetta[7]
			CASE 8  RETURN g_sMPTunables.iVehicleVendetta[8]
			CASE 9  RETURN g_sMPTunables.iVehicleVendetta[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_VEHICLE_VENDETTA)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_ComeOutToPlay)
		SWITCH iMission
			CASE 0 RETURN g_sMPTunables.iComeOutToPlay0
			CASE 1 RETURN g_sMPTunables.iComeOutToPlay1
			CASE 2 RETURN g_sMPTunables.iComeOutToPlay2
			CASE 3 RETURN g_sMPTunables.iComeOutToPlay3
			CASE 4 RETURN g_sMPTunables.iComeOutToPlay4
			CASE 5 RETURN g_sMPTunables.iSiegeMentality0
			CASE 6 RETURN g_sMPTunables.iSiegeMentality1
			CASE 7 RETURN g_sMPTunables.iSiegeMentality2
			CASE 8 RETURN g_sMPTunables.iSiegeMentality3
			CASE 9 RETURN g_sMPTunables.iSiegeMentality4
			CASE 10 RETURN g_sMPTunables.iSiegeMentality5
			CASE 11 RETURN g_sMPTunables.iSiegeMentality6
			CASE 12 RETURN g_sMPTunables.iHastaLaVista0
			CASE 13 RETURN g_sMPTunables.iHastaLaVista1
			CASE 14 RETURN g_sMPTunables.iHastaLaVista2
			CASE 15 RETURN g_sMPTunables.iHastaLaVista3
			CASE 16 RETURN g_sMPTunables.iHastaLaVista4
		ENDSWITCH
	//If we're on a hasta la vista then pick hasta la vista Then SM
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista)
		SWITCH iMission
			CASE 0 RETURN g_sMPTunables.iHastaLaVista0
			CASE 1 RETURN g_sMPTunables.iHastaLaVista1
			CASE 2 RETURN g_sMPTunables.iHastaLaVista2
			CASE 3 RETURN g_sMPTunables.iHastaLaVista3
			CASE 4 RETURN g_sMPTunables.iHastaLaVista4
			CASE 5 RETURN g_sMPTunables.iSiegeMentality0
			CASE 6 RETURN g_sMPTunables.iSiegeMentality1
			CASE 7 RETURN g_sMPTunables.iSiegeMentality2
			CASE 8 RETURN g_sMPTunables.iSiegeMentality3
			CASE 9 RETURN g_sMPTunables.iSiegeMentality4
			CASE 10 RETURN g_sMPTunables.iSiegeMentality5
			CASE 11 RETURN g_sMPTunables.iSiegeMentality6
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
		SWITCH iMission
			CASE 0 RETURN g_sMPTunables.iSiegeMentality0
			CASE 1 RETURN g_sMPTunables.iSiegeMentality1
			CASE 2 RETURN g_sMPTunables.iSiegeMentality2
			CASE 3 RETURN g_sMPTunables.iSiegeMentality3
			CASE 4 RETURN g_sMPTunables.iSiegeMentality4
			CASE 5 RETURN g_sMPTunables.iSiegeMentality5
			CASE 6 RETURN g_sMPTunables.iSiegeMentality6
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_CrossTheLine)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iCrossTheLine[0]
			CASE 1  RETURN g_sMPTunables.iCrossTheLine[1]
			CASE 2  RETURN g_sMPTunables.iCrossTheLine[2]
			CASE 3  RETURN g_sMPTunables.iCrossTheLine[3]
			CASE 4  RETURN g_sMPTunables.iCrossTheLine[4]
			CASE 5  RETURN g_sMPTunables.iCrossTheLine[5]
			CASE 6  RETURN g_sMPTunables.iCrossTheLine[6]
			CASE 7  RETURN g_sMPTunables.iHuntingPack[0]
			CASE 8  RETURN g_sMPTunables.iHuntingPack[1]
			CASE 9  RETURN g_sMPTunables.iHuntingPack[2]
			CASE 10 RETURN g_sMPTunables.iHuntingPack[3]
			CASE 11 RETURN g_sMPTunables.iHuntingPack[4]
			CASE 12 RETURN g_sMPTunables.iHuntingPack[5]
			CASE 13 RETURN g_sMPTunables.iHuntingPack[6]
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HuntingPack)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iHuntingPack[0]
			CASE 1  RETURN g_sMPTunables.iHuntingPack[1]
			CASE 2  RETURN g_sMPTunables.iHuntingPack[2]
			CASE 3  RETURN g_sMPTunables.iHuntingPack[3]
			CASE 4  RETURN g_sMPTunables.iHuntingPack[4]
			CASE 5  RETURN g_sMPTunables.iHuntingPack[5]
			CASE 6  RETURN g_sMPTunables.iHuntingPack[6]
			CASE 7  RETURN g_sMPTunables.iCrossTheLine[0]
			CASE 8  RETURN g_sMPTunables.iCrossTheLine[1]
			CASE 9  RETURN g_sMPTunables.iCrossTheLine[2]
			CASE 10 RETURN g_sMPTunables.iCrossTheLine[3]
			CASE 11 RETURN g_sMPTunables.iCrossTheLine[4]
			CASE 12 RETURN g_sMPTunables.iCrossTheLine[5]
			CASE 13 RETURN g_sMPTunables.iCrossTheLine[6]
		ENDSWITCH

	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAVID_AND_GOLIATH)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iDavidAndGoliath[0]
			CASE 1  RETURN g_sMPTunables.iDavidAndGoliath[1]
			CASE 2  RETURN g_sMPTunables.iDavidAndGoliath[2]
			CASE 3  RETURN g_sMPTunables.iDavidAndGoliath[3]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAVID_AND_GOLIATH)
		ENDSWITCH
		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BE_MY_VALENTIN)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iBeMyValentine[0]
			CASE 1  RETURN g_sMPTunables.iBeMyValentine[1]
			CASE 2  RETURN g_sMPTunables.iBeMyValentine[2]
			CASE 3  RETURN g_sMPTunables.iBeMyValentine[3]
			CASE 4  RETURN g_sMPTunables.iBeMyValentine[4]
			CASE 5  RETURN g_sMPTunables.iBeMyValentine[5]
			CASE 6  RETURN g_sMPTunables.iBeMyValentine[6]
			CASE 7  RETURN g_sMPTunables.iBeMyValentine[7]
			CASE 8  RETURN g_sMPTunables.iBeMyValentine[8]
			CASE 9  RETURN g_sMPTunables.iBeMyValentine[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BE_MY_VALENTIN)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SUMO)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iSumo[0]
			CASE 1  RETURN g_sMPTunables.iSumo[1]
			CASE 2  RETURN g_sMPTunables.iSumo[2]
			CASE 3  RETURN g_sMPTunables.iSumo[3]
			CASE 4  RETURN g_sMPTunables.iSumo[4]
			CASE 5  RETURN g_sMPTunables.iSumo[5]
			CASE 6  RETURN g_sMPTunables.iSumo[6]
			CASE 7  RETURN g_sMPTunables.iSumo[7]
			CASE 8  RETURN g_sMPTunables.iSumo[8]
			CASE 9  RETURN g_sMPTunables.iSumo[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SUMO)
		ENDSWITCH
		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUGBY)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iinch_by_inch[0]
			CASE 1  RETURN g_sMPTunables.iinch_by_inch[1]
			CASE 2  RETURN g_sMPTunables.iinch_by_inch[2]
			CASE 3  RETURN g_sMPTunables.iinch_by_inch[3]
			CASE 4  RETURN g_sMPTunables.iinch_by_inch[4]
			CASE 5  RETURN g_sMPTunables.iinch_by_inch[5]
			CASE 6  RETURN g_sMPTunables.iinch_by_inch[6]
			CASE 7  RETURN g_sMPTunables.iinch_by_inch[7]
			CASE 8  RETURN g_sMPTunables.iinch_by_inch[8]
			CASE 9  RETURN g_sMPTunables.iinch_by_inch[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUGBY)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAWN_RAID)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iDawnRaid[0]
			CASE 1  RETURN g_sMPTunables.iDawnRaid[1]
			CASE 2  RETURN g_sMPTunables.iDawnRaid[2]
			CASE 3  RETURN g_sMPTunables.iDawnRaid[3]
			CASE 4  RETURN g_sMPTunables.iDawnRaid[4]
			CASE 5  RETURN g_sMPTunables.iDawnRaid[5]
			CASE 6  RETURN g_sMPTunables.iDawnRaid[6]
			CASE 7  RETURN g_sMPTunables.iDawnRaid[7]
			CASE 8  RETURN g_sMPTunables.iDawnRaid[8]
			CASE 9  RETURN g_sMPTunables.iDawnRaid[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAWN_RAID)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DIRTY_SANCHEZ)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iDirtySanchez[0]
			CASE 1  RETURN g_sMPTunables.iDirtySanchez[1]
			CASE 2  RETURN g_sMPTunables.iDirtySanchez[2]
			CASE 3  RETURN g_sMPTunables.iDirtySanchez[3]
			CASE 4  RETURN g_sMPTunables.iDirtySanchez[4]
			CASE 5  RETURN g_sMPTunables.iDirtySanchez[5]
			CASE 6  RETURN g_sMPTunables.iDirtySanchez[6]
			CASE 7  RETURN g_sMPTunables.iDirtySanchez[7]
			CASE 8  RETURN g_sMPTunables.iDirtySanchez[8]
			CASE 9  RETURN g_sMPTunables.iDirtySanchez[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DIRTY_SANCHEZ)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RATG)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iRageAgainstTheCampers[0]
			CASE 1  RETURN g_sMPTunables.iRageAgainstTheCampers[1]
			CASE 2  RETURN g_sMPTunables.iRageAgainstTheCampers[2]
			CASE 3  RETURN g_sMPTunables.iRageAgainstTheCampers[3]
			CASE 4  RETURN g_sMPTunables.iRageAgainstTheCampers[4]
			CASE 5  RETURN g_sMPTunables.iRageAgainstTheCampers[5]
			CASE 6  RETURN g_sMPTunables.iRageAgainstTheCampers[6]
			CASE 7  RETURN g_sMPTunables.iRageAgainstTheCampers[7]
			CASE 8  RETURN g_sMPTunables.iRageAgainstTheCampers[8]
			CASE 9  RETURN g_sMPTunables.iRageAgainstTheCampers[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RATG)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_GRASS_GREENER)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iGrassGreener[0]
			CASE 1  RETURN g_sMPTunables.iGrassGreener[1]
			CASE 2  RETURN g_sMPTunables.iGrassGreener[2]
			CASE 3  RETURN g_sMPTunables.iGrassGreener[3]
			CASE 4  RETURN g_sMPTunables.iGrassGreener[4]
			CASE 5  RETURN g_sMPTunables.iGrassGreener[5]
			CASE 6  RETURN g_sMPTunables.iGrassGreener[6]
			CASE 7  RETURN g_sMPTunables.iGrassGreener[7]
			CASE 8  RETURN g_sMPTunables.iGrassGreener[8]
			CASE 9  RETURN g_sMPTunables.iGrassGreener[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_GRASS_GREENER)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_SUMO_ZONE)
		SWITCH iMission
			CASE 0 RETURN g_sMPTunables.iSumoZone[0]
			CASE 1 RETURN g_sMPTunables.iSumoZone[1]
			CASE 2 RETURN g_sMPTunables.iSumoZone[2]
			CASE 3 RETURN g_sMPTunables.iSumoZone[3]
			CASE 4 RETURN g_sMPTunables.iSumoZone[4]
			CASE 5 RETURN g_sMPTunables.iSumoZone[5]
			CASE 6 RETURN g_sMPTunables.iSumoZone[6]
			CASE 7 RETURN g_sMPTunables.iSumoZone[7]
			CASE 8 RETURN g_sMPTunables.iSumoZone[8]
			CASE 9 RETURN g_sMPTunables.iSumoZone[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_SUMO_ZONE)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_TOUR_DE_FORCE)
		SWITCH iMission
			CASE 0 RETURN g_sMPTunables.iTourDeForce[0]
			CASE 1 RETURN g_sMPTunables.iTourDeForce[1]
			CASE 2 RETURN g_sMPTunables.iTourDeForce[2]
			CASE 3 RETURN g_sMPTunables.iTourDeForce[3]
			CASE 4 RETURN g_sMPTunables.iTourDeForce[4]
			CASE 5 RETURN g_sMPTunables.iTourDeForce[5]
			CASE 6 RETURN g_sMPTunables.iTourDeForce[6]
			CASE 7 RETURN g_sMPTunables.iTourDeForce[7]
			CASE 8 RETURN g_sMPTunables.iTourDeForce[8]
			CASE 9 RETURN g_sMPTunables.iTourDeForce[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_TOUR_DE_FORCE)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_GUNSMITH)
		SWITCH iMission
			CASE 0 RETURN g_sMPTunables.iGunsmith[0]
			CASE 1 RETURN g_sMPTunables.iGunsmith[1]
			CASE 2 RETURN g_sMPTunables.iGunsmith[2]
			CASE 3 RETURN g_sMPTunables.iGunsmith[3]
			CASE 4 RETURN g_sMPTunables.iGunsmith[4]
			CASE 5 RETURN g_sMPTunables.iGunsmith[5]
			CASE 6 RETURN g_sMPTunables.iGunsmith[6]
			CASE 7 RETURN g_sMPTunables.iGunsmith[7]
			CASE 8 RETURN g_sMPTunables.iGunsmith[8]
			CASE 9 RETURN g_sMPTunables.iGunsmith[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_GUNSMITH)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_POWER_PLAY)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iPowerPlay[0]
			CASE 1  RETURN g_sMPTunables.iPowerPlay[1]
			CASE 2  RETURN g_sMPTunables.iPowerPlay[2]
			CASE 3  RETURN g_sMPTunables.iPowerPlay[3]
			CASE 4  RETURN g_sMPTunables.iPowerPlay[4]
			CASE 5  RETURN g_sMPTunables.iPowerPlay[5]
			CASE 6  RETURN g_sMPTunables.iPowerPlay[6]
			CASE 7  RETURN g_sMPTunables.iPowerPlay[7]
			CASE 8  RETURN g_sMPTunables.iPowerPlay[8]
			CASE 9  RETURN g_sMPTunables.iPowerPlay[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_POWER_PLAY)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_ENTOURAGE)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iEntourage[0]
			CASE 1  RETURN g_sMPTunables.iEntourage[1]
			CASE 2  RETURN g_sMPTunables.iEntourage[2]
			CASE 3  RETURN g_sMPTunables.iEntourage[3]
			CASE 4  RETURN g_sMPTunables.iEntourage[4]
			CASE 5  RETURN g_sMPTunables.iEntourage[5]
			CASE 6  RETURN g_sMPTunables.iEntourage[6]
			CASE 7  RETURN g_sMPTunables.iEntourage[7]
			CASE 8  RETURN g_sMPTunables.iEntourage[8]
			CASE 9  RETURN g_sMPTunables.iEntourage[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_ENTOURAGE)
		ENDSWITCH
		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_DONT_CROSS_THE_LINE)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iDeadline[0]
			CASE 1  RETURN g_sMPTunables.iDeadline[1]
			CASE 2  RETURN g_sMPTunables.iDeadline[2]
			CASE 3  RETURN g_sMPTunables.iDeadline[3]
			CASE 4  RETURN g_sMPTunables.iDeadline[4]
			CASE 5  RETURN g_sMPTunables.iDeadline[5]
			CASE 6  RETURN g_sMPTunables.iDeadline[6]
			CASE 7  RETURN g_sMPTunables.iDeadline[7]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_DONT_CROSS_THE_LINE)
		ENDSWITCH
		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_LOST_DAMNED)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iLostDamned[0]
			CASE 1  RETURN g_sMPTunables.iLostDamned[1]
			CASE 2  RETURN g_sMPTunables.iLostDamned[2]
			CASE 3  RETURN g_sMPTunables.iLostDamned[3]
			CASE 4  RETURN g_sMPTunables.iLostDamned[4]
			CASE 5  RETURN g_sMPTunables.iLostDamned[5]
			CASE 6  RETURN g_sMPTunables.iLostDamned[6]
			CASE 7  RETURN g_sMPTunables.iLostDamned[7]
			CASE 8  RETURN g_sMPTunables.iLostDamned[8]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_LOST_DAMNED)
		ENDSWITCH
	
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_IN_AND_OUT)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iInAndOut[0]
			CASE 1  RETURN g_sMPTunables.iInAndOut[1]
			CASE 2  RETURN g_sMPTunables.iInAndOut[2]
			CASE 3  RETURN g_sMPTunables.iInAndOut[3]
			CASE 4  RETURN g_sMPTunables.iInAndOut[4]
			CASE 5  RETURN g_sMPTunables.iInAndOut[5]
			CASE 6  RETURN g_sMPTunables.iInAndOut[6]
			CASE 7  RETURN g_sMPTunables.iInAndOut[7]
			CASE 8  RETURN g_sMPTunables.iInAndOut[8]
			CASE 9  RETURN g_sMPTunables.iInAndOut[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_IN_AND_OUT)
		ENDSWITCH
		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DROP_ZONE)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iDropZone[0]
			CASE 1  RETURN g_sMPTunables.iDropZone[1]
			CASE 2  RETURN g_sMPTunables.iDropZone[2]
			CASE 3  RETURN g_sMPTunables.iDropZone[3]
			CASE 4  RETURN g_sMPTunables.iDropZone[4]
			CASE 5  RETURN g_sMPTunables.iDropZone[5]
			CASE 6  RETURN g_sMPTunables.iDropZone[6]
			CASE 7  RETURN g_sMPTunables.iDropZone[7]
			CASE 8  RETURN g_sMPTunables.iDropZone[8]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DROP_ZONE)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BVS1)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iBeastVsSlasherV1[0]
			CASE 1  RETURN g_sMPTunables.iBeastVsSlasherV1[1]
			CASE 2  RETURN g_sMPTunables.iBeastVsSlasherV1[2]
			CASE 3  RETURN g_sMPTunables.iBeastVsSlasherV1[3]
			CASE 4  RETURN g_sMPTunables.iBeastVsSlasherV1[4]
			CASE 5  RETURN g_sMPTunables.iBeastVsSlasherV1[5]
			CASE 6  RETURN g_sMPTunables.iBeastVsSlasherV1[6]
			CASE 7  RETURN g_sMPTunables.iBeastVsSlasherV1[7]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BVS1)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EXTRACTION)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iExtraction[0]
			CASE 1  RETURN g_sMPTunables.iExtraction[1]
			CASE 2  RETURN g_sMPTunables.iExtraction[2]
			CASE 3  RETURN g_sMPTunables.iExtraction[3]
			CASE 4  RETURN g_sMPTunables.iExtraction[4]
			CASE 5  RETURN g_sMPTunables.iExtraction[5]
			CASE 6  RETURN g_sMPTunables.iExtraction[6]
			CASE 7  RETURN g_sMPTunables.iExtraction[7]
			CASE 8  RETURN g_sMPTunables.iExtraction[8]
			CASE 9  RETURN g_sMPTunables.iExtraction[9]
			DEFAULT
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EXTRACTION)
		ENDSWITCH
		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_HUNTDARK)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iHuntDark[0]
			CASE 1  RETURN g_sMPTunables.iHuntDark[1]
			CASE 2  RETURN g_sMPTunables.iHuntDark[2]
			CASE 3  RETURN g_sMPTunables.iHuntDark[3]
			CASE 4  RETURN g_sMPTunables.iHuntDark[4]
			CASE 5  
			CASE 6  
			CASE 7  
			CASE 8  
			CASE 9  
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_HUNTDARK)
			
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iSpeedRace[0]
			CASE 1  RETURN g_sMPTunables.iSpeedRace[1]
			CASE 2  RETURN g_sMPTunables.iSpeedRace[2]
			CASE 3  RETURN g_sMPTunables.iSpeedRace[3]
			CASE 4  RETURN g_sMPTunables.iSpeedRace[4]
			CASE 5  
			CASE 6  
			CASE 7  
			CASE 8  
			CASE 9  
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE)
		ENDSWITCH
		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iShepherd[0]
			CASE 1  RETURN g_sMPTunables.iShepherd[1]
			CASE 2  RETURN g_sMPTunables.iShepherd[2]
			CASE 3  RETURN g_sMPTunables.iShepherd[3]
			CASE 4  RETURN g_sMPTunables.iShepherd[4]
			CASE 5  
			CASE 6  
			CASE 7  
			CASE 8  
			CASE 9  
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD)
		ENDSWITCH
		
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iRelay[0]
			CASE 1  RETURN g_sMPTunables.iRelay[1]
			CASE 2  RETURN g_sMPTunables.iRelay[2]
			CASE 3  RETURN g_sMPTunables.iRelay[3]
			CASE 4  RETURN g_sMPTunables.iRelay[4]
			CASE 5  
			CASE 6  
			CASE 7  
			CASE 8  
			CASE 9  
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY)
		ENDSWITCH
	
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUNNINGBACK)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iRunningBack[0]
			CASE 1  RETURN g_sMPTunables.iRunningBack[1]
			CASE 2  RETURN g_sMPTunables.iRunningBack[2]
			CASE 3  RETURN g_sMPTunables.iEBC[0]
			CASE 4  RETURN g_sMPTunables.iEBC[1]
			CASE 5  RETURN g_sMPTunables.iEBC[2]
			CASE 6  RETURN g_sMPTunables.iEBC[3]
			CASE 7  
			CASE 8  
			CASE 9  
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUNNINGBACK)
		ENDSWITCH
	ELIF IS_BIT_SET(iNewVSBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EBC)
		SWITCH iMission
			CASE 0  RETURN g_sMPTunables.iEBC[0]
			CASE 1  RETURN g_sMPTunables.iEBC[1]
			CASE 2  RETURN g_sMPTunables.iEBC[2]
			CASE 3  RETURN g_sMPTunables.iEBC[3]
			CASE 4  RETURN g_sMPTunables.iRunningBack[0]
			CASE 5  RETURN g_sMPTunables.iRunningBack[1]
			CASE 6  RETURN g_sMPTunables.iRunningBack[2]
			CASE 7  
			CASE 8  
			CASE 9  
				RETURN GET_RANDOM_LOWRIDER_ADVERSRAY_MISSION(ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EBC)
		ENDSWITCH
	ENDIF

	RETURN -1
ENDFUNC



FUNC INT CV2_GET_CURRENT_JOB_PLAYLIST_POS()
	INT iLoop
	REPEAT ciMAX_FM_CORONA_PLAYLIST_LENGTH iLoop
		IF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iFmCoronaPlaylistRootContentId[iLoop]
			RETURN iLoop
		ENDIF
	ENDREPEAT
	RETURN 0
ENDFUNC



//Gets the next compatable new VS or the order specified in the tunables
FUNC INT GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(	SERVER_NEXT_JOB_VARS	&serverVars,
														INT 					iNewVSBitSet 			    = 0, 
														BOOL					bCoronaFlowPlay				= FALSE, 
														BOOL					bCoronaFlowAdversary		= FALSE,
														BOOL					bCoronaFlowTarget			= FALSE,
														BOOL					bCoronaFlowHotring			= FALSE,
														BOOL					bAssassination				= FALSE,
														BOOL					bCoronaFlowArena			= FALSE,
														BOOL					bCoronaFlowRaceSeries		= FALSE,
														BOOL					bCoronaFlowSurvivalSeries	= FALSE,
														BOOL					bCoronaFlowOpenWheelSeries	= FALSE,
														BOOL					bCoronaFlowStreetRaceSeries	= FALSE,
														BOOL					bCoronaFlowPursuitSeries	= FALSE,
														BOOL					bCoronaFlowCommunitySeries	= FALSE,
														BOOL					bCoronaFlowCayoPericoSeries	= FALSE
														#IF FEATURE_GEN9_EXCLUSIVE , BOOL bCoronaFlowHSWRaceSeries = FALSE #ENDIF 
														)
	INT iLoop
	INT iLoopContent
	INT iRootContentIDHashToGet
	INT iAdversaryPlaylist
	
	//INT iMissionType = FMMC_TYPE_MISSION
//	IF bCoronaFlowPlay
//		IF bCoronaFlowAdversary
//		AND g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
//			iMissionType = FMMC_TYPE_MISSION
//		ELSE
//			iMissionType = FMMC_TYPE_RACE
//		ENDIF
//	ENDIF
	//If it's for the corona flow then get the next mission
	
	INT iV2CoronaCurrentPos
	INT iV2CoronaCurrentLength
	IF bAssassination
		IF gAssassinationMissionData.iActiveMissionArrayPos != -1
			iV2CoronaCurrentPos = gAssassinationMissionData.iActiveMissionArrayPos
		ELSE
			bAssassination = FALSE
		ENDIF
	ELIF bCoronaFlowPlay
		IF bCoronaFlowAdversary
		OR bCoronaFlowTarget
		OR bCoronaFlowHotring
		OR bCoronaFlowArena
		OR bCoronaFlowRaceSeries
		OR bCoronaFlowSurvivalSeries
		OR bCoronaFlowOpenWheelSeries
		OR bCoronaFlowStreetRaceSeries
		OR bCoronaFlowPursuitSeries
		#IF FEATURE_GEN9_EXCLUSIVE
		OR bCoronaFlowHSWRaceSeries
		#ENDIF
		OR bCoronaFlowCommunitySeries
		OR bCoronaFlowCayoPericoSeries
			IF IS_CURRENT_MISSION_UGC_NON_ROCKSTAR()
				IF bCoronaFlowArena
					iAdversaryPlaylist = FMMC_TYPE_ARENA_SERIES
				ELIF bCoronaFlowHotring
					iAdversaryPlaylist = FMMC_TYPE_HOTRING_SERIES
				ELIF bCoronaFlowTarget
					iAdversaryPlaylist = FMMC_TYPE_TARGET_ASSAULT_SERIES
				ELIF bCoronaFlowAdversary
					iAdversaryPlaylist = FMMC_TYPE_ADVERSARY_SERIES
				ELIF bCoronaFlowRaceSeries
					iAdversaryPlaylist = FMMC_TYPE_RACE_SERIES
				ELIF bCoronaFlowSurvivalSeries
					iAdversaryPlaylist = FMMC_TYPE_SURVIVAL_SERIES
				ELIF bCoronaFlowOpenWheelSeries
					iAdversaryPlaylist = FMMC_TYPE_OPEN_WHEEL_SERIES
				ELIF bCoronaFlowStreetRaceSeries
					iAdversaryPlaylist = FMMC_TYPE_STREET_RACE_SERIES
				ELIF bCoronaFlowPursuitSeries
					iAdversaryPlaylist = FMMC_TYPE_PURSUIT_SERIES
				#IF FEATURE_GEN9_EXCLUSIVE
				ELIF bCoronaFlowHSWRaceSeries
					iAdversaryPlaylist = FMMC_TYPE_HSW_RACE_SERIES
				#ENDIF
				ELIF bCoronaFlowCommunitySeries
					iAdversaryPlaylist = FMMC_TYPE_COMMUNITY_SERIES
				ELIF bCoronaFlowCayoPericoSeries
					iAdversaryPlaylist = FMMC_TYPE_CAYO_PERICO_SERIES
				ENDIF
			ELSE
				iAdversaryPlaylist = CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)
			ENDIF
			iV2CoronaCurrentPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(CV2_GET_ADVERSARY_SERIES_ARRAY_POS(iAdversaryPlaylist))
			iV2CoronaCurrentLength = CV2_GET_V2_ADVERSARY_SERIES_LENGTH(iAdversaryPlaylist)
		ELSE
			iV2CoronaCurrentPos = CV2_GET_CURRENTLY_ACTIVE_V2_CORONA_POS()
			iV2CoronaCurrentLength = CV2_GET_V2_CORONA_FLOW_LENGTH()
		ENDIF
	ENDIF
	
//	iV2CoronaCurrentPos++
//	IF iV2CoronaCurrentPos >= iV2CoronaCurrentLength
//		iV2CoronaCurrentPos = 0
//	ENDIF
	
	FOR iLoop = 0 TO (17)
		//If it's for the corona flow then grab the next mission in the flow
		IF bAssassination
			iRootContentIDHashToGet = GET_ASSASSINATION_MISSION_ROOT_CONTENT_ID_HASH(iV2CoronaCurrentPos)
			iV2CoronaCurrentPos++
			IF iV2CoronaCurrentPos >= ciASSASSINATION_MISSION_COUNT
				iV2CoronaCurrentPos = 0
			ENDIF
			PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - iV2CoronaCurrentPos = ", iV2CoronaCurrentPos, " - iRootContentIDHashToGet = ", iRootContentIDHashToGet)
		ELIF bCoronaFlowPlay
		AND iV2CoronaCurrentPos != -1
			IF bCoronaFlowAdversary
			OR bCoronaFlowTarget
			OR bCoronaFlowHotring
			OR bCoronaFlowArena
			OR bCoronaFlowRaceSeries
			OR bCoronaFlowSurvivalSeries
			OR bCoronaFlowOpenWheelSeries
			OR bCoronaFlowStreetRaceSeries
			OR bCoronaFlowPursuitSeries
			#IF FEATURE_GEN9_EXCLUSIVE
			OR bCoronaFlowHSWRaceSeries
			#ENDIF
			OR bCoronaFlowCommunitySeries
			OR bCoronaFlowCayoPericoSeries
				iRootContentIDHashToGet = g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[CV2_GET_ADVERSARY_SERIES_ARRAY_POS(iAdversaryPlaylist)][iV2CoronaCurrentPos]
			ELSE
				iRootContentIDHashToGet = g_sMPTunables.iFmCoronaPlaylistRootContentId[iV2CoronaCurrentPos]
			ENDIF
			iV2CoronaCurrentPos++
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ArenaWarsTutorialMission")
			AND iV2CoronaCurrentPos = GET_COMMANDLINE_PARAM_INT("sc_ArenaWarsTutorialMission")
				iV2CoronaCurrentPos ++
			ENDIF
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ArenaWarsSecondMission")
			AND iV2CoronaCurrentPos = GET_COMMANDLINE_PARAM_INT("sc_ArenaWarsSecondMission")
				iV2CoronaCurrentPos ++
			ENDIF
			#ENDIF
			IF iV2CoronaCurrentPos >= iV2CoronaCurrentLength
				iV2CoronaCurrentPos = 0
			ENDIF
			PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - iV2CoronaCurrentPos = ", iV2CoronaCurrentPos, " - iRootContentIDHashToGet = ", iRootContentIDHashToGet)
		ELSE
			iRootContentIDHashToGet = GET_NEXT_VS_ROOT_CONTENT_ID(iNewVSBitSet, iLoop)
		ENDIF
		
		PRINTLN("[NEWVS] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - iRootContentIDHashToGet = ", iRootContentIDHashToGet)

		IF iRootContentIDHashToGet != -1
			IF bCoronaFlowCommunitySeries
			
				FOR iLoopContent = 0 TO (FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED - 1)
					IF iRootContentIDHashToGet = g_FMMC_ROCKSTAR_VERIFIED.sDefaultCoronaOptions[iLoopContent].iRootContentIdHash
						IF IS_BIT_SET(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iLoopContent].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
							IF IS_NEW_RANDOM_JOB(serverVars, iLoopContent, TRUE, bCoronaFlowPlay)
								PRINTLN("[NEWVS] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - Verified - RETURING iLoopContent = ", iLoopContent, " = ", g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iLoopContent].tlMissionName)
								RETURN iLoopContent
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			
			ELSE
		
				FOR iLoopContent = 0 TO (FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD - 1)
					IF iRootContentIDHashToGet = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iLoopContent].iRootContentIdHash
						IF IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoopContent].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
							IF IS_NEW_RANDOM_JOB(serverVars, iLoopContent, FALSE, bCoronaFlowPlay)
								PRINTLN("[NEWVS] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - Created - RETURING iLoopContent = ", iLoopContent, " = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoopContent].tlMissionName)
								RETURN iLoopContent
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
			ENDIF
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

// ditch this and use FMMC_ROCKSTAR_CREATED_STRUCT, iTotalNumMissions get_random int
PROC GET_NEXT_JOB_TO_LOAD(	SERVER_NEXT_JOB_VARS	&serverVars, 
							MISSION_TYPE_COUNT 		&sTypeCounts, 
							INT 					iMissionType, 
							INT 					&iReturn, 
							INT 					iNumberOfPlayersOnScript 	= 0, 
							INT 					iAverageRank 				= 1000,
							BOOL 					bLTS 						= FALSE, 
							BOOL					bRockStarCreated 			= TRUE, 
							BOOL					bSameAsLast					= FALSE, 
							INT 					iMissionTypeBitSet 			= 0)

	INT iNextMission 
	INT iRange
	BOOL bRecentHistoryCheck
	BOOL bNeedsToBeTeam = SHOULD_PICK_TEAM(serverVars) 
	
	IF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_KOTH)
		bNeedsToBeTeam = FALSE
	ENDIF
	
	//Cap it if it's shit
	IF iMissionType >= MAX_CLOUD_MISSION_TYPES
		iMissionType = 0
	ENDIF
	
	// KEITH 5/5/13: This needs a slight revamp to ignore randomly choosing Heists or Contact Missions - I've made a change within CONVERT_VAR_NUMBER_TO_ARRAY_POSTION()
	IF NOT bRockStarCreated
		IF iMissionType = FMMC_TYPE_RACE
			iRange = sTypeCounts.iTypeVerifiedType[ciNUMBER_OF_VERIFIED_TYPE_RACE]
			PRINTLN(" [BW][CS_JOB] iRange = sTypeCounts.iTypeVerifiedType[ciNUMBER_OF_VERIFIED_TYPE_RACE]         = ", iRange)
		ELSE
			iRange = sTypeCounts.iTypeVerifiedType[ciNUMBER_OF_VERIFIED_TYPE_DM]
			PRINTLN(" [BW][CS_JOB] iRange = sTypeCounts.iTypeVerifiedType[ciNUMBER_OF_VERIFIED_TYPE_DM]           = ", iRange)
		ENDIF
	//Needs to be an LTF
	ELIF bLTS
		iRange = serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberLTSCompatable
		PRINTLN(" [BW][CS_JOB] iRange = serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberLTSCompatable     = ", iRange)
	//Needs to be same as the last type we played
	ELIF bSameAsLast
		iRange = serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfSameAsLastFound
		PRINTLN(" [BW][CS_JOB] iRange = serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfSameAsLastFound = ", iRange)
	ELIF iMissionTypeBitSet != 0
		iRange = serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound
		PRINTLN(" [BW][CS_JOB] [NEWVS] iRange = serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound = ", iRange)
	ELSE
		iRange = sTypeCounts.iType[iMissionType]
		//Set that we want to check the recent history
		bRecentHistoryCheck = TRUE
		PRINTLN(" [BW][CS_JOB] iRange = sTypeCounts.iType[iMissionType] = ", iRange)
	ENDIF
	
	//Get the next mission
	iNextMission = NETWORK_GET_RANDOM_INT_RANGED(0, iRange)		
	PRINTLN(" [BW][CS_JOB] iNextMission = NETWORK_GET_RANDOM_INT_RANGED(0, ", iRange, ") = ", iNextMission)

	//cap it
	IF iNextMission > iRange
		iNextMission = NETWORK_GET_RANDOM_INT()
		PRINTLN(" [BW][CS_JOB] iNextMission = NETWORK_GET_RANDOM_INT() = ", iNextMission)
		iNextMission = (iNextMission%iRange)
		PRINTLN(" [BW][CS_JOB] iNextMission = NETWORK_GET_RANDOM_INT()%", iRange, " = ", iNextMission)
	ENDIF
	
	IF bRecentHistoryCheck
	AND iMissionTypeBitSet = 0
		//Check to see if the mission is in my local recent history, if it is get another. 
//		IF IS_MISSION_IN_RECENT_HISTORY(iNextMission, sTypeCounts)
//			//Bomb out
//			iReturn = -1		
//			EXIT
//		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_NEXT_JOB_TO_LOAD")
	PRINTLN("iMissionType                = ", iMissionType)
	PRINTLN(" [BW][CS_JOB]  iNextMission = ", iNextMission)
	IF NOT bRockStarCreated
		IF iMissionType = FMMC_TYPE_RACE
			PRINTLN("sTypeCounts.iTypeVerifiedType[ciNUMBER_OF_VERIFIED_TYPE_RACE]                = ", sTypeCounts.iTypeVerifiedType[ciNUMBER_OF_VERIFIED_TYPE_RACE])
		ELSE
			PRINTLN("sTypeCounts.iTypeVerifiedType[ciNUMBER_OF_VERIFIED_TYPE_DM]                = ", sTypeCounts.iTypeVerifiedType[ciNUMBER_OF_VERIFIED_TYPE_DM])
		ENDIF		
	ELIF bSameAsLast
		PRINTLN("[SamsAsLast] serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfSameAsLastFound = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfSameAsLastFound)
	ELIF bLTS
		PRINTLN("serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberLTSCompatable = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfSameAsLastFound)
	ELIF iMissionTypeBitSet != 0
		PRINTLN(" [BW][CS_JOB] [NEWVS] iRange = serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound)
	ELSE
		PRINTLN("sTypeCounts.iType[", iMissionType, "]                = ", sTypeCounts.iType[iMissionType])
	ENDIF
	#ENDIF
	iReturn = CONVERT_VAR_NUMBER_TO_ARRAY_POSTION(	serverVars, iMissionType, iNextMission, iNumberOfPlayersOnScript, bNeedsToBeTeam, 
													iAverageRank, bLTS, bRockStarCreated, bSameAsLast, iMissionTypeBitSet, 
													IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ADVERSARY_SERIES_V2), 
													IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION))
ENDPROC

CONST_INT ATTEMPTS_LIMIT 100
CONST_INT ATTEMPTS_LIMIT_FAIL_SAFE 200
CONST_INT ATTEMPTS_LIMIT_FAIL_SAFE_KING 400

PROC CLEAR_ATTEMPTS_LIMIT(SERVER_NEXT_JOB_VARS& serverVars)
	PRINTLN(" [CS_JOB] CLEAR_ATTEMPTS_LIMIT")
	serverVars.iAttempts = 0
ENDPROC

PROC CLEAR_JOB_TYPES_FOUND(SERVER_NEXT_JOB_VARS& serverVars)
	INT i
	REPEAT NUM_JOBS_TO_GRAB i
		IF IS_BIT_SET(serverVars.iFoundJobOfTypeBitset, i)
			CLEAR_BIT(serverVars.iFoundJobOfTypeBitset, i)
		ENDIF
	ENDREPEAT
	PRINTLN(" [CS_JOB] CLEAR_JOB_TYPES_FOUND ")
ENDPROC

//Get the number of refreshes
FUNC INT GET_NUMBER_OF_REFRESHES(SERVER_NEXT_JOB_VARS& serverVars)
	IF NOT CANT_REFRESH_YET(serverVars)
		//We've got 18
		IF serverVars.iNumberCompatable = NUM_JOBS_TO_GRAB
			RETURN 2
		//we've got less than 18 but more than 12	
		ELIF serverVars.iNumberCompatable >= (NUM_JOBS_ON_ONE_PAGE + NUM_JOBS_ON_ONE_PAGE)
			RETURN 1
		ENDIF
	ENDIF
	//no, none, unlucky
	RETURN 0
ENDFUNC

FUNC INT GET_NUM_REFRESHES_LEFT(SERVER_NEXT_JOB_VARS& serverVars)

	IF CV2_IS_THIS_JOB_A_SERIES_JOB(TRUE)
		RETURN 0
	ENDIF
	 // #IF FEATURE_STUNT
	
	IF ON_LAST_PAGE_REFRESHED(serverVars)
		RETURN 0
	ELIF ON_SECOND_PAGE_REFRESHED(serverVars)
		INT iNumber = GET_NUMBER_OF_REFRESHES(serverVars)
		IF iNumber = 1
		OR iNumber = 0
			RETURN 0
		ELSE	
			RETURN 1
		ENDIF
	ENDIF	
	RETURN GET_NUMBER_OF_REFRESHES(serverVars)
ENDFUNC

FUNC BOOL RAN_OUT_OF_REFRESHES(SERVER_NEXT_JOB_VARS& serverVars) 
	RETURN GET_NUMBER_OF_REFRESHES(serverVars) - GET_NUMBER_OF_TIMES_REFRESHED(serverVars) <= 0
ENDFUNC

FUNC INT GET_NUMBER_OF_JOBS_GRABBED(SERVER_NEXT_JOB_VARS& serverVars)
	RETURN serverVars.iNumberCompatable
ENDFUNC 

//Set what types of missions the player is allowed to play
PROC SET_MISSION_TYPES_UNLOCKS(SERVER_NEXT_JOB_VARS& serverVars)
	PARTICIPANT_INDEX temppart
	PLAYER_INDEX playerId 
	INT iparticipant
	//These are always unlocked
	SET_BIT(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_MISSION)
	SET_BIT(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_DEATHMATCH)
	SET_BIT(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_RACE)
	PRINTLN(" [CS_JOB] SET_MISSION_TYPES_UNLOCKS - FMMC_TYPE_MISSION")
	PRINTLN(" [CS_JOB] SET_MISSION_TYPES_UNLOCKS - FMMC_TYPE_DEATHMATCH")
	PRINTLN(" [CS_JOB] SET_MISSION_TYPES_UNLOCKS - FMMC_TYPE_RACE")	
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		temppart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
			playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF NOT IS_PLAYER_SCTV(playerId)
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_UNLOCKED_PARA)
					SET_BIT(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_BASE_JUMP)
				ENDIF
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_UNLOCKED_SURVIVAL)
					SET_BIT(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_SURVIVAL)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_BASE_JUMP)
		PRINTLN(" [CS_JOB] SET_MISSION_TYPES_UNLOCKS - FMMC_TYPE_BASE_JUMP")
	ENDIF
	IF IS_BIT_SET(serverVars.iMatchTypeUnlockBitSet, FMMC_TYPE_SURVIVAL)
		PRINTLN(" [CS_JOB] SET_MISSION_TYPES_UNLOCKS - FMMC_TYPE_SURVIVAL")
	ENDIF
	#ENDIF
ENDPROC

FUNC BOOL SHOULD_THIS_JOB_BE_LTS(SERVER_NEXT_JOB_VARS& serverVars, MISSION_TYPE_COUNT &sTypeCounts)
	IF sTypeCounts.iNumLtsFound < ciNUMBER_OF_LTS_TO_GRAB
		IF serverVars.iFound = sTypeCounts.iMissionThatIsLTS[sTypeCounts.iNumLtsFound]	
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_THIS_JOB_BE_NEW_VS(SERVER_NEXT_JOB_VARS& serverVars, MISSION_TYPE_COUNT &sTypeCounts)
	IF sTypeCounts.iNumNewVsFound < ciNUMBER_OF_NEW_VS_TO_GRAB
		IF serverVars.iFound = sTypeCounts.iMissionThatIsNewVs[sTypeCounts.iNumNewVsFound]	
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_THIS_JOB_BE_THE_TYPE_IVE_JUST_PLAYED(SERVER_NEXT_JOB_VARS& serverVars, MISSION_TYPE_COUNT &sTypeCounts)
	IF g_sMPTunables.bDisablePlayStyle
	OR sTypeCounts.iNumSameAsLastToFind = 0
		RETURN FALSE
	ELIF serverVars.iFound < sTypeCounts.iNumSameAsLastToFind
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//Get a safe LTS mission to grab
FUNC INT  GET_SAFE_LTS(INT iRange)	
	INT iRandMission, iStuck
	BOOL bNotFound = TRUE
	WHILE bNotFound
	AND iStuck < 10
		bNotFound = FALSE
		iRandMission = NETWORK_GET_RANDOM_INT_RANGED(iRange, iRange + NUM_JOBS_ON_ONE_PAGE)
//		FOR iLoop = 0 TO (ciNUMBER_OF_LTS_TO_GRAB - 1)
//			IF iRandMission = sTypeCounts.iMissionThatIsSamsAsLast[iLoop]
//				PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, iRandMission = sTypeCounts.iMissionThatIsLTS[iInsideLoop] = iTypeCounts.iMissionThatIsLTS[", iLoop, "] = ", iRandMission)
//				bNotFound = FALSE
				iStuck++
//				iRandMission = -1
//			ENDIF
//		ENDFOR
	ENDWHILE	
	RETURN iRandMission	
ENDFUNC

FUNC BOOL IS_FIRST_JOB_RANDOM(SERVER_NEXT_JOB_VARS& serverVars)
	RETURN IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_FIRST_JOB_IS_RANDOM)
ENDFUNC

CONST_INT VOTE_SERVER_INIT 		0
CONST_INT VOTE_SERVER_PROCESS 	1
CONST_INT VOTE_SERVER_RESET		2

FUNC INT GET_JOB_SERVER_STAGE(SERVER_NEXT_JOB_VARS& serverVars)
	RETURN serverVars.iServerLogicProgress 
ENDFUNC

FUNC BOOL SHOULD_ALTERNATE_IMAGES(SERVER_NEXT_JOB_VARS& serverVars)
	IF IS_FIRST_JOB_RANDOM(serverVars)
		IF NOT ON_SECOND_PAGE_REFRESHED(serverVars)
			IF NOT ON_LAST_PAGE_REFRESHED(serverVars)
				IF GET_JOB_SERVER_STAGE(serverVars) <> VOTE_SERVER_RESET

					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Get a safe verified mission to grab
FUNC INT GET_SAFE_VERIFIED(MISSION_TYPE_COUNT &sTypeCounts, INT iRange)	
	INT iRandMission, iLoop, iStuck
	BOOL bNotFound = TRUE
	WHILE bNotFound
	AND iStuck < 10
		bNotFound = FALSE
		iRandMission = NETWORK_GET_RANDOM_INT_RANGED(iRange, iRange + NUM_JOBS_ON_ONE_PAGE)
		FOR iLoop = 0 TO (ciNUMBER_OF_LTS_TO_GRAB - 1)
			//IF iRandMission = sTypeCounts.iMissionThatIsSamsAsLast[iLoop]
			IF iRandMission = sTypeCounts.iMissionThatIsLTS[iLoop]
				PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, iRandMission = sTypeCounts.iMissionThatIsLTS[iInsideLoop] = iTypeCounts.iMissionThatIsLTS[", iLoop, "] = ", iRandMission)
				bNotFound = FALSE
				iStuck++
				iRandMission = -1
			ENDIF
		ENDFOR
	ENDWHILE	
	RETURN iRandMission	
ENDFUNC

PROC SET_MISSION_TYPE_BIT_SET(INT &iMissionTypeBitSet)
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_ComeOutToPlay)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_ComeOutToPlay) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_ComeOutToPlay)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_CrossTheLine)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_CrossTheLine) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_CrossTheLine)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HuntingPack)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HuntingPack) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HuntingPack)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_HUNTDARK)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_HUNTDARK) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_HUNTDARK)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EBC)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EBC) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EBC)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUNNINGBACK)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUNNINGBACK) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUNNINGBACK)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DROP_ZONE)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DROP_ZONE) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DROP_ZONE)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BVS1)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BVS1) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BVS1)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EXTRACTION)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EXTRACTION) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EXTRACTION)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BE_MY_VALENTIN)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BE_MY_VALENTIN) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BE_MY_VALENTIN)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SUMO)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SUMO) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SUMO)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAVID_AND_GOLIATH)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAVID_AND_GOLIATH) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAVID_AND_GOLIATH)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUGBY)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUGBY) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUGBY)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_IN_AND_OUT)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_IN_AND_OUT) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_IN_AND_OUT)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_POWER_PLAY) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_POWER_PLAY)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAWN_RAID) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAWN_RAID)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DIRTY_SANCHEZ(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DIRTY_SANCHEZ) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DIRTY_SANCHEZ)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RAGE_AGAINST_THE_CAMPERS(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RATG) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RATG)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GRASS_GREENER(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_GRASS_GREENER) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_MISSION_GRASS_GREENER)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_ZONE(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_SUMO_ZONE) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_SUMO_ZONE)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_GUNSMITH) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_GUNSMITH)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_TOUR_DE_FORCE) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_TOUR_DE_FORCE)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ENTOURAGE(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_ENTOURAGE) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_ENTOURAGE)	
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_LOST_DAMNED) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_LOST_DAMNED)	
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_DONT_CROSS_THE_LINE) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_DONT_CROSS_THE_LINE)	
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_JUGGERNAUT) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_JUGGERNAUT)	
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_TURF_WAR) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_TURF_WAR)	
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POINTLESS(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_POINTLESS) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_POINTLESS)	
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_VEHICLE_VENDETTA) ")
		SET_BIT(iMissionTypeBitSet, ciROCKSTAR_CREATED_2_NEW_VS_VEHICLE_VENDETTA)	
	ENDIF
ENDPROC

CONST_INT ciCOMPATABLE_JOBS_ROCKSTAR_CREATED			0
CONST_INT ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED			1
CONST_INT ciCOMPATABLE_JOBS_ROCKSTAR_CREATED_LTS		2
CONST_INT ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED_LTS		3
CONST_INT ciCOMPATABLE_JOBS_SAME_AS_LAST				4
CONST_INT ciCOMPATABLE_JOBS_ROCKSTAR_CREATED_TEAM		5
CONST_INT ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED_TEAM		6
CONST_INT ciCOMPATABLE_JOBS_DONE						7

FUNC BOOL SERVER_COUNT_COMPATABLE_JOBS(SERVER_NEXT_JOB_VARS& serverVars, MISSION_TYPE_COUNT &sTypeCounts, BOOL bAdversaryFlow, BOOL bAssassination)
	SWITCH serverVars.iNumberCompatableStage
		
		//Count the number of created and verified jobs
		CASE ciCOMPATABLE_JOBS_ROCKSTAR_CREATED
			serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iTotalNumberCompatable 		= COUNT_NUMBER_OF_MISSIONS_THAT_ARE_COMPATABLE(serverVars, sTypeCounts, serverVars.iMatchTypeUnlockBitSet, serverVars.iNumberOnJob, serverVars.iAverageRank, DEFAULT, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, bAdversaryFlow, bAssassination)
			serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED
			PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED")
		BREAK
		CASE ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED
			serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iTotalNumberCompatable	 	= COUNT_NUMBER_OF_MISSIONS_THAT_ARE_COMPATABLE(serverVars, sTypeCounts, serverVars.iMatchTypeUnlockBitSet, serverVars.iNumberOnJob, serverVars.iAverageRank, DEFAULT, DEFAULT, TRUE, FALSE, DEFAULT, DEFAULT, bAdversaryFlow, bAssassination)
			serverVars.iNumberCompatable = serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iTotalNumberCompatable + serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iTotalNumberCompatable
			serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_ROCKSTAR_CREATED_LTS
			PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_ROCKSTAR_CREATED_LTS")
		BREAK
		
		//Get the number of LTSs
		CASE ciCOMPATABLE_JOBS_ROCKSTAR_CREATED_LTS
			serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberLTSCompatable 			= COUNT_NUMBER_OF_MISSIONS_THAT_ARE_COMPATABLE(serverVars, sTypeCounts, serverVars.iMatchTypeUnlockBitSet, serverVars.iNumberOnJob, serverVars.iAverageRank, FALSE, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bAdversaryFlow, bAssassination)
			serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED_LTS
			PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED_LTS")
		BREAK
		CASE ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED_LTS
			serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iNumberLTSCompatable 			= COUNT_NUMBER_OF_MISSIONS_THAT_ARE_COMPATABLE(serverVars, sTypeCounts, serverVars.iMatchTypeUnlockBitSet, serverVars.iNumberOnJob, serverVars.iAverageRank, FALSE, TRUE, FALSE, FALSE, DEFAULT, DEFAULT, bAdversaryFlow, bAssassination)
			serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_SAME_AS_LAST
			PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_SAME_AS_LAST")
		BREAK
		
		//Count the number that are the same as the last mission that we played
		CASE ciCOMPATABLE_JOBS_SAME_AS_LAST
			serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfSameAsLastFound 		= COUNT_NUMBER_OF_MISSIONS_THAT_ARE_COMPATABLE(serverVars, sTypeCounts, serverVars.iMatchTypeUnlockBitSet, serverVars.iNumberOnJob, serverVars.iAverageRank, FALSE, FALSE, DEFAULT, DEFAULT, TRUE, DEFAULT, bAdversaryFlow, bAssassination)
			serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_ROCKSTAR_CREATED_TEAM
			PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_ROCKSTAR_CREATED_TEAM")
		BREAK
		
		//Get the number of team missions found
		CASE ciCOMPATABLE_JOBS_ROCKSTAR_CREATED_TEAM
			serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfTeamMissionsFound 	= COUNT_NUMBER_OF_MISSIONS_THAT_ARE_COMPATABLE(serverVars, sTypeCounts, serverVars.iMatchTypeUnlockBitSet, serverVars.iNumberOnJob, serverVars.iAverageRank, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bAdversaryFlow, bAssassination)
			serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED_TEAM
			PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED_TEAM")
		BREAK
		CASE ciCOMPATABLE_JOBS_ROCKSTAR_VERIFIED_TEAM
			serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iNumberOfTeamMissionsFound 	= COUNT_NUMBER_OF_MISSIONS_THAT_ARE_COMPATABLE(serverVars, sTypeCounts, serverVars.iMatchTypeUnlockBitSet, serverVars.iNumberOnJob, serverVars.iAverageRank, TRUE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, bAdversaryFlow, bAssassination)
			serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_DONE
			PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_DONE")
			RETURN TRUE
			
		//Done
		CASE ciCOMPATABLE_JOBS_DONE
			RETURN TRUE
			
	ENDSWITCH
	
	RETURN FALSE		
ENDFUNC


FUNC BOOL SERVER_ONLY_GRABBED_RANDOM_JOBS(SERVER_NEXT_JOB_VARS& serverVars, MISSION_TYPE_COUNT &sTypeCounts)//, NEXT_JOB_STRUCT& nextJobStruct)
	INT iReturnArrayPos	
	INT iJobType
	INT iLoop
	INT iRandMission
	INT iRange
	INT iBitSet, iBitSet2, iAdversaryModeType
	BOOL bRockStarCreated
	INT iMissionTypeBitSet
	INT iNumPagesOnNJVS
	TEXT_LABEL_23 tl23ContentID
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 tl63Debug
	#ENDIF
	
	//set up the posix time for the seed
	IF g_iCurrentPosixforNJVS = 0
		//get the current posix time
		g_iCurrentPosixforNJVS = GET_CLOUD_TIME_AS_INT()
		PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, g_iCurrentPosixforNJVS = GET_CLOUD_TIME_AS_INT()           = ", g_iCurrentPosixforNJVS)
		//devide it by two minutes
		g_iCurrentPosixforNJVS /= g_sMPTunables.iNjvsSyncWindow
		PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, g_iCurrentPosixforNJVS = ", GET_CLOUD_TIME_AS_INT(), "/", g_sMPTunables.iNjvsSyncWindow, " = ", g_iCurrentPosixforNJVS)	//Seed the random int!
		NETWORK_SEED_RANDOM_NUMBER_GENERATOR(g_iCurrentPosixforNJVS)
		PRINTLN(" [BW][CS_JOB] , NETWORK_SEED_RANDOM_NUMBER_GENERATOR(", g_iCurrentPosixforNJVS, ")")
	ENDIF	
	
	SWITCH serverVars.iRandomProgress

		CASE RANDOM_STAGE_INIT
			#IF IS_DEBUG_BUILD 
			g_bPrintMissionNames = FALSE
			#ENDIF
			#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_ForceNewVs"))
				g_bPrintMissionNames = TRUE
			ENDIF
			#ENDIF	
			//Check to see if we should force the return to freemode
//			#IF IS_DEBUG_BUILD
//			IF SHOULD_SKIP_END_OF_JOB_VOTE_SCREEN_BECAUSE_OF_Z_MENU_LAUNCH()
//				PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_INIT, SHOULD_SKIP_END_OF_JOB_VOTE_SCREEN_BECAUSE_OF_Z_MENU_LAUNCH = TRUE ")
//				serverVars.bForceVoteForFreemode = TRUE
//			ENDIF
//			#ENDIF			
			INITIALISE_SERVER_VARS(serverVars)
			PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_INIT, serverVars.iRandomProgress = RANDOM_STAGE_GRAB ")
			
			SET_MISSION_TYPES_UNLOCKS(serverVars)
			sTypeCounts.iNumNewVsFound = 0
			
			
			BOOL bBunker
			bBunker = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_BUNKER_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
				SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)
				PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
			ENDIF
			
			IF IS_CURRENT_MISSION_UGC_NON_ROCKSTAR()
			AND IS_ARENA_WARS_JOB()
			AND CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_ARENA_SERIES) != -1
				PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CORONA_FLOW_V2)")
				SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ARENA_SERIES)
				SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
			ENDIF
			
			//If it's the new corona flow then set that it is
			IF g_sMPTunables.bFmCoronaPlaylistActive
			AND CV2_IS_THIS_JOB_A_CORONA_FLOW_JOB()
			AND CV2_GET_V2_CORONA_FLOW_LENGTH() >= ciNUMBER_OF_SAME_TO_GRAB
				PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CORONA_FLOW_V2)")
				SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CORONA_FLOW_V2)
			ELIF IS_THIS_ROOT_CONTENT_ID_AN_ASSASSINATION_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
			AND gAssassinationMissionData.iActiveMissionArrayPos != -1
				SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION)
				PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION)")
			ELIF CV2_GET_ADVERSARY_SERIES_ARRAY_POS(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) != -1
				IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_TARGET_ASSAULT_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_TARGET_SERIES)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_TARGET_SERIES)
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
					PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HOTRING_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HOTRING_SERIES)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HOTRING_SERIES)
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
					PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
				ELIF (CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash) OR IS_ARENA_WARS_JOB(TRUE))
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ARENA_SERIES)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ARENA_SERIES)
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
					PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
				
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_RACE_SERIES)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_RACE_SERIES)
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
					PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_SURVIVAL_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_SURVIVAL_SERIES)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_SURVIVAL_SERIES)
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
					PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
				
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_OPEN_WHEEL_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_OPEN_WHEEL_SERIES)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_OPEN_WHEEL_SERIES)
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
					PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
					
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_STREET_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_STREET_RACE_SERIES)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_STREET_RACE_SERIES)
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
					PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
					
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_PURSUIT_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_PURSUIT_SERIES)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_PURSUIT_SERIES)
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
					PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
					
				#IF FEATURE_GEN9_EXCLUSIVE
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HSW_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HSW_RACE_SERIES)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HSW_RACE_SERIES)
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
					PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
				#ENDIF
				
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_COMMUNITY_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_COMMUNITY_SERIES)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_COMMUNITY_SERIES)
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
					PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
					
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CAYO_PERICO_SERIES)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CAYO_PERICO_SERIES)
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT) 
					PRINTLN(" [CS_JOB] SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IGNORE_MIN_PLAYER_COUNT)")
				
				ELIF (CV2_IS_THIS_JOB_A_CORONA_FLOW_ADVERSARY_SERIES_JOB(TRUE) OR CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_FEATURED_ADVERSARY_MODE_JOB(g_FMMC_STRUCT.iRootContentIDHash) OR bBunker)
				AND CV2_GET_V2_ADVERSARY_SERIES_LENGTH(CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)) >= ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ADVERSARY_SERIES_V2)")
					SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ADVERSARY_SERIES_V2)
					IF bBunker
						SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_BUNKER_SERIES_V2)
						PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_BUNKER_SERIES_V2)")
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_CONTENT_KING_OF_THE_HILL(g_FMMC_STRUCT.iMissionType, g_FMMC_STRUCT.iMissionSubType)
				SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_IS_KOTH)
				PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS IS_CONTENT_KING_OF_THE_HILL")
			ENDIF
			
			//Clear the old types
			FOR iLoop = 0 TO (MAX_CLOUD_MISSION_TYPES - 1)
				sTypeCounts.iType[iLoop] = 0
			ENDFOR
			FOR iLoop = 0 TO (ciNUMBER_OF_VERIFIED_TYPES - 1)
				sTypeCounts.iTypeVerifiedType[iLoop] = 0
			ENDFOR
			FOR iLoop = 0 TO (ciNUMBER_OF_NEW_VS_TO_GRAB - 1)
				sTypeCounts.iMissionThatIsNewVs[iLoop] = 0
			ENDFOR
			FOR iLoop = 0 TO (ciNUMBER_OF_LTS_TO_GRAB - 1)
				sTypeCounts.iMissionThatIsLTS[iLoop] = -1
			ENDFOR
			sTypeCounts.iNumSameAsLastToFind = 0
			sTypeCounts.bUseRandom = TRUE
			CLEAR_ATTEMPTS_LIMIT(serverVars)
			sTypeCounts.bTypeCountSetUp = TRUE
			serverVars.iNumberCompatableStage = ciCOMPATABLE_JOBS_ROCKSTAR_CREATED
			serverVars.iRandomProgress = RANDOM_STAGE_COUNT
		BREAK
		
		CASE RANDOM_STAGE_COUNT
			//get the total number of Jobs that are compatable
			BOOL bAdversaryFlow
			BOOL bAssassination
			bAdversaryFlow= IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ADVERSARY_SERIES_V2)
			bAssassination= IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION)
			
			IF sTypeCounts.bTypeCountSetUp = FALSE
				serverVars.iRandomProgress = RANDOM_STAGE_INIT				
				PRINTLN(" [CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.bTypeCountSetUp = FALSE - serverVars.iRandomProgress = RANDOM_STAGE_INIT	")
			ENDIF
			
			IF NOT SERVER_COUNT_COMPATABLE_JOBS(serverVars, sTypeCounts, bAdversaryFlow, bAssassination)
				RETURN FALSE
			ENDIF

			//If it's the new corona flow then set that it is
			IF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CORONA_FLOW_V2)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ADVERSARY_SERIES_V2)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION)
				sTypeCounts.iNumSameAsLastToFind = ciNUMBER_OF_SAME_TO_GRAB
				PRINTLN(" [CS_JOB][2001613] SERVER_BOOL_IS_CORONA_FLOW_V2 OR SERVER_BOOL_IS_ADVERSARY_SERIES_V2 - sTypeCounts.iNumSameAsLastToFind = ", sTypeCounts.iNumSameAsLastToFind)
			//Only get the New VS if we are on one. 
			ELIF IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()
				PRINTLN(" [CS_JOB][2001613] IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()")
				SET_MISSION_TYPE_BIT_SET(iMissionTypeBitSet)
				serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound = COUNT_NUMBER_OF_MISSIONS_THAT_ARE_COMPATABLE(serverVars, sTypeCounts, serverVars.iMatchTypeUnlockBitSet, serverVars.iNumberOnJob, serverVars.iAverageRank, TRUE, FALSE, FALSE, TRUE, FALSE, iMissionTypeBitSet, bAdversaryFlow)
				//Clear the number that it same as last to find
				sTypeCounts.iNumSameAsLastToFind =  0
			ELIF g_sMPTunables.bDisablePlayStyle = FALSE
				//Set the number of mission that we should find that are the same
				sTypeCounts.iNumSameAsLastToFind = serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfSameAsLastFound
				PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind = serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfSameAsLastFound  = ", sTypeCounts.iNumSameAsLastToFind)
				//Cap it
				IF sTypeCounts.iNumSameAsLastToFind > ciNUMBER_OF_SAME_TO_GRAB
					sTypeCounts.iNumSameAsLastToFind = ciNUMBER_OF_SAME_TO_GRAB
					PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind > ciNUMBER_OF_SAME_TO_GRAB = ", sTypeCounts.iNumSameAsLastToFind)
				ENDIF
			ENDIF
			
			PRINTLN(" [CS_JOB][2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.iNumberCompatable  = ", serverVars.iNumberCompatable)
			PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iTotalNumberCompatable       = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iTotalNumberCompatable)
			PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberLTSCompatable         = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberLTSCompatable)
			PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfTeamMissionsFound   = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfTeamMissionsFound)
			PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfSameAsLastFound     = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfSameAsLastFound, " [SamsAsLast]")
			PRINTLN(" [CS_JOB] [NEWVS] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound)
			PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iTotalNumberCompatable      = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iTotalNumberCompatable)
			PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iNumberLTSCompatable        = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iNumberLTSCompatable)
			PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iNumberOfTeamMissionsFound  = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iNumberOfTeamMissionsFound)
			
			IF serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfTeamMissionsFound > NUM_OF_TEAM_JOBS_TO_GRAB
				serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfTeamMissionsFound = NUM_OF_TEAM_JOBS_TO_GRAB
				PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.iNumberOfTeamMissionsFound capped to NUM_OF_TEAM_JOBS_TO_GRAB = ", NUM_OF_TEAM_JOBS_TO_GRAB)
			ENDIF			
//			IF serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound > ciNUMBER_OF_NEW_VS_TO_GRAB
//				serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound = ciNUMBER_OF_NEW_VS_TO_GRAB
//				PRINTLN(" [CS_JOB] [NEWVS] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.iNumberOfNewVsFound capped to ciNUMBER_OF_NEW_VS_TO_GRAB = ", ciNUMBER_OF_NEW_VS_TO_GRAB)
//			ENDIF
			#IF IS_DEBUG_BUILD 
			FOR iLoop = 0 TO (MAX_CLOUD_MISSION_TYPES - 1)
				PRINTLN(" [CS_JOB] COUNT_NUMBER_OF_MISSIONS_THAT_ARE_COMPATABLE sTypeCounts.iType[", iLoop, "] = ", sTypeCounts.iType[iLoop])
			ENDFOR
			#ENDIF

			
			INT iLtsGrabbed
			
			//Pick the mission that will be New VS, only if we have some
			IF (iMissionTypeBitSet != 0 OR g_FMMC_STRUCT.iAdversaryModeType != 0)
			AND serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound != 0
				FOR iLoop = 0 TO (ciNUMBER_OF_NEW_VS_TO_GRAB - 1)
					IF iLoop < serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound 
						sTypeCounts.iMissionThatIsNewVs[iLoop] = iLoop
						PRINTLN(" [CS_JOB] [NEWVS] SERVER_ONLY_GRABBED_RANDOM_JOBS, sTypeCounts.iMissionThatIsNewVs[", iLoop, "] = ", sTypeCounts.iMissionThatIsNewVs[iLoop])
					ELSE
						sTypeCounts.iMissionThatIsNewVs[iLoop] = -1
						PRINTLN(" [CS_JOB] [NEWVS] SERVER_ONLY_GRABBED_RANDOM_JOBS, sTypeCounts.iMissionThatIsNewVs[", iLoop, "] = ", sTypeCounts.iMissionThatIsNewVs[iLoop])
						IF serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iNumberLTSCompatable >= iLoop
						AND iLtsGrabbed < ciNUMBER_OF_LTS_TO_GRAB
							sTypeCounts.iMissionThatIsLTS[iLtsGrabbed] = iLoop
							PRINTLN(" [CS_JOB] [NEWVS] SERVER_ONLY_GRABBED_RANDOM_JOBS, sTypeCounts.iMissionThatIsLTS[", iLtsGrabbed, "] = ", sTypeCounts.iMissionThatIsLTS[iLtsGrabbed])
							iLtsGrabbed++
						ENDIF
					ENDIF				 
				ENDFOR
			//Not looking for new VS, then clean up
			ELSE
				PRINTLN(" [CS_JOB] [NEWVS] SERVER_ONLY_GRABBED_RANDOM_JOBS, sTypeCounts.iMissionThatIsNewVs[iLoop] = -1[")
				FOR iLoop = 0 TO (ciNUMBER_OF_NEW_VS_TO_GRAB - 1)
					sTypeCounts.iMissionThatIsNewVs[iLoop] = -1
				ENDFOR
				serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound = 0
				PRINTLN(" [CS_JOB] [NEWVS] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iNumberOfNewVsFound = ", serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound)
			ENDIF				 
			
			//Pick the mission that will be LTS, start at one as we want none of these on the first page, these should be LTS
			FOR iLoop = 1 TO (ciNUMBER_OF_LTS_TO_GRAB - 1)
				IF serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfTeamMissionsFound >= iLoop 
					IF sTypeCounts.iMissionThatIsLTS[iLoop] = -1
						iRange = (NUM_JOBS_ON_ONE_PAGE * iLoop)
						sTypeCounts.iMissionThatIsLTS[iLoop] = GET_SAFE_LTS(iRange)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, sTypeCounts.iMissionThatIsLTS[", iLoop, "] != 0")
						#ENDIF
					ENDIF
				ELSE
					sTypeCounts.iMissionThatIsLTS[iLoop] = -1
				ENDIF				 
				PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, sTypeCounts.iMissionThatIsLTS[", iLoop, "] = ", sTypeCounts.iMissionThatIsLTS[iLoop])
			ENDFOR
			
			
			//Pick the missions that will rockstar verified
			//Clear old bit set
			serverVars.iVerifiedBitSet = 0
			//Set all to be verified
			IF IS_CURRENT_MISSION_ROCKSTAR_COMMUNITY()
				serverVars.iVerifiedBitSet = -1
				PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.iVerifiedBitSet = -1 - IS_CURRENT_MISSION_ROCKSTAR_COMMUNITY")
			//Loop and pick ones that should be verified if there is more than 0
			ELIF NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CORONA_FLOW_V2)
			AND NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ADVERSARY_SERIES_V2)
			AND NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION)
			AND NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ARENA_SERIES)
			AND NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_RACE_SERIES)
			AND NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_SURVIVAL_SERIES)
				IF serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iTotalNumberCompatable > 0
					FOR iLoop = 0 TO (ciNUMBER_OF_VERIFIED_TO_GRAB - 1)
						IF serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_VERIFIED].iTotalNumberCompatable > iLoop 
							iRange = (NUM_JOBS_ON_ONE_PAGE * iLoop)
							iRandMission = GET_SAFE_VERIFIED(sTypeCounts, iRange)
							IF iRandMission != -1
								SET_BIT(serverVars.iVerifiedBitSet, iRandMission)				
								PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, SET_BIT(serverVars.iVerifiedBitSet, ", iRandMission, ")")
							ENDIF	
						ENDIF	
					ENDFOR
				ENDIF
			ENDIF
			
			//Cap the limit
			IF serverVars.iNumberCompatable > NUM_JOBS_TO_GRAB
				serverVars.iNumberCompatable = NUM_JOBS_TO_GRAB
				PRINTLN(" [CS_JOB] [2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.iNumberCompatable capped to NUM_JOBS_TO_GRAB = ", NUM_JOBS_TO_GRAB)
			ELIF serverVars.iNumberCompatable > (NUM_JOBS_ON_ONE_PAGE + NUM_JOBS_ON_ONE_PAGE)
				serverVars.iNumberCompatable = NUM_JOBS_ON_ONE_PAGE + NUM_JOBS_ON_ONE_PAGE
				PRINTLN(" [CS_JOB] [2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.iNumberCompatable capped to NUM_JOBS_ON_ONE_PAGE + NUM_JOBS_ON_ONE_PAGE = ", NUM_JOBS_ON_ONE_PAGE + NUM_JOBS_ON_ONE_PAGE)
				//clear the use random flag
				IF iMissionTypeBitSet = 0 
				AND g_FMMC_STRUCT.iAdversaryModeType = 0
				AND !IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_KOTH)
					sTypeCounts.bUseRandom = FALSE
				ENDIF
			ELSE
				IF serverVars.iNumberCompatable > NUM_JOBS_ON_ONE_PAGE
					serverVars.iNumberCompatable = NUM_JOBS_ON_ONE_PAGE
					PRINTLN(" [CS_JOB] [2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.iNumberCompatable capped to NUM_JOBS_ON_ONE_PAGE = ", NUM_JOBS_ON_ONE_PAGE)
				ENDIF
				PRINTLN(" [CS_JOB] [2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.iNumberCompatable = ", serverVars.iNumberCompatable)
				//clear the use random flag
				IF iMissionTypeBitSet = 0 
				AND g_FMMC_STRUCT.iAdversaryModeType = 0
				AND !IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_KOTH)
					sTypeCounts.bUseRandom = FALSE
				ENDIF
			ENDIF
			iNumPagesOnNJVS = g_sMPTunables.iNumPagesOnNJVS
			IF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CORONA_FLOW_V2)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ADVERSARY_SERIES_V2)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_TARGET_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HOTRING_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ARENA_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_RACE_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_SURVIVAL_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_OPEN_WHEEL_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_STREET_RACE_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_PURSUIT_SERIES)
			#IF FEATURE_GEN9_EXCLUSIVE
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HSW_RACE_SERIES)
			#ENDIF
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_COMMUNITY_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CAYO_PERICO_SERIES)
				iNumPagesOnNJVS = 1
				PRINTLN("[CS_JOB][CV2] SERVER_ONLY_GRABBED_RANDOM_JOBS - iNumPagesOnNJVS = ", iNumPagesOnNJVS)
			ENDIF
			//Cap based on a tunable for the number of pages
			IF serverVars.iNumberCompatable > (iNumPagesOnNJVS * NUM_JOBS_ON_ONE_PAGE)
				serverVars.iNumberCompatable = (iNumPagesOnNJVS * NUM_JOBS_ON_ONE_PAGE)
				PRINTLN(" [CS_JOB] [2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.iNumberCompatable capped to (g_sMPTunables.iNumPagesOnNJVS * NUM_JOBS_ON_ONE_PAGE) = ", serverVars.iNumberCompatable)
			ENDIF
			
			PRINTLN(" [CS_JOB] [2001613] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.iNumberCompatable, serverVars.iNumberCompatable = ", serverVars.iNumberCompatable)
			serverVars.iRandomProgress = RANDOM_STAGE_GRAB
		BREAK
	
		CASE RANDOM_STAGE_GRAB
			IF serverVars.iFound >= GET_NUMBER_OF_JOBS_GRABBED(serverVars)
				PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, serverVars.iRandomProgress = RANDOM_STAGE_END")
			
				SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_GRABBED_JOBS)
			
				serverVars.iRandomProgress = RANDOM_STAGE_END
			ELSE
				IF serverVars.iStoredRandom[serverVars.iFound] = -1
					//If we are only using safe missions then clear that this is a verified one
					IF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_GET_SAFE_JOBS)
					AND NOT IS_CURRENT_MISSION_ROCKSTAR_COMMUNITY()
						IF IS_BIT_SET(serverVars.iVerifiedBitSet, serverVars.iFound)	
							CLEAR_BIT(serverVars.iVerifiedBitSet, serverVars.iFound)
						ENDIF
					ENDIF
					BOOL bCoronaFlowPlay
					bRockStarCreated = !IS_BIT_SET(serverVars.iVerifiedBitSet, serverVars.iFound)	
					IF sTypeCounts.bUseRandom	
						IF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION)
							IF serverVars.iFound = 0
								PRINTLN(" [CS_JOB] [NJVSQMR] SERVER_BOOL_IS_ASSASSINATION_MISSION g_sMPTunables.bDisableNjvsRandom = FALSE - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_FIRST_JOB_IS_RANDOM)")
								//Set the flag to say it's random
								SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_FIRST_JOB_IS_RANDOM)
								IF IS_BIT_SET(serverVars.iVerifiedBitSet, serverVars.iFound)	
									CLEAR_BIT(serverVars.iVerifiedBitSet, serverVars.iFound)
									PRINTLN(" [CS_JOB] [SamsAsLast] SERVER_BOOL_IS_ASSASSINATION_MISSION, SERVER_BOOL_FIRST_JOB_IS_RANDOM, CLEAR_BIT(serverVars.iVerifiedBitSet, ", serverVars.iFound, ")")
								ENDIF
								//incrament
								serverVars.iFound++
								PRINTLN(" [CS_JOB] [NJVSQMR] SERVER_BOOL_IS_ASSASSINATION_MISSION serverVars.iFound++ - Then exit")
								RETURN FALSE
							ENDIF
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, FALSE, FALSE, FALSE, FALSE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_ASSASSINATION_MISSION - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_ASSASSINATION_MISSION")
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CORONA_FLOW_V2)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE) 
							bCoronaFlowPlay = TRUE
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_CORONA_FLOW_V2 - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_CORONA_FLOW_V2")
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ADVERSARY_SERIES_V2)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_ADVERSARY_SERIES_V2 - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_ADVERSARY_SERIES_V2")
							bCoronaFlowPlay = TRUE
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_TARGET_SERIES)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, FALSE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_TARGET_SERIES - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_TARGET_SERIES")
							bCoronaFlowPlay = TRUE
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HOTRING_SERIES)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, FALSE, FALSE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_HOTRING_SERIES - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_HOTRING_SERIES")
							bCoronaFlowPlay = TRUE
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ARENA_SERIES)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, FALSE, FALSE, FALSE, FALSE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_ARENA_SERIES - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_ARENA_SERIES")
							bCoronaFlowPlay = TRUE
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_RACE_SERIES)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_RACE_SERIES - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_RACE_SERIES")
							bCoronaFlowPlay = TRUE
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_SURVIVAL_SERIES)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_SURVIVAL_SERIES - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_SURVIVAL_SERIES")
							bCoronaFlowPlay = TRUE
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_OPEN_WHEEL_SERIES)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_OPEN_WHEEL_SERIES - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_OPEN_WHEEL_SERIES")
							bCoronaFlowPlay = TRUE
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_STREET_RACE_SERIES)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_STREET_RACE_SERIES - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_STREET_RACE_SERIES")
							bCoronaFlowPlay = TRUE
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_PURSUIT_SERIES)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_PURSUIT_SERIES - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_PURSUIT_SERIES")
							bCoronaFlowPlay = TRUE
							
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_COMMUNITY_SERIES)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_HSW_RACE_SERIES - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_HSW_RACE_SERIES")
							bCoronaFlowPlay = TRUE
							
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CAYO_PERICO_SERIES)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_CAYO_PERICO_SERIES - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_HSW_RACE_SERIES")
							bCoronaFlowPlay = TRUE
							
						#IF FEATURE_GEN9_EXCLUSIVE
						ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HSW_RACE_SERIES)
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, 0, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE #IF FEATURE_GEN9_EXCLUSIVE , TRUE #ENDIF ) 
							PRINTLN("[CS_JOB][CV2] GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH - SERVER_BOOL_IS_HSW_RACE_SERIES - iReturnArrayPos = ", iReturnArrayPos, " - SERVER_BOOL_IS_HSW_RACE_SERIES")
							bCoronaFlowPlay = TRUE
						#ENDIF
							
						ELIF SHOULD_THIS_JOB_BE_NEW_VS(serverVars, sTypeCounts)
						AND serverVars.sCounts[ciEOJV_TYPE_ROCKSTAR_CREATED].iNumberOfNewVsFound != 0
							PRINTLN(" [CS_JOB] [NEWVS] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_GRAB, iJobType                 = FORCED NEW VS")
							SET_MISSION_TYPE_BIT_SET(iMissionTypeBitSet)
							bRockStarCreated = TRUE
							iReturnArrayPos = GET_ONLY_NEW_VS_COMPATABLE_JOBS_BASED_ON_HASH(serverVars, iMissionTypeBitSet)
						//If the first job should be random, then set it
						ELIF NOT g_sMPTunables.bDisableNjvsRandom
						AND serverVars.iFound = 0
							PRINTLN(" [CS_JOB] [NJVSQMR] SERVER_ONLY_GRABBED_RANDOM_JOBS g_sMPTunables.bDisableNjvsRandom = FALSE - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_FIRST_JOB_IS_RANDOM)")
							//Set the flag to say it's random
							SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_FIRST_JOB_IS_RANDOM)
							IF IS_BIT_SET(serverVars.iVerifiedBitSet, serverVars.iFound)	
								CLEAR_BIT(serverVars.iVerifiedBitSet, serverVars.iFound)
								PRINTLN(" [CS_JOB] [SamsAsLast] SERVER_ONLY_GRABBED_RANDOM_JOBS, SERVER_BOOL_FIRST_JOB_IS_RANDOM, CLEAR_BIT(serverVars.iVerifiedBitSet, ", serverVars.iFound, ")")
							ENDIF
							//incrament
							serverVars.iFound++
							PRINTLN(" [CS_JOB] [NJVSQMR] SERVER_ONLY_GRABBED_RANDOM_JOBS serverVars.iFound++ - Then exit")
							RETURN FALSE
						//If we want this mission to be specifically LTS
						ELIF SHOULD_THIS_JOB_BE_LTS(serverVars, sTypeCounts)						
							GET_NEXT_JOB_TO_LOAD(serverVars, sTypeCounts, FMMC_TYPE_MISSION, iReturnArrayPos, serverVars.iNumberOnJob, serverVars.iAverageRank, TRUE, TRUE)
							PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_GRAB, iJobType                 = FORCED LTS")
						//If we want this one to be same as the last
						ELIF SHOULD_THIS_JOB_BE_THE_TYPE_IVE_JUST_PLAYED(serverVars, sTypeCounts)
							IF IS_BIT_SET(serverVars.iVerifiedBitSet, serverVars.iFound)	
								CLEAR_BIT(serverVars.iVerifiedBitSet, serverVars.iFound)
								PRINTLN(" [CS_JOB] [SamsAsLast] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_GRAB, CLEAR_BIT(serverVars.iVerifiedBitSet, ", serverVars.iFound, ")")
							ENDIF
							bRockStarCreated = TRUE
							PRINTLN(" [CS_JOB] [SamsAsLast] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_GRAB, iJobType    = FORCED TYPE PLAYED - serverVars.iFound = ", serverVars.iFound)
							GET_NEXT_JOB_TO_LOAD(serverVars, sTypeCounts, g_FMMC_STRUCT.iMissionType, iReturnArrayPos, serverVars.iNumberOnJob, serverVars.iAverageRank, FALSE, TRUE, TRUE)
						//This is just any random mission
						ELSE
							iJobType = GET_JOB_TYPE_TO_GRAB(serverVars, bRockStarCreated)
							GET_NEXT_JOB_TO_LOAD(serverVars, sTypeCounts, iJobType, iReturnArrayPos, serverVars.iNumberOnJob, serverVars.iAverageRank, FALSE, bRockStarCreated)
						ENDIF
						PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_GRAB, bRockStarCreated             = ", bRockStarCreated)
						PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_GRAB, iJobType                     = ", iJobType)
						PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_GRAB  iReturnArrayPos              = ", iReturnArrayPos)
						PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_GRAB, iNumberOnJob                 = ", serverVars.iNumberOnJob)
						PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_GRAB, iAverageRank                 = ", serverVars.iAverageRank)
					ELSE
						iReturnArrayPos = GET_ONLY_COMPATABLE_JOBS(	serverVars, sTypeCounts, serverVars.iNumberOnJob, serverVars.iAverageRank, 
																	FALSE, FALSE, sTypeCounts.bUseRandomKing, bRockStarCreated, 
																	IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ADVERSARY_SERIES_V2), 
																	IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION))
						PRINTLN(" [CS_JOB] GET_ONLY_COMPATABLE_JOBS iReturnArrayPos = ", iReturnArrayPos)
					ENDIF
					
					serverVars.iAttempts++
					PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_GRAB, serverVars.iAttempts         = ", serverVars.iAttempts)
						
					// Backup stops it getting stuck
					IF serverVars.iAttempts >= ATTEMPTS_LIMIT_FAIL_SAFE_KING
					#IF IS_DEBUG_BUILD
					OR g_bForceJobFailSafeKing
					#ENDIF
						IF NOT sTypeCounts.bUseRandomKing
							PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS serverVars.iAttempts >= ATTEMPTS_LIMIT_FAIL_SAFE_KING = ", ATTEMPTS_LIMIT_FAIL_SAFE_KING)
							sTypeCounts.bUseRandomKing = TRUE
						ENDIF
					ELIF serverVars.iAttempts >= ATTEMPTS_LIMIT_FAIL_SAFE
					#IF IS_DEBUG_BUILD
					OR g_bForceJobFailSafe
					#ENDIF
						IF sTypeCounts.bUseRandom
							PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS serverVars.iAttempts >= ATTEMPTS_LIMIT_FAIL_SAFE = ", ATTEMPTS_LIMIT_FAIL_SAFE)
							sTypeCounts.bUseRandom = FALSE
						ENDIF
					ELIF serverVars.iAttempts >= ATTEMPTS_LIMIT
						IF NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_GET_SAFE_JOBS)
							CLEAR_JOB_TYPES_FOUND(serverVars)
							CLEAR_ATTEMPTS_LIMIT(serverVars)
							PRINTLN(" [CS_JOB] SERVER_BOOL_GET_SAFE_JOBS ")
							SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_GET_SAFE_JOBS)
						ENDIF
					ENDIF
					
					//If we've got an array position
					IF iReturnArrayPos <> -1
					
						//get the label so we can check it's not 
						tl23ContentID = GET_EOJV_MISSION_CONTENT_ID(iReturnArrayPos, !bRockStarCreated)
												
						//If the string's not null or empty
						IF NOT IS_STRING_NULL_OR_EMPTY(tl23ContentID)	
							
							//Tell the world!
							#IF IS_DEBUG_BUILD
							tl63Debug = GET_EOJV_MISSION_NAME(iReturnArrayPos, !bRockStarCreated)
							PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, RANDOM_STAGE_GRAB, tlFileName for iFound        = ", serverVars.iFound, "  tlFileName = ", tl23ContentID, " Name = ", tl63Debug)
							#ENDIF
							
							//If it's a new job
							IF IS_NEW_RANDOM_JOB(serverVars, iReturnArrayPos, !bRockStarCreated, bCoronaFlowPlay)
								IF SHOULD_THIS_JOB_BE_NEW_VS(serverVars, sTypeCounts)
									iBitSet = GET_EOJV_MISSION_BIT_SET(iReturnArrayPos, !bRockStarCreated)
									iBitSet2 = GET_EOJV_MISSION_BIT_SET_TWO(iReturnArrayPos, !bRockStarCreated)
									iAdversaryModeType = GET_EOJV_MISSION_iAdversaryModeType(iReturnArrayPos, !bRockStarCreated)
									IF IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
									OR IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista)
									OR IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_ComeOutToPlay)
									OR IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HuntingPack)
									OR IS_BIT_SET(iBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_CrossTheLine)
									OR iBitSet2 != 0
									OR iAdversaryModeType != 0
										sTypeCounts.iNumNewVsFound++
										PRINTLN(" [CS_JOB] [NEWVS] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumNewVsFound = ", sTypeCounts.iNumNewVsFound)
									ENDIF
								//Incrament the num of LTS found
								ELIF sTypeCounts.iNumLtsFound < ciNUMBER_OF_LTS_TO_GRAB
									iBitSet = GET_EOJV_MISSION_BIT_SET(iReturnArrayPos, !bRockStarCreated)
									IF (serverVars.iFound = sTypeCounts.iMissionThatIsLTS[sTypeCounts.iNumLtsFound]	
									OR IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_LTS)
									OR IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS)
									OR IS_BIT_SET(iBitSet, ciMISSION_OPTION_BS_CTF))
										sTypeCounts.iNumLtsFound++
										PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumLtsFound = ", sTypeCounts.iNumLtsFound)
									ENDIF
								ENDIF
								
								//Print if same as last found
								#IF IS_DEBUG_BUILD
								IF g_FMMC_STRUCT.iMissionType = GET_EOJV_MISSION_TYPE(iReturnArrayPos, !bRockStarCreated) 
									PRINTLN(" [CS_JOB] [SamsAsLast] SERVER_ONLY_GRABBED_RANDOM_JOBS sTypeCounts.iNumSameAsLastToFind = ", sTypeCounts.iNumSameAsLastToFind)
								ENDIF
								#ENDIF
								//Set the file name to server broadcast data
								serverVars.tlFileName[serverVars.iFound] = GET_EOJV_MISSION_CONTENT_ID(iReturnArrayPos, !bRockStarCreated) 
								IF g_sMPTunables.bStuntSeriesSkipNJVS
								AND NOT IS_BIT_SET(serverVars.iVerifiedBitSet, serverVars.iFound)
									INT iRootContentIDHash
									IF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION)
										iRootContentIDHash = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iReturnArrayPos].iRootContentIdHash
										IF g_FMMC_STRUCT.iRootContentIDHash != iRootContentIDHash
											serverVars.iJobVoteWinner = serverVars.iFound
											SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_GRABBED_JOBS)
											serverVars.iRandomProgress = RANDOM_STAGE_END
										ENDIF
									ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CORONA_FLOW_V2)
										iRootContentIDHash = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iReturnArrayPos].iRootContentIdHash
										IF g_FMMC_STRUCT.iRootContentIDHash != iRootContentIDHash
										AND CV2_IS_THIS_ROOT_CONTENT_ID_A_CORONA_FLOW_JOB(iRootContentIDHash)
											serverVars.iJobVoteWinner = serverVars.iFound
											SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_GRABBED_JOBS)
											serverVars.iRandomProgress = RANDOM_STAGE_END
										ENDIF
									ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ADVERSARY_SERIES_V2)
										iRootContentIDHash = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iReturnArrayPos].iRootContentIdHash
										IF g_FMMC_STRUCT.iRootContentIDHash != iRootContentIDHash
										AND CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ADVERSARY_SERIES_JOB(iRootContentIDHash)
											serverVars.iJobVoteWinner = serverVars.iFound
											SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_GRABBED_JOBS)
											serverVars.iRandomProgress = RANDOM_STAGE_END
										ENDIF
									ELIF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_TARGET_SERIES)
									OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HOTRING_SERIES)
									OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ARENA_SERIES)
									OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_RACE_SERIES)
									OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_SURVIVAL_SERIES)
									OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_OPEN_WHEEL_SERIES)
									OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_STREET_RACE_SERIES)
									OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_PURSUIT_SERIES)
									#IF FEATURE_GEN9_EXCLUSIVE
									OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HSW_RACE_SERIES)
									#ENDIF
									OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_COMMUNITY_SERIES)
									OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CAYO_PERICO_SERIES)
										iRootContentIDHash = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iReturnArrayPos].iRootContentIdHash
										IF g_FMMC_STRUCT.iRootContentIDHash != iRootContentIDHash
										AND CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ADVERSARY_SERIES_JOB(iRootContentIDHash, TRUE)
											serverVars.iJobVoteWinner = serverVars.iFound
											SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_GRABBED_JOBS)
											serverVars.iRandomProgress = RANDOM_STAGE_END
										ENDIF
									ENDIF
								ENDIF
								
								#IF IS_DEBUG_BUILD
								IF GET_COMMANDLINE_PARAM_EXISTS("sc_NJVSskipRandom")
									INT iLoopTemp
									TEXT_LABEL_23 tl23Temp
									FOR iLoopTemp = 0 TO (NUM_JOBS_TO_GRAB - 1)
										IF serverVars.iFound = iLoopTemp
											tl23Temp = "sc_NJVS"
											tl23Temp += iLoopTemp
											IF GET_COMMANDLINE_PARAM_EXISTS(tl23Temp)
												serverVars.tlFileName[serverVars.iFound] = GET_COMMANDLINE_PARAM(tl23Temp)
											ENDIF
										ENDIF
									ENDFOR									
								ENDIF
								#ENDIF
								
								#IF IS_DEBUG_BUILD
								PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, [SUCCESS] GET_JOB_NAME = ", tl23ContentID)
								PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, [SUCCESS] GET_JOB_TYPE = ", GET_EOJV_MISSION_TYPE(iReturnArrayPos, !bRockStarCreated))
								#ENDIF
								
								serverVars.iStoredRandom[serverVars.iFound] = iReturnArrayPos
								PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, [SUCCESS] RANDOM_STAGE_GRAB, iStoredRandom[", serverVars.iFound, "] = ", serverVars.iStoredRandom[serverVars.iFound])
								
								//clear that we are using verified if it's been over written
								IF bRockStarCreated
								AND IS_BIT_SET(serverVars.iVerifiedBitSet, serverVars.iFound)
									PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, CLEAR_BIT(serverVars.iVerifiedBitSet, serverVars.iFound) - serverVars.iFound = ", serverVars.iFound)
									CLEAR_BIT(serverVars.iVerifiedBitSet, serverVars.iFound)
								ENDIF
								
								// Increment found
								serverVars.iFound++
								PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, [SUCCESS] RANDOM_STAGE_GRAB, serverVars.iFound = ", serverVars.iFound)
								
								// Set the bit saying we have grabbed a Job of type
								SET_BIT(serverVars.iFoundJobOfTypeBitset, iJobType)
								
								// Increment team mission counter
								IF GET_EOJV_MISSION_MAX_NUMBER_OF_TEAMS(iReturnArrayPos, !bRockStarCreated) > 1
									PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, iReturnArrayPos = ", iReturnArrayPos, " iMaxNumberOfTeams = ", GET_EOJV_MISSION_MAX_NUMBER_OF_TEAMS(iReturnArrayPos, !bRockStarCreated))
									g_sMC_serverBDEndJob.iTeamMissionsFound++
									PRINTLN(" [CS_JOB] SERVER_ONLY_GRABBED_RANDOM_JOBS, iTeamMissionsFound = ", g_sMC_serverBDEndJob.iTeamMissionsFound)
								ENDIF
							ELSE
								PRINTLN(" [CS_JOB] GET_ONLY_COMPATABLE_JOBS IS_NEW_RANDOM_JOB = FALSE")
							ENDIF
							
						//Complain if it's NULL!
						#IF IS_DEBUG_BUILD
						
						ELSE
							IF serverVars.iFound != 0
							OR NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_FIRST_JOB_IS_RANDOM)
								PRINTLN(" [CS_JOB] GET_ONLY_COMPATABLE_JOBS IS_STRING_NULL_OR_EMPTY(", tl23ContentID, ")")							
								SCRIPT_ASSERT(" [CS_JOB] GET_ONLY_COMPATABLE_JOBS IS_STRING_NULL_OR_EMPTY(tl23ContentID)")			
							ENDIF
							
						#ENDIF
						
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE RANDOM_STAGE_END
		
			RETURN TRUE
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

CONST_INT RANK_MIN 5

PROC SERVER_COUNTING(SERVER_NEXT_JOB_VARS& serverVars, BOOL bCountVotes)

	PARTICIPANT_INDEX temppart
	INT iparticipant	
	INT iJobVotes[JOB_PANEL_TOTAL]
	INT iNumberOnMission 
	INT iPlayerRank, iTotalRank
	INT iTotalVotes
	INT iExitVotes
//	INT iNumReady, iNumRefreshed
	INT iAverageRank
	BOOL bEveryoneRefreshed = TRUE
	BOOL bEveryoneReady = TRUE
	BOOL bEveryoneVoting = TRUE

	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		temppart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
			PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
//			IF NOT IS_PLAYER_ON_A_PLAYLIST(playerId)
			IF NOT IS_PLAYER_SCTV(playerId)
			
				INT iPlayerSlot = NATIVE_TO_INT(playerId)
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[iPlayerSlot].bQuitJob

					iNumberOnMission++
					
					// Ranks
					iPlayerRank = GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(playerId))
					iTotalRank += iPlayerRank
					iAverageRank = (iTotalRank / iNumberOnMission)
					
					IF iAverageRank < RANK_MIN
						iAverageRank = RANK_MIN
					ENDIF
					
					// Count number ready
					IF NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_EVERYONE_READY)
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVoteBitset, JOB_VOTE_CLIENT_READY)
							//PRINTLN(" [CS_JOB] SERVER_COUNTING, SERVER_BOOL_EVERYONE_READY, - iPlayerSlot =  ", iPlayerSlot)
							bEveryoneReady = FALSE
						ENDIF
					ENDIF
					
					// Count num refreshed
					IF NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENTS_REF)
						IF GlobalplayerBD_FM[iPlayerSlot].iClientJobLogicStage <> JOB_VOTE_REFRESHING
						OR GlobalplayerBD_FM[iPlayerSlot].iClientJobLogicStage = JOB_VOTE_END
							//PRINTLN(" [CS_JOB] SERVER_COUNTING, SERVER_BOOL_ALL_CLIENTS_REF - iPlayerSlot =  ", iPlayerSlot)
							bEveryoneRefreshed = FALSE
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENT_VOTING)
						IF GlobalplayerBD_FM[iPlayerSlot].iClientJobLogicStage <> JOB_VOTE_DO_VOTING
						AND GlobalplayerBD_FM[iPlayerSlot].iClientJobLogicStage <> JOB_VOTE_END
							bEveryoneVoting = FALSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[1943902] bEveryoneVoting = FALSE, iPlayerSlot = ", iPlayerSlot, " IS on iClientJobLogicStage = ", GlobalplayerBD_FM[iPlayerSlot].iClientJobLogicStage)
						#ENDIF
						ENDIF
					ENDIF
					
					IF bCountVotes

						// Votes
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_ZERO)
							iJobVotes[JOB_PANEL_ZERO]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_ONE)
							iJobVotes[JOB_PANEL_ONE]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_TWO)
							iJobVotes[JOB_PANEL_TWO]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_THREE)
							iJobVotes[JOB_PANEL_THREE]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_FOUR)
							iJobVotes[JOB_PANEL_FOUR]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_FIVE)
							iJobVotes[JOB_PANEL_FIVE]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_SIX)
							iJobVotes[JOB_PANEL_SIX]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_SEVEN)
							iJobVotes[JOB_PANEL_SEVEN]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_EIGHT)
							iJobVotes[JOB_PANEL_EIGHT]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_NINE)
							iJobVotes[JOB_PANEL_NINE]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_TEN)
							iJobVotes[JOB_PANEL_TEN]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_ELEVEN)
							iJobVotes[JOB_PANEL_ELEVEN]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_TWELVE)
							iJobVotes[JOB_PANEL_TWELVE]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_THIRTEEN)
							iJobVotes[JOB_PANEL_THIRTEEN]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_FOURTEEN)
							iJobVotes[JOB_PANEL_FOURTEEN]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_FIFTEEN)
							iJobVotes[JOB_PANEL_FIFTEEN]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_SIXTEEN)
							iJobVotes[JOB_PANEL_SIXTEEN]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_SEVENTEEN)
							iJobVotes[JOB_PANEL_SEVENTEEN]++
							iTotalVotes++
						ENDIF
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_REPLAY)
							iJobVotes[JOB_PANEL_REPLAY]++
							iTotalVotes++
						ENDIF
						
						IF ON_LAST_PAGE_REFRESHED(serverVars) 
							IF iJobVotes[JOB_PANEL_REFRESH] <> 0
								iJobVotes[JOB_PANEL_REFRESH] = 0
							ENDIF
						ELSE
							IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_REFRESH)
								iJobVotes[JOB_PANEL_REFRESH]++
								iTotalVotes++
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_EXIT)
							iJobVotes[JOB_PANEL_EXIT]++
							iTotalVotes++
							iExitVotes++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	// Store average rank
	INT iAverageRankBand = GET_AVERAGE_RANK_BAND_FOR_NJVS(iAverageRank)
	IF serverVars.iAverageRank <> iAverageRankBand
		serverVars.iAverageRank = iAverageRankBand
	ENDIF
	
	// Store highest rank
	IF serverVars.iHighestPlayerRank < iPlayerRank
		serverVars.iHighestPlayerRank = iPlayerRank
	ENDIF
	
	// Number players
	IF serverVars.iNumberOnJob <> iNumberOnMission
		serverVars.iNumberOnJob = iNumberOnMission
		PRINTLN(" [CS_JOB] SERVER_COUNTING - iNumberOnJob =  ", serverVars.iNumberOnJob)
	ENDIF
	
	// Set everyone ready to vote
	IF NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_EVERYONE_READY)
		IF iNumberOnMission > 0
			IF bEveryoneReady
				PRINTLN(" [CS_JOB] SERVER_COUNTING - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_EVERYONE_READY) iNumberOnMission = ",iNumberOnMission)
				serverVars.iRefreshValue++
				SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_EVERYONE_READY)
			ENDIF
		ENDIF
	ENDIF
	
	// Set everyone refreshed
	IF NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENTS_REF)
		IF iNumberOnMission > 0
			IF bEveryoneRefreshed
				PRINTLN(" [CS_JOB] SERVER_COUNTING - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENTS_REF) ", iNumberOnMission)
				serverVars.iRefreshValue++
				SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENTS_REF)
			ENDIF
		ENDIF
	ELSE
		IF NOT bEveryoneRefreshed
			PRINTLN(" [CS_JOB] SERVER_COUNTING - CLEAR_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENTS_REF) ", iNumberOnMission)
			CLEAR_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENTS_REF)
		ENDIF
	ENDIF
	
	// All clients voting
	IF NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENT_VOTING)
		IF iNumberOnMission > 0
			IF bEveryoneVoting
				PRINTLN(" [CS_JOB] SERVER_COUNTING - SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENT_VOTING) ", iNumberOnMission)
				SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENT_VOTING)
			ENDIF
		ENDIF
	ELSE
		IF NOT bEveryoneVoting
			PRINTLN(" [CS_JOB] SERVER_COUNTING - CLEAR_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENT_VOTING)) ")
			CLEAR_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_ALL_CLIENT_VOTING)
		ENDIF
	ENDIF
	
	// Votes for each Job
	IF bCountVotes
		IF serverVars.iJobVotes[JOB_PANEL_ZERO] <> iJobVotes[JOB_PANEL_ZERO]
			serverVars.iJobVotes[JOB_PANEL_ZERO] = iJobVotes[JOB_PANEL_ZERO]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_ZERO =  ", serverVars.iJobVotes[JOB_PANEL_ZERO])
		ENDIF
		IF serverVars.iJobVotes[JOB_PANEL_ONE] <> iJobVotes[JOB_PANEL_ONE]
			serverVars.iJobVotes[JOB_PANEL_ONE] = iJobVotes[JOB_PANEL_ONE]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_ONE =  ", serverVars.iJobVotes[JOB_PANEL_ONE])
		ENDIF
		IF serverVars.iJobVotes[JOB_PANEL_TWO] <> iJobVotes[JOB_PANEL_TWO]
			serverVars.iJobVotes[JOB_PANEL_TWO] = iJobVotes[JOB_PANEL_TWO]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_TWO =  ", serverVars.iJobVotes[JOB_PANEL_TWO])
		ENDIF
		IF serverVars.iJobVotes[JOB_PANEL_THREE] <> iJobVotes[JOB_PANEL_THREE]
			serverVars.iJobVotes[JOB_PANEL_THREE] = iJobVotes[JOB_PANEL_THREE]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_THREE =  ", serverVars.iJobVotes[JOB_PANEL_THREE])
		ENDIF
		IF serverVars.iJobVotes[JOB_PANEL_FOUR] <> iJobVotes[JOB_PANEL_FOUR]
			serverVars.iJobVotes[JOB_PANEL_FOUR] = iJobVotes[JOB_PANEL_FOUR]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_FOUR =  ", serverVars.iJobVotes[JOB_PANEL_FOUR])
		ENDIF
		IF serverVars.iJobVotes[JOB_PANEL_FIVE] <> iJobVotes[JOB_PANEL_FIVE]
			serverVars.iJobVotes[JOB_PANEL_FIVE] = iJobVotes[JOB_PANEL_FIVE]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_FIVE =  ", serverVars.iJobVotes[JOB_PANEL_FIVE])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_SIX] <> iJobVotes[JOB_PANEL_SIX]
			serverVars.iJobVotes[JOB_PANEL_SIX] = iJobVotes[JOB_PANEL_SIX]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_SIX =  ", serverVars.iJobVotes[JOB_PANEL_SIX])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_SEVEN] <> iJobVotes[JOB_PANEL_SEVEN]
			serverVars.iJobVotes[JOB_PANEL_SEVEN] = iJobVotes[JOB_PANEL_SEVEN]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_SEVEN =  ", serverVars.iJobVotes[JOB_PANEL_SEVEN])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_EIGHT] <> iJobVotes[JOB_PANEL_EIGHT]
			serverVars.iJobVotes[JOB_PANEL_EIGHT] = iJobVotes[JOB_PANEL_EIGHT]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_EIGHT =  ", serverVars.iJobVotes[JOB_PANEL_EIGHT])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_NINE] <> iJobVotes[JOB_PANEL_NINE]
			serverVars.iJobVotes[JOB_PANEL_NINE] = iJobVotes[JOB_PANEL_NINE]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_NINE =  ", serverVars.iJobVotes[JOB_PANEL_NINE])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_TEN] <> iJobVotes[JOB_PANEL_TEN]
			serverVars.iJobVotes[JOB_PANEL_TEN] = iJobVotes[JOB_PANEL_TEN]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_TEN =  ", serverVars.iJobVotes[JOB_PANEL_TEN])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_ELEVEN] <> iJobVotes[JOB_PANEL_ELEVEN]
			serverVars.iJobVotes[JOB_PANEL_ELEVEN] = iJobVotes[JOB_PANEL_ELEVEN]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_ELEVEN =  ", serverVars.iJobVotes[JOB_PANEL_ELEVEN])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_TWELVE] <> iJobVotes[JOB_PANEL_TWELVE]
			serverVars.iJobVotes[JOB_PANEL_TWELVE] = iJobVotes[JOB_PANEL_TWELVE]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_TWELVE =  ", serverVars.iJobVotes[JOB_PANEL_TWELVE])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_THIRTEEN] <> iJobVotes[JOB_PANEL_THIRTEEN]
			serverVars.iJobVotes[JOB_PANEL_THIRTEEN] = iJobVotes[JOB_PANEL_THIRTEEN]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_THIRTEEN =  ", serverVars.iJobVotes[JOB_PANEL_THIRTEEN])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_FOURTEEN] <> iJobVotes[JOB_PANEL_FOURTEEN]
			serverVars.iJobVotes[JOB_PANEL_FOURTEEN] = iJobVotes[JOB_PANEL_FOURTEEN]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_FOURTEEN =  ", serverVars.iJobVotes[JOB_PANEL_FOURTEEN])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_FIFTEEN] <> iJobVotes[JOB_PANEL_FIFTEEN]
			serverVars.iJobVotes[JOB_PANEL_FIFTEEN] = iJobVotes[JOB_PANEL_FIFTEEN]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_FIFTEEN =  ", serverVars.iJobVotes[JOB_PANEL_FIFTEEN])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_SIXTEEN] <> iJobVotes[JOB_PANEL_SIXTEEN]
			serverVars.iJobVotes[JOB_PANEL_SIXTEEN] = iJobVotes[JOB_PANEL_SIXTEEN]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_SIXTEEN =  ", serverVars.iJobVotes[JOB_PANEL_SIXTEEN])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_SEVENTEEN] <> iJobVotes[JOB_PANEL_SEVENTEEN]
			serverVars.iJobVotes[JOB_PANEL_SEVENTEEN] = iJobVotes[JOB_PANEL_SEVENTEEN]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_SEVENTEEN =  ", serverVars.iJobVotes[JOB_PANEL_SEVENTEEN])
		ENDIF
		
		IF serverVars.iJobVotes[JOB_PANEL_REPLAY] <> iJobVotes[JOB_PANEL_REPLAY]
			serverVars.iJobVotes[JOB_PANEL_REPLAY] = iJobVotes[JOB_PANEL_REPLAY]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_REPLAY =  ", serverVars.iJobVotes[JOB_PANEL_REPLAY])
		ENDIF
		
		IF ON_LAST_PAGE_REFRESHED(serverVars)
		OR RAN_OUT_OF_REFRESHES(serverVars) 
		OR CANT_REFRESH_YET(serverVars)
			IF serverVars.iJobVotes[JOB_PANEL_REFRESH] <> 0
				serverVars.iJobVotes[JOB_PANEL_REFRESH] = 0
			ENDIF
		ELSE
			IF serverVars.iJobVotes[JOB_PANEL_REFRESH] <> iJobVotes[JOB_PANEL_REFRESH]
				serverVars.iJobVotes[JOB_PANEL_REFRESH] = iJobVotes[JOB_PANEL_REFRESH]
				serverVars.iRefreshValue++
				PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_REFRESH =  ", serverVars.iJobVotes[JOB_PANEL_REFRESH])
			ENDIF
		ENDIF

		IF serverVars.iJobVotes[JOB_PANEL_EXIT] <> iJobVotes[JOB_PANEL_EXIT]
			serverVars.iJobVotes[JOB_PANEL_EXIT] = iJobVotes[JOB_PANEL_EXIT]
			serverVars.iRefreshValue++
			PRINTLN(" [CS_JOB] SERVER_COUNTING - JOB_PANEL_EXIT =  ", serverVars.iJobVotes[JOB_PANEL_EXIT])
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF g_bDontCountVotes = FALSE
		#ENDIF
		
		// Total vote count
		IF serverVars.iVoteTotal <> iTotalVotes
			serverVars.iVoteTotal = iTotalVotes
			PRINTLN(" [CS_JOB] SERVER_COUNTING - iVoteTotal =  ", serverVars.iVoteTotal)
		ENDIF
		
		// Total votes minus those for Freemode
		INT iMajorityVotes = (iTotalVotes - iExitVotes)
		IF serverVars.iMajorityVoteTotal <> iMajorityVotes
			serverVars.iMajorityVoteTotal = iMajorityVotes
			PRINTLN(" [CS_JOB] SERVER_COUNTING - iMajorityVoteTotal =  ", serverVars.iMajorityVoteTotal)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAVE_PLAYERS_VOTED_RETURN_GTA_ONLINE(SERVER_NEXT_JOB_VARS& serverVars)
	IF serverVars.iJobVoteWinner = JOB_PANEL_EXIT
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_PLAYERS_VOTED_REFRESH(SERVER_NEXT_JOB_VARS& serverVars)
	IF serverVars.iJobVoteWinner = JOB_PANEL_REFRESH
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_PLAYERS_VOTED_RESTART(SERVER_NEXT_JOB_VARS& serverVars, BOOL bSecondMost = FALSE)
	IF bSecondMost
		IF serverVars.iSecondryJobVoteWinner = JOB_PANEL_REPLAY
		
			RETURN TRUE
		ENDIF
	ELSE
		IF serverVars.iJobVoteWinner = JOB_PANEL_REPLAY
		
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_SERVER_PICKED_WINNER(SERVER_NEXT_JOB_VARS& serverVars)
	
	IF HAVE_PLAYERS_VOTED_REFRESH(serverVars)
	
		RETURN FALSE
	ENDIF

	IF serverVars.iJobVoteWinner <> -1
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_SERVER_PICKED_RANDOM_AS_SECONDRY_VOTE(SERVER_NEXT_JOB_VARS& serverVars)
	IF serverVars.iSecondryJobVoteWinner = -1
		RETURN FALSE
	ENDIF
	IF serverVars.iSecondryJobVoteWinner = JOB_PANEL_ZERO
	OR serverVars.iSecondryJobVoteWinner = JOB_PANEL_ONE
	OR serverVars.iSecondryJobVoteWinner = JOB_PANEL_TWO
	OR serverVars.iSecondryJobVoteWinner = JOB_PANEL_THREE
	OR serverVars.iSecondryJobVoteWinner = JOB_PANEL_FOUR
	OR serverVars.iSecondryJobVoteWinner = JOB_PANEL_FIVE
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SERVER_SETS_JOB_WINNER(SERVER_NEXT_JOB_VARS& serverVars)
	INT iRandom
	IF HAS_VOTING_ENDED(serverVars)
		IF NOT HAS_SERVER_PICKED_WINNER(serverVars)
			IF serverVars.iVoteTotal = 0
				//last page
				IF ON_LAST_PAGE_REFRESHED(serverVars)
					iRandom = NETWORK_GET_RANDOM_INT_RANGED((NUM_JOBS_ON_ONE_PAGE * 2), NUM_JOBS_TO_GRAB - 1)
					PRINTLN(" [CS_JOB] SERVER_SETS_JOB_WINNER - ON_LAST_PAGE_REFRESHED, iRandom = NETWORK_GET_RANDOM_INT_RANGED((NUM_JOBS_ON_ONE_PAGE * 2), NUM_JOBS_TO_GRAB - 1)")
				//Second page
				ELIF ON_SECOND_PAGE_REFRESHED(serverVars)
					iRandom = NETWORK_GET_RANDOM_INT_RANGED(NUM_JOBS_ON_ONE_PAGE, (NUM_JOBS_ON_ONE_PAGE * 2) - 1)
					PRINTLN(" [CS_JOB] SERVER_SETS_JOB_WINNER - ON_SECOND_PAGE_REFRESHED, iRandom = NETWORK_GET_RANDOM_INT_RANGED(NUM_JOBS_ON_ONE_PAGE, (NUM_JOBS_ON_ONE_PAGE * 2) - 1)")
				//First page
				ELSE
					//If we're set to use random on the first page then pick that
					IF IS_FIRST_JOB_RANDOM(serverVars)
						PRINTLN(" [CS_JOB] SERVER_SETS_JOB_WINNER - IS_BIT_SET(serverVars.iServerJobBitSet, IF IS_FIRST_JOB_RANDOM), iRandom = 0")
						iRandom = 0
					//standard behaviour then select any
					ELSE
						iRandom = NETWORK_GET_RANDOM_INT_RANGED(0, NUM_JOBS_ON_ONE_PAGE - 1)
						PRINTLN(" [CS_JOB] SERVER_SETS_JOB_WINNER - iRandom = NETWORK_GET_RANDOM_INT_RANGED(0, NUM_JOBS_ON_ONE_PAGE - 1)")
					ENDIF
				ENDIF
				serverVars.iJobVoteWinner = iRandom
				PRINTLN(" [CS_JOB] SERVER_SETS_JOB_WINNER, HAS_VOTING_TIMED_OUT =  ", serverVars.iJobVoteWinner)
			ELSE
				serverVars.iJobVoteWinner = GET_JOB_MOST_VOTES(serverVars)
				PRINTLN(" [CS_JOB] SERVER_SETS_JOB_WINNER =  ",serverVars.iJobVoteWinner)
				//Force the assassination missions to pick a mission out of the random selection
				IF serverVars.iJobVoteWinner = 0
				AND IS_FIRST_JOB_RANDOM(serverVars)
				AND IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION)	
					serverVars.iJobVoteWinner = NETWORK_GET_RANDOM_INT_RANGED(1, NUM_JOBS_ON_ONE_PAGE - 1)
				ENDIF
				PRINTLN(" [CS_JOB] SERVER_SETS_JOB_WINNER =  SERVER_BOOL_IS_ASSASSINATION_MISSION - ",serverVars.iJobVoteWinner)
				serverVars.iRefreshValue++
				//If the vote was passed to return to freemode then we need to select a secondry mission if there is one
				IF HAVE_PLAYERS_VOTED_RETURN_GTA_ONLINE(serverVars)
					serverVars.iSecondryJobVoteWinner = GET_JOB_MOST_VOTES(serverVars, TRUE)
					PRINTLN(" [CS_JOB] SERVER_SETS_JOB_WINNER  iSecondryJobVoteWinner =  ",serverVars.iSecondryJobVoteWinner)
				ENDIF		
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL IS_SPECIAL_JOB(INT iJob)

	SWITCH iJob
		CASE JOB_PANEL_REPLAY
		CASE JOB_PANEL_REFRESH
		CASE JOB_PANEL_EXIT
		
			RETURN TRUE
		BREAK
		 
	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC REMOVE_VOTE_FROM_JOB(INT iJobPanel)
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	CLEAR_BIT(GlobalplayerBD_FM[iPlayer].iJobVote, iJobPanel)

	PRINTLN(" [CS_JOB] REMOVE_VOTE_FROM_JOB  ", iJobPanel)
ENDPROC

PROC WIPE_ALL_CLIENT_VOTES()
	INT i
	REPEAT JOB_PANEL_TOTAL i
		REMOVE_VOTE_FROM_JOB(i)
	ENDREPEAT
ENDPROC

FUNC BOOL JOB_IS_REPLAY(INT iJob)
	IF iJob = JOB_PANEL_REPLAY
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL JOB_IS_REFRESH(INT iJob)
	IF iJob = JOB_PANEL_REFRESH
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL JOB_IS_EXIT(INT iJob)
	IF iJob = JOB_PANEL_EXIT
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_JOB_NEED_IMAGE(INT iJob)	

	IF iJob < NUM_JOBS_TO_GRAB
	
		RETURN TRUE
	ENDIF
//
//	SWITCH iJob
//		CASE JOB_PANEL_ZERO 		
//		CASE JOB_PANEL_ONE			
//		CASE JOB_PANEL_TWO 		
//		CASE JOB_PANEL_THREE 		
//		CASE JOB_PANEL_FOUR			
//		CASE JOB_PANEL_FIVE		
//		CASE 
//	
//	
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUM_VOTES_FOR_JOB(SERVER_NEXT_JOB_VARS& serverVars, INT iJob)
	RETURN serverVars.iJobVotes[iJob]
ENDFUNC

FUNC INT GET_JOB_PANEL_SELECTED(NEXT_JOB_STRUCT& nextJobStruct)
	RETURN nextJobStruct.iJobPanelSelected
ENDFUNC

FUNC STRING GET_GTA_ONLINE_JOB_NAME(SERVER_NEXT_JOB_VARS& serverVars, INT iJob, BOOL bGridRefresh = FALSE)
	
	IF JOB_IS_REPLAY(iJob)
	
		RETURN "FM_NXT_TREP"
	ENDIF
	
	IF JOB_IS_REFRESH(iJob)
		IF bGridRefresh
		
			RETURN "FM_NXT_TREF"
		ELSE
			IF ON_SECOND_PAGE_REFRESHED(serverVars)
			OR ON_LAST_PAGE_REFRESHED(serverVars)
			
				RETURN "FM_NXT_TREFN"
			ELSE
		
				RETURN "FM_NXT_TREFN"
			ENDIF
		ENDIF
	ENDIF
	
	IF JOB_IS_EXIT(iJob)
	
		RETURN "FM_NXT_GTA"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_DESC_SUB_HEADING(INT iJob)
	
	IF JOB_IS_REPLAY(iJob)
	
		RETURN "FM_NXT_SUB1"
	ENDIF
	
	IF JOB_IS_REFRESH(iJob)

		RETURN "FM_NXT_SUB2"
	ENDIF
	
	IF JOB_IS_EXIT(iJob)
	
		RETURN "FM_NXT_SUB3"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_GTA_ONLINE_JOB_DESCRIPTION(INT iJob)

	IF JOB_IS_REPLAY(iJob)
	
		RETURN "FM_NXT_REP"
	ENDIF
	
	IF JOB_IS_REFRESH(iJob)
	
		RETURN "FM_NXT_REF"
	ENDIF
	
	IF JOB_IS_EXIT(iJob)
	
		RETURN "FM_NXT_GTAD"
	ENDIF
	
	RETURN ""
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║		DRAWING																	║
//╚═════════════════════════════════════════════════════════════════════════════╝	

PROC DRAW_END_JOB_TITLE(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NXT_J_TIT") 					// Vote on the next Job!
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_NXT_VCNT")							// 9/9 Votes
			ADD_TEXT_COMPONENT_INTEGER(serverVars.iVoteTotal)
			ADD_TEXT_COMPONENT_INTEGER(serverVars.iNumberOnJob)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC	

CONST_INT TXT_FROM_CLOUD 	0
CONST_INT TXT_FROM_DISC  	1

CONST_INT ROCK_NONE			0
CONST_INT ROCK_VERIFIED		1
CONST_INT ROCK_CREATED		2

FUNC INT GET_ROCKSTAR_VERIFIED_STATUS()//(INT iJob)

//	SWITCH iJob
//		CASE 
//	ENDSWITCH
	
	RETURN ROCK_CREATED

ENDFUNC

FUNC INT GET_JOB_FOR_DRAWING(INT iJob)
	SWITCH iJob
		CASE JOB_PANEL_REPLAY	
		
			RETURN JOB_PANEL_SIX
		BREAK
		CASE JOB_PANEL_REFRESH			
		
			RETURN JOB_PANEL_SEVEN
		BREAK
		CASE JOB_PANEL_EXIT			
		
			RETURN JOB_PANEL_EIGHT
		BREAK
	ENDSWITCH
	
	RETURN (iJob%NUM_JOBS_ON_ONE_PAGE)
ENDFUNC

FUNC BOOL IS_JOB_BEING_DISPLAYED(SERVER_NEXT_JOB_VARS& serverVars, INT iJob)

	IF NOT DOES_JOB_NEED_IMAGE(iJob)
	
		RETURN TRUE
	ENDIF
	
	IF ON_LAST_PAGE_REFRESHED(serverVars)
		SWITCH iJob
			CASE JOB_PANEL_TWELVE 	
			CASE JOB_PANEL_THIRTEEN
			CASE JOB_PANEL_FOURTEEN
			CASE JOB_PANEL_FIFTEEN	
			CASE JOB_PANEL_SIXTEEN	
			CASE JOB_PANEL_SEVENTEEN
			
				RETURN TRUE
			BREAK
		ENDSWITCH
	ELIF ON_SECOND_PAGE_REFRESHED(serverVars) 
		SWITCH iJob
			CASE JOB_PANEL_SIX
			CASE JOB_PANEL_SEVEN
			CASE JOB_PANEL_EIGHT
			CASE JOB_PANEL_NINE	
			CASE JOB_PANEL_TEN
			CASE JOB_PANEL_ELEVEN
			
				RETURN TRUE
			BREAK
		ENDSWITCH
	ELSE
		SWITCH iJob
			CASE JOB_PANEL_ZERO 	
			CASE JOB_PANEL_ONE		
			CASE JOB_PANEL_TWO 	
			CASE JOB_PANEL_THREE 	
			CASE JOB_PANEL_FOUR	
			CASE JOB_PANEL_FIVE		
			
				RETURN TRUE
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_NJVS_SPECTATOR()
	IF DID_I_JOIN_MISSION_AS_SPECTATOR() 
	OR IS_PLAYER_SCTV(PLAYER_ID())
	
		RETURN TRUE 
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_REPLAY_ALLOWED()

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("scNJVSReplayYes")
	
		RETURN TRUE
	ENDIF
	#ENDIF

	IF CONTENT_IS_USING_ARENA()
		RETURN FALSE
	ENDIF

	IF CV2_IS_THIS_JOB_A_SERIES_JOB(TRUE)
	AND NOT g_sMPTunables.bEnableStuntSeriesReplay
	AND NOT CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_SURVIVAL_SERIES(g_FMMC_STRUCT.iRootContentIDHash)	// url:bugstar:5807077
		RETURN FALSE
	ENDIF
	 // #IF FEATURE_STUNT

	// Block replay for Contact Mission or random event (unless failed)
	IF g_bAllowJobReplay = FALSE
	    IF (Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID()) OR Is_Player_Currently_On_MP_Random_Event(PLAYER_ID()))

			RETURN FALSE
		ENDIF	
	ENDIF
	
	IF IS_NJVS_SPECTATOR()
	
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ARENA_MODE_FOR_NJVS(INT iAdversaryModeType)
	SWITCH iAdversaryModeType
		CASE ciNEWVS_ARENA_CTF
		CASE ciNEWVS_BOMB_FOOTBALL
		CASE ciNEWVS_MONSTER_JAM
		CASE ciNEWVS_TAG_TEAM
		CASE ciNEWVS_GAMES_MASTERS
		CASE ciNEWVS_DM_CARNAGE
		CASE ciNEWVS_DM_PASS_THE_BOMB
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC INT GET_JOB_TYPE_ICON(INT iType, INT iSubType = 0, INT iAdversaryModeType = 0, BOOL bGridItem = FALSE)

	SWITCH iType

		CASE FMMC_TYPE_MISSION
			IF IS_ARENA_MODE_FOR_NJVS(iAdversaryModeType)
				RETURN 25				
			ELIF iSubType = 0
				RETURN 0
			ELIF iSubType = FMMC_TYPE_MISSION_LTS
				RETURN 15
			ELIF iSubType = FMMC_TYPE_MISSION_CTF
				RETURN 16
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH    	
		
			SWITCH iSubType
				CASE FMMC_KING_OF_THE_HILL                              	
				CASE FMMC_KING_OF_THE_HILL_TEAM                            
	
					IF bGridItem
						RETURN 25
					ENDIF
					
					RETURN 28
			ENDSWITCH
		
			IF IS_ARENA_MODE_FOR_NJVS(iAdversaryModeType)
				RETURN 25				
			ENDIF
			
			RETURN 1
		
		CASE FMMC_TYPE_BASE_JUMP
		
			RETURN 8
		
		CASE FMMC_TYPE_RACE  
			SWITCH iSubType	
				//	Foot Race - 11
				CASE FMMC_RACE_TYPE_STANDARD									
				CASE FMMC_RACE_TYPE_P2P				
					RETURN 10
				
				CASE FMMC_RACE_TYPE_ARCADE_RACE
				CASE FMMC_RACE_TYPE_ARCADE_RACE_P2P
				CASE FMMC_RACE_TYPE_DESTRUCTION_DERBY
				CASE FMMC_RACE_TYPE_DESTRUCTION_DERBY_P2P
					RETURN 25
				
				CASE FMMC_RACE_TYPE_STUNT
				CASE FMMC_RACE_TYPE_STUNT_P2P	
				CASE FMMC_RACE_TYPE_TARGET
				CASE FMMC_RACE_TYPE_TARGET_P2P	
					RETURN 19
								
				CASE FMMC_RACE_TYPE_BOAT									
				CASE FMMC_RACE_TYPE_BOAT_P2P	 
				
					RETURN 13
				
				CASE FMMC_RACE_TYPE_AIR									
				CASE FMMC_RACE_TYPE_AIR_P2P		
				
					RETURN 14
						
				CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE							
				CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE_P2P	
				
					RETURN 12
			
				DEFAULT
					RETURN 2
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_SURVIVAL  
			RETURN 3
		
		CASE FMMC_TYPE_GANGHIDEOUT    
			RETURN 6
	ENDSWITCH 
	
	RETURN 0	
ENDFUNC

FUNC INT GET_ICON_COLOUR(BOOL bRockstarCreated)
	INT iReturn = ENUM_TO_INT(HUD_COLOUR_WHITE)

	IF bRockstarCreated
		iReturn = ENUM_TO_INT(HUD_COLOUR_BLUE)
	ENDIF
	
	RETURN iReturn
ENDFUNC

// Details in 1593290
PROC DRAW_IMAGES(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, BOOL bFirstTileOnly)
	INT i, iToDraw
	TEXT_LABEL_63 tlJob[NUM_JOBS_TO_GRAB] 
	STRING sJobName
	STRING stImageName[NUM_JOBS_TO_GRAB]
	INT iNumRefreshes = GET_NUM_REFRESHES_LEFT(serverVars)
	PRINTLN(" [CS_JOB] DRAW_IMAGES - iNumberCompatable - iNumRefreshes = ", iNumRefreshes)
	PRINTLN(" [CS_JOB] DRAW_IMAGES - iNumberCompatable - GET_NUMBER_OF_REFRESHES = ", GET_NUMBER_OF_REFRESHES(serverVars))
	
	//INT iImage
	
	REPEAT NUM_JOBS_TO_GRAB i
		stImageName[i] = TEXTURE_DOWNLOAD_GET_NAME(g_sNextJobImages.sDownLoadPhotoVars[i].iTextureDownloadHandle)
		PRINTLN(" [CS_JOB] DRAW_IMAGES - DOWNLOAD_PHOTO_FOR_FMMC_LITE - [", i, "] [", g_sNextJobImages.sDownLoadPhotoVars[i].iTextureDownloadHandle, "] stImageName = ", stImageName[i])
		tlJob[i] = GET_JOB_NAME(nextJobStruct, i, IS_BIT_SET(serverVars.iVerifiedBitSet, i))			
		PRINTLN(" [CS_JOB] DRAW_IMAGES tlJob = ", tlJob[i])
	ENDREPEAT
	
	BOOL bRockstarCreated
	INT iType 			
	INT iSubType		
	INT iSubTypeBadge 	
	BOOL bPlayed		
	BOOL bIsPushBikeOnly
	TEXT_LABEL_23 tl23	
	INT iHash, iRootContentIDHash, iAdversaryModeType
	FLOAT fRpX 			
	FLOAT fCashX
	FLOAT fApX
	FLOAT fFakeRpX
	FLOAT fFakeCashX
	
	REPEAT JOB_PANEL_TOTAL i
	
		// Only refresh first tile
		IF bFirstTileOnly
			IF i <> 0
				EXIT
			ENDIF
		ENDIF

		IF IS_JOB_BEING_DISPLAYED(serverVars, i)
			BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_GRID_ITEM")
			
				iToDraw = GET_JOB_FOR_DRAWING(i)
				
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iToDraw)					// 1 index
				
				// REPLAY / REFRESH / GTA ONLINE PANELS //////////////////
				
				IF NOT DOES_JOB_NEED_IMAGE(i)
					sJobName = GET_GTA_ONLINE_JOB_NAME(serverVars, i)
					IF JOB_IS_REFRESH(i)
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sJobName)
							ADD_TEXT_COMPONENT_INTEGER(iNumRefreshes)
						END_TEXT_COMMAND_SCALEFORM_STRING()
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sJobName) 		// 2 title
					ENDIF
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("")	// 3 txd	
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("") 	// 4 txn
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) 					// 5 textureLoadType
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) 					// 6 verifiedType
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-2)					// eIcon
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)				// played tick
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0) 					// rpMult
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0) 					// cashMult
					// Grey out
					IF JOB_IS_REFRESH(i)
						IF iNumRefreshes = 0
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						ENDIF
					ELIF JOB_IS_REPLAY(i)
						IF IS_REPLAY_ALLOWED()
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
						ENDIF
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)			// DISABLED
					ENDIF
				ELSE
				
					// JOB PANELS WITH IMAGES //////////////////
					//SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlJob[i]) 						// 2
					
					iType 				= GET_JOB_TYPE(serverVars, nextJobStruct, i)
					iSubType			= GET_EOJV_MISSION_SUB_TYPE(serverVars, nextJobStruct, i)
					iSubTypeBadge 		= GET_EOJV_MISSION_SUB_TYPE(serverVars, nextJobStruct, i, TRUE)
					bPlayed				= GET_JOB_PLAYED_STATUS(serverVars, nextJobStruct, i)
					bIsPushBikeOnly 	= IS_THIS_A_PUSHBIKE_ONLY_RACE(serverVars, nextJobStruct, i)
					iRootContentIDHash = GET_JOB_START_ROOT_ID(serverVars, nextJobStruct, i)
					iAdversaryModeType = GET_JOB_START_ADVERSARY_MODE_TYPE(serverVars, nextJobStruct, i)
					tl23				= GET_JOB_CONTENT_ID(serverVars, nextJobStruct, i)
					iHash 				= GET_HASH_KEY(tl23)
					fRpX 				= 0.0
					fCashX 				= 0.0
					fApX				= 0.0
					g_eTunablesContexts eTunAbleCon 	= GET_TUNABLE_CONTEXT(iType, iSubTypeBadge, bIsPushBikeOnly, iRootContentIDHash, iAdversaryModeType)
					g_eTunablesContexts eTunAbleConSub	= GET_CONTENT_MODIFIER_FROM_JOB_HASH(iHash)			
					#IF IS_DEBUG_BUILD
					PRINTLN("[NJVS BWTUN] [CS_ICONS] tl23        = ", tl23)
					PRINTLN("[NJVS BWTUN] [CS_ICONS] Name        = ", tl23)
					PRINTLN("[NJVS BWTUN] [CS_ICONS] iHash       = ", iHash)
					TEXT_LABEL_63 tl63Dbg = ""
					tl63Dbg = GET_g_eTunablesContexts_DEBUG_PRINT(eTunAbleCon)
					PRINTLN("[NJVS BWTUN] [CS_ICONS] GET_TUNABLE_CONTEXT(", iType, ", ", iSubTypeBadge, ") = ", tl63Dbg)
					tl63Dbg = GET_g_eTunablesContexts_DEBUG_PRINT(eTunAbleConSub)
					PRINTLN("[NJVS BWTUN] [CS_ICONS] GET_CONTENT_MODIFIER_FROM_JOB_HASH(", iHash, ")  = ", tl63Dbg)
					#ENDIF
					Fill_MP_Float_Tunable(eTunAbleCon, eTunAbleConSub, HASH("XP_MULTIPLIER"), fRpX)
					PRINTLN("[NJVS BWTUN] [CS_ICONS] XP_MULTIPLIER   = ", fRpX)
					Fill_MP_Float_Tunable(eTunAbleCon, eTunAbleConSub, HASH("CASH_MULTIPLIER"), fCashX)
					PRINTLN("[NJVS BWTUN] [CS_ICONS] CASH_MULTIPLIER = ", fCashX)
					Fill_MP_Float_Tunable(eTunAbleCon, eTunAbleConSub, HASH("AP_MULTIPLIER"), fApX)
					PRINTLN("[NJVS BWTUN] [CS_ICONS] AP_MULTIPLIER = ", fApX)
					
					Fill_MP_Float_Tunable(eTunAbleCon, eTunAbleConSub, HASH("FAKE_XP_MULTIPLIER"), fFakeRpX)
					Fill_MP_Float_Tunable(eTunAbleCon, eTunAbleConSub, HASH("FAKE_CASH_MULTIPLIER"), fFakeCashX)
					
					IF fFakeRpX > 0.0
						fRpX = fFakeRpX
					ENDIF
					IF fFakeCashX > 0.0
						fCashX = fFakeCashX
					ENDIF 
					
					//Get the XP and Cash multipliers
					IF fRpX < 1.0
						fRpX = 1.0
					ENDIF
					IF fCashX < 1.0
						fCashX = 1.0
					ENDIF
					IF fApX < 1.0
						fApX = 1.0
					ENDIF
					
					
					// 1972304
					IF (g_sMPTunables.bdisable_modifier_badges)
						fCashX = 1.0
						fRpX = 1.0
						fApX = 1.0
					ENDIF
					
					bRockstarCreated = IS_JOB_ROCKSTAR_CREATED(serverVars, nextJobStruct, i)
					
					// Overwrite above when alternating random first image
					IF SHOULD_ALTERNATE_IMAGES(serverVars)	
					
						IF i = 0
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NXT_RNDM") 					// 2
							//iImage 				= nextJobStruct.iImageToDisplay
							bRockstarCreated 	= TRUE
							fCashX 				= 1.0
							fRpX 				= 1.0
							fApX				= 1.0
							iType 				= g_fmmc_struct.iMissionType
							IF iType = FMMC_TYPE_RACE
								iSubType			= g_fmmc_struct.iRaceType
								iSubTypeBadge 		= g_fmmc_struct.iRaceType
							ELSE
								iSubType			= g_fmmc_struct.iMissionSubType
								iSubTypeBadge 		= g_fmmc_struct.iMissionSubType
							ENDIF
							
							bPlayed				= FALSE
							bIsPushBikeOnly 	= FALSE
							tl23				= ""
							iHash 				= GET_HASH_KEY(tl23)
							fRpX 				= 0.0
							fCashX 				= 0.0
							fApX				= 0.0
							
						// Default
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlJob[i]) 					// 2
							//iImage = i
						ENDIF
					// Default
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlJob[i]) 						// 2
						//iImage = i
					ENDIF
					
					// CLOUD IMAGES
					IF NOT IS_STRING_NULL_OR_EMPTY(stImageName[i])
					#IF IS_DEBUG_BUILD
					AND g_bBlankImages = FALSE
					#ENDIF
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(stImageName[i]) 	// 3 		
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(stImageName[i]) 	// 4	
						PRINTLN(" [CS_JOB] DRAW_IMAGES - CS_TEST [i] = ", i, " Image = ", stImageName[i])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(TXT_FROM_CLOUD) 					// 5
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROCK_NONE) 						// 6
						PRINTLN("[CS_ICONS] GET_JOB_TYPE_ICON = ", GET_JOB_TYPE_ICON(iType, iSubType, iAdversaryModeType, TRUE), " iType = ", iType, " iSubType = ", iSubType, " iAdversaryModeType = ", iAdversaryModeType)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_JOB_TYPE_ICON(iType, iSubType, iAdversaryModeType, TRUE))// icon
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bPlayed)							// played tick
						IF fRpX != 1.0
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fRpX) 						// rpMulti
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0) 						// rpMulti		
						ENDIF
						IF fCashX != 1.0
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCashX) 						// cashMulti
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0) 						// rpMulti		
						ENDIF
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)							// DISABLED
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_ICON_COLOUR(bRockstarCreated))
						IF fApX != 1.0
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(fApX))
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						ENDIF
					ELSE
//						// Don't draw R* tiles for the 1st random one
//						IF SHOULD_ALTERNATE_IMAGES(serverVars)		
//							IF i = 0
//								EXIT
//							ENDIF
//						ENDIF
						
						// DISK IMAGES (R* LOGO)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("")				// 3	
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("") 				// 4
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(TXT_FROM_DISC) 					// 5
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROCK_NONE) 						// 6
						PRINTLN("[CS_ICONS] GET_JOB_TYPE_ICON = ", GET_JOB_TYPE_ICON(iType, iSubType, iAdversaryModeType, TRUE), " iType = ", iType, " iSubType = ", iSubType, " iAdversaryModeType = ", iAdversaryModeType)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_JOB_TYPE_ICON(iType, iSubType, iAdversaryModeType, TRUE))// icon
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bPlayed)							// played tick
						IF fRpX != 1.0
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fRpX) 						// rpMulti
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0) 							// rpMulti							
						ENDIF
						IF fCashX != 1.0
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCashX) 						// cashMulti
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0) 							// rpMulti		
						ENDIF
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)							// DISABLED
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_ICON_COLOUR(bRockstarCreated))
						IF fApX != 1.0
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(fApX))
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						ENDIF
					ENDIF
				ENDIF

			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_VOTE_COLOUR(SERVER_NEXT_JOB_VARS& serverVars, INT iJob)

	INT iReturn

	IF iJob = GET_JOB_MOST_VOTES(serverVars)
		iReturn = ENUM_TO_INT(HUD_COLOUR_GREEN)
	ELIF serverVars.iJobVotes[iJob] <> 0
		iReturn = ENUM_TO_INT(HUD_COLOUR_RED)
	ELSE
		iReturn = ENUM_TO_INT(HUD_COLOUR_WHITE)
	ENDIF
	
	RETURN iReturn
ENDFUNC

CONST_INT FLASH_TIME_JOB_VOTE 500

FUNC BOOL SHOULD_FLASH(NEXT_JOB_STRUCT& nextJobStruct, BOOL bVoted)
	IF bVoted
		IF NOT HAS_NET_TIMER_STARTED(nextJobStruct.timerFlash)
			START_NET_TIMER(nextJobStruct.timerFlash)
		ELSE
			HAS_NET_TIMER_EXPIRED(nextJobStruct.timerFlash, FLASH_TIME_JOB_VOTE)
		ENDIF
		
		RETURN TRUE
	ELSE
		REINIT_NET_TIMER(nextJobStruct.timerFlash)
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_SAME_CREW_AS_LOCAL(INT iPlayerCrew)
	GAMER_HANDLE gamerHLocal
	INT iLocalCrew
	
	gamerHLocal = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	iLocalCrew	= GET_PLAYER_CREW_INT(gamerHLocal)
	
	IF iPlayerCrew <> 0 // No crew returns 0
	AND iLocalCrew <> 0
		IF iPlayerCrew = iLocalCrew  
			
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

// Draw the player vote on the correct Job tile (3 sets of 6 Jobs plus the special ones at the bottom 18,19,20)
FUNC INT GET_JOB_PLAYER_VOTED_FOR_TO_SHOW()
	INT iReturn = -1
	
	SWITCH g_sNextJobImages.iJobVotedFor
		
		CASE JOB_PANEL_ZERO
		CASE JOB_PANEL_SIX
		CASE JOB_PANEL_TWELVE
			iReturn = JOB_PANEL_ZERO
		BREAK
		
		CASE JOB_PANEL_ONE
		CASE JOB_PANEL_SEVEN
		CASE JOB_PANEL_THIRTEEN
			iReturn = JOB_PANEL_ONE
		BREAK
		
		CASE JOB_PANEL_TWO
		CASE JOB_PANEL_EIGHT
		CASE JOB_PANEL_FOURTEEN
			iReturn = JOB_PANEL_TWO
		BREAK
		
		CASE JOB_PANEL_THREE
		CASE JOB_PANEL_NINE
		CASE JOB_PANEL_FIFTEEN
			iReturn = JOB_PANEL_THREE
		BREAK	
		
		CASE JOB_PANEL_FOUR
		CASE JOB_PANEL_TEN
		CASE JOB_PANEL_SIXTEEN
			iReturn = JOB_PANEL_FOUR
		BREAK
			
		CASE JOB_PANEL_FIVE
		CASE JOB_PANEL_ELEVEN
		CASE JOB_PANEL_SEVENTEEN
			iReturn = JOB_PANEL_FIVE
		BREAK

		CASE JOB_PANEL_REPLAY	
			iReturn = JOB_PANEL_SIX
		BREAK
		
		CASE JOB_PANEL_REFRESH	
			iReturn = JOB_PANEL_SEVEN 
		BREAK
		
		CASE JOB_PANEL_EXIT		
			iReturn = JOB_PANEL_EIGHT
		BREAK

	ENDSWITCH
	
	RETURN iReturn
ENDFUNC

// Show player voting (1672219)
PROC SHOW_PLAYER_VOTING(NEXT_JOB_STRUCT& nextJobStruct)
	PLAYER_INDEX playerVoted 
	STRING sPlayerVoted
	INT iR = 255
	INT iG = 255
	INT iB = 255
	INT iA
	GAMER_HANDLE gamerH
	INT iPlayerCrew
	INT iJobToDrawPlayerOn
	
	IF g_sNextJobImages.bShowVote
		IF g_sNextJobImages.iPlayerWhoVoted <> -1
			playerVoted = INT_TO_PLAYERINDEX(g_sNextJobImages.iPlayerWhoVoted)
			sPlayerVoted = GET_PLAYER_NAME(playerVoted)
			iJobToDrawPlayerOn = GET_JOB_PLAYER_VOTED_FOR_TO_SHOW()
			
			IF IS_NET_PLAYER_OK(playerVoted, FALSE)
				gamerH 		= GET_GAMER_HANDLE_PLAYER(playerVoted)
				iPlayerCrew	= GET_PLAYER_CREW_INT(gamerH)
				
				// Colour player name
				IF playerVoted <> PLAYER_ID() 	
					IF IS_PLAYER_SAME_CREW_AS_LOCAL(iPlayerCrew)
						// Crew mates
						GET_PLAYER_CREW_COLOUR(iR, iG, iB)
						// CMcM 2232366 - Votes showing as black in the vote box. If colour has defaulted to black set as white.
						IF iR = 0
						AND iG = 0
						AND iB = 0
							iR = 255
							iG = 255
							iB = 255
						ENDIF
					ELSE
						IF NETWORK_IS_FRIEND(gamerH)
							// Friends
							GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)
						ELSE
							// Enemies
							GET_HUD_COLOUR(HUD_COLOUR_NET_PLAYER1, iR, iG, iB, iA)
						ENDIF
					ENDIF
				ENDIF

				IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerVoted)
					BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SHOW_PLAYER_VOTE")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iJobToDrawPlayerOn)	// 0-8, 0-5 for Jobs, 6-8 for replay, refresh, exit 
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sPlayerVoted)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
					END_SCALEFORM_MOVIE_METHOD()
					g_sNextJobImages.bShowVote = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_END_JOB_VOTES(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	INT i, iToDraw
	BOOL bDidVoteForJob
	REPEAT JOB_PANEL_TOTAL i
		IF IS_JOB_BEING_DISPLAYED(serverVars, i)
			iToDraw = GET_JOB_FOR_DRAWING(i)
			bDidVoteForJob = DID_I_VOTE_FOR_THIS_JOB(i)
			
			BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_GRID_ITEM_VOTE") 
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iToDraw)											// Index
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(serverVars.iJobVotes[i])							// numberOfVotes
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_VOTE_COLOUR(serverVars, i))					// COLOUR
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bDidVoteForJob)									// bShowCheckMark
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(SHOULD_FLASH(nextJobStruct, bDidVoteForJob))		// bFlashBG
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDREPEAT
ENDPROC

PROC DRAW_THE_END_OF_JOBS_PANELS_ONSCREEN(NEXT_JOB_STRUCT& nextJobStruct)
	IF HAS_SCALEFORM_MOVIE_LOADED(nextJobStruct.scaleformJobsMovie)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(nextJobStruct.scaleformJobsMovie, 255, 255, 255, 255)
	ENDIF
ENDPROC

FUNC BOOL HAVE_DESCRIPTIONS_LOADED(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	INT iLoop
	BOOL bReturn = TRUE
	REPEAT NUM_JOBS_TO_GRAB iLoop
		IF REQUEST_AND_LOAD_CACHED_DESCRIPTION(GET_JOB_HASH(serverVars, nextJobStruct, iLoop), g_sCMDLvar[iLoop])
			PRINTLN(" [CS_JOB] HAVE_DESCRIPTIONS_LOADED sTypeCounts.REQUEST_AND_LOAD_CACHED_DESCRIPTION  iLoop = ", iLoop)
		ELSE
			bReturn = FALSE
		ENDIF
	ENDREPEAT
	RETURN bReturn
ENDFUNC

CONST_INT MAX_DESC_SIZE 500

PROC DRAW_SELECTED_MISSION_DETAILS(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	TEXT_LABEL_63 tlJob
	STRING sJobDesc
	STRING sJobName
	STRING sJobDescription
	INT iJobSelected, iToDraw
	iJobSelected	= GET_JOB_PANEL_SELECTED(nextJobStruct)
	INT iArrayShift = GET_ARRAY_SHIFT(serverVars)
	IF DOES_JOB_NEED_IMAGE(iJobSelected)
		sJobDesc 		= UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(GET_JOB_HASH(serverVars, nextJobStruct, iJobSelected + iArrayShift), MAX_DESC_SIZE)
		tlJob 			= GET_JOB_NAME(nextJobStruct, iJobSelected + iArrayShift, IS_BIT_SET(serverVars.iVerifiedBitSet, iJobSelected + iArrayShift))
	//ELIF JOB_IS_REPLAY(iJobSelected)
		//sJobDesc 		= UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(g_FMMC_STRUCT.iMissionDecHash, MAX_DESC_SIZE)
	ELSE
		sJobName 		= GET_DESC_SUB_HEADING(iJobSelected)
		sJobDescription = GET_GTA_ONLINE_JOB_DESCRIPTION(iJobSelected)
	ENDIF
	iToDraw = GET_JOB_FOR_DRAWING(iJobSelected)
	BOOL bHideWhiteSelectionRect = IS_PLAYER_SCTV(PLAYER_ID())
	
	BOOL bFirstTileIsRandom = FALSE
	IF SHOULD_ALTERNATE_IMAGES(serverVars)
		bFirstTileIsRandom = TRUE
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_SELECTION") 	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iToDraw)									// White selection box
		IF DOES_JOB_NEED_IMAGE(iJobSelected)
			IF bFirstTileIsRandom = TRUE
			AND iToDraw = 0
				// "Random"
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NXT_RNDM")
				// "Vote for a random Job."
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NXT_RNDMD")
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlJob)  						// Mission Name
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sJobDesc)						// Description
			ENDIF
		ELIF JOB_IS_REPLAY(iJobSelected)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(g_FMMC_STRUCT.tl63MissionName) // Mission Name
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sJobDesc)					// Description		
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_OFFLN_HD")
				INT iLoop 
				FOR iLoop = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION - 1)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_FMMC_STRUCT.tl63MissionDecription[iLoop])
				ENDFOR			
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sJobName)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sJobDescription)	
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bHideWhiteSelectionRect)					// 1687835
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC STRING GET_JOB_TYPE_NAME(INT iType, INT iSubType = 0, INT iAdversaryModeType = 0)

	STRING sJob

	SWITCH iType
		
		CASE FMMC_TYPE_MISSION   		
			IF IS_ARENA_MODE_FOR_NJVS(iAdversaryModeType)
				sJob = "PM_ARENA"
			ELIF iSubType = 0
				sJob = "FM_NXT_MIS"
			ELIF iSubType = FMMC_TYPE_MISSION_LTS
				sJob = "FM_NXT_LTS"
			ELIF iSubType = FMMC_TYPE_MISSION_CTF
				sJob = "FM_NXT_CTF"
			ELIF iSubType = FMMC_TYPE_MISSION_COOP
				sJob = "FMMC_RSTAR_MCP"
			ELIF iSubType = FMMC_TYPE_MISSION_NEW_VS
				sJob = "PM_INF_QKMT8B"
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH    	
			IF IS_CONTENT_KING_OF_THE_HILL(iType, iSubType)
				IF iSubType = FMMC_KING_OF_THE_HILL_TEAM
					sJob = "FM_MTYPE_TKOTH"
				ELSE
					sJob = "FM_MTYPE_KOTH"	
				ENDIF	
			ELIF IS_ARENA_MODE_FOR_NJVS(iAdversaryModeType)
				sJob = "PM_ARENA"
			ELSE
				sJob = "FM_NXT_DM"
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_BASE_JUMP
		
			sJob = "FM_NXT_PAR"
		BREAK
		
		CASE FMMC_TYPE_RACE   
			IF iSubType = FMMC_RACE_TYPE_STUNT
			OR iSubType = FMMC_RACE_TYPE_STUNT_P2P
			OR iSubType = FMMC_RACE_TYPE_TARGET_P2P
			OR iSubType = FMMC_RACE_TYPE_TARGET_P2P
				sJob = "FMMC_RSTAR_STR"			
			ELIF iSubType = FMMC_RACE_TYPE_ARCADE_RACE
			OR iSubType = FMMC_RACE_TYPE_ARCADE_RACE_P2P
			OR iSubType = FMMC_RACE_TYPE_DESTRUCTION_DERBY
			OR iSubType = FMMC_RACE_TYPE_DESTRUCTION_DERBY_P2P
				sJob = "PM_ARENA"
			ELSE
				sJob = "FM_NXT_RAC"
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_SURVIVAL  
		
			sJob = "FM_NXT_SUR"
		BREAK
	ENDSWITCH 
	
	RETURN sJob	
ENDFUNC

FUNC HUD_COLOURS GET_JOB_ICON_COLOUR(BOOL bUGC)

	IF bUGC
		RETURN HUD_COLOUR_WHITE
	ENDIF

	RETURN HUD_COLOUR_BLUE
ENDFUNC

PROC DRAW_DETAILS_BOTTOM_RIGHT(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	INT iJobSelected	= GET_JOB_PANEL_SELECTED(nextJobStruct)
	INT iArrayShift 	= GET_ARRAY_SHIFT(serverVars) 
	TEXT_LABEL_23 tlCreator 
	INT iType 				
	INT iMinPlayers			
	INT iMaxPlayers 		
	VECTOR vPos				
	STRING sAreaName 
	STRING sJobTypeName
	INT iRating 			
	INT iSubType
	INT iAdversaryModeType
	BOOL bPlayed 	
	BOOL bSocialClubDude
	BOOL bUGCJob 
	BOOL bRandom
	IF IS_PLAYER_LIST_ON(nextJobStruct)
//	OR IS_AMMO_WARNING_SCREEN_ACTIVE(nextJobStruct)
	
		EXIT
	ENDIF
	
	IF DOES_JOB_NEED_IMAGE(iJobSelected)
	OR JOB_IS_REPLAY(iJobSelected)
		IF SHOULD_ALTERNATE_IMAGES(serverVars)	
		AND iJobSelected + iArrayShift = 0
			iType 				= g_FMMC_STRUCT.iMissionType 				
			bRandom				= TRUE
			IF IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()
				iSubType		= FMMC_TYPE_MISSION_NEW_VS
			ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
				iSubType		= FMMC_TYPE_MISSION_LTS
			ELIF Is_Player_Currently_On_MP_Coop_Mission(PLAYER_ID())
				iSubType		= FMMC_TYPE_MISSION_COOP
			ELIF Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
				iSubType		= FMMC_TYPE_MISSION_CTF
			ELIF iType = FMMC_TYPE_RACE
				iSubType		= g_fmmc_struct.iRaceType
			ELIF IS_CONTENT_KING_OF_THE_HILL(iType, g_FMMC_STRUCT.iMissionSubType)

				iSubType		= g_FMMC_STRUCT.iMissionSubType
			ELSE 
				iSubType		= 0
			ENDIF
			tlCreator 		= GET_ROCKSTAR_CREATED_USER_STRING()
			iAdversaryModeType = g_FMMC_STRUCT.iAdversaryModeType 
		ELIF JOB_IS_REPLAY(iJobSelected)
			iType 				= g_FMMC_STRUCT.iMissionType 				
			iMinPlayers			= g_FMMC_STRUCT.iMinNumParticipants	
			iMaxPlayers 		= g_FMMC_STRUCT.iNumParticipants	
			vPos				= g_FMMC_STRUCT.vStartPos 			
			iRating 			= g_FMMC_STRUCT.iRating	
			iAdversaryModeType = g_FMMC_STRUCT.iAdversaryModeType 
			IF IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()
				iSubType		= FMMC_TYPE_MISSION_NEW_VS
			ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
				iSubType		= FMMC_TYPE_MISSION_LTS
			ELIF Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
				iSubType		= FMMC_TYPE_MISSION_CTF
			ELIF Is_Player_Currently_On_MP_Coop_Mission(PLAYER_ID())
				iSubType		= FMMC_TYPE_MISSION_COOP
			ELIF iType = FMMC_TYPE_RACE
				iSubType		= g_fmmc_struct.iRaceType
			ELIF IS_CONTENT_KING_OF_THE_HILL(iType, g_FMMC_STRUCT.iMissionSubType)

				iSubType		= g_FMMC_STRUCT.iMissionSubType
			ELSE
				iSubType		= 0
			ENDIF
			bPlayed				= g_FMMC_STRUCT.bPlayedAlready
			IF IS_CURRENT_MISSION_ROCKSTAR_CREATED()
				tlCreator 		= GET_ROCKSTAR_CREATED_USER_STRING()
			ELIF IS_CURRENT_MISSION_ROCKSTAR_VERIFIED()
			OR IS_CURRENT_MISSION_ROCKSTAR_COMMUNITY()
				IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl31VerifiedMissionCreator)
					IF IS_CURRENT_MISSION_ROCKSTAR_VERIFIED()
						tlCreator 	= GET_ROCKSTAR_VERIFIED_USER_STRING() 
					ELSE
						tlCreator 	= GET_ROCKSTAR_COMMUNITY_USER_STRING()
					ENDIF
				ELSE
					tlCreator	= g_FMMC_STRUCT.tl31VerifiedMissionCreator
				ENDIF
			ELSE
				tlCreator 		= g_FMMC_STRUCT.tl63MissionCreator
				bUGCJob			= TRUE
			ENDIF
			bSocialClubDude     = IS_SOCIAL_CLUB_CREATOR(nextJobStruct, (iJobSelected + iArrayShift), TRUE)
		ELSE
			iType 				= GET_JOB_TYPE(serverVars, nextJobStruct, iJobSelected + iArrayShift)
			iMinPlayers			= GET_JOB_MIN_PLAYERS(serverVars, nextJobStruct, iJobSelected + iArrayShift)
			iMaxPlayers 		= GET_JOB_MAX_PLAYERS(serverVars, nextJobStruct, iJobSelected + iArrayShift)
			vPos				= GET_JOB_START_POS(serverVars, nextJobStruct, iJobSelected + iArrayShift)
			iRating 			= GET_JOB_RATING(serverVars, nextJobStruct, iJobSelected + iArrayShift)
			iSubType 			= GET_EOJV_MISSION_SUB_TYPE(serverVars, nextJobStruct, iJobSelected + iArrayShift)
			iAdversaryModeType = GET_JOB_START_ADVERSARY_MODE_TYPE(serverVars, nextJobStruct, iJobSelected + iArrayShift)			
			bPlayed				= GET_JOB_PLAYED_STATUS(serverVars, nextJobStruct, iJobSelected + iArrayShift)
			tlCreator 			= GET_JOB_CREATOR(serverVars, nextJobStruct, (iJobSelected + iArrayShift))
			bSocialClubDude     = IS_SOCIAL_CLUB_CREATOR(nextJobStruct, (iJobSelected + iArrayShift), FALSE)
		ENDIF
		IF bRandom
			sAreaName			= "FM_NEXT_RAND_A"
		ELSE
			sAreaName 			= GET_NAME_OF_ZONE(vPos) 
		ENDIF
		PRINTLN(" [CS JOB] GET_EOJV_MISSION_SUB_TYPE - iSubType = ", iSubType)	
		sJobTypeName 			= GET_JOB_TYPE_NAME(iType, iSubType, iAdversaryModeType)
		
		// Rating	
		BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_DETAILS_ITEM")  
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NXT_RAT") 		// rating
			IF bRandom
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NEXT_RAND_R") 
			ELIF iRating = -1
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NXT_NOT") 
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_NXT_RAT1")		// Players 1-8									
					ADD_TEXT_COMPONENT_INTEGER(iRating)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_DETAILS_ITEM")  
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NXT_CRE")		// created by
			IF IS_STRING_NULL_OR_EMPTY(tlCreator)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_THUM_ROCK")	
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlCreator)	
			ENDIF
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")					// Not used crew tag
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bSocialClubDude)		// 1830853 Social Club icon
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_DETAILS_ITEM")  
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)		
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NXT_PLY")		// players
			IF bRandom
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NEXT_RAND_P")	
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("LBD_NUM")				// Players 1/8									
					ADD_TEXT_COMPONENT_INTEGER(iMinPlayers)
					ADD_TEXT_COMPONENT_INTEGER(iMaxPlayers)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_DETAILS_ITEM")  
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3) 											// detailsItemIndex
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) 											// 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) 											// 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2) 											// type
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) 											// 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)												// 0	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NXT_TYP")								// textOnLeft
			IF IS_STRING_NULL_OR_EMPTY(sJobTypeName)
				PRINTLN("[1953388] IS_STRING_NULL_OR_EMPTY(sJobTypeName), iType = ", iType, " iSubType = ", iSubType)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sJobTypeName)							// textOnRight
			ENDIF
			PRINTLN("SET_DETAILS_ITEM, GET_JOB_TYPE_ICON = ", GET_JOB_TYPE_ICON(iType, iSubType, iAdversaryModeType), " iType = ", iType, " iSubType = ", iSubType, " iAdversaryModeType = ", iAdversaryModeType)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_JOB_TYPE_ICON(iType, iSubType, iAdversaryModeType))			// INT iconEnum/STRING crewTagString
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_JOB_ICON_COLOUR(bUGCJob)))		// HudColourEnum iconColour
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bPlayed)										// Have I played this?
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_DETAILS_ITEM")  
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)		
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_NXT_ARA")		// area
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sAreaName)	
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

//		LOBBY LIST																
//╚═════════════════════════════════════════════════════════════════════════════╝	

FUNC BOOL SHOULD_LIST_UPDATE(NEXT_JOB_STRUCT& nextJobStruct)
	IF NOT HAS_NET_TIMER_STARTED(nextJobStruct.sTimerDpad)
		START_NET_TIMER(nextJobStruct.sTimerDpad)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(nextJobStruct.sTimerDpad, 1000)
			REINIT_NET_TIMER(nextJobStruct.sTimerDpad)
			
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_VOTED_FOR_A_JOB(INT iPlayer)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bVotedForAJob)
ENDFUNC

FUNC STRING GET_VOTED_TEXT(BOOL bVoted)
	STRING sReturn
	IF bVoted
		sReturn = "FM_NXT_VOTED"
	ENDIF

	RETURN sReturn
ENDFUNC

FUNC INT GET_MIC_OR_RANK(BOOL bSpeaking)
	INT iReturn
	
	iReturn = ENUM_TO_INT(ICON_RANK_FREEMODE)
	
	IF bSpeaking
		iReturn = ENUM_TO_INT(ACTIVE_HEADSET)
	ELSE
		iReturn = ENUM_TO_INT(ICON_RANK_FREEMODE)
	ENDIF
	
	RETURN iReturn
ENDFUNC

FUNC INT GET_SPECTATOR_EYE(BOOL bSpectator)
	INT iReturn
	
	IF bSpectator
		iReturn = ENUM_TO_INT(SPECTATOR)
	ENDIF
	
	RETURN iReturn
ENDFUNC

PROC SET_PLAYER_LIST_ROW(NEXT_JOB_STRUCT& nextJobStruct, INT iRow, STRING tlGamertag, STRING strHeadshotTxd, INT iOnlineRank, BOOL bSpeaking, BOOL bVoted, BOOL bSpectator, BOOL bRefreshList = FALSE)
	STRING strMethodName 
	
	UNUSED_PARAMETER(bRefreshList)
	
	// 2059294
	IF IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_LOBBY_INIT)
	AND NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_LOBBY_INIT_DONE)
		strMethodName = "SET_LOBBY_LIST_DATA_SLOT"
	ELSE	
		IF SHOULD_LIST_UPDATE(nextJobStruct)
			PRINTLN("[CS_JOB] PLAYER_ROWS, UPDATE_LOBBY_LIST_DATA_SLOT ")
			strMethodName = "UPDATE_LOBBY_LIST_DATA_SLOT" // Updating
		ELSE
			strMethodName = "SET_LOBBY_LIST_DATA_SLOT"
		ENDIF
	ENDIF

	STRING sName = GET_VOTED_TEXT(bVoted)
	BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, strMethodName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRow)									// iRowIndex, 					
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// iMenuID, 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// iUniqueID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)										// iType
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iOnlineRank)							// rank number
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)									// iIsSelectable
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlGamertag)					// Gamertag
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_FREEMODE))		// Colour
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)								// *** SHOW TAB NEXT TO GAMERTAG ? ***
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_SPECTATOR_EYE(bSpectator))			// Spectator eyeballs
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_MIC_OR_RANK(bSpeaking))			// INT iconEnumForRankOrMic, 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// *** DEPRECATED ***
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")							// crewtags! "" for blank
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// *** NUMBER OF KICK VOTES ****
		IF IS_STRING_NULL_OR_EMPTY(sName)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")						
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sName)							// "VOTED"
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_FREEMODE))		// HudColour votedBGColour, 
		IF IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	
//			PRINTLN("TEST_BUG_CS")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)	// NEW_avatarTXD
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)	// NEW_avatarTXN
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC LOBBY_LIST_VISIBLE(NEXT_JOB_STRUCT& nextJobStruct, BOOL bVisible)
	BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_LOBBY_LIST_VISIBILITY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVisible)	
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC CLEAR_LOBBY_LIST(NEXT_JOB_STRUCT& nextJobStruct)
	BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_LOBBY_LIST_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC BOOL PLAYER_ROWS(NEXT_JOB_STRUCT& nextJobStruct, BOOL bRefreshList = FALSE)
	
	INT i, iCount, iPlayer, iRank
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX playerId
	STRING sPlayerName[NUM_NETWORK_PLAYERS]
//	GAMER_HANDLE ghPlayer
//	TEXT_LABEL_15 tlCrewTag
	STRING strHeadshotTxd
	PEDHEADSHOT_ID pedHeadshot
	BOOL bSpeaking
	BOOL bVoted
	BOOL bDrawPlayerList
	BOOL bSpectator
	GAMER_HANDLE tempHandle
	
	IF bRefreshList
		PRINTLN(" [CS JOB] PLAYER_ROWS - Refresh list")
		CLEAR_LOBBY_LIST(nextJobStruct)
	ENDIF
	
	IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_CALL_SET_UPDATE)
	OR bRefreshList
		bDrawPlayerList = TRUE
	ENDIF
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		tempPart = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
		
			playerId = NETWORK_GET_PLAYER_INDEX(tempPart)

			bSpeaking = NETWORK_IS_PLAYER_TALKING(playerId)
			// Names
			sPlayerName[i] = GET_PLAYER_NAME(playerId)
			// Tags
//			ghPlayer = GET_GAMER_HANDLE_PLAYER(playerId)
//			tlCrewTag = ""
			iPlayer = NATIVE_TO_INT(playerId)
//			IF IS_GAMER_HANDLE_VALID(ghPlayer)
//				GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(ghPlayer, tlCrewTag)
//			ENDIF
			
			// Attempt to get player headshot
			pedHeadshot = Get_HeadshotID_For_Player(playerId)
			strHeadshotTxd = ""
			IF pedHeadshot <> NULL
				strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
//				PRINTLN("[CS_JOB] OK strHeadshotTxd = ", strHeadshotTxd)
			ELSE
//				PRINTLN("[CS_JOB] NULL strHeadshotTxd = ", strHeadshotTxd)
			ENDIF
			
			iRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
			
			bVoted = HAS_PLAYER_VOTED_FOR_A_JOB(iPlayer)
			
//			#IF IS_DEBUG_BUILD
//			PRINTLN("[CS_JOB]  i = ", i, " sPlayerName = ", sPlayerName[i])
//			IF bVoted 
//				PRINTLN("[CS_JOB] bVoted = TRUE iPlayer = ", iPlayer)
//			ELSE
//				PRINTLN("[CS_JOB] bVoted = FALSE iPlayer = ", iPlayer)
//			ENDIF
//			IF bSpeaking
//				PRINTLN("[CS_JOB] bSpeaking = TRUE")
//			ELSE
//				PRINTLN("[CS_JOB] bSpeaking = FALSE")
//			ENDIF
//			#ENDIF

			bSpectator = DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
						
			SET_PLAYER_LIST_ROW(nextJobStruct, iCount, sPlayerName[i], strHeadshotTxd, iRank, bSpeaking, bVoted, bSpectator, bRefreshList) // UPDATE_LOBBY_LIST_DATA_SLOT
			
			// Show profiles
			IF HAS_PLAYER_CARD_BUTTON_BEEN_PRESSED()
				IF nextJobStruct.iRowSelected = i
					tempHandle = GET_GAMER_HANDLE_PLAYER(PlayerId)
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					NETWORK_SHOW_PROFILE_UI(tempHandle)
				ENDIF
			ENDIF
										
			iCount++
		ELSE
			IF NOT bRefreshList
				IF IS_BIT_SET(nextJobStruct.iPlayerActiveBitSet, i)
					PRINTLN("[CS_JOB] PLAYER_ROWS - Player has become inactive. Hard Refresh")
					
					CLEAR_BIT(nextJobStruct.iPlayerActiveBitSet, i)
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Count players
	IF nextJobStruct.iPlayerListCount <> iCount
		nextJobStruct.iPlayerListCount = iCount 
	ENDIF

	// Draw
	IF bDrawPlayerList
		IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_DISPLAY_VIEW)
			BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "DISPLAY_LOBBY_LIST_VIEW") //DISPLAY_LOBBY_LIST_VIEW
			END_SCALEFORM_MOVIE_METHOD()
			PRINTLN("[CS_JOB] PLAYER_ROWS - CLIENT_BOOL_DISPLAY_VIEW")
			SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_DISPLAY_VIEW)
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║		SCROLL NG 32															║

//scroll types
//public static var SCROLL_TYPE_ALL:Number 			= 0; 
//public static var SCROLL_TYPE_UP_DOWN:Number 		= 1; 
//public static var SCROLL_TYPE_LEFT_RIGHT:Number 	= 2; 
//public static var SCROLL_TYPE_NONE:Number 		= 3;
//arrow positions
//public static var POSITION_ARROW_LEFT:Number 		= 0; 
//public static var POSITION_ARROW_CENTER:Number 	= 1; 
//public static var POSITION_ARROW_RIGHT:Number 	= 2;

PROC INITIALISE_SCROLL(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct) 
	IF serverVars.iNumberOnJob > 16
		IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_INIT_SCROLL)
			BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "INIT_LOBBY_LIST_SCROLL")					
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) 					// PM_ScrollBase.SCROLL_TYPE_UP_DOWN,
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) 					// PM_ScrollBase.POSITION_ARROW_CENTER
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			END_SCALEFORM_MOVIE_METHOD()
			PRINTLN(" [CS_JOB] [SCROLL] CLIENT_BOOL_INIT_SCROLL, TRUE, iNumberOnJob = ",serverVars.iNumberOnJob)
			SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_INIT_SCROLL)
		ENDIF
	ELSE
		PRINTLN(" [CS_JOB] [SCROLL] CLIENT_BOOL_INIT_SCROLL, FALSE, iNumberOnJob = ",serverVars.iNumberOnJob)
	ENDIF
ENDPROC

PROC DO_SCROLLING(NEXT_JOB_STRUCT& nextJobStruct)
	BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_LOBBY_LIST_SCROLL")					
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) 
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCROLL_HIGHLIGHT_PLAYER(NEXT_JOB_STRUCT& nextJobStruct)
	BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_LOBBY_LIST_HIGHLIGHT")					
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(nextJobStruct.iRowSelected)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC NEXT_GEN_SCROLL_PLAYER_LIST(NEXT_JOB_STRUCT& nextJobStruct)
	DO_SCROLLING(nextJobStruct)
	SCROLL_HIGHLIGHT_PLAYER(nextJobStruct)
ENDPROC

//╚═════════════════════════════════════════════════════════════════════════════╝	

PROC DRAW_PLAYER_LIST(NEXT_JOB_STRUCT& nextJobStruct)
	IF IS_PLAYER_LIST_ON(nextJobStruct)
	
		IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_LOBBY_INIT)
			SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_LOBBY_INIT)
		ENDIF
	
		// Populate the players rows.
		IF NOT PLAYER_ROWS(nextJobStruct)
		
			//...update to players, refresh the list, now!
			PRINTLN("[CS_JOB] PLAYER_ROWS - Change in players, refresh list")
			CLEAR_LOBBY_LIST(nextJobStruct)
			PLAYER_ROWS(nextJobStruct, TRUE)
		ENDIF
		
		IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_LOBBY_LIST)
			LOBBY_LIST_VISIBLE(nextJobStruct, TRUE)
			SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_LOBBY_LIST)
		ENDIF
		
		NEXT_GEN_SCROLL_PLAYER_LIST(nextJobStruct)		
	ELSE
		IF IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_LOBBY_INIT)
			SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_LOBBY_INIT_DONE)
		ENDIF
		IF IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_CALL_SET_UPDATE)
			CLEAR_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_CALL_SET_UPDATE)
		ENDIF
		IF IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_LOBBY_LIST)
			CLEAR_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_LOBBY_LIST)
		ENDIF
		LOBBY_LIST_VISIBLE(nextJobStruct, FALSE)
	ENDIF
ENDPROC

PROC DRAW_END_JOB_SELECTION(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
//	DRAW_PLAYER_LIST(nextJobStruct)
	DRAW_SELECTED_MISSION_DETAILS(serverVars, nextJobStruct)
	DRAW_DETAILS_BOTTOM_RIGHT(serverVars, nextJobStruct) 
ENDPROC

PROC SET_UP_END_JOB_SCREENS(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, BOOL bDoScreens, BOOL bFirstTileOnly)
	DRAW_END_JOB_TITLE(serverVars, nextJobStruct)
	IF bDoScreens
		DRAW_IMAGES(serverVars, nextJobStruct, bFirstTileOnly)
	ENDIF
	DRAW_END_JOB_SELECTION(serverVars, nextJobStruct) //needs to be called so the selected rect is not top left
	DRAW_END_JOB_VOTES(serverVars, nextJobStruct)  
ENDPROC

PROC REFRESH_VOTES(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, BOOL bFirstTileOnly)
	IF nextJobStruct.iRefreshValueLocal <> serverVars.iRefreshValue
		IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_UPDATE)
			SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_UPDATE)
		ENDIF
		SET_UP_END_JOB_SCREENS(serverVars, nextJobStruct, FALSE, bFirstTileOnly)
		nextJobStruct.iRefreshValueLocal = serverVars.iRefreshValue
	ELSE
		IF IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_UPDATE)
			CLEAR_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_UPDATE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_ALLOW_VOTE_FOR_PLAYER(SERVER_NEXT_JOB_VARS& serverVars, INT iJobPanel)
	IF ON_LAST_PAGE_REFRESHED(serverVars)
	OR RAN_OUT_OF_REFRESHES(serverVars) 
	OR CANT_REFRESH_YET(serverVars)
		IF iJobPanel = JOB_PANEL_REFRESH
		
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	
		RETURN FALSE
	ENDIF	

	RETURN TRUE
ENDFUNC

PROC VOTE_FOR_THIS_JOB(SERVER_NEXT_JOB_VARS& serverVars, INT iJobPanel)

	INT iArrayShift = GET_ARRAY_SHIFT(serverVars)
	IF IS_SPECIAL_JOB(iJobPanel)
		iArrayShift = 0
	ENDIF
	
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())

	IF SHOULD_ALLOW_VOTE_FOR_PLAYER(serverVars, iJobPanel)
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayer].iJobVote, iJobPanel + iArrayShift)
			PRINTLN(" [CS_JOB] VOTE_FOR_THIS_JOB  ", iJobPanel + iArrayShift, " iPlayer = ", iPlayer)
			//GlobalplayerBD_FM[iPlayer].bVotedForAJob = TRUE
			SET_BIT(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bVotedForAJob)
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")	
			SET_BIT(GlobalplayerBD_FM[iPlayer].iJobVote, iJobPanel + iArrayShift)
			BROADCAST_VOTED_FOR_JOB(iJobPanel + iArrayShift, TRUE)
		ELSE
			PRINTLN(" [CS_JOB] UN_VOTE_THIS_JOB  ", iJobPanel + iArrayShift)
			//GlobalplayerBD_FM[iPlayer].bVotedForAJob = FALSE
			CLEAR_BIT(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bVotedForAJob)
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")	
			CLEAR_BIT(GlobalplayerBD_FM[iPlayer].iJobVote, iJobPanel + iArrayShift)	
			BROADCAST_VOTED_FOR_JOB(iJobPanel + iArrayShift, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC REFRESH_SCREENS_NOW(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, BOOL bFirstTileOnly)
	SET_UP_END_JOB_SCREENS(serverVars, nextJobStruct, TRUE, bFirstTileOnly)
ENDPROC

PROC SET_BITSET_OF_END_VOTE_PLAYERS(NEXT_JOB_STRUCT& nextJobStruct)

	INT i
	PARTICIPANT_INDEX tempPart
	
	PRINTLN(" [CS_JOB] SET_BITSET_OF_END_VOTE_PLAYERS *********************************")
	
	// Reset our bitset
	nextJobStruct.iPlayerActiveBitSet = 0
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		tempPart = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
		
			SET_BIT(nextJobStruct.iPlayerActiveBitSet, i)
			PRINTLN(" [CS_JOB] SET_BITSET_OF_END_VOTE_PLAYERS - Player: ", GET_PLAYER_NAME( NETWORK_GET_PLAYER_INDEX(tempPart)), " is active on end vote screen")
		ENDIF
	ENDREPEAT

ENDPROC

/// PURPOSE:
///    Sets the mouse highlight around the specified slot
/// PARAMS:
///    iUID - slot to set
///    bToggleHoverHighlightOn - TRUE will set the slot hover to ON
PROC SET_MOUSE_HOVER_HIGHLIGHT_AROUND_SLOT(NEXT_JOB_STRUCT& nextJobStruct, INT iUID, BOOL bToggleHoverHighlightOn)

	IF iUID > -1
	AND iUID < 9
		PRINTLN(" [CS_JOB] SET_MOUSE_HOVER_HIGHLIGHT_AROUND_SLOT - iUID: ", iUID, " set to ", bToggleHoverHighlightOn)
		BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_HOVER")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUID)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(!bToggleHoverHighlightOn) // Oddly, the scaleform sets the hover highlight ON when the supplied bool is FALSE
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

ENDPROC

/// PURPOSE:
///    Sets the greyed-out status of the specified slot
/// PARAMS:
///    iUID - slot to set
///    bToggleGreyedOutOn - TRUE will set the slot greyed out status to ON
PROC SET_NEXT_JOB_SLOTS_GREYED_OUT(NEXT_JOB_STRUCT& nextJobStruct, BOOL bToggleGreyedOutOn)

	PRINTLN(" [CS_JOB] SET_NEXT_JOB_SLOTS_GREYED_OUT - set to ", bToggleGreyedOutOn)
	BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "SET_ITEMS_GREYED_OUT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bToggleGreyedOutOn)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

FUNC BOOL HAS_USER_PRESSED_MOUSE_ON_NEXT_JOB_WHEN_PLAYER_LIST_IS_DISPLAYED(NEXT_JOB_STRUCT& nextJobStruct)

	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	OR NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_PLAYER_LIST_ON)
		RETURN FALSE
	ENDIF
	
	SET_MOUSE_CURSOR_THIS_FRAME() 
	
	eMOUSE_EVT eMouseEvent
	INT iUID
	INT iContext
	
	IF HAS_SCALEFORM_MOVIE_LOADED(nextJobStruct.scaleformJobsMovie)
	AND GET_MOUSE_EVENT(nextJobStruct.scaleformJobsMovie, eMouseEvent, iUID, iContext)
		IF eMouseEvent = EVENT_TYPE_MOUSE_PRESS
		AND iUID > -1
		AND iUID < 9
			nextJobStruct.iCurrentMouseHoverHighlightId = iUID
			SET_MOUSE_HOVER_HIGHLIGHT_AROUND_SLOT(nextJobStruct, nextJobStruct.iCurrentMouseHoverHighlightId, TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE

ENDFUNC

PROC PLAYER_LIST_TOGGLE(NEXT_JOB_STRUCT& nextJobStruct)
	IF HAS_END_VOTE_PLAYER_LIST_BUTTON_BEEN_PRESSED() 
	OR HAS_USER_PRESSED_MOUSE_ON_NEXT_JOB_WHEN_PLAYER_LIST_IS_DISPLAYED(nextJobStruct)
	
		PLAY_SOUND_FRONTEND(-1, "LEADER_BOARD", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		IF IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_PLAYER_LIST_ON)
			PRINTLN(" [CS_JOB] PLAYER_LIST_TOGGLE - FALSE  ")
			nextJobStruct.iRefreshValueLocal++
			CLEAR_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_PLAYER_LIST_ON)
			SET_NEXT_JOB_SLOTS_GREYED_OUT(nextJobStruct, FALSE)
		ELSE
			PRINTLN(" [CS_JOB] PLAYER_LIST_TOGGLE - TRUE  ")
			nextJobStruct.iRefreshValueLocal++
			SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_PLAYER_LIST_ON)
			
			SET_BITSET_OF_END_VOTE_PLAYERS(nextJobStruct)
			
			SET_MOUSE_HOVER_HIGHLIGHT_AROUND_SLOT(nextJobStruct, nextJobStruct.iCurrentMouseHoverHighlightId, FALSE)
			SET_NEXT_JOB_SLOTS_GREYED_OUT(nextJobStruct, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC QUIT_SCREEN_TOGGLE_ON(NEXT_JOB_STRUCT& nextJobStruct)

	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF

//	IF NOT IS_AMMO_WARNING_SCREEN_ACTIVE(nextJobStruct)	
		IF HAS_END_JOB_QUIT_BEEN_PRESSED()	
			IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_QUIT_SCREEN_ON)
				PRINTLN(" [CS_JOB] QUIT_SCREEN_TOGGLE_ON - TRUE  ")
				SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_QUIT_SCREEN_ON)
			ENDIF
			IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_QUIT_SOUND_ONCE)
				PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_QUIT_SOUND_ONCE)
			ENDIF
		ENDIF
//	ENDIF
ENDPROC

PROC QUIT_SCREEN_TOGGLE_OFF(NEXT_JOB_STRUCT& nextJobStruct)

	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF

	IF HAS_END_JOB_QUIT_BEEN_PRESSED()
		IF IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_QUIT_SCREEN_ON)
			PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			PRINTLN(" [CS_JOB] QUIT_SCREEN_TOGGLE_OFF - FALSE  ")
			CLEAR_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_QUIT_SCREEN_ON)
		ENDIF
	ENDIF
ENDPROC

////╔═════════════════════════════════════════════════════════════════════════════╗
////║		ARMOUR																	║
////╚═════════════════════════════════════════════════════════════════════════════╝	
//
//PROC INITIAL_ARMOUR_CHECK(NEXT_JOB_STRUCT& nextJobStruct)
//	INT iArmorLevel = GET_MP_MAX_UNLOCKED_ARMOR_LEVEL()
//	IF iArmorLevel >= 0
//
//		GUNCLUB_WEAPON_DATA_STRUCT sWeaponData
//		GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(WEAPONTYPE_UNARMED, sWeaponData, iArmorLevel)	// fetches armor data
//		
//		INT iArmorToGive = FLOOR((TO_FLOAT(GET_PLAYER_MAX_ARMOUR(PLAYER_ID()))/100.0)*sWeaponData.iDefaultClipSize)
//
//		INT iArmorCost, iDummy1
//		IF GET_GUNSHOP_WEAPON_COST(WEAPONTYPE_UNARMED, iArmorCost, iDummy1, iArmorLevel)
//		
//			IF GET_PED_ARMOUR(PLAYER_PED_ID()) >= iArmorToGive
//			
//				PRINTLN("[CS_JOB] [ammo/armour] INITIAL_ARMOUR_CHECK - Unable to purchase armor, ARMOR FULL")
//				SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_BLOCK_ARMOUR)
//			
//			ELIF NOT NETWORK_CAN_SPEND_MONEY(iArmorCost, FALSE, TRUE, FALSE)
//			
//				PRINTLN("[CS_JOB] [ammo/armour]  INITIAL_ARMOUR_CHECK - Unable to purchase armor, CANT AFFORD IT, NETWORK_CAN_SPEND_MONEY: ", NETWORK_CAN_SPEND_MONEY(iArmorCost, FALSE, TRUE, FALSE))
//				
//				SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_BLOCK_ARMOUR)
//			ENDIF
//		ELSE
//			PRINTLN("[CS_JOB] [ammo/armour]  INITIAL_ARMOUR_CHECK - Unable to purchase armor, could not find cost")
//		ENDIF
//	ELSE
//		PRINTLN("[CS_JOB] [ammo/armour]  INITIAL_ARMOUR_CHECK - Unable to purchase armor, armor not currently unlocked")
//	ENDIF
//ENDPROC
//
//PROC PROCESS_PLAYER_PURCHASING_ARMOR(NEXT_JOB_STRUCT& nextJobStruct)
//
//	INT iArmorLevel = GET_MP_MAX_UNLOCKED_ARMOR_LEVEL()
//
//	GUNCLUB_WEAPON_DATA_STRUCT sWeaponData
//	GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(WEAPONTYPE_UNARMED, sWeaponData, iArmorLevel)	// fetches armor data
//		
//	INT iArmorToGive = FLOOR((TO_FLOAT(GET_PLAYER_MAX_ARMOUR(PLAYER_ID()))/100.0)*sWeaponData.iDefaultClipSize)
//
//	INT iArmorCost
//
//	PRINTLN("[CS_JOB] [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR - Player Cash $", GET_PLAYER_CASH(PLAYER_ID()), " and Armor Cost: $", iArmorCost)
//		
//	SET_PED_ARMOUR(PLAYER_PED_ID(), iArmorToGive)
//	
//	BOOL fromBank = (NETWORK_GET_VC_BANK_BALANCE() > 0) 
//	BOOL fromBankAndWallet = (NETWORK_GET_VC_BANK_BALANCE() < iArmorCost)
//	
//	TEXT_LABEL_15 tlArmorName = GET_MP_ARMOR_NAME(iArmorLevel)
//	NETWORK_BUY_ITEM(iArmorCost, GET_HASH_KEY(tlArmorName), PURCHASE_ARMOR, DEFAULT, fromBank, tlArmorName, GET_HASH_KEY("SHOP_NJVS"), ENUM_TO_INT(WEAPONTYPE_UNARMED), DEFAULT, fromBankAndWallet) 
//		
//	PRINTLN("[CS_JOB] [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR - Purchased - Remaining Cash: $", GET_PLAYER_CASH(PLAYER_ID()))
//	
//	// Put the armor clothing component on
//	INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_ARMOUR)
//	IF iStat = 1
//		PED_COMP_NAME_ENUM eBodyArmour
//		eBodyArmour = GET_BODY_ARMOUR_FOR_JBIB(GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB))//, sWeaponData.iDefaultClipSize)
//		IF eBodyArmour != SPECIAL2_FMM_0_0
//		    eBodyArmour += INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_BODY_ARMOUR_FROM_CLIP_SIZE(sWeaponData.iDefaultClipSize))
//		ENDIF
//		PRINTLN("[CS_JOB] [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR Player bought body armour, equiping = ", eBodyArmour)
//		
//		// Local ped
//		SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, eBodyArmour, FALSE)
//		// Local ped clone
//		SET_PED_COMP_ITEM_CURRENT_MP(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID, COMP_TYPE_SPECIAL2, eBodyArmour, FALSE)
//	ELSE
//		PRINTLN("[CS_JOB] [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR Player bought body armour, don't auto-equip ")
//	ENDIF
//	
//	PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
//	
//	SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_BLOCK_ARMOUR)
//	REFRESH_BUTTONS(nextJobStruct)
//ENDPROC
//
//PROC HANDLE_BUYING_ARMOUR(NEXT_JOB_STRUCT& nextJobStruct)
//	IF ARMOUR_AVAILABLE(nextJobStruct)
//		INITIAL_ARMOUR_CHECK(nextJobStruct)
//		IF HAS_END_JOB_BUY_ARMOUR_BEEN_PRESSED()
//		
//			PROCESS_PLAYER_PURCHASING_ARMOR(nextJobStruct)
//		ENDIF
//	ENDIF
//ENDPROC
//
////╔═════════════════════════════════════════════════════════════════════════════╗
////║		AMMO																	║
////╚═════════════════════════════════════════════════════════════════════════════╝	
//PROC COUNT_AMMO_COST(NEXT_JOB_STRUCT& nextJobStruct)
//	
//	INT iBSCheckedAmmoTypes
//	
//	INT iWeaponSlot
//	INT iWeaponAmmoCost
//	WEAPON_TYPE aWeapon
//	
//	nextJobStruct.iTotalCostToFillAmmo = 0
//	
//	// Loop over our normal weapons
//	FOR iWeaponSlot = 0 TO (ENUM_TO_INT(NUM_WEAPONSLOTS)-1) STEP 1
//		
//		// Get the weapon type from the slot
//		IF GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot) = WEAPONSLOT_INVALID
//			aWeapon = WEAPONTYPE_INVALID
//		ELSE
//			aWeapon = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot))
//		ENDIF
//		
//		// Is the weapon valid for the corona?
//		IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType)
//			
//			//...yes, get the total cost for ammo type
//			IF GET_CORONA_WEAPON_TOTAL_AMMO_COST(aWeapon, iBSCheckedAmmoTypes, iWeaponAmmoCost)
//				
//				//...yes, add the total cost to overall value
//				nextJobStruct.iTotalCostToFillAmmo += iWeaponAmmoCost
//						
//				PRINTLN("[CS_JOB] [ammo/armour] COUNT_AMMO_COST			- Current Total Cost: ", nextJobStruct.iTotalCostToFillAmmo)
//			ENDIF
//		ENDIF
//	ENDFOR
//	
//	// Process our DLC weapons in the same way	
//	scrShopWeaponData sWeaponData
//	FOR iWeaponSlot = 0 TO (GET_NUM_DLC_WEAPONS()-1) STEP 1
//		
//		IF GET_DLC_WEAPON_DATA(iWeaponSlot, sWeaponData)
//				
//			IF IS_CORONA_WEAPON_VALID(INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash), g_FMMC_STRUCT.iMissionType)
//				
//				IF GET_CORONA_WEAPON_TOTAL_AMMO_COST(INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash), iBSCheckedAmmoTypes, iWeaponAmmoCost)
//				
//					//...yes, add the total cost to overall value
//					nextJobStruct.iTotalCostToFillAmmo += iWeaponAmmoCost
//							
//					PRINTLN("[CS_JOB] [ammo/armour] COUNT_AMMO_COST			- Current Total Cost: ", nextJobStruct.iTotalCostToFillAmmo)
//				ENDIF
//					
//			ENDIF
//		ENDIF
//	ENDFOR
//	
//	IF nextJobStruct.iTotalCostToFillAmmo <= 0
//		SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_BLOCK_AMMO)
//		PRINTLN("[CS_JOB] [ammo/armour] COUNT_AMMO_COST - CLIENT_BOOL_BLOCK_AMMO ")
//	ENDIF
//	
//	PRINTLN("[CS_JOB] [ammo/armour] COUNT_AMMO_COST - Total Cost: ", nextJobStruct.iTotalCostToFillAmmo)
//ENDPROC
//
//FUNC BOOL PROCESS_PURCHASING_AMMO(NEXT_JOB_STRUCT& nextJobStruct, WEAPON_TYPE aStartingWeapon)
//
//	IF CAN_PLAYER_PURCHASE_CORONA_AMMO_FOR_WEAPON(aStartingWeapon)
//	
//		GUNCLUB_WEAPON_DATA_STRUCT	sWeaponStruct
//			
//		IF GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(aStartingWeapon, sWeaponStruct)
//		
//			// Make sure weapon is valid and has an ammo type
//			IF sWeaponStruct.eType != ITEM_TYPE_INVALID
//			AND sWeaponStruct.eWeapon != WEAPONTYPE_INVALID
//			AND sWeaponStruct.eAmmoType != INT_TO_ENUM(AMMO_TYPE, 0)
//
//				INT iAmmoCount = sWeaponStruct.iDefaultClipSize
//
//				IF IS_CORONA_WEAPON_THROWN(aStartingWeapon)
//					PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Weapon is a thrown type. Give 1 ammo")
//					iAmmoCount = 1
//					
//				ELIF sWeaponStruct.eType = ITEM_TYPE_GUN
//					PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Weapon is ITEM_TYPE_GUN. Give 2 * default clip size")
//					iAmmoCount *= 2 // 2 clips for a gun
//				ENDIF
//
//				INT iWeaponCost
//				INT iAmmoCost
//				INT iMaxAmmo
//				
//				GET_GUNSHOP_WEAPON_COST(aStartingWeapon, iWeaponCost, iAmmoCost)
//
//				iAmmoCost 	= ROUND(TO_FLOAT(iAmmoCost) * GET_AMMO_MULTIPLIER_FOR_FULL_AMMO(sWeaponStruct.eWeapon, iAmmoCount))
//				iAmmoCount	= ROUND(TO_FLOAT(iAmmoCount) * GET_AMMO_MULTIPLIER_FOR_FULL_AMMO(sWeaponStruct.eWeapon, iAmmoCount))
//				
//				IF GET_MAX_AMMO(PLAYER_PED_ID(), sWeaponStruct.eWeapon, iMaxAmmo)
//					IF iMaxAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), sWeaponStruct.eWeapon)
//						PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Weapon is FULL. Set iAmmoCost = 0")
//						iAmmoCost = 0							
//					ENDIF
//				ENDIF
//				
//				IF iAmmoCost > 0
//
//					BOOL fromBank = (NETWORK_GET_VC_BANK_BALANCE() > 0) 
//					BOOL fromBankAndWallet = (NETWORK_GET_VC_BANK_BALANCE() < iAmmoCost)
//
//					IF GET_CORONA_CASH_AVAILABLE() >= iAmmoCost
//					AND NETWORK_CAN_SPEND_MONEY(iAmmoCost, fromBank, fromBankAndWallet, FALSE)
//	
//						ADD_AMMO_TO_PED(PLAYER_PED_ID(), aStartingWeapon, iAmmoCount)
//						CHECK_AND_SAVE_WEAPON(aStartingWeapon)
//														
//						PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Telemetry: 	Player Cash: 		$", GET_PLAYER_CASH(PLAYER_ID()))
//						PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - 				- amount: 			$", iAmmoCost)
//						PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - 				- itemHash:			", ENUM_TO_INT(aStartingWeapon))
//						PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - 				- extra1:			", iAmmoCount)
//						
//						PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - 				- itemIdentifier:	FM_COR_AMMO_FILL")
//						NETWORK_BUY_ITEM(	iAmmoCost, ENUM_TO_INT(aStartingWeapon), PURCHASE_WEAPONAMMO, iAmmoCount, fromBank, "FM_COR_AMMO_FILL", GET_HASH_KEY("SHOP_NJVS"), DEFAULT, DEFAULT, fromBankAndWallet)
//						
//						nextJobStruct.iTotalCostToFillAmmo -= iAmmoCost
//						
////						IF nextJobStruct.iTotalCostToFillAmmo <= CORONA_PURCHASE_ALL_AMMO_REFRESH_THRESHOLD
////							COUNT_AMMO_COST(nextJobStruct)
////							PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Re-calculating full ammo amount so we have accurate cost")
////						ENDIF
//						
//						IF nextJobStruct.iTotalCostToFillAmmo <= 0
//							nextJobStruct.iTotalCostToFillAmmo = 0
//							SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_BLOCK_AMMO)
//							PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Player has full ammo, remove option")
//						ENDIF
//						
//						PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Purchased: Ammo: ", iAmmoCount, ", Remaining Cash: $", GET_PLAYER_CASH(PLAYER_ID()))
//						PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Purchase all ammo is now: ", nextJobStruct.iTotalCostToFillAmmo)
//									
//						RETURN TRUE
//					
//					//Can't Afford
//					ELSE
//						PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - CHECK B - Unable to purchase ammo, CANT AFFORD IT. NETWORK_CAN_SPEND_MONEY = ", NETWORK_CAN_SPEND_MONEY(iAmmoCost, fromBank, fromBankAndWallet, FALSE))
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ELSE
//	
//		// Player has hit the real limit
//		IF HAS_PLAYER_HIT_CORONA_AMMO_LIMIT(GET_CORONA_WEAPON_FROM_INT(nextJobStruct.iWeaponSlot), FALSE)
//		
//			PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Unable to purchase ammo, AMMO MAXED")
//		
//		// Player has hit the locked weapon limit
//		ELIF HAS_PLAYER_HIT_CORONA_AMMO_LIMIT(GET_CORONA_WEAPON_FROM_INT(nextJobStruct.iWeaponSlot), TRUE)
//		
//			PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Unable to purchase ammo, AMMO LIMITED - this weapon is not unlocked")
//		
//		// Player can't afford the ammo
//		ELSE
//			INT iWeaponCost, iAmmoCost
//			GET_GUNSHOP_WEAPON_COST(GET_CORONA_WEAPON_FROM_INT(nextJobStruct.iWeaponSlot), iWeaponCost, iAmmoCost)
//			IF iAmmoCost > 0
//			AND (GET_CORONA_CASH_AVAILABLE() < iAmmoCost
//			OR NOT NETWORK_CAN_SPEND_MONEY(iAmmoCost, FALSE, TRUE, FALSE))
//				PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Unable to purchase ammo, CANT AFFORD IT: NETWORK_CAN_SPEND_MONEY = ", NETWORK_CAN_SPEND_MONEY(iAmmoCost, FALSE, TRUE, FALSE))
//			ELSE
//				PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - Unable to purchase ammo, UNKNOWN?")
//			ENDIF
//		ENDIF
//	ENDIF
//	
////	SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_BLOCK_AMMO)
////	PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_AMMO - CLIENT_BOOL_BLOCK_AMMO ")
//	
//	RETURN FALSE
//ENDFUNC
//
///// PURPOSE: Returns TRUE if the player can afford this purchase
//FUNC BOOL CAN_PLAYER_AFFORD_FULL_AMMO(NEXT_JOB_STRUCT& nextJobStruct)
//	// Can the player afford this max ammo option
////	IF GET_CORONA_CASH_AVAILABLE() < coronaMenuData.iTotalCostToFillAmmo
//	IF NOT NETWORK_CAN_SPEND_MONEY(nextJobStruct.iTotalCostToFillAmmo, FALSE, TRUE, FALSE)
//		RETURN FALSE
//	ENDIF
//	
//	RETURN TRUE
//ENDFUNC
//
//FUNC BOOL PROCESS_PURCHASING_FULL_AMMO(NEXT_JOB_STRUCT& nextJobStruct)
//	
//	INT iBSCheckedAmmoTypes
//	
//	INT iWeaponSlot
//	WEAPON_TYPE aWeapon
//
//	GUNCLUB_WEAPON_DATA_STRUCT	sWeaponStruct
//
//	// Loop over our normal weapons
//	FOR iWeaponSlot = 0 TO (ENUM_TO_INT(NUM_WEAPONSLOTS)-1) STEP 1
//		
//		// Get the weapon type from the slot
//		IF GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot) = WEAPONSLOT_INVALID
//			aWeapon = WEAPONTYPE_INVALID
//		ELSE
//			aWeapon = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot))
//		ENDIF
//		
//		// Is the weapon valid for the corona?
//		IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType)
//			
//			IF GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(aWeapon, sWeaponStruct)
//			
//				IF aWeapon != WEAPONTYPE_UNARMED
//					IF GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType) != -1
//					OR aWeapon = WEAPONTYPE_GRENADE
//						IF aWeapon = WEAPONTYPE_GRENADE
//						OR NOT IS_BIT_SET(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
//						
//							IF aWeapon != WEAPONTYPE_GRENADE
//								SET_BIT(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
//							ENDIF
//							
//							PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_FULL_AMMO - Purchasing ammo for: ", GET_WEAPON_NAME(aWeapon))
//							PROCESS_PURCHASING_AMMO(nextJobStruct, aWeapon)
//							
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			
//		ENDIF
//	ENDFOR
//	
//	// Process our DLC weapons in the same way	
//	scrShopWeaponData sWeaponData
//	FOR iWeaponSlot = 0 TO (GET_NUM_DLC_WEAPONS()-1) STEP 1
//		
//		IF GET_DLC_WEAPON_DATA(iWeaponSlot, sWeaponData)
//
//			aWeapon = INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash)
//
//			IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType)
//				
//				// Grab our script data for the weapon
//				IF GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(aWeapon, sWeaponStruct)
//					IF GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType) != -1
//					
//						IF NOT IS_BIT_SET(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
//								
//							SET_BIT(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
//							
//							PRINTLN("[CS_JOB] [ammo/armour] PROCESS_PURCHASING_FULL_AMMO - Purchasing ammo for: ", GET_WEAPON_NAME(aWeapon))
//							PROCESS_PURCHASING_AMMO(nextJobStruct, aWeapon)
//						ENDIF
//					ENDIF
//				ENDIF	
//			ENDIF
//		ENDIF
//	ENDFOR
//
//	// Stop us from offering this to the player any more
//	SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_BLOCK_AMMO)
//	PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
//	REFRESH_BUTTONS(nextJobStruct)
//	
//	RETURN TRUE
//ENDFUNC
//
//PROC HANDLE_BUYING_AMMO(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
//	IF AMMO_AVAILABLE(nextJobStruct)
//	
//		// Count cost of total ammo
//		IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_COUNT_AMMO_COST)
//			COUNT_AMMO_COST(nextJobStruct)
//			SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_COUNT_AMMO_COST)
//		ELSE
//	
//			// Can the player afford this max ammo option
//			IF NOT CAN_PLAYER_AFFORD_FULL_AMMO(nextJobStruct)
//				PRINTLN("[CS_JOB] [ammo/armour] HANDLE_BUYING_AMMO - FALSE Total Cost: ", nextJobStruct.iTotalCostToFillAmmo, ", Player Cash: ", GET_CORONA_CASH_AVAILABLE())
//				SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_BLOCK_AMMO)
//			ENDIF
//			
//			// Warning screen ON
//			IF IS_AMMO_WARNING_SCREEN_ACTIVE(nextJobStruct)
//
//				IF HAS_YES_BUTTON_BEEN_PRESSED()
//					// Buy ammo
//					PROCESS_PURCHASING_FULL_AMMO(nextJobStruct)
//					TURN_ON_AMMO_WARNING(nextJobStruct, FALSE)
//					PRINTLN("[CS_JOB] [ammo/armour] HANDLE_BUYING_AMMO - 1 ")
//				ELSE
//					// Quit out
//					IF HAS_END_JOB_QUIT_BEEN_PRESSED()
//					OR HAS_VOTING_ENDED(serverVars)
//						PRINTLN("[CS_JOB] [ammo/armour] HANDLE_BUYING_AMMO - 2 ")
//						TURN_ON_AMMO_WARNING(nextJobStruct, FALSE)
//					ENDIF	
//					PRINTLN("[CS_JOB] [ammo/armour] HANDLE_BUYING_AMMO - 3 ")
//				ENDIF
//
//				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS("FM_CSC_QUIT", "FM_COR_PWAR", (FE_WARNING_YES | FE_WARNING_NO), "", TRUE, nextJobStruct.iTotalCostToFillAmmo)
//			// Warning screen OFF
//			ELSE
//				PRINTLN("[CS_JOB] [ammo/armour] HANDLE_BUYING_AMMO - 4 ")
//				// Buy ammo button
//				IF HAS_END_JOB_BUY_AMMO_BEEN_PRESSED()
//					PRINTLN("[CS_JOB] [ammo/armour] HANDLE_BUYING_AMMO - 5 ")
//					IF WARN_ABOUT_AMMO_COST(nextJobStruct)
//						// Activate warning screen
//						TURN_ON_AMMO_WARNING(nextJobStruct, TRUE)
//						PRINTLN("[CS_JOB] [ammo/armour] HANDLE_BUYING_AMMO - 6 ")
//					ELSE
//						// Buy ammo
//						PROCESS_PURCHASING_FULL_AMMO(nextJobStruct)
//						PRINTLN("[CS_JOB] [ammo/armour] HANDLE_BUYING_AMMO - 7 ")
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║		SELECTION																║
//╚═════════════════════════════════════════════════════════════════════════════╝	

PROC MOVE_TO_JOB_PANEL(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iMoveToPanel)
	BOOL bFirstTileOnly = FALSE
	PRINTLN(" [CS_JOB] MOVE_TO_JOB_PANEL from  ", nextJobStruct.iJobPanelSelected)
	nextJobStruct.iJobPanelSelected = iMoveToPanel
	DRAW_END_JOB_SELECTION(serverVars, nextJobStruct)
	REFRESH_VOTES(serverVars, nextJobStruct, bFirstTileOnly)
	PRINTLN(" [CS_JOB] MOVE_TO_JOB_PANEL --> ", nextJobStruct.iJobPanelSelected)
ENDPROC

PROC HANDLE_PANEL_SELECTION(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, INT iDirection, BOOL bUseMouseWheelScrolling = FALSE)

	IF IS_PLAYER_SCTV(PLAYER_ID())
	
		EXIT
	ENDIF
	
	// Moving panels
	IF bUseMouseWheelScrolling = FALSE
		SWITCH iDirection
			CASE PRESSED_UP	
				SWITCH GET_JOB_PANEL_SELECTED(nextJobStruct)
					CASE JOB_PANEL_ZERO  
						IF IS_REPLAY_ALLOWED()
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REPLAY)
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_THREE)
						ENDIF
					BREAK
					CASE JOB_PANEL_ONE		
						IF ON_LAST_PAGE_REFRESHED(serverVars)
						OR RAN_OUT_OF_REFRESHES(serverVars) 
						OR CANT_REFRESH_YET(serverVars)
						
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FOUR)
						ELSE	
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REFRESH)
						ENDIF
					BREAK
					CASE JOB_PANEL_TWO 	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
					BREAK
					CASE JOB_PANEL_THREE 	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ZERO)
					BREAK
					CASE JOB_PANEL_FOUR	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ONE)
					BREAK
					CASE JOB_PANEL_FIVE		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_TWO)
					BREAK
					CASE JOB_PANEL_REPLAY		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_THREE)
					BREAK
					CASE JOB_PANEL_REFRESH	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FOUR)
					BREAK
					CASE JOB_PANEL_EXIT		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FIVE)
					BREAK
				ENDSWITCH
			BREAK
			CASE PRESSED_DOWN
				SWITCH GET_JOB_PANEL_SELECTED(nextJobStruct)
					CASE JOB_PANEL_ZERO  
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_THREE)
					BREAK
					CASE JOB_PANEL_ONE		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FOUR)
					BREAK
					CASE JOB_PANEL_TWO 	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FIVE)
					BREAK
					CASE JOB_PANEL_THREE 
						IF IS_REPLAY_ALLOWED()
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REPLAY)
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ZERO)
						ENDIF
					BREAK
					CASE JOB_PANEL_FOUR	
						IF ON_LAST_PAGE_REFRESHED(serverVars)
						OR RAN_OUT_OF_REFRESHES(serverVars) 
						OR CANT_REFRESH_YET(serverVars)
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ONE)
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REFRESH)
						ENDIF
					BREAK
					CASE JOB_PANEL_FIVE		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
					BREAK
					CASE JOB_PANEL_REPLAY		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ZERO)
					BREAK
					CASE JOB_PANEL_REFRESH	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ONE)
					BREAK
					CASE JOB_PANEL_EXIT		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_TWO)
					BREAK
				ENDSWITCH
			BREAK
			CASE PRESSED_LEFT	
				SWITCH GET_JOB_PANEL_SELECTED(nextJobStruct)
					CASE JOB_PANEL_ZERO  
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_TWO)
					BREAK
					CASE JOB_PANEL_ONE		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ZERO)
					BREAK
					CASE JOB_PANEL_TWO 	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ONE)
					BREAK
					CASE JOB_PANEL_THREE 	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FIVE)
					BREAK
					CASE JOB_PANEL_FOUR	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_THREE)
					BREAK
					CASE JOB_PANEL_FIVE		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FOUR)
					BREAK
					CASE JOB_PANEL_REPLAY		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
					BREAK
					CASE JOB_PANEL_REFRESH	
						IF IS_REPLAY_ALLOWED()
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REPLAY)
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
						ENDIF
					BREAK
					CASE JOB_PANEL_EXIT		
						IF ON_LAST_PAGE_REFRESHED(serverVars)
						OR RAN_OUT_OF_REFRESHES(serverVars) 
						OR CANT_REFRESH_YET(serverVars)
							IF IS_REPLAY_ALLOWED()
								MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REPLAY)
							ELSE
								MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
							ENDIF
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REFRESH)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE PRESSED_RIGHT	
				SWITCH GET_JOB_PANEL_SELECTED(nextJobStruct)
					CASE JOB_PANEL_ZERO  
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ONE)
					BREAK
					CASE JOB_PANEL_ONE		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_TWO)
					BREAK
					CASE JOB_PANEL_TWO 	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ZERO)
					BREAK
					CASE JOB_PANEL_THREE 	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FOUR)
					BREAK
					CASE JOB_PANEL_FOUR	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FIVE)
					BREAK
					CASE JOB_PANEL_FIVE		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_THREE)
					BREAK
					CASE JOB_PANEL_REPLAY	
						IF ON_LAST_PAGE_REFRESHED(serverVars)
						OR RAN_OUT_OF_REFRESHES(serverVars) 
						OR CANT_REFRESH_YET(serverVars)
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REFRESH)
						ENDIF
					BREAK
					CASE JOB_PANEL_REFRESH	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
					BREAK
					CASE JOB_PANEL_EXIT	
						IF IS_REPLAY_ALLOWED()
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REPLAY)
						ELSE
							IF ON_LAST_PAGE_REFRESHED(serverVars)
							OR RAN_OUT_OF_REFRESHES(serverVars) 
							OR CANT_REFRESH_YET(serverVars)
								MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
							ELSE
								MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REFRESH)
							ENDIF						
						ENDIF
					BREAK
				ENDSWITCH
			BREAK	
		ENDSWITCH
	ELSE // bUseMouseWheelScrolling = TRUE, so we need to scroll horizontally and handle changing rows too
		SWITCH iDirection
			CASE PRESSED_LEFT
				SWITCH GET_JOB_PANEL_SELECTED(nextJobStruct)
					CASE JOB_PANEL_ZERO  
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
					BREAK
					CASE JOB_PANEL_ONE		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ZERO)
					BREAK
					CASE JOB_PANEL_TWO 	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ONE)
					BREAK
					CASE JOB_PANEL_THREE 	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_TWO)
					BREAK
					CASE JOB_PANEL_FOUR	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_THREE)
					BREAK
					CASE JOB_PANEL_FIVE		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FOUR)
					BREAK
					CASE JOB_PANEL_REPLAY		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FIVE)
					BREAK
					CASE JOB_PANEL_REFRESH	
						IF IS_REPLAY_ALLOWED()
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REPLAY)
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FIVE)
						ENDIF
					BREAK
					CASE JOB_PANEL_EXIT		
						IF ON_LAST_PAGE_REFRESHED(serverVars)
						OR RAN_OUT_OF_REFRESHES(serverVars) 
						OR CANT_REFRESH_YET(serverVars)
							IF IS_REPLAY_ALLOWED()
								MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REPLAY)
							ELSE
								MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FIVE)
							ENDIF
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REFRESH)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE PRESSED_RIGHT	
				SWITCH GET_JOB_PANEL_SELECTED(nextJobStruct)
					CASE JOB_PANEL_ZERO  
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ONE)
					BREAK
					CASE JOB_PANEL_ONE		
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_TWO)
					BREAK
					CASE JOB_PANEL_TWO 	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_THREE)
					BREAK
					CASE JOB_PANEL_THREE 	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FOUR)
					BREAK
					CASE JOB_PANEL_FOUR	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_FIVE)
					BREAK
					CASE JOB_PANEL_FIVE	
						IF IS_REPLAY_ALLOWED()
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REPLAY)
						ELIF (ON_LAST_PAGE_REFRESHED(serverVars)
						OR RAN_OUT_OF_REFRESHES(serverVars) 
						OR CANT_REFRESH_YET(serverVars))
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REFRESH)
						ENDIF
					BREAK
					CASE JOB_PANEL_REPLAY	
						IF ON_LAST_PAGE_REFRESHED(serverVars)
						OR RAN_OUT_OF_REFRESHES(serverVars) 
						OR CANT_REFRESH_YET(serverVars)
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REFRESH)
						ENDIF
					BREAK
					CASE JOB_PANEL_REFRESH	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
					BREAK
					CASE JOB_PANEL_EXIT	
						MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ZERO)
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

CONST_INT SCROLL_TIME 175

PROC CHECK_FOR_DIRECTION_PRESSES(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)

	INT iMoveUp
	BOOL bPressed
	INT iDirection
	
	INT iMaxNumPlayers = nextJobStruct.iPlayerListCount
	BOOL bPlayerListOn = IS_PLAYER_LIST_ON(nextJobStruct)
	
	// PC mouse support - displays borders around the next job tiles when hovering the mouse cursor over them
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	AND bPlayerListOn = FALSE
	
		SET_MOUSE_CURSOR_THIS_FRAME() 
	
		eMOUSE_EVT eMouseEvent
		INT iUID
		INT iContext
		
		IF HAS_SCALEFORM_MOVIE_LOADED(nextJobStruct.scaleformJobsMovie)
		AND GET_MOUSE_EVENT(nextJobStruct.scaleformJobsMovie, eMouseEvent, iUID, iContext)
			IF iUID > -1
			AND iUID < 9
				IF eMouseEvent = EVENT_TYPE_MOUSE_ROLL_OVER
					nextJobStruct.iCurrentMouseHoverHighlightId = iUID
					SET_MOUSE_HOVER_HIGHLIGHT_AROUND_SLOT(nextJobStruct, nextJobStruct.iCurrentMouseHoverHighlightId, TRUE)
				ELIF eMouseEvent = EVENT_TYPE_MOUSE_ROLL_OUT
					nextJobStruct.iCurrentMouseHoverHighlightId = -1
					SET_MOUSE_HOVER_HIGHLIGHT_AROUND_SLOT(nextJobStruct, iUID, FALSE)
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

	// Up/down
	IF SHOULD_SELECTION_BE_INCREMENTED(nextJobStruct.timerScrollDelay, iMoveUp, bPressed, FALSE, FALSE, SCROLL_TIME, TRUE)
	
		// On PC if the player scrolls the mouse wheel we actually want to scroll the selection horizontally not vertically
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		AND (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP) OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN))
		AND bPlayerListOn = FALSE
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
				iDirection = PRESSED_LEFT
			ELSE
				iDirection = PRESSED_RIGHT
			ENDIF
			HANDLE_PANEL_SELECTION(serverVars, nextJobStruct, iDirection, TRUE)
		ELSE
	
			IF iMoveUp > 0
				PRINTLN(" [CS_JOB] CHECK_FOR_DIRECTION_PRESSES - PRESSED_UP  ")
				iDirection = PRESSED_UP
			ELSE
				PRINTLN(" [CS_JOB] CHECK_FOR_DIRECTION_PRESSES - PRESSED_DOWN  ")
				iDirection = PRESSED_DOWN
			ENDIF
		
			// Scrolling player list
			IF bPlayerListOn
				IF iDirection = PRESSED_UP
					IF (nextJobStruct.iRowSelected > 0)
						nextJobStruct.iRowSelected --
					ELSE
						nextJobStruct.iRowSelected = (iMaxNumPlayers - 1)
					ENDIF
				ELSE
					IF (nextJobStruct.iRowSelected < (iMaxNumPlayers - 1))
						nextJobStruct.iRowSelected ++
					ELSE
						nextJobStruct.iRowSelected = 0
					ENDIF
				ENDIF
				PRINTLN(" [CS_JOB] CHECK_FOR_DIRECTION_PRESSES - iRowSelected =  ", nextJobStruct.iRowSelected )
			ELSE
				HANDLE_PANEL_SELECTION(serverVars, nextJobStruct, iDirection)
			ENDIF
		ENDIF
	ENDIF
	
	// Left/right
	IF NOT bPlayerListOn // Player list is only up/down
		IF SHOULD_SELECTION_BE_INCREMENTED(nextJobStruct.timerScrollDelay, iMoveUp, bPressed, TRUE, FALSE, SCROLL_TIME, TRUE)
			IF iMoveUp > 0
				PRINTLN(" [CS_JOB] CHECK_FOR_DIRECTION_PRESSES - PRESSED_RIGHT  ")
				iDirection = PRESSED_RIGHT
			ELSE
				PRINTLN(" [CS_JOB] CHECK_FOR_DIRECTION_PRESSES - PRESSED_LEFT  ")
				iDirection = PRESSED_LEFT
			ENDIF
		
			HANDLE_PANEL_SELECTION(serverVars, nextJobStruct, iDirection)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_VOTE_BUTTON_BEEN_PRESSED(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	
	IF IS_PC_VERSION()
	AND NETWORK_TEXT_CHAT_IS_TYPING()
		RETURN FALSE
	ENDIF
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	
		PRINTLN("------------------------------------------------------------------------------")
		PRINTLN("--                                                                          --")
		PRINTLN("--                        [CS_JOB]  VOTE_PRESSED				             --")
		PRINTLN("--                                                                          --")
		PRINTLN("------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	// PC mouse support
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		SET_MOUSE_CURSOR_THIS_FRAME() 
	
		eMOUSE_EVT eMouseEvent
		INT iUID
		INT iContext
		
		IF HAS_SCALEFORM_MOVIE_LOADED(nextJobStruct.scaleformJobsMovie)
		AND GET_MOUSE_EVENT(nextJobStruct.scaleformJobsMovie, eMouseEvent, iUID, iContext)
			IF iUID > -1
			AND iUID < 9
				IF eMouseEvent = EVENT_TYPE_MOUSE_PRESS
					IF iUID = 6 // REPLAY
						IF IS_REPLAY_ALLOWED()
							IF GET_JOB_PANEL_SELECTED(nextJobStruct) = JOB_PANEL_REPLAY
								RETURN TRUE
							ELSE
								MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REPLAY)
							ENDIF
						ENDIF
					ELIF iUID = 7 // REFRESH
						IF NOT ON_LAST_PAGE_REFRESHED(serverVars)
						AND NOT RAN_OUT_OF_REFRESHES(serverVars) 
						AND NOT CANT_REFRESH_YET(serverVars)
							IF GET_JOB_PANEL_SELECTED(nextJobStruct) = JOB_PANEL_REFRESH
								RETURN TRUE
							ELSE
								MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_REFRESH)
							ENDIF
						ENDIF
					ELIF iUID = 8 // EXIT
						IF GET_JOB_PANEL_SELECTED(nextJobStruct) = JOB_PANEL_EXIT
							RETURN TRUE
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_EXIT)
						ENDIF
					ELSE
						IF GET_JOB_PANEL_SELECTED(nextJobStruct) = iUID
							RETURN TRUE
						ELSE
							MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, iUID)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CHECK_FOR_BUTTON_VOTES(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	INT i
	INT iJob = GET_JOB_PANEL_SELECTED(nextJobStruct)
	INT iArrayShift = GET_ARRAY_SHIFT(serverVars)
	IF IS_SPECIAL_JOB(iJob)
		iArrayShift = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF serverVars.bForceVoteForFreemode
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_EXIT)
			PRINTLN("SHOULD_SKIP_END_OF_JOB_VOTE_SCREEN_BECAUSE_OF_Z_MENU_LAUNCH() - SET_BIT(GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_EXIT)")
			//GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].bVotedForAJob = TRUE
			SET_BIT(GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bVotedForAJob)
			SET_BIT(GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_EXIT)
		ENDIF
	ELIF g_SkipCelebAndLbd
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_REPLAY)
			PRINTLN("SHOULD_SKIP_END_OF_JOB_VOTE_SCREEN_BECAUSE_OF_g_SkipCelebAndLbd - SET_BIT(GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_REPLAY)")
			//GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].bVotedForAJob = TRUE
			SET_BIT(GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bVotedForAJob)
			SET_BIT(GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_REPLAY)
		ENDIF
	ELSE
	#ENDIF // #IF IS_DEBUG_BUILD
	
		IF HAS_VOTE_BUTTON_BEEN_PRESSED(serverVars, nextJobStruct)
			VOTE_FOR_THIS_JOB(serverVars, iJob)
			REPEAT JOB_PANEL_TOTAL i
				IF i <> (iJob + iArrayShift)
					REMOVE_VOTE_FROM_JOB(i)
				ENDIF
			ENDREPEAT
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
ENDPROC

FUNC BOOL SHOULD_RUN_NEW_VOTING_SYSTEM(INT iQuitProgress, INT iVoteStatus)

	IF SHOULD_SKIP_NJVS()
		
		PRINTLN(" [CS_JOB] SHOULD_RUN_NEW_VOTING_SYSTEM, SHOULD_SKIP_NJVS")
		
		RETURN FALSE
	ENDIF
	
	IF iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
	
		PRINTLN(" [CS_JOB] SHOULD_RUN_NEW_VOTING_SYSTEM, ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART")
	
		RETURN FALSE
	ENDIF
	
	IF iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP

		PRINTLN(" [CS_JOB] SHOULD_RUN_NEW_VOTING_SYSTEM, ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP")
	
		RETURN FALSE
	ENDIF
	
	IF iQuitProgress = LBD_QUIT_LBD
	
		PRINTLN(" [CS_JOB] SHOULD_RUN_NEW_VOTING_SYSTEM, LBD_QUIT_LBD")
	
		RETURN FALSE
	ENDIF
	
	IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())	// KGM 5/3/14: Added Heist Planning check
	
		PRINTLN(" [CS_JOB] SHOULD_RUN_NEW_VOTING_SYSTEM, Is_Player_Currently_On_MP_Heist")

		RETURN FALSE
	ENDIF
	
	IF IS_THIS_IS_A_STRAND_MISSION() 
	AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
		RETURN FALSE
	ENDIF

	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
	
		PRINTLN(" [CS_JOB] SHOULD_RUN_NEW_VOTING_SYSTEM, IS_PLAYER_ON_A_PLAYLIST")
	
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
	
		PRINTLN(" [CS_JOB] SHOULD_RUN_NEW_VOTING_SYSTEM, IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL")
	
		RETURN FALSE
	ENDIF
	
	// Is creator, stops array overrun
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
	
		PRINTLN(" [CS_JOB] SHOULD_RUN_NEW_VOTING_SYSTEM, IS_FAKE_MULTIPLAYER_MODE_SET()")
	
		RETURN FALSE
	ENDIF	
	
	RETURN TRUE
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║		REFRESH																	║
//╚═════════════════════════════════════════════════════════════════════════════╝

PROC RESET_CLIENT_VOTING(NEXT_JOB_STRUCT& nextJobStruct)
	nextJobStruct.iJobPanelSelected = JOB_PANEL_ZERO
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	//GlobalplayerBD_FM[iPlayer].bVotedForAJob = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bVotedForAJob)
	CLEAR_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_PLAYER_LIST_ON)
	WIPE_ALL_CLIENT_VOTES()
	PRINTLN("[CS_JOB] RESET_CLIENT_VOTING  ")
ENDPROC

PROC GOTO_JOB_SERVER_STAGE(SERVER_NEXT_JOB_VARS& serverVars, INT iStageGoto)
	#IF IS_DEBUG_BUILD
		SWITCH iStageGoto
			CASE VOTE_SERVER_INIT 		PRINTLN("[CS_JOB] JOB_SERVER_STAGE  >>> VOTE_SERVER_INIT  ") 		BREAK		
			CASE VOTE_SERVER_PROCESS 	PRINTLN("[CS_JOB] JOB_SERVER_STAGE  >>> VOTE_SERVER_PROCESS  ") 	BREAK
			CASE VOTE_SERVER_RESET		PRINTLN("[CS_JOB] JOB_SERVER_STAGE  >>> VOTE_SERVER_RESET  ") 		BREAK
		ENDSWITCH
	#ENDIF
	
	serverVars.iServerLogicProgress  = iStageGoto
ENDPROC

FUNC BOOL HAS_SERVER_RESET_VOTING(SERVER_NEXT_JOB_VARS& serverVars)

	INT i

	REINIT_NET_TIMER(serverVars.timerVotingEnds)
	serverVars.iJobVoteWinner = -1
	serverVars.iSecondryJobVoteWinner = -1
	serverVars.iVoteTotal = 0
//	serverVars.iServerLogicProgress = 0
	serverVars.iMajorityVoteTotal = 0
	PRINTLN("[CS_JOB] HAS_SERVER_RESET_VOTING")
	
	REPEAT JOB_PANEL_TOTAL i
		IF serverVars.iJobVotes[i] <> 0
			PRINTLN("[CS_JOB] SERVER_REMOVE_VOTE_FROM i = ", i)
			serverVars.iJobVotes[i] = 0
			
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC 

//Did I vote to restart the mission
FUNC BOOL DID_I_VOTE_FOR_REPLAY()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_REPLAY)
ENDFUNC

FUNC BOOL DID_I_VOTE_FOR_RETURN_GTA_ONLINE()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_EXIT)
ENDFUNC

//Did I vote for random
FUNC BOOL DID_I_VOTE_FOR_RANDOM()
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_ZERO)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_ONE)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_TWO)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_THREE)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_FOUR)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_FIVE)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_SIX)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_SEVEN)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_EIGHT)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_NINE)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_TEN)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_ELEVEN)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_TWELVE)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_THIRTEEN)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_FOURTEEN)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_FIFTEEN)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_SIXTEEN)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iJobVote, JOB_PANEL_SEVENTEEN)
	
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_CLIENT_READY_TO_VOTE()
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayer].iJobVoteBitset, JOB_VOTE_CLIENT_READY)
		PRINTLN(" [CS_JOB] SET_CLIENT_READY_TO_VOTE - JOB_VOTE_CLIENT_READY, iPlayer = ", iPlayer)
		SET_BIT(GlobalplayerBD_FM[iPlayer].iJobVoteBitset, JOB_VOTE_CLIENT_READY)
	ENDIF
ENDPROC

PROC CLEAR_CLIENT_READY_TO_VOTE()
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF IS_BIT_SET(GlobalplayerBD_FM[iPlayer].iJobVoteBitset, JOB_VOTE_CLIENT_READY)
		PRINTLN(" [CS_JOB] CLEAR_CLIENT_READY_TO_VOTE - iPlayer = ", iPlayer)
		CLEAR_BIT(GlobalplayerBD_FM[iPlayer].iJobVoteBitset, JOB_VOTE_CLIENT_READY)
	ENDIF
ENDPROC

PROC SET_NJVS_TELEMETRY_FOR_RESULT(SERVER_NEXT_JOB_VARS& serverVars, STRUCT_NJVS_VOTE& sNjvsVoteStruct, INT iJobVoteWinner, BOOL bWinnerRandom)
	sNjvsVoteStruct.result = ""
	IF bWinnerRandom
		sNjvsVoteStruct.result += JOB_PANEL_RANDOM
		PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.result += JOB_PANEL_RANDOM = ", sNjvsVoteStruct.result)	
	ELIF iJobVoteWinner >=JOB_PANEL_ZERO AND iJobVoteWinner <= JOB_PANEL_SEVENTEEN
		sNjvsVoteStruct.result = serverVars.tlFileName[iJobVoteWinner]
		PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.result = serverVars.tlFileName[serverVars.iJobVoteWinner] = ", sNjvsVoteStruct.result)	
	ELIF serverVars.iJobVoteWinner = -1
		sNjvsVoteStruct.result += JOB_PANEL_INVALID
		PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.result += JOB_PANEL_INVALID = ", sNjvsVoteStruct.result)	
	ELSE
		sNjvsVoteStruct.result += serverVars.iJobVoteWinner
		PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.result += serverVars.iJobVoteWinner = ", sNjvsVoteStruct.result)	
	ENDIF
ENDPROC


PROC CALL_END_OF_MISSION_NJVS_TELEMETRY(SERVER_NEXT_JOB_VARS& serverVars, BOOL bSecondryVote, BOOL bWinnerRandom)

	STRUCT_NJVS_VOTE sNjvsVoteStruct
	
	sNjvsVoteStruct.sMid = g_FMMC_STRUCT.tl31LoadedContentID
	sNjvsVoteStruct.numPlayers = NETWORK_GET_NUM_CONNECTED_PLAYERS()
	
	INT iPlayerSlot = NATIVE_TO_INT(PLAYER_ID())	
	INT voteChoice = JOB_PANEL_NO_VOTE
	//Get what was selected by the local player
	IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_ZERO)
		voteChoice = JOB_PANEL_ZERO
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_ONE)
		voteChoice = JOB_PANEL_ONE
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_TWO)
		voteChoice = JOB_PANEL_TWO
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_THREE)
		voteChoice = JOB_PANEL_THREE
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_FOUR)
		voteChoice = JOB_PANEL_FOUR
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_FIVE)
		voteChoice = JOB_PANEL_FIVE
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_SIX)
		voteChoice = JOB_PANEL_SIX
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_SEVEN)
		voteChoice = JOB_PANEL_SEVEN
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_EIGHT)
		voteChoice = JOB_PANEL_EIGHT
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_NINE)
		voteChoice = JOB_PANEL_NINE
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_TEN)
		voteChoice = JOB_PANEL_TEN
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_ELEVEN)
		voteChoice = JOB_PANEL_ELEVEN
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_TWELVE)
		voteChoice = JOB_PANEL_TWELVE
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_THIRTEEN)
		voteChoice = JOB_PANEL_THIRTEEN
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_FOURTEEN)
		voteChoice = JOB_PANEL_FOURTEEN
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_FIFTEEN)
		voteChoice = JOB_PANEL_FIFTEEN
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_SIXTEEN)
		voteChoice = JOB_PANEL_SIXTEEN
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_SEVENTEEN)
		voteChoice = JOB_PANEL_SEVENTEEN
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_REPLAY)
		voteChoice = JOB_PANEL_REPLAY
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_REFRESH)
		voteChoice = JOB_PANEL_REFRESH
	ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].iJobVote, JOB_PANEL_EXIT)
		voteChoice = JOB_PANEL_EXIT
	ENDIF
	
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - bSecondryVote = ", bSecondryVote)
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - iJobVoteWinner = ", serverVars.iJobVoteWinner)
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - iSecondryJobVoteWinner = ", serverVars.iSecondryJobVoteWinner)
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - voteChoice = ", voteChoice)	
	
	sNjvsVoteStruct.voteChoice = ""	
	IF voteChoice >= JOB_PANEL_ZERO AND voteChoice <= JOB_PANEL_SEVENTEEN
		IF voteChoice = JOB_PANEL_ZERO
		AND IS_FIRST_JOB_RANDOM(serverVars)
			sNjvsVoteStruct.voteChoice += JOB_PANEL_RANDOM
			PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.voteChoice = serverVars.tlFileName[voteChoice] = JOB_PANEL_RANDOM = ", sNjvsVoteStruct.voteChoice)	
		ELSE
			sNjvsVoteStruct.voteChoice = serverVars.tlFileName[voteChoice]
			PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.voteChoice = serverVars.tlFileName[voteChoice] = ", sNjvsVoteStruct.voteChoice)	
		ENDIF
	ELSE
		sNjvsVoteStruct.voteChoice = ""
		sNjvsVoteStruct.voteChoice += voteChoice
		PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.voteChoice += voteChoice = ", sNjvsVoteStruct.voteChoice)	
	ENDIF
	
	//get what the server picked, for the first and second vote winner.
	IF bSecondryVote
		SET_NJVS_TELEMETRY_FOR_RESULT(serverVars, sNjvsVoteStruct, serverVars.iSecondryJobVoteWinner, bWinnerRandom)
		sNjvsVoteStruct.voteDiff = (voteChoice != serverVars.iSecondryJobVoteWinner)
	ELSE
		SET_NJVS_TELEMETRY_FOR_RESULT(serverVars, sNjvsVoteStruct, serverVars.iJobVoteWinner, bWinnerRandom)
		sNjvsVoteStruct.voteDiff = (voteChoice != serverVars.iJobVoteWinner)
	ENDIF
	
	g_NjvsAndQuitModeTelemetryData.voteDiff = sNjvsVoteStruct.voteDiff
	sNjvsVoteStruct.timeOnline = 0
	
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.sMid = ", sNjvsVoteStruct.sMid)
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.numPlayers = ", sNjvsVoteStruct.numPlayers)			
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.voteChoice = ", sNjvsVoteStruct.voteChoice)	
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.result = ", sNjvsVoteStruct.result)			
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.timeOnline = ", sNjvsVoteStruct.timeOnline)			
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - sNjvsVoteStruct.voteDiff = ", sNjvsVoteStruct.voteDiff)	
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - g_NjvsAndQuitModeTelemetryData.voteDiff = ", g_NjvsAndQuitModeTelemetryData.voteDiff)
	PRINTLN("[TEL] - CALL_END_OF_MISSION_NJVS_TELEMETRY - calling PLAYSTATS_NJVS_VOTE")
	
	PLAYSTATS_NJVS_VOTE(sNjvsVoteStruct)
ENDPROC

PROC FINISH_END_VOTE_CLIENT_SIDE(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	PRINTLN("FMMC_EOM -  [CS_JOB] FINISH_END_VOTE_CLIENT_SIDE, STARTING")
	//Set that I left with everyone else
	SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
	
	CLEAR_CLIENT_READY_TO_VOTE()
	
	//Clean up old settings and set new
	CLEAR_CORONA_INITIALISING_A_QUICK_RESTART()
	CLEAR_SWAP_TEAMS_ON_QUICK_RESTART()
	CLEAR_MY_TEAM_FOR_RESTART()
	CLEAR_MY_EMO_VOTE_STATUS() 
	CLEAR_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
	
	BOOL bSecondryVote, bRockStarVerified, bOkToMoveToJob, bWinnerRandom
	INT iVoteArrayPos = -1
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	AND IS_ROCKSTAR_DEV()
		PRINTLN("[TS] SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].sClientCoronaData.iBitSet, ciCORONA_DEV_SPECTATOR)")
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_DEV_SPECTATOR)	
	ENDIF
	
	//If I voted to return to Freemode
	IF DID_I_VOTE_FOR_RETURN_GTA_ONLINE() // 1643601
	AND NOT DID_QUIT_END_JOB_VOTING(nextJobStruct)
	AND NOT TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING()
		PRINTLN("[TS] TO_GAME - FINISH_END_VOTE_CLIENT_SIDE Doing a continue")
		SET_ONLY_CONSIDER_ME_FOR_TO_GAME_CONTINUE()
		PRINTLN("FMMC_EOM -  [CS_JOB] ciFMMC_EOM_VOTE_STATUS_CONTINUE ")
		g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_CONTINUE
	ELIF SHOULD_FM_FLOW_MISSION_SKIP_NJVS()
		PRINTLN("[TS] TO_GAME - FINISH_END_VOTE_CLIENT_SIDE SHOULD_FM_FLOW_MISSION_SKIP_NJVS Doing a continue")
		SET_ONLY_CONSIDER_ME_FOR_TO_GAME_CONTINUE()
		PRINTLN("FMMC_EOM -  [CS_JOB] ciFMMC_EOM_VOTE_STATUS_CONTINUE ")
		g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_CONTINUE
	// QUIT
	ELIF DID_QUIT_END_JOB_VOTING(nextJobStruct)
	OR HAVE_PLAYERS_VOTED_REFRESH(serverVars) // Failsafe, assert if it gets here see (1639258)
	OR TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING()
		g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUIT
		PRINTLN("FMMC_EOM - FINISH_END_VOTE_CLIENT_SIDE [CS_JOB] ciFMMC_EOM_VOTE_STATUS_QUIT ")
	// RESTART
	ELIF HAVE_PLAYERS_VOTED_RESTART(serverVars)
		PRINTLN("FMMC_EOM -FINISH_END_VOTE_CLIENT_SIDE  [CS_JOB] ciFMMC_EOM_VOTE_STATUS_RESTART ")
		g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_RESTART		
		SET_ONLY_CONSIDER_ME_FOR_TO_GAME_RANDOM_RESTART()
		
	// GTA ONLINE
	ELIF HAVE_PLAYERS_VOTED_RETURN_GTA_ONLINE(serverVars)
	
		//If the secondry vote was restart and that's what I voted for
		IF (HAVE_PLAYERS_VOTED_RESTART(serverVars, TRUE)
		AND DID_I_VOTE_FOR_REPLAY())
			PRINTLN("[TS] TO_GAME - FINISH_END_VOTE_CLIENT_SIDE secondry vote passed for RESTART and that's what I voted for")
			SET_ONLY_CONSIDER_ME_FOR_TO_GAME_RANDOM_RESTART()
			PRINTLN("FMMC_EOM -  [CS_JOB] ciFMMC_EOM_VOTE_STATUS_RESTART ")
			g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_RESTART	
			bSecondryVote = TRUE
			
		//If the secondry vote was random and that's what I voted for
		ELIF (HAS_SERVER_PICKED_RANDOM_AS_SECONDRY_VOTE(serverVars)
		AND DID_I_VOTE_FOR_RANDOM())
			PRINTLN("[TS] TO_GAME - FINISH_END_VOTE_CLIENT_SIDE secondry vote passed for RANDOM and that's what I voted for")
			SET_ONLY_CONSIDER_ME_FOR_TO_GAME_RANDOM_RESTART()
			g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_RANDOM
			bSecondryVote = TRUE
			
		// CONTINUE	
		ELSE
			PRINTLN("[TS] TO_GAME - FINISH_END_VOTE_CLIENT_SIDE Doing a continue")
			SET_ONLY_CONSIDER_ME_FOR_TO_GAME_CONTINUE()
			PRINTLN("FMMC_EOM -  [CS_JOB] ciFMMC_EOM_VOTE_STATUS_CONTINUE ")
			g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_CONTINUE	
		ENDIF
	// RANDOM	
	ELSE
		PRINTLN("[TS] TO_GAME - FINISH_END_VOTE_CLIENT_SIDE Doing a ciFMMC_EOM_VOTE_STATUS_RANDOM")
		g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_RANDOM
		SET_ONLY_CONSIDER_ME_FOR_TO_GAME_RANDOM_RESTART()
	ENDIF
	
	INT iType
	INT iPlayListType
	
	//Swith the vote status
	SWITCH g_sFMMCEOM.iVoteStatus
	
		// Players chose a Job
		CASE ciFMMC_EOM_VOTE_STATUS_RANDOM		
			
			PRINTLN("FMMC_EOM - FINISH_END_VOTE_CLIENT_SIDE - iJobVoteWinner = ", serverVars.iJobVoteWinner)
			PRINTLN("FMMC_EOM - FINISH_END_VOTE_CLIENT_SIDE - iSecondryJobVoteWinner = ", serverVars.iSecondryJobVoteWinner)
			
			//Get the vote array pos
			IF bSecondryVote
			AND (serverVars.iSecondryJobVoteWinner <> -1)
				IF DOES_SECONDRY_JOB_HAVE_ANY_VOTES(serverVars)
					iVoteArrayPos = nextJobStruct.iArrayPos[(serverVars.iSecondryJobVoteWinner)]
				ENDIF
				PRINTLN("FMMC_EOM - FINISH_END_VOTE_CLIENT_SIDE - bSecondryVote = TRUE - iVoteArrayPos = ", iVoteArrayPos)
				bRockStarVerified = IS_BIT_SET(serverVars.iVerifiedBitSet, serverVars.iSecondryJobVoteWinner)
				bOkToMoveToJob = TRUE
			ELSE	
				IF (serverVars.iJobVoteWinner <> -1)
					iVoteArrayPos = nextJobStruct.iArrayPos[(serverVars.iJobVoteWinner)]		
					PRINTLN("FMMC_EOM - FINISH_END_VOTE_CLIENT_SIDE - bSecondryVote = FALSE - iVoteArrayPos = ", iVoteArrayPos)
					bRockStarVerified = IS_BIT_SET(serverVars.iVerifiedBitSet, serverVars.iJobVoteWinner)
					bOkToMoveToJob = TRUE
				ENDIF
			ENDIF

			// If the vote winner was not -1
			IF bOkToMoveToJob = TRUE
				//If the flag is set for the first mission to be random
				SET_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
				SET_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS_NUM_PLAYERS(serverVars.iNumberOnJob)
					
				IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_SPECIAL_VEHICLE_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_SPECIAL_VEHICLE_RACE_SERIES
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_BUNKER_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_BUNKER_SERIES
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_TRANSFORM_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_TRANSFORM_SERIES
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_TARGET_ASSAULT_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_TARGET_ASSAULT_SERIES
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HOTRING_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_HOTRING_SERIES
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_ARENA_SERIES
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_RACE_SERIES
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_SURVIVAL_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_SURVIVAL_SERIES
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_OPEN_WHEEL_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_OPEN_WHEEL_SERIES
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_STREET_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_STREET_RACE_SERIES
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_PURSUIT_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_PURSUIT_SERIES
				#IF FEATURE_GEN9_EXCLUSIVE
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HSW_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_HSW_RACE_SERIES
				#ENDIF
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_COMMUNITY_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_COMMUNITY_SERIES
				ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					iType = FMMC_TYPE_CAYO_PERICO_SERIES
				//If it's a mission then set the sub types
				ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
					iPlayListType = CV2_PROFESSIONAL_PLAYLIST_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)
					IF CV2_IS_THIS_JOB_A_CORONA_FLOW_ADVERSARY_SERIES_JOB(TRUE)
					AND CV2_GET_ADVERSARY_SERIES_ARRAY_POS(iPlayListType) != -1
						iType = iPlayListType
					ELIF IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
						iType = FMMC_TYPE_MISSION_NEW_VS
					ElIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
						iType = FMMC_TYPE_MISSION_LTS
					ELIF Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
						iType = FMMC_TYPE_MISSION_CTF
					ELIF Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
					OR Is_Player_Currently_On_MP_Random_Event(PLAYER_ID())
						iType = FMMC_TYPE_MISSION_VS
					ELIF Is_Player_Currently_On_MP_Coop_Mission(PLAYER_ID())
						iType = FMMC_TYPE_MISSION_COOP
					ELSE
						iType = FMMC_TYPE_MISSION
					ENDIF
				//not a mission then just use the main type
				ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
				AND CV2_IS_THIS_JOB_A_CORONA_FLOW_JOB()
				AND NOT g_sMPTunables.bDisableStuntSeriesBucket
					iType =  FMMC_TYPE_RACE_STUNT_FOR_QM
				ELSE
					iType = g_FMMC_STRUCT.iMissionType
				ENDIF
				SET_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS_TYPE(iType)
				
				IF IS_FIRST_JOB_RANDOM(serverVars)
				AND ((serverVars.iJobVoteWinner) = 0
				OR iVoteArrayPos = -1)
					IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
					AND (g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_STUNT
					OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_STUNT_P2P
					OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_TARGET
					OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_TARGET_P2P)
						SET_TRANSITION_SESSION_QUICK_MATCH_RANDOM_TRY_STUNT()
					ENDIF
					g_sFMMCEOM.contentID = ""
					PRINTLN("[NJVSQMR] FMMC_EOM - FINISH_END_VOTE_CLIENT_SIDE - IS_BIT_SET(serverVars.iServerJobBitSet, IF IS_FIRST_JOB_RANDOM)")
					SET_TRANSITION_SESSION_QUICK_MATCH_RANDOM_LAST_CONTENT_ID(g_FMMC_STRUCT.tl31LoadedContentID)
					SET_ME_RANDOM_WITH_NO_CONTENT_ID()
					bWinnerRandom = TRUE
				ELSE
					g_sFMMCEOM.bDoWarp 		= TRUE 
					SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_END_OF_JOB_SCREEN)
					g_sFMMCEOM.vWarp		= GET_EOJV_MISSION_START_LOCATION(iVoteArrayPos, bRockStarVerified) 
					g_sFMMCEOM.contentID 	= GET_EOJV_MISSION_CONTENT_ID(iVoteArrayPos, bRockStarVerified)
					SET_MY_TRANSITION_SESSION_CONTENT_ID(g_sFMMCEOM.contentID)
					PRINTLN("FMMC_EOM - FINISH_END_VOTE_CLIENT_SIDE - sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_RANDOM vWarp = ", g_sFMMCEOM.vWarp)
					CLEAR_TRANSITION_SESSION_QUICK_MATCH_RANDOM_LAST_CONTENT_ID()
				ENDIF
				//GlobalplayerBD_FM[iPlayer].bMissionRestartInProgress = FALSE
				CLEAR_BIT(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bMissionRestartInProgress)
			ELSE
				// Failsafe
				SET_ONLY_CONSIDER_ME_FOR_TO_GAME_CONTINUE()
				g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_CONTINUE	
				
				#IF IS_DEBUG_BUILD
					PRINTLN("FMMC_EOM -  [CS_JOB] FINISH_END_VOTE_CLIENT_SIDE, ciFMMC_EOM_VOTE_STATUS_RANDOM, <<< FAILSAFE >>>, ciFMMC_EOM_VOTE_STATUS_CONTINUE ")
					SCRIPT_ASSERT("NJVS, Bug for Speirs, Find out why vote winner was -1 ")
				#ENDIF
			ENDIF
		BREAK
	
		// Return to GTA Online
		CASE ciFMMC_EOM_VOTE_STATUS_CONTINUE
			//Clear flag for warp 
			g_sFMMCEOM.bDoWarp 	= FALSE 
			g_sFMMCEOM.vWarp	= <<0.0, 0.0, 0.0>>			
			g_sFMMCEOM.contentID 	= "" 
			//GlobalplayerBD_FM[iPlayer].bMissionRestartInProgress = FALSE
			CLEAR_BIT(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bMissionRestartInProgress)
			PRINTLN("FMMC_EOM - FINISH_END_VOTE_CLIENT_SIDE - sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_CONTINUE")
		BREAK
	
		// Replay, keep the same teams
		CASE ciFMMC_EOM_VOTE_STATUS_RESTART	
			SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_END_OF_JOB_SCREEN_RESTART)
			IF GlobalplayerBD_FM[iPlayer].iCurrentMissionType = FMMC_TYPE_MISSION
			OR GlobalplayerBD_FM[iPlayer].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
				IF ARE_ALL_TEAMS_EVEN()
					SET_MY_TEAM_FOR_RESTART(GlobalplayerBD_FM[iPlayer].sClientCoronaData.iTeamChosen)
				ELSE
					CLEAR_MY_TEAM_FOR_RESTART()
				ENDIF
			ENDIF
			IF NOT IS_NJVS_SPECTATOR() // NT Fix 1630520
				//Set that we want to use backed up menu selection
				SET_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
			ENDIF
			//Set flag for warp 
			g_sFMMCEOM.bDoWarp = TRUE 
			g_sFMMCEOM.vWarp = serverVars.vRestartPos
			g_sFMMCEOM.contentID = g_FMMC_STRUCT.tl31LoadedContentID
			//GlobalplayerBD_FM[iPlayer].bMissionRestartInProgress = FALSE
			CLEAR_BIT(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bMissionRestartInProgress)
			PRINTLN("FMMC_EOM - FINISH_END_VOTE_CLIENT_SIDE - sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_RESTART")	
		BREAK
		
		// Quit out button
		CASE ciFMMC_EOM_VOTE_STATUS_QUIT
			PRINTLN("FMMC_EOM - FINISH_END_VOTE_CLIENT_SIDE - sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUIT")	
			SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_QUIT_FOR_SCTV)
			//Set the transition session vote status
			SET_TRANSITION_SESSION_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUIT)
		BREAK
	ENDSWITCH
	
	CALL_END_OF_MISSION_NJVS_TELEMETRY(serverVars, bSecondryVote, bWinnerRandom)
	
	//Clean up the corona flag
	CLEAR_ME_AS_READY_FOR_QUICK_RESTART()
	
	//Set the transition session vote status
	SET_TRANSITION_SESSION_VOTE_STATUS(g_sFMMCEOM.iVoteStatus)	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vEOMPos = g_sFMMCEOM.vWarp
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].tl23EOMRestartContentID = g_sFMMCEOM.contentID
	
	//Print it all out.
	PRINTLN("FMMC EOM - FINISH_END_VOTE_CLIENT_SIDE RETURN TRUE")
	PRINTLN("FMMC_EOM - g_sFMMCEOM.vWarp     = ", g_sFMMCEOM.vWarp)
	PRINTLN("FMMC_EOM - g_sFMMCEOM.bDoWarp   = ", g_sFMMCEOM.bDoWarp)
	PRINTLN("FMMC_EOM - g_sFMMCEOM.contentID = ", g_sFMMCEOM.contentID)
	PRINTLN("FMMC_EOM - ,GlobalPlayerBroadcastDataFM_BS_bMissionRestartInProgress = ", IS_BIT_SET(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bMissionRestartInProgress))
	PRINTLN("FMMC_EOM -  [CS_JOB] FINISH_END_VOTE_CLIENT_SIDE, FINISH iVoteStatus = ", g_sFMMCEOM.iVoteStatus)
ENDPROC

FUNC BOOL REQUEST_SCALEFORM_END_OF_JOBS_CARD(NEXT_JOB_STRUCT& nextJobStruct)
	nextJobStruct.scaleformJobsMovie = REQUEST_SCALEFORM_MOVIE("mp_next_job_selection")
	RETURN HAS_SCALEFORM_MOVIE_LOADED(nextJobStruct.scaleformJobsMovie)
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║		GOTO																	║
//╚═════════════════════════════════════════════════════════════════════════════╝	

PROC GOTO_CLIENT_JOB_VOTE_STAGE(INT iStageGoto)
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	#IF IS_DEBUG_BUILD
		SWITCH iStageGoto
			CASE JOB_GET_ASSETS				PRINTLN("[CS_JOB] GOTO_CLIENT_JOB_VOTE_STAGE  >>> JOB_GET_ASSETS  ") 			BREAK	
			CASE JOB_VOTE_REQUEST_ASSETS 	PRINTLN("[CS_JOB] GOTO_CLIENT_JOB_VOTE_STAGE  >>> JOB_VOTE_REQUEST_ASSETS  ") 	BREAK
			CASE JOB_VOTE_DO_VOTING			PRINTLN("[CS_JOB] GOTO_CLIENT_JOB_VOTE_STAGE  >>> JOB_VOTE_DO_VOTING  ") 		BREAK
			CASE JOB_VOTE_REFRESHING 		PRINTLN("[CS_JOB] GOTO_CLIENT_JOB_VOTE_STAGE  >>> JOB_VOTE_REFRESHING  ") 		BREAK
			CASE JOB_VOTE_END				PRINTLN("[CS_JOB] GOTO_CLIENT_JOB_VOTE_STAGE  >>> JOB_VOTE_END  ")				BREAK
		ENDSWITCH
	#ENDIF
	
	GlobalplayerBD_FM[iPlayer].iClientJobLogicStage = iStageGoto
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║		BOBBYS MESSY SCRIPT LOBBED INTO TIDY FUNCTIONS							║
//╚═════════════════════════════════════════════════════════════════════════════╝	

CONST_INT PHOTO_ATTEMPTS 1
CONST_INT ciSAFETY_TIMEOUT_DL_IMAGES 60000

//Get all the images for the next Job
FUNC BOOL GET_ALL_IMAGES_FOR_NEXT_JOB_SCREEN(SERVER_NEXT_JOB_VARS& serverVars, STRUCT_IMAGES_FOR_NEXT_JOB_SCREEN_VARS &sNextJobImages, NEXT_JOB_STRUCT& nextJobStruct)
	BOOL bRETURN = TRUE
	INT iJob = 0
	TEXT_LABEL_23 tl23
	IF SHOULD_USE_OFFLINE_UGC_CONTENT()
		RETURN TRUE
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_31 tlJobName
	#ENDIF
	
	STRING sRtextureName = "2_0.jpg"
	STRING sCloudPath = "njvs/randomimage/"
	
	IF IS_PS3_VERSION()
	OR IS_XBOX360_VERSION()
		sRtextureName = "1_0.jpg"
	ENDIF
	
	//Loop all images
	FOR iJob = 0 TO ((NUM_JOBS_ON_ONE_PAGE *2) -1)
		IF iJob = 0
		AND IS_FIRST_JOB_RANDOM(serverVars)
			IF NOT IS_BIT_SET(sNextJobImages.iDownloadedBitSet, iJob)
				IF DOWNLOAD_PHOTO_FOR_FMMC_MINI_GAME(sNextJobImages.sDownLoadPhotoVars[iJob], sCloudPath, sRtextureName)	//Worked
					IF sNextJobImages.sDownLoadPhotoVars[iJob].bSucess
					OR sNextJobImages.sDownLoadPhotoVars[iJob].iAttempts > PHOTO_ATTEMPTS
						//Set that it has downloaded
						SET_BIT(sNextJobImages.iDownloadedBitSet, iJob)
						IF sNextJobImages.sDownLoadPhotoVars[iJob].bSucess
							PRINTLN("[CS_JOB] GET_ALL_IMAGES_FOR_NEXT_JOB_SCREEN - DOWNLOAD_PHOTO_FOR_FMMC_LITE - Photo [", iJob, "] download Sucess")
							SET_BIT(sNextJobImages.iSucessBitSet, iJob)		
							#IF IS_DEBUG_BUILD
							tlJobName = GET_JOB_NAME(nextJobStruct, iJob, IS_BIT_SET(serverVars.iVerifiedBitSet, iJob))
							PRINTLN(" [CS_JOB] GET_ALL_IMAGES_FOR_NEXT_JOB_SCREEN iJob = ", tlJobName)
							#ENDIF
						ENDIF
					//Failed, try again. 
					ELSE
						sNextJobImages.sDownLoadPhotoVars[iJob].iAttempts++
						RESET_STRUCT_DL_PHOTO_VARS_LITE(sNextJobImages.sDownLoadPhotoVars[iJob], FALSE)
						PRINTLN("[CS_JOB] - GET_ALL_IMAGES_FOR_NEXT_JOB_SCREEN - DOWNLOAD_PHOTO_FOR_FMMC_LITE - Photo [", iJob, "] download FAILED")
						bRETURN = FALSE
					ENDIF
				//not downloaded then clear the TRUE for the retuen
				ELSE	
					bRETURN = FALSE
				ENDIF
			ENDIF
		ELSE
			//If it's not already downloaded
			IF NOT IS_BIT_SET(sNextJobImages.iDownloadedBitSet, iJob)
				tl23 = GET_EOJV_MISSION_CONTENT_ID(nextJobStruct.iArrayPos[iJob], IS_BIT_SET(serverVars.iVerifiedBitSet, iJob))
				//Call the function to download the image
				IF DOWNLOAD_PHOTO_FOR_FMMC_LITE(sNextJobImages.sDownLoadPhotoVars[iJob], tl23, GET_JOB_PHOTO_VERSION(serverVars, nextJobStruct, iJob), PHOTO_ATTEMPTS, GET_JOB_PHOTO_PATH(serverVars, nextJobStruct, iJob))				
					//Worked
					IF sNextJobImages.sDownLoadPhotoVars[iJob].bSucess
					OR sNextJobImages.sDownLoadPhotoVars[iJob].iAttempts > PHOTO_ATTEMPTS
						//Set that it has downloaded
						SET_BIT(sNextJobImages.iDownloadedBitSet, iJob)
						IF sNextJobImages.sDownLoadPhotoVars[iJob].bSucess
							PRINTLN("[CS_JOB] GET_ALL_IMAGES_FOR_NEXT_JOB_SCREEN - DOWNLOAD_PHOTO_FOR_FMMC_LITE - Photo [", iJob, "] download Sucess")
							SET_BIT(sNextJobImages.iSucessBitSet, iJob)		
							#IF IS_DEBUG_BUILD
							tlJobName = GET_JOB_NAME(nextJobStruct, iJob, IS_BIT_SET(serverVars.iVerifiedBitSet, iJob))
							PRINTLN(" [CS_JOB] GET_ALL_IMAGES_FOR_NEXT_JOB_SCREEN iJob = ", tlJobName)
							#ENDIF
						ENDIF
					//Failed, try again. 
					ELSE
						sNextJobImages.sDownLoadPhotoVars[iJob].iAttempts++
						RESET_STRUCT_DL_PHOTO_VARS_LITE(sNextJobImages.sDownLoadPhotoVars[iJob], FALSE)
						PRINTLN("[CS_JOB] - GET_ALL_IMAGES_FOR_NEXT_JOB_SCREEN - DOWNLOAD_PHOTO_FOR_FMMC_LITE - Photo [", iJob, "] download FAILED")
						bRETURN = FALSE
					ENDIF
				//not downloaded then clear the TRUE for the retuen
				ELSE	
					bRETURN = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR	
	
	IF bRETURN = FALSE
		IF NOT HAS_NET_TIMER_STARTED(nextJobStruct.sDLImagesTimeout1)
			START_NET_TIMER(nextJobStruct.sDLImagesTimeout1)
			PRINTLN(" [CS_JOB] GET_ALL_IMAGES_FOR_NEXT_JOB_SCREEN, START_NET_TIMER ")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(nextJobStruct.sDLImagesTimeout1, ciSAFETY_TIMEOUT_DL_IMAGES)
				PRINTLN(" [CS_JOB] GET_ALL_IMAGES_FOR_NEXT_JOB_SCREEN, bRETURN = TRUE ")
				bRETURN = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bRETURN
ENDFUNC

FUNC BOOL GET_SECOND_SET_IMAGES(SERVER_NEXT_JOB_VARS& serverVars, STRUCT_IMAGES_FOR_NEXT_JOB_SCREEN_VARS &sNextJobImages, NEXT_JOB_STRUCT& nextJobStruct)
	BOOL bRETURN = TRUE
	INT iJob = 0
	TEXT_LABEL_23 tl23
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_31 tlJobName
	#ENDIF
	
	IF SHOULD_USE_OFFLINE_UGC_CONTENT()
		RETURN TRUE
	ENDIF
	
	//Loop all images
	FOR iJob = (NUM_JOBS_ON_ONE_PAGE *2) TO (NUM_JOBS_TO_GRAB - 1)
		//If it's not already downloaded
		IF NOT IS_BIT_SET(sNextJobImages.iDownloadedBitSet, iJob)
			//Call the function to download the image
			tl23 = GET_EOJV_MISSION_CONTENT_ID(nextJobStruct.iArrayPos[iJob], IS_BIT_SET(serverVars.iVerifiedBitSet, iJob))
			IF DOWNLOAD_PHOTO_FOR_FMMC_LITE(sNextJobImages.sDownLoadPhotoVars[iJob], tl23, GET_JOB_PHOTO_VERSION(serverVars, nextJobStruct, iJob), PHOTO_ATTEMPTS, GET_JOB_PHOTO_PATH(serverVars, nextJobStruct, iJob))			
				//Worked
				IF sNextJobImages.sDownLoadPhotoVars[iJob].bSucess
				OR sNextJobImages.sDownLoadPhotoVars[iJob].iAttempts > PHOTO_ATTEMPTS
					//Set that it has downloaded
					SET_BIT(sNextJobImages.iDownloadedBitSet, iJob)
					IF sNextJobImages.sDownLoadPhotoVars[iJob].bSucess
						PRINTLN("[CS_JOB] GET_SECOND_SET_IMAGES - DOWNLOAD_PHOTO_FOR_FMMC_LITE - Photo [", iJob, "] download Sucess")
						SET_BIT(sNextJobImages.iSucessBitSet, iJob)		
						#IF IS_DEBUG_BUILD
						tlJobName = GET_JOB_NAME(nextJobStruct, iJob, IS_BIT_SET(serverVars.iVerifiedBitSet, iJob))
						PRINTLN(" [CS_JOB] GET_SECOND_SET_IMAGES iJob = ", tlJobName)
						#ENDIF
					ENDIF
				//Failed, try again. 
				ELSE
					sNextJobImages.sDownLoadPhotoVars[iJob].iAttempts++
					RESET_STRUCT_DL_PHOTO_VARS_LITE(sNextJobImages.sDownLoadPhotoVars[iJob], FALSE)
					PRINTLN("[CS_JOB] - GET_SECOND_SET_IMAGES - DOWNLOAD_PHOTO_FOR_FMMC_LITE - Photo [", iJob, "] download FAILED")
					bRETURN = FALSE
				ENDIF
				
			//not downloaded then clear the TRUE for the retuen
			ELSE	
				bRETURN = FALSE
			ENDIF
		ENDIF
	ENDFOR	
	
	IF bRETURN = FALSE
		IF NOT HAS_NET_TIMER_STARTED(nextJobStruct.sDLImagesTimeout2)
			START_NET_TIMER(nextJobStruct.sDLImagesTimeout2)
			PRINTLN(" [CS_JOB] GET_SECOND_SET_IMAGES, START_NET_TIMER ")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(nextJobStruct.sDLImagesTimeout2, ciSAFETY_TIMEOUT_DL_IMAGES)
				PRINTLN(" [CS_JOB] GET_SECOND_SET_IMAGES, bRETURN = TRUE ")
				bRETURN = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bRETURN
ENDFUNC

// To make way for the next 6, we can only have 16 in memory
PROC RELEASE_FIRST_FEW_TEXTURES(NEXT_JOB_STRUCT& nextJobStruct)
	INT i
	IF nextJobStruct.iReleaseCount < NUM_JOBS_ON_ONE_PAGE
		FOR i = 0 TO (NUM_JOBS_ON_ONE_PAGE - 1)
			IF g_sNextJobImages.sDownLoadPhotoVars[i].iTextureDownloadHandle != -1
				TEXTURE_DOWNLOAD_RELEASE(g_sNextJobImages.sDownLoadPhotoVars[i].iTextureDownloadHandle)
				PRINTLN(" [CS_JOB] RELEASE_TEXTURES - FIRST_FEW_TEXTURES - TEXTURE_DOWNLOAD_RELEASE [SUCCESS] i = ", g_sNextJobImages.sDownLoadPhotoVars[i].iTextureDownloadHandle)
			ENDIF
			g_sNextJobImages.sDownLoadPhotoVars[i].iTextureDownloadHandle = -1
			nextJobStruct.iReleaseCount++
			PRINTLN(" [CS_JOB] RELEASE_FIRST_FEW_TEXTURES - TEXTURE_DOWNLOAD_RELEASE i = ", i, " iReleaseCount = ", nextJobStruct.iReleaseCount)
		ENDFOR
	ENDIF
ENDPROC

CONST_INT RELEASE_DELAY 2000

FUNC BOOL HAVE_TEXTURES_BEEN_RELEASED(NEXT_JOB_STRUCT& nextJobStruct)
	IF nextJobStruct.iReleaseCount = NUM_JOBS_ON_ONE_PAGE
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Grab the final set of images
PROC GET_EXTRA_IMAGES(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	IF ON_SECOND_PAGE_REFRESHED(serverVars)
		IF NOT IS_BIT_SET(g_sNextJobImages.iImageBoolsBitSet, ciIMAGE_RELEASED_FIRST_TEXTURES)
			RELEASE_FIRST_FEW_TEXTURES(nextJobStruct)
			IF HAVE_TEXTURES_BEEN_RELEASED(nextJobStruct)
				PRINTLN(" [CS_JOB] DO_END_JOB_VOTING_SCREEN - HAVE_TEXTURES_BEEN_RELEASED ")
				SET_BIT(g_sNextJobImages.iImageBoolsBitSet, ciIMAGE_RELEASED_FIRST_TEXTURES)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(g_sNextJobImages.iImageBoolsBitSet, ciIMAGE_GOT_SECOND_SET_IMAGES)
				IF GET_SECOND_SET_IMAGES(serverVars, g_sNextJobImages, nextJobStruct)
					PRINTLN(" [CS_JOB] DO_END_JOB_VOTING_SCREEN - GET_EXTRA_IMAGES = TRUE")
					SET_BIT(g_sNextJobImages.iImageBoolsBitSet, ciIMAGE_GOT_SECOND_SET_IMAGES)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GET_IMAGES(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	IF NOT IS_BIT_SET(g_sNextJobImages.iImageBoolsBitSet, ciIMAGE_GOT_IMAGES)
		IF GET_ALL_IMAGES_FOR_NEXT_JOB_SCREEN(serverVars, g_sNextJobImages, nextJobStruct)
			SET_BIT(g_sNextJobImages.iImageBoolsBitSet, ciIMAGE_GOT_IMAGES)
			PRINTLN(" [CS_JOB] DO_END_JOB_VOTING_SCREEN - GET_ALL_IMAGES_FOR_NEXT_JOB_SCREEN = TRUE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_CLIENT_GOT_ARRAY_POSITIONS(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	//If the number has not been initilised
	IF GET_NUMBER_OF_JOBS_GRABBED(serverVars) = -1
		PRINTLN("[CS_JOB] [2001613] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN, HAS_CLIENT_GOT_ARRAY_POSITIONS - GET_NUMBER_OF_JOBS_GRABBED(serverVars) =  ", GET_NUMBER_OF_JOBS_GRABBED(serverVars))
	//we've got the number of jobs that are compatable
	ELIF nextJobStruct.iFoundArrayPosCounter >= GET_NUMBER_OF_JOBS_GRABBED(serverVars)
		RETURN TRUE
	ELSE
		//If we should be random for the first mission!
		IF (IS_FIRST_JOB_RANDOM(serverVars)
		AND nextJobStruct.iFoundArrayPosCounter = 0)
			PRINTLN("[CS_JOB] [NJVSQMR] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN - IS_BIT_SET(serverVars.iServerJobBitSet, IF IS_FIRST_JOB_RANDOM)  - nextJobStruct.iFoundArrayPosCounter++")
			nextJobStruct.iFoundArrayPosCounter++
		//If the string is empty
		ELIF IS_STRING_NULL_OR_EMPTY(serverVars.tlFileName[nextJobStruct.iFoundArrayPosCounter])
			PRINTLN("[CS_JOB] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN, idVariation = ", nextJobStruct.missionIdData.idVariation)
			Clear_MP_MISSION_ID_DATA_Struct(nextJobStruct.missionIdData)
			nextJobStruct.iFoundArrayPosCounter++
			PRINTLN("[CS_JOB] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN, iFoundArrayPosCounter = ", nextJobStruct.iFoundArrayPosCounter)
			PRINTLN("[CS_JOB] IS_STRING_NULL_OR_EMPTY(serverVars.tlFileName[nextJobStruct.iFoundArrayPosCounter])")
		ELIF Search_For_And_Retrieve_MissionIdData_From_Cloud_Filename(serverVars.tlFileName[nextJobStruct.iFoundArrayPosCounter], nextJobStruct.missionIdData, nextJobStruct.bSecondProcess)
		#IF IS_DEBUG_BUILD
		AND nextJobStruct.iFoundArrayPosCounter != g_iEndOfJobScreenJobToFail
		AND nextJobStruct.iFoundArrayPosCounter != g_iEndOfJobScreenJobToFail2
		AND g_iEndOfJobScreenJobToFailAll = FALSE
		#ENDIF
//			nextJobStruct.dataIsLocallyAvailable[nextJobStruct.iFoundArrayPosCounter] = TRUE
			nextJobStruct.iArrayPos[nextJobStruct.iFoundArrayPosCounter] = nextJobStruct.missionIdData.idVariation
			PRINTLN("[CS_JOB] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN, idVariation = ", nextJobStruct.missionIdData.idVariation)
			Clear_MP_MISSION_ID_DATA_Struct(nextJobStruct.missionIdData)
			nextJobStruct.iFoundArrayPosCounter++
			PRINTLN("[CS_JOB] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN, iFoundArrayPosCounter = ", nextJobStruct.iFoundArrayPosCounter)
		//We failed to find it, set a bit and add it to the list that need downloaded
		ELSE
			Clear_MP_MISSION_ID_DATA_Struct(nextJobStruct.missionIdData)
			PRINTLN("[CS_JOB] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN, mission not found = ", nextJobStruct.iFoundArrayPosCounter, " = ", serverVars.tlFileName[nextJobStruct.iFoundArrayPosCounter])			
			SET_BIT(nextJobStruct.iNotFoundBitSet, nextJobStruct.iFoundArrayPosCounter)			
			nextJobStruct.iFoundArrayPosCounter++
			nextJobStruct.iNotFoundCount++
			PRINTLN("[CS_JOB] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN, iFoundArrayPosCounter = ", nextJobStruct.iFoundArrayPosCounter)			
			PRINTLN("[CS_JOB] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN, iNotFoundCount        = ", nextJobStruct.iNotFoundCount)			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_DESCRIPTIONS(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_GOT_DESCRIPTIONS)	
		IF HAVE_DESCRIPTIONS_LOADED(serverVars, nextJobStruct)
			PRINTLN(" [CS_JOB] - HAVE_DESCRIPTIONS_LOADED ")
			SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_GOT_DESCRIPTIONS)
		ENDIF
	ENDIF
ENDPROC

PROC RELEASE_TEXTURES()
	INT i
	//IF IS_BIT_SET(g_sNextJobImages.iImageBoolsBitSet, ciIMAGE_GOT_IMAGES)
		REPEAT NUM_JOBS_TO_GRAB i
			IF g_sNextJobImages.sDownLoadPhotoVars[i].iTextureDownloadHandle != -1
				TEXTURE_DOWNLOAD_RELEASE(g_sNextJobImages.sDownLoadPhotoVars[i].iTextureDownloadHandle)
				PRINTLN(" [CS_JOB] RELEASE_TEXTURES - TEXTURE_DOWNLOAD_RELEASE [SUCCESS] i = ", g_sNextJobImages.sDownLoadPhotoVars[i].iTextureDownloadHandle)
			ENDIF
			g_sNextJobImages.sDownLoadPhotoVars[i].iTextureDownloadHandle = -1
			PRINTLN(" [CS_JOB] RELEASE_TEXTURES - TEXTURE_DOWNLOAD_RELEASE i = ", i)
		ENDREPEAT
	//ENDIF
ENDPROC

PROC BLOCK_CASH()
	REMOVE_MULTIPLAYER_WALLET_CASH() 
	REMOVE_MULTIPLAYER_BANK_CASH()
	PRINTLN("BLOCK_CASH TRUE")
ENDPROC

PROC CLEANUP_END_JOB_VOTING(NEXT_JOB_STRUCT& nextJobStruct)
	
//	IF SHOULD_SKIP_NJVS()
//		EXIT
//	ENDIF

	RELEASE_TEXTURES()
	UGC_RELEASE_ALL_CACHED_DESCRIPTIONS()
	IF HAS_SCALEFORM_MOVIE_LOADED(nextJobStruct.scaleformJobsMovie)
		BEGIN_SCALEFORM_MOVIE_METHOD(nextJobStruct.scaleformJobsMovie, "CLEANUP_MOVIE")
		END_SCALEFORM_MOVIE_METHOD()
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(nextJobStruct.scaleformJobsMovie)
	ENDIF
	NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE)
	CLEAR_CLIENT_READY_TO_VOTE()
	SET_NEW_JOB_VOTE_GLOBAL(FALSE)
	// 2124373
	SHOW_GAME_TOOL_TIPS(FALSE)								
	THEFEED_FLUSH_QUEUE()
	PRINTLN(" [CS_JOB] CLEANUP_END_JOB_VOTING ")
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║		CLIENT																	║
//╚═════════════════════════════════════════════════════════════════════════════╝	

PROC OPEN_UP_JOB_CHAT()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN(" [CS_JOB] OPEN_UP_JOB_CHAT")
		PRINTLN(" [CS_JOB] NETWORK_SET_TALKER_PROXIMITY(0)")
		NETWORK_SET_TALKER_PROXIMITY(0.0)
		PRINTLN(" [CS_JOB] NETWORK_SET_TEAM_ONLY_CHAT(FALSE)", 0.0)
		NETWORK_SET_TEAM_ONLY_CHAT(FALSE)
		NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_PLAYERS_ON_END_JOB()
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))	
			PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF NOT IS_PLAYER_SCTV(playerId)
				IF IS_NET_PLAYER_OK(PlayerId, FALSE, FALSE)
					INT iPlayerSlot = NATIVE_TO_INT(playerId)
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[iPlayerSlot].bQuitJob
				
						PRINTLN("DEBUG_PRINT_PLAYERS_ON_END_JOB,  i = ", i, " Player = ", GET_PLAYER_NAME(PlayerId))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC PRINT_USEFUL_END_JOB_VOTE_STUFF(SERVER_NEXT_JOB_VARS& serverVars)
	PRINTLN(" [CS_JOB]  PRINT_USEFUL_END_JOB_VOTE_STUFF ")
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PRINTLN(" [CS_JOB]  I_AM_DM_HOST ", NETWORK_PLAYER_ID_TO_INT())
	ELSE
		PRINTLN(" [CS_JOB] I_AM_DM_CLIENT ", NETWORK_PLAYER_ID_TO_INT())
		PRINTLN("HOST_IS_CALLED ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))  
	ENDIF
	PRINTLN("  [CS_JOB] iNumberOnJob =  ", serverVars.iNumberOnJob)
	PRINTLN("  [CS_JOB] iHighestPlayerRank =  ", serverVars.iHighestPlayerRank)
	PRINTLN("  [CS_JOB] iAverageRank =  ", serverVars.iAverageRank)
	PRINTLN("  [CS_JOB] GET_JOB_VOTE_STAGE() =  ", GET_JOB_VOTE_STAGE())
	PRINTLN("  [CS_JOB] GET_JOB_SERVER_STAGE =  ", GET_JOB_SERVER_STAGE(serverVars))
	DEBUG_PRINT_PLAYERS_ON_END_JOB()
ENDPROC

PROC DEBUG_REFRESH(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	BOOL bFirstTileOnly = FALSE
	IF g_bDebugRefresh
		g_bDebugRefresh = FALSE
		REFRESH_SCREENS_NOW(serverVars, nextJobStruct, bFirstTileOnly)
		REFRESH_VOTES(serverVars, nextJobStruct, bFirstTileOnly)
	ENDIF
ENDPROC

#ENDIF

PROC HARD_REFRESH(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, BOOL bFirstTileOnly)
	SET_NEXT_JOB_SLOTS_GREYED_OUT(nextJobStruct, FALSE) // B*2214450 Ensure the greyed out flag gets reset
	REFRESH_SCREENS_NOW(serverVars, nextJobStruct, bFirstTileOnly)
	REFRESH_VOTES(serverVars, nextJobStruct, bFirstTileOnly)
ENDPROC
//
//TWEAK_INT tiNJVS_REFRESH_TIME 1000
//
//PROC REFRESH_NJVS_IF_ALTERNATING(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)	
//	BOOL bFirstTileOnly = FALSE
//	IF SHOULD_ALTERNATE_IMAGES(serverVars)	
//	
//		// Start refreshing after the images have been onscreen for a bit
//		IF HAS_NET_TIMER_STARTED(serverVars.timerVotingEnds)
//			IF HAS_NET_TIMER_EXPIRED(serverVars.timerVotingEnds, tiNJVS_REFRESH_TIME)
//			// Stop near the end
//			AND NOT HAS_NET_TIMER_EXPIRED(serverVars.timerVotingEnds, (g_sMPTunables.iNjvsTimeOut - 2500))
//			// Unless debugging
//			#IF IS_DEBUG_BUILD OR g_bNeverTimeOut #ENDIF
//	
//				// Start refresh timer
//				IF NOT HAS_NET_TIMER_STARTED(nextJobStruct.sImageTimer)
//					START_NET_TIMER(nextJobStruct.sImageTimer)
//				ELIF HAS_NET_TIMER_EXPIRED(nextJobStruct.sImageTimer, tiNJVS_REFRESH_TIME)
//					nextJobStruct.iImageToDisplay++
//					IF nextJobStruct.iImageToDisplay >= NUM_JOBS_ON_ONE_PAGE
//						nextJobStruct.iImageToDisplay = 1
//					ENDIF
//					RESET_NET_TIMER(nextJobStruct.sImageTimer)
//					bFirstTileOnly = TRUE
//					HARD_REFRESH(serverVars, nextJobStruct, bFirstTileOnly)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

FUNC BOOL READY_TO_MOVE_TO_NEXT_JOBS(NEXT_JOB_STRUCT& nextJobStruct)
	RETURN IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_READY_TO_GO)
ENDFUNC

PROC SET_READY_FOR_NEXT_JOBS(NEXT_JOB_STRUCT& nextJobStruct)

	IF SHOULD_SKIP_NJVS()
		
		EXIT
	ENDIF


	IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_READY_TO_GO)
		PRINTLN("[CS_JOB] SET_READY_FOR_NEXT_JOBS ")
		SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_READY_TO_GO)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CLIENT_REFRESH(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct)
	IF nextJobStruct.iNumClientRefreshes = 0
		IF ON_SECOND_PAGE_REFRESHED(serverVars)
		
			RETURN TRUE
		ENDIF
	ELSE
		IF nextJobStruct.iNumClientRefreshes = 1
			IF ON_LAST_PAGE_REFRESHED(serverVars)
			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

CONST_INT REFRESH_TIME 1500

FUNC BOOL HAS_SAFE_REFRESH_TIMER_FINISHED(SERVER_NEXT_JOB_VARS& serverVars)
	IF HAS_NET_TIMER_STARTED(serverVars.timerServerRefresh)
		IF HAS_NET_TIMER_EXPIRED(serverVars.timerServerRefresh, REFRESH_TIME)
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



PROC START_END_JOB_AUDIO()
	SET_AUDIO_FLAG("PlayMenuMusic", TRUE)
	PRINTLN("START_END_JOB_AUDIO")
ENDPROC

PROC STOP_END_JOB_AUDIO()
	SET_AUDIO_FLAG("PlayMenuMusic", FALSE)
	PRINTLN("STOP_END_JOB_AUDIO")
ENDPROC

FUNC BOOL SHOULD_DO_END_JOB_AUDIO()
	IF CONTENT_IS_USING_ARENA()
		PRINTLN("SHOULD_DO_END_JOB_AUDIO - Arena Wars mode, not using end job audio.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// Process end Job voting
FUNC BOOL DO_END_JOB_VOTING_SCREEN(SERVER_NEXT_JOB_VARS& serverVars, NEXT_JOB_STRUCT& nextJobStruct, TIME_DATATYPE Timer)
	
	BOOL bFirstTileOnly = FALSE 
	
	IF SHOULD_SKIP_NJVS()
		
		RETURN TRUE
	ENDIF

	//If the cloud failed then we need to bail
	IF HAS_SERVER_GRABBED_JOBS(serverVars)
	AND READY_TO_MOVE_TO_NEXT_JOBS(nextJobStruct)
		IF IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_CLOUD_BAIL)
		#IF IS_DEBUG_BUILD
		OR g_bForceCloudEndOfJobFail
		#ENDIF
			SET_WARNING_MESSAGE("CLOUD_LD_FL", FE_WARNING_OK, "CLOUD_LD_FL2")
			PRINTLN(" [CS_JOB] DO_END_JOB_VOTING_SCREEN - SET_WARNING_MESSAGE on cloud bail")	
			SET_PLAYER_QUIT_JOB(nextJobStruct) // This needs to be before the button press else array overrun 1644403
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) 
				PRINTLN(" [CS_JOB] DO_END_JOB_VOTING_SCREEN - SET_PLAYER_QUIT_JOB on cloud bail")
				GOTO_CLIENT_JOB_VOTE_STAGE(JOB_VOTE_END)
				#IF IS_DEBUG_BUILD
				g_bForceCloudEndOfJobFail = FALSE
				#ENDIF
				CLEAR_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_CLOUD_BAIL)
			ENDIF
			RETURN FALSE			
		ENDIF
	ENDIF
	
	STRING stPartyWarning = ""	
	
	IF READY_TO_MOVE_TO_NEXT_JOBS(nextJobStruct)
		SET_CLIENT_READY_TO_VOTE()
		DO_JOB_VOTE_SCREEN_BUTTONS(nextJobStruct, Timer)
		DISABLE_CELLPHONE_THIS_FRAME_ONLY() 
		HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE) // Heli overlay 1735330
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		DISPLAY_RADAR(FALSE) // Fix for 1806324.
		HIDE_HELP_TEXT_THIS_FRAME()
		BLOCK_CASH()
		GET_DESCRIPTIONS(serverVars, nextJobStruct)
		GET_EXTRA_IMAGES(serverVars, nextJobStruct)	
		//Block invites and people Jipping in
		IF NOT IS_BIT_SET(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_SET_BLOCKING)
			SET_BLOCKING_STATUS_OF_INVITES_AND_REQUESTS()
			#IF IS_DEBUG_BUILD
			PRINTLN("READY_TO_MOVE_TO_NEXT_JOBS, PRINT_USEFUL_END_JOB_VOTE_STUFF")
			PRINT_USEFUL_END_JOB_VOTE_STUFF(serverVars)
			#ENDIF
			IF IS_PC_VERSION()
				CLOSE_MP_TEXT_CHAT() // url:bugstar:2201855. ST.
			ENDIF
			SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_SET_BLOCKING)
		ENDIF
	ENDIF
	
	// Draw the images 
	IF GET_JOB_VOTE_STAGE() = JOB_VOTE_DO_VOTING
	OR GET_JOB_VOTE_STAGE() = JOB_VOTE_END
		IF NOT DID_QUIT_END_JOB_VOTING(nextJobStruct)
			DRAW_THE_END_OF_JOBS_PANELS_ONSCREEN(nextJobStruct)
			//REFRESH_NJVS_IF_ALTERNATING(serverVars, nextJobStruct)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_NJVS_Spam
	PRINTLN("[CS_JOB] SPAM, GET_NUMBER_OF_JOBS_GRABBED =  ", GET_NUMBER_OF_JOBS_GRABBED(serverVars))
	ENDIF
	#ENDIF
	
	SWITCH GET_JOB_VOTE_STAGE()
	
		CASE JOB_GET_ASSETS
			IF IS_BIT_SET(serverVars.iCheckPlayersHaveJobsBitSet, ciPLAYERS_SET_ON_END_OF_JOB)
				IF NOT IS_LOCAL_PLAYER_ON_END_OF_JOB_VOTE_SCREEN()
					RESET_EXTRA_ROCKSTAR_CREATED_SLOTS()
					nextJobStruct.bSecondProcess = FALSE
				ENDIF
				SET_ME_ON_END_OF_JOB_VOTE_SCREEN()
			ENDIF
			IF HAS_SERVER_GRABBED_JOBS(serverVars)
				IF NOT SHOULD_RUN_ROCKSTAR_CONTENT_REFRESH()
					SET_ME_NO_REFRESH_AS_END_OF_JOB_VOTE_SCREEN()
					#IF IS_DEBUG_BUILD
					IF g_bEndOfJobScreenDoRefresh
						BROADCAST_GENERAL_EVENT(GENERAL_EVENT_REFRESH_FM_ROCKSTAR_CREATED, ALL_PLAYERS())
						g_bEndOfJobScreenDoRefresh = FALSE
					ENDIF

					#ENDIF
					//Get the array positions of the missions
					IF HAS_CLIENT_GOT_ARRAY_POSITIONS(serverVars, nextJobStruct)
						//If we got all of them sucessfully then sweet
						IF nextJobStruct.iNotFoundBitSet = 0
							GET_IMAGES(serverVars, nextJobStruct)
							IF READY_TO_MOVE_TO_NEXT_JOBS(nextJobStruct)
								IF IS_BIT_SET(g_sNextJobImages.iImageBoolsBitSet, ciIMAGE_GOT_IMAGES)
									nextJobStruct.iJobPanelSelected = 0
									SET_NEW_JOB_VOTE_GLOBAL(TRUE)
									Clear_MP_MISSION_ID_DATA_Struct(nextJobStruct.missionIdData)
									SHOW_GAME_TOOL_TIPS(FALSE)		
									
									GOTO_CLIENT_JOB_VOTE_STAGE(JOB_VOTE_REQUEST_ASSETS)
								#IF IS_DEBUG_BUILD
								ELSE
								PRINTLN(" [CS_JOB] JOB_GET_ASSETS stuck ciIMAGE_GOT_IMAGES ")
								#ENDIF
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN(" [CS_JOB] JOB_GET_ASSETS stuck READY_TO_MOVE_TO_NEXT_JOBS ")
							#ENDIF
							ENDIF
						//We failed to get the missions then download them 
						ELIF DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN(serverVars, nextJobStruct.sGetUGC_content, nextJobStruct.iNotFoundBitSet, nextJobStruct.iNotFoundCount, nextJobStruct.iOffset)
							//if we got the content:
							IF nextJobStruct.sGetUGC_content.bSuccess			
								#IF IS_DEBUG_BUILD
								IF g_iEndOfJobScreenJobToFailAll 
									g_iEndOfJobScreenJobToFailAll = FALSE
								ENDIF
								//IF nextJobStruct.iOffset != 0
									#IF IS_DEBUG_BUILD
									g_iEndOfJobScreenJobToFail = -1
									#ENDIF
									#IF IS_DEBUG_BUILD
									g_iEndOfJobScreenJobToFail2 = -1
									#ENDIF
								//ENDIF
								#ENDIF
								nextJobStruct.iOffset = nextJobStruct.iNotFoundCount
								PRINTLN(" [CS_JOB] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN - nextJobStruct.iOffset = ", nextJobStruct.iOffset)
								IF nextJobStruct.iOffset > MAX_CREATOR_RESULTS
									nextJobStruct.iOffset = MAX_CREATOR_RESULTS									
									PRINTLN(" [CS_JOB] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN - nextJobStruct.iOffset = MAX_CREATOR_RESULTS = ", MAX_CREATOR_RESULTS)
								ENDIF
								nextJobStruct.iNotFoundBitSet = 0
								nextJobStruct.iFoundArrayPosCounter = 0
								nextJobStruct.iNotFoundCount = 0	
								nextJobStruct.bSecondProcess = TRUE
								RESET_UGC_LOAD_VARS(nextJobStruct.sGetUGC_content)		
								PRINTLN(" [CS_JOB] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN - DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN")
							//We failed to get the content then we need to give a warning and bail as a quit
							ELSE
								SET_BIT(nextJobStruct.iClientJobBitSet, CLIENT_BOOL_CLOUD_BAIL)
								PRINTLN(" [CS_JOB] DOWNLOAD_MISSING_ROCKSTAR_CONTENT_FOR_END_OF_JOB_SCREEN - CLIENT_BOOL_CLOUD_BAIL")
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN(" [CS_JOB] CLIENT_JOB_VOTE_GRAB_IMAGES - stuck limbo, CANDIDATE ARGS?")
						#ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN(" [CS_JOB] CLIENT_JOB_VOTE_GRAB_IMAGES - HAS_CLIENT_GOT_ARRAY_POSITIONS iFoundArrayPosCounter = ", nextJobStruct.iFoundArrayPosCounter)
						#ENDIF
					ENDIF
				ELSE
					PRINTLN(" [CS_JOB] CLIENT_JOB_VOTE_GRAB_IMAGES - SHOULD_RUN_ROCKSTAR_CONTENT_REFRESH")
					IF IS_LOCAL_PLAYER_SET_AS_NO_REFRESH_AS_END_OF_JOB_VOTE_SCREEN()
						CLEAR_SHOULD_RUN_ROCKSTAR_CONTENT_REFRESH()
					ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF READY_TO_MOVE_TO_NEXT_JOBS(nextJobStruct)
					PRINTLN(" [CS_JOB] JOB_GET_ASSETS - HAS_SERVER_GRABBED_JOBS = FALSE - HOST_IS_CALLED ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())), "  serverVars.iServerJobBitSet = ", serverVars.iServerJobBitSet)  
				ENDIF
			#ENDIF
			ENDIF
		BREAK
		
		// Load in assets
		CASE JOB_VOTE_REQUEST_ASSETS
			IF g_sMPTunables.bStuntSeriesSkipNJVS
			AND (IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CORONA_FLOW_V2)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ADVERSARY_SERIES_V2)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_TARGET_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HOTRING_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ASSASSINATION_MISSION)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_ARENA_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_RACE_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_SURVIVAL_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_OPEN_WHEEL_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_STREET_RACE_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_PURSUIT_SERIES)
			#IF FEATURE_GEN9_EXCLUSIVE
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_HSW_RACE_SERIES)
			#ENDIF
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_COMMUNITY_SERIES)
			OR IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_IS_CAYO_PERICO_SERIES)
			)
				IF HAS_SERVER_PICKED_WINNER(serverVars)			
					GOTO_CLIENT_JOB_VOTE_STAGE(JOB_VOTE_END)
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN(" [CS_JOB] READY_TO_MOVE_TO_NEXT_JOBS, HAS_SERVER_PICKED_WINNER = FALSE")
					#ENDIF
				ENDIF
			ELIF REQUEST_SCALEFORM_END_OF_JOBS_CARD(nextJobStruct)
				SET_UP_END_JOB_SCREENS(serverVars, nextJobStruct, TRUE, bFirstTileOnly)				
				OPEN_UP_JOB_CHAT()
				
				IF SHOULD_DO_END_JOB_AUDIO()
					START_END_JOB_AUDIO()
				ENDIF
				
				ADD_MISSION_TO_RECENT_HISTORY()
				INITIALISE_SCROLL(serverVars, nextJobStruct)
				#IF IS_DEBUG_BUILD
				PRINTLN("READY_TO_MOVE_TO_NEXT_JOBS, PRINT_USEFUL_END_JOB_VOTE_STUFF")
				PRINT_USEFUL_END_JOB_VOTE_STUFF(serverVars)
				PRINTLN("DO_END_JOB_VOTING_SCREEN, CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)")
				#ENDIF		
				// 2190881
				CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
				GOTO_CLIENT_JOB_VOTE_STAGE(JOB_VOTE_DO_VOTING)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN(" [CS_JOB] READY_TO_MOVE_TO_NEXT_JOBS, REQUEST_SCALEFORM_END_OF_JOBS_CARD = FALSE")
				#ENDIF
			ENDIF
		BREAK
		
		CASE JOB_VOTE_DO_VOTING
			REFRESH_VOTES(serverVars, nextJobStruct, bFirstTileOnly)
			DRAW_PLAYER_LIST(nextJobStruct)
			SHOW_PLAYER_VOTING(nextJobStruct)
			IF SHOULD_CLIENT_REFRESH(serverVars, nextJobStruct)
			OR TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING()
				nextJobStruct.iNumClientRefreshes++
				
				// 1816873
				IF DID_I_VOTE_FOR_RETURN_GTA_ONLINE() 
				OR TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING()
					// Clear vote so it doesn't linger for refreshing players
					RESET_CLIENT_VOTING(nextJobStruct)
					
					////////// 1816873
					FINISH_END_VOTE_CLIENT_SIDE(serverVars, nextJobStruct)	
					//Set that we were on the NJVS if we launched a live tile
					IF TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING()
						SET_TRANSITION_SESSION_LIVE_TILE_SKIP_SET_UP_BECAUSE_ON_NJVS()
					ENDIF
					STOP_END_JOB_AUDIO()
					RETURN TRUE
					//////////					
				
					PRINTLN("[CS_JOB] DID_I_VOTE_FOR_RETURN_GTA_ONLINE don't refresh, instead quit ") 

					GOTO_CLIENT_JOB_VOTE_STAGE(JOB_VOTE_END)
				ELSE
					// Clear vote so it doesn't linger for refreshing players
					RESET_CLIENT_VOTING(nextJobStruct)
				
					GOTO_CLIENT_JOB_VOTE_STAGE(JOB_VOTE_REFRESHING)
				ENDIF
			ELSE		
				IF HAS_SERVER_PICKED_WINNER(serverVars)			
					
					GOTO_CLIENT_JOB_VOTE_STAGE(JOB_VOTE_END)
				ELSE	
					// QUIT CONFIRM SCREEN		
					IF IS_QUIT_SCREEN_ON(nextJobStruct)	
						QUIT_SCREEN_TOGGLE_OFF(nextJobStruct)
						
						//If we're in a party
						IF IS_XBOX_PLATFORM()
							IF NETWORK_GET_PLATFORM_PARTY_MEMBER_COUNT() > 1
								stPartyWarning = "FM_CSC_QUITP"			
							ENDIF
						ENDIF
						
						IF serverVars.iNumberOnJob > 1						
							// "Are you sure you want to quit the voting screen? You will be separated from the current group of players."
							SET_WARNING_MESSAGE_WITH_HEADER("FM_CSC_QUIT", "FM_END_QUIT", (FE_WARNING_YES | FE_WARNING_NO), stPartyWarning)
						ELSE
							// "Are you sure you want to quit the voting screen?"
							SET_WARNING_MESSAGE_WITH_HEADER("FM_CSC_QUIT", "FM_END_1QUIT", (FE_WARNING_YES | FE_WARNING_NO), stPartyWarning)
						ENDIF
						IF HAS_YES_BUTTON_BEEN_PRESSED()
							PLAY_SOUND_FRONTEND(-1, "EXIT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							VOTE_FOR_THIS_JOB(serverVars, JOB_PANEL_EXIT)
							SET_PLAYER_QUIT_JOB(nextJobStruct)
							
							GOTO_CLIENT_JOB_VOTE_STAGE(JOB_VOTE_END)
						ENDIF
					ELSE
						// DRAWING THE VOTE TILES AND CLIENT VOTING LOGIC
						#IF IS_DEBUG_BUILD	DEBUG_REFRESH(serverVars, nextJobStruct) #ENDIF
						
						IF NOT IS_WARNING_MESSAGE_ACTIVE()
							// Up down left right
							CHECK_FOR_DIRECTION_PRESSES(serverVars, nextJobStruct)
							
							IF IS_PLAYER_LIST_ON(nextJobStruct)
	//						OR IS_AMMO_WARNING_SCREEN_ACTIVE(nextJobStruct)
								IF nextJobStruct.iJobPanelSelected <> JOB_PANEL_ZERO
									MOVE_TO_JOB_PANEL(serverVars, nextJobStruct, JOB_PANEL_ZERO)
								ENDIF
							ELSE
								// X to vote
								CHECK_FOR_BUTTON_VOTES(serverVars, nextJobStruct)
							ENDIF
							
							// Listen out for quit screen toggle
							QUIT_SCREEN_TOGGLE_ON(nextJobStruct)
							// Listen out for player list toggle
							PLAYER_LIST_TOGGLE(nextJobStruct)
							// Allow players to buy armour/ammo
	//						HANDLE_BUYING_ARMOUR(nextJobStruct)
	//						HANDLE_BUYING_AMMO(serverVars, nextJobStruct)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE JOB_VOTE_REFRESHING
			RESET_CLIENT_VOTING(nextJobStruct)
			IF HAS_SERVER_CHECKED_CLIENTS_READY_FOR_REFRESH(serverVars)
			AND GET_JOB_SERVER_STAGE(serverVars) = VOTE_SERVER_PROCESS // 2059792
				IF HAS_SAFE_REFRESH_TIMER_FINISHED(serverVars)
					HARD_REFRESH(serverVars, nextJobStruct, bFirstTileOnly)
					
					GOTO_CLIENT_JOB_VOTE_STAGE(JOB_VOTE_DO_VOTING)
				ENDIF
			ELSE
				PRINTLN(" [CS_JOB] DO_END_JOB_VOTING_SCREEN, JOB_VOTE_REFRESHING  ")
			ENDIF
		BREAK
		
		// Finish up
		CASE JOB_VOTE_END	
			IF NOT HAS_NET_TIMER_STARTED(nextJobStruct.timerShowResult)
				FINISH_END_VOTE_CLIENT_SIDE(serverVars, nextJobStruct)	
				STOP_END_JOB_AUDIO()
				PRINTLN(" [CS_JOB] JOB_VOTE_END, START_NET_TIMER  ")
				START_NET_TIMER(nextJobStruct.timerShowResult)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(nextJobStruct.timerShowResult, SHOW_RESULTS_TIME)
				
					PRINTLN(" [CS_JOB] DO_END_JOB_VOTING_SCREEN - RETURN TRUE ")
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║		SERVER																	║
//╚═════════════════════════════════════════════════════════════════════════════╝	

PROC RESET_TIMER_IF_HOST_MIGRATES(SERVER_NEXT_JOB_VARS& serverVars)
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF NOT IS_BIT_SET(serverVars.iHostMigrateBitSet, iPlayer)
		IF serverVars.playerCurrentJobHost <> PLAYER_ID()
			REINIT_NET_TIMER(serverVars.timerVotingEnds)
			START_NET_TIMER(serverVars.timerVotingEnds)
			serverVars.playerCurrentJobHost = PLAYER_ID()
			SET_BIT(serverVars.iHostMigrateBitSet, iPlayer)
			PRINTLN(" [CS_JOB] RESET_TIMER_IF_HOST_MIGRATES ")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_COUNT_VOTES(SERVER_NEXT_JOB_VARS& serverVars)

	IF GET_JOB_SERVER_STAGE(serverVars) = VOTE_SERVER_PROCESS
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SET_PAGE_BITS(SERVER_NEXT_JOB_VARS& serverVars)
	IF serverVars.iNumRefreshed = 0
		IF NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_SECOND_PAGE)
			PRINTLN("[CS_JOB] [HOST] SERVER_BOOL_SECOND_PAGE")
			
			SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_SECOND_PAGE)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_LAST_PAGE)
			PRINTLN("[CS_JOB] [HOST]  SERVER_BOOL_LAST_PAGE")
			SET_BIT(serverVars.iServerJobBitSet, SERVER_BOOL_LAST_PAGE)
		ENDIF
	ENDIF
ENDPROC

PROC START_SAFE_REFRESH_TIMER(SERVER_NEXT_JOB_VARS& serverVars)
	IF NOT HAS_NET_TIMER_STARTED(serverVars.timerServerRefresh)
		PRINTLN(" [CS_JOB] SERVER_COUNTING - START_SAFE_REFRESH_TIMER =  ")
		START_NET_TIMER(serverVars.timerServerRefresh)
	ENDIF
ENDPROC

PROC RESET_SAFE_REFRESH_TIMER(SERVER_NEXT_JOB_VARS& serverVars)
	PRINTLN(" [CS_JOB] SERVER_COUNTING - RESET_NET_TIMER =  ")
	REINIT_NET_TIMER(serverVars.timerServerRefresh)
ENDPROC

//Check to see if we need to run a rockstar created refresh
FUNC BOOL NEED_TO_RUN_ROCKSTAR_CREATED_HEADER_REFRESH()
	#IF IS_DEBUG_BUILD
		IF g_bFakeContentMissMatch 
			RETURN TRUE
		ENDIF
	#ENDIF
	PARTICIPANT_INDEX temppart
	PLAYER_INDEX playerId 
	INT iparticipant
	INT iPlayer 
	INT iHash
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		temppart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
			playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF NOT IS_PLAYER_SCTV(playerId)
				iPlayer = NATIVE_TO_INT(playerId)
				IF GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iHash != 0
					//If the server has is 0 then set it to this players
					IF iHash = 0
						iHash = GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iHash
						PRINTLN("[CS_JOB] [HOST] - CLOUD REF - NEED_TO_RUN_ROCKSTAR_CREATED_HEADER_REFRESH iHash = ", iHash)
					//Server has is not zero
					ELSE
						//If they are not equal then tell every body about it. as we want a content refresh 
						IF iHash != GlobalplayerBD_FM[iPlayer].sDownLoadedContent.iHash
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT	
	RETURN FALSE
ENDFUNC

PROC SERVER_JOB_VOTE_PROCESSING(SERVER_NEXT_JOB_VARS& serverVars)

	IF SHOULD_SKIP_NJVS()
		
		EXIT
	ENDIF

	// Grab Jobs
	SERVER_COUNTING(serverVars, SHOULD_COUNT_VOTES(serverVars))
	SERVER_STARTS_VOTE_TIMER(serverVars, FALSE)

	SWITCH GET_JOB_SERVER_STAGE(serverVars)
	
		CASE VOTE_SERVER_INIT
			SET_BIT(serverVars.iCheckPlayersHaveJobsBitSet, ciPLAYERS_SET_ON_END_OF_JOB)
			//Check to see if we need to run a content refresh
			IF NOT IS_BIT_SET(serverVars.iCheckPlayersHaveJobsBitSet, ciCHECK_PLAYERS_JOBS_INT)
				PRINTLN("[CS_JOB] [HOST] SERVER_JOB_VOTE_PROCESSING, VOTE_SERVER_INIT, IS_BIT_SET(serverVars.iCheckPlayersHaveJobsBitSet, ciCHECK_PLAYERS_JOBS_INT)")
				IF NEED_TO_RUN_ROCKSTAR_CREATED_HEADER_REFRESH()
					PRINTLN("[CS_JOB] [HOST] SERVER_JOB_VOTE_PROCESSING, VOTE_SERVER_INIT, NEED_TO_RUN_ROCKSTAR_CREATED_HEADER_REFRESH returning TRUE ")
					//Send the refresh event
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_REFRESH_FM_ROCKSTAR_CREATED, ALL_PLAYERS())
					//Set that we need to refresh
					SET_SHOULD_RUN_ROCKSTAR_CONTENT_REFRESH()
				ENDIF
				//Set that we've checked that we don't need to refresh
				SET_BIT(serverVars.iCheckPlayersHaveJobsBitSet, ciCHECK_PLAYERS_JOBS_INT)
			//wait till we've finished refreshing
			ELIF NOT SHOULD_RUN_ROCKSTAR_CONTENT_REFRESH()
				//Grab the missions
				IF SERVER_ONLY_GRABBED_RANDOM_JOBS(serverVars, serverVars.sTypeCounts)	
					IF IS_BIT_SET(serverVars.iServerJobBitSet, SERVER_BOOL_EVERYONE_READY)
						serverVars.iNumRefreshed = 0
						serverVars.playerCurrentJobHost = PLAYER_ID()
						
						PRINTLN("[CS_JOB] [HOST] SERVER_JOB_VOTE_PROCESSING, VOTE_SERVER_INIT, VOTE_SERVER_PROCESS, playerCurrentJobHost = ", NATIVE_TO_INT(serverVars.playerCurrentJobHost))
						
						GOTO_JOB_SERVER_STAGE(serverVars, VOTE_SERVER_PROCESS)
					ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[CS_JOB] [HOST] SERVER_JOB_VOTE_PROCESSING, VOTE_SERVER_INIT, Witing for SHOULD_RUN_ROCKSTAR_CONTENT_REFRESH to be false! ")				
				#ENDIF	
			ENDIF
		BREAK
		
		CASE VOTE_SERVER_PROCESS
			// Reset
			IF HAVE_PLAYERS_VOTED_REFRESH(serverVars)
				SET_PAGE_BITS(serverVars)
				serverVars.iNumRefreshed++
				RESET_SAFE_REFRESH_TIMER(serverVars)
				START_SAFE_REFRESH_TIMER(serverVars)
				
				PRINTLN("[CS_JOB] [HOST] SERVER_JOB_VOTE_PROCESSING, VOTE_SERVER_PROCESS, VOTE_SERVER_RESET ", serverVars.iNumRefreshed)
				
				GOTO_JOB_SERVER_STAGE(serverVars, VOTE_SERVER_RESET)
			ELSE
				// Listen for votes
				SERVER_SETS_JOB_WINNER(serverVars)
			ENDIF
		BREAK
		
		CASE VOTE_SERVER_RESET
			IF HAS_SERVER_CHECKED_CLIENTS_READY_FOR_REFRESH(serverVars)
				IF HAS_SERVER_RESET_VOTING(serverVars)

					SERVER_STARTS_VOTE_TIMER(serverVars, TRUE)
					PRINTLN("[CS_JOB] [HOST]  SERVER_JOB_VOTE_PROCESSING, VOTE_SERVER_RESET, VOTE_SERVER_PROCESS ")

					GOTO_JOB_SERVER_STAGE(serverVars, VOTE_SERVER_PROCESS)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

