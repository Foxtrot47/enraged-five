USING "rage_builtins.sch"
USING "globals.sch"


CONST_INT MAX_ATTACKER_GROUND_VEHS			4
CONST_INT MAX_ATTACKER_SEA_VEHS				4
CONST_INT MAX_ATTACKER_AIR_VEHS				3
CONST_INT MAX_ATTACKER_VEHS					5
CONST_INT ATTACKER_VEH_RESPAWN_TIME			20000
CONST_INT NUM_DEFENDER_SPAWN_OFFSETS		10

STRUCT YACHT_ATTACK_ATTACKER_VEHICLE_STRUCT

	MODEL_NAMES mAttackerBoatModel = SEASHARK
	MODEL_NAMES	mAttackerHeliModel = MAVERICK

	INT iNumAttackerBoats		
	INT iNumAttackerHelis
	
	VECTOR vAttackerBoatSpawnLoc[MAX_ATTACKER_SEA_VEHS]
	FLOAT fAttackerBoatSpawnHeading
	
	VECTOR vAttackerHeliSpawnLoc[MAX_ATTACKER_AIR_VEHS]
	FLOAT fAttackerHeliSpawnHeading[MAX_ATTACKER_AIR_VEHS]
ENDSTRUCT

// Tunable accessors

FUNC INT YA_TUNABLE_EVENT_DURATION()
	RETURN g_sMPTunables.igb_yachtattack_time_limit
ENDFUNC

FUNC INT YA_TUNABLE_CAPTURE_DURATION()
	RETURN g_sMPTunables.igb_yachtattack_capture_time
ENDFUNC

FUNC INT YA_TUNABLE_NUM_BOATS()
	RETURN g_sMPTunables.igb_yachtattack_number_of_boats_spawned
ENDFUNC

FUNC INT YA_TUNABLE_NUM_HELIS()
	RETURN g_sMPTunables.igb_yachtattack_number_of_helis_spawned
ENDFUNC


FUNC INT GET_GROUP_FROM_YACHT(INT iYacht)
	SWITCH iYacht
		CASE 0
		CASE 1
		CASE 2
			RETURN 1
			
		CASE 3
		CASE 4
		CASE 5
			RETURN 2
			
		CASE 6
		CASE 7
		CASE 8
			RETURN 3
			
		CASE 9
		CASE 10
		CASE 11
			RETURN 4
			
		CASE 12
		CASE 13
		CASE 14
			RETURN 5
			
		CASE 15
		CASE 16
		CASE 17
			RETURN 6
			
		CASE 18
		CASE 19
		CASE 20
			RETURN 7
			
		CASE 21
		CASE 22
		CASE 23
			RETURN 8
			
		CASE 24
		CASE 25
		CASE 26
			RETURN 9
			
		CASE 27
		CASE 28
		CASE 29
			RETURN 10
			
		CASE 30
		CASE 31
		CASE 32
			RETURN 11
			
		CASE 33
		CASE 34
		CASE 35
			RETURN 12
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC YACHT_ATTACK_ATTACKER_VEHICLE_STRUCT SETUP_ATTACKER_VEHICLE_DATA(INT iYacht)
	YACHT_ATTACK_ATTACKER_VEHICLE_STRUCT sVehStruct
		
	SWITCH GET_GROUP_FROM_YACHT(iYacht)
		CASE 0
			SCRIPT_ASSERT("[MAGNATE] [GB YACHT ATTACK] - SETUP_ATTACKER_VEHICLE_DATA - attempting to gather invalid group data for yacht")
		BREAK
		
		CASE 1
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
			
			sVehStruct.fAttackerBoatSpawnHeading	 	= 	66.4028
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<-2823.2329, 2367.7334, -0.8566>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			
			sVehStruct.vAttackerHeliSpawnLoc[0] 		= 	<<-2776.7354, 2348.3191, 1.4100>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<-2728.9441, 2385.2795, 0.0830>>
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<-2769.9937, 2419.9639, 1.6353>>
			sVehStruct.fAttackerHeliSpawnHeading[0]		= 	62.2792
			sVehStruct.fAttackerHeliSpawnHeading[1]		= 	85.5583
			sVehStruct.fAttackerHeliSpawnHeading[2]		= 	70.7174
		BREAK

		CASE 2
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
			
			sVehStruct.fAttackerBoatSpawnHeading 		= 	70.4773
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<-2579.0339, 3901.0852, -0.1937>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
																												
			sVehStruct.vAttackerHeliSpawnLoc[0] 		= 	<<-2522.5249, 3914.6453, 2.9836>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<-2542.5232, 3842.6746, 2.0916>>
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<-2479.2009, 3856.1055, 15.2048>>
			sVehStruct.fAttackerHeliSpawnHeading[0] 	= 	78.8918
			sVehStruct.fAttackerHeliSpawnHeading[1] 	= 	80.8251
			sVehStruct.fAttackerHeliSpawnHeading[2] 	= 	80.8251
		BREAK

		CASE 3
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
									
			sVehStruct.fAttackerBoatSpawnHeading		= 	169.7533
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<-2987.9526, -36.4359, -1.2517>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
																												
			sVehStruct.vAttackerHeliSpawnLoc[0]			= 	<<-2962.5095, 2.8172, 5.6602>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<-2998.3093, 20.0011, 6.1754>>
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<-3044.6421, 32.2043, 9.1183>>
			sVehStruct.fAttackerHeliSpawnHeading[0]		=  	156.7838
			sVehStruct.fAttackerHeliSpawnHeading[1]		=  	152.8213
			sVehStruct.fAttackerHeliSpawnHeading[2]		=  	189.9627
		BREAK
		
		CASE 4
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
			
			sVehStruct.fAttackerBoatSpawnHeading		= 	115.6337
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<-1443.3983, -1566.7523, -0.9671>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
																												
			sVehStruct.vAttackerHeliSpawnLoc[0]			= 	<<-1415.8198, -1542.5511, 1.1638>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<-1380.2035, -1497.4137, 3.2869>>
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<-1362.9293, -1561.2792, 2.4111>>
			sVehStruct.fAttackerHeliSpawnHeading[0]		= 	112.1372
			sVehStruct.fAttackerHeliSpawnHeading[1]		=  	165.4805
			sVehStruct.fAttackerHeliSpawnHeading[2]		=  	140.3241
		BREAK

		CASE 5
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
			
			sVehStruct.fAttackerBoatSpawnHeading		= 	175.8105
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<-1693.0682, -3369.1116, -0.8255>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
																												
			sVehStruct.vAttackerHeliSpawnLoc[0]			= 	<<-1684.5569, -3318.9250, 4.1928>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<-1629.5065, -3340.7009, 4.5049>>
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<-1672.9641, -3249.6152, 10.4633>>
			sVehStruct.fAttackerHeliSpawnHeading[0]		= 	169.2553
			sVehStruct.fAttackerHeliSpawnHeading[1]		=  	144.2308
			sVehStruct.fAttackerHeliSpawnHeading[2]		=  	148.2526
		BREAK

		CASE 6
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
			
			sVehStruct.fAttackerBoatSpawnHeading		= 	183.6682
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<1524.8176, -2766.2876, -1.0223>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
																												
			sVehStruct.vAttackerHeliSpawnLoc[0]			= 	<<1512.3849, -2743.4126, 1.0624>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<1589.0612, -2738.7793, 1.0615>>
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<1464.3584, -2756.5942, 1.0462>>
			sVehStruct.fAttackerHeliSpawnHeading[0]		= 	206.8662
			sVehStruct.fAttackerHeliSpawnHeading[1]		=  	180.3761
			sVehStruct.fAttackerHeliSpawnHeading[2]		=  	204.6316
		BREAK

		CASE 7
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
			
			sVehStruct.fAttackerBoatSpawnHeading		= 	223.3824
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<2577.0669, -1229.0287, -0.7027>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
																												
			sVehStruct.vAttackerHeliSpawnLoc[0]			= 	<<2528.7466, -1218.5795, 1.4213>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<2508.3557, -1224.9917, 1.9813>>
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<2570.9126, -1208.7970, 1.4082>>
			sVehStruct.fAttackerHeliSpawnHeading[0]		= 	207.3117
			sVehStruct.fAttackerHeliSpawnHeading[1]		=  	246.0081
			sVehStruct.fAttackerHeliSpawnHeading[2]		=  	212.1435
		BREAK

		CASE 8
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
			
			sVehStruct.fAttackerBoatSpawnHeading		= 	238.7823										//<<3037.9387, 2078.0676, -2.5238>> //<<>>, 
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<3049.0796, 1909.5797, -1.9514>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<7.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<7.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<7.0, 0, 0>>)
																												
			sVehStruct.vAttackerHeliSpawnLoc[0]			= 	<<3013.1101, 1846.4299, 1.9894>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<3014.8135, 1916.1144, 4.7614>>
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<2995.5486, 1902.8778, 14.2636>>
			sVehStruct.fAttackerHeliSpawnHeading[0]		= 	271.0676
			sVehStruct.fAttackerHeliSpawnHeading[1]		=  	235.8285
			sVehStruct.fAttackerHeliSpawnHeading[2]		=  	273.6174
		BREAK
		
		CASE 9
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
			
			sVehStruct.fAttackerBoatSpawnHeading		= 	265.8553
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<3766.2996, 3822.0327, -0.3582>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
																												
			sVehStruct.vAttackerHeliSpawnLoc[0]			= 	<<3728.3945, 3815.4104, 3.9261>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<3721.1855, 3771.1953, 9.6335>>
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<3690.1218, 3802.0120, 13.2367>>
			sVehStruct.fAttackerHeliSpawnHeading[0]		= 	285.2891
			sVehStruct.fAttackerHeliSpawnHeading[1]		=  	310.7798
			sVehStruct.fAttackerHeliSpawnHeading[2]		=  	295.2160
		BREAK

		CASE 10
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
			
			sVehStruct.fAttackerBoatSpawnHeading		= 	248.9274
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<3233.3259, 5330.2632, -0.2857>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
																												
			sVehStruct.vAttackerHeliSpawnLoc[0]			= 	<<3202.5095, 5348.1328, 7.4442>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<3213.1858, 5379.1738, 20.8310>>
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<3145.6694, 5357.9805, 26.4973>>
			sVehStruct.fAttackerHeliSpawnHeading[0]		= 	232.1860
			sVehStruct.fAttackerHeliSpawnHeading[1]		=  	219.7652
			sVehStruct.fAttackerHeliSpawnHeading[2]		=  	250.2235
		BREAK

		CASE 11
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
			
			sVehStruct.fAttackerBoatSpawnHeading		= 	30.6432
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<1245.1693, 6624.7998, -1.5617>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
																												
			sVehStruct.vAttackerHeliSpawnLoc[0]			= 	<<1273.1908, 6600.5337, 1.2142>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<1220.0166, 6572.2637, 1.6511>> 
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<1300.1992, 6578.1694, 1.3607>>
			sVehStruct.fAttackerHeliSpawnHeading[0]		= 	39.4737
			sVehStruct.fAttackerHeliSpawnHeading[1]		=  	358.8115
			sVehStruct.fAttackerHeliSpawnHeading[2]		=  	0.4214
		BREAK

		CASE 12
			sVehStruct.iNumAttackerBoats 				= 	YA_TUNABLE_NUM_BOATS()
			sVehStruct.iNumAttackerHelis 				= 	YA_TUNABLE_NUM_HELIS()
			
			sVehStruct.fAttackerBoatSpawnHeading		= 	71.0029
			sVehStruct.vAttackerBoatSpawnLoc[0] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	<<-662.8556, 6332.6714, -2.0879>>, 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<0.0, 8.0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[1] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[0], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[2] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[1], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
			sVehStruct.vAttackerBoatSpawnLoc[3] 		= 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(	sVehStruct.vAttackerBoatSpawnLoc[2], 
																												sVehStruct.fAttackerBoatSpawnHeading, 
																												<<8.0, 0, 0>>)
																												
			sVehStruct.vAttackerHeliSpawnLoc[0]			= 	<<-631.3538, 6334.6455, 2.3758>>
			sVehStruct.vAttackerHeliSpawnLoc[1] 		= 	<<-635.9840, 6278.1045, 1.8032>>
			sVehStruct.vAttackerHeliSpawnLoc[2] 		= 	<<-588.2267, 6346.3594, 2.3655>>
			sVehStruct.fAttackerHeliSpawnHeading[0]		= 	65.2852
			sVehStruct.fAttackerHeliSpawnHeading[1]		=  	57.9091
			sVehStruct.fAttackerHeliSpawnHeading[2]		=  	58.3541
		BREAK

	ENDSWITCH
		
	RETURN 	sVehStruct	
ENDFUNC

PROC GET_ATTACKER_SPAWN_AREA(INT iYacht, VECTOR& Loc, FLOAT& Heading, FLOAT& Radius)
	Radius = 45.0
	
	SWITCH GET_GROUP_FROM_YACHT(iYacht)
		CASE 0
			SCRIPT_ASSERT("[MAGNATE] [GB YACHT ATTACK] - GET_ATTACKER_SPAWN_AREA - attempting to gather invalid group data for yacht")
		BREAK
	
		CASE 1
			Loc = <<-2781.3303, 2377.8203, 1.7155>>
			Heading = 60.9918
		BREAK
		
		CASE 2
			Loc = <<-2531.3840, 3889.5442, 4.1205>>
			Heading = 71.1209
		BREAK
		
		CASE 3
			Loc = <<-2986.8552, -22.9918, 0.6682>>
			Heading = 158.4044
		BREAK
		
		CASE 4
			Loc = <<-1409.9052, -1556.7712, 1.1444>>
			Heading = 118.3419
		BREAK
		
		CASE 5
			Loc = <<-1669.8047, -3320.3430, 4.3639>>
			Heading = 153.2052
		BREAK
		
		CASE 6
			Loc = <<1523.2567, -2750.3235, 0.8483>>
			Heading = 176.5467
		BREAK
				
		CASE 7
			Loc = <<2547.5566, -1234.7336, -0.0110>>
			Heading = 217.2500
		BREAK
		
		CASE 8
			Loc = <<3027.5957, 1920.2650, 2.2126>>
			Heading = 274.8940
		BREAK
		
		CASE 9
			Loc = <<3758.9639, 3807.2461, -0.4568>>
			Heading = 265.0912 
		BREAK
		
		CASE 10
			Loc = <<3186.0354, 5346.3271, 13.2991>>
			Heading = 226.9029
		BREAK
		
		CASE 11
			Loc = <<1256.5809, 6597.1782, 0.5421>>
			Heading = 21.8257
		BREAK
		
		CASE 12
			Loc = <<-633.9815, 6322.4863, 2.4190>>
			Heading = 54.1462
		BREAK
	
	ENDSWITCH
ENDPROC


