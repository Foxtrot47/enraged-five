
USING "FMMC_Menus_Helpers.sch"

PROC SET_UP_KOTH_HILLS_BASE_MENU(structFMMC_MENU_ITEMS &sFMMCMenu)

	INIT_CREATOR_MENU(sFMMCMenu)
	SET_MENU_TITLE("FMMC_AB_KHA")
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	
	IF sFMMCMenu.iEntityCreation != CREATION_TYPE_HILL
	AND IS_HILL_TYPE_A_SHAPE(sFMMCMenu.sTempHillStruct.iHillType)
		SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_HILL)
		sFMMCMenu.fKOTH_SelectionCooldown = 0.15
	ENDIF
	
	BOOL bInvalid = NOT CAN_EDIT_KOTH_HILL(sFMMCMenu)
	sFMMCMenu.iCurrentMenuLength = KOTH_HILL__MAX
	
	IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RADIUS AND sFMMCMenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__CIRCLE AND NOT bInvalid)
	OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__CYCLE AND g_FMMC_STRUCT.sKOTHData.iNumberOfHills > 0)
	OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__TYPE AND sFMMCmenu.iSelectedEntity = -1) //AND is this allowed in current Creator variation
	OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__CAPTURE_TIME AND NOT bInvalid)
	OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__POINTS_PER_SECOND AND NOT bInvalid)
	OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__MINIMUM_PLAYERS AND NOT bInvalid)
	OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_WIDTH AND sFMMCMenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__RECT AND NOT bInvalid)
	OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_HEIGHT AND sFMMCMenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__RECT AND NOT bInvalid)
	OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_LENGTH AND sFMMCMenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__RECT AND NOT bInvalid)
		SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
	ELSE
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
	ENDIF
		
	ADD_MENU_ITEM_TEXT(KOTH_HILL__TYPE, "KOTH_AT_TYPE", DEFAULT)
	STRING sTypeName = ""
	SWITCH sFMMCMenu.sTempHillStruct.iHillType
		CASE ciKOTH_HILL_TYPE__CIRCLE
			sTypeName = "KOTH_AT_CIR"
		BREAK
		CASE ciKOTH_HILL_TYPE__RECT
			sTypeName = "KOTH_AT_REC"
		BREAK
		CASE ciKOTH_HILL_TYPE__VEHICLE
			sTypeName = "KOTH_AT_VEH"
		BREAK
		CASE ciKOTH_HILL_TYPE__OBJECT
			sTypeName = "KOTH_AT_OBJ"
		BREAK
	ENDSWITCH

	ADD_MENU_ITEM_TEXT(KOTH_HILL__TYPE, sTypeName, DEFAULT, sFMMCmenu.iSelectedEntity = -1)
	
	IF sFMMCMenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__CIRCLE
	AND NOT DOING_HILL_VEHICLE_CREATION(sFMMCmenu)
	AND NOT DOING_HILL_OBJECT_CREATION(sFMMCmenu)
		ADD_MENU_ITEM_TEXT(KOTH_HILL__RADIUS, "FMMCCMENU_GTR", DEFAULT, !bInvalid)
		ADD_MENU_ITEM_TEXT(KOTH_HILL__RADIUS, "NUMBER", 1, !bInvalid)
		ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT(sFMMCMenu.sTempHillStruct.fHill_Radius)
		
	ELIF sFMMCMenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__RECT
	AND NOT DOING_HILL_VEHICLE_CREATION(sFMMCmenu)
	AND NOT DOING_HILL_OBJECT_CREATION(sFMMCmenu)
		ADD_MENU_ITEM_TEXT(KOTH_HILL__RECT_WIDTH, "KOTH_AT_RECWD", DEFAULT, !bInvalid)
		ADD_MENU_ITEM_TEXT(KOTH_HILL__RECT_WIDTH, "NUMBER", 1, !bInvalid)
		ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT(sFMMCMenu.sTempHillStruct.fHill_Width)

		ADD_MENU_ITEM_TEXT(KOTH_HILL__RECT_LENGTH, "KOTH_AT_RECLN", DEFAULT, !bInvalid)
		ADD_MENU_ITEM_TEXT(KOTH_HILL__RECT_LENGTH, "NUMBER", 1, !bInvalid)
		ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT(sFMMCMenu.sTempHillStruct.fHill_Length)
		
		ADD_MENU_ITEM_TEXT(KOTH_HILL__RECT_HEIGHT, "KOTH_AT_RECHE", DEFAULT, !bInvalid)
		ADD_MENU_ITEM_TEXT(KOTH_HILL__RECT_HEIGHT, "NUMBER", 1, !bInvalid)
		ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT(sFMMCMenu.sTempHillStruct.fHill_Height)
	ELSE
		IF DOING_HILL_VEHICLE_CREATION(sFMMCmenu)
			ADD_MENU_ITEM_TEXT(KOTH_HILL__ENTITY_OPTIONS, "KOTH_AT_VEHST", DEFAULT, !bInvalid)
			
			IF !bInvalid
				sFMMCMenu.sMenuGoto[KOTH_HILL__ENTITY_OPTIONS] = eFmmc_VEHICLES_BASE
			ENDIF
		ELIF DOING_HILL_OBJECT_CREATION(sFMMCmenu)
			ADD_MENU_ITEM_TEXT(KOTH_HILL__ENTITY_OPTIONS, "KOTH_AT_OBJST", DEFAULT, !bInvalid)
			IF !bInvalid
				sFMMCMenu.sMenuGoto[KOTH_HILL__ENTITY_OPTIONS] = eFmmc_CAPTURE_OBJECT_BASE
			ENDIF
		ENDIF
	ENDIF
	
	ADD_MENU_ITEM_TEXT(KOTH_HILL__CAPTURE_TIME, "KOTH_AT_CPTM", DEFAULT, !bInvalid)
	
	IF sFMMCMenu.sTempHillStruct.fHill_CaptureTime > cfKOTH_HILL_MIN_CAPTIME
		ADD_MENU_ITEM_TEXT(KOTH_HILL__CAPTURE_TIME, "FMMC_SEL_SEC", 1, !bInvalid)
		ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT(sFMMCMenu.sTempHillStruct.fHill_CaptureTime, 2)
	ELSE
		ADD_MENU_ITEM_TEXT(KOTH_HILL__CAPTURE_TIME, "KOTH_AT_CPTM_I", DEFAULT, !bInvalid)
	ENDIF
	
	ADD_MENU_ITEM_TEXT(KOTH_HILL__POINTS_PER_SECOND, "KOTH_AT_PPT", DEFAULT, !bInvalid)
	ADD_MENU_ITEM_TEXT(KOTH_HILL__POINTS_PER_SECOND, "NUMBER", 1, !bInvalid)
	ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT(sFMMCMenu.sTempHillStruct.fHill_PointsPerTick)
	
	IF FMMC_IS_ROCKSTAR_DEV()
		ADD_MENU_ITEM_TEXT(KOTH_HILL__MINIMUM_PLAYERS, "KOTH_AT_MNP", DEFAULT, !bInvalid)
		IF sFMMCMenu.sTempHillStruct.iHill_MinimumPlayers > 2
			ADD_MENU_ITEM_TEXT(KOTH_HILL__MINIMUM_PLAYERS, "NUMBER", 1, !bInvalid)
			ADD_MENU_ITEM_TEXT_COMPONENT_INT(sFMMCMenu.sTempHillStruct.iHill_MinimumPlayers)
		ELSE
			ADD_MENU_ITEM_TEXT(KOTH_HILL__MINIMUM_PLAYERS, "KOTH_H_AT_MNP", 0, !bInvalid)
		ENDIF
	ENDIF
	
	ADD_CREATOR_CYCLE_MENU_ITEM(sFMMCmenu, sFMMCMenu.iSwitchCam, g_FMMC_STRUCT.sKOTHData.iNumberOfHills, KOTH_HILL__CYCLE, g_FMMC_STRUCT.sKOTHData.iNumberOfHills > 0)
	
	sFMMCMenu.sMenuBack = eFmmc_MAIN_MENU_BASE
	
	IF IS_HILL_TYPE_A_SHAPE(sFMMCMenu.sTempHillStruct.iHillType)
		sFMMCMenu.sMenuAction[KOTH_HILL__TYPE] = eFmmc_Action_KotH_PLACE_HILL
		sFMMCMenu.sMenuAction[KOTH_HILL__RADIUS] = eFmmc_Action_KotH_PLACE_HILL
		sFMMCMenu.sMenuAction[KOTH_HILL__RECT_WIDTH] = eFmmc_Action_KotH_PLACE_HILL
		sFMMCMenu.sMenuAction[KOTH_HILL__RECT_HEIGHT] = eFmmc_Action_KotH_PLACE_HILL
		sFMMCMenu.sMenuAction[KOTH_HILL__RECT_LENGTH] = eFmmc_Action_KotH_PLACE_HILL
		sFMMCMenu.sMenuAction[KOTH_HILL__CAPTURE_TIME] = eFmmc_Action_KotH_PLACE_HILL
		sFMMCMenu.sMenuAction[KOTH_HILL__POINTS_PER_SECOND] = eFmmc_Action_KotH_PLACE_HILL
		sFMMCMenu.sMenuAction[KOTH_HILL__MINIMUM_PLAYERS] = eFmmc_Action_KotH_PLACE_HILL
		sFMMCMenu.sMenuAction[KOTH_HILL__CYCLE] = eFmmc_Action_KotH_PLACE_HILL
	ENDIF
ENDPROC

PROC SET_UP_DM_SHRINKING_BOUNDS_MENU(structFMMC_MENU_ITEMS &sFMMCMenu)

	INIT_CREATOR_MENU(sFMMCMenu)
	SET_MENU_TITLE("DM_BNDS_SHBO_T")
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT, MENU_ITEM_ICON)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT, FONT_RIGHT)
	sFMMCMenu.sMenuBack = eFmmc_KotH_BOUNDS
	sFMMCMenu.iCurrentMenuLength = DM_SHRINKING_BOUNDS_MAX
	
	INT iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
	
	ADD_CREATOR_INT_MENU_ITEM_WITH_OFF_VALUE(DM_SHRINKING_BOUNDS_SHRINK_TO, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iShrinkToPercent, "DM_BNDS_SHB0", "", "FMMC_PERCENT", 100)
	ADD_DESCRIPTION_FOR_MENU_ITEM(sFMMCMenu, DM_SHRINKING_BOUNDS_SHRINK_TO, "DM_BNDS_SHB0_H")
	
	ADD_CREATOR_TIME_MENU_ITEM(DM_SHRINKING_BOUNDS_SHRINK_TIME, "DM_BNDS_SHB1", g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iShrinkTime * 1000, "DM_BNDS_SHB1_0")
	ADD_DESCRIPTION_FOR_MENU_ITEM(sFMMCMenu, DM_SHRINKING_BOUNDS_SHRINK_TIME, "DM_BNDS_SHB1_H")
	
	ADD_CREATOR_INT_MENU_ITEM_WITH_VALUE_AS_TEXT_LABEL(DM_SHRINKING_BOUNDS_SHRINK_DELAY_CONDITION, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iShrinkDelayCondition, "DM_BNDS_SHB3", "DM_BNDS_SHB3_")
	ADD_DESCRIPTION_FOR_MENU_ITEM(sFMMCMenu, DM_SHRINKING_BOUNDS_SHRINK_DELAY_CONDITION, "DM_BNDS_SHB3_H")
	
	ADD_CREATOR_TIME_MENU_ITEM(DM_SHRINKING_BOUNDS_TIME_SHRINK_DELAY, "DM_BNDS_SHB2", g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iShrinkDelay * 1000, "FMMC_SEL_OFF")
	ADD_DESCRIPTION_FOR_MENU_ITEM(sFMMCMenu, DM_SHRINKING_BOUNDS_TIME_SHRINK_DELAY, "DM_BNDS_SHB2_H")

ENDPROC

PROC ADD_MENU_ITEMS_FOR_KotH_SPHERE_BOUNDS(structFMMC_MENU_ITEMS &sFMMCMenu, BOOL bCustomBounds)
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_TYPE
	OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_RADIUS
	OR bCustomBounds
		SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
	ENDIF
	
	INT iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
	
	IF IS_KING_OF_THE_HILL_CREATOR()
		ADD_MENU_ITEM_TEXT(KOTH_BOUNDS_TYPE, "KTH_B_TY")
		TEXT_LABEL_15 tl15 = "KTH_B_TY"
		tl15 += g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsSetting
		ADD_MENU_ITEM_TEXT(KOTH_BOUNDS_TYPE, tl15)
	ENDIF
	
	ADD_MENU_ITEM_TEXT(KOTH_BOUNDS_RELOCATE, "KTH_B_RL", DEFAULT, bCustomBounds)
	IF bCustomBounds
		sFMMCMenu.sMenuAction[KOTH_BOUNDS_RELOCATE] = eFmmc_Action_KotH_PLACE_BOUNDS
	ENDIF
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsCentre)
		ADD_MENU_ITEM_ICON(KOTH_BOUNDS_RELOCATE, MENU_ICON_ALERT)
	ENDIF
	
	ADD_MENU_ITEM_TEXT(KOTH_BOUNDS_RADIUS, "KTH_B_RA", DEFAULT, bCustomBounds)
	ADD_MENU_ITEM_TEXT(KOTH_BOUNDS_RADIUS, "NUMBER", 1, bCustomBounds)
	ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT(g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius)
	
	IF NOT IS_KING_OF_THE_HILL_CREATOR()
		ADD_CREATOR_BITSET_MENU_ITEM(KOTH_BOUNDS_3D_CHECK, g_FMMC_STRUCT.sKotHData.iKotHBS, ciKotHBS_3DBoundsCheck, "KTH_B_3D", "", "")
	ENDIF
	
ENDPROC

PROC ADD_MENU_ITEMS_FOR_KotH_ANGLED_AREA_BOUNDS(structFMMC_MENU_ITEMS &sFMMCMenu)
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_WIDTH
	OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_HEIGHT
		SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
	ENDIF
	
	INT iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
	
	ADD_MENU_ITEM_TEXT(KOTH_BOUNDS_POSITION_ONE, "KTH_B_PS0", DEFAULT)
	sFMMCMenu.sMenuAction[KOTH_BOUNDS_POSITION_ONE] = eFmmc_Action_KotH_PLACE_BOUNDS_POINT_0
	IF IS_VECTOR_ZERO(GET_DEATHMATCH_BOUNDS_POINT(iBoundsIndex, 0, <<0,0,0>>, <<0,0,0>>))
		ADD_MENU_ITEM_ICON(KOTH_BOUNDS_POSITION_ONE, MENU_ICON_ALERT)
	ENDIF
	
	ADD_MENU_ITEM_TEXT(KOTH_BOUNDS_POSITION_TWO, "KTH_B_PS1", DEFAULT)
	sFMMCMenu.sMenuAction[KOTH_BOUNDS_POSITION_TWO] = eFmmc_Action_KotH_PLACE_BOUNDS_POINT_1
	IF IS_VECTOR_ZERO(GET_DEATHMATCH_BOUNDS_POINT(iBoundsIndex, 1, <<0,0,0>>, <<0,0,0>>))
		ADD_MENU_ITEM_ICON(KOTH_BOUNDS_POSITION_TWO, MENU_ICON_ALERT)
	ENDIF
	
	ADD_MENU_ITEM_TEXT(KOTH_BOUNDS_WIDTH, "KTH_B_WDT", DEFAULT)
	ADD_MENU_ITEM_TEXT(KOTH_BOUNDS_WIDTH, "NUMBER", 1)
	ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT(g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius)
	
	ADD_MENU_ITEM_TEXT(KOTH_BOUNDS_HEIGHT, "KTH_B_HET", DEFAULT)
	ADD_MENU_ITEM_TEXT(KOTH_BOUNDS_HEIGHT, "NUMBER", 1)
	ADD_MENU_ITEM_TEXT_COMPONENT_FLOAT(g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsHeight)
	
	ADD_CREATOR_BITSET_MENU_ITEM(KOTH_BOUNDS_3D_CHECK, g_FMMC_STRUCT.sKotHData.iKotHBS, ciKotHBS_3DBoundsCheck, "KTH_B_3D", "", "")
ENDPROC

PROC SET_UP_KOTH_BOUNDS_MENU(structFMMC_MENU_ITEMS &sFMMCMenu)

	INIT_CREATOR_MENU(sFMMCMenu)
	SET_MENU_TITLE("KTH_BNDS_T")
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT, MENU_ITEM_ICON)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT, FONT_RIGHT)
	sFMMCMenu.sMenuBack = eFmmc_MAIN_MENU_BASE
	sFMMCMenu.iCurrentMenuLength = KOTH_BOUNDS_MAX
	
	INT iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
	BOOL bCustomBounds = (g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsSetting = ciDMBOUNDSSETTING_CUSTOM OR NOT IS_KING_OF_THE_HILL_CREATOR())	
	
	IF bCustomBounds
		sFMMCMenu.sBackupKOTHBoundsInfo[iBoundsIndex] = g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex]
	ENDIF
	
	IF NOT IS_KING_OF_THE_HILL_CREATOR()
		ADD_CREATOR_INT_MENU_ITEM(KOTH_BOUNDS_INDEX, g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex+1, "KTH_B_IND")
		ADD_DESCRIPTION_FOR_MENU_ITEM(sFMMCMenu, KOTH_BOUNDS_INDEX, "KTH_H_B_IND")
	ENDIF
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_SHAPE
		SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
	ELSE
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
	ENDIF
	
	ADD_CREATOR_INT_MENU_ITEM_WITH_VALUE_AS_TEXT_LABEL(KOTH_BOUNDS_SHAPE, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape, "KTH_B_SH", "KTH_B_SH")
	
	IF NOT IS_KING_OF_THE_HILL_CREATOR()
		ADD_CREATOR_INT_MENU_ITEM_WITH_OFF_VALUE(KOTH_BOUNDS_OOB_TIME, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iOOBTimer/1000, "DM_BNDS_OOB", "DM_BNDS_INST", "FMMC_SEL_SEC")
		ADD_DESCRIPTION_FOR_MENU_ITEM(sFMMCMenu, KOTH_BOUNDS_OOB_TIME, "DM_H_BNDS_OOB")
	ENDIF
	
	IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape = ciDMBOUNDSSHAPE_SPHERE
		ADD_MENU_ITEMS_FOR_KotH_SPHERE_BOUNDS(sFMMCmenu, bCustomBounds)
		
	ELIF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape = ciDMBOUNDSSHAPE_ANGLED_AREA
		ADD_MENU_ITEMS_FOR_KotH_ANGLED_AREA_BOUNDS(sFMMCmenu)
		
	ENDIF
	
	IF NOT IS_KING_OF_THE_HILL_CREATOR()
		ADD_CREATOR_SUBMENU_MENU_ITEM(sFMMCMenu, KOTH_BOUNDS_SHRINKING_BOUNDS, efmmc_DM_SHRINKING_BOUNDS, "DM_BNDS_SHBO")
		ADD_DESCRIPTION_FOR_MENU_ITEM(sFMMCmenu, KOTH_BOUNDS_SHRINKING_BOUNDS, "DM_H_SHBO")
	ENDIF
	
ENDPROC
