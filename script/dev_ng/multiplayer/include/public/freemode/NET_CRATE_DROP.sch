//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_CRATE_DROP.sch																			//
// Description: Controls the Cop Reaction to a Hold Up. 													//
// Written by:  Ryan Baker																					//
// Date: xx/xx/2011																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "building_control_public.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"

// CnC Headers
USING "net_scoring_common.sch"
USING "net_mission.sch"
USING "shared_hud_displays.sch"
USING "net_freemode_cut.sch"
//USING "net_take_money.sch"
USING "fm_in_corona_header.sch"
USING "net_cloud_reward_system.sch"

//CONST_INT TOTAL_MAX_NUM_PEDS				8
//CONST_INT TOTAL_MAX_NUM_VEHICLES			2

CONST_INT LOCATION_MAX_NUM_PEDS				4
CONST_INT LOCATION_MAX_NUM_VEHICLES			1
CONST_INT MAX_NUM_VEH_SPAWN_POINTS			3

CONST_INT SPECIAL_CRATE_WAIT_1				3
CONST_INT SPECIAL_CRATE_WAIT_2				5
CONST_INT SPECIAL_CRATE_WAIT_3				10

CONST_INT CRATE_STANDARD_CLEANUP_TIME		480000	//(8*60*1000 = 8mins)
CONST_INT CRATE_ADDITIONAL_CLEANUP_TIME		120000	//(2*60*1000 = 2mins)

CONST_INT CLOTHING_ARRAY_SIZE	4

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    CRATE
//Different Contents types for the Crate
ENUM CD_CRATE_CONTENTS_TYPE
	CD_CONTENTS_CASH,
	CD_CONTENTS_WEAPON,
	CD_CONTENTS_RANDOM,
	CD_CONTENTS_XP,
	CD_CONTENTS_DRUGS,
	CD_CONTENTS_AMMO,
	CD_CONTENTS_CLOTHING,
	CD_CONTENTS_UNLOCK,
	CD_CONTENTS_DLC
ENDENUM

//BitSet Data
CONST_INT biCD_Crate_ReadyForDrop				0
CONST_INT biCD_Crate_DropDone					1
CONST_INT biCD_Crate_LandAnimStarted			2
CONST_INT biCD_Crate_LandDone					3
CONST_INT biCD_Crate_PickedUp					4
CONST_INT biCD_Crate_AiSetup					5
CONST_INT biCD_Crate_AllCratesDropped			6
CONST_INT biCD_Crate_AllCratesPickedUp			7
CONST_INT biCD_Crate_NoCratesExist				8
CONST_INT biCD_Crate_AllAiSetup					9
CONST_INT biCD_Crate_NoPlayerAtAnyDropLocation	10
CONST_INT biCD_Crate_LandAnimPlaying			11
CONST_INT biCD_Crate_SlowDownForDrop			12
CONST_INT biCD_Crate_NoPlayerAtDropLocation		13
CONST_INT biCD_Crate_CrateReservationReduced	14
CONST_INT biCD_Crate_DestructibleCrateBroken	15

//Crate Data
STRUCT CD_CRATE_DATA
	INT iIdentifier
	NETWORK_INDEX niCrate
	NETWORK_INDEX niChute
	MODEL_NAMES CrateModel
	MODEL_NAMES ChuteModel
	VECTOR ChuteOffset
	
	TEXT_LABEL_23 AnimDictionary 	//= "P_cargo_chute_S"
	TEXT_LABEL_31 AnimDeploy 		//= "P_cargo_chute_S_deploy"
	TEXT_LABEL_31 AnimCrumple 		//= "P_cargo_chute_S_crumple"
	
	CD_CRATE_CONTENTS_TYPE eContentsType = CD_CONTENTS_CASH
	INT iContentsVariation = 0
	INT iContentsAmount = 1000
	WEAPON_TYPE iContentsWeapon = WEAPONTYPE_ADVANCEDRIFLE
	
	PED_COMP_TYPE_ENUM eContentsMaleClothingType = -1
	PED_COMP_NAME_ENUM eContentsMaleClothingName = -1
	
	PED_COMP_TYPE_ENUM eContentsFemaleClothingType = -1
	PED_COMP_NAME_ENUM eContentsFemaleClothingName = -1
	
	PED_COMP_TYPE_ENUM eContentsMaleClothingTypeArray[CLOTHING_ARRAY_SIZE]
	PED_COMP_NAME_ENUM eContentsMaleClothingNameArray[CLOTHING_ARRAY_SIZE]
	
	PED_COMP_TYPE_ENUM eContentsFemaleClothingTypeArray[CLOTHING_ARRAY_SIZE]
	PED_COMP_NAME_ENUM eContentsFemaleClothingNameArray[CLOTHING_ARRAY_SIZE]
	
	PTFX_ID ptfxFlare
	PTFX_ID ptfxBeacon
	
	INT BleepSoundID = -1
	//INT PickupSoundID = -1
	
	SCRIPT_TIMER iCleanupTimer
	INT iCleanupTimerDelay = CRATE_STANDARD_CLEANUP_TIME
	
	INT iParticpantGotCrate = -1
	INT iBitSet
	
	//TAKE_MONEY_SERVER_STRUCT TakeMoneyServerData
ENDSTRUCT
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
///    PEDS
//Different AI Types for the Location
ENUM CD_AI_SETUP
	//CD_AI_NONE,
	CD_AI_ON_FOOT_BEFORE,
	//CD_AI_ON_FOOT_AFTER,
	//CD_AI_VEHICLE_STAIONARY,
	CD_AI_VEHICLE_ARRIVE,
	//CD_AI_HELICOPTER,
	CD_AI_RANDOM
ENDENUM

//Different combat types for peds
ENUM CD_PED_COMBAT_TYPE_ENUM
	CD_PCT_COWARD,
	CD_PCT_BELOW_AVERAGE,
	CD_PCT_AVERAGE,
	CD_PCT_ABOVE_AVERAGE,
	CD_PCT_PRO,
	CD_PCT_RANDOM
ENDENUM

//Different combat styles for peds
ENUM CD_PED_COMBAT_STYLE_ENUM
	CD_PCS_NORMAL,
	CD_PCS_DEFENSIVE,
	CD_PCS_AGGRESSIVE,
	CD_PCS_DRIVER,
	CD_PCS_PASSENGER,
	CD_PCS_RANDOM
ENDENUM

//Ped Data
STRUCT CD_PED_DATA
	NETWORK_INDEX niPed
	MODEL_NAMES PedModel = G_M_Y_MexGoon_02
	VECTOR vCreationPos
	FLOAT fHeading
	VECTOR vDefensivePos
	FLOAT fDefensiveRadius		// <= 0 MEANS NO DEFENSIVE AREA
	BOOL bPatrolArea = FALSE
	REL_GROUP_HASH RelGroup		// = rgFM_AiHate
	CD_PED_COMBAT_TYPE_ENUM eCombatType = CD_PCT_AVERAGE
	CD_PED_COMBAT_STYLE_ENUM eCombatStyle = CD_PCS_NORMAL
	WEAPON_TYPE Weapon = WEAPONTYPE_PISTOL
	INT iAccuracy = 30
	INT iHealth = 200
	INT iArmour = 100
ENDSTRUCT
////////////////////////////////////////////////////////////////////////////////////////////////////////////// 


////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
///    VEHICLES
//Vehicle Data
STRUCT CD_VEH_DATA
	NETWORK_INDEX niVeh
	MODEL_NAMES VehModel = CAVALCADE
	VECTOR vCreationPos[MAX_NUM_VEH_SPAWN_POINTS]
	FLOAT fHeading[MAX_NUM_VEH_SPAWN_POINTS]
	INT iHealth = 1000
	//Colour
	//Mods
	//Toughness
	//BOOL bBurstProofTyres
ENDSTRUCT
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
///    GENERAL
//Different model sets for peds and vehicles
ENUM CD_MODEL_SET_ENUM
	CD_PMS_VAGOS,
	CD_PMS_LOST,
	CD_PMS_FAMILY,
	CD_PMS_PROFESSIONAL,
	CD_PMS_TRIAD,
	CD_PMS_REDNECK,
	CD_PMS_RANDOM
ENDENUM


//Location Data
STRUCT CD_LOCATION_DATA
	INT iIdentifier
	VECTOR vLocation
	CD_AI_SETUP eAiSetup = CD_AI_ON_FOOT_BEFORE	//CD_AI_NONE
	CD_PED_DATA Peds[LOCATION_MAX_NUM_PEDS]
	CD_VEH_DATA Vehicle[LOCATION_MAX_NUM_VEHICLES]
	INT iNumPeds = 4
	VECTOR vPlaneStart
	FLOAT fPlaneHeading
	VECTOR vSpecificForceToGroundLocation
ENDSTRUCT

//Plane Data
STRUCT CD_PLANE_DATA
	INT iIdentifier
	CD_VEH_DATA Plane
	CD_PED_DATA Pilot
ENDSTRUCT

//All Crate Drop Data
STRUCT CRATE_DROP_DATA
	INT iNumDrops = MAX_NUM_CRATE_DROPS
	//CD_PLANE_DATA PlaneData
	CD_CRATE_DATA CrateData[MAX_NUM_CRATE_DROPS]
	CD_LOCATION_DATA LocationData[MAX_NUM_CRATE_DROPS]
	CD_MODEL_SET_ENUM eModelSet = CD_PMS_RANDOM
ENDSTRUCT
////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

















///////////////////////////////////////////
///    			PROCEDURES    			///
///////////////////////////////////////////
PROC CD_USE_MODEL_SET(CD_LOCATION_DATA &LocationData, CD_MODEL_SET_ENUM &eModelSet)
	
	//Use Random
	IF eModelSet = CD_PMS_RANDOM
		eModelSet = INT_TO_ENUM(CD_MODEL_SET_ENUM , GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CD_PMS_RANDOM)))
		NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CD_USE_MODEL_SET - RANDOMISED") NET_NL()
	ENDIF
	
	INT i
	INT iRand
	
	//Set the Models
	SWITCH eModelSet
		CASE CD_PMS_VAGOS
			REPEAT LOCATION_MAX_NUM_PEDS i
				LocationData.Peds[i].PedModel			=	G_M_Y_MEXGOON_03 
				iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
				IF iRand < 33
					LocationData.Peds[i].Weapon	= WEAPONTYPE_MICROSMG
				ELIF iRand > 66
					LocationData.Peds[i].Weapon	= WEAPONTYPE_ASSAULTRIFLE
				ENDIF
			ENDREPEAT
			i=0
			REPEAT LOCATION_MAX_NUM_VEHICLES i
				LocationData.Vehicle[i].VehModel		=	CAVALCADE	//CAVALCADE2		//BLAZER
			ENDREPEAT
			NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CD_USE_MODEL_SET - CD_PMS_VAGOS") NET_NL()
		BREAK
		
		CASE CD_PMS_LOST
			REPEAT LOCATION_MAX_NUM_PEDS i
				LocationData.Peds[i].PedModel			=	G_M_Y_LOST_03
				iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
				IF iRand < 33
					LocationData.Peds[i].Weapon	= WEAPONTYPE_SAWNOFFSHOTGUN
				ELIF iRand > 66
					LocationData.Peds[i].Weapon	= WEAPONTYPE_ASSAULTRIFLE
				ENDIF
			ENDREPEAT
			i=0
			REPEAT LOCATION_MAX_NUM_VEHICLES i
				LocationData.Vehicle[i].VehModel		=	HEXER		//GBURRITO
			ENDREPEAT
			NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CD_USE_MODEL_SET - CD_PMS_LOST") NET_NL()
		BREAK
		
		CASE CD_PMS_FAMILY
			REPEAT LOCATION_MAX_NUM_PEDS i
				LocationData.Peds[i].PedModel			=	G_M_Y_FAMCA_01
				iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
				IF iRand < 33
					LocationData.Peds[i].Weapon	= WEAPONTYPE_MICROSMG
				ELIF iRand > 66
					LocationData.Peds[i].Weapon	= WEAPONTYPE_PUMPSHOTGUN
				ENDIF
			ENDREPEAT
			i=0
			REPEAT LOCATION_MAX_NUM_VEHICLES i
				LocationData.Vehicle[i].VehModel		=	BALLER	//PATRIOT			//VOODOO2
			ENDREPEAT
			NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CD_USE_MODEL_SET - CD_PMS_FAMILY") NET_NL()
		BREAK
		
		CASE CD_PMS_PROFESSIONAL
			REPEAT LOCATION_MAX_NUM_PEDS i
				LocationData.Peds[i].PedModel			=	MP_G_M_PROS_01
				iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
				IF iRand < 33
					LocationData.Peds[i].Weapon	= WEAPONTYPE_SMG
				ELIF iRand > 66
					LocationData.Peds[i].Weapon	= WEAPONTYPE_COMBATMG
				ENDIF
			ENDREPEAT
			i=0
			REPEAT LOCATION_MAX_NUM_VEHICLES i
				LocationData.Vehicle[i].VehModel		=	GRANGER	//SCHAFTER2		//GRESLEY 	//BUZZARD
			ENDREPEAT
			NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CD_USE_MODEL_SET - CD_PMS_PROFESSIONAL") NET_NL()
		BREAK
		
		CASE CD_PMS_TRIAD
			REPEAT LOCATION_MAX_NUM_PEDS i
				LocationData.Peds[i].PedModel			=	G_M_M_CHIGOON_01
				iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
				IF iRand < 33
					LocationData.Peds[i].Weapon	= WEAPONTYPE_SMG
				ELIF iRand > 66
					LocationData.Peds[i].Weapon	= WEAPONTYPE_CARBINERIFLE
				ENDIF
			ENDREPEAT
			i=0
			REPEAT LOCATION_MAX_NUM_VEHICLES i
				LocationData.Vehicle[i].VehModel		=	JACKAL			//BATI			//FROGGER
			ENDREPEAT
			NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CD_USE_MODEL_SET - CD_PMS_TRIAD") NET_NL()
		BREAK
		
		CASE CD_PMS_REDNECK
			REPEAT LOCATION_MAX_NUM_PEDS i
				LocationData.Peds[i].PedModel			=	A_M_M_HILLBILLY_02
				iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
				IF iRand < 33
					LocationData.Peds[i].Weapon	= WEAPONTYPE_SAWNOFFSHOTGUN
				ELIF iRand > 66
					LocationData.Peds[i].Weapon	= WEAPONTYPE_COMBATMG
				ENDIF
			ENDREPEAT
			i=0
			REPEAT LOCATION_MAX_NUM_VEHICLES i
				LocationData.Vehicle[i].VehModel		=	REBEL			//DUNE
			ENDREPEAT
			NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CD_USE_MODEL_SET - CD_PMS_REDNECK") NET_NL()
		BREAK
	ENDSWITCH
ENDPROC






















///////////////////////////////////////////
///    			CLIENT    				///
///////////////////////////////////////////

//PURPOSE: Launches the Crate Drop script
PROC PROCESS_LAUNCH_CRATE_DROP_CLIENT()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		SWITCH MPGlobalsAmbience.CrateDropLaunchData.iLaunchStageClient
			
			//Launch Crate Drop?
			CASE 0
				IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_CRATE_DROP", -1, TRUE)	//GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_CRATE_DROP")) = 0
					
					#IF IS_DEBUG_BUILD
					IF bDebugCrateDropIgnoreNumPlayers = TRUE
						g_sMptunables.iplayerrestrictioncratedrop = 0
					ENDIF
					#ENDIF
					
					IF NETWORK_IS_SCRIPT_ACTIVE("AM_CRATE_DROP")
					OR (GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage = 1 AND GlobalServerBD_BlockB.CrateDropServerLaunchData.bUseSpecialCrate = FALSE AND NETWORK_GET_NUM_PARTICIPANTS() >= g_sMptunables.iplayerrestrictioncratedrop)
					OR (GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage = 1 AND GlobalServerBD_BlockB.CrateDropServerLaunchData.bUseSpecialCrate = TRUE)
						IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
						AND NOT NETWORK_IS_ACTIVITY_SESSION()
						AND NOT IS_THIS_PLAYER_ACTIVE_IN_CORONA_PAST_TRANSITION_SESSION(PLAYER_ID())
						AND NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
						AND NOT IS_TRANSITION_ACTIVE()
						AND IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CRATE_DROP)
						AND NOT IS_MP_AMBIENT_SCRIPT_BLOCKED(MPAM_TYPE_CRATEDROP)
							IF ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH(5, 2, 3, FALSE, TRUE)
								MPGlobalsAmbience.CrateDropLaunchData.missionData.mdID.idMission = eAM_CRATE_DROP
								MPGlobalsAmbience.CrateDropLaunchData.missionData.iInstanceId = DEFAULT_MISSION_INSTANCE
								BROADCAST_JOIN_THIS_AMBIENT_MP_MISSION_EVENT(MPGlobalsAmbience.CrateDropLaunchData.missionData, SPECIFIC_PLAYER(PLAYER_ID()))
								MPGlobalsAmbience.CrateDropLaunchData.iLaunchStageClient = 1
								NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_CLIENT - LAUNCHED - iLaunchStageClient = 1  <-----     ") NET_NL()
							#IF IS_DEBUG_BUILD
							ELSE
								NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_CLIENT - FAILED - ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH  <-----     ") NET_NL()
							#ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ELSE
					MPGlobalsAmbience.CrateDropLaunchData.iLaunchStageClient = 2
					NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_CLIENT - ALREADY RUNNING - iLaunchStageClient = 2  <-----     ") NET_NL()
				ENDIF
				
				//DEBUG FORCE LAUNCH
				#IF IS_DEBUG_BUILD
				IF GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage = 88
				AND NOT NETWORK_IS_IN_TUTORIAL_SESSION()
				AND NOT NETWORK_IS_ACTIVITY_SESSION()
					IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_CRATE_DROP", -1, TRUE)	//GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_CRATE_DROP")) = 0
						MPGlobalsAmbience.CrateDropLaunchData.missionData.mdID.idMission = eAM_CRATE_DROP
						MPGlobalsAmbience.CrateDropLaunchData.missionData.iInstanceId = DEFAULT_MISSION_INSTANCE
						BROADCAST_JOIN_THIS_AMBIENT_MP_MISSION_EVENT(MPGlobalsAmbience.CrateDropLaunchData.missionData, SPECIFIC_PLAYER(PLAYER_ID()))
						MPGlobalsAmbience.CrateDropLaunchData.iLaunchStageClient = 1
						NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_CLIENT - DEBUG FORCE LAUNCH - iLaunchStageClient = 1  <-----     ") NET_NL()
					ELSE
						MPGlobalsAmbience.CrateDropLaunchData.iLaunchStageClient = 2
						NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_CLIENT - DEBUG FORCE LAUNCH - ALREADY RUNNING - iLaunchStageClient = 2  <-----     ") NET_NL()
					ENDIF
				ENDIF
				#ENDIF
				
			BREAK
			
			//Check if Crate Drop has Started
			CASE 1
				IF NETWORK_IS_SCRIPT_ACTIVE("AM_CRATE_DROP", -1, TRUE)	//GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_CRATE_DROP")) > 0
					MPGlobalsAmbience.CrateDropLaunchData.iLaunchStageClient = 2
					NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_CLIENT - STARTED - iLaunchStageClient = 2  <-----     ") NET_NL()
				ENDIF
			BREAK
			
			//Check for End
			CASE 2
				IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_CRATE_DROP", -1, TRUE)	//GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_CRATE_DROP")) = 0
					MPGlobalsAmbience.CrateDropLaunchData.iLaunchStageClient = 3
					NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_CLIENT - ENDED - iLaunchStageClient = 3  <-----     ") NET_NL()
				ENDIF
			BREAK
			
			//Wait - to make sure everyone else has ended
			CASE 3
				IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_CRATE_DROP", -1, TRUE)	//GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_CRATE_DROP")) = 0
					IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.CrateDropLaunchData.iWaitTime, 30000)
						CLEAR_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker1)
						CLEAR_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker2)
						CLEAR_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker3)
						RESET_NET_TIMER(MPGlobalsAmbience.CrateDropLaunchData.iWaitTime)
						MPGlobalsAmbience.CrateDropLaunchData.iLaunchStageClient = 0
						NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_CLIENT - WAIT OVER - iLaunchStageClient = 0  <-----     ") NET_NL()
					ENDIF
				ELSE
					RESET_NET_TIMER(MPGlobalsAmbience.CrateDropLaunchData.iWaitTime)
					MPGlobalsAmbience.CrateDropLaunchData.iLaunchStageClient = 2
					NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_CLIENT - ALREADY RUNNING 2 - iLaunchStageClient = 2  <-----     ") NET_NL()
				ENDIF
			BREAK
		ENDSWITCH
				
	ENDIF
ENDPROC

//PURPOSE: Displays lead up messages for a SPecial Crate Drop
PROC PROCESS_SPEICAL_CRATE_DROP_MESSAGES_CLIENT()
	IF GlobalServerBD_BlockB.CrateDropServerLaunchData.bUseSpecialCrate = TRUE
		IF NOT IS_BIT_SET(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker1)
			INT iDifSec, iDifMin, iDifHour, iDifDay, iDifMon, iDifYear
			GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime, iDifSec, iDifMin, iDifHour, iDifDay, iDifMon, iDifYear)
			IF iDifYear = 0
			AND iDifMon = 0
			AND iDifDay = 0
			AND iDifHour < SPECIAL_CRATE_WAIT_3
				//3 hours OR less
				IF iDifHour < SPECIAL_CRATE_WAIT_1
					IF NOT IS_BIT_SET(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker1)
						IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CRATE_DROP)
						AND NOT IS_TRANSITION_ACTIVE()
							IF g_sMPTunables.iCrateDropSpecialFrequency < SPECIAL_CRATE_WAIT_1
								PRINT_TICKER_WITH_INT("ACD_TSCH", g_sMPTunables.iCrateDropSpecialFrequency)
								NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_SPEICAL_CRATE_DROP_MESSAGES_CLIENT - g_sMPTunables.iCrateDropSpecialFrequency = ") NET_PRINT_INT(g_sMPTunables.iCrateDropSpecialFrequency) NET_NL()
							ELSE
								PRINT_TICKER_WITH_INT("ACD_TSCH", SPECIAL_CRATE_WAIT_1)
								NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_SPEICAL_CRATE_DROP_MESSAGES_CLIENT - 3 hours   <-----     ") NET_NL()
							ENDIF
						ENDIF
						SET_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker1)
					ENDIF
				//5 hours
				ELIF iDifHour  < SPECIAL_CRATE_WAIT_2
					IF NOT IS_BIT_SET(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker2)
						IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CRATE_DROP)
						AND NOT IS_TRANSITION_ACTIVE()
							PRINT_TICKER_WITH_INT("ACD_TSCH", SPECIAL_CRATE_WAIT_2)
							NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_SPEICAL_CRATE_DROP_MESSAGES_CLIENT - 5 hours  <-----     ") NET_NL()
						ENDIF
						SET_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker2)
					ENDIF
				//10 hours
				ELIF iDifHour <= SPECIAL_CRATE_WAIT_3
				AND iDifHour >= (SPECIAL_CRATE_WAIT_3-1)
					IF NOT IS_BIT_SET(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker3)
						IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CRATE_DROP)
						AND NOT IS_TRANSITION_ACTIVE()
							PRINT_TICKER_WITH_INT("ACD_TSCH", SPECIAL_CRATE_WAIT_3)
							NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_SPEICAL_CRATE_DROP_MESSAGES_CLIENT - 10 hours  <-----     ") NET_NL()
						ENDIF
						SET_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker3)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls the launching of a Crate Drop 
PROC MAINTAIN_CRATE_DROP_CLIENT()
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		PROCESS_LAUNCH_CRATE_DROP_CLIENT()
		PROCESS_SPEICAL_CRATE_DROP_MESSAGES_CLIENT()
	ENDIF
ENDPROC










///////////////////////////////////////////
///    			SERVER    				///
///////////////////////////////////////////

//PURPOSE: Controls whether or not we should start to launch a Crate Drop
PROC PROCESS_START_CRATE_DROP_SERVER()
	IF GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage = -1
	AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_CRATE_DROP", -1, TRUE)			//GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_CRATE_DROP")) = 0
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
	
		#IF IS_DEBUG_BUILD
		//Debug Launch
		IF bDebugLaunchCrateDrop
			bDebugLaunchCrateDrop = FALSE
			GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage = 88	//0
			GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime = INVALID_TIMEOFDAY
			bDebugCrateDropIgnoreNumPlayers = TRUE
			g_sMptunables.iplayerrestrictioncratedrop = 1
			NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_START_CRATE_DROP_SERVER - DEBUG FORCE LAUNCH - START LAUNCH - iLaunchStage = 88  <-----     ") NET_NL()
		ENDIF
		//Debug set Special Crate Drop
		IF bDebugSetCrateDropSpecial
			bDebugSetCrateDropSpecial = FALSE
			GlobalServerBD_BlockB.CrateDropServerLaunchData.bUseSpecialCrate = TRUE
			IF g_sMPTunables.bturnoffspecialcrates = FALSE //Tunable disables the Special Crate Drop per session limit
				GlobalServerBD_BlockB.CrateDropServerLaunchData.bSpecialCrateDone = TRUE
			ENDIF
			NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_START_CRATE_DROP_SERVER - DEBUG SPECIAL CRATE SET BY DEBUG  <-----     ") NET_NL()
			bDebugCrateDropIgnoreNumPlayers = TRUE
			g_sMPTunables.iCrateDropSpecial_min_players = 1
			
			TIMEOFDAY ThisTimeOfDay = GET_CURRENT_TIMEOFDAY()
			SET_TIMEOFDAY(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime, GET_TIMEOFDAY_SECOND(ThisTimeOfDay), GET_TIMEOFDAY_MINUTE(ThisTimeOfDay), GET_TIMEOFDAY_HOUR(ThisTimeOfDay), GET_TIMEOFDAY_DAY(ThisTimeOfDay), GET_TIMEOFDAY_MONTH(ThisTimeOfDay), GET_TIMEOFDAY_YEAR(ThisTimeOfDay))
			ADD_TIME_TO_TIMEOFDAY(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime, 0, GET_RANDOM_INT_IN_RANGE(0, 10), g_sMPTunables.iCrateDropSpecialFrequency, 0, 0, 0)
			NET_PRINT("   >>>   CRATE DROP - SPEICAL CRATE DEBUG - CURRENT TIME = ") 		NET_PRINT_INT(GET_TIMEOFDAY_HOUR(ThisTimeOfDay))NET_PRINT(":") NET_PRINT_INT(GET_TIMEOFDAY_MINUTE(ThisTimeOfDay)) NET_NL()
			NET_PRINT("   >>>   CRATE DROP - SPEICAL CRATE DEBUG - NEXT TRIGGER TIME = ")	NET_PRINT_INT(GET_TIMEOFDAY_HOUR(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime))NET_PRINT(":") NET_PRINT_INT(GET_TIMEOFDAY_MINUTE(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime)) NET_NL()
		ENDIF
		#ENDIF
				
		//TIMER CHECK
		//Initialise
		IF GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime = INVALID_TIMEOFDAY		//IF MPGlobalsAmbience.CrateDropLaunchData.TriggerTime = INVALID_TIMEOFDAY
			
			//Don't set the time if there aren't enough players in the session - 1672349
			IF NETWORK_GET_NUM_PARTICIPANTS() < g_sMptunables.iplayerrestrictioncratedrop
			#IF IS_DEBUG_BUILD AND GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage != 88 #ENDIF
				//PRINTLN("   >>>   CRATE DROP - NETWORK_GET_NUM_PARTICIPANTS = ", NETWORK_GET_NUM_PARTICIPANTS(), " REQUIRED = ", g_sMptunables.iplayerrestrictioncratedrop)
				EXIT
			ENDIF
			PRINTLN("   >>>   CRATE DROP - NETWORK_GET_NUM_PARTICIPANTS = ", NETWORK_GET_NUM_PARTICIPANTS(), " REQUIRED = ", g_sMptunables.iplayerrestrictioncratedrop)
			
			//Check if this should be a Special Crate Drop
			GlobalServerBD_BlockB.CrateDropServerLaunchData.bUseSpecialCrate = FALSE
			IF GlobalServerBD_BlockB.CrateDropServerLaunchData.bSpecialCrateDone = FALSE
				
				IF GET_RANDOM_INT_IN_RANGE(0, 100) < g_sMPTunables.iCrateDropSpecial_crate_drop_percentage
					PRINTLN("   >>>   CRATE DROP - SPECIAL CRATE CHECK - NETWORK_GET_NUM_PARTICIPANTS = ", NETWORK_GET_NUM_PARTICIPANTS(), " REQUIRED = ", g_sMPTunables.iCrateDropSpecial_min_players)
					IF NETWORK_GET_NUM_PARTICIPANTS() >= g_sMPTunables.iCrateDropSpecial_min_players
						GlobalServerBD_BlockB.CrateDropServerLaunchData.bUseSpecialCrate = TRUE
						IF g_sMPTunables.bturnoffspecialcrates = FALSE //Tunable disables the Special Crate Drop per session limit
							GlobalServerBD_BlockB.CrateDropServerLaunchData.bSpecialCrateDone = TRUE
						ENDIF
						NET_PRINT_TIME() NET_PRINT("   >>>   CRATE DROP - bUseSpecialCrate = TRUE   <-----   <-----   <-----") NET_NL()
					ELSE
						PRINTLN("   >>>   CRATE DROP - SPECIAL CRATE CHECK - FAILED - NUM MIN PLAYERS CHECK")
					ENDIF
				ELSE
					PRINTLN("   >>>   CRATE DROP - SPECIAL CRATE CHECK - FAILED - PERCENTAGE CHECK - TUNABLE = ", g_sMPTunables.iCrateDropSpecial_crate_drop_percentage)
				ENDIF
			ELSE
				PRINTLN("   >>>   CRATE DROP - SPECIAL CRATE CHECK - FAILED - SPECIAL CRATE ALREADY DONE THIS SESSION")
			ENDIF
			
			//Set Time of Next Drop
			TIMEOFDAY CurrentTimeOfDay = GET_CURRENT_TIMEOFDAY()
			SET_TIMEOFDAY(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime, GET_TIMEOFDAY_SECOND(CurrentTimeOfDay), GET_TIMEOFDAY_MINUTE(CurrentTimeOfDay), GET_TIMEOFDAY_HOUR(CurrentTimeOfDay), GET_TIMEOFDAY_DAY(CurrentTimeOfDay), GET_TIMEOFDAY_MONTH(CurrentTimeOfDay), GET_TIMEOFDAY_YEAR(CurrentTimeOfDay))
			
			INT iHours
			INT iMinutes
			
			IF GlobalServerBD_BlockB.CrateDropServerLaunchData.bUseSpecialCrate = FALSE
				iHours 		= GET_RANDOM_INT_IN_RANGE(g_sMPTunables.iCrateDropStandardFrequencyMin, g_sMPTunables.iCrateDropStandardFrequencyMax)
				iMinutes 	= GET_RANDOM_INT_IN_RANGE(0, 60)
				ADD_TIME_TO_TIMEOFDAY(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime, 0, iMinutes, iHours, 0, 0, 0)
				NET_PRINT("   >>>   CRATE DROP - g_sMPTunables.iCrateDropStandardFrequencyMin = ")	NET_PRINT_INT(g_sMPTunables.iCrateDropStandardFrequencyMin) NET_PRINT(" g_sMPTunables.iCrateDropStandardFrequencyMax = ") NET_PRINT_INT(g_sMPTunables.iCrateDropStandardFrequencyMax) NET_NL()
			ELSE
				iHours 		= g_sMPTunables.iCrateDropSpecialFrequency
				iMinutes 	= GET_RANDOM_INT_IN_RANGE(0, 10)
				ADD_TIME_TO_TIMEOFDAY(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime, 0, iMinutes, iHours, 0, 0, 0)
				NET_PRINT("   >>>   CRATE DROP - g_sMPTunables.iCrateDropSpecialFrequency = ")	NET_PRINT_INT(g_sMPTunables.iCrateDropSpecialFrequency) NET_NL()
			ENDIF
			NET_PRINT("   >>>   CRATE DROP - TIME ADDED = Hrs:")	NET_PRINT_INT(iHours) NET_PRINT(" Mins:") NET_PRINT_INT(iMinutes) NET_NL()
			NET_PRINT("   >>>   CRATE DROP - CURRENT TIME = ") 		NET_PRINT_INT(GET_TIMEOFDAY_HOUR(CurrentTimeOfDay))NET_PRINT(":") NET_PRINT_INT(GET_TIMEOFDAY_MINUTE(CurrentTimeOfDay)) NET_NL()		//PRINT_TIMEOFDAY(CurrentTimeOfDay) NET_NL()
			NET_PRINT("   >>>   CRATE DROP - NEXT TRIGGER TIME = ")	NET_PRINT_INT(GET_TIMEOFDAY_HOUR(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime))NET_PRINT(":") NET_PRINT_INT(GET_TIMEOFDAY_MINUTE(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime)) NET_NL()		//PRINT_TIMEOFDAY(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime) NET_NL()
			
		ELSE
			IF IS_NOW_AFTER_TIMEOFDAY(GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime)
			AND ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH(4, 1, 3, TRUE, TRUE)
				IF NETWORK_GET_NUM_PARTICIPANTS() < g_sMptunables.iplayerrestrictioncratedrop
				AND GlobalServerBD_BlockB.CrateDropServerLaunchData.bUseSpecialCrate = FALSE
				#IF IS_DEBUG_BUILD AND GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage != 88 #ENDIF
					GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime = INVALID_TIMEOFDAY
					NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_START_CRATE_DROP_SERVER - TRIGGERED TIME PASSED - RESET TIMER DUE TO LACK OF PLAYERS <-----     ") NET_NL()
					PRINTLN("   >>>   CRATE DROP - NETWORK_GET_NUM_PARTICIPANTS = ", NETWORK_GET_NUM_PARTICIPANTS(), " REQUIRED = ", g_sMptunables.iplayerrestrictioncratedrop)
				ELSE
					GlobalServerBD_BlockB.CrateDropServerLaunchData.TriggerTime = INVALID_TIMEOFDAY
					GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage = 0
					NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_START_CRATE_DROP_SERVER - TRIGGERED TIME PASSED - START LAUNCH - iLaunchStage = 0  <-----     ") NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//PURPOSE: Launches the Crate Drop script
PROC PROCESS_LAUNCH_CRATE_DROP_SERVER()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		SWITCH GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage
			//Launch Crate Drop
			CASE 0
				//MPGlobalsAmbience.CrateDropLaunchData.missionData.mdID.idMission = eAM_CRATE_DROP
				//MPGlobalsAmbience.CrateDropLaunchData.missionData.iInstanceId = DEFAULT_MISSION_INSTANCE
			   	//IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(MPGlobalsAmbience.CrateDropLaunchData.missionData)
				//	BROADCAST_JOIN_THIS_AMBIENT_MP_MISSION_EVENT(MPGlobalsAmbience.CrateDropLaunchData.missionData, ALL_PLAYERS())
					GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage = 1
					NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_SERVER - LAUNCHED - iLaunchStage = 1  <-----     ") NET_NL()
				//ENDIF
			BREAK
			
			//Check if Crate Drop has Started
			CASE 1
				//IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_CRATE_DROP")) > 0
				IF NETWORK_IS_SCRIPT_ACTIVE("AM_CRATE_DROP")
					GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage = -1
					NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_SERVER - STARTED - iLaunchStage = -1  <-----     ") NET_NL()
				ENDIF
			BREAK
			
			#IF IS_DEBUG_BUILD
			CASE 88
				IF NETWORK_IS_SCRIPT_ACTIVE("AM_CRATE_DROP")
					GlobalServerBD_BlockB.CrateDropServerLaunchData.iLaunchStage = -1
					NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_LAUNCH_CRATE_DROP_SERVER - DEBUG LAUNCH STARTED - iLaunchStage = -1  <-----     ") NET_NL()
				ENDIF
			BREAK
			#ENDIF
		ENDSWITCH
	ENDIF
ENDPROC

//PURPOSE: (SERVER ONLY) Controls the launching of a Crate Drop 
PROC MAINTAIN_CRATE_DROP_SERVER()
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		PROCESS_START_CRATE_DROP_SERVER()
		PROCESS_LAUNCH_CRATE_DROP_SERVER()
	ENDIF
ENDPROC




