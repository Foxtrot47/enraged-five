

//----------------------
//	INCLUDES
//----------------------
USING "globals.sch"
USING "net_prints.sch"
USING "net_gang_boss_common.sch"
USING "net_include.sch"
//USING "net_spawn.sch"
USING "net_vehicle_drop.sch"

//----------------------
//	ENUM
//----------------------



//----------------------
//	CONSTANTS
//----------------------

CONST_INT GB_LIMO_SPAWN_TIMEOUT		10000

CONST_INT GB_LIMO_SPAWN_RANGE		50

CONST_INT GB_VEH_BODY_HEALTH		4000
CONST_INT GB_VEH_ENG_HEALTH			2000
CONST_INT GB_VEH_PET_HEALTH			2000

CONST_INT GB_VEH_INVINCIBLE_TIME	10000

CONST_INT GB_DISTANCE_TO_BOSS		10

//Local Bitset
CONST_INT GBL_LOCAL_BS_GOON_DELIVERED_LIMO	0
CONST_INT GBL_LOCAL_BS_VEHICLE_SPAWNING_SET	1
CONST_INT GBL_LOCAL_BS_SPAWNING_TOO_LONG	2

CONST_INT GB_VEHICLE_MAX_SPAWN_RANGE	1000
CONST_INT GB_VEHICLE_MIN_SPAWN_RANGE	10

CONST_INT GB_BOAT_CREATION_RADIUS		50
CONST_INT GB_BOAT_CREATION_HEIGHT		10

//----------------------
//	STRUCT
//----------------------

//Data stored by the boss about the limo
STRUCT GANG_BOSS_LIMO_LOCAL_DATA

	//Boss Only
	INT iSpawnBS
	VECTOR vDropLocation
	INT iNode
	VECTOR pos
	FLOAT fHeading

	SCRIPT_TIMER spawnTimer
	
	INT iLocalBS
	INT iPlayerStagger
	//Other
	BLIP_INDEX vehBlip
	TIME_DATATYPE timeLeaveEvent
	
ENDSTRUCT

//----------------------
//	FUNCTIONS
//----------------------

FUNC INT GB_GET_BOSS_VEHICLE_MIN_CALL_RANGE_TUNABLE()

	RETURN g_sMPTunables.igb_called_vehicle_proximity

ENDFUNC

FUNC BOOL GB_IS_CURRENT_BOSS_VEHICLE_A_HELI()

	RETURN IS_THIS_MODEL_A_HELI(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.VehicleModel)

ENDFUNC

FUNC BOOL GB_IS_CURRENT_BOSS_VEHICLE_A_BIKE()

	RETURN IS_THIS_MODEL_A_BIKE(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.VehicleModel)

ENDFUNC

FUNC BOOL GB_IS_CURRENT_BOSS_VEHICLE_A_LIMO()

	RETURN (GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.VehicleModel = LIMO2)

ENDFUNC

FUNC BOOL GB_IS_CURRENT_BOSS_VEHICLE_A_BOAT()

	RETURN IS_THIS_MODEL_A_BOAT(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.VehicleModel)

ENDFUNC

FUNC BOOL GB_DOES_BOSS_VEHICLE_EXIST(PLAYER_INDEX playerID, BOOL bDoDriveableCheck = FALSE)

	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID)
		VEHICLE_INDEX bossVeh = GB_GET_GANG_BOSS_LIMO_INDEX(playerID)
		IF DOES_ENTITY_EXIST(bossVeh)
			IF NOT bDoDriveableCheck
			OR IS_VEHICLE_DRIVEABLE(bossVeh)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC MODEL_NAMES GB_GET_MODEL_OF_BOSS_VEHICLE()

	IF GB_DOES_BOSS_VEHICLE_EXIST(PLAYER_ID())
		RETURN GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.VehicleModel
	ENDIF
	
	//CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_GET_MODEL_OF_BOSS_VEHICLE - Returning DUMMY_MODEL_FOR_SCRIPT!!!")
	RETURN DUMMY_MODEL_FOR_SCRIPT

ENDFUNC

FUNC BOOL GB_IS_LOCAL_BOSS_NEAR_BOSS_LIMO()
		
	VEHICLE_INDEX tempveh = MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())] 
	IF DOES_ENTITY_EXIST(tempveh)
		IF IS_VEHICLE_DRIVEABLE(tempveh)
			PED_INDEX pedID = GET_PLAYER_PED(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			IF NOT IS_PED_INJURED(pedID)
				IF IS_PED_IN_VEHICLE(pedID, tempveh)
				OR IS_ENTITY_AT_ENTITY(pedID, tempveh, <<GB_GET_BOSS_VEHICLE_MIN_CALL_RANGE_TUNABLE(), GB_GET_BOSS_VEHICLE_MIN_CALL_RANGE_TUNABLE(), GB_GET_BOSS_VEHICLE_MIN_CALL_RANGE_TUNABLE()>>,TRUE)
					CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_IS_LOCAL_BOSS_NEAR_BOSS_LIMO - player in range or in vehicle.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL GB_IS_BOSS_LIMO_EMPTY()

	VEHICLE_INDEX tempveh = MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())] 
	IF DOES_ENTITY_EXIST(tempveh)
		IF IS_VEHICLE_DRIVEABLE(tempveh)
			IF NOT IS_VEHICLE_EMPTY(tempveh)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL GB_IS_PLAYER_DRIVING_BOSS_LIMO(PLAYER_INDEX playerID)

	IF IS_NET_PLAYER_OK(playerID)
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID)
			VEHICLE_INDEX tempveh = MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(GB_GET_THIS_PLAYER_GANG_BOSS(playerID))] 
			IF DOES_ENTITY_EXIST(tempveh)
				IF IS_VEHICLE_DRIVEABLE(tempveh)
					PED_INDEX pedID = GET_PLAYER_PED(playerID)
					IF IS_PED_SITTING_IN_VEHICLE_SEAT(pedID,tempveh,VS_DRIVER)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL GB_IS_THIS_VEHICLE_AN_OLD_BV_CLONE_OF_PLAYER(VEHICLE_INDEX VehicleID, INT iPlayerNameHash)

	CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_IS_THIS_VEHICLE_AN_OLD_BV_CLONE_OF_PLAYER = Vehicle = ",GET_MODEL_NAME_FOR_DEBUG (GET_ENTITY_MODEL(VehicleID)))

	IF DOES_ENTITY_EXIST(VehicleID)	
	AND NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)
	AND IS_VEHICLE_EMPTY(VehicleID, TRUE, TRUE)
	AND NOT IS_ENTITY_A_MISSION_ENTITY(VehicleID)	
		CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_IS_THIS_VEHICLE_AN_OLD_BV_CLONE_OF_PLAYER - NOT IS_ENTITY_A_MISSION_ENTITY")
		IF DECOR_EXIST_ON(VehicleID, "Previous_Boss")
			CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_IS_THIS_VEHICLE_AN_OLD_BV_CLONE_OF_PLAYER = iPlayerNameHash = ",iPlayerNameHash," decorator = ",DECOR_GET_INT(VehicleID, "Previous_Boss"))
			IF (DECOR_GET_INT(VehicleID, "Previous_Boss") = iPlayerNameHash)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN FALSE

ENDFUNC

PROC GB_DELETE_BV_CLONE(VEHICLE_INDEX VehicleID)
	CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] called GB_DELETE_BV_CLONE ") 
	DEBUG_PRINTCALLSTACK()
	
	SET_ENTITY_AS_MISSION_ENTITY(VehicleID, FALSE, TRUE)
	DELETE_VEHICLE(VehicleID)		
ENDPROC

PROC GB_DELETE_OLD_BOSS_VEHICLES_FOR_PLAYER(PLAYER_INDEX playerID)

	// check any nearby vehicles
	VEHICLE_INDEX VehicleID[32] 
	INT iNumVehicles, i
	INT iPlayerNameHash = NETWORK_HASH_FROM_PLAYER_HANDLE(playerID)
	
	iNumVehicles = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), VehicleID)
	CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_DELETE_OLD_BOSS_VEHICLES_FOR_PLAYER = iNumVehicles = ",iNumVehicles)
	REPEAT iNumVehicles i
		IF DOES_ENTITY_EXIST(VehicleID[i])
			IF GB_IS_THIS_VEHICLE_AN_OLD_BV_CLONE_OF_PLAYER(VehicleID[i], iPlayerNameHash)
				GB_DELETE_BV_CLONE(VehicleID[i])
				CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_DELETE_OLD_BOSS_VEHICLES_FOR_PLAYER - deleting old clone for player ",GET_PLAYER_NAME(PlayerID))
			ELSE
			#IF IS_DEBUG_BUILD
				
				NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_DELETE_OLD_BOSS_VEHICLES_FOR_PLAYER - checking vehicle model ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG (GET_ENTITY_MODEL(VehicleID[i])))
				NET_PRINT(" at coords ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(VehicleID[i], FALSE))  									
				NET_PRINT_VEHICLE_DECORATOR_DETAILS(VehicleID[i])					
				NET_NL()
				
			#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT


ENDPROC


PROC GB_KICK_PLAYER_FROM_BOSS_VEHICLE(PLAYER_INDEX playerID, VEHICLE_INDEX bossVeh)

	VEHICLE_INDEX limoVeh = bossVeh
	PED_INDEX pedID = GET_PLAYER_PED(playerID)
	
	IF IS_VEHICLE_DRIVEABLE(limoVeh)
		IF IS_PED_IN_VEHICLE(pedID,limoVeh,TRUE)
			BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(playerID), FALSE, 0, 0, FALSE, FALSE)
			CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_KICK_PLAYER_FROM_BOSS_VEHICLE - Kicking ",GET_PLAYER_NAME(playerID)," from vehicle ",NATIVE_TO_INT(limoVeh))
		ENDIF
	ENDIF

ENDPROC

PROC GB_SET_BOSS_LIMO_LOCKS(VEHICLE_INDEX limoVeh, PLAYER_INDEX gangID)

	INT iPlayer

	//Set all doors to be locked initially
	SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(limoVeh,TRUE)
	CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_SET_BOSS_LIMO_LOCKS - Limo belonging to ",GET_PLAYER_NAME(gangID)," locked for all players.")

	//Check the locks for each player on that limo
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iPlayer)
		
		IF IS_NET_PLAYER_OK(playerID,FALSE)
			//If it's me an I'm being dismissed then kick me and lock me out
			IF playerID = PLAYER_ID()
			AND (MPGlobalsAmbience.sMagnateGangBossData.eEndBeingGoonReasonFromBossEvent = eGBENDBEINGGOONREASON_SACKED
			OR MPGlobalsAmbience.sMagnateGangBossData.eEndBeingGoonReasonFromBossEvent = eGBENDBEINGGOONREASON_TERMINATED)
				GB_KICK_PLAYER_FROM_BOSS_VEHICLE(playerID,limoVeh)
			//If player is a member of the gang that owns the limo, unlock the doors for them
			ELIF (GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID,gangID)
			OR GB_IS_PLAYER_JOINING_THIS_GANG(playerID,gangID))
				SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(limoVeh,playerID,FALSE)
				CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_SET_BOSS_LIMO_LOCKS - Limo belonging to ",GET_PLAYER_NAME(gangID)," unlocked for player ",GET_PLAYER_NAME(playerID))
			ELSE
				//If player should not have access, kick them out
				GB_KICK_PLAYER_FROM_BOSS_VEHICLE(playerID,limoVeh)
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC GB_SET_ALL_BOSS_LIMO_LOCKS()

	INT iLimo
	
	//Go through all limos in session
	REPEAT NUM_NETWORK_PLAYERS iLimo
		VEHICLE_INDEX limoVeh = MPGlobalsAmbience.sMagnateGangBossData.bossLimo[iLimo] 
	
		IF IS_VEHICLE_DRIVEABLE(limoVeh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(limoVeh)
				PLAYER_INDEX gangID = INT_TO_PLAYERINDEX(iLimo)
				IF IS_NET_PLAYER_OK(gangID,FALSE)
					GB_SET_BOSS_LIMO_LOCKS(limoVeh,gangID)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_SET_ALL_BOSS_LIMO_LOCKS - Done.")
	
ENDPROC

PROC LOCK_LIMO_DOORS_FOR_NON_GOONS()

	IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
	AND NOT GB_IS_PLAYER_JOINING_A_GANG(PLAYER_ID())
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND GB_IS_PLAYER_IN_ANY_GANG_BOSS_LIMO(PLAYER_ID())
			BROADCAST_GB_REFRESH_LIMO_LOCKS(ALL_PLAYERS())
			CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] LOCK_LIMO_DOORS_FOR_NON_GOONS")
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL GB_IS_BOSS_VEHICLE_AVAILABLE_FOR_SESSION_START()

	RETURN (g_BossVehicleToSpawnOnSessionStart != DUMMY_MODEL_FOR_SCRIPT)

ENDFUNC

FUNC MODEL_NAMES GB_GET_BOSS_VEHICLE_FOR_SESSION_START()

	RETURN g_BossVehicleToSpawnOnSessionStart

ENDFUNC

FUNC INT GB_GET_GANG_BOSS_LIMO_CREATION_STATE()

	RETURN GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.iLimoCreationState

ENDFUNC

FUNC INT GB_GET_GANG_BOSS_LIMO_CREATION_STATE_INT(INT iBoss)

	RETURN GlobalplayerBD_FM_3[iBoss].sMagnateGangBossData.iLimoCreationState

ENDFUNC

FUNC BOOL GB_IS_PLAYER_NEAR_BOSS_LIMO(PLAYER_INDEX playerID, BOOL bDo3DCheck = TRUE, INT iOverrideRadius = -1)

	INT iX, iY, iZ
	iX = GB_GET_BOSS_VEHICLE_MIN_CALL_RANGE_TUNABLE()
	iY = GB_GET_BOSS_VEHICLE_MIN_CALL_RANGE_TUNABLE()
	iZ = GB_GET_BOSS_VEHICLE_MIN_CALL_RANGE_TUNABLE()
	
	IF iOverrideRadius != -1
		iX = iOverrideRadius
		iY = iOverrideRadius
		iZ = iOverrideRadius
	ENDIF
		
	//CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_IS_PLAYER_NEAR_BOSS_LIMO - Being called.")
	VEHICLE_INDEX tempveh = MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())] 
	IF DOES_ENTITY_EXIST(tempveh)
		IF IS_VEHICLE_DRIVEABLE(tempveh)
			PED_INDEX pedID = GET_PLAYER_PED(playerID)
			IF NOT IS_PED_INJURED(pedID)
				IF IS_PED_IN_VEHICLE(pedID, tempveh)
				OR IS_ENTITY_AT_ENTITY(pedID, tempveh, <<iX, iY, iZ>>, bDo3DCheck)
					CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_IS_PLAYER_NEAR_BOSS_LIMO - player in range or in vehicle.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL GB_IS_ANY_GANG_MEMBER_NEAR_BOSS_LIMO(BOOL bIncludeNonOrgPlayers = TRUE)
	INT iPlayer
	PLAYER_INDEX gangID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	//Check the locks for each player on that limo
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_NET_PLAYER_OK(playerID,TRUE)
			IF (GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, gangID)
			OR GB_IS_PLAYER_JOINING_THIS_GANG(playerID, gangID))
				IF GB_IS_PLAYER_NEAR_BOSS_LIMO(playerID)
					RETURN TRUE
				ENDIF
			ENDIF
			
			//3472951
			IF bIncludeNonOrgPlayers
				IF GB_IS_PLAYER_NEAR_BOSS_LIMO(playerID,DEFAULT,2)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC
FUNC BOOL GB_IS_LOCAL_PLAYER_NEAR_BOSS_LIMO(BOOL bDo3DCheck = TRUE, INT iOverrideRadius = -1)
		
	RETURN GB_IS_PLAYER_NEAR_BOSS_LIMO(PLAYER_ID(), bDo3DCheck, iOverrideRadius)

ENDFUNC

FUNC STRING GET_GBL_MISSION_STATE_STRING(GBL_STAGE iState)

	SWITCH(iState)
		CASE GBL_IDLE			RETURN "GBL_IDLE"
		CASE GBL_SPAWN 			RETURN "GBL_SPAWN"
		CASE GBL_MAINTAIN 		RETURN "GBL_MAINTAIN"
		CASE GBL_CLEANUP		RETURN "GBL_CLEANUP"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_BOSS_GBL_STATE(GBL_STAGE iState)
	GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.limoStage = iState
	CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] SET_BOSS_GBL_STATE = ",GET_GBL_MISSION_STATE_STRING(iState))
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    vehicleModel - 
///    blipColour - leave this as default if you want to use gang boss blip colour 
PROC GB_CREATE_VEHICLE_REQUEST(MODEL_NAMES vehicleModel, INT blipColour = -1, STRING blipName = NULL)

	GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iLimoCreationState = GB_LIMO_CREATION_PENDING
	GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel = vehicleModel
	GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iLimoBlipColour  = blipColour
	GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tl15LimoBlipName  = blipName
	IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.limoStage != GBL_IDLE
		SET_BOSS_GBL_STATE(GBL_IDLE)
	ENDIF
	
	PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_CREATE_VEHICLE_REQUEST for model: ",GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel = vehicleModel)

ENDPROC

//PROC GB_SET_PLAYER_SHOULD_SPAWN_IN_BOSS_VEHICLE(GANG_BOSS_LIMO_LOCAL_DATA &limoData, BOOL bSet)
//
//	VEHICLE_INDEX limoVeh = GB_GET_GANG_BOSS_LIMO_INDEX(PLAYER_ID())
//
//	IF bSet
//		IF NOT IS_BIT_SET(limoData.iLocalBS,GBL_LOCAL_BS_VEHICLE_SPAWNING_SET)
//			SET_PLAYER_TO_RESPAWN_IN_SPECIFIC_RESPAWN_VEHICLE(limoVeh)
//			SET_BIT(limoData.iLocalBS,GBL_LOCAL_BS_VEHICLE_SPAWNING_SET)
//			PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_SET_PLAYER_SPAWN_IN_BOSS_VEHICLE - TRUE")
//			DEBUG_PRINTCALLSTACK()
//		ENDIF
//	ELSE
//		IF IS_BIT_SET(limoData.iLocalBS,GBL_LOCAL_BS_VEHICLE_SPAWNING_SET)
//			CLEAR_SPECIFIC_VEHICLE_TO_RESPAWN_IN()
//			CLEAR_BIT(limoData.iLocalBS,GBL_LOCAL_BS_VEHICLE_SPAWNING_SET)
//			PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_SET_PLAYER_SPAWN_IN_BOSS_VEHICLE - FALSE")
//			DEBUG_PRINTCALLSTACK()
//		ENDIF
//	ENDIF
//
//ENDPROC

FUNC BLIP_SPRITE GB_GET_SPRITE_FOR_BOSS_VEHICLE()

	IF GB_IS_CURRENT_BOSS_VEHICLE_A_HELI()
		RETURN RADAR_TRACE_HELICOPTER
	ELIF GB_IS_CURRENT_BOSS_VEHICLE_A_LIMO()	
		RETURN RADAR_TRACE_TURRETED_LIMO
	ELIF GB_IS_CURRENT_BOSS_VEHICLE_A_BIKE()
		RETURN RADAR_TRACE_GANG_BIKE
	ELIF GB_IS_CURRENT_BOSS_VEHICLE_A_BOAT()
		RETURN RADAR_TRACE_PLAYER_BOAT
	ELSE
		RETURN RADAR_TRACE_GANG_VEHICLE
	ENDIF

	RETURN RADAR_TRACE_GANG_VEHICLE

ENDFUNC

FUNC STRING GB_GET_STRING_SPRITE_FOR_BOSS_VEHICLE()

	IF GB_IS_CURRENT_BOSS_VEHICLE_A_HELI()
		RETURN "GB_HELI_BLIP"
	ELIF GB_IS_CURRENT_BOSS_VEHICLE_A_LIMO()
		RETURN "GB_LIMO_BLIP"
	ELIF GB_IS_CURRENT_BOSS_VEHICLE_A_BOAT()
		RETURN "GB_BOAT_BLIP"
	ELIF GB_IS_CURRENT_BOSS_VEHICLE_A_BIKE()
		RETURN "GB_BIKE_BLIP"
	ELSE
		RETURN "GB_VEH_BLIP"
	ENDIF

	RETURN ""

ENDFUNC

PROC ADD_BOSS_LIMO_BLIP(BLIP_INDEX &vehBlip)

	IF NOT DOES_BLIP_EXIST(vehBlip)
		IF GET_GANG_ID_FOR_PLAYER(PLAYER_ID()) != -1
			vehBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.niBossLimo))
			SET_BLIP_SPRITE(VehBlip, GB_GET_SPRITE_FOR_BOSS_VEHICLE())
			HUD_COLOURS blipColour 
			IF GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.iLimoBlipColour = -1
				blipColour = GET_HUD_COLOUR_FOR_GANG_ID(GET_GANG_ID_FOR_PLAYER(PLAYER_ID()))
			ELSE
				blipColour = INT_TO_ENUM(HUD_COLOURS, GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.iLimoBlipColour)
			ENDIF
			SET_BLIP_PRIORITY(VehBlip,BLIPPRIORITY_HIGH)
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(VehBlip, blipColour)
			IF NOT IS_STRING_NULL_OR_EMPTY(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.tl15LimoBlipName)
				SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip, GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.tl15LimoBlipName)
			ELIF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip, "MC_V_BLIP")
			ELSE
				IF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
					SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip, "GB_LIMOC")	//CEO Vehicle
				ELSE
					SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip, "GB_LIMO")	//Boss Vehicle
				ENDIF
			ENDIF
			CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] ADD_BOSS_LIMO_BLIP - ",HUD_COLOUR_AS_STRING(blipColour))
		ENDIF
	ELSE
		IF GB_IS_CURRENT_BOSS_VEHICLE_A_LIMO()
			SET_BLIP_ROTATION(vehBlip, ROUND(GET_ENTITY_HEADING_FROM_EULERS(NET_TO_VEH(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.niBossLimo))))
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_BOSS_LIMO_BLIP(BLIP_INDEX &vehBlip)

	IF DOES_BLIP_EXIST(vehBlip)
		REMOVE_BLIP(vehBlip)
		CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] REMOVE_BOSS_LIMO_BLIP")
	ENDIF
	
ENDPROC

PROC GB_SET_LIMO_SPAWN_POINT(GANG_BOSS_LIMO_LOCAL_DATA &limoData)

		// Allow limo to be spawned at exact coords of our choice. This is used to recreate helicopter that could be destroyed in office-heli transition and we want it back on the helipad.
		IF MPGlobalsAmbience.sMagnateGangBossData.bSpawnBossLimoAtExactCoords
			limoData.pos = MPGlobalsAmbience.sMagnateGangBossData.vBossLimoExactSpawnPoint
			limoData.fHeading = MPGlobalsAmbience.sMagnateGangBossData.fBossLimoExactSpawnHeading
			
			SET_BIT(limoData.iSpawnBS, VDP_CreationCoordsFound)
			
			MPGlobalsAmbience.sMagnateGangBossData.bSpawnBossLimoAtExactCoords = FALSE
			
			CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_SET_LIMO_SPAWN_POINT bSpawnBossLimoAtExactCoords = TRUE, using coords ", limoData.pos, " and heading ", limoData.fHeading)
			EXIT
		ENDIF

	IF MPGlobalsAmbience.sMagnateGangBossData.limoSpawnPlayer != INVALID_PLAYER_INDEX()
		CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_SET_LIMO_SPAWN_POINT - ",GET_PLAYER_NAME(MPGlobalsAmbience.sMagnateGangBossData.limoSpawnPlayer))
		IF IS_NET_PLAYER_OK(MPGlobalsAmbience.sMagnateGangBossData.limoSpawnPlayer)
			PED_INDEX pedID = GET_PLAYER_PED(MPGlobalsAmbience.sMagnateGangBossData.limoSpawnPlayer)
			limoData.vDropLocation = GET_ENTITY_COORDS(pedID)
			CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_SET_LIMO_SPAWN_POINT near player ",GET_PLAYER_NAME(MPGlobalsAmbience.sMagnateGangBossData.limoSpawnPlayer)," at ",limoData.vDropLocation)
			EXIT
		ENDIF
	ENDIF
	
	limoData.vDropLocation = GET_PLAYER_COORDS(PLAYER_ID())
	CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_SET_LIMO_SPAWN_POINT local player at ",limoData.vDropLocation)

ENDPROC

FUNC VECTOR GB_GET_RANDOM_POINT_IN_DONUT(VECTOR vPos, FLOAT fInnerRadius, FLOAT fOuterRadius, FLOAT fHeight)
 
	// Pick a random unit direction vector.
	VECTOR vDir = << GET_RANDOM_FLOAT_IN_RANGE(-1,1) , GET_RANDOM_FLOAT_IN_RANGE(-1,1), 0 >>
	FLOAT fHalfHeight = fHeight / 2.0
	
	// Project this vector into the disc some random distance.
	vDir = GET_VECTOR_OF_LENGTH(vDir, GET_RANDOM_FLOAT_IN_RANGE(fInnerRadius, fOuterRadius))
	
	// Grab a random height and add it on.
	vDir.z = GET_RANDOM_FLOAT_IN_RANGE(-fHalfHeight, fHalfHeight)
	
	RETURN vPos + vDir
ENDFUNC

PROC GB_GET_BOAT_CREATION_POINT(VECTOR vCentre, VECTOR &vPos, FLOAT &fHeading)
	vPos = GB_GET_RANDOM_POINT_IN_DONUT(vCentre, GB_BOAT_CREATION_RADIUS/2, GB_BOAT_CREATION_RADIUS, GB_BOAT_CREATION_HEIGHT)
	fHeading = GET_HEADING_BETWEEN_VECTORS_2D(vPos, vCentre)
ENDPROC

PROC SET_BOSS_HELI_SPAWN_PROOFS(VEHICLE_INDEX tempveh, GANG_BOSS_LIMO_LOCAL_DATA &limoData)

	IF GB_IS_CURRENT_BOSS_VEHICLE_A_HELI()
		SET_ENTITY_INVINCIBLE(tempveh,TRUE)
		REINIT_NET_TIMER(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vehInvincibilityTimer)
		SET_ENTITY_PROOFS(tempveh,FALSE,FALSE,FALSE,TRUE,FALSE)
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vLimoCreationPoint = limoData.pos
		CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] SET_BOSS_HELI_SPAWN_PROOFS - TRUE - on spawn.")
	ENDIF

ENDPROC

FUNC BOOL GB_CREATE_BOSS_LIMO(NETWORK_INDEX &niVeh,GANG_BOSS_LIMO_LOCAL_DATA &limoData)

	IF REQUEST_LOAD_MODEL(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel)
			
		IF HAS_NET_TIMER_EXPIRED(limoData.spawnTimer,GB_LIMO_SPAWN_TIMEOUT)
			SET_BIT(limoData.iLocalBS,GBL_LOCAL_BS_SPAWNING_TOO_LONG)
			CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] Spawning taking too long, ignoring sight checks.")
		ENDIF
		
		VEHICLE_SPAWN_LOCATION_PARAMS Params
		Params.fMinDistFromCoords= VD_VEHICLE_MIN_SPAWN_RANGE
		Params.fMaxDistance= VD_VEHICLE_MAX_SPAWN_RANGE
		
		BOOL bUseRoadOffset = TRUE
		IF GB_IS_CURRENT_BOSS_VEHICLE_A_HELI()
			bUseRoadOffset = FALSE
		ENDIF
		
		BOOL bSpawn
		IF GB_IS_CURRENT_BOSS_VEHICLE_A_BOAT()
			GB_GET_BOAT_CREATION_POINT(limoData.vDropLocation,limoData.pos, limoData.fHeading)
			
			CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] attempting to spawn at ",limoData.pos)
			
			IF TEST_PROBE_AGAINST_WATER(limoData.pos+<<0, 0, 2>>, limoData.pos-<<0, 0, GB_BOAT_CREATION_HEIGHT>>, limoData.pos)
			OR IS_BIT_SET(limoData.iLocalBS,GBL_LOCAL_BS_SPAWNING_TOO_LONG)
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(limoData.pos)
					IF IS_LOCATION_ABOVE_GROUND(limoData.pos)
					OR IS_BIT_SET(limoData.iLocalBS,GBL_LOCAL_BS_SPAWNING_TOO_LONG)
						bSpawn = TRUE
						CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] spawn boat coords = ", limoData.pos)
						CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] spawn boat heading = ", limoData.fHeading)
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] spawn boat - Point not above ground.")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] spawn boat - Point not ok for net entity creation")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] spawn boat - test probe against water failed")
			ENDIF
		ELSE
			
			IF GB_IS_CURRENT_BOSS_VEHICLE_A_HELI()
				Params.bIgnoreCustomNodesForArea = TRUE
				CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] spawning a heli so ignoring custom vehicle nodes added for area.")
			ENDIF
			
			IF IS_LOCAL_PLAYER_IN_FORT_ZANCUDO() OR IS_LOCAL_PLAYER_IN_LSIA()
				
				IF DOES_PLAYER_OWN_A_HANGER(PLAYER_ID())
				OR (GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID()) AND DOES_PLAYER_OWN_A_HANGER(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
					Params.bIgnoreCustomNodesForArea = FALSE
					NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] Setting bIgnoreCustomNodesForArea = FALSE") NET_NL()
					
					Params.bAvoidSpawningInExclusionZones = FALSE
					NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] Setting bAvoidSpawningInExclusionZones = FALSE") NET_NL()
					
					Params.bUseExactCoordsIfPossible = TRUE
					NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] Setting bUseExactCoordsIfPossible = TRUE") NET_NL()
					
					Params.bStartOfMissionPVSpawn = TRUE
					NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] Setting bStartOfMissionPVSpawn = TRUE") NET_NL()
					
					g_SpawnData.bIsBossSpawningInAircraft = TRUE
				ENDIF
			ENDIF
		
			IF IS_BIT_SET(limoData.iSpawnBS, VDP_CreationCoordsFound)
			OR HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(limoData.vDropLocation, <<0.0, 0.0, 0.0>>, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel, bUseRoadOffset, limoData.pos, limoData.fHeading, Params)
		
				SET_BIT(limoData.iSpawnBS, VDP_CreationCoordsFound)
				NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] - GB_CREATE_BOSS_LIMO - VDP_CreationCoordsFound SET") NET_NL()
		
				IF IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP(limoData.vDropLocation, limoData.pos, GB_VEHICLE_MAX_SPAWN_RANGE, GB_VEHICLE_MIN_SPAWN_RANGE,TRUE,IS_BIT_SET(limoData.iLocalBS,GBL_LOCAL_BS_SPAWNING_TOO_LONG))
					bSpawn = TRUE
				ELSE
					CLEAR_BIT(limoData.iSpawnBS, VDP_CreationCoordsFound)
					NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] - GB_CREATE_BOSS_LIMO - VDP_CreationCoordsFound CLEARED - A") NET_NL()
					GET_CLOSE_VEHICLE_NODE(GET_PLAYER_COORDS(PLAYER_ID()), limoData.vDropLocation, limoData.iNode)
					NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] - GB_CREATE_BOSS_LIMO - OVER MAX VEHICLE_SPAWN_RANGE LIMIT") NET_PRINT_FLOAT(GB_VEHICLE_MAX_SPAWN_RANGE) NET_NL()
				ENDIF
			ELSE
				NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] - GB_CREATE_BOSS_LIMO - HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS = FALSE") NET_NL()
			ENDIF
		ENDIF

		IF bSpawn							
			CLEAR_AREA_OF_VEHICLES(limoData.pos, 5.0)//, TRUE, FALSE, FALSE, TRUE)
			
			NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] - GB_CREATE_BOSS_LIMO - spawn pos generated = ") NET_PRINT_VECTOR(limoData.pos) NET_PRINT(" heading = ") NET_PRINT_FLOAT(limoData.fHeading) NET_PRINT(" vDropLocation (place navigating to = ") NET_PRINT_VECTOR(limoData.vDropLocation) NET_NL()
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
			
				//If doesn't exist, create it
				IF CAN_REGISTER_MISSION_VEHICLES(1)
					CLEAR_AREA(limoData.pos, 2.0, TRUE)
					IF CREATE_NET_VEHICLE(niVeh,GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel,limoData.pos, limoData.fHeading,FALSE,TRUE,TRUE)
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(niVeh,PLAYER_ID(),TRUE) 
								
						VEHICLE_INDEX tempveh= NET_TO_VEH(niVeh)
								
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempveh,TRUE)
						SET_VEHICLE_ON_GROUND_PROPERLY(tempveh)
						LOCK_DOORS_WHEN_NO_LONGER_NEEDED(tempveh)
						SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(tempveh,TRUE)
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempveh,TRUE)
						
						//Tankiness
//							IF GB_IS_CURRENT_BOSS_VEHICLE_A_LIMO()
							IF NOT GB_IS_CURRENT_BOSS_VEHICLE_A_BOAT()
								SET_VEHICLE_TYRES_CAN_BURST(tempveh,FALSE)
							ENDIF
//								SET_VEHICLE_BODY_HEALTH(tempveh,GB_VEH_BODY_HEALTH)
//								SET_VEHICLE_ENGINE_HEALTH(tempveh,GB_VEH_ENG_HEALTH)
//								SET_VEHICLE_PETROL_TANK_HEALTH(tempveh,GB_VEH_PET_HEALTH)
//								SET_VEHICLE_HAS_STRONG_AXLES(tempveh,TRUE)
//								SET_VEHICLE_STRONG(tempveh,TRUE)
//								SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(tempveh,FALSE)
//								IF GET_NUM_MOD_KITS(tempveh) > 0
//									SET_VEHICLE_MOD_KIT(tempveh, 0)
//									SET_VEHICLE_MOD(tempveh, MOD_ARMOUR, GET_NUM_VEHICLE_MODS(tempveh, MOD_ARMOUR)-1)
//								ENDIF 
//							ENDIF

						SET_BOSS_HELI_SPAWN_PROOFS(tempveh, limoData)
						
						VEHICLE_SETUP_STRUCT_MP VehicleSetupMP
						GET_VEHICLE_SETUP_MP(tempveh, VehicleSetupMP)
						
						//Black
						IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel = BJXL
							SET_VEHICLE_COLOURS(tempveh,0,0)
						ELIF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel = SUPERVOLITO
							SET_VEHICLE_COLOURS(tempveh,40,12)
							SET_VEHICLE_EXTRA_COLOURS(tempveh,12,12)
						ELIF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel = HAVOK
							IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = PLAYER_ID()
								IF  GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
									SET_VEHICLE_COLOURS(tempveh,12,12)
									SET_VEHICLE_EXTRA_COLOURS(tempveh,12,12)
								ELSE
									SET_VEHICLE_COLOURS(tempveh,12,40)
									SET_VEHICLE_EXTRA_COLOURS(tempveh,12,40)	
								ENDIF
							ENDIF
							
						ELIF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel = COGCABRIO
							VehicleSetupMP.VehicleSetup.iColour1 = 141
							VehicleSetupMP.VehicleSetup.iColour2 = 1
							VehicleSetupMP.VehicleSetup.iColourExtra1 = 70
							VehicleSetupMP.VehicleSetup.iColourExtra2 = 82
							VehicleSetupMP.iColour5 = 1
							VehicleSetupMP.iColour6 = 132
							VehicleSetupMP.iLivery2 = 0
							VehicleSetupMP.VehicleSetup.iWindowTintColour = 1
							VehicleSetupMP.VehicleSetup.iWheelType = 7
							VehicleSetupMP.VehicleSetup.iTyreR = 255
							VehicleSetupMP.VehicleSetup.iTyreG = 255
							VehicleSetupMP.VehicleSetup.iTyreB = 255
							VehicleSetupMP.VehicleSetup.iNeonR = 255
							VehicleSetupMP.VehicleSetup.iNeonB = 255
							VehicleSetupMP.VehicleSetup.iLivery = 0
							SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
							VehicleSetupMP.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
							VehicleSetupMP.VehicleSetup.iModIndex[MOD_WHEELS] = 3
							SET_VEHICLE_SETUP_MP(tempveh, VehicleSetupMP)
							
						ELIF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel = WINDSOR
							VehicleSetupMP.VehicleSetup.iColour1 = 9
							VehicleSetupMP.VehicleSetup.iColour2 = 5
							VehicleSetupMP.VehicleSetup.iColourExtra1 = 7
							VehicleSetupMP.VehicleSetup.iColourExtra2 = 156
							VehicleSetupMP.iColour5 = 1
							VehicleSetupMP.iColour6 = 132
							VehicleSetupMP.iLivery2 = 0
							VehicleSetupMP.VehicleSetup.iWindowTintColour = 2
							VehicleSetupMP.VehicleSetup.iWheelType = 3
							VehicleSetupMP.VehicleSetup.iTyreR = 255
							VehicleSetupMP.VehicleSetup.iTyreG = 255
							VehicleSetupMP.VehicleSetup.iTyreB = 255
							VehicleSetupMP.VehicleSetup.iNeonR = 255
							VehicleSetupMP.VehicleSetup.iNeonB = 255
							VehicleSetupMP.VehicleSetup.iLivery = 0
							SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
							VehicleSetupMP.VehicleSetup.iModIndex[MOD_WHEELS] = 32
							SET_VEHICLE_SETUP_MP(tempveh, VehicleSetupMP)
							
						ELIF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel = FUGITIVE
							VehicleSetupMP.VehicleSetup.iColour1 = 147
							VehicleSetupMP.VehicleSetup.iColourExtra1 = 4
							VehicleSetupMP.VehicleSetup.iColourExtra2 = 156
							VehicleSetupMP.iColour5 = 1
							VehicleSetupMP.iColour6 = 132
							VehicleSetupMP.iLivery2 = 0
							VehicleSetupMP.VehicleSetup.iWindowTintColour = 1
							VehicleSetupMP.VehicleSetup.iTyreR = 255
							VehicleSetupMP.VehicleSetup.iTyreG = 255
							VehicleSetupMP.VehicleSetup.iTyreB = 255
							VehicleSetupMP.VehicleSetup.iNeonR = 255
							VehicleSetupMP.VehicleSetup.iNeonB = 255
							VehicleSetupMP.VehicleSetup.iLivery = 0
							SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
							SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
							VehicleSetupMP.VehicleSetup.iModIndex[MOD_WHEELS] = 41
							SET_VEHICLE_SETUP_MP(tempveh, VehicleSetupMP)
							
						ELIF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel = SUPERD
							//	
							VehicleSetupMP.VehicleSetup.iColour1 = 62
							VehicleSetupMP.VehicleSetup.iColour2 = 1
							VehicleSetupMP.VehicleSetup.iColourExtra1 = 70
							VehicleSetupMP.VehicleSetup.iColourExtra2 = 156
							VehicleSetupMP.iColour5 = 1
							VehicleSetupMP.iColour6 = 132
							VehicleSetupMP.iLivery2 = 0
							VehicleSetupMP.VehicleSetup.iWindowTintColour = 3
							VehicleSetupMP.VehicleSetup.iTyreR = 255
							VehicleSetupMP.VehicleSetup.iTyreG = 255
							VehicleSetupMP.VehicleSetup.iTyreB = 255
							VehicleSetupMP.VehicleSetup.iNeonR = 255
							VehicleSetupMP.VehicleSetup.iNeonB = 255
							VehicleSetupMP.VehicleSetup.iLivery = 0
							SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
							VehicleSetupMP.VehicleSetup.iModIndex[MOD_WHEELS] = 24
							SET_VEHICLE_SETUP_MP(tempveh, VehicleSetupMP)
							
						ELIF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel = DUBSTA2
							VehicleSetupMP.VehicleSetup.iColour1 = 13
							VehicleSetupMP.VehicleSetup.iColour2 = 12
							VehicleSetupMP.VehicleSetup.iColourExtra2 = 156
							VehicleSetupMP.iColour5 = 1
							VehicleSetupMP.iColour6 = 132
							VehicleSetupMP.iLivery2 = 0
							VehicleSetupMP.VehicleSetup.iWindowTintColour = 1
							VehicleSetupMP.VehicleSetup.iWheelType = 3
							VehicleSetupMP.VehicleSetup.iTyreR = 255
							VehicleSetupMP.VehicleSetup.iTyreG = 255
							VehicleSetupMP.VehicleSetup.iTyreB = 255
							VehicleSetupMP.VehicleSetup.iNeonR = 255
							VehicleSetupMP.VehicleSetup.iNeonB = 255
							VehicleSetupMP.VehicleSetup.iLivery = 0
							SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
							VehicleSetupMP.VehicleSetup.iModIndex[MOD_ROOF] = 2
							VehicleSetupMP.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
							VehicleSetupMP.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
							SET_VEHICLE_SETUP_MP(tempveh, VehicleSetupMP)
							
						ELIF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel = FELTZER2
							VehicleSetupMP.VehicleSetup.iColour1 = 50
							VehicleSetupMP.VehicleSetup.iColour2 = 28
							VehicleSetupMP.VehicleSetup.iColourExtra1 = 92
							VehicleSetupMP.VehicleSetup.iColourExtra2 = 156
							VehicleSetupMP.iColour5 = 1
							VehicleSetupMP.iColour6 = 132
							VehicleSetupMP.iLivery2 = 0
							VehicleSetupMP.VehicleSetup.iWindowTintColour = 3
							VehicleSetupMP.VehicleSetup.iWheelType = 7
							VehicleSetupMP.VehicleSetup.iTyreR = 255
							VehicleSetupMP.VehicleSetup.iTyreG = 255
							VehicleSetupMP.VehicleSetup.iTyreB = 255
							VehicleSetupMP.VehicleSetup.iNeonR = 255
							VehicleSetupMP.VehicleSetup.iNeonB = 255
							VehicleSetupMP.VehicleSetup.iLivery = 0
							SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
							VehicleSetupMP.VehicleSetup.iModIndex[MOD_SPOILER] = 1
							VehicleSetupMP.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
							VehicleSetupMP.VehicleSetup.iModIndex[MOD_WHEELS] = 20
							SET_VEHICLE_SETUP_MP(tempveh, VehicleSetupMP)
							
						ELSE
							SET_VEHICLE_COLOURS(tempveh,12,12)
							SET_VEHICLE_EXTRA_COLOURS(tempveh,12,12)
						ENDIF
						
						IF GET_NUM_MOD_KITS(tempveh) > 0
							SET_VEHICLE_MOD_KIT(tempveh, 0)
							SET_VEHICLE_WINDOW_TINT(tempveh,1)
						ENDIF
						
						SET_GOON_BOSS_VEHICLE(tempveh,TRUE)
						SET_VEHICLE_AS_A_SPECIFIC_RESPAWN_VEHICLE(tempveh)
													
						GB_SET_BOSS_LIMO_LOCKS(tempveh,PLAYER_ID())
						
						//Set decorator
						INT iDecoratorValue
						IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
							IF DECOR_EXIST_ON(tempveh, "MPBitset")	
								iDecoratorValue = DECOR_GET_INT(tempveh, "MPBitset")
							ENDIF
							SET_BIT(iDecoratorValue,MP_DECORATOR_BS_BOSS_LIMO)
							SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
							DECOR_SET_INT(tempveh, "MPBitset", iDecoratorValue)
							CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] SET_BIT(iDecoratorValue,MP_DECORATOR_BS_BOSS_LIMO) MPBitset = ",iDecoratorValue)
						ENDIF
						
						IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
		                    DECOR_SET_INT(tempveh, "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
							CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] Not_Allow_As_Saved_Veh")
		  				ENDIF
															
						CLEAR_BIT(limoData.iSpawnBS, VDP_CreationCoordsFound)
						NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] - GB_CREATE_BOSS_LIMO - VDP_CreationCoordsFound CLEARED - B") NET_NL()
						NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] - GB_CREATE_BOSS_LIMO - NORMAL - return TRUE : pos = ") NET_PRINT_VECTOR(limoData.pos) NET_PRINT(" heading = ") NET_PRINT_FLOAT(limoData.fHeading) NET_NL()
					ENDIF
				ENDIF
			ELSE
			
				MODEL_NAMES previousModel = GET_ENTITY_MODEL(NET_TO_VEH(niVeh))
				IF previousModel != GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel
			
					//Different model, need to spawn again
					BOOL bReadyForCleanup
					IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niVeh))
						bReadyForCleanup = EMPTY_AND_LOCK_VEHICLE(NET_TO_VEH(niVeh), limoData.timeLeaveEvent)
					ELSE
						bReadyForCleanup = TRUE
					ENDIF
					
					IF bReadyForCleanup
						//Cleanup vehicle
						IF DOES_ENTITY_EXIST(NET_TO_VEH(niVeh))
							DECOR_SET_INT(NET_TO_VEH(niVeh), "Previous_Boss",NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
							CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_CREATE_BOSS_LIMO - Set Previous_Boss decorator as player ",NATIVE_TO_INT(PLAYER_ID()))
						ENDIF
						CLEANUP_NET_ID(niVeh)
						
						//Cleanup blip
						REMOVE_BOSS_LIMO_BLIP(limoData.vehBlip)
						
						PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] - GB_CREATE_BOSS_LIMO - Cleaned up previous model ",previousModel," replacing with ",GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel," ")
					ENDIF
					
					RETURN FALSE
				ELSE
			
					//If already exists, move it
					IF TAKE_CONTROL_OF_NET_ID(niVeh)
						IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niVeh))
							IF NOT IS_ENTITY_AT_COORD(NET_TO_VEH(niVeh), limoData.pos, <<10, 10, 10>>)
								SET_ENTITY_HEADING(NET_TO_VEH(niVeh), limoData.fHeading)
								SET_ENTITY_COORDS(NET_TO_VEH(niVeh), limoData.pos)
								NET_PRINT_TIME() NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] - BOSS VEHICLE COORDS SET - AT ") NET_PRINT_VECTOR(limoData.pos) NET_NL()
								
								SET_BOSS_HELI_SPAWN_PROOFS(NET_TO_VEH(niVeh),limoData)
								
								CLEAR_BIT(limoData.iSpawnBS, VDP_CreationCoordsFound)
								NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] - GB_CREATE_BOSS_LIMO - VDP_CreationCoordsFound CLEARED - C") NET_NL()
							ENDIF
						ENDIF
					ELSE
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
				RETURN FALSE
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.VehicleModel)

			g_SpawnData.bIsBossSpawningInAircraft = FALSE

			RETURN TRUE
		ENDIF
			
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC CLEAR_LIMO_DATA(GANG_BOSS_LIMO_LOCAL_DATA &limoData)

	GANG_BOSS_LIMO_LOCAL_DATA emptyStruct
	limoData = emptyStruct
	GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_OPEN_DOORS_BOSS)
	
ENDPROC

FUNC BOOL CLEANUP_BOSS_LIMO(NETWORK_INDEX &niVeh, GANG_BOSS_LIMO_LOCAL_DATA &limoData)

	BOOL bReadyForCleanup = FALSE
	
	VEHICLE_INDEX tempveh

	IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niVeh)
		tempveh = NET_TO_VEH(niVeh)
		IF IS_VEHICLE_DRIVEABLE(tempveh)
			bReadyForCleanup = EMPTY_AND_LOCK_VEHICLE(tempveh, limoData.timeLeaveEvent)
		ELSE
			bReadyForCleanup = TRUE
			CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] CLEANUP_BOSS_LIMO - Vehicle dead, going straight to cleanup.")
		ENDIF
	ELSE
		bReadyForCleanup = TRUE
		CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] CLEANUP_BOSS_LIMO - Vehicle nonexistant, going straight to cleanup.")
	ENDIF

	IF bReadyForCleanup
	
		//Cleanup vehicle
		IF DOES_ENTITY_EXIST(tempveh)
			DECOR_SET_INT(tempveh, "Previous_Boss",NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
			CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] CLEANUP_BOSS_LIMO - Set Previous_Boss decorator as player ",NATIVE_TO_INT(PLAYER_ID()))
		ENDIF
		CLEANUP_NET_ID(niVeh)
		
		//Cleanup blip
		REMOVE_BOSS_LIMO_BLIP(limoData.vehBlip)
	
		//Cleanup data
		CLEAR_LIMO_DATA(limoData)
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iLimoCreationState = GB_LIMO_CREATION_NULL
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.limoDeliverer = INVALID_PLAYER_INDEX()
		MPGlobalsAmbience.sMagnateGangBossData.limoSpawnPlayer = INVALID_PLAYER_INDEX()
		GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_DONE_LIMO_DELIVER_REWARD)

		CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] CLEANUP_BOSS_LIMO")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL MAINTAIN_CHECK_FOR_REQUEST_BOSS_VEHICLE_NETWORK_FADE(PLAYER_INDEX piGangBoss)
	
	BOOL bFadeOutVeh = GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_LOCAL_REQUEST_BOSS_LIMO_FADE_OUT)
	BOOL bReturn
	
	IF NOT bFadeOutVeh
	AND NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_LOCAL_REQUEST_BOSS_LIMO_FADE_IN)
		RETURN bReturn
	ENDIF
	
	
	INT iPlayerBoss = NATIVE_TO_INT(piGangBoss)
	
	IF piGangBoss = INVALID_PLAYER_INDEX()
		PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] MAINTAIN_CHECK_FOR_REQUEST_BOSS_VEHICLE_NETWORK_FADE - gang boss is invalid!")
		RETURN bReturn
	ENDIF
	
	IF GlobalplayerBD_FM_3[iPlayerBoss].sMagnateGangBossData.niBossLimo != NULL
	AND NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD_FM_3[iPlayerBoss].sMagnateGangBossData.niBossLimo)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD_FM_3[iPlayerBoss].sMagnateGangBossData.niBossLimo)
			VEHICLE_INDEX eVeh = NET_TO_VEH(GlobalplayerBD_FM_3[iPlayerBoss].sMagnateGangBossData.niBossLimo)
			bReturn = TRUE
			IF IS_ENTITY_ALIVE(eVeh)
				IF bFadeOutVeh
					NETWORK_FADE_OUT_ENTITY(eVeh, TRUE, TRUE)
				ELSE
					NETWORK_FADE_IN_ENTITY(eVeh, TRUE, TRUE)
				ENDIF
			ELSE
				PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] MAINTAIN_CHECK_FOR_REQUEST_BOSS_VEHICLE_NETWORK_FADE - vehicle is not alive!")
			ENDIF
		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] MAINTAIN_CHECK_FOR_REQUEST_BOSS_VEHICLE_NETWORK_FADE - we don't have control of the network ID!")
		ENDIF
	ELSE
		PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] MAINTAIN_CHECK_FOR_REQUEST_BOSS_VEHICLE_NETWORK_FADE - networked vehicle does not exist!")
	ENDIF
	
	GB_CLEAR_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_LOCAL_REQUEST_BOSS_LIMO_FADE_OUT)
	GB_CLEAR_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_LOCAL_REQUEST_BOSS_LIMO_FADE_IN)
	
	RETURN bReturn
	
ENDFUNC

PROC GB_REQUEST_CREATE_GANG_BOSS_LIMO(MODEL_NAMES vehicleModel = LIMO2, BOOL bForceBoss = FALSE, BOOL bIgnoreDistanceChecks = FALSE)

	PLAYER_INDEX playerID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	IF bForceBoss
		playerID = PLAYER_ID()
	ENDIF

	IF GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].sMagnateGangBossData.iLimoCreationState != GB_LIMO_CREATION_PENDING
		IF bForceBoss 
		OR bIgnoreDistanceChecks
		OR NOT GB_IS_LOCAL_PLAYER_NEAR_BOSS_LIMO()
			IF bForceBoss
			OR GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_REQUEST_CREATE_GANG_BOSS_LIMO - Player is boss.")
				GB_CREATE_VEHICLE_REQUEST(vehicleModel)
			ELIF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
				PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_REQUEST_CREATE_GANG_BOSS_LIMO - Player is goon.")
				PLAYER_INDEX bossID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
				IF IS_NET_PLAYER_OK(bossID,FALSE)
					BROADCAST_REQUEST_BOSS_LIMO(SPECIFIC_PLAYER(bossID),vehicleModel,bIgnoreDistanceChecks)
					PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_REQUEST_CREATE_GANG_BOSS_LIMO - Sending event to boss - ",GET_PLAYER_NAME(bossID))
				ENDIF
			ENDIF
			PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_REQUEST_CREATE_GANG_BOSS_LIMO")
		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_REQUEST_CREATE_GANG_BOSS_LIMO - Failed, player near limo.")
		ENDIF
	ELSE
		PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_REQUEST_CREATE_GANG_BOSS_LIMO - Failed, vehicle being created already.")
	ENDIF

	DEBUG_PRINTCALLSTACK()

ENDPROC

FUNC STRING GB_GET_BOSS_VEHICLE_MODEL_NAME(MODEL_NAMES modelName)

	modelName = modelName
	
	#IF IS_DEBUG_BUILD
	IF modelName != DUMMY_MODEL_FOR_SCRIPT
		RETURN GET_MODEL_NAME_FOR_DEBUG(modelName)
	ENDIF
	#ENDIF
	
	RETURN "DUMMY_MODEL_FOR_SCRIPT"

ENDFUNC

PROC GB_SET_BOSS_VEHICLE_FOR_SESSION_START(MODEL_NAMES vehicleModel)

	IF g_BossVehicleToSpawnOnSessionStart != vehicleModel
		g_BossVehicleToSpawnOnSessionStart = vehicleModel
		PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_SET_BOSS_VEHICLE_FOR_SESSION_START - g_BossVehicleToSpawnOnSessionStart = ",GB_GET_BOSS_VEHICLE_MODEL_NAME(vehicleModel))
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
		IF GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.VehicleModel != vehicleModel
			GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.VehicleModel = vehicleModel
			PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_SET_BOSS_VEHICLE_FOR_SESSION_START - GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.VehicleModel = ",GB_GET_BOSS_VEHICLE_MODEL_NAME(vehicleModel))
		ENDIF
	ENDIF

ENDPROC

PROC GB_CLEAR_BOSS_VEHICLE_FOR_SESSION_START()

	GB_SET_BOSS_VEHICLE_FOR_SESSION_START(DUMMY_MODEL_FOR_SCRIPT)

ENDPROC

PROC TIDYUP_BOSS_LIMO(GANG_BOSS_LIMO_LOCAL_DATA &limoData)

	REMOVE_BOSS_LIMO_BLIP(limoData.vehBlip)
	CLEAR_LIMO_DATA(limoData)
		
ENDPROC

FUNC INT GB_GET_BOSS_VEHICLE_CD_TUNABLE()

	RETURN g_sMPTunables.igb_call_vehicle_cooldown

ENDFUNC

FUNC INT GB_GET_BOSS_LIMO_COOLDOWN()
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_GBSkipCoolDownTimer")
			CPRINTLN(DEBUG_AMBIENT, "GB_GET_BOSS_LIMO_COOLDOWN - sc_GBSkipCoolDownTimer")
			RETURN -1
		ENDIF
	#ENDIF

	IF NOT HAS_NET_TIMER_STARTED(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.limoCooldown)
		PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO]  GB_GET_BOSS_LIMO_COOLDOWN - NOT HAS_NET_TIMER_STARTED")
		RETURN -1
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.limoCooldown,GB_GET_BOSS_VEHICLE_CD_TUNABLE())
		PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO]  GB_GET_BOSS_LIMO_COOLDOWN - HAS_NET_TIMER_EXPIRED")
		RETURN -1
	ENDIF

	PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO]  GB_GET_BOSS_LIMO_COOLDOWN - Time is ",(GB_GET_BOSS_VEHICLE_CD_TUNABLE() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.limoCooldown)))
	RETURN (GB_GET_BOSS_VEHICLE_CD_TUNABLE() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.limoCooldown))

ENDFUNC

PROC MAINTAIN_REMOTE_BOSS_LIMO(GANG_BOSS_LIMO_LOCAL_DATA &limoData)

	CONST_INT ciDistance 2500 //50 * 50

	NETWORK_INDEX bossLimo = GlobalplayerBD_FM_3[limoData.iPlayerStagger].sMagnateGangBossData.niBossLimo
	IF GlobalplayerBD_FM_3[limoData.iPlayerStagger].sMagnateGangBossData.niBossLimo != NULL
	AND NETWORK_DOES_NETWORK_ID_EXIST(bossLimo)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(bossLimo)
		//Store global so vehicle can be referenced by other scripts
		IF MPGlobalsAmbience.sMagnateGangBossData.bossLimo[limoData.iPlayerStagger]  != NET_TO_VEH(bossLimo)
			MPGlobalsAmbience.sMagnateGangBossData.bossLimo[limoData.iPlayerStagger] = NET_TO_VEH(bossLimo)
			CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] MAINTAIN_REMOTE_BOSS_LIMO - Saving vehicle for player ",limoData.iPlayerStagger)
		ENDIF
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(bossLimo)
			//CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] MAINTAIN_REMOTE_BOSS_LIMO - Time: ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(GlobalplayerBD_FM_3[limoData.iPlayerStagger].sMagnateGangBossData.vehInvincibilityTimer))
			IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(bossLimo))
				IF VDIST2(GET_ENTITY_COORDS(NET_TO_VEH(bossLimo)),GlobalplayerBD_FM_3[limoData.iPlayerStagger].sMagnateGangBossData.vLimoCreationPoint) > ciDistance
					SET_ENTITY_PROOFS(NET_TO_VEH(bossLimo),FALSE,FALSE,FALSE,FALSE,FALSE)
				ENDIF
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(GlobalplayerBD_FM_3[limoData.iPlayerStagger].sMagnateGangBossData.vehInvincibilityTimer) >= GB_VEH_INVINCIBLE_TIME
					IF NOT GET_ENTITY_CAN_BE_DAMAGED(NET_TO_ENT(bossLimo))
						SET_ENTITY_INVINCIBLE(NET_TO_VEH(bossLimo),FALSE)
						CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] MAINTAIN_REMOTE_BOSS_LIMO - SET_ENTITY_INVINCIBLE - FALSE - timer up.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF MPGlobalsAmbience.sMagnateGangBossData.bossLimo[limoData.iPlayerStagger] != NULL
		AND DOES_ENTITY_EXIST(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[limoData.iPlayerStagger])
		AND IS_ENTITY_A_VEHICLE(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[limoData.iPlayerStagger])
		AND IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[limoData.iPlayerStagger])
			IF EMPTY_AND_LOCK_VEHICLE(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[limoData.iPlayerStagger], limoData.timeLeaveEvent)
				//SET_VEHICLE_AS_NO_LONGER_NEEDED(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[limoData.iPlayerStagger])
				//DECOR_SET_INT(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[limoData.iPlayerStagger], "Previous_Boss",NETWORK_HASH_FROM_PLAYER_HANDLE(INT_TO_PLAYERINDEX()))
				CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] MAINTAIN_REMOTE_BOSS_LIMO - Set Previous_Boss decorator as player ",limoData.iPlayerStagger)
				MPGlobalsAmbience.sMagnateGangBossData.bossLimo[limoData.iPlayerStagger] = null
				CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] MAINTAIN_REMOTE_BOSS_LIMO for player ",limoData.iPlayerStagger)
			ENDIF
		ENDIF
	ENDIF

	limoData.iPlayerStagger ++
	IF limoData.iPlayerStagger >= NUM_NETWORK_PLAYERS
		limoData.iPlayerStagger = 0
	ENDIF
	
ENDPROC

PROC PROCESS_LIMO_IN_BOSS_PROXIMITY(VEHICLE_INDEX bossLimo)

	IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(),eGB_GLOBAL_CLIENT_BITSET_0_DONE_LIMO_DELIVER_REWARD)
	AND GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.limoDeliverer = INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		
			//If limo in proximity
			IF GB_GET_VEHICLE_DISTANCE_FROM_BOSS(bossLimo,PLAYER_ID()) <= GB_DISTANCE_TO_BOSS
			
				//Check if there is a player in driver seat
				PED_INDEX pedID = GET_PED_IN_VEHICLE_SEAT(bossLimo)
				IF DOES_ENTITY_EXIST(pedID)
					IF NOT IS_PED_INJURED(pedID)
						IF IS_PED_A_PLAYER(pedID)
							PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedID)
							IF IS_NET_PLAYER_OK(playerID)
								GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.limoDeliverer = playerID
								CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] Giving limo deliver bonus to player ",GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//If no driver when in proximity, vehicle spawned inside proximity
				GB_SET_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_DONE_LIMO_DELIVER_REWARD)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_GOON_DELIVERY_BONUS(GANG_BOSS_LIMO_LOCAL_DATA &limoData)

	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
		EXIT
	ENDIF

	IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(GB_GET_LOCAL_PLAYER_GANG_BOSS(),eGB_GLOBAL_CLIENT_BITSET_0_DONE_LIMO_DELIVER_REWARD)
		IF GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.limoDeliverer = PLAYER_ID()
			IF NOT IS_BIT_SET(limoData.iLocalBS,GBL_LOCAL_BS_GOON_DELIVERED_LIMO)
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(), "XPT_MAGNATE", XPTYPE_ACTION, XPCATEGORY_DELIVER_LIMO, g_sMPTunables.igb_rp_deliver_limo, 1)
				SET_BIT(limoData.iLocalBS,GBL_LOCAL_BS_GOON_DELIVERED_LIMO)
				PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] PROCESS_GOON_DELIVERY_BONUS - I delivered boss limo and earned reward.")
			ENDIF
		ELSE
			GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_GOON_DELIVER_LIMO)
		ENDIF
	ENDIF
		
ENDPROC

PROC PROCESS_GOON_OPEN_DOOR_FOR_BOSS()

	


ENDPROC

FUNC BOOL GB_MAINTAIN_BOSS_LIMO_PROCESSING(NETWORK_INDEX bossLimo,GANG_BOSS_LIMO_LOCAL_DATA &limoData)
		
	IF NETWORK_DOES_NETWORK_ID_EXIST(bossLimo)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(bossLimo)
		VEHICLE_INDEX limoVeh = NET_TO_VEH(bossLimo)
		
		//Vehicle alive checks
		IF IS_VEHICLE_DRIVEABLE(limoVeh)
		
			PROCESS_LIMO_IN_BOSS_PROXIMITY(limoVeh)
			
			//Player alive checks
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),limoVeh)
				OR IS_SHIPMENT_PLAYER_IN_VEHICLE(limoVeh)
				OR IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_INSIDE_MISSION_INTERIOR)
					GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_OPEN_DOORS_BOSS)
					REMOVE_BOSS_LIMO_BLIP(limoData.vehBlip)
				ELSE
					ADD_BOSS_LIMO_BLIP(limoData.vehBlip)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), limoVeh) < 6
						IF NOT GB_IS_CURRENT_BOSS_VEHICLE_A_BOAT()
						AND NOT GB_IS_CURRENT_BOSS_VEHICLE_A_BIKE()
						AND GB_GET_MODEL_OF_BOSS_VEHICLE() != DUNE
						AND GB_GET_MODEL_OF_BOSS_VEHICLE() != BRICKADE
						AND GB_GET_MODEL_OF_BOSS_VEHICLE() != HAVOK
							GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_OPEN_DOORS_BOSS)
						ENDIF
						IF GB_IS_CURRENT_BOSS_VEHICLE_A_LIMO()
							GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_ENTER_TURRET)
						ENDIF
					ELSE
						GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_OPEN_DOORS_BOSS)
						GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_ENTER_TURRET)
					ENDIF
				ENDIF
			ENDIF
			
			RETURN TRUE
		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_MAINTAIN_BOSS_LIMO_PROCESSING - Limo is dead.")
			
			IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
				SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_MP_RAY_LAVOY,"GB_TXT_2",TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
			ENDIF
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iLimoCreationState = GB_LIMO_CREATION_DESTROYED
			REINIT_NET_TIMER(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.limoCooldown)
			REMOVE_BOSS_LIMO_BLIP(limoData.vehBlip)
			GB_CLEAR_BOSS_VEHICLE_FOR_SESSION_START()
			RETURN FALSE
		ENDIF
	ELSE
		PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_MAINTAIN_BOSS_LIMO_PROCESSING - Limo does not exist.")
		REMOVE_BOSS_LIMO_BLIP(limoData.vehBlip)
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC GB_MAINTAIN_GOON_LIMO_PROCESSING(NETWORK_INDEX bossLimo,GANG_BOSS_LIMO_LOCAL_DATA &limoData)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(bossLimo)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(bossLimo)
		VEHICLE_INDEX limoVeh = NET_TO_VEH(bossLimo)
		
		//Vehicle alive checks
		IF IS_VEHICLE_DRIVEABLE(limoVeh)
			
			//Player alive checks
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),limoVeh)
				OR IS_SHIPMENT_PLAYER_IN_VEHICLE(limoVeh)
				OR IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_INSIDE_MISSION_INTERIOR)
					REMOVE_BOSS_LIMO_BLIP(limoData.vehBlip)
				ELSE
					ADD_BOSS_LIMO_BLIP(limoData.vehBlip)
					IF GB_IS_CURRENT_BOSS_VEHICLE_A_LIMO()
					AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), limoVeh) < 6
						IF GB_IS_CURRENT_BOSS_VEHICLE_A_LIMO()
							GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_ENTER_TURRET)
						ENDIF
					ELSE
						GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_ENTER_TURRET)
					ENDIF
				ENDIF
				PROCESS_GOON_DELIVERY_BONUS(limoData)
			ENDIF
			
		ELSE
			PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_MAINTAIN_GOON_LIMO_PROCESSING - Limo is dead.")
			REMOVE_BOSS_LIMO_BLIP(limoData.vehBlip)
		ENDIF
	ELSE
		PRINTLN("[MAGNATE_GANG_BOSS][BOSS_LIMO] GB_MAINTAIN_GOON_LIMO_PROCESSING - Limo does not exist.")
		REMOVE_BOSS_LIMO_BLIP(limoData.vehBlip)
	ENDIF
	
ENDPROC

PROC MAINTAIN_NO_FLYING_INTO_SUNSET()

	IF GB_IS_PLAYER_IN_ANY_GANG_BOSS_LIMO(PLAYER_ID())
		IF DO_LEFT_PLAYERS_FLY_HELIS_INTO_SUNSET()
			SET_LEFT_PLAYERS_FLY_HELIS_INTO_SUNSET(FALSE)
		ENDIF
	ELSE
		IF NOT DO_LEFT_PLAYERS_FLY_HELIS_INTO_SUNSET()
			IF FM_EVENT_GET_SESSION_ACTIVE_FM_EVENT() != FMMC_TYPE_KILL_LIST
				SET_LEFT_PLAYERS_FLY_HELIS_INTO_SUNSET(TRUE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC GBL_GOON_PROCESSING(GANG_BOSS_LIMO_LOCAL_DATA &limoData)
	
	INT iBoss = NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	IF GlobalplayerBD_FM_3[iBoss].sMagnateGangBossData.limoStage = GBL_MAINTAIN
		GB_MAINTAIN_GOON_LIMO_PROCESSING(GlobalplayerBD_FM_3[iBoss].sMagnateGangBossData.niBossLimo,limoData)
	ELSE
		TIDYUP_BOSS_LIMO(limoData)
	ENDIF
	
ENDPROC

FUNC BOOL BOSS_INIT_LIMO(GANG_BOSS_LIMO_LOCAL_DATA &limoData)

	TIDYUP_BOSS_LIMO(limoData)
	GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.limoDeliverer = INVALID_PLAYER_INDEX()
	GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_DONE_LIMO_DELIVER_REWARD)
	CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] BOSS_INIT_LIMO")
	RETURN TRUE

ENDFUNC

PROC GBL_BOSS_PROCESSING(GANG_BOSS_LIMO_LOCAL_DATA &limoData, INT iBoss)
	
	SWITCH(GlobalplayerBD_FM_3[iBoss].sMagnateGangBossData.limoStage)
	
		CASE GBL_IDLE
			//Do nothing
			IF GB_GET_GANG_BOSS_LIMO_CREATION_STATE_INT(iBoss) = GB_LIMO_CREATION_PENDING
				IF BOSS_INIT_LIMO(limoData)
					SET_BOSS_GBL_STATE(GBL_SPAWN)
				ENDIF
			ENDIF
		BREAK
		
		CASE GBL_SPAWN
			//Spawn the vehicle
			IF MPGlobals.sFreemodeCache.bLocalPlayerOk 
				GB_SET_LIMO_SPAWN_POINT(limoData)
				IF GB_CREATE_BOSS_LIMO(GlobalplayerBD_FM_3[iBoss].sMagnateGangBossData.niBossLimo,limoData)
				
					//Set global to spawn this vehicle on session entry
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						GB_SET_BOSS_VEHICLE_FOR_SESSION_START(GlobalplayerBD_FM_3[iBoss].sMagnateGangBossData.VehicleModel)
					ENDIF
				
					//Fade in the vehicle - No.
					//NETWORK_FADE_IN_ENTITY(NET_TO_ENT(GlobalplayerBD_FM_3[iBoss].sMagnateGangBossData.niBossLimo),TRUE)
				
					//Do help the first time
					GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_LIMO)
							
					//Reset player to spawn limo at
					MPGlobalsAmbience.sMagnateGangBossData.limoSpawnPlayer = INVALID_PLAYER_INDEX()
					CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] MPGlobalsAmbience.sMagnateGangBossData.limoSpawnPlayer = INVALID_PLAYER_INDEX()")
				
					//Cleanup any old boss vehicles
					BROADCAST_DELETE_OLD_BV(ALL_PLAYERS())
									
					//Success (used for feedback)
					GlobalplayerBD_FM_3[iBoss].sMagnateGangBossData.iLimoCreationState = GB_LIMO_CREATION_SUCCESS
					
					//Move to maintain limo
					SET_BOSS_GBL_STATE(GBL_MAINTAIN)
//				ELSE
//					IF HAS_NET_TIMER_EXPIRED(limoData.spawnTimer,GB_LIMO_SPAWN_TIMEOUT) //timeout
//						//go to cleanup and return fail
//						GlobalplayerBD_FM_3[iBoss].sMagnateGangBossData.iLimoCreationState = GB_LIMO_CREATION_FAIL
//						MPGlobalsAmbience.sMagnateGangBossData.limoSpawnPlayer = INVALID_PLAYER_INDEX()
//						RESET_NET_TIMER(limoData.spawnTimer)
//						CLEAR_LIMO_DATA(limoData)
//						CPRINTLN(DEBUG_NET_AMBIENT, "[MAGNATE_GANG_BOSS][BOSS_LIMO] Spawn timed out.")
//						SET_BOSS_GBL_STATE(GBL_IDLE)
//					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE GBL_MAINTAIN
			//Maintain blips/dead checks
			IF NOT GB_MAINTAIN_BOSS_LIMO_PROCESSING(GlobalplayerBD_FM_3[iBoss].sMagnateGangBossData.niBossLimo,limoData)
			OR IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
				SET_BOSS_GBL_STATE(GBL_CLEANUP)
			ENDIF
		BREAK
		
		CASE GBL_CLEANUP
			//Cleanup vehicle and data
			IF CLEANUP_BOSS_LIMO(GlobalplayerBD_FM_3[iBoss].sMagnateGangBossData.niBossLimo,limoData)
				SET_BOSS_GBL_STATE(GBL_IDLE)
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC


PROC GB_MAINTAIN_BOSS_LIMO(GANG_BOSS_LIMO_LOCAL_DATA &limoData)

	MAINTAIN_REMOTE_BOSS_LIMO(limoData)
	
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		GBL_BOSS_PROCESSING(limoData, iPlayer)
		IF NOT GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_NEED_TO_CLEAN_LIMO)
			GB_SET_GLOBAL_NON_BD_BIT1(eGB_GLOBAL_NON_BD_BITSET_1_NEED_TO_CLEAN_LIMO)
		ENDIF
		
	ELIF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		GBL_GOON_PROCESSING(limoData)
		IF NOT GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_NEED_TO_CLEAN_LIMO)
			GB_SET_GLOBAL_NON_BD_BIT1(eGB_GLOBAL_NON_BD_BITSET_1_NEED_TO_CLEAN_LIMO)
		ENDIF
		
	ELIF GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_NEED_TO_CLEAN_LIMO)
	
		//If we were a member of the gang, but are no longer, tidyup some data
		TIDYUP_BOSS_LIMO(limoData)
		
		//Cleanup session restart vehicle
		GB_CLEAR_BOSS_VEHICLE_FOR_SESSION_START()
			
		//If was boss with vehicle, clean it up
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.niBossLimo)
			CLEANUP_BOSS_LIMO(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.niBossLimo,limoData)
			SET_BOSS_GBL_STATE(GBL_IDLE)
			GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.niBossLimo = NULL
		ENDIF
		
		GB_CLEAR_GLOBAL_NON_BD_BIT1(eGB_GLOBAL_NON_BD_BITSET_1_NEED_TO_CLEAN_LIMO)
		
	ENDIF
	
ENDPROC





































