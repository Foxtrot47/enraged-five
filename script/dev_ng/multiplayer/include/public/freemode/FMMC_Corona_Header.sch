
USING "rage_builtins.sch"
USING "globals.sch"
USING "gunclub_shop_private.sch"
USING "commands_datafile.sch"
USING "net_mission.sch"

USING "MP_Scaleform_Functions_XML.sch"

USING "net_heists_hardcoded.sch"
USING "net_heists_common.sch"

USING "net_corona_v2.sch"

// Corona column index
CONST_INT CORONA_COLUMN_ONE 	0
CONST_INT CORONA_COLUMN_TWO 	1
CONST_INT CORONA_COLUMN_THREE 	2
CONST_INT CORONA_COLUMN_FOUR	3
CONST_INT CORONA_COLUMN_FIVE	4
CONST_INT CORONA_COLUMN_SIX		5
CONST_INT CORONA_COLUMN_SEVEN	6
CONST_INT CORONA_COLUMN_EIGHT	7
CONST_INT CORONA_COLUMN_NINE	8

// Toggles for destroy last player in Races
CONST_INT ciCORONA_DESTROY_VEHICLE_OFF				0
CONST_INT ciCORONA_DESTROY_VEHICLE_LAP				1
CONST_INT ciCORONA_DESTROY_VEHICLE_15				2
CONST_INT ciCORONA_DESTROY_VEHICLE_30				3
CONST_INT ciCORONA_DESTROY_VEHICLE_60				4
CONST_INT ciCORONA_DESTROY_VEHICLE_MAX_OPTIONS		4

// Constant for number of dummy corona locations we have
CONST_INT MAX_TEMP_CORONA_LOCATIONS	20

/// PURPOSE: Returns TRUE if the corona is for setting / taking on a challenge
FUNC BOOL IS_CORONA_IN_CHALLENGE_MODE()
	IF IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
	//OR IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if the corona is in a head 2 head mode
FUNC BOOL IS_CORONA_IN_HEAD_2_HEAD_MODE()
	IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
	OR IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Can the SCTV player launch the corna
FUNC BOOL CAN_SCTV_LAUNCH_THIS_TRANSITION_SESSION()

	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("tournament_ignore_sctv")
			RETURN FALSE
		ENDIF
	#ENDIF


	//If we are in a tournament playlist
	IF IS_SCTV_IN_CONTROL_OF_TRANSITION_SESSION()
	//And we've not set it as ready to launch
	AND NOT IS_TRANSITION_SESSION_TOURNAMENT_READY_TO_LAUNCH()
	//AND NOT HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//Bool to see if a tournament should be launchable
FUNC BOOL SHOULD_THIS_TOURNAMENT_TRANSITON_SESSION_LAUNCH()
	//If we are in a tournament playlist
	IF IS_SCTV_IN_CONTROL_OF_TRANSITION_SESSION()
	//If we've been told it's ready
	AND IS_TRANSITION_SESSION_TOURNAMENT_READY_TO_LAUNCH()
	//And we've not done the initial transition
	//AND NOT HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC PROCESS_PLAYLIST_DATA_FOR_INITIAL_LAUNCH()
	
	IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
		g_sCurrentPlayListDetails.iInitialCashWager = g_sCurrentPlayListDetails.iCashBet
		g_sCurrentPlayListDetails.iCashBet = (g_sCurrentPlayListDetails.iCashBet * GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION())
	
		PRINTLN("[CORONA] PROCESS_PLAYLIST_DATA_FOR_INITIAL_LAUNCH - Participant in H2H is charged: ", g_sCurrentPlayListDetails.iInitialCashWager)
		PRINTLN("[CORONA] PROCESS_PLAYLIST_DATA_FOR_INITIAL_LAUNCH - Number of players in H2H: ", GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION())
		PRINTLN("[CORONA] PROCESS_PLAYLIST_DATA_FOR_INITIAL_LAUNCH - Total pot for H2H ", g_sCurrentPlayListDetails.iCashBet)
	ENDIF
ENDPROC

/// PURPOSE: Make players within the challenge / playlist pay to take part
FUNC BOOL PROCESS_CREW_CHALLENGE_AND_H2H_PAYMENT(INT &iCashTransactionIndex, BOOL &bCashTransactionActive)

	IF IS_CORONA_IN_CHALLENGE_MODE()
		IF IS_CORONA_BIT_SET(CORONA_CHALLENGE_TAKE_CASH_THIS_JOB)

			g_sCurrentPlayListDetails.iInitialCashWager = CEIL(TO_FLOAT(g_sCurrentPlayListDetails.iCashBet) / TO_FLOAT(GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION()))

			PRINTLN("[CORONA] PROCESS_CREW_CHALLENGE_AND_H2H_PAYMENT - Participant in Challenge is charged: ", g_sCurrentPlayListDetails.iInitialCashWager)
			PRINTLN("[CORONA] PROCESS_CREW_CHALLENGE_AND_H2H_PAYMENT - Number of players in Challenge: ", GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION())
			PRINTLN("[CORONA] PROCESS_CREW_CHALLENGE_AND_H2H_PAYMENT - Total pot for Challenge: ", g_sCurrentPlayListDetails.iCashBet)
			
			CLEAR_CORONA_BIT(CORONA_CHALLENGE_TAKE_CASH_THIS_JOB)
			
			// Request a cash transaction and wait for the reply
			IF USE_SERVER_TRANSACTIONS()
				IF NETWORK_REQUEST_CASH_TRANSACTION(iCashTransactionIndex, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_SPEND, CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_SPEND_CHALLENGE_WAGER, g_sCurrentPlayListDetails.iInitialCashWager, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
					
					PRINTLN("[CORONA] PROCESS_CREW_CHALLENGE_AND_H2H_PAYMENT - Successfully requested a cash transaction: ", iCashTransactionIndex)
					bCashTransactionActive = TRUE
				ELSE
					PRINTLN("[CORONA] PROCESS_CREW_CHALLENGE_AND_H2H_PAYMENT - Failed to requested a cash transaction")
					bCashTransactionActive = FALSE
				ENDIF
				
				RETURN TRUE
			ELSE
				NETWORK_SPENT_BETTING(g_sCurrentPlayListDetails.iInitialCashWager, 0, g_tl31uniqueMatchId, FALSE, TRUE)
			ENDIF
			
			
			
			RETURN FALSE
		ENDIF
	ELIF IS_CORONA_IN_HEAD_2_HEAD_MODE()
		IF IS_CORONA_BIT_SET(CORONA_H2H_CHARGE_PLAYER_FOR_CASH)
			
			// Adjust our bet value now for end of playlist where pot is divided up.
			
			PRINTLN("[CORONA] PROCESS_CREW_CHALLENGE_AND_H2H_PAYMENT - Participant in H2H is charged: ", g_sCurrentPlayListDetails.iInitialCashWager)
			PRINTLN("[CORONA] PROCESS_CREW_CHALLENGE_AND_H2H_PAYMENT - Number of players in H2H: ", GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION())
			PRINTLN("[CORONA] PROCESS_CREW_CHALLENGE_AND_H2H_PAYMENT - Total pot for H2H: ", (g_sCurrentPlayListDetails.iInitialCashWager * GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION()))

			CLEAR_CORONA_BIT(CORONA_H2H_CHARGE_PLAYER_FOR_CASH)
			
			// Request a cash transaction and wait for the reply
			IF USE_SERVER_TRANSACTIONS()
				IF NETWORK_REQUEST_CASH_TRANSACTION(iCashTransactionIndex, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_SPEND, CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_SPEND_CHALLENGE_WAGER, g_sCurrentPlayListDetails.iInitialCashWager, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
					
					PRINTLN("[CORONA] PROCESS_CREW_CHALLENGE_AND_H2H_PAYMENT - H2H - Successfully requested a cash transaction: ", iCashTransactionIndex)
					bCashTransactionActive = TRUE
				ELSE
					PRINTLN("[CORONA] PROCESS_CREW_CHALLENGE_AND_H2H_PAYMENT - H2H - Failed to requested a cash transaction")
					bCashTransactionActive = FALSE
				ENDIF
				
				RETURN TRUE
			ELSE
				SET_TRANSITION_SESSIONS_PLAYER_PAID_H2H_WAGER()
				NETWORK_SPENT_BETTING(g_sCurrentPlayListDetails.iInitialCashWager, 0, g_tl31uniqueMatchId, FALSE, TRUE)
			ENDIF
			
			RETURN FALSE
		ENDIF
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Quick toggle of all instructional buttons in corona
PROC DISPLAY_CORONA_INSTRUCTIONAL_BUTTONS(BOOL bShow)

	IF bShow
		PRINTLN("[CORONA] DISPLAY_CORONA_INSTRUCTIONAL_BUTTONS - Display our instructional buttons")
		PAUSE_MENU_ACTIVATE_CONTEXT(HASH("DISPLAY_CORONA_BUTTONS"))
	ELSE
		PRINTLN("[CORONA] DISPLAY_CORONA_INSTRUCTIONAL_BUTTONS - Hide our instructional buttons")
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("DISPLAY_CORONA_BUTTONS"))
	ENDIF
	
	PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
ENDPROC

PROC SET_UP_CAMERA_FOCUS_POS_AND_VEL(VECTOR vCameraPos)

	IF NOT SHOULD_CORONA_PLAYER_CLEAR_CORONA_CAMERAS()
		PRINTLN("[CORONA] SET_UP_CAMERA_FOCUS_POS_AND_VEL SCTV player not clearing focus")
		EXIT
	ENDIF	

	PRINTLN("[CORONA] SET_UP_CAMERA_FOCUS_POS_AND_VEL called with vCameraPos = ", vCameraPos)
	SET_FOCUS_POS_AND_VEL(vCameraPos, <<0.0, 0.0, 0.0>>)
	PRINTLN("SET_FOCUS_POS_AND_VEL(", vCameraPos, ", <<0.0, 0.0, 0.0>>)")
	NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
	PRINTLN("[CORONA] NETWORK_SET_IN_FREE_CAM_MODE(TRUE)")
ENDPROC


PROC FREEZE_PLAYER_PED(BOOL bIsRaceTutorialCorona = FALSE, BOOL bSMV = FALSE)
	#IF IS_DEBUG_BUILD
		IF bIsRaceTutorialCorona
			PRINTLN("[dsw] [FREEZE_PLAYER_PED] called during tutorial!")
		ENDIF
	#ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		PRINTLN("[FREEZE_PLAYER_PED] - SCTV - EXIT THIS COMMAND")
		EXIT
	ENDIF
	
	IF NOT bSMV
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE))
			IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				DETACH_ENTITY(PLAYER_PED_ID())
				PRINTLN("[dsw] [FREEZE_PLAYER_PED] detaching entity.")
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT bIsRaceTutorialCorona
		IF NETWORK_IS_GAME_IN_PROGRESS()
			// if in someone elses pv then clear tasks immediately see 1705850
			IF AM_I_IN_SOMEONE_ELSES_PERSONAL_VEHICLE()
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				PRINTLN("[dsw] [FREEZE_PLAYER_PED] in someone elses personal vehicle.")
			ENDIF
			
			IF IS_PLAYER_LAUNCHING_FIXER_SHORT_TRIP_IN_SMOKING_ROOM(PLAYER_ID())
			
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION)
				PRINTLN("[CORONA][AMEC] FREEZE_PLAYER_PED - player control switched off but left VISIBLE (Short trip).")	
			
			ELSE
			
				IF GET_HEIST_CORONA_FROM_PLANNING_BOARD()
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_FREEZE_POSITION)
					PRINTLN("[CORONA][AMEC] FREEZE_PLAYER_PED - player control switched off but left VISIBLE.")	
				ELSE
				
				IF NOT bSMV
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_USE_PLAYER_FADE_OUT | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION | NSPC_SET_INVISIBLE)
				ENDIF
				PRINTLN("[dsw] [FREEZE_PLAYER_PED] player control switched off.")
				
			ENDIF
			
			ENDIF
			
		ELSE
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
			PRINTLN("[dsw] [FREEZE_PLAYER_PED] network game not in progress.")
		ENDIF
	ENDIF
	

//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
//		CLEAR_PED_TASKS(PLAYER_PED_ID())
//		IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE))
//			DETACH_ENTITY(PLAYER_PED_ID())
//		ENDIF
//		IF NOT bIsRaceTutorialCorona
//			IF NETWORK_IS_GAME_IN_PROGRESS()
//				FADE_OUT_LOCAL_PLAYER(TRUE) // NEIL F 27/8/13 - swapped to a fade out player to make it a bit less jarring (see 1395492) - perhaps need to do it for contact missions only.
//			ELSE
//				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
//			ENDIF
//			IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE))
//				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
//				SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
//			ENDIF
//		ENDIF
//		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
//	ENDIF

ENDPROC

/// PURPOSE: Sets up the player's wanted level for the corona
PROC SET_CORONA_WANTED_LEVEL_SETTINGS()
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			
			PRINTLN("[CORONA] SET_CORONA_WANTED_LEVEL_SETTINGS - clearing players wanted level and multiplier")
			PRINTLN("[CORONA] 				 - CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())")
			PRINTLN("[CORONA] 				 - SET_WANTED_LEVEL_MULTIPLIER(0.0)")

			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ENDIF
	ENDIF
		
	SET_WANTED_LEVEL_MULTIPLIER(0.0)

ENDPROC

/// PURPOSE: Cleans up the player's wanted level settings for the corona
PROC CLEAR_CORONA_WANTED_LEVEL_SETTINGS()
	
	PRINTLN("[CORONA] CLEAR_CORONA_WANTED_LEVEL_SETTINGS - clearing players wanted level and multiplier")
	PRINTLN("[CORONA] 				 - SET_WANTED_LEVEL_MULTIPLIER(1.0)")

	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
ENDPROC

/// PURPOSE:
///    This will shut down the pause menu if its active or restarting.
PROC CLEAR_PAUSE_MENU_FOR_CORONA_IF_ACTIVE()
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_PAUSE_MENU_RESTARTING()

		DISPLAY_RADAR(FALSE)
		PRINTLN("[CORONA] CLEAR_PAUSE_MENU_FOR_CORONA_IF_ACTIVE - DISPLAY_RADAR(FALSE) - N")
	
		#IF IS_DEBUG_BUILD			
			PRINTLN("[CORONA] CLEAR_PAUSE_MENU_FOR_CORONA_IF_ACTIVE - SET_FRONTEND_ACTIVE(FALSE): ")
		
			IF IS_PAUSE_MENU_ACTIVE()
				PRINTLN("[CORONA] CLEAR_PAUSE_MENU_FOR_CORONA_IF_ACTIVE - Pause menu active")
			ENDIF
			
			IF IS_PAUSE_MENU_RESTARTING()
				PRINTLN("[CORONA] CLEAR_PAUSE_MENU_FOR_CORONA_IF_ACTIVE	- Pause menu restarting")
			ENDIF
		#ENDIF
		
		SET_FRONTEND_ACTIVE(FALSE)
	ENDIF
ENDPROC

/// PURPOSE: Wrapper for the blur we apply in the corona
PROC TRIGGER_CORONA_CAMERA_BLUR(
								 BOOL bIsHeist = FALSE
								  )

	IF NOT ANIMPOSTFX_IS_RUNNING(GET_CORONA_FX_IN())
		#IF IS_DEBUG_BUILD
		// Debug functionality to disable the blur of the screen
		IF NOT g_bDisableCoronaIntroAndBlur
		#ENDIF
		
		PRINTLN("TRIGGER_CORONA_CAMERA_BLUR - ANIMPOSTFX_PLAY(", GET_CORONA_FX_IN(), " 0, TRUE)")
		DEBUG_PRINTCALLSTACK()
		
		ANIMPOSTFX_PLAY(GET_CORONA_FX_IN(  bIsHeist  ), 0, TRUE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

//Hide all the players for the corona. 
PROC HIDE_ALL_PLAYERS_FOR_CORONA()

	IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		EXIT
	ENDIF

	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		EXIT
	ENDIF
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(i))
			IF INT_TO_PLAYERINDEX(i) = PLAYER_ID()
				IF NOT IS_CORONA_BIT_SET(CORONA_MAKE_LOCAL_PLAYER_VISIBLE)
					SET_PLAYER_INVISIBLE_LOCALLY(INT_TO_PLAYERINDEX(i))
				ENDIF
			ELSE		
				SET_PLAYER_INVISIBLE_LOCALLY(INT_TO_PLAYERINDEX(i))
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

CONST_INT ciCORONA_CAMERA_SETUP_BS_SVM							0
CONST_INT ciCORONA_CAMERA_SETUP_BS_GANGOPS						1
CONST_INT ciCORONA_CAMERA_SETUP_BS_ARENA						2
CONST_INT ciCORONA_CAMERA_SETUP_BS_CASINO						3
CONST_INT ciCORONA_CAMERA_SETUP_BS_CASINO_HEIST					4
CONST_INT ciCORONA_CAMERA_SETUP_BS_YACHT_MISSION				5
CONST_INT ciCORONA_CAMERA_SETUP_BS_GENERIC_HEIST_PLANNING		6
CONST_INT ciCORONA_CAMERA_SETUP_BS_FIXER_GOLF_COURSE			7
CONST_INT ciCORONA_CAMERA_SETUP_BS_FIXER_STORY					8
CONST_INT ciCORONA_CAMERA_SETUP_BS_FIXER_SHORT_TRIP_INTRO		9
CONST_INT ciCORONA_CAMERA_SETUP_BS_FIXER_SHORT_TRIP				10
CONST_INT ciCORONA_CAMERA_SETUP_BS_XMAS22_STORY_INTRO			11
CONST_INT ciCORONA_CAMERA_SETUP_BS_XMAS22_STORY					12

/// PURPOSE: Sets up the corona camera (moved from fm_maintain_transition_players.sc)
PROC SETUP_CORONA_CAMERA(CAMERA_INDEX &ciCamera, INT iMissionType, INT iMissionVariation, VECTOR vStartPoint, VECTOR &vCamPos, VECTOR vCamHead, BOOL bContactMission, BOOL bHeistMission,
						 VECTOR &vFocusPos, BOOL &bSetFocus, BOOL bClearBlur = TRUE, BOOL bTutorialSwitch = FALSE, INT iSetupBS = 0 ) 
	
	PRINTLN("[CORONA] SETUP_CORONA_CAMERA - called by script [", GET_THIS_SCRIPT_NAME(),"]")
	PRINTLN("SPIN SET UP TIME - SETUP_CORONA_CAMERA - GET_GAME_TIMER = ", GET_GAME_TIMER(), " GET_FRAME_COUNT = ", GET_FRAME_COUNT())
	DEBUG_PRINTCALLSTACK()
	
	// ****** Initialise all the camera now so we don't see player become invisible ******
	CLEAN_UP_INTRO_STAGE_CAM(ciCamera, iMissionType, TRUE, bClearBlur)
	
	// If we are SCTV and on a heist then we should not set up a camera since we will be in an interior
	IF IS_PLAYER_SCTV(PLAYER_ID())	
		IF bHeistMission
		
			// Only if we can we should apply the special effects
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
				OR bTutorialSwitch
					TRIGGER_CORONA_CAMERA_BLUR()
				ENDIF
			ENDIF
		
			PRINTLN("[CORONA] SETUP_CORONA_CAMERA - Player is SCTV and on a heist. DO NOT SETUP CORONA CAMERA")
			EXIT
		ENDIF
	ENDIF
	
	//...create camera if it doesn't already exist
	IF NOT DOES_CAM_EXIST(ciCamera)
		ciCamera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
	ENDIF
	VECTOR vCameraPos
	VECTOR vCameraRot = vCamHead
	FLOAT fFov = -1
	BOOL bShake = FALSE
	FLOAT fShake = 0.2
	BOOL bDoCameraBlur = TRUE
	IF IS_THIS_A_MINI_GAME(iMissionType)
		vCamPos 		= GET_FMMC_MINI_GAME_CAMERA_LOCATION(iMissionType, iMissionVariation)
		vCameraRot 		= GET_FMMC_MINI_GAME_CAMERA_ROTATION(iMissionType, iMissionVariation)
		fFov	 		= GET_FMMC_MINI_GAME_CAMERA_FOC(iMissionType, iMissionVariation)
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vCamPos)
		vCameraPos = vCamPos
		PRINTLN("[CORONA] vCameraPos = fmmcCoronaMissionData.vCamPos")
	ENDIF
	
	// Catch the case where the camera position has not been set up
	IF IS_VECTOR_ZERO(vCameraPos)
		// KGM 9/6/13: Contact Missions cameraPos will be set to ZERO, but it should fall through here and use the player coords routine below this
		IF NOT (bContactMission)
			vCameraPos = vStartPoint
			PRINTLN("[CORONA] vCameraPos = fmmcCoronaMissionData.vStartPoint")
			IF IS_COLLISION_MARKED_OUTSIDE(<<vCameraPos.x, vCameraPos.y, vCameraPos.z +1.0>>)
			ELSE
				vCameraPos.z += 4.0
			ENDIF
		ENDIF
	ENDIF
	
	// Grab the player coords if we have a zero camera position
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_VECTOR_ZERO(vCameraPos)
			
			// Grab correct coords
			IF IS_PLAYER_SCTV(PLAYER_ID())
				PRINTLN("[CORONA] vCameraPos = GET_ENTITY_COORDS(GET_SPECTATOR_SELECTED_PED())")
				IF DOES_ENTITY_EXIST(GET_SPECTATOR_SELECTED_PED())
				AND NOT IS_ENTITY_DEAD(GET_SPECTATOR_SELECTED_PED())
					vCameraPos = GET_ENTITY_COORDS(GET_SPECTATOR_SELECTED_PED())
					vCameraRot = << 0.0, 0.0, GET_ENTITY_HEADING(GET_SPECTATOR_SELECTED_PED()) >>
					
					// If we are in a property we should use the camera we are currently in.
					IF IS_PLAYER_IN_PROPERTY(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget, TRUE)
						vCameraPos = GET_FINAL_RENDERED_CAM_COORD()
						vCameraRot = GET_FINAL_RENDERED_CAM_ROT()
						
						PRINTLN("[CORONA] Target is in a property. Use current camera: ", vCameraPos, ", Rot: ", vCameraRot)
					ENDIF
				ELSE
					PRINTLN("[CORONA] vCameraPos = GET_ENTITY_COORDS(PLAYER_PED_ID()) - failed to get SCTV ped")
					vCameraPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					vCameraRot = << 0.0, 0.0, GET_ENTITY_HEADING(PLAYER_PED_ID()) >>
				ENDIF
			ELSE
				PRINTLN("[CORONA] vCameraPos = GET_ENTITY_COORDS(PLAYER_PED_ID())")
				vCameraPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				vCameraRot = << 0.0, 0.0, GET_ENTITY_HEADING(PLAYER_PED_ID()) >>
			ENDIF
			
// KEITH 27/7/13: Changed checking method - constant test output shows that the only accurate method seems to be the GET_INTERIOR_FROM_ENTITY() method
//				  For Info: IS_COLLISION_MARKED_OUTSIDE constantly returned TRUE even when in a shop/apartment
//							GET_INTERIOR_AT_COORDS() using the player coords started returning TRUE while still outside the interior
//							GET_INTERIOR_FROM_ENTITY() seemed to be the only method that returned TRUE when the player walked through the door
//			IF IS_COLLISION_MARKED_OUTSIDE(<<vCameraPos.x, vCameraPos.y, vCameraPos.z +1.0>>)
			IF (IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
			OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID()))
			AND SVM_FLOW_TRANSITION_SESSIONS_LAUNCHING_SMV_FROM_LAPTOP()
				PRINTLN("[CORONA] vCameraPos = GET_TRANSITION_SESSIONS_QUICK_MATCH_TYPE() = FMMC_TYPE_GUNRUNNING_WEAPONIZED_VEHICLE - vCameraPos.z += 1.5")
				vCameraPos.z += 1.5
			ELIF (IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())))
				PRINTLN("[CORONA] vCameraPos = In Interior so small z addition")
				IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
					vCameraPos.z += 0.4
				ELSE
					vCameraPos.z += 1.5
				ENDIF
			ELSE
				PRINTLN("[CORONA] vCameraPos = Not In Interior so larger z addition")
				vCameraPos.z += 4.0
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_CASINO)
		PRINTLN("[CORONA] ciCORONA_CAMERA_SETUP_BS_CASINO")
		vCameraPos = <<1124.7449, 263.6554, -48.8552>>
		vCameraRot = <<-22.4617, 0.0000, -75.8431>>
		fFov = 50.0000
	ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
	OR IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_ARENA)
		IF IS_PLAYER_USING_ARENA_CAREER_WALL(PLAYER_ID())
			PRINTLN("[CORONA] CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash) - In Garage")
			vCameraPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			vCameraRot = << 0.0, 0.0, GET_ENTITY_HEADING(PLAYER_PED_ID()) >>
			vCameraPos.z += 0.4
			fFov = GET_FINAL_RENDERED_CAM_FOV()
		ELSE 
			PRINTLN("[CORONA] CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash) - Not in garage")
			vCameraPos = <<-194.2056, -1853.7499, 70.3345>>
			vCameraRot = <<-10.0011, -0.0000, 131.0271>>
			fFov = 42.6052
		ENDIF	
	ELIF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
	OR IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_GANGOPS)
		PRINTLN("[CORONA] GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)")
		vCameraPos = <<347.2638, 4867.3242, -59.2679>>
		vCameraRot = <<1.2933, 0.0747, -34.6934>>
		fFov = 50.0000
		
	#IF FEATURE_CASINO_HEIST
	ELIF IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_CASINO_HEIST)
	OR IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[CORONA] ciCORONA_CAMERA_SETUP_BS_CASINO_HEIST OR IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)")
		vCameraPos = <<2709.3323, -366.5988, -52.2786>>
		vCameraRot = <<-19.9620, -0.0000, -121.3927>>
		fFov = 56.6574
	
	#ENDIF
		
	ELIF IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_YACHT_MISSION)
//	OR IS_THIS_ROOT_CONTENT_ID_A_YACHT_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[CORONA] ciCORONA_CAMERA_SETUP_BS_YACHT_MISSION OR IS_THIS_ROOT_CONTENT_ID_A_YACHT_MISSION(g_FMMC_STRUCT.iRootContentIDHash)")
		INT iYacht = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID())
		vCameraPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_COORDS_OF_PRIVATE_YACHT(iYacht), GET_HEADING_OF_PRIVATE_YACHT(iYacht), <<3.5688, 7.9792, 4.1641>>)
		vCameraRot = <<-21.6791, 0.0, GET_HEADING_OF_PRIVATE_YACHT(iYacht)+40.237>>
		fFov = 70.0000
		
	ELIF IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_GENERIC_HEIST_PLANNING)
	OR IS_THIS_A_NET_HEIST_PLANNING_GENERIC_CORONA()
		PRINTLN("[CORONA] ciCORONA_CAMERA_SETUP_BS_GENERIC_HEIST_PLANNING")
		IF IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_GENERIC_HEIST_PLANNING)
			vCameraPos = g_sNetHeistPlanningGenericClientData.sCoronaCameraData.vCamPos
			vCameraRot = g_sNetHeistPlanningGenericClientData.sCoronaCameraData.vCamRot
			fFov = g_sNetHeistPlanningGenericClientData.sCoronaCameraData.fCamFoV
		ELSE
			GET_NET_HEIST_PLANNING_GENERIC_CORONA_CAMERA_DATA(vCameraPos, vCameraRot, fFov)
		ENDIF
	
	ELIF IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_FIXER_GOLF_COURSE)
		PRINTLN("[CORONA] ciCORONA_CAMERA_SETUP_BS_FIXER_GOLF_COURSE")
		vCameraPos = <<-1381.7837, 38.8164, 63.1614>>
		vCameraRot = <<-19.0288, 0.0000, -35.2577>>
		fFov = 50.0000
	
	ELIF IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_FIXER_STORY)
	OR IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
	
	ELIF IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_FIXER_SHORT_TRIP_INTRO)
	OR (IS_THIS_A_FIXER_SHORT_TRIP()
	AND IS_CORONA_BIT_SET(CORONA_FIXER_SHORT_TRIP_INTRO))
		PRINTLN("[CORONA] ciCORONA_CAMERA_SETUP_BS_FIXER_SHORT_TRIP_INTRO")
		vCameraPos = <<-822.1176, -198.5518, 40.0850>>
		vCameraRot = <<13.7424, 0.0000, 145.7582>>
		fFov = 65.2994
		bShake = TRUE
		IF NOT IS_TRANSITION_SESSION_RESTARTING()
			bDoCameraBlur = FALSE
		ENDIF
	ELIF IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_FIXER_SHORT_TRIP)
		
	ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[CORONA] CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(g_FMMC_STRUCT.iRootContentIDHash)")
		vCameraPos = <<800.2757, -3051.3931, 10.5728>>
		vCameraRot = <<3.1956, 0.0000, -8.4268>>
		fFov = 50.0
		
	#IF FEATURE_DLC_2_2022
	ELIF IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_XMAS22_STORY_INTRO)
	OR (IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash) AND IS_BIT_SET(g_sXmas22Flow.Bitset, ciXMAS22_FLOW_BITSET__LAUNCHING_INTRO_MISSION))
		PRINTLN("[CORONA] ciCORONA_CAMERA_SETUP_BS_XMAS22_STORY_INTRO")
		vCameraPos = <<1403.9072, 3586.4277, 41.1676>>
		vCameraRot = <<-8.8877, 0.0000, 39.6448>>
		fFov = 62.0
		
	ELIF IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_XMAS22_STORY)
	OR IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[CORONA] ciCORONA_CAMERA_SETUP_BS_XMAS22_STORY or IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION")
		vCameraPos = <<622.9118, -396.3098, 31.0305>>
		vCameraRot = <<-10.3971, 0.0000, 133.2900>>
		fFov = 60.0
	#ENDIF
		
	ENDIF
		
	// Hard coded tutorial positioning of camera and rotation
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
	AND NOT bTutorialSwitch
		PRINTLN("[dsw] [SETUP_CORONA_CAMERA] Running tutorial ")
//
//		vCameraPos = <<-219.0, 318.1, 97.7>>
//		vCameraRot = <<-2.9, 0.0, -118.3>>
//		fFov = 39.0
		
		vCameraPos =  <<-213.5595, 316.9778, 96.9244>>
		vCameraRot = <<-1.0483, -0.0000, -127.3989>>
		fFov = 25.5102
	ENDIF
	
	// Handle the new heist camera positions
	IF iMissionType = FMMC_TYPE_MISSION
	AND bHeistMission
		
		HIDE_ALL_PLAYERS_FOR_CORONA()
		
		VECTOR vHeistVec
		GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistVec, MP_PROP_ELEMENT_HEIST_CAM_POS_OVERVIEW, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		
		IF NOT IS_VECTOR_ZERO(vHeistVec)
			PRINTLN("[CORONA] Set camera position to heist board: ", vHeistVec)
			vCameraPos = vHeistVec
		ENDIF
		
		GET_PLAYER_PROPERTY_HEIST_ROTATION(vHeistVec, MP_PROP_ELEMENT_HEIST_CAM_POS_OVERVIEW, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		PRINTLN("[CORONA] Set camera rotation to heist board: ", vHeistVec)
		vCameraRot = vHeistVec
		
// KGM 11/6/14: Removed - Rotation as 0,0,0 is valid
//		IF NOT IS_VECTOR_ZERO(vHeistVec)
//			PRINTLN("[CORONA] Set camera rotation to heist board: ", vHeistVec)
//			vCameraRot = vHeistVec
//		ENDIF		
	ENDIF
	 
	PRINTLN("[CORONA] vCameraPos = ", vCameraPos)
	PRINTLN("[CORONA] vCameraRot = ", vCameraRot)
		
	//Set the camera active
	SET_CAM_ACTIVE(ciCamera, TRUE)
	SET_CAM_COORD(ciCamera, vCameraPos)
	SET_CAM_ROT(ciCamera,  vCameraRot)
	IF fFov > 0.0
		SET_CAM_FOV(ciCamera, fFov)
	ELSE
		SET_CAM_FOV(ciCamera, 40.0)
	ENDIF
	
	IF bShake
		SHAKE_CAM(ciCamera, "HAND_SHAKE", fShake)
	ENDIF
	
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
	AND NOT bTutorialSwitch	
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//	IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
		//	AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK)
			IF NOT HAS_PLAYER_ACTIVATED_TUTORIAL_CORONA_CAMERA(PLAYER_ID())
				PRINTLN("[dsw] [SETUP_CORONA_CAMERA] Giving player goto task...")
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS )
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -209.434, 305.016, 95.9464 >>)  //<<-209.867, 306.969, 95.9464>>
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 12.500 )
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), <<-209.867, 306.969, 95.9464>>, PEDMOVE_WALK, DEFAULT, DEFAULT, 0.1)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
								

				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				
	//			VECTOR vTemp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<-209.867, 306.969, 95.9464>>, 12.500, <<0.0, -2.0, 0.0>>)
	//			
	//			PRINTLN("[dsw] Player offset pos... ",vTemp )
			ENDIF
		ENDIF
		CLEAR_AREA_OF_VEHICLES(<< -209.434, 305.016, 95.9464 >>, 30.0)
		SET_LOCAL_PLAYER_ACTIVATED_TUTORIAL_CORONA_CAMERA(TRUE)
	ENDIF
	
	IF bDoCameraBlur
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		OR bTutorialSwitch
			#IF IS_DEBUG_BUILD
			IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_toggleHeistRenderPhases")
			#ENDIF
			TRIGGER_CORONA_CAMERA_BLUR()
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	IF !AM_I_TRANSITION_SESSIONS_STARTING_GANG_OPS_QUICK_MATCH()
	AND !AM_I_TRANSITION_SESSIONS_STARTING_CASINO_HEIST_QUICK_MATCH()
	AND !AM_I_TRANSITION_SESSIONS_STARTING_HEIST_ISLAND_QUICK_MATCH()
	AND !AM_I_TRANSITION_SESSIONS_STARTING_TUNER_ROBBERY_QUICK_MATCH()
	AND !AM_I_TRANSITION_SESSIONS_STARTING_SHORT_TRIP_QUICK_MATCH()
		DO_SCREEN_FADE_IN_FOR_TRANSITION(0)
	ENDIF
	
	IF SHOULD_CORONA_PLAYER_CLEAR_CORONA_CAMERAS()
		CLEAR_FOCUS()
		PRINTLN("[CORONA] CLEAR_FOCUS()")
	ENDIF
	
	//Check if it's ok to set the focus
	IF IS_PLAYER_SWITCH_IN_PROGRESS() 
		vFocusPos = vCameraPos
		bSetFocus = TRUE
	ELSE
		SET_UP_CAMERA_FOCUS_POS_AND_VEL(vCameraPos)	
	ENDIF
	
	#IF IS_DEBUG_BUILD
	// Debug functionality to disable the blur of the screen
	IF g_bDisableCoronaIntroAndBlur
		ANIMPOSTFX_STOP_ALL()
	ENDIF
	#ENDIF
	
	PRINTLN("[CORONA] SET_EVERYONE_IGNORE_PLAYER TRUE")
	SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), TRUE)	
	FREEZE_PLAYER_PED(IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL(), IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_SVM) OR IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_GANGOPS) OR IS_BIT_SET(iSetupBS, ciCORONA_CAMERA_SETUP_BS_ARENA))
ENDPROC

/// PURPOSE: Display correct help text for host corona screen
PROC DISPLAY_CORONA_HOST_SCREEN_HELP(INT iMissionType, INT iSelection)
	IF iSelection = CORONA_HOST_NEW_INSTANCE
		IF IS_THIS_A_MINI_GAME(iMissionType)
			SET_FRONTEND_HELP_TEXT(0, "FM_HOST_CHA")
		ELSE
			SET_FRONTEND_HELP_TEXT(0, "FM_HOST_CH")
		ENDIF
	ELIF iSelection = CORONA_JOIN_EXISTING_PLAYER_INSTANCE
		SET_FRONTEND_HELP_TEXT(0, "FM_JOIN_P_CH")	
	ELSE
		IF IS_THIS_A_MINI_GAME(iMissionType)
			SET_FRONTEND_HELP_TEXT(0, "FM_JOIN_CHA")
		ELSE
			SET_FRONTEND_HELP_TEXT(0, "FM_JOIN_CH")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_TUTORIAL_CORONA_CAMERA(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails, INT &iTutorialCameraProg, SCRIPT_TIMER &timeTutCameraProg)
	
	//-- Help text done in MAINTAIN_TUTORIAL_CORONA_HELP in x:\gta5\script\dev_network\multiplayer\include\public\freemode\FMMC_Corona_Controller.sch
	VECTOR vFocusPos
	BOOL bStillNeedToSetFocus
	SWITCH iTutorialCameraProg
		CASE 0
			iTutorialCameraProg++
			SETUP_CORONA_CAMERA(g_sTransitionSessionData.ciCam, sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.iMissionVariation, 
								sLaunchMissionDetails.vStartPoint, sLaunchMissionDetails.vCamPos, 
								sLaunchMissionDetails.vCamHead, sLaunchMissionDetails.bIsContactMission, 
								 (sLaunchMissionDetails.bIsHeistMission OR sLaunchMissionDetails.bIsHeistPlanning),
								vFocusPos, bStillNeedToSetFocus, TRUE)
		BREAK
		
		CASE 1
			IF DOES_CAM_EXIST(g_sTransitionSessionData.ciCam)
				IF IS_CAM_ACTIVE(g_sTransitionSessionData.ciCam)
					IF HAS_PLAYER_STARTED_TUTORIAL_CORONA_PLAYBACK(PLAYER_ID())
						SET_LOCAL_PLAYER_READY_FOR_CORONA_HELP(TRUE)
						RESET_NET_TIMER(timeTutCameraProg)
						START_NET_TIMER(timeTutCameraProg)
				//		SET_CAM_PARAMS(g_sTransitionSessionData.ciCam, <<-218.8, 317.9, 97.7>>, <<-1.4, 0.0, -133.1>>, 39.0, 7000) //, GRAPH_TYPE_ACCEL,GRAPH_TYPE_LINEAR
						iTutorialCameraProg++
						CPRINTLN(DEBUG_MP_TUTORIAL," [dsw] [TriggerTut] [MAINTAIN_TUTORIAL_CORONA_CAMERA] iTutorialCameraProg = ", iTutorialCameraProg)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF HAS_NET_TIMER_EXPIRED(timeTutCameraProg, 10)
				SET_CAM_PARAMS(g_sTransitionSessionData.ciCam,<<-213.5261, 316.8810, 97.4122>>, <<-1.0483, -0.0000, -159.8570>>, 25.5102, 5000)
//				SHAKE_CAM(g_sTransitionSessionData.ciCam,"Hand_shake", 0.3)
				iTutorialCameraProg++
				CPRINTLN(DEBUG_MP_TUTORIAL," [dsw] [TriggerTut] [MAINTAIN_TUTORIAL_CORONA_CAMERA] iTutorialCameraProg = ", iTutorialCameraProg)
					
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_CAM_INTERPOLATING(g_sTransitionSessionData.ciCam)
				IF HAS_PLAYER_FINISHED_TUTORIAL_CORONA_PLAYBACK(PLAYER_ID())
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						
						SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-209.867, 306.969, 95.9464>>, DEFAULT, TRUE)  //
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 12.500 )
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						SEQUENCE_INDEX seq
						OPEN_SEQUENCE_TASK(seq)
							TASK_CLEAR_LOOK_AT(NULL)
							TASK_PLAY_ANIM(NULL, "mp_intro_seq@ig_3_car", "player_intro", DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0.19)
						//	TASK_PLAY_ANIM(NULL, "mp_intro_seq@ig_3_car", "player_intro")
							TASK_PLAY_ANIM(NULL, "mp_intro_seq@ig_3_car", "player_idle", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING)
						CLOSE_SEQUENCE_TASK(seq)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
						ENDIF

						CLEAR_SEQUENCE_TASK(seq)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						
						IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),  "mp_intro_seq@ig_3_car", "player_intro")
						//	SET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "mp_intro_seq@ig_3_car", "player_intro", 0.18)
							CPRINTLN(DEBUG_MP_TUTORIAL," [dsw] [TriggerTut] [MAINTAIN_TUTORIAL_CORONA_CAMERA] Set player current anim time")
						ELSE
							CPRINTLN(DEBUG_MP_TUTORIAL," [dsw] [TriggerTut] [MAINTAIN_TUTORIAL_CORONA_CAMERA] Not playing anim ")
						ENDIF
					ENDIF
					IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iMyTutorialSceneId)
						SET_SYNCHRONIZED_SCENE_PHASE(g_iMyTutorialSceneId, 0.23)
						CPRINTLN(DEBUG_MP_TUTORIAL," [dsw] [TriggerTut] [MAINTAIN_TUTORIAL_CORONA_CAMERA] Set scene phase ")
					ENDIF
				//	SET_CAM_PARAMS(g_sTransitionSessionData.ciCam, <<-204.1000, 307.8000, 97.3000>>, <<-3.1000, 0.0000, 88.9397>>, 40.0)
					SET_CAM_PARAMS(g_sTransitionSessionData.ciCam,  <<-204.5381, 307.7626, 96.9876>>, <<1.0590, 0.0000, 85.1157>>, 33.0196)
					iTutorialCameraProg++
					CPRINTLN(DEBUG_MP_TUTORIAL," [dsw] [TriggerTut] [MAINTAIN_TUTORIAL_CORONA_CAMERA] iTutorialCameraProg = ", iTutorialCameraProg)
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF IS_LOCAL_PLAYER_READY_FOR_TUTORIAL_CORONA_SWOOP() 
				SET_LOCAL_PLAYER_DONE_RACE_TUTORIAL_CORONA_SWOOP(TRUE)				
				iTutorialCameraProg++
			ENDIF
		BREAK
//		
//		CASE 5
//			TOGGLE_RENDERPHASES(FALSE) 
//			TOGGLE_RENDERPHASES(FALSE)	
//			PRINTLN("[TS] TOGGLE_RENDERPHASES(FALSE)")					
//			PRINTLN("[TS] TOGGLE_RENDERPHASES(FALSE)")					
//			iTutorialCameraProg++
//		BREAK

	ENDSWITCH
ENDPROC

/// PURPOSE: Returns the correct text to pass in as the playlist type for the string:
///    [FMMC_PL_LEAD2]
///    ~a~ ~a~ - ~a~ - ~1~ of ~1~
FUNC STRING GET_CORONA_PLAYLIST_TYPE_STRING()
	//Playing a challenge
	IF IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
		RETURN "FMMC_PL_TYPE1"	//Challenge
	//Setting a challenge time
//	ELIF IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
//		RETURN "FMMC_PL_TYPE2"	//Challenge
	//doing a head to head
	ELIF IS_CORONA_IN_HEAD_2_HEAD_MODE()
		RETURN "FMMC_PL_TYPE3"	//Head 2 Head 
	ENDIF
	//bog standard playlist!
	RETURN "FMMC_PL_TYPE0" //Playlist
ENDFUNC
/// PURPOSE: Returns the correct text label for the minigame
FUNC STRING GET_CORONA_MINIGAME_TITLE(INT iMissionType)
	SWITCH iMissionType
		CASE FMMC_TYPE_MG_DARTS					RETURN "HUD_MG_DARTS"
		CASE FMMC_TYPE_MG_GOLF					RETURN "HUD_MG_GOLF"
		CASE FMMC_TYPE_MG_SHOOTING_RANGE		RETURN "HUD_MG_RANGEN"
		CASE FMMC_TYPE_MG_TENNIS				RETURN "HUD_MG_TENNISN"
		CASE FMMC_TYPE_MG_ARM_WRESTLING			RETURN "HUD_MG_ARM"
		CASE FMMC_TYPE_MG_PILOT_SCHOOL			RETURN "HUD_MG_PILOT"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE: Keep track of if we are skyswooping into corona
FUNC BOOL MAINTAIN_CORONA_SKYSWOOP_STATE()
	IF IS_CORONA_BIT_SET(CORONA_PLAYER_IN_SKYSWOOP)
		
		IF IS_SKYSWOOP_AT_GROUND()
			PRINTLN("[CORONA] MAINTAIN_CORONA_SKYSWOOP_STATE - no longer skyswooping")
			
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_SKYSWOOP"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
			
			CLEAR_CORONA_BIT(CORONA_PLAYER_IN_SKYSWOOP)
			
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PERFORM_SIMPLE_SHAPETEST_AROUND_CORONA(SHAPETEST_INDEX &stiShapeTest, VECTOR vPos, FLOAT fCoronaSize, BOOL &bPass)
	VECTOR vTemp1
	VECTOR vTemp2
	INT iResult
	ENTITY_INDEX hitEntity
	
	IF stiShapeTest  = NULL
		
		//Move the position up so it's off the ground
		VECTOR vDimensions
		VECTOR vRot
		
		//Get the rotation to aligh it to the camera
		vRot = GET_FINAL_RENDERED_CAM_ROT()
		vRot.X = 0.0
		vRot.Y = 0.0
		vDimensions = <<fCoronaSize, fCoronaSize, 2.0>>	
		
		vPos += <<0.0, 0.0, 1.4>>
		
		stiShapeTest = START_SHAPE_TEST_BOX(vPos, vDimensions, vRot, EULER_YXZ, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS)
	ELSE
		IF GET_SHAPE_TEST_RESULT(stiShapeTest, iResult, vTemp1, vTemp2, hitEntity) = SHAPETEST_STATUS_RESULTS_READY
			RELEASE_SCRIPT_GUID_FROM_ENTITY(hitEntity)
			stiShapeTest  = NULL
			IF iResult = 0
				bPass = TRUE				
			ELSE
				bPass = FALSE				
			ENDIF
			iResult = 0
			RETURN TRUE
		ELSE
			RELEASE_SCRIPT_GUID_FROM_ENTITY(hitEntity)
			IF GET_SHAPE_TEST_RESULT(stiShapeTest, iResult, vTemp1, vTemp2, hitEntity) = SHAPETEST_STATUS_NONEXISTENT
				stiShapeTest = NULL
			ENDIF
			RELEASE_SCRIPT_GUID_FROM_ENTITY(hitEntity)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Use this to check if an area is clear to start the lobby corona. It checks an area with a width of ciSTART_CHECK_POINT_SIZE_SAFE_AREA = 6.7
/// PARAMS:
///    bReturnPass - When the func returns true, check bReturnPass to see if the area is clear or not
///    stiShapeTest - The shapetest index used to test if the area is clear
///    vPos - The position to check is clear. Should be the ground position.
/// RETURNS:
///    Returns True when the shapetest is finished. Check bReturnPass to see if the area is clear or not.
FUNC BOOL IS_AREA_CLEAR_FOR_CORONA(BOOL &bReturnPass, SHAPETEST_INDEX &stiShapeTest, VECTOR vPos)
		
	IF PERFORM_SIMPLE_SHAPETEST_AROUND_CORONA(stiShapeTest, vPos, ciSTART_CHECK_POINT_SIZE_SAFE_AREA, bReturnPass)
		
		FLOAT fRange = ciSTART_CHECK_POINT_SIZE_SAFE_AREA/2
		FLOAT fMaxSlope = 25.0
	
		IF FMMC_IS_THIS_A_SLOPE(vPos, NULL, fMaxSlope, fRange)	
			bReturnPass = FALSE
		ENDIF
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC
PROC SET_FRONTEND_HEADER_TITLE_OFFLINE(	STRING tlHeaderTitle, BOOL bVerified)

	PRINTLN("MP Scaleform: SET_FRONTEND_HEADER_TITLE - called with title string: ", tlHeaderTitle)

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND_HEADER("SHIFT_CORONA_DESC")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND_HEADER("SET_HEADER_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlHeaderTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVerified)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_OFFLN_HD")
			INT iLoop 
			FOR iLoop = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION - 1)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_FMMC_STRUCT.tl63MissionDecription[iLoop])
			ENDFOR			
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


FUNC STRING GET_FMMC_MISSION_TYPE_FOR_HEADER(INT iMissionType)
	SWITCH iMissionType
		CASE FMMC_TYPE_MISSION				RETURN "FM_NXT_MIS"
		CASE FMMC_TYPE_DEATHMATCH			RETURN "FM_NXT_DM"
		CASE FMMC_TYPE_RACE					RETURN "FM_NXT_RAC"
		CASE FMMC_TYPE_BASE_JUMP			RETURN "FM_NXT_PAR"
		CASE FMMC_TYPE_SURVIVAL				RETURN "FM_NXT_SUR"
	ENDSWITCH
	RETURN "FM_NXT_MIS"
ENDFUNC

/// PURPOSE: Passes the mission title to the scaleform. If it is a playlist it will display job number.
PROC CORONA_SET_HEADER_TITLE(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails, BOOL bUpdateTexture = FALSE)
	
	BOOL bLiteralStr = TRUE
	//STRING cachedDescription = ""
	TEXT_LABEL_63 strName = sLaunchMissionDetails.stMissionName
		
	IF IS_THIS_A_MINI_GAME(sLaunchMissionDetails.iMissionType)
		bLiteralStr = FALSE
		strName = GET_CORONA_MINIGAME_TITLE(sLaunchMissionDetails.iMissionType)
		
		SET_FRONTEND_HEADER_TITLE(strName, FALSE, bLiteralStr, "", -1, -1, FALSE)
	ELSE
		
		IF SHOULD_USE_OFFLINE_UGC_CONTENT()		
			SET_FRONTEND_HEADER_TITLE_OFFLINE(strName, FALSE)			
		ELSE
			STRING strAdditionalDesc = ""
			IF GET_CORONA_STATUS() = CORONA_STATUS_BETTING
			OR GET_CORONA_STATUS() = CORONA_STATUS_IN_CORONA
				IF g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers > 1
					// Adding mode specific text B* 2413956
					IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
						SWITCH g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_DESTROY_LOSER]
							CASE ciCORONA_DESTROY_VEHICLE_LAP 	strAdditionalDesc = "COR_DESC_DL0"	BREAK
							CASE ciCORONA_DESTROY_VEHICLE_15	strAdditionalDesc = "COR_DESC_DL1"	BREAK
							CASE ciCORONA_DESTROY_VEHICLE_30	strAdditionalDesc = "COR_DESC_DL2"	BREAK
							CASE ciCORONA_DESTROY_VEHICLE_60	strAdditionalDesc = "COR_DESC_DL3"	BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			
				// If we have used an additional string and we are beyond our bound, remove this string.
				IF NOT IS_STRING_NULL_OR_EMPTY(strAdditionalDesc)
					
					INT iLoop 
					INT iLength
					FOR iLoop = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION - 1)
						iLength += GET_LENGTH_OF_LITERAL_STRING(g_FMMC_STRUCT.tl63MissionDecription[iLoop])
					ENDFOR
					iLength += GET_LENGTH_OF_STRING_WITH_THIS_TEXT_LABEL(strAdditionalDesc)
					
					INT iMaxLength = 500
					IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
					OR GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
					OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
						iMaxLength = 250
					ENDIF
					
					// Null our string if we are close to the max characters for description
					IF iLength > iMaxLength
						strAdditionalDesc = ""
					ENDIF
				ENDIF
				
			ENDIF
			
			
			//cachedDescription = UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(iDescHash, 500)
			PRINTLN("[CORONA] CORONA_SET_HEADER_TITLE - Mission Name: ", strName, " and Description: ", g_FMMC_STRUCT.tl63MissionDecription[0], g_FMMC_STRUCT.tl63MissionDecription[1], g_FMMC_STRUCT.tl63MissionDecription[2], g_FMMC_STRUCT.tl63MissionDecription[3], g_FMMC_STRUCT.tl63MissionDecription[4], g_FMMC_STRUCT.tl63MissionDecription[5],g_FMMC_STRUCT.tl63MissionDecription[6],g_FMMC_STRUCT.tl63MissionDecription[7])
			IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()				
				TEXT_LABEL_63 tlPlaylistName = g_sCurrentPlayListDetails.tl31PlaylistName
				IF IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
				//OR IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
					tlPlaylistName = g_sCurrentPlayListDetails.tl31ChallengeName
				ENDIF

				SET_FRONTEND_HEADER_TITLE_FOR_PLAYLIST_MODE("FMMC_PL_LEAD2", GET_CORONA_PLAYLIST_TYPE_STRING(), tlPlaylistName, GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.iCoronaMissionSubtype, g_FMMC_STRUCT.iMissionTypeBitSet, g_FMMC_STRUCT.iMissionTypeBitSetTwo, DEFAULT, g_FMMC_STRUCT.iAdversaryModeType), 
															strName, g_sCurrentPlayListDetails.iPlaylistProgress, g_sCurrentPlayListDetails.iLength, DEFAULT, TRUE, DEFAULT, strAdditionalDesc)
			ELSE
				
				// Option to overwrite the description we display
				TEXT_LABEL_15 tlDescOverwrite = ""
				BOOL bShowDescription = TRUE
			
				#IF IS_DEBUG_BUILD
				IF (IS_THIS_MISSION_A_PRE_PLANNING_INTRO_CUTSCENE(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsForHeistPrepCutscene, sLaunchMissionDetails.bIsForMidStrandCutscene)
				OR IS_THIS_MISSION_A_PRE_PLANNING_MID_STRAND_CUTSCENE(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsForHeistPrepCutscene, sLaunchMissionDetails.bIsForMidStrandCutscene))
				AND NOT IS_THIS_MISSION_A_TUTORIAL_CUTSCENE(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsForTutorialCutscene)
					bShowDescription = FALSE
					tlDescOverwrite = GET_HEIST_STRAND_GENERIC_DESCRIPTION(g_FMMC_STRUCT.iRootContentIDHash)
					PRINTLN("[CORONA] CORONA_SET_HEADER_TITLE - This is a heist cutscene. Hide description and overwrite with generic label")
				ENDIF
				#ENDIF
			
				IF sLaunchMissionDetails.bIsContactMission
				AND sLaunchMissionDetails.contactAsInt != -1
				AND NOT CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				#IF FEATURE_CASINO_HEIST
				AND NOT IS_THIS_MISSION_A_CASINO_HEIST_INTRO_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
				#ENDIF
				#IF FEATURE_HEIST_ISLAND
				AND NOT IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
				#ENDIF
				AND NOT IS_THIS_A_TUNER_ROBBERY_FINALE()
				AND NOT IS_THIS_A_FIXER_STORY_MISSION()
				AND NOT CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					
					TEXT_LABEL_23 stDetail
					IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
						stDetail = "SECUROSERV"
					ELIF WVM_FLOW_IS_CURRENT_MISSION_WVM_FLOW()
						stDetail = "AGENT14"
					ELSE
						stDetail = g_sCharacterSheetAll[sLaunchMissionDetails.contactAsInt].label
					ENDIF
					SET_FRONTEND_HEADER_TITLE("FMMC_TL_CM", FALSE, bLiteralStr, strName, -1, -1, bShowDescription, stDetail, DEFAULT, strAdditionalDesc)
				ELSE			
					SET_FRONTEND_HEADER_TITLE(strName, FALSE, bLiteralStr, "", -1, -1, bShowDescription, DEFAULT, tlDescOverwrite, strAdditionalDesc)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bUpdateTexture
		// Initially removed for 2113114.
//		#IF FEATURE_HEIST_PLANNING
//		// Check to see if we are required to add a texture
//		IF IS_THIS_MISSION_OF_TYPE_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistMission, sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene)
//			
//			PRINTLN("[CORONA] CORONA_SET_HEADER_TITLE - Set BG Image with title.")
//			SET_FRONTEND_HEADER_BG_IMAGE("MPHEIST_HEADER_WALLPAPER", "MPHEIST_HEADER_WALLPAPER", 0.71)			
//		ENDIF
//		#ENDIF
	ENDIF
	
ENDPROC

FUNC TEXT_LABEL_15 GET_RACE_CLASS_STRING()

	TEXT_LABEL_15 tlHostOption

	IF CV2_IS_THIS_CORONA_A_PROFESSIONAL_JOB()
		IF CV2_GET_CURRENT_PLAYLIST_CLASS() = FMMC_VEHICLE_CLASS_CUSTOM
			tlHostOption = "FMSTP_PRC14"
			RETURN tlHostOption
		ENDIF
	ENDIF
	
	tlHostOption =  GET_RACE_BASE_STRING(g_FMMC_STRUCT.iRaceType)
	
	INT iVehicleClass = 0
	IF g_FMMC_STRUCT.iRaceClass != -1
		iVehicleClass += g_FMMC_STRUCT.iRaceClass	
	ENDIF
	
	tlHostOption += iVehicleClass
	
	RETURN tlHostOption
ENDFUNC
/// PURPOSE: Fills out the details panel on the right hand side for the host screen
PROC DISPLAY_CORONA_HOST_SCREEN_DETAILS(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)

	// Set up details card
	IF NOT IS_THIS_A_MINI_GAME(sLaunchMissionDetails.iMissionType)
		
		BOOL bRockstarCreated
		TEXT_LABEL_23 tlCreator
		TEXT_LABEL_15 tlCreatorCrewTag
		
		// Grab the correct creator string
		IF sLaunchMissionDetails.bDefaultOptionsLoaded
			PRINTLN("[CORONA] DISPLAY_CORONA_HOST_SCREEN_DETAILS - sLaunchMissionDetails.bDefaultOptionsLoaded = TRUE, use sLaunchMissionDetails.stUserName. bSCNickname: ", g_FMMC_STRUCT.bScNickName)
			tlCreator = sLaunchMissionDetails.stUserName
		ELSE
			PRINTLN("[CORONA] DISPLAY_CORONA_HOST_SCREEN_DETAILS - sLaunchMissionDetails.bDefaultOptionsLoaded = FALSE, use g_FMMC_STRUCT.tl63MissionCreator = ", g_FMMC_STRUCT.tl63MissionCreator, ", bSCNickname: ", g_FMMC_STRUCT.bScNickName)
			tlCreator = g_FMMC_STRUCT.tl63MissionCreator
		ENDIF
			
		GAMER_HANDLE creatorHandle
		INT iJobType = 0
		INT iIconColour = ENUM_TO_INT(HUD_COLOUR_WHITE)
		
		IF sLaunchMissionDetails.iCreatorID = FMMC_ROCKSTAR_CREATOR_ID
			tlCreator = GET_ROCKSTAR_CREATED_USER_STRING()
			tlCreatorCrewTag = ""
			bRockstarCreated = TRUE
			iIconColour = ENUM_TO_INT(HUD_COLOUR_BLUE)
		ELIF sLaunchMissionDetails.iCreatorID = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
			bRockstarCreated = TRUE
			iIconColour = ENUM_TO_INT(HUD_COLOUR_BLUE)
		ELSE
			// Grab the crewtag of creator if possible
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23MissionCreatorID)
				creatorHandle = GET_GAMER_HANDLE_USER(g_FMMC_STRUCT.tl23MissionCreatorID)
				IF IS_GAMER_HANDLE_VALID(creatorHandle)
					GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(GET_GAMER_HANDLE_USER(g_FMMC_STRUCT.tl23MissionCreatorID), tlCreatorCrewTag)			
				ENDIF
			ENDIF
		ENDIF
	
		INT iDetailIndex
		iDetailIndex = 0
	
		// Set our photo if possible
		SET_FRONTEND_DETAILS_TITLE(CORONA_COLUMN_TWO, "", sLaunchMissionDetails.stMissionName, iJobType, "", "", TRUE, 1,  sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.iCoronaMissionSubType, sLaunchMissionDetails.stFileName, DEFAULT, g_FMMC_STRUCT.iRootContentIDHash)
		
		//	-----------
		//	RATING
		//	-----------
		
		//  url:bugstar:2545351 - Dont show mission rating if this is a lowrider flow mission
		IF NOT FM_FLOW_IS_THIS_A_MISSION_A_MP_FLOW_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		AND NOT SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		AND NOT GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		#IF FEATURE_CASINO_HEIST
		AND NOT CASINO_HEIST_FLOW_IS_A_CASINO_HEIST_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		#ENDIF
		#IF FEATURE_HEIST_ISLAND
		AND NOT HEIST_ISLAND_FLOW_IS_A_HEIST_ISLAND_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		#ENDIF
		AND NOT IS_THIS_A_TUNER_ROBBERY_FINALE()
		AND NOT IS_THIS_A_FIXER_STORY_MISSION()
		
			IF NOT IS_THIS_MISSION_OF_TYPE_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistMission
								 , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene )
				IF sLaunchMissionDetails.iRating = FMMC_NOT_YET_RATED
					SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_TEXT_LABEL, 	CORONA_COLUMN_TWO, iDetailIndex, 0, "FM_ISC_RAT0", "FMMC_NOTRATE")
				ELSE
					SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_FLOAT, 	CORONA_COLUMN_TWO, iDetailIndex, 0, "FM_ISC_RAT0", "FM_ISC_RAT1", -1, -1, PICK_FLOAT((TO_FLOAT(sLaunchMissionDetails.iRating) > 0.0), TO_FLOAT(sLaunchMissionDetails.iRating), 0.0) )
				ENDIF
				iDetailIndex++
			ENDIF
			
		ENDIF
		
		//	-----------
		//	FROM
		//	-----------
		
		IF NOT CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		AND NOT CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		
			IF NOT (sLaunchMissionDetails.bIsContactMission AND bRockstarCreated)
				SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_LITERAL_STRING, 	CORONA_COLUMN_TWO, iDetailIndex, 0, "FM_ISC_CB", tlCreator, -1, -1, -1, FALSE, tlCreatorCrewTag, DEFAULT, g_FMMC_STRUCT.bScNickName)
				iDetailIndex++
			ELSE
				TEXT_LABEL_23 stDetail
				IF DOES_CURRENT_MISSION_USE_GANG_BOSS_FLOW()
				AND NOT GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
				#IF FEATURE_CASINO_HEIST
				AND NOT CASINO_HEIST_FLOW_IS_A_CASINO_HEIST_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
				#ENDIF
				#IF FEATURE_HEIST_ISLAND
				AND NOT HEIST_ISLAND_FLOW_IS_A_HEIST_ISLAND_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
				#ENDIF
				AND NOT IS_THIS_A_TUNER_ROBBERY_FINALE()
				AND NOT IS_THIS_A_FIXER_STORY_MISSION()
					stDetail = "SECUROSERV"
				ELSE
					stDetail = g_sCharacterSheetAll[sLaunchMissionDetails.contactAsInt].label
				ENDIF
				SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_TEXT_LABEL, 	CORONA_COLUMN_TWO, iDetailIndex, 0, "FM_ISC_FROM", stDetail, -1, -1, -1, FALSE, tlCreatorCrewTag, DEFAULT, g_FMMC_STRUCT.bScNickName)
				iDetailIndex++
			ENDIF
			
		ENDIF
		
		//	---------------
		//	OPENS AT RANK
		//	---------------
		
		IF NOT IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
		AND NOT GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		#IF FEATURE_CASINO_HEIST
		AND NOT CASINO_HEIST_FLOW_IS_A_CASINO_HEIST_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		#ENDIF
		#IF FEATURE_HEIST_ISLAND
		AND NOT HEIST_ISLAND_FLOW_IS_A_HEIST_ISLAND_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		#ENDIF
		AND NOT IS_THIS_A_TUNER_ROBBERY_FINALE()
		AND NOT IS_THIS_A_FIXER_STORY_MISSION()
		AND NOT IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		#IF FEATURE_DLC_2_2022
		AND NOT IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		#ENDIF
			IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
				TEXT_LABEL_15 tlHostOption = GET_RACE_CLASS_STRING()
				SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_TEXT_LABEL, 			CORONA_COLUMN_TWO, iDetailIndex, 0, "FMMC_VEH_CL", tlHostOption)
			ELSE
				IF GET_PLAYER_RANK(PLAYER_ID()) >= sLaunchMissionDetails.iRank
					SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_INT, 				CORONA_COLUMN_TWO, iDetailIndex, 0, "PM_RANK", "NUMBER", sLaunchMissionDetails.iRank)
				ELSE
					SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_2_INTS, 			CORONA_COLUMN_TWO, iDetailIndex, 0, "PM_RANK", "FM_ISC_RANKB", sLaunchMissionDetails.iRank, GET_PLAYER_RANK(PLAYER_ID()))
				ENDIF
			ENDIF
			iDetailIndex++
		ENDIF
		
		// If number of players are the same, show single number
		IF GET_MISSION_TYPE_MIN(sLaunchMissionDetails) = GET_MISSION_TYPE_MAX(sLaunchMissionDetails)
			SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_INT, 			CORONA_COLUMN_TWO, iDetailIndex, 0, "FM_ISC_NO0", "NUMBER", GET_MISSION_TYPE_MIN(sLaunchMissionDetails))
		ELSE
			SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_2_INTS, 		CORONA_COLUMN_TWO, iDetailIndex, 0, "FM_ISC_NO0", "FM_ISC_NO1", GET_MISSION_TYPE_MIN(sLaunchMissionDetails), GET_MISSION_TYPE_MAX(sLaunchMissionDetails))
		ENDIF
		iDetailIndex++

		//	-----------
		//	PLAYERS
		//	-----------

		INT iMissionIcon
		IF sLaunchMissionDetails.iMissionType = FMMC_TYPE_RACE
			GET_PAUSE_MENU_MISSION_ICON(sLaunchMissionDetails.iMissionType, g_FMMC_STRUCT.iRaceType, iMissionIcon, g_FMMC_STRUCT.iAdversaryModeType)
		ELSE
			GET_PAUSE_MENU_MISSION_ICON(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.iCoronaMissionSubType, iMissionIcon, g_FMMC_STRUCT.iAdversaryModeType)
		ENDIF
		
		STRING strMissionType = GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.iCoronaMissionSubType, g_FMMC_STRUCT.iMissionTypeBitSet, g_FMMC_STRUCT.iMissionTypeBitSetTwo, DEFAULT, g_FMMC_STRUCT.iAdversaryModeType)
		
		//	-----------
		//	TYPE
		//	-----------
		
		IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
			strMissionType = "FMMC_RSTAR_HF"
			iIconColour = ENUM_TO_INT(HUD_COLOUR_H)
			
		ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		#IF FEATURE_HEIST_ISLAND
		OR HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_MISSION_FINALE()
		#ENDIF
			strMissionType = "FMMC_RSTAR_HF"
			iIconColour = ENUM_TO_INT(HUD_COLOUR_H)
			
		// Override mission type for Prep Missions. May want to integrate this into GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU()
		ELIF IS_THIS_MISSION_A_PRE_PLANNING_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistPlanning)
			strMissionType = "FMMC_RSTAR_HP"
			iIconColour = ENUM_TO_INT(HUD_COLOUR_H)
		ELIF IS_THIS_MISSION_A_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistMission)
			strMissionType = "FMMC_RSTAR_HF"
			iIconColour = ENUM_TO_INT(HUD_COLOUR_H)
			
			// Reset our icon here as the subtype for finales is the same for opening cutscenes
			IF IS_THIS_MISSION_A_PRE_PLANNING_INTRO_CUTSCENE(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsForHeistPrepCutscene, sLaunchMissionDetails.bIsForMidStrandCutscene)
				iMissionIcon = 17
			ENDIF			
		ENDIF
	
		SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_MISSION_TYPE, CORONA_COLUMN_TWO, iDetailIndex, 0, "PM_TYPE", strMissionType, 
										iMissionIcon, iIconColour, DEFAULT, DEFAULT, DEFAULT, sLaunchMissionDetails.bPlayedAlready)
		iDetailIndex++

		//	--------------------
		//	PLAYERS (NON-HEIST)
		//	--------------------

		IF sLaunchMissionDetails.iMissionType = FMMC_TYPE_RACE
			//Fix for 1461303. Ross W 
			IF SHOULD_USE_METRIC_MEASUREMENTS()
				SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_FLOAT, 		CORONA_COLUMN_TWO, iDetailIndex, 0, "FM_ISC_DIST", "FM_ISC_DIST2KM", -1, -1, (g_FMMC_STRUCT.fRaceTotalLength)/1000.0)
			ELSE
				SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_FLOAT, 		CORONA_COLUMN_TWO, iDetailIndex, 0, "FM_ISC_DIST", "FM_ISC_DIST2", -1, -1, CONVERT_METRES_TO_MILES(g_FMMC_STRUCT.fRaceTotalLength))
			ENDIF
			iDetailIndex++
		ELIF sLaunchMissionDetails.iMissionType = FMMC_TYPE_MISSION
			IF g_FMMC_STRUCT.iMaxNumberOfTeams > 1
			AND NOT IS_THIS_MISSION_OF_TYPE_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistMission
						 , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene )
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSELECTABLE_TEAM_NUMBERS)
					SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_INT, 		CORONA_COLUMN_TWO, iDetailIndex, 0, "FM_ISC_TEAM", "NUMBER", g_FMMC_STRUCT.iMaxNumberOfTeams)
				ELSE
					SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_2_INTS, 		CORONA_COLUMN_TWO, iDetailIndex, 0, "FM_ISC_TEAM", "FM_ISC_NO1", g_FMMC_STRUCT.iMinNumberOfTeams, g_FMMC_STRUCT.iMaxNumberOfTeams)
				ENDIF
				iDetailIndex++
			ENDIF
		ENDIF
		
		//	-----------------------------
		//	HEIST CHALLENGES (GANG OPS)
		//	-----------------------------

		IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		
			SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_DESCRIPTION, CORONA_COLUMN_TWO, iDetailIndex, 0, "", GET_FILENAME_FOR_AUDIO_CONVERSATION("GOPSCHAL_CHALL"))
			iDetailIndex++
		
			IF IS_ANY_GANGOPS_FLOW_AWARD_IN_PROGRESS()
		
				IF IS_ANY_GANGOPS_MASTERMIND_AWARD_IN_PROGRESS()
					SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_TEXT_LABEL, CORONA_COLUMN_TWO, iDetailIndex, 0, GET_GANGOPS_AWARD_TEXT_LABEL(GET_CURRENT_GANGOPS_MASTERMIND_AWARD()), "GOPSCHAL_ACT")
					iDetailIndex++
				ENDIF
							
				IF IS_ANY_GANGOPS_LOYALTY_AWARD_IN_PROGRESS()
					SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_TEXT_LABEL, CORONA_COLUMN_TWO, iDetailIndex, 0, GET_GANGOPS_AWARD_TEXT_LABEL(GET_CURRENT_GANGOPS_LOYALTY_AWARD()), "GOPSCHAL_ACT")
					iDetailIndex++
				ENDIF
			
				IF IS_GANGOPS_FLOW_AWARD_IN_PROGRESS(MPPLY_AWD_GANGOPS_ALLINORDER_INDEX)
					SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_TEXT_LABEL, CORONA_COLUMN_TWO, iDetailIndex, 0, GET_GANGOPS_AWARD_TEXT_LABEL(MPPLY_AWD_GANGOPS_ALLINORDER_INDEX), "GOPSCHAL_ACT")
					iDetailIndex++
				ENDIF
				
				IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DESCRIPTION")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
			ELSE
				SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_LITERAL_STRING, CORONA_COLUMN_TWO, iDetailIndex, 0, "", GET_FILENAME_FOR_AUDIO_CONVERSATION("GOPSCHAL_NCHAL"))
				iDetailIndex++
			ENDIF
		ENDIF

	ELSE
		
		// Set Title over the photo
		SET_FRONTEND_DETAILS_TITLE(CORONA_COLUMN_TWO, "", GET_CORONA_MINIGAME_TITLE(sLaunchMissionDetails.iMissionType), 0, "", "", FALSE, 1,  sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.iCoronaMissionSubType, sLaunchMissionDetails.stFileName, DEFAULT, g_FMMC_STRUCT.iRootContentIDHash)
		
		// Type
		SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_TEXT_LABEL, 	CORONA_COLUMN_TWO, 0, 0, "PM_TYPE", GET_CORONA_MINIGAME_TITLE(sLaunchMissionDetails.iMissionType))
		
		// Players
		IF GET_MISSION_TYPE_MIN(sLaunchMissionDetails) = GET_MISSION_TYPE_MAX(sLaunchMissionDetails)
			SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_INT, 			CORONA_COLUMN_TWO, 1, 0, "FM_ISC_NO0", "NUMBER", GET_MISSION_TYPE_MIN(sLaunchMissionDetails))
		ELSE
			SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_2_INTS, 		CORONA_COLUMN_TWO, 1, 0, "FM_ISC_NO0", "FM_ISC_NO1", GET_MISSION_TYPE_MIN(sLaunchMissionDetails), GET_MISSION_TYPE_MAX(sLaunchMissionDetails))
		ENDIF
		
		// Location
		SET_FRONTEND_DETAILS_DATA_SLOT(FRONTEND_DETAILS_TYPE_TEXT_LABEL, 	CORONA_COLUMN_TWO, 2, 0, "PM_AREA", GET_NAME_OF_ZONE(sLaunchMissionDetails.vStartPoint))
	ENDIF
	
ENDPROC

FUNC BOOL CONTROL_FM_MISSION_WARNING_SCREEN(	BOOL 		&bQuit,
												INT 		&iButtonBS,
												BOOL		bFreezeScreen = FALSE,
												BOOL		bIsHeist = FALSE)
	
	// Call this every frame
	TEXT_LABEL_15 tlWarningText = "FM_CSC_QUIT1"
	IF IS_THIS_A_MINI_GAME(g_FMMC_STRUCT.iMissionType)
		tlWarningText = "FM_CSC_QUIT2"
	ENDIF

	// If this is a playlist and you have not reached the end, warn about bad sport
	IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		
		IF NOT IS_TRANSITION_SESSIONS_PLAYLIST_SAFE_TO_QUIT()
			tlWarningText = "FM_CSC_QUIT3"
		ENDIF
	ENDIF
	
	// Check if the player is SCTV and adjust message slightly
	IF IS_PLAYER_SCTV(PLAYER_ID())
		tlWarningText = "SPEC_SURE"		
	ENDIF
	
	//If we're in a party
	STRING stPartyWarning = ""	
	IF IS_XBOX_PLATFORM()
		IF NETWORK_GET_PLATFORM_PARTY_MEMBER_COUNT() > 1
			stPartyWarning = "FM_CSC_QUITP"			
		ENDIF
	ENDIF
	
	IF bFreezeScreen
	AND IS_XBOX_PLATFORM()
		// Handle the delay there is in a toggle of renderphase for XB1
		IF GET_SKYFREEZE_STAGE() != SKYFREEZE_FROZEN
			PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - XB1 Freeze screen behind warning message")
			SET_SKYFREEZE_FROZEN()
		ENDIF		
	ENDIF
	
	// Disable vote whilst we're on warning screen to quit
	DISABLE_VOTE_THIS_FRAME(TRUE)
	
	SET_WARNING_MESSAGE_WITH_HEADER("FM_CSC_QUIT", tlWarningText, (FE_WARNING_YES | FE_WARNING_NO), stPartyWarning)
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
	ENDIF
	
	// Cancel quit
	IF NOT IS_BIT_SET(iButtonBS, FM_PRESSED_BS_CIRCLE)
	AND NOT NETWORK_SESSION_IS_DISPLAYING_INVITE_CONFIRMATION()
		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		
			PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - cancel pressed")
			
			// Make sure the freeze is removed
			IF GET_SKYFREEZE_STAGE() != SKYFREEZE_NONE
			AND bFreezeScreen
				PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - cancel pressed, clear frozen screen")
				SET_SKYFREEZE_CLEAR()
			ENDIF
								
			bQuit = FALSE
			RETURN TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF NETWORK_SESSION_IS_DISPLAYING_INVITE_CONFIRMATION()
			PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - ignore (cancel) due to NETWORK_SESSION_IS_DISPLAYING_INVITE_CONFIRMATION")
		ENDIF
		IF IS_BIT_SET(iButtonBS, FM_PRESSED_BS_CIRCLE)
			PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - ignore (cancel) due to IS_BIT_SET(iButtonBS, FM_PRESSED_BS_CIRCLE)")
		ENDIF		
		#ENDIF
	
		IF NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			CLEAR_BIT(iButtonBS, FM_PRESSED_BS_CIRCLE)
		ENDIF
	ENDIF
	
	// Confirm quit
	IF NOT IS_BIT_SET(iButtonBS, FM_PRESSED_BS_ACCEPT)
	AND NOT NETWORK_SESSION_IS_DISPLAYING_INVITE_CONFIRMATION()
		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		
			PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - accept pressed")
								
			bQuit = TRUE
			
			// Make sure any freeze is removed
			IF GET_SKYFREEZE_STAGE() != SKYFREEZE_NONE
			AND NOT bFreezeScreen
			AND bIsHeist
				PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - accept pressed, clear frozen screen")
				SET_SKYFREEZE_CLEAR()
			ENDIF
			
			// Set global for bad sport behaviour
			SET_TRANSITION_SESSIONS_PLAYER_QUIT_FROM_HUD()
			
			RETURN TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF NETWORK_SESSION_IS_DISPLAYING_INVITE_CONFIRMATION()
			PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - ignore (accept) due to NETWORK_SESSION_IS_DISPLAYING_INVITE_CONFIRMATION")
		ENDIF
		IF IS_BIT_SET(iButtonBS, FM_PRESSED_BS_ACCEPT)
			PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - ignore (accept) due to IS_BIT_SET(iButtonBS, FM_PRESSED_BS_ACCEPT)")
		ENDIF	
		#ENDIF
	
		IF NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			CLEAR_BIT(iButtonBS, FM_PRESSED_BS_ACCEPT)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CONTROL_FM_MISSION_PURCHASE_AMMO_CONFIRMATION(	BOOL 		&bPurchased,
															INT 		&iButtonBS,
															INT 		iAmount)
		
	SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS("FM_CSC_QUIT", "FM_COR_PWAR", (FE_WARNING_YES | FE_WARNING_NO), "", TRUE, iAmount)
	
	// Cancel quit
	IF NOT IS_BIT_SET(iButtonBS, FM_PRESSED_BS_CIRCLE)
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			
			PRINTLN("[CORONA] CONTROL_FM_MISSION_PURCHASE_AMMO_CONFIRMATION - cancel pressed")
								
			bPurchased = FALSE
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			CLEAR_BIT(iButtonBS, FM_PRESSED_BS_CIRCLE)
		ENDIF
	ENDIF
	
	// Confirm quit
	IF NOT IS_BIT_SET(iButtonBS, FM_PRESSED_BS_ACCEPT)
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			
			PRINTLN("[CORONA] CONTROL_FM_MISSION_PURCHASE_AMMO_CONFIRMATION - accept pressed")
								
			bPurchased = TRUE			
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			CLEAR_BIT(iButtonBS, FM_PRESSED_BS_ACCEPT)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CONTROL_FM_MISSION_CHALLENGE_WARNING(	BOOL &bConfirmed, INT &iButtonBS)
			
	SET_WARNING_MESSAGE_WITH_HEADER("FM_CSC_QUIT", "FM_COR_WARUC", FE_WARNING_BACK | FE_WARNING_OK)
	
	// Cancel quit
	IF NOT IS_BIT_SET(iButtonBS, FM_PRESSED_BS_CIRCLE)
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			
			PRINTLN("[CORONA] CONTROL_FM_MISSION_CHALLENGE_WARNING - cancel pressed")
								
			bConfirmed = FALSE
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			CLEAR_BIT(iButtonBS, FM_PRESSED_BS_CIRCLE)
		ENDIF
	ENDIF
	
	// Confirm quit
	IF NOT IS_BIT_SET(iButtonBS, FM_PRESSED_BS_ACCEPT)
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			
			PRINTLN("[CORONA] CONTROL_FM_MISSION_CHALLENGE_WARNING - accept pressed")
								
			bConfirmed = TRUE			
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			CLEAR_BIT(iButtonBS, FM_PRESSED_BS_ACCEPT)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Process the transition across to the intro screen
PROC PROCESS_TRANSITION_FROM_HOST_JOIN_SCREEN()

	PRINTLN("[CORONA] PROCESS_TRANSITION_FROM_HOST_JOIN_SCREEN - Hide columns and show spinner")

	SET_FRONTEND_TAB_TEXT(0, "")
	SET_FRONTEND_TAB_TEXT(1, "")
	
	// Display relevant columns
	SET_FRONTEND_DISPLAY_COLUMN(0, FALSE)
	
	DISPLAY_CORONA_INSTRUCTIONAL_BUTTONS(FALSE)
	
	PAUSE_MENU_SET_BUSY_SPINNER(TRUE, 0)

ENDPROC

/// PURPOSE: Maintai the loading of our photo
PROC MAINTAIN_CORONA_INTRO_SCREEN_PHOTO(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails, STRUCT_DL_PHOTO_VARS &sDownLoadPhotoVars)
		
	IF NOT IS_CORONA_BIT_SET(CORONA_PHOTO_HAS_BEEN_INITIALISED)
			
		TEXT_LABEL_31 tlTxd = ""
		TEXT_LABEL_31 tlTxn = ""
		INT iLoadInt = 1
		INT iJobType = 0
		
		IF IS_THIS_A_MINI_GAME(sLaunchMissionDetails.iMissionType)
			IF NOT sLaunchMissionDetails.bPhotoLoaded 
				EXIT
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
		AND sLaunchMissionDetails.bPhotoLoaded
				
						
			PRINTLN("[CORONA] MAINTAIN_CORONA_INTRO_SCREEN_PHOTO - Photo loaded, sDownLoadPhotoVars.tlContentID_DownLoaded = ", sDownLoadPhotoVars.tlContentID_DownLoaded)
								
			IF NOT IS_STRING_NULL_OR_EMPTY(sDownLoadPhotoVars.tlContentID_DownLoaded)
				tlTxd	= sDownLoadPhotoVars.tlContentID_DownLoaded
				tlTxn	= sDownLoadPhotoVars.tlContentID_DownLoaded
				iLoadInt = 0
			ELSE
				IF GET_HASH_OF_MAP_AREA_AT_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID())) = MAP_AREA_CITY
					tlTxd = "MPCarHUD"
					tlTxn = "City"
				ELSE
					tlTxd = "MPCarHUD"
					tlTxn = "Country_side"
				ENDIF
				iLoadInt = 1
			ENDIF
			
			PRINTLN("[CORONA] MAINTAIN_CORONA_INTRO_SCREEN_PHOTO - Photo loaded, passing to scaleform: Dict: ", tlTxd, " Name: ", tlTxn)
			
			SET_FRONTEND_DETAILS_SPINNER(CORONA_COLUMN_TWO, FALSE)
			
			IF IS_THIS_A_MINI_GAME(sLaunchMissionDetails.iMissionType)
				SET_FRONTEND_DETAILS_TITLE(CORONA_COLUMN_TWO, "", GET_CORONA_MINIGAME_TITLE(sLaunchMissionDetails.iMissionType), 0, tlTxd, tlTxn, FALSE, iLoadInt, sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.iCoronaMissionSubType, sLaunchMissionDetails.stFileName, DEFAULT, g_FMMC_STRUCT.iRootContentIDHash)
			ELSE
			
				IF IS_CURRENT_MISSION_ROCKSTAR_VERIFIED()
				AND NOT CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_COMMUNITY_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					PRINTLN("[CORONA] MAINTAIN_CORONA_INTRO_SCREEN_PHOTO - IS_CURRENT_MISSION_ROCKSTAR_VERIFIED = TRUE. Show this over photo")
					iJobType = 1
				ENDIF
			
				SET_FRONTEND_DETAILS_TITLE(CORONA_COLUMN_TWO, "", sLaunchMissionDetails.stMissionName, iJobType, tlTxd, tlTxn, TRUE, iLoadInt,  sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.iCoronaMissionSubType, sLaunchMissionDetails.stFileName, DEFAULT, g_FMMC_STRUCT.iRootContentIDHash)
			ENDIF
			
			SET_CORONA_BIT(CORONA_PHOTO_HAS_BEEN_INITIALISED)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Function to initialise and maintain the pre corona host screen. 
FUNC BOOL MAINTAIN_CORONA_HOST_SCREEN(	MISSION_TO_LAUNCH_DETAILS 	&sLaunchMissionDetails, 
										MP_MISSION_ID_DATA 			&theMissionIDData, 
										STRUCT_DL_PHOTO_VARS 		&sDownLoadPhotoVars,
										BOOL 						&bQuit,
										BOOL 						&bToggleRenderPhasesNextFrame)

	#IF IS_DEBUG_BUILD
		IF GET_QUICKLAUNCH_STATE() != QUICKLAUNCH_EMPTY
			PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - GET_QUICKLAUNCH_STATE() != QUICKLAUNCH_EMPTY (EXIT)")
			SET_FRONTEND_ACTIVE(FALSE)
			RETURN FALSE
		ENDIF
	#ENDIF
	
	// If player is idling then they get kicked.
	IF IS_CORONA_SET_TO_CLEANUP()
		PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - IS_CORONA_SET_TO_CLEANUP() cleanup corona")
		SET_FRONTEND_ACTIVE(FALSE)
		RETURN FALSE
	ENDIF

	
	FRONTEND_MENU_SCREEN currentScreen
	FRONTEND_MENU_SCREEN nextScreen
	INT iMenuID

	SWITCH g_iPreCoronaHostScreenState
	
		CASE CORONA_PRE_HOST_SCREEN_INIT
			IF NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_PAUSE_MENU_RESTARTING()
			
				PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - Request host screen. ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_CORONA, FALSE)")
				ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_CORONA, FALSE)
				
				VECTOR		vFocusPos
				BOOL		bSetFocus
				SETUP_CORONA_CAMERA(g_sTransitionSessionData.ciCam, sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.iMissionVariation, sLaunchMissionDetails.vStartPoint, 
									sLaunchMissionDetails.vCamPos, sLaunchMissionDetails.vCamHead, sLaunchMissionDetails.bIsContactMission,
									 (sLaunchMissionDetails.bIsHeistMission OR sLaunchMissionDetails.bIsHeistPlanning), 
									vFocusPos, bSetFocus, FALSE)

				g_iPreCoronaHostScreenState = CORONA_PRE_HOST_SCREEN_WAIT
			ELSE
				PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - Frontend was active, need to kill before requesting")
				SET_FRONTEND_ACTIVE(FALSE)
			ENDIF
		BREAK
		
		CASE CORONA_PRE_HOST_SCREEN_WAIT
			IF NOT bToggleRenderPhasesNextFrame		
				PRINTLN("[TS][CORONA] bToggleRenderPhasesNextFrame - calling TOGGLE_RENDERPHASES(FALSE)")
				TOGGLE_RENDERPHASES(FALSE)
				bToggleRenderPhasesNextFrame = TRUE
			ENDIF
			IF IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_PAUSE_MENU_RESTARTING()
				IF IS_FRONTEND_READY_FOR_CONTROL()
					
					IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
												
						GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(currentScreen, nextScreen, iMenuID)
					
						PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()")
						PRINTLN("[CORONA]		iCurrentScreen = ", GET_XML_SCREEN_STRING_VALUE(currentScreen))
						PRINTLN("[CORONA]		iNextScreen = ", GET_XML_SCREEN_STRING_VALUE(nextScreen))
						PRINTLN("[CORONA]		iMenuID = ", iMenuID)						
						
						IF nextScreen = MENU_UNIQUE_ID_CORONA_SETTINGS
						OR currentScreen = MENU_UNIQUE_ID_CORONA_SETTINGS
							
							TAKE_CONTROL_OF_FRONTEND()
							LOCK_MOUSE_SUPPORT(FALSE) // Disable header mouse interaction
							
							CLEAR_FRONTEND_DATA_SLOT(0)
							
							INT iSelection
							SET_FRONTEND_DATA_SLOT(0, CORONA_JOIN_EXISTING_INSTANCE, 	ENUM_TO_INT(MENU_UNIQUE_ID_CORONA_SETTINGS_LIST) + 1000, CORONA_JOIN_EXISTING_INSTANCE, "FM_JOIN_CORONA", FALSE, 2)
							iSelection = CORONA_JOIN_EXISTING_INSTANCE
							
							SET_FRONTEND_DATA_SLOT(0, CORONA_HOST_NEW_INSTANCE, 		ENUM_TO_INT(MENU_UNIQUE_ID_CORONA_SETTINGS_LIST) + 1000, CORONA_HOST_NEW_INSTANCE, 		"FM_HOST_CORONA", FALSE, 2)

							DISPLAY_FRONTEND_DATA_SLOT(0)
							
							SET_FRONTEND_TAB_TEXT(0, "PM_SCR_VHI")
							SET_FRONTEND_TAB_TEXT(1, "")
							
							// Display relevant columns
							SET_FRONTEND_DISPLAY_COLUMN(0, TRUE)
							SET_FRONTEND_DISPLAY_COLUMN(1, FALSE)
							SET_FRONTEND_DISPLAY_COLUMN(2, FALSE)
							SET_FRONTEND_DISPLAY_COLUMN(3, FALSE)
														
							// Place focus in column and lock player
							SET_FRONTEND_COLUMN_FOCUS(0, TRUE)
							SET_FRONTEND_COLUMN_AS_LOCKED(0, TRUE)
							
							// Hide right hand data
							SHOW_FRONTEND_HEADING_DETAILS(FALSE)
							
							CORONA_SET_HEADER_TITLE(sLaunchMissionDetails)
							
							DISPLAY_CORONA_HOST_SCREEN_HELP(sLaunchMissionDetails.iMissionType, iSelection)
							
							DISPLAY_CORONA_HOST_SCREEN_DETAILS(sLaunchMissionDetails)
							DISPLAY_FRONTEND_DATA_SLOT(1)
							SET_FRONTEND_DISPLAY_COLUMN(1, TRUE)
							
							// Don't allow us to quit if we are skyswooping
							IF NOT IS_SKYSWOOP_AT_GROUND()
								PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - Player is skyswooping - IS_SKYSWOOP_AT_GROUND()")
								
								PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CORONA_SKYSWOOP"))
								
								SET_CORONA_BIT(CORONA_PLAYER_IN_SKYSWOOP)	
							ENDIF
							
							PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CORONA_HOST_SCREEN"))
							DISPLAY_CORONA_INSTRUCTIONAL_BUTTONS(TRUE)
							
							RELEASE_CONTROL_OF_FRONTEND()
							
							g_iPreCoronaHostScreenState = CORONA_PRE_HOST_SCREEN_INPUT_SKIP
						ENDIF
					ENDIF
				ENDIF
				
			#IF IS_DEBUG_BUILD
			ELSE
			
				PRINTLN("[CORONA] FM_MISSION_INTRO_SCREEN_WAIT_FOR_FRONTEND - WAITING...")
				
				IF NOT IS_PAUSE_MENU_ACTIVE()
					PRINTLN("	 - IS_PAUSE_MENU_ACTIVE() = FALSE")
				ENDIF
				
				IF IS_PAUSE_MENU_RESTARTING()
					PRINTLN("	 - IS_PAUSE_MENU_RESTARTING() = FALSE")
				ENDIF
			
			#ENDIF
							
			ENDIF
		BREAK
				
		CASE CORONA_PRE_HOST_SCREEN_INPUT_SKIP
		
			IF HAS_MENU_TRIGGER_EVENT_OCCURRED()
				GET_MENU_TRIGGER_EVENT_DETAILS(currentScreen, iMenuID)
				
				PRINTLN("[CORONA] CORONA_PRE_HOST_SCREEN_INPUT_SKIP - HAS_MENU_TRIGGER_EVENT_OCCURRED()")
				PRINTLN("[CORONA]		iCurrentScreen = ", GET_XML_SCREEN_STRING_VALUE(currentScreen))
				PRINTLN("[CORONA]		iMenuID = ", iMenuID)
				
				g_iPreCoronaHostScreenState = CORONA_PRE_HOST_SCREEN_MAINTAIN
			#IF IS_DEBUG_BUILD
			ELSE
				IF GET_GAME_TIMER() % 1000 < 50
					PRINTLN("[CORONA] CORONA_PRE_HOST_SCREEN_INPUT_SKIP - waiting for HAS_MENU_TRIGGER_EVENT_OCCURRED()")
				ENDIF
			#ENDIF
			
			ENDIF
		BREAK
		
		CASE CORONA_PRE_HOST_SCREEN_MAINTAIN
		
			// Attempt to set the photo if we can
			MAINTAIN_CORONA_INTRO_SCREEN_PHOTO(sLaunchMissionDetails, sDownLoadPhotoVars)
			
			IF MAINTAIN_CORONA_SKYSWOOP_STATE()
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CORONA_SKYSWOOP"))
				PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
			ENDIF
		
			// Listen for the player moving up and down in menu
			IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
						
				GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(currentScreen, nextScreen, iMenuID)
			
				PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()")
				PRINTLN("[CORONA]		iCurrentScreen = ", GET_XML_SCREEN_STRING_VALUE(currentScreen))
				PRINTLN("[CORONA]		iNextScreen = ", GET_XML_SCREEN_STRING_VALUE(nextScreen))
				PRINTLN("[CORONA]		iMenuID = ", iMenuID)
					
				DISPLAY_CORONA_HOST_SCREEN_HELP(sLaunchMissionDetails.iMissionType, iMenuID)
			ENDIF
			
			// If player quits then kill frontend and simulate walk out
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				IF NOT IS_CORONA_BIT_SET(CORONA_PLAYER_IN_SKYSWOOP)
				
					PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) player has QUIT")
					
					g_iPreCoronaHostScreenState = CORONA_PRE_HOST_SCREEN_QUIT
					
					RETURN FALSE
				ENDIF
			ENDIF
			
			// Listen for the player pressing Select on an option
			IF HAS_MENU_TRIGGER_EVENT_OCCURRED()
			
				GET_MENU_TRIGGER_EVENT_DETAILS(currentScreen, iMenuID)
				
				PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - HAS_MENU_TRIGGER_EVENT_OCCURRED()")
				PRINTLN("[CORONA]		iCurrentScreen = ", GET_XML_SCREEN_STRING_VALUE(currentScreen))
				PRINTLN("[CORONA]		iMenuID = ", iMenuID)
				
				// Check which option has been selected
				IF iMenuID = CORONA_HOST_NEW_INSTANCE
				
					//  Set global flag that this will be a new session
					SET_TRANSITION_SESSIONS_HOST_CORONA()
					//Set the job entry type
					SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_HOST_CORONA)	
					// Also setting the other flags too now
					SET_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA()
					CLEAR_TRANSITION_SESSIONS_DO_QUICK_MATCH_CORONA()			
					PRINTLN("[CORONA]...KGM MP [At Coords]: SHOULD_TRANSITION_SESSIONS_HOST_CORONA = TRUE hosting my own instance of the corona")
					PRINTLN("[TS] MAINTAIN_CORONA_HOST_SCREEN() - SHOULD_TRANSITION_SESSIONS_HOST_CORONA = TRUE")						
					//Clear the joining local flag
					IF TRANSITION_SESSIONS_JOINING_LOCAL_ON_QUICK_MATCH_CORONA()
						CLEAR_TRANSITION_SESSIONS_JOINING_LOCAL_ON_QUICK_MATCH_CORONA()
					ENDIF				
					
				ELIF iMenuID = CORONA_JOIN_EXISTING_INSTANCE
				OR iMenuID = CORONA_JOIN_EXISTING_PLAYER_INSTANCE
					// Clear flag so we attempt to join an existing session
					CLEAR_TRANSITION_SESSIONS_HOST_CORONA()
					
					//Set the job entry type
					SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_JOIN_CORONA)		
					IF NOT (Is_This_Mission_Request_Active_And_Joinable(theMissionIdData))
						// Local mission instance isn't joinable
						SET_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA()
						SET_TRANSITION_SESSIONS_DO_QUICK_MATCH_CORONA()		
						//Clear the joining local flag
						IF TRANSITION_SESSIONS_JOINING_LOCAL_ON_QUICK_MATCH_CORONA()
							CLEAR_TRANSITION_SESSIONS_JOINING_LOCAL_ON_QUICK_MATCH_CORONA()
						ENDIF
						PRINTLN("[CORONA]...KGM MP [At Coords]: Is_This_Mission_Request_Active_And_Joinable = FALSE going to quick match into my own instance of the corona")
						PRINTLN("[TS] MAINTAIN_CORONA_HOST_SCREEN() - Is_This_Mission_Request_Active_And_Joinable = FALSE")
					
					ELSE
						// Local mission instance is joinable
						CLEAR_TRANSITION_SESSIONS_DO_QUICK_MATCH_CORONA()
						CLEAR_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA()
						SET_TRANSITION_SESSIONS_JOINING_LOCAL_ON_QUICK_MATCH_CORONA()
						PRINTLN("[CORONA]...KGM MP [At Coords]: joining local instance of the mission as a client")
						PRINTLN("[TS] Reserve_MissionsAtCoords_Mission_With_Mission_Controller - Is_This_Mission_Request_Active_And_Joinable = TRUE")
					ENDIF
										
				ENDIF
				
				PROCESS_TRANSITION_FROM_HOST_JOIN_SCREEN()
				
				// Play confirm sound
				PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FREEMODE_SOUNDSET")
				
				IF NOT IS_PAUSE_MENU_RESTARTING()
					PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - Clean up column and remove focus")
					CLEAR_FRONTEND_DATA_SLOT(0)
					SET_FRONTEND_COLUMN_FOCUS(0, FALSE, FALSE)
				ENDIF
				
				RETURN TRUE
			ENDIF
		
		BREAK
		
		CASE CORONA_PRE_HOST_SCREEN_QUIT
			INT iButtonBS
				
			IF CONTROL_FM_MISSION_WARNING_SCREEN(bQuit, iButtonBS)
				
				IF bQuit
					PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - CONTROL_FM_MISSION_WARNING_SCREEN(bQuit, iButtonBS)")					
					RETURN TRUE
				ELSE
					PRINTLN("[CORONA] MAINTAIN_CORONA_HOST_SCREEN - Player not quit, return to MAINTAIN")
					g_iPreCoronaHostScreenState = CORONA_PRE_HOST_SCREEN_MAINTAIN
				ENDIF
				
			ENDIF
		BREAK
			
	ENDSWITCH


	RETURN FALSE
ENDFUNC


/// PURPOSE: Resets the global vars for 
PROC RESET_CORONA_HOST_SCREEN_VARS()
	g_iPreCoronaHostScreenState 	= CORONA_PRE_HOST_SCREEN_INIT
ENDPROC


// ************************** CORONA WEAPON COMMANDS *************************

/// ********************************************
///    		CONTACT MISSION DUMMY CORONA
/// ********************************************


/// PURPOSE: Returns the vector in the world of our temp corona that we can use. Using MAX_TEMP_CORONA_LOCATIONS.
FUNC VECTOR GET_TEMP_CORONA_LOCATION_FOR_CONTACT_MISSION(INT iCoronaPos)
	VECTOR vAreaToLoad
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
		vAreaToLoad = GANGOPS_GET_GANG_OPS_BOARD_START_POS()
		RETURN vAreaToLoad 
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		vAreaToLoad = GET_CASINO_HEIST_BOARD_START_POS()
		RETURN vAreaToLoad 
	ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		vAreaToLoad = GET_HEIST_ISLAND_BOARD_START_POS()
		RETURN vAreaToLoad 
	ENDIF
	#ENDIF
	
	IF IS_THIS_A_TUNER_ROBBERY_FINALE()
		vAreaToLoad = GET_TUNER_BOARD_START_POS()
		RETURN vAreaToLoad 
	ENDIF
	
	IF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ()
		vAreaToLoad = GET_FIXER_STORY_START_POS()
		RETURN vAreaToLoad 
	ENDIF
	
	IF IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		vAreaToLoad = GET_ULP_MISSION_LOCATION()
		RETURN vAreaToLoad 
	ENDIF
	
	SWITCH iCoronaPos
		CASE 0		RETURN <<	-756.4	, 	5538.6	, 	56.3	>>			// Pala spings cable car parking lot
		CASE 1		RETURN <<	2197.8	,	5571.1	,	75.6	>>			// Rural house
		CASE 2		RETURN <<	311.4	,	4344.9	,	50.5	>>			// Grassy verge by r/way bridge
		CASE 3		RETURN <<	3275.4	,	5149	,	30.3	>>			// Lighthouse home
		CASE 4		RETURN <<	1632.4	,	4738.6	,	55.3	>>			// Dirt j/o freeway
		CASE 5		RETURN <<	41.9	,	2803.3	,	57.9	>>			// Abandoned gas station
		CASE 6		RETURN <<	2544.9	,	2621.5	,	37.9	>>			// Pink T-Rex gas station
		CASE 7		RETURN <<	721.5	,	1282.1	,	360.3	>>			// Vinewood sign 
		CASE 8		RETURN <<	-1911.3	,	2037.7	,	140.7	>>			// Vineyard parking lot
		CASE 9		RETURN <<	2585	,	417.9	,	108.5	>>			// Freeway services parking lot
		CASE 10		RETURN <<	-1694.1	,	-297.2	,	51.8	>>			// Cemetary
		CASE 11		RETURN <<	278.9	,	944.2	,	211.1	>>			// Reservoir
		CASE 12		RETURN <<	876.7	,	-33.4	,	78.8	>>			// Racecourse parking lot
		CASE 13		RETURN <<	-241.3	,	-264.8	,	49.1	>>			// Rockford Plaza
		CASE 14		RETURN <<	-1484.9	,	-1219.6	,	2.7		>>			// Beach
		CASE 15		RETURN <<	-179.8	,	-1374.7	,	31.3	>>			// Glass Heroes
		CASE 16		RETURN <<	731		,	-2020	,	29.3	>>			// Power station
		CASE 17		RETURN <<	796.6	,	-3279.8	,	11.9	>>			// Cargo dock near entrance
		CASE 18		RETURN <<	-263.2	,	-1895.7	,	27.8	>>			// Stadium entrance
		CASE 19		RETURN <<	-923	,	-2439.6	,	13.8	>>			// Airport area
	ENDSWITCH
	
	RETURN <<0, 0, 0>>
ENDFUNC

/// PURPOSE: Finds the closest vector location of the contact mission   
FUNC VECTOR GET_NEAREST_TEMP_CORONA_LOCATION(VECTOR &vContactMissionLocation)

	INT i
	INT iClosestVector
	INT iMinDistance = 5000
	INT iCurrentDistance 
	VECTOR vCurrentLoc
	
	REPEAT MAX_TEMP_CORONA_LOCATIONS i
	
		vCurrentLoc = GET_TEMP_CORONA_LOCATION_FOR_CONTACT_MISSION(i)
		iCurrentDistance = ABSI(ROUND(GET_DISTANCE_BETWEEN_COORDS(vContactMissionLocation, vCurrentLoc, FALSE)))
		
		IF iCurrentDistance < iMinDistance
			iClosestVector = i
			iMinDistance = iCurrentDistance
		ENDIF
		
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	vCurrentLoc = GET_TEMP_CORONA_LOCATION_FOR_CONTACT_MISSION(iClosestVector)
	PRINTLN("[CORONA] GET_NEAREST_TEMP_CORONA_LOCATION - Nearest point to host location (", vContactMissionLocation, ") is: iIndex = ", iClosestVector, ", pos = ", vCurrentLoc)
	#ENDIF
	
	RETURN GET_TEMP_CORONA_LOCATION_FOR_CONTACT_MISSION(iClosestVector)
ENDFUNC

/// PURPOSE: Empties our coords for next mission
PROC SET_PLAYER_CONTACT_MISSION_COORDS_TO_ZERO()
	PRINTLN("[CORONA] SET_PLAYER_CONTACT_MISSION_COORDS_TO_ZERO - Clear our start vector for contact missions")
	g_TransitionSessionNonResetVars.PlayerMissionStartLocation = <<0, 0, 0>>
ENDPROC


/// PURPOSE: Returns TRUE if the mission is to start in at specified location or current corona is in an active area
FUNC BOOL IS_CONTACT_MISSION_USING_FORCED_START_LOCATION(VECTOR vStartPos)

	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_START_AT_ALT_LOC)
		RETURN TRUE
	ENDIF
	
	IF IS_POSITION_IN_MISSION_RESTRICTED_AREA(vStartPos)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Grabs the correct loaded z of the corona in the apartment
PROC UPDATE_CORONA_TO_ACCURATE_INTERIOR_POSITION(VECTOR &vStartPoint)
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
		IF IS_PROPERTY_ONLY_A_GARAGE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			vStartPoint = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].garage.vSafeCoronaLoc
			PRINTLN("[CORONA] UPDATE_CORONA_TO_ACCURATE_INTERIOR_POSITION - MISSION, get location of interior (GARAGE) safe location: ", vStartPoint)
		ELSE
			vStartPoint = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vSafeCoronaLoc
			PRINTLN("[CORONA] UPDATE_CORONA_TO_ACCURATE_INTERIOR_POSITION - MISSION, get location of interior (HOUSE) safe location: ", vStartPoint)
			IF ARE_VECTORS_EQUAL(vStartPoint, <<0,0,0>>)
				vStartPoint = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].entrance[0].vBuzzerLoc
				PRINTLN("[CORONA] UPDATE_CORONA_TO_ACCURATE_INTERIOR_POSITION - MISSION, get location of interior (BUZZER) safe location: ", vStartPoint)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Update our start point if we need to use a dummy corona
PROC SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES(VECTOR &vStartPoint, BOOL bContactMission)

	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	AND NOT IS_ARENA_WARS_JOB()
	AND NOT IS_THIS_A_TUNER_ROBBERY_FINALE()
	AND NOT IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ()
	AND NOT IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
	#IF FEATURE_DLC_2_2022
	AND NOT IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
	#ENDIF
		IF bContactMission
		
			IF IS_THIS_A_TEAM_MISSION_WITH_BALANCING_OFF(g_sTransitionSessionOptions)
				
				VECTOR vIncomingVector = vStartPoint
				
				IF g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_ADHOC_CORONA] = 1
					vStartPoint = g_TransitionSessionNonResetVars.PlayerMissionStartLocation
					PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Use host location for corona, vector: ", vStartPoint)
				ELSE
					vStartPoint = GET_NEAREST_TEMP_CORONA_LOCATION(g_TransitionSessionNonResetVars.PlayerMissionStartLocation)
					PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Contact Mission with balancing off so use a dummy corona, vector: ", vStartPoint)
				ENDIF
				
				FLOAT fGroundZ
				
				IF NOT IS_CORONA_CONTACT_MISSION_LAUNCHING_IN_APARTMENT()
					IF GET_GROUND_Z_FOR_3D_COORD(vStartPoint, fGroundZ)
					
						PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Updating ground z to: ", fGroundZ)
						vStartPoint.z = fGroundZ
					ENDIF
				ENDIF
				
				IF IS_CONTACT_MISSION_USING_FORCED_START_LOCATION(vStartPoint)
					PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - IS_CONTACT_MISSION_USING_FORCED_START_LOCATION = TRUE, Setting dummy corona loc to zero, reset to original vector: ", vIncomingVector)
					SET_PLAYER_CONTACT_MISSION_COORDS_TO_ZERO()
					vStartPoint = vIncomingVector
				ENDIF			
								
				// Override our leaving vector so we warp the player back to the correct location
				SET_PLAYER_LEAVING_CORONA_VECTOR(g_TransitionSessionNonResetVars.PlayerMissionStartLocation)
				PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Override leaving vector, GET_PLAYER_LEAVING_CORONA_VECTOR() = ", GET_STRING_FROM_VECTOR(GET_PLAYER_LEAVING_CORONA_VECTOR()))
			ENDIF
		ENDIF
	ELIF IS_ARENA_WARS_JOB()
		vStartPoint = <<2825.4329, -3898.2473, 138.9586>>
//		vStartPoint = <<2825.4329, -3850.7473, 138.99998>>
		PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Arena wars job, vector: ", vStartPoint)
		DEBUG_PRINTCALLSTACK()
		
//		SET_PLAYER_LEAVING_CORONA_VECTOR(g_TransitionSessionNonResetVars.PlayerMissionStartLocation)
//		PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Arena wars job, override leaving vector, GET_PLAYER_LEAVING_CORONA_VECTOR() = ", GET_PLAYER_LEAVING_CORONA_VECTOR())
	
	ELIF IS_THIS_A_TUNER_ROBBERY_FINALE()
	
		vStartPoint = GET_TUNER_BOARD_START_POS()
		PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Tuner robbery, vector: ", vStartPoint)
		DEBUG_PRINTCALLSTACK()
		
	ELIF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ()
	
		vStartPoint = GET_FIXER_STORY_START_POS()
		PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Fixer mission, vector: ", vStartPoint)
		DEBUG_PRINTCALLSTACK()
		
	ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		
		IF NETWORK_IS_ACTIVITY_SESSION()
			INT iArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_FMMC_STRUCT.iRootContentIDHash)
			IF iArrayPos != -1
				vStartPoint = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].vStartPos
				PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Cayo Perico series - Job is array pos ", iArrayPos, ", vector: ", vStartPoint )			
			ELSE	
				PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Cayo Perico series - iArrayPos = -1")			
			ENDIF
		ELSE
			vStartPoint = GET_CAYO_PERICO_SERIES_START_POS()
			PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Cayo Perico series, vector: ", vStartPoint)
			DEBUG_PRINTCALLSTACK()
		ENDIF
		
	ELIF IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		
		vStartPoint = GET_ULP_MISSION_LOCATION()
		PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - ULP mission, vector: ", vStartPoint)
		DEBUG_PRINTCALLSTACK()
		
	#IF FEATURE_DLC_2_2022
	ELIF IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		
		vStartPoint = GET_XMAS22_STORY_START_POS()
		PRINTLN("[CORONA] SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES - Xmas22 Story mission, vector: ", vStartPoint)
		DEBUG_PRINTCALLSTACK()
		
	#ENDIF
		
	ENDIF
	


ENDPROC


///Sets the label for the weapon options to either 'Starting Weapons' or just 'Weapons'
FUNC STRING GET_CORONA_STARTING_WEAPON_LABEL()
	//No starting weapons for Heists or Heist planning missions
	IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG) 
		RETURN "FM_COR_WPN"
	ENDIF
	
	RETURN "FM_BET_WPN"
ENDFUNC





