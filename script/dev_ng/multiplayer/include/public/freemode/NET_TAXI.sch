USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_entity.sch"
USING "commands_interiors.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_vehicle.sch"
USING "commands_zone.sch"

USING "script_player.sch"

USING "waiting_taxi.sch"
USING "dialogue_public.sch"


FUNC BOOL IS_COORD_IN_COUNTRYSIDE_MP(VECTOR thisCoord)
	STRING tempZone
	//IF NOT IS_PED_INJURED(PlayerPedId)
		printDebugString("<")printDebugString(GET_THIS_SCRIPT_NAME())printDebugString("> - GET_NAME_OF_ZONE(thisCoord) = ")
		tempZone = GET_NAME_OF_ZONE(thisCoord)
		printDebugString(tempZone)
		printDebugString("\n")
		IF ARE_STRINGS_EQUAL("Alamo", tempZone)
		OR ARE_STRINGS_EQUAL("COSI", tempZone)
		OR ARE_STRINGS_EQUAL("Desrt", tempZone)
		OR ARE_STRINGS_EQUAL("GOcean", tempZone)
		OR ARE_STRINGS_EQUAL("Harmo", tempZone)
		OR ARE_STRINGS_EQUAL("SANDY", tempZone)
		OR ARE_STRINGS_EQUAL("Senora", tempZone)
		OR ARE_STRINGS_EQUAL("WindF", tempZone)
		OR ARE_STRINGS_EQUAL("Zancudo", tempZone)
		OR ARE_STRINGS_EQUAL("Lago", tempZone)
		OR ARE_STRINGS_EQUAL("CANNY", tempZone)
		OR ARE_STRINGS_EQUAL("RTRAK", tempZone)
		OR ARE_STRINGS_EQUAL("CHU", tempZone)
		OR ARE_STRINGS_EQUAL("CHIL", tempZone)
		OR ARE_STRINGS_EQUAL("GrapeS", tempZone)
		OR ARE_STRINGS_EQUAL("MTJose", tempZone)
		OR ARE_STRINGS_EQUAL("Paleto", tempZone)
		OR ARE_STRINGS_EQUAL("Lago", tempZone)
		OR ARE_STRINGS_EQUAL("CANNY", tempZone)
		OR ARE_STRINGS_EQUAL("RTRAK", tempZone)
		OR ARE_STRINGS_EQUAL("CHU", tempZone)
		OR ARE_STRINGS_EQUAL("CHIL", tempZone)
		OR ARE_STRINGS_EQUAL("GrapeS", tempZone)
		OR ARE_STRINGS_EQUAL("MTJose", tempZone)
		OR ARE_STRINGS_EQUAL("GALLI", tempZone)
		
			RETURN TRUE
		ENDIF
	//ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if the passed in vector is within a special creation area (such as the airport pickup zone)
FUNC BOOL ARE_COORDS_IN_SPECIAL_CREATION_AREA(VECTOR vCoords)
	
	// Airport - Upper Taxi Rank FIRST		
	IF IS_POINT_IN_ANGLED_AREA(vCoords, <<-1061.018066,-2540.235107,18.669323>>, <<-1023.332458,-2562.114502,31.576157>>, 250.0)
		NET_PRINT_TIME() NET_PRINT(" -- TAXI - ARE_COORDS_IN_SPECIAL_CREATION_AREA - Airport - Upper Taxi Rank FIRST") NET_NL()		
		RETURN TRUE
	// Airport - Lower Taxi Rank FIRST
	ELIF IS_POINT_IN_ANGLED_AREA(vCoords, <<-1061.058716,-2540.253662,10.944668>>, <<-1023.296814,-2562.052490,18.516848>>, 250.0)
		NET_PRINT_TIME() NET_PRINT(" -- TAXI - ARE_COORDS_IN_SPECIAL_CREATION_AREA - Airport - Lower Taxi Rank FIRST") NET_NL()		
		RETURN TRUE
		
	// Airport - Upper Taxi Rank SECOND
	ELIF IS_POINT_IN_ANGLED_AREA(vCoords, <<-1043.364136,-2747.290039,16.960567>>, <<-991.536804,-2657.681641,69.123489>>, 200.0)
		NET_PRINT_TIME() NET_PRINT(" -- TAXI - ARE_COORDS_IN_SPECIAL_CREATION_AREA - Airport - Upper Taxi Rank SECOND") NET_NL()		
		RETURN TRUE
	// Airport - Lower Taxi Rank SECOND
	ELIF IS_POINT_IN_ANGLED_AREA(vCoords, <<-1047.646484,-2754.947021,2.941969>>, <<-991.731445,-2653.153320,16.892282>>, 200.0)
	OR IS_POINT_IN_ANGLED_AREA(vCoords, <<-1116.893433,-2675.670654,12.177572>>, <<-1183.932617,-2789.450684,38.951855>>, 53.0)
		NET_PRINT_TIME() NET_PRINT(" -- TAXI - ARE_COORDS_IN_SPECIAL_CREATION_AREA - Airport - Lower Taxi RankSECOND") NET_NL()		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if a safe location is found
FUNC BOOL GET_SCRIPTED_TAXI_CREATION_POINT(VECTOR vCoords, VECTOR &vCreationCoords, FLOAT &fCreationHeading)
	INT i, iRepeatSize
	VECTOR vPos
	FLOAT fHeading
	
	// Airport - Upper Taxi Rank - FIRST
	IF IS_POINT_IN_ANGLED_AREA(vCoords, <<-1061.018066,-2540.235107,18.669323>>, <<-1023.332458,-2562.114502,31.576157>>, 250.0)
		iRepeatSize = 5
		REPEAT iRepeatSize i
			SWITCH i
				CASE 0	vPos = <<-988.9172, -2428.5791, 19.0608>>		fHeading = 148.3473		BREAK
				CASE 1	vPos = <<-968.0948, -2392.3274, 19.0560>>		fHeading = 150.0203		BREAK
				CASE 2	vPos = <<-946.4792, -2419.6594, 19.0562>>		fHeading = 91.6004		BREAK
				CASE 3	vPos = <<-899.0128, -2432.8423, 19.0847>>		fHeading = 58.9819		BREAK
				CASE 4	vPos = <<-890.7079, -2272.4019, 18.2077>>		fHeading = 135.9921		BREAK
			ENDSWITCH
			
			IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos)
				vCreationCoords = vPos
				fCreationHeading = fHeading
				NET_PRINT_TIME() NET_PRINT(" -- TAXI - Airport - Upper Taxi Rank - FIRST - GET_SCRIPTED_TAXI_CREATION_POINT - vCreationCoords = ") NET_PRINT_VECTOR(vCreationCoords) NET_NL()
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
	// Airport - Lower Taxi Rank - FIRST	
	ELIF IS_POINT_IN_ANGLED_AREA(vCoords, <<-1061.058716,-2540.253662,10.944668>>, <<-1023.296814,-2562.052490,18.516848>>, 250.0)
		iRepeatSize = 5
		REPEAT iRepeatSize i
			SWITCH i
				CASE 0	vPos = <<-996.2861, -2449.9167, 12.6953>>		fHeading = 148.5654 	BREAK
				CASE 1	vPos = <<-1006.0941, -2421.4482, 12.9447>>		fHeading = 237.9966		BREAK
				CASE 2	vPos = <<-948.7274, -2403.4619, 12.7341>>		fHeading = 111.3975		BREAK
				CASE 3	vPos = <<-895.4352, -2364.8669, 13.4802>>		fHeading = 142.0129		BREAK
				CASE 4	vPos = <<-906.6598, -2419.5898, 12.6381>>		fHeading = 78.0133		BREAK
			ENDSWITCH
						
			IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos)
				vCreationCoords = vPos
				fCreationHeading = fHeading
				NET_PRINT_TIME() NET_PRINT(" -- TAXI - Airport - Lower Taxi Rank - FIRST - GET_SCRIPTED_TAXI_CREATION_POINT - vCreationCoords = ") NET_PRINT_VECTOR(vCreationCoords) NET_NL()
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
	// Airport - Upper Taxi Rank - SECOND
	ELIF IS_POINT_IN_ANGLED_AREA(vCoords, <<-1043.364136,-2747.290039,16.960567>>, <<-991.536804,-2657.681641,69.123489>>, 200.0)
		iRepeatSize = 5
		REPEAT iRepeatSize i
			SWITCH i
				CASE 0	vPos = <<-1084.0027, -2690.4734, 19.0585>>		fHeading = 213.4924		BREAK
				CASE 1	vPos = <<-1087.5422, -2660.0574, 19.0615>>		fHeading = 191.5389		BREAK
				CASE 2	vPos = <<-1083.3998, -2612.1841, 19.0622>>		fHeading = 153.7453		BREAK
				CASE 3	vPos = <<-1059.8929, -2570.6555, 19.0610>>		fHeading = 150.5092		BREAK
				CASE 4	vPos = <<-1038.3873, -2533.8455, 19.0498>>		fHeading = 150.6370		BREAK
			ENDSWITCH
			
			IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos)
				vCreationCoords = vPos
				fCreationHeading = fHeading
				NET_PRINT_TIME() NET_PRINT(" -- TAXI - Airport - Upper Taxi Rank - SECOND - GET_SCRIPTED_TAXI_CREATION_POINT - vCreationCoords = ") NET_PRINT_VECTOR(vCreationCoords) NET_NL()
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
	// Airport - Lower Taxi Rank - SECOND	
	ELIF IS_POINT_IN_ANGLED_AREA(vCoords, <<-1047.646484,-2754.947021,2.941969>>, <<-991.731445,-2653.153320,16.892282>>, 200.0)
	OR IS_POINT_IN_ANGLED_AREA(vCoords, <<-1116.893433,-2675.670654,12.177572>>, <<-1183.932617,-2789.450684,38.951855>>, 53.0)
		iRepeatSize = 5
		REPEAT iRepeatSize i
			SWITCH i
				CASE 0	vPos = <<-1076.4076, -2693.8457, 12.6509>>		fHeading = 221.0395 	BREAK
				CASE 1	vPos = <<-1121.4222, -2703.1462, 12.9444>>		fHeading = 326.1454		BREAK
				CASE 2	vPos = <<-1095.9696, -2644.1775, 12.6382>>		fHeading = 182.0544		BREAK
				CASE 3	vPos = <<-1084.0457, -2601.4124, 12.6823>>		fHeading = 153.5335		BREAK
				CASE 4	vPos = <<-1049.5753, -2589.9451, 12.6066>>		fHeading = 135.7366		BREAK
			ENDSWITCH
			
			IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos)
				vCreationCoords = vPos
				fCreationHeading = fHeading
				NET_PRINT_TIME() NET_PRINT(" -- TAXI - Airport - Lower Taxi Rank - SECOND - GET_SCRIPTED_TAXI_CREATION_POINT - vCreationCoords = ") NET_PRINT_VECTOR(vCreationCoords) NET_NL()
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE; Creates a Taxi with a Driver for Multiplayer
//FUNC BOOL CREATE_MP_TAXI(NETWORK_INDEX &niTaxi, NETWORK_INDEX &niDriver, MODEL_NAMES TaxiModel, VECTOR pos, FLOAT heading, BOOL bCreateSlightlyToRight = FALSE, BOOL bCreateAsWaiting = TRUE)
//	
//	//VEHICLE_INDEX CarID
//	//PED_INDEX DriverID
//	
//		
//	IF REQUEST_LOAD_MODEL(TaxiModel)
//	AND REQUEST_LOAD_MODEL(A_M_M_EastSA_02)
//			
//		// Create Taxi
//		//CarID = CREATE_VEHICLE(TaxiModel, pos, heading)
//		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niTaxi)
//			IF CREATE_NET_VEHICLE(niTaxi, TaxiModel, pos, heading)
//				IF (bCreateSlightlyToRight)
//					SET_ENTITY_HEADING(NET_TO_VEH(niTaxi), heading)
//					SET_ENTITY_COORDS(NET_TO_VEH(niTaxi), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_VEH(niTaxi), <<2.0, 0.0, 0.0>>))
//				ENDIF
//				//SET_TAXI_LIGHTS(NET_TO_VEH(niTaxi), TRUE)
//				SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(niTaxi), TRUE)
//				
//				IF bCreateAsWaiting
//					IF DECOR_IS_REGISTERED_AS_TYPE("Am_Taxi Instance", DECOR_TYPE_INT)
//						IF NOT DECOR_EXIST_ON(NET_TO_VEH(niTaxi), "Am_Taxi Instance")
//							DECOR_SET_INT(NET_TO_VEH(niTaxi), "Am_Taxi Instance", NATIVE_TO_INT(PLAYER_ID()))
//							NET_PRINT_TIME() NET_PRINT(" -- TAXI - CREATE_MP_TAXI - DECOR ADDED - Am_Taxi Instance     <----------     ") NET_NL()
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				NET_PRINT_TIME() NET_PRINT(" -- TAXI - TAXI CREATED     <----------     ") NET_NL()
//			ENDIF
//		ENDIF
//		
//		// Create Driver
//		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niDriver)
//			//DriverID = CREATE_PED_INSIDE_VEHICLE(NET_TO_VEH(niTaxi), PEDTYPE_SPECIAL, A_M_M_EastSA_02)
//			IF CREATE_NET_PED_IN_VEHICLE(niDriver, niTaxi, PEDTYPE_SPECIAL, A_M_M_EastSA_02, VS_DRIVER)
//				SET_VEHICLE_FORWARD_SPEED(NET_TO_VEH(niTaxi), 5.0)
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(niDriver), TRUE)
//				SET_MODEL_AS_NO_LONGER_NEEDED(TaxiModel)
//				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_EastSA_02)
//				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(niTaxi), FALSE)
//				
//				VEHICLE_INDEX CarID = NET_TO_VEH(niTaxi)
//				PED_INDEX DriverID = NET_TO_PED(niDriver)
//				
//				IF bCreateAsWaiting
//					STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER(CarID, DriverID)
//				ELSE
//					SET_VEHICLE_AS_NO_LONGER_NEEDED(CarID)
//					SET_PED_AS_NO_LONGER_NEEDED(DriverID)
//				ENDIF
//				
//				NET_PRINT_TIME() NET_PRINT(" -- TAXI - TAXI DRIVER CREATED     <----------     ") NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niTaxi)
//	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(niDriver)
//		RETURN FALSE
//	ENDIF
//	
//	RETURN TRUE
//ENDFUNC






////PURPOSE: Spawns a Taxi near an MP player
//FUNC BOOL SPAWN_TAXI_NEAR_PLAYER_MP(NETWORK_INDEX &niTaxi, NETWORK_INDEX &niDriver, MODEL_NAMES TaxiModel = TAXI, FLOAT fSpawnRadius = 20.0) // this function had a defunct taxi midel 'TAXI2' I changed it to 'taxi'. KW 14/10/2011
//	VECTOR pos, pos2, pos3, vec, vec2
//	FLOAT fHeading
//	INT iNumLanes
//	VECTOR resultLinkDir
//	
//	//VIEWPORT_INDEX GameViewport
//	REQUEST_MODEL(TaxiModel)
//	REQUEST_MODEL(A_M_M_EastSA_02)
//	
//	IF IS_PLAYER_PLAYING(PLAYER_ID())
//		IF HAS_MODEL_LOADED(TaxiModel)
//			IF HAS_MODEL_LOADED(A_M_M_EastSA_02)
//				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//					pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
//					IF GENERATE_VEHICLE_CREATION_POS_FROM_PATHS(pos, pos2, resultLinkDir, 0, 180, fSpawnRadius, IS_COORD_IN_COUNTRYSIDE_MP(PLAYER_PED_ID()), TRUE, FALSE)
//						GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(pos2, 1, pos3, fHeading, iNumLanes)
//						CLEAR_AREA(pos2, 5.0, TRUE)
//										
//						// Invert the heading if taxi is create facing away from the player
//						pos3 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(pos2, fHeading, <<0.0, 1.0, 0.0>>)
//						// Vehicle Vector
//						vec = pos3 - pos2
//						// Vector to Player
//						vec2 = pos - pos2
//						
//						IF (GET_ANGLE_BETWEEN_2D_VECTORS(vec.x, vec.y, vec2.x, vec2.y) > 60.0)
//							fHeading += 180
//							fHeading = WRAP(fHeading, 0, 360)
//							printDebugStringAndFloat("\nSPAWN_TAXI_NEAR_PLAYER - revised fHeading = ", fHeading)
//						ENDIF
//						
//						IF CREATE_MP_TAXI(niTaxi, niDriver, TaxiModel, pos2, fHeading, TRUE)
//							RETURN TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC






PROC STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER_MP(VEHICLE_INDEX &InCar, PED_INDEX &InDriver)
	PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER called.\n")
	// if there is already a waiting taxi, tell it to bugger off
	IF NOT (InCar = g_WaitingTaxi)
	AND NOT (g_WaitingTaxi = NULL)
		CLEANUP_WAITING_TAXI()
	ENDIF
	
	// store this car as the new waiting taxi
	IF DOES_ENTITY_EXIST(InCar)
	AND DOES_ENTITY_EXIST(InDriver)

		IF IS_VEHICLE_DRIVEABLE(InCar)
			IF NOT IS_ENTITY_DEAD(InDriver)
				IF IS_PED_IN_VEHICLE(InDriver, InCar)
				
					/*IF GET_VEHICLE_NUMBER_OF_PASSENGERS() >= 2
						
						IF DECOR_EXIST_ON(InCar, "Am_Taxi Instance")
							MP_MISSION_DATA amTaxi
							amTaxi.mission = eAM_TAXI
							amTaxi.iInstanceId = DECOR_GET_INT(InCar, "Am_Taxi Instance")
							amTaxi.missionVariation = 99	//ENUM_TO_INT(TAXI_LAUNCHED_HAILED)
							IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amTaxi)	
								SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_TAXI")
								NET_PRINT_TIME() NET_PRINT(" -- TAXI LAUNCHER - STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER_MP - AM_TAXI LAUNCHED - C     <----------     ") NET_NL()
								GOTO_TAXI_TRIGGER_STAGE(hailTaxiScriptRunning) 
							ENDIF
						ENDIF
						
					ELSE*/
						IF NETWORK_REQUEST_CONTROL_OF_ENTITY (InCar)
						AND NETWORK_REQUEST_CONTROL_OF_ENTITY(InDriver)
							IF NETWORK_HAS_CONTROL_OF_ENTITY (InCar)
							AND NETWORK_HAS_CONTROL_OF_ENTITY(InDriver)
								g_WaitingTaxi = InCar
								g_WaitingTaxiDriver = InDriver

								g_bTaxiIsWaiting = TRUE
								
								IF NETWORK_IS_GAME_IN_PROGRESS()
									IF DECOR_IS_REGISTERED_AS_TYPE("Am_Taxi Instance", DECOR_TYPE_INT)
										IF NOT DECOR_EXIST_ON(g_WaitingTaxi, "Am_Taxi Instance")
											DECOR_SET_INT(g_WaitingTaxi, "Am_Taxi Instance", NATIVE_TO_INT(PLAYER_ID()))
											NET_PRINT_TIME() NET_PRINT(" -- TAXI - STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER - DECOR ADDED - Am_Taxi Instance     <----------     ") NET_NL()
										ENDIF
									ENDIF
								ENDIF
								
								PRINTSTRING("\n<")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("> STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER - ")
								IF NOT IS_ENTITY_A_MISSION_ENTITY(g_WaitingTaxiDriver)
									PRINTSTRING("g_WaitingTaxiDriver")
									SET_ENTITY_AS_MISSION_ENTITY(g_WaitingTaxiDriver)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_WaitingTaxiDriver, TRUE)
								ENDIF

								IF NOT IS_ENTITY_A_MISSION_ENTITY(g_WaitingTaxi)
									PRINTSTRING(", g_WaitingTaxi")
									SET_ENTITY_AS_MISSION_ENTITY(g_WaitingTaxi)
									SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(g_WaitingTaxi, TRUE)
									//PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> - g_vTaxiDropOffPosition = ", g_vTaxiDropOffPosition)
									TASK_VEHICLE_PARK(g_WaitingTaxiDriver, g_WaitingTaxi, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_HEADING(g_WaitingTaxi), PARK_TYPE_PULL_OVER)
									g_sTaxiEntitiesOwnedByThisScript = GET_THIS_SCRIPT_NAME()
			//						bThisScriptStoredCarAsMissionCar = TRUE
								ENDIF
								SET_VEHICLE_AUTOMATICALLY_ATTACHES(g_WaitingTaxi, FALSE)
								PRINTNL()
							ENDIF
						ENDIF
					//ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDIF

ENDPROC

























//PURPOSE: Creates a Taxi that won't be used by the Ambient Taxi Script
FUNC BOOL CREATE_NON_AM_TAXI(NETWORK_INDEX &niTaxi, NETWORK_INDEX &niDriver, VECTOR pos, FLOAT heading, structPedsForConversation &phonecallStruct)
	
	IF REQUEST_LOAD_MODEL(MPGlobalsAmbience.TaxiCarModel)
	AND REQUEST_LOAD_MODEL(MPGlobalsAmbience.TaxiDriverModel)
			
		// Create Taxi
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niTaxi)
			IF CREATE_NET_VEHICLE(niTaxi, MPGlobalsAmbience.TaxiCarModel, pos, heading)
				SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(NET_TO_VEH(niTaxi), TRUE)
				NET_PRINT_TIME() NET_PRINT(" -- TAXI - CREATE_NON_AM_TAXI - TAXI CREATED - AT ") NET_PRINT_VECTOR(pos) NET_NL()
			ENDIF
		ENDIF
		
		// Create Driver
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niDriver)
			IF CREATE_NET_PED_IN_VEHICLE(niDriver, niTaxi, PEDTYPE_SPECIAL, MPGlobalsAmbience.TaxiDriverModel, VS_DRIVER)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(niDriver), TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(MPGlobalsAmbience.TaxiCarModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(MPGlobalsAmbience.TaxiDriverModel)
				
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(niTaxi), FALSE)
				
				SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(NET_TO_PED(niDriver), TRUE)
								
				ADD_PED_FOR_DIALOGUE(phonecallStruct, 8, NET_TO_PED(niDriver), "FM_TAXI")
				
				NET_PRINT_TIME() NET_PRINT(" -- TAXI - CREATE_NON_AM_TAXI - TAXI DRIVER CREATED ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niTaxi)
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(niDriver)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC



/////** TUTORIAL STUFF **///
////Tutorial Creation Point
//<<-1033.3330, -2730.7366, 19.0628>>, 241.6186
////Taxi Dialogue Line - Let's Go
//CREATE_CONVERSATION(phonecallStruct, "MPTXIAU", "MPTXI_T1", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
////Taxi Dialogue Line - We're Here
//CREATE_CONVERSATION(phonecallStruct, "MPTXIAU", "MPTXI_AR", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
////Load Nodes
//LOAD_ALL_PATH_NODES(TRUE/FALSE)
//RELEASE_PATH_NODES()
////Task Drive
//TASK_VEHICLE_MISSION_COORS_TARGET(NET_TO_PED(niDriver), NET_TO_VEH(niTaxi), <<17.6804, -1114.2880, 28.7970>>, MISSION_GOTO, TAXI_SPEED_NORMAL, DrivingMode_StopForCars|DF_AdjustCruiseSpeedBasedOnRoadSpeed, 5, 5, FALSE)
////Take Money
//GIVE_LOCAL_PLAYER_CASH(-200)
////Exit
//TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
////Wander
//TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(niDriver), NET_TO_VEH(niTaxi), 12, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
////Player Control Off with Cam Control On
//PROC MAKE_ONLY_PLAYER_CAMERA_WORK()
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
//ENDPROC




