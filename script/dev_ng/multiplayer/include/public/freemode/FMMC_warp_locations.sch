
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////         Freemode mission selector menu  	    //////////////////////////////
//////////////////////////////           	   Bobby Wright          	    //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"


//CONST_INT FM_SAVED_LOCATIONS_0			0
//CONST_INT FM_SAVED_LOCATIONS_1			1
//CONST_INT FM_SAVED_LOCATIONS_2			2
//CONST_INT FM_SAVED_LOCATIONS_3			3
//CONST_INT FM_SAVED_LOCATIONS_4			4
//CONST_INT FM_SAVED_LOCATIONS_5			5
//CONST_INT FM_SAVED_LOCATIONS_6			6
//CONST_INT FM_SAVED_LOCATIONS_7			7
//CONST_INT FM_SAVED_LOCATIONS_8			8
//CONST_INT FM_SAVED_LOCATIONS_9			9
//CONST_INT MAX_FM_SAVED_LOCATIONS		10
//
//CONST_INT MAX_FM_DEFAULT_LOCATIONS		14
//
//FUNC MPPLY_VECTOR_STATS GET_VECTOR_STAT_INDEX_FROM_INT(INT iInt)
//	SWITCH iInt
//		CASE 0	RETURN MPPLY_SPAWN_POSITION_1
//		CASE 1	RETURN MPPLY_SPAWN_POSITION_2
//		CASE 2	RETURN MPPLY_SPAWN_POSITION_3
//		CASE 3	RETURN MPPLY_SPAWN_POSITION_4
//		CASE 4	RETURN MPPLY_SPAWN_POSITION_5
//		CASE 5	RETURN MPPLY_SPAWN_POSITION_6
//		CASE 6	RETURN MPPLY_SPAWN_POSITION_7
//		CASE 7	RETURN MPPLY_SPAWN_POSITION_8
//		CASE 8	RETURN MPPLY_SPAWN_POSITION_9
//		CASE 9	RETURN MPPLY_SPAWN_POSITION_10
//	ENDSWITCH
//	RETURN MPPLY_SPAWN_POSITION_1
//ENDFUNC
//








