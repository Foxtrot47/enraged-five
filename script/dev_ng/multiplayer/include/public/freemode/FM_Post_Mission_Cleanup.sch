
// ---------------------------------------------------------
//
//		POST MISSION CLEANUP
//
// ---------------------------------------------------------
//
// ---------------------------------------------------------
//
//		Flow for placing the player back in freemode
//		after doing a session job.
//
// ---------------------------------------------------------

// Included headers
USING "globals.sch"
USING "net_ambience.sch"
USING "leader_board_common.sch"
USING "mp_skycam.sch"
USING "net_spawn.sch"
USING "transition_common.sch"
USING "commands_network.sch"
USING "net_save_create_eom_vehicle.sch"
USING "net_heists_common.sch"
USING "net_celebration_screen.sch"
USING "net_corona_V2.sch"

// Get if the heist planning board has loaded. Need for loading back into the heist planning room.
FUNC BOOL HAS_PLANNING_BOARD_LOADED()
	
	IF IS_MP_HEIST_PRE_PLANNING_ACTIVE()
		RETURN TRUE
	ENDIF

	IF GET_HEIST_FINALE_STATE() = HEIST_FLOW_FAKE_UPDATE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP()
	
	PRINTLN("[PMC] - GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP - being called.")
	
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		PRINTLN("[PMC] - GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP - IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL = TRUE.")
		IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			PRINTLN("[PMC] - GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP - IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()) = FALSE.")
			PRINTLN("[PMC] - GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP - returning TRUE.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE()
		PRINTLN("[PMC] - GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP - SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE = TRUE.")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[PMC] - GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP - returning FALSE.")
	RETURN FALSE
	
ENDFUNC

FUNC BOOL GET_DOING_CLEANUP_BETWEEN_RACE_AND_MISSION()
	
	IF NOT HAS_FM_TRIGGER_TUT_BEEN_DONE()
		IF HAS_FM_RACE_TUT_BEEN_DONE()
			IF NOT g_bFailedTutorialMission
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// Gets if the sky swoop is needed. In some cases we don't need a sky swoop due to fades, cutscene or renderphase pausing.
FUNC BOOL IS_SKYSWOOP_NEEDED()
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
//		#IF IS_DEBUG_BUILD IF bIsSkySwoopNeededPrint PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED - bDoingHeistCelebration = TRUE, returning FALSE.") ENDIF #ENDIF
		#IF USE_FINAL_PRINTS PRINTLN_FINAL(" IS_SKYSWOOP_NEEDED: 7799618 g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration") #ENDIF
		RETURN FALSE
	ENDIF
	
	IF GET_DOING_CLEANUP_BETWEEN_RACE_AND_MISSION()
//		#IF IS_DEBUG_BUILD IF bIsSkySwoopNeededPrint PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED - GET_DOING_CLEANUP_BETWEEN_RACE_AND_MISSION = TRUE, returning FALSE.") ENDIF #ENDIF
		#IF USE_FINAL_PRINTS PRINTLN_FINAL(" IS_SKYSWOOP_NEEDED: 7799618 GET_DOING_CLEANUP_BETWEEN_RACE_AND_MISSION") #ENDIF
		RETURN FALSE
	ENDIF
	
	//If it's a playlist that's being restarted then we need to not warp, and the restart playlist stuff will deal with that. 
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bShouldTransitionSessionsRestartPlaylist
//		#IF IS_DEBUG_BUILD IF bIsSkySwoopNeededPrint PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED - sPostMissionCleanupData.bShouldTransitionSessionsRestartPlaylist = TRUE, returning FALSE.") ENDIF #ENDIF
		#IF USE_FINAL_PRINTS PRINTLN_FINAL(" IS_SKYSWOOP_NEEDED: 7799618 g_TransitionSessionNonResetVars.sPostMissionCleanupData.bShouldTransitionSessionsRestartPlaylist") #ENDIF
		RETURN FALSE
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_RESTART_PLAYLIST
//		#IF IS_DEBUG_BUILD IF bIsSkySwoopNeededPrint PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED - iFmLauncherGameState = FMMC_LAUNCHER_STATE_RESTART_PLAYLIST, returning FALSE.") ENDIF #ENDIF
		#IF USE_FINAL_PRINTS PRINTLN_FINAL(" IS_SKYSWOOP_NEEDED: 7799618 GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_RESTART_PLAYLIST") #ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition
		#IF USE_FINAL_PRINTS PRINTLN_FINAL(" IS_SKYSWOOP_NEEDED: 7799618 g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition") #ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != (-1)
		#IF USE_FINAL_PRINTS PRINTLN_FINAL(" IS_SKYSWOOP_NEEDED: 7799618 g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ",g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId) #ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.bFinishedFirstArenaEvent
		PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED - Returning false for bFinishedFirstArenaEvent")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL(" IS_SKYSWOOP_NEEDED: 7799618 g_TransitionSessionNonResetVars.bFinishedFirstArenaEvent") #ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DOING_TRANSITION_INTO_SIMPLE_INTERIOR_VIA_SKYCAM_QUICKWARP)
		PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED - Returning false for BS5_SIMPLE_INTERIOR_DOING_TRANSITION_INTO_SIMPLE_INTERIOR_VIA_SKYCAM_QUICKWARP")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL(" IS_SKYSWOOP_NEEDED: 7799618 IS_BIT_SET(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DOING_TRANSITION_INTO_SIMPLE_INTERIOR_VIA_SKYCAM_QUICKWARP") #ENDIF
		RETURN FALSE
	ENDIF
	
//	#IF IS_DEBUG_BUILD IF bIsSkySwoopNeededPrint PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED - iFmLauncherGameState = FMMC_LAUNCHER_STATE_RESTART_PLAYLIST, returning FALSE.") ENDIF #ENDIF
	RETURN TRUE
	
ENDFUNC

// Does the sky swoop if it is needed.
FUNC BOOL DO_SKYSWOOP_IF_NEEDED(BOOL bTrueIsUpFalseIsDown, BOOL bSkipIntro = FALSE)
	
	IF NOT IS_SKYSWOOP_NEEDED()
		RETURN TRUE
	ENDIF
	
	STOP_GAMEPLAY_HINT()
	#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_SKYSWOOP_IF_NEEDED: 7799618 DO_SKYSWOOP_IF_NEEDED bTrueIsUpFalseIsDown = ",bTrueIsUpFalseIsDown," bSkipIntro = ",bSkipIntro) #ENDIF
	#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_SKYSWOOP_IF_NEEDED: 7799618 DO_SKYSWOOP_IF_NEEDED GET_SKYSWOOP_STAGE() = ",GET_SKYSWOOP_STAGE()) #ENDIF
	IF bTrueIsUpFalseIsDown
		IF (GET_SKYSWOOP_STAGE() != SKYSWOOP_GOINGDOWN)
			IF SET_SKYSWOOP_UP(TRUE, TRUE, FALSE, DEFAULT, bSkipIntro, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("[PMC] - swoop down in progress, waiting for this to finish before beginning swoop up.")
		ENDIF
	ELSE
		IF (GET_SKYSWOOP_STAGE() != SKYSWOOP_GOINGUP)
			
			CAMERA_INDEX ciCamera = NULL
			
			IF g_sFMMCEOM.bDoWarp
			AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
			AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
				ciCamera = g_sTransitionSessionData.ciCam
				PRINTLN("[PMC] - bDoWarp = TRUE and IS_CORONA_INITIALISING_A_QUICK_RESTART() = FALSE, setting ciCamera = g_sTransitionSessionData.ciCam.")
			ENDIF
			
			IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.bRunPostMatchTransitionScene
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.bRunPostMatchTransitionScene = FALSE
				PRINTLN("[PMC] - set bRunPostMatchTransitionScene = FALSE. Call 0.")
			ENDIF
			
			BOOL bSkipSkySwoopOutro = FALSE
			#IF FEATURE_FIXER
			IF (g_TransitionSessionNonResetVars.sPostMissionCleanupData.PostMissionSpawnScenario = POST_MISSION_SPAWN_SITTING_SMOKING)
				PRINTLN("[PMC] - set bSkipSkySwoopOutro = TRUE")
				bSkipSkySwoopOutro = TRUE
			ENDIF
			#ENDIF
			
			
			IF SET_SKYSWOOP_DOWN(ciCamera, FALSE, bSkipSkySwoopOutro, (NOT g_sFMMCEOM.bDoWarp))
			
				IF DOES_ENTITY_EXIST(PlayerSwitchLeftBehindPed)
					IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(PlayerSwitchLeftBehindPed, FALSE)
						SET_ENTITY_AS_MISSION_ENTITY(PlayerSwitchLeftBehindPed, FALSE, TRUE)
						PRINTLN("[PMC] - set entity PlayerSwitchLeftBehindPed as a mission entity.")
					ELSE
						PRINTLN("[PMC] - PlayerSwitchLeftBehindPed is a mission entity.")
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(PlayerSwitchLeftBehindPed))
					DELETE_PED(PlayerSwitchLeftBehindPed)
					PRINTLN("[PMC] - set layerSwitchLeftBehindPed model as no longer needed.")
					PRINTLN("[PMC] - deleted ped PlayerSwitchLeftBehindPed.")
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(U_M_M_FilmDirector)

				RETURN TRUE
				
			ENDIF
		ELSE
			PRINTLN("[PMC] - swoop up in progress, waiting for this to finish before beginning swoop down.")
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    bBeenOnStandardHeistOrContactMission - 
/// RETURNS:
///    99 = no result
///    0 = True
///    1 = false
FUNC INT SHOULD_SET_PERSONAL_VEHICLE_WARP_FLAG(BOOL bBeenOnStandardHeistOrContactMission)
	
	IF g_sFMMCEOM.bDoWarp
		PRINTLN("[PMC] - [dsw] - SHOULD_SET_PERSONAL_VEHICLE_WARP_FLAG - g_sFMMCEOM.bDoWarp = TRUE, should not set personal veh warp flag.")
		RETURN 0
	ENDIF
	
	IF GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP()
		PRINTLN("[PMC] - [dsw] - SHOULD_SET_PERSONAL_VEHICLE_WARP_FLAG - GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP = TRUE, should not set personal veh warp flag.")
		RETURN 0
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		PRINTLN("[PMC] - [dsw] - SHOULD_SET_PERSONAL_VEHICLE_WARP_FLAG - IS_FAKE_MULTIPLAYER_MODE_SET() = TRUE, should not set personal veh warp flag.")
		RETURN 0
	ENDIF
	
	IF NOT IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID(TRUE))
		IF NOT IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION()
			PRINTLN("[PMC] - [dsw] - SHOULD_SET_PERSONAL_VEHICLE_WARP_FLAG - personal veh does not exist and is not going to be created, should not set personal veh warp flag.")
			RETURN 0
		ELSE
			PRINTLN("[PMC] - [dsw] - SHOULD_SET_PERSONAL_VEHICLE_WARP_FLAG - personal veh does not exist but is going to be created.")
		ENDIF
	ELSE
		IF bBeenOnStandardHeistOrContactMission
			
			#IF IS_DEBUG_BUILD
			VECTOR vTempPlayerCoords
			VECTOR vTempPvCoords
			vTempPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			vTempPvCoords = GET_ENTITY_COORDS(NET_TO_VEH(PERSONAL_VEHICLE_NET_ID(TRUE)))
			PRINTLN("[PMC] - [dsw] - SHOULD_SET_PERSONAL_VEHICLE_WARP_FLAG - been on a standard or contact mission.")
			PRINTLN("[PMC] - [dsw] - SHOULD_SET_PERSONAL_VEHICLE_WARP_FLAG - player ped coords: ", vTempPlayerCoords)
		 	PRINTLN("[PMC] - [dsw] - SHOULD_SET_PERSONAL_VEHICLE_WARP_FLAG - personal vehicle coords: ", vTempPvCoords)
			#ENDIF
			
			RETURN 0
			
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_VEH(PERSONAL_VEHICLE_NET_ID(TRUE))) < 100.0
				PRINTLN("[PMC] - [dsw] - SHOULD_SET_PERSONAL_VEHICLE_WARP_FLAG - personal veh < 100m so warping into it.")
				RETURN 1
			ELSE
				PRINTLN("[PMC] - [dsw] - CLEANUP_GOING_TO_USE_FADE - personal veh > 100m not warping into it.")
				RETURN 0
			ENDIF
		
		ELSE
			
			PRINTLN("[PMC] - [dsw] - CLEANUP_GOING_TO_USE_FADE - not been on a standard or contact mission, should warp into personal vehicle.")
			RETURN 1
			
		ENDIF
	ENDIF
	
	RETURN 99
				
ENDFUNC

// Gets if the personal vehicle system is a state where it is valid to call queries on the system.
FUNC BOOL READY_TO_DO_PERSONAL_VEHICLE_CHECKS()

	IF NOT IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION()
		RETURN TRUE
	ENDIF
	
	IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID(TRUE))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// Sets the player warp location to be the post mission location setup in the mission cloud data.
FUNC BOOL SETUP_CREATOR_POST_MISSION_WARP_LOCATION(PLAYER_SPAWN_LOCATION &eSpawnLoc)

//	For testing.
//	g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = <<63.0801, -1526.9858, 28.2931>>
//	g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius = 50.6765 
//	g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest = <<91.1055, -1541.6669, 28.3423>>
//	
	
	PRINTLN("[PMC] - SETUP_CREATOR_POST_MISSION_WARP_LOCATION - Calling Function.")	
	
	IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bForcePostMissionWarpCreatorPosition
		IF g_sFMMCEOM.bDoWarp
			PRINTLN("[PMC] - SETUP_CREATOR_POST_MISSION_WARP_LOCATION - g_sFMMCEOM.bDoWarp = TRUE, not using creator post mission location, returning FALSE.")
			RETURN FALSE
		ENDIF
	ELSE
		PRINTLN("[PMC] - SETUP_CREATOR_POST_MISSION_WARP_LOCATION - Forcing the use of creator placed post mission location.")
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos)
		
		eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
		FLOAT fRadius = 100
		IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius != 0
			fRadius = g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius
		ENDIF		
		
		SETUP_SPECIFIC_SPAWN_LOCATION(	g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos, 0.0,
										fRadius,
										TRUE,
										1, 
										FALSE,
										FALSE,	
										10)
		PRINTLN("[PMC] - SETUP_CREATOR_POST_MISSION_WARP_LOCATION - setup specific spawn near location. Position = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos, ", search radius = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius, ", visible checks = TRUE, min dis from coords = 1, consider interiors = FALSE, near roads = FALSE, min dis from other players = 65.")
		
		SET_PLAYER_WILL_SPAWN_FACING_COORDS(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest, TRUE, FALSE)
		PRINTLN("[PMC] - SETUP_CREATOR_POST_MISSION_WARP_LOCATION - setup player will spoawn facing coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest)
		
		// No being placed inside the grounds of the Humane Labs.
		SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3410.697998,3751.923828,9.634556>>, <<3657.413818,3700.305420,72.006607>>, 250.000000, 0, TRUE)
		PRINTLN("[PMC] - called SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3410.697998,3751.923828,9.634556>>, <<3657.413818,3700.305420,72.006607>>, 250.000000) - call 2.")
		
		RETURN TRUE
		
	ENDIF
	
	PRINTLN("[PMC] - SETUP_CREATOR_POST_MISSION_WARP_LOCATION - g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = << 0.0, 0.0, 0.0 >>, returning FALSE.")
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_POST_MISSION_CLEANUP_PUT_PLAYER_NEAR_EVENT()
	
	IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnNearEvent
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeenOnStandardHeistOrContactMission
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
		RETURN FALSE
	ENDIF
	
	IF GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_QUIT
		RETURN FALSE
	ENDIF
	
	IF NOT (GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_CONTINUE OR GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_INVALID) //And the vote status is ok!
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// Selects and sets up the player warp to location based on various data set when exiting missions. 
PROC SETUP_FMMC_POST_ACTIVITY_CLEANUP_WARP_LOCATION(POST_MISSION_CLEANUP_DATA &sData, PLAYER_SPAWN_LOCATION &eSpawnLoc, BOOL bGettingLoadSceneCoords = FALSE)
	
	VECTOR vSpawnCentre
	FLOAT fSearchRadius, fWarpHeading
	FLOAT fExclusionZone = 2.5
	FLOAT fMinDisFromOthers = 65.0
	BOOL bDoApartment
	BOOL bNearCentrePoint = TRUE
	FLOAT fUpperZLimitForNodes = -1.0
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		
		
		// *******************************************************************
		// SETUP INITIAL SPAWN VALUES
		// *******************************************************************
		
		vSpawnCentre = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		fSearchRadius = 100.0
		fExclusionZone = 2.5
		
		IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
			PRINTLN("[PMC] - bPlaceInApartment = TRUE.")
			IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPackedInt > 0
				PRINTLN("[PMC] - iPackedInt = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPackedInt)
				bDoApartment = TRUE
			ELSE
				PRINTLN("[PMC] - iPackedInt <= 0.")
			ENDIF
		ELSE
			PRINTLN("[PMC] - bPlaceInApartment = FALSE.")
		ENDIF
		

		
		// *******************************************************************
		// SETUP SPAWN POSITION BASED ON TYPE OF JOB THAT WAS JUST COMPLETED
		// *******************************************************************
		
		IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeachAfterSumoSpawnFlag
			
			PRINTLN("[PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeachAfterSumoSpawnFlag = TRUE, doing specific area for Sumo VI.")
			
			eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
			vSpawnCentre = <<66.0164, 7221.4541, 2.2992>>
			fSearchRadius = 100.0
			fExclusionZone = 0.1
					
			PRINTLN("[PMC] - vSpawnCentre = ", vSpawnCentre)
			PRINTLN("[PMC] - fSearchRadius = ", fSearchRadius)
			PRINTLN("[PMC] - fExclusionZone = ", fExclusionZone)
			PRINTLN("[PMC] - fMinDisFromOthers = ", fMinDisFromOthers)
			
			SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<34.237541,7694.354004,-9.227982>>, <<49.133461,7259.433105,24.664213>>, 600.000000, 1, TRUE)
			PRINTLN("[PMC] - called SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<34.237541,7694.354004,-9.227982>>, <<49.133461,7259.433105,24.664213>>, 600.000000, 1, TRUE) - call 0.")
			
		ELIF bDoApartment
			
			PRINTLN("[PMC] - bDoApartment = TRUE.")
			
			VECTOR vApartmentLocation
		
			UNPACK_INVITED_DATA_INT(	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPackedInt, 
										g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum, 
										g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyInstance, 
										g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyEntrance
										 , g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation )
			
			PRINTLN("[PMC] - iPackedInt = ", 				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPackedInt)
			PRINTLN("[PMC] - iInvitedPropertyNum = ", 		g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum)
			PRINTLN("[PMC] - iInvitedPropertyInstance = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyInstance)
			PRINTLN("[PMC] - iInvitedPropertyEntrance = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyEntrance)
			PRINTLN("[PMC] - iApartmentCustomVariation = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation)
			
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
				
				PRINTLN("[PMC] - bDoingAfterApartmentPanHeistCelebration = TRUE.")
				
				GET_PLAYER_PROPERTY_HEIST_LOCATION(vApartmentLocation, MP_PROP_ELEMENT_HEIST_PLAN_LOC, g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum)
				vSpawnCentre = vApartmentLocation
				fSearchRadius = 1.0
				fExclusionZone = 0.0
				fMinDisFromOthers = 0.0
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords = vSpawnCentre
				eSpawnLoc = SPAWN_LOCATION_AT_SPECIFIC_COORDS
				
			ELSE
				
				PRINTLN("[PMC] - bDoingAfterApartmentPanHeistCelebration = FALSE.")
				
				GET_PLAYER_PROPERTY_HEIST_LOCATION(vApartmentLocation, g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentLocation, g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum)
				vSpawnCentre = vApartmentLocation
				fSearchRadius = 1.0
				fExclusionZone = 0.0
				fMinDisFromOthers = 0.25
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords = vSpawnCentre
				eSpawnLoc = SPAWN_LOCATION_AT_SPECIFIC_COORDS
				
			ENDIF
			
			PRINTLN("[PMC] - bPlaceInApartment = TRUE.")
			PRINTLN("[PMC] - vSpawnCentre = ", vSpawnCentre)
			PRINTLN("[PMC] - fSearchRadius = ", fSearchRadius)
			PRINTLN("[PMC] - fExclusionZone = ", fExclusionZone)
			PRINTLN("[PMC] - fMinDisFromOthers = ", fMinDisFromOthers)
		ELIF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn
		
			eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
			//vSpawnCentre = <<-269.0, -1903.4, 27.5>>
			vSpawnCentre = <<ARENA_X, ARENA_Y, ARENA_Z>>
			fSearchRadius = 100.0
			fExclusionZone = 0.1
					
			PRINTLN("[PMC] - vSpawnCentre = ", vSpawnCentre)
			PRINTLN("[PMC] - fSearchRadius = ", fSearchRadius)
			PRINTLN("[PMC] - fExclusionZone = ", fExclusionZone)
			PRINTLN("[PMC] - fMinDisFromOthers = ", fMinDisFromOthers)
			
			SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3122, -3631, 85>>, <<2543, -3985, 208>>, 600.000000, 1, TRUE)
			PRINTLN("[PMC] - called SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3122, -3631, 85>>, <<2543, -3985, 208>>, 600.000000, 1, TRUE).")
			
		#IF FEATURE_FIXER
		ELIF (g_TransitionSessionNonResetVars.sPostMissionCleanupData.PostMissionSpawnScenario = POST_MISSION_SPAWN_SITTING_SMOKING)
			
			eSpawnLoc = SPAWN_LOCATION_SITTING_SMOKING 
			
			PRINTLN("[PMC] - eSpawnLoc = SPAWN_LOCATION_SITTING_SMOKING")
					
		ELIF (g_TransitionSessionNonResetVars.sPostMissionCleanupData.PostMissionSpawnScenario = POST_MISSION_SPAWN_DRUNK_WAKE_UP_MUSIC_STUDIO)
				
			eSpawnLoc = SPAWN_LOCATION_DRUNK_WAKE_UP_MUSIC_STUDIO 
				
				
			PRINTLN("[PMC] - eSpawnLoc = SPAWN_LOCATION_DRUNK_WAKE_UP_MUSIC_STUDIO")
			
		#ENDIF
				
		// If we have been on a contact, heist or standard mission or we have been on a non-mission job where we were not playing with friendlies in a team.
		ELSE
			
			IF SHOULD_POST_MISSION_CLEANUP_PUT_PLAYER_NEAR_EVENT()
						
				#IF IS_DEBUG_BUILD
					PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - SETUP_FMMC_POST_ACTIVITY_CLEANUP_WARP_LOCATION - FM_EVENT_SHOULD_SPAWN_NEAR_ACTIVE_FM_EVENT = TRUE")
				#ENDIF
				
				vSpawnCentre = g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords
				eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
				bNearCentrePoint = FALSE
				fSearchRadius = g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent
				fExclusionZone = (g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent/3)
				
				IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEventMin > 0.0
					fExclusionZone = g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEventMin
					PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - fDistanceFromFreemodeEventMin = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEventMin, ", valid value, using this as exclusion zone.")
				ENDIF
					
				#IF IS_DEBUG_BUILD
				PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - vSpawnCentre = g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords)
				PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS")
				PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - bNearCentrePoint = FALSE")
				PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - fSearchRadius = g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent)
				FLOAT fDisTemp = (g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent/3)
				PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - fExclusionZone = (g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent/3) = ", fDisTemp)
				#ENDIF
				
			ELIF ( (sData.bBeenOnStandardHeistOrContactMission) OR (NOT sData.bBeenOnTeamNonMissionJob) )
				
				// Print debug info.
				#IF IS_DEBUG_BUILD
					IF sData.bBeenOnStandardHeistOrContactMission
						PRINTLN("[PMC] - sData.bBeenOnStandardHeistOrContactMission = TRUE")
					ELSE
						PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - sData.bBeenOnStandardHeistOrContactMission = FALSE")
					ENDIF
					IF sData.bBeenOnTeamNonMissionJob
						PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - sData.bBeenOnTeamNonMissionJob = TRUE")
					ELSE
						PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - sData.bBeenOnTeamNonMissionJob = FALSE")
					ENDIF
				#ENDIF
				
				PRINTLN("[PMC] - sData.vEndOfJobPosition = ", sData.vEndOfJobPosition)
				PRINTLN("[PMC] - current player coords =", (vSpawnCentre + << 0.0, -15.0, 0.0 >>))
				
				// Use the end of job position as a common point to spawn near.
				SET_ENTITY_COORDS(PLAYER_PED_ID(), (sData.vEndOfJobPosition + << 0.0, -15.0, 0.0 >>))
				
				FLOAT fEndOfJobZ = sData.vEndOfJobPosition.z
				
				GET_GROUND_Z_FOR_3D_COORD(sData.vEndOfJobPosition, sData.vEndOfJobPosition.z)

					
				
				// if initial end of ground z is above water then dont pick a point on the sea bed. #4473681
				IF (fEndOfJobZ > 0.0)
				AND (sData.vEndOfJobPosition.z < 0.0)
					PRINTLN("[PMC] - avoiding sea bed for end of job pos.")
					sData.vEndOfJobPosition.z = 0.0
				ENDIF
					
				// if we have moved the z a great deal then increase the upper z when doing a node search #4473681
				IF (ABSF(fEndOfJobZ - sData.vEndOfJobPosition.z) > 4.0)
					fUpperZLimitForNodes = ABSF(fEndOfJobZ - sData.vEndOfJobPosition.z)	+ 4.0	
					PRINTLN("[PMC] - increasing fUpperZLimitForNodes to ", fUpperZLimitForNodes)
				ENDIF
				
				
				
				vSpawnCentre = sData.vEndOfJobPosition
				fWarpHeading = sData.fEndOfJobHeading
				
				IF sData.bBeenOnStandardHeistOrContactMission
					fSearchRadius = 100.0
					fExclusionZone = 0.0
					fMinDisFromOthers = 1.0
					PRINTLN("[PMC] - sData.bBeenOnStandardHeistOrContactMission = TRUE, been on a contact mission, setting closeby spanwing.")
				ENDIF
				
				// If we are not doing the early load scene for the transition, set that we are going to use the specific end of job position as out warp coords.
				IF NOT bGettingLoadSceneCoords
					sData.bDoSpecificEndOfJobPos = TRUE
					eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
					bNearCentrePoint = FALSE
				ENDIF
				
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
					IF NOT IS_VECTOR_ZERO(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.vMyPostJobSpawnArea)
						eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG
						fSearchRadius = 175.0
						fExclusionZone = 0.0
						fMinDisFromOthers = 1.0
						vSpawnCentre = GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.vMyPostJobSpawnArea
						
						IF vSpawnCentre.z < 0
							vSpawnCentre.z = 0
							fSearchRadius = 300
							
							PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - vSpawnCentre.z < 0, setting to 0")
							PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - calling SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION")
							
							SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION(FALSE, TRUE)
							
							g_SpecificSpawnLocation.bAllowFallbackToUseInteriors = FALSE
							PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - g_SpecificSpawnLocation.bAllowFallbackToUseInteriors = FALSE")
							
							g_SpecificSpawnLocation.bConsiderInteriors = FALSE
							PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - g_SpecificSpawnLocation.bConsiderInteriors = FALSE")
						ENDIF
						
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - my boss' vMyPostJobSpawnArea = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.vMyPostJobSpawnArea)
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG ")
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set fSearchRadius = ", fSearchRadius)
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set fExclusionZone = ", fExclusionZone)
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set fMinDisFromOthers = ", fMinDisFromOthers)
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set vSpawnCentre = ", vSpawnCentre)
					ELSE
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - my boss' vMyPostJobSpawnArea = zero vector, not setting up SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG.")
					ENDIF
				ENDIF
				
			// If we have ***NOT*** been on a contact, heist or standard mission.
			ELSE
				
				// Print that we're in here.
				PRINTLN("[PMC] - sData.bBeenOnStandardHeistOrContactMission = FALSE")
				
				// If we have been on a non-mission job with friendlies on a team.
				IF sData.bBeenOnTeamNonMissionJob
					
					// Print why we're here.
					PRINTLN("[PMC] - sData.bBeenOnTeamNonMissionJob = TRUE")
					
					// Set the spawn location to be the same 
					IF NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc)
						
						PRINTLN("[PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc =", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc)
						
						FLOAT fZ
						
						IF GET_GROUND_Z_FOR_3D_COORD(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc, fZ)
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc.z = fZ
						ENDIF
						
						vSpawnCentre = g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc
						fWarpHeading = sData.fEndOfJobHeading
						
						IF NOT bGettingLoadSceneCoords
							sData.bDoSpecificEndOfJobPos = TRUE
							eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
							bNearCentrePoint = FALSE
						ENDIF
						
					ENDIF	
					
				ENDIF
				
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
					IF NOT IS_VECTOR_ZERO(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.vMyPostJobSpawnArea)
						eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG
						fSearchRadius = 175.0
						fExclusionZone = 0.0
						fMinDisFromOthers = 1.0
						vSpawnCentre = GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.vMyPostJobSpawnArea
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - my boss' vMyPostJobSpawnArea = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.vMyPostJobSpawnArea)
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG ")
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set fSearchRadius = ", fSearchRadius)
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set fExclusionZone = ", fExclusionZone)
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set fMinDisFromOthers = ", fMinDisFromOthers)
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set vSpawnCentre = ", vSpawnCentre)
					ELSE
						PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - my boss' vMyPostJobSpawnArea = zero vector, not setting up SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG.")
					ENDIF
				ENDIF
				
			ENDIF
			
			
			// *******************************************************************
			// SETUP SPAWN POSITION FOR LEAVING TUTORIALS.
			// *******************************************************************
			
			// Get centre of spawn location and spawn radius depending on activity we were just doing.
			IF GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP()
			OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCompletedGeraldMission
				IF (SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE() OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCompletedGeraldMission)
					
					// Print why we are in here.
					PRINTLN("[PMC] - SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE = TRUE, using alley angled area.")
					
					IF NOT bGettingLoadSceneCoords
						eSpawnLoc = SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA
						SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA(<<-69.594696,-1523.905762,23.250015>>, <<-51.842068,-1502.246948,37.875168>>, 22.000000, <<-79.0202, -1484.3698, 31.2329>>)
					ENDIF
					
				// If we are spawning after the race.
				ELSE
					
					// Print why we are here.
					PRINTLN("[PMC] - [dsw] - IS_LOCAL_PLAYER_RUNNING_DM_TUTORIAL - SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE = FALSE, using Dave Watson' specific coords for after race cutscene.")
					
					// If not doing early load scene for transition, setup type of spawn location warp to do.
					IF NOT bGettingLoadSceneCoords
						eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
						bNearCentrePoint = FALSE
					ENDIF
					
					// Set data to be Dave's specific data.
					vSpawnCentre = <<396.6714, 268.6234, 102.0248>> // <<-170.8024, 269.5732, 92.1215>>
					fSearchRadius = 10.0
					fWarpHeading = 348.9612 //279.4054
					fExclusionZone = 0.1
					
				ENDIF
				
			ENDIF
			
			
			// *******************************************************************
			// SETUP SPAWN POSITION FOR MINI GAMES.
			// *******************************************************************
			IF sData.bBeenOnMiniGame
				eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
				vSpawnCentre = sData.vMiniGameCoronaLoc
				fSearchRadius = 15.0
				fExclusionZone = 2.5
				PRINTLN("[PMC] - sData.bBeenOnMiniGame = TRUE, setting up spawn data to be around corona area.")
			ENDIF
		
		ENDIF
		
	ENDIF
	
	// *******************************************************************
	// SETUP SPAWN POSITION FOR WARPING TO A NEW JOB.
	// *******************************************************************
	
	IF g_sFMMCEOM.bDoWarp
		
		// If Bobby has given us a vector, save it as where we want ot warp to.
		IF NOT IS_VECTOR_ZERO(g_sFMMCEOM.vWarp)
			vSpawnCentre = g_sFMMCEOM.vWarp
			IF NOT bGettingLoadSceneCoords
				eSpawnLoc = SPAWN_LOCATION_AT_SPECIFIC_COORDS
			ENDIF
			PRINTLN("[PMC] - g_sFMMCEOM.vWarp = ", g_sFMMCEOM.vWarp)
			PRINTLN("[PMC] - set eSpawnLoc = SPAWN_LOCATION_AT_SPECIFIC_COORDS.")
		ELSE
			PRINTLN("[PMC] - just using player coords since g_sFMMCEOM.vWarp is zero vector.")
			SCRIPT_ASSERT("g_sFMMCEOM.bDoWarp = TRUE but g_sFMMCEOM.vWarp = zero. Please add a bug for Robert Wright and cc william Kennedy.")
		ENDIF
	
	ENDIF
		
	// *******************************************************************
	// SAVE CALCULATED DATA TO PERMANENT VARIABLES FOR USE ELSEWHERE.
	// *******************************************************************
	
	// If we are not doing the early load scene for the transition, save the warp location we have ust calculated to a permanent vector.
	IF NOT bGettingLoadSceneCoords
		sData.vWarpLoc = vSpawnCentre
	ENDIF
	
	// Call Neil's setup spawn location command and setup the next spawn location with the data we just calculated.
	IF NOT bGettingLoadSceneCoords
		IF SHOULD_POST_MISSION_CLEANUP_PUT_PLAYER_NEAR_EVENT()
			
			SETUP_SPECIFIC_SPAWN_LOCATION(vSpawnCentre, fWarpHeading, fSearchRadius, TRUE, fExclusionZone, FALSE, TRUE, fMinDisFromOthers, bNearCentrePoint, DEFAULT, DEFAULT, DEFAULT, fUpperZLimitForNodes)
			SET_PLAYER_WILL_SPAWN_FACING_COORDS(vSpawnCentre, TRUE, FALSE)
			IF FM_EVENT_GET_SESSION_ACTIVE_FM_EVENT() = FMMC_TYPE_KING_CASTLE
				IF g_AML_serverBD.sHistory[0].iVaration = CASTLE_AREA_FIRE_STATION
					SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<-108.9606, 6101.9077, 30.1041>>, <<1166.3878, 4431.0894, 850.0>>, 2250.000000, 0, TRUE)
					PRINTLN("[PMC] - called SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<-108.9606, 6101.9077, 30.1041>>, <<1166.3878, 4431.0894, 850.0>>, 2250.000000, 0, TRUE) - avoid spawning on mountain.")
				ELSE
					PRINTLN("[PMC] - FM_EVENT_GET_SESSION_ACTIVE_FM_EVENT() != FMMC_TYPE_KING_CASTLE")
				ENDIF
			ELSE
				PRINTLN("[PMC] - g_AML_serverBD.sHistory[0].iVaration != CASTLE_AREA_FIRE_STATION")
			ENDIF
			PRINTLN("[PMC] - called SETUP_SPECIFIC_SPAWN_LOCATION for FM_EVENT_SHOULD_SPAWN_NEAR_ACTIVE_FM_EVENT.")
			
		ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
		
			SETUP_SPECIFIC_SPAWN_LOCATION(vSpawnCentre, fWarpHeading, fSearchRadius, FALSE, 0, TRUE, FALSE, fMinDisFromOthers, TRUE, DEFAULT, DEFAULT, DEFAULT, fUpperZLimitForNodes)
			PRINTLN("[PMC] - called SETUP_SPECIFIC_SPAWN_LOCATION for bDoingAfterApartmentPanHeistCelebration.")
			
		ELIF (eSpawnLoc != SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA)
		
			BOOL bConsiderInterior = IS_THIS_A_QUICK_RESTART_JOB()
			PRINTLN("[PMC] - IS_THIS_A_QUICK_RESTART_JOB = ", bConsiderInterior, " setting bConsiderInterior = ", bConsiderInterior)
			
			bConsiderInterior = TRUE
			IF GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_CONTINUE
				bConsiderInterior = FALSE
				PRINTLN("[PMC] - ciFMMC_EOM_VOTE_STATUS_CONTINUE, setting bConsiderInterior = FALSE")
			ENDIF
			IF GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_RANDOM
				bConsiderInterior = FALSE
				PRINTLN("[PMC] - ciFMMC_EOM_VOTE_STATUS_RANDOM, setting bConsiderInterior = FALSE")
			ENDIF
			IF GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_QUIT
				bConsiderInterior = FALSE
				PRINTLN("[PMC] - ciFMMC_EOM_VOTE_STATUS_QUIT, setting bConsiderInterior = FALSE")
			ENDIF
			
			SETUP_SPECIFIC_SPAWN_LOCATION(vSpawnCentre, fWarpHeading, fSearchRadius, TRUE, fExclusionZone, bConsiderInterior, FALSE, fMinDisFromOthers, bNearCentrePoint, FALSE, 0, bConsiderInterior, fUpperZLimitForNodes)
			// No being placed inside the grounds of the Humane Labs.
			SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3410.697998,3751.923828,9.634556>>, <<3657.413818,3700.305420,72.006607>>, 250.000000, 0, TRUE)
			PRINTLN("[PMC] - called SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3410.697998,3751.923828,9.634556>>, <<3657.413818,3700.305420,72.006607>>, 250.000000) - call 0.")
			IF NOT g_sFMMCEOM.bDoWarp
				MOVE_SPECIFIC_SPAWN_LOCATION_OUTSIDE_EXCLUSION_ZONES()
			ENDIF
			
		ELSE
		
			// No being placed inside the grounds of the Humane Labs.
			SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3410.697998,3751.923828,9.634556>>, <<3657.413818,3700.305420,72.006607>>, 250.000000, 0, TRUE)
			PRINTLN("[PMC] - called SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3410.697998,3751.923828,9.634556>>, <<3657.413818,3700.305420,72.006607>>, 250.000000) - call 1.")
		
		ENDIF
		
	// If we are doing the initial load scene for the transition, save the vector just calculated to a permanent vector for it to use.
	ELSE
		
		IF IS_VECTOR_ZERO(sData.vLoadSceneCoords)
			sData.vLoadSceneCoords = vSpawnCentre
			PRINTLN("[PMC] - sData.vLoadSceneCoords is zero. Setting to ", sData.vLoadSceneCoords)
		ELSE
			PRINTLN("[PMC] - sData.vLoadSceneCoords is not zero. It is ", sData.vLoadSceneCoords)
		ENDIF
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
	PROC DEBUG_PRINT_GOING_TO_POST_ACTIVITY_CLEANUP_STAGE(ePOST_MISSION_CLEANUP_STAGE &eStage)
		SWITCH eStage
			CASE ePOSTMISSIONCLEANUPSTAGE_WAIT_FOR_LEADERBOARD_CAM_TO_BE_READY	PRINTLN("[PMC] - Going to stage WAIT_FOR_LEADERBOARD_CAM_TO_BE_READY.") 	BREAK
			CASE ePOSTMISSIONCLEANUPSTAGE_PREPARE_FOR_FREMODE_DEATH				PRINTLN("[PMC] - Going to stage PREPARE_FOR_FREMODE_DEATH.") 				BREAK
			CASE ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD							PRINTLN("[PMC] - Going to stage FREEMODE_DEAD.") 							BREAK
			CASE ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_SAVED_EOM_CAR				PRINTLN("[PMC] - Going to stage INTERP_TO_SAVED_EOM_CAR")					BREAK
			CASE ePOSTMISSIONCLEANUPSTAGE_DECIDE_INTERP_TYPE 					PRINTLN("[PMC] - Going to stage DECIDE_INTERP_TYPE.") 						BREAK
			CASE ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_PERSONAL_VEH 				PRINTLN("[PMC] - Going to stage INTERP_TO_PERSONAL_VEH.") 					BREAK
			CASE ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_NEARBY_ON_FOOT 				PRINTLN("[PMC] - Going to stage INTERP_TO_NEARBY_ON_FOOT.") 				BREAK
//			CASE ePOSTMISSIONCLEANUPSTAGE_CREATE_LAST_VEHICLE					PRINTLN("[PMC] - Going to stage CREATE_LAST_VEHICLE.")						BREAK
			CASE ePOSTMISSIONCLEANUPSTAGE_LOAD_SCENE							PRINTLN("[PMC] - Going to stage LOAD_SCENE")								BREAK
			CASE ePOSTMISSIONCLEANUPSTAGE_CUT_TO_PLAYER							PRINTLN("[PMC] - Going to stage CUT_TO_PLAYER	.")							BREAK
			CASE ePOSTMISSIONCLEANUPSTAGE_FADE_IN 								PRINTLN("[PMC] - Going to stage FADE_IN.") 								BREAK
			CASE ePOSTMISSIONCLEANUPSTAGE_TUTORIAL_CUT 							PRINTLN("[PMC] - Going to stage TUTORIAL_CUT.") 							BREAK
			CASE ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP 								PRINTLN("[PMC] - Going to stage CLEAN_UP.") 								BREAK
		ENDSWITCH
		
	ENDPROC
#ENDIF
#IF IS_DEBUG_BUILD
	FUNC STRING DEBUG_PRINT_GOING_TO_POST_ACTIVITY_CLEANUP_STAGE_SHORT(ePOST_MISSION_CLEANUP_STAGE &eStage)
		SWITCH eStage
			CASE ePOSTMISSIONCLEANUPSTAGE_WAIT_FOR_LEADERBOARD_CAM_TO_BE_READY	RETURN "WAIT_FOR_LEADERBOARD_CAM_TO_BE_READY" 	
			CASE ePOSTMISSIONCLEANUPSTAGE_PREPARE_FOR_FREMODE_DEATH				RETURN "PREPARE_FOR_FREMODE_DEATH" 			
			CASE ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD							RETURN "FREEMODE_DEAD" 						
			CASE ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_SAVED_EOM_CAR				RETURN "INTERP_TO_SAVED_EOM_CAR"				
			CASE ePOSTMISSIONCLEANUPSTAGE_DECIDE_INTERP_TYPE 					RETURN "DECIDE_INTERP_TYPE" 					
			CASE ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_PERSONAL_VEH 				RETURN "INTERP_TO_PERSONAL_VEH" 				
			CASE ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_NEARBY_ON_FOOT 				RETURN "INTERP_TO_NEARBY_ON_FOOT" 				
//			CASE ePOSTMISSIONCLEANUPSTAGE_CREATE_LAST_VEHICLE					RETURN "CREATE_LAST_VEHICLE"					
			CASE ePOSTMISSIONCLEANUPSTAGE_LOAD_SCENE							RETURN "LOAD_SCENE"								
			CASE ePOSTMISSIONCLEANUPSTAGE_CUT_TO_PLAYER							RETURN "CUT_TO_PLAYER	"						
			CASE ePOSTMISSIONCLEANUPSTAGE_FADE_IN 								RETURN "FADE_IN" 								
			CASE ePOSTMISSIONCLEANUPSTAGE_TUTORIAL_CUT 							RETURN "TUTORIAL_CUT" 							
			CASE ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP 								RETURN "CLEAN_UP" 								
		ENDSWITCH
		RETURN ""
	ENDFUNC
#ENDIF

// Sets the current post mission cleanup stage.
PROC GOTO_POST_MISSION_CLEANUP_STAGE(ePOST_MISSION_CLEANUP_STAGE &eStage, ePOST_MISSION_CLEANUP_STAGE eNewStage)
	
	#IF IS_DEBUG_BUILD
		IF eStage != eNewStage
			DEBUG_PRINT_GOING_TO_POST_ACTIVITY_CLEANUP_STAGE(eNewStage)
			PRINTLN("TRANSITION TIME - POST_MISSION_CLEANUP - [PMC] - ", DEBUG_PRINT_GOING_TO_POST_ACTIVITY_CLEANUP_STAGE_SHORT(eNewStage), "- FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
		ENDIF
	#ENDIF
	#IF USE_FINAL_PRINTS PRINTLN_FINAL("GOTO_POST_MISSION_CLEANUP_STAGE: 7799618 eNewStage = ",eNewStage) #ENDIF
	eStage = eNewStage
	
ENDPROC

// Gets if the player should be placed inot the pv during the post mission cleanup.
FUNc BOOL SHOULD_PLAYER_WARP_INTO_PERSONAL_VEHICLE_FOR_CLEANUP(BOOL bBeenOnStandardHeistOrContactMission)

	IF SHOULD_SET_PERSONAL_VEHICLE_WARP_FLAG(bBeenOnStandardHeistOrContactMission) = 1
		RETURN TRUE
	ENDIF
	
	IF MP_SAVE_VEHICLE_IS_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// Warps the player into their personal vehicle.
FUNc BOOL WARP_PLAYER_INTO_PERSONAL_VEHICLE(SCRIPT_TIMER &eTimer)
	
	IF NOT HAS_NET_TIMER_STARTED(eTimer)
		
		START_NET_TIMER(eTimer)
		
	ELSE
	
		IF NOT HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
		OR HAS_NET_TIMER_EXPIRED(eTimer, 30000)
			
			#IF IS_DEBUG_BUILD
				IF HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
					SCRIPT_ASSERT("[PMC] - wait for MPGlobals.VehicleData.bWarpNear failsafe timer has timed out. Personal veh took longer than 30 seconds to warp nearby.")
				ENDIF
			#ENDIF
			
			IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(PERSONAL_VEHICLE_NET_ID(TRUE)))
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), false)
					ENDIF
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					
					IF IS_MP_PASSIVE_MODE_ENABLED()
					AND IS_VEHICLE_RESTRICTED_IN_PASSIVE_MODE(NET_TO_VEH(PERSONAL_VEHICLE_NET_ID(TRUE)))
						DISABLE_MP_PASSIVE_MODE(PMER_END_OF_MATCH, FALSE)
						PRINTLN("[PMC] WARP_PLAYER_INTO_PERSONAL_VEHICLE - Disabled passive mode, players PV is restricted in passive mode.")
					ENDIF
					
					TASK_ENTER_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(PERSONAL_VEHICLE_NET_ID(TRUE)), 1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
					PRINTLN("[PMC] - called TASK_ENTER_VEHICLE (ECF_WARP_PED).")
					REMOVE_PLAYERS_TOP_HAT_IF_IN_A_VEHICLE(TRUE)
				ENDIF
			ELSE
				REMOVE_PLAYERS_TOP_HAT_IF_IN_A_VEHICLE()
				RETURN TRUE
			ENDIF
			
			// Speirs added as temp fix (1831326)
			IF HAS_NET_TIMER_EXPIRED(eTimer, 45000)
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("[PMC] - failed to warp into personal vehicle, tell WillK ")
					PRINTLN("[PMC] - failed to warp into personal vehicle, tell WillK")
				#ENDIF
			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
						
	RETURN FALSE
ENDFUNC

// Sets up the players warp location to be in the shooting range.
PROC SET_SHOOTING_RANGE_WARP_LOCATION(PLAYER_SPAWN_LOCATION &eSpawnLoc)
	
	IF NOT g_sFMMCEOM.bDoWarp
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_1]
			eSpawnLoc = SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA
			SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA(<<22.514502,-1111.060669,28.697018>>, <<4.838369,-1103.583496,32.247017>>, 10.000000, <<0.0, 0.0, 0.0>>, TRUE, TRUE, FALSE)
			SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA)
			PRINTLN("[PMC] - was just in shooting range and g_sFMMCEOM.bDoWarp = FALSE, setting spawn location to SPECIFIC_ANGLED_AREA.")
		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_2]
			eSpawnLoc = SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA
			SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA(<<825.974121,-2155.823486,27.949789>>, <<807.876404,-2154.074707,31.619005>>, 8.000000, <<0.0, 0.0, 0.0>>, TRUE, TRUE, FALSE)
			SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA)
			PRINTLN("[PMC] - was just in shooting range and g_sFMMCEOM.bDoWarp = FALSE, setting spawn location to SPECIFIC_ANGLED_AREA.")
		ENDIF
	ELSE
		PRINTLN("[PMC] - g_sFMMCEOM.bDoWarp = TRUE, so skipping setting spawn location to SPECIFIC_ANGLED_AREA.")
	ENDIF
	
ENDPROC

// Gets if it is ok to visually cut to the player after the post mission cleanup.
FUNC BOOL OK_TO_CUT_TO_PLAYER(POST_MISSION_CLEANUP_DATA &sData)
	
	BOOL bDoCheck
	
	#IF IS_DEBUG_BUILD
		IF g_sFMMCEOM.bDoWarp
			PRINTLN("[PMC] - OK_TO_CUT_TO_PLAYER - g_sFMMCEOM.bDoWarp = TRUE")
		ELSE
			PRINTLN("[PMC] - OK_TO_CUT_TO_PLAYER - g_sFMMCEOM.bDoWarp = FALSE")
		ENDIF
		IF IS_PLAYER_ACTIVE_IN_CORONA()
			PRINTLN("[PMC] - OK_TO_CUT_TO_PLAYER - IS_PLAYER_ACTIVE_IN_CORONA = TRUE")
		ELSE
			PRINTLN("[PMC] - OK_TO_CUT_TO_PLAYER - IS_PLAYER_ACTIVE_IN_CORONA = FALSE")
		ENDIF
		IF GET_CORONA_STATUS() = CORONA_STATUS_JOIN_HOST
			PRINTLN("[PMC] - OK_TO_CUT_TO_PLAYER - GET_CORONA_STATUS() = CORONA_STATUS_JOIN_HOST ")
		ELSE
			PRINTLN("[PMC] - OK_TO_CUT_TO_PLAYER - GET_CORONA_STATUS() != CORONA_STATUS_JOIN_HOST ")
		ENDIF
		IF g_bFailedTutorialMission
			PRINTLN("[PMC] - OK_TO_CUT_TO_PLAYER - g_bFailedTutorialMission = TRUE")
		ELSE
			PRINTLN("[PMC] - OK_TO_CUT_TO_PLAYER - g_bFailedTutorialMission = FALSE")
		ENDIF
	#ENDIF
	
	IF SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE()
		IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stMinSplashLengthTimer)
			START_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stMinSplashLengthTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stMinSplashLengthTimer, 5000)
				bDoCheck = TRUE
			ENDIF
		ENDIF
	ELSE
		bDoCheck = TRUE
	ENDIF
	
	IF bDoCheck
		IF (g_sFMMCEOM.bDoWarp AND (IS_PLAYER_ACTIVE_IN_CORONA() OR GET_CORONA_STATUS() = CORONA_STATUS_JOIN_HOST))
			RETURN TRUE
		ELIF (g_sFMMCEOM.bDoWarp AND DOES_TRANSITION_SESSIONS_NEED_TO_WAIT_FOR_HAS_STARTED_BEFORE_CLEAN())
			RETURN TRUE
		ELIF NOT g_sFMMCEOM.bDoWarp
			RETURN TRUE
		ELIF g_bFailedTutorialMission
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Safety bail:
	IF g_sFMMCEOM.bDoWarp
	AND NOT IS_VECTOR_ZERO(g_sFMMCEOM.vWarp)
	AND GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_RESTART
	AND NOT IS_STRING_NULL_OR_EMPTY(g_sFMMCEOM.contentID)
		IF IS_STRING_NULL_OR_EMPTY(g_sTriggerMP.mtMissionData.mdID.idCloudFilename)
			IF NOT HAS_NET_TIMER_STARTED(sData.sRestartStuckTimer)
				PRINTLN("[PMC] OK_TO_CUT_TO_PLAYER START_NET_TIMER()")
				START_NET_TIMER(sData.sRestartStuckTimer)
			ELIF HAS_NET_TIMER_EXPIRED(sData.sRestartStuckTimer, g_sMptunables.iStuckEntering_modifier)			
				PRINTLN("[TS] *****************************************************************")
				PRINTLN("[TS] *  OK_TO_CUT_TO_PLAYER BAIL TO MP T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
				#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] *  OK_TO_CUT_TO_PLAYER BAIL TO MP  T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")#ENDIF
				PRINTLN("[TS] *****************************************************************")
				
				SET_CAMERA_QUICK_EXIT_TO_SP(TRUE)
				SET_MP_PAUSE_MENU_SKYCAM_UP(TRUE)
				RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
				RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
				SET_PAUSE_MENU_REQUESTING_A_WARP()
				SET_PAUSE_MENU_REQUESTING_A_NEW_SESSION()
				SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)
				SET_SCTV_TO_CLEANUP()
				Make_Ped_Sober(PLAYER_PED_ID())
				SET_SOMETHING_QUITTING_MP(TRUE)
				SET_CURRENT_TRANSITION_FM_MENU_CHOICE(FM_MENU_CHOICE_JOIN_NEW_SESSION)
				SET_JOINING_GAMEMODE(GAMEMODE_FM)
				HUD_CHANGE_STATE(HUD_STATE_LOADING)
				TRANSITION_CHANGE_STATE(TRANSITION_STATE_LOOK_TO_JOIN_ANOTHER_SESSION_FM)
				RESET_NET_TIMER(sData.sRestartStuckTimer)
				
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// Gets if the personla vehicle has been created or it is in the process of being created.
FUNC BOOL IS_PERSONAL_VEHICLE_DRIVEABLE_OR_GOING_TO_BE_CREATED()
	
	IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
		IF NOT IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID(TRUE))
			IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION)
			AND NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_OUT_GARAGE)
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC



// *************************************************************************************
// Stuff Bobby has asked to be added, mostly for handling edge case bails from MP when things go wrong and playlists. 

// ****************************************************************
//
// 			DO_TRANSITION_TO_GAME .
//
// ****************************************************************
//Consts for the stages
CONST_INT ciSET_UP_DO_TRANSITION_TO_GAME_INT						0
CONST_INT ciSET_UP_DO_TRANSITION_TO_GAME_WAIT						1
CONST_INT ciSET_UP_DO_TRANSITION_TO_GAME_WAIT_FOR_HOST				2
CONST_INT ciSET_UP_DO_TRANSITION_TO_GAME_WAIT_FOR_OTHER_PLAYERS		3

//Set the status of the transition joining
PROC SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS(INT &iSwitchINT, INT iStatus)
	#IF IS_DEBUG_BUILD
	SWITCH iStatus
		CASE ciSET_UP_DO_TRANSITION_TO_GAME_INT						
			PRINTLN("[TS] TO_GAME - SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS - [PMC] - ciSET_UP_DO_TRANSITION_TO_GAME_INT                    - (", GET_CLOUD_TIME_AS_INT(),")") 
			PRINTLN("TRANSITION TIME - DO_TRANSITION_TO_GAME_INT - [PMC]                     - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
		BREAK
		CASE ciSET_UP_DO_TRANSITION_TO_GAME_WAIT					
			PRINTLN("[TS] TO_GAME - SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS - ciSET_UP_DO_TRANSITION_TO_GAME_WAIT                   - (", GET_CLOUD_TIME_AS_INT(),")") 
			PRINTLN("TRANSITION TIME - DO_TRANSITION_TO_GAME_WAIT - [PMC]                    - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
		BREAK
		CASE ciSET_UP_DO_TRANSITION_TO_GAME_WAIT_FOR_HOST			
			PRINTLN("[TS] TO_GAME - SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS - ciSET_UP_DO_TRANSITION_TO_GAME_WAIT_FOR_HOST          - (", GET_CLOUD_TIME_AS_INT(),")") 
			PRINTLN("TRANSITION TIME - DO_TRANSITION_TO_GAME_WAIT_FOR_HOST - [PMC]           - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
		BREAK
		CASE ciSET_UP_DO_TRANSITION_TO_GAME_WAIT_FOR_OTHER_PLAYERS	
			PRINTLN("[TS] TO_GAME - SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS - ciSET_UP_DO_TRANSITION_TO_GAME_WAIT_FOR_OTHER_PLAYERS - (", GET_CLOUD_TIME_AS_INT(),")")	
			PRINTLN("TRANSITION TIME - DO_TRANSITION_TO_GAME_WAIT_FOR_OTHER_PLAYERS - [PMC]  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
		BREAK
	ENDSWITCH
	#ENDIF
	iSwitchINT = iStatus
ENDPROC

//Reset vars
PROC RESET_DO_TRANSITION_TO_GAME_VARS(DO_TRANSITION_TO_GAME_VARS &sTranToGameVars)
	DO_TRANSITION_TO_GAME_VARS sTranToGameVarsTemp
	sTranToGameVars = sTranToGameVarsTemp
ENDPROC

CONST_INT ciNEW_FREEMODE_DELAY_TIME 4000
//Check to see if I'm the BOSS of transitioning out of the session!
//Basically it usses the player ID order to deterimin who is incharge of 
FUNC BOOL AM_I_NETWORK_DO_TRANSITION_TO_NEW_FREEMODE_GROUP_LEADER()
	INT 			iLoop
	INT 			iParticipant
	PLAYER_INDEX 	piPlayer
	GAMER_HANDLE 	hGamerMe
	GAMER_HANDLE 	hGamerPlayer
	//get me
	hGamerMe = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	//Loop through all other players on this script!
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		//If they are active
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			//Get their gamer handel
			piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			hGamerPlayer = GET_GAMER_HANDLE_PLAYER(piPlayer)
			//IF it's me, then I got to me first so it's me that's in charge!
			IF NETWORK_ARE_HANDLES_THE_SAME(hGamerMe, hGamerPlayer)
			AND (NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].sClientCoronaData.iBitSet, ciCORONA_DEV_SPECTATOR)
			OR piPlayer = PLAYER_ID())	
				//If I've been classed as not the leader
				IF g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader
					//Start a timer 
					IF g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer = 0
						g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer = GET_GAME_TIMER() + g_sMPTunables.iNEW_FREEMODE_DELAY_TIME
						PRINTLN("[TS] TO_GAME - [PMC] - AM_I_NETWORK_DO_TRANSITION_TO_NEW_FREEMODE_GROUP_LEADER - I might be the BOSS, setting timer - g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer = ", g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer)
						RETURN FALSE
					//when we've waited a second
					ELIF GET_GAME_TIMER() >= g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer
						PRINTLN("[TS] TO_GAME - [PMC] - AM_I_NETWORK_DO_TRANSITION_TO_NEW_FREEMODE_GROUP_LEADER - timer expired, I'M now the BOSS - g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer = ", g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer, " - GET_GAME_TIMER() = ", GET_GAME_TIMER())
						//Now I'm the BOSS
						g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer = 0
						g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader = FALSE
						RETURN TRUE
					ELSE
						//Wait
						RETURN FALSE
					ENDIF
				//I've never been not the BOSS!
				ELSE
					RETURN TRUE
				ENDIF
			//It's not me, so check to see if he's in my array
			ELSE
				FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iNumberOfPlayers - 1)
					//If it's valid:
					IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamer.hGamers[iLoop], SIZE_OF(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamer.hGamers[iLoop]))
						//He's in my array!
						IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamer.hGamers[iLoop], hGamerPlayer)
							#IF IS_DEBUG_BUILD
							PRINTLN("[TS] TO_GAME - [PMC] - ", g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.tl23Player[iLoop], " IS THE BOSS OF ME - (", GET_CLOUD_TIME_AS_INT(),")")
							#ENDIF
							//Set that I've been classed as not the leader
							IF NOT g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader
								PRINTLN("[TS] TO_GAME - [PMC] - AM_I_NETWORK_DO_TRANSITION_TO_NEW_FREEMODE_GROUP_LEADER - setting I'm NOT the BOSS")
								g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader = TRUE
							ENDIF
							//I'm not the boss
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR	
			ENDIF			
		ENDIF
	ENDREPEAT
	//Shouldn't get here, but if it does then we'll return TRUE for safties sake!
	RETURN TRUE
ENDFUNC

//returns true if efvery body is ready for a restart 
FUNC BOOL IS_EVERYBODY_READY_FOR_DO_TRANSITION_TO_GAME()
	BOOL			bReturn = TRUE
	INT 			iParticipant
	PLAYER_INDEX 	piPlayer
	//Loop through all other players on this script!
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		//If they are active
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			//Get their gamer handel
			piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			//if they've not set the flag then wait
			IF NOT IS_PLAYER_READY_FOR_DO_TRANSITION_TO_GAME(piPlayer)
			AND NOT IS_PLAYER_SCTV(piPlayer)
				PRINTLN("[TS] TO_GAME - [PMC] - IS_EVERYBODY_READY_FOR_DO_TRANSITION_TO_GAME - Player ", GET_PLAYER_NAME(piPlayer), " is not ready yet")
				bReturn = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
		
	RETURN bReturn 
ENDFUNC

//returns true if efvery body is ready for a restart
FUNC BOOL IS_EVERYBODY_COMING_WITH_ME_READY_FOR_DO_TRANSITION_TO_GAME()
	BOOL			bReturn = TRUE
	INT 			iParticipant
	INT 			iLoop
	PLAYER_INDEX 	piPlayer
	GAMER_HANDLE 	hGamerPlayer
	//Loop through all other players on this script!
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		//If they are active
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			//Get their gamer handel
			piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			hGamerPlayer = GET_GAMER_HANDLE_PLAYER(piPlayer)
			FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iNumberOfPlayers - 1)
				//If it's valid:
				IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamer.hGamers[iLoop], SIZE_OF(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamer.hGamers[iLoop]))
					//He's in my array!
					IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamer.hGamers[iLoop], hGamerPlayer)
						//if they've not set the flag then wait
						IF NOT IS_PLAYER_READY_FOR_DO_TRANSITION_TO_GAME(piPlayer)
						AND NOT IS_PLAYER_SCTV(piPlayer)
							PRINTLN("[TS] TO_GAME - [PMC] - IS_EVERYBODY_READY_FOR_DO_TRANSITION_TO_GAME - Player ", GET_PLAYER_NAME(piPlayer), " is not ready yet")
							bReturn = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDREPEAT		
	RETURN bReturn 
ENDFUNC

//Set up doing a transition to a freemode session 
FUNC BOOL SET_UP_DO_TRANSITION_TO_GAME(DO_TRANSITION_TO_GAME_VARS &sTranToGameVars)
	
	INT iQuitReason
	PLAYER_INDEX piLastCoronaHost
	//Switch the joining stage of the transition session
	SWITCH sTranToGameVars.iTranisitionSessionSetUpStage	
		//Set up the transition
		CASE ciSET_UP_DO_TRANSITION_TO_GAME_INT
			//Doing a quick match because of the tutorial race then set the vote status of quit
			IF SHOULD_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()
				PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - [PMC] - SHOULD_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH = TRUE - Calling SET_TRANSITION_SESSION_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUIT)")
				SET_TRANSITION_SESSION_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUIT)
				SETUP_MATCHMAKING_RULES(GAMEMODE_FM) //added in here due to bug 1864243, where this somehow gets into ciSET_UP_DO_TRANSITION_TO_GAME_WAIT case without hitting any other MM setup. This is the last print so adding it in here. BC: 31/05/2014 
				
			ENDIF
			
			PRINTLN("[TS] TO_GAME - NETWORK_DO_TRANSITION_TO_GAME - [PMC] - SETUP_MATCHMAKING_RULES - (", GET_CLOUD_TIME_AS_INT(),")")
			SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY)
			//Clear up old flags
			IF SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()
				CLEAR_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()
			ENDIF
			IF TRANSITION_SESSION_CAM_PULL_DOWN_WHEN_SPECTATING_READY()
				CLEAR_TRANSITION_SESSION_CAM_PULL_DOWN_WHEN_SPECTATING_READY()
			ENDIF
			//Set up the match making rules // BC : 31/01/2014 commented out so I can move it to the next case. 
//			SETUP_MATCHMAKING_RULES(GAMEMODE_FM)			
			//If I left with every one
			IF NOT DID_I_LEAVE_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
				PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - [PMC] - DID_I_LEAVE_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT = FALSE")
				g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUIT
				CLEAR_TRANSITION_SESSION_ACTIVE_ON_A_MISSION()
			ENDIF
			SET_TRANSITION_SESSIONS_CLEANUP_AFTER_MISSION()
			//If I joined a private session then set some broadcast data to tell everone about it!
			IF DID_I_JOIN_A_PRIVATE_SESSION()
				PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - [PMC] - DID_I_JOIN_A_PRIVATE_SESSION - SET_ME_AS_GOING_PRIVATE_FOR_DO_TRANSITION_TO_FREEMODE")
				SET_ME_AS_GOING_PRIVATE_FOR_DO_TRANSITION_TO_FREEMODE()
			ENDIF
			IF IS_PLAYER_SCTV(PLAYER_ID()) 
			AND IS_ROCKSTAR_DEV() 
			AND NETWORK_IS_PLAYER_ACTIVE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
			AND IS_PLAYER_GOING_PRIVATE_FOR_DO_TRANSITION_TO_FREEMODE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
				PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - [PMC] - IS_PLAYER_GOING_PRIVATE_FOR_DO_TRANSITION_TO_FREEMODE - SET_ME_AS_GOING_PRIVATE_FOR_DO_TRANSITION_TO_FREEMODE - playerSCTVTarget = ", GET_PLAYER_NAME(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget))
				SET_ME_AS_GOING_PRIVATE_FOR_DO_TRANSITION_TO_FREEMODE()
			ENDIF
			//If this is the tutorial transition transition to game we act like it was a continue
			IF SHOULD_TRANSITION_SESSIONS_DO_TUT_TRANSITION_TO_GAME()			
			AND NOT SHOULD_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()
				PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - [PMC] - SHOULD_TRANSITION_SESSIONS_DO_TUT_TRANSITION_TO_GAME = TRUE - Calling REFRESH_TRANSITION_SESSION_PLAYERS_FOR_RANDOM_RESTART")
				//refresh the players in my array
				REFRESH_TRANSITION_SESSION_PLAYERS_FOR_RANDOM_RESTART()
			ENDIF
			
			//Clear that we are doing a quick match if it's already set
			CLEAR_TRANSITION_SESSION_DO_QUICKMATCH_FROM_NJVS()	
			
			//Do set up
			SWITCH GET_TRANSITION_SESSION_VOTE_STATUS()
				//restart random vote status
				CASE ciFMMC_EOM_VOTE_STATUS_RANDOM		
				CASE ciFMMC_EOM_VOTE_STATUS_RESTART		
					//over ride the chat
					NETWORK_OVERRIDE_TRANSITION_CHAT(TRUE)
					PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - [PMC] - NETWORK_OVERRIDE_TRANSITION_CHAT(TRUE)")
//					//rtetain the que of people wanting to join
//					NETWORK_RETAIN_ACTIVITY_JOIN_QUEUE(TRUE)
//					PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - [PMC] - NETWORK_RETAIN_ACTIVITY_JOIN_QUEUE(TRUE)")
					//Set a flag that we are using restart or random
					SET_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
					//If we've voted random
					IF (GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_RANDOM 
					//And it's not blocked:
					AND g_sMPTunables.bDisableNjvsQuickMatch = FALSE
					//And we've not disabled it from the NJVS
					AND ((!GET_PACKED_STAT_BOOL(PACKED_MP_TOGGLE_MATCHMAKING_AFTER_NJVS) AND !IS_PC_VERSION())
					OR (!GET_MP_BOOL_CHARACTER_STAT(MP_STAT_REMAINHOSTOPTION_PC) AND IS_PC_VERSION()))
					//And we didn't come from a private session
					AND NOT DID_I_JOIN_A_PRIVATE_SESSION())
					//Or debug is making us!
					#IF IS_DEBUG_BUILD
					OR((GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_RANDOM) AND GET_COMMANDLINE_PARAM_EXISTS("sc_NjvsQuickMatch"))
					#ENDIF
						//Print the status of the bool
						#IF IS_DEBUG_BUILD
						IF IS_PC_VERSION()
							IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_REMAINHOSTOPTION_PC)
								PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - GET_MP_BOOL_CHARACTER_STAT(MP_STAT_REMAINHOSTOPTION_PC)) = FALSE")
							ENDIF
						ELSE
							IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_TOGGLE_MATCHMAKING_AFTER_NJVS)
								PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - GET_PACKED_STAT_BOOL(PACKED_MP_TOGGLE_MATCHMAKING_AFTER_NJVS)) = FALSE")
							ENDIF
						ENDIF
						#ENDIF
						//Set that we should quick match with a group when we get into the corona
						SET_I_WANT_TO_DO_QUICKMATCH_FROM_NJVS()
					//ELSE
					//	PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - CLEAR_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS")
					//	CLEAR_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
					ENDIF
					//refresh the players in my array
					REFRESH_TRANSITION_SESSION_PLAYERS_FOR_RANDOM_RESTART()
					//don't need to reset controll
					SET_TRANSITION_SESSIONS_SHOULD_NOT_RESET_CONTROL()
					//Set that we are in a corona after a restart/random
					SET_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
				BREAK
				DEFAULT
					PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - [PMC] - GET_TRANSITION_SESSION_VOTE_STATUS() = ", GET_TRANSITION_SESSION_VOTE_STATUS(), " = ", GET_EMO_STATUS_FOR_DEBUG_PRINT(GET_TRANSITION_SESSION_VOTE_STATUS()))								
				BREAK
			ENDSWITCH			
			SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS(sTranToGameVars.iTranisitionSessionSetUpStage, ciSET_UP_DO_TRANSITION_TO_GAME_WAIT_FOR_OTHER_PLAYERS)			
		BREAK
		
		//Wait for the other players to all be ready
		CASE ciSET_UP_DO_TRANSITION_TO_GAME_WAIT_FOR_OTHER_PLAYERS
			IF SHOULD_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()				
				PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - [PMC] - SHOULD_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH = TRUE - ciSET_UP_DO_TRANSITION_TO_GAME_WAIT_FOR_OTHER_PLAYERS")
				//Refresh all the players
				REFRESH_TRANSITION_SESSION_PLAYERS_FOR_QUIT()
				SETUP_MATCHMAKING_RULES(GAMEMODE_FM) //BC : 31/01/2014 Added here and normal params due to not changing Aim type back to original before joining job. Les Todo 1652504 
				SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS(sTranToGameVars.iTranisitionSessionSetUpStage, ciSET_UP_DO_TRANSITION_TO_GAME_WAIT)
			//If this is the tutorial transition transition to game we act like it was a continue
			ELIF SHOULD_TRANSITION_SESSIONS_DO_TUT_TRANSITION_TO_GAME()	
				//If I'm not ready then set me
				IF NOT AM_I_READY_FOR_DO_TRANSITION_TO_GAME()
					SET_ME_AS_READY_FOR_DO_TRANSITION_TO_GAME_RESTART()
					RETURN FALSE
				//wait for everybody
				ELIF NOT IS_EVERYBODY_READY_FOR_DO_TRANSITION_TO_GAME()
					PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - [PMC] - IS_EVERYBODY_READY_FOR_DO_TRANSITION_TO_GAME = FALSE")
					RETURN FALSE
				//then go
				ELSE
					SETUP_MATCHMAKING_RULES(GAMEMODE_FM) //BC : 31/01/2014 Added here and normal params due to not changing Aim type back to original before joining job. Les Todo 1652504 
					SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS(sTranToGameVars.iTranisitionSessionSetUpStage, ciSET_UP_DO_TRANSITION_TO_GAME_WAIT)	
				ENDIF
			//Not the tutorail transiton
			ELSE
				//If this is a solo session then we go alone and take spectators
				IF IS_THIS_A_SOLO_SESSION()
				
					//Refresh all the players
					PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - [PMC] - IS_THIS_A_SOLO_SESSION = TRUE")
					REFRESH_TRANSITION_SESSION_PLAYERS_FOR_QUIT()
					SETUP_MATCHMAKING_RULES(GAMEMODE_FM) //BC : 31/01/2014 Added here and normal params due to not changing Aim type back to original before joining job. Les Todo 1652504 
					SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS(sTranToGameVars.iTranisitionSessionSetUpStage, ciSET_UP_DO_TRANSITION_TO_GAME_WAIT)	
				//Not a solo session then we go together
				ELSE
					SWITCH GET_TRANSITION_SESSION_VOTE_STATUS()
						CASE ciFMMC_EOM_VOTE_STATUS_QUIT
							//Refresh all the players
							REFRESH_TRANSITION_SESSION_PLAYERS_FOR_QUIT()
							SETUP_MATCHMAKING_RULES(GAMEMODE_FM)
							CALL_PLAYSTATS_LEAVE_JOB_CHAIN(ciJOB_CHAIN_LEAVE_REASON_EOJV_QUIT)
							CLEAN_END_OF_MISSION_QUIT_TELEMETRY()
							CV2_SETUP_HIDE_BLIP_HELP()
							SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS(sTranToGameVars.iTranisitionSessionSetUpStage, ciSET_UP_DO_TRANSITION_TO_GAME_WAIT)	
						BREAK
						
						//continue to vote status
						CASE ciFMMC_EOM_VOTE_STATUS_CONTINUE														
							//If I'm not ready then set me
							IF NOT AM_I_READY_FOR_DO_TRANSITION_TO_GAME()
								SET_ME_AS_READY_FOR_DO_TRANSITION_TO_GAME_RESTART()
								RETURN FALSE
							//wait for everybody
							ELIF NOT IS_EVERYBODY_COMING_WITH_ME_READY_FOR_DO_TRANSITION_TO_GAME()
								PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - [PMC] - IS_EVERYBODY_READY_FOR_DO_TRANSITION_TO_GAME = FALSE")
								RETURN FALSE
							//then go
							ELSE
								//If it's continue then only people that I came in with come, or spectators
								SETUP_MATCHMAKING_RULES(GAMEMODE_FM) //BC : 31/01/2014 Added here and normal params due to not changing Aim type back to original before joining job. Les Todo 1652504 
								IF DID_I_JOIN_A_PRIVATE_SESSION()
									REFRESH_TRANSITION_SESSION_PLAYERS_I_CAME_IN_WITH_ON_RETURN_TO_PRIVATE_FREEMODE(iQuitReason, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene)
									//If we're SCTV 
									IF IS_PLAYER_SCTV(PLAYER_ID())
									//And it's only us that's going back to the session
									AND g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iNumberOfPlayers = 1
									//And we're not a rockstar DEV
									AND NOT IS_ROCKSTAR_DEV()
										//Then clean up that we are going private
										g_sTransitionSessionData.bJoinedPrivateSession = FALSE
									ENDIF
								ELSE
									REFRESH_TRANSITION_SESSION_PLAYERS_I_CAME_IN_WITH_ON_RETURN_TO_FREEMODE(iQuitReason, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene)
								ENDIF
								CV2_SETUP_HIDE_BLIP_HELP()
								CLEAN_END_OF_MISSION_QUIT_TELEMETRY()
								CALL_PLAYSTATS_LEAVE_JOB_CHAIN(iQuitReason)
								SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS(sTranToGameVars.iTranisitionSessionSetUpStage, ciSET_UP_DO_TRANSITION_TO_GAME_WAIT)	
							ENDIF
						BREAK
						
						//restart random vote status
						CASE ciFMMC_EOM_VOTE_STATUS_RANDOM		
						CASE ciFMMC_EOM_VOTE_STATUS_RESTART	
							//If we are going random
							IF GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_RANDOM
							AND NOT SHOULD_TRANSITION_SESSION_DO_QUICKMATCH_FROM_NJVS()
							#IF IS_DEBUG_BUILD
							AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_NjvsRandomNoQm")
							#ENDIF
								//get the last vote captain
								piLastCoronaHost = INT_TO_PLAYERINDEX(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iLastVoteCaptain)
								PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - piLastCoronaHost = INT_TO_PLAYERINDEX(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iLastVoteCaptain) - GlobalplayerBD_FM[", NATIVE_TO_INT(PLAYER_ID()), "].iLastVoteCaptain = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iLastVoteCaptain)
								//if they are active
								IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iLastVoteCaptain != -1
								AND NETWORK_IS_PLAYER_ACTIVE(piLastCoronaHost)
									PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - NETWORK_IS_PLAYER_ACTIVE(piLastCoronaHost) - piLastCoronaHost = ", GET_PLAYER_NAME(piLastCoronaHost))
									//and they want to do a quick match from the NJVS
									IF PLAYER_DOING_QUICKMATCH_FROM_NJVS(piLastCoronaHost)
										PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - PLAYER_DOING_QUICKMATCH_FROM_NJVS(", GET_PLAYER_NAME(piLastCoronaHost),")")
										//then set that's what we're doing
										SET_TRANSITION_SESSION_DO_QUICKMATCH_FROM_NJVS()
									ELIF (IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
									AND GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_RANDOM
									AND SHOULD_ONLY_CONSIDER_PLAYER_FOR_TO_GAME_RANDOM_RESTART(PLAYER_ID()))
										PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - we're doing a quick match on our own")
										//then set that's what we're doing
										SET_TRANSITION_SESSION_DO_QUICKMATCH_FROM_NJVS()
										
									//	PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - CLEAR_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS")
									//	CLEAR_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
									ENDIF
								//not active, then set any way
								ELSE
									PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - NETWORK_IS_PLAYER_ACTIVE() = FALSE")
									SET_TRANSITION_SESSION_DO_QUICKMATCH_FROM_NJVS()										
								ENDIF
							ENDIF
							//If I'm not ready then set me
							IF NOT AM_I_READY_FOR_DO_TRANSITION_TO_GAME()
								SET_ME_AS_READY_FOR_DO_TRANSITION_TO_GAME_RESTART()
								RETURN FALSE
							//wait for everybody
							ELIF NOT IS_EVERYBODY_READY_FOR_DO_TRANSITION_TO_GAME()
								PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - [PMC] - IS_EVERYBODY_READY_FOR_DO_TRANSITION_TO_GAME = FALSE")
								RETURN FALSE
							//then go
							ELSE
								SETUP_MATCHMAKING_RULES(GAMEMODE_FM, -1, TRUE) //BC : 31/01/2014 Added here with extra params due to changing Aim type back to original before joining job. i.e. quiting job back to freemode. Les Todo 1652504 
								SET_SET_UP_DO_TRANSITION_TO_GAME_STATUS(sTranToGameVars.iTranisitionSessionSetUpStage, ciSET_UP_DO_TRANSITION_TO_GAME_WAIT)	
							ENDIF
						BREAK
						DEFAULT
							PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - [PMC] - GET_TRANSITION_SESSION_VOTE_STATUS() = ", GET_TRANSITION_SESSION_VOTE_STATUS(), " = ", GET_EMO_STATUS_FOR_DEBUG_PRINT(GET_TRANSITION_SESSION_VOTE_STATUS()))						
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		BREAK
		
		CASE ciSET_UP_DO_TRANSITION_TO_GAME_WAIT	
			//If we are doing a quick match on our own then do a transition to a new freemode.
			IF SHOULD_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()
				PRINTLN("[TS] TO_GAME - SET_UP_DO_TRANSITION_TO_GAME - [PMC] - SHOULD_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH = TRUE - ciSET_UP_DO_TRANSITION_TO_GAME_WAIT")
				IF IS_TRANSITION_SESSION_ACTIVE_ON_A_MISSION()
					CLEAR_TRANSITION_SESSION_ACTIVE_ON_A_MISSION()
				ENDIF
				CALL_NETWORK_SESSION_SET_GAMEMODE_BEFORE_TO_GAME()
				IF NOT NETWORK_IS_TRANSITION_TO_GAME()
				AND NETWORK_DO_TRANSITION_TO_FREEMODE(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamerRestartRandom, g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iNumberOfPlayersForRestartRandom, TRUE, NUM_NETWORK_PLAYERS)
					PRINTLN("[TS] TO_GAME - QUIT - [PMC] - NETWORK_DO_TRANSITION_TO_FREEMODE = TRUE - (", GET_CLOUD_TIME_AS_INT(),")")					
					RETURN TRUE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[TS] TO_GAME - QUIT - [PMC] - NETWORK_DO_TRANSITION_TO_FREEMODE = FALSE")					
				#ENDIF
				ENDIF		
				
			//If this is the tutorial transition transition to game we act like it was a continue
			ELIF SHOULD_TRANSITION_SESSIONS_DO_TUT_TRANSITION_TO_GAME()	
				IF NETWORK_IS_HOST()
					IF g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader
						IF g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer = 0
							PRINTLN("[TS] TO_GAME - [PMC] - RESTART/RANDOM - I might be the BOSS, setting timer")
							g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer = GET_GAME_TIMER() + ciNEW_FREEMODE_DELAY_TIME
							RETURN FALSE
						//when we've waited a second
						ELIF GET_GAME_TIMER() >= g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer
							PRINTLN("[TS] TO_GAME - [PMC] - RESTART/RANDOM - timer expired, I'M now the BOSS")
							//Now I'm the BOSS
							g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer = 0
							g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader = FALSE
							RETURN FALSE
						ELSE
							//Wait
							RETURN FALSE
						ENDIF
					ELSE
						CALL_NETWORK_SESSION_SET_GAMEMODE_BEFORE_TO_GAME()
						IF NOT NETWORK_IS_TRANSITION_TO_GAME()
						AND NETWORK_DO_TRANSITION_TO_FREEMODE(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamerRestartRandom, g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iNumberOfPlayersForRestartRandom, FALSE, NUM_NETWORK_PLAYERS)
							PRINTLN("[TS] TO_GAME - [PMC] - RESTART/RANDOM - NETWORK_DO_TRANSITION_TO_FREEMODE = TRUE - (", GET_CLOUD_TIME_AS_INT(),")")		
							RETURN TRUE
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[TS] TO_GAME - [PMC] - RESTART/RANDOM - NETWORK_DO_TRANSITION_TO_FREEMODE = FALSE")					
						#ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader
						PRINTLN("[TS] TO_GAME - [PMC] - RESTART/RANDOM  - setting I'm NOT the BOSS")
						g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader = TRUE
					ENDIF						
				ENDIF
			//Not the tutorail transiton
			ELSE	
				//If this is a solo session then we go on our own. 					
				IF IS_THIS_A_SOLO_SESSION()
					IF IS_TRANSITION_SESSION_ACTIVE_ON_A_MISSION()
						CLEAR_TRANSITION_SESSION_ACTIVE_ON_A_MISSION()
					ENDIF
					CALL_NETWORK_SESSION_SET_GAMEMODE_BEFORE_TO_GAME()
					IF NETWORK_DO_TRANSITION_TO_NEW_FREEMODE(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamerRestartRandom, g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iNumberOfPlayersForRestartRandom, ciMAX_SOLO_SESSION_SLOTS, TRUE)
						PRINTLN("[TS] TO_GAME - QUIT - [PMC] - IS_THIS_A_SOLO_SESSION-  NETWORK_DO_TRANSITION_TO_NEW_FREEMODE = TRUE - (", GET_CLOUD_TIME_AS_INT(),")")					
						PRINTLN("TRANSITION TIME - NETWORK_DO_TRANSITION_TO_NEW_FREEMODE  QUIT  - [PMC] - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
						RETURN TRUE
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[TS] TO_GAME - QUIT - [PMC] - DID_I_JOIN_A_PRIVATE_SESSION - NETWORK_DO_TRANSITION_TO_NEW_FREEMODE = FALSE")					
					#ENDIF
					ENDIF	
				//Not a solo session
				ELSE
					//Switch The vote status
					SWITCH GET_TRANSITION_SESSION_VOTE_STATUS()
						// back via quickmatch, with social matchmaking
						CASE ciFMMC_EOM_VOTE_STATUS_QUIT
							IF IS_TRANSITION_SESSION_ACTIVE_ON_A_MISSION()
								CLEAR_TRANSITION_SESSION_ACTIVE_ON_A_MISSION()
							ENDIF
							IF DID_I_JOIN_A_PRIVATE_SESSION()
								CALL_NETWORK_SESSION_SET_GAMEMODE_BEFORE_TO_GAME()
								IF NETWORK_DO_TRANSITION_TO_NEW_FREEMODE(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamerRestartRandom, g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iNumberOfPlayersForRestartRandom, NUM_NETWORK_PLAYERS, TRUE)
									PRINTLN("[TS] TO_GAME - QUIT - [PMC] - DID_I_JOIN_A_PRIVATE_SESSION-  NETWORK_DO_TRANSITION_TO_NEW_FREEMODE = TRUE - (", GET_CLOUD_TIME_AS_INT(),")")					
									PRINTLN("TRANSITION TIME - NETWORK_DO_TRANSITION_TO_NEW_FREEMODE  QUIT  - [PMC] - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
									RETURN TRUE
								#IF IS_DEBUG_BUILD
								ELSE
									PRINTLN("[TS] TO_GAME - QUIT - [PMC] - DID_I_JOIN_A_PRIVATE_SESSION - NETWORK_DO_TRANSITION_TO_NEW_FREEMODE = FALSE")					
								#ENDIF
								ENDIF								
							ELSE
								CALL_NETWORK_SESSION_SET_GAMEMODE_BEFORE_TO_GAME()
								IF NOT NETWORK_IS_TRANSITION_TO_GAME()
								AND NETWORK_DO_TRANSITION_TO_FREEMODE(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamerRestartRandom, g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iNumberOfPlayersForRestartRandom, TRUE, NUM_NETWORK_PLAYERS)
									PRINTLN("[TS] TO_GAME - QUIT - [PMC] - NETWORK_DO_TRANSITION_TO_FREEMODE = TRUE - (", GET_CLOUD_TIME_AS_INT(),")")					
									RETURN TRUE
								#IF IS_DEBUG_BUILD
								ELSE
									PRINTLN("[TS] TO_GAME - QUIT - [PMC] - NETWORK_DO_TRANSITION_TO_FREEMODE = FALSE")					
								#ENDIF
								ENDIF		
							ENDIF		
						BREAK	
						
						//To a new freemode session
						CASE ciFMMC_EOM_VOTE_STATUS_CONTINUE
							//Clear that I'm active on a mission
							IF IS_TRANSITION_SESSION_ACTIVE_ON_A_MISSION()
								CLEAR_TRANSITION_SESSION_ACTIVE_ON_A_MISSION()
							ENDIF
							//If I'm incharge of this corona
							
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_DEV_SPECTATOR)
								IF g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iDevSpecTimer = 0
									g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iDevSpecTimer  = GET_GAME_TIMER() + g_sMPTunables.iNEW_FREEMODE_DELAY_TIME/2
									PRINTLN("[TS] TO_GAME - IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID)].sClientCoronaData.iBitSet, ciCORONA_DEV_SPECTATOR) - g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iDevSpecTimer = ", g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iDevSpecTimer)					
									RETURN FALSE
								ELIF GET_GAME_TIMER() <= g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iDevSpecTimer
									PRINTLN("[TS] TO_GAME - IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID)].sClientCoronaData.iBitSet, ciCORONA_DEV_SPECTATOR) ")					
									RETURN FALSE
								ENDIF
							ENDIF
							
							IF AM_I_NETWORK_DO_TRANSITION_TO_NEW_FREEMODE_GROUP_LEADER()
								IF DID_I_JOIN_A_PRIVATE_SESSION()
									// this should be gamers that came with me
									// private freemode
									CALL_NETWORK_SESSION_SET_GAMEMODE_BEFORE_TO_GAME()
									IF NETWORK_DO_TRANSITION_TO_NEW_FREEMODE(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamer, g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iNumberOfPlayers, NUM_NETWORK_PLAYERS, TRUE)
										PRINTLN("[TS] TO_GAME - [PMC] - GROUP_IS_PRIVATE = TRUE - NETWORK_DO_TRANSITION_TO_NEW_FREEMODE = TRUE - (", GET_CLOUD_TIME_AS_INT(),")")					
										PRINTLN("TRANSITION TIME - NETWORK_DO_TRANSITION_TO_NEW_FREEMODE  CONT  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
										RETURN TRUE
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[TS] TO_GAME - [PMC] - GROUP_IS_PRIVATE = TRUE - NETWORK_DO_TRANSITION_TO_NEW_FREEMODE = FALSE")					
									#ENDIF
									ENDIF
								ELSE
									// this should be gamers that came with me
									// with social matchmaking
									CALL_NETWORK_SESSION_SET_GAMEMODE_BEFORE_TO_GAME()
									IF NOT NETWORK_IS_TRANSITION_TO_GAME()
									AND NETWORK_DO_TRANSITION_TO_FREEMODE(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamer, g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iNumberOfPlayers, TRUE, NUM_NETWORK_PLAYERS)
										PRINTLN("[TS] TO_GAME - [PMC] - GROUP_IS_PRIVATE = FALSE - NETWORK_DO_TRANSITION_TO_FREEMODE = TRUE - (", GET_CLOUD_TIME_AS_INT(),")")					
										PRINTLN("TRANSITION TIME - NETWORK_DO_TRANSITION_TO_NEW_FREEMODE  CONT  - [PMC] - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
										RETURN TRUE
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[TS] TO_GAME - [PMC] - GROUP_IS_PRIVATE = FALSE - NETWORK_DO_TRANSITION_TO_FREEMODE = FALSE")					
									#ENDIF
									ENDIF
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
							#ENDIF
								PRINTLN("[TS] TO_GAME - [PMC] - AM_I_NETWORK_DO_TRANSITION_TO_NEW_FREEMODE_GROUP_LEADER = FALSE")	
							ENDIF
						BREAK	
						
						// as host, this should be all the gamers that are in the session
						// back to new freemode - not private	
						CASE ciFMMC_EOM_VOTE_STATUS_RANDOM		
						CASE ciFMMC_EOM_VOTE_STATUS_RESTART
							IF NETWORK_IS_HOST()
								IF g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader
									IF g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer = 0
										PRINTLN("[TS] TO_GAME - RESTART/RANDOM - [PMC] - I might be the BOSS, setting timer")
										g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer = GET_GAME_TIMER() + ciNEW_FREEMODE_DELAY_TIME
										RETURN FALSE
									//when we've waited a second
									ELIF GET_GAME_TIMER() >= g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer
										PRINTLN("[TS] TO_GAME - [PMC] - RESTART/RANDOM - timer expired, I'M now the BOSS")
										//Now I'm the BOSS
										g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iTimer = 0
										g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader = FALSE
										RETURN FALSE
									ELSE
										//Wait
										RETURN FALSE
									ENDIF
								ELSE
									CALL_NETWORK_SESSION_SET_GAMEMODE_BEFORE_TO_GAME()
									IF NETWORK_DO_TRANSITION_TO_NEW_FREEMODE(g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.sGamerRestartRandom, g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.iNumberOfPlayersForRestartRandom, NUM_NETWORK_PLAYERS, TRUE, FALSE, ENUM_TO_INT(HOST_FLAG_JOB_TO_JOB))
										PRINTLN("[TS] TO_GAME - RESTART/RANDOM - [PMC] - NETWORK_DO_TRANSITION_TO_NEW_FREEMODE = TRUE - (", GET_CLOUD_TIME_AS_INT(),")")		
										PRINTLN("TRANSITION TIME - NETWORK_DO_TRANSITION_TO_NEW_FREEMODE  R/R    - [PMC] - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
										RETURN TRUE
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[TS] TO_GAME - RESTART/RANDOM - [PMC] - NETWORK_DO_TRANSITION_TO_NEW_FREEMODE = FALSE")					
									#ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader
									PRINTLN("[TS] TO_GAME - RESTART/RANDOM  - [PMC] - setting I'm NOT the BOSS")
									g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bNotLeader = TRUE
								ENDIF						
							ENDIF
						BREAK		
						
						//nothing
						CASE ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART	
						CASE ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP	
						BREAK
						DEFAULT
							PRINTLN("[TS] NETWORK_DO_TRANSITION_TO_GAME - [PMC] - GET_TRANSITION_SESSION_VOTE_STATUS() = ", GET_TRANSITION_SESSION_VOTE_STATUS(), " = ", GET_EMO_STATUS_FOR_DEBUG_PRINT(GET_TRANSITION_SESSION_VOTE_STATUS()))								
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// End of stuff bobby asked to be added for session bails and playlists.
// *************************************************************************************



// Are we doing the tutorial cutscene?
FUNC BOOL IS_TUTORIAL_CUTSCENE_SETUP()
	
	BOOL bShouldbeDoingTutCleanup = GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP()
	BOOL bShouldSpawnAtLamarsHouse = SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE()
	BOOL bCreatedCutsceneAssets = HAS_LOCAL_PLAYER_CREATED_LAMAR_CUT_ASSETS()
	
	// Print function data.
	#IF IS_DEBUG_BUILD
		PRINTLN("[PMC] - GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP() = ", bShouldbeDoingTutCleanup)
		PRINTLN("[PMC] - SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE() = ", bShouldSpawnAtLamarsHouse)
		PRINTLN("[PMC] - HAS_LOCAL_PLAYER_CREATED_LAMAR_CUT_ASSETS() = ", bCreatedCutsceneAssets)
		PRINTLN("[PMC] - g_b_ForceTutorial = ", g_b_ForceTutorial)
	#ENDIF
	
	// Run checks for skipping out because we don't need the cutscene.
	IF NOT bShouldbeDoingTutCleanup
		RETURN TRUE
	ENDIF
	
	IF bShouldSpawnAtLamarsHouse
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_b_ForceTutorial
		RETURN TRUE
	ENDIF
	#ENDIF
	
	// If got this far we need to load the cutscene assets.
    IF bCreatedCutsceneAssets
		RETURN TRUE
    ENDIF
	
	// Cutscene not setup yet.
	RETURN FALSE
	
ENDFUNC

// clears passive mode and unsets action mode.
PROC CLEAR_ACTION_AND_PASSIVE_MODE()
	
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	//DISABLE_MP_PASSIVE_MODE()
	SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
	
	//Make Sure Player is put into Passive Mode if flag is set
	IF g_PassiveModeStored.bEnabled = TRUE
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
		AND NOT HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
		AND NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
			PRINTLN("[PMC] - g_PassiveModeStored.bEnabled = TRUE - PUT PLAYER INTO PASSIVE MODE")
			ENABLE_MP_PASSIVE_MODE(PMER_END_OF_MATCH)
		ELSE
			PRINTLN("[PMC] - DISABLE PASSIVE MODE")
			DISABLE_MP_PASSIVE_MODE(PMER_END_OF_MATCH, FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CONTACT_MISSION_DISTANCE_CHECK(POST_MISSION_CLEANUP_DATA &sData)
	
	IF NOT sData.bBeenOnStandardHeistOrContactMission	
		RETURN TRUE
	ELSE
//		IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(NET_TO_VEH(PERSONAL_VEHICLE_NET_ID(TRUE)))) <= 100.0
		IF g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bFinishedInPersonalVehicle
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// Function added at request of Bobby handling bailing transitions/sessions. 
FUNC BOOL SHOULD_CLEANUP_BAIL_FOR_TRANSITION_SESSIONS_INVITE()
	
	IF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
		RETURN TRUE
	ENDIF
	
	IF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
		RETURN TRUE
	ENDIF
	
	IF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
		RETURN TRUE
	ENDIF
	
	IF IS_MAINTRANSITION_PROCESSING_AN_SCTV_TRANSITION_INVITE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// Should we clear the screen of renderphases, effects etc?
FUNC BOOL SHOULD_VISUAL_CLEANUP_BE_CALLED()
	
	IF SHOULD_CLEANUP_BAIL_FOR_TRANSITION_SESSIONS_INVITE()
		PRINTLN("[PMC] - SHOULD_VISUAL_CLEANUP_BE_CALLED - SHOULD_CLEANUP_BAIL_FOR_TRANSITION_SESSIONS_INVITE() = TRUE")
		RETURN FALSE
	ENDIF
	
	IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
		PRINTLN("[PMC] - SHOULD_VISUAL_CLEANUP_BE_CALLED - GET_CORONA_STATUS() = CORONA_STATUS_IDLE")
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bJumpingToCleanupForSp
		PRINTLN("[PMC] - SHOULD_VISUAL_CLEANUP_BE_CALLED - bJumpingToCleanupForSp")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// Gets if the player will be going into a tutorial mission.
FUNC BOOL IS_PLAYER_ABOUT_TO_START_TUTORIAL_MISSION()
	IF HAS_FM_RACE_TUT_BEEN_DONE()
		IF NOT HAS_FM_TRIGGER_TUT_BEEN_DONE()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Should we unpause the renderphases?
FUNC BOOL SHOULD_SCREEN_BE_UNBLURRED()
	
	IF SHOULD_CLEANUP_BAIL_FOR_TRANSITION_SESSIONS_INVITE()
		RETURN TRUE
	ENDIF
	
	IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
		RETURN FALSE
	ENDIF
	
	// For Dave W.
	IF IS_PLAYER_ABOUT_TO_START_TUTORIAL_MISSION()
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// Should we unpause the renderphases?
FUNC BOOL SHOULD_RENDERPHASES_BE_UNFROZEN()
	
	// For Dave W.
	IF IS_PLAYER_ABOUT_TO_START_TUTORIAL_MISSION()
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_LB_POST_FX_STOP()
	
	// For Dave W.
	IF IS_PLAYER_ABOUT_TO_START_TUTORIAL_MISSION()
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// Quickly moves the player to the passed in coords. Should only be used in special cases, Neil's warp command should usually be used.
PROC MOVE_PLAYER_TO_COORDS(VECTOR vMoveToLocation)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vMoveToLocation)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		ENDIF
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
		PRINTLN("[PMC] - TRANSITION TIME - moved player to coords: ", vMoveToLocation)
	ENDIF
	
ENDPROC

// Gets the coords we need to load the scene around for the post mission cleanup.
FUNC BOOL GET_LOAD_SCENE_COORDS(POST_MISSION_CLEANUP_DATA &sData)
	
	PLAYER_SPAWN_LOCATION eTempSpawnLocation
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		PRINTLN("[PMC] - GET_LOAD_SCENE_COORDS TIME - returning TRUE and skipping getting load scene coords or moving player. We are in SCTV, it will handle this.")
		sData.bGotLoadSceneCoords = TRUE	
		RETURN TRUE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != (-1) // Lowrider lamar mission post mission scene.
		PRINTLN("[PMC] - GET_LOAD_SCENE_COORDS TIME - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != (-1), setting bGotLoadSceneCoords = TRUE and not doing anuything.")
		sData.bGotLoadSceneCoords = TRUE	
		RETURN TRUE
	ENDIF
	
	IF NOT sData.bGotLoadSceneCoords
		
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
			sData.bGotLoadSceneCoords = TRUE
			RETURN TRUE
		ENDIF
		
		// If going to a corona, use the corona.
		IF g_sFMMCEOM.bDoWarp
			sData.vLoadSceneCoords = g_sFMMCEOM.vWarp
			sData.bGotLoadSceneCoords = TRUE
			MOVE_PLAYER_TO_COORDS(sData.vLoadSceneCoords)
			RETURN TRUE
		ENDIF
		
		// If going into saved vehicle, use it's position.
		IF NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos)
			sData.vLoadSceneCoords = g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos
			sData.bGotLoadSceneCoords = TRUE
			MOVE_PLAYER_TO_COORDS(sData.vLoadSceneCoords)
			RETURN TRUE
		ENDIF
		
		// If going on foot or in personal vehicle.
		SETUP_FMMC_POST_ACTIVITY_CLEANUP_WARP_LOCATION(sData, eTempSpawnLocation, TRUE)
		IF IS_VECTOR_ZERO(sData.vLoadSceneCoords)
			#IF IS_DEBUG_BUILD
				NET_SCRIPT_ASSERT("[WJK] - GET_LOAD_SCENE_COORDS - sData.vLoadSceneCoords still zero after all checks.")
			#ENDIF
			sData.vLoadSceneCoords = GET_PLAYER_COORDS(PLAYER_ID())
		ENDIF
		
		sData.bGotLoadSceneCoords = TRUE	
		MOVE_PLAYER_TO_COORDS(sData.vLoadSceneCoords)
		RETURN TRUE
		
	ENDIF
	
	RETURN sData.bGotLoadSceneCoords
	
ENDFUNC

FUNC BOOL BAIL_MISSION_CLEANUP_CORONA_STATUS_CHECK()
	
	CORONA_STATUS_ENUM eStatus = GET_CORONA_STATUS()
	
	IF eStatus = CORONA_STATUS_WALK_OUT
	OR eStatus = CORONA_STATUS_RESET_POSITION
	OR eStatus = CORONA_STATUS_WARP_TO_SAFE_LOC
	OR eStatus = CORONA_STATUS_KICKED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IsSwoopDownAlmostFinished()
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS() 
		IF GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
			IF GET_PLAYER_SWITCH_STATE() > SWITCH_STATE_OUTRO_HOLD
				RETURN(TRUE)
			ENDIF
		ENDIF
	ELSE
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
	
ENDFUNC

FUNc BOOL UNFREEZE_RECREATED_AMBIENT_EOM_CAR()
	
	IF DOES_ENTITY_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze)
		PRINTLN("[PMC] - viAfterMissionWarpVehToUnfreeze exists.")
		IF NOT IS_ENTITY_DEAD(g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze)
			PRINTLN("[PMC] - viAfterMissionWarpVehToUnfreeze is not dead.")
			IF NETWORK_HAS_CONTROL_OF_ENTITY(g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze)
			 	PRINTLN("[PMC] - viAfterMissionWarpVehToUnfreeze is in my control.")
				IF HAS_COLLISION_LOADED_AROUND_ENTITY(g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze)
					PRINTLN("[PMC] - viAfterMissionWarpVehToUnfreeze has collision loaded around it.")
					FREEZE_ENTITY_POSITION(g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze, FALSE)
					SET_VEHICLE_ON_GROUND_PROPERLY(g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze)
					ACTIVATE_PHYSICS(g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze)
					SET_ENTITY_DYNAMIC(g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze, TRUE)
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL REACTIVATE_PLAYER_FOR_END_OF_CLEANUP()
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
	OR IS_BIT_SET(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DOING_TRANSITION_INTO_SIMPLE_INTERIOR_VIA_SKYCAM_QUICKWARP)
		RETURN TRUE
	ENDIF
	
	FULLY_ACTIVATE_PLAYER()
	
	// Unfreeze the player.
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
			IF NOT IS_MP_WEAPON_PURCHASED(GADGETTYPE_PARACHUTE, -1)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0) // Remove parachute. B*1603513.
			ENDIF
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF UNFREEZE_RECREATED_AMBIENT_EOM_CAR()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC HANDLE_CREATE_SUVIVAL_REWARDS_VEHICLES_DATA()
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bComingOutOfSuccessfulSurvival
		IF NOT g_sFMMCEOM.bDoWarp
			mpglobals.bDoCreateSurvivalRewardVehicles = TRUE
			PRINTLN("[PMC] - coming ou of successful survival, setting mpglobals.bDoCreateSurvivalRewardVehicles = TRUE.")
		ELSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bComingOutOfSuccessfulSurvival = FALSE
			PRINTLN("[PMC] - coming ou of successful survival, bDoWarp = TRUE, resetting.")
		ENDIF
	ENDIF

ENDPROC

PROC MAKE_LOCAL_PLAYER_VISIBLE_THIS_FRAME()
	
	// Player visible when we cut.
	IF NOT g_sFMMCEOM.bDoWarp
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF NETWORK_IS_GAME_IN_PROGRESS()                                
	        	SET_PLAYER_VISIBLE_LOCALLY(PLAYER_ID()) 
	    	ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC RESET_TEMP_REZZED_ELSEWHERE_DATA_BUG_1717705() // Failsafe timer for bug 1717705. Temp fix until Neil is back and can look into a proper fix. #LimboBug
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRezzedElsewhere_Bug1717705 = FALSE
	RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stRezzedElsewhereTimer_Bug1717705)
ENDPROC						

PROC CLEAR_INITIAL_SPAWN_LOCATION(INT iCall)
	
	IF (iCall = iCall)
		iCall = iCall
	ENDIF
	
	g_vInitialSpawnLocation = << 0.0, 0.0, 0.0 >>
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[PMC] - cleared g_vInitialSpawnLocation - ", g_vInitialSpawnLocation, " - call ", iCall)
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
ENDPROC



// ****************************************************************
//
// 			DO_FMMC_POST_ACTIVITY_CLEANUP .
//
// ****************************************************************
FUNC BOOL DO_FMMC_POST_ACTIVITY_CLEANUP(POST_MISSION_CLEANUP_DATA &sData)
	
	BOOL bSortedPersonalVehicle
//	FLOAT fLastVehHeading
//	VEHICLE_INDEX tempveh
//	MODEL_NAMES vehmodel
	//INT i
	
	STOP_CONTROL_SHAKE(Player_control) // No shaky pads during the cleanup - B*1837406
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPausePvCreation
		DISABLE_PERSONAL_VEHICLE_CREATION_THIS_FRAME()
	ENDIF
	
					
	// If we need to bail out and reset, do so.
	IF BAIL_MISSION_CLEANUP_CORONA_STATUS_CHECK()
		
		sData.iMaintainStage = 3
		PRINTLN("[PMC] - sData.iMaintainStage = 3 call 0.")
		PRINTLN("[PMC] - BAIL_MISSION_CLEANUP_CORONA_STATUS_CHECK() = TRUE, setting main stage to stage 3 (reset stage).")
		
	ELIF SHOULD_CLEANUP_BAIL_FOR_TRANSITION_SESSIONS_INVITE()
		
		IF sData.eStage != ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP
			PRINTLN("[PMC] - SHOULD_CLEANUP_BAIL_FOR_TRANSITION_SESSIONS_INVITE = TRUE, cleaning up post mission cleanup.")
			CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT() 
			CLEAR_SPAWNING_MISSION_DATA()
			sData.eStage = ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP
		ENDIF	
		
	ELIF (	SHOULD_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
			OR TRANSITION_SESSION_QUICK_MATCH_RANDOM_TRY_STUNT()
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_END_OF_JOB_SCREEN
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NJVS_QM_JOIN
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NJVS_QM_HOST		)

	AND NOT NETWORK_IS_ACTIVITY_SESSION()
	AND NOT SHOULD_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()
		
		IF sData.eStage != ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP
			#IF IS_DEBUG_BUILD
			IF SHOULD_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
				PRINTLN("[PMC] [TS] [NJVSQMR] - SHOULD_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS = TRUE, cleaning up post mission cleanup.")
			ENDIF
			IF TRANSITION_SESSION_QUICK_MATCH_RANDOM_TRY_STUNT()
				PRINTLN("[PMC] [TS] [NJVSQMR] - TRANSITION_SESSION_QUICK_MATCH_RANDOM_TRY_STUNT = TRUE, cleaning up post mission cleanup.")
			ENDIF
			IF GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_END_OF_JOB_SCREEN
				PRINTLN("[PMC] [TS] [NJVSQMR] - GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_END_OF_JOB_SCREEN, cleaning up post mission cleanup.")
			ENDIF
			IF GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NJVS_QM_JOIN
				PRINTLN("[PMC] [TS] [NJVSQMR] - GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NJVS_QM_JOIN, cleaning up post mission cleanup.")
			ENDIF
			IF GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NJVS_QM_HOST
				PRINTLN("[PMC] [TS] [NJVSQMR] - GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NJVS_QM_HOST, cleaning up post mission cleanup.")
			ENDIF	
			#ENDIF
			CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT() 
			CLEAR_SPAWNING_MISSION_DATA()
			sData.eStage = ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP
		ENDIF
		
	ELSE
	
		// *****************************************************************
		// Save required vars to saved globals until free mode terminates.
		// Once freemode has terminated, force the cleanup into the dead
		// stage where it will handle waiting for freemode to restart and 
		// moving on from there.
		// *****************************************************************
		 
		IF (sData.iForceIntoFreemodeDeadStage = 0)
		
			// Save copy of the required variables.
			sData.BbNeedsFmCleanup_Copy = TRUE
			sData.bComingOutOfShootingRange_Copy[SHOOTING_RANGE_1] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_1]
			sData.bComingOutOfShootingRange_Copy[SHOOTING_RANGE_2] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_2]
			sData.eManualRespawnState_Copy = GET_MANUAL_RESPAWN_STATE()
			sData.iFmTutProgBitset_Copy = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset
			
		ELIF (sData.iForceIntoFreemodeDeadStage = 1) // This is set in x:\gta5\script\dev\singleplayer\scripts\main\main_persistent.sc by the func MAINTAIN_RELAUNCHING_FREEMODE_AFTER_TRANSITION_TO_GAME_JOINED().
			
			IF sData.eStage < ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD
				PRINTLN("[PMC] - sData.bForceIntoFreemodeDeadStage = 1.")
				GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD)
			ENDIF
			
		ENDIF
	
	ENDIF
	
	// if post mission cleanup is ready to do swoop to player let the hanging in sky know
	#IF FEATURE_SHORTEN_SKY_HANG
	IF (sData.eStage = ePOSTMISSIONCLEANUPSTAGE_CUT_TO_PLAYER)
	OR (sData.eStage = ePOSTMISSIONCLEANUPSTAGE_FADE_IN)
	OR (sData.eStage = ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP)
		PRINTLN("[PMC] [HangingInSky] - setting g_bPMCIsSwitchingToPlayer = TRUE ")
		g_bPMCIsSwitchingToPlayer = TRUE
	ELSE
		g_bPMCIsSwitchingToPlayer = FALSE
	ENDIF
	#ENDIF
	
	#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP: 7799618 sData.eStage = ",sData.eStage) #ENDIF
	
	// *********************
	// Main cleanup switch.
	// *********************
	
	SWITCH sData.eStage
		
		
		// ***********************************************
		// Wait for leaderboard cam to finish interp.
		// ***********************************************
		CASE ePOSTMISSIONCLEANUPSTAGE_WAIT_FOR_LEADERBOARD_CAM_TO_BE_READY
			
			BOOL bContinue
			
			// No radar.
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			
			SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
			#IF IS_DEBUG_BUILD
			PRINTLN("[PMC] - called SET_PLAYER_LEAVE_PED_BEHIND(FALSE)")
			DEBUG_PRINTCALLSTACK()
			#ENDIF
			
			IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			// Set values for variables in intial checks.
			IF sData.bDidInitChecks
				
				// Save the coords where we are going to start a load scene and begin it.
				IF NOT IS_SKYSWOOP_NEEDED()
					IF GET_LOAD_SCENE_COORDS(sData)
						// ok. 
	//					RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
						NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE) 
						SET_IN_SPECTATOR_MODE(FALSE)
						PRINTLN("[PMC] - calling NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE) and SET_IN_SPECTATOR_MODE(FALSE).")
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						bContinue = TRUE
					ELSE
						CLEAR_FOCUS()
						NEW_LOAD_SCENE_STOP()
					ENDIF
				ENDIF
				
				IF (sData.bStartedFirstLoadScene OR IS_SKYSWOOP_NEEDED())
					
					IF IS_LEADERBOARD_CAM_READY()
						bContinue = TRUE
					ELSE
						IF IS_SKYSWOOP_NEEDED()
							bContinue = TRUE
						ENDIF
					ENDIF
					
					IF IS_SKYSWOOP_IN_SKY()
						bContinue = TRUE
					ENDIF
					
					IF sData.bComingOutOfPlaylist
						bContinue = TRUE
					ENDIF
					
				ENDIF
				
				// If the leaderboard cam is ready or we a re leaving a playlist, we are ok to keep going.
				IF bContinue	
					
					
					// *******
					// Debug.
					// *******
					// Print why we are in here. 
					#IF IS_DEBUG_BUILD
						IF sData.bComingOutOfPlaylist
							PRINTLN("[PMC] - bComingOutOfPlaylist = TRUE.")
						ENDIF
						IF IS_LEADERBOARD_CAM_READY()
							PRINTLN("[PMC] - leaderboard pan is complete.")
						ENDIF
					#ENDIF
					
					
					// *********
					// Sort UI.
					// *********
					
					// No script hud.
					DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(PLAYER_ID()))
					
					// *******************
					// Tutorial sessions.
					// *******************
					
					// Exit any tutorial sessions, we want to be in free mode now.
					IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
				        IF NETWORK_IS_IN_TUTORIAL_SESSION()
				            NETWORK_END_TUTORIAL_SESSION()
				           	PRINTLN("[PMC] - NETWORK_END_TUTORIAL_SESSION")
				        ENDIF
				    ENDIF
					
					
					// ************
					// Respawning.
					// ************
					
					// Don't want the repsawning to swoop here, we're doing our own camera at the moment.
					IF NOT (GET_MANUAL_RESPAWN_STATE() = MRS_READY_FOR_PLACEMENT)
						SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
					ELSE
						PRINTLN("[PMC] - manual respawn state already MRS_READY_FOR_PLACEMENT.")
					ENDIF
					
					
					// *****************
					// Cleanup Post FX. 
					// *****************
					
					IF IS_SKYSWOOP_NEEDED()
						IF NOT sData.bDidWarpToSwitchStartLoc
//							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
//								IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
//									SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_FINAL_RENDERED_CAM_COORD(), FALSE)
//								ENDIF
//							ENDIF
							sData.bDidWarpToSwitchStartLoc = TRUE
						ENDIF
						
					ENDIF
					
					// Disable blur after first part of sky swoop up.
					IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
						IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition
							IF IS_PLAYER_SWITCH_IN_PROGRESS()
								IF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_ASCENT
							        SET_SKYFREEZE_CLEAR(TRUE)
							        SET_SKYBLUR_CLEAR()
							        TOGGLE_RENDERPHASES(TRUE)
									IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
										ANIMPOSTFX_STOP("MinigameTransitionIn")
									ENDIF
									IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Win")
									    ANIMPOSTFX_STOP("MP_Celeb_Win")
									ENDIF
									IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Lose")
									   ANIMPOSTFX_STOP("MP_Celeb_Lose")
									ENDIF
							    ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF DO_SKYSWOOP_IF_NEEDED(TRUE, TRUE)
						
//						IF NOT IS_SKYSWOOP_NEEDED()
//							IF SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE()
//								IF NOT IS_SCREEN_FADED_OUT()
//									DO_SCREEN_FADE_OUT(500)
//								ENDIF
//							ENDIF
//						ENDIF
						
						IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition
						AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
						AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
							SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(TRUE, TRUE)
						ENDIF
						
						IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stMinSplashLengthTimer)
							START_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stMinSplashLengthTimer)
						ENDIF
						
						// Bug 1954263.
						IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.bRunPostMatchTransitionScene = TRUE
							PRINTLN("[PMC] - set bRunPostMatchTransitionScene = TRUE.")
						ENDIF
						
						// Bobby sessioning command. If it returns true go to the prepare for freemode script termination stage, if false then continue as normal. 
						IF SHOULD_DO_TRANSITION_TO_GAME_RUN()
							PRINTLN("[PMC] - SHOULD_DO_TRANSITION_TO_GAME_RUN() = TRUE.")
							CLEAR_TRANSITION_SESSIONS_QUICK_JOB_JOINING_LAUNCHED() // Added as requested by Bobby for url:bugstar:2705333
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_PREPARE_FOR_FREMODE_DEATH)
							sData.iPrepareForFreemodeDeathStage = 0
							sData.sTranToGameVars.iTranisitionSessionSetUpStage = 0
							PRINTLN("[TS] SET_UP_DO_TRANSITION_TO_GAME - [PMC] - sData.iPrepareForFreemodeDeathStage = 0")
							PRINTLN("[TS] SET_UP_DO_TRANSITION_TO_GAME - [PMC] - sData.sTranToGameVars.iTranisitionSessionSetUpStage = 0")
						ELSE
							PRINTLN("[PMC] - SHOULD_DO_TRANSITION_TO_GAME_RUN() = FALSE.")
							CLEAR_TRANSITION_SESSIONS_QUICK_JOB_JOINING_LAUNCHED() // Added as requested by Bobby for url:bugstar:2705333
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_SAVED_EOM_CAR)
						ENDIF
						
					ENDIF
					
				// Request the leaderboard cam, just in case a job scripter hasn't bothered to, or forgot.
				ELSE
					
					IF NOT IS_SKYSWOOP_NEEDED()
						IF NOT sData.bComingOutOfPlaylist
							REQUEST_LEADERBOARD_CAM()
						ENDIF
					ENDIF
					
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[PMC] - sData.bDidInitChecks = FALSE.")
				#ENDIF
			ENDIF
			
		BREAK
		
		
		
		// *************************************************
		// Save data before death of freemode script.
		// *************************************************
		CASE ePOSTMISSIONCLEANUPSTAGE_PREPARE_FOR_FREMODE_DEATH
			
			// No radar.
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			
			IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP: 7799618 sData.iPrepareForFreemodeDeathStage = ",sData.iPrepareForFreemodeDeathStage) #ENDIF
			// Save data that needs to be kept for when freemode is relaunched again, then let Bobby do his freemode killage.
			SWITCH sData.iPrepareForFreemodeDeathStage
				CASE 0
					// Copy variables.
					sData.BbNeedsFmCleanup_Copy = TRUE
					sData.bComingOutOfShootingRange_Copy[SHOOTING_RANGE_1] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_1]
					sData.bComingOutOfShootingRange_Copy[SHOOTING_RANGE_2] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_2]
					sData.eManualRespawnState_Copy = GET_MANUAL_RESPAWN_STATE()
					sData.iFmTutProgBitset_Copy = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset
					PRINTLN("[PMC] - saved copy of non POST_MISSION_CLEANUP_DATA data.")
					sData.iPrepareForFreemodeDeathStage++
				BREAK
				CASE 1
					IF SET_UP_DO_TRANSITION_TO_GAME(sData.sTranToGameVars)
						CLEAR_DO_TRANSITION_TO_GAME()
						GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD)
						// ...then kill freemode here.
					ENDIF
				BREAK
			ENDSWITCH
			
		BREAK
		
		
		
		// ********************************************************
		// Holding state while freemode script is restarted.
		// ********************************************************
		CASE ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD	
			
			// No radar.
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			
			PRINTLN("[PMC] - in stage ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD.")
			
			IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP: 7799618 sData.iPrepareForFreemodeDeathStage = ",sData.iPrepareForFreemodeDeathStage) #ENDIF
			// Reset variables.
			SWITCH sData.iFreemodeDeadMoveOnStage
			
				CASE 0
					
					BOOL bMoveOn
					
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPausePvCreation = TRUE
					
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.bShouldTransitionSessionsRestartPlaylist = SHOULD_TRANSITION_SESSIONS_RESTART_PLAYLIST()
					PRINTLN("[PMC] - SHOULD_TRANSITION_SESSIONS_RESTART_PLAYLIST = ", sData.bShouldTransitionSessionsRestartPlaylist)
					PRINTLN("[PMC] - stored in = g_TransitionSessionNonResetVars.sPostMissionCleanupData.bShouldTransitionSessionsRestartPlaylist")
					
					IF IS_PLAYER_SCTV(PLAYER_ID())
						PRINTLN("[PMC] - player is sctv, we do not have to have the cmaera in the sky, skipping swoop up.")
						bMoveOn = TRUE
					ELSE
						IF DO_SKYSWOOP_IF_NEEDED(TRUE, TRUE)
							bMoveOn = TRUE
						ENDIF
					ENDIF
					#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618 bMoveOn = ",bMoveOn) #ENDIF
					// Make sure Brenda's swoop up function has been allowed to fully complete. We may have killed freemode before it got a chance to finish. 
					IF bMoveOn
						
						IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBlockLoadingTvForHeistCleanup = TRUE
							PRINTLN("[PMC] - bPlaceInApartment, setting bBlockLoadingTvForHeistCleanup so we don;t load the TV until after finishing loading the apartment (code say this needs to be done).")
						ENDIF
						
						#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP: 7799618 sData.bCleanUpReadyToMoveOn = ",sData.bCleanUpReadyToMoveOn) #ENDIF
						#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP: 7799618 IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART = ",IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()) #ENDIF
						#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP: 7799618 GB_HAS_PLAYER_COMPLETED_INITIAL_BOSS_DATA_REFRESH(PLAYER_ID()))= ",GB_HAS_PLAYER_COMPLETED_INITIAL_BOSS_DATA_REFRESH(PLAYER_ID())) #ENDIF
						// Bobby's stuff says we are good to go, a new session has started.
						IF sData.bCleanUpReadyToMoveOn
						AND (IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
						OR GB_HAS_PLAYER_COMPLETED_INITIAL_BOSS_DATA_REFRESH(PLAYER_ID()))
							
							PRINTLN("[PMC] - sData.bCleanUpReadyToMoveOn = TRUE.")
							
							IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
								GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vMyPostJobSpawnArea = sData.vEndOfJobPosition
								SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR(sData.vEndOfJobPosition)
								PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG = TRUE, set sMagnateGangBossData.vMyPostJobSpawnArea and SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vMyPostJobSpawnArea)
							ELIF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
								GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vMyPostJobSpawnArea = GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))].sMagnateGangBossData.vMyPostJobSpawnArea
								SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))].sMagnateGangBossData.vMyPostJobSpawnArea)
								PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE) = TRUE, set sMagnateGangBossData.vMyPostJobSpawnArea and SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR = bosses vMyPostJobSpawnArea = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vMyPostJobSpawnArea)
							ELSE
								GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vMyPostJobSpawnArea = sData.vEndOfJobPosition
								PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - I am nothing to do with a gang, set sMagnateGangBossData.vMyPostJobSpawnArea = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vMyPostJobSpawnArea)
							ENDIF
							
							// Set data back to saved values.
							SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(TRUE)
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_1] = sData.bComingOutOfShootingRange_Copy[SHOOTING_RANGE_1]
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_2] = sData.bComingOutOfShootingRange_Copy[SHOOTING_RANGE_2]
							IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset = 0
								PRINTLN("[dsw] [TIMESTAMP] RESTORING iFmTutProgBitset TIME ", GET_CLOUD_TIME_AS_INT(), " iFmTutProgBitset =")
								GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset = sData.iFmTutProgBitset_Copy
							ELSE
								PRINTLN("[dsw] [TIMESTAMP] NOT RESTORING iFmTutProgBitset TIME ", GET_CLOUD_TIME_AS_INT(), " iFmTutProgBitset =")
							ENDIF
			//				SET_MANUAL_RESPAWN_STATE(sData.eManualRespawnState_Copy)
							sData.bCleanUpReadyToMoveOn = FALSE
							PRINTLN("[PMC] - reset non POST_MISSION_CLEANUP_DATA data to previosu values.")
							
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION )
							
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
								g_vInitialSpawnLocation = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
								PRINTLN("[PMC] - set g_vInitialSpawnLocation = local play coords = ", g_vInitialSpawnLocation)
							ENDIF
							
							IF IS_PLAYER_SCTV(PLAYER_ID())
								
								PRINTLN("[PMC] - player is SCTV, going straight to cleanup so we don;t interfere with it.")
								GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP)
								
							ELSE
								
								IF IS_SKYSWOOP_NEEDED()
									CLEANUP_LEADERBOARD_CAM(TRUE, TRUE, FALSE, SHOULD_SCREEN_BE_UNBLURRED(), SHOULD_RENDERPHASES_BE_UNFROZEN(), SHOULD_LB_POST_FX_STOP())
								ENDIF
								
								// Save if the personal vehicle is due to appear. If so we'll need to wait for it later on.
								IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
									#IF IS_DEBUG_BUILD
										PRINTLN("[PMC] - IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION()= ")
										IF IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION()
											PRINTLN("[PMC] - TRUE")
										ELSE
											PRINTLN("[PMC] - FALSE")
										ENDIF
									#ENDIF
								ENDIF
								
								sData.iFreemodeDeadMoveOnStage++
								PRINTLN("[PMC] - sData.iFreemodeDeadMoveOnStage = 1.")
								
							ENDIF
							
						ENDIF
						
					ENDIF
					
				BREAK
				
				CASE 1
				
					BOOL bCutToPlayerForHeistNonApartmentCleanup
					
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						SET_MAX_WANTED_LEVEL(0)
					ENDIF
					
					IF ( g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
					AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration )
						
						PRINTLN("[PMC] - bDoingHeistCelebration = TRUE AND bDoingAfterApartmentPanHeistCelebration = FALSE, setting bCutToPlayerForHeistNonApartmentCleanup = TRUE.")
						bCutToPlayerForHeistNonApartmentCleanup = TRUE
						
					ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
						
						PRINTLN("[PMC] - bDoingHeistEndWinnerScene = TRUE, setting bCutToPlayerForHeistNonApartmentCleanup = TRUE.")
						bCutToPlayerForHeistNonApartmentCleanup = TRUE
						
					ELSE
						
						//Bail till we've got the BD to say that it's ok to go on
						IF FM_EVENT_GET_SESSION_ACTIVE_FM_EVENT() = FMMC_TYPE_UNINITIALIZED
							CPRINTLN(DEBUG_NET_AMBIENT, "[PMC] [FMEG] - GlobalServerBD_FM_events.sAActiveEventData.iEvent = FMMC_TYPE_UNINITIALIZED")
							RETURN FALSE
						ENDIF
						
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnNearEvent = FM_EVENT_SHOULD_SPAWN_NEAR_ACTIVE_FM_EVENT(g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent, g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords, g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEventMin)
						
						#IF IS_DEBUG_BUILD
						INT iTransitionVotestatus 
						iTransitionVotestatus = GET_TRANSITION_SESSION_VOTE_STATUS()
						PRINTLN("[PMC] - [FMEG] - FM_EVENT_SHOULD_SPAWN_NEAR_ACTIVE_FM_EVENT = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnNearEvent)
						PRINTLN("[PMC] - [FMEG] - sData.bBeenOnStandardHeistOrContactMission = ", sData.bBeenOnStandardHeistOrContactMission)
						PRINTLN("[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment)
						PRINTLN("[PMC] - [FMEG] - GET_TRANSITION_SESSION_VOTE_STATUS() = ", iTransitionVotestatus)
						PRINTLN("[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos)
						PRINTLN("[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius)
						PRINTLN("[PMC] - [FMEG] - sData.bBeenOnMiniGame = ", sData.bBeenOnMiniGame)
						PRINTLN("[PMC] - [FMEG] - sData.bBeenOnMiniGame = ", sData.bBeenOnMiniGame)
						PRINTLN("[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent)
						PRINTLN("[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords)
						PRINTLN("[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEventMin = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEventMin)
						#ENDIF
						#IF USE_FINAL_PRINTS
						INT iTransitionVotestatus 
						iTransitionVotestatus = GET_TRANSITION_SESSION_VOTE_STATUS()
						PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618[PMC] - [FMEG] - FM_EVENT_SHOULD_SPAWN_NEAR_ACTIVE_FM_EVENT = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnNearEvent)
						PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618[PMC] - [FMEG] - sData.bBeenOnStandardHeistOrContactMission = ", sData.bBeenOnStandardHeistOrContactMission)
						PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment)
						PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618[PMC] - [FMEG] - GET_TRANSITION_SESSION_VOTE_STATUS() = ", iTransitionVotestatus)
						PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos)
						PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius)
						PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618[PMC] - [FMEG] - sData.bBeenOnMiniGame = ", sData.bBeenOnMiniGame)
						PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618[PMC] - [FMEG] - sData.bBeenOnMiniGame = ", sData.bBeenOnMiniGame)
						PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent)
						PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords)
						PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP:7799618[PMC] - [FMEG] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEventMin = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEventMin)
						#ENDIF
						// Decide type of warp to do.
						IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != (-1) // Lowrider lamar mission post mission scene.
						
							PRINTLN("[PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, ", going to warp to on foot stage.")
							
						ELIF NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos) 
							
							PRINTLN("[PMC] - vPostMissionPos = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos, ", going to warp to on foot stage.")
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_NEARBY_ON_FOOT)
							IF NOT g_bSkipReturnFalseInPMCFreemodeDead
								RETURN FALSE
							ENDIF
							
						ELIF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
							
							PRINTLN("[PMC] - bPlaceInApartment = TRUE.")
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_SAVED_EOM_CAR)
							IF NOT g_bSkipReturnFalseInPMCFreemodeDead
								RETURN FALSE
							ENDIF
							
						ELIF sData.bBeenOnMiniGame
							
							// No vehicle stuff, we want to warp near the corona.
							PRINTLN("[PMC] - sData.bBeenOnMiniGame = TRUE.")
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_SAVED_EOM_CAR)
							IF NOT g_bSkipReturnFalseInPMCFreemodeDead
								RETURN FALSE
							ENDIF
							
						// If I've been on a contact mission.
						ELIF sData.bBeenOnStandardHeistOrContactMission
							
							// Do not warp the vehicle, it'll be created in it's original position.
							PRINTLN("[PMC] - sData.bBeenOnStandardHeistOrContactMission = TRUE.")
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_SAVED_EOM_CAR)
							IF NOT g_bSkipReturnFalseInPMCFreemodeDead
								RETURN FALSE
							ENDIF
							
						// If I've not been on a contact mission.
						ELSE
							
							PRINTLN("[PMC] - sData.bBeenOnStandardHeistOrContactMission = FALSE.")
							
							// Wait for the vehicle to be created.
							IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID(TRUE))
								
								PRINTLN("[PMC] - personal veh is driveable.")
								
								// If it's been created more than 100m away, warp it nearby.
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NET_TO_VEH(PERSONAL_VEHICLE_NET_ID(TRUE))) > 100.0
									
									PRINTLN("[PMC] - personal veh is > 100m away, setting bWarpNear = TRUE.")
									GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_SAVED_EOM_CAR)
									IF NOT g_bSkipReturnFalseInPMCFreemodeDead
										RETURN FALSE
									ENDIF
									
								// If it's been created less than 100m away, do not warp it nearby, leave it where it is.
								ELSE
									
									PRINTLN("[PMC] - personal veh is <= 100m away, setting bWarpNear = FALSE.")
									GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_SAVED_EOM_CAR)
									IF NOT g_bSkipReturnFalseInPMCFreemodeDead
										RETURN FALSE
									ENDIF
									
								ENDIF
								
							ELSE
								PRINTLN("[PMC] - personal veh is not driveable yet, waiting on creation.")
							ENDIF
							
						ENDIF
					
					ENDIF
					
					
					// If a playlist is restarting.
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bShouldTransitionSessionsRestartPlaylist
						PRINTLN("[PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.bShouldTransitionSessionsRestartPlaylist = TRUE.")
						GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_LOAD_SCENE)
					// If a playlist is not restarting.
					ELSE
						IF NOT bCutToPlayerForHeistNonApartmentCleanup
							PRINTLN("[PMC] - SHOULD_TRANSITION_SESSIONS_RESTART_PLAYLIST = FALSE.")
							PRINTLN("[PMC] - bCutToPlayerForHeistNonApartmentCleanup = FALSE")
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_SAVED_EOM_CAR)
						ELSE
							PRINTLN("[PMC] - SHOULD_TRANSITION_SESSIONS_RESTART_PLAYLIST = FALSE.")
							PRINTLN("[PMC] - bCutToPlayerForHeistNonApartmentCleanup = TRUE")
							CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT() 
							CLEAR_SPAWNING_MISSION_DATA()
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_CUT_TO_PLAYER)
						ENDIF
					ENDIf
				BREAK
			
			ENDSWITCH
			
		BREAK
		
		
		
		// ************************************************
		// Try to warp into saved eom vehicle if possible.
		// ************************************************
			CASE ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_SAVED_EOM_CAR
			
			//If we are waiting for an event to launch 
//			#IF IS_DEBUG_BUILD
//				IF GET_COMMANDLINE_PARAM_EXISTS("sc_holdOnPlaylistScreen")
//					IF NOT IS_ANY_EVENT_PLAYLIST_LAUNCHING()
//					AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//						IF NOT SHOULD_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
//						AND NOT SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT()
//							MAINTAIN_LAUNCHING_COMMUNITY_PLAYLIST_DURING_TRANSITION()
//						ENDIF
//						RETURN FALSE
//					ENDIF
//				ENDIF
//			#ENDIF
			
			BOOL bDoWarpIntoVeh
			BOOL bDoWarpLastVehNearby
			BOOL bProcessStage
			bDoWarpIntoVeh = TRUE
			bProcessStage = TRUE
			
			// Need to make sure we are out of any tutorial sessions so we don't create non-networked entities. 
			IF NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
//				PRINTLN("[PMC] - NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING = TRUE, cannot process rest of cleanup yet.")
				bProcessStage = FALSE
			ELSE
//				PRINTLN("[PMC] - NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING = FALSE.")
			ENDIF
			
			IF NETWORK_IS_IN_TUTORIAL_SESSION()
//				PRINTLN("[PMC] - NETWORK_IS_IN_TUTORIAL_SESSION = TRUE, cannot process rest of cleanup yet.")
				bProcessStage = FALSE
			ELSE
//				PRINTLN("[PMC] - NETWORK_IS_IN_TUTORIAL_SESSION = FALSE.")
			ENDIF
			
			IF NETWORK_IS_IN_MP_CUTSCENE()
				NETWORK_SET_IN_MP_CUTSCENE(FALSE)
//				PRINTLN("[PMC] - NETWORK_IS_IN_MP_CUTSCENE = TRUE, called NETWORK_SET_IN_MP_CUTSCENE(FALSE).")
			ELSE	
				
//				PRINTLN("[PMC] - NETWORK_IS_IN_MP_CUTSCENE = FALSE.")
			ENDIF
			
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
//				PRINTLN("[PMC] - NETWORK_IS_GAME_IN_PROGRESS = FALSE, cannot process rest of cleanup yet.")
				bProcessStage = FALSE
			ELSE
//				PRINTLN("[PMC] - NETWORK_IS_GAME_IN_PROGRESS = TRUE.")
			ENDIF
			
			// We don't care about the tutorial session checks until we have started the tutorial mission.
			IF sData.bPreTutorialMissionInFlow
//				PRINTLN("[PMC] - bPreTutorialMissionInFlow = TRUE, processing rest of cleanup yet.")
				bProcessStage = TRUE
			ELSE
//				PRINTLN("[PMC] - Data.bPreTutorialMissionInFlow = FALSE.")
			ENDIF
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP: 7799618 bProcessStage = ",bProcessStage) #ENDIF
			// Main logic.
			IF bProcessStage
				
				IF GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP()
					IF NOT SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE()
						bDoWarpIntoVeh = FALSE
						PRINTLN("[PMC] - GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP() = TRUE and SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE() = FALSE, setting bDoWarpIntoVeh = FALSE.")
					ELSE
						bDoWarpLastVehNearby = TRUE
						bDoWarpIntoVeh = FALSE
					ENDIF
				ENDIF
				
				IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
					bDoWarpIntoVeh = FALSE
					bDoWarpLastVehNearby = FALSE
					PRINTLN("[PMC] - bPlaceInApartment = TRUE, setting bDoWarpIntoVeh = FALSE.")
				ENDIF
				
				IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCompletedGeraldMission
					bDoWarpIntoVeh = FALSE
					PRINTLN("[PMC] - bCompletedGeraldMission = TRUE, setting bDoWarpIntoVeh = FALSE.")
				ENDIF
				
				IF g_sFMMCEOM.bDoWarp
					bDoWarpIntoVeh = FALSE
					PRINTLN("[PMC] - g_sFMMCEOM.bDoWarp = TRUE, setting bDoWarpIntoVeh = FALSE.")
				ENDIF
				
				IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() 
					IF NOT g_bPassedMyTutorialMission
						bDoWarpIntoVeh = FALSE
						bDoWarpLastVehNearby = FALSE
						PRINTLN("[PMC] - g_bPassedMyTutorialMission = FALSE, setting bDoWarpIntoVeh = FALSE and bDoWarpLastVehNearby = FALSE.")
					ENDIF
				ENDIF

				IF sData.bBeenOnMiniGame
					bDoWarpIntoVeh = FALSE
					bDoWarpLastVehNearby = FALSE
					PRINTLN("[PMC] - g_sFMMCEOM.bBeenOnMiniGame = TRUE, setting bDoWarpIntoVeh = FALSE and bDoWarpLastVehNearby = FALSE.")
				ENDIF
				
				IF bDoWarpIntoVeh
					
					// Do creation and warp.
					eCREATE_AND_ENTER_AMBIENT_VEHICLE_RESULT eTempResult
					eTempResult = CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE(<<0.0,0.0,0.0>>, FALSE, TRUE)
					
					// Once we have a result move onto the appropriate next stage.
					IF (eTempResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_CREATED_AND_ENTERED_VEHICLE)
					
						PRINTLN("[PMC] - created and warped into saved ambient vehicle.")
						
						// We want the cam in a good looking place.
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						
						// Debug info.
						#IF IS_DEBUG_BUILD
							IF NOT sData.bBeenOnStandardHeistOrContactMission
								PRINTLN("[PMC] - player not on standard or contact mission.")
							ELSE
								PRINTLN("[PMC] - player on standard or contact mission.")
							ENDIF
						#ENDIF
						
						// Go to next stage.
						GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_PERSONAL_VEH)
						
					ELIF (eTempResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED)
					
						PRINTLN("[PMC] - aborted create and warp into saved ambient vehicle.")
						
						IF NOT sData.bBeenOnStandardHeistOrContactMission
							PRINTLN("[PMC] - player not on standard or contact mission.")
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_PERSONAL_VEH)
						ELSE
							PRINTLN("[PMC] - player on standard or contact mission.")
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_PERSONAL_VEH)
						ENDIF
						
					ENDIF
				
				ELIF bDoWarpLastVehNearby
					
					IF CREATE_MY_LAST_VEHICLE()
						// Go to next stage.
						GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_PERSONAL_VEH)
					ENDIF
					
				ELSE
					
					#IF IS_DEBUG_BUILD
					IF GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP()
						PRINTLN("[PMC] - GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP() = TRUE.")
					ENDIF
					IF g_sFMMCEOM.bDoWarp
						PRINTLN("[PMC] - g_sFMMCEOM.bDoWarp = TRUE.")
					ENDIF
					PRINTLN("[PMC] - not recreating previous eom vehicle.")
					#ENDIF
					
					GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_DECIDE_INTERP_TYPE)
					
				ENDIF
			
			ENDIF
			
		BREAK
		
		
		
		// ******************************************
		// Decide what kind transition to use.
		// ******************************************
		CASE ePOSTMISSIONCLEANUPSTAGE_DECIDE_INTERP_TYPE
			
			// No radar or other script hud.
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(PLAYER_ID()))
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			
			// If we are alive and finished spawning.
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					// Failsafe timer for bug 1717705. Temp fix until Neil is back and can look into a proper fix. #LimboBug
					IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRezzedElsewhere_Bug1717705
						IF NOT HAS_NET_TIMER_STARTED(sData.stRezzedElsewhereTimer_Bug1717705)
							START_NET_TIMER(sData.stRezzedElsewhereTimer_Bug1717705)
						ENDIF
					ELSE
						IF HAS_NET_TIMER_STARTED(sData.stRezzedElsewhereTimer_Bug1717705)
							RESET_NET_TIMER(sData.stRezzedElsewhereTimer_Bug1717705)
						ENDIF
					ENDIF
					IF ( ( (GET_MANUAL_RESPAWN_STATE() = MRS_READY_FOR_PLACEMENT) OR (NOT sData.bDeadWhenStartedCleanup) ) AND ( NOT IS_PLAYER_RESPAWNING(PLAYER_ID()) OR HAS_NET_TIMER_EXPIRED(sData.stRezzedElsewhereTimer_Bug1717705, 30000) ) )
					OR ( (GET_MANUAL_RESPAWN_STATE() != MRS_READY_FOR_PLACEMENT) AND (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING) )	
						// Based on if we should enter the personal vehicle or not, go to the warp into vehicle or warp on foot stage.
						IF NOT HAS_NET_TIMER_STARTED(sData.stWaitForCheckPvFailTimer)
							START_NET_TIMER(sData.stWaitForCheckPvFailTimer)
						ELSE
							IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
								PRINTLN("[PMC] - bPlaceInApartment, want to end up on foot.")
								GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_NEARBY_ON_FOOT)
							ELIF sData.bBeenOnMiniGame
								PRINTLN("[PMC] - bBeenOnMiniGame, want to end up on foot.")
								GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_NEARBY_ON_FOOT)
							ELSE
								IF READY_TO_DO_PERSONAL_VEHICLE_CHECKS()
								OR HAS_NET_TIMER_EXPIRED(sData.stWaitForCheckPvFailTimer, NETWORK_GET_TIMEOUT_TIME()+1000)
									#IF IS_DEBUG_BUILD
									IF HAS_NET_TIMER_EXPIRED(sData.stWaitForCheckPvFailTimer, NETWORK_GET_TIMEOUT_TIME()+1000)
										NET_SCRIPT_ASSERT("[PMC] - stWaitForCheckPvFailTimer timed out.")
									ENDIF
									#ENDIF
									IF SHOULD_PLAYER_WARP_INTO_PERSONAL_VEHICLE_FOR_CLEANUP(sData.bBeenOnStandardHeistOrContactMission)
										PRINTLN("[PMC] - player has a personal vehicle and should warp into it.")
										GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_PERSONAL_VEH)
									ELSE
										PRINTLN("[PMC] - player should not warp into personal vehicle.")
										IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
										OR g_sFMMCEOM.bDoWarp
											#IF IS_DEBUG_BUILD
												IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
												PRINTLN("[PMC] - player is not already sitting in a vehicle.")	
												ENDIF
												IF g_sFMMCEOM.bDoWarp
												PRINTLN("[PMC] - g_sFMMCEOM.bDoWarp = TRUE, don't care if in a vehicle.")
												ENDIF
											#ENDIF
											GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_NEARBY_ON_FOOT)
										ELSE
											PRINTLN("[PMC] - player is already sitting in a vehicle.")
											GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_LOAD_SCENE)
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[PMC] - waiting for READY_TO_DO_PERSONAL_VEHICLE_CHECKS().")
								ENDIF
							ENDIF
						ENDIF
						
					ELSE
						
						// Print this so I can pass PTs over to Fergie from time to time.
						#IF IS_DEBUG_BUILD
							IF (GET_MANUAL_RESPAWN_STATE() != MRS_READY_FOR_PLACEMENT)
								PRINTLN("[PMC] - GET_MANUAL_RESPAWN_STATE() != MRS_READY_FOR_PLACEMENT.")
							ENDIF
							IF NOT sData.bDeadWhenStartedCleanup
								PRINTLN("[PMC] - sData.bDeadWhenStartedCleanup = FALSE.")
							ENDIF
							IF IS_PLAYER_RESPAWNING(PLAYER_ID())
								PRINTLN("[PMC] - IS_PLAYER_RESPAWNING(PLAYER_ID()) = TRUE.")
							ENDIF
							IF NOT HAS_NET_TIMER_EXPIRED(sData.stRezzedElsewhereTimer_Bug1717705, 30000)
								PRINTLN("[PMC] - stRezzedElsewhereTimer_Bug1717705, 30000 has not expired yet.")
							ENDIF
						#ENDIF
						
					ENDIF
					
				ELSE
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[PMC] - player is dead.")
						SWITCH GET_MANUAL_RESPAWN_STATE()
							CASE MRS_NULL						PRINTLN("[PMC] - current manual respawn = MRS_NULL.") 						BREAK
							CASE MRS_EXIT_SPECTATOR_AFTERLIFE	PRINTLN("[PMC] - current manual respawn = MRS_EXIT_SPECTATOR_AFTERLIFE.")	BREAK
							CASE MRS_RESPAWN_PLAYER_HIDDEN		PRINTLN("[PMC] - current manual respawn = MRS_RESPAWN_PLAYER_HIDDEN.") 	BREAK
							CASE MRS_READY_FOR_PLACEMENT		PRINTLN("[PMC] - current manual respawn = MRS_READY_FOR_PLACEMENT.") 		BREAK
							CASE MRS_FINISH_RESPAWN				PRINTLN("[PMC] - current manual respawn = MRS_FINISH_RESPAWN.") 			BREAK
						ENDSWITCH
					#ENDIF
					
				ENDIF
				
			ENDIF
		BREAK
		
		
		
		// ******************************************
		// Warp into personal vehicle.
		// ******************************************
		CASE ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_PERSONAL_VEH
			
			// No radar or other script hud.
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(PLAYER_ID()))
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			
			// If we have the personal vehicle or we are waiting on it being created.
			IF IS_PERSONAL_VEHICLE_DRIVEABLE_OR_GOING_TO_BE_CREATED() 
			AND (IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION() OR IS_PERSONAL_VEHICLE_CREATION_DISABLED_THIS_FRAME())
			AND NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
			AND NOT g_TransitionSessionNonResetVars.bFinishedFirstArenaEvent
				
				BOOL bAllowVehCreation
				
				IF SHOULD_POST_MISSION_CLEANUP_PUT_PLAYER_NEAR_EVENT()
					
					VECTOR vSpawnCentre
					vSpawnCentre = g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords
					PLAYER_SPAWN_LOCATION eSpawnLoc
					eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
					BOOL bNearCentrePoint
					bNearCentrePoint = FALSE
					FLOAT fSearchRadius
					fSearchRadius = g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent
					FLOAT fExclusionZone
					fExclusionZone = (g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent/3)
					
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEventMin > 0.0
						fExclusionZone = g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEventMin
						PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - fDistanceFromFreemodeEventMin = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEventMin, ", valid value, using this as exclusion zone.")
					ENDIF
					
					SWITCH g_TransitionSessionNonResetVars.sPostMissionCleanupData.iWarpForPvAtEvent
						
						CASE 0
							
							SETUP_SPECIFIC_SPAWN_LOCATION(vSpawnCentre, 0.0, fSearchRadius, TRUE, fExclusionZone, FALSE, TRUE, 65, bNearCentrePoint)
							SET_PLAYER_WILL_SPAWN_FACING_COORDS(vSpawnCentre, TRUE, FALSE)
							
							#IF IS_DEBUG_BUILD
							PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - warping before allowing PV to be created.")
							PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - vSpawnCentre = g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords)
							PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS")
							PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - bNearCentrePoint = FALSE")
							PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - fSearchRadius = g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent)
							PRINTLN("[PMC] - DO_FMMC_POST_ACTIVITY_CLEANUP - fExclusionZone = (g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent/3) = ", fExclusionZone)
							#ENDIF
							
							// Freeze player to stop falling through world.
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
									IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
										FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE) 
									ENDIF
								ENDIF
							ENDIF
						
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.iWarpForPvAtEvent++
							
						BREAK
						
						CASE 1
							
							IF WARP_TO_SPAWN_LOCATION(eSpawnLoc, FALSE, FALSE, FALSE, TRUE)	
								IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnNearEvent
									g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPvSpawnPosOverride = << 0.0, 0.0, 0.0 >>
									PRINTLN("[PMC] - cleared vPvSpawnPosOverride, we want the vehicle near an event.")
								ENDIF
								g_TransitionSessionNonResetVars.sPostMissionCleanupData.iWarpForPvAtEvent++
							ENDIF
							
						BREAK
						
						CASE 2
							
							bAllowVehCreation = TRUE
							
						BREAK
						
					ENDSWITCH
					
				ELSE
					
					bAllowVehCreation = TRUE
					
				ENDIF
				
				IF bAllowVehCreation
					
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPausePvCreation
						SET_NEXT_PERSONAL_VEHICLE_CREATION_TO_AVOID_RADIUS(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords, (g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent/3))
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPausePvCreation = FALSE
					ENDIF
							
					// If we have an un-impounded, fully existing personal vehicle that is ready to drive.
					IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID(TRUE))
						
						// Freeze player to stop falling through world.
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE) 
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT MP_SAVE_VEHICLE_IS_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
						AND CONTACT_MISSION_DISTANCE_CHECK(sData)
							
							// Warp the player in to the personal vehicle.
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								PRINTLN("[PMC] - player already in a vehicle.")
								bSortedPersonalVehicle = TRUE
							ENDIF
							
							// If we are not aleady in a vehicle, warp into the personal vehicle.
							IF NOT bSortedPersonalVehicle
								IF WARP_PLAYER_INTO_PERSONAL_VEHICLE(sData.eTimer)
									PRINTLN("[PMC] - player in personal vehicle.")
									bSortedPersonalVehicle = TRUE
								ENDIF
							ENDIF
							
							// If we've done everything we need with the personal vehicle, time to move on. 
							IF bSortedPersonalVehicle
								
								// We want the cam in a good looking place.
								SET_GAMEPLAY_CAM_RELATIVE_PITCH()
								SET_GAMEPLAY_CAM_RELATIVE_HEADING()
								
								// Let's have some vehicles nearby.
								INSTANTLY_FILL_VEHICLE_POPULATION()
								
								// For Neil to let him know we're done.
								SET_MANUAL_RESPAWN_STATE(MRS_FINISH_RESPAWN) 
								
								// Move on.
								GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_LOAD_SCENE)
								
							ENDIF
						
						// If the personal vehicle is impounded, better warp nearby on foot instead.
						ELSE
							
							// Print what's gone wrong.
							#IF IS_DEBUG_BUILD
								IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
									PRINTLN("[PMC] - bPlaceInApartment = TRUE, not going to warp into personal vehicle.")
								ELSE
									IF NOT IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID(TRUE))
										PRINTLN("[PMC] - player personal vehicle not driveable anymore.")
									ELIF MP_SAVE_VEHICLE_IS_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
										PRINTLN("[PMC] - player personal vehicle is impounded.")
									ELIF NOT CONTACT_MISSION_DISTANCE_CHECK(sData)
										PRINTLN("[PMC] - CONTACT_MISSION_DISTANCE_CHECK = FALSE.")
									ENDIF
								ENDIF
							#ENDIF
							
							// Go to the warp to nearby on foot stage.
							PRINTLN("[PMC] - player already in a vehicle.")
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_LOAD_SCENE)
							ELSE
								GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_NEARBY_ON_FOOT)
							ENDIF
							
						ENDIF
					ENDIF
				
				ENDIF
				
			// If the personal vehicle is not driveable and is never going to be, we best spawn on foot instead.	
			ELSE
				
				
				#IF IS_DEBUG_BUILD
					IF NOT IS_PERSONAL_VEHICLE_DRIVEABLE_OR_GOING_TO_BE_CREATED()
						PRINTLN("[PMC] - IS_PERSONAL_VEHICLE_DRIVEABLE_OR_GOING_TO_BE_CREATED() = FALSE")
					ENDIF
					IF NOT IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION()
						PRINTLN("[PMC] - IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION() = FALSE")
						PRINTLN("[PMC] - g_iFailReason_SavedVehicleAvailableForCreation = ", g_iFailReason_SavedVehicleAvailableForCreation)
					ENDIF
				#ENDIF
				
				PRINTLN("[PMC] - IS_PERSONAL_VEHICLE_DRIVEABLE_OR_GOING_TO_BE_CREATED = FALSE.")
				
				// Go to the warp to nearby on foot stage.
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					PRINTLN("[PMC] - player already in a vehicle.")
					GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_LOAD_SCENE)
				ELSE
					GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_NEARBY_ON_FOOT)
				ENDIF
				
				
			ENDIF
			
		BREAK
		
		
		
		// ******************************************
		// Warp nearby.
		// ******************************************
		CASE ePOSTMISSIONCLEANUPSTAGE_INTERP_TO_NEARBY_ON_FOOT
			
			BOOL bGoToOnFootWarpStage1
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP: 7799618 sData.iOnFootWarpStage = ",sData.iOnFootWarpStage) #ENDIF
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("DO_FMMC_POST_ACTIVITY_CLEANUP: 7799618 g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ",g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId) #ENDIF
			SWITCH sData.iOnFootWarpStage
				
				CASE 0
					
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != (-1)
					
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
								VECTOR vSceneCoords
								vSceneCoords = GET_SYNCED_INTERACTION_SCENE_COORDS(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId)
								SET_ENTITY_COORDS(PLAYER_PED_ID(), vSceneCoords, FALSE)
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
									IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
										FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
									ENDIF
								ENDIF
								PRINTLN("[RCC MISSION] - [PMC] - we are doing a mission controller flow post mission scene, put player at GET_SYNCED_INTERACTION_SCENE_COORDS(", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, ") = ", vSceneCoords)
							ENDIF
						ENDIF
						
						INTERIOR_INSTANCE_INDEX tempInteriorIndex
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = tempInteriorIndex
						PRINTLN("[PMC] - set g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = tempInteriorIndex")
			
						sData.iOnFootWarpStage = 3
						PRINTLN("[PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, ", setting iOnFootWarpStage = 3")
						
					ELSE
						
						IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
							IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPackedInt > 0
								PRINTLN("[PMC] - iPackedInt > 0, setting bGoToOnFootWarpStage1 = TRUE.")
								bGoToOnFootWarpStage1 = TRUE
							ELSE	
								IF NOt HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.waitForPackedIntTimer)
									START_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.waitForPackedIntTimer)
								ELSE
									IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.waitForPackedIntTimer, 6000)
										PRINTLN("[PMC] - waitForPackedIntTimer expired, setting bGoToOnFootWarpStage1 = TRUE.")
										bGoToOnFootWarpStage1 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[PMC] - bPlaceInApartment = FALSE, setting bGoToOnFootWarpStage1 = TRUE.")
							bGoToOnFootWarpStage1 = TRUE
						ENDIF
						
					ENDIF
					
					IF bGoToOnFootWarpStage1
						
						// Get where I need to warp to and set the correct warp to spawn location enum.
						IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeachAfterSumoSpawnFlag
						AND NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn
							IF NOT SETUP_CREATOR_POST_MISSION_WARP_LOCATION(sData.eSpawnLoc)
								SETUP_FMMC_POST_ACTIVITY_CLEANUP_WARP_LOCATION(sData, sData.eSpawnLoc)
							ENDIF
						ELSE
							SETUP_FMMC_POST_ACTIVITY_CLEANUP_WARP_LOCATION(sData, sData.eSpawnLoc)
						ENDIF
						
						// If we are leaving the shooting range, sort the special case location for that (inside the gun shop).
						IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
							SET_SHOOTING_RANGE_WARP_LOCATION(sData.eSpawnLoc)
						ENDIF
						
						// Make sure the focus is clear in prep for cleanup. 
						CLEAR_FOCUS()
							
						// Stop load scenes.
						NEW_LOAD_SCENE_STOP()
						
						// Freeze player to stop falling through world.
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE) 
								ENDIF
							ENDIF
						ENDIF
						
						INTERIOR_INSTANCE_INDEX tempInteriorIndex
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = tempInteriorIndex
						PRINTLN("[PMC] - set g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = tempInteriorIndex")
						
						PRINTLN("TRANSITION TIME - FREEMODE - [PMC] - TRANSITION BEGINNING WARP_TO_SPAWN_LOCATION               - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
						
						PRINTLN("[PMC] - iOnFootWarpStage = 1.")
						sData.iOnFootWarpStage++
						
					ENDIF
					
				BREAK
				
				CASE 1
						
					// No radar or script hud please.
					DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(PLAYER_ID()))
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
					
					// If we have a spawn vector, put the player there!
					IF IS_PLAYER_SCTV(PLAYER_ID())	//We're SCTV, so do a quick warp and move on with our lives. 
						IF WARP_TO_SPAWN_LOCATION(sData.eSpawnLoc, FALSE, FALSE, FALSE, TRUE)
							
							PRINTLN("TRANSITION TIME - FREEMODE - TRANSITION COMPLETED WARP_TO_SPAWN_LOCATION - [PMC]              - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
							
							PRINTLN("[PMC] - iOnFootWarpStage = 2.")
							
							sData.bDidOnFootWarp = TRUE
							
							sData.iOnFootWarpStage++
							
						ENDIF
					ELSE	// Otherwise do it properly
						
						BOOL bDoWarp
						BOOL bLoadedApartment
						
						IF ( g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
						AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration )
							 
							sData.bDidOnFootWarp = TRUE
							sData.iOnFootWarpStage = 3
							PRINTLN("[PMC] - bDoingHeistCelebration = TRUE - setting bDidOnFootWarp = TRUE and bLoadedApartment = TRUE.")
							
						ELSE
						
							// read lots of globals from Connor to figure out which apartment to put the player in and where to put thme in it. 
							IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
								IF NOT ARE_VECTORS_EQUAL(<< 0.0, 0.0, 0.0 >>, g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords)
									PRINTLN("[PMC] - going into apartment, need to put it into memory.")
									
									IF NOT IS_VALID_INTERIOR(g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment)
										
										IF IS_PROPERTY_CUSTOM_APARTMENT(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum)
											IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bActivatedCutsomaprtmentIpls
												ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum, g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation)
												g_TransitionSessionNonResetVars.sPostMissionCleanupData.bActivatedCutsomaprtmentIpls = TRUE
												PRINTLN("[PMC] - activated custom apartment IPLs - bActivatedCutsomaprtmentIpls = TRUE")
											ENDIF
											IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation = PROPERTY_VARIATION_1
												g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"apa_v_mp_h_01")
												PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = apa_v_mp_h_01")
											ELIF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation = PROPERTY_VARIATION_2
												g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"apa_v_mp_h_02")
												PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = apa_v_mp_h_02")
											ELIF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation = PROPERTY_VARIATION_3	
												g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"apa_v_mp_h_03")
												PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = apa_v_mp_h_03")
											ELIF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation = PROPERTY_VARIATION_4	
												g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"apa_v_mp_h_04")
												PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = apa_v_mp_h_04")
											ELIF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation = PROPERTY_VARIATION_5
												g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"apa_v_mp_h_05")
												PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = apa_v_mp_h_05")
											ELIF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation = PROPERTY_VARIATION_6
												g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"apa_v_mp_h_06")
												PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = apa_v_mp_h_06")
											ELIF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation = PROPERTY_VARIATION_7
												g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"apa_v_mp_h_07")
												PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = apa_v_mp_h_07")
											ELIF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation = PROPERTY_VARIATION_8
												g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"apa_v_mp_h_08")
												PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = apa_v_mp_h_08")
											ENDIF
										ELIF IS_PROPERTY_STILT_APARTMENT(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum, PROPERTY_STILT_APT_5_BASE_A)
											IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bActivatedCutsomaprtmentIpls
												IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum = PROPERTY_STILT_APT_8_A
													ACTIVATE_IPLS_FOR_STILT_APARTMENT(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum)
													g_TransitionSessionNonResetVars.sPostMissionCleanupData.bActivatedCutsomaprtmentIpls = TRUE
													PRINTLN("[PMC] - activated custom apartment IPLs for PROPERTY_STILT_APT_8_A - bActivatedCutsomaprtmentIpls = TRUE")
												ENDIF
											ENDIF
											g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"apa_v_mp_stilts_a")
											PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = apa_v_mp_stilts_a")
										ELIF IS_PROPERTY_STILT_APARTMENT(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum, PROPERTY_STILT_APT_1_BASE_B)
											g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"apa_v_mp_stilts_b")
											PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = apa_v_mp_stilts_b")
										ELIF IS_PROPERTY_BUSINESS_APARTMENT(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum)
											g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"hei_dlc_apart_high2_new")
											PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = hei_dlc_apart_high2_new")
										ELSE
											g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords,"hei_dlc_apart_high_new")
											PRINTLN("[PMC] - apartment interior not valid yet, grabbing form coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords, ", interior type = hei_dlc_apart_high_new")
										ENDIF
									ELSE
										IF IS_INTERIOR_DISABLED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment) 
		//										SCRIPT_ASSERT("[PMC] - Trying to load a disabled interior.")
											DISABLE_INTERIOR(g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment, FALSE)
											PRINTLN("[PMC] - Trying to load a disabled interior, re-enabling.")
										ENDIF
		//									IF IS_INTERIOR_CAPPED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment)
		//										SCRIPT_ASSERT("[PMC] - Trying to load a capped interior.")
		//										PRINTLN("[PMC] - Trying to load a capped interior.")
		//									ENDIF
	//									#ENDIF
										PIN_INTERIOR_IN_MEMORY(g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment)
										IF IS_INTERIOR_READY(g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment)
											RETAIN_ENTITY_IN_INTERIOR(PLAYER_PED_ID(), g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment)
											PRINTLN("[PMC] - pinned apartment into memory.")
											bLoadedApartment = TRUE
										ELSE
											PRINTLN("[PMC] - pinning apartment into memory.")
										ENDIF
									ENDIF
								ELSE
									// Something bad has happened with the aprtment location. Bail and do a normal on foot end. 
									SET_POST_MISSION_PLACE_IN_APARTMENT_FLAG(FALSE)
									g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum = -1
									g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyInstance = -1
									g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyEntrance = -1
									g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation = -1
									g_TransitionSessionNonResetVars.sPostMissionCleanupData.bActivatedCutsomaprtmentIpls = FALSE
									
									sData.iOnFootWarpStage = 0
									PRINTLN("[PMC] - something bad has happened with the aprtment location, the vector is zero. Forget the aprtment, get a new spawn location and let's go there. Setting iOnFootWarpStage = 0 and bPlaceInApartment = FALSE.")
								ENDIF
							ELSE
								bLoadedApartment = TRUE
							ENDIF
							
						ENDIF
						
						IF bLoadedApartment
							IF NOT IS_SKYSWOOP_NEEDED()
								
								PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED = FALSE.")
								
								IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bStartedFirstLoadScene
								
									PRINTLN("[PMC] - bStartedFirstLoadScene = TRUE.")
									
									IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bFirstLoadSceneComplete
										PRINTLN("[PMC] - bFirstLoadSceneComplete = TRUE.")
										bDoWarp = TRUE
									ELSE
										PRINTLN("[PMC] - bFirstLoadSceneComplete = FALSE.")
									ENDIF
									
								ELSE
								
									PRINTLN("[PMC] - bStartedFirstLoadScene = FALSE.")
									bDoWarp = TRUE
									
								ENDIF
								
							ELSE
								
								PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED = TRUE, setting bDoWarp = TRUE.")
								bDoWarp = TRUE
								
							ENDIF
						ENDIF
						
						IF bDoWarp
							
							PRINTLN("[PMC] - ok to warp = TRUE, calling WARP_TO_SPAWN_LOCATION.")
							
							IF g_TransitionSessionNonResetVars.bFinishedFirstArenaEvent
							AND sData.eSpawnLoc != SPAWN_LOCATION_ARENA_GARAGE
								PLAYER_INDEX pOwner
								pOwner = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(g_TransitionSessionNonResetVars.ghOriginalArenaOwnerLeader)
								
								IF pOwner != INVALID_PLAYER_INDEX()
									sData.eSpawnLoc = SPAWN_LOCATION_ARENA_GARAGE
									g_SimpleInteriorData.eRelaunchInteriorScript = SIMPLE_INTERIOR_ARENA_GARAGE_1
									
									#IF IS_DEBUG_BUILD
										IF pOwner = PLAYER_ID()
											PRINTLN("[PMC] - ok to warp = TRUE, override spawn location SPAWN_LOCATION_ARENA_GARAGE. I am the owner")
										ELSE
											PRINTLN("[PMC] - ok to warp = TRUE, override spawn location SPAWN_LOCATION_ARENA_GARAGE. I am not the owner")
										ENDIF
									#ENDIF
								ELSE
									SCRIPT_ASSERT("[PMC] - Finished first arena event, but cannot get a handle on the original owner!")
									g_TransitionSessionNonResetVars.bFinishedFirstArenaEvent 	= FALSE
									g_TransitionSessionNonResetVars.bStartedFirstArenaEvent		= FALSE
								ENDIF
							ENDIF
												
							IF sData.bAllWarpLocIsForRace
							AND NOT g_sFMMCEOM.bDoWarp
							AND IS_SKYSWOOP_NEEDED()
								
								FLOAT fHeadingTemp
								
								IF (AreSpecificCoordsSetToArena() AND sData.eSpawnLoc = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
									PRINTLN("[PMC] - AreSpecificCoordsSetToArena = TRUE, no need to shift coords.")
									sData.iOnFootWarpStage = 2
								ELSE
									IF HAS_GOT_SPAWN_LOCATION(sData.vRaceAllWarpLocSpawnResult, fHeadingTemp, sData.eSpawnLoc, FALSE, FALSE)
										PRINTLN("TRANSITION TIME - FREEMODE - TRANSITION COMPLETED HAS_GOT_SPAWN_LOCATION - [PMC]              - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
										PRINTLN("[PMC] - iOnFootWarpStage = 2.")
										SETUP_SPECIFIC_SPAWN_LOCATION(sData.vRaceAllWarpLocSpawnResult, fHeadingTemp)
										sData.iOnFootWarpStage = 2
									ENDIF
								ENDIF
								
							ELSE
								
								IF IS_NET_PLAYER_OK(PLAYER_ID())
									IF ( (NOT IS_PLAYER_RESPAWNING(PLAYER_ID())) OR (HAS_NET_TIMER_EXPIRED(sData.stRezzedElsewhereTimer_Bug1717705, 30000)) )
										IF WARP_TO_SPAWN_LOCATION(sData.eSpawnLoc, FALSE, FALSE, FALSE, (g_sFMMCEOM.bDoWarp OR IS_SKYSWOOP_NEEDED() OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment OR g_TransitionSessionNonResetVars.bFinishedFirstArenaEvent) )
											
											PRINTLN("TRANSITION TIME - FREEMODE - [PMC] - TRANSITION COMPLETED WARP_TO_SPAWN_LOCATION - [PMC]               - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
											PRINTLN("[PMC] - iOnFootWarpStage = 3.")
											
											IF g_TransitionSessionNonResetVars.bFinishedFirstArenaEvent
												g_TransitionSessionNonResetVars.bFinishedFirstArenaEvent	= FALSE
												g_TransitionSessionNonResetVars.bStartedFirstArenaEvent		= FALSE
												DO_SCREEN_FADE_OUT(250)
												SET_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DOING_TRANSITION_INTO_SIMPLE_INTERIOR_VIA_SKYCAM_QUICKWARP)
												PRINTLN("[SIMPLE_INTERIOR] Set BS5_SIMPLE_INTERIOR_DOING_TRANSITION_INTO_SIMPLE_INTERIOR_VIA_SKYCAM_QUICKWARP")
											ENDIF
											
											IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
												IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
													IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
														PRINTLN("[PMC] - placing in apartment after heist or heist planning mission, setting heading to face planning room board.")
														SET_ENTITY_HEADING(PLAYER_PED_ID(), POSITION_HEIST_PLAYERS_TO_FACE_BOARD(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum))
													ENDIF
												ENDIF
											ENDIF
											
											sData.bDidOnFootWarp = TRUE
											sData.iOnFootWarpStage = 3
											
										ENDIF
									ELSE
										PRINTLN("[PMC] - IS_PLAYER_RESPAWNING returning TRUE, waiting until FALSE to continue with warp to location.")
									ENDIF
								ELSE
									PRINTLN("[PMC] - IS_NET_PLAYER_OK returning FALSE, waiting until TRUE to continue with warp to location.")
								ENDIF
								
							ENDIF
							
						ELSE
							PRINTLN("[PMC] - ok to warp = FALSE.")
						ENDIF
						
					ENDIF
					
				BREAK
				
				CASE 2
					
					IF WARP_TO_SPAWN_LOCATION(sData.eSpawnLoc, FALSE, FALSE, FALSE, (g_sFMMCEOM.bDoWarp OR IS_SKYSWOOP_NEEDED()))
						PRINTLN("TRANSITION TIME - FREEMODE - TRANSITION COMPLETED WARP_TO_SPAWN_LOCATION - [PMC]              - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
						PRINTLN("[PMC] - iOnFootWarpStage = 3.")
						sData.bDidOnFootWarp = TRUE
						sData.iOnFootWarpStage = 3
					ENDIF
						
				BREAK
				
				CASE 3
					
					BOOL bGoToLoadScene
					
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != (-1)
					
						bGoToLoadScene = TRUE
						PRINTLN("[PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, ", bGoToLoadScene = TRUE.")
						
					ELIF ( g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
					AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration )
						
						bGoToLoadScene = TRUE
						PRINTLN("[PMC] - bDoingHeistCelebration = TRUE, bGoToLoadScene = TRUE.")
						
					ELSE
						
						IF IS_PLAYER_SCTV(PLAYER_ID())
							
							PRINTLN("[PMC] - IS_PLAYER_SCTV(PLAYER_ID()) = TRUE, setting bGoToLoadScene = TRUE.")
							bGoToLoadScene = TRUE
						
						ELIF (g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment AND NOT IS_BIT_SET(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bMissionRestartInProgress))
							
							IF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
								PRINTLN("[PMC] - player inside aprtment and aprtment script has started, bGoToLoadScene = TRUE.")
//								IF HAS_PLANNING_BOARD_LOADED()
//								OR NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY)
									
										IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
										AND NOT IS_PLAYER_SCTV(PLAYER_ID())
											SET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD(TRUE)
											PRINTLN("[PMC] - [NETCELEBRATION] - [SAC] - apartment is ready, set SET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD(TRUE).")
										ENDIF
										
										PRINTLN("[PMC] - planning board has loaded, bGoToLoadScene = TRUE.")
										bGoToLoadScene = TRUE
									ELSE
										PRINTLN("[PMC] - PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY bit not set yet.")
									ENDIF
//								ELSE
//									IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.timerApartmentLoadBoard)
//										PRINTLN("[PMC] - starting timerApartmentLoadBoard.")
//										START_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.timerApartmentLoadBoard, TRUE)
//									ELSE
//										IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.timerApartmentLoadBoard, 60000, TRUE)
//											IF NOT GET_HEIST_CORONA_ACTIVE()
//												PRINTLN("[PMC] - timerApartmentLoadBoard has expired, kicking in failsafe, setting bGoToLoadScene = TRUE.")
//												SCRIPT_ASSERT("[PMC] - HAS_PLANNING_BOARD_LOADED() expired, bGoToLoadScene = TRUE.")
//												bGoToLoadScene = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//									PRINTLN("[PMC] - planning board has not loaded yet.")
//								ENDIF
							ELSE
								IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.timerApartmentScriptStartTimer)
									PRINTLN("[PMC] - starting timerApartmentScriptStartTimer.")
									START_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.timerApartmentScriptStartTimer, TRUE)
								ELSE
									IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.timerApartmentScriptStartTimer, 60000, TRUE)
										PRINTLN("[PMC] - timerApartmentScriptStartTimer has expired, kicking in failsafe, setting bGoToLoadScene = TRUE.")
										SCRIPT_ASSERT("[PMC] - IS_PLAYER_IN_MP_PROPERTY() expired, bGoToLoadScene = TRUE.")
										bGoToLoadScene = TRUE
									ENDIF
								ENDIF
							ENDIF
							
						ELSE
						
							PRINTLN("[PMC] - bPlaceInApartment = FALSE, setting bGoToLoadScene = TRUE.")
							bGoToLoadScene = TRUE
							
						ENDIF
						
					ENDIF
					
					IF bGoToLoadScene
						
						// Put the player there. 
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
								ENDIF
								CLEAR_PED_WETNESS(PLAYER_PED_ID())
							ENDIF
						ENDIF
						
						IF IS_TUTORIAL_CUTSCENE_SETUP()
							
							// If we just warped to Lamar's house, unset the flag saying we need to do it.
							IF SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE()
								SET_LOCAL_PLAYER_SHOULD_SPANW_AT_LAMARS_HOUSE(FALSE)
							ENDIF
							
							// Get some vehicles nearby.
							INSTANTLY_FILL_VEHICLE_POPULATION()
							
							// Put the camera in a nice looking position.
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							
							// For Neil to let him know we're done.
							SET_MANUAL_RESPAWN_STATE(MRS_FINISH_RESPAWN)
							
							PRINTLN("[PMC] - completed warp to nearby on foot.")
							
							REQUEST_PERSONAL_VEHICLE_TO_WARP_NEAR(IS_CORONA_INITIALISING_A_QUICK_RESTART())
							
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnRandomVehicle = TRUE
							
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_LOAD_SCENE)
							
						ELSE
							
							PRINTLN("[PMC] - IS_TUTORIAL_CUTSCENE_SETUP return FALSE.")
							
						ENDIF
						
					ENDIF
					
				BREAK
				
			ENDSWITCH
			
		BREAK
		
		
		// ******************************************
		// Swoop back down.
		// ******************************************
		CASE ePOSTMISSIONCLEANUPSTAGE_LOAD_SCENE
			
			IF IS_PLAYER_SCTV(PLAYER_ID())
				
				sData.iLoadSceneStage = 0
				RESET_NET_TIMER(sData.stLoadSceneTimer)
				CLEAR_INITIAL_SPAWN_LOCATION(0)
				CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT() 
				CLEAR_SPAWNING_MISSION_DATA()
				GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_FADE_IN)
				
			ELSE
			
				BOOL bMoveOn
				
				IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != (-1)
					bMoveOn = TRUE
					sData.iLoadSceneStage = 2
					PRINTLN("[PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, ", bMoveOn = TRUE and sData.iLoadSceneStage = 2")
				ENDIF
				
				// No falling through the world while waiting for the load scene.
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				// Load the scene.
				SWITCH sData.iLoadSceneStage
					
					CASE 0
						
//						IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bStartedFirstLoadScene
							
							// We are back in freemode, decide which little anim the player is going to play.
							IF NOT g_sFMMCEOM.bDoWarp
								IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
									IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
										IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
											IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
											
												#IF FEATURE_FIXER
												IF (g_TransitionSessionNonResetVars.sPostMissionCleanupData.PostMissionSpawnScenario = POST_MISSION_SPAWN_SITTING_SMOKING)													
													SET_SPAWN_ACTIVITY(SPAWN_ACTIVITY_SITTING_SMOKING)	
													g_TransitionSpawnData.bIgnoreSpawnActivity = FALSE
												ELIF (g_TransitionSessionNonResetVars.sPostMissionCleanupData.PostMissionSpawnScenario = POST_MISSION_SPAWN_DRUNK_WAKE_UP_MUSIC_STUDIO)														
													SET_SPAWN_ACTIVITY(SPAWN_ACTIVITY_WAKE_UP_ON_FLOOR_SMOKING)	
													g_TransitionSpawnData.bIgnoreSpawnActivity = FALSE
												ELSE
												#ENDIF
											
												DECIDE_ACTIVITY_FOR_SPAWNING_AT_LAST_LOCATION(TRUE)
												
												#IF FEATURE_FIXER
												ENDIF
												#ENDIF
												
												sData.bCalledDecideAnim = TRUE
												PRINTLN("[PMC] - called DECIDE_ACTIVITY_FOR_SPAWNING_AT_LAST_LOCATION to decide which anim to play upon re-entry.")
											ELSE
												PRINTLN("[PMC] - not calling DECIDE_ACTIVITY_FOR_SPAWNING_AT_LAST_LOCATION, player is doing a heist celebration.")
											ENDIF
										ELSE
											PRINTLN("[PMC] - not calling DECIDE_ACTIVITY_FOR_SPAWNING_AT_LAST_LOCATION, player is in a vehicle.")
										ENDIF
									ELSE
										PRINTLN("[PMC] - not calling DECIDE_ACTIVITY_FOR_SPAWNING_AT_LAST_LOCATION, player is dead.")
									ENDIF
								ELSE
									PRINTLN("[PMC] - not calling DECIDE_ACTIVITY_FOR_SPAWNING_AT_LAST_LOCATION, player does not exist.")
								ENDIF
							ELSE
								PRINTLN("[PMC] - not calling DECIDE_ACTIVITY_FOR_SPAWNING_AT_LAST_LOCATION, g_sFMMCEOM.bDoWarp = TRUE.")
							ENDIF
							
							
							// clearing focus or stoping a load scene during a player switch can break things. url:bugstar:6762771 
							IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
							
								// Make sure the focus is clear in prep for cleanup.
								CLEAR_FOCUS()
								
								// Stop load scenes.
								NEW_LOAD_SCENE_STOP()
							
							ELSE
								PRINTLN("[PMC] - player switch in progress, not calling CLEAR_FOCUS or NEW_LOAD_SCENE_STOP.")	
							
							ENDIF
							
//						ENDIF
						
						PRINTLN("[PMC] - stopped any load scenes currently running. sData.iLoadSceneStage = 1.")
						sData.iLoadSceneStage++
						
					BREAK
					
					CASE 1
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						
						// Start loading.
//						IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bStartedFirstLoadScene
						IF NOT sData.bDidOnFootWarp
							IF NOT IS_SKYSWOOP_NEEDED()
								
								VECTOR vDir
								NEWLOADSCENE_FLAGS eFlag
								
								vDir = GET_ENTITY_ROTATION(PLAYER_PED_ID())
								
								IF IS_VECTOR_ZERO(vDir)
									vDir = << 0.1, 0.1, 0.1 >>
								ENDIF
								
								IF NOT g_sFMMCEOM.bDoWarp
									eFlag = NEWLOADSCENE_FLAG_REQUIRE_COLLISION
								ENDIF
								
								IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
									IF NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 25.0, eFlag)
										#IF IS_DEBUG_BUILD
										VECTOR vTempPlayercoords
										vTempPlayercoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
										PRINTLN("[PMC] - started new load scene at coords: ", vTempPlayercoords)
										PRINTLN("[PMC] - new load scene started. sData.iLoadSceneStage = 2.")
										#ENDIF
										sData.iLoadSceneStage++
									ELSE
										PRINTLN("[PMC] - starting new load scene around player.")
									ENDIF
								ELSE
									PRINTLN("[PMC] - cannot start new load scene, IS_PLAYER_SWITCH_IN_PROGRESS = TRUE. sData.iLoadSceneStage = 2.")
									sData.iLoadSceneStage++
								ENDIF
							
							ELSE
								PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED = TRUE, sData.iLoadSceneStage = 2.")
								sData.iLoadSceneStage++
							ENDIF
						ELSE
							PRINTLN("[PMC] - bDidOnFootWarp = FALSE, sData.iLoadSceneStage = 2.")
							sData.iLoadSceneStage++
						ENDIF
						
					BREAK
					
					// Wait for the scene to finish loading.
					CASE 2
						
						// Timer in case load scene takes too long or gets stuck.
						IF NOT HAS_NET_TIMER_STARTED(sData.stLoadSceneTimer)
							START_NET_TIMER(sData.stLoadSceneTimer)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(sData.stLoadSceneTimer, 30000)
								bMoveOn = TRUE
								NET_SCRIPT_ASSERT("Post mission cleanup IS_NEW_LOAD_SCENE_LOADED() has taken > 30 seconds to return TRUE. Cancelling load.")
							ENDIF
						ENDIF
						
//						IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bStartedFirstLoadScene
						
						IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = (-1)
							IF NOT IS_SKYSWOOP_NEEDED()
								IF NOT sData.bDidOnFootWarp
									IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
										IF IS_NEW_LOAD_SCENE_LOADED()
											PRINTLN("[PMC] - new load scene finished loading.")
											bMoveOn = TRUE
										ELSE
											PRINTLN("[PMC] - new load scene still loading.")
										ENDIF
									ELSE
										bMoveOn = TRUE
										PRINTLN("[PMC] - IS_PLAYER_SWITCH_IN_PROGRESS = TRUE, setting bMoveOn = TRUE.")
									ENDIF
								ELSE
									bMoveOn = TRUE
									PRINTLN("[PMC] - bStartedFirstLoadScene = TRUE, setting bMoveOn = TRUE.")
								ENDIF
							ELSE
								bMoveOn = TRUE
								PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED = TRUE, setting bMoveOn = TRUE.")
							ENDIF
						ELSE
							SET_NO_LOADING_SCREEN(TRUE)
							PRINTLN("[PMC] - called SET_NO_LOADING_SCREEN(TRUE). At request of Neil Ferguson for 2550602.")
							START_SYNCED_INTERACTION_SCENE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPlayerNameHash)
							IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlayBennyPreScene
								PRINTLN("[PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlayBennyPreScene = TRUE, calling SET_SYNCED_INTERACTION_TO_PLAY_PRE_CUTSCENE(0)")
								SET_SYNCED_INTERACTION_TO_PLAY_PRE_CUTSCENE(0)
							ENDIF
							bMoveOn = TRUE
							PRINTLN("[PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, " setting bMoveOn = TRUE.")
						ENDIF
						
						IF bMoveOn
							// clearing focus or stoping a load scene during a player switch can break things. url:bugstar:6762771 
							IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
								NEW_LOAD_SCENE_STOP()
								CLEAR_FOCUS()
							ELSE
								PRINTLN("[PMC] - player switch in progress, not calling CLEAR_FOCUS or NEW_LOAD_SCENE_STOP. (2)")
							ENDIF
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
									CLEAR_PED_WETNESS(PLAYER_PED_ID())
									CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
								ENDIF
							ENDIF
							sData.iLoadSceneStage = 0
							RESET_NET_TIMER(sData.stLoadSceneTimer)
							CLEAR_ACTION_AND_PASSIVE_MODE()
							CLEAR_INITIAL_SPAWN_LOCATION(1)
							CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT() 
							CLEAR_SPAWNING_MISSION_DATA()
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_CUT_TO_PLAYER)
							RESET_NET_TIMER(sData.sRestartStuckTimer)
						ENDIF
						
					BREAK
					
				ENDSWITCH
			ENDIF
			
		BREAK
		
		
		
		// ******************************************
		// Go back to gameplay.
		// ******************************************
		CASE ePOSTMISSIONCLEANUPSTAGE_CUT_TO_PLAYER
			
			// No radar.
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			
			// If we have been doing a warp to specific location for Bobby, let Keith's mission/corona stuff know so his checks work properly.
			IF g_sFMMCEOM.bDoWarp
				sData.iPostCleanupReadyForPlayAgainSwoopDownStage = 1
			ENDIF
			
			IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
			OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()	
			OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != (-1)
			
				IF (sData.bCalledDecideAnim = FALSE)
				OR ((sData.bCalledDecideAnim = TRUE) AND IS_SPAWN_ACTIVITY_READY())
					// Reset clothes.
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != (-1)
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							Clear_All_Generated_MP_Headshots(FALSE)
							IF NOT GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
								SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[PMC] - not setting into freemode clothes, g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId)
					ENDIF
					PRINTLN("[PMC] - IS_CORONA_INITIALISING_A_QUICK_RESTART() = TRUE or g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId != (-1), time to finish up.")
					GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP)
				ELSE
					PRINTLN("[PMC]	[PMC] - IS_CORONA_INITIALISING_A_QUICK_RESTART - waiting for spawn activity")
				ENDIF
										
			ELSE
				
				// If we are ready to cut back to the player.
				IF OK_TO_CUT_TO_PLAYER(sData)
					
					BOOL bEntryAnimReady
					
					IF NOT sData.bCalledDecideAnim
						bEntryAnimReady = TRUE
					ELSE
						IF IS_SPAWN_ACTIVITY_READY()
							bEntryAnimReady = TRUE
						ELSE
							PRINTLN("[PMC] - waiting for IS_SPAWN_ACTIVITY_READY() to return TRUE.")
						ENDIF
					ENDIF
					
					IF bEntryAnimReady
					
					// Keep this stuff lying around in case we need it later when the design changes.
					
		//				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
		//				AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		//				IF SET_SKYSWOOP_DOWN(NULL, FALSE)
							
							// We do this in two stages so the dispable of the renderpahses and blur are a frame after the game has returned to the gameplay camera.
							// This stop ugly flickering and popping.
							SWITCH sData.iCutToPlayerStage
								
								CASE 0
									
									// Remove parachute. B*1603513.
									IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
										IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
											Clear_All_Generated_MP_Headshots(FALSE)
											IF NOT GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
												SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
											ENDIF
										ENDIF
									ELSE
										PRINTLN("[PMC] - not setting into freemode clothes, bDoingAfterApartmentPanHeistCelebration = TRUE.")
									ENDIF
									
									// Turn off the leaderboard camera. 
									IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
										RENDER_SCRIPT_CAMS(FALSE, FALSE, 1000, FALSE, TRUE)
										SET_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, FALSE)
										DESTROY_CAM(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, TRUE)
									ENDIF
									
									NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
									
									SET_LOADING_ICON_INACTIVE()
									
									g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping = FALSE
									
									BUSYSPINNER_OFF()
									PRINTLN("[PMC] - BUSYSPINNER_OFF() called.")
									
									IF GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP()
										IF IS_TUTORIAL_CUTSCENE_SETUP()
											IF NOT SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE()
												IF DOES_CAM_EXIST(g_CamTutorial)
									                //-- When you're finished
									                SET_CAM_ACTIVE(g_CamTutorial, TRUE)
													RENDER_SCRIPT_CAMS(TRUE, FALSE)
													PRINTLN("[PMC] - [dsw] Setting g_CamTutorial active in post mission cleanup")
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									// Reactivate the player if not doing the sky swoop. If we arre doing the sky swoop this will b done later on once it has almost completed.
									IF NOT IS_SKYSWOOP_NEEDED()
										PRINTLN("[PMC] - iCutToPlayerStage = 0, sky swoop not needed.")
										IF REACTIVATE_PLAYER_FOR_END_OF_CLEANUP()
											PRINTLN("[PMC] - call 0 to RESET_MISSION_CONTROLLER_IPLS().")
											RESET_MISSION_CONTROLLER_IPLS()
											sData.iCutToPlayerStage++
										ENDIF
									ELSE
										PRINTLN("[PMC] - call 1 to RESET_MISSION_CONTROLLER_IPLS().")
										RESET_MISSION_CONTROLLER_IPLS()
										sData.iCutToPlayerStage++
									ENDIF
									
								BREAK
								
								CASE 1
									
									IF (GET_CORONA_STATUS() = CORONA_STATUS_IDLE) // Fix bobby suggested for B*1392555.
										IF NOT IS_PLAYER_ABOUT_TO_START_TUTORIAL_MISSION()
											PRINTLN("[PMC] - (GET_CORONA_STATUS() = CORONA_STATUS_IDLE), calling DISABLE_SCREENBLUR_FADE().")
											IF NOT IS_SKYSWOOP_NEEDED()
												DISABLE_SCREENBLUR_FADE()
											ENDIF
										ELSE
											PRINTLN("[PMC] - Not calling DISABLE_SCREENBLUR_FADE() as IS_PLAYER_ABOUT_TO_START_TUTORIAL_MISSION.")
										ENDIF
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[PMC] - (GET_CORONA_STATUS() != CORONA_STATUS_IDLE), not calling DISABLE_SCREENBLUR_FADE(), leaving blur on, Bobby will clean it.")
									#ENDIF
									ENDIF
									
									// Get rid of any post fx, we're cutting back to gameplay/corona.
									IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
										IF NOT IS_PLAYER_ABOUT_TO_START_TUTORIAL_MISSION()
										OR (IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() AND g_bFailedTutorialMission)
											IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
												IF NOT IS_SKYSWOOP_NEEDED()
													IF NOT g_sFMMCEOM.bDoWarp
														IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
															ANIMPOSTFX_STOP("MinigameTransitionIn")
														ENDIF
														IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Win")
														    ANIMPOSTFX_STOP("MP_Celeb_Win")
														ENDIF
														IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Lose")
														   ANIMPOSTFX_STOP("MP_Celeb_Lose")
														ENDIF
														SET_SKYBLUR_CLEAR()
														PRINTLN("[PMC] - ANIMPOSTFX_STOP ")
													ENDIF
												ENDIF
												SET_SKYFREEZE_CLEAR(TRUE)
												SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)
												IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.bRunPostMatchTransitionScene
													g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.bRunPostMatchTransitionScene = FALSE
													PRINTLN("[PMC] - set bRunPostMatchTransitionScene = FALSE. Call 4.")
												ENDIF
											ENDIF
										ELSE
											PRINTLN("[PMC] - Not calling ANIMPOSTFX_STOP as IS_PLAYER_ABOUT_TO_START_TUTORIAL_MISSION")
											PRINTLN("[PMC] - Not turning off splash screen - 4")
										ENDIF
									ENDIF
									
									sData.iCutToPlayerStage++
									
								BREAK
								
							CASE 2
								
	//							// Fade in.
	//							IF NOT IS_SKYSWOOP_NEEDED()
	//								IF SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE()
	//									IF NOT IS_SCREEN_FADED_IN()
	//										IF IS_NET_PLAYER_OK(PLAYER_ID())
	//											IF NOT IS_SCREEN_FADING_IN()
	//												DO_SCREEN_FADE_IN(2000)
	//											ENDIF
	//										ENDIF
	//									ENDIF
	//								ENDIF
	//							ENDIF
	//							
								IF NOT g_sFMMCEOM.bDoWarp
									MAKE_LOCAL_PLAYER_VISIBLE_THIS_FRAME()
								ENDIF
								
								IF IsSwoopDownAlmostFinished()
									UNFREEZE_RECREATED_AMBIENT_EOM_CAR()
								ENDIF
								
								// Swoop camera back down.
								IF DO_SKYSWOOP_IF_NEEDED(FALSE)
									
									IF (GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE)
									AND IS_BIT_SET(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DOING_TRANSITION_INTO_SIMPLE_INTERIOR_VIA_SKYCAM_QUICKWARP)
										KILL_PLAYER_SWITCH_NOW()
										PRINTLN("[PMC] - IS_SKYSWOOP_NEEDED - Kill Skycam")
									ENDIF
									
									IF REACTIVATE_PLAYER_FOR_END_OF_CLEANUP()
										
										g_TransitionSessionNonResetVars.sPostMissionCleanupData.bWantSwoopCam = FALSE
										
										STOP_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
										PRINTLN("[PMC] - called STOP_AUDIO_SCENE(MP_JOB_CHANGE_RADIO_MUTE).")
										
		//								IF NOT g_sFMMCEOM.bDoWarp
		//									g_bPlayEndOfJobRadioFrontEnd = FALSE
		//								ELSE
		//									g_iPlayEndOfJobRadioFrontEndState = 0
		//									SET_AUDIO_FLAG("AllowRadioDuringSwitch", FALSE)
		//									PRINTLN("[PMC] - setting g_iPlayEndOfJobRadioFrontEndState = 0 because bDoWarp = TRUE.")
		//								ENDIF
										
										IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnRandomVehicle
											SPAWN_RANDOM_VEHICLE_NEAR_PLAYER()
											PRINTLN("[PMC] - sPostMissionCleanupData.bSpawnRandomVehicle = TRUE.")
											PRINTLN("[PMC] - called SPAWN_RANDOM_VEHICLE_NEAR_PLAYER().")
										#IF IS_DEBUG_BUILD
										ELSE
											PRINTLN("[PMC] - sPostMissionCleanupData.bSpawnRandomVehicle = FALSE.")
										#ENDIF
										ENDIF
										
										IF g_bLastMissionWVM
											IF IS_GUNRUNNING_TRUCK_PURCHASED()
												IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
													PRINTLN("[PMC] - <truck> current mission is WVM, player owns truck, gang boss - LAUNCH DROP!!!")
													PRINTLN("[PMC] - <truck> MPGlobalsAmbience.bLaunchVehicleDropTruck = TRUE")
													MPGlobalsAmbience.bLaunchVehicleDropTruck = TRUE
												ELSE
													PRINTLN("[PMC] - <truck> player not gang boss")
												ENDIF
											ELSE
												PRINTLN("[PMC] - <truck> player doesnt own MOC truck")
											ENDIf
										ELSE
											PRINTLN("[PMC] - <truck> g_bLastMissionWVM = FALSE")
										ENDIF
										
										GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_FADE_IN)
										
									ENDIF
								ENDIF
								
							BREAK
							
						ENDSWITCH
						
						// If we just warped to Lamar's house, unset the flag saying we need to do it.
						IF SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE()
							SET_LOCAL_PLAYER_SHOULD_SPANW_AT_LAMARS_HOUSE(FALSE)
						ENDIF
							
		//				ENDIF
		//				ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// ******************************************
		// Fade in.
		// ******************************************
		CASE ePOSTMISSIONCLEANUPSTAGE_FADE_IN
						
							
			// If we were doing a warp for Bobby/Keith, skip this.
			IF g_sFMMCEOM.bDoWarp
			
				PRINTLN("[PMC] - g_sFMMCEOM.bDoWarp.")
				GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP)
			
			ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
			
				PRINTLN("[PMC] - bDoingHeistCelebration = TRUE.")
				GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP)
				
			// If nothing for Bobby/Keith, make sure the screen is faded in, then move on.
			ELSE
				
				MAKE_LOCAL_PLAYER_VISIBLE_THIS_FRAME()
				
				IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpecCamDoingTransition
					
					PRINTLN("[PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpecCamDoingTransition = TRUE, don;t want to interfere with the spectator camera. Going to clean up so the spec cam can finish off fades and camera positions.")
					GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP)
					
				ELSE
					
					IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
						// Fade in.
						IF NOT IS_SCREEN_FADED_IN()	
						#IF FEATURE_SHORTEN_SKY_HANG
						AND NOT (g_bSkySwoopDownDidFadeOut) // added by Neil
						#ENDIF
						
							IF IS_NET_PLAYER_OK(PLAYER_ID())
								IF NOT IS_SCREEN_FADING_IN()
									IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
									AND NOT IS_BIT_SET(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DOING_TRANSITION_INTO_SIMPLE_INTERIOR_VIA_SKYCAM_QUICKWARP)
										IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
											ANIMPOSTFX_STOP("MinigameTransitionIn")
										ENDIF
										IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Win")
										    ANIMPOSTFX_STOP("MP_Celeb_Win")
										ENDIF
										IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Lose")
										   ANIMPOSTFX_STOP("MP_Celeb_Lose")
										ENDIF
										TOGGLE_RENDERPHASES(TRUE)
										SET_SKYBLUR_CLEAR()
										DO_SCREEN_FADE_IN(2000)
									ENDIF
								ENDIF
							ENDIF
							
						// tidy up and return that we're all done.
						ELSE
							PRINTLN("[PMC] - screen is faded in.")
							GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP)
						ENDIF
					ELSE
						PRINTLN("[PMC] - IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL = TRUE, Dave will sort the fade.")
						GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP)
					ENDIF
				
				ENDIF
				
			ENDIF
			
		BREAK
		
		
		
		// ******************************************
		// Setup for tutorial cut.
		// ******************************************
		CASE ePOSTMISSIONCLEANUPSTAGE_TUTORIAL_CUT
			
			// If we need to do a special cut for Dave's tutorial, do it.
			IF GET_SHOULD_BE_DOING_TUTORIAL_CLEANUP()
				PRINTLN("[PMC] - setup tutorial cut.")
				SET_LOCAL_PLAYER_READY_FOR_DM_TUTORIAL_CUT(TRUE)
				GOTO_POST_MISSION_CLEANUP_STAGE(sData.eStage, ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP)
			ENDIF
			
		BREAK
		
		
		
		// ******************************************
		// Clean up.
		// ******************************************
		CASE ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP
			
			#IF FEATURE_SHORTEN_SKY_HANG
			// wait to cleanup if the Hanging in sky did a fade out. neilf. 
			IF (g_bSkySwoopDownDidFadeOut)
			AND NOT IS_SCREEN_FADED_OUT()
				PRINTLN("[PMC] [HangingInSky] ePOSTMISSIONCLEANUPSTAGE_CLEAN_UP - waiting for the fuckhaninginsky to do fadeout.")
				RETURN FALSE
			ENDIF
			#ENDIF
			
			// \o/ we're all done. Let's cleanup and get out of here.
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				
				// Text from mechanic to tell player about personal vehicles being left nearby.
				IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID(TRUE))
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(PERSONAL_VEHICLE_NET_ID(TRUE)))
								IF GET_OWNED_PROPERTY(0) > 0
									SET_BIT(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_NEARBY_PVEH)
									PRINTLN("[PMC] - personal vehicle exists nearby. Called SET_BIT(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_NEARBY_PVEH).")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				PRINTLN("[PMC] - done, doing cleanup.")
				
				IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
					THEFEED_FLUSH_QUEUE()
					THEFEED_RESUME()
					PRINTLN("[RCC MISSION] - DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - called THEFEED_FLUSH_QUEUE and THEFEED_RESUME.")
				ENDIF
				
				BOOL bDoVisualClean
				bDoVisualClean = SHOULD_VISUAL_CLEANUP_BE_CALLED()
				
				IF g_sFMMCEOM.bDoWarp
					SET_TRANSITION_SESSIONS_CORONA_NEEDS_TO_REPAUSE_RENDER_PHASES()
				ENDIF
				
				HANDLE_CREATE_SUVIVAL_REWARDS_VEHICLES_DATA()
				
				BOOL bCleanClothes
				
				// url:bugstar:2689796
				IF SHOULD_CLEANUP_BAIL_FOR_TRANSITION_SESSIONS_INVITE()
					PRINTLN("[PMC] - SHOULD_CLEANUP_BAIL_FOR_TRANSITION_SESSIONS_INVITE = TRUE, setting bCleanClothes = TRUE")
					bCleanClothes = TRUE
				ENDIF
				IF SHOULD_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
					PRINTLN("[PMC] - SHOULD_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS = TRUE, setting bCleanClothes = TRUE")
					bCleanClothes = TRUE
				ENDIF
				IF bCleanClothes
					PRINTLN("[PMC] - called Clear_All_Generated_MP_Headshots")
					Clear_All_Generated_MP_Headshots(FALSE)
					IF g_sMagnateOutfitStruct.iBossStyle = ENUM_TO_INT(GB_BOSS_STYLE_NONE)
						PRINTLN("[PMC] - g_sMagnateOutfitStruct.iBossStyle = ENUM_TO_INT(GB_BOSS_STYLE_NONE), called SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE")
					    SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[PMC] - g_sMagnateOutfitStruct.iBossStyle != ENUM_TO_INT(GB_BOSS_STYLE_NONE), not calling SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE")
					#ENDIF
					ENDIF
				ENDIF
				
				g_sFMMCEOM.bDoWarp = FALSE
				g_sFMMCEOM.vWarp = <<0.0, 0.0, 0.0 >>
//				CLEAR_TRANSITION_SESSIONS_RESTART_PLAYLIST() - 17195578. 
				
				IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
					IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
					AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
					AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
					AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
					AND g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = -1
					AND NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlayBennyPreScene
					AND NOT SHOULD_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
					AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					AND NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bShouldTransitionSessionsRestartPlaylist
						PRINTLN("[PMC] - IS_CORONA_INITIALISING_A_QUICK_RESTART() = FALSE and (GET_CORONA_STATUS() = CORONA_STATUS_IDLE) and bDoingAfterApartmentPanHeistCelebration = FALSE, turning player control back on.")
						NETWORK_CONCEAL_PLAYER(PLAYER_ID(), FALSE) // url:bugstar:6028121 - Casino - Instanced - Meeting -  The local player returned to GTAO without movement control after quitting Casino - Meeting.
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_CLEAR_FOCUS)						
					ENDIF
				ELSE
					PRINTLN("[PMC] - (GET_CORONA_STATUS() = CORONA_STATUS_IDLE) keeping player control off and making sure the player is not visible.")
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
				
				SET_BOAT_RACE_GLOBAL(FALSE)
				
				CLEANUP_LEADERBOARD_CAM(TRUE, TRUE, bDoVisualClean, SHOULD_SCREEN_BE_UNBLURRED(), SHOULD_RENDERPHASES_BE_UNFROZEN(), SHOULD_LB_POST_FX_STOP())
				
				RESET_NET_TIMER(sData.eTimer)
				RESET_NET_TIMER(sData.stLoadSceneTimer)
				RESET_NET_TIMER(sData.stWaitForCheckPvFailTimer)
				
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_FinishedPostRaceCleanup)
				
				
				g_TransitionSessionNonResetVars.bSavedLastVehBeforeJob = FALSE
				
				IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.bRunPostMatchTransitionScene
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.bRunPostMatchTransitionScene = FALSE
					PRINTLN("[PMC] - set bRunPostMatchTransitionScene = FALSE. Call 3.")
				ENDIF
				
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoSpecificEndOfJobPos = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = <<0.0, 0.0, 0.0>>
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeenOnStandardHeistOrContactMission = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeenOnTeamNonMissionJob = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iForceIntoFreemodeDeadStage = 0
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iFreemodeDeadMoveOnStage = 0
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeenOnMiniGame = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLbCamClearedLoadsScenesAndFocus = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iCutToPlayerStage = 0
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bWantSwoopCam = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDidWarpToSwitchStartLoc = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bAllWarpLocIsForRace = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vRaceAllWarpLocSpawnResult = <<0.0,0.0,0.0>>
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze = NULL
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCompletedGeraldMission = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanupGoingToWarpIntoPv = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSkipTransitionCam = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnRandomVehicle = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCalledDecideAnim = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vStartedSpecCamLocation = << 0.0, 0.0, 0.0 >>
				RESET_TEMP_REZZED_ELSEWHERE_DATA_BUG_1717705() // Failsafe timer for bug 1717705. Temp fix until Neil is back and can look into a proper fix. #LimboBug
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete = FALSE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bStreamingRequestsComplete = FALSE
				
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent = 0.0
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords = << 0.0, 0.0, 0.0 >>
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnNearEvent = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iWarpForPvAtEvent = 0
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPausePvCreation = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlayBennyPreScene = FALSE
				
				g_bLastMissionWVM = FALSE
				PRINTLN("[PMC] - post mission cleanup cleaning up, setting g_bLastMissionWVM = FALSE so we don't recreate after races, survivals, etc. Call 0.")
				
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bShouldTransitionSessionsRestartPlaylist = FALSE
				
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeachAfterSumoSpawnFlag = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn = FALSE
				PRINTLN("[ARENA] g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn - FALSE")
				INT iCounter
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = (-1)
				REPEAT COUNT_OF(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPlayerNameHash) iCounter
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPlayerNameHash[iCounter] = (-1)
				ENDREPEAT
				
				IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = << 0.0, 0.0, 0.0 >>
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius = 0.0
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest = << 0.0, 0.0, 0.0 >>
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.bForcePostMissionWarpCreatorPosition = FALSE
				ENDIF
				
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPostMissionPosIgnoreExclusionZones = FALSE
				
				IF NOT IS_PLAYER_ABOUT_TO_START_TUTORIAL_MISSION()
				OR g_bFailedTutorialMission
					SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)
//				
//					IF HAS_SCALEFORM_MOVIE_LOADED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
//						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
//					ENDIF
				ELSE
					PRINTLN("[PMC] - Not turning off splash screen - 1")
				ENDIF
				RESET_CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE_DATA()
				
				sData.iOnFootWarpStage = 0
				
				IF bDoVisualClean
					BUSYSPINNER_OFF()
					PRINTLN("[PMC] - BUSYSPINNER_OFF() called.")
				ELSE
					PRINTLN("[PMC] - SHOULD_VISUAL_CLEANUP_BE_CALLED() is FALSE, keep busyspinner.")
				ENDIF
				
				IF bDoVisualClean
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						NEW_LOAD_SCENE_STOP()
						CLEAR_FOCUS()
						PRINTLN("[PMC] - called NEW_LOAD_SCENE_STOP() and CLEAR_FOCUS().")
					ENDIF
				ELSE
					PRINTLN("[PMC] - SHOULD_VISUAL_CLEANUP_BE_CALLED() is FALSE, not calling NEW_LOAD_SCENE_STOP() and CLEAR_FOCUS().")
				ENDIF
				
				// Want the player to be invincible for 4 seconds on respawn form a job.
				NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(4000)
				MPGlobals.g_rank_after_activity	= GET_PLAYER_FM_RANK(PLAYER_ID())
				
				
				#IF IS_DEBUG_BUILD
				 	PRINTLN("[PMC] - PRINT_UNLOCK_HELP_TICKERS g_rank_before_activity = ", g_rank_before_activity)
					PRINTLN("[PMC] - PRINT_UNLOCK_HELP_TICKERS MPGlobals.g_rank_after_activity = ", MPGlobals.g_rank_after_activity)
				#ENDIF
				
				// try and limit to last 7 ranks
				/*IF MPGlobals.g_rank_after_activity - g_rank_before_activity > 7
					g_rank_before_activity = MPGlobals.g_rank_after_activity - 7
				ENDIF
				*/
				// KW save players rank for bug 1322590
				
				// g_rank_before_activity is saved at start of mission
				IF MPGlobals.g_rank_after_activity	> g_rank_before_activity
					/*FOR i = g_rank_before_activity+1 to MPGlobals.g_rank_after_activity 
						PRINT_UNLOCK_HELP_TICKERS(i)
						PRINTLN("[PMC] - PRINT_UNLOCK_HELP_TICKERS RANK DISPLAY called at end of mission  =  ", i)
					ENDFOR*/
					
					MPGlobalsHud_TitleUpdate.display_unlock_stage = 0
					MPGlobalsHud_TitleUpdate.display_unlock_tickers = TRUE
					MPGlobalsHud_TitleUpdate.unlock_ticker_timer = 0
					MPGlobalsHud_TitleUpdate.unlock_ticker_main_timer = 0
				ENDIF
				
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					SET_MAX_WANTED_LEVEL(5)

				ENDIF
				
				#IF USE_TU_CHANGES
				IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
					IF NOT IS_PED_FALLING (PLAYER_PED_ID())
						g_bRUN_SPECIAL_REWARD_UNLOCKS = TRUE
						UNLOCK_SPECIAL_EVENT_ITEMS(FALSE)
						
					ENDIF
				ENDIF
				#ENDIF
					
				RETURN TRUE
				
			ENDIF
				
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC RESET_CLEANUP_POST_MISSION_DATA(ePOST_MISSION_CLEANUP_STAGE &eStage, INT &iMaintainStage, BOOL &bDeadWhenStartedCleanup, INT &iPlayAgainSwoopUpFlag, BOOL &bBobbyTempFlag2, BOOL &bComingOutOfPlaylist, SCRIPT_TIMER &stLoadSceneTimer, INT &iOnFootWarpStage)
	
	BOOL bDoVisualClean = SHOULD_VISUAL_CLEANUP_BE_CALLED()
	BOOL bCallCleanCam
	
	eStage = ePOSTMISSIONCLEANUPSTAGE_WAIT_FOR_LEADERBOARD_CAM_TO_BE_READY
	iMaintainStage = 0
	SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(FALSE)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_1] = FALSE
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_2] = FALSE
	iPlayAgainSwoopUpFlag = 0
	bDeadWhenStartedCleanup = FALSE
	SET_DO_TRANSITION_TO_GAME()
	bBobbyTempFlag2 = TRUE
	SET_BOAT_RACE_GLOBAL(FALSE)
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
			bCallCleanCam = TRUE
		ELSE
			IF bComingOutOfPlaylist
				bCallCleanCam = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bCallCleanCam
		CLEANUP_LEADERBOARD_CAM(TRUE, TRUE, bDoVisualClean, SHOULD_SCREEN_BE_UNBLURRED(), SHOULD_RENDERPHASES_BE_UNFROZEN(), SHOULD_LB_POST_FX_STOP())
	ENDIF
	

	g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.bRunPostMatchTransitionScene = FALSE
	PRINTLN("[PMC] - set bRunPostMatchTransitionScene = FALSE. Call 2.")
	
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoSpecificEndOfJobPos = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = <<0.0, 0.0, 0.0>>
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeenOnStandardHeistOrContactMission = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeenOnTeamNonMissionJob = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iForceIntoFreemodeDeadStage = 0
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iFreemodeDeadMoveOnStage = 0
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeenOnMiniGame = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLbCamClearedLoadsScenesAndFocus = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iCutToPlayerStage = 0
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bAllWarpLocIsForRace = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.vRaceAllWarpLocSpawnResult = <<0.0,0.0,0.0>>
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze = NULL
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCompletedGeraldMission = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanupGoingToWarpIntoPv = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSkipTransitionCam = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnRandomVehicle = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCalledDecideAnim = FALSE
	RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stWaitForCheckPvFailTimer)
	RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stMinSplashLengthTimer)
	RESET_TEMP_REZZED_ELSEWHERE_DATA_BUG_1717705() // Failsafe timer for bug 1717705. Temp fix until Neil is back and can look into a proper fix. #LimboBug
	
	IF NOT IS_PLAYER_ABOUT_TO_START_TUTORIAL_MISSION()
	OR g_bFailedTutorialMission
		SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)
//	
//		IF HAS_SCALEFORM_MOVIE_LOADED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
//			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
//		ENDIF
	ELSE
		PRINTLN("[PMC] - [dsw] - Not turning off splash screen - 2")
	ENDIF
	RESET_CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE_DATA()
	RESET_NET_TIMER(stLoadSceneTimer)
	iOnFootWarpStage = 0
	
	MPGlobals.iWatchedMissionMocapBitset = 0
	
	IF SHOULD_VISUAL_CLEANUP_BE_CALLED()
		BUSYSPINNER_OFF()
		PRINTLN("[PMC] - BUSYSPINNER_OFF() called.")
	ELSE
		PRINTLN("[PMC] - SHOULD_VISUAL_CLEANUP_BE_CALLED() is FALSE, keep busyspinner.")
	ENDIF
	
	// Turn off the leaderboard camera.
	IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bJumpingToCleanupForSp
	AND bCallCleanCam
		IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
			IF IS_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
				IF IS_CAM_RENDERING(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
					IF NOT g_sFMMCEOM.bDoWarp
						RENDER_SCRIPT_CAMS(FALSE, FALSE, 1000, FALSE, TRUE)
					ENDIF
					SET_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, FALSE)
				ENDIF
			ENDIF
			DESTROY_CAM(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, TRUE)
			NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
		ENDIF
	ENDIF
	
	bComingOutOfPlaylist = FALSE
	
ENDPROC

FUNC BOOL SAVE_AFTER_MISSION_SPAWN_LOC()
	
	INT iCheckPointIndex, iPlayerTeam
	
	SWITCH GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID())
	
		// ----------
		// Survival
		// ----------
		CASE FMMC_TYPE_SURVIVAL
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos)
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos
				PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - on a survival. g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos = ", g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos)
			ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].vPos)
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].vPos
				PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - on a survival. g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[0].vPos = ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].vPos)
			ELSE
				NET_SCRIPT_ASSERT("Doing post mission cleanup after a survival, g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos = <<0.0,0.0,0.0>>")
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
			
			MOVE_POINT_OUTSIDE_EXCLUSION_ZONES(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc, TRUE, TRUE, TRUE, TRUE)
			PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - On a survival. MOVE_POINT_OUTSIDE_EXCLUSION_ZONES has been called - New spawn pos = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc)
		BREAK
		
		// ------------
		// Deathmatch
		// ------------
		CASE FMMC_TYPE_DEATHMATCH 	
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[0].vPos)
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[0].vPos  		
				PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - on a deathmatch. g_FMMC_STRUCT.spl  = ", g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[0].vPos )		
			ELSE
				NET_SCRIPT_ASSERT("Doing post mission cleanup after a deathmatch, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[0].vPos = <<0.0,0.0,0.0>>")
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
		BREAK
		
		// ------------
		// Race
		// ------------
		CASE FMMC_TYPE_RACE 
			IF (g_FMMC_STRUCT.iNumberOfCheckPoints-1) >= 0
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints-1].vCheckpoint)
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints-1].vCheckpoint	
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.bAllWarpLocIsForRace = TRUE
					PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - on a race. g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints-1].vCheckpoint = ", g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints-1].vCheckpoint)
				ELSE
					NET_SCRIPT_ASSERT("Doing post mission cleanup after a race, g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints-1].vCheckpoint = <<0.0,0.0,0.0>>")
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RACETOPOINT)
					NET_SCRIPT_ASSERT("Doing post mission cleanup after a race, (g_FMMC_STRUCT.iNumberOfCheckPoints-1) < 0")
				ENDIF
				#ENDIF
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
		BREAK
		
		// ------------
		// P2P Race
		// ------------
		CASE FMMC_TYPE_RACE_TO_POINT 	
			IF (g_FMMC_STRUCT.iNumberOfCheckPoints-1) >= 0
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints-1].vCheckpoint)
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints-1].vCheckpoint  	
					PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - on a p2p race. g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints-1].vCheckpoint = ", g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints-1].vCheckpoint)
				ELSE
					NET_SCRIPT_ASSERT("Doing post mission cleanup after a race, g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints-1].vCheckpoint = <<0.0,0.0,0.0>>")
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ELSE
				NET_SCRIPT_ASSERT("Doing post mission cleanup after a race, (g_FMMC_STRUCT.iNumberOfCheckPoints-1) < 0")
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
		BREAK
		
		// ------------
		// Parachuting
		// ------------
		CASE FMMC_TYPE_BASE_JUMP 
			iCheckPointIndex = g_FMMC_STRUCT.iNumberOfCheckPoints
			IF (g_FMMC_STRUCT.iNumberOfCheckPoints) >= 0
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedCheckpoint[0].vCheckpoint) // If a heli jump, use num check points -1.
					iCheckPointIndex = (g_FMMC_STRUCT.iNumberOfCheckPoints-1)
				ENDIF
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedCheckpoint[iCheckPointIndex].vCheckpoint)
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = g_FMMC_STRUCT.sPlacedCheckpoint[iCheckPointIndex].vCheckpoint  	
					PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - on a base jump. g_FMMC_STRUCT.sPlacedCheckpoint[g_FMMC_STRUCT.iNumberOfCheckPoints].vCheckpoint  = ", g_FMMC_STRUCT.sPlacedCheckpoint[iCheckPointIndex].vCheckpoint)
				ELSE
					NET_SCRIPT_ASSERT("Doing post mission cleanup after a race, g_FMMC_STRUCT.sPlacedCheckpoint[iCheckPointIndex].vCheckpoint = <<0.0,0.0,0.0>>")
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ELSE
				NET_SCRIPT_ASSERT("Doing post mission cleanup after a race, (g_FMMC_STRUCT.iNumberOfCheckPoints) < 0")
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
		BREAK
		
		// ------------
		// Mission
		// ------------
		CASE FMMC_TYPE_MISSION
			iPlayerTeam = GET_PLAYER_TEAM(PLAYER_ID())
			IF (iPlayerTeam >= 0
			AND iPlayerTeam <= 3)
			OR IS_PLAYER_SCTV(PLAYER_ID()) 
            OR DID_I_JOIN_MISSION_AS_SPECTATOR()
				PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - on a mission. GET_PLAYER_TEAM(PLAYER_ID()) = ", iPlayerTeam)
				IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bOnVersusMission
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos)
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos  		
						PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - not on a versus mission. g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos = ", g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos)
					ELSE
						NET_SCRIPT_ASSERT("Doing post mission cleanup after a race, g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos = <<0.0,0.0,0.0>>")
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
						ENDIF
					ENDIF
				ELSE
					IF IS_PLAYER_SCTV(PLAYER_ID()) 
            		OR DID_I_JOIN_MISSION_AS_SPECTATOR()
						iPlayerTeam = 0
					ENDIF
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].vStartPos)
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].vStartPos
						PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - on a versus mission. g_FMMC_STRUCT.sFMMCEndConditions[GET_PLAYER_TEAM(PLAYER_ID())].vStartPos = ", g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].vStartPos)
					ELSE
						NET_SCRIPT_ASSERT("Doing post mission cleanup after a race, g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].vStartPos = <<0.0,0.0,0.0>>")
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
						ENDIF
					ENDIF
				ENDIF
			//ELSE
				//PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - player team not setup yet.")
			ENDIF
		BREAK
		
		DEFAULT
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - on a mini game. Setting vAllWarpLoc = player coords.")
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc)
		PRINTLN("[PMC] - SAVE_AFTER_MISSION_SPAWN_LOC - vAllWarpLoc = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC POST_MISSION_CLEANUP(POST_MISSION_CLEANUP_DATA &sData)
	
	// Reset the cleanup if we are in sp and mid-cleanup.
	IF sData.iMaintainStage > 0
	AND sData.iMaintainStage < 3
		IF (GET_CURRENT_GAMEMODE() != GAMEMODE_FM)
			sData.iMaintainStage = 3
			sData.bJumpingToCleanupForSp = TRUE
			PRINTLN("[PMC] - sData.iMaintainStage = 3 call 1.")
			PRINTLN("[PMC] - we are in single player, resetting post mission cleanup.")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bForceThroughCleanup
			SET_FORCE_ME_THROUGH_POST_MISSION_CLEAN_UP()
			bForceThroughCleanup = FALSE
		ENDIF
//		IF sData.iMaintainStage > 1
//			PRINTLN("[PMC] - iMaintainStage = ", sData.iMaintainStage)
//		ENDIF
	#ENDIF
	
	// Main logic.
	SWITCH sData.iMaintainStage
		
		CASE 0
			
			IF IS_LOCAL_PLAYER_TO_BE_FORCED_THROUGH_POST_MISSION_CLEAN_UP()
				PRINTLN("[PMC] - IS_LOCAL_PLAYER_TO_BE_FORCED_THROUGH_POST_MISSION_CLEAN_UP = TRUE, forcing through.")
				sData.bPostActivityCleanupComplete = FALSE
				sData.iMaintainStage++
				PRINTLN("[PMC] - sData.iMaintainStage++ call 0.")
			ENDIF
			
//			PRINTLN("[PMC] - Players last vehicle = ", NATIVE_TO_INT(GET_PLAYERS_LAST_VEHICLE())
			
			IF NOT IS_TRANSITION_SESSION_QUITING_CORONA()
				IF NOT BAIL_MISSION_CLEANUP_CORONA_STATUS_CHECK()
					
					IF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) != FMMC_TYPE_GUN_INTRO
					AND GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) != FMMC_TYPE_GANGHIDEOUT
					AND GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) != FMMC_TYPE_IMPROMPTU_DM
					AND GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) != FMMC_TYPE_RACE_TO_POINT
					AND GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) != FMMC_TYPE_PENNED_IN
					AND GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) != FMMC_TYPE_KILL_LIST
					AND GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) != FMMC_TYPE_GB_BOSS_DEATHMATCH
					AND GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) != FMMC_TYPE_MG_INSIDE_TRACK
						IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
						OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_CleanUp)
							
							sData.bPostActivityCleanupComplete = FALSE
							
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_CleanUp)
								sData.bComingOutOfPlaylist = TRUE
							ENDIF
							
							#IF IS_DEBUG_BUILD
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_CleanUp)
								PRINTLN("[PMC] - IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_CleanUp), going to state ", sData.iMaintainStage)
							ENDIF
							#ENDIF
							CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_CleanUp)
							
							IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
								IF HAS_FM_TRIGGER_TUT_BEEN_DONE()
//									PRINTLN("[PMC] - bPreTutorialMissionInFlow = FALSE, print 1.")
							        sData.bPreTutorialMissionInFlow = FALSE
								ENDIF
								
								IF SAVE_AFTER_MISSION_SPAWN_LOC()
									
									// Save if on a mission.
									sData.bBeenOnStandardHeistOrContactMission = IS_PLAYER_ON_CONTACT_STANDARD_OR_HEIST_MISSION(PLAYER_ID()) // IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION)// Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
									
									INT iMissionType
									iMissionType = -1
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_ARM_WRESTLING)
										PRINTLN("[PMC] - IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_ARM_WRESTLING) = TRUE.")
										iMissionType = FMMC_TYPE_MG_ARM_WRESTLING
									ENDIF
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_DARTS)
										PRINTLN("[PMC] - IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_DARTS) = TRUE.")
										iMissionType = FMMC_TYPE_MG_DARTS
									ENDIF
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_GOLF)
										PRINTLN("[PMC] - IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_GOLF) = TRUE.")
										iMissionType = FMMC_TYPE_MG_GOLF
									ENDIF
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_HUNTING)
										PRINTLN("[PMC] - IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_HUNTING) = TRUE.")
										iMissionType = FMMC_TYPE_MG_HUNTING
									ENDIF
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_PILOT_SCHOOL)
										PRINTLN("[PMC] - IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_PILOT_SCHOOL) = TRUE.")
										iMissionType = FMMC_TYPE_MG_PILOT_SCHOOL
									ENDIF
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_RANGE_COVERED)
										PRINTLN("[PMC] - IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_RANGE_COVERED) = TRUE.")
										iMissionType = FMMC_TYPE_MG_RANGE_COVERED
									ENDIF
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_RANGE_GRID)
										PRINTLN("[PMC] - IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_RANGE_GRID) = TRUE.")
										iMissionType = FMMC_TYPE_MG_RANGE_GRID
									ENDIF
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_RANGE_RANDOM)
										PRINTLN("[PMC] - IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_RANGE_RANDOM) = TRUE.")
										iMissionType = FMMC_TYPE_MG_RANGE_RANDOM
									ENDIF
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_SHOOTING_RANGE)
										PRINTLN("[PMC] - IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_SHOOTING_RANGE) = TRUE.")
										iMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
									ENDIF
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_TENNIS)
										PRINTLN("[PMC] - IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_TENNIS) = TRUE.")
										iMissionType = FMMC_TYPE_MG_TENNIS
									ENDIF
									
									IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_TENNIS_SP)
										PRINTLN("[PMC] - IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_TENNIS_SP) = TRUE.")
										iMissionType = FMMC_TYPE_MG_TENNIS_SP
										PRINTLN("[PMC] - sData.bBeenOnMiniGame = TRUE.")
										sData.bBeenOnMiniGame = TRUE
									ENDIF
									
									IF iMissionType != (-1)
										sData.bBeenOnMiniGame = TRUE
										sData.vMiniGameCoronaLoc = GET_FMMC_MINI_GAME_START_LOCATION(iMissionType, GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].activeMission.mdID.idVariation)
										PRINTLN("[PMC] - sData.bBeenOnMiniGame = TRUE.")
										PRINTLN("[PMC] - sData.vMiniGameCoronaLoc = ", sData.vMiniGameCoronaLoc)
									ENDIF
									
									// Print useful info.
									#IF IS_DEBUG_BUILD
										IF sData.bBeenOnStandardHeistOrContactMission
											PRINTLN("[PMC] - sData.bBeenOnStandardHeistOrContactMission = TRUE.")
										ELSE
											PRINTLN("[PMC] - sData.bBeenOnStandardHeistOrContactMission = FALSE.")
										ENDIF
									#ENDIF
									
									GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vMyPostJobSpawnArea = << 0.0, 0.0, 0.0 >>
									PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set sMagnateGangBossData.vMyPostJobSpawnArea = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vMyPostJobSpawnArea)
									
									sData.iMaintainStage++
									PRINTLN("[PMC] - sData.iMaintainStage++ call 1.")
									PRINTLN("[PMC] - case 0 - player on a mission, going to state ", sData.iMaintainStage)
									
								ENDIF
							ELSE
								GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vMyPostJobSpawnArea = << 0.0, 0.0, 0.0 >>
								PRINTLN("[PMC] - [MAGNATE_GANG_BOSS] - set sMagnateGangBossData.vMyPostJobSpawnArea = ", GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.vMyPostJobSpawnArea)
								sData.iMaintainStage++
								PRINTLN("[PMC] - sData.iMaintainStage++ call 2.")
								//PRINTLN("[PMC] - case 0 - player on a mission, going to state ", sData.iMaintainStage)
							ENDIF
							
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 1
			//The corona is quitting
			IF IS_TRANSITION_SESSION_QUITING_CORONA()			
				sData.iMaintainStage = 3
				PRINTLN("[PMC] - sData.iMaintainStage-- call 0.")
				PRINTLN("[PMC] - case 1 - IS_TRANSITION_SESSION_QUITING_CORONA = TRUE, need to reset, going to state ", sData.iMaintainStage)
			//The corona is not quitting
			ELSE
				#IF IS_DEBUG_BUILD
					IF IS_LOCAL_PLAYER_TO_BE_FORCED_THROUGH_POST_MISSION_CLEAN_UP()
						PRINTLN("[PMC] - IS_LOCAL_PLAYER_TO_BE_FORCED_THROUGH_POST_MISSION_CLEAN_UP = TRUE.")
					ENDIF
				#ENDIF
				IF ( NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE) AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED() )
				OR IS_LOCAL_PLAYER_TO_BE_FORCED_THROUGH_POST_MISSION_CLEAN_UP()
					
					IF NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
					OR IS_LOCAL_PLAYER_TO_BE_FORCED_THROUGH_POST_MISSION_CLEAN_UP()
				
						//If the playlist is not in the process of setting up the leaderboard.
						IF NOT IS_PLAYLIST_LEADERBOARD_BEING_SETTING_UP()
						OR IS_LOCAL_PLAYER_TO_BE_FORCED_THROUGH_POST_MISSION_CLEAN_UP()
							
							HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
							HIDE_HUD_AND_RADAR_THIS_FRAME()
							DISABLE_SELECTOR()
							
							// Set values for variables in intial checks.
							IF NOT sData.bDidInitChecks
								
								// Save if the player was dead or not when the clean up started. Needed to know if the special respawn state should be set and checked against later on.
								IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
									sData.bDeadWhenStartedCleanup = TRUE
									PRINTLN("[PMC] - player dead when cleanup started.")
								ELSE
									sData.bDeadWhenStartedCleanup = FALSE
									PRINTLN("[PMC] - player alive when cleanup started.")
								ENDIF
								
								// Show loading spinner.
								IF NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
									//BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
									//END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_SPINNER))
									PRINTLN("[PMC] - called for busyspinner + label (MP_SPINLOADING).")
								ENDIF
								
								// Turn off player control.
								IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
//									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION )
								
									// Don't want a wanted level.
									CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
								ENDIF

								// Make sure the focus is clear in prep for cleanup.
								CLEAR_FOCUS()
								
								// Stop load scenes.
								NEW_LOAD_SCENE_STOP()
								
								// Stop tool tips during blurry screen.
								SHOW_GAME_TOOL_TIPS(FALSE)
								THEFEED_FLUSH_QUEUE()
								 g_b_ReapplyStickySaveFailedFeed = FALSE
								THEFEED_HIDE_THIS_FRAME()
								
								sData.bDidInitChecks = TRUE
								
							ENDIF
				
							// In case the leaderboard cam hasn't been called.
							IF IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition)
								IF IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vStartedSpecCamLocation)
									IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
										g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
										IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
											g_TransitionSessionNonResetVars.sPostMissionCleanupData.fEndOfJobHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
										ENDIF
										PRINTLN("[PMC] - vEndOfJobPosition was zero when mission script ended, was the leaderboard cam called?using current player coords, vEndOfJobPosition = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition)
									ENDIF
								ELSE
									g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition = g_TransitionSessionNonResetVars.sPostMissionCleanupData.vStartedSpecCamLocation
									PRINTLN("[PMC] - vEndOfJobPosition was zero when mission script ended, player is spectating, taking coords of spectator target, vEndOfJobPosition = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition, ", vStartedSpecCamLocation = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vStartedSpecCamLocation)
								ENDIF
							ENDIF
							
							// Let Neil that we're going to warp into the personal vehicle.
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanupGoingToWarpIntoPv = FALSE
							CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN() // added at the request of Kenneth Ross - in game internet was incorrectly setting it and this was interfering with the cleanup.
							IF NOT g_sFMMCEOM.bDoWarp
								IF NOT sData.bBeenOnStandardHeistOrContactMission
									IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
										IF NOT sData.bBeenOnMiniGame
											g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanupGoingToWarpIntoPv = TRUE
											PRINTLN("[PMC] - bBeenOnStandardHeistOrContactMission, setting bCleanupGoingToWarpIntoPv = TRUE.")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							sData.iMaintainStage++
							PRINTLN("[PMC] - sData.iMaintainStage++ call 3.")
							PRINTLN("[PMC] - case 1 -  player no longer on a mission, going to state ", sData.iMaintainStage)
						ELSE
							PRINTLN("[PMC] - case 1 -  IS_PLAYLIST_LEADERBOARD_BEING_SETTING_UP = TRUE")
						ENDIF
					ELSE
//						PRINTLN("[PMC] - case 1 -  IS_FM_MISSION_LAUNCH_IN_PROGRESS() = TRUE")
					ENDIF
				ELSE
//					PRINTLN("[PMC] - case 1 -  IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE) = TRUE")
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			THEFEED_HIDE_THIS_FRAME()
			
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
			OR sData.bComingOutOfPlaylist
			OR IS_LOCAL_PLAYER_TO_BE_FORCED_THROUGH_POST_MISSION_CLEAN_UP()
				#IF IS_DEBUG_BUILD
					IF IS_LOCAL_PLAYER_TO_BE_FORCED_THROUGH_POST_MISSION_CLEAN_UP()
						PRINTLN("[PMC] - IS_LOCAL_PLAYER_TO_BE_FORCED_THROUGH_POST_MISSION_CLEAN_UP = TRUE.")
					ENDIF
				#ENDIF
				IF NOT IS_PLAYLIST_LEADERBOARD_ACTIVE()
				
					// Destroy our cam if it exists
					IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
						PRINTLN("[PMC] - case 2 -  leaderboard camera exists. Clean up as no longer needed.")
						CLEANUP_LEADERBOARD_CAM(TRUE, TRUE, TRUE)
					ENDIF
					
					IF DO_FMMC_POST_ACTIVITY_CLEANUP(sData)
						CLEAR_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR()
						sData.bPostActivityCleanupComplete = TRUE
						PRINTLN("[PMC] - case 2 -  DO_FMMC_POST_ACTIVITY_CLEANUP returned TRUE, going to state ")
					ENDIF
				ELSE
					
					PRINTLN("[PMC] - waiting for MPGlobals.IS_PLAYLIST_LEADERBOARD_ACTIVE() to return FALSE, currently returning TRUE.")
					
				ENDIF
			ELSE
				IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()) // B* 2187551
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						Clear_All_Generated_MP_Headshots(FALSE)
						IF g_sMagnateOutfitStruct.iBossStyle = ENUM_TO_INT(GB_BOSS_STYLE_NONE)
							SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
						ENDIF
					ENDIF
				ENDIF
				IF IS_LOCAL_PLAYER_TO_BE_FORCED_THROUGH_POST_MISSION_CLEAN_UP()
					CLEAR_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR()
				ENDIF
				sData.bPostActivityCleanupComplete = TRUE
				PRINTLN("[PMC] - case 2 - BbNeedsFmCleanup is FALSE, setting sData.bPostActivityCleanupComplete.")
			ENDIF
			
			NETWORK_SET_CURRENT_PUBLIC_CONTENT_ID("")
			PRINTLN("SUM22 - POST_MISSION_CLEANUP - NETWORK_SET_CURRENT_PUBLIC_CONTENT_ID(\"\")")
			
			IF sData.bPostActivityCleanupComplete
				sData.iMaintainStage++
				PRINTLN("[PMC] - sData.iMaintainStage++ call 4.")
				PRINTLN("[PMC] - sData.iMaintainStage = ", sData.iMaintainStage)
			ENDIF
			
		BREAK
		
		CASE 3
			
			
			IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOkToKillCelebrationScript = TRUE
			ENDIF
			
			
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.sPostMissionTransitionAudioData.bRunPostMatchTransitionScene = FALSE
			PRINTLN("[PMC] - set bRunPostMatchTransitionScene = FALSE. Call 1.")
			
			ENABLE_SELECTOR()
			RESET_CLEANUP_POST_MISSION_DATA(sData.eStage, sData.iMaintainStage, sData.bDeadWhenStartedCleanup, sData.iPostCleanupReadyForPlayAgainSwoopDownStage, sData.bCleanUpReadyToMoveOn, sData.bComingOutOfPlaylist, sData.stLoadSceneTimer, sData.iOnFootWarpStage)
			PRINTLN("[PMC] - reset sData.iMaintainStage call 0.")
			RESET_CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE_DATA()
			g_TransitionSessionNonResetVars.bSavedLastVehBeforeJob = FALSE
			
			IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBlockCleanupOfEndMissionPosData
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition = <<0.0, 0.0, 0.0>>
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.fEndOfJobHeading = 0.0
				PRINTLN("[PMC] - bBlockCleanupOfEndMissionPosData = FALSE, reset vEndOfJobPosition and fEndOfJobHeading.")
			ELSE
				PRINTLN("[PMC] - bBlockCleanupOfEndMissionPosData = TRUE, not resetting vEndOfJobPosition and fEndOfJobHeading.")
			ENDIF
			
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBlockCleanupOfEndMissionPosData = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoSpecificEndOfJobPos = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.vAllWarpLoc = <<0.0, 0.0, 0.0>>
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeenOnStandardHeistOrContactMission = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeenOnTeamNonMissionJob = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iForceIntoFreemodeDeadStage = 0
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iFreemodeDeadMoveOnStage = 0
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeenOnMiniGame = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLbCamClearedLoadsScenesAndFocus = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bGotLoadSceneCoords = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLoadSceneCoords = << 0.0, 0.0, 0.0 >>
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bStartedFirstLoadScene = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bFirstLoadSceneComplete = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDidOnFootWarp = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bJumpingToCleanupForSp = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iCutToPlayerStage = 0
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bWantSwoopCam = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDidWarpToSwitchStartLoc = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bAllWarpLocIsForRace = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.vRaceAllWarpLocSpawnResult = <<0.0,0.0,0.0>>
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze = NULL
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCompletedGeraldMission = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanupGoingToWarpIntoPv = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSkipTransitionCam = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnRandomVehicle = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentLocation = -1
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeginningMissionInApartment = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBlockLoadingTvForHeistCleanup = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.vStartedSpecCamLocation = << 0.0, 0.0, 0.0 >>
			RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.timerApartmentScriptStartTimer)
			RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.timerApartmentLoadBoard)
			RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.waitForPackedIntTimer)
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete = FALSE
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bStreamingRequestsComplete = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent = 0.0
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords = << 0.0, 0.0, 0.0 >>
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnNearEvent = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iWarpForPvAtEvent = 0
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPausePvCreation = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlayBennyPreScene = FALSE
			
			g_bLastMissionWVM = FALSE
			PRINTLN("[PMC] - post mission cleanup cleaning up, setting g_bLastMissionWVM = FALSE so we don't recreate after races, survivals, etc. Call 1.")
			
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bShouldTransitionSessionsRestartPlaylist = FALSE
			
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeachAfterSumoSpawnFlag = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn = FALSE
			PRINTLN("[ARENA] g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn - FALSE")
			
			INT iCounter
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = (-1)
			REPEAT COUNT_OF(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPlayerNameHash) iCounter
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPlayerNameHash[iCounter] = (-1)
			ENDREPEAT
			
			MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bLaunchedHeist = FALSE
			PRINTLN("[overheads] - [PMC] - MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bLaunchedHeist = FALSE")
			SET_TEAM_COLOUR_OVERRIDE_FLAG(FALSE)	
			
			IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = << 0.0, 0.0, 0.0 >>
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius = 0.0
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest = << 0.0, 0.0, 0.0 >>
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bForcePostMissionWarpCreatorPosition = FALSE
			ENDIF
			
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPostMissionPosIgnoreExclusionZones = FALSE
			
			SET_MAINTRANSITION_PROCESSING_AN_SCTV_TRANSITION_INVITE(FALSE)
			
			IF NOT IS_PLAYER_IN_CORONA()
				DISPLAY_RADAR(TRUE) // Fix for 1806324. 
				PRINTLN("[PMC] - IS_PLAYER_IN_CORONA = FALSE. Called DISPLAY_RADAR(TRUE).")
			ENDIF
			
			RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stMinSplashLengthTimer)
			RESET_TEMP_REZZED_ELSEWHERE_DATA_BUG_1717705() // Failsafe timer for bug 1717705. Temp fix until Neil is back and can look into a proper fix. #LimboBug
			
			IF IS_VALID_INTERIOR(g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment)
				PRINTLN("[PMC] - IS_VALID_INTERIOR(set g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment) = TRUE")
				IF IS_INTERIOR_READY(g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment)
					UNPIN_INTERIOR(g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment)
					PRINTLN("[PMC] - UNPIN_INTERIOR(set g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment)")
				ENDIF
			ENDIF
			
			INTERIOR_INSTANCE_INDEX tempInteriorIndex
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = tempInteriorIndex
			PRINTLN("[PMC] - set g_TransitionSessionNonResetVars.sPostMissionCleanupData.interiorApartment = tempInteriorIndex")
			
			IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
			AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
				GAMER_HANDLE tempHandle
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPackedInt  = 0
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.ownerHandle = tempHandle
				SET_POST_MISSION_PLACE_IN_APARTMENT_FLAG(FALSE)
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords = << 0.0, 0.0, 0.0 >>
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentLocation = -1
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum = -1
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyInstance = -1
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyEntrance = -1
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation = -1
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bActivatedCutsomaprtmentIpls = FALSE
				PRINTLN("[PMC] - [SAC] - reset packed int, call 2.")
			ELSE
				IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
					PRINTLN("[PMC] - [SAC] - not resetting apartment data, we're doing a quick restart, need to retain apartment data.")
				ENDIF
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
					PRINTLN("[PMC] - [SAC] - not resetting apartment data, bDoingHeistCelebration = TRUE, need to retain apartment data.")
				ENDIF
			ENDIF
			
			SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
			#IF IS_DEBUG_BUILD
			PRINTLN("[PMC] - called SET_PLAYER_LEAVE_PED_BEHIND(TRUE)")
			DEBUG_PRINTCALLSTACK()
			#ENDIF
			
			CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
			
			SET_LOADING_ICON_INACTIVE()
			
			IF NOT IS_PLAYER_ABOUT_TO_START_TUTORIAL_MISSION()
			OR g_bFailedTutorialMission
				SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)
//			
//				IF HAS_SCALEFORM_MOVIE_LOADED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
//					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
//				ENDIF
			ELSE
				PRINTLN("[PMC] - Not turning off splash screen - 3")
			ENDIF
			
			SET_REDO_NEWS_HUD_DUE_TO_EVENT()
			
			// AMEC HEISTS
			IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART() 
			AND NOT GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD()
				CLEAN_HEIST_FINALE_ALL(FALSE)
				CLEAN_HEIST_FINALE_GLOBAL_ALL()
				CLEAN_ALL_HEIST_PRE_PLANNING()
				CLEAN_ALL_HEIST_STRAND_PLANNING()
				//CLEAN_HEIST_SHARED_DATA()	// Removed (with Alastair) for asserts seen as we leave Freemode / Launcher
			ENDIF	
			
			PRINTLN("[PMC] - case 3 -  RESET_CLEANUP_POST_MISSION_DATA has been called, going to state ", sData.iMaintainStage)
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

