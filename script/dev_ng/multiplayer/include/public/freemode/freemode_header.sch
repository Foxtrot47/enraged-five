USING "net_prints.sch"
USING "net_script_tunables.sch"		// KGM 25/7/12 - added so that Refresh_MP_Script_Tunables() can be called on return to Freemode play after a mission
USING "FM_Relationships.sch"
USING "Net_Entity_Icons.sch"
USING "net_betting_client.sch"
USING "commands_brains.sch"
USING "net_common_functions.sch"
USING "net_mission_info.sch"
USING "fm_playlist_header.sch"
//USING "net_buddies.sch"
USING "website_public.sch"
USING "FM_Unlocks_Header_Public.sch"
USING "net_transition_sessions.sch"
USING "commands_money.sch"
USING "achievement_public.sch"
USING "stripclub_public.sch"
USING "net_saved_vehicles.sch"
USING "net_ambience.sch"
USING "context_control_public.sch"
USING "Cheat_Handler.sch"
USING "net_missions_at_coords_public_checks.sch"	// Keith 27/4/13: Used to Inform Shared Mission routines that a mission is being played again
													// Keith 29/5/13: Used to Inform MissionsAtCoords that a 'random' mission needs temporarily unlocked
USING "FMMC_SCTV_TICKER.sch"
USING "net_cash_transactions.sch"
USING "net_mental_state.sch"
USING "net_doors.sch"
USING "net_heists_hardcoded.sch"
USING "Net_Heists_public.sch"

//USING "freemode_events_header_START_END.sch"

PROC CLEANUP_KILL_STRIP_OVER_RIDE_PLAYER_ID()
	PRINTLN("[JA@KILLSTRIP] CLEANUP_KILL_STRIP_OVER_RIDE_PLAYER_ID") 
	MPGlobals.g_KillStrip.piOverRideKillerPlayer = INVALID_PLAYER_INDEX()
ENDPROC

#IF IS_DEBUG_BUILD

PROC CLEAR_DEBUG_FILE_REWARD_OUTPUT()
	string sFilePath	= "X:\\gta5\\titleupdate\\dev_ng\\"
	TEXT_LABEL_31 sFileName
	INT I
	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
		sFileName	= "Player_Last_Reward_"
		sFileName += I
		sFileName += ".txt"
		CLEAR_NAMED_DEBUG_FILE(sFilePath, sFileName)
	ENDFOR
ENDPROC

PROC DEBUG_FILE_REWARD_OUTPUT()
	
	string sFilePath	= "X:\\gta5\\titleupdate\\dev_ng\\"
	TEXT_LABEL_31 sFileName	= "Player_Last_Reward_"
	sFileName += NATIVE_TO_INT(PLAYER_ID())
	sFileName += ".txt"

	


	OPEN_NAMED_DEBUG_FILE(sFilePath, sFileName)
	

	SAVE_STRING_TO_NAMED_DEBUG_FILE("{\"JobName\":\"", sFilePath, sFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(g_missionFile.JobName, sFilePath, sFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", sFilePath, sFileName)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"ProfileName\":\"", sFilePath, sFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_PLAYER_NAME(PLAYER_ID()), sFilePath, sFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", sFilePath, sFileName)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"ContentID\":\"", sFilePath, sFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(g_FMMC_STRUCT.tl31LoadedContentID, sFilePath, sFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", sFilePath, sFileName)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"TypeOfJob\":\"", sFilePath, sFileName)
	SWITCH g_FMMC_STRUCT.iMissionType
		CASE FMMC_TYPE_MISSION			SAVE_STRING_TO_NAMED_DEBUG_FILE("Mission", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_DEATHMATCH  		SAVE_STRING_TO_NAMED_DEBUG_FILE("Deathmatch", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_RACE  			SAVE_STRING_TO_NAMED_DEBUG_FILE("Race", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_BASE_JUMP  		SAVE_STRING_TO_NAMED_DEBUG_FILE("Parachute", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_SURVIVAL  		SAVE_STRING_TO_NAMED_DEBUG_FILE("Survival", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_MISSION_CTF  	SAVE_STRING_TO_NAMED_DEBUG_FILE("Capture", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_IMPROMPTU_DM  	SAVE_STRING_TO_NAMED_DEBUG_FILE("Impromptu DM", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_GANGHIDEOUT  	SAVE_STRING_TO_NAMED_DEBUG_FILE("GangHideout", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_MISSION_COOP		SAVE_STRING_TO_NAMED_DEBUG_FILE("Coop", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_MISSION_LTS 		SAVE_STRING_TO_NAMED_DEBUG_FILE("LTS", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_MISSION_CONTACT  SAVE_STRING_TO_NAMED_DEBUG_FILE("Contact", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_MISSION_VS  		SAVE_STRING_TO_NAMED_DEBUG_FILE("Versus", sFilePath, sFileName) BREAK
		CASE FMMC_TYPE_MISSION_NEW_VS  	SAVE_STRING_TO_NAMED_DEBUG_FILE("NewVS", sFilePath, sFileName)	 BREAK
		CASE FMMC_TYPE_MG_PILOT_SCHOOL_FOR_LOOP  	SAVE_STRING_TO_NAMED_DEBUG_FILE("PilotSchool", sFilePath, sFileName)	 BREAK
	
		CASE FMMC_TYPE_HEIST_INTRO
		CASE FMMC_TYPE_HEIST_PHONE_INVITE
		CASE FMMC_TYPE_HEIST_PHONE_INVITE_2
		CASE FMMC_TYPE_HEIST_TEXT_INTRO
			SAVE_STRING_TO_NAMED_DEBUG_FILE("Heist", sFilePath, sFileName)
		BREAK
		
	ENDSWITCH
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_ComeOutToPlay)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(":ComeOutToPlay", sFilePath, sFileName)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_HEIST_HIGH_END)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(":HeistHighEnd", sFilePath, sFileName)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(":SiegeMentality", sFilePath, sFileName)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_CrossTheLine)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(":CrossTheLine", sFilePath, sFileName)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(":HastaLaVista", sFilePath, sFileName)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HuntingPack)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(":HuntingPack", sFilePath, sFileName)
	ENDIF
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", sFilePath, sFileName)



	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"TotalPlayers\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.TotalPlayers, sFilePath, sFileName) //!!!!!
	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"Time\":", sFilePath, sFileName)
	INT TimeinSeconds = FLOOR(TO_FLOAT(g_missionFile.Time)/1000.0)
	SAVE_INT_TO_NAMED_DEBUG_FILE(TimeinSeconds, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"PlayerRank\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.PlayerRank, sFilePath, sFileName) 
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"Checkpoints\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.Checkpoints, sFilePath, sFileName) 
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"Dnf\":", sFilePath, sFileName)
	SAVE_BOOL_TO_NAMED_DEBUG_FILE(g_missionFile.Dnf, sFilePath, sFileName) //!!!!!

	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"CheckpointsPassed\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.CheckpointsPassed, sFilePath, sFileName) //!!!!!

	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"FinalPosition\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.FinalPosition, sFilePath, sFileName)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"TeamGame\":", sFilePath, sFileName)
	SAVE_BOOL_TO_NAMED_DEBUG_FILE(g_missionFile.TeamGame, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"KillTarget\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.KillTarget, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"TeamPlace\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.TeamPlace, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"LastWaveCompleted\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.LastWaveCompleted, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"Difficulty\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.Difficulty, sFilePath, sFileName) 
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"PassFail\":", sFilePath, sFileName)
	SAVE_BOOL_TO_NAMED_DEBUG_FILE(g_missionFile.PassFail, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"MissionRank\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.MissionRank, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"TripSkipped\":", sFilePath, sFileName)
	SAVE_BOOL_TO_NAMED_DEBUG_FILE(g_missionFile.TripSkipped, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"HeistStage\":", sFilePath, sFileName)
	SAVE_BOOL_TO_NAMED_DEBUG_FILE(g_missionFile.HeistStage, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"HeistLeader\":", sFilePath, sFileName)
	SAVE_BOOL_TO_NAMED_DEBUG_FILE(g_missionFile.HeistLeader, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"Medal\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.Medal, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"TimesAwarded\":", sFilePath, sFileName)
	SAVE_BOOL_TO_NAMED_DEBUG_FILE(g_missionFile.TimesAwarded, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"HighestMedal\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.HighestMedal, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"VehicleType\":\"", sFilePath, sFileName)
	IF g_missionFile.VehicleType != DUMMY_MODEL_FOR_SCRIPT
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(g_missionFile.VehicleType), sFilePath, sFileName)
	ELSE
		SAVE_STRING_TO_NAMED_DEBUG_FILE("DUMMY_MODEL_FOR_SCRIPT", sFilePath, sFileName)
	ENDIF
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", sFilePath, sFileName)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"ImportExport\":", sFilePath, sFileName)
	SAVE_BOOL_TO_NAMED_DEBUG_FILE(g_missionFile.ImportExport, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"ActualCashAwarded\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.ActualCashAwarded, sFilePath, sFileName) //!!!!!

	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"ActualRPAwarded\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.ActualRPAwarded, sFilePath, sFileName) //!!!!!

	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"AverageRank\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.AverageRank, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"SumEntryFees\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.SumEntryFees, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"JobFinished\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.JobFinished_Posix, sFilePath, sFileName)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"PointsParachute\":", sFilePath, sFileName)
	SAVE_INT_TO_NAMED_DEBUG_FILE(g_missionFile.PointsParachute, sFilePath, sFileName) //!!!!!
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"IsCrewMember\":", sFilePath, sFileName)
	SAVE_BOOL_TO_NAMED_DEBUG_FILE(g_missionFile.IsCrewMember, sFilePath, sFileName) //!!!!!

	SAVE_STRING_TO_NAMED_DEBUG_FILE(",\"IsFreeAim\":", sFilePath, sFileName)
	SAVE_BOOL_TO_NAMED_DEBUG_FILE(g_missionFile.bIsFreeAim, sFilePath, sFileName) //!!!!!


	SAVE_STRING_TO_NAMED_DEBUG_FILE("},", sFilePath, sFileName)
	CLOSE_DEBUG_FILE()
	
	g_missionFile = g_missionFileEMPTY
	g_missionFile_ExternalTracker = g_missionFileEMPTY
	

ENDPROC




PROC FILL_DEBUG_OUTPUT_FILE_STRUCT(TEXT_LABEL_63 JobName,
							STRING TypeOfJob,
							TEXT_LABEL_23 ContentID,
							INT TotalPlayers = 0,
							INT Time = 0,
							INT PlayerRank = 0,
							INT Checkpoints = 0, 
							BOOL Dnf = FALSE,
							INT CheckpointsPassed = 0,
							INT FinalPosition = 0, 
							BOOL TeamGame = FALSE,
							INT KillTarget = 0,
							INT TeamPlace = 0, 
							INT LastWaveCompleted = 0,
							INT Difficulty = 0,
							BOOL PassFail = FALSE,
							INT MissionRank = 0,
							BOOL TripSkipped = FALSE,
							BOOL HeistStage = FALSE,
							BOOL HeistLeader = FALSE,
							INT Medal = 0,
							BOOL TimesAwarded = FALSE,
							INT HighestMedal = 0,
							MODEL_NAMES VehicleType = DUMMY_MODEL_FOR_SCRIPT,
							BOOL ImportExport	 = FALSE,
							INT ActualCashAwarded = 0,
							INT ActualRPAwarded = 0,
							INT AverageRank = 0,
							INT SumEntryFees = 0,
							INT JobFinished_Posix = 0,
							INT PointsParachute = 0,
							BOOL IsCrewMember = FALSE,
							BOOL DestroyLastPlace = FALSE)
							
							
							
	BOOL IsFreeAim = (GET_LOCAL_PLAYER_AIM_STATE() = ENUM_TO_INT(TARGETING_OPTION_FREEAIM))						

	DEBUG_PRINTCALLSTACK()
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: JobName = ", JobName, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: TypeOfJob = ", TypeOfJob, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: ContentID = ", ContentID, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: TotalPlayers = ", TotalPlayers, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: Time = ", Time, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: PlayerRank = ", PlayerRank, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: Checkpoints = ", Checkpoints, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: Dnf = ", Dnf, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: CheckpointsPassed = ", CheckpointsPassed, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: FinalPosition = ", FinalPosition, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: TeamGame = ", TeamGame, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: KillTarget = ", KillTarget, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: TeamPlace = ", TeamPlace, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: LastWaveCompleted = ", LastWaveCompleted, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: Difficulty = ", Difficulty, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: PassFail = ", PassFail, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: MissionRank = ", MissionRank, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: TripSkipped = ", TripSkipped, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: HeistStage = ", HeistStage, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: HeistLeader = ", HeistLeader, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: Medal = ", Medal, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: TimesAwarded = ", TimesAwarded, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: HighestMedal = ", HighestMedal, " ")
	IF VehicleType != DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: VehicleType = ", GET_MODEL_NAME_FOR_DEBUG(VehicleType), " ")
	ELSE
		PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: VehicleType = DUMMY_MODEL_FOR_SCRIPT ")
	ENDIF
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: ImportExport = ", ImportExport, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: ActualCashAwarded = ", ActualCashAwarded, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: ActualRPAwarded = ", ActualRPAwarded, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: AverageRank = ", AverageRank, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: SumEntryFees = ", SumEntryFees, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: JobFinished_Posix = ", JobFinished_Posix, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: PointsParachute = ", PointsParachute, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: IsCrewMember = ", IsCrewMember, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: IsFreeAim = ", IsFreeAim, " ")
	PRINTLN("[BCOUTPUT] FILL_DEBUG_OUTPUT_FILE_STRUCT: bDestroyLastPlace = ", DestroyLastPlace, " ")



	g_missionFile.JobName = JobName
	g_missionFile.ContentID = ContentID
	g_missionFile.TypeOfJob = TypeOfJob
	g_missionFile.TotalPlayers = TotalPlayers
	g_missionFile.Time = Time
	g_missionFile.PlayerRank = PlayerRank
	g_missionFile.Checkpoints = Checkpoints
	g_missionFile.Dnf = Dnf
	g_missionFile.CheckpointsPassed = CheckpointsPassed
	g_missionFile.FinalPosition = FinalPosition
	g_missionFile.TeamGame = TeamGame
	g_missionFile.KillTarget = KillTarget
	g_missionFile.TeamPlace = TeamPlace
	g_missionFile.LastWaveCompleted = LastWaveCompleted
	g_missionFile.Difficulty = Difficulty
	g_missionFile.PassFail = PassFail
	g_missionFile.MissionRank = MissionRank
	g_missionFile.TripSkipped = TripSkipped
	g_missionFile.HeistStage = HeistStage
	g_missionFile.HeistLeader = HeistLeader
	g_missionFile.Medal = Medal
	g_missionFile.TimesAwarded = TimesAwarded
	g_missionFile.HighestMedal = HighestMedal
	g_missionFile.VehicleType = VehicleType
	g_missionFile.ImportExport = ImportExport
	g_missionFile.ActualCashAwarded = ActualCashAwarded
	g_missionFile.ActualRPAwarded = ActualRPAwarded
	g_missionFile.AverageRank = AverageRank
	g_missionFile.SumEntryFees = SumEntryFees
	g_missionFile.JobFinished_Posix = JobFinished_Posix
	g_missionFile.PointsParachute = PointsParachute
	g_missionFile.IsCrewMember = IsCrewMember
	g_missionFile.bIsFreeAim = IsFreeAim
	g_missionFile.bDestroyLastPlace = DestroyLastPlace
	
ENDPROC

#ENDIF

FUNC BOOL IS_PLAYER_USING_CREATOR_ACTIVITY(PLAYER_INDEX playerID)
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("net_apartment_activity")) > 0
		IF NETWORK_IS_SCRIPT_ACTIVE("net_apartment_activity", NATIVE_TO_INT(playerID), TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_USING_RESTRICTED_ACCOUNT()
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		RETURN GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROPERTY_OWNER_USING_RESTRICTED_ACCOUNT(PLAYER_INDEX pOwner)
	
	IF pOwner != INVALID_PLAYER_INDEX()
		IF (GB_IS_GLOBAL_CLIENT_BIT0_SET(pOwner, eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
		OR (DOES_ENTITY_EXIST(GET_PLAYER_PED(pOwner)) AND GB_IS_THIS_PLAYERS_GANG_NAME_BLOCKED(pOwner)))
		AND NOT IS_DURANGO_AND_CAN_VIEW_GAMER_USER_CONTENT(pOwner)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(BOOL BbNeedsFmCleanup)
	
	#IF IS_DEBUG_BUILD
	
	NET_PRINT("SET_CURRENT_FM_MISSION_NEEDS_CLEANUP has been called:")NET_NL()
	
	NET_PRINT("BbNeedsFmCleanup = ")
	IF BbNeedsFmCleanup
	NET_PRINT(" TRUE.")
	ELSE
	NET_PRINT(" FALSE.")
	ENDIF
	NET_NL()
	
	NET_PRINT("IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup) ")
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
	NET_PRINT(" TRUE.")
	ELSE
	NET_PRINT(" FALSE.")
	ENDIF
	NET_NL()
	
	DEBUG_PRINTCALLSTACK()
	
	#ENDIF
	
	IF BbNeedsFmCleanup
	
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].BbNeedsFmCleanup = TRUE
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
			NET_PRINT("GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].BbNeedsFmCleanup changed to TRUE.")NET_NL()
		ENDIF
		
	ELSE
	
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].BbNeedsFmCleanup = FALSE
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
			NET_PRINT("GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup) changed to FALSE.")NET_NL()
		ENDIF
		
	ENDIF
	
ENDPROC

//Do we need to clean up?
FUNC BOOL DOES_CURRENT_FM_MISSION_NEED_CLEANED_UP()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
ENDFUNC

FUNC STRING GET_MISSION_NAME_FROM_TYPE(INT iMissionType)
	// KGM 12/11/12: FM Missions are being set up to use mission data - this is progressive
	SWITCH iMissionType
		CASE FMMC_TYPE_MG_ARM_WRESTLING		// eFM_ARM_WRESTLING
		CASE FMMC_TYPE_MG_DARTS				// eFM_DARTS
		CASE FMMC_TYPE_GANGHIDEOUT			// eFM_GANG_ATTACK_CLOUD
		CASE FMMC_TYPE_MG_GOLF				// eFM_GOLF
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	// eFM_SHOOTING_RANGE
		CASE FMMC_TYPE_MG_TENNIS			// eFM_TENNIS
		CASE FMMC_TYPE_BASE_JUMP			// eFM_BASEJUMP_CLOUD
		CASE FMMC_TYPE_DEATHMATCH			// eFM_DEATHMATCH_CLOUD
		CASE FMMC_TYPE_SURVIVAL				// eFM_SURVIVAL_CLOUD
		CASE FMMC_TYPE_MISSION				// eFM_MISSION_CLOUD
		CASE FMMC_TYPE_RACE					// eFM_RACE_CLOUD
		CASE FMMC_TYPE_MG_PILOT_SCHOOL		// eFM_PILOT_SCHOOL
			RETURN (GET_MP_MISSION_NAME(Convert_FM_Mission_Type_To_MissionID(iMissionType)))
	ENDSWITCH
	
	// FM Missions that don't use Mission Data (yet)
	SWITCH iMissionType
		CASE FMMC_TYPE_INVALID						RETURN ""
//		CASE FMMC_TYPE_MISSION  					RETURN "FM_Mission_Controller"  			// Obsolete 7/1/13:   eFM_MISSION_CLOUD
		
		CASE FMMC_TYPE_TEST_MISSION  
		CASE FMMC_TYPE_MISSION_CREATOR
			IF IS_FAKE_MULTIPLAYER_MODE_SET()
				RETURN "FM_Mission_Creator_test" 
			ELSE
				RETURN "FM_Mission_Creator"
			ENDIF
		BREAK
		CASE FMMC_TYPE_GUN_INTRO					RETURN "FM_Intro"
		CASE FMMC_TYPE_IMPORT_EXPORT				RETURN "AM_IMP_EXP"

		CASE FMMC_TYPE_CINEMA						RETURN "act_cinema"
		CASE FMMC_TYPE_SHOPS_HIGH_END				RETURN "clothes_shop_mp"	
		CASE FMMC_TYPE_SHOPS_MID_END				RETURN "clothes_shop_mp"
		CASE FMMC_TYPE_SHOPS						RETURN "clothes_shop_mp"
		CASE FMMC_TYPE_PROSTITUTES					RETURN "AM_PROSTITUTE"
		CASE FMMC_TYPE_STRIP_CLUB					RETURN "stripclub_mp"
		CASE FMMC_TYPE_HOLD_UPS						RETURN "AM_HOLD_UP"
		CASE FMMC_TYPE_CRATE_DROP					RETURN "AM_CRATE_DROP"
		CASE FMMC_TYPE_RACE_TO_POINT				RETURN "MG_RACE_TO_POINT"
		CASE FMMC_TYPE_BOUNTIES						RETURN "Net_bounty"
		CASE FMMC_TYPE_COLLECTABLES					RETURN "FM_weapon_pickups"
		CASE FMMC_TYPE_APARTMENT					RETURN "Apartment Invite"
		CASE FMMC_TYPE_SIMPLE_INTERIOR				RETURN "Invite to Business"
	ENDSWITCH
	PRINTLN("iMissionType = ", iMissionType)
	SCRIPT_ASSERT("GET_MISSION_NAME_FROM_TYPE INVALID, TYPE")
	RETURN ""
	
ENDFUNC


PROC GET_UNIQUE_MATCHID_FOR_MINIGAME(TEXT_LABEL_31 &tl31, INT matchType, INT iVariation = 0)
	SWITCH matchType		
		CASE FMMC_TYPE_MG_GOLF	
			tl31 = "MG_GOLF_"
		BREAK
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	
			tl31 = "MG_RANGE_"
		BREAK
		CASE FMMC_TYPE_MG_TENNIS		
			tl31 = "MG_TENNIS_"
		BREAK
		CASE FMMC_TYPE_MG_ARM_WRESTLING		
			tl31 = "MG_ARM_"
		BREAK
		CASE FMMC_TYPE_MG_DARTS		
			tl31 = "MG_DARTS_"
		BREAK	
		CASE FMMC_TYPE_MG_PILOT_SCHOOL
			tl31 = "MG_PILOT_"
		BREAK		
		CASE FMMC_TYPE_IMPROMPTU_DM
			tl31 = "IMPROMPTU_DM"
			EXIT
		BREAK
		
		CASE FMMC_TYPE_GB_BOSS_DEATHMATCH
			tl31 = "IMPROMPTU_DM"
			EXIT
		BREAK
	ENDSWITCH
	tl31 += iVariation
ENDPROC

PROC GET_UNIQUE_MATCHID_FOR_AMBIANT(TEXT_LABEL_31 &tl31, INT matchType, INT iVariation = 0)
	SWITCH matchType
		CASE FMMC_TYPE_RACE_TO_POINT			
			tl31 = "RACE_TO_POINT_"
		BREAK
		CASE FMMC_TYPE_PROSTITUTES			
			tl31 = "PROSTITUTES_"
		BREAK
		CASE FMMC_TYPE_IMPORT_EXPORT			
			tl31 = "IMPORT_EXPORT_"
		BREAK

		CASE FMMC_TYPE_HOLD_UPS			
			tl31 = "HOLD_UPS_"
		BREAK
		CASE FMMC_TYPE_CRATE_DROP			
			tl31 = "CREATE_DROP_"
		BREAK
	ENDSWITCH
	tl31 += iVariation
ENDPROC

FUNC BOOL CAN_CURRENT_MISSION_BE_PLAYED_SOLO()
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
	OR IS_THIS_A_TUNER_ROBBERY_FINALE()
	OR IS_THIS_A_FIXER_STORY_MISSION()
	OR IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
	#IF FEATURE_DLC_2_2022
	OR IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
	#ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SKIP_SOLO_CHECK()

	#IF IS_DEBUG_BUILD
	
		IF g_b_IgnoreSoloChecksForCheating
			PRINTLN("[BCCHEAT] SHOULD_SKIP_SOLO_CHECK = TRUE, g_b_IgnoreSoloChecksForCheating ")
			RETURN TRUE
		ENDIF
		
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_IgnoreSoloCheatCheck")
			PRINTLN("[BCCHEAT] SHOULD_SKIP_SOLO_CHECK = TRUE, sc_IgnoreSoloCheatCheck ")
			RETURN TRUE
		ENDIF
		
	#ENDIF
	
	IF g_iNumberOfPlayersOnThisActivity = 1 
	AND (IS_THIS_A_FIXER_SHORT_TRIP()
	OR CAN_CURRENT_MISSION_BE_PLAYED_SOLO())
		PRINTLN("[BCCHEAT] SHOULD_SKIP_SOLO_CHECK = TRUE, g_iNumberOfPlayersOnThisActivity, IS_PLAYER_PLAYING_OR_PLANNING_HEIST ")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT INCREMENT_STARTING_ACTIVITY_CHEAT_STAT_BY(BOOL bSkipTransitionSessionCheck = FALSE)

	INT IncrementBy = 2 
	
	BOOL SkipSoloCheck = SHOULD_SKIP_SOLO_CHECK()

	IF g_iNumberOfPlayersOnThisActivity = 1
	AND SkipSoloCheck = FALSE
	
		IncrementBy = 0
	ENDIF

	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		IncrementBy = 0
	ENDIF
	
	IF bSkipTransitionSessionCheck = FALSE
		IF NOT IS_THIS_A_MINI_GAME_TRANSITION_SESSION()
			IF g_FMMC_STRUCT.bMissionIsPublished = FALSE
				IncrementBy = 0
			ENDIF
		ENDIF
	ENDIF

	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IncrementBy = 0
	ENDIF
	
	IF IS_LOCAL_PLAYER_SPECTATOR()
		IncrementBy = 0
	ENDIF
	
	
	g_b_ProcessedStartingCheatTracker = TRUE

	
	#IF IS_DEBUG_BUILD
	

		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT("<<<[BCCHEAT] INCREMENT_STARTING_ACTIVITY_CHEAT_STAT_BY = ")NET_PRINT_INT(IncrementBy)
		NET_NL()NET_PRINT("		- g_iNumberOfPlayersOnThisActivity = ")NET_PRINT_INT(g_iNumberOfPlayersOnThisActivity)
		NET_NL()NET_PRINT("		- IS_FAKE_MULTIPLAYER_MODE_SET = ")NET_PRINT_BOOL(IS_FAKE_MULTIPLAYER_MODE_SET())
		NET_NL()NET_PRINT("		- g_b_IgnoreSoloChecksForCheating = ")NET_PRINT_BOOL(g_b_IgnoreSoloChecksForCheating)
		NET_NL()NET_PRINT("		- g_FMMC_STRUCT.bMissionIsPublished = ")NET_PRINT_BOOL(g_FMMC_STRUCT.bMissionIsPublished)
		NET_NL()NET_PRINT("		- IS_PLAYER_SCTV(PLAYER_ID()) = ")NET_PRINT_BOOL(IS_PLAYER_SCTV(PLAYER_ID()))		
		NET_NL()NET_PRINT("		- IS_LOCAL_PLAYER_SPECTATOR() = ")NET_PRINT_BOOL(IS_LOCAL_PLAYER_SPECTATOR())	
		NET_NL()NET_PRINT("		- IS_THIS_A_MINI_GAME_TRANSITION_SESSION() = ")NET_PRINT_BOOL(IS_THIS_A_MINI_GAME_TRANSITION_SESSION())		
		NET_NL()NET_PRINT("		- IS_MINIGAME_IN_PROGRESS() = ")NET_PRINT_BOOL(IS_MINIGAME_IN_PROGRESS())	
		NET_NL()NET_PRINT("		- IS_MP_HEIST_PRE_PLANNING_ACTIVE() = ")NET_PRINT_BOOL(IS_MP_HEIST_PRE_PLANNING_ACTIVE())	
		NET_NL()NET_PRINT("		- IS_MP_HEIST_PLANNING_ACTIVE() = ")NET_PRINT_BOOL(IS_MP_HEIST_PLANNING_ACTIVE())	
		NET_NL()NET_PRINT("		- IS_MP_HEIST_PRE_PLANNING_GUEST_MODE_ACTIVE() = ")NET_PRINT_BOOL(IS_MP_HEIST_PRE_PLANNING_GUEST_MODE_ACTIVE())	
		NET_NL()NET_PRINT("		- IS_MP_HEIST_PRE_PLANNING_CURRENTLY_IN_USE(TRUE) = ")NET_PRINT_BOOL(IS_MP_HEIST_PRE_PLANNING_CURRENTLY_IN_USE(TRUE))
		NET_NL()NET_PRINT("		- Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()) = ")NET_PRINT_BOOL(Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))		
		NET_NL()NET_PRINT("		- Is_Player_Currently_On_MP_Heist(PLAYER_ID()) = ")NET_PRINT_BOOL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))		
		NET_NL()NET_PRINT("		- bSkipTransitionSessionCheck = ")NET_PRINT_BOOL(bSkipTransitionSessionCheck )		
		NET_NL()NET_PRINT("		- g_B_DidEveryoneQuitAndLeaveYouOnActivity = ")NET_PRINT_BOOL(g_B_DidEveryoneQuitAndLeaveYouOnActivity)		
	
		NET_NL()NET_PRINT("[BCCHEAT] ***- IncrementBy = ")NET_PRINT_INT(IncrementBy)	
		NET_PRINT(" >>>> ")
	#ENDIF

	RETURN IncrementBy
ENDFUNC

FUNC INT INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY(BOOL bSkipTransitionSessionCheck = FALSE)
	
	INT IncrementBy = 2 
	
	BOOL SkipSoloCheck = SHOULD_SKIP_SOLO_CHECK()
	
	IF g_b_ProcessedStartingCheatTracker = FALSE
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT("<<<[BCCHEAT] INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY Skipped due to Start not being hit. ")
		#ENDIF
		RETURN 0
	ENDIF
	
	IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
		IF NETWORK_IS_ACTIVITY_SESSION()
		OR (FM_EVENT_IS_PLAYER_LEAVING_EVENT_SEVERE() OR FM_EVENT_IS_PLAYER_LEAVING_EVENT_SUPER_SEVERE())
			NET_NL()NET_PRINT("[BCCHEAT] INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY and quit through the pause menu - g_b_QuitActivityThroughPauseMenu = TRUE ")
			g_b_QuitActivityThroughPauseMenu = TRUE
		ENDIF
	ENDIF
	
	
	
	
	
	g_b_ProcessedStartingCheatTracker  = FALSE
	

	IF g_HasCancelledJobOnPhone
		IncrementBy = 1
	ENDIF
	
	IF g_b_QuitActivityThroughPauseMenu
		IncrementBy = 1
	ENDIF
	
	IF g_b_QuitActivityThroughSelector
		IncrementBy = 1
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(g_sGolfGlobals.GlobalGolfControlFlag, GGCF_PLAYER_QUIT_MP_GOLF)
		IncrementBy = 1
	ENDIF
	
	IF bMinigamePlayerQuitFlag
		IncrementBy = 1
	ENDIF
	
	IF IS_TENNIS_GLOBAL_FLAG_SET(TGF_QUIT_FROM_TENNIS_MP)
		IncrementBy = 1
	ENDIF
	
	IF IS_SHOOTING_RANGE_UNSAVED_BITFLAG_SET(SRB_QuitFromRange)
		IncrementBy = 1
	ENDIF
	
	IF g_iNumberOfPlayersOnThisActivity = 1
	AND SkipSoloCheck = FALSE
		IncrementBy = 0
	ENDIF

	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		IncrementBy = 0
	ENDIF
	
	IF g_B_DidEveryoneQuitAndLeaveYouOnActivity
		IncrementBy = 2
	ENDIF
	
	IF bSkipTransitionSessionCheck = FALSE
		IF NOT IS_THIS_A_MINI_GAME_TRANSITION_SESSION()
			IF g_FMMC_STRUCT.bMissionIsPublished = FALSE
				IncrementBy = 0
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IncrementBy = 0
	ENDIF
	
	IF IS_LOCAL_PLAYER_SPECTATOR()
		IncrementBy = 0
	ENDIF
	
	IF g_b_QuitActivityThroughPauseMenu OR g_b_QuitActivityThroughSelector OR g_HasCancelledJobOnPhone
		IF FM_EVENT_IS_PLAYER_LEAVING_EVENT_SUPER_SEVERE()
			NET_NL()NET_PRINT("[BCCHEAT] INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY and quit the event but during a super severe point - g_b_QuitActivityThroughPauseMenu = TRUE ")
			IncrementBy = 0
		ENDIF
	ENDIF
	
	IF g_bDeathmatchExemptFromBadsport
		PRINTLN("[BADSPORT_EXEMPT][BCCHEAT] INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY - Low ranked job")
		IncrementBy = 2
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT("<<<[BCCHEAT] INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY = ")NET_PRINT_INT(IncrementBy)
		NET_NL()NET_PRINT("		- g_iNumberOfPlayersOnThisActivity = ")NET_PRINT_INT(g_iNumberOfPlayersOnThisActivity)
		NET_NL()NET_PRINT("		- IS_FAKE_MULTIPLAYER_MODE_SET = ")NET_PRINT_BOOL(IS_FAKE_MULTIPLAYER_MODE_SET())
		NET_NL()NET_PRINT("		- g_HasCancelledJobOnPhone = ")NET_PRINT_BOOL(g_HasCancelledJobOnPhone)
		NET_NL()NET_PRINT("		- IS_SHOOTING_RANGE_UNSAVED_BITFLAG_SET(SRB_QuitFromRange) = ")NET_PRINT_BOOL(IS_SHOOTING_RANGE_UNSAVED_BITFLAG_SET(SRB_QuitFromRange))
		NET_NL()NET_PRINT("		- IS_TENNIS_GLOBAL_FLAG_SET(TGF_QUIT_FROM_TENNIS_MP) = ")NET_PRINT_BOOL(IS_TENNIS_GLOBAL_FLAG_SET(TGF_QUIT_FROM_TENNIS_MP))
		NET_NL()NET_PRINT("		- bMinigamePlayerQuitFlag = ")NET_PRINT_BOOL(bMinigamePlayerQuitFlag)
		NET_NL()NET_PRINT("		- IS_BITMASK_AS_ENUM_SET(g_sGolfGlobals.GlobalGolfControlFlag, GGCF_PLAYER_QUIT_MP_GOLF) = ")NET_PRINT_BOOL(IS_BITMASK_AS_ENUM_SET(g_sGolfGlobals.GlobalGolfControlFlag, GGCF_PLAYER_QUIT_MP_GOLF))
		NET_NL()NET_PRINT("		- g_b_IgnoreSoloChecksForCheating = ")NET_PRINT_BOOL(g_b_IgnoreSoloChecksForCheating)
		NET_NL()NET_PRINT("		- g_b_QuitActivityThroughPauseMenu = ")NET_PRINT_BOOL(g_b_QuitActivityThroughPauseMenu)
		NET_NL()NET_PRINT("		- g_b_QuitActivityThroughSelector = ")NET_PRINT_BOOL(g_b_QuitActivityThroughSelector)		
		NET_NL()NET_PRINT("		- g_B_DidEveryoneQuitAndLeaveYouOnActivity = ")NET_PRINT_BOOL(g_B_DidEveryoneQuitAndLeaveYouOnActivity)		
		NET_NL()NET_PRINT("		- g_FMMC_STRUCT.bMissionIsPublished = ")NET_PRINT_BOOL(g_FMMC_STRUCT.bMissionIsPublished)		
		NET_NL()NET_PRINT("		- IS_PLAYER_SCTV(PLAYER_ID()) = ")NET_PRINT_BOOL(IS_PLAYER_SCTV(PLAYER_ID()))		
		NET_NL()NET_PRINT("		- IS_LOCAL_PLAYER_SPECTATOR() = ")NET_PRINT_BOOL(IS_LOCAL_PLAYER_SPECTATOR())	
		NET_NL()NET_PRINT("		- IS_THIS_A_MINI_GAME_TRANSITION_SESSION() = ")NET_PRINT_BOOL(IS_THIS_A_MINI_GAME_TRANSITION_SESSION())		
		NET_NL()NET_PRINT("		- IS_MINIGAME_IN_PROGRESS() = ")NET_PRINT_BOOL(IS_MINIGAME_IN_PROGRESS())	
		NET_NL()NET_PRINT("		- IS_MP_HEIST_PRE_PLANNING_ACTIVE() = ")NET_PRINT_BOOL(IS_MP_HEIST_PRE_PLANNING_ACTIVE())	
		NET_NL()NET_PRINT("		- IS_MP_HEIST_PLANNING_ACTIVE() = ")NET_PRINT_BOOL(IS_MP_HEIST_PLANNING_ACTIVE())	
		NET_NL()NET_PRINT("		- IS_MP_HEIST_PRE_PLANNING_GUEST_MODE_ACTIVE() = ")NET_PRINT_BOOL(IS_MP_HEIST_PRE_PLANNING_GUEST_MODE_ACTIVE())	
		NET_NL()NET_PRINT("		- IS_MP_HEIST_PRE_PLANNING_CURRENTLY_IN_USE(TRUE) = ")NET_PRINT_BOOL(IS_MP_HEIST_PRE_PLANNING_CURRENTLY_IN_USE(TRUE))		
		NET_NL()NET_PRINT("		- Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()) = ")NET_PRINT_BOOL(Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))		
		NET_NL()NET_PRINT("		- Is_Player_Currently_On_MP_Heist(PLAYER_ID()) = ")NET_PRINT_BOOL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))		
		
		NET_NL()NET_PRINT("		- FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT = ")NET_PRINT_BOOL(FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID()) )		
		NET_NL()NET_PRINT("		- FM_EVENT_IS_PLAYER_LEAVING_EVENT_SUPER_SEVERE = ")NET_PRINT_BOOL(FM_EVENT_IS_PLAYER_LEAVING_EVENT_SUPER_SEVERE() )		
		NET_NL()NET_PRINT("		- FM_EVENT_IS_PLAYER_LEAVING_EVENT_SEVERE = ")NET_PRINT_BOOL(FM_EVENT_IS_PLAYER_LEAVING_EVENT_SEVERE())		
		NET_NL()NET_PRINT("		- bSkipTransitionSessionCheck = ")NET_PRINT_BOOL(bSkipTransitionSessionCheck )		

		NET_NL()NET_PRINT("[BCCHEAT] - **** IncrementBy = ")NET_PRINT_INT(IncrementBy)
		NET_PRINT(" >>>> ")
	#ENDIF
	
	g_B_DidEveryoneQuitAndLeaveYouOnActivity = FALSE
	g_b_QuitActivityThroughSelector = FALSE
	g_b_QuitActivityThroughPauseMenu = FALSE
	g_HasCancelledJobOnPhone = FALSE
	bMinigamePlayerQuitFlag = FALSE
	CLEAR_BITMASK_AS_ENUM(g_sGolfGlobals.GlobalGolfControlFlag, GGCF_PLAYER_QUIT_MP_GOLF)
	CLEAR_TENNIS_GLOBAL_FLAG(TGF_QUIT_FROM_TENNIS_MP)
	CLEAR_SHOOTING_RANGE_UNSAVED_BITFLAG(SRB_QuitFromRange) 
	g_bDeathmatchExemptFromBadsport = FALSE
	
	RETURN IncrementBy
ENDFUNC

/// Vehicles from Pegasus


FUNC BOOL IS_BIG_ASS_VEHICLE_SAFE_FOR_PEGASUS(INT iBigAssID)
	IF GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BIG_YACHT) = iBigAssID
		RETURN FALSE
	ENDIF
	
	IF GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BIG_TRUCK) = iBigAssID
		RETURN FALSE
	ENDIF
	IF GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BALLISTIC_EQUIPMENT) = iBigAssID
		RETURN FALSE
	ENDIF
	
	IF GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BIG_PLANE) = iBigAssID
		RETURN FALSE
	ENDIF
	
	IF GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_HACKER_TRUCK) = iBigAssID
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_IH_KOSATKA) = iBigAssID
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC


PROC SET_BIG_ASS_VEHICLE_BS(INT iBigAssID)
	IF iBigAssID != -1
		SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBigAssVehicles.iBigAssVehiclesBS[(iBigAssID/32)], (iBigAssID%32))
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBigAssVehicles.iBAV_timestamp = GET_CLOUD_TIME_AS_INT()
		MP_BIG_ASS_VEHICLES_STORE_DATA_IN_STATS()
		PRINTLN("[BIGASS] SET_BIG_ASS_VEHICLE_BS - iBigAssID = ",iBigAssID)
		PRINT_BIG_ASS_VEHICLE_BS()
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("SET_BIG_ASS_VEHICLE_BS - iBigAssID = -1")
	#ENDIF
	ENDIF
ENDPROC

PROC CLEAR_BIG_ASS_VEHICLE_BS(INT iBigAssID)
	IF iBigAssID != -1
		CLEAR_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBigAssVehicles.iBigAssVehiclesBS[(iBigAssID/32)], (iBigAssID%32))
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("CLEAR_BIG_ASS_VEHICLE_BS - iBigAssID = -1")
	#ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_BIG_ASS_VEHICLE_BS_SET()
	INT i
	REPEAT NUM_BIG_ASS_VEHICLE_BITSETS i
		IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBigAssVehicles.iBigAssVehiclesBS[i] <> 0
			INT iBigAssVehiclesBS = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBigAssVehicles.iBigAssVehiclesBS[i]
			
			INT iBigYachtID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BIG_YACHT)
			IF (i = (iBigYachtID/32))
				CLEAR_BIT(iBigAssVehiclesBS, (iBigYachtID%32))
			ENDIF
			
			INT iBigTruckID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BIG_TRUCK)
			IF (i = (iBigTruckID/32))
				CLEAR_BIT(iBigAssVehiclesBS, (iBigTruckID%32))
			ENDIF
			
			INT iBallisticEquipmentID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BALLISTIC_EQUIPMENT)
			IF (i = (iBallisticEquipmentID/32))
				CLEAR_BIT(iBigAssVehiclesBS, (iBallisticEquipmentID%32))
			ENDIF
			
			INT iBigPlaneID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BIG_PLANE)
			IF (i = (iBigPlaneID/32))
				CLEAR_BIT(iBigAssVehiclesBS, (iBigPlaneID%32))
			ENDIF
			
			INT iBigHackerTruckID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_HACKER_TRUCK)
			IF (i = (iBigHackerTruckID/32))
				CLEAR_BIT(iBigAssVehiclesBS, (iBigHackerTruckID%32))
			ENDIF
			
			#IF FEATURE_HEIST_ISLAND
			INT iBigKosatkaID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_IH_KOSATKA)
			IF (i = (iBigKosatkaID/32))
				CLEAR_BIT(iBigAssVehiclesBS, (iBigKosatkaID%32))
			ENDIF
			#ENDIF
			
			IF iBigAssVehiclesBS <> 0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_THIS_PEGASUS_VEHICLE(MODEL_NAMES vehModel, SITE_BUYABLE_VEHICLE_COLOURS eLastBuyableVehicleColourSelected = BCV_NO_COLOUR)
	SITE_BUYABLE_VEHICLE sbv = GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(vehModel) 
	INT iBigAssID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(sbv, eLastBuyableVehicleColourSelected)
	IF iBigAssID != -1
		RETURN IS_BIG_ASS_VEHICLE_BS_SET(iBigAssID) 
	ENDIF	
	RETURN FALSE
ENDFUNC

ENUM BIGASS_VEHICLE_REWARDED_ENUM
	 eBIGASS_VEHICLE_REWARDED_unset		= -1
	,eBIGASS_VEHICLE_REWARDED_SUCCESS	= 0
	,eBIGASS_VEHICLE_REWARDED_FAILED_INVALID
	,eBIGASS_VEHICLE_REWARDED_FAILED_OWNED
	,eBIGASS_VEHICLE_REWARDED_FAILED_TRANSACTION
ENDENUM
FUNC BOOL SET_BIGASS_VEHICLE_REWARDED(SITE_BUYABLE_VEHICLE eSBV, INT &iTransactionStatus, BIGASS_VEHICLE_REWARDED_ENUM &eResult)
	eResult = eBIGASS_VEHICLE_REWARDED_unset
	INT iBigAssID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(eSBV)
	IF (iBigAssID = -1)
		PRINTLN("SET_BIGASS_VEHICLE_REWARDED(", GET_LABEL_BUYABLE_VEHICLE(eSBV), ") eResult = INVALID")
		eResult = eBIGASS_VEHICLE_REWARDED_FAILED_INVALID
		iTransactionStatus = GENERIC_TRANSACTION_STATE_FAILED
		RETURN TRUE
	ENDIF
	
	IF IS_BIG_ASS_VEHICLE_BS_SET(iBigAssID)
		PRINTLN("SET_BIGASS_VEHICLE_REWARDED(", GET_LABEL_BUYABLE_VEHICLE(eSBV), ") eResult = OWNED")
		eResult = eBIGASS_VEHICLE_REWARDED_FAILED_OWNED
		iTransactionStatus = GENERIC_TRANSACTION_STATE_FAILED
		RETURN TRUE
	ENDIF
	
	IF !USE_SERVER_TRANSACTIONS()
		PRINTLN("SET_BIGASS_VEHICLE_REWARDED(\"", GET_LABEL_BUYABLE_VEHICLE(eSBV), "\") eResult = SUCCESS")
		
		SET_BIG_ASS_VEHICLE_BS(iBigAssID)
		GB_SET_SHOULD_REFRESH_OWNED_ABILITY_VEHICLES()
		eResult = eBIGASS_VEHICLE_REWARDED_SUCCESS
		iTransactionStatus = GENERIC_TRANSACTION_STATE_SUCCESS
		RETURN TRUE
	ELSE
		TEXT_LABEL_63 tl63ItemKey
		GENERATE_BA_VEHICLE_KEY_FOR_SHOP_CATALOGUE(tl63ItemKey, eSBV)
		
		SWITCH iTransactionStatus
			// Start the transaction
			CASE GENERIC_TRANSACTION_STATE_DEFAULT
				//add the vehicle
				IF NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_SERVICE_UNLOCKED, GET_HASH_KEY(tl63ItemKey), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, 0, DEFAULT_CASH_TRANSACTION_FLAGS)
					PRINTLN("SET_BIGASS_VEHICLE_REWARDED - Failed to add item \"", tl63ItemKey, "\"")
					eResult = eBIGASS_VEHICLE_REWARDED_FAILED_TRANSACTION
					iTransactionStatus = GENERIC_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					RETURN TRUE
				ENDIF
				
				//add the free price override
				STRING sDoCouponName
				sDoCouponName = "PO_COUPON_CAR_XMAS2017"	//"P0_COUPON_CASINO_REWARD"
				IF NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_SERVICE_UNLOCKED, GET_HASH_KEY(sDoCouponName), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, GET_HASH_KEY(tl63ItemKey))
					PRINTLN("SET_BIGASS_VEHICLE_REWARDED - Failed to add vehicle coupon hash \"", sDoCouponName, "\" to basket (iItemId: \"", tl63ItemKey, "\").")
					eResult = eBIGASS_VEHICLE_REWARDED_FAILED_TRANSACTION
					iTransactionStatus = GENERIC_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					RETURN TRUE
				ENDIF
				
				IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
					PRINTLN("SET_BIGASS_VEHICLE_REWARDED - Checkout started")
					iTransactionStatus = GENERIC_TRANSACTION_STATE_PENDING
				ELSE
					PRINTLN("SET_BIGASS_VEHICLE_REWARDED - Failed to start checkout")
					eResult = eBIGASS_VEHICLE_REWARDED_FAILED_TRANSACTION
					iTransactionStatus = GENERIC_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					RETURN TRUE
				ENDIF
			BREAK
			CASE GENERIC_TRANSACTION_STATE_PENDING
				IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
						PRINTLN("SET_BIGASS_VEHICLE_REWARDED(\"", GET_LABEL_BUYABLE_VEHICLE(eSBV), "\") \"", tl63ItemKey, "\" eResult = SUCCESS")
						eResult = eBIGASS_VEHICLE_REWARDED_SUCCESS
						iTransactionStatus = GENERIC_TRANSACTION_STATE_SUCCESS
						DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
						
						SET_BIG_ASS_VEHICLE_BS(iBigAssID)
						GB_SET_SHOULD_REFRESH_OWNED_ABILITY_VEHICLES()
						RETURN TRUE
					ELSE
						PRINTLN("SET_BIGASS_VEHICLE_REWARDED(\"", GET_LABEL_BUYABLE_VEHICLE(eSBV), "\") \"", tl63ItemKey, "\" eResult = FAILED")
						eResult = eBIGASS_VEHICLE_REWARDED_FAILED_TRANSACTION
						iTransactionStatus = GENERIC_TRANSACTION_STATE_FAILED
						DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL IS_ANY_PEGASUS_DELIVERED_DLC_VEHICLE_OWNED()
	scrShopVehicleData vehicleData
	INT iRepeat = 0

	REPEAT GET_NUM_DLC_VEHICLES() iRepeat
		IF GET_DLC_VEHICLE_DATA(iRepeat, vehicleData)
			IF NOT IS_CONTENT_ITEM_LOCKED(vehicleData.m_lockHash)
				IF NOT IS_DLC_VEHICLE_SLOT_SUITABLE_FOR_GARAGE(iRepeat)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Check to see if player has the chosen vehicle for boat / plane races   
FUNC BOOL IS_PLAYER_SELECTING_PURCHASED_PLANE_OR_BOAT()

	IF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
	OR IS_BOAT_RACE(g_FMMC_STRUCT.iRaceType)
		
		IF IS_ANY_BIG_ASS_VEHICLE_BS_SET()
			
			PRINTLN("[CORONA] IS_PLAYER_SELECTING_PURCHASED_PLANE_OR_BOAT - PLAYER HAS PURCHASED VEHICLE, ", PICK_STRING(IS_BIG_ASS_VEHICLE_BS_SET(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(g_mnMyRaceModel))), "TRUE", "FALSE"))
			
			RETURN IS_BIG_ASS_VEHICLE_BS_SET(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(g_mnMyRaceModel)))
		ENDIF		
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC SITE_BUYABLE_VEHICLE_COLOURS GET_COLOUR_OF_OWNED_PEGASUS_VEHICLE(SITE_BUYABLE_VEHICLE ePegasusEnum)

	// check if owns any of the marshalss with the color
	INT iBigAssID
	INT iColour			
	REPEAT COUNT_OF(SITE_BUYABLE_VEHICLE_COLOURS) iColour
		IF (INT_TO_ENUM(SITE_BUYABLE_VEHICLE_COLOURS, iColour) != BCV_NO_COLOUR)
			iBigAssID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(ePegasusEnum, INT_TO_ENUM(SITE_BUYABLE_VEHICLE_COLOURS, iColour))	
			IF IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBigAssVehicles.iBigAssVehiclesBS[(iBigAssID/32)], (iBigAssID%32))								
				RETURN INT_TO_ENUM(SITE_BUYABLE_VEHICLE_COLOURS, iColour)
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN BCV_NO_COLOUR

ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_PEGASUS_VEHICLE(SITE_BUYABLE_VEHICLE ePegasusEnum, SITE_BUYABLE_VEHICLE_COLOURS eLastBuyableVehicleColourSelected = BCV_NO_COLOUR)
	IF IS_ANY_BIG_ASS_VEHICLE_BS_SET()
		INT iBigAssID
		IF (ePegasusEnum = BV_NG_MARSHALL)
		
			// check if owns any of the marshalss with the color
			INT iColour			
			REPEAT COUNT_OF(SITE_BUYABLE_VEHICLE_COLOURS) iColour
				iBigAssID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(ePegasusEnum, INT_TO_ENUM(SITE_BUYABLE_VEHICLE_COLOURS, iColour))	
				IF (iBigAssID > -1)
					IF IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBigAssVehicles.iBigAssVehiclesBS[(iBigAssID/32)], (iBigAssID%32))								
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
					
		ELSE
			iBigAssID = GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(ePegasusEnum, eLastBuyableVehicleColourSelected)				
			IF (iBigAssID > -1)
				RETURN IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBigAssVehicles.iBigAssVehiclesBS[(iBigAssID/32)], (iBigAssID%32))	
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//Add to the players cash earned for this job
PROC ADD_TO_JOB_CASH_EARNINGS(INT iCash)
	g_iMyMatchCashEarnings += iCash
ENDPROC

//Reset the heist data after we're done with it. 	
PROC RESET_HEIST_JobBInfo_FOR_TELEMETRY(BOOL bFullReset = TRUE)
	PRINTLN("[TEL] * RESET_HEIST_JobBInfo_FOR_TELEMETRY")
	g_sJobHeistInfo.m_kills = 0
	g_sJobHeistInfo.m_deaths = 0
	g_sJobHeistInfo.m_Cash = 0
	g_sJobHeistInfo.m_CashStart = 0
	g_sJobHeistInfo.m_CashEnd = 0
	g_sJobHeistInfo.m_MissionLaunch = 0
	g_sJobHeistInfo.m_XP = 0
	g_sJobHeistInfo.m_Suicides = 0
	g_sJobHeistInfo.m_Rank = 0
	g_sJobHeistInfo.m_CrewId = 0
	g_sJobHeistInfo.m_TeamId = 0
	g_sJobHeistInfo.m_matchResult = 0
	g_sJobHeistInfo.m_jpEarned = 0
	g_sJobHeistInfo.m_celebrationanim = 0
	g_sJobHeistInfo.m_quickplayanim = 0
	g_sJobHeistInfo.m_leftInProgress = FALSE
	g_sJobHeistInfo.m_leaderscashcutpercentage = 0
	g_sJobHeistInfo.m_lesterCalled = 0
	g_sJobHeistInfo.m_TimePlanningBoard = 0
	g_sJobHeistInfo.m_outfitChoiceBy = 0
	g_sJobHeistInfo.m_difficulty = 0
	g_sJobHeistInfo.m_1stperson = 0
	g_sJobHeistInfo.m_medal = 0
	g_sJobHeistInfo.m_teamLivesUsed = 0
	g_sJobHeistInfo.m_failureReason = 0
	g_sJobHeistInfo.m_failureRole = 0
	g_sJobHeistInfo.m_spookedCops = 0
	g_sJobHeistInfo.m_cashLost = 0
	g_sJobHeistInfo.m_cashPickedUp = 0
	g_sJobHeistInfo.m_minigameTimeTaken0 = 0
	g_sJobHeistInfo.m_minigameNumberOfTimes0 = 0
	g_sJobHeistInfo.m_minigameTimeTaken1 = 0
	g_sJobHeistInfo.m_minigameNumberOfTimes1 = 0
	g_sJobHeistInfo.m_usedTripSkip = 0
	//Only do this if it's not a quick restart
	IF bFullReset
		g_sJobHeistInfo.m_ishost = FALSE
		g_sJobHeistInfo.m_role = 0
		g_sJobHeistInfo.m_cashcutpercentage = 0
		g_sJobHeistInfo.m_clothes = 0
		g_sJobHeistInfo.m_sessionVisible = FALSE
		g_sJobHeistInfo.m_heistRootConstentId = 0
		g_sJobHeistInfo.m_outfitChoiceType = 0
	ENDIF
ENDPROC

PROC RESET_GANGOPS_JobBInfo_FOR_TELEMETRY(BOOL bFullReset = TRUE)
	PRINTLN("[JS][GOTEL][TEL] RESET_HEIST_JobBInfo_FOR_TELEMETRY")
	g_sJobGangopsInfo.m_infos.m_kills = 0
	g_sJobGangopsInfo.m_infos.m_deaths = 0
	g_sJobGangopsInfo.m_infos.m_Cash = 0
	g_sJobGangopsInfo.m_infos.m_CashStart = 0
	g_sJobGangopsInfo.m_infos.m_CashEnd = 0
	g_sJobGangopsInfo.m_infos.m_MissionLaunch = 0
	g_sJobGangopsInfo.m_infos.m_XP = 0
	g_sJobGangopsInfo.m_infos.m_Suicides = 0
	g_sJobGangopsInfo.m_infos.m_Rank = 0
	g_sJobGangopsInfo.m_infos.m_CrewId = 0
	g_sJobGangopsInfo.m_infos.m_TeamId = 0
	g_sJobGangopsInfo.m_infos.m_matchResult = 0
	g_sJobGangopsInfo.m_infos.m_jpEarned = 0
	g_sJobGangopsInfo.m_infos.m_celebrationanim = 0
	g_sJobGangopsInfo.m_infos.m_quickplayanim = 0
	g_sJobGangopsInfo.m_infos.m_leftInProgress = FALSE
	IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
	AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
	AND g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
	AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		PRINTLN("[JS][GOTEL][TEL] Resetting g_sJobGangopsInfo.m_infos.m_heistSessionHashMac and g_sJobGangopsInfo.m_infos.m_heistSessionIdPosixTime to 0")
		g_sJobGangopsInfo.m_infos.m_heistSessionHashMac = 0
		g_sJobGangopsInfo.m_infos.m_heistSessionIdPosixTime = 0
	ENDIF	
	g_sJobGangopsInfo.m_infos.m_lesterCalled = 0
	g_sJobGangopsInfo.m_infos.m_TimePlanningBoard = 0
	g_sJobGangopsInfo.m_infos.m_outfitChoiceBy = 0
	g_sJobGangopsInfo.m_infos.m_difficulty = 0
	g_sJobGangopsInfo.m_infos.m_1stperson = 0
	g_sJobGangopsInfo.m_infos.m_medal = 0
	g_sJobGangopsInfo.m_infos.m_teamLivesUsed = 0
	g_sJobGangopsInfo.m_infos.m_failureReason = 0
	g_sJobGangopsInfo.m_infos.m_failureRole = 0
	g_sJobGangopsInfo.m_infos.m_spookedCops = 0
	g_sJobGangopsInfo.m_infos.m_cashLost = 0
	g_sJobGangopsInfo.m_infos.m_cashPickedUp = 0
	g_sJobGangopsInfo.m_infos.m_minigameTimeTaken0 = 0
	g_sJobGangopsInfo.m_infos.m_minigameNumberOfTimes0 = 0
	g_sJobGangopsInfo.m_infos.m_minigameTimeTaken1 = 0
	g_sJobGangopsInfo.m_infos.m_minigameNumberOfTimes1 = 0
	g_sJobGangopsInfo.m_infos.m_usedTripSkip = 0
	g_sJobGangopsInfo.m_FailedStealth = 0

	//Only do this if it's not a quick restart
	IF bFullReset
		g_sJobGangopsInfo.m_infos.m_ishost = FALSE
		g_sJobGangopsInfo.m_infos.m_role = 0
		IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
		AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
		AND g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
			g_sJobGangopsInfo.m_infos.m_cashcutpercentage = 0
			g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage = 0
		ENDIF	
		g_sJobGangopsInfo.m_infos.m_clothes = 0
		g_sJobGangopsInfo.m_infos.m_sessionVisible = FALSE
		g_sJobGangopsInfo.m_infos.m_heistRootConstentId = 0
		g_sJobGangopsInfo.m_infos.m_outfitChoiceType = 0
		g_sJobGangopsInfo.m_bossId1 = 0
		g_sJobGangopsInfo.m_bossId2 = 0
		g_sJobGangopsInfo.m_BossType = 0
		g_sJobGangopsInfo.m_OwnBase = 0
		g_sJobGangopsInfo.m_OwnCannon = 0
		g_sJobGangopsInfo.m_OwnSecurityRoom = 0
		g_sJobGangopsInfo.m_OwnLounge = 0
		g_sJobGangopsInfo.m_OwnLivingQuarters = 0
		g_sJobGangopsInfo.m_OwnTiltRotor = 0
	ENDIF
ENDPROC

FUNC INT GANG_OPS_GET_FINALE_FROM_ROOT_ID()
	PRINTLN("[TEL] GANG_OPS_GET_FINALE_FROM_ROOT_ID - g_FMMC_STRUCT.iRootContentIDHash = ", g_FMMC_STRUCT.iRootContentIDHash)
	IF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_MORGUE]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_DELOREAN]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_SERVERFARM]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_IAABASE_FINALE]
		PRINTLN("[TEL] GANG_OPS_GET_FINALE_FROM_ROOT_ID - RETURN g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_IAABASE_FINALE] = ", g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_IAABASE_FINALE])
		RETURN g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_IAABASE_FINALE]
	ELIF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_STEALOSPREY]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_FOUNDRY]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_RIOTVAN]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_SUBMARINECAR]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE]
		PRINTLN("[TEL] GANG_OPS_GET_FINALE_FROM_ROOT_ID - RETURN g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE] = ", g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE])
		RETURN g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE]
	ELIF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_PREDATOR]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_BMLAUNCHER]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_BCCUSTOM]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_STEALTHTANKS]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_SPYPLANE]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE]
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2]
		PRINTLN("[TEL] GANG_OPS_GET_FINALE_FROM_ROOT_ID - RETURN g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE] = ", g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE])
		RETURN g_sMPTunables.iGangOpsFlowRootContentID[ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE]
	ENDIF
	RETURN 0
ENDFUNC

FUNC BOOL IS_THIS_ARENA_MODE_A_TEAM_MODE()

	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ELIF IS_THIS_ARCADE_RACE_TYPE()
		RETURN FALSE
	ELIF IS_THIS_DESTRUCTION_DERBY_RACE_TYPE() 
		RETURN FALSE
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
		IF GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM
			RETURN TRUE
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		IF GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM		
			RETURN TRUE
		ENDIF
	ENDIF	
	
	IF IS_THIS_A_DEATHMATCH()
		IF GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM
		OR GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_KOTH_TEAM
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC 


PROC RESET_ARENA_TELEMETRY_DATA_VARS()
	PRINTLN("[ARENA WARS TEL] * RESET_ARENA_TELEMETRY_DATA_VARS")

	STRUCT_ARENA_WARS_ENDED sArena_Wars_Telemetry_dataTemp
	g_sArena_Telemetry_data = sArena_Wars_Telemetry_dataTemp

ENDPROC




/// PURPOSE: DEALS WITH TELEMETRY FOR MATCH STARTS
/// PARAMS:
///    matchType -  they type of match being started:
///    FMMC_TYPE_MG_ARM_WRESTLING		
///    FMMC_TYPE_MG_DARTS				
///    FMMC_TYPE_GANGHIDEOUT			
///    FMMC_TYPE_MG_GOLF				
///    FMMC_TYPE_MG_SHOOTING_RANGE	
///    FMMC_TYPE_MG_TENNIS			 
///    FMMC_TYPE_BASE_JUMP			 
///    FMMC_TYPE_DEATHMATCH			 
///    FMMC_TYPE_SURVIVAL			 	
///    FMMC_TYPE_MISSION				
///    FMMC_TYPE_RACE		
PROC private_DEAL_WITH_FM_MATCH_START_AND_END(	INT matchType, 
										INT posixTime, 					//posixTime - server brodcast data obtained by the script server calling:PLAYSTATS_CREATE_MATCH_HISTORY_ID_2()
										INT hashedMac, 					//hashedMac		 - server brodcast data obtained by the script server calling:PLAYSTATS_CREATE_MATCH_HISTORY_ID_2()										
										BOOL bStart 			= TRUE, //Is this the start of the match
										INT iVariation 			= 0,	//The mission variation, only relevent for minigames, MP_MISSION_DATA  MissionData.missionVariation
										INT kills 				= 0,	//The number of Kills
										INT deaths 				= 0,	//The number of Deaths
										INT suicides 			= 0,	//The number of suicides
										INT highestKillStreak 	= 0,	//The Highest kill streak
										INT teamId 				= 0,	//Players Team
										INT iPassFailStatus		= 0, 	//The enod of mission status
										INT iPositionInLB		= 0,	//Position in leaderboard	
										INT iHeadShots			= 0,	//number of Headshots
										MODEL_NAMES nmVehicleId = DUMMY_MODEL_FOR_SCRIPT, //The vehicle you were in at the end of a race
										INT iSurvivedWave		= 0, 	//Survival waved reached
										BOOL bForceRoundsEnd	= FALSE,//Force Call Rounds
										INT iMissionDifficulty 	= 0,	//Mission Difficulty
										INT Time				= 0, 	//Time taken to pass mission
										BOOL bDnf 				= FALSE,//DNF?
										BOOL bTeamMode			= FALSE)//Team
	#IF IS_DEBUG_BUILD		
	IF bStart
		PRINTLN("[TEL] * private_DEAL_WITH_FM_MATCH_START_AND_END bStart = TRUE")
	ELSE
		PRINTLN("[TEL] * private_DEAL_WITH_FM_MATCH_START_AND_END bStart = FALSE")
	ENDIF
	#ENDIF
	
	IF Time = 0
	ENDIF
	
	//If we're not in a net game then get out. 
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[TEL] private_DEAL_WITH_FM_MATCH_START_AND_END - NETWORK_IS_GAME_IN_PROGRESS - get out")
		EXIT
	ENDIF

	
	TEXT_LABEL_31 matchCreator
	TEXT_LABEL_31 uniqueMatchId
	SWITCH matchType		
		CASE FMMC_TYPE_GANGHIDEOUT			
		CASE FMMC_TYPE_BASE_JUMP			
		CASE FMMC_TYPE_DEATHMATCH			
		CASE FMMC_TYPE_SURVIVAL				
		CASE FMMC_TYPE_MISSION				
		CASE FMMC_TYPE_RACE					
			matchCreator  = g_FMMC_STRUCT.tl23MissionCreatorIDForTelemetry
			uniqueMatchId = g_FMMC_STRUCT.tl31LoadedContentID
		BREAK
		
		CASE FMMC_TYPE_RACE_TO_POINT		
		CASE FMMC_TYPE_PROSTITUTES		
		CASE FMMC_TYPE_IMPORT_EXPORT			
		CASE FMMC_TYPE_CRATE_DROP		
			matchCreator 	= "ROCKSTAR_AMBIANT"
			GET_UNIQUE_MATCHID_FOR_AMBIANT(uniqueMatchId, matchType, iVariation)
		BREAK
		
		CASE FMMC_TYPE_MG_GOLF				
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	
		CASE FMMC_TYPE_MG_TENNIS		
		CASE FMMC_TYPE_MG_ARM_WRESTLING		
		CASE FMMC_TYPE_MG_DARTS		
		CASE FMMC_TYPE_MG_PILOT_SCHOOL
		CASE FMMC_TYPE_IMPROMPTU_DM
		CASE FMMC_TYPE_GB_BOSS_DEATHMATCH
			matchCreator 	= "ROCKSTAR_MINIGAME"
			GET_UNIQUE_MATCHID_FOR_MINIGAME(uniqueMatchId, matchType, iVariation)
		BREAK
		#IF IS_DEBUG_BUILD
		DEFAULT
			SCRIPT_ASSERT("private_DEAL_WITH_FM_MATCH_START_AND_END matchType = DEFAULT - Bug to Bobby Wright please")
		BREAK
		#ENDIF
	ENDSWITCH
	
	BOOL isTeamDeathmatch = FALSE
	INT  raceType = 0
	IF matchType = FMMC_TYPE_DEATHMATCH
		isTeamDeathmatch = bTeamMode 
	ELIF matchType = FMMC_TYPE_RACE
		IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
			PRINTLN("[TEL] IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL 		  = TRUE")
			matchType += ciTUTORIAL_RACE_TYPE_CONST
		ENDIF
		raceType = GlobalServerBD_Races.iRaceMode
		IF IS_PLAYER_SCTV(PLAYER_ID())
		OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
			PRINTLN("[TEL] iPositionInLBis less than 0, setting to 32")
			iPositionInLB = 0
		ENDIF
		IF iPositionInLB < 0
			iPositionInLB = 32
			PRINTLN("[TEL] iPositionInLBis less than 0, setting to 32")
		ENDIF
	ELIF matchType = FMMC_TYPE_MISSION
		IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
			PRINTLN("[TEL] IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL 		  = TRUE")
			matchType += ciTUTORIAL_RACE_TYPE_CONST
		ENDIF		
	ELIF matchType = FMMC_TYPE_SURVIVAL
		IF NOT HAS_PRIMARY_SURVIVAL_BEEN_COMPLETED()
			PRINTLN("[TEL] IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL 		  = TRUE")
			matchType += ciTUTORIAL_RACE_TYPE_CONST
		ENDIF		
	ENDIF		
//Removed as we now pass up the POSIX time in END
//	//If we're on a hesit then we need to over ride the vales!
//	#IF FEATURE_HEIST_PLANNING
//	//If we are on a heist then we need to call the heist end of mission
//	IF (Is_Player_Currently_On_MP_Heist(PLAYER_ID()) 
//	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))
//		IF g_HeistTelemetryData.iHeistHashedMac != 0
//		AND g_HeistTelemetryData.iHeistPosixTime != 0
//			hashedMac = g_HeistTelemetryData.iHeistHashedMac
//			posixTime = g_HeistTelemetryData.iHeistPosixTime
//			PRINTLN("[TEL] private_DEAL_WITH_FM_MATCH_START_AND_END - g_HeistTelemetryData.iHeistHashedMac = ", g_HeistTelemetryData.iHeistHashedMac)
//			PRINTLN("[TEL] private_DEAL_WITH_FM_MATCH_START_AND_END - g_HeistTelemetryData.iHeistPosixTime = ", g_HeistTelemetryData.iHeistPosixTime)
//		ENDIF
//	ENDIF
//	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF hashedMac = 0
	AND bStart
		PRINTLN("[TEL] private_DEAL_WITH_FM_MATCH_START_AND_END hashedMac = 0")
		SCRIPT_ASSERT("private_DEAL_WITH_FM_MATCH_START_AND_END hashedMac = 0")
	ENDIF
	IF posixTime = 0
		PRINTLN("[TEL] private_DEAL_WITH_FM_MATCH_START_AND_END posixTime = 0")
		SCRIPT_ASSERT("private_DEAL_WITH_FM_MATCH_START_AND_END posixTime = 0")
	ENDIF
	IF IS_STRING_NULL_OR_EMPTY(uniqueMatchId)
		PRINTLN("[TEL] private_DEAL_WITH_FM_MATCH_START_AND_END uniqueMatchId = NULL")
		SCRIPT_ASSERT("private_DEAL_WITH_FM_MATCH_START_AND_END uniqueMatchId = NULL")
	ENDIF
	#ENDIF
	
	IF bStart
		IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		AND HAS_ROUNDS_TELEMETRY_STARTED()
			PRINTLN("[TEL] * private_DEAL_WITH_FM_MATCH_START_AND_END - IS_THIS_A_ROUNDS_MISSION_FOR_CORONA")
			EXIT
		ENDIF
		
		g_NjvsAndQuitModeTelemetryData.duration = GET_NETWORK_TIME()
		PRINTLN("[TEL] - g_NjvsAndQuitModeTelemetryData.duration = GET_NETWORK_TIME()")					
		
		SET_ROUNDS_TELEMETRY_STARTED()
		//Reset values
		g_iMyMatchCashEarnings = 0
		IF matchType = FMMC_TYPE_MISSION
		OR matchType = FMMC_TYPE_DEATHMATCH
		OR matchType = FMMC_TYPE_IMPROMPTU_DM
			g_bNeedToCallMatchEndOnQuit = TRUE
			PRINTLN("[TEL] - g_bNeedToCallMatchEndOnQuit = TRUE")	
		ENDIF
		//Set the unique match ID for Neil F
		g_tl31uniqueMatchId = uniqueMatchId
		PRINTLN("[TEL] g_tl31uniqueMatchId = ", g_tl31uniqueMatchId)
		PRINTLN("[TEL] PLAYSTATS_MATCH_STARTED()")		
		PRINTLN("[TEL] matchType 		  = ", matchType)
		PRINTLN("[TEL] matchCreator     = ", matchCreator)
		PRINTLN("[TEL] uniqueMatchId    = ", uniqueMatchId)
		PRINTLN("[TEL] hashedMac        = ", hashedMac)
		PRINTLN("[TEL] posixTime        = ", posixTime)
		PRINTLN("[TEL] oncalljointime   = ", g_iOnCallTelemetryLaunchTime, " (TELEMETRY_ON_CALL)")
		PRINTLN("[TEL] oncalljoinstate  = ", g_iOnCallTelemetryLaunchStatus, " (TELEMETRY_ON_CALL)")
			
		// mission name
		SC_PRESENCE_ATTR_SET_STRING(mp_mis_str, GlobalplayerBD_MissionName[NATIVE_TO_INT(PLAYER_ID())].tl63MissionName)

		// mission instance
		SC_PRESENCE_ATTR_SET_INT(mp_mis_inst, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iInstanceId)
		
		// mission id
		SC_PRESENCE_ATTR_SET_STRING(mp_mis_id, g_tl31uniqueMatchId)
		
		// team
		SC_PRESENCE_ATTR_SET_INT(mp_team, GET_PLAYER_TEAM(PLAYER_ID()))		
	
		IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
			matchType = FMMC_TYPE_SPECIAL_VEHICLE_MISSION
		ELIF WVM_FLOW_IS_CURRENT_MISSION_WVM_FLOW()
			matchType = FMMC_TYPE_GUNRUNNING_WEAPONIZED_VEHICLE
			PRINTLN("[TEL] PLAYSTATS_POST_MATCH_BLOB() matchType = FMMC_TYPE_GUNRUNNING_WEAPONIZED_VEHICLE")	
		ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
			matchType = FMMC_TYPE_MISSION_LTS
			PRINTLN("[TEL] PLAYSTATS_MATCH_STARTED() matchType = FMMC_TYPE_MISSION_LTS")		
		ELIF Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
			matchType = FMMC_TYPE_MISSION_CTF
			PRINTLN("[TEL] PLAYSTATS_POST_MATCH_BLOB() matchType = FMMC_TYPE_MISSION_CTF")	
		ELIF Is_Player_Currently_On_MP_Coop_Mission(PLAYER_ID())
			matchType = FMMC_TYPE_MISSION_COOP
			PRINTLN("[TEL] PLAYSTATS_POST_MATCH_BLOB() matchType = FMMC_TYPE_MISSION_COOP")	
		ENDIF
		
		INT iMissionLaunch
		IF matchType = FMMC_TYPE_IMPROMPTU_DM
		OR matchType = FMMC_TYPE_GANGHIDEOUT
		OR matchType = FMMC_TYPE_GB_BOSS_DEATHMATCH
			iMissionLaunch = ciMISSION_ENTERY_TYPE_WALK_IN
		ELIF GET_FM_JOB_ENTERY_TYPE() != ciMISSION_ENTERY_TYPE_INVALID
			iMissionLaunch		= GET_FM_JOB_ENTERY_TYPE()
		#IF IS_DEBUG_BUILD
		ELSE
			IF NOT HAS_KICKED_IDLING_TRANSITION()
			AND GET_JOINING_GAMEMODE() != GAMEMODE_SP
				PRINTLN("[TEL]  GET_FM_JOB_ENTERY_TYPE - START = ciMISSION_ENTERY_TYPE_INVALID bug to bobby wright with how you got on this mission please!")
				SCRIPT_ASSERT(" GET_FM_JOB_ENTERY_TYPE - START = ciMISSION_ENTERY_TYPE_INVALID bug to bobby wright with how you got on this mission please!")
			ENDIF
		#ENDIF
		ENDIF
		

		
		MatchStartInfo sMatchStartInfo
		sMatchStartInfo.m_matchType = matchType
		sMatchStartInfo.m_hashedMac = hashedMac
		sMatchStartInfo.m_posixTime = posixTime
		sMatchStartInfo.m_oncalljointime = g_iOnCallTelemetryLaunchTime	// Last two params are now on-call: 1911107
		sMatchStartInfo.m_oncalljoinstate = g_iOnCallTelemetryLaunchStatus	// Last two params are now on-call: 1911107
		sMatchStartInfo.m_missionDifficulty = iMissionDifficulty
		sMatchStartInfo.m_MissionLaunch = iMissionLaunch
		sMatchStartInfo.m_RootContentId = g_FMMC_STRUCT.iRootContentIDHash
		PRINTLN("[TEL]m_matchType         = ", sMatchStartInfo.m_matchType)
		PRINTLN("[TEL]m_hashedMac         = ", sMatchStartInfo.m_hashedMac)
		PRINTLN("[TEL]m_posixTime         = ", sMatchStartInfo.m_posixTime)
		PRINTLN("[TEL]m_oncalljointime    = ", sMatchStartInfo.m_oncalljointime)
		PRINTLN("[TEL]m_oncalljoinstate   = ", sMatchStartInfo.m_oncalljoinstate)
		PRINTLN("[TEL]m_missionDifficulty = ", sMatchStartInfo.m_missionDifficulty)
		PRINTLN("[TEL]m_MissionLaunch 	  = ", sMatchStartInfo.m_MissionLaunch)
		PRINTLN("[TEL]m_RootContentId 	  = ", sMatchStartInfo.m_RootContentId)
		PRINTLN("[TEL] * PLAYSTATS_MATCH_STARTED(", matchType, ", ", matchCreator, ", ", uniqueMatchId, ", ", hashedMac, ", ", posixTime, ", ", g_iOnCallTelemetryLaunchTime, ", ", g_iOnCallTelemetryLaunchStatus, ")")		
		PLAYSTATS_MATCH_STARTED(matchCreator, uniqueMatchId, sMatchStartInfo)
		
		// Reset our on call telemetry values
		RESET_JOB_ON_CALL_TELEMETRY()
		
		IF NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			//Set the current
			g_TransitionSessionNonResetVars.sJobChain.iCurrMacaddresshash = hashedMac
			g_TransitionSessionNonResetVars.sJobChain.iCurrPosixtime = posixTime
			//Set the chain if it's 0!
			IF 	g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash = 0
			OR g_TransitionSessionNonResetVars.sJobChain.iPosixtime = 0
				g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash = hashedMac
				g_TransitionSessionNonResetVars.sJobChain.iPosixtime = posixTime
			ENDIF
			//Tell Logs about this!
			PRINTLN("[TEL] PLAYSTATS_JOB_CHAIN - g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash     = ", g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash)	
			PRINTLN("[TEL] PLAYSTATS_JOB_CHAIN - g_TransitionSessionNonResetVars.sJobChain.iPosixtime          = ", g_TransitionSessionNonResetVars.sJobChain.iPosixtime)	
			PRINTLN("[TEL] PLAYSTATS_JOB_CHAIN - g_TransitionSessionNonResetVars.sJobChain.iCurrMacaddresshash = ", g_TransitionSessionNonResetVars.sJobChain.iCurrMacaddresshash)	
			PRINTLN("[TEL] PLAYSTATS_JOB_CHAIN - g_TransitionSessionNonResetVars.sJobChain.iCurrPosixtime      = ", g_TransitionSessionNonResetVars.sJobChain.iCurrPosixtime)	
		ELSE			
			//Set the chain if it's 0!
			IF 	g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash = 0
			OR g_TransitionSessionNonResetVars.sJobChain.iPosixtime = 0
				g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash = hashedMac
				g_TransitionSessionNonResetVars.sJobChain.iPosixtime = posixTime
			ENDIF
			PRINTLN("[TEL] PLAYSTATS_JOB_CHAIN - g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash     = ", g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash)	
			PRINTLN("[TEL] PLAYSTATS_JOB_CHAIN - g_TransitionSessionNonResetVars.sJobChain.iPosixtime          = ", g_TransitionSessionNonResetVars.sJobChain.iPosixtime)	
		ENDIF
		g_sJobPoints.iJobPointsStart = GET_JOB_POINTS_LOCAL()
		PRINTLN("[TEL] private_DEAL_WITH_FM_MATCH_START_AND_END g_sJobPoints.iJobPointsStart = ", g_sJobPoints.iJobPointsStart)

		IF g_bPlayerHasToPayForMission
			
			INT iMissionCostExcRental = g_iMissionCost
			BOOL bProcessVehicleRentalCost
		
			// Player gets refunded cash if they use custom vehicle
			IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
				IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID())
				OR IS_PLAYER_SELECTING_PURCHASED_PLANE_OR_BOAT()
					PRINTLN("[TEL] NETWORK_PAY_MATCH_ENTRY_FEE - Used custom vehicle, reduce by: ", g_sMPTunables.ivehicle_rental_price_modifier)
					g_iMissionCost -= g_sMPTunables.ivehicle_rental_price_modifier
				ELSE
					bProcessVehicleRentalCost = TRUE
					iMissionCostExcRental -= g_sMPTunables.ivehicle_rental_price_modifier
					PRINTLN("[TEL] NETWORK_PAY_MATCH_ENTRY_FEE - Handle cash transactions as 2 separate services. Reduce entrance to: ", iMissionCostExcRental, " from: ", g_iMissionCost)
				ENDIF
			ENDIF
			
			IF g_iMissionCost > 0
			//if we didn't join late
			AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
			//And not SCTV
			AND NOT IS_PLAYER_SCTV(PLAYER_ID())
			AND NOT CV2_WAS_STUNT_RACE_LAUNCHED_AS_PROFESSIONAL()
			AND NETWORK_CAN_SPEND_MONEY(g_iMissionCost, FALSE, FALSE, TRUE)
				INT iScriptTransactionIndex
				
				// For all standard Jobs:
				IF NOT bProcessVehicleRentalCost
					// Only trigger this for costs greater than 0
					IF g_iMissionCost > 0
						IF USE_SERVER_TRANSACTIONS()
							PRINTLN("[TEL] NETWORK_PAY_MATCH_ENTRY_FEE(", g_iMissionCost, ", ", uniqueMatchId, ") - PC Transaction")	
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_MATCH_ENTRY_FEE, g_iMissionCost, iScriptTransactionIndex, FALSE, TRUE)
							g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = uniqueMatchId
						ELSE
							PRINTLN("[TEL] NETWORK_PAY_MATCH_ENTRY_FEE(", g_iMissionCost, ", ", uniqueMatchId, ")")	
							NETWORK_PAY_MATCH_ENTRY_FEE(g_iMissionCost, uniqueMatchId, FALSE, TRUE)
						ENDIF
					ENDIF
				ELSE
					// Else, if we are charging for vehicle rental, take both the entrance and rental separately (2165464)
					IF iMissionCostExcRental > 0
						IF USE_SERVER_TRANSACTIONS()
							PRINTLN("[TEL] NETWORK_PAY_MATCH_ENTRY_FEE(", iMissionCostExcRental, ", ", uniqueMatchId, ") - PC Transaction for mission cost alone")	
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_MATCH_ENTRY_FEE, iMissionCostExcRental, iScriptTransactionIndex, FALSE, TRUE)
							g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = uniqueMatchId
						ENDIF
					ENDIF
					IF USE_SERVER_TRANSACTIONS()
						PRINTLN("[TEL] NETWORK_PAY_MATCH_ENTRY_FEE(", g_sMPTunables.ivehicle_rental_price_modifier, ", ", uniqueMatchId, ") - PC Transaction for rental cost alone")	
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_RACE_VEHICLE_RENTAL, g_sMPTunables.ivehicle_rental_price_modifier, iScriptTransactionIndex, FALSE, TRUE)
						g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = uniqueMatchId
					ELSE
						PRINTLN("[TEL] NETWORK_PAY_MATCH_ENTRY_FEE(", g_iMissionCost, ", ", uniqueMatchId, ")")	
						NETWORK_PAY_MATCH_ENTRY_FEE(g_iMissionCost, uniqueMatchId, FALSE, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("[TEL] Paid = ", g_iMissionCost, ", uniqueID =  ", uniqueMatchId)
			g_bPlayerHasToPayForMission = FALSE
		ENDIF
		
		
		// DO ARENA START
			PRINTLN("[TEL] ARENA START")
			RESET_ARENA_TELEMETRY_DATA_VARS()




	ELSE
		INT iCurrentOnFootQuickplayAnim
		IF GET_MP_PLAYER_ANIM_TYPE() = ENUM_TO_INT(INTERACTION_ANIM_TYPE_CREW)
			iCurrentOnFootQuickplayAnim =  MPGlobalsInteractions.iMyCrewInteractionAnim
		ELSE
			iCurrentOnFootQuickplayAnim = GET_MP_PLAYER_ANIM_SETTING()
		ENDIF
		
		//Set the unique match ID for Neil F
		g_tl31uniqueMatchId = ""
		PRINTLN("[TEL] Clearing g_tl31uniqueMatchId = ", g_tl31uniqueMatchId)
		
		PRINTLN("[TEL] PLAYSTATS_POST_MATCH_BLOB()")
		NETWORK_CLAN_DESC sNETWORK_CLAN_DESC
		sNETWORK_CLAN_DESC = GET_PLAYER_CREW(PLAYER_ID())
		// (1313111)
		IF sNETWORK_CLAN_DESC.Id < 0
			PRINTLN("[TEL] private_DEAL_WITH_FM_MATCH_START_AND_END, sNETWORK_CLAN_DESC.Id = 0")
			sNETWORK_CLAN_DESC.Id = 0
		ENDIF
		//Need to add
		iHeadShots 			= iHeadShots
		PRINTLN("[TEL] g_iMyBetWinnings       = ", g_iMyBetWinnings)
		PRINTLN("[TEL] g_iMyMatchCashEarnings = ", g_iMyMatchCashEarnings)
		PRINTLN("[TEL] highestKillStreak 	    = ", highestKillStreak)
		PRINTLN("[TEL] kills                  = ", kills)
		PRINTLN("[TEL] deaths                 = ", deaths)
		PRINTLN("[TEL] suicides               = ", suicides)
		PRINTLN("[TEL] iPositionInLB          = ", iPositionInLB)
		PRINTLN("[TEL] teamId                 = ", teamId)
		PRINTLN("[TEL] clanId                 = ", sNETWORK_CLAN_DESC.Id)
		PRINTLN("[TEL] nmVehicleId            = ", ENUM_TO_INT(nmVehicleId))
		PRINTLN("[TEL] 0                      = ", 0)
		PRINTLN("[TEL] g_iMyBetWinnings       = ", g_iMyBetWinnings)
		PRINTLN("[TEL] g_iMyWinningBet        = ", g_iMyWinningBet)
		PRINTLN("[TEL] g_iMyStartMatchCash    = ", g_iMyStartMatchCash)
		PRINTLN("[TEL] GET_PLAYER_FM_CASH     = ", (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE()))
		
		BOOL bQuit 
		//If we have no vote status then we quit
		IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
		//OR (GET_TRANSITION_SESSION_VOTE_STATUS() = ciFMMC_EOM_VOTE_STATUS_QUIT)
			bQuit = TRUE
		ELIF NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			g_iTotalMissionsPlayedInRow++
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_JOBS_DONE_CONSECUTIVELY, g_iTotalMissionsPlayedInRow)
			PRINTLN("[TEL] g_iTotalMissionsPlayedInRow = ", g_iTotalMissionsPlayedInRow)
		ENDIF
		
		JobActivityInfo sJobActivityInfo
		
		IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
			matchType = FMMC_TYPE_SPECIAL_VEHICLE_MISSION
		ELIF WVM_FLOW_IS_CURRENT_MISSION_WVM_FLOW()
			matchType = FMMC_TYPE_GUNRUNNING_WEAPONIZED_VEHICLE
			PRINTLN("[TEL] PLAYSTATS_POST_MATCH_BLOB() matchType = FMMC_TYPE_GUNRUNNING_WEAPONIZED_VEHICLE")	
		ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
			matchType = FMMC_TYPE_MISSION_LTS
			PRINTLN("[TEL] PLAYSTATS_POST_MATCH_BLOB() matchType = FMMC_TYPE_MISSION_LTS")
		ELIF Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
			matchType = FMMC_TYPE_MISSION_CTF
			PRINTLN("[TEL] PLAYSTATS_POST_MATCH_BLOB() matchType = FMMC_TYPE_MISSION_CTF")		
		ELIF Is_Player_Currently_On_MP_Coop_Mission(PLAYER_ID())
			matchType = FMMC_TYPE_MISSION_COOP
			PRINTLN("[TEL] PLAYSTATS_POST_MATCH_BLOB() matchType = FMMC_TYPE_MISSION_COOP")	
		ENDIF
		
		//Integer Values
		sJobActivityInfo.m_Cash					= g_iMyMatchCashEarnings
		sJobActivityInfo.m_BetCash				= g_iMyBetWinnings
		IF sJobActivityInfo.m_BetCash <0
			sJobActivityInfo.m_BetCash = 0
		ENDIF
		sJobActivityInfo.m_CashStart			= g_iMyStartMatchCash
		sJobActivityInfo.m_dnf = bDnf
		sJobActivityInfo.m_CashEnd				= (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE())
		IF matchType = FMMC_TYPE_IMPROMPTU_DM
		OR matchType = FMMC_TYPE_GANGHIDEOUT
		OR matchType = FMMC_TYPE_GB_BOSS_DEATHMATCH
			sJobActivityInfo.m_MissionLaunch = ciMISSION_ENTERY_TYPE_WALK_IN
		ELIF GET_FM_JOB_ENTERY_TYPE() != ciMISSION_ENTERY_TYPE_INVALID
			sJobActivityInfo.m_MissionLaunch		= GET_FM_JOB_ENTERY_TYPE()
		#IF IS_DEBUG_BUILD
		ELSE
			IF NOT HAS_KICKED_IDLING_TRANSITION()
				PRINTLN("[TEL]  GET_FM_JOB_ENTERY_TYPE = ciMISSION_ENTERY_TYPE_INVALID bug to bobby wright with how you got on this mission please!")
				SCRIPT_ASSERT(" GET_FM_JOB_ENTERY_TYPE = ciMISSION_ENTERY_TYPE_INVALID bug to bobby wright with how you got on this mission please!")
			ENDIF
			#ENDIF
		ENDIF
		
		// Save out the Jobs outfit choices (2103812)
		IF g_iVersusOutfitStyleSetup >= 0
			sJobActivityInfo.m_outfitChoiceType = g_iVersusOutfitStyleSetup
		ENDIF
		IF g_iVersusOutfitStyleChoice >= 0
			sJobActivityInfo.m_outfitStyle = g_iVersusOutfitStyleChoice
		ENDIF
		
		//If it's a rounds mission then don't call because we only call at the very start and very end
		IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
		AND NOT bForceRoundsEnd
		AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds != 0
		AND NOT IS_ARENA_WARS_JOB()
			JobLtsRoundInfo sLtsMidRoundinfo
			PRINTLN("[TEL] [TS] [MSROUND] - private_DEAL_WITH_FM_MATCH_START_AND_END - IS_THIS_A_ROUNDS_MISSION_FOR_CORONA - PLAYSTATS_JOB_LTS_ROUND_END")
			g_sTransitionSessionData.sMissionRoundData.bNeedToCallMatchEnd = TRUE
			sLtsMidRoundinfo.m_roundNumber = GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iRoundScore
			sLtsMidRoundinfo.m_roundsplayed = g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed
			sLtsMidRoundinfo.m_Cash = g_iMyMatchCashEarnings
			sLtsMidRoundinfo.m_BetCash = g_iMyBetWinnings
			IF sLtsMidRoundinfo.m_BetCash <0
				sLtsMidRoundinfo.m_BetCash = 0
			ENDIF
			sLtsMidRoundinfo.m_CashStart = g_iMyStartMatchCash
			sLtsMidRoundinfo.m_CashEnd = (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE())
			sLtsMidRoundinfo.m_MissionLaunch = sJobActivityInfo.m_MissionLaunch
			IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
				sLtsMidRoundinfo.m_playlistHashMac = g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash
				sLtsMidRoundinfo.m_playlistPosixTime = g_TransitionSessionNonResetVars.sJobChain.iPosixtime+1
			ELSE
				sLtsMidRoundinfo.m_1stHashMac = g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash
				sLtsMidRoundinfo.m_1stPosixTime = g_TransitionSessionNonResetVars.sJobChain.iPosixtime
			ENDIF
			sLtsMidRoundinfo.m_XP = GET_PLAYER_FM_XP(PLAYER_ID())		- g_iMyStartMatchXP
			sLtsMidRoundinfo.m_Kills = kills
			sLtsMidRoundinfo.m_Deaths = deaths
			sLtsMidRoundinfo.m_Suicides = suicides
			sLtsMidRoundinfo.m_Rank = iPositionInLB
			sLtsMidRoundinfo.m_CrewId = sNETWORK_CLAN_DESC.Id	
			sLtsMidRoundinfo.m_betWon = g_iMyWinningBet
			sLtsMidRoundinfo.m_TeamId = teamId
			sLtsMidRoundinfo.m_matchResult = iPassFailStatus
			sLtsMidRoundinfo.m_jpEarned = GET_JOB_POINTS_LOCAL() - g_sJobPoints.iJobPointsStart
			IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
			OR IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
				sLtsMidRoundinfo.m_playlistID = GET_HASH_KEY(g_sCurrentPlayListDetails.tl31szContentID)
			ENDIF
			sLtsMidRoundinfo.m_leftInProgress = bQuit
			//Set the outfit choise
			//2148035
			
			//Check that the data for outfits are valid and mask the value if it isn't 
			IF g_iVersusOutfitStyleSetup >= 0
				sLtsMidRoundinfo.m_outfitStyle = g_iVersusOutfitStyleSetup
			ELSE
				IF g_iVersusOutfitStyleSetup = - 2
					sLtsMidRoundinfo.m_outfitStyle = ENUM_TO_INT(OUTFIT_STYLE_NOT_SET)
				ELSE
					sLtsMidRoundinfo.m_outfitStyle = ENUM_TO_INT(OUTFIT_STYLE_NONE)
				ENDIF
			ENDIF
			IF g_iVersusOutfitStyleChoice >= 0 
				sLtsMidRoundinfo.m_outfitChoiceType = g_iVersusOutfitStyleChoice
			ELSE
				IF g_iVersusOutfitStyleChoice = -2
					sLtsMidRoundinfo.m_outfitChoiceType = ENUM_TO_INT(OUTFIT_STYLE_NOT_SET)
				ELSE
					sLtsMidRoundinfo.m_outfitChoiceType = ENUM_TO_INT(OUTFIT_STYLE_NONE)
				ENDIF
			ENDIF
			
			PRINTLN("[TEL] sLtsMidRoundinfo.m_roundNumber       = ", sLtsMidRoundinfo.m_roundNumber)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_roundsplayed      = ", sLtsMidRoundinfo.m_roundsplayed)			
			//2148035
			PRINTLN("[TEL] sLtsMidRoundinfo.m_outfitStyle       = ", sLtsMidRoundinfo.m_outfitStyle)			
			PRINTLN("[TEL] sLtsMidRoundinfo.m_outfitChoiceType  = ", sLtsMidRoundinfo.m_outfitChoiceType)			
			PRINTLN("[TEL] sLtsMidRoundinfo.m_Cash              = ", sLtsMidRoundinfo.m_Cash)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_BetCash           = ", sLtsMidRoundinfo.m_BetCash)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_CashStart         = ", sLtsMidRoundinfo.m_CashStart)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_CashEnd           = ", sLtsMidRoundinfo.m_CashEnd)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_MissionLaunch     = ", sLtsMidRoundinfo.m_MissionLaunch)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_1stHashMac        = ", sLtsMidRoundinfo.m_1stHashMac)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_1stPosixTime      = ", sLtsMidRoundinfo.m_1stPosixTime)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_playlistHashMac   = ", sLtsMidRoundinfo.m_playlistHashMac)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_playlistPosixTime = ", sLtsMidRoundinfo.m_playlistPosixTime)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_XP                = ", sLtsMidRoundinfo.m_XP)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_Kills             = ", sLtsMidRoundinfo.m_Kills)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_Deaths            = ", sLtsMidRoundinfo.m_Deaths)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_Suicides          = ", sLtsMidRoundinfo.m_Suicides)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_Rank              = ", sLtsMidRoundinfo.m_Rank)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_CrewId            = ", sLtsMidRoundinfo.m_CrewId)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_betWon            = ", sLtsMidRoundinfo.m_betWon)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_TeamId            = ", sLtsMidRoundinfo.m_TeamId)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_matchResult       = ", sLtsMidRoundinfo.m_matchResult)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_jpEarned          = ", sLtsMidRoundinfo.m_jpEarned)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_playlistID        = ", sLtsMidRoundinfo.m_playlistID)
			PRINTLN("[TEL] sLtsMidRoundinfo.m_leftInProgress    = ", sLtsMidRoundinfo.m_leftInProgress)

//			INT CashEarned = (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE()) - g_iMyStartMatchCash

			#IF IS_DEBUG_BUILD
			FILL_DEBUG_OUTPUT_FILE_STRUCT(g_FMMC_STRUCT.tl63MissionName, 
										GET_FMMC_BIT_SET_NAME_FOR_LOGS(matchType),
										g_FMMC_STRUCT.tl31LoadedContentID,
										DEFAULT,
										Time,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_missionFile_ExternalTracker.TripSkipped,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_iMissionCost,
										DEFAULT,
										DEFAULT,
										(sNETWORK_CLAN_DESC.Id != 0))
									
									
									
//			DEBUG_FILE_REWARD_OUTPUT()
			#ENDIF

			TEXT_LABEL_23 stPlayList = ""
			IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
			OR IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
				stPlayList = g_sCurrentPlayListDetails.tl31szContentID 
			ENDIF
			//Add a print for Miguel
			PRINTLN("[TEL] * PLAYSTATS_JOB_LTS_ROUND_END(", matchCreator, ", ", uniqueMatchId, ", sLtsMidRoundinfo, ", stPlayList, ")")
			PLAYSTATS_JOB_LTS_ROUND_END(matchCreator, uniqueMatchId, sLtsMidRoundinfo, stPlayList)
			EXIT
		ENDIF
		
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
			g_bSessionVisibilitySetInCorona = NETWORK_SESSION_IS_VISIBLE()					
			PRINTLN("[TS][VIS] g_bSessionVisibilitySetInCorona = ", g_bSessionVisibilitySetInCorona)
		ENDIF
		
		//Boolean  Values
		sJobActivityInfo.m_isTeamDeathmatch				= isTeamDeathmatch
		sJobActivityInfo.m_leftInProgress				= bQuit
		//Unsigned Values (must be >= 0)
		sJobActivityInfo.m_XP							= GET_PLAYER_FM_XP(PLAYER_ID())		- g_iMyStartMatchXP
		sJobActivityInfo.m_HighestKillStreak			= highestKillStreak
		sJobActivityInfo.m_Kills						= kills
		sJobActivityInfo.m_Deaths						= deaths
		sJobActivityInfo.m_Suicides						= suicides
		sJobActivityInfo.m_Rank							= iPositionInLB	
		sJobActivityInfo.m_CrewId						= sNETWORK_CLAN_DESC.Id	
		sJobActivityInfo.m_VehicleId					= ENUM_TO_INT(nmVehicleId)
		sJobActivityInfo.m_betWon						= g_iMyWinningBet
		sJobActivityInfo.m_survivedWave					= iSurvivedWave	
		sJobActivityInfo.m_TeamId						= teamId
		sJobActivityInfo.m_MatchType					= matchType
		sJobActivityInfo.m_raceType						= g_FMMC_STRUCT.iRaceType
		IF CV2_WAS_STUNT_RACE_LAUNCHED_AS_PROFESSIONAL()
			IF sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_STUNT
				sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_PROFESSIONAL
				PRINTLN("[TEL][CV2] sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_PROFESSIONAL = ", sJobActivityInfo.m_raceType)
			ELIF sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_STUNT_P2P
				sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_PROFESSIONAL_P2P
				PRINTLN("[TEL][CV2] sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_PROFESSIONAL_P2P = ", sJobActivityInfo.m_raceType)
			ENDIF
		ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_TRANSFORM_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
			IF sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_STUNT
				sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_TRANS
				PRINTLN("[TEL][CV2] sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_TRANS = ", sJobActivityInfo.m_raceType)
			ELIF sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_STUNT_P2P
				sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_TRANS_P2P
				PRINTLN("[TEL][CV2] sJobActivityInfo.m_raceType = FMMC_RACE_TYPE_TRANS_P2P = ", sJobActivityInfo.m_raceType)
			ENDIF
		ENDIF
		sJobActivityInfo.m_raceSubtype					= raceType
		
		sJobActivityInfo.m_matchResult					= iPassFailStatus
		sJobActivityInfo.m_jobVisibilitySetInCorona		= g_bjobVisibilitySetInCorona		
		sJobActivityInfo.m_jpEarned						= GET_JOB_POINTS_LOCAL() - g_sJobPoints.iJobPointsStart
		sJobActivityInfo.m_celebrationanim 				= GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_ANIM_TY)
		IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			sJobActivityInfo.m_playlistHashMac				= g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash
			sJobActivityInfo.m_playlistPosixTime			= g_TransitionSessionNonResetVars.sJobChain.iPosixtime+1
		ELSE
			sJobActivityInfo.m_OriginalHashMac				= g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash
			sJobActivityInfo.m_OriginalPosixTime			= g_TransitionSessionNonResetVars.sJobChain.iPosixtime
		ENDIF
		sJobActivityInfo.m_sessionVisible				= g_bSessionVisibilitySetInCorona
		sJobActivityInfo.m_jobDifficulty 				= iMissionDifficulty		
		PRINTLN("[TEL] [TS][VIS] m_sessionVisible           = ", sJobActivityInfo.m_sessionVisible)
		PRINTLN("[TEL] [TS][VIS] m_jobVisibilitySetInCorona = ", sJobActivityInfo.m_jobVisibilitySetInCorona)
		

		sJobActivityInfo.m_quickplayanim 				= iCurrentOnFootQuickplayAnim

		IF IS_JOB_FORCED_WEAPON_ONLY()
		OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
			sJobActivityInfo.m_weaponsfixed					= TRUE
		ENDIF
		
		//get the number of laps
		IF matchType = FMMC_TYPE_RACE
			sJobActivityInfo.m_numLaps = GlobalServerBD_Races.iLaps
			sJobActivityInfo.m_aggregateScore = g_sRC_SB_CoronaOptions.bAggregate
			IF CV2_IS_THIS_JOB_A_SERIES_JOB()
			AND g_iCV2TelemetryLaunchCorona != -1
			AND NOT IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
			AND NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
				sJobActivityInfo.m_StuntLaunchCorona = g_iCV2TelemetryLaunchCorona
				PRINTLN("[TEL] [CV2] sLtsinfo.m_playlistID = g_iCV2TelemetryLaunchCorona = ", sJobActivityInfo.m_StuntLaunchCorona)
			ENDIF
			g_iCV2TelemetryLaunchCorona = -1
			
			sJobActivityInfo.m_totalPoints = GlobalplayerBD_Races[MY_GBD_SLOT()].iTotalPoints
			sJobActivityInfo.m_largeTargetsHit = GlobalplayerBD_Races[MY_GBD_SLOT()].iLargeTargetsHit
			sJobActivityInfo.m_mediumTargetsHit = GlobalplayerBD_Races[MY_GBD_SLOT()].iMediumTargetsHit
			sJobActivityInfo.m_smallTargetsHit = GlobalplayerBD_Races[MY_GBD_SLOT()].iSmallTargetsHit
			sJobActivityInfo.m_tinyTargetsHit = GlobalplayerBD_Races[MY_GBD_SLOT()].iTinyTargetsHit
		ENDIF
		
		
		
		//if we are on a playlist then set the ID
		IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
		OR IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			sJobActivityInfo.m_playlistID		= GET_HASH_KEY(g_sCurrentPlayListDetails.tl31szContentID)			
			//If this is a head to head then set the bool
			IF IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
			OR IS_PLAYLIST_DOING_HEAD_TO_HEAD()
				sJobActivityInfo.m_ishead2head	= TRUE
			ELIF IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
			OR IS_PLAYLIST_DOING_CHALLENGE()	
				sJobActivityInfo.m_isPlaylistChallenge = TRUE
			ENDIF
		ENDIF
		
		
		TEXT_LABEL_23 tlPlaylistIDString
		IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
		OR IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			tlPlaylistIDString = g_sCurrentPlayListDetails.tl31szContentID
		ENDIF
		
		
		
		// mission name
		SC_PRESENCE_ATTR_SET_STRING(mp_mis_str, "")

		// mission instance
		SC_PRESENCE_ATTR_SET_INT(mp_mis_inst, 0)
		
		// mission id
		SC_PRESENCE_ATTR_SET_STRING(mp_mis_id, "")

		// team
		SC_PRESENCE_ATTR_SET_INT(mp_team, 0)	
		//do the rounds telemetry
		IF NOT IS_PLAYER_USING_ARENA() 
			IF IS_THIS_A_ROUNDS_MISSION()
			OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			OR bForceRoundsEnd
				JobLtsInfo sLtsinfo	
				sLtsinfo.m_roundsset = g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds
				sLtsinfo.m_roundsplayed = g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed
				g_sTransitionSessionData.sMissionRoundData.bNeedToCallMatchEnd = FALSE
				sLtsinfo.m_Cash = g_iMyMatchCashEarnings
				sLtsinfo.m_BetCash = g_iMyBetWinnings
				IF sLtsinfo.m_BetCash <0
					sLtsinfo.m_BetCash = 0
				ENDIF
				sLtsinfo.m_CashStart = g_iMyStartMatchCash
				sLtsinfo.m_CashEnd = (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE())
				sLtsinfo.m_MissionLaunch = sJobActivityInfo.m_MissionLaunch
				
				IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
					sLtsinfo.m_playlistHashMac = g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash
					sLtsinfo.m_playlistPosixTime = g_TransitionSessionNonResetVars.sJobChain.iPosixtime+1
				ELSE
					sLtsinfo.m_1stHashMac = g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash
					sLtsinfo.m_1stPosixTime = g_TransitionSessionNonResetVars.sJobChain.iPosixtime
				ENDIF		
				sLtsinfo.m_XP = GET_PLAYER_FM_XP(PLAYER_ID())		- g_iMyStartMatchXP
				sLtsinfo.m_HighestKillStreak = highestKillStreak
				sLtsinfo.m_Kills = kills
				sLtsinfo.m_Deaths = deaths
				sLtsinfo.m_Suicides = suicides
				sLtsinfo.m_Rank = iPositionInLB
				sLtsinfo.m_CrewId = sNETWORK_CLAN_DESC.Id	
				sLtsinfo.m_betWon = g_iMyWinningBet
				sLtsinfo.m_TeamId = teamId
				sLtsinfo.m_matchResult = iPassFailStatus
				sLtsinfo.m_jpEarned = GET_JOB_POINTS_LOCAL() - g_sJobPoints.iJobPointsStart
			
				IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
				OR IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
					sLtsinfo.m_playlistID = GET_HASH_KEY(g_sCurrentPlayListDetails.tl31szContentID)
					sLtsinfo.m_ishead2head = IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD() OR IS_PLAYLIST_DOING_HEAD_TO_HEAD()
					sLtsinfo.m_isPlaylistChallenge = IS_THIS_TRANSITION_SESSION_A_CHALLENGE() OR IS_PLAYLIST_DOING_CHALLENGE()	
				ENDIF
				
				sLtsinfo.m_celebrationanim = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_ANIM_TY)
				sLtsinfo.m_quickplayanim = iCurrentOnFootQuickplayAnim
				sLtsinfo.m_jobVisibilitySetInCorona = g_bjobVisibilitySetInCorona
				sLtsinfo.m_weaponsfixed = IS_JOB_FORCED_WEAPON_ONLY() OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
				sLtsinfo.m_sessionVisible = g_bSessionVisibilitySetInCorona
				sLtsinfo.m_leftInProgress = bQuit
				
				//Check that the data for outfits are valid and mask the value if it isn't 
				IF g_iVersusOutfitStyleSetup >= 0
					sLtsinfo.m_outfitStyle = g_iVersusOutfitStyleSetup
				ELSE
					IF g_iVersusOutfitStyleSetup = - 2
						sLtsinfo.m_outfitStyle = ENUM_TO_INT(OUTFIT_STYLE_NOT_SET)
					ELSE
						sLtsinfo.m_outfitStyle = ENUM_TO_INT(OUTFIT_STYLE_NONE)
					ENDIF
				ENDIF
				IF g_iVersusOutfitStyleChoice >= 0 
					sLtsinfo.m_outfitChoiceType = g_iVersusOutfitStyleChoice
				ELSE
					IF g_iVersusOutfitStyleChoice = -2
						sLtsinfo.m_outfitChoiceType = ENUM_TO_INT(OUTFIT_STYLE_NOT_SET)
					ELSE
						sLtsinfo.m_outfitChoiceType = ENUM_TO_INT(OUTFIT_STYLE_NONE)
					ENDIF
				ENDIF
				
				PRINTLN("[TEL] sLtsinfo.m_roundsset                = ", sLtsinfo.m_roundsset)
				PRINTLN("[TEL] sLtsinfo.m_roundsplayed             = ", sLtsinfo.m_roundsplayed)
				PRINTLN("[TEL] sLtsinfo.m_outfitStyle              = ", sLtsinfo.m_outfitStyle)			
				PRINTLN("[TEL] sLtsinfo.m_outfitChoiceType         = ", sLtsinfo.m_outfitChoiceType)	
				PRINTLN("[TEL] sLtsinfo.m_Cash                     = ", sLtsinfo.m_Cash)
				PRINTLN("[TEL] sLtsinfo.m_BetCash                  = ", sLtsinfo.m_BetCash)
				PRINTLN("[TEL] sLtsinfo.m_CashStart                = ", sLtsinfo.m_CashStart)
				PRINTLN("[TEL] sLtsinfo.m_CashEnd                  = ", sLtsinfo.m_CashEnd)
				PRINTLN("[TEL] sLtsinfo.m_MissionLaunch            = ", sLtsinfo.m_MissionLaunch)
				PRINTLN("[TEL] sLtsinfo.m_1stHashMac               = ", sLtsinfo.m_1stHashMac)
				PRINTLN("[TEL] sLtsinfo.m_1stPosixTime             = ", sLtsinfo.m_1stPosixTime)			
				PRINTLN("[TEL] sLtsinfo.m_playlistHashMac          = ", sLtsinfo.m_playlistHashMac)
				PRINTLN("[TEL] sLtsinfo.m_playlistPosixTime        = ", sLtsinfo.m_playlistPosixTime)			
				PRINTLN("[TEL] sLtsinfo.m_XP                       = ", sLtsinfo.m_XP)
				PRINTLN("[TEL] sLtsinfo.m_HighestKillStreak        = ", sLtsinfo.m_HighestKillStreak)
				PRINTLN("[TEL] sLtsinfo.m_Kills                    = ", sLtsinfo.m_Kills)
				PRINTLN("[TEL] sLtsinfo.m_Deaths                   = ", sLtsinfo.m_Deaths)
				PRINTLN("[TEL] sLtsinfo.m_Suicides                 = ", sLtsinfo.m_Suicides)
				PRINTLN("[TEL] sLtsinfo.m_Rank                     = ", sLtsinfo.m_Rank)
				PRINTLN("[TEL] sLtsinfo.m_CrewId                   = ", sLtsinfo.m_CrewId)
				PRINTLN("[TEL] sLtsinfo.m_betWon                   = ", sLtsinfo.m_betWon)
				PRINTLN("[TEL] sLtsinfo.m_TeamId                   = ", sLtsinfo.m_TeamId)
				PRINTLN("[TEL] sLtsinfo.m_matchResult              = ", sLtsinfo.m_matchResult)
				PRINTLN("[TEL] sLtsinfo.m_jpEarned                 = ", sLtsinfo.m_jpEarned)
				PRINTLN("[TEL] sLtsinfo.m_playlistID               = ", sLtsinfo.m_playlistID)
				PRINTLN("[TEL] sLtsinfo.m_celebrationanim          = ", sLtsinfo.m_celebrationanim)
				PRINTLN("[TEL] sLtsinfo.m_quickplayanim            = ", sLtsinfo.m_quickplayanim)
				PRINTLN("[TEL] sLtsinfo.m_ishead2head              = ", sLtsinfo.m_ishead2head)
				PRINTLN("[TEL] sLtsinfo.m_isPlaylistChallenge      = ", sLtsinfo.m_isPlaylistChallenge)
				PRINTLN("[TEL] sLtsinfo.m_jobVisibilitySetInCorona = ", sLtsinfo.m_jobVisibilitySetInCorona)
				PRINTLN("[TEL] sLtsinfo.m_weaponsfixed             = ", sLtsinfo.m_weaponsfixed)
				PRINTLN("[TEL] sLtsinfo.m_sessionVisible           = ", sLtsinfo.m_sessionVisible)
				PRINTLN("[TEL] sLtsinfo.m_leftInProgress           = ", sLtsinfo.m_leftInProgress)		
				TEXT_LABEL_23 stPlayList = ""
				IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
				OR IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
					stPlayList = g_sCurrentPlayListDetails.tl31szContentID 
				ENDIF
				
	//			INT CashEarned = (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE()) - g_iMyStartMatchCash
				
				#IF IS_DEBUG_BUILD
				FILL_DEBUG_OUTPUT_FILE_STRUCT(g_FMMC_STRUCT.tl63MissionName, 
											GET_FMMC_BIT_SET_NAME_FOR_LOGS(matchType),
											g_FMMC_STRUCT.tl31LoadedContentID,
											DEFAULT,
											Time,
											GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID())),
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											g_missionFile_ExternalTracker.TripSkipped,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											DEFAULT,
											g_iMissionCost,
											DEFAULT,
											DEFAULT,
											(sNETWORK_CLAN_DESC.Id != 0))
										
										
										
	//			DEBUG_FILE_REWARD_OUTPUT()	
				#ENDIF
				
				//Add a print for Miguel
				PRINTLN("[TEL] * PLAYSTATS_JOB_LTS_END(", matchCreator, ", ", uniqueMatchId, ", sLtsinfo, ", stPlayList, ")")
				PLAYSTATS_JOB_LTS_END(matchCreator, uniqueMatchId, sLtsinfo, stPlayList)
				CLEAR_FM_JOB_ENTERY_TYPE()
				g_bNeedToCallMatchEndOnQuit = FALSE
				EXIT
				
			ENDIF
		ENDIF
		
		//If we are on a heist then we need to call the heist end of mission
		IF (Is_Player_Currently_On_MP_Heist(PLAYER_ID()) 
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))
		
			//Number of kills
			g_sJobHeistInfo.m_kills = sJobActivityInfo.m_Kills
			//Number of deaths
			g_sJobHeistInfo.m_deaths = sJobActivityInfo.m_Deaths
				g_sJobHeistInfo.m_Cash = g_iMyMatchCashEarnings
			g_sJobHeistInfo.m_CashStart = g_iMyStartMatchCash
			g_sJobHeistInfo.m_CashEnd = (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE())
			g_sJobHeistInfo.m_MissionLaunch = sJobActivityInfo.m_MissionLaunch
			g_sJobHeistInfo.m_XP = GET_PLAYER_FM_XP(PLAYER_ID()) - g_iMyStartMatchXP
			g_sJobHeistInfo.m_Suicides = suicides
			g_sJobHeistInfo.m_Rank = iPositionInLB
			g_sJobHeistInfo.m_CrewId = sNETWORK_CLAN_DESC.Id	
			g_sJobHeistInfo.m_TeamId = teamId
			g_sJobHeistInfo.m_matchResult = iPassFailStatus
			g_sJobHeistInfo.m_jpEarned = GET_JOB_POINTS_LOCAL() - g_sJobPoints.iJobPointsStart
			g_sJobHeistInfo.m_celebrationanim = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_ANIM_TY)
			g_sJobHeistInfo.m_quickplayanim = iCurrentOnFootQuickplayAnim
			g_sJobHeistInfo.m_sessionVisible = g_bSessionVisibilitySetInCorona 
			g_sJobHeistInfo.m_leftInProgress = bQuit
			g_sJobHeistInfo.m_difficulty = iMissionDifficulty		
			//2139396			
			g_sJobHeistInfo.m_heistSessionHashMac = g_HeistTelemetryData.iHeistHashedMac
			g_sJobHeistInfo.m_heistSessionIdPosixTime = g_HeistTelemetryData.iHeistPosixTime
			g_sJobHeistInfo.m_heistRootConstentId = g_HeistTelemetryData.iRootContentIdHash

			IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HEIST_INTRO)
				g_sJobHeistInfo.m_lesterCalled = 1
			ELSE
				g_sJobHeistInfo.m_lesterCalled = 0
			ENDIF
			
			//Print the heist data
			//2139396
			PRINTLN("[TEL] g_sJobHeistInfo.m_heistSessionHashMac      = ", g_sJobHeistInfo.m_heistSessionHashMac)
			PRINTLN("[TEL] g_sJobHeistInfo.m_heistSessionIdPosixTime  = ", g_sJobHeistInfo.m_heistSessionIdPosixTime)
			PRINTLN("[TEL] g_sJobHeistInfo.m_ishost                   = ", g_sJobHeistInfo.m_ishost)
			PRINTLN("[TEL] g_sJobHeistInfo.m_role                     = ", g_sJobHeistInfo.m_role)
			PRINTLN("[TEL] g_sJobHeistInfo.m_cashcutpercentage        = ", g_sJobHeistInfo.m_cashcutpercentage)
			PRINTLN("[TEL] g_sJobHeistInfo.m_kills                    = ", g_sJobHeistInfo.m_kills)
			PRINTLN("[TEL] g_sJobHeistInfo.m_deaths                   = ", g_sJobHeistInfo.m_deaths)
			PRINTLN("[TEL] g_sJobHeistInfo.m_clothes                  = ", g_sJobHeistInfo.m_clothes)
			PRINTLN("[TEL] g_sJobHeistInfo.m_Cash                     = ", g_sJobHeistInfo.m_Cash)
			PRINTLN("[TEL] g_sJobHeistInfo.m_CashStart                = ", g_sJobHeistInfo.m_CashStart)
			PRINTLN("[TEL] g_sJobHeistInfo.m_CashEnd                  = ", g_sJobHeistInfo.m_CashEnd)
			PRINTLN("[TEL] g_sJobHeistInfo.m_MissionLaunch            = ", g_sJobHeistInfo.m_MissionLaunch)
			PRINTLN("[TEL] g_sJobHeistInfo.m_XP                       = ", g_sJobHeistInfo.m_XP)
			PRINTLN("[TEL] g_sJobHeistInfo.m_Suicides                 = ", g_sJobHeistInfo.m_Suicides)
			PRINTLN("[TEL] g_sJobHeistInfo.m_Rank                     = ", g_sJobHeistInfo.m_Rank)
			PRINTLN("[TEL] g_sJobHeistInfo.m_CrewId                   = ", g_sJobHeistInfo.m_CrewId)
			PRINTLN("[TEL] g_sJobHeistInfo.m_TeamId                   = ", g_sJobHeistInfo.m_TeamId)
			PRINTLN("[TEL] g_sJobHeistInfo.m_matchResult              = ", g_sJobHeistInfo.m_matchResult)
			PRINTLN("[TEL] g_sJobHeistInfo.m_jpEarned                 = ", g_sJobHeistInfo.m_jpEarned)
			PRINTLN("[TEL] g_sJobHeistInfo.m_celebrationanim          = ", g_sJobHeistInfo.m_celebrationanim)
			PRINTLN("[TEL] g_sJobHeistInfo.m_quickplayanim            = ", g_sJobHeistInfo.m_quickplayanim)
			PRINTLN("[TEL] g_sJobHeistInfo.m_sessionVisible           = ", g_sJobHeistInfo.m_sessionVisible)
			PRINTLN("[TEL] g_sJobHeistInfo.m_leftInProgress           = ", g_sJobHeistInfo.m_leftInProgress)
			PRINTLN("[TEL] g_sJobHeistInfo.m_leaderscashcutpercentage = ", g_sJobHeistInfo.m_leaderscashcutpercentage)
			PRINTLN("[TEL] g_sJobHeistInfo.m_lesterCalled             = ", g_sJobHeistInfo.m_lesterCalled)
			PRINTLN("[TEL] g_sJobHeistInfo.m_heistRootConstentId      = ", g_sJobHeistInfo.m_heistRootConstentId)
			PRINTLN("[TEL] g_sJobHeistInfo.m_TimePlanningBoard        = ", g_sJobHeistInfo.m_TimePlanningBoard)
			PRINTLN("[TEL] g_sJobHeistInfo.m_outfitChoiceBy           = ", g_sJobHeistInfo.m_outfitChoiceBy)
			PRINTLN("[TEL] g_sJobHeistInfo.m_outfitChoiceType         = ", g_sJobHeistInfo.m_outfitChoiceType)
			PRINTLN("[TEL] g_sJobHeistInfo.m_outfitStyle	          = ", g_sJobHeistInfo.m_outfitStyle)			
			PRINTLN("[TEL] g_sJobHeistInfo.m_difficulty               = ", g_sJobHeistInfo.m_difficulty)
			PRINTLN("[TEL] g_sJobHeistInfo.m_1stperson                = ", g_sJobHeistInfo.m_1stperson)
			PRINTLN("[TEL] g_sJobHeistInfo.m_medal                    = ", g_sJobHeistInfo.m_medal)
			PRINTLN("[TEL] g_sJobHeistInfo.m_teamLivesUsed            = ", g_sJobHeistInfo.m_teamLivesUsed)
			PRINTLN("[TEL] g_sJobHeistInfo.m_failureReason            = ", g_sJobHeistInfo.m_failureReason)
			PRINTLN("[TEL] g_sJobHeistInfo.m_failureRole              = ", g_sJobHeistInfo.m_failureRole)
			PRINTLN("[TEL] g_sJobHeistInfo.m_spookedCops              = ", g_sJobHeistInfo.m_spookedCops)
			PRINTLN("[TEL] g_sJobHeistInfo.m_cashLost                 = ", g_sJobHeistInfo.m_cashLost)
			PRINTLN("[TEL] g_sJobHeistInfo.m_cashPickedUp             = ", g_sJobHeistInfo.m_cashPickedUp)
			PRINTLN("[TEL] g_sJobHeistInfo.m_minigameTimeTaken0       = ", g_sJobHeistInfo.m_minigameTimeTaken0)
			PRINTLN("[TEL] g_sJobHeistInfo.m_minigameNumberOfTimes0   = ", g_sJobHeistInfo.m_minigameNumberOfTimes0)
			PRINTLN("[TEL] g_sJobHeistInfo.m_minigameTimeTaken1       = ", g_sJobHeistInfo.m_minigameTimeTaken1)
			PRINTLN("[TEL] g_sJobHeistInfo.m_minigameNumberOfTimes1   = ", g_sJobHeistInfo.m_minigameNumberOfTimes1)
				
				
//			INT CashEarned = (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE()) - g_iMyStartMatchCash

			#IF IS_DEBUG_BUILD
			FILL_DEBUG_OUTPUT_FILE_STRUCT(g_FMMC_STRUCT.tl63MissionName, 
										GET_FMMC_BIT_SET_NAME_FOR_LOGS(matchType),
										g_FMMC_STRUCT.tl31LoadedContentID,
										DEFAULT,
										Time,
										GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID())),
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_missionFile_ExternalTracker.TripSkipped,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_iMissionCost,
										DEFAULT,
										DEFAULT,
										(sNETWORK_CLAN_DESC.Id != 0))
									
									
									
//			DEBUG_FILE_REWARD_OUTPUT()		
			#ENDIF
				
			//Add a print for Miguel
			PRINTLN("[TEL] * PLAYSTATS_JOB_BEND(", matchCreator, ", ", uniqueMatchId, ", g_sJobHeistInfo)")
			PLAYSTATS_JOB_BEND(matchCreator, uniqueMatchId, g_sJobHeistInfo, tlPlaylistIDString)
			CLEAR_FM_JOB_ENTERY_TYPE()
			g_bNeedToCallMatchEndOnQuit = FALSE
			EXIT
		ENDIF
		
		IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			//Number of kills
			g_sJobGangopsInfo.m_infos.m_kills = sJobActivityInfo.m_Kills
			//Number of deaths
			g_sJobGangopsInfo.m_infos.m_deaths = sJobActivityInfo.m_Deaths
				g_sJobGangopsInfo.m_infos.m_Cash = g_iMyMatchCashEarnings
			g_sJobGangopsInfo.m_infos.m_CashStart = g_iMyStartMatchCash
			g_sJobGangopsInfo.m_infos.m_CashEnd = (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE())
			g_sJobGangopsInfo.m_infos.m_MissionLaunch = sJobActivityInfo.m_MissionLaunch
			g_sJobGangopsInfo.m_infos.m_XP = GET_PLAYER_FM_XP(PLAYER_ID()) - g_iMyStartMatchXP
			g_sJobGangopsInfo.m_infos.m_Suicides = suicides
			g_sJobGangopsInfo.m_infos.m_Rank = iPositionInLB
			g_sJobGangopsInfo.m_infos.m_CrewId = sNETWORK_CLAN_DESC.Id	
			g_sJobGangopsInfo.m_infos.m_TeamId = teamId
			g_sJobGangopsInfo.m_infos.m_matchResult = iPassFailStatus
			g_sJobGangopsInfo.m_infos.m_sessionVisible = g_bSessionVisibilitySetInCorona 
			g_sJobGangopsInfo.m_infos.m_leftInProgress = bQuit
			g_sJobGangopsInfo.m_infos.m_difficulty = iMissionDifficulty		
			g_sJobGangopsInfo.m_infos.m_heistSessionHashMac = g_HeistTelemetryData.iHeistHashedMac
			g_sJobGangopsInfo.m_infos.m_heistSessionIdPosixTime = g_HeistTelemetryData.iHeistPosixTime
			g_sJobGangopsInfo.m_infos.m_heistRootConstentId = GANG_OPS_GET_FINALE_FROM_ROOT_ID()
			
			//Print the Gangops data
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_heistSessionHashMac      = ", g_sJobGangopsInfo.m_infos.m_heistSessionHashMac)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_heistSessionIdPosixTime  = ", g_sJobGangopsInfo.m_infos.m_heistSessionIdPosixTime)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_ishost                   = ", g_sJobGangopsInfo.m_infos.m_ishost)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_role                     = ", g_sJobGangopsInfo.m_infos.m_role)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_cashcutpercentage        = ", g_sJobGangopsInfo.m_infos.m_cashcutpercentage)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_kills                    = ", g_sJobGangopsInfo.m_infos.m_kills)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_deaths                   = ", g_sJobGangopsInfo.m_infos.m_deaths)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_clothes                  = ", g_sJobGangopsInfo.m_infos.m_clothes)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_Cash                     = ", g_sJobGangopsInfo.m_infos.m_Cash)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_CashStart                = ", g_sJobGangopsInfo.m_infos.m_CashStart)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_CashEnd                  = ", g_sJobGangopsInfo.m_infos.m_CashEnd)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_MissionLaunch            = ", g_sJobGangopsInfo.m_infos.m_MissionLaunch)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_XP                       = ", g_sJobGangopsInfo.m_infos.m_XP)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_Suicides                 = ", g_sJobGangopsInfo.m_infos.m_Suicides)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_Rank                     = ", g_sJobGangopsInfo.m_infos.m_Rank)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_CrewId                   = ", g_sJobGangopsInfo.m_infos.m_CrewId)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_TeamId                   = ", g_sJobGangopsInfo.m_infos.m_TeamId)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_matchResult              = ", g_sJobGangopsInfo.m_infos.m_matchResult)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_jpEarned                 = ", g_sJobGangopsInfo.m_infos.m_jpEarned)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_celebrationanim          = ", g_sJobGangopsInfo.m_infos.m_celebrationanim)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_quickplayanim            = ", g_sJobGangopsInfo.m_infos.m_quickplayanim)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_sessionVisible           = ", g_sJobGangopsInfo.m_infos.m_sessionVisible)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_leftInProgress           = ", g_sJobGangopsInfo.m_infos.m_leftInProgress)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage = ", g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_heistRootConstentId      = ", g_sJobGangopsInfo.m_infos.m_heistRootConstentId)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_TimePlanningBoard        = ", g_sJobGangopsInfo.m_infos.m_TimePlanningBoard)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_difficulty               = ", g_sJobGangopsInfo.m_infos.m_difficulty)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_failureReason            = ", g_sJobGangopsInfo.m_infos.m_failureReason)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_failureRole              = ", g_sJobGangopsInfo.m_infos.m_failureRole)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_cashLost                 = ", g_sJobGangopsInfo.m_infos.m_cashLost)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_cashPickedUp             = ", g_sJobGangopsInfo.m_infos.m_cashPickedUp)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_minigameTimeTaken0       = ", g_sJobGangopsInfo.m_infos.m_minigameTimeTaken0)
			PRINTLN("[TEL] g_sJobGangopsInfo.m_infos.m_minigameNumberOfTimes0   = ", g_sJobGangopsInfo.m_infos.m_minigameNumberOfTimes0)
			
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_bossId1 = ", g_sJobGangopsInfo.m_bossId1) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_bossId2 = ", g_sJobGangopsInfo.m_bossId2) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_BossType = ", g_sJobGangopsInfo.m_BossType) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_AttackType = ", g_sJobGangopsInfo.m_AttackType) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_OwnBase = ", g_sJobGangopsInfo.m_OwnBase) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_OwnCannon = ", g_sJobGangopsInfo.m_OwnCannon) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_OwnSecurityRoom = ", g_sJobGangopsInfo.m_OwnSecurityRoom) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_OwnLounge = ", g_sJobGangopsInfo.m_OwnLounge) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_OwnLivingQuarters = ", g_sJobGangopsInfo.m_OwnLivingQuarters) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_OwnTiltRotor = ", g_sJobGangopsInfo.m_OwnTiltRotor) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_OrbitalCannonShots = ", g_sJobGangopsInfo.m_OrbitalCannonShots) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_OrbitalCannonKills = ", g_sJobGangopsInfo.m_OrbitalCannonKills) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_AssasinationLevel1Calls = ", g_sJobGangopsInfo.m_AssasinationLevel1Calls) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_AssasinationLevel2Calls = ", g_sJobGangopsInfo.m_AssasinationLevel2Calls) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_AssasinationLevel3Calls = ", g_sJobGangopsInfo.m_AssasinationLevel3Calls) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_ObserveTargetCalls = ", g_sJobGangopsInfo.m_ObserveTargetCalls) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_PrepCompletionType = ", g_sJobGangopsInfo.m_PrepCompletionType) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_FailedStealth = ", g_sJobGangopsInfo.m_FailedStealth) 
			PRINTLN("[JS][GOTEL][TEL] g_sJobGangopsInfo.m_QuickRestart = ", g_sJobGangopsInfo.m_QuickRestart) 
			
			#IF IS_DEBUG_BUILD
			FILL_DEBUG_OUTPUT_FILE_STRUCT(g_FMMC_STRUCT.tl63MissionName, 
										GET_FMMC_BIT_SET_NAME_FOR_LOGS(matchType),
										g_FMMC_STRUCT.tl31LoadedContentID,
										DEFAULT,
										Time,
										GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID())),
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_missionFile_ExternalTracker.TripSkipped,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_iMissionCost,
										DEFAULT,
										DEFAULT,
										(sNETWORK_CLAN_DESC.Id != 0))
									
									
									
			#ENDIF
			
			PRINTLN("[JS][GOTEL][TEL] - private_DEAL_WITH_FM_MATCH_START_AND_END - PLAYSTATS_INSTANCED_HEIST_ENDED")
			
			PRINTLN("[JS][GOTEL][TEL] - g_sJobGangopsInfo uniqueMatchId = ", uniqueMatchId)
			PLAYSTATS_INSTANCED_HEIST_ENDED(matchCreator, uniqueMatchId, tlPlaylistIDString, g_sJobGangopsInfo)
			CLEAR_FM_JOB_ENTERY_TYPE()
			g_bNeedToCallMatchEndOnQuit = FALSE
			EXIT
		ENDIF
		
		
		
		
		// DO ARENA END
		IF IS_PLAYER_USING_ARENA() 
			
			PRINTLN("[ARENA WARS TEL]  [TEL] ARENA END")
			
		
		
			g_sTransitionSessionData.sMissionRoundData.bNeedToCallMatchEnd = FALSE
		
			g_sArena_Telemetry_data.m_sessionid =         	hashedMac   

			//g_sArena_Telemetry_data.m_cashearned =     		    

			g_sArena_Telemetry_data.m_leftinprogress =  	BOOL_TO_INT(bQuit)     
			g_sArena_Telemetry_data.m_xp =     				GET_PLAYER_FM_XP(PLAYER_ID()) - g_iMyStartMatchXP                  
			g_sArena_Telemetry_data.m_kills =   			Kills            
			g_sArena_Telemetry_data.m_deaths =  			Deaths             
			g_sArena_Telemetry_data.m_suicides = 			suicides            
			g_sArena_Telemetry_data.m_rank =     			GET_PLAYER_RANK(PLAYER_ID()) //iPositionInLB              
			g_sArena_Telemetry_data.m_vehicleid =        	ENUM_TO_INT(nmVehicleId)   // vehicle model? Tools 
			g_sArena_Telemetry_data.m_matchduration =     	Time  

			g_sArena_Telemetry_data.m_result =           	iPassFailStatus
			g_sArena_Telemetry_data.m_skillLevel =           GET_MP_INT_CHARACTER_STAT(MP_STAT_ARENAWARS_SKILL_LEVEL)

			g_sArena_Telemetry_data.m_matchtype =      		matchType      
			g_sArena_Telemetry_data.m_matchname =     	    g_FMMC_STRUCT.iRootContentIDHash



			IF IS_THIS_ARENA_MODE_A_TEAM_MODE()	
				g_sArena_Telemetry_data.m_teamType =  1
			ELSE
				g_sArena_Telemetry_data.m_teamType =  0
			ENDIF
			
			g_sArena_Telemetry_data.m_teamId =              teamid


//			g_sArena_Telemetry_data.m_publiccontentid =  // ??    ID of the ugc job TOOLS
//			g_sArena_Telemetry_data.m_playingtime =          // tools
//			g_sArena_Telemetry_data.m_flagsStolen =           //tools
//			g_sArena_Telemetry_data.m_flagsDelivered =       //tools
//			g_sArena_Telemetry_data.m_totalPoints =        //tools  
//			g_sArena_Telemetry_data.m_goalsScored =        //tools  
//			g_sArena_Telemetry_data.m_suddenDeath =          //tools
//			g_sArena_Telemetry_data.m_winConditions =      //tools  
//			g_sArena_Telemetry_data.m_damageDealt =          //tools
//			g_sArena_Telemetry_data.m_damageReceived =      //tools 
//			g_sArena_Telemetry_data.m_gladiatorKills =       //tools
//			g_sArena_Telemetry_data.m_bombTime =             //tools
//			g_sArena_Telemetry_data.m_spectatorTime =     //tools   
//			g_sArena_Telemetry_data.m_totalTagIns =      //tools    
//			g_sArena_Telemetry_data.m_timeEliminated =      //tools 
//			g_sArena_Telemetry_data.m_endreason =            // tools
//			g_sArena_Telemetry_data.m_killer =              // tools
//			g_sArena_Telemetry_data.m_killervehicle =        //tools
//			g_sArena_Telemetry_data.m_checkpointsCollected =   //tools
//			g_sArena_Telemetry_data.m_bonusTime =            	//tool
//			g_sArena_Telemetry_data.m_finishPosition =       //tools
		
//			g_sArena_Telemetry_data.m_controlTurret =        // Tools (orlando?)

			g_sArena_Telemetry_data.m_lobbyModLivery =       g_iArenaWarModLiveryForTelem
			g_sArena_Telemetry_data.m_lobbyModArmor =        g_iArenaWarModArmourForTelem
			g_sArena_Telemetry_data.m_lobbyModWeapon =       g_iArenaWarModWeaponForTelem
			g_sArena_Telemetry_data.m_lobbyModMine =         g_iArenaWarModMinesForTelem
			g_iArenaWarModLiveryForTelem		= -2
			g_iArenaWarModArmourForTelem		= -2
			g_iArenaWarModWeaponForTelem		= -2
			g_iArenaWarModMinesForTelem			= -2
			g_sArena_Telemetry_data.m_launchmethod = GET_FM_JOB_ENTERY_TYPE()
			g_sArena_Telemetry_data.m_personalVehicle =     PICK_INT(IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID()), 1, 0)
			
			//Print the Gangops data
		
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_sessionid =                " , g_sArena_Telemetry_data.m_sessionid )            
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_publiccontentid =          " , g_sArena_Telemetry_data.m_publiccontentid )      
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_cashearned =               " , g_sArena_Telemetry_data.m_cashearned )           
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_launchmethod =             " , g_sArena_Telemetry_data.m_launchmethod )         
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_leftinprogress =           " , g_sArena_Telemetry_data.m_leftinprogress )       
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_xp =                       " , g_sArena_Telemetry_data.m_xp )                   
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_kills =                    " , g_sArena_Telemetry_data.m_kills )                
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_deaths =                   " , g_sArena_Telemetry_data.m_deaths )               
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_suicides =                 " , g_sArena_Telemetry_data.m_suicides )             
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_rank =                     " , g_sArena_Telemetry_data.m_rank )                 
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_vehicleid =                " , g_sArena_Telemetry_data.m_vehicleid )            
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_matchduration =            " , g_sArena_Telemetry_data.m_matchduration )        
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_playingtime =              " , g_sArena_Telemetry_data.m_playingtime )          
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_result =                   " , g_sArena_Telemetry_data.m_result )               
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_premiumEvent =             " , g_sArena_Telemetry_data.m_premiumEvent )         
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_skillLevel =               " , g_sArena_Telemetry_data.m_skillLevel )           
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_endreason =                " , g_sArena_Telemetry_data.m_endreason )            
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_killer =                   " , g_sArena_Telemetry_data.m_killer )               
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_killervehicle =            " , g_sArena_Telemetry_data.m_killervehicle )        
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_matchtype =                " , g_sArena_Telemetry_data.m_matchtype )            
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_matchname =                " , g_sArena_Telemetry_data.m_matchname )            
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_checkpointsCollected =     " , g_sArena_Telemetry_data.m_checkpointsCollected ) 
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_bonusTime =                " , g_sArena_Telemetry_data.m_bonusTime )            
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_finishPosition =           " , g_sArena_Telemetry_data.m_finishPosition )       
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_teamType =                 " , g_sArena_Telemetry_data.m_teamType )             
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_teamId =                   " , g_sArena_Telemetry_data.m_teamId )               
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_personalVehicle =          " , g_sArena_Telemetry_data.m_personalVehicle )      
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_flagsStolen =              " , g_sArena_Telemetry_data.m_flagsStolen )          
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_flagsDelivered =           " , g_sArena_Telemetry_data.m_flagsDelivered )       
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_totalPoints =              " , g_sArena_Telemetry_data.m_totalPoints )          
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_goalsScored =              " , g_sArena_Telemetry_data.m_goalsScored )          
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_suddenDeath =              " , g_sArena_Telemetry_data.m_suddenDeath )          
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_winConditions =            " , g_sArena_Telemetry_data.m_winConditions )        
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_damageDealt =              " , g_sArena_Telemetry_data.m_damageDealt )          
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_damageReceived =           " , g_sArena_Telemetry_data.m_damageReceived )       
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_gladiatorKills =           " , g_sArena_Telemetry_data.m_gladiatorKills )       
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_bombTime =                 " , g_sArena_Telemetry_data.m_bombTime )             
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_spectatorTime =            " , g_sArena_Telemetry_data.m_spectatorTime )        
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_totalTagIns =              " , g_sArena_Telemetry_data.m_totalTagIns )          
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_timeEliminated =           " , g_sArena_Telemetry_data.m_timeEliminated )       
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_lobbyModLivery =           " , g_sArena_Telemetry_data.m_lobbyModLivery )       
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_lobbyModArmor =            " , g_sArena_Telemetry_data.m_lobbyModArmor )        
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_lobbyModWeapon =           " , g_sArena_Telemetry_data.m_lobbyModWeapon )       
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_lobbyModMine =             " , g_sArena_Telemetry_data.m_lobbyModMine )         
			PRINTLN("[ARENA WARS TEL]  [TEL] g_sArena_Telemetry_data.m_controlTurret =            " , g_sArena_Telemetry_data.m_controlTurret )   
			

			
			#IF IS_DEBUG_BUILD
			FILL_DEBUG_OUTPUT_FILE_STRUCT(g_FMMC_STRUCT.tl63MissionName, 
										GET_FMMC_BIT_SET_NAME_FOR_LOGS(matchType),
										g_FMMC_STRUCT.tl31LoadedContentID,
										DEFAULT,
										Time,
										GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID())),
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_missionFile_ExternalTracker.TripSkipped,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_iMissionCost,
										DEFAULT,
										DEFAULT,
										(sNETWORK_CLAN_DESC.Id != 0))
									
									
									
			#ENDIF
			
			PRINTLN("[JS][GOTEL][ARENA WARS TEL]  [TEL] - private_DEAL_WITH_FM_MATCH_START_AND_END - PLAYSTATS_ARENA_WARS_ENDED")
			
			PRINTLN("[JS][GOTEL][ARENA WARS TEL]  [TEL] - g_sJobGangopsInfo uniqueMatchId = ", uniqueMatchId)
			
			

			
		
			
			CLEAR_FM_JOB_ENTERY_TYPE()
			g_bNeedToCallMatchEndOnQuit = FALSE
			
			//PLAYSTATS_ARENA_WARS_SPECTATOR(g_sArena_Telemetry_data.m_missionId, BOOL_TO_INT(WAS_A_SPECIAL_SPECTATOR_IN_THIS_ROUND()), TRUE, g_sArena_Telemetry_data.m_matchduration, g_sArena_Telemetry_data.m_kills )
			PLAYSTATS_ARENA_WARS_ENDED(g_sArena_Telemetry_data)
			RESET_ARENA_TELEMETRY_DATA_VARS()

			EXIT
		ENDIF
		
		
		
		IF VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash) 
			
			PRINTLN("[CASINO STORY TEL]  [TEL] CASINO END")
			
		
		
			g_sTransitionSessionData.sMissionRoundData.bNeedToCallMatchEnd = FALSE
		

			g_sCasino_Story_Telemetry_data.m_leftinprogress =  		bQuit
			g_sCasino_Story_Telemetry_data.m_xp =     				GET_PLAYER_FM_XP(PLAYER_ID()) - g_iMyStartMatchXP                  
			g_sCasino_Story_Telemetry_data.m_kills =   				Kills            
			g_sCasino_Story_Telemetry_data.m_deaths =  				Deaths             
			g_sCasino_Story_Telemetry_data.m_suicides = 			suicides            
			g_sCasino_Story_Telemetry_data.m_rank =     			GET_PLAYER_RANK(PLAYER_ID()) //iPositionInLB              
	//		g_sCasino_Story_Telemetry_data.m_vehicleid =        	ENUM_TO_INT(nmVehicleId)   // vehicle model? Tools 
	//		g_sCasino_Story_Telemetry_data.m_matchduration =     	Time  

			g_sCasino_Story_Telemetry_data.m_result =           	iPassFailStatus
			
			IF iPassFailStatus = 0
				g_sCasino_Story_Telemetry_data.m_FailureReason = 	0		// url:bugstar:5871904
			ENDIF

		//	g_sCasino_Story_Telemetry_data.m_matchtype =      		matchType      
		//	g_sCasino_Story_Telemetry_data.m_matchname =     	    g_FMMC_STRUCT.iRootContentIDHash
		  //  g_sCasino_Story_Telemetry_data.m_MatchCreator          
		  //  g_sCasino_Story_Telemetry_data.m_PublicContentId       
		  //  g_sCasino_Story_Telemetry_data.m_SessionType           
		  //  g_sCasino_Story_Telemetry_data.m_PlaylistId            



			g_sCasino_Story_Telemetry_data.m_IsSessionVisible = g_bSessionVisibilitySetInCorona
		
	
			 
		    g_sCasino_Story_Telemetry_data.m_Cash        	 = g_iMyMatchCashEarnings              
		    g_sCasino_Story_Telemetry_data.m_CashStart    	 = g_iMyStartMatchCash        
		    g_sCasino_Story_Telemetry_data.m_CashEnd         = (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE())    
		    g_sCasino_Story_Telemetry_data.m_MissionLaunch 	 = sJobActivityInfo.m_MissionLaunch        
		    g_sCasino_Story_Telemetry_data.m_Difficulty      = iMissionDifficulty         
		    g_sCasino_Story_Telemetry_data.m_FirstPerson     =  ENUM_TO_INT(GET_FOLLOW_PED_CAM_VIEW_MODE()  )    
			g_sCasino_Story_Telemetry_data.m_ownPenthouse     =  DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
			g_sCasino_Story_Telemetry_data.m_ownGarage		 = HAS_PLAYER_PURCHASED_CASINO_APARTMENT_GARAGE(PLAYER_ID()) 		
			g_sCasino_Story_Telemetry_data.m_ownOffice        = HAS_PLAYER_PURCHASED_CASINO_APARTMENT_OFFICE(PLAYER_ID())
		    g_sCasino_Story_Telemetry_data.m_LeftInProgress   = bQuit  
		  	g_sCasino_Story_Telemetry_data.m_HeistSessionId    = g_HeistTelemetryData.iHeistPosixTime    
			
		  
		  //  g_sCasino_Story_Telemetry_data.m_Medal                 
		  //  g_sCasino_Story_Telemetry_data.m_TeamLivesUsed         
		  //  g_sCasino_Story_Telemetry_data.m_FailureReason         
		  //  g_sCasino_Story_Telemetry_data.m_UsedQuickRestart      
		  //  g_sCasino_Story_Telemetry_data.m_IsHost                
		    g_sCasino_Story_Telemetry_data.m_IsSessionVisible  		= g_bSessionVisibilitySetInCorona  
		  //  g_sCasino_Story_Telemetry_data.m_SpookedCops           
		  //  g_sCasino_Story_Telemetry_data.m_PlayingTime	= Time          
		    g_sCasino_Story_Telemetry_data.m_CrewId         		= sNETWORK_CLAN_DESC.Id              
		    g_sCasino_Story_Telemetry_data.m_JobPoints 				= GET_JOB_POINTS_LOCAL() - g_sJobPoints.iJobPointsStart            
		    g_sCasino_Story_Telemetry_data.m_UsedVoiceChat         	= NETWORK_HAS_HEADSET() 
		    g_sCasino_Story_Telemetry_data.m_ClosedJob 				= FALSE            
		    g_sCasino_Story_Telemetry_data.m_PrivateJob  			= FALSE   
			
			IF g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY = FM_SESSION_MENU_CHOICE_JOIN_CLOSED_ME_ONLY
			OR DID_I_COME_INTO_A_PRIVATE_SESSION()
		   		 g_sCasino_Story_Telemetry_data.m_FromClosedFreemode = TRUE
			ELSE
				g_sCasino_Story_Telemetry_data.m_FromClosedFreemode = FALSE
			ENDIF
			
		    g_sCasino_Story_Telemetry_data.m_FromPrivateFreemode	= DID_I_COME_INTO_A_PRIVATE_SESSION()
   
		  //  g_sCasino_Story_Telemetry_data.m_BossUUID              
		  //  g_sCasino_Story_Telemetry_data.m_BossType              
		  //  g_sCasino_Story_Telemetry_data.m_FailedStealth         
		  //  g_sCasino_Story_Telemetry_data.m_MissionID             
		  //  g_sCasino_Story_Telemetry_data.m_MissionVariation      
		    g_sCasino_Story_Telemetry_data.m_HouseChipsEarned   	= 0   
		  //  g_sCasino_Story_Telemetry_data.m_RestartPoint          



			//Print the Casino data
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_MatchCreator            " ,		g_sCasino_Story_Telemetry_data.m_MatchCreator          )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_SessionType             " ,		g_sCasino_Story_Telemetry_data.m_SessionType           )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_PlaylistId              " ,		g_sCasino_Story_Telemetry_data.m_PlaylistId            )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_Kills                   " ,		g_sCasino_Story_Telemetry_data.m_Kills                 )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_Deaths                  " ,		g_sCasino_Story_Telemetry_data.m_Deaths                )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_Cash                    " ,		g_sCasino_Story_Telemetry_data.m_Cash                  )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_CashStart               " ,		g_sCasino_Story_Telemetry_data.m_CashStart             )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_CashEnd                 " ,		g_sCasino_Story_Telemetry_data.m_CashEnd               )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_MissionLaunch           " ,		g_sCasino_Story_Telemetry_data.m_MissionLaunch         )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_Difficulty              " ,		g_sCasino_Story_Telemetry_data.m_Difficulty            )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_FirstPerson             " ,		g_sCasino_Story_Telemetry_data.m_FirstPerson           )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_Medal                   " ,		g_sCasino_Story_Telemetry_data.m_Medal                 )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_TeamLivesUsed           " ,		g_sCasino_Story_Telemetry_data.m_TeamLivesUsed         )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_FailureReason           " ,		g_sCasino_Story_Telemetry_data.m_FailureReason         )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_UsedQuickRestart        " ,		g_sCasino_Story_Telemetry_data.m_UsedQuickRestart      )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_IsHost                  " ,		g_sCasino_Story_Telemetry_data.m_IsHost                )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_IsSessionVisible        " ,		g_sCasino_Story_Telemetry_data.m_IsSessionVisible      )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_LeftInProgress          " ,		g_sCasino_Story_Telemetry_data.m_LeftInProgress        )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_SpookedCops             " ,		g_sCasino_Story_Telemetry_data.m_SpookedCops           )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_PlayingTime             " ,		g_sCasino_Story_Telemetry_data.m_PlayingTime           )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_XP                      " ,		g_sCasino_Story_Telemetry_data.m_XP                    )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_Suicides                " ,		g_sCasino_Story_Telemetry_data.m_Suicides              )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_Rank                    " ,		g_sCasino_Story_Telemetry_data.m_Rank                  )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_CrewId                  " ,		g_sCasino_Story_Telemetry_data.m_CrewId                )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_Result                  " ,		g_sCasino_Story_Telemetry_data.m_Result                )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_JobPoints               " ,		g_sCasino_Story_Telemetry_data.m_JobPoints             )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_UsedVoiceChat           " ,		g_sCasino_Story_Telemetry_data.m_UsedVoiceChat         )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_HeistSessionId          " ,		g_sCasino_Story_Telemetry_data.m_HeistSessionId        )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_ClosedJob               " ,		g_sCasino_Story_Telemetry_data.m_ClosedJob             )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_PrivateJob              " ,		g_sCasino_Story_Telemetry_data.m_PrivateJob            )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_FromClosedFreemode      " ,		g_sCasino_Story_Telemetry_data.m_FromClosedFreemode    )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_FromPrivateFreemode     " ,		g_sCasino_Story_Telemetry_data.m_FromPrivateFreemode   )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_BossUUID                " ,		g_sCasino_Story_Telemetry_data.m_BossUUID              )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_BossUUID2                " ,	g_sCasino_Story_Telemetry_data.m_BossUUID2              )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_BossType                " ,		g_sCasino_Story_Telemetry_data.m_BossType              )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_FailedStealth           " ,		g_sCasino_Story_Telemetry_data.m_FailedStealth         )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_MissionID               " ,		g_sCasino_Story_Telemetry_data.m_MissionID             )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_MissionVariation        " ,		g_sCasino_Story_Telemetry_data.m_MissionVariation      )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_OwnPenthouse            " ,		g_sCasino_Story_Telemetry_data.m_OwnPenthouse          )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_OwnGarage               " ,		g_sCasino_Story_Telemetry_data.m_OwnGarage             )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_OwnOffice               " ,		g_sCasino_Story_Telemetry_data.m_OwnOffice             )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_HouseChipsEarned        " ,		g_sCasino_Story_Telemetry_data.m_HouseChipsEarned      )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_RestartPoint            " ,		g_sCasino_Story_Telemetry_data.m_RestartPoint          )
			PRINTLN("[CASINO STORY TEL]  [TEL] g_sCasino_Story_Telemetry_data.m_LaunchedByPlayer        " ,		g_sCasino_Story_Telemetry_data.m_LaunchedByPlayer          )

			#IF IS_DEBUG_BUILD
			FILL_DEBUG_OUTPUT_FILE_STRUCT(g_FMMC_STRUCT.tl63MissionName, 
										GET_FMMC_BIT_SET_NAME_FOR_LOGS(matchType),
										g_FMMC_STRUCT.tl31LoadedContentID,
										DEFAULT,
										Time,
										GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID())),
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_missionFile_ExternalTracker.TripSkipped,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_iMissionCost,
										DEFAULT,
										DEFAULT,
										(sNETWORK_CLAN_DESC.Id != 0))

			#ENDIF
			
			PRINTLN("[JS][GOTEL][CASINO STORY TEL]  [TEL] - private_DEAL_WITH_FM_MATCH_START_AND_END - PLAYSTATS_CASINO_STORY_MISSION_ENDED")
			
			PRINTLN("[JS][GOTEL][CASINO STORY TEL]  [TEL] - g_sJobGangopsInfo uniqueMatchId = ", uniqueMatchId)
			

			CLEAR_FM_JOB_ENTERY_TYPE()
			g_bNeedToCallMatchEndOnQuit = FALSE
			
			//PLAYSTATS_ARENA_WARS_SPECTATOR(g_sCasino_Story_Telemetry_data.m_missionId, BOOL_TO_INT(WAS_A_SPECIAL_SPECTATOR_IN_THIS_ROUND()), TRUE, g_sCasino_Story_Telemetry_data.m_matchduration, g_sCasino_Story_Telemetry_data.m_kills )
			PLAYSTATS_CASINO_STORY_MISSION_ENDED(g_sCasino_Story_Telemetry_data, uniqueMatchId)
			RESET_CASINO_TELEMETRY_DATA_VARS()

			EXIT
		ENDIF

		
		IF CASINO_HEIST_FLOW_IS_A_CASINO_HEIST_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) 
			PRINTLN("[CASINO STORY TEL]  [TEL] CASINO HEIST FINALE END", g_FMMC_STRUCT.iRootContentIDHash)
			
			g_sTransitionSessionData.sMissionRoundData.bNeedToCallMatchEnd = FALSE
			
			
			 g_sCasino_Heist_Finale_Telemetry_data.playthroughId    	  = GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST3_SESSION_ID_POSTIME)         	
    	  	 g_sCasino_Heist_Finale_Telemetry_data.missionId             = g_FMMC_STRUCT.iRootContentIDHash
	    //   g_sCasino_Heist_Finale_Telemetry_data.publicContentId 	   
	         g_sCasino_Heist_Finale_Telemetry_data.playthroughHashMac     = hashedMac
	    //   g_sCasino_Heist_Finale_Telemetry_data.bossId1               
	    //   g_sCasino_Heist_Finale_Telemetry_data.bossId2               
	    //   g_sCasino_Heist_Finale_Telemetry_data.bosstype              
	    //   g_sCasino_Heist_Finale_Telemetry_data.launcherRank          
	    //   g_sCasino_Heist_Finale_Telemetry_data.playerRole            
	    //   g_sCasino_Heist_Finale_Telemetry_data.endingReason          
	    //   g_sCasino_Heist_Finale_Telemetry_data.replay                
	      	 g_sCasino_Heist_Finale_Telemetry_data.rpEarned					= GET_PLAYER_XP(PLAYER_ID()) - g_iMyStartMatchXP			          
	    //   g_sCasino_Heist_Finale_Telemetry_data.difficult             
	    //   g_sCasino_Heist_Finale_Telemetry_data.timeTakenToComplete   
	    //   g_sCasino_Heist_Finale_Telemetry_data.checkpoint            
	    //   g_sCasino_Heist_Finale_Telemetry_data.playCount             
	    //   g_sCasino_Heist_Finale_Telemetry_data.approachBoard         
	    //   g_sCasino_Heist_Finale_Telemetry_data.approachDirect        
	    //   g_sCasino_Heist_Finale_Telemetry_data.wCrew                 
	    //   g_sCasino_Heist_Finale_Telemetry_data.wLoadout              
	    //   g_sCasino_Heist_Finale_Telemetry_data.dCrew                 
	    //   g_sCasino_Heist_Finale_Telemetry_data.vehicleGetaway        
	    //   g_sCasino_Heist_Finale_Telemetry_data.vehicleSwap           
	     //  g_sCasino_Heist_Finale_Telemetry_data.hCrew       				 = sNETWORK_CLAN_DESC.Id           
	    //   g_sCasino_Heist_Finale_Telemetry_data.outfitIn              
	    //   g_sCasino_Heist_Finale_Telemetry_data.outfitOut             
	    //   g_sCasino_Heist_Finale_Telemetry_data.mask                  
	    //   g_sCasino_Heist_Finale_Telemetry_data.vehicleSwapped        
	    //   g_sCasino_Heist_Finale_Telemetry_data.useEMP                
	    //   g_sCasino_Heist_Finale_Telemetry_data.useDrone              
	    //   g_sCasino_Heist_Finale_Telemetry_data.useThermite           
	    //   g_sCasino_Heist_Finale_Telemetry_data.useKeycard            
	    //   g_sCasino_Heist_Finale_Telemetry_data.hack                  
	    //   g_sCasino_Heist_Finale_Telemetry_data.cameras               
	    //   g_sCasino_Heist_Finale_Telemetry_data.accessPoints          
	    //   g_sCasino_Heist_Finale_Telemetry_data.vaultTarget           
	    //   g_sCasino_Heist_Finale_Telemetry_data.vaultAmt              
	    //   g_sCasino_Heist_Finale_Telemetry_data.dailyCashRoomAmt      
	    //   g_sCasino_Heist_Finale_Telemetry_data.depositBoxAmt         
	    //   g_sCasino_Heist_Finale_Telemetry_data.percentage            
	    //   g_sCasino_Heist_Finale_Telemetry_data.deaths                
	    //   g_sCasino_Heist_Finale_Telemetry_data.targetsKilled         
	    //   g_sCasino_Heist_Finale_Telemetry_data.innocentsKilled       
	    //   g_sCasino_Heist_Finale_Telemetry_data.buyerLocation         
				
			#IF IS_DEBUG_BUILD
			FILL_DEBUG_OUTPUT_FILE_STRUCT(g_FMMC_STRUCT.tl63MissionName, 
										GET_FMMC_BIT_SET_NAME_FOR_LOGS(matchType),
										g_FMMC_STRUCT.tl31LoadedContentID,
										DEFAULT,
										Time,
										GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID())),
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_missionFile_ExternalTracker.TripSkipped,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										DEFAULT,
										g_iMissionCost,
										DEFAULT,
										DEFAULT,
										(sNETWORK_CLAN_DESC.Id != 0))

			#ENDIF
			
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.playthroughId          = " , g_sCasino_Heist_Finale_Telemetry_data.playthroughId        			)
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.missionId              = " , g_sCasino_Heist_Finale_Telemetry_data.missionId                      )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.publicContentId 	   = " , g_sCasino_Heist_Finale_Telemetry_data.publicContentId 	            )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.playthroughHashMac     = " , g_sCasino_Heist_Finale_Telemetry_data.playthroughHashMac             )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.bossId1                = " , g_sCasino_Heist_Finale_Telemetry_data.bossId1                        )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.bossId2                = " , g_sCasino_Heist_Finale_Telemetry_data.bossId2                        )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.bosstype               = " , g_sCasino_Heist_Finale_Telemetry_data.bosstype                       )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.launcherRank           = " , g_sCasino_Heist_Finale_Telemetry_data.launcherRank                   )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.playerRole             = " , g_sCasino_Heist_Finale_Telemetry_data.playerRole                     )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.endingReason           = " , g_sCasino_Heist_Finale_Telemetry_data.endingReason                   )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.replay                 = " , g_sCasino_Heist_Finale_Telemetry_data.replay                         )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.rpEarned               = " , g_sCasino_Heist_Finale_Telemetry_data.rpEarned                       )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.difficult              = " , g_sCasino_Heist_Finale_Telemetry_data.difficult                      )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.timeTakenToComplete    = " , g_sCasino_Heist_Finale_Telemetry_data.timeTakenToComplete            )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.checkpoint             = " , g_sCasino_Heist_Finale_Telemetry_data.checkpoint                     )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.playCount              = " , g_sCasino_Heist_Finale_Telemetry_data.playCount                      )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.approachBoard          = " , g_sCasino_Heist_Finale_Telemetry_data.approachBoard                  )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.approachDirect         = " , g_sCasino_Heist_Finale_Telemetry_data.approachDirect                 )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.wCrew                  = " , g_sCasino_Heist_Finale_Telemetry_data.wCrew                          )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.wLoadout               = " , g_sCasino_Heist_Finale_Telemetry_data.wLoadout                       )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.dCrew                  = " , g_sCasino_Heist_Finale_Telemetry_data.dCrew                          )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.vehicleGetaway         = " , g_sCasino_Heist_Finale_Telemetry_data.vehicleGetaway                 )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.vehicleSwap            = " , g_sCasino_Heist_Finale_Telemetry_data.vehicleSwap                    )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.hCrew                  = " , g_sCasino_Heist_Finale_Telemetry_data.hCrew                          )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.outfitIn               = " , g_sCasino_Heist_Finale_Telemetry_data.outfitIn                       )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.outfitOut              = " , g_sCasino_Heist_Finale_Telemetry_data.outfitOut                      )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.mask                   = " , g_sCasino_Heist_Finale_Telemetry_data.mask                           )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.vehicleSwapped         = " , g_sCasino_Heist_Finale_Telemetry_data.vehicleSwapped                 )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.useEMP                 = " , g_sCasino_Heist_Finale_Telemetry_data.useEMP                         )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.useDrone               = " , g_sCasino_Heist_Finale_Telemetry_data.useDrone                       )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.useThermite            = " , g_sCasino_Heist_Finale_Telemetry_data.useThermite                    )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.useKeycard             = " , g_sCasino_Heist_Finale_Telemetry_data.useKeycard                     )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.hack                   = " , g_sCasino_Heist_Finale_Telemetry_data.hack                           )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.cameras                = " , g_sCasino_Heist_Finale_Telemetry_data.cameras                        )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.accessPoints           = " , g_sCasino_Heist_Finale_Telemetry_data.accessPoints                   )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.vaultTarget            = " , g_sCasino_Heist_Finale_Telemetry_data.vaultTarget                    )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.vaultAmt               = " , g_sCasino_Heist_Finale_Telemetry_data.vaultAmt                       )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.dailyCashRoomAmt       = " , g_sCasino_Heist_Finale_Telemetry_data.dailyCashRoomAmt               )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.depositBoxAmt          = " , g_sCasino_Heist_Finale_Telemetry_data.depositBoxAmt                  )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.percentage             = " , g_sCasino_Heist_Finale_Telemetry_data.percentage                     )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.deaths                 = " , g_sCasino_Heist_Finale_Telemetry_data.deaths                         )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.targetsKilled          = " , g_sCasino_Heist_Finale_Telemetry_data.targetsKilled                  )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.innocentsKilled        = " , g_sCasino_Heist_Finale_Telemetry_data.innocentsKilled                )
			PRINTLN("[CASINO HEIST FINALE TEL]  [TEL] g_sCasino_Heist_Finale_Telemetry_data.buyerLocation          = " , g_sCasino_Heist_Finale_Telemetry_data.buyerLocation                  )				
			PRINTLN("[JS][GOTEL][CASINO HEIST FINALE TEL]  [TEL] - g_sCasino_Story_Telemetry_data uniqueMatchId = ", uniqueMatchId)
			
			
			
			
			PLAYSTATS_HEIST3_FINALE(g_sCasino_Heist_Finale_Telemetry_data)
			CLEAR_FM_JOB_ENTERY_TYPE()
			g_bNeedToCallMatchEndOnQuit = FALSE
		ENDIF
		
		IF HEIST_ISLAND_FLOW_IS_A_HEIST_ISLAND_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) 
			PRINTLN("[Island Heist TEL]  [TEL] Island END")
			g_sTransitionSessionData.sMissionRoundData.bNeedToCallMatchEnd = FALSE
		
		
		
			g_sIsland_Heist_Finale_Telemetry_data.playthroughId	= GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST4_SESSION_ID_POSTIME)    	
		
			g_sIsland_Heist_Finale_Telemetry_data.publicContentId = GET_HASH_KEY(g_FMMC_STRUCT.tl31LoadedContentID)
			
			g_sIsland_Heist_Finale_Telemetry_data.hashmac       = hashedMac
			
			g_sIsland_Heist_Finale_Telemetry_data.rpEarned		= GET_PLAYER_XP(PLAYER_ID()) - g_iMyStartMatchXP
		
		
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.missionId            = " ,g_sIsland_Heist_Finale_Telemetry_data.missionId            )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.sessionId            = " ,g_sIsland_Heist_Finale_Telemetry_data.sessionId            )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.publicContentId  	 = " ,g_sIsland_Heist_Finale_Telemetry_data.publicContentId  	 )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.hashmac              = " ,g_sIsland_Heist_Finale_Telemetry_data.hashmac              )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.bossId1              = " ,g_sIsland_Heist_Finale_Telemetry_data.bossId1              )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.bossId2              = " ,g_sIsland_Heist_Finale_Telemetry_data.bossId2              )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.bosstype             = " ,g_sIsland_Heist_Finale_Telemetry_data.bosstype             )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.launcherRank         = " ,g_sIsland_Heist_Finale_Telemetry_data.launcherRank         )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.playerRole           = " ,g_sIsland_Heist_Finale_Telemetry_data.playerRole           )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.endingReason         = " ,g_sIsland_Heist_Finale_Telemetry_data.endingReason         )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.replay               = " ,g_sIsland_Heist_Finale_Telemetry_data.replay               )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.rpEarned             = " ,g_sIsland_Heist_Finale_Telemetry_data.rpEarned             )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.difficult            = " ,g_sIsland_Heist_Finale_Telemetry_data.difficult            )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.timeTakenToComplete  = " ,g_sIsland_Heist_Finale_Telemetry_data.timeTakenToComplete  )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.checkpoint           = " ,g_sIsland_Heist_Finale_Telemetry_data.checkpoint           )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.playCount            = " ,g_sIsland_Heist_Finale_Telemetry_data.playCount            )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.deaths               = " ,g_sIsland_Heist_Finale_Telemetry_data.deaths               )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.targetsKilled        = " ,g_sIsland_Heist_Finale_Telemetry_data.targetsKilled        )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.innocentsKilled      = " ,g_sIsland_Heist_Finale_Telemetry_data.innocentsKilled      )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.properties           = " ,g_sIsland_Heist_Finale_Telemetry_data.properties           )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.percentage           = " ,g_sIsland_Heist_Finale_Telemetry_data.percentage           )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.tod                  = " ,g_sIsland_Heist_Finale_Telemetry_data.tod                  )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.wLoadout             = " ,g_sIsland_Heist_Finale_Telemetry_data.wLoadout             )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.outfits              = " ,g_sIsland_Heist_Finale_Telemetry_data.outfits              )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.suppressors          = " ,g_sIsland_Heist_Finale_Telemetry_data.suppressors          )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.supCrewBoard         = " ,g_sIsland_Heist_Finale_Telemetry_data.supCrewBoard         )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.islApproach          = " ,g_sIsland_Heist_Finale_Telemetry_data.islApproach          )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.islEscBoard          = " ,g_sIsland_Heist_Finale_Telemetry_data.islEscBoard          )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.primTar              = " ,g_sIsland_Heist_Finale_Telemetry_data.primTar              )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.weather              = " ,g_sIsland_Heist_Finale_Telemetry_data.weather              )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.weapDisrup           = " ,g_sIsland_Heist_Finale_Telemetry_data.weapDisrup           )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.supCrewActual        = " ,g_sIsland_Heist_Finale_Telemetry_data.supCrewActual        )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.miscActions          = " ,g_sIsland_Heist_Finale_Telemetry_data.miscActions          )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.compEntrance         = " ,g_sIsland_Heist_Finale_Telemetry_data.compEntrance         )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.primTarEntrance      = " ,g_sIsland_Heist_Finale_Telemetry_data.primTarEntrance      )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.compExit             = " ,g_sIsland_Heist_Finale_Telemetry_data.compExit             )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.interestItemUsed     = " ,g_sIsland_Heist_Finale_Telemetry_data.interestItemUsed     )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.islEscActual         = " ,g_sIsland_Heist_Finale_Telemetry_data.islEscActual         )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.failedStealth        = " ,g_sIsland_Heist_Finale_Telemetry_data.failedStealth        )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.specCam              = " ,g_sIsland_Heist_Finale_Telemetry_data.specCam              )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.moneyEarned          = " ,g_sIsland_Heist_Finale_Telemetry_data.moneyEarned          )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.secLoot              = " ,g_sIsland_Heist_Finale_Telemetry_data.secLoot              )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.secTarCash           = " ,g_sIsland_Heist_Finale_Telemetry_data.secTarCash           )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.secTarCocaine        = " ,g_sIsland_Heist_Finale_Telemetry_data.secTarCocaine        )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.secTarWeed           = " ,g_sIsland_Heist_Finale_Telemetry_data.secTarWeed           )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.secTarGold           = " ,g_sIsland_Heist_Finale_Telemetry_data.secTarGold           )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.secTarPaint          = " ,g_sIsland_Heist_Finale_Telemetry_data.secTarPaint          )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.bagCapacity          = " ,g_sIsland_Heist_Finale_Telemetry_data.bagCapacity          )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.properties2          = " ,g_sIsland_Heist_Finale_Telemetry_data.properties2          )
			
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.secTarCash     = " ,g_sIsland_Heist_Finale_Telemetry_data.secTarCash        )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.secTarCocaine  = " ,g_sIsland_Heist_Finale_Telemetry_data.secTarCocaine     )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.secTarWeed     = " ,g_sIsland_Heist_Finale_Telemetry_data.secTarWeed        )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.secTarGold     = " ,g_sIsland_Heist_Finale_Telemetry_data.secTarGold        )
			PRINTLN("[ISLAND HEIST FINALE TEL]  [TEL] g_sIsland_Heist_Finale_Telemetry_data.secTarPaint    = " ,g_sIsland_Heist_Finale_Telemetry_data.secTarPaint       )
					
					
			
			PLAYSTATS_HEIST4_FINALE(g_sIsland_Heist_Finale_Telemetry_data)
			CLEAR_FM_JOB_ENTERY_TYPE()
			g_bNeedToCallMatchEndOnQuit = FALSE
		ENDIF
		
		
		IF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)
		OR (IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash) )
		OR (IS_THIS_A_FIXER_SHORT_TRIP() AND IS_CORONA_BIT_SET(CORONA_FIXER_SHORT_TRIP_INTRO))	
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] Tuner Finale END")
			
			g_sTransitionSessionData.sMissionRoundData.bNeedToCallMatchEnd = FALSE
		
			g_sTuner_Robbery_Finale_Telemetry_data.playthroughId		= g_HeistTelemetryData.iHeistPosixTime
			g_sTuner_Robbery_Finale_Telemetry_data.missionId     		= g_FMMC_STRUCT.iRootContentIDHash
			g_sTuner_Robbery_Finale_Telemetry_data.sessionID			= g_HeistTelemetryData.iHeistPosixTime
			IF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)
				g_sTuner_Robbery_Finale_Telemetry_data.strandID			= ENUM_TO_INT(GET_TUNER_ROBBERY_FINALE_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash))					
			ENDIF
			g_sTuner_Robbery_Finale_Telemetry_data.publicContentId 		= GET_HASH_KEY(g_FMMC_STRUCT.tl31LoadedContentID)
						
			g_sTuner_Robbery_Finale_Telemetry_data.hashmac       		= hashedMac
			g_sTuner_Robbery_Finale_Telemetry_data.rpEarned				= GET_PLAYER_XP(PLAYER_ID()) - g_iMyStartMatchXP
			
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.playthroughID            = ",g_sTuner_Robbery_Finale_Telemetry_data.playthroughID       )     //playthrough ID, this should appear in all related content for a strand, like preps and finale, and would enable us to track an entire strand playthrough
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.missionID                = ",g_sTuner_Robbery_Finale_Telemetry_data.missionID           )     //mission name
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.sessionID                = ",g_sTuner_Robbery_Finale_Telemetry_data.sessionID           )     //session ID
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.strandID                 = ",g_sTuner_Robbery_Finale_Telemetry_data.strandID            )     //active strand
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.publicContentID  		  = ",g_sTuner_Robbery_Finale_Telemetry_data.publicContentID  	)	  //public content ID
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.bossId1                  = ",g_sTuner_Robbery_Finale_Telemetry_data.bossId1             )     //unique ID of the Boss instance
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.bossId2                  = ",g_sTuner_Robbery_Finale_Telemetry_data.bossId2             )     //unique ID of the Boss instance
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.bosstype                 = ",g_sTuner_Robbery_Finale_Telemetry_data.bosstype            )     //type of org: Securoserv, MC, unaffiliated
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.launcherRank             = ",g_sTuner_Robbery_Finale_Telemetry_data.launcherRank        )     //rank of the player launching
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.playerRole               = ",g_sTuner_Robbery_Finale_Telemetry_data.playerRole          )     //similar to previous metrics, the player's role: CEO, bodyguard, biker prospect etc.
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.endingReason             = ",g_sTuner_Robbery_Finale_Telemetry_data.endingReason        )     //ending reason
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.replay                   = ",g_sTuner_Robbery_Finale_Telemetry_data.replay              )     //counts how many times this mission was failed, then continued from checkpoint/restarted; resets after the player exits the mission, null if not applicable
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.rpEarned                 = ",g_sTuner_Robbery_Finale_Telemetry_data.rpEarned            )     //RP earned
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.timeTakenToComplete      = ",g_sTuner_Robbery_Finale_Telemetry_data.timeTakenToComplete )     //time taken to complete, in seconds (for consistency)
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.checkpoint               = ",g_sTuner_Robbery_Finale_Telemetry_data.checkpoint          )     //what checkpoint was the mission failed at; null if not applicable
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.deaths                   = ",g_sTuner_Robbery_Finale_Telemetry_data.deaths              )     //how many times the player died
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.targetsKilled            = ",g_sTuner_Robbery_Finale_Telemetry_data.targetsKilled       )     //number of mission critical/aggressive peds killed
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.innocentsKilled          = ",g_sTuner_Robbery_Finale_Telemetry_data.innocentsKilled     )     //number of innocent peds killed
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.properties               = ",g_sTuner_Robbery_Finale_Telemetry_data.properties          )     //existing bitset of type of properties owned: casino penthouse, gunrunning bunker, smuggler hangar, terrorbyte, submarine etc.
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.properties2              = ",g_sTuner_Robbery_Finale_Telemetry_data.properties2         )     //bitset of type of properties owned; future proofing
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.failedStealth            = ",g_sTuner_Robbery_Finale_Telemetry_data.failedStealth       )    //whether the player failed stealth and alerted the enemies
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.wLoadout                 = ",g_sTuner_Robbery_Finale_Telemetry_data.wLoadout            )     //weapon loadout the player has picked on the board
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.outfits                  = ",g_sTuner_Robbery_Finale_Telemetry_data.outfits             )     //outfits chosen on the board
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.moneyEarned              = ",g_sTuner_Robbery_Finale_Telemetry_data.moneyEarned         )     //total amount of money earned from the heist
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.vehicleBoard             = ",g_sTuner_Robbery_Finale_Telemetry_data.vehicleBoard        )     //hash, car chosen on the planning board
			PRINTLN("[TUNER ROBBERY FINALE TEL]  [TEL] g_sTuner_Robbery_Finale_Telemetry_data.hashmac                  = ",g_sTuner_Robbery_Finale_Telemetry_data.hashmac             )     //hash mac
					
			PLAYSTATS_ROBBERY_FINALE(g_sTuner_Robbery_Finale_Telemetry_data)
			CLEAR_FM_JOB_ENTERY_TYPE()
			g_bNeedToCallMatchEndOnQuit = FALSE
		ENDIF
		
		
		IF IS_CONTENT_AFTER_DLC(ciDLC_FIXER)
		OR IS_THIS_A_FIXER_SHORT_TRIP() 	
			PRINTLN("[INSTANCED MISSION END TEL]  END")
			g_sTransitionSessionData.sMissionRoundData.bNeedToCallMatchEnd = FALSE
			
			
			g_sInstanced_Mission_End_Telemetry_data.rpEarned  		=  GET_PLAYER_XP(PLAYER_ID()) - g_iMyStartMatchXP
			g_sInstanced_Mission_End_Telemetry_data.publicContentID = GET_HASH_KEY(g_FMMC_STRUCT.tl31LoadedContentID)
			
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.groupID             = ", g_sInstanced_Mission_End_Telemetry_data.groupID            ) // group ID; this would enable us to track those players who play this mission together
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.missionCategory     = ", g_sInstanced_Mission_End_Telemetry_data.missionCategory    ) // which set of missions these are; for now, Franklin and Lamar short trips
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.missionName         = ", g_sInstanced_Mission_End_Telemetry_data.missionName        ) // mission name
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.sessionID           = ", g_sInstanced_Mission_End_Telemetry_data.sessionID          ) // session ID
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.type                = ", g_sInstanced_Mission_End_Telemetry_data.type               ) // type of mission
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.location            = ", g_sInstanced_Mission_End_Telemetry_data.location           ) // mission variation
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.launchMethod        = ", g_sInstanced_Mission_End_Telemetry_data.launchMethod       ) // launch method
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.publicContentID 	= ", g_sInstanced_Mission_End_Telemetry_data.publicContentID    ) // public content ID
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.bossId1             = ", g_sInstanced_Mission_End_Telemetry_data.bossId1            ) // unique ID of the Boss instance
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.bossId2             = ", g_sInstanced_Mission_End_Telemetry_data.bossId2            ) // unique ID of the Boss instance
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.bosstype            = ", g_sInstanced_Mission_End_Telemetry_data.bosstype           ) // type of org: Securoserv, MC, unaffiliated
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.launcherRank        = ", g_sInstanced_Mission_End_Telemetry_data.launcherRank       ) // rank of the player launching
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.playerRole          = ", g_sInstanced_Mission_End_Telemetry_data.playerRole         ) // similar to previous metrics, the player's role: CEO, bodyguard, biker prospect etc.
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.timeTakenToComplete = ", g_sInstanced_Mission_End_Telemetry_data.timeTakenToComplete) // time taken to complete, in seconds (for consistency)
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.timeLimit           = ", g_sInstanced_Mission_End_Telemetry_data.timeLimit          ) // time limit, in seconds (for consistency); null if not applicable
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.won                 = ", g_sInstanced_Mission_End_Telemetry_data.won                ) // whether the mission was completed successfully
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.endingReason        = ", g_sInstanced_Mission_End_Telemetry_data.endingReason       ) // ending reason
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.cashEarned          = ", g_sInstanced_Mission_End_Telemetry_data.cashEarned         ) // money earned
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.rpEarned            = ", g_sInstanced_Mission_End_Telemetry_data.rpEarned           ) // RP earned
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.deaths              = ", g_sInstanced_Mission_End_Telemetry_data.deaths             ) // how many times the player died
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.targetsKilled       = ", g_sInstanced_Mission_End_Telemetry_data.targetsKilled      ) // number of mission critical/aggressive peds killed
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.innocentsKilled     = ", g_sInstanced_Mission_End_Telemetry_data.innocentsKilled    ) // number of innocent peds killed
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.properties          = ", g_sInstanced_Mission_End_Telemetry_data.properties         ) // existing bitset of type of properties owned: casino penthouse, gunrunning bunker, smuggler hangar, terrorbyte, submarine etc.
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.properties2         = ", g_sInstanced_Mission_End_Telemetry_data.properties2        ) // bitset of type of properties owned; future proofing
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.failedStealth       = ", g_sInstanced_Mission_End_Telemetry_data.failedStealth      ) // whether the player failed stealth and alerted the enemies
			PRINTLN("[INSTANCED MISSION END TEL]  [TEL] g_sInstanced_Mission_End_Telemetry_data.difficulty          = ", g_sInstanced_Mission_End_Telemetry_data.difficulty         ) // mission difficulty
			
			PLAYSTATS_INST_MISSION_END(g_sInstanced_Mission_End_Telemetry_data) 
			CLEAR_FM_JOB_ENTERY_TYPE()
			g_bNeedToCallMatchEndOnQuit = FALSE
		ENDIF
		
		

		//Print it all out for the default
		PRINTLN("[TEL] Integer Values - PLAYSTATS_JOB_ACTIVITY_ENDED")
		PRINTLN("[TEL] sJobActivityInfo.m_Cash                     = ", g_iMyMatchCashEarnings)
		PRINTLN("[TEL] sJobActivityInfo.m_aggregateScore           = ", sJobActivityInfo.m_aggregateScore)
		PRINTLN("[TEL] sJobActivityInfo.m_BetCash                  = ", g_iMyBetWinnings)
		PRINTLN("[TEL] sJobActivityInfo.m_CashStart                = ", g_iMyStartMatchCash)
		PRINTLN("[TEL] sJobActivityInfo.m_CashEnd                  = ", (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE()))
		PRINTLN("[TEL] Boolean  Values")					        
		PRINTLN("[TEL] sJobActivityInfo.m_isTeamDeathmatch         = ", isTeamDeathmatch)
		PRINTLN("[TEL] sJobActivityInfo.m_leftInProgress           = ", bQuit)
		PRINTLN("[TEL] Unsigned Values (must be >= 0)")	        
		PRINTLN("[TEL] sJobActivityInfo.m_XP                       = ", sJobActivityInfo.m_XP)
		PRINTLN("[TEL] sJobActivityInfo.m_HighestKillStreak        = ", highestKillStreak)
		PRINTLN("[TEL] sJobActivityInfo.m_Kills                    = ", kills)
		PRINTLN("[TEL] sJobActivityInfo.m_Deaths                   = ", deaths)
		PRINTLN("[TEL] sJobActivityInfo.m_Suicides                 = ", suicides)
		PRINTLN("[TEL] sJobActivityInfo.m_Rank                     = ", iPositionInLB)
		PRINTLN("[TEL] sJobActivityInfo.m_CrewId                   = ", sNETWORK_CLAN_DESC.Id)
		PRINTLN("[TEL] sJobActivityInfo.m_VehicleId                = ", ENUM_TO_INT(nmVehicleId))
		PRINTLN("[TEL] sJobActivityInfo.m_betWon                   = ", g_iMyWinningBet)
		PRINTLN("[TEL] sJobActivityInfo.m_survivedWave             = ", iSurvivedWave)
		PRINTLN("[TEL] sJobActivityInfo.m_TeamId                   = ", teamId)
		PRINTLN("[TEL] sJobActivityInfo.m_MatchType                = ", matchType)
		PRINTLN("[TEL] sJobActivityInfo.m_raceType                 = ", sJobActivityInfo.m_raceType)
		PRINTLN("[TEL] sJobActivityInfo.m_raceSubtype              = ", sJobActivityInfo.m_raceSubtype)
		PRINTLN("[TEL] sJobActivityInfo.m_matchResult              = ", iPassFailStatus)
		PRINTLN("[TEL] sJobActivityInfo.m_jpEarned		           = ", sJobActivityInfo.m_jpEarned)
		PRINTLN("[TEL] sJobActivityInfo.m_MissionLaunch	           = ", sJobActivityInfo.m_MissionLaunch)
		PRINTLN("[TEL] sJobActivityInfo.m_numLaps	               = ", sJobActivityInfo.m_numLaps)
		PRINTLN("[TEL] sJobActivityInfo.m_playlistID   	           = ", sJobActivityInfo.m_playlistID)
		PRINTLN("[TEL] sJobActivityInfo.m_ishead2head  	           = ", sJobActivityInfo.m_ishead2head)
		PRINTLN("[TEL] sJobActivityInfo.m_isPlaylistChallenge      = ", sJobActivityInfo.m_isPlaylistChallenge)
		PRINTLN("[TEL] sJobActivityInfo.m_weaponsfixed             = ", sJobActivityInfo.m_weaponsfixed)
		PRINTLN("[TEL] sJobActivityInfo.m_celebrationanim          = ", sJobActivityInfo.m_celebrationanim)
		PRINTLN("[TEL] sJobActivityInfo.m_OriginalHashMac          = ", sJobActivityInfo.m_OriginalHashMac)
		PRINTLN("[TEL] sJobActivityInfo.m_OriginalPosixTime        = ", sJobActivityInfo.m_OriginalPosixTime)
		PRINTLN("[TEL] sJobActivityInfo.m_sessionVisible           = ", sJobActivityInfo.m_sessionVisible)
		PRINTLN("[TEL] sJobActivityInfo.m_jobVisibilitySetInCorona = ", sJobActivityInfo.m_jobVisibilitySetInCorona)
		PRINTLN("[TEL] sJobActivityInfo.m_jobDifficulty            = ", sJobActivityInfo.m_jobDifficulty)
		PRINTLN("[TEL] sJobActivityInfo.m_outfitStyle			   = ", sJobActivityInfo.m_outfitStyle)
		PRINTLN("[TEL] sJobActivityInfo.m_outfitChoiceType         = ", sJobActivityInfo.m_outfitChoiceType)
		PRINTLN("[TEL] sJobActivityInfo.m_playlistHashMac          = ", sJobActivityInfo.m_playlistHashMac)
		PRINTLN("[TEL] sJobActivityInfo.m_playlistPosixTime        = ", sJobActivityInfo.m_playlistPosixTime)
		PRINTLN("[TEL] sJobActivityInfo.m_OriginalHashMac          = ", sJobActivityInfo.m_OriginalHashMac)
		PRINTLN("[TEL] sJobActivityInfo.m_OriginalPosixTime        = ", sJobActivityInfo.m_OriginalPosixTime)
		PRINTLN("[TEL] sJobActivityInfo.m_StuntLaunchCorona        = ", sJobActivityInfo.m_StuntLaunchCorona)


//		INT CashEarned = (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE()) - g_iMyStartMatchCash
		#IF IS_DEBUG_BUILD
		FILL_DEBUG_OUTPUT_FILE_STRUCT(g_FMMC_STRUCT.tl63MissionName, 
									GET_FMMC_BIT_SET_NAME_FOR_LOGS(matchType),
									g_FMMC_STRUCT.tl31LoadedContentID,
									g_missionFile_ExternalTracker.TotalPlayers,
									g_missionFile_ExternalTracker.Time,
									GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID())),
									g_missionFile_ExternalTracker.Checkpoints,
									g_missionFile_ExternalTracker.Dnf,
									g_missionFile_ExternalTracker.CheckpointsPassed,
									g_missionFile_ExternalTracker.FinalPosition,
									g_missionFile_ExternalTracker.TeamGame,
									DEFAULT,
									g_missionFile_ExternalTracker.TeamPlace,
									iSurvivedWave,
									iMissionDifficulty,
									(iPassFailStatus > 0),
									g_FMMC_STRUCT.iRank,
									g_missionFile_ExternalTracker.TripSkipped,
									DEFAULT,
									DEFAULT,
									DEFAULT,
									DEFAULT,
									DEFAULT,
									nmVehicleId,
									DEFAULT,
									g_iMyMatchCashEarnings,
									sJobActivityInfo.m_XP,
									g_missionFile_ExternalTracker.AverageRank,
									g_iMissionCost,
									sJobActivityInfo.m_OriginalPosixTime,
									DEFAULT,
									(sNETWORK_CLAN_DESC.Id != 0))
									
									
									
//		DEBUG_FILE_REWARD_OUTPUT()	
		#ENDIF
				
		g_sTransitionSessionData.sMissionRoundData.bNeedToCallMatchEnd = FALSE
		//default call
		PRINTLN("[TEL] * PLAYSTATS_JOB_ACTIVITY_ENDED(", matchCreator, ", ", uniqueMatchId, ", sJobActivityInfo, ", tlPlaylistIDString, ")")
		PLAYSTATS_JOB_ACTIVITY_END(matchCreator, uniqueMatchId, sJobActivityInfo, tlPlaylistIDString)
		
		CLEAR_FM_JOB_ENTERY_TYPE()
		g_bNeedToCallMatchEndOnQuit = FALSE
	ENDIF
ENDPROC

/// PURPOSE: DEALS WITH TELEMETRY FOR MATCH STARTS
/// PARAMS:
/// 	matchType -  they type of match being started:
///		posixTime - server brodcast data obtained by the script server calling:PLAYSTATS_CREATE_MATCH_HISTORY_ID()	
PROC DEAL_WITH_FM_MATCH_START(INT matchType, INT posixTime, INT hashedMac, INT iMissionDifficulty = 0, INT iVariation = 0) 			
	//-- 2609551
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		PRINTLN("[TEL] * DEAL_WITH_FM_MATCH_START - EXIT AS IS_PLAYER_ON_BOSSVBOSS_DM")	
		EXIT
	ENDIF
	
	g_iMyStartMatchCash = (GET_PLAYER_FM_CASH(PLAYER_ID()) + NETWORK_GET_VC_BANK_BALANCE())
	g_iMyStartMatchXP	= GET_PLAYER_FM_XP(PLAYER_ID())
	g_iMyBetWinnings 	= 0
	g_iMyWinningBet		= 0
	PRINTLN("[TEL] * DEAL_WITH_FM_MATCH_START")							
	private_DEAL_WITH_FM_MATCH_START_AND_END(matchType, posixTime, hashedMac, TRUE, iVariation, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iMissionDifficulty)			
	IF matchType != FMMC_TYPE_GANGHIDEOUT
		CLEAR_LAST_JOB_PLAYERS()
	ENDIF	
	// Update our betting telemetry
	g_iProcessBetTelemetry = TRUE
ENDPROC								

/// PURPOSE: DEALS WITH TELEMETRY FOR MATCH ENDS
/// PARAMS:						
PROC DEAL_WITH_FM_MATCH_END(INT matchType, 
							INT posixTime, 					//posixTime - server brodcast data obtained by the script server calling:PLAYSTATS_CREATE_MATCH_HISTORY_ID()
							INT iVariation 			= 0,	//The mission variation, only relevent for minigames, MP_MISSION_DATA  MissionData.missionVariation
							INT iPassFailStatus		= 0, 	//The enod of mission status
							INT iPositionInLB		= 0,	//Position in leaderboard	
							INT kills 				= 0,	//The number of Kills
							INT deaths 				= 0,	//The number of Deaths
							INT suicides 			= 0,	//The number of suicides
							INT highestKillStreak 	= 0,	//The Highest kill streak
							INT teamId 				= 0,	//Players Team
							INT iHeadShots			= 0,	//number of Headshots
							MODEL_NAMES nmVehicleId = DUMMY_MODEL_FOR_SCRIPT,
							INT iSurvivedWave		= 0,  	//The vehicle you were in at the end of a race
							BOOL bForceRoundsEnd	= FALSE, 
							INT iMissionDifficulty = 0,
							INT Time = 0,
							BOOL bDnf = FALSE,
							BOOL bTeamMode = FALSE)
	
	//-- 2609551
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		PRINTLN("[TEL] * DEAL_WITH_FM_MATCH_END - EXIT AS IS_PLAYER_ON_BOSSVBOSS_DM")	
		EXIT
	ENDIF
	
	PRINTLN("[TEL] * DEAL_WITH_FM_MATCH_END")			

	CHECK_FRANKIE_SAYS_ACHIEVEMENT()
	private_DEAL_WITH_FM_MATCH_START_AND_END( matchType, posixTime, 0, FALSE, iVariation, kills, deaths, suicides, highestKillStreak, teamId, iPassFailStatus, iPositionInLB, iHeadShots, nmVehicleId, iSurvivedWave, bForceRoundsEnd, iMissionDifficulty, Time, bDnf, bTeamMode)												
	
	
ENDPROC		

FUNC BOOL IS_PED_IN_THEIR_PERSONAL_VEHICLE(PLAYER_INDEX playerid)
	
	VEHICLE_INDEX tempveh
	
	IF IS_NET_PLAYER_OK(playerid)
		IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerid))
			tempveh = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(playerid))
			IF IS_VEHICLE_DRIVEABLE(tempveh)
				IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
					IF DECOR_EXIST_ON(tempveh, "Player_Vehicle")
						IF DECOR_GET_INT(tempveh, "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(playerid)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC	

FUNC PLAYER_INDEX GET_DRIVER_OF_CAR_AS_PLAYER(VEHICLE_INDEX CarID)
	PED_INDEX PedID
	PLAYER_INDEX PlayerID
	IF IS_VEHICLE_DRIVEABLE(CarID)
		IF NOT IS_VEHICLE_SEAT_FREE(CarID, VS_DRIVER )
			PedID = GET_PED_IN_VEHICLE_SEAT(CarID, VS_DRIVER)		
			IF NOT IS_PED_INJURED(PedID)
				PlayerID = 	NETWORK_GET_PLAYER_INDEX_FROM_PED(PedID)
				IF NOT (PlayerID = INVALID_PLAYER_INDEX())
					IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
						RETURN(PlayerID)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN(PlayerID)
ENDFUNC							

FUNC BOOL PLAYER_IS_THE_DRIVER_IN_A_CAR()
	VEHICLE_INDEX PlayerCar
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PlayerCar = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		IF NOT IS_VEHICLE_SEAT_FREE(PlayerCar, VS_DRIVER )
			IF (PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(PlayerCar, VS_DRIVER))
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC								
								
								


// Added by kevin wong XP rewards for playing friends mission creations 21/12/2012
FUNC BOOL IS_THIS_A_RSTAR_ACTIVITY()
    INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
    IF GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idCreator = FMMC_ROCKSTAR_CREATOR_ID
    OR GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
    OR GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idCreator = FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID
			#IF IS_DEBUG_BUILD
				NET_PRINT("\n  IS_THIS_A_ROCKSTAR_ACTIVITY(): TRUE \n ")
			#ENDIF

    	RETURN TRUE
		#IF IS_DEBUG_BUILD
			NET_PRINT("\n  IS_THIS_A_ROCKSTAR_ACTIVITY(): FALSE \n ")
		#ENDIF

    ENDIF
    
	RETURN FALSE    
ENDFUNC
PROC CHECK_AND_REWARD_PLAYING_FRIENDS_CREATION(string currentmissionowner)

	IF NOT IS_THIS_A_RSTAR_ACTIVITY()
	AND GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_TRIGTUT_DONE)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	
		GAMER_HANDLE gamerHandle
		NETWORK_HANDLE_FROM_MEMBER_ID(currentmissionowner, gamerHandle, SIZE_OF(gamerHandle))
		IF NETWORK_IS_HANDLE_VALID(gamerHandle,SIZE_OF(gamerHandle))
			IF NETWORK_IS_FRIEND(gamerHandle)
				//Commented out. Do not give player 
				//GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXPFRIMISS", XPTYPE_SOCIALCLUB, XPCATEGORY_SOCIALCLUB_PLAYED_A_FRIENDS_MISSION,500, 1)
				//PRINT_TICKER_WITH_INT("REWXPFRIMISS", 500)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("\n  CHECK_AND_REWARD_FRIENDS_CREATION(): Given award \n ")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("\n  CHECK_AND_REWARD_FRIENDS_CREATION(): NETWORK_IS_FRIEND FALSE ")
				#ENDIF
			
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC
///
///    UPDATES THE END OF MISSION STATUS
///    INT iPassFailStatus 
///    				ciFMMC_END_OF_MISSION_STATUS_PASSED	
///    				ciFMMC_END_OF_MISSION_STATUS_FAILED	
///    				ciFMMC_END_OF_MISSION_STATUS_CANCELLED	
///    				ciFMMC_END_OF_MISSION_STATUS_NOT_SET	
PROC UPDATE_FMMC_END_OF_MISSION_STATUS(INT iPassFailStatus, INT iMissionType)
	
	#IF IS_DEBUG_BUILD
	PRINTLN("UPDATE_FMMC_END_OF_MISSION_STATUS")
	IF iMissionType = FMMC_TYPE_INVALID 
		g_b_has_player_accepted_game_invite = FALSE
		EXIT
	ENDIF
	#ENDIF
	
	SWITCH iPassFailStatus
		CASE ciFMMC_END_OF_MISSION_STATUS_NOT_SET
			PRINTLN("iPassFailStatus = ciFMMC_END_OF_MISSION_STATUS_NOT_SET")
			//PLAYSTATS_MISSION_CANCELLED(GET_MISSION_NAME_FROM_TYPE(iMissionType))
			SCRIPT_PLAYSTATS_MISSION_OVER(GET_THIS_SCRIPT_NAME(),0,0,FALSE,TRUE)
		BREAK
		
		CASE ciFMMC_END_OF_MISSION_STATUS_PASSED
			PRINTLN("iPassFailStatus = ciFMMC_END_OF_MISSION_STATUS_PASSED")
			SCRIPT_PLAYSTATS_MISSION_OVER(GET_MISSION_NAME_FROM_TYPE(iMissionType))
//			// award player 500xp if they accepted an invite for game
//			IF G_B_HAS_PLAYER_ACCEPTED_GAME_INVITE = TRUE
//				GIVE_LOCAL_PLAYER_FM_XP("XPT_ACCPT_INVIT", 500, 1,FALSE, -1, TRUE)
//			ENDIF	
//			
//			IF IS_BIT_SET(MPGlobalsAmbience.firstTimeOnMission.iBS,FIRST_TIME_CONFIRMED)
//				GIVE_LOCAL_PLAYER_FM_XP("XPT_FIRSTTIME", 500, 1,FALSE, -1, TRUE)
//				PRINT_TICKER_WITH_INT("XPT_FIRSTTIME_TICK",500)
//			ENDIF
		BREAK
		
		CASE ciFMMC_END_OF_MISSION_STATUS_FAILED
			PRINTLN("iPassFailStatus = ciFMMC_END_OF_MISSION_STATUS_FAILED")
			//PLAYSTATS_MISSION_FAILED(GET_MISSION_NAME_FROM_TYPE(iMissionType))
			SCRIPT_PLAYSTATS_MISSION_OVER(GET_MISSION_NAME_FROM_TYPE(iMissionType),0,0,TRUE,FALSE)
			
//			IF IS_BIT_SET(MPGlobalsAmbience.firstTimeOnMission.iBS,FIRST_TIME_CONFIRMED)
//				GIVE_LOCAL_PLAYER_FM_XP("XPT_FIRSTTIME", 500, 1,FALSE, -1, TRUE)
//				PRINT_TICKER_WITH_INT("XPT_FIRSTTIME_TICK",500)
//			ENDIF
		BREAK
		
		CASE ciFMMC_END_OF_MISSION_STATUS_CANCELLED
			PRINTLN("iPassFailStatus = ciFMMC_END_OF_MISSION_STATUS_CANCELLED")
			//PLAYSTATS_MISSION_CANCELLED(GET_MISSION_NAME_FROM_TYPE(iMissionType))
			SCRIPT_PLAYSTATS_MISSION_OVER(GET_MISSION_NAME_FROM_TYPE(iMissionType),0,0,FALSE,TRUE)
		BREAK
		
		CASE ciFMMC_END_OF_MISSION_STATUS_OVER
			PRINTLN("iPassFailStatus = ciFMMC_END_OF_MISSION_STATUS_OVER")
			SCRIPT_PLAYSTATS_MISSION_OVER(GET_MISSION_NAME_FROM_TYPE(iMissionType))
//			IF IS_BIT_SET(MPGlobalsAmbience.firstTimeOnMission.iBS,FIRST_TIME_CONFIRMED)
//				GIVE_LOCAL_PLAYER_FM_XP("XPT_FIRSTTIME", 500, 1,FALSE, -1, TRUE)
//				PRINT_TICKER_WITH_INT("XPT_FIRSTTIME_TICK",500)
//			ENDIF
		BREAK
	ENDSWITCH
	g_b_has_player_accepted_game_invite = FALSE
	
ENDPROC

//Set the local players FM decarators
PROC SET_LOCAL_PLAYERS_FM_MISSION_DECORATORS(INT iMissionType, INT iInstance)	
	PRINTLN("SET_LOCAL_PLAYERS_FM_MISSION_DECORATORS")
	PRINTLN("iMissionType = ", iMissionType)
	PRINTLN("iInstance    = ", iInstance)
	IF DECOR_IS_REGISTERED_AS_TYPE("MissionType", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), "MissionType")
			DECOR_SET_INT(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()),"MissionType", iMissionType)
		ENDIF
	ENDIF 
	IF DECOR_IS_REGISTERED_AS_TYPE("MatchId", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), "MatchId")
			DECOR_SET_INT(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()),"MatchId", iInstance)
		ENDIF
	ENDIF 
ENDPROC

//Set the local players team decarator. 
PROC SET_LOCAL_PLAYERS_FM_TEAM_DECORATOR(INT iTeam)	
	PRINTLN("SET_LOCAL_PLAYERS_FM_TEAM_DECORATOR")
	PRINTLN("iTeam = ", iTeam)
	IF DECOR_IS_REGISTERED_AS_TYPE("TeamId", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), "TeamId")
			PRINTLN("SET_LOCAL_PLAYERS_FM_TEAM_DECORATOR - DECOR_SET_INT(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), \"TeamId\", ", iTeam, ")")
			DECOR_SET_INT(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()),"TeamId", iTeam)
		ENDIF
	ENDIF 
ENDPROC

//clean up the mission decarators
PROC CLEAN_UP_FM_MISSION_DECORATORS()	
	PRINTLN("CLEAN_UP_FM_MISSION_DECORATORS()")
	IF DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), "MissionType")
		DECOR_REMOVE(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), "MissionType")
		PRINTLN("DECOR_REMOVE(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), MissionType)")
	ELSE
		PRINTLN("IF DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), MissionType) = FALSE")
	ENDIF
	
	IF DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), "MatchId")
		DECOR_REMOVE(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), "MatchId")
		PRINTLN("DECOR_REMOVE(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), MatchId)")
	ELSE
		PRINTLN("IF DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), MatchId) = FALSE")
	ENDIF
	
	IF DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), "TeamId")
		DECOR_REMOVE(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), "TeamId")
		PRINTLN("DECOR_REMOVE(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), TeamId)")
	ELSE
		PRINTLN("IF DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID()), TeamId) = FALSE")
	ENDIF
ENDPROC

FUNC BOOL DONE_TRADE_IN_PROPERTY_HELP()
	INT iStatInt 
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DONE_TRADE_IN_HELP)	
		iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3)
		IF NOT IS_BIT_SET(iStatInt, biNmh3_DoneTradeInPropHelp)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC SET_DONE_TRADE_IN_PROPERTY_HELP(BOOL bDone = TRUE)
	INT iStatInt 
	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3)
	IF bDone
		SET_BIT(iStatInt, biNmh3_DoneTradeInPropHelp)
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DONE_TRADE_IN_HELP)	
	ELSE
		CLEAR_BIT(iStatInt, biNmh3_DoneTradeInPropHelp)
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DONE_TRADE_IN_HELP)	
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3, iStatInt)
//	REQUEST_SAVE()
ENDPROC

// Check to see if we need to do help text
FUNC BOOL DONE_CORONA_BETTING_HELP()
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4)
	
	IF NOT IS_BIT_SET(iStatInt, biNmh4_BetHelp1)
	OR NOT IS_BIT_SET(iStatInt, biNmh4_BetHelp2)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_DONE_CORONA_BETTING_HELP(BOOL bDone = TRUE)
	INT iStatInt 
	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4)
	IF bDone
		IF NOT IS_BIT_SET(iStatInt, biNmh4_BetHelp1)
			SET_BIT(iStatInt, biNmh4_BetHelp1)
		ELIF NOT IS_BIT_SET(iStatInt, biNmh4_BetHelp2)
			SET_BIT(iStatInt, biNmh4_BetHelp2)
		ENDIF
	ELSE
		IF IS_BIT_SET(iStatInt, biNmh4_BetHelp2)
			CLEAR_BIT(iStatInt, biNmh4_BetHelp2)
		ELIF IS_BIT_SET(iStatInt, biNmh4_BetHelp1)
			CLEAR_BIT(iStatInt, biNmh4_BetHelp1)
		ENDIF
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4, iStatInt)
ENDPROC

// Check to see if we need to do help text
FUNC BOOL DONE_CORONA_NEW_JOB_HELP()
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4)
	
	IF NOT IS_BIT_SET(iStatInt, biNmh4_NewJobHelp1)
	OR NOT IS_BIT_SET(iStatInt, biNmh4_NewJobHelp2)
	OR NOT IS_BIT_SET(iStatInt, biNmh4_NewJobHelp3)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_DONE_CORONA_NEW_JOB_HELP(BOOL bDone = TRUE)
	INT iStatInt 
	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4)
	IF bDone
		IF NOT IS_BIT_SET(iStatInt, biNmh4_NewJobHelp1)
			SET_BIT(iStatInt, biNmh4_NewJobHelp1)
		ELIF NOT IS_BIT_SET(iStatInt, biNmh4_NewJobHelp2)
			SET_BIT(iStatInt, biNmh4_NewJobHelp2)
		ELIF NOT IS_BIT_SET(iStatInt, biNmh4_NewJobHelp3)
			SET_BIT(iStatInt, biNmh4_NewJobHelp3)
		ENDIF
	ELSE
		IF IS_BIT_SET(iStatInt, biNmh4_NewJobHelp3)
			CLEAR_BIT(iStatInt, biNmh4_NewJobHelp3)
		ELIF IS_BIT_SET(iStatInt, biNmh4_NewJobHelp2)
			CLEAR_BIT(iStatInt, biNmh4_NewJobHelp2)
		ELIF IS_BIT_SET(iStatInt, biNmh4_NewJobHelp1)
			CLEAR_BIT(iStatInt, biNmh4_NewJobHelp1)
		ENDIF
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4, iStatInt)
ENDPROC

// Check to see if we need to do help text
FUNC BOOL DONE_CORONA_PARACHTING_HELP()
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT5)
	
	IF NOT IS_BIT_SET(iStatInt, biNmh4_NewJobHelp1)
	OR NOT IS_BIT_SET(iStatInt, biNmh4_NewJobHelp2)
	OR NOT IS_BIT_SET(iStatInt, biNmh4_NewJobHelp3)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_DONE_CORONA_PARACHUTING_HELP(BOOL bDone = TRUE)
	INT iStatInt 
	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT5)
	IF bDone
		IF NOT IS_BIT_SET(iStatInt, biNMH5_ParachutingHelp1)
			SET_BIT(iStatInt, biNMH5_ParachutingHelp1)
		ELIF NOT IS_BIT_SET(iStatInt, biNMH5_ParachutingHelp2)
			SET_BIT(iStatInt, biNMH5_ParachutingHelp2)
		ELIF NOT IS_BIT_SET(iStatInt, biNMH5_ParachutingHelp3)
			SET_BIT(iStatInt, biNMH5_ParachutingHelp3)
		ENDIF
	ELSE
		IF IS_BIT_SET(iStatInt, biNMH5_ParachutingHelp3)
			CLEAR_BIT(iStatInt, biNMH5_ParachutingHelp3)
		ELIF IS_BIT_SET(iStatInt, biNMH5_ParachutingHelp2)
			CLEAR_BIT(iStatInt, biNMH5_ParachutingHelp2)
		ELIF IS_BIT_SET(iStatInt, biNMH5_ParachutingHelp1)
			CLEAR_BIT(iStatInt, biNMH5_ParachutingHelp1)
		ENDIF
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT5, iStatInt)
ENDPROC

// Check to see if we need to do help text
FUNC BOOL DONE_VISIBLE_ON_RADAR_HELP()
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4)
	
	IF NOT IS_BIT_SET(iStatInt, biNmh4_VisibleOnRadar)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_DONE_VISIBLE_ON_RADAR_HELP(BOOL bDone = TRUE)
	INT iStatInt 
	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4)
	IF bDone
		IF NOT IS_BIT_SET(iStatInt, biNmh4_VisibleOnRadar)
			SET_BIT(iStatInt, biNmh4_VisibleOnRadar)
		ENDIF
	ELSE
		IF IS_BIT_SET(iStatInt, biNmh4_VisibleOnRadar)
			CLEAR_BIT(iStatInt, biNmh4_VisibleOnRadar)
		ENDIF
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4, iStatInt)
//	REQUEST_SAVE()
ENDPROC

FUNC BOOL DONE_VISIBLE_ON_RADAR_HELP2()
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4)
	
	IF NOT IS_BIT_SET(iStatInt, biNmh4_VisibleOnRadar2)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_DONE_VISIBLE_ON_RADAR_HELP2(BOOL bDone = TRUE)
	INT iStatInt 
	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4)
	IF bDone
		IF NOT IS_BIT_SET(iStatInt, biNmh4_VisibleOnRadar2)
			SET_BIT(iStatInt, biNmh4_VisibleOnRadar2)
		ENDIF
	ELSE
		IF IS_BIT_SET(iStatInt, biNmh4_VisibleOnRadar2)
			CLEAR_BIT(iStatInt, biNmh4_VisibleOnRadar2)
		ENDIF
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4, iStatInt)
//	REQUEST_SAVE()
ENDPROC


FUNC BOOL DONE_REPLAY_JOB_HELP()
	INT iStatInt 
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DONE_RETRY_HELP)	
		iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3)
		IF NOT IS_BIT_SET(iStatInt, biNmh3_DoneReplayJobHelp)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC SET_DONE_REPLAY_JOB_HELP_HELP(BOOL bDone = TRUE)
	INT iStatInt 
	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3)
	IF bDone
		SET_BIT(iStatInt, biNmh3_DoneReplayJobHelp)
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DONE_RETRY_HELP)	
	ELSE
		CLEAR_BIT(iStatInt, biNmh3_DoneReplayJobHelp)
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DONE_RETRY_HELP)	
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3, iStatInt)
//	REQUEST_SAVE()
ENDPROC

FUNC BOOL IS_REPLAY_JOB_HELP_BEING_DISPLAYED()
	RETURN IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_RETRY_INV")
//	RETURN IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_RETRY_INV")
ENDFUNC

PROC CLEAR_REPLAY_JOB_HELP()
	IF IS_REPLAY_JOB_HELP_BEING_DISPLAYED()
	//	CLEAR_HELP()
		#IF IS_DEBUG_BUILD 
			PRINTLN("CLEAR_REPLAY_JOB_HELP called.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
//		CLEAR_THIS_FLOATING_HELP("FM_RETRY_INV")
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC DISPLAY_REPLAY_JOB_FLOATING_HELP()
	#IF IS_DEBUG_BUILD PRINTLN("[dsw] DISPLAY_REPLAY_JOB_FLOATING_HELP called...") #ENDIF
	//PRINT_HELP("FM_RETRY_INV")
//	HELP_AT_SCREEN_LOCATION("FM_RETRY_INV", 0.75, 0.89, INT_TO_ENUM(eARROW_DIRECTION,3), -1, FLOATING_HELP_MISSION_SLOT)
	PRINT_HELP("FM_RETRY_INV")
ENDPROC



FUNC BOOL DONE_TRACKER_HELP()
	INT iStatInt 
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DONE_TRACKER_HELP)	
		iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3)
		IF NOT IS_BIT_SET(iStatInt, biNmh3_TrackerHelp)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC SET_DONE_TRACKER_HELP(BOOL bDone = TRUE)
	INT iStatInt 
	#IF IS_DEBUG_BUILD 
		PRINTLN("SET_DONE_TRACKER_HELP called.")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3)
	IF bDone
		SET_BIT(iStatInt, biNmh3_TrackerHelp)
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DONE_TRACKER_HELP)	
	ELSE
		CLEAR_BIT(iStatInt, biNmh3_TrackerHelp)
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DONE_TRACKER_HELP)	
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3, iStatInt)
//	REQUEST_SAVE()
ENDPROC

FUNC BOOL IS_TRACKER_HELP_BEING_DISPLAYED()
	RETURN IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_TRK")
//	RETURN IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_IHELP_TRK")
ENDFUNC
PROC CLEAN_UP_PLAYER_PED_PARACHUTE()

	MPGlobalsAmbience.bDisableTakeOffChute = FALSE
	SET_PACKED_STAT_BOOL(PACKED_MP_IS_PARACHUTE_EQUIPED, FALSE)
	//If the ped is not injured then remove his parachute
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PLAYER_WEARING_PARACHUTE()
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0) // Remove parachute. B*1603513.
			RESET_CACHED_PARACHUTE_DRAWABLE_FOR_MP_VARIABLES()
			//CLEAR_PLAYER_PARACHUTE_VARIATION_OVERRIDE(PLAYER_ID())		  // Removing call to clear variation due to B*1927929
			PRINTLN("CLEAN_UP_PLAYER_PED_PARACHUTE - Player wearing a parachute. Removing parachute by calling SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0).")
		ELSE
			PRINTLN("CLEAN_UP_PLAYER_PED_PARACHUTE - Player not wearing a parachute.")
		ENDIF
	ENDIF
ENDPROC
PROC CLEAR_TRACKER_HELP()
	IF IS_TRACKER_HELP_BEING_DISPLAYED()
	//	CLEAR_HELP()
		#IF IS_DEBUG_BUILD 
			PRINTLN("CLEAR_TRACKER_HELP called.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
		//CLEAR_THIS_FLOATING_HELP("FM_IHELP_TRK")
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC DISPLAY_TRACKER_HELP()
	#IF IS_DEBUG_BUILD PRINTLN("[dsw] DISPLAY_TRACKER_HELP called...") #ENDIF

//	HELP_AT_SCREEN_LOCATION("FM_IHELP_TRK", 0.165, 0.63, INT_TO_ENUM(eARROW_DIRECTION,1), -1, FLOATING_HELP_MISSION_SLOT)
	PRINT_HELP("FM_IHELP_TRK")
ENDPROC

//FUNC BOOL IS_PLAYER_AUTOMUTED_BADSPORT_GOOD_BOY(PLAYER_INDEX aPlayer)
//	INT iPlayerID = NATIVE_TO_INT(aPlayer)
//	IF iPlayerID > -1
//		RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_AUTOMUTED_BADSPORT)
//	ENDIF
//	RETURN FALSE
//ENDFUNC

PROC MAINTAIN_GOOD_BEHAVIOUR_CASH_REWARD()
	
	IF NETWORK_IS_SIGNED_ONLINE()
		IF NOT NETWORK_PLAYER_IS_BADSPORT()
		ANd NOT NETWORK_PLAYER_IS_CHEATER()
		AND NOT IS_PLAYER_AUTOMUTED_BADSPORT(PLAYER_ID())
		AND NOT	IS_TRANSITION_ACTIVE() 
		AND HAS_IMPORTANT_STATS_LOADED()
	//		STRUCT_STAT_DATE LastDate = GET_MP_DATE_PLAYER_STAT(MPPLY_NON_CHEATER_CASH)
	//		STRUCT_STAT_DATE TodayDate = GET_TODAYS_DATE()
	//		NET_NL()NET_PRINT("MAINTAIN_GOOD_BEHAVIOUR_CASH_REWARD: MPPLY_NON_CHEATER_CASH = ")NET_PRINT_DATE(LastDate)
	//		NET_NL()NET_PRINT("MAINTAIN_GOOD_BEHAVIOUR_CASH_REWARD: GET_TODAYS_DATE = ")NET_PRINT_DATE(TodayDate)
	//		NET_NL()NET_PRINT("HAS_MINS_PASSED_BETWEEN_DATES = ")NET_PRINT_BOOL(HAS_MINS_PASSED_BETWEEN_DATES(LastDate, TodayDate, 5))
			
			

			IF GET_MP_INT_PLAYER_STAT(MPPLY_TOTALPLAYTIME_GOODBOY) > g_sMPTunables.iGoodBehaviourMillisecondTime //5 hours
		//		IF HAS_HOURS_PASSED_BETWEEN_DATES(GET_MP_DATE_PLAYER_STAT(MPPLY_NON_CHEATER_CASH), GET_TODAYS_DATE(), 5)
		//		IF HAS_MINS_PASSED_BETWEEN_DATES(GET_MP_DATE_PLAYER_STAT(MPPLY_NON_CHEATER_CASH), GET_TODAYS_DATE(), 5)
				//Give Cash 
				PRINTLN(" MAINTAIN_GOOD_BEHAVIOUR_CASH_REWARD MPPLY_TOTALPLAYTIME_GOODBOY 1= ",GET_MP_INT_PLAYER_STAT(MPPLY_TOTALPLAYTIME_GOODBOY) )
				INT iGoodBoyCashToGive
				iGoodBoyCashToGive = MULTIPLY_CASH_BY_TUNABLE(g_sMPTunables.iGoodBehaviourCashawardValue)
				
				
				NET_NL()NET_PRINT("MAINTAIN_GOOD_BEHAVIOUR_CASH_REWARD - Give player ")NET_PRINT_INT(iGoodBoyCashToGive)

				
				SET_GOOD_BEHAVIOUR_CASH_REWARD_TICKER_NEED_DISPLAY(TRUE, iGoodBoyCashToGive)
				
				SET_MP_INT_PLAYER_STAT(MPPLY_TOTALPLAYTIME_GOODBOY, 0)
				
				PRINTLN(" MAINTAIN_GOOD_BEHAVIOUR_CASH_REWARD MPPLY_TOTALPLAYTIME_GOODBOY 2 = ",GET_MP_INT_PLAYER_STAT(MPPLY_TOTALPLAYTIME_GOODBOY) )
		//				SET_MP_DATE_PLAYER_STAT(MPPLY_NON_CHEATER_CASH, GET_TODAYS_DATE())
		//				REQUEST_SAVE(STAT_SAVETYPE_CASH)

			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC FMMC_MISSIONS_CLEANUP_COMMON(BOOL bResetClockOverride = TRUE)
	PRINTLN("FMMC_MISSIONS_CLEANUP_COMMON()")
	
	//-- Dave W - don't clear clock override if it's the intro mission
	IF bResetClockOverride
		IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
			
			CPRINTLN(DEBUG_MP_TUTORIAL, "FMMC_MISSIONS_CLEANUP_COMMON - tutorial - not clearing clock override")
		ELIF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
		AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		AND g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION
		AND g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
			PRINTLN("NETWORK_CLEAR_CLOCK_TIME_OVERRIDE")
			SET_TIME_OF_DAY(TIME_OFF, TRUE)
		ENDIF
	ELSE 
		PRINTLN("bResetClockOverride - time")
	ENDIF
	
	PRINTLN("REMOVE_SCENARIO_BLOCKING_AREAS")
	REMOVE_SCENARIO_BLOCKING_AREAS()
	IF NOT FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
		CLEANUP_POLICE()
	ENDIF
	
	IF CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
		SET_HEIST_STRAND_UPDATE_CHECK_REQUIRED(TRUE)
		SET_GANGOPS_STRAND_UPDATE_CHECK_REQUIRED(TRUE)
		SET_CASINO_HEIST_STRAND_UPDATE_CHECK_REQUIRED(TRUE)
		SET_ISLAND_HEIST_STRAND_UPDATE_CHECK_REQUIRED(TRUE)
	ENDIF
	
	IF bResetClockOverride
	AND NOT IS_ON_IMPROMPTU_DEATHMATCH_GLOBAL_SET()
	AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND NOT g_sMPTunables.bTurn_snow_on_off
	#IF IS_DEBUG_BUILD
	AND NOT g_bDemo
	#ENDIF
	AND NOT IS_HEIST_QUICK_RESTART()
		CLEAR_OVERRIDE_WEATHER()
	ELSE 
		PRINTLN("bResetClockOverride - weather")
	ENDIF
	PRINTLN("ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)")
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)			
	PRINTLN("ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)")
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)	
	
	BOOL FakeBool
	REFRESH_ALL_PROFILE_SETTING_STATS(FakeBool)

	//Deal with saving stats
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM //Only save if actually in FM, don't save in teh creator. BC: 19/03/2014
	AND NOT IS_TRANSITION_ACTIVE() //Don't bother if the transition is running, that will handle saving. BC 11/06/2014 bug 1885657
	AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		//If this is the last mission of a playlist then the playlist script will save.
		IF IS_THIS_THE_LAST_PLAYLIST_MISSION()
			SET_PLAYLIST_SAVE_STATS()
		//Don't bother if its the last mission of a playlist, 
		ELSE
			REQUEST_SAVE(SSR_REASON_INSTANCED_CONTENT, STAT_SAVETYPE_END_MATCH)
		ENDIF
	ENDIF

	
	DISABLE_PERSONAL_VEHICLE_CREATION_FOR_TRANSITION(FALSE)
	PRINTLN("FMMC_MISSIONS_CLEANUP_COMMON called DISABLE_PERSONAL_VEHICLE_CREATION_FOR_TRANSITION(FALSE)")
	
ENDPROC
#IF IS_DEBUG_BUILD
FUNC STRING GET_FMMC_MISSION_TYPE_STRING(INT iMissionType)
	SWITCH iMissionType
		CASE FMMC_TYPE_MISSION				RETURN "F9_FM_MISS"
		CASE FMMC_TYPE_DEATHMATCH			RETURN "F9_FM_DEATH"
		CASE FMMC_TYPE_RACE					RETURN "F9_FM_RACE"
		CASE FMMC_TYPE_BASE_JUMP			RETURN "F9_FM_BJ"
		//CASE FMMC_TYPE_TRIATHLON			RETURN "F9_FM_TRI"
		CASE FMMC_TYPE_SURVIVAL				RETURN "F9_FM_HORDE"
		CASE FMMC_TYPE_IMPROMPTU_DM			RETURN "F9_FM_RAM"
		CASE FMMC_TYPE_GANGHIDEOUT			RETURN "F9_FM_GH"
		CASE FMMC_TYPE_MG_ARM_WRESTLING		RETURN "F9_FM_AW"
		CASE FMMC_TYPE_MG_DARTS				RETURN "F9_FM_DARTS"
		CASE FMMC_TYPE_MG_GOLF				RETURN "F9_FM_GOLF"
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	RETURN "F9_FM_SHOOT"
		CASE FMMC_TYPE_MG_TENNIS			RETURN "F9_FM_TEN"
		CASE FMMC_TYPE_GUN_INTRO			RETURN "F9_FM_INT"
		CASE FMMC_TYPE_RACE_TO_POINT		RETURN "F9_FM_R2P"
	ENDSWITCH
	RETURN "F9_LINE"
ENDFUNC
#ENDIF
PROC SAVE_PERSONAL_VEHICLE_END_STATE(BOOL bStartingJob,BOOL bmission = FALSE)

//stores if personal vehicle should be recreated in same position
VEHICLE_INDEX tempveh

	IF bStartingJob
//	----------
// 	Fix for TODO 1625250 - we need to make sure players vehicles are warped close to the player when the start a new session.
// 	The corona script set the bWarpNear flag to TRUE.
//		IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
//			tempveh= NET_TO_VEH(PERSONAL_VEHICLE_ID())
//			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),GET_ENTITY_COORDS(tempveh)) < 100.0
//				SET_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
//				g_TransitionSessionNonResetVars.PVStartLocation = GET_ENTITY_COORDS(tempveh)
//				g_TransitionSessionNonResetVars.PVStartHeading = GET_ENTITY_HEADING(tempveh)
//				PRINTLN("PLYVEH FREEMODE CLEANUP -player near PV - so set in original position: ",g_TransitionSessionNonResetVars.PVStartLocation, " with heading: ",g_TransitionSessionNonResetVars.PVStartHeading) 
//			ELSE
//				g_TransitionSessionNonResetVars.PVStartLocation = GET_ENTITY_COORDS(tempveh)
//				g_TransitionSessionNonResetVars.PVStartHeading = GET_ENTITY_HEADING(tempveh)
//				CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
//				PRINTLN("PLYVEH FREEMODE CLEANUP -player NOT near PV - so warp") 
//			ENDIF
//		ELSE
			CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
			g_TransitionSessionNonResetVars.PVStartLocation = <<0.0,0.0,0.0>>
//		ENDIF
//	----------
	ELSE
		IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
			tempveh= PERSONAL_VEHICLE_ID()
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),GET_ENTITY_COORDS(tempveh)) < 100.0
				SET_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
				g_TransitionSessionNonResetVars.PVStartLocation = GET_ENTITY_COORDS(tempveh)
				g_TransitionSessionNonResetVars.PVStartHeading = GET_ENTITY_HEADING(tempveh)
				PRINTLN("PLYVEH JOB CLEANUP -player near PV - so set in original position: ",g_TransitionSessionNonResetVars.PVStartLocation, " with heading: ",g_TransitionSessionNonResetVars.PVStartHeading) 
			ELSE
				CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
				g_TransitionSessionNonResetVars.PVStartLocation = <<0.0,0.0,0.0>>
				PRINTLN("PLYVEH JOB CLEANUP -player NOT near PV - so warp near") 
			ENDIF
		ELSE
			IF bmission
				CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
				g_TransitionSessionNonResetVars.PVStartLocation = <<0.0,0.0,0.0>>
				PRINTLN("PLYVEH JOB CLEANUP -mission vehicle dead- so warp near") 
			ELSE
				IF NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.PVStartLocation)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),g_TransitionSessionNonResetVars.PVStartLocation) < 100.0
						SET_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
						PRINTLN("PLYVEH JOB CLEANUP -player near PV - so set in original position: ",g_TransitionSessionNonResetVars.PVStartLocation, " with heading: ",g_TransitionSessionNonResetVars.PVStartHeading) 
					ELSE
						CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
						g_TransitionSessionNonResetVars.PVStartLocation = <<0.0,0.0,0.0>>
						PRINTLN("PLYVEH JOB CLEANUP -player NOT near PV - so warp near") 
					ENDIF
				ELSE
					CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
					PRINTLN("PLYVEH JOB CLEANUP -PV pos ZERO- so warp near")
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC RESET_CHAT_PROXIMITY()
//	// set up proximity chat
//	IF (g_bBroadcastAppEnabled)
//	AND (g_bBroadcastAppOn)
//		CLIENT_SET_UP_EVERYONE_CHAT()
//		NET_PRINT("RESET_CHAT_PROXIMITY - CLIENT_SET_UP_EVERYONE_CHAT") NET_NL()
//		DEBUG_PRINTCALLSTACK()
//	ELSE
		CLIENT_SET_UP_PROXIMITY_CHAT(CHAT_PROXIMITY_FM)
		NET_PRINT("RESET_CHAT_PROXIMITY - CLIENT_SET_UP_PROXIMITY_CHAT(CHAT_PROXIMITY_FM) - CHAT_PROXIMITY_FM = ") NET_PRINT_FLOAT(CHAT_PROXIMITY_FM) NET_NL()
//		DEBUG_PRINTCALLSTACK()	
//	ENDIF
ENDPROC

//Set the the mission is over and on the LB for all players on a mission/race
PROC SET_MISSION_OVER_AND_ON_LB_FOR_SCTV()
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MISSION_OVER_AND_ON_LB_FOR_SCTV)
		CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === SET_MISSION_OVER_AND_ON_LB_FOR_SCTV")
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MISSION_OVER_AND_ON_LB_FOR_SCTV)
	ENDIF
ENDPROC
//Clear  the the mission is over and on the LB for all players on a mission/race
PROC CLEAR_MISSION_OVER_AND_ON_LB_FOR_SCTV()
	CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === CLEAR_MISSION_OVER_AND_ON_LB_FOR_SCTV")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MISSION_OVER_AND_ON_LB_FOR_SCTV)
ENDPROC
//Is the current mission over and on the leader board for all players on a mission
FUNC BOOL IS_MISSION_OVER_AND_ON_LB_FOR_SCTV(PLAYER_INDEX piPlayer)
	INT iPlayer = NATIVE_TO_INT(piPlayer)
	IF iPlayer = -1
		RETURN FALSE
	ENDIF
	RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayer].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MISSION_OVER_AND_ON_LB_FOR_SCTV)
ENDFUNC

FUNC BOOL IS_PLAYER_RUNNING_A_CUTSCENE(PLAYER_INDEX piPlayer)
	INT iPlayer = NATIVE_TO_INT(piPlayer)
	IF iPlayer = -1
		RETURN FALSE
	ENDIF
	RETURN IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
ENDFUNC

//Clear the serverbroadcast data global struct
PROC CLEAR_THE_LEADERBOARD_STRUCT_SERVER_DATA(BOOL bClearServerVars = TRUE)
	PRINTLN("[RCC MISSION] pre game - CLEAR_THE_LEADERBOARD_STRUCT_SERVER_DATA")
	PRINTLN("[TS] [MSRAND] - CLEAR_THE_LEADERBOARD_STRUCT_SERVER_DATA")
	THE_LEADERBOARD_STRUCT sleaderboard
	
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds = 0
		g_MissionControllerserverBD_LB.iHighestRankInSession = 0
	ENDIF
	
	INT iLoop
	INT iRoundsScore
	FOR iLoop = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		//Store out the rounds score
		iRoundsScore = g_MissionControllerserverBD_LB.sleaderboard[iLoop].iRoundsScore
		//Clear the array
		g_MissionControllerserverBD_LB.sleaderboard[iLoop] = sleaderboard
		//re initilise the rounds score. 
		IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
			g_MissionControllerserverBD_LB.sleaderboard[iLoop].iRoundsScore = iRoundsScore
			PRINTLN("[TS] [MSRAND] - g_MissionControllerserverBD_LB.sleaderboard[", iLoop, "].iRoundsScore = ", g_MissionControllerserverBD_LB.sleaderboard[iLoop].iRoundsScore)
		ENDIF
		//set the defailts for all the other details
		g_MissionControllerserverBD_LB.sleaderboard[iLoop].playerID 		= INVALID_PLAYER_INDEX()
		g_MissionControllerserverBD_LB.sleaderboard[iLoop].iParticipant 	= -1
		g_MissionControllerserverBD_LB.sleaderboard[iLoop].iTeam            = -1
	ENDFOR	
	IF NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
	AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND bClearServerVars
		PRINTLN("[TS] [MSRAND] - CLEAR_THE_LEADERBOARD_STRUCT_SERVER_DATA - g_MissionControllerserverBD_LB.iHashMAC     = 0")
		PRINTLN("[TS] [MSRAND] - CLEAR_THE_LEADERBOARD_STRUCT_SERVER_DATA - g_MissionControllerserverBD_LB.iMatchTimeID = 0")
		g_MissionControllerserverBD_LB.iHashMAC = 0
		g_MissionControllerserverBD_LB.iMatchTimeID = 0		
	ENDIF
ENDPROC

//Clear all the globals that are needed for the celebration if it's a strand mission
PROC CLEAR_STRAND_MISSION_CELEBRATION_GLOBALS()
	PRINTLN("[RCC MISSION] pre game - CLEAR_STRAND_MISSION_CELEBRATION_GLOBALS")
	PRINTLN("[TS] [MSRAND] - CLEAR_STRAND_MISSION_CELEBRATION_GLOBALS")

ENDPROC

//Reset the player broadcast rounds data after the rounds mission is over
PROC RESET_FMMC_MISSION_ROUND_BD()
	PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_ROUND_BD")
	FMMC_MISSION_ROUND_BD		sJobRoundDataTemp
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	IF iMyGBD != -1
		GlobalplayerBD_FM_2[iMyGBD].sJobRoundData = sJobRoundDataTemp
	ENDIF
ENDPROC

//For Strans missions resets the cash and XP earn INTS, this is so we can send one end/earn event at the end of telemetry rather than two. 
PROC RESET_EARN_AND_XP_VARS_FOR_STRAND_TELEMETRY(BOOL bFullReset = TRUE)
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	OR bFullReset
		#IF IS_DEBUG_BUILD
		IF g_iFirstMissionCash != 0
			PRINTLN("[TEL] g_iFirstMissionCash       = 0")
		ENDIF
		IF g_iFirstMissionXP != 0
			PRINTLN("[TEL] g_iFirstMissionXP       = 0")
		ENDIF
		#ENDIF		
		g_iFirstMissionCash = 0
		g_iFirstMissionXP = 0
	ENDIF
ENDPROC
#IF IS_DEBUG_BUILD
FUNC STRING  GET_g_sFMMCEOMiVoteStatus_FOR_DEBUG_PRINT()
	SWITCH g_sFMMCEOM.iVoteStatus
		CASE ciFMMC_EOM_VOTE_STATUS_INVALID				RETURN "INVALID"
		CASE ciFMMC_EOM_VOTE_STATUS_CONTINUE			RETURN "CONTINUE"
		CASE ciFMMC_EOM_VOTE_STATUS_RANDOM				RETURN "RANDOM"
		CASE ciFMMC_EOM_VOTE_STATUS_RESTART				RETURN "RESTART"
		CASE ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART		RETURN "QUICK"
		CASE ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP	RETURN "QUICK_SWAP"
		CASE ciFMMC_EOM_VOTE_STATUS_QUIT				RETURN "QUIT"
		CASE ciFMMC_EOM_VOTE_STATUS_JOB_VOTE			RETURN "JOB_VOTE"
		CASE ciFMMC_EOM_VOTE_STATUS_STRAND_MISSION		RETURN "STRAND"
		CASE ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION		RETURN "ROUNDS"	
		CASE ciFMMC_EOM_VOTE_STATUS_HEIST_CUT			RETURN "HEIST_CUT"	
	ENDSWITCH										
	RETURN ""
ENDFUNC
#ENDIF

FUNC BOOL HAVE_ANY_PLAYERS_DIED_ON_CURRENT_HEIST_MISSION()
	INT i
  
  	REPEAT NUM_NETWORK_PLAYERS i
        IF g_MissionControllerserverBD_LB.sleaderboard[i].playerID <> INVALID_PLAYER_INDEX()
         	IF g_MissionControllerserverBD_LB.sleaderboard[i].iDeaths > 0
				PRINTLN("[MJM] HAVE_ANY_PLAYERS_DIED_ON_CURRENT_HEIST_MISSION - Player ",NATIVE_TO_INT(g_MissionControllerserverBD_LB.sleaderboard[i].playerID)," died ",g_MissionControllerserverBD_LB.sleaderboard[i].iDeaths, " time/s, not counting mission for ultimate challenge.")
				RETURN TRUE
		 	ENDIF
        ENDIF 
  	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


PROC CLEAN_PLAYER_ELO_GLOBALS()
	INT iLoop 
	FOR iLoop = 0 TO (NUM_NETWORK_PLAYERS - 1)
		g_iPlayerELO[iLoop] = 0
	ENDFOR
ENDPROC 

PROC CLEAR_READY_TO_START_INTRO_ANIMS()
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_READY_TO_START_INTRO_ANIMS)
		PRINTLN("[TS][ROUNDCAM] - CLEAR_READY_TO_START_INTRO_ANIMS")					
	ENDIF
	#ENDIF
	CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_READY_TO_START_INTRO_ANIMS)
ENDPROC

STRUCT READY_TO_START_INTRO_ANIMS_STRUCT
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
ENDSTRUCT

PROC SET_READY_TO_START_INTRO_ANIMS(BOOL bSendEvent = TRUE)
	IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_READY_TO_START_INTRO_ANIMS)
	AND bSendEvent
		INT y,m,d,h,min,s
		GET_POSIX_TIME(y,m,d,h,min,s)
		PRINTLN("[TS][ROUNDCAM] - SET_READY_TO_START_INTRO_ANIMS", " y ", y, " m ", m, " h ", h, " min ", min, " s ", s)					
		READY_TO_START_INTRO_ANIMS_STRUCT Event
		Event.Details.Type				= SCRIPT_EVENT_READY_TO_START_INTRO_ANIMS
		Event.Details.FromPlayerIndex	= PLAYER_ID()
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[TS][ROUNDCAM] - SET_READY_TO_START_INTRO_ANIMS - Already Set")					
		#ENDIF
	ENDIF
	SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_READY_TO_START_INTRO_ANIMS)
ENDPROC

FUNC BOOL IS_PLAYER_READY_TO_START_INTRO_ANIMS(PLAYER_INDEX piPassed)
	RETURN IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(piPassed)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_READY_TO_START_INTRO_ANIMS)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_READY_TO_START_INTRO_ANIMS()
	RETURN IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_READY_TO_START_INTRO_ANIMS)
ENDFUNC

PROC RESET_TEAMS_FOR_PLAYLIST()
	PRINTLN("[TS] RESET_TEAMS_FOR_PLAYLIST()")					
	INT iLoop
	FOR iLoop = 0 TO (NUM_NETWORK_PLAYERS - 1)
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] = 0
	ENDFOR
ENDPROC

PROC RESET_FMMC_MISSION_VARIABLES(BOOL bSetNeedsCleanup = TRUE, BOOL bComingOutOfShootingRange = FALSE, INT iPassFailStatus = ciFMMC_END_OF_MISSION_STATUS_NOT_SET, BOOL bResetOverride = TRUE, BOOL bHasPlayerStartedRace = TRUE)
	
	INT iPlayerSlot = NATIVE_TO_INT(PLAYER_ID())
	INT iMissionType = GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType
	BOOL bRetainPropertyDetailsOnRestart
	INT irepeat
	
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("BWW...  RESET_FMMC_MISSION_VARIABLES CALLED")PRINTNL()
	PRINTLN("[TS] ***********************************")
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_BASE_JUMP
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL
		PRINTLN("[TS] * **  ENDING   - ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_FMMC_MISSION_TYPE_STRING(g_FMMC_STRUCT.iMissionType)), " - ", g_FMMC_STRUCT.tl31LoadedContentID, " - ", g_FMMC_STRUCT.tl63MissionName, " - VOTE = ", GET_g_sFMMCEOMiVoteStatus_FOR_DEBUG_PRINT(), " T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " ** *")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * **  ENDING   - ", g_FMMC_STRUCT.iMissionType, " - ", g_FMMC_STRUCT.tl31LoadedContentID, " - ", g_FMMC_STRUCT.tl63MissionName, "  T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " ** *")#ENDIF
	ELSE
		PRINTLN("[TS] * **  ENDING   - ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_FMMC_MISSION_TYPE_STRING(g_FMMC_STRUCT.iMissionType)), " T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " ** *")
		#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * **  ENDING   - ", g_FMMC_STRUCT.iMissionType, " T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " ** *")#ENDIF
	ENDIF
	PRINTLN("[TS] ***********************************")
	DEBUG_PRINTCALLSTACK()
	PRINTLN("TRANSITION TIME - MISSION CLEANUP - RESET_FMMC_MISSION_VARIABLES- FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
	g_bLaunchedViaZmenu = FALSE	
	g_bDebugLaunchedMission = FALSE
	#ENDIF
	CLEAR_MISSION_OVER_AND_ON_LB_FOR_SCTV()
	//Clear the SCTV Ticker feed
	CLEAR_SCTV_TICKER_FEED_STRUCT()
	// 1993749 cleanup pad vibration
	CLEAR_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL)
	CLEAR_SCTV_READY_FOR_NEXT_MISSION()
	//Clear the old posix time
	g_iCurrentPosixforNJVS = 0
	bSplitSessionDuringJob = FALSE
	PRINTLN("[bSplitSessionDuringJob] RESET_FMMC_MISSION_VARIABLES - bSplitSessionDuringJob = FALSE ")
//	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
//		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_DO_DEADLINER_TEXT)
//	ENDIF
	
	//Reset the kill strip override
	CLEANUP_KILL_STRIP_OVER_RIDE_PLAYER_ID()
	
	PRINTLN("[TEL] RESET_FMMC_MISSION_VARIABLES - NETWORK_BLOCK_JOIN_QUEUE_INVITES(FALSE)")
	NETWORK_BLOCK_JOIN_QUEUE_INVITES(FALSE)
	
	CLEAR_TRANSITION_SESSIONS_LAUNCHED_FROM_ON_YACHT()
	
	g_HeistTelemetryData.iHeistHashedMac = 0
	g_HeistTelemetryData.iHeistPosixTime = 0
	PRINTLN("[TEL] RESET_FMMC_MISSION_VARIABLES - g_HeistTelemetryData.iHeistHashedMac = ", g_HeistTelemetryData.iHeistHashedMac)
	PRINTLN("[TEL] RESET_FMMC_MISSION_VARIABLES - g_HeistTelemetryData.iHeistPosixTime = ", g_HeistTelemetryData.iHeistPosixTime)
	//Clear Speirs globals
	g_b_CheckpointAnims = FALSE
	bSCTVHeliActive = FALSE
	bShowTextSCTV = FALSE
	SET_GLOBAL_CLIENT_FINISHED_JOB(FALSE)
	g_dPadSecondPage = FALSE
	g_i_NumPlayersForLbd = 0
	g_FinalRoundsLbd = FALSE
	g_b_JobEnded = FALSE
	g_b_BlockAmmo = FALSE
	g_bShouldBlockPauseJobs = FALSE
	g_bNeedToCallMatchEndOnQuit = TRUE
	g_b_DevSpecLbdContinue = FALSE
	g_i_RacePlayerExploded = -1

	COLOUR_PC_TEXT_CHAT(FALSE)
	CLEAR_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RESET_STATE)
	#IF IS_DEBUG_BUILD
	g_debugDisplayState = DEBUG_DISP_OFF
	#ENDIF
//	RESET_CLOUDS_ALPHA() // 2135898
//	PRINTLN("[CLOUDS] RESET_CLOUDS_ALPHA ")
	
	IF NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
	AND NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		CLEANUP_FORCED_CAMERA()
	ENDIF
	//Clear that we have passed the forst mission in a strand mission
	//Lukasz: 	Went over this call with Bobby and it might actually not be needed here and causing issues.
	//			This flag would be reset when quitting out from the mission anyway, when RESET_STRAND_MISSION_DATA() is called.
	//			Commenting this out for now, as a possible fix for B*2081615.
	//			Otherwise all calls to HAS_FIRST_STRAND_MISSION_BEEN_PASSED() from this point would always returns FALSE, which is not as intended.
	//CLEAR_FIRST_STRAND_MISSION_BEEN_PASSED()
	
	//Clear that this is the last strand mission
	CLEAR_THIS_IS_THE_LAST_STRAND_MISSION()
	
	// 2132454
	CLEAR_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
	
	CLEAR_QUICK_START_APARTMENT_DATA()
	
	//reset the rounds mission broadcast player bit set
	RESET_ROUNDS_MISSION_BROAD_CAST_DATA_BIT_SET()
	
	//Reset the ELO globals
	CLEAN_PLAYER_ELO_GLOBALS()
	
	
	//Clear that we're ready to start the intro animations
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CLEAR_READY_TO_START_INTRO_ANIMS()
		CLEAR_THIS_PLAYER_ON_JIP_WARNING()
	ENDIF
	
	//If we should show the LB then clear it
	IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
	OR NOT IS_THIS_A_ROUNDS_MISSION()
		CLEAR_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
		RESET_ROUND_MISSION_DATA()
		CLEAR_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
		RESET_FMMC_MISSION_ROUND_BD()
		IF iPlayerSlot != -1
			GlobalplayerBD_FM_2[iPlayerSlot].sJobRoundData.iNumberOfRounds = 0
			GlobalplayerBD_FM_2[iPlayerSlot].sJobRoundData.iNumberOfRoundsPlayed = 0
			GlobalplayerBD_FM_2[iPlayerSlot].sJobRoundData.iMaxNumberOfTeams = 0
			PRINTLN("[TS] [SST] * RESET_FMMC_MISSION_VARIABLES GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams= ", 0)
		ENDIF
	ENDIF
	
	//Clear the the NJVS should be skipped
	CLEAR_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
		
	// 1643044
	IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_GANGHIDEOUT
	AND GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) != ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE
	AND GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) != ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2
	AND NOT CONTENT_IS_USING_ARENA()
	AND NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
				PRINTLN("RESET_FMMC_MISSION_VARIABLES, TRANSITION REMOVE_PED_HELMET - ")
				REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	g_bHeistQuickRestart = FALSE
	IF g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART        
    	IF Is_Player_Currently_On_MP_Heist(PLAYER_ID()) 
        OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			g_bHeistQuickRestart = TRUE
			PRINTLN("RESET_FMMC_MISSION_VARIABLES: g_bHeistQuickRestart = TRUE")
		ENDIF
	ENDIF
	
	//Kill face to face converstaion
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
	//if we are holding the transition then clear the flag
	IF TRANSITION_SESSION_MAIN_WAITING_FOR_CAM_PULL_DOWN_WHEN_SPECTATING_READY()
		CLEAR_TRANSITION_SESSION_MAIN_WAITING_FOR_CAM_PULL_DOWN_WHEN_SPECTATING_READY()
	ENDIF
	
	//clear that we started the tutorial this session
	CLEAR_HEIST_STARTED_TUTORIAL_THIS_SESSION()
	
	//Clear that this is an sc nick name mission
	g_FMMC_STRUCT.bScNickName = FALSE
	
	//Clean that the mission needs flagged for delete
	g_FMMC_Current_Mission_Cloud_Data.bFlaggedForDelete = FALSE			
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_CLEAN_UP_LEAVERS_FOR_MISSION)
	AND NOT IS_PLAYER_USING_ARENA() 
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciREMOVE_PED_WHEN_PLAYER_LEAVES)
			PRINTLN("[TS] RESET_FMMC_MISSION_VARIABLES - ciREMOVE_PED_WHEN_PLAYER_LEAVES (TRUE)")
			SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
			NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
		ELSE
			PRINTLN("[TS] RESET_FMMC_MISSION_VARIABLES - ciREMOVE_PED_WHEN_PLAYER_LEAVES (FALSE)")
			SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
			NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)
		ENDIF
	ENDIF
	
	g_FMMC_STRUCT.tl31VerifiedMissionCreator = ""
	PRINTLN("[TS] RESET_FMMC_MISSION_VARIABLES - g_FMMC_STRUCT.tl31VerifiedMissionCreator = NULL")
	
	IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		//hide stuff
		THEFEED_HIDE_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
		
		IF bResetOverride
		AND NOT IS_PLAYER_ON_IMPROMPTU_DM()
		AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
			HIDE_HUD_AND_RADAR_THIS_FRAME() // Dave W, don't want to call this if intro cutscene just finished
		ENDIF
	ENDIF
	
	FOR irepeat = 0 TO (NUM_NETWORK_PLAYERS-1)
		g_iKilledThisPlayer[irepeat] = 0
		g_iWasKilledBy[irepeat] = 0
	ENDFOR
	
	IF iMissionType =FMMC_TYPE_MISSION
		SET_BIT(g_TransitionSessionNonResetVars.MissionPVBitset,ci_PV_WAS_ON_MISSION)
	ELSE
		CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset,ci_PV_WAS_ON_MISSION)
	ENDIF
	
	//Clear the warning screen for quitting via a heist - 2032357-Heist alert screen "If you quit, the Heist will fail for the entire Crew" persisted into freemode
	#IF IS_DEBUG_BUILD
		IF g_bRunQuitHeistWarningScreen
			PRINTLN("...KGM MP [JobList][App]: set g_bRunQuitHeistWarningScreen = FALSE")
		ENDIF
	g_bRunQuitHeistWarningScreen = FALSE
	#ENDIF
	
	//Set the number of rounds we've played into BD data
	IF iPlayerSlot != -1
	AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed != 0
		PRINTLN("[TS] [MSROUND] - GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iNumberOfRoundsPlayed = g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed")
		GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iNumberOfRoundsPlayed = g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed
	ENDIF
	
	//if we quit then kill the playlist
	IF IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET()
	OR (iPlayerSlot != -1
	AND IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob))
		IF NOT bHasPlayerStartedRace
			PRINTLN("RESET_FMMC_MISSION_VARIABLES Do not call CALL_END_OF_MISSION_QUIT_TELEMETRY fix for url:bugstar:6267658 ")
			g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
			CALL_END_OF_MISSION_QUIT_TELEMETRY()
			
			g_FMMC_STRUCT.iMissionType = 0
		ELSE
			CALL_END_OF_MISSION_QUIT_TELEMETRY()
		ENDIF
		
		PRINTLN("[TEL] - CALL_END_OF_MISSION_VOTE_TELEMETRY - Player is skipping, skip vote telemerty")	
		CLEAR_GETTING_CONTENT_BY_ID()
		//Clean up the strand mission data
		RESET_STRAND_MISSION_DATA()
		RESET_ROUND_MISSION_DATA(DEFAULT, DEFAULT, FALSE)
		IF iPlayerSlot != -1
			GlobalplayerBD_FM_2[iPlayerSlot].sJobRoundData.iNumberOfRounds = 0
			GlobalplayerBD_FM_2[iPlayerSlot].sJobRoundData.iNumberOfRoundsPlayed = 0
			GlobalplayerBD_FM_2[iPlayerSlot].sJobRoundData.iMaxNumberOfTeams = 0
			PRINTLN("[TS] [SST] * RESET_FMMC_MISSION_VARIABLES GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams= ", 0)
		ENDIF
		RESET_EARN_AND_XP_VARS_FOR_STRAND_TELEMETRY()
		//Set that we are quitting a tournament if that's what's happening
		IF IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
			SET_I_AM_QUITTING_A_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
		ENDIF
		IF NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			CALL_PLAYSTATS_LEAVE_JOB_CHAIN(ciJOB_CHAIN_LEAVE_REASON_QUIT_PHONE)
		ENDIF
		CLEAR_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
		SET_PLAYER_ON_A_PLAYLIST(FALSE) 
		CLEAR_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
		g_iTotalMissionsPlayedInRow = 0
		PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_iTotalMissionsPlayedInRow = ", g_iTotalMissionsPlayedInRow)
	ENDIF
	
	//If we're not setting up a strand mission then clear the global data
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		CLEAR_THIS_IS_A_STRAND_MISSION()
		//if a strand mission was being set up
		IF IS_STRAND_MISSION_READY_TO_START_DOWNLOAD()
			//If we are currently getting content by ID
			IF ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID()
				//Then clear it!
				CLEAR_GETTING_CONTENT_BY_ID()
			ENDIF
		ENDIF
		RESET_EARN_AND_XP_VARS_FOR_STRAND_TELEMETRY()
		
			
			
		
			IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()	
				//Cleanup the globals used in cash grab
				PRINTLN("RESET_FMMC_MISSION_VARIABLES - Cleaning up cash grab globals as not a strand mission is being initialised and not a corona quick restart is being initialised.")
				g_TransitionSessionNonResetVars.iTotalCashGrabTake 	= 0
				g_TransitionSessionNonResetVars.iTotalCashGrabDrop 	= 0
				PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_TransitionSessionNonResetVars.iTotalCashGrabTake = ", g_TransitionSessionNonResetVars.iTotalCashGrabTake)
				PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_TransitionSessionNonResetVars.iTotalCashGrabDrop = ", g_TransitionSessionNonResetVars.iTotalCashGrabDrop)
				
				g_TransitionSessionNonResetVars.bInvolvedInCashGrab 	= FALSE
				g_TransitionSessionNonResetVars.bDisplayCashGrabTake 	= FALSE
				PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_TransitionSessionNonResetVars.bInvolvedInCashGrab  = ", g_TransitionSessionNonResetVars.bInvolvedInCashGrab)
				PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_TransitionSessionNonResetVars.bDisplayCashGrabTake = ", g_TransitionSessionNonResetVars.bDisplayCashGrabTake)
				g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed = 0
			ENDIF
	ELSE
		CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
		PRINTLN("RESET_FMMC_MISSION_VARIABLES - CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)")
	ENDIF
	
	RESET_HEIST_JobBInfo_FOR_TELEMETRY(g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED())

	RESET_GANGOPS_JobBInfo_FOR_TELEMETRY(g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED())
	
	RESET_CASINO_HEIST_TELEMETRY_DATA_VARS(g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED())
	
	RESET_ISLAND_HEIST_TELEMETRY_DATA_VARS(g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED())
	
	IF IS_CORONA_INITIALISING_A_QUICK_RESTART()	
		g_TransitionSessionNonResetVars.bHasQuickRestarted = TRUE
	ELSE
		g_TransitionSessionNonResetVars.bHasQuickRestarted = FALSE
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF IS_CORONA_INITIALISING_A_QUICK_RESTART()	
			g_TransitionSessionNonResetVars.bHasQuickRestartedDuringStrandMission = TRUE
		ELSE
			g_TransitionSessionNonResetVars.bHasQuickRestartedDuringStrandMission = FALSE
		ENDIF
	ENDIF
	
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
		IF HAVE_ANY_PLAYERS_DIED_ON_CURRENT_HEIST_MISSION()
			g_TransitionSessionNonResetVars.bAnyPlayerDiedDuringMission = TRUE
		ELSE
			g_TransitionSessionNonResetVars.bAnyPlayerDiedDuringMission = FALSE
		ENDIF
		PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_TransitionSessionNonResetVars.bAnyPlayerDiedDuringMission  = ", g_TransitionSessionNonResetVars.bAnyPlayerDiedDuringMission )
	ENDIF
	
	
	//Clear rounds mission data
	IF NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		g_MissionControllerserverBD_LB.iHighestRankInSession = 0
	ENDIF
	
	//If we're not setting up a strand mission then clear the global data
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	//And not a rounds mission
	AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		PRINTLN("[TS] [MSRAND] - RESET_FMMC_MISSION_VARIABLES - IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED() - clearing stuff")
		RESET_STRAND_MISSION_DATA(DEFAULT, g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART)
		RESET_ROUND_MISSION_DATA(DEFAULT, DEFAULT, FALSE)
		CLEAR_THE_LEADERBOARD_STRUCT_SERVER_DATA(FALSE)
		CLEAR_STRAND_MISSION_CELEBRATION_GLOBALS()
		g_bjobVisibilitySetInCorona = FALSE
		IF iPlayerSlot != -1
			GlobalplayerBD_FM_2[iPlayerSlot].sJobRoundData.iNumberOfRounds = 0
			GlobalplayerBD_FM_2[iPlayerSlot].sJobRoundData.iNumberOfRoundsPlayed = 0
			GlobalplayerBD_FM_2[iPlayerSlot].sJobRoundData.iMaxNumberOfTeams = 0
			PRINTLN("[TS] [SST] * RESET_FMMC_MISSION_VARIABLES GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams= ", 0)
		ENDIF
	ENDIF
	
	//Clean up the menu
	CLEANUP_MENU_ASSETS()
	
	IF SHOULD_TRANSITION_SESSIONS_DO_TUT_TRANSITION_TO_GAME()
		CLEAR_TRANSITION_SESSIONS_DO_TUT_TRANSITION_TO_GAME()
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		NETWORK_SKIP_RADIO_RESET_NEXT_CLOSE()
		PRINTLN("RESET_FMMC_MISSION_VARIABLES - NETWORK_IS_ACTIVITY_SESSION = TRUE, calling NETWORK_SKIP_RADIO_RESET_NEXT_CLOSE.")
	ENDIF
	
	//If the ped is not injured then remove his parachute
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED() //Lukasz: added this check as removing parachute was also removing heist bag, see B*2116175
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		CLEAN_UP_PLAYER_PED_PARACHUTE()
	ENDIF
	
	//Clear that the language is unsupported
	PRINTLN("g_FMMC_STRUCT.bPlayerLangNotEqualCreated = FALSE")
	g_FMMC_STRUCT.bPlayerLangNotEqualCreated = FALSE
	
	// Reset last damager vars
	PRINTLN(" RESET_FMMC_MISSION_VARIABLES,  [CS_DAMAGE] g_iLastDamagerPlayer 	= -1, g_weaponUsedLastDamager	= -1 ")
	RESET_LAST_DAMAGER()
	g_i_ActualLeaderboardPlayerSelected = -1
	g_iLastKillerId = -1
	g_iDMObjective = -1
	g_iMyDMObjectiveOutcome = -1
	g_i_NumDpadLbdPlayers = 0
	g_i_Headshots = 0
	g_b_SkipKillStrip = FALSE
	bNeedLbdCleanup = FALSE
	g_b_DoubleXpFinalKill = FALSE
	SET_CORONA_CLEANED_UP_BET_INTRO_CAM_GLOBAL(FALSE)
	RESET_DEATH_STREAK()
	//GlobalplayerBD_FM[iPlayerSlot].bQuitJob = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[iPlayerSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
	g_f_MyDamageTaken = 0
	g_f_MyDamageDealt = 0
	g_b_CoronaCleanedIntroCam = FALSE
	g_i_CoronaHighestRankPlayer = -1
	//gdisablerankupmessage 	= FALSE
	SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	g_b_onNewJobVoteScreen = FALSE
	g_i_RPKillsTotalThisJob = 0
	
	CLEAR_TIME_OF_DAY_FOR_QUICK_RESTART()
	// Audio
	SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
	SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
	TOGGLE_STRIPPER_AUDIO(TRUE)
		
	//Clear the tutorial flag
	IF SHOULD_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()
		CLEAR_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()
	ENDIF
	
	//Deal with the player being on a tutorial
    IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		//If I'm on a race
		IF IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
			//Check to see if I need to do a quick match 
			//And there's less than two in my session
			IF NETWORK_GET_TOTAL_NUM_PLAYERS() < 2
				SET_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()			
				SHOW_GAME_TOOL_TIPS(TRUE)
	  			PRINTLN("[TS] SHOW_GAME_TOOL_TIPS(TRUE) called")
			ELSE
				SET_TRANSITION_SESSIONS_DO_TUT_TRANSITION_TO_GAME()
				PRINTLN("[TS] SET_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH - NETWORK_GET_TOTAL_NUM_PLAYERS = ", NETWORK_GET_TOTAL_NUM_PLAYERS())
			ENDIF	
			//Set the player data
			IF g_sFMMCEOM.iVoteStatus != -1
				SET_UGC_PLAYER_DATA_TUTORIAL()
	  			PRINTLN("[TS] SET_UGC_PLAYER_DATA_TUTORIAL() called")
			ENDIF
		//If I'm on a mission
		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
			//Set the player data
			IF g_sFMMCEOM.iVoteStatus != -1
				SET_UGC_PLAYER_DATA_TUTORIAL()
	  			PRINTLN("[TS] SET_UGC_PLAYER_DATA_TUTORIAL() called")
			ENDIF	
		ENDIF	
	ENDIF

	//If we are forcing the quickmatch thing
	#IF IS_DEBUG_BUILD
	IF g_sTransitionSessionData.bForceTransitionSessionQuickMatch
		SET_TRANSITION_SESSIONS_DO_TUT_MISSION_QUICKMATCH()
	ENDIF
	#ENDIF
	
	BOOL bOnPlayList =  IS_PLAYER_ON_A_PLAYLIST_INT(iPlayerSlot)
	GlobalplayerBD_FM[iPlayerSlot].iKillStreak = 0
	
	//reset the teams
	IF bOnPlayList
	AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		RESET_TEAMS_FOR_PLAYLIST()
	ENDIF
	
	IF NOT bOnPlayList
		SET_ON_LBD_GLOBAL(FALSE)
	ENDIF
		
	IF GET_CORONA_STATUS() != CORONA_STATUS_WALK_OUT 
	AND GET_CORONA_STATUS() != CORONA_STATUS_WARP_TO_SAFE_LOC 	
	AND NOT TRANSITION_SESSIONS_CLEANUP_AFTER_JIP_BACK_OUT()
		//Clear the players initial spawn
		g_vInitialSpawnLocation = << 0.0, 0.0, 0.0 >>
		PRINTLN("g_vInitialSpawnLocation = ", g_vInitialSpawnLocation)
	ENDIF
	
	IF SHOULD_LOCAL_PLAYER_STAY_IN_ARENA_BOX_BETWEEN_ROUNDS()
	AND DID_I_JOIN_MISSION_AS_SPECTATOR()
		PRINTLN("[LM][SPEC_SPEC][JIP] - Flagged that we want to stay in Spec Box so not clearing JOIN_MISSION_AS_SPECTATOR")
	ELSE
		PRINTLN("[LM][SPEC_SPEC][JIP] - We do not want to stay as a spectator. Clearing JIP Status.")
		CLEAR_I_JOIN_MISSION_AS_SPECTATOR()
	ENDIF
	
	CLEAR_SPEC_BAILED_FOR_TRANSITION()
	
	SHOW_GAME_TOOL_TIPS(FALSE)
	PLAYER_INDEX piPlayer
	//Call Keith's functions based on the vote status
	PRINTLN("g_sFMMCEOM.iVoteStatus = ", g_sFMMCEOM.iVoteStatus)
	
	BOOL bServeBlockJoin = FALSE
	IF NOT bOnPlayList
		IF g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUIT
			bServeBlockJoin = TRUE
		ENDIF
		IF NOT ARE_TUTORIAL_INVITES_ARE_BLOCKED()
			SET_BLOCKING_STATUS_OF_INVITES_AND_REQUESTS(FALSE, bServeBlockJoin)
		ENDIF
	ENDIF
	
	// 2089419
	PRINTLN("[CHAT_FUNC] RESET_FMMC_MISSION_VARIABLES, NETWORK_SET_PROXIMITY_AFFECTS_TEAM(FALSE)")
	NETWORK_SET_PROXIMITY_AFFECTS_TEAM(FALSE)

	IF NETWORK_IS_GAME_IN_PROGRESS()
		SWITCH g_sFMMCEOM.iVoteStatus
			//Vote for continue
			CASE ciFMMC_EOM_VOTE_STATUS_CONTINUE
				//set that we should skip the next flow call if it's not a flow mission we're on!
				IF NOT IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
				AND GET_FM_STRAND_PROGRESS(ciFLOW_STRAND_LOW_RIDER) > 0
					CPRINTLN(DEBUG_MP_UNLOCKS, "[LOWFLOW] ciFMMC_EOM_VOTE_STATUS_CONTINUE - g_sFmFlowProgressVars.sFmStrandProgressVars[ciFLOW_STRAND_LOW_RIDER].bShortUnLockCall = TRUE") 
					g_sFmFlowProgressVars.sFmStrandProgressVars[ciFLOW_STRAND_LOW_RIDER].bShortUnLockCall = TRUE
				ENDIF
				
				IF NOT bOnPlayList
					RESET_CHAT_PROXIMITY()
					CLEAR_FM_JOB_ENTERY_TYPE()
				ENDIF
			BREAK
			
			//We quit out of the mission
			CASE ciFMMC_EOM_VOTE_STATUS_QUIT
				g_iTotalMissionsPlayedInRow = 0
				PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_iTotalMissionsPlayedInRow = ", g_iTotalMissionsPlayedInRow)
				IF NOT bOnPlayList
					RESET_CHAT_PROXIMITY()
				ENDIF
				CLEAR_FM_JOB_ENTERY_TYPE()
				//Clear flag for warp 
				g_sFMMCEOM.bDoWarp = FALSE 
				g_sFMMCEOM.vWarp = <<0.0, 0.0, 0.0>>			
				//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bMissionRestartInProgress = FALSE
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bMissionRestartInProgress)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].tl23EOMRestartContentID = ""
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vEOMPos = <<0.0, 0.0, 0.0>>	
				
				SET_LOCAL_PLAYER_STAYS_IN_ARENA_BOX_BETWEEN_ROUNDS(FALSE)
				
				PRINTLN("RESET_FMMC_MISSION_VARIABLES - FMMC EOM - sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUIT")
				PRINTLN("RESET_FMMC_MISSION_VARIABLES - FMMC EOM - g_sFMMCEOM.vWarp        = <<0.0, 0.0, 0.0>>")
				PRINTLN("RESET_FMMC_MISSION_VARIABLES - FMMC EOM - g_sFMMCEOM.bDoWarp      = FALSE")
				PRINTLN("RESET_FMMC_MISSION_VARIABLES - FMMC EOM - tl23EOMRestartContentID = NULL")
				PRINTLN("RESET_FMMC_MISSION_VARIABLES - FMMC EOM - vEOMPos                 = <<0.0, 0.0, 0.0>>")
			BREAK
			
			//we voted for a mission on the end of job vote screen!
			CASE ciFMMC_EOM_VOTE_STATUS_RANDOM			
				// Allow everyone to talk to everyone
				CLIENT_SET_UP_EVERYONE_CHAT()			
				// Keith 2/7/13: Let Keith know the contetnID of the randomly chosen mission so it can be prioritised and unlocked after the transition
				IF NOT SHOULD_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
				OR (SHOULD_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
				AND IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.tl31LastContentID))
					Mission_Shared_Next_Mission_Randomly_Chosen(g_sFMMCEOM.contentID)
					Unblock_One_MissionsAtCoords_Mission(g_sFMMCEOM.contentID)
				ENDIF
				piPlayer = INT_TO_NATIVE(PLAYER_INDEX, GlobalplayerBD_FM[iPlayerSlot].iLastVoteCaptain)
				BOOL bUseLocal
				IF piPlayer != INVALID_PLAYER_INDEX()
					IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
						g_TransitionSessionNonResetVars.ghLastVoteCaptain = GET_GAMER_HANDLE_PLAYER(piPlayer)		
					ELSE
						bUseLocal = TRUE
						PRINTLN("iLastVoteCaptain - RESET_FMMC_MISSION_VARIABLES - NETWORK_IS_PLAYER_ACTIVE = FALSE")
					ENDIF
				ELSE
					bUseLocal = TRUE
					PRINTLN("iLastVoteCaptain - RESET_FMMC_MISSION_VARIABLES - piPlayer = INVALID_PLAYER_INDEX()")
				ENDIF
				
				//Use the local player
				IF bUseLocal
				AND SHOULD_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
				AND NETWORK_GET_NUM_PARTICIPANTS() = 1
					PRINTLN("iLastVoteCaptain - RESET_FMMC_MISSION_VARIABLES - piPlayer = GET_LOCAL_GAMER_HANDLE()")
					g_TransitionSessionNonResetVars.ghLastVoteCaptain = GET_LOCAL_GAMER_HANDLE()
				ENDIF
				
				//If the player was on a team mission then set that he was
				IF g_FMMC_STRUCT.iMaxNumberOfTeams > 1
				AND NOT DOES_THIS_ROCKSTAR_MISSION_REQUIRE_ASYMMETRIC_TEAM_BALANCING()
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_CrossTheLine(g_FMMC_STRUCT.iRootContentIDHash)
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIDHash)
				AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen > -1
				AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen < FMMC_MAX_TEAMS				
					SET_LAST_MISSION_WAS_TEAM(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
				ELSE
					 CLEAR_LAST_MISSION_WAS_TEAM()
				ENDIF
			BREAK
			
			//Restart, keep the same teams
			CASE ciFMMC_EOM_VOTE_STATUS_RESTART			
				// Allow everyone to talk to everyone
				CLIENT_SET_UP_EVERYONE_CHAT()			
				bRetainPropertyDetailsOnRestart = TRUE
				// KGM 27/4/13: Let Keith know the mission is being played again - this is needed for playing another player's UGC when the UGC owner decides not to play again
				Mission_Shared_Mission_Being_Played_Again()			
				piPlayer = INT_TO_NATIVE(PLAYER_INDEX, GlobalplayerBD_FM[iPlayerSlot].iLastVoteCaptain)
				IF piPlayer != INVALID_PLAYER_INDEX()
					IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
						g_TransitionSessionNonResetVars.ghLastVoteCaptain = GET_GAMER_HANDLE_PLAYER(piPlayer)			
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("iLastVoteCaptain - RESET_FMMC_MISSION_VARIABLES - NETWORK_IS_PLAYER_ACTIVE = FALSE")
						#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("iLastVoteCaptain - RESET_FMMC_MISSION_VARIABLES - piPlayer = INVALID_PLAYER_INDEX()")
				#ENDIF
				ENDIF
				//If the player was on a team mission then set that he was
				IF g_FMMC_STRUCT.iMaxNumberOfTeams > 1
				AND NOT DOES_THIS_ROCKSTAR_MISSION_REQUIRE_ASYMMETRIC_TEAM_BALANCING()
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_CrossTheLine(g_FMMC_STRUCT.iRootContentIDHash)
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIDHash)
					SET_LAST_MISSION_WAS_TEAM(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
				ELSE
					 CLEAR_LAST_MISSION_WAS_TEAM()
				ENDIF
			BREAK
			
			//If this is a quick restart corona
			CASE ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART				
				// Allow everyone to talk to everyone
				CLIENT_SET_UP_EVERYONE_CHAT()			
				//CDM 2013-8-19 refill property invite struct if restarting a mission
				bRetainPropertyDetailsOnRestart = TRUE
				
				
				//reset these globals on quick retry
				//IF IS_A_STRAND_MISSION_BEING_INITIALISED()
				IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
					//here shitty
					SET_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
				ELIF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
					PRINTLN("RESET_FMMC_MISSION_VARIABLES - On SVM mission that is quick restarting, let's keep the cash grab variables.")
				ELSE
					IF NOT (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
					OR NOT CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
						//Cleanup the globals used in cash grab
						PRINTLN("RESET_FMMC_MISSION_VARIABLES - Cleaning up cash grab globals as we're on a quick restart and and first strand mission has not been passed.")
						g_TransitionSessionNonResetVars.iTotalCashGrabTake 	= 0
						g_TransitionSessionNonResetVars.iTotalCashGrabDrop 	= 0
						PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_TransitionSessionNonResetVars.iTotalCashGrabTake = ", g_TransitionSessionNonResetVars.iTotalCashGrabTake)
						PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_TransitionSessionNonResetVars.iTotalCashGrabDrop = ", g_TransitionSessionNonResetVars.iTotalCashGrabDrop)
					ELSE
						PRINTLN("RESET_FMMC_MISSION_VARIABLES - Not cleaning up g_TransitionSessionNonResetVars.iTotalCashGrabTake")
						PRINTLN("RESET_FMMC_MISSION_VARIABLES - Not cleaning up g_TransitionSessionNonResetVars.iTotalCashGrabDrop")
					ENDIF
					IF NOT CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
						g_TransitionSessionNonResetVars.bInvolvedInCashGrab 	= FALSE
						g_TransitionSessionNonResetVars.bDisplayCashGrabTake 	= FALSE
						PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_TransitionSessionNonResetVars.bInvolvedInCashGrab  = ", g_TransitionSessionNonResetVars.bInvolvedInCashGrab)
						PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_TransitionSessionNonResetVars.bDisplayCashGrabTake = ", g_TransitionSessionNonResetVars.bDisplayCashGrabTake)
					ENDIF
				ENDIF
				IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
				OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
					//Incrament the quick restart stuff:
					g_sJobHeistInfo.m_usedQuickRestart++
					PRINTLN("[TEL] * g_sJobHeistInfo.m_usedQuickRestart++ - g_sJobHeistInfo.m_usedQuickRestart = ", g_sJobHeistInfo.m_usedQuickRestart)
				ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
					g_sJobGangopsInfo.m_infos.m_usedQuickRestart++
					IF g_sJobGangopsInfo.m_QuickRestart = 0
						g_sJobGangopsInfo.m_QuickRestart = 1
					ENDIF
					PRINTLN("[JS][GOTEL][TEL] - RESET_FMMC_MISSION_VARIABLES - g_sJobGangopsInfo.m_QuickRestart = ", g_sJobGangopsInfo.m_QuickRestart, " g_sJobGangopsInfo.m_infos.m_usedQuickRestart: ", g_sJobGangopsInfo.m_infos.m_usedQuickRestart)
				ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
					g_sCasino_Heist_Finale_Telemetry_data.replay++
					PRINTLN("[JS][GOTEL][TEL] - RESET_FMMC_MISSION_VARIABLES - g_sCasino_Heist_Finale_Telemetry_data.replay = ", g_sCasino_Heist_Finale_Telemetry_data.replay)
				ELIF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
					g_sIsland_Heist_Finale_Telemetry_data.replay++
					PRINTLN("[JS][TEL] - RESET_FMMC_MISSION_VARIABLES - g_sIsland_Heist_Finale_Telemetry_data.replay = ", g_sIsland_Heist_Finale_Telemetry_data.replay)
				#IF FEATURE_TUNER
				ELIF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)	
					OR (IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)  )
					OR (IS_THIS_A_FIXER_SHORT_TRIP() AND IS_CORONA_BIT_SET(CORONA_FIXER_SHORT_TRIP_INTRO))
					g_sTuner_Robbery_Finale_Telemetry_data.replay++
					PRINTLN("[JS][TEL] - RESET_FMMC_MISSION_VARIABLES - g_sTuner_Robbery_Finale_Telemetry_data.replay = ", g_sTuner_Robbery_Finale_Telemetry_data.replay)
				#ENDIF
				ENDIF
				
				IF  VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
					IF g_sCasino_Story_Telemetry_data.m_UsedQuickRestart = 0
						g_sCasino_Story_Telemetry_data.m_UsedQuickRestart = 1
					ENDIF
				ENDIF
				
				//Set the TOD for the quick restart
				//If the flag is not set to maintain TOD on the first play
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciLOCK_TOD_FIRST_PLAY_ONLY)
					IF GET_TIME_OF_DAY_FOR_QUICK_RESTART_FROM_RULE() = -1
					
						//If it's not force to a specfic TOD
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
							SET_TIME_OF_DAY_FOR_QUICK_RESTART(g_FMMC_STRUCT.iTimeOfDay)
						//it is force to a specfic time of day
						ELSE
							SET_SPECFIC_TIME_OF_DAY_FOR_QUICK_RESTART(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes)
						ENDIF
						//Set the current time ofday to avoid a flash
						IF g_sFMMCEOM.iTodQuickRestartHours > -1
							NETWORK_OVERRIDE_CLOCK_TIME(g_sFMMCEOM.iTodQuickRestartHours,  g_sFMMCEOM.iTodQuickRestartMinutes, 0)
						ELIF GET_TIME_OF_DAY_FOR_QUICK_RESTART() > -1
							PRINTLN("SET_TIME_OF_DAY(GET_TIME_OF_DAY_FOR_QUICK_RESTART())")
							SET_TIME_OF_DAY(GET_TIME_OF_DAY_FOR_QUICK_RESTART())
						ENDIF
					ELSE
						PRINTLN("NETWORK_OVERRIDE_CLOCK_TIME Using Rule Options from previous quick restart.")
						NETWORK_OVERRIDE_CLOCK_TIME(GET_TIME_OF_DAY_FOR_QUICK_RESTART_FROM_RULE(), 0, 0)
					ENDIF
				ENDIF
				IF SHOULD_STRAND_MISSION_RESTART_ON_FIRST_MISSION()
					TEXT_LABEL_23 tl23StrandRestart
					tl23StrandRestart = GET_FIRST_STRAND_MISSION_CONTENT_ID()
					Quick_Restart_Strand_Mission_Part_One(tl23StrandRestart)	
					//reinit our team
					IF GET_STRAND_MISSION_FIRST_MISSION_TEAM() != -1
						GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = GET_STRAND_MISSION_FIRST_MISSION_TEAM()
					ENDIF
					PRINTLN("[TS] [MSRAND] - SHOULD_STRAND_MISSION_RESTART_ON_FIRST_MISSION - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = GET_STRAND_MISSION_FIRST_MISSION_TEAM() = ", GET_STRAND_MISSION_FIRST_MISSION_TEAM())	
					CLEAR_STRAND_MISSION_FIRST_MISSION_TEAM()
					CLEAR_FIRST_STRAND_MISSION_BEEN_PASSED()
				ELSE
					// KGM 27/4/13: Let Keith know the mission is being played again - this is needed for playing another player's UGC when the UGC owner decides not to play again
					Mission_Shared_Mission_Being_Played_Again()
				ENDIF
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [TS] [MSROUND] - SET_IN_SPECTATOR_MODE FALSE")
					NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE) 
					SET_IN_SPECTATOR_MODE(FALSE)
				ENDIF
			BREAK
			
			//Are we swapping teams on a quick restart
			CASE ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP				
				// Allow everyone to talk to everyone
				CLIENT_SET_UP_EVERYONE_CHAT()			
				bRetainPropertyDetailsOnRestart = TRUE
				// KGM 27/4/13: Let Keith know the mission is being played again - this is needed for playing another player's UGC when the UGC owner decides not to play again
				Mission_Shared_Mission_Being_Played_Again()
			BREAK
			
			//If it's a strand mission then we need to set that up.
			CASE ciFMMC_EOM_VOTE_STATUS_STRAND_MISSION
				PRINTLN("[TS] [MSRAND] - RESET_FMMC_MISSION_VARIABLES - ciFMMC_EOM_VOTE_STATUS_STRAND_MISSION")
				// Keith 2/7/13: Let Keith know the contetnID of the next chosen mission so it can be prioritised and unlocked after the transition
				IF NOT SHOULD_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
					Mission_Shared_Next_Mission_Randomly_Chosen(g_sFMMCEOM.contentID)	
				ENDIF
			BREAK
			
			//If it's a rounds mission
			CASE ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION			
				PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES - ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION")
				// Allow everyone to talk to everyone
				CLIENT_SET_UP_EVERYONE_CHAT()			
				//CDM 2013-8-19 refill property invite struct if restarting a mission
				bRetainPropertyDetailsOnRestart = TRUE
				// KGM 27/4/13: Let Keith know the mission is being played again - this is needed for playing another player's UGC when the UGC owner decides not to play again
				Mission_Shared_Mission_Being_Played_Again()
				Unblock_One_MissionsAtCoords_Mission(g_FMMC_STRUCT.tl31LoadedContentID)
				
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].tl23EOMRestartContentID = g_FMMC_STRUCT.tl31LoadedContentID
				
				IF SHOULD_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING()
					PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES - ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION - SHOULD_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING")
					CLEAR_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING()
				ENDIF
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [TS] [MSROUND] - SET_IN_SPECTATOR_MODE FALSE")
					NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE) 
					SET_IN_SPECTATOR_MODE(FALSE)
				ENDIF
			BREAK			
			//If it's a heist cut vote?
			CASE ciFMMC_EOM_VOTE_STATUS_HEIST_CUT
				PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES - ciFMMC_EOM_VOTE_STATUS_HEIST_CUT")
				TEXT_LABEL_23 tl23Null
				SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_HEIST_CUT, <<0.0, 0.0, 0.0>>, tl23Null)
				SET_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE()
			BREAK
		ENDSWITCH
	ELSE
		g_iTotalMissionsPlayedInRow = 0
		PRINTLN("RESET_FMMC_MISSION_VARIABLES - g_iTotalMissionsPlayedInRow = ", g_iTotalMissionsPlayedInRow)
	  	PRINTLN("[TS] RESET_FMMC_MISSION_VARIABLES - setting up vote stuff - NETWORK_IS_GAME_IN_PROGRESS = FALSE")			
	ENDIF
	
	IF bRetainPropertyDetailsOnRestart
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
		
		//CDM 2013-8-19 refill property invite struct if restarting a mission
		IF Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())	// KGM 5/3/14: Added Heist Planning mission check too
			IF misRestartStoredPropertyData.iPackedInt > 0
				g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt = misRestartStoredPropertyData.iPackedInt
				g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.ownerHandle = misRestartStoredPropertyData.ownerHandle
				//misRestartStoredPropertyData.misRestartStoredPropertyData = TRUE
				#IF IS_DEBUG_BUILD
				PRINTLN("5 setting sInviteToPropertyDetails.iPackedInt = ",g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt )
				DEBUG_PRINT_GAMER_HANDLE(g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.ownerHandle)
				PRINTLN("CDM: TRANSITION_SESSION_PROPERTY_INVITE_DETAILS set by restart of heist/contact mission")
				#ENDIF
			ENDIF
		ELSE
			PRINTLN("CDM: TRANSITION_SESSION_PROPERTY_INVITE_DETAILS cleared by restart of mission")
			CLEAR_TRANSITION_SESSION_PROPERTY_INVITE_DETAILS()
		ENDIF
	ENDIF
	
	//clear a flag that everyone is ready
	g_TransitionSessionNonResetVars.sTransVars.sLastSessionPlayers.bEveryBodyReady = FALSE
	
	// Reset dm first strike global
	CLEAR_BIT(g_sDmGlobalVars.iBitSet, ciDM_GLOBAL_BITSET_FIRST_STRIKE)
	
	ACTIVATE_KILL_TICKERS(FALSE) // g_bCompKillTickers
	g_b_PopulateReady = FALSE
	g_i_PowerPlayer = -1
	
	iplayerjoinbitset = 0
	g_bBlockRankuphudendofactivity = FALSE
	g_sFMMCEOM.bPostToFacebook = FALSE
	g_sFMMCEOM.bGotFBResult = FALSE
	
	//Set that the player quit out. 
	IF IS_TRANSITION_SESSION_QUITING_CORONA() = FALSE
		IF g_sFMMCEOM.iVoteStatus = -1
		AND bOnPlayList = FALSE
			SET_UGC_PLAYER_DATA_PLAYER_QUIT_MISSION()
		ELSE
			g_sFMMCEOM.iVoteStatus = -1
		ENDIF
	ENDIF
	
	CLEAR_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED()
	
	//Clear that the radar needs to hide the radar
	IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
		CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
	ENDIF
	
	//clear the flag that we are restarting
	IF HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
		CLEAR_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
	ENDIF
	
	//if it's not an activity session then make sure to turn the store back on
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		SET_STORE_ENABLED(TRUE)
		PRINTLN("[TS] RESET_FMMC_MISSION_VARIABLES - SET_STORE_ENABLED(TRUE)")
	ENDIF
	
	//If we are not quitting the corona
	IF IS_TRANSITION_SESSION_QUITING_CORONA() = FALSE
		IF bSetNeedsCleanup
		AND iPassFailStatus <> ciFMMC_END_OF_MISSION_STATUS_SCTV_LEAVING	//added by david g, 01/03/2013 for 1157393, to stop BbNeedsFmCleanup being set
			IF NOT IS_AREA_TRIGGERED_MISSION(GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType)
			AND NOT IS_PLAYER_ON_IMPROMPTU_DM()
			AND NOT bOnPlayList
			AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
			//AND GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType < FMMC_TYPE_MG
				NET_PRINT("[WJK] - FMMC_Launcher - RESET_FMMC_MISSION_VARIABLES - iCurrentMissionType is not an area triggered mission. iCurrentMissionType = ")NET_PRINT_INT(GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType)NET_NL()
				SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(TRUE)
				IF bComingOutOfShootingRange = bComingOutOfShootingRange
					// Stop compiler moaning.
				ENDIF
			ELSE
				NET_PRINT("[WJK] - FMMC_Launcher - RESET_FMMC_MISSION_VARIABLES - iCurrentMissionType is an area triggered mission. iCurrentMissionType = ")NET_PRINT_INT(GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType)NET_NL()
			ENDIF
		ENDIF
	//We are quitting the corona then tell the logs
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[TS] [WJK] - IS_TRANSITION_SESSION_QUITING_CORONA - Not running Leaderboard clean up")
	#ENDIF
	ENDIF
	
	CLEAR_NETWORK_CLEAR_TRANSITION_CREATOR_HANDLE()
	
	#IF IS_DEBUG_BUILD
	IF g_sFMMCEOM.bPostToFacebook 
		PRINTLN("g_sFMMCEOM.bPostToFacebook = TRUE")
	ENDIF
	#ENDIF
	
	g_sFMMCEOM.bPostToFacebook = FALSE

	//clean up the mission decarators
	IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		CLEAN_UP_FM_MISSION_DECORATORS()
	ELSE
		PRINTLN("CLEAN_UP_FM_MISSION_DECORATORS = FALSE because IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL")
	ENDIF
	
	IF GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType != FMMC_TYPE_INVALID
		//UPDATE_FMMC_END_OF_MISSION_STATUS(iPassFailStatus, GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType)
		iPassFailStatus = iPassFailStatus
		IF IS_THIS_A_MINI_GAME(GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType)
			IF NETWORK_IS_SIGNED_ONLINE()  
				INT IncrementBy = INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY()
				INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_MINIGAME_END)
				INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_END,IncrementBy )
				IF IncrementBy > 0
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_ENDED, 1)
				ENDIF
			ENDIF
		
		ENDIF
		
	ENDIF
	
	
	

	IF NETWORK_IS_GAME_IN_PROGRESS()
		NETWORK_ENABLE_VOICE_BANDWIDTH_RESTRICTION(PLAYER_ID())
		PRINTLN("NETWORK_ENABLE_VOICE_BANDWIDTH_RESTRICTION(PLAYER_ID())")
	ENDIF
	
	CLEAR_REPLAY_JOB_HELP()
	
	IF g_sFMMCEOM.bDoWarp = FALSE
		IF NOT bOnPlayList
			PRINTLN("IF NOT bOnPlayList - GlobalplayerBD_FM[iMyGBD].iLastVoteCaptain = -1")
			GlobalplayerBD_FM[iPlayerSlot].iLastVoteCaptain = -1
			//GlobalplayerBD_FM[iPlayerSlot].bMissionRestartInProgress = FALSE
			CLEAR_BIT(GlobalplayerBD_FM[iPlayerSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bMissionRestartInProgress)
		ENDIF
	ENDIF
	
	//We are on a Playlist.
	IF bOnPlayList		
		SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(FALSE)
		//GlobalplayerBD_FM[iPlayerSlot].bMissionRestartInProgress = TRUE
		SET_BIT(GlobalplayerBD_FM[iPlayerSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bMissionRestartInProgress)
		GlobalplayerBD_FM[iPlayerSlot].iLastVoteCaptain = GET_PLAYERS_HOST_OF_LAST_CORONA(PLAYER_ID())
		PRINTLN("GlobalplayerBD_FM[iPlayerSlot].iLastVoteCaptain = ", GlobalplayerBD_FM[iPlayerSlot].iLastVoteCaptain)
		// Turn off the spec cam if it is running.
		NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE) 
		SET_IN_SPECTATOR_MODE(FALSE)
		NET_PRINT("[WJK] - - RESET_FMMC_MISSION_VARIABLES - GLOBAL_SPEC_BS_WAS_LAST_DEACTIVATED_BY_QUIT_SCREEN bit is set, deactivating spec cam with natives.")NET_NL()
		
		//Set that the playlist has done the initial transition!
		IF DID_I_LEAVE_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
			SET_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
		ENDIF
	ENDIF

	IF GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType < FMMC_TYPE_MG
	AND GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType != -1
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF NOT IS_STRING_NULL_OR_EMPTY(GlobalplayerBD_FM[iPlayerSlot].tl23CurrentMissionOwner)
		AND GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType != FMMC_TYPE_IMPROMPTU_DM
		AND GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType != FMMC_TYPE_GB_BOSS_DEATHMATCH 
			PRINTLN("GlobalplayerBD_FM[iPlayerSlot].tl23CurrentMissionOwner = ", GlobalplayerBD_FM[iPlayerSlot].tl23CurrentMissionOwner)
			CHECK_AND_REWARD_PLAYING_FRIENDS_CREATION(GlobalplayerBD_FM[iPlayerSlot].tl23CurrentMissionOwner)
		ENDIF
	ENDIF
	
	IF GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType != FMMC_TYPE_GANGHIDEOUT
		PRINTLN("Setting g_bFreemodeCleanUpAfterMission to TRUE")PRINTNL()
		g_bFreemodeCleanUpAfterMission = TRUE
	ENDIF
	
	GlobalplayerBD_FM[iPlayerSlot].iCurrentMissionType 							= FMMC_TYPE_INVALID
	GlobalplayerBD_FM[iPlayerSlot].currentMissionData.mdID.idCreator			= -1
//	GlobalplayerBD_DM[iPlayerSlot].bInDeathmatch 								= FALSE
	GlobalplayerBD_FM[iPlayerSlot].currentMissionData.iInstanceId				= DEFAULT_MISSION_INSTANCE
	GlobalplayerBD_FM[iPlayerSlot].currentMissionData.mdID.idVariation 			= FMMC_TYPE_INVALID
	
	g_FMMC_STRUCT.iMissionType = 0
	 
//	// set up proximity chat
//	//On a playlist set it to everyone
//	IF bOnPlayList		
//		CLIENT_SET_UP_EVERYONE_CHAT()
//	//not on a playlist then clean it up
//	ELSE
//		CLIENT_SET_UP_PROXIMITY_CHAT(CHAT_PROXIMITY_FM)
//	ENDIF
	IF NOT bOnPlayList
		ALLOW_SPECTATORS_TO_CHAT(FALSE)
	ENDIF
	GlobalplayerBD_FM[iPlayerSlot].currentMissionData.iBitSet = 0
	PRINTLN("GlobalplayerBD_FM[", iPlayerSlot, "].currentMissionData.iBitSet = 0")
	
	// Reset XP job ticker flag (1212357)
	CLEAR_BIT(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iXPBitset, ciGLOBAL_XP_BIT_ON_JOB)
	//clear the mission names
	SET_MISSION_NAME_FOR_UGC_MISSION(FALSE)
	
	IF iMissionType != FMMC_TYPE_GANGHIDEOUT
	AND iMissionType != FMMC_TYPE_GB_BOSS_DEATHMATCH // 2574713
		THEFEED_FLUSH_QUEUE()
		 g_b_ReapplyStickySaveFailedFeed = FALSE
		PRINTLN("THEFEED_FLUSH_QUEUE()")
	ENDIF
	
	STRING stNull
	IF NOT bOnPlayList
		GlobalplayerBD_FM[iPlayerSlot].tl23CurrentMissionOwner 		= stNull 
		GlobalplayerBD_FM[iPlayerSlot].tl31CurrentMissionFileName 	= stNull
		FM_PLAY_LIST_CLEAN_VARS()
	ENDIF
	GlobalplayerBD_MissionName[iPlayerSlot].tl63MissionName 	= stNull
	GlobalplayerBD_FM[iPlayerSlot].tl61MissionDescription 		= stNull
	
	SCRIPT_TIMER sCoronaTimerTemp
	GlobalplayerBD_FM_2[iPlayerSlot].sCoronaTimer = sCoronaTimerTemp
	GlobalplayerBD_FM_2[iPlayerSlot].sCoronaTimer2 = sCoronaTimerTemp

	g_bFM_ON_TEAM_DEATHMATCH = FALSE
	PRINTLN("g_bFM_ON_TEAM_DEATHMATCH		= FALSE")
	g_bFM_ON_TEAM_MISSION = FALSE
	PRINTLN("g_bFM_ON_TEAM_MISSION		= FALSE")
	
	

	PRINTLN("[overhead] g_iOverheadNamesState = -1 - RESET ")
	g_iOverheadNamesState = -1

	// Check for already being in a tutorial session due to being on the race / dm tutorial
	BOOL bInTutorialSessionForRaceOrDMTtutorial = FALSE
//	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
//	//	IF NETWORK_IS_IN_TUTORIAL_SESSION()
//		//	bInTutorialSessionForRaceOrDMTtutorial = TRUE
//		//	#IF IS_DEBUG_BUILD
//		//		PRINTLN("[dsw] [RESET_FMMC_MISSION_VARIABLES] bInTutorialSessionForRaceOrDMTtutorial = TRUE")
//		//	#ENDIF
//	//	ENDIF
//	ENDIF  
	CLEAR_TRANSITION_SESSION_MISSION_LOADED_BEFORE_LAUNCH()
	CLEAR_TRANSITION_SESSIONS_MISSION_HAS_LOADED()
	CLEAR_TRANSITION_SESSION_EOM_VARS() 
	//If we quit the current playlist
	IF TRANSITION_SESSIONS_QUIT_CURRENT_PLAYLIST()
		CLEAR_TRANSITION_SESSIONS_QUIT_CURRENT_PLAYLIST()
	ENDIF

	//clean up stuff aftyer joing a launched mission
	IF SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()
	AND NETWORK_IS_ACTIVITY_SESSION()
		CLEAR_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)	
	ENDIF
	
	//If we are on a playlist then we don't clear up. 
	IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
		PRINTLN("[TS] HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION - Setting - CLEAR_DO_TRANSITION_TO_GAME = FALSE")
		CLEAR_DO_TRANSITION_TO_GAME()
	//Mp invite
	ELIF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
		PRINTLN("[TS] AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP - Setting - CLEAR_DO_TRANSITION_TO_GAME = FALSE")
		CLEAR_DO_TRANSITION_TO_GAME()	
	//Invite invite
	ELIF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
		PRINTLN("[TS] AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE - Setting - CLEAR_DO_TRANSITION_TO_GAME = FALSE")
		CLEAR_DO_TRANSITION_TO_GAME()	
	//Tournament invite
	ELIF IS_TRANSITION_SESSION_ACCEPTING_TOURNAMENT_INVITE_ON_A_JOB()
		PRINTLN("[TS] AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE - Setting - CLEAR_DO_TRANSITION_TO_GAME = FALSE")
		CLEAR_DO_TRANSITION_TO_GAME()	
	//killing stuff, don't need to do transtion to game
	ELIF DOES_TRANSITION_SESSIONS_NEED_TO_CLEANUP_FM_AFTER_INVITE()
		PRINTLN("[TS] DOES_TRANSITION_SESSIONS_NEED_TO_CLEANUP_FM_AFTER_INVITE - Setting - CLEAR_DO_TRANSITION_TO_GAME = FALSE")
		CLEAR_DO_TRANSITION_TO_GAME()	
	//If are we quitting
	ELIF IS_TRANSITION_SESSION_QUITING_CORONA()
		PRINTLN("[TS] IS_TRANSITION_SESSION_QUITING_CORONA - Setting - CLEAR_DO_TRANSITION_TO_GAME = FALSE")
		CLEAR_DO_TRANSITION_TO_GAME()	
	//If are we killing all scripts
	ELIF SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
	AND SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT()
		PRINTLN("[TS] SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS - Setting - CLEAR_DO_TRANSITION_TO_GAME = FALSE")
		CLEAR_DO_TRANSITION_TO_GAME()	
	//If we are going to the heist cut scene in the apartment
	ELIF SHOULD_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE()
		PRINTLN("[TS] SHOULD_LAUNCH_TUTORIAL_MID_STRAND_CUT_SCENE - Setting - CLEAR_DO_TRANSITION_TO_GAME = FALSE")
		CLEAR_DO_TRANSITION_TO_GAME()	
	ELSE
	//Not on a playlist

		CLEAR_LEAVE_MISSION_WITH_EVERYBODY()
		
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bMissionRestartInProgress)
		AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
			//Clean up the activity session if in one
			IF NETWORK_IS_ACTIVITY_SESSION()
				PRINTLN("[TS] NETWORK_IS_ACTIVITY_SESSION = TRUE")
				//If I left at the very end of the mission
				IF DID_I_LEAVE_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
					PRINTLN("[TS] DID_I_LEAVE_MISSION_AT_LEADERBOARD_TIME_OUT = TRUE")
					//Set that I left the mission with everybody
					SET_LEAVE_MISSION_WITH_EVERYBODY()
				ELSE
					//If we've quit then we go alone 
                    PRINTLN("[TS] DID_I_LEAVE_MISSION_AT_LEADERBOARD_TIME_OUT = FALSE")  
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_POST_MISSION_SPAWN_ON_FAIL)
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
							PRINTLN("[PMC] - quitting job, setting vPostMissionPos to current coords = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos)
						ENDIF
					ENDIF
                    CLEAR_BACKED_UP_CORONA_MENU_SELECTION() 
                    CLEAR_USE_BACKED_UP_MENU_SELECTION_ON_RESTART() 
                    SET_TRANSITION_SESSION_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUIT) 
					CLEAR_FM_JOB_ENTERY_TYPE()
                    IF bOnPlayList 
                        PRINTLN("[TS] IS_PLAYER_ON_A_PLAYLIST = TRUE") 
                        SET_PLAYER_ON_A_PLAYLIST(FALSE) 
                    ENDIF
				ENDIF
				
				//Set that we need to clean up
				SET_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()
				SET_TRANSITION_SESSIONS_FREEMODE_DONT_PULL_DOWN_CAM()
				//Set the don't make car flag
				SET_TRANSITION_SESSIONS_PLAYER_VEHICLE_CLEAN_UP_CHECK()
				// Don't want ot be in freemode dead stage.
				IF (g_TransitionSessionNonResetVars.sPostMissionCleanupData.eStage = ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD)
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.eStage = ePOSTMISSIONCLEANUPSTAGE_PREPARE_FOR_FREMODE_DEATH
					PRINTLN("[TS] RESET_FMMC_MISSION_VARIABLES() - g_TransitionSessionNonResetVars.sPostMissionCleanupData.eStage = ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD after call to SET_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH.")
					SCRIPT_ASSERT("RESET_FMMC_MISSION_VARIABLES() - g_TransitionSessionNonResetVars.sPostMissionCleanupData.eStage = ePOSTMISSIONCLEANUPSTAGE_FREEMODE_DEAD after call to SET_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH.")
				ENDIF
				
				IF IS_PLAYER_SCTV(PLAYER_ID())
					SET_TRANSITION_SESSIONS_RELAUNCHING_FREEMODE_AS_SCTV()
				ENDIF
				SET_DO_TRANSITION_TO_GAME()
				//reset the switch stage int
				#IF IS_DEBUG_BUILD
				IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars.iTranisitionSessionSetUpStage != 0
					PRINTLN("[TS] RESET_FMMC_MISSION_VARIABLES() - g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars.iTranisitionSessionSetUpStage != 0")
				ENDIF
				#ENDIF
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars.iTranisitionSessionSetUpStage = 0
			ELSE
				PRINTLN("[TS] NETWORK_IS_ACTIVITY_SESSION = FALSE")
				CLEAR_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()
				CLEAR_DO_TRANSITION_TO_GAME()
			ENDIF
		ELSE
			//If we are not off to do a quick restart corona
			IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()	
			AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("IS_CORONA_INITIALISING_A_QUICK_RESTART = TRUE, not clearing up NETWORK_IS_ACTIVITY_SESSION")
			#ENDIF
			ENDIF
			//Clear stuff up
			CLEAR_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()
			CLEAR_DO_TRANSITION_TO_GAME()
		ENDIF
	ENDIF

	IF NOT HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
	AND NOT HAS_TRANSITION_SESSION_TOLD_FREEMODE_TO_RESTART()
	AND NOT SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT()
	AND NOT SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
	AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
	AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
	AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_SP()
	AND NOT DOES_TRANSITION_SESSIONS_NEED_TO_CLEANUP_FM_AFTER_INVITE()
	AND NOT IS_TRANSITION_SESSION_QUITING_CORONA()
	AND NOT IS_TRANSITION_SESSION_ACCEPTING_TOURNAMENT_INVITE_ON_A_JOB()
	AND NOT IS_PS4_LIVE_STREAM_LAUNCH_ACTIVE()
	
		IF NOT IS_TRANSITION_SESSION_LAUNCHING()
			PRINTLN("[TS] IF NOT IS_TRANSITION_SESSION_LAUNCHING - Clear all vars")
			CLEAR_TRANSITION_SESSIONS_BITSET()
			CLEAR_TRANSITION_SESSION_RESTARTING()
			CLEAR_TRANSITION_SESSION_LAUNCHING()
			RESET_TRANSITION_SESSION_NON_RESET_VARS()
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[TS] RESET_FMMC_MISSION_VARIABLES- IS_TRANSITION_SESSION_LAUNCHING = TRUE, not resettign - CLEAR_TRANSITION_SESSIONS_BITSET - RESET_TRANSITION_SESSION_NON_RESET_VARS")
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[TS] RESET_FMMC_MISSION_VARIABLES - HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION = TRUE, not resettign - CLEAR_TRANSITION_SESSIONS_BITSET - RESET_TRANSITION_SESSION_NON_RESET_VARS")
	#ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	//clear debug launch
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		CLEAR_TRANSITION_DEBUG_LAUNCH_VAR()
		g_sTransitionSessionData.sStrandMissionData.bStartWithOnePlayer = FALSE		
	ENDIF
	#ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
				IF NOT IS_LOCAL_PLAYER_IN_TUTORIAL_SESSION_FOR_INTRO()
					IF NOT bInTutorialSessionForRaceOrDMTtutorial
						IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
							NETWORK_END_TUTORIAL_SESSION()
							#IF IS_DEBUG_BUILD
								PRINTSTRING("--------------- BWW - NETWORK_END_TUTORIAL_SESSION")PRINTNL()	
							#ENDIF
							// Get all the shop scripts to re-initialise 
					        REACTIVATE_ALL_OBJECT_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE() 
					        #IF IS_DEBUG_BUILD
						        REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
							#ENDIF
						ELSE
							PRINTLN("IS_PLAYER_ON_A_PLAYLIST() = FALSE")
						ENDIF
					ELSE
						PRINTLN("bInTutorialSessionForRaceOrDMTtutorial = TRUE")
					ENDIF
				ELSE
					PRINTLN("IS_LOCAL_PLAYER_IN_TUTORIAL_SESSION_FOR_INTRO() = TRUE")
				ENDIF
			ELSE
				PRINTLN("NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING = TRUE")
			ENDIF
		ELSE
			PRINTLN("NETWORK_IS_IN_TUTORIAL_SESSION = FALSE")
		ENDIF
	ELSE
		PRINTLN("NETWORK_IS_GAME_IN_PROGRESS = FALSE")
	ENDIF
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET() // NOT sure why the title is being removed but it is breaking testing missions in the creator. Neil B
		g_FMMC_STRUCT.tl63MissionName = ""
		PRINTLN("g_FMMC_STRUCT.tl63MissionName = \"\"")
	ENDIF
	
//	#IF FEATURE_HEIST_PLANNING
//		#IF IS_DEBUG_BUILD
//		DEBUG_PRINTCALLSTACK()
//		#ENDIF
//		PRINTLN("[AMEC][HEIST_MISC] - RESET_FMMC_MISSION_VARIABLES - BOARD, checking if scaleform is loaded. Index: ", NATIVE_TO_INT(g_HeistSharedClient.PlanningBoardIndex))
//		IF HAS_SCALEFORM_MOVIE_LOADED(g_HeistSharedClient.PlanningBoardIndex)
//			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_HeistSharedClient.PlanningBoardIndex, FALSE)
//				PRINTLN("[AMEC][HEIST_MISC] - RESET_FMMC_MISSION_VARIABLES - UNLOADING PLANNING BOARD!")
//				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_HeistSharedClient.PlanningBoardIndex)
//			ELSE
//				PRINTLN("[AMEC][HEIST_MISC] - RESET_FMMC_MISSION_VARIABLES - PlanningBoardIndex movie does NOT belong to this script, do not clean up.")
//			ENDIF
//		ENDIF
//		g_HeistSharedClient.PlanningBoardIndex = NULL
//		
//		PRINTLN("[AMEC][HEIST_MISC] - RESET_FMMC_MISSION_VARIABLES - MAP, checking if scaleform is loaded. Index: ", NATIVE_TO_INT(g_HeistSharedClient.PlanningMapIndex))
//		IF HAS_SCALEFORM_MOVIE_LOADED(g_HeistSharedClient.PlanningMapIndex)
//			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_HeistSharedClient.PlanningMapIndex, FALSE)
//				PRINTLN("[AMEC][HEIST_MISC] - RESET_FMMC_MISSION_VARIABLES - UNLOADING PLANNING MAP!")
//				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_HeistSharedClient.PlanningMapIndex)
//			ELSE
//				PRINTLN("[AMEC][HEIST_MISC] - RESET_FMMC_MISSION_VARIABLES - PlanningMapIndex movie does NOT belong to this script, do not clean up.")
//			ENDIF
//		ENDIF
//		g_HeistSharedClient.PlanningMapIndex = NULL
//		
//		PRINTLN("[AMEC][HEIST_MISC] - RESET_FMMC_MISSION_VARIABLES - INSTR BTNS, checking if scaleform is loaded. Index: ", NATIVE_TO_INT(g_HeistSharedClient.PlanningInstButtonsIndex))
//		IF HAS_SCALEFORM_MOVIE_LOADED(g_HeistSharedClient.PlanningInstButtonsIndex)
//			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_HeistSharedClient.PlanningInstButtonsIndex, FALSE)
//				PRINTLN("[AMEC][HEIST_MISC] - RESET_FMMC_MISSION_VARIABLES - UNLOADING INSTR BUTTONS!")
//				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_HeistSharedClient.PlanningInstButtonsIndex)
//			ELSE
//				PRINTLN("[AMEC][HEIST_MISC] - RESET_FMMC_MISSION_VARIABLES - PlanningInstButtonsIndex movie does NOT belong to this script, do not clean up.")
//			ENDIF
//		ENDIF
//		g_HeistSharedClient.PlanningInstButtonsIndex = NULL
//		
//		PRINTLN("[AMEC][HEIST_MISC] - RESET_FMMC_MISSION_VARIABLES - Removing heist audio banks.")
//		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_PLANNING_BOARD")
//	#ENDIF
	
	// Reset freemode HUD colour back to blue if currently overriden
	INT iSlot
	IF IS_MP_HUD_COLOUR_OVERRIDDEN(HUD_COLOUR_FREEMODE, iSlot)
		RESET_HUD_COLOUR(iSlot)
	ENDIF
	IF IS_MP_HUD_COLOUR_OVERRIDDEN(HUD_COLOUR_FREEMODE_DARK, iSlot)
		RESET_HUD_COLOUR(iSlot)
	ENDIF
	
	// JA: Make sure the player has left their current betting pool (available for a new one)
	IF NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		PRINTLN("[JA@BETTING] - RESET_FMMC_MISSION_VARIABLES - Cleaning up player from betting pool.")
		BROADCAST_PLAYER_LEFT_BETTING()
	ELIF AM_I_TRANSITION_SESSIONS_LAUNCHING_ARENA_SPECTATE()
		SET_IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA()
	ENDIF

	//Reset that everyone died on the mission last time we played
	CLEAR_SHOW_QUICK_RESTART_OPTION()
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
		CLEAR_STRAND_ROOT_CONTENT_ID_HASH()
	ENDIF
	
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	UNHIDE_PROBLEM_MODELS_FOR_CELEBRATION_SCREEN()
	
	FMMC_MISSIONS_CLEANUP_COMMON(bResetOverride)
		
	// enable prospero activity script routing
#IF FEATURE_GEN9_STANDALONE	
	SET_ACTIVITY_SCRIPT_ROUTING_ENABLED(TRUE)
#ENDIF // FEATURE_GEN9_STANDALONE
	
	// KGM 25/7/12: Re-activate the freemode versions of the script tunable variables
	//				This is necessary because Races and Deathmatches may modify these variables
//	Refresh_MP_Script_Tunables(TUNE_CONTEXT_FM)
ENDPROC

PROC RESET_FM_AMBIENT_SCRIPT_VARIABLES()
	PRINTSTRING("BWW...  RESET_FM_AMBIENT_SCRIPT_VARIABLES CALLED")PRINTNL()
	RESET_MP_AMBIENT_SCRIPT_VARIABLES()
ENDPROC
/// PURPOSE: Checks to see if everyone that was in the lobby has made it into the script.
FUNC BOOL HAVE_ALL_PLAYERS_COMPLETED_FREEMODE_LOBBY()

	#IF IS_DEBUG_BUILD
	NET_PRINT("HAVE_ALL_PLAYERS_COMPLETED_LOBBY: ")
	NET_PRINT(" NETWORK_GET_TOTAL_NUM_PLAYERS() = ") NET_PRINT_INT(NETWORK_GET_TOTAL_NUM_PLAYERS())
	NET_PRINT(" NETWORK_GET_NUM_PARTICIPANTS() = ") NET_PRINT_INT(NETWORK_GET_NUM_PARTICIPANTS())
	NET_NL()
	#ENDIF

	IF NETWORK_GET_TOTAL_NUM_PLAYERS() = NETWORK_GET_NUM_PARTICIPANTS()
		
		
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CONTROL_UPDATING_CHALLENGES()
	IF g_iCurrentActiveChallange > -1
	AND g_iCurrentActiveChallange < ciMAX_AT_ONCE_CHALLANGES 
		PRINTLN("CONTROL_UPDATING_CHALLENGES")
		PRINTLN("     g_iCurrentActiveChallange               = ", g_iCurrentActiveChallange)
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].iExpireTime          = ", g_sChallangeData[g_iCurrentActiveChallange].iExpireTime)			
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].iAwardType           = ", g_sChallangeData[g_iCurrentActiveChallange].iAwardType)			// Award type integer - 0 is xp, 1 is title, 2+ TBC
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].iAwardAmount         = ", g_sChallangeData[g_iCurrentActiveChallange].iAwardAmount)			// Award value - number of XP or title to be awarded etc
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].iStartTime           = ", g_sChallangeData[g_iCurrentActiveChallange].iStartTime)			// Int representing start date of challenge
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].iEndTime             = ", g_sChallangeData[g_iCurrentActiveChallange].iEndTime)				// Int representing end date of challenge
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].iChallangeType       = ", g_sChallangeData[g_iCurrentActiveChallange].iChallangeType)		// Challenge type (0 is intracrew, 1 is crew-vs-crew, 2 is SP mission challenge (TBC))
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].tl63GrpHandel        = ", g_sChallangeData[g_iCurrentActiveChallange].tl63GrpHandel)		// Grouphandle for associated leaderboard
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].tl23File             = ", g_sChallangeData[g_iCurrentActiveChallange].tl23File)				// Challenge document name
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].tl23ChalangeCreator  = ", g_sChallangeData[g_iCurrentActiveChallange].tl23ChalangeCreator)	// Challenge creator ownerid
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].tl23ChalangeDocName  = ", g_sChallangeData[g_iCurrentActiveChallange].tl23ChalangeDocName)	// Mission docname
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].tl23Owner            = ", g_sChallangeData[g_iCurrentActiveChallange].tl23Owner)			// Mission creator 
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].tl23MissionName      = ", g_sChallangeData[g_iCurrentActiveChallange].tl23MissionName)		// Mission pretty name for display
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].iMissionType         = ", g_sChallangeData[g_iCurrentActiveChallange].iMissionType)			// Mission type (e.g. 1 is DM, 2 is race, etc)
		PRINTLN("     g_sChallangeData[", g_iCurrentActiveChallange, "].vPos                 = ", g_sChallangeData[g_iCurrentActiveChallange].vPos)					// Mission X,Y,Z start coords																																							// List of crews particpating (only valid for crew-vs-crew)	
		RETURN TRUE
	ELSE																																									
		PRINTLN("CONTROL_UPDATING_CHALLENGES")
		PRINTLN("     g_iCurrentActiveChallange               = ", g_iCurrentActiveChallange)
		RETURN TRUE																																						
	ENDIF																																									
	RETURN FALSE																																							
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC BOOL KILL_SCRIPT_BECAUSE_ITS_NOT_SET_UP_FOR_THIS_NUMBER_OF_PLAYERS()

//	//if my participant ID is larger than the number of people that this script has been set up for then get kill
//	IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()	
//		IF (NETWORK_GET_MAX_NUM_PARTICIPANTS() - PARTICIPANT_ID_TO_INT()) > (g_FMMC_STRUCT.iNumParticipants + 2)
//			PRINTLN("")
//			PRINTLN("")
//			PRINTLN("")
//			PRINTLN("")
//			PRINTLN("")
//			PRINTLN("")
//			PRINTLN("")
//			PRINTLN("")
//			PRINTLN("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\")
//			PRINTLN("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\")
//			PRINTLN("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\")
//			PRINTLN("KILL_SCRIPT_BECAUSE_ITS_NOT_SET_UP_FOR_THIS_NUMBER_OF_PLAYERS = TRUE")
//			PRINTLN("NETWORK_GET_MAX_NUM_PARTICIPANTS() = ", NETWORK_GET_MAX_NUM_PARTICIPANTS())
//			PRINTLN("NETWORK_GET_NUM_PARTICIPANTS()     = ", NETWORK_GET_NUM_PARTICIPANTS())
//			PRINTLN("PARTICIPANT_ID_TO_INT()            = ", PARTICIPANT_ID_TO_INT())
//			PRINTLN("g_FMMC_STRUCT.iNumParticipants + 2 = ", (g_FMMC_STRUCT.iNumParticipants + 2))
//			PRINTLN("g_FMMC_STRUCT.iNumParticipants     = ", g_FMMC_STRUCT.iNumParticipants)
//			PRINTLN("")
//			PRINTLN("")
//			DO_SCREEN_FADE_IN(50)
//			PRINT_HELP("FMMC_KILL2MN")
//			RETURN TRUE
//		ENDIF	
//	ENDIF	
	RETURN FALSE
ENDFUNC
#ENDIF


//PURPOSE: Returns a gang ped model based on the passed in location. When set as true bUseRandomModelWhenNotInGangArea will return a random spare gang model, otherwise it will always return the professional model.
FUNC MODEL_NAMES GET_GANG_MODEL_FOR_AREA(VECTOR vLocation, BOOL bUseRandomModelWhenNotInGangArea = TRUE)
	
	//VAGOS
	IF IS_POINT_NEAR_POINT(vLocation, <<1301, -1696, 300>>, 350, FALSE)
		NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - G_M_Y_MEXGOON_01") NET_NL()
		RETURN G_M_Y_MEXGOON_01
	
	//LOST
	ELIF IS_POINT_NEAR_POINT(vLocation, <<1100, -196, 300>>, 350, FALSE)
	OR IS_POINT_NEAR_POINT(vLocation, <<77, 3657, 100>>, 150, FALSE)
		NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - G_M_Y_Lost_01") NET_NL()
		RETURN G_M_Y_Lost_01
	
	//BALLA
	ELIF IS_POINT_NEAR_POINT(vLocation, <<208, -1835, 200>>, 250, FALSE)
		NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - G_M_Y_BallaOrig_01") NET_NL()
		RETURN G_M_Y_BallaOrig_01
	
	//FAMILY
	ELIF IS_POINT_NEAR_POINT(vLocation, <<-26, -1450, 200>>, 250, FALSE)
		NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - G_M_Y_FAMCA_01") NET_NL()
		RETURN G_M_Y_FAMCA_01
	
	//KOREAN
	ELIF IS_POINT_NEAR_POINT(vLocation, <<-654, -922, 300>>, 350, FALSE)
		NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - G_M_Y_Korean_02") NET_NL()
		RETURN G_M_Y_Korean_02
	
	//HILLBILLY
	ELIF IS_POINT_NEAR_POINT(vLocation, <<1840, 3657, 450>>, 500, FALSE)
		NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - A_M_M_Hillbilly_02") NET_NL()
		RETURN A_M_M_Hillbilly_02
	
	//SALVA
	ELIF IS_POINT_NEAR_POINT(vLocation, <<550, -1885, 200>>, 250, FALSE)
		NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - G_M_Y_SalvaGoon_01") NET_NL()
		RETURN G_M_Y_SalvaGoon_01
	
	//NOT IN GANG AREA
	ELSE
		//RANDOM - PROFESSIONAL/ALBANIAN/TRIAD
		IF bUseRandomModelWhenNotInGangArea = TRUE
			INT iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
			
			//PROFESSIONAL
			IF iRand < 33
				NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - MP_G_M_Pros_01") NET_NL()
				RETURN MP_G_M_Pros_01
			
			//ARMENIAN
			ELIF iRand >= 66
				NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - G_M_M_ArmGoon_01") NET_NL()
				RETURN G_M_M_ArmGoon_01
			
			//TRIAD
			ELSE
				NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - G_M_M_ChiGoon_01") NET_NL()
				RETURN G_M_M_ChiGoon_01
			ENDIF
			
			//Random chance of Hillbilly in the country.
			IF vLocation.y > 800
				iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
				IF iRand < 33
					NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - A_M_M_Hillbilly_02 - COUNTRY") NET_NL()
					RETURN A_M_M_Hillbilly_02
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	NET_PRINT_TIME() NET_PRINT("    ---->  GET_GANG_MODEL_FOR_AREA - DIDN'T FIND MODEL - USING MP_G_M_Pros_01") NET_NL()
	RETURN MP_G_M_Pros_01
ENDFUNC

///PURPOSE: Determine if the local player has completed the freemode intro mission.
FUNC BOOL HAS_FM_INTRO_MISSION_BEEN_DONE(INT iSlot = -1)
//	RETURN FALSE
	#IF IS_DEBUG_BUILD
		IF g_bTutorialCompleteCheck
			NET_NL() NET_PRINT("[dsw] Checking HAS_FM_INTRO_MISSION_BEEN_DONE...") NET_NL()
			IF g_ShouldShiftingTutorialsBeSkipped
				NET_NL() NET_PRINT("[dsw] True because g_ShouldShiftingTutorialsBeSkipped") NET_NL()
			ENDIF
			
			IF g_SkipFmTutorials
				NET_NL() NET_PRINT("[dsw] True because g_SkipFmTutorials") NET_NL()
			ENDIF
			
			IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_MISS_DONE, iSlot)
				NET_NL() NET_PRINT("[dsw] True because MP_STAT_FM_INTRO_MISS_DONE") NET_NL()
			ENDIF
			
			NET_NL() NET_PRINT("[dsw] ...done HAS_FM_INTRO_MISSION_BEEN_DONE check") NET_NL()
		ENDIF
		
		IF g_ShouldShiftingTutorialsBeSkipped = TRUE
		//	NET_NL() NET_PRINT("HAS_FM_INTRO_MISSION_BEEN_DONE True because g_ShouldShiftingTutorialsBeSkipped") NET_NL()
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF g_SkipFmTutorials
//		NET_NL() NET_PRINT("[dsw] True because g_SkipFmTutorials") NET_NL()	
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD()
//		NET_NL() NET_PRINT("[dsw] True because HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD") NET_NL()	
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTERED_OFFLINE_SAVE_FM()
		RETURN TRUE
	ENDIF
	
//	IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_MISS_DONE, iSlot)
////		NET_NL() NET_PRINT("[dsw] True because GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_MISS_DONE, iSlot)") NET_NL()	
//	ENDIF	

	
	RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_MISS_DONE, iSlot)
ENDFUNC

FUNC BOOL DID_LOCAL_PLAYER_CHOOSE_TO_SKIP_TUTORIALS( INT iSlot = -1)

#IF FEATURE_GEN9_STANDALONE
	IF IS_TRANSITION_SESSION_SRLINK_REQUESTED_JOB_ACTIVE()
		RETURN TRUE
	ENDIF
#ENDIF

	RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_NOTUT_DONE, iSlot) //FALSE	

//	RETURN FALSE	
	
ENDFUNC

FUNC BOOL SHOULD_SKIP_HELP_TEXT_IF_NO_TUTORIAL()
	RETURN DID_LOCAL_PLAYER_CHOOSE_TO_SKIP_TUTORIALS()
ENDFUNC

/////PURPOSE: Determine if the local player has completed the freemode trigger tutorial.
//FUNC BOOL HAS_FM_TRIGGER_TUT_BEEN_DONE()
//	//RETURN FALSE
//	#IF IS_DEBUG_BUILD
//		IF g_bTutorialCompleteCheck
//			NET_NL() NET_PRINT("[dsw] Checking HAS_FM_TRIGGER_TUT_BEEN_DONE...") NET_NL()
//			IF g_ShouldShiftingTutorialsBeSkipped
//				NET_NL() NET_PRINT("[dsw] True because g_ShouldShiftingTutorialsBeSkipped") NET_NL() 
//			ENDIF
//			
//			IF g_SkipFmTutorials
//				NET_NL() NET_PRINT("[dsw] True because g_SkipFmTutorials") NET_NL()
//			ENDIF
//			
//			IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_TRIGTUT_DONE)
//				NET_NL() NET_PRINT("[dsw] True because MP_STAT_FM_TRIGTUT_DONE") NET_NL() 
//			ENDIF
//			
//			IF DID_LOCAL_PLAYER_CHOOSE_TO_SKIP_TUTORIALS()
//				NET_NL() NET_PRINT("[dsw] True because DID_LOCAL_PLAYER_CHOOSE_TO_SKIP_TUTORIALS") NET_NL() 
//			ENDIF
//			NET_NL() NET_PRINT("[dsw] ...done HAS_FM_TRIGGER_TUT_BEEN_DONE check") NET_NL()
//		ENDIF
//		
//		IF g_ShouldShiftingTutorialsBeSkipped = TRUE
//		//	NET_NL() NET_PRINT("HAS_FM_INTRO_MISSION_BEEN_DONE True because g_ShouldShiftingTutorialsBeSkipped") NET_NL()
//			RETURN TRUE
//		ENDIF
//	#ENDIF
//	
//	IF g_SkipFmTutorials
//		RETURN TRUE
//	ENDIF
//	
//	RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_TRIGTUT_DONE)
//ENDFUNC

//FUNC BOOL HAS_FM_RACE_TUT_BEEN_DONE()
//	//RETURN FALSE
//	#IF IS_DEBUG_BUILD
//		IF g_bTutorialCompleteCheck
//			NET_NL() NET_PRINT("[dsw] Checking HAS_FM_RACE_TUT_BEEN_DONE...") NET_NL()
//			IF g_ShouldShiftingTutorialsBeSkipped
//				NET_NL() NET_PRINT("[dsw] True because g_ShouldShiftingTutorialsBeSkipped") NET_NL() 
//			ENDIF
//			
//			IF g_SkipFmTutorials
//				NET_NL() NET_PRINT("[dsw] True because g_SkipFmTutorials") NET_NL()
//			ENDIF
//			
//			IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_RACETUT_DONE)
//				NET_NL() NET_PRINT("[dsw] True because MP_STAT_FM_RACETUT_DONE") NET_NL() 
//			ENDIF
//			
//			IF DID_LOCAL_PLAYER_CHOOSE_TO_SKIP_TUTORIALS()
//				NET_NL() NET_PRINT("[dsw] True because DID_LOCAL_PLAYER_CHOOSE_TO_SKIP_TUTORIALS") NET_NL() 
//			ENDIF
//			NET_NL() NET_PRINT("[dsw] ...done HAS_FM_TRIGGER_TUT_BEEN_DONE check") NET_NL()
//		ENDIF
//		
//		IF g_ShouldShiftingTutorialsBeSkipped = TRUE
//		//	NET_NL() NET_PRINT("HAS_FM_INTRO_MISSION_BEEN_DONE True because g_ShouldShiftingTutorialsBeSkipped") NET_NL()
//			RETURN TRUE
//		ENDIF
//	#ENDIF
//	
//	IF g_SkipFmTutorials
//		RETURN TRUE
//	ENDIF
//	
//	IF HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD()
//		RETURN TRUE
//	ENDIF
//	
//	RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_RACETUT_DONE)
//ENDFUNC

PROC SET_RACE_TUTORIAL_AS_COMPLETE(BOOL bComplete = TRUE)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL()
		NET_PRINT("[dsw] [SET_RACE_TUTORIAL_AS_COMPLETE] called with ") 
		IF bComplete 
			NET_PRINT("True") 
		ELSE 
			NET_PRINT("FALSE") 
		ENDIF
		NET_NL()
	#ENDIF
	
//	SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_RACETUT_DONE)
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_RACETUT_DONE, bComplete)
	
//	REQUEST_SAVE() //This is done in the common end mission functions. Brenda
ENDPROC

FUNC BOOL HAS_FM_DEATHMATCH_TUT_BEEN_DONE()
	//RETURN FALSE
	//INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE)
	
	
	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	#IF IS_DEBUG_BUILD
		IF g_bTutorialCompleteCheck
			NET_NL() NET_PRINT("[dsw] Checking HAS_FM_DEATHMATCH_TUT_BEEN_DONE...") NET_NL()
			IF g_ShouldShiftingTutorialsBeSkipped
				NET_NL() NET_PRINT("[dsw] True because g_ShouldShiftingTutorialsBeSkipped") NET_NL() 
			ENDIF
			
			IF g_SkipFmTutorials
				NET_NL() NET_PRINT("[dsw] True because g_SkipFmTutorials") NET_NL()
			ENDIF
			
			IF IS_BIT_SET(iStatInt, biFmCut_DeathmatchTut)
				NET_NL() NET_PRINT("[dsw] True because biFmCut_DeathmatchTut") NET_NL() 
			ENDIF
			
			IF DID_LOCAL_PLAYER_CHOOSE_TO_SKIP_TUTORIALS()
				NET_NL() NET_PRINT("[dsw] True because DID_LOCAL_PLAYER_CHOOSE_TO_SKIP_TUTORIALS") NET_NL() 
			ENDIF
			NET_NL() NET_PRINT("[dsw] ...done HAS_FM_TRIGGER_TUT_BEEN_DONE check") NET_NL()
		ENDIF
		
		IF g_ShouldShiftingTutorialsBeSkipped = TRUE
		//	NET_NL() NET_PRINT("HAS_FM_INTRO_MISSION_BEEN_DONE True because g_ShouldShiftingTutorialsBeSkipped") NET_NL()
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF g_SkipFmTutorials
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD()
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTERED_OFFLINE_SAVE_FM()
		RETURN TRUE
	ENDIF
	
	RETURN IS_BIT_SET(iStatInt, biFmCut_DeathmatchTut)
ENDFUNC

PROC SET_DEATHMATCH_TUTORIAL_AS_COMPLETE(BOOL bComplete = TRUE)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL()
		NET_PRINT("[dsw] [SET_DEATHMATCH_TUTORIAL_AS_COMPLETE] called with ") 
		IF bComplete 
			NET_PRINT("True") 
		ELSE 
			NET_PRINT("FALSE") 
		ENDIF
		NET_NL()
	#ENDIF
	//INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE)
	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	IF bComplete
		SET_BIT(iStatInt, biFmCut_DeathmatchTut)
//		SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_DEATHMATCH_TUTORIAL)
	ELSE
		CLEAR_BIT(iStatInt, biFmCut_DeathmatchTut)
//		CLEAR_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_DEATHMATCH_TUTORIAL)
	ENDIF
	SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
//	REQUEST_SAVE()
ENDPROC

FUNC BOOL NEED_TO_LAUNCH_MISSION_TUTORIAL()
	#IF IS_DEBUG_BUILD
		IF g_ShouldShiftingTutorialsBeSkipped = TRUE
		
			RETURN FALSE
		ENDIF
	#ENDIF
	
	IF g_SkipFmTutorials
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_FM_TRIGGER_TUT_BEEN_DONE()
		IF HAS_FM_RACE_TUT_BEEN_DONE()
			PRINTLN("[dsw] [NEED_TO_LAUNCH_MISSION_TUTORIAL] True...")
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC





FUNC BOOL HAS_FM_HOLD_UP_TUT_BEEN_DONE()
	//RETURN FALSE
	#IF IS_DEBUG_BUILD
		IF g_bTutorialCompleteCheck
			NET_NL() NET_PRINT("[dsw] Checking HAS_FM_HOLD_UP_TUT_BEEN_DONE...") NET_NL()
			IF g_ShouldShiftingTutorialsBeSkipped
				NET_NL() NET_PRINT("[dsw] True because g_ShouldShiftingTutorialsBeSkipped") NET_NL()
			ENDIF
			
			IF g_SkipFmTutorials
				NET_NL() NET_PRINT("[dsw] True because g_SkipFmTutorials") NET_NL() 
			ENDIF
			
			IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_HOLDTUT_DONE)
				NET_NL() NET_PRINT("[dsw] True because MP_STAT_FM_HOLDTUT_DONE") NET_NL()
			ENDIF
			
			NET_NL() NET_PRINT("[dsw] ...done HAS_FM_HOLD_UP_TUT_BEEN_DONE check") NET_NL()
		ENDIF
		
		IF g_ShouldShiftingTutorialsBeSkipped = TRUE
		//	NET_NL() NET_PRINT("HAS_FM_INTRO_MISSION_BEEN_DONE True because g_ShouldShiftingTutorialsBeSkipped") NET_NL()
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF g_SkipFmTutorials
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD()
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTERED_OFFLINE_SAVE_FM()
		RETURN TRUE
	ENDIF
	
	
	RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_HOLDTUT_DONE)
ENDFUNC

FUNC BOOL HAS_PURCHASE_FIRST_GARAGE_CUTSCENE_BEEN_DONE()
	
	IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_BUYGAR_DONE)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//FUNC BOOL HAS_INTRO_TO_GARAGES_CUTSCENE_BEEN_DONE()
//	//INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE)
//	#IF IS_DEBUG_BUILD
//		IF g_ShouldShiftingTutorialsBeSkipped
//			RETURN TRUE
//		ENDIF
//	#ENDIF
//	
//	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
//	IF IS_BIT_SET(iStatInt, biFmCut_IntroToGarages)
//		RETURN TRUE
//	ENDIF
//	
//	IF HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD()
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

FUNC BOOL HAS_INTRO_TO_APARTMENTS_CUTSCENE_BEEN_DONE()
	//INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE)
//	#IF IS_DEBUG_BUILD
//		IF g_ShouldShiftingTutorialsBeSkipped
//			RETURN TRUE
//		ENDIF
//	#ENDIF
	
	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	IF IS_BIT_SET(iStatInt, biFmCut_IntroToApartments)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_COMPLETED_LESTER_CUTSCENE(PLAYER_INDEX player)
	IF HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD()
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTERED_OFFLINE_SAVE_FM()
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_ShouldShiftingTutorialsBeSkipped
			RETURN TRUE
		ENDIF
	#ENDIF
	
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iFmTutProgBitset, biTrigTut_DoneLesterCutscene)
ENDFUNC


FUNC BOOL SHOULD_LOCAL_PLAYER_CHECK_FOR_HEIST_REMINDER()
	RETURN IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_CHECK_HST_REM)
ENDFUNC

PROC SET_LOCAL_PLAYER_SHOULD_CHECK_FOR_HEIST_REMINDER(BOOL bSet, BOOL bClearDoneHelp = FALSE)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_SHOULD_CHECK_FOR_HEIST_REMINDER] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_CHECK_HST_REM)
			SET_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_CHECK_HST_REM)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_CHECK_HST_REM)
			CLEAR_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_CHECK_HST_REM)
		ENDIF
	ENDIF
	
	IF bClearDoneHelp
		CLEAR_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_DONE_HST_REM)
	ENDIF
ENDPROC

FUNC BOOL HAS_HEIST_APARTMENT_REFURB_CUTSCENE_BEEN_DONE()

	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	IF IS_BIT_SET(iStatInt, biFmCut_AprtRefurbCutscene)
		IF NOT GET_MP_BOOL_PLAYER_STAT(MPPLY_JOBFLOW_HEIST_TEXT)
			#IF IS_DEBUG_BUILD
				NET_NL() NET_PRINT("[dsw] [HAS_HEIST_APARTMENT_REFURB_CUTSCENE_BEEN_DONE] Cutscene done, but MPPLY_JOBFLOW_HEIST_TEXT not set. Setting...")
			#ENDIF
			SET_MP_BOOL_PLAYER_STAT(MPPLY_JOBFLOW_HEIST_TEXT, TRUE)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



PROC SET_LOCAL_PLAYER_COMPLETED_HEIST_APT_REFURB_CUTSCENE(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_HEIST_APT_REFURB_CUTSCENE] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	IF bSet
		#IF IS_DEBUG_BUILD
			NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_HEIST_APT_REFURB_CUTSCENE] Set MPPLY_JOBFLOW_HEIST_TEXT")
		#ENDIF
		SET_MP_BOOL_PLAYER_STAT(MPPLY_JOBFLOW_HEIST_TEXT, TRUE) //-- For Neil F
		SET_BIT(iStatInt, biFmCut_AprtRefurbCutscene)
	ELSE
		CLEAR_BIT(iStatInt, biFmCut_AprtRefurbCutscene)
	ENDIF
	
	SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
//	REQUEST_SAVE(STAT_SAVETYPE_PROLOGUE)
ENDPROC

PROC SET_LOCAL_PLAYER_SHOULD_PLAY_HEIST_APT_REFURB_CUT(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_SHOULD_PLAY_HEIST_APT_REFURB_CUT] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_APT_REFURB)
	ELSE
		CLEAR_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_APT_REFURB)
	ENDIF
	
ENDPROC


FUNC BOOL IS_LOCAL_PLAYER_RUNNING_APARTMENT_REFURB_CUTSCENE()
	RETURN IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_APT_RUNNING)
ENDFUNC

PROC SET_LOCAL_PLAYER_RUNNING_APARTMENT_REFURB_CUTSCENE(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_RUNNING_APARTMENT_REFURB_CUTSCENE] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_APT_RUNNING)
			SET_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_APT_RUNNING)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_APT_RUNNING)
			CLEAR_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_APT_RUNNING)
		ENDIF
	ENDIF
ENDPROC
FUNC BOOL IS_LOCAL_PLAYER_RUNNING_INTRO_TO_PLANNING_BOARD_CUTSCENE()
	RETURN IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_RUNNING_PB_CUT)
ENDFUNC

PROC SET_LOCAL_PLAYER_RUNNING_INTRO_TO_PLANNING_BOARD_CUTSCENE(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_RUNNING_INTRO_TO_PLANNING_BOARD_CUTSCENE] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_RUNNING_PB_CUT)
			SET_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_RUNNING_PB_CUT)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_RUNNING_PB_CUT)
			CLEAR_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_RUNNING_PB_CUT)
		ENDIF
	ENDIF
ENDPROC
FUNC BOOL HAS_INTRO_TO_PLANNING_BOARD_CUTSCENE_BEEN_DONE()
//	RETURN FALSE
	
	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	IF IS_BIT_SET(iStatInt, biFmCut_IntroToPlanningBoard)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL NEED_TO_PLAY_INTRO_TO_PLANNING_BOARD_CUTSCENE()
//	RETURN TRUE
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	//-- From Keith's email, 17/06/2014
	INT numCompletedAsLeader = GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(g_sLocalMPHeistControl.lhcHashHeistRCID, TRUE)
	BOOL IsCompletedAsLeader = (numCompletedAsLeader > 0)
	IF g_sLocalMPHeistControl.lhcHashHeistRCID = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job
		IF NOT (IsCompletedAsLeader)
			// FIRST PLAY OF TUTORIAL HEIST AS LEADER, SO PLAY CUTSCENE
			
			IF NOT IS_LOCAL_PLAYER_RUNNING_APARTMENT_REFURB_CUTSCENE()
				IF NOT HAS_HEIST_APARTMENT_REFURB_CUTSCENE_BEEN_DONE()
					//-- Don't play the apartment refurbished custscene if we're about to watch the
					//-- intro-to-planning board cutscene
					
					SET_LOCAL_PLAYER_COMPLETED_HEIST_APT_REFURB_CUTSCENE(TRUE)
					SET_LOCAL_PLAYER_SHOULD_PLAY_HEIST_APT_REFURB_CUT(FALSE)
				ENDIF
				
				PRINTLN("[dsw] [NEED_TO_PLAY_INTRO_TO_PLANNING_BOARD_CUTSCENE] *** TRUE lhcHashHeistRCID =  ", g_sLocalMPHeistControl.lhcHashHeistRCID)
				PRINTLN("[dsw] [NEED_TO_PLAY_INTRO_TO_PLANNING_BOARD_CUTSCENE] *** TRUE IsCompletedAsLeader =  ", IsCompletedAsLeader)	

				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 60) = 0
			PRINTLN("[dsw] [NEED_TO_PLAY_INTRO_TO_PLANNING_BOARD_CUTSCENE] Not playing planning board cutscene... ")
			PRINTLN("[dsw] [NEED_TO_PLAY_INTRO_TO_PLANNING_BOARD_CUTSCENE] lhcHashHeistRCID =  ", g_sLocalMPHeistControl.lhcHashHeistRCID)
			PRINTLN("[dsw] [NEED_TO_PLAY_INTRO_TO_PLANNING_BOARD_CUTSCENE] IsCompletedAsLeader =  ", IsCompletedAsLeader)	
			PRINTLN("[dsw] [NEED_TO_PLAY_INTRO_TO_PLANNING_BOARD_CUTSCENE] IS_LOCAL_PLAYER_RUNNING_APARTMENT_REFURB_CUTSCENE =  ", IS_LOCAL_PLAYER_RUNNING_APARTMENT_REFURB_CUTSCENE())
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL NEED_TO_PLAY_HEIST_APT_REFURBISHED_CUTSCENE( INT iProperty )
	BOOL bNeedToPlay = IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_APT_REFURB)
	
	IF bNeedToPlay
		IF IS_PROPERTY_STILT_APARTMENT(iProperty)
			bNeedToPlay = FALSE
			PRINTLN("[dsw] [NEED_TO_PLAY_HEIST_APT_REFURBISHED_CUTSCENE] g_iFmNmhBitSet8 SET BUT IS_PROPERTY_STILT_APARTMENT")
		ELIF IS_PROPERTY_CUSTOM_APARTMENT(iProperty)
			bNeedToPlay = FALSE
			PRINTLN("[dsw] [NEED_TO_PLAY_HEIST_APT_REFURBISHED_CUTSCENE] g_iFmNmhBitSet8 SET BUT IS_PROPERTY_CUSTOM_APARTMENT")
		ENDIF
	ENDIF
	PRINTLN("[dsw] [NEED_TO_PLAY_HEIST_APT_REFURBISHED_CUTSCENE] RETURNING ", bNeedToPlay)
	
	RETURN(bNeedToPlay)
ENDFUNC



FUNC BOOL HAS_PLAYER_COMPLETED_RON_AND_TREVOR_CUTSCENE(PLAYER_INDEX player)
/*	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iFmTutProgBitset, biTrigTut_DoneTrevorCutscene)
	ENDIF
	RETURN TRUE
*/	
	IF HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD()
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTERED_OFFLINE_SAVE_FM()
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_ShouldShiftingTutorialsBeSkipped
			RETURN TRUE
		ENDIF
	#ENDIF
	
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iFmTutProgBitset, biTrigTut_DoneTrevorCutscene)
ENDFUNC

PROC SET_LOCAL_PLAYER_COMPLETED_LESTER_CUTSCENE(BOOL bSet)
	
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_DoneLesterCutscene)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_LESTER_CUTSCENE] called with... True ") 
			#ENDIF
			SET_PLAYER_HAS_UNLOCKED_CONTACT(PLAYER_ID(), CHAR_LESTER)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_DoneLesterCutscene)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_DoneLesterCutscene)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_LESTER_CUTSCENE] called with... False ") 
			#ENDIF
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_DoneLesterCutscene)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_COMPLETED_RON_AND_TREVOR_CUTSCENE(BOOL bSet)
	
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_DoneTrevorCutscene)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_RON_AND_TREVOR_CUTSCENE] called with... True ") 
			#ENDIF
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_DoneTrevorCutscene)
			SET_PLAYER_HAS_UNLOCKED_CONTACT(PLAYER_ID(), CHAR_TREVOR)
			SET_PLAYER_HAS_UNLOCKED_CONTACT(PLAYER_ID(), CHAR_RON)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_DoneTrevorCutscene)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_RON_AND_TREVOR_CUTSCENE] called with... False ") 
			#ENDIF
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_DoneTrevorCutscene)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_LESTER_CUTSCENE_BEEN_DONE()
	IF HAS_PLAYER_COMPLETED_LESTER_CUTSCENE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	//INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE)
	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	IF IS_BIT_SET(iStatInt, biFmCut_LesterCutscene)
		SET_LOCAL_PLAYER_COMPLETED_LESTER_CUTSCENE(TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_RON_AND_TREVOR_CUTSCENE_BEEN_DONE()
	IF HAS_PLAYER_COMPLETED_RON_AND_TREVOR_CUTSCENE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	//INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE)
	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	IF IS_BIT_SET(iStatInt, biFmCut_TrevorCut)
		SET_LOCAL_PLAYER_COMPLETED_RON_AND_TREVOR_CUTSCENE(TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_LOCAL_PLAYER_FINISHED_INITIAL_HEIST_PHONECALL(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_FINISHED_INITIAL_HEIST_PHONECALL] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LesterHeistCall)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LesterHeistCall)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LesterHeistCall)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LesterHeistCall)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Will return True if the player specified has received the phonecall
///    from Lester where he tells the player he'll send them details
///    of heists now that the players has an apartment
FUNC BOOL HAS_PLAYER_COMPLETED_INITIAL_APARTMENT_HEIST_PHONECALL(PLAYER_INDEX player)
	INT index = NATIVE_TO_INT(player)
	IF index > -1
		RETURN IS_BIT_SET(GlobalplayerBD_FM[index].iFmTutProgBitset, biTrigTut_LesterHeistCall)
	ENDIF
	RETURN FALSE
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The HAS_PLAYER_COMPLETED_INITIAL_APARTMENT_HEIST_PHONECALL() function takes time to be filled with real data, so going straight to the STAT value
//
// RETURN VALUE:		BOOL			TRUE if the Heist Intro Cut Stat value is set, otherwise FALSE
FUNC BOOL Has_Intro_To_Heists_Been_Done()

	BOOL statValue = FALSE
	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	statValue = IS_BIT_SET(iStatInt, biFmCut_HeistInt)
	
	IF (statValue != HAS_PLAYER_COMPLETED_INITIAL_APARTMENT_HEIST_PHONECALL(PLAYER_ID()))
		PRINTLN("...KGM MP [ActSelect][Heist]: HAS_PLAYER_COMPLETED_INITIAL_APARTMENT_HEIST_PHONECALL() disagrees with g_MP_STAT_FM_CUT_DONE STAT, so returning the STAT value")
	ENDIF
	
	RETURN statValue

ENDFUNC

FUNC BOOL HAS_LESTER_INTRO_TO_HEISTS_CUTSCENE_BEEN_DONE()
	IF HAS_PLAYER_COMPLETED_INITIAL_APARTMENT_HEIST_PHONECALL(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	IF IS_BIT_SET(iStatInt, biFmCut_HeistInt)
		SET_LOCAL_PLAYER_FINISHED_INITIAL_HEIST_PHONECALL(TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_LOCAL_PLAYER_FINISHED_LOWRIDER_INTRO_CUTSCENE_BEEN_DONE(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		IF bSet
			PRINTLN("[LOWFLOW] [SET_LOCAL_PLAYER_FINISHED_LOWRIDER_INTRO_CUTSCENE_BEEN_DONE] called with... TRUE")
		ELSE
			PRINTLN("[LOWFLOW] [SET_LOCAL_PLAYER_FINISHED_LOWRIDER_INTRO_CUTSCENE_BEEN_DONE] called with... FALSE")
		ENDIF
	#ENDIF
	
	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_COMPLEATED_LOW_RIDER_CUT)
			SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_COMPLEATED_LOW_RIDER_CUT)
		ENDIF
		SET_BIT(iStatInt, biFmCut_LowriderCutscene)
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_COMPLEATED_LOW_RIDER_CUT)
			CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_COMPLEATED_LOW_RIDER_CUT)
		ENDIF
		CLEAR_BIT(iStatInt, biFmCut_LowriderCutscene)
	ENDIF
	SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
ENDPROC

FUNC BOOL HAS_PLAYER_COMPLETED_LOWRIDER_INTRO_CUTSCENE_BEEN_DONE(PLAYER_INDEX player)
	INT index = NATIVE_TO_INT(player)
	IF index > -1
		RETURN IS_BIT_SET(GlobalplayerBD_FM_2[index].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_COMPLEATED_LOW_RIDER_CUT)
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL HAS_LOWRIDER_INTRO_CUTSCENE_BEEN_DONE()

	IF HAS_PLAYER_COMPLETED_LOWRIDER_INTRO_CUTSCENE_BEEN_DONE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
	IF IS_BIT_SET(iStatInt, biFmCut_LowriderCutscene)
		SET_LOCAL_PLAYER_FINISHED_LOWRIDER_INTRO_CUTSCENE_BEEN_DONE(TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Determine if the help text about being the host of a job for the first time should display
/// RETURNS:
///    
FUNC BOOL DONE_FIRST_TIME_JOB_HOST_HELP()
	INT iStatInt 
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_DONE_JOB_HOST_HELP)	
		iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3)
		IF NOT IS_BIT_SET(iStatInt, biNmh3_FirstJobHostHelp)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC SET_DONE_FIRST_TIME_JOB_HOST_HELP(BOOL bDone = TRUE)
	INT iStatInt 
	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3)
	IF bDone
		SET_BIT(iStatInt, biNmh3_FirstJobHostHelp)
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_DONE_JOB_HOST_HELP)	
	ELSE
		CLEAR_BIT(iStatInt, biNmh3_FirstJobHostHelp)
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_DONE_JOB_HOST_HELP)	
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT3, iStatInt)
ENDPROC

/// PURPOSE:
///    Determine if the help text about being the host of a minigame has been displayed
/// RETURNS:
FUNC BOOL DONE_MINIGAME_HOST_HELP_TEXT()
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6)
	
	IF NOT IS_BIT_SET(iStatInt, biNmh6_HostMinigame1)
	OR NOT IS_BIT_SET(iStatInt, biNmh6_HostMinigame2)
	OR NOT IS_BIT_SET(iStatInt, biNmh6_HostMinigame3)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_DONE_MINIGAME_HOST_HELP_TEXT(BOOL bDone = TRUE)
	INT iStatInt 
	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6)
	IF bDone
		IF NOT IS_BIT_SET(iStatInt, biNmh6_HostMinigame1)
			SET_BIT(iStatInt, biNmh6_HostMinigame1)
		ELIF NOT IS_BIT_SET(iStatInt, biNmh6_HostMinigame2)
			SET_BIT(iStatInt, biNmh6_HostMinigame2)
		ELIF NOT IS_BIT_SET(iStatInt, biNmh6_HostMinigame3)
			SET_BIT(iStatInt, biNmh6_HostMinigame3)
		ENDIF
	ELSE
		IF IS_BIT_SET(iStatInt, biNmh6_HostMinigame3)
			SET_BIT(iStatInt, biNmh6_HostMinigame3)
		ELIF IS_BIT_SET(iStatInt, biNmh6_HostMinigame2)
			SET_BIT(iStatInt, biNmh6_HostMinigame2)
		ELIF IS_BIT_SET(iStatInt, biNmh6_HostMinigame1)
			SET_BIT(iStatInt, biNmh6_HostMinigame1)
		ENDIF
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6, iStatInt)
ENDPROC

/// PURPOSE:
///    Determine if the help text about masks being placed in inventory has been displayed
/// RETURNS:
FUNC BOOL DONE_MASK_SELECTION_HELP_TEXT()
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6)
	
	IF NOT IS_BIT_SET(iStatInt, biNmh6_MaskSelection1)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_DONE_MASK_SELECTION_HELP_TEXT(BOOL bDone = TRUE)
	INT iStatInt 
	iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6)
	IF bDone
		SET_BIT(iStatInt, biNmh6_MaskSelection1)
	ELSE
		CLEAR_BIT(iStatInt, biNmh6_MaskSelection1)
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6, iStatInt)
ENDPROC

PROC GET_SCREEN_POS_FOR_HOST_HELP(FLOAT &fXpos, FLOAT &fYpos)
	//fXpos = 0.275
	//fYpos = 0.655
	fXpos = 0.170
	fYpos = 0.82
ENDPROC
PROC DISPLAY_FIRST_TIME_JOB_HOST_HELP()
	//FLOAT fXpos, fYpos
//	GET_SCREEN_POS_FOR_HOST_HELP(fXpos, fYpos)

//	HELP_AT_SCREEN_LOCATION("FM_JHOST_INV", fXpos, fYpos, INT_TO_ENUM(eARROW_DIRECTION,0), -1, FLOATING_HELP_MISSION_SLOT)
	PRINT_HELP("FM_JHOST_INV")
ENDPROC

FUNC BOOL IS_FIRST_TIME_JOB_HOST_HELP_BEING_DISPLAYED()
//	RETURN IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_JHOST_INV")
	RETURN IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_JHOST_INV")
ENDFUNC

PROC CLEAR_FIRST_TIME_JOB_HOST_HELP()
	IF IS_FIRST_TIME_JOB_HOST_HELP_BEING_DISPLAYED()
		//CLEAR_THIS_FLOATING_HELP("FM_JHOST_INV")
		CLEAR_HELP()
	ENDIF
ENDPROC



PROC DISPLAY_SELECT_VEHICLE_FLOATING_HELP()
	//HELP_AT_SCREEN_LOCATION("FM_TRIG_FL4", 0.275, 0.525,  HELP_TEXT_NORTH, -1, FLOATING_HELP_MISSION_SLOT)
	PRINT_HELP("FM_TRIG_FL4")
ENDPROC

FUNC BOOL IS_SELECT_VEHICLE_FLOATING_HELP_BEING_DISPLAYED()
	//RETURN IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_TRIG_FL4")
	RETURN IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_TRIG_FL4")
ENDFUNC

PROC CLEAR_SELECT_VEHICLE_FLOATING_HELP()
	IF IS_SELECT_VEHICLE_FLOATING_HELP_BEING_DISPLAYED()
	//	CLEAR_THIS_FLOATING_HELP("FM_TRIG_FL4")
		CLEAR_HELP()
	ENDIF
ENDPROC	

PROC DISPLAY_BETTING_SCREEN_FLOATING_HELP(STRING strHelpText)
	//HELP_AT_SCREEN_LOCATION(strHelpText, 0.275, 0.525, HELP_TEXT_NORTH, -1, FLOATING_HELP_MISSION_SLOT) 
	PRINT_HELP(strHelpText)
ENDPROC

FUNC BOOL IS_BETTING_SCREEN_FLOATING_HELP_BEING_DISPLAYED()
//	RETURN IS_THIS_FLOATING_HELP_BEING_DISPLAYED("FM_IHELP_BET")
	RETURN IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_BET")
ENDFUNC

PROC CLEAR_BETTING_SCREEN_FLOATING_HELP()
//	IF IS_ANY_FLOATING_HELP_BEING_DISPLAYED()
//		CLEAR_ALL_FLOATING_HELP(TRUE)
//	ENDIF
	IF IS_BETTING_SCREEN_FLOATING_HELP_BEING_DISPLAYED()
		CLEAR_HELP()
	ENDIF
ENDPROC	




//PURPOSE: Let the freemode script know it should print the help regarding receiveing cash even though the heist was failed
PROC SET_LOCAL_PLAYER_SHOULD_DO_HEIST_FAIL_CASH_HELP_TEXT()
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_HST_FAIL_HLP)
		PRINTLN("[dsw] SET_LOCAL_PLAYER_SHOULD_DO_HEIST_FAIL_CASH_HELP_TEXT() Called")
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_HST_FAIL_HLP)
	ENDIF
ENDPROC
 // FEATURE_HEIST_PLANNING



FUNC BOOL IS_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerReadyForTrigTut)

ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_RACE_FROM_SKYSWOOP()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchRaceFromTransition)

ENDFUNC
FUNC BOOL SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchMissionFromTransition)

ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_RUNNING_FM_INTRO_CUT()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_RunningIntroCut)

ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_RUNNING_DM_TUTORIAL() 
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_RunningDMTut)
ENDFUNC


FUNC BOOL IS_LOCAL_PLAYER_READY_FOR_DM_TUTORIAL_CUT() 
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ReadyForTutDmCut)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_READY_FOR_TUTORIAL_DEATHMATCH() 
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ReadyForTutDeathmatch)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_TAXI_FOR_INTRO()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerInTaxiForIntro)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_PROCESSING_TUTORIAL_CORONA()
	RETURN IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTutProcessingRaceCorona)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_PROCESSING_TUTORIAL_DM_CORONA()
	RETURN IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTutProcessingDMCorona)
ENDFUNC


FUNC BOOL IS_LOCAL_PLAYER_READY_TO_PRELOAD_LAMAR_ASSETS()
	RETURN IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_ReadyToPreloadLamar)
ENDFUNC


//FUNC BOOL HAS_LOCAL_PLAYER_ACCEPTED_TUTORIAL_INVITE()
//	RETURN IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_AcceptedInvite)
//ENDFUNC

FUNC BOOL HAS_TUTORIAL_CORONA_INTRO_TIMER_EXPIRED()
	RETURN IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_CoronaIntroTimeExpired)
ENDFUNC

FUNC BOOL HAS_PLAYER_LOADED_CLOUD_DATA_FOR_TUTORIAL_MISSION(PLAYER_INDEX player)
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iFmTutProgBitset, biTrigTut_LoadedCloudData))
ENDFUNC


FUNC BOOL HAS_PLAYER_COMPLETED_POST_RACE_CUTSCENE(PLAYER_INDEX player)
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iFmTutProgBitset, biTrigTut_CompletedPostRaceCut))
ENDFUNC

FUNC BOOL HAS_PLAYER_STARTED_WAIT_FOR_MISSION_PLAYERS_TIMEOUT(PLAYER_INDEX player)
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iFmTutProgBitset, biTrigTut_MissLaunchTimeStarted))
ENDFUNC
FUNC BOOL IS_LOCAL_PLAYER_READY_TO_DO_HOLD_UP_TUT()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReadyToDoHoldUp)
ENDFUNC

FUNC BOOL IS_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL(PLAYER_INDEX player)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset, biHT_AboutToDoHoldUpTut)
ENDFUNC

///	PURPOSE:
///    Needed to determine if the hold up tutorial should launch via a phonecall, or via a text message
PROC SET_LOCAL_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_AboutToDoHoldUpTut)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_AboutToDoHoldUpTut)
	ENDIF

ENDPROC	

FUNC BOOL IS_PLAYER_DOING_MP_MOD_SHOP_TUTORIAL(PLAYER_INDEX player)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset, biCMT_InModShopForTutorial))
ENDFUNC
PROC SET_LOCAL_PLAYER_DOING_MOD_SHOP_TUTORIAL(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_DOING_MOD_SHOP_TUTORIAL] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biCMT_InModShopForTutorial)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biCMT_InModShopForTutorial)
	ENDIF
ENDPROC	

FUNC BOOL HAS_PLAYER_COMPLETED_MOD_SHOP_TUTORIAL_HELP(PLAYER_INDEX player)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset, biCMT_CompletedModShopTutorialHelp))
ENDFUNC
PROC SET_LOCAL_PLAYER_COMPLETED_MP_MOD_SHOP_TUTORIAL_HELP(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_MP_MOD_SHOP_TUTORIAL_HELP] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biCMT_CompletedModShopTutorialHelp)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biCMT_CompletedModShopTutorialHelp)
	ENDIF
ENDPROC	

FUNC BOOL HAS_PLAYER_PURCHASED_ALL_MOD_SHOP_TUTORIAL_ITEMS(PLAYER_INDEX player)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset, biCMT_PurchasedAllTutorialItems))
ENDFUNC

PROC SET_LOCAL_PLAYER_PURCHASED_ALL_MP_MOD_SHOP_TUTORIAL_ITEMS(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_PURCHASED_ALL_MP_MOD_SHOP_TUTORIAL_ITEMS] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biCMT_PurchasedAllTutorialItems)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biCMT_PurchasedAllTutorialItems)
	ENDIF
ENDPROC


FUNC BOOL SHOULD_LOCAL_PLAYER_FADE_IN_FROM_PASSIVE_MODE_CUTSCENE()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_PASSIVE_FADE_IN)
ENDFUNC
PROC SET_LOCAL_PLAYER_SHOULD_FADE_IN_FROM_PASSIVE_MODE_CUTSCENE(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_SHOULD_FADE_IN_FROM_PASSIVE_MODE_CUTSCENE] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_PASSIVE_FADE_IN)
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_PASSIVE_FADE_IN)
	ENDIF

ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_JUST_DONE_PASSIVE_MODE_CUTSCENE()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_JUST_DONE_PASSIVE)
ENDFUNC
PROC SET_LOCAL_PLAYER_JUST_DONE_PASSIVE_MODE_CUTSCENE(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_JUST_DONE_PASSIVE_MODE_CUTSCENE] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_JUST_DONE_PASSIVE)
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_JUST_DONE_PASSIVE)
	ENDIF

ENDPROC
FUNC BOOL IS_LOCAL_PLAYER_RUNNING_PASSIVE_MODE_CUTSCENE()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_RUN_PASSIVE_CUT)
ENDFUNC

FUNC BOOL IS_PLAYER_RUNNING_PASSIVE_MODE_CUTSCENE(PLAYER_INDEX player)
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset, biPM_RunningPassiveModeCut))
ENDFUNC
PROC SET_LOCAL_PLAYER_RUNNING_PASSIVE_MODE_CUTSCENE(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_RUNNING_PASSIVE_MODE_CUTSCENE] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_RUN_PASSIVE_CUT)
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biPM_RunningPassiveModeCut)
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_RUN_PASSIVE_CUT)
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biPM_RunningPassiveModeCut)
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_IN_VALID_CAR_FOR_MP_MODSHOP_TUTORIAL()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_VALID_MODSHOP_CAR)
ENDFUNC
PROC SET_LOCAL_PLAYER_IN_VALID_CAR_FOR_MP_MODSHOP_TUTORIAL(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_IN_VALID_CAR_FOR_MP_MODSHOP_TUTORIAL] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_VALID_MODSHOP_CAR)
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_VALID_MODSHOP_CAR)
	ENDIF
ENDPROC

FUNC BOOL IS_CORONA_WALK_IN_DISABLED_FOR_TUTORIAL()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_DISABLE_WALK_IN)
ENDFUNC
PROC SET_CORONA_WALK_IN_DISABLED_FOR_TUTORIAL(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_CORONA_WALK_IN_DISABLED_FOR_TUTORIAL] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_DISABLE_WALK_IN)
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_DISABLE_WALK_IN)
	ENDIF
ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_STARTED_TUTORIAL_RACE()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_STARTED_TUT_RACE)
ENDFUNC

PROC SET_LOCAL_PLAYER_STARTED_TUTORIAL_RACE(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_STARTED_TUTORIAL_RACE] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_STARTED_TUT_RACE)
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_STARTED_TUT_RACE)
	ENDIF
ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_DONE_RACE_TUTORIAL_CORONA_SWOOP()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_DONE_RACE_SWOOP)
ENDFUNC

PROC SET_LOCAL_PLAYER_DONE_RACE_TUTORIAL_CORONA_SWOOP(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_DONE_RACE_TUTORIAL_CORONA_SWOOP] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_DONE_RACE_SWOOP)
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_DONE_RACE_SWOOP)
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_EXITIED_MOD_SHOP_FOR_TUTORIAL(PLAYER_INDEX player)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset, biCMT_PlayerExitedModShopAfterTutorial))
ENDFUNC

PROC SET_LOCAL_PLAYER_EXITED_MP_MOD_SHOP_FOR_TUTORIAL(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_EXITED_MP_MOD_SHOP_FOR_TUTORIAL] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biCMT_PlayerExitedModShopAfterTutorial)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biCMT_PlayerExitedModShopAfterTutorial)
	ENDIF
ENDPROC
PROC SET_LOCAL_PLAYER_IS_RUNNING_INTRO_MISSION(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_IS_RUNNING_INTRO_MISSION] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerOnIntroMission)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerOnIntroMission)
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_IS_IN_ACTIVITY_TUTORIAL_CORONA(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_IS_IN_ACTIVITY_TUTORIAL_CORONA] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerInFmActivityTutorialCorona)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerInFmActivityTutorialCorona)
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_LOADED_CLOUD_DATA_FOR_TUTORIAL_MISSION(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_LOADED_CLOUD_DATA_FOR_TUTORIAL_MISSION] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LoadedCloudData)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LoadedCloudData)
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_STARTED_WAIT_FOR_TUTORIAL_PLAYERS_TIMEOUT(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_STARTED_WAIT_FOR_TUTORIAL_PLAYERS_TIMEOUT] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_MissLaunchTimeStarted)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_MissLaunchTimeStarted)
	ENDIF
ENDPROC



PROC SET_LOCAL_PLAYER_IS_IN_DM_TUTORIAL_CORONA(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_IS_IN_DM_TUTORIAL_CORONA] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerInDmCorona)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerInDmCorona)
	ENDIF
ENDPROC

/*
PROC SET_LOCAL_PLAYER_IS_RUNNING_ACTIVITY_TUTORIAL_CUT(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_IS_RUNNING_ACTIVITY_TUTORIAL_CUT] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerRunningActivityTutorialCut)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerRunningActivityTutorialCut)
	ENDIF
ENDPROC
*/
PROC SET_LOCAL_PLAYER_IS_RUNNING_ACTIVITY_TUTORIAL(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_IS_RUNNING_ACTIVITY_TUTORIAL] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerRunningActivityTutorial)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerRunningActivityTutorial)
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_HAS_COMPLETED_POST_RACE_CUTSCENE(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_HAS_COMPLETED_POST_RACE_CUTSCENE] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
		NET_PRINT(" [TIMESTAMP] ")NET_PRINT_INT(GET_CLOUD_TIME_AS_INT())
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_CompletedPostRaceCut)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_CompletedPostRaceCut)
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_READY_FOR_ACTIVITY_TUTORIAL] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerReadyForTrigTut)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerReadyForTrigTut)
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_INDEX player)
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset , biTrigtTut_StartedFinalIntroScene))
ENDFUNC

PROC SET_LOCAL_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigtTut_StartedFinalIntroScene)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigtTut_StartedFinalIntroScene)
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_READY_FOR_TUTORIAL_MOCAP(PLAYER_INDEX player)
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset , biTrigTut_ReadyForTutorialMocap))
ENDFUNC

PROC SET_LOCAL_PLAYER_READY_FOR_TUTORIAL_MOCAP(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_READY_FOR_TUTORIAL_MOCAP] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_ReadyForTutorialMocap)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_ReadyForTutorialMocap)
	ENDIF
ENDPROC 

FUNC BOOL IS_LOCAL_PLAYER_READY_FOR_CORONA_HELP()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_TUT_CORONA_HELP)
ENDFUNC

PROC SET_LOCAL_PLAYER_READY_FOR_CORONA_HELP(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_READY_FOR_CORONA_HELP] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_TUT_CORONA_HELP)
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_TUT_CORONA_HELP)
	ENDIF
ENDPROC
FUNC BOOL HAS_PLAYER_STARTED_TUTORIAL_CORONA_PLAYBACK(PLAYER_INDEX player)
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset , biTrigTut_TutorialCarRecPlayback))
ENDFUNC

PROC SET_LOCAL_PLAYER_STARTED_TUTORIAL_CORONA_PLAYBACK(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_STARTED_TUTORIAL_CORONA_PLAYBACKP] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_TutorialCarRecPlayback)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_TutorialCarRecPlayback)
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_FINISHED_TUTORIAL_CORONA_PLAYBACK(PLAYER_INDEX player)
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset , biTrigTut_FinishedTutorialCarRecPlayback))
ENDFUNC

PROC SET_LOCAL_PLAYER_FINISHED_TUTORIAL_CORONA_PLAYBACK(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_STARTED_TUTORIAL_CORONA_PLAYBACKP] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_FinishedTutorialCarRecPlayback)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_FinishedTutorialCarRecPlayback)
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_READY_FOR_TUTORIAL_CORONA_SWOOP()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_RACE_TUT_SWOOP)
ENDFUNC
PROC SET_LOCAL_PLAYER_READY_TUTORIAL_CORONA_SWOOP(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_READY_TUTORIAL_CORONA_SWOOP] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet 
		SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_RACE_TUT_SWOOP)
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_RACE_TUT_SWOOP)
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_ACTIVATED_TUTORIAL_CORONA_CAMERA(PLAYER_INDEX player)
	RETURN(IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset , biTrigTut_StartedCoronaCamera))
ENDFUNC

PROC SET_LOCAL_PLAYER_ACTIVATED_TUTORIAL_CORONA_CAMERA(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_ACTIVATED_TUTORIAL_CORONA_CAMERA] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_StartedCoronaCamera)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biTrigTut_StartedCoronaCamera)
	ENDIF
ENDPROC


PROC SET_LOCAL_PLAYER_COMPLETED_ACTIVITY_TUTORIAL(BOOL bSet)
	
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PLayerCompletedTriggerTut)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_ACTIVITY_TUTORIAL] called with... True ") 
			#ENDIF
			SET_PLAYER_HAS_UNLOCKED_CONTACT(PLAYER_ID(), CHAR_LAMAR)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PLayerCompletedTriggerTut)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PLayerCompletedTriggerTut)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_ACTIVITY_TUTORIAL] called with... False ") 
			#ENDIF
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PLayerCompletedTriggerTut)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_CompletedInitalAmbientTut)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS] called with... True ") 
			#ENDIF
			SET_PLAYER_HAS_UNLOCKED_CONTACT(PLAYER_ID(), CHAR_LAMAR)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_CompletedInitalAmbientTut)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_CompletedInitalAmbientTut)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS] called with... False ") 
			#ENDIF
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_CompletedInitalAmbientTut)
		ENDIF
	ENDIF
ENDPROC




FUNC BOOL HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(PLAYER_INDEX player)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iFmTutProgBitset, biTrigTut_PLayerCompletedTriggerTut)
ENDFUNC

FUNC BOOL HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_INDEX player)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iFmTutProgBitset, biTrigTut_CompletedInitalAmbientTut)
ENDFUNC

PROC SET_TUTORIAL_CORONA_INTRO_TIMER_EXPIRED(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_CoronaIntroTimeExpired)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_TUTORIAL_CORONA_INTRO_TIMER_EXPIRED] called with... True ") 
			#ENDIF
			SET_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_CoronaIntroTimeExpired)
		ENDIF
	ELSE
		IF IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_CoronaIntroTimeExpired)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_TUTORIAL_CORONA_INTRO_TIMER_EXPIRED] called with... False ") 
			#ENDIF
			CLEAR_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_CoronaIntroTimeExpired)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_CREATED_LAMAR_POST_RACE_CUT_ASSETS(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_PlayerLoadedCutAssets)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_CREATED_LAMAR_POST_RACE_CUT_ASSETS] called with... True ") 
			#ENDIF
			SET_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_PlayerLoadedCutAssets)
		ENDIF
	ELSE
		IF IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_PlayerLoadedCutAssets)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_LOADED_LAMAR_POST_RACE_CUT_ASSETS] called with... False ") 
			#ENDIF
			CLEAR_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_PlayerLoadedCutAssets)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_CREATED_LAMAR_CUT_ASSETS()
	RETURN IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_PlayerLoadedCutAssets)

ENDFUNC

PROC SET_LOCAL_PLAYER_SHOULD_LAUNCH_TUTORIAL_RACE_FROM_SKYSWOOP(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_SHOULD_LAUNCH_TUTORIAL_RACE_FROM_SKYSWOOP] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchRaceFromTransition)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchRaceFromTransition)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchRaceFromTransition)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchRaceFromTransition)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_SHOULD_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_SHOULD_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchMissionFromTransition)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchMissionFromTransition)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchMissionFromTransition)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_LaunchMissionFromTransition)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_RUNNING_FM_INTRO_CUT(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_RUNNING_FM_INTRO_CUT] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_RunningIntroCut)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_RunningIntroCut)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_RunningIntroCut)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_RunningIntroCut)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_DOING_ACTIVITY_TUTORIAL_RACE(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_DOING_ACTIVITY_TUTORIAL_RACE] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_OnTutRace)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_OnTutRace)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_OnTutRace)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_OnTutRace)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_RUNNING_DM_TUTORIAL(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_RUNNING_DM_TUTORIAL] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_RunningDMTut)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_RunningDMTut)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_RunningDMTut)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_RunningDMTut)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_READY_FOR_DM_TUTORIAL_CUT(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_READY_FOR_DM_TUTORIAL_CUT] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ReadyForTutDmCut)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ReadyForTutDmCut)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ReadyForTutDmCut)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ReadyForTutDmCut)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_READY_FOR_TUTORIAL_DEATHMATCH(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_READY_FOR_TUTORIAL_DEATHMATCH] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ReadyForTutDeathmatch)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ReadyForTutDeathmatch)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ReadyForTutDeathmatch)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ReadyForTutDeathmatch)
		ENDIF
	ENDIF
ENDPROC


PROC SET_LOCAL_PLAYER_IN_TAXI_FOR_INTRO(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_IN_TAXI_FOR_INTRO] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerInTaxiForIntro)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerInTaxiForIntro)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerInTaxiForIntro)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerInTaxiForIntro)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_SHOULD_SPANW_AT_LAMARS_HOUSE(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_SHOULD_SPANW_AT_LAMARS_HOUSE] using bShouldSpawnAtLamars called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		g_sTransitionSessionData.bShouldSpawnAtLamars = TRUE
//		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ShouldSpawnAtLamarHouseAfterMission)
//			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ShouldSpawnAtLamarHouseAfterMission)
//		ENDIF
	ELSE
		g_sTransitionSessionData.bShouldSpawnAtLamars = FALSE
//		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ShouldSpawnAtLamarHouseAfterMission)
//			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ShouldSpawnAtLamarHouseAfterMission)
//		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE()
//	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_ShouldSpawnAtLamarHouseAfterMission)
	RETURN g_sTransitionSessionData.bShouldSpawnAtLamars
ENDFUNC

/// PURPOSE:
///    Use to this to check for corona loading in. FMMC_Launcher will set this when the tutorial race corona has been processed
/// PARAMS:
///    bSet - 
PROC SET_PROCESSING_TUTORAL_RACE_CORONA(BOOL bSet)
	
	
	IF bSet
		IF NOT IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTutProcessingRaceCorona)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_PROCESSING_TUTORAL_RACE_CORONA] called with... True ") 
			#ENDIF
			SET_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTutProcessingRaceCorona)
		ENDIF
	ELSE
		IF IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTutProcessingRaceCorona)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_PROCESSING_TUTORAL_RACE_CORONA] called with... False ") 
			#ENDIF
			CLEAR_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTutProcessingRaceCorona)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Use to this to check for corona loading in. FMMC_Launcher will set this when the tutorial DM corona has been processed
/// PARAMS:
///    bSet -
PROC SET_PROCESSING_TUTORAL_DM_CORONA(BOOL bSet)
	
	IF bSet
		IF NOT IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTutProcessingDMCorona)
			SET_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTutProcessingDMCorona)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_PROCESSING_TUTORAL_DM_CORONA] called with... True ") 
			#ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTutProcessingDMCorona)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_PROCESSING_TUTORAL_DM_CORONA] called with... False ") 
			#ENDIF
			CLEAR_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTutProcessingDMCorona)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_IS_IN_TUTORIAL_SESSION_FOR_INTRO(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTutInTutorialSession)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTutInTutorialSession)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_IS_IN_TUTORIAL_SESSION_FOR_INTRO] called with... True ") 
			#ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTutInTutorialSession)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_IS_IN_TUTORIAL_SESSION_FOR_INTRO] called with... False ") 
			#ENDIF
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTutInTutorialSession)
		ENDIF
	ENDIF
ENDPROC


PROC SET_LOCAL_PLAYER_READY_TO_PRELOAD_LAMAR_ASSETS(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_ReadyToPreloadLamar)
			SET_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_ReadyToPreloadLamar)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_READY_TO_PRELOAD_LAMAR_ASSETS] called with... True ") 
			#ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_ReadyToPreloadLamar)
			#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_READY_TO_PRELOAD_LAMAR_ASSETS] called with... False ") 
			#ENDIF
			CLEAR_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_ReadyToPreloadLamar)
		ENDIF
	ENDIF
ENDPROC

/*
PROC SET_LOCAL_PLAYER_WATCHING_TUTORIAL_DM_CUT(BOOL bSet)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_WATCHING_TUTORIAL_DM_CUT] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_WatchingDmCUt)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_WatchingDmCUt)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_WatchingDmCUt)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_WatchingDmCUt)
		ENDIF
	ENDIF
ENDPROC
*/
//PROC SET_LOCAL_PLAYER_ACCEPTED_TUTORIAL_INVITE(BOOL bSet)
//	
//	#IF IS_DEBUG_BUILD
//		DEBUG_PRINTCALLSTACK()
//		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_ACCEPTED_TUTORIAL_INVITE] called with... ")
//		IF bSet
//			NET_PRINT(" TRUE") NET_NL()
//		ELSE
//			NET_PRINT(" FALSE") NET_NL()
//		ENDIF
//	#ENDIF
//	
//	IF bSet
//		IF NOT IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_AcceptedInvite)
//			SET_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_AcceptedInvite)
//		ENDIF
//	ELSE
//		IF IS_BIT_SET(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_AcceptedInvite)
//			CLEAR_BIT(fmTriggerTutStruct.iFmTriggerTutorialBitset, biTrigTut_AcceptedInvite)
//		ENDIF
//	ENDIF
//ENDPROC



PROC SET_LOCAL_PLAYER_READY_FOR_HOLD_UP_TUT(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_READY_FOR_HOLD_UP_TUT] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReadyForHoldUpTut)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_DOING_HOLD_UP_TUT(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_DOING_HOLD_UP_TUT] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_DoingHoldUpTut)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_DoingHoldUpTut)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_DoingHoldUpTut)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_DoingHoldUpTut)
		ENDIF
	ENDIF
ENDPROC


PROC SET_LOCAL_PLAYER_READY_TO_DO_TUT_HOLD_UP(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_READY_TO_DO_TUT_HOLD_UP] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReadyToDoHoldUp)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReadyToDoHoldUp)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReadyToDoHoldUp)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_ReadyToDoHoldUp)
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_USING_TUTORIAL_HOLD_UP_STORE(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [HoldUpTut] [HoldUpTutClient] [SET_LOCAL_PLAYER_USING_TUTORIAL_HOLD_UP_STORE]  called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_IN_TUT_HOLD_UP)
			SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_IN_TUT_HOLD_UP)
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_IN_TUT_HOLD_UP)
			CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_IN_TUT_HOLD_UP)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_USING_TUTORIAL_HOLD_UP_STORE()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_IN_TUT_HOLD_UP)
ENDFUNC

PROC SET_LOCAL_PLAYER_COMPLETED_HOLD_UP_TUT(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_COMPLETED_HOLD_UP_TUT] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_CompletedHoldUpTut)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_CompletedHoldUpTut)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_CompletedHoldUpTut)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_CompletedHoldUpTut)
		ENDIF
	ENDIF
ENDPROC


PROC SET_LOCAL_PLAYER_HAD_WANTED_LEVEL_WHEN_COMPLETEING_HOLD_UP_TUT(BOOL bSet)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_HAD_WANTED_LEVEL_WHEN_COMPLETEING_HOLD_UP_TUT] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_HadWantedLevelWhenComplete)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_HadWantedLevelWhenComplete)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_HadWantedLevelWhenComplete)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iHoldUpTutBitset, biHT_HadWantedLevelWhenComplete)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DID_PLAYER_HAVE_WANTED_LEVEL_WHEN_COMPLETING_HOLD_UP_TUT(PLAYER_INDEX player)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iHoldUpTutBitset, biHT_HadWantedLevelWhenComplete)
ENDFUNC

PROC SET_FREEMODE_FLOW_COMMS_DISABLED(BOOL bSet)
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("[dsw] [SET_FREEMODE_FLOW_COMMS_DISABLED] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
		
		DEBUG_PRINTCALLSTACK()
		
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet10, BI_FM_NMH10_DIS_FLOW_COMMS)
			SET_BIT(MPGlobalsAmbience.iFmNmhBitSet10, BI_FM_NMH10_DIS_FLOW_COMMS)
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet10, BI_FM_NMH10_DIS_FLOW_COMMS)
			CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet10, BI_FM_NMH10_DIS_FLOW_COMMS)
		ENDIF
	ENDIF
ENDPROC 

FUNC BOOL ARE_FREEMODE_FLOW_COMMS_DISABLED()
	RETURN (IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet10, BI_FM_NMH10_DIS_FLOW_COMMS))
ENDFUNC

PROC SET_LOCAL_PLAYER_DOING_KILL_LIST(BOOL bSet)
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("[dsw] [SET_LOCAL_PLAYER_DOING_KILL_LIST] called with... ")
		IF bSet
			NET_PRINT(" TRUE") NET_NL()
		ELSE
			NET_PRINT(" FALSE") NET_NL()
		ENDIF
		
		DEBUG_PRINTCALLSTACK()
		
	#ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet10, BI_FM_NMH10_DOING_UW)
			SET_BIT(MPGlobalsAmbience.iFmNmhBitSet10, BI_FM_NMH10_DOING_UW)
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet10, BI_FM_NMH10_DOING_UW)
			CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet10, BI_FM_NMH10_DOING_UW)
		ENDIF
	ENDIF
ENDPROC 

FUNC BOOL IS_LOCAL_PLAYER_DOING_KILL_LIST()
	RETURN (IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet10, BI_FM_NMH10_DOING_UW))
ENDFUNC


/// PURPOSE: Get the units the current challenge uses when displaying the score
///    
/// RETURNS: A text label with the units
///    
FUNC STRING GET_CHALLENGE_UNITS(AM_FREEMODE_CHALLENGES challenge, BOOL bForLeaderboard=FALSE, BOOL bForBottomRightHUD=FALSE, INT iDisplayScore = 0)

	SWITCH(challenge)
		CASE AM_CHALLENGE_LONGEST_JUMP		
		CASE AM_CHALLENGE_LONGEST_STOPPIE	
		CASE AM_CHALLENGE_LONGEST_WHEELIE
		CASE AM_CHALLENGE_LONGEST_NO_CRASH
		CASE AM_CHALLENGE_LONGEST_SKYDIVE
		CASE AM_CHALLENGE_LOWEST_PARACHUTE
		CASE AM_CHALLENGE_REVERSE_DRIVING
		CASE AM_CHALLENGE_LONGEST_FALL
		CASE AM_CHALLENGE_LONGEST_BAIL
		CASE AM_CHALLENGE_LOW_FLYING
		CASE AM_CHALLENGE_LOW_FLYING_INVERTED
			IF SHOULD_USE_METRIC_MEASUREMENTS()
				IF bForLeaderboard
					RETURN "AMCH_M_LB"
				ELIF bForBottomRightHUD
					RETURN "AMCH_M"
				ELSE
					RETURN "AMCH_METRES"
				ENDIF
			ELSE
				IF bForLeaderboard
					RETURN "AMCH_FT_LB"
				ELIF bForBottomRightHUD
					RETURN "AMCH_FT"
				ELSE
					RETURN "AMCH_FEET"
				ENDIF
			ENDIF
		BREAK
		
		CASE AM_CHALLENGE_HIGHEST_SPEED	
			IF SHOULD_USE_METRIC_MEASUREMENTS()
				IF bForLeaderboard
					RETURN "AMCH_KMH_LB"
				ELIF bForBottomRightHUD
					RETURN "AMCH_KMHN"
				ELSE
					RETURN "AMCH_KMH"
				ENDIF
			ELSE
				IF bForLeaderboard
					RETURN "AMCH_MPH_LB"
				ELIF bForBottomRightHUD
					RETURN "AMCH_MPHN"
				ELSE
					RETURN "AMCH_MPH"
				ENDIF
			ENDIF
		BREAK
						
		CASE AM_CHALLENGE_MOST_VEHICLES_STOLEN
			IF bForLeaderboard OR bForBottomRightHUD
				RETURN ""
			ELSE
				RETURN "AMCH_VEH"
			ENDIF
		BREAK
			
		CASE AM_CHALLENGE_PVP_HEADSHOTS
		CASE AM_CHALLENGE_PVP_DRIVEBY
		CASE AM_CHALLENGE_PVP_MELEE
		CASE AM_CHALLENGE_PVP_LONGEST_SNIPE
			IF bForLeaderboard OR bForBottomRightHUD
				RETURN ""
			ELSE
				IF iDisplayScore != 1
					RETURN "AMCH_KILLS"
				ELSE
					RETURN "AMCH_KILL"
				ENDIF
			ENDIF
		BREAK
					
	ENDSWITCH
	
	IF bForLeaderboard OR bForBottomRightHUD
		RETURN ""
	ELSE
		RETURN "AMCH_EMPTY"
	ENDIF
	
ENDFUNC

/// PURPOSE: Handles conversions between metric and imperial measurements
///    
/// PARAMS:
///    iScore - The player's score in ingame units
/// RETURNS: The player's score in the correct regional units
///    
FUNC FLOAT CONVERT_FOR_CHALLENGE(AM_FREEMODE_CHALLENGES challenge, INT iScore)

	SWITCH(challenge)
		CASE AM_CHALLENGE_LONGEST_JUMP		
		CASE AM_CHALLENGE_LONGEST_STOPPIE	
		CASE AM_CHALLENGE_LONGEST_WHEELIE
		CASE AM_CHALLENGE_LONGEST_NO_CRASH
		CASE AM_CHALLENGE_LONGEST_SKYDIVE
		CASE AM_CHALLENGE_LOWEST_PARACHUTE
		CASE AM_CHALLENGE_REVERSE_DRIVING
		CASE AM_CHALLENGE_LONGEST_FALL
		CASE AM_CHALLENGE_LONGEST_BAIL
		CASE AM_CHALLENGE_LOW_FLYING
		CASE AM_CHALLENGE_LOW_FLYING_INVERTED
			IF SHOULD_USE_METRIC_MEASUREMENTS()
				RETURN TO_FLOAT(iScore)
			ELSE
				RETURN CONVERT_METERS_TO_FEET(TO_FLOAT(iScore))
			ENDIF
		BREAK
			 
		CASE AM_CHALLENGE_HIGHEST_SPEED	
			IF SHOULD_USE_METRIC_MEASUREMENTS()
				RETURN TO_FLOAT(iScore)
			ELSE
				RETURN CONVERT_KPH_TO_MPH(TO_FLOAT(iScore))
			ENDIF
		BREAK
			
	ENDSWITCH
	
	RETURN TO_FLOAT(iScore)

ENDFUNC


/// PURPOSE: Check if challenge uses a float score
///    
/// RETURNS: TRUE if challenge uses float, FALSE otherwise
///    
FUNC BOOL DOES_CHALLENGE_USE_FLOAT_FOR_LBD(AM_FREEMODE_CHALLENGES eChallengeType)

	SWITCH(eChallengeType)
		CASE AM_CHALLENGE_LONGEST_JUMP			
		CASE AM_CHALLENGE_HIGHEST_SPEED		
		CASE AM_CHALLENGE_LONGEST_STOPPIE		
		CASE AM_CHALLENGE_LONGEST_NO_CRASH	
		CASE AM_CHALLENGE_LONGEST_WHEELIE
		CASE AM_CHALLENGE_LONGEST_SKYDIVE
		CASE AM_CHALLENGE_LOWEST_PARACHUTE
		CASE AM_CHALLENGE_REVERSE_DRIVING
		CASE AM_CHALLENGE_LONGEST_FALL
		CASE AM_CHALLENGE_LONGEST_BAIL
		CASE AM_CHALLENGE_LOW_FLYING	
		CASE AM_CHALLENGE_LOW_FLYING_INVERTED
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

/// PURPOSE: Check if challenge uses a float score
///    
/// RETURNS: TRUE if challenge uses float, FALSE otherwise
///    
FUNC BOOL DOES_CHALLENGE_USE_TIME_FOR_LBD(AM_FREEMODE_CHALLENGES eChallengeType)

	RETURN FALSE

	SWITCH(eChallengeType)
		CASE AM_CHALLENGE_LOW_FLYING	
		CASE AM_CHALLENGE_LOW_FLYING_INVERTED
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

/// PURPOSE: Formats the player's score into the correct data type
///    
/// PARAMS:
///    iReturnScore - The value is returned here if the score is converted to an int
///    fReturnScore - The value is returned here if the score is converted to an float
///    iPassed - The original value that is passed in
FUNC BOOL FORMAT_SCORE_FOR_LBD_DISPLAY(INT &iReturnScore,FLOAT &fReturnScore, INT iPassed, AM_FREEMODE_CHALLENGES eChallengeType)

	IF DOES_CHALLENGE_USE_FLOAT_FOR_LBD(eChallengeType)
		fReturnScore = (CONVERT_FOR_CHALLENGE(eChallengeType, iPassed) / 10.0)
		RETURN TRUE
	ELIF DOES_CHALLENGE_USE_TIME_FOR_LBD(eChallengeType)
		fReturnScore = (CONVERT_FOR_CHALLENGE(eChallengeType, iPassed) / 1000.0) // Convert ms to s
		RETURN TRUE
	ELSE
		iReturnScore = iPassed
		RETURN FALSE
	ENDIF

ENDFUNC

 // FEATURE_NEW_AMBIENT_EVENTS

//Maintain the ped and vehicle densitys in freemode. 
PROC FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME(	FLOAT fPed = 0.0, 		FLOAT fSecnarioPedInside = 0.0, FLOAT fSecnarioPedOutSide = 0.0, 
												FLOAT fVehicle = 0.0,	FLOAT fVehicleRandom = 0.0, 	FLOAT fVehicleParked = 0.0, 		FLOAT fVehicleAmbient = 0.0,
												BOOL  bDisableTrain = FALSE)
	// Speirs added this check for 2050154, Jobs should be using the new server only commands (SERVER_SET_PED_AND_TRAFFIC_DENSITIES, SERVER_CLEAR_PED_AND_TRAFFIC__DENSITIES)
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(fPed)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(fVehicle)
	ENDIF
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(fSecnarioPedInside, fSecnarioPedOutSide)
	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(fVehicleRandom)
	SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(fVehicleParked)
	
	IF (fVehicleAmbient = 0)
		// redundant
	ENDIF
	//SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(fVehicleAmbient) // phil asked to remove this, see  1323419
	SET_DISABLE_RANDOM_TRAINS_THIS_FRAME(bDisableTrain)
ENDPROC

//PURPOSE: Draws the Kill Streak display in the bottom right
PROC DRAW_KILLSTREAK_DISPLAY(INT iTotalKills) 
	
	FLOAT fKillStreakMultiplier = 1
	FLOAT fKillStreakTunable = 1
	IF IS_ON_DEATHMATCH_GLOBAL_SET()
//		fKillStreakTunable = g_sMPTunables.fearnings_Killstreak_Bonus_on_Deathmatch_modifier
		//1737572
		IF iTotalKills >= BIG_STREAK
			fKillStreakTunable = g_sMPTunables.fxp_tunable_10_kills
		ELIF iTotalKills >= MEDIUM_STREAK
			fKillStreakTunable = g_sMPTunables.fxp_tunable_6_kills
		ELIF iTotalKills >= SMALL_STREAK
			fKillStreakTunable = g_sMPTunables.fxp_tunable_3_kills	
		ENDIF
	ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION 
		fKillStreakTunable = g_sMPTunables.fearnings_Killstreak_Bonus_on_Mission_modifier 
	ENDIF
	
	IF iTotalKills >= BIG_STREAK 
		fKillStreakMultiplier = (LARGE_STREAK_MULTIPLIER*fKillStreakTunable)
	ELIF iTotalKills >= MEDIUM_STREAK
		fKillStreakMultiplier = (MED_STREAK_MULTIPLIER*fKillStreakTunable)
	ELIF iTotalKills >= SMALL_STREAK
		fKillStreakMultiplier = (SMALL_STREAK_MULTIPLIER*fKillStreakTunable)
	ENDIF
	
	IF fKillStreakMultiplier <> 1
		
		DRAW_GENERIC_SCORE(0, "HUD_KSTREAK", 4000, HUD_COLOUR_WHITE, HUDORDER_TOP, FALSE, "", TRUE, fKillStreakMultiplier, HUDFLASHING_NONE,
							0, HUD_COLOUR_PURE_WHITE, FALSE, 0, FALSE, ACTIVITY_POWERUP_XP)
//		DRAW_GENERIC_SCORE(0, "HUD_KSTREAK", 4000, HUD_COLOUR_WHITE,HUDORDER_TOP, FALSE, "HUD_KSMULTI", TRUE, fKillStreakMultiplier) //Kill Streak	x~1~ <XP>
	ENDIF
ENDPROC


FUNC BOOL FMMC_DOES_THIS_LINE_SLOPE_TOO_MUCH(VECTOR vec1, VECTOR vec2, float fMaxSlopeAngle)

	FLOAT fHeightDifference
	FLOAT fDistanceBetween
	FLOAT fAngleBetween
	
	fHeightDifference = vec1.z - vec2.z	
	IF fHeightDifference > 0.2
	OR fHeightDifference < -0.2
		fDistanceBetween = GET_DISTANCE_BETWEEN_COORDS(<<vec1.x, vec1.y, 0>>, <<vec2.x, vec2.y, 0>>)	
		fAngleBetween = GET_ANGLE_BETWEEN_2D_VECTORS(fDistanceBetween, 0, fDistanceBetween, fHeightDifference)
		IF fAngleBetween > fMaxSlopeAngle
			//PRINTLN("Failed. Angle = ", fAngleBetween, " fHeightDifference = ", fHeightDifference)
			RETURN TRUE
		ENDIF
	ENDIF
	
	return FALSE
ENDFUNC

FUNC BOOL FMMC_IS_THIS_A_SLOPE(VECTOR vCoronaPos, ENTITY_INDEX eiPassed = NULL, FLOAT fMaxSlopeAngle = 12.0, FLOAT fRange = 0.5, BOOL bCap = TRUE, FLOAT fZDiff = 1.5, BOOL bForceWater = FALSE)

	CONST_FLOAT	ciZ_UP					1.0
	CONST_INT	ciX_OUT					0
	CONST_INT	ciY_OUT					1
	CONST_INT	ciX_OUT_MIN 			2
	CONST_INT	ciY_OUT_MIN 			3
	CONST_INT	ciMAX_OFFSETS 			4
	CONST_INT	ciVECTOR_TOPL			0
	CONST_INT	ciVECTOR_TOP			1
	CONST_INT	ciVECTOR_TOPR			2
	CONST_INT	ciVECTOR_RIGHT			3
	CONST_INT	ciVECTOR_BOTL			4
	CONST_INT	ciVECTOR_BOT			5
	CONST_INT	ciVECTOR_BOTR			6
	CONST_INT	ciVECTOR_LEFT			7
	CONST_INT	ciVECTOR_MAX			8
	CONST_INT	ciTOP_SIDE_R			0
	CONST_INT	ciTOP_SIDE_L			1
	CONST_INT	ciRIGHT_SIDE_T			2
	CONST_INT	ciRIGHT_SIDE_B			3
	CONST_INT	ciBOTTOM_SIDE_R			4
	CONST_INT	ciBOTTOM_SIDE_L			5
	CONST_INT	ciLEFT_SIDE_T			6
	CONST_INT	ciLEFT_SIDE_B			7
	CONST_INT	ciUP_DOWN				8
	CONST_INT	ciLEFT_RIGHT			9
	CONST_INT	ciMAX_SIDES				10
	VECTOR 		vPos[ciVECTOR_MAX]
	VECTOR 		vCam
	FLOAT 		fOff[ciMAX_OFFSETS]
	FLOAT 		fGroundZ[ciVECTOR_MAX]
	INT i
	
	IF g_FMMC_STRUCT.iRaceType =  FMMC_RACE_TYPE_BOAT			
	OR g_FMMC_STRUCT.iRaceType =  FMMC_RACE_TYPE_BOAT_P2P
	OR bForceWater
		FLOAT fTemp
		IF TEST_VERTICAL_PROBE_AGAINST_ALL_WATER( vCoronaPos+ <<0.0, 0.0, 2.5>>,SCRIPT_INCLUDE_MOVER,fTemp) = SCRIPT_WATER_TEST_RESULT_WATER
			RETURN FALSE
		ENDIF
	ENDIF
	
	//get the rotation
	vCam = GET_FINAL_RENDERED_CAM_ROT()
	
	//Set the offsets 
	fOff[ciX_OUT]			= fRange
	fOff[ciY_OUT]			= fRange
	fOff[ciX_OUT_MIN] 		= -fRange 
	fOff[ciY_OUT_MIN] 		= -fRange 
	//If an entity has been passed work out the offsets and rotation based on that. 
	IF eiPassed != NULL
		IF DOES_ENTITY_EXIST(eiPassed)
			MODEL_NAMES mnTemp = GET_ENTITY_MODEL(eiPassed)
			REQUEST_MODEL(mnTemp)
			IF HAS_MODEL_LOADED(mnTemp)
				VECTOR vMax, vMin
				GET_MODEL_DIMENSIONS(mnTemp, vMax, vMin)
				fOff[ciX_OUT]		= (vMax.x * 0.7)
				fOff[ciY_OUT]		= (vMax.y * 0.7)
				fOff[ciX_OUT_MIN]	= (vMin.x * 0.7) 
				fOff[ciY_OUT_MIN]	= (vMin.y * 0.7)
				vCam 				= GET_ENTITY_ROTATION(eiPassed)
			ENDIF
		ENDIF
	ENDIF
	
	//Cap things if the bool is set
	IF bCap
		FOR i = 0 TO (ciMAX_OFFSETS - 1)
			IF fOff[i]	 < fRange
			AND fOff[i]	 > 0  
				fOff[i] = fRange
			ELIF fOff[i] > 	-fRange
			AND fOff[i]  <	0  
				fOff[i] = -fRange
			ENDIF
		ENDFOR
	ENDIF
	
	//Get some offsets based on the current heading
	vPos[ciVECTOR_TOP]		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, vCam.z, << 0.0,  				fOff[ciY_OUT], 		ciZ_UP>>)
	vPos[ciVECTOR_TOPR]		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, vCam.z, << fOff[ciX_OUT],		fOff[ciY_OUT], 		ciZ_UP>>)
	vPos[ciVECTOR_RIGHT]	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, vCam.z, << fOff[ciX_OUT],  		0.0, 				ciZ_UP>>)
	vPos[ciVECTOR_BOTR]		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, vCam.z, << fOff[ciX_OUT], 		fOff[ciY_OUT_MIN],	ciZ_UP>>)
	
	vPos[ciVECTOR_BOT]		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, vCam.z, << 0.0, 				fOff[ciY_OUT_MIN],	ciZ_UP>>)
	vPos[ciVECTOR_BOTL]		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, vCam.z, << fOff[ciX_OUT_MIN], 	fOff[ciY_OUT_MIN],	ciZ_UP>>)
	vPos[ciVECTOR_LEFT]		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, vCam.z, << fOff[ciX_OUT_MIN],	0.0, 				ciZ_UP>>)
	vPos[ciVECTOR_TOPL]		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, vCam.z, << fOff[ciX_OUT_MIN],  	fOff[ciY_OUT], 		ciZ_UP>>)
	
//	PRINTLN("Max Angle = ", fMaxSlopeAngle)
	//Calculate a ground Z
	FOR i = 0 TO (ciVECTOR_MAX - 1)
		IF NOT IS_VECTOR_ZERO(vPos[i])
			IF NOT GET_GROUND_Z_FOR_3D_COORD(vPos[i], fGroundZ[i])
				//If we can't get a ground Z then it is broken
//				PRINTLN("i = ", i, ", NOT GET_GROUND_Z_FOR_3D_COORD, vPos[i].z = ", vPos[i].z, ", fGroundZ[i] = ", fGroundZ[i])
				RETURN TRUE
			ENDIF
			IF vPos[i].z - fGroundZ[i] > fZDiff
			OR vPos[i].z - fGroundZ[i] < -fZDiff
//				PRINTLN("i = ", i, ", vPos[i].z = ", vPos[i].z, ", fGroundZ[i] = ", fGroundZ[i])
				RETURN TRUE	
			ENDIF
			vPos[i].z = fGroundZ[i]	
		ENDIF
	ENDFOR
	
	//Calculate the diffrences in height between the sectios of the sides
	//Topside
	if FMMC_DOES_THIS_LINE_SLOPE_TOO_MUCH(vPos[ciVECTOR_TOPL], vPos[ciVECTOR_TOPR], fMaxSlopeAngle)
		RETURN TRUE
	ENDIF
	
	//Right	
	if FMMC_DOES_THIS_LINE_SLOPE_TOO_MUCH(vPos[ciVECTOR_TOPR], vPos[ciVECTOR_BOTR], fMaxSlopeAngle)
		RETURN TRUE
	ENDIF
	
	//Bottom
	if FMMC_DOES_THIS_LINE_SLOPE_TOO_MUCH(vPos[ciVECTOR_BOTR], vPos[ciVECTOR_BOTL], fMaxSlopeAngle)
		RETURN TRUE
	ENDIF
	
	//Left
	if FMMC_DOES_THIS_LINE_SLOPE_TOO_MUCH(vPos[ciVECTOR_BOTL], vPos[ciVECTOR_TOPL], fMaxSlopeAngle)
		RETURN TRUE
	ENDIF
	
	//Across
	if FMMC_DOES_THIS_LINE_SLOPE_TOO_MUCH(vPos[ciVECTOR_TOPR], vPos[ciVECTOR_BOTL], fMaxSlopeAngle)
		RETURN TRUE
	ENDIF
	if FMMC_DOES_THIS_LINE_SLOPE_TOO_MUCH(vPos[ciVECTOR_TOPL], vPos[ciVECTOR_BOTR], fMaxSlopeAngle)
		RETURN TRUE
	ENDIF

	//This is not  a slope so it's OK
	RETURN FALSE
	
ENDFUNC


//PURPOSE: Retursn TRUE if its okay to do a Control Pressed check
FUNC BOOL SAFE_TO_DO_INPUT_CHECK(BOOL bDoCustomMenuCheck = TRUE, BOOL bDoPhoneOnScreenCheck = TRUE)
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF bDoCustomMenuCheck = TRUE
		IF IS_CUSTOM_MENU_ON_SCREEN()
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		RETURN FALSE
	ENDIF
	
	IF bDoPhoneOnScreenCheck = TRUE
		IF IS_PHONE_ONSCREEN()
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		RETURN FALSE
	ENDIF
	
	IF IS_SYSTEM_UI_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF g_bInATM
		RETURN FALSE
	ENDIF
	
	IF g_bBrowserVisible
		RETURN FALSE
	ENDIF
	
	IF IS_WARNING_MESSAGE_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF g_bMissionEnding
		RETURN FALSE
	ENDIF
	
	IF g_b_OnLeaderboard
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


//PURPOSE: Returns TRUE if the Freemode Intro Cutscene has been played (if the stat is set)
FUNC BOOL HAS_FREEMODE_INTRO_BEEN_DONE()
//	RETURN FALSE
	#IF IS_DEBUG_BUILD
		IF g_ShouldShiftingTutorialsBeSkipped = TRUE
		//	NET_NL() NET_PRINT("HAS_FREEMODE_INTRO_BEEN_DONE True because g_ShouldShiftingTutorialsBeSkipped") NET_NL()
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF g_SkipFmTutorials
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD()
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTERED_OFFLINE_SAVE_FM()
		RETURN TRUE
	ENDIF
	
	//RETURN GET_MP_BOOL_PLAYER_STAT(MPPLY_FM_INTRO_CUT_DONE_INDEX)
	RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_CUT_DONE)
ENDFUNC


FUNC BOOL GET_IS_LEADERBOARD_CAM_ACTIVE_OR_INTERPING()
	RETURN g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping
ENDFUNC

//PURPOSE: Retursn TRUE if it is safe to do the cinematic cam
FUNC BOOL CAN_DO_CINEMATIC_SHOT() 
	
	IF IS_PHONE_ONSCREEN()
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC MAINTAIN_CINEMATIC_SHOT_OF_ENTITY(MP_MISSION eMission, NETWORK_INDEX netId, FLOAT fTriggerDistance, STRING strHelpLabel, INT &iStage, BOOL &bHelpFlag)
	
	ENTITY_INDEX eiEntity	//viVeh
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(netId)		//IF IS_NET_VEHICLE_DRIVEABLE(netId)
			
			eiEntity = NET_TO_ENT(netId)
			
			IF NOT IS_ENTITY_DEAD(eiEntity)
				
				SWITCH iStage
					CASE 0
						IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(eiEntity)) <= fTriggerDistance
							SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
							SETTIMERB(0)
							iStage++
							NET_PRINT("[WJK] - MAINTAIN_CINEMATIC_SHOT - Entity within trigger distance, setting cinematic cam button inactive.")NET_NL()
						ENDIF
					BREAK
					
					CASE 1
						
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
						
						//IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
						IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
							IF CAN_DO_CINEMATIC_SHOT()
								IF TIMERB() > 275
									IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(eiEntity)) <= fTriggerDistance
										IF MPGlobals.eEntityCinematicMission = eNULL_MISSION
										OR MPGlobals.eEntityCinematicMission = eMission
											IF NOT IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_HELI_CHASE)
												SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
												CREATE_CINEMATIC_SHOT(SHOTTYPE_HELI_CHASE, 100000, NULL, eiEntity)
												MPGlobals.eEntityCinematicMission = eMission
												NET_PRINT("[WJK] - MAINTAIN_CINEMATIC_SHOT - Player pressing circle, creating cinematic shot - eMission = ") NET_PRINT_INT(ENUM_TO_INT(MPGlobals.eEntityCinematicMission)) NET_NL()
											ELSE
												IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(strHelpLabel)
													CLEAR_HELP()
												ENDIF
												iStage++
												NET_PRINT("[WJK] - MAINTAIN_CINEMATIC_SHOT - Created cinematic shot.")NET_NL()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT bHelpFlag
								IF CAN_DO_CINEMATIC_SHOT()
									IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(eiEntity)) <= fTriggerDistance
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											PRINT_HELP(strHelpLabel)
											bHelpFlag = TRUE
											NET_PRINT("[WJK] -  MAINTAIN_CINEMATIC_SHOT - Displayed help.")NET_NL()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF eMission = MPGlobals.eEntityCinematicMission
								IF IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_HELI_CHASE)
									STOP_CINEMATIC_SHOT(SHOTTYPE_HELI_CHASE)
								ENDIF
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								MPGlobals.eEntityCinematicMission = eNULL_MISSION
								NET_PRINT("[WJK] - MAINTAIN_CINEMATIC_SHOT - eMission CLEARED - B") NET_NL()
							ENDIF
							SETTIMERB(0)
						ENDIF
					BREAK
					
					CASE 2
						IF IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_HELI_CHASE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
							IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
							OR GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(eiEntity)) > fTriggerDistance
								STOP_CINEMATIC_SHOT(SHOTTYPE_HELI_CHASE)
								NET_PRINT("[WJK] - MAINTAIN_CINEMATIC_SHOT - Player stopped holding circle, stopping cinematic shot.")NET_NL()
							ENDIF
						ELSE
							IF eMission = MPGlobals.eEntityCinematicMission
								MPGlobals.eEntityCinematicMission = eNULL_MISSION
								SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								NET_PRINT("[WJK] - MAINTAIN_CINEMATIC_SHOT - FINISHED - eMission = ") NET_PRINT_INT(ENUM_TO_INT(MPGlobals.eEntityCinematicMission)) NET_NL()
							ENDIF
							iStage = 0
							NET_PRINT("[WJK] - MAINTAIN_CINEMATIC_SHOT - Cinematic shot complete. Finished!")NET_NL()
						ENDIF
					BREAK
				ENDSWITCH
				
			ENDIF
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(strHelpLabel)
			CLEAR_HELP()
		ENDIF
		IF eMission = MPGlobals.eEntityCinematicMission
			STOP_CINEMATIC_SHOT(SHOTTYPE_HELI_CHASE)
			MPGlobals.eEntityCinematicMission = eNULL_MISSION
			SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			NET_PRINT("[WJK] - MAINTAIN_CINEMATIC_SHOT - RESET - eMission = ") NET_PRINT_INT(ENUM_TO_INT(MPGlobals.eEntityCinematicMission)) NET_NL()
		ENDIF
		iStage = 0
		NET_PRINT("[WJK] - MAINTAIN_CINEMATIC_SHOT - PLAYER DEAD - RESET")NET_NL()
	ENDIF
	
ENDPROC

//PURPOSE: Stops the cinematic shot and activates the cinematic cam button again.
PROC CLEANUP_CINEMATIC_SHOT_OF_ENTITY(MP_MISSION eMission, STRING strHelpLabel)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(strHelpLabel)
		CLEAR_HELP()
	ENDIF
	IF eMission = MPGlobals.eEntityCinematicMission
   		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		STOP_CINEMATIC_SHOT(SHOTTYPE_HELI_CHASE)
		MPGlobals.eEntityCinematicMission = eNULL_MISSION
		NET_PRINT("[WJK] - MAINTAIN_CINEMATIC_SHOT - CLEANUP_CINEMATIC_SHOT_OF_ENTITY - DONE - eMission = ") NET_PRINT_INT(ENUM_TO_INT(MPGlobals.eEntityCinematicMission)) NET_NL()
	ELSE
		NET_PRINT("[WJK] - MAINTAIN_CINEMATIC_SHOT - CLEANUP_CINEMATIC_SHOT_OF_ENTITY - NOT DONE - eMission = ") NET_PRINT_INT(ENUM_TO_INT(MPGlobals.eEntityCinematicMission)) NET_NL()
	ENDIF
ENDPROC


FUNC BOOL IS_OK_TO_PRINT_FREEMODE_HELP(BOOL bCheckOnAmbient = TRUE, BOOL bCheckOnMission = TRUE, BOOL bCheckHelpShowing = TRUE, BOOL bCheckControl = TRUE)
	#IF IS_DEBUG_BUILD
		BOOL bDebug = TRUE
	#ENDIF
	IF bCheckHelpShowing
	AND IS_HELP_MESSAGE_BEING_DISPLAYED()
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_HELP_MESSAGE_BEING_DISPLAYED")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_ANY_CONVERSATION_ONGOING_OR_QUEUED")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_SCREEN_FADED_IN")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_CUSTOM_MENU_ON_SCREEN")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_FM_MISSION_LAUNCH_IN_PROGRESS")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF bCheckOnMission
		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			#IF IS_DEBUG_BUILD
			IF bDebug
				PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_PLAYER_ON_ANY_FM_MISSION")
			ENDIF
			#ENDIF
		
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF bCheckOnAmbient
		IF IS_PLAYER_ON_ANY_MP_AMBIENT_SCRIPT(PLAYER_ID())
			#IF IS_DEBUG_BUILD
			IF bDebug
				PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_PLAYER_ON_ANY_MP_AMBIENT_SCRIPT")
			ENDIF
			#ENDIF
		
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_MISSION_SUMMARY_SCREEN_DISPLAYED")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_PED_INJURED")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_PAUSE_MENU_ACTIVE")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF bCheckControl
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			#IF IS_DEBUG_BUILD
			IF bDebug
				PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_PLAYER_CONTROL_ON")
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
//	IF GET_IS_LEADERBOARD_CAM_ACTIVE_OR_INTERPING() 
//		PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False 12")
//		RETURN FALSE
//	ENDIF
	
	IF g_Show_Lap_Dpad // Chris' dpad-down player list
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False g_Show_Lap_Dpad")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_SESSION_LAUNCHING()
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_TRANSITION_SESSION_LAUNCHING")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_PLAYER_IN_CORONA")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF g_bBrowserVisible
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False g_bBrowserVisible")
		ENDIF
		#ENDIF
		
		//-- Internet active
		RETURN FALSE
	ENDIF
	
	IF g_bAtmShowing
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False g_bAtmShowing")
		ENDIF
		#ENDIF
		
		
		//-- Atm front-end on screen
		RETURN FALSE
	ENDIF
	
	
	IF IS_LOCAL_PLAYER_AN_ANIMAL()
		#IF IS_DEBUG_BUILD
		IF bDebug
			PRINTLN("[IS_OK_TO_PRINT_FREEMODE_HELP] False IS_PLAYER_AN_ANIMAL")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Disables the Kill Yourself option
PROC DISABLE_KILL_YOURSELF_OPTION()
	MPGlobals.PlayerInteractionData.bDisableKillYourself = TRUE
	NET_PRINT_TIME() NET_PRINT(" ---> DISABLE_KILL_YOURSELF_OPTION - CALLED ") NET_NL()
ENDPROC

//PURPOSE: Enables the Kill Yourself option
PROC ENABLE_KILL_YOURSELF_OPTION()
	MPGlobals.PlayerInteractionData.bDisableKillYourself = FALSE
	NET_PRINT_TIME() NET_PRINT(" ---> ENABLE_KILL_YOURSELF_OPTION - CALLED ") NET_NL()
ENDPROC

FUNC BOOL IS_KILL_YOURSELF_OPTION_DISABLED()
	RETURN MPGlobals.PlayerInteractionData.bDisableKillYourself
ENDFUNC

//PURPOSE: Returns the cost of player to perform the suicide action
FUNC INT GET_KILL_YOURSELF_COST()
	
	#IF FEATURE_COPS_N_CROOKS
	IF IS_FREEMODE_ARCADE()
		IF ARC_COPS_CROOKS = GET_ARCADE_MODE()
			RETURN KILL_YOURSELF_COST_COPSCROOKS
		ENDIF
	ENDIF
	#ENDIF
	
	RETURN KILL_YOURSELF_COST_NORMAL
ENDFUNC

//PURPOSE: Returns the cost of player to perform the suicide action
FUNC FLOAT GET_KILL_YOURSELF_COST_FLOAT()
	RETURN TO_FLOAT(GET_KILL_YOURSELF_COST())
ENDFUNC

//PURPOSE: Retursn TRUE if the player has enough Cash to perform the suicide action
FUNC BOOL HAS_PLAYER_GOT_CASH_FOR_KILL_YOURSELF()
	RETURN (GET_PLAYER_CASH(PLAYER_ID()) >= GET_KILL_YOURSELF_COST())
ENDFUNC

//PURPOSE: Returns TRUE if player is allowed to use the Kill Yourself option
FUNC BOOL CAN_PLAYER_USE_KILL_YOURSELF()
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN FALSE
	ENDIF
		
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	
	IF IS_KILL_YOURSELF_OPTION_DISABLED()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_FREE_FOR_AMBIENT_TASK(PLAYER_ID()) 
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_INTERACTION_ANIM_PLAYING()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION)
	AND NOT g_bAllowKillYourselfOnMission
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_RACE)
		IF g_b_IsRallyRace = TRUE
		OR g_b_Is_Air_Race = TRUE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_AN_ANIMAL(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_BASE_JUMP)
		RETURN FALSE
	ENDIF
	
	IF IS_TARGET_ASSAULT_RACE()
		RETURN FALSE
	ENDIF
	
	
	IF IS_ON_IMPROMPTU_DEATHMATCH_GLOBAL_SET()
		RETURN FALSE
	ENDIF
	
	IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
	OR FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
	OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		RETURN FALSE
	ENDIF
	
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_IMPEXP_VELOCITY
	AND g_bDriverOfVelocityCar
		RETURN FALSE
	ENDIF

	IF IS_PROPERTY_OFFICE_MOD_GARAGE(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_ARENA_SERIES)
	OR IS_PLAYER_USING_ARENA()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_SURVIVAL)
		RETURN FALSE
	ENDIF 
	
	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
	OR IS_PLAYER_SCTV(PLAYER_ID())
	OR IS_PLAYER_SPECTATING(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_LEAVING_SIMPLE_INTERIOR_USING_THE_VALET()
	OR IS_PLAYER_LEAVING_ANY_SIMPLE_INTERIOR_USING_THE_LIMO_SERVICE()
		RETURN FALSE
	ENDIF
	
	IF HAVE_I_GOT_TRUCK_INVITE_INSTANCE()
	OR HAVE_I_GOT_AIRCRAFT_INVITE_INSTANCE()
	OR IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_WARPING_VIA_ENTER_WITH_NEARBY_PLAYER)
		PRINTLN("CAN_PLAYER_USE_KILL_YOURSELF player getting pulled intor interior with nearby request")
		RETURN FALSE
	ENDIF
	
	// Bug 6024477 Can we please make a few changes to the "Kill Yourself" option and normal suicides in freemode?
	IF  MPGlobalsAmbience.isuicidetimer > GET_GAME_TIMER()
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF  GB_IS_PLAYER_ON_ISLAND_HEIST_PREP_MISSION(PLAYER_ID())
	OR IS_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(PLAYER_ID())
	#IF FEATURE_TUNER
	OR GB_IS_PLAYER_ON_TUNER_ROBBERY_PREP_MISSION(PLAYER_ID())
	#ENDIF 

		RETURN FALSE
	ENDIF
	#ENDIF 
	RETURN TRUE
ENDFUNC

//PURPOSE: Clears the Kill Yourself Data
PROC KILL_YOURSELF_CLEANUP()
	IF NOT FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
	ENDIF
	REMOVE_ANIM_DICT("MP_SUICIDE")
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_IN_MP_CUTSCENE()	//url:bugstar:2805018
			CLEANUP_PLAYER_FOR_MP_SHOP_CUTSCENE()
			IF g_bShopCutsceneActive
				END_SHOP_CUTSCENE()
			ENDIF	
			PRINTLN("KILL_YOURSELF_CLEANUP(): player is still in network cut scene - clean up network cutscene")
		ENDIF	
	ENDIF	
	g_MultiplayerSettings.g_bSuicide = FALSE
	MPGlobals.KillYourselfData.sAnim = ""
	CLEAR_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)	// = 0
	CLEAR_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_PistolUsed)	
	CLEAR_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_PistolFired)	
	CLEAR_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_AnimEnded)	
	NET_PRINT(" ---> PROCESS_KILL_YOURSELF - CLEANED UP") NET_NL()
ENDPROC

//PURPOSE: Returns TRUE when the Kill Yoursefl anims are loaded
FUNC BOOL HAVE_KILL_YOURSELF_ANIMS_LOADED()
	REQUEST_ANIM_DICT("MP_SUICIDE")
		
	IF HAS_ANIM_DICT_LOADED("MP_SUICIDE")
		//NET_PRINT_TIME() NET_PRINT(" ---> PROCESS_KILL_YOURSELF - HAVE_KILL_YOURSELF_ANIMS_LOADED - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Turns off some inputs when in Kill Yourself
PROC DISABLE_CONTROLS_ON_KILL_YOURSELF()
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
ENDPROC

//PURPOSE: Turns player control off and takes money for choosing Kill Yourself
PROC INIT_KILL_YOURSELF()
	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
		//IF HAS_PLAYER_GOT_CASH_FOR_KILL_YOURSELF()	//REMOVED CHECK FOR Bug 1225327
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_ALLOW_PLAYER_DAMAGE | SPC_CLEAR_TASKS | SPC_LEAVE_CAMERA_CONTROL_ON | SPC_REENABLE_CONTROL_ON_DEATH)
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			PLAYSTATS_KILL_YOURSELF()
			DISABLE_CONTROLS_ON_KILL_YOURSELF()
			//INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_DEATHS)
			INT iCost = GET_KILL_YOURSELF_COST()
			INT iDifference
			IF NOT NETWORK_CAN_SPEND_MONEY2(iCost, FALSE, FALSE, FALSE, iDifference)
				iCost -= iDifference
				//APPLY_PED_DAMAGE_PACK(PLAYER_PED_ID(), "Explosion_Med", 0, 1.0) - DONE AT RESPAWN NOW
				MPGlobals.KillYourselfData.bDoDamageEffect = TRUE
				NET_PRINT(" ---> INIT_KILL_YOURSELF - NOT ENOUGH CASH - APPLY DAMAGE PACK TO PLAYER") NET_NL()
			ENDIF
			//**TWH - CMcM - 1443508 - Removed NETWORK_BUY_HEALTHCARE & NETWORK_SPENT_CASH_DROP if the player has no money.
			//IF iPlayerCash > 0
			IF iCost > 0
				IF USE_SERVER_TRANSACTIONS()
					INT iTransactionID
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_HEALTHCARE, iCost, iTransactionID)
				ELSE
					NETWORK_BUY_HEALTHCARE(iCost)
					GIVE_LOCAL_PLAYER_CASH(-iCost, TRUE)
				ENDIF
			ENDIF
			MPGlobalsAmbience.isuicidetimer = GET_GAME_TIMER()+ g_sMPTunables.isuicidecooldown // 5 minutes
			MPGlobalsAmbience.bcommitedsuicide = TRUE
			CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] I KILLED MYSELF THROUGH INTERACTION MENU")
			 //FEATURE_GANG_BOSS
			
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bJustKilledSelf	= TRUE
			NET_PRINT(" ---> INIT_KILL_YOURSELF - setting GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bJustKilledSelf") NET_NL()
			
			NET_PRINT(" ---> INIT_KILL_YOURSELF - CONTROL OFF") NET_NL()
		//ELSE
		//	KILL_YOURSELF_CLEANUP()
		//	EXIT
		//ENDIF
	ENDIF
ENDPROC

//PURPOSE: Retursn TRUE if the build is a version taht should use no anims
FUNC BOOL KILL_YOURSELF_NO_ANIM_TERRITORY()
	#IF IS_JAPANESE_BUILD
		RETURN TRUE
	#ENDIF
	
	/*IF IS_GERMAN_VERSION()
		RETURN TRUE
	ENDIF*/
	
	/*IF IS_AUSSIE_VERSION()
		RETURN TRUE
	ENDIF*/
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns the STatsenum required for STAT_NETWORK_INCREMENT_ON_SUICIDE
FUNC STATSENUM GET_DEATH_STAT()
	IF GET_ACTIVE_CHARACTER_SLOT() = 1
		RETURN MP1_DEATHS
//	ELIF GET_ACTIVE_CHARACTER_SLOT() = 2
//		RETURN MP2_DEATHS
//	ELIF GET_ACTIVE_CHARACTER_SLOT() = 3
//		RETURN MP3_DEATHS
//	ELIF GET_ACTIVE_CHARACTER_SLOT() = 4
//		RETURN MP4_DEATHS
	ENDIF
	
	RETURN MP0_DEATHS
ENDFUNC

FUNC STATSENUM GET_DEATH_PLAYER_STAT()
	IF GET_ACTIVE_CHARACTER_SLOT() = 1
		RETURN MP1_DEATHS_PLAYER
//	ELIF GET_ACTIVE_CHARACTER_SLOT() = 2
//		RETURN MP2_DEATHS
//	ELIF GET_ACTIVE_CHARACTER_SLOT() = 3
//		RETURN MP3_DEATHS
//	ELIF GET_ACTIVE_CHARACTER_SLOT() = 4
//		RETURN MP4_DEATHS
	ENDIF
	
	RETURN MP0_DEATHS_PLAYER
ENDFUNC

//PURPOSE: Broadcasts an even to make other players print a ticker about this player choosing Kill Yourself
PROC SEND_KILL_YOURSELF_TICKER()
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_KILL_YOURSELF
	TickerEventData.playerID = PLAYER_ID()
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS(FALSE))
ENDPROC

//PURPOSE: Broadcasts an even to make other players print a ticker about this player being eliminated by the bomb in Pass the Bomb
PROC SEND_PASS_THE_BOMB_ELIMINATED_TICKER()
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_PTB_ELIMINATED
	TickerEventData.playerID = PLAYER_ID()
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS(FALSE))
ENDPROC

//PURPOSE: If the player is in their Personal Vehicle then Impound it
PROC IMPOUND_PV_ON_KILL_YOURSELF()
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		//IF IS_PLAYER_PED_IN_PERSONAL_VEHICLE(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_MY_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				
				IF IS_THIS_MODEL_A_SPECIAL_VEHICLE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
				OR IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
					SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_SVM_IMPOUNDED)
					CLEANUP_MP_SAVED_VEHICLE(FALSE,FALSE,TRUE,FALSE,TRUE,FALSE,TRUE)
					NET_PRINT(" ---> PROCESS_KILL_YOURSELF - IMPOUND_PV_ON_KILL_YOURSELF - car cant be impounded, destroying instead.") NET_NL()
				ELSE
			
					MP_SAVE_VEHICLE_SET_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
					SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
					SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUND_HELP)
					MPGlobals.VehicleData.bFadingOutForWarp = FALSE
					MPGlobals.VehicleData.iTimesImpounded++
					NET_PRINT(" ---> PROCESS_KILL_YOURSELF - IMPOUND_PV_ON_KILL_YOURSELF - DONE") NET_NL()
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				NET_PRINT(" ---> PROCESS_KILL_YOURSELF - IMPOUND_PV_ON_KILL_YOURSELF - NOT IN PERSONAL VEHICLE") NET_NL()
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			NET_PRINT(" ---> PROCESS_KILL_YOURSELF - IMPOUND_PV_ON_KILL_YOURSELF - NOT IN VEHICLE") NET_NL()
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		NET_PRINT(" ---> PROCESS_KILL_YOURSELF - IMPOUND_PV_ON_KILL_YOURSELF - NO WANTED LEVEL") NET_NL()
	#ENDIF
	ENDIF
ENDPROC


//PURPOSE: Sets a flag if the player used Kill Yourself in a Deathmatch
PROC SET_KILL_YOURSELF_USED_IN_DM()
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_DEATHMATCH)
		SET_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_UsedInDeathmatch)
	ENDIF
ENDPROC

//PURPOSE: Clears a flag if the player used Kill Yourself in a Deathmatch
PROC CLEAR_KILL_YOURSELF_USED_IN_DM()
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_DEATHMATCH)
		CLEAR_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_UsedInDeathmatch)
	ENDIF
ENDPROC

//PURPOSE: Checks if a flag was set for the player using Kill Yourself in a Deathmatch
FUNC BOOL WAS_KILL_YOURSELF_USED_IN_DM()
	RETURN IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_UsedInDeathmatch)
ENDFUNC

// PURPOSE: For bug 4363483. To prevent the player from influencing their KILL/DEATH ratio by clearing last kill from the damager by using suicide. 
PROC SET_PLAYERS_HEALTH_ZERO_FOR_SUICIDE()
	
	INT timestamptimerwindow =  GET_GAME_TIMER() -  g_sMPTunables.iTimeWindowInstigatorsuicide
	ENTITY_INDEX entity_instigator_for_suicide = NULL
	VECTOR damagerscoordinates
	
	IF g_iLastDamagerPlayer != -1
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(g_iLastDamagerPlayer))
			damagerscoordinates = GET_ENTITY_COORDS( GET_PLAYER_PED(INT_TO_PLAYERINDEX(g_iLastDamagerPlayer)))
		ENDIF
	ENDIF
	
	IF g_iLastDamagerPlayer != -1
		IF g_iLastDamagerTimeStamp != -1
			IF g_iLastDamagerTimeStamp > timestamptimerwindow
				IF NOT IS_VECTOR_ZERO(damagerscoordinates)
					#IF IS_DEBUG_BUILD
						g_iLastDamagerdistance =  ROUND(GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), damagerscoordinates, TRUE))
					#ENDIF
				
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), damagerscoordinates, TRUE) <= g_sMPTunables.fDistanceWindowInstigatorsuicide
					
						PRINTLN("SET_PLAYERS_HEALTH_ZERO_FOR_SUICIDE() setting an instigator PLAYERID =  ", g_iLastDamagerPlayer )
						entity_instigator_for_suicide = GET_PLAYER_PED(INT_TO_PLAYERINDEX(g_iLastDamagerPlayer) )
						
					ELSE
						PRINTLN("SET_PLAYERS_HEALTH_ZERO_FOR_SUICIDE() Too far away exceeded GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), damagerscoordinates, TRUE)  = ", GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), damagerscoordinates, TRUE)  )
					ENDIF
				ELSE
				 	PRINTLN("SET_PLAYERS_HEALTH_ZERO_FOR_SUICIDE()damagerscoordinates   = ", damagerscoordinates )
				ENDIF
			ELSE
				PRINTLN("SET_PLAYERS_HEALTH_ZERO_FOR_SUICIDE() Time has exceeded g_iLastDamagerTimeStamp = ", g_iLastDamagerTimeStamp, " and timestamptimerwindow = ",timestamptimerwindow  )
			ENDIF
		ELSE
		 	PRINTLN("SET_PLAYERS_HEALTH_ZERO_FOR_SUICIDE() g_iLastDamagerTimeStamp = -1 ", g_iLastDamagerTimeStamp )
		ENDIF
	ELSE
		PRINTLN("SET_PLAYERS_HEALTH_ZERO_FOR_SUICIDE() g_iLastDamagerPlayer = -1 ", g_iLastDamagerPlayer )
	ENDIF
	
	SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0, entity_instigator_for_suicide )
	
	STATSENUM stattoincrement = GET_DEATH_STAT()
	
	IF entity_instigator_for_suicide != NULL
		MPGlobals.g_KillStrip.bIsInstigatedSuicide = TRUE
		stattoincrement = GET_DEATH_PLAYER_STAT()
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_DEATHS_PLAYER_SUICIDE)
	ELSE
		MPGlobals.g_KillStrip.bIsInstigatedSuicide = TRUE
	ENDIF
	
	STAT_NETWORK_INCREMENT_ON_SUICIDE(stattoincrement, 1.0)
	
ENDPROC

//PURPOSE: If the Kill Yourself flag is set controls player playing the correct anims and killing themselves.
PROC MAINTAIN_KILL_YOURSELF()
	IF g_MultiplayerSettings.g_bSuicide = TRUE
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			
			NET_PRINT(" ---> PROCESS_KILL_YOURSELF - RUNNING") NET_NL()
			
			//Bail Checks
			IF IS_PLAYER_IN_CORONA()
			OR IS_KILL_YOURSELF_OPTION_DISABLED()
			OR IS_PLAYER_SWITCH_IN_PROGRESS()
				KILL_YOURSELF_CLEANUP()
				NET_PRINT(" ---> PROCESS_KILL_YOURSELF - BAIL - PLAYER BUSY") NET_NL()
				EXIT
			ENDIF
			
			INIT_KILL_YOURSELF()
			DISABLE_CONTROLS_ON_KILL_YOURSELF()
			//Kill without Anims
			IF NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
			OR IS_PED_RAGDOLL(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				OR IS_ENTITY_IN_DEEP_WATER(PLAYER_PED_ID())
				OR IS_PED_RAGDOLL(PLAYER_PED_ID())
				OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
				OR NOT IS_PLAYER_FREE_FOR_AMBIENT_TASK(PLAYER_ID())
//				OR KILL_YOURSELF_NO_ANIM_TERRITORY()
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
					//CLEAR_PED_LAST_WEAPON_DAMAGE(PLAYER_PED_ID())
					IMPOUND_PV_ON_KILL_YOURSELF()
					
					SET_PLAYERS_HEALTH_ZERO_FOR_SUICIDE()

					SEND_KILL_YOURSELF_TICKER()

					SET_KILL_YOURSELF_USED_IN_DM()
					SET_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_DoWastedStrapline)
					NET_PRINT(" ---> PROCESS_KILL_YOURSELF - DONE - NO ANIMS - PLAYER KILLED") NET_NL()
					EXIT
				ENDIF
			ENDIF
			
			//Do Animnated Kill
			//#IF NOT IS_JAPANESE_BUILD
				//IF NOT IS_GERMAN_VERSION()
				//	IF NOT IS_AUSSIE_VERSION()
						IF HAVE_KILL_YOURSELF_ANIMS_LOADED()
							
							//Play Anim
							IF NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
								IF (HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL) OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_PISTOL_MK2))
								AND (GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL) > 0 OR GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_PISTOL_MK2) > 0)
								AND NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_SUPP_02)
								AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
								AND NOT IS_PLAYER_IN_ANY_SHOP()
								AND NOT IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
								AND NOT IS_MP_PASSIVE_MODE_ENABLED()
								AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != GET_HASH_KEY("YachtRm_Bridge") 
								AND NOT KILL_YOURSELF_NO_ANIM_TERRITORY()
									IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, TRUE)
									ELIF  HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_PISTOL_MK2)
										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_PISTOL_MK2, TRUE)
									ENDIF
									
									MPGlobals.KillYourselfData.sAnim = "PISTOL"
									SET_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_PistolUsed)
									MPGlobals.KillYourselfData.fAnimPhaseToRagdoll = 0.365	//0.72
								ELSE
									SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
									MPGlobals.KillYourselfData.sAnim = "PILL"
									MPGlobals.KillYourselfData.fAnimPhaseToRagdoll = 0.536	//67
								ENDIF																										//AF_HOLD_LAST_FRAME|AF_REORIENT_WHEN_FINISHED|AF_EXTRACT_INITIAL_OFFSET|AF_REPOSITION_WHEN_FINISHED
								TASK_PLAY_ANIM(PLAYER_PED_ID(), "MP_SUICIDE", MPGlobals.KillYourselfData.sAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE|AF_EXIT_AFTER_INTERRUPTED|AF_USE_ALTERNATIVE_FP_ANIM)
								SET_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
								NET_PRINT(" ---> PROCESS_KILL_YOURSELF - ANIM PLAYED ") NET_PRINT(MPGlobals.KillYourselfData.sAnim) NET_NL()
							
							//Anim Playing Checks
							ELSE
								IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "MP_SUICIDE", MPGlobals.KillYourselfData.sAnim)
									//Pistol Shot
									IF NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_PistolFired)
										IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("Fire"))
											CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
											//CLEAR_PED_LAST_WEAPON_DAMAGE(PLAYER_PED_ID())
											WEAPON_TYPE PlayerWeapon = GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())
											IF PlayerWeapon = WEAPONTYPE_PISTOL
											OR PlayerWeapon = WEAPONTYPE_DLC_PISTOL_MK2
												SET_PED_SHOOTS_AT_COORD(PLAYER_PED_ID(), <<0, 0, 0>>)
											ENDIF
											SET_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_PistolFired)
											NET_PRINT(" ---> PROCESS_KILL_YOURSELF - biKY_PistolFired SET") NET_NL()
										ENDIF
									ENDIF
									
									//Anim Phase Checks
									IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "MP_SUICIDE", MPGlobals.KillYourselfData.sAnim) > MPGlobals.KillYourselfData.fAnimPhaseToRagdoll
										SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 0, 250, TASK_RELAX, FALSE, FALSE, FALSE)
										CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
										//CLEAR_PED_LAST_WEAPON_DAMAGE(PLAYER_PED_ID())
										IMPOUND_PV_ON_KILL_YOURSELF()
										SET_PLAYERS_HEALTH_ZERO_FOR_SUICIDE()
					
										SEND_KILL_YOURSELF_TICKER()
						
										SET_KILL_YOURSELF_USED_IN_DM()
										SET_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_DoWastedStrapline)
										NET_PRINT(" ---> PROCESS_KILL_YOURSELF - ANIM PHASE > fAnimPhaseToRagdoll - PLAYER KILLED") NET_NL()
										KILL_YOURSELF_CLEANUP()
									ENDIF
								//Anim End
								ELSE
									SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 0, 250, TASK_RELAX, FALSE, FALSE, FALSE)
									CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
									//CLEAR_PED_LAST_WEAPON_DAMAGE(PLAYER_PED_ID())
									IMPOUND_PV_ON_KILL_YOURSELF()
									SET_PLAYERS_HEALTH_ZERO_FOR_SUICIDE()
							
									SEND_KILL_YOURSELF_TICKER()
						
									SET_KILL_YOURSELF_USED_IN_DM()
									SET_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_DoWastedStrapline)
									NET_PRINT(" ---> PROCESS_KILL_YOURSELF - DONE - ANIMS FINISHED - PLAYER KILLED") NET_NL()
									KILL_YOURSELF_CLEANUP()
								ENDIF
							ENDIF
						ENDIF
				//	ENDIF
				//ENDIF
			//#ENDIF
			
		ELSE
			KILL_YOURSELF_CLEANUP()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if the NightVIsion Activate/Deactivate button in the Interaction Menu has been disabled.
FUNC BOOL IS_PIM_NIGHTVISION_BUTTON_DISABLED()
	IF NOT (GET_PLAYER_VISUAL_AID_OVERRIDE() = VISUALAID_DEFAULT)
	AND g_sFMMCVisualAid.bMaintainOverride
		RETURN TRUE
	ENDIF
	
	RETURN MPGlobals.PlayerInteractionData.bDisablePimNvButton
ENDFUNC

//PURPOSE: Disables the NightVision Activate/Deactivate button in the Interaction Menu (PIM)
PROC DISABLE_PIM_NIGHTVISION_BUTTON(BOOL bDisable)
	MPGlobals.PlayerInteractionData.bDisablePimNvButton = bDisable
	PRINTLN(" ---> DISABLE_PIM_NIGHTVISION_BUTTON - bDisablePimNvButton SET TO ", PICK_STRING(bDisable, "TRUE", "FALSE"))
	DEBUG_PRINTCALLSTACK()
ENDPROC

//PURPOSE: Disables the Interaction Menu (PIM)
PROC DISABLE_INTERACTION_MENU()
	IF !MPGlobals.PlayerInteractionData.bDisableInteractionMenu
		MPGlobals.PlayerInteractionData.bDisableInteractionMenu = TRUE
		NET_PRINT_TIME() NET_PRINT(" ---> DISABLE_INTERACTION_MENU - CALLED ") NET_NL()
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC

//PURPOSE: Enables the Interaction Menu (PIM)
PROC ENABLE_INTERACTION_MENU()
	IF MPGlobals.PlayerInteractionData.bDisableInteractionMenu
		MPGlobals.PlayerInteractionData.bDisableInteractionMenu = FALSE
		NET_PRINT_TIME() NET_PRINT(" ---> ENABLE_INTERACTION_MENU - CALLED ") NET_NL()
		DEBUG_PRINTCALLSTACK()
	ENDIF	
ENDPROC

FUNC BOOL IS_INTERACTION_MENU_DISABLED()
	RETURN MPGlobals.PlayerInteractionData.bDisableInteractionMenu
ENDFUNC

//PURPOSE: Return TRUE if the Interaction Menu is currently being displayed
FUNC BOOL IS_INTERACTION_MENU_OPEN()
	IF MPGlobals.PlayerInteractionData.iTargetPlayerInt = -1
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: A one off call that forces the Interaction Menu to close. If you want to disable the Interaction Menu use DISABLE_INTERACTION_MENU and ENABLE_INTERACTION_MENU.
PROC KILL_INTERACTION_MENU()
	IF IS_INTERACTION_MENU_OPEN()
		MPGlobals.PlayerInteractionData.bKillInteractionMenu = TRUE
		NET_PRINT_TIME() NET_PRINT(" ---> KILL_INTERACTION_MENU - CALLED ") NET_NL()
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PASSIVE_MODE_CUTSCENE_RUN()
	IF NOT HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(PLAYER_ID())
		PRINTLN("[dsw] SHOULD_PASSIVE_MODE_CUTSCENE_RUN FALSE - Haven't done tutorial race / mission ")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		PRINTLN("[dsw] SHOULD_PASSIVE_MODE_CUTSCENE_RUN FALSE - player on a mission ")
		RETURN FALSE
	ENDIF
	
	IF HAS_PASSIVE_MODE_CUTSCENE_BEEN_DONE()
		PRINTLN("[dsw] SHOULD_PASSIVE_MODE_CUTSCENE_RUN FALSE - Already done cutscene")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_DOING_SIMEON_MOCAP()
		PRINTLN("[dsw] SHOULD_PASSIVE_MODE_CUTSCENE_RUN FALSE - Running simeon mocap")
		RETURN FALSE
	ENDIF
	
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		PRINTLN("[dsw] SHOULD_PASSIVE_MODE_CUTSCENE_RUN FALSE - post mission cleanup running")
		RETURN FALSE
	ENDIF
	
	IF IS_COMMERCE_STORE_OPEN()
		PRINTLN("[dsw] SHOULD_PASSIVE_MODE_CUTSCENE_RUN FALSE - IS_COMMERCE_STORE_OPEN")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_MP_AMBIENT_SCRIPT(PLAYER_ID())
		PRINTLN("[dsw] SHOULD_PASSIVE_MODE_CUTSCENE_RUN FALSE - IS_PLAYER_ON_ANY_MP_AMBIENT_SCRIPT")
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
		PRINTLN("[dsw] SHOULD_PASSIVE_MODE_CUTSCENE_RUN FALSE - FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

//PURPOSE: Store the player's look when they get a Wanted Level of 2 or above.
PROC STORE_DETAILS_FOR_ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK()
	INT iWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	IF NOT IS_BIT_SET(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_StoredLook)
		IF iWantedLevel >= 2
			MPGlobalsAmbience.AWLChangedLookData.iBitSet = 0
			//Hat or Helmet
			IF NOT IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_HEAD_NONE)
				//IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
				//PRINTLN(" >>> STORE_DETAILS_FOR_ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK - GET_PED_COMP_ITEM_CURRENT_MP = ", GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
				IF IS_ITEM_A_HAT(GET_PLAYER_MODEL(), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
					SET_BIT(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WearingHat)
					PRINTLN(" >>> STORE_DETAILS_FOR_ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK - biAWL_WearingHat")
				ELSE
					SET_BIT(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WearingHelmet)
					PRINTLN(" >>> STORE_DETAILS_FOR_ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK - biAWL_WearingHelmet")
				ENDIF
			ENDIF
			//Glasses
			IF NOT IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_EYES_NONE)
				SET_BIT(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WearingGlasses)
				PRINTLN(" >>> STORE_DETAILS_FOR_ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK - biAWL_WearingGlasses")
			ENDIF
			//Mask
			IF NOT IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0))
				SET_BIT(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WearingMask)
				PRINTLN(" >>> STORE_DETAILS_FOR_ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK - biAWL_WearingMask")
			ENDIF
			
			SET_BIT(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_StoredLook)
			PRINTLN(" >>> STORE_DETAILS_FOR_ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK - Done")
		ENDIF
	ELSE
		IF iWantedLevel <= 0
			MPGlobalsAmbience.AWLChangedLookData.iBitSet = 0
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if the Player's look has changed since it was stored.
FUNC BOOL HAS_LOOK_CHANGED()
	//Hat or Helmet
	IF IS_BIT_SET(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WearingHat)
	OR IS_BIT_SET(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WearingHelmet)
		IF IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_HEAD_NONE)
			PRINTLN(" >>> ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK - HAS_LOOK_CHANGED - NO HAT OR HELMET")
			RETURN TRUE
		ELSE
			//IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
			IF IS_ITEM_A_HAT(GET_PLAYER_MODEL(), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
				IF IS_BIT_SET(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WearingHelmet)
					PRINTLN(" >>> ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK - HAS_LOOK_CHANGED - HAT ORIGINALLY HELMET")
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_BIT_SET(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WearingHat)
					PRINTLN(" >>> ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK - HAS_LOOK_CHANGED - HELMET ORIGINALLY HAT")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_HEAD_NONE)
			RETURN TRUE
		ENDIF
	ENDIF
		
	//Glasses
	IF IS_BIT_SET(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WearingGlasses)
		IF IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_EYES_NONE)
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_EYES_NONE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Mask
	IF IS_BIT_SET(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WearingMask)
		IF IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0))
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0))
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

//PURPOSE: Changes the player's Wanted Level if this is the first time they have changed their look since getting a Wanted Level of 2 or above.
PROC ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK()
	IF NOT IS_BIT_SET(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WantedLevelPutBackUp)
		INT iWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
		IF NOT IS_BIT_SET(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WantedLevelLowered)
			IF iWantedLevel >= 2
				IF ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID())
					IF HAS_LOOK_CHANGED()
						IF NOT IS_PLAYER_IN_VEHICLE_WITH_ANOTHER_PLAYER(PLAYER_ID())
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), (iWantedLevel-1))
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							FORCE_START_HIDDEN_EVASION(PLAYER_ID())
							SET_BIT(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WantedLevelLowered)
							PRINTLN(" >>> ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK - Lowered Wanted Level By 1 Star")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF iWantedLevel >= 1
			AND iWantedLevel < 5
				IF NOT HAS_LOOK_CHANGED()
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), (iWantedLevel+1))
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					CLEAR_BIT(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WantedLevelLowered)
					SET_BIT(MPGlobalsAmbience.AWLChangedLookData.iBitSet, biAWL_WantedLevelPutBackUp)
					PRINTLN(" >>> ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK - Upped Wanted Level By 1 Star")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE: 
///    Handles te Lester Contact ability where the cops turn a blind eye for a short period of time.
PROC MAINTAIN_LESTER_DISABLE_COPS()
	IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Activated)
	
		INT iTimerDuration = MAX_TIME_COPS_TURN_BLIND_EYE
		TEXT_LABEL_15 tlTimerText = "LEST_NCOPS"
		BOOL bGangModeActive
		
		//-- For 2789315, IS_PLAYER_GANG_BRIBING_POLICE shouldn't return false until it has returned true at least once, if biNoCops_Gang is set
		IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Gang)
			IF NOT IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_GangVipBribing)
				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
					bGangModeActive = TRUE
					IF IS_PLAYER_GANG_BRIBING_POLICE()
						SET_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_GangVipBribing)
						RESET_NET_TIMER(MPGlobalsAmbience.timeCopsDisabled)
						START_NET_TIMER(MPGlobalsAmbience.timeCopsDisabled)
						PRINTLN("[dsw] [MAINTAIN_LESTER_DISABLE_COPS] Set biNoCops_GangVipBribing")
					ELSE
						PRINTLN("[dsw] [MAINTAIN_LESTER_DISABLE_COPS] Waiting for IS_PLAYER_GANG_BRIBING_POLICE")
					ENDIF
				ENDIF
			ELSE
				bGangModeActive = IS_PLAYER_GANG_BRIBING_POLICE()
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Gang)
			iTimerDuration = g_sMPTunables.iGB_BRIBE_AUTHORITIES_DURATION 
			tlTimerText = "GB_PIM_BRIBE"
			PRINTLN("[dsw] [MAINTAIN_LESTER_DISABLE_COPS] Setting timer label to: GB_PIM_BRIBE")
		ENDIF
	
		SWITCH MPGlobalsAmbience.iLesterDisableCopsProg
			CASE 0
				MPGlobalsAmbience.iMyMaxWantedLevel = GET_MAX_WANTED_LEVEL()
				SET_MAX_WANTED_LEVEL(0)
				IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Personal)
				OR IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Gang)
				AND NOT IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_GangJip)
					RESET_NET_TIMER(MPGlobalsAmbience.timeCopsDisabled)
					PRINTLN("[dsw] [MAINTAIN_LESTER_DISABLE_COPS] RESET_NET_TIMER")
				ENDIF
				START_NET_TIMER(MPGlobalsAmbience.timeCopsDisabled)
				MPGlobalsAmbience.iLesterDisableCopsProg++
				PRINTLN("[dsw] [MAINTAIN_LESTER_DISABLE_COPS] Activated! MPGlobalsAmbience.iMyMaxWantedLevel = ", MPGlobalsAmbience.iMyMaxWantedLevel)
				PRINTLN("[dsw] [MAINTAIN_LESTER_DISABLE_COPS] MPGlobalsAmbience.iLesterDisableCopsProg = ", MPGlobalsAmbience.iLesterDisableCopsProg)
			BREAK
			
			CASE 1
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						IF NOT IS_PLAYER_IN_CORONA()
							IF NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
								IF IS_PLAYER_IN_NO_COPS_INFLUENCE()
								OR bGangModeActive
									//-- From Ryan's Brucie Pickup 
									
									INT iTimerDifference
									iTimerDifference = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MPGlobalsAmbience.timeCopsDisabled)
									
									IF (iTimerDuration-iTimerDifference) >= 0
										IF NOT IS_PHONE_ONSCREEN()
					      					DRAW_GENERIC_TIMER((iTimerDuration-iTimerDifference), tlTimerText, 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_TOP)
										ENDIF
										IF GET_MAX_WANTED_LEVEL() != 0
											IF MPGlobalsAmbience.iMyMaxWantedLevel = 0
												MPGlobalsAmbience.iMyMaxWantedLevel = GET_MAX_WANTED_LEVEL()
												PRINTLN("[dsw] [MAINTAIN_LESTER_DISABLE_COPS] MPGlobalsAmbience.iMyMaxWantedLevel was 0, now ", MPGlobalsAmbience.iMyMaxWantedLevel)
											ENDIF
											CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
											SET_MAX_WANTED_LEVEL(0)
											PRINTLN("[dsw] [MAINTAIN_LESTER_DISABLE_COPS] SET_MAX_WANTED_LEVEL(0) AS GET_MAX_WANTED_LEVEL() != 0")
										ENDIF
										
										IF iTimerDifference >= 5000
											IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Gang)
												IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_GangJip)
													IF NOT bGangModeActive
														IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , MPGlobalsAmbience.timeCopsDisabledJipGang) >= 5000)
															PRINTLN("[GB][AMEC] - MAINTAIN_LESTER_DISABLE_COPS - Player did not JIP to gang in 5 seconds, cancel no cops.")
															MPGlobalsAmbience.iLesterDisableCopsProg++
														ENDIF
													ELSE
														CLEAR_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_GangJip)
													ENDIF
												ELSE
													IF NOT bGangModeActive
														PRINTLN("[GB][AMEC] - MAINTAIN_LESTER_DISABLE_COPS - Player has left gang during Bribe Auth, cancel no cops. iTimerDifference: ", iTimerDifference)
														MPGlobalsAmbience.iLesterDisableCopsProg++
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
									ELSE
										IF NOT IS_PHONE_ONSCREEN()
											// Make sure no timer value below 0 is ever shown
					     					DRAW_GENERIC_TIMER(0, tlTimerText, 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_TOP)
										ENDIF
										
										MPGlobalsAmbience.iLesterDisableCopsProg++
										PRINTLN("[dsw] [MAINTAIN_LESTER_DISABLE_COPS] Time expired MPGlobalsAmbience.iLesterDisableCopsProg = ", MPGlobalsAmbience.iLesterDisableCopsProg)
									ENDIF
								ELSE
									MPGlobalsAmbience.iLesterDisableCopsProg++
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				PRINTLN("[dsw] [MAINTAIN_LESTER_DISABLE_COPS] Resetting. ")
				RESET_LESTER_NO_COPS()
			BREAK
			
		ENDSWITCH
		
	ENDIF
ENDPROC


//PURPOSE: Returns TRUE if it is okay to send a Contact Text Message
FUNC BOOL IS_OKAY_TO_SEND_CONTACT_TEXT_MESSAGE()
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		RETURN FALSE
	ENDIF
	
	IF IS_CELLPHONE_DISABLED()
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		RETURN FALSE
	ENDIF
	
	/*IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF*/
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF GET_IS_LEADERBOARD_CAM_ACTIVE_OR_INTERPING() 
		RETURN FALSE
	ENDIF
	
	IF g_bBrowserVisible 	//(Checks if the internet active)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_SESSION_LAUNCHING()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Blocks attacks, weapon changing, etc.
PROC BLOCK_AGGRESSIVE_ACTIONS()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PICKUP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
	//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_PLAYER_THROW_GRENADE_WHILE_USING_GUN()
	
	IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) != WEAPONTYPE_UNARMED
	AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
	AND NOT IS_LOCAL_PLAYER_AN_ANIMAL()
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
	ENDIF
ENDPROC
//PURPOSE: Turns player attack off if in a remote Passive Mode players Vehicle.

FUNC BOOL AM_I_IN_A_VEHICLE_WITH_A_PASSIVE_PLAYER(VEHICLE_INDEX tempveh)
	INT iTemp
	PLAYER_INDEX playerInCar
	PED_INDEX pedInCar 
	FOR iTemp = -1 TO GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempveh)-1
		pedInCar = GET_PED_IN_VEHICLE_SEAT(tempveh,INT_TO_ENUM(VEHICLE_SEAT,iTemp))
		IF DOES_ENTITY_EXIST(pedInCar)
		AND IS_PED_A_PLAYER(pedInCar)
			playerInCar = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInCar)
			IF playerInCar != PLAYER_ID()
			AND NETWORK_IS_PLAYER_ACTIVE(playerInCar)
				IF GlobalplayerBD_FM_3[NATIVE_TO_INT(playerInCar)].bInPassive
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_IN_PASSIVE_MODE_VEH_CHECK()
	VEHICLE_INDEX tempveh
	BOOL bExtraCheck = TRUE
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			tempveh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(tempveh)
				IF IS_ENTITY_A_GHOST(tempveh)
						IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_KILL_LIST
							bExtraCheck = FALSE
						ENDIF
					
						IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CHAL_FIVESTAR
							bExtraCheck = FALSE
						ENDIF
					
					IF NETWORK_IS_ACTIVITY_SESSION()
					AND (IS_THIS_ROCKSTAR_MISSION_NEW_VS_GRASS_GREENER(g_FMMC_STRUCT.iAdversaryModeType)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType))
						bExtraCheck = FALSE
					ENDIF
									
					IF bExtraCheck
						IF (IS_MP_PASSIVE_MODE_ENABLED()
						//And we're not in temp passive
						OR IS_TEMP_PASSIVE_MODE_ENABLED()
						OR AM_I_IN_A_VEHICLE_WITH_A_PASSIVE_PLAYER(tempveh))
							BLOCK_AGGRESSIVE_ACTIONS()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls disbaling player shooting/melee if Passive Mode is enabled
PROC MAINTAIN_PASSIVE_MODE_EFFECTS()
	IF IS_MP_PASSIVE_MODE_ENABLED()
	//OR NOT HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.iPassiveModeDisableDelayTimer, MPGlobalsAmbience.iPassiveModeDisableDelay)
		
		//FAIL SAFE - MAKES SURE WE ARE IN GHOST MODE
		IF NOT IS_ENTITY_A_GHOST(PLAYER_PED_ID())
			IF g_MultiplayerSettings.g_bTempPassiveModeSetting = FALSE
			OR g_MultiplayerSettings.g_bTempPassiveModeUseGhost = TRUE
				SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), FALSE)
				IF NOT NETWORK_IS_ACTIVITY_SESSION()
				OR (NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_GRASS_GREENER(g_FMMC_STRUCT.iAdversaryModeType)
					AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType))
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				ENDIF
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, TRUE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableMelee, TRUE)
				SET_PLAYER_VEHICLE_DEFENSE_MODIFIER(PLAYER_ID(), 0.5)
				
				SET_LOCAL_PLAYER_AS_GHOST(TRUE)
				NET_PRINT(" MAINTAIN_PASSIVE_MODE_EFFECTS - PASSIVE MODE - SET_LOCAL_PLAYER_AS_GHOST = TRUE") NET_NL()
			ENDIF
		ENDIF
		
		//Use the correct timer
		INT iPassiveModeDisableDelay = g_sMPTunables.iVC_PASSIVE_TIME_AFTER_DISABLE
//		IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
//		OR g_FM_EVENT_VARS.iFmAmbientEventCountDownType != FMMC_TYPE_INVALID
//		OR GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
//			iPassiveModeDisableDelay = g_sMPTunables.iFmEventPassiveCoolDown
//		ENDIF
		
		//Do Turn Off Timer
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
		OR (NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_GRASS_GREENER(g_FMMC_STRUCT.iAdversaryModeType)
			AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType))
			IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iPassiveModeDisableDelayTimer)
				IF NOT HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.iPassiveModeDisableDelayTimer, iPassiveModeDisableDelay)
					DRAW_GENERIC_TIMER((iPassiveModeDisableDelay-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MPGlobalsAmbience.iPassiveModeDisableDelayTimer)), "PIM_PASTIM", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_TOP)
				ELSE
					IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
						RESET_NET_TIMER(MPGlobalsAmbience.iPassiveModeDisableDelayTimer)
						g_MultiplayerSettings.g_bPassiveModeSetting = FALSE
						GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].bInPassive = FALSE
						SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
						PRINTLN(" MAINTAIN_PASSIVE_MODE_EFFECTS - COOLDOWN FINISHED - PASSIVE MODE DISABLED")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
			OR (NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_GRASS_GREENER(g_FMMC_STRUCT.iAdversaryModeType)
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType))
			
				BLOCK_AGGRESSIVE_ACTIONS()
			ENDIF
			
			/*IF MPGlobalsAmbience.bPassiveModeInVeh = TRUE
				IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.iPassiveModeDisableDelayTimer, MPGlobalsAmbience.iPassiveModeDisableDelay)
					SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), FALSE)
					NETWORK_SET_PLAYER_IS_PASSIVE(TRUE)
					NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
					SET_LOCAL_PLAYER_AS_GHOST(FALSE)
					MPGlobalsAmbience.bPassiveModeInVeh = FALSE
					RESET_NET_TIMER(MPGlobalsAmbience.iPassiveModeDisableDelayTimer)
					NET_PRINT(" MAINTAIN_PASSIVE_MODE_EFFECTS - bPassiveModeInVeh = FALSE - NORMAL") NET_NL()
				ELSE
					MPGlobalsAmbience.bPassiveModeInVeh = FALSE
					NET_PRINT(" MAINTAIN_PASSIVE_MODE_EFFECTS - bPassiveModeInVeh = FALSE - DURING COUNTDOWN") NET_NL()
				ENDIF
			ENDIF*/
		/*ELSE
			IF MPGlobalsAmbience.bPassiveModeInVeh = FALSE
				SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), TRUE)
				NETWORK_SET_PLAYER_IS_PASSIVE(FALSE)
				NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)
				SET_LOCAL_PLAYER_AS_GHOST(TRUE)
				MPGlobalsAmbience.bPassiveModeInVeh = TRUE
				NET_PRINT(" MAINTAIN_PASSIVE_MODE_EFFECTS - bPassiveModeInVeh = TRUE") NET_NL()
			ENDIF
		ENDIF*/		
	ELSE
		MAINTAIN_IN_PASSIVE_MODE_VEH_CHECK()
	ENDIF
ENDPROC

//Checks to see if the host of a script is a spectator
FUNC BOOL IS_HOST_A_SPECTATOR()
	PLAYER_INDEX piPlayer
	PARTICIPANT_INDEX piPart = NETWORK_GET_HOST_OF_THIS_SCRIPT()
	IF NATIVE_TO_INT(piPart) != -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
			IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
				IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
				OR IS_PLAYER_SCTV(piPlayer)
				OR IS_PLAYER_SPECTATING(piPlayer)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//Deal with the forcing the host on to a player who is not a spectator. 
PROC MAINTAIN_SPEC_NOT_HOST(INT &iManageSpechost, INT &iDesiredHost)

	//If I nost no
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		//PRINTLN("[RCC MISSION] MAINTAIN_SPEC_NOT_HOST - NETWORK_IS_HOST_OF_THIS_SCRIPT")
		iDesiredHost = -1
		iManageSpechost = 0
		EXIT
	ENDIF
	
	//If it's not a spec get out
	IF NOT IS_HOST_A_SPECTATOR()
		//PRINTLN("[RCC MISSION] MAINTAIN_SPEC_NOT_HOST - IS_HOST_A_SPECTATOR")
		iDesiredHost = -1
		iManageSpechost = 0
		EXIT
	ENDIF
	
	//If I'm a spec no
	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
	OR IS_PLAYER_SCTV(PLAYER_ID())
	OR IS_PLAYER_SPECTATING(PLAYER_ID())
		//PRINTLN("[RCC MISSION] MAINTAIN_SPEC_NOT_HOST - DID_PLAYER_JOIN_MISSION_AS_SPECTATOR")
		iDesiredHost = -1
		iManageSpechost = 0
		EXIT
	ENDIF
	
	//If we've not got a host
	IF iDesiredHost = -1
		//If the player is active
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iManageSpecHost))
			PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iManageSpechost))
			//and that player is not a spectator
			IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
			AND NOT IS_PLAYER_SCTV(piPlayer)
			AND NOT IS_PLAYER_SPECTATING(piPlayer)
				//then set them as the desired host
				PRINTLN("MAINTAIN_SPEC_NOT_HOST - iDesiredHost = ", iManageSpechost)
				iDesiredHost = iManageSpechost
			ENDIF
		ENDIF
		//increment
		iManageSpechost++	
		//Cap
		IF iManageSpechost = NUM_NETWORK_PLAYERS
			PRINTLN("MAINTAIN_SPEC_NOT_HOST - iManageSpechost = NUM_NETWORK_PLAYERS")
			iManageSpechost = 0
			//Reset
			iDesiredHost = -1
		ENDIF
	//If I'm the desired host
	ELIF iDesiredHost = NATIVE_TO_INT(PLAYER_ID())
		NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
		PRINTLN("MAINTAIN_SPEC_NOT_HOST - NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT")
	ENDIF
	
ENDPROC

//Removes any Sp scripts that might have been loaded.
PROC SET_ALL_SP_SCRIPT_AS_NO_LONGER_NEEDED_WHEN_ENTERING_FM()
	PRINTLN("[TS] - SET_ALL_SP_SCRIPT_AS_NO_LONGER_NEEDED_WHEN_ENTERING_FM")
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_Mime"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_Mime"))						ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_Jesse"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_Jesse"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_AndyMoon"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_AndyMoon"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_Baygor"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_Baygor"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_Superhero"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_Superhero"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_PamelaDrake"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_PamelaDrake"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_BillBinder"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_BillBinder"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_Clinton"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_Clinton"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_Jane"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_Jane"))						ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_Jerome"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_Jerome"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_Zombie"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_Zombie"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_Griff"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_Griff"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_Mani"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_Mani"))						ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("gpb_Tonya"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("gpb_Tonya"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_SecurityVan"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_SecurityVan"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_paparazzi"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_paparazzi"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_atmrobbery"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_atmrobbery"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_rescuehostage"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_rescuehostage"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_arrests"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_arrests"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_border"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_border"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_burials"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_burials"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_muggings"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_muggings"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_domestic"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_domestic"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_drunkdriver"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_drunkdriver"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_gang_Intimidation "))		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_gang_Intimidation "))		ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_getaway_driver"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_getaway_driver"))			ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_shoprobbery"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_shoprobbery"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_lured"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_lured"))						ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_carTheft"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_carTheft"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_prisonerlift"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_prisonerlift"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_accident"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_accident"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_bus_tours"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_bus_tours"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_prisonvanbreak"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_prisonvanbreak"))			ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_DealGoneWrong"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_DealGoneWrong"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_hitch_lift"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_hitch_lift"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_stag_do"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_stag_do"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_mountdance"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_mountdance"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_Duel"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_Duel"))						ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_SeaPlane"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_SeaPlane"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_monkey"))					SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_monkey"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_chasethieves"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_chasethieves"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_cultshootout"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_cultshootout"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_crashrescue"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_crashrescue"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_bikethief"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_bikethief"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_Homeland_Security"))		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_Homeland_Security"))			ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_abandonedcar"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_abandonedcar"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_snatched"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_snatched"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_gangfight"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_gangfight"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("re_yetarian"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("re_yetarian"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_BasejumpHeli"))		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_BasejumpHeli"))		ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_BasejumpPack"))		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_BasejumpPack"))		ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_golf"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_golf"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Hunting_Ambient"))	SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Hunting_Ambient"))		ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_OffroadRacing"))		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_OffroadRacing"))		ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Racing"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Racing"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_rampage"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_rampage"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_pilotschool"))		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_pilotschool"))			ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_range"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_range"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_stunts"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_stunts"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_tennis"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_tennis"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Triathlon"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Triathlon"))			ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Yoga"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Yoga"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_CarWash"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_CarWash"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Abigail"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Abigail"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Barry"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Barry"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Dreyfuss"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Dreyfuss"))			ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Epsilon"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Epsilon"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Extreme"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Extreme"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Fanatic"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Fanatic"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Hao"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Hao"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Hunting"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Hunting"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Josh"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Josh"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Maude"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Maude"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_MrsPhilips"))		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_MrsPhilips"))			ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Minute"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Minute"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Nigel"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Nigel"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Omega"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Omega"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Paparazzo"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Paparazzo"))			ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_TheLastOne"))		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_TheLastOne"))			ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("launcher_Tonya"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("launcher_Tonya"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("chop"))						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("chop"))							ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("murderMystery"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("murderMystery"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("ShopRobberies"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("ShopRobberies"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("altruist_cult"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("altruist_cult"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("flyUnderBridges"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("flyUnderBridges"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("savegame_bed"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("savegame_bed"))					ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("fairgroundHub"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("fairgroundHub"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("underwaterPickups"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("underwaterPickups"))			ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("wp_PartyBoomBox"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("wp_PartyBoomBox"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("UFO"))						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("UFO"))							ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("ob_foundry_cauldron"))		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("ob_foundry_cauldron"))			ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("ob_sofa_michael"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("ob_sofa_michael"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("ob_sofa_franklin"))			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("ob_sofa_franklin"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("ob_franklin_tv"))				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("ob_franklin_tv"))				ENDIF
	IF DOES_SCRIPT_WITH_NAME_HASH_EXIST(HASH("ob_tv"))						SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("ob_tv"))						ENDIF
ENDPROC

//PURPOSE: Gets the players Maximum armour value based on their Rank which determines the level of armour unlocked
FUNC INT GET_PLAYER_MAX_ARMOUR_FROM_RANK()
	IF GET_PLAYER_RANK(PLAYER_ID()) >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_SUPERHEAVYARMOUR)
		RETURN GET_PLAYER_MAX_ARMOUR(PLAYER_ID())
	ELIF GET_PLAYER_RANK(PLAYER_ID()) >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_HEAVYARMOUR)
		RETURN ROUND((SAFELY_DIVIDE_INT_RETURN_AS_FLOAT(GET_PLAYER_MAX_ARMOUR(PLAYER_ID()), 100)*80))
	ELIF GET_PLAYER_RANK(PLAYER_ID()) >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_STANDARDARMOUR)
		RETURN ROUND((SAFELY_DIVIDE_INT_RETURN_AS_FLOAT(GET_PLAYER_MAX_ARMOUR(PLAYER_ID()), 100)*60))
	ELIF GET_PLAYER_RANK(PLAYER_ID()) >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_LIGHTARMOUR)
		RETURN ROUND((SAFELY_DIVIDE_INT_RETURN_AS_FLOAT(GET_PLAYER_MAX_ARMOUR(PLAYER_ID()), 100)*40))
	ENDIF
	
	RETURN ROUND((SAFELY_DIVIDE_INT_RETURN_AS_FLOAT(GET_PLAYER_MAX_ARMOUR(PLAYER_ID()), 100)*20))
ENDFUNC

//PURPOSE: Sets whether a player is able to collect Armour pickups based on their current armour level.
PROC MAINTAIN_CAN_COLLECT_ARMOUR()
	IF GET_PED_ARMOUR(PLAYER_PED_ID()) >= GET_PLAYER_MAX_ARMOUR_FROM_RANK()
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_COLLECTING_ARMOUR)
			SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), PICKUP_ARMOUR_STANDARD, FALSE)
			SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_COLLECTING_ARMOUR)
			PRINTLN("MAINTAIN_CAN_COLLECT_ARMOUR - iABI_BLOCK_COLLECTING_ARMOUR - SET")
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_COLLECTING_ARMOUR)
			SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), PICKUP_ARMOUR_STANDARD, TRUE)
			CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_COLLECTING_ARMOUR)
			PRINTLN("MAINTAIN_CAN_COLLECT_ARMOUR - iABI_BLOCK_COLLECTING_ARMOUR - CLEARED")
		ENDIF
	ENDIF
ENDPROC
//
//#IF FEATURE_NEW_AMBIENT_EVENTS
//FUNC BOOL IS_VEHICLE_BEING_USED_FOR_TIME_TRIAL(VEHICLE_INDEX veh)
//
//	IF DECOR_IS_REGISTERED_AS_TYPE("UsingForTimeTrial", DECOR_TYPE_BOOL)
//		IF DECOR_EXIST_ON(veh, "UsingForTimeTrial") 
//			RETURN DECOR_GET_BOOL(veh,"UsingForTimeTrial")
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//	
//ENDFUNC
//#ENDIF
//




PROC SETUP_PLAYER_FROM_STATS()

	
	INT iSlot = GET_ACTIVE_CHARACTER_SLOT()
	BOOL isMale 
	IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, iSlot) = 0
		isMale = TRUE
	ELSE
		isMale = FALSE
	ENDIF
	
	//GIVE_PED_STORED_HAIR(aPed, iSlot) // we dont need to set the stored hair here as we will apply it later on...
	// Update stored hairdo...
	SET_CURRENT_MP_SLOT_FOR_CLOTHES(iSlot)

	PATCH_CG_HAIR_IN_NG(iSlot)
	UPDATE_LAST_GEN_CHARACTER_HAIR(iSlot, isMale)
	
	BOOL bNewCharacterCreation = (GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NG_VERSION, iSlot) >= 1)
	
	PED_INDEX aPed = PLAYER_PED_ID()
	

	SET_MP_PLAYERS_START_CLOTHES(FALSE, TSHIRT_STATES_INGAME_USE_STORED,aPed, iSlot )
	GIVE_AND_EQUIP_PLAYERS_PARACHUTE()
	
	// Run the conversion of teeth to props
	UPDATE_PROPS_AND_TEETH_FOR_CG_TO_NG(aPed, iSlot)
	
	IF NOT bNewCharacterCreation
		APPLY_CHAR_OVERLAYS(PLAYER_PED_ID(), iSlot)
	ELSE
		APPLY_CHARACTER_CREATOR_FEATURES_FROM_STATS(aPed, iSlot, FALSE)
	ENDIF
	
	// STORE HEAD BLEND
	scrPedHeadBlendData headBlendData
	#IF NOT IS_NEXTGEN_BUILD
	BOOL bHASGRANDPARENT
	#ENDIF
	GET_PED_HEAD_BLEND_DATA(PLAYER_PED_ID(), headBlendData)

	
	PRINTLN("[KR STORE DEFAULT HEAD BLEND - EXISTING CHARACTER slot = ", iSlot, "]")
	PRINTLN("[HEAD BLEND] - GET_PED_HEAD_BLEND_DATA m_head0 		= ", headBlendData.m_head0)
	PRINTLN("[HEAD BLEND] - GET_PED_HEAD_BLEND_DATA m_head1 		= ", headBlendData.m_head1)
	PRINTLN("[HEAD BLEND] - GET_PED_HEAD_BLEND_DATA m_head2 		= ", headBlendData.m_head2)
	PRINTLN("[HEAD BLEND] - GET_PED_HEAD_BLEND_DATA m_tex0 		= ", headBlendData.m_tex0)
	PRINTLN("[HEAD BLEND] - GET_PED_HEAD_BLEND_DATA m_tex1 		= ", headBlendData.m_tex1)
	PRINTLN("[HEAD BLEND] - GET_PED_HEAD_BLEND_DATA m_tex2 		= ", headBlendData.m_tex2)
	PRINTLN("[HEAD BLEND] - GET_PED_HEAD_BLEND_DATA m_headBlend 	= ", headBlendData.m_headBlend)
	PRINTLN("[HEAD BLEND] - GET_PED_HEAD_BLEND_DATA m_texBlend 	= ", headBlendData.m_texBlend)
	PRINTLN("[HEAD BLEND] - GET_PED_HEAD_BLEND_DATA m_varBlend 	= ", headBlendData.m_varBlend)
	PRINTLN("[HEAD BLEND] - GET_PED_HEAD_BLEND_DATA m_isParent 	= ", headBlendData.m_isParent)
	#IF NOT IS_NEXTGEN_BUILD
	PRINTLN("[HEAD BLEND] - GET_PED_HEAD_BLEND_HAS_GRANDPARENTS_NG 	= ", bHASGRANDPARENT)
	#ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_HEAD0, headBlendData.m_head0, iSlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_HEAD1, headBlendData.m_head1, iSlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_HEAD2, headBlendData.m_head2, iSlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_TEX0, headBlendData.m_tex0, iSlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_TEX1, headBlendData.m_tex1, iSlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_TEX2, headBlendData.m_tex2, iSlot)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MESH_HEADBLEND, headBlendData.m_headBlend, iSlot)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MESH_TEXBLEND, headBlendData.m_texBlend, iSlot)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MESH_VARBLEND, headBlendData.m_varBlend, iSlot)
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_MESH_ISPARENT, headBlendData.m_isParent, iSlot)
	
	#IF NOT IS_NEXTGEN_BUILD
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_MESH_HASGRANDPARENT, bHASGRANDPARENT, iSlot)
	#ENDIF
	
	
	INT iPrimaryTint = GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT, iSlot)
	INT iSecondaryTint = GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT, iSlot)
	INT SA_Hairdo = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, iSlot)
	
	PRINTLN("[JA@FACES] RUN_PED_MENU - Apply the hair for the menu ped: iPrimaryTint = ", iPrimaryTint, ", iSecondaryTint = ", iSecondaryTint, " Before CHAR_FM_STORED_HAIRDO_SA = ", SA_Hairdo, " ")

	
	PED_COMP_NAME_ENUM theHair = INT_TO_ENUM(PED_COMP_NAME_ENUM, SA_Hairdo)	
	SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_HAIR, theHair, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iPrimaryTint, iSecondaryTint, iSlot)
	
	IF NOT IS_PC_VERSION()
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, iSlot), iSlot)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, iSlot), iSlot)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT, TRUE, iSlot)
		PRINTLN("[JA@FACES] RUN_PED_MENU - CONSOLE MP_STAT_CHAR_FM_STORED_HAIRDO_SA =  ", SA_Hairdo, " " ) 
	ELSE
		IF GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, iSlot) !=0	// fix for missing hair bug 2247698 KW 10/3/15
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, iSlot), iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, iSlot), iSlot)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT, TRUE, iSlot)
		ENDIF
		
		PRINTLN("[JA@FACES] RUN_PED_MENU - PC VERSION MP_STAT_CHAR_FM_STORED_HAIRDO_SA =  ", SA_Hairdo, " " ) 
	ENDIF
	SA_Hairdo = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, iSlot)
	
	PRINTLN("[JA@FACES] RUN_PED_MENU - Apply the hair for the menu ped: After CHAR_FM_STORED_HAIRDO_SA = ", SA_Hairdo, " " ) 
	

ENDPROC


/// PURPOSE: Populates the tattoo broadcast data with our values
PROC SET_PLAYER_TATTOO_DATA()
	
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	
	IF iMyGBD != -1
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[0] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_0)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[1] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_1)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[2] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_2)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[3] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_3)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[4] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_4)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[5] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_5)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[6] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_6)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[7] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_7)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[8] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_8)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[9] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_9)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[10] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_10)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[11] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_11)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[12] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_12)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[13] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_13)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[14] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_14)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[15] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_15)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[16] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_16)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[17] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_17)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[18] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_18)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[19] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_19)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[20] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_20)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[21] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_21)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[22] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_22)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[23] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_23)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[24] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_24)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[25] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_25)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[26] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_26)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[27] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_27)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[28] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_28)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[29] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_29)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[30] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_30)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[31] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_31)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[32] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_32)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[33] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_33)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[34] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_34)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[35] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_35)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[36] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_36)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[37] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_37)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[38] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_38)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[39] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_39)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[40] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_40)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[41] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_41)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[42] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_42)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[43] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_43)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[44] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_44)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[45] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_45)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[46] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_46)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[47] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_47)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[48] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_48)
		GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[49] = GET_MP_INT_CHARACTER_STAT(MP_STAT_TATTOO_FM_CURRENT_49)
		#IF IS_DEBUG_BUILD
		INT i
		PRINTLN("SET_PLAYER_TATTOO_DATA - Details follow:............")
		FOR i = 0 TO NUMBER_OF_CURRENT_TATTOO_STATS-1 STEP 1
			PRINTLN("SET_PLAYER_TATTOO_DATA - GlobalplayerBD_Tattoos[", iMyGBD, "].iPlayerTattooData[", i, "] = ", GlobalplayerBD_Tattoos[iMyGBD].iPlayerTattooData[i])
		ENDFOR		
		#ENDIF
	ENDIF
ENDPROC


PROC CHANGE_PLAYER_PED_TO_BIGFOOT_FLAGS_ONLY()

	iMaxHealthBeforebigfoot = GET_PED_MAX_HEALTH(PLAYER_PED_ID())

	PRINTLN("[MORPHMODEL] CHANGE_PLAYER_PED_TO_BIGFOOT_FLAGS_ONLY - iMaxHealthBeforebigfoot = ", iMaxHealthBeforebigfoot, " ")

	//Fix for url:bugstar:2412778 - Hunt the Beast - Local player had a parachute on before becoming the Beast, which remained on and clipping through Beast clothes.
	CLEAN_UP_PLAYER_PED_PARACHUTE()	
	
	//Fix for url:bugstar:2410944 - Player was still wearing their Bulletproof Helmet after turning into The Beast
	CLEAR_PED_STORED_HAT_PROP(PLAYER_PED_ID())
	CLEAR_ALL_PED_PROPS(PLAYER_PED_ID())
	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayerIsWeird, TRUE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseNormalExplosionDamageWhenBlownUpInVehicle, TRUE)
	SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), 100.0)
	SET_PED_MAX_HEALTH(PLAYER_PED_ID(), g_sMPTunables.iHunt_The_Beast_Beast_health)
	SET_ENTITY_HEALTH(PLAYER_PED_ID(), g_sMPTunables.iHunt_The_Beast_Beast_health)
	SET_PED_SUFFERS_CRITICAL_HITS(PLAYER_PED_ID(), FALSE)
	SET_DISABLE_HIGH_FALL_DEATH(PLAYER_PED_ID(), TRUE)
	SET_PLAYER_MAX_EXPLOSIVE_DAMAGE(PLAYER_ID(), 500)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IgnoreMeleeFistWeaponDamageMult, TRUE)
	SET_ENTITY_ALPHA(PLAYER_PED_ID(), 255, FALSE)
	SET_PLAYER_VEHICLE_DEFENSE_MODIFIER(PLAYER_ID(), 0.5)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_LawPedsCanFleeFromNonWantedPlayer, TRUE)
	
	g_i_BigfootTransformStages = 0
	RESET_NET_TIMER(g_st_BigfootFadeoutTimer)
	
	//Tell code we're now an animal. Don’t think you need this.
	SET_PLAYER_IS_IN_ANIMAL_FORM(TRUE)
	
	PRINTLN("[MORPHMODEL] CHANGE_PLAYER_PED_TO_BIGFOOT_FLAGS_ONLY - SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE) & NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE) ")
	SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
	NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)


	
	SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_IS_PLAYER_BIGFOOT)
	
	SET_PLAYER_PED_BIGFOOT(TRUE)
	
ENDPROC

PROC APPLY_FEMALE_BIGFOOT_CLOTHES()

	PRINTLN("[MORPHMODEL] APPLY_FEMALE_BIGFOOT_CLOTHES - called ")

	scrShopPedComponent componentItem
	
	GET_SHOP_PED_COMPONENT(HASH("DLC_MP_REPLAY_F_BERD_0_0"), componentItem)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)

	GET_SHOP_PED_COMPONENT(HASH("DLC_MP_REPLAY_F_JBIB_0_0"), componentItem)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	
	GET_SHOP_PED_COMPONENT(HASH("DLC_MP_REPLAY_F_TORSO_0_0"), componentItem)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_LEG, 4, 8)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_FEET, 2, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL, 3, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_DECL, 0, 0)

//	
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 1, 8)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_FEET, 3, 2)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL, 34, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_JBIB, 72, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_BERD, 59, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_TORSO, 112, 0)

ENDPROC


PROC APPLY_MALE_BIGFOOT_CLOTHES()

	PRINTLN("[MORPHMODEL] APPLY_MALE_BIGFOOT_CLOTHES - called ")

	scrShopPedComponent componentItem

	GET_SHOP_PED_COMPONENT(HASH("DLC_MP_REPLAY_M_BERD_0_0"), componentItem)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)

	GET_SHOP_PED_COMPONENT(HASH("DLC_MP_REPLAY_M_JBIB_0_0"), componentItem)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	
	GET_SHOP_PED_COMPONENT(HASH("DLC_MP_REPLAY_M_TORSO_0_0"), componentItem)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_LEG, 1, 5)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_FEET, 0, 10)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL, 15, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_DECL, 0, 0)

//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 0, 6)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_FEET, 4, 4)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_SPECIAL, 5, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_JBIB, 79, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_BERD, 59, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(),PED_COMP_TORSO, 97, 0)
	
ENDPROC

FUNC BOOL CHANGE_PLAYER_PED_TO_BIGFOOT()
	
	
	SWITCH g_i_BigfootTransformStages
	
		CASE 0
			IF IS_PLAYER_PED_BIGFOOT() = FALSE
				
				IF IS_PED_INJURED(PLAYER_PED_ID())
					NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_BIGFOOT - CALLED NETWORK_RESURRECT_LOCAL_PLAYER ")
					NETWORK_RESURRECT_LOCAL_PLAYER(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0.0, 0, IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())) // leave dead ped behind if on a mission (see 1684610)
				ENDIF
				
				
				g_i_BigfootTransformStages = 1
			ELSE
				PRINTLN("[MORPHMODEL] CHANGE_PLAYER_PED_TO_BIGFOOT: Already bigfoot ")
				RETURN TRUE
			ENDIF
		
		BREAK
		
		
		CASE 1
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0 //Male
					APPLY_MALE_BIGFOOT_CLOTHES()
				ELSE
					APPLY_FEMALE_BIGFOOT_CLOTHES()
				ENDIF
				CHANGE_PLAYER_PED_TO_BIGFOOT_FLAGS_ONLY()
				FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
				PRINTLN("[MORPHMODEL] CHANGE_PLAYER_PED_TO_BIGFOOT: done ")
				g_i_BigfootTransformStages = 2
				RESET_NET_TIMER(g_st_BigfootFadeoutTimer)

			ELSE
				PRINTLN("[MORPHMODEL] CHANGE_PLAYER_PED_TO_BIGFOOT: IS_PED_INJURED = TRUE, skip ")
				g_i_BigfootTransformStages = 2
			ENDIF
			
			
		
		BREAK
	
		CASE 2
			g_i_BigfootTransformStages = 0
			RETURN TRUE
		
		BREAK
	
	ENDSWITCH


	RETURN FALSE
	
ENDFUNC

PROC RUN_BIGFOOT_MAINTAIN_FUNCTIONS()
	
	IF IS_PLAYER_PED_BIGFOOT()

		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ARREST)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)

		IF IS_NET_PLAYER_OK(PLAYER_ID())
			
//			SET_PLAYER_CAN_USE_COVER(PLAYER_ID(),FALSE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableSecondaryAnimationTasks, TRUE)
			RESET_PLAYER_STAMINA(PLAYER_ID())
			SET_PED_MOVE_RATE_OVERRIDE(PLAYER_PED_ID(), g_sMPTunables.fHunt_The_Beast_Beast_speed)
			
			IF HAS_NET_TIMER_STARTED(g_SpawnData.timerBigfootShockingEvent)
				IF HAS_NET_TIMER_EXPIRED(g_SpawnData.timerBigfootShockingEvent, 500)
					IF g_SpawnData.iBigfootShockingEventId > 0
						REMOVE_SHOCKING_EVENT(g_SpawnData.iBigfootShockingEventId)
					ENDIF
					RESET_NET_TIMER(g_SpawnData.timerBigfootShockingEvent)
				ENDIF
			ELSE
				START_NET_TIMER(g_SpawnData.timerBigfootShockingEvent)
				g_SpawnData.iBigfootShockingEventId = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_EXPLOSION, PLAYER_PED_ID(),0)
			ENDIF
			
		ENDIF
		
//		IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//		OR GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_THIRD_PERSON_NEAR
//		      SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
//		ENDIF
	ENDIF

ENDPROC

PROC CHANGE_PLAYER_PED_TO_HUMAN_FLAGS_ONLY()
	
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN_FLAGS_ONLY - CALLED NETWORK_RESURRECT_LOCAL_PLAYER ")
		NETWORK_RESURRECT_LOCAL_PLAYER(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0.0, 0, FALSE) // leave dead ped behind if on a mission (see 1684610)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		SET_PED_MAX_HEALTH(PLAYER_PED_ID(), iMaxHealthBeforebigfoot)
		SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealthBeforebigfoot)
		
		PRINTLN("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN_FLAGS_ONLY - iMaxHealthBeforebigfoot = ", iMaxHealthBeforebigfoot, " ")
		
		//Fix for url:bugstar:2412778 - Hunt the Beast - Local player had a parachute on before becoming the Beast, which remained on and clipping through Beast clothes.
		CLEAN_UP_PLAYER_PED_PARACHUTE()	
		

		PRINTLN("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN_FLAGS_ONLY - SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE) & NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)")
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), TRUE)
		NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)
		
		SET_PED_SUFFERS_CRITICAL_HITS(PLAYER_PED_ID(), TRUE)
		
		Add_Delayed_Headshot_Regeneration_Request()
		
		RESET_NET_TIMER(g_SpawnData.timerBigfootShockingEvent)
		IF g_SpawnData.iBigfootShockingEventId > 0
			REMOVE_SHOCKING_EVENT(g_SpawnData.iBigfootShockingEventId)
		ENDIF
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayerIsWeird, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseNormalExplosionDamageWhenBlownUpInVehicle, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IgnoreMeleeFistWeaponDamageMult, FALSE)
		SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_MELEE_WEAPON_DAMAGE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_LawPedsCanFleeFromNonWantedPlayer, FALSE)
		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IgnoreMeleeFistWeaponDamageMult, FALSE)
		
		SET_DISABLE_HIGH_FALL_DEATH(PLAYER_PED_ID(), FALSE)
		
		SET_PLAYER_IS_IN_ANIMAL_FORM(FALSE)
		SET_PLAYER_PED_BIGFOOT(FALSE)
		
		SET_PLAYER_MAX_EXPLOSIVE_DAMAGE(PLAYER_ID())
		
		SET_PLAYER_VEHICLE_DEFENSE_MODIFIER(PLAYER_ID(), 1.0)
		
		SETUP_PLAYER_FROM_STATS()
		
		g_i_BigfootTransformStages = 0
		RESET_NET_TIMER(g_st_BigfootFadeoutTimer)
		
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_IS_PLAYER_BIGFOOT)
				
		PRINTLN("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN_FLAGS_ONLY - called ")
	ELSE
		PRINTLN("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN_FLAGS_ONLY - IS_PED_INJURED = TRUE ")
	ENDIF
		
ENDPROC


FUNC BOOL CHANGE_PLAYER_PED_TO_HUMAN(BOOL bDoFadeIn = TRUE)

	NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN - g_i_BigfootTransformStages ")NET_PRINT_INT(g_i_BigfootTransformStages)

	SWITCH g_i_BigfootTransformStages
	
		CASE 0
			

			IF IS_PLAYER_PED_BIGFOOT() 
				IF IS_SCREEN_FADED_IN()
				OR IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_OUT(500)
					NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN - DO_SCREEN_FADE_OUT ")
				ENDIF
				g_i_BigfootTransformStages = 1
			ELSE
				NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN - CASE 0 Player is not bigfoot, move on. ")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 1
			
			IF IS_SCREEN_FADED_OUT()
				IF IS_PLAYER_PED_BIGFOOT() 

					CHANGE_PLAYER_PED_TO_HUMAN_FLAGS_ONLY()
					
					FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
					

					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					g_i_BigfootTransformStages = 2
					RESET_NET_TIMER(g_st_BigfootFadeoutTimer)
				ELSE
					RETURN TRUE
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN - CASE 1 screen is not faded out, wait ")
			ENDIF
			
		BREAK
		
		CASE 2
			NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN - CASE 2 ")
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_st_BigfootFadeoutTimer, 500)
			AND HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID())
			AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())

				
				IF bDoFadeIn 
					IF IS_SCREEN_FADED_OUT()
					OR IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					g_i_BigfootTransformStages = 3
				ELSE
					g_i_BigfootTransformStages = 4
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN - HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID()) = ")NET_PRINT_BOOL(HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID()))
				NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN - HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID()) = ")NET_PRINT_BOOL(HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID()))
			ENDIF
		BREAK
		
		CASE 3
			NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN - CASE 3 ")
			IF IS_SCREEN_FADED_IN()
				g_i_BigfootTransformStages = 0
				RESET_NET_TIMER(g_st_BigfootFadeoutTimer)
				RETURN TRUE
			ELSE
				IF IS_SCREEN_FADED_OUT()
				OR IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
			ENDIF
		BREAK
	
		CASE 4
			NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN - CASE 4 ")
			g_i_BigfootTransformStages = 0
			RESET_NET_TIMER(g_st_BigfootFadeoutTimer)
			RETURN TRUE
		
		BREAK
	
	
	ENDSWITCH
	
	NET_NL()NET_PRINT("[MORPHMODEL] CHANGE_PLAYER_PED_TO_HUMAN - still running. ")

	RETURN FALSE
	
ENDFUNC



FUNC BOOL SHOULD_HIDE_ALL_JOB_BLIPS_FOR_YACHT()
	RETURN g_FmBlockAllMissionBlipsForYacht
ENDFUNC

PROC SET_HIDE_ALL_JOB_BLIPS_FOR_YACHT_STATUS(BOOL bStatus)
	#IF IS_DEBUG_BUILD
	IF bStatus
		IF g_FmBlockAllMissionBlipsForYacht = FALSE
			PRINTLN("[LOWFLOW] * SET_HIDE_ALL_JOB_BLIPS_FOR_YACHT_STATUS = TRUE")
		ENDIF
	ELSE
		IF g_FmBlockAllMissionBlipsForYacht
			PRINTLN("[LOWFLOW] * SET_HIDE_ALL_JOB_BLIPS_FOR_YACHT_STATUS = FALSE")
		ENDIF
	ENDIF
	#ENDIF
	
	g_FmBlockAllMissionBlipsForYacht = bStatus
ENDPROC

PROC INITIALISE_RESTRICTED_AREA_BLOCKING()
	g_iDisabledRestrictedAreas = g_iDisabledRestrictedAreas | g_fmmc_struct.iDisabledRestrictedAreasBitSet
	PRINTLN("[AREA_BLOCKING] INITIALISE_RESTRICTED_AREA_BLOCKING - g_iDisabledRestrictedAreas = g_iDisabledRestrictedAreas | g_fmmc_struct.iDisabledRestrictedAreasBitSet = ", g_iDisabledRestrictedAreas)
ENDPROC

PROC CLEAR_RESTRICTED_AREA_BLOCKING()
	INT iBitsToClear = g_fmmc_struct.iDisabledRestrictedAreasBitSet ^ 255
	
	g_iDisabledRestrictedAreas &= iBitsToClear
	PRINTLN("[AREA_BLOCKING] CLEAR_RESTRICTED_AREA_BLOCKING - g_iDisabledRestrictedAreas &= iBitsToClear = ", g_iDisabledRestrictedAreas)
ENDPROC


FUNC BOOL IS_LOCAL_PLAYER_WATCHING_BUNKER_INTRO_MOCAP()
	RETURN MPGlobalsAmbience.bWatchingBunkerMocap
ENDFUNC

PROC SET_LOCAL_PLAYER_WATCHING_BUNKER_INTRO_MOCAP()
	IF NOT MPGlobalsAmbience.bWatchingBunkerMocap
		PRINTLN("[RUN_BUNKER_INTRO_CUTSCENE] [SET_LOCAL_PLAYER_WATCHING_BUNKER_INTRO_MOCAP] Called")
		MPGlobalsAmbience.bWatchingBunkerMocap = TRUE
	ENDIF
ENDPROC

PROC CLEAR_LOCAL_PLAYER_WATCHING_BUNKER_INTRO_MOCAP()
	IF MPGlobalsAmbience.bWatchingBunkerMocap
		PRINTLN("[RUN_BUNKER_INTRO_CUTSCENE] [CLEAR_LOCAL_PLAYER_WATCHING_BUNKER_INTRO_MOCAP] Called")
		MPGlobalsAmbience.bWatchingBunkerMocap = FALSE
	ENDIF
ENDPROC
 // FEATURE_GUNRUNNING

FUNC MODEL_NAMES GET_MOC_PED_MODEL()
	RETURN G_M_M_ChiGoon_02
ENDFUNC

FUNC BOOL MAINTAIN_FREEMODE_BAIL_CONDITIONS()
	
	#IF FEATURE_SUMMER_2020
	IF NOT g_sMPTunables.bDISABLE_MAIN_SC_FM_KICK
	#ENDIF
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main")) > 0
		AND IS_SKYSWOOP_AT_GROUND()
			SCRIPT_BLOCK_ACCESS_TO_MULTIPLAYER(TRUE)
		ENDIF
	#IF FEATURE_SUMMER_2020
	ENDIF
	#ENDIF
	
	IF HAS_SCRIPT_BLOCKED_MULTIPLAYER_ACCESS()
		NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_TS_SAFETY_BAIL))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

