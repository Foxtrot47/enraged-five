/* ===================================================================================== *\
		SCRIPT NAME		:	FMMC_Corona_Cutscenes.sch
		AUTHOR			:	Alastair Costley
		CREATED (Y/M/D)	:	2015/06/02 (R.I.P. Calton Square)
		DESCRIPTION		:	Container for all generic cutscene flows that might be used during a corona.
\* ===================================================================================== */


// INCLUDES.
USING "rage_builtins.sch"
USING "globals.sch"
USING "Commands_Cutscene.sch"
USING "script_network.sch"
USING "net_team_info.sch"
USING "net_realty_details.sch"
USING "net_cutscene.sch"
USING "context_control_public.sch"
USING "fmmc_corona_menu.sch"
USING "FMMC_Veh_Mod_Preset_List.sch"


// CONSTANTS, STRUCTS & VARIABLES.


// DEBUG ONLY
#IF IS_DEBUG_BUILD

WIDGET_GROUP_ID DEBUG_CutsceneWidgets
WIDGET_GROUP_ID DEBUG_CoronaParentWidgets
BOOL			DEBUG_bDrawnWidgets = FALSE
BOOL			DEBUG_bClearAllStats, DEBUG_bClearSeen, DEBUG_bClearHelptext, DEBUG_bDisableStatsSave

INT 			DEBUG_iState, DEBUG_iSubState

PROC DRAW_CORONA_CUTSCENE_WIDGETS(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	IF NOT DEBUG_bDrawnWidgets
	
		DEBUG_CutsceneWidgets = DEBUG_CutsceneWidgets
		SET_CURRENT_WIDGET_GROUP(DEBUG_CoronaParentWidgets)
		DEBUG_CutsceneWidgets = START_WIDGET_GROUP("Corona Cutscene System") 
		
			START_WIDGET_GROUP("Cutscene Config")
				ADD_WIDGET_STRING(sCoronaCutsConf.tlName)
				ADD_WIDGET_INT_READ_ONLY("State: ", DEBUG_iState)
				ADD_WIDGET_INT_READ_ONLY("Substate:", DEBUG_iSubState)
				ADD_WIDGET_VECTOR_SLIDER("vPosition: ", sCoronaCutsConf.vPosition, -10000, 10000, 1)
				ADD_WIDGET_VECTOR_SLIDER("vRotation: ", sCoronaCutsConf.vRotation, -10000, 10000, 1)
				ADD_WIDGET_INT_READ_ONLY("Participants: ", sCoronaCutsConf.iNumParticipants)
				
				ADD_WIDGET_BOOL("Disable Stats Save", DEBUG_bDisableStatsSave)
				ADD_WIDGET_BOOL("Clear All Cutscene Stats", DEBUG_bClearAllStats)
				ADD_WIDGET_BOOL("Clear Seen Stats", DEBUG_bClearSeen)
				ADD_WIDGET_BOOL("Clear Helptext Stats", DEBUG_bClearHelptext)

				// TODO: Add the array here too.		
						
			STOP_WIDGET_GROUP()
		
		STOP_WIDGET_GROUP()
		
		CLEAR_CURRENT_WIDGET_GROUP(DEBUG_CoronaParentWidgets)
		
		DEBUG_bDrawnWidgets = TRUE

	ENDIF

ENDPROC


FUNC STRING GET_STRING_FROM_CORONA_STATE(CORONA_CUTSCENE_STATE eValue)
	SWITCH eValue
		DEFAULT 						RETURN "ERROR - MISSING STATE"
		CASE CORONA_CUTSCENE_NONE 		RETURN "CORONA_CUTSCENE_NONE"
		CASE CORONA_CUTSCENE_INIT 		RETURN "CORONA_CUTSCENE_INIT"
		CASE CORONA_CUTSCENE_WAIT 		RETURN "CORONA_CUTSCENE_WAIT"
		CASE CORONA_CUTSCENE_PLAY		RETURN "CORONA_CUTSCENE_PLAY"
		CASE CORONA_CUTSCENE_CLEANUP 	RETURN "CORONA_CUTSCENE_CLEANUP"
	ENDSWITCH
ENDFUNC

FUNC STRING GET_STRING_FROM_CORONA_SUBSTATE(CORONA_CUTSCENE_SUBSTATE eValue)
	SWITCH eValue
		DEFAULT 									RETURN "ERROR - MISSING SUBSTATE"
		CASE CORONA_CUTSCENE_SUBSTATE_PEDS			RETURN "CORONA_CUTSCENE_SUBSTATE_PEDS"
		CASE CORONA_CUTSCENE_SUBSTATE_TRIGGER 		RETURN "CORONA_CUTSCENE_SUBSTATE_TRIGGER"
		CASE CORONA_CUTSCENE_SUBSTATE_STREAM		RETURN "CORONA_CUTSCENE_SUBSTATE_STREAM"
		CASE CORONA_CUTSCENE_SUBSTATE_LOADING 		RETURN "CORONA_CUTSCENE_SUBSTATE_LOADING"
		CASE CORONA_CUTSCENE_SUBSTATE_VEHICLES		RETURN "CORONA_CUTSCENE_SUBSTATE_VEHICLES"
		CASE CORONA_CUTSCENE_SUBSTATE_INTERIOR		RETURN "CORONA_CUTSCENE_SUBSTATE_INTERIOR"
		CASE CORONA_CUTSCENE_SUBSTATE_FINISHED		RETURN "CORONA_CUTSCENE_SUBSTATE_FINISHED"
	ENDSWITCH
ENDFUNC


/// PURPOSE:
///    Output all corona cutscene stats to the log.
PROC DUMP_CORONA_CUTSCENE_STATS_TO_LOG()
	PRINTLN("[CORONA][CUTS] - DUMP_CORONA_CUTSCENE_STATS_TO_LOG - Begin dump:")
	PRINTLN(".....	MP_STAT_LOW_FLOW_CS_DRV_SEEN	- ", GET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_DRV_SEEN))
	PRINTLN(".....	MP_STAT_LOW_FLOW_CS_TRA_SEEN	- ", GET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_TRA_SEEN))
	PRINTLN(".....	MP_STAT_LOW_FLOW_CS_FUN_SEEN	- ", GET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_FUN_SEEN))
	PRINTLN(".....	MP_STAT_LOW_FLOW_CS_PHO_SEEN	- ", GET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_PHO_SEEN))
	PRINTLN(".....	MP_STAT_LOW_FLOW_CS_FIN_SEEN	- ", GET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_FIN_SEEN))
	PRINTLN(".....	MP_STAT_LOW_FLOW_CS_HELPTEXT	- ", GET_MP_INT_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_HELPTEXT))
	PRINTLN("[CORONA][CUTS] - DUMP_CORONA_CUTSCENE_STATS_TO_LOG - End dump.")
ENDPROC


/// PURPOSE:
///    Maintain debug controls that require every-frame updates (e.g. stat clear bools).
PROC MAINTAIN_DEBUG_STAT_CONTROLS()
	
	IF DEBUG_bClearAllStats
		DEBUG_bClearAllStats = FALSE
		PRINTLN("[CORONA][CUTS] - MAINTAIN_DEBUG_STAT_CONTROLS - CLEAR ALL STATS.")
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_DRV_SEEN, FALSE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_TRA_SEEN, FALSE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_FUN_SEEN, FALSE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_PHO_SEEN, FALSE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_FIN_SEEN, FALSE)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_HELPTEXT, 0)
	ENDIF
	
	IF DEBUG_bClearSeen
		DEBUG_bClearSeen = FALSE
		PRINTLN("[CORONA][CUTS] - MAINTAIN_DEBUG_STAT_CONTROLS - CLEAR SEEN STATS.")
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_DRV_SEEN, FALSE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_TRA_SEEN, FALSE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_FUN_SEEN, FALSE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_PHO_SEEN, FALSE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_FIN_SEEN, FALSE)
	ENDIF
	
	IF DEBUG_bClearHelptext
		DEBUG_bClearHelptext = FALSE
		PRINTLN("[CORONA][CUTS] - MAINTAIN_DEBUG_STAT_CONTROLS - CLEAR HELPTEXT STATS.")
		SET_MP_INT_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_HELPTEXT, 0)
	ENDIF
	
ENDPROC
#ENDIF

/// PURPOSE:
///    Cutscene system state set wrapper.
PROC SET_CORONA_CUTSCENE_STATE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, CORONA_CUTSCENE_STATE eState)

	#IF IS_DEBUG_BUILD
		PRINTLN("[CORONA][CUTS] - SET_CORONA_CUTSCENE_STATE - Setting state from '",GET_STRING_FROM_CORONA_STATE(sCoronaCutsConf.eState),"' to '",GET_STRING_FROM_CORONA_STATE(eState),"'.")
		DEBUG_PRINTCALLSTACK()
		DEBUG_iState = ENUM_TO_INT(eState)
	#ENDIF

	sCoronaCutsConf.eState = eState

ENDPROC


/// PURPOSE:
///    Cutscene system sub-state set wrapper.
PROC SET_CORONA_CUTSCENE_SUBSTATE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE eSubState)

	#IF IS_DEBUG_BUILD
		PRINTLN("[CORONA][CUTS] - SET_CORONA_CUTSCENE_SUBSTATE - Setting state from '",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState),"' to '",GET_STRING_FROM_CORONA_SUBSTATE(eSubState),"'.")
		DEBUG_PRINTCALLSTACK()
		DEBUG_iSubState = ENUM_TO_INT(eSubState)
	#ENDIF

	sCoronaCutsConf.eSubState = eSubState

ENDPROC


FUNC BOOL IS_CORONA_CUTSCENE_IN_PROGRESS(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	IF sCoronaCutsConf.eState = CORONA_CUTSCENE_PLAY
	OR sCoronaCutsConf.eState = CORONA_CUTSCENE_CLEANUP
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Process the Skip Cutscene event on all connected clients.
PROC PROCESS_EVENT_SKIP_CORONA_CUTSCENE()
	PRINTLN("[CORONA][CUTS] - PROCESS_EVENT_SKIP_CORONA_CUTSCENE - Processing skip corona cutscene event.")
	g_CoronaCutsceneData.bSkip = TRUE // Nasty use of globals, unavoidable.
ENDPROC


/// PURPOSE:
///    Process the Skip Cutscene event on all connected clients.
PROC BROADCAST_EVENT_SKIP_CORONA_CUTSCENE()
	PRINTLN("[CORONA][CUTS] - BROADCAST_EVENT_SKIP_CORONA_CUTSCENE - Sending skip corona cutscene event from player: [",NATIVE_TO_INT(PLAYER_ID()),", ", GET_PLAYER_NAME(PLAYER_ID()), "]")
	BROADCAST_GENERAL_EVENT(GENERAL_EVENT_SKIP_CORONA_CUTSCENE, ALL_PLAYERS())
ENDPROC


/// PURPOSE:
///    Start a safety timeout timer in case cutscene triggering gets stuck.
PROC START_CORONA_CUTSCENE_SAFETY_TIMER(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	PRINTLN("[CORONA][CUTS] - START_CORONA_CUTSCENE_SAFETY_TIMER - Setting safety timeout to: ", ciCORONA_CUTSCENE_SAFETY_TIMEOUT, "ms")
	sCoronaCutsConf.SafetyTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciCORONA_CUTSCENE_SAFETY_TIMEOUT)
ENDPROC


/// PURPOSE:
///    Check the corona cutscene safety timer against the current time, if the timer
///    has expired then return TRUE.
/// RETURNS:
///    TRUE when cutscene timed out.
FUNC BOOL HAS_CORONA_CUTSCENE_SAFETY_TIMER_EXPIRED(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), sCoronaCutsConf.SafetyTimer))
ENDFUNC


/// PURPOSE:
///    Start a radio cutout timer to kill the corona radio 1 second after starting a cutscene.
///    This is to give time for the active audio scene to fade out properly.
PROC START_CORONA_CUTSCENE_RADIO_TIMER(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	PRINTLN("[CORONA][CUTS] - START_CORONA_CUTSCENE_SAFETY_TIMER - Setting radio cutout time to: ", ciCORONA_CUTSCENE_RADIO_TIMEOUT, "ms")
	sCoronaCutsConf.RadioTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciCORONA_CUTSCENE_RADIO_TIMEOUT)
ENDPROC


/// PURPOSE:
///    Check the corona cutscene radio cutout timer against the current time, if the timer
///    has expired then return TRUE.
/// RETURNS:
///    TRUE when radio needs to be killed.
FUNC BOOL HAS_CORONA_CUTSCENE_RADIO_TIMER_EXPIRED(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), sCoronaCutsConf.RadioTimer))
ENDFUNC


/// PURPOSE:
///    Check playerBD to make sure that all cutscene participants have entered the cutscene playing state.
/// RETURNS:
///    TRUE when all have set playerBD.
FUNC BOOL ARE_ALL_PARTICIPANTS_READY_TO_SKIP(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	INT index
	BOOL bReady = TRUE
	
	FOR index = 0 TO (sCoronaCutsConf.iNumParticipants-1)
		IF (sCoronaCutsConf.pidPlayerList[index] != INVALID_PLAYER_INDEX())
			IF NOT GlobalplayerBD_FM[NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[index])].sClientCoronaData.bPlayingCoronaCutscene
				PRINTLN("[CORONA][CUTS] - ARE_ALL_PARTICIPANTS_READY_TO_SKIP - Player [",NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[index]),", ",GET_PLAYER_NAME(sCoronaCutsConf.pidPlayerList[index]),"] is not ready for skip.")
				bReady = FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	IF bReady
		PRINTLN("[CORONA][CUTS] - ARE_ALL_PARTICIPANTS_READY_TO_SKIP - All players are ready to skip, returning TRUE.")
	ENDIF
	#ENDIF
	
	RETURN bReady

ENDFUNC


/// PURPOSE:
///    Poll inputs to check if the skip button has been pressed.
/// RETURNS:
///    TRUE when skip is pressed.
FUNC BOOL HAS_CORONA_CUTSCENE_SKIP_BEEN_REQUESTED(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	
	// Corona host is the only person who can request a skip.
	IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
		RETURN FALSE
	ENDIF
	
	IF (sCoronaCutsConf.eTrackerStat != MP_STAT_DEFAULT_STATS_SET)
		IF NOT GET_MP_BOOL_CHARACTER_STAT(sCoronaCutsConf.eTrackerStat)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF sCoronaCutsConf.iSkipTracker != ciCORONA_CUTSCENE_INVALID_SKIP
		IF (NOT IS_HELP_MESSAGE_BEING_DISPLAYED())
			INT iSkipTextBitset = GET_MP_INT_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_HELPTEXT)
			
			IF NOT IS_BIT_SET(iSkipTextBitset, sCoronaCutsConf.iSkipTracker)
				SET_BIT(iSkipTextBitset, sCoronaCutsConf.iSkipTracker)
				PRINT_HELP("FLOW_CS_SKIP2")
				SET_MP_INT_CHARACTER_STAT(MP_STAT_LOW_FLOW_CS_HELPTEXT, iSkipTextBitset)
				PRINTLN("[CORONA][CUTS] - HAS_CORONA_CUTSCENE_SKIP_BEEN_REQUESTED - Updating iSkipTracker: ",sCoronaCutsConf.iSkipTracker," (bitset: ",iSkipTextBitset,"), showing help-text: FLOW_CS_SKIP2")
			ENDIF
		ENDIF
	ENDIF

	IF IS_PC_VERSION()
		IF NOT NETWORK_TEXT_CHAT_IS_TYPING()
			IF (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SKIP_CUTSCENE))
			OR (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE))
				PRINTLN("[CORONA][CUTS] - HAS_CORONA_CUTSCENE_SKIP_BEEN_REQUESTED - PC - Host has elected to SKIP this cutscene, returning TRUE.")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SKIP_CUTSCENE))
		OR (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE))
			PRINTLN("[CORONA][CUTS] - HAS_CORONA_CUTSCENE_SKIP_BEEN_REQUESTED - Host has elected to SKIP this cutscene, returning TRUE.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_VCM_FLOW_CUTSCENE_INDEX(INT index)
	SWITCH index
		CASE 0		RETURN ciLOW_FLOW_MISSION_VCM_RESCUE
		CASE 1		RETURN ciLOW_FLOW_MISSION_VCM_BRAWL
		CASE 2		RETURN ciLOW_FLOW_MISSION_VCM_MEETING
		CASE 3		RETURN ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION
		CASE 4		RETURN ciLOW_FLOW_MISSION_VCM_DEFEND
		CASE 5		RETURN ciLOW_FLOW_MISSION_VCM_FINALE
	ENDSWITCH
	
	RETURN INVALID_CUTSCENE_DATA
ENDFUNC

FUNC BOOL IS_THIS_A_VCM_CUTSCENE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_VCM_RESCUE
		CASE ciLOW_FLOW_MISSION_VCM_BRAWL
		CASE ciLOW_FLOW_MISSION_VCM_MEETING
		CASE ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION
		CASE ciLOW_FLOW_MISSION_VCM_DEFEND
		CASE ciLOW_FLOW_MISSION_VCM_FINALE
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if a cutscene has been selected for playback, if so:
///    - Grab cutscene details for respective ID (participants, name, etc)
///    - Return TRUE
/// RETURNS:
///    TRUE when config is available and cutscene playback is imminent.
FUNC BOOL HAVE_CUTSCENE_CONFIG(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	IF sCoronaCutsConf.iCutsceneId = INVALID_CUTSCENE_DATA
		RETURN FALSE
	ENDIF
	
	// Currently there are only 2 missions with cutscenes in the lowrider pack. Add more here as required.
	SWITCH sCoronaCutsConf.iCutsceneId
	
		DEFAULT 
			PRINTLN("[CORONA][CUTS] - HAVE_CUTSCENE_CONFIG - CRITICAL ERROR! iCutsceneId is INVALID, value: ", sCoronaCutsConf.iCutsceneId)
		RETURN FALSE
	
		CASE ciLOW_FLOW_MISSION_DRIVE_BY_SHOOTOUT
			
			sCoronaCutsConf.tlName = "LOW_DRV_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
			sCoronaCutsConf.eTrackerStat = MP_STAT_LOW_FLOW_CS_DRV_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_DRV_SKIP_SEEN
			
			// add models/handles for cars
			sCoronaCutsConf.iNumVehicles = 2
			sCoronaCutsConf.vehModel[0] = BUCCANEER2
			sCoronaCutsConf.vehModel[1] = chino2			
			sCoronaCutsConf.vehHandle[0] = "BUCCANEER2"
			sCoronaCutsConf.vehHandle[1] = "chino2"
			sCoronaCutsConf.vehNumPlate[0] = ""
			sCoronaCutsConf.vehNumPlate[1] = ""
			sCoronaCutsConf.eCreateVehSubstate = CORONA_CUTSCENE_VEHICLE_CREATION_SUBSTATE_LOAD_MODELS
			
			// This cutscene has specific model movers for specific teams.
			sCoronaCutsConf.bTeamSpecificMovers = TRUE
			
			// Vargos
			sCoronaCutsConf.tlMoverName[1] = "MP_2"
			sCoronaCutsConf.iMoverTeam[1] = 0
			sCoronaCutsConf.tlMoverName[2] = "MP_3"
			sCoronaCutsConf.iMoverTeam[2] = 0
			
			// Ballas
			sCoronaCutsConf.tlMoverName[0] = "MP_1"
			sCoronaCutsConf.iMoverTeam[0] = 1
			sCoronaCutsConf.tlMoverName[3] = "MP_4"
			sCoronaCutsConf.iMoverTeam[3] = 1		
			
		BREAK
	
		CASE ciLOW_FLOW_MISSION_TRANSPORT_LOWRIDERS 
			
			sCoronaCutsConf.tlName = "LOW_TRA_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = 2
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
			sCoronaCutsConf.eTrackerStat = MP_STAT_LOW_FLOW_CS_TRA_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_TRA_SKIP_SEEN
			
			// Vehicles
			sCoronaCutsConf.iNumVehicles = 2
			sCoronaCutsConf.vehModel[0] = VOODOO
			sCoronaCutsConf.vehModel[1] = FACTION2
			sCoronaCutsConf.vehHandle[0] = "MP_Car_1"
			sCoronaCutsConf.vehHandle[1] = "MP_Car_2"
			sCoronaCutsConf.vehNumPlate[0] = ""
			sCoronaCutsConf.vehNumPlate[1] = ""
			sCoronaCutsConf.eCreateVehSubstate = CORONA_CUTSCENE_VEHICLE_CREATION_SUBSTATE_LOAD_MODELS
			
		BREAK
		
		CASE ciLOW_FLOW_MISSION_FUNERAL_SHOOTOUT			
			
			sCoronaCutsConf.tlName = "LOW_FUN_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_BOTH_CLONES // Needs 8 peds instead of 4.
			sCoronaCutsConf.eTrackerStat = MP_STAT_LOW_FLOW_CS_FUN_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_FUN_SKIP_SEEN
		
		BREAK
		
		CASE ciLOW_FLOW_MISSION_PHOTO_CARS			
			
			sCoronaCutsConf.tlName = "LOW_PHO_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = 2
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
			sCoronaCutsConf.eTrackerStat = MP_STAT_LOW_FLOW_CS_PHO_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_PHO_SKIP_SEEN
			
			// This cutscene has specific model movers for specific teams.
			sCoronaCutsConf.bTeamSpecificMovers = TRUE

			sCoronaCutsConf.tlMoverName[0] = "MP_1"
			sCoronaCutsConf.iMoverTeam[0] = 1
			sCoronaCutsConf.tlMoverName[1] = "MP_2"
			sCoronaCutsConf.iMoverTeam[1] = 0
			
		BREAK
		
		CASE ciLOW_FLOW_MISSION_LOWRIDER_FINALE		
			
			sCoronaCutsConf.tlName = "LOW_FIN_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
			sCoronaCutsConf.eTrackerStat = MP_STAT_LOW_FLOW_CS_FIN_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_FIN_SKIP_SEEN
		
		BREAK
		
		CASE ciLOW_FLOW_MISSION_VCM_RESCUE
			sCoronaCutsConf.tlName = "MPCAS1_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS		// g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
			sCoronaCutsConf.eTrackerStat = MP_STAT_VCM_FLOW_CS_RSC_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_VCM_SKIP_SEEN
		BREAK
		
		CASE ciLOW_FLOW_MISSION_VCM_BRAWL
			sCoronaCutsConf.tlName = "MPCAS2_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
			sCoronaCutsConf.eTrackerStat = MP_STAT_VCM_FLOW_CS_BWL_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_VCM_SKIP_SEEN
		BREAK
		
		CASE ciLOW_FLOW_MISSION_VCM_MEETING
			sCoronaCutsConf.tlName = "MPCAS3_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
			sCoronaCutsConf.eTrackerStat = MP_STAT_VCM_FLOW_CS_MTG_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_VCM_SKIP_SEEN
		BREAK
		
		CASE ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION
			sCoronaCutsConf.tlName = "MPCAS4_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
			sCoronaCutsConf.eTrackerStat = MP_STAT_VCM_FLOW_CS_OIL_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_VCM_SKIP_SEEN
			
			sCoronaCutsConf.binkMovieID = SET_BINK_MOVIE("MPCASTV_MPCAS4_A")
			sCoronaCutsConf.bRenderFullscreen = TRUE	// start fullscreen for this cutscene
		BREAK
		
		CASE ciLOW_FLOW_MISSION_VCM_DEFEND
			sCoronaCutsConf.tlName = "MPCAS5_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
			sCoronaCutsConf.eTrackerStat = MP_STAT_VCM_FLOW_CS_DEF_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_VCM_SKIP_SEEN
			
			sCoronaCutsConf.binkMovieID = SET_BINK_MOVIE("MPCASTV_MPCAS5_A")
		BREAK
		
		CASE ciLOW_FLOW_MISSION_VCM_FINALE
			sCoronaCutsConf.tlName = "MPCAS6_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
			sCoronaCutsConf.eTrackerStat = MP_STAT_VCM_FLOW_CS_FIN_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_VCM_SKIP_SEEN
		BREAK
		
		#IF FEATURE_CASINO_HEIST
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT1
			sCoronaCutsConf.tlName = "HS3F_INT1"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
//			sCoronaCutsConf.eTrackerStat = MP_STAT_VCM_FLOW_CS_FIN_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_CASINO_HEIST_SEEN
			sCoronaCutsConf.iNumNPCs = MAX_CORONA_CUTSCENE_NPC_PEDS
		BREAK
		
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT2
			sCoronaCutsConf.tlName = "HS3F_INT2"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
//			sCoronaCutsConf.eTrackerStat = MP_STAT_VCM_FLOW_CS_FIN_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_CASINO_HEIST_SEEN
			sCoronaCutsConf.iNumNPCs = MAX_CORONA_CUTSCENE_NPC_PEDS
			sCoronaCutsConf.bDelayPedConfig = TRUE
		BREAK
		#ENDIF
		
		#IF FEATURE_HEIST_ISLAND
		CASE ciLOW_FLOW_MISSION_HEIST_ISLAND_HS4F_INT
			sCoronaCutsConf.tlName = "HS4F_INT"
			sCoronaCutsConf.vPosition = g_FMMC_STRUCT.vStartPos
			sCoronaCutsConf.vRotation = << 0.0, 0.0, 0.0 >>
			sCoronaCutsConf.iNumParticipants = MAX_CORONA_CUTSCENE_PARTICIPANTS
			sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_CORONA_CLONES
//			sCoronaCutsConf.eTrackerStat = MP_STAT_VCM_FLOW_CS_FIN_SEEN
			sCoronaCutsConf.iSkipTracker = ciCORONA_CUTSCENE_HEIST_ISLAND_SEEN
			sCoronaCutsConf.bHasPlaybackList = TRUE
			IF g_sHeistIslandConfig.eApproachVehicle = HIAV_SUBMARINE
				sCoronaCutsConf.ePlaybackFlags = CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_6 | CS_SECTION_7
			ELSE
				sCoronaCutsConf.ePlaybackFlags = CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5
			ENDIF
		BREAK
		#ENDIF
	ENDSWITCH 

	#IF IS_DEBUG_BUILD
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_LowFlowIgnoreCutsceneAuth")
		IF NOT IS_CUTSCENE_AUTHORIZED(sCoronaCutsConf.tlName)
			PRINTLN("[CORONA][CUTS] - HAVE_CUTSCENE_CONFIG - ERROR! Cutscene is NOT authorised for playback! ", sCoronaCutsConf.tlName)
			SET_CORONA_CUTSCENE_STATE(sCoronaCutsConf, CORONA_CUTSCENE_CLEANUP)
			RETURN FALSE
		ENDIF
	ENDIF
	#ENDIF
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Get the players in the session and store their details for the cutscene system.
///    Also grabs corona clone details if required.
PROC GRAB_CORONA_CUTSCENE_DATA(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	// Store the Player_Indexes of the players that should be in the Mocap
	INT tempLoop, numPlayers, index, subIndex
	PLAYER_INDEX thisPlayer = INVALID_PLAYER_INDEX()
	
	REPEAT MAX_CORONA_CUTSCENE_PARTICIPANTS tempLoop
		sCoronaCutsConf.pidPlayerList[tempLoop] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	// Then, find up to three other players
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (numPlayers < sCoronaCutsConf.iNumParticipants)
			thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
			IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
				IF NOT (IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(thisPlayer))
					sCoronaCutsConf.pidPlayerList[numPlayers] = thisPlayer
					PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_DATA - Saving member: ", GET_PLAYER_NAME(thisPlayer), ", in pidPlayerList slot: ", numPlayers)
					numPlayers++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Clear out the Mocap PedIndexes
	REPEAT MAX_CORONA_CUTSCENE_PARTICIPANTS tempLoop
		sCoronaCutsConf.pedCloneList[tempLoop] = NULL
	ENDREPEAT
	
	SWITCH sCoronaCutsConf.iCloneSource
		
		DEFAULT
			// Remove any peds hanging around from the corona.
			FM_DELETE_ALL_PED_FOR_CORONA()
		BREAK
		
		CASE ciCORONA_CUTSCENE_CORONA_CLONES
		
			PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_DATA - Processing clone data, config: ciCORONA_CUTSCENE_CORONA_CLONES")
		
			FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
				IF DOES_ENTITY_EXIST(g_CoronaClonePeds.piPlayersPed[index].pedID)
					FOR subIndex = 0 TO (sCoronaCutsConf.iNumParticipants-1)
						// Check if the existant corona ped is a valid cutscene participant.
						IF (NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[subIndex]) = index)
							PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_DATA - Reusing corona ped: ", NATIVE_TO_INT(g_CoronaClonePeds.piPlayersPed[index].pedID))
							sCoronaCutsConf.pedCloneList[subIndex] = g_CoronaClonePeds.piPlayersPed[index].pedID
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
		
		BREAK
		
		CASE ciCORONA_CUTSCENE_BOTH_CLONES
		
			PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_DATA - Processing clone data, config: ciCORONA_CUTSCENE_BOTH_CLONES")
		
			FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
				IF DOES_ENTITY_EXIST(g_CoronaClonePeds.piPlayersPed[index].pedID)
					FOR subIndex = 0 TO (sCoronaCutsConf.iNumParticipants-1)
						// Check if the existant corona ped is a valid cutscene participant.
						IF (NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[subIndex]) = index)
							PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_DATA - Adding corona ped: ", NATIVE_TO_INT(g_CoronaClonePeds.piPlayersPed[index].pedID))
							sCoronaCutsConf.pedCoronaCloneList[subIndex] = g_CoronaClonePeds.piPlayersPed[index].pedID
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
		
		BREAK
	
	ENDSWITCH
	
	// Clear out NPC ped indexes
	REPEAT MAX_CORONA_CUTSCENE_NPC_PEDS tempLoop
		sCoronaCutsConf.pedNPC[tempLoop] = NULL
	ENDREPEAT
	
ENDPROC

PROC SETUP_PED_FOR_CUTSCENE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, INT tempLoop, BOOL forceSetup)
	
	IF sCoronaCutsConf.bDelayPedConfig
	AND NOT forceSetup						// A ped is forced to be setup the same frame the cutscene will begin
		PRINTLN("[CORONA][CUTS] - SETUP_PED_FOR_CUTSCENE - sCoronaCutsConf.bDelayPedConfig = TRUE. tempLoop = ", tempLoop)		
		EXIT
	ENDIF
	
	CLEAR_PED_TASKS_IMMEDIATELY(sCoronaCutsConf.pedCloneList[tempLoop])
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sCoronaCutsConf.pedCloneList[tempLoop], TRUE)
	
	// ------- Fix for Bug: 206347 Rowan J
	SET_PED_RESET_FLAG(sCoronaCutsConf.pedCloneList[tempLoop], PRF_BlockWeaponReactionsUnlessDead, TRUE) 
	SET_PED_RESET_FLAG(sCoronaCutsConf.pedCloneList[tempLoop], PRF_DisableSecondaryAnimationTasks, TRUE) 
	SET_PED_CONFIG_FLAG(sCoronaCutsConf.pedCloneList[tempLoop], PCF_PedIgnoresAnimInterruptEvents, TRUE)
	SET_PED_CONFIG_FLAG(sCoronaCutsConf.pedCloneList[tempLoop], PCF_DisableExplosionReactions, TRUE)
	SET_PED_CONFIG_FLAG(sCoronaCutsConf.pedCloneList[tempLoop], PCF_RunFromFiresAndExplosions, FALSE)
	// -------
	
	SET_PED_RELATIONSHIP_GROUP_HASH(sCoronaCutsConf.pedCloneList[tempLoop], rgFM_AiLike)
	
	SET_ENTITY_INVINCIBLE(sCoronaCutsConf.pedCloneList[tempLoop], TRUE)
	FREEZE_ENTITY_POSITION(sCoronaCutsConf.pedCloneList[tempLoop], TRUE)
	SET_ENTITY_COLLISION(sCoronaCutsConf.pedCloneList[tempLoop], FALSE)
	SET_ENTITY_VISIBLE(sCoronaCutsConf.pedCloneList[tempLoop], FALSE)
	
	FINALIZE_HEAD_BLEND(sCoronaCutsConf.pedCloneList[tempLoop])
	SET_PED_HELMET(sCoronaCutsConf.pedCloneList[tempLoop], FALSE)
	REMOVE_PED_HELMET(sCoronaCutsConf.pedCloneList[tempLoop], TRUE)
	SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedCloneList[tempLoop], PED_COMP_BERD, 0, 0)		// Remove the mask
	
	// Checks if ped is wearing scarf before setting teeth comp to 0 - url:bugstar:2235506
	IF GET_PED_DRAWABLE_VARIATION(sCoronaCutsConf.pedCloneList[tempLoop], PED_COMP_TEETH) = 30
	OR GET_PED_DRAWABLE_VARIATION(sCoronaCutsConf.pedCloneList[tempLoop], PED_COMP_TEETH) = 31
	OR GET_PED_DRAWABLE_VARIATION(sCoronaCutsConf.pedCloneList[tempLoop], PED_COMP_TEETH) = 34
	OR GET_PED_DRAWABLE_VARIATION(sCoronaCutsConf.pedCloneList[tempLoop], PED_COMP_TEETH) = 35
		PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_PEDS - Player is wearing a scarf - removing the scarf for cloned mocap ped")
		SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedCloneList[tempLoop], PED_COMP_TEETH, 0, 0)		// Remove scarf url:bugstar:2220157
	ENDIF
	
	PED_COMP_ITEM_DATA_STRUCT sTempCompData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(sCoronaCutsConf.pedCloneList[tempLoop]), COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[tempLoop])].iMyCurrentHair))
	SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedCloneList[tempLoop], PED_COMP_HAIR, sTempCompData.iDrawable, sTempCompData.iTexture)
	PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_PEDS - Cloning ped with hair: ", GlobalplayerBD[NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[tempLoop])].iMyCurrentHair)		

	SET_FACIAL_IDLE_ANIM_OVERRIDE(sCoronaCutsConf.pedCloneList[tempLoop], GET_PLAYER_MOOD_ANIM_FROM_INDEX(GlobalplayerBD_FM[NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[tempLoop])].iPlayerMood))
	PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_PEDS - Cloning ped with mood: ", GlobalplayerBD_FM[NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[tempLoop])].iPlayerMood)		
		
	CLEAR_PED_PROP(sCoronaCutsConf.pedCloneList[tempLoop], ANCHOR_HEAD)
ENDPROC

PROC SETUP_PED_OUTFIT_FOR_CUTSCENE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, INT tempLoop)
	IF sCoronaCutsConf.iCutsceneId = ciLOW_FLOW_MISSION_HEIST_ISLAND_HS4F_INT
	AND g_sHeistIslandConfig.eApproachVehicle = HIAV_SUBMARINE
		IF !sCoronaCutsConf.bPedOutfitSet[tempLoop]
			MP_OUTFITS_APPLY_DATA 	sApplyData
			sApplyData.pedID 		= sCoronaCutsConf.pedCloneList[tempLoop]
			sApplyData.iPlayerIndex = NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[tempLoop])
			sApplyData.eOutfit 		= OUTFIT_GANGOPS_HEIST_SCUBA_NOTANK
			sApplyData.eApplyStage 	= AOS_SET
			SET_PED_MP_OUTFIT(sApplyData, TRUE)
			sCoronaCutsConf.bPedOutfitSet[tempLoop] = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_PED_OUTFITS_FOR_CUTSCENE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	INT tempLoop
	REPEAT sCoronaCutsConf.iNumParticipants tempLoop
		SETUP_PED_OUTFIT_FOR_CUTSCENE(sCoronaCutsConf, tempLoop)
	ENDREPEAT
ENDPROC

FUNC BOOL DOES_CORONA_CUTSCENE_NEED_NPC_PEDS(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT1
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT2
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_CORONA_CUTSCENE_NPC_PED_MODEL(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, INT iPed)
	MODEL_NAMES modelToLoad = DUMMY_MODEL_FOR_SCRIPT
	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT1
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT2
			SWITCH iPed
				CASE 0
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(sCoronaCutsConf.piJobLeader)
						CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI			
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Karl Abolaji")
							modelToLoad = hc_gunman
						BREAK
						CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA	
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Gustavo Mota")
							modelToLoad = hc_gunman
						BREAK
						CASE CASINO_HEIST_WEAPON_EXPERT__CHARLIE		
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Charlie")
							modelToLoad = INT_TO_ENUM(MODEL_NAMES,HASH("u_m_y_smugmech_01"))
						BREAK
						CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT	
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Chester McCoy (Gunman)")
							modelToLoad = mp_m_weapexp_01
						BREAK
						CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY	
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Patrick McReary")
							modelToLoad = hc_gunman
						BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH GET_PLAYER_CASINO_HEIST_DRIVER(sCoronaCutsConf.piJobLeader)
						CASE CASINO_HEIST_DRIVER__KARIM_DENZ 			
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Karim Denz")
							modelToLoad = hc_driver
						BREAK
						CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ 			
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Taliana Martinez")
							modelToLoad = hc_driver
						BREAK
						CASE CASINO_HEIST_DRIVER__EDDIE_TOH 			
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Eddie Toh")
							modelToLoad = u_m_m_edtoh
						BREAK
						CASE CASINO_HEIST_DRIVER__ZACH 			
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Zach")
							modelToLoad = S_M_Y_XMech_02
						BREAK
						CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT 			
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Chester McCoy (Driver)")
							modelToLoad = mp_m_weapexp_01
						BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH GET_PLAYER_CASINO_HEIST_HACKER(sCoronaCutsConf.piJobLeader)
						CASE CASINO_HEIST_HACKER__RICKIE_LUKENS 			
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Rickie Lukens")
							modelToLoad = hc_hacker
						BREAK
						CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ 			
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Christian Feltz")
							modelToLoad = hc_hacker
						BREAK
						CASE CASINO_HEIST_HACKER__YOHAN 			
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Yohan")
							modelToLoad = INT_TO_ENUM(MODEL_NAMES,HASH("s_m_y_waretech_01"))
						BREAK
						CASE CASINO_HEIST_HACKER__AVI_SCHWARTZMAN 			
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Avi Schwartzman")
							modelToLoad = ig_money
						BREAK
						CASE CASINO_HEIST_HACKER__PAIGE_HARRIS 			
							PRINTLN("GET_CORONA_CUTSCENE_NPC_PED_MODEL - Paige Harris")
							modelToLoad = ig_paige
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN modelToLoad
ENDFUNC

FUNC VECTOR GET_CORONA_CUTSCENE_NPC_PED_SPAWN_COORD(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, INT iPed)
	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT1
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT2
			SWITCH iPed
				CASE 0			RETURN <<2714.122, -355.6775, -56.15743>>
				CASE 1			RETURN <<2710.603, -354.328, -56.162>>
				CASE 2			RETURN <<2713.429, -352.94, -55.19493>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC STRING GET_CORONA_CUTSCENE_NPC_PED_SCENE_HANDLE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, INT iPed)
	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT1
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT2
			SWITCH iPed
				CASE 0		RETURN "gunman_selection_1"
				CASE 1		RETURN "driver_selection_1"
				CASE 2		RETURN "hacker_selection_1"
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC SETUP_NPC_PED_FOR_CUTSCENE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, INT iPed)
	SET_ENTITY_CAN_BE_DAMAGED(sCoronaCutsConf.pedNPC[iPed], FALSE)
	SET_PED_AS_ENEMY(sCoronaCutsConf.pedNPC[iPed], FALSE)
	SET_CURRENT_PED_WEAPON(sCoronaCutsConf.pedNPC[iPed], WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sCoronaCutsConf.pedNPC[iPed], TRUE)
	SET_PED_RESET_FLAG(sCoronaCutsConf.pedNPC[iPed], PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(sCoronaCutsConf.pedNPC[iPed], PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(sCoronaCutsConf.pedNPC[iPed], PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(sCoronaCutsConf.pedNPC[iPed], PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(sCoronaCutsConf.pedNPC[iPed], FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(sCoronaCutsConf.pedNPC[iPed], TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sCoronaCutsConf.pedNPC[iPed], FALSE)
	SET_PED_CAN_RAGDOLL(sCoronaCutsConf.pedNPC[iPed], FALSE)
	SET_PED_CONFIG_FLAG(sCoronaCutsConf.pedNPC[iPed], PCF_DisableExplosionReactions, TRUE)
	CLEAR_PED_TASKS(sCoronaCutsConf.pedNPC[iPed])
	
	SET_ENTITY_INVINCIBLE(sCoronaCutsConf.pedNPC[iPed], TRUE)
//	FREEZE_ENTITY_POSITION(sCoronaCutsConf.pedNPC[iPed], TRUE)
//	SET_ENTITY_VISIBLE(sCoronaCutsConf.pedNPC[iPed], FALSE)
//	SET_ENTITY_COLLISION(sCoronaCutsConf.pedNPC[iPed], FALSE)

	FINALIZE_HEAD_BLEND(sCoronaCutsConf.pedNPC[iPed])
	
	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT1
		CASE ciLOW_FLOW_MISSION_CASINO_HEIST_INT2
			SWITCH iPed
				CASE 0
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(sCoronaCutsConf.piJobLeader)
						CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI
							SET_PED_DEFAULT_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed])
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HEAD,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_BERD,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HAIR,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_TORSO,6,2)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_LEG,6,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_FEET,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL2,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_DECL,1,0)
						BREAK
						CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA
							SET_PED_DEFAULT_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed])
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HEAD,4,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_BERD,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HAIR,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_TORSO,6,1)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_LEG,6,1)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_FEET,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL2,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_DECL,1,0)
						BREAK
						CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY
							SET_PED_DEFAULT_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed])
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HEAD,5,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_BERD,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HAIR,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_TORSO,6,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_LEG,6,2)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_FEET,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL,0,0)
						BREAK
						CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT
							SET_PED_PROP_INDEX(sCoronaCutsConf.pedNPC[iPed], ANCHOR_HEAD, 0, 0)
						BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH GET_PLAYER_CASINO_HEIST_DRIVER(sCoronaCutsConf.piJobLeader)
						CASE CASINO_HEIST_DRIVER__KARIM_DENZ
							SET_PED_DEFAULT_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed])
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HEAD,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HAIR,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_TORSO,4,2)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_LEG,3,2)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_FEET,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL2,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_DECL,1,0)
						BREAK
						CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
							SET_PED_DEFAULT_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed])
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HEAD,2,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HAIR,2,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_TORSO,2,2)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_LEG,2,2)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_FEET,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL2,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_DECL,1,0)
						BREAK
						CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT
							SET_PED_PROP_INDEX(sCoronaCutsConf.pedNPC[iPed], ANCHOR_HEAD, 0, 0)
						BREAK
						CASE CASINO_HEIST_DRIVER__ZACH
							SET_PED_DEFAULT_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed])
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HEAD,1,1)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HAIR,1,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_TORSO,0,1)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_LEG,1,1)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_DECL,1,0)
						BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH GET_PLAYER_CASINO_HEIST_HACKER(sCoronaCutsConf.piJobLeader)
						CASE CASINO_HEIST_HACKER__RICKIE_LUKENS
							SET_PED_DEFAULT_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed])
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HEAD,2,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HAIR,3,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_TORSO,3,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_LEG,3,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HAND,2,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL,0,0)
							SET_PED_PROP_INDEX(sCoronaCutsConf.pedNPC[iPed],ANCHOR_EYES,0,0)
							SET_PED_PROP_INDEX(sCoronaCutsConf.pedNPC[iPed],ANCHOR_EARS,0,0)
						BREAK
						CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ
							SET_PED_DEFAULT_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed])
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HEAD,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HAIR,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_TORSO,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_LEG,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HAND,0,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL,0,0)
						BREAK
						CASE CASINO_HEIST_HACKER__YOHAN
							SET_PED_DEFAULT_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed])
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_HEAD,5,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_TORSO,5,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_LEG,5,0)
							SET_PED_COMPONENT_VARIATION(sCoronaCutsConf.pedNPC[iPed],PED_COMP_SPECIAL,4,0)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC


// PURPOSE:	
//    Make clones of the players that are to appear in the cutscene
// RETURN VALUE:			
//    BOOL - TRUE when all the clones are created, otherwise FALSE
FUNC BOOL CREATE_CORONA_CUTSCENE_PEDS(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	INT tempLoop = 0
	PED_INDEX tempPed
	
	REPEAT sCoronaCutsConf.iNumParticipants tempLoop
	
		IF (sCoronaCutsConf.pidPlayerList[tempLoop] != INVALID_PLAYER_INDEX())
			IF (IS_NET_PLAYER_OK(sCoronaCutsConf.pidPlayerList[tempLoop], FALSE))
				
				IF (sCoronaCutsConf.pedCloneList[tempLoop] = NULL)
					IF NOT (DOES_ENTITY_EXIST(sCoronaCutsConf.pedCloneList[tempLoop]))
					
						PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_PEDS - Requesting Ped for Player in array: ", tempLoop)
						tempPed = GET_PLAYER_PED(sCoronaCutsConf.pidPlayerList[tempLoop])

						sCoronaCutsConf.pedCloneList[tempLoop] = CLONE_PED(tempPed, FALSE, FALSE, FALSE)					
						
						SETUP_PED_FOR_CUTSCENE(sCoronaCutsConf, tempLoop, FALSE)
										
						// Only adding return false here because the mission Controller template does this - it may be that only one ped can be cloned in a frame
						RETURN FALSE
						
					ENDIF
					
				ELSE
					
					IF DOES_ENTITY_EXIST(sCoronaCutsConf.pedCloneList[tempLoop])
					AND NOT IS_ENTITY_DEAD(sCoronaCutsConf.pedCloneList[tempLoop])
					
						INT iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(sCoronaCutsConf.pedCloneList[tempLoop]), ENUM_TO_INT(PED_COMP_BERD), GET_PED_DRAWABLE_VARIATION(sCoronaCutsConf.pedCloneList[tempLoop], PED_COMP_BERD), GET_PED_TEXTURE_VARIATION(sCoronaCutsConf.pedCloneList[tempLoop], PED_COMP_BERD))
							
						//force normal head
						IF sCoronaCutsConf.pidPlayerList[tempLoop] = PLAYER_ID()
						AND NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_SHRINK_HEAD, ENUM_TO_INT(SHOP_PED_COMPONENT)) 
							RESET_PLAYER_HEAD_BLEND_TO_NORM(sCoronaCutsConf.pedCloneList[tempLoop])
						ELSE
							IF NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_SHRINK_HEAD, ENUM_TO_INT(SHOP_PED_COMPONENT)) 
								IF NETWORK_HAS_CACHED_PLAYER_HEAD_BLEND_DATA(sCoronaCutsConf.pidPlayerList[tempLoop])
									IF NOT NETWORK_APPLY_CACHED_PLAYER_HEAD_BLEND_DATA(sCoronaCutsConf.pedCloneList[tempLoop], sCoronaCutsConf.pidPlayerList[tempLoop])
										PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_PEDS - Waiting to apply head-blend from cache. Index: ", tempLoop)
										RETURN FALSE
									ELSE
										PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_PEDS - Cached head blend applied successfully")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF sCoronaCutsConf.iCutsceneId = ciLOW_FLOW_MISSION_CASINO_HEIST_INT2
							SETUP_PED_FOR_CUTSCENE(sCoronaCutsConf, tempLoop, FALSE)
						ENDIF
					ELSE
						IF IS_ENTITY_DEAD(sCoronaCutsConf.pedCloneList[tempLoop])
							PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_PEDS - Failed head-blend, entity: ",NATIVE_TO_INT(sCoronaCutsConf.pedCloneList[tempLoop])," is dead.")
						ELSE
							PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_PEDS - Failed head-blend, entity: ",NATIVE_TO_INT(sCoronaCutsConf.pedCloneList[tempLoop])," does not exist.")
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	IF DOES_CORONA_CUTSCENE_NEED_NPC_PEDS(sCoronaCutsConf)
		tempLoop = 0
		
		REPEAT sCoronaCutsConf.iNumNPCs tempLoop
			IF NOT (DOES_ENTITY_EXIST(sCoronaCutsConf.pedNPC[tempLoop]))
				MODEL_NAMES tempModel = GET_CORONA_CUTSCENE_NPC_PED_MODEL(sCoronaCutsConf, tempLoop)
				PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_PEDS - Attempt to load model ", ENUM_TO_INT(tempModel), " for NPC ped ", tempLoop)
				IF REQUEST_LOAD_MODEL(tempModel)
				
					PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_PEDS - creating new ped for NPC: ", tempLoop)

					sCoronaCutsConf.pedNPC[tempLoop] = CREATE_PED(PEDTYPE_MISSION, tempModel, GET_CORONA_CUTSCENE_NPC_PED_SPAWN_COORD(sCoronaCutsConf, tempLoop), DEFAULT, FALSE, FALSE)				
					
					SETUP_NPC_PED_FOR_CUTSCENE(sCoronaCutsConf, tempLoop)
									
					// Create one ped per frame, avoid borking it
					RETURN FALSE
					
				ELSE
				
					PRINTLN("[CORONA][CUTS] - CREATE_CORONA_CUTSCENE_PEDS - Waiting for NPC ped ", tempLoop, " model to load...")
					RETURN FALSE
					
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Set the cutscene entity stream flags for a specific ped.
///    Useful for dynamic custscele pool sizes, can be iterated over using a loop.
///    MAXIMUM SUPPORTED: 4
/// PARAMS:
///    iPedIndex - index relating to streaming flag (e.g. 0 = MP_1, 1 = MP_2, etc).
PROC SET_STREAM_FLAG_FOR_CORONA_CUTSCENE_PED(INT iPedIndex)
	SWITCH iPedIndex
		CASE 0 SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS) BREAK
		CASE 1 SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS) BREAK
		CASE 2 SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_3", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS) BREAK
		CASE 3 SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_4", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS) BREAK
		CASE 4 SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_5", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS) BREAK
		CASE 5 SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_6", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS) BREAK
		CASE 6 SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_7", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS) BREAK
		CASE 7 SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_8", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS) BREAK
	ENDSWITCH
ENDPROC

PROC SET_STREAM_FLAG_FOR_CORONA_CUTSCENE_NPC_PED(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, INT iPedIndex)
	TEXT_LABEL_23 pedHandle = GET_CORONA_CUTSCENE_NPC_PED_SCENE_HANDLE(sCoronaCutsConf, iPedIndex)
	SET_CUTSCENE_ENTITY_STREAMING_FLAGS(pedHandle, DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
ENDPROC

//	Configure Vehicles
PROC REGISTER_CORONA_CUTSCENE_VEHICLES(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	IF sCoronaCutsConf.iNumVehicles > 0
		INT i
		REPEAT sCoronaCutsConf.iNumVehicles i
		
			IF NATIVE_TO_INT(sCoronaCutsConf.vehList[i]) >= 0
				IF DOES_ENTITY_EXIST(sCoronaCutsConf.vehList[i])
				AND IS_VEHICLE_DRIVEABLE(sCoronaCutsConf.vehList[i], TRUE)
					REGISTER_ENTITY_FOR_CUTSCENE(sCoronaCutsConf.vehList[i], sCoronaCutsConf.vehHandle[i], CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_NONE)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, sCoronaCutsConf.vehHandle[i], CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_NONE)
				ENDIF
			ENDIF
		
		ENDREPEAT
	ENDIF
ENDPROC


/// PURPOSE:
///    Register the cutscene clones to a specific entity ID within the cutscene itself.
///    MAXIMUM SUPPORTED: 4
PROC REGISTER_CORONA_CUTSCENE_PEDS(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	INT tempLoop, iCloneCount, index
	BOOL registerThisPlayer = FALSE
	TEXT_LABEL_23 cutscenePlayerID = ""
	
	iCloneCount = sCoronaCutsConf.iNumParticipants
	
	IF sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_BOTH_CLONES
		iCloneCount += iCloneCount
	ENDIF
	
	PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - Registering (",iCloneCount,") peds for cutscene. bTeamSpecificMovers: ", sCoronaCutsConf.bTeamSpecificMovers)
	
	REPEAT sCoronaCutsConf.iNumParticipants tempLoop
		IF sCoronaCutsConf.pidPlayerList[tempLoop] != INVALID_PLAYER_INDEX()
			PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - Cutscene ped ", tempLoop, " is a clone of ", GET_PLAYER_NAME(sCoronaCutsConf.pidPlayerList[tempLoop]))
		ELSE
			PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - Cutscene ped ", tempLoop, " is a clone of an invalid player index")
		ENDIF
	ENDREPEAT

	REPEAT sCoronaCutsConf.iNumParticipants tempLoop
	
		registerThisPlayer = FALSE
	
		IF sCoronaCutsConf.bTeamSpecificMovers
		
			IF (NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[tempLoop]) >= 0)
			AND (NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[tempLoop]) < NUM_NETWORK_PLAYERS)
		
				INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[tempLoop])].sClientCoronaData.iTeamChosen
				
				IF iTeam > INVALID_CUTSCENE_DATA
				
					FOR index = 0 TO (sCoronaCutsConf.iNumParticipants-1)
					
						IF sCoronaCutsConf.iMoverTeam[index] > INVALID_CUTSCENE_DATA
							IF iTeam = sCoronaCutsConf.iMoverTeam[index]
								cutscenePlayerID = sCoronaCutsConf.tlMoverName[index]				
								sCoronaCutsConf.tlMoverName[index] = ""
								sCoronaCutsConf.iMoverTeam[index] = INVALID_CUTSCENE_DATA
								
								index = (sCoronaCutsConf.iNumParticipants-1) // Break out of loop.
								PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - Found matching team data, snatching mover ID: ", iTeam, ", Value: ", cutscenePlayerID)
							ELSE
								PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - iTeamChosen(",iTeam,") != MoverTeam[",index,"](",sCoronaCutsConf.iMoverTeam[index],")")
							ENDIF
						ELSE
							PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - Mover already snatched: ", index)
						ENDIF
					
					ENDFOR
					
				ELSE
					PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - iTeamChosen is INVALID, bypass cutscene member: ", tempLoop)
				ENDIF
				
			ELSE
				PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - Player ",tempLoop," is INVALID, bypass this iteration. Value: ", NATIVE_TO_INT(sCoronaCutsConf.pidPlayerList[tempLoop]))
			ENDIF

		ELSE
		
			SWITCH (tempLoop)
				DEFAULT
					SCRIPT_ASSERT("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - ERROR: Illegal Corona Cuts Player ID. Tell Alastair/James.")
					PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - ERROR: Illegal Corona Cuts Player ID: ", tempLoop, ", MaxParts: ", sCoronaCutsConf.iNumParticipants)
				BREAK

				CASE 0		cutscenePlayerID = "MP_1"		BREAK
				CASE 1		cutscenePlayerID = "MP_2"		BREAK
				CASE 2		cutscenePlayerID = "MP_3"		BREAK
				CASE 3		cutscenePlayerID = "MP_4"		BREAK

			ENDSWITCH
		
		ENDIF
		
		IF NOT (sCoronaCutsConf.pedCloneList[tempLoop] = NULL)
			IF (DOES_ENTITY_EXIST(sCoronaCutsConf.pedCloneList[tempLoop]))
				// Register this player for the cutscene
				IF NOT (IS_PED_INJURED(sCoronaCutsConf.pedCloneList[tempLoop]))
					registerThisPlayer = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF (registerThisPlayer)
			
			// Ensure the ped is fully setup
			IF sCoronaCutsConf.bDelayPedConfig
				SETUP_PED_FOR_CUTSCENE(sCoronaCutsConf, tempLoop, TRUE)
			ENDIF
		
			// Register this player		
			REGISTER_ENTITY_FOR_CUTSCENE(sCoronaCutsConf.pedCloneList[tempLoop], cutscenePlayerID, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME | CEO_PRESERVE_BODY_BLOOD_DAMAGE | CEO_PRESERVE_FACE_BLOOD_DAMAGE)
			PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - Registered Player for Cutscene: ", cutscenePlayerID, " in slot: ", tempLoop)
			
			IF (sCoronaCutsConf.pidPlayerList[tempLoop] = PLAYER_ID())
				IF NOT NETWORK_IS_IN_MP_CUTSCENE()
					NETWORK_SET_IN_MP_CUTSCENE(TRUE)
					PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - Setting local player as IN MP Cutscene: ", cutscenePlayerID)
				ELSE
					PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - Not setting local player as IN MP Cutscene as we are already in one.")
				ENDIF
			ENDIF
		ELSE
			// No player to register
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, cutscenePlayerID, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME | CEO_PRESERVE_BODY_BLOOD_DAMAGE | CEO_PRESERVE_FACE_BLOOD_DAMAGE)
			PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - Player not required for cutscene: ", cutscenePlayerID)
		ENDIF
		
	ENDREPEAT
	
	IF sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_BOTH_CLONES
	
		REPEAT sCoronaCutsConf.iNumParticipants tempLoop
	
			registerThisPlayer = FALSE
			
			SWITCH (tempLoop)
				DEFAULT
					SCRIPT_ASSERT("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - BOTH - ERROR: Illegal Corona Cuts Player ID. Tell Alastair/James.")
					PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - BOTH - ERROR: Illegal Corona Cuts Player ID: ", tempLoop, ", MaxParts: ", sCoronaCutsConf.iNumParticipants)
				BREAK

				CASE 0		cutscenePlayerID = "MP_5"		BREAK
				CASE 1		cutscenePlayerID = "MP_6"		BREAK
				CASE 2		cutscenePlayerID = "MP_7"		BREAK
				CASE 3		cutscenePlayerID = "MP_8"		BREAK

			ENDSWITCH
			
			IF NOT (sCoronaCutsConf.pedCoronaCloneList[tempLoop] = NULL)
				IF (DOES_ENTITY_EXIST(sCoronaCutsConf.pedCoronaCloneList[tempLoop]))
					// Register this player for the cutscene
					IF NOT (IS_PED_INJURED(sCoronaCutsConf.pedCoronaCloneList[tempLoop]))
						registerThisPlayer = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF (registerThisPlayer)
				// Register this player
				REGISTER_ENTITY_FOR_CUTSCENE(sCoronaCutsConf.pedCoronaCloneList[tempLoop], cutscenePlayerID, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME | CEO_PRESERVE_BODY_BLOOD_DAMAGE | CEO_PRESERVE_FACE_BLOOD_DAMAGE)
				PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - BOTH - Registered Player for Cutscene: ", cutscenePlayerID, " in slot: ", tempLoop)
			ELSE
				// No player to register
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, cutscenePlayerID, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME | CEO_PRESERVE_BODY_BLOOD_DAMAGE | CEO_PRESERVE_FACE_BLOOD_DAMAGE)
				PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - BOTH - Player not required for cutscene: ", cutscenePlayerID)
			ENDIF
			
		ENDREPEAT
	
	ENDIF
	
	IF DOES_CORONA_CUTSCENE_NEED_NPC_PEDS(sCoronaCutsConf)
	
		REPEAT sCoronaCutsConf.iNumNPCs tempLoop
	
			IF (DOES_ENTITY_EXIST(sCoronaCutsConf.pedNPC[tempLoop]))
			AND NOT (IS_PED_INJURED(sCoronaCutsConf.pedNPC[tempLoop]))
		
				cutscenePlayerID = GET_CORONA_CUTSCENE_NPC_PED_SCENE_HANDLE(sCoronaCutsConf, tempLoop)

				// Register this player
				REGISTER_ENTITY_FOR_CUTSCENE(sCoronaCutsConf.pedNPC[tempLoop], cutscenePlayerID, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - NPC - Registered NPC for Cutscene: ", cutscenePlayerID, " in slot: ", tempLoop)
		
			ELSE
				PRINTLN("[CORONA][CUTS] - REGISTER_CORONA_CUTSCENE_PEDS - NPC - ped ", tempLoop, " either doesnt exist or is dead")
			
			ENDIF
			
		ENDREPEAT
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Set the visibility state of all corona cutscene clone peds.
/// PARAMS:
///    bVisible - TRUE = visible.
PROC SET_CORONA_CUTSCENE_PED_VISIBILITY(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, BOOL bVisible)

	INT tempLoop
	
	REPEAT sCoronaCutsConf.iNumParticipants tempLoop
		IF NOT (sCoronaCutsConf.pedCloneList[tempLoop] = NULL)
			IF (DOES_ENTITY_EXIST(sCoronaCutsConf.pedCloneList[tempLoop]))
				PRINTLN("[CORONA][CUTS] - SET_CORONA_CUTSCENE_PED_VISIBILITY - PLAYER - Setting ped index: ",tempLoop,"'s visibility to: ", bVisible)
				SET_ENTITY_VISIBLE(sCoronaCutsConf.pedCloneList[tempLoop], bVisible)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_BOTH_CLONES
		REPEAT sCoronaCutsConf.iNumParticipants tempLoop
			IF NOT (sCoronaCutsConf.pedCoronaCloneList[tempLoop] = NULL)
				IF (DOES_ENTITY_EXIST(sCoronaCutsConf.pedCoronaCloneList[tempLoop]))
					PRINTLN("[CORONA][CUTS] - SET_CORONA_CUTSCENE_PED_VISIBILITY - CORONA - Setting ped index: ",tempLoop,"'s visibility to: ", bVisible)
					SET_ENTITY_VISIBLE(sCoronaCutsConf.pedCoronaCloneList[tempLoop], bVisible)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	sCoronaCutsConf.bPedsVisible = TRUE
	
ENDPROC

PROC SET_CORONA_CUTSCENE_VEHICLE_VISIBILITY(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, BOOL bVisible)

	INT tempLoop
	
	REPEAT sCoronaCutsConf.iNumVehicles tempLoop
		IF DOES_ENTITY_EXIST(sCoronaCutsConf.vehList[tempLoop])
			PRINTLN("[CORONA][CUTS] - SET_CORONA_CUTSCENE_VEHICLE_VISIBILITY - Setting vehicle index: ",tempLoop,"'s visibility to: ", bVisible)
			SET_ENTITY_VISIBLE(sCoronaCutsConf.vehList[tempLoop], bVisible)
		ENDIF
	ENDREPEAT

	sCoronaCutsConf.bVehsVisible = TRUE

ENDPROC


PROC CLEAN_CORONA_CUTSCENE_DATA(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	sCoronaCutsConf.iCutsceneId = INVALID_CUTSCENE_DATA
	sCoronaCutsConf.eState = CORONA_CUTSCENE_NONE
	sCoronaCutsConf.eSubState = CORONA_CUTSCENE_SUBSTATE_TRIGGER
	
	sCoronaCutsConf.tlName = ""
	sCoronaCutsConf.vPosition = <<0,0,0>>
	sCoronaCutsConf.vRotation = <<0,0,0>>
	sCoronaCutsConf.iNumParticipants = 0
	sCoronaCutsConf.bSkip = FALSE
	sCoronaCutsConf.iCloneSource = 0
	sCoronaCutsConf.bPedsVisible = FALSE
	sCoronaCutsConf.bTeamSpecificMovers = FALSE
	sCoronaCutsConf.bKilledRadio = FALSE
	sCoronaCutsConf.bVehsVisible = FALSE
	sCoronaCutsConf.iNumVehicles = 0
	
	INT index
	FOR index = 0 TO (MAX_CORONA_CUTSCENE_VEHICLES-1)
		sCoronaCutsConf.vehHandle[index] = ""
		sCoronaCutsConf.vehNumPlate[index] = ""
		IF sCoronaCutsConf.vehModel[index] != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(sCoronaCutsConf.vehModel[index])
		ENDIF
	ENDFOR
	
	index = 0
	FOR index = 0 TO (MAX_CORONA_CUTSCENE_PARTICIPANTS-1)
		sCoronaCutsConf.pidPlayerList[index] = INVALID_PLAYER_INDEX()
		sCoronaCutsConf.pedCloneList[index] = NULL
		sCoronaCutsConf.pedCoronaCloneList[index] = NULL
		sCoronaCutsConf.tlMoverName[index] = ""
		sCoronaCutsConf.iMoverTeam[index] = INVALID_CUTSCENE_DATA
		sCoronaCutsConf.bPedOutfitSet[index] = FALSE
	ENDFOR
	
	sCoronaCutsConf.iInteriorLoadStage = ciCORONA_CUTSCENE_INTERIOR_REQUEST
	RESET_NET_TIMER(sCoronaCutsConf.stInteriorLoadTimer)
	sCoronaCutsConf.iInteriorIndex = NULL
	
ENDPROC

FUNC BOOL SHOULD_LOAD_CUTSCENE_VEHICLES(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	
	IF sCoronaCutsConf.iNumVehicles = 0
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC	

/// PURPOSE:
///    Confirms if the models for the cutscene with the passed configuration have been loaded
/// PARAMS:
///    sCoronaCutsConf - the cutscene configuration
/// RETURNS:
///    TRUE when loaded
FUNC BOOL HAVE_CUTSCENE_VEHICLE_MODELS_LOADED(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	INT i
	REPEAT sCoronaCutsConf.iNumVehicles i
		REQUEST_MODEL(sCoronaCutsConf.vehModel[i])
		IF NOT HAS_MODEL_LOADED(sCoronaCutsConf.vehModel[i])
			PRINTLN("[CORONA][CUTS] - HAVE_CUTSCENE_VEHICLE_MODELS_LOADED - model for vehicle ", i, " (", GET_MODEL_NAME_FOR_DEBUG(sCoronaCutsConf.vehModel[i]), ") has not loaded.")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	PRINTLN("[CORONA][CUTS] - HAVE_CUTSCENE_VEHICLE_MODELS_LOADED - models have loaded")
	RETURN TRUE
ENDFUNC

PROC SET_VEHICLE_MODS_FOR_CUTSCENE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, VEHICLE_INDEX& thisVeh)
	INT preset
	
	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_DRIVE_BY_SHOOTOUT
			IF DOES_ENTITY_EXIST(thisVeh)
				SET_VEHICLE_MOD_KIT(thisVeh, 0)
				
				// Buccaneer
				IF GET_ENTITY_MODEL(thisVeh) = BUCCANEER2
					preset = 1
					FMMC_SET_VEH_MOD_PRESET(thisVeh, BUCCANEER2, preset)
					SET_VEHICLE_MOD(thisVeh, MOD_LIVERY, 0)
					SET_VEHICLE_COLOURS(thisVeh, 89, 89)
					SET_VEHICLE_EXTRA(thisVeh, 1, TRUE)
					SET_VEHICLE_EXTRA(thisVeh, 2, TRUE)
					SET_VEHICLE_EXTRA(thisVeh, 3, TRUE)
					SET_VEHICLE_EXTRA(thisVeh, 4, FALSE)
					SET_VEHICLE_DIRT_LEVEL(thisVeh, 0)
				ENDIF
				
				// Chino
				IF GET_ENTITY_MODEL(thisVeh) = CHINO2
					preset = 2
					FMMC_SET_VEH_MOD_PRESET(thisVeh, CHINO2, preset)
					SET_VEHICLE_COLOURS(thisVeh, 145, 145)
					SET_VEHICLE_EXTRA(thisVeh, 1, TRUE)
					SET_VEHICLE_EXTRA(thisVeh, 2, TRUE)
					SET_VEHICLE_EXTRA(thisVeh, 3, FALSE)
					SET_VEHICLE_EXTRA_COLOURS(thisVeh, 0, 0)
					SET_VEHICLE_DIRT_LEVEL(thisVeh, 0)
				ENDIF
			ENDIF
		BREAK
		
		CASE ciLOW_FLOW_MISSION_TRANSPORT_LOWRIDERS
			IF DOES_ENTITY_EXIST(thisVeh)
				SET_VEHICLE_MOD_KIT(thisVeh, 0)
				
				// Voodoo
				IF GET_ENTITY_MODEL(thisVeh) = VOODOO
					SET_VEHICLE_WHEEL_TYPE(thisVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, 2))
					SET_VEHICLE_MOD(thisVeh, MOD_BUMPER_F, 0)
					SET_VEHICLE_MOD(thisVeh, MOD_EXHAUST, 1)
					SET_VEHICLE_MOD(thisVeh, MOD_GRILL, 2)
					SET_VEHICLE_MOD(thisVeh, MOD_ENGINE, 3)
					SET_VEHICLE_MOD(thisVeh, MOD_BRAKES, 2)
					SET_VEHICLE_MOD(thisVeh, MOD_GEARBOX, 2)
					TOGGLE_VEHICLE_MOD(thisVeh, MOD_TOGGLE_XENON_LIGHTS,  TRUE)
					SET_VEHICLE_MOD(thisVeh, MOD_WHEELS, 18, TRUE)
					SET_VEHICLE_MOD(thisveh, MOD_REAR_WHEELS, 3)
					SET_VEHICLE_MOD(thisVeh, MOD_PLTHOLDER, 7)
					SET_VEHICLE_MOD(thisVeh, MOD_PLTVANITY, 5)
					SET_VEHICLE_MOD(thisveh, MOD_INTERIOR1, 9)
					SET_VEHICLE_MOD(thisVeh, MOD_INTERIOR2, 9)
					SET_VEHICLE_MOD(thisVeh, MOD_STEERING, 11)
					SET_VEHICLE_MOD(thisVeh, MOD_KNOB, 2)
					SET_VEHICLE_MOD(thisVeh, MOD_PLAQUE, 3)
					SET_VEHICLE_MOD(thisVeh, MOD_TRUNK, 1)
					SET_VEHICLE_MOD(thisVeh, MOD_HYDRO, 4)
					SET_VEHICLE_MOD(thisVeh, MOD_ENGINEBAY1, 3)
					SET_VEHICLE_MOD(thisVeh, MOD_ENGINEBAY2, 1)
					SET_VEHICLE_MOD(thisVeh, MOD_CHASSIS2, 0)
					SET_VEHICLE_MOD(thisVeh, MOD_CHASSIS3, 1)
					SET_VEHICLE_MOD(thisVeh, MOD_CHASSIS5, 1)
					SET_VEHICLE_MOD(thisVeh, MOD_LIVERY, 0)					
					SET_VEHICLE_COLOURS(thisVeh, 27, 27)
					SET_VEHICLE_EXTRA_COLOUR_5(thisVeh, 91)
					SET_VEHICLE_DIRT_LEVEL(thisVeh, 0)
				ENDIF
				
				// Faction2
				IF GET_ENTITY_MODEL(thisVeh) = FACTION2
					SET_VEHICLE_WHEEL_TYPE(thisVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, 2))
					SET_VEHICLE_MOD(thisVeh, MOD_BONNET, 0)
					SET_VEHICLE_MOD(thisVeh, MOD_WHEELS, 23, TRUE)
					SET_VEHICLE_MOD(thisveh, MOD_REAR_WHEELS, 2)
					SET_VEHICLE_MOD(thisVeh, MOD_PLTHOLDER, 4)
					SET_VEHICLE_MOD(thisVeh, MOD_PLTVANITY, 9)
					SET_VEHICLE_MOD(thisveh, MOD_INTERIOR1, 3)
					SET_VEHICLE_MOD(thisVeh, MOD_INTERIOR2, 15)
					SET_VEHICLE_MOD(thisVeh, MOD_INTERIOR4, 12)
					SET_VEHICLE_MOD(thisVeh, MOD_STEERING, 3)
					SET_VEHICLE_MOD(thisVeh, MOD_SUSPENSION, 3)
					SET_VEHICLE_MOD(thisVeh, MOD_KNOB, 0)
					SET_VEHICLE_MOD(thisVeh, MOD_PLAQUE, 11)
					SET_VEHICLE_MOD(thisVeh, MOD_ICE, 1)
					SET_VEHICLE_MOD(thisVeh, MOD_TRUNK, 0)
					SET_VEHICLE_MOD(thisVeh, MOD_HYDRO, 3)
					SET_VEHICLE_MOD(thisVeh, MOD_ENGINEBAY1, 3)
					SET_VEHICLE_MOD(thisVeh, MOD_ENGINEBAY2, 4)
					SET_VEHICLE_MOD(thisVeh, MOD_LIVERY, 5)					
					TOGGLE_VEHICLE_MOD(thisVeh, MOD_TOGGLE_XENON_LIGHTS, TRUE)
					SET_VEHICLE_COLOURS(thisVeh, 0, 0)
					SET_VEHICLE_EXTRA_COLOUR_5(thisVeh, 137)
					SET_VEHICLE_DIRT_LEVEL(thisVeh, 0)
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Confirms if the vehicles for the cutscene with the passed configuration have been created
/// PARAMS:
///    sCoronaCutsConf - the cutscene configuration
/// RETURNS:
///    TRUE when all vehicles exist
FUNC BOOL HAVE_CUTSCENE_VEHICLES_BEEN_CREATED(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	INT i
	REPEAT sCoronaCutsConf.iNumVehicles i
		IF NOT DOES_ENTITY_EXIST(sCoronaCutsConf.vehList[i])
			sCoronaCutsConf.vehList[i] = CREATE_VEHICLE(sCoronaCutsConf.vehModel[i], g_FMMC_STRUCT.vStartPos, 0.0, FALSE, FALSE)
			PRINTLN("[CORONA][CUTS] - HAVE_CUTSCENE_VEHICLES_BEEN_CREATED - vehicle ", i, " (", GET_MODEL_NAME_FOR_DEBUG(sCoronaCutsConf.vehModel[i]), ") is being created.", NATIVE_TO_INT(sCoronaCutsConf.vehList[i]))
			
			FREEZE_ENTITY_POSITION(sCoronaCutsConf.vehList[i], TRUE)
			SET_ENTITY_DYNAMIC(sCoronaCutsConf.vehList[i], FALSE)
			SET_ENTITY_COLLISION(sCoronaCutsConf.vehList[i], FALSE)
			SET_VEHICLE_ENGINE_ON(sCoronaCutsConf.vehList[i], FALSE, TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sCoronaCutsConf.vehList[i], TRUE)				
			SET_ENTITY_INVINCIBLE(sCoronaCutsConf.vehList[i], TRUE)
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(sCoronaCutsConf.vehList[i], FALSE)
			SET_VEHICLE_ON_GROUND_PROPERLY(sCoronaCutsConf.vehList[i])
			SET_ENTITY_VISIBLE(sCoronaCutsConf.vehList[i], FALSE)
			
			SET_VEHICLE_MODS_FOR_CUTSCENE(sCoronaCutsConf, sCoronaCutsConf.vehList[i])
			
//			SET_VEHICLE_NUMBER_PLATE_TEXT(sCoronaCutsConf.vehModel[i], sCoronaCutsConf.vehNumPlate[i])
						
			// Returning false here so that one vehicle is made per frame, until all vehicles exist
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	PRINTLN("[CORONA][CUTS] - HAVE_CUTSCENE_VEHICLES_BEEN_CREATED - vehicles have been created")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates vehicles to be used in a cutscene
/// PARAMS:
///    sCoronaCutsConf - 
/// RETURNS:
///    TRUE when vehicles have been created
FUNC BOOL CREATE_CUTSCENE_VEHICLES(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	SWITCH sCoronaCutsConf.eCreateVehSubstate
		CASE CORONA_CUTSCENE_VEHICLE_CREATION_SUBSTATE_LOAD_MODELS
			INT i
			REPEAT sCoronaCutsConf.iNumVehicles i
				PRINTLN("[CORONA][CUTS] - CREATE_CUTSCENE_VEHICLES - requesting ", GET_MODEL_NAME_FOR_DEBUG(sCoronaCutsConf.vehModel[i]), " model.")
				REQUEST_MODEL(sCoronaCutsConf.vehModel[i])
			ENDREPEAT
			
			PRINTLN("[CORONA][CUTS] - CREATE_CUTSCENE_VEHICLES - vehicle models have been requested, moving to ** CORONA_CUTSCENE_VEHICLE_CREATION_SUBSTATE_CREATE_VEHICLES ** ")
			sCoronaCutsConf.eCreateVehSubstate = CORONA_CUTSCENE_VEHICLE_CREATION_SUBSTATE_CREATE_VEHICLES
		BREAK
		
		CASE CORONA_CUTSCENE_VEHICLE_CREATION_SUBSTATE_CREATE_VEHICLES
			IF HAVE_CUTSCENE_VEHICLE_MODELS_LOADED(sCoronaCutsConf)
				IF HAVE_CUTSCENE_VEHICLES_BEEN_CREATED(sCoronaCutsConf)
					PRINTLN("[CORONA][CUTS] - CREATE_CUTSCENE_VEHICLES - vehicles created, moving to ** CORONA_CUTSCENE_VEHICLE_CREATION_SUBSTATE_FINISHED ** ")
					sCoronaCutsConf.eCreateVehSubstate = CORONA_CUTSCENE_VEHICLE_CREATION_SUBSTATE_FINISHED
				ELSE
					PRINTLN("[CORONA][CUTS] - CREATE_CUTSCENE_VEHICLES - waiting for all vehicle to be created/exist..")
				ENDIF
			ENDIF
		BREAK
		
		CASE CORONA_CUTSCENE_VEHICLE_CREATION_SUBSTATE_FINISHED
			RETURN TRUE
		BREAK
	ENDSWITCH
		
	RETURN FALSE
ENDFUNC

PROC SET_CUTSCENE_ID(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	IF sCoronaCutsConf.iCutsceneId = INVALID_CUTSCENE_DATA

		INT index

		REPEAT ciMAX_ROCKSTAR_FLOW_MISSIONS index
			//If it's one of the lowrider missions
			IF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iLowFlowMission[index]
			OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iLowFlowPartTwoMission[index]
				sCoronaCutsConf.iCutsceneId = index
				PRINTLN("[CORONA][CUTS] - SET_CUTSCENE_ID - Lowrider flow cutscene. ID: ", sCoronaCutsConf.iCutsceneId)
			ENDIF	
		ENDREPEAT
		
		REPEAT ciVCM_MAX index
			IF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iVinewoodCasinoStoryMissionRootContentID[index]
				sCoronaCutsConf.iCutsceneId = GET_VCM_FLOW_CUTSCENE_INDEX(index)
				PRINTLN("[CORONA][CUTS] - SET_CUTSCENE_ID - VCM flow cutscene. ID: ", sCoronaCutsConf.iCutsceneId)
			ENDIF
		ENDREPEAT
		
		#IF FEATURE_CASINO_HEIST
		IF IS_THIS_MISSION_A_CASINO_HEIST_INTRO_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
			IF NOT IS_CORONA_BIT_SET(CORONA_CASINO_HEIST_CUTSCENE_1_VIEWED)
				sCoronaCutsConf.iCutsceneId = ciLOW_FLOW_MISSION_CASINO_HEIST_INT1
			ELSE
				sCoronaCutsConf.iCutsceneId = ciLOW_FLOW_MISSION_CASINO_HEIST_INT2
			ENDIF
			PRINTLN("[CORONA][CUTS] - SET_CUTSCENE_ID - Casino Heist cutscene. ID: ", sCoronaCutsConf.iCutsceneId)
		ENDIF
		#ENDIF
		
		#IF FEATURE_HEIST_ISLAND
		IF IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
			sCoronaCutsConf.iCutsceneId = ciLOW_FLOW_MISSION_HEIST_ISLAND_HS4F_INT
			PRINTLN("[CORONA][CUTS] - SET_CUTSCENE_ID - Heist Island cutscene. ID: ", sCoronaCutsConf.iCutsceneId)
		ENDIF
		#ENDIF
		
	ENDIF

ENDPROC

FUNC BOOL DOES_CUTSCENE_REQUIRE_INTERIOR(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	IF IS_THIS_A_VCM_CUTSCENE(sCoronaCutsConf)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL LOAD_CUTSCENE_INTERIOR(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	IF DOES_CUTSCENE_REQUIRE_INTERIOR(sCoronaCutsConf)
		
		BOOL bLoaded = TRUE
		
		SWITCH sCoronaCutsConf.iInteriorLoadStage
			CASE ciCORONA_CUTSCENE_INTERIOR_REQUEST
								
				IF IS_THIS_A_VCM_CUTSCENE(sCoronaCutsConf)
					sCoronaCutsConf.vPedCachedStartPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					sCoronaCutsConf.fPedCachedStartHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1111.0322, 245.8896, -46.8410>>)
					PRINTLN("[CORONA][CUTS] - LOAD_CUTSCENE_INTERIOR - Warped player inside casino to load interior. Cached coord: ", sCoronaCutsConf.vPedCachedStartPos)
					REQUEST_RELAUCHING_OF_SIMPLE_INTERIOR_INT_SCRIPT(SIMPLE_INTERIOR_CASINO)
					g_SimpleInteriorData.bAllowInteriorScriptForActivitySession = TRUE
				ENDIF
				
				sCoronaCutsConf.iInteriorLoadStage = ciCORONA_CUTSCENE_INTERIOR_WAIT_FOR_LOAD
				PRINTLN("[CORONA][CUTS] - LOAD_CUTSCENE_INTERIOR - sCoronaCutsConf.iInteriorLoadStage = ciCORONA_CUTSCENE_INTERIOR_WAIT_FOR_LOAD")
			BREAK
			
			CASE ciCORONA_CUTSCENE_INTERIOR_WAIT_FOR_LOAD
				
				IF sCoronaCutsConf.iCutsceneId = ciLOW_FLOW_MISSION_VCM_BRAWL
				AND NOT g_bInitCasinoPedsCreated
					PRINTLN("[CORONA][CUTS] - LOAD_CUTSCENE_INTERIOR - FALSE - NOT g_bInitCasinoPedsCreated")
					bLoaded = FALSE
				ENDIF
				
				IF g_iSimpleInteriorState != SIMPLE_INT_STATE_IDLE
					PRINTLN("[CORONA][CUTS] - LOAD_CUTSCENE_INTERIOR - FALSE - g_iSimpleInteriorState != SIMPLE_INT_STATE_IDLE")
					bLoaded = FALSE
				ENDIF
				
				IF bLoaded
					PRINTLN("[CORONA][CUTS] - LOAD_CUTSCENE_INTERIOR - Cutscene interior is loaded! Returning TRUE.")
					RETURN TRUE
				ENDIF
			BREAK
				
		ENDSWITCH
	ELSE
		PRINTLN("[CORONA][CUTS] - LOAD_CUTSCENE_INTERIOR - No interior needed for cutscene, return TRUE.")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL REGISTER_CUTSCENE_RENDERTARGET(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	
	STRING strTargetName
	MODEL_NAMES mnTargetModel
	
	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION
		CASE ciLOW_FLOW_MISSION_VCM_DEFEND
			strTargetName = "tvscreen"
			mnTargetModel = PROP_TV_FLAT_MICHAEL
		BREAK
	ENDSWITCH
	
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED(strTargetName)
		REGISTER_NAMED_RENDERTARGET(strTargetName)
		LINK_NAMED_RENDERTARGET(mnTargetModel)
		sCoronaCutsConf.iCutsceneRendertargetRenderID = GET_NAMED_RENDERTARGET_RENDER_ID(strTargetName)
		PRINTLN("[CORONA][CUTS] - REGISTER_CUTSCENE_RENDERTARGET - sCoronaCutsConf.iCutsceneRendertargetRenderID = GET_NAMED_RENDERTARGET_RENDER_ID(",strTargetName,")")
	ELSE
		IF sCoronaCutsConf.iCutsceneRendertargetRenderID != -1
			PRINTLN("[CORONA][CUTS] - REGISTER_CUTSCENE_RENDERTARGET - Rendertarget registered, returning TRUE.")
			RETURN TRUE
		ELSE
			PRINTLN("[CORONA][CUTS] - REGISTER_CUTSCENE_RENDERTARGET - Rendertarget registered, sCoronaCutsConf.iCutsceneRendertargetRenderID = -1")				
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_MODEL_HIDES_FOR_INTERIOR(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, BOOL bCreate)
	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_VCM_RESCUE
			IF bCreate
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_RESCUE")
			
				// url:bugstar:5790280 - MPCAS1_INT - Doors to secretary area need hiding.
				CREATE_MODEL_HIDE(<<1118.390,258.292,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				CREATE_MODEL_HIDE(<<1120.389,258.292,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				
				// url:bugstar:5790281 - MPCAS1_INT - Chair at secretary's desk needs hiding
				CREATE_MODEL_HIDE(<<1115.171,253.689,-46.242>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("ex_prop_offchair_exec_03")), FALSE)
				
				// url:bugstar:5790283 - MPCAS1_INT - Doors to manager's office need hiding
				CREATE_MODEL_HIDE(<<1112.702,254.944,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				CREATE_MODEL_HIDE(<<1112.702,256.944,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				
				// url:bugstar:5790286 - MPCAS1_INT - Chairs in manager's office need hiding
				CREATE_MODEL_HIDE(<<1111.947,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				CREATE_MODEL_HIDE(<<1110.250,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				
				// url:bugstar:5790289 - MPCAS1_INT - Doors inside managers office need hiding
				CREATE_MODEL_HIDE(<<1110.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				CREATE_MODEL_HIDE(<<1112.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_RESCUE")
			ELSE
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_RESCUE")
			
				// url:bugstar:5790280 - MPCAS1_INT - Doors to secretary area need hiding.
				REMOVE_MODEL_HIDE(<<1118.390,258.292,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				REMOVE_MODEL_HIDE(<<1120.389,258.292,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				
				// url:bugstar:5790281 - MPCAS1_INT - Chair at secretary's desk needs hiding
				REMOVE_MODEL_HIDE(<<1115.171,253.689,-46.242>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("ex_prop_offchair_exec_03")), FALSE)
				
				// url:bugstar:5790283 - MPCAS1_INT - Doors to manager's office need hiding
				REMOVE_MODEL_HIDE(<<1112.702,254.944,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				REMOVE_MODEL_HIDE(<<1112.702,256.944,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				
				// url:bugstar:5790286 - MPCAS1_INT - Chairs in manager's office need hiding
				REMOVE_MODEL_HIDE(<<1111.947,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				REMOVE_MODEL_HIDE(<<1110.250,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				
				// url:bugstar:5790289 - MPCAS1_INT - Doors inside managers office need hiding
				REMOVE_MODEL_HIDE(<<1110.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				REMOVE_MODEL_HIDE(<<1112.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_RESCUE")
			ENDIF
		BREAK
		CASE ciLOW_FLOW_MISSION_VCM_BRAWL
			IF bCreate
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_BRAWL")
			
				// url:bugstar:5795933 - MPCAS2_INT - Agatha's chair needs hiding
				CREATE_MODEL_HIDE(<<1111.063,242.979,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_offchair_02")), FALSE)
				
				// url:bugstar:5795936 - MPCAS2_INT - Doors to managers office need hiding
				CREATE_MODEL_HIDE(<<1112.702,254.944,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				CREATE_MODEL_HIDE(<<1112.702,256.944,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				
				// url:bugstar:5795943 - MPCAS2_INT - Chairs in manager's office need hiding
				CREATE_MODEL_HIDE(<<1111.947,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				CREATE_MODEL_HIDE(<<1110.250,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_BRAWL")
			ELSE
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_BRAWL")
			
				// url:bugstar:5795933 - MPCAS2_INT - Agatha's chair needs hiding
				REMOVE_MODEL_HIDE(<<1111.063,242.979,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_offchair_02")), FALSE)
				
				// url:bugstar:5795936 - MPCAS2_INT - Doors to managers office need hiding
				REMOVE_MODEL_HIDE(<<1112.702,254.944,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				REMOVE_MODEL_HIDE(<<1112.702,256.944,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01a")), FALSE)
				
				// url:bugstar:5795943 - MPCAS2_INT - Chairs in manager's office need hiding
				REMOVE_MODEL_HIDE(<<1111.947,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				REMOVE_MODEL_HIDE(<<1110.250,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_BRAWL")
			ENDIF
		BREAK
		CASE ciLOW_FLOW_MISSION_VCM_MEETING
			IF bCreate
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_MEETING")
			
				// url:bugstar:5790473 - MPCAS3_INT - Doors to managers office need hiding
				CREATE_MODEL_HIDE(<<1110.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				CREATE_MODEL_HIDE(<<1112.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				
				// url:bugstar:5790480 - MPCAS3_INT - Chairs in managers office need hiding
				CREATE_MODEL_HIDE(<<1111.947,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				CREATE_MODEL_HIDE(<<1110.250,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				
				// url:bugstar:5792982 - MPCAS4_INT - Agatha's chair needs hiding
				CREATE_MODEL_HIDE(<<1111.063,242.979,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_offchair_02")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_MEETING")
			ELSE
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_MEETING")
			
				// url:bugstar:5790473 - MPCAS3_INT - Doors to managers office need hiding
				REMOVE_MODEL_HIDE(<<1110.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				REMOVE_MODEL_HIDE(<<1112.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				
				// url:bugstar:5790480 - MPCAS3_INT - Chairs in managers office need hiding
				REMOVE_MODEL_HIDE(<<1111.947,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				REMOVE_MODEL_HIDE(<<1110.250,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				
				// url:bugstar:5792982 - MPCAS4_INT - Agatha's chair needs hiding
				REMOVE_MODEL_HIDE(<<1111.063,242.979,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_offchair_02")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_MEETING")
			ENDIF
		BREAK
		CASE ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION
			IF bCreate
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION")
			
				// url:bugstar:5792972 - MPCAS4_INT - Doors to managers office need hiding
				CREATE_MODEL_HIDE(<<1110.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				CREATE_MODEL_HIDE(<<1112.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				
				// url:bugstar:5792978 - MPCAS4_INT - table in managers office needs hiding
				CREATE_MODEL_HIDE(<<1108.675,247.943,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_vwint01_manager_ctable_01")), FALSE)
				
				// url:bugstar:5792982 - MPCAS4_INT - Agatha's chair needs hiding
				CREATE_MODEL_HIDE(<<1111.063,242.979,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_offchair_02")), FALSE)
				
				// url:bugstar:5792987 - MPCAS4_INT - Chair in manager's office needs hiding
				CREATE_MODEL_HIDE(<<1110.250,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				
				// url:bugstar:5793166 - MPCAS4_INT - Magazine needs hiding
				CREATE_MODEL_HIDE(<<1108.405,247.554,-46.450>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_res_fashmag1")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION")
			ELSE
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION")
			
				// url:bugstar:5792972 - MPCAS4_INT - Doors to managers office need hiding
				REMOVE_MODEL_HIDE(<<1110.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				REMOVE_MODEL_HIDE(<<1112.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				
				// url:bugstar:5792978 - MPCAS4_INT - table in managers office needs hiding
				REMOVE_MODEL_HIDE(<<1108.675,247.943,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_vwint01_manager_ctable_01")), FALSE)
				
				// url:bugstar:5792982 - MPCAS4_INT - Agatha's chair needs hiding
				REMOVE_MODEL_HIDE(<<1111.063,242.979,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_offchair_02")), FALSE)
				
				// url:bugstar:5792987 - MPCAS4_INT - Chair in manager's office needs hiding
				REMOVE_MODEL_HIDE(<<1110.250,244.814,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_corp_offchair")), FALSE)
				
				// url:bugstar:5793166 - MPCAS4_INT - Magazine needs hiding
				REMOVE_MODEL_HIDE(<<1108.405,247.554,-46.450>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_res_fashmag1")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION")
			ENDIF
		BREAK
		CASE ciLOW_FLOW_MISSION_VCM_DEFEND
			IF bCreate
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_DEFEND")
			
				// url:bugstar:5790654 - MPCAS5_INT - Doors to managers office need hiding
				CREATE_MODEL_HIDE(<<1110.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				CREATE_MODEL_HIDE(<<1112.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				
				// url:bugstar:5804801 - MPCAS5_INT - Table needs hiding
				CREATE_MODEL_HIDE(<<1108.675,247.943,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_vwint01_manager_ctable_01")), FALSE)
				
				// url:bugstar:5804805 - MPCAS5_INT - Agatha's chair needs hiding
				CREATE_MODEL_HIDE(<<1111.063,242.979,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_offchair_02")), FALSE)
								
				// url:bugstar:5804804 - MPCAS5_INT - Magazine needs hiding
				CREATE_MODEL_HIDE(<<1108.405,247.554,-46.450>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_res_fashmag1")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_DEFEND")
			ELSE
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_DEFEND")
			
				// url:bugstar:5790654 - MPCAS5_INT - Doors to managers office need hiding
				REMOVE_MODEL_HIDE(<<1110.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				REMOVE_MODEL_HIDE(<<1112.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				
				// url:bugstar:5804801 - MPCAS5_INT - Table needs hiding
				REMOVE_MODEL_HIDE(<<1108.675,247.943,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_vwint01_manager_ctable_01")), FALSE)
				
				// url:bugstar:5804805 - MPCAS5_INT - Agatha's chair needs hiding
				REMOVE_MODEL_HIDE(<<1111.063,242.979,-46.841>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_offchair_02")), FALSE)
								
				// url:bugstar:5804804 - MPCAS5_INT - Magazine needs hiding
				REMOVE_MODEL_HIDE(<<1108.405,247.554,-46.450>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_res_fashmag1")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_DEFEND")
			ENDIF
		BREAK
		CASE ciLOW_FLOW_MISSION_VCM_FINALE
			IF bCreate
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_FINALE")
			
				// url:bugstar:5790851 - MPCAS6_INT - Doors to managers office need hiding
				CREATE_MODEL_HIDE(<<1110.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				CREATE_MODEL_HIDE(<<1112.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending creation of model hides for cutscene ciLOW_FLOW_MISSION_VCM_FINALE")
			ELSE
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Starting removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_FINALE")
				
				// url:bugstar:5790851 - MPCAS6_INT - Doors to managers office need hiding
				REMOVE_MODEL_HIDE(<<1110.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				REMOVE_MODEL_HIDE(<<1112.052,251.043,-45.691>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_casino_door_01d")), FALSE)
				
				PRINTLN("[CORONA][CUTS] - HANDLE_MODEL_HIDES_FOR_INTERIOR - Ending removal of model hides for cutscene ciLOW_FLOW_MISSION_VCM_FINALE")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL CORONA_CUTSCENE_HAS_RENDERTARGET(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION
		CASE ciLOW_FLOW_MISSION_VCM_DEFEND
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handle preload and registration of entities for a corona cutscene.
///    When it returns TRUE, stop calling!
/// RETURNS:
///    TRUE when preload has finished.
FUNC BOOL HAVE_PRELOADED_CUTSCENE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	SWITCH sCoronaCutsConf.eSubState
	
		CASE CORONA_CUTSCENE_SUBSTATE_TRIGGER
		
			SET_CUTSCENE_ID(sCoronaCutsConf)
		
			IF HAVE_CUTSCENE_CONFIG(sCoronaCutsConf)
			
				GRAB_CORONA_CUTSCENE_DATA(sCoronaCutsConf)
				
				// Grab the time as the point of progression.
				sCoronaCutsConf.SafetyTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), PICK_INT(IS_THIS_A_VCM_CUTSCENE(sCoronaCutsConf), 45000, 15000)) // TODO: Replace with a proper const_int & timer
				PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - safety timer: ", PICK_INT(IS_THIS_A_VCM_CUTSCENE(sCoronaCutsConf), 45000, 15000))
		
				IF sCoronaCutsConf.bHasPlaybackList
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(sCoronaCutsConf.tlName, sCoronaCutsConf.ePlaybackFlags)
					PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Requested cutscene: ", sCoronaCutsConf.tlName, " with playback flags: ", sCoronaCutsConf.ePlaybackFlags)
				ELSE
					REQUEST_CUTSCENE(sCoronaCutsConf.tlName)
					PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Requested cutscene: ", sCoronaCutsConf.tlName)
				ENDIF
		
				SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_STREAM)
				
			ELSE 
			// The only reason this would fail is if the config for the cutscene is invalid in some way.
			// In that case, exit back to continue past, thus preventing players getting frozen in the corona flow.
			
				SET_CORONA_CUTSCENE_STATE(sCoronaCutsConf, CORONA_CUTSCENE_CLEANUP)
				
			ENDIF
			
		BREAK
		
		CASE CORONA_CUTSCENE_SUBSTATE_STREAM
		
			IF (CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY())
					
				INT index, iCloneCount
				
				iCloneCount = sCoronaCutsConf.iNumParticipants
	
				IF sCoronaCutsConf.iCloneSource = ciCORONA_CUTSCENE_BOTH_CLONES
					iCloneCount += iCloneCount
				ENDIF
					
				FOR index = 0 TO (iCloneCount-1)
					SET_STREAM_FLAG_FOR_CORONA_CUTSCENE_PED(index)
				ENDFOR
				
				IF DOES_CORONA_CUTSCENE_NEED_NPC_PEDS(sCoronaCutsConf)
					REPEAT sCoronaCutsConf.iNumNPCs index
						SET_STREAM_FLAG_FOR_CORONA_CUTSCENE_NPC_PED(sCoronaCutsConf, index)
					ENDREPEAT
				ENDIF
				
				PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Asset steam flags have been set, moving to peds.")
				
				SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_PEDS)
			ELSE
				PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Waiting for CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY to return TRUE.")
			ENDIF
		
		BREAK
		
		CASE CORONA_CUTSCENE_SUBSTATE_PEDS
		
			IF CREATE_CORONA_CUTSCENE_PEDS(sCoronaCutsConf)
			
				IF SHOULD_LOAD_CUTSCENE_VEHICLES(sCoronaCutsConf)
					PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Created corona cutscene peds, move to load vehicles")
					SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_VEHICLES)
				ELIF DOES_CUTSCENE_REQUIRE_INTERIOR(sCoronaCutsConf)
					PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Created corona cutscene peds, move to load interior")
					SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_INTERIOR)
				ELIF CORONA_CUTSCENE_HAS_RENDERTARGET(sCoronaCutsConf)
					PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Created corona cutscene peds, move to rendertarget")
					SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_RENDERTARGET)
				ELSE
					PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Created corona cutscene peds, move to loading")
					SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_LOADING)
				ENDIF
			
			ENDIF
		
		BREAK
		
		// Add new case for cars
		CASE CORONA_CUTSCENE_SUBSTATE_VEHICLES
			
			IF CREATE_CUTSCENE_VEHICLES(sCoronaCutsConf)
				IF DOES_CUTSCENE_REQUIRE_INTERIOR(sCoronaCutsConf)
					PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Created corona cutscene vehicles, move to load interior")
					SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_INTERIOR)
				ELSE
					PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Created corona cutscene vehicles, move to load")
					SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_LOADING)
				ENDIF
			ELSE
				PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Waiting for CREATE_CUTSCENE_VEHICLES.")
			ENDIF
			
		BREAK
		
		CASE CORONA_CUTSCENE_SUBSTATE_INTERIOR
		
			IF LOAD_CUTSCENE_INTERIOR(sCoronaCutsConf)
				HANDLE_MODEL_HIDES_FOR_INTERIOR(sCoronaCutsConf, TRUE)
				IF CORONA_CUTSCENE_HAS_RENDERTARGET(sCoronaCutsConf)
					PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Created corona cutscene interior, move to rendertarget")
					SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_RENDERTARGET)
				ELSE
					PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Created corona cutscene interior, move to load")
					SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_LOADING)
				ENDIF
			ELSE
				PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Waiting for LOAD_CUTSCENE_INTERIOR.")
			ENDIF
			
		BREAK
		
		CASE CORONA_CUTSCENE_SUBSTATE_RENDERTARGET
		
			IF REGISTER_CUTSCENE_RENDERTARGET(sCoronaCutsConf)
				PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Registered corona cutscene rendertarget, move to load")
				SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_LOADING)
			ELSE
				PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Waiting for REGISTER_CUTSCENE_RENDERTARGET.")
			ENDIF
			
		BREAK
		
		CASE CORONA_CUTSCENE_SUBSTATE_LOADING
		
			IF (HAS_CUTSCENE_LOADED())
				SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_FINISHED)
			ELSE
				PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Waiting for HAS_CUTSCENE_LOADED.")
			ENDIF
			
		BREAK

		CASE CORONA_CUTSCENE_SUBSTATE_FINISHED
		
//			REGISTER_CORONA_CUTSCENE_PEDS(sCoronaCutsConf)
//			SET_CORONA_CUTSCENE_PED_VISIBILITY(sCoronaCutsConf, TRUE)
			
			PRINTLN("[CORONA][CUTS] - HAVE_PRELOADED_CUTSCENE - ",GET_STRING_FROM_CORONA_SUBSTATE(sCoronaCutsConf.eSubState)," - Cutscene is fully loaded, moving to wait main state.")
			
			// Reset pre-load state here as we shouldn't ever go backwards in the flow from this point.
			//SET_CORONA_CUTSCENE_SUBSTATE(sCoronaCutsConf, CORONA_CUTSCENE_SUBSTATE_TRIGGER)
		
		RETURN TRUE

	ENDSWITCH
	
	RETURN FALSE

ENDFUNC


// PURPOSE:	Delete any cloned peds used inteh cutscene to represent the players
PROC DESTROY_CORONA_CUTSCENE_PEDS(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	INT tempLoop = 0
	MODEL_NAMES tempModel
	
	REPEAT MAX_CORONA_CUTSCENE_PARTICIPANTS tempLoop
		IF NOT (sCoronaCutsConf.pedCloneList[tempLoop] = NULL)
			IF (DOES_ENTITY_EXIST(sCoronaCutsConf.pedCloneList[tempLoop]))
				PRINTLN("[CORONA][CUTS] - DESTROY_CORONA_CUTSCENE_PEDS - Mark clone as no longer needed: ", tempLoop)
				tempModel = GET_ENTITY_MODEL(sCoronaCutsConf.pedCloneList[tempLoop])
				SET_MODEL_AS_NO_LONGER_NEEDED(tempModel)
				DELETE_PED(sCoronaCutsConf.pedCloneList[tempLoop])
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF DOES_CORONA_CUTSCENE_NEED_NPC_PEDS(sCoronaCutsConf)
		REPEAT MAX_CORONA_CUTSCENE_NPC_PEDS tempLoop
			IF (DOES_ENTITY_EXIST(sCoronaCutsConf.pedNPC[tempLoop]))
				PRINTLN("[CORONA][CUTS] - DESTROY_CORONA_CUTSCENE_PEDS - Mark NPC ped as no longer needed: ", tempLoop)
				tempModel = GET_ENTITY_MODEL(sCoronaCutsConf.pedNPC[tempLoop])
				SET_MODEL_AS_NO_LONGER_NEEDED(tempModel)
				DELETE_PED(sCoronaCutsConf.pedNPC[tempLoop])
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC DESTROY_CORONA_CUTSCENE_VEHICLES(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	INT tempLoop = 0
	MODEL_NAMES tempModel
	
	REPEAT MAX_CORONA_CUTSCENE_VEHICLES tempLoop
		IF DOES_ENTITY_EXIST(sCoronaCutsConf.vehList[tempLoop])
			PRINTLN("[CORONA][CUTS] - DESTROY_CORONA_CUTSCENE_VEHICLES - Mark vehicle as no longer needed: ", tempLoop, " index ", NATIVE_TO_INT(sCoronaCutsConf.vehList[tempLoop]))
			tempModel = GET_ENTITY_MODEL(sCoronaCutsConf.vehList[tempLoop])
			SET_MODEL_AS_NO_LONGER_NEEDED(tempModel)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(sCoronaCutsConf.vehList[tempLoop])
			DELETE_VEHICLE(sCoronaCutsConf.vehList[tempLoop])
		ENDIF
	ENDREPEAT

ENDPROC



//PROC PRE_PROCESS(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
//
//ENDPROC


PROC CORONA_CUTSCENE_IDLE_FOR_INSTRUCTIONS(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	#IF IS_DEBUG_BUILD
	DRAW_CORONA_CUTSCENE_WIDGETS(sCoronaCutsConf)
	DUMP_CORONA_CUTSCENE_STATS_TO_LOG()
	#ENDIF
	
	START_CORONA_CUTSCENE_SAFETY_TIMER(sCoronaCutsConf)
	
	SET_CORONA_CUTSCENE_STATE(sCoronaCutsConf, CORONA_CUTSCENE_INIT)

ENDPROC

CONST_FLOAT cfVCM_OIL_DEMOLITION_BINK_DURATION		45.0
CONST_FLOAT cfVCM_DEFEND_BINK_DURATION				35.0

PROC SET_CORONA_CUTSCENE_RENDERTARGET_STATE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, INT iState)
	IF sCoronaCutsConf.iRenderTargetState != iState
		PRINTLN("[CORONA][CUTS] - SET_CORONA_CUTSCENE_RENDERTARGET_STATE - Setting rendertarget state to ", iState)
	ENDIF
	sCoronaCutsConf.iRenderTargetState = iState
ENDPROC

FUNC BOOL CORONA_CUTSCENE_HAS_BINK_MOVIE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION
		CASE ciLOW_FLOW_MISSION_VCM_DEFEND
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_CUTSCENE_EVENT_INFO(STRING eventName)
	PRINTLN("[CORONA][CUTS] - PRINT_CUTSCENE_EVENT_INFO - Event Name: ", eventName)
	PRINTLN("[CORONA][CUTS] - PRINT_CUTSCENE_EVENT_INFO - Cutscene Time: ", GET_CUTSCENE_TIME())
ENDPROC
#ENDIF

PROC HANDLE_RENDERTARGET_SYNCS(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	FLOAT fBinkPercentage

	SWITCH sCoronaCutsConf.iCutsceneId
		CASE ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION
			fBinkPercentage = (((TO_FLOAT(GET_CUTSCENE_TIME()-sCoronaCutsConf.iBinkMovieStartTime)/1000.0) / cfVCM_OIL_DEMOLITION_BINK_DURATION) * 100.0)
			IF DOES_ENTITY_EXIST(sCoronaCutsConf.pedCloneList[0])
			AND NOT IS_PED_INJURED(sCoronaCutsConf.pedCloneList[0])
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_START"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 0)
					SET_BINK_MOVIE_TIME(sCoronaCutsConf.binkMovieID, fBinkPercentage)
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 0)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_START")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("FULLSCREEN_END"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 1)
					sCoronaCutsConf.bRenderFullscreen = FALSE
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 1)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("FULLSCREEN_END")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_SYNC_1"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 2)
					SET_BINK_MOVIE_TIME(sCoronaCutsConf.binkMovieID, fBinkPercentage)
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 2)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_SYNC_1")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_SYNC_2"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 3)
					SET_BINK_MOVIE_TIME(sCoronaCutsConf.binkMovieID, fBinkPercentage)
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 3)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_SYNC_2")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_SYNC_3"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 4)
					SET_BINK_MOVIE_TIME(sCoronaCutsConf.binkMovieID, fBinkPercentage)
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 4)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_SYNC_3")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_SYNC_4"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 5)
					SET_BINK_MOVIE_TIME(sCoronaCutsConf.binkMovieID, fBinkPercentage)
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 5)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_SYNC_4")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_SYNC_5"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 6)
					SET_BINK_MOVIE_TIME(sCoronaCutsConf.binkMovieID, fBinkPercentage)
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 6)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_SYNC_5")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_TV_OFF"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 7)
					IF sCoronaCutsConf.bStartedBinkMovie
						STOP_BINK_MOVIE(sCoronaCutsConf.binkMovieID)
						RELEASE_BINK_MOVIE(sCoronaCutsConf.binkMovieID)
						sCoronaCutsConf.binkMovieID = SET_BINK_MOVIE("MPCASTV_MPCAS4_B")
						sCoronaCutsConf.bStartedBinkMovie = FALSE
						sCoronaCutsConf.bRenderBink = FALSE
					ENDIF
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 7)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_TV_OFF")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("B_START"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 8)
					sCoronaCutsConf.bRenderBink = TRUE
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 8)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("B_START")		#ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE ciLOW_FLOW_MISSION_VCM_DEFEND
			fBinkPercentage = (((TO_FLOAT(GET_CUTSCENE_TIME()-sCoronaCutsConf.iBinkMovieStartTime)/1000.0) / cfVCM_DEFEND_BINK_DURATION) * 100.0)
			IF DOES_ENTITY_EXIST(sCoronaCutsConf.pedCloneList[0])
			AND NOT IS_PED_INJURED(sCoronaCutsConf.pedCloneList[0])
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_START"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 0)
					SET_BINK_MOVIE_TIME(sCoronaCutsConf.binkMovieID, fBinkPercentage)
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 0)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_START")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_SYNC_1"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 1)
					SET_BINK_MOVIE_TIME(sCoronaCutsConf.binkMovieID, fBinkPercentage)
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 1)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_SYNC_1")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_SYNC_2"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 2)
					SET_BINK_MOVIE_TIME(sCoronaCutsConf.binkMovieID, fBinkPercentage)
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 2)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_SYNC_2")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_SYNC_3"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 3)
					SET_BINK_MOVIE_TIME(sCoronaCutsConf.binkMovieID, fBinkPercentage)
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 3)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_SYNC_3")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("A_TV_OFF"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 4)
					IF sCoronaCutsConf.bStartedBinkMovie
						STOP_BINK_MOVIE(sCoronaCutsConf.binkMovieID)
						RELEASE_BINK_MOVIE(sCoronaCutsConf.binkMovieID)
						sCoronaCutsConf.binkMovieID = SET_BINK_MOVIE("MPCASTV_MPCAS5_B")
						sCoronaCutsConf.bStartedBinkMovie = FALSE
						sCoronaCutsConf.bRenderBink = FALSE
					ENDIF
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 4)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("A_TV_OFF")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("B_TV_ON"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 5)
					sCoronaCutsConf.bRenderBink = TRUE
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 5)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("B_TV_ON")		#ENDIF
				ENDIF
			ENDIF
		BREAK
		#IF FEATURE_HEIST_ISLAND
		CASE ciLOW_FLOW_MISSION_HEIST_ISLAND_HS4F_INT
			IF DOES_ENTITY_EXIST(sCoronaCutsConf.pedCloneList[0])
			AND NOT IS_PED_INJURED(sCoronaCutsConf.pedCloneList[0])
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("IslandScreen"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 0)
					g_sNetHeistPlanningGenericClientData.sScaleformData.bDrawBoard = FALSE
					g_sNetHeistPlanningGenericClientData.sScaleformData.bDrawAlt = TRUE
					
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 0)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("IslandScreen")		#ENDIF
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sCoronaCutsConf.pedCloneList[0], HASH("IntelLockedScreen"))
				AND NOT IS_BIT_SET(sCoronaCutsConf.iBinkMovieSyncsComplete, 1)
										
					SET_BIT(sCoronaCutsConf.iBinkMovieSyncsComplete, 1)
					#IF IS_DEBUG_BUILD 		PRINT_CUTSCENE_EVENT_INFO("IntelLockedScreen")		#ENDIF
				ENDIF
			ENDIF
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

PROC RENDER_CUTSCENE_BINK_MOVIE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	IF NOT CORONA_CUTSCENE_HAS_BINK_MOVIE(sCoronaCutsConf)
		EXIT
	ENDIF

	IF !sCoronaCutsConf.bRenderBink
		IF (GET_FRAME_COUNT() % 30 = 0)
			PRINTLN("[CORONA][CUTS] - RENDER_CUTSCENE_BINK_MOVIE - !sCoronaCutsConf.bRenderBink - bailing")
		ENDIF
		EXIT
	ENDIF

	IF sCoronaCutsConf.bRenderFullscreen
		SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	ELSE
		SET_TEXT_RENDER_ID(sCoronaCutsConf.iCutsceneRendertargetRenderID)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
		SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	ENDIF
	
	IF !sCoronaCutsConf.bStartedBinkMovie
		PLAY_BINK_MOVIE(sCoronaCutsConf.binkMovieID)
		SET_BINK_MOVIE_TIME(sCoronaCutsConf.binkMovieID, 0.0)
		SET_BINK_MOVIE_VOLUME(sCoronaCutsConf.binkMovieID, -24.0)
		SET_BINK_SHOULD_SKIP(sCoronaCutsConf.binkMovieID, TRUE)
		sCoronaCutsConf.bStartedBinkMovie = TRUE
		sCoronaCutsConf.iBinkMovieStartTime = GET_CUTSCENE_TIME()
		sCoronaCutsConf.iBinkMovieSyncsComplete = 0
		PRINTLN("[CORONA][CUTS] - RENDER_CUTSCENE_BINK_MOVIE - Started bink video")
	ENDIF

	DRAW_BINK_MOVIE(sCoronaCutsConf.binkMovieID, 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
	IF (GET_FRAME_COUNT() % 30 = 0)
		PRINTLN("[CORONA][CUTS] - RENDER_CUTSCENE_BINK_MOVIE - Drawing bink video. Fullscreen? ", GET_STRING_FROM_BOOL(sCoronaCutsConf.bRenderFullscreen))
	ENDIF
	
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())

ENDPROC

PROC MAINTAIN_CORONA_CUTSCENE_RENDERTARGET(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	HANDLE_RENDERTARGET_SYNCS(sCoronaCutsConf)
	RENDER_CUTSCENE_BINK_MOVIE(sCoronaCutsConf)

ENDPROC

PROC CLEAN_CORONA_CUTSCENE_RENDERTARGETS(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
	IF sCoronaCutsConf.bStartedBinkMovie
		STOP_BINK_MOVIE(sCoronaCutsConf.binkMovieID)
		RELEASE_BINK_MOVIE(sCoronaCutsConf.binkMovieID)
		sCoronaCutsConf.binkMovieID = NULL
		sCoronaCutsConf.bStartedBinkMovie = FALSE
		PRINTLN("[CORONA][CUTS] - CLEAN_CORONA_CUTSCENE_RENDERTARGETS - Bink video stopped")
	ENDIF
	
	IF IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
		RELEASE_NAMED_RENDERTARGET("tvscreen")
		sCoronaCutsConf.iCutsceneRendertargetRenderID = -1
		PRINTLN("[CORONA][CUTS] - CLEAN_CORONA_CUTSCENE_RENDERTARGETS - named rendertarget released")
	ENDIF
	
	SET_CORONA_CUTSCENE_RENDERTARGET_STATE(sCoronaCutsConf, 0)
	
	sCoronaCutsConf.iBinkMovieStartTime = 0
	sCoronaCutsConf.iBinkMovieSyncsComplete = 0
	sCoronaCutsConf.bRenderBink = TRUE
	sCoronaCutsConf.bRenderFullscreen = FALSE
ENDPROC

PROC CORONA_CUTSCENE_INIT_CUTSCENE_LOADING(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	IF HAS_CORONA_CUTSCENE_SAFETY_TIMER_EXPIRED(sCoronaCutsConf)
		#IF IS_DEBUG_BUILD
			PRINTLN("[CORONA][CUTS] - SAFETY_TIMER - Warning! Safety timer has expired while attempting to load cutscene: ",sCoronaCutsConf.tlName,", moving to cleanup!")
			SCRIPT_ASSERT("[CORONA][CUTS] - SAFETY_TIMER - Warning! Safety timer has expired while attempting to load cutscene, moving to cleanup! Is there an asset issue with your build?")
		#ENDIF
		
		SET_CORONA_BIT(CORONA_CUTSCENE_FAILED_TO_START)
		
		SET_CORONA_CUTSCENE_STATE(sCoronaCutsConf, CORONA_CUTSCENE_CLEANUP)
	ENDIF

	IF HAVE_PRELOADED_CUTSCENE(sCoronaCutsConf)
		
		SET_CORONA_CUTSCENE_STATE(sCoronaCutsConf, CORONA_CUTSCENE_WAIT)
		
	ENDIF

ENDPROC


PROC CORONA_CUTSCENE_WAIT_FOR_TRIGGER(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	//Add wait conditions here
	IF sCoronaCutsConf.iCutsceneId = ciLOW_FLOW_MISSION_CASINO_HEIST_INT1
		IF g_iSimpleInteriorState < SIMPLE_INT_STATE_IDLE
		OR NOT IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_GLOBAL_DATA_COMPLETED_ARCADE_HEIST_INT_REFRESH)
			PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_WAIT_FOR_TRIGGER - ciLOW_FLOW_MISSION_CASINO_HEIST_INT1 waiting for simple interior.")
			EXIT
		ENDIF
		
		SET_CORONA_BIT(CORONA_CASINO_HEIST_MAINTAIN_BOARDS)
	ENDIF
		
	IF IS_THIS_A_VCM_CUTSCENE(sCoronaCutsConf)
	#IF FEATURE_CASINO_HEIST
	OR sCoronaCutsConf.iCutsceneId = ciLOW_FLOW_MISSION_CASINO_HEIST_INT1
	#ENDIF
		CORONA_CLEAN_UP_SCREEN_STATE()
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_IN()
		PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_WAIT_FOR_TRIGGER - Fading screen in.")
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
	ENDIF
	
	// This has to happen before START_CUTSCENE and has to be on the same frame, otherwise code
	// starts to get upset and fires off a bunch of asserts.
	REGISTER_CORONA_CUTSCENE_PEDS(sCoronaCutsConf)
	REGISTER_CORONA_CUTSCENE_VEHICLES(sCoronaCutsConf)

	PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_WAIT_FOR_TRIGGER - Setting bPlayingCoronaCutscene to TRUE.")
	GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].sClientCoronaData.bPlayingCoronaCutscene = TRUE

	//Terminate_MP_Mission_At_Coords_Vehicle_Generators()

	START_AUDIO_SCENE("DLC_Lowrider_Cutscene_Corona_Radio_Mute_Scene")
	START_CORONA_CUTSCENE_RADIO_TIMER(sCoronaCutsConf)

	// Fire off the cutscene.
	START_CUTSCENE()
	
	IF IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(PLAYER_ID())
		IF NOT IS_SPECTATOR_RUNNING_CUTSCENE()
			SET_SPECTATOR_RUNNING_CUTSCENE(TRUE)
		ENDIF
	ENDIF
	
	MAINTAIN_CORONA_CUTSCENE_RENDERTARGET(sCoronaCutsConf)
	
	SET_CORONA_CUTSCENE_STATE(sCoronaCutsConf, CORONA_CUTSCENE_PLAY)

ENDPROC

PROC HANDLE_END_OF_CUTSCENE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	#IF FEATURE_CASINO_HEIST
	IF sCoronaCutsConf.iCutsceneId = ciLOW_FLOW_MISSION_CASINO_HEIST_INT1
		PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_PLAYBACK - HANDLE_END_OF_CUTSCENE - Dont do skyfreeze clear as this is the end of casino heist cutscene 1, we need to go to the finale board.")
		
//		IF NOT IS_SCREEN_FADED_OUT()
//		AND NOT IS_SCREEN_FADING_OUT()
//			DO_SCREEN_FADE_OUT(200)
//		ENDIF
//		
//		SET_CORONA_BIT(CORONA_FADED_SCREEN_OUT)
				
		SET_CORONA_BIT(CORONA_FREEZE_FOR_CASINO_HEIST_PLANNING)
				
		EXIT
	ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF sCoronaCutsConf.iCutsceneId = ciLOW_FLOW_MISSION_HEIST_ISLAND_HS4F_INT
		g_sNetHeistPlanningGenericClientData.sScaleformData.bDrawBoard = TRUE
		g_sNetHeistPlanningGenericClientData.sScaleformData.bDrawAlt = FALSE
		PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_PLAYBACK - HANDLE_END_OF_CUTSCENE - End of HS4F_INT, stop drawing alt and draw main board.")
	ENDIF
	#ENDIF

	SET_SKYFREEZE_FROZEN(TRUE)
ENDPROC

PROC CORONA_CUTSCENE_PLAYBACK(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	IF NOT sCoronaCutsConf.bPedsVisible
		SET_CORONA_CUTSCENE_PED_VISIBILITY(sCoronaCutsConf, TRUE)
		SET_FRONTEND_ACTIVE(FALSE)
		CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam, g_FMMC_STRUCT.iMissionType, TRUE, TRUE, TRUE)			
	ENDIF
	
	IF NOT sCoronaCutsConf.bVehsVisible = TRUE
		SET_CORONA_CUTSCENE_VEHICLE_VISIBILITY(sCoronaCutsConf, TRUE)
	ENDIF
	
	SETUP_PED_OUTFITS_FOR_CUTSCENE(sCoronaCutsConf)
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	THEFEED_HIDE_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
	
	MAINTAIN_CORONA_CUTSCENE_RENDERTARGET(sCoronaCutsConf)
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_CUTSCENE_AUTHORIZED(sCoronaCutsConf.tlName)
		PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_PLAYBACK - ERROR! Cutscene ",sCoronaCutsConf.tlName," is NOT authorized for playback. Please verify your local assets are up to date.")
		SCRIPT_ASSERT("[CORONA][CUTS] - CORONA_CUTSCENE_PLAYBACK - ERROR! Cutscene is NOT authorized for playback. Please verify your local assets are up to date.")
		SET_CORONA_CUTSCENE_STATE(sCoronaCutsConf, CORONA_CUTSCENE_CLEANUP)
		EXIT
	ENDIF
	#ENDIF
	
	IF NOT sCoronaCutsConf.bKilledRadio
		IF HAS_CORONA_CUTSCENE_RADIO_TIMER_EXPIRED(sCoronaCutsConf)
			SET_RADIO_TO_STATION_NAME("OFF")
			PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_PLAYBACK - Radio cutout timer has expired, setting radio station to OFF.")
			sCoronaCutsConf.bKilledRadio = TRUE
		ENDIF
	ENDIF
	
	IF CAN_SET_EXIT_STATE_FOR_CAMERA()
		HANDLE_END_OF_CUTSCENE(sCoronaCutsConf)
		PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_PLAYBACK - Freezing screen for cutscene finish.")
	ENDIF

	IF (HAS_CUTSCENE_FINISHED())
		HANDLE_END_OF_CUTSCENE(sCoronaCutsConf)
		PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_PLAYBACK - Cutscene thinks it has played to the end: ", sCoronaCutsConf.tlName)
		SET_CORONA_CUTSCENE_STATE(sCoronaCutsConf, CORONA_CUTSCENE_CLEANUP)
	ELIF sCoronaCutsConf.bSkip
		// A 'skip cutscene' event has been received, so set the flag that allows the cutscene to cleanup normally
		NETWORK_SET_MOCAP_CUTSCENE_CAN_BE_SKIPPED(TRUE)
		PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_PLAYBACK - Cutscene skip requested: ", sCoronaCutsConf.tlName)
		
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
		ENDIF
	ELSE
		IF HAS_CORONA_CUTSCENE_SKIP_BEEN_REQUESTED(sCoronaCutsConf)
			IF ARE_ALL_PARTICIPANTS_READY_TO_SKIP(sCoronaCutsConf)
				BROADCAST_EVENT_SKIP_CORONA_CUTSCENE()
			ENDIF
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:
///    Clean all cutscene data and track completion.
PROC CORONA_CUTSCENE_CLEAN_ALL(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf, BOOL bNoStats = FALSE, BOOL bKicked = FALSE)

	PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_CLEAN_ALL - Cleaning corona cutscene data. bNoStats: ", bNoStats)

	#IF IS_DEBUG_BUILD
	DEBUG_bDrawnWidgets = FALSE
	
	IF NOT DEBUG_bDisableStatsSave
	#ENDIF
	
	IF NOT bNoStats
	AND NOT sCoronaCutsConf.bSkip
		IF (sCoronaCutsConf.eTrackerStat != MP_STAT_DEFAULT_STATS_SET)
			SET_MP_BOOL_CHARACTER_STAT(sCoronaCutsConf.eTrackerStat, TRUE)
			PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_CLEAN_ALL - Saved seen status to tracker stat.")
		ELSE
			PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_CLEAN_ALL - ERROR! Could not track cutscene completion, tracker stat is INVALID.")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_CLEAN_ALL - DEBUG_bDisableStats = TRUE, do not save cutscene progress.")
	ENDIF
	#ENDIF
	
	IF bKicked
	AND NOT IS_VECTOR_ZERO(sCoronaCutsConf.vPedCachedStartPos)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), sCoronaCutsConf.vPedCachedStartPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), sCoronaCutsConf.fPedCachedStartHeading)
		PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_CLEAN_ALL - Player was kicked, due to not enough players, placing them back to where they were before.")
	ENDIF
	
	IF IS_CORONA_BIT_SET(CORONA_CUTSCENE_FAILED_TO_START)
		IF HAS_CUTSCENE_LOADED()
			REMOVE_CUTSCENE()
			PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_CLEAN_ALL - Cutscene had loaded but failed to play. REMOVE_CUTSCENE called.")
		ENDIF
		CLEAR_CORONA_BIT(CORONA_CUTSCENE_FAILED_TO_START)
	ENDIF
	
	CLEAN_CORONA_CUTSCENE_RENDERTARGETS(sCoronaCutsConf)
	HANDLE_MODEL_HIDES_FOR_INTERIOR(sCoronaCutsConf, FALSE)
	DESTROY_CORONA_CUTSCENE_PEDS(sCoronaCutsConf)
	DESTROY_CORONA_CUTSCENE_VEHICLES(sCoronaCutsConf)
	CLEAN_CORONA_CUTSCENE_DATA(sCoronaCutsConf)
	
	NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
	CLEANUP_MP_CUTSCENE(FALSE, FALSE)
	
	IF IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(PLAYER_ID())
		IF IS_SPECTATOR_RUNNING_CUTSCENE()
			SET_SPECTATOR_RUNNING_CUTSCENE(FALSE)
		ENDIF
	ENDIF
	
	PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_CLEAN_ALL - Setting bPlayingCoronaCutscene to FALSE.")
	GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].sClientCoronaData.bPlayingCoronaCutscene = FALSE
	
	PRINTLN("[CORONA][CUTS] - CORONA_CUTSCENE_CLEAN_ALL - Cutscene fully cleaned up.")

	SET_CORONA_CUTSCENE_STATE(sCoronaCutsConf, CORONA_CUTSCENE_NONE)

ENDPROC


//PROC POST_PROCESS(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)
//
//ENDPROC


/// PURPOSE:
///    State controller for corona cutscenes.
/// PARAMS:
///    sCoronaCutsConf - Passed in config for the cutscene flow.
FUNC BOOL MANAGE_CORONA_CUTSCENE_STATE(CORONA_CUTSCENE_CONFIG& sCoronaCutsConf)

	//PRE_PROCESS(sCoronaCutsConf)

	SWITCH sCoronaCutsConf.eState
	
		DEFAULT
		
		BREAK
	
		CASE CORONA_CUTSCENE_NONE
		
			CORONA_CUTSCENE_IDLE_FOR_INSTRUCTIONS(sCoronaCutsConf)
		
		BREAK
		
		CASE CORONA_CUTSCENE_INIT
		
			CORONA_CUTSCENE_INIT_CUTSCENE_LOADING(sCoronaCutsConf)
		
		BREAK
		
		CASE CORONA_CUTSCENE_WAIT
		
			CORONA_CUTSCENE_WAIT_FOR_TRIGGER(sCoronaCutsConf)
		
		BREAK
		
		CASE CORONA_CUTSCENE_PLAY
		
			CORONA_CUTSCENE_PLAYBACK(sCoronaCutsConf)
		
		BREAK
		
		CASE CORONA_CUTSCENE_CLEANUP
		
			CORONA_CUTSCENE_CLEAN_ALL(sCoronaCutsConf)
		
		RETURN TRUE
	
	ENDSWITCH
	
	//POST_PROCESS(sCoronaCutsConf)
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Main cutscene maintain.
/// RETURNS:
///    TRUE when cutscene has finished playback.
FUNC BOOL CONTROL_FM_CORONA_CUTSCENE(	CORONA_CUTSCENE_CONFIG& sCoronaCutsConf 
										#IF IS_DEBUG_BUILD , WIDGET_GROUP_ID wgParentWidgetGroup #ENDIF)

	#IF IS_DEBUG_BUILD
		DEBUG_CoronaParentWidgets = wgParentWidgetGroup
		MAINTAIN_DEBUG_STAT_CONTROLS()
	#ENDIF

	RETURN MANAGE_CORONA_CUTSCENE_STATE(sCoronaCutsConf)

ENDFUNC

 // FEATURE_LOWRIDER_CONTENT

//
// End of file.
//
