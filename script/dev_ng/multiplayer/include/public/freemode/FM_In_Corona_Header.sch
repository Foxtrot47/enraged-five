//////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FM_In_Corona_Header.sch																//
// Written by:  Robert Wright																		//
// Date: 27/11/2012																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////
USING "rage_builtins.sch"
USING "globals.sch"
USING "script_network.sch"
USING "net_scoring_common.sch"
USING "net_include.sch"
USING "fm_maintain_strand_mission.sch"

// iCoronaBitSet						- NUM_CORONA_BITSETS = 1
CONST_INT CORONA_ACTIVE_HOST_OF_MENU								0		// (0)
CONST_INT CORONA_CLIENT_ABLE_TO_PLAY								1		// (1)
CONST_INT CORONA_VEHICLE_SELECTOR_DISPLAYING						2		// (2)
CONST_INT CORONA_BETTING_VISUAL_ENABLED								3		// (3)
CONST_INT CORONA_PLAYER_IN_SKYSWOOP									4		// (4)
CONST_INT CORONA_PLAYER_ON_TUTORIAL									5		// (5)
CONST_INT CORONA_LAUNCHED_FM_PLAYERS_IN_CORONA						6		// (6)
CONST_INT CORONA_RETURNING_FROM_INVITE_SCREEN						7		// (7)
CONST_INT CORONA_SKIP_WAIT_FOR_FRONTEND								8		// (8)
CONST_INT CORONA_ADJUSTING_CASH_VALUE								9		// (9)
CONST_INT CORONA_AUTOFILL_CAN_CONTINUE								10		// (10)
CONST_INT CORONA_AUTOFILL_IS_NOT_AVAILABLE							11		// (11)
CONST_INT CORONA_PLAYER_SENT_INVITE_ALL								12		// (12)
CONST_INT CORONA_READY_UP_15_TIMEOUT								13		// (13)
CONST_INT CORONA_PLAYER_PLAY_ANIM									14		// (14)
CONST_INT CORONA_PLAYER_PLAYING_ANIM								15		// (15)
CONST_INT CORONA_MISSION_IS_COOP									16		// (16)
CONST_INT CORONA_BETTING_MENU_HIDDEN_FOR_ANIM						17		// (17)
CONST_INT CORONA_BETTING_IS_LOCKED									18		// (18)
CONST_INT CORONA_PLAYER_TEAMS_HAVE_BEEN_PROCESSED					19		// (19)
CONST_INT CORONA_PLAYER_LAUNCHING_SOLO_JOB							20		// (20)
CONST_INT CORONA_AUTOFILL_SENT										21		// (21)
CONST_INT CORONA_PLAYER_VEHICLE_COLOURS_RANDOM						22		// (22)
CONST_INT CORONA_INVITED_PLAYER_LIST_UPDATED						23		// (23)
CONST_INT CORONA_INITIALISE_VEHICLE_COLOURS							24		// (24)
CONST_INT CORONA_HAS_PLAYER_SAVED_VEHICLE							25		// (25)
CONST_INT CORONA_DISPLAYING_CLIENT_CHOSEN_TEAM						26		// (26)
CONST_INT CORONA_AUTOFILL_SENT_TO_LARGE_GROUPS						27		// (27)
CONST_INT CORONA_PULSE_READY_UP										28		// (28)
CONST_INT CORONA_CORONA_HELP_DISPLAYING								29		// (29)
CONST_INT CORONA_VEHICLE_COLOURS_DEFAULTED							30		// (30)
CONST_INT CORONA_REFRESH_RECENT_PLAYER_PED							31		// (31)
	
// iCoronaBitSet2						- NUM_CORONA_BITSETS = 2	
CONST_INT CORONA_REQUEST_BETTING_PLAYER_CREW_EMBLEM					32		// (0)
CONST_INT CORONA_VEHICLE_SELECTION_UNAVAILABLE						33  	// (1)
CONST_INT CORONA_SPLIT_COMPARE_DISPLAYED							34  	// (2)
CONST_INT CORONA_HOST_SELECTED_THEIR_PARTNER						35  	// (3)
CONST_INT CORONA_PHOTO_HAS_BEEN_INITIALISED							36  	// (4)
CONST_INT CORONA_DETAILS_SCREEN_HAS_BEEN_VISITED					37  	// (5)
CONST_INT CORONA_RECENT_PLAYER_PED_DISPLAYED						38  	// (6)
CONST_INT CORONA_TRIGGER_PERSISTENT_MUSIC							39  	// (7)
CONST_INT CORONA_TUTORIAL_PED_HEADSHOT_LOADED						40  	// (8)
CONST_INT CORONA_REQUESTED_TRANSITION_SCREEN_HEADER					41  	// (9)
CONST_INT CORONA_LEADERBOARD_HAS_EXITED								42  	// (10)
CONST_INT CORONA_TRIGGERED_EARLY_LOAD_SCENE							43  	// (11)
CONST_INT CORONA_PLAYER_PLAYING_QUICK_PLAY_ANIM						44  	// (12)
CONST_INT CORONA_HEAD_TO_HEAD_WAIT_FOR_MATCHED_CREWS				45  	// (13)
CONST_INT CORONA_HEAD_TO_HEAD_WAIT_FOR_FRIENDS_CREWS				46  	// (14)
CONST_INT CORONA_HEAD_TO_HEAD_NO_INVITE_DATA						47  	// (15)
CONST_INT CORONA_MAKE_LOCAL_PLAYER_VISIBLE							48  	// (16)
CONST_INT CORONA_WAIT_FOR_GROUP_INVITES_TO_COMPLETE					49  	// (17)
CONST_INT CORONA_AUTOSTART_TRIGGERED								50  	// (18)
CONST_INT CORONA_AUTOSTART_LAUCNHED									51  	// (19)
CONST_INT CORONA_AUTOSTART_DELAYED									52  	// (20)
CONST_INT CORONA_DISPLAYING_THIRD_DETAIL_PANEL						53  	// (21)
CONST_INT CORONA_MISSION_DELAYED_FOR_TEAMS							54  	// (22)
CONST_INT CORONA_SOLO_PLAY_AVAILABLE								55  	// (23)
CONST_INT CORONA_CONTINUE_IS_PLAY									56  	// (24)
CONST_INT CORONA_DISPLAY_FLASHING_HELP_TEXT							57  	// (25)
CONST_INT CORONA_DISPLAYED_NEW_JOB_CORONA_HELP						58  	// (26)
CONST_INT CORONA_TUTORIAL_PLAY_PED_DIALOGUE							59  	// (27)
CONST_INT CORONA_TUTORIAL_PED_DIALOGUE_CHOSEN						60  	// (28)
CONST_INT CORONA_HOST_CHANGE										61  	// (29)
CONST_INT CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE					62  	// (30)
CONST_INT CORONA_HOST_BROADCAST_TEAM_EVENT							63  	// (31)
	
// iCoronaBitSet3						- NUM_CORONA_BITSETS = 3	
CONST_INT CORONA_TUTORIAL_INTRO_WAITING_INIT						64		// (0)
CONST_INT CORONA_TUTORIAL_INTRO_DISPLAY_TIME						65  	// (1)
CONST_INT CORONA_TUTORIAL_INTRO_DISPLAYING_TIME						66  	// (2)
CONST_INT CORONA_HOST_INVITE_FRIENDS_OF_FRIENDS						67  	// (3)
CONST_INT CORONA_PLAYER_HAS_BEEN_ON_HOST_JOIN						68  	// (4)
CONST_INT CORONA_PROCESS_SLOT_SELECTION_CAMERA						69  	// (5)
CONST_INT CORONA_HOST_HAS_EXTENDED_TIME								70  	// (6)
CONST_INT CORONA_HOST_TESTED_CONTACT_MISSION_CORONA					71  	// (7)
CONST_INT CORONA_MENU_ANIM_GREYED_OUT								72  	// (8)
CONST_INT CORONA_MENU_RADIO_FADE_OUT_STARTED						73  	// (9)
CONST_INT CORONA_IN_CORONA_CAMERA_INITIALISED						74  	// (10)
CONST_INT CORONA_MENU_CAR_AND_COLOUR_CLASH							75  	// (11)
CONST_INT CORONA_BUTTON_INVITING_PLAYERS							76  	// (12)
CONST_INT CORONA_BUTTON_NO_INVITES_SENT								77  	// (13)
CONST_INT CORONA_BUTTON_INVITING_PLAYERS_AUTOFILL					78  	// (14)
CONST_INT CORONA_BUTTONS_UPDATED_FOR_INVITES						79  	// (15)
CONST_INT CORONA_PROCESSING_GAMER_CREW_TAG							80  	// (16)
CONST_INT CORONA_PROCESSING_GAMER_CREW_TAG_INVITED					81  	// (17)
CONST_INT CORONA_PENDING_VEHICLE_STAT_UPDATE						82  	// (18)
CONST_INT CORONA_OKAY_TO_DO_CLASH_CHECK								83  	// (19)
CONST_INT CORONA_REQUESTED_PLAYER_RELATIONSHIP						84  	// (20)
CONST_INT CORONA_WEAPON_SAVED_OUT									85  	// (21)
CONST_INT CORONA_CHECK_TEAMS_FOR_BETTING_SCREEN						86  	// (22)
CONST_INT CORONA_UPDATE_TO_PLAYER_READY_STATUS						87  	// (23)
CONST_INT CORONA_PLAYER_ANIMTION_BUTTON_CHECK						88  	// (24)
CONST_INT CORONA_PLAYER_IS_IN_PARTY									89  	// (25)
CONST_INT CORONA_END_OF_JOB_FLUSH_TIMER								90  	// (26)
CONST_INT CORONA_END_OF_JOB_LIST_FLUSHED							91  	// (27)
CONST_INT CORONA_CHALLENGE_TAKE_CASH_THIS_JOB						92  	// (28)
CONST_INT CORONA_HOST_HAS_LOCKED_TEAMS								93  	// (29)
CONST_INT CORONA_PLAYER_HAS_SEEN_TUTORIAL_MENU						94  	// (30)
CONST_INT CORONA_WAIT_FOR_BETTING_TO_BECOME_AVAIL					95  	// (31)
	
// iCoronaBitSet4						- NUM_CORONA_BITSETS = 4	
CONST_INT CORONA_INIT_TIMER_FOR_TRANSITION_JOINING					96		// (0)
CONST_INT CORONA_STAT_CARD_AVAILABLE								97  	// (1)
CONST_INT CORONA_CONTACT_MISSION_IN_APARTMENT_INIT					98  	// (2)
CONST_INT CORONA_REQUESTING_NEW_PLAYER_CREW_INFO					99  	// (3)
CONST_INT CORONA_H2H_LOCK_CASH_OPTION								100 	// (4)
CONST_INT CORONA_H2H_CHARGE_PLAYER_FOR_CASH							101 	// (5)
CONST_INT CORONA_FULL_AMMO_NO_LONGER_AVAILABLE						102 	// (6)
CONST_INT CORONA_CHECKED_H2H_CASH_WAGER								103 	// (7)
CONST_INT CORONA_RANDOMISE_COLOUR_FOR_VEHICLE						104 	// (8)
CONST_INT CORONA_CLIENT_HAS_ACCESSED_JOINED_PLAYERS					105 	// (9)
CONST_INT CORONA_VEHICLE_SCREEN_DISPLAYING_COLOUR					106 	// (10)
CONST_INT CORONA_SCTV_COMPLETED_LAUNCHING_OF_TRANSITION				107 	// (11)
CONST_INT CORONA_SCTV_FORCE_CORONA_TO_LAUNCH_STATE					108 	// (12)
CONST_INT CORONA_SCTV_FULLY_SETUP_IN_CORONA_SCREEN					109 	// (13)
CONST_INT CORONA_SPECIAL_VEHICLE_HAS_BEEN_ALLOWED					110 	// (14)
CONST_INT CORONA_DISPLAYED_MINIGAME_HOST_TEXT						111 	// (15)
CONST_INT CORONA_DISPLAY_MAP										112 	// (16)
CONST_INT CORONA_HIDE_MAP											113 	// (17)
CONST_INT CORONA_DISPLAY_BETTING_HELP_TEXT							114 	// (18)
CONST_INT CORONA_NEW_VEHICLE_LOADING								115 	// (19)
CONST_INT CORONA_OUTFITS_ARE_AVAILABLE								116 	// (20)
CONST_INT CORONA_NO_TEAM_OUTFITS									117 	// (21)
CONST_INT CORONA_ACTION_INSTEAD_OF_WINNING_TOAST					118 	// (22)
CONST_INT CORONA_NO_RADIO											119 	// (23)
CONST_INT CORONA_KEEP_WALK_IN_MATCHMAKING_OPEN						120 	// (24)
CONST_INT CORONA_INITIAL_HEIST_PAYMENT								121 	// (25)
CONST_INT CORONA_HIDE_WINNING_TOAST									122 	// (26)
CONST_INT CORONA_ALLOW_INVITE_SCREEN_TO_BE_SKIPPED					123 	// (27)
CONST_INT CORONA_USE_ZOOMED_OUT_CAMERA_FOR_VEHICLES					124 	// (28)
CONST_INT CORONA_FP_CAMERA_SET_UP_FOR_CUTSCENE 						125 	// (29)
CONST_INT CORONA_BESPOKE_PED_PLACEMENT								126 	// (30)
CONST_INT CORONA_KEEP_SCRIPTED_CAMS_AFTER_CLEANUP					127 	// (31)
	
// iCoronaBitSet5						- NUM_CORONA_BITSETS = 5	
CONST_INT CORONA_HEIST_CUTSCENE_HAS_BEEN_VALIDATED					128		// (0)
CONST_INT CORONA_HEIST_FINALE_CUTSCENE_CAN_PLAY						129 	// (1)
CONST_INT CORONA_MISSION_HAS_OUTFITS								130 	// (2)
CONST_INT CORONA_PEDS_HAVE_ANIMATION_PLAYING						131 	// (3)
CONST_INT CORONA_PLAYER_LEFT_REQUIRES_CAM_UPDATE					132 	// (4)
CONST_INT CORONA_INVITE_CONTEXT_MENU_IS_ACTIVE						133 	// (5)
CONST_INT CORONA_GLOBAL_HEIST_FLAG									134 	// (6)
CONST_INT CORONA_FORCE_OPEN_TO_MM_ON_AUTOINVITE						135 	// (7)		A flag that we are to force the corona to open match making if this is a heist corona when auto invite is selected
CONST_INT CORONA_HEIST_SINGLE_TEAM_OUTFITS							136 	// (8)	
CONST_INT CORONA_PLAYER_CONSIDERED_ON_INTRO_SCREEN					137 	// (9)	
CONST_INT CORONA_HEIST_SINGLE_TEAM_OUTFITS_PREP						138 	// (10)	
CONST_INT CORONA_HEIST_MASKS_AVAILABLE								139 	// (11)	
CONST_INT CORONA_HEIST_LOCKED_TEAM_STYLE							140 	// (12)	
CONST_INT CORONA_HEIST_FLOW_WILL_BE_BROKEN_LOYALTY					141		// (13)		A flag to indicate if the flow of the Player's progress will be broken with this player setup
CONST_INT CORONA_OUTFIT_MASKS_SHOWN									142		// (14)		A flag used for the current state of the masks being show or not in the outfits
CONST_INT CORONA_OUTFIT_MASKS_HAVE_BEEN_UPDATED						143		// (15)		A flag to indicate the player has adjusted their masks
CONST_INT CORONA_GLOBAL_HEIST_FINALE_FLAG							144		// (16)		A global flag to indicate this is a finale heist
CONST_INT CORONA_HEIST_PLANNING_CAMERA								145		// (17)		A global flag to indicate that we're currently viewing the heist finale board from outfit selection.
CONST_INT CORONA_HEIST_PLANNING_INSTR_BUTTONS						146		// (18)	
CONST_INT CORONA_VERSUS_OUTFIT_STYLE_UPDATED						147		// (19)		A flag to indicate to the player a versus outfit style has occurred
CONST_INT CORONA_OUTFIT_UPDATED_LOCALLY								148		// (20)		A flag to indicate we have updated our outfit locally. We should then wait till we are streamed in before updating
CONST_INT CORONA_GLOBAL_HEIST_CUTSCENE_FLAG							149		// (21)		A flag to indicate the current corona is for a cutscene
CONST_INT CORONA_HEIST_CAMERA_FROM_BOARD							150 	// (22)	
CONST_INT CORONA_HEIST_PLAY_PISTOL_ANIM								151 	// (23) 	A flag to spawn in a pistol for the 3rd player slot in a single team heist mission
CONST_INT CORONA_UPDATE_LIGHT_RIG_ON_SWITCH							152		// (24)		A flag to ensure the light rigs are updated to the latest selected slot on the corona screen
CONST_INT CORONA_HEIST_PLAY_LOOPING_ANIM							153		// (25)		A flag to play the looping animaiton after the synced scene pistol intro animation
CONST_INT CORONA_REFRESH_MENU_FOR_CLIENT_INVITE						154		// (26)		A flag state whether the host has the client invite option displayed in corona menu
CONST_INT CORONA_VERSUS_HOST_IS_SCROLLING							155		// (27)		A flag to indicate the host is scrolling through the outfits quickly
CONST_INT CORONA_MATCHMAKING_UPDATED								156		// (28)		A flag to indicate our matchmaking option has changed locally
CONST_INT CORONA_HIDE_INVITE_FOR_FULL_LOBBY							157		// (29)		A flag to indicate the invite options have been hidden due to lobby being full
CONST_INT CORONA_PROCESS_ALL_PLAYERS_FOR_VERSUS_OUTFITS				158		// (30)		A flag to force a full player loop when there are some players updating their versus outfits
CONST_INT CORONA_PLAYER_FULLY_ON_INTRO_SCREEN						159		// (31)		A flag to indicate when a player has made it onto the intro screen
		
// iCoronaBitSet6						- NUM_CORONA_BITSETS = 6		
CONST_INT CORONA_CURRENT_VIEWED_TEAM_IS_FULL						160		// (0)		A flag that indicates the current viewed team has max or greater players in it
CONST_INT CORONA_HEIST_FLOW_WILL_BE_BROKEN_MASTERMIND				161		// (1)		A flag to indicate if the flow of the Player's progress will be broken with this player setup for mastermind challenge
CONST_INT CORONA_HEIST_HEELS_ADJUST_CAM_MOVEMENT        			162 	// (2)  	A flag to adjust the Y axis movement in a heist corona depending on if the player is wearing heels or not
CONST_INT CORONA_HEIST_FORCE_MATCHMAKING_SUPPRESSED					163		// (3)		A flag to indicate the player has updated the matchmaking so we should not update to it
CONST_INT CORONA_HEIST_SUSPEND_MIN_PLAYER_CHECK						164		// (4)		Has the minimum player check been suspended.
CONST_INT CORONA_MISSION_VEHICLE_HAS_MULTIPLE_CLASSES				165		// (5)		NG Only, Lowrider vehicle class check. Multiple classes to pick from.
CONST_INT CORONA_FULL_SCREEN_MAP_ACTIVE								166		// (6)		Full screen map is active in the corona, used to suppress calls to other natives
CONST_INT CORONA_RETURNING_FROM_FULL_SCREEN_MAP						167		// (7)		This is used as we close the full screen map and repopulate the initial screen
CONST_INT CORONA_AMMO_ARMOR_EXTERNAL_UPDATE							168		// (8)		An update to the ammo / armor status for the player has occurred external to the corona (PC)
CONST_INT CORONA_AUTO_INVITE_GATHERING_PLAYERS						169		// (9)		When set, 'Gathering additional players' will be displayed in the corona for auto inviting
CONST_INT CORONA_SCTV_LAUNCHED_JOB									170		// (10)		Set when the SCTV player who is in control of a playlist will launch the job
CONST_INT CORONA_REFRESH_PLAYER_TITLES								171		// (11)		Refresh all player titles following a fade to 0 alpha
CONST_INT CORONA_INVALID_ROLES										172		// (12)		Corona has invalid roles
CONST_INT CORONA_DELAYED_CORONA_FOR_TEAM_ROLES						173		// (13)		Have we delayed the launching of the corona for invalid roles
CONST_INT CORONA_ALLOW_ALL_OUTFITS									174		// (14)		Should all outfits be considere valid for this Job?
CONST_INT CORONA_ONE_OUTFIT_ACTIVE_FOR_MISSION						175		// (15)		Is only one outfit style turned on for this Job
CONST_INT CORONA_FORCE_LAUNCH_AND_REORGANISE_TEAMS					176		// (16)		Host forces the corona to launch and organises the team to make them valid
CONST_INT CORONA_FORCED_LAUNCH_INSTRUCTIONAL_BUTTONS				177		// (17)
CONST_INT CORONA_RESET_OUTFITS_DUE_TO_FORCE_LAUNCH					178		// (18)		nsures correct outfits are set when a heist is force launched - url:bugstar:2632381
CONST_INT CORONA_VEHICLE_SCREEN_DISPLAYING_LIVERY					179 	// (19)	
CONST_INT CORONA_SELECTING_FROM_PERSONAL_VEHICLES					180		// (20)		Flagged when player is selecting from personal vehicles in adversary modes
CONST_INT CORONA_SHOW_MAX_TEAM_DIFFERENCE_HELP						181		// (21)		Flagged when there is am imbalance between teams and we should show help to explain that there is > g_FMMC_STRUCT.iMaxTeamDifference team difference
CONST_INT CORONA_TRIGGERING_AUDIO_SCENE_FOR_ACTIVE_MODES			182		// (22)		Flag set when audio scenes are activated to suppress active mode scores
CONST_INT CORONA_UPDATE_NEW_PILOT									183		// (23)		Flag set when we receive a broadcast from the host that we need to update the selected pilot
CONST_INT CORONA_VEHICLE_SCREEN_DISPLAYING_PARACHUTE				184		// (24)		Flag set when showing parachute options on vehicle select screen
CONST_INT CORONA_GANG_OPS_PLANNING_SCRIPT_REQUESTED					185		// (25)		Flag set when the planning script has been requested by fmmc_launcher
CONST_INT CORONA_GANG_OPS_PLANNING_SCRIPT_LAUNCHED					186		// (26)		Flag set when the planning script has been launched
CONST_INT CORONA_GANG_OPS_PLANNING_SCRIPT_RUNNING					187		// (27)		Flag set when the planning script is running and the player is on the finale screen
CONST_INT CORONA_GANG_OPS_PLANNING_ENABLED_INTERACTION				188		// (28)		Flag set when the player is interacting with the planning screen
CONST_INT CORONA_GLOBAL_GANG_OPS_HEIST_FINALE_FLAG					189		// (29)		Flag to signify that this corona is for a gang ops finale
CONST_INT CORONA_GANGOPS_FLOW_AWARD_WILL_BE_BROKEN					190		// (30)		A flag to indicate if the flow of the Player's progress will be broken with this player setup for gangops challenge
CONST_INT CORONA_GANGOPS_FINALE_START_AUDIO_SCENE					191		// (31)		Flag to indicate that the gang ops finale audio scene has been started
		
// iCoronaBitSet7						- NUM_CORONA_BITSETS = 7		
CONST_INT CORONA_HOTRING_SERIES_DISABLE_OUTFIT_SELECTION			192		// (0)		A flag to disable race outfit selection in hotring series races
CONST_INT CORONA_VEHICLE_SCREEN_DISPLAYING_ARMOUR					193		// (1)		Flag set when showing armour option on vehicle select screen
CONST_INT CORONA_VEHICLE_SCREEN_DISPLAYING_SCOOPS_RAMBARS			194		// (2)		Flag set when showing scoops/rambars option on vehicle select screen
CONST_INT CORONA_VEHICLE_SCREEN_DISPLAYING_MINES					195		// (3)		Flag set when showing mines option on vehicle select screen
CONST_INT CORONA_VEHICLE_SCREEN_REFRESHED_VEHICLE_DATA				196		// (4)		Flag set when we move to vehicle selection in generic jobs after refreshing vehicle data
CONST_INT CORONA_LOADED_JOB_IPL										197		// (5)		Flag set when we have completed loading of the arena interior
CONST_INT CORONA_VEHICLE_SELECTION_DISPLAY_VEH_STATS				198		// (6)		Flag set when player has pressed X to view vehicle stats instead of mod costs in arena wars jobs
CONST_INT CORONA_VEHICLE_SELECTION_VEH_STATS_BUTTON_HIDDEN			199		// (7)		Flag set when player is selecting from stock vehicles
CONST_INT CORONA_VEHICLE_CUSTOM_COLOUR_SET							200		// (8)		Flag set when a custom vehicle colour is set in the corona (crew colour)
CONST_INT CORONA_GENERIC_JOB_RECREATE_CAMERA						201		// (9)		Flag set when the camera/team view is switched in a generic job corona, so we know to recreate the camera once we move to vehicle selection
CONST_INT CORONA_VEHICLE_SELECTION_VOTE_ENABLED						202		// (10)		Flag set when we've received a sync from the server to know players can vote on the vehicle select screen safely
CONST_INT CORONA_VEHICLE_SCREEN_DISPLAYING_PURCHASE_MOD				203		// (11)		Flag set when the Purchase Mods row is being displayed on vehicle select screen
CONST_INT CORONA_USE_CORONA_CENTER_POS_FOR_VEHICLE					204		// (12)		Flag set to force using the centre point of the corona for the next vehicle to load
CONST_INT CORONA_VEHICLE_VECTOR_CACHED								205		// (13)		Flag set when caching the corona vehicle vector for reuse
CONST_INT CORONA_ARENA_WARS_SET_CROWD_AUDIO							206		// (14)		Flag set when starting the crowd audio for arena jobs
CONST_INT CORONA_UPDATE_VEHICLE_DETAILS								207		// (15)		Flag set when moving from team DM to vehicle select in generic jobs
CONST_INT CORONA_VEHICLE_MODS_PRELOADING							208		// (16)		Flag set when vehicle mods are preloading
CONST_INT CORONA_VEHICLE_MODS_PRELOADED								209		// (17)		Flag set when vehicle mods have been preloaded
CONST_INT CORONA_ARENA_WARS_LOBBY_AUDIO_SCENE_ACTIVE				210		// (18)		Flag set when the unique arena wars audio scene is activated
CONST_INT CORONA_GENERIC_JOB_DIALOGUE_SAFE_TO_PLAY					211		// (19)		Flag set when it is safe to trigger dialogue in generic jobs
CONST_INT CORONA_GENERIC_JOB_DIALOGUE_ONE_SHOT_DONE					212		// (20)		Flag set when triggering one shot dialogue in generic jobs
CONST_INT CORONA_POST_TRANSITION_CAMERA_CUT_DONE					213		// (21)		Flag set when we initially cut camera for team/vehile/betting screens
CONST_INT CORONA_BINK_VIDEO_MUSIC_EVENT_TRIGGERED					214		// (22)		Flag set when music event for corona bink video has been triggered
CONST_INT CORONA_BINK_VIDEO_VEHICLE_SELECT_MUSIC_EVENT_TRIGGERED	215		// (23)		Flag set when music event for corona lobby video transition to team/vehicle select is triggered
CONST_INT CORONA_BINK_VIDEO_WORLD_MUTED								216		// (24)		Flag set when the world is muted behind the corona bink video
CONST_INT CORONA_SELECTED_RANDOM_ARENA_LIGHTING						217		// (25)		Flag set when arena lighting is initialised
CONST_INT CORONA_ARENA_TUTORIAL_HELP_SHOWN							218		// (26)		Flag set when tutorial event help has been shown
CONST_INT CORONA_ANNOUNCER_FINISHED_EVENT_TRIGGERED					219		// (27)		Flag set when announcer dialogue has finished and music event has triggered
CONST_INT CORONA_IS_RCBANDITO_RACE									220		// (28)		Flag set when in an rc bandito race
CONST_INT CORONA_IS_KING_OF_THE_HILL								221		// (29)		Flag set when the lobby is started with deathmatch mission subtype set to FMMC_KING_OF_THE_HILL or FMMC_KING_OF_THE_HILL_TEAM
CONST_INT CORONA_IS_KING_OF_THE_HILL_TEAM							222		// (30)		Flag set when the lobby is started with deathmatch mission subtype set specifically to FMMC_KING_OF_THE_HILL_TEAM
CONST_INT CORONA_CUTSCENE_FAILED_TO_START							223		// (31)		Flag set when a corona cutscene fails to start and needs to be unloaded/released during cleanup

// iCoronaBitSet7						- NUM_CORONA_BITSETS = 8	
CONST_INT CORONA_BITSET_BLOCK_SKYSWOOP_HIDE_FOR_CASINO				224		// (0)		Flag set when the corona blocks the hiding of the skycam up inside the corona
CONST_INT CORONA_FLUSHED_CUTSCENE_STRUCT_IF_NECESSARY				225		// (1)		Flag set when entering the flow cutscene state after checking whether a struct flush is required
CONST_INT CORONA_GPS_FLAGS_SET										226		// (2)		
CONST_INT CORONA_VEHICLE_SCREEN_DISPLAYING_TYRES					227		// (3)
CONST_INT CORONA_GLOBAL_CASINO_HEIST_FINALE_FLAG					228 	// (4)
CONST_INT CORONA_CASINO_HEIST_PLANNING_BOARD_SCRIPT_REQUESTED		229		// (5)
CONST_INT CORONA_CASINO_HEIST_PLANNING_BOARD_SCRIPT_LAUNCHED		230		// (6)
CONST_INT CORONA_CASINO_HEIST_PLANNING_BOARD_SCRIPT_RUNNING			231		// (7)
CONST_INT CORONA_FADED_SCREEN_OUT									232		// (8)
CONST_INT CORONA_CASINO_HEIST_CUTSCENE_1_VIEWED						233		// (9)
CONST_INT CORONA_CASINO_HEIST_MAINTAIN_BOARDS						234		// (10)
CONST_INT CORONA_CASINO_HEIST_READY_TO_START						235		// (11)
CONST_INT CORONA_FROZEN_FOR_CASINO_HEIST_PLANNING					236		// (12)
CONST_INT CORONA_FREEZE_FOR_CASINO_HEIST_PLANNING					237		// (13)
CONST_INT CORONA_GLOBAL_GENERIC_HEIST_FINALE_FLAG					238		// (14)
CONST_INT CORONA_GENERIC_HEIST_MAINTAIN_PLANNING					239		// (15)
CONST_INT CORONA_GENERIC_HEIST_READY_TO_START						240		// (16)
CONST_INT CORONA_GENERIC_HEIST_PLANNING_SCRIPT_REQUESTED			241		// (17)
CONST_INT CORONA_GENERIC_HEIST_PLANNING_SCRIPT_LAUNCHED				242		// (18)
CONST_INT CORONA_GENERIC_HEIST_PLANNING_SCRIPT_RUNNING				243		// (19)
CONST_INT CORONA_GENERIC_HEIST_START_INTERACTION					244		// (20)
CONST_INT CORONA_DOWNLOAD_ISLAND_HEIST_MISSION						245		// (21)
CONST_INT CORONA_ISLAND_HEIST_MISSION_DOWNLOADED					246		// (22)
CONST_INT CORONA_BROADCAST_OUTFIT_CHANGE							247		// (23)
CONST_INT CORONA_FIXER_MISSION_HAS_OUTFITS							248		// (24)
CONST_INT CORONA_FIXER_SHORT_TRIP_INTRO								249		// (25)
CONST_INT CORONA_USE_CUSTOM_UI_FOR_JOB_LOBBY						250		// (26)
CONST_INT CORONA_SUPPRESS_AUTO_START_COUNTDOWN						251		// (27)
CONST_INT CORONA_LAUNCH_FIXER_HQ									252		// (28)
CONST_INT CORONA_VEHICLE_SCREEN_DISPLAYING_OUTFITS					253		// (29)
CONST_INT CORONA_VEHICLE_SCREEN_DISPLAYING_HELMETS					254		// (30)

ENUM CORONA_INVITE_TYPE
	CORONA_INVITE_TYPE_STANDARD,
	CORONA_INVITE_TYPE_PRESENCE
ENDENUM

#IF IS_DEBUG_BUILD
FUNC STRING GET_CORONA_BIT_NAME_FOR_DEBUG(INT iCoronaBit)
	SWITCH iCoronaBit
		CASE CORONA_ACTIVE_HOST_OF_MENU															RETURN "CORONA_ACTIVE_HOST_OF_MENU"
		CASE CORONA_CLIENT_ABLE_TO_PLAY                                                         RETURN "CORONA_CLIENT_ABLE_TO_PLAY"
		CASE CORONA_VEHICLE_SELECTOR_DISPLAYING                                                 RETURN "CORONA_VEHICLE_SELECTOR_DISPLAYING"
		CASE CORONA_BETTING_VISUAL_ENABLED                                                      RETURN "CORONA_BETTING_VISUAL_ENABLED"
		CASE CORONA_PLAYER_IN_SKYSWOOP                                                          RETURN "CORONA_PLAYER_IN_SKYSWOOP"
		CASE CORONA_PLAYER_ON_TUTORIAL                                                          RETURN "CORONA_PLAYER_ON_TUTORIAL"
		CASE CORONA_LAUNCHED_FM_PLAYERS_IN_CORONA                                               RETURN "CORONA_LAUNCHED_FM_PLAYERS_IN_CORONA"
		CASE CORONA_RETURNING_FROM_INVITE_SCREEN                                                RETURN "CORONA_RETURNING_FROM_INVITE_SCREEN"
		CASE CORONA_SKIP_WAIT_FOR_FRONTEND                                                      RETURN "CORONA_SKIP_WAIT_FOR_FRONTEND"
		CASE CORONA_ADJUSTING_CASH_VALUE				                                        RETURN "CORONA_ADJUSTING_CASH_VALUE"
		CASE CORONA_AUTOFILL_CAN_CONTINUE				                                        RETURN "CORONA_AUTOFILL_CAN_CONTINUE"
		CASE CORONA_AUTOFILL_IS_NOT_AVAILABLE			                                        RETURN "CORONA_AUTOFILL_IS_NOT_AVAILABLE"
		CASE CORONA_PLAYER_SENT_INVITE_ALL				                                        RETURN "CORONA_PLAYER_SENT_INVITE_ALL"
		CASE CORONA_READY_UP_15_TIMEOUT					                                        RETURN "CORONA_READY_UP_15_TIMEOUT"
		CASE CORONA_PLAYER_PLAY_ANIM					                                        RETURN "CORONA_PLAYER_PLAY_ANIM"
		CASE CORONA_PLAYER_PLAYING_ANIM					                                        RETURN "CORONA_PLAYER_PLAYING_ANIM"
		CASE CORONA_MISSION_IS_COOP						                                        RETURN "CORONA_MISSION_IS_COOP"
		CASE CORONA_BETTING_MENU_HIDDEN_FOR_ANIM		                                        RETURN "CORONA_BETTING_MENU_HIDDEN_FOR_ANIM"
		CASE CORONA_BETTING_IS_LOCKED					                                        RETURN "CORONA_BETTING_IS_LOCKED"
		CASE CORONA_PLAYER_TEAMS_HAVE_BEEN_PROCESSED	                                        RETURN "CORONA_PLAYER_TEAMS_HAVE_BEEN_PROCESSED"
		CASE CORONA_PLAYER_LAUNCHING_SOLO_JOB			                                        RETURN "CORONA_PLAYER_LAUNCHING_SOLO_JOB"
		CASE CORONA_AUTOFILL_SENT						                                        RETURN "CORONA_AUTOFILL_SENT"
		CASE CORONA_PLAYER_VEHICLE_COLOURS_RANDOM		                                        RETURN "CORONA_PLAYER_VEHICLE_COLOURS_RANDOM"
		CASE CORONA_INVITED_PLAYER_LIST_UPDATED			                                        RETURN "CORONA_INVITED_PLAYER_LIST_UPDATED"
		CASE CORONA_INITIALISE_VEHICLE_COLOURS			                                        RETURN "CORONA_INITIALISE_VEHICLE_COLOURS"
		CASE CORONA_HAS_PLAYER_SAVED_VEHICLE			                                        RETURN "CORONA_HAS_PLAYER_SAVED_VEHICLE"
		CASE CORONA_DISPLAYING_CLIENT_CHOSEN_TEAM		                                        RETURN "CORONA_DISPLAYING_CLIENT_CHOSEN_TEAM"
		CASE CORONA_AUTOFILL_SENT_TO_LARGE_GROUPS		                                        RETURN "CORONA_AUTOFILL_SENT_TO_LARGE_GROUPS"
		CASE CORONA_PULSE_READY_UP						                                        RETURN "CORONA_PULSE_READY_UP"
		CASE CORONA_CORONA_HELP_DISPLAYING				                                        RETURN "CORONA_CORONA_HELP_DISPLAYING"
		CASE CORONA_VEHICLE_COLOURS_DEFAULTED			                                        RETURN "CORONA_VEHICLE_COLOURS_DEFAULTED"
		CASE CORONA_REFRESH_RECENT_PLAYER_PED			                                        RETURN "CORONA_REFRESH_RECENT_PLAYER_PED"
		
		CASE CORONA_REQUEST_BETTING_PLAYER_CREW_EMBLEM	                                        RETURN "CORONA_REQUEST_BETTING_PLAYER_CREW_EMBLEM"
		CASE CORONA_VEHICLE_SELECTION_UNAVAILABLE		                                        RETURN "CORONA_VEHICLE_SELECTION_UNAVAILABLE"
		CASE CORONA_SPLIT_COMPARE_DISPLAYED				                                        RETURN "CORONA_SPLIT_COMPARE_DISPLAYED"
		CASE CORONA_HOST_SELECTED_THEIR_PARTNER			                                        RETURN "CORONA_HOST_SELECTED_THEIR_PARTNER"
		CASE CORONA_PHOTO_HAS_BEEN_INITIALISED			                                        RETURN "CORONA_PHOTO_HAS_BEEN_INITIALISED"
		CASE CORONA_DETAILS_SCREEN_HAS_BEEN_VISITED		                                        RETURN "CORONA_DETAILS_SCREEN_HAS_BEEN_VISITED"
		CASE CORONA_RECENT_PLAYER_PED_DISPLAYED			                                        RETURN "CORONA_RECENT_PLAYER_PED_DISPLAYED"
		CASE CORONA_TRIGGER_PERSISTENT_MUSIC			                                        RETURN "CORONA_TRIGGER_PERSISTENT_MUSIC"
		CASE CORONA_TUTORIAL_PED_HEADSHOT_LOADED		                                        RETURN "CORONA_TUTORIAL_PED_HEADSHOT_LOADED"
		CASE CORONA_REQUESTED_TRANSITION_SCREEN_HEADER	                                        RETURN "CORONA_REQUESTED_TRANSITION_SCREEN_HEADER"
		CASE CORONA_LEADERBOARD_HAS_EXITED				                                        RETURN "CORONA_LEADERBOARD_HAS_EXITED"
		CASE CORONA_TRIGGERED_EARLY_LOAD_SCENE			                                        RETURN "CORONA_TRIGGERED_EARLY_LOAD_SCENE"
		CASE CORONA_PLAYER_PLAYING_QUICK_PLAY_ANIM		                                        RETURN "CORONA_PLAYER_PLAYING_QUICK_PLAY_ANIM"
		CASE CORONA_HEAD_TO_HEAD_WAIT_FOR_MATCHED_CREWS	                                        RETURN "CORONA_HEAD_TO_HEAD_WAIT_FOR_MATCHED_CREWS"
		CASE CORONA_HEAD_TO_HEAD_WAIT_FOR_FRIENDS_CREWS	                                        RETURN "CORONA_HEAD_TO_HEAD_WAIT_FOR_FRIENDS_CREWS"
		CASE CORONA_HEAD_TO_HEAD_NO_INVITE_DATA			                                        RETURN "CORONA_HEAD_TO_HEAD_NO_INVITE_DATA"
		CASE CORONA_MAKE_LOCAL_PLAYER_VISIBLE			                                        RETURN "CORONA_MAKE_LOCAL_PLAYER_VISIBLE"
		CASE CORONA_WAIT_FOR_GROUP_INVITES_TO_COMPLETE	                                        RETURN "CORONA_WAIT_FOR_GROUP_INVITES_TO_COMPLETE"
		CASE CORONA_AUTOSTART_TRIGGERED					                                        RETURN "CORONA_AUTOSTART_TRIGGERED"
		CASE CORONA_AUTOSTART_LAUCNHED					                                        RETURN "CORONA_AUTOSTART_LAUCNHED"
		CASE CORONA_AUTOSTART_DELAYED					                                        RETURN "CORONA_AUTOSTART_DELAYED"
		CASE CORONA_DISPLAYING_THIRD_DETAIL_PANEL		                                        RETURN "CORONA_DISPLAYING_THIRD_DETAIL_PANEL"
		CASE CORONA_MISSION_DELAYED_FOR_TEAMS			                                        RETURN "CORONA_MISSION_DELAYED_FOR_TEAMS"
		CASE CORONA_SOLO_PLAY_AVAILABLE					                                        RETURN "CORONA_SOLO_PLAY_AVAILABLE"
		CASE CORONA_CONTINUE_IS_PLAY					                                        RETURN "CORONA_CONTINUE_IS_PLAY"
		CASE CORONA_DISPLAY_FLASHING_HELP_TEXT			                                        RETURN "CORONA_DISPLAY_FLASHING_HELP_TEXT"
		CASE CORONA_DISPLAYED_NEW_JOB_CORONA_HELP		                                        RETURN "CORONA_DISPLAYED_NEW_JOB_CORONA_HELP"
		CASE CORONA_TUTORIAL_PLAY_PED_DIALOGUE			                                        RETURN "CORONA_TUTORIAL_PLAY_PED_DIALOGUE"
		CASE CORONA_TUTORIAL_PED_DIALOGUE_CHOSEN		                                        RETURN "CORONA_TUTORIAL_PED_DIALOGUE_CHOSEN"
		CASE CORONA_HOST_CHANGE							                                        RETURN "CORONA_HOST_CHANGE"
		CASE CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE	                                        RETURN "CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE"
		CASE CORONA_HOST_BROADCAST_TEAM_EVENT			                                        RETURN "CORONA_HOST_BROADCAST_TEAM_EVENT"
	ENDSWITCH
		
	SWITCH iCoronaBit
		CASE CORONA_TUTORIAL_INTRO_WAITING_INIT			                                        RETURN "CORONA_TUTORIAL_INTRO_WAITING_INIT"
		CASE CORONA_TUTORIAL_INTRO_DISPLAY_TIME			                                        RETURN "CORONA_TUTORIAL_INTRO_DISPLAY_TIME"
		CASE CORONA_TUTORIAL_INTRO_DISPLAYING_TIME		                                        RETURN "CORONA_TUTORIAL_INTRO_DISPLAYING_TIME"
		CASE CORONA_HOST_INVITE_FRIENDS_OF_FRIENDS		                                        RETURN "CORONA_HOST_INVITE_FRIENDS_OF_FRIENDS"
		CASE CORONA_PLAYER_HAS_BEEN_ON_HOST_JOIN		                                        RETURN "CORONA_PLAYER_HAS_BEEN_ON_HOST_JOIN"
		CASE CORONA_PROCESS_SLOT_SELECTION_CAMERA		                                        RETURN "CORONA_PROCESS_SLOT_SELECTION_CAMERA"
		CASE CORONA_HOST_HAS_EXTENDED_TIME				                                        RETURN "CORONA_HOST_HAS_EXTENDED_TIME"
		CASE CORONA_HOST_TESTED_CONTACT_MISSION_CORONA	                                        RETURN "CORONA_HOST_TESTED_CONTACT_MISSION_CORONA"
		CASE CORONA_MENU_ANIM_GREYED_OUT				                                        RETURN "CORONA_MENU_ANIM_GREYED_OUT"
		CASE CORONA_MENU_RADIO_FADE_OUT_STARTED			                                        RETURN "CORONA_MENU_RADIO_FADE_OUT_STARTED"
		CASE CORONA_IN_CORONA_CAMERA_INITIALISED		                                        RETURN "CORONA_IN_CORONA_CAMERA_INITIALISED"
		CASE CORONA_MENU_CAR_AND_COLOUR_CLASH			                                        RETURN "CORONA_MENU_CAR_AND_COLOUR_CLASH"
		CASE CORONA_BUTTON_INVITING_PLAYERS				                                        RETURN "CORONA_BUTTON_INVITING_PLAYERS"
		CASE CORONA_BUTTON_NO_INVITES_SENT				                                        RETURN "CORONA_BUTTON_NO_INVITES_SENT"
		CASE CORONA_BUTTON_INVITING_PLAYERS_AUTOFILL	                                        RETURN "CORONA_BUTTON_INVITING_PLAYERS_AUTOFILL"
		CASE CORONA_BUTTONS_UPDATED_FOR_INVITES			                                        RETURN "CORONA_BUTTONS_UPDATED_FOR_INVITES"
		CASE CORONA_PROCESSING_GAMER_CREW_TAG			                                        RETURN "CORONA_PROCESSING_GAMER_CREW_TAG"
		CASE CORONA_PROCESSING_GAMER_CREW_TAG_INVITED	                                        RETURN "CORONA_PROCESSING_GAMER_CREW_TAG_INVITED"
		CASE CORONA_PENDING_VEHICLE_STAT_UPDATE			                                        RETURN "CORONA_PENDING_VEHICLE_STAT_UPDATE"
		CASE CORONA_OKAY_TO_DO_CLASH_CHECK				                                        RETURN "CORONA_OKAY_TO_DO_CLASH_CHECK"
		CASE CORONA_REQUESTED_PLAYER_RELATIONSHIP		                                        RETURN "CORONA_REQUESTED_PLAYER_RELATIONSHIP"
		CASE CORONA_WEAPON_SAVED_OUT					                                        RETURN "CORONA_WEAPON_SAVED_OUT"
		CASE CORONA_CHECK_TEAMS_FOR_BETTING_SCREEN		                                        RETURN "CORONA_CHECK_TEAMS_FOR_BETTING_SCREEN"
		CASE CORONA_UPDATE_TO_PLAYER_READY_STATUS		                                        RETURN "CORONA_UPDATE_TO_PLAYER_READY_STATUS"
		CASE CORONA_PLAYER_ANIMTION_BUTTON_CHECK		                                        RETURN "CORONA_PLAYER_ANIMTION_BUTTON_CHECK"
		CASE CORONA_PLAYER_IS_IN_PARTY					                                        RETURN "CORONA_PLAYER_IS_IN_PARTY"
		CASE CORONA_END_OF_JOB_FLUSH_TIMER				                                        RETURN "CORONA_END_OF_JOB_FLUSH_TIMER"
		CASE CORONA_END_OF_JOB_LIST_FLUSHED				                                        RETURN "CORONA_END_OF_JOB_LIST_FLUSHED"
		CASE CORONA_CHALLENGE_TAKE_CASH_THIS_JOB		                                        RETURN "CORONA_CHALLENGE_TAKE_CASH_THIS_JOB"
		CASE CORONA_HOST_HAS_LOCKED_TEAMS				                                        RETURN "CORONA_HOST_HAS_LOCKED_TEAMS"
		CASE CORONA_PLAYER_HAS_SEEN_TUTORIAL_MENU		                                        RETURN "CORONA_PLAYER_HAS_SEEN_TUTORIAL_MENU"
		CASE CORONA_WAIT_FOR_BETTING_TO_BECOME_AVAIL	                                        RETURN "CORONA_WAIT_FOR_BETTING_TO_BECOME_AVAIL"
		
		CASE CORONA_INIT_TIMER_FOR_TRANSITION_JOINING	                                        RETURN "CORONA_INIT_TIMER_FOR_TRANSITION_JOINING"
		CASE CORONA_STAT_CARD_AVAILABLE					                                        RETURN "CORONA_STAT_CARD_AVAILABLE"
		CASE CORONA_CONTACT_MISSION_IN_APARTMENT_INIT	                                        RETURN "CORONA_CONTACT_MISSION_IN_APARTMENT_INIT"
		CASE CORONA_REQUESTING_NEW_PLAYER_CREW_INFO		                                        RETURN "CORONA_REQUESTING_NEW_PLAYER_CREW_INFO"
		CASE CORONA_H2H_LOCK_CASH_OPTION				                                        RETURN "CORONA_H2H_LOCK_CASH_OPTION"
		CASE CORONA_H2H_CHARGE_PLAYER_FOR_CASH			                                        RETURN "CORONA_H2H_CHARGE_PLAYER_FOR_CASH"
		CASE CORONA_FULL_AMMO_NO_LONGER_AVAILABLE		                                        RETURN "CORONA_FULL_AMMO_NO_LONGER_AVAILABLE"
		CASE CORONA_CHECKED_H2H_CASH_WAGER				                                        RETURN "CORONA_CHECKED_H2H_CASH_WAGER"
		CASE CORONA_RANDOMISE_COLOUR_FOR_VEHICLE		                                        RETURN "CORONA_RANDOMISE_COLOUR_FOR_VEHICLE"
		CASE CORONA_CLIENT_HAS_ACCESSED_JOINED_PLAYERS	                                        RETURN "CORONA_CLIENT_HAS_ACCESSED_JOINED_PLAYERS"
		CASE CORONA_VEHICLE_SCREEN_DISPLAYING_COLOUR	                                        RETURN "CORONA_VEHICLE_SCREEN_DISPLAYING_COLOUR"
		CASE CORONA_SCTV_COMPLETED_LAUNCHING_OF_TRANSITION                                      RETURN "CORONA_SCTV_COMPLETED_LAUNCHING_OF_TRANSITION"
		CASE CORONA_SCTV_FORCE_CORONA_TO_LAUNCH_STATE	                                        RETURN "CORONA_SCTV_FORCE_CORONA_TO_LAUNCH_STATE"
		CASE CORONA_SCTV_FULLY_SETUP_IN_CORONA_SCREEN	                                        RETURN "CORONA_SCTV_FULLY_SETUP_IN_CORONA_SCREEN"
		CASE CORONA_SPECIAL_VEHICLE_HAS_BEEN_ALLOWED	                                        RETURN "CORONA_SPECIAL_VEHICLE_HAS_BEEN_ALLOWED"
		CASE CORONA_DISPLAYED_MINIGAME_HOST_TEXT		                                        RETURN "CORONA_DISPLAYED_MINIGAME_HOST_TEXT"
		CASE CORONA_DISPLAY_MAP							                                        RETURN "CORONA_DISPLAY_MAP"
		CASE CORONA_HIDE_MAP							                                        RETURN "CORONA_HIDE_MAP"
		CASE CORONA_DISPLAY_BETTING_HELP_TEXT			                                        RETURN "CORONA_DISPLAY_BETTING_HELP_TEXT"
		CASE CORONA_NEW_VEHICLE_LOADING					                                        RETURN "CORONA_NEW_VEHICLE_LOADING"
		CASE CORONA_OUTFITS_ARE_AVAILABLE				                                        RETURN "CORONA_OUTFITS_ARE_AVAILABLE"
		CASE CORONA_NO_TEAM_OUTFITS						                                        RETURN "CORONA_NO_TEAM_OUTFITS"
		CASE CORONA_ACTION_INSTEAD_OF_WINNING_TOAST		                                        RETURN "CORONA_ACTION_INSTEAD_OF_WINNING_TOAST"
		CASE CORONA_NO_RADIO							                                        RETURN "CORONA_NO_RADIO"
		CASE CORONA_KEEP_WALK_IN_MATCHMAKING_OPEN		                                        RETURN "CORONA_KEEP_WALK_IN_MATCHMAKING_OPEN"
		CASE CORONA_INITIAL_HEIST_PAYMENT				                                        RETURN "CORONA_INITIAL_HEIST_PAYMENT"
		CASE CORONA_HIDE_WINNING_TOAST					                                        RETURN "CORONA_HIDE_WINNING_TOAST"
		CASE CORONA_ALLOW_INVITE_SCREEN_TO_BE_SKIPPED	                                        RETURN "CORONA_ALLOW_INVITE_SCREEN_TO_BE_SKIPPED"
		CASE CORONA_USE_ZOOMED_OUT_CAMERA_FOR_VEHICLES	                                        RETURN "CORONA_USE_ZOOMED_OUT_CAMERA_FOR_VEHICLES"
		CASE CORONA_FP_CAMERA_SET_UP_FOR_CUTSCENE 		                                        RETURN "CORONA_FP_CAMERA_SET_UP_FOR_CUTSCENE"
		CASE CORONA_BESPOKE_PED_PLACEMENT				                                        RETURN "CORONA_BESPOKE_PED_PLACEMENT"
		CASE CORONA_KEEP_SCRIPTED_CAMS_AFTER_CLEANUP	                                        RETURN "CORONA_KEEP_SCRIPTED_CAMS_AFTER_CLEANUP"
	ENDSWITCH
		
	SWITCH iCoronaBit
		CASE CORONA_HEIST_CUTSCENE_HAS_BEEN_VALIDATED	                                        RETURN "CORONA_HEIST_CUTSCENE_HAS_BEEN_VALIDATED"
		CASE CORONA_HEIST_FINALE_CUTSCENE_CAN_PLAY		                                        RETURN "CORONA_HEIST_FINALE_CUTSCENE_CAN_PLAY"
		CASE CORONA_MISSION_HAS_OUTFITS					                                        RETURN "CORONA_MISSION_HAS_OUTFITS"
		CASE CORONA_PEDS_HAVE_ANIMATION_PLAYING			                                        RETURN "CORONA_PEDS_HAVE_ANIMATION_PLAYING"
		CASE CORONA_PLAYER_LEFT_REQUIRES_CAM_UPDATE		                                        RETURN "CORONA_PLAYER_LEFT_REQUIRES_CAM_UPDATE"
		CASE CORONA_INVITE_CONTEXT_MENU_IS_ACTIVE		                                        RETURN "CORONA_INVITE_CONTEXT_MENU_IS_ACTIVE"
		CASE CORONA_GLOBAL_HEIST_FLAG					                                        RETURN "CORONA_GLOBAL_HEIST_FLAG"
		CASE CORONA_FORCE_OPEN_TO_MM_ON_AUTOINVITE		                                        RETURN "CORONA_FORCE_OPEN_TO_MM_ON_AUTOINVITE"
		CASE CORONA_HEIST_SINGLE_TEAM_OUTFITS			                                        RETURN "CORONA_HEIST_SINGLE_TEAM_OUTFITS"
		CASE CORONA_PLAYER_CONSIDERED_ON_INTRO_SCREEN	                                        RETURN "CORONA_PLAYER_CONSIDERED_ON_INTRO_SCREEN"
		CASE CORONA_HEIST_SINGLE_TEAM_OUTFITS_PREP		                                        RETURN "CORONA_HEIST_SINGLE_TEAM_OUTFITS_PREP"
		CASE CORONA_HEIST_MASKS_AVAILABLE				                                        RETURN "CORONA_HEIST_MASKS_AVAILABLE"
		CASE CORONA_HEIST_LOCKED_TEAM_STYLE				                                        RETURN "CORONA_HEIST_LOCKED_TEAM_STYLE"
		CASE CORONA_HEIST_FLOW_WILL_BE_BROKEN_LOYALTY	                                        RETURN "CORONA_HEIST_FLOW_WILL_BE_BROKEN_LOYALTY"
		CASE CORONA_OUTFIT_MASKS_SHOWN					                                        RETURN "CORONA_OUTFIT_MASKS_SHOWN"
		CASE CORONA_OUTFIT_MASKS_HAVE_BEEN_UPDATED		                                        RETURN "CORONA_OUTFIT_MASKS_HAVE_BEEN_UPDATED"
		CASE CORONA_GLOBAL_HEIST_FINALE_FLAG			                                        RETURN "CORONA_GLOBAL_HEIST_FINALE_FLAG"
		CASE CORONA_HEIST_PLANNING_CAMERA				                                        RETURN "CORONA_HEIST_PLANNING_CAMERA"
		CASE CORONA_HEIST_PLANNING_INSTR_BUTTONS		                                        RETURN "CORONA_HEIST_PLANNING_INSTR_BUTTONS"
		CASE CORONA_VERSUS_OUTFIT_STYLE_UPDATED			                                        RETURN "CORONA_VERSUS_OUTFIT_STYLE_UPDATED"
		CASE CORONA_OUTFIT_UPDATED_LOCALLY				                                        RETURN "CORONA_OUTFIT_UPDATED_LOCALLY"
		CASE CORONA_GLOBAL_HEIST_CUTSCENE_FLAG			                                        RETURN "CORONA_GLOBAL_HEIST_CUTSCENE_FLAG"
		CASE CORONA_HEIST_CAMERA_FROM_BOARD				                                        RETURN "CORONA_HEIST_CAMERA_FROM_BOARD"
		CASE CORONA_HEIST_PLAY_PISTOL_ANIM				                                        RETURN "CORONA_HEIST_PLAY_PISTOL_ANIM"
		CASE CORONA_UPDATE_LIGHT_RIG_ON_SWITCH			                                        RETURN "CORONA_UPDATE_LIGHT_RIG_ON_SWITCH"
		CASE CORONA_HEIST_PLAY_LOOPING_ANIM				                                        RETURN "CORONA_HEIST_PLAY_LOOPING_ANIM"
		CASE CORONA_REFRESH_MENU_FOR_CLIENT_INVITE		                                        RETURN "CORONA_REFRESH_MENU_FOR_CLIENT_INVITE"
		CASE CORONA_VERSUS_HOST_IS_SCROLLING			                                        RETURN "CORONA_VERSUS_HOST_IS_SCROLLING"
		CASE CORONA_MATCHMAKING_UPDATED					                                        RETURN "CORONA_MATCHMAKING_UPDATED"
		CASE CORONA_HIDE_INVITE_FOR_FULL_LOBBY			                                        RETURN "CORONA_HIDE_INVITE_FOR_FULL_LOBBY"
		CASE CORONA_PROCESS_ALL_PLAYERS_FOR_VERSUS_OUTFITS                                      RETURN "CORONA_PROCESS_ALL_PLAYERS_FOR_VERSUS_OUTFITS"
		CASE CORONA_PLAYER_FULLY_ON_INTRO_SCREEN			                                    RETURN "CORONA_PLAYER_FULLY_ON_INTRO_SCREEN"
		
		CASE CORONA_CURRENT_VIEWED_TEAM_IS_FULL				                                    RETURN "CORONA_CURRENT_VIEWED_TEAM_IS_FULL"
		CASE CORONA_HEIST_FLOW_WILL_BE_BROKEN_MASTERMIND	                                    RETURN "CORONA_HEIST_FLOW_WILL_BE_BROKEN_MASTERMIND"
		CASE CORONA_HEIST_HEELS_ADJUST_CAM_MOVEMENT                                             RETURN "CORONA_HEIST_HEELS_ADJUST_CAM_MOVEMENT"
		CASE CORONA_HEIST_FORCE_MATCHMAKING_SUPPRESSED		                                    RETURN "CORONA_HEIST_FORCE_MATCHMAKING_SUPPRESSED"
		CASE CORONA_HEIST_SUSPEND_MIN_PLAYER_CHECK			                                    RETURN "CORONA_HEIST_SUSPEND_MIN_PLAYER_CHECK"
		CASE CORONA_MISSION_VEHICLE_HAS_MULTIPLE_CLASSES	                                    RETURN "CORONA_MISSION_VEHICLE_HAS_MULTIPLE_CLASSES"
		CASE CORONA_FULL_SCREEN_MAP_ACTIVE					                                    RETURN "CORONA_FULL_SCREEN_MAP_ACTIVE"
		CASE CORONA_RETURNING_FROM_FULL_SCREEN_MAP			                                    RETURN "CORONA_RETURNING_FROM_FULL_SCREEN_MAP"
		CASE CORONA_AMMO_ARMOR_EXTERNAL_UPDATE				                                    RETURN "CORONA_AMMO_ARMOR_EXTERNAL_UPDATE"
		CASE CORONA_AUTO_INVITE_GATHERING_PLAYERS			                                    RETURN "CORONA_AUTO_INVITE_GATHERING_PLAYERS"
		CASE CORONA_SCTV_LAUNCHED_JOB						                                    RETURN "CORONA_SCTV_LAUNCHED_JOB"
		CASE CORONA_REFRESH_PLAYER_TITLES					                                    RETURN "CORONA_REFRESH_PLAYER_TITLES"
		CASE CORONA_INVALID_ROLES							                                    RETURN "CORONA_INVALID_ROLES"
		CASE CORONA_DELAYED_CORONA_FOR_TEAM_ROLES			                                    RETURN "CORONA_DELAYED_CORONA_FOR_TEAM_ROLES"
		CASE CORONA_ALLOW_ALL_OUTFITS						                                    RETURN "CORONA_ALLOW_ALL_OUTFITS"
		CASE CORONA_ONE_OUTFIT_ACTIVE_FOR_MISSION			                                    RETURN "CORONA_ONE_OUTFIT_ACTIVE_FOR_MISSION"
		CASE CORONA_FORCE_LAUNCH_AND_REORGANISE_TEAMS		                                    RETURN "CORONA_FORCE_LAUNCH_AND_REORGANISE_TEAMS"
		CASE CORONA_FORCED_LAUNCH_INSTRUCTIONAL_BUTTONS		                                    RETURN "CORONA_FORCED_LAUNCH_INSTRUCTIONAL_BUTTONS"
		CASE CORONA_RESET_OUTFITS_DUE_TO_FORCE_LAUNCH		                                    RETURN "CORONA_RESET_OUTFITS_DUE_TO_FORCE_LAUNCH"
		CASE CORONA_VEHICLE_SCREEN_DISPLAYING_LIVERY		                                    RETURN "CORONA_VEHICLE_SCREEN_DISPLAYING_LIVERY"
		CASE CORONA_SELECTING_FROM_PERSONAL_VEHICLES		                                    RETURN "CORONA_SELECTING_FROM_PERSONAL_VEHICLES"
		CASE CORONA_SHOW_MAX_TEAM_DIFFERENCE_HELP			                                    RETURN "CORONA_SHOW_MAX_TEAM_DIFFERENCE_HELP"
		CASE CORONA_TRIGGERING_AUDIO_SCENE_FOR_ACTIVE_MODES	                                    RETURN "CORONA_TRIGGERING_AUDIO_SCENE_FOR_ACTIVE_MODES"
		CASE CORONA_UPDATE_NEW_PILOT						                                    RETURN "CORONA_UPDATE_NEW_PILOT"
		CASE CORONA_VEHICLE_SCREEN_DISPLAYING_PARACHUTE		                                    RETURN "CORONA_VEHICLE_SCREEN_DISPLAYING_PARACHUTE"
		CASE CORONA_GANG_OPS_PLANNING_SCRIPT_REQUESTED		                                    RETURN "CORONA_GANG_OPS_PLANNING_SCRIPT_REQUESTED"
		CASE CORONA_GANG_OPS_PLANNING_SCRIPT_LAUNCHED		                                    RETURN "CORONA_GANG_OPS_PLANNING_SCRIPT_LAUNCHED"
		CASE CORONA_GANG_OPS_PLANNING_SCRIPT_RUNNING		                                    RETURN "CORONA_GANG_OPS_PLANNING_SCRIPT_RUNNING"
		CASE CORONA_GANG_OPS_PLANNING_ENABLED_INTERACTION	                                    RETURN "CORONA_GANG_OPS_PLANNING_ENABLED_INTERACTION"
		CASE CORONA_GLOBAL_GANG_OPS_HEIST_FINALE_FLAG		                                    RETURN "CORONA_GLOBAL_GANG_OPS_HEIST_FINALE_FLAG"
		CASE CORONA_GANGOPS_FLOW_AWARD_WILL_BE_BROKEN		                                    RETURN "CORONA_GANGOPS_FLOW_AWARD_WILL_BE_BROKEN"
		CASE CORONA_GANGOPS_FINALE_START_AUDIO_SCENE		                                    RETURN "CORONA_GANGOPS_FINALE_START_AUDIO_SCENE"
	ENDSWITCH
		
	SWITCH iCoronaBit
		CASE CORONA_HOTRING_SERIES_DISABLE_OUTFIT_SELECTION	                                    RETURN "CORONA_HOTRING_SERIES_DISABLE_OUTFIT_SELECTION"
		CASE CORONA_VEHICLE_SCREEN_DISPLAYING_ARMOUR		                            		RETURN "CORONA_VEHICLE_SCREEN_DISPLAYING_ARMOUR"
		CASE CORONA_VEHICLE_SCREEN_DISPLAYING_SCOOPS_RAMBARS                                    RETURN "CORONA_VEHICLE_SCREEN_DISPLAYING_SCOOPS_RAMBARS"
		CASE CORONA_VEHICLE_SCREEN_DISPLAYING_MINES			                                    RETURN "CORONA_VEHICLE_SCREEN_DISPLAYING_MINES"
		CASE CORONA_VEHICLE_SCREEN_REFRESHED_VEHICLE_DATA	                                    RETURN "CORONA_VEHICLE_SCREEN_REFRESHED_VEHICLE_DATA"
		CASE CORONA_LOADED_JOB_IPL					                                   		 	RETURN "CORONA_LOADED_JOB_IPL"
		CASE CORONA_VEHICLE_SELECTION_DISPLAY_VEH_STATS		                                    RETURN "CORONA_VEHICLE_SELECTION_DISPLAY_VEH_STATS"
		CASE CORONA_VEHICLE_SELECTION_VEH_STATS_BUTTON_HIDDEN                               	RETURN "CORONA_VEHICLE_SELECTION_VEH_STATS_BUTTON_HIDDEN"
		CASE CORONA_VEHICLE_CUSTOM_COLOUR_SET				                                    RETURN "CORONA_VEHICLE_CUSTOM_COLOUR_SET"
		CASE CORONA_GENERIC_JOB_RECREATE_CAMERA				                                    RETURN "CORONA_GENERIC_JOB_RECREATE_CAMERA"
		CASE CORONA_VEHICLE_SELECTION_VOTE_ENABLED												RETURN "CORONA_VEHICLE_SELECTION_VOTE_ENABLED"
		CASE CORONA_VEHICLE_SCREEN_DISPLAYING_PURCHASE_MOD										RETURN "CORONA_VEHICLE_SCREEN_DISPLAYING_PURCHASE_MOD"
		CASE CORONA_USE_CORONA_CENTER_POS_FOR_VEHICLE											RETURN "CORONA_USE_CORONA_CENTER_POS_FOR_VEHICLE"
		CASE CORONA_VEHICLE_VECTOR_CACHED														RETURN "CORONA_VEHICLE_VECTOR_CACHED"
		CASE CORONA_ARENA_WARS_SET_CROWD_AUDIO													RETURN "CORONA_ARENA_WARS_SET_CROWD_AUDIO"
		CASE CORONA_UPDATE_VEHICLE_DETAILS														RETURN "CORONA_UPDATE_VEHICLE_DETAILS"
		CASE CORONA_VEHICLE_MODS_PRELOADING														RETURN "CORONA_VEHICLE_MODS_PRELOADING"
		CASE CORONA_VEHICLE_MODS_PRELOADED														RETURN "CORONA_VEHICLE_MODS_PRELOADED"
		CASE CORONA_ARENA_WARS_LOBBY_AUDIO_SCENE_ACTIVE											RETURN "CORONA_ARENA_WARS_LOBBY_AUDIO_SCENE_ACTIVE"
		CASE CORONA_GENERIC_JOB_DIALOGUE_SAFE_TO_PLAY											RETURN "CORONA_GENERIC_JOB_DIALOGUE_SAFE_TO_PLAY"
		CASE CORONA_GENERIC_JOB_DIALOGUE_ONE_SHOT_DONE											RETURN "CORONA_GENERIC_JOB_DIALOGUE_ONE_SHOT_DONE"
		CASE CORONA_POST_TRANSITION_CAMERA_CUT_DONE												RETURN "CORONA_POST_TRANSITION_CAMERA_CUT_DONE"
		CASE CORONA_BINK_VIDEO_MUSIC_EVENT_TRIGGERED											RETURN "CORONA_BINK_VIDEO_MUSIC_EVENT_TRIGGERED"
		CASE CORONA_BINK_VIDEO_VEHICLE_SELECT_MUSIC_EVENT_TRIGGERED								RETURN "CORONA_BINK_VIDEO_VEHICLE_SELECT_MUSIC_EVENT_TRIGGERED"
		CASE CORONA_BINK_VIDEO_WORLD_MUTED														RETURN "CORONA_BINK_VIDEO_WORLD_MUTED"
		CASE CORONA_SELECTED_RANDOM_ARENA_LIGHTING												RETURN "CORONA_SELECTED_RANDOM_ARENA_LIGHTING"
		CASE CORONA_ARENA_TUTORIAL_HELP_SHOWN													RETURN "CORONA_ARENA_TUTORIAL_HELP_SHOWN"
		CASE CORONA_ANNOUNCER_FINISHED_EVENT_TRIGGERED											RETURN "CORONA_ANNOUNCER_FINISHED_EVENT_TRIGGERED"
		CASE CORONA_IS_RCBANDITO_RACE															RETURN "CORONA_IS_RCBANDITO_RACE"
		CASE CORONA_IS_KING_OF_THE_HILL															RETURN "CORONA_IS_KING_OF_THE_HILL"
		CASE CORONA_IS_KING_OF_THE_HILL_TEAM													RETURN "CORONA_IS_KING_OF_THE_HILL_TEAM"
		CASE CORONA_CUTSCENE_FAILED_TO_START													RETURN "CORONA_CUTSCENE_FAILED_TO_START"
		
		CASE CORONA_BITSET_BLOCK_SKYSWOOP_HIDE_FOR_CASINO										RETURN "CORONA_BITSET_BLOCK_SKYSWOOP_HIDE_FOR_CASINO"
		CASE CORONA_FLUSHED_CUTSCENE_STRUCT_IF_NECESSARY										RETURN "CORONA_FLUSHED_CUTSCENE_STRUCT_IF_NECESSARY"
		CASE CORONA_GPS_FLAGS_SET																RETURN "CORONA_GPS_FLAGS_SET"
		CASE CORONA_VEHICLE_SCREEN_DISPLAYING_TYRES												RETURN "CORONA_VEHICLE_SCREEN_DISPLAYING_TYRES"
		CASE CORONA_GLOBAL_CASINO_HEIST_FINALE_FLAG												RETURN "CORONA_GLOBAL_CASINO_HEIST_FINALE_FLAG"
		CASE CORONA_CASINO_HEIST_PLANNING_BOARD_SCRIPT_REQUESTED								RETURN "CORONA_CASINO_HEIST_PLANNING_BOARD_SCRIPT_REQUESTED"
		CASE CORONA_CASINO_HEIST_PLANNING_BOARD_SCRIPT_LAUNCHED									RETURN "CORONA_CASINO_HEIST_PLANNING_BOARD_SCRIPT_LAUNCHED"
		CASE CORONA_CASINO_HEIST_PLANNING_BOARD_SCRIPT_RUNNING									RETURN "CORONA_CASINO_HEIST_PLANNING_BOARD_SCRIPT_RUNNING"
		CASE CORONA_FADED_SCREEN_OUT															RETURN "CORONA_FADED_SCREEN_OUT"
		CASE CORONA_CASINO_HEIST_CUTSCENE_1_VIEWED												RETURN "CORONA_CASINO_HEIST_CUTSCENE_1_VIEWED"
		CASE CORONA_CASINO_HEIST_MAINTAIN_BOARDS												RETURN "CORONA_CASINO_HEIST_MAINTAIN_BOARDS"										
		CASE CORONA_CASINO_HEIST_READY_TO_START													RETURN "CORONA_CASINO_HEIST_READY_TO_START"
		CASE CORONA_FROZEN_FOR_CASINO_HEIST_PLANNING											RETURN "CORONA_FROZEN_FOR_CASINO_HEIST_PLANNING"
		CASE CORONA_FREEZE_FOR_CASINO_HEIST_PLANNING											RETURN "CORONA_FREEZE_FOR_CASINO_HEIST_PLANNING"
		CASE CORONA_GLOBAL_GENERIC_HEIST_FINALE_FLAG											RETURN "CORONA_GLOBAL_GENERIC_HEIST_FINALE_FLAG"
		CASE CORONA_GENERIC_HEIST_MAINTAIN_PLANNING												RETURN "CORONA_GENERIC_HEIST_MAINTAIN_PLANNING"
		CASE CORONA_GENERIC_HEIST_READY_TO_START												RETURN "CORONA_GENERIC_HEIST_READY_TO_START"
		CASE CORONA_GENERIC_HEIST_PLANNING_SCRIPT_REQUESTED										RETURN "CORONA_GENERIC_HEIST_PLANNING_SCRIPT_REQUESTED"
		CASE CORONA_GENERIC_HEIST_PLANNING_SCRIPT_LAUNCHED										RETURN "CORONA_GENERIC_HEIST_PLANNING_SCRIPT_LAUNCHED"
		CASE CORONA_GENERIC_HEIST_PLANNING_SCRIPT_RUNNING										RETURN "CORONA_GENERIC_HEIST_PLANNING_SCRIPT_RUNNING"
		CASE CORONA_GENERIC_HEIST_START_INTERACTION												RETURN "CORONA_GENERIC_HEIST_START_INTERACTION"
		CASE CORONA_DOWNLOAD_ISLAND_HEIST_MISSION												RETURN "CORONA_DOWNLOAD_ISLAND_HEIST_MISSION"
		CASE CORONA_ISLAND_HEIST_MISSION_DOWNLOADED												RETURN "CORONA_ISLAND_HEIST_MISSION_DOWNLOADED"
		CASE CORONA_BROADCAST_OUTFIT_CHANGE														RETURN "CORONA_BROADCAST_OUTFIT_CHANGE"
		CASE CORONA_FIXER_MISSION_HAS_OUTFITS													RETURN "CORONA_FIXER_MISSION_HAS_OUTFITS"
		CASE CORONA_FIXER_SHORT_TRIP_INTRO														RETURN "CORONA_FIXER_SHORT_TRIP_INTRO"
		CASE CORONA_USE_CUSTOM_UI_FOR_JOB_LOBBY													RETURN "CORONA_USE_CUSTOM_UI_FOR_JOB_LOBBY"
		CASE CORONA_SUPPRESS_AUTO_START_COUNTDOWN												RETURN "CORONA_SUPPRESS_AUTO_START_COUNTDOWN"
		CASE CORONA_LAUNCH_FIXER_HQ																RETURN "CORONA_LAUNCH_FIXER_HQ"
		CASE CORONA_VEHICLE_SCREEN_DISPLAYING_OUTFITS											RETURN "CORONA_VEHICLE_SCREEN_DISPLAYING_OUTFITS"
		CASE CORONA_VEHICLE_SCREEN_DISPLAYING_HELMETS											RETURN "CORONA_VEHICLE_SCREEN_DISPLAYING_HELMETS"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC
#ENDIF

FUNC BOOL IS_CORONA_BIT_SET(INT iCoronaBit)

	INT iBitSet = (iCoronaBit / 32)
	INT iBitVal = (iCoronaBit % 32)
	
	IF iBitSet >= NUM_CORONA_BITSETS
		SCRIPT_ASSERT("IS_CORONA_BIT_SET - iBitSet >= NUM_CORONA_BITSETS. See logs for more info.")
		PRINTLN("[CORONA] IS_CORONA_BIT_SET - iBitSet (", iBitSet, ") >= NUM_CORONA_BITSETS (", NUM_CORONA_BITSETS, "). Increase NUM_CORONA_BITSETS.")
		RETURN FALSE
	ENDIF
	
	RETURN IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.iCoronaBitSet[iBitSet], iBitVal)
ENDFUNC

PROC SET_CORONA_BIT(INT iCoronaBit)
	INT iBitSet = (iCoronaBit / 32)
	INT iBitVal = (iCoronaBit % 32)
	
	IF iBitSet >= NUM_CORONA_BITSETS
		SCRIPT_ASSERT("SET_CORONA_BIT - iBitSet >= NUM_CORONA_BITSETS. See logs for more info.")
		PRINTLN("[CORONA] SET_CORONA_BIT - iBitSet (", iBitSet, ") >= NUM_CORONA_BITSETS (", NUM_CORONA_BITSETS, "). Increase NUM_CORONA_BITSETS.")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.iCoronaBitSet[iBitSet], iBitVal)
		PRINTLN("[CORONA] SET_CORONA_BIT - ", GET_CORONA_BIT_NAME_FOR_DEBUG(iCoronaBit))
	ENDIF
	
	SET_BIT(g_TransitionSessionNonResetVars.sTransVars.iCoronaBitSet[iBitSet], iBitVal)
ENDPROC

PROC CLEAR_CORONA_BIT(INT iCoronaBit)
	INT iBitSet = (iCoronaBit / 32)
	INT iBitVal = (iCoronaBit % 32)
	
	IF iBitSet >= NUM_CORONA_BITSETS
		SCRIPT_ASSERT("CLEAR_CORONA_BIT - iBitSet >= NUM_CORONA_BITSETS. See logs for more info.")
		PRINTLN("[CORONA] CLEAR_CORONA_BIT - iBitSet (", iBitSet, ") >= NUM_CORONA_BITSETS (", NUM_CORONA_BITSETS, "). Increase NUM_CORONA_BITSETS.")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.iCoronaBitSet[iBitSet], iBitVal)
		PRINTLN("[CORONA] CLEAR_CORONA_BIT - ", GET_CORONA_BIT_NAME_FOR_DEBUG(iCoronaBit))
	ENDIF
	
	CLEAR_BIT(g_TransitionSessionNonResetVars.sTransVars.iCoronaBitSet[iBitSet], iBitVal)
ENDPROC

PROC SET_IS_THIS_A_KING_OF_THE_HILL_CORONA()
	PRINTLN("SET_IS_THIS_A_KING_OF_THE_HILL_CORONA")
	SET_CORONA_BIT(CORONA_IS_KING_OF_THE_HILL)
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC CLEAR_IS_THIS_A_KING_OF_THE_HILL_CORONA()
	PRINTLN("CLEAR_IS_THIS_A_KING_OF_THE_HILL_CORONA")
	CLEAR_CORONA_BIT(CORONA_IS_KING_OF_THE_HILL)
	DEBUG_PRINTCALLSTACK()
ENDPROC

FUNC BOOL IS_THIS_A_KING_OF_THE_HILL_CORONA(INT iType = -1, INT iSubtype = -1)
	IF IS_CORONA_BIT_SET(CORONA_IS_KING_OF_THE_HILL)
		RETURN TRUE
	ENDIF
	
	IF iType = FMMC_TYPE_DEATHMATCH
		IF iSubtype = FMMC_KING_OF_THE_HILL
		OR iSubtype = FMMC_KING_OF_THE_HILL_TEAM
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_IS_THIS_A_KING_OF_THE_HILL_TEAM_CORONA()
	PRINTLN("SET_IS_THIS_A_KING_OF_THE_HILL_CORONA")
	SET_CORONA_BIT(CORONA_IS_KING_OF_THE_HILL)
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC CLEAR_IS_THIS_A_KING_OF_THE_HILL_TEAM_CORONA()
	PRINTLN("CLEAR_IS_THIS_A_KING_OF_THE_HILL_CORONA")
	CLEAR_CORONA_BIT(CORONA_IS_KING_OF_THE_HILL)
	DEBUG_PRINTCALLSTACK()
ENDPROC

FUNC BOOL IS_THIS_A_KING_OF_THE_HILL_TEAM_CORONA(INT iType = -1, INT iSubtype = -1)
	IF IS_CORONA_BIT_SET(CORONA_IS_KING_OF_THE_HILL_TEAM)
		RETURN TRUE
	ENDIF
	
	IF iType = FMMC_TYPE_DEATHMATCH
		IF iSubtype = FMMC_KING_OF_THE_HILL_TEAM
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT PACK_CORONA_INTS(INT iInt1, INT iInt2, INT iInt3)
	RETURN (SHIFT_LEFT(iInt1, 24) | SHIFT_LEFT(iInt2, 16) | SHIFT_LEFT(iInt3, 8))
ENDFUNC

/// PURPOSE: Adjust our slot information handling > 16 coronas
PROC ADJUST_CORONA_PED_SLOT_DATA(INT &iSlot, INT &iNumSlots)
	
	// Are we over the max number of slots
	IF iNumSlots > 16

		// Is the number of slots even
		IF iNumSlots % 2 != 0
			iNumSlots++
		ENDIF
		
		iNumSlots = iNumSlots / 2	
		
		iSlot = (iSlot % iNumSlots)
	ENDIF

ENDPROC

/// PURPOSE: Returns correct GBD slot based on if player is SCTV or not
FUNC INT GET_CORONA_GBD_SLOT()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget != INVALID_PLAYER_INDEX()
			RETURN NATIVE_TO_INT( GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
		ENDIF
	ENDIF
	
	RETURN NATIVE_TO_INT(PLAYER_ID())
ENDFUNC

/// PURPOSE:
///    Check if the local player is stood in the race corona used in the freemode activity launch tutorial
/// RETURNS:
///    
FUNC BOOL IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerInFmActivityTutorialCorona)  
ENDFUNC

//returns TRUE if the player is in the corona
FUNC BOOL IS_PLAYER_IN_CORONA()
	RETURN (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].eCoronaStatus != CORONA_STATUS_IDLE)
ENDFUNC

//returns TRUE if the player is in the corona
FUNC BOOL IS_THIS_PLAYER_IN_CORONA(PLAYER_INDEX playerID)
	RETURN (GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].eCoronaStatus != CORONA_STATUS_IDLE)
ENDFUNC

//returns TRUE if the player is in the corona
FUNC BOOL IS_LOCAL_PLAYER_STANDING_IN_A_CORONA()
	RETURN g_sV2CoronaVars.g_isplayer_inside_corona 
ENDFUNC

//Gets the status
FUNC CORONA_STATUS_ENUM GET_CORONA_STATUS()
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].eCoronaStatus
ENDFUNC

// Get the corona status of player
FUNC CORONA_STATUS_ENUM GET_PLAYER_CORONA_STATUS(PLAYER_INDEX playerID)
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].eCoronaStatus
ENDFUNC

/// PURPOSE: Lets us query if the corona is launching contact mission in an apartment
FUNC BOOL IS_CORONA_CONTACT_MISSION_LAUNCHING_IN_APARTMENT()
	RETURN IS_BIT_SET(g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_HOST_IN_PROPERTY_DATA], ciCORONA_CONTACT_MISSION_IN_APARTMENT)
ENDFUNC

/// PURPOSE: Allows us to set the flag to indicate contact missions are launching in apartment
PROC SET_CONTACT_MISSION_LAUNCHING_IN_APARTMENT(BOOL bIsLaunchingInApartment)
	IF bIsLaunchingInApartment
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeginningMissionInApartment = TRUE
		PRINTLN("[Save_Create_Vehicle] - SET_CONTACT_MISSION_LAUNCHING_IN_APARTMENT: setting g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeginningMissionInApartment = TRUE")
		IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
			g_TransitionSessionNonResetVars.sTransVars.bKillInteriorScriptForContactMission = TRUE
			PRINTLN("SET_CONTACT_MISSION_LAUNCHING_IN_APARTMENT: Set true killing interior script. g_TransitionSessionNonResetVars.sTransVars.bKillInteriorScriptForContactMission = TRUE")
		ENDIF
	ELSE
		g_TransitionSessionNonResetVars.sTransVars.bKillInteriorScriptForContactMission = FALSE
		PRINTLN("SET_CONTACT_MISSION_LAUNCHING_IN_APARTMENT: g_TransitionSessionNonResetVars.sTransVars.bKillInteriorScriptForContactMission = FALSE")
	ENDIF
	g_TransitionSessionNonResetVars.sTransVars.bLaunchingContactMissionInApartment = bIsLaunchingInApartment
ENDPROC

/// PURPOSE: Returns TRUE if we are launching contact mission in apartment
FUNC BOOL IS_CONTACT_MISSION_LAUNCHING_IN_APARTMENT()
	RETURN g_TransitionSessionNonResetVars.sTransVars.bLaunchingContactMissionInApartment
ENDFUNC


FUNC BOOL IS_THIS_PLAYER_ACTIVE_IN_CORONA(PLAYER_INDEX playerID)
	SWITCH GET_PLAYER_CORONA_STATUS(playerID) 
		CASE CORONA_STATUS_IDLE				
		CASE CORONA_STATUS_CAN_START	
		CASE CORONA_STATUS_WALK_IN		
		CASE CORONA_STATUS_JOIN_HOST			
		CASE CORONA_STATUS_MatC_HAND_SHAKE
		CASE CORONA_STATUS_INIT_TRAN_SCRIPT		
		CASE CORONA_STATUS_INIT_CORONA		
		CASE CORONA_STATUS_GET_DATA_SETUP_TRAN
		CASE CORONA_STATUS_WAIT_FOR_ACTIVE
		CASE CORONA_STATUS_KICKED
		CASE CORONA_STATUS_WALK_OUT	
		CASE CORONA_STATUS_QUIT
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns TRUE if the player is in a state of leaving the corona
FUNC BOOL IS_THIS_PLAYER_LEAVING_THE_CORONA(PLAYER_INDEX playerID)

	//If we are getting out of the corona then don't need to return TRUE
	IF GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_WALK_OUT
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_QUIT
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_KICKED
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_RESET_POSITION
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_WARP_TO_SAFE_LOC
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_ACTIVE_IN_CORONA()
	RETURN IS_THIS_PLAYER_ACTIVE_IN_CORONA(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_ACTIVE_IN_CORONA_PAST_TRANSITION_SESSION(PLAYER_INDEX playerID)
	SWITCH GET_PLAYER_CORONA_STATUS(playerID) 		
		CASE CORONA_LAUNCH_TRANSITION
		CASE CORONA_STATUS_BALANCE_AND_LOAD
		CASE CORONA_STATUS_IN_CORONA
		CASE CORONA_STATUS_TEAM_DM
		CASE CORONA_STATUS_HEIST_PLANNING
		CASE CORONA_STATUS_HEIST_CUTSCENE
		CASE CORONA_STATUS_HEIST_PLANNING_CUTSCENE
		CASE CORONA_STATUS_HEIST_TUTORIAL_CUTSCENE
		 // AMEC HEISTS
		CASE CORONA_STATUS_FLOW_CUTSCENE
		CASE CORONA_STATUS_GANG_OPS_HEIST_PLANNING
		CASE CORONA_STATUS_BETTING	
		CASE CORONA_STATUS_IN_GENERIC_JOB_CORONA
		#IF FEATURE_CASINO_HEIST
		CASE CORONA_STATUS_CASINO_HEIST_PLANNING
		#ENDIF
		#IF FEATURE_HEIST_ISLAND
		CASE CORONA_STATUS_GENERIC_HEIST_PLANNING
		#ENDIF
			RETURN TRUE	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

// This includes the checks above but also the setup vars status
FUNC BOOL IS_THIS_PLAYER_FULLY_ACTIVE_IN_CORONA(PLAYER_INDEX playerID)
	IF NOT IS_THIS_PLAYER_ACTIVE_IN_CORONA(playerID)
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_SETUP_VARS
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns TRUE if the player is in a screen post transition (that is interactive)
FUNC BOOL IS_THIS_PLAYER_IN_ACTIVE_CORONA_SCREEN_POST_TRANSITION(PLAYER_INDEX playerID)
	
	IF GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_IN_CORONA 					
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_TEAM_DM
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_BETTING
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_HEIST_PLANNING
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_HEIST_CUTSCENE
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_HEIST_PLANNING_CUTSCENE
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_HEIST_TUTORIAL_CUTSCENE
	 // AMEC HEISTS
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_FLOW_CUTSCENE
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_GANG_OPS_HEIST_PLANNING
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_IN_GENERIC_JOB_CORONA
	#IF FEATURE_CASINO_HEIST
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_CASINO_HEIST_PLANNING
	#ENDIF
	#IF FEATURE_HEIST_ISLAND
	OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_GENERIC_HEIST_PLANNING
	#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_MINI_GAME_DEBUG_NAME()
	SWITCH g_FMMC_STRUCT.iMissionType
		CASE FMMC_TYPE_MG_GOLF				RETURN "Golf"
		CASE FMMC_TYPE_MG_TENNIS			RETURN "Tennis"
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	RETURN "Range"
		CASE FMMC_TYPE_MG_DARTS				RETURN "Darts"
		CASE FMMC_TYPE_MG_ARM_WRESTLING		RETURN "Arm W"
		CASE FMMC_TYPE_MG_PILOT_SCHOOL		RETURN "Pilot"
	ENDSWITCH
	RETURN ""
ENDFUNC
FUNC STRING GET_CORONA_STATUS_DEBUG_NAME(CORONA_STATUS_ENUM eCoronaStatus)
	SWITCH eCoronaStatus 
			CASE CORONA_STATUS_IDLE			 				RETURN " IDLE             "
			CASE CORONA_STATUS_CAN_START		 			RETURN " CAN START        "
			CASE CORONA_STATUS_INIT_TRAN_SCRIPT 			RETURN " TRAN_SCRIPT      "			
			CASE CORONA_STATUS_WALK_IN	 					RETURN " WALK_IN	 	  "
			CASE CORONA_STATUS_JOIN_HOST		 			RETURN " JOIN_HOST	      "
			CASE CORONA_STATUS_MatC_HAND_SHAKE 				RETURN " MatC_HAND_SHAKE  "			
			CASE CORONA_STATUS_INIT_CORONA		 			RETURN " INIT_CORONA      "
			CASE CORONA_STATUS_GET_DATA_SETUP_TRAN			RETURN " DATA_SETUP_TRAN  "
			CASE CORONA_STATUS_SETUP_VARS		 			RETURN " SETUP_VARS       "
			CASE CORONA_STATUS_INTRO 			 			RETURN " INTRO            "
			CASE CORONA_STATUS_LOBBY			 			RETURN " LOBBY            "
			CASE CORONA_STATUS_H2H_LOBBY					RETURN " H2H LOBBY        "
			CASE CORONA_STATUS_LEADERBOARD		 			RETURN " LEADERBOARD      "
			CASE CORONA_STATUS_JOINED_PLAYERS	 			RETURN " JOINED_PLAYERS   "
			CASE CORONA_LAUNCH_TRANSITION	 				RETURN " LAUNCH_TRAN      "
			CASE CORONA_STATUS_LOCAL_PLAYERS	 			RETURN " LOCAL_PLAYERS    "
			CASE CORONA_STATUS_LAST_JOB_PLAYERS 			RETURN " LAST_JOB_PLAYERS "
			CASE CORONA_STATUS_LAST_HEIST_PLAYERS			RETURN " LAST_HEIST_PLAYERS"
			CASE CORONA_STATUS_FRIENDS			 			RETURN " FRIENDS          "
			CASE CORONA_STATUS_CREW_MEMBERS	 				RETURN " CREW_MEMBERS     "
			CASE CORONA_STATUS_MATCHED_PLAYERS	 			RETURN " MATCHED_PLAYERS  "
			CASE CORONA_STATUS_BALANCE_AND_LOAD	 			RETURN " BALANCE_AND_LOAD "
			CASE CORONA_STATUS_SCTV_HOLDING					RETURN " SCTV_HOLDING	  "
			CASE CORONA_STATUS_SCTV_PROPERTY_CAM			RETURN " SCTV_PROPERTY	  "
			CASE CORONA_STATUS_IN_CORONA 		 			RETURN " IN_CORONA        "
			CASE CORONA_STATUS_IN_GENERIC_JOB_CORONA		RETURN " IN_GJOB_CORONA  "
			CASE CORONA_STATUS_TEAM_DM 		 				RETURN " TEAM_DM          "
			CASE CORONA_STATUS_HEIST_PLANNING				RETURN " HEIST            "
			CASE CORONA_STATUS_HEIST_PLANNING_CUTSCENE		RETURN " PLANNING_CUT     "
			CASE CORONA_STATUS_HEIST_CUTSCENE				RETURN " HEIST_CUT		  "
			CASE CORONA_STATUS_HEIST_TUTORIAL_CUTSCENE  	RETURN " HEIST_TUT_CUTS   "
			CASE CORONA_STATUS_FLOW_CUTSCENE				RETURN " FLOW_CUT         "
			CASE CORONA_STATUS_GANG_OPS_HEIST_PLANNING		RETURN " GO_HEIST		  "
			#IF FEATURE_CASINO_HEIST
			CASE CORONA_STATUS_CASINO_HEIST_PLANNING		RETURN " CASINO_HEIST	  "
			#ENDIF
			#IF FEATURE_HEIST_ISLAND
			CASE CORONA_STATUS_GENERIC_HEIST_PLANNING		RETURN " GENERIC_HEIST	  "
			#ENDIF
			CASE CORONA_STATUS_BETTING			 			RETURN " BETTING          "
			CASE CORONA_STATUS_WAIT_FOR_CASH_TRANSACTION	RETURN " CASH_TRANSACTION "
			CASE CORONA_STATUS_KICKED			 			RETURN " KICKED           "
			CASE CORONA_STATUS_RESET_POSITION	 			RETURN " RESET_POSITION   "
			CASE CORONA_STATUS_WARP_TO_SAFE_LOC	 			RETURN " WARP_TO_SAFE_LOC "
			CASE CORONA_STATUS_WALK_OUT		 				RETURN " WALK_OUT         "
			CASE CORONA_STATUS_LAUNCH			 			RETURN " LAUNCH           "
			CASE CORONA_STATUS_WAIT_FOR_ACTIVE	 			RETURN " WAIT_FOR_ACTIVE  "
			CASE CORONA_STATUS_QUIT			 				RETURN " QUIT             "
		ENDSWITCH
	RETURN ""
ENDFUNC
#ENDIF
//Sets the status 
PROC SET_CORONA_STATUS(CORONA_STATUS_ENUM eCoronaStatus #IF IS_DEBUG_BUILD , STRING stMission = NULL #ENDIF)
	#IF IS_DEBUG_BUILD
		//If the transition session is restarting then put a nice wee R on it to help us.
		STRING sType
		STRING sPlaylist = ""
		STRING sSCTV = ""
		STRING sHeist = ""
		STRING sRounds = ""
		//If the transition session is restarting	
		IF IS_STRING_NULL_OR_EMPTY(stMission)
			stMission = GET_MINI_GAME_DEBUG_NAME()
		ENDIF
		//if we are on a playlist?
		IF IS_PLAYER_ON_A_PLAYLIST_INT2(NATIVE_TO_INT(PLAYER_ID()))
			sPlaylist = " - *PL* "
		ENDIF
		IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
			sRounds = " - *ROUNDS* "
		ENDIF
		
		//if we are in corona flow as spectator?
		IF IS_PLAYER_SCTV(PLAYER_ID())
			sSCTV =  " - *SCTV*"
		ENDIF
		
		IF g_DebugBoolHeistIsCutscene
			sHeist = " - *HEIST CUT*"
		ENDIF
		
		//Print for James
		IF IS_TRANSITION_SESSION_RESTARTING()
			sType = "SET_CORONA_STATUS R-"
		ELSE
			sType = "SET_CORONA_STATUS  -"
		ENDIF	
		PRINTLN("[CORONA] ", sType, GET_CORONA_STATUS_DEBUG_NAME(eCoronaStatus), " T(", GET_CLOUD_TIME_AS_INT(),")  F(", GET_FRAME_COUNT(), ") (", stMission , ")", sPlaylist, sSCTV, sHeist, sRounds)
		//Print for bobby so it can be found in among all ther other TRANSITION SESSIONS -
		IF IS_TRANSITION_SESSION_RESTARTING()
			sType = "SETCORONA_STATUS R-"
		ELSE
			sType = "SETCORONA_STATUS  -"
		ENDIF	
		PRINTLN("****[TS] ", sType, GET_CORONA_STATUS_DEBUG_NAME(eCoronaStatus), " T(", GET_CLOUD_TIME_AS_INT(),")  F(", GET_FRAME_COUNT(), ") (", stMission , ")", sPlaylist, sSCTV, sHeist, sRounds)
		PRINTLN("TRANSITION TIME - SETCORONA_STATUS - ", GET_CORONA_STATUS_DEBUG_NAME(eCoronaStatus), "        - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")	
		
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_CoronaStatusPrintCallstack")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].eCoronaStatus = eCoronaStatus
ENDPROC


#IF IS_DEBUG_BUILD
//Sets the status 
FUNC STRING GET_CORONA_STATUS_FOR_DEBUG_PRINT(CORONA_STATUS_ENUM eCoronaStatus)
	SWITCH eCoronaStatus 
		CASE CORONA_STATUS_IDLE							RETURN "CORONA_STATUS_IDLE"
		CASE CORONA_STATUS_CAN_START					RETURN "CORONA_STATUS_CAN_START"
		CASE CORONA_STATUS_WALK_IN						RETURN "CORONA_STATUS_WALK_IN"
		CASE CORONA_STATUS_JOIN_HOST					RETURN "CORONA_STATUS_JOIN_HOST"
		CASE CORONA_STATUS_MatC_HAND_SHAKE				RETURN "CORONA_STATUS_MatC_HAND_SHAKE"
		CASE CORONA_STATUS_INIT_CORONA					RETURN "CORONA_STATUS_INIT_CORONA"
		CASE CORONA_STATUS_INIT_TRAN_SCRIPT				RETURN "CORONA_STATUS_INIT_TRAN_SCRIPT"
		CASE CORONA_STATUS_GET_DATA_SETUP_TRAN			RETURN "CORONA_STATUS_GET_DATA_SETUP_TRAN"
		CASE CORONA_STATUS_SETUP_VARS					RETURN "CORONA_STATUS_SETUP_VARS"
		CASE CORONA_STATUS_INTRO 						RETURN "CORONA_STATUS_INTRO"
		CASE CORONA_STATUS_LOBBY						RETURN "CORONA_STATUS_LOBBY"
		CASE CORONA_STATUS_H2H_LOBBY					RETURN "CORONA_STATUS_H2H_LOBBY"
		CASE CORONA_STATUS_LEADERBOARD					RETURN "CORONA_STATUS_LEADERBOARD"
		CASE CORONA_STATUS_JOINED_PLAYERS				RETURN "CORONA_STATUS_JOINED_PLAYERS"
		CASE CORONA_STATUS_LOCAL_PLAYERS				RETURN "CORONA_STATUS_LOCAL_PLAYERS"
		CASE CORONA_STATUS_LAST_JOB_PLAYERS 			RETURN "CORONA_STATUS_LAST_JOB_PLAYERS"
		CASE CORONA_STATUS_LAST_HEIST_PLAYERS			RETURN "CORONA_STATUS_LAST_HEIST_PLAYERS"
		CASE CORONA_STATUS_FRIENDS						RETURN "CORONA_STATUS_FRIENDS"
		CASE CORONA_STATUS_CREW_MEMBERS					RETURN "CORONA_STATUS_CREW_MEMBERS"
		CASE CORONA_STATUS_MATCHED_PLAYERS				RETURN "CORONA_STATUS_MATCHED_PLAYERS"
		CASE CORONA_LAUNCH_TRANSITION			 		RETURN "CORONA_LAUNCH_TRANSITION"
		CASE CORONA_STATUS_BALANCE_AND_LOAD				RETURN "CORONA_STATUS_BALANCE_AND_LOAD"
		CASE CORONA_STATUS_SCTV_HOLDING					RETURN "CORONA_STATUS_SCTV_HOLDING"
		CASE CORONA_STATUS_SCTV_PROPERTY_CAM			RETURN "CORONA_STATUS_SCTV_PROPERTY_CAM"
		CASE CORONA_STATUS_IN_CORONA 					RETURN "CORONA_STATUS_IN_CORONA"
		CASE CORONA_STATUS_IN_GENERIC_JOB_CORONA		RETURN "CORONA_STATUS_IN_GENERIC_JOB_CORONA"
		#IF FEATURE_CASINO_HEIST
		CASE CORONA_STATUS_CASINO_HEIST_PLANNING		RETURN "CORONA_STATUS_CASINO_HEIST_PLANNING"
		#ENDIF
		#IF FEATURE_HEIST_ISLAND
		CASE CORONA_STATUS_GENERIC_HEIST_PLANNING		RETURN "CORONA_STATUS_GENERIC_HEIST_PLANNING"
		#ENDIF
		CASE CORONA_STATUS_TEAM_DM			 			RETURN "CORONA_STATUS_TEAM_DM"
		CASE CORONA_STATUS_HEIST_PLANNING				RETURN "CORONA_STATUS_HEIST_PLANNING"
		CASE CORONA_STATUS_HEIST_PLANNING_CUTSCENE		RETURN "CORONA_STATUS_HEIST_PLANNING_CUTSCENE"
		CASE CORONA_STATUS_HEIST_CUTSCENE				RETURN "CORONA_STATUS_HEIST_CUTSCENE"
		CASE CORONA_STATUS_HEIST_TUTORIAL_CUTSCENE		RETURN "CORONA_STATUS_HEIST_TUTORIAL_CUTSCENE"
		CASE CORONA_STATUS_FLOW_CUTSCENE				RETURN "CORONA_STATUS_FLOW_CUTSCENE"
		CASE CORONA_STATUS_GANG_OPS_HEIST_PLANNING		RETURN "CORONA_STATUS_GANG_OPS_HEIST_PLANNING"
		CASE CORONA_STATUS_BETTING						RETURN "CORONA_STATUS_BETTING"
		CASE CORONA_STATUS_WAIT_FOR_CASH_TRANSACTION	RETURN "CORONA_STATUS_WAIT_FOR_CASH_TRANSACTION"
		CASE CORONA_STATUS_KICKED						RETURN "CORONA_STATUS_KICKED"
		CASE CORONA_STATUS_WALK_OUT						RETURN "CORONA_STATUS_WALK_OUT"
		CASE CORONA_STATUS_LAUNCH						RETURN "CORONA_STATUS_LAUNCH"
		CASE CORONA_STATUS_WAIT_FOR_ACTIVE				RETURN "CORONA_STATUS_WAIT_FOR_ACTIVE"
		CASE CORONA_STATUS_QUIT							RETURN "CORONA_STATUS_QUIT" 		
	ENDSWITCH
	RETURN ""
ENDFUNC 
#ENDIF


/// PURPOSE: Finds the player in the next slot
FUNC PLAYER_INDEX GET_PLAYER_IN_CORONA_SLOT(INT iSlot)
	INT i
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	REPEAT NUM_NETWORK_PLAYERS i
		IF GlobalplayerBD_FM_2[i].iVoteToUse = GlobalplayerBD_FM_2[iMyGBD].iVoteToUse
			IF iSlot = GlobalServerBD_BlockB.g_PlayerCoronaPos[i].iCoronaPos
				RETURN INT_TO_PLAYERINDEX(i)
			ENDIF
		ENDIF
	ENDREPEAT	
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

/// PURPOSE: Return the current corona position state of player
FUNC INT GET_LOCAL_PLAYER_CORONA_POS_STATE()
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCoronaPosState
ENDFUNC
//Get the corona stat of any one!
FUNC INT GET_PLAYER_CORONA_POS_STATE(PLAYER_INDEX playerID)
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCoronaPosState 
ENDFUNC

//Set the flag that team balincing should run
PROC SET_TEAM_BALANCING_SHOULD_RUN()
	PRINTLN("SET_TEAM_BALANCING_SHOULD_RUN")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_START_TEAM_BALANCING_START)
ENDPROC
//Clear the flag that team balincing should run
PROC CLEAR_TEAM_BALANCING_SHOULD_RUN()
	PRINTLN("CLEAR_TEAM_BALANCING_SHOULD_RUN")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_START_TEAM_BALANCING_START)
ENDPROC
//Check to see that team balincing should run
FUNC BOOL SHOULD_TEAM_BALANCING_RUN()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_START_TEAM_BALANCING_START)
ENDFUNC


//Set the flag that my teams have been balanced
PROC SET_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED()
	PRINTLN("SET_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED)
ENDPROC
//Clear the flag that team balincing has been done on my globals
PROC CLEAR_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED()
	PRINTLN("CLEAR_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED)
ENDPROC
//Check to see that team balincing has happened on my globals
FUNC BOOL HAVE_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED)
ENDFUNC


//Set the flag that ALL teams have been balanced
PROC SET_ALL_GLOBALS_HAVE_BEEN_TEAM_BALANCED()
	PRINTLN("SET_ALL_GLOBALS_HAVE_BEEN_TEAM_BALANCED")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_ALL_GLOBALS_HAVE_BEEN_TEAM_BALANCED)
ENDPROC
//Clear the flag that team balincing has been done on ALL globals
PROC CLEAR_ALL_GLOBALS_HAVE_BEEN_TEAM_BALANCED()
	PRINTLN("CLEAR_ALL_GLOBALS_HAVE_BEEN_TEAM_BALANCED")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_ALL_GLOBALS_HAVE_BEEN_TEAM_BALANCED)
ENDPROC
//Check to see that team balincing has happened on ALL globals
FUNC BOOL HAVE_ALL_GLOBALS_HAVE_BEEN_TEAM_BALANCED()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_ALL_GLOBALS_HAVE_BEEN_TEAM_BALANCED)
ENDFUNC


//Set the flag that my teams have been balanced
PROC SET_EVERONE_ON_MY_TEAM_IS_IN_THE_SAME_CREW()
	PRINTLN("SET_EVERONE_ON_MY_TEAM_IS_IN_THE_SAME_CREW")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_EVERONE_ON_MY_TEAM_IS_IN_THE_SAME_CREW)
ENDPROC
//Clear the flag that team balincing has been done on my globals
PROC CLEAR_EVERONE_ON_MY_TEAM_IS_IN_THE_SAME_CREW()
	PRINTLN("CLEAR_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_EVERONE_ON_MY_TEAM_IS_IN_THE_SAME_CREW)
ENDPROC
//Check to see that team balincing has happened on my globals
FUNC BOOL GET_IS_EVERONE_ON_MY_TEAM_IN_THE_SAME_CREW(PLAYER_INDEX piPlayer)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].sClientCoronaData.iBitSet, ciCORONA_BS_EVERONE_ON_MY_TEAM_IS_IN_THE_SAME_CREW)
ENDFUNC

//Set the flag that my teams have been balanced
PROC SET_EVERONE_ON_A_TEAM_IS_IN_THE_SAME_CREW(INT iTeam)
	PRINTLN("SET_EVERONE_ON_A_TEAM_IS_IN_THE_SAME_CREW: ", ciCORONA_BS_EVERONE_ON_TEAM_IN_THE_SAME_CREW_TEAM_1 + iTeam)
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_EVERONE_ON_TEAM_IN_THE_SAME_CREW_TEAM_1 + iTeam)
ENDPROC
//Clear the flag that team balincing has been done on my globals
PROC CLEAR_EVERONE_ON_A_TEAM_IS_IN_THE_SAME_CREW(INT iTeam)
	PRINTLN("CLEAR_EVERONE_ON_A_TEAM_IS_IN_THE_SAME_CREW")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_EVERONE_ON_TEAM_IN_THE_SAME_CREW_TEAM_1 + iTeam)
ENDPROC
//Check to see that team balincing has happened on my globals
FUNC BOOL GET_EVERONE_ON_A_TEAM_IS_IN_THE_SAME_CREW(INT iTeam)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_EVERONE_ON_TEAM_IN_THE_SAME_CREW_TEAM_1 + iTeam)
ENDFUNC

/// PURPOSE:
///    Set that the cash transaction has succeeded for local player
PROC SET_PLAYER_CORONA_CASH_TRANSACTION_SUCCEEDED()
	PRINTLN("SET_PLAYER_CORONA_CASH_TRANSACTION_SUCCEEDED: ", ciCORONA_BS_PLAYER_CASH_TRANSACTION_SUCCEEDED)
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_PLAYER_CASH_TRANSACTION_SUCCEEDED)
ENDPROC

/// PURPOSE:
///    Clear the flag to say the cash transaction has succeeded
PROC CLEAR_PLAYER_CORONA_CASH_TRANSACTION_SUCCEEDED()
	PRINTLN("CLEAR_PLAYER_CORONA_CASH_TRANSACTION_SUCCEEDED")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iBitSet, ciCORONA_BS_PLAYER_CASH_TRANSACTION_SUCCEEDED)
ENDPROC

/// PURPOSE:
///    Check if the player has successfully processed their cash transaction
FUNC BOOL GET_PLAYER_CORONA_CASH_TRANSACTION_SUCCEEDED(PLAYER_INDEX playerID)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sClientCoronaData.iBitSet, ciCORONA_BS_PLAYER_CASH_TRANSACTION_SUCCEEDED)
ENDFUNC



/// PURPOSE: Returns the team the player has been assigned   
FUNC INT GET_MY_TEAM_IN_CORONA()
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
ENDFUNC

/// PURPOSE: Returns the team the player has been assigned   
FUNC INT GET_PLAYER_TEAM_IN_CORONA(PLAYER_INDEX playerID)
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sClientCoronaData.iTeamChosen
ENDFUNC

/// PURPOSE: Checks to see if we are setting up a mission with team balancing off   
FUNC BOOL IS_THIS_A_TEAM_MISSION_WITH_BALANCING_OFF(FMMC_SELECTED_ITEMS &sSelection)
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		IF g_FMMC_STRUCT.iMaxNumberOfTeams > 1
			IF sSelection.iSelection[ciMISSION_HOST_OPTION_TEAM_BAL] = DM_TEAM_BALANCING_OFF
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
		IF sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DEATHMATCH_TEAM // Team Deathmatch / Last team standing
		OR sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_KING_OF_THE_HILL_TEAM	// Team King of the Hill
			IF sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL] = DM_TEAM_BALANCING_OFF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_MY_GENERIC_JOB_TEAM()
	INT iTeam = 0
	IF GET_MY_TEAM_IN_CORONA() != -1
	AND iTeam != GET_MY_TEAM_IN_CORONA()
		iTeam = GET_MY_TEAM_IN_CORONA()
	ENDIF
	
	// No races in teams, always assume team 0
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
		iTeam = 0
	ENDIF
	
	// Assume team 0 if a deatchmatch and not team deathmatch
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH 
	AND g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE] != 1
		iTeam = 0
	ENDIF
	
	IF iTeam >= FMMC_MAX_TEAMS
		PRINTLN("[CORONA][GENERIC_JOB] GET_MY_GENERIC_JOB_TEAM - iTeam >= FMMC_MAX_TEAMS, assuming team 0")
		iTeam = 0
	ENDIF
	
	RETURN iTeam
ENDFUNC

FUNC BOOL SHOULD_THIS_CORONA_MOVE_TO_BETTING_SCREEN(INT iMissionVar)

	IF DOES_JOB_USE_RACE_VEHICLE_SELECTION()
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_THIS_A_NET_HEIST_PLANNING_GENERIC_FINALE_CORONA()
		RETURN FALSE
	ENDIF
	#ENDIF

	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		IF iMissionVar = FMMC_MISSION_TYPE_LTS
		OR iMissionVar = FMMC_MISSION_TYPE_CTF
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_WE_USE_FX()

	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE_TO_POINT
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_LTS
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_COOP
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_BASE_JUMP
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


// ************************************
//  New early launching of Job scripts
//		while player is in corona
// ************************************

PROC RESET_CORONA_JOB_LAUNCHING_FLAGS()
	PRINTLN("[CORONA] RESET_CORONA_JOB_LAUNCHING_FLAGS - Clear all flags")		
	g_CoronaJobLaunchData.iBS_CoronaJobFlags = 0
ENDPROC

/// PURPOSE: Called by corona when the job can begin and take full control
PROC SET_CORONA_IS_READY_TO_START_JOB(BOOL bReady = TRUE)
	IF bReady
		PRINTLN("[CORONA] SET_CORONA_IS_READY_TO_START_JOB - SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_LAUNCH_READY)")	
		g_b_TransitionActive = TRUE
		SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_LAUNCH_READY)
	ELSE
		PRINTLN("[CORONA] SET_CORONA_IS_READY_TO_START_JOB - CLEAR_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_LAUNCH_READY)")	
		CLEAR_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_LAUNCH_READY)
	ENDIF
ENDPROC

// AMEC HEISTS.
PROC SET_CORONA_HEIST_IS_READY_TO_START_JOB(BOOL bReady = TRUE)
	IF NOT bReady
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_HEIST_NOT_READY_TO_LAUNCH)
			PRINTLN("[CORONA] SET_CORONA_HEIST_IS_READY_TO_START_JOB - SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_HEIST_NOT_READY_TO_LAUNCH)")	
		ENDIF
		#ENDIF
		SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_HEIST_NOT_READY_TO_LAUNCH)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_HEIST_NOT_READY_TO_LAUNCH)
		PRINTLN("[CORONA] SET_CORONA_HEIST_IS_READY_TO_START_JOB - CLEAR_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_HEIST_NOT_READY_TO_LAUNCH)")	
		ENDIF
		#ENDIF
		CLEAR_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_HEIST_NOT_READY_TO_LAUNCH)
	ENDIF
ENDPROC

/// PURPOSE: Return TRUE when corona has indicated it is safe for Job to start  
FUNC BOOL IS_CORONA_READY_TO_START_WITH_JOB()

	// AMEC HEISTS.

	IF IS_BIT_SET(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_HEIST_NOT_READY_TO_LAUNCH)
		RETURN FALSE
	ENDIF
	
	
	RETURN IS_BIT_SET(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_LAUNCH_READY)
ENDFUNC

PROC SET_CORONA_IS_IN_QUICK_RESTART(BOOL bInRestart = TRUE)
	IF bInRestart
		PRINTLN("[CORONA] SET_CORONA_IS_IN_QUICK_RESTART - SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_IN_QUICK_RESTART)")	
		SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_IN_QUICK_RESTART)
	ELSE
		PRINTLN("[CORONA] SET_CORONA_IS_IN_QUICK_RESTART - CLEAR_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_IN_QUICK_RESTART)")	
		CLEAR_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_IN_QUICK_RESTART)
	ENDIF
ENDPROC

PROC SET_CORONA_FREEZE_AND_START_FX(BOOL bStartFreezeFX = TRUE)
	IF bStartFreezeFX
		PRINTLN("[CORONA] SET_CORONA_FREEZE_AND_START_FX - SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, SET_CORONA_FREEZE_AND_START_FX)")	
		SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_FREEZE_AND_RX)
	ELSE
		PRINTLN("[CORONA] SET_CORONA_FREEZE_AND_START_FX - CLEAR_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, SET_CORONA_FREEZE_AND_START_FX)")	
		CLEAR_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_FREEZE_AND_RX)
	ENDIF
ENDPROC

FUNC BOOL IS_CORONA_FREEZE_AND_START_FX_SET()
	RETURN IS_BIT_SET(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_FREEZE_AND_RX)
ENDFUNC
 
FUNC BOOL IS_THIS_A_QUICK_RESTART_JOB()
	RETURN IS_BIT_SET(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_IN_QUICK_RESTART)
ENDFUNC

/// PURPOSE: Called by corona when the job can begin and take full control
PROC SET_CORONA_IS_SAFE_TO_CLEANUP(BOOL bSafe = TRUE)
	IF bSafe
		PRINTLN("[CORONA] SET_CORONA_IS_SAFE_TO_CLEANUP - SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_LAUNCH_SAFE_TO_CLEANUP)")	
		SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_LAUNCH_SAFE_TO_CLEANUP)
	ELSE
		PRINTLN("[CORONA] SET_CORONA_IS_SAFE_TO_CLEANUP - CLEAR_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_LAUNCH_SAFE_TO_CLEANUP)")	
		CLEAR_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_LAUNCH_SAFE_TO_CLEANUP)
	ENDIF
ENDPROC

/// PURPOSE: Returns TRUE when the Job launching is ready to take control  
FUNC BOOL IS_IT_SAFE_FOR_CORONA_TO_CLEANUP()
	RETURN IS_BIT_SET(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_LAUNCH_SAFE_TO_CLEANUP)
ENDFUNC

// ****************************** HANDLE CORONA INVITED LIST ***********************************


/// PURPOSE: Returns the next available slot for invited players   
FUNC BOOL REMOVE_INVITED_GAMER_TO_CORONA_LIST(GAMER_HANDLE &aGamer, STRING tlGamerTag)
	
	INT i
	BOOL bShiftPlayer
	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
			
		IF bShiftPlayer
			g_CoronaInvitedPlayers.coronaInvitedPlayers[i-1] = g_CoronaInvitedPlayers.coronaInvitedPlayers[i]
			
			IF g_CoronaInvitedPlayers.iPlayerToGetGamerTag = i
				g_CoronaInvitedPlayers.iPlayerToGetGamerTag = (i-1)
				PRINTLN("[CORONA] REMOVE_INVITED_GAMER_TO_CORONA_LIST - Adjust our gamertag pointer = ", (i-1))
			ENDIF
			
			// If we are moving invalid players..STOP
			IF NOT IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
			AND g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedNameState != CORONA_INVITED_NAME_FROM_HOST
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
				IF NETWORK_ARE_HANDLES_THE_SAME(aGamer, g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
					bShiftPlayer = TRUE
					PRINTLN("[CORONA] REMOVE_INVITED_GAMER_TO_CORONA_LIST - Found player ", tlGamerTag, ", shift players down from i = ", i)
				ENDIF
			ELSE
				IF g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedNameState = CORONA_INVITED_NAME_FROM_HOST
					IF ARE_STRINGS_EQUAL(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerName, tlGamerTag)
						bShiftPlayer = TRUE
						PRINTLN("[CORONA] REMOVE_INVITED_GAMER_TO_CORONA_LIST (name) - Found player ", tlGamerTag, ", shift players down from i = ", i)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	RETURN bShiftPlayer
ENDFUNC

FUNC BOOL REMOVE_END_OF_JOB_GAMER_TO_CORONA_LIST(GAMER_HANDLE &aGamer)
	
	INT i
	BOOL bShiftPlayer
	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
			
		IF bShiftPlayer
			g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i-1] = g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i]
			
			// If we are moving invalid players..STOP
			IF NOT IS_GAMER_HANDLE_VALID(g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerHandle)
				g_CoronaEndOfJobPlayers.iNumberOfEndOfJobPlayers--
				PRINTLN("[CORONA] REMOVE_END_OF_JOB_GAMER_TO_CORONA_LIST - Removed player: g_CoronaEndOfJobPlayers.iNumberOfEndOfJobPlayers = ", g_CoronaEndOfJobPlayers.iNumberOfEndOfJobPlayers, " - RETURN TRUE")
			
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_GAMER_HANDLE_VALID(g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerHandle)
				IF NETWORK_ARE_HANDLES_THE_SAME(aGamer, g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerHandle)
					PRINTLN("[CORONA] REMOVE_END_OF_JOB_GAMER_TO_CORONA_LIST - Removing player: ", g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerTag, " from END OF JOB list")
				
					bShiftPlayer = TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	IF bShiftPlayer
		g_CoronaEndOfJobPlayers.iNumberOfEndOfJobPlayers--
		PRINTLN("[CORONA] REMOVE_END_OF_JOB_GAMER_TO_CORONA_LIST - Removed player: g_CoronaEndOfJobPlayers.iNumberOfEndOfJobPlayers = ", g_CoronaEndOfJobPlayers.iNumberOfEndOfJobPlayers, " - RETURN bShiftPlayer")
	ENDIF
	
	RETURN bShiftPlayer
ENDFUNC

/// PURPOSE: Remove a player from a specific position
PROC REMOVE_INVITED_GAMER_FROM_CORONA_LIST(INT iPos)
	
	INT i
	CORONA_INVITED_PLAYER_INFO emptyPlayerInfo
	
	FOR i = (iPos+1) TO (NUM_NETWORK_PLAYERS-1) STEP 1
			
		g_CoronaInvitedPlayers.coronaInvitedPlayers[i-1] = g_CoronaInvitedPlayers.coronaInvitedPlayers[i]
		
		IF g_CoronaInvitedPlayers.iPlayerToGetGamerTag = i
			g_CoronaInvitedPlayers.iPlayerToGetGamerTag = (i-1)
		ENDIF
		
		// If we are moving invalid players..STOP
		IF NOT IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
		AND g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedNameState != CORONA_INVITED_NAME_FROM_HOST
			EXIT
		ENDIF
		
	ENDFOR
	
	// Clear our last position
	g_CoronaInvitedPlayers.coronaInvitedPlayers[NUM_NETWORK_PLAYERS-1] = emptyPlayerInfo
	
ENDPROC


FUNC BOOL HAVE_PLAYER_BEEN_INVITED_TO_CORONA()

	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
			RETURN TRUE
		ENDIF
		
	ENDREPEAT

	RETURN FALSE
ENDFUNC

// PURPOSE: Check if new gamer handle is already part of our corona
FUNC BOOL IS_PLAYER_ALREADY_IN_CORONA(GAMER_HANDLE &aGamer)

	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF IS_GAMER_HANDLE_VALID(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].hGamer)
		AND IS_GAMER_HANDLE_VALID(aGamer)
			IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].hGamer, aGamer)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
		
	ENDREPEAT

	RETURN FALSE
ENDFUNC


// PURPOSE: Check if new gamer handle is already part of our corona
FUNC BOOL HAS_PLAYER_ALREADY_BEEN_INVITE_TO_CORONA(GAMER_HANDLE &aGamer)

	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
		AND IS_GAMER_HANDLE_VALID(aGamer)
			IF NETWORK_ARE_HANDLES_THE_SAME(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle, aGamer)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
		
	ENDREPEAT

	RETURN FALSE
ENDFUNC

/// PURPOSE: Check if the player name is already in the corona (client)
FUNC BOOL IS_PLAYER_NAME_ALREADY_IN_CORONA(TEXT_LABEL_63 tlGamertag)

	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF IS_GAMER_HANDLE_VALID(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].hGamer)
			IF ARE_STRINGS_EQUAL(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].szGamerTag, tlGamertag)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
		
	ENDREPEAT

	RETURN FALSE
ENDFUNC

/// PURPOSE: Query if the invited player has a gamertag being displayed
FUNC BOOL DOES_CORONA_INVITED_PLAYER_HAVE_GAMERTAG(INT i)
	RETURN (g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedNameState = CORONA_INVITED_HAS_NAME)
ENDFUNC

FUNC BOOL DOES_CORONA_INVITED_PLAYER_HAVE_CREW_TAG(INT i)
	RETURN (g_CoronaInvitedPlayers.coronaInvitedPlayers[i].iGamerCrewState != ciTRANSITION_SESSIONS_CREW_STATE_REQUEST)
ENDFUNC

FUNC BOOL IS_INVITED_PLAYER_IN_INVALID_CORONA_STATUS(SCALEFORM_PLAYER_ROW_STATE aState)

	SWITCH aState
		CASE SCALEFORM_PLAYER_ROW_BUSY
		CASE SCALEFORM_PLAYER_ROW_BLOCKED
		CASE SCALEFORM_PLAYER_ROW_CHEATER
		CASE SCALEFORM_PLAYER_ROW_BAD_SPORT
		CASE SCALEFORM_PLAYER_ROW_ACCEPTED
		CASE SCALEFORM_PLAYER_ROW_REJECTED
		CASE SCALEFORM_PLAYER_ROW_LEFT
		CASE SCALEFORM_PLAYER_ROW_INVITED
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL DO_WE_NEED_TO_SYNC_CORONA_STATUS_WITH_CODE(SCALEFORM_PLAYER_ROW_STATE aState)
	
	SWITCH aState
		CASE SCALEFORM_PLAYER_ROW_BUSY
		CASE SCALEFORM_PLAYER_ROW_BLOCKED
		CASE SCALEFORM_PLAYER_ROW_CHEATER
		CASE SCALEFORM_PLAYER_ROW_BAD_SPORT
		CASE SCALEFORM_PLAYER_ROW_ON_CALL
		CASE SCALEFORM_PLAYER_ROW_ON_CALL_HOST
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: Returns the correct stats of the player in the corona. 
///    Sets the text to be displayed and colour of HUD to indicate if they are in a positive or negative state.
PROC GET_PLAYER_CORONA_INVITE_STATUS(SCALEFORM_PLAYER_ROW_STATE playerStatus, TEXT_LABEL_15 &tlStatus, HUD_COLOURS &hcStatus)
	SWITCH playerStatus							
		CASE SCALEFORM_PLAYER_ROW_JOINED
			tlStatus = "FM_PLY_JOINED"
			hcStatus = HUD_COLOUR_GREEN
		BREAK
		
		CASE SCALEFORM_PLAYER_ROW_JOINING
			tlStatus = "FM_PLY_JOINING"
			hcStatus = HUD_COLOUR_ORANGE
		BREAK
		
		CASE SCALEFORM_PLAYER_ROW_ON_CALL
			tlStatus = "FM_PLY_ON_CALL"
			hcStatus = HUD_COLOUR_ORANGE
		BREAK
		
		CASE SCALEFORM_PLAYER_ROW_ON_CALL_HOST
			tlStatus = "FM_PLY_ON_CALLH"
			hcStatus = HUD_COLOUR_FREEMODE
		BREAK
		
		CASE SCALEFORM_PLAYER_ROW_INVITED
			tlStatus = "FM_PLY_INVITED"
			hcStatus = HUD_COLOUR_RED
		BREAK
		
		CASE SCALEFORM_PLAYER_ROW_HOST
			tlStatus = "FM_PLY_HOST"
			hcStatus = HUD_COLOUR_FREEMODE
		BREAK
		
		CASE SCALEFORM_PLAYER_ROW_ACCEPTED
			tlStatus = "FM_PLY_ACC"
			hcStatus = HUD_COLOUR_ORANGE
		BREAK
		
		CASE SCALEFORM_PLAYER_ROW_REJECTED
			tlStatus = "FM_PLY_REJ"
			hcStatus = HUD_COLOUR_RED
		BREAK
		
		CASE SCALEFORM_PLAYER_ROW_LEFT
			tlStatus = "FM_PLY_LEFT"
			hcStatus = HUD_COLOUR_ORANGE
		BREAK
		
		CASE SCALEFORM_PLAYER_ROW_BUSY
			tlStatus = "FM_PLY_BUSY"
			hcStatus = HUD_COLOUR_RED
		BREAK

		CASE SCALEFORM_PLAYER_ROW_BLOCKED
			tlStatus = "FM_PLY_BLCK"
			hcStatus = HUD_COLOUR_RED
		BREAK
		
		CASE SCALEFORM_PLAYER_ROW_CHEATER
			tlStatus = "FM_PLY_CHEAT"
			hcStatus = HUD_COLOUR_RED
		BREAK
		
		CASE SCALEFORM_PLAYER_ROW_BAD_SPORT
			tlStatus = "FM_PLY_BAD"
			hcStatus = HUD_COLOUR_RED
		BREAK

	ENDSWITCH
ENDPROC

/// PURPOSE: Takes a new player details and add them to the current invited array (players not yet in the corona)
FUNC BOOL ADD_INVITED_PLAYER_TO_CORONA_SLOT(INT i, GAMER_HANDLE &aGamer, STRING tlName, SCALEFORM_PLAYER_ROW_STATE playerInviteStatus, STRING tlCrewTag = NULL)

	g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle = aGamer
	g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedPlayerStatus = playerInviteStatus				// Update the player state here
	g_CoronaInvitedPlayers.coronaInvitedPlayers[i].iTimeout = GET_CLOUD_TIME_AS_INT()
	g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerName = tlName
	
	PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_SLOT - Adding player: ", tlName, ", position = ", i)
			
	IF NETWORK_IS_GAMER_IN_MY_SESSION(aGamer)
		g_CoronaInvitedPlayers.coronaInvitedPlayers[i].iGamerCrewState = ciTRANSITION_SESSIONS_CREW_STATE_COMPLETE
		GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(aGamer, g_CoronaInvitedPlayers.coronaInvitedPlayers[i].tlCrewTag)
	ELSE
		IF NOT IS_STRING_NULL_OR_EMPTY(tlCrewTag)
			PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_SLOT - Passed crewtag is valid. Save out and stop requesting")
			g_CoronaInvitedPlayers.coronaInvitedPlayers[i].iGamerCrewState = ciTRANSITION_SESSIONS_CREW_STATE_COMPLETE
			g_CoronaInvitedPlayers.coronaInvitedPlayers[i].tlCrewTag	= tlCrewTag
		ELSE
			PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_SLOT - Passed crewtag is invalid. Request the crew tag")
			g_CoronaInvitedPlayers.coronaInvitedPlayers[i].iGamerCrewState = ciTRANSITION_SESSIONS_CREW_STATE_REQUEST
			g_CoronaInvitedPlayers.coronaInvitedPlayers[i].tlCrewTag	= ""
		ENDIF
	ENDIF	
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tlName)
		g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedNameState = CORONA_INVITED_HAS_NAME
		RETURN TRUE
	ELSE
		IF IS_PS3_VERSION()
//		OR IS_PLAYSTATION_PLATFORM()
			g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerName = NETWORK_GET_GAMERTAG_FROM_HANDLE(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
			g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedNameState = CORONA_INVITED_HAS_NAME
			
			IF IS_PS3_VERSION()
				PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_SLOT - Player without name (PS3) Get gamertag from handle: ", g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerName)		
			ELIF IS_PLAYSTATION_PLATFORM()
				PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_SLOT - Player without name (PS4) Get gamertag from handle: ", g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerName)		
			ENDIF
			RETURN TRUE
		ELSE
			IF IS_XBOX360_VERSION()
				PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_LIST - Player without name (XBOX) Get gamertag from polling service")
			ELIF IS_XBOX_PLATFORM()
				PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_LIST - Player without name (DURANGO) Get gamertag from polling service")
			ELIF IS_PC_VERSION()
				PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_LIST - Player without name (PC) Get gamertag from polling service")
			ELIF IS_PLAYSTATION_PLATFORM()
				PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_LIST - Player without name (ORBIS) Get gamertag from polling service")
			ENDIF
			g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedNameState = CORONA_INVITED_REQUEST_NAME
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: This returns the players name if they are already in the invited list
FUNC TEXT_LABEL_63 GET_PLAYER_NAME_FROM_INVITE_LIST(GAMER_HANDLE &aGamer)
	
	INT i
	TEXT_LABEL_63 tlName = ""
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
			IF NETWORK_ARE_HANDLES_THE_SAME(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle, aGamer)
			
				tlName = g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerName
				i = NUM_NETWORK_PLAYERS
			ENDIF
		ENDIF
		
	ENDREPEAT

	RETURN tlName
ENDFUNC

/// PURPOSE: Handles players with a priority invite status to push them at front of invite queue
PROC ADD_PLAYER_TO_FRONT_OF_CORONA_INVITED_LIST(GAMER_HANDLE &aGamer, STRING tlName, SCALEFORM_PLAYER_ROW_STATE playerInviteStatus, STRING tlCrewTag = NULL)

	PRINTLN("[CORONA] ADD_PLAYER_TO_FRONT_OF_CORONA_INVITED_LIST - Adding player to FRONT of invited list: ", tlName)

	INT i
	BOOL bShiftUp
	CORONA_INVITED_PLAYER_INFO tempPlayerA
	CORONA_INVITED_PLAYER_INFO tempPlayerB
	
	tempPlayerB.gamerHandle = aGamer
	tempPlayerB.gamerName = tlname
	tempPlayerB.tlCrewTag = ""
	tempPlayerB.iGamerCrewState = ciTRANSITION_SESSIONS_CREW_STATE_NULL
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tlCrewTag)
		tempPlayerB.tlCrewTag = tlCrewTag
		tempPlayerB.iGamerCrewState = ciTRANSITION_SESSIONS_CREW_STATE_COMPLETE
	ENDIF
	
	tempPlayerB.invitedNameState = CORONA_INVITED_HAS_NAME
	tempPlayerB.invitedPlayerStatus = playerInviteStatus
	tempPlayerB.iTimeout = GET_CLOUD_TIME_AS_INT()
	
	IF IS_STRING_NULL_OR_EMPTY(tlname)
		IF IS_PS3_VERSION()
//		OR IS_PLAYSTATION_PLATFORM()
			tempPlayerB.gamerName = NETWORK_GET_GAMERTAG_FROM_HANDLE(aGamer)
		ELSE
			tempPlayerB.gamerName = GET_PLAYER_NAME_FROM_INVITE_LIST(aGamer)
			
			// Still haven't found the name so request it
			IF IS_STRING_NULL_OR_EMPTY(tempPlayerB.gamerName)
				tempPlayerB.invitedNameState = CORONA_INVITED_REQUEST_NAME
			ENDIF
		ENDIF
	ENDIF
	
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1 STEP 1
	
		// If we have a player left, start shifting immediately
		IF playerInviteStatus = SCALEFORM_PLAYER_ROW_LEFT
			bShiftUp = TRUE
		
		// Else if we have accepted and the current player slot is not left then begin
		ELIF playerInviteStatus = SCALEFORM_PLAYER_ROW_ACCEPTED
			IF g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedPlayerStatus != SCALEFORM_PLAYER_ROW_LEFT
				bShiftUp = TRUE
			ENDIF
		ENDIF
		
		IF bShiftUp

			// Otherwise, switch everything around
			tempPlayerA = g_CoronaInvitedPlayers.coronaInvitedPlayers[i]
			g_CoronaInvitedPlayers.coronaInvitedPlayers[i] = tempPlayerB
			
			tempPlayerB = tempPlayerA
		
			// If we come across this player already then remove them
			IF IS_GAMER_HANDLE_VALID(tempPlayerA.gamerHandle)
				IF NETWORK_ARE_HANDLES_THE_SAME(tempPlayerA.gamerHandle, aGamer)
				OR ARE_STRINGS_EQUAL(tempPlayerA.gamerName, tlName)
					EXIT
				ENDIF
			ENDIF
		
		ENDIF
	
	ENDFOR

ENDPROC

/// PURPOSE: Broadcasts a gamer instruction with details of player status
PROC BROADCAST_CORONA_INVITE_PLAYERS_DETAILS(GAMER_HANDLE &aGamer, STRING tlName, SCALEFORM_PLAYER_ROW_STATE playerInviteStatus)
	
	INT iPlayerStatus = TRANSITION_SESSION_INSTRUCTION_MEMBER_INVITED
	
	SWITCH playerInviteStatus
		CASE SCALEFORM_PLAYER_ROW_BUSY			iPlayerStatus = TRANSITION_SESSION_INSTRUCTION_MEMBER_BUSY			BREAK
		CASE SCALEFORM_PLAYER_ROW_BLOCKED		iPlayerStatus = TRANSITION_SESSION_INSTRUCTION_MEMBER_BLOCKED		BREAK
		CASE SCALEFORM_PLAYER_ROW_CHEATER		iPlayerStatus = TRANSITION_SESSION_INSTRUCTION_MEMBER_CHEATER		BREAK
		CASE SCALEFORM_PLAYER_ROW_BAD_SPORT		iPlayerStatus = TRANSITION_SESSION_INSTRUCTION_MEMBER_BAD_SPORT		BREAK
		CASE SCALEFORM_PLAYER_ROW_ACCEPTED		iPlayerStatus = TRANSITION_SESSION_INSTRUCTION_MEMBER_ACCEPTED		BREAK
		CASE SCALEFORM_PLAYER_ROW_REJECTED		iPlayerStatus = TRANSITION_SESSION_INSTRUCTION_MEMBER_REJECTED		BREAK
	ENDSWITCH
	
	NETWORK_SEND_TRANSITION_GAMER_INSTRUCTION(	aGamer, tlName, TRANSITION_SESSION_INSTRUCTION_MEMBER_INVITE_LIST, iPlayerStatus, TRUE)

ENDPROC

/// PURPOSE: Returns the correct player status for invite list
FUNC SCALEFORM_PLAYER_ROW_STATE GET_CORONA_INVITE_STATUS_FROM_INSTRUCTION(INT iInstruction)

	SWITCH iInstruction
		CASE TRANSITION_SESSION_INSTRUCTION_MEMBER_BUSY			RETURN	SCALEFORM_PLAYER_ROW_BUSY
		CASE TRANSITION_SESSION_INSTRUCTION_MEMBER_BLOCKED		RETURN	SCALEFORM_PLAYER_ROW_BLOCKED
		CASE TRANSITION_SESSION_INSTRUCTION_MEMBER_CHEATER		RETURN	SCALEFORM_PLAYER_ROW_CHEATER
		CASE TRANSITION_SESSION_INSTRUCTION_MEMBER_BAD_SPORT	RETURN	SCALEFORM_PLAYER_ROW_BAD_SPORT
		CASE TRANSITION_SESSION_INSTRUCTION_MEMBER_ACCEPTED		RETURN	SCALEFORM_PLAYER_ROW_ACCEPTED
		CASE TRANSITION_SESSION_INSTRUCTION_MEMBER_REJECTED		RETURN	SCALEFORM_PLAYER_ROW_REJECTED
	ENDSWITCH
	
	RETURN SCALEFORM_PLAYER_ROW_INVITED
ENDFUNC

FUNC BOOL IS_GAMER_IN_CORONA_INVITED_LIST(GAMER_HANDLE &aGamer)

	IF IS_GAMER_HANDLE_VALID(aGamer)
		INT i
		FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
			IF IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
			AND NETWORK_ARE_HANDLES_THE_SAME(aGamer, g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the next available slot for invited players   
FUNC INT ADD_INVITED_PLAYER_TO_CORONA_LIST(	INT iSeed, GAMER_HANDLE &aGamer, STRING tlName, CORONA_INVITE_TYPE eCoronaInviteType = CORONA_INVITE_TYPE_STANDARD, 
											SCALEFORM_PLAYER_ROW_STATE playerInviteStatus = SCALEFORM_PLAYER_ROW_INVITED, BOOL bBroadcastPlayer = TRUE, STRING tlCrewtag = NULL)
		
	IF playerInviteStatus = SCALEFORM_PLAYER_ROW_INVITED
		PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_LIST - Setting them to invited")
		NETWORK_SET_GAMER_INVITED_TO_TRANSITION(aGamer)
	ELSE
		NETWORK_REMOVE_TRANSITION_INVITE(aGamer)
	ENDIF
	
	IF iSeed < NUM_NETWORK_PLAYERS


		// If the player has been sent an presence invite, then don't add to list until we receive event
		IF eCoronaInviteType = CORONA_INVITE_TYPE_PRESENCE
		// Check to see if presence replies are being sent, if so we should wait for the INVITE_REPLY (2295106)
		AND NETWORK_TRY_ACCESS_TUNABLE_BOOL_HASH(HASH("MP_GLOBAL"), HASH("SEND_PRESENCE_INVITE_ACKS"), TRUE)
		
			PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_LIST - Not adding gamer: ", tlName, ", Invite type = CORONA_INVITE_TYPE_PRESENCE. T(", GET_CLOUD_TIME_AS_INT(), ")")
		
			RETURN iSeed
		ENDIF

		BOOL bExistsInCorona
		// Quit out if the player is already part of our corona
		IF IS_PLAYER_ALREADY_IN_CORONA(aGamer)
		
			PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_LIST - Gamer already in list: ", tlName, " T(", GET_CLOUD_TIME_AS_INT(), ")")
		
			bExistsInCorona = TRUE
		ENDIF
		
		IF NETWORK_IS_GAMER_IN_MY_SESSION(aGamer)
			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(aGamer)
			IF playerID != INVALID_PLAYER_INDEX()
				IF IS_PLAYER_SCTV(playerID)
				
					PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_LIST - Gamer already is SCTV: ", tlName, " T(", GET_CLOUD_TIME_AS_INT(), ")")
				
					bExistsInCorona = TRUE
				ENDIF
			ENDIF
		ENDIF

		// Process any status that we definitely want to show
		IF (playerInviteStatus = SCALEFORM_PLAYER_ROW_ACCEPTED AND NOT bExistsInCorona)
		OR playerInviteStatus = SCALEFORM_PLAYER_ROW_LEFT
			
			ADD_PLAYER_TO_FRONT_OF_CORONA_INVITED_LIST(aGamer, tlName, playerInviteStatus)
			
			IF HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
				REMOVE_END_OF_JOB_GAMER_TO_CORONA_LIST(aGamer)
			ENDIF
		
			SET_CORONA_BIT(CORONA_INVITED_PLAYER_LIST_UPDATED)
			RETURN 0
		ENDIF
		
		// If the player is already in, EXIT
		IF bExistsInCorona
			RETURN iSeed
		ENDIF

		INT i

		// Else, process player and add them to the invited list
		FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
		
			// Find next slot that has invalid player index
			IF NOT IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
				
				IF ADD_INVITED_PLAYER_TO_CORONA_SLOT(i, aGamer, tlName, playerInviteStatus, tlCrewtag)
				
					PRINTLN("[CORONA] ADD_INVITED_PLAYER_TO_CORONA_LIST - Gamer added to list: ", g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerName, " T(", GET_CLOUD_TIME_AS_INT(), ") to slot: ", i)
				
					IF HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
						REMOVE_END_OF_JOB_GAMER_TO_CORONA_LIST(aGamer)
					ENDIF
				
					IF bBroadcastPlayer
						BROADCAST_CORONA_INVITE_PLAYERS_DETAILS(aGamer, g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerName, g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedPlayerStatus)
					ENDIF
					
					SET_CORONA_BIT(CORONA_INVITED_PLAYER_LIST_UPDATED)
				ENDIF
				
				RETURN i
			ELSE
				// Have we already got the player in the list
				IF NETWORK_ARE_HANDLES_THE_SAME(aGamer, g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
				
					// Update our player status (if we have accepted, ignore anything that further comes through)
					IF g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedPlayerStatus != SCALEFORM_PLAYER_ROW_ACCEPTED
					AND NOT (g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedPlayerStatus = SCALEFORM_PLAYER_ROW_INVITED AND playerInviteStatus = SCALEFORM_PLAYER_ROW_INVITED)
						g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedPlayerStatus = playerInviteStatus
						g_CoronaInvitedPlayers.coronaInvitedPlayers[i].iTimeout = GET_CLOUD_TIME_AS_INT()	// Update our time
					ENDIF
				
					IF bBroadcastPlayer
						BROADCAST_CORONA_INVITE_PLAYERS_DETAILS(aGamer, g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerName, g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedPlayerStatus)
					ENDIF
				
					SET_CORONA_BIT(CORONA_INVITED_PLAYER_LIST_UPDATED)
					RETURN i
				ENDIF
			ENDIF
			
		ENDFOR
	ENDIF

	RETURN NUM_NETWORK_PLAYERS
ENDFUNC


/// PURPOSE: Cleans up the corona invited list that we use across clients
PROC RESET_CORONA_INVITED_LIST()
	
	PRINTLN("[CORONA] CLEANUP_CORONA_STRUCT - RESET_CORONA_INVITED_LIST() clean up all our invite players")
	
	GAMER_HANDLE aGamer
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
	
		// Clear the invite player list
		g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle = aGamer
		g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerName = ""
		g_CoronaInvitedPlayers.coronaInvitedPlayers[i].tlCrewTag = ""
		g_CoronaInvitedPlayers.coronaInvitedPlayers[i].iGamerCrewState = ciTRANSITION_SESSIONS_CREW_STATE_NULL
		g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedNameState = CORONA_INVITED_HAS_NAME
		g_CoronaInvitedPlayers.coronaInvitedPlayers[i].invitedPlayerStatus = SCALEFORM_PLAYER_ROW_NONE
	
	ENDREPEAT
			
	// Clear counters for out invited player list
	g_CoronaInvitedPlayers.iPlayerToGetGamerTag = 0
	g_CoronaInvitedPlayers.iPlayerToUpdateStatus = 0
	
ENDPROC


PROC DO_FMMC_CORONA_SPINNER(BOOL &bSetUp, STRING sMainStringSlot, INT iIcon, STRING stOncall)
	IF bSetUp = FALSE
		PRINTLN("SPIN SET UP TIME - DO_FMMC_CORONA_SPINNER: ", sMainStringSlot, " - GET_GAME_TIMER = ", GET_GAME_TIMER(), " GET_FRAME_COUNT = ", GET_FRAME_COUNT())
		//If the on call sting is not empty
		IF NOT IS_STRING_NULL_OR_EMPTY(stOncall)
		//and we've got a transiton session host
		AND GET_TRANSITION_SESSION_HOST() != -1
		//and we're starting on call
		AND(AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
		//or the on call has been set up
		OR HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP())
		//and the host is not an on call host
		AND NOT IS_THIS_TRANSITION_SESSION_PLAYER_ON_CALL(GET_TRANSITION_SESSION_HOST())
		//And the corona start time is less than the current cloud time as an INT - 1 so it'a a bit more acurate
		AND (GET_TRANSITION_PLAYER_CORONA_START() - 1) > GET_CLOUD_TIME_AS_INT()
			BEGIN_TEXT_COMMAND_BUSYSPINNER_ON(stOncall)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[GET_TRANSITION_SESSION_HOST()].szGamerTag)
			END_TEXT_COMMAND_BUSYSPINNER_ON(iIcon)
		//default the spinner
		ELSE
			BEGIN_TEXT_COMMAND_BUSYSPINNER_ON(sMainStringSlot)
			END_TEXT_COMMAND_BUSYSPINNER_ON(iIcon)
		ENDIF
		bSetUp = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Function to maintain the DOF settings while actively in the corona (vehicle, betting, outfit screens)
FUNC BOOL MAINTAIN_IN_CORONA_DOF_SETTINGS()
	
	// If we are in a heist and on last gen, we don't need to use HI DOF (2148250)
	IF IS_PS3_VERSION()
	OR IS_XBOX360_VERSION()
		IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Else, apply the DOF every frame
	SET_USE_HI_DOF()	
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns the correct anim post fx for the corona we are in
FUNC STRING GET_CORONA_FX_IN( BOOL bIsHeist = FALSE  )
	
	
	IF HEIST_ISLAND_FLOW_IS_A_HEIST_ISLAND_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		RETURN "MenuMGIslandHeistIn"
	ENDIF
	
	// First check if we are in a heist (we can override this or check our global flag)
	IF bIsHeist
	OR IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
		RETURN "MenuMGHeistIn"	
	ENDIF
	
	// Is the player on an active tournament
	IF IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
		RETURN "MenuMGTournamentIn"
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_A_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN "MenuMGRemixIn"
	ENDIF
	
	IF IS_THIS_A_NEW_SURVIVAL_MISSION(g_FMMC_STRUCT.iAdversaryModeType)
	AND IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_Halloween)
		RETURN "MenuSurvivalAlienIn"
	ENDIF
	
	// Return the default effect
	RETURN "MenuMGSelectionIn"
ENDFUNC

/// PURPOSE: Returns the correct anim post fx for the corona we are in when cleaning 
FUNC STRING GET_CORONA_FX_OUT(  BOOL bIsHeist = FALSE  )
	
	
	IF HEIST_ISLAND_FLOW_IS_A_HEIST_ISLAND_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		RETURN "MenuMGIslandHeistOut "
	ENDIF
	
	// First check if we are in a heist
	IF bIsHeist
	OR IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
		RETURN "MenuMGHeistOut"	
	ENDIF
	
	// Is the player on an active tournament
	IF IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
		RETURN "MenuMGTournamentOut"
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_A_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN "MenuMGRemixOut"
	ENDIF
	
	IF IS_THIS_A_NEW_SURVIVAL_MISSION(g_FMMC_STRUCT.iAdversaryModeType)
	AND IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_Halloween)
		RETURN "MenuSurvivalAlienOut"
	ENDIF
	
	// Return the default effect
	RETURN "MenuMGOut"
ENDFUNC

/// PURPOSE: Terminates any post fx we have active specific to the corona
FUNC BOOL STOP_ALL_CORONA_FX_IN()
	
	BOOL bStoppedFX
	IF ANIMPOSTFX_IS_RUNNING("MenuMGIn")
		PRINTLN("[CORONA] STOP_ALL_CORONA_FX_IN - Stop MenuMGIn")
		ANIMPOSTFX_STOP("MenuMGIn")
		bStoppedFX = TRUE
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MenuMGSelectionIn")
		PRINTLN("[CORONA] STOP_ALL_CORONA_FX_IN - Stop MenuMGSelectionIn")
		ANIMPOSTFX_STOP("MenuMGSelectionIn")
		bStoppedFX = TRUE
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MenuMGSelectionTint")
		PRINTLN("[CORONA] STOP_ALL_CORONA_FX_IN - Stop MenuMGSelectionTint")
		ANIMPOSTFX_STOP("MenuMGSelectionTint")
		bStoppedFX = TRUE
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MenuMGTournamentIn")
		PRINTLN("[CORONA] STOP_ALL_CORONA_FX_IN - Stop MenuMGTournamentIn")
		ANIMPOSTFX_STOP("MenuMGTournamentIn")
		bStoppedFX = TRUE
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MenuMGHeistIn")
		PRINTLN("[CORONA] STOP_ALL_CORONA_FX_IN - Stop MenuMGHeistIn")
		ANIMPOSTFX_STOP("MenuMGHeistIn")
		bStoppedFX = TRUE
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MenuMGHeistTint")
		PRINTLN("[CORONA] STOP_ALL_CORONA_FX_IN - Stop MenuMGHeistTint")
		ANIMPOSTFX_STOP("MenuMGHeistTint")
		bStoppedFX = TRUE
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MenuMGHeistIntro")
		PRINTLN("[CORONA] STOP_ALL_CORONA_FX_IN - Stop MenuMGHeistIntro")
		ANIMPOSTFX_STOP("MenuMGHeistIntro")
		bStoppedFX = TRUE
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MenuMGTournamentTint")
		PRINTLN("[CORONA] STOP_ALL_CORONA_FX_IN - Stop MenuMGTournamentTint")
		ANIMPOSTFX_STOP("MenuMGTournamentTint")
		bStoppedFX = TRUE
	ENDIF
	
	
	IF ANIMPOSTFX_IS_RUNNING("MenuMGRemixIn")
		PRINTLN("[CORONA] STOP_ALL_CORONA_FX_IN - Stop MenuMGRemixIn")
		ANIMPOSTFX_STOP("MenuMGRemixIn")
		bStoppedFX = TRUE
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MenuSurvivalAlienIn")
		PRINTLN("[CORONA] STOP_ALL_CORONA_FX_IN - Stop MenuSurvivalAlienIn")
		ANIMPOSTFX_STOP("MenuSurvivalAlienIn")
		bStoppedFX = TRUE
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MenuMGIslandHeistIn")
		PRINTLN("[CORONA] STOP_ALL_CORONA_FX_IN - Stop MenuMGIslandHeistIn")
		ANIMPOSTFX_STOP("MenuMGIslandHeistIn")
		bStoppedFX = TRUE
	ENDIF
	
	RETURN bStoppedFX
ENDFUNC

/// PURPOSE: Applies an overall tint to the game for specific game modes
PROC SET_CORONA_FX_TINT()


	// If we are in a tournament, add a tint to the in corona screens
	IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
	OR IS_CORONA_BIT_SET(CORONA_GLOBAL_GENERIC_HEIST_FINALE_FLAG)
//		IF NOT ANIMPOSTFX_IS_RUNNING("MenuMGHeistTint")
//			PRINTLN("[CORONA] SET_CORONA_FX_TINT - Play MenuMGHeistTint")
//			ANIMPOSTFX_PLAY("MenuMGHeistTint", 0, TRUE)
//		ENDIF
		
		EXIT
	ENDIF
	
	// If we are in a tournament, add a golden tint to the game
	IF IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
//		IF NOT ANIMPOSTFX_IS_RUNNING("MenuMGTournamentTint")
//			PRINTLN("[CORONA] SET_CORONA_FX_TINT - Play MenuMGTournamentTint")
//			ANIMPOSTFX_PLAY("MenuMGTournamentTint", 0, TRUE)
//		ENDIF
		
		EXIT
	ENDIF


	ANIMPOSTFX_PLAY("MenuMGSelectionTint", 0, TRUE)
ENDPROC

/// PURPOSE: Returns TRUE if the player is safe to clean up corona cameras. FALSE if the player is SCTV and watching a player in apartment
FUNC BOOL SHOULD_CORONA_PLAYER_CLEAR_CORONA_CAMERAS()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget, FALSE)
				IF IS_PLAYER_IN_PROPERTY(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget, TRUE)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


/// PURPOSE: Cleans up first scripted cam of corona screens
PROC CLEAN_UP_INTRO_STAGE_CAM(CAMERA_INDEX &ciCam, INT iMissionType, BOOL bMissionRequest = FALSE, BOOL bClearBlur = TRUE, BOOL bStopRenderScriptCams = TRUE)
	PRINTLN("[TS] CLEAN_UP_INTRO_STAGE_CAM")
	DEBUG_PRINTCALLSTACK()
	
	// Cleanup camera if it exists
	IF (iMissionType != FMMC_TYPE_MISSION AND iMissionType != FMMC_TYPE_SURVIVAL)
	OR  bMissionRequest = TRUE
		PRINTLN("[TS] CLEAN_UP_INTRO_STAGE_CAM - CALLED")
		IF DOES_CAM_EXIST(ciCam)
			NET_PRINT("CLEAN_UP_INTRO_STAGE_CAM - cleaning up camera") NET_NL()
			SET_CAM_ACTIVE(ciCam, FALSE)
			
			IF bStopRenderScriptCams
				NET_PRINT("CLEAN_UP_INTRO_STAGE_CAM - stop script cameras rendering") NET_NL()
				RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
			ENDIF
			
			DESTROY_CAM(ciCam)	
			ciCam = NULL
			PRINTLN("[TS] CLEAR_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA - CLEAN_UP_INTRO_STAGE_CAM")
			CLEAR_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
			
			IF SHOULD_CORONA_PLAYER_CLEAR_CORONA_CAMERAS()
				CLEAR_FOCUS()			
				PRINTLN("CLEAR_FOCUS()")
			ENDIF
			
			//SET_FOCUS_ENTITY(PLAYER_PED_ID())
			PRINTLN("SET_FOCUS_ENTITY(PLAYER_PED_ID())")
			NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
			PRINTLN("NETWORK_SET_IN_FREE_CAM_MODE(FALSE)")
			
			IF g_Private_BailHappened = FALSE
			AND bClearBlur = TRUE

//				IF STOP_ALL_CORONA_FX_IN()			
//					PRINTLN("ANIMPOSTFX_PLAY(", GET_CORONA_FX_OUT(), ", 0, FALSE)")
//					ANIMPOSTFX_PLAY(GET_CORONA_FX_OUT(), 0, FALSE)
//				ENDIF
			
				#IF IS_DEBUG_BUILD
					DEBUG_PRINTCALLSTACK()
					NET_NL()NET_PRINT("SET_SKYFREEZE_STAGE() - Called  = SKYFREEZE_FROZEN_CLEAR")
				#ENDIF
				TOGGLE_RENDERPHASES(TRUE) 
				TOGGLE_RENDERPHASES(TRUE)
				
				PRINTLN("[TS] CLEAN_UP_INTRO_STAGE_CAM  -  TOGGLE_RENDERPHASES(TRUE)")
				PRINTLN("[TS] CLEAN_UP_INTRO_STAGE_CAM  -  TOGGLE_RENDERPHASES(TRUE)")
			ENDIF
			
			IF NOT IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()					
				IF IS_XBOX_PLATFORM()
				OR IS_PLAYSTATION_PLATFORM()
					IF NOT g_sCURRENT_UGC_STATUS.g_bGettingContentById //ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID()
						PRINTLN("[CORONA] - CLEAN_UP_INTRO_STAGE_CAM - BUSYSPINNER_OFF called")						
						BUSYSPINNER_OFF()
					ENDIF
				ELSE
					// Turn off the downloading spinner
					PRINTLN("[CORONA] - CLEAN_UP_INTRO_STAGE_CAM - BUSYSPINNER_OFF called")						
					BUSYSPINNER_OFF()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[TS] CLEAN_UP_INTRO_STAGE_CAM - NOT CALLED")		
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up any FX or screen states set by the corona at the time of launching the Job.
PROC CLEANUP_ALL_CORONA_FX(BOOL bShouldTurnOffCams = TRUE, BOOL bBlockClearFocus = FALSE)

	NET_PRINT("CLEANUP_ALL_CORONA_FX") NET_NL()
	
	// We no longer need to override out HIDOF for 2122508
	SET_HIDOF_OVERRIDE(FALSE, FALSE, 0, 0, 0, 0)
	
	IF NOT SHOULD_CORONA_PLAYER_CLEAR_CORONA_CAMERAS()
		bShouldTurnOffCams = FALSE
	ENDIF	
	
	IF bShouldTurnOffCams
		RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
	ENDIF
	
	IF SHOULD_CORONA_PLAYER_CLEAR_CORONA_CAMERAS()
	AND !bBlockClearFocus
		CLEAR_FOCUS()			
		PRINTLN("CLEAR_FOCUS()")
	ENDIF
	
	NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
	
	IF IS_SCREENBLUR_FADE_RUNNING() //This may break stuff - So double check
		TRIGGER_SCREENBLUR_FADE_OUT(200)
	ENDIF

	// Turn off the downloading spinner
	PRINTLN("[CORONA] - CLEANUP_ALL_CORONA_FX - BUSYSPINNER_OFF called")						
	BUSYSPINNER_OFF()
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		RESET_OVERRIDE_OVERHEAD_STATE()
		PRINTLN("CLEANUP_ALL_CORONA_FX - RESET_OVERRIDE_OVERHEAD_STATE")
	ENDIF
	
	g_bGamerTagBlocker = FALSE
	
	STOP_ALL_CORONA_FX_IN()
	SET_SKYBLUR_CLEAR()

ENDPROC

//Moved to here from fmmc_corona_controller so other scripts can get access
//Delete Corona PED
PROC FM_DELETE_PED_FOR_CORONA(PED_INDEX &aPed #IF IS_DEBUG_BUILD, INT iPedNumber #ENDIF)

	#IF IS_DEBUG_BUILD
		IF bDebugClonesForCoronaScene
		OR g_bDummyTeamCoronaPosition
			EXIT
		ENDIF
	#ENDIF

	IF DOES_ENTITY_EXIST(aPed)
	AND NOT IS_ENTITY_DEAD(aPed)
		// Clean up any assets / data brought in for this ped.
		CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(aPed, AAT_IDLE)
		RELEASE_PED_PRELOAD_VARIATION_DATA(aPed)
		RELEASE_PED_PRELOAD_PROP_DATA(aPed)
		DELETE_PED(aPed)
		
		#IF IS_DEBUG_BUILD
		PRINTLN(" DELETE_PED_FOR_CORONA number = ", iPedNumber)
		#ENDIF
	ENDIF
ENDPROC

PROC FM_DELETE_CORONA_PED(INT iIndex)
	
	FM_DELETE_PED_FOR_CORONA(g_CoronaClonePeds.piPlayersPed[iIndex].pedID #IF IS_DEBUG_BUILD, iIndex #ENDIF)
	g_CoronaClonePeds.piPlayersPed[iIndex].iCoronaCloneFlags = 0
	g_CoronaClonePeds.piPlayersPed[iIndex].iCloneOutfitStyle = -1
	g_CoronaClonePeds.piPlayersPed[iIndex].iCloneOutfit = -1
	g_CoronaClonePeds.piPlayersPed[iIndex].iCloneMaskStyle = -1
	g_CoronaClonePeds.piPlayersPed[iIndex].iCloneMask = -1
	g_CoronaClonePeds.piPlayersPed[iIndex].iClonedTeam = -1
	g_CoronaClonePeds.piPlayersPed[iIndex].iClonedTeamForOutfit = -1
	g_CoronaClonePeds.piPlayersPed[iIndex].iClonePreloadStage = 0
ENDPROC

PROC FM_DELETE_CORONA_MODEL_PED(INT iIndex)
	
	IF DOES_ENTITY_EXIST(g_CoronaClonePeds.piPlayerModelSwaps[iIndex].pedID)
		DELETE_PED(g_CoronaClonePeds.piPlayerModelSwaps[iIndex].pedID)
	ENDIF
	g_CoronaClonePeds.piPlayerModelSwaps[iIndex].playerSwap = INVALID_PLAYER_INDEX()
	
	PRINTLN(" FM_DELETE_CORONA_MODEL_PED iIndex = ", iIndex)	
ENDPROC

/// PURPOSE: This is called when a player does not exist in the corona. If we have a ped created we will fade them out
PROC FM_DELETE_CORONA_PED_FOR_PLAYER_LEFT(INT iIndex)
	
	IF IS_BIT_SET(g_CoronaClonePeds.piPlayersPed[iIndex].iCoronaCloneFlags, ciCORONA_CLONE_CREATED)
		
		// If we have not initialised the fade out yet, do so.
		IF NOT IS_BIT_SET(g_CoronaClonePeds.piPlayersPed[iIndex].iCoronaCloneFlags, ciCORONA_CLONE_FADE_OUT)
			
			PRINTLN("[CORONA] FM_DELETE_CORONA_PED_FOR_PLAYER_LEFT - Begin fade out for ped: ", iIndex, " T(", GET_CLOUD_TIME_AS_INT(), ")")
			SET_BIT(g_CoronaClonePeds.piPlayersPed[iIndex].iCoronaCloneFlags, ciCORONA_CLONE_FADE_OUT)
			
			g_CoronaClonePeds.piPlayersPed[iIndex].cloneTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciCORONA_CLONE_FADE_OUT_TIME)
		ELSE
			// While we have not passed the fade out time, slowly fade the ped out.
			IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_CoronaClonePeds.piPlayersPed[iIndex].cloneTimer)
			AND DOES_ENTITY_EXIST(g_CoronaClonePeds.piPlayersPed[iIndex].pedID)
			AND NOT IS_ENTITY_DEAD(g_CoronaClonePeds.piPlayersPed[iIndex].pedID)
				
				INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_CoronaClonePeds.piPlayersPed[iIndex].cloneTimer))
				
				INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / ciCORONA_CLONE_FADE_OUT_TIME) * 255)
				
				PRINTLN("[CORONA] Fading out ped: ", iIndex, " with alpha = ", iAlpha, " iTimeDifference = ", iTimeDifference)
				
				SET_ENTITY_ALPHA(g_CoronaClonePeds.piPlayersPed[iIndex].pedID, iAlpha, FALSE)
			ELSE
				FM_DELETE_CORONA_PED(iIndex)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

//Delete all those corona PEDs
PROC FM_DELETE_ALL_PED_FOR_CORONA()
	PRINTLN("FM_DELETE_ALL_PED_FOR_CORONA")
	INT iLoop 
	REPEAT NUM_NETWORK_PLAYERS iLoop
		FM_DELETE_CORONA_PED(iLoop)
	ENDREPEAT
	REPEAT ciCORONA_SUPPORTED_PLAYER_MODEL_SWAPS iLoop
		FM_DELETE_CORONA_MODEL_PED(iLoop)
	ENDREPEAT
ENDPROC


// KGM 16/4/13: Moved here to avoid cyclic header issues
/// PURPOSE:
///    Player gets put in an 8-player tutorial session for the intro
/// RETURNS:
///    
FUNC BOOL IS_LOCAL_PLAYER_IN_TUTORIAL_SESSION_FOR_INTRO()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTutInTutorialSession)
ENDFUNC

/// PURPOSE:
///    Determine if the player is on the race/dm tutorial
/// RETURNS:
///    
FUNC BOOL IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() 

	#IF IS_DEBUG_BUILD
	IF g_b_ForceTutorial
		RETURN TRUE
	ENDIF
	#ENDIF

	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PlayerRunningActivityTutorial)
ENDFUNC

/// PURPOSE:
///    Determine if the player is on the LTS tutorial
/// RETURNS:
///    
/// NOTES: Set up By Keith so it can be FREE rather than cost the standard LTS price
FUNC BOOL IS_LOCAL_PLAYER_ON_TUTORIAL_LTS() 
	RETURN (g_isOnOrTriggeringTutorialLTS)
ENDFUNC

FUNC VECTOR Get_Coords_Of_Tutorial_LTS()
	IF NOT (IS_LOCAL_PLAYER_ON_TUTORIAL_LTS())
		VECTOR nullCoords = << 0.0, 0.0, 0.0 >>
		RETURN (nullCoords)
	ENDIF
	RETURN (g_coordsTutorialLTS)
ENDFUNC

// PURPOSE: Set player as on Tutorial LTS
PROC Set_Player_As_On_Tutorial_LTS(VECTOR paramCoords)
	g_isOnOrTriggeringTutorialLTS	= TRUE
	g_coordsTutorialLTS				= paramCoords
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [dsw]:")
		NET_PRINT_TIME()
		NET_PRINT("Setting Player is on Tutorial LTS. Coords: ")
		NET_PRINT_VECTOR(g_coordsTutorialLTS)
		NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF
ENDPROC

// PURPOSE: Clear player as on Tutorial LTS
PROC Clear_Player_As_On_Tutorial_LTS()
	g_isOnOrTriggeringTutorialLTS = FALSE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [dsw]:")
		NET_PRINT_TIME()
		NET_PRINT("Clearing Player is on Tutorial LTS") NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF
ENDPROC

/// PURPOSE: Return if player needs to pay for Job  
FUNC BOOL DO_I_NEED_TO_PAY_TO_PLAY_THIS_FM_ACTIVITY(INT iMissionType, INT iMissionSubtype)
//	#IF IS_DEBUG_BUILD
//	IF g_SkipFmTutorials
//	OR g_ShouldShiftingTutorialsBeSkipped
//		RETURN FALSE
//	ENDIF
//	#ENDIF

	// KGM 28/8/13: A poor player is allowed to Play Again or play a Random mission after completing a mission without having to pay
	IF (g_allowPoorPlayerToPlayAgainForFree)
		RETURN FALSE
	ENDIF

	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
	OR IS_LOCAL_PLAYER_ON_TUTORIAL_LTS()
		//-- Tutorial is free
		RETURN FALSE
	ENDIF
	
	IF CONTENT_IS_USING_ARENA() 
		RETURN FALSE
	ENDIF
	
	// KEITH 14/7/13: bug 1548493 - no charge for missions, but normal change for LTS and CTF
	IF (iMissionType = FMMC_TYPE_MISSION)
		IF (iMissionSubtype = FMMC_MISSION_TYPE_LTS)
		OR (iMissionSubtype = FMMC_MISSION_TYPE_CTF)
		OR (iMissionSubtype = FMMC_MISSION_TYPE_COOP)
		OR (iMissionSubtype = FMMC_MISSION_TYPE_VERSUS)
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF iMissionType != FMMC_TYPE_GANGHIDEOUT
	AND iMissionType < FMMC_TYPE_MG
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// Activity Costs

/// PURPOSE: Get the amount player must pay for job 
FUNC INT GET_FM_ACTIVITY_COST(INT iMissionType, INT iMissionSubtype, BOOL bReturnCostEvenIfPlayerIsPoor = FALSE, INT iNumberOfPlayers = -1)
	
	// KGM 28/8/13: A poor player is allowed to Play Again or play a Random mission after completing a mission without having to pay
	IF (g_allowPoorPlayerToPlayAgainForFree)
	AND NOT bReturnCostEvenIfPlayerIsPoor
		RETURN (0)
	ENDIF
	
	IF CONTENT_IS_USING_ARENA() 
		RETURN (0)
	ENDIF
	
	//on a playlist do this
	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		RETURN g_sMPTunables.iPLAYLIST_ENTRY_FEE			
	ENDIF

	//Not a playlist
	SWITCH iMissionType
		CASE FMMC_TYPE_MISSION     		// KEITH 14/7/13: bug 1548493 - no charge for missions, normal charge for CTF and LTS
			SWITCH (iMissionSubtype)
				CASE FMMC_MISSION_TYPE_LTS
					RETURN g_sMPTunables.ientrancefee_lts_expenditure_tunable
				CASE FMMC_MISSION_TYPE_CTF
					RETURN g_sMPTunables.ientrancefee_ctf_expenditure_tunable
				CASE FMMC_MISSION_TYPE_VERSUS
					RETURN g_sMPTunables.ientrancefee_versus_expenditure_tunable
				CASE FMMC_MISSION_TYPE_COOP
					RETURN g_sMPTunables.ientrancefee_coop_expenditure_tunable
			ENDSWITCH
			
			RETURN 0
		CASE FMMC_TYPE_DEATHMATCH
			RETURN g_sMPTunables.ientrancefee_deathmatch_expenditure_tunable 
		CASE FMMC_TYPE_SURVIVAL 
			RETURN g_sMPTunables.ientrancefee_survival_expenditure_tunable 
		CASE FMMC_TYPE_GANGHIDEOUT
			RETURN 500
		CASE FMMC_TYPE_BASE_JUMP
		
			// Do not charge for solo para / races 1635150
			IF iNumberOfPlayers = 1
				PRINTLN("GET_FM_ACTIVITY_COST - 1 player so don't charge (PARA)")
				RETURN 0
			ELSE
				RETURN g_sMPTunables.ientrancefee_parachuting_expenditure_tunable 
			ENDIF
		BREAK
		CASE FMMC_TYPE_RACE
		
			IF iNumberOfPlayers = 1
				PRINTLN("GET_FM_ACTIVITY_COST - 1 player so don't charge (RACE)")
				RETURN g_sMPTunables.ivehicle_rental_price_modifier
			ELSE
				SWITCH g_FMMC_STRUCT.iRaceType
					CASE FMMC_RACE_TYPE_AIR
					CASE FMMC_RACE_TYPE_AIR_P2P
						RETURN (g_sMPTunables.ientrancefee_air_race_expenditure_tunable + g_sMPTunables.ivehicle_rental_price_modifier)
					CASE FMMC_RACE_TYPE_BOAT
					CASE FMMC_RACE_TYPE_BOAT_P2P
						RETURN (g_sMPTunables.ientrancefee_sea_race_expenditure_tunable + g_sMPTunables.ivehicle_rental_price_modifier)
					CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE
					CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE_P2P
						RETURN (g_sMPTunables.ientrancefee_bike_race_expenditure_tunable + g_sMPTunables.ivehicle_rental_price_modifier)
					DEFAULT
						RETURN (g_sMPTunables.ientrancefee_car_race_expenditure_tunable + g_sMPTunables.ivehicle_rental_price_modifier)	// JAMES A: 1598529 - costs to borrow a car / bike
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

// PURPOSE:	KGM 16/4/13: A combination of the above two functions to check if the player can afford to play the current activity - used within Missions At Coords and Joblist Invite systems
FUNC BOOL CAN_I_AFFORD_TO_PLAY_THIS_ACTIVITY(INT iMissionType, INT iMissionSubtype)
	IF NOT (DO_I_NEED_TO_PAY_TO_PLAY_THIS_FM_ACTIVITY(iMissionType, iMissionSubtype))
		RETURN TRUE
	ENDIF
	
	IF NETWORK_CAN_SPEND_MONEY(GET_FM_ACTIVITY_COST(iMissionType, iMissionSubtype), FALSE, TRUE, FALSE)
		RETURN TRUE
	ENDIF
	
	// Can't afford it
	RETURN FALSE
ENDFUNC




//Set that I'm reentering the same corona
PROC SET_ENTERING_SAME_CORONA()
	PRINTLN("SET_ENTERING_SAME_CORONA")
	g_sLastCoronaDetails.bNewCorona = TRUE
ENDPROC
//Clear that I'm reentering the same corona
PROC CLEAR_ENTERING_SAME_CORONA()
	PRINTLN("CLEAR_ENTERING_SAME_CORONA")
	g_sLastCoronaDetails.bNewCorona = FALSE	
ENDPROC
//Check that I'm reentering the same corona
FUNC BOOL AM_I_RE_ENTERING_SAME_CORONA()
	RETURN (g_sLastCoronaDetails.bNewCorona)
ENDFUNC

//Set the content ID of the last corona I was in
PROC SET_LAST_CORONA_CONTENT_ID(TEXT_LABEL_23 tl23LastCoronaContentID)
	PRINTLN("SET_ENTERING_SAME_CORONA - tl31LastCoronaContentID")
	g_sLastCoronaDetails.tl23LastCoronaContentID = tl23LastCoronaContentID
ENDPROC
//Clear the content ID of the last corona I was in
PROC CLEAR_LAST_CORONA_CONTENT_ID()
	PRINTLN("CLEAR_LAST_CORONA_CONTENT_ID")
	g_sLastCoronaDetails.tl23LastCoronaContentID = ""
ENDPROC
//Get the content ID of the last corona I was in
FUNC TEXT_LABEL_23 GET_LAST_CORONA_CONTENT_ID()
	RETURN (g_sLastCoronaDetails.tl23LastCoronaContentID)
ENDFUNC

//Back up the corona menu selection so it can be used again
PROC BACK_UP_CORONA_MENU_SELECTION()
	#IF IS_DEBUG_BUILD
	PRINTLN("[CORONA] - BACK_UP_CORONA_MENU_SELECTION")
	DEBUG_PRINTCALLSTACK()
	#ENDIF	
	g_sCoronaOptionsBackUp = g_sTransitionSessionOptions	
ENDPROC

//Clear the back up menu selection
PROC CLEAR_BACKED_UP_CORONA_MENU_SELECTION()
	#IF IS_DEBUG_BUILD
	PRINTLN("[CORONA] - CLEAR_BACKED_UP_CORONA_MENU_SELECTION")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	FMMC_SELECTED_ITEMS g_sCoronaOptionsBackUpTemp
	g_sCoronaOptionsBackUp = g_sCoronaOptionsBackUpTemp
ENDPROC

/// PURPOSE: Return the maximum players for a mission type    
FUNC INT GET_MISSION_TYPE_MAX(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)
	RETURN sLaunchMissionDetails.iMaxParticipants
ENDFUNC

/// PURPOSE: Return the minimum players for a mission type
FUNC INT GET_MISSION_TYPE_MIN(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)
	RETURN sLaunchMissionDetails.iMinPlayers
ENDFUNC

/// PURPOSE: Check if the job is one player only  
FUNC BOOL IS_JOB_TYPE_ONE_PLAYER_ONLY(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)
	RETURN GET_MISSION_TYPE_MAX(sLaunchMissionDetails) <= 1
ENDFUNC



























