//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////        Deals with the Corona menus in FM	    //////////////////////////////
//////////////////////////////           	   James Adwick          	    //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

//USING "rage_builtins.sch"
//USING "globals.sch"
//USING "help_at_location.sch"
USING "net_common_functions.sch"
//USING "net_vote_at_blip.sch" 
//USING "net_mission_details_hud.sch"
//USING "net_betting_common.sch"
//USING "Transition_Invites.sch"
//USING "net_corona_position.sch"
//USING "Screen_placements.sch"

USING "FMMC_Launcher_Menu.sch"
USING "MP_Scaleform_Functions_XML.sch"
USING "net_mission_control.sch"
USING "net_transition_sessions.sch"
USING "cheat_handler.sch"
USING "FMMC_Corona_Header.sch"
USING "net_simple_interior.sch"
USING "net_mod_value.sch"


// Controller Constants
CONST_INT FM_MISSION_INTRO_SCREEN_SET_UP 					0
CONST_INT FM_MISSION_INTRO_SCREEN_WAIT_FOR_FRONTEND			1
CONST_INT FM_MISSION_INTRO_SCREEN_POPULATE_DATA				2
CONST_INT FM_MISSION_INTRO_SCREEN_MAINTAIN					3
CONST_INT FM_MISSION_INTRO_SCREEN_SOLO_LAUNCH				4
CONST_INT FM_MISSION_INTRO_SCREEN_EXITING					5
CONST_INT FM_MISSION_INTRO_SCREEN_TUTORIAL_RACE				6
CONST_INT FM_MISSION_INTRO_SCREEN_CONFIRMATION				7
CONST_INT FM_MISSION_INTRO_SCREEN_CHALLENGE_WARNING_SCREEN	8
CONST_INT FM_MISSION_INTRO_SCREEN_FULL_SCREEN_MAP			9

CONST_INT MAX_FM_MISSION_INTRO_LINES 					5

CONST_INT FM_MISSION_LOBBY_SCREEN_SET_UP				0
CONST_INT FM_MISSION_LOBBY_SCREEN_WAIT_FOR_FRONTEND		1
CONST_INT FM_MISSION_LOBBY_SCREEN_MAINTAIN				2
CONST_INT FM_MISSION_LOBBY_SCREEN_MAINTAIN_SUB			3
CONST_INT FM_MISSION_LOBBY_SCREEN_SOLO_LAUNCH			4
CONST_INT FM_MISSION_LOBBY_SCREEN_PROGRESS_WARNING		5
CONST_INT FM_MISSION_LOBBY_SCREEN_EXITING				6
CONST_INT FM_MISSION_LOBBY_SCREEN_CUSTOM_MENU			7
CONST_INT FM_MISSION_LOBBY_SCREEN_HIDDEN				8

CONST_INT FM_MISSION_INVITE_SCREEN_SET_UP				0
CONST_INT FM_MISSION_INVITE_SCREEN_WAIT_FOR_FRONTEND	1
CONST_INT FM_MISSION_INVITE_SCREEN_MAINTAIN				2

CONST_INT FM_MISSION_LOAD_IN_CORONA_SCENE_SET_UP		0
CONST_INT FM_MISSION_LOAD_IN_CORONA_SCENE_MAINTAIN		1
CONST_INT FM_MISSION_LOAD_IN_CORONA_SCENE_COMPLETE		2

CONST_INT FM_MISSION_IN_CORONA_SET_UP					0
CONST_INT FM_MISSION_IN_CORONA_PREPARE					1
CONST_INT FM_MISSION_IN_CORONA_SETUP_CAMERA				2
CONST_INT FM_MISSION_IN_CORONA_WAIT_FOR_FRONTEND		3
CONST_INT FM_MISSION_IN_CORONA_SCREEN_MAINTAIN			4
CONST_INT FM_MISSION_IN_CORONA_SCREEN_EXITING			5
CONST_INT FM_MISSION_IN_CORONA_SCREEN_LOAD				6
CONST_INT FM_MISSION_IN_CORONA_VIEW_PLANNING			7

CONST_INT FM_MISSION_BETTING_SCREEN_SET_UP				0
CONST_INT FM_MISSION_BETTING_SCREEN_LOAD_SCENE			1
CONST_INT FM_MISSION_BETTING_SCREEN_WAIT_FOR_FRONTEND	2
CONST_INT FM_MISSION_BETTING_SCREEN_MAINTAIN			3
CONST_INT FM_MISSION_BETTING_SCREEN_EXITING				4
CONST_INT FM_MISSION_BETTING_SCREEN_CONFIRMATION		5

// Summary bits, to indicate which options we should display under main menu
CONST_INT ciCORONA_TEAM_DM_SUMMARY_OPT_0	0
CONST_INT ciCORONA_TEAM_DM_SUMMARY_OPT_1	1
CONST_INT ciCORONA_TEAM_DM_SUMMARY_OPT_2	2
CONST_INT ciCORONA_TEAM_DM_SUMMARY_OPT_3	3
CONST_INT ciCORONA_TEAM_DM_SUMMARY_OPT_4	4

CONST_INT ciCORONA_DEFAULT_TEAM_ROLE		0

// Need to know how we want to handle Lamar
CONST_INT CORONA_LAMAR_RANK							99

CONST_INT CORONA_MAX_PLAYER_ROWS					16

CONST_INT CORONA_PLAYER_LOOK_AT_INIT_TIME 			400

CONST_FLOAT CORONA_LOAD_SCENE_RADIUS				60.0
CONST_INT CORONA_HIGH_DETAIL_VEHICLE_TIMEOUT 		3000

CONST_INT CORONA_AUTOSTART_TIME						120
CONST_INT CORONA_AUTOSTART_TIME_10					10

CONST_INT CORONA_LOAD_SCENE_TIMEOUT 				15000

CONST_INT CORONA_CREW_EMBLEM_TIMEOUT				3000

CONST_INT CORONA_PLAYER_CARD_TITLE_REFRESH 			2000
CONST_INT CORONA_PLAYER_END_OF_JOB_TIMER 			3000

CONST_INT CORONA_DISPLAY_INVITE_TIME				4000

CONST_INT CORONA_PED_OUTFIT_TIMEOUT					8000
CONST_INT CORONA_OUTFIT_SHADOW_CASTING_TIME			600

CONST_INT CORONA_VEHICLE_SCENE_LOAD_TIMEOUT			10000

CONST_INT CORONA_PLAYLIST_STATUS_CHECK_TIMER		10000

CONST_INT CORONA_PLANNING_BOARD_TIMEOUT				15000

CONST_INT CORONA_PLAYER_OUTFIT_NONE					-1
CONST_INT CORONA_TOTAL_OUTFIT_BS					2

CONST_INT CORONA_PLAYER_MASK_NONE					-1
CONST_INT CORONA_TOTAL_MASK_BS						1

CONST_INT CORONA_HEIST_FLOW_BREAK_TIMER				30000

CONST_INT CORONA_WARNING_MESSAGE_TIME_OUT			30000

// Time out when waiting for the cash transactions
CONST_INT CORONA_CASH_TRANSACTION_TIMEOUT			10000

CONST_FLOAT CORONA_BOAT_WATER_THRESHOLD 			0.5

// Clothing setup options
CONST_INT ciCORONA_CLOTHING_SETUP_OPTION_0			0		// Player Owned
CONST_INT ciCORONA_CLOTHING_SETUP_OPTION_1			1		// Recommended
//CONST_INT ciCORONA_CLOTHING_SETUP_OPTION_2			2		// Themed (vs)
//CONST_INT ciCORONA_CLOTHING_SETUP_OPTION_3			3		// Halloween
//CONST_INT ciCORONA_CLOTHING_SETUP_OPTION_ENTOURAGE	4		// Entourage (special case in corona)

// Bits for creating the ped in recently joined character
CONST_INT ciCORONA_PED_GENERATED 	0
CONST_INT ciCORONA_PED_CREW_GRAB	1
CONST_INT ciCORONA_PED_CREW_TXD		2

// Camera states
ENUM CORONA_CAMERA_MODE
	CORONA_CAMERA_MODE_INDIVIDUAL = 0,
	CORONA_CAMERA_MODE_TEAM,
	CORONA_CAMERA_MODE_STANDARD_CAR,
	CORONA_CAMERA_MODE_BIKE,
	CORONA_CAMERA_MODE_WATER,
	CORONA_CAMERA_MODE_AIR,
	CORONA_CAMERA_MODE_AIR_HELI,
	CORONA_CAMERA_MODE_LARGE_VEHICLE,
	CORONA_CAMERA_MODE_MINIGAME,
	CORONA_CAMERA_MODE_SYNC_SCENE
ENDENUM


ENUM CORONA_VEHICLE_CLASS_STATE
	VEHICLE_CLASS_LOADING = 0,
	VEHICLE_CLASS_LOADED
ENDENUM

ENUM CORONA_VEHICLE_STATE
	VEHICLE_NOT_DISPLAYED = 0,
	VEHICLE_DISPLAY_VEHICLE,
	VEHICLE_HIGH_DETAIL,
	VEHICLE_DISPLAYING
ENDENUM

ENUM CORONA_SUMMARY_CARD_STATE
	CORONA_SUMMARY_CARD_PLAYER = 0,
	CORONA_SUMMARY_CARD_VEHICLE,
	CORONA_SUMMARY_CARD_BETTING,
	CORONA_SUMMARY_CARD_STATS,
	CORONA_SUMMARY_CARD_MAP,
	CORONA_SUMMARY_CARD_HIDE
ENDENUM

ENUM CORONA_PLAYER_VOICE_STATE
	CORONA_PLAYER_VOICE_NONE = 0,
	CORONA_PLAYER_VOICE_TALKING,
	CORONA_PLAYER_VOICE_MUTE
ENDENUM

ENUM CORONA_PLAYER_LOOK_AT_STATE
	CORONA_PLAYER_LOOK_AT_INIT = 0,
	CORONA_PLAYER_LOOK_AT_CAMERA,
	CORONA_PLAYER_LOOK_AT_ACTIVE,
	CORONA_PLAYER_LOOK_AT_INACTIVE,
	CORONA_PLAYER_LOOK_AT_ANIMATION
ENDENUM

ENUM CORONA_CURRENT_WEAPON_TYPE
	CORONA_WEAPON_STANDARD,
	CORONA_WEAPON_DLC
ENDENUM

ENUM GENERIC_JOB_DIALOGUE_STAGES
	eGJ_DIALOGUE_STAGE_IDLE = -1,
	eGJ_DIALOGUE_STAGE_SELECT_DIALOGUE,
	eGJ_DIALOGUE_STAGE_PREPARE,
	eGJ_DIALOGUE_STAGE_WAIT_FOR_TRIGGER,
	eGJ_DIALOGUE_STAGE_DIALOGUE_ONE_SHOT,
	eGJ_DIALOGUE_STAGE_DIALOGUE_LOOPED,
	eGJ_DIALOGUE_STAGE_DIALOGUE_END
ENDENUM

ENUM GENERIC_JOB_DIALOGUE
	// Keep this at the start
	eGJD_UNSET = -1,
	// --

	eGJD_ARENA_ANNOUNCER_GENERAL_0,
	eGJD_ARENA_ANNOUNCER_GENERAL_1,
	eGJD_ARENA_ANNOUNCER_GENERAL_2,
	eGJD_ARENA_ANNOUNCER_GENERAL_3,
	eGJD_ARENA_ANNOUNCER_GENERAL_4,
	
	eGJD_ARENA_ANNOUNCER_BUZZER_BEATER_0,
	eGJD_ARENA_ANNOUNCER_BUZZER_BEATER_1,
	eGJD_ARENA_ANNOUNCER_BUZZER_BEATER_2,
	
	eGJD_ARENA_ANNOUNCER_CARNAGE_LMS_0,
	eGJD_ARENA_ANNOUNCER_CARNAGE_LMS_1,
	eGJD_ARENA_ANNOUNCER_CARNAGE_LMS_2,
	eGJD_ARENA_ANNOUNCER_CARNAGE_LTS_0,
	eGJD_ARENA_ANNOUNCER_CARNAGE_LTS_1,
	eGJD_ARENA_ANNOUNCER_CARNAGE_LTS_2,
	
	eGJD_ARENA_ANNOUNCER_FLAG_WAR_0,
	eGJD_ARENA_ANNOUNCER_FLAG_WAR_1,
	eGJD_ARENA_ANNOUNCER_FLAG_WAR_2,
	
	eGJD_ARENA_ANNOUNCER_BOMB_BALL_0,
	eGJD_ARENA_ANNOUNCER_BOMB_BALL_1,
	eGJD_ARENA_ANNOUNCER_BOMB_BALL_2,
	
	eGJD_ARENA_ANNOUNCER_GAMES_MASTERS_0,
	eGJD_ARENA_ANNOUNCER_GAMES_MASTERS_1,
	eGJD_ARENA_ANNOUNCER_GAMES_MASTERS_2,
	
	eGJD_ARENA_ANNOUNCER_HERE_COME_THE_MONSTERS_0,
	eGJD_ARENA_ANNOUNCER_HERE_COME_THE_MONSTERS_1,
	eGJD_ARENA_ANNOUNCER_HERE_COME_THE_MONSTERS_2,
	
	eGJD_ARENA_ANNOUNCER_HOT_BOMB_LMS_0,
	eGJD_ARENA_ANNOUNCER_HOT_BOMB_LMS_1,
	eGJD_ARENA_ANNOUNCER_HOT_BOMB_LMS_2,
	eGJD_ARENA_ANNOUNCER_HOT_BOMB_LTS_0,
	eGJD_ARENA_ANNOUNCER_HOT_BOMB_LTS_1,
	eGJD_ARENA_ANNOUNCER_HOT_BOMB_LTS_2,
	
	eGJD_ARENA_ANNOUNCER_TAG_TEAM_0,
	eGJD_ARENA_ANNOUNCER_TAG_TEAM_1,
	eGJD_ARENA_ANNOUNCER_TAG_TEAM_2,
	
	eGJD_ARENA_ANNOUNCER_WRECK_IT_0,
	eGJD_ARENA_ANNOUNCER_WRECK_IT_1,
	eGJD_ARENA_ANNOUNCER_WRECK_IT_2,
	
	// Keep this at the end
	eGJD_MAX
	// --
ENDENUM

CONST_INT CORONA_PLAYER_TEAM_POS_INSIDE		0
CONST_INT CORONA_PLAYER_TEAM_POS_AT_EDGE	1

// Sudden death options for missions
CONST_INT CORONA_MISSION_SUDDEN_DEATH_OPT_1		0
CONST_INT CORONA_MISSION_SUDDEN_DEATH_OPT_2		1
CONST_INT CORONA_MISSION_SUDDEN_DEATH_OPT_MAX	1

// Pickup option for missions
CONST_INT CORONA_MISSION_PICKUPS_NORMAL		0
CONST_INT CORONA_MISSION_PICKUPS_RANDOM		1
CONST_INT CORONA_MISSION_PICKUPS_OPT_MAX	1

STRUCT CORONA_PLAYER_DETAILS
	PLAYER_INDEX playerID											// Player index
	INT iTeam = -1													// This will dictate who they are shown with in corona (bets etc)
	INT iTeamMembers												// Total number of members in the team
	INT iTeamRow													// Which row from the front are we placed on
	INT iTeamPosIndex												// A fixed index of position within team (ie. 2nd member of 4). Used for placement in bespoke coronas
	INT iSlotAssigned
	INT iTeamPos				= CORONA_PLAYER_TEAM_POS_INSIDE		// Is the player at the edge of the corona team row
	FLOAT fNameX = -1.0												// X position of gamertag
	FLOAT fNameY = -1.0												// Y position of gamertag
	INT	iVoiceChat				= ciTRANSITION_SESSIONS_VOICE_CHAT_NULL
	BOOL bPlaced													// Have we placed them in corona already
	CORONA_PLAYER_VOICE_STATE cVoiceState							// Are they talking or muted
ENDSTRUCT

STRUCT CORONA_CLIENT_SYNC_DATA
	INT iClientSyncID
	BOOL bVoted
ENDSTRUCT

CONST_INT CORONA_ORDERED_VEHICLE_IS_DLC		0

STRUCT CORONA_ORDERED_VEHICLE
	INT iVehicleIndex
	INT iVehicleInfoBS
ENDSTRUCT

CONST_INT ciCORONA_CUSTOM_VEHICLE_ON		0
CONST_INT ciCORONA_CUSTOM_VEHICLE_OFF		1

CONST_INT CORONA_PLAYER_PED_TAG_SETUP		0
CONST_INT CORONA_PLAYER_PED_TAG_INIT		1
CONST_INT CORONA_PLAYER_PED_TAG_COMPLETE	2

CONST_INT ciCORONA_AGGREGATE_RACE_POSITION_OFF 	0
CONST_INT ciCORONA_AGGREGATE_RACE_POSITION_ON	1

#IF FEATURE_GEN9_EXCLUSIVE
CONST_INT ciCORONA_HSW_MODS_ENABLED				0
CONST_INT ciCORONA_HSW_MODS_DISABLED			1
#ENDIF

// 
CONST_INT ciCORONA_BETTING_PLAYER				0
CONST_INT ciCORONA_BETTING_BET					1
CONST_INT ciCORONA_BETTING_ANIM					2
CONST_INT ciCORONA_BETTING_WEAPON				3
CONST_INT ciCORONA_BETTING_PURCHASE_AMMO		4
CONST_INT ciCORONA_BETTING_PURCHASE_ALL_AMMO 	5
CONST_INT ciCORONA_BETTING_MY_CASH				6
CONST_INT ciCORONA_BETTING_TOTAL_BETS			7
CONST_INT ciCORONA_BETTING_ODDS					8
CONST_INT ciCORONA_BETTING_BETS_ON_PLY			9
CONST_INT ciCORONA_BETTING_BETS_ON_YOU			10
CONST_INT ciCORONA_BETTING_AMMO_COST			11
CONST_INT ciCORONA_BETTING_PURCHASE_ARMOR		12
CONST_INT ciCORONA_BETTING_CASH_OR_AMMO			13
CONST_INT ciCORONA_BETTING_CHUTE				14
CONST_INT ciCORONA_BETTING_CUSTOM_VEHICLE		15

CONST_INT ciGENERIC_JOB_BITSET_PURCHASE_MODS_ALERT_SCREEN_SETUP			0
CONST_INT ciGENERIC_JOB_BITSET_PROCEED_WITH_PURCHASE					1
CONST_INT ciGENERIC_JOB_BITSET_ALERT_SCREEN_CLEARED_THIS_FRAME			2

CONST_INT ciCORONA_NULL		0
CONST_INT ciCORONA_INTRO	1
CONST_INT ciCORONA_LOBBY	2
CONST_INT ciMAX_CORONA_HELMETS 26
STRUCT CORONA_PLAYER_PED_TAGS
	INT iPlayerCoronaTagId = -1
	INT iPlayerCoronaTagBitset = 0
ENDSTRUCT

STRUCT CORNA_COMP_DATA_EXTRA
	PED_COMP_TYPE_ENUM eType = COMP_TYPE_BERD  
	PED_COMP_NAME_ENUM eName = BERD_FMM_0_0
	INT iDLCNameHash
ENDSTRUCT

CONST_INT ciFIXER_SHORT_TRIPS_MENU_MAIN											0
CONST_INT ciFIXER_SHORT_TRIPS_MENU_INVITE										1
CONST_INT ciFIXER_SHORT_TRIPS_MENU_INVITE_PLAYER								2

CONST_INT ciFIXER_SHORT_TRIPS_MAIN_MENU_ITEM_ENTER								0
CONST_INT ciFIXER_SHORT_TRIPS_MAIN_MENU_ITEM_INVITE								1

CONST_INT ciFIXER_SHORT_TRIPS_INVITE_MENU_ITEM_ORG								0
CONST_INT ciFIXER_SHORT_TRIPS_INVITE_MENU_ITEM_NEARBY							1
CONST_INT ciFIXER_SHORT_TRIPS_INVITE_MENU_ITEM_PLAYER							2

CONST_INT ciFIXER_SHORT_TRIPS_MENU_INVALID										-1

STRUCT FIXER_SHORT_TRIPS_MENU_STRUCT
	TIME_DATATYPE tInputControlTimer
	BOOL bCancelled, bAccepted 
	BOOL bSetConfirmTooltip = FALSE
	BOOL bRedraw
	INT iCurrentMenu = ciFIXER_SHORT_TRIPS_MENU_MAIN
	INT iNewMenuItem
	PLAYER_INDEX playerInviteList[NUM_NETWORK_PLAYERS]
	INT iNumPlayers
ENDSTRUCT

// ********************************
// * MAIN STRUCT FOR CORONA LOBBY *
// ********************************

// MAIN STRUCT FOR CORONA FLOW (DATA HELD FOR SYNCS AND LAUNCHING OF ACTIVITIES)
// NOTE: These should be reset manually within: PROC CLEANUP_CORONA_STRUCT(CORONA_MENU_DATA &coronaMenuData)
STRUCT CORONA_MENU_DATA

	// Main data handling
	BOOL 						bDefaultHostData 					= TRUE		// If player needs to default the data
	TIME_DATATYPE 				lobbyOptionTimer								// Timer used to track input
	CORONA_PLAYER_DETAILS 		lobbyPlayers[NUM_NETWORK_PLAYERS]				// Array of whos in the corona (shown on lobby screen)
	PLAYER_INDEX				joiningPlayers[NUM_NETWORK_PLAYERS]
	INT							iTotalPlayersJoinedTeamDMScreen		= -1
	INT							iTotalPlayersInCorona				= 0
	INT							iTotalFullyJoined					= 0
	INT							iTotalPlayersReady					= 0
	INT							iMaxPlayers							= 0
	INT							iCoronaHost
	GAMER_HANDLE				coronaHostHandle
	GAMER_HANDLE				recentPlayer
	INT							iPreviousCoronaHostIndex			= 0
	
	INT							iUnbalancedCoronaTeam				= 0
	
	TEXT_LABEL_63				tlRecentPedCrewTxd
	INT							iRecentPlayerCrewRank
	TEXT_LABEL_15				tlRecentPlayerCrewTag
	INT							iRecentPlayerCrewColRed
	INT							iRecentPlayerCrewColGreen
	INT							iRecentPlayerCrewColBlue
	INT							iRecentPlayerBitSet
	
	INT							iRecentPedLoad										// Recently player load stage for crew info
	INT							iRecentPedCrewEmblemState							// State of grabbing a crew emblem
	INT							iRecentPedCrewID									// The crew id of the player
	BOOL						bRecentPedInCrew									// Is player in a crew
	INT							iRecentPedFrameDelay								// Delay until we pass this ped to code
	PED_INDEX					recentPed											// Ped to display in the menus
	BOOL 						bcreatedrecentPed
	FLOAT 						fBackupGroundZ
	
	// Corona vehicle
	INT							iTotalCustomVehicles				= 0				// Total number of custom vehicles for class

	VEHICLE_INDEX 				vehDisplayedVehicle								    // Players currently chosen vehicle (Race only)
	VEHICLE_INDEX				vehLoadingVehicle									// Player vehicle that is loading
	OBJECT_INDEX				vehLightRig											// Lighting Rig for corona vehicle
	CORONA_VEHICLE_STATE 		vehDisplayedState									// State of current vehicle displaying (Race only
	CORONA_VEHICLE_CLASS_STATE	vehClassState										// State of current vehicle class loading (Race only)
	//BOOL						bCustomEmblem										// Custom vehicle emblem loaded
 	INT 						iVehClass							= -1			// Record of vehicle class
	BOOL						bVehHasModKit						= FALSE			// If current vehicle has mod kits available
 	MOD_COLOR_TYPE				iSelectedColourType
	INT							iSelectedColourIndex
	INT							iSelectedLivery
	PLAYER_INDEX				piRacePartner
	VECTOR						vVehicleVector
	VECTOR						vCachedPlaneRotation				
	BOOL						bHasCachedPlaneRotation				= FALSE
	CORONA_ORDERED_VEHICLE		orderedCoronaVehicles[ciRC_MAX_VEHICLES_IN_CORONA]		// An ordered list of our vehicles (index used in net_race_common_functions.sch)
	INT							iVehicleCoronaIndex					= 0
	
	// Player animations
	INT 						iPlayerAnimType									// Type of animation player is playing (player / crew)
	INT 						iPlayerAnim										// Which animation currently showing / playing
	INT							iPlayerAnimTypeToPlay							// Type of animation to play (due to special ability)
	INT							iPlayerAnimToPlay								// Animation to play (due to special ability)
	
	// Corona Weapon
	INT							iWeaponSlot										// Global WeaponSlot
	CORONA_CURRENT_WEAPON_TYPE	coronaWeaponType								// Current type of weapons we are looping through
	INT							iStandardWeaponSlot								// Current weapon slot we are looping through for standard weapons
	WEAPON_GROUP				coronaWeaponGroup								// Current weapon group we are looking for
	WEAPON_TYPE					coronaStandardWeapons[NUM_WEAPONSLOTS]			// Grouped array of our weapons
	INT							iTotalCostToFillAmmo							// This is the total cost for filling all ammo
	
	INT							iCoronaChute									// Corona chute index the player is choosing

 	// Menu processing / interaction
	INT 						iCurrentSelection 					= 0			// Current selection on menu
	INT							iPreviousSelection					= 0			// Previous selection on menu
	INT							iCurrentInviteSubSelection			= 0			// Sub invite selection on lobby screen
	INT 						iCurrentSyncID 						= 0			// Sync ID of host options across those in lobby
	INT 						iBS_PlayersInCorona								// Bitset of player index who is in corona
	INT							iBS_PlayersJoiningCorona						
	INT 						iLobbyPlayerToCheck								// Staggered loop
	INT							iStaggeredJoiningCount				= 0
	INT 						iAutofillOption						= -1
	INT							iAutofillOptionsBitSet				= 0
	INT							iCoronaTimerSounds					= -1
	INT							iPlayerHeadshotsLoaded				= 0
	INT							iPlayerHeadshotsLoop				= 0
	INT							iStaggeredPlayerCrewInfo			= 0
	
	INT							iGamerCrewTagState
	GAMER_HANDLE				aGamerHandleForCrew
	TEXT_LABEL_15 				tlGamerCrewTag
	
	BESTRACETIMEBOX 			BoxType								= BESTRACETIMEBOX_NONE
	
	// Timers
	TIME_DATATYPE 				coronaSummaryTimer								// Automatic timer for updating display
	TIME_DATATYPE				coronaGeneralTimer								// Generic timer used for tutorial / cash / invites
	TIME_DATATYPE				coronaCashTimer									// Cash timer used for tutorial / cash / invites
	TIME_DATATYPE				coronaWaveFXTimer								// Timer for applying disturbance to waves
	TIME_DATATYPE				coronaHelpTextTimer								// Timer for flashing help text
	TIME_DATATYPE				coronaGreyOutOptionTimer						// Timer for greying out an option on the menu
	TIME_DATATYPE				coronaRelRequestTimer							// Timer for requesting relationships
	TIME_DATATYPE				coronaEndOfJobFlushTimer						// Timer to clear Job to Job player list if they have left
	
	TIME_DATATYPE				coronaLaunchDelayTimer							// Timer to delay host actually launching the transition session
	TIME_DATATYPE				coronaLaunchStuckTimer							// Timer to for the host to boot players if they get stuck
	TIME_DATATYPE				coronaLaunchStuckTimerLocalPlayer				// Timer to for the local player if they get stuck
	TIME_DATATYPE				coronaValidateCrewTimer							// TImer to validate if the crews in a H2H are valid
	TIME_DATATYPE				coronaCoronaCrewEmblemTimer						// Timer to grab the crew emblem, if this fails - move on
	TIME_DATATYPE				coronaOutfitLoadTimer							// Timeout for waiting on outfits / planning board etc.
	TIME_DATATYPE				coronaGeneralTimer2								// A second generic timer (currently used for ready pulsing)
	TIME_DATATYPE				coronaLaunchStuckTimerLocalPlayerBalance		// Timer to for the local player if they get stuck in the CORONA_STATUS_BALANCE_AND_LOAD
	TIME_DATATYPE				coronaClothingGeneralTimer						// Timer to know when to notify clients of clothing change
	TIME_DATATYPE				coronaHeistMatchmakingTimer						// When expires (~30s) matchmaking will be force open unless we have already manually closed it
	
	
	BOOL						bFinishedBootingPlayers
	BOOL						bFinishedBootingPlayersLaunch
	BOOL						bCappedTeamCount
	INT							iCoronaRelRequestCounter

	TEXT_LABEL_15				tlFlashHelpText
	TEXT_LABEL_15				tlWarningText									// Warning text when trying to progress
	INT							iFlashHelpTextState
	INT							iFlashHelpTextOptInt

	INT 						iTeamUnbalanced
	BOOL						bCoronaTeamOverMax
	INT							iForcedLaunchSec

	INT							iCoronaAutoStartTime
	INT							iCoroanAutoFillLoopCount			= 0
	INT							iPreviousAutoStartTime

	INT							iPreviousInviteSelection			= -1		// Previous invite all - allow you to send different invites
	INT							iDevSpectatorIndex					= 0			// The index of the player the dev spectator is currently spectating
	
	// Visual updates
	INT					 		iCoronaClientSyncCheck				= 0			// Counter of who to check next for summary card
	INT							iCoronaClientVoiceChatCheck			= 0
	CORONA_CLIENT_SYNC_DATA		coronaClientSyncData[NUM_NETWORK_PLAYERS]		// Local knowledge of sync IDs of all client
	CORONA_SUMMARY_CARD_STATE 	coronaSummaryCardState							// State of summary card in top right
	
	g_structASFMHeistMP			coronaHeistDetails								// Struct holding data about heists

	// Corona Ped placement
	//PED_INDEX 					piPlayersPed[NUM_NETWORK_PLAYERS]				// Array of players ped clones we move around corona screens **THIS HAS BEEN MOVED INTO GLOBAL
	CORONA_PLAYER_PED_TAGS		playersPedsTags[NUM_NETWORK_PLAYERS]			// Overhead tag IDs that we use to display overheads
	CAMERA_INDEX				ciCoronaCam										// Camera used to point to player in corona
	CORONA_CAMERA_MODE			coronaCamMode									// What positioning we should be using for this corona
	FLOAT						fCameraRotationX								// Keep track of our camera rotation (x-axis)
	FLOAT						fCameraRotationY								// Keep track of our camera rotation (y-axis)
	PLAYER_INDEX				piViewedPlayer									// Viewed player outside team coronas
	INT							iLocalPlayerSlot								// Where we are in the corona
	INT							iCoronaSlotTarget								// Which slot in corona are we aiming at
	INT							iCoronaSlotsActive								// Which slots are currently active in corona
	INT							iCoronaPedIndex									// Index players are added if late joining
	INT							iCoronaSlotLayout[NUM_NETWORK_PLAYERS]			// Array of which team is held in which slot
	INT							iCoronaPlayerTeamSyncCheck			= 0
	INT							iCoronaPlayerOverheadToCheck		= 0			// Counter to check ped overheads
	INT							iCoronaStatPlayer					= -1
	
	VECTOR						vCurrentCamVector										// Peds to cycle through on betting screen
	VECTOR						vTargetCamVector										// Ped to move to on betting screen
	FLOAT						fCurrentCamHeading
	FLOAT 						fTargetCamHeading
	VECTOR						vCurrentCamRotation
	VECTOR						vTargetCamRotation
	INT							iMovingLeft
	
	// Tutorial Variables
	PED_INDEX					tutorialPedIndex								// Tutorial ped index (Lamar)
	PEDHEADSHOT_ID				tutorialPedHeadshot								// Tutorial peds headshot (Lamar)
	INT							iTutorialPedSlot								// Tutorial ped slot
	structPedsForConversation	coronaPedConvStruct								// Tutorial ped conversation struct
	INT							iTutorialConvo
	INT 						iTutorialCameraProg								// Tutorial corona camera update
	INT 						iTutorialHelpIndex
	
	// Animated help text
	INT							iBS_CoronaAnimTextActive			= 0			// Bitset to indicate which animated help text is being shown
	TIME_DATATYPE				coronaAnimHelpTimer								// Timer to show how long animated text is displayed
	
	// Corona sync scene
	FLOAT						fCoronaSyncSceneRot
	
	// Heist variables
	INT							allCoronaOutfits[CORONA_TOTAL_OUTFIT_BS]		// Player outfits available for heist
	PLAYER_INDEX				piOutfitPlayer									// Displays the player name for the outfit
	INT							allCoronaMasks[CORONA_TOTAL_MASK_BS]			// Player masks available for heist
	BOOL						bMasksAvailable[FMMC_MAX_TEAMS]
	OBJECT_INDEX				oPistol											// For pistol specific heist corona intro anim
	OBJECT_INDEX				oAmmoClip										// For pistol specific heist corona intro anim
	PED_INDEX 					pTempPed										// For pistol specific heist corona intro anim
	INT 						iSyncedSceneID									// For pistol specific heist corona intro anim
	INT 						iLoopingSyncedScene								// For pistol specific heist corona loop anim
	INT							iCoronaSummaryBitset							// Summary bitset for team selection rows
	
	INT							iPlayerOutfitLoop					= 0
	
	// Keeps track of status we are in when transitioning
	INT							iMenuToRefresh
	
	// Contact mission shapetest
	SHAPETEST_INDEX				coronaShapeTest
	
	// Unique IDs
	INT CORONA_COLUMN_ONE_ID													// Corona IDS
	INT CORONA_COLUMN_TWO_ID
	
	//Selection Camera vars
	INT 						iSelectionCameraSwitchStage						//Stage of selection camera.
	INT 						iSelectionCameraTimer				= -1		//Timer for selection camera.
	BOOL 						bFirstTransitionFlag
	
	// Ped look at stuff
	INT 						iLookAtPedBeingProcessed
	CORONA_PLAYER_LOOK_AT_STATE eCoronaPedLookAtState[NUM_NETWORK_PLAYERS]
	TIME_DATATYPE 				nextPedLookAt[NUM_NETWORK_PLAYERS]
	
	// Larger session handling (32+ players)
	INT							iCoronaPlayerPage								// Page that we are viewing for the players
	
	// Mission Vehicle indexes
	INT iMissionVehicleClass[FMMC_MAX_TEAMS]			// Vehicle class selected for each team
	INT iMissionTeamRole[FMMC_MAX_TEAMS]				// The chosen role for the mission team
	INT iMissionVehicle[FMMC_MAX_TEAMS]					// Vehicle chosen for each team
	BOOL bInvalidRoles									// Bool to indicate players roles are invalid (VS or Heists)
	
	//Vehicle camera vars
	FLOAT 						desiredXRot
	FLOAT 						desiredYRot
	
	//Betting camera vars. Note for James: these could use the same variables as the vehicle camera if there is a suitable palce to reset them)
	FLOAT 						desiredXRotPed = 0.0
	FLOAT 						desiredYRotPed = 0.0
	FLOAT 						fCameraRotationXPed = 0.0//1
	FLOAT 						fCameraRotationYPed = 0.0//1	//Make this non-zero to kick off the camera pointing
	FLOAT 						fCamMovementSpeed = 0.0
	FLOAT 						fCamReturnSpeed = 0.0
	FLOAT 						fCamMovementSpeedY = 0.0
	FLOAT 						fCamReturnSpeedY = 0.0
	FLOAT						fCamPointAtOffset
	VECTOR 						vTargetPedCoords
	FLOAT 						fTargetPedHeading
	BOOL 						bPedCoordsGrabbed
	VECTOR 						vCameraPointOffset	//Track the point offset adjusting wether or not status bar is displayed.
	FLOAT 						fXOffsetLerp
	FLOAT 						fYOffsetLerp
	
	FLOAT 						fCoronaCurrentZoom
	FLOAT 						fCoronaZoomLerp
	
	// Cash Transactions in corona
	INT							iCashTransactionIndex = -1
	
	//Betting screen fancy transition stuff
	VECTOR vGrabbedTargetCoords
	FLOAT fGrabbedTargetHeading
	
	BOOL bPlayerQuitting
	
	INT iPlayerTitlesAlpha		= 100
	FLOAT fPointOffsetLerp		= 0.0
	
	BOOL bSellFullAmmo
	
	INT  iNumMenuItems			= 0 // Number of menu items in the list so we can keep track of the size of the menu for mouse detection.

	INT iMaxTeamDifferenceAmount

	//Ped Idle Anims
	SCRIPT_TIMER iIdleAnimTimer[NUM_NETWORK_PLAYERS]
	INT iIdleAnimDelay[NUM_NETWORK_PLAYERS]
	INT iIdleAnimStage[NUM_NETWORK_PLAYERS]
	INT iIdleAnimBase[NUM_NETWORK_PLAYERS]
	INT iPlayerRaceOutfit
	INT iPlayerRaceHelmet
	INT iLastPlayerRaceHelmet = -1
	#IF IS_DEBUG_BUILD
	INT iAutofillCounter
	#ENDIF	
	INT iHelmetTotalCount
	CORNA_COMP_DATA_EXTRA HelmetsData[118]
	#IF FEATURE_TUNER
	INT iPlayerRaceMask
	#ENDIF
	
	BOOL bSelectedPilot = FALSE
	
	BOOL bAggregateWasPreviouslyAvailable = FALSE
	BOOl bAggregateCachedValue = FALSE
	
	INT iClientOptionVehicleSeat = 0
	
	INT iGangOpsFinaleCoronaState = 0
	SCRIPT_TIMER stGangOpsFinaleBailTimer
	
	#IF FEATURE_CASINO_HEIST
	INT iCasinoHeistFinaleCoronaState = 0
	SCRIPT_TIMER stCasinoHeistFinaleBailTimer
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	INT iGenericHeistFinaleCoronaState = 0
	SCRIPT_TIMER stGenericHeistFinaleBailTimer
	#ENDIF
	
	INT 							iGenericJobBitset
	INT 							iGenericJobVehicleClass
	INT 							iGenericJobRaceType
	INTERIOR_INSTANCE_INDEX 		iArenaInteriorIndex
	INTERIOR_INSTANCE_INDEX 		iArenaInterior_VIPLoungeIndex
	BOOL 							bRefreshVehicleDetails
	INT 							iDMTeamBalancingSettingCached = -1
	SCRIPT_TIMER					stInteriorReadyBailTimer
	
	structPedsForConversation		genericJobPedConvStruct
	GENERIC_JOB_DIALOGUE_STAGES 	eGenericJobDialogueStage = eGJ_DIALOGUE_STAGE_IDLE
	GENERIC_JOB_DIALOGUE 			eGenericJobDialogueToPlay = eGJD_UNSET
	
	SCRIPT_TIMER					stWaitActiveBailTimer
	
	PLAYER_INDEX					piJobLeader
	
	FIXER_SHORT_TRIPS_MENU_STRUCT	sCustomLobbyMenuData
	
	PED_COMP_NAME_ENUM eReturnItem
ENDSTRUCT

CONST_INT CORONA_IDLE_ANIM_INIT		0
CONST_INT CORONA_IDLE_ANIM_BASE		1
CONST_INT CORONA_IDLE_ANIM_IDLE		2
CONST_INT CORONA_IDLE_ANIM_JUGGERNAUT_INIT	3
CONST_INT CORONA_IDLE_ANIM_JUGGERNAUT_BASE	4
CONST_INT CORONA_IDLE_ANIM_JUGGERNAUT_IDLE	5

// Scaleform Constants

CONST_INT CORONA_OPTIONS_TIMER_DELAY	300
CONST_INT CORONA_SUMMARY_CARD_TIMER		3000
CONST_INT CORONA_TUTORIAL_TIMER			30000
CONST_INT CORONA_INVITE_ALL_TIMEOUT		8000
CONST_INT CORONA_HOST_HELP_TIME			7000
CONST_INT CORONA_MAX_VISIBLE_PLAYERS	16

// iBS_CoronaAnimTextActive
CONST_INT CORONA_HELP_INVITE_SENT						0
CONST_INT CORONA_HELP_NO_INVITES_SENT					1
CONST_INT CORONA_HELP_AUTOFILL							2
CONST_INT CORONA_HELP_READY_PLAYERS						3
CONST_INT CORONA_HELP_AUTOSTART_INIT					4
CONST_INT CORONA_HELP_AUTOSTART							5
CONST_INT CORONA_HELP_AUTOSTART_DELAYED					6
CONST_INT CORONA_HELP_TUTORIAL_WAIT						7
CONST_INT CORONA_HELP_ON_CALL_HOST						8
CONST_INT CORONA_HELP_WAIT_ON_HOST						9
CONST_INT CORONA_HELP_FORCE_LAUNCH						10

// *************************
// 		CLIENT CONSTANTS
// *************************

// All client menus (only option for mini games)
CONST_INT ciALL_CLIENT_OPTION_READY			0

// Race Client Options
CONST_INT ciRC_CLIENT_OPTION_VEHICLE		3		// Start at 2 for above (should be handled better)
CONST_INT ciRC_CLIENT_OPTION_COLOUR			4
CONST_INT ciRC_CLIENT_OPTION_2ND_COLOUR		5

// ********************************************
//  Allows us to query what class we are using
// ********************************************
FUNC BOOL IS_CORONA_CLASS_BIKE(FMMC_SELECTED_ITEMS &sSelection)
	RETURN ((sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_BIKES)
			OR (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_CYCLES))
ENDFUNC

FUNC BOOL IS_CORONA_CLASS_CYCLES(FMMC_SELECTED_ITEMS &sSelection)
	RETURN (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_CYCLES)
ENDFUNC

FUNC BOOL IS_CORONA_CLASS_SUVS(FMMC_SELECTED_ITEMS &sSelection)
	RETURN (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_SUV)
ENDFUNC

FUNC BOOL IS_CORONA_CLASS_JETS(FMMC_SELECTED_ITEMS &sSelection)
	RETURN (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_JETS)
ENDFUNC

FUNC BOOL IS_CORONA_CLASS_HELIS(FMMC_SELECTED_ITEMS &sSelection)
	RETURN (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_HELI)
ENDFUNC

FUNC BOOL IS_CORONA_CLASS_OFF_ROAD(FMMC_SELECTED_ITEMS &sSelection)
	RETURN (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_OFF_ROAD)
ENDFUNC

/// PURPOSE: Retudns the correct player index for the player to default looking at  
FUNC PLAYER_INDEX GET_CORONA_DEFAULT_TARGET_PED()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget != INVALID_PLAYER_INDEX()
			RETURN GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
		ENDIF
	ENDIF
	
	RETURN PLAYER_ID()
ENDFUNC

/// PURPOSE: Returns TRUE if the player is in a tutorial corona
FUNC BOOL IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
	
	IF (IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
	OR IS_BIT_SET(g_iMyRaceTutorialBitset, biTrigTut_PlayerInFmActivityTutorialCorona))
	AND g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
		
		RETURN TRUE		
		
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if the player is in a stunt race
FUNC BOOL IS_PLAYER_IN_STUNT_RACE()
	IF g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_STUNT 
	OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_STUNT_P2P
	OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_TARGET
	OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_TARGET_P2P
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC 

/// PURPOSE: Returns TRUE if the corona is in a closed crew session
FUNC BOOL IS_THIS_CORONA_IN_CLOSED_CREW_MODE()

	SWITCH g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY 
		CASE FM_SESSION_MENU_CHOICE_JOIN_CLOSED_MY_CREW_ONLY_SESSION 
		CASE FM_SESSION_MENU_CHOICE_JOIN_CLOSED_CREW_2_ONLY_SESSION 
		CASE FM_SESSION_MENU_CHOICE_JOIN_CLOSED_CREW_3_ONLY_SESSION 
		CASE FM_SESSION_MENU_CHOICE_JOIN_CLOSED_CREW_4_ONLY_SESSION 
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the gamer handle of the corona host.
/// RETURNS:
///    GAMER_HANDLE - corona host.
FUNC GAMER_HANDLE GET_CORONA_HOST(BOOL &bIAmCoronaHost)
	
	INT iPlayer
	GAMER_HANDLE eTempHandle
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF IS_THIS_TRANSITION_SESSION_PLAYER_THE_HOST(iPlayer)
			
			PRINTLN("[WJK] - SAVE_CREATE_EOM_VEHICLE - GET_CORONA_HOST - corona host is player: ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iPlayer].szGamerTag)
			PRINTLN("[WJK] - SAVE_CREATE_EOM_VEHICLE - GET_CORONA_HOST - player being checked in NUM_NETWORK_PLAYERS loop = ", iPlayer)
			PRINTLN("[WJK] - SAVE_CREATE_EOM_VEHICLE - GET_CORONA_HOST - I am player ", NATIVE_TO_INT(PLAYER_ID()))
			
			bIAmCoronaHost = FALSE
			
			IF NETWORK_IS_PLAYER_ACTIVE(PLAYER_ID())
				eTempHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
				IF NETWORK_IS_HANDLE_VALID(eTempHandle, SIZE_OF(eTempHandle))
					IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iPlayer].hGamer, SIZE_OF(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iPlayer].hGamer))
						IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iPlayer].hGamer, eTempHandle)
							bIAmCoronaHost = TRUE
							PRINTLN("[WJK] - SAVE_CREATE_EOM_VEHICLE - GET_CORONA_HOST - I am corona host, setting bIAmCoronaHost = TRUE.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			RETURN g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iPlayer].hGamer
			
		ENDIF
	ENDREPEAT
	
	PRINTLN("[WJK] - SAVE_CREATE_EOM_VEHICLE - GET_CORONA_HOST - no host found?")
	
	GAMER_HANDLE temp
	
	RETURN temp
	
ENDFUNC

FUNC PLAYER_INDEX GET_CORONA_HOST_PLAYER_INDEX()
	BOOL bTemp
	GAMER_HANDLE ghHost = GET_CORONA_HOST(bTemp)
	IF IS_GAMER_HANDLE_VALID(ghHost)
		RETURN NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(ghHost)
	ENDIF
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_GENERIC_JOB_DIALOGUE_STAGE_STRING_FOR_DEBUG(GENERIC_JOB_DIALOGUE_STAGES eStage)
	SWITCH eStage
		CASE eGJ_DIALOGUE_STAGE_IDLE						RETURN "eGJ_DIALOGUE_STAGE_IDLE"
		CASE eGJ_DIALOGUE_STAGE_SELECT_DIALOGUE				RETURN "eGJ_DIALOGUE_STAGE_SELECT_DIALOGUE"
		CASE eGJ_DIALOGUE_STAGE_PREPARE						RETURN "eGJ_DIALOGUE_STAGE_PREPARE"
		CASE eGJ_DIALOGUE_STAGE_WAIT_FOR_TRIGGER			RETURN "eGJ_DIALOGUE_STAGE_WAIT_FOR_TRIGGER"
		CASE eGJ_DIALOGUE_STAGE_DIALOGUE_ONE_SHOT			RETURN "eGJ_DIALOGUE_STAGE_DIALOGUE_ONE_SHOT"
		CASE eGJ_DIALOGUE_STAGE_DIALOGUE_LOOPED				RETURN "eGJ_DIALOGUE_STAGE_DIALOGUE_LOOPED"
		CASE eGJ_DIALOGUE_STAGE_DIALOGUE_END				RETURN "eGJ_DIALOGUE_STAGE_DIALOGUE_END"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_GENERIC_JOB_DIALOGUE_STRING_FOR_DEBUG(GENERIC_JOB_DIALOGUE eDialogue)
	SWITCH eDialogue
		CASE eGJD_UNSET															RETURN "eGJD_UNSET"

		CASE eGJD_ARENA_ANNOUNCER_GENERAL_0										RETURN "eGJD_ARENA_ANNOUNCER_GENERAL_0"
		CASE eGJD_ARENA_ANNOUNCER_GENERAL_1										RETURN "eGJD_ARENA_ANNOUNCER_GENERAL_1"
		CASE eGJD_ARENA_ANNOUNCER_GENERAL_2										RETURN "eGJD_ARENA_ANNOUNCER_GENERAL_2"
		CASE eGJD_ARENA_ANNOUNCER_GENERAL_3										RETURN "eGJD_ARENA_ANNOUNCER_GENERAL_3"
		CASE eGJD_ARENA_ANNOUNCER_GENERAL_4										RETURN "eGJD_ARENA_ANNOUNCER_GENERAL_4"
		
		CASE eGJD_ARENA_ANNOUNCER_BUZZER_BEATER_0								RETURN "eGJD_ARENA_ANNOUNCER_BUZZER_BEATER_0"
		CASE eGJD_ARENA_ANNOUNCER_BUZZER_BEATER_1								RETURN "eGJD_ARENA_ANNOUNCER_BUZZER_BEATER_1"
		CASE eGJD_ARENA_ANNOUNCER_BUZZER_BEATER_2								RETURN "eGJD_ARENA_ANNOUNCER_BUZZER_BEATER_2"
		
		CASE eGJD_ARENA_ANNOUNCER_CARNAGE_LMS_0									RETURN "eGJD_ARENA_ANNOUNCER_CARNAGE_LMS_0"
		CASE eGJD_ARENA_ANNOUNCER_CARNAGE_LMS_1									RETURN "eGJD_ARENA_ANNOUNCER_CARNAGE_LMS_1"
		CASE eGJD_ARENA_ANNOUNCER_CARNAGE_LMS_2									RETURN "eGJD_ARENA_ANNOUNCER_CARNAGE_LMS_2"
		CASE eGJD_ARENA_ANNOUNCER_CARNAGE_LTS_0									RETURN "eGJD_ARENA_ANNOUNCER_CARNAGE_LTS_0"
		CASE eGJD_ARENA_ANNOUNCER_CARNAGE_LTS_1									RETURN "eGJD_ARENA_ANNOUNCER_CARNAGE_LTS_1"
		CASE eGJD_ARENA_ANNOUNCER_CARNAGE_LTS_2									RETURN "eGJD_ARENA_ANNOUNCER_CARNAGE_LTS_2"
		
		CASE eGJD_ARENA_ANNOUNCER_FLAG_WAR_0									RETURN "eGJD_ARENA_ANNOUNCER_FLAG_WAR_0"
		CASE eGJD_ARENA_ANNOUNCER_FLAG_WAR_1									RETURN "eGJD_ARENA_ANNOUNCER_FLAG_WAR_1"
		CASE eGJD_ARENA_ANNOUNCER_FLAG_WAR_2									RETURN "eGJD_ARENA_ANNOUNCER_FLAG_WAR_2"
		
		CASE eGJD_ARENA_ANNOUNCER_BOMB_BALL_0									RETURN "eGJD_ARENA_ANNOUNCER_BOMB_BALL_0"
		CASE eGJD_ARENA_ANNOUNCER_BOMB_BALL_1									RETURN "eGJD_ARENA_ANNOUNCER_BOMB_BALL_1"
		CASE eGJD_ARENA_ANNOUNCER_BOMB_BALL_2									RETURN "eGJD_ARENA_ANNOUNCER_BOMB_BALL_2"
		
		CASE eGJD_ARENA_ANNOUNCER_GAMES_MASTERS_0								RETURN "eGJD_ARENA_ANNOUNCER_GAMES_MASTERS_0"
		CASE eGJD_ARENA_ANNOUNCER_GAMES_MASTERS_1								RETURN "eGJD_ARENA_ANNOUNCER_GAMES_MASTERS_1"
		CASE eGJD_ARENA_ANNOUNCER_GAMES_MASTERS_2								RETURN "eGJD_ARENA_ANNOUNCER_GAMES_MASTERS_2"
		
		CASE eGJD_ARENA_ANNOUNCER_HERE_COME_THE_MONSTERS_0						RETURN "eGJD_ARENA_ANNOUNCER_HERE_COME_THE_MONSTERS_0"
		CASE eGJD_ARENA_ANNOUNCER_HERE_COME_THE_MONSTERS_1						RETURN "eGJD_ARENA_ANNOUNCER_HERE_COME_THE_MONSTERS_1"
		CASE eGJD_ARENA_ANNOUNCER_HERE_COME_THE_MONSTERS_2						RETURN "eGJD_ARENA_ANNOUNCER_HERE_COME_THE_MONSTERS_2"
		
		CASE eGJD_ARENA_ANNOUNCER_HOT_BOMB_LMS_0								RETURN "eGJD_ARENA_ANNOUNCER_HOT_BOMB_LMS_0"
		CASE eGJD_ARENA_ANNOUNCER_HOT_BOMB_LMS_1								RETURN "eGJD_ARENA_ANNOUNCER_HOT_BOMB_LMS_1"
		CASE eGJD_ARENA_ANNOUNCER_HOT_BOMB_LMS_2								RETURN "eGJD_ARENA_ANNOUNCER_HOT_BOMB_LMS_2"
		CASE eGJD_ARENA_ANNOUNCER_HOT_BOMB_LTS_0								RETURN "eGJD_ARENA_ANNOUNCER_HOT_BOMB_LTS_0"
		CASE eGJD_ARENA_ANNOUNCER_HOT_BOMB_LTS_1								RETURN "eGJD_ARENA_ANNOUNCER_HOT_BOMB_LTS_1"
		CASE eGJD_ARENA_ANNOUNCER_HOT_BOMB_LTS_2								RETURN "eGJD_ARENA_ANNOUNCER_HOT_BOMB_LTS_2"
		
		CASE eGJD_ARENA_ANNOUNCER_TAG_TEAM_0									RETURN "eGJD_ARENA_ANNOUNCER_TAG_TEAM_0"
		CASE eGJD_ARENA_ANNOUNCER_TAG_TEAM_1									RETURN "eGJD_ARENA_ANNOUNCER_TAG_TEAM_1"
		CASE eGJD_ARENA_ANNOUNCER_TAG_TEAM_2									RETURN "eGJD_ARENA_ANNOUNCER_TAG_TEAM_2"
		
		CASE eGJD_ARENA_ANNOUNCER_WRECK_IT_0									RETURN "eGJD_ARENA_ANNOUNCER_WRECK_IT_0"
		CASE eGJD_ARENA_ANNOUNCER_WRECK_IT_1									RETURN "eGJD_ARENA_ANNOUNCER_WRECK_IT_1"
		CASE eGJD_ARENA_ANNOUNCER_WRECK_IT_2									RETURN "eGJD_ARENA_ANNOUNCER_WRECK_IT_2"
		
		CASE eGJD_MAX															RETURN "eGJD_MAX"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC
#ENDIF

PROC SET_GENERIC_JOB_DIALOGUE_STAGE(CORONA_MENU_DATA &coronaMenuData, GENERIC_JOB_DIALOGUE_STAGES eNewStage)
	PRINTLN("[CORONA][GENERIC_JOB][DIALOGUE] SET_GENERIC_JOB_DIALOGUE_STAGE - ", GET_GENERIC_JOB_DIALOGUE_STAGE_STRING_FOR_DEBUG(eNewStage))
	coronaMenuData.eGenericJobDialogueStage = eNewStage
ENDPROC

FUNC GENERIC_JOB_DIALOGUE_STAGES GET_GENERIC_JOB_DIALOGUE_STAGE(CORONA_MENU_DATA &coronaMenuData)
	RETURN coronaMenuData.eGenericJobDialogueStage
ENDFUNC

PROC CLEANUP_GENERIC_JOB_DIALOGUE(CORONA_MENU_DATA &coronaMenuData)
	IF IS_CORONA_BIT_SET(CORONA_GENERIC_JOB_DIALOGUE_ONE_SHOT_DONE)
		IF IS_STREAM_PLAYING()
			STOP_STREAM()
			PRINTLN("[CORONA][GENERIC_JOB] - CONTROL_FM_GENERIC_JOB_VEHICLE_SELECTION - STOP_STREAM()")
		ELSE
			PRINTLN("[CORONA][GENERIC_JOB] - CONTROL_FM_GENERIC_JOB_VEHICLE_SELECTION - No stream")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("DLC_AW_Arena_Lobby_Banter_Playing_Scene")
			STOP_AUDIO_SCENE("DLC_AW_Arena_Lobby_Banter_Playing_Scene")
		ENDIF
		CLEAR_CORONA_BIT(CORONA_GENERIC_JOB_DIALOGUE_ONE_SHOT_DONE)
		SET_GENERIC_JOB_DIALOGUE_STAGE(coronaMenuData, eGJ_DIALOGUE_STAGE_DIALOGUE_END)
	ENDIF
	
	IF IS_CORONA_BIT_SET(CORONA_ARENA_WARS_SET_CROWD_AUDIO)						
		STOP_AUDIO_SCENE("DLC_AW_Arena_Lobby_Vehicle_or_Team_Select_Scene")
		PRINTLN("[CORONA][GENERIC_JOB] - CONTROL_FM_GENERIC_JOB_VEHICLE_SELECTION - STOP_AUDIO_SCENE(DLC_AW_Arena_Lobby_Vehicle_or_Team_Select_Scene)")
		
		CLEAR_CORONA_BIT(CORONA_ARENA_WARS_SET_CROWD_AUDIO)
	ENDIF
ENDPROC


#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_CORONA_OUTFIT_STYLE_NAME(INT iStyle)
	
	SWITCH iStyle
		CASE VS_STYLE_PLAYER_OWNED		RETURN "VS_STYLE_PLAYER_OWNED"
		CASE VS_STYLE_CLASSIC			RETURN "VS_STYLE_CLASSIC"
		CASE VS_STYLE_THEMED			RETURN "VS_STYLE_THEMED"
		CASE VS_STYLE_ENTOURAGE			RETURN "VS_STYLE_ENTOURAGE"
		CASE VS_STYLE_HALLOWEEN			RETURN "VS_STYLE_HALLOWEEN"
		CASE VS_STYLE_SOLO				RETURN "VS_STYLE_SOLO"
		CASE VS_STYLE_HIDDEN				RETURN "VS_STYLE_HIDDEN"
	ENDSWITCH
	
	RETURN "UNKNOWN OUTFIT STYLE"
ENDFUNC
#ENDIF

/// PURPOSE:
///    Returns TRUE if the heist outfit is set to recommended
FUNC BOOL IS_CORONA_HEIST_OUTFIT_RECOMMENDED()
	RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = ciCORONA_CLOTHING_SETUP_OPTION_1
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the heist outfit is set to recommended
FUNC BOOL IS_CORONA_HEIST_OUTFIT_OWNED()
	RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = ciCORONA_CLOTHING_SETUP_OPTION_0
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the versus outfit is set to classic
FUNC BOOL IS_CORONA_VERSUS_OUTFIT_CLASSIC()
	RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = VS_STYLE_CLASSIC
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the versus outfit is set to themed
FUNC BOOL IS_CORONA_VERSUS_OUTFIT_THEMED()
	RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = VS_STYLE_THEMED
ENDFUNC

/// PURPOSE:
///		Returns TRUE is the versus outfit is set to Halloween
FUNC BOOL IS_CORONA_VERSUS_OUTFIT_HALLOWEEN()
	RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = VS_STYLE_HALLOWEEN
ENDFUNC

/// PURPOSE:
///		Returns TRUE is the versus outfit is set to Solo
FUNC BOOL IS_CORONA_VERSUS_OUTFIT_SOLO()
	RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = VS_STYLE_SOLO
ENDFUNC

/// PURPOSE:
///		Returns TRUE is the versus outfit is set to Hidden
FUNC BOOL IS_CORONA_VERSUS_OUTFIT_HIDDEN()
	RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = VS_STYLE_HIDDEN
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the versus outfit is set to player owned
FUNC BOOL IS_CORONA_VERSUS_OUTFIT_PLAYER_OWNED()
	RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = VS_STYLE_PLAYER_OWNED
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the versus outfit is set to entourage
FUNC BOOL IS_CORONA_VERSUS_OUTFIT_ENTOURAGE()
	RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = VS_STYLE_ENTOURAGE
ENDFUNC

/// PURPOSE:
///    Sets the value of our random number used throughout the corona
PROC SET_PLAYER_CORONA_RANDOM_SEED(INT iValue)
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	IF iMyGBD != -1
		PRINTLN("SET_PLAYER_CORONA_RANDOM_SEED - GlobalplayerBD_FM[", iMyGBD, "].sClientCoronaData.iHostRandomValue = ", iValue)
		GlobalplayerBD_FM[iMyGBD].sClientCoronaData.iHostRandomValue = iValue
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the value of our random number used throughout the corona
FUNC INT GET_PLAYER_CORONA_RANDOM_SEED(INT iPlayer)
	IF iPlayer != -1
		RETURN GlobalplayerBD_FM[iPlayer].sClientCoronaData.iHostRandomValue
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Clear our random number used throughout the corona for randomising the menu options
PROC CLEAR_PLAYER_CORONA_RANDOM_SEED()
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	IF iMyGBD != -1
		PRINTLN("CLEAR_PLAYER_CORONA_RANDOM_SEED - GlobalplayerBD_FM[", iMyGBD, "].sClientCoronaData.iHostRandomValue = 0")
		GlobalplayerBD_FM[iMyGBD].sClientCoronaData.iHostRandomValue = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the Versus clothing setup
FUNC INT GET_CORONA_VERSUS_OUTFIT_SETUP()
	RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP]
ENDFUNC

/// PURPOSE:
///    Sets our versus outfit style
PROC SET_CORONA_VERSUS_OUTFIT_STYLE(INT iOutfitStyle)

	PRINTLN("[CORONA] SET_CORONA_VERSUS_OUTFIT_STYLE - Setting outfit style to: ", iOutfitStyle)

	g_CoronaClonePeds.iVersusOutfitStyle = iOutfitStyle
	g_CoronaClonePeds.iVersusBackgroundStyle = iOutfitStyle // Always update the bg style if we update the main style
	
	GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iVSOutfitSet = iOutfitStyle	// Update our BD data for the outfit being used
ENDPROC

/// PURPOSE:
///    Returns the currently set outfit style
FUNC INT GET_CORONA_VERSUS_OUTFIT_STYLE()
	RETURN g_CoronaClonePeds.iVersusOutfitStyle
ENDFUNC


/// PURPOSE:
///    Returns TRUE when the mission Team selection screen should be set up for single players (ie. a race with outfits)
FUNC BOOL SHOULD_CORONA_TEAM_MISSION_SIMULATE_SINGLE_PLAYER_TEAMS()

	INT iTeam
	REPEAT g_FMMC_STRUCT.iNumberOfTeams iTeam
		IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam] > 1
			PRINTLN("[CORONA][LH][TEAMNAMES] SHOULD_CORONA_TEAM_MISSION_SIMULATE_SINGLE_PLAYER_TEAMS - Returning False - g_FMMC_STRUCT.iMaxNumPlayersPerTeam[",iTeam,"] > 1 (", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam], ")")
			RETURN FALSE
		ENDIF
	ENDREPEAT

	// Speed Race
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SpeedRace(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[CORONA][LH][TEAMNAMES] SHOULD_CORONA_TEAM_MISSION_SIMULATE_SINGLE_PLAYER_TEAMS - Returning True - Speed Race")
		RETURN TRUE
	ENDIF
	
	// Thanksgiving Dinner
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_Ebc(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[CORONA][LH][TEAMNAMES] SHOULD_CORONA_TEAM_MISSION_SIMULATE_SINGLE_PLAYER_TEAMS - Returning True - Every Bullet Counts")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciPLAYER_NAME_NOT_CREW_NAME)
		PRINTLN("[CORONA][LH][TEAMNAMES] SHOULD_CORONA_TEAM_MISSION_SIMULATE_SINGLE_PLAYER_TEAMS - Returning True - ciPLAYER_NAME_NOT_CREW_NAME")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///   	Returns TRUE if the teams are unbalanced for the current Mission.
FUNC BOOL DOES_CORONA_VERSUS_MISSION_HAVE_UNBALANCED_TEAMS()
	
	INT iTeam
	INT iNumPlayersOnTeam = -1
	
	// For each team check there are no differences on max num players per team
	REPEAT g_FMMC_STRUCT.iNumberOfTeams iTeam
		IF iNumPlayersOnTeam != -1
			IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam] != iNumPlayersOnTeam
				RETURN TRUE
			ENDIF
		ENDIF
		
		iNumPlayersOnTeam = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam]
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


// Fix for 2201644, now called for heists aswell. We copy our selected outfit to all teams so any team we switch to will have the same outfit
PROC COPY_PLAYER_CORONA_OUTFITS_FOR_ALL_TEAMS()
	INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen	

	// If we have jipped during rounds, we need to alter the original team we would have been on
	IF ROUNDS_NEED_TO_SET_VS_OUTFITS()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciNO_TEAM_SWAP_ON_ROUNDS)

		INT iRound = g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed

		WHILE(iRound > 0)
			iTeam--
			
			IF iTeam < 0
				iTeam = (g_FMMC_STRUCT.iNumberOfTeams-1)
			ENDIF
			
			iRound--
		ENDWHILE
		PRINTLN("COPY_PLAYER_CORONA_OUTFITS_FOR_ALL_TEAMS  - Override our teams due to rounds and JIP: iTeam (new) = ", iTeam, ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed, ", g_FMMC_STRUCT.iNumberOfTeams = ", g_FMMC_STRUCT.iNumberOfTeams)
	ENDIF

	IF iTeam >= 0
	AND iTeam < FMMC_MAX_TEAMS
	
		INT i
		INT iOutfit = GET_LOCAL_DEFAULT_OUTFIT(iTeam)
		INT iMask = GET_LOCAL_DEFAULT_MASK(iTeam)
		
		PRINTLN("COPY_PLAYER_CORONA_OUTFITS_FOR_ALL_TEAMS  - Saving all team outfits to be ours: iTeam = ", iTeam, ", iOutfit = ", iOutfit, ", iMask = ", iMask)
		
		// Loop over all teams and update all teams to match ours
		REPEAT FMMC_MAX_TEAMS i
			
			IF i != iTeam
				
				// update these values to our outfit / mask
				SET_LOCAL_DEFAULT_OUTFIT(i, iOutfit)
				SET_LOCAL_DEFAULT_MASK(i, iMask)
			ENDIF
		ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("COPY_PLAYER_CORONA_OUTFITS_FOR_ALL_TEAMS  - iTeam = ", iTeam)
	#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Copy the local players team outfit and mask to all other team data locally. This means for rounds the outfit / mask will persist
PROC SAVE_PLAYER_CHOSEN_OUTFIT_FOR_ALL_TEAMS()
	
	// IF the teams are unbalanced, team outfits will be changing each round (2181492)
	IF (DOES_CORONA_VERSUS_MISSION_HAVE_UNBALANCED_TEAMS() OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciMISSION_DISABLE_OUTFIT_RETENTION))
	AND NOT IS_CORONA_VERSUS_OUTFIT_PLAYER_OWNED()
	
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciMISSION_DISABLE_OUTFIT_RETENTION)
			PRINTLN("[CORONA] SAVE_PLAYER_CHOSEN_OUTFIT_FOR_ALL_TEAMS  - IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciMISSION_DISABLE_OUTFIT_RETENTION) = TRUE, exit")
		ELSE
			PRINTLN("[CORONA] SAVE_PLAYER_CHOSEN_OUTFIT_FOR_ALL_TEAMS  - DOES_CORONA_VERSUS_MISSION_HAVE_UNBALANCED_TEAMS = TRUE, exit")
		ENDIF
		#ENDIF
		
		// Exit the function
		EXIT
	ENDIF
	
	COPY_PLAYER_CORONA_OUTFITS_FOR_ALL_TEAMS()
ENDPROC

///// PURPOSE:
/////    Returns TRUE if an ammo / armor cash transaction is active
//FUNC BOOL IS_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE()
//	RETURN g_sAmmoArmorCashTransaction.iAmmoArmorCashTransactionActive != 0
//ENDFUNC
//
///// PURPOSE:
/////    Set our global as to whether an ammo or armor cash transaction is currently in progress
//PROC SET_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE(AMMO_ARMOR_CASH_TRANSACTION_ENUM eAmmoArmorAction)
//
//	DEBUG_PRINTCALLSTACK()
//	PRINTLN("SET_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE - Setting ammo / armor cash transaction: ", ENUM_TO_INT(eAmmoArmorAction), " as active")
//
//	SET_BIT(g_sAmmoArmorCashTransaction.iAmmoArmorCashTransactionActive, ENUM_TO_INT(eAmmoArmorAction))
//ENDPROC
//
///// PURPOSE:
/////    Returns TRUE if a full ammo transaction is currently active
//FUNC BOOL IS_FULL_AMMO_CASH_TRANSACTION_ACTIVE()
//	RETURN IS_BIT_SET(g_sAmmoArmorCashTransaction.iAmmoArmorCashTransactionActive, ENUM_TO_INT(AMMO_ARMOR_CASH_TRANSACTION_FULL_AMMO))
//ENDFUNC
//
///// PURPOSE:
/////    Clears our flags for any pending ammo / armor cash transactions
//PROC CLEAR_AMMO_ARMOR_CASH_TRANSACTIONS()
//
//	DEBUG_PRINTCALLSTACK()
//	PRINTLN("SET_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE - Clearing ammo / armor cash transaction")
//	
//	g_sAmmoArmorCashTransaction.iAmmoArmorCashTransactionActive = 0
//ENDPROC

/// PURPOSE: Query if the player has sent an invite
FUNC BOOL HAS_PLAYER_SENT_AN_INVITE()
	RETURN g_bPlayerHasSentInvite
ENDFUNC

/// PURPOSE: Mark this player as having sent an invite
PROC SET_PLAYER_HAS_SENT_AN_INVITE(BOOL bSentAnInvite)
	PRINTLN("SET_PLAYER_HAS_SENT_AN_INVITE - ", GET_STRING_FROM_BOOL(bSentAnInvite))
	g_bPlayerHasSentInvite = bSentAnInvite
ENDPROC

/// PURPOSE: Returns TRUE if the vehicle in the corona array is DLC
FUNC BOOL IS_CORONA_VEHICLE_DLC(CORONA_ORDERED_VEHICLE coronaOrderedVehicle)

	IF IS_BIT_SET(coronaOrderedVehicle.iVehicleInfoBS, CORONA_ORDERED_VEHICLE_IS_DLC)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

// Find out type of race
FUNC BOOL IS_THIS_A_RALLY_RACE_TEMP(FMMC_SELECTED_ITEMS &sSelection)

	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
		IF (sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_RALLY )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MISSION_TEAM_BALANCING_OFF_TEMP(FMMC_SELECTED_ITEMS &sSelection)
	RETURN (sSelection.iSelection[ciMISSION_HOST_OPTION_TEAM_BAL] = DM_TEAM_BALANCING_OFF)
ENDFUNC

/// PURPOSE: Returns TRUE if the mission allows the player to select rounds 
FUNC BOOL IS_MISSION_VALID_FOR_ROUND(INT iMissionSubType)

	// Do not allow for playlists
	IF iMissionSubType = FMMC_MISSION_TYPE_CTF
	OR iMissionSubType = FMMC_MISSION_TYPE_LTS
	OR iMissionSubType = FMMC_MISSION_TYPE_VERSUS
	OR iMissionSubType = FMMC_MISSION_TYPE_COOP
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

//
FUNC BOOL IS_MISSION_VALID_FOR_FORCED_WEAPON()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_CORONA_WEAPON_RESTRICTIONS)
		RETURN FALSE
	ENDIF

	IF (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF)
	OR (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_VERSUS)
	OR (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_COOP)
	OR (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CORONA_AUTOFILL_AVAILABLE()

	IF IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
		RETURN FALSE
	ENDIF
	
	// As a client we should never have this option
	IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
		RETURN FALSE
	ENDIF
	
	// We do not want the quickstart option for head to head
	IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
	OR IS_CORONA_IN_CHALLENGE_MODE()
		RETURN FALSE
	ENDIF
	
	// Completelty remove the auto invite menu if this tunable is set
	IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
		IF g_sMPTunables.bDisableHeistAutoInviteMenu
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN (NOT IS_CORONA_BIT_SET(CORONA_AUTOFILL_IS_NOT_AVAILABLE))
ENDFUNC

FUNC BOOL IS_CORONA_FULL(MISSION_TO_LAUNCH_DETAILS	&sLaunchMissionDetails)
	RETURN (GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION() = GET_MISSION_TYPE_MAX(sLaunchMissionDetails))
ENDFUNC

/// PURPOSE:
///    This function will return FALSE if the 'ALL' option for this invite menu has been disabled
FUNC BOOL SHOULD_CORONA_MOVE_TO_SUB_INVITE_MENU(CORONA_MENU_DATA &coronaMenuData)
	
	SWITCH coronaMenuData.iCurrentSelection
		
		CASE ciCORONA_LOBBY_LAST_JOB_PLAYERS
		
			// If we are host or client check the correct tunable
			IF IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Host_All_Last_Job
					RETURN FALSE
				ENDIF
			ELSE
				IF g_sMPTunables.bDisable_Client_All_Last_Job
					RETURN FALSE
				ENDIF
			ENDIF		
		BREAK
		
		CASE ciCORONA_LOBBY_FRIENDS
			IF IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Host_All_Friends
					RETURN FALSE
				ENDIF
			ELSE
				IF g_sMPTunables.bDisable_Client_All_Friends
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciCORONA_LOBBY_CREW_MEMBERS
			IF IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Host_All_Crews
					RETURN FALSE
				ENDIF
			ELSE
				IF g_sMPTunables.bDisable_Client_All_Crews
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciCORONA_LOBBY_MATCHED_PLAYERS
			IF IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Host_All_Skilled_Matched
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK

		CASE ciCORONA_LOBBY_LAST_HEIST_PLAYERS
			IF IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Host_All_Last_Heist
					RETURN FALSE
				ENDIF
			ELSE
				IF g_sMPTunables.bDisable_Client_All_Last_Heist
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Checks to see if we are setting up a team deathmatch with team balancing off   
FUNC BOOL IS_THIS_A_TEAM_DEATHMATCH_WITH_BALANCING_OFF(FMMC_SELECTED_ITEMS &sSelection)
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
		IF sSelection.iSelection[ciDM_HOST_OPTION_TYPE] =  FMMC_DM_TYPE_TEAM
		OR sSelection.iSelection[ciDM_HOST_OPTION_TYPE] =  FMMC_DM_TYPE_TEAM_KOTH
			IF sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL] = DM_TEAM_BALANCING_OFF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if type of race has been set to GTA 
FUNC BOOL IS_THIS_A_GTA_RACE_TEMP(FMMC_SELECTED_ITEMS &sSelection)

	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
		IF (sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_GTA )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if the GTA Race has been set to teams 
FUNC BOOL IS_THIS_CORONA_A_GTA_TEAM_RACE(FMMC_SELECTED_ITEMS &sSelection)
	
	IF NOT IS_THIS_A_GTA_RACE_TEMP(sSelection)
		RETURN FALSE
	ENDIF

	IF (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_CYCLES)
		RETURN FALSE
	ENDIF

	IF (sSelection.iSelection[ciRC_HOST_OPTION_GTA_TEAM] = ciRACE_GTA_TEAMS_OFF)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE: Translates the setting int to a total number of rounds
FUNC INT GET_CORONA_NUMBER_OF_ROUND(INT iSetting)
	RETURN iSetting
ENDFUNC

/// PURPOSE:
///    Return the max number of rounds based on teams involved
FUNC INT GET_CORONA_MAX_ROUNDS_FOR_TEAMS(INT iTeams)
	SWITCH iTeams
		CASE 2		RETURN (ciMISSION_MAX_TYPE_ROUNDS-3)
		CASE 3		RETURN (ciMISSION_MAX_TYPE_ROUNDS-4)
	ENDSWITCH
	
	RETURN (ciMISSION_MAX_TYPE_ROUNDS-5)
ENDFUNC

FUNC BOOL CAN_PLAYER_PURCHASE_AMMO_IN_CORONA(FMMC_SELECTED_ITEMS &sSelection, BOOL bOnSettingScreen = FALSE)

	IF IS_PLAYER_SCTV(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
		IF g_myStartingWeapon = WEAPONTYPE_UNARMED
			RETURN FALSE
		ENDIF
	ENDIF
	
	// If deathmatch is locked to a weapon, we have to hide starting weapon options
	IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH)
	AND (sSelection.iSelection[ciDM_HOST_OPTION_WEAPONS] != ciDM_WEAPON_DEFAULT)
		RETURN FALSE
	ENDIF
	
	// If capture then we need to hide starting weapon.
	IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION)
		IF sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPONS] != ciDM_WEAPON_DEFAULT
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION)
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAERIAL_VEHICLE_SPAWN) 
		RETURN FALSE
	ENDIF
	
	IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION)
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciMISSION_IS_PSEUDORACE) 
		RETURN FALSE
	ENDIF

	IF ( g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH 	AND sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_VEHICLE )
	OR ( g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE 		AND ( NOT IS_THIS_A_GTA_RACE(sSelection) OR IS_AIR_RACE(g_FMMC_STRUCT.iRaceType) ) )
	OR ( g_FMMC_STRUCT.iMissionType = FMMC_TYPE_BASE_JUMP 	AND sSelection.iSelection[ciPARA_HOST_OPTION_SHOOTING] = 0 )
	OR IS_THIS_A_MINI_GAME(g_FMMC_STRUCT.iMissionType)
		RETURN FALSE
	ENDIF
	
	IF bOnSettingScreen
		IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
		OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_TARGET_ASSAULT_RACE()
		RETURN FALSE
	ENDIF
	
	IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL)
	AND IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_Halloween)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_HideCoronaOptionStartingWeapon)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns TRUE if the player can purchase armour for the current Job
FUNC BOOL CAN_PLAYER_PURCHASE_ARMOR_IN_CORONA(FMMC_SELECTED_ITEMS &sSelection, BOOL bOnSettingScreen = FALSE)

	IF IS_PLAYER_SCTV(PLAYER_ID())
		RETURN FALSE
	ENDIF

	IF ( g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE 		AND NOT IS_THIS_A_GTA_RACE(sSelection) )
	OR ( g_FMMC_STRUCT.iMissionType = FMMC_TYPE_BASE_JUMP 	AND sSelection.iSelection[ciPARA_HOST_OPTION_SHOOTING] = 0 )
	OR IS_THIS_A_MINI_GAME(g_FMMC_STRUCT.iMissionType)
		RETURN FALSE
	ENDIF
	
	IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION)
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAERIAL_VEHICLE_SPAWN) 
		RETURN FALSE
	ENDIF
	
	IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION)
	AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciMISSION_IS_PSEUDORACE) 
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciREMOVE_ARMOUR_PURCHASE_OPTION))
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciREMOVE_ARMOUR_PURCHASE_OPTION)
		RETURN FALSE
	ENDIF
	
	IF bOnSettingScreen
		IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
		OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_TARGET_ASSAULT_RACE()
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns TRUE if the Race is a Foot Race
FUNC BOOL IS_THIS_RACE_A_FOOT_RACE()
	RETURN (g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_ON_FOOT OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_ON_FOOT_P2P)
ENDFUNC

FUNC BOOL IS_CORONA_VEHICLE_VALID_FOR_GTA_RACE(MODEL_NAMES mnVeh)
	
	IF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
	
		IF IS_CORONA_CLASS_HELIS(g_sTransitionSessionOptions)
			IF (mnVeh != ANNIHILATOR AND mnVeh != BUZZARD AND mnVeh != SAVAGE)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_CORONA_CLASS_JETS(g_sTransitionSessionOptions)
			IF (mnVeh = besra)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Perform a number of checks against the DLC vehicles to ensure they are valid for the Race
FUNC BOOL IS_DLC_CORONA_VEHICLE_VALID(INT iClass)
	
	MODEL_NAMES mnDLCVeh = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, g_iMyRaceModelChoice, g_FMMC_STRUCT.iRaceType)
	
	IF DOES_JOB_USE_RACE_VEHICLE_SELECTION(IS_RACE())
		mnDLCVeh = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass), g_iMyRaceModelChoice)
		PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - model overriden to grab from creator selection for generic job")
	ENDIF
	
	IF NOT IS_DLC_VEHICLE_MODEL_ALLOWED(mnDLCVeh)
		#IF IS_DEBUG_BUILD
		IF IS_MODEL_VALID(mnDLCVeh)
			IF mnDLCVeh = DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - IS_DLC_VEHICLE_MODEL_ALLOWED = DUMMY_MODEL_FOR_SCRIPT is not allowed by tunable")
			ELSE
				PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - IS_DLC_VEHICLE_MODEL_ALLOWED = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnDLCVeh), " is not allowed by tunable")
			ENDIF
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	// Check if the model is allowed within the creator.
	IF NOT DOES_CREATOR_ALLOW_DLC_VEHICLE(iClass, g_iMyRaceModelChoice)
		
		#IF IS_DEBUG_BUILD
			IF IS_MODEL_VALID(mnDLCVeh)
				IF mnDLCVeh = DUMMY_MODEL_FOR_SCRIPT
					PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - DLC Vehicle Model = DUMMY_MODEL_FOR_SCRIPT is not turned ON in creator")
				ELSE
					PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - DLC Vehicle Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnDLCVeh), " is not turned ON in creator")
				ENDIF
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_THIS_A_HSW_RACE()
		IF IS_VEHICLE_MODEL_A_HSW_VEHICLE(mnDLCVeh)
			SWITCH mnDLCVeh
				CASE BRIOSO
					IF NOT g_sMPTunables.bENABLE_HSW_BRIOSO_RA
						PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - FALSE - IS_THIS_A_HSW_RACE and NOT g_sMPTunables.bENABLE_HSW_BRIOSO_RA, Brioso R/A is disabled as an HSW vehicle via tunable")
						RETURN FALSE
					ENDIF
				BREAK
				CASE DEVESTE
					IF NOT g_sMPTunables.bENABLE_HSW_DEVESTE_EIGHT
						PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - FALSE - IS_THIS_A_HSW_RACE and NOT g_sMPTunables.bENABLE_HSW_DEVESTE_EIGHT, Deveste Eight is disabled as an HSW vehicle via tunable")
						RETURN FALSE
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	#ENDIF
	
	IF IS_THIS_A_GTA_RACE_TEMP(g_sTransitionSessionOptions)
		IF NOT IS_CORONA_VEHICLE_VALID_FOR_GTA_RACE(mnDLCVeh)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_THIS_A_RALLY_RACE_TEMP(g_sTransitionSessionOptions)
		IF NOT DOES_VEHICLE_HAVE_TWO_OR_MORE_SEATS(mnDLCVeh)
			RETURN FALSE
		ENDIF	
	ENDIF
	
	IF NOT IS_VEHICLE_AVAILABLE_FOR_GAME(mnDLCVeh)
	
		#IF IS_DEBUG_BUILD
			IF IS_MODEL_VALID(mnDLCVeh)
				IF mnDLCVeh = DUMMY_MODEL_FOR_SCRIPT
					PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - DLC Vehicle not available to game. DUMMY_MODEL_FOR_SCRIPT")
				ELSE
					PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - DLC Vehicle not available to game. ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnDLCVeh))
				ENDIF
			ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF CV2_WAS_STUNT_RACE_LAUNCHED_AS_PROFESSIONAL()
		IF CV2_GET_PROFESSIONAL_RACE_VEHICLE_MODEL() != DUMMY_MODEL_FOR_SCRIPT
			IF mnDLCVeh != CV2_GET_PROFESSIONAL_RACE_VEHICLE_MODEL()
				#IF IS_DEBUG_BUILD
				IF IS_MODEL_VALID(CV2_GET_PROFESSIONAL_RACE_VEHICLE_MODEL())
					PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - Vehicle does not match professional fixed model - CV2_GET_PROFESSIONAL_RACE_VEHICLE_MODEL() = ", GET_MODEL_NAME_FOR_DEBUG(CV2_GET_PROFESSIONAL_RACE_VEHICLE_MODEL()))
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ELSE
			PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - CV2_GET_PROFESSIONAL_RACE_VEHICLE_MODEL() = DUMMY_MODEL_FOR_SCRIPT")
		ENDIF
	ELSE
		PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - CV2_WAS_STUNT_RACE_LAUNCHED_AS_PROFESSIONAL() = FALSE")
	ENDIF
	
	IF IS_STUNT_RACE()
		IF mnDLCVeh = SANCTUS
			PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - Model is Sanctus = FALSE")
			RETURN FALSE
		ENDIF
		
		IF mnDLCVeh = AVARUS
			PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - Model is Avarus = FALSE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_AN_INVALID_IMPORT_EXPORT_SPECIAL_VEHICLE(mnDLCVeh)
	AND NOT IS_TARGET_ASSAULT_RACE()
		PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - Not a target assault race, vehicle is an invalid import export special vehicle")
	
		PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - IS_VEHICLE_AN_INVALID_IMPORT_EXPORT_SPECIAL_VEHICLE() = TRUE. Return FALSE")
	
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE)
		IF IS_VEHICLE_AN_IMPORT_EXPORT_SPECIAL_VEHICLE(mnDLCVeh)
		AND NOT IS_SPECIAL_OR_WEAPONISED_VEHICLE_ALLOWED_FOR_RACE(mnDLCVeh, iClass)
			PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - Vehicle is an import export special. IS_VEHICLE_AN_IMPORT_EXPORT_SPECIAL_VEHICLE = TRUE")
			RETURN FALSE
		ENDIF
		
		IF IS_VEHICLE_A_WEAPONISED_VEHICLE(mnDLCVeh)
		AND NOT IS_SPECIAL_OR_WEAPONISED_VEHICLE_ALLOWED_FOR_RACE(mnDLCVeh, iClass)
			PRINTLN("[CORONA] IS_DLC_CORONA_VEHICLE_VALID - Vehicle is a gunrunning weaponised vehicle. IS_VEHICLE_A_WEAPONISED_VEHICLE = TRUE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_MODEL_AVAILABLE_IN_RACES(mnDLCVeh)
		PRINTLN("[CORONA] IS_VEHICLE_VALID_FOR_RACE - IS_MODEL_AVAILABLE_IN_RACES - FALSE - ", GET_MODEL_NAME_FOR_DEBUG(mnDLCVeh), " is not available in races")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_A_VEHICLE_DM()
	RETURN (g_FMMC_STRUCT.iVehicleDeathmatch > 0)
ENDFUNC

PROC UPDATE_TOTAL_PLAYER_IN_CORONA(CORONA_MENU_DATA &coronaMenuData, INT iAmount)
	coronaMenuData.iTotalPlayersInCorona = iAmount
ENDPROC

/// PURPOSE: Returns TRUE if we are in Autofill mode  
FUNC BOOL IS_CORONA_IN_AUTOFILL_MODE()
	RETURN IS_CORONA_BIT_SET(CORONA_AUTOFILL_SENT)	
ENDFUNC

/// PURPOSE: Returns TRUE if we are in Autostart mode
FUNC BOOL IS_CORONA_IN_AUTOSTART_MODE()
	RETURN IS_CORONA_BIT_SET(CORONA_AUTOSTART_LAUCNHED)
ENDFUNC


FUNC BOOL IS_DM_FIXED_TO_MIN_TEAMS()
	RETURN (g_FMMC_STRUCT.iMaxNumberOfTeams <= 2)
ENDFUNC

/// PURPOSE: Returns TRUE if custom vehicles are turned on for races
FUNC BOOL IS_CUSTOM_VEHICLE_TURNED_ON_FOR_COORNA(FMMC_SELECTED_ITEMS &sSelection)

	IF DOES_JOB_USE_RACE_VEHICLE_SELECTION(IS_RACE())
		IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
			RETURN TRUE
		ENDIF
	ENDIF

	IF (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_CYCLES)
		RETURN FALSE
	ENDIF
	
	IF IS_CORONA_BIT_SET(CORONA_SPECIAL_VEHICLE_HAS_BEEN_ALLOWED)
		RETURN FALSE
	ENDIF

	RETURN (sSelection.iSelection[ciRC_HOST_OPTION_CUSTOM_VEHICLE] = ciCORONA_CUSTOM_VEHICLE_ON)
ENDFUNC

/// PURPOSE: Returns TRUE if corona is set up for competitive teams   
FUNC BOOL IS_THIS_A_TEAM_CORONA(FMMC_SELECTED_ITEMS &sSelection, BOOL bRaceCheckOnly = FALSE)

	// Is this a rally
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
		IF IS_THIS_A_RALLY_RACE_TEMP(sSelection)
			RETURN TRUE
		ENDIF
		
		IF IS_THIS_CORONA_A_GTA_TEAM_RACE(sSelection)
			RETURN TRUE
		ENDIF
		
		IF IS_TARGET_ASSAULT_RACE()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF bRaceCheckOnly
		RETURN FALSE
	ENDIF
	
	// Is this a team dm	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
		IF sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM
		OR sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM_KOTH
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Is this a survival	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL
		RETURN TRUE
	ENDIF
	
	// Is this a mission with more than 1 team
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	
		IF g_FMMC_STRUCT.iMaxNumberOfTeams > 1
			RETURN TRUE
		ENDIF
		
		IF IS_CORONA_BIT_SET(CORONA_NO_TEAM_OUTFITS)
			RETURN TRUE
		ENDIF
		
		IF IS_LOADED_MISSION_TYPE_FLOW_MISSION()
			RETURN TRUE
		ENDIF
		
		
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Return the current viewed slot
FUNC INT GET_MY_CURRENT_TARGET_SLOT(CORONA_MENU_DATA &coronaMenuData)
	RETURN coronaMenuData.iCoronaSlotTarget
ENDFUNC

/// PURPOSE: Returns TRUE when we are viewing Lamar in corona 
FUNC BOOL IS_PLAYER_VIEWING_TUTORIAL_PED(CORONA_MENU_DATA &coronaMenuData)

	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		
		IF coronaMenuData.iTutorialPedSlot = coronaMenuData.iCoronaSlotTarget
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the team number that is being displayed in the slot
FUNC INT GET_ACTUAL_TEAM_FOR_CORONA_SLOT(CORONA_MENU_DATA &coronaMenuData, INT iSlot)
	
	INT iTeam = -1
	
	IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
		iTeam = coronaMenuData.iCoronaSlotLayout[iSlot]
	ELSE
		iTeam = coronaMenuData.lobbyPlayers[iSlot].iTeam
	ENDIF

	IF iTeam = -1
	AND IS_PLAYER_VIEWING_TUTORIAL_PED(coronaMenuData)
		iTeam = BETTING_TUTORIAL_PED_INDEX
	ENDIF

	RETURN iTeam
ENDFUNC

FUNC STRING GET_CORONA_JOB_FULL_TEXT()
	IF IS_THIS_A_MINI_GAME(g_FMMC_STRUCT.iMissionType)
		IF IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
			RETURN "FM_ISC_FULLA"
		ELSE
			RETURN "FM_ISC_CFULLA"
		ENDIF
	ELSE
		IF IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
			RETURN "FM_ISC_FULLJ"
		ELSE
			RETURN "FM_ISC_CFULLJ"
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC


/// PURPOSE: Checks all teams like each other which means this mission is co-op
PROC SET_CORONA_IS_MISSION_COOP()
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		IF g_FMMC_STRUCT.iMaxNumberOfTeams > 1
			
			INT i, j
			
			// Loop through all teams comparing each to each other
			FOR i = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams-1) STEP 1
				FOR j = 1 TO (g_FMMC_STRUCT.iMaxNumberOfTeams-1) STEP 1
					
					// Check if the teams like each other
					IF NOT DOES_TEAM_LIKE_TEAM(i, j)
						//...no, quit as not COOP
						EXIT
					ENDIF
				ENDFOR
				
			ENDFOR
			
			PRINTLN("[CORONA] SET_CORONA_IS_MISSION_COOP - settingn mission as CO-OP")
			
			//...reached here then all teams like all other teams
			SET_CORONA_BIT(CORONA_MISSION_IS_COOP)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Validates the current clothing setup
FUNC BOOL IS_CORONA_MISSION_OUTFIT_SETUP_VALID(INT iSetup, BOOL bIgnoreAllCheck = FALSE)
	
	SWITCH iSetup
		CASE VS_STYLE_PLAYER_OWNED				
			IF g_FMMC_STRUCT.iVSDefaultStyleClass != VS_STYLE_HIDDEN
			AND g_FMMC_STRUCT.iVSDefaultStyleClass != VS_STYLE_ENTOURAGE
				RETURN TRUE
			ENDIF
		BREAK
			
		CASE VS_STYLE_HALLOWEEN
			IF g_sMPTunables.bENABLE_OUTFIT_ADVERSARY_HALLOWEEN
			OR g_sMPTunables.bDEFAULT_OUTFIT_ADVERSARY_HALLOWEEN
				RETURN TRUE
			ELSE
				IF g_FMMC_STRUCT.biVSAvailableOutfit[iSetup][0] != 0
				OR g_FMMC_STRUCT.biVSAvailableOutfit[iSetup][1] != 0
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
			// Default behaviour is this is never available
			RETURN FALSE
		BREAK
		CASE VS_STYLE_SOLO
			IF g_sMPTunables.bENABLE_OUTFIT_ADVERSARY_SOLO
			OR g_sMPTunables.bDEFAULT_OUTFIT_ADVERSARY_SOLO
				RETURN TRUE
			ELSE
				IF g_FMMC_STRUCT.biVSAvailableOutfit[iSetup][0] != 0
				OR g_FMMC_STRUCT.biVSAvailableOutfit[iSetup][1] != 0
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
			// Default behaviour is this is never available
			RETURN FALSE
		BREAK
		CASE VS_STYLE_ENTOURAGE
			IF bIgnoreAllCheck
				RETURN g_FMMC_STRUCT.biVSAvailableOutfit[iSetup][0] != 0
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		CASE VS_STYLE_HIDDEN
			IF bIgnoreAllCheck
				IF g_FMMC_STRUCT.biVSAvailableOutfit[iSetup][0] != 0
				OR g_FMMC_STRUCT.biVSAvailableOutfit[iSetup][1] != 0
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
				
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	PRINTLN("[CORONA] IS_CORONA_MISSION_OUTFIT_SETUP_VALID - BitSet = ", IS_CORONA_BIT_SET(CORONA_ALLOW_ALL_OUTFITS), ", iSetup = ", iSetup)
	
	IF NOT bIgnoreAllCheck
		// If all outfits are available for old missions, return TRUE
		IF IS_CORONA_BIT_SET(CORONA_ALLOW_ALL_OUTFITS)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// If all future outfits have been set for the style we should allow them to be selectable
	IF IS_BIT_SET(g_FMMC_STRUCT.biVSAllowFutureOutfits, iSetup)
		RETURN TRUE
	ENDIF
	
	// If there are some outfits available then allow this category to be selected.
	IF g_FMMC_STRUCT.biVSAvailableOutfit[iSetup][0] != 0
	OR g_FMMC_STRUCT.biVSAvailableOutfit[iSetup][1] != 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CORONA_OUTFIT_OPTION_AVAILABLE_TO_HOST(INT iOutfitStyle)
		
	SWITCH iOutfitStyle
		CASE VS_STYLE_HIDDEN
		CASE VS_STYLE_SOLO
			RETURN FALSE
	ENDSWITCH	
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Check the creator data to see if we are handling an old vs mission. If so, all outfits should be available by default
PROC SET_CORONA_MISSION_OUTFIT_OPTION()
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_VERSUS
		OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_COOP
			
			INT iOutfitStyle
			
			#IF IS_DEBUG_BUILD
			PRINTLN("[CORONA] ****** Mission Outfit Summary *******")
			PRINTLN("[CORONA] 		Default Style: ", DEBUG_GET_CORONA_OUTFIT_STYLE_NAME(g_FMMC_STRUCT.iVSDefaultStyleClass))
			REPEAT MAX_VS_STYLES iOutfitStyle
				PRINTLN("[CORONA] 			Style: ", DEBUG_GET_CORONA_OUTFIT_STYLE_NAME(iOutfitStyle))
				PRINTLN("[CORONA] 				- Available		: ", g_FMMC_STRUCT.biVSAvailableOutfit[iOutfitStyle][0])
				PRINTLN("[CORONA] 				- Available		: ", g_FMMC_STRUCT.biVSAvailableOutfit[iOutfitStyle][1])
				PRINTLN("[CORONA] 				- Default		: ", g_FMMC_STRUCT.iVSDefaultOutfit[iOutfitStyle])
				PRINTLN("[CORONA] 				- Allow Future	: ", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT.biVSAllowFutureOutfits, iOutfitStyle), "TRUE", "FALSE"))
				PRINTLN("[CORONA] *****************************************************")
			ENDREPEAT
			REPEAT FMMC_MAX_TEAMS iOutfitStyle
				PRINTLN("[CORONA] 			Team: ", iOutfitStyle)
				PRINTLN("[CORONA] 				- BitSet: ", g_FMMC_STRUCT.sFMMCEndConditions[iOutfitStyle].iTeamBitset2)
				PRINTLN("[CORONA] 				- Beast Override: ", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iOutfitStyle].iTeamBitset2, ciBS2_ENABLE_BEAST_OUTFIT), "TRUE", "FALSE"))
				PRINTLN("[CORONA] *****************************************************")
			ENDREPEAT
			
			#ENDIF
					
			// Look at creator data (If all data is unset (ie. bitsets = 0) then allow all outfits. This is for old VS missions)
			SET_CORONA_BIT(CORONA_ALLOW_ALL_OUTFITS)
			
			INT iTotalOutfitsSet
			// Loop over each style and see if the creator has set any info on the outfits
			REPEAT MAX_VS_STYLES iOutfitStyle
			
				IF IS_CORONA_MISSION_OUTFIT_SETUP_VALID(iOutfitStyle, TRUE)
					
					IF IS_CORONA_OUTFIT_OPTION_AVAILABLE_TO_HOST(iOutfitStyle)
						iTotalOutfitsSet++
					ENDIF
					
					CLEAR_CORONA_BIT(CORONA_ALLOW_ALL_OUTFITS)
					
					PRINTLN("[CORONA] SET_PLAYER_CORONA_DEFAULT_TEAM_OUTFIT - Outfit data from cloud. Style = ", DEBUG_GET_CORONA_OUTFIT_STYLE_NAME(iOutfitStyle), ", Value = [0] = ", g_FMMC_STRUCT.biVSAvailableOutfit[iOutfitStyle][0], " [1] = ", g_FMMC_STRUCT.biVSAvailableOutfit[iOutfitStyle][1])
				ENDIF	
			ENDREPEAT
			
			IF iTotalOutfitsSet = 1
				SET_CORONA_BIT(CORONA_ONE_OUTFIT_ACTIVE_FOR_MISSION)
				PRINTLN("[CORONA] SET_PLAYER_CORONA_DEFAULT_TEAM_OUTFIT - Only one outfit style available. Lock: CORONA_ONE_OUTFIT_ACTIVE_FOR_MISSION")
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Temp command to query if mission is a contact mission or not (faking mission data struct)
FUNC BOOL IS_THIS_CORONA_A_CONTACT_MISSION(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)
	
	//PRINTLN("[CORONA] IS_THIS_CORONA_A_CONTACT_MISSION - Contact Mission: ", PICK_STRING(sLaunchMissionDetails.bIsContactMission, "TRUE", "FALSE"))
	
	RETURN sLaunchMissionDetails.bIsContactMission
ENDFUNC

/// PURPOSE: Returns TRUE if this is a foot race
FUNC BOOL IS_THIS_A_FOOT_RACE(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)
	IF sLaunchMissionDetails.iMissionType = FMMC_TYPE_RACE
		IF g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_ON_FOOT 
		OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_ON_FOOT_P2P
			//PRINTLN("IS_THIS_A_FOOT_RACE") //removed as it spams for LBs.
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// ******** VEHICLE SELECTION IN CORONA *********

/// PURPOSE:
///    Returns the default mission vehicle class from the creator if valid
FUNC INT GET_CORONA_DEFAULT_MISSION_VEHICLE_CLASS(INT iTeam = 0, INT iRole = 0)

	IF g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeam] != -1

		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
		AND iRole != -1
			IF (SUM_BITS(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][iRole][g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[iTeam][iRole]][0]) + SUM_BITS(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][iRole][g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[iTeam][iRole]][1])) != 0
				PRINTLN("[CORONA] GET_CORONA_DEFAULT_MISSION_VEHICLE_CLASS (ROLES) - Team: ", iTeam, " Default vehicle class: ", g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[iTeam][iRole])
				RETURN g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[iTeam][iRole]
			ENDIF
		ELSE
			IF (SUM_BITS(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeam]][0]) + SUM_BITS(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeam]][1])) != 0
				PRINTLN("[CORONA] GET_CORONA_DEFAULT_MISSION_VEHICLE_CLASS - Team: ", iTeam, " Default vehicle class: ", g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeam])
				RETURN g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeam]
			ENDIF			
		ENDIF
	ENDIF

	RETURN -1
ENDFUNC


FUNC BOOL CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION(BOOL bForceGTARace = FALSE, BOOL bReturnOnEqual = FALSE, INT iTotalCustomVehicles = 0)
	
	INT iClass = g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS]
	
	INT i
	INT iCount = 0
	MODEL_NAMES eModel
	
	IF IS_SPECIAL_VEHICLE_RACE()
	OR IS_WEAPONISED_VEHICLE_RACE()
		PRINTLN("CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION - bReturnOnEqual = TRUE - Special or weaponised")
		bReturnOnEqual = TRUE
	ENDIF
	
	IF DOES_JOB_USE_RACE_VEHICLE_SELECTION(IS_RACE())
	
		IF g_bArenaWarsUseForcedStockImperator
			RETURN FALSE
		ENDIF
	
		#IF FEATURE_TUNER
		IF IS_THIS_A_TUNER_ROBBERY_FINALE()
		AND iTotalCustomVehicles = 0
			PRINTLN("CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION - Generic Job - Tuner robbery and player owns no custom vehicles. Cannot change option.")
			RETURN FALSE
		ENDIF
		#ENDIF
	
		INT iTeam = GET_MY_GENERIC_JOB_TEAM()
		IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_RACE
			iClass = GET_CORONA_DEFAULT_MISSION_VEHICLE_CLASS(iTeam)
		ENDIF
		REPEAT ciMAX_VEHICLES_PER_CREATOR_LIBRARY i
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass)][GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
				iCount += 1
			ENDIF
		ENDREPEAT
		
		PRINTLN("CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION - Generic Job - iCount before custom vehicles = ", iCount)		
		iCount += iTotalCustomVehicles
		PRINTLN("CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION -Generic Job - iCount after custom vehicles = ", iCount)
		
		IF iCount > 1
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	FOR i = 0 TO (GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)-1) STEP 1
	
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableVehicleBitSet[iClass], i)
		
			eModel = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i, DEFAULT, TRUE)
				
			IF eModel = HOTKNIFE
			OR eModel = CARBONRS
			OR eModel = KHAMELION
				IF IS_COLLECTORS_EDITION_GAME()
					iCount += 1
				ENDIF
			ELSE
				IF IS_THIS_A_GTA_RACE_TEMP(g_sTransitionSessionOptions)
				OR bForceGTARace
					IF IS_CORONA_VEHICLE_VALID_FOR_GTA_RACE(eModel)
						iCount += 1
					ENDIF
				ELSE
					iCount += 1
				ENDIF
			ENDIF
			
			// If we have more than 1 vehicle then it's not locked
			IF iCount > 1
			OR (bReturnOnEqual AND iCount >= 1)
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDFOR
	
	// Check our DLC vehicles
	INT iDLCMax = GET_SELECTION_MAX_DLC_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)
//	PRINTLN("CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION - iDLCMax = ", iDLCMax)
	FOR i = 0 TO (iDLCMax-1) STEP 1
		
		IF DOES_CREATOR_ALLOW_DLC_VEHICLE(iClass, i)
		
			eModel = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i)
		
//			PRINTLN("CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION - creator allows dlc vehicle with model ", ENUM_TO_INT(eModel))
		
			IF IS_VEHICLE_AVAILABLE_FOR_GAME(eModel, FALSE)
//				PRINTLN("CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION - it is available for game")
				IF IS_THIS_A_GTA_RACE_TEMP(g_sTransitionSessionOptions)
				OR bForceGTARace
					IF IS_CORONA_VEHICLE_VALID_FOR_GTA_RACE(eModel)
						iCount += 1
					ENDIF
				ELSE
					iCount += 1
				ENDIF
			ENDIF
//		ELSE
//			PRINTLN("CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION - creator does not allow vehicle ", i, " in class ", iClass)
		ENDIF
		
		IF iCount > 1
		OR (bReturnOnEqual AND iCount >= 1)
			RETURN TRUE
		ENDIF			
	ENDFOR
	
	// Only count custom vehicles for premium races, as these can only be launched with custom vehicles
	IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
	OR IS_CUSTOM_VEHICLE_TURNED_ON_FOR_COORNA(g_sTransitionSessionOptions)
		PRINTLN("CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION - iCount  [1] = ", iCount)
		PRINTLN("CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION - iTotalCustomVehicles = ", iTotalCustomVehicles)
		iCount += iTotalCustomVehicles
	ENDIF
	
	PRINTLN("CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION - iCount [2] = ", iCount)
	
	IF iCount > 1
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Determines if the player's vehicle (local or saved) is RANDOM
FUNC BOOL IS_CHOSEN_VEHICLE_RANDOM(INT iClass, PLAYER_INDEX playerID, BOOL bLocal = FALSE, INT iTotalCustomVehicles = 0)

	IF IS_PLAYER_SCTV(playerID)
		RETURN FALSE
	ENDIF

	IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), bLocal)
	OR IS_PLAYER_SELECTING_DLC_VEHICLE()
		RETURN FALSE
	ENDIF
	
	IF DOES_JOB_USE_RACE_VEHICLE_SELECTION(IS_RACE())
		RETURN FALSE
	ENDIF

	INT iMaxChoice = GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)

	IF GET_PLAYER_CORONA_VEHICLE(playerID, bLocal) = iMaxChoice
	AND CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION(DEFAULT, DEFAULT, iTotalCustomVehicles)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Has player confirmed their vehicle with X
FUNC BOOL HAS_PLAYER_CONFIRMED_CORONA_VEHICLE()

	IF IS_CHOSEN_VEHICLE_RANDOM(g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS], PLAYER_ID(), TRUE)
		RETURN TRUE
	ENDIF

	RETURN IS_CORONA_BIT_SET(CORONA_HAS_PLAYER_SAVED_VEHICLE)
ENDFUNC


/// PURPOSE: Returns FALSE if we are a co-driver on this screen
FUNC BOOL IS_CORONA_VEHICLE_SELECTION_AVAILABLE()
	
	// If the player is SCTV they will not see the options on vehicle screen
	IF IS_PLAYER_SCTV(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	// If player is a co-driver they will not see the options on vehicle screen
	IF IS_THIS_A_RALLY_RACE_TEMP(g_sTransitionSessionOptions)
	OR IS_THIS_CORONA_A_GTA_TEAM_RACE(g_sTransitionSessionOptions)
	OR IS_TARGET_ASSAULT_RACE()
		
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole = 1
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns TRUE if the vehicle / colour can be randomised
FUNC BOOL IS_CORONA_RANDOM_VEHICLE_SETUP_AVAILABLE(INT iTotalCustomVehicles = 0)

	IF ((CV2_IS_THIS_CORONA_A_PROFESSIONAL_JOB())
	AND iTotalCustomVehicles = 1)
		RETURN FALSE
	ENDIF
	
	IF IS_TARGET_ASSAULT_RACE()
		RETURN FALSE
	ENDIF
	
	IF IS_SPECIAL_VEHICLE_RACE()
		RETURN FALSE
	ENDIF
	
	IF DOES_JOB_USE_RACE_VEHICLE_SELECTION()
		RETURN FALSE
	ENDIF	

	// If we are not a co-driver and the vehicle optin can be changed we can choose to randomise
	IF IS_CORONA_VEHICLE_SELECTION_AVAILABLE()
	AND CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION(DEFAULT, DEFAULT, iTotalCustomVehicles)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CHOSEN_VEHICLE_SELECTED(CORONA_MENU_DATA &coronaMenuData)

	IF GET_PLAYER_CORONA_VEHICLE(PLAYER_ID(), TRUE) != GET_PLAYER_CORONA_VEHICLE(PLAYER_ID(), FALSE)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE) !=  IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), FALSE)
		RETURN FALSE
	ENDIF
	
	IF GET_PLAYER_CORONA_VEHICLE_NAME(PLAYER_ID(), TRUE) != GET_PLAYER_CORONA_VEHICLE_NAME(PLAYER_ID(), FALSE)
		RETURN FALSE
	ENDIF

	IF coronaMenuData.iSelectedColourType != g_iMyRaceColorType
	OR coronaMenuData.iSelectedColourIndex != g_iMyRaceColorSelection
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the mission has a sudden death option. Based off 2 creator bits being set
FUNC BOOL IS_CORONA_MISSION_SUDDEN_DEATH_AVAILABLE()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_NO_ENEMY_TEAMS)
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_MORE_PEOPLE_ARE_IN_LOC)
		RETURN TRUE	
	ENDIF
		
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Returns TRUE if the mission has a Pickup option.
FUNC BOOL IS_CORONA_MISSION_PICKUP_OPTION_AVAILABLE()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciRANDOM_PICKUP_TYPES_OPTION)
		RETURN TRUE	
	ENDIF
		
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if there are valid players in your last job list
FUNC BOOL IS_CORONA_LAST_JOB_AVAILABLE()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
			
		IF IS_GAMER_HANDLE_VALID(lastJobPlayers.Gamers[i])
			RETURN TRUE
		ENDIF
		
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if there are valid players in your heist job list
FUNC BOOL IS_CORONA_LAST_HEIST_AVAILABLE()

	IF NOT IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
	AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
		RETURN FALSE
	ENDIF

	INT i
	REPEAT NUM_NETWORK_PLAYERS i
			
		IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
			IF IS_GAMER_HANDLE_VALID(lastGangOpsHeistPlayers.Gamers[i])
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_GAMER_HANDLE_VALID(lastHeistPlayers.Gamers[i])
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


FUNC STRING GET_CORONA_INVITE_HEADER_STRING()
	SWITCH GET_CORONA_STATUS()
		CASE CORONA_STATUS_JOINED_PLAYERS		RETURN "PM_SCR_JOP"
		CASE CORONA_STATUS_LOCAL_PLAYERS		RETURN "PM_SCR_IVP"	
		CASE CORONA_STATUS_LAST_JOB_PLAYERS 	RETURN "PM_SCR_ILJ"
		CASE CORONA_STATUS_FRIENDS				RETURN "PM_SCR_IFR"
		CASE CORONA_STATUS_CREW_MEMBERS			RETURN "PM_SCR_ICM"
		CASE CORONA_STATUS_MATCHED_PLAYERS		RETURN "PM_SCR_IMP"
		CASE CORONA_STATUS_LAST_HEIST_PLAYERS	RETURN "PM_SCR_ILH"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC INT GET_CORONA_LOBBY_START_INDEX()
	IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
		RETURN ciCORONA_LOBBY_CREW_MEMBERS
	ELSE
		RETURN ciCORONA_LOBBY_ALL_LOCAL_PLAYERS
	ENDIF
ENDFUNC

FUNC BOOL IS_CORONA_TUTORIAL_PED_HEADSHOT_READY(CORONA_MENU_DATA &coronaMenuData)

	IF IS_CORONA_BIT_SET(CORONA_TUTORIAL_PED_HEADSHOT_LOADED)
		
		IF IS_PEDHEADSHOT_VALID(coronaMenuData.tutorialPedHeadshot)
			IF IS_PEDHEADSHOT_READY(coronaMenuData.tutorialPedHeadshot)
				RETURN TRUE
			ENDIF
		ELSE
			CLEAR_CORONA_BIT(CORONA_TUTORIAL_PED_HEADSHOT_LOADED)
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE: Returns total players standing in corona (that we can scroll to) 
FUNC INT GET_TOTAL_PLAYERS_IN_CORONA(CORONA_MENU_DATA &coronaMenuData)

	INT iTotal = coronaMenuData.iTotalPlayersInCorona
	
	// *************** NEW TRANSITION SESSION BEHAVIOUR **************
	iTotal = GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION()
	
	// If player in tutorial add the AI ped
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		iTotal++
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_bDebugTotalPlayersInCorona
		iTotal = g_iDebugTotalPlayersInCorona
	ENDIF
	#ENDIF

	RETURN iTotal
ENDFUNC

/// PURPOSE: Returns total players standing in corona (that we can scroll to) 
FUNC INT GET_TOTAL_PLAYERS_IN_CORONA_OR_JOINING(CORONA_MENU_DATA &coronaMenuData)

	INT iTotal = coronaMenuData.iTotalPlayersInCorona
	
	// *************** NEW TRANSITION SESSION BEHAVIOUR **************
	iTotal = GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION()
	
	// If player in tutorial add the AI ped
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		iTotal++
	ENDIF
	
	// Check our 'Joining' players from last job
	iTotal += g_CoronaEndOfJobPlayers.iNumberOfEndOfJobPlayers	

	RETURN iTotal
ENDFUNC

/// PURPOSE: Returns total players standing in corona (that we can scroll to) 
FUNC INT GET_TOTAL_PLAYERS_READY_IN_CORONA()

	INT iTotal
	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
	
		IF IS_GAMER_HANDLE_VALID(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].hGamer)
		
			IF IS_TRANSITION_PLAYER_READY_TO_LAUNCH_JOB(i)
			OR i = GET_MY_TRANSITION_SESSION_ARRAY_POS()
			OR IS_THIS_TRANSITION_SESSION_PLAYER_ON_CALL(i)
			
				iTotal++
				
			ENDIF
		ENDIF
	
	ENDREPEAT
	
	
	// If player in tutorial add the AI ped
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		iTotal++
	ENDIF
	
	// Catch for SCTV player. If it is 0 we know we had a linked player
	// to get in this stage so return the minimu of 1
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF iTotal <= 0
			iTotal = 1
		ENDIF
	ENDIF

	RETURN iTotal
ENDFUNC

//Has the player progressed past the download data stage of the corona
FUNC  BOOL HAS_PLAYER_PROGRESSED_IN_CORONA_PAST_DOWNLOAD(PLAYER_INDEX playerID)
	SWITCH GET_PLAYER_CORONA_STATUS(playerID) 
		CASE CORONA_STATUS_CAN_START			
		CASE CORONA_STATUS_WALK_IN				
		CASE CORONA_STATUS_JOIN_HOST					
		CASE CORONA_STATUS_MatC_HAND_SHAKE			
		CASE CORONA_STATUS_INIT_CORONA		
		CASE CORONA_STATUS_INIT_TRAN_SCRIPT
		CASE CORONA_STATUS_GET_DATA_SETUP_TRAN
		CASE CORONA_STATUS_SETUP_VARS		
			RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_PLAYERS_JOINING_CORONA(BOOL &bPlayerLeaving)

	INT iLoop 
	
	INT iMyVote				= GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iVoteToUse
	INT missionUniqueID		= Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	INT iBS_ReservedPlayers	= Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission(missionUniqueID)
	INT iBS_JoininPlayers	= Get_Bitfield_Of_Players_On_Or_Joining_Mission(missionUniqueID)
	
	bPlayerLeaving = FALSE
	
	PLAYER_INDEX playerID
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iLoop
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLoop))
			playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLoop))
			//...check player is ok
			IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
			AND NOT IS_THIS_PLAYER_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA(playerID)
				//...Is the player reserved for the mission or on same vote
				IF IS_BIT_SET(iBS_ReservedPlayers, NATIVE_TO_INT(playerID))
				OR GlobalplayerBD_FM_2[NATIVE_TO_INT(playerID)].iVoteToUse = iMyVote
					
					// If the player is not yet active in corona they are still joining
					IF NOT IS_THIS_PLAYER_FULLY_ACTIVE_IN_CORONA(playerID)
						PRINTLN("PART ", iLoop, " is still loading into corona Player - iBS_ReservedPlayers = ", GET_PLAYER_NAME(playerID))
						
						IF (GET_PLAYER_CORONA_STATUS(playerID) >= CORONA_STATUS_KICKED AND GET_PLAYER_CORONA_STATUS(playerID) <= CORONA_STATUS_WALK_OUT)
						OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_QUIT
							PRINTLN("PART ", iLoop, " is still loading into corona Player - is leaving")
							bPlayerLeaving = TRUE
						ENDIF
						
						RETURN TRUE
					ENDIF
					
				ELIF IS_BIT_SET(iBS_JoininPlayers, NATIVE_TO_INT(playerID)) 
					// If the player is not yet active in corona they are still joining
					IF NOT IS_THIS_PLAYER_FULLY_ACTIVE_IN_CORONA(playerID)
						PRINTLN("PART ", iLoop, " is still loading into corona Player - iBS_JoininPlayers = ", GET_PLAYER_NAME(playerID))
						
						IF (GET_PLAYER_CORONA_STATUS(playerID) >= CORONA_STATUS_KICKED AND GET_PLAYER_CORONA_STATUS(playerID) <= CORONA_STATUS_WALK_OUT)
						OR GET_PLAYER_CORONA_STATUS(playerID) = CORONA_STATUS_QUIT
							PRINTLN("PART ", iLoop, " is still loading into corona Player - is leaving")
							bPlayerLeaving = TRUE
						ENDIF
						
						RETURN TRUE
					ENDIF				
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC

/// PURPOSE: Set the lobby as open or closed
PROC UPDATE_CORONA_MATCHMAKING_STATUS(INT iMissionType, BOOL bOpen, BOOL bSetTelemetry = TRUE)

	// If we are using new sessioning, make lobby open / closed
	IF SHOULD_TRANSITION_SESSION_RUN(iMissionType)
	
		PRINTLN("[CORONA]UPDATE_CORONA_MATCHMAKING_STATUS - bOpen: ", bOpen)
		INT uniqueID = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	
		IF bOpen
		AND NOT IS_CORONA_IN_CHALLENGE_MODE()
			IF bSetTelemetry
				SET_JOB_VISIBILITY_IN_CORONA_TRANSITION_SESSION_FOR_TELEMETRY(TRUE)
			ENDIF
			IF NOT NETWORK_IS_TRANSITION_OPEN_TO_MATCHMAKING()
				PRINTLN("[TS] UPDATE_CORONA_MATCHMAKING_STATUS - bOpen = TRUE - NETWORK_OPEN_TRANSITION_MATCHMAKING() called")
				DEBUG_PRINTCALLSTACK()
				
				IF NETWORK_IS_TRANSITION_VISIBILITY_LOCKED()
					NETWORK_SET_TRANSITION_VISIBILITY_LOCK(FALSE, FALSE)
				ENDIF
				IF NOT IS_CORONA_BIT_SET(CORONA_KEEP_WALK_IN_MATCHMAKING_OPEN)
					Broadcast_Corona_Matchmaking_Open(uniqueID)
				ENDIF
				
				NETWORK_OPEN_TRANSITION_MATCHMAKING()
			
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[TS] UPDATE_CORONA_MATCHMAKING_STATUS - bOpen = TRUE - NETWORK_OPEN_TRANSITION_MATCHMAKING() NOT CALLED AS IT'S ALREADY OPEN")
			#ENDIF
			ENDIF
		ELSE
			IF bSetTelemetry
				SET_JOB_VISIBILITY_IN_CORONA_TRANSITION_SESSION_FOR_TELEMETRY(FALSE)
			ENDIF
			IF NETWORK_IS_TRANSITION_OPEN_TO_MATCHMAKING()
		
				PRINTLN("[TS] UPDATE_CORONA_MATCHMAKING_STATUS - bOpen = FALSE - NETWORK_CLOSE_TRANSITION_MATCHMAKING() called")
				DEBUG_PRINTCALLSTACK()
				
				IF NOT IS_CORONA_BIT_SET(CORONA_KEEP_WALK_IN_MATCHMAKING_OPEN)
					Broadcast_Corona_Matchmaking_Closed(uniqueID)
				ENDIF
				
				NETWORK_CLOSE_TRANSITION_MATCHMAKING()
				
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[TS] UPDATE_CORONA_MATCHMAKING_STATUS - bOpen = FALSE - NETWORK_CLOSE_TRANSITION_MATCHMAKING() NOT CALLED AS IT'S ALL READY CLOSED")
			#ENDIF
			ENDIF
		ENDIF

	ENDIF
ENDPROC


FUNC BOOL SET_TRANSITION_PLAYER_BIT_FOR_OWNED_VEHICLES_OF_CLASS(INT &iTransitionBit, BOOL bInGarageSlot = TRUE, BOOL bCheckAvailablePastCurrentTime = TRUE)
	INT i, j

	i = 0
	REPEAT MAX_MP_SAVED_VEHICLES i
		IF g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
		AND IS_VEHICLE_AVAILABLE_FOR_GAME(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel, bCheckAvailablePastCurrentTime)
		AND	IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel)
		AND (NOT bInGarageSlot
		OR (bInGarageSlot AND DOES_VEHICLE_SLOT_BELONG_IN_ANY_GARAGE(i)))
		AND NOT IS_PERSONAL_VEHICLE_BLOCKED_BY_CREATOR(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel)
			REPEAT FMMC_VEHICLE_CLASS_MAX j
				IF MP_DOES_VEHICLE_BELONG_TO_CLASS(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel,j)
					iTransitionBit = (j + ciTRANSITION_SESSIONS_PBS_CUSTOM_COMPACTS)
					PRINTLN("[CORONA] DOES_PLAYER_OWN_ANY_SAVED_VEHICLES_OF_CLASS - Local player has custom vehicle for class bit: ", iTransitionBit, " (Class #): ", j)
					SET_TRANSITION_PLAYER_BIT(iTransitionBit)
					j = FMMC_VEHICLE_CLASS_MAX-1
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_CUSTOM_SPECIAL_VEHICLE(BOOL bInGarageSlot = TRUE, BOOL bCheckAvailablePastCurrentTime = TRUE)
	INT i
	
	REPEAT MAX_MP_SAVED_VEHICLES i
		IF g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
		AND IS_VEHICLE_AVAILABLE_FOR_GAME(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel, bCheckAvailablePastCurrentTime)
		AND	IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel)
		AND (NOT bInGarageSlot
		OR (bInGarageSlot AND DOES_VEHICLE_SLOT_BELONG_IN_ANY_GARAGE(i)))
		AND NOT IS_PERSONAL_VEHICLE_BLOCKED_BY_CREATOR(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel)
			IF MP_DOES_VEHICLE_BELONG_TO_CLASS(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel, FMMC_VEHICLE_CLASS_SPECIAL)
			OR MP_DOES_VEHICLE_BELONG_TO_CLASS(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel, FMMC_VEHICLE_CLASS_WEAPONISED)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_CUSTOM_CONTENDER_VEHICLE(BOOL bInGarageSlot = TRUE, BOOL bCheckAvailablePastCurrentTime = TRUE)
	INT i
	
	REPEAT MAX_MP_SAVED_VEHICLES i
		IF g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
		AND IS_VEHICLE_AVAILABLE_FOR_GAME(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel, bCheckAvailablePastCurrentTime)
		AND	IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel)
		AND (NOT bInGarageSlot
		OR (bInGarageSlot AND DOES_VEHICLE_SLOT_BELONG_IN_ANY_GARAGE(i)))
		AND NOT IS_PERSONAL_VEHICLE_BLOCKED_BY_CREATOR(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel)
			IF MP_DOES_VEHICLE_BELONG_TO_CLASS(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.eModel, FMMC_VEHICLE_CLASS_CONTENDER)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Called by player entering corona to set all bits where they have a custom vehicle
PROC SET_CORONA_CUSTOM_VEHICLE_AVAILABILITY()
	INT iTransitionBit
	
	SET_TRANSITION_PLAYER_BIT_FOR_OWNED_VEHICLES_OF_CLASS(iTransitionBit, FALSE)
	
	IF IS_TARGET_ASSAULT_RACE()
		IF DOES_PLAYER_OWN_CUSTOM_SPECIAL_VEHICLE(FALSE)
			SET_BIT(g_TransitionSessionNonResetVars.sTransVars.iTransitionSessionLocalBitSet_2, ciTRANSITION_SESSIONS_PBS_2_HAS_CUSTOM_SPECIAL_VEHICLE)
			SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYER_BIT_SET_2, g_TransitionSessionNonResetVars.sTransVars.iTransitionSessionLocalBitSet_2)
		ENDIF
//	ELIF IS_ARENA_WARS_JOB()
//		IF DOES_PLAYER_OWN_CUSTOM_CONTENDER_VEHICLE()
//			SET_BIT(g_TransitionSessionNonResetVars.sTransVars.iTransitionSessionLocalBitSet_2, ciTRANSITION_SESSIONS_PBS_2_HAS_CUSTOM_SPECIAL_VEHICLE)
//			SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYER_BIT_SET_2, g_TransitionSessionNonResetVars.sTransVars.iTransitionSessionLocalBitSet_2)
//		ENDIF
	ENDIF
	
	SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYER_BIT_SET, g_TransitionSessionNonResetVars.sTransVars.iTransitionSessionLocalBitSet)
ENDPROC

/// PURPOSE: Checks if player passed (transition array) has custom vehicle for class  
FUNC BOOL DOES_TRANSITION_PLAYER_HAVE_CUSTOM_VEHICLE_FOR_CLASS(INT iPlayerArrayPos, INT iClass)
	
	IF IS_TARGET_ASSAULT_RACE()
	AND (iClass = FMMC_VEHICLE_CLASS_SPECIAL
	OR iClass = FMMC_VEHICLE_CLASS_WEAPONISED)
		PRINTLN("[CORONA] DOES_TRANSITION_PLAYER_HAVE_CUSTOM_VEHICLE_FOR_CLASS - TARGET ASSAULT RACE - For class: ", iClass, " for player: ", iPlayerArrayPos)
		PRINTLN("[CORONA] DOES_TRANSITION_PLAYER_HAVE_CUSTOM_VEHICLE_FOR_CLASS - TARGET ASSAULT RACE - Does player have custom vehicle? ", 
			GET_STRING_FROM_BOOL(IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iPlayerArrayPos].iTranPlayerBitSet_2, ciTRANSITION_SESSIONS_PBS_2_HAS_CUSTOM_SPECIAL_VEHICLE)))
		RETURN IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iPlayerArrayPos].iTranPlayerBitSet_2, ciTRANSITION_SESSIONS_PBS_2_HAS_CUSTOM_SPECIAL_VEHICLE)
	
//	ELIF IS_ARENA_WARS_JOB()
//	AND iClass = FMMC_VEHICLE_CLASS_CONTENDER
//		PRINTLN("[CORONA] DOES_TRANSITION_PLAYER_HAVE_CUSTOM_VEHICLE_FOR_CLASS - ARENA RACE - For class: ", iClass, " for player: ", iPlayerArrayPos)
//		PRINTLN("[CORONA] DOES_TRANSITION_PLAYER_HAVE_CUSTOM_VEHICLE_FOR_CLASS - ARENA RACE - Does player have custom vehicle? ", 
//			GET_STRING_FROM_BOOL(IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iPlayerArrayPos].iTranPlayerBitSet_2, ciTRANSITION_SESSIONS_PBS_2_HAS_CUSTOM_SPECIAL_VEHICLE)))
//		RETURN IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iPlayerArrayPos].iTranPlayerBitSet_2, ciTRANSITION_SESSIONS_PBS_2_HAS_CUSTOM_SPECIAL_VEHICLE)
	ENDIF
	
	INT iBitSet = (iClass + ciTRANSITION_SESSIONS_PBS_CUSTOM_COMPACTS)
	
	PRINTLN("[CORONA] DOES_TRANSITION_PLAYER_HAVE_CUSTOM_VEHICLE_FOR_CLASS - For class bit: ", iBitSet, " for player: ", iPlayerArrayPos)
	PRINTLN("[CORONA] DOES_TRANSITION_PLAYER_HAVE_CUSTOM_VEHICLE_FOR_CLASS - Does player have custom vehicle? ", PICK_STRING(IS_THIS_TRANSITION_PLAYER_BIT_SET(iPlayerArrayPos, iBitSet), "TRUE", "FALSE"))
	
	RETURN IS_THIS_TRANSITION_PLAYER_BIT_SET(iPlayerArrayPos, iBitSet)
ENDFUNC


/// PURPOSE: Returns the correct text label for the outfit of the player
FUNC STRING GET_PLAYER_CORONA_ACCESS_LEVEL_TEXT(INT iOutfitAccessLevel)
	SWITCH iOutfitAccessLevel
		CASE ci_HEIST_CORONA_OUTFIT_ACCESS_LEADER 	RETURN "HEIST_OF_LDR"
		CASE ci_HEIST_CORONA_OUTFIT_ACCESS_MASKS 	RETURN "HEIST_OF_MASK"
		CASE ci_HEIST_CORONA_OUTFIT_ACCESS_OUTFITS 	
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciLOCK_STYLE_ALL_TEAMS)
				RETURN "HEIST_OF_CREW2"
			ELSE
				RETURN "HEIST_OF_CREW"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Returns the current value for the outfit access level
FUNC INT GET_CORONA_OUTFIT_ACCESS_LEVEL()
	RETURN g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_OUTFIT_CUSTOMISATION]	
ENDFUNC

/// PURPOSE:
///    Returns the multiplier for the heist difficulty level
FUNC FLOAT GET_CORONA_HEIST_DIFFICULTY_MULTIPLIER(INT iDifficulty)
	
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
		SWITCH iDifficulty
			CASE 1	RETURN g_sMPTunables.fGangops_difficulty_normal
			CASE 2	RETURN g_sMPTunables.fGangops_difficulty_hard
		ENDSWITCH
	ENDIF
	
	SWITCH iDifficulty
		CASE 0	RETURN g_sMPTunables.fheist_difficulty_easy
		CASE 2	RETURN g_sMPTunables.fheist_difficulty_hard
	ENDSWITCH
	
	RETURN -1.0
ENDFUNC


// *********************
// 	Corona Music Logic
// *********************

FUNC INT GET_RADIO_STATION_INDEX_FROM_HASH(INT iHash)
	INT i
	REPEAT GET_NUM_UNLOCKED_RADIO_STATIONS() i
		PRINTLN("GET_RADIO_STATION_INDEX_FROM_HASH - i = ", i, " iHash = ", iHash, " Radio Name", GET_RADIO_STATION_NAME(i))
		IF iHash = GET_HASH_KEY(GET_RADIO_STATION_NAME(i))
			PRINTLN(" Found Radio - ", GET_RADIO_STATION_NAME(i))
			RETURN i
		ENDIF
	ENDREPEAT
	
	PRINTLN("GET_RADIO_STATION_INDEX_FROM_HASH - GET_NUM_UNLOCKED_RADIO_STATIONS = ", GET_NUM_UNLOCKED_RADIO_STATIONS())
	PRINTLN("GET_RADIO_STATION_INDEX_FROM_HASH - iIndex = -1")
	RETURN -1
ENDFUNC

PROC BEGIN_CORONA_RADIO()

	PRINTLN("[CORONA] BEGIN_CORONA_RADIO - beginning radio T(", GET_CLOUD_TIME_AS_INT(), ")")

	SET_MOBILE_PHONE_RADIO_STATE(TRUE)
	SET_AUDIO_FLAG("MobileRadioInGame", TRUE)

	IF DOES_RACE_HAVE_A_RADIO_MIX()
	
		PROCESS_RACE_RADIO_MIX(FALSE, TRUE)
		g_iMyChosenRadioStation = GET_RADIO_STATION_INDEX_FROM_HASH(GET_HASH_KEY("RADIO_36_AUDIOPLAYER"))
		PRINTLN("[CORONA] BEGIN_CORONA_RADIO - USE RADIO MIX - Radio Station g_iMyChosenRadioStation = ", g_iMyChosenRadioStation)
	
	ELSE
				
		INT iIndex 	//BACK TO HASH FOR BUG 1658309	 //INT iIndex = g_FMMC_STRUCT.iMusic	//NEIL NOW STORES THE INDEX INSTEAD OF THE HASH
		INT iHash = g_FMMC_STRUCT.iMusic
		iIndex =  GET_RADIO_STATION_INDEX_FROM_HASH(iHash)
				
		PRINTLN("[CORONA] BEGIN_CORONA_RADIO - iHash = ", iHash, ", Index = ", iIndex)
				
		IF iIndex = -1
			g_iMyChosenRadioStation = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_RADIO)
			SET_RADIO_TO_MY_CHOSEN()
			PRINTLN("[CORONA] BEGIN_CORONA_RADIO - USE DEFAULT - Radio Station g_iMyChosenRadioStation = ", g_iMyChosenRadioStation)
		ELSE
			g_iMyChosenRadioStation = iIndex
			SET_RADIO_TO_STATION_INDEX(iIndex)
			PRINTLN("[CORONA] BEGIN_CORONA_RADIO - USE CREATOR SET - Radio Station Index = ", iIndex)
		ENDIF
		
	ENDIF
		
	IF IS_PLAYER_USING_ARENA()
		TRIGGER_MUSIC_EVENT("AW_FADE_IN_RADIO")
	ELSE
		TRIGGER_MUSIC_EVENT("MP_RADIO_FADE_IN")
	ENDIF
	
ENDPROC

//PURPOSE: Initialise any music we need for corona
PROC SET_CORONA_MUSIC()
	IF NOT HAS_CORONA_TRIGGERED_JOB_MUSIC()
		/*IF iMissionType = FMMC_TYPE_MISSION
			PRINTLN("[CORONA] SET_CORONA_MUSIC - beginning music for missions")
			
			SET_MISSION_MUSIC(g_FMMC_STRUCT.iMusic)
			SET_CORONA_HAS_TRIGGERED_JOB_MUSIC(TRUE)
			
		ELSE*/
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HuntDark(g_FMMC_STRUCT.iRootContentIdHash)
			SET_CORONA_BIT(CORONA_NO_RADIO)
			
			PRINTLN("[CORONA] MAINTAIN_CORONA_SCREEN_DURING_TRANSITION - trigger HuntDark audio - g_FMMC_STRUCT.iAudioScore = ", g_FMMC_STRUCT.iAudioScore, " - T(", GET_CLOUD_TIME_AS_INT(), ")")
			SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore)
			TRIGGER_MUSIC_EVENT(GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, MUSIC_SUSPENSE))
		ENDIF
		
		IF IS_ARENA_WARS_JOB(TRUE)
			SET_CORONA_BIT(CORONA_NO_RADIO)
			PRINTLN("[CORONA] SET_CORONA_MUSIC - Arena wars, kill radio")
		ENDIF
		
		#IF FEATURE_CASINO_HEIST
		IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
			SET_CORONA_BIT(CORONA_NO_RADIO)
			PRINTLN("[CORONA] SET_CORONA_MUSIC - Csaino heist, kill radio")
		ENDIF
		#ENDIF
		
		#IF FEATURE_HEIST_ISLAND
		IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_MISSION_FINALE()
			SET_CORONA_BIT(CORONA_NO_RADIO)
			PRINTLN("[CORONA] SET_CORONA_MUSIC - Island heist, kill radio")
		ENDIF
		#ENDIF
		
		#IF FEATURE_TUNER
		IF IS_THIS_A_TUNER_ROBBERY_FINALE()
			SET_CORONA_BIT(CORONA_NO_RADIO)
			PRINTLN("[CORONA] SET_CORONA_MUSIC - Tuner robbery, kill radio")
		ENDIF
		#ENDIF
		
		#IF FEATURE_FIXER
		IF IS_THIS_A_FIXER_STORY_MISSION()
		OR IS_THIS_A_FIXER_SHORT_TRIP()
			SET_CORONA_BIT(CORONA_NO_RADIO)
			PRINTLN("[CORONA] SET_CORONA_MUSIC - Fixer mission, kill radio")
		ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_NoLobbyRadio")
			SET_CORONA_BIT(CORONA_NO_RADIO)
			PRINTLN("[CORONA] SET_CORONA_MUSIC - sc_NoLobbyRadio, kill radio")
		ENDIF
		#ENDIF
		
		IF NOT IS_CORONA_BIT_SET(CORONA_NO_RADIO)
			SET_AUDIO_FLAG("AllowRadioDuringSwitch", TRUE)
		
			BEGIN_CORONA_RADIO()			
			
			SET_CORONA_BIT(CORONA_TRIGGER_PERSISTENT_MUSIC)
			
			SET_CORONA_HAS_TRIGGERED_JOB_MUSIC(TRUE)
			
			PRINTLN("[CORONA] SET_CORONA_MUSIC - beginning radio for Race / DM: ", GET_RADIO_STATION_NAME(g_iMyChosenRadioStation))
		ELSE
			PRINTLN("[CORONA] SET_CORONA_MUSIC - DON'T DO CORONA RADIO - ON HEIST OR HUNTDARK OR ARENA WAR")
			SET_CORONA_HAS_TRIGGERED_JOB_MUSIC(TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Cleans up our corona music
PROC CLEAN_UP_CORONA_MUSIC()//INT iMissionType)
	PRINTLN("[CORONA] CLEAN_UP_CORONA_MUSIC()")
	IF NOT IS_CORONA_BIT_SET(CORONA_MENU_RADIO_FADE_OUT_STARTED)
		PRINTLN("[CORONA] CLEAN_UP_CORONA_MUSIC() - Stopping radio")
		
		//STOP_AUDIO_SCENE("CAR_MOD_RADIO_MUTE_SCENE")
		//TRIGGER_MUSIC_EVENT("GLOBAL_KILL_MUSIC_FADEIN_RADIO") 
		//PRINTLN("[CORONA] STOP_CORONA_MUSIC - DONE")
		
		// Stop any score music
		//IF iMissionType != FMMC_TYPE_MISSION
		//IF (iMissionType != FMMC_TYPE_RACE AND NOT IS_ON_VEH_DEATHMATCH_GLOBAL_SET())
		//OR (iMissionType = FMMC_TYPE_RACE AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole = 1)
		//	TRIGGER_MUSIC_EVENT("MP_MC_STOP")
		//ENDIF
		
		// Stop any radio
		IF NETWORK_IS_GAME_IN_PROGRESS()
			START_AUDIO_SCENE("MP_CORONA_STOP_RADIO")	//SET_MOBILE_PHONE_RADIO_STATE(FALSE)
		ENDIF
		SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
		SET_AUDIO_FLAG("AllowRadioDuringSwitch", FALSE)
		
		IF IS_CORONA_BIT_SET(CORONA_NO_RADIO)
		AND NOT Is_Player_Currently_On_MP_Heist(PLAYER_ID()) // B* 2573910
		AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[CORONA] CLEAN_UP_CORONA_MUSIC - Kill all score music")
			TRIGGER_MUSIC_EVENT("HALLOWEEN_FAST_STOP_MUSIC")
		ENDIF

		SET_CORONA_BIT(CORONA_MENU_RADIO_FADE_OUT_STARTED)
	ELSE
		PRINTLN("[CORONA] CLEAN_UP_CORONA_MUSIC() - CORONA_MENU_RADIO_FADE_OUT_STARTED is set. Not calling stop radio audio scene or flags.")
	ENDIF
ENDPROC

//PURPOSE: Start Music Fade Out. Not for Races or Missions
/*PROC FADE_OUT_CORONA_MUSIC(INT iMissionType)
	IF NOT IS_CORONA_BIT_SET(CORONA_MENU_RADIO_FADE_OUT_STARTED)
		//IF iMissionType != FMMC_TYPE_MISSION
		IF (iMissionType != FMMC_TYPE_RACE AND NOT IS_ON_VEH_DEATHMATCH_GLOBAL_SET())
		OR (iMissionType = FMMC_TYPE_RACE AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole = 1)
			//START_AUDIO_SCENE("CAR_MOD_RADIO_MUTE_SCENE")
			TRIGGER_MUSIC_EVENT("MP_RADIO_FADE_OUT")
			PRINTLN("[CORONA] FADE_OUT_CORONA_MUSIC - DONE")
		ENDIF
		SET_CORONA_BIT(CORONA_MENU_RADIO_FADE_OUT_STARTED)
	ENDIF
ENDPROC*/

//PURPOSE: Stop Corona Music. Used on Walk Out.
PROC STOP_CORONA_MUSIC(INT iMissionType, BOOL bForce = FALSE)
	//IF (iMissionType != FMMC_TYPE_MISSION AND iMissionType != FMMC_TYPE_RACE)	
	IF iMissionType != FMMC_TYPE_RACE	//(iMissionType != FMMC_TYPE_RACE AND NOT IS_ON_VEH_DEATHMATCH_GLOBAL_SET())
	OR (iMissionType = FMMC_TYPE_RACE AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole = 1)
	OR bForce = TRUE
		CLEAN_UP_CORONA_MUSIC()//iMissionType)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_VEH_COLOUR_TYPE()
	SWITCH g_iMyRaceColorType
		CASE MCT_METALLIC RETURN "MCT_METALLIC"
   		CASE MCT_CLASSIC RETURN "MCT_CLASSIC"
   		CASE MCT_PEARLESCENT RETURN "MCT_PEARLESCENT"
   		CASE MCT_MATTE RETURN "MCT_MATTE"
   		CASE MCT_METALS RETURN "MCT_METALS"
   		CASE MCT_CHROME RETURN "MCT_CHROME"
   		CASE MCT_NONE RETURN "MCT_NONE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC
#ENDIF


/// PURPOSE: Sets the colours to those saved to the stats
PROC SET_CORONA_DEFAULT_COLOURS(CORONA_MENU_DATA &coronaMenuData)

	g_iMyRaceColorType			= INT_TO_ENUM(MOD_COLOR_TYPE, GET_MP_INT_PLAYER_STAT(MPPLY_FM_VEH_COLOUR_TYPE))
	g_iMyRaceColorSelection		= GET_MP_INT_PLAYER_STAT(MPPLY_FM_VEH_COLOUR)
	
	IF IS_OPEN_WHEEL_RACE()
	AND g_iMyOpenWheelColourSelection != -1
		g_iMyRaceColorSelection = g_iMyOpenWheelColourSelection
		PRINTLN("[CORONA] SET_CORONA_DEFAULT_COLOURS - Open wheel race. Setting colour to previously used: ", g_iMyRaceColorSelection)
	ENDIF
	
	PRINTLN("[CORONA] SET_CORONA_DEFAULT_COLOURS - setting players default colors to: ")
	
	// If we have not yet defaulted our colours - default
	IF NOT IS_CORONA_BIT_SET(CORONA_VEHICLE_COLOURS_DEFAULTED)
		PRINTLN("[CORONA]				- Defaulting saved colours")
	
		IF IS_DEFAULT_COLOUR_SELECTION_AVAILABLE()
			g_iMyRaceColorSelection = FM_RACE_COLOUR_DEFAULT
			PRINTLN("[CORONA] SET_CORONA_DEFAULT_COLOURS - Default colours available, setting FM_RACE_COLOUR_DEFAULT as default colour")
		ENDIF
	
		coronaMenuData.iSelectedColourType = g_iMyRaceColorType
		coronaMenuData.iSelectedColourIndex = g_iMyRaceColorSelection
		coronaMenuData.iSelectedLivery = g_iMyRaceLiverySelection
		
		IF g_iMyRaceColorSelection = -1
			PRINTLN("[CORONA]				- Saved colour is crew, check it's valid")
			
			GAMER_HANDLE aGamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
			IF NOT IS_PLAYER_IN_ACTIVE_CLAN(aGamerHandle)
				PRINTLN("[CORONA]				- Player not in a crew, default to index 0")
				
				g_iMyRaceColorSelection = 0
			ENDIF
		ELIF g_iMyRaceColorSelection = FM_RACE_COLOUR_DEFAULT
			IF NOT IS_DEFAULT_COLOUR_SELECTION_AVAILABLE()
				PRINTLN("[CORONA]				- Default colour selection is not available, default to index 0")
				
				g_iMyRaceColorSelection = 0
			ENDIF
		ENDIF
		
		IF g_iMyRaceColorSelection >= MAX_RACE_COLOUR
			PRINTLN("[CORONA]				- Player saved colour is out of range, set to 0")
			g_iMyRaceColorSelection = 0
		ENDIF
		
		SET_CORONA_BIT(CORONA_VEHICLE_COLOURS_DEFAULTED)
	ENDIF
	
	// If we should use another player's colour selection, override default
	IF IS_CORONA_BIT_SET(CORONA_VEHICLE_SELECTION_UNAVAILABLE)
		IF coronaMenuData.piRacePartner != INVALID_PLAYER_INDEX()
			
			g_iMyRaceColorSelection = GET_PLAYER_CORONA_VEHICLE_COLOUR_INDEX(coronaMenuData.piRacePartner)
			
			PRINTLN("[CORONA]				- Taking colour of partner")
		ENDIF
	ELSE
		IF IS_CORONA_BIT_SET(CORONA_RANDOMISE_COLOUR_FOR_VEHICLE)
			
			INT iOldValue = g_iMyRaceColorSelection
			
			CLEAR_CORONA_BIT(CORONA_RANDOMISE_COLOUR_FOR_VEHICLE)
			g_iMyRaceColorSelection = GET_RANDOM_INT_IN_RANGE(0, MAX_RACE_COLOUR)
			
			IF g_iMyRaceColorSelection = FM_RACE_COLOUR_DEFAULT
			AND NOT IS_DEFAULT_COLOUR_SELECTION_AVAILABLE()
				g_iMyRaceColorSelection--
			ENDIF
			
			// Check we haven't got the same colour as before
			IF g_iMyRaceColorSelection = iOldValue
				IF g_iMyRaceColorSelection < (MAX_RACE_COLOUR-1)
					g_iMyRaceColorSelection++
				ELSE
					g_iMyRaceColorSelection--
				ENDIF
			ENDIF
			
			PRINTLN("[CORONA]				- Randomise has been chosen, change colour to: ", g_iMyRaceColorSelection)
		ENDIF
	ENDIF
	
	PRINTLN("[CORONA]				- Colour Type: ", GET_VEH_COLOUR_TYPE(), " Colour: ", g_iMyRaceColorSelection)		
ENDPROC

// Save the current colours to stats
PROC SAVE_CORONA_DEFAULT_COLOUR(CORONA_MENU_DATA &coronaMenuData)
	
	IF coronaMenuData.bVehHasModKit
	OR (g_mnMyRaceModel = HOTRING
	AND NOT IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID()))
	
		
		PRINTLN("[CORONA] SAVE_CORONA_DEFAULT_COLOUR - saving player colours: ")
		PRINTLN("				- Colour Type: ", GET_VEH_COLOUR_TYPE())
		PRINTLN("				- Colour: ", g_iMyRaceColorSelection)
		
		SET_MP_INT_PLAYER_STAT(MPPLY_FM_VEH_COLOUR_TYPE, ENUM_TO_INT(g_iMyRaceColorType))
		SET_MP_INT_PLAYER_STAT(MPPLY_FM_VEH_COLOUR, ENUM_TO_INT(g_iMyRaceColorSelection))
	
	ENDIF
ENDPROC

//should the spinner be turned off
FUNC BOOL SHOULD_SPINNER_BE_DISABLED_FOR_MISSION()
	//if we are restarting then no
	IF IS_TRANSITION_SESSION_RESTARTING()
		RETURN FALSE
	//if this is not a mission then yes	
	ELIF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION
		RETURN TRUE
	//if it is a mission
	ELSE
		//and the mission is starting
		IF GET_CORONA_STATUS() = CORONA_STATUS_LAUNCH
		OR GET_CORONA_STATUS() = CORONA_LAUNCH_TRANSITION
		OR GET_CORONA_STATUS() = CORONA_STATUS_BALANCE_AND_LOAD
		OR GET_CORONA_STATUS() = CORONA_STATUS_WAIT_FOR_ACTIVE
			RETURN FALSE
		ENDIF		
		//if we are quiting the corona, and the mission is not running
		IF GET_CORONA_STATUS() = CORONA_STATUS_QUIT
			IF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_MISSION
			AND NOT SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC CLEAR_CORONA_ANIMATED_HELP_TEXT(CORONA_MENU_DATA &coronaMenuData, SCALEFORM_LOADING_ICON &scaleformLoadingIcon)
	IF SHOULD_SPINNER_BE_DISABLED_FOR_MISSION()
		REFRESH_SCALEFORM_LOADING_ICON(scaleformLoadingIcon)
		
		coronaMenuData.iBS_CoronaAnimTextActive = 0
		
		IF NOT IS_TRANSITION_ACTIVE()
		
			SET_TRANSITION_STRING("")
			SET_LOADING_ICON_INACTIVE(IS_PLAYER_ACTIVE_IN_CORONA())	
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_CORONA_ANIMATED_HELP_TEXT_DISPLAYING(CORONA_MENU_DATA &coronaMenuData, INT iHelpType)
	RETURN IS_BIT_SET(coronaMenuData.iBS_CoronaAnimTextActive, iHelpType)
ENDFUNC

PROC DISPLAY_CORONA_ANIMATED_HELP_TEXT(CORONA_MENU_DATA &coronaMenuData, SCALEFORM_LOADING_ICON &scaleformLoadingIcon, INT iHelpType, BOOL bUpdate = FALSE)

	// Check that we are not already showing the animated text
	IF NOT IS_BIT_SET(coronaMenuData.iBS_CoronaAnimTextActive, iHelpType)
	OR iHelpType = CORONA_HELP_READY_PLAYERS
	OR iHelpType = CORONA_HELP_AUTOSTART
	OR iHelpType = CORONA_HELP_AUTOSTART_DELAYED
	OR iHelpType = CORONA_HELP_TUTORIAL_WAIT
	
		IF bUpdate
			scaleformLoadingIcon.bInitialised = FALSE
		ELSE
			REFRESH_SCALEFORM_LOADING_ICON(scaleformLoadingIcon, (NOT IS_PLAYER_ACTIVE_IN_CORONA()))
		ENDIF
	
		// Set the correct icon
		SWITCH iHelpType
			
			CASE CORONA_HELP_INVITE_SENT
			CASE CORONA_HELP_NO_INVITES_SENT
			CASE CORONA_HELP_AUTOFILL
			CASE CORONA_HELP_READY_PLAYERS
				SET_SCALEFORM_LOADING_ICON_DISPLAY(scaleformLoadingIcon, LOADING_ICON_LOADING)
			BREAK
			
			CASE CORONA_HELP_AUTOSTART
			CASE CORONA_HELP_AUTOSTART_DELAYED
			CASE CORONA_HELP_TUTORIAL_WAIT
			CASE CORONA_HELP_ON_CALL_HOST
			CASE CORONA_HELP_WAIT_ON_HOST
				SET_SCALEFORM_LOADING_ICON_DISPLAY(scaleformLoadingIcon, LOADING_ICON_LOADING)
			BREAK
			
			DEFAULT
				SET_SCALEFORM_LOADING_ICON_DISPLAY(scaleformLoadingIcon, LOADING_ICON_LOADING)
			BREAK
			
		ENDSWITCH
		
		TEXT_LABEL_15 tlLoadingText = ""
		// Set the correct icon
		SWITCH iHelpType
			
			CASE CORONA_HELP_INVITE_SENT
				tlLoadingText = "BM_INV_SENT"
			BREAK
			
			CASE CORONA_HELP_NO_INVITES_SENT
				tlLoadingText = "BM_NO_INV"
			BREAK
			
			CASE CORONA_HELP_AUTOFILL
				IF IS_CORONA_BIT_SET(CORONA_AUTO_INVITE_GATHERING_PLAYERS)
					tlLoadingText = "BM_INVITE_GADD"
				ELSE
					tlLoadingText = "BM_INVITE_PLY"
				ENDIF
			BREAK
			
			CASE CORONA_HELP_ON_CALL_HOST
				tlLoadingText = "FM_COR_OCH"
			BREAK
			
			CASE CORONA_HELP_READY_PLAYERS
				
				tlLoadingText = "FM_COR_PRDY"
				scaleformLoadingIcon.IsTextDoubleNumber = TRUE
				scaleformLoadingIcon.TopNumber = coronaMenuData.iTotalPlayersReady
				scaleformLoadingIcon.Denominator = coronaMenuData.iTotalPlayersInCorona
			BREAK
			
			CASE CORONA_HELP_TUTORIAL_WAIT
				tlLoadingText = "FM_IHELP_WAT"
				scaleformLoadingIcon.IsNumberTime = TRUE
				scaleformLoadingIcon.TopNumber = (coronaMenuData.iCoronaAutoStartTime)*1000
			BREAK
			
			CASE CORONA_HELP_AUTOSTART_DELAYED
				tlLoadingText = "FM_COR_AUTOD"
			BREAK
			
			CASE CORONA_HELP_WAIT_ON_HOST
				tlLoadingText = "FM_COR_HEIWAIT"
			BREAK			
		ENDSWITCH

		scaleformLoadingIcon.sMainStringSlot = tlLoadingText
		SET_TRANSITION_STRING(tlLoadingText)

//		PRINTLN("[CORONA] DISPLAY_CORONA_ANIMATED_HELP_TEXT - Displaying help text and setting global string: ", tlLoadingText)
//		PRINTLN("[CORONA] DISPLAY_CORONA_ANIMATED_HELP_TEXT - scaleformLoadingIcon.TopNumber: ", scaleformLoadingIcon.TopNumber)
//		PRINTLN("[CORONA] DISPLAY_CORONA_ANIMATED_HELP_TEXT - scaleformLoadingIcon.Denominator: ", scaleformLoadingIcon.Denominator)
//		PRINTLN("[CORONA] DISPLAY_CORONA_ANIMATED_HELP_TEXT - coronaMenuData.iTotalPlayersInCorona: ", coronaMenuData.iTotalPlayersInCorona)

		// Record the time of the request.
		coronaMenuData.coronaAnimHelpTimer = GET_NETWORK_TIME()
		SET_BIT(coronaMenuData.iBS_CoronaAnimTextActive, iHelpType)
		
		RUN_SCALEFORM_LOADING_ICON(scaleformLoadingIcon, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(scaleformLoadingIcon))
	ENDIF

ENDPROC

PROC MAINTAIN_CORONA_ANIMATED_HELP_TEXT(CORONA_MENU_DATA &coronaMenuData, SCALEFORM_LOADING_ICON &coronaLoadingIcon)
	
	IF IS_BIT_SET(coronaMenuData.iBS_CoronaAnimTextActive, CORONA_HELP_INVITE_SENT)
	OR IS_BIT_SET(coronaMenuData.iBS_CoronaAnimTextActive, CORONA_HELP_NO_INVITES_SENT)
	OR IS_CORONA_BIT_SET(CORONA_BUTTON_INVITING_PLAYERS)
	OR IS_CORONA_BIT_SET(CORONA_BUTTON_NO_INVITES_SENT)
		
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), coronaMenuData.coronaAnimHelpTimer)) > CORONA_DISPLAY_INVITE_TIME
			CLEAR_CORONA_ANIMATED_HELP_TEXT(coronaMenuData, coronaLoadingIcon)
			
			CLEAR_CORONA_BIT(CORONA_BUTTON_INVITING_PLAYERS)
			CLEAR_CORONA_BIT(CORONA_BUTTON_NO_INVITES_SENT)
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEAR_CORONA_ANIMATED_INSTRUCTIONAL_BUTTONS(INT iSpecificType = -1)

	IF GET_CORONA_STATUS() = CORONA_STATUS_LEADERBOARD
		EXIT
	ENDIF

	IF iSpecificType = -1
		
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_DISPLAY_TIMER"))
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_EXTEND_TIME"))
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_EXTEND_TIME_KM"))
		PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
				
		CLEAR_CORONA_BIT(CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE)
				
	ELSE
		SWITCH iSpecificType
			CASE CORONA_HELP_AUTOSTART
			CASE CORONA_HELP_AUTOSTART_INIT
				
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_DISPLAY_TIMER"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_EXTEND_TIME"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_EXTEND_TIME_KM"))
				PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
				
				CLEAR_CORONA_BIT(CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE)
				
			BREAK
			
			
		ENDSWITCH
	ENDIF

ENDPROC

PROC DISPLAY_CORONA_ANIMATED_INSTRUCTIONAL_BUTTONS(	CORONA_MENU_DATA &coronaMenuData, INT iHelpType)
	
	TEXT_LABEL_31 tlHostName
	BOOL bRefreshButtons
	
	IF GET_CORONA_STATUS() = CORONA_STATUS_LEADERBOARD
		EXIT
	ENDIF
	
	// If the host has changed we need to refresh dynamic strings
	IF IS_CORONA_BIT_SET(CORONA_HOST_CHANGE)
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_31 tlTempDebugName = GET_TRANSITION_PLAYER_GAMERTAG(coronaMenuData.iCoronaHost)
		PRINTLN("[CORONA] DISPLAY_CORONA_ANIMATED_INSTRUCTIONAL_BUTTONS - host has changed - update buttons, coronaMenuData.iCoronaHost = ", coronaMenuData.iCoronaHost, ", name = ", tlTempDebugName)
		#ENDIF
	
		bRefreshButtons = TRUE
		CLEAR_CORONA_BIT(CORONA_HOST_CHANGE)
	ENDIF
	
	SWITCH iHelpType
		CASE CORONA_HELP_AUTOSTART
		
			IF NOT IS_CORONA_BIT_SET(CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE)
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CORONA_DISPLAY_TIMER"))
				
				bRefreshButtons = TRUE
				SET_CORONA_BIT(CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE)
			ENDIF
			
			IF GET_CORONA_STATUS() = CORONA_STATUS_BETTING
			OR IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
				UPDATE_FRONTEND_ANIMATED_BUTTON(0, "FM_COR_SSCRN", (coronaMenuData.iCoronaAutoStartTime)*1000, -1, TRUE)
			ELSE
				UPDATE_FRONTEND_ANIMATED_BUTTON(0, "FM_COR_FSCRN", (coronaMenuData.iCoronaAutoStartTime)*1000, -1, TRUE)
			ENDIF
		BREAK
		
		CASE CORONA_HELP_AUTOSTART_INIT
		
			
		
			IF NOT IS_CORONA_BIT_SET(CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE)
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CORONA_DISPLAY_TIMER"))
				
				bRefreshButtons = TRUE
				SET_CORONA_BIT(CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE)
			ENDIF
			IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
				UPDATE_FRONTEND_ANIMATED_BUTTON(0, "FM_COR_FLCH", (coronaMenuData.iCoronaAutoStartTime - GET_CLOUD_TIME_AS_INT())*1000, -1, TRUE)
			ELSE
				IF IS_CORONA_BIT_SET(CORONA_BUTTON_INVITING_PLAYERS)
				OR IS_CORONA_BIT_SET(CORONA_BUTTON_INVITING_PLAYERS_AUTOFILL)
				
					IF NOT IS_CORONA_BIT_SET(CORONA_BUTTONS_UPDATED_FOR_INVITES)
						SET_CORONA_BIT(CORONA_BUTTONS_UPDATED_FOR_INVITES)
						bRefreshButtons = TRUE
					ENDIF
					
					UPDATE_FRONTEND_ANIMATED_BUTTON(0, "FM_COR_INVP", (coronaMenuData.iCoronaAutoStartTime - GET_CLOUD_TIME_AS_INT())*1000, -1, TRUE)
				ELIF IS_CORONA_BIT_SET(CORONA_BUTTON_NO_INVITES_SENT)
					
					IF NOT IS_CORONA_BIT_SET(CORONA_BUTTONS_UPDATED_FOR_INVITES)
						SET_CORONA_BIT(CORONA_BUTTONS_UPDATED_FOR_INVITES)
						bRefreshButtons = TRUE
					ENDIF
					
					UPDATE_FRONTEND_ANIMATED_BUTTON(0, "FM_COR_NINVP", (coronaMenuData.iCoronaAutoStartTime - GET_CLOUD_TIME_AS_INT())*1000, -1, TRUE)
				ELSE
					IF IS_CORONA_BIT_SET(CORONA_BUTTONS_UPDATED_FOR_INVITES)
						CLEAR_CORONA_BIT(CORONA_BUTTONS_UPDATED_FOR_INVITES)
						bRefreshButtons = TRUE
					ENDIF
				
					tlHostName = GET_TRANSITION_PLAYER_GAMERTAG(coronaMenuData.iCoronaHost)
					
					IF coronaMenuData.iCoronaHost = GET_MY_TRANSITION_SESSION_ARRAY_POS()
						UPDATE_FRONTEND_ANIMATED_BUTTON(0, "FM_COR_INVP", (coronaMenuData.iCoronaAutoStartTime - GET_CLOUD_TIME_AS_INT())*1000, -1, TRUE)
					ELSE
						UPDATE_FRONTEND_ANIMATED_BUTTON(0, "FM_COR_WAITHT", (coronaMenuData.iCoronaAutoStartTime - GET_CLOUD_TIME_AS_INT())*1000, -1, TRUE, tlHostName, TRUE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CORONA_HELP_FORCE_LAUNCH
			
			IF NOT IS_CORONA_BIT_SET(CORONA_FORCED_LAUNCH_INSTRUCTIONAL_BUTTONS)
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CORONA_DISPLAY_TIMER"))
				
				bRefreshButtons = TRUE
				SET_CORONA_BIT(CORONA_FORCED_LAUNCH_INSTRUCTIONAL_BUTTONS)
			ENDIF
			
			UPDATE_FRONTEND_ANIMATED_BUTTON(0, "FM_COR_FLCH", (coronaMenuData.iForcedLaunchSec)*1000, -1, TRUE)

		BREAK
		
		CASE CORONA_HELP_TUTORIAL_WAIT
		
			IF NOT IS_CORONA_BIT_SET(CORONA_BUTTONS_UPDATED_FOR_INVITES)
				SET_CORONA_BIT(CORONA_BUTTONS_UPDATED_FOR_INVITES)
				bRefreshButtons = TRUE
			ENDIF
			
			IF g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY = FM_SESSION_MENU_CHOICE_JOIN_CLOSED_SOLO_PLAYABLE_SESSION
				UPDATE_FRONTEND_ANIMATED_BUTTON(0, "FM_COR_SSCRN", (coronaMenuData.iCoronaAutoStartTime - GET_CLOUD_TIME_AS_INT())*1000, -1, TRUE)
			ELSE
				UPDATE_FRONTEND_ANIMATED_BUTTON(0, "FM_IHELP_WAT", (coronaMenuData.iCoronaAutoStartTime - GET_CLOUD_TIME_AS_INT())*1000, -1, TRUE)
			ENDIF
		BREAK
		
		CASE CORONA_HELP_AUTOSTART_DELAYED
			
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CORONA_DISPLAY_TIMER"))
			
			bRefreshButtons = TRUE						
		BREAK

	ENDSWITCH
	
	IF bRefreshButtons
		PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
	ENDIF
ENDPROC


FUNC INT DISPLAY_CORONA_COUNT_DOWN_TIMER(	CORONA_MENU_DATA 		&coronaMenuData,
										INT iVoteParticipant, INT iCoronaTimerTemp, BOOL bUseSecondaryTimer = FALSE)

	INT iSeconds = -1

	#IF IS_DEBUG_BUILD
	IF NOT IS_BETTING_ALLOWING_TIMER()
	
		IF IS_CORONA_BIT_SET(CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE)
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_DISPLAY_TIMER"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		ENDIF
	
		RETURN iSeconds
	ENDIF
	#ENDIF
	
	BOOL bTimerStarted = HAS_NET_TIMER_STARTED(GlobalplayerBD_FM_2[iVoteParticipant].sCoronaTimer)
	
	IF bUseSecondaryTimer
		bTimerStarted = HAS_NET_TIMER_STARTED(GlobalplayerBD_FM_2[iVoteParticipant].sCoronaTimer2)
	ENDIF

	IF bTimerStarted
		TIME_DATATYPE tdTimerTime
		IF !bUseSecondaryTimer
			tdTimerTime = GlobalplayerBD_FM_2[iVoteParticipant].sCoronaTimer.Timer
		ELSE
			tdTimerTime = GlobalplayerBD_FM_2[iVoteParticipant].sCoronaTimer2.Timer
		ENDIF
	
		INT ms = (iCoronaTimerTemp - GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdTimerTime))
		IF ms >= 0
		
			iSeconds = ROUND(ms * 0.001)
			IF coronaMenuData.iCoronaAutoStartTime != iSeconds
			
				coronaMenuData.iCoronaAutoStartTime = iSeconds
			
				DISPLAY_CORONA_ANIMATED_INSTRUCTIONAL_BUTTONS(coronaMenuData, CORONA_HELP_AUTOSTART)
			ENDIF
		ELSE
			IF IS_CORONA_BIT_SET(CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE)
				CLEAR_CORONA_ANIMATED_INSTRUCTIONAL_BUTTONS(CORONA_HELP_AUTOSTART)
			ENDIF
		ENDIF
	ELSE
		IF IS_CORONA_BIT_SET(CORONA_INSTRUCTIONAL_BUTTONS_TIMER_ACTIVE)
			CLEAR_CORONA_ANIMATED_INSTRUCTIONAL_BUTTONS(CORONA_HELP_AUTOSTART)
		ENDIF
	ENDIF
	
	RETURN iSeconds
ENDFUNC

/// PURPOSE: Displays help text in bottom column of corona menu
PROC DISPLAY_CORONA_FLASHING_HELP_TEXT(CORONA_MENU_DATA &coronaMenuData, STRING strHelpText, BOOL bSetTime = TRUE, BOOL bDisplayForever = FALSE, BOOL bCanBeOverriden = FALSE, BOOL bCheckStrings = FALSE, INT iOptionalInt = -1)

	IF NOT IS_PAUSE_MENU_ACTIVE()
		EXIT
	ENDIF

	// Not already displaying this flashing help
	IF NOT bCheckStrings
	OR (IS_STRING_NULL_OR_EMPTY(coronaMenuData.tlFlashHelpText) OR NOT ARE_STRINGS_EQUAL(coronaMenuData.tlFlashHelpText, strHelpText))
		SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, strHelpText, TRUE, iOptionalInt)
		SET_CORONA_BIT(CORONA_DISPLAY_FLASHING_HELP_TEXT)
		coronaMenuData.tlFlashHelpText = strHelpText
		coronaMenuData.iFlashHelpTextOptInt = iOptionalInt
	ENDIF
	
	IF bCanBeOverriden
		coronaMenuData.iFlashHelpTextState = 1
	ELSE
		coronaMenuData.iFlashHelpTextState = 2
	ENDIF

	IF bSetTime
		coronaMenuData.coronaHelpTextTimer = GET_NETWORK_TIME()
	ENDIF
	
	IF bDisplayForever
		coronaMenuData.iFlashHelpTextState = -coronaMenuData.iFlashHelpTextState
	ENDIF
	
	PRINTLN("[CORONA] DISPLAY_CORONA_FLASHING_HELP_TEXT - Help: ", strHelpText)
ENDPROC

 
// PURPOSE:
///    Make a string point at a text label.
/// RETURNS:
///    A STRING which points at whatever was passed in.
FUNC STRING TEXT_LABEL_INTO_STRING(STRING sTextLabel)
	RETURN sTextLabel
ENDFUNC

//PURPOSE: Gets the Team Name for the Team. Checks if it should be the Crew Name.
FUNC TEXT_LABEL_63 GET_CREW_NAME_FOR_TEAM(INT &iClanID, CORONA_MENU_DATA &coronaMenuData, INT iTeam)
	INT i
	GAMER_HANDLE thisHandle
	TEXT_LABEL_63 tlName = "FM_TDM_TEAM_S"
	iClanID = -1
	
	PRINTLN("GET_CORONA_NAME_FOR_SLOT - Team: ", iTeam)
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_ACTIVE(coronaMenuData.lobbyPlayers[i].playerID)
				IF coronaMenuData.lobbyPlayers[i].iTeam = iTeam
					thisHandle = GET_GAMER_HANDLE_PLAYER(coronaMenuData.lobbyPlayers[i].playerID)
					//Store First PLayer ClanID
					IF iClanID = -1
						iClanID = GET_PLAYER_CLAN_ID(thisHandle)
						IF iClanID = -1
							PRINTLN("GET_CORONA_NAME_FOR_SLOT - PLAYER NOT IN A CLAN ")
							tlName = "FM_TDM_TEAM_S"
							iClanID = -1
							RETURN tlName
						ENDIF
						tlName = GET_PLAYER_CLAN_NAME(thisHandle)
						PRINTLN("GET_CORONA_NAME_FOR_SLOT - iClanID = ", iClanID, " tlClanName = ", tlName, " for player: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerID))
					//Exit if ClanIDs don't match
					ELSE
						IF iClanID != GET_PLAYER_CLAN_ID(thisHandle)
							PRINTLN("GET_CORONA_NAME_FOR_SLOT - iClanID = ", iClanID, " NOT EQUAL TO ", GET_PLAYER_CLAN_ID(thisHandle))
							tlName = "FM_TDM_TEAM_S"
							iClanID = -1
							RETURN tlName
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN tlName
ENDFUNC

FUNC STRING GET_CORONA_NAME_FOR_TEAM(CORONA_MENU_DATA &coronaMenuData, INT &iAnInt, INT iTeam)
	//TCC - Check to see if all team are on the same Crew
	TEXT_LABEL_63 tlClanName
	INT iClanID = -1
	
	tlClanName = GET_CREW_NAME_FOR_TEAM(iClanID, coronaMenuData, iTeam)
	PRINTLN(" GET_CORONA_NAME_FOR_SLOT - iTeam = ", iTeam, " iClanID = ", iClanID, " tlClanName = ", tlClanName)
	
	IF iClanID != -1
		
		//Check we aren't already using the Crew Name
		IF iTeam > 0
			INT iTeamCount
			INT iFake
			TEXT_LABEL_63 OtherTeamClanName
			
			FOR iTeamCount = 0 TO FMMC_MAX_TEAMS
				IF iTeam != iTeamCount
					iFake = -1
					OtherTeamClanName = GET_CREW_NAME_FOR_TEAM(iFake, coronaMenuData, iTeamCount)
					PRINTLN(" GET_CORONA_NAME_FOR_SLOT - CHECK OTHER TEAM NAMES - iTeamCount = ", iTeamCount, " OtherTeamClanName = ", OtherTeamClanName)
					IF NOT IS_STRING_NULL_OR_EMPTY(OtherTeamClanName)
						IF ARE_STRINGS_EQUAL(tlClanName, OtherTeamClanName)
							PRINTLN(" GET_CORONA_NAME_FOR_SLOT - Crew Name already used so use default")
							RETURN "FM_TDM_TEAM_S"
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		iAnInt = 99
		PRINTLN(" GET_CORONA_NAME_FOR_SLOT - USE tlClanName = ", tlClanName)
		RETURN TEXT_LABEL_INTO_STRING(tlClanName)
	ENDIF

	PRINTLN(" GET_CORONA_NAME_FOR_SLOT - NO PLAYERS IN TEAM ")
	RETURN "FM_TDM_TEAM_S"
ENDFUNC

/// PURPOSE:
///    Returns the correctly constructed team name. Bounds checked for < 0.
FUNC TEXT_LABEL_15 GET_CORONA_TEAM_NAME(INT iTeam, BOOL bSingular = FALSE)
	
	TEXT_LABEL_15 tlTeamName = "HEIST_RL_NONE"
	
	IF iTeam < 0 
		RETURN tlTeamName
	ENDIF
	
	tlTeamName = "COR_TEAM_"
	
	IF bSingular
		tlTeamName += "S_"
	ENDIF
	
	PRINTLN("GET_CORONA_TEAM_NAME - iTeam = ", iTeam)
	PRINTLN("GET_CORONA_TEAM_NAME - g_FMMC_STRUCT.iTeamNames[",iTeam,"] = ", g_FMMC_STRUCT.iTeamNames[iTeam])
	
	tlTeamName += g_FMMC_STRUCT.iTeamNames[iTeam]
	
	PRINTLN("GET_CORONA_TEAM_NAME - tlTeamName [1] = ", tlTeamName)
	
	// Override our name if it is a custom name
	IF g_FMMC_STRUCT.iTeamNames[iTeam] = ciTEAM_NAME_MAX
		tlTeamName = g_FMMC_STRUCT.tlCustomName[iTeam]
		PRINTLN("GET_CORONA_TEAM_NAME - tlTeamName [2] = ", tlTeamName)
	ENDIF
	
	RETURN tlTeamName
ENDFUNC

FUNC BOOL SHOULD_DISGUISE_TEAM()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciDISGUISE_TEAM_SELECTION) 
ENDFUNC

FUNC TEXT_LABEL_15 GET_ALTERNATIVE_TEAM_NAME(INT iStyleSetup, VERSUS_OUTFIT_STYLE eVersusStyle, INT iTeam)
	MP_OUTFIT_ENUM eLocalOutfit = GET_MP_VERSUS_OUTFIT_DEFAULT(iStyleSetup, eVersusStyle, iTeam)
		
	TEXT_LABEL_15 tlTeamName = ""
	
	SWITCH eLocalOutfit
		
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_5
			tlTeamName = "TEAM_TOXIC_P"
		BREAK
			
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_5
			tlTeamName = "TEAM_KILLER_B"
		BREAK
		
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_5
			tlTeamName = "TEAM_SHADES" 
		BREAK
		
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_5
			tlTeamName = "TEAM_RAINBOWS" 
		BREAK
		
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_5
			tlTeamName = "TEAM_ANIMALS" 
		BREAK
		
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_5
			tlTeamName = "TEAM_MUNCHIES" 
		BREAK
		
		CASE OUTFIT_VERSUS_HIDDEN_TEAM_SPORT_PURPLE_0
			tlTeamName = "TEAM_BOARS" 
		BREAK
			
		CASE OUTFIT_VERSUS_HIDDEN_TEAM_SPORT_ORANGE_0
			tlTeamName = "TEAM_COCKS" 
		BREAK
	
	ENDSWITCH
	
	RETURN tlTeamName
ENDFUNC


PROC DISPLAY_CORONA_UNBALANCED_TEAM_HELP(CORONA_MENU_DATA &coronaMenuData, INT iTeam, BOOL bOverMax, BOOL bInvalidRoles = FALSE)
	
	coronaMenuData.iTeamUnbalanced = iTeam
	coronaMenuData.bCoronaTeamOverMax = bOverMax
	coronaMenuData.bInvalidRoles = bInvalidRoles
	
	TEXT_LABEL_63 tlTeamName
	INT iTeamNo = -1
		
	PRINTLN("[CORONA] DISPLAY_CORONA_UNBALANCED_TEAM_HELP - called...")
		
	IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		OR (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH AND IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)))
	AND g_FMMC_STRUCT.iTeamNames[iTeam] != 0
		tlTeamName = GET_CORONA_TEAM_NAME(iTeam)
		PRINTLN("[CORONA] DISPLAY_CORONA_UNBALANCED_TEAM_HELP - tlTeamName = GET_CORONA_TEAM_NAME(iTeam)")
	ELSE
		iTeamNo = (iTeam+1)
		tlTeamName = GET_CORONA_NAME_FOR_TEAM(coronaMenuData, iTeamNo, iTeam)
		PRINTLN("[CORONA] DISPLAY_CORONA_UNBALANCED_TEAM_HELP - tlTeamName = GET_CORONA_NAME_FOR_TEAM(coronaMenuData, iTeamNo, iTeam)")
	ENDIF
	
	PRINTLN("[CORONA] DISPLAY_CORONA_UNBALANCED_TEAM_HELP - iTeamNo = ", iTeamNo)
	
	IF SHOULD_DISGUISE_TEAM()
		TEXT_LABEL_15 tlTeamNDisguise
		
		IF GET_ACTUAL_TEAM_FOR_CORONA_SLOT(coronaMenuData, coronaMenuData.iCoronaSlotTarget) = coronaMenuData.iTeamUnbalanced
			tlTeamNDisguise = "EXEC_TRD_IMB"
		ELSE
			tlTeamNDisguise = "EXEC_TRD_OTHER"
		ENDIF
		
		tlTeamName = TEXT_LABEL_INTO_STRING(tlTeamNDisguise)
		PRINTLN("[CORONA] DISPLAY_CORONA_UNBALANCED_TEAM_HELP - team should be disguised, team name: 'The other team'")
	ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DESCRIPTION")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CORONA_COLUMN_ONE)
		
		TEXT_LABEL_15 tlUnbTeam = "FM_UNB_TEAM"
		
		IF IS_CORONA_BIT_SET(CORONA_SHOW_MAX_TEAM_DIFFERENCE_HELP)
		
			tlUnbTeam = "BIK_UNB_TEAM"
		
			IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam] = 1
				tlUnbTeam += "1"
			ENDIF
		
			IF iTeamNo = -1
				
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlUnbTeam)
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tlTeamName)
			ELIF iTeamNo = 99
			
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlUnbTeam)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlTeamName)
			ELSE
				tlUnbTeam += "I"
			
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlUnbTeam)
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("FM_UNB_T")
				ADD_TEXT_COMPONENT_INTEGER(iTeamNo)	
			
			ENDIF
			
			ADD_TEXT_COMPONENT_INTEGER(coronaMenuData.iMaxTeamDifferenceAmount)
			
			END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
		ELSE
			IF bInvalidRoles
				tlUnbTeam = "FM_UNB_ROLE"
				
				IF iTeamNo = -1
				
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlUnbTeam)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tlTeamName)
				ELIF iTeamNo = 99
				
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlUnbTeam)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlTeamName)
				ELSE
					tlUnbTeam += "I"
				
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlUnbTeam)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("FM_UNB_T")
					ADD_TEXT_COMPONENT_INTEGER(iTeamNo)	
				
				ENDIF
				
				END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
			ELSE
			
				IF iTeamNo = -1
				
					IF bOverMax
						tlUnbTeam += "O"
						
						IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam] = 1
							tlUnbTeam += "1"
						ENDIF
					ELSE
						IF g_FMMC_STRUCT.iNumPlayersPerTeam[iTeam] = 1
							tlUnbTeam += "1"
						ENDIF
					ENDIF

					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlUnbTeam)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tlTeamName)
				ELIF iTeamNo = 99
				
					IF bOverMax
						tlUnbTeam += "O"
						
						IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam] = 1
							tlUnbTeam += "1"
						ENDIF
					ELSE
						IF g_FMMC_STRUCT.iNumPlayersPerTeam[iTeam] = 1
							tlUnbTeam += "1"
						ENDIF
					ENDIF
					
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlUnbTeam)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlTeamName)
					
				ELSE
					IF bOverMax
						tlUnbTeam += "IO"
						
						IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam] = 1
							tlUnbTeam += "1"
						ENDIF
					ELSE
						tlUnbTeam += "I"
					
						IF g_FMMC_STRUCT.iNumPlayersPerTeam[iTeam] = 1
							tlUnbTeam += "1"
						ENDIF
					ENDIF
				
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlUnbTeam)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("FM_UNB_T")
					ADD_TEXT_COMPONENT_INTEGER(iTeamNo)	
				ENDIF
			
				IF bOverMax
					ADD_TEXT_COMPONENT_INTEGER(g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam])
				ELSE
					ADD_TEXT_COMPONENT_INTEGER(g_FMMC_STRUCT.iNumPlayersPerTeam[iTeam])
				ENDIF
				
				END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
				
			ENDIF
		
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	SET_CORONA_BIT(CORONA_DISPLAY_FLASHING_HELP_TEXT)
	coronaMenuData.tlFlashHelpText = tlTeamName
	
	coronaMenuData.coronaHelpTextTimer = GET_NETWORK_TIME()
	coronaMenuData.iFlashHelpTextState = -2
ENDPROC

/// PURPOSE: Returns if the option is locked for the mode  
FUNC BOOL IS_CORONA_MENU_OPTION_LOCKED_FOR_CHALLENGE(INT iSelection)
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
		
		SWITCH iSelection
			CASE ciRC_HOST_OPTION_NUM_LAPS
			CASE ciRC_HOST_OPTION_VEHICLE_CLASS
			CASE ciRC_HOST_OPTION_TIME_OF_DAY
			CASE ciRC_HOST_OPTION_TRAFFIC
			CASE ciRC_HOST_OPTION_COPS
			CASE ciRC_HOST_OPTION_MAX_WANTED
			CASE ciRC_HOST_OPTION_CUSTOM_VEHICLE
			CASE ciRC_HOST_OPTION_CATCHUP_CODE
			CASE ciRC_HOST_OPTION_SLIP_STREAM
			CASE ciRC_HOST_OPTION_FIXED_CAMERA
			#IF FEATURE_GEN9_EXCLUSIVE
			CASE ciRC_HOST_OPTION_HSW_MODS
			#ENDIF
				RETURN TRUE		
		ENDSWITCH
		
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		SWITCH iSelection
			CASE ciMISSION_HOST_OPTION_DIFFICULTY
			CASE ciMISSION_HOST_OPTION_END_CONDITIONS
			CASE ciMISSION_HOST_OPTION_FIXED_CAMERA
				CASE ciMISSION_HOST_OPTION_ROUNDS
				RETURN TRUE
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DISPLAY_CORONA_WEAPON_LOADOUT_HELP_TEXT(FMMC_SELECTED_ITEMS &sSelection)
	TEXT_LABEL_63 baseLabel = "COR_INVENT_B"
	INT iWeapon, iCount
	WEAPON_TYPE eWeapon
	
	FOR iWeapon = 0 TO FMMC_MAX_INVENTORY_WEAPONS-1
		eWeapon = g_FMMC_STRUCT.sPlayerWeaponInventories[sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPON_LOADOUT]].sWeaponStruct[iWeapon].wtWeapon
		IF eWeapon != WEAPONTYPE_INVALID
			iCount++
		ENDIF
	ENDFOR
	
	PRINTLN("[CORONA] DISPLAY_CORONA_WEAPON_LOADOUT_HELP_TEXT - Total weapon count available for loadout ", sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPON_LOADOUT], " is ", iCount)
	
	// Dont show help if count is 0
	IF iCount = 0
		EXIT
	ENDIF
	
	baseLabel += iCount
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DESCRIPTION")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CORONA_COLUMN_ONE)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(baseLabel)
		
			FOR iWeapon = 0 TO iCount-1
				eWeapon = g_FMMC_STRUCT.sPlayerWeaponInventories[sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPON_LOADOUT]].sWeaponStruct[iWeapon].wtWeapon
				IF eWeapon != WEAPONTYPE_INVALID
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_WEAPON_NAME(eWeapon))
				ENDIF
			ENDFOR
		
		END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE: Displays help text at bottom of column for current selection
PROC DISPLAY_CORONA_HELP_TEXT(CORONA_MENU_DATA &coronaMenuData, INT iMissionType, FMMC_SELECTED_ITEMS &sSelection, INT iSubSelection = -1, BOOL bHost = TRUE)
	
	// If we are locking this option explain why
	IF IS_CORONA_IN_CHALLENGE_MODE()
		IF IS_CORONA_MENU_OPTION_LOCKED_FOR_CHALLENGE(coronaMenuData.iCurrentSelection)
//			IF IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
//				SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_CHAL_LOCK")
//			ELSE
				SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_CHAL_LOCKB")
//			ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	// If we have flashing help already display, then don't override it
	IF IS_CORONA_BIT_SET(CORONA_DISPLAY_FLASHING_HELP_TEXT)
	AND ABSI(coronaMenuData.iFlashHelpTextState) = 2 // can't be overidden

		DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, coronaMenuData.tlFlashHelpText, FALSE, DEFAULT, DEFAULT, DEFAULT, coronaMenuData.iFlashHelpTextOptInt)
	
		EXIT
	ELSE
		// stop current flashing text
		coronaMenuData.iFlashHelpTextState 	= 0
		coronaMenuData.tlFlashHelpText		= ""
		CLEAR_CORONA_BIT(CORONA_DISPLAY_FLASHING_HELP_TEXT)
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
		EXIT
	ENDIF
	
	IF iMissionType = FMMC_TYPE_MISSION
	AND coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_WEAPON_LOADOUT
	AND IS_BIT_SET( g_FMMC_STRUCT.sTeamData[0].iTeamBitSet, ciTEAM_BS_SET_STARTING_INVENTORY_CHOSEN_IN_LOBBY)
		DISPLAY_CORONA_WEAPON_LOADOUT_HELP_TEXT(sSelection)
		EXIT
	ENDIF
	
	TEXT_LABEL_15 tlHelpText
	
	IF NOT bHost
	AND coronaMenuData.iCurrentSelection = -1
		SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "")
		
	ELIF coronaMenuData.iCurrentSelection = ciALL_LOCAL_RADIO_STATION
	
		IF DOES_RACE_HAVE_A_RADIO_MIX()
			SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_RAD2")
		ELSE
			SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_RAD")
		ENDIF
		
	ELIF coronaMenuData.iCurrentSelection = ciALL_LOCAL_VIEW_JOINED_PLAYERS
	
		SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_LSC_H11")
	
	ELIF coronaMenuData.iCurrentSelection = ciALL_LOCAL_WEAPON_LOCKED
	
		SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "LOB_CAT_LCKWH")
		
	ELIF coronaMenuData.iCurrentSelection = ciALL_LOCAL_WEAPON
	
		SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_WPN")
	
	ELIF coronaMenuData.iCurrentSelection = ciALL_LOCAL_PURCHASE_AMMO
	
		SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_WPNP")
	
	ELIF coronaMenuData.iCurrentSelection = ciALL_LOCAL_PURCHASE_ARMOR
	
		SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_ARMP")
		
	ELIF coronaMenuData.iCurrentSelection = ciALL_LOCAL_WEAPON_FILL_ALL
	
		SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_FILLA")
		
	ELIF coronaMenuData.iCurrentSelection = ciALL_HOST_OPTION_CONTINUE
	
		IF IS_PLAYER_SCTV(PLAYER_ID())
		AND CAN_SCTV_LAUNCH_THIS_TRANSITION_SESSION()
			SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_PLAY")
		ELIF IS_CORONA_BIT_SET(CORONA_CONTINUE_IS_PLAY)
			IF IS_CORONA_IN_CHALLENGE_MODE()
				SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_CHAL_PLAY")
			ELSE
				SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_PLAY")
			ENDIF
		ELSE
			IF IS_CORONA_IN_CHALLENGE_MODE()
			OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
				SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_CHAL_CONFIRM")
			ELSE
				IF IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
					SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_CONT")
				ELSE
					SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_CONTC")
				ENDIF
			ENDIF
		ENDIF
	
	ELIF coronaMenuData.iCurrentSelection = ciALL_HOST_OPTION_AUTOFILL
//		IF IS_CORONA_BIT_SET(CORONA_AUTOFILL_CAN_CONTINUE)
//			SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_PLAY")
//		ELSE
		SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_ISC_AUTO")
//		ENDIF
	ELIF coronaMenuData.iCurrentSelection = ciALL_CREW_CHALLENGE_CASH
	
		IF IS_CORONA_IN_CHALLENGE_MODE()
			SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_CREW_CHAL")
		ELSE
			SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_CREW_H2H")
		ENDIF
		
	ELIF coronaMenuData.iCurrentSelection = ciALL_HOST_OPTION_CLIENT_INVITES
	
		IF IS_THIS_A_MINI_GAME(g_FMMC_STRUCT.iMissionType)
			SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_LSC_HCINVA")
		ELSE
			SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "FM_LSC_HCINV")
		ENDIF
	ELSE
	
		IF coronaMenuData.iCurrentSelection = -1
			PRINTLN("[CORONA] DISPLAY_CORONA_HELP_TEXT - coronaMenuData.iCurrentSelection = -1, no help set")
			SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "")
			EXIT
		ENDIF
	
		SWITCH GET_CORONA_STATUS()
		
			CASE CORONA_STATUS_INTRO
		
				SWITCH iMissionType
					CASE FMMC_TYPE_RACE
						
						// If we are looking at client option pick generic string
						IF coronaMenuData.iCurrentSelection = ciRC_CLIENT_OPTION_PARTNER
						OR coronaMenuData.iCurrentSelection = ciRC_CLIENT_OPTION_ROLE
							tlHelpText = "FM_LSC_HR"
						ELSE
							tlHelpText = "FM_ISC_HR"
						ENDIF
						
						tlHelpText += coronaMenuData.iCurrentSelection
						
						// If we are looking at partner and they have a custom vehicle - override
						IF coronaMenuData.iCurrentSelection = ciRC_CLIENT_OPTION_PARTNER
							IF g_iMyPreferedPartner >= 0
								IF DOES_TRANSITION_PLAYER_HAVE_CUSTOM_VEHICLE_FOR_CLASS(g_iMyPreferedPartner, sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])
									tlHelpText = "FM_LSC_HRC"
								ENDIF
							ENDIF
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_TYPE
							tlHelpText += "_"
							tlHelpText += sSelection.iSelection[ciRC_HOST_OPTION_TYPE]
							
							IF HAS_PLAYER_SENT_AN_INVITE()
								tlHelpText = "FM_ISC_LOCK"
							ENDIF
							
							// Target Assault overrides GTA race type if a target assault race, so show new help for target assault races
							IF IS_TARGET_ASSAULT_RACE()
								IF sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_GTA
									tlHelpText = "FM_TARG_EXPL"
								ELIF sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_NON_CONTACT
									tlHelpText = "FM_TARG_EXPL2"
								ENDIF
							ENDIF
							
							IF IS_ARENA_WARS_JOB()
								SWITCH g_FMMC_STRUCT.iRaceType
									CASE FMMC_RACE_TYPE_ARCADE_RACE
									CASE FMMC_RACE_TYPE_ARCADE_RACE_P2P
										IF sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_STANDARD
											tlHelpText = "FM_AW_ARC_STD"
										ELIF sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_GTA
											tlHelpText = "FM_AW_ARC_GTA"
										ENDIF
									BREAK
									CASE FMMC_RACE_TYPE_DESTRUCTION_DERBY
									CASE FMMC_RACE_TYPE_DESTRUCTION_DERBY_P2P
										IF sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_STANDARD
											tlHelpText = "FM_AW_DES_STD"
										ELIF sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_GTA
											tlHelpText = "FM_AW_DES_GTA"
										ENDIF
									BREAK
								ENDSWITCH	
							ENDIF
							
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_FIXED_CAMERA
							tlHelpText = "LOB_H_FCP"
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_DESTROY_LOSER
							tlHelpText += "_"
							tlHelpText += sSelection.iSelection[ciRC_HOST_OPTION_DESTROY_LOSER]
						ENDIF
						
//						#IF FEATURE_IMPORT_EXPORT
						IF coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_AGGREGATE_POS
							tlHelpText = "BIK_AGPOS_HLP"
						ENDIF
//						#ENDIF

						IF coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_NUM_LAPS
							IF sSelection.iSelection[ciRC_HOST_OPTION_AGGREGATE_POS] = 1
								tlHelpText = "FM_AGPOS_HRL"
							ENDIF
						ENDIF
				
						IF coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_CUSTOM_VEHICLE
							IF IS_TRANSFORM_RACE()
								tlHelpText = "FM_TRANSCUSTOM"
							ENDIF
							IF g_FMMC_STRUCT.iRaceCustomVehSetting = ciRCVS_RESTRICTED
								tlHelpText = "FM_ISC_HR9b"
							ENDIF
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_MAX_WANTED
							tlHelpText = "COR_MAX_WANTH"
						ENDIF
						
						#IF FEATURE_GEN9_EXCLUSIVE
						IF coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_HSW_MODS
							tlHelpText = "COR_HSW_MODS"
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_VEHICLE_CLASS
						AND IS_THIS_A_HSW_RACE()
							SWITCH sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS]
								CASE FMMC_VEHICLE_CLASS_HSW_T1
									tlHelpText = "COR_HSW_HT1"
								BREAK
								CASE FMMC_VEHICLE_CLASS_HSW_T2
									tlHelpText = "COR_HSW_HT2"
								BREAK
								CASE FMMC_VEHICLE_CLASS_HSW_T3
									tlHelpText = "COR_HSW_HT3"
								BREAK
								CASE FMMC_VEHICLE_CLASS_HSW
									tlHelpText = "COR_HSW_HTA"
								BREAK
							ENDSWITCH
						ENDIF
						#ENDIF
						
						SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, tlHelpText)
					BREAK
				
					CASE FMMC_TYPE_DEATHMATCH
						tlHelpText = "FM_ISC_HD"
						
						tlHelpText += coronaMenuData.iCurrentSelection										
						
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_TYPE
						
							IF sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_VEHICLE
								tlHelpText += "V"
							ENDIF
							
							IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_TYPE
								IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
									tlHelpText = "FM_H2H_LOCK"
								ENDIF
							ENDIF
							
							IF HAS_PLAYER_SENT_AN_INVITE()
								tlHelpText = "FM_ISC_LOCK"
							ENDIF
							
							IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
							OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
							OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType)
								tlHelpText = "AW_COR_LTS_h"
							ENDIF
							
							IF IS_THIS_A_KING_OF_THE_HILL_CORONA(FMMC_TYPE_DEATHMATCH, sSelection.iSelection[ciDM_HOST_OPTION_TYPE])
								tlHelpText = "FM_COR_KH_TYPE"
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LockedDeathmatchType)
								tlHelpText = "FM_ISC_HD0_L"
							ENDIF
							
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_NUM_TEAMS
							IF IS_THIS_A_KING_OF_THE_HILL_CORONA(FMMC_TYPE_DEATHMATCH, sSelection.iSelection[ciDM_HOST_OPTION_TYPE])
								tlHelpText = "FM_COR_KH_TEAM"
							ENDIF
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_TIME_LIMIT
							IF IS_THIS_A_KING_OF_THE_HILL_CORONA(FMMC_TYPE_DEATHMATCH, sSelection.iSelection[ciDM_HOST_OPTION_TYPE])
								tlHelpText = "FM_COR_KH_TIME"
							ENDIF
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_TARGET_SCORE
							IF IS_THIS_A_KING_OF_THE_HILL_CORONA(FMMC_TYPE_DEATHMATCH, sSelection.iSelection[ciDM_HOST_OPTION_TYPE])
								tlHelpText = "FM_COR_KH_TARG"
							ELIF NOT IS_THIS_LEGACY_DM_CONTENT()
								tlHelpText += "b"
							ENDIF
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_GAMERTAGS
							IF IS_THIS_A_KING_OF_THE_HILL_CORONA(FMMC_TYPE_DEATHMATCH, sSelection.iSelection[ciDM_HOST_OPTION_TYPE])
								tlHelpText = "FM_COR_KH_TAGS"
							ENDIF
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_TEAM_BAL
							IF IS_THIS_A_KING_OF_THE_HILL_CORONA(FMMC_TYPE_DEATHMATCH, sSelection.iSelection[ciDM_HOST_OPTION_TYPE])
								tlHelpText = "FM_COR_KH_BAL"
							ENDIF
						ENDIF
						
						// Share text label (1528635)
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_WEATHER
							tlHelpText = "FM_ISC_HR6"
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_TRAFFIC
							tlHelpText = "FM_ISC_HR7"
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_WEAPONS
							IF IS_THIS_A_KING_OF_THE_HILL_CORONA(FMMC_TYPE_DEATHMATCH, sSelection.iSelection[ciDM_HOST_OPTION_TYPE])
							AND sSelection.iSelection[ciDM_HOST_OPTION_WEAPONS] = ciDM_WEAPON_DEFAULT
								tlHelpText = "FM_COR_KH_WEAP"
							ELSE
								tlHelpText += sSelection.iSelection[ciDM_HOST_OPTION_WEAPONS]
							ENDIF
						ENDIF
							
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_FIXED_CAMERA
							tlHelpText = "LOB_H_FCP"
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_NUM_LIVES
							tlHelpText = "COR_DMNL_H"
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_ROUNDS
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_EnableDeathmatchRoundsForLobby)
							tlHelpText = "FM_ISC_HD13L"
						ENDIF
						
						SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, tlHelpText)
					BREAK
					
					CASE FMMC_TYPE_MISSION
//						IF coronaMenuData.iCurrentSelection = ciMISSION_CLIENT_OPTION_TEAM
//							tlHelpText = "FM_LSC_HM"
//						ELSE
							tlHelpText = "FM_ISC_HM"
//						ENDIF
	
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_CLOTHING_SETUP
							IF NOT IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
								tlHelpText += "V"
							ENDIF
							
								IF IS_LOADED_MISSION_TYPE_FLOW_MISSION()
									tlHelpText += "L"
								ENDIF
						ENDIF
					
						tlHelpText += coronaMenuData.iCurrentSelection
						
						IF (coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_TARGET_SCORE
						AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType))
						OR (coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_TARGET_SCORE
						AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_POINTS_INSTEAD_OF_KILLS))
							tlHelpText += "b"
						ENDIF
						
						// Share text label (1528635)
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_WEATHER
							tlHelpText = "FM_ISC_HR6"
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_END_CONDITIONS
							tlHelpText += "_"
							tlHelpText += sSelection.iSelection[ciMISSION_HOST_OPTION_END_CONDITIONS]
						ENDIF
						
						// Use Deathmatch text labels for this weapon text
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_WEAPONS
							IF sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPONS] = ciDM_WEAPON_DEFAULT
								tlHelpText = "FM_ISC_HM"
								tlHelpText += ciMISSION_HOST_OPTION_WEAPONS
								tlHelpText += sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPONS]
							ELSE
								tlHelpText = "FM_ISC_HD"
								tlHelpText += ciDM_HOST_OPTION_WEAPONS
								tlHelpText += sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPONS]
							ENDIF
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_DIFFICULTY
							IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
								tlHelpText = "FM_ISC_HHD"
								
								IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FINALE_FLAG)
									tlHelpText = "FM_ISC_HHD2"
															
									IF sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY] = DIFF_EASY
										tlHelpText = "HEC_CORONA"
									ELIF sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY] = DIFF_NORMAL
										tlHelpText = "HNC_CORONA"
									ENDIF
								ELSE
									IF sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY] != DIFF_HARD
										tlHelpText = "HNC_CORONA"
									ENDIF									
								ENDIF
							ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
								IF sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY] != DIFF_HARD
									tlHelpText = "GOFA_COR_DIFF"
								ENDIF	
							ENDIF
						ENDIF
						
						// Unique strings for each different customisation option
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_OUTFIT_CUSTOMISATION
							
							tlHelpText += "_"
							tlHelpText += GET_CORONA_OUTFIT_ACCESS_LEVEL()
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciLOCK_STYLE_ALL_TEAMS)
							AND GET_CORONA_OUTFIT_ACCESS_LEVEL() = ci_HEIST_CORONA_OUTFIT_ACCESS_OUTFITS
								tlHelpText += "B"
							ENDIF
						ENDIF
						
						
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_FIXED_CAMERA
							tlHelpText = "LOB_H_FCP"
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_SUDDEN_DEATH
							IF IS_CORONA_MISSION_SUDDEN_DEATH_AVAILABLE()
								tlHelpText = "LOB_H_MSD"
							ELSE
								tlHelpText = "LOB_H_PUT"
							ENDIF
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_VEHICLE_CLASS
							tlHelpText = "LOB_H_VC"
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_WEAPON_LOADOUT
							tlHelpText = "FM_ISC_HM"
							tlHelpText += coronaMenuData.iCurrentSelection
							
							// Alt 3
							IF sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPON_LOADOUT] = 3
								tlHelpText += "d"
							// Alt 2
							ELIF sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPON_LOADOUT] = 2
								tlHelpText += "c"
							// Alt 1
							ELIF sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPON_LOADOUT] = 1
								tlHelpText += "b"
							// Standard
							ELSE
								tlHelpText += "a"
							ENDIF
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_EXIT_TYPE
						AND SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
							tlHelpText = "IE_ETYPE_DESC"
						ENDIF
						
						IF coronaMenuData.iCurrentSelection =  27 // ciMISSION_HOST_OPTION_NUMBER_OF_LAPS & ciMISSION_HOST_OPTION_EXIT_TYPE
							IF NOT SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
								OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
									tlHelpText = "FM_GR_ATS"
								ENDIF
							ENDIF
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciMISSION_CLIENT_OPTION_SEAT
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
							OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
								tlHelpText = "FMMC_SM_COVS"
							ENDIF
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_VEHICLE_LIST
							IF sSelection.iSelection[ciMISSION_HOST_OPTION_VEHICLE_LIST] = 1
								// "Hydra, Rogue, Pyro, Akula, Buzzard Attack Chopper, Thruster."
								tlHelpText = "FMMC_AQ_VL2"
							ELSE
								// "Lazer, Molotok, Nokota, Savage, Hunter, Thruster."
								tlHelpText = "FMMC_AQ_VL1"
							ENDIF
						ENDIF
						
						SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, tlHelpText)
					BREAK
					
					CASE FMMC_TYPE_MG_TENNIS
						
						tlHelpText = "FM_ISC_HT"					
						tlHelpText += coronaMenuData.iCurrentSelection
						
						SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, tlHelpText)
					BREAK
					
					CASE FMMC_TYPE_MG_GOLF
						
						tlHelpText = "FM_ISC_HG"					
						tlHelpText += coronaMenuData.iCurrentSelection
						
						// Share text label (1528635)
						IF coronaMenuData.iCurrentSelection = ciGOLF_HOST_OPTION_WEATHER
							tlHelpText = "FM_ISC_HR6"
						ENDIF
						
						SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, tlHelpText)
					BREAK
					
					CASE FMMC_TYPE_MG_DARTS
						
						tlHelpText = "FM_ISC_HDS"					
						tlHelpText += coronaMenuData.iCurrentSelection
						
						SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, tlHelpText)
					BREAK
					
					CASE FMMC_TYPE_MG_SHOOTING_RANGE
						
						tlHelpText = "FM_ISC_HSR"					
						tlHelpText += coronaMenuData.iCurrentSelection
						
						SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, tlHelpText)
					BREAK
					
					CASE FMMC_TYPE_BASE_JUMP
						
						tlHelpText = "FM_ISC_HBJ"					
						tlHelpText += coronaMenuData.iCurrentSelection
						
						// Share text label (1528635)
						IF coronaMenuData.iCurrentSelection = ciPARA_HOST_OPTION_WEATHER
							tlHelpText = "FM_ISC_HR6"
						ENDIF
						
						IF coronaMenuData.iCurrentSelection = ciPARA_HOST_OPTION_FIXED_CAMERA
							tlHelpText = "LOB_H_FCP"
						ENDIF
						
						SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, tlHelpText)						
					BREAK
					
					DEFAULT
						SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, tlHelpText)
					BREAK
				
				ENDSWITCH
			
			BREAK
		
			CASE CORONA_STATUS_LOBBY
			CASE CORONA_STATUS_H2H_LOBBY
				
				IF coronaMenuData.iCurrentSelection = ciCORONA_LOBBY_START_GAME
					IF IS_CORONA_IN_CHALLENGE_MODE()
						tlHelpText = "FM_CHAL_PLAY"
					ELIF IS_CORONA_IN_HEAD_2_HEAD_MODE()
						tlHelpText = "FM_H2H_PLAY"
					ELSE
						tlHelpText = "FM_LSC_H"
						tlHelpText += coronaMenuData.iCurrentSelection
					ENDIF
				ELSE
				
					tlHelpText = "FM_LSC_H"
					tlHelpText += coronaMenuData.iCurrentSelection
								
					IF iMissionType = FMMC_TYPE_RACE
						IF coronaMenuData.iCurrentSelection = ciRC_CLIENT_OPTION_PARTNER
						OR coronaMenuData.iCurrentSelection = ciRC_CLIENT_OPTION_ROLE
							tlHelpText = "FM_LSC_HR"
							tlHelpText += coronaMenuData.iCurrentSelection
							
							// If we are looking at partner and they have a custom vehicle - override
							IF coronaMenuData.iCurrentSelection = ciRC_CLIENT_OPTION_PARTNER
								IF g_iMyPreferedPartner >= 0
									IF DOES_TRANSITION_PLAYER_HAVE_CUSTOM_VEHICLE_FOR_CLASS(g_iMyPreferedPartner, sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])
										tlHelpText = "FM_LSC_HRC"
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF
					ELIF iMissioNType = FMMC_TYPE_MISSION
						IF coronaMenuData.iCurrentSelection = ciMISSION_CLIENT_OPTION_SEAT
							tlHelpText = "FMMC_SM_COVS"
						ENDIF
					ENDIF
					
					// If we are in a sub menu, append selection
					IF iSubSelection != -1
						tlHelpText += "_"
						tlHelpText += iSubSelection
					ENDIF
					
					IF ARE_STRINGS_EQUAL(tlHelpText, "FM_LSC_H5")
					OR ARE_STRINGS_EQUAL(tlHelpText, "FM_LSC_H6")
					OR ARE_STRINGS_EQUAL(tlHelpText, "FM_LSC_H5_1")
					OR ARE_STRINGS_EQUAL(tlHelpText, "FM_LSC_H6_1")
					OR ARE_STRINGS_EQUAL(tlHelpText, "FM_LSC_H12")
						IF IS_THIS_A_MINI_GAME(g_FMMC_STRUCT.iMissionType)
							tlHelpText += "A"
						ENDIF
					ENDIF
					
					// Catch any special cases for challenges and head 2 heads
					IF IS_CORONA_IN_CHALLENGE_MODE()
						IF coronaMenuData.iCurrentSelection = ciCORONA_LOBBY_CREW_MEMBERS
							 //IF IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
							// 	tlHelpText = "FM_CHAL_CREWI"
							// ELSE
								tlHelpText = "FM_CHAL_CREWIB"
							//ENDIF
						ENDIF
					ENDIF
					
					// Catch any special cases for challenges and head 2 heads
					IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
						IF coronaMenuData.iCurrentSelection = ciCORONA_LOBBY_CREW_MEMBERS
							tlHelpText = "FM_CHAL_H2HIC"
						ENDIF
					ENDIF
				ENDIF
				
				SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, tlHelpText)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL MAINTAIN_CORONA_FLASHING_HELP_TEXT(CORONA_MENU_DATA &coronaMenuData)

	// If our help is being displayed
	IF IS_CORONA_BIT_SET(CORONA_DISPLAY_FLASHING_HELP_TEXT)
	
		IF coronaMenuData.iFlashHelpTextState > 0
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), coronaMenuData.coronaHelpTextTimer)) >= PICK_INT(ARE_STRINGS_EQUAL(coronaMenuData.tlFlashHelpText, "FMAW_TUTHLP"), DEFAULT_HELP_TEXT_TIME+5000, DEFAULT_HELP_TEXT_TIME) 
			OR IS_STRING_NULL_OR_EMPTY(coronaMenuData.tlFlashHelpText)
			
				coronaMenuData.tlFlashHelpText 		= ""
				coronaMenuData.iFlashHelpTextState	= 0
				CLEAR_CORONA_BIT(CORONA_DISPLAY_FLASHING_HELP_TEXT)
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SET_CORONA_FRONTEND_HELP_TEXT(CORONA_MENU_DATA &coronaMenuData, INT iColumn, STRING tlHelpText)
	
	IF NOT IS_CORONA_BIT_SET(CORONA_DISPLAY_FLASHING_HELP_TEXT)
	OR ABSI(coronaMenuData.iFlashHelpTextState) != 2 // can be overridden
	
		// stop current flashing text
		coronaMenuData.iFlashHelpTextState 	= 0
		coronaMenuData.tlFlashHelpText		= ""
	
		SET_FRONTEND_HELP_TEXT(iColumn, tlHelpText)
	ELSE
		IF IS_CORONA_BIT_SET(CORONA_MISSION_DELAYED_FOR_TEAMS)
			DISPLAY_CORONA_UNBALANCED_TEAM_HELP(coronaMenuData, coronaMenuData.iTeamUnbalanced, coronaMenuData.bCoronaTeamOverMax, coronaMenuData.bInvalidRoles)
		ELSE
			DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, coronaMenuData.tlFlashHelpText, FALSE, DEFAULT, DEFAULT, DEFAULT, coronaMenuData.iFlashHelpTextOptInt)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Sets up help text about who is doing something in the corona
PROC SET_CORONA_FRONTEND_HELP_TEXT_WITH_PLAYER_NAME(CORONA_MENU_DATA &coronaMenuData, INT iColumn, STRING tlHelpText, PLAYER_INDEX playerID)
	IF NOT IS_CORONA_BIT_SET(CORONA_DISPLAY_FLASHING_HELP_TEXT)
	OR ABSI(coronaMenuData.iFlashHelpTextState) != 2 // can be overridden
	
		// stop current flashing text
		coronaMenuData.iFlashHelpTextState 	= 0
		coronaMenuData.tlFlashHelpText		= ""
	
		SET_FRONTEND_HELP_TEXT(iColumn, tlHelpText, DEFAULT, DEFAULT, GET_PLAYER_NAME(playerID))
	ELSE
		IF IS_CORONA_BIT_SET(CORONA_MISSION_DELAYED_FOR_TEAMS)
			DISPLAY_CORONA_UNBALANCED_TEAM_HELP(coronaMenuData, coronaMenuData.iTeamUnbalanced, coronaMenuData.bCoronaTeamOverMax, coronaMenuData.bInvalidRoles)
		ELSE
			DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, coronaMenuData.tlFlashHelpText, FALSE, DEFAULT, DEFAULT, DEFAULT, coronaMenuData.iFlashHelpTextOptInt)
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_CORONA_NEW_JOB_HELP()
	
	SWITCH g_FMMC_STRUCT.iMissionType
		CASE FMMC_TYPE_RACE			RETURN "FM_JOB_INIR"
		CASE FMMC_TYPE_DEATHMATCH	
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_DM_TYPE_KOTH
			OR g_FMMC_STRUCT.iMissionSubType = FMMC_DM_TYPE_TEAM_KOTH
				RETURN "FM_JOB_INIKH"
			ENDIF
			RETURN "FM_JOB_INIDM"
		CASE FMMC_TYPE_MISSION		RETURN "FM_JOB_INIM"
		CASE FMMC_TYPE_BASE_JUMP	RETURN "FM_JOB_INIP"
		CASE FMMC_TYPE_SURVIVAL		RETURN "FM_JOB_INIS"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

//Should this option be blocked/disabled
FUNC BOOL SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(BOOL bCheckHead2Head = TRUE)
	IF IS_CORONA_IN_CHALLENGE_MODE()
//	OR IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
//	OR IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
//	OR IS_THIS_TRANSITION_SESSION_IS_A_LIVESTREAM_PLAYLIST()
		RETURN TRUE
	ENDIF
	//Do we need to check head to head
	IF bCheckHead2Head
		IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
			RETURN TRUE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

PROC PROCESS_CORONA_NEW_JOB_HELP_TEXT(CORONA_MENU_DATA &coronaMenuData, BOOL bHost)
	
	// Don't show this if its a challenge mode
	IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED()
		EXIT
	ENDIF
	
	IF bHost
		IF NOT IS_CORONA_BIT_SET(CORONA_DISPLAYED_NEW_JOB_CORONA_HELP)
			IF NOT IS_CORONA_BIT_SET(CORONA_DISPLAY_FLASHING_HELP_TEXT)
				
				IF NOT DONE_CORONA_NEW_JOB_HELP()
					
					STRING strHelpText = GET_CORONA_NEW_JOB_HELP()
					
					IF NOT IS_STRING_NULL_OR_EMPTY(strHelpText)
						PRINTLN("[CORONA] PROCESS_CORONA_NEW_JOB_HELP_TEXT - Displaying our flashing help text for new job")
					
						DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, strHelpText)
					
						SET_DONE_CORONA_NEW_JOB_HELP(TRUE)
					ENDIF
					
					SET_CORONA_BIT(CORONA_DISPLAYED_NEW_JOB_CORONA_HELP)
				ELSE
					IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_BASE_JUMP
						
						IF NOT DONE_CORONA_PARACHTING_HELP()
							PRINTLN("[CORONA] PROCESS_CORONA_NEW_JOB_HELP_TEXT - Displaying our flashing help text for parachuting")
							DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_PARA_H")
					
							SET_DONE_CORONA_PARACHUTING_HELP(TRUE)
						ENDIF
					
						SET_CORONA_BIT(CORONA_DISPLAYED_NEW_JOB_CORONA_HELP)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_STORE_DIFFICULTY_OPTION()

	IF IS_THIS_A_FIXER_SHORT_TRIP()
		RETURN TRUE
	ENDIF

	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
	OR HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_MISSION_FINALE()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_HideCoronaDifficultyOption)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE: Sets the Globals used in activities to the options in the menu 
FUNC BOOL SAVE_CORONA_OPTIONS_TO_GLOBALS(INT iMissionType, FMMC_SELECTED_ITEMS &sSelection, MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)

	UNUSED_PARAMETER(iMissionType)

	PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - saving menu selections to globals, for mission type: ", g_FMMC_STRUCT.iMissionType, " and subtype = ", g_FMMC_STRUCT.iMissionSubType, "; iMissionType = ", iMissionType)
	DEBUG_PRINTCALLSTACK()
	
	#IF IS_DEBUG_BUILD
	INT i
	FOR i = 0 TO (FMMC_MAX_MENU_ITEMS-1) STEP 1
		PRINTLN("[CORONA] 		SAVE_CORONA_OPTIONS_TO_GLOBALS - Menu Option:   ", i)
		PRINTLN("[CORONA] 		SAVE_CORONA_OPTIONS_TO_GLOBALS - Menu Value:    ", sSelection.iSelection[i])
		PRINTLN("[CORONA] 		SAVE_CORONA_OPTIONS_TO_GLOBALS ------------------------")
	ENDFOR
	#ENDIF
	INT iTeamCap
	// Reset any data we need to:
	IF NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		PRINTLN("[CORONA] 		NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA ------------------------")
		GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iNumberOfRounds = 0
		GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams = 0
		PRINTLN("[TS] [SST] * SAVE_CORONA_OPTIONS_TO_GLOBALS GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams = ", 0)
		iTeamCap = g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams
		g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams = 0
		g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds = 0
	ENDIF
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	
		IF SHOULD_STORE_DIFFICULTY_OPTION()
			g_FMMC_STRUCT.iDifficulity				= sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY]
			PRINTLN("[CORONA] 		SAVE_CORONA_OPTIONS_TO_GLOBALS - setting difficulty to ", g_FMMC_STRUCT.iDifficulity)
		ELSE
			PRINTLN("[CORONA] 		SAVE_CORONA_OPTIONS_TO_GLOBALS - not setting difficulty.")
		ENDIF
		IF NOT HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_MISSION_FINALE()
			g_FMMC_STRUCT.iWeather					=(sSelection.iSelection[ciMISSION_HOST_OPTION_WEATHER])
		ENDIF
		g_FMMC_STRUCT.iNumberOfTeams 			= g_FMMC_STRUCT.iMaxNumberOfTeams
		g_FMMC_STRUCT.iBalanceTeams				= sSelection.iSelection[ciMISSION_HOST_OPTION_TEAM_BAL]							
		g_FMMC_STRUCT.iTimeOfDay				= sSelection.iSelection[ciMISSION_HOST_OPTION_TIME_OF_DAY]
		g_FMMC_STRUCT.bHideBlips				= (sSelection.iSelection[ciMISSION_HOST_OPTION_DISABLE_BLIPS] = 1)
		g_FMMC_STRUCT.iFixedCamera				= sSelection.iSelection[ciMISSION_HOST_OPTION_FIXED_CAMERA]
		
		IF IS_ARENA_WARS_JOB(TRUE)
			g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = sSelection.iSelection[ciMISSION_HOST_OPTION_WEATHER]
			PRINTLN("[CORONA][CORONA_ARENA_LIGHTING] SAVE_CORONA_OPTIONS_TO_GLOBALS - Mission, g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = ", GET_ARENA_LIGHTING_STRING(g_FMMC_STRUCT.sArenaInfo.iArena_Lighting))
		ENDIF
		
		IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
		
			CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseCTFSuddenDeath)
			CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseCTFAllPoints)
		
			IF sSelection.iSelection[ciMISSION_HOST_OPTION_END_CONDITIONS] = ciMISSION_SUDDEN_DEATH
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseCTFSuddenDeath)
			ELIF sSelection.iSelection[ciMISSION_HOST_OPTION_END_CONDITIONS] = ciMISSION_ALL_POINTS
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseCTFAllPoints)
			ENDIF
						
		ENDIF
		
		IF IS_MISSION_VALID_FOR_FORCED_WEAPON()
		AND NOT IS_THIS_MISSION_OF_TYPE_HEIST(g_FMMC_STRUCT.iMissionType, sLaunchMissionDetails.bIsHeistMission  , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
		
			CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked) 
			CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked)
		
			// Set the correct global bit so the DM / spawning systems pick them up
			IF sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPONS] = ciDM_WEAPON_LOCKED
				PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Valid mission, weapons locked to single weapon")
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked)
			ELIF sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPONS] = ciDM_WEAPON_STARTING_LOCKED
				PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Valid mission, weapons locked to DM weapons")
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked)
			ENDIF
		ENDIF
		
		IF NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
			IF IS_MISSION_VALID_FOR_ROUND(g_FMMC_STRUCT.iMissionSubType)
				g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds			= (GET_CORONA_NUMBER_OF_ROUND(sSelection.iSelection[ciMISSION_HOST_OPTION_ROUNDS])-1)			
				PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Versus / LTS setting rounds to (one less as additional to standard play): ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds)
				g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams = iTeamCap
				GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams = iTeamCap
				PRINTLN("[TS] [SST] * SAVE_CORONA_OPTIONS_TO_GLOBALS GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams = ", iTeamCap)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE)
			IF sSelection.iSelection[ciMISSION_HOST_OPTION_MISSION_TYPE] = 0
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_TEAM_LIVES_SETTING)
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE_CORONA)
				PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - sSelection.iSelection[ciMISSION_HOST_OPTION_MISSION_TYPE] = ", sSelection.iSelection[ciMISSION_HOST_OPTION_MISSION_TYPE], "; SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE_CORONA)")
			ELSE
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_TEAM_LIVES_SETTING)
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE_CORONA)
				PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - sSelection.iSelection[ciMISSION_HOST_OPTION_MISSION_TYPE] = ", sSelection.iSelection[ciMISSION_HOST_OPTION_MISSION_TYPE], "; CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE_CORONA)")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciTLAD_HIGHEST_SCORE_TOGGLE)
			IF sSelection.iSelection[ciMISSION_HOST_OPTION_HIGHEST_SCORE] = 0
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciTLAD_HIGHEST_SCORE_TOGGLE_CORONA)
			ELSE
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciTLAD_HIGHEST_SCORE_TOGGLE_CORONA)
			ENDIF
		ENDIF
		
		// Save out our round data to BD so players who JIP can pick up on this.
		GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iNumberOfRounds		= g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds
		
		IF IS_CORONA_MISSION_SUDDEN_DEATH_AVAILABLE()
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Mission has sudden death available")

			IF sSelection.iSelection[ciMISSION_HOST_OPTION_SUDDEN_DEATH] = CORONA_MISSION_SUDDEN_DEATH_OPT_1		// Picked first option Over Line, clear enemies
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_NO_ENEMY_TEAMS)
				PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Mission Sudden Death: Clear ciSUDDEN_DEATH_THAT_ENDS_WHEN_NO_ENEMY_TEAMS")
			ELSE
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_MORE_PEOPLE_ARE_IN_LOC)	// Picked all enemies, clear over the line
				PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Mission Sudden Death: Clear ciSUDDEN_DEATH_THAT_ENDS_WHEN_MORE_PEOPLE_ARE_IN_LOC")
			ENDIF
		ELSE
			// Check the flag
			// Set the bool
			IF IS_CORONA_MISSION_PICKUP_OPTION_AVAILABLE()
				IF sSelection.iSelection[ciMISSION_HOST_OPTION_SUDDEN_DEATH] = CORONA_MISSION_PICKUPS_RANDOM
					g_FMMC_STRUCT_ENTITIES.bRandomPickupTypes = TRUE
					PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Mission PickUp Option: g_FMMC_STRUCT_ENTITIES.bRandomPickupTypes = TRUE")
				ELSE
					g_FMMC_STRUCT_ENTITIES.bRandomPickupTypes = FALSE
					PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Mission PickUp Option: g_FMMC_STRUCT_ENTITIES.bRandomPickupTypes = FALSE")
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSELECTABLE_TEAM_NUMBERS)
			
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciAUTO_NUMBER_OF_TEAMS)
				IF sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS] = -1
					
					IF NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
						IF NOT NETWORK_IS_ACTIVITY_SESSION()
						OR (HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
						AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType))
							PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Mission: sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS] = -1, calculating number of teams from number of players in corona")
						
							g_bAutoNumTeamsSelected = TRUE
							
							PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Mission: g_bAutoNumTeamsSelected has been set ")
							INT iNumPlayers = g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers 
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
							OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
								IF iNumPlayers >= 0 AND iNumPlayers <= 4
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_TWO_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_TWO_TEAMS
								ELIF iNumPlayers >= 5 AND iNumPlayers <= 6
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_THREE_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_THREE_TEAMS
								ELSE
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_FOUR_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_FOUR_TEAMS
								ENDIF
							ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
							OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_AVAILABLE_TEAMS_BALANCING)
								IF iNumPlayers >= 0 AND iNumPlayers <= 2
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_TWO_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_TWO_TEAMS
								ELIF iNumPlayers = 3
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_THREE_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_THREE_TEAMS
								ELSE
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_FOUR_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_FOUR_TEAMS
								ENDIF
							ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
								IF iNumPlayers >= 0 AND iNumPlayers <= 5
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_TWO_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_TWO_TEAMS
								ELIF iNumPlayers >= 6 and iNumPlayers <= 7
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_THREE_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_THREE_TEAMS
								ELIF iNumPlayers = 8
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_FOUR_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_FOUR_TEAMS
								ELIF iNumPlayers = 9
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_THREE_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_THREE_TEAMS
								ELSE
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_FOUR_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_FOUR_TEAMS
								ENDIF
							ELSE
								IF iNumPlayers >= 4 AND iNumPlayers <= 8
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_TWO_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_TWO_TEAMS
								ELIF iNumPlayers >= 9 AND iNumPlayers <= 12
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_THREE_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_THREE_TEAMS
								ELSE
									g_FMMC_STRUCT.iNumberOfTeams = ciCORONA_FOUR_TEAMS
									g_FMMC_STRUCT.iMaxNumberOfTeams = ciCORONA_FOUR_TEAMS
								ENDIF
							ENDIF
							PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Mission: Num players in corona: ", iNumPlayers, "; Number of teams should be: ", g_FMMC_STRUCT.iNumberOfTeams)
						ELSE
							PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - NETWORK_IS_ACTIVITY_SESSION()	OR (!HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()	AND/OR !IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType))")
						ENDIF
					ELSE
						PRINTLN("[SST] * IS_THIS_A_ROUNDS_MISSION_FOR_CORONA")
						PRINTLN("[SST] * g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams = ", g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams)
						PRINTLN("[SST] * GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams = ", GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams)
						IF g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams != 0
							g_FMMC_STRUCT.iNumberOfTeams = g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams
							g_FMMC_STRUCT.iMaxNumberOfTeams = g_FMMC_STRUCT.iNumberOfTeams
							PRINTLN("[SST] * g_FMMC_STRUCT.iNumberOfTeams = g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams = ", g_FMMC_STRUCT.iNumberOfTeams)
						ELIF GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams != 0
							g_FMMC_STRUCT.iNumberOfTeams = GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams
							g_FMMC_STRUCT.iMaxNumberOfTeams = g_FMMC_STRUCT.iNumberOfTeams
							PRINTLN("[SST] * g_FMMC_STRUCT.iNumberOfTeams = GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams = ", g_FMMC_STRUCT.iNumberOfTeams)
						ELSE
							PRINTLN("[SST] * g_FMMC_STRUCT.iNumberOfTeams, not changing as all are 0")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[CORONA] g_FMMC_STRUCT.iNumberOfTeams = sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS] = ", g_FMMC_STRUCT.iNumberOfTeams)
					g_FMMC_STRUCT.iNumberOfTeams = sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS]
					g_FMMC_STRUCT.iMaxNumberOfTeams = g_FMMC_STRUCT.iNumberOfTeams
				ENDIF
			ELSE
				g_FMMC_STRUCT.iNumberOfTeams = sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS]
				g_FMMC_STRUCT.iMaxNumberOfTeams = g_FMMC_STRUCT.iNumberOfTeams
			ENDIF
			
			g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams =  g_FMMC_STRUCT.iNumberOfTeams
			GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams =  g_FMMC_STRUCT.iNumberOfTeams
			PRINTLN("[SST] * [CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Mission: No. of teams overridden: g_FMMC_STRUCT.iNumberOfTeams = ", g_FMMC_STRUCT.iNumberOfTeams)
			PRINTLN("[TS] [SST] * SAVE_CORONA_OPTIONS_TO_GLOBALS GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams = ", g_FMMC_STRUCT.iNumberOfTeams)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)
			g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit = sSelection.iSelection[ciMISSION_HOST_OPTION_TARGET_SCORE]
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Target Score: g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit = ", g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_CORONO_OPTION)
		AND sSelection.iSelection[ciMISSION_HOST_OPTION_USE_BAR] = 0
			g_FMMC_STRUCT.iTradingPlacesTimeBarDuration = -1
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Use Bar: g_FMMC_STRUCT.iTradingPlacesTimeBarDuration = ", g_FMMC_STRUCT.iTradingPlacesTimeBarDuration)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_ROUND_LENGTH)
			g_FMMC_STRUCT.iMissionCustomMultiruleTime = sSelection.iSelection[ciMISSION_HOST_OPTION_ROUND_DURATION]
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Set round duration: g_FMMC_STRUCT.iMissionCustomMultiruleTime = ", g_FMMC_STRUCT.iMissionCustomMultiruleTime)
		ENDIF
		
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_TEAM_LIVES_SETTING)
			g_FMMC_STRUCT.sFMMCEndConditions[0].iPlayerLives = sSelection.iSelection[ciMISSION_HOST_OPTION_TEAM_LIVES]
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Set round duration: g_FMMC_STRUCT.sFMMCEndConditions[0].iPlayerLives = ", g_FMMC_STRUCT.sFMMCEndConditions[0].iPlayerLives)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTen, ciMODE_RULE_BASED_SCORE_SCALING)
			g_FMMC_STRUCT.iTargetScoreMultiplierSetting = sSelection.iSelection[ciMISSION_HOST_OPTION_KILLS_MULTIPLIER]
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Set kill multiplier: g_FMMC_STRUCT.iTargetScoreMultiplierSetting = ", g_FMMC_STRUCT.iTargetScoreMultiplierSetting)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciOVERRIDE_GUNSMITH_LOADOUT_SETTINGS)
			g_FMMC_STRUCT.iGunsmithLoadoutSetting = sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPON_LOADOUT]
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sTeamData[0].iTeamBitSet, ciTEAM_BS_SET_STARTING_INVENTORY_CHOSEN_IN_LOBBY)
			g_iMissionLobbyStartingInventoryIndex[0] = sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPON_LOADOUT]
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Set weapon loadout/inventory option: g_iMissionLobbyStartingInventoryIndex[0] = ", g_iMissionLobbyStartingInventoryIndex[0])
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciSHOW_LAPS_OPTION)
			g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionLapLimit = sSelection.iSelection[ciMISSION_HOST_OPTION_NUMBER_OF_LAPS]
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Set lap option: g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionLapLimit = ", g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionLapLimit)
		ENDIF
		
		
		IF SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
			g_FMMC_STRUCT.iSVMExitType = sSelection.iSelection[ciMISSION_HOST_OPTION_EXIT_TYPE]
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Set exit type: g_FMMC_STRUCT.iSVMExitType = ", g_FMMC_STRUCT.iSVMExitType)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION)
			IF sSelection.iSelection[ciMISSION_HOST_OPTION_PICKUPS] = 1
				g_FMMC_STRUCT.bAllowPickUpsCorona = TRUE// True
			ELSE
				g_FMMC_STRUCT.bAllowPickUpsCorona = FALSE// False
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_AIRQUOTA_ALT_OPTION)
			IF sSelection.iSelection[ciMISSION_HOST_OPTION_VEHICLE_LIST] = 0
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_USE_AIRQUOTA_ALT_OPTION)
				PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Air Quota Vehicle List 1")
			ELSE
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_USE_AIRQUOTA_ALT_OPTION)
				PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Air Quota Vehicle List 2")
			ENDIF
		ENDIF
		
	//Set THe Menu up for Deathmatches
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
	
		g_sDM_SB_CoronaOptions.iDuration 			= sSelection.iSelection[ciDM_HOST_OPTION_TIME_LIMIT]		
		IF g_sDM_SB_CoronaOptions.iDuration > DM_DURATION_60
			g_sDM_SB_CoronaOptions.iDuration = DM_DURATION_60
		ENDIF
		g_sDM_SB_CoronaOptions.iTarget				= sSelection.iSelection[ciDM_HOST_OPTION_TARGET_SCORE]		
		g_sDM_SB_CoronaOptions.iSpawnTime			= DM_SPAWN_TIME_MED	
		g_sDM_SB_CoronaOptions.iWeaponRespawnTime	= 2	
		g_sDM_SB_CoronaOptions.iVehicleRespawnTime 	= VEH_RESPAWN_OFF	
		g_sDM_SB_CoronaOptions.iBlips				= DM_BLIPS_ALWAYS
		g_sDM_SB_CoronaOptions.iWeaponBlips	 		= PICKUP_BLIPS_ON
		
		IF sSelection.iSelection[ciDM_HOST_OPTION_GAMERTAGS] = TAGS_ON
			g_sDM_SB_CoronaOptions.iTags				= TAGS_WHEN_TARGETED	
		ELSE
			g_sDM_SB_CoronaOptions.iTags				= TAGS_OFF
		ENDIF
		
		g_sDM_SB_CoronaOptions.iHealthBar			= sSelection.iSelection[ciDM_HOST_OPTION_OPP_HEALTH_BAR]	
		g_sDM_SB_CoronaOptions.iTimeOfDay			= sSelection.iSelection[ciDM_HOST_OPTION_TIME_OF_DAY]	
		g_sDM_SB_CoronaOptions.iTraffic				= sSelection.iSelection[ciDM_HOST_OPTION_TRAFFIC]
		g_sDM_SB_CoronaOptions.iPeds				= PEDS_OFF			
		g_sDM_SB_CoronaOptions.iPolice				= POLICE_OFF
		g_sDM_SB_CoronaOptions.iWeather				= sSelection.iSelection[ciDM_HOST_OPTION_WEATHER]	
		g_sDM_SB_CoronaOptions.iVehicleDeathmatch	= 0
		
		IF IS_ARENA_WARS_JOB(TRUE)
			g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = sSelection.iSelection[ciDM_HOST_OPTION_WEATHER]
			PRINTLN("[CORONA][CORONA_ARENA_LIGHTING] SAVE_CORONA_OPTIONS_TO_GLOBALS - Deathmatch, g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = ", GET_ARENA_LIGHTING_STRING(g_FMMC_STRUCT.sArenaInfo.iArena_Lighting))
		ENDIF
		
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked) 
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked)
		
		// Set the correct global bit so the DM / spawning systems pick them up
		IF sSelection.iSelection[ciDM_HOST_OPTION_WEAPONS] = ciDM_WEAPON_LOCKED
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Deathmatch, weapons locked to single weapon")
			SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked)
		ELIF sSelection.iSelection[ciDM_HOST_OPTION_WEAPONS] = ciDM_WEAPON_STARTING_LOCKED
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Deathmatch, weapons locked to DM weapons")
			SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked)
		ENDIF
		
		IF NOT IS_ARENA_WARS_JOB()
		AND NOT IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE)
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - setting vehicle deathmatch vehicle to: ", sSelection.iSelection[ciDM_HOST_OPTION_VEHICLE])		
			g_mnMyRaceModel = GET_VDM_VEHICLE(sSelection.iSelection[ciDM_HOST_OPTION_VEHICLE])
		ELSE
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Arena deathmatch, not setting vehicle deathmatch vehicle as this is handled in vehicle selection")
		ENDIF
		
		// Validate our deathmatch type
		IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
			IF sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM
				PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - H2H but type is incorrect, HARD SET it to TEAM ")		
				sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM
			ENDIF
		ENDIF
		
		g_sDM_SB_CoronaOptions.iTypeOfDeathmatch = sSelection.iSelection[ciDM_HOST_OPTION_TYPE]
	
		IF sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM
		OR sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM_KOTH
			g_sDM_SB_CoronaOptions.bIsTeamDM = TRUE
		ELSE 
			g_sDM_SB_CoronaOptions.bIsTeamDM = FALSE
		ENDIF

		g_sDM_SB_CoronaOptions.iStartWeapon		= g_FMMC_STRUCT_ENTITIES.iWeaponPallet
		
		g_FMMC_STRUCT.iNumberOfTeams			= sSelection.iSelection[ciDM_HOST_OPTION_NUM_TEAMS] + 2
		g_FMMC_STRUCT.iBalanceTeams				= sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL]
		g_sDM_SB_CoronaOptions.iNumberOfTeams  	= g_FMMC_STRUCT.iNumberOfTeams
		g_sDM_SB_CoronaOptions.iBalanceTeams	= g_FMMC_STRUCT.iBalanceTeams
		g_sDM_SB_CoronaOptions.iPlayerHealth	= HEALTH_MED
		g_FMMC_STRUCT.iFixedCamera				= sSelection.iSelection[ciDM_HOST_OPTION_FIXED_CAMERA]
		g_FMMC_STRUCT.iPlayerLives 				= sSelection.iSelection[ciDM_HOST_OPTION_NUM_LIVES] 
		
		IF NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()	
			g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds			= (GET_CORONA_NUMBER_OF_ROUND(sSelection.iSelection[ciDM_HOST_OPTION_ROUNDS])-1)
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Deathmatch setting rounds to (one less as additional to standard play): ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds)
			g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams = iTeamCap
			GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams = iTeamCap
			PRINTLN("[TS] [SST] * SAVE_CORONA_OPTIONS_TO_GLOBALS GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams = ", iTeamCap)			
		ENDIF
		
		// Save out our round data to BD so players who JIP can pick up on this.
		GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iNumberOfRounds		= g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds
				
	//Set the menu up for Races 
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
	//OR 	iMissionType = FMMC_TYPE_TRIATHLON
		g_sRC_SB_CoronaOptions.iLaps			= (sSelection.iSelection[ciRC_HOST_OPTION_NUM_LAPS] + 1)
		g_sRC_SB_CoronaOptions.iRaceType		= g_FMMC_STRUCT.iRaceType
		g_sRC_SB_CoronaOptions.iRaceMode		= sSelection.iSelection[ciRC_HOST_OPTION_TYPE]
		g_sRC_SB_CoronaOptions.iVehicleDamage	= 0 //sSelection.iSelection[ciRC_HOST_TYPE_DAMAGE]
		g_sRC_SB_CoronaOptions.iTags			= TAGS_ON //sSelection.iSelection[ciRC_HOST_TYPE_NAMES_ABOVE_HEAD]		
		g_sRC_SB_CoronaOptions.iWeapons 		= g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons	
		g_sRC_SB_CoronaOptions.iTraffic 		= sSelection.iSelection[ciRC_HOST_OPTION_TRAFFIC]	
		g_sRC_SB_CoronaOptions.iRaceClass 		= sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS]
		g_sRC_SB_CoronaOptions.iPedestrians  	= 0 //sSelection.iSelection[ciRC_HOST_TYPE_PEDS]
		g_sRC_SB_CoronaOptions.iTimeOfDay 		= sSelection.iSelection[ciRC_HOST_OPTION_TIME_OF_DAY]
		g_sRC_SB_CoronaOptions.iStartingGrid 	= GRID_BEST_LAST //sSelection.iSelection[ciRC_HOST_TYPE_STARTING_GRID]
		g_sRC_SB_CoronaOptions.iPolice	 		= sSelection.iSelection[ciRC_HOST_OPTION_COPS]
		g_FMMC_STRUCT.iRaceMaxWantedLevel = sSelection.iSelection[ciRC_HOST_OPTION_MAX_WANTED]
		g_sRC_SB_CoronaOptions.iRadio		 	= g_iMyChosenRadioStation
		g_sRC_SB_CoronaOptions.iWeather			= sSelection.iSelection[ciRC_HOST_OPTION_WEATHER]
		g_sRC_SB_CoronaOptions.iSwapRoles		= sSelection.iSelection[ciRC_HOST_OPTION_SWAP_ROLES]
		g_sRC_SB_CoronaOptions.iSplitComparison = sSelection.iSelection[ciRC_HOST_OPTION_SPLIT_COMPARE]
		g_sRC_SB_CoronaOptions.iSlipStream		= sSelection.iSelection[ciRC_HOST_OPTION_SLIP_STREAM]
		g_FMMC_STRUCT.iFixedCamera				= sSelection.iSelection[ciRC_HOST_OPTION_FIXED_CAMERA]
		
		IF IS_ARENA_WARS_JOB(TRUE)
			g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = sSelection.iSelection[ciRC_HOST_OPTION_WEATHER]
			PRINTLN("[CORONA][CORONA_ARENA_LIGHTING] SAVE_CORONA_OPTIONS_TO_GLOBALS - Race, g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = ", GET_ARENA_LIGHTING_STRING(g_FMMC_STRUCT.sArenaInfo.iArena_Lighting))
		ENDIF
		
		// (1835747)
		IF sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_GTA
			g_sRC_SB_CoronaOptions.iGTATeamRace		= sSelection.iSelection[ciRC_HOST_OPTION_GTA_TEAM]		
		ENDIF
		
		g_sRC_SB_CoronaOptions.iCatchupCode		= sSelection.iSelection[ciRC_HOST_OPTION_CATCHUP_CODE]
		
			// If we are in a team race (Rally or GTA Team) then disable this option
			IF IS_THIS_CORONA_A_GTA_TEAM_RACE(sSelection)
			OR IS_THIS_A_RALLY_RACE_TEMP(sSelection)
			OR IS_TARGET_ASSAULT_RACE()
				PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Defaulting destroy loser to OFF as Team Race.")
				g_sRC_SB_CoronaOptions.iDestroyLoser	= ciCORONA_DESTROY_VEHICLE_OFF
			ELSE
				g_sRC_SB_CoronaOptions.iDestroyLoser	= sSelection.iSelection[ciRC_HOST_OPTION_DESTROY_LOSER]
				
				// Cache our choice so we can initialise to this the next time.
				g_TransitionSessionNonResetVars.iCachedRaceOption = g_sRC_SB_CoronaOptions.iDestroyLoser
			ENDIF
			
		
//		#IF FEATURE_IMPORT_EXPORT
		IF sSelection.iSelection[ciRC_HOST_OPTION_AGGREGATE_POS] = 0
			g_sRC_SB_CoronaOptions.bAggregate = FALSE
		ELSE
			g_sRC_SB_CoronaOptions.bAggregate = TRUE
		ENDIF
//		#ENDIF

		#IF FEATURE_GEN9_EXCLUSIVE
		IF sSelection.iSelection[ciRC_HOST_OPTION_HSW_MODS] = ciCORONA_HSW_MODS_ENABLED
			CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_DisableHSWMods)
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_DisableHSWMods)")
		ELSE
			SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_DisableHSWMods)
			PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_DisableHSWMods)")
		ENDIF
		#ENDIF
		
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_BASE_JUMP
	
		g_sRC_SB_CoronaOptions.iTimeOfDay 			= sSelection.iSelection[ciPARA_HOST_OPTION_TIME_OF_DAY]
		g_sRC_SB_CoronaOptions.iWeather				= sSelection.iSelection[ciPARA_HOST_OPTION_WEATHER]
		g_sRC_SB_CoronaOptions.iBaseJumpShooting 	= sSelection.iSelection[ciPARA_HOST_OPTION_SHOOTING]
		g_FMMC_STRUCT.iFixedCamera					= sSelection.iSelection[ciPARA_HOST_OPTION_FIXED_CAMERA]
				
	//Set the menu up for Tennis
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_TENNIS
		g_FMMC_STRUCT.iNumOfSets = sSelection.iSelection[ciTENNIS_HOST_OPTION_NUM_OF_SETS]
		
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_GOLF
		g_FMMC_STRUCT.iWeather 			=  sSelection.iSelection[ciGOLF_HOST_OPTION_WEATHER]
		g_FMMC_STRUCT.iStartingHole 	=  sSelection.iSelection[ciGOLF_HOST_OPTION_START_HOLE]
		g_FMMC_STRUCT.iNumberOfHoles 	=  sSelection.iSelection[ciGOLF_HOST_OPTION_NUM_OF_HOLES] + 1
		
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_DARTS
		g_FMMC_STRUCT.iNumberOfLegs		=  sSelection.iSelection[ciDARTS_HOST_OPTION_NUM_OF_LEGS]
		g_FMMC_STRUCT.iNumberOfSets		=  sSelection.iSelection[ciDARTS_HOST_OPTION_NUM_OF_SETS]
		
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
		
		g_FMMC_STRUCT.iShootingRangeMatches = sSelection.iSelection[ciSHOOTING_HOST_OPTION_MATCH]
		
	ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL
		
		IF sSelection.iSelection[ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES] = ciSURVIVAL_ENDLESS_WAVES_ENABLED
			SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_EndlessWaves)
		ELSE
			CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_EndlessWaves)
		ENDIF
		g_FMMC_STRUCT.iTimeOfDay 		=  sSelection.iSelection[ciSURVIVAL_HOST_OPTION_TIME_OF_DAY]
		g_FMMC_STRUCT.iWeather 			=  sSelection.iSelection[ciSURVIVAL_HOST_OPTION_WEATHER]
		
	ENDIF
	
	// Saving out our menu options to our BD for any JIPs that may occur during job
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sMenuSelection = g_sTransitionSessionOptions
	PRINTLN("[CORONA] SAVE_CORONA_OPTIONS_TO_GLOBALS - Saved menu settings: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sMenuSelection = g_sTransitionSessionOptions")
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Set players vehicle to random
PROC SET_PLAYER_VEHICLE_TO_RANDOM()

	g_iMyRaceModelChoice = GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])
	
	// Clean up custom vehicle selection at this point (1603248)
	SET_PLAYER_SELECTING_CUSTOM_VEHICLE(FALSE)
	
	SET_PLAYER_CORONA_VEHICLE(g_mnMyRaceModel)
ENDPROC


FUNC BOOL IS_MISSION_VEHICLE_CLASS_BLOCKED(INT iClass)
	UNUSED_PARAMETER(iClass)

	#IF NOT FEATURE_GEN9_EXCLUSIVE
	SWITCH iClass
		CASE VEHICLE_LIBRARY_HSW
		CASE VEHICLE_LIBRARY_HSW_T1
		CASE VEHICLE_LIBRARY_HSW_T2
		CASE VEHICLE_LIBRARY_HSW_T3
			RETURN TRUE
	ENDSWITCH
	#ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Increment the class to the next valid vehicle class for the mission
FUNC BOOL FIND_NEXT_CORONA_MISSION_VEHICLE_CLASS(INT &iCurrentVehicleClass, INT iTeam, INT iRole, INT iIncrement, BOOL bAllowOff = FALSE)

	// Check our team value
	IF iTeam = -1
		PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE_CLASS - iTeam = -1. Return FALSE")
		RETURN FALSE
	ENDIF

	BOOL bFound
	INT iVehicleClass = iCurrentVehicleClass // coronaMenuData.iMissionVehicleClass[iTeam]
	
	#IF IS_DEBUG_BUILD
	
		IF iVehicleClass != -1
			INT i
			FOR i = 0 TO ciNumOfDLCVehBitsets-1
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
					IF iRole != -1
						PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE_CLASS - g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[", iTeam, "][", iRole, "][", iVehicleClass, "][",i,"]: ", g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][iRole][iVehicleClass][i])
					ELSE
						PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE_CLASS - g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[", iTeam, "][-1][", iVehicleClass, "][",i,"]. ERROR ")
					ENDIF
				ELSE
					PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE_CLASS - g_FMMC_STRUCT.iAvailableCommonVehicles[", iTeam, "][", iVehicleClass, "][",i,"]: ", g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][iVehicleClass][i])
				ENDIF
			ENDFOR
		ELSE
			PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE_CLASS - g_FMMC_STRUCT.iAvailableCommonVehicles[", iTeam, "][iVehicleClass]: iVehicleClass = -1")
		ENDIF
	#ENDIF
	
	INT iMinClass = 0
	
	IF bAllowOff
		iMinClass = -1
	ENDIF
	
	WHILE NOT bFound
	
		iVehicleClass += iIncrement
		
		// Cap our values to the range of vehicle classes in the creator
		IF iVehicleClass < iMinClass
			iVehicleClass = VEHICLE_LIBRARY_MAX-1
		ELIF iVehicleClass >= VEHICLE_LIBRARY_MAX
			iVehicleClass = iMinClass
			
			IF iCurrentVehicleClass = -1
				iCurrentVehicleClass = 0
				PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE_CLASS - Setting coronaMenuData.iMissionVehicleClass[iTeam] = 0 due to NO valid vehicle class being found")
			ENDIF
		ENDIF
	
		// If we have looped back to your current selection or there are valid vehicles in the class then update
		IF iVehicleClass = iCurrentVehicleClass
			bFound = TRUE
		ELIF iVehicleClass = -1
			bFound = TRUE		
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
		AND iRole != -1
		AND NOT IS_MISSION_VEHICLE_CLASS_BLOCKED(iVehicleClass)
			IF (SUM_BITS(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][iRole][iVehicleClass][0]) + SUM_BITS(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][iRole][iVehicleClass][1])) != 0
				bFound = TRUE
			ENDIF
		ELSE
			IF (SUM_BITS(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][iVehicleClass][0]) + SUM_BITS(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][iVehicleClass][1])) != 0
			AND NOT IS_MISSION_VEHICLE_CLASS_BLOCKED(iVehicleClass)
				bFound = TRUE
			ENDIF			
		ENDIF
				
	ENDWHILE
	
	PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE_CLASS - Found vehicle class: ", iVehicleClass, " with increment: ", iIncrement)
	
	// Only if we find a new vehicle class do we need to trigger sound / update
	IF iVehicleClass != iCurrentVehicleClass
		iCurrentVehicleClass = iVehicleClass
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MODEL_VALID_FOR_MISSION(MODEL_NAMES eModel, INT iTeam, INT iClass)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciRESTRICT_CUSTOM_VEHICLES)
		INT iVehicle
		REPEAT ciMAX_VEHICLES_PER_CREATOR_LIBRARY iVehicle
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass)][GET_LONG_BITSET_INDEX(iVehicle)], GET_LONG_BITSET_BIT(iVehicle))
				IF eModel = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass), iVehicle)
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDREPEAT
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Find the next valid vehicle in the current class that has been selected for the passed team
FUNC BOOL FIND_NEXT_CORONA_MISSION_VEHICLE(CORONA_MENU_DATA &coronaMenuData, INT iTeam, INT iClass, INT iIncrement)

	BOOL bFound
	INT iVehicle = coronaMenuData.iMissionVehicle[iTeam]
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
		PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE - g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[",iTeam,"][",coronaMenuData.iMissionTeamRole[iTeam],"][",iClass,"][0]: ", g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][coronaMenuData.iMissionTeamRole[iTeam]][iClass][0])
		PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE - g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[",iTeam,"][",coronaMenuData.iMissionTeamRole[iTeam],"][",iClass,"][1]: ", g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][coronaMenuData.iMissionTeamRole[iTeam]][iClass][1])
	ELSE
		PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE - g_FMMC_STRUCT.iAvailableCommonVehicles[",iTeam,"][",iClass,"][0]: ", g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][iClass][0])
		PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE - g_FMMC_STRUCT.iAvailableCommonVehicles[",iTeam,"][",iClass,"][1]: ", g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][iClass][1])
	ENDIF
	#ENDIF
	
	MODEL_NAMES mnModel
	WHILE NOT bFound
	
		IF IS_CORONA_BIT_SET(CORONA_SELECTING_FROM_PERSONAL_VEHICLES)
		
			PRINTLN("[CORONA SPAM] - IS_CORONA_BIT_SET(CORONA_SELECTING_FROM_PERSONAL_VEHICLES)")
		
			coronaMenuData.iVehicleCoronaIndex += iIncrement
			
			IF coronaMenuData.iVehicleCoronaIndex = -1
				iVehicle = ciMAX_VEHICLES_PER_CREATOR_LIBRARY
				CLEAR_CORONA_BIT(CORONA_SELECTING_FROM_PERSONAL_VEHICLES)
				PRINTLN("[CORONA SPAM] - CLEAR_CORONA_BIT(CORONA_SELECTING_FROM_PERSONAL_VEHICLES) [1]")
			ELIF coronaMenuData.iVehicleCoronaIndex >= coronaMenuData.iTotalCustomVehicles
				iVehicle = -1
				CLEAR_CORONA_BIT(CORONA_SELECTING_FROM_PERSONAL_VEHICLES)
				PRINTLN("[CORONA SPAM] - CLEAR_CORONA_BIT(CORONA_SELECTING_FROM_PERSONAL_VEHICLES) [2]")
			ENDIF
					
			// Check is still safe to do this
			IF IS_CORONA_BIT_SET(CORONA_SELECTING_FROM_PERSONAL_VEHICLES)
				mnModel = GET_CUSTOM_VEHICLE_MODEL_NAME(g_sCoronaCustomVehiclesData.customVehicleArray[coronaMenuData.iVehicleCoronaIndex])
				IF mnModel != DUMMY_MODEL_FOR_SCRIPT
					IF IS_MODEL_VALID(mnModel)
						IF IS_MODEL_VALID_FOR_MISSION(mnModel, iTeam, iClass)
							VEHICLE_CLASS vehClass = GET_VEHICLE_CLASS_FROM_CREATOR_LIBRARY(iClass)
							IF GET_VEHICLE_CLASS_FROM_NAME(mnModel) = vehClass
							OR iClass = VEHICLE_LIBRARY_WEAPONISED //Custom script vehicle class
							OR iClass = VEHICLE_LIBRARY_CONTENDER //Custom script vehicle class
							OR iClass = VEHICLE_LIBRARY_OPEN_WHEEL
							OR iClass = VEHICLE_LIBRARY_GOKART
							OR iClass = VEHICLE_LIBRARY_TUNER
								GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.mnVehicleChosen = mnModel
								bFound = TRUE
								PRINTLN("[CORONA SPAM] - bFound = TRUE, model: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnModel))
							ELSE
								PRINTLN("[CORONA SPAM] - vehicle ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnModel), " is not in vehicle class ", ENUM_TO_INT(vehClass))
							ENDIF
						ENDIF
					ENDIF
				ENDIF 
			ENDIF
			
			PRINTLN("[CORONA SPAM] - coronaMenuData.iVehicleCoronaIndex = ", coronaMenuData.iVehicleCoronaIndex)
			IF IS_MODEL_VALID(mnModel)
				PRINTLN("[CORONA SPAM] - model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnModel))
			ENDIF
			
			IF IS_CORONA_BIT_SET(CORONA_SELECTING_FROM_PERSONAL_VEHICLES)
			AND bFound = TRUE
				PRINTLN("[CORONA SPAM] - found")
				RETURN TRUE // Only if its all ok 
			ENDIF
		ELSE
			iVehicle += iIncrement
			
			// Cap our values to the range of vehicle classes in the creator
			IF iVehicle < 0
			
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_PERSONAL_VEHICLES)
				AND coronaMenuData.iTotalCustomVehicles > 0
					
					coronaMenuData.iVehicleCoronaIndex = coronaMenUData.iTotalCustomVehicles
					coronaMenuData.iMissionVehicle[iTeam] = -1
											
					SET_CORONA_BIT(CORONA_SELECTING_FROM_PERSONAL_VEHICLES)
					PRINTLN("[CORONA SPAM] - SET_CORONA_BIT(CORONA_SELECTING_FROM_PERSONAL_VEHICLES) [1]")
					
				ELSE
					PRINTLN("[CORONA SPAM] - coronaMenUData.iTotalCustomVehicles [1] = ", coronaMenUData.iTotalCustomVehicles)
				
					iVehicle = ciMAX_VEHICLES_PER_CREATOR_LIBRARY-1
				ENDIF
							
			ELIF iVehicle >= ciMAX_VEHICLES_PER_CREATOR_LIBRARY
			
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_PERSONAL_VEHICLES)
				AND coronaMenUData.iTotalCustomVehicles > 0
					
					coronaMenuData.iVehicleCoronaIndex = -1
					coronaMenuData.iMissionVehicle[iTeam] = -1
											
					SET_CORONA_BIT(CORONA_SELECTING_FROM_PERSONAL_VEHICLES)
					PRINTLN("[CORONA SPAM] - SET_CORONA_BIT(CORONA_SELECTING_FROM_PERSONAL_VEHICLES) [2]")
					
				ELSE
					PRINTLN("[CORONA SPAM] - coronaMenUData.iTotalCustomVehicles [2] = ", coronaMenUData.iTotalCustomVehicles)
				
					iVehicle = 0
				
					IF coronaMenuData.iMissionVehicle[iTeam] = -1
						coronaMenuData.iMissionVehicle[iTeam] = 0
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT IS_CORONA_BIT_SET(CORONA_SELECTING_FROM_PERSONAL_VEHICLES)
		
				mnModel = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(iClass, iVehicle)
			
				// If we have looped back to your current selection or there are valid vehicles in the class then update
				IF iVehicle = coronaMenuData.iMissionVehicle[iTeam]
					bFound = TRUE
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
					IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][coronaMenuData.iMissionTeamRole[iTeam]][iClass][GET_LONG_BITSET_INDEX(iVehicle)], GET_LONG_BITSET_BIT(iVehicle))
						IF mnModel != DUMMY_MODEL_FOR_SCRIPT
							bFound = TRUE
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][iClass][GET_LONG_BITSET_INDEX(iVehicle)], GET_LONG_BITSET_BIT(iVehicle))
						IF mnModel != DUMMY_MODEL_FOR_SCRIPT
							bFound = TRUE
						ENDIF
					ENDIF			
				ENDIF
			ENDIF
		ENDIF
	ENDWHILE
	
	PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE - Found: ", iVehicle, " with increment: ", iIncrement)
	
	IF IS_CORONA_BIT_SET(CORONA_SELECTING_FROM_PERSONAL_VEHICLES)
		PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_VEHICLE - Found: ", coronaMenuData.iVehicleCoronaIndex, " with increment: ", iIncrement)
	ENDIF
	
	// Only if we find a new vehicle class do we need to trigger sound / update
	IF iVehicle != coronaMenuData.iMissionVehicle[iTeam]
		coronaMenuData.iMissionVehicle[iTeam] = iVehicle
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.mnVehicleChosen = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(coronaMenuData.iMissionVehicleClass[iTeam], coronaMenuData.iMissionVehicle[iTeam])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates our knowledge of if we have multiple classes available for the current team
PROC UPDATE_CORONA_MISSION_VEHICLE_CLASS_AVAILABILITY(CORONA_MENU_DATA &coronaMenuData, INT iPreviousTeam = -1)
	
	// If this is not a mission, exit
	IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION
		EXIT
	ENDIF
	
	INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
	
	IF iTeam >= FMMC_MAX_TEAMS
	OR iTeam < 0
		PRINTLN("[CORONA] UPDATE_CORONA_MISSION_VEHICLE_CLASS_AVAILABILITY - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen is not valid. Team = ", iTeam)
		SCRIPT_ASSERT("[CORONA] UPDATE_CORONA_MISSION_VEHICLE_CLASS_AVAILABILITY - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen is not valid. See url:bugstar:2672115.")
		EXIT
	ENDIF
	
	// If the creator has not activated this option, exit
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST)
		EXIT
	ENDIF
	
	INT i
	INT iCount = 0
	
	// Clear our bit each time
	CLEAR_CORONA_BIT(CORONA_MISSION_VEHICLE_HAS_MULTIPLE_CLASSES)
	
	REPEAT VEHICLE_LIBRARY_MAX i
		
		// Increment our count if a class has some vehicles available
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
			IF (SUM_BITS(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][coronaMenuData.iMissionTeamRole[iTeam]][i][0]) + SUM_BITS(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][coronaMenuData.iMissionTeamRole[iTeam]][i][1])) != 0
				iCount++
			ENDIF
		ELSE
			IF (SUM_BITS(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][i][0]) + SUM_BITS(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][i][1])) != 0
				iCount++
			ENDIF
		ENDIF
		
		// If we have more than 2 then we have multiple classes available
		IF iCount >= 2
		
			PRINTLN("[CORONA] UPDATE_CORONA_MISSION_VEHICLE_CLASS_AVAILABILITY - Team has more than 1 vehicle class available.")
			i = VEHICLE_LIBRARY_MAX
			SET_CORONA_BIT(CORONA_MISSION_VEHICLE_HAS_MULTIPLE_CLASSES)
		ENDIF
	ENDREPEAT
	
	// Check if our chosen vehicle for the previous team is now valid? (2624747)
	IF iPreviousTeam != -1
				
		PRINTLN("[CORONA] UPDATE_CORONA_MISSION_VEHICLE_CLASS_AVAILABILITY - iPreviousTeam != -1")
		PRINTLN("[CORONA] UPDATE_CORONA_MISSION_VEHICLE_CLASS_AVAILABILITY - Previous Vehicle Class = ", coronaMenuData.iMissionVehicleClass[iPreviousTeam])
		PRINTLN("[CORONA] UPDATE_CORONA_MISSION_VEHICLE_CLASS_AVAILABILITY - Previous Vehicle = ", coronaMenuData.iMissionVehicle[iPreviousTeam])
				
		BOOL bValid = FALSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][coronaMenuData.iMissionTeamRole[iTeam]][coronaMenuData.iMissionVehicleClass[iPreviousTeam]][GET_LONG_BITSET_INDEX(coronaMenuData.iMissionVehicle[iPreviousTeam])], GET_LONG_BITSET_BIT(coronaMenuData.iMissionVehicle[iPreviousTeam]))
				bValid = TRUE
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][coronaMenuData.iMissionVehicleClass[iPreviousTeam]][GET_LONG_BITSET_INDEX(coronaMenuData.iMissionVehicle[iPreviousTeam])], GET_LONG_BITSET_BIT(coronaMenuData.iMissionVehicle[iPreviousTeam]))
				bValid = TRUE
			ENDIF
		ENDIF
		
		IF bValid
			PRINTLN("[CORONA] UPDATE_CORONA_MISSION_VEHICLE_CLASS_AVAILABILITY - Previous Vehicle is valid. Overwrite for team: ", iTeam)
			coronaMenuData.iMissionVehicleClass[iTeam] = coronaMenuData.iMissionVehicleClass[iPreviousTeam]
			coronaMenuData.iMissionVehicle[iTeam] = coronaMenuData.iMissionVehicle[iPreviousTeam]
			
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.mnVehicleChosen = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(coronaMenuData.iMissionVehicleClass[iTeam], coronaMenuData.iMissionVehicle[iTeam])
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL GET_CORONA_NEXT_VALID_MISSION_VEHICLE_CLASS(CORONA_MENU_DATA &coronaMenuData, INT iIncrement, FMMC_SELECTED_ITEMS &sSelection)
	
	IF FIND_NEXT_CORONA_MISSION_VEHICLE_CLASS(sSelection.iSelection[coronaMenuData.iCurrentSelection], 0, 0, iIncrement, TRUE)
		
		PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC MODEL_NAMES GET_DEFAULT_RACE_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, g_FMMC_STRUCT.iVehicleModel)
ENDFUNC

FUNC BOOL DOES_MODEL_ALLOW_CUSTOM_VEHICLES(MODEL_NAMES ModelToCheck)
	SWITCH ModelToCheck
		CASE LIMO2
		CASE BOXVILLE5
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_RACE_LOCKED_TO_MODEL(MODEL_NAMES ModelToCheck)
	IF ModelToCheck = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("IS_RACE_LOCKED_TO_MODEL - FALSE - ModelToCheck = DUMMY_MODEL_FOR_SCRIPT")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_MODEL_VALID(ModelToCheck)
		PRINTLN("IS_RACE_LOCKED_TO_MODEL - FALSE - NOT IS_MODEL_VALID(ModelToCheck)")
		RETURN FALSE
	ENDIF

	INT iClass = g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS]
	
	INT i
	MODEL_NAMES eModel
	BOOL bLockedToModel = TRUE
	
	FOR i = 0 TO (GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)-1) STEP 1
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableVehicleBitSet[iClass], i)
		
			eModel = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i, DEFAULT, TRUE)
				
			IF eModel != ModelToCheck
				bLockedToModel = FALSE
				PRINTLN("IS_RACE_LOCKED_TO_MODEL - FALSE - race is not locked to ", GET_MODEL_NAME_FOR_DEBUG(ModelToCheck), " as ", GET_MODEL_NAME_FOR_DEBUG(eModel), " is also enabled")
				BREAKLOOP
			ENDIF
		ENDIF	
	ENDFOR
	
	// Only check DLC if we've not already ascertained if the race is not locked to the passed model because a non-dlc vehicle is enabled
	IF bLockedToModel
	
		// Check our DLC vehicles
		FOR i = 0 TO (GET_SELECTION_MAX_DLC_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)-1) STEP 1
			IF DOES_CREATOR_ALLOW_DLC_VEHICLE(iClass, i)
				eModel = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i)
			
				IF IS_VEHICLE_AVAILABLE_FOR_GAME(eModel, FALSE)
					IF eModel != ModelToCheck
						bLockedToModel = FALSE
						PRINTLN("IS_RACE_LOCKED_TO_MODEL - FALSE - race is not locked to ", GET_MODEL_NAME_FOR_DEBUG(ModelToCheck), " as DLC model ", GET_MODEL_NAME_FOR_DEBUG(eModel), " is also enabled")
					ENDIF
				ELSE
					IF eModel = ModelToCheck
						bLockedToModel = FALSE
						PRINTLN("IS_RACE_LOCKED_TO_MODEL - FALSE - ", GET_MODEL_NAME_FOR_DEBUG(ModelToCheck), " is not available for game")
					ENDIF
				ENDIF
			ELSE
				IF GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i) = ModelToCheck
					bLockedToModel = FALSE
					PRINTLN("IS_RACE_LOCKED_TO_MODEL - FALSE - creator does not allow dlc model ", GET_MODEL_NAME_FOR_DEBUG(ModelToCheck))
				ENDIF
			ENDIF		
		ENDFOR	
	ENDIF
	
	PRINTLN("IS_RACE_LOCKED_TO_MODEL - ", GET_STRING_FROM_BOOL(bLockedToModel))
	RETURN bLockedToModel
ENDFUNC


FUNC INT SELECT_RANDOM_ARENA_LIGHTING()
	BOOL bFound, bLightingTypesAvailable
	INT iLighting
	
	REPEAT ARENA_LIGHTING_MAX iLighting
		IF IS_BIT_SET(g_FMMC_STRUCT.sArenaInfo.iArena_RandomLightingBS, iLighting)
			PRINTLN("[CORONA] SELECT_RANDOM_ARENA_LIGHTING - Lighting style ", GET_ARENA_LIGHTING_STRING(iLighting), " is set")
			bLightingTypesAvailable = TRUE
		ELSE
			PRINTLN("[CORONA] SELECT_RANDOM_ARENA_LIGHTING - Lighting style ", GET_ARENA_LIGHTING_STRING(iLighting), " is not set")
		ENDIF
	ENDREPEAT
	
	IF bLightingTypesAvailable
		WHILE (NOT bFound)
			iLighting = GET_RANDOM_INT_IN_RANGE(ARENA_LIGHTING_STANDARD, ARENA_LIGHTING_MAX)
			IF IS_BIT_SET(g_FMMC_STRUCT.sArenaInfo.iArena_RandomLightingBS, iLighting)
				PRINTLN("[CORONA] SELECT_RANDOM_ARENA_LIGHTING - Found! Setting to ", GET_ARENA_LIGHTING_STRING(iLighting))
				bFound = TRUE
			ELSE
				PRINTLN("[CORONA] SELECT_RANDOM_ARENA_LIGHTING - Unable to use lighting style ", GET_ARENA_LIGHTING_STRING(iLighting))
			ENDIF
		ENDWHILE
	ELSE
		PRINTLN("[CORONA] SELECT_RANDOM_ARENA_LIGHTING - No lighting types are available, using g_FMMC_STRUCT.sArenaInfo.iArena_Lighting (", GET_ARENA_LIGHTING_STRING(g_FMMC_STRUCT.sArenaInfo.iArena_Lighting),")")
		iLighting = g_FMMC_STRUCT.sArenaInfo.iArena_Lighting
	ENDIF
	
	RETURN iLighting
ENDFUNC

/// PURPOSE: Returns the default setting for each type in host menu
FUNC INT GET_CORONA_MENU_ITEM_DEFAULT(INT iMissionType, INT iItem, INT iMissionSubType = 0)

	SWITCH iMissionType
		CASE  FMMC_TYPE_RACE
			IF g_bUseRCHostOptionOverrides
				IF g_iRCHostOptionOverrideDefault[iItem] != LOWEST_INT
					PRINTLN("[CORONA] - GET_CORONA_MENU_ITEM_DEFAULT - Override enabled for Race option ", iItem, "; value = ", g_iRCHostOptionOverrideDefault[iItem])
					RETURN g_iRCHostOptionOverrideDefault[iItem]
				ENDIF
			ENDIF
		
			SWITCH iItem
				CASE ciRC_HOST_OPTION_TYPE					
				
					IF CV2_IS_THIS_CORONA_A_PROFESSIONAL_JOB()
					 	IF g_sMPTunables.bCollisionOffInPremiumRaces
							RETURN ciRACE_SUB_TYPE_NON_CONTACT
						ENDIF
					ENDIF 
					
					IF IS_TARGET_ASSAULT_RACE()
						RETURN ciRACE_SUB_TYPE_GTA
					ENDIF
					
					IF IS_ARENA_WARS_JOB()
						IF g_FMMC_STRUCT.iRaceMode = ciRACE_SUB_TYPE_RALLY
							SCRIPT_ASSERT("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT - ciRC_HOST_OPTION_TYPE - Bad data from arena race creator, ciRACE_SUB_TYPE_RALLY set.")
							PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT - ciRC_HOST_OPTION_TYPE - Bad data from arena race creator, invalid rally race type set. Setting to ciRACE_SUB_TYPE_STANDARD")
							RETURN ciRACE_SUB_TYPE_STANDARD
						ENDIF
					ENDIF
					
					RETURN g_FMMC_STRUCT.iRaceMode
				CASE ciRC_HOST_OPTION_GTA_TEAM				
				
					IF IS_TARGET_ASSAULT_RACE()
						RETURN ciRACE_GTA_TEAMS_ON
					ENDIF
				
					RETURN ciRACE_GTA_TEAMS_OFF
				CASE ciRC_HOST_OPTION_NUM_LAPS
				
					IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HOTRING_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
						IF g_FMMC_STRUCT.iNumLaps < g_sMPTunables.iSSAS_MIN_LAPS
							RETURN g_sMPTunables.iSSAS_MIN_LAPS
						ENDIF
					ENDIF
				
					IF CV2_IS_THIS_JOB_A_CORONA_FLOW_JOB()
					 	IF g_FMMC_STRUCT.iNumLaps > g_sMPTunables.iRC_HOST_OPTION_STUNT_RACE_NUM_LAPS 
							RETURN g_sMPTunables.iRC_HOST_OPTION_STUNT_RACE_NUM_LAPS
						ENDIF
					ENDIF 
				
					RETURN g_FMMC_STRUCT.iNumLaps
				BREAK
				CASE ciRC_HOST_OPTION_VEHICLE_CLASS
					PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT - g_FMMC_STRUCT.iRaceClass = ", g_FMMC_STRUCT.iRaceClass)
				
					IF CV2_IS_THIS_CORONA_A_PROFESSIONAL_JOB()
						INT iCurrentPlaylistClass
						iCurrentPlaylistClass = CV2_GET_CURRENT_PLAYLIST_CLASS()
						PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT - OVERRIDE - CV2_GET_CURRENT_PLAYLIST_CLASS = ", iCurrentPlaylistClass)
						IF iCurrentPlaylistClass >= 0
						AND iCurrentPlaylistClass < FMMC_VEHICLE_UPDATE_FUTURE_VEHICLES
							RETURN iCurrentPlaylistClass
						ENDIF
					ENDIF
				
					IF IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
						RETURN FMMC_VEHICLE_CLASS_SEDANS				
					ELIF g_FMMC_STRUCT.iRaceClass = -1
						RETURN 0
					ELSE
						RETURN g_FMMC_STRUCT.iRaceClass
					ENDIF
				BREAK
				CASE ciRC_HOST_OPTION_TIME_OF_DAY
				
					IF g_FMMC_STRUCT.iTimeOfDay >= ciRC_MAX_TYPE_RACE_TIME_OF_DAY
						RETURN ciRC_MAX_TYPE_RACE_TIME_OF_DAY
					ELSE
						RETURN g_FMMC_STRUCT.iTimeOfDay
					ENDIF
				BREAK
				CASE ciRC_HOST_OPTION_WEATHER				
					IF IS_STUNT_RACE()
					OR IS_TRANSFORM_RACE()
						IF IS_TRANSFORM_RACE()
						AND (g_FMMC_STRUCT.iWeather = ciFMMC_WEATHER_OPTION_HALLOWEEN
						OR g_FMMC_STRUCT.iWeather = ciFMMC_WEATHER_OPTION_HALLOWEEN_NO_RAIN)
							RETURN g_FMMC_STRUCT.iWeather
						ENDIF
						
						RETURN ciFMMC_WEATHER_OPTION_SUNNY
					ENDIF 
				
					//If we are setting a challenge time or playing then its always nice!
					IF IS_CORONA_IN_CHALLENGE_MODE()
						RETURN ciFMMC_WEATHER_OPTION_SUNNY
					ELSE
						RETURN g_FMMC_STRUCT.iWeather
					ENDIF
				BREAK
				CASE ciRC_HOST_OPTION_TRAFFIC				RETURN g_FMMC_STRUCT.iTraffic			
				CASE ciRC_HOST_OPTION_COPS					RETURN g_FMMC_STRUCT.iPolice
				CASE ciRC_HOST_OPTION_MAX_WANTED			RETURN g_FMMC_STRUCT.iRaceMaxWantedLevel
				
				CASE ciRC_HOST_OPTION_CUSTOM_VEHICLE	
					IF g_sMPTunables.b4142885_UGC_RACE_CHANGES_ENABLED
						IF IS_RACE_LOCKED_TO_MODEL(GET_DEFAULT_RACE_MODEL()) 
						AND NOT DOES_MODEL_ALLOW_CUSTOM_VEHICLES(GET_DEFAULT_RACE_MODEL())
							RETURN ciCORONA_CUSTOM_VEHICLE_OFF
						ENDIF
												
						IF g_FMMC_STRUCT.iRaceCustomVehSetting = ciRCVS_OFF
							RETURN ciCORONA_CUSTOM_VEHICLE_OFF
						ENDIF
						
						RETURN ciCORONA_CUSTOM_VEHICLE_ON
					ELSE
						RETURN ciCORONA_CUSTOM_VEHICLE_ON
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_CATCHUP_CODE			
					
					IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
					OR IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
						PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT - Turning CATCHUP off as in tournament playlist")
						RETURN ciCORONA_CATCHUP_CODE_OFF
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciDISABLE_CATCH_UP)
							RETURN ciCORONA_CATCHUP_CODE_OFF
						ENDIF
						
						RETURN ciCORONA_CATCHUP_CODE_ON
					ENDIF		
				BREAK
				
				CASE ciRC_HOST_OPTION_SLIP_STREAM
					IF g_FMMC_STRUCT.iVehicleModel = ENUM_TO_INT(THRUSTER)
					AND IS_RACE_LOCKED_TO_MODEL(THRUSTER)
						RETURN ciRC_SLIP_STREAM_OFF
					ENDIF
					RETURN ciRC_SLIP_STREAM_ON
				BREAK
				
				CASE ciRC_HOST_OPTION_FIXED_CAMERA		RETURN g_FMMC_STRUCT.iFixedCamera
				
				CASE ciRC_HOST_OPTION_DESTROY_LOSER		
					IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
					
						// Point to point does not allow lap option so reset to OFF
						IF IS_THIS_A_POINT_TO_POINT_RACE()
						AND g_TransitionSessionNonResetVars.iCachedRaceOption = ciCORONA_DESTROY_VEHICLE_LAP
							PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT - LAP option was chosen for P2P Race. Reset to ciCORONA_DESTROY_VEHICLE_OFF")
							RETURN ciCORONA_DESTROY_VEHICLE_OFF
						ENDIF
						
						RETURN g_TransitionSessionNonResetVars.iCachedRaceOption		// 2354315, return last used
					ELSE
						RETURN ciCORONA_DESTROY_VEHICLE_OFF
					ENDIF
				BREAK
				
//				#IF FEATURE_IMPORT_EXPORT
				CASE ciRC_HOST_OPTION_AGGREGATE_POS
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciAGGREGATE_POSITION_RACE_DEFAULT_ON)
						RETURN ciCORONA_AGGREGATE_RACE_POSITION_ON
					ENDIF
				
					RETURN ciCORONA_AGGREGATE_RACE_POSITION_OFF
				BREAK
//				#ENDIF
				
				#IF FEATURE_GEN9_EXCLUSIVE
				CASE ciRC_HOST_OPTION_HSW_MODS
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_DisableHSWMods)
						RETURN ciCORONA_HSW_MODS_DISABLED
					ENDIF
					RETURN ciCORONA_HSW_MODS_ENABLED
				BREAK
				#ENDIF
				
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_BASE_JUMP
			SWITCH iItem
				CASE ciPARA_HOST_OPTION_TIME_OF_DAY			RETURN g_FMMC_STRUCT.iTimeOfDay
				CASE ciPARA_HOST_OPTION_WEATHER
					//If we are setting a challenge time or playing then its always nice!
					IF IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
					//OR IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
						RETURN ciFMMC_WEATHER_OPTION_SUNNY
					ELSE
						RETURN g_FMMC_STRUCT.iWeather
					ENDIF
				BREAK
				CASE ciPARA_HOST_OPTION_SHOOTING			RETURN 0
				CASE ciPARA_HOST_OPTION_FIXED_CAMERA		RETURN g_FMMC_STRUCT.iFixedCamera
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH
			IF g_bUseDMHostOptionOverrides
				IF g_iDMHostOptionOverrideDefault[iItem] != LOWEST_INT
					PRINTLN("[CORONA] - GET_CORONA_MENU_ITEM_DEFAULT - Override enabled for DM option ", iItem, "; value = ", g_iDMHostOptionOverrideDefault[iItem])
					RETURN g_iDMHostOptionOverrideDefault[iItem]
				ENDIF
			ENDIF
		
			SWITCH iItem
				CASE ciDM_HOST_OPTION_TYPE
					PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT - ciDM_HOST_OPTION_TYPE = ", iMissionSubType)
					
					// For head to head mode always to team
					IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
						RETURN FMMC_DM_TYPE_TEAM
					ENDIF
					
					RETURN iMissionSubType
				BREAK		
				CASE ciDM_HOST_OPTION_NUM_TEAMS				
//					IF g_FMMC_STRUCT.iMaxNumberOfTeams >= 2
//						RETURN (g_FMMC_STRUCT.iMaxNumberOfTeams - 2)
//					ELSE
						RETURN 0
//					ENDIF
				BREAK
				CASE ciDM_HOST_OPTION_TEAM_BAL
					IF IS_THIS_A_KING_OF_THE_HILL_TEAM_CORONA(FMMC_TYPE_DEATHMATCH, g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE])
						RETURN DM_TEAM_BALANCING_OFF
					ENDIF
					IF (IS_ARENA_WARS_JOB()
					OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType))
					AND (g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM
					OR g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM_KOTH)
						RETURN DM_TEAM_BALANCING_OFF
					ENDIF
					RETURN DM_TEAM_BALANCING_ON
				BREAK
				CASE ciDM_HOST_OPTION_TIME_LIMIT
					IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
						RETURN DM_DURATION_10
					ELSE
						IF g_FMMC_STRUCT.iRoundTime	> DM_DURATION_60
							g_FMMC_STRUCT.iRoundTime = DM_DURATION_60
						ENDIF
						
						// Override any extreme settings if we are in an open session (2210842)
						IF NOT DID_I_COME_INTO_A_PRIVATE_SESSION()
							IF g_FMMC_STRUCT.iRoundTime > DM_DURATION_20
								PRINTLN("[CORONA] ciDM_HOST_OPTION_TIME_LIMIT - g_FMMC_STRUCT.iRoundTime = ", g_FMMC_STRUCT.iRoundTime, " , cap to DM_DURATION_20")
								g_FMMC_STRUCT.iRoundTime = DM_DURATION_20
							ENDIF
						ENDIF
						
						RETURN g_FMMC_STRUCT.iRoundTime	
					ENDIF
				BREAK
				CASE ciDM_HOST_OPTION_TARGET_SCORE			
					IF IS_THIS_LEGACY_DM_CONTENT()
						RETURN g_FMMC_STRUCT.iTargetScore
					ELSE
						IF g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM
							RETURN g_FMMC_STRUCT.sDMCustomSettings.iTeamTargetScore[0]
						ELSE
							RETURN g_FMMC_STRUCT.iTargetScore
						ENDIF
					ENDIF
				BREAK
				CASE ciDM_HOST_OPTION_OPP_HEALTH_BAR		RETURN 0
				CASE ciDM_HOST_OPTION_GAMERTAGS				RETURN 1
				CASE ciDM_HOST_OPTION_TIME_OF_DAY			
					IF g_FMMC_STRUCT.iTimeOfDay >= ciDM_MAX_TYPE_TIME_OF_DAY
						RETURN ciDM_MAX_TYPE_TIME_OF_DAY
					ELSE
						RETURN g_FMMC_STRUCT.iTimeOfDay
					ENDIF
				BREAK
				CASE ciDM_HOST_OPTION_WEATHER
					//If we are setting a challenge time or playing then its always nice!
					IF IS_CORONA_IN_CHALLENGE_MODE()
						RETURN ciFMMC_WEATHER_OPTION_SUNNY
					ELSE
						RETURN g_FMMC_STRUCT.iWeather
					ENDIF
				BREAK
				CASE ciDM_HOST_OPTION_TRAFFIC				RETURN g_FMMC_STRUCT.iTraffic
				CASE ciDM_HOST_OPTION_VEHICLE				RETURN g_FMMC_STRUCT.iVehicleModel
				
				CASE ciDM_HOST_OPTION_WEAPONS
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked)	// Starting weapon is locked and all other weapons are based on creator
						RETURN ciDM_WEAPON_STARTING_LOCKED
					ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked)	// Locked to one specific weapon
						RETURN ciDM_WEAPON_LOCKED
					ELSE
						IF IS_THIS_A_KING_OF_THE_HILL_CORONA(FMMC_TYPE_DEATHMATCH, g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE])
						OR NOT IS_THIS_LEGACY_DM_CONTENT()
							RETURN ciDM_WEAPON_DEFAULT
						ELSE
							RETURN ciDM_WEAPON_STARTING_LOCKED									// Default is weapons are locked.
						ENDIF
					ENDIF					
				BREAK
				
				CASE ciDM_HOST_OPTION_FIXED_CAMERA		RETURN g_FMMC_STRUCT.iFixedCamera
				CASE ciDM_HOST_OPTION_NUM_LIVES			
					IF IS_THIS_LEGACY_DM_CONTENT()
						RETURN g_FMMC_STRUCT.iPlayerLives
					ELSE
						IF g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM
							RETURN g_FMMC_STRUCT.sDMCustomSettings.iTeamLives[0]
						ELSE
							RETURN g_FMMC_STRUCT.sDMCustomSettings.iFFALives
						ENDIF
					ENDIF
				BREAK
				CASE ciDM_HOST_OPTION_ROUNDS
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_EnableDeathmatchRoundsForLobby)
						RETURN g_FMMC_STRUCT.iNumOfRounds
					ELSE
						RETURN 1
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_MISSION
			IF g_bUseMissionHostOptionOverrides
				IF g_iMissionHostOptionOverrideDefault[iItem] != LOWEST_INT
					PRINTLN("[CORONA] - GET_CORONA_MENU_ITEM_DEFAULT - Override enabled for Mission option ", iItem, "; value = ", g_iMissionHostOptionOverrideDefault[iItem])
					RETURN g_iMissionHostOptionOverrideDefault[iItem]
				ENDIF
			ENDIF
		
			SWITCH iItem
				CASE ciMISSION_HOST_OPTION_TEAM_BAL			
					IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
					OR iMissionSubType = FMMC_MISSION_TYPE_CTF			// Team balancing defaults to on for CTF and LTS
					OR iMissionSubType = FMMC_MISSION_TYPE_LTS
					OR IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamRelations, FMMC_TEAM_DISABLE_BALANCING)
						RETURN 1
					ELSE
						RETURN 0
					ENDIF
				BREAK
				CASE ciMISSION_HOST_OPTION_DIFFICULTY		
					IF IS_THIS_A_FIXER_SHORT_TRIP()
						IF PIMenuData.eShortTripsSubMenu.eDifficulty = SS_NORMAL
							PRINTLN("[CORONA] - GET_CORONA_MENU_ITEM_DEFAULT - Difficulty - Fixer Short Trip - PIMenuData.eShortTripsSubMenu.eDifficulty = SS_NORMAL, returning DIFF_NORMAL")
							RETURN DIFF_NORMAL
						ELIF PIMenuData.eShortTripsSubMenu.eDifficulty = SS_HARD
							PRINTLN("[CORONA] - GET_CORONA_MENU_ITEM_DEFAULT - Difficulty - Fixer Short Trip - PIMenuData.eShortTripsSubMenu.eDifficulty = SS_HARD, returning DIFF_HARD")
							RETURN DIFF_HARD
						ENDIF
					ENDIF
					RETURN DIFF_NORMAL
				CASE ciMISSION_HOST_OPTION_TIME_OF_DAY		
					IF g_FMMC_STRUCT.iTimeOfDay >= ciMISSION_MAX_TYPE_TIME_OF_DAY
						RETURN ciMISSION_MAX_TYPE_TIME_OF_DAY
					ELSE
						RETURN g_FMMC_STRUCT.iTimeOfDay
					ENDIF
				BREAK
				CASE ciMISSION_HOST_OPTION_WEATHER
					//If we are setting a challenge time or playing then its always nice!
					IF IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
					//OR IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
						RETURN ciFMMC_WEATHER_OPTION_SUNNY
					ELSE
						RETURN g_FMMC_STRUCT.iWeather
					ENDIF
				BREAK
				CASE ciMISSION_HOST_OPTION_END_CONDITIONS	RETURN ciMISSION_END_ON_TIME
				CASE ciMISSION_HOST_OPTION_WEAPONS
				
					IF NOT IS_MISSION_VALID_FOR_FORCED_WEAPON()
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked)	// Starting weapon is locked and all other weapons are based on creator
							PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT, NOT IS_MISSION_VALID_FOR_FORCED_WEAPON(), ciDMInitialWepLocked ")
							RETURN ciDM_WEAPON_STARTING_LOCKED
						ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked)	// Locked to one specific weapon
							PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT, NOT IS_MISSION_VALID_FOR_FORCED_WEAPON(), ciDMWeaponsLocked ")
							RETURN ciDM_WEAPON_LOCKED
						ELSE
							PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT, NOT IS_MISSION_VALID_FOR_FORCED_WEAPON(), ciDM_WEAPON_DEFAULT ")
							RETURN ciDM_WEAPON_DEFAULT											// 2037494
						ENDIF
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked)	// Starting weapon is locked and all other weapons are based on creator
							PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT, IS_MISSION_VALID_FOR_FORCED_WEAPON(), ciDMInitialWepLocked ")
							RETURN ciDM_WEAPON_STARTING_LOCKED
						ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked)	// Locked to one specific weapon
							PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT, IS_MISSION_VALID_FOR_FORCED_WEAPON(), ciDMWeaponsLocked ")
							RETURN ciDM_WEAPON_LOCKED
						ELSE
							PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT, IS_MISSION_VALID_FOR_FORCED_WEAPON(), ciDM_WEAPON_STARTING_LOCKED ")
							RETURN ciDM_WEAPON_STARTING_LOCKED									// Default is weapons are locked.
						ENDIF
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_FIXED_CAMERA		RETURN g_FMMC_STRUCT.iFixedCamera
				
				CASE ciMISSION_HOST_OPTION_ROUNDS		
					
					// Override any extreme settings if we are in an open session
					IF NOT DID_I_COME_INTO_A_PRIVATE_SESSION()
						IF g_FMMC_STRUCT.iNumOfRounds > GET_CORONA_MAX_ROUNDS_FOR_TEAMS(g_FMMC_STRUCT.iNumberOfTeams)							
						
							PRINTLN("[CORONA] ciDM_HOST_OPTION_TIME_LIMIT - g_FMMC_STRUCT.iNumOfRounds = ", g_FMMC_STRUCT.iNumOfRounds, " , cap to ", GET_CORONA_MAX_ROUNDS_FOR_TEAMS(g_FMMC_STRUCT.iNumberOfTeams))
							g_FMMC_STRUCT.iNumOfRounds = GET_CORONA_MAX_ROUNDS_FOR_TEAMS(g_FMMC_STRUCT.iNumberOfTeams)	
						ENDIF
					ENDIF
					
					RETURN g_FMMC_STRUCT.iNumOfRounds
				
				CASE ciMISSION_HOST_OPTION_CLOTHING_SETUP	
					IF iMissionSubType = FMMC_MISSION_TYPE_VERSUS
					OR iMissionSubType = FMMC_MISSION_TYPE_COOP
					
						// We can override the creator default with tunables
						IF g_sMPTunables.bDEFAULT_OUTFIT_ADVERSARY_HALLOWEEN
							PRINTLN("[CORONA] ciDM_HOST_OPTION_TIME_LIMIT - g_sMPTunables.bDEFAULT_OUTFIT_ADVERSARY_HALLOWEEN = TRUE, force Halloween")
							RETURN VS_STYLE_HALLOWEEN
						ENDIF
						
						// We can override the creator default with tunables
						IF g_sMPTunables.bDEFAULT_OUTFIT_ADVERSARY_SOLO
							PRINTLN("[CORONA] ciDM_HOST_OPTION_TIME_LIMIT - g_sMPTunables.bDEFAULT_OUTFIT_ADVERSARY_SOLO = TRUE, force Solo")
							RETURN VS_STYLE_SOLO
						ENDIF						
						
						
						// Otherwise, take the default style from the creator if valid.
						IF g_FMMC_STRUCT.iVSDefaultStyleClass != -1
							PRINTLN("[CORONA] ciDM_HOST_OPTION_TIME_LIMIT - Default outfit style: g_FMMC_STRUCT.iVSDefaultStyleClass = ", g_FMMC_STRUCT.iVSDefaultStyleClass)
							RETURN g_FMMC_STRUCT.iVSDefaultStyleClass
						ENDIF
						
						// Return our player owned option as standard
						PRINTLN("[CORONA] ciDM_HOST_OPTION_TIME_LIMIT - No defined outfit style. Return ciCORONA_CLOTHING_SETUP_OPTION_0")
						RETURN VS_STYLE_CLASSIC
					ELSE
						RETURN ciCORONA_CLOTHING_SETUP_OPTION_1
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_OUTFIT_CUSTOMISATION		RETURN ci_HEIST_CORONA_OUTFIT_ACCESS_OUTFITS
				
				CASE ciMISSION_HOST_OPTION_DISABLE_BLIPS			RETURN 0
				
				
				CASE ciMISSION_HOST_OPTION_SUDDEN_DEATH				
					IF IS_CORONA_MISSION_SUDDEN_DEATH_AVAILABLE()
						RETURN CORONA_MISSION_SUDDEN_DEATH_OPT_1
					ELSE
						RETURN CORONA_MISSION_PICKUPS_NORMAL
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_VEHICLE_CLASS
					INT iVehicleClass
					// Grab the default vehicle from the creator
					iVehicleClass = GET_CORONA_DEFAULT_MISSION_VEHICLE_CLASS()
					
					IF iVehicleClass = -1
						RETURN 0
					ENDIF
						
					RETURN iVehicleClass				
				BREAK
				
				// Return the default number of teams from the creator
				CASE ciMISSION_HOST_OPTION_NUM_TEAMS
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciAUTO_NUMBER_OF_TEAMS)
						// Return -1 so that we know to show "Auto" and calculate the number of teams ourselves
						RETURN -1
					ENDIF
				
					RETURN g_FMMC_STRUCT.iNumberOfTeams
				BREAK
				
				CASE ciMISSION_HOST_OPTION_ROUND_DURATION
					IF g_FMMC_STRUCT.iCustomRoundLengthDefault > 0
						PRINTLN("[CORONA] GET_CORONA_MENU_ITEM_DEFAULT - ciMISSION_HOST_OPTION_ROUND_DURATION - g_FMMC_STRUCT.iCustomRoundLengthDefault = ", g_FMMC_STRUCT.iCustomRoundLengthDefault)
						RETURN g_FMMC_STRUCT.iCustomRoundLengthDefault
					ELSE
						IF g_FMMC_STRUCT.sFMMCEndConditions[0].iTimeLimit[0] = FMMC_OBJECTIVE_TIME_10_MINUTES
							RETURN 10
						ELIF g_FMMC_STRUCT.sFMMCEndConditions[0].iTimeLimit[0] = FMMC_OBJECTIVE_TIME_15_MINUTES
							RETURN 15
						ELIF g_FMMC_STRUCT.sFMMCEndConditions[0].iTimeLimit[0] = FMMC_OBJECTIVE_TIME_20_MINUTES
							RETURN 20
						ELSE
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
								RETURN 6
							ELSE
								RETURN 5
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_TARGET_SCORE
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN 3
					ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN 5
					ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN 300
					ENDIF
				
					RETURN g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
				BREAK
				
				CASE ciMISSION_HOST_OPTION_USE_BAR
					RETURN PICK_INT(!IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_CORONO_OPTION), 0, 1)
				BREAK
				
				CASE ciMISSION_HOST_OPTION_TEAM_LIVES
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN 6
					ELSE
						RETURN g_FMMC_STRUCT.iDefaultCoronaTeamLives
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_MISSION_TYPE
					// Reverse logic: if not set default to deathmatch
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_DEFAULT)
						RETURN 0
					ELSE
						RETURN 1
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_KILLS_MULTIPLIER
					RETURN g_FMMC_STRUCT.iTargetScoreMultiplierSetting
				BREAK
				
				CASE ciMISSION_HOST_OPTION_WEAPON_LOADOUT
					RETURN 0	// Standard
				BREAK
				
				CASE 27 // ciMISSION_HOST_OPTION_NUMBER_OF_LAPS & ciMISSION_HOST_OPTION_EXIT_TYPE
					IF SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
						RETURN ciSVM_EXIT_TYPE_HELI
					ELSE
						IF g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionLapDefault > 0
							RETURN g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionLapDefault
						ELSE
							RETURN g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionLapLimit
						ENDIF
					ENDIF
				BREAK
				
				
				CASE ciMISSION_HOST_OPTION_PICKUPS
					RETURN 1	// Enabled by default
				BREAK
				
				CASE ciMISSION_HOST_OPTION_VEHICLE_LIST
					RETURN 0
				BREAK
				
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_MG_TENNIS
			
			SWITCH iItem
				CASE ciTENNIS_HOST_OPTION_NUM_OF_SETS		RETURN 3
			ENDSWITCH
			
		BREAK
		
		CASE FMMC_TYPE_MG_GOLF
			
			SWITCH iItem
				CASE ciGOLF_HOST_OPTION_NUM_OF_HOLES		RETURN 8
			ENDSWITCH
			
		BREAK
		
		CASE FMMC_TYPE_SURVIVAL
			SWITCH iItem
				CASE ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES	RETURN ciSURVIVAL_ENDLESS_WAVES_DISABLED
				CASE ciSURVIVAL_HOST_OPTION_TIME_OF_DAY
					IF g_FMMC_STRUCT.iTimeOfDay >= ciMISSION_MAX_TYPE_TIME_OF_DAY
						RETURN ciMISSION_MAX_TYPE_TIME_OF_DAY
					ELSE
						RETURN g_FMMC_STRUCT.iTimeOfDay
					ENDIF
				BREAK
				CASE ciSURVIVAL_HOST_OPTION_WEATHER			RETURN g_FMMC_STRUCT.iWeather
			ENDSWITCH	
		BREAK

	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE: Returns the maximum setting value for selected option
FUNC INT GET_HOST_SELECTION_MIN(INT iMissionType, INT iSelectionInt)
	
	SWITCH iMissionType
		CASE FMMC_TYPE_MG_TENNIS
			SWITCH iSelectionInt
				CASE ciTENNIS_HOST_OPTION_NUM_OF_SETS		RETURN 3		// Only allow sets now (not games)
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_RACE
			IF g_bUseRCHostOptionOverrides
				IF g_iRCHostOptionOverrideMin[iSelectionInt] != LOWEST_INT
					PRINTLN("[CORONA] - GET_HOST_SELECTION_MIN - Override enabled for Race option ", iSelectionInt, "; value = ", g_iRCHostOptionOverrideMin[iSelectionInt])
					RETURN g_iRCHostOptionOverrideMin[iSelectionInt]
				ENDIF
			ENDIF
		
			SWITCH iSelectionInt
				CASE ciRC_HOST_OPTION_NUM_LAPS
					IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HOTRING_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
						RETURN g_sMPTunables.iSSAS_MIN_LAPS
					ENDIF
					
					IF g_FMMC_STRUCT.iMinimumLaps != 0
						RETURN g_FMMC_STRUCT.iMinimumLaps
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_MAX_WANTED
					RETURN g_FMMC_STRUCT.iRaceCoronaMinWantedLevel
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_MISSION
			IF g_bUseMissionHostOptionOverrides
				IF g_iMissionHostOptionOverrideMin[iSelectionInt] != LOWEST_INT
					PRINTLN("[CORONA] - GET_HOST_SELECTION_MIN - Override enabled for Mission option ", iSelectionInt, "; value = ", g_iMissionHostOptionOverrideMin[iSelectionInt])
					RETURN g_iMissionHostOptionOverrideMin[iSelectionInt]
				ENDIF
			ENDIF
		
			SWITCH iSelectionInt
				CASE ciMISSION_HOST_OPTION_DIFFICULTY
					IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
						RETURN DIFF_NORMAL
					ENDIF
				BREAK
			
				CASE ciMISSION_HOST_OPTION_ROUNDS			RETURN 1
				CASE ciMISSION_HOST_OPTION_CLOTHING_SETUP
					
					IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
						// url:bugstar:4236565 - Can we remove the lobby option for Player Owned clothing in the following missions?
						IF GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_MORGUE
						OR GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_SERVERFARM
						OR GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_STEALOSPREY
						OR GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_PREDATOR
							RETURN ciCORONA_CLOTHING_SETUP_OPTION_1
						ELSE
							RETURN ciCORONA_CLOTHING_SETUP_OPTION_0 
						ENDIF
					ENDIF
					
					#IF FEATURE_CASINO_HEIST
					IF IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
						RETURN ciCORONA_CLOTHING_SETUP_OPTION_1
					ENDIF
					#ENDIF
					
					IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
					OR IS_LOADED_MISSION_TYPE_FLOW_MISSION()
						RETURN ciCORONA_CLOTHING_SETUP_OPTION_0
					ELSE
						RETURN VS_STYLE_PLAYER_OWNED
					ENDIF
				BREAK
				CASE ciMISSION_HOST_OPTION_NUM_TEAMS
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciAUTO_NUMBER_OF_TEAMS)
						// Make it -1 so that we can caltulate it based on num players
						RETURN -1
					ENDIF
				
					RETURN g_FMMC_STRUCT.iMinNumberOfTeams
				BREAK
				
				CASE ciMISSION_HOST_OPTION_ROUND_DURATION
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN 4
					ELSE
						RETURN 5
					ENDIF
				BREAK		
				CASE ciMISSION_HOST_OPTION_TARGET_SCORE
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN 3
					ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN 200
					ENDIF
				
					RETURN 5
				BREAK
				
				CASE ciMISSION_HOST_OPTION_TEAM_LIVES
					RETURN g_FMMC_STRUCT.iLivesIncrementValue
				BREAK
				
				CASE 27 // ciMISSION_HOST_OPTION_NUMBER_OF_LAPS & ciMISSION_HOST_OPTION_EXIT_TYPE
					IF SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
						RETURN ciSVM_EXIT_TYPE_HELI
					ELSE
						IF g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionLapMinimum > 0
							RETURN g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionLapMinimum
						ELSE
							RETURN ciMISSION_MIN_NUMBER_OF_LAPS
						ENDIF
					ENDIF
				BREAK
				
			ENDSWITCH
		BREAK
		CASE FMMC_TYPE_DEATHMATCH
			IF g_bUseDMHostOptionOverrides
				IF g_iDMHostOptionOverrideMin[iSelectionInt] != LOWEST_INT
					PRINTLN("[CORONA] - GET_HOST_SELECTION_MIN - Override enabled for DM option ", iSelectionInt, "; value = ", g_iDMHostOptionOverrideMin[iSelectionInt])
					RETURN g_iDMHostOptionOverrideMin[iSelectionInt]
				ENDIF
			ENDIF
			
			SWITCH iSelectionInt
				CASE ciDM_HOST_OPTION_TYPE
					IF IS_THIS_A_KING_OF_THE_HILL_CORONA()
						RETURN FMMC_KING_OF_THE_HILL
					ENDIF
				BREAK
				CASE ciDM_HOST_OPTION_TARGET_SCORE
					IF IS_THIS_A_KING_OF_THE_HILL_CORONA()
						RETURN 60
					ENDIF
				BREAK
				CASE ciDM_HOST_OPTION_NUM_LIVES		RETURN g_FMMC_STRUCT.iMinimumCoronaTeamLives
				CASE ciDM_HOST_OPTION_ROUNDS	RETURN 1
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_MAX_FROM_LIVES_INCREMENT_VALUE()
	SWITCH g_FMMC_STRUCT.iLivesIncrementValue
		CASE 1	RETURN 5
		CASE 2	RETURN 10
		CASE 3	RETURN 15
		CASE 4	RETURN 20
		CASE 5	RETURN 25
	ENDSWITCH
	
	PRINTLN("[CORONA], GET_HOST_SELECTION_MAX - GET_MAX_FROM_LIVES_INCREMENT_VALUE - returning 40 by default")
	RETURN 40
ENDFUNC

/// PURPOSE: Returns the maximum setting value for selected option
FUNC INT GET_HOST_SELECTION_MAX(INT iMissionType, INT iSelectionInt, FMMC_SELECTED_ITEMS &sSelection, MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)


	SWITCH iMissionType
	
		// Return maximum selections for Race types
		CASE FMMC_TYPE_RACE
		//CASE FMMC_TYPE_TRIATHLON
			IF g_bUseRCHostOptionOverrides
				IF g_iRCHostOptionOverrideMax[iSelectionInt] != LOWEST_INT
					PRINTLN("[CORONA] - GET_HOST_SELECTION_MAX - Override enabled for Race option ", iSelectionInt, "; value = ", g_iRCHostOptionOverrideMax[iSelectionInt])
					RETURN g_iRCHostOptionOverrideMax[iSelectionInt]
				ENDIF
			ENDIF
			
			SWITCH iSelectionInt
			
				CASE ciRC_HOST_OPTION_VEHICLE_CLASS			RETURN GET_SELECTION_MAX_VEHICLE_CLASS(g_FMMC_STRUCT.iRaceType)
				CASE ciRC_HOST_OPTION_GTA_TEAM				RETURN 1	// ON or OFF
				CASE ciRC_HOST_OPTION_TYPE					RETURN ciRC_MAX_TYPE_RACE_TYPE				
				CASE ciRC_HOST_OPTION_NUM_LAPS				
					// If it's a stunt series race return 6 (tunable default) 
					IF CV2_IS_THIS_JOB_A_CORONA_FLOW_JOB() 
						RETURN g_sMPTunables.iRC_HOST_OPTION_STUNT_RACE_NUM_LAPS 
					ENDIF 
					 /* Otherwise return 98 */ 
					
					IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HOTRING_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
						RETURN 24
					ENDIF
					
					IF IS_SPECIAL_VEHICLE_RACE()
					OR IS_STUNT_RACE()
					OR IS_TRANSFORM_RACE()
					OR IS_TARGET_ASSAULT_RACE()
						IF IS_CURRENT_MISSION_ROCKSTAR_CREATED()
							RETURN 6
						ENDIF
					ENDIF
					
					IF g_FMMC_STRUCT.iMaximumLaps != 0
						RETURN g_FMMC_STRUCT.iMaximumLaps
					ENDIF
					
					RETURN ciRC_MAX_TYPE_NUM_LAPS
				BREAK
				CASE ciRC_HOST_OPTION_TRAFFIC				RETURN ciRC_MAX_TYPE_RACE_TRAFFIC		
				CASE ciRC_HOST_OPTION_TIME_OF_DAY			RETURN ciRC_MAX_TYPE_RACE_TIME_OF_DAY		
				CASE ciRC_HOST_OPTION_COPS					RETURN ciRC_MAX_TYPE_RACE_COPS
				CASE ciRC_HOST_OPTION_MAX_WANTED			RETURN g_FMMC_STRUCT.iRaceCoronaMaxWantedLevel
				CASE ciRC_HOST_OPTION_WEATHER				RETURN ciRC_MAX_TYPE_WEATHER
				CASE ciRC_HOST_OPTION_SWAP_ROLES			RETURN ciRC_MAX_TYPE_RACE_SWAP_ROLES
				CASE ciRC_HOST_OPTION_SPLIT_COMPARE			RETURN ciRC_MAX_TYPE_RACE_SPLIT_COMPARE
				CASE ciRC_HOST_OPTION_CUSTOM_VEHICLE		RETURN 1
				CASE ciRC_HOST_OPTION_CATCHUP_CODE			RETURN 1
				CASE ciRC_HOST_OPTION_SLIP_STREAM			RETURN 1
				CASE ciRC_HOST_OPTION_FIXED_CAMERA			RETURN ciMAX_TYPE_FIXED_CAMERAS
				CASE ciRC_HOST_OPTION_DESTROY_LOSER			RETURN ciCORONA_DESTROY_VEHICLE_MAX_OPTIONS
//				#IF FEATURE_IMPORT_EXPORT
				CASE ciRC_HOST_OPTION_AGGREGATE_POS			RETURN ciCORONA_AGGREGATE_RACE_POSITION_ON
//				#ENDIF
				#IF FEATURE_GEN9_EXCLUSIVE
				CASE ciRC_HOST_OPTION_HSW_MODS				RETURN ciCORONA_HSW_MODS_DISABLED
				#ENDIF
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_BASE_JUMP
			SWITCH iSelectionInt
				CASE ciPARA_HOST_OPTION_TIME_OF_DAY			RETURN ciRC_MAX_TYPE_RACE_TIME_OF_DAY
				CASE ciPARA_HOST_OPTION_WEATHER				RETURN ciRC_MAX_TYPE_WEATHER
				CASE ciPARA_HOST_OPTION_SHOOTING			RETURN 1
				CASE ciPARA_HOST_OPTION_FIXED_CAMERA		RETURN ciMAX_TYPE_FIXED_CAMERAS
			ENDSWITCH
		BREAK

		// Return maximum selections for Deathmatch types
		CASE FMMC_TYPE_DEATHMATCH
			IF g_bUseDMHostOptionOverrides
				IF g_iDMHostOptionOverrideMax[iSelectionInt] != LOWEST_INT
					PRINTLN("[CORONA] - GET_HOST_SELECTION_MAX - Override enabled for DM option ", iSelectionInt, "; value = ", g_iDMHostOptionOverrideMax[iSelectionInt])
					RETURN g_iDMHostOptionOverrideMax[iSelectionInt]
				ENDIF
			ENDIF
		
			SWITCH iSelectionInt 
				CASE ciDM_HOST_OPTION_TIME_LIMIT
					IF DID_I_COME_INTO_A_PRIVATE_SESSION()
						RETURN ciDM_MAX_TYPE_TIME_LIMIT	
					ELSE
						RETURN DM_DURATION_20
					ENDIF
				BREAK
				CASE ciDM_HOST_OPTION_NUM_TEAMS	
				
					IF g_FMMC_STRUCT.iMaxNumberOfTeams > MAX_NUM_DM_TEAMS
						g_FMMC_STRUCT.iMaxNumberOfTeams = MAX_NUM_DM_TEAMS
					ENDIF

					IF IS_DM_FIXED_TO_MIN_TEAMS()
						g_FMMC_STRUCT.iMaxNumberOfTeams = 2
					ENDIF
					
					IF IS_THIS_A_TEAM_CORONA(sSelection)
						IF GET_MISSION_TYPE_MAX(sLaunchMissionDetails) < g_FMMC_STRUCT.iMaxNumberOfTeams
							PRINTLN("[CORONA] GET_HOST_SELECTION_MAX - ciDM_HOST_OPTION_NUM_TEAMS - GET_MISSION_TYPE_MAX(sLaunchMissionDetails) (",GET_MISSION_TYPE_MAX(sLaunchMissionDetails),") < g_FMMC_STRUCT.iMaxNumberOfTeams (",g_FMMC_STRUCT.iMaxNumberOfTeams,"), overriding the max number of teams")
							g_FMMC_STRUCT.iMaxNumberOfTeams = GET_MISSION_TYPE_MAX(sLaunchMissionDetails)
						ELSE
							PRINTLN("[CORONA] GET_HOST_SELECTION_MAX - ciDM_HOST_OPTION_NUM_TEAMS - GET_MISSION_TYPE_MAX(sLaunchMissionDetails) (",GET_MISSION_TYPE_MAX(sLaunchMissionDetails),") >= g_FMMC_STRUCT.iMaxNumberOfTeams (",g_FMMC_STRUCT.iMaxNumberOfTeams,")")
						ENDIF
					ENDIF

					RETURN (g_FMMC_STRUCT.iMaxNumberOfTeams - 2)
					
				CASE ciDM_HOST_OPTION_TEAM_BAL				RETURN ciDM_MAX_TYPE_TEAMS_BALANCING	
				CASE ciDM_HOST_OPTION_TARGET_SCORE			
					IF IS_THIS_A_KING_OF_THE_HILL_CORONA()
						RETURN 150
					ENDIF
					RETURN ciDM_MAX_TYPE_TARGET_SCORE 					
				CASE ciDM_HOST_OPTION_OPP_HEALTH_BAR		RETURN ciDM_MAX_TYPE_OPONANT_HEALTH_BAR
				CASE ciDM_HOST_OPTION_GAMERTAGS				RETURN 1
				CASE ciDM_HOST_OPTION_TYPE
					IF IS_THIS_A_KING_OF_THE_HILL_CORONA()
						RETURN FMMC_DM_TYPE_TEAM_KOTH
					ENDIF
				
					IF (sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_VEHICLE)
						RETURN ciDM_MAX_TYPE_DEATHMATCH_TYPE
					ELSE
						RETURN (ciDM_MAX_TYPE_DEATHMATCH_TYPE-1)
					ENDIF				
				RETURN ciDM_MAX_TYPE_DEATHMATCH_TYPE	
//				CASE ciDM_HOST_OPTION_VEHICLE_DM			RETURN ciDM_MAX_TYPE_VEHICLE_DEATHMATCH_LOCKED	
//				CASE ciDM_HOST_OPTION_VEHICLE				RETURN ciDM_MAX_TYPE_VEHICLE_DEATHMATCH_VEHICLE	
				CASE ciDM_HOST_OPTION_TIME_OF_DAY			RETURN ciDM_MAX_TYPE_TIME_OF_DAY	
				CASE ciDM_HOST_OPTION_TRAFFIC				RETURN ciDM_MAX_TYPE_VEHICLE_DENSITY
				CASE ciDM_HOST_OPTION_WEATHER				RETURN ciDM_MAX_TYPE_WEATHER
				CASE ciDM_HOST_OPTION_WEAPONS				RETURN ciDM_MAX_TYPE_DM_WEAPONS
				CASE ciDM_HOST_OPTION_FIXED_CAMERA			RETURN ciMAX_TYPE_FIXED_CAMERAS
				CASE ciDM_HOST_OPTION_NUM_LIVES				RETURN g_FMMC_STRUCT.iMaximumCoronaTeamLives
				CASE ciDM_HOST_OPTION_ROUNDS
				
					// If we are in a private session (invite only  / closed) limit to normal ranges
					IF DID_I_COME_INTO_A_PRIVATE_SESSION()
						IF g_FMMC_STRUCT.iNumberOfTeams = 4
							RETURN (ciMISSION_MAX_TYPE_ROUNDS-4)
						ELIF g_FMMC_STRUCT.iNumberOfTeams = 3
							RETURN (ciMISSION_MAX_TYPE_ROUNDS-2)
						ELSE
							RETURN ciMISSION_MAX_TYPE_ROUNDS
						ENDIF
					ELSE
						RETURN GET_CORONA_MAX_ROUNDS_FOR_TEAMS(g_FMMC_STRUCT.iNumberOfTeams)
					ENDIF
					
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_MISSION
			IF g_bUseMissionHostOptionOverrides
				IF g_iMissionHostOptionOverrideMax[iSelectionInt] != LOWEST_INT
					PRINTLN("[CORONA] - GET_HOST_SELECTION_MAX - Override enabled for Mission option ", iSelectionInt, "; value = ", g_iMissionHostOptionOverrideMax[iSelectionInt])
					RETURN g_iMissionHostOptionOverrideMax[iSelectionInt]
				ENDIF
			ENDIF
		
			SWITCH iSelectionInt
				CASE ciMISSION_HOST_OPTION_TEAM_BAL			RETURN ciMISSION_MAX_TYPE_BALANCE_TEAMS
				CASE ciMISSION_HOST_OPTION_DIFFICULTY		RETURN ciMISSION_MAX_TYPE_DIFFICULITY
				CASE ciMISSION_HOST_OPTION_TIME_OF_DAY		RETURN ciMISSION_MAX_TYPE_TIME_OF_DAY	
				CASE ciMISSION_HOST_OPTION_WEATHER			RETURN ciMISSION_MAX_TYPE_WEATHER
				CASE ciMISSION_HOST_OPTION_END_CONDITIONS	RETURN ciMISSION_MAX_TYPE_END_CONDITIONS
				CASE ciMISSION_HOST_OPTION_WEAPONS			RETURN ciDM_MAX_TYPE_DM_WEAPONS
				CASE ciMISSION_HOST_OPTION_FIXED_CAMERA		RETURN ciMAX_TYPE_FIXED_CAMERAS
					CASE ciMISSION_HOST_OPTION_ROUNDS
					
						// If we are in a private session (invite only  / closed) limit to normal ranges
						IF DID_I_COME_INTO_A_PRIVATE_SESSION()
							IF g_FMMC_STRUCT.iNumberOfTeams = 4
								RETURN (ciMISSION_MAX_TYPE_ROUNDS-4)
							ELIF g_FMMC_STRUCT.iNumberOfTeams = 3
								RETURN (ciMISSION_MAX_TYPE_ROUNDS-2)
							ELSE
								RETURN ciMISSION_MAX_TYPE_ROUNDS
							ENDIF
						ELSE
							RETURN GET_CORONA_MAX_ROUNDS_FOR_TEAMS(g_FMMC_STRUCT.iNumberOfTeams)
						ENDIF
					BREAK
				CASE ciMISSION_HOST_OPTION_CLOTHING_SETUP
					IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
					OR IS_LOADED_MISSION_TYPE_FLOW_MISSION()
					OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
					#IF FEATURE_CASINO_HEIST
					OR IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
					#ENDIF
						RETURN ciCORONA_CLOTHING_SETUP_OPTION_1
					ELSE
						RETURN (MAX_VS_STYLES-1)
					ENDIF
				BREAK				
				
				CASE ciMISSION_HOST_OPTION_OUTFIT_CUSTOMISATION		RETURN ci_HEIST_CORONA_OUTFIT_ACCESS_MAX
				
				CASE ciMISSION_HOST_OPTION_DISABLE_BLIPS			RETURN 1
				CASE ciMISSION_HOST_OPTION_SUDDEN_DEATH				
					IF IS_CORONA_MISSION_SUDDEN_DEATH_AVAILABLE()
						RETURN CORONA_MISSION_SUDDEN_DEATH_OPT_MAX
					ELSE
						RETURN CORONA_MISSION_PICKUPS_OPT_MAX
					ENDIF
				BREAK
				CASE ciMISSION_HOST_OPTION_VEHICLE_CLASS			RETURN VEHICLE_LIBRARY_MAX
				
				CASE ciMISSION_HOST_OPTION_NUM_TEAMS				RETURN g_FMMC_STRUCT.iMaxNumberOfTeams
				
				CASE ciMISSION_HOST_OPTION_ROUND_DURATION		
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN 10	// 10 mins, 2 minute increments
					ELSE
						RETURN 15	// 15 mins, 5 minute increments
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_TARGET_SCORE				
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN 5
					ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN 7
					ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN 400
					ENDIF
				
					RETURN 40 // ciMISSION_END_TYPE_SCORE_LIMIT_MAX
				BREAK
				
				CASE ciMISSION_HOST_OPTION_USE_BAR					RETURN 1
				
				CASE ciMISSION_HOST_OPTION_TEAM_LIVES				RETURN GET_MAX_FROM_LIVES_INCREMENT_VALUE()
				CASE ciMISSION_HOST_OPTION_KILLS_MULTIPLIER			RETURN 2 // Large
				CASE ciMISSION_HOST_OPTION_MISSION_TYPE				RETURN 1
				CASE ciMISSION_HOST_OPTION_HIGHEST_SCORE			RETURN 1
				CASE ciMISSION_HOST_OPTION_WEAPON_LOADOUT			
					IF IS_BIT_SET( g_FMMC_STRUCT.sTeamData[0].iTeamBitSet, ciTEAM_BS_SET_STARTING_INVENTORY_CHOSEN_IN_LOBBY)
						RETURN MC_GET_MAX_NUMBER_OF_VALID_INVENTORIES()-1
					ENDIF
					RETURN 3
				
				CASE 27 // ciMISSION_HOST_OPTION_NUMBER_OF_LAPS & ciMISSION_HOST_OPTION_EXIT_TYPE
					IF SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
						RETURN ciSVM_EXIT_TYPE_GROUND
					ELSE
						IF g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionLapLimit > 0
							RETURN g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionLapLimit
						ELSE
							RETURN ciMISSION_MAX_NUMBER_OF_LAPS
						ENDIF
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_PICKUPS					RETURN 1 // Enabled
				
				CASE ciMISSION_HOST_OPTION_VEHICLE_LIST				RETURN 1 // True (vehicle list 2)
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_MG_TENNIS
			SWITCH iSelectionInt
				CASE ciTENNIS_HOST_OPTION_NUM_OF_SETS		RETURN ciTN_MAX_TYPE_NUMBER_OF_SETS
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_MG_GOLF
			SWITCH iSelectionInt
				CASE ciGOLF_HOST_OPTION_WEATHER				RETURN ciGOLG_MAX_TYPE_WEATHER_TYPE
				CASE ciGOLF_HOST_OPTION_START_HOLE			RETURN ciGOLG_MAX_TYPE_STARTING_HOLE
				CASE ciGOLF_HOST_OPTION_NUM_OF_HOLES		RETURN ciGOLG_MAX_TYPE_NUMBER_OF_HOLES
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_MG_DARTS
			SWITCH iSelectionInt
				CASE ciDARTS_HOST_OPTION_NUM_OF_LEGS		RETURN ciDARTS_MAX_TYPE_LEGS
				CASE ciDARTS_HOST_OPTION_NUM_OF_SETS		RETURN ciDARTS_MAX_SETS
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_MG_SHOOTING_RANGE
			SWITCH iSelectionInt
				CASE ciSHOOTING_HOST_OPTION_MATCH
					IF GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION() > 1
						RETURN ciSHOOTING_MAX_TYPE_MATCHES
					ELSE
						RETURN ciSHOOTING_MAX_TYPE_MATCHES + 1
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_SURVIVAL
			SWITCH iSelectionInt
				CASE ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES	RETURN ciSURVIVAL_ENDLESS_WAVES_ENABLED
				CASE ciSURVIVAL_HOST_OPTION_TIME_OF_DAY		RETURN ciSURVIVAL_MAX_TYPE_TIME_OF_DAY	
				CASE ciSURVIVAL_HOST_OPTION_WEATHER			RETURN ciSURVIVAL_MAX_TYPE_WEATHER
			ENDSWITCH
		BREAK
	
	ENDSWITCH

	//Shouldn't get in here unless you break it.
	PRINTLN("[CORONA] GET_HOST_SELECTION_MAX")
	PRINTLN("	iMissionType   = ", iMissionType)
	PRINTLN("	iSelectionInt  = ", iSelectionInt)
	SCRIPT_ASSERT("[CORONA] GET_HOST_SELECTION_MAX - Unknown option passed in ")
	RETURN 1
ENDFUNC

FUNC INT GET_CLIENT_SELECTION_MAX(INT iMissionType, INT iSelectionInt, FMMC_SELECTED_ITEMS &sSelection, INT iTotalCustomVehicles)

	SWITCH iMissionType
		CASE FMMC_TYPE_MISSION
			SWITCH iSelectionInt
				CASE ciMISSION_CLIENT_OPTION_TEAM			RETURN (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)
				CASE ciMISSION_CLIENT_OPTION_SEAT			RETURN ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_TURRET
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_RACE
			SWITCH iSelectionInt
						
				CASE ciRC_CLIENT_OPTION_VEHICLE				
					IF NOT CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION(DEFAULT, DEFAULT, iTotalCustomVehicles)
						RETURN 0
					ELSE
						RETURN GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])	
					ENDIF
				BREAK
				CASE ciRC_CLIENT_OPTION_PARTNER				RETURN (g_iNoOfOtherPlayersinMyVote+2)
				CASE ciRC_CLIENT_OPTION_ROLE				RETURN ciRC_MAX_TYPE_ROLE
			ENDSWITCH
		BREAK
	ENDSWITCH

	//Shouldn't get in here unless you break it.
	PRINTLN("[CORONA] GET_CLIENT_SELECTION_MAX")
	PRINTLN("	iMenuType      = ", iMissionType)
	PRINTLN("	iSelectionInt  = ", iSelectionInt)

	RETURN 0
ENDFUNC

PROC SETUP_CORONA_MENU_OPTIONS_DEFAULT(INT iMissionType, FMMC_SELECTED_ITEMS &sSelection, INT iMissionSubType)
	
	PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - setting menu items to default for mission type: ", iMissionType, "  subtype: ", iMissionSubType)
	PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT ------------------------")
		
	SET_g_sDM_SB_CoronaOptions_TO_DEFAULT()
	SET_g_sRC_SB_CoronaOptions_TO_DEFAULT()
	
	VECTOR vPlayerCoords
	
	SIMPLE_INTERIORS eSimpleInteriorID = SIMPLE_INTERIOR_INVALID
	
	SWITCH iMissionType
		CASE FMMC_TYPE_RACE
		
			sSelection.iSelection[ciRC_HOST_OPTION_TYPE]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_TYPE)
			sSelection.iSelection[ciRC_HOST_OPTION_GTA_TEAM]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_GTA_TEAM)
			sSelection.iSelection[ciRC_HOST_OPTION_NUM_LAPS]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_NUM_LAPS)				
			sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_VEHICLE_CLASS)				
			sSelection.iSelection[ciRC_HOST_OPTION_TIME_OF_DAY]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_TIME_OF_DAY)				
			sSelection.iSelection[ciRC_HOST_OPTION_WEATHER]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_WEATHER)					
			sSelection.iSelection[ciRC_HOST_OPTION_TRAFFIC]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_TRAFFIC)					
			sSelection.iSelection[ciRC_HOST_OPTION_COPS]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_COPS)
			sSelection.iSelection[ciRC_HOST_OPTION_MAX_WANTED]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_MAX_WANTED)
			sSelection.iSelection[ciRC_HOST_OPTION_SWAP_ROLES]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_SWAP_ROLES)
			sSelection.iSelection[ciRC_HOST_OPTION_SPLIT_COMPARE]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_SPLIT_COMPARE)
			sSelection.iSelection[ciRC_HOST_OPTION_CUSTOM_VEHICLE]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_CUSTOM_VEHICLE)
			sSelection.iSelection[ciRC_HOST_OPTION_CATCHUP_CODE]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_CATCHUP_CODE)
			sSelection.iSelection[ciRC_HOST_OPTION_SLIP_STREAM]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_SLIP_STREAM)
			sSelection.iSelection[ciRC_HOST_OPTION_FIXED_CAMERA]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_FIXED_CAMERA)
			
			sSelection.iSelection[ciRC_HOST_OPTION_DESTROY_LOSER]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_DESTROY_LOSER)
			
//			#IF FEATURE_IMPORT_EXPORT
			sSelection.iSelection[ciRC_HOST_OPTION_AGGREGATE_POS] 	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_AGGREGATE_POS)
//			#ENDIF

			#IF FEATURE_GEN9_EXCLUSIVE
			sSelection.iSelection[ciRC_HOST_OPTION_HSW_MODS] 	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_RACE, ciRC_HOST_OPTION_HSW_MODS)
			#ENDIF
			
			IF IS_ARENA_WARS_JOB()
				g_TransitionSessionNonResetVars.PlayerMissionStartLocation = GET_PLAYER_COORDS(PLAYER_ID())
				
				IF IS_ARENA_WARS_JOB(TRUE)
					sSelection.iSelection[ciRC_HOST_OPTION_WEATHER] = SELECT_RANDOM_ARENA_LIGHTING()
					PRINTLN("[CORONA][CORONA_ARENA_LIGHTING] SETUP_CORONA_MENU_OPTIONS_DEFAULT - Race, sSelection.iSelection[ciRC_HOST_OPTION_WEATHER] = ", sSelection.iSelection[ciRC_HOST_OPTION_WEATHER], "(", GET_ARENA_LIGHTING_STRING(sSelection.iSelection[ciRC_HOST_OPTION_WEATHER]),")")
				ENDIF
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_BASE_JUMP
			sSelection.iSelection[ciPARA_HOST_OPTION_TIME_OF_DAY]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_BASE_JUMP, ciPARA_HOST_OPTION_TIME_OF_DAY)		
			sSelection.iSelection[ciPARA_HOST_OPTION_WEATHER]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_BASE_JUMP, ciPARA_HOST_OPTION_WEATHER)		
			sSelection.iSelection[ciPARA_HOST_OPTION_SHOOTING]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_BASE_JUMP, ciPARA_HOST_OPTION_SHOOTING)		
			sSelection.iSelection[ciPARA_HOST_OPTION_FIXED_CAMERA]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_BASE_JUMP, ciPARA_HOST_OPTION_FIXED_CAMERA)
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH
		
			sSelection.iSelection[ciDM_HOST_OPTION_TYPE]				= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_TYPE, iMissionSubType)		
			sSelection.iSelection[ciDM_HOST_OPTION_NUM_TEAMS]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_NUM_TEAMS)				
			sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_TEAM_BAL)				
			sSelection.iSelection[ciDM_HOST_OPTION_TIME_LIMIT]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_TIME_LIMIT)				
			sSelection.iSelection[ciDM_HOST_OPTION_TARGET_SCORE]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_TARGET_SCORE)
			sSelection.iSelection[ciDM_HOST_OPTION_GAMERTAGS]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_GAMERTAGS)
			sSelection.iSelection[ciDM_HOST_OPTION_OPP_HEALTH_BAR]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_OPP_HEALTH_BAR)		
			sSelection.iSelection[ciDM_HOST_OPTION_TIME_OF_DAY]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_TIME_OF_DAY)
			sSelection.iSelection[ciDM_HOST_OPTION_WEATHER]				= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_WEATHER)					
			sSelection.iSelection[ciDM_HOST_OPTION_TRAFFIC]				= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_TRAFFIC)					
//			sSelection.iSelection[ciDM_HOST_OPTION_VEHICLE_DM]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_VEHICLE_DM )		
			sSelection.iSelection[ciDM_HOST_OPTION_VEHICLE]				= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_VEHICLE)
			sSelection.iSelection[ciDM_HOST_OPTION_WEAPONS]				= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_WEAPONS)
			sSelection.iSelection[ciDM_HOST_OPTION_FIXED_CAMERA]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_FIXED_CAMERA)
			sSelection.iSelection[ciDM_HOST_OPTION_ROUNDS]				= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_ROUNDS)	
			sSelection.iSelection[ciDM_HOST_OPTION_NUM_LIVES]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_DEATHMATCH, ciDM_HOST_OPTION_NUM_LIVES)
			
			
			IF IS_ARENA_WARS_JOB()
				g_TransitionSessionNonResetVars.PlayerMissionStartLocation = GET_PLAYER_COORDS(PLAYER_ID())
				
				IF IS_ARENA_WARS_JOB(TRUE)
					sSelection.iSelection[ciDM_HOST_OPTION_WEATHER] = SELECT_RANDOM_ARENA_LIGHTING()
					PRINTLN("[CORONA][CORONA_ARENA_LIGHTING] SETUP_CORONA_MENU_OPTIONS_DEFAULT - Deathmatch, sSelection.iSelection[ciDM_HOST_OPTION_WEATHER] = ", sSelection.iSelection[ciDM_HOST_OPTION_WEATHER], "(", GET_ARENA_LIGHTING_STRING(sSelection.iSelection[ciDM_HOST_OPTION_WEATHER]),")")
				ENDIF
			ENDIF
			
		BREAK
		
		CASE FMMC_TYPE_MISSION
			
			sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_NUM_TEAMS)
			sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_DIFFICULTY)
			sSelection.iSelection[ciMISSION_HOST_OPTION_WEATHER]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_WEATHER)
			sSelection.iSelection[ciMISSION_HOST_OPTION_END_CONDITIONS]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_END_CONDITIONS)
			sSelection.iSelection[ciMISSION_HOST_OPTION_TEAM_BAL]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_TEAM_BAL, iMissionSubType)
			sSelection.iSelection[ciMISSION_HOST_OPTION_TIME_OF_DAY]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_TIME_OF_DAY)
			sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPONS]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_WEAPONS)	
			sSelection.iSelection[ciMISSION_HOST_OPTION_FIXED_CAMERA]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_FIXED_CAMERA)	
				sSelection.iSelection[ciMISSION_HOST_OPTION_ROUNDS]				= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_ROUNDS)	
			sSelection.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP]			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_CLOTHING_SETUP, iMissionSubType)
			sSelection.iSelection[ciMISSION_HOST_OPTION_OUTFIT_CUSTOMISATION] 	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_OUTFIT_CUSTOMISATION, iMissionSubType)
			sSelection.iSelection[ciMISSION_HOST_OPTION_DISABLE_BLIPS] 			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_DISABLE_BLIPS, iMissionSubType)
			sSelection.iSelection[ciMISSION_HOST_OPTION_SUDDEN_DEATH]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_SUDDEN_DEATH)
			
			sSelection.iSelection[ciMISSION_HOST_OPTION_VEHICLE_CLASS]	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_VEHICLE_CLASS)
			
			sSelection.iSelection[ciMISSION_HOST_OPTION_TARGET_SCORE] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_TARGET_SCORE)
			sSelection.iSelection[ciMISSION_HOST_OPTION_USE_BAR] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_USE_BAR)
			sSelection.iSelection[ciMISSION_HOST_OPTION_ROUND_DURATION] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_ROUND_DURATION)
			
			sSelection.iSelection[ciMISSION_HOST_OPTION_TEAM_LIVES] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_TEAM_LIVES)
			sSelection.iSelection[ciMISSION_HOST_OPTION_MISSION_TYPE] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_MISSION_TYPE)
			sSelection.iSelection[ciMISSION_HOST_OPTION_KILLS_MULTIPLIER] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_KILLS_MULTIPLIER)
			sSelection.iSelection[ciMISSION_HOST_OPTION_HIGHEST_SCORE] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_HIGHEST_SCORE)
			sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPON_LOADOUT] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_WEAPON_LOADOUT)
			IF SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
				sSelection.iSelection[ciMISSION_HOST_OPTION_EXIT_TYPE] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_EXIT_TYPE)
			ELSE
				sSelection.iSelection[ciMISSION_HOST_OPTION_NUMBER_OF_LAPS] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_NUMBER_OF_LAPS)
			ENDIF
			
			 // FEATURE_BIKER
			
			
			sSelection.iSelection[ciMISSION_HOST_OPTION_PILOT] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_PILOT)
			
			sSelection.iSelection[ciMISSION_HOST_OPTION_PICKUPS] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_PICKUPS)
			
			sSelection.iSelection[ciMISSION_HOST_OPTION_VEHICLE_LIST] = GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MISSION, ciMISSION_HOST_OPTION_VEHICLE_LIST)
			
			IF SHOULD_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, clear the start vector")
				SET_PLAYER_CONTACT_MISSION_COORDS_TO_ZERO()
				vPlayerCoords = g_TransitionSessionNonResetVars.PlayerMissionStartLocation
			ELSE
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, get current host location")				
					
					IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
						//SIMPLE_INTERIOR_DETAILS interiorDetails
						//GET_SIMPLE_INTERIOR_DETAILS(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), interiorDetails)
						FLOAT fTemp
						
						IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID()) AND IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(g_ownerOfArmoryAircraftPropertyIAmIn)
							eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_DEFUNCT_BASE_ID(GET_OWNED_DEFUNCT_BASE(g_ownerOfArmoryAircraftPropertyIAmIn)) 
							
							IF eSimpleInteriorID != SIMPLE_INTERIOR_INVALID
								GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT(eSimpleInteriorID, 0, vPlayerCoords, fTemp, FALSE)
								PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, get current host location - Player is inside of an Avenger stored in a Facility - vPlayerCoords = ", vPlayerCoords)	
							ELSE
								PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, get current host location - Unable to determine which simple interior player is in.")
							ENDIF
						ELSE
							GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior, 0, vPlayerCoords, fTemp, FALSE)
							PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, get current host location - IS_PLAYER_IN_ANY_SIMPLE_INTERIOR - vPlayerCoords = ", vPlayerCoords)		
							
							IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())	
							AND NOT IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
								vPlayerCoords = GET_NEAREST_TEMP_CORONA_LOCATION(vPlayerCoords)
								PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, host is in water. Use nearest dummy corona location: ", vPlayerCoords)
							ENDIF
							
						ENDIF
					ELSE
						vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
						PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, get current host location  - vPlayerCoords = ", vPlayerCoords)
					ENDIF 
					
					IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
						IF IS_PROPERTY_ONLY_A_GARAGE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
							vPlayerCoords = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].garage.vSafeCoronaLoc
							PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, get location of interior (GARAGE) safe location: ", vPlayerCoords)
						ELSE
							IF (iMissionSubType = FMMC_MISSION_TYPE_HEIST)
							OR (iMissionSubType = FMMC_MISSION_TYPE_PLANNING)
								// KGM 14/6/14: Player is in a heist-related corona, so setting Heist Corona location as the player coords
								vPlayerCoords = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vHeistPlanningLocate
								PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION (HEIST-RELATED), get location of interior (HOUSE) Heist Corona location: ", vPlayerCoords)
							ELSE
								// FEATURE_HEIST_PLANNING
								// KGM 14/6/14: The original lines of script - now used only if the player isn't in a Heist-Related corona
								vPlayerCoords = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vSafeCoronaLoc
								PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, get location of interior (HOUSE) safe location: ", vPlayerCoords)
							ENDIF
								// FEATURE_HEIST_PLANNING
							
							IF ARE_VECTORS_EQUAL(vPlayerCoords, <<0,0,0>>)
								vPlayerCoords = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].entrance[0].vBuzzerLoc
								PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, get location of interior (BUZZER) safe location: ", vPlayerCoords)
							ENDIF							
						ENDIF
						
						
					ELSE
						IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
						OR IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
							vPlayerCoords = GET_NEAREST_TEMP_CORONA_LOCATION(vPlayerCoords)
							PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, host is in water. Use nearest dummy corona location: ", vPlayerCoords)
						ENDIF
					ENDIF
					
					IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
						vPlayerCoords = GANGOPS_GET_GANG_OPS_BOARD_START_POS()
						PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW, set location: ", vPlayerCoords)
					ENDIF
					
					#IF FEATURE_CASINO_HEIST
					IF IS_THIS_MISSION_A_CASINO_HEIST_INTRO_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
						vPlayerCoords = GET_CASINO_HEIST_BOARD_START_POS()
						PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - IS_THIS_MISSION_A_CASINO_HEIST_MISSION, set location: ", vPlayerCoords)
					ENDIF
					#ENDIF
					
					#IF FEATURE_HEIST_ISLAND
					IF IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
						vPlayerCoords = GET_HEIST_ISLAND_BOARD_START_POS()
						PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - IS_THIS_MISSION_A_HEIST_ISLAND_MISSION, set location: ", vPlayerCoords)
					ENDIF
					#ENDIF
					
					IF IS_THIS_A_TUNER_ROBBERY_FINALE()
						vPlayerCoords = GET_TUNER_BOARD_START_POS()
						PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - IS_THIS_A_TUNER_ROBBERY_FINALE, set location: ", vPlayerCoords)
					ENDIF
					
					IF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ()
						vPlayerCoords = GET_FIXER_STORY_START_POS()
						PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - IS_THIS_A_FIXER_STORY_MISSION, set location: ", vPlayerCoords)
					ENDIF
					
					IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
						vPlayerCoords = GET_CAYO_PERICO_SERIES_START_POS()
						PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES, set location: ", vPlayerCoords)
					ENDIF
					
					IF IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
						vPlayerCoords = GET_ULP_MISSION_LOCATION()
						PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION, set location: ", vPlayerCoords)
					ENDIF
					
					#IF FEATURE_DLC_2_2022
					IF IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
						vPlayerCoords = GET_XMAS22_STORY_START_POS()
						PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION, set location: ", vPlayerCoords)
					ENDIF
					#ENDIF
					
				ENDIF
			ENDIF
			
			PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, broadcasting host coords as: ", vPlayerCoords)
			
			sSelection.iSelection[ciMISSION_HOST_OPTION_START_POS_X]			= ROUND(vPlayerCoords.x)
			sSelection.iSelection[ciMISSION_HOST_OPTION_START_POS_Y]			= ROUND(vPlayerCoords.y)
			sSelection.iSelection[ciMISSION_HOST_OPTION_START_POS_Z]			= ROUND(vPlayerCoords.z)
			sSelection.iSelection[ciMISSION_HOST_OPTION_ADHOC_CORONA]			= 0
			sSelection.iSelection[ciMISSION_HOST_OPTION_HOST_IN_PROPERTY_DATA]	= 0
			
			IF TRANSITION_SESSIONS_LAUNCHING_FIXER_STORY_MISSION_FROM_APP()
				SET_BIT(sSelection.iSelection[ciMISSION_HOST_OPTION_HOST_IN_PROPERTY_DATA], ciCORONA_CONTACT_MISSION_IN_APARTMENT)				
				PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, Player is TRANSITION_SESSIONS_LAUNCHING_FIXER_STORY_MISSION_FROM_APP")		
				IF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_NOT_IN_HQ()
					vPlayerCoords = GET_FIXER_STORY_START_POS_NOT_IN_HQ()
					sSelection.iSelection[ciMISSION_HOST_OPTION_START_POS_X]		= ROUND(g_FMMC_STRUCT.vStartPos.x)
					sSelection.iSelection[ciMISSION_HOST_OPTION_START_POS_Y]		= ROUND(g_FMMC_STRUCT.vStartPos.y)
					sSelection.iSelection[ciMISSION_HOST_OPTION_START_POS_Z]		= ROUND(g_FMMC_STRUCT.vStartPos.z)
				ENDIF
			ELIF WAS_TRANSITION_SESSIONS_LAUNCHED_FROM_ON_YACHT()
				PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, Player is in the WAS_TRANSITION_SESSIONS_LAUNCHED_FROM_ON_YACHT")
				sSelection.iSelection[ciMISSION_HOST_OPTION_START_POS_X]		= ROUND(g_FMMC_STRUCT.vStartPos.x)
				sSelection.iSelection[ciMISSION_HOST_OPTION_START_POS_Y]		= ROUND(g_FMMC_STRUCT.vStartPos.y)
				sSelection.iSelection[ciMISSION_HOST_OPTION_START_POS_Z]		= ROUND(g_FMMC_STRUCT.vStartPos.z)
			ELIF g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt > 0
				PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - MISSION, Player is in the apartment")
				
				// Make everyone aware we are in an apartment and can use the specific location from Conor.
				SET_BIT(sSelection.iSelection[ciMISSION_HOST_OPTION_HOST_IN_PROPERTY_DATA], ciCORONA_CONTACT_MISSION_IN_APARTMENT)	
				sSelection.iSelection[ciMISSION_HOST_OPTION_ADHOC_CORONA] = 1
								
			ENDIF
			
			g_TransitionSessionNonResetVars.PlayerMissionStartLocation = vPlayerCoords
			
			IF IS_ARENA_WARS_JOB(TRUE)
				sSelection.iSelection[ciMISSION_HOST_OPTION_WEATHER] = SELECT_RANDOM_ARENA_LIGHTING()
				PRINTLN("[CORONA][CORONA_ARENA_LIGHTING] SETUP_CORONA_MENU_OPTIONS_DEFAULT - Mission, sSelection.iSelection[ciMISSION_HOST_OPTION_WEATHER] = ", sSelection.iSelection[ciMISSION_HOST_OPTION_WEATHER], "(", GET_ARENA_LIGHTING_STRING(sSelection.iSelection[ciMISSION_HOST_OPTION_WEATHER]),")")
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_MG_TENNIS
			
			sSelection.iSelection[ciTENNIS_HOST_OPTION_NUM_OF_SETS]		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_MG_TENNIS, ciTENNIS_HOST_OPTION_NUM_OF_SETS)

		BREAK
		
		CASE FMMC_TYPE_MG_GOLF
			
			sSelection.iSelection[ciGOLF_HOST_OPTION_WEATHER]			= 0
			sSelection.iSelection[ciGOLF_HOST_OPTION_START_HOLE]		= 0
			sSelection.iSelection[ciGOLF_HOST_OPTION_NUM_OF_HOLES]		= 8
			
		BREAK
		
		CASE FMMC_TYPE_MG_DARTS
			
			sSelection.iSelection[ciDARTS_HOST_OPTION_NUM_OF_LEGS]		= 0
			sSelection.iSelection[ciDARTS_HOST_OPTION_NUM_OF_SETS]		= 0
			
		BREAK
		
		CASE FMMC_TYPE_MG_SHOOTING_RANGE
			
			sSelection.iSelection[ciSHOOTING_HOST_OPTION_MATCH] = 0

		BREAK
		
		CASE FMMC_TYPE_SURVIVAL
			sSelection.iSelection[ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES] 	= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_SURVIVAL, ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES)
			sSelection.iSelection[ciSURVIVAL_HOST_OPTION_TIME_OF_DAY] 		= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_SURVIVAL, ciSURVIVAL_HOST_OPTION_TIME_OF_DAY)
			sSelection.iSelection[ciSURVIVAL_HOST_OPTION_WEATHER] 			= GET_CORONA_MENU_ITEM_DEFAULT(FMMC_TYPE_SURVIVAL, ciSURVIVAL_HOST_OPTION_WEATHER)
		BREAK
				
	ENDSWITCH
	
	
	#IF IS_DEBUG_BUILD
	INT i
	FOR i = 0 TO (FMMC_MAX_MENU_ITEMS-1) STEP 1
		PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - Menu Option:   ", i)
		PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT - Menu Value:    ", sSelection.iSelection[i])
		PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_DEFAULT ------------------------")
	ENDFOR
	#ENDIF
	
	
ENDPROC

/// PURPOSE:
///    We are yet to have initialise our outfit options. Do this now for a VS mission rounds Job
PROC SET_VERSUS_MISSION_OUTFIT_VALUES_FOR_JIP()

	IF IS_CORONA_VERSUS_OUTFIT_PLAYER_OWNED()
		SET_LOCAL_PLAYER_OWNED_DEFAULT_OUTFIT(-1)
		SET_LOCAL_DEFAULT_OUTFIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen, ENUM_TO_INT(OUTFIT_MP_FREEMODE))
		SET_LOCAL_DEFAULT_MASK(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen, ENUM_TO_INT(OUTFIT_MASK_FREEMODE))

		PRINTLN("[TS] SET_VERSUS_MISSION_OUTFIT_VALUES_FOR_JIP - IS_CORONA_VERSUS_OUTFIT_PLAYER_OWNED(). Set player outfit to OUTFIT_MP_FREEMODE")
	ELSE
		
		INT i
		INT iIndex = NATIVE_TO_INT(PLAYER_ID())
		VERSUS_OUTFIT_STYLE theStyle = INT_TO_ENUM(VERSUS_OUTFIT_STYLE, GET_CORONA_VERSUS_OUTFIT_STYLE())
		
		PRINTLN("[TS] SET_VERSUS_MISSION_OUTFIT_VALUES_FOR_JIP - Set up player VS outfit following a JIP, iIndex = ", iIndex, ", theStyle = ", ENUM_TO_INT(theStyle))
		
		INT iOutfitIndex
		REPEAT FMMC_MAX_TEAMS i
			
			iOutfitIndex = (iIndex % GET_MP_VERSUS_MAX_OUTFITS_FOR_STYLE(GET_MP_VERSUS_OUTFIT(GET_CORONA_VERSUS_OUTFIT_SETUP(), theStyle, i, 0)))
			SET_LOCAL_DEFAULT_OUTFIT(i, ENUM_TO_INT(GET_MP_VERSUS_OUTFIT(GET_CORONA_VERSUS_OUTFIT_SETUP(), theStyle, i, iOutfitIndex)))
					
		ENDREPEAT
		
		SAVE_PLAYER_CHOSEN_OUTFIT_FOR_ALL_TEAMS()
	ENDIF

ENDPROC

PROC SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP(INT iMissionType, FMMC_SELECTED_ITEMS &sSelection)
	
	PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP - setting menu items to backed up values for mission type: ", iMissionType)
	PRINTLN("[CORONA] SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP - Setting Value: **********************************************")
	
	SET_g_sDM_SB_CoronaOptions_TO_DEFAULT()
	SET_g_sRC_SB_CoronaOptions_TO_DEFAULT()
	
	INT iLoop
	SWITCH iMissionType
		CASE FMMC_TYPE_RACE
			FOR iLoop = ciRC_HOST_OPTION_TYPE TO (ciRC_MAX_OPTIONS-1)
				sSelection.iSelection[iLoop] = g_sCoronaOptionsBackUp.iSelection[iLoop]
			ENDFOR
		BREAK
		
		CASE FMMC_TYPE_BASE_JUMP
			FOR iLoop = ciPARA_HOST_OPTION_TIME_OF_DAY TO (ciPARA_HOST_OPTION_SHOOTING)
				sSelection.iSelection[iLoop] = g_sCoronaOptionsBackUp.iSelection[iLoop]
			ENDFOR
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH
			FOR iLoop = ciDM_HOST_OPTION_TYPE TO (ciDM_MAX_OPTIONS-1)
				sSelection.iSelection[iLoop] = g_sCoronaOptionsBackUp.iSelection[iLoop]
			ENDFOR
		BREAK
		
		CASE FMMC_TYPE_MISSION
			FOR iLoop = ciMISSION_HOST_OPTION_NUM_TEAMS TO (ciMISSION_MAX_OPTIONS-1)
			
				// We never need to reinitialise our start positions (always done by host each new corona)
				IF iLoop != ciMISSION_HOST_OPTION_START_POS_X
				AND iLoop != ciMISSION_HOST_OPTION_START_POS_Y
				AND iLoop != ciMISSION_HOST_OPTION_START_POS_Z
					sSelection.iSelection[iLoop] = g_sCoronaOptionsBackUp.iSelection[iLoop]
				ENDIF
			ENDFOR
		BREAK
		
		CASE FMMC_TYPE_MG_TENNIS
			sSelection.iSelection[ciTENNIS_HOST_OPTION_NUM_OF_SETS] = g_sCoronaOptionsBackUp.iSelection[ciTENNIS_HOST_OPTION_NUM_OF_SETS]
		BREAK
		
		CASE FMMC_TYPE_MG_GOLF
			FOR iLoop = ciGOLF_HOST_OPTION_START_HOLE TO (ciGOLF_HOST_OPTION_WEATHER)
				sSelection.iSelection[iLoop] = g_sCoronaOptionsBackUp.iSelection[iLoop]
			ENDFOR
		BREAK
		
		CASE FMMC_TYPE_MG_DARTS
			FOR iLoop = ciDARTS_HOST_OPTION_NUM_OF_LEGS TO (ciDARTS_HOST_OPTION_NUM_OF_SETS)
				sSelection.iSelection[iLoop] = g_sCoronaOptionsBackUp.iSelection[iLoop]
			ENDFOR
		BREAK
		
		CASE FMMC_TYPE_MG_SHOOTING_RANGE
			sSelection.iSelection[ciSHOOTING_HOST_OPTION_MATCH] = g_sCoronaOptionsBackUp.iSelection[ciSHOOTING_HOST_OPTION_MATCH]
		BREAK
		
		CASE FMMC_TYPE_SURVIVAL
			FOR iLoop = ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES TO (ciSURVIVAL_HOST_OPTION_WEATHER)
				sSelection.iSelection[iLoop] = g_sCoronaOptionsBackUp.iSelection[iLoop]
			ENDFOR
		BREAK
				
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	INT i
	FOR i = 0 TO (FMMC_MAX_MENU_ITEMS-1) STEP 1
		PRINTLN("[CORONA] 		SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP - Menu Option:   ", i)
		PRINTLN("[CORONA] 		SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP - Menu Value:    ", sSelection.iSelection[i])
		PRINTLN("[CORONA] 		SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP ------------------------")
	ENDFOR
	#ENDIF
	
ENDPROC


/// PURPOSE: Defaults the data for the passed mission type
PROC SETUP_CORONA_HOST_OPTIONS_DEFAULT(INT iMissionType, FMMC_SELECTED_ITEMS &sSelection, BOOL bIsHeistMission, INT iMissionSubType  , BOOL bIsHeistPlanning, BOOL bIsHeistPrePlanningCutscene  )

	PRINTLN("[CORONA] SETUP_CORONA_HOST_OPTIONS_DEFAULT - setting menu items to default for mission type: ", iMissionType, " iMissionSubType: ", iMissionSubType)

	SET_g_sDM_SB_CoronaOptions_TO_DEFAULT()
	SET_g_sRC_SB_CoronaOptions_TO_DEFAULT()
		
	// Default lobby availability
	IF (IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
	OR SHOULD_TRANSITION_SESSIONS_CREATE_WITH_OPEN_MATCHMAKING())	//If the corona was launched as a quick match we need to create the corona as open.
	AND NOT SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		sSelection.bLobbyAvailability = TRUE
		//Clear the flag as we no longer need it after set up
		CLEAR_TRANSITION_SESSIONS_CREATE_WITH_OPEN_MATCHMAKING()
		
		PRINTLN("[CORONA] SETUP_CORONA_HOST_OPTIONS_DEFAULT - Job was quick match. Open matchmaking")
		
	ELIF IS_CORONA_IN_CHALLENGE_MODE()
	OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
	OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
	OR IS_THIS_A_MINI_GAME(iMissionType)
	OR g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY = FM_SESSION_MENU_CHOICE_JOIN_CLOSED_INVITE_ONLY
	OR IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, bIsHeistMission  , bIsHeistPlanning, bIsHeistPrePlanningCutscene  )
	OR SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
	OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
		sSelection.bLobbyAvailability = FALSE
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_HeistQm")
			sSelection.bLobbyAvailability = TRUE
		ENDIF
		#ENDIF
		
		//Set a flag that we are to force the corona to match making if this is a heist corona when auto invite is selected
		IF IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, bIsHeistMission  , bIsHeistPlanning, bIsHeistPrePlanningCutscene  )
		OR SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
			// We should only open up matchmaking if we do not have this option already available
			IF g_sMPTunables.bDisableHeistMatchmakingMenu
				SET_CORONA_BIT(CORONA_FORCE_OPEN_TO_MM_ON_AUTOINVITE)
			ENDIF
		ENDIF
		
		PRINTLN("[CORONA] SETUP_CORONA_HOST_OPTIONS_DEFAULT - Job has to be closed, Close matchmaking")
	ELSE
		sSelection.bLobbyAvailability = (NOT GET_PACKED_STAT_BOOL(PACKED_MP_MATCHMAKING_IS_PRIVATE))
		PRINTLN("[CORONA] SETUP_CORONA_HOST_OPTIONS_DEFAULT - Matchmaking set to stat")
	ENDIF
	
	// Always set our corona to the relevant matchmaking setting
	UPDATE_CORONA_MATCHMAKING_STATUS(iMissionType, sSelection.bLobbyAvailability)
	
	// We are only allowed 10 params per event to send over network
	EVENT_STRUCT_TRANSITION_PARAMETER_CHANGED Event, Event2, Event3, Event4
	
	Event.Details.Type = SCRIPT_EVENT_TRANSITION_PARAMETER_CHANGED
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.Params.hGamer = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	Event2.Details.Type = SCRIPT_EVENT_TRANSITION_PARAMETER_CHANGED
	Event2.Details.FromPlayerIndex = PLAYER_ID()
	Event2.Params.hGamer = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	Event3.Details.Type = SCRIPT_EVENT_TRANSITION_PARAMETER_CHANGED
	Event3.Details.FromPlayerIndex = PLAYER_ID()
	Event3.Params.hGamer = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	Event4.Details.Type = SCRIPT_EVENT_TRANSITION_PARAMETER_CHANGED
	Event4.Details.FromPlayerIndex = PLAYER_ID()
	Event4.Params.hGamer = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	
	INT iSendTo = ALL_PLAYERS()
	INT iCount = 0
	
	INT iLoop
	SWITCH iMissionType
		CASE FMMC_TYPE_RACE
		
			SETUP_CORONA_MENU_OPTIONS_DEFAULT(iMissionType, sSelection, iMissionSubType)
			
			// If we need to use our previous settings (PLAY AGAIN) then do so.
			IF SHOULD_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP(iMissionType, sSelection)
			ENDIF
			
			// Broadcast our params to transition session
			IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
				
				FOR iLoop = ciRC_HOST_OPTION_TYPE TO (ciRC_MAX_OPTIONS-1)
				
					IF iCount < (MAX_NUM_PARAMS)
						Event.Params.nParameterData[iCount].nParameterID		= iLoop
						Event.Params.nParameterData[iCount].nParameterValue 	= sSelection.iSelection[iLoop]
					ELSE
						Event2.Params.nParameterData[(iCount-MAX_NUM_PARAMS)].nParameterID		= iLoop
						Event2.Params.nParameterData[(iCount-MAX_NUM_PARAMS)].nParameterValue 	= sSelection.iSelection[iLoop]
					ENDIF
				
					iCount++
				ENDFOR
				
				Event.Params.nNumberParams = iCount
				Event2.Params.nNumberParams = (iCount - MAX_NUM_PARAMS)
				
				IF NOT (iSendTo = 0)
					SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
					
					IF iCount >= MAX_NUM_PARAMS
						SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event2, SIZE_OF(Event2), iSendTo)
					ENDIF
				ENDIF
								
			ELSE
				FOR iLoop = ciRC_HOST_OPTION_TYPE TO (ciRC_MAX_OPTIONS-1)
					SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(iLoop, sSelection.iSelection[iLoop])
				ENDFOR
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_BASE_JUMP
			
			SETUP_CORONA_MENU_OPTIONS_DEFAULT(iMissionType, sSelection, iMissionSubType)
			
			// If we need to use our previous settings (PLAY AGAIN) then do so.
			IF SHOULD_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP(iMissionType, sSelection)
			ENDIF
			IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
				FOR iLoop = ciPARA_HOST_OPTION_TIME_OF_DAY TO (ciPARA_HOST_OPTION_SHOOTING)
					Event.Params.nParameterData[iCount].nParameterID		= iLoop
					Event.Params.nParameterData[iCount].nParameterValue 	= sSelection.iSelection[iLoop]
					iCount++
				ENDFOR
				Event.Params.nNumberParams = iCount		
				IF NOT (iSendTo = 0)
					SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
				ENDIF	
			ELSE
				FOR iLoop = ciPARA_HOST_OPTION_TIME_OF_DAY TO (ciPARA_HOST_OPTION_SHOOTING)
					SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(iLoop, sSelection.iSelection[iLoop])
				ENDFOR
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH
		
			SETUP_CORONA_MENU_OPTIONS_DEFAULT(iMissionType, sSelection, iMissionSubType)
			
			// If we need to use our previous settings (PLAY AGAIN) then do so.
			IF SHOULD_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP(iMissionType, sSelection)
			ENDIF
			
			IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
				FOR iLoop = ciDM_HOST_OPTION_TYPE TO (ciDM_MAX_OPTIONS-1)
					IF iCount < (MAX_NUM_PARAMS)
						Event.Params.nParameterData[iCount].nParameterID		= iLoop
						Event.Params.nParameterData[iCount].nParameterValue 	= sSelection.iSelection[iLoop]
					ELSE
						Event2.Params.nParameterData[(iCount-MAX_NUM_PARAMS)].nParameterID		= iLoop
						Event2.Params.nParameterData[(iCount-MAX_NUM_PARAMS)].nParameterValue 	= sSelection.iSelection[iLoop]
					ENDIF
					iCount++
				ENDFOR
				
				Event.Params.nNumberParams = iCount		
				Event2.Params.nNumberParams = (iCount - MAX_NUM_PARAMS)
				
				IF NOT (iSendTo = 0)
					SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
					
					IF iCount >= MAX_NUM_PARAMS
						SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event2, SIZE_OF(Event2), iSendTo)
					ENDIF
				ENDIF	
			ELSE
				FOR iLoop = ciDM_HOST_OPTION_TYPE TO (ciDM_MAX_OPTIONS-1)
					SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(iLoop, sSelection.iSelection[iLoop])
				ENDFOR
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_MISSION
		
			SETUP_CORONA_MENU_OPTIONS_DEFAULT(iMissionType, sSelection, iMissionSubType)
			
			// If we need to use our previous settings (PLAY AGAIN) then do so.
			IF SHOULD_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP(iMissionType, sSelection)
			ENDIF
	
			IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
				FOR iLoop = ciMISSION_HOST_OPTION_NUM_TEAMS TO (ciMISSION_MAX_OPTIONS-1)
					IF iCount < (MAX_NUM_PARAMS)
						Event.Params.nParameterData[iCount].nParameterID		= iLoop
						Event.Params.nParameterData[iCount].nParameterValue 	= sSelection.iSelection[iLoop]
					ELIF iCount < (MAX_NUM_PARAMS*2)
						Event2.Params.nParameterData[(iCount-MAX_NUM_PARAMS)].nParameterID		= iLoop
						Event2.Params.nParameterData[(iCount-MAX_NUM_PARAMS)].nParameterValue 	= sSelection.iSelection[iLoop]
					ELIF iCount < (MAX_NUM_PARAMS*3)
						Event3.Params.nParameterData[(iCount-MAX_NUM_PARAMS*2)].nParameterID		= iLoop
						Event3.Params.nParameterData[(iCount-MAX_NUM_PARAMS*2)].nParameterValue 	= sSelection.iSelection[iLoop]
					ELSE
						Event4.Params.nParameterData[(iCount-(MAX_NUM_PARAMS*3))].nParameterID		= iLoop
						Event4.Params.nParameterData[(iCount-(MAX_NUM_PARAMS*3))].nParameterValue 	= sSelection.iSelection[iLoop]
					ENDIF
					iCount++
				ENDFOR
				
				Event.Params.nNumberParams = iCount		
				Event2.Params.nNumberParams = (iCount - MAX_NUM_PARAMS)
				Event3.Params.nNumberParams = (iCount - (MAX_NUM_PARAMS*2))
				Event4.Params.nNumberParams = (iCount - (MAX_NUM_PARAMS*3))
				
				IF NOT (iSendTo = 0)
					SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
					
					IF iCount >= MAX_NUM_PARAMS
						SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event2, SIZE_OF(Event2), iSendTo)
					ENDIF
					
					IF iCount >= (MAX_NUM_PARAMS*2)
						SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event3, SIZE_OF(Event3), iSendTo)
					ENDIF
					
					IF iCount >= (MAX_NUM_PARAMS*3)
						SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event4, SIZE_OF(Event4), iSendTo)
					ENDIF
				ENDIF	
			ELSE
				FOR iLoop = ciMISSION_HOST_OPTION_NUM_TEAMS TO (ciMISSION_MAX_OPTIONS-1)
					SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(iLoop, sSelection.iSelection[iLoop])
				ENDFOR
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_MG_TENNIS
			
			SETUP_CORONA_MENU_OPTIONS_DEFAULT(iMissionType, sSelection, iMissionSubType)
			
			// If we need to use our previous settings (PLAY AGAIN) then do so.
			IF SHOULD_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP(iMissionType, sSelection)
			ENDIF
			
			SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTENNIS_HOST_OPTION_NUM_OF_SETS, sSelection.iSelection[ciTENNIS_HOST_OPTION_NUM_OF_SETS])
		BREAK
		
		CASE FMMC_TYPE_MG_GOLF
			
			SETUP_CORONA_MENU_OPTIONS_DEFAULT(iMissionType, sSelection, iMissionSubType)
			
			// If we need to use our previous settings (PLAY AGAIN) then do so.
			IF SHOULD_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP(iMissionType, sSelection)
			ENDIF
			
			IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
				FOR iLoop = ciGOLF_HOST_OPTION_START_HOLE TO (ciGOLF_HOST_OPTION_WEATHER)
					Event.Params.nParameterData[iCount].nParameterID		= iLoop
					Event.Params.nParameterData[iCount].nParameterValue 	= sSelection.iSelection[iLoop]
					iCount++
				ENDFOR
				Event.Params.nNumberParams = iCount		
				IF NOT (iSendTo = 0)
					SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
				ENDIF	
			ELSE
				FOR iLoop = ciGOLF_HOST_OPTION_START_HOLE TO (ciGOLF_HOST_OPTION_WEATHER)
					SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(iLoop, sSelection.iSelection[iLoop])
				ENDFOR
			ENDIF	
			
		BREAK
		
		CASE FMMC_TYPE_MG_DARTS
			
			SETUP_CORONA_MENU_OPTIONS_DEFAULT(iMissionType, sSelection, iMissionSubType)
			
			// If we need to use our previous settings (PLAY AGAIN) then do so.
			IF SHOULD_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP(iMissionType, sSelection)
			ENDIF
			
			IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
				FOR iLoop = ciDARTS_HOST_OPTION_NUM_OF_LEGS TO (ciDARTS_HOST_OPTION_NUM_OF_SETS)
					Event.Params.nParameterData[iCount].nParameterID		= iLoop
					Event.Params.nParameterData[iCount].nParameterValue 	= sSelection.iSelection[iLoop]
					iCount++
				ENDFOR
				Event.Params.nNumberParams = iCount		
				IF NOT (iSendTo = 0)
					SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
				ENDIF	
			ELSE
				FOR iLoop = ciDARTS_HOST_OPTION_NUM_OF_LEGS TO (ciDARTS_HOST_OPTION_NUM_OF_SETS)
					SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(iLoop, sSelection.iSelection[iLoop])
				ENDFOR
			ENDIF
			
		BREAK
		
		CASE FMMC_TYPE_MG_SHOOTING_RANGE
			
			SETUP_CORONA_MENU_OPTIONS_DEFAULT(iMissionType, sSelection, iMissionSubType)
			
			// If we need to use our previous settings (PLAY AGAIN) then do so.
			IF SHOULD_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP(iMissionType, sSelection)
			ENDIF
			
			SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciSHOOTING_HOST_OPTION_MATCH, sSelection.iSelection[ciSHOOTING_HOST_OPTION_MATCH])
		BREAK
		
		CASE FMMC_TYPE_SURVIVAL
			
			SETUP_CORONA_MENU_OPTIONS_DEFAULT(iMissionType, sSelection, iMissionSubType)
			
			// If we need to use our previous settings (PLAY AGAIN) then do so.
			IF SHOULD_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				SETUP_CORONA_MENU_OPTIONS_FROM_BACKUP(iMissionType, sSelection)
			ENDIF
			
			IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
				FOR iLoop = ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES TO (ciSURVIVAL_HOST_OPTION_WEATHER)
					Event.Params.nParameterData[iCount].nParameterID		= iLoop
					Event.Params.nParameterData[iCount].nParameterValue 	= sSelection.iSelection[iLoop]
					iCount++
				ENDFOR
				Event.Params.nNumberParams = iCount		
				IF NOT (iSendTo = 0)
					SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
				ENDIF	
			ELSE
				FOR iLoop = ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES TO (ciSURVIVAL_HOST_OPTION_WEATHER)
					SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(iLoop, sSelection.iSelection[iLoop])
				ENDFOR
			ENDIF	
			
		BREAK
				
	ENDSWITCH

	// Clear our backup as no longer need it
	CLEAR_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
	CLEAR_BACKED_UP_CORONA_MENU_SELECTION()
ENDPROC

FUNC BOOL GET_CORONA_DLC_VEHICLE_FROM_CLOUD_DATA(INT iModelHash)
	
	INT i
	INT iMaxDLCVeh = GET_SELECTION_MAX_DLC_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])

	MODEL_NAMES mnVeh

	REPEAT iMaxDLCVeh i
		
		mnVeh = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS], i)
		
		IF iModelHash = ENUM_TO_INT(mnVeh)
			
			g_iMyRaceModelChoice = i
			PRINTLN("[CORONA] GET_CORONA_DLC_VEHICLE_FROM_CLOUD_DATA - Found default DLC vehicle. g_iMyRaceModelChoice = ", g_iMyRaceModelChoice, " Name: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVeh))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Containing function for PROCESS_SPECIAL_CASE_CORONA_VEHICLES so we can enable vehicles of same type if race is locked to either model names passed
PROC PROCESS_SPECIAL_CASE_CORONA_VEHICLE_OF_TYPE(INT iClass, INT iParentClass, MODEL_NAMES mnModelName1, MODEL_NAMES mnModelName2, INT iTotalCustomVehicles)

	IF iClass = iParentClass
		
		IF NOT CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION(DEFAULT, DEFAULT, iTotalCustomVehicles)
		AND (mnModelName1 = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, g_FMMC_STRUCT.iVehicleModel)
			OR mnModelName2 = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, g_FMMC_STRUCT.iVehicleModel))
			
			INT i
			MODEL_NAMES mnVeh
			INT iMaxVeh = GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)
			
			REPEAT iMaxVeh i
			
				mnVeh = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i)
			
				
				IF mnVeh = mnModelName1
				OR mnVeh = mnModelName2
					
					PRINTLN("[CORONA] PROCESS_SPECIAL_CASE_CORONA_VEHICLE_OF_TYPE - Making race vehicle available (CLEAR_BIT): ", i)
					CLEAR_BIT(g_FMMC_STRUCT.iAvailableVehicleBitSet[iClass], i)
					SET_CORONA_BIT(CORONA_SPECIAL_VEHICLE_HAS_BEEN_ALLOWED)	
				ENDIF
			
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Handle the change of vehicles available if the race is locked to a vehicle that has liver / non-livery version
PROC PROCESS_SPECIAL_CASE_CORONA_VEHICLES(INT iClass, INT iTotalCustomVehicles)

	// Handle the two different situations of the sanchez
	PROCESS_SPECIAL_CASE_CORONA_VEHICLE_OF_TYPE(iClass, FMMC_VEHICLE_CLASS_BIKES, SANCHEZ, SANCHEZ2, iTotalCustomVehicles)
	PROCESS_SPECIAL_CASE_CORONA_VEHICLE_OF_TYPE(iClass, FMMC_VEHICLE_CLASS_OFF_ROAD, SANCHEZ, SANCHEZ2, iTotalCustomVehicles)
	
	IF IS_CORONA_CLASS_OFF_ROAD(g_sTransitionSessionOptions)
		INT i
		INT iNumberOfVeh = GET_SELECTION_MAX_DLC_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)
		
		MODEL_NAMES mnVeh
		
		REPEAT iNumberOfVeh i
		
			mnVeh = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i)
			
			IF mnVeh != DUMMY_MODEL_FOR_SCRIPT
			AND IS_MODEL_VALID(mnVeh)
				
				IF DOES_CREATOR_ALLOW_DLC_VEHICLE(iClass, i)
					IF mnVeh = MONSTER
						PRINTLN("[CORONA] PROCESS_SPECIAL_CASE_CORONA_VEHICLES - Use zoom out camera for vehicle screen")
						SET_CORONA_BIT(CORONA_USE_ZOOMED_OUT_CAMERA_FOR_VEHICLES)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

/// PURPOSE: Returns the INT of the custom vehicle we should default to
FUNC INT GET_PLAYER_SAVED_CORONA_CUSTOM_VEHICLE(CORONA_MENU_DATA &coronaMenuData)

	INT i
	// Get the saved corona vehicle id.
	INT iSavedVehicle = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_USED_CUSTOM_VEHICLE)
	
	// Loop over our custom vehicles for the Race and see if we can default to the last used.
	FOR i = 0 TO (coronaMenuData.iTotalCustomVehicles-1) STEP 1
	
		IF g_sCoronaCustomVehiclesData.customVehicleArray[i] >= 0
		
			// Do we have a matching custom vehicle.
			IF iSavedVehicle = GET_CUSTOM_VEHICLE_UNIQUE_IDENTIFIER(g_sCoronaCustomVehiclesData.customVehicleArray[i])
				
				PRINTLN("[CORONA] GET_PLAYER_SAVED_CORONA_CUSTOM_VEHICLE - Matching custom vehicle with our saved stat. Value: ", iSavedVehicle, ", iIndex = ", i)
				RETURN i
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN 0
ENDFUNC

// This sets the custom vehicle to default for missions
PROC SET_CORONA_CUSTOM_VEHICLE_TO_DEFAULT_FOR_MISSION(CORONA_MENU_DATA &coronaMenuData)

	INT iPersonalVehSlot = -1
	INT iLastUsedVehSlot = -1
	
	INT i
	// Get the saved corona vehicle id.
	INT iSavedVehicle = GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_LAST_CUSTOM_CAR)
	INT iPersonalVehicleSlot = CURRENT_SAVED_VEHICLE_SLOT()
	
	PRINTLN("[CORONA] SET_CORONA_CUSTOM_VEHICLE_TO_DEFAULT_FOR_MISSION - iSavedVehicle = ", iSavedVehicle)
	PRINTLN("[CORONA] SET_CORONA_CUSTOM_VEHICLE_TO_DEFAULT_FOR_MISSION - iPersonalVehicleSlot = ", iPersonalVehicleSlot)
	
	// Loop over our custom vehicles for the Race and see if we can default to the last used.
	FOR i = 0 TO (coronaMenuData.iTotalCustomVehicles-1) STEP 1
	
		IF g_sCoronaCustomVehiclesData.customVehicleArray[i] >= 0
		
			// If our personal vehicle slot is valid
			IF iPersonalVehicleSlot != -1
			
				// Does this match the vehicle returned from Conor and it's available
				IF g_sCoronaCustomVehiclesData.customVehicleArray[i] = iPersonalVehicleSlot
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iPersonalVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iPersonalVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_IMPOUNDED)
					
					iPersonalVehSlot = i
					PRINTLN("[CORONA] SET_CORONA_CUSTOM_VEHICLE_TO_DEFAULT_FOR_MISSION - Matching custom vehicle with our personal vehicle. Value: ", iPersonalVehicleSlot, ", iIndex = ", i)
				ENDIF
			ENDIF
				
			// Do we have a matching custom vehicle.
			IF iSavedVehicle = GET_CUSTOM_VEHICLE_UNIQUE_IDENTIFIER(g_sCoronaCustomVehiclesData.customVehicleArray[i])
				
				PRINTLN("[CORONA] SET_CORONA_CUSTOM_VEHICLE_TO_DEFAULT_FOR_MISSION - Matching custom vehicle with our saved stat. Value: ", iSavedVehicle, ", iIndex = ", i)
				iLastUsedVehSlot = i
			ENDIF
		ENDIF
	ENDFOR
	
	// Depending on which slot is valid, 
	IF iPersonalVehSlot != -1
		coronaMenuData.iVehicleCoronaIndex = iPersonalVehSlot
		PRINTLN("[CORONA] SET_CORONA_CUSTOM_VEHICLE_TO_DEFAULT_FOR_MISSION - Saving our initial custom vehicle to iPersonalVehSlot = ", iPersonalVehSlot)
	ELIF iLastUsedVehSlot != -1
		coronaMenuData.iVehicleCoronaIndex = iLastUsedVehSlot
		PRINTLN("[CORONA] SET_CORONA_CUSTOM_VEHICLE_TO_DEFAULT_FOR_MISSION - Saving our initial custom vehicle to iLastUsedVehSlot = ", iLastUsedVehSlot)
	ELSE
		coronaMenuData.iVehicleCoronaIndex = 0
		PRINTLN("[CORONA] SET_CORONA_CUSTOM_VEHICLE_TO_DEFAULT_FOR_MISSION - Saving our initial custom vehicle to 0")
	ENDIF
	
ENDPROC

/// PURPOSE: Save out our custom vehicle to the stats
PROC SAVE_CORONA_CUSTOM_VEHICLE(CORONA_MENU_DATA &coronaMenuData)
	
	IF coronaMenuData.iVehicleCoronaIndex >= 0
	AND g_sCoronaCustomVehiclesData.customVehicleArray[coronaMenuData.iVehicleCoronaIndex] >= 0
		INT iUniqueID = GET_CUSTOM_VEHICLE_UNIQUE_IDENTIFIER(g_sCoronaCustomVehiclesData.customVehicleArray[coronaMenuData.iVehicleCoronaIndex])
		SET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_USED_CUSTOM_VEHICLE, iUniqueID)
		PRINTLN("[CORONA] SAVE_CORONA_CUSTOM_VEHICLE - Save selected custom vehicle ID: ", iUniqueID)
	ENDIF	
	
ENDPROC

// Sets up players default vehicles in preparation for vehicle selection screen
PROC SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT(CORONA_MENU_DATA &coronaMenuData)
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.bCustomVehicleChosen = FALSE
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.mnVehicleChosen = DUMMY_MODEL_FOR_SCRIPT
	
	// Set our vehicle to specific if forced
	IF DOES_RACE_HAVE_DEFAULT_VEHICLE()
		PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - vehicle is default. Default to: ", g_FMMC_STRUCT.iVehicleModel)
		
		INT iMaxVehicles = GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])
		
		IF (g_FMMC_STRUCT.iVehicleModel >= iMaxVehicles OR g_FMMC_STRUCT.iVehicleModel < 0)
		
			PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - vehicle is default but beyond allowed vehicles for class. Default to 0. Saved: ", g_FMMC_STRUCT.iVehicleModel, 
					" Class: ", g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS], " Max for Class: ", iMaxVehicles)
			
			IF g_FMMC_STRUCT.iVehicleModel = -1
			
				PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - Vehicle model not defaulted (-1), set to 0")
			
				g_FMMC_STRUCT.iVehicleModel = 0
				g_iMyRaceModelChoice = 0
			ELSE	
				
				IF GET_CORONA_DLC_VEHICLE_FROM_CLOUD_DATA(g_FMMC_STRUCT.iVehicleModel)
				
					PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - Vehicle model defaulted to DLC")
				
					SET_PLAYER_SELECTING_DLC_VEHICLE(TRUE)
					g_FMMC_STRUCT.iVehicleModel = g_iMyRaceModelChoice
					
					
					INT iDLC
					// Find the correct index in our array
					REPEAT ciRC_MAX_VEHICLES_IN_CORONA iDLC
						
						IF IS_PLAYER_SELECTING_DLC_VEHICLE()
							IF coronaMenuData.orderedCoronaVehicles[iDLC].iVehicleIndex = g_FMMC_STRUCT.iVehicleModel
							AND IS_CORONA_VEHICLE_DLC(coronaMenuData.orderedCoronaVehicles[iDLC])
								coronaMenuData.iVehicleCoronaIndex = iDLC
								
								PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - Set global to position after grabbing DLC: ", iDLC)
								iDLC = ciRC_MAX_VEHICLES_IN_CORONA
							ENDIF
						ENDIF				
					ENDREPEAT
				ELSE				
					g_iMyRaceModelChoice = coronaMenuData.orderedCoronaVehicles[0].iVehicleIndex
					g_FMMC_STRUCT.iVehicleModel = g_iMyRaceModelChoice
					coronaMenuData.iVehicleCoronaIndex = 0
					
					PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - Vehicle model not known as a DLC vehicle in class chosen. See if we can default to another vehicle, starting with: g_iMyRaceModelChoice = ", g_iMyRaceModelChoice)

					IF IS_CORONA_VEHICLE_DLC(coronaMenuData.orderedCoronaVehicles[0])
						SET_PLAYER_SELECTING_DLC_VEHICLE(TRUE)
						PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - Defaulting to index 0 which is a DLC vehicle")
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			INT i = 0
			
			// If our class does not match that set in the creator we can not use the default vehicle
			IF g_FMMC_STRUCT.iRaceClass != g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS]
				g_FMMC_STRUCT.iVehicleModel = 0
			ENDIF
			
			g_iMyRaceModelChoice = g_FMMC_STRUCT.iVehicleModel
			
			// Find the correct index in our array
			REPEAT ciRC_MAX_VEHICLES_IN_CORONA i
				
				IF coronaMenuData.orderedCoronaVehicles[i].iVehicleIndex = g_FMMC_STRUCT.iVehicleModel
				AND NOT IS_CORONA_VEHICLE_DLC(coronaMenuData.orderedCoronaVehicles[i])
					
					coronaMenuData.iVehicleCoronaIndex = i
					
					PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - Set global to position: ", i)
					i = ciRC_MAX_VEHICLES_IN_CORONA
				ENDIF
				
			ENDREPEAT
		ENDIF
				
		// Broadcast the correct index
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iVehicleChosen = g_FMMC_STRUCT.iVehicleModel
	ELSE
		g_iMyRaceModelChoice = 0
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iVehicleChosen = 0
	ENDIF
	
	PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - coronaMenuData.iTotalCustomVehicles = ", coronaMenuData.iTotalCustomVehicles)
	
	// Check if we should be defaulting to our custom vehicles
	IF coronaMenuData.iTotalCustomVehicles > 0
	AND IS_CUSTOM_VEHICLE_TURNED_ON_FOR_COORNA(g_sTransitionSessionOptions)
	AND (CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION(FALSE, FALSE, coronaMenuData.iTotalCustomVehicles)
	OR CV2_IS_THIS_CORONA_A_PROFESSIONAL_JOB()
	)
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
	AND IS_CORONA_VEHICLE_SELECTION_AVAILABLE()
	
		PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - Player has custom vehicle, default to it")
	
		// Find out custom vehicle we last used
		INT iCustomVehicleIndex = GET_PLAYER_SAVED_CORONA_CUSTOM_VEHICLE(coronaMenuData)
		g_iMyRaceModelChoice = iCustomVehicleIndex
		coronaMenuData.iVehicleCoronaIndex = iCustomVehicleIndex
		
		SET_PLAYER_SELECTING_CUSTOM_VEHICLE(TRUE, TRUE)
		SET_PLAYER_SELECTING_DLC_VEHICLE(FALSE)
	ELSE
		IF IS_OPEN_WHEEL_RACE()
			IF g_iMyOpenWheelVehicleSelection != -1
				g_iMyRaceModelChoice = g_iMyOpenWheelVehicleSelection
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iVehicleChosen = g_iMyOpenWheelVehicleSelection
				PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - Open wheel race, using stock vehicle previously used. Previous: ", g_iMyOpenWheelVehicleSelection)
			ENDIF
		ELSE
			IF g_iMyOpenWheelVehicleSelection != -1
				g_iMyOpenWheelVehicleSelection = -1
				g_iMyOpenWheelColourSelection = -1
				g_iMyOpenWheelLiverySelection = -1
				PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - g_iMyOpenWheelVehicleSelection = ", g_iMyOpenWheelVehicleSelection)
				PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - g_iMyOpenWheelColourSelection = ", g_iMyOpenWheelColourSelection)
				PRINTLN("[CORONA] SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT - g_iMyOpenWheelLiverySelection = ", g_iMyOpenWheelLiverySelection)
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("			- .sClientCoronaData.bCustomVehicleChosen: ", PICK_STRING(IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE), "TRUE", "FALSE"))
	PRINTLN("			- .sClientCoronaData.iVehicleChosen: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iVehicleChosen)
	
ENDPROC

FUNC BOOL IS_VEHICLE_BLOCKED_BY_CREATOR(MODEL_NAMES model, INT iClass)
	IF g_FMMC_STRUCT.iRaceCustomVehSetting = ciRCVS_RESTRICTED
		INT i
		
		IF DOES_JOB_USE_RACE_VEHICLE_SELECTION(IS_RACE())
			INT iTeam = GET_MY_GENERIC_JOB_TEAM()
		
			REPEAT ciMAX_VEHICLES_PER_CREATOR_LIBRARY i
				IF GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass), i) = model
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass)][GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
						RETURN TRUE
					ELSE
						RETURN FALSE
					ENDIF
				ENDIF
			ENDREPEAT
			
			RETURN FALSE
		ENDIF
		
		INT iSelectionMax = GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)
									
		// Checking all the standard (stock) vehicles for a race.
		REPEAT iSelectionMax i
			// If the custom vehicle matches, check if its available
			IF GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i,DEFAULT, TRUE) = model
				IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableVehicleBitSet[iClass], i)
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		// Checking all the DLC vehicles for a race
		iSelectionMax = GET_SELECTION_MAX_DLC_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)
		PRINTLN("[JS] IS_VEHICLE_BLOCKED_BY_CREATOR GET_SELECTION_MAX_DLC_VEHICLE_TYPE = ", iSelectionMax)
		REPEAT iSelectionMax i
			IF GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i) = model
				PRINTLN("[JS] IS_VEHICLE_BLOCKED_BY_CREATOR - Class = ", iClass ," i = ", i)
				IF NOT DOES_CREATOR_ALLOW_DLC_VEHICLE(iClass, i)
					PRINTLN("[JS] IS_VEHICLE_BLOCKED_BY_CREATOR DOES_CREATOR_ALLOW_DLC_VEHICLE NOPE")
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MODEL_VALID_FOR_GENERIC_JOB(MODEL_NAMES eModel, INT iTeam, INT iClass, BOOL bRace = TRUE)
	
	#IF FEATURE_TUNER
	IF IS_THIS_A_TUNER_ROBBERY_FINALE()
	AND NOT IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE)
		IF eModel != TAILGATER2
			RETURN FALSE
		ENDIF
	ENDIF
	#ENDIF
	
//	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciRESTRICT_CUSTOM_VEHICLES)
		INT iVehicle
		REPEAT ciMAX_VEHICLES_PER_CREATOR_LIBRARY iVehicle
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass)][GET_LONG_BITSET_INDEX(iVehicle)], GET_LONG_BITSET_BIT(iVehicle))
				IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE)
					PRINTLN("JT DEBUG - Custom Veh")
					IF GET_ARENA_VEHICLE_VARIATION(eModel) = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass, bRace), iVehicle)
						RETURN TRUE
					ENDIF
				ENDIF
				IF eModel = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass, bRace), iVehicle)
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDREPEAT
		
		RETURN FALSE
//	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_VEHICLE_VALID_FOR_RACE(FMMC_SELECTED_ITEMS &sSelection, MODEL_NAMES mnVehName, INT iClassOverride = -1)

	PRINTLN("[CORONA] IS_VEHICLE_VALID_FOR_RACE - checking if model ", GET_MODEL_NAME_FOR_DEBUG(mnVehName), " is valid for race.")

	INT iClass = sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS]
	
	INT iTeam = GET_MY_GENERIC_JOB_TEAM()
	
	IF iClassOverride != -1
		iClass = iClassOverride
	ENDIF
	
	// TODO: We could look to use a similar command to DOES_VEHICLE_HAVE_WEAPONS
	IF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
		IF IS_THIS_A_GTA_RACE_TEMP(g_sTransitionSessionOptions)
			IF NOT IS_CORONA_VEHICLE_VALID_FOR_GTA_RACE(mnVehName)
				RETURN FALSE
			ENDIF
		ELSE
			IF mnVehName = BUZZARD
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Don't allow premium super diamond car
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		IF mnVehName = SUPERD
			RETURN FALSE
		ENDIF
	ENDIF

	// If bit is set then vehicle is not valid
	IF NOT IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE)
	AND NOT IS_PLAYER_SELECTING_DLC_VEHICLE()
		
		IF DOES_JOB_USE_RACE_VEHICLE_SELECTION(IS_RACE())
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass)][GET_LONG_BITSET_INDEX(g_iMyRaceModelChoice)], GET_LONG_BITSET_BIT(g_iMyRaceModelChoice))
				RETURN FALSE
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableVehicleBitSet[iClass], g_iMyRaceModelChoice)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_SELECTING_DLC_VEHICLE()
	AND NOT DOES_JOB_USE_RACE_VEHICLE_SELECTION(IS_RACE())
		IF NOT IS_DLC_CORONA_VEHICLE_VALID(iClass)
			RETURN FALSE
		ENDIF
	ENDIF

	IF IS_THIS_A_RALLY_RACE_TEMP(sSelection)
	OR IS_THIS_CORONA_A_GTA_TEAM_RACE(sSelection)

		IF NOT DOES_VEHICLE_HAVE_TWO_OR_MORE_SEATS(mnVehName)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_VEHICLE_AVAILABLE_FOR_GAME(mnVehName, FALSE)
		PRINTLN("[CORONA] IS_VEHICLE_VALID_FOR_RACE - Vehicle is not available in the game. IS_VEHICLE_AVAILABLE_FOR_GAME = FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_BLOCKED_BY_CREATOR(mnVehName, iClass)
		PRINTLN("[CORONA] IS_VEHICLE_VALID_FOR_RACE - Vehicle is blocked by creator - IS_VEHICLE_BLOCKED_BY_CREATOR()")
		RETURN FALSE
	ENDIF
	
	IF CV2_WAS_STUNT_RACE_LAUNCHED_AS_PROFESSIONAL()
		IF CV2_GET_PROFESSIONAL_RACE_VEHICLE_MODEL() != DUMMY_MODEL_FOR_SCRIPT
			IF mnVehName != CV2_GET_PROFESSIONAL_RACE_VEHICLE_MODEL()
				PRINTLN("[CORONA] IS_VEHICLE_VALID_FOR_RACE - Vehicle doe not match professional fixed model - CV2_GET_PROFESSIONAL_RACE_VEHICLE_MODEL() = ", GET_MODEL_NAME_FOR_DEBUG(CV2_GET_PROFESSIONAL_RACE_VEHICLE_MODEL()))
				RETURN FALSE
			ENDIF
		ELSE
			PRINTLN("[CORONA] IS_VEHICLE_VALID_FOR_RACE - CV2_GET_PROFESSIONAL_RACE_VEHICLE_MODEL() = DUMMY_MODEL_FOR_SCRIPT")
		ENDIF
	ELSE
		PRINTLN("[CORONA] IS_VEHICLE_VALID_FOR_RACE - CV2_WAS_STUNT_RACE_LAUNCHED_AS_PROFESSIONAL() = FALSE")
	ENDIF
	
	IF IS_VEHICLE_AN_IMPORT_EXPORT_SPECIAL_VEHICLE(mnVehName)
	AND NOT IS_SPECIAL_OR_WEAPONISED_VEHICLE_ALLOWED_FOR_RACE(mnVehName, iClass)
		PRINTLN("[CORONA] IS_VEHICLE_VALID_FOR_RACE - Vehicle is an import export special. IS_VEHICLE_AN_IMPORT_EXPORT_SPECIAL_VEHICLE = TRUE")
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_A_WEAPONISED_VEHICLE(mnVehName)
	AND NOT IS_SPECIAL_OR_WEAPONISED_VEHICLE_ALLOWED_FOR_RACE(mnVehName, iClass)
	AND NOT IS_ARENA_WARS_JOB()
		PRINTLN("[CORONA] IS_VEHICLE_VALID_FOR_RACE - Vehicle is a gunrunning weaponised vehicle. IS_VEHICLE_A_WEAPONISED_VEHICLE = TRUE")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_MODEL_AVAILABLE_IN_RACES(mnVehName)
		PRINTLN("[CORONA] IS_VEHICLE_VALID_FOR_RACE - IS_MODEL_AVAILABLE_IN_RACES - FALSE - ", GET_MODEL_NAME_FOR_DEBUG(mnVehName), " is not available in races")
		RETURN FALSE
	ENDIF
	
	IF DOES_JOB_USE_RACE_VEHICLE_SELECTION()
	AND ( (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION) OR (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH) )
		IF NOT IS_MODEL_VALID_FOR_GENERIC_JOB(mnVehName, iTeam, iClass, FALSE)
			PRINTLN("[CORONA] IS_VEHICLE_VALID_FOR_RACE - DOES_JOB_USE_RACE_VEHICLE_SELECTION() - IS_MODEL_VALID_FOR_MISSION - FALSE - ", GET_MODEL_NAME_FOR_DEBUG(mnVehName), " is not available in this job")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Returns TRUE if the only vehicles available have one seat (means we can't do Rally or Team GTA)
FUNC BOOL ARE_THERE_ONLY_ONE_SEAT_VEHICLES_AVAILABLE(FMMC_SELECTED_ITEMS &sSelection)
	
	INT iClass = sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS]
	INT iSelectionMax = GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)
	INT i
	MODEL_NAMES mnVehicle
	
	IF IS_ARENA_RACE()
		INT iTeam = GET_MY_GENERIC_JOB_TEAM()
	
		REPEAT ciMAX_VEHICLES_PER_CREATOR_LIBRARY i
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass)][GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
				mnVehicle = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass), i)
				
				IF IS_MODEL_VALID(mnVehicle)
					PRINTLN("[CORONA] ARE_THERE_ONLY_ONE_SEAT_VEHICLES_AVAILABLE - Checking ", i, ", ", GET_MODEL_NAME_FOR_DEBUG(mnVehicle))
				
					IF GET_VEHICLE_MODEL_NUMBER_OF_SEATS(mnVehicle) > 1
						PRINTLN("[CORONA] ARE_THERE_ONLY_ONE_SEAT_VEHICLES_AVAILABLE - More than One Seat on Vehicle ", i)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF	
		ENDREPEAT
	
		PRINTLN("[CORONA] ARE_THERE_ONLY_ONE_SEAT_VEHICLES_AVAILABLE - Generic Job - No Vehicles with More than One Seat ")
		RETURN TRUE
	ENDIF
	
	REPEAT iSelectionMax i
		mnVehicle = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i)
		
		IF IS_MODEL_VALID(mnVehicle)
		
			PRINTLN("[CORONA] ARE_THERE_ONLY_ONE_SEAT_VEHICLES_AVAILABLE - Checking ", i, ", ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicle))
		
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableVehicleBitSet[iClass], i)
			AND GET_VEHICLE_MODEL_NUMBER_OF_SEATS(mnVehicle) > 1
				PRINTLN("[CORONA] ARE_THERE_ONLY_ONE_SEAT_VEHICLES_AVAILABLE - More than One Seat on Vehicle ", i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	iSelectionMax = GET_SELECTION_MAX_DLC_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)
	REPEAT iSelectionMax i
		mnVehicle = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i)
		
		IF IS_MODEL_VALID(mnVehicle)
		
			PRINTLN("[CORONA] ARE_THERE_ONLY_ONE_SEAT_VEHICLES_AVAILABLE - Checking ", i, ", ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicle))
		
			IF DOES_CREATOR_ALLOW_DLC_VEHICLE(iClass, i)
			AND GET_VEHICLE_MODEL_NUMBER_OF_SEATS(mnVehicle) > 1
				PRINTLN("[CORONA] ARE_THERE_ONLY_ONE_SEAT_VEHICLES_AVAILABLE - More than One Seat on Vehicle ", i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[CORONA] ARE_THERE_ONLY_ONE_SEAT_VEHICLES_AVAILABLE - No Vehicles with More than One Seat ")
	RETURN TRUE
ENDFUNC

/// PURPOSE: Gets the best partner for us (1363107)
PROC GET_CORONA_DEFAULT_PREFERRED_PARTNER()
	
	// Quit if we are SCTV
	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF
		
	INT iBitSet = 0
	INT iCounterFriends
	
	GAMER_HANDLE aGamer
	
	INT i
	FOR i = 1 TO (GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION()-1)
		
		aGamer = GET_TRANSITION_PLAYER_GAMER_HANDLE(i)
		IF IS_GAMER_HANDLE_VALID(aGamer)
			
			IF NETWORK_IS_FRIEND(aGamer)
				
				// Set our bit for a friend
				SET_BIT(iBitSet, i)
				iCounterFriends++
				
			ENDIF
		ENDIF
	ENDFOR
	
	IF iCounterFriends > 0
		PRINTLN("[CORONA] GET_CORONA_DEFAULT_PREFERRED_PARTNER - Friends found, total: ", iCounterFriends)
		
		INT iFriend = GET_RANDOM_INT_IN_RANGE(1, (iCounterFriends+1))
		
		REPEAT GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION() i
			
			IF IS_BIT_SET(iBitSet, i)
				iFriend -= 1
				
				IF iFriend <= 0
					IF NOT IS_CORONA_IN_HEAD_2_HEAD_MODE()
					OR ( IS_CORONA_IN_HEAD_2_HEAD_MODE() AND (GET_TRANSITION_PLAYER_CREW_ID(GET_MY_TRANSITION_SESSION_ARRAY_POS()) = GET_TRANSITION_PLAYER_CREW_ID(i)) )
						PRINTLN("[CORONA] GET_CORONA_DEFAULT_PREFERRED_PARTNER - Selected Friend: ", i)
						
						g_iMyPreferedPartner = i
						g_ghPreferredPartnerHandle = GET_TRANSITION_PLAYER_GAMER_HANDLE(g_iMyPreferedPartner)
						
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT		
	ELSE
		PRINTLN("[CORONA] GET_CORONA_DEFAULT_PREFERRED_PARTNER - No friends set to TEAM_RACE_CHOSE_ANY")
		g_iMyPreferedPartner = TEAM_RACE_CHOSE_ANY
	ENDIF
	
	// Set the bit that this has been set up (prevents us refreshing each time we access lobby screen)
	SET_CORONA_BIT(CORONA_HOST_SELECTED_THEIR_PARTNER)
ENDPROC

PROC SETUP_CORONA_CLIENT_OPTIONS_DEFAULT(INT iMissionType)

	PRINTLN("[CORONA] SETUP_CORONA_CLIENT_OPTIONS_DEFAULT - MissionType: ", iMissionType)
	
	SET_CORONA_MUSIC()
	
	SWITCH iMissionType
		CASE FMMC_TYPE_RACE
		
			// Set our partner preference to valid option (favouring friends)			
			GET_CORONA_DEFAULT_PREFERRED_PARTNER()
			
			SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYER_OPTION_0, g_iMyPreferedPartner) 

			// Set our position to driver
			g_iMyPreferedPosition = 0
			
			// Default our radio (want to do stat here)
			//DONE IN BEGIN_CORONA_RADIO
			//g_iMyChosenRadioStation = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_RADIO)
			//SETUP_CORONA_CLIENT_VEHICLE_SELECTION_DEFAULT()
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH
			//Should I set my team to the one I was on last time because of a restart // 1380652
			IF SHOULD_SET_MY_TEAM_FOR_RESTART()
				PRINTLN("[TS] TEAM BAL - 			- SHOULD_SET_MY_TEAM_FOR_RESTART: TRUE")
				//get my team
				g_FMMC_STRUCT.iMyTeam = GET_MY_TEAM_FOR_RESTART()
				PRINTLN("[TS] TEAM BAL - g_FMMC_STRUCT.iMyTeam   = ", g_FMMC_STRUCT.iMyTeam)
				//move every body once
				g_FMMC_STRUCT.iMyTeam++
				PRINTLN("[TS] TEAM BAL - g_FMMC_STRUCT.iMyTeam++ = ", g_FMMC_STRUCT.iMyTeam)
				//if I'm more than the max used last mission
				IF g_FMMC_STRUCT.iMyTeam >= GET_LAST_NUM_TEAMS_FOR_RESTART() //g_FMMC_STRUCT.iMaxNumberOfTeams
					PRINTLN("[TS] TEAM BAL - g_FMMC_STRUCT.iMyTeam >= g_sFMMCEOM.iTeamChosen", GET_LAST_NUM_TEAMS_FOR_RESTART())
					//go to zero
					g_FMMC_STRUCT.iMyTeam = 0
					PRINTLN("[TS] TEAM BAL - g_FMMC_STRUCT.iMyTeam   = ", g_FMMC_STRUCT.iMyTeam)
				ELSE
					PRINTLN("[TS] TEAM BAL -  GET_LAST_NUM_TEAMS_FOR_RESTART() ", GET_LAST_NUM_TEAMS_FOR_RESTART())
				ENDIF
			//no, I shouldn't
			ELSE
				PRINTLN("[TS] TEAM BAL - 			- SHOULD_SET_MY_TEAM_FOR_RESTART: FALSE")
				g_FMMC_STRUCT.iMyTeam = 0
				PRINTLN("[TS] TEAM BAL - g_FMMC_STRUCT.iMyTeam   = ", g_FMMC_STRUCT.iMyTeam)
			ENDIF
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen			= g_FMMC_STRUCT.iMyTeam
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredPartner	= 0
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole		= 0
			
			PRINTLN("			- .sClientCoronaData.iTeamChosen: ", g_FMMC_STRUCT.iMyTeam)
			PRINTLN("			- .sClientCoronaData.iPreferredPartner: 0")
			PRINTLN("			- .sClientCoronaData.iPreferredRole: 0")
			
			//DONE IN BEGIN_CORONA_RADIO
			//g_iMyChosenRadioStation = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_RADIO)
			
			SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYER_OPTION_0, g_FMMC_STRUCT.iMyTeam) 
		BREAK
		
		CASE FMMC_TYPE_MISSION
			//Should I set my team to the one I was on last time because of a restart
			IF SHOULD_SET_MY_TEAM_FOR_RESTART()
				PRINTLN("			- SHOULD_SET_MY_TEAM_FOR_RESTART: TRUE")
				g_FMMC_STRUCT.iMyTeam = GET_MY_TEAM_FOR_RESTART()
				IF g_FMMC_STRUCT.iMyTeam < 0 
					PRINTLN("			- SHOULD_SET_MY_TEAM_FOR_RESTART: g_FMMC_STRUCT.iMyTeam < 0 ")
					SCRIPT_ASSERT("			- SHOULD_SET_MY_TEAM_FOR_RESTART: g_FMMC_STRUCT.iMyTeam < 0 ")
					g_FMMC_STRUCT.iMyTeam = 0 
				ENDIF
			//no, I shouldn't
			ELSE
				PRINTLN("			- SHOULD_SET_MY_TEAM_FOR_RESTART: FALSE")
				g_FMMC_STRUCT.iMyTeam = 0
			ENDIF
			
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen			= g_FMMC_STRUCT.iMyTeam
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredPartner	= 0
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole		= 0
			
			PRINTLN("			- .sClientCoronaData.iTeamChosen: ", g_FMMC_STRUCT.iMyTeam)
			PRINTLN("			- .sClientCoronaData.iPreferredPartner: 0")
			PRINTLN("			- .sClientCoronaData.iPreferredRole: 0")
			
			INT iOutfitTeam
			
			PRINTLN("		- Save our the default outfits for teams:")
			REPEAT FMMC_MAX_TEAMS iOutfitTeam
				SET_LOCAL_DEFAULT_OUTFIT(iOutfitTeam, g_FMMC_STRUCT.iOutfitDefault[iOutfitTeam])
				PRINTLN("			- iOutfitTeam: ", iOutfitTeam, ", Default Outfit: ", g_FMMC_STRUCT.iOutfitDefault[iOutfitTeam])
			ENDREPEAT
			
			SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYER_OPTION_0, g_FMMC_STRUCT.iMyTeam) 
		BREAK
		
	ENDSWITCH

ENDPROC

FUNC BOOL IS_CORONA_MENU_OPTION_VALID(	CORONA_MENU_DATA 			&coronaMenuData, 
										MISSION_TO_LAUNCH_DETAILS 	&sLaunchMissionDetails,
										FMMC_SELECTED_ITEMS &sSelection, INT iMissionType, INT iOption, BOOL bHost)
	
	BOOL bIsRally = IS_THIS_A_RALLY_RACE_TEMP(sSelection)
	BOOL bIsGtaRace = IS_THIS_A_GTA_RACE_TEMP(sSelection)

	// If you are not host - everything will be displayed
	SWITCH iMissionType
		
		// Don't show partner / role if you are the host
		CASE FMMC_TYPE_RACE
		
			IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
			AND iOption != ciRC_HOST_OPTION_VEHICLE_CLASS
			AND iOption != ciALL_LOCAL_VIEW_JOINED_PLAYERS
			AND iOption != ciRC_HOST_OPTION_TIME_OF_DAY
			AND iOption != ciRC_HOST_OPTION_WEATHER
			AND iOption != ciRC_HOST_OPTION_TRAFFIC
				IF NOT g_sMPTunables.bEnableProfessionalRaceInvites
				OR iOption != ciALL_HOST_OPTION_CONTINUE
					RETURN FALSE
				ENDIF
			ENDIF
		
			SWITCH iOption
					
				CASE ciRC_CLIENT_OPTION_PARTNER
				CASE ciRC_CLIENT_OPTION_ROLE
				
					IF IS_PLAYER_SCTV(PLAYER_ID())
						RETURN FALSE
					ENDIF
								
					IF bHost
						RETURN FALSE							// Host never sees these options
					ELSE			
						IF IS_TARGET_ASSAULT_RACE()				// These are always team races
							RETURN TRUE
						ENDIF
					
						IF (bIsGtaRace AND sSelection.iSelection[ciRC_HOST_OPTION_GTA_TEAM] = 0)		// GTA race is set to FALSE for team
							RETURN FALSE
						ENDIF
					
						IF (NOT bIsRally AND NOT bIsGtaRace)
						OR IS_THIS_RACE_A_FOOT_RACE()
							RETURN FALSE						// Client and standard race, hide partner / role options
						ENDIF
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_NUM_LAPS
					IF IS_THIS_A_POINT_TO_POINT_RACE()
						RETURN FALSE							// No need for laps on P2P race
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Laps)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_VEHICLE_CLASS
					IF NOT CAN_CHANGE_RACE_CLASS_OPTION()
					OR IS_THIS_RACE_A_FOOT_RACE()
						RETURN FALSE							// Can't change vehicle class for this race
					ENDIF
				BREAK
						
				CASE ciRC_HOST_OPTION_TIME_OF_DAY
					IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
						RETURN TRUE
					ENDIF
					
					IF IS_ARENA_WARS_JOB()
						RETURN FALSE
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
						RETURN FALSE
					ENDIF
				BREAK
								
				CASE ciRC_HOST_OPTION_WEATHER
				
					IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
						IF CV2_GET_PRO_RACE_CASH_REWARD(2, FALSE) <= 0
							PRINTLN("prof - not showing 2nd place - reward tunable is 0")
							RETURN FALSE
						ENDIF
						
						RETURN TRUE
					ENDIF
				
					IF IS_STUNT_RACE()
					AND NOT IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
						RETURN FALSE
					ENDIF 
				
					IF IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
					//OR IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
					OR g_sMPTunables.bTurn_snow_on_off
						RETURN FALSE
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Hide_Corona_Option_Weather)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_TRAFFIC
					IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
						IF CV2_GET_PRO_RACE_CASH_REWARD(3, FALSE) <= 0
							PRINTLN("prof - not showing 3rd place - reward tunable is 0")
							RETURN FALSE
						ENDIF
						
						RETURN TRUE
					ENDIF
				
					IF IS_ARENA_WARS_JOB()
						RETURN FALSE
					ENDIF
				
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Traffic)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_COPS
					IF IS_ARENA_WARS_JOB()
						RETURN FALSE
					ENDIF
				
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Wanted)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_MAX_WANTED
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnableCoronaRaceMaxWantedOption)
						RETURN FALSE
					ENDIF
					
					IF (g_FMMC_STRUCT.iRaceCoronaMaxWantedLevel - g_FMMC_STRUCT.iRaceCoronaMinWantedLevel) <= 0
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_CUSTOM_VEHICLE
					IF g_sMPTunables.b4142885_UGC_RACE_CHANGES_ENABLED
					
						// Race type that doesnt allow custom?
						IF IS_BOAT_RACE(g_FMMC_STRUCT.iRaceType)
							RETURN FALSE
						ENDIF
						
						IF IS_THIS_RACE_A_FOOT_RACE()
							RETURN FALSE
						ENDIF
						
						IF (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_CYCLES)
							RETURN FALSE
						ENDIF
						
						IF g_FMMC_STRUCT.iRaceCustomVehSetting = ciRCVS_OFF
							RETURN FALSE
						ENDIF
						
						IF IS_RACE_LOCKED_TO_MODEL(GET_DEFAULT_RACE_MODEL()) 
						AND NOT DOES_MODEL_ALLOW_CUSTOM_VEHICLES(GET_DEFAULT_RACE_MODEL())
							RETURN FALSE
						ENDIF
					
					ELSE
					
						INT iCustomVehCount
						iCustomVehCount = coronaMenuData.iTotalCustomVehicles
						IF NOT IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
							iCustomVehCount = 0
						ENDIF
					
						IF NOT CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION(FALSE, DEFAULT, iCustomVehCount)
						OR IS_BOAT_RACE(g_FMMC_STRUCT.iRaceType)
	//					OR IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
						OR IS_THIS_RACE_A_FOOT_RACE()
						OR (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_CYCLES)
						OR (IS_RACE_LOCKED_TO_MODEL(GET_DEFAULT_RACE_MODEL()) AND DOES_MODEL_ALLOW_CUSTOM_VEHICLES(GET_DEFAULT_RACE_MODEL()))
							RETURN FALSE
						ENDIF
					
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_CATCHUP_CODE
					IF IS_NON_CONTACT_RACE(sSelection.iSelection[ciRC_HOST_OPTION_TYPE])
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_SLIP_STREAM
					IF IS_BOAT_RACE(g_FMMC_STRUCT.iRaceType)
					OR IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
					OR IS_THIS_RACE_A_FOOT_RACE()
					OR IS_NON_CONTACT_RACE(sSelection.iSelection[ciRC_HOST_OPTION_TYPE])
					OR (g_FMMC_STRUCT.iVehicleModel = ENUM_TO_INT(THRUSTER) AND IS_RACE_LOCKED_TO_MODEL(THRUSTER))
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_SPLIT_COMPARE
					IF GET_TOTAL_PLAYERS_IN_CORONA_OR_JOINING(coronaMenuData) > 1
					OR sLaunchMissionDetails.iCreatorId < NUM_NETWORK_PLAYERS
						CLEAR_CORONA_BIT(CORONA_SPLIT_COMPARE_DISPLAYED)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_GTA_TEAM
					IF NOT bIsGtaRace
					OR (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_CYCLES)
					OR (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_INDUSTRIAL)
					OR IS_THIS_RACE_A_FOOT_RACE()
					OR ARE_THERE_ONLY_ONE_SEAT_VEHICLES_AVAILABLE(sSelection)
					OR IS_TARGET_ASSAULT_RACE()
					OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_DisableTeamGTARaceOption)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_SWAP_ROLES
					IF NOT bIsRally
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciRC_HOST_OPTION_DESTROY_LOSER
					// url:bugstar:2544967
					IF g_sMPTunables.blowrider_disable_explode_last_place = TRUE
						RETURN FALSE
					ENDIF
					
					// We can not show the destroy loser option if not a lapped race
					IF bIsRally
					OR IS_THIS_CORONA_A_GTA_TEAM_RACE(sSelection)
					OR IS_TARGET_ASSAULT_RACE()
						RETURN FALSE
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Destroy_Last)
						RETURN FALSE
					ENDIF
				BREAK	
				
				CASE ciRC_HOST_OPTION_AGGREGATE_POS
//					#IF NOT FEATURE_IMPORT_EXPORT
//						RETURN FALSE
//					#ENDIF
//					
//					#IF FEATURE_IMPORT_EXPORT
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciDISABLE_AGGREGATE_POSITION_RACE)
					OR g_sMPTunables.bDisableAggregatePosition
					OR IS_THIS_A_SOLO_SESSION()
					OR IS_TARGET_ASSAULT_RACE()
						RETURN FALSE
					ENDIF
//					#ENDIF

					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Aggregate_Race)
						RETURN FALSE
					ENDIF
					
					// If destroy loser is available and is turned on
					IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, sSelection, iMissionType, ciRC_HOST_OPTION_DESTROY_LOSER, bHost)
					AND sSelection.iSelection[ciRC_HOST_OPTION_DESTROY_LOSER] != ciCORONA_DESTROY_VEHICLE_OFF
						RETURN FALSE
					ENDIF
				
				BREAK
				
				#IF FEATURE_GEN9_EXCLUSIVE
				CASE ciRC_HOST_OPTION_HSW_MODS
					IF IS_TRANSFORM_RACE()
						RETURN TRUE
					ENDIF
				
					IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HSW_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					OR CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HOTRING_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					OR IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
					OR IS_BOAT_RACE(g_FMMC_STRUCT.iRaceType)
					OR IS_JUST_CYCLE_RACE(g_FMMC_STRUCT.iRaceType)
					OR IS_OPEN_WHEEL_RACE()
					OR IS_TARGET_ASSAULT_RACE()
					OR IS_BASEJUMP_RACE(g_FMMC_STRUCT.iRaceType)
					OR IS_ONFOOT_RACE(g_FMMC_STRUCT.iRaceType)
					OR NOT ARE_ANY_HSW_VEHICLES_AVAILABLE_IN_CLASS(sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])
						RETURN FALSE
					ENDIF
				BREAK
				#ENDIF
				
			ENDSWITCH
		BREAK
		
		// Don't show team if you are the host
		CASE FMMC_TYPE_DEATHMATCH
		
			SWITCH iOption
				
				CASE ciDM_HOST_OPTION_NUM_TEAMS
					IF sSelection.iSelection[ciDM_HOST_OPTION_TYPE] != FMMC_DM_TYPE_TEAM
					AND sSelection.iSelection[ciDM_HOST_OPTION_TYPE] != FMMC_DM_TYPE_TEAM_KOTH
						RETURN FALSE																	// Hide teams option if not team deathmatch
					ENDIF
				BREAK
				
				CASE ciDM_HOST_OPTION_TEAM_BAL
					IF IS_THIS_A_KING_OF_THE_HILL_TEAM_CORONA(FMMC_TYPE_DEATHMATCH, sSelection.iSelection[ciDM_HOST_OPTION_TYPE])
						RETURN TRUE
					ENDIF
				
					IF NOT IS_ARENA_WARS_JOB()
						RETURN FALSE
					ELSE
						IF sSelection.iSelection[ciDM_HOST_OPTION_TYPE] != FMMC_DM_TYPE_TEAM
						AND sSelection.iSelection[ciDM_HOST_OPTION_TYPE] != FMMC_DM_TYPE_TEAM_KOTH
							RETURN FALSE
						ENDIF
					ENDIF
				BREAK
				
				CASE ciDM_HOST_OPTION_TIME_LIMIT
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_HideCoronaTimeLimit)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciDM_HOST_OPTION_TARGET_SCORE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Target_Score)
						RETURN FALSE
					ENDIF
					
					IF NOT IS_THIS_LEGACY_DM_CONTENT()
					AND sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM
					AND NOT DO_ALL_DM_TEAMS_HAVE_SAME_TARGET_SCORE()
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciDM_HOST_OPTION_GAMERTAGS
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Online_ID)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciDM_HOST_OPTION_OPP_HEALTH_BAR
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Opponent_Health_Bar)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciDM_HOST_OPTION_TIME_OF_DAY
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciDM_HOST_OPTION_WEATHER
				
					IF IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
					//OR IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
					OR g_sMPTunables.bTurn_snow_on_off
						RETURN FALSE
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Hide_Corona_Option_Weather)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciDM_HOST_OPTION_TRAFFIC
				
					IF IS_ARENA_WARS_JOB()
						RETURN FALSE
					ENDIF
				
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Traffic)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciDM_HOST_OPTION_VEHICLE
					RETURN FALSE
					
				CASE ciDM_HOST_OPTION_WEAPONS
					IF sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_VEHICLE
						RETURN FALSE
					ENDIF
					
					IF IS_ARENA_WARS_JOB()
						RETURN FALSE
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciMISSION_IS_PSEUDORACE)
						RETURN FALSE
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Lock_Weapons)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciDM_HOST_OPTION_ROUNDS
					RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_EnableDeathmatchRoundsForLobby)
				BREAK
				
				CASE ciDM_HOST_OPTION_NUM_LIVES
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_ShowDMLivesOptionInCorona)
						RETURN FALSE
					ENDIF
					
					IF NOT IS_THIS_LEGACY_DM_CONTENT()
					AND sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM
					AND NOT DO_ALL_DM_TEAMS_HAVE_SAME_NUMBER_OF_LIVES_SET()
						RETURN FALSE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_MISSION
		
			SWITCH iOption
			
				CASE ciMISSION_HOST_OPTION_START_POS_X
				CASE ciMISSION_HOST_OPTION_START_POS_Y
				CASE ciMISSION_HOST_OPTION_START_POS_Z
				CASE ciMISSION_HOST_OPTION_ADHOC_CORONA
				CASE ciMISSION_HOST_OPTION_HOST_IN_PROPERTY_DATA
					RETURN FALSE
			
				CASE ciMISSION_HOST_OPTION_NUM_TEAMS
					IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSELECTABLE_TEAM_NUMBERS)
						RETURN FALSE
					ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN FALSE
					ENDIF
				BREAK
			
				CASE ciMISSION_HOST_OPTION_TEAM_BAL
					IF g_FMMC_STRUCT.iMaxNumberOfTeams = 1
					OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
					OR sLaunchMissionDetails.iCoronaMissionSubType = FMMC_MISSION_TYPE_CTF
					OR sLaunchMissionDetails.iCoronaMissionSubType = FMMC_MISSION_TYPE_LTS
					OR IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, sLaunchMissionDetails.bIsHeistMission  , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
					OR IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamRelations, FMMC_TEAM_DISABLE_BALANCING)
					OR IS_LOADED_MISSION_TYPE_FLOW_MISSION()
					OR SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
					//OR sLaunchMissionDetails.bIsHeistMission
						RETURN FALSE																	// Hide team balacning if we only have one team or it is a heist
					ENDIF
				BREAK
			
				CASE ciMISSION_HOST_OPTION_DIFFICULTY
					IF (NOT IS_CORONA_BIT_SET(CORONA_MISSION_IS_COOP) AND g_FMMC_STRUCT.iMaxNumberOfTeams > 1)
					OR sLaunchMissionDetails.iCoronaMissionSubType = FMMC_MISSION_TYPE_LTS
					OR ((IS_THIS_MISSION_A_PRE_PLANNING_CUTSCENE(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsForHeistPrepCutscene) OR IS_THIS_MISSION_A_PRE_PLANNING_MID_STRAND_CUTSCENE(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsForHeistPrepCutscene, sLaunchMissionDetails.bIsForMidStrandCutscene)) 
						AND NOT IS_THIS_MISSION_A_TUTORIAL_CUTSCENE(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsForTutorialCutscene))
					//OR IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, sLaunchMissionDetails.bIsHeistMission #IF FEATURE_HEIST_PLANNING , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene #ENDIF )
					//OR sLaunchMissionDetails.iCoronaMissionSubType = FMMC_MISSION_TYPE_CTF
						RETURN FALSE
					ENDIF
					
					#IF FEATURE_CASINO_HEIST
					IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
						RETURN FALSE
					ENDIF
					#ENDIF
					
					#IF FEATURE_HEIST_ISLAND
					IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_MISSION_FINALE()
						RETURN FALSE
					ENDIF
					#ENDIF
					
					#IF FEATURE_TUNER
					IF IS_THIS_A_TUNER_ROBBERY_FINALE()
						RETURN FALSE
					ENDIF
					#ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_HideCoronaDifficultyOption)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_END_CONDITIONS
					IF g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_CTF
					OR IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, sLaunchMissionDetails.bIsHeistMission  , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
						RETURN FALSE
					ENDIF
				BREAK
			
				CASE ciMISSION_HOST_OPTION_TIME_OF_DAY
					IF IS_THIS_CORONA_A_CONTACT_MISSION(sLaunchMissionDetails)
					OR IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
					//OR IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
					OR IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, sLaunchMissionDetails.bIsHeistMission  , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
					OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
					OR IS_LOADED_MISSION_TYPE_FLOW_MISSION()
					OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciDISABLE_CORONA_TIME_SELECTION)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_WEATHER
					IF IS_THIS_CORONA_A_CONTACT_MISSION(sLaunchMissionDetails)
					OR IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
					//OR IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
					OR IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, sLaunchMissionDetails.bIsHeistMission  , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
					OR g_sMPTunables.bTurn_snow_on_off
					OR IS_LOADED_MISSION_TYPE_FLOW_MISSION()
					OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciDISABLE_CORONA_WEATHER_SELECTION)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_WEAPONS
					IF NOT IS_MISSION_VALID_FOR_FORCED_WEAPON()
					OR IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, sLaunchMissionDetails.bIsHeistMission  , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
						RETURN FALSE
					ENDIF
				BREAK
			
				CASE ciMISSION_HOST_OPTION_ROUNDS
					
					IF NOT IS_MISSION_VALID_FOR_ROUND(sLaunchMissionDetails.iCoronaMissionSubType)
						RETURN FALSE
					ENDIF
					
				BREAK
				
				CASE ciMISSION_HOST_OPTION_CLOTHING_SETUP
					
					// If we only have one outfit, do not show this option. Forced.
					IF IS_CORONA_BIT_SET(CORONA_ONE_OUTFIT_ACTIVE_FOR_MISSION)
						RETURN FALSE
					ENDIF
					
					IF IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, sLaunchMissionDetails.bIsHeistMission  , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
						IF IS_THIS_MISSION_A_PRE_PLANNING_CUTSCENE(iMissionType, sLaunchMissionDetails.bIsForHeistPrepCutscene)
						OR IS_THIS_MISSION_A_PRE_PLANNING_MID_STRAND_CUTSCENE(iMissionType, sLaunchMissionDetails.bIsForHeistPrepCutscene, sLaunchMissionDetails.bIsForMidStrandCutscene)
							RETURN FALSE
						ENDIF
						
						INT i
						BOOL bPlayerOwnedOption
						REPEAT g_FMMC_STRUCT.iNumberOfTeams i
							
							// If there is a team which allows player owned, we are safe to show this option in the corona
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamBitset, ciBS_DISABLE_PLAYER_OWNED_OUTFIT)
								bPlayerOwnedOption = TRUE
							ENDIF
						ENDREPEAT						
						
						// If all teams are locked for player owned then we shouldn't show this option
						IF NOT bPlayerOwnedOption
							RETURN FALSE
						ENDIF						
					ELSE
						// 2520639 - We need to offer the player owned option to players of a lowrider flow mission
						IF IS_LOADED_MISSION_TYPE_FLOW_MISSION()
							RETURN TRUE
						ENDIF			
					
						IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
							RETURN TRUE
						ENDIF
					
						IF sLaunchMissionDetails.iCoronaMissionSubType != FMMC_MISSION_TYPE_VERSUS
						AND sLaunchMissionDetails.iCoronaMissionSubType != FMMC_MISSION_TYPE_COOP
						OR IS_CORONA_VERSUS_OUTFIT_ENTOURAGE()	
						OR IS_CORONA_VERSUS_OUTFIT_HIDDEN()
							RETURN FALSE
						ENDIF
					ENDIF
					
					
					// Never valid for non heist builds
				BREAK
				
				CASE ciMISSION_HOST_OPTION_OUTFIT_CUSTOMISATION
					
					// If the setting is not recommended then we have 
					IF NOT IS_CORONA_HEIST_OUTFIT_RECOMMENDED()
						RETURN FALSE
					ENDIF
					
					IF NOT IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, sLaunchMissionDetails.bIsHeistMission  , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
						RETURN FALSE
					ENDIF
					
					IF IS_THIS_MISSION_A_PRE_PLANNING_CUTSCENE(iMissionType, sLaunchMissionDetails.bIsForHeistPrepCutscene)
					OR IS_THIS_MISSION_A_PRE_PLANNING_MID_STRAND_CUTSCENE(iMissionType, sLaunchMissionDetails.bIsForHeistPrepCutscene, sLaunchMissionDetails.bIsForMidStrandCutscene)
						RETURN FALSE
					ENDIF
					
					// Never valid for non heist builds
				BREAK
				
				CASE ciMISSION_HOST_OPTION_DISABLE_BLIPS
				
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciALLOW_BLIP_DISABLING)
						RETURN FALSE
					ENDIF
					
					// Never valid for non heist builds
				BREAK
				
				CASE ciMISSION_HOST_OPTION_FIXED_CAMERA
					IF ((IS_THIS_MISSION_A_PRE_PLANNING_CUTSCENE(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsForHeistPrepCutscene) OR IS_THIS_MISSION_A_PRE_PLANNING_MID_STRAND_CUTSCENE(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsForHeistPrepCutscene, sLaunchMissionDetails.bIsForMidStrandCutscene)) 
						AND NOT IS_THIS_MISSION_A_TUTORIAL_CUTSCENE(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsForTutorialCutscene))
						RETURN FALSE
					ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_SUDDEN_DEATH
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN FALSE
					ENDIF
				
					RETURN (IS_CORONA_MISSION_SUDDEN_DEATH_AVAILABLE() OR IS_CORONA_MISSION_PICKUP_OPTION_AVAILABLE())
				BREAK					
					
				CASE ciMISSION_HOST_OPTION_VEHICLE_CLASS
				
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciHOST_VEHICLE_CLASS_SELECTION)
						RETURN TRUE
					ENDIF
				
					RETURN FALSE
				BREAK
			
				CASE ciMISSION_HOST_OPTION_USE_BAR
					
					IF (NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_ENABLED))
					OR (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration = -1)
					OR (NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_CORONO_OPTION))
						RETURN FALSE
					ENDIF
					
				BREAK
				
				CASE ciMISSION_HOST_OPTION_ROUND_DURATION
					
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_ROUND_LENGTH)
							RETURN FALSE						
						ENDIF
					
				BREAK
				
				CASE ciMISSION_HOST_OPTION_TARGET_SCORE
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)
						RETURN FALSE
					ENDIF
					
				BREAK				
				
				CASE ciMISSION_HOST_OPTION_TEAM_LIVES
					
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_TEAM_LIVES_SETTING)
							RETURN FALSE
						ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_MISSION_TYPE
					
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE)
							RETURN FALSE
						ENDIF
				
				BREAK
				
				CASE ciMISSION_HOST_OPTION_KILLS_MULTIPLIER
					
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTen, ciMODE_RULE_BASED_SCORE_SCALING) 
							RETURN FALSE
						ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_HIGHEST_SCORE
					
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciTLAD_HIGHEST_SCORE_TOGGLE)
							RETURN FALSE
						ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_WEAPON_LOADOUT
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciOVERRIDE_GUNSMITH_LOADOUT_SETTINGS)
							RETURN FALSE
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sTeamData[0].iTeamBitSet, ciTEAM_BS_SET_STARTING_INVENTORY_CHOSEN_IN_LOBBY)
							RETURN FALSE
						ENDIF
					ENDIF

				BREAK
				
				
				CASE 27 // ciMISSION_HOST_OPTION_NUMBER_OF_LAPS & ciMISSION_HOST_OPTION_EXIT_TYPE
					IF SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
						// yay
					ELSE
						
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciSHOW_LAPS_OPTION)
								RETURN FALSE
							ENDIF
					ENDIF
					
				BREAK
				
				CASE ciMISSION_HOST_OPTION_PILOT
					
						IF NOT SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
						OR (SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
						AND sSelection.iSelection[ciMISSION_HOST_OPTION_EXIT_TYPE] != ciSVM_EXIT_TYPE_HELI)
							RETURN FALSE
						ENDIF
				BREAK
				
				
				
				
				
				CASE ciMISSION_HOST_OPTION_PICKUPS
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION)
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE ciMISSION_HOST_OPTION_VEHICLE_LIST
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_AIRQUOTA_ALT_OPTION)
						RETURN FALSE
					ENDIF
				BREAK
				
				
				CASE ciMISSION_CLIENT_OPTION_SEAT
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciSHOW_CORONA_SEAT_PREFERENCES)
						RETURN FALSE
					ENDIF
				BREAK
			
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_BASE_JUMP
			SWITCH iOption
				
				CASE ciPARA_HOST_OPTION_WEATHER
					IF g_sMPTunables.bTurn_snow_on_off
						RETURN FALSE
					ENDIF
				BREAK
				
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_MG_GOLF
			SWITCH iOption
				
				CASE ciGOLF_HOST_OPTION_WEATHER
					IF g_sMPTunables.bTurn_snow_on_off
						RETURN FALSE
					ENDIF
				BREAK
				
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_SURVIVAL
			SWITCH iOption
				CASE ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES
					IF NOT IS_THIS_MISSION_ROCKSTAR_CREATED()
						RETURN FALSE
					ENDIF
				BREAK
				CASE ciSURVIVAL_HOST_OPTION_TIME_OF_DAY
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
						RETURN FALSE
					ENDIF
				BREAK
				CASE ciSURVIVAL_HOST_OPTION_WEATHER
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Hide_Corona_Option_Weather)
						RETURN FALSE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Allows us to force an update to the partner row
PROC UPDATE_CORONA_OPTION_PREFERRED_PARTNER(CORONA_MENU_DATA &coronaMenuData, INT iColumnID, INT iIndex, BOOL bUpdate = FALSE)
	IF g_iMyPreferedPartner = TEAM_RACE_CHOSE_NONE
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_CLIENT_OPTION_PARTNER, "LOB_CAT_29", "FMMC_RC_NONE", TRUE, TRUE, bUpdate)
	ELIF g_iMyPreferedPartner = TEAM_RACE_CHOSE_ANY
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_CLIENT_OPTION_PARTNER, "LOB_CAT_29", "FMMC_RC_ANY", (GET_TOTAL_PLAYERS_IN_CORONA(coronaMenuData) > 1), TRUE, bUpdate)
	ELSE
		TEXT_LABEL_63 sGamertag
		sGamertag = GET_TRANSITION_PLAYER_GAMERTAG(g_iMyPreferedPartner)
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_CLIENT_OPTION_PARTNER, "LOB_CAT_29", "", TRUE, TRUE, bUpdate, -1, -1, sGamertag)
	ENDIF
ENDPROC

PROC SET_CORONA_CASH_AND_AMMO_SUMMARY(CORONA_MENU_DATA &coronaMenuData, INT &iIndex, BOOL bViewingSelf, BOOL &bBorderShown, BOOL bUpdate = FALSE) //, INT iBettingIndex = -1)

	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF

	TEXT_LABEL_23 tlTitle, tlOption
	INT iOptionInt1, iOptionInt2
		
	BOOL bDisplayBettingCash
	bBorderShown = FALSE
	
	SWITCH coronaMenuData.iCurrentSelection
		CASE ciALL_LOCAL_WEAPON
		CASE ciALL_LOCAL_PURCHASE_AMMO
		CASE ciALL_LOCAL_PURCHASE_ARMOR
		CASE ciALL_LOCAL_WEAPON_FILL_ALL
		CASE ciCORONA_BETTING_WEAPON
		CASE ciCORONA_BETTING_PURCHASE_AMMO
		CASE ciCORONA_BETTING_PURCHASE_ARMOR
		DEFAULT
			bDisplayBettingCash = FALSE
		BREAK
		CASE ciCORONA_BETTING_PLAYER
		CASE ciCORONA_BETTING_BET
			bDisplayBettingCash = TRUE
		BREAK
		
	ENDSWITCH
	
	BOOL bShowCashRow = FALSE
	
	IF (bDisplayBettingCash	
	OR NOT bViewingSelf)
	AND IS_CORONA_BIT_SET(CORONA_BETTING_VISUAL_ENABLED)
	//AND iBettingIndex != -1
	
		bShowCashRow = TRUE
	
		// Display available betting cash
		tlTitle 	= "FM_COR_CASH_BET"
		tlOption	= "MP_BET_CASH"
		iOptionInt1	= GET_CORONA_AVAILABLE_BETTING_CASH() //iBettingIndex) //, TRUE)
	ELSE
		IF CAN_PLAYER_PURCHASE_AMMO_IN_CORONA(g_sTransitionSessionOptions)
		
			bShowCashRow = TRUE
			
			// Display total available cash
			tlTitle 	= "FM_COR_CASH"
			tlOption	= "MP_BET_CASH"
			iOptionInt1	= GET_CORONA_CASH_AVAILABLE()
		ENDIF
	ENDIF
	
	IF bShowCashRow
		// Cash
		SET_FRONTEND_SETTING_DATA_SLOT(	CORONA_COLUMN_ONE, iIndex, coronaMenuData.CORONA_COLUMN_ONE_ID, ciCORONA_BETTING_CASH_OR_AMMO, tlTitle, tlOption, 
										FALSE, FALSE, bUpdate, iOptionInt1, -1, "", -1, FALSE, TRUE, 0, TRUE)
										
		bBorderShown = TRUE
										
		iIndex++
	ENDIF
		
	
	IF bViewingSelf
	AND CAN_PLAYER_PURCHASE_AMMO_IN_CORONA(g_sTransitionSessionOptions)
		WEAPON_TYPE		eWeapon			= GET_CORONA_WEAPON_FROM_INT(coronaMenuData.iWeaponSlot)
		TEXT_LABEL_23 	tlTitleString	= GET_WEAPON_NAME(eWeapon)
		tlTitle 						= "FM_COR_AMMO_DIS"
		
		IF eWeapon = WEAPONTYPE_UNARMED
		OR eWeapon = WEAPONTYPE_PETROLCAN
		OR eWeapon = WEAPONTYPE_DLC_FLASHLIGHT
		OR IS_WEAPON_A_MELEE(eWeapon)
		OR NOT IS_WEAPON_VALID(eWeapon)
		OR eWeapon = WEAPONTYPE_DLC_RAYPISTOL
			EXIT	// Jerry can should not display the ammo count, quit out now
		ENDIF
		
		INT iOverallAmmo 	= GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), GET_CORONA_WEAPON_FROM_INT(coronaMenuData.iWeaponSlot))
		INT iClipSize 		= GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), GET_CORONA_WEAPON_FROM_INT(coronaMenuData.iWeaponSlot))
		iOptionInt2 = CLAMP_INT(iOverallAmmo, 0, iClipSize)

		IF IS_CORONA_WEAPON_THROWN(eWeapon)
		OR iClipSize <= 1
			iOptionInt1 = iOverallAmmo
			iOptionInt2 = -1
			tlOption	= "NUMBER"
			
		ELSE
			iOptionInt1 = IMAX( iOverallAmmo - iOptionInt2, 0)
			tlOption	= "FM_COR_AMMO_2INT"
			
		ENDIF
	
		// Ammo currently in your weapon
		SET_FRONTEND_SETTING_DATA_SLOT(	CORONA_COLUMN_ONE, iIndex, coronaMenuData.CORONA_COLUMN_ONE_ID, 
			ciCORONA_BETTING_CASH_OR_AMMO, tlTitle, tlOption, 
			FALSE, FALSE, bUpdate, iOptionInt1, iOptionInt2, "", -1, FALSE, FALSE, 0, !bBorderShown, FALSE, tlTitleString)
			
		bBorderShown = TRUE
			
		iIndex++
	
	ENDIF

ENDPROC

FUNC BOOL SET_UP_CORONA_AMMO_PURCHASE(CORONA_MENU_DATA &coronaMenuData, INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId)

	// IF we are playing on PC and there is a transaction being processed, do not add option
	IF IS_PC_VERSION()
		IF IS_FULL_AMMO_CASH_TRANSACTION_ACTIVE()
			RETURN FALSE
		ENDIF
	ENDIF
	
	INT iMaxAmmo
//	INT iIndividualCost
//	INT iAmmoSpace
	WEAPON_TYPE eWeapon = GET_CORONA_WEAPON_FROM_INT(coronaMenuData.iWeaponSlot)
	IF GET_MAX_AMMO(PLAYER_PED_ID(), eWeapon, iMaxAmmo)
		IF iMaxAmmo > 0

			GUNCLUB_WEAPON_DATA_STRUCT	sWeaponStruct
			
			// For standard ammo, use the MkI weapon instead
			IF IS_WEAPON_MK2(eWeapon)
			AND IS_CORONA_WEAPON_VALID(eWeapon, g_FMMC_STRUCT.iMissionType, TRUE)
				eWeapon = GET_MK1_WEAPON_TYPE_FROM_MK2_WEAPON(eWeapon)
				PRINTLN("[CORONA] SET_UP_CORONA_AMMO_PURCHASE - Passed weapon was MKII, using its MKI equivalent (", GET_WEAPON_NAME(eWeapon),") for purchasing standard ammo")
			ENDIF
			
			IF GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(eWeapon, sWeaponStruct)

				// Make sure weapon is valid and has an ammo type
				IF sWeaponStruct.eType != ITEM_TYPE_INVALID
				AND sWeaponStruct.eWeapon != WEAPONTYPE_INVALID
				AND sWeaponStruct.eAmmoType != INT_TO_ENUM(AMMO_TYPE, 0)
				AND sWeaponStruct.eAmmoType != AMMOTYPE_DLC_RAYPISTOL
				AND sWeaponStruct.eAmmoType != AMMOTYPE_STUNGUN

					INT iAmmoCount = sWeaponStruct.iDefaultClipSize

					IF IS_CORONA_WEAPON_THROWN(eWeapon)
						PRINTLN("[CORONA] SET_UP_CORONA_AMMO_PURCHASE - Weapon is a thrown type. Give 1 ammo")
						iAmmoCount = 1
						
						IF coronaMenuData.bSellFullAmmo
							sWeaponStruct.iAmmoCost = ROUND(TO_FLOAT(sWeaponStruct.iAmmoCost) * GET_AMMO_MULTIPLIER_FOR_FULL_AMMO(sWeaponStruct.eWeapon, iAmmoCount))
							iAmmoCount				= ROUND(TO_FLOAT(iAmmoCount) * GET_AMMO_MULTIPLIER_FOR_FULL_AMMO(sWeaponStruct.eWeapon, iAmmoCount))
							PRINTLN("[CORONA] SET_UP_CORONA_AMMO_PURCHASE - THROWN - bSellFullAmmo = TRUE   iAmmoCost = ", sWeaponStruct.iAmmoCost, " iAmmoCount ", iAmmoCount)
						ENDIF
						
					ELIF sWeaponStruct.eType = ITEM_TYPE_GUN
						PRINTLN("[CORONA] SET_UP_CORONA_AMMO_PURCHASE - Weapon is ITEM_TYPE_GUN. Give 2 * default clip size")
						iAmmoCount *= 2 // 2 clips for a gun
						
						IF USE_SERVER_TRANSACTIONS()
						AND sWeaponStruct.bAmmoChargedPerUnit
							sWeaponStruct.iAmmoCost *= iAmmoCount
						ENDIF
						
						IF coronaMenuData.bSellFullAmmo
						
							sWeaponStruct.iAmmoCost = ROUND( TO_FLOAT( sWeaponStruct.iAmmoCost ) * GET_AMMO_MULTIPLIER_FOR_FULL_AMMO( sWeaponStruct.eWeapon, iAmmoCount ) )
							iAmmoCount 				= ROUND( TO_FLOAT( iAmmoCount ) * GET_AMMO_MULTIPLIER_FOR_FULL_AMMO( sWeaponStruct.eWeapon, iAmmoCount ) )
							PRINTLN("[CORONA] SET_UP_CORONA_AMMO_PURCHASE - GUN - bSellFullAmmo = TRUE   iAmmoCost = ", sWeaponStruct.iAmmoCost, " iAmmoCount ", iAmmoCount)
						ELSE
						
							// Check if we need to adjust the ammo count and cost due to lack of remaining space (will only affect some weapons)
							GET_AMMO_COUNT_AND_COST_FOR_REMAINING_INVENTORY_SPACE( sWeaponStruct.eWeapon, sWeaponStruct.iAmmoCost, iAmmoCount )
							
						ENDIF
					ENDIF
					
					TEXT_LABEL_15 strAmmoName
					IF GET_GUNCLUB_WEAPON_AMMO_TEXT(sWeaponStruct.eAmmoType, iAmmoCount, strAmmoName, "FM")
					
						// Ammo full - weapon capacity is either full
						// ..... or player doesn't have it unlocked so can only purchase 25% of the ammo for it
						IF HAS_PLAYER_HIT_CORONA_AMMO_LIMIT(eWeapon, TRUE)
							
							IF (sWeaponStruct.eWeapon = WEAPONTYPE_GRENADELAUNCHER 
							OR sWeaponStruct.eWeapon = WEAPONTYPE_RPG
							OR sWeaponStruct.eWeapon = WEAPONTYPE_DLC_FLAREGUN
							OR sWeaponStruct.eWeapon = WEAPONTYPE_DLC_HOMINGLAUNCHER
							OR sWeaponStruct.eWeapon = WEAPONTYPE_DLC_COMPACTLAUNCHER)
								SET_FRONTEND_DATA_SLOT(	iColumn, iMenuIndex, iMenuId, iUniqueId, 
									"HUD_INPUT92N", FALSE, 1, TRUE, FALSE, INT_TO_ENUM(HUD_COLOURS, -1),
									strAmmoName, -1, "FM_COR_AMMO_F")
							ELSE
								SET_FRONTEND_DATA_SLOT(	iColumn, iMenuIndex, iMenuId, iUniqueId, 
									"HUD_INPUT92", FALSE, 1, TRUE, FALSE, INT_TO_ENUM(HUD_COLOURS, -1),
									strAmmoName, iAmmoCount, "FM_COR_AMMO_F")
							ENDIF
							
						// display as normal, even if the player can't afford the ammo
						ELSE
							
							IF coronaMenuData.bSellFullAmmo = TRUE
								SET_FRONTEND_DATA_SLOT(	iColumn, iMenuIndex, iMenuId, iUniqueId, 
								"HUD_INPUT92FC", FALSE, 1, TRUE, FALSE, INT_TO_ENUM(HUD_COLOURS, -1),
								strAmmoName, -1, "MP_BET_LOST", "", sWeaponStruct.iAmmoCost)
							ELSE
								SET_FRONTEND_DATA_SLOT(	iColumn, iMenuIndex, iMenuId, iUniqueId, 
									"HUD_INPUT92", FALSE, 1, TRUE, FALSE, INT_TO_ENUM(HUD_COLOURS, -1),
									strAmmoName, iAmmoCount, "MP_BET_LOST", "", sWeaponStruct.iAmmoCost)
							ENDIF
						ENDIF
					
						RETURN TRUE
					ELSE
						PRINTLN("[CORONA-MENU] - SET_UP_CORONA_AMMO_PURCHASE - could not find ammo type text label")
					ENDIF				
				ELSE
					PRINTLN("[CORONA-MENU] - Invalid weapon/ammo type")
					PRINTLN("				sWeaponStruct.eType: ", 	sWeaponStruct.eType)
					PRINTLN("				sWeaponStruct.eWeapon: ", 	sWeaponStruct.eWeapon)
					PRINTLN("				sWeaponStruct.eAmmoType: ", sWeaponStruct.eAmmoType)
				ENDIF
			ELSE
				PRINTLN("[CORONA-MENU] - SET_UP_CORONA_AMMO_PURCHASE - failed to obtain weapon data")
			ENDIF

		ENDIF
		
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SET_UP_CORONA_ARMOR_PURCHASE(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId)

	INT iArmorLevel = GET_MP_MAX_UNLOCKED_ARMOR_LEVEL()
	IF iArmorLevel >= 0
		TEXT_LABEL_15 tlArmorName = GET_MP_ARMOR_NAME(iArmorLevel)

		GUNCLUB_WEAPON_DATA_STRUCT sWeaponData
		GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(WEAPONTYPE_UNARMED, sWeaponData, iArmorLevel)

		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND GET_PED_ARMOUR(PLAYER_PED_ID()) < FLOOR((TO_FLOAT(GET_PLAYER_MAX_ARMOUR(PLAYER_ID()))/100.0)*sWeaponData.iDefaultClipSize)
		
			INT iArmorCost, iDummy1
			BOOL bDummy		
			
			GET_GUNSHOP_WEAPON_COST(WEAPONTYPE_UNARMED, iArmorCost, iDummy1, bDummy, iArmorLevel)
			
			IF iArmorCost >= 0

				SET_FRONTEND_DATA_SLOT(	iColumn, iMenuIndex, iMenuId, iUniqueId, 
					"FM_COR_ARM", FALSE, 1, TRUE, FALSE, INT_TO_ENUM(HUD_COLOURS, -1), tlArmorName, -1, "MP_BET_LOST", "", iArmorCost)
					
			ELSE
				PRINTLN("[CORONA] - SET_UP_CORONA_ARMOR_PURCHASE - Couldn't find armor price")
			ENDIF
					
		ELSE
		
			SET_FRONTEND_DATA_SLOT(	iColumn, iMenuIndex, iMenuId, iUniqueId, 
				"FM_COR_ARM", FALSE, 1, TRUE, FALSE, INT_TO_ENUM(HUD_COLOURS, -1), tlArmorName, -1, "FM_COR_ARM_F")		
		ENDIF
		
		// Prints to catch a bug
		PRINTLN("[CORONA] - SET_UP_CORONA_ARMOR_PURCHASE - iArmorLevel 							= ", iArmorLevel)
		PRINTLN("[CORONA] - SET_UP_CORONA_ARMOR_PURCHASE - GET_PED_ARMOUR(PLAYER_PED_ID()) 		= ", GET_PED_ARMOUR(PLAYER_PED_ID()))
		PRINTLN("[CORONA] - SET_UP_CORONA_ARMOR_PURCHASE - GET_PLAYER_MAX_ARMOUR(PLAYER_ID()) 	= ", GET_PLAYER_MAX_ARMOUR(PLAYER_ID()))
		PRINTLN("[CORONA] - SET_UP_CORONA_ARMOR_PURCHASE - sWeaponData.iDefaultClipSize 		= ", sWeaponData.iDefaultClipSize)
		PRINTLN("[CORONA] - SET_UP_CORONA_ARMOR_PURCHASE - Calc result 							= ", FLOOR((TO_FLOAT(GET_PLAYER_MAX_ARMOUR(PLAYER_ID()))/100.0)*sWeaponData.iDefaultClipSize))
				
		RETURN TRUE
	ELSE
		PRINTLN("[CORONA] - SET_UP_CORONA_ARMOR_PURCHASE - None of the armor types are unlocked")
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Sets the current corona starting weapon (Saved out and picked up in DM)
PROC SET_CORONA_STARTING_WEAPON(CORONA_MENU_DATA &coronaMenuData)
		
	WEAPON_TYPE aStartingWeapon = GET_CORONA_WEAPON_FROM_INT(coronaMenuData.iWeaponSlot)
	
	PRINTLN("[CORONA] SET_CORONA_STARTING_WEAPON - coronaMenuData.iWeaponSlot = ", coronaMenuData.iWeaponSlot, ", aStartingWeapon = ",  GET_WEAPON_NAME(g_myStartingWeapon))
	
	// Need to store this for all match types - bug 1599456
	IF IS_CORONA_WEAPON_VALID(aStartingWeapon)
		g_myStartingWeapon = aStartingWeapon
		PRINTLN("[CORONA] SET_CORONA_STARTING_WEAPON - g_myStartingWeapon = ", GET_WEAPON_NAME(g_myStartingWeapon))
	ELSE
		g_myStartingWeapon = WEAPONTYPE_UNARMED
		PRINTLN("[CORONA] SET_CORONA_STARTING_WEAPON - NO VALID WEAPON - g_myStartingWeapon = ", GET_WEAPON_NAME(WEAPONTYPE_UNARMED))
	ENDIF
	
	SET_PACKED_STAT_INT(PACKED_MP_ACTIVITY_STARTING_WEAPON, coronaMenuData.iWeaponSlot)
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	OR (IS_A_STRAND_MISSION_BEING_INITIALISED() AND IS_THIS_A_QUICK_RESTART_JOB())
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), g_myStartingWeapon, TRUE)
	ENDIF
ENDPROC

FUNC BOOL DOES_PLAYER_HAVE_VALID_CORONA_WEAPON_OPTIONS()
	
	INT iWeaponSlot = 0
	INT iCounter = 0
	
	WEAPON_TYPE aWeapon
	REPEAT ENUM_TO_INT(NUM_WEAPONSLOTS) iWeaponSlot
		
		aWeapon = GET_CORONA_WEAPON_FROM_INT(iWeaponSlot)
		
		IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType)
			iCounter++
		ENDIF
		
		IF iCounter > 1
			RETURN TRUE
		ENDIF
		
	ENDREPEAT
	
	REPEAT GET_NUM_DLC_WEAPONS() iWeaponSlot
		
		aWeapon = GET_CORONA_WEAPON_FROM_INT(ENUM_TO_INT(NUM_WEAPONSLOTS) + iWeaponSlot)
		
		IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType)
			iCounter++
		ENDIF
		
		IF iCounter > 1
			RETURN TRUE
		ENDIF
		
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: This checks the standard weapons (non dlc) cycling from our current weapon slot
FUNC BOOL CHECK_CORONA_STANDARD_WEAPONS(CORONA_MENU_DATA &coronaMenuData, WEAPON_GROUP &weaponGroup, INT iIncrement)

	// Get number of weapons to loop over based on button press
	INT iEndIndex
	IF iIncrement < 0
		iEndIndex = coronaMenuData.iStandardWeaponSlot
	ELSE
		iEndIndex = (ENUM_TO_INT(NUM_WEAPONSLOTS) - coronaMenuData.iStandardWeaponSlot) - 1
	ENDIF
	
	PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - STANDARD, coronaMenuData.iStandardWeaponSlot = ", coronaMenuData.iStandardWeaponSlot)
	PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - STANDARD, iEndIndex = ", iEndIndex)
	
	INT i = 0
	WEAPON_TYPE aWeapon
	
	// Loop over the weapons we need to checl
	REPEAT iEndIndex i
	
		coronaMenuData.iStandardWeaponSlot += iIncrement
		
		// Ensure they are in range
		IF coronaMenuData.iStandardWeaponSlot >= 0
		AND coronaMenuData.iStandardWeaponSlot < ENUM_TO_INT(NUM_WEAPONSLOTS)
		
			aWeapon = coronaMenuData.coronaStandardWeapons[coronaMenuData.iStandardWeaponSlot]
						
			// Is the weapon valid?
			IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType)
				
				// ...yes, check our weapon group matches
				IF GET_WEAPONTYPE_GROUP(aWeapon) = weaponGroup
					
					PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - STANDARD, next weapon valid = ", GET_WEAPON_NAME(aWeapon))
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF

	ENDREPEAT

	RETURN FALSE
ENDFUNC

/// PURPOSE: This checks the DLC weapons, see above for comments
FUNC BOOL CHECK_CORONA_DLC_WEAPONS(CORONA_MENU_DATA &coronaMenuData, WEAPON_GROUP &weaponGroup, INT iIncrement)

	INT iEndIndex

	IF iIncrement < 0
		iEndIndex = coronaMenuData.iStandardWeaponSlot
	ELSE
		iEndIndex = (GET_NUM_DLC_WEAPONS() - coronaMenuData.iStandardWeaponSlot) - 1
	ENDIF

	PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - DLC, coronaMenuData.iStandardWeaponSlot = ", coronaMenuData.iStandardWeaponSlot)
	PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - DLC, iEndIndex = ", iEndIndex)
	
	INT i
	WEAPON_TYPE aWeapon
		
	REPEAT iEndIndex i
	
		coronaMenuData.iStandardWeaponSlot += iIncrement
		aWeapon = GET_CORONA_WEAPON_FROM_INT(ENUM_TO_INT(NUM_WEAPONSLOTS) + coronaMenuData.iStandardWeaponSlot)
					
		// If valid
		IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType)
			
			// Check the ammo type is the same
			IF GET_WEAPONTYPE_GROUP(aWeapon) = weaponGroup
				
				PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - DLC, next weapon valid = ", GET_WEAPON_NAME(aWeapon))
				
				RETURN TRUE
			ENDIF
		ENDIF

	ENDREPEAT

	RETURN FALSE
ENDFUNC

FUNC BOOL FIND_NEXT_CORONA_WEAPON_IN_GROUP(CORONA_MENU_DATA &coronaMenuData, WEAPON_GROUP &weaponGroup, INT iIncrement)

	// Begin our search for a matching weapon based on state we are in standard or dlc
	SWITCH coronaMenuData.coronaWeaponType
	
		CASE CORONA_WEAPON_STANDARD
		
			PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - 	checking Standard weapons first, ", iIncrement)
		
			// If standard, is there another standard weapon available?
			IF CHECK_CORONA_STANDARD_WEAPONS(coronaMenuData, weaponGroup, iIncrement)
			
				//...yes, return
				coronaMenuData.coronaWeaponType = CORONA_WEAPON_STANDARD
			
				RETURN TRUE
			ELSE
			
				coronaMenuData.coronaWeaponType = CORONA_WEAPON_DLC
				IF iIncrement < 0
					PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - 	failed to find weapon, get next group")
				
					RETURN FALSE
				ELSE
					coronaMenuData.iStandardWeaponSlot = -1
				ENDIF
			
				PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - 	checking DLC weapons second, ", iIncrement)
				IF CHECK_CORONA_DLC_WEAPONS(coronaMenuData, weaponGroup, iIncrement)
					
					coronaMenuData.coronaWeaponType = CORONA_WEAPON_DLC
					
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE CORONA_WEAPON_DLC
		
			PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - 	checking DLC weapons first, ", iIncrement)
		
			IF CHECK_CORONA_DLC_WEAPONS(coronaMenuData, weaponGroup, iIncrement)
			
				coronaMenuData.coronaWeaponType = CORONA_WEAPON_DLC
			
				RETURN TRUE
			ELSE
			
				coronaMenuData.coronaWeaponType = CORONA_WEAPON_STANDARD
				IF iIncrement < 0
					coronaMenuData.iStandardWeaponSlot = ENUM_TO_INT(NUM_WEAPONSLOTS)
				ELSE
					PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - 	failed to find weapon, get next group")
					RETURN FALSE
				ENDIF
			
				PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - 	checking Standard weapons second, ", iIncrement)
				IF CHECK_CORONA_STANDARD_WEAPONS(coronaMenuData, weaponGroup, iIncrement)
					
					coronaMenuData.coronaWeaponType = CORONA_WEAPON_STANDARD
					
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK	
	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Get the next valid weapon in the players inventory
FUNC BOOL GET_CORONA_NEXT_VALID_WEAPON(CORONA_MENU_DATA &coronaMenuData, INT iIncrement)
	
	PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON **************************************************")
	PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - Current weapon 						= ", GET_WEAPON_NAME(g_myStartingWeapon))
	PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - Increment								= ", PICK_STRING((iIncrement < 0), "LEFT", "RIGHT"))
	PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - coronaMenuData.iWeaponSlot 			= ", coronaMenuData.iWeaponSlot)
	PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - coronaMenuData.coronaWeaponType 		= ", PICK_STRING((coronaMenuData.coronaWeaponType = CORONA_WEAPON_STANDARD), "STANDARD", "DLC"))
	PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - coronaMenuData.iStandardWeaponSlot 	= ", coronaMenuData.iStandardWeaponSlot)
	PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - coronaMenuData.coronaWeaponGroup 		= ", ENUM_TO_INT(coronaMenuData.coronaWeaponGroup))

	BOOL bFoundWeapon
	WEAPON_TYPE aWeapon
	
	INT iStartIndex = coronaMenuData.iStandardWeaponSlot
	WEAPON_GROUP eStartingWeaponGroup = coronaMenuData.coronaWeaponGroup
	CORONA_CURRENT_WEAPON_TYPE eStartingWeaponType = coronaMenuData.coronaWeaponType
	
	INT iStartGroup = GET_INT_FROM_PLAYER_PED_WEAPON_GROUP(coronaMenuData.coronaWeaponGroup)
	INT iCurrentGroup = iStartGroup
	
	WHILE (NOT bFoundWeapon)
	
		// Try and find another valid weapon in the current group
		IF FIND_NEXT_CORONA_WEAPON_IN_GROUP(coronaMenuData, coronaMenuData.coronaWeaponGroup, iIncrement)
		
			IF coronaMenuData.coronaWeaponType = CORONA_WEAPON_STANDARD
				
				aWeapon = coronaMenuData.coronaStandardWeapons[coronaMenuData.iStandardWeaponSlot]
				coronaMenuData.coronaWeaponGroup = GET_WEAPONTYPE_GROUP(aWeapon)
														
				// Update our weapon slot
				coronaMenuData.iWeaponSlot = GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(GET_WEAPONTYPE_SLOT(aWeapon))
			ELSE
				
				aWeapon = GET_CORONA_WEAPON_FROM_INT(ENUM_TO_INT(NUM_WEAPONSLOTS) + coronaMenuData.iStandardWeaponSlot)
				coronaMenuData.coronaWeaponGroup = GET_WEAPONTYPE_GROUP(aWeapon)
														
				// Update our weapon slot
				coronaMenuData.iWeaponSlot = ENUM_TO_INT(NUM_WEAPONSLOTS) + coronaMenuData.iStandardWeaponSlot
				
			ENDIF
			
			PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - Successfully found weapon: Slot = ", coronaMenuData.iWeaponSlot,
					", Type = ", PICK_STRING((coronaMenuData.coronaWeaponType = CORONA_WEAPON_STANDARD), "STANDARD", "DLC"),
					", WeaponSlot = ", coronaMenuData.iStandardWeaponSlot)
			
			bFoundWeapon = TRUE
		ELSE
			// No valid weapon, so loop to next group
			PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - No valid weapon, move to next group, from ", iCurrentGroup, " to ", iCurrentGroup + iIncrement)
						
			iCurrentGroup += iIncrement
			
			// Shift past missed out weapon group
			IF iCurrentGroup = 13
				iCurrentGroup += iIncrement
			ENDIF
			
			IF iCurrentGroup < -1
				iCurrentGroup = NUM_PLAYER_PED_WEAPON_GROUPS-1
			ELIF iCurrentGroup >= NUM_PLAYER_PED_WEAPON_GROUPS
				iCurrentGroup = -1
			ENDIF
			
			// Get our new group we are looking for
			coronaMenuData.coronaWeaponGroup = GET_PLAYER_PED_WEAPON_GROUP_FROM_INT(iCurrentGroup)
			
			// Correct our invalid group to take the unarmed into consideration
			IF coronaMenuData.coronaWeaponGroup = WEAPONGROUP_INVALID
				coronaMenuData.coronaWeaponGroup = GET_WEAPONTYPE_GROUP(WEAPONTYPE_UNARMED)
				PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - Move weapon group for unarmed as invalid = ", ENUM_TO_INT(coronaMenuData.coronaWeaponGroup))
			ENDIF
			
			PRINTLN("[CORONA] GET_CORONA_NEXT_VALID_WEAPON - Move to the next weapon group	= ", ENUM_TO_INT(coronaMenuData.coronaWeaponGroup))
			
			IF iIncrement < 0
				coronaMenuData.coronaWeaponType = CORONA_WEAPON_DLC
				coronaMenuData.iStandardWeaponSlot = GET_NUM_DLC_WEAPONS()
			ELSE
				coronaMenuData.coronaWeaponType = CORONA_WEAPON_STANDARD
				coronaMenuData.iStandardWeaponSlot = -1
			ENDIF
			
		ENDIF
		
		// We have come across the same weapon, quit out
		IF iStartIndex = coronaMenuData.iStandardWeaponSlot
		AND eStartingWeaponGroup = coronaMenuData.coronaWeaponGroup
		AND eStartingWeaponType = coronaMenuData.coronaWeaponType
			RETURN FALSE
		ENDIF
	ENDWHILE
	
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	SET_CORONA_STARTING_WEAPON(coronaMenuData)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:    Finds if the player has more than 1 weapon to cycle through.
FUNC BOOL IS_CORONA_WEAPON_LIMITED_TO_CURRENT_WEAPON()

	IF NOT DOES_PLAYER_HAVE_VALID_CORONA_WEAPON_OPTIONS()
		RETURN TRUE	
	ENDIF

	RETURN FALSE
ENDFUNC

// url:bugstar:3326320 - Can we allow these races to have the GTA race category available in the corona please.
FUNC BOOL IS_GTA_OPTION_AVAILABLE_FOR_RACE()
	IF IS_TARGET_ASSAULT_RACE()
		RETURN TRUE
	ENDIF

	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_GTA_RACE_TYPE_FOR_STUNT_AND_SPR)
ENDFUNC

/// PURPOSE: Check if the race type is valid
FUNC BOOL IS_CORONA_RACE_TYPE_SELECTABLE(INT iType, INT iMaxValue, INT iTotalCustomVehicles)
	
	IF iType > iMaxValue
		RETURN FALSE
	ENDIF
	
	SWITCH iType
				
		CASE ciRACE_SUB_TYPE_STANDARD
			IF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
				IF g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_PLANE
					IF g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_GTA
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_TARGET_ASSAULT_RACE()
				RETURN FALSE
			ENDIF
			
			IF IS_ARENA_WARS_JOB()
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciRACE_SUB_TYPE_GTA
			IF IS_TARGET_ASSAULT_RACE()
				RETURN TRUE
			ENDIF
			
			IF IS_ARENA_WARS_JOB()
				RETURN TRUE
			ENDIF
		
			IF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
			
				IF g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_PLANE
					IF g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_TYPE] != ciRACE_SUB_TYPE_GTA
						RETURN FALSE
					ENDIF					
				ENDIF

				IF NOT IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
					iTotalCustomVehicles = 0
				ENDIF
			
				IF NOT CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION(DEFAULT, DEFAULT, iTotalCustomVehicles)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
				RETURN FALSE
			ENDIF
			
			
			// url:bugstar:3326320 - Can we allow these races to have the GTA race category available in the corona please.
			
			IF IS_PLAYER_IN_STUNT_RACE()
			AND IS_CURRENT_MISSION_ROCKSTAR_CREATED()
			AND NOT IS_GTA_OPTION_AVAILABLE_FOR_RACE()
				RETURN FALSE
			ENDIF
			
			// url:bugstar:3873368
			IF IS_TRANSFORM_RACE()
				RETURN FALSE
			ENDIF
			
			IF IS_OPEN_WHEEL_RACE()
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DisableGTARaceOption)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciRACE_SUB_TYPE_RALLY
			
			// Not valid if it is an air race / boat race or only one seat available
			IF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
			OR IS_BOAT_RACE(g_FMMC_STRUCT.iRaceType)
			OR ARE_THERE_ONLY_ONE_SEAT_VEHICLES_AVAILABLE(g_sTransitionSessionOptions)
				RETURN FALSE
			ENDIF
			
			// Not valid if the class is cycles or industrial
			IF (g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_CYCLES)
			OR (g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_INDUSTRIAL)
				RETURN FALSE
			ENDIF
			
			IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
				RETURN FALSE
			ENDIF
			
			IF IS_PLAYER_IN_STUNT_RACE()
			AND IS_CURRENT_MISSION_ROCKSTAR_CREATED()
				RETURN FALSE
			ENDIF
			
			// url:bugstar:3873368
			IF IS_TRANSFORM_RACE()
				RETURN FALSE
			ENDIF
			
			IF IS_TARGET_ASSAULT_RACE()
				RETURN FALSE
			ENDIF
			
			IF IS_ARENA_WARS_JOB()
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisableRallyOption)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciRACE_SUB_TYPE_NON_CONTACT	
			IF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
				IF g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_PLANE
					IF g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_GTA
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ARENA_WARS_JOB()
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_RACE_Hide_NonContact_Option)
				RETURN FALSE
			ENDIF
			
//			#IF FEATURE_STUNT
//			IF IS_PLAYER_IN_STUNT_RACE()
//				RETURN FALSE
//			ENDIF
//			#ENDIF
			
			// Do not allow this to be scrolled to within qualifying playlist (removed because of 1758521)
//			IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
//				RETURN FALSE
//			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_CORONA_ALLOW_TYPE_TO_BE_CHANGED(INT iTotalCustomVehicles)
	
	INT iCount
	INT i
	
	FOR i = 0 TO ciRC_MAX_TYPE_RACE_TYPE STEP 1
		
		IF IS_CORONA_RACE_TYPE_SELECTABLE(i, ciRC_MAX_TYPE_RACE_TYPE, iTotalCustomVehicles)
			iCount++
		ENDIF
		
	ENDFOR
	
	RETURN (iCount > 1)	
ENDFUNC

/// PURPOSE: Returns FALSE is some other option is disabling current selection
FUNC BOOL IS_HOST_ALLOWED_TO_CHANGE_OPTION(INT iMissionType, INT iCurrentSelection, FMMC_SELECTED_ITEMS &sSelection)
	
	IF iCurrentSelection = ciALL_HOST_OPTION_CONTINUE
	OR iCurrentSelection = ciALL_HOST_OPTION_AUTOFILL
	OR iCurrentSelection = ciALL_LOCAL_PURCHASE_AMMO
	OR iCurrentSelection = ciALL_LOCAL_PURCHASE_ARMOR
	OR iCurrentSelection = ciALL_LOCAL_WEAPON_FILL_ALL
	OR iCurrentSelection = ciALL_LOCAL_WEAPON_LOCKED
		RETURN FALSE
	ENDIF
	
	IF iCurrentSelection = ciALL_LOCAL_RADIO_STATION
	AND DOES_RACE_HAVE_A_RADIO_MIX()
		RETURN FALSE
	ENDIF
	
	IF iCurrentSelection = ciALL_CREW_CHALLENGE_CASH
		
		IF IS_CORONA_BIT_SET(CORONA_H2H_LOCK_CASH_OPTION) 
			RETURN FALSE
		ENDIF
	ENDIF

	SWITCH iMissionType
	
		CASE FMMC_TYPE_MISSION
		
			// We are not allowed to change the difficult / cops setting if in challenge mode
			IF iCurrentSelection = ciMISSION_HOST_OPTION_DIFFICULTY
			OR iCurrentSelection = ciMISSION_HOST_OPTION_END_CONDITIONS
			OR iCurrentSelection = ciMISSION_HOST_OPTION_WEAPONS
			OR iCurrentSelection = ciMISSION_HOST_OPTION_FIXED_CAMERA
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
		
			// Only allow us to change the team balancing if we have more than 1 team
			IF iCurrentSelection = ciMISSION_HOST_OPTION_TEAM_BAL
			AND g_FMMC_STRUCT.iMaxNumberOfTeams = 1
				RETURN FALSE
			ENDIF
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_ROUNDS
				/*IF g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_LTS
				AND g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_VERSUS
				AND g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_CTF
					RETURN FALSE
				ENDIF*/
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_WEATHER
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_SUDDEN_DEATH
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_ROUND_DURATION
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_TARGET_SCORE
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_USE_BAR
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_TEAM_LIVES
			OR iCurrentSelection = ciMISSION_HOST_OPTION_MISSION_TYPE
			OR iCurrentSelection = ciMISSION_HOST_OPTION_KILLS_MULTIPLIER
			OR iCurrentSelection = ciMISSION_HOST_OPTION_HIGHEST_SCORE
			OR iCurrentSelection = ciMISSION_HOST_OPTION_WEAPON_LOADOUT
			OR iCurrentSelection = ciMISSION_HOST_OPTION_NUMBER_OF_LAPS
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_EXIT_TYPE
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_PILOT
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_PICKUPS
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_VEHICLE_LIST
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciMISSION_HOST_OPTION_CLOTHING_SETUP
				IF GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_MORGUE
				OR GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_SERVERFARM
				OR GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_STEALOSPREY
				OR GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_PREDATOR
					RETURN FALSE
				ENDIF
			ENDIF
			
		BREAK
	
		CASE FMMC_TYPE_DEATHMATCH
			IF (iCurrentSelection = ciDM_HOST_OPTION_NUM_TEAMS AND (sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_NORMAL))
			OR (iCurrentSelection = ciDM_HOST_OPTION_TEAM_BAL AND (sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_NORMAL))
				RETURN FALSE
			ENDIF
			
			IF (iCurrentSelection = ciDM_HOST_OPTION_TYPE) AND (sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_VEHICLE OR HAS_PLAYER_SENT_AN_INVITE())
				RETURN FALSE
			ENDIF
			
			IF iCurrentSelection = ciDM_HOST_OPTION_TYPE
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED()
					RETURN FALSE
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LockedDeathmatchType)
					RETURN FALSE
				ENDIF
			ENDIF
			
			// All these options are to be locked when in a challenge or tournament
			IF iCurrentSelection = ciDM_HOST_OPTION_TEAM_BAL
			OR iCurrentSelection = ciDM_HOST_OPTION_TIME_LIMIT
			OR iCurrentSelection = ciDM_HOST_OPTION_TARGET_SCORE
			OR iCurrentSelection = ciDM_HOST_OPTION_GAMERTAGS
			OR iCurrentSelection = ciDM_HOST_OPTION_OPP_HEALTH_BAR
			OR iCurrentSelection = ciDM_HOST_OPTION_TIME_OF_DAY
			OR iCurrentSelection = ciDM_HOST_OPTION_WEATHER
			OR iCurrentSelection = ciDM_HOST_OPTION_TRAFFIC
			OR iCurrentSelection = ciDM_HOST_OPTION_VEHICLE
			OR iCurrentSelection = ciDM_HOST_OPTION_WEAPONS
			OR iCurrentSelection = ciDM_HOST_OPTION_FIXED_CAMERA
			OR iCurrentSelection = ciDM_HOST_OPTION_NUM_LIVES			
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciDM_HOST_OPTION_ROUNDS
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
				OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_EnableDeathmatchRoundsForLobby)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciDM_HOST_OPTION_NUM_TEAMS
				IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
					RETURN FALSE
				ENDIF
			ENDIF			
		BREAK
		
		// Parachuting, lock options if qualifying
		CASE FMMC_TYPE_BASE_JUMP
		
			IF iCurrentSelection = ciPARA_HOST_OPTION_WEATHER
			OR iCurrentSelection = ciPARA_HOST_OPTION_SHOOTING
			OR iCurrentSelection = ciPARA_HOST_OPTION_TIME_OF_DAY
			OR iCurrentSelection = ciPARA_HOST_OPTION_FIXED_CAMERA
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
		
		BREAK
		
		CASE FMMC_TYPE_RACE
		//CASE FMMC_TYPE_TRIATHLON
		
			// Lock these categories if in challenge mode
			IF iCurrentSelection = ciRC_HOST_OPTION_NUM_LAPS
			OR iCurrentSelection = ciRC_HOST_OPTION_COPS
			OR iCurrentSelection = ciRC_HOST_OPTION_MAX_WANTED
			OR iCurrentSelection = ciRC_HOST_OPTION_TRAFFIC
			OR iCurrentSelection = ciRC_HOST_OPTION_CATCHUP_CODE
			OR iCurrentSelection = ciRC_HOST_OPTION_CUSTOM_VEHICLE
			OR iCurrentSelection = ciRC_HOST_OPTION_TIME_OF_DAY
			OR iCurrentSelection = ciRC_HOST_OPTION_WEATHER
			OR iCurrentSelection = ciRC_HOST_OPTION_VEHICLE_CLASS
			OR iCurrentSelection = ciRC_HOST_OPTION_GTA_TEAM
			OR iCurrentSelection = ciRC_HOST_OPTION_SWAP_ROLES
			OR iCurrentSelection = ciRC_HOST_OPTION_SLIP_STREAM
			OR iCurrentSelection = ciRC_HOST_OPTION_FIXED_CAMERA
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
				OR IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
					RETURN FALSE
				ENDIF
				
				IF iCurrentSelection = ciRC_HOST_OPTION_TRAFFIC
					IF IS_PLAYER_IN_STUNT_RACE()
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF iCurrentSelection = ciRC_HOST_OPTION_CATCHUP_CODE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciDISABLE_CATCH_UP)
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF iCurrentSelection = ciRC_HOST_OPTION_CUSTOM_VEHICLE
					IF g_FMMC_STRUCT.iRaceCustomVehSetting = ciRCVS_OFF
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciRC_HOST_OPTION_TIME_OF_DAY
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciRACE_LOCK_TIME_OF_DAY)
					RETURN FALSE
				ENDIF
			ENDIF
					
			IF iCurrentSelection = ciRC_HOST_OPTION_TYPE
				IF IS_THIS_RACE_A_FOOT_RACE()
				OR HAS_PLAYER_SENT_AN_INVITE()
				OR IS_CORONA_IN_CHALLENGE_MODE()
					RETURN FALSE
				ENDIF
				
//				#IF FEATURE_STUNT
//				IF IS_PLAYER_IN_STUNT_RACE()
//					RETURN FALSE
//				ENDIF
//				#ENDIF
				
				IF (IS_AIR_RACE(g_FMMC_STRUCT.iRaceType) AND sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_PLANE
					AND sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_GTA)

					RETURN FALSE
				ENDIF
				
				// Players can also choose non-contact
//				IF IS_TARGET_ASSAULT_RACE()
//					RETURN FALSE
//				ENDIF
			ENDIF
		
		
			IF (iCurrentSelection = ciRC_HOST_OPTION_NUM_LAPS  AND IS_THIS_A_POINT_TO_POINT_RACE())
				RETURN FALSE
			ENDIF
			
			IF (iCurrentSelection = ciRC_HOST_OPTION_VEHICLE_CLASS)
				IF SUM_BITS(g_FMMC_STRUCT.iRaceClassBitSet) <= 1
				OR SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iCurrentSelection = ciRC_HOST_OPTION_MAX_WANTED
				IF (g_FMMC_STRUCT.iRaceCoronaMaxWantedLevel - g_FMMC_STRUCT.iRaceInitialCPWantedLevel) <= 0
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
	
	RETURN TRUE
ENDFUNC

FUNC STRING GET_WEAPON_LOADOUT_BASE_STRING()
	#IF FEATURE_TUNER
	IF IS_THIS_A_TUNER_ROBBERY_FINALE()
		RETURN "TUNER_WLOAD"
	ENDIF
	#ENDIF
	RETURN "BIKER_WLOAD"
ENDFUNC

FUNC BOOL SET_UP_CORONA_OPTION(CORONA_MENU_DATA &coronaMenuData, INT iColumnID, INT &iIndex, INT iOption, INT iMissionType, FMMC_SELECTED_ITEMS &sSelection, BOOL bHost, BOOL bUpdate = FALSE)

	TEXT_LABEL_15 tlHostOption
		
	BOOL bArrows
	BOOL bSelectable = (bHost AND NOT IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA())
	
	// Option all players have (special cases)
	IF iOption = ciALL_LOCAL_RADIO_STATION
		//IF iMissionType != FMMC_TYPE_MISSION	//CORONA NOW DOES RADIO ON MISSIONS AGAIN
			bArrows = TRUE
			
			IF DOES_RACE_HAVE_A_RADIO_MIX()
				bArrows = FALSE
			ENDIF
			
			SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, iOption, "LOB_CAT_13", GET_MY_CHOSEN_RADIO_NAME_SHORT_VERSION(), bArrows, TRUE, bUpdate)	// Radio
			RETURN TRUE
		//ENDIF
		
	ELIF iOption = ciALL_LOCAL_VIEW_JOINED_PLAYERS
	
		SET_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, 	iColumnID, iOption,	"FM_LSC_JP", FALSE, 1, TRUE)	// View Joined Players
		RETURN TRUE
		
	ELIF iOption = ciALL_LOCAL_WEAPON_LOCKED
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, iOption, "LOB_CAT_LCKW", GET_WEAPON_NAME(GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet)), FALSE, bHost, bUpdate)		// Weapon Locked
		RETURN TRUE
		
	ELIF iOption = ciALL_LOCAL_WEAPON

		WEAPON_TYPE eWeapon = GET_CORONA_WEAPON_FROM_INT(coronaMenuData.iWeaponSlot)
		IF IS_CORONA_WEAPON_VALID(eWeapon, iMissionType)
			TEXT_LABEL_15 strWeapon = GET_WEAPON_NAME(eWeapon)
			PRINTLN("[CORONA] SET_UP_CORONA_OPTION - weapon name string = ", strWeapon, ", literal = ", GET_STRING_FROM_TEXT_FILE(strWeapon))

			IF eWeapon = WEAPONTYPE_DLC_GUSENBERG
				strWeapon = "WT_GUSNBRGS"
			ENDIF

			IF NOT IS_CORONA_WEAPON_LIMITED_TO_CURRENT_WEAPON()
				bArrows = TRUE
			ENDIF
			
			SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, iOption, GET_CORONA_STARTING_WEAPON_LABEL(), strWeapon, bArrows, TRUE, bUpdate)
			RETURN TRUE
		ENDIF
		
	ELIF iOption = ciALL_LOCAL_PURCHASE_AMMO
		
//		tlHostOption = "HUD_INPUT92"
//		IF NOT CAN_PLAYER_PURCHASE_CORONA_AMMO_FOR_WEAPON(GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(coronaMenuData.iWeaponSlot)))
//			tlHostOption = "FM_COR_AMMO_F"
//		ENDIF
//		
//		SET_FRONTEND_DATA_SLOT(	CORONA_COLUMN_ONE, iIndex, iColumnID, iOption, tlHostOption, FALSE, 2, TRUE)

		WEAPON_TYPE eWeapon = GET_CORONA_WEAPON_FROM_INT(coronaMenuData.iWeaponSlot)
		IF IS_CORONA_WEAPON_VALID(eWeapon, iMissionType, TRUE)
			IF SET_UP_CORONA_AMMO_PURCHASE(coronaMenuData, CORONA_COLUMN_ONE, iIndex, iColumnID, iOption)
				RETURN TRUE
			ENDIF
		ENDIF
		
	ELIF iOption = ciALL_LOCAL_WEAPON_FILL_ALL
	
		WEAPON_TYPE eWeapon = GET_CORONA_WEAPON_FROM_INT(coronaMenuData.iWeaponSlot)
		IF IS_CORONA_WEAPON_VALID(eWeapon, iMissionType, TRUE)
			IF NOT IS_CORONA_BIT_SET(CORONA_FULL_AMMO_NO_LONGER_AVAILABLE)
				SET_FRONTEND_DATA_SLOT(	CORONA_COLUMN_ONE, iIndex, iColumnID, iOption, 
									"FM_COR_AMMO_FILL", FALSE, 1, TRUE, FALSE, HUD_COLOUR_FREEMODE,
									"", -1, "MP_BET_LOST", "", coronaMenuData.iTotalCostToFillAmmo)		
				RETURN TRUE
			ENDIF
		ENDIF
		
	ELIF iOption = ciALL_LOCAL_PURCHASE_ARMOR
	
		IF SET_UP_CORONA_ARMOR_PURCHASE(CORONA_COLUMN_ONE, iIndex, iColumnID, iOption)
			RETURN TRUE
		ENDIF
		
	ELIF iOption = ciALL_LOCAL_CASH_OR_AMMO
	
		IF CAN_PLAYER_PURCHASE_AMMO_IN_CORONA(sSelection, TRUE)
		OR CAN_PLAYER_PURCHASE_ARMOR_IN_CORONA(sSelection, TRUE)
			BOOL bBorderShown
			SET_CORONA_CASH_AND_AMMO_SUMMARY(coronaMenuData, iIndex, TRUE, bBorderShown)
			RETURN TRUE
		ENDIF

	ENDIF
	
	INT iNumberToInsert
	bArrows = FALSE
		
	IF iMissionType = FMMC_TYPE_RACE

		BOOL bIsRaceToPoint = IS_THIS_RACE_A_POINT_2_POINT_RACE()
		BOOL bIsThisAFootRace = IS_THIS_RACE_A_FOOT_RACE()
				
		INT iType, iHighlightColour
		iType = -1
				
		SWITCH iOption
			CASE ciRC_CLIENT_OPTION_PARTNER
				UPDATE_CORONA_OPTION_PREFERRED_PARTNER(coronaMenuData, iColumnID, iIndex, bUpdate)
			BREAK
			
			CASE ciRC_CLIENT_OPTION_ROLE
				tlHostOption = "LOB_CAT_ROLE"
				tlHostOption += g_iMyPreferedPosition
				
				IF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
					tlHostOption += "A"
				ENDIF
				
				IF IS_TARGET_ASSAULT_RACE()
				AND g_iMyPreferedPosition = 1
					tlHostOption = "TEAM_BIOGUNNER"
				ENDIF
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_CLIENT_OPTION_ROLE, "LOB_CAT_30", tlHostOption, TRUE, TRUE, bUpdate)
			BREAK
			
			CASE ciRC_HOST_OPTION_TYPE
				
				// Handle different prefix of type (P2P or Normal)
				IF NOT bIsRaceToPoint
					tlHostOption = "RL_CLASS_"			// Race / GTA Race / Rally
				ELSE
					tlHostOption = "RL_CLASS_P2P"		// Point to Point Race / GTA Race / Rally
				ENDIF
				
				// Handle if this is a foot race
				IF bIsThisAFootRace
					tlHostOption += "F"					// Foot Race / Point to Point Foot Race
				ELSE
					tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_TYPE]
				ENDIF
				
				IF IS_TARGET_ASSAULT_RACE()
				AND sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_GTA
					tlHostOption = "FM_C_TARGASLT"
				ENDIF
				
				bArrows = DOES_CORONA_ALLOW_TYPE_TO_BE_CHANGED(coronaMenuData.iTotalCustomVehicles)
				
				// Lock this selection if we have sent an invite
				IF HAS_PLAYER_SENT_AN_INVITE()
				OR IS_CORONA_IN_CHALLENGE_MODE()
//				OR IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
//				OR IS_THIS_TRANSITION_SESSION_IS_A_LIVESTREAM_PLAYLIST()
					bArrows = FALSE
				ENDIF
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_TYPE, "FMMC_T2_M13", tlHostOption, bArrows, bSelectable, bUpdate)
			BREAK
			
			CASE ciRC_HOST_OPTION_GTA_TEAM
				tlHostOption = "FMMC_GTA_TEAM"
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_GTA_TEAM]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_GTA_TEAM, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_GTA_TEAM, "FMMC_T2_M25", tlHostOption, bArrows, bSelectable, bUpdate)
			BREAK
			
			CASE ciRC_HOST_OPTION_NUM_LAPS
			
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_NUM_LAPS, g_sTransitionSessionOptions)
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_NUM_LAPS, "FMMC_T2_M12", "FMMC_NO", bArrows, bSelectable, bUpdate, sSelection.iSelection[ciRC_HOST_OPTION_NUM_LAPS] + 1)
				
			BREAK
			
			CASE ciRC_HOST_OPTION_VEHICLE_CLASS
				//Set the vehicle class
				tlHostOption =  GET_RACE_BASE_STRING(g_FMMC_STRUCT.iRaceType)
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS]
				
				bArrows = (SUM_BITS(g_FMMC_STRUCT.iRaceClassBitSet) > 1 AND NOT IS_BOAT_RACE(g_FMMC_STRUCT.iRaceType))
								
				IF NOT IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_VEHICLE_CLASS, g_sTransitionSessionOptions)
					bArrows = FALSE
				ENDIF
				
				IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_VEHICLE_CLASS, "FMMC_PREFEE", "FMMC_DOLLARS", FALSE, FALSE, bUpdate, CV2_PROFESSIONAL_RACE_FEE())		// Class
				ELSE
					IF IS_TRANSFORM_RACE()
						PRINTLN("[3725304] - transform")
						SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_VEHICLE_CLASS, "FMMC_VEH_CLS", tlHostOption, bArrows, bSelectable, bUpdate)		// Class
					ELSE
						PRINTLN("[3725304] - not transform")
						SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_VEHICLE_CLASS, "FMMC_VEH_CL", tlHostOption, bArrows, bSelectable, bUpdate)		// Class
					ENDIF
				ENDIF
			BREAK
			
			CASE ciRC_HOST_OPTION_TIME_OF_DAY
				// Display current settings:
				tlHostOption = "LOB_TIME_DAY_"		
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_TIME_OF_DAY]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_TIME_OF_DAY, g_sTransitionSessionOptions)
				
				IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_TIME_OF_DAY, "FMMC_PREMFIRST", "FMMC_DOLLARS", FALSE, FALSE, bUpdate, CV2_GET_PRO_RACE_CASH_REWARD(1, FALSE))		// Class
				ELSE
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_TIME_OF_DAY, "LOB_CAT_22", tlHostOption, bArrows, bSelectable, bUpdate)			// Time of Day
				ENDIF
			BREAK
			
			CASE ciRC_HOST_OPTION_WEATHER
				tlHostOption = "FMMC_MS_W0_" 
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_WEATHER]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_WEATHER, g_sTransitionSessionOptions)
				
				IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
				AND CV2_GET_PRO_RACE_CASH_REWARD(2, FALSE) > 0
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_WEATHER, "FMMC_PREMSEC", "FMMC_DOLLARS", FALSE, FALSE, bUpdate, CV2_GET_PRO_RACE_CASH_REWARD(2, FALSE))		// Class
				ELSE
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_WEATHER, "FMMC_MG_GL0", tlHostOption, bArrows, bSelectable, bUpdate)				// Weather
				ENDIF
			BREAK
			
			CASE ciRC_HOST_OPTION_TRAFFIC
			
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_TRAFFIC, g_sTransitionSessionOptions)
							
				tlHostOption = "LOB_TRAFFIC_" 
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_TRAFFIC]	
				
				IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
				AND CV2_GET_PRO_RACE_CASH_REWARD(3, FALSE) > 0
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_TRAFFIC, "FMMC_PREMTHIRD", "FMMC_DOLLARS", FALSE, FALSE, bUpdate, CV2_GET_PRO_RACE_CASH_REWARD(3, FALSE))		// Class
				ELSE
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_TRAFFIC, "LOB_CAT_7", tlHostOption, bArrows, bSelectable, bUpdate)				// Traffic
				ENDIF
			BREAK
			
			CASE ciRC_HOST_OPTION_COPS
			
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_COPS, g_sTransitionSessionOptions)
			
				tlHostOption = "LOB_POLICE_" 
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_COPS]
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_COPS, "LOB_CAT_9", tlHostOption, bArrows, bSelectable, bUpdate)					// Wanted Levels
			BREAK
			
			CASE ciRC_HOST_OPTION_MAX_WANTED
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_MAX_WANTED, g_sTransitionSessionOptions)
			
				tlHostOption = "COR_MAX_WANT_" 
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_MAX_WANTED]
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_MAX_WANTED, "COR_MAX_WANT", tlHostOption, bArrows, bSelectable, bUpdate)					// Max Wanted Level
			BREAK
			
//			CASE ciRC_HOST_OPTION_RADIO
//				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_RADIO, "LOB_CAT_13", GET_RADIO_STATION_NAME(sSelection.iSelection[ciRC_HOST_OPTION_RADIO]), bSelectable, bSelectable, bUpdate)	// Radio
//			BREAK
			
			CASE ciRC_HOST_OPTION_SWAP_ROLES
				tlHostOption = "LOB_SWAP_R_" 
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_SWAP_ROLES]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_SWAP_ROLES, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_SWAP_ROLES, "LOB_CAT_37", tlHostOption, bArrows, bSelectable, bUpdate)	
			BREAK
			
			CASE ciRC_HOST_OPTION_SPLIT_COMPARE
				SET_CORONA_BIT(CORONA_SPLIT_COMPARE_DISPLAYED)
			
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_SPLIT_COMPARE, g_sTransitionSessionOptions)
							
				tlHostOption = "LOB_SPLIT_" 
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_SPLIT_COMPARE]
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_SPLIT_COMPARE, "LOB_CAT_38", tlHostOption, bArrows, bSelectable, bUpdate)	
			BREAK
			
			CASE ciRC_HOST_OPTION_CUSTOM_VEHICLE
				IF g_FMMC_STRUCT.iRaceCustomVehSetting = ciRCVS_RESTRICTED
					tlHostOption = "LOB_CUSTb_"
					tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_CUSTOM_VEHICLE]
					
					IF sSelection.iSelection[ciRC_HOST_OPTION_CUSTOM_VEHICLE] = ciCORONA_CUSTOM_VEHICLE_ON
						iType = 4
						iHighlightColour = ENUM_TO_INT(HUD_COLOUR_RED)
					ENDIF
				ELSE
					tlHostOption = "LOB_CUST_"
					tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_CUSTOM_VEHICLE]
				ENDIF
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_CUSTOM_VEHICLE, g_sTransitionSessionOptions)
								
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_CUSTOM_VEHICLE, "LOB_CAT_39", tlHostOption, bArrows, bSelectable, bUpdate, DEFAULT, 
													DEFAULT, DEFAULT, iHighlightColour, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iType)					
			BREAK
			
			CASE ciRC_HOST_OPTION_CATCHUP_CODE
				tlHostOption = "LOB_CUST_"
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_CATCHUP_CODE]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciRC_HOST_OPTION_CATCHUP_CODE, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_CATCHUP_CODE, "LOB_CAT_40", tlHostOption, bArrows, bSelectable, bUpdate)					
			BREAK
			
			CASE ciRC_HOST_OPTION_SLIP_STREAM
				tlHostOption = "LOB_STRM_"
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_SLIP_STREAM]
				
				bArrows = TRUE
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					bArrows = FALSE
				ENDIF
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_SLIP_STREAM, "LOB_CAT_42", tlHostOption, bArrows, bSelectable, bUpdate)					
			BREAK
			
			CASE ciRC_HOST_OPTION_FIXED_CAMERA
				tlHostOption = "LOB_FCP_"
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_FIXED_CAMERA]
				
				bArrows = TRUE
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					bArrows = FALSE
				ENDIF
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_FIXED_CAMERA, "LOB_T_FCP", tlHostOption, bArrows, bSelectable, bUpdate)					
			BREAK
			
			// Add the destroy loser option with on / off text for whether the option is active or not
			CASE ciRC_HOST_OPTION_DESTROY_LOSER
				
				tlHostOption = "LOB_MIS_DL_"
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_DESTROY_LOSER]
				
				bArrows = TRUE
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					bArrows = FALSE
				ENDIF
				
				IF sSelection.iSelection[ciRC_HOST_OPTION_DESTROY_LOSER] != ciCORONA_DESTROY_VEHICLE_OFF
					iType = 4
					iHighlightColour = ENUM_TO_INT(HUD_COLOUR_RED)
				ENDIF
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_DESTROY_LOSER, "LOB_MIS_DL", tlHostOption, bArrows, bSelectable, bUpdate, DEFAULT
													, DEFAULT, DEFAULT, iHighlightColour, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iType)
				
			BREAK
			
//			#IF FEATURE_IMPORT_EXPORT
			CASE ciRC_HOST_OPTION_AGGREGATE_POS
				IF sSelection.iSelection[ciRC_HOST_OPTION_AGGREGATE_POS] = 0
					tlHostOption = "BIK_AGPOS_OFF"
				ELSE
					tlHostOption = "BIK_AGPOS_ON"
				ENDIF
				
				bArrows = TRUE
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					bArrows = FALSE
				ENDIF
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_AGGREGATE_POS, "BIK_AGPOS", tlHostOption, bArrows, bSelectable, bUpdate)					
			BREAK
//			#ENDIF

			#IF FEATURE_GEN9_EXCLUSIVE
			CASE ciRC_HOST_OPTION_HSW_MODS
				tlHostOption = "LOB_HSW_MOD_"
				tlHostOption += sSelection.iSelection[ciRC_HOST_OPTION_HSW_MODS]
				
				bArrows = TRUE
				IF SHOULD_THIS_CORONA_OPTION_BE_BLOCKED(FALSE)
					bArrows = FALSE
				ENDIF
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciRC_HOST_OPTION_HSW_MODS, "LOB_HSW_MOD", tlHostOption, bArrows, bSelectable, bUpdate)
			BREAK
			#ENDIF
		ENDSWITCH
		
	ELIF iMissionType = FMMC_TYPE_DEATHMATCH
		
		SWITCH iOption
						
			CASE ciDM_HOST_OPTION_TYPE
				tlHostOption = "LOB_CAT_TY"
				tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_TYPE]
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
				OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
				OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType)
					IF sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = 1 
						tlHostOption = "AW_COR_LTS"	// "Team Deathmatch" > "Last Team Standing"
					ELSE
						tlHostOption = "AW_COR_LMS"	// "Normal" > "Last Man Standing"
					ENDIF
				ENDIF
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_TYPE, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_TYPE, "LOB_CAT_24", tlHostOption, bArrows, bSelectable, bUpdate)					// Type
			BREAK
			
			CASE ciDM_HOST_OPTION_NUM_TEAMS
				tlHostOption = "LOB_NOTEAMS_"
				tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_NUM_TEAMS]
				
				bArrows = (NOT IS_DM_FIXED_TO_MIN_TEAMS())
				IF NOT IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_NUM_TEAMS, g_sTransitionSessionOptions)
					bArrows = FALSE
				ENDIF
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_NUM_TEAMS, "LOB_CAT_18", tlHostOption, bArrows, bSelectable, bUpdate)				// Number of Teams
			BREAK
			
			CASE ciDM_HOST_OPTION_TEAM_BAL
				tlHostOption = "LOB_TAGS_"
				tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_TEAM_BAL, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_TEAM_BAL, "LOB_CAT_19", tlHostOption, bArrows, bSelectable, bUpdate)				// Team Balancing
			BREAK
			
			CASE ciDM_HOST_OPTION_TIME_LIMIT
				tlHostOption = "LOB_DURATION_"
				tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_TIME_LIMIT]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_TIME_LIMIT, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_TIME_LIMIT, "LOB_CAT_3", tlHostOption, bArrows, bSelectable, bUpdate)				// Time Limit
			BREAK
			
			CASE ciDM_HOST_OPTION_TARGET_SCORE
				IF IS_THIS_A_KING_OF_THE_HILL_CORONA(iMissionType, sSelection.iSelection[ciDM_HOST_OPTION_TYPE])
					IF sSelection.iSelection[ciDM_HOST_OPTION_TARGET_SCORE] = 0
						tlHostOption = "TAR_KILL_OFF"
						iNumberToInsert = -1
					ELSE
						tlHostOption = "IMP_TAR_POINT"
						iNumberToInsert = sSelection.iSelection[ciDM_HOST_OPTION_TARGET_SCORE]
					ENDIF
				ELSE
					IF IS_THIS_LEGACY_DM_CONTENT()
						tlHostOption = "LOB_TARG_KIL_"
					ELSE
						tlHostOption = "LOB_TARG_SCR_"
					ENDIF
					tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_TARGET_SCORE]
					iNumberToInsert = -1
				ENDIF
					
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_TARGET_SCORE, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_TARGET_SCORE, "LOB_CAT_4", tlHostOption, bArrows, bSelectable, bUpdate, iNumberToInsert)			// Target Score
			BREAK
			
			CASE ciDM_HOST_OPTION_GAMERTAGS
				tlHostOption = "LOB_TAGS_"
				tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_GAMERTAGS]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_GAMERTAGS, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_GAMERTAGS, "LOB_CAT_41", tlHostOption, bArrows, bSelectable, bUpdate)			// Target Score
			BREAK
			
			CASE ciDM_HOST_OPTION_OPP_HEALTH_BAR
				tlHostOption = "LOB_CAT_32_"
				tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_OPP_HEALTH_BAR]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_OPP_HEALTH_BAR, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_OPP_HEALTH_BAR, "LOB_CAT_32", tlHostOption, bArrows, bSelectable, bUpdate)		// Opponent Health Bar
			BREAK
			
			CASE ciDM_HOST_OPTION_TIME_OF_DAY
				tlHostOption = "LOB_TIME_DAY_"
				tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_TIME_OF_DAY]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_TIME_OF_DAY, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_TIME_OF_DAY, "LOB_CAT_22", tlHostOption, bArrows, bSelectable, bUpdate)			// Time of Day
			BREAK
			
			CASE ciDM_HOST_OPTION_WEATHER
				tlHostOption = "FMMC_MS_W0_"
				tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_WEATHER]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_WEATHER, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_WEATHER, "FMMC_MG_GL0", tlHostOption, bArrows, bSelectable, bUpdate)				// Weather
			BREAK
			
			CASE ciDM_HOST_OPTION_TRAFFIC
				tlHostOption = "LOB_TRAFFIC_"
				tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_TRAFFIC]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_TRAFFIC, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_TRAFFIC, "LOB_CAT_7", tlHostOption, bArrows, bSelectable, bUpdate)				// Traffic
			BREAK
			
			CASE ciDM_HOST_OPTION_WEAPONS
				tlHostOption = "FMMC_LKW_"
				tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_WEAPONS]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_WEAPONS, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_WEAPONS, "FMMC_T0_LKW", tlHostOption, bArrows, bSelectable, bUpdate)				// Traffic
			BREAK
			
			CASE ciDM_HOST_OPTION_FIXED_CAMERA
				tlHostOption = "LOB_FCP_"
				tlHostOption += sSelection.iSelection[ciDM_HOST_OPTION_FIXED_CAMERA]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_FIXED_CAMERA, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_FIXED_CAMERA, "LOB_T_FCP", tlHostOption, bArrows, bSelectable, bUpdate)				// Traffic
			BREAK
			
			CASE ciDM_HOST_OPTION_NUM_LIVES
				tlHostOption = "COR_DMNLIFE"
				IF sSelection.iSelection[ciDM_HOST_OPTION_FIXED_CAMERA] > 1
					tlHostOption = "COR_DMNLIVES"
				ENDIF
								
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_NUM_LIVES, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_NUM_LIVES, "COR_DMNL", "NUMBR", bArrows, bSelectable, bUpdate, sSelection.iSelection[ciDM_HOST_OPTION_NUM_LIVES])				// Number of Lives
			BREAK
			
			CASE ciDM_HOST_OPTION_ROUNDS
			
				IF GET_CORONA_NUMBER_OF_ROUND(sSelection.iSelection[ciDM_HOST_OPTION_ROUNDS]) = 1
					tlHostOption = "FMMC_RNDTXTS"
				ELSE
					tlHostOption = "FMMC_RNDTXTP"
				ENDIF

				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciDM_HOST_OPTION_ROUNDS, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDM_HOST_OPTION_ROUNDS, "FMMC_NUMRDS", tlHostOption, bArrows, bSelectable, bUpdate, GET_CORONA_NUMBER_OF_ROUND(sSelection.iSelection[ciDM_HOST_OPTION_ROUNDS]))		// Number of Rounds
			BREAK
		ENDSWITCH

	ELIF iMissionType = FMMC_TYPE_MISSION
	
	
		SWITCH iOption
			CASE ciMISSION_CLIENT_OPTION_SEAT
				tlHostOption = "FMMC_SM_SEAT"
				tlHostOption += coronaMenuData.iClientOptionVehicleSeat
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_CLIENT_OPTION_SEAT, "FMMC_SM_PFSEAT", tlHostOption, TRUE, TRUE, bUpdate)
			BREAK
		
			CASE ciMISSION_HOST_OPTION_TEAM_BAL
				tlHostOption = "LOB_TAGS_"
				tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_TEAM_BAL]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_TEAM_BAL, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_TEAM_BAL, "LOB_CAT_19", tlHostOption, bArrows, bSelectable, bUpdate)			// Team Balancing
			BREAK
			CASE ciMISSION_HOST_OPTION_DIFFICULTY
			
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_DIFFICULTY, g_sTransitionSessionOptions)
			
				tlHostOption = "LOB_DIF_"
				
				IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
					tlHostOption += "H"
					tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY]
					
					IF sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY] = 1
						SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_DIFFICULTY, "FMMC_T2_M15", tlHostOption, bArrows, bSelectable, bUpdate)		// Difficulty
					ELSE
						SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_DIFFICULTY, "FMMC_T2_M15", tlHostOption, bArrows, bSelectable, bUpdate, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
															DEFAULT, DEFAULT, DEFAULT, GET_CORONA_HEIST_DIFFICULTY_MULTIPLIER(sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY]))		// Difficulty
					ENDIF
					
				ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
					tlHostOption += "G"
					tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY]
					
					IF sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY] = 0
						SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_DIFFICULTY, "FMMC_T2_M15", tlHostOption, bArrows, bSelectable, bUpdate)		// Difficulty
					ELSE
						SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_DIFFICULTY, "FMMC_T2_M15", tlHostOption, bArrows, bSelectable, bUpdate, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
															DEFAULT, DEFAULT, DEFAULT, GET_CORONA_HEIST_DIFFICULTY_MULTIPLIER(sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY]))		// Difficulty
					ENDIF
					
				
					
				ELSE
					
					tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY]
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_DIFFICULTY, "FMMC_T2_M15", tlHostOption, bArrows, bSelectable, bUpdate)		// Difficulty
				
				ENDIF
			BREAK
			CASE ciMISSION_HOST_OPTION_END_CONDITIONS
			
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_END_CONDITIONS, g_sTransitionSessionOptions)
			
				tlHostOption = "LOB_ECON_"
				tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_END_CONDITIONS]
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_END_CONDITIONS, "LOB_CAT_EC", tlHostOption, bArrows, bSelectable, bUpdate)		// Difficulty
			BREAK
			
			CASE ciMISSION_HOST_OPTION_ROUNDS
			
				IF GET_CORONA_NUMBER_OF_ROUND(sSelection.iSelection[ciMISSION_HOST_OPTION_ROUNDS]) = 1
					tlHostOption = "FMMC_RNDTXTS"
				ELSE
					tlHostOption = "FMMC_RNDTXTP"
				ENDIF

				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_ROUNDS, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_ROUNDS, "FMMC_NUMRDS", tlHostOption, bArrows, bSelectable, bUpdate, GET_CORONA_NUMBER_OF_ROUND(sSelection.iSelection[ciMISSION_HOST_OPTION_ROUNDS]))		// Number of Rounds
			BREAK
			
			CASE ciMISSION_HOST_OPTION_TIME_OF_DAY
				tlHostOption = "LOB_TIME_DAY_"
				tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_TIME_OF_DAY]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_TIME_OF_DAY, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_TIME_OF_DAY, "LOB_CAT_22", tlHostOption, bArrows, bSelectable, bUpdate)		// Time of Day
			BREAK
			CASE ciMISSION_HOST_OPTION_WEATHER
				tlHostOption = "FMMC_MS_W0_"
				tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_WEATHER]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_WEATHER, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_WEATHER, "FMMC_MG_GL0", tlHostOption, bArrows, bSelectable, bUpdate)			// Weather
			BREAK
			CASE ciMISSION_HOST_OPTION_WEAPONS
				tlHostOption = "FMMC_LKW_"
				tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPONS]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_WEAPONS, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_WEAPONS, "FMMC_T0_LKW", tlHostOption, bArrows, bSelectable, bUpdate)				// Traffic
			BREAK
			
			CASE ciMISSION_HOST_OPTION_OUTFIT_CUSTOMISATION
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_OUTFIT_CUSTOMISATION, "HEIST_OF_MAIN", GET_PLAYER_CORONA_ACCESS_LEVEL_TEXT(sSelection.iSelection[ciMISSION_HOST_OPTION_OUTFIT_CUSTOMISATION]), TRUE, bSelectable, bUpdate)
			BREAK
			
			CASE ciMISSION_HOST_OPTION_CLOTHING_SETUP
			
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_CLOTHING_SETUP, g_sTransitionSessionOptions)
			
				TEXT_LABEL_15 tlTitle
				IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
				OR IS_LOADED_MISSION_TYPE_FLOW_MISSION()
				
					IF IS_LOADED_MISSION_TYPE_FLOW_MISSION()
						tlTitle = "VERSUS_CLOTH"
					ELSE
				
					tlTitle = "HEIST_CLOTH"
					
					ENDIF
					
					tlHostOption = "FMMC_HC_"
					
					tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP]
					
				ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
				
					tlTitle = "HEIST_CLOTH"
				
					IF sSelection.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = 1
						tlHostOption = "FMMC_HC_1"
					ELSE
						tlHostOption = "FMMC_VC_0"
					ENDIF
				
				ELSE
					tlTitle = "VERSUS_CLOTH"
					tlHostOption = "FMMC_VC_"
					
					tlHostOption += (sSelection.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP]+1)
				ENDIF
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_CLOTHING_SETUP, tlTitle, tlHostOption, bArrows, bSelectable, bUpdate)
			BREAK
			
			CASE ciMISSION_HOST_OPTION_DISABLE_BLIPS
				
				tlHostOption = "LOB_TAGS_"
				tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_DISABLE_BLIPS]
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_DISABLE_BLIPS, "FM_COR_BLIP", tlHostOption, TRUE, bSelectable, bUpdate)				
			BREAK
			

			CASE ciMISSION_HOST_OPTION_FIXED_CAMERA
				tlHostOption = "LOB_FCP_"
				tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_FIXED_CAMERA]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_FIXED_CAMERA, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_FIXED_CAMERA, "LOB_T_FCP", tlHostOption, bArrows, bSelectable, bUpdate)				// Traffic
			BREAK
			
			CASE ciMISSION_HOST_OPTION_SUDDEN_DEATH
		
				IF IS_CORONA_MISSION_SUDDEN_DEATH_AVAILABLE()
					tlTitle = "LOB_T_MSD"
					tlHostOption = "LOB_MSD_"
					tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_SUDDEN_DEATH]
				ELSE
					tlTitle = "LOB_T_MPU"
					tlHostOption = "LOB_MPU_"
					tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_SUDDEN_DEATH]
				ENDIF
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_SUDDEN_DEATH, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_SUDDEN_DEATH, tlTitle, tlHostOption, bArrows, bSelectable, bUpdate)				// Traffic
			BREAK
			
			CASE ciMISSION_HOST_OPTION_VEHICLE_CLASS
				tlHostOption = "LOB_VC_OFF"
				
				IF sSelection.iSelection[ciMISSION_HOST_OPTION_VEHICLE_CLASS] != -1
					tlHostOption = GET_VEHICLE_LIBRARY_NAME(sSelection.iSelection[ciMISSION_HOST_OPTION_VEHICLE_CLASS])
				ENDIF
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_VEHICLE_CLASS, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_VEHICLE_CLASS, "LOB_T_VC", tlHostOption, bArrows, bSelectable, bUpdate)				// Traffic			
			BREAK			
			
			CASE ciMISSION_HOST_OPTION_NUM_TEAMS
				IF sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS] = -1
					bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_NUM_TEAMS, g_sTransitionSessionOptions)
									
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_NUM_TEAMS, "LOB_CAT_18", "BIK_AUTO", bArrows, bSelectable, bUpdate)				// Number of Teams
				ELSE
					bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_NUM_TEAMS, g_sTransitionSessionOptions)
									
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_NUM_TEAMS, "LOB_CAT_18", "NUMBER", bArrows, bSelectable, bUpdate, sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS])				// Number of Teams
				ENDIF
			BREAK
			
			CASE ciMISSION_HOST_OPTION_ROUND_DURATION
								
				tlHostOption = "EXEC_RND_MINS"
				iNumberToInsert = sSelection.iSelection[ciMISSION_HOST_OPTION_ROUND_DURATION]
							
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_ROUND_DURATION, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_ROUND_DURATION, "EXEC_RND_DURA", tlHostOption, bArrows, bSelectable, bUpdate, iNumberToInsert)			// Round Duration
			BREAK
			
			CASE ciMISSION_HOST_OPTION_TARGET_SCORE
				
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_POINTS_INSTEAD_OF_KILLS)
				OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
					tlHostOption = "IMP_TAR_POINT"
					iNumberToInsert = sSelection.iSelection[ciMISSION_HOST_OPTION_TARGET_SCORE]
				ELSE
				
					IF sSelection.iSelection[ciMISSION_HOST_OPTION_TARGET_SCORE] = 0
						tlHostOption = "TAR_KILL_OFF"
						iNumberToInsert = -1
					ELSE
						tlHostOption = "TAR_KILL_VAL"
						iNumberToInsert = sSelection.iSelection[ciMISSION_HOST_OPTION_TARGET_SCORE]
					ENDIF
				ENDIF
				
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_TARGET_SCORE, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_TARGET_SCORE, "LOB_CAT_4", tlHostOption, bArrows, bSelectable, bUpdate, iNumberToInsert)			// Target Score
			BREAK
			
			CASE ciMISSION_HOST_OPTION_USE_BAR
				
				IF sSelection.iSelection[ciMISSION_HOST_OPTION_USE_BAR] = 0
					tlHostOption = "TP_PASS_PARC"
				ELSE
					tlHostOption = "TP_POD_FIN"
				ENDIF
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_USE_BAR, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_USE_BAR, "TP_GAME_TYPE", tlHostOption, bArrows, bSelectable, bUpdate)			// Use bar
			BREAK
			
			CASE ciMISSION_HOST_OPTION_TEAM_LIVES
				
				tlHostOption = "BIK_TLIVCOUNT"
				iNumberToInsert = sSelection.iSelection[ciMISSION_HOST_OPTION_TEAM_LIVES]
							
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_TEAM_LIVES, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_TEAM_LIVES, "BIK_TEAMLIVES", tlHostOption, bArrows, bSelectable, bUpdate, iNumberToInsert)			// Team Lives
			
			BREAK
			
			CASE ciMISSION_HOST_OPTION_MISSION_TYPE
				
				IF sSelection.iSelection[ciMISSION_HOST_OPTION_MISSION_TYPE] = 0
					tlHostOption = "BIKER_MTYPE_DM"
				ELSE
					tlHostOption = "BIKER_MTYPE_LA"
				ENDIF
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_MISSION_TYPE, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_MISSION_TYPE, "BIKER_MTYPE", tlHostOption, bArrows, bSelectable, bUpdate)			// Mission type
			BREAK
			
			CASE ciMISSION_HOST_OPTION_KILLS_MULTIPLIER
				IF sSelection.iSelection[ciMISSION_HOST_OPTION_KILLS_MULTIPLIER] = 0
					tlHostOption = "BIKER_KMULT_S"
				ELIF sSelection.iSelection[ciMISSION_HOST_OPTION_KILLS_MULTIPLIER] = 1
					tlHostOption = "BIKER_KMULT_M"
				ELSE
					tlHostOption = "BIKER_KMULT_L"
				ENDIF
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_KILLS_MULTIPLIER, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_KILLS_MULTIPLIER, "BIKER_KMULT", tlHostOption, bArrows, bSelectable, bUpdate)			// Kill multiplier
			BREAK
			
			CASE ciMISSION_HOST_OPTION_HIGHEST_SCORE
				
				IF sSelection.iSelection[ciMISSION_HOST_OPTION_HIGHEST_SCORE] = 0
					tlHostOption = "BIKER_MTYPE_FF"
				ELSE
					tlHostOption = "BIKER_MTYPE_HS"
				ENDIF
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_HIGHEST_SCORE, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_HIGHEST_SCORE, "BIKER_MTYPE", tlHostOption, bArrows, bSelectable, bUpdate)			// Highest Score variation
			BREAK
			
			CASE ciMISSION_HOST_OPTION_WEAPON_LOADOUT
				
				tlHostOption = GET_WEAPON_LOADOUT_BASE_STRING()
				tlHostOption += sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPON_LOADOUT]
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_WEAPON_LOADOUT, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_WEAPON_LOADOUT, GET_WEAPON_LOADOUT_BASE_STRING(), tlHostOption, bArrows, bSelectable, bUpdate)			// Weapon Loadout
			BREAK 
			
			CASE 27 // ciMISSION_HOST_OPTION_NUMBER_OF_LAPS & ciMISSION_HOST_OPTION_EXIT_TYPE
				IF SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
					IF sSelection.iSelection[ciMISSION_HOST_OPTION_EXIT_TYPE] = 0
						tlHostOption = "IE_ETYPE_HELI"
					ELSE
						tlHostOption = "IE_ETYPE_PVS"
					ENDIF
					
					bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_EXIT_TYPE, g_sTransitionSessionOptions)
					
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_EXIT_TYPE, "IE_EXITTYPE", tlHostOption, bArrows, bSelectable, bUpdate)			// Exit type
				ELSE
				
					bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_NUMBER_OF_LAPS, g_sTransitionSessionOptions)
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
						tlHostOption = "LOB_M_NOR"
					ELSE
						tlHostOption = "LOB_M_NOL"
					ENDIF
					SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_NUMBER_OF_LAPS, tlHostOption, "NUMBER", bArrows, bSelectable, bUpdate, sSelection.iSelection[ciMISSION_HOST_OPTION_NUMBER_OF_LAPS])			// Weapon Loadout
				ENDIF
			
			BREAK 
			
			CASE ciMISSION_HOST_OPTION_PILOT
				TEXT_LABEL_63 tl63HostOption
				tl63HostOption = GET_TRANSITION_PLAYER_GAMERTAG(g_iSVMSelectedPilot)
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_PILOT, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_PILOT, "IE_PILOT", "", bArrows, bSelectable, bUpdate, DEFAULT, DEFAULT, tl63HostOption) // Pilot
			BREAK
			
			CASE ciMISSION_HOST_OPTION_PICKUPS
				IF sSelection.iSelection[ciMISSION_HOST_OPTION_PICKUPS] = 0
					tlHostOption = "FMMC_SEL_DSB"	// Disabled
				ELSE
					tlHostOption = "FMMC_SEL_ENB"	// Enabled
				ENDIF
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_PICKUPS, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_PICKUPS, "SR_COR_PUPS", tlHostOption, bArrows, bSelectable, bUpdate)			// Pickups
			BREAK
			
			CASE ciMISSION_HOST_OPTION_VEHICLE_LIST
				IF sSelection.iSelection[ciMISSION_HOST_OPTION_VEHICLE_LIST] = 0
					tlHostOption = "FMMC_AQ_VL1S"	// List 1
				ELSE
					tlHostOption = "FMMC_AQ_VL2S"	// List 2
				ENDIF
				
				bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciMISSION_HOST_OPTION_VEHICLE_LIST, g_sTransitionSessionOptions)
				
				SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciMISSION_HOST_OPTION_VEHICLE_LIST, "FMMC_AQ_VL", tlHostOption, bArrows, bSelectable, bUpdate)			// Vehicle List
			BREAK
		ENDSWITCH
		
	ENDIF

	RETURN FALSE
ENDFUNC


FUNC BOOL IS_SELECTION_CLIENT_OPTION(INT iMissionType, INT iCurrentSelection)
	
	IF iCurrentSelection = ciALL_LOCAL_RADIO_STATION
		RETURN TRUE
	ENDIF
	
	IF iCurrentSelection = ciALL_LOCAL_WEAPON
		RETURN TRUE
	ENDIF
	
	IF iCurrentSelection = ciALL_LOCAL_PURCHASE_AMMO
		RETURN TRUE
	ENDIF	
	
	IF iCurrentSelection = ciALL_LOCAL_PURCHASE_ARMOR
		RETURN TRUE
	ENDIF
	
	IF iCurrentSelection = ciALL_LOCAL_WEAPON_FILL_ALL
		RETURN TRUE
	ENDIF
	
	IF iCurrentSelection = ciALL_LOCAL_VIEW_JOINED_PLAYERS
		RETURN TRUE
	ENDIF
	
	SWITCH iMissionType
		CASE FMMC_TYPE_RACE
			IF iCurrentSelection = ciRC_CLIENT_OPTION_PARTNER
			OR iCurrentSelection = ciRC_CLIENT_OPTION_ROLE
				RETURN TRUE
			ENDIF
		BREAK
		CASE FMMC_TYPE_MISSION
			IF iCurrentSelection = ciMISSION_CLIENT_OPTION_SEAT
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC DEFAULT_CORONA_MENU_SELECTION(INT &iCurrentSelection, INT &iHighlightIndex, INT i, INT iMissionType, INT iIndex, BOOL bHost)
	IF iCurrentSelection = -1
		IF (iIndex = 0 AND bHost)
		OR IS_SELECTION_CLIENT_OPTION(iMissionType, i)
		
			PRINTLN("[CORONA] DEFAULT_CORONA_MENU_SELECTION - coronaMenuData.iCurrentSelection = ", i)
		
			iCurrentSelection = i
		ENDIF
	ENDIF
	
	IF i = iCurrentSelection
		PRINTLN("[CORONA] DEFAULT_CORONA_MENU_SELECTION - iHighlightIndex = iIndex ", iIndex, " i = ", i)
		iHighlightIndex = iIndex
	ENDIF
ENDPROC

PROC PROCESS_CORONA_MENU_RESET(INT &iCurrentSelection, INT &iHighlight, INT iOption)
	IF iCurrentSelection = iOption
		iCurrentSelection = ciALL_LOCAL_RADIO_STATION
		iHighlight = 0
	ENDIF
ENDPROC

FUNC BOOL SETUP_CORONA_CLIENT_OPTIONS_FOR_ALL_JOBS(BOOL bHost, INT i, INT iMissionType, BOOL bOptionsInit)

	IF NOT bHost
		IF NOT IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
			IF NOT IS_SELECTION_CLIENT_OPTION(iMissionType, i)
				IF NOT bOptionsInit
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SETUP_CORONA_WEAPON_MENU_OPTION(CORONA_MENU_DATA &coronaMenuData, FMMC_SELECTED_ITEMS &sSelection, INT &iHighlightIndex, BOOL bHost, INT &iIndex, INT iMissionType, INT iColumnID)

	IF CAN_PLAYER_PURCHASE_AMMO_IN_CORONA(sSelection, TRUE)

		BOOL bRefreshHighlightForFullAmmo = FALSE

		IF coronaMenuData.iCurrentSelection = ciALL_LOCAL_WEAPON_FILL_ALL
		AND IS_CORONA_BIT_SET(CORONA_FULL_AMMO_NO_LONGER_AVAILABLE)
			bRefreshHighlightForFullAmmo = TRUE
		ENDIF

		IF SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_WEAPON, iMissionType, sSelection, bHost)
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_WEAPON, iMissionType, iIndex, bHost)
			
			// Move highlight and selection up above purchase all
			IF bRefreshHighlightForFullAmmo
				coronaMenuData.iCurrentSelection = ciALL_LOCAL_WEAPON
				iHighlightIndex = iIndex
			ENDIF
			
			iIndex++
			
			IF SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_PURCHASE_AMMO, iMissionType, sSelection, bHost)
				DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_AMMO, iMissionType, iIndex, bHost)
				
				// Move highlight and selection up above purchase all
				IF bRefreshHighlightForFullAmmo
					coronaMenuData.iCurrentSelection = ciALL_LOCAL_PURCHASE_AMMO
					iHighlightIndex = iIndex
				ENDIF
				
				iIndex++
			ELSE
				PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_AMMO)
			ENDIF
		ELSE
			PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_WEAPON)
			PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_AMMO)
			PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_WEAPON_FILL_ALL)
		ENDIF

		IF SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_WEAPON_FILL_ALL, iMissionType, sSelection, bHost)
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_WEAPON_FILL_ALL, iMissionType, iIndex, bHost)
			iIndex++
		ENDIF
	ELSE
		// Catch us if we have cycled to a now invalid method
		PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_WEAPON)
		PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_AMMO)
		PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_WEAPON_FILL_ALL)
	ENDIF

ENDPROC

PROC SET_UP_OPTIONAL_SCTV_CORONA_OPTION(CORONA_MENU_DATA &coronaMenuData, INT &iIndex, INT iColumnID, INT &iHighlightIndex, INT iMissionType)
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF CAN_SCTV_LAUNCH_THIS_TRANSITION_SESSION()
		AND NOT SHOULD_THIS_TOURNAMENT_TRANSITON_SESSION_LAUNCH()
			SET_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciALL_HOST_OPTION_CONTINUE, "FM_ICS_CONT", FALSE, 2)
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_HOST_OPTION_CONTINUE, iMissionType, iIndex, TRUE)
			iIndex++
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Check if we should skip the invite screen  
FUNC BOOL SHOULD_CORONA_SKIP_INVITE_SCREEN( CORONA_MENU_DATA 			&coronaMenuData,
											MISSION_TO_LAUNCH_DETAILS 	&sLaunchMissionDetails)
	
	// If this mission is a heist
	IF IS_THIS_MISSION_OF_TYPE_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistMission, sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene)
		// And it is the immediate corona following the opening cutscene
		IF IS_CORONA_BIT_SET(CORONA_ALLOW_INVITE_SCREEN_TO_BE_SKIPPED)
			
			// And total players is at the max, then we can safely skip
			IF GET_TOTAL_PLAYERS_IN_CORONA(coronaMenuData) >= GET_MISSION_TYPE_MAX(sLaunchMissionDetails)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	IF (IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE())
	AND NOT g_sMPTunables.bEnableProfessionalRaceInvites
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the client should have the invite option in the corona
FUNC BOOL SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)

	// SCTV players can not have this option, so should always see the view joined player info
	IF IS_PLAYER_SCTV(PLAYER_ID())
		RETURN FALSE
	ENDIF

	IF GET_MISSION_TYPE_MAX(sLaunchMissionDetails) <= 2
		RETURN FALSE
	ENDIF

	IF (IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE())
	AND NOT g_sMPTunables.bEnableProfessionalRaceInvites
		RETURN FALSE
	ENDIF
	
	IF IS_CORONA_IN_CHALLENGE_MODE()
	OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
	OR IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
	OR IS_THIS_TRANSITION_SESSION_IS_A_LIVESTREAM_PLAYLIST()
		RETURN FALSE
	ENDIF

	// If the tunable has been set to TRUE, do not allow the clients the option to invite
	IF g_sMPTunables.bDisableClientCoronaInvites
		RETURN FALSE
	ENDIF
		
	// If the host has allowed clients to invite
	IF g_sTransitionSessionData.bTransitionSessionClientInvites
		RETURN TRUE
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Add the CONTINUE option to the menu, always available for the host and for client if option is enabled in lobby
PROC ADD_CORONA_MENU_CONTINUE_OPTION(CORONA_MENU_DATA &coronaMenuData, MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails, BOOL &bContinueSetup, 
									 INT &iIndex, INT &iHighlightIndex, BOOL bHost, BOOL bHideContinue)

	// We have already set this up then exit
	IF bContinueSetup
		EXIT
	ENDIF
	
	// SCTV players can not have this option
	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF

	IF NOT bHideContinue
		IF (bHost OR SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails))
		AND NOT IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
		AND NOT CAN_SCTV_LAUNCH_THIS_TRANSITION_SESSION()
		
			// Set up 'MANUAL INVITE' option
			
			BOOL bAllowContinueOption = TRUE
			TEXT_LABEL_15 tlHostOption = "FM_ISC_MINV"
			
			IF (IS_CORONA_IN_AUTOFILL_MODE() AND GET_TOTAL_PLAYERS_IN_CORONA(coronaMenuData) >= GET_MISSION_TYPE_MAX(sLaunchMissionDetails))
			OR (IS_CORONA_IN_HEAD_2_HEAD_MODE() AND HAVE_DONE_FIRST_PLAYLIST_MISSION())
			OR IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
			OR IS_THIS_A_SOLO_SESSION()
			OR IS_JOB_TYPE_ONE_PLAYER_ONLY(sLaunchMissionDetails)
			OR SHOULD_CORONA_SKIP_INVITE_SCREEN(coronaMenuData, sLaunchMissionDetails)
				tlHostOption = "FM_LSC_SG"
				SET_CORONA_BIT(CORONA_CONTINUE_IS_PLAY)
				
				// IF we are not the host here, we shouldn't see the continue option at all
				IF NOT bHost
					bAllowContinueOption = FALSE
				ENDIF
			ELSE
				CLEAR_CORONA_BIT(CORONA_CONTINUE_IS_PLAY)
			ENDIF
			
			// If we are a client, we should see continue not Confirm Setting or Play
			IF NOT bHost
			OR IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
				tlHostOption = "FM_ISC_CCONF"
			ENDIF
				
			IF bAllowContinueOption
				SET_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, coronaMenuData.CORONA_COLUMN_ONE_ID, ciALL_HOST_OPTION_CONTINUE, tlHostOption, FALSE, 2)
				
				IF coronaMenuData.iCurrentSelection = -1
					coronaMenuData.iCurrentSelection = ciALL_HOST_OPTION_CONTINUE
					iHighlightIndex = 0
				ENDIF
				
				IF coronaMenuData.iCurrentSelection = ciALL_HOST_OPTION_CONTINUE
					PRINTLN("[CORONA] SETUP_CORONA_HOST_MENU_OPTIONS - coronaMenuData.iCurrentSelection = ciALL_HOST_OPTION_CONTINUE: ", iIndex)
					iHighlightIndex = iIndex
				ENDIF
				
				iIndex++
			
				bContinueSetup = TRUE
			ENDIF
		ENDIF
		
	ENDIF	

ENDPROC

FUNC BOOL SHOULD_HIDE_FORCED_WEAPONS_OPTION()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciMISSION_IS_PSEUDORACE)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Forced_Weapons)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: Sets up the host menu options on first screen of corona
PROC SETUP_CORONA_HOST_MENU_OPTIONS(CORONA_MENU_DATA 			&coronaMenuData, 
									MISSION_TO_LAUNCH_DETAILS 	&sLaunchMissionDetails,
									INT iMissionType, FMMC_SELECTED_ITEMS &sSelection, BOOL bHost, BOOL bHideContinue = FALSE)

	// All host behaviour is lost for tutorial corona
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
	OR (IS_SCTV_IN_CONTROL_OF_TRANSITION_SESSION() AND NOT IS_PLAYER_SCTV(PLAYER_ID()))
		bHost = FALSE
	ELSE
		IF (IS_SCTV_IN_CONTROL_OF_TRANSITION_SESSION() AND IS_PLAYER_SCTV(PLAYER_ID()))
			bHost = TRUE
		ENDIF
	ENDIF
	
	IF IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
		bHost = FALSE
	ENDIF

	BOOL bInitialiseGlobalClientOptions // So we place our local client options such as RADIO
	BOOL bContinueSetupAlready
	TEXT_LABEL_15 tlHostOption
	INT iColumnID = coronaMenuData.CORONA_COLUMN_ONE_ID

	INT i = 0
	INT iIndex
	INT iHighlightIndex = coronaMenuData.iCurrentSelection

	IF NOT bHost
		IF NOT IS_SELECTION_CLIENT_OPTION(iMissionType, coronaMenuData.iCurrentSelection)
		OR (coronaMenuData.iCurrentSelection = ciALL_LOCAL_WEAPON_FILL_ALL AND IS_CORONA_BIT_SET(CORONA_FULL_AMMO_NO_LONGER_AVAILABLE))
		OR NOT IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, sSelection, iMissionType, coronaMenuData.iCurrentSelection, bHost)
		OR (coronaMenuData.iCurrentSelection = ciALL_LOCAL_VIEW_JOINED_PLAYERS AND SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails))
			coronaMenuData.iCurrentSelection = -1
		ENDIF
	ELSE
		IF (coronaMenuData.iCurrentSelection = ciALL_LOCAL_WEAPON_FILL_ALL AND IS_CORONA_BIT_SET(CORONA_FULL_AMMO_NO_LONGER_AVAILABLE))
			coronaMenuData.iCurrentSelection = -1
		ENDIF
	ENDIF

	TAKE_CONTROL_OF_FRONTEND()
	LOCK_MOUSE_SUPPORT(FALSE)

	// Now always clearing the data slots
	CLEAR_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE)

	IF iMissionType = FMMC_TYPE_RACE
		
		PRINTLN("[CORONA] SETUP_CORONA_HOST_MENU_OPTIONS - iMissionType = FMMC_TYPE_RACE")
		PRINTLN("[CORONA] SETUP_CORONA_HOST_MENU_OPTIONS - coronaMenuData.iCurrentSelection = ", coronaMenuData.iCurrentSelection)

		iIndex = 0
		FOR i = 0 TO ciRC_MAX_OPTIONS-1 STEP 1
			
			IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, sSelection, iMissionType, i, bHost)
				
				// Set up global options as client option (E.g. Radio)
				IF SETUP_CORONA_CLIENT_OPTIONS_FOR_ALL_JOBS(bHost, i, iMissionType, bInitialiseGlobalClientOptions)
							
					IF NOT IS_CORONA_BIT_SET(CORONA_NO_RADIO)
						SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, sSelection, bHost)
						DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, iIndex, bHost)
						iIndex++
					ENDIF
					
					IF NOT SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails)
						SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, sSelection, bHost)
						DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, iIndex, bHost)
						iIndex++
					ENDIF
					
					SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
					
					// Add continue option if needed
					ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)
					
					bInitialiseGlobalClientOptions = TRUE
				ENDIF
				
				SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, i, iMissionType, sSelection, bHost)
				
				// Initialise our selection if needed
				DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, i, iMissionType, iIndex, bHost)
				
				iIndex++
			ELSE
				IF i = coronaMenuData.iCurrentSelection
					iHighlightIndex = 0
					coronaMenuData.iCurrentSelection = 0
				ENDIF
			ENDIF
		ENDFOR

	ELIF iMissionType = FMMC_TYPE_DEATHMATCH
	
		PRINTLN("[CORONA] SETUP_CORONA_HOST_MENU_OPTIONS - iMissionType = FMMC_TYPE_DEATHMATCH")
		PRINTLN("[CORONA] SETUP_CORONA_HOST_MENU_OPTIONS - coronaMenuData.iCurrentSelection = ", coronaMenuData.iCurrentSelection)
	
		iIndex = 0
		FOR i = 0 TO ciDM_MAX_OPTIONS-1 STEP 1
			
			IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, sSelection, iMissionType, i, bHost)
				
				// Set up global options as client option (E.g. Radio)
				IF SETUP_CORONA_CLIENT_OPTIONS_FOR_ALL_JOBS(bHost, i, iMissionType, bInitialiseGlobalClientOptions)
							
					IF NOT IS_CORONA_BIT_SET(CORONA_NO_RADIO)
						SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, sSelection, bHost)
						DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, iIndex, bHost)
						iIndex++
					ENDIF
					
					IF NOT SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails)
						SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, sSelection, bHost)
						DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, iIndex, bHost)
						iIndex++
					ENDIF
					
					SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
					
					// Add continue option if needed
					ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)
										
					bInitialiseGlobalClientOptions = TRUE
				ENDIF
				
				
				SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, i, iMissionType, sSelection, bHost)
				
				// Initialise our selection if needed
				IF coronaMenuData.iCurrentSelection = -1
					IF (iIndex = 0 AND bHost)
					OR IS_SELECTION_CLIENT_OPTION(iMissionType, i)
						PRINTLN("[CORONA] SETUP_CORONA_HOST_MENU_OPTIONS - coronaMenuData.iCurrentSelection = ", i)
					
						coronaMenuData.iCurrentSelection = i
					ENDIF
				ENDIF
								
				IF i = coronaMenuData.iCurrentSelection
					PRINTLN("[CORONA] SETUP_CORONA_HOST_MENU_OPTIONS - iHighlightIndex = iIndex ", iIndex, " i = ", i)
				
					iHighlightIndex = iIndex
				ENDIF
				
				iIndex++
			ENDIF
		ENDFOR
		
	ELIF iMissionType = FMMC_TYPE_MISSION
		
		PRINTLN("[CORONA] SETUP_CORONA_HOST_MENU_OPTIONS - iMissionType = FMMC_TYPE_MISSION")
	
		iIndex = 0
		FOR i = 0 TO ciMISSION_MAX_OPTIONS-1 STEP 1
			
			IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, sSelection, iMissionType, i, bHost)
				
				IF SETUP_CORONA_CLIENT_OPTIONS_FOR_ALL_JOBS(bHost, i, iMissionType, bInitialiseGlobalClientOptions)
					
					IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, sSelection, iMissionType, ciMISSION_CLIENT_OPTION_SEAT, bHost)
						SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciMISSION_CLIENT_OPTION_SEAT, iMissionType, sSelection, bHost)
						DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciMISSION_CLIENT_OPTION_SEAT, iMissionType, iIndex, bHost)
						iIndex++
					ENDIF
					
					IF NOT IS_CORONA_BIT_SET(CORONA_NO_RADIO)
						SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, sSelection, bHost)
						DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, iIndex, bHost)
						iIndex++
					ENDIF
					
					SETUP_CORONA_WEAPON_MENU_OPTION(coronaMenuData, sSelection, iHighlightIndex, bHost, iIndex, iMissionType, iColumnID)
					
					IF CAN_PLAYER_PURCHASE_ARMOR_IN_CORONA(sSelection, TRUE)
						IF SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, sSelection, bHost)
							DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, iIndex, bHost)
							iIndex++
						ELSE
							PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
						ENDIF
					ELSE
						PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
					ENDIF
					
					IF NOT SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails)
						SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, sSelection, bHost)
						DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, iIndex, bHost)
						iIndex++
					ENDIF
					
					SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
					
					// Add continue option if needed
					ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)			
					
					bInitialiseGlobalClientOptions = TRUE
				ENDIF
				
				SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, i, iMissionType, sSelection, bHost)
				
				// Initialise our selection if needed
				IF coronaMenuData.iCurrentSelection = -1
					IF (iIndex = 0 AND bHost)
					OR IS_SELECTION_CLIENT_OPTION(iMissionType, i)
						coronaMenuData.iCurrentSelection = i
					ENDIF
				ENDIF
				
				IF i = coronaMenuData.iCurrentSelection
					iHighlightIndex = iIndex
				ENDIF
				
				iIndex++
				PRINTLN("INCREMENTING iIndex: ", iIndex, " Menu: ", i)
			ENDIF
		ENDFOR
		
			// Hard coded this in as we hide all options for heists but need weapons etc.
			// We probably want to move this out to below on the ELSE section
			IF IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, sLaunchMissionDetails.bIsHeistMission   , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
				
				IF NOT bHost
				AND NOT bInitialiseGlobalClientOptions
					
					SETUP_CORONA_WEAPON_MENU_OPTION(coronaMenuData, sSelection, iHighlightIndex, bHost, iIndex, iMissionType, iColumnID)
					
					IF CAN_PLAYER_PURCHASE_ARMOR_IN_CORONA(sSelection, TRUE)
						IF SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, sSelection, bHost)
							DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, iIndex, bHost)
							iIndex++
						ELSE
							PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
						ENDIF
					ELSE
						PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
					ENDIF
					
					IF NOT SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails)
						SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, sSelection, bHost)
						DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, iIndex, bHost)
						iIndex++
					ENDIF
					
					SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
					
					// Add continue option if needed
					ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)
					
					bInitialiseGlobalClientOptions = TRUE
				
				ENDIF
				
				
			ENDIF
		
		
	ELIF iMissionType = FMMC_TYPE_MG_TENNIS
	
		// Set up global options as client option (E.g. Radio)
		IF SETUP_CORONA_CLIENT_OPTIONS_FOR_ALL_JOBS(bHost, ciTENNIS_HOST_OPTION_NUM_OF_SETS, iMissionType, bInitialiseGlobalClientOptions)
					
			SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, sSelection, bHost)
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, iIndex, bHost)
			iIndex++
			
			IF NOT SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails)
				SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, sSelection, bHost)
				DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, iIndex, bHost)
				iIndex++
			ENDIF
			
			SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
			
			// Add continue option if needed
			ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)

			bInitialiseGlobalClientOptions = TRUE
		ENDIF
	
		// Setup host options
		tlHostOption = "FMMC_MG_TN0_"
		IF sSelection.iSelection[ciTENNIS_HOST_OPTION_NUM_OF_SETS] < 0
			sSelection.iSelection[ciTENNIS_HOST_OPTION_NUM_OF_SETS] = 3
		ENDIF
		
		tlHostOption += sSelection.iSelection[ciTENNIS_HOST_OPTION_NUM_OF_SETS]
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciTENNIS_HOST_OPTION_NUM_OF_SETS, "FMMC_MG_TN0", tlHostOption, bHost, bHost)		// Length of Game
					
		DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, iIndex, iMissionType, iIndex, bHost)
		iIndex++

	ELIF iMissionType = FMMC_TYPE_MG_GOLF
		
		// Set up global options as client option (E.g. Radio)
		IF SETUP_CORONA_CLIENT_OPTIONS_FOR_ALL_JOBS(bHost, ciGOLF_HOST_OPTION_START_HOLE, iMissionType, bInitialiseGlobalClientOptions)
					
			SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, sSelection, bHost)
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, iIndex, bHost)
			iIndex++
			
			IF NOT SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails)
				SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, sSelection, bHost)
				DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, iIndex, bHost)
				iIndex++
			ENDIF
			
			SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
			
			// Add continue option if needed
			ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)

			bInitialiseGlobalClientOptions = TRUE
		ENDIF
		
		// Add row for the start hole
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciGOLF_HOST_OPTION_START_HOLE, "FMMC_T2_M19", "FMMC_NO", bHost, bHost, FALSE, sSelection.iSelection[ciGOLF_HOST_OPTION_START_HOLE] + 1)			// Start Hole
		DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, iIndex, iMissionType, iIndex, bHost)
		
		iIndex++

		// Add row for the number of holes to play
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciGOLF_HOST_OPTION_NUM_OF_HOLES, "FMMC_T2_M20", "FMMC_NO", bHost, bHost, FALSE, sSelection.iSelection[ciGOLF_HOST_OPTION_NUM_OF_HOLES] + 1)		// Number of Holes
		DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, iIndex, iMissionType, iIndex, bHost)
		
		iIndex++
		
		// Add row for the weather during golf
		
		IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, g_sTransitionSessionOptions, iMissionType, ciGOLF_HOST_OPTION_WEATHER, bHost)
		
			tlHostOption = "FMMC_MS_W0_"
			IF sSelection.iSelection[ciGOLF_HOST_OPTION_WEATHER] < 0
				sSelection.iSelection[ciGOLF_HOST_OPTION_WEATHER] = 2
			ENDIF
			tlHostOption += sSelection.iSelection[ciGOLF_HOST_OPTION_WEATHER]
			SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciGOLF_HOST_OPTION_WEATHER, "FMMC_MG_GL0", tlHostOption, bHost, bHost)			// Weather
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, iIndex, iMissionType, iIndex, bHost)

			iIndex++
		ENDIF
	
	ELIF iMissionType = FMMC_TYPE_MG_DARTS
	
		IF SETUP_CORONA_CLIENT_OPTIONS_FOR_ALL_JOBS(bHost, ciDARTS_HOST_OPTION_NUM_OF_LEGS, iMissionType, bInitialiseGlobalClientOptions)
					
			SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, sSelection, bHost)
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, iIndex, bHost)
			iIndex++
			
			IF NOT SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails)
				SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, sSelection, bHost)
				DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, iIndex, bHost)
				iIndex++
			ENDIF
			
			SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
			
			// Add continue option if needed
			ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)	
			
			bInitialiseGlobalClientOptions = TRUE
		ENDIF
	
		tlHostOption = "FMMC_DR_L"
		tlHostOption += sSelection.iSelection[ciDARTS_HOST_OPTION_NUM_OF_LEGS]
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDARTS_HOST_OPTION_NUM_OF_LEGS, "FMMC_T2_M21", tlHostOption, bHost, bHost)		// Wanted Levle
		DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, iIndex, iMissionType, iIndex, bHost)
		
		iIndex++

		tlHostOption = "FMMC_DR_S"
		tlHostOption += sSelection.iSelection[ciDARTS_HOST_OPTION_NUM_OF_SETS]
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciDARTS_HOST_OPTION_NUM_OF_SETS, "FMMC_T2_M22", tlHostOption, bHost, bHost)		// Wanted Levle
		DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, iIndex, iMissionType, iIndex, bHost)
		
		iIndex++

	ELIF iMissionType = FMMC_TYPE_BASE_JUMP
	
		IF SETUP_CORONA_CLIENT_OPTIONS_FOR_ALL_JOBS(bHost, ciPARA_HOST_OPTION_TIME_OF_DAY, iMissionType, bInitialiseGlobalClientOptions)
					
			SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, sSelection, bHost)
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, iIndex, bHost)
			
			iIndex++
			
			SETUP_CORONA_WEAPON_MENU_OPTION(coronaMenuData, sSelection, iHighlightIndex, bHost, iIndex, iMissionType, iColumnID)
			
			IF CAN_PLAYER_PURCHASE_ARMOR_IN_CORONA(sSelection, TRUE)
				IF SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, sSelection, bHost)
					DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, iIndex, bHost)
					iIndex++
				ELSE
					PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
				ENDIF
			ELSE
				PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
			ENDIF
			
			IF NOT SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails)
				SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, sSelection, bHost)
				DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, iIndex, bHost)
				iIndex++
			ENDIF
			
			SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
			
			// Add continue option if needed
			ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)
			
			bInitialiseGlobalClientOptions = TRUE
		ENDIF
	
		tlHostOption = "LOB_TIME_DAY_"
		tlHostOption += sSelection.iSelection[ciPARA_HOST_OPTION_TIME_OF_DAY]
		
		BOOL bArrows = TRUE
		bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciPARA_HOST_OPTION_TIME_OF_DAY, g_sTransitionSessionOptions)
		
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciPARA_HOST_OPTION_TIME_OF_DAY, "LOB_CAT_22", tlHostOption, bArrows, bHost)		// Wanted Levle
		DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, iIndex, iMissionType, iIndex, bHost)
		
		iIndex++
		
		IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, g_sTransitionSessionOptions, iMissionType, ciPARA_HOST_OPTION_WEATHER, bHost)
		
			tlHostOption = "FMMC_MS_W0_"
			tlHostOption += sSelection.iSelection[ciPARA_HOST_OPTION_WEATHER]
			
			bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciPARA_HOST_OPTION_WEATHER, g_sTransitionSessionOptions)
		
			SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciPARA_HOST_OPTION_WEATHER, "FMMC_MG_GL0", tlHostOption, bArrows, bHost)		// Wanted Levle
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciPARA_HOST_OPTION_WEATHER, iMissionType, iIndex, bHost)
		
			iIndex++
		ENDIF
		
		tlHostOption = "LOB_TAGS_"
		tlHostOption += sSelection.iSelection[ciPARA_HOST_OPTION_SHOOTING]
		
		bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciPARA_HOST_OPTION_SHOOTING, g_sTransitionSessionOptions)
		
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciPARA_HOST_OPTION_SHOOTING, "FMMC_MG_GL1", tlHostOption, bArrows, bHost)		// Wanted Levle
		DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciPARA_HOST_OPTION_SHOOTING, iMissionType, iIndex, bHost)
		
		iIndex++
		
		tlHostOption = "LOB_FCP_"
		tlHostOption += sSelection.iSelection[ciPARA_HOST_OPTION_FIXED_CAMERA]
		
		bArrows = IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, ciPARA_HOST_OPTION_FIXED_CAMERA, g_sTransitionSessionOptions)
		
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciPARA_HOST_OPTION_FIXED_CAMERA, "LOB_T_FCP", tlHostOption, bArrows, bHost)	//Fixed Camera
		DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciPARA_HOST_OPTION_FIXED_CAMERA, iMissionType, iIndex, bHost)
		
		iIndex++
			
	ELIF iMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
		
		IF SETUP_CORONA_CLIENT_OPTIONS_FOR_ALL_JOBS(bHost, ciSHOOTING_HOST_OPTION_MATCH, iMissionType, bInitialiseGlobalClientOptions)
					
			SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, sSelection, bHost)
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, iIndex, bHost)
			iIndex++
			
			IF NOT SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails)
				SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, sSelection, bHost)
				DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, iIndex, bHost)
				iIndex++
			ENDIF
			
			SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
			
			// Add continue option if needed
			ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)
			
			bInitialiseGlobalClientOptions = TRUE
		ENDIF
		
		tlHostOption = "FMMC_T2_MSH_"
		tlHostOption += sSelection.iSelection[ciSHOOTING_HOST_OPTION_MATCH]
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciSHOOTING_HOST_OPTION_MATCH, "FMMC_T2_M24", tlHostOption, bHost, bHost)		// Wanted Level
		DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, iIndex, iMissionType, iIndex, bHost)
		
		iIndex++
	
	ELIF iMissionType = FMMC_TYPE_SURVIVAL
		
		IF SETUP_CORONA_CLIENT_OPTIONS_FOR_ALL_JOBS(bHost, ciSHOOTING_HOST_OPTION_MATCH, iMissionType, bInitialiseGlobalClientOptions)
					
			SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, sSelection, bHost)
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, iIndex, bHost)
			
			iIndex++
			
			SETUP_CORONA_WEAPON_MENU_OPTION(coronaMenuData, sSelection, iHighlightIndex, bHost, iIndex, iMissionType, iColumnID)
			
			IF CAN_PLAYER_PURCHASE_ARMOR_IN_CORONA(sSelection, TRUE)
				IF SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, sSelection, bHost)
					DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, iIndex, bHost)
					iIndex++
				ELSE
					PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
				ENDIF
			ELSE
				PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
			ENDIF
			
			IF NOT SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails)
				SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, sSelection, bHost)
				DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, iIndex, bHost)
				iIndex++
			ENDIF
			
			SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
			
			// Add continue option if needed
			ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)	
			
			bInitialiseGlobalClientOptions = TRUE		
		ENDIF
		
		IF IS_THIS_A_NEW_SURVIVAL_MISSION(g_FMMC_STRUCT.iAdversaryModeType)
		AND IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, g_sTransitionSessionOptions, iMissionType, ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES, bHost)
		
			// Add row for endless waves
			IF sSelection.iSelection[ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES] = ciSURVIVAL_ENDLESS_WAVES_ENABLED
				tlHostOption = "SURV_EWAVEEN"
			ELSE
				tlHostOption = "SURV_EWAVEDIS"
			ENDIF
			SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES, "SURV_EWAVES", tlHostOption, bHost, bHost)		// Endless waves
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciSURVIVAL_HOST_OPTION_ENDLESS_WAVES, iMissionType, iIndex, bHost)
			
			iIndex++
			
		ENDIF
	
		IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, g_sTransitionSessionOptions, iMissionType, ciSURVIVAL_HOST_OPTION_TIME_OF_DAY, bHost)
			tlHostOption = "LOB_TIME_DAY_"		
			tlHostOption += sSelection.iSelection[ciSURVIVAL_HOST_OPTION_TIME_OF_DAY]
			SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciSURVIVAL_HOST_OPTION_TIME_OF_DAY, "LOB_CAT_22", tlHostOption, bHost, bHost)			// Time of Day
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciSURVIVAL_HOST_OPTION_TIME_OF_DAY, iMissionType, iIndex, bHost)
			iIndex++
		ENDIF
			
		IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, g_sTransitionSessionOptions, iMissionType, ciSURVIVAL_HOST_OPTION_WEATHER, bHost)
			tlHostOption = "FMMC_MS_W0_" 
			tlHostOption += sSelection.iSelection[ciSURVIVAL_HOST_OPTION_WEATHER]
			SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciSURVIVAL_HOST_OPTION_WEATHER, "FMMC_MG_GL0", tlHostOption, bHost, bHost)				// Weather
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciSURVIVAL_HOST_OPTION_WEATHER, iMissionType, iIndex, bHost)
			iIndex++
		ENDIF
				
	ELSE
		IF NOT bHost
		AND NOT bInitialiseGlobalClientOptions
			SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, sSelection, bHost)
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, iIndex, bHost)
			
			iIndex++
			
			SETUP_CORONA_WEAPON_MENU_OPTION(coronaMenuData, sSelection, iHighlightIndex, bHost, iIndex, iMissionType, iColumnID)
			
			IF CAN_PLAYER_PURCHASE_ARMOR_IN_CORONA(sSelection, TRUE)
				IF SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, sSelection, bHost)
					DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, iIndex, bHost)
					iIndex++
				ELSE
					PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
				ENDIF
			ELSE
				PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
			ENDIF
			
			IF NOT SHOULD_CLIENT_HAVE_ACCESS_TO_CORONA_INVITES(sLaunchMissionDetails)
				SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, sSelection, bHost)
				DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_VIEW_JOINED_PLAYERS, iMissionType, iIndex, bHost)
				iIndex++
			ENDIF
			
			SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
			
			// Add continue option if needed
			ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)	
			
			bInitialiseGlobalClientOptions = TRUE		
		ENDIF
	ENDIF
	
	// ****************************
	//     SPECIAL CASE OPTIONS
	// ****************************
	
	IF bHost
		IF NOT IS_CORONA_BIT_SET(CORONA_NO_RADIO)
			BOOL bArrows = bHost
			BOOL bSelectable = bHost
		
			IF DOES_RACE_HAVE_A_RADIO_MIX()
				bArrows = FALSE
			ENDIF
		
			SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciALL_LOCAL_RADIO_STATION, "LOB_CAT_13", GET_MY_CHOSEN_RADIO_NAME_SHORT_VERSION(), bArrows, bSelectable)
			DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_RADIO_STATION, iMissionType, iIndex, bHost)
			iIndex++
		ENDIF

		SETUP_CORONA_WEAPON_MENU_OPTION(coronaMenuData, sSelection, iHighlightIndex, bHost, iIndex, iMissionType, iColumnID)
		
		IF CAN_PLAYER_PURCHASE_ARMOR_IN_CORONA(sSelection, TRUE)
			IF SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, sSelection, bHost)
				DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR, iMissionType, iIndex, bHost)
				iIndex++
			ELSE
				PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
			ENDIF
		ELSE
			PROCESS_CORONA_MENU_RESET(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_PURCHASE_ARMOR)
		ENDIF
		
	ENDIF
	
	IF (iMissionType = FMMC_TYPE_DEATHMATCH AND sSelection.iSelection[ciDM_HOST_OPTION_WEAPONS] != ciDM_WEAPON_DEFAULT AND NOT SHOULD_HIDE_FORCED_WEAPONS_OPTION())
	OR (iMissionType = FMMC_TYPE_MISSION AND IS_MISSION_VALID_FOR_FORCED_WEAPON() AND sSelection.iSelection[ciMISSION_HOST_OPTION_WEAPONS] != ciDM_WEAPON_DEFAULT)
	
		SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_WEAPON_LOCKED, iMissionType, sSelection, bHost)					// Weapon Lock
		DEFAULT_CORONA_MENU_SELECTION(coronaMenuData.iCurrentSelection, iHighlightIndex, ciALL_LOCAL_WEAPON_LOCKED, iMissionType, iIndex, bHost)
		iIndex++
	ENDIF
	
	// If we are setting a challenge, give cash option
	//IF (IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
	IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
	AND NOT HAVE_DONE_FIRST_PLAYLIST_MISSION()
	
		// Update our highlighted row
		IF coronaMenuData.iCurrentSelection = ciALL_CREW_CHALLENGE_CASH
			iHighlightIndex = iIndex
		ENDIF
		
		BOOL bArrows = (NOT IS_CORONA_BIT_SET(CORONA_H2H_LOCK_CASH_OPTION))
	
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciALL_CREW_CHALLENGE_CASH,	"FM_ISC_CC", "FM_ISC_CASH", bArrows, bHost, FALSE, g_sCurrentPlayListDetails.iCashBet)
		iIndex++
	ENDIF

	// Display our continue button here
	IF (IS_SCTV_IN_CONTROL_OF_TRANSITION_SESSION() AND IS_PLAYER_SCTV(PLAYER_ID()))
	AND bHost
		SET_UP_OPTIONAL_SCTV_CORONA_OPTION(coronaMenuData, iIndex, iColumnID, iHighlightIndex, iMissionType)
	ELSE
		ADD_CORONA_MENU_CONTINUE_OPTION(coronaMenuData, sLaunchMissionDetails, bContinueSetupAlready, iIndex, iHighlightIndex, bHost, bHideContinue)
	ENDIF
	
	IF CAN_PLAYER_PURCHASE_AMMO_IN_CORONA(sSelection, TRUE)
	OR CAN_PLAYER_PURCHASE_ARMOR_IN_CORONA(sSelection, TRUE)
		IF SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciALL_LOCAL_CASH_OR_AMMO, iMissionType, sSelection, bHost)
			iIndex++
		ENDIF
	ENDIF
	
	// ****************************
	//    DISPLAY UPDATED COLUMN
	// ****************************
	
	// We now always need to display it
	DISPLAY_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE)
	
	IF NOT bHideContinue
		IF (bHost AND NOT IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA())
		OR IS_SELECTION_CLIENT_OPTION(iMissionType, coronaMenuData.iCurrentSelection)
			SET_FRONTEND_HIGHLIGHT(CORONA_COLUMN_ONE, iHighlightIndex, TRUE)
		ELSE
			SET_FRONTEND_COLUMN_FOCUS(CORONA_COLUMN_ONE, FALSE)
		ENDIF
	ENDIF
	
	coronaMenuData.iNumMenuItems = iIndex // Store number of menu items so the mouse detection knows how big the menu is.
	
	PRINTLN("[CORONA] SETUP_CORONA_HOST_MENU_OPTIONS - Total Menu items: ", coronaMenuData.iNumMenuItems)
		
	RELEASE_CONTROL_OF_FRONTEND()
ENDPROC

/// PURPOSE: Returns the participant ID of the host of corona menu
FUNC INT GET_HOST_OF_CORONA_LOBBY(MP_SERVER_VOTE_STRUCT &mpVoteStruct)
	INT iVoteParticipant = -1
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		INT iSpecPlayer = NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
		//If we've got a spectator
		IF iSpecPlayer != -1
			//if they've got a vote
			IF GlobalplayerBD_FM_2[iSpecPlayer].iVoteToUse != -1
				iVoteParticipant = mpVoteStruct.iParticipantInChargeOfVote[GlobalplayerBD_FM_2[iSpecPlayer].iVoteToUse]
			ENDIF
		ENDIF
		RETURN iVoteParticipant
	ELSE
		IF GlobalplayerBD_FM_2[iMyGBD].iVoteToUse != -1
			//calculate who is voting to set it up.
			iVoteParticipant = GET_PLAYER_WHO_IS_VOTING(mpVoteStruct)
		ENDIF
		
		IF GlobalplayerBD_FM_2[iVoteParticipant].iPersonInChargeOfVote != iVoteParticipant
			PRINTLN("[CORONA] GlobalplayerBD_FM_2[iVoteParticipant].iPersonInChargeOfVote = ", iVoteParticipant)
			GlobalplayerBD_FM_2[iVoteParticipant].iPersonInChargeOfVote = iVoteParticipant
		ENDIF
		RETURN iVoteParticipant
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    Initialise our client invites option to TRUE or FALSE for heists only.
PROC INITIALISE_CORONA_CLIENT_INVITE_OPTION(INT iMissionType, INT iMaxParticipants)

	// Default to your tunable
	g_sTransitionSessionData.bTransitionSessionClientInvites = g_sMPTunables.bDefaultClientCoronaInvites
	
	IF IS_CORONA_IN_CHALLENGE_MODE()
	OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
	OR IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
	OR IS_THIS_TRANSITION_SESSION_IS_A_LIVESTREAM_PLAYLIST()
		PRINTLN("INITIALISE_CORONA_CLIENT_INVITE_OPTION - Invalid playlist mode. Default client invite option to OFF")
		g_sTransitionSessionData.bTransitionSessionClientInvites = FALSE
	ENDIF
	
	IF iMaxParticipants <= 2
		PRINTLN("INITIALISE_CORONA_CLIENT_INVITE_OPTION - iMaxParticipants <= 2. Default client invite option to OFF")
		g_sTransitionSessionData.bTransitionSessionClientInvites = FALSE
	ENDIF
	
	IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
		PRINTLN("INITIALISE_CORONA_CLIENT_INVITE_OPTION - This is a heist. Default client invite option to ", PICK_STRING(g_sMPTunables.bDefaultHeistClientCoronaInvites, "ON", "OFF"))
		g_sTransitionSessionData.bTransitionSessionClientInvites = g_sMPTunables.bDefaultHeistClientCoronaInvites
	ENDIF
	
	// If the tunable has been set to TRUE, make sure the clients can not invite
	IF g_sMPTunables.bDisableClientCoronaInvites
		PRINTLN("INITIALISE_CORONA_CLIENT_INVITE_OPTION - Tunable has disabled client invites. Default to OFF")
		g_sTransitionSessionData.bTransitionSessionClientInvites = FALSE
	ENDIF
	
	// Send the parameter to all other players
	IF SHOULD_TRANSITION_SESSION_RUN(iMissionType)
		PRINTLN("INITIALISE_CORONA_CLIENT_INVITE_OPTION - SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(", ciTRANSITION_SESSIONS_CLIENT_INVITES, ", ", PICK_STRING(g_sTransitionSessionData.bTransitionSessionClientInvites, "TRUE", "FALSE"), ")")
		SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_CLIENT_INVITES, PICK_INT(g_sTransitionSessionData.bTransitionSessionClientInvites, 1, 0))
	ENDIF	
ENDPROC

// handles the initialisation of the corona menu
PROC PROCESS_SETUP_OF_CORONA_MENU(	MP_SERVER_VOTE_STRUCT 	&mpVoteStruct,
									INT 					iMissionType,
									INT						iMaxParticipants,
									BOOL 					bIsHeistMission,
									INT						iVoteParticipant,
									INT						iMissionSubType
									, BOOL					bIsHeistPlanning
									, BOOL					bIsHeistPrePlanningCutscene
									 )
									
	IF AM_I_IN_CHARGE_OF_THIS_VOTE(mpVoteStruct)
		PRINTLN("[CORONA] PROCESS_SETUP_OF_CORONA_MENU - setting data to default and initialising content id: ", g_FMMC_STRUCT.iRootContentIDHash)
	
		SETUP_CORONA_HOST_OPTIONS_DEFAULT(iMissionType, g_sTransitionSessionOptions, bIsHeistMission, iMissionSubType , bIsHeistPlanning, bIsHeistPrePlanningCutscene  )
		g_sTransitionSessionOptions.iSelectionChangeID++
		g_sTransitionSessionOptions.iContentIDForMenuItem = g_FMMC_STRUCT.iRootContentIDHash
		
		INITIALISE_CORONA_CLIENT_INVITE_OPTION(iMissionType, iMaxParticipants)
	ELSE
		IF g_sTransitionSessionOptions.iSelectionChangeID = 0
			
			PRINTLN("[CORONA] PROCESS_SETUP_OF_CORONA_MENU - I am client but not had update from HOST. Set local menu options to default")
			SETUP_CORONA_MENU_OPTIONS_DEFAULT(iMissionType, g_sTransitionSessionOptions, iMissionSubType)
			
			
			// If we are on a playlist and the host is active, init our data to theirs.
			IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
				PLAYER_INDEX hostPlayer = INT_TO_PLAYERINDEX(iVoteParticipant)
				
				IF hostPlayer != INVALID_PLAYER_INDEX()
					IF IS_THIS_PLAYER_FULLY_ACTIVE_IN_CORONA(hostPlayer)
					AND GlobalplayerBD_FM[iVoteParticipant].sMenuSelection.iSelectionChangeID > 0
						
						PRINTLN("[CORONA] PROCESS_SETUP_OF_CORONA_MENU - I am client and syncing options to hosts broadcast data")
						g_sTransitionSessionOptions.iSelection = GlobalplayerBD_FM[iVoteParticipant].sMenuSelection.iSelection
					ENDIF
				ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[CORONA] PROCESS_SETUP_OF_CORONA_MENU - I am client and had an update.")
		#ENDIF
			
		ENDIF
		//Set it to what it's current set to
		g_sTransitionSessionOptions.bLobbyAvailability = NETWORK_IS_TRANSITION_OPEN_TO_MATCHMAKING()
		PRINTLN("[CORONA] PROCESS_SETUP_OF_CORONA_MENU - g_sTransitionSessionOptions.bLobbyAvailability = NETWORK_IS_TRANSITION_OPEN_TO_MATCHMAKING() = ", g_sTransitionSessionOptions.bLobbyAvailability)
	ENDIF					
	
ENDPROC


PROC TOGGLE_MISSION_TYPE_SETTINGS_FOR_CORONA()
	IF g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_MISSION_TYPE] = 0
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_TEAM_LIVES_SETTING)
	ENDIF
					
	IF g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_MISSION_TYPE] = 1
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_TEAM_LIVES_SETTING)
	ENDIF
ENDPROC

PROC MAINTAIN_MISSION_TYPE_SETTINGS()
	IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE)
		TOGGLE_MISSION_TYPE_SETTINGS_FOR_CORONA()
	ENDIF	
ENDPROC


PROC TOGGLE_HIGHEST_SCORE_SETTINGS_FOR_CORONA()
	IF g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_HIGHEST_SCORE] = 0
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)
	ENDIF
					
	IF g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_HIGHEST_SCORE] = 1
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)
	ENDIF
ENDPROC

PROC MAINTAIN_HIGHEST_SCORE_SETTINGS()
	IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciTLAD_HIGHEST_SCORE_TOGGLE)
		TOGGLE_HIGHEST_SCORE_SETTINGS_FOR_CORONA()
	ENDIF
ENDPROC

/// PURPOSE: Returns TRUE when the left hand menu has been set up for the player (Host or Client)
FUNC BOOL INITIALISE_CORONA_HOST_OPTION_MENU(	CORONA_MENU_DATA 			&coronaMenuData,
												MISSION_TO_LAUNCH_DETAILS 	&sLaunchMissionDetails,
												MP_SERVER_VOTE_STRUCT 		&mpVoteStruct, 
												INT iMissionType, BOOL &bDefaultData)

	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())

		INT iVoteParticipant = GET_HOST_OF_CORONA_LOBBY(mpVoteStruct)
		
		IF iVoteParticipant != -1
			
			IF bDefaultData
			
				PRINTLN("[CORONA] INITIALISE_CORONA_HOST_OPTION_MENU - bDefaultData = TRUE")
				
				// Set this when we initialise the options
				SET_CORONA_IS_MISSION_COOP()
				
				SET_CORONA_MISSION_OUTFIT_OPTION()
				
				PROCESS_SETUP_OF_CORONA_MENU(mpVoteStruct, iMissionType, GET_MISSION_TYPE_MAX(sLaunchMissionDetails), sLaunchMissionDetails.bIsHeistMission, iVoteParticipant, 
											 sLaunchMissionDetails.iCoronaMissionSubType  , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
				
				IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
				AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE)
						TOGGLE_MISSION_TYPE_SETTINGS_FOR_CORONA()
					ENDIF
				ENDIF
				
				IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
				AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciTLAD_HIGHEST_SCORE_TOGGLE)
						TOGGLE_HIGHEST_SCORE_SETTINGS_FOR_CORONA()
					ENDIF
				ENDIF
				
				SETUP_CORONA_CLIENT_OPTIONS_DEFAULT(iMissionType)
					
				bDefaultData = FALSE
			ENDIF
			
			SETUP_CORONA_HOST_MENU_OPTIONS(coronaMenuData, sLaunchMissionDetails, iMissionType, g_sTransitionSessionOptions, AM_I_IN_CHARGE_OF_THIS_VOTE(mpVoteStruct))
			
			RETURN TRUE
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[CORONA] INITIALISE_CORONA_HOST_OPTION_MENU - iVoteParticipant = -1. Can't initialise data")
		#ENDIF
			
		ENDIF
		
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CORONA_INVITE_SELECTION_VALID(INT iSelection)
	
	SWITCH iSelection
		CASE ciCORONA_LOBBY_ALL_LOCAL_PLAYERS
		CASE ciCORONA_LOBBY_LOCAL_PLAYERS
		CASE ciCORONA_LOBBY_LAST_JOB_PLAYERS
		CASE ciCORONA_LOBBY_LAST_HEIST_PLAYERS
		CASE ciCORONA_LOBBY_FRIENDS
		CASE ciCORONA_LOBBY_CREW_MEMBERS
		CASE ciCORONA_LOBBY_MATCHED_PLAYERS
		CASE ciCORONA_LOBBY_JOINED_PLAYERS
		CASE ciCORONA_LOBBY_H2H_MATCHED_CREWS
		CASE ciCORONA_LOBBY_H2H_FRIEND_CREW
		CASE ciCORONA_LOBBY_PARTY_MEMBERS
		CASE ciALL_HOST_OPTION_AUTOFILL
		CASE ciCORONA_LOBBY_START_GAME
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns highlight position based on state of corona 
FUNC INT CORONA_GET_HIGHLIGHT_OFFSET()
	
	IF IS_CORONA_IN_CHALLENGE_MODE()
	OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
		RETURN ciCORONA_LOBBY_CREW_MEMBERS
	ENDIF
	
	RETURN ciCORONA_LOBBY_ALL_LOCAL_PLAYERS
ENDFUNC

FUNC BOOL IS_CORONA_INVITE_OPTION_VALID(INT iOption , MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails  )
	
	SWITCH iOption
	
		// CHECK THIS THEN FALLTHRU TO OTHER CHECKS
		CASE ciCORONA_LOBBY_LAST_JOB_PLAYERS
		
			IF IS_CORONA_LAST_HEIST_AVAILABLE()
				RETURN FALSE
			ENDIF
		
			IF NOT IS_CORONA_LAST_JOB_AVAILABLE()
				RETURN FALSE
			ENDIF
			
			IF IS_CORONA_IN_CHALLENGE_MODE()
			OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
			OR HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
				RETURN FALSE
			ENDIF
			
			IF IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
				RETURN FALSE
			ENDIF
			
			IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Client_Invites_Last_Job
					PRINTLN("[CORONA] CLIENT INVITE - disabled last job")
					RETURN FALSE
				ENDIF	
			ENDIF
			
		BREAK
				
		CASE ciCORONA_LOBBY_ALL_LOCAL_PLAYERS
			IF IS_CORONA_IN_CHALLENGE_MODE()
			OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
			OR HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
				RETURN FALSE
			ENDIF
			
			IF IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
				RETURN FALSE
			ENDIF
			
			IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Client_Invites_Session
					PRINTLN("[CORONA] CLIENT INVITE - disabled session")
					RETURN FALSE
				ENDIF	
			ENDIF
			
			IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Client_All_Session
					PRINTLN("[CORONA] CLIENT INVITE - disabled all session")
					RETURN FALSE
				ENDIF	
			ELSE
				IF g_sMPTunables.bDisable_Host_All_Session
					PRINTLN("[CORONA] HOST INVITE - disabled all session")
					RETURN FALSE
				ENDIF	
			ENDIF
		BREAK		
		
		CASE ciCORONA_LOBBY_LOCAL_PLAYERS
			IF IS_CORONA_IN_CHALLENGE_MODE()
			OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
			OR HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
				RETURN FALSE
			ENDIF
			
			IF IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
				RETURN FALSE
			ENDIF
			
			IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Client_Invites_Session
					PRINTLN("[CORONA] CLIENT INVITE - disabled session")
					RETURN FALSE
				ENDIF	
			ENDIF
		BREAK
		
		CASE ciCORONA_LOBBY_FRIENDS
			IF IS_CORONA_IN_CHALLENGE_MODE()
			OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
				RETURN FALSE
			ENDIF
			
			IF IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
				RETURN FALSE
			ENDIF
			
			// If we have disabled the friend option for client invite then we should hide this option
			IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Client_Invites_Friends
					PRINTLN("[CORONA] CLIENT INVITE - disabled friends")
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciCORONA_LOBBY_MATCHED_PLAYERS
			IF IS_CORONA_IN_CHALLENGE_MODE()
			OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
				RETURN FALSE
			ENDIF
			
			// We should never see this as a client
			IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				RETURN FALSE
			ENDIF
			
			IF IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciCORONA_LOBBY_JOINED_PLAYERS
			IF IS_CORONA_IN_CHALLENGE_MODE()
			OR IS_CORONA_IN_HEAD_2_HEAD_MODE()
			//OR g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY = FM_SESSION_MENU_CHOICE_JOIN_CLOSED_FRIEND_ONLY_SESSION
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciCORONA_LOBBY_CREW_MEMBERS
			IF NOT IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
			OR g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY = FM_SESSION_MENU_CHOICE_JOIN_CLOSED_FRIEND_ONLY_SESSION
				RETURN FALSE
			ENDIF
			
			IF IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
				RETURN FALSE
			ENDIF
			
			// If we have disabled the crew option for client invite then we should hide this option
			IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Client_Invites_Crews
					PRINTLN("[CORONA] CLIENT INVITE - disabled crews")
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciCORONA_LOBBY_PARTY_MEMBERS
		
			// We no longer need this option for XB1 1945976
			IF IS_XBOX_PLATFORM()
				RETURN FALSE
			ENDIF
		
			IF NOT NETWORK_IS_IN_PLATFORM_PARTY()
				RETURN FALSE				
			ENDIF
			
			// We should never see this as a client
			IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				RETURN FALSE
			ENDIF
			
			IF IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
				RETURN FALSE
			ENDIF
		BREAK
		
		// These 2 options are only valid if we are doing a head to head
		CASE ciCORONA_LOBBY_H2H_FRIEND_CREW
		CASE ciCORONA_LOBBY_H2H_MATCHED_CREWS
			IF NOT IS_CORONA_IN_HEAD_2_HEAD_MODE()
				RETURN FALSE
			ENDIF
			
			IF IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciCORONA_LOBBY_LAST_HEIST_PLAYERS
			IF NOT IS_THIS_MISSION_OF_TYPE_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistMission, sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene)
			AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
				RETURN FALSE
			ELSE
				IF NOT IS_CORONA_LAST_HEIST_AVAILABLE()
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
				RETURN FALSE
			ENDIF
			
			IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
				IF g_sMPTunables.bDisable_Client_Invites_Last_Heist
					PRINTLN("[CORONA] CLIENT INVITE - disabled last heist")
					RETURN FALSE
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	RETURN TRUE	
ENDFUNC

FUNC STRING GET_CORONA_INVITE_OPTION_LABEL(INT iOption)
	
	SWITCH iOption
		CASE ciCORONA_LOBBY_ALL_LOCAL_PLAYERS
			RETURN "FM_LSC_IALP"
		CASE ciCORONA_LOBBY_LOCAL_PLAYERS
			RETURN "FM_LSC_ILP"
		CASE ciCORONA_LOBBY_LAST_JOB_PLAYERS
			RETURN "FM_LSC_LJP"
		CASE ciCORONA_LOBBY_FRIENDS
			RETURN "FM_LSC_IF"
		CASE ciCORONA_LOBBY_MATCHED_PLAYERS
			RETURN "FM_LSC_MP"
		CASE ciCORONA_LOBBY_JOINED_PLAYERS
			RETURN "FM_LSC_JP"
		CASE ciCORONA_LOBBY_CREW_MEMBERS
			RETURN "FM_LSC_ICM"
		CASE ciCORONA_LOBBY_H2H_FRIEND_CREW
			RETURN "FM_LSC_H2H_FC"
		CASE ciCORONA_LOBBY_H2H_MATCHED_CREWS
			RETURN "FM_LSC_H2H_MC"
		CASE ciCORONA_LOBBY_PARTY_MEMBERS
			RETURN "FM_LSC_PARTY"
			
		CASE ciCORONA_LOBBY_LAST_HEIST_PLAYERS
			RETURN "FM_LSC_HEIST"
			
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE: Displays matched crews list
PROC SETUP_CORONA_INVITE_MENU_MATCHED_CREWS(CORONA_MENU_DATA &coronaMenuData, SC_MATCHED_CREWS &scMatchedCrews)
	
	PAUSE_MENU_SET_BUSY_SPINNER(FALSE, CORONA_COLUMN_ONE)
	
	CLEAR_CORONA_BIT(CORONA_HEAD_TO_HEAD_NO_INVITE_DATA)
	CLEAR_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE)
	
	INT i
	INT iCount = 0
	REPEAT MAX_SC_MATCHED_CREWS i
	
		IF scMatchedCrews.CrewData[i].iID != 0
			
			PRINTLN("[CORONA] SETUP_CORONA_INVITE_MENU_MATCHED_CREWS - Found matched crew. crewID = ", scMatchedCrews.CrewData[i].iID, " Name = ", scMatchedCrews.CrewData[i].Name)
			
			SET_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE, iCount, coronaMenuData.CORONA_COLUMN_ONE_ID, scMatchedCrews.CrewData[i].iID, scMatchedCrews.CrewData[i].Name, TRUE)
			
			IF iCount = 0
				PRINTLN("[CORONA] SETUP_CORONA_INVITE_MENU_MATCHED_CREWS - Initial crew is = ", scMatchedCrews.CrewData[i].iID, " Name = ", scMatchedCrews.CrewData[i].Name)
				coronaMenuData.iCurrentInviteSubSelection  = scMatchedCrews.CrewData[i].iID
			ENDIF
			
			iCount++
			
		#IF IS_DEBUG_BUILD	
		ELSE
			PRINTLN("[CORONA] SETUP_CORONA_INVITE_MENU_MATCHED_CREWS - Crew ID invalid. crewID = ", scMatchedCrews.CrewData[i].iID)
		#ENDIF
		ENDIF
	ENDREPEAT
	
	IF iCount = 0
		SET_CORONA_BIT(CORONA_HEAD_TO_HEAD_NO_INVITE_DATA)
		SET_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE, iCount, coronaMenuData.CORONA_COLUMN_ONE_ID, 0, "FM_LSC_NMC")
	ENDIF
	
	DISPLAY_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE)
	SET_FRONTEND_HIGHLIGHT(CORONA_COLUMN_ONE, 0, TRUE)
	SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "")
	
ENDPROC

/// PURPOSE: Displays matched crews list
PROC SETUP_CORONA_INVITE_MENU_FRIENDS_CREWS(CORONA_MENU_DATA &coronaMenuData)
	
	PAUSE_MENU_SET_BUSY_SPINNER(FALSE, CORONA_COLUMN_ONE)
	
	CLEAR_CORONA_BIT(CORONA_HEAD_TO_HEAD_NO_INVITE_DATA)
	CLEAR_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE)
	
	INT i
	INT iCount = 0
	REPEAT FRIEND_CREW_DATA_MAX_CREWS i
	
		IF globalFriendCrewData[i].iID != 0
			
			PRINTLN("[CORONA] SETUP_CORONA_INVITE_MENU_FRIENDS_CREWS - Found friend crew. crewID = ", globalFriendCrewData[i].iID, " Name = ", globalFriendCrewData[i].Name)
			
			SET_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE, iCount, coronaMenuData.CORONA_COLUMN_ONE_ID, globalFriendCrewData[i].iID, globalFriendCrewData[i].Name, TRUE)
			
			IF iCount = 0
				PRINTLN("[CORONA] SETUP_CORONA_INVITE_MENU_FRIENDS_CREWS - Initial crew is = ", globalFriendCrewData[i].iID, " Name = ", globalFriendCrewData[i].Name)
				coronaMenuData.iCurrentInviteSubSelection  = globalFriendCrewData[i].iID
			ENDIF
			
			iCount++
			
		#IF IS_DEBUG_BUILD	
		ELSE
			PRINTLN("[CORONA] SETUP_CORONA_INVITE_MENU_FRIENDS_CREWS - Crew ID invalid. crewID = ", globalFriendCrewData[i].iID)
		#ENDIF
		ENDIF
	ENDREPEAT
	
	IF iCount = 0
		SET_CORONA_BIT(CORONA_HEAD_TO_HEAD_NO_INVITE_DATA)
		SET_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE, iCount, coronaMenuData.CORONA_COLUMN_ONE_ID, 0, "FM_LSC_NFC")
	ENDIF
	
	DISPLAY_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE)
	IF iCount <= 1
		SET_FRONTEND_HIGHLIGHT(CORONA_COLUMN_ONE, 0, TRUE)
	ENDIF
	SET_FRONTEND_HELP_TEXT(CORONA_COLUMN_ONE, "")	
ENDPROC


/// PURPOSE:
///    Returns TRUE if the corona will open for matchmaking in N seconds
FUNC BOOL IS_CORONA_FORCED_MATCHMAKING_ACTIVE()
	
	// Do not process this for non-heists
	IF NOT IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
		RETURN FALSE
	ENDIF
	
	// If the tunable is not set to enable this then exit
	IF NOT g_sMPTunables.bEnableHeistForceMatchmaking
		RETURN FALSE
	ENDIF
	
	// If we have already adjusted the matchmaking option, nothing more to do
	IF IS_CORONA_BIT_SET(CORONA_HEIST_FORCE_MATCHMAKING_SUPPRESSED)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_MATCHMAKING_OPTION(INT iMissionType,  MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)

	IF IS_CORONA_IN_CHALLENGE_MODE()
		RETURN FALSE
	ENDIF
	
	IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
		RETURN FALSE
	ENDIF
	
	IF (IS_THIS_TRANSITION_SESSION_A_PLAYLIST() AND HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION())
		RETURN FALSE
	ENDIF
	
	// Only disable for heists if the matchmaking tunable is not set (2282889)
	IF (IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, sLaunchMissionDetails.bIsHeistMission   , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  ) AND g_sMPTunables.bDisableHeistMatchmakingMenu)
		RETURN FALSE		 
	ENDIF
	
	IF HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
		RETURN FALSE
	ENDIF
	
	IF CV2_IS_THIS_CORONA_A_PROFESSIONAL_JOB()
		RETURN FALSE
	ENDIF
	
	IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
	AND (GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA
	OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA_WALL
	OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_ON_CALL
	OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_END_OF_JOB_SCREEN
	OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NJVS_QM_HOST
	OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NJVS_QM_JOIN)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE: Sets up the first column of the invite page displaying client options if valid
PROC SETUP_CORONA_HOST_INVITE_MENU(CORONA_MENU_DATA &coronaMenuData, MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails, INT iMissionType, BOOL bUpdate, BOOL bHideContinue = FALSE)

	// Could move behaviour of this menu to check if each selection is valid and loop over them
	// See set up of intro screen.

	BOOL bSelectable = TRUE
	INT iHighlight = -1
	INT iHighestIndex = -1

	INT iColumnID = coronaMenuData.CORONA_COLUMN_ONE_ID
	INT iIndex = 0
	
	TEXT_LABEL_15 tlMatchmakingOption
	
	// Monitor our party status (this can be changed at any point, so need to handle the full update of the menu)
	IF IS_CORONA_BIT_SET(CORONA_PLAYER_IS_IN_PARTY)
		IF NOT NETWORK_IS_IN_PLATFORM_PARTY()
			PRINTLN("[CORONA] SETUP_CORONA_HOST_INVITE_MENU - Player no longer in party, FULL UPDATE")
			CLEAR_CORONA_BIT(CORONA_PLAYER_IS_IN_PARTY)
			
			bUpdate = FALSE
		ENDIF
	ELSE
		IF NETWORK_IS_IN_PLATFORM_PARTY()
			PRINTLN("[CORONA] SETUP_CORONA_HOST_INVITE_MENU - Player now in a party, FULL UPDATE")
			SET_CORONA_BIT(CORONA_PLAYER_IS_IN_PARTY)
			
			bUpdate = FALSE
		ENDIF
	ENDIF
	
	// Update our flag to indicate if we have a full lobby (hide invite options)
	IF GET_TOTAL_PLAYERS_IN_CORONA_OR_JOINING(coronaMenuData) >= GET_MISSION_TYPE_MAX(sLaunchMissionDetails)
		IF NOT IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
			PRINTLN("[CORONA] SETUP_CORONA_HOST_INVITE_MENU - We have a full lobby, hide our invite options")
			SET_CORONA_BIT(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
		ENDIF
	ELSE
		IF IS_CORONA_BIT_SET(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
			PRINTLN("[CORONA] SETUP_CORONA_HOST_INVITE_MENU - We do not have a full lobby, hide our invite options")
			CLEAR_CORONA_BIT(CORONA_HIDE_INVITE_FOR_FULL_LOBBY)
		ENDIF
	ENDIF	
	
	IF NOT bUpdate
		CLEAR_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE)
	ENDIF
	
	// Add a matchmaking option
	IF IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
		IF SHOULD_DISPLAY_MATCHMAKING_OPTION(iMissionType, sLaunchMissionDetails)

			tlMatchmakingOption = PICK_STRING(g_sTransitionSessionOptions.bLobbyAvailability, "FM_LSC_OPEN", "FM_LSC_CLS")
			
			// If we are forcing matchmaking then default the display to open
			IF IS_CORONA_FORCED_MATCHMAKING_ACTIVE()
				tlMatchmakingOption = "FM_LSC_OPEN"
			ENDIF
				
			SET_FRONTEND_MENU_OPTION_DATA_SLOT(	CORONA_COLUMN_ONE, iIndex, iColumnID, ciCORONA_LOBBY_MATCHMAKING, "FM_LSC_OP", tlMatchmakingOption, 
												IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU), IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU), bUpdate, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
												DEFAULT, DEFAULT, DEFAULT, DEFAULT, (NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)))
		
			IF coronaMenuData.iCurrentSelection = -1
				coronaMenuData.iCurrentSelection = ciCORONA_LOBBY_MATCHMAKING
			ENDIF
		
			IF coronaMenuData.iCurrentSelection = ciCORONA_LOBBY_MATCHMAKING
				iHighlight = iIndex
			ENDIF
				
			iIndex++
		ENDIF
	ENDIF
	
	IF NOT bHideContinue
		IF IS_CORONA_AUTOFILL_AVAILABLE()
			// Add autofill option
			
			TEXT_LABEL_15 tlAutofill = "FM_LSC_ANC"
			IF IS_CORONA_IN_AUTOFILL_MODE()
				tlAutofill = "FM_LSC_ANCC"
			ENDIF
			
			SET_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciALL_HOST_OPTION_AUTOFILL, tlAutofill, FALSE, DEFAULT, bSelectable)
		
			IF coronaMenuData.iCurrentSelection = -1
				coronaMenuData.iCurrentSelection = ciALL_HOST_OPTION_AUTOFILL
			ENDIF
		
			IF coronaMenuData.iCurrentSelection = ciALL_HOST_OPTION_AUTOFILL
				iHighlight = iIndex
			ENDIF
			
			IF iHighestIndex = -1
				iHighestIndex = ciALL_HOST_OPTION_AUTOFILL
			ENDIF
			
			iIndex++
		ELSE
			IF coronaMenuData.iCurrentSelection = ciALL_HOST_OPTION_AUTOFILL
				coronaMenuData.iCurrentSelection = -1
				iHighlight = -1
			ENDIF
		ENDIF
	ENDIF
	
	
	INT i
	BOOL bSetupLastHeist
	// Loop over our invite options and add any that are valid
	FOR i = ciCORONA_LOBBY_ALL_LOCAL_PLAYERS TO ciCORONA_LOBBY_JOINED_PLAYERS STEP 1
		
		IF IS_CORONA_INVITE_OPTION_VALID(i , sLaunchMissionDetails  )
			
			IF NOT bUpdate
			
					// If last job, add last heist first. If last job is not valid, add it before friends
					IF NOT bSetupLastHeist
						IF IS_CORONA_INVITE_OPTION_VALID(ciCORONA_LOBBY_LAST_HEIST_PLAYERS, sLaunchMissionDetails)
														
							SET_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, 	iColumnID, ciCORONA_LOBBY_LAST_HEIST_PLAYERS, GET_CORONA_INVITE_OPTION_LABEL(ciCORONA_LOBBY_LAST_HEIST_PLAYERS), FALSE, 1, bSelectable)
				
							IF coronaMenuData.iCurrentSelection = -1
								coronaMenuData.iCurrentSelection = ciCORONA_LOBBY_LAST_HEIST_PLAYERS
							ENDIF
							
							IF coronaMenuData.iCurrentSelection = ciCORONA_LOBBY_LAST_HEIST_PLAYERS
								iHighlight = iIndex
							ENDIF
							
							IF iHighestIndex = -1
								iHighestIndex = ciCORONA_LOBBY_LAST_HEIST_PLAYERS
							ENDIF
							
							iIndex++
						ENDIF
					ENDIF
			
				SET_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, 	iColumnID, i,	GET_CORONA_INVITE_OPTION_LABEL(i), FALSE, 1, bSelectable)
				
				IF coronaMenuData.iCurrentSelection = -1
					coronaMenuData.iCurrentSelection = i
				ENDIF
				
				IF coronaMenuData.iCurrentSelection = i
					iHighlight = iIndex
				ENDIF
				
			ELSE
						
					IF i = ciCORONA_LOBBY_ALL_LOCAL_PLAYERS
						IF IS_CORONA_INVITE_OPTION_VALID(ciCORONA_LOBBY_LAST_HEIST_PLAYERS, sLaunchMissionDetails)
							iIndex++
						ENDIF
					ENDIF
			
			ENDIF
			
			bSetupLastHeist = TRUE
			
			IF iHighestIndex = -1
				iHighestIndex = i
			ENDIF

			iIndex++
		ENDIF
		
	ENDFOR

	FMMC_SELECTED_ITEMS sSelection = g_sTransitionSessionOptions
	
	IF iMissionType = FMMC_TYPE_RACE

		IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, sSelection, iMissionType, ciRC_CLIENT_OPTION_PARTNER, FALSE)	
			SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciRC_CLIENT_OPTION_PARTNER, iMissionType, sSelection, TRUE, bUpdate)
			
			IF coronaMenuData.iCurrentSelection = -1
				coronaMenuData.iCurrentSelection = ciRC_CLIENT_OPTION_PARTNER
			ENDIF
			
			IF coronaMenuData.iCurrentSelection = ciRC_CLIENT_OPTION_PARTNER
				iHighlight = iIndex
			ENDIF
			
			iIndex++
		ENDIF
			
		IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, sSelection, iMissionType, ciRC_CLIENT_OPTION_ROLE, FALSE)	
			SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciRC_CLIENT_OPTION_ROLE, iMissionType, sSelection, TRUE, bUpdate)
			
			IF coronaMenuData.iCurrentSelection = -1
				coronaMenuData.iCurrentSelection = ciRC_CLIENT_OPTION_ROLE
			ENDIF
			
			IF coronaMenuData.iCurrentSelection = ciRC_CLIENT_OPTION_ROLE
				iHighlight = iIndex
			ENDIF
			
			iIndex++
		ENDIF
		
	ELIF iMissionType = FMMC_TYPE_MISSION
	
//		IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, sSelection, iMissionType, ciMISSION_CLIENT_OPTION_TEAM, FALSE)
//			SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciMISSION_CLIENT_OPTION_TEAM, iMissionType, sSelection, TRUE, bUpdate)
//			
//			IF coronaMenuData.iCurrentSelection = -1
//				coronaMenuData.iCurrentSelection = ciMISSION_CLIENT_OPTION_TEAM
//			ENDIF
//			
//			IF coronaMenuData.iCurrentSelection = ciMISSION_CLIENT_OPTION_TEAM
//				iHighlight = iIndex
//			ENDIF
//			
//			iIndex++
//		ENDIF

		IF IS_CORONA_MENU_OPTION_VALID(coronaMenuData, sLaunchMissionDetails, sSelection, iMissionType, ciMISSION_CLIENT_OPTION_SEAT, FALSE)
			SET_UP_CORONA_OPTION(coronaMenuData, iColumnID, iIndex, ciMISSION_CLIENT_OPTION_SEAT, iMissionType, sSelection, TRUE, bUpdate)
			
			IF coronaMenuData.iCurrentSelection = -1
				coronaMenuData.iCurrentSelection = ciMISSION_CLIENT_OPTION_SEAT
			ENDIF
			
			IF coronaMenuData.iCurrentSelection = ciMISSION_CLIENT_OPTION_SEAT
				iHighlight = iIndex
			ENDIF
			
			iIndex++
		ENDIF
		
	ENDIF
	
	// Display client invite option
	IF NOT IS_CORONA_IN_CHALLENGE_MODE()
	AND NOT IS_CORONA_IN_HEAD_2_HEAD_MODE()
	AND IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
	//AND GET_TOTAL_PLAYERS_IN_CORONA_OR_JOINING(coronaMenuData) <= 1
	AND NOT g_sMPTunables.bDisableClientCoronaInvites
	AND GET_MISSION_TYPE_MAX(sLaunchMissionDetails) > 2
		
		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciALL_HOST_OPTION_CLIENT_INVITES, "FM_LSC_CINV", PICK_STRING(g_sTransitionSessionData.bTransitionSessionClientInvites, "FM_LSC_CON", "FM_LSC_COFF"), TRUE, bSelectable, bUpdate)
	
		IF coronaMenuData.iCurrentSelection = -1
			coronaMenuData.iCurrentSelection = ciALL_HOST_OPTION_CLIENT_INVITES
		ENDIF
	
		IF coronaMenuData.iCurrentSelection = ciALL_HOST_OPTION_CLIENT_INVITES
			iHighlight = iIndex
		ENDIF
			
		iIndex++	
	ENDIF
		
	// Add a matchmaking option
	IF NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
		IF NOT IS_CORONA_IN_CHALLENGE_MODE()
		AND NOT IS_CORONA_IN_HEAD_2_HEAD_MODE()
		AND NOT (IS_THIS_TRANSITION_SESSION_A_PLAYLIST() AND HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION())
		// Only disable for heists if the matchmaking tunable is not set (2282889)
		AND NOT (IS_THIS_MISSION_OF_TYPE_HEIST(iMissionType, sLaunchMissionDetails.bIsHeistMission   , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
				 AND g_sMPTunables.bDisableHeistMatchmakingMenu)
		AND NOT (GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
				 AND g_sMPTunables.bDisableHeistMatchmakingMenu)
		AND NOT HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()		// Also can not have this option in a launched transition session (heist intro cut)
		AND NOT CV2_WAS_STUNT_RACE_LAUNCHED_AS_PROFESSIONAL()
			
			tlMatchmakingOption = PICK_STRING(g_sTransitionSessionOptions.bLobbyAvailability, "FM_LSC_OPEN", "FM_LSC_CLS")
			
			IF IS_CORONA_FORCED_MATCHMAKING_ACTIVE()
			
				// As a client in this situation
				IF NOT IS_CORONA_BIT_SET(CORONA_HEIST_FORCE_MATCHMAKING_SUPPRESSED)
					IF NETWORK_IS_TRANSITION_OPEN_TO_MATCHMAKING()
						SET_CORONA_BIT(CORONA_HEIST_FORCE_MATCHMAKING_SUPPRESSED)
					ENDIF
				ENDIF
				
				IF NOT IS_CORONA_BIT_SET(CORONA_HEIST_FORCE_MATCHMAKING_SUPPRESSED)
					tlMatchmakingOption = "FM_LSC_OPEN"
				ENDIF
			ENDIF
			
			SET_FRONTEND_MENU_OPTION_DATA_SLOT(	CORONA_COLUMN_ONE, iIndex, iColumnID, ciCORONA_LOBBY_MATCHMAKING, "FM_LSC_OP", tlMatchmakingOption, 
												IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU), IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU), bUpdate, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
												DEFAULT, DEFAULT, DEFAULT, DEFAULT, (NOT IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)))
		
			IF coronaMenuData.iCurrentSelection = -1
				coronaMenuData.iCurrentSelection = ciCORONA_LOBBY_MATCHMAKING
			ENDIF
		
			IF coronaMenuData.iCurrentSelection = ciCORONA_LOBBY_MATCHMAKING
				iHighlight = iIndex
			ENDIF
				
			iIndex++
		ENDIF
	ENDIF
		
	// Handle special case of crew challenge
//	IF IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
//	AND NOT HAVE_DONE_FIRST_PLAYLIST_MISSION()
//	AND IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
//	
//		SET_FRONTEND_MENU_OPTION_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciALL_CREW_CHALLENGE_CASH,	"FM_ISC_CC", "FM_ISC_CASH", TRUE, bSelectable, bUpdate, g_sCurrentPlayListDetails.iCashBet)
//		
//		IF coronaMenuData.iCurrentSelection = -1
//			coronaMenuData.iCurrentSelection = ciALL_CREW_CHALLENGE_CASH
//		ENDIF
//		
//		IF coronaMenuData.iCurrentSelection = ciALL_CREW_CHALLENGE_CASH
//			iHighlight = iIndex
//		ENDIF
//		
//		iIndex++
//	ENDIF
	
	IF NOT bHideContinue
		IF NOT IS_CORONA_BIT_SET(CORONA_PLAYER_IN_SKYSWOOP)
		AND IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
			IF NOT bUpdate
				SET_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE, iIndex, iColumnID, ciCORONA_LOBBY_START_GAME, 		"FM_LSC_SG", FALSE, 2, bSelectable)
			ENDIF
			
			IF coronaMenuData.iCurrentSelection = -1
				coronaMenuData.iCurrentSelection = ciCORONA_LOBBY_START_GAME
			ENDIF
			
			IF coronaMenuData.iCurrentSelection = ciCORONA_LOBBY_START_GAME
				iHighlight = iIndex
			ENDIF
			
			iIndex++
		ENDIF	
	ENDIF
			
	coronaMenuData.iNumMenuItems = iIndex
	
	IF NOT bUpdate
		DISPLAY_FRONTEND_DATA_SLOT(CORONA_COLUMN_ONE)
		IF NOT bHideContinue
		
			// We need to reset our highlight if we were on an option that removed
			IF iHighlight = -1
				iHighlight = 0
				coronaMenuData.iCurrentSelection = iHighestIndex				// Restore our selection to the highest option so it is functioning.
			ENDIF
		
			SET_FRONTEND_HIGHLIGHT(CORONA_COLUMN_ONE, iHighlight, TRUE)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Check if the vehicle class is valid
FUNC BOOL IS_VEHICLE_CLASS_SELECTABLE(INT iClass, INT iMaxValue)
	IF iClass > iMaxValue
		RETURN FALSE
	ENDIF
	IF g_FMMC_STRUCT.iRaceClassBitSet = 0
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_A_RALLY_RACE_TEMP(g_sTransitionSessionOptions)
	OR IS_THIS_CORONA_A_GTA_TEAM_RACE(g_sTransitionSessionOptions)
	OR IS_TARGET_ASSAULT_RACE()
		IF (iClass = FMMC_VEHICLE_CLASS_CYCLES)
		OR (iClass = FMMC_VEHICLE_CLASS_INDUSTRIAL)
			return FALSE
		ENDIF
	ENDIF
	
	#IF NOT FEATURE_GEN9_EXCLUSIVE
	SWITCH iClass
		CASE FMMC_VEHICLE_CLASS_HSW
		CASE FMMC_VEHICLE_CLASS_HSW_T1
		CASE FMMC_VEHICLE_CLASS_HSW_T2
		CASE FMMC_VEHICLE_CLASS_HSW_T3
			RETURN FALSE
	ENDSWITCH
	#ENDIF
	
	IF iClass >= 0
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iRaceClassBitSet, iClass)
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


// ************** PURCHASING AMMO IN CORONA ****************

PROC STORE_CORONA_STARTING_WEAPON(	WEAPON_TYPE aStartingWeapon, WEAPON_TYPE aWeapon, CORONA_MENU_DATA &coronaMenuData, 
									INT &iIndex, INT &iSlotIndex, BOOL &bFoundStartingWeapon, INT &iWeaponSlot)
	
	coronaMenuData.coronaStandardWeapons[iIndex] = aWeapon
	
	IF aStartingWeapon = aWeapon
		// Set this slot number
		coronaMenuData.iWeaponSlot = GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(GET_WEAPONTYPE_SLOT(aWeapon))
		coronaMenuData.coronaWeaponType = CORONA_WEAPON_STANDARD
		coronaMenuData.iStandardWeaponSlot = iIndex
		coronaMenuData.coronaWeaponGroup = GET_WEAPONTYPE_GROUP(aStartingWeapon)
	
		bFoundStartingWeapon = TRUE
		
		PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - Found STANDARD weapon slot = ", coronaMenuData.iWeaponSlot)
		PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - coronaMenuData.iStandardWeaponSlot = ", coronaMenuData.iStandardWeaponSlot)
		PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - coronaMenuData.coronaWeaponGroup = ", ENUM_TO_INT(coronaMenuData.coronaWeaponGroup))
						
		IF NOT IS_CORONA_BIT_SET(CORONA_WEAPON_SAVED_OUT)
			SET_CORONA_STARTING_WEAPON(coronaMenuData)
			SET_CORONA_BIT(CORONA_WEAPON_SAVED_OUT)
		ENDIF
	ELSE
		IF NOT bFoundStartingWeapon
			IF GET_WEAPONTYPE_GROUP(aStartingWeapon) = GET_WEAPONTYPE_GROUP(aWeapon)
				coronaMenuData.iStandardWeaponSlot = (iIndex+1)
			ENDIF
		ENDIF
	ENDIF
	
	iIndex++
	
	IF iWeaponSlot = (iSlotIndex+1)
		PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - Moving slot index up by one so we process minimal amount of weapons: ", iWeaponSlot)
		iSlotIndex = iWeaponSlot
	ENDIF
ENDPROC

/// PURPOSE: Finds a valid starting weapon (attempts to use held or best weapon)
PROC INIT_CORONA_STARTING_WEAPON(CORONA_MENU_DATA &coronaMenuData)

	WEAPON_TYPE aStartingWeapon 
	
	IF NOT IS_CORONA_BIT_SET(CORONA_WEAPON_SAVED_OUT)
	
		aStartingWeapon = GET_CORONA_WEAPON_FROM_INT( GET_PACKED_STAT_INT(PACKED_MP_ACTIVITY_STARTING_WEAPON) )
		PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - Initialise aStartingWeapon = ", GET_WEAPON_NAME(aStartingWeapon))
	
		// Check weapon is valid and the player still has it, if not try and use their current weapon
		IF NOT IS_CORONA_WEAPON_VALID(aStartingWeapon, g_FMMC_STRUCT.iMissionType)
		OR NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), aStartingWeapon)
		
			// Get the currently held weapon
			aStartingWeapon = GET_PLAYER_CURRENT_HELD_WEAPON()

			IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
				IF IS_WEAPON_A_MELEE(aStartingWeapon)
				OR IS_WEAPON_A_THROWING(aStartingWeapon)
				OR aStartingWeapon = WEAPONTYPE_PETROLCAN
					PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - In a heist and the weapon is one we don't want to default to: ", GET_WEAPON_NAME(aStartingWeapon))

					aStartingWeapon = WEAPONTYPE_PISTOL
				ENDIF
			ENDIF
			

			PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - aStartingWeapon = GET_PLAYER_CURRENT_HELD_WEAPON() = ", GET_WEAPON_NAME(aStartingWeapon))

			// current weapon is not valid, get their best weapon
			IF NOT IS_CORONA_WEAPON_VALID(aStartingWeapon, g_FMMC_STRUCT.iMissionType)

				// ...yes, get the best weapon for the ped
				aStartingWeapon = GET_BEST_PED_WEAPON(PLAYER_PED_ID(), TRUE)
				PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - Invalid startin weapon: GET_BEST_PED_WEAPON() = ", GET_WEAPON_NAME(aStartingWeapon))
				
				// Is the weapon still invalid
				IF NOT IS_CORONA_WEAPON_VALID(aStartingWeapon, g_FMMC_STRUCT.iMissionType)
				
					//...yes, set it to unarmed (probably should not let them scroll)
					aStartingWeapon = WEAPONTYPE_UNARMED
					coronaMenuData.iWeaponSlot = -1
					coronaMenuData.coronaWeaponType = CORONA_WEAPON_STANDARD
					coronaMenuData.iStandardWeaponSlot = 0
					coronaMenuData.coronaWeaponGroup = GET_WEAPONTYPE_GROUP(aStartingWeapon)
					PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - Invalid starting weapon: aStartWeapon = WEAPONTYPE_UNARMED ")
					EXIT
				ENDIF
			
			ENDIF
		
		ENDIF
	
	// ... already stored the weapon type so use that instead
	ELSE
		aStartingWeapon = g_myStartingWeapon
	
	ENDIF
	
	INT iIndex
	INT iWeaponGroup
	INT iWeaponSlot
	INT iSlotIndex
	BOOL bFoundStartingWeapon
//	BOOL bStoredUnarmed
	
	WEAPON_TYPE aWeapon
	WEAPON_GROUP aWeaponGroup

	IF NOT IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
		STORE_CORONA_STARTING_WEAPON(aStartingWeapon, WEAPONTYPE_UNARMED, coronaMenuData, iIndex, iSlotIndex, bFoundStartingWeapon, iWeaponSlot)
	ENDIF

	#IF IS_DEBUG_BUILD
	BOOL bWeaponDebug
	#ENDIF

	// Group our weapons together
	FOR iWeaponGroup = 0 TO (NUM_PLAYER_PED_WEAPON_GROUPS-1) STEP 1

		// Grab the current weapon group we are processing
		aWeaponGroup = GET_PLAYER_PED_WEAPON_GROUP_FROM_INT(iWeaponGroup)

		// Loop through the weapons
		FOR iWeaponSlot = iSlotIndex TO (ENUM_TO_INT(NUM_WEAPONSLOTS)-1) STEP 1
			
			IF GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot) = WEAPONSLOT_INVALID
				aWeapon = WEAPONTYPE_INVALID
			ELSE
				aWeapon = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot))
			ENDIF
			
			// Is the weapon valid?
			IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType)
			
				//...yes, is it the weapon group we are processing?
				IF GET_WEAPONTYPE_GROUP(aWeapon) = aWeaponGroup
				
					//...yes, store it in our array
					PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - Storing weapon: ", GET_WEAPON_NAME(aWeapon), " in slot: ", iIndex)
					STORE_CORONA_STARTING_WEAPON(aStartingWeapon, aWeapon, coronaMenuData, iIndex, iSlotIndex, bFoundStartingWeapon, iWeaponSlot)
				ENDIF
				
				
				
			#IF IS_DEBUG_BUILD
			ELSE
			
				IF NOT bWeaponDebug
					IF aWeapon != WEAPONTYPE_INVALID
						PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - Weapon: ", GET_WEAPON_NAME(aWeapon), " in slot: ", iWeaponSlot, " is not valid:")
						PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - IS_WEAPON_A_MELEE(): ", PICK_STRING(IS_WEAPON_A_MELEE(aWeapon), "TRUE", "FALSE") )
						PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - IS_MP_WEAPON_UNLOCKED(): ", PICK_STRING(IS_MP_WEAPON_UNLOCKED(aWeapon), "TRUE", "FALSE") )
						PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - HAS_PED_GOT_WEAPON(): ", PICK_STRING(HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), aWeapon), "TRUE", "FALSE") )
						IF aWeapon != WEAPONTYPE_DLC_RAILGUN
							PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - IS_MP_WEAPON_PURCHASED(): ", PICK_STRING(IS_MP_WEAPON_PURCHASED(aWeapon), "TRUE", "FALSE") )
						ENDIF
					ENDIF
				ENDIF
			#ENDIF
			
			ENDIF
		ENDFOR
		
		#IF IS_DEBUG_BUILD
			bWeaponDebug = TRUE
		#ENDIF
		
	ENDFOR
	
	// Invalidate remaining weapon slots
	FOR iSlotIndex = iIndex TO (ENUM_TO_INT(NUM_WEAPONSLOTS)-1) STEP 1
		coronaMenuData.coronaStandardWeapons[iSlotIndex] = WEAPONTYPE_INVALID
	ENDFOR

	// Temp print out of it all
	#IF IS_DEBUG_BUILD
		FOR iSlotIndex = 0 TO (ENUM_TO_INT(NUM_WEAPONSLOTS)-1) STEP 1
			
			PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - Weapon[", iSlotIndex, "] = ", GET_WEAPON_NAME(coronaMenuData.coronaStandardWeapons[iSlotIndex]), " - ", GET_STRING_FROM_TEXT_FILE(GET_WEAPON_NAME(coronaMenuData.coronaStandardWeapons[iSlotIndex])))
			
		ENDFOR
	#ENDIF
	
	// Process our DLC weapons	
	scrShopWeaponData sWeaponData
	INT i
	FOR i = 0 TO (GET_NUM_DLC_WEAPONS()-1) STEP 1
		
		IF GET_DLC_WEAPON_DATA(i, sWeaponData)
				
			IF aStartingWeapon = INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash)
				
				// Set this slot number
				coronaMenuData.iWeaponSlot = ENUM_TO_INT(NUM_WEAPONSLOTS) + i
				coronaMenuData.coronaWeaponType = CORONA_WEAPON_DLC
				coronaMenuData.iStandardWeaponSlot = i
				coronaMenuData.coronaWeaponGroup = GET_WEAPONTYPE_GROUP(aStartingWeapon)
				
				PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - Found DLC weapon slot = ", ENUM_TO_INT(NUM_WEAPONSLOTS) + i)
				PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - coronaMenuData.iStandardWeaponSlot = ", coronaMenuData.iStandardWeaponSlot)
				PRINTLN("[CORONA] INIT_CORONA_STARTING_WEAPON - coronaMenuData.coronaWeaponGroup = ", ENUM_TO_INT(coronaMenuData.coronaWeaponGroup))
				
				IF NOT IS_CORONA_BIT_SET(CORONA_WEAPON_SAVED_OUT)
					SET_CORONA_STARTING_WEAPON(coronaMenuData)
					SET_CORONA_BIT(CORONA_WEAPON_SAVED_OUT)
				ENDIF
				
			ENDIF
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC SET_CORONA_FULL_AMMO_AMOUNT(CORONA_MENU_DATA &coronaMenuData)
	
	INT iBSCheckedAmmoTypes
	
	INT iWeaponSlot
	INT iWeaponAmmoCost
	WEAPON_TYPE aWeapon
	GUNCLUB_WEAPON_DATA_STRUCT sWeaponStruct
	
	coronaMenuData.iTotalCostToFillAmmo = 0
	
	// Loop over our normal weapons
	FOR iWeaponSlot = 0 TO (ENUM_TO_INT(NUM_WEAPONSLOTS)-1) STEP 1
		
		// Get the weapon type from the slot
		IF GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot) = WEAPONSLOT_INVALID
			aWeapon = WEAPONTYPE_INVALID
		ELSE
			aWeapon = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot))
		ENDIF
		
		// Is the weapon valid for the corona?
		IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType, TRUE)
			
			//...yes, get the total cost for ammo type
			IF GET_CORONA_WEAPON_TOTAL_AMMO_COST(aWeapon, sWeaponStruct, iBSCheckedAmmoTypes, iWeaponAmmoCost)
				
				//...yes, add the total cost to overall value
				coronaMenuData.iTotalCostToFillAmmo += iWeaponAmmoCost
						
				PRINTLN("[CORONA] SET_CORONA_FULL_AMMO_AMOUNT			- Current Total Cost: ", coronaMenuData.iTotalCostToFillAmmo)
			ELSE
				PRINTLN("[CORONA] SET_CORONA_FULL_AMMO_AMOUNT			- total cost not available" )
			ENDIF
		ELSE
			PRINTLN("[CORONA] SET_CORONA_FULL_AMMO_AMOUNT			- invalid weapon" )
		ENDIF
	ENDFOR
	
	// Process our DLC weapons in the same way	
	scrShopWeaponData sWeaponData
	FOR iWeaponSlot = 0 TO (GET_NUM_DLC_WEAPONS()-1) STEP 1
		
		IF GET_DLC_WEAPON_DATA(iWeaponSlot, sWeaponData)
				
			IF IS_CORONA_WEAPON_VALID(INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash), g_FMMC_STRUCT.iMissionType, TRUE)
				
				IF GET_CORONA_WEAPON_TOTAL_AMMO_COST(INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash), sWeaponStruct, iBSCheckedAmmoTypes, iWeaponAmmoCost)
				
					//...yes, add the total cost to overall value
					coronaMenuData.iTotalCostToFillAmmo += iWeaponAmmoCost
							
					PRINTLN("[CORONA] SET_CORONA_FULL_AMMO_AMOUNT			- Current Total Cost: ", coronaMenuData.iTotalCostToFillAmmo)
				ENDIF
					
			ENDIF
		ENDIF
	ENDFOR
	
	IF coronaMenuData.iTotalCostToFillAmmo <= 0
		SET_CORONA_BIT(CORONA_FULL_AMMO_NO_LONGER_AVAILABLE)
	ENDIF
	
	PRINTLN("[CORONA] SET_CORONA_FULL_AMMO_AMOUNT - Total Cost: ", coronaMenuData.iTotalCostToFillAmmo)
	
ENDPROC

/// PURPOSE: Returns FALSE is some other option is disabling current selection
FUNC BOOL IS_CLIENT_ALLOWED_TO_CHANGE_OPTION(INT iMissionType, INT iCurrentSelection, FMMC_SELECTED_ITEMS &sSelection)

	// If the player has an invalid selection, ignore input
	IF iCurrentSelection = -1
		RETURN FALSE
	ENDIF

	IF iCurrentSelection = ciALL_LOCAL_RADIO_STATION
	AND DOES_RACE_HAVE_A_RADIO_MIX()
		RETURN FALSE
	ENDIF

	IF iCurrentSelection = ciALL_LOCAL_PURCHASE_AMMO
	OR iCurrentSelection = ciALL_LOCAL_PURCHASE_ARMOR
	OR iCurrentSelection = ciALL_LOCAL_WEAPON_FILL_ALL
	OR iCurrentSelection = ciALL_LOCAL_VIEW_JOINED_PLAYERS
	OR iCurrentSelection = ciALL_HOST_OPTION_CONTINUE
		RETURN FALSE
	ENDIF

	SWITCH iMissionType		
		CASE FMMC_TYPE_RACE
		//CASE FMMC_TYPE_TRIATHLON
			IF NOT IS_THIS_A_RALLY_RACE_TEMP(sSelection)
			AND NOT IS_THIS_A_GTA_RACE_TEMP(sSelection)
			AND NOT IS_TARGET_ASSAULT_RACE()
				IF iCurrentSelection = ciRC_CLIENT_OPTION_PARTNER
				OR iCurrentSelection = ciRC_CLIENT_OPTION_ROLE
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
	// Can not change the invite options. Simply enter
	IF GET_CORONA_STATUS() = CORONA_STATUS_LOBBY
	OR GET_CORONA_STATUS() = CORONA_STATUS_H2H_LOBBY
		SWITCH iCurrentSelection
			CASE ciCORONA_LOBBY_ALL_LOCAL_PLAYERS
			CASE ciCORONA_LOBBY_LOCAL_PLAYERS    
			CASE ciCORONA_LOBBY_LAST_JOB_PLAYERS 
			CASE ciCORONA_LOBBY_FRIENDS          
			CASE ciCORONA_LOBBY_CREW_MEMBERS     
			CASE ciCORONA_LOBBY_MATCHED_PLAYERS  
			CASE ciCORONA_LOBBY_H2H_FRIEND_CREW  
			CASE ciCORONA_LOBBY_H2H_MATCHED_CREWS
			CASE ciCORONA_LOBBY_PARTY_MEMBERS    
			CASE ciCORONA_LOBBY_JOINED_PLAYERS      
			CASE ciCORONA_LOBBY_START_GAME       
			CASE ciCORONA_LOBBY_LAST_HEIST_PLAYERS
				RETURN FALSE
		ENDSWITCH
	ENDIF
	
	RETURN TRUE
ENDFUNC

//FUNC BOOL IS_SELECTION_LOBBY_AVAILABILITY(INT iMissionType, INT iCurrentSelection)
//	
//	SWITCH iMissionType
//		
//		CASE FMMC_TYPE_RACE				RETURN (iCurrentSelection = ciRC_HOST_OPTION_LOBBY)
//		CASE FMMC_TYPE_DEATHMATCH		RETURN (iCurrentSelection = ciDM_HOST_OPTION_LOBBY)
//		CASE FMMC_TYPE_MISSION			RETURN (iCurrentSelection = ciMISSION_HOST_OPTION_LOBBY)
//		CASE FMMC_TYPE_MG_TENNIS		RETURN (iCurrentSelection = ciTENNIS_HOST_OPTION_LOBBY)
//		CASE FMMC_TYPE_MG_GOLF			RETURN (iCurrentSelection = ciGOLF_HOST_OPTION_LOBBY)
//		CASE FMMC_TYPE_MG_DARTS			RETURN (iCurrentSelection = ciDARTS_HOST_OPTION_LOBBY)
//		DEFAULT
//			RETURN (iCurrentSelection = 0)
//	ENDSWITCH
//	
//	RETURN FALSE
//ENDFUNC


FUNC INT GET_CORONA_CASH_DELTA_VALUE()
//	IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), cashTimer)) < 1000
//		RETURN 5
//	ELIF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), cashTimer)) < 2000
//		RETURN 10
//	ELIF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), cashTimer)) < 3000
//		RETURN 50
//	ELIF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), cashTimer)) < 4000
//		RETURN 100
//	ELIF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), cashTimer)) < 5000
//		RETURN 500
//	ENDIF
	
	RETURN 100
ENDFUNC

/// PURPOSE: Update the cash the player is wagering on the challenge
PROC UPDATE_CORONA_CREW_CHALLENGE_CASH(CORONA_MENU_DATA &coronaMenuData, INT iIncrement)
	IF NOT IS_CORONA_BIT_SET(CORONA_ADJUSTING_CASH_VALUE)
		SET_CORONA_BIT(CORONA_ADJUSTING_CASH_VALUE)
		coronaMenuData.coronaCashTimer = GET_NETWORK_TIME()
	ENDIF

	//The crew challenge isn't used any more, so GET_LOCAL_PLAYER_VC_AMOUNT doesn't need to be fixed here.
	INT iDeltaAmount = GET_CORONA_CASH_DELTA_VALUE() * iIncrement
	INT iMaxCashToBet = (GET_LOCAL_PLAYER_VC_AMOUNT(TRUE) - (g_sCurrentPlayListDetails.iLength * g_sMPTunables.iPLAYLIST_ENTRY_FEE))
	
	g_sCurrentPlayListDetails.iCashBet += iDeltaAmount
	
	INT iMaxWager
	IF IS_CORONA_IN_HEAD_2_HEAD_MODE()
		IF iMaxCashToBet > g_sMPTunables.iMaxCashWagerForHeadToHead
			iMaxWager = g_sMPTunables.iMaxCashWagerForHeadToHead
		ELSE
			iMaxWager = iMaxCashToBet
		ENDIF
	ELSE
		IF iMaxCashToBet > g_sMPTunables.iMaxCashWagerForCrewChallenges
			iMaxWager = g_sMPTunables.iMaxCashWagerForCrewChallenges
		ELSE
			iMaxWager = iMaxCashToBet
		ENDIF
	ENDIF

	IF g_sCurrentPlayListDetails.iCashBet < ciMIN_CHALLENGE_CASH_BET
		g_sCurrentPlayListDetails.iCashBet = iMaxWager
	ENDIF
	
	IF g_sCurrentPlayListDetails.iCashBet > iMaxWager
		g_sCurrentPlayListDetails.iCashBet  = ciMIN_CHALLENGE_CASH_BET
	ENDIF
	
	// Update our broadcast data so it is available to invites
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPlaylistWager = g_sCurrentPlayListDetails.iCashBet
		
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYLIST_CASH_BET, g_sCurrentPlayListDetails.iCashBet)
	
ENDPROC


/// PURPOSE: Limit holes based on other values in the menu
PROC UPDATE_CORONA_GOLF_HOLES(CORONA_MENU_DATA &coronaMenuData, INT iMissionType, FMMC_SELECTED_ITEMS &sSelection, INT iIncrement, MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)

	INT iBound1, iBound2

	// Only allow start hole to go up to the end hole
	IF coronaMenuData.iCurrentSelection = ciGOLF_HOST_OPTION_START_HOLE
		iBound1 = 0
		iBound2 = sSelection.iSelection[ciGOLF_HOST_OPTION_NUM_OF_HOLES]
	ELSE
		iBound1 = sSelection.iSelection[ciGOLF_HOST_OPTION_START_HOLE]
		iBound2 = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
	ENDIF

	sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
	
	IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iBound1
		sSelection.iSelection[coronaMenuData.iCurrentSelection] = iBound2
	ELIF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iBound2
		sSelection.iSelection[coronaMenuData.iCurrentSelection] = iBound1		
	ENDIF
	
ENDPROC

PROC GET_CORONA_NEXT_VALID_CLOTHING_SETUP(CORONA_MENU_DATA &coronaMenuData, INT iIncrement, FMMC_SELECTED_ITEMS &sSelection, MISSION_TO_LAUNCH_DETAILS	&sLaunchMissionDetails)

	BOOL bFound 
	INT iCurrentValue = sSelection.iSelection[coronaMenuData.iCurrentSelection]
	INT iMinCount = GET_HOST_SELECTION_MIN(g_FMMC_STRUCT.iMissionType, coronaMenuData.iCurrentSelection)
	INT iMaxCount = GET_HOST_SELECTION_MAX(g_FMMC_STRUCT.iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
	
	WHILE NOT bFound
	
		sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
		
		IF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iMaxCount
			sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
		ENDIF
		IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
			sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMaxCount
		ENDIF
		
		IF sSelection.iSelection[coronaMenuData.iCurrentSelection] = iCurrentValue
			bFound = TRUE
		ELSE
			IF IS_CORONA_MISSION_OUTFIT_SETUP_VALID(sSelection.iSelection[coronaMenuData.iCurrentSelection])
				bFound = TRUE
			ENDIF		
		ENDIF	
	ENDWHILE
	
ENDPROC


/// PURPOSE:
///    Returns TRUE if the destroy loser option is valid for the current race setup
FUNC BOOL IS_CORONA_DESTROY_LOSER_OPTION_VALID(INT iOption)
	
	IF iOption = ciCORONA_DESTROY_VEHICLE_LAP
	AND IS_THIS_RACE_A_POINT_2_POINT_RACE()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_TLAD_ROUND_LENGTHS(INT iSelection)
	INT iIncrement = iSelection * 5
	RETURN iIncrement + (iSelection - 2)
ENDFUNC

FUNC BOOL IS_CORONA_PILOT_VALID()

	IF g_iMyPreferedPartner >= 0
	
		GAMER_HANDLE aGamerHandle = GET_TRANSITION_PLAYER_GAMER_HANDLE(g_iMyPreferedPartner)
	
		IF NOT IS_GAMER_HANDLE_VALID(aGamerHandle)
		
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Loops through valid players to find next available partner
PROC FIND_NEXT_PLAYER_FOR_PILOT(INT iIncrement)
	
	BOOL bFoundPartner
								
	WHILE (NOT bFoundPartner)
		
		g_iSVMSelectedPilot += iIncrement
		
		IF g_iSVMSelectedPilot < 0
		
			g_iSVMSelectedPilot = (GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION()-1)
		
		// If we have moved our of array (set to ANY)
		ELIF g_iSVMSelectedPilot >= GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION()
			
			g_iSVMSelectedPilot = 0
			
		// Else move to next player in array
		ENDIF
		
		IF g_iSVMSelectedPilot != -1
			g_ghSVMPilot = GET_TRANSITION_PLAYER_GAMER_HANDLE(g_iSVMSelectedPilot)
			PRINTLN("[CORONA] - FIND_NEXT_PLAYER_FOR_PILOT - Found ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(g_ghSVMPilot)))
			bFoundPartner = TRUE
		ENDIF
				
	ENDWHILE
ENDPROC

PROC MAINTAIN_AGGREGATE_POSITION_AVAILABILITY(BOOL bHostOfCorona, INT iMissionType, CORONA_MENU_DATA &coronaMenuData, FMMC_SELECTED_ITEMS &sSelection)
	IF iMissionType = FMMC_TYPE_RACE
		IF sSelection.iSelection[ciRC_HOST_OPTION_DESTROY_LOSER] != ciCORONA_DESTROY_VEHICLE_OFF
		OR sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_RALLY
		OR (sSelection.iSelection[ciRC_HOST_OPTION_TYPE] = ciRACE_SUB_TYPE_GTA AND sSelection.iSelection[ciRC_HOST_OPTION_GTA_TEAM] = 1)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciDISABLE_AGGREGATE_POSITION_RACE)
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciDISABLE_AGGREGATE_POSITION_RACE)
				coronaMenuData.bAggregateWasPreviouslyAvailable = TRUE
				IF sSelection.iSelection[ciRC_HOST_OPTION_AGGREGATE_POS] = 1
					coronaMenuData.bAggregateCachedValue = TRUE
					
					IF bHostOfCorona
						sSelection.iSelection[ciRC_HOST_OPTION_AGGREGATE_POS] = 0
						SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciRC_HOST_OPTION_AGGREGATE_POS, sSelection.iSelection[ciRC_HOST_OPTION_AGGREGATE_POS])
					ENDIF
				ELSE
					coronaMenuData.bAggregateCachedValue = FALSE
				ENDIF
				
				PRINTLN("[CORONA] - MAINTAIN_AGGREGATE_POSITION_AVAILABILITY - Turning aggregate position off as either destroy last place is on, team GTA selected or in a rally race")
			ENDIF
		ELSE
			IF coronaMenuData.bAggregateWasPreviouslyAvailable = TRUE
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciDISABLE_AGGREGATE_POSITION_RACE)
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciDISABLE_AGGREGATE_POSITION_RACE)
				coronaMenuData.bAggregateWasPreviouslyAvailable = FALSE
				IF coronaMenuData.bAggregateCachedValue = TRUE
				AND bHostOfCorona
					sSelection.iSelection[ciRC_HOST_OPTION_AGGREGATE_POS] = 1
					SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciRC_HOST_OPTION_AGGREGATE_POS, sSelection.iSelection[ciRC_HOST_OPTION_AGGREGATE_POS])
				ENDIF
				
				PRINTLN("[CORONA] - MAINTAIN_AGGREGATE_POSITION_AVAILABILITY - Turning aggregate position on as it was previously available and its now safe")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_TEAM_BALANCING_AVAILABILITY(BOOL bHostOfCorona, INT iMissionType, CORONA_MENU_DATA &coronaMenuData, FMMC_SELECTED_ITEMS &sSelection)
	IF iMissionType = FMMC_TYPE_DEATHMATCH
	AND (IS_ARENA_WARS_JOB()
	OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType))
		// Only change if its the current option
		IF coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_TYPE
			// Team Deathmatch?
			IF sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM
			OR sSelection.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_TEAM_KOTH
				// Setting cached?
				IF coronaMenuData.iDMTeamBalancingSettingCached != -1
					PRINTLN("[CORONA] - MAINTAIN_TEAM_BALANCING_AVAILABILITY - Mode is LAST TEAM STANDING and there was a cached value, setting to ", PICK_STRING((coronaMenuData.iDMTeamBalancingSettingCached = DM_TEAM_BALANCING_ON), "DM_TEAM_BALANCING_ON", "DM_TEAM_BALANCING_OFF"))
					sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL] = coronaMenuData.iDMTeamBalancingSettingCached
					coronaMenuData.iDMTeamBalancingSettingCached = -1
					
					IF bHostOfCorona
						SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciDM_HOST_OPTION_TEAM_BAL, sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL])
					ENDIF
				ELSE
					sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL] = DM_TEAM_BALANCING_OFF
					PRINTLN("[CORONA] - MAINTAIN_TEAM_BALANCING_AVAILABILITY - Mode is LAST TEAM STANDING and there was no previously cached value, setting to DM_TEAM_BALANCING_OFF (Default for TDM)")
					
					IF bHostOfCorona
						SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciDM_HOST_OPTION_TEAM_BAL, sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL])
					ENDIF
				ENDIF
			ELSE
				IF coronaMenuData.iDMTeamBalancingSettingCached = -1
					coronaMenuData.iDMTeamBalancingSettingCached = sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL]
					sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL] = DM_TEAM_BALANCING_ON
					PRINTLN("[CORONA] - MAINTAIN_TEAM_BALANCING_AVAILABILITY - Mode is LAST MAN STANDING, setting to DM_TEAM_BALANCING_ON, cached previous value of ", PICK_STRING((coronaMenuData.iDMTeamBalancingSettingCached = DM_TEAM_BALANCING_ON), "DM_TEAM_BALANCING_ON", "DM_TEAM_BALANCING_OFF"))
					
					IF bHostOfCorona
						SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciDM_HOST_OPTION_TEAM_BAL, sSelection.iSelection[ciDM_HOST_OPTION_TEAM_BAL])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_CHANGEABLE_WEATHER_OPTION(INT iMissionType, INT iCurrentSelection)

	SWITCH iMissionType
		CASE FMMC_TYPE_RACE
			IF iCurrentSelection = ciRC_HOST_OPTION_WEATHER
				RETURN TRUE
			ENDIF
		BREAK
		CASE FMMC_TYPE_DEATHMATCH
			IF iCurrentSelection = ciDM_HOST_OPTION_WEATHER
				RETURN TRUE
			ENDIF
		BREAK
		CASE FMMC_TYPE_MISSION
			IF iCurrentSelection = ciMISSION_HOST_OPTION_WEATHER
				RETURN TRUE
			ENDIF
		BREAK
		CASE FMMC_TYPE_SURVIVAL
			IF iCurrentSelection = ciSURVIVAL_HOST_OPTION_WEATHER
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Deal with host changing options
FUNC BOOL MAINTAIN_CORONA_HOST_OPTIONS_INPUT(CORONA_MENU_DATA &coronaMenuData, INT iMissionType, FMMC_SELECTED_ITEMS &sSelection, MISSION_TO_LAUNCH_DETAILS	&sLaunchMissionDetails)
				
	IF IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
		RETURN FALSE 
	ENDIF
	
	INT iIncrement
	BOOL bButtonPressed
	
	//SET_FRONTEND_PC_MOUSE_SELECT_HIGHLIGHT( coronaMenuData.iCurrentSelection)
	
	//Maintain Ammo Toggle
	IF coronaMenuData.iCurrentSelection = ciALL_LOCAL_PURCHASE_AMMO
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			IF coronaMenuData.bSellFullAmmo = TRUE
				coronaMenuData.bSellFullAmmo = FALSE			
			ELSE
				coronaMenuData.bSellFullAmmo = TRUE
			ENDIF
			
			PRINTLN("[CORONA] Player toggling full ammo purchase option (MAINTAIN_CORONA_HOST_OPTIONS_INPUT)")
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	// If value should change, change it.
	IF SHOULD_SELECTION_BE_INCREMENTED(coronaMenuData.lobbyOptionTimer, iIncrement, bButtonPressed, TRUE, TRUE, -1, FALSE, coronaMenuData.iCurrentSelection)
	
		IF IS_HOST_ALLOWED_TO_CHANGE_OPTION(iMissionType, coronaMenuData.iCurrentSelection, sSelection)
	
			INT iMaxCount, iMinCount
					
			// Is the player editing a Crew Challenge cash amount			
			IF coronaMenuData.iCurrentSelection = ciALL_CREW_CHALLENGE_CASH
				
				// Increment our cash value
				UPDATE_CORONA_CREW_CHALLENGE_CASH(coronaMenuData, iIncrement)
			
			ELIF coronaMenuData.iCurrentSelection = ciALL_LOCAL_RADIO_STATION
			
				// Increment our radio station
				UPDATE_CORONA_RADIO_STATION(iIncrement)
			
			ELIF coronaMenuData.iCurrentSelection = ciALL_LOCAL_WEAPON
			
				// Increment the next valid weapon
				GET_CORONA_NEXT_VALID_WEAPON(coronaMenuData, iIncrement)
			ELIF (iMissionType = FMMC_TYPE_MISSION)
			AND (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_VERSUS OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_COOP)
			AND  (coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_CLOTHING_SETUP)
					
				GET_CORONA_NEXT_VALID_CLOTHING_SETUP(coronaMenuData, iIncrement, sSelection, sLaunchMissionDetails)
				
			ELIF (iMissionType = FMMC_TYPE_MISSION)
			AND (coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_VEHICLE_CLASS)
			
				GET_CORONA_NEXT_VALID_MISSION_VEHICLE_CLASS(coronaMenuData, iIncrement, sSelection)
				
			// Or the host is changing the vehicle class (handle differently)			
			ELIF (iMissionType = FMMC_TYPE_RACE)
			AND  (coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_VEHICLE_CLASS)
				IF (g_FMMC_STRUCT.iRaceClassBitSet != 0)
							
					INT iTempClass
					BOOL bFound
					iTempClass = sSelection.iSelection[coronaMenuData.iCurrentSelection]
					iMaxCount =  GET_SELECTION_MAX_VEHICLE_CLASS(g_FMMC_STRUCT.iRaceType)
					
					// Loop until we find a valid class
					WHILE (NOT bFound)
					
						iTempClass += iIncrement
					
						IF iTempClass = sSelection.iSelection[coronaMenuData.iCurrentSelection]
							bFound = TRUE
						ELSE
						
							IF iTempClass > iMaxCount
								iTempClass = 0
							ENDIF
							
							IF iTempClass < 0
								iTempClass = iMaxCount
							ENDIF
						
							IF IS_VEHICLE_CLASS_SELECTABLE(iTempClass, iMaxCount)
								bFound = TRUE
								
								IF iTempClass != sSelection.iSelection[coronaMenuData.iCurrentSelection]
									PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								ENDIF
							ENDIF
						ENDIF
					
					ENDWHILE					
					
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iTempClass
					
				ENDIF
			
			ELIF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_GOLF)
			AND (coronaMenuData.iCurrentSelection = ciGOLF_HOST_OPTION_START_HOLE OR coronaMenuData.iCurrentSelection = ciGOLF_HOST_OPTION_NUM_OF_HOLES)
				
				UPDATE_CORONA_GOLF_HOLES(coronaMenUData, iMissionType, sSelection, iIncrement, sLaunchMissionDetails)
			
			
			ELIF (iMissionType = FMMC_TYPE_RACE)
			AND  (coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_TYPE)
			
				INT iTempType
				BOOL bFoundType
				iTempType = sSelection.iSelection[coronaMenuData.iCurrentSelection]
				iMaxCount = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
				
				WHILE (NOT bFoundType)
					
					iTempType += iIncrement
				
					IF iTempType = sSelection.iSelection[coronaMenuData.iCurrentSelection]
						bFoundType = TRUE
					
					ELSE				
						IF iTempType > iMaxCount
							iTempType = 0
						ENDIF
						
						IF iTempType < 0
							iTempType = iMaxCount
						ENDIF
					
						IF IS_CORONA_RACE_TYPE_SELECTABLE(iTempType, iMaxCount,coronaMenuData.iTotalCustomVehicles)
							bFoundType = TRUE
							
							IF iTempType != sSelection.iSelection[coronaMenuData.iCurrentSelection]
								PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							ENDIF
						ENDIF
					ENDIF
					
				ENDWHILE
				
				sSelection.iSelection[coronaMenuData.iCurrentSelection] = iTempType
			
			ELIF (iMissionType = FMMC_TYPE_RACE)
			AND  (coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_DESTROY_LOSER)
				
				BOOL bDestroyOptionFound
				INT iCurrentSetting = sSelection.iSelection[coronaMenuData.iCurrentSelection]
				iMinCount = GET_HOST_SELECTION_MIN(iMissionType, coronaMenuData.iCurrentSelection)
				iMaxCount = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
				
				// Loop until we find a valid option
				WHILE (NOT bDestroyOptionFound)
				
					sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
					
					IF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iMaxCount
						sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
					ENDIF
					
					IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
						sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMaxCount
					ENDIF
					
					IF IS_CORONA_DESTROY_LOSER_OPTION_VALID(sSelection.iSelection[coronaMenuData.iCurrentSelection])
					OR sSelection.iSelection[coronaMenuData.iCurrentSelection] = iCurrentSetting
						
						bDestroyOptionFound = TRUE
					ENDIF					
				ENDWHILE
				
				PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
			
			ELIF IS_CHANGEABLE_WEATHER_OPTION(iMissionType, coronaMenuData.iCurrentSelection)
			
				iMinCount = GET_HOST_SELECTION_MIN(iMissionType, coronaMenuData.iCurrentSelection)
				iMaxCount = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
				
				sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iMaxCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
				ENDIF
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMaxCount
				ENDIF
				
				// ciFMMC_WEATHER_OPTION_SNOW, ciFMMC_WEATHER_OPTION_HALLOWEEN and ciFMMC_WEATHER_OPTION_HALLOWEEN_NO_RAIN are dev only, need to skip over them
				
				IF iIncrement > 0
					IF sSelection.iSelection[coronaMenuData.iCurrentSelection] = ciFMMC_WEATHER_OPTION_SNOW
						sSelection.iSelection[coronaMenuData.iCurrentSelection] = ciFMMC_WEATHER_OPTION_SMOG
					ENDIF
					
					IF (sSelection.iSelection[coronaMenuData.iCurrentSelection] = ciFMMC_WEATHER_OPTION_HALLOWEEN
					OR sSelection.iSelection[coronaMenuData.iCurrentSelection] = ciFMMC_WEATHER_OPTION_HALLOWEEN_NO_RAIN)
						sSelection.iSelection[coronaMenuData.iCurrentSelection] = ciFMMC_WEATHER_OPTION_CLEAR
					ENDIF
				ENDIF
				
				IF iIncrement < 0
					IF sSelection.iSelection[coronaMenuData.iCurrentSelection] = ciFMMC_WEATHER_OPTION_SNOW	
						sSelection.iSelection[coronaMenuData.iCurrentSelection] = ciFMMC_WEATHER_OPTION_RAINING	
					ENDIF
					
					IF (sSelection.iSelection[coronaMenuData.iCurrentSelection] = ciFMMC_WEATHER_OPTION_HALLOWEEN_NO_RAIN	
					OR sSelection.iSelection[coronaMenuData.iCurrentSelection] = ciFMMC_WEATHER_OPTION_HALLOWEEN)	
						sSelection.iSelection[coronaMenuData.iCurrentSelection] = ciFMMC_WEATHER_OPTION_SMOG	
					ENDIF
				ENDIF
				
				IF iMinCount != iMaxCount
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			
			ELIF (iMissionType = FMMC_TYPE_DEATHMATCH)
			AND  (coronaMenuData.iCurrentSelection = ciDM_HOST_OPTION_TARGET_SCORE)
			AND  (IS_THIS_A_KING_OF_THE_HILL_CORONA(iMissionType, sSelection.iSelection[ciDM_HOST_OPTION_TYPE]) OR (NOT IS_THIS_LEGACY_DM_CONTENT()))
				
				IF IS_THIS_A_KING_OF_THE_HILL_CORONA(iMissionType, sSelection.iSelection[ciDM_HOST_OPTION_TYPE])
					
					iIncrement *= 10
					
					iMinCount = GET_HOST_SELECTION_MIN(iMissionType, coronaMenuData.iCurrentSelection)
					iMaxCount = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
					
					sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
					
					// Increment
					IF iIncrement > 0
						// If more than max, go to 0 for 'off'
						IF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iMaxCount
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = 0
						// If was on 0 and not at min, go to min
						ELIF sSelection.iSelection[coronaMenuData.iCurrentSelection] > 0 
						AND sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
						ENDIF
					ENDIF
					
					// Decrement
					IF iIncrement < 0
						// If less than min and more than 0, go to 0 for 'off'
						IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
						AND sSelection.iSelection[coronaMenuData.iCurrentSelection] > 0
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = 0
						// If less than 0, go to max
						ELIF sSelection.iSelection[coronaMenuData.iCurrentSelection] < 0
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMaxCount
						ENDIF
					ENDIF
					
					IF iMinCount != iMaxCount
						PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					ENDIF
					
				ELIF NOT IS_THIS_LEGACY_DM_CONTENT()
				
					IF iIncrement > 0
						IF sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_40
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_45
						ELIF sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_150
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_OFF
						ELIF sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_OFF
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_10
						ELSE
							sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
						ENDIF
					ELSE
						IF sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_45
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_40
						ELIF sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_10
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_OFF
						ELIF sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_OFF
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = DM_TARGET_KILLS_150
						ELSE
							sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
						ENDIF
					ENDIF
					IF iMinCount != iMaxCount
						PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					ENDIF
					
				ENDIF
	
			ELIF (iMissionType = FMMC_TYPE_MISSION)
			AND  (coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_TARGET_SCORE)
			OR	 (coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_ROUND_DURATION)
			
				IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType) 
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
					IF (IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType) 
					AND coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_ROUND_DURATION)
						iIncrement *= 2
					ELSE
						iIncrement *= 5
					ENDIF
				ENDIF
					
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
					IF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_TARGET_SCORE
						iIncrement *= 50
					ELIF coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_ROUND_DURATION
						iIncrement *= 5
					ENDIF
				ENDIF
					
				iMinCount = GET_HOST_SELECTION_MIN(iMissionType, coronaMenuData.iCurrentSelection)
				iMaxCount = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
				
				sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iMaxCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
				ENDIF
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMaxCount
				ENDIF
				
				IF iMinCount != iMaxCount
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			
			ELIF (iMissionType = FMMC_TYPE_MISSION)
			AND  (coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_TEAM_LIVES)	
				
				iIncrement *= g_FMMC_STRUCT.iLivesIncrementValue
					
				iMinCount = GET_HOST_SELECTION_MIN(iMissionType, coronaMenuData.iCurrentSelection)
				iMaxCount = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
				sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iMaxCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
				ENDIF
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMaxCount
				ENDIF
				
				IF iMinCount != iMaxCount
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			
			ELIF (iMissionType = FMMC_TYPE_MISSION)
			AND (coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_MISSION_TYPE)
				
				iMinCount = GET_HOST_SELECTION_MIN(iMissionType, coronaMenuData.iCurrentSelection)
				iMaxCount = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
				sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iMaxCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
				ENDIF
				
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMaxCount
				ENDIF
				
				TOGGLE_MISSION_TYPE_SETTINGS_FOR_CORONA()
			
				PRINTLN("[CORONA] [ST] - iMinCount = ", iMinCount, "; iMaxCount = ", iMaxCount)
				PRINTLN("[CORONA] [ST] - sSelection.iSelection[coronaMenuData.iCurrentSelection] = ", sSelection.iSelection[coronaMenuData.iCurrentSelection])
				PRINTLN("[CORONA] [ST] - IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES) = ", GET_STRING_FROM_BOOL(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)))
				PRINTLN("[CORONA] [ST] - IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_TEAM_LIVES_SETTING) = ", GET_STRING_FROM_BOOL(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_TEAM_LIVES_SETTING)))
			
				IF iMinCount != iMaxCount
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			
			ELIF (iMissionType = FMMC_TYPE_MISSION)
			AND (coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_HIGHEST_SCORE)
				
				iMinCount = GET_HOST_SELECTION_MIN(iMissionType, coronaMenuData.iCurrentSelection)
				iMaxCount = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
				sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iMaxCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
				ENDIF
				
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMaxCount
				ENDIF
				
				TOGGLE_HIGHEST_SCORE_SETTINGS_FOR_CORONA()
			
				PRINTLN("[CORONA] [ST] - iMinCount = ", iMinCount, "; iMaxCount = ", iMaxCount)
				PRINTLN("[CORONA] [ST] - sSelection.iSelection[coronaMenuData.iCurrentSelection] = ", sSelection.iSelection[coronaMenuData.iCurrentSelection])
			
				IF iMinCount != iMaxCount
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			
			ELIF (iMissionType = FMMC_TYPE_MISSION)
			AND (coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_NUM_TEAMS)
				
				iMinCount = GET_HOST_SELECTION_MIN(iMissionType, coronaMenuData.iCurrentSelection)
				iMaxCount = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
				sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciAUTO_NUMBER_OF_TEAMS)
					// If incrementing up from -1 to 0 or 1
					IF iIncrement > 0
						IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < g_FMMC_STRUCT.iMinNumberOfTeams
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = g_FMMC_STRUCT.iMinNumberOfTeams
						ENDIF
						
					// If decrementing down to 1 or 0 from 2
					ELIF iIncrement < 0
						IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < g_FMMC_STRUCT.iMinNumberOfTeams
						AND sSelection.iSelection[coronaMenuData.iCurrentSelection] >= iMinCount
							sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
						ENDIF
					ENDIF
				ENDIF
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iMaxCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
				ENDIF
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMaxCount
				ENDIF
				
				IF iMinCount != iMaxCount
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			
			ELIF (iMissionType = FMMC_TYPE_MISSION)
			AND (coronaMenuData.iCurrentSelection = ciMISSION_HOST_OPTION_PILOT)
				FIND_NEXT_PLAYER_FOR_PILOT(iIncrement)
				
				GAMER_HANDLE gHandle = GET_TRANSITION_PLAYER_GAMER_HANDLE(g_iSVMSelectedPilot)
				NETWORK_SEND_TRANSITION_GAMER_INSTRUCTION(gHandle, "", TRANSITION_SESSION_INSTRUCTION_UPDATE_SELECTED_PILOT, 0, TRUE)
				PRINTLN("[CORONA] - MAINTAIN_CORONA_HOST_OPTIONS_INPUT - Sent TRANSITION_SESSION_INSTRUCTION_UPDATE_SELECTED_PILOT request, nInstructionParam = 0")
				
				IF GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION() > 1
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			
			ELIF (iMissionType = FMMC_TYPE_RACE)
			AND  (coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_NUM_LAPS)
				IF IS_THIS_MISSION_ROCKSTAR_CREATED()
				AND g_FMMC_STRUCT.iRaceLobbyLapIncrement > 1
					iIncrement *= g_FMMC_STRUCT.iRaceLobbyLapIncrement
				ENDIF
				
				iMinCount = GET_HOST_SELECTION_MIN(iMissionType, coronaMenuData.iCurrentSelection)
				iMaxCount = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
				sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iMaxCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
				ENDIF
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMaxCount
				ENDIF
				
				IF iMinCount != iMaxCount
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			
			// Else incrememnt our current value.
			ELSE
				
				iMinCount = GET_HOST_SELECTION_MIN(iMissionType, coronaMenuData.iCurrentSelection)
				iMaxCount = GET_HOST_SELECTION_MAX(iMissionType, coronaMenuData.iCurrentSelection, sSelection, sLaunchMissionDetails)
				sSelection.iSelection[coronaMenuData.iCurrentSelection] += iIncrement				
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] > iMaxCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMinCount
				ENDIF
				
				IF sSelection.iSelection[coronaMenuData.iCurrentSelection] < iMinCount
					sSelection.iSelection[coronaMenuData.iCurrentSelection] = iMaxCount
				ENDIF
				
				IF iMinCount != iMaxCount
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			ENDIF
				
			sSelection.iSelectionChangeID++
		
			// Check our partner options are up to date.
			IF (iMissionType = FMMC_TYPE_RACE)
			AND (coronaMenuData.iCurrentSelection = ciRC_HOST_OPTION_TYPE)
				SETUP_CORONA_CLIENT_OPTIONS_DEFAULT(iMissionType)
			ENDIF
			
			IF coronaMenuData.iCurrentSelection != ciALL_CREW_CHALLENGE_CASH
			AND coronaMenuData.iCurrentSelection != ciALL_LOCAL_RADIO_STATION
			AND coronaMenuData.iCurrentSelection != ciALL_LOCAL_WEAPON
				IF SHOULD_TRANSITION_SESSION_RUN(iMissionType)
					PRINTLN("MAINTAIN_CORONA_HOST_OPTIONS_INPUT - SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(", coronaMenuData.iCurrentSelection, ", ", sSelection.iSelection[coronaMenuData.iCurrentSelection], ")")
					SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(coronaMenuData.iCurrentSelection, sSelection.iSelection[coronaMenuData.iCurrentSelection])
				ENDIF
			ENDIF
			
			RETURN TRUE		
		ENDIF
	ELSE
		IF NOT bButtonPressed
			IF IS_CORONA_BIT_SET(CORONA_ADJUSTING_CASH_VALUE)
				CLEAR_CORONA_BIT(CORONA_ADJUSTING_CASH_VALUE)
			ENDIF 
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ********** CLIENT MENU OPTIONS **************

/// PURPOSE: Check the player is able to purchase the ammo / deduct cash and give ammo
FUNC BOOL PROCESS_PLAYER_PURCHASING_CORONA_AMMO(CORONA_MENU_DATA &coronaMenuData, WEAPON_TYPE aStartingWeapon, BOOL bFullAmmo, 
												BOOL bPurchaseAllAmmo = FALSE, BOOL bApplyAmmo = TRUE, BOOL bChargeCash = TRUE)

	// If we are using the basket system and there is already one in process, show help text
	IF USE_SERVER_TRANSACTIONS()
	AND GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
		IF NOT bPurchaseAllAmmo
		AND bChargeCash
			PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Trying to by ammo but already pending a transaction")
			DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_COR_TRAN_P", TRUE, FALSE, TRUE, TRUE)		
			RETURN FALSE
		ENDIF
	ENDIF
	
	BOOL bMK2Weapon = FALSE
	IF IS_WEAPON_MK2(aStartingWeapon)
		bMK2Weapon = TRUE
	ENDIF
	
	GUNCLUB_WEAPON_DATA_STRUCT	sWeaponStruct
	
	IF GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(aStartingWeapon, sWeaponStruct, DEFAULT, bMK2Weapon)
	
		// Make sure weapon is valid and has an ammo type
		IF sWeaponStruct.eType != ITEM_TYPE_INVALID
		AND sWeaponStruct.eWeapon != WEAPONTYPE_INVALID
		AND sWeaponStruct.eAmmoType != INT_TO_ENUM(AMMO_TYPE, 0)
		AND sWeaponStruct.eAmmoType != AMMOTYPE_DLC_RAYPISTOL
		AND sWeaponStruct.eAmmoType != AMMOTYPE_STUNGUN
			
			INT iAmmoInitialCount = sWeaponStruct.iDefaultClipSize

			IF IS_CORONA_WEAPON_THROWN(aStartingWeapon)
				PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Weapon is a thrown type. Give 1 ammo")
				iAmmoInitialCount = 1
				
			ELIF sWeaponStruct.eType = ITEM_TYPE_GUN
				PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Weapon is ITEM_TYPE_GUN. Give 2 * default clip size")
				iAmmoInitialCount *= 2 // 2 clips for a gun
				
			ENDIF
				
			IF CAN_PLAYER_PURCHASE_CORONA_AMMO_FOR_WEAPON(aStartingWeapon, iAmmoInitialCount)

				INT iWeaponCost
				INT iAmmoCost
				INT iMaxAmmo
				BOOL bChargePerUnit
				
				GET_GUNSHOP_WEAPON_COST(aStartingWeapon, iWeaponCost, iAmmoCost, bChargePerUnit)
				
				IF USE_SERVER_TRANSACTIONS()
					IF bChargePerUnit
						iAmmoCost *= iAmmoInitialCount
					ENDIF
				ENDIF
				
				INT iAmmoCount = iAmmoInitialCount

				IF bFullAmmo
					iAmmoCost 	= ROUND(TO_FLOAT(iAmmoCost) * GET_AMMO_MULTIPLIER_FOR_FULL_AMMO(sWeaponStruct.eWeapon, iAmmoInitialCount))
					iAmmoCount	= ROUND(TO_FLOAT(iAmmoInitialCount) * GET_AMMO_MULTIPLIER_FOR_FULL_AMMO(sWeaponStruct.eWeapon, iAmmoInitialCount))
					
					IF GET_MAX_AMMO(PLAYER_PED_ID(), sWeaponStruct.eWeapon, iMaxAmmo)
						IF iMaxAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), sWeaponStruct.eWeapon)
							PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Weapon is FULL. Set iAmmoCost = 0")
							iAmmoCost = 0							
						ENDIF
					ENDIF
					
				ELSE
				
					// Check if we need to adjust the ammo count and cost due to lack of remaining space (will only affect some weapons)
					GET_AMMO_COUNT_AND_COST_FOR_REMAINING_INVENTORY_SPACE( aStartingWeapon, iAmmoCost, iAmmoCount )
				
				ENDIF
				
				BOOL bCanProceedWithPayment = (iAmmoCost > 0)
				PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - iAmmoCost = ", iAmmoCost)
				PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - bCanProceedWithPayment = ", GET_STRING_FROM_BOOL(bCanProceedWithPayment))
				IF g_sMPtunables.bCORONA_AMMO_PURCHASE_ALLOW_ZERO_PAYMENTS
					bCanProceedWithPayment = (iAmmoCost >= 0)
					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Zero payments permitted, bCanProceedWithPayment = ", GET_STRING_FROM_BOOL(bCanProceedWithPayment))
				ENDIF
				IF bCanProceedWithPayment

					BOOL fromBank = (NETWORK_GET_VC_BANK_BALANCE() > 0) 
					BOOL fromBankAndWallet = (NETWORK_GET_VC_BANK_BALANCE() < iAmmoCost)
					
					IF fromBankAndWallet
						PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - fromBankAndWallet is TRUE. NETWORK_CAN_SPEND_MONEY can only have one TRUE param. Set fromBank = FALSE")
						fromBank = FALSE
					ENDIF

					IF GET_CORONA_CASH_AVAILABLE() >= iAmmoCost
					AND NETWORK_CAN_SPEND_MONEY(iAmmoCost, fromBank, fromBankAndWallet, FALSE)
						
						IF USE_SERVER_TRANSACTIONS()
						AND bChargeCash
							TEXT_LABEL_63 tlCatalogueKey, tlCatalogueKeyExtra
							TEXT_LABEL_15 sName
							
							AMMO_TYPE eAmmoType
							GET_WEAPON_AMMO_TYPE_FROM_WEAPON_TYPE( aStartingWeapon, eAmmoType )
							
							IF aStartingWeapon = WEAPONTYPE_GRENADE
							OR aStartingWeapon = WEAPONTYPE_STICKYBOMB
							OR aStartingWeapon = WEAPONTYPE_SMOKEGRENADE
							OR aStartingWeapon = WEAPONTYPE_DLC_PROXMINE
							OR aStartingWeapon = WEAPONTYPE_DLC_PIPEBOMB
								sName = GET_WEAPON_NAME( aStartingWeapon )
								GENERATE_KEY_FOR_SHOP_CATALOGUE( tlCatalogueKeyExtra, sName, GET_ENTITY_MODEL( PLAYER_PED_ID() ), SHOP_TYPE_GUN, ENUM_TO_INT(ITEM_TYPE_NADE), 0, GET_WEAPON_ALT_NUM_FOR_CATALOG_KEY(aStartingWeapon))
								GENERATE_KEY_FOR_SHOP_CATALOGUE( tlCatalogueKey, sName, GET_ENTITY_MODEL( PLAYER_PED_ID() ), SHOP_TYPE_GUN, ENUM_TO_INT(ITEM_TYPE_NADE), 1, GET_WEAPON_ALT_NUM_FOR_CATALOG_KEY(aStartingWeapon))
							ELSE
								GET_GUNCLUB_WEAPON_AMMO_TEXT( eAmmoType, iAmmoCount, sName )
								GENERATE_KEY_FOR_SHOP_CATALOGUE( tlCatalogueKey, sName, GET_ENTITY_MODEL( PLAYER_PED_ID() ), SHOP_TYPE_GUN, 0, GET_WEAPON_INDEX_FROM_WEAPON_TYPE( aStartingWeapon ), GET_WEAPON_ALT_NUM_FOR_CATALOG_KEY(aStartingWeapon))
							ENDIF
						
							INT iBasketQuantity 
							IF bChargePerUnit
								iBasketQuantity = iAmmoCount
							ELSE
								IF bFullAmmo
									iBasketQuantity = FLOOR( GET_AMMO_MULTIPLIER_FOR_FULL_AMMO( aStartingWeapon, iAmmoInitialCount ) )
								ELSE
									iBasketQuantity = 1
								ENDIF
							ENDIF

							PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Purchasing ammo, key='", tlCatalogueKey, "'")
							PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - iBasketQuantity = ", iBasketQuantity, ", iAmmoCost = ", iAmmoCost, ", iAmmoCount = ", iAmmoCount, ", iAmmoInitialCount = ", iAmmoInitialCount)
						
							IF NETWORK_REQUEST_BASKET_TRANSACTION( NET_SHOP_TTYPE_BASKET, CATEGORY_WEAPON_AMMO, GET_HASH_KEY( tlCatalogueKey ), NET_SHOP_ACTION_SPEND, iBasketQuantity, (iAmmoCost/iBasketQuantity), 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, DEFAULT, CTPF_AUTO_RETRY )
								IF GET_HASH_KEY( tlCatalogueKeyExtra ) != 0
									NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_WEAPON, GET_HASH_KEY( tlCatalogueKeyExtra ), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, DEFAULT, CTPF_AUTO_RETRY )
								ENDIF
							
								// When purchasing all ammo do not check out, done by the calling function
								IF NOT bPurchaseAllAmmo
								
									// Cache our data so we can give this ammo later
									g_sAmmoArmorCashTransaction.wtWeapon = aStartingWeapon
									
									IF bFullAmmo
										SET_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE(AMMO_ARMOR_CASH_TRANSACTION_FULL_CLIP_AMMO)
									ELSE
										SET_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE(AMMO_ARMOR_CASH_TRANSACTION_SINGLE_CLIP_AMMO)
									ENDIF
								
									NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
								ENDIF
							ENDIF						
						ELSE
							IF bChargeCash
							OR (USE_SERVER_TRANSACTIONS() AND NOT bChargeCash)		// We need to run the buy item telemetry on successful transaction

								TEXT_LABEL_15 tlAmmoType = GET_CORONA_AMMO_STRING_FOR_TELEMETRY(sWeaponStruct.eAmmoType, iAmmoCount, bFullAmmo)
																
								PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Telemetry: 	Player Cash: 		$", GET_PLAYER_CASH(PLAYER_ID()))
								PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - 				- amount: 			$", iAmmoCost)
								PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - 				- itemHash:			", ENUM_TO_INT(aStartingWeapon))
								PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - 				- extra1:			", iAmmoCount)
								
								// We must set up the nonce seed for a PC transaction when we are calling telemetry
								IF (USE_SERVER_TRANSACTIONS() AND NOT bChargeCash)
									PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Set up nonce seed for our ID: ", GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
									NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
								ENDIF
								
								IF bPurchaseAllAmmo
									PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - 				- itemIdentifier:	FM_COR_AMMO_FILL")
									NETWORK_BUY_ITEM(	iAmmoCost, ENUM_TO_INT(aStartingWeapon), PURCHASE_WEAPONAMMO, iAmmoCount, fromBank, 
														"FM_COR_AMMO_FILL", GET_HASH_KEY("SHOP_CORONA"), DEFAULT, DEFAULT, fromBankAndWallet)
								ELSE
									PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - 				- itemIdentifier:	", tlAmmoType)
									NETWORK_BUY_ITEM(	iAmmoCost, ENUM_TO_INT(aStartingWeapon), PURCHASE_WEAPONAMMO, iAmmoCount, fromBank, 
														tlAmmoType, GET_HASH_KEY("SHOP_CORONA"), DEFAULT, DEFAULT, fromBankAndWallet)
								ENDIF
							ELSE
								PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - bChargeCash = FALSE, SKIP purchasing")
							ENDIF
						ENDIF
						
						// Only apply the ammo if it is flagged to do so
						IF bApplyAmmo
							ADD_AMMO_TO_PED(PLAYER_PED_ID(), aStartingWeapon, iAmmoCount)
							CHECK_AND_SAVE_WEAPON(aStartingWeapon)
						ENDIF
						
						IF bChargeCash
							coronaMenuData.iTotalCostToFillAmmo -= iAmmoCost
						ENDIF
						
						IF coronaMenuData.iTotalCostToFillAmmo <= CORONA_PURCHASE_ALL_AMMO_REFRESH_THRESHOLD
							SET_CORONA_FULL_AMMO_AMOUNT(coronaMenuData)
							PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Re-calculating full ammo amount so we have accurate cost")
						ENDIF
						
						IF coronaMenuData.iTotalCostToFillAmmo <= 0
							coronaMenuData.iTotalCostToFillAmmo = 0
							SET_CORONA_BIT(CORONA_FULL_AMMO_NO_LONGER_AVAILABLE)
							PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Player has full ammo, remove option")
						ENDIF
						
						PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Purchased: Ammo: ", iAmmoCount, ", Remaining Cash: $", GET_PLAYER_CASH(PLAYER_ID()))
						PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Purchase all ammo is now: ", coronaMenuData.iTotalCostToFillAmmo)
						
						IF NOT bFullAmmo
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						ENDIF
						
						DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_COR_AMMO_BUY", TRUE, FALSE, TRUE, TRUE)
									
						RETURN TRUE
					
					//Can't Afford
					ELSE
						DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_COR_AFF_I", FALSE, TRUE, TRUE, TRUE)
						PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - CHECK B - Unable to purchase ammo, CANT AFFORD IT. NETWORK_CAN_SPEND_MONEY = ", NETWORK_CAN_SPEND_MONEY(iAmmoCost, fromBank, fromBankAndWallet, FALSE))
						
						IF NOT bFullAmmo
							PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
			
				// Player has hit the real limit
				IF HAS_PLAYER_HIT_CORONA_AMMO_LIMIT(GET_CORONA_WEAPON_FROM_INT(coronaMenuData.iWeaponSlot), FALSE)
				
					DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_COR_AMMO", FALSE, TRUE, TRUE, TRUE)
					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Unable to purchase ammo, AMMO MAXED")
				
				// Player has hit the locked weapon limit
				ELIF HAS_PLAYER_HIT_CORONA_AMMO_LIMIT(GET_CORONA_WEAPON_FROM_INT(coronaMenuData.iWeaponSlot), TRUE)
				
					DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_COR_AMMO_LCK", FALSE, TRUE, TRUE, TRUE)
					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Unable to purchase ammo, AMMO LIMITED - this weapon is not unlocked")
				
				// Player can't afford the ammo
				ELSE
					INT iWeaponCost, iAmmoCost
					BOOL bDummy
					GET_GUNSHOP_WEAPON_COST(GET_CORONA_WEAPON_FROM_INT(coronaMenuData.iWeaponSlot), iWeaponCost, iAmmoCost, bDummy)
					BOOL bCanProceedWithPayment = (iAmmoCost > 0)
					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO [2] - iAmmoCost = ", iAmmoCost)
					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO [2] - bCanProceedWithPayment = ", GET_STRING_FROM_BOOL(bCanProceedWithPayment))
					IF g_sMPtunables.bCORONA_AMMO_PURCHASE_ALLOW_ZERO_PAYMENTS
						bCanProceedWithPayment = (iAmmoCost >= 0)
						PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO [2] - Zero payments permitted, bCanProceedWithPayment = ", GET_STRING_FROM_BOOL(bCanProceedWithPayment))
					ENDIF
					IF bCanProceedWithPayment
					AND (GET_CORONA_CASH_AVAILABLE() < iAmmoCost
					OR NOT NETWORK_CAN_SPEND_MONEY(iAmmoCost, FALSE, TRUE, FALSE))
						DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_COR_AFF_I", FALSE, TRUE, TRUE, TRUE)
						PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Unable to purchase ammo, CANT AFFORD IT: NETWORK_CAN_SPEND_MONEY = ", NETWORK_CAN_SPEND_MONEY(iAmmoCost, FALSE, TRUE, FALSE))
					ELSE
						PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_AMMO - Unable to purchase ammo, UNKNOWN?")
					ENDIF
				ENDIF

				IF NOT bFullAmmo
					PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			
			ENDIF
				
		ENDIF
		
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if the player can afford this purchase
FUNC BOOL CAN_PLAYER_AFFORD_CORONA_FULL_AMMO(CORONA_MENU_DATA &coronaMenuData)
	// Can the player afford this max ammo option
	IF GET_CORONA_CASH_AVAILABLE() < coronaMenuData.iTotalCostToFillAmmo
	OR NOT NETWORK_CAN_SPEND_MONEY(coronaMenuData.iTotalCostToFillAmmo, FALSE, TRUE, FALSE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PROCESS_PLAYER_PURCHASING_FULL_AMMO(CORONA_MENU_DATA &coronaMenuData, BOOL bApplyAmmo = TRUE, BOOL bChargeCash = TRUE)
	
	// If we are using the basket system and there is already one in process, show help text
	IF USE_SERVER_TRANSACTIONS()
	AND GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
	AND bChargeCash
		PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - Trying to by full ammo but already pending a transaction")
		
		// If we are in the corona, update our help text
		IF IS_PLAYER_IN_CORONA()
			DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_COR_TRAN_P", TRUE, FALSE, TRUE, TRUE)		
		ENDIF	
		
		RETURN FALSE
	ENDIF
	
	// Can the player afford this max ammo option
	IF NOT CAN_PLAYER_AFFORD_CORONA_FULL_AMMO(coronaMenuData)
		PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - FALSE Total Cost: ", coronaMenuData.iTotalCostToFillAmmo, ", Player Cash: ", GET_CORONA_CASH_AVAILABLE())
		RETURN FALSE
	ENDIF
	
	INT iBSCheckedAmmoTypes
	BOOL bMadePurchase
	
	INT iWeaponSlot
	WEAPON_TYPE aWeapon

	GUNCLUB_WEAPON_DATA_STRUCT	sWeaponStruct

	// Loop over our normal weapons
	FOR iWeaponSlot = 0 TO (ENUM_TO_INT(NUM_WEAPONSLOTS)-1) STEP 1
		
		// Get the weapon type from the slot
		IF GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot) = WEAPONSLOT_INVALID
			aWeapon = WEAPONTYPE_INVALID
		ELSE
			aWeapon = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot))
		ENDIF
		
		// Is the weapon valid for the corona?
		IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType, TRUE)
			
			IF GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(aWeapon, sWeaponStruct)
			
				IF aWeapon != WEAPONTYPE_UNARMED
					IF GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType) != -1
					OR aWeapon = WEAPONTYPE_GRENADE
						IF aWeapon = WEAPONTYPE_GRENADE
						OR NOT IS_BIT_SET(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
													
							PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - Purchasing ammo for: ", GET_WEAPON_NAME(aWeapon))
							IF PROCESS_PLAYER_PURCHASING_CORONA_AMMO(coronaMenuData, aWeapon, TRUE, TRUE, bApplyAmmo, bChargeCash)
								
								IF aWeapon != WEAPONTYPE_GRENADE
									SET_BIT(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
								ENDIF
								
								PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - Purchase ammo for: ", GET_WEAPON_NAME(aWeapon), " successful")
								bMadePurchase = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDFOR
	
	// Process our DLC weapons in the same way	
	scrShopWeaponData sWeaponData
	FOR iWeaponSlot = 0 TO (GET_NUM_DLC_WEAPONS()-1) STEP 1
		
		IF GET_DLC_WEAPON_DATA(iWeaponSlot, sWeaponData)

			aWeapon = INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash)
			
			PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - Checking if we can purchase ammo for: ", GET_WEAPON_NAME(aWeapon))

			IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType, TRUE)
				
				PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - ", GET_WEAPON_NAME(aWeapon), " is a valid weapon")
				
				BOOL bMK2Weapon = FALSE
				IF IS_WEAPON_MK2(aWeapon)
					bMK2Weapon = TRUE
				ENDIF
				
				// Grab our script data for the weapon
				IF GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(aWeapon, sWeaponStruct, DEFAULT, bMK2Weapon)
				
					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - Grabbed gunclub weapon data for: ", GET_WEAPON_NAME(aWeapon))
				
					IF GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType) != -1
					
						PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - Corona ammo type for: ", GET_WEAPON_NAME(aWeapon), " = ", GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
					
						IF NOT IS_BIT_SET(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
							
							PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - We havent checked ammo type: ", GET_AMMO_TYPE_NAME(sWeaponStruct.eAmmoType), " for weapon ", GET_WEAPON_NAME(aWeapon))
							
							PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - Purchasing ammo for: ", GET_WEAPON_NAME(aWeapon))
							IF PROCESS_PLAYER_PURCHASING_CORONA_AMMO(coronaMenuData, aWeapon, TRUE, TRUE, bApplyAmmo, bChargeCash)
							
								SET_BIT(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
															
								PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - Purchase ammo for: ", GET_WEAPON_NAME(aWeapon), " successful")
								bMadePurchase = TRUE
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - Checking if we can purchase ammo for FALSE: GET_CORONA_AMMO_TYPE_INT = -1")
					#ENDIF
					ENDIF
				ELSE
					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - Failed to grabb gunclub weapon data for: ", GET_WEAPON_NAME(aWeapon))
				ENDIF
			
			#IF IS_DEBUG_BUILD
			ELSE
				IF aWeapon != WEAPONTYPE_INVALID
					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - Weapon: ", GET_WEAPON_NAME(aWeapon), " in slot: ", iWeaponSlot, " is not valid:")
					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - IS_WEAPON_A_MELEE(): ", PICK_STRING(IS_WEAPON_A_MELEE(aWeapon), "TRUE", "FALSE") )
					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - IS_MP_WEAPON_UNLOCKED(): ", PICK_STRING(IS_MP_WEAPON_UNLOCKED(aWeapon), "TRUE", "FALSE") )
					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - HAS_PED_GOT_WEAPON(): ", PICK_STRING(HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), aWeapon), "TRUE", "FALSE") )
					IF aWeapon != WEAPONTYPE_DLC_RAILGUN
						PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_FULL_AMMO - IS_MP_WEAPON_PURCHASED(): ", PICK_STRING(IS_MP_WEAPON_PURCHASED(aWeapon), "TRUE", "FALSE") )
					ENDIF
				ENDIF
			#ENDIF
				
			ENDIF
		ENDIF
	ENDFOR
	
	IF USE_SERVER_TRANSACTIONS()
		IF bChargeCash
		AND bMadePurchase
			NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
			
			// Set our ammom / armor cash transaction as being active
			SET_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE(AMMO_ARMOR_CASH_TRANSACTION_FULL_AMMO)
		ENDIF
	ENDIF

	// Stop us from offering this to the player any more
	SET_CORONA_BIT(CORONA_FULL_AMMO_NO_LONGER_AVAILABLE)
	
	RETURN TRUE
ENDFUNC

// Broadcast event when player purchases the armor
PROC BROADCAST_CORONA_PED_PURCHASED_ARMOR(PED_COMP_NAME_ENUM eArmorType)

	EVENT_STRUCT_CORONA_PLAYER_PURCHASED_ARMOR Event
	
	Event.Details.Type 					= SCRIPT_EVENT_CORONA_PLAYER_PURCHASED_ARMOR
	Event.Details.FromPlayerIndex 		= PLAYER_ID()
	Event.eArmorType					= eArmorType
	
	INT iSendTo = ALL_PLAYERS()
	IF NOT (iSendTo = 0)		
		
		PRINTLN("[CORONA] BROADCAST_CORONA_PED_PURCHASED_ARMOR - Player has purchased armor in corona")
		PRINTLN("[CORONA] BROADCAST_CORONA_PED_PURCHASED_ARMOR 					eArmorType = ", ENUM_TO_INT(eArmorType))
		
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		PRINTLN("BROADCAST_CORONA_PED_PURCHASED_ARMOR - playerflags = 0 so not broadcasting")
	ENDIF

ENDPROC

PROC PROCESS_CORONA_PLAYER_PURCHASING_ARMOR(INT iEventID)
	
	EVENT_STRUCT_CORONA_PLAYER_PURCHASED_ARMOR Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		// Check if we are in a valid corona status
		IF IS_PLAYER_IN_CORONA()
			PRINTLN("[CORONA] PROCESS_CORONA_PLAYER_PURCHASING_ARMOR - ")
			PRINTLN("[CORONA]			Player: ", NATIVE_TO_INT(Event.Details.FromPlayerIndex))
			PRINTLN("[CORONA]			Armour: ", ENUM_TO_INT(Event.eArmorType))
			
			INT iPlayerInt = NATIVE_TO_INT(Event.Details.FromPlayerIndex)
				
			// Valid player index
			IF iPlayerInt >= 0
			
				//...yes, apply the armour to the local clone of the player
				PED_INDEX aClonedPed = g_CoronaClonePeds.piPlayersPed[iPlayerInt].pedID
				
				IF Event.eArmorType != DUMMY_PED_COMP
					// APPLY ARMOUR TO aClonedPed
					SET_PED_COMP_ITEM_CURRENT_MP(aClonedPed, COMP_TYPE_SPECIAL2, Event.eArmorType, FALSE)
				
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[CORONA] PROCESS_CORONA_PLAYER_PURCHASING_ARMOR - eArmorType = DUMMY_PED_COMP")
				#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_CORONA_PLAYER_PURCHASING_ARMOR - could not retrieve data")
	ENDIF
	
ENDPROC


FUNC BOOL PROCESS_PLAYER_PURCHASING_CORONA_ARMOR(CORONA_MENU_DATA &coronaMenuData, BOOL bApplyArmor = TRUE, BOOL bChargeCash = TRUE)

	// If we are using the basket system and there is already one in process, show help text
	IF USE_SERVER_TRANSACTIONS()
	AND GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
		IF bChargeCash
			PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR - Trying to by armor but already pending a transaction")
			DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_COR_TRAN_P", TRUE, FALSE, TRUE, TRUE)		
			RETURN FALSE
		ENDIF
	ENDIF

	INT iArmorLevel = GET_MP_MAX_UNLOCKED_ARMOR_LEVEL()
	IF iArmorLevel >= 0

		GUNCLUB_WEAPON_DATA_STRUCT sWeaponData
		GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(WEAPONTYPE_UNARMED, sWeaponData, iArmorLevel)	// fetches armor data
		
		INT iArmorToGive = FLOOR((TO_FLOAT(GET_PLAYER_MAX_ARMOUR(PLAYER_ID()))/100.0)*sWeaponData.iDefaultClipSize)

		INT iArmorCost, iDummy1
		BOOL bDummy
		
		GET_GUNSHOP_WEAPON_COST(WEAPONTYPE_UNARMED, iArmorCost, iDummy1, bDummy, iArmorLevel)
		
		IF iArmorCost >= 0
		
			IF GET_PED_ARMOUR(PLAYER_PED_ID()) >= iArmorToGive
			
				DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_COR_ARM_MAX", FALSE, TRUE, TRUE, TRUE)
				PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR - Unable to purchase armor, ARMOR FULL")
			
				PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			ELIF (iArmorCost > GET_CORONA_CASH_AVAILABLE() OR (NOT NETWORK_CAN_SPEND_MONEY(iArmorCost, FALSE, TRUE, FALSE)))
			
			
				DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_COR_AFF_I", FALSE, TRUE, TRUE, TRUE)
				PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR - Unable to purchase armor, CANT AFFORD IT, NETWORK_CAN_SPEND_MONEY: ", NETWORK_CAN_SPEND_MONEY(iArmorCost, FALSE, TRUE, FALSE))
				
				PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
			ELSE
			
				PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR - Player Cash $", GET_PLAYER_CASH(PLAYER_ID()), " and Armor Cost: $", iArmorCost)
				
				TEXT_LABEL_15 tlArmorName = GET_MP_ARMOR_NAME(iArmorLevel)
				
//				IF iArmorCost > 0
					IF USE_SERVER_TRANSACTIONS()
					AND bChargeCash
						TEXT_LABEL_63 tlCatalogueKey
						GENERATE_KEY_FOR_SHOP_CATALOGUE( tlCatalogueKey, tlArmorName, GET_ENTITY_MODEL( PLAYER_PED_ID() ), SHOP_TYPE_GUN, ENUM_TO_INT( ITEM_TYPE_ARMOUR ), 0 )
						
						PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR - Purchasing armour, key='", tlCatalogueKey, "'")
						
						IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_WEAPON_AMMO, GET_HASH_KEY( tlCatalogueKey ), NET_SHOP_ACTION_SPEND, 1, iArmorCost, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, DEFAULT, CTPF_AUTO_RETRY )
							NETWORK_START_BASKET_TRANSACTION_CHECKOUT()				
							SET_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE(AMMO_ARMOR_CASH_TRANSACTION_ARMOR)				
						ENDIF
					ELSE
						IF bChargeCash
						OR (USE_SERVER_TRANSACTIONS() AND NOT bChargeCash)
					
							BOOL fromBank = (NETWORK_GET_VC_BANK_BALANCE() > 0) 
							BOOL fromBankAndWallet = (NETWORK_GET_VC_BANK_BALANCE() < iArmorCost)

							// We must set up the nonce seed for a PC transaction when we are calling telemetry
							IF (USE_SERVER_TRANSACTIONS() AND NOT bChargeCash)
								PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR - Set up nonce seed for our ID: ", GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
								NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
							ENDIF

							NETWORK_BUY_ITEM(iArmorCost, GET_HASH_KEY(tlArmorName), PURCHASE_ARMOR, DEFAULT, fromBank, tlArmorName, GET_HASH_KEY("SHOP_CORONA"), ENUM_TO_INT(WEAPONTYPE_UNARMED), DEFAULT, fromBankAndWallet) 
						ENDIF
					ENDIF
//				ELSE
//					PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR - Not paying as Armor Cost: $", iArmorCost)
//				ENDIF
				
				PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR - Purchased - Remaining Cash: $", GET_PLAYER_CASH(PLAYER_ID()))
				
				IF bApplyArmor
					SET_PED_ARMOUR(PLAYER_PED_ID(), iArmorToGive)
					
					SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(2, GET_MP_ARMOR_HASH(iArmorLevel), hash("ARMOR"), 1,  GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("JOB_QUICK_RESTART"))
					// Put the armor clothing component on
					INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_ARMOUR)
					IF iStat = 1
						PED_COMP_NAME_ENUM eBodyArmour
						eBodyArmour = GET_BODY_ARMOUR_FOR_JBIB(PLAYER_PED_ID(), GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_JBIB, GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB))//, sWeaponData.iDefaultClipSize)
						
						IF eBodyArmour != DUMMY_PED_COMP
							IF eBodyArmour != SPECIAL2_FMM_0_0
							    eBodyArmour += INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_BODY_ARMOUR_FROM_CLIP_SIZE(sWeaponData.iDefaultClipSize))
							ENDIF
							
							INT iDLCSpecialHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_SPECIAL), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL))
							IF NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCSpecialHash, DLC_RESTRICTION_TAG_OVER_JACKET, ENUM_TO_INT(PED_COMP_SPECIAL))
							
								CPRINTLN(DEBUG_SHOPS, "[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR Player bought body armour, equiping = ", eBodyArmour)
								
								// Local ped
								SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, eBodyArmour, FALSE)
								// Local ped clone
								SET_PED_COMP_ITEM_CURRENT_MP(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID, COMP_TYPE_SPECIAL2, eBodyArmour, FALSE)
								// Broadcast to others
								BROADCAST_CORONA_PED_PURCHASED_ARMOR(eBodyArmour)
							#IF IS_DEBUG_BUILD
							ELSE
								CPRINTLN(DEBUG_SHOPS, "[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR Player bought body armour, do not equip = ", eBodyArmour)
							#ENDIF
							ENDIF
						
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR - eBodyArmour = DUMMY_PED_COMP")
						#ENDIF
						ENDIF
						
					ELSE
						CPRINTLN(DEBUG_SHOPS, "[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR Player bought body armour, don't auto-equip ")
					ENDIF
				ENDIF
				
				IF bChargeCash
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					DISPLAY_CORONA_FLASHING_HELP_TEXT(coronaMenuData, "FM_COR_ARM_BUY", TRUE, FALSE, TRUE, TRUE)
				ENDIF
							
				RETURN TRUE
				
			ENDIF
		
		ELSE
			PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR - Unable to purchase armor, could not find cost")
		ENDIF
	ELSE
		PRINTLN("[CORONA] PROCESS_PLAYER_PURCHASING_CORONA_ARMOR - Unable to purchase armor, armor not currently unlocked")
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Function to listen for an ammo or armor cash transaction to complete. If successful we will act on the appropiate action
PROC MAINTAIN_AMMO_AND_ARMOR_CASH_TRANSACTIONS(CORONA_MENU_DATA &coronaMenuData)

	// Bail if we are not in a PC / cash transaction environment
	IF NOT USE_SERVER_TRANSACTIONS()
		EXIT
	ENDIF
	
	// If there is one active
	IF IS_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE()
	
		// Wait until the transaction has completed
		IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
			IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				
				PRINTLN("MAINTAIN_AMMO_AND_ARMOR_CASH_TRANSACTIONS - Transaction state: ", ENUM_TO_INT(GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX())))
				
				// If this was a success apply the full ammo purchase
				IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
				
					PRINTLN("MAINTAIN_AMMO_AND_ARMOR_CASH_TRANSACTIONS - CASH_TRANSACTION_STATUS_SUCCESS, handle update to player inventory")	
				
					// FULL AMMO: If this flag is set we should attempt to give the player all ammo.
					IF IS_BIT_SET(g_sAmmoArmorCashTransaction.iAmmoArmorCashTransactionActive, ENUM_TO_INT(AMMO_ARMOR_CASH_TRANSACTION_FULL_AMMO))
							
						// GIVE THE FULL AMMO
						IF PROCESS_PLAYER_PURCHASING_FULL_AMMO(coronaMenuData, TRUE, FALSE)
							PRINTLN("MAINTAIN_AMMO_AND_ARMOR_CASH_TRANSACTIONS - Now apply ammo to ped for successful full ammo")
						ENDIF
					ENDIF
					
					// SINGLE CLIPS: If either of these flags are set we need to give full / clip to the specific weapon
					IF IS_BIT_SET(g_sAmmoArmorCashTransaction.iAmmoArmorCashTransactionActive, ENUM_TO_INT(AMMO_ARMOR_CASH_TRANSACTION_FULL_CLIP_AMMO))
					OR IS_BIT_SET(g_sAmmoArmorCashTransaction.iAmmoArmorCashTransactionActive, ENUM_TO_INT(AMMO_ARMOR_CASH_TRANSACTION_SINGLE_CLIP_AMMO))
						
						BOOL bFullAmmo = IS_BIT_SET(g_sAmmoArmorCashTransaction.iAmmoArmorCashTransactionActive, ENUM_TO_INT(AMMO_ARMOR_CASH_TRANSACTION_FULL_CLIP_AMMO))
						
						IF PROCESS_PLAYER_PURCHASING_CORONA_AMMO(coronaMenuData, g_sAmmoArmorCashTransaction.wtWeapon, bFullAmmo, FALSE, TRUE, FALSE)
							PRINTLN("MAINTAIN_AMMO_AND_ARMOR_CASH_TRANSACTIONS - Now purchase ammo for weapon: ", GET_WEAPON_NAME(g_sAmmoArmorCashTransaction.wtWeapon), ". FullAmmo = ", PICK_STRING(bFullAmmo, "TRUE", "FALSE"))
						ENDIF						
					ENDIF
					
					// ARMOR: If the flag is set we need to process the giving of armor to the ped for a successful transaction
					IF IS_BIT_SET(g_sAmmoArmorCashTransaction.iAmmoArmorCashTransactionActive, ENUM_TO_INT(AMMO_ARMOR_CASH_TRANSACTION_ARMOR))
						IF PROCESS_PLAYER_PURCHASING_CORONA_ARMOR(coronaMenuData, TRUE, FALSE)
							PRINTLN("MAINTAIN_AMMO_AND_ARMOR_CASH_TRANSACTIONS - Now purchase armor for successfull transaction")							
						ENDIF
					ENDIF
				ENDIF
				
				// If we are in a corona, force a refresh of the menu
				IF IS_PLAYER_IN_CORONA()
					SET_CORONA_BIT(CORONA_AMMO_ARMOR_EXTERNAL_UPDATE)
				ENDIF
				
				// Delete our flags as we are finished with this transaction
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CLEAR_AMMO_ARMOR_CASH_TRANSACTIONS()
			ENDIF
	
		ELSE
			IF !g_bPendingCashTransactionCommunicatingWithServer
				CLEAR_AMMO_ARMOR_CASH_TRANSACTIONS()
			
				PRINTLN("MAINTAIN_AMMO_AND_ARMOR_CASH_TRANSACTIONS - GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1, but we still think we are pending a response")	
				SCRIPT_ASSERT("MAINTAIN_AMMO_AND_ARMOR_CASH_TRANSACTIONS - GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1, but IS_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE() = TRUE. Waiting for a response?")
			ELSE
				PRINTLN("MAINTAIN_AMMO_AND_ARMOR_CASH_TRANSACTIONS - g_bPendingCashTransactionCommunicatingWithServer")	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Rally / GTA partners

FUNC BOOL IS_RACE_PARTNER_VALID(INT iMissionType)
	
	IF g_iMyPreferedPartner >= 0
	
		GAMER_HANDLE aGamerHandle = GET_TRANSITION_PLAYER_GAMER_HANDLE(g_iMyPreferedPartner)
	
		IF NOT IS_GAMER_HANDLE_VALID(aGamerHandle)
				
			g_iMyPreferedPartner = TEAM_RACE_CHOSE_ANY
		
			IF SHOULD_TRANSITION_SESSION_RUN(iMissionType)
				PRINTLN("IS_RACE_PARTNER_VALID - SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(", ciTRANSITION_SESSIONS_PLAYER_OPTION_0, ", ", g_iMyPreferedPartner, ")")
				SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYER_OPTION_0, g_iMyPreferedPartner)
			ENDIF
		
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Loops through valid players to find next available partner
PROC GET_NEXT_VALID_RACE_PARTNER(INT iIncrement)
	
	BOOL bFoundPartner
	GAMER_HANDLE ghLocalPlayer = GET_LOCAL_GAMER_HANDLE()
								
	WHILE (NOT bFoundPartner)
		
		g_iMyPreferedPartner += iIncrement
		
		// If we currently are on ANY, move to a player
		IF g_iMyPreferedPartner < 0 // TEAM_RACE_CHOSE_ANY
		
			IF g_iMyPreferedPartner < TEAM_RACE_CHOSE_ANY //TEAM_RACE_CHOSE_NONE
				g_iMyPreferedPartner = GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION()
			ELSE
				bFoundPartner = TRUE
			
				PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")

			ENDIF
		ELSE
		
			// If we have moved our of array (set to ANY)
			IF g_iMyPreferedPartner >= GET_NUMBER_OF_PLAYERS_IN_MY_TRANSITION_SESSION()
				
//				IF IS_THIS_A_RALLY_RACE_TEMP(g_sTransitionSessionOptions)
				g_iMyPreferedPartner = TEAM_RACE_CHOSE_ANY
//				ELSE
//					g_iMyPreferedPartner = TEAM_RACE_CHOSE_NONE
//				ENDIF
				
				bFoundPartner = TRUE
				
				PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
			// Else move to next player in array
			ELSE
				g_ghPreferredPartnerHandle = GET_TRANSITION_PLAYER_GAMER_HANDLE(g_iMyPreferedPartner)
				
				// Is player valid and not us
				IF IS_GAMER_HANDLE_VALID(g_ghPreferredPartnerHandle)
					IF NOT NETWORK_ARE_HANDLES_THE_SAME(g_ghPreferredPartnerHandle, ghLocalPlayer)
						IF NOT IS_CORONA_IN_HEAD_2_HEAD_MODE()
						OR ( IS_CORONA_IN_HEAD_2_HEAD_MODE() AND (GET_TRANSITION_PLAYER_CREW_ID(GET_MY_TRANSITION_SESSION_ARRAY_POS()) = GET_TRANSITION_PLAYER_CREW_ID(g_iMyPreferedPartner)) )
							bFoundPartner = TRUE
							
							PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
				
	ENDWHILE
ENDPROC


/// PURPOSE:
///    Returns TRUE if the sub role option is active for the current mission
FUNC BOOL IS_CORONA_MISSION_SUB_ROLE_ACTIVE()

	#IF IS_DEBUG_BUILD
	IF g_bDebugReorganiseRolesActive
		PRINTLN("[CORONA] IS_CORONA_MISSION_SUB_ROLE_ACTIVE DEBUG: g_bDebugReorganiseRolesActive = TRUE")
		RETURN TRUE
	ENDIF
	#ENDIF

	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Constructs the team role name to display in the corona
FUNC TEXT_LABEL_15 GET_CORONA_MISSION_SUB_ROLE_NAME(INT iTeam, INT iRole)
	
	TEXT_LABEL_15 tlRoleName = "COR_ROLE_"
	tlRoleName += g_FMMC_STRUCT.iTeamRoleNames[iRole]
	
	// Override the role name with the custom label from the creator
	IF iTeam != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63RoleName[iRole])
			tlRoleName = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63RoleName[iRole]
		ENDIF
	ENDIF
	
	RETURN tlRoleName	
ENDFUNC





/// PURPOSE:
///    Returns the default mission vehicle from the creator if valid
FUNC INT GET_CORONA_DEFAULT_MISSION_VEHICLE(CORONA_MENU_DATA &coronaMenuData, INT iTeam)

	INT iVehicle = -1
	INT iClass = coronaMenuData.iMissionVehicleClass[iTeam]
	
	// 2632975 - Update for reading in a default vehicle for the class chosen.
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_DEFAULT_VEHICLES_PER_TYPE)
		IF iClass != -1
			PRINTLN("[CORONA] GET_CORONA_DEFAULT_MISSION_VEHICLE - Default vehicle set for class: ", g_FMMC_STRUCT.iDefaultCommonVehicleForType[iClass], ", iClass = ", iClass)
			iVehicle = g_FMMC_STRUCT.iDefaultCommonVehicleForType[iClass]
		ENDIF		
	ENDIF
	
	IF g_FMMC_STRUCT.iDefaultCommonVehicle[iTeam] != -1

		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
		AND coronaMenuData.iMissionTeamRole[iTeam] != -1
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][coronaMenuData.iMissionTeamRole[iTeam]][iClass][GET_LONG_BITSET_INDEX(g_FMMC_STRUCT.iDefaultMissionVehicleForRole[iTeam][coronaMenuData.iMissionTeamRole[iTeam]])], GET_LONG_BITSET_BIT(g_FMMC_STRUCT.iDefaultMissionVehicleForRole[iTeam][coronaMenuData.iMissionTeamRole[iTeam]]))
				PRINTLN("[CORONA] GET_CORONA_DEFAULT_MISSION_VEHICLE (ROLES) - Team: ", iTeam, " Default vehicle: ", g_FMMC_STRUCT.iDefaultMissionVehicleForRole[iTeam][coronaMenuData.iMissionTeamRole[iTeam]])
				iVehicle = g_FMMC_STRUCT.iDefaultMissionVehicleForRole[iTeam][coronaMenuData.iMissionTeamRole[iTeam]]
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][iClass][GET_LONG_BITSET_INDEX(g_FMMC_STRUCT.iDefaultCommonVehicle[iTeam])], GET_LONG_BITSET_BIT(g_FMMC_STRUCT.iDefaultCommonVehicle[iTeam]))
				PRINTLN("[CORONA] GET_CORONA_DEFAULT_MISSION_VEHICLE - Team: ", iTeam, " Default vehicle: ", g_FMMC_STRUCT.iDefaultCommonVehicle[iTeam])
				iVehicle = g_FMMC_STRUCT.iDefaultCommonVehicle[iTeam]
			ENDIF
		ENDIF
	ENDIF

	RETURN iVehicle
ENDFUNC

/// PURPOSE:
///    Sets up the initial vehicle availability for each of the teams
PROC SET_CORONA_MISSION_VEHICLE_AVAILABILITY(CORONA_MENU_DATA &coronaMenuData)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tlVehicleClass
	#ENDIF
	
	INT iTeam
	REPEAT FMMC_MAX_TEAMS iTeam
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST)
		
			PRINTLN("[CORONA] SET_CORONA_MISSION_VEHICLE_AVAILABILITY - Team: ", iTeam, " has vehicles available")
		
			// Use the default if possible
			coronaMenuData.iMissionVehicleClass[iTeam] = GET_CORONA_DEFAULT_MISSION_VEHICLE_CLASS(iTeam, coronaMenuData.iMissionTeamRole[iTeam])
			
			// Has the host selected the class?
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciHOST_VEHICLE_CLASS_SELECTION)
				// Check they have locked the class.
				IF g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_VEHICLE_CLASS] != -1
					
					// Override the class
					coronaMenuData.iMissionVehicleClass[iTeam] = g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_VEHICLE_CLASS]
					PRINTLN("[CORONA] SET_CORONA_MISSION_VEHICLE_AVAILABILITY - Team: ", iTeam, " override class from host option: ", coronaMenuData.iMissionVehicleClass[iTeam])
				ENDIF
			ENDIF
			
			IF coronaMenuData.iMissionVehicleClass[iTeam] != -1
			OR FIND_NEXT_CORONA_MISSION_VEHICLE_CLASS(coronaMenuData.iMissionVehicleClass[iTeam], iTeam, coronaMenuData.iMissionTeamRole[iTeam], 1)
				
				#IF IS_DEBUG_BUILD
				tlVehicleClass = GET_VEHICLE_LIBRARY_NAME(coronaMenuData.iMissionVehicleClass[iTeam])
				PRINTLN("[CORONA] SET_CORONA_MISSION_VEHICLE_AVAILABILITY - Team: ", iTeam, " initialised to vehicle class: ", coronaMenuData.iMissionVehicleClass[iTeam], " [", tlVehicleClass, "]")
				#ENDIF
				
				coronaMenuData.iMissionVehicle[iTeam] = GET_CORONA_DEFAULT_MISSION_VEHICLE(coronaMenuData, iTeam)
				
				IF coronaMenuData.iMissionVehicle[iTeam] = -1
					FIND_NEXT_CORONA_MISSION_VEHICLE(coronaMenuData, iTeam, coronaMenuData.iMissionVehicleClass[iTeam], 1)
				ELSE
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.mnVehicleChosen = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(coronaMenuData.iMissionVehicleClass[iTeam], coronaMenuData.iMissionVehicle[iTeam])	
				ENDIF
				
				PRINTLN("[CORONA] SET_CORONA_MISSION_VEHICLE_AVAILABILITY - Team: ", iTeam, " initialised to vehicle at bit: ", coronaMenuData.iMissionVehicle[iTeam])
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Update our availability of the vehicle classes for the current team
	UPDATE_CORONA_MISSION_VEHICLE_CLASS_AVAILABILITY(coronaMenuData)	
ENDPROC

FUNC BOOL IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID(INT iTeam, INT iRole)

	IF g_iCoronaMissionVehicleClass != -1
		
		IF IS_ARENA_WARS_JOB()
		AND g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
		AND g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_NORMAL
			PRINTLN("[CORONA] IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID - Clobber team to 0 for g_FMMC_STRUCT.iMissionType = ", g_FMMC_STRUCT.iMissionType)
			
			iTeam = 0
		ENDIF
		
		BOOL bVehicleClassValid
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
			IF (SUM_BITS(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][iRole][g_iCoronaMissionVehicleClass][0]) + SUM_BITS(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][iRole][g_iCoronaMissionVehicleClass][1])) != 0
				bVehicleClassValid = TRUE
			ENDIF
		ELSE
			IF (SUM_BITS(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][g_iCoronaMissionVehicleClass][0]) + SUM_BITS(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][g_iCoronaMissionVehicleClass][1])) != 0
				bVehicleClassValid = TRUE
			ENDIF			
		ENDIF
		
		PRINTLN("[CORONA] IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID - iTeam = ", iTeam, ", iRole = ", iRole, ", bVehicleClassValid = ", bVehicleClassValid, ", g_iCoronaMissionVehicleClass = ", g_iCoronaMissionVehicleClass)
				
		IF bVehicleClassValid
		
			PRINTLN("[CORONA] IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID - Vehicle Class is valid: ", g_iCoronaMissionVehicleClass)
		
			INT i
			INT iVehicleValue
			FOR i = 0 TO 63 STEP 1
				IF g_mnCoronaMissionVehicle = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(g_iCoronaMissionVehicleClass, i)
					PRINTLN("[CORONA] IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID - Vehicle Index in class is: ", i)
					iVehicleValue = i
					i = 63
				ENDIF
			ENDFOR
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
				IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[iTeam][iRole][g_iCoronaMissionVehicleClass][GET_LONG_BITSET_INDEX(iVehicleValue)], GET_LONG_BITSET_BIT(iVehicleValue))
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeam][g_iCoronaMissionVehicleClass][GET_LONG_BITSET_INDEX(iVehicleValue)], GET_LONG_BITSET_BIT(iVehicleValue))
					RETURN TRUE
				ENDIF			
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SET_VERSUS_MISSION_VEHICLES_FOR_ROUNDS(CORONA_MENU_DATA &coronaMenuData)
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	AND (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_VERSUS
	OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_COOP)
	
				
		INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
		IF iTeam != -1
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST)
				
				IF ROUNDS_NEED_TO_SET_VEHICLE()
				OR NOT IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID(iTeam, GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamSubRole)
					
					PRINTLN("[CORONA] SET_VERSUS_MISSION_VEHICLES_FOR_ROUNDS - IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID = FALSE, update vehicle")
					
					SET_CORONA_MISSION_VEHICLE_AVAILABILITY(coronaMenuData)
					g_iCoronaMissionVehicleClass = coronaMenuData.iMissionVehicleClass[iTeam]
					g_mnCoronaMissionVehicle = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(coronaMenuData.iMissionVehicleClass[iTeam], coronaMenuData.iMissionVehicle[iTeam])
				ENDIF
				
				PRINTLN("[CORONA] SET_VERSUS_MISSION_VEHICLES_FOR_ROUNDS g_mnCoronaMissionVehicle = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_mnCoronaMissionVehicle))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the role of the local player to BD data for all remote players to read
PROC SET_PLAYER_CHOSEN_MISSION_TEAM_ROLE(INT iRole)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[CORONA] [TEAM BAL DATA] [SEAT] SET_PLAYER_CHOSEN_MISSION_TEAM_ROLE - Setting the players team role as: ", iRole, ", T(", GET_CLOUD_TIME_AS_INT(), ")")
	
	// Increment our client side sync ID so all UI updates for other players
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iClientSyncID++
	GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamSubRole = iRole
	
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole = iRole
		PRINTLN("[CORONA] [TEAM BAL DATA] [SEAT] SET_PLAYER_CHOSEN_MISSION_TEAM_ROLE - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole = ", iRole)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnSeatPreference = iRole
		PRINTLN("[CORONA] [TEAM BAL DATA] [SEAT] SET_PLAYER_CHOSEN_MISSION_TEAM_ROLE - GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnSeatPreference = ", iRole)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
		PRINTLN("[CORONA] SET_PLAYER_CHOSEN_MISSION_TEAM_ROLE - g_iCoronaMissionRole =  ", iRole, ", T(", GET_CLOUD_TIME_AS_INT(), ")")
		g_iCoronaMissionRole = iRole
		INT iTeam = GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
		IF NOT IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID(iTeam, g_iCoronaMissionRole)
			PRINTLN("[CORONA] SET_PLAYER_CHOSEN_MISSION_TEAM_ROLE - Updated g_iCoronaMissionVehicleClass ", g_iCoronaMissionVehicleClass)
			g_iCoronaMissionVehicleClass = GET_CORONA_DEFAULT_MISSION_VEHICLE_CLASS(iTeam, g_iCoronaMissionRole)
			PRINTLN("[CORONA] SET_PLAYER_CHOSEN_MISSION_TEAM_ROLE - Updated g_mnCoronaMissionVehicle (g_FMMC_STRUCT.iDefaultMissionVehicleForRole[iTeam][g_iCoronaMissionRole]) =  ", g_FMMC_STRUCT.iDefaultMissionVehicleForRole[iTeam][g_iCoronaMissionRole])
			g_mnCoronaMissionVehicle = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(g_iCoronaMissionVehicleClass, g_FMMC_STRUCT.iDefaultMissionVehicleForRole[iTeam][g_iCoronaMissionRole])
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE: Restore function called when players choose quick restart. Re-initialises player data for restart.
PROC SETUP_CORONA_CLIENT_OPTIONS_FROM_BACKUP(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails, CORONA_MENU_DATA &coronaMenuData)
	
	IF IS_THIS_MISSION_OF_TYPE_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistMission  , sLaunchMissionDetails.bIsHeistPlanning, sLaunchMissionDetails.bIsForHeistPrepCutscene  )
		// Removed with new outfit handling from Craig V / Alastair and Tom. We now set nonResetVars which will be persistent across attempts of a Job.
		
//		PRINTLN("SETUP_CORONA_CLIENT_OPTIONS_FROM_BACKUP = Restore outfit to: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iOutfit)
//		SET_LOCAL_DEFAULT_OUTFIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen,GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iOutfit)
		EXIT
	ENDIF
	
	IF ROUNDS_NEED_TO_SET_VS_OUTFITS()
		
		PRINTLN("[CORONA] SETUP_CORONA_CLIENT_OPTIONS_FROM_BACKUP - VS mission where we need to set up our outfit options.")
		SET_VERSUS_MISSION_OUTFIT_VALUES_FOR_JIP()
		
		ROUNDS_CLEAR_NEED_TO_SET_VS_OUTFITS()
	ENDIF
	
	
	PRINTLN("[CORONA] SETUP_CORONA_CLIENT_OPTIONS_FROM_BACKUP - VS mission where we need to set up our vehicle options.")
	SET_VERSUS_MISSION_VEHICLES_FOR_ROUNDS(coronaMenuData)
ENDPROC

/// PURPOSE:
///    Gets the current chosen sub team role for the player passed in
FUNC INT GET_PLAYER_CHOSEN_MISSION_TEAM_ROLE(PLAYER_INDEX playerID)
	RETURN GlobalPlayerBD_FM[NATIVE_TO_INT(playerID)].sClientCoronaData.iTeamSubRole
ENDFUNC

/// PURPOSE:
///    Finds the next available role for the current team
FUNC BOOL FIND_NEXT_CORONA_MISSION_TEAM_ROLE(CORONA_MENU_DATA &coronaMenuData, INT iIncrement, INT iTeam)
	
	// If we have a valid team
	IF iTeam != -1
				
		// Increment, and cap
		coronaMenuData.iMissionTeamRole[iTeam] += iIncrement
		
		IF coronaMenuData.iMissionTeamRole[iTeam] < ciCORONA_DEFAULT_TEAM_ROLE
			
			IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam] > 0
				coronaMenuData.iMissionTeamRole[iTeam] = (g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam]-1)
			ELSE
				PRINTLN("[CORONA] FIND_NEXT_CORONA_MISSION_TEAM_ROLE - g_FMMC_STRUCT.iMaxNumPlayersPerTeam[", iTeam, "] = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam], " setting to default team role ciCORONA_DEFAULT_TEAM_ROLE (", ciCORONA_DEFAULT_TEAM_ROLE, ")")
				coronaMenuData.iMissionTeamRole[iTeam] = ciCORONA_DEFAULT_TEAM_ROLE
			ENDIF
		ENDIF
		
		IF coronaMenuData.iMissionTeamRole[iTeam] >= g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam]
			coronaMenuData.iMissionTeamRole[iTeam] = ciCORONA_DEFAULT_TEAM_ROLE
		ENDIF
		
		// Update our BD data for the new chosen team role.
		SET_PLAYER_CHOSEN_MISSION_TEAM_ROLE(coronaMenuData.iMissionTeamRole[iTeam])
				
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Assigns a sub role to the local player in the corona
PROC SET_CORONA_MISSION_DEFAULT_SUB_ROLE(CORONA_MENU_DATA &coronaMenuData)

	SET_PLAYER_CHOSEN_MISSION_TEAM_ROLE(0)

	// Only process this logic is mission requires it
	IF IS_CORONA_MISSION_SUB_ROLE_ACTIVE()
		
		INT i
		INT iMyTeam = GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
		INT iTeamRole = 0
		PLAYER_INDEX playerID
		
		// Loop over the players, if we find outselves we should take the current incremented role
		REPEAT NUM_NETWORK_PLAYERS i
		
			playerID = INT_TO_PLAYERINDEX(i)
			IF IS_NET_PLAYER_OK(playerID, FALSE)
			AND NOT IS_PLAYER_SCTV(playerID)
				
				IF playerID = PLAYER_ID()
					PRINTLN("[CORONA] SET_CORONA_MISSION_DEFAULT_SUB_ROLE - Set my team role to: ", iTeamRole)
					SET_PLAYER_CHOSEN_MISSION_TEAM_ROLE(iTeamRole)
					
					// Save out the team roles we have assigned ourselves to corona data for use in team screen
					IF iMyTeam != -1
						coronaMenuData.iMissionTeamRole[iMyTeam] = iTeamRole
						PRINTLN("[CORONA] SET_CORONA_MISSION_DEFAULT_SUB_ROLE - Update corona data: coronaMenuData.iMissionTeamRole[", iMyTeam, "] = ", iTeamRole)
					ENDIF
				ELSE
					// Only increment roles for the team you are on, until we find ourselves with the check above
					IF GlobalPlayerBD_FM[i].sClientCoronaData.iTeamChosen = iMyTeam					
						iTeamRole++
						
						IF iTeamRole >= g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iMyTeam]			// Cap out roles to the max players in the team or roles
						OR iTeamRole >= FMMC_MAX_ROLES
							iTeamRole = 0
						ENDIF
						
						PRINTLN("[CORONA] SET_CORONA_MISSION_DEFAULT_SUB_ROLE - Player in my team (", iMyTeam, "). Increment Role to: ", iTeamRole)
					ENDIF
				ENDIF
			ENDIF		
		ENDREPEAT
	ENDIF	

ENDPROC


/// PURPOSE:
///    Toggles the coroan client invite option and broadcasts this across the transition session
PROC UPDATE_CORONA_CLIENT_INVITE_OPTION(INT iMissionType)

	g_sTransitionSessionData.bTransitionSessionClientInvites = !(g_sTransitionSessionData.bTransitionSessionClientInvites)
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")

	IF SHOULD_TRANSITION_SESSION_RUN(iMissionType)
		PRINTLN("UPDATE_CORONA_CLIENT_INVITE_OPTION - SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(", ciTRANSITION_SESSIONS_CLIENT_INVITES, ", ", PICK_STRING(g_sTransitionSessionData.bTransitionSessionClientInvites, "TRUE", "FALSE"), ")")
		SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_CLIENT_INVITES, PICK_INT(g_sTransitionSessionData.bTransitionSessionClientInvites, 1, 0))
	ENDIF
ENDPROC


	
FUNC BOOL GET_NEXT_VALID_VEHICLE(CORONA_MENU_DATA &coronaMenuData, INT iIncrement, FMMC_SELECTED_ITEMS &sSelection, BOOL bIncludeRandom = TRUE, BOOL bIgnoreLockedOption = FALSE)

	IF NOT CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION(DEFAULT, DEFAULT, coronaMenuData.iTotalCustomVehicles)
	AND NOT bIgnoreLockedOption
		PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - NOT CAN_CHANGE_CORONA_RACE_VEHICLE_OPTION()")
		RETURN FALSE
	ENDIF


	BOOL bFound
	BOOL bFoundNewVeh
	MODEL_NAMES mnVehicle
	
	// Record our previous state
	BOOL bPrevStateCust = IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE)
	BOOL bPrevStateDLC = IS_PLAYER_SELECTING_DLC_VEHICLE()
	INT iPrevVehicle = g_iMyRaceModelChoice
	
	PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - ************************************")
	PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - 		bPrevStateCust: ", PICK_STRING(bPrevStateCust, "TRUE", "FALSE"))
	PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - 		bPrevStateDLC: ", PICK_STRING(bPrevStateDLC, "TRUE", "FALSE"))
	PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - 		iPrevVehicle: ", iPrevVehicle)
	PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - 		iVehicleCoronaIndex: ", coronaMenuData.iVehicleCoronaIndex)
	PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - 		iTotalCustomVehicles: ", coronaMenuData.iTotalCustomVehicles)
	PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - 		bIgnoreLockedOption: ", GET_STRING_FROM_BOOL(bIgnoreLockedOption))
	PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - ************************************")
	
	INT iClass = sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS]
	INT iRaceType = g_FMMC_STRUCT.iRaceType
	INT iClassOverride = -1
	
	// Set up the max count (LOOK TO MAYBE GRAB THIS ONCE AS WE MOVE ONTO THIS SCREEN)
	INT iMaxCount
	INT iMaxDLCCount
	
	IF DOES_JOB_USE_RACE_VEHICLE_SELECTION(IS_RACE())
		iClass = coronaMenuData.iGenericJobVehicleClass
		iClassOverride = iClass
		iRaceType = coronaMenuData.iGenericJobRaceType
		iMaxCount = (ciRC_MAX_VEHICLES_IN_CORONA/2)
		iMaxDLCCount = iMaxCount
		
		PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - 		iClass: ", iClass)
		PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - 		iClassOverride: ", iClassOverride)
		PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - 		iRaceType: ", iRaceType)
	ELSE
		iMaxCount = GET_SELECTION_MAX_VEHICLE_TYPE(iRaceType, iClass)
		iMaxDLCCount = GET_SELECTION_MAX_DLC_VEHICLE_TYPE(iRaceType, iClass)
	ENDIF

	PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - 		iMaxCount: ", iMaxCount)
	PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - 		iMaxDLCCount: ", iMaxDLCCount)

	IF (iMaxCount > 0 OR iMaxDLCCount > 0 OR coronaMenuData.iTotalCustomVehicles > 0)
	OR bIgnoreLockedOption
	
		BOOL bDefaultToFail
	
		// Loop to find the next available vehicle
		WHILE (NOT bFound)
		
			// Always reset this bool
			bDefaultToFail = FALSE
		
			//g_iMyRaceModelChoice += iIncrement
			coronaMenuData.iVehicleCoronaIndex += iIncrement
			
			IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE)
				
				PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - CUSTOM VEHICLE")
				
				IF iMaxCount + iMaxDLCCount = 0
					// Has player gone off custom vehicle list
					IF coronaMenuData.iVehicleCoronaIndex < 0
					
										
						//...yes, move back to our normal vehicles
						
						coronaMenuData.iVehicleCoronaIndex = (coronaMenuData.iTotalCustomVehicles-1)
						
						
					ELIF coronaMenuData.iVehicleCoronaIndex > (coronaMenuData.iTotalCustomVehicles - 1)
					
						//...yes, move back to the first default vehicle
						coronaMenuData.iVehicleCoronaIndex = 0
					ENDIF
				ELSE
					// Has player gone off custom vehicle list
					IF coronaMenuData.iVehicleCoronaIndex < 0
					
										
						//...yes, move back to our normal vehicles
						
						coronaMenuData.iVehicleCoronaIndex = ((iMaxCount + iMaxDLCCount)-1)
						
						SET_PLAYER_SELECTING_CUSTOM_VEHICLE(FALSE, TRUE)
						SET_PLAYER_SELECTING_DLC_VEHICLE(FALSE)
						
					ELIF coronaMenuData.iVehicleCoronaIndex > (coronaMenuData.iTotalCustomVehicles - 1)
					
						//...yes, move back to the first default vehicle
						coronaMenuData.iVehicleCoronaIndex = 0
						SET_PLAYER_SELECTING_CUSTOM_VEHICLE(FALSE, TRUE)
						SET_PLAYER_SELECTING_DLC_VEHICLE(FALSE)
					ENDIF
				ENDIF
				
				
			
			ELSE
				
				PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - STANDARD ORDERED VEHICLE: ", coronaMenuData.iVehicleCoronaIndex)
				
				// Has player gone off default vehicles
				IF coronaMenuData.iVehicleCoronaIndex < 0
				
					IF coronaMenuData.iTotalCustomVehicles > 0
					AND IS_CUSTOM_VEHICLE_TURNED_ON_FOR_COORNA(sSelection)
					
						//...yes, wrap round to it
						coronaMenuData.iVehicleCoronaIndex = coronaMenuData.iTotalCustomVehicles-1
						SET_PLAYER_SELECTING_CUSTOM_VEHICLE(TRUE, TRUE)
						SET_PLAYER_SELECTING_DLC_VEHICLE(FALSE)
					ELSE
						coronaMenuData.iVehicleCoronaIndex = (iMaxCount + iMaxDLCCount) - 1
					ENDIF
				
				// Has player gone off default vehicles
				ELIF coronaMenuData.iVehicleCoronaIndex >= (iMaxCount + iMaxDLCCount)
					
					// Do we have customvehicles
					IF coronaMenuData.iTotalCustomVehicles > 0
					AND IS_CUSTOM_VEHICLE_TURNED_ON_FOR_COORNA(sSelection)
					
						//...yes, wrap round to it
						coronaMenuData.iVehicleCoronaIndex = 0
						SET_PLAYER_SELECTING_CUSTOM_VEHICLE(TRUE, TRUE)
						SET_PLAYER_SELECTING_DLC_VEHICLE(FALSE)
					ELSE
						coronaMenuData.iVehicleCoronaIndex = 0
					ENDIF				
				ENDIF
								
			ENDIF
			
			IF NOT IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE)
						
				IF IS_CORONA_VEHICLE_DLC(coronaMenuData.orderedCoronaVehicles[coronaMenuData.iVehicleCoronaIndex])
					SET_PLAYER_SELECTING_DLC_VEHICLE(TRUE)
				ELSE
					SET_PLAYER_SELECTING_DLC_VEHICLE(FALSE)
				ENDIF
			ENDIF
						
			g_iMyRaceModelChoice = coronaMenuData.iVehicleCoronaIndex
			PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - g_iMyRaceModelChoice = ", g_iMyRaceModelChoice)
		
			// Get the model name of our custom vehicle			
			IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE)
				mnVehicle = GET_CUSTOM_VEHICLE_MODEL_NAME(g_sCoronaCustomVehiclesData.customVehicleArray[g_iMyRaceModelChoice])
			ELSE
				g_iMyRaceModelChoice = coronaMenuData.orderedCoronaVehicles[coronaMenuData.iVehicleCoronaIndex].iVehicleIndex
					
				IF DOES_JOB_USE_RACE_VEHICLE_SELECTION(IS_RACE())
					mnVehicle = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(GET_CREATOR_LIBRARY_FROM_RACE_CLASS(iClass), g_iMyRaceModelChoice)
				ELSE
					IF IS_CORONA_VEHICLE_DLC(coronaMenuData.orderedCoronaVehicles[coronaMenuData.iVehicleCoronaIndex])
						mnVehicle = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, g_iMyRaceModelChoice)
					ELSE
						mnVehicle = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, g_iMyRaceModelChoice)
					ENDIF
				ENDIF
				
				// If the index is -1 we have an invalid vehicle.
				IF coronaMenuData.orderedCoronaVehicles[coronaMenuData.iVehicleCoronaIndex].iVehicleIndex = -1
					PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - Out ordered index vehicle is invalid. SKIP checks below")
					bDefaultToFail = TRUE
				ENDIF
				
				IF IS_ARENA_WARS_JOB(TRUE)
				AND g_bArenaWarsUseForcedStockImperator
					PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - Only imperator stock vehicles are allowed. SKIP checks below")
					bDefaultToFail = TRUE
				ENDIF
			ENDIF
			
			// Check our updated values with our previous state (if same we have looped - get out)
			IF bPrevStateCust = IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE)
			AND bPrevStateDLC = IS_PLAYER_SELECTING_DLC_VEHICLE()
			AND iPrevVehicle = g_iMyRaceModelChoice
				PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - NOT found new vehicle")
			
				bFound = TRUE
				bFoundNewVeh = FALSE
				
			ELIF NOT bDefaultToFail
			AND IS_PLAYER_SELECTING_DLC_VEHICLE()
				
				PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - CHECKING DLC Vehicles")
				
				IF IS_DLC_CORONA_VEHICLE_VALID(iClass)
				
					PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - DLC Vehicle a new vehicle")
				
					bFound = TRUE
					bFoundNewVeh = TRUE
				ENDIF
								
			ELIF NOT bDefaultToFail
			AND IS_VEHICLE_VALID_FOR_RACE(sSelection, mnVehicle, iClassOverride)
				PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - FOUND new vehicle")
			
				bFound = TRUE
				bFoundNewVeh = TRUE
			ENDIF
			
			IF NOT IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE)
			AND NOT IS_PLAYER_SELECTING_DLC_VEHICLE()
				IF NOT bIncludeRandom
					IF coronaMenuData.iVehicleCoronaIndex = (iMaxCount + iMaxDLCCount)
					
						PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - FOUND new vehicle, RANDOM but this is not allowed. Keep searching")
					
						bFound = FALSE
						bFoundNewVeh = FALSE
					ENDIF
				ENDIF
			ENDIF			
				
		ENDWHILE
		
		PRINTLN("[CORONA] GET_NEXT_VALID_VEHICLE - vehicle: ", g_iMyRaceModelChoice, " Name: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicle), " Is Custom? ", IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(), TRUE))
		
		// Save our vehicle choice locally
		SET_PLAYER_CORONA_VEHICLE(mnVehicle, TRUE)
	ENDIF
	
	RETURN bFoundNewVeh
ENDFUNC

/// PURPOSE: Order our vehicle in alphabetical order.
PROC ORDER_CORONA_VEHICLES_ALPHABETICALLY_FOR_CLASS(CORONA_ORDERED_VEHICLE &vehicleInOrder[], INT iClass)

	INT i, j, iCompareResult
	INT iCounter
	CORONA_ORDERED_VEHICLE tempVeh
	
	INT iMaxVehicles = GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)
	
	PRINTLN("[CORONA] ORDER_CORONA_VEHICLES_ALPHABETICALLY_FOR_CLASS [STANDARD] - Class: ", iClass, " No. Of Vehicles: ", iMaxVehicles) 
	
	// Wipe all our data
	REPEAT COUNT_OF(vehicleInOrder) i
		vehicleInOrder[i].iVehicleIndex = -1
		vehicleInOrder[i].iVehicleInfoBS = 0
	ENDREPEAT
	
	// Populate our initial array of what is probably unordered list.
	REPEAT iMaxVehicles i
		IF GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i) != DUMMY_MODEL_FOR_SCRIPT
			vehicleInOrder[iCounter].iVehicleIndex = i
			vehicleInOrder[iCounter].iVehicleInfoBS = 0
			iCounter++
		ENDIF
	ENDREPEAT
	
	
	iMaxVehicles = GET_SELECTION_MAX_DLC_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)
	
	PRINTLN("[CORONA] ORDER_CORONA_VEHICLES_ALPHABETICALLY_FOR_CLASS [DLC] - Class: ", iClass, " No. Of Vehicles: ", iMaxVehicles) 
	
	// Populate our initial array of what is probably unordered list.
	REPEAT iMaxVehicles i
		IF GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i) != DUMMY_MODEL_FOR_SCRIPT
			vehicleInOrder[iCounter].iVehicleIndex = i
			SET_BIT(vehicleInOrder[iCounter].iVehicleInfoBS, CORONA_ORDERED_VEHICLE_IS_DLC)
			iCounter++
		ENDIF
	ENDREPEAT
	
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[CORONA] ORDER_CORONA_VEHICLES_ALPHABETICALLY_FOR_CLASS - BEFORE SORT") 
	REPEAT iCounter i
	
		IF NOT IS_CORONA_VEHICLE_DLC(vehicleInOrder[i])
			IF GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex) != DUMMY_MODEL_FOR_SCRIPT
			AND IS_MODEL_VALID(GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex))
				PRINTLN("[CORONA] ORDER_CORONA_VEHICLES_ALPHABETICALLY_FOR_CLASS - iVehicleInOrder[", i,"].iVehicleIndex = ",
				GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex))))
			ENDIF
		ELSE
			IF GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex) != DUMMY_MODEL_FOR_SCRIPT
			AND IS_MODEL_VALID(GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex))
				PRINTLN("[CORONA] ORDER_CORONA_VEHICLES_ALPHABETICALLY_FOR_CLASS - iVehicleInOrder[", i,"].iVehicleIndex = ",
				GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex))))
			ENDIF
		ENDIF
	ENDREPEAT
	PRINTLN("[CORONA] ORDER_CORONA_VEHICLES_ALPHABETICALLY_FOR_CLASS - END BEFORE SORT PRINT") 
	#ENDIF
	
	// Loop through our vehicles, ordering them alphabetically
	TEXT_LABEL_31 strVeh1
	TEXT_LABEL_31 strVeh2
	MODEL_NAMES mnVehI
	MODEL_NAMES mnVehJ
	
	REPEAT iCounter i
	
		IF IS_CORONA_VEHICLE_DLC(vehicleInOrder[i])
			mnVehI = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex)
		ELSE
			mnVehI = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex)
		ENDIF
	
		IF mnVehI != DUMMY_MODEL_FOR_SCRIPT
			FOR j=i+1 TO iCounter-1
			
				IF IS_CORONA_VEHICLE_DLC(vehicleInOrder[i])
					mnVehI = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex)
				ELSE
					mnVehI = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex)
				ENDIF
					
				IF IS_CORONA_VEHICLE_DLC(vehicleInOrder[j])
					mnVehJ = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[j].iVehicleIndex)
				ELSE
					mnVehJ = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[j].iVehicleIndex)
				ENDIF
			
				IF mnVehJ != DUMMY_MODEL_FOR_SCRIPT
				AND IS_MODEL_VALID(mnVehJ)
				AND IS_MODEL_VALID(mnVehI)
					
					strVeh1 = GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehJ))
					strVeh2 = GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehI))

					iCompareResult = COMPARE_STRINGS(strVeh1, strVeh2, FALSE, 5)
					
					IF iCompareResult < 0
						tempVeh = vehicleInOrder[j]
						vehicleInOrder[j] = vehicleInOrder[i]
						vehicleInOrder[i] = tempVeh
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[CORONA] ORDER_CORONA_VEHICLES_ALPHABETICALLY_FOR_CLASS - AFTER SORT") 
	REPEAT iCounter i
		IF NOT IS_CORONA_VEHICLE_DLC(vehicleInOrder[i])
			IF GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex) != DUMMY_MODEL_FOR_SCRIPT
			AND IS_MODEL_VALID(GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex))
				PRINTLN("[CORONA] ORDER_CORONA_VEHICLES_ALPHABETICALLY_FOR_CLASS - iVehicleInOrder[", i,"].iVehicleIndex = ",
				GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex))))
			ENDIF
		ELSE
			IF GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex) != DUMMY_MODEL_FOR_SCRIPT
			AND IS_MODEL_VALID(GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex))
				PRINTLN("[CORONA] ORDER_CORONA_VEHICLES_ALPHABETICALLY_FOR_CLASS - iVehicleInOrder[", i,"].iVehicleIndex = ",
				GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, vehicleInOrder[i].iVehicleIndex))))
			ENDIF
		ENDIF
	ENDREPEAT
	PRINTLN("[CORONA] ORDER_CORONA_VEHICLES_ALPHABETICALLY_FOR_CLASS - END AFTER SORT PRINT") 
	#ENDIF
	

ENDPROC

/// PURPOSE: Find a random vehicle for player. This includes normal, custom and DLC vehicles
FUNC BOOL SELECT_RANDOM_VEHICLE_FOR_RACE(CORONA_MENU_DATA &coronaMenuData)
	
	// Grab our totals for the two types of vehicles we can have
	INT iMaxStandardVehicles = GET_CLIENT_SELECTION_MAX(g_FMMC_STRUCT.iMissionType, ciRC_CLIENT_OPTION_VEHICLE, g_sTransitionSessionOptions, coronaMenuData.iTotalCustomVehicles)
	INT iMaxDLCVehicles = GET_SELECTION_MAX_DLC_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])
	INT iMaxCustomVehicles = coronaMenuData.iTotalCustomVehicles
	
	// Adjust our total if this is turned off
	IF NOT IS_CUSTOM_VEHICLE_TURNED_ON_FOR_COORNA(g_sTransitionSessionOptions)
		PRINTLN("[CORONA] SELECT_RANDOM_VEHICLE_FOR_RACE - Custom vehicles disabled")
		iMaxCustomVehicles = 0
	ENDIF
	
	PRINTLN("[CORONA] SELECT_RANDOM_VEHICLE_FOR_RACE - We have following amount of vehicles: ")
	PRINTLN("[CORONA] SELECT_RANDOM_VEHICLE_FOR_RACE - 		Standard Vehicles: 	", iMaxStandardVehicles)
	PRINTLN("[CORONA] SELECT_RANDOM_VEHICLE_FOR_RACE - 		DLC Vehicles: 		", iMaxDLCVehicles)
	PRINTLN("[CORONA] SELECT_RANDOM_VEHICLE_FOR_RACE - 		Custom Vehicles: 	", iMaxCustomVehicles)
	
	// Calculate the total vehicles we can randomise between and get the random int
	INT iTotalVehicles = (iMaxStandardVehicles + iMaxDLCVehicles + iMaxCustomVehicles)
	INT iRandomVehicle = GET_RANDOM_INT_IN_RANGE(0, iTotalVehicles)
	MODEL_NAMES mnRandomVeh = DUMMY_MODEL_FOR_SCRIPT
	MODEL_NAMES mnCurrentVeh = GET_PLAYER_CORONA_VEHICLE_NAME(PLAYER_ID(), TRUE)
	
	PRINTLN("[CORONA] SELECT_RANDOM_VEHICLE_FOR_RACE - Random vehicle chosen: ", iRandomVehicle)
	
	// Clear our current vehicle setup
	SET_PLAYER_SELECTING_DLC_VEHICLE(FALSE)
	SET_PLAYER_SELECTING_CUSTOM_VEHICLE(FALSE, TRUE)
	
	// Handle our normal vehicles
	IF iRandomVehicle < (iMaxStandardVehicles + iMaxDLCVehicles)

		coronaMenuData.iVehicleCoronaIndex = iRandomVehicle
		
		g_iMyRaceModelChoice = coronaMenuData.orderedCoronaVehicles[coronaMenuData.iVehicleCoronaIndex].iVehicleIndex
					
		IF IS_CORONA_VEHICLE_DLC(coronaMenuData.orderedCoronaVehicles[coronaMenuData.iVehicleCoronaIndex])
			SET_PLAYER_SELECTING_DLC_VEHICLE(TRUE)
			mnRandomVeh = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS], g_iMyRaceModelChoice)
		ELSE
			mnRandomVeh = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS], g_iMyRaceModelChoice)
		ENDIF
	
		PRINTLN("[CORONA] SELECT_RANDOM_VEHICLE_FOR_RACE - Random has picked a vehicle ", g_iMyRaceModelChoice, ", Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnRandomVeh))		
	ELSE

		g_iMyRaceModelChoice = (iRandomVehicle - (iMaxStandardVehicles + iMaxDLCVehicles))
		SET_PLAYER_SELECTING_CUSTOM_VEHICLE(TRUE, TRUE)
		mnRandomVeh = GET_CUSTOM_VEHICLE_MODEL_NAME(g_sCoronaCustomVehiclesData.customVehicleArray[(iRandomVehicle - (iMaxStandardVehicles + iMaxDLCVehicles))])
			
		PRINTLN("[CORONA] SELECT_RANDOM_VEHICLE_FOR_RACE - Random has picked a Custom vehicle ", (iRandomVehicle - (iMaxStandardVehicles + iMaxDLCVehicles)), ", Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnRandomVeh))
	ENDIF
	
	// Ensure the vehicle we have chosen is valid.
	IF IS_VEHICLE_VALID_FOR_RACE(g_sTransitionSessionOptions, mnRandomVeh)
	AND (mnRandomVeh != mnCurrentVeh)
		// Success, do we need to set anything? Play sound?
		
		SET_PLAYER_CORONA_VEHICLE(mnRandomVeh, TRUE)
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
		
		RETURN TRUE
	ELSE
		INT iIncrement = PICK_INT(GET_RANDOM_INT_IN_RANGE(0, 2) = 0, 1, -1)
		PRINTLN("[CORONA] SELECT_RANDOM_VEHICLE_FOR_RACE - Vehicle is not valid. Find next valid vehicle for Race, iIncrement = ", iIncrement)
		
		IF GET_NEXT_VALID_VEHICLE(coronaMenuData, iIncrement, g_sTransitionSessionOptions)
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
			PRINTLN("[CORONA] SELECT_RANDOM_VEHICLE_FOR_RACE - New random vehicle is ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_PLAYER_CORONA_VEHICLE_NAME(PLAYER_ID(), TRUE)))
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Listen for player input on vehicle selection screen 
FUNC BOOL MAINTAIN_CORONA_VEHICLE_SELECTION_INPUT(CORONA_MENU_DATA &coronaMenuData, FMMC_SELECTED_ITEMS &sSelection, TIME_DATATYPE &coronaOptionTimer)

	INT iIncrement
	BOOL bButtonPressed

	// If value should change, change it (LEFT / RIGHT for vehicle)
	IF SHOULD_SELECTION_BE_INCREMENTED(coronaOptionTimer, iIncrement, bButtonPressed, DEFAULT, DEFAULT, DEFAULT, FALSE, coronaMenuData.iCurrentSelection)
		PRINTLN("[CORONA] MAINTAIN_CORONA_VEHICLE_SELECTION_INPUT - finding new vehicle from: ", g_iMyRaceModelChoice)
		
		MODEL_NAMES mnCurrentVeh = GET_PLAYER_CORONA_VEHICLE_NAME(PLAYER_ID(), TRUE)

		IF GET_NEXT_VALID_VEHICLE(coronaMenuData, iIncrement, sSelection)
			PRINTLN("[CORONA] MAINTAIN_CORONA_VEHICLE_SELECTION_INPUT - found new vehicle: ", g_iMyRaceModelChoice)
			
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
			
			// Remove the vehicle assets if we have a valid previous model
			IF mnCurrentVeh != DUMMY_MODEL_FOR_SCRIPT
				REMOVE_VEHICLE_ASSET(mnCurrentVeh)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD

PROC PRINT_CORONA_VEHICLE_DATA_FOR_CLASS(INT iClass)

	PRINTLN("[CORONA] PRINT_CORONA_VEHICLE_DATA_FOR_CLASS ************")
	PRINTLN("[CORONA]		- Vehicle BitSet: ", g_FMMC_STRUCT.iAvailableVehicleBitSet[iClass])
	
	INT i
	INT iNumberOfVeh
	MODEL_NAMES mnVeh
	
	iNumberOfVeh = GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)
	PRINTLN("[CORONA]		- iNumberOfVeh: ", iNumberOfVeh)
	
	REPEAT iNumberOfVeh i
		
		mnVeh = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i, DEFAULT, TRUE)
		
		IF mnVeh != DUMMY_MODEL_FOR_SCRIPT
		
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableVehicleBitSet[iClass], i)
				PRINTLN("[CORONA]			- Vehicle ", i, ": ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVeh), " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVeh)), ") is NOT AVAILABLE")
			ELSE
				PRINTLN("[CORONA]			- Vehicle ", i, ": ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVeh), " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVeh)), ") is AVAILABLE")
			ENDIF
		ENDIF
	ENDREPEAT

	// Print out if all vehicles across the game are allowed.
	IF IS_BIT_SET(g_FMMC_STRUCT.iRaceClassBitSet, FMMC_VEHICLE_UPDATE_FUTURE_VEHICLES)
		PRINTLN("[CORONA]		- All Vehicles are AVAILABLE due to FMMC_VEHICLE_UPDATE_FUTURE_VEHICLES")
	ENDIF
	
	// Print out if all vehicles for this class are allowed.
	IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableVehicleBitSet[iClass], FMMC_VEHICLE_UPDATE_FUTURE_VEHICLES)
		PRINTLN("[CORONA]		- All Vehicles are AVAILABLE to this CLASS due to FMMC_VEHICLE_UPDATE_FUTURE_VEHICLES")
	ENDIF

	PRINTLN("[CORONA]")
	PRINTLN("[CORONA]		- DLC Vehicle BitSet: ", g_FMMC_STRUCT.iAvailDLCVehicleBS[iClass][0])
	PRINTLN("[CORONA]		- DLC Vehicle BitSet2: ", g_FMMC_STRUCT.iAvailDLCVehicleBS[iClass][1])
	PRINTLN("[CORONA]		- DLC Vehicle BitSet3: ", g_FMMC_STRUCT.iAvailDLCVehicleBS[iClass][2])
	
	iNumberOfVeh = GET_SELECTION_MAX_DLC_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, iClass)
	PRINTLN("[CORONA]		- DLC iNumberOfVeh: ", iNumberOfVeh)
	
	REPEAT iNumberOfVeh i
		
		mnVeh = GET_DLC_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(iClass, i)
		
		IF mnVeh != DUMMY_MODEL_FOR_SCRIPT
		AND IS_MODEL_VALID(mnVeh)
		
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailDLCVehicleBS[iClass][GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
				PRINTLN("[CORONA]			- DLC Vehicle ", i, ": ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVeh), " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVeh)), ") is AVAILABLE")
			ELSE
				PRINTLN("[CORONA]			- DLC Vehicle ", i, ": ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVeh), " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVeh)), ") is NOT AVAILABLE")
			ENDIF
		ENDIF
	ENDREPEAT
	PRINTLN("[CORONA] PRINT_CORONA_VEHICLE_DATA_FOR_CLASS ************")

ENDPROC

#ENDIF

/*
/// PURPOSE: Get the next available colour group for vehicle
PROC GET_NEXT_AVAILABLE_VEHICLE_COLOUR_TYPE(INT iIncrement)
	RETURN TRUE
	INT iModColor = ENUM_TO_INT(g_iMyRaceColorType) + iIncrement
	BOOL bFoundColour

	WHILE (NOT bFoundColour)
	
		IF iModColor < 0
			iModColor = (ENUM_TO_INT(MCT_NONE)-1)				// Maximum mod colour type
		ENDIF
		
		IF iModColor >=  ENUM_TO_INT(MCT_NONE)
			iModColor = 0
		ENDIF
	
		IF ciNUMBER_OF_VEHICLE_COLOURS > 0
			bFoundColour = TRUE
		ELSE
			iModColor += iIncrement
		ENDIF	
			
	ENDWHILE

	g_iMyRaceColorType = INT_TO_ENUM(MOD_COLOR_TYPE, iModColor)
ENDPROC
*/

FUNC BOOL IS_CORONA_COLOUR_VALID_FOR_VEHICLE(VEHICLE_INDEX veh, INT iOverrideColour = -1, MODEL_NAMES overrideModel = DUMMY_MODEL_FOR_SCRIPT)

	IF NOT DOES_ENTITY_EXIST(veh)
	AND overrideModel = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[CORONA] - IS_CORONA_COLOUR_VALID_FOR_VEHICLE - Veh doesnt exist, returning false")
		RETURN FALSE
	ENDIF

	MODEL_NAMES model = GET_ENTITY_MODEL(veh)
	INT iColour = g_iMyRaceColorSelection
	
	IF overrideModel != DUMMY_MODEL_FOR_SCRIPT
		model = overrideModel
	ENDIF
	
	IF iOverrideColour > -1
		iColour = iOverrideColour
	ENDIF
	
	SWITCH iColour
		CASE FM_RACE_COLOUR_BLACK
		BREAK
		CASE FM_RACE_COLOUR_BLACK_STEEL
			IF model = DOMINATOR2
			OR model = BUFFALO3
				RETURN FALSE
			ENDIF
		BREAK		
		CASE FM_RACE_COLOUR_SILVER
			IF model = DOMINATOR2
			OR model = BUFFALO3
				RETURN FALSE
			ENDIF
		BREAK		
		CASE FM_RACE_COLOUR_RED
		BREAK		
		CASE FM_RACE_COLOUR_SUNSET_RED
		
		BREAK		
		CASE FM_RACE_COLOUR_HOT_PINK
			IF model = DOMINATOR2
			OR model = BUFFALO3
				RETURN FALSE
			ENDIF
		BREAK		
		CASE FM_RACE_COLOUR_ORANGE
		BREAK		
		CASE FM_RACE_COLOUR_BRONZE
		BREAK		
		CASE FM_RACE_COLOUR_RACE_YELLOW
		BREAK		
		CASE FM_RACE_COLOUR_RACING_GREEN
		BREAK		
		CASE FM_RACE_COLOUR_BRIGHT_GREEN
		BREAK		
		CASE FM_RACE_COLOUR_BLUE
			IF model = DOMINATOR2
			OR model = BUFFALO3
				RETURN FALSE
			ENDIF
		BREAK		
		CASE FM_RACE_COLOUR_LIGHT_BLUE
			IF model = DOMINATOR2
			OR model = BUFFALO3
				RETURN FALSE
			ENDIF
		BREAK		
		CASE FM_RACE_COLOUR_CREEK_BROWN
		BREAK		
		CASE FM_RACE_COLOUR_BRIGHT_PURPLE
			IF model = DOMINATOR2
			OR model = BUFFALO3
				RETURN FALSE
			ENDIF
		BREAK		
		CASE FM_RACE_COLOUR_ICE_WHITE
			IF model = DOMINATOR2
			OR model = BUFFALO3
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Loops around until we reach another valid colour for corona
PROC GET_NEXT_VALID_CORONA_VEHICLE_COLOUR(CORONA_MENU_DATA &coronaMenuData, INT iIncrement = 1)
	
	BOOL bFound
	//INT iMaxCount = GET_NUM_MOD_COLORS(g_iMyRaceColorType, TRUE)
	INT iMaxCount =	MAX_RACE_COLOUR
	INT iMinCount = 0
	
	GAMER_HANDLE aGamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	IF IS_PLAYER_IN_ACTIVE_CLAN(aGamerHandle)
		iMinCount = -1
	ENDIF
	
	WHILE (NOT bFound)
		
		g_iMyRaceColorSelection += iIncrement
		
		IF g_iMyRaceColorSelection = FM_RACE_COLOUR_DEFAULT
			IF NOT IS_DEFAULT_COLOUR_SELECTION_AVAILABLE()
				PRINTLN("[CORONA] GET_NEXT_VALID_CORONA_VEHICLE_COLOUR - Default colour not available, skip ", g_iMyRaceColorSelection)
				g_iMyRaceColorSelection += iIncrement
			ENDIF
		ENDIF
		
		IF g_iMyRaceColorSelection >= iMaxCount
			g_iMyRaceColorSelection = iMinCount
		ENDIF
		
		IF g_iMyRaceColorSelection < iMinCount
			g_iMyRaceColorSelection = iMaxCount-1
		ENDIF
					
		SET_UP_PLAYERS_VEHICLE_COLOURS(coronaMenuData.vehDisplayedVehicle)
		
		IF g_iMyRaceColorSelection = -1
			PRINTLN("[CORONA] GET_NEXT_VALID_CORONA_VEHICLE_COLOUR - g_iMyRaceColorSelection = -1")
			bFound = TRUE
		ELSE
			IF IS_CORONA_COLOUR_VALID_FOR_VEHICLE(coronaMenuData.vehDisplayedVehicle)		
				bFound = TRUE
			ENDIF
		ENDIF
	ENDWHILE
	
ENDPROC

FUNC INT GET_RANDOM_VALID_COLOUR_FOR_VEHICLE(MODEL_NAMES model)
	
	INT iRandColour = GET_RANDOM_INT_IN_RANGE(0, MAX_RACE_COLOUR)
	
	IF model = DOMINATOR2
	OR model = BUFFALO3
		INT iRandTemp = GET_RANDOM_INT_IN_RANGE(0, 9)
		IF iRandTemp = 0 iRandColour = FM_RACE_COLOUR_BLACK 		ENDIF
		IF iRandTemp = 1 iRandColour = FM_RACE_COLOUR_RED 			ENDIF
		IF iRandTemp = 2 iRandColour = FM_RACE_COLOUR_SUNSET_RED 	ENDIF
		IF iRandTemp = 3 iRandColour = FM_RACE_COLOUR_ORANGE 		ENDIF
		IF iRandTemp = 4 iRandColour = FM_RACE_COLOUR_BRONZE 		ENDIF
		IF iRandTemp = 5 iRandColour = FM_RACE_COLOUR_RACE_YELLOW 	ENDIF
		IF iRandTemp = 6 iRandColour = FM_RACE_COLOUR_RACING_GREEN 	ENDIF
		IF iRandTemp = 7 iRandColour = FM_RACE_COLOUR_BRIGHT_GREEN 	ENDIF
		IF iRandTemp = 8 iRandColour = FM_RACE_COLOUR_CREEK_BROWN 	ENDIF
	ENDIF
	
	RETURN iRandColour
ENDFUNC


/// PURPOSE: Picks a random colour / livery for current vehicle    
PROC GET_RANDOM_CORONA_VEHICLE_COLOUR(CORONA_MENU_DATA &coronaMenuData)
	
	g_iMyRaceColorType = MCT_METALLIC
	
	IF DOES_ENTITY_EXIST(coronaMenuData.vehDisplayedVehicle)
	AND NOT IS_ENTITY_DEAD(coronaMenuData.vehDisplayedVehicle)
		IF DOES_CORONA_VEHICLE_HAVE_LIVERY_MODS(PLAYER_ID(), coronaMenuData.vehDisplayedVehicle, TRUE)
			g_iMyRaceLiverySelection = GET_RANDOM_INT_IN_RANGE(0, GET_NUM_VEHICLE_MODS(coronaMenuData.vehDisplayedVehicle, MOD_LIVERY))
		ELIF GET_VEHICLE_LIVERY_COUNT(coronaMenuData.vehDisplayedVehicle) > 1
			g_iMyRaceLiverySelection = GET_RANDOM_INT_IN_RANGE(0, GET_VEHICLE_LIVERY_COUNT(coronaMenuData.vehDisplayedVehicle))
		ELSE
			IF DOES_RACE_MODEL_ALLOW_COLOURS(GET_ENTITY_MODEL(coronaMenuData.vehDisplayedVehicle))
				g_iMyRaceColorSelection = GET_RANDOM_VALID_COLOUR_FOR_VEHICLE(GET_ENTITY_MODEL(coronaMenuData.vehDisplayedVehicle))
				GET_NEXT_VALID_CORONA_VEHICLE_COLOUR(coronaMenuData)
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[CORONA] GET_RANDOM_CORONA_VEHICLE_COLOUR - Random Colour picked: TYPE: ", GET_VEH_COLOUR_TYPE(), " Colour: ", g_iMyRaceColorSelection, " Livery: ", PICK_STRING(GET_VEHICLE_LIVERY_COUNT(coronaMenuData.vehDisplayedVehicle) > 0, "TRUE", "FALSE"))
ENDPROC


/// PURPOSE: Listen for player input on vehicle selection screen 
FUNC BOOL MAINTAIN_CORONA_VEHICLE_COLOUR_SELECTION_INPUT(CORONA_MENU_DATA &coronaMenuData)

	INT iIncrement
	BOOL bButtonPressed

	// If value should change, change it (UP / DOWN for colour)
	IF SHOULD_SELECTION_BE_INCREMENTED(coronaMenuData.lobbyOptionTimer, iIncrement, bButtonPressed, TRUE, FALSE, -1, TRUE, coronaMenuData.iCurrentSelection )
	
		IF DOES_ENTITY_EXIST(coronaMenuData.vehDisplayedVehicle)
		AND NOT IS_ENTITY_DEAD(coronaMenuData.vehDisplayedVehicle)
		AND NOT IS_CORONA_BIT_SET(CORONA_NEW_VEHICLE_LOADING)
			
			GET_NEXT_VALID_CORONA_VEHICLE_COLOUR(coronaMenuData, iIncrement)
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_ARENA_WARS_CORONA_VEHICLE_LIVERY_SELCTION_INPUT(CORONA_MENU_DATA &coronaMenuData, INT iIncrement, INT iMaxCount)

	BOOL bFound = FALSE
	INT iLastLivery = g_iMyRaceLiverySelection

	WHILE (!bFound)
		g_iMyRaceLiverySelection += iIncrement
		IF g_iMyRaceLiverySelection >= iMaxCount
			g_iMyRaceLiverySelection = -1
			
			bFound = TRUE
		ENDIF
		
		IF g_iMyRaceLiverySelection < -1
			g_iMyRaceLiverySelection = iMaxCount-1
		ENDIF
		
		IF IS_CORONA_VEHICLE_MOD_UNLOCKED(coronaMenuData.vehDisplayedVehicle, CVT_LIVERY, g_iMyRaceLiverySelection)
		OR g_iMyRaceLiverySelection = g_ArenaWarsCurrentLivery
			PRINTLN("[CORONA][GENERIC_JOB] MAINTAIN_ARENA_WARS_CORONA_VEHICLE_LIVERY_SELCTION_INPUT - Found new livery that has been unlocked ", g_iMyRaceLiverySelection)
			bFound = TRUE
		ENDIF
		
		IF g_iMyRaceLiverySelection = iLastLivery
			bFound = TRUE
		ENDIF
		
	ENDWHILE
	
	RETURN bFound

ENDFUNC

FUNC BOOL IS_PROBLEM_LIVERY(MODEL_NAMES vehModel, INT iLivery)
	SWITCH vehModel
		CASE FORMULA2
			SWITCH iLivery
				CASE 0
				CASE 4
					RETURN TRUE
			ENDSWITCH
		BREAK
		CASE BRIOSO
			SWITCH iLivery
				CASE 3
				CASE 4
					RETURN TRUE
			ENDSWITCH
		BREAK
		CASE HOTRING
			SWITCH iLivery
				CASE 30
					IF GET_MP_INT_CHARACTER_STAT(MP_STAT_COUNT_HOTRING_RACE) < GET_NUM_HOTRACE_NEEDED_FOR_LIVERY_UNLOCK(iLivery)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_CORONA_VEHICLE_LIVERY_SELECTION_INPUT(CORONA_MENU_DATA &coronaMenuData)
	INT iIncrement
	BOOL bButtonPressed
	
	BOOL bFound
	
	IF SHOULD_SELECTION_BE_INCREMENTED(coronaMenuData.lobbyOptionTimer, iIncrement, bButtonPressed, TRUE, FALSE, -1, TRUE, coronaMenuData.iCurrentSelection)
	
		PRINTLN("[CORONA] MAINTAIN_CORONA_VEHICLE_LIVERY_SELECTION_INPUT - iIncrement = ", iIncrement)
	
		IF DOES_ENTITY_EXIST(coronaMenuData.vehDisplayedVehicle)
		AND NOT IS_ENTITY_DEAD(coronaMenuData.vehDisplayedVehicle)
		AND NOT IS_CORONA_BIT_SET(CORONA_NEW_VEHICLE_LOADING)
		
			INT iMaxCount
		
			IF DOES_CORONA_VEHICLE_HAVE_LIVERY_MODS(PLAYER_ID(), coronaMenuData.vehDisplayedVehicle, TRUE)
				iMaxCount = GET_NUM_VEHICLE_MODS(coronaMenuData.vehDisplayedVehicle, MOD_LIVERY)
				MODEL_NAMES model = GET_ENTITY_MODEL(coronaMenuData.vehDisplayedVehicle)
				
				IF IS_ARENA_WARS_JOB(TRUE) AND IS_VEHICLE_AN_ARENA_CONTENDER_VEHICLE(model) AND IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID(),TRUE)
					RETURN MAINTAIN_ARENA_WARS_CORONA_VEHICLE_LIVERY_SELCTION_INPUT(coronaMenuData, iIncrement, iMaxCount)
				ELSE
				
					INT iLastLivery = g_iMyRaceLiverySelection
				
					WHILE (!bFound)
						g_iMyRaceLiverySelection += iIncrement
						PRINTLN("[CORONA] MAINTAIN_CORONA_VEHICLE_LIVERY_SELECTION_INPUT - Checking livery ", g_iMyRaceLiverySelection)
					
						IF g_iMyRaceLiverySelection >= iMaxCount
							g_iMyRaceLiverySelection = -1
						ENDIF
						
						IF g_iMyRaceLiverySelection < -1
							g_iMyRaceLiverySelection = iMaxCount-1
						ENDIF
					
						IF NOT IS_PROBLEM_LIVERY(model, g_iMyRaceLiverySelection)
						AND NOT IS_VEHICLE_MOD_LOCKED(coronaMenuData.vehDisplayedVehicle, MOD_LIVERY, g_iMyRaceLiverySelection)
						AND CHECK_LIVERY_IS_VALID(model, GET_MOD_TEXT_LABEL(coronaMenuData.vehDisplayedVehicle, MOD_LIVERY, g_iMyRaceLiverySelection), coronaMenuData.vehDisplayedVehicle, g_iMyRaceLiverySelection, FALSE)
							bFound = TRUE
							PRINTLN("[CORONA] MAINTAIN_CORONA_VEHICLE_LIVERY_SELECTION_INPUT - Found available livery = ", g_iMyRaceLiverySelection)
						ENDIF
						
						IF g_iMyRaceLiverySelection = iLastLivery
							bFound = TRUE
							PRINTLN("[CORONA] MAINTAIN_CORONA_VEHICLE_LIVERY_SELECTION_INPUT - Full loop completed and no new one found, g_iMyRaceLiverySelection = iLastLivery (", g_iMyRaceLiverySelection, ")")
						ENDIF
					ENDWHILE
					
				ENDIF
				
			ELIF GET_VEHICLE_LIVERY_COUNT(coronaMenuData.vehDisplayedVehicle) > 1 AND NOT IS_LIVERY_EXCLUDED(coronaMenuData.vehDisplayedVehicle)
				iMaxCount = GET_VEHICLE_LIVERY_COUNT(coronaMenuData.vehDisplayedVehicle)
				
				g_iMyRaceLiverySelection += iIncrement
				IF g_iMyRaceLiverySelection >= iMaxCount
					g_iMyRaceLiverySelection = 0
				ENDIF
				
				IF g_iMyRaceLiverySelection < 0
					g_iMyRaceLiverySelection = iMaxCount-1
				ENDIF
			
			ENDIF
		
			IF GET_ENTITY_MODEL(coronaMenuData.vehDisplayedVehicle) = HOTRING
				g_iMyRaceLiverySelectionHotring = g_iMyRaceLiverySelection
			ENDIF
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_CORONA_VEHICLE_PARACHUTE_SELECTION_INPUT(CORONA_MENU_DATA &coronaMenuData)
	INT iIncrement
	BOOL bButtonPressed
	
	IF SHOULD_SELECTION_BE_INCREMENTED(coronaMenuData.lobbyOptionTimer, iIncrement, bButtonPressed, TRUE, FALSE, -1, TRUE, coronaMenuData.iCurrentSelection)
	
		g_iMyRaceParachuteSelection += iIncrement
		
		IF g_iMyRaceParachuteSelection > 8
			g_iMyRaceParachuteSelection = 0
		ENDIF
		
		IF g_iMyRaceParachuteSelection < 0
			g_iMyRaceParachuteSelection = 8
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_CORONA_VEHICLE_TYRES_SELECTION_INPUT(CORONA_MENU_DATA &coronaMenuData)
	INT iIncrement
	BOOL bButtonPressed
	
	IF SHOULD_SELECTION_BE_INCREMENTED(coronaMenuData.lobbyOptionTimer, iIncrement, bButtonPressed, TRUE, FALSE, -1, TRUE, coronaMenuData.iCurrentSelection)
	
		g_iMyRaceTyresSelection += iIncrement
		
		IF g_iMyRaceTyresSelection > 2
			g_iMyRaceTyresSelection = 0
		ENDIF
		
		IF g_iMyRaceTyresSelection < 0
			g_iMyRaceTyresSelection = 2
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_DISPLAYED_VEHICLE_CORONA_MOD_COUNT(CORONA_MENU_DATA &coronaMenuData, CORONA_VEH_MOD_TYPE eModType)
	IF DOES_ENTITY_EXIST(coronaMenuData.vehDisplayedVehicle)
		RETURN GET_NUM_VEHICLE_MODS(coronaMenuData.vehDisplayedVehicle, GET_CORONA_VEH_MOD_TYPE(coronaMenuData.vehDisplayedVehicle, eModType))
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL MAINTAIN_CORONA_VEHICLE_ARMOUR_SELECTION_INPUT(CORONA_MENU_DATA &coronaMenuData)
	INT iIncrement
	BOOL bButtonPressed
	BOOL bFound = FALSE
	
	IF SHOULD_SELECTION_BE_INCREMENTED(coronaMenuData.lobbyOptionTimer, iIncrement, bButtonPressed, TRUE, FALSE, -1, TRUE, coronaMenuData.iCurrentSelection)
	
		IF GET_DISPLAYED_VEHICLE_CORONA_MOD_COUNT(coronaMenuData, CVT_ARMOUR) > 0
	
			INT iLastArmour = g_iMyArenaWarsArmourSelection
			
			WHILE (!bFound)
				g_iMyArenaWarsArmourSelection += iIncrement
				
				IF g_iMyArenaWarsArmourSelection > GET_DISPLAYED_VEHICLE_CORONA_MOD_COUNT(coronaMenuData, CVT_ARMOUR)-1
					g_iMyArenaWarsArmourSelection = -1
					
					bFound = TRUE
				ENDIF
				
				IF g_iMyArenaWarsArmourSelection < -1
					g_iMyArenaWarsArmourSelection = GET_DISPLAYED_VEHICLE_CORONA_MOD_COUNT(coronaMenuData, CVT_ARMOUR)-1
				ENDIF
				
				IF IS_CORONA_VEHICLE_MOD_UNLOCKED(coronaMenuData.vehDisplayedVehicle, CVT_ARMOUR, g_iMyArenaWarsArmourSelection)
				OR g_iMyArenaWarsArmourSelection = g_ArenaWarsCurrentArmour
					PRINTLN("[CORONA][GENERIC_JOB] MAINTAIN_CORONA_VEHICLE_ARMOUR_SELECTION_INPUT - Found new armour that has been unlocked ", g_iMyArenaWarsArmourSelection)
					bFound = TRUE
				ENDIF
				
				IF g_iMyArenaWarsArmourSelection = iLastArmour
					bFound = TRUE
				ENDIF
			
			ENDWHILE
		ENDIF
	ENDIF
	
	RETURN bFound
ENDFUNC

FUNC BOOL MAINTAIN_CORONA_VEHICLE_SCOOPS_AND_RAMBARS_SELECTION_INPUT(CORONA_MENU_DATA &coronaMenuData)
	INT iIncrement
	BOOL bButtonPressed
	BOOL bFound = FALSE
	
	IF SHOULD_SELECTION_BE_INCREMENTED(coronaMenuData.lobbyOptionTimer, iIncrement, bButtonPressed, TRUE, FALSE, -1, TRUE, coronaMenuData.iCurrentSelection)
	
		IF GET_DISPLAYED_VEHICLE_CORONA_MOD_COUNT(coronaMenuData, CVT_SCOOP_RAMBAR) > 0
	
			INT iLastRambar = g_iMyArenaWarsScoopRambarSelection
			
			WHILE (!bFound)
				g_iMyArenaWarsScoopRambarSelection += iIncrement
				
				IF g_iMyArenaWarsScoopRambarSelection > GET_DISPLAYED_VEHICLE_CORONA_MOD_COUNT(coronaMenuData, CVT_SCOOP_RAMBAR)-1
					g_iMyArenaWarsScoopRambarSelection = -1
					
					bFound = TRUE
				ENDIF
				
				IF g_iMyArenaWarsScoopRambarSelection < -1
					g_iMyArenaWarsScoopRambarSelection = GET_DISPLAYED_VEHICLE_CORONA_MOD_COUNT(coronaMenuData, CVT_SCOOP_RAMBAR)-1
				ENDIF
				
				IF IS_CORONA_VEHICLE_MOD_UNLOCKED(coronaMenuData.vehDisplayedVehicle, CVT_SCOOP_RAMBAR, g_iMyArenaWarsScoopRambarSelection)
				OR g_iMyArenaWarsScoopRambarSelection = g_ArenaWarsCurrentScoopRambar
					PRINTLN("[CORONA][GENERIC_JOB] MAINTAIN_CORONA_VEHICLE_SCOOPS_AND_RAMBARS_SELECTION_INPUT - Found new ram bar that has been unlocked ", g_iMyArenaWarsScoopRambarSelection)
					bFound = TRUE
				ENDIF
				
				IF g_iMyArenaWarsScoopRambarSelection = iLastRambar
					bFound = TRUE
				ENDIF
			
			ENDWHILE
			
		ENDIF
	ENDIF
	
	RETURN bFound
ENDFUNC

FUNC BOOL MAINTAIN_CORONA_VEHICLE_MINES_SELECTION_INPUT(CORONA_MENU_DATA &coronaMenuData)
	INT iIncrement
	BOOL bButtonPressed
	BOOL bFound = FALSE
	
	IF SHOULD_SELECTION_BE_INCREMENTED(coronaMenuData.lobbyOptionTimer, iIncrement, bButtonPressed, TRUE, FALSE, -1, TRUE, coronaMenuData.iCurrentSelection)
	
		IF GET_DISPLAYED_VEHICLE_CORONA_MOD_COUNT(coronaMenuData, CVT_MINE) > 0
	
			INT iLastMine = g_iMyArenaWarsMinesSelection
			
			WHILE (!bFound)
	
				g_iMyArenaWarsMinesSelection += iIncrement
				PRINTLN("[CORONA][GENERIC_JOB] MAINTAIN_CORONA_VEHICLE_MINES_SELECTION_INPUT - g_iMyArenaWarsMinesSelection = ",g_iMyArenaWarsMinesSelection)
				
				IF g_iMyArenaWarsMinesSelection > GET_DISPLAYED_VEHICLE_CORONA_MOD_COUNT(coronaMenuData, CVT_MINE)-1
					g_iMyArenaWarsMinesSelection = -1
					
					bFound = TRUE
					PRINTLN("[CORONA][GENERIC_JOB] MAINTAIN_CORONA_VEHICLE_MINES_SELECTION_INPUT - Reached end, set to none")
				ENDIF
				
				IF g_iMyArenaWarsMinesSelection < -1
					g_iMyArenaWarsMinesSelection = GET_DISPLAYED_VEHICLE_CORONA_MOD_COUNT(coronaMenuData, CVT_MINE)-1
					PRINTLN("[CORONA][GENERIC_JOB] MAINTAIN_CORONA_VEHICLE_MINES_SELECTION_INPUT - Reached beginning, set to end")
				ENDIF
				
				IF IS_CORONA_VEHICLE_MOD_UNLOCKED(coronaMenuData.vehDisplayedVehicle, CVT_MINE, g_iMyArenaWarsMinesSelection)
				OR g_iMyArenaWarsMinesSelection = g_ArenaWarsCurrentMines
					PRINTLN("[CORONA][GENERIC_JOB] MAINTAIN_CORONA_VEHICLE_MINES_SELECTION_INPUT - Found new mines that have been unlocked ",g_iMyArenaWarsMinesSelection)
					bFound = TRUE
				ENDIF
				
				IF g_iMyArenaWarsMinesSelection = iLastMine
					bFound = TRUE
					PRINTLN("[CORONA][GENERIC_JOB] MAINTAIN_CORONA_VEHICLE_MINES_SELECTION_INPUT - Reached previously selected")
				ENDIF
			
			ENDWHILE
			
		ENDIF
	ENDIF
	
	RETURN bFound
ENDFUNC

/// PURPOSE: Syncs the data from host to client
FUNC BOOL MAINTAIN_SYNC_OF_CORONA_HOST_OPTIONS(INT iMissionType, INT &iLocalSyncID, FMMC_SELECTED_ITEMS &sSelection, INT iMyGBD)
	
	IF sSelection.iSelectionChangeID != iLocalSyncID
			
		PRINTLN("[CORONA] MAINTAIN_SYNC_OF_CORONA_HOST_OPTIONS - syncing host options, iLocalSyncID = ", iLocalSyncID, ", sSelection.iSelectionChangeID = ", sSelection.iSelectionChangeID)
		
		iLocalSyncID = sSelection.iSelectionChangeID
		
		IF iMissioNType = FMMC_TYPE_RACE
			
			IF GlobalplayerBD_FM[iMyGBD].sMenuSelection.iSelection[ciRC_HOST_OPTION_TYPE] != sSelection.iSelection[ciRC_HOST_OPTION_TYPE]
				SETUP_CORONA_CLIENT_OPTIONS_DEFAULT(iMissionType)
			ENDIF
		ENDIF
		
		
		// Update our host data in case we lost the host
		GlobalplayerBD_FM[iMyGBD].sMenuSelection = sSelection
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if the custom vehicle option can be included in corona
FUNC BOOL IS_CORONA_CUSTOM_VEHICLE_OPTION_VALID(CORONA_MENU_DATA &coronaMenuData)
	
	// It has to be a mission
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		// The mission has to have been setup to allow this choice
		IF IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet, ciALLOW_VEHICLE_CHOICE)
			// The player needs to have custom vehicles to choose from
			IF coronaMenuData.iTotalCustomVehicles > 0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ************* SAVE PLAYER OPTIONS FROM CORONA **************
PROC SAVE_PLAYER_CORONA_CHOICES(CORONA_MENU_DATA &coronaMenuData, INT iMissionType, FMMC_SELECTED_ITEMS &sSelection, BOOL bStartingJob = FALSE)
		
	PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Start")
	DEBUG_PRINTCALLSTACK()
		
	IF iMissionType = FMMC_TYPE_RACE
		
//		BOOL bCustom = (sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_CUSTOM)
//		INT iSelectionMax = GET_SELECTION_MAX_VEHICLE_TYPE(g_FMMC_STRUCT.iRaceType, sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])		
		
		g_iMyRaceModelChoice = GET_PLAYER_CORONA_VEHICLE(PLAYER_ID())
		
		// Update our colour to locked colours
		g_iMyRaceColorType = coronaMenuData.iSelectedColourType
		g_iMyRaceColorSelection = coronaMenuData.iSelectedColourIndex
		g_iMyRaceLiverySelection = coronaMenuData.iSelectedLivery
		
		IF bStartingJob
		AND IS_OPEN_WHEEL_RACE()
			g_iMyOpenWheelVehicleSelection = g_iMyRaceModelChoice
			g_iMyOpenWheelColourSelection = g_iMyRaceColorSelection
			g_iMyOpenWheelLiverySelection = g_iMyRaceLiverySelection
			PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Open wheel - g_iMyOpenWheelVehicleSelection = ", g_iMyOpenWheelVehicleSelection)
			PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Open wheel - g_iMyOpenWheelColourSelection = ", g_iMyOpenWheelColourSelection)
			PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Open wheel - g_iMyOpenWheelLiverySelection = ", g_iMyOpenWheelLiverySelection)
		ENDIF
		
		PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - colour: ", g_iMyRaceColorSelection)
		
		SAVE_CORONA_DEFAULT_COLOUR(coronaMenuData)
				
		// If player is selecting custom update our choice to the save slot
		IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID())
			g_iMyRaceModelChoice = g_sCoronaCustomVehiclesData.customVehicleArray[g_iMyRaceModelChoice]
		ENDIF
		
		g_mnMyRaceModel = GET_PLAYER_CORONA_VEHICLE_NAME(PLAYER_ID())
		
		#IF IS_DEBUG_BUILD
			IF g_mnMyRaceModel != DUMMY_MODEL_FOR_SCRIPT
			AND IS_MODEL_VALID(g_mnMyRaceModel)
				PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - saving PLAYER CHOSEN vehicle: ", g_iMyRaceModelChoice, " name: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_mnMyRaceModel))
			ELSE
				PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - saving PLAYER CHOSEN vehicle: ", g_iMyRaceModelChoice, " name: DUMMY_MODEL_FOR_SCRIPT")
			ENDIF
		#ENDIF
		
		// Save our colour choice
		SAVE_CORONA_DEFAULT_COLOUR(coronaMenuData)
		
		// Safety check for rally races (should never be hit now)
		IF IS_THIS_A_RALLY_RACE_TEMP(sSelection)
		OR IS_THIS_CORONA_A_GTA_TEAM_RACE(sSelection)
			IF NOT DOES_VEHICLE_HAVE_TWO_OR_MORE_SEATS(g_mnMyRaceModel)		

				g_iMyRaceModelChoice 				= 0
				IF sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS] = FMMC_VEHICLE_CLASS_BIKES
					g_iMyRaceModelChoice 				= 1
				ENDIF
								
				g_mnMyRaceModel			= GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS], g_iMyRaceModelChoice, DEFAULT, TRUE)
				
				
				
				IF g_mnMyRaceModel != DUMMY_MODEL_FOR_SCRIPT
				AND IS_MODEL_VALID(g_mnMyRaceModel)
					PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - overriding vehicle for RALLY: ", g_iMyRaceModelChoice, " name: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_mnMyRaceModel))
				ELSE
					PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - overriding vehicle for RALLY: ", g_iMyRaceModelChoice, " name: DUMMY_MODEL_FOR_SCRIPT")
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAVE_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED()
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.ghPreferredPartner			= g_ghPreferredPartnerHandle
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredPartner 			= g_iMyPreferedPartner
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole 				= g_iMyPreferedPosition
			
			PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - g_iMyPreferedPartner: ", g_iMyPreferedPartner, " , g_iMyPreferedPosition: ", g_iMyPreferedPosition)
		ENDIF
		
		g_iMyCurrentClass = GET_CLASS_FOR_CLOUD_FROM_SELECTION(sSelection.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])
		
		PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - colour type: ", g_iMyRaceColorType, " colour index: ", g_iMyRaceColorSelection)
	ELSE
		// Print out mission values
		PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - g_TransitionSessionNonResetVars.PlayerMissionStartLocation = ", g_TransitionSessionNonResetVars.PlayerMissionStartLocation)
		
		// don't update our team if we are already balanced
		IF NOT HAVE_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED()
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = g_FMMC_STRUCT.iMyTeam
			PRINTLN("SAVE_PLAYER_CORONA_CHOICES - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", g_FMMC_STRUCT.iMyTeam)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen < MAX_MISS_CONTROLER_TEAMS
		PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Saved outfit = ", GET_LOCAL_DEFAULT_OUTFIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen))
		ENDIF
		#ENDIF
		
		
		// Reset our globals
		IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
		AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA() //2059300
			IF NOT DOES_JOB_USE_RACE_VEHICLE_SELECTION()
				g_mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT
				g_iMyRaceModelChoice = 0
				PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - g_mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT, check for custom vehicles")
			ENDIF
			
			IF IS_CORONA_CUSTOM_VEHICLE_OPTION_VALID(coronaMenuData)
			AND GET_CORONA_STATUS() > CORONA_STATUS_BALANCE_AND_LOAD
				
				// Set the globals to the correct values for custom vehicle
				IF coronaMenuData.iVehicleCoronaIndex >= 0
					g_mnMyRaceModel = GET_CUSTOM_VEHICLE_MODEL_NAME(g_sCoronaCustomVehiclesData.customVehicleArray[coronaMenuData.iVehicleCoronaIndex])
					g_iMyRaceModelChoice = g_sCoronaCustomVehiclesData.customVehicleArray[coronaMenuData.iVehicleCoronaIndex]

					IF g_mnMyRaceModel != DUMMY_MODEL_FOR_SCRIPT
					AND IS_MODEL_VALID(g_mnMyRaceModel)
						PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Job has a custom vehicle to spawn: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_mnMyRaceModel), ", Index = ", g_iMyRaceModelChoice)
					ELSE
						PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Job has a custom vehicle to spawn: DUMMY_MODEL_FOR_SCRIPT, Index = ", g_iMyRaceModelChoice)
					ENDIF
					
					// Clear the flag to say it should spawn at mors:
					IF g_iMyRaceModelChoice != -1
					AND IS_BIT_SET(g_MpSavedVehicles[g_iMyRaceModelChoice].iVehicleBS, MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
						PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Clear flag to indicate it should be spawned at MORS")
						CLEAR_BIT(g_MpSavedVehicles[g_iMyRaceModelChoice].iVehicleBS, MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
					ENDIF
				ELSE 
					g_mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT
					g_iMyRaceModelChoice = -1
					PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Job has selected NONE as custom vehicle to spawn")			
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
			PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - IS_CORONA_INITIALISING_A_QUICK_RESTART()")
		ENDIF
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - IS_A_STRAND_MISSION_BEING_INITIALISED()")
		ENDIF
		IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
			PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()")
		ENDIF
		IF IS_ARENA_WARS_JOB()
			PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - IS_ARENA_WARS_JOB()")
		ENDIF
		#IF FEATURE_TUNER
		IF DOES_JOB_USE_RACE_VEHICLE_SELECTION()
			PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - DOES_JOB_USE_RACE_VEHICLE_SELECTION()")
		ENDIF
		#ENDIF
		#ENDIF
		
		// If not doing a quick restart, strand or rounds then save out a vehicle we may have chosen
		IF (NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
		AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA())
		OR DOES_JOB_USE_RACE_VEHICLE_SELECTION()
			
			INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
			PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - iTeam = ", iTeam)
			
			BOOL bClearVehicleData = TRUE
			
			IF IS_THIS_A_KING_OF_THE_HILL_TEAM_CORONA(g_FMMC_STRUCT.iMissionType, g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE])
			AND IS_INT_IN_RANGE(iTeam, 0, FMMC_MAX_TEAMS)
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST)
				bClearVehicleData = FALSE
				PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Team king of the hill, ciBS_TEAM_USES_CORONA_VEHICLE_LIST set for team ", iTeam, ", bClearVehicleData = FALSE")
			ELIF IS_THIS_A_KING_OF_THE_HILL_CORONA(g_FMMC_STRUCT.iMissionType, g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE])
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[0].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST)
				bClearVehicleData = FALSE
				PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Normal king of the hill, ciBS_TEAM_USES_CORONA_VEHICLE_LIST set for team 0, bClearVehicleData = FALSE")
			ENDIF
			
			IF bClearVehicleData
				g_iCoronaMissionVehicleClass = 0
				g_mnCoronaMissionVehicle = DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - bClearVehicleData = TRUE!")
				PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - 				g_iCoronaMissionVehicleClass = 0")
				PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - 				g_mnCoronaMissionVehicle = DUMMY_MODEL_FOR_SCRIPT")
			ENDIF
			
			// Only process this is if its a mission.
			IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
			OR (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH AND DOES_JOB_USE_RACE_VEHICLE_SELECTION())
			
				
				IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
					IF iTeam = -1
						iTeam = 0
						PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Updating iTeam to 0 because this is a deathmatch and it was -1")
					ENDIF
					
					IF DOES_JOB_USE_RACE_VEHICLE_SELECTION()
					AND g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DM_TYPE_NORMAL
						PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Clobber team to 0 for g_FMMC_STRUCT.iMissionType = ", g_FMMC_STRUCT.iMissionType)
							
						iTeam = 0
					ENDIF
				ENDIF
				
				IF iTeam != -1
				AND iTeam < FMMC_MAX_TEAMS
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST)
					OR (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[0].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST))
													
						g_iCoronaMissionVehicleClass = coronaMenuData.iMissionVehicleClass[iTeam]
						
						IF IS_CORONA_BIT_SET(CORONA_SELECTING_FROM_PERSONAL_VEHICLES)
							g_mnMyRaceModel = GET_CUSTOM_VEHICLE_MODEL_NAME(g_sCoronaCustomVehiclesData.customVehicleArray[coronaMenuData.iVehicleCoronaIndex])
							g_iMyRaceModelChoice = g_sCoronaCustomVehiclesData.customVehicleArray[coronaMenuData.iVehicleCoronaIndex]
							IF IS_MODEL_VALID(g_mnMyRaceModel)
							AND g_mnMyRaceModel != DUMMY_MODEL_FOR_SCRIPT
								PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - using custom vehicle ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_mnMyRaceModel))
							ELSE
								PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - using custom vehicle DUMMY_MODEL_FOR_SCRIPT or invalid")
							ENDIF
						ELIF (DOES_JOB_USE_RACE_VEHICLE_SELECTION() AND IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID()))
							g_iMyRaceModelChoice = g_sCoronaCustomVehiclesData.customVehicleArray[GET_PLAYER_CORONA_VEHICLE(PLAYER_ID())]
							g_mnMyRaceModel = GET_CUSTOM_VEHICLE_MODEL_NAME(g_iMyRaceModelChoice)
							IF IS_MODEL_VALID(g_mnMyRaceModel)
							AND g_mnMyRaceModel != DUMMY_MODEL_FOR_SCRIPT
								PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Arena Wars - using custom vehicle ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_mnMyRaceModel))
							ELSE
								PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Arena Wars - using custom vehicle DUMMY_MODEL_FOR_SCRIPT or invalid")
							ENDIF
						ELSE
							IF DOES_JOB_USE_RACE_VEHICLE_SELECTION()
								g_mnMyRaceModel = GET_PLAYER_CORONA_VEHICLE_NAME(PLAYER_ID())
								g_mnCoronaMissionVehicle = g_mnMyRaceModel
								
								IF g_mnCoronaMissionVehicle != DUMMY_MODEL_FOR_SCRIPT
								AND IS_MODEL_VALID(g_mnCoronaMissionVehicle)
									PRINTLN("SAVE_PLAYER_CORONA_CHOICES - IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID - Arena Wars - Setting g_mnCoronaMissionVehicle to g_mnMyRaceModel: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_mnCoronaMissionVehicle))
								ELSE
									PRINTLN("SAVE_PLAYER_CORONA_CHOICES - IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID - Arena Wars - Setting g_mnCoronaMissionVehicle to g_mnMyRaceModel: DUMMY_MODEL_FOR_SCRIPT")
								ENDIF
							ELSE
								g_mnCoronaMissionVehicle = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(coronaMenuData.iMissionVehicleClass[iTeam], coronaMenuData.iMissionVehicle[iTeam])
							ENDIF
							
							// Ensure our vehicles are valid before we proceed with launching.
							IF NOT IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID(iTeam, GET_PLAYER_CHOSEN_MISSION_TEAM_ROLE(PLAYER_ID()))
								PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - IS_CORONA_SAVED_VEHICLE_AND_CLASS_VALID = FALSE, update vehicle")
								
								// Update our local knowledge of the role.
								coronaMenuData.iMissionTeamRole[iTeam] = GET_PLAYER_CHOSEN_MISSION_TEAM_ROLE(PLAYER_ID())
								SET_CORONA_MISSION_VEHICLE_AVAILABILITY(coronaMenuData)
								
								g_iCoronaMissionVehicleClass = coronaMenuData.iMissionVehicleClass[iTeam]
								g_mnCoronaMissionVehicle = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(coronaMenuData.iMissionVehicleClass[iTeam], coronaMenuData.iMissionVehicle[iTeam])
							ENDIF
						ENDIF
						
						IF IS_MODEL_VALID(g_mnCoronaMissionVehicle)
						AND g_mnCoronaMissionVehicle != DUMMY_MODEL_FOR_SCRIPT
							PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Saving out mission vehicle model: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_mnCoronaMissionVehicle), ", in class: ", g_iCoronaMissionVehicleClass)
						ENDIF
					ELSE
						PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Not saving out mission vehicle model, ciBS_TEAM_USES_CORONA_VEHICLE_LIST not set")
						PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - g_FMMC_STRUCT.sFMMCEndConditions[0].iTeamBitset: ", g_FMMC_STRUCT.sFMMCEndConditions[0].iTeamBitset)
						
					ENDIF
					
					// If we are using roles for the team
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
						g_iCoronaMissionRole = GET_PLAYER_CHOSEN_MISSION_TEAM_ROLE(PLAYER_ID())
						PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Saving out mission sub team role: ", g_iCoronaMissionRole, " for Team: ", iTeam)
					ENDIF
				ELSE
					PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - iTeam is invalid! iTeam: ", iTeam)
				ENDIF
			ENDIF
		ENDIF
		
		// Need to adjust the teams as we move through the corona due to these being used in team balancing
		IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSELECTABLE_TEAM_NUMBERS)
			IF sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS] != -1
			OR NOT NETWORK_IS_ACTIVITY_SESSION()
				g_FMMC_STRUCT.iNumberOfTeams = sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS]
				g_FMMC_STRUCT.iMaxNumberOfTeams = sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS]
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciAUTO_NUMBER_OF_TEAMS)
					IF sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS] = -1
						PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Mission: sSelection.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS] = -1, calculating number of teams from number of players in corona")
									
						g_bAutoNumTeamsSelected = TRUE
						
						PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Mission: g_bAutoNumTeamsSelected SET TRUE ")
					ENDIF
				ENDIF
				
				PRINTLN("[CORONA] SAVE_PLAYER_CORONA_CHOICES - Mission: No. of teams overridden: g_FMMC_STRUCT.iNumberOfTeams = ", g_FMMC_STRUCT.iNumberOfTeams)
			ENDIF			
		ENDIF			
		
			g_iMyRaceColorSelection = coronaMenuData.iSelectedColourIndex
		
	ENDIF
	
	PRINTLN("[CORONA][GENERIC_JOB] SAVE_PLAYER_CORONA_CHOICES - g_iMyRaceModelChoice = ", g_iMyRaceModelChoice)
	IF IS_MODEL_VALID(g_mnMyRaceModel)
	AND g_mnMyRaceModel != DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[CORONA][GENERIC_JOB] SAVE_PLAYER_CORONA_CHOICES - g_mnMyRaceModel = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_mnMyRaceModel))
	ELSE
		PRINTLN("[CORONA][GENERIC_JOB] SAVE_PLAYER_CORONA_CHOICES - g_mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT")
	ENDIF
	PRINTLN("[CORONA][GENERIC_JOB] SAVE_PLAYER_CORONA_CHOICES - IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID()) = ", GET_STRING_FROM_BOOL(IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID())))
	

ENDPROC

FUNC PLAYER_PS_STAT_MP GET_MP_MOST_RELEVANT_STAT_FROM_PLAYER_STAT(PLAYER_STATS_ENUM ePlayerStat)
	SWITCH ePlayerStat
		CASE PS_STAMINA				RETURN MP_STAMINA
		CASE PS_STRENGTH			RETURN MP_STRENGTH_ABILITY
		CASE PS_DRIVING_ABILITY		RETURN MP_DRIVING_ABILITY
		CASE PS_FLYING_ABILITY		RETURN MP_FLYING_ABILITY
		CASE PS_SHOOTING_ABILITY	RETURN MP_SHOOTING_ABILITY
		CASE PS_STEALTH_ABILITY		RETURN MP_STEALTH_ABILITY
	ENDSWITCH
	
	PRINTLN("[CORONA] GET_MOST_RELEVANT_MP_STAT_FROM_PLAYER_STAT - Unknown PLAYER_STATS_ENUM passed, returning MP_STAMINA by default")
	RETURN MP_STAMINA
ENDFUNC

FUNC PLAYER_PS_STAT_MP GET_MP_MOST_RELEVANT_STAT(INT iJobType, INT iHash)

	PLAYER_STATS_ENUM eRelevantStat = INT_TO_ENUM(PLAYER_STATS_ENUM, -1)
	PLAYER_PS_STAT_MP ePlayerMPStat = INT_TO_ENUM(PLAYER_PS_STAT_MP, -1)

	SWITCH iJobType
		CASE FMMC_TYPE_RACE					
			IF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
			
				eRelevantStat = PS_FLYING_ABILITY
				ePlayerMPStat = MP_FLYING_ABILITY
				
			ELIF IS_CORONA_CLASS_CYCLES(g_sTransitionSessionOptions)
			
				eRelevantStat = PS_STAMINA
				ePlayerMPStat = MP_STAMINA
				
			ELSE
			
				eRelevantStat = PS_DRIVING_ABILITY
				ePlayerMPStat = MP_DRIVING_ABILITY
				
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH
		CASE FMMC_TYPE_MISSION				
		CASE FMMC_TYPE_MG_HUNTING			
		CASE FMMC_TYPE_MG_SHOOTING_RANGE
		CASE FMMC_TYPE_MG_DARTS
		CASE FMMC_TYPE_SURVIVAL
			eRelevantStat = PS_SHOOTING_ABILITY	
			ePlayerMPStat = MP_SHOOTING_ABILITY
			
		BREAK
		
		CASE FMMC_TYPE_BASE_JUMP
		CASE FMMC_TYPE_MG_GOLF
		CASE FMMC_TYPE_MG_TENNIS
			eRelevantStat = PS_STAMINA
			ePlayerMPStat = MP_STAMINA
		BREAK
		
	ENDSWITCH
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SpeedRace(iHash)
		eRelevantStat = PS_DRIVING_ABILITY
		ePlayerMPStat = MP_DRIVING_ABILITY
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
		eRelevantStat = PS_DRIVING_ABILITY	
		ePlayerMPStat = MP_DRIVING_ABILITY
	ENDIF
	
	IF g_FMMC_STRUCT.iSkillStatToDisplay > -1
		PRINTLN("[CORONA STAT] - GET_MP_MOST_RELEVANT_STAT - g_FMMC_STRUCT.iSkillStatToDisplay = ", g_FMMC_STRUCT.iSkillStatToDisplay)
		eRelevantStat = INT_TO_ENUM(PLAYER_STATS_ENUM, g_FMMC_STRUCT.iSkillStatToDisplay)	
		ePlayerMPStat = GET_MP_MOST_RELEVANT_STAT_FROM_PLAYER_STAT(eRelevantStat)
	ENDIF
	
	INT iStatValue
	IF eRelevantStat != INT_TO_ENUM(PLAYER_STATS_ENUM, -1)
		PRINTLN("[CORONA STAT] - GET_MP_MOST_RELEVANT_STAT - Found stat, ", ENUM_TO_INT(eRelevantStat))
		iStatValue = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, eRelevantStat, FALSE)

	// FALL BACK - Use player's best stat
	ELSE
		PRINTLN("[CORONA STAT] - GET_MP_MOST_RELEVANT_STAT - Fall back, using best stat")
		ePlayerMPStat = MP_STAMINA
		
		iStatValue = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STAMINA, FALSE)

		IF iStatValue < CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_SHOOTING_ABILITY, FALSE)
			ePlayerMPStat = MP_SHOOTING_ABILITY
			
			iStatValue = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_SHOOTING_ABILITY, FALSE)
		ENDIF
		
		IF iStatValue < CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STEALTH_ABILITY, FALSE)
			ePlayerMPStat = MP_STEALTH_ABILITY
			
			iStatValue = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STEALTH_ABILITY, FALSE)
		ENDIF
		
		IF iStatValue < CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_FLYING_ABILITY, FALSE)
			ePlayerMPStat = MP_FLYING_ABILITY
			
			iStatValue = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_FLYING_ABILITY, FALSE)
		ENDIF
		
		IF iStatValue < CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_DRIVING_ABILITY, FALSE)
			ePlayerMPStat = MP_DRIVING_ABILITY
			
			iStatValue = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_DRIVING_ABILITY, FALSE)
		ENDIF

	ENDIF

	RETURN ePlayerMPStat
ENDFUNC

FUNC HUD_COLOURS CORONA_GET_SUMMARY_HIGHLIGHT_COLOUR(CORONA_MENU_DATA &coronaMenuData,  INT iPlayerID, INT iMissionType, BOOL bTeamCorona = FALSE)

	// If we are in a heist, return the heist green colour
	IF IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
		RETURN HUD_COLOUR_H
	ENDIF
	
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
		RETURN HUD_COLOUR_H
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		RETURN HUD_COLOUR_H
	ENDIF
	#ENDIF

	#IF FEATURE_HEIST_ISLAND
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_MISSION_FINALE()
		RETURN HUD_COLOUR_H
	ENDIF
	#ENDIF

	IF NOT SHOULD_DISGUISE_TEAM()
		// If the HUD_COLOUR has been overridden within the creator then take this value.
		IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
			IF coronaMenuData.lobbyPlayers[iPlayerID].iTeam != -1
				IF g_FMMC_STRUCT.iTeamColourOverride[coronaMenuData.lobbyPlayers[iPlayerID].iTeam] != -1
				
					RETURN GET_HUD_COLOUR_FOR_FMMC_TEAM(coronaMenuData.lobbyPlayers[iPlayerID].iTeam, INT_TO_PLAYERINDEX(iPlayerID))
				ENDIF
			ENDIF
		ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH 
		AND IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType)
				RETURN GET_HUD_COLOUR_FOR_ARENA_TDM(coronaMenuData.lobbyPlayers[iPlayerID].iTeam, GET_MY_TEAM_IN_CORONA())
			ENDIF
		ENDIF
	ENDIF

	// Is the player us?
	IF PLAYER_ID() = coronaMenuData.lobbyPlayers[iPlayerID].playerID
	
		//...yes, return LIGHT BLUE highlight
		RETURN HUD_COLOUR_BLUE
	
	// Is this player on the same team as us?
	ELIF NOT IS_THIS_A_MINI_GAME(iMissionType)
	AND (coronaMenuData.lobbyPlayers[iPlayerID].iTeam = GET_MY_TEAM_IN_CORONA()	OR DOES_TEAM_LIKE_TEAM(coronaMenuData.lobbyPlayers[iPlayerID].iTeam, GET_MY_TEAM_IN_CORONA()))
		
		//...yes, return BLUE highlight
		RETURN HUD_COLOUR_BLUE
	
	ELSE
		// If it is a team corona (with 4 teams) then get shade of red
		IF bTeamCorona
		AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_RACE
			RETURN GET_HUD_COLOUR_FOR_OPPONENT_CHOSEN_TEAM(GET_MY_TEAM_IN_CORONA(), coronaMenuData.lobbyPlayers[iPlayerID].iTeam)
		ENDIF
		
		//...otherwise, must be enemy
		RETURN HUD_COLOUR_NET_PLAYER1
	ENDIF

ENDFUNC


// *********** HOST LOBBY SCREEN ************

PROC RESET_CORONA_PLAYER_DETAILS(CORONA_PLAYER_DETAILS &playerDetails)

	playerDetails.playerID = INVALID_PLAYER_INDEX()
	playerDetails.iTeam = -1
	playerDetails.bPlaced = FALSE
	playerDetails.cVoiceState = CORONA_PLAYER_VOICE_NONE
	playerDetails.iSlotAssigned = 0
	playerDetails.iTeamRow = 0
	playerDetails.iTeamPos = CORONA_PLAYER_TEAM_POS_INSIDE
	playerDetails.fNameX = -1.0
	playerDetails.fNameY = -1.0
	playerDetails.iVoiceChat = ciTRANSITION_SESSIONS_VOICE_CHAT_NULL
ENDPROC

/// PURPOSE: Updates players talking state (Speaking, Muted etc)
FUNC BOOL UPDATE_PLAYER_CORONA_VOICE_STATE(CORONA_PLAYER_DETAILS &playerDetails)
	
	CORONA_PLAYER_VOICE_STATE previousState = playerDetails.cVoiceState

	IF NETWORK_IS_PLAYER_TALKING(playerDetails.playerID)
		playerDetails.cVoiceState = CORONA_PLAYER_VOICE_TALKING
	ELSE
		IF NETWORK_IS_PLAYER_MUTED_BY_ME(playerDetails.playerID)
			playerDetails.cVoiceState = CORONA_PLAYER_VOICE_MUTE
		ELSE
			playerDetails.cVoiceState = CORONA_PLAYER_VOICE_NONE
		ENDIF
	ENDIF
	
	RETURN (previousState != playerDetails.cVoiceState)
ENDFUNC

FUNC INT GET_PLAYER_CORONA_VOICE_ICON(CORONA_PLAYER_DETAILS &playerDetails)
	SWITCH playerDetails.cVoiceState
		CASE CORONA_PLAYER_VOICE_TALKING		RETURN ENUM_TO_INT(ACTIVE_HEADSET) 
		CASE CORONA_PLAYER_VOICE_MUTE			RETURN ENUM_TO_INT(MUTED_HEADSET)
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC INT GET_CORONA_TRANSITION_SESSION_VOICE_ICON(INT iTransitionArrayPos)

	INT iVoiceState = GET_TRANSITION_PLAYER_VOICE_STATE(iTransitionArrayPos)

	SWITCH iVoiceState
		CASE ciTRANSITION_SESSIONS_VOICE_CHAT_TALKING	RETURN ENUM_TO_INT(ACTIVE_HEADSET)
		CASE ciTRANSITION_SESSIONS_VOICE_CHAT_MUTED		RETURN ENUM_TO_INT(MUTED_HEADSET)
	ENDSWITCH
	
	RETURN 0
ENDFUNC

#IF IS_DEBUG_BUILD
	FUNC STRING GET_PLAYER_CORONA_VOICE_STATE_STRING(CORONA_PLAYER_DETAILS &playerDetails)
	
		SWITCH playerDetails.cVoiceState
			CASE CORONA_PLAYER_VOICE_NONE		RETURN "CORONA_PLAYER_VOICE_NONE"
			CASE CORONA_PLAYER_VOICE_TALKING	RETURN "CORONA_PLAYER_VOICE_TALKING"
			CASE CORONA_PLAYER_VOICE_MUTE		RETURN "CORONA_PLAYER_VOICE_MUTE"		
		ENDSWITCH
	
		RETURN ""
	ENDFUNC
#ENDIF

/// PURPOSE: Returns TRUE if the aGamer is the host of the corona
FUNC BOOL IS_CORONA_PLAYER_ROW_HOST(GAMER_HANDLE &aGamer, GAMER_HANDLE &aHostGamer)
	
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
	OR IS_SCTV_IN_CONTROL_OF_TRANSITION_SESSION()
		RETURN FALSE
	ENDIF
	
	IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
	OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
		//...if playlist check handle of vote participant
		
		IF IS_GAMER_HANDLE_VALID(aHostGamer)
		
			IF NETWORK_ARE_HANDLES_THE_SAME(aGamer, aHostGamer)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		//...else, check transition session host
		IF NETWORK_IS_TRANSITION_HOST_FROM_HANDLE(aGamer)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CORONA_PLAYER_ROW_ICON(INT i, INT iRank, SCALEFORM_PLAYER_ROW_STATE aRowState, GAMER_HANDLE aRemoteGamer)
	INT iRowIcon = GET_CORONA_TRANSITION_SESSION_VOICE_ICON(i)

	IF iRowIcon = 0
	
		// If they rank we have retrieved is 0, try and grab in session players
		IF iRank = 0
		AND (aRowState = SCALEFORM_PLAYER_ROW_JOINED OR aRowState = SCALEFORM_PLAYER_ROW_HOST)
			IF NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(aRemoteGamer) != INVALID_PLAYER_INDEX()
				iRank = GET_PLAYER_RANK(NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(aRemoteGamer))
				SET_TRANSITION_PLAYER_RANK(i, iRank)
			ENDIF
		ENDIF
	
		IF iRank > 0
			iRowIcon = ENUM_TO_INT(ICON_RANK_FREEMODE)
		ENDIF
	ENDIF
	
	RETURN iRowIcon
ENDFUNC

/// PURPOSE: Returns the correct status for the player in the corona
FUNC SCALEFORM_PLAYER_ROW_STATE GET_TRANSITION_PLAYER_STATUS(INT iRow, GAMER_HANDLE &aRemoteGamer, GAMER_HANDLE &hostHandle, BOOL bForceJoin = FALSE)
	
	GAMER_HANDLE aLocalGamer = GET_LOCAL_GAMER_HANDLE()
	SCALEFORM_PLAYER_ROW_STATE pRowState = SCALEFORM_PLAYER_ROW_JOINED
	
	IF NOT IS_TRANSITION_PLAYER_READY_TO_LAUNCH_JOB(iRow)
 	AND NOT NETWORK_ARE_HANDLES_THE_SAME(aLocalGamer, aRemoteGamer)
	
		IF IS_THIS_TRANSITION_SESSION_PLAYER_ON_CALL(iRow)
			pRowState = SCALEFORM_PLAYER_ROW_ON_CALL
		ELSE	
			pRowState = SCALEFORM_PLAYER_ROW_JOINING
		ENDIF
	ENDIF
	
	// Is this player the host?
	IF IS_CORONA_PLAYER_ROW_HOST(aRemoteGamer, hostHandle)
	AND NOT IS_THIS_TRANSITION_SESSION_PROFESSIONAL_RACE()
		pRowState = SCALEFORM_PLAYER_ROW_HOST
	ELSE	
		IF bForceJoin
			pRowState = SCALEFORM_PLAYER_ROW_JOINED
		ENDIF
	ENDIF
	
	// Finally if they are the host but on call, display different text
	IF pRowState = SCALEFORM_PLAYER_ROW_HOST
		IF IS_THIS_TRANSITION_SESSION_PLAYER_ON_CALL(iRow)
		AND NOT IS_TRANSITION_PLAYER_READY_TO_LAUNCH_JOB(iRow)
			pRowState = SCALEFORM_PLAYER_ROW_ON_CALL_HOST
		ENDIF
	ENDIF	
	
	RETURN pRowState
ENDFUNC

FUNC INT GET_CORONA_CONTROL_TYPE_ICON(INT iIconVal)
	
	IF iIconVal = ciTRANSITION_SESSION_USING_KEYBOARD
		RETURN ENUM_TO_INT(ICON_PC_PLAYER)
	ELIF iIconVal = ciTRANSITION_SESSION_USING_CONTROLLER
		RETURN ENUM_TO_INT(ICON_CONSOLE_PLAYER)
	ENDIF
	
	RETURN 0
ENDFUNC

PROC UPDATE_TRANSITION_SESSION_PLAYER_ROW(INT iColumn, CORONA_MENU_DATA &coronaMenuData, INT iRow, INT iVoteParticipant)

	IF IS_GAMER_HANDLE_VALID(GET_TRANSITION_PLAYER_GAMER_HANDLE(iRow))
		
		// Get the page that this player would be on
		INT iPage = (iRow / CORONA_MAX_PLAYER_ROWS)
				
		// Is the page matching the page we are viewing
		IF iPage = coronaMenuData.iCoronaPlayerPage
		
			// Get the correct row in the 16 rows we display
			INT iIndex = (iRow % CORONA_MAX_PLAYER_ROWS)
		
			INT iRank
			INT iRowIcon
			INT iRowControl
			TEXT_LABEL_15 tlCrewTag
			TEXT_LABEL_63 tlGamerTag
			GAMER_HANDLE aRemoteGamer
					
			PLAYER_INDEX playerHost = INT_TO_PLAYERINDEX(iVoteParticipant)
			GAMER_HANDLE hostGamerHandle
			
			// Make sure our host is valid
			IF playerHost != INVALID_PLAYER_INDEX()
			AND IS_NET_PLAYER_OK(playerHost)
				hostGamerHandle = GET_GAMER_HANDLE_PLAYER(playerHost)
			ENDIF
			
			HUD_COLOURS hcHighlight = HUD_COLOUR_FREEMODE
			SCALEFORM_PLAYER_ROW_STATE pRowState
						
			//...yes, retrieve the correct information
			iRank 			= GET_TRANSITION_PLAYER_RANK(iRow)
			tlGamerTag 		= GET_TRANSITION_PLAYER_GAMERTAG(iRow)
			aRemoteGamer 	= GET_TRANSITION_PLAYER_GAMER_HANDLE(iRow)
			tlCrewTag		= GET_TRANSITION_PLAYER_CREW_TAG(iRow)
			pRowState 		= GET_TRANSITION_PLAYER_STATUS(iRow, aRemoteGamer, hostGamerHandle)
			iRowIcon 		= GET_CORONA_PLAYER_ROW_ICON(iRow, iRank, pRowState, aRemoteGamer)
			iRowControl		= GET_CORONA_CONTROL_TYPE_ICON(GET_TRANSITION_PLAYER_CONTROL_TYPE(iRow))
			
			// Don't show this player if their gamertag is empty (usually XB1 issue only)
			IF NOT IS_STRING_NULL_OR_EMPTY(tlGamerTag)
			
				UPDATE_FRONTEND_PLAYER_DETAIL_ROW(	iColumn, iIndex, coronaMenuData.CORONA_COLUMN_TWO_ID, iRow, iRank, tlGamerTag, tlCrewTag, 
													pRowState, iRowIcon, hcHighlight, iRowControl)
			
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("UPDATE_TRANSITION_SESSION_PLAYER_ROW - Player in slot: ", iRow, ", IS_STRING_NULL_OR_EMPTY(tlGamerTag) = TRUE")		
			#ENDIF
			
			ENDIF
		ENDIF
	ENDIF
											
ENDPROC


FUNC BOOL MAINTAIN_CORONA_CLIENT_OPTIONS_INPUT(CORONA_MENU_DATA &coronaMenuData, INT iMissionType, INT iCurrentSelection, FMMC_SELECTED_ITEMS &sSelection)

	INT iIncrement
	BOOL bButtonPressed

	//Maintain Ammo Toggle
	IF iCurrentSelection = ciALL_LOCAL_PURCHASE_AMMO
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			IF coronaMenuData.bSellFullAmmo = TRUE
				coronaMenuData.bSellFullAmmo = FALSE
				PRINTLN("MAINTAIN_CORONA_CLIENT_OPTIONS_INPUT - coronaMenuData.bSellFullAmmo = FALSE")				
			ELSE
				coronaMenuData.bSellFullAmmo = TRUE
				PRINTLN("MAINTAIN_CORONA_CLIENT_OPTIONS_INPUT - coronaMenuData.bSellFullAmmo = TRUE")
			ENDIF

			PRINTLN("[CORONA] Player toggling full ammo purchase option (MAINTAIN_CORONA_HOST_OPTIONS_INPUT)")

			RETURN TRUE
		ENDIF
	ENDIF
	
	//SET_FRONTEND_PC_MOUSE_SELECT_HIGHLIGHT( coronaMenuData.iCurrentSelection)
	
	IF IS_CLIENT_ALLOWED_TO_CHANGE_OPTION(iMissionType, iCurrentSelection, sSelection)
	
		// If value should change, change it.
		IF SHOULD_SELECTION_BE_INCREMENTED(coronaMenuData.lobbyOptionTimer, iIncrement, bButtonPressed, TRUE, TRUE, -1, FALSE, coronaMenuData.iCurrentSelection)
	
			IF iCurrentSelection = ciCORONA_LOBBY_MATCHMAKING
			
				IF NOT IS_CORONA_FORCED_MATCHMAKING_ACTIVE()
			
					sSelection.bLobbyAvailability = !(sSelection.bLobbyAvailability)
				
					UPDATE_CORONA_MATCHMAKING_STATUS(iMissionType, sSelection.bLobbyAvailability)
				ELSE
					// We have now toggled the heist option, mark it as adjusted
					SET_CORONA_BIT(CORONA_HEIST_FORCE_MATCHMAKING_SUPPRESSED)
					PRINTLN("[CORONA] MAINTAIN_CORONA_CLIENT_OPTIONS_INPUT - Client has adjusted matchmaking while we were in the forced open time")
				ENDIF
				
				PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								
			ELIF iCurrentSelection = ciALL_HOST_OPTION_CLIENT_INVITES
								
				UPDATE_CORONA_CLIENT_INVITE_OPTION(iMissionType)
			
			ELIF iCurrentSelection = ciALL_CREW_CHALLENGE_CASH
				
				// Increment our cash value
				UPDATE_CORONA_CREW_CHALLENGE_CASH(coronaMenuData, iIncrement)
			
			ELIF coronaMenuData.iCurrentSelection = ciALL_LOCAL_RADIO_STATION
			
				// Increment our radio station
				UPDATE_CORONA_RADIO_STATION(iIncrement)
				
			ELIF coronaMenuData.iCurrentSelection = ciALL_LOCAL_WEAPON
			
				// Increment the next valid weapon
				GET_CORONA_NEXT_VALID_WEAPON(coronaMenuData, iIncrement)
				
			ELSE
	
				INT iMaxCount =  GET_CLIENT_SELECTION_MAX(iMissionType, iCurrentSelection, sSelection, coronaMenuData.iTotalCustomVehicles)
				
				SWITCH iMissionType
					
					CASE FMMC_TYPE_MISSION
						IF iCurrentSelection = ciMISSION_CLIENT_OPTION_TEAM
							
							g_FMMC_STRUCT.iMyTeam += iIncrement
							
							IF g_FMMC_STRUCT.iMyTeam > iMaxCount
								g_FMMC_STRUCT.iMyTeam = 0
							ENDIF
							
							IF g_FMMC_STRUCT.iMyTeam < 0
								g_FMMC_STRUCT.iMyTeam = iMaxCount
							ENDIF
							
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = g_FMMC_STRUCT.iMyTeam	
							PRINTLN("MAINTAIN_CORONA_CLIENT_OPTIONS_INPUT - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", g_FMMC_STRUCT.iMyTeam)
							IF SHOULD_TRANSITION_SESSION_RUN(iMissionType)
								PRINTLN("MAINTAIN_CORONA_CLIENT_OPTIONS_INPUT - SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(", ciTRANSITION_SESSIONS_PLAYER_OPTION_0, ", ", g_FMMC_STRUCT.iMyTeam, ")")
								SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYER_OPTION_0, g_FMMC_STRUCT.iMyTeam)
							ENDIF
							
							PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						ELIF iCurrentSelection = ciMISSION_CLIENT_OPTION_SEAT
							coronaMenuData.iClientOptionVehicleSeat += iIncrement
							
							IF coronaMenuData.iClientOptionVehicleSeat > ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_TURRET
								coronaMenuData.iClientOptionVehicleSeat = ciMISSION_CLIENT_SEAT_OPTION_DRIVER
							ENDIF
							
							IF coronaMenuData.iClientOptionVehicleSeat < ciMISSION_CLIENT_SEAT_OPTION_DRIVER
								coronaMenuData.iClientOptionVehicleSeat = ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_TURRET
							ENDIF
							
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnSeatPreference = coronaMenuData.iClientOptionVehicleSeat
							g_iMyPreferedPartner = coronaMenuData.iClientOptionVehicleSeat
							PRINTLN("MAINTAIN_CORONA_CLIENT_OPTIONS_INPUT - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iRespawnSeatPreference = ", coronaMenuData.iClientOptionVehicleSeat)
							IF SHOULD_TRANSITION_SESSION_RUN(iMissionType)
								PRINTLN("MAINTAIN_CORONA_CLIENT_OPTIONS_INPUT - SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(", ciTRANSITION_SESSIONS_PLAYER_OPTION_1, ", ", coronaMenuData.iClientOptionVehicleSeat, ")")
								SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYER_OPTION_1, coronaMenuData.iClientOptionVehicleSeat)
							ENDIF
							
							PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						ENDIF
					BREAK
										
					CASE FMMC_TYPE_RACE
					
						IF iCurrentSelection = ciRC_CLIENT_OPTION_PARTNER
						
							// Preferred partner needs to find next valid player or default option
							GET_NEXT_VALID_RACE_PARTNER(iIncrement)
					
							IF SHOULD_TRANSITION_SESSION_RUN(iMissionType)
								PRINTLN("MAINTAIN_CORONA_CLIENT_OPTIONS_INPUT - SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(", ciTRANSITION_SESSIONS_PLAYER_OPTION_0, ", ", g_iMyPreferedPartner, ")")
								SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYER_OPTION_0, g_iMyPreferedPartner)
							ENDIF
				
						
						// Option ROLE can be incremented and wrapped when reaching max / min
						ELIF iCurrentSelection = ciRC_CLIENT_OPTION_ROLE
							
							g_iMyPreferedPosition += iIncrement
							
							IF g_iMyPreferedPosition > iMaxCount
								g_iMyPreferedPosition = 0
							ENDIF
							
							IF g_iMyPreferedPosition < 0
								g_iMyPreferedPosition = iMaxCount
							ENDIF
						
							IF SHOULD_TRANSITION_SESSION_RUN(iMissionType)
								PRINTLN("MAINTAIN_CORONA_CLIENT_OPTIONS_INPUT - SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(", ciTRANSITION_SESSIONS_PLAYER_OPTION_1, ", ", g_iMyPreferedPosition, ")")
								SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciTRANSITION_SESSIONS_PLAYER_OPTION_1, g_iMyPreferedPosition)
							ENDIF
							
							PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						ENDIF
					BREAK
				
				ENDSWITCH
			ENDIF
			
			RETURN TRUE
		
		ELSE
			IF NOT bButtonPressed
				IF IS_CORONA_BIT_SET(CORONA_ADJUSTING_CASH_VALUE)
					CLEAR_CORONA_BIT(CORONA_ADJUSTING_CASH_VALUE)
				ENDIF 
			ENDIF
		ENDIF
	ENDIF

    
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Update the page buttons for the corona
PROC UPDATE_CORONA_PAGE_BUTTONS(CORONA_MENU_DATA &coronaMenuData)

	PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_PLAYERS_R2"))
	PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CORONA_PLAYERS_L2"))

	INT iNoPages = (GET_TOTAL_PLAYERS_IN_CORONA(coronaMenuData)-1) / CORONA_MAX_PLAYER_ROWS
	
	IF iNoPages > 0
		
		IF coronaMenuData.iCoronaPlayerPage = 0
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CORONA_PLAYERS_R2"))
		ELSE
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CORONA_PLAYERS_L2"))
		ENDIF		
	ENDIF

	PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
ENDPROC

/// PURPOSE: Returns TRUE if the player has changed the player list view in the centre of the corona screens 
FUNC BOOL PROCESS_CORONA_PLAYER_PAGE_SCROLL(CORONA_MENU_DATA &coronaMenuData)

	// Early check to see if we are less than the rows we are currently displaying
	IF GET_TOTAL_PLAYERS_IN_CORONA(coronaMenuData) <= CORONA_MAX_PLAYER_ROWS	// Probably want to do CORONA_MAX_PLAYER_ROWS * (coronaMenuData.iCoronaPlayerPage+1) to become future proof
		
		// If we are not on the first page but now we have less that 16, reset ourselves back to this
		IF coronaMenuData.iCoronaPlayerPage > 0
			PRINTLN("[CORONA] PROCESS_CORONA_PLAYER_PAGE_SCROLL - GET_TOTAL_PLAYERS_IN_CORONA(coronaMenuData) <= CORONA_MAX_PLAYER_ROWS, move back to page 1")
			
			// Fall back to the last valid page of the corona
			coronaMenuData.iCoronaPlayerPage = (GET_TOTAL_PLAYERS_IN_CORONA(coronaMenuData) / CORONA_MAX_PLAYER_ROWS)
			RETURN TRUE
		ENDIF
	
		RETURN FALSE
	ENDIF
	
	// Grab the total pages available to us based on number of players we have
	INT iNoPages = (GET_TOTAL_PLAYERS_IN_CORONA(coronaMenuData)-1) / CORONA_MAX_PLAYER_ROWS

	// If we have pages to the 'right' of us, listen for the RT press
	IF coronaMenuData.iCoronaPlayerPage < iNoPages
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
			
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			//...button pressed, increment to next page
			coronaMenuData.iCoronaPlayerPage++
			
			PRINTLN("[CORONA] PROCESS_CORONA_PLAYER_PAGE_SCROLL - Player pressed RT. Increment page to: ", coronaMenuData.iCoronaPlayerPage)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF coronaMenuData.iCoronaPlayerPage > 0
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
			
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			//...button pressed, increment to previous page
			coronaMenuData.iCoronaPlayerPage--
			
			PRINTLN("[CORONA] PROCESS_CORONA_PLAYER_PAGE_SCROLL - Player pressed LT. Decrement page to: ", coronaMenuData.iCoronaPlayerPage)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_TRANSITION_SESSION_CREW_TAG_FOR_CORONA_VALID(GAMER_HANDLE aGamerHandle)

	// Perform checks for privilages. Hide crewtag if we are no allowed to see this player's gamer content (2111127)
	IF IS_XBOX_PLATFORM()
	#IF FEATURE_GEN9_RELEASE
	OR IS_PROSPERO_VERSION()
	#ENDIF
		IF IS_GAMER_HANDLE_VALID(aGamerHandle)
			IF NOT NETWORK_CAN_VIEW_GAMER_USER_CONTENT(aGamerHandle)
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		IF NOT NETWORK_HAVE_USER_CONTENT_PRIVILEGES()
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC



PROC UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY(INT iColumn, CORONA_MENU_DATA &coronaMenuData, INT iVoteParticipant, BOOL bForceJoin = FALSE)

	PRINTLN("[CORONA] UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY - WILL POPULATE WITH NEW TRANSITION PLAYERS, iVoteParticipant = ", iVoteParticipant)
	
	#IF IS_DEBUG_BUILD
		IF iVoteParticipant != -1
			PRINTLN("[CORONA] UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY - WILL POPULATE WITH NEW TRANSITION PLAYERS, Host name = ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iVoteParticipant)))
		ENDIF
	#ENDIF	

	INT i
	INT iRank
	INT iRowIcon
	INT iRowControl
	INT iCount
	TEXT_LABEL_15 tlCrewTag
	TEXT_LABEL_63 tlGamerTag
	GAMER_HANDLE aRemoteGamer
	HUD_COLOURS hcHighlight = HUD_COLOUR_FREEMODE
	SCALEFORM_PLAYER_ROW_STATE pRowState
	
	// Empty contents of column
	CLEAR_FRONTEND_DATA_SLOT(iColumn)
	
	PLAYER_INDEX playerHost = INT_TO_PLAYERINDEX(iVoteParticipant)
	
	IF bForceJoin
		playerHost = INT_TO_PLAYERINDEX(coronaMenuData.iPreviousCoronaHostIndex)
		#IF IS_DEBUG_BUILD
			IF coronaMenuData.iPreviousCoronaHostIndex != -1
				PRINTLN("[CORONA] UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY - WILL POPULATE WITH NEW TRANSITION PLAYERS, Old host name = ", GET_PLAYER_NAME(playerHost))
			ENDIF
		#ENDIF
	ENDIF
	
	GAMER_HANDLE hostGamerHandle
	
	// Make sure our host is valid
	IF playerHost != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(playerHost)
		hostGamerHandle = GET_GAMER_HANDLE_PLAYER(playerHost)
	ENDIF
	
	INT iStartIndex = (coronaMenuData.iCoronaPlayerPage * CORONA_MAX_PLAYER_ROWS)
	INT iEndIndex = (iStartIndex + CORONA_MAX_PLAYER_ROWS)
	
	// Loop over from our start index to end index for transition session players
	FOR i = iStartIndex TO (iEndIndex - 1) STEP 1

		
		// Is the players gamer handle valid
		IF IS_GAMER_HANDLE_VALID(GET_TRANSITION_PLAYER_GAMER_HANDLE(i))
		#IF IS_DEBUG_BUILD 
		OR g_bDebugTotalPlayersInCorona
		#ENDIF
			
			//...yes, retrieve the correct information
			iRank 			= GET_TRANSITION_PLAYER_RANK(i)
			tlGamerTag 		= GET_TRANSITION_PLAYER_GAMERTAG(i)
			aRemoteGamer 	= GET_TRANSITION_PLAYER_GAMER_HANDLE(i)
			tlCrewTag 		= GET_TRANSITION_PLAYER_CREW_TAG(i)
			pRowState 		= GET_TRANSITION_PLAYER_STATUS(i, aRemoteGamer, hostGamerHandle, bForceJoin)
			
			// We should only populate the player if they have a gamertag
			IF NOT IS_STRING_NULL_OR_EMPTY(tlGamerTag)
						
				IF pRowState = SCALEFORM_PLAYER_ROW_HOST
				OR pRowState = SCALEFORM_PLAYER_ROW_ON_CALL_HOST
					// Monitor if the corona host has changed
					IF NOT IS_GAMER_HANDLE_VALID(coronaMenuData.coronaHostHandle)
					OR NOT NETWORK_ARE_HANDLES_THE_SAME(aRemoteGamer, coronaMenuData.coronaHostHandle)
					
						PRINTLN("[CORONA] UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY - Corona host has changed from ", coronaMenuData.iCoronaHost, " to ", i, ", Name = ", tlGamerTag, " TIME(", GET_CLOUD_TIME_AS_INT(), ")")
				
						SET_CORONA_BIT(CORONA_HOST_CHANGE)
						coronaMenuData.iCoronaHost = i
						coronaMenuData.coronaHostHandle = aRemoteGamer
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					IF GET_GAME_TIMER() % 2500 < 50
						PRINTLN("[2021326] pRowState ", i, " = ", ENUM_TO_INT(pRowState))
					ENDIF
					#ENDIF
				ENDIF
				
				iRowIcon = GET_CORONA_PLAYER_ROW_ICON(i, iRank, pRowState, aRemoteGamer)
				
				#IF IS_DEBUG_BUILD
				IF g_bDebugTotalPlayersInCorona
					tlGamerTag = "Player Row "
					tlGamerTag += i
				ENDIF
				#ENDIF
				
				iRowControl		= GET_CORONA_CONTROL_TYPE_ICON(GET_TRANSITION_PLAYER_CONTROL_TYPE(i))
				
				//...set up frontend row
				SET_FRONTEND_PLAYER_DETAIL_ROW(	iColumn, iCount, coronaMenuData.CORONA_COLUMN_TWO_ID, iCount, iRank, tlGamerTag, tlCrewTag, 
												pRowState, iRowIcon, hcHighlight, iRowControl)

				iCount++
				
			
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[CORONA] UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY - Player in slot: ", i, ", IS_STRING_NULL_OR_EMPTY(tlGamerTag) = TRUE")
			#ENDIF
				
			ENDIF
				
		#IF IS_DEBUG_BUILD
		ELSE
			IF GET_GAME_TIMER() % 2500 < 50
				PRINTLN("[2021326] IS_GAMER_HANDLE_VALID(GET_TRANSITION_PLAYER_GAMER_HANDLE(", i, ")) = FALSE")
			ENDIF
			#ENDIF
		ENDIF

	ENDFOR
		
	// Make sure we process any players from previous job if valid
	IF HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
		PRINTLN("[CORONA] ****** Process end of job vote players ******")
	
		FOR i = 0 TO NUM_NETWORK_PLAYERS-1 STEP 1
			
			IF iCount < CORONA_MAX_PLAYER_ROWS
			
				IF IS_GAMER_HANDLE_VALID(g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerHandle)
				
					pRowState = SCALEFORM_PLAYER_ROW_JOINING
					IF bForceJoin
						pRowState = SCALEFORM_PLAYER_ROW_JOINED
					ENDIF
				
					IF IS_TRANSITION_SESSION_CREW_TAG_FOR_CORONA_VALID(g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerHandle)
						tlCrewTag = g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].crewTag
					ELSE
						tlCrewTag = ""
					ENDIF
				
					PRINTLN("[CORONA] Adding player from end of job: ", g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerTag, ", crewTag = ", tlCrewTag)
										
					SET_FRONTEND_PLAYER_DETAIL_ROW(iColumn, iCount, coronaMenuData.CORONA_COLUMN_TWO_ID, iCount, -1, 
													g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerTag, tlCrewTag, pRowState)
					
					iCount++
				ELSE
					i = NUM_NETWORK_PLAYERS
				ENDIF				
				
			ELSE
				i = NUM_NETWORK_PLAYERS
			ENDIF
		ENDFOR
	
	ENDIF
	
	INT iTotalRows = coronaMenuData.iMaxPlayers
	
	// ******* TUTORIAL SPECIAL CASE *******
	
	// Is player in tutorial corona?
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		
		// Add Lamar to our visuals
		SET_FRONTEND_PLAYER_DETAIL_ROW(iColumn, iCount, coronaMenuData.CORONA_COLUMN_TWO_ID, iCount, CORONA_LAMAR_RANK, GET_TUTORIAL_PED_NAME(), "", SCALEFORM_PLAYER_ROW_JOINED, ENUM_TO_INT(ICON_RANK_FREEMODE))
		iCount++
	ENDIF
	
	// Process any more invite rows
	IF GET_TOTAL_PLAYERS_IN_CORONA(coronaMenuData) < coronaMenuData.iMaxPlayers
	
		INT iGamerHandleCount
		FOR i = iCount TO (CORONA_MAX_PLAYER_ROWS-1) STEP 1

			PRINTLN("[CORONA] UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY - iCount = ", iCount, ", iGamerHandleCount = ", iGamerHandleCount)

			IF IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[iGamerHandleCount].gamerHandle)
			AND DOES_CORONA_INVITED_PLAYER_HAVE_GAMERTAG(iGamerHandleCount)
			AND (DOES_CORONA_INVITED_PLAYER_HAVE_CREW_TAG(iGamerHandleCount) OR IS_PLAYSTATION_PLATFORM())
			
				PRINTLN("[CORONA] UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY - name: ", g_CoronaInvitedPlayers.coronaInvitedPlayers[iGamerHandleCount].gamerName)
			
				IF IS_TRANSITION_SESSION_CREW_TAG_FOR_CORONA_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[iGamerHandleCount].gamerHandle)
					tlCrewTag = g_CoronaInvitedPlayers.coronaInvitedPlayers[iGamerHandleCount].tlCrewTag
				ELSE
					tlCrewTag = ""
				ENDIF
			
				SET_FRONTEND_PLAYER_DETAIL_ROW(iColumn, iCount, coronaMenuData.CORONA_COLUMN_TWO_ID, iCount, -1, g_CoronaInvitedPlayers.coronaInvitedPlayers[iGamerHandleCount].gamerName, tlCrewTag, g_CoronaInvitedPlayers.coronaInvitedPlayers[iGamerHandleCount].invitedPlayerStatus)
				
				iGamerHandleCount++
				iCount++
			ELSE
				IF NOT IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[iGamerHandleCount].gamerHandle)
					PRINTLN("[CORONA] UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY - not a valid gamer handle")
				ENDIF
				IF NOT DOES_CORONA_INVITED_PLAYER_HAVE_GAMERTAG(iGamerHandleCount)
					PRINTLN("[CORONA] UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY - invited player doesnt have gamertag")
				ENDIF
				IF NOT DOES_CORONA_INVITED_PLAYER_HAVE_CREW_TAG(iGamerHandleCount)
					PRINTLN("[CORONA] UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY - invited player doesnt have crew tag")
				ENDIF
			ENDIF
		ENDFOR
		
		// Process any more invalid rows
		FOR i = iCount TO (iTotalRows-1) STEP 1
			SET_FRONTEND_PLAYER_DETAIL_ROW_INVALID(iColumn, iCount, coronaMenuData.CORONA_COLUMN_TWO_ID, iCount)
			iCount++
		ENDFOR
	ENDIF
	
	INT iNoPages = (GET_TOTAL_PLAYERS_IN_CORONA(coronaMenuData)-1) / CORONA_MAX_PLAYER_ROWS
	IF iNoPages > 1
		ADD_SCALEFORMXML_INIT_COLUMN_SCROLL_GEN(iColumn, 1, 1, 1, 0, FALSE)
	ELSE
		ADD_SCALEFORMXML_SET_COLUMN_SCROLL(iColumn, 1, 1, "", TRUE)
	ENDIF
		
	// Display the data
	DISPLAY_FRONTEND_DATA_SLOT(iColumn)
ENDPROC

/// PURPOSE: Fills out the data slots in frontend menu
PROC UPDATE_JOINED_PLAYER_LOBBY_LIST_DISPLAY(INT iColumn, CORONA_MENU_DATA &coronaMenuData)
	
	// ***************** NEW TRANSITION SESSION BEHAVIOUR ****************

	UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY(iColumn, coronaMenuData)
ENDPROC

/// PURPOSE: Initialises our lobby list to display whos in our corona
/// PARAMS:
///    lobbyList - 
///    iLobbyBitSet - 
PROC INIT_JOINED_PLAYER_LOBBY_LIST(INT iColumn, CORONA_MENU_DATA &coronaMenuData, INT iVoteParticipant)
	
	PRINTLN("[CORONA] INIT_JOINED_PLAYER_LOBBY_LIST - clear data and setup initial list")	
		
	// ******************** NEW TRANSITION SESSION BEHAVIOUR **************
	UPDATE_TRANSITION_SESSION_PLAYER_LOBBY_LIST_DISPLAY(iColumn, coronaMenuData, iVoteParticipant)
ENDPROC

/// PURPOSE: Keep player list updated with host information
PROC MAINTAIN_CORONA_HOST(CORONA_MENU_DATA &coronaMenuData, INT iVoteParticipant)
	
	GAMER_HANDLE aCoronaHost
	
	// No need to maintain this as voteparticipant updates for this anyway
	IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
	OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
	
		// Player taken from the iVoteParticipatn
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iVoteParticipant)
		IF playerID != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_ACTIVE(playerID)
			aCoronaHost = GET_GAMER_HANDLE_PLAYER(playerID)			
			
			// If both the vote participant and current host handles are safe, compare.
			IF IS_GAMER_HANDLE_VALID(coronaMenuData.coronaHostHandle)
				IF IS_GAMER_HANDLE_VALID(aCoronaHost)
					// has there been a change
					IF NOT NETWORK_ARE_HANDLES_THE_SAME(aCoronaHost, coronaMenuData.coronaHostHandle)
						PRINTLN("[CORONA] MAINTAIN_CORONA_HOST - Host has updated (launched transition session), refresh player list. iVoteParticipant = ", iVoteParticipant )
		
						SET_TRANSITION_SESSION_PLAYER_DATA_REFRESH()
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[CORONA] MAINTAIN_CORONA_HOST - No host known, IS_GAMER_HANDLE_VALID(coronaMenuData.coronaHostHandle) = FALSE")	
		
				SET_TRANSITION_SESSION_PLAYER_DATA_REFRESH()
			ENDIF
		ENDIF
	
		EXIT
	ENDIF

	// Do normal transition session checks for host change
	IF NETWORK_GET_TRANSITION_HOST(aCoronaHost)
	
		IF IS_GAMER_HANDLE_VALID(coronaMenuData.coronaHostHandle)
			
			IF NOT NETWORK_ARE_HANDLES_THE_SAME(aCoronaHost, coronaMenuData.coronaHostHandle)
				PRINTLN("[CORONA] MAINTAIN_CORONA_HOST - Host has updated, refresh player list")
		
				SET_TRANSITION_SESSION_PLAYER_DATA_REFRESH()
			ENDIF
		ELSE
			IF NOT IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()
				PRINTLN("[CORONA] MAINTAIN_CORONA_HOST - No host known, use NETWORK_GET_TRANSITION_HOST()")	
		
				SET_TRANSITION_SESSION_PLAYER_DATA_REFRESH()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Finds next available slot in lobby array passed
FUNC INT GET_NEXT_JOINED_PLAYER_LOBBY_SLOT(CORONA_PLAYER_DETAILS &lobbyList[])
	
	INT i = 0
	REPEAT NUM_NETWORK_PLAYERS i
	
		// Find next slot that has invalid player index
		IF lobbyList[i].playerID = INVALID_PLAYER_INDEX()
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC


/// PURPOSE: Finds next available slot in lobby array passed
FUNC INT GET_NEXT_JOINING_PLAYER_LOBBY_SLOT(PLAYER_INDEX &joiningList[])
	
	INT i = 0
	REPEAT NUM_NETWORK_PLAYERS i
	
		// Find next slot that has invalid player index
		IF joiningList[i] = INVALID_PLAYER_INDEX()
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC


FUNC INT GET_INDEX_OF_CORONA_PLAYER(CORONA_MENU_DATA &coronaMenuData, PLAYER_INDEX playerID)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF coronaMenuData.lobbyPlayers[i].playerID = playerID
			RETURN i
		ENDIF
		
	ENDREPEAT
	
	RETURN -1
ENDFUNC

/// PURPOSE: Removes player from array and moves everyone up
PROC REMOVE_PLAYER_FROM_JOINED_PLAYER_LOBBY_LIST(PLAYER_INDEX playerID, CORONA_PLAYER_DETAILS &lobbyList[])
	
	INT i
	BOOL bShiftPlayer = FALSE
	REPEAT NUM_NETWORK_PLAYERS i
	
		IF bShiftPlayer
			
			lobbyList[i-1] = lobbyList[i]
			
			// If we are moving invalid players..STOP
			IF lobbyList[i].playerID = INVALID_PLAYER_INDEX()
				EXIT
			ENDIF
		ELSE	
			// Keep list as it is until we find current player
			IF lobbyList[i].playerID = playerID
				bShiftPlayer = TRUE
			ENDIF
		ENDIF
				
	ENDREPEAT
	
ENDPROC

/// PURPOSE: Removes the player from the joining player list
PROC REMOVE_PLAYER_FROM_JOINING_PLAYER_LIST(PLAYER_INDEX playerID, PLAYER_INDEX &joiningList[])
	INT i
	BOOL bShiftPlayer = FALSE
	REPEAT NUM_NETWORK_PLAYERS i
	
		IF bShiftPlayer
			
			joiningList[i-1] = joiningList[i]
			
			// If we are moving invalid players..STOP
			IF joiningList[i] = INVALID_PLAYER_INDEX()
				EXIT
			ENDIF
		ELSE	
			// Keep list as it is until we find current player
			IF joiningList[i] = playerID
				bShiftPlayer = TRUE
			ENDIF
		ENDIF
				
	ENDREPEAT
ENDPROC

/// PURPOSE: Maintains our player list and gets name of time
PROC MAINTAIN_CORONA_INVITED_PLAYER_NAMES()

	IF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
	OR IS_PLAYSTATION_PLATFORM()
		IF NOT DOES_CORONA_INVITED_PLAYER_HAVE_GAMERTAG(g_CoronaInvitedPlayers.iPlayerToGetGamerTag)
		AND IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].gamerHandle)
			
			// Populate an array of 1 right now. We could potentially store this for 1-2s to see if we could group requests for efficiency
			GAMER_HANDLE gamerHandleArray[1]
			TEXT_LABEL_63 tlGamerNames[1]
			gamerHandleArray[0] = g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].gamerHandle
			BOOL bSafe = TRUE
			
			// Switch on the invited state
			SWITCH g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].invitedNameState
			
				CASE CORONA_INVITED_REQUEST_NAME
				
					IF g_CoronaInvitedPlayers.iRequestCount >= 8	// Max requests is 8. url:bugstar:5514238
						#IF IS_DEBUG_BUILD
						PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_NAMES - Maximum requests reached at ", g_CoronaInvitedPlayers.iRequestCount, " requests. bSafe = FALSE")
						#ENDIF
						
						g_CoronaInvitedPlayers.iPlayerToGetGamerTag++
		
						IF g_CoronaInvitedPlayers.iPlayerToGetGamerTag >= NUM_NETWORK_PLAYERS
							g_CoronaInvitedPlayers.iPlayerToGetGamerTag = 0
						ENDIF
						
						bSafe = FALSE
					ENDIF
				
					IF bSafe
						g_CoronaInvitedPlayers.iGamertagRequestID = NETWORK_DISPLAYNAMES_FROM_HANDLES_START(gamerHandleArray, 1)
					
						IF g_CoronaInvitedPlayers.iGamertagRequestID >= 0
							
							PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_NAMES - Moving to CORONA_INVITED_PENDING with request ID: ", g_CoronaInvitedPlayers.iGamertagRequestID)
							
							g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].invitedNameState = CORONA_INVITED_PENDING
							g_CoronaInvitedPlayers.iRequestCount++
							
						#IF IS_DEBUG_BUILD
						ELSE
							IF GET_GAME_TIMER() % 1000 < 75
								PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_NAMES - NETWORK_DISPLAYNAMES_FROM_HANDLES_START invalid request id: ", g_CoronaInvitedPlayers.iGamertagRequestID)
							ENDIF
						#ENDIF
						ENDIF						
					ENDIF
				BREAK
				
				CASE CORONA_INVITED_PENDING

					INT iNameState
					// Grab our status for pending gamertags / names
					iNameState = NETWORK_GET_DISPLAYNAMES_FROM_HANDLES(g_CoronaInvitedPlayers.iGamertagRequestID, tlGamerNames, 1)

					SWITCH iNameState
						
						// This is a fail
						CASE -1
							PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_NAMES - Request: ", g_CoronaInvitedPlayers.iGamertagRequestID, " failed. Reset to CORONA_INVITED_REQUEST_NAME")
							g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].invitedNameState = CORONA_INVITED_REQUEST_NAME
							g_CoronaInvitedPlayers.iRequestCount--
							PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_NAMES - New Request count: ", g_CoronaInvitedPlayers.iRequestCount)
						BREAK
						
						// This is successful
						CASE 0
							g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].gamerName = tlGamerNames[0]
							g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].invitedNameState = CORONA_INVITED_HAS_NAME
							g_CoronaInvitedPlayers.iRequestCount--
							PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_NAMES - New Request count: ", g_CoronaInvitedPlayers.iRequestCount)
							
							PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_NAMES - Got gamertag: ", g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].gamerName)
							
							IF IS_PLAYER_ALREADY_IN_CORONA(g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].gamerHandle)
								PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_NAMES - Player is now in our corona. Clean up: ", g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].gamerName)
								REMOVE_INVITED_GAMER_TO_CORONA_LIST(g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].gamerHandle, tlGamerNames[0])
							ELSE
								GAMER_HANDLE aGamerHandle
								aGamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
								
								BROADCAST_CORONA_INVITE_PLAYERS_DETAILS(aGamerHandle, g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].gamerName, g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToGetGamerTag].invitedPlayerStatus)

								SET_TRANSITION_SESSION_PLAYER_DATA_REFRESH()
							ENDIF
						BREAK
						
						// Still pending
						CASE 1
							#IF IS_DEBUG_BUILD
							IF GET_GAME_TIMER() % 1000 < 75
								PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_NAMES - CORONA_INVITED_PENDING for ", g_CoronaInvitedPlayers.iPlayerToGetGamerTag, " NETWORK_GET_DISPLAYNAMES_FROM_HANDLES() = 1")
							ENDIF
							#ENDIF
						BREAK
					ENDSWITCH
				BREAK
			
			ENDSWITCH
			
		ELSE
			g_CoronaInvitedPlayers.iPlayerToGetGamerTag++
		
			IF g_CoronaInvitedPlayers.iPlayerToGetGamerTag >= NUM_NETWORK_PLAYERS
				g_CoronaInvitedPlayers.iPlayerToGetGamerTag = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Finds the gamer in the array and updates their crew tag
PROC ADD_CREW_TAG_TO_TRANSITION_PLAYER(GAMER_HANDLE &aGamerHandleForCrew, TEXT_LABEL_15 &tlCrewTag)
	
	INT i = 0
	REPEAT NUM_NETWORK_PLAYERS i
	
		// Process the normal joined transition players
		IF NOT IS_CORONA_BIT_SET(CORONA_PROCESSING_GAMER_CREW_TAG_INVITED)
			IF IS_GAMER_HANDLE_VALID(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].hGamer)
				
				IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].hGamer, aGamerHandleForCrew)
					g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].tlCrewTag = tlCrewTag
					g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].iGamerCrewState = ciTRANSITION_SESSIONS_CREW_STATE_COMPLETE
					
					PRINTLN("[CORONA] ADD_CREW_TAG_TO_TRANSITION_PLAYER - Update the crew tag of ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].szGamerTag, " Tag: ", tlCrewTag)
					
					SET_TRANSITION_SESSION_PLAYER_DATA_REFRESH()
				ENDIF
			ENDIF
		ELSE
			IF IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle)
				
				IF NETWORK_ARE_HANDLES_THE_SAME(g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerHandle, aGamerHandleForCrew)
					g_CoronaInvitedPlayers.coronaInvitedPlayers[i].tlCrewTag = tlCrewTag
					g_CoronaInvitedPlayers.coronaInvitedPlayers[i].iGamerCrewState = ciTRANSITION_SESSIONS_CREW_STATE_COMPLETE
					
					PRINTLN("[CORONA] ADD_CREW_TAG_TO_TRANSITION_PLAYER - Update the crew tag of (INVITED) ", g_CoronaInvitedPlayers.coronaInvitedPlayers[i].gamerName, " Tag: ", tlCrewTag)
					
					SET_TRANSITION_SESSION_PLAYER_DATA_REFRESH()
				ENDIF
			ENDIF
		ENDIF
	
	ENDREPEAT
	
ENDPROC

FUNC BOOL DOES_CORONA_GAMER_NEED_CREW_TAG(GAMER_HANDLE &aGamerHandle, INT iSlot)

	IF NOT IS_CORONA_BIT_SET(CORONA_PROCESSING_GAMER_CREW_TAG_INVITED)
		IF g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iSlot].iGamerCrewState = ciTRANSITION_SESSIONS_CREW_STATE_REQUEST
		AND IS_GAMER_HANDLE_VALID(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iSlot].hGamer)
		
			PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_CREW_TAG - New player who needs a crew tag: ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iSlot].szGamerTag)
			aGamerHandle = g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iSlot].hGamer
		
			RETURN TRUE
		ENDIF
	ELSE
		IF g_CoronaInvitedPlayers.coronaInvitedPlayers[iSlot].iGamerCrewState = ciTRANSITION_SESSIONS_CREW_STATE_REQUEST
		AND IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[iSlot].gamerHandle)
			
			PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_CREW_TAG - New player who needs a crew tag (INVITED LIST): ", g_CoronaInvitedPlayers.coronaInvitedPlayers[iSlot].gamerName)
			aGamerHandle = g_CoronaInvitedPlayers.coronaInvitedPlayers[iSlot].gamerHandle
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_NEXT_CORONA_GAMER_INVALID_FOR_CREW_CHECK(INT iSlot)

	IF NOT IS_CORONA_BIT_SET(CORONA_PROCESSING_GAMER_CREW_TAG_INVITED)
		IF NOT IS_GAMER_HANDLE_VALID(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iSlot].hGamer)
			RETURN FALSE
		ENDIF
	ELSE
		IF NOT IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[iSlot].gamerHandle)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Keeps searching for all joined / joining players crew tags
PROC MAINTAIN_CORONA_INVITED_PLAYER_CREW_TAG(CORONA_MENU_DATA &coronaMenuData)

	// If we are not processing a player already, find a new one
	IF NOT IS_CORONA_BIT_SET(CORONA_PROCESSING_GAMER_CREW_TAG)
	
		// Is the staggered person we are looking at needing crew tag
		IF DOES_CORONA_GAMER_NEED_CREW_TAG(coronaMenuData.aGamerHandleForCrew, coronaMenuData.iStaggeredPlayerCrewInfo)
						
			coronaMenuData.iGamerCrewTagState = 0
			coronaMenuData.tlGamerCrewTag = ""
			
			SET_CORONA_BIT(CORONA_PROCESSING_GAMER_CREW_TAG)
		ELSE
			// Increment our staggered loop
			coronaMenuData.iStaggeredPlayerCrewInfo++
			
			// Wrap round if we exceed the amount of players
			IF coronaMenuData.iStaggeredPlayerCrewInfo >= NUM_NETWORK_PLAYERS
			OR NOT IS_NEXT_CORONA_GAMER_INVALID_FOR_CREW_CHECK(coronaMenuData.iStaggeredPlayerCrewInfo)
				coronaMenuData.iStaggeredPlayerCrewInfo = 0
				
				IF NOT IS_CORONA_BIT_SET(CORONA_PROCESSING_GAMER_CREW_TAG_INVITED)
					SET_CORONA_BIT(CORONA_PROCESSING_GAMER_CREW_TAG_INVITED)
				ELSE
					CLEAR_CORONA_BIT(CORONA_PROCESSING_GAMER_CREW_TAG_INVITED)
				ENDIF
				
			ENDIF
		ENDIF
	ELSE
		// Check handle is valid
		IF IS_GAMER_HANDLE_VALID(coronaMenuData.aGamerHandleForCrew)
		
			IF NOT IS_CORONA_BIT_SET(CORONA_REQUESTING_NEW_PLAYER_CREW_INFO)
		
				IF GET_CREW_TAG_FOR_GAMER(coronaMenuData.iGamerCrewTagState, coronaMenuData.aGamerHandleForCrew, coronaMenuData.tlGamerCrewTag)
				
					PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_CREW_TAG - Got gamer tag for player: ", coronaMenuData.tlGamerCrewTag)
							
					ADD_CREW_TAG_TO_TRANSITION_PLAYER(coronaMenuData.aGamerHandleForCrew, coronaMenuData.tlGamerCrewTag)
					CLEAR_CORONA_BIT(CORONA_PROCESSING_GAMER_CREW_TAG)
				ENDIF
			ELSE
				IF coronaMenuData.iGamerCrewTagState != 0
					PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_CREW_TAG - Crew Info retrieved for different player. Reset state")
					coronaMenuData.iGamerCrewTagState = 0
				ENDIF
			ENDIF
		ELSE
			CLEAR_CORONA_BIT(CORONA_PROCESSING_GAMER_CREW_TAG)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Has the player status expired on the invited player list
FUNC BOOL HAS_CORONA_INVITED_PLAYER_STATUS_EXPIRED(INT iPlayerIndex)

	// IF the player's status is ACCEPTED, we should check they are not still joining
	IF g_CoronaInvitedPlayers.coronaInvitedPlayers[iPlayerIndex].invitedPlayerStatus = SCALEFORM_PLAYER_ROW_ACCEPTED
		
		// If the handle is valid and they are on our session we should check the joining / reserved bitsets from Keith
		IF IS_GAMER_HANDLE_VALID(g_CoronaInvitedPlayers.coronaInvitedPlayers[iPlayerIndex].gamerHandle)
			IF NETWORK_IS_GAMER_IN_MY_SESSION(g_CoronaInvitedPlayers.coronaInvitedPlayers[iPlayerIndex].gamerHandle)
			
				// Grab the details of the reserved, joining players
				INT missionUniqueID		= Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
				INT iBS_ReservedPlayers	= Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission(missionUniqueID)
				INT iBS_JoiningPlayers	= Get_Bitfield_Of_Players_On_Or_Joining_Mission(missionUniqueID)
				PLAYER_INDEX playerID	= NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(g_CoronaInvitedPlayers.coronaInvitedPlayers[iPlayerIndex].gamerHandle)
		
				IF playerID != INVALID_PLAYER_INDEX()
					// If they are in either of these bitsets, don't yet clean them up unless they are in the corona already
					IF IS_BIT_SET(iBS_ReservedPlayers, NATIVE_TO_INT(playerID))
					OR IS_BIT_SET(iBS_JoiningPlayers, NATIVE_TO_INT(playerID))
					
						RETURN FALSE
					ENDIF					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// If we are in an invited state, we have a longer timeout before cleaning up (2294718)
	IF g_CoronaInvitedPlayers.coronaInvitedPlayers[iPlayerIndex].invitedPlayerStatus = SCALEFORM_PLAYER_ROW_INVITED
		
		// The value has to be greater than 0 otherwise we will never timeout for the invited state
		IF g_sMPTunables.iCorona_Invite_Status_Timeout <= 0
			RETURN FALSE
		ENDIF
		
		// Wait until we have timed out
		IF (g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToUpdateStatus].iTimeout + g_sMPTunables.iCorona_Invite_Status_Timeout) > GET_CLOUD_TIME_AS_INT()
			RETURN FALSE
		ENDIF
	ELSE
		// Else, perform normal time out check
		IF (g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToUpdateStatus].iTimeout + CORONA_INVITED_INVALID_TIMEOUT) > GET_CLOUD_TIME_AS_INT()
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE: Maintain the player invited status (if timeout status, clean up)
PROC MAINTAIN_CORONA_INVITED_PLAYER_STATUS()
	
	IF IS_INVITED_PLAYER_IN_INVALID_CORONA_STATUS(g_CoronaInvitedPlayers.coronaInvitedPlayers[g_CoronaInvitedPlayers.iPlayerToUpdateStatus].invitedPlayerStatus)
		
		IF HAS_CORONA_INVITED_PLAYER_STATUS_EXPIRED(g_CoronaInvitedPlayers.iPlayerToUpdateStatus)
			
			PRINTLN("[CORONA] MAINTAIN_CORONA_INVITED_PLAYER_STATUS - Player in pos: ", g_CoronaInvitedPlayers.iPlayerToUpdateStatus, " has timed out, REMOVE from list")
			
			REMOVE_INVITED_GAMER_FROM_CORONA_LIST(g_CoronaInvitedPlayers.iPlayerToUpdateStatus)
			SET_TRANSITION_SESSION_PLAYER_DATA_REFRESH()	// Refresh our list
		ENDIF
	ENDIF
	
	g_CoronaInvitedPlayers.iPlayerToUpdateStatus++
	
	IF g_CoronaInvitedPlayers.iPlayerToUpdateStatus >= NUM_NETWORK_PLAYERS
		g_CoronaInvitedPlayers.iPlayerToUpdateStatus = 0
	ENDIF
	
ENDPROC

/// PURPOSE: Wait until the time is up and then clean up the list
PROC MAINTAIN_CORONA_END_OF_JOB_PLAYER_LIST(CORONA_MENU_DATA &coronaMenuData)
	
	// Wait until we are at the ground
	IF IS_SKYSWOOP_AT_GROUND()
		
		// Check to see if we have already processed the end of job list
		IF NOT IS_CORONA_BIT_SET(CORONA_END_OF_JOB_LIST_FLUSHED)
			
			// See if the timer has already started
			IF NOT IS_CORONA_BIT_SET(CORONA_END_OF_JOB_FLUSH_TIMER)
				PRINTLN("[CORONA] MAINTAIN_CORONA_END_OF_JOB_PLAYER_LIST - begin timer to flush queue")
				
				coronaMenuData.coronaEndOfJobFlushTimer = GET_NETWORK_TIME()
				SET_CORONA_BIT(CORONA_END_OF_JOB_FLUSH_TIMER)
			ELSE
			
				// Wait until the timer has expired
				IF ABSI(GET_TIME_DIFFERENCE(coronaMenuData.coronaEndOfJobFlushTimer, GET_NETWORK_TIME())) > CORONA_PLAYER_END_OF_JOB_TIMER
					
					PRINTLN("[CORONA] MAINTAIN_CORONA_END_OF_JOB_PLAYER_LIST - time up, flush queue")
					
					INT i
					INT iStillJoining = 0
					PLAYER_INDEX playerID
					
					REPEAT NUM_NETWORK_PLAYERS i
						
						IF IS_GAMER_HANDLE_VALID(g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerHandle)
							
							playerID = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerHandle)
							
							IF playerID != INVALID_PLAYER_INDEX()
								PRINTLN("[CORONA] MAINTAIN_CORONA_END_OF_JOB_PLAYER_LIST - player still joining (in session but not corona) = ", GET_PLAYER_NAME(playerID))
								iStillJoining++
							ELSE
								// Display this player as left.
								ADD_INVITED_PLAYER_TO_CORONA_LIST(	0, g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerHandle, 
																	g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerTag, CORONA_INVITE_TYPE_STANDARD, SCALEFORM_PLAYER_ROW_LEFT)
								
								// Check if still valid, can be modified in ADD_INVITED_PLAYER_TO_CORONA_LIST
								IF IS_GAMER_HANDLE_VALID(g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerHandle)
									REMOVE_END_OF_JOB_GAMER_TO_CORONA_LIST(g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerHandle)
								ENDIF
								
								PRINTLN("[CORONA] MAINTAIN_CORONA_END_OF_JOB_PLAYER_LIST - removed player and shifted array. Reset i from ", i, " to ", (i-1))
								i--
							ENDIF							
						ELSE
							i = NUM_NETWORK_PLAYERS
						ENDIF
					ENDREPEAT
					
					IF iStillJoining > 0
						PRINTLN("[CORONA] MAINTAIN_CORONA_END_OF_JOB_PLAYER_LIST - player still joining (in session but not corona), wait again")
						coronaMenuData.coronaEndOfJobFlushTimer = GET_NETWORK_TIME()
					ELSE
						SET_TRANSITION_SESSION_PLAYER_DATA_REFRESH()
						SET_CORONA_BIT(CORONA_END_OF_JOB_LIST_FLUSHED)	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Clear our end of job player list
PROC CLEAR_CORONA_END_OF_JOB_PLAYER_LIST()
	
	PRINTLN("[CORONA] CLEAR_CORONA_END_OF_JOB_PLAYER_LIST - Clearing the end of job player list")
	
	INT i
	GAMER_HANDLE aGamerHandle
	
	REPEAT NUM_NETWORK_PLAYERS i
	
		g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerHandle = aGamerHandle
		g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].gamerTag = ""
		g_CoronaEndOfJobPlayers.coronaEndOfJobPlayers[i].crewTag = ""
	
	ENDREPEAT
	
ENDPROC

PROC GET_TRANSITION_SESSION_GAMER_ARRAY_FOR_LOBBY(GAMER_HANDLE &gamers[], INT &iCount)
	
	INT i
	GAMER_HANDLE aGamer
	REPEAT NUM_NETWORK_PLAYERS i
		
		aGamer = GET_TRANSITION_PLAYER_GAMER_HANDLE(i)
		IF IS_GAMER_HANDLE_VALID(aGamer)
			gamers[iCount] = aGamer
			iCount++
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE: Maintains who is in lobby and updates middle column
/// PARAMS:
///    iPlayerToCheck - Next player to check
FUNC BOOL MAINTAIN_JOINED_PLAYER_LOBBY_LIST()

	// ******************** NEW TRANSITION SESSION BEHAVIOUR ****************
	IF SHOULD_TRANSITION_SESSION_PLAYER_DATA_REFRESH()
		
		PRINTLN("[CORONA] MAINTAIN_JOINED_PLAYER_LOBBY_LIST - SHOULD_TRANSITION_SESSION_PLAYER_DATA_REFRESH() returned TRUE")
		CLEAR_TRANSITION_SESSION_PLAYER_DATA_REFRESH()

		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_JOINED_PLAYER_COUNT()

	// ******************** NEW TRANSITION SESSION BEHAVIOUR ****************
	IF IS_CORONA_BIT_SET(CORONA_UPDATE_TO_PLAYER_READY_STATUS)
		
		CLEAR_CORONA_BIT(CORONA_UPDATE_TO_PLAYER_READY_STATUS)

		// bugstar://7492305 - Gen9 - Quitf "x:\gta5\src\dev_gen9_sga\rage\framework\src\vcproj\RageFramework\_Unity/../../../fwnet/neteventmgr.cpp(952): 0: Network event pool is full"
		IF IS_BIT_SET(g_sFixerFlow.Bitset3, ciFIXER_FLOW_BITSET3__ENABLE_SEND_TRANSITION_PLAYER_DATA)

			// If we are in a launched transition session and the player status has updated, sync latest stats
			IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
			
				PRINTLN("MAINTAIN_JOINED_PLAYER_COUNT - Playlist / Launched transition. Send local player data")
			
				GAMER_HANDLE aLocalGamer = GET_LOCAL_GAMER_HANDLE()
				SETUP_AND_SEND_INIT_TRANSITION_PLAYER_DATA(aLocalGamer, TRUE)
			ENDIF
			
		ENDIF

		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC GET_STORED_MP_MENU_PLAYER_COMPONENTS(PED_VARIATION_STRUCT& ClothesConstruct, INT iSlot = -1)

	ClothesConstruct.iTextureVariation[0] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_HEAD,     iSlot)
	ClothesConstruct.iTextureVariation[1] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_BERD,     iSlot)
	ClothesConstruct.iTextureVariation[2] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_HAIR,     iSlot)
	ClothesConstruct.iTextureVariation[3] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_TORSO,    iSlot)
	ClothesConstruct.iTextureVariation[4] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_LEG,      iSlot)
	ClothesConstruct.iTextureVariation[5] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_HAND,     iSlot)
	ClothesConstruct.iTextureVariation[6] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_FEET,     iSlot)
	ClothesConstruct.iTextureVariation[7] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_TEETH,    iSlot)
	ClothesConstruct.iTextureVariation[8] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_SPECIAL,  iSlot)
	ClothesConstruct.iTextureVariation[9] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_SPECIAL2, iSlot)
	ClothesConstruct.iTextureVariation[10] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_DECL,     iSlot)
	ClothesConstruct.iTextureVariation[11] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_JBIB,     iSlot)
	
	ClothesConstruct.iDrawableVariation[0] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_HEAD,     iSlot)
	ClothesConstruct.iDrawableVariation[1] =  GET_STORED_MP_PLAYER_COMPONENT_MENU_BERD_DRAWABLE(iSlot)
	ClothesConstruct.iDrawableVariation[2] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_HAIR,     iSlot)
	ClothesConstruct.iDrawableVariation[3] =  GET_STORED_MP_PLAYER_COMPONENT_MENU_TORSO_DRAWABLE(iSlot)
	ClothesConstruct.iDrawableVariation[4] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_LEG,      iSlot)
	ClothesConstruct.iDrawableVariation[5] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_HAND,     iSlot)
	ClothesConstruct.iDrawableVariation[6] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_FEET,     iSlot)
	ClothesConstruct.iDrawableVariation[7] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_TEETH,   	iSlot)
	ClothesConstruct.iDrawableVariation[8] =  GET_STORED_MP_PLAYER_COMPONENT_MENU_SPECIAL_DRAWABLE(iSlot)
	ClothesConstruct.iDrawableVariation[9] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_SPECIAL2, iSlot)
	ClothesConstruct.iDrawableVariation[10] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_DECL,     iSlot)
	ClothesConstruct.iDrawableVariation[11] = GET_STORED_MP_PLAYER_COMPONENT_MENU_JBIB_DRAWABLE(iSlot)
	
	ClothesConstruct.iPaletteVariation[0] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_HEAD,     iSlot)
	ClothesConstruct.iPaletteVariation[1] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_BERD,     iSlot)
	ClothesConstruct.iPaletteVariation[2] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_HAIR,     iSlot)
	ClothesConstruct.iPaletteVariation[3] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_TORSO,    iSlot)
	ClothesConstruct.iPaletteVariation[4] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_LEG,      iSlot)
	ClothesConstruct.iPaletteVariation[5] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_HAND,     iSlot)
	ClothesConstruct.iPaletteVariation[6] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_FEET,     iSlot)
	ClothesConstruct.iPaletteVariation[7] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_TEETH,    iSlot)
	ClothesConstruct.iPaletteVariation[8] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_SPECIAL,  iSlot)
	ClothesConstruct.iPaletteVariation[9] =  GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_SPECIAL2, iSlot)
	ClothesConstruct.iPaletteVariation[10] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_DECL,     iSlot)
	ClothesConstruct.iPaletteVariation[11] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PALVAR_JBIB,     iSlot)

	#IF IS_DEBUG_BUILD

	PRINTSTRING("...LOADING MENU COMPONENT variation SLOT = ")PRINTINT(iSlot)PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[0])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[0])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[1])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[1])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[2])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[2])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[3])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[3])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[4])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[4])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[5])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[5])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[6])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[6])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[7])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[7])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[8])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[8])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[9])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[9])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[10])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[10])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU COMPONENT variation [")PRINTINT(ClothesConstruct.iDrawableVariation[11])PRINTSTRING(",")PRINTINT(ClothesConstruct.iTextureVariation[11])PRINTSTRING("]")PRINTNL()
	
	#ENDIF
	
	ClothesConstruct.iPropIndex[0] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_0, iSlot)
	ClothesConstruct.iPropIndex[1] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_1, iSlot)
	ClothesConstruct.iPropIndex[2] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_2, iSlot)
	ClothesConstruct.iPropIndex[3] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_3, iSlot)
	ClothesConstruct.iPropIndex[4] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_4, iSlot)
	ClothesConstruct.iPropIndex[5] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_5, iSlot)
	ClothesConstruct.iPropIndex[6] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_6, iSlot)
	ClothesConstruct.iPropIndex[7] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_7, iSlot)
	ClothesConstruct.iPropIndex[8] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_8, iSlot)
	
	ClothesConstruct.iPropTexture[0] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_0, iSlot)
	ClothesConstruct.iPropTexture[1] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_1, iSlot)
	ClothesConstruct.iPropTexture[2] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_2, iSlot)
	ClothesConstruct.iPropTexture[3] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_3, iSlot)
	ClothesConstruct.iPropTexture[4] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_4, iSlot)
	ClothesConstruct.iPropTexture[5] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_5, iSlot)
	ClothesConstruct.iPropTexture[6] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_6, iSlot)
	ClothesConstruct.iPropTexture[7] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_7, iSlot)
	ClothesConstruct.iPropTexture[8] = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_8, iSlot)
	
	#IF IS_DEBUG_BUILD

	
	PRINTSTRING("...LOADING MENU PROP variation SLOT = ")PRINTINT(iSlot)PRINTNL()
	PRINTSTRING("...LOADING MENU PROP variation [")PRINTINT(ClothesConstruct.iPropIndex[0])PRINTSTRING(",")PRINTINT(ClothesConstruct.iPropTexture[0])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU PROP variation [")PRINTINT(ClothesConstruct.iPropIndex[1])PRINTSTRING(",")PRINTINT(ClothesConstruct.iPropTexture[1])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU PROP variation [")PRINTINT(ClothesConstruct.iPropIndex[2])PRINTSTRING(",")PRINTINT(ClothesConstruct.iPropTexture[2])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU PROP variation [")PRINTINT(ClothesConstruct.iPropIndex[3])PRINTSTRING(",")PRINTINT(ClothesConstruct.iPropTexture[3])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU PROP variation [")PRINTINT(ClothesConstruct.iPropIndex[4])PRINTSTRING(",")PRINTINT(ClothesConstruct.iPropTexture[4])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU PROP variation [")PRINTINT(ClothesConstruct.iPropIndex[5])PRINTSTRING(",")PRINTINT(ClothesConstruct.iPropTexture[5])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU PROP variation [")PRINTINT(ClothesConstruct.iPropIndex[6])PRINTSTRING(",")PRINTINT(ClothesConstruct.iPropTexture[6])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU PROP variation [")PRINTINT(ClothesConstruct.iPropIndex[7])PRINTSTRING(",")PRINTINT(ClothesConstruct.iPropTexture[7])PRINTSTRING("]")PRINTNL()
	PRINTSTRING("...LOADING MENU PROP variation [")PRINTINT(ClothesConstruct.iPropIndex[8])PRINTSTRING(",")PRINTINT(ClothesConstruct.iPropTexture[8])PRINTSTRING("]")PRINTNL()

	#ENDIF

ENDPROC

FUNC BOOL WAS_PLAYER_WEARING_CREW_TSHIRT_PEDMENU(INT iSlot )		
				
	INT iDraw =  GET_STORED_MP_PLAYER_COMPONENT_JBIB_DRAWABLE(iSlot)
    INT iTex = GET_PACKED_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_JBIB, iSlot) 			
				
    IF iDraw = 0 
    AND iTex = 0
		PRINTLN("WAS_PLAYER_WEARING_CREW_TSHIRT_PEDMENU = TRUE ")
	
		RETURN TRUE
	ENDIF
	PRINTLN("WAS_PLAYER_WEARING_CREW_TSHIRT_PEDMENU = FALSE ")	
	RETURN FALSE
ENDFUNC

FUNC BOOL ShouldPlayerWearCrewTshirt_PEDMENU(INT iSlot )
	
	
	
	RETURN WAS_PLAYER_WEARING_CREW_TSHIRT_PEDMENU(iSlot)



	RETURN FALSE
ENDFUNC


PROC PUT_ON_CREW_TSHIRT_PEDMENU(PED_INDEX& aPed, INT iSlot, INT iPicture, INT Red, INT Green, INT Blue)

	IF ShouldPlayerWearCrewTshirt_PEDMENU(iSlot)
		PED_COMP_NAME_ENUM WhichElement	

		IF iPicture = 0
			WhichElement = JBIB_FMM_0_0
		
		ELSE
			WhichElement = JBIB_FMF_0_0
		
		ENDIF

		IF Red > -1
		AND Green > -1
		AND Blue > -1
			PRINTLN("PUT_ON_CREW_TSHIRT_PEDMENU - Runs ")
			SET_PED_COMP_ITEM_CURRENT_MP(aPed, COMP_TYPE_JBIB, WhichElement, FALSE)
			SET_HEAD_BLEND_PALETTE_COLOR(aPed,Red, Green, Blue, 0 )	
			SET_HEAD_BLEND_PALETTE_COLOR(aPed,Red, Green, Blue, 1 )	
			SET_HEAD_BLEND_PALETTE_COLOR(aPed,Red, Green, Blue, 2 )	
			SET_HEAD_BLEND_PALETTE_COLOR(aPed,Red, Green, Blue, 3 )	
		ENDIF

	ENDIF
	
	
ENDPROC

PROC DELETE_ALL_PARENT_PAUSE_MENU_PED(INT iSlot)


	IF DOES_ENTITY_EXIST(g_CharPedCreation[iSlot].pedMum)	
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_CharPedCreation[iSlot].pedMum, FALSE)
			IF NOT IS_ENTITY_A_MISSION_ENTITY(g_CharPedCreation[iSlot].pedMum)
				SET_ENTITY_AS_MISSION_ENTITY(g_CharPedCreation[iSlot].pedMum, FALSE)
				PRINTLN("DELETE_ALL_PARENT_PAUSE_MENU_PED: NOT IS_ENTITY_A_MISSION_ENTITY(g_CharPedCreation[iSlot].pedMum)")
			ELSE
				PRINTLN("DELETE_ALL_PARENT_PAUSE_MENU_PED: IS_ENTITY_A_MISSION_ENTITY(g_CharPedCreation[iSlot].pedMum)")
			ENDIF
			DELETE_PED(g_CharPedCreation[iSlot].pedMum)
		ELSE
			PRINTLN("DELETE_ALL_PARENT_PAUSE_MENU_PED: g_CharPedCreation[iSlot].pedMum Does not belong to this script, skip delete ")
		ENDIF
	ELSE
		PRINTLN("DELETE_ALL_PARENT_PAUSE_MENU_PED: g_CharPedCreation[iSlot].pedMum Does not exist ")
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_CharPedCreation[iSlot].pedDad)	
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_CharPedCreation[iSlot].pedDad, FALSE)

			IF NOT IS_ENTITY_A_MISSION_ENTITY(g_CharPedCreation[iSlot].pedDad)
				SET_ENTITY_AS_MISSION_ENTITY(g_CharPedCreation[iSlot].pedDad, FALSE)
				PRINTLN("NOT IS_ENTITY_A_MISSION_ENTITY(g_CharPedCreation[iSlot].pedDad)")
			ELSE
				PRINTLN("IS_ENTITY_A_MISSION_ENTITY(g_CharPedCreation[iSlot].pedDad)")
			ENDIF
			DELETE_PED(g_CharPedCreation[iSlot].pedDad)
		ELSE
			PRINTLN("DELETE_ALL_PARENT_PAUSE_MENU_PED: g_CharPedCreation[iSlot].pedDad Does not belong to this script, skip delete ")
		ENDIF
	ELSE
		PRINTLN("DELETE_ALL_PARENT_PAUSE_MENU_PED: g_CharPedCreation[iSlot].pedDad Does not exist ")
		
	ENDIF
	

	
ENDPROC

PROC DELETE_CHILD_PED(PED_INDEX &pauseMenuPed)

	IF DOES_ENTITY_EXIST(pauseMenuPed)	
		IF IS_ENTITY_A_MISSION_ENTITY(pauseMenuPed)
			SET_ENTITY_AS_MISSION_ENTITY(pauseMenuPed, FALSE)
			PRINTLN("DELETE_CHILD_PED: Calling SET_ENTITY_AS_MISSION_ENTITY now ")
		ELSE
			PRINTLN("DELETE_CHILD_PED: IS_ENTITY_A_MISSION_ENTITY(pauseMenuPed) = FALSE already ")
		ENDIF
		DELETE_PED(pauseMenuPed)
	ENDIF

ENDPROC

FUNC BOOL GENERATE_PAUSE_MENU_PED(GAMER_HANDLE& aGamer, PED_INDEX &pauseMenuPed, INT iSlot, INT Red = -1, INT Green = -1, INT Blue = -1)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	
		IF IS_GAMER_HANDLE_VALID(aGamer)
			
			IF NETWORK_IS_GAMER_IN_MY_SESSION(aGamer)
				
					PLAYER_INDEX PlayerFriendIndex = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(aGamer)
					PED_INDEX PedFriendIndex = GET_PLAYER_PED(PlayerFriendIndex)
					IF PlayerFriendIndex <> INVALID_PLAYER_INDEX()
						IF IS_NET_PLAYER_OK(PlayerFriendIndex, FALSE)
							IF NOT IS_PED_AN_ANIMAL(PedFriendIndex)
								//IF PlayerFriendIndex = PLAYER_ID() 
								//OR NETWORK_GET_CURRENTLY_SELECTED_GAMER_HANDLE_FROM_INVITE_MENU(aGamer) // This takes gamer handle by reference and so changes it. Don't need this
								IF (DOES_ENTITY_EXIST(PedFriendIndex)
								AND NOT IS_ENTITY_DEAD(PedFriendIndex)
								//AND NETWORK_HAS_CONTROL_OF_ENTITY(PedFriendIndex) // Removed due to us never successfully creating an in-session ped (do we need the code assert?)
								AND HAS_PED_HEAD_BLEND_FINISHED(PedFriendIndex))
									PED_INDEX FriendIndex = GET_PLAYER_PED(PlayerFriendIndex)
									IF DOES_ENTITY_EXIST(FriendIndex)
										IF NOT DOES_ENTITY_EXIST(pauseMenuPed)
										
											
											PRINTLN("GENERATE_PAUSE_MENU_PED: CLONE_PED_TO_TARGET ")

											MODEL_NAMES mPlayer = GET_ENTITY_MODEL(FriendIndex)
											VECTOR vTargetPos = GET_ENTITY_COORDS(FriendIndex, FALSE)
											vTargetPos.Z = vTargetPos.Z + 2.0
											pauseMenuPed = CREATE_PED(PEDTYPE_SPECIAL, mPlayer, vTargetPos, 0.0, FALSE, FALSE)

											PRINTLN("GENERATE_PAUSE_MENU_PED: created ped ", NATIVE_TO_INT(pauseMenuPed))


											PRINTLN("GENERATE_PAUSE_MENU_PED: SET_ENTITY_COLLISION(pauseMenuPed, FALSE) 1")
											SET_ENTITY_VISIBLE(pauseMenuPed, FALSE)
											SET_ENTITY_COLLISION(pauseMenuPed, FALSE)
											
											CLONE_PED_TO_TARGET(FriendIndex, pauseMenuPed)
											
											SET_ENTITY_VISIBLE(pauseMenuPed, FALSE)
											SET_ENTITY_COLLISION(pauseMenuPed, FALSE)
											
											FriendIndex = NULL
											SET_PED_LOD_MULTIPLIER(pauseMenuPed, 4.9)
											FINALIZE_HEAD_BLEND(pauseMenuPed)	
											SET_PED_MONEY(pauseMenuPed, 0)
											FREEZE_ENTITY_POSITION(pauseMenuPed, TRUE)
											PRINTLN("GENERATE_PAUSE_MENU_PED: FREEZE_ENTITY_POSITION on ped ", NATIVE_TO_INT(pauseMenuPed))
										ENDIF
										
										RETURN TRUE
									ELSE
										PRINTLN("GENERATE_PAUSE_MENU_PED: PED DOES_ENTITY_EXIST FRIEND = FALSE ")
										RETURN FALSE 
									ENDIF
								ELSE 	
									PRINTLN("GENERATE_PAUSE_MENU_PED: PED IS IN SESSION RETURN FALSE ")
									RETURN FALSE
								ENDIF
							ELSE 	
								PRINTLN("GENERATE_PAUSE_MENU_PED: IS_PLAYER_AN_ANIMAL RETURN FALSE ")
								RETURN FALSE
							ENDIF
						ELSE
							PRINTLN("GENERATE_PAUSE_MENU_PED: IS_NET_PLAYER_OK FALSE ")
							RETURN FALSE
						ENDIF
					ELSE
						PRINTLN("GENERATE_PAUSE_MENU_PED: PlayerFriendIndex = INVALID_PLAYER_INDEX() ")
						RETURN FALSE
					ENDIF
			ELSE
				PRINTLN("GENERATE_PAUSE_MENU_PED: This player is not in your session")
			ENDIF
		ENDIF
	ELSE
		PRINTLN("GENERATE_PAUSE_MENU_PED: NETWORK_IS_GAME_IN_PROGRESS() = FALSE ")
		RETURN FALSE
	ENDIF
	
	PED_VARIATION_STRUCT PedsClothes
	INT iTeam = GET_PACKED_MENU_STAT_INT(PACKED_CHAR_TEAM, iSlot)
	INT iPicture = GET_PACKED_MENU_STAT_INT(PACKED_CHAR_PICTURE, iSlot)
	
	PRINTLN("GENERATE_PAUSE_MENU_PED: iTeam = ", iTeam)
	PRINTLN("GENERATE_PAUSE_MENU_PED: iPicture = ", iPicture)
	PRINTLN("GENERATE_PAUSE_MENU_PED: NETWORK_IS_SESSION_ACTIVE = ", NETWORK_IS_SESSION_ACTIVE())
	
	MODEL_NAMES FinalPedModel = GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU(iTeam, iPicture)
//	VECTOR Position = GET_FINAL_RENDERED_CAM_COORD()

	
	IF IS_MODEL_VALID(FinalPedModel) = FALSE
		RETURN FALSE
	ENDIF
	
		
//	REQUEST_MENU_PED_MODEL(FinalPedModel)
//
//	IF HAS_MODEL_LOADED(FinalPedModel)
//	
//		IF bCreated = FALSE
//		
//			NET_NL()NET_PRINT("GENERATE_PAUSE_MENU_PED: NOT bCreated(pauseMenuPed) ")
//		
//			pauseMenuPed = CREATE_PED(PEDTYPE_SPECIAL, FinalPedModel, Position, 0, FALSE, FALSE)
//	//		CLEAR_PED_TASKS_IMMEDIATELY(FinalPed)
//			SET_PED_DESIRED_HEADING(pauseMenuPed, GET_ENTITY_HEADING(pauseMenuPed))
//			SET_ENTITY_COLLISION(pauseMenuPed, FALSE)
//			SET_ENTITY_VISIBLE(pauseMenuPed, FALSE)
//			FREEZE_ENTITY_POSITION(pauseMenuPed, TRUE)
//			bCreated = TRUE
//		ELSE
		
			PRINTLN("GENERATE_PAUSE_MENU_PED: bCreated(pauseMenuPed) & CREATE_PEDS_MENUPED(pauseMenuPed, iSlot)= FALSE ")
		
			IF CREATE_PEDS_MENUPED(pauseMenuPed, iTeam, iSlot)	
			AND DOES_ENTITY_EXIST(pauseMenuPed) AND HAS_PED_HEAD_BLEND_FINISHED(pauseMenuPed)
			
//				FINALIZE_HEAD_BLEND(pauseMenuPed)			
									
				GET_STORED_MP_MENU_PLAYER_COMPONENTS(PedsClothes, iSlot)
				SET_PED_VARIATIONS(pauseMenuPed, PedsClothes, TRUE)
				GIVE_PED_MENU_STORED_HAIR(pauseMenuPed, iSlot, TRUE)
				
				
		//		SET_MP_PLAYERS_MENU_START_FACE_PEDMENU(pauseMenuPed, iSlot)
				SET_MP_CHARACTER_TATTOOS_AND_PATCHES_PEDMENU(pauseMenuPed, iSlot)
				
				PUT_ON_CREW_TSHIRT_PEDMENU(pauseMenuPed, iSlot, iPicture, Red, Green, Blue)
				
				SET_ENTITY_VISIBLE(pauseMenuPed, FALSE)
				SET_ENTITY_INVINCIBLE(pauseMenuPed, TRUE)
				SET_ENTITY_COLLISION(pauseMenuPed, FALSE)
				PRINTLN("GENERATE_PAUSE_MENU_PED: SET_ENTITY_COLLISION(pauseMenuPed, FALSE) 2")
				
//				DRESS_FREEMODE_PLAYER_AT_START_TORSO(pauseMenuPed,TSHIRT_STATES_INGAME_USE_STORED, iSlot, FALSE )
//				NETWORK_APPLY_PED_SCAR_DATA(pauseMenuPed, iSlot)
				SET_PED_MONEY(pauseMenuPed, 0)
				FINALIZE_HEAD_BLEND(pauseMenuPed)
				PRINTLN("GENERATE_PAUSE_MENU_PED: CREATE_PEDS_MENUPED(pauseMenuPed, iSlot)= TRUE ")

				RETURN TRUE
			#IF IS_DEBUG_BUILD
			ELSE
				IF DOES_ENTITY_EXIST(pauseMenuPed)
					PRINTLN("GENERATE_PAUSE_MENU_PED: DOES_ENTITY_EXIST = ", DOES_ENTITY_EXIST(pauseMenuPed))
					PRINTLN("GENERATE_PAUSE_MENU_PED: HAS_PED_HEAD_BLEND_FINISHED = ", HAS_PED_HEAD_BLEND_FINISHED(pauseMenuPed))
				ELSE
					PRINTLN("GENERATE_PAUSE_MENU_PED: DOES_ENTITY_EXIST = ", DOES_ENTITY_EXIST(pauseMenuPed))
					PRINTLN("GENERATE_PAUSE_MENU_PED: HAS_PED_HEAD_BLEND_FINISHED = ", FALSE)					
				ENDIF
				#ENDIF
			ENDIF
//		ENDIF
//	ENDIF
	RETURN FALSE
ENDFUNC



// Check if the contact mission is safe for corona where host is
PROC MAINTAIN_HOST_CONTACT_MISSION_CORONA(CORONA_MENU_DATA &coronaMenuData, BOOL bContactMission, BOOL bHost)

	// If the player is not the host then they don't need to do this.
	IF NOT bHost
		EXIT
	ENDIF

	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
		IF bContactMission
			
			IF NOT IS_CORONA_BIT_SET(CORONA_HOST_TESTED_CONTACT_MISSION_CORONA)			
				BOOL bSafeToUse
				IF IS_AREA_CLEAR_FOR_CORONA(bSafeToUse, coronaMenuData.coronaShapeTest, g_TransitionSessionNonResetVars.PlayerMissionStartLocation)
					
					IF bSafeToUse
						PRINTLN("[CORONA] MAINTAIN_HOST_CONTACT_MISSION_CORONA - IS_AREA_CLEAR_FOR_CORONA, bSafeToUse = TRUE, ", g_TransitionSessionNonResetVars.PlayerMissionStartLocation)
						g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_ADHOC_CORONA] = 1
						
						SEND_EVENT_NETWORK_TRANSITION_PARAMETER_CHANGED(ciMISSION_HOST_OPTION_ADHOC_CORONA, g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_ADHOC_CORONA])
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[CORONA] MAINTAIN_HOST_CONTACT_MISSION_CORONA - IS_AREA_CLEAR_FOR_CORONA, bSafeToUse = FALSE, ", g_TransitionSessionNonResetVars.PlayerMissionStartLocation)
					#ENDIF
					
					ENDIF
					
					SET_CORONA_BIT(CORONA_HOST_TESTED_CONTACT_MISSION_CORONA)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL CORONA_SHOULD_SET_INSTANCED_MISSION_TELEMETRY()
	IF IS_THIS_A_FIXER_SHORT_TRIP()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT CORONA_GET_INSTANCED_MISSION_TELEMETRY_GROUP_ID()
	RETURN g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers
ENDFUNC

FUNC INT CORONA_GET_INSTANCED_MISSION_TELEMETRY_MISSION_CATEGORY()
	IF IS_THIS_A_FIXER_SHORT_TRIP()
		RETURN ENUM_TO_INT(INSTANCED_MISSION_TELEMETRY_CATEGORY_FIXER_SHORT_TRIP)
	ENDIF
	RETURN ENUM_TO_INT(INSTANCED_MISSION_TELEMETRY_CATEGORY_INVALID)
ENDFUNC

FUNC INT CORONA_GET_INSTANCED_MISSION_TELEMETRY_MISSION_NAME()
	RETURN g_FMMC_STRUCT.iRootContentIDHash
ENDFUNC

FUNC INT CORONA_GET_INSTANCED_MISSION_TELEMETRY_SESSION_ID()
	RETURN -1
ENDFUNC

FUNC INT CORONA_GET_INSTANCED_MISSION_TELEMETRY_TYPE()
	RETURN g_FMMC_STRUCT.iMissionType
ENDFUNC

FUNC INT CORONA_GET_INSTANCED_MISSION_TELEMETRY_LOCATION()
	RETURN -1
ENDFUNC

FUNC INT CORONA_GET_INSTANCED_MISSION_TELEMETRY_LAUNCH_METHOD()
	RETURN GET_FM_JOB_ENTERY_TYPE()
ENDFUNC

FUNC INT CORONA_GET_INSTANCED_MISSION_TELEMETRY_LAUNCHER_RANK()
	PLAYER_INDEX piLauncher = GET_CORONA_HOST_PLAYER_INDEX()
	IF NATIVE_TO_INT(piLauncher) != -1
		RETURN GET_PLAYER_RANK(piLauncher)
	ENDIF
	RETURN -1
ENDFUNC

FUNC INT CORONA_GET_INSTANCED_MISSION_TELEMETRY_PROPERTIES_BITSET()
	RETURN GET_PROPERTIES_OWNED_BS(TRUE)
ENDFUNC

FUNC INT CORONA_GET_INSTANCED_MISSION_TELEMETRY_PROPERTIES_BITSET_2()
	RETURN GET_PROPERTIES_OWNED_BS(FALSE)
ENDFUNC

FUNC INT CORONA_GET_INSTANCED_MISSION_TELEMETRY_DIFFICULTY()
	RETURN g_FMMC_STRUCT.iDifficulity
ENDFUNC


/// PURPOSE:
///    Save out telemetry data used throughout game at the end of the corona.
PROC SAVE_CORONA_TELEMETRY_DATA_FOR_JOB(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails, CORONA_MENU_DATA &coronaMenuData)

	// Reset out values before saving out
	g_iVersusOutfitStyleSetup 	= -2
	g_iVersusOutfitStyleChoice 	= -2
	PRINTLN(" SAVE_CORONA_TELEMETRY_DATA_FOR_JOB sLaunchMissionDetails.iMissionType = ", sLaunchMissionDetails.iMissionType)
	PRINTLN(" SAVE_CORONA_TELEMETRY_DATA_FOR_JOB 	sLaunchMissionDetails.iRootContentIDHash = ", 	sLaunchMissionDetails.iRootContentIDHash)

	SWITCH sLaunchMissionDetails.iMissionType
		CASE FMMC_TYPE_MISSION
			
			IF g_FMMC_STRUCT.iMissionSubType  = FMMC_MISSION_TYPE_VERSUS
			OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_COOP
				
				g_iVersusOutfitStyleSetup	= GET_CORONA_VERSUS_OUTFIT_SETUP()
				g_iVersusOutfitStyleChoice	= GET_CORONA_VERSUS_OUTFIT_STYLE()
				
				IF g_iVersusOutfitStyleSetup = VS_STYLE_PLAYER_OWNED
					g_iVersusOutfitStyleSetup = PLAYER_OWNED_VS_STYLE_TELEMETRY			// Override the telemetry value for 2578310 (can't send -1)
				ENDIF
				
				PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - Saving details for Versus Mission: ")
				PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  g_iVersusOutfitStyleSetup:		", g_iVersusOutfitStyleSetup)
				PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  g_iVersusOutfitStyleChoice:		", g_iVersusOutfitStyleChoice)
				
			ENDIF
			
			// Save details for heist telemetry.
			IF IS_THIS_MISSION_A_PRE_PLANNING_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistPlanning)
			OR (IS_THIS_MISSION_A_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistMission)
				AND Is_This_A_Known_Heist_Finale(g_FMMC_STRUCT.iRootContentIDHash))
				
				g_sJobHeistInfo.m_usedQuickRestart	= 0
				g_sJobHeistInfo.m_role 				= GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
				g_sJobHeistInfo.m_clothes 			= 0
				g_sJobHeistInfo.m_ishost 			= AM_I_LEADER_OF_HEIST()
				g_sJobHeistInfo.m_outfitChoiceBy	= GET_CORONA_OUTFIT_ACCESS_LEVEL()
				//g_sJobHeistInfo.m_outfitChoiceType 	= g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP]
				
				// Need to track the cash cut percentage if in a finale
				IF IS_THIS_MISSION_A_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistMission)
				AND Is_This_A_Known_Heist_Finale(g_FMMC_STRUCT.iRootContentIDHash)
					g_sJobHeistInfo.m_cashcutpercentage 	= g_HeistPlanningClient.m_cashcutpercentage
					g_sJobHeistInfo.m_TimePlanningBoard 	= g_HeistPlanningClient.m_TimePlanningBoard
				ENDIF
				
				// m_outfitStyle - Check our value is valid for telemetry
				IF g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].iCloneOutfitStyle  >= 0
					g_sJobHeistInfo.m_outfitStyle		= g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].iCloneOutfitStyle
				ELSE
					IF g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].iCloneOutfitStyle = -2
						g_sJobHeistInfo.m_outfitStyle		= ENUM_TO_INT(OUTFIT_STYLE_NOT_SET)	// 998
					ELSE
						g_sJobHeistInfo.m_outfitStyle		= ENUM_TO_INT(OUTFIT_STYLE_NONE)	// 999
					ENDIF
				ENDIF
				
				// m_outfitChoiceType  - Check our value is valid for telemetry
				IF g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP]  >= 0
					g_sJobHeistInfo.m_outfitChoiceType 	= g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP]
				ELSE
					IF g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = -2
						g_sJobHeistInfo.m_outfitChoiceType		= ENUM_TO_INT(OUTFIT_STYLE_NOT_SET)	// 998
					ELSE
						g_sJobHeistInfo.m_outfitChoiceType		= ENUM_TO_INT(OUTFIT_STYLE_NONE)	// 999
					ENDIF
				ENDIF
				
				// track our mask
				IF g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].iCloneMask >= 0
					g_sJobHeistInfo.m_maskChoice 		= g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].iCloneMask
				ELSE
					g_sJobHeistInfo.m_maskChoice 		= ENUM_TO_INT(OUTFIT_STYLE_NONE)	// 999
				ENDIF
									
				// If we have a valid team, record our outfit (else it is left as 0 which is default freemode outfit)
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen != INVALID_HEIST_DATA
				AND GET_LOCAL_DEFAULT_OUTFIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen) != INVALID_HEIST_DATA
					g_sJobHeistInfo.m_clothes 	= GET_LOCAL_DEFAULT_OUTFIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
				ENDIF
				
				g_sJobHeistInfo.m_1stperson		= g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_FIXED_CAMERA]
				
				PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - Saving details for heist telemetry: ")
				PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_role:				", g_sJobHeistInfo.m_role)
				PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_clothes:			", g_sJobHeistInfo.m_clothes)
				PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_ishost:				", g_sJobHeistInfo.m_ishost)
				PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_outfitChoiceBy:		", g_sJobHeistInfo.m_outfitChoiceBy)
				PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_outfitChoiceType:	", g_sJobHeistInfo.m_outfitChoiceType)
				PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_outfitStyle:		", g_sJobHeistInfo.m_outfitStyle)
				PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_maskChoice:			", g_sJobHeistInfo.m_maskChoice)
				PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_cashcutpercentage:	", g_sJobHeistInfo.m_cashcutpercentage)
				PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_TimePlanningBoard:	", g_sJobHeistInfo.m_TimePlanningBoard)
				PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_1stperson:			", g_sJobHeistInfo.m_1stperson)
			
				IF GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_INVALID
					SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_JOIN_CORONA)
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - Job entry type INVALID, setting job entry type: ciMISSION_ENTERY_TYPE_JOIN_CORONA")
				ENDIF

				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_SPINNER))
			
			ENDIF
			
			IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) 
					g_sJobGangopsInfo.m_infos.m_usedQuickRestart	= 0
					g_sJobGangopsInfo.m_infos.m_role 				= GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
					g_sJobGangopsInfo.m_infos.m_clothes 			= 0
					g_sJobGangopsInfo.m_infos.m_ishost 			= (GB_GET_LOCAL_PLAYER_GANG_BOSS() = PLAYER_ID())
					g_sJobGangopsInfo.m_infos.m_outfitChoiceBy	= GET_CORONA_OUTFIT_ACCESS_LEVEL()
					//g_sJobGangopsInfo.m_infos.m_outfitChoiceType 	= g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP]
					
					// Need to track the cash cut percentage if in a finale
					IF GANGOPS_FLOW_IS_MISSION_A_GANGOPS_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) 
//						g_sJobGangopsInfo.m_infos.m_cashcutpercentage 	= g_HeistPlanningClient.m_cashcutpercentage
						g_sJobGangopsInfo.m_infos.m_TimePlanningBoard 	= ABSI(GET_TIME_DIFFERENCE(g_tsHeistPlanningBoardTime, GET_NETWORK_TIME()))
						PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - Setting g_sJobGangopsInfo.m_infos.m_TimePlanningBoard: ", g_sJobGangopsInfo.m_infos.m_TimePlanningBoard)
					ENDIF
					
					// m_outfitStyle - Check our value is valid for telemetry
					IF g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].iCloneOutfitStyle  >= 0
						g_sJobGangopsInfo.m_infos.m_outfitStyle		= g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].iCloneOutfitStyle
					ELSE
						IF g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].iCloneOutfitStyle = -2
							g_sJobGangopsInfo.m_infos.m_outfitStyle		= ENUM_TO_INT(OUTFIT_STYLE_NOT_SET)	// 998
						ELSE
							g_sJobGangopsInfo.m_infos.m_outfitStyle		= ENUM_TO_INT(OUTFIT_STYLE_NONE)	// 999
						ENDIF
					ENDIF
					
					// m_outfitChoiceType  - Check our value is valid for telemetry
					IF g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP]  >= 0
						g_sJobGangopsInfo.m_infos.m_outfitChoiceType 	= g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP]
					ELSE
						IF g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_CLOTHING_SETUP] = -2
							g_sJobGangopsInfo.m_infos.m_outfitChoiceType		= ENUM_TO_INT(OUTFIT_STYLE_NOT_SET)	// 998
						ELSE
							g_sJobGangopsInfo.m_infos.m_outfitChoiceType		= ENUM_TO_INT(OUTFIT_STYLE_NONE)	// 999
						ENDIF
					ENDIF
					
					// track our mask
					IF g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].iCloneMask >= 0
						g_sJobGangopsInfo.m_infos.m_maskChoice 		= g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].iCloneMask
					ELSE
						g_sJobGangopsInfo.m_infos.m_maskChoice 		= ENUM_TO_INT(OUTFIT_STYLE_NONE)	// 999
					ENDIF
										
					// If we have a valid team, record our outfit (else it is left as 0 which is default freemode outfit)
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen != INVALID_HEIST_DATA
					AND GET_LOCAL_DEFAULT_OUTFIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen) != INVALID_HEIST_DATA
						g_sJobGangopsInfo.m_infos.m_clothes 	= GET_LOCAL_DEFAULT_OUTFIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
					ENDIF
					
					g_sJobGangopsInfo.m_infos.m_1stperson		= g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_FIXED_CAMERA]
					
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - Saving details for heist telemetry: ")
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_role:				", g_sJobGangopsInfo.m_infos.m_role)
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_clothes:			", g_sJobGangopsInfo.m_infos.m_clothes)
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_ishost:				", g_sJobGangopsInfo.m_infos.m_ishost)
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_outfitChoiceBy:		", g_sJobGangopsInfo.m_infos.m_outfitChoiceBy)
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_outfitChoiceType:	", g_sJobGangopsInfo.m_infos.m_outfitChoiceType)
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_outfitStyle:		", g_sJobGangopsInfo.m_infos.m_outfitStyle)
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_maskChoice:			", g_sJobGangopsInfo.m_infos.m_maskChoice)
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_cashcutpercentage:	", g_sJobGangopsInfo.m_infos.m_cashcutpercentage)
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_TimePlanningBoard:	", g_sJobGangopsInfo.m_infos.m_TimePlanningBoard)
					PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB ...  m_1stperson:			", g_sJobGangopsInfo.m_infos.m_1stperson)
				
					IF GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_INVALID
						SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_JOIN_CORONA)
						PRINTLN("[CORONA][AMEC] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - Job entry type INVALID, setting job entry type: ciMISSION_ENTERY_TYPE_JOIN_CORONA")
					ENDIF

					BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
					END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_SPINNER))
				
			ENDIF
		
			// Save details for casino telemetry.
			IF VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
				g_sCasino_Story_Telemetry_data.m_UsedQuickRestart	= 0
				g_sCasino_Story_Telemetry_data.m_ishost	= IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
			ENDIF
			
			#IF FEATURE_HEIST_ISLAND
			IF HEIST_ISLAND_FLOW_IS_A_HEIST_ISLAND_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
				g_sIsland_Heist_Finale_Telemetry_data.outfits = GET_LOCAL_DEFAULT_OUTFIT(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].sClientCoronaData.iTeamChosen)
				PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - Island Heist - g_sIsland_Heist_Finale_Telemetry_data.outfits = ", g_sIsland_Heist_Finale_Telemetry_data.outfits)	
			ENDIF
			#ENDIF
			
			IF IS_THIS_A_TUNER_ROBBERY_FINALE()
			#IF FEATURE_FIXER
			OR IS_THIS_A_FIXER_STORY_MISSION()
			#ENDIF
				g_sTuner_Robbery_Finale_Telemetry_data.launcherRank			= GET_PLAYER_RANK(coronaMenuData.piJobLeader)
				g_sTuner_Robbery_Finale_Telemetry_data.properties			= GET_PROPERTIES_OWNED_BS(TRUE)
				g_sTuner_Robbery_Finale_Telemetry_data.properties2			= GET_PROPERTIES_OWNED_BS(FALSE)
				g_sTuner_Robbery_Finale_Telemetry_data.outfits				= g_iMyRaceOutfitEnum
				g_sTuner_Robbery_Finale_Telemetry_data.wLoadout				= g_iMissionLobbyStartingInventoryIndex[0]
				g_sTuner_Robbery_Finale_Telemetry_data.vehicleBoard			= ENUM_TO_INT(g_mnMyRaceModel)
				
				IF IS_THIS_A_FIXER_STORY_MISSION()
					IF IS_THIS_A_FIXER_STORY_MISSION_WITH_OUTFIT_SELECTION()
						g_sTuner_Robbery_Finale_Telemetry_data.outfits		= GET_LOCAL_DEFAULT_OUTFIT(GET_PLAYER_TEAM_IN_CORONA(PLAYER_ID()))
					ELSE
						g_sTuner_Robbery_Finale_Telemetry_data.outfits		= -1
					ENDIF
					g_sTuner_Robbery_Finale_Telemetry_data.wLoadout			= -1
					g_sTuner_Robbery_Finale_Telemetry_data.vehicleBoard		= -1
				ENDIF
				
				PRINTLN("[CORONA][TUNER] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sTuner_Robbery_Finale_Telemetry_data.launcherRank 		= ", g_sTuner_Robbery_Finale_Telemetry_data.launcherRank)
				PRINTLN("[CORONA][TUNER] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sTuner_Robbery_Finale_Telemetry_data.properties 			= ", g_sTuner_Robbery_Finale_Telemetry_data.properties)
				PRINTLN("[CORONA][TUNER] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sTuner_Robbery_Finale_Telemetry_data.properties2 			= ", g_sTuner_Robbery_Finale_Telemetry_data.properties2)
				PRINTLN("[CORONA][TUNER] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sTuner_Robbery_Finale_Telemetry_data.outfits 				= ", g_sTuner_Robbery_Finale_Telemetry_data.outfits)
				PRINTLN("[CORONA][TUNER] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sTuner_Robbery_Finale_Telemetry_data.wLoadout 			= ", g_sTuner_Robbery_Finale_Telemetry_data.wLoadout)
				PRINTLN("[CORONA][TUNER] - SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sTuner_Robbery_Finale_Telemetry_data.vehicleBoard 		= ", g_sTuner_Robbery_Finale_Telemetry_data.vehicleBoard)
				
			ENDIF
		BREAK	
	ENDSWITCH

	IF CORONA_SHOULD_SET_INSTANCED_MISSION_TELEMETRY()
	
		g_sInstanced_Mission_End_Telemetry_data.groupID					= CORONA_GET_INSTANCED_MISSION_TELEMETRY_GROUP_ID()
		g_sInstanced_Mission_End_Telemetry_data.missionCategory			= CORONA_GET_INSTANCED_MISSION_TELEMETRY_MISSION_CATEGORY()
		g_sInstanced_Mission_End_Telemetry_data.missionName				= CORONA_GET_INSTANCED_MISSION_TELEMETRY_MISSION_NAME()
		g_sInstanced_Mission_End_Telemetry_data.sessionID				= CORONA_GET_INSTANCED_MISSION_TELEMETRY_SESSION_ID()
		g_sInstanced_Mission_End_Telemetry_data.type					= CORONA_GET_INSTANCED_MISSION_TELEMETRY_TYPE()
		g_sInstanced_Mission_End_Telemetry_data.location				= CORONA_GET_INSTANCED_MISSION_TELEMETRY_LOCATION()
		g_sInstanced_Mission_End_Telemetry_data.launchMethod			= CORONA_GET_INSTANCED_MISSION_TELEMETRY_LAUNCH_METHOD()
		g_sInstanced_Mission_End_Telemetry_data.launcherRank			= CORONA_GET_INSTANCED_MISSION_TELEMETRY_LAUNCHER_RANK()
		g_sInstanced_Mission_End_Telemetry_data.properties				= CORONA_GET_INSTANCED_MISSION_TELEMETRY_PROPERTIES_BITSET()
		g_sInstanced_Mission_End_Telemetry_data.properties2				= CORONA_GET_INSTANCED_MISSION_TELEMETRY_PROPERTIES_BITSET_2()
		g_sInstanced_Mission_End_Telemetry_data.difficulty				= CORONA_GET_INSTANCED_MISSION_TELEMETRY_DIFFICULTY()
	
		PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sInstanced_Mission_End_Telemetry_data.groupID 				= ", g_sInstanced_Mission_End_Telemetry_data.groupID)
		PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sInstanced_Mission_End_Telemetry_data.missionCategory 		= ", g_sInstanced_Mission_End_Telemetry_data.missionCategory)
		PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sInstanced_Mission_End_Telemetry_data.missionName 			= ", g_sInstanced_Mission_End_Telemetry_data.missionName)
		PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sInstanced_Mission_End_Telemetry_data.sessionID 			= ", g_sInstanced_Mission_End_Telemetry_data.sessionID)
		PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sInstanced_Mission_End_Telemetry_data.type 				= ", g_sInstanced_Mission_End_Telemetry_data.type)
		PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sInstanced_Mission_End_Telemetry_data.location 			= ", g_sInstanced_Mission_End_Telemetry_data.location)
		PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sInstanced_Mission_End_Telemetry_data.launchMethod 		= ", g_sInstanced_Mission_End_Telemetry_data.launchMethod)
		PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sInstanced_Mission_End_Telemetry_data.launcherRank 		= ", g_sInstanced_Mission_End_Telemetry_data.launcherRank)
		PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sInstanced_Mission_End_Telemetry_data.properties 			= ", g_sInstanced_Mission_End_Telemetry_data.properties)
		PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sInstanced_Mission_End_Telemetry_data.properties2 			= ", g_sInstanced_Mission_End_Telemetry_data.properties2)
		PRINTLN("[CORONA] SAVE_CORONA_TELEMETRY_DATA_FOR_JOB - g_sInstanced_Mission_End_Telemetry_data.difficulty 			= ", g_sInstanced_Mission_End_Telemetry_data.difficulty)
	
	ENDIF

ENDPROC

/// PURPOSE: Clear all skyfreeze and blur and remove any running POSTFX. This should only be called if we want
///    to remove all effects on the screen to return to the normal displayed state.
PROC CORONA_CLEAN_UP_SCREEN_STATE(BOOL bStopAllPostFX = TRUE, BOOL bClearPausedRender = TRUE)
	
	PRINTLN("[CORONA] CORONA_CLEAN_UP_SCREEN_STATE - cleaning up screen for in-corona screen")
	DEBUG_PRINTCALLSTACK()
	
	IF bClearPausedRender
	AND NOT GET_CONFIRM_INVITE_INTO_GAME_STATE()
		PRINTLN("[CORONA] CORONA_CLEAN_UP_SCREEN_STATE - Clearing skyfreeze, bClearPausedRender: ", bClearPausedRender)
		// Clean up and reset our render phases as we are in the corona
		SET_SKYFREEZE_CLEAR(TRUE)
	ENDIF
	
	// Clear with fades out
	SET_SKYBLUR_CLEAR()
	
	// Clear any other blur
	DISABLE_SCREENBLUR_FADE()
	
	// Clear any running FX
	IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
		PRINTLN("[CORONA]	- ANIMPOSTFX_STOP(MinigameTransitionIn)")
		ANIMPOSTFX_STOP("MinigameTransitionIn")
	ENDIF
	
	STOP_ALL_CORONA_FX_IN()
	
	IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Win")
		PRINTLN("[CORONA]	- ANIMPOSTFX_STOP(MP_Celeb_Win)")
		ANIMPOSTFX_STOP("MP_Celeb_Win")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Lose")
		PRINTLN("[CORONA]	- ANIMPOSTFX_STOP(MP_Celeb_Lose)")
		ANIMPOSTFX_STOP("MP_Celeb_Lose")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("MP_job_load")
		PRINTLN("[CORONA]	- ANIMPOSTFX_STOP(MP_job_load)")
		ANIMPOSTFX_STOP("MP_job_load")
	ENDIF
	
	// We don't need to control the camera any more
	IF SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
		CLEAR_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
	ENDIF
	
	IF bStopAllPostFX
		PRINTLN("[CORONA]	- bStopAllPostFX = TRUE, stop all postFX.")
		// Clean up all effects since we apply this after.
		ANIMPOSTFX_STOP_ALL()
	ENDIF
	
	
	IF GET_HEIST_PROPERTY_CAM_STAGE() != HEIST_PROPERTY_START
	
		IF NOT GET_HEIST_PROPERTY_CAMERA_STAT_CLEANUP()
			PRINTLN("[AMEC][HEIST_ANIMS] - CORONA CLEANUP - Kill SkyCam due to CORONA cleanup.")
			SET_HEIST_PROPERTY_CAMERA_TO_MOVE_INTO_INTERIOR()
			SET_HEIST_PROPERTY_CAMERA_STAT_CLEANUP()
		ENDIF
	ENDIF

	
ENDPROC










