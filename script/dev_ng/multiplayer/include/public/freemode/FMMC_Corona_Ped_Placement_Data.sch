/***********************************************************\
/ Name: 		FMMC_Corona_Ped_Placement_Data.sch			\
/ Author(s): 	James Adwick								\
/ Purpose: 		Header containing data about ped placement	\
/				within the corona							\
************************************************************/

USING "globals.sch"
USING "FMMC_Corona_Menu.sch"

/// PURPOSE: Grabs the position / rotation of a placement within the apartment for a bespoke corona
PROC GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(VECTOR& vLocation, VECTOR &vRotation, INT iTarget)

	MP_PROP_OFFSET_STRUCT tempStruct
	INT iProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
	
	SWITCH iProperty
		
		CASE PROPERTY_HIGH_APT_1	
		CASE PROPERTY_HIGH_APT_2	
		CASE PROPERTY_HIGH_APT_3	
		CASE PROPERTY_HIGH_APT_4	
		CASE PROPERTY_HIGH_APT_5	
		CASE PROPERTY_HIGH_APT_6	
		CASE PROPERTY_HIGH_APT_7	
		CASE PROPERTY_HIGH_APT_8
		CASE PROPERTY_HIGH_APT_9
		CASE PROPERTY_HIGH_APT_10
		CASE PROPERTY_HIGH_APT_11
		CASE PROPERTY_HIGH_APT_12
		CASE PROPERTY_HIGH_APT_13
		CASE PROPERTY_HIGH_APT_14
		CASE PROPERTY_HIGH_APT_15
		CASE PROPERTY_HIGH_APT_16
		CASE PROPERTY_HIGH_APT_17
			GET_POSITION_AS_OFFSET_FOR_PROPERTY(iProperty, iTarget, tempStruct)
		BREAK
		
		CASE PROPERTY_BUS_HIGH_APT_1
		CASE PROPERTY_BUS_HIGH_APT_2
		CASE PROPERTY_BUS_HIGH_APT_3
		CASE PROPERTY_BUS_HIGH_APT_4
		CASE PROPERTY_BUS_HIGH_APT_5
			GET_POSITION_AS_OFFSET_FOR_PROPERTY(iProperty, iTarget, tempStruct, PROPERTY_BUS_HIGH_APT_1)
		BREAK
		
		DEFAULT
			INT iBaseProperty
			iBaseProperty = GET_BASE_PROPERTY_FROM_PROPERTY(iProperty)
			GET_POSITION_AS_OFFSET_FOR_PROPERTY(iProperty, iTarget, tempStruct, iBaseProperty)
			
			PRINTLN("[CORONA] - GET_PLAYER_PROPERTY_CORONA_PED_POSITION - Property base property is: ", iBaseProperty)
		BREAK

	ENDSWITCH

	vLocation = tempStruct.vLoc
	vRotation = tempStruct.vRot
ENDPROC

#IF FEATURE_CASINO_HEIST
PROC GET_CASINO_HEIST_PED_POSITION(INT iSlot, VECTOR& vPosition, FLOAT& fHeading)
	SWITCH iSlot
		CASE 0
			vPosition = << 2712.928955, -370.848389, -55.780899 >>
			fHeading = 355.291687
			EXIT
		BREAK
		CASE 1
			vPosition = << 2713.310059, -371.458405, -55.780899 >>
			fHeading = 14.9369
			EXIT
		BREAK
		CASE 2
			vPosition = << 2712.476074, -371.496094, -55.780899 >>
			fHeading = 355.042572
			EXIT
		BREAK
		CASE 3
			vPosition = << 2712.079102, -371.839294, -55.780899 >>
			fHeading = 347.080597
			EXIT
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("GET_CASINO_HEIST_PED_POSITION - Invalid slot, zero vector and heading")
	vPosition = <<0.0, 0.0, 0.0>>
	fHeading = 0.0
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
PROC GET_HEIST_ISLAND_PED_POSITION(INT iSlot, VECTOR& vPosition, FLOAT& fHeading)
	SWITCH iSlot
		CASE 0
			vPosition = <<1560.0487, 386.6647, -50.6854>>
			fHeading = 25.1216
			EXIT
		BREAK
		CASE 1
			vPosition = << 1560.5680, 386.4490, -50.6855 >>
			fHeading = 31.9719
			EXIT
		BREAK
		CASE 2
			vPosition = << 1559.8210, 386.0270, -50.6855 >>
			fHeading = 13.2719
			EXIT
		BREAK
		CASE 3
			vPosition = << 1561.0090, 386.3910, -50.6855 >>
			fHeading = 31.6719
			EXIT
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("GET_HEIST_ISLAND_PED_POSITION - Invalid slot, zero vector and heading")
	vPosition = <<0.0, 0.0, 0.0>>
	fHeading = 0.0
ENDPROC
#ENDIF

PROC GET_FIXER_STORY_PED_POSITION(INT iSlot, VECTOR& vPosition, FLOAT& fHeading)
	SWITCH iSlot
		CASE 0
			vPosition = << -1070.0531, -74.4995, -91.2000 >>
			fHeading = 76.7700
			EXIT
		BREAK
		CASE 1
			vPosition = << -1069.6852, -75.0827, -91.2000 >>
			fHeading = 62.9798
			EXIT
		BREAK
		CASE 2
			vPosition = << -1069.6350, -74.1382, -91.2000 >>
			fHeading = 89.6239
			EXIT
		BREAK
		CASE 3
			vPosition = << -1069.2468, -73.6538, -91.2000 >>
			fHeading = 92.6672
			EXIT
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("GET_FIXER_STORY_PED_POSITION - Invalid slot, zero vector and heading")
	vPosition = <<0.0, 0.0, 0.0>>
	fHeading = 0.0
ENDPROC



/// PURPOSE: Returns the vector of the position the ped should be placed for upcoming team corona
/// PARAMS:
///    iTeam - Team the ped is a part of
///    iMemberIndex - The index with this team
FUNC VECTOR GET_CORONA_PED_POSITION_FOR_TEAM(INT iTeam, INT iMemberIndex, FLOAT &fHeading)

	VECTOR vLoc
	VECTOR vRot

	// If the player is in 4:3 resolution, change to team 0 formation
	IF NOT GET_IS_WIDESCREEN()
	AND iTeam = -1
		PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team -1. GET_IS_WIDESCREEN() = FALSE, override to 0")
		iTeam = 0
	ENDIF

	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
		
		SWITCH iTeam
			CASE -1
				SWITCH iMemberIndex
					CASE 0
						vLoc = <<351.1165, 4873.4058, -61.7930>>
						vRot = <<0.0, 0.0, 146.6179>>
					BREAK
					CASE 1
						vLoc = <<352.0273, 4873.8730, -61.7930>>
						vRot = <<0.0, 0.0, 136.1772>>
					BREAK
					CASE 2
						vLoc = <<350.9424, 4874.5859, -61.7930>>
						vRot = <<0.0, 0.0, 146.4783>>
					BREAK
					CASE 3
						vLoc = <<351.7650, 4875.4170, -61.7930>>
						vRot = <<0.0, 0.0, 138.9998>>
					BREAK
				ENDSWITCH
			BREAK
			
			CASE 0
				SWITCH iMemberIndex
					CASE 0
						vLoc = <<351.1165, 4873.4058, -61.7930>>
						vRot = <<0.0, 0.0, 146.6179>>
					BREAK
					CASE 1
						vLoc = <<352.0273, 4873.8730, -61.7930>>
						vRot = <<0.0, 0.0, 136.1772>>
					BREAK
					CASE 2
						vLoc = <<350.9424, 4874.5859, -61.7930>>
						vRot = <<0.0, 0.0, 146.4783>>
					BREAK
					CASE 3
						vLoc = <<351.7650, 4875.4170, -61.7930>>
						vRot = <<0.0, 0.0, 138.9998>>
					BREAK
				ENDSWITCH
			BREAK
			
			CASE 1
				SWITCH iMemberIndex
					CASE 0
						vLoc = << 354.0161, 4872.9834, -61.7926 >>
						vRot = <<0.0, 0.0, 90.0350>>
					BREAK
					CASE 1
						vLoc = << 354.7678, 4872.4663, -61.7930 >>
						vRot = <<0.0, 0.0, 90.0350>>
					BREAK
					CASE 2
						vLoc = << 354.9763, 4873.7925, -61.7930 >>
						vRot = <<0.0, 0.0, 90.0350>>
					BREAK
					CASE 3
						vLoc = << 355.5648, 4873.4170, -61.7930 >>
						vRot = <<0.0, 0.0, 90.0350>>
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		GET_CASINO_HEIST_PED_POSITION(iMemberIndex, vLoc, vRot.z)
	#IF FEATURE_HEIST_ISLAND
	ELIF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_MISSION_FINALE()
		GET_HEIST_ISLAND_PED_POSITION(iMemberIndex, vLoc, vRot.z)
	#ENDIF
	ELIF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ()
		GET_FIXER_STORY_PED_POSITION(iMemberIndex, vLoc, vRot.z)
	ELSE

		SWITCH iTeam
		
			// Special no team case but still display them as a team (used mainly in Heists)
			CASE -1
				SWITCH iMemberIndex
					CASE 0	GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_NO_TEAM_0)	BREAK
					CASE 1	GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_NO_TEAM_1)	BREAK
					CASE 2	GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_NO_TEAM_2)	BREAK
					CASE 3	GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_NO_TEAM_3)	BREAK
					DEFAULT
						PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team -1. iMemberIndex = ", iMemberIndex)
						SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team -1.")
						RETURN <<0, 0, 0>>
					BREAK
				ENDSWITCH
			BREAK
		
			CASE 0
				SWITCH iMemberIndex
					CASE 0	GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_0_0)	BREAK
					CASE 1  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_0_1)	BREAK
					CASE 2  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_0_2)	BREAK
					CASE 3  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_0_3)	BREAK
					DEFAULT
						PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 0. iMemberIndex = ", iMemberIndex)
						SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 0.")
						RETURN <<0, 0, 0>>
					BREAK
				ENDSWITCH
			BREAK
				
			CASE 1
				SWITCH iMemberIndex
					CASE 0	GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_1_0)	BREAK
					CASE 1  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_1_1)	BREAK
					CASE 2  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_1_2)	BREAK
					CASE 3  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_1_3)	BREAK
					DEFAULT
						PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 1. iMemberIndex = ", iMemberIndex)
						SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 1.")
						RETURN <<0, 0, 0>>
					BREAK
				ENDSWITCH
			BREAK
			
			CASE 2
				SWITCH iMemberIndex
					CASE 0	GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_2_0)	BREAK
					CASE 1  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_2_1)	BREAK
					CASE 2  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_2_2)	BREAK
					CASE 3  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_2_3)	BREAK
					DEFAULT
						PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 2. iMemberIndex = ", iMemberIndex)
						SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 2.")
						RETURN <<0, 0, 0>>
					BREAK
				ENDSWITCH
			BREAK
			
			CASE 3
				SWITCH iMemberIndex
					CASE 0	GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_3_0)	BREAK
					CASE 1  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_3_1)	BREAK
					CASE 2  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_3_2)	BREAK
					CASE 3  GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_PLACEMENT_TEAM_3_3)	BREAK
					DEFAULT
						PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 3. iMemberIndex = ", iMemberIndex)
						SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 3.")
						RETURN <<0, 0, 0>>
					BREAK
				ENDSWITCH
			BREAK
				
		ENDSWITCH

	ENDIF

	// Return the vector if valid
	IF NOT IS_VECTOR_ZERO(vLoc)
	
		fHeading = vRot.z
	
		RETURN vLoc
	ENDIF

	PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid team. iTeam = ", iTeam)
	SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid team. 0 <= iTeam < 4")

	RETURN <<0, 0, 0>>
ENDFUNC

/// PURPOSE: Returns the offset for the players tag in the heist layout
FUNC INT GET_CORONA_PED_POSITION_TAG_OFFSET(INT iTeam, INT iMemberIndex)

	// If the player is in 4:3 resolution, change to team 0 formation
	IF NOT GET_IS_WIDESCREEN()
	AND iTeam = -1
		PRINTLN("[CORONA] GET_CORONA_PED_POSITION_TAG_OFFSET - Called with invalid iMemberIndex for team -1. GET_IS_WIDESCREEN() = FALSE, override to 0")
		iTeam = 0
	ENDIF

	SWITCH iTeam
	
		// Special no team case but still display them as a team (used mainly in Heists)
		CASE -1
			SWITCH iMemberIndex
				CASE 0	RETURN -2
				CASE 1	RETURN 0
				CASE 2	RETURN -2
				CASE 3	RETURN 1
				DEFAULT
					PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team -1. iMemberIndex = ", iMemberIndex)
					SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team -1.")
					RETURN 0
				BREAK
			ENDSWITCH
		BREAK
	
		CASE 0
			SWITCH iMemberIndex
				CASE 0	RETURN 0
				CASE 1 	RETURN 0
				CASE 2  RETURN 1
				CASE 3 	RETURN 2
				DEFAULT
					PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 0. iMemberIndex = ", iMemberIndex)
					SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 0.")
					RETURN 0
				BREAK
			ENDSWITCH
		BREAK
			
		CASE 1
			SWITCH iMemberIndex
				CASE 0	RETURN 0
				CASE 1  RETURN 0
				CASE 2  RETURN 1
				CASE 3  RETURN 1
				DEFAULT
					PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 1. iMemberIndex = ", iMemberIndex)
					SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 1.")
					RETURN 0
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH iMemberIndex
				CASE 0	RETURN 1
				CASE 1  RETURN 2
				CASE 2  RETURN 0
				CASE 3  RETURN 2
				DEFAULT
					PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 2. iMemberIndex = ", iMemberIndex)
					SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 2.")
					RETURN 0
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 3
			SWITCH iMemberIndex
				CASE 0	RETURN 2
				CASE 1  RETURN 1
				CASE 2  RETURN 0
				CASE 3  RETURN 0
				DEFAULT
					PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 3. iMemberIndex = ", iMemberIndex)
					SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid iMemberIndex for team 3.")
					RETURN 0
				BREAK
			ENDSWITCH
		BREAK
			
	ENDSWITCH

	PRINTLN("[CORONA] GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid team. iTeam = ", iTeam)
	SCRIPT_ASSERT("GET_CORONA_PED_POSITION_FOR_TEAM - Called with invalid team. 0 <= iTeam < 4")

	RETURN 0
ENDFUNC

/// PURPOSE: Returns the camera coordinate for a specific team in a bespoke corona
FUNC VECTOR GET_BESPOKE_CORONA_CAMERA_PLACEMENT(INT iTeam)
	
	VECTOR vLoc
	VECTOR vRot
	
	// If the player is in 4:3 resolution, change to team 0 formation
	IF NOT GET_IS_WIDESCREEN()
	AND iTeam = -1
		PRINTLN("[CORONA] GET_BESPOKE_CORONA_CAMERA_PLACEMENT - Called with invalid iMemberIndex for team -1. GET_IS_WIDESCREEN() = FALSE, override to 0")
		iTeam = 0
	ENDIF
	
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
	
		SWITCH iTeam
			CASE -1			vLoc = <<349.6579, 4871.3901, -60.3398>>		BREAK
			CASE 0			vLoc = <<349.6579, 4871.3901, -60.3398>>		BREAK
			CASE 1			vLoc = <<351.6961, 4873.0791, -60.3469>>		BREAK
		ENDSWITCH
	
	#IF FEATURE_CASINO_HEIST
	ELIF IS_CORONA_BIT_SET(CORONA_GLOBAL_CASINO_HEIST_FINALE_FLAG)
	
		vLoc = <<2712.6011, -366.6223, -54.0750>>
	
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	ELIF IS_CORONA_BIT_SET(CORONA_GLOBAL_GENERIC_HEIST_FINALE_FLAG)
	
		vLoc = <<1559.4620, 388.2543, -49.1195>>
	
	#ENDIF
	
	ELIF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ()
	
		vLoc = <<-1071.5896, -74.1736, -89.6535>>
	
	ELSE
	
		SWITCH iTeam
			CASE -1		GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_CAMERA_PLACEMENT_NO_TEAM)	BREAK
			CASE 0		GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_CAMERA_PLACEMENT_TEAM_0)	BREAK
			CASE 1		GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_CAMERA_PLACEMENT_TEAM_1)	BREAK
			CASE 2		GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_CAMERA_PLACEMENT_TEAM_2)	BREAK
			CASE 3		GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_CAMERA_PLACEMENT_TEAM_3)	BREAK
		
		ENDSWITCH
		
	ENDIF
	
	RETURN vLoc
ENDFUNC

/// PURSPOE: Returns the camera rotation for a specific team in a bespoke corona
FUNC VECTOR GET_BESPOKE_CORONA_CAMERA_ROTATION(INT iTeam)
	
	VECTOR vLoc
	VECTOR vRot
	
	// If the player is in 4:3 resolution, change to team 0 formation
	IF NOT GET_IS_WIDESCREEN()
	AND iTeam = -1
		PRINTLN("[CORONA] GET_BESPOKE_CORONA_CAMERA_ROTATION - Called with invalid iMemberIndex for team -1. GET_IS_WIDESCREEN() = FALSE, override to 0")
		iTeam = 0
	ENDIF
	
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
	
		SWITCH iTeam
			CASE -1			vRot = <<-0.4184, -0.0000, -25.8622>>		BREAK
			CASE 0			vRot = <<-0.4184, -0.0000, -25.8622>>		BREAK
			CASE 1			vRot = <<1.5399, 0.0000, -81.4707>>		BREAK
		ENDSWITCH
	
	#IF FEATURE_CASINO_HEIST
	ELIF IS_CORONA_BIT_SET(CORONA_GLOBAL_CASINO_HEIST_FINALE_FLAG)
	
		vRot = <<-4.0950, -0.0000, -173.7363>>
	
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	ELIF IS_CORONA_BIT_SET(CORONA_GLOBAL_GENERIC_HEIST_FINALE_FLAG)
	
		vRot = <<-2.9522, -0.0000, -145.9398>>
	
	#ENDIF
	
	ELIF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ()
	
		vRot = <<-3.0222, -0.0000, -89.8774>>
	
	ELSE
	
		SWITCH iTeam
			CASE -1		GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_CAMERA_PLACEMENT_NO_TEAM)	BREAK
			CASE 0		GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_CAMERA_PLACEMENT_TEAM_0)	BREAK
			CASE 1		GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_CAMERA_PLACEMENT_TEAM_1)	BREAK
			CASE 2		GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_CAMERA_PLACEMENT_TEAM_2)	BREAK
			CASE 3		GET_PLAYER_PROPERTY_CORONA_PROPERTY_COORDINATES(vLoc, vRot, MP_PROP_ELEMENT_BESPOKE_CAMERA_PLACEMENT_TEAM_3)	BREAK
		
		ENDSWITCH
	
	ENDIF
	
	RETURN vRot
ENDFUNC

/// PURPOSE: Returns the animation dictionary  
FUNC STRING GET_CORONA_PED_ANIMATION_DICT(INT iTeam, INT iTeamPosIndex)
	
	// If the player is in 4:3 resolution, change to team 0 formation
	IF NOT GET_IS_WIDESCREEN()
	AND iTeam = -1
		PRINTLN("[CORONA] GET_CORONA_PED_ANIMATION_DICT - Called with invalid iMemberIndex for team -1. GET_IS_WIDESCREEN() = FALSE, override to 0")
		iTeam = 0
	ENDIF
	
	//HEIST ANIMS
	IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
	AND IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
		SWITCH iTeam
			CASE -1		RETURN "ANIM@HEISTS@HEIST_CORONA@SINGLE_TEAM"
			CASE 0		
			CASE 1		
			CASE 2		
			CASE 3
				SWITCH iTeamPosIndex
					CASE 0	RETURN "ANIM@HEISTS@HEIST_CORONA@TEAM_IDLES@FEMALE_A"
					CASE 1	RETURN "ANIM@HEISTS@HEIST_CORONA@TEAM_IDLES@MALE_B"
					CASE 2	RETURN "ANIM@HEISTS@HEIST_CORONA@TEAM_IDLES@MALE_C"
					CASE 3	RETURN "ANIM@HEISTS@HEIST_CORONA@TEAM_IDLES@MALE_A"		
				ENDSWITCH
			BREAK
		ENDSWITCH
	
	/*//GENERAL ANIMS
	ELSE
		IF IS_PLAYER_MALE(PLAYER_ID())
			IF GET_RANDOM_BOOL()
				RETURN "ANIM@MP_CORONA_IDLES@MALE_A@BASE"
			ELSE
				RETURN "ANIM@MP_CORONA_IDLES@MALE_B@BASE"
			ENDIF
		ELSE
			RETURN "ANIM@MP_CORONA_IDLES@FEMALE_A@BASE"
		ENDIF*/
	ENDIF
	
	PRINTLN("[CORONA] GET_CORONA_PED_ANIMATION_DICT - Unknown team to retrieve corona ped animation dictionary: ", iTeam, ", iTeamPosIndex: ", iTeamPosIndex)
	RETURN ""
ENDFUNC

/// PURPOSE: Populates the strings for the animations to be used based on the players team and their position within it
PROC GET_CORONA_PED_ANIMATION_INTRO_AND_LOOP(INT iTeam, INT iTeamPosIndex, TEXT_LABEL_31 &strAnimIntro, TEXT_LABEL_31 &strAnimLoop, FLOAT &fStartPhase)
	
	// If the player is in 4:3 resolution, change to team 0 formation
	IF NOT GET_IS_WIDESCREEN()
	AND iTeam = -1
		PRINTLN("[CORONA] GET_CORONA_PED_ANIMATION_INTRO_AND_LOOP - Called with invalid iMemberIndex for team -1. GET_IS_WIDESCREEN() = FALSE, override to 0")
		iTeam = 0
	ENDIF
	
	//HEIST ANIMS
	IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
	AND IS_CORONA_BIT_SET(CORONA_BESPOKE_PED_PLACEMENT)
		fStartPhase = 0.0
		
		SWITCH iTeam
		
			CASE -1
				
				SWITCH iTeamPosIndex
					CASE 0
						strAnimIntro 	= "Single_team_intro_boss"
						strAnimLoop		= "Single_team_loop_boss" 
					BREAK
					CASE 1
						strAnimIntro 	= "Single_team_intro_three"
						strAnimLoop		= "Single_team_loop_three"
					BREAK
					CASE 2
						strAnimIntro 	= "Single_team_intro_one"
						strAnimLoop		= "Single_team_loop_one"
					BREAK
					CASE 3
						strAnimIntro 	= "Single_team_intro_two"
						strAnimLoop		= "Single_team_loop_two"
					BREAK
					DEFAULT
						PRINTLN("[CORONA] GET_CORONA_PED_ANIMATION_INTRO_AND_LOOP - Unknown team position for ped when requesting animations: Team: ", iTeam, ", iPos: ", iTeamPosIndex)
						SCRIPT_ASSERT("GET_CORONA_PED_ANIMATION_INTRO_AND_LOOP - Unknown team position for ped in team -1 when requesting animations. See logs for details.")
					BREAK
				ENDSWITCH
				
			BREAK
			
			CASE 0
			CASE 1
			CASE 2
			CASE 3
				
				SWITCH iTeamPosIndex
					CASE 0
						strAnimIntro 	= ""
						strAnimLoop		= "idle" 
					BREAK
					CASE 1
						strAnimIntro 	= ""
						strAnimLoop		= "idle"
					BREAK
					CASE 2
						strAnimIntro 	= ""
						strAnimLoop		= "idle"
					BREAK
					CASE 3
						strAnimIntro 	= ""
						strAnimLoop		= "idle"
					BREAK
					DEFAULT
						PRINTLN("[CORONA] GET_CORONA_PED_ANIMATION_INTRO_AND_LOOP - Unknown team position for ped when requesting animations: Team: ", iTeam, ", iPos: ", iTeamPosIndex)
						SCRIPT_ASSERT("GET_CORONA_PED_ANIMATION_INTRO_AND_LOOP - Unknown team position for ped in team 0 when requesting animations. See logs for details.")
					BREAK
				ENDSWITCH
				
			BREAK
		
			DEFAULT
				PRINTLN("[CORONA] GET_CORONA_PED_ANIMATION_INTRO_AND_LOOP - Unknown team for ped when requesting animations: ", iTeam)
				SCRIPT_ASSERT("GET_CORONA_PED_ANIMATION_INTRO_AND_LOOP - Unknown team for ped when requesting animations. See logs for details.")
			BREAK
		ENDSWITCH
	
	//GENERAL ANIMS
	/*ELSE
		fStartPhase = GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0)
		strAnimLoop = "BASE"*/
	ENDIF
ENDPROC

/// PURPOSE: Poll this every frame to see if the required animation dictionaries have loaded
FUNC BOOL REQUEST_CORONA_PED_ANIMATION_DICTS_FOR_TEAMS(INT iTeams)
	
	STRING strDict
	BOOL bHaveAllAnimDictsLoaded = TRUE
	
//	IF iTeams = 1
//	OR IS_CORONA_BIT_SET(CORONA_HEIST_SINGLE_TEAM_OUTFITS)
	strDict = GET_CORONA_PED_ANIMATION_DICT(-1, 0)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(strDict)
		REQUEST_ANIM_DICT(strDict)
		IF NOT HAS_ANIM_DICT_LOADED(strDict)
			PRINTLN("REQUEST_CORONA_PED_ANIMATION_DICTS_FOR_TEAMS - Not loaded, -1")
			bHaveAllAnimDictsLoaded = FALSE
		ENDIF
	ENDIF
//	ELSE
		
	INT i, j
	FOR i = 0 TO (iTeams-1) STEP 1
		
		FOR j = 0 TO 3 STEP 1
			strDict = GET_CORONA_PED_ANIMATION_DICT(i, j)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(strDict)
				REQUEST_ANIM_DICT(strDict)
				IF NOT HAS_ANIM_DICT_LOADED(strDict)
					PRINTLN("REQUEST_CORONA_PED_ANIMATION_DICTS_FOR_TEAMS - Not loaded, ", i)
					bHaveAllAnimDictsLoaded = FALSE
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
//	ENDIF
	
	RETURN bHaveAllAnimDictsLoaded
ENDFUNC

/// PURPOSE:
///    Free up our dictionaries from memory
PROC CLEAR_ALL_CORONA_PED_ANIMATION_DICTS()
	
	PRINTLN("[CORONA] CLEAR_ALL_CORONA_PED_ANIMATION_DICTS - Removing all animation dictionaries for bespoke corona")
	
	INT i, j
	STRING strDict
	
	FOR i = -1 TO (FMMC_MAX_TEAMS-1) STEP 1
		
		FOR j = 0 TO 3 STEP 1
			strDict = GET_CORONA_PED_ANIMATION_DICT(i, j)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(strDict)
				REMOVE_ANIM_DICT(strDict)
			ENDIF
		ENDFOR
	ENDFOR
	
ENDPROC

/// PURPOSE:
///    Populates our global struct with the corona placement offsets for team and normal coronas
PROC INIT_CORONA_REPOSITION_VECTORS()
	
	IF IS_THIS_A_TEAM_CORONA(g_sTransitionSessionOptions)
		
		// Team corona positions
		g_vCoronaPedPositions[0] = << 0.5605, 0.6750, 0.0000 >>
		g_vCoronaPedPositions[1] = << -0.650, 0.6150, 0.0000 >>
		g_vCoronaPedPositions[2] = << -1.1600, 1.170, 0.0000 >>
		g_vCoronaPedPositions[3] = << -0.4200, 1.5000, 0.0000 >>
		g_vCoronaPedPositions[4] = << 1.2648, 1.2600, 0.0000 >>
		g_vCoronaPedPositions[5] = << -1.9500, 1.3040, 0.0000 >>
		g_vCoronaPedPositions[6] = << -1.8733, 1.8855, 0.0000 >>
		g_vCoronaPedPositions[7] = << 1.2500, 2.0500, 0.0000 >>
		g_vCoronaPedPositions[8] = << -1.3807, 2.4120, 0.0000 >>
		g_vCoronaPedPositions[9] = << 0.8185, 2.4600, 0.0000 >>
		g_vCoronaPedPositions[10] = << 2.1793, 2.4177, 0.0000 >>
		g_vCoronaPedPositions[11] = << -0.8138, 3.0898, 0.0000 >>
		g_vCoronaPedPositions[12] = << -2.1553, 3.0000, 0.0000 >>
		g_vCoronaPedPositions[13] = << 2.1000, 1.7200, 0.0000 >>
		g_vCoronaPedPositions[14] = << 1.9000, 3.2000, 0.0000 >>
		
		// Player headings
		g_fCoronaPedHeadings[0] = -27.7200
		g_fCoronaPedHeadings[1] = 13.0000
		g_fCoronaPedHeadings[2] = 24.1200
		g_fCoronaPedHeadings[3] = 0.0000		
		g_fCoronaPedHeadings[4] = -27.7200
		g_fCoronaPedHeadings[5] = 30.9600
		g_fCoronaPedHeadings[6] = 24.2010
		g_fCoronaPedHeadings[7] = -22.6800		
		g_fCoronaPedHeadings[8] = 24.1200
		g_fCoronaPedHeadings[9] = -22.6800
		g_fCoronaPedHeadings[10] = -32.7600
		g_fCoronaPedHeadings[11] = 0.0000
		g_fCoronaPedHeadings[12] = 14.4000
		g_fCoronaPedHeadings[13] = -36.0000
		g_fCoronaPedHeadings[14] = -19.0800
		
	ELSE
		g_vCoronaPedPositions[0] = << -0.200, -0.040, 0.427 >>						//<< -0.365112, -0.33493, 0.426865 >>
		g_vCoronaPedPositions[1] = << 0.317, -3.133, 0.310 >> 						//<< 0.317, -3.382, 0.310 >> 					
		g_vCoronaPedPositions[2] = << 1.958, -1.841, 0.425 >>						//<< 1.95779, -1.87537, 0.424683 >>
		g_vCoronaPedPositions[3] = << -1.031, 1.449, 0.425 >> 						//<< -1.08072, 1.6731, 0.424713 >>
		g_vCoronaPedPositions[4] = << -0.041, 1.596, 0.425 >>						//<< 0.444977, 2.02411, 0.42543 >>
		g_vCoronaPedPositions[5] = << -1.167, 0.297, 0.452 >> 						//<< -1.43585, 0.355286, 0.452034 >>
		g_vCoronaPedPositions[6] = << 0.220, -1.387, 0.427 >>						//<< 0.621124, -1.22824, 0.426727 >>
		g_vCoronaPedPositions[7] = << 2.102, 0.977, 0.426 >>						//<< 2.44083, 0.996887, 0.425735 >>
		g_vCoronaPedPositions[8] = << -0.217, -2.192, 0.427 >>						//<< -0.284241, -2.53958, 0.427032 >>
		g_vCoronaPedPositions[9] = << 0.422, 0.698, 0.425 >>						//<< 0.481506, 0.729065, 0.42495 >>
		g_vCoronaPedPositions[10] = << 2.480, -1.055, 0.425 >> 						//<< 2.93509, -0.906342, 0.42498 >>
		g_vCoronaPedPositions[11] = << 1.664, -0.040, 0.425 >>						//<< 1.66418, -0.163544, 0.425056 >>
		g_vCoronaPedPositions[12] = << 1.275, 1.974, 0.426 >>						//<< 1.7532, 2.47116, 0.42614 >>
		g_vCoronaPedPositions[13] = << 1.214, -3.042, 0.425 >>						//<< 0.973297, -2.87323, 0.424683 >>
		g_vCoronaPedPositions[14] = <<-1.161, -1.579, 0.451>> 						//<< -1.17847, -1.67871, 0.4505 >>
	ENDIF
	
ENDPROC


