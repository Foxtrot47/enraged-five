USING "FMMC_Vars.sch"
USING "net_car_rockets.sch"
USING "net_car_boost.sch"
USING "FMMC_Header.sch"

#IF IS_DEBUG_BUILD
FUNC STRING GET_STATE_NAME(FMMC_POWERUP_STRUCT &sPowerUp)
	SWITCH sPowerUp.ePowerUpState
		CASE POWERUP_STATE_NONE 			RETURN "POWERUP_STATE_NONE"
		CASE POWERUP_STATE_INIT 			RETURN "POWERUP_STATE_INIT"
		CASE POWERUP_STATE_IDLE 			RETURN "POWERUP_STATE_IDLE"
		CASE POWERUP_STATE_RUNNING 			RETURN "POWERUP_STATE_RUNNING"
		CASE POWERUP_STATE_CLEANUP_EFFECT 	RETURN "POWERUP_STATE_CLEANUP_EFFECT"
		CASE POWERUP_STATE_CLEANUP 			RETURN "POWERUP_STATE_CLEANUP"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_POWERUP_NAME(FMMC_POWERUP_TYPE ePowerUpType)
	SWITCH ePowerUpType
		CASE POWERUP_NONE			RETURN "POWERUP_NONE"
		CASE POWERUP_BOOST_SHUNT	RETURN "POWERUP_BOOST_SHUNT"
		CASE POWERUP_BOMB			RETURN "POWERUP_BOMB"
		CASE POWERUP_REPAIR			RETURN "POWERUP_REPAIR"
		CASE POWERUP_GHOST			RETURN "POWERUP_GHOST"
		CASE POWERUP_ROCKET			RETURN "POWERUP_ROCKET"
		CASE POWERUP_MINE_KINETIC	RETURN "POWERUP_MINE_KINETIC"
		CASE POWERUP_MINE_SPIKE		RETURN "POWERUP_MINE_SPIKE"
		CASE POWERUP_MINE_EMP		RETURN "POWERUP_MINE_EMP"
		CASE POWERUP_MINE_SLICK		RETURN "POWERUP_MINE_SLICK"
		CASE POWERUP_TANK			RETURN "POWERUP_TANK"
		CASE POWERUP_MG_AMP			RETURN "POWERUP_MG_AMP"
		CASE POWERUP_MONSTER_TRUCK	RETURN "POWERUP_MONSTER_TRUCK"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC
#ENDIF

FUNC FMMC_POWERUP_TYPE GET_CURRENT_POWERUP(FMMC_POWERUP_STRUCT &sPowerUp)
	IF sPowerUp.iCurrentPowerUpIndex = -1
		RETURN POWERUP_NONE
	ENDIF
	
	RETURN sPowerUp.eCurrentPowerUps[sPowerUp.iCurrentPowerUpIndex]
ENDFUNC

FUNC BOOL DOES_LOCAL_PLAYER_HAVE_ANY_POWERUPS(FMMC_POWERUP_STRUCT &sPowerUp)
	RETURN sPowerUp.iCurrentPowerUpsCount > 0
ENDFUNC

FUNC BOOL IS_POWERUP_SLOT_FILLED(FMMC_POWERUP_STRUCT &sPowerUp)
	RETURN GET_CURRENT_POWERUP(sPowerUp) != POWERUP_NONE
ENDFUNC

PROC SET_CURRENT_POWERUP_INDEX(FMMC_POWERUP_STRUCT &sPowerUp, INT iNewIndex)
	sPowerUp.iCurrentPowerUpIndex = iNewIndex
	PRINTLN("[Power Ups] SET_CURRENT_POWERUP_INDEX - iCurrentPowerUpIndex is now: ", sPowerUp.iCurrentPowerUpIndex)
ENDPROC

FUNC BOOL HAS_CURRENT_POWERUP_BEEN_TRIGGERED(FMMC_POWERUP_STRUCT &sPowerUp)
	RETURN IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPOWERUP_BS_Triggered)
ENDFUNC

PROC SET_CURRENT_POWERUP_HAS_BEEN_TRIGGERED(FMMC_POWERUP_STRUCT &sPowerUp)
	SET_BIT(sPowerUp.iPowerUpBitSet, ciPOWERUP_BS_Triggered)
	PRINTLN("[Power Ups] SET_CURRENT_POWERUP_HAS_BEEN_TRIGGERED - Called!")
ENDPROC

PROC CLEAR_CURRENT_POWERUP_HAS_BEEN_TRIGGERED(FMMC_POWERUP_STRUCT &sPowerUp)
	CLEAR_BIT(sPowerUp.iPowerUpBitSet, ciPOWERUP_BS_Triggered)
	PRINTLN("[Power Ups] CLEAR_CURRENT_POWERUP_HAS_BEEN_TRIGGERED - Cleared!")
ENDPROC

PROC SET_POWERUP_STATE(FMMC_POWERUP_STRUCT &sPowerUp, FMMC_POWERUP_STATE eNewPowerUpState)
	PRINTLN("[Power Ups] SET_POWERUP_STATE - Changing state from ", sPowerUp.ePowerUpState," to: ", eNewPowerUpState)
	sPowerUp.ePowerUpState = eNewPowerUpState
ENDPROC

FUNC BOOL HAS_CURRENT_POWERUP_TIME_EXPIRED(FMMC_POWERUP_STRUCT &sPowerUp)
	RETURN HAS_NET_TIMER_EXPIRED_READ_ONLY(sPowerUp.tdPowerUpTimer, sPowerUp.iPowerUpDuration)
ENDFUNC

FUNC BOOL HAS_CURRENT_POWERUP_TIME_REACHED_PERCENTAGE(FMMC_POWERUP_STRUCT &sPowerUp, INT iPercentage)
	INT iPercentageTime = (sPowerUp.iPowerUpDuration / 100) * iPercentage
	RETURN HAS_NET_TIMER_STARTED_AND_EXPIRED(sPowerUp.tdPowerUpTimer, iPercentageTime)
ENDFUNC

FUNC FMMC_POWERUP_HUD_STATE GET_POWERUP_HUD_STATE(FMMC_POWERUP_STRUCT &sPowerUp)
	RETURN sPowerUp.eHUDState
ENDFUNC

PROC SET_POWERUP_HUD_STATE(FMMC_POWERUP_STRUCT &sPowerUp, FMMC_POWERUP_HUD_STATE eNewState)
	PRINTLN("[Power Ups] SET_POWERUP_HUD_STATE - Setting Power Up HUD state from ", sPowerUp.eHUDState, " to ", eNewState)
	sPowerUp.eHUDState = eNewState
ENDPROC

FUNC INT GET_MAX_NUMBER_OF_POWERUPS()
	
	RETURN g_FMMC_STRUCT.sPowerupData.iPWRMaxHeld
	
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_COLLECT_MORE_POWERUPS(FMMC_POWERUP_STRUCT &sPowerUp)
	RETURN sPowerUp.iCurrentPowerUpsCount < GET_MAX_NUMBER_OF_POWERUPS()
ENDFUNC

PROC ENABLE_POWERUP_MINES(BOOL bEnable)
	IF bEnable
		IF !g_bEnablePowerUpMines
			g_bEnablePowerUpMines = TRUE
		
			#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[Power Ups] ENABLE_POWERUP_MINES TRUE")
			#ENDIF
		ENDIF	
	ELSE
		IF g_bEnablePowerUpMines
			g_bEnablePowerUpMines = FALSE
		
			#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[Power Ups] ENABLE_POWERUP_MINES FALSE")
			#ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC SET_POWERUP_MINE_TYPE(INT iNewType)
	IF g_iPowerUpMineType != iNewType
		PRINTLN("[Power Ups] SET_POWERUP_MINE_TYPE - Setting g_iPowerUpMineType from ", g_iPowerUpMineType, " to ", iNewType)
		g_iPowerUpMineType = iNewType
	ENDIF
ENDPROC

FUNC BOOL IS_CURRENT_POWERUP_A_MINE(FMMC_POWERUP_STRUCT &sPowerUp)
	SWITCH GET_CURRENT_POWERUP(sPowerUp)
		CASE POWERUP_MINE_KINETIC
		CASE POWERUP_MINE_SPIKE
		CASE POWERUP_MINE_EMP
		CASE POWERUP_MINE_SLICK
		CASE POWERUP_BOMB
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CURRENT_POWERUP_A_TRANSFORM(FMMC_POWERUP_STRUCT &sPowerUp)
	SWITCH GET_CURRENT_POWERUP(sPowerUp)
		CASE POWERUP_TANK
		CASE POWERUP_MONSTER_TRUCK
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Audio
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL LOAD_POWERUPS_AUDIO()
	RETURN REQUEST_SCRIPT_AUDIO_BANK("DLC_TUNER/DLC_Tuner_Adversary_GoKart_Battle")
ENDFUNC

PROC UNLOAD_POWERUPS_AUDIO()
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_TUNER/DLC_Tuner_Adversary_GoKart_Battle")
ENDPROC

FUNC STRING GET_POWERUP_APPEND_FOR_TYPE(FMMC_POWERUP_TYPE ePowerUpType)
	
	SWITCH ePowerUpType
		CASE POWERUP_BOOST_SHUNT 	RETURN "Shunt"
		CASE POWERUP_BOMB 			RETURN "Bomb"
		CASE POWERUP_REPAIR 		RETURN "Repair"
		CASE POWERUP_GHOST 			RETURN ""
		CASE POWERUP_ROCKET 		RETURN "Rockets"
		CASE POWERUP_MINE_KINETIC 	RETURN "KineticMine"
		CASE POWERUP_MINE_SPIKE 	RETURN ""
		CASE POWERUP_MINE_EMP 		RETURN "EMPMine"
		CASE POWERUP_MINE_SLICK 	RETURN "OilMine"
		CASE POWERUP_TANK 			RETURN "Tank"
		CASE POWERUP_MG_AMP 		RETURN "Amped"
		CASE POWERUP_MONSTER_TRUCK 	RETURN "Truck"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC PLAY_POWERUPS_SOUND_FRONTEND(FMMC_POWERUP_TYPE ePowerUpType, STRING sSoundName, BOOL bAppend = TRUE)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63 = sSoundName
	IF bAppend
		tl63 += GET_POWERUP_APPEND_FOR_TYPE(ePowerUpType)
	ENDIF
	
	PLAY_SOUND_FRONTEND(-1, tl63, "Go_Kart_Death_Match_Soundset")
ENDPROC

PROC PLAY_POWERUPS_COLLECTION_SOUND(FMMC_POWERUP_TYPE ePowerUpType)
	PLAY_POWERUPS_SOUND_FRONTEND(ePowerUpType, "Pickup_Powerup_FE_")
ENDPROC

PROC PLAY_POWERUPS_USED_SOUND(FMMC_POWERUP_TYPE ePowerUpType)
	PLAY_POWERUPS_SOUND_FRONTEND(ePowerUpType, "Use_Powerup_")
ENDPROC

PROC PLAY_POWERUPS_END_SOUND(FMMC_POWERUP_TYPE ePowerUpType)
	PLAY_POWERUPS_SOUND_FRONTEND(ePowerUpType, "Powerup_Depleted_")
ENDPROC

PROC PLAY_POWERUPS_SOUND_FROM_PICKUP(PICKUP_INDEX puPickup, STRING sSoundName)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
		EXIT
	ENDIF
	
	IF DOES_PICKUP_EXIST(puPickup)
	AND DOES_PICKUP_OBJECT_EXIST(puPickup)
		PLAY_SOUND_FROM_ENTITY(-1, sSoundName, GET_PICKUP_OBJECT(puPickup), "Go_Kart_Death_Match_Soundset")
	ENDIF
	
ENDPROC

PROC PROCESS_POWERUP_PICKUP_SOUND_LOOP(FMMC_POWERUP_STRUCT &sPowerUp, PICKUP_INDEX puPickup, INT iPickupIndex)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
		EXIT
	ENDIF
	
	IF DOES_PICKUP_EXIST(puPickup)
		IF DOES_PICKUP_OBJECT_EXIST(puPickup)
			OBJECT_INDEX oiPickup = GET_PICKUP_OBJECT(puPickup)
			IF DOES_ENTITY_EXIST(oiPickup)
			AND sPowerUp.iPowerupPickupSoundLoop[iPickupIndex] = -1
				sPowerUp.iPowerupPickupSoundLoop[iPickupIndex] = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(sPowerUp.iPowerupPickupSoundLoop[iPickupIndex], "Powerup_Block_Loop", oiPickup, "Go_Kart_Death_Match_Soundset")
				PRINTLN("PROCESS_POWERUP_PICKUP_SOUND_LOOP - Starting loop sound for pickup ", iPickupIndex)
			ENDIF
		ELSE
			IF sPowerUp.iPowerupPickupSoundLoop[iPickupIndex] != -1
				STOP_SOUND(sPowerUp.iPowerupPickupSoundLoop[iPickupIndex])
				RELEASE_SOUND_ID(sPowerUp.iPowerupPickupSoundLoop[iPickupIndex])
				sPowerUp.iPowerupPickupSoundLoop[iPickupIndex] = -1
				PRINTLN("PROCESS_POWERUP_PICKUP_SOUND_LOOP - Stopping loop sound for pickup ", iPickupIndex)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PLAY_POWERUPS_SWITCH_SOUND(BOOL bSuccessful)
	IF bSuccessful
		PLAY_POWERUPS_SOUND_FRONTEND(POWERUP_NONE, "FE_Switch_Powerup", FALSE)
	ELSE
		PLAY_POWERUPS_SOUND_FRONTEND(POWERUP_NONE, "FE_Switch_Powerup_Unavailable", FALSE)
	ENDIF
ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Init
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC INIT_POWERUP_SYSTEM(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
		EXIT
	ENDIF
	
	PRINTLN("[Power Ups] INIT_POWERUP_SYSTEM")
	
	FMMC_POWERUP_STRUCT sEmptyStruct
	sPowerUp = sEmptyStruct
	
	INT i
	FOR i = 0 TO GET_MAX_NUMBER_OF_POWERUPS()-1
		sPowerUp.eCurrentPowerUps[i] = POWERUP_NONE
	ENDFOR
	
	b_gAllowZForCarRockets = TRUE
	
	SET_PARTICLE_FX_OVERRIDE("exp_xs_mine_slick", "scr_tn_exp_mine_slick_nodecal")
	SET_WEAPON_EFFECT_DURATION_MODIFIER(WEAPONTYPE_DLC_VEHICLE_MINE_SLICK, 0.0)
	
	INITIALISE_INT_ARRAY(sPowerUp.iPowerupPickupSoundLoop, -1)
ENDPROC

FUNC BOOL HAVE_POWERUP_ASSETS_LOADED()
	REQUEST_NAMED_PTFX_ASSET("veh_xs_vehicle_mods")
	REQUEST_NAMED_PTFX_ASSET("scr_tn_slick")
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("veh_xs_vehicle_mods")
	OR NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_tn_slick")
		PRINTLN("[Power Ups] HAVE_POWERUP_ASSETS_LOADED - Waiting on mine ptfx veh_xs_vehicle_mods")
		RETURN FALSE
	ENDIF
	
	IF NOT LOAD_POWERUPS_AUDIO()
		PRINTLN("[Power Ups] HAVE_POWERUP_ASSETS_LOADED - Waiting audio bank to load")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Power Up Slots
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC FILL_EMPTY_POWERUP_SLOT(FMMC_POWERUP_STRUCT &sPowerUp, FMMC_POWERUP_TYPE eNewPowerUp)
	INT i
	FOR i = 0 TO GET_MAX_NUMBER_OF_POWERUPS()-1
		IF sPowerUp.eCurrentPowerUps[i] = POWERUP_NONE
		AND eNewPowerUp != POWERUP_NONE
			
			sPowerUp.eCurrentPowerUps[i] = eNewPowerUp
			sPowerUp.ePowerUpJustCollected = eNewPowerUp
			sPowerUp.ePowerUpLastCollected = eNewPowerUp
			sPowerUp.iCurrentPowerUpsCount++
			PRINTLN("[Power Ups] FILL_EMPTY_POWERUP_SLOT - Power Up index ", i, " filled with ", GET_POWERUP_NAME(sPowerUp.eCurrentPowerUps[i]), ". Power Up count: ", sPowerUp.iCurrentPowerUpsCount)
			BREAKLOOP
		ENDIF
	ENDFOR
	
ENDPROC

FUNC INT FIND_NEXT_FILLED_POWERUP_SLOT(FMMC_POWERUP_STRUCT &sPowerUp)
	INT i
	INT iNextFilled = -1
	INT iHighestSlot = -1
	
	FOR i = 0 TO GET_MAX_NUMBER_OF_POWERUPS()-1
		IF i != -1
		AND sPowerUp.eCurrentPowerUps[i] != POWERUP_NONE
		AND i > iHighestSlot
		AND i != sPowerUp.iCurrentPowerUpIndex
			iHighestSlot = i
			iNextFilled = i
		ENDIF
	ENDFOR
	
	RETURN iNextFilled
ENDFUNC

FUNC INT GET_RANDOM_POWERUP_TO_GIVE()
	INT iRandomPowerUp
	
	iRandomPowerUp = ROUND(TO_FLOAT(GET_RANDOM_INT_IN_RANGE(0, 1001))/10.0)
	
	PRINTLN("[Power Ups] GET_RANDOM_POWERUP_TO_GIVE - Random INT: ", iRandomPowerUp)
	INT iPickupType, iTotalChance
	FOR iPickupType = 0 TO FMMC_POWERUP_TYPES_MAX-1
		iTotalChance += g_FMMC_STRUCT.sPowerupData.iPWRRandomChance[iPickupType]
		PRINTLN("[Power Ups] GET_RANDOM_POWERUP_TO_GIVE - iTotalChance: ", iTotalChance, " just added ", g_FMMC_STRUCT.sPowerupData.iPWRRandomChance[iPickupType], " on")
		IF iTotalChance >= iRandomPowerUp
			RETURN iPickupType
		ENDIF
	ENDFOR
	
	PRINTLN("[Power Ups] GET_RANDOM_POWERUP_TO_GIVE - Returning POWERUP_NONE as random result was invalid iTotalChance: ", iTotalChance, " iRandomPowerUp: ", iRandomPowerUp)
	SCRIPT_ASSERT("[Power Ups] GET_RANDOM_POWERUP_TO_GIVE - Returning POWERUP_NONE as random result was invalid")
	RETURN ENUM_TO_INT(POWERUP_NONE)
ENDFUNC

PROC PROCESS_CAN_LOCAL_PLAYER_COLLECT_PICKUPS(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF IS_PED_IN_ANY_VEHICLE(sPowerUp.LocalPlayerPed)
		IF CAN_LOCAL_PLAYER_COLLECT_MORE_POWERUPS(sPowerUp)
			SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), PICKUP_VEHICLE_CUSTOM_SCRIPT, TRUE)
		ELSE
			SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), PICKUP_VEHICLE_CUSTOM_SCRIPT, FALSE)
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Power Up Cleanup
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
PROC CLEANUP_CURRENT_POWERUP_INDEX(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF sPowerUp.iCurrentPowerUpIndex != -1
		sPowerUp.eCurrentPowerUps[sPowerUp.iCurrentPowerUpIndex] = POWERUP_NONE
	ENDIF
	
	SET_CURRENT_POWERUP_INDEX(sPowerUp, FIND_NEXT_FILLED_POWERUP_SLOT(sPowerUp))
	PRINTLN("[Power Ups] CLEANUP_CURRENT_POWERUP_INDEX - sPowerUp.iCurrentPowerUpIndex is now: ", sPowerUp.iCurrentPowerUpIndex)
	
ENDPROC

PROC CLEANUP_CURRENT_POWERUP_DATA(FMMC_POWERUP_STRUCT &sPowerUp)
	
	PRINTLN("[Power Ups] CLEANUP_CURRENT_POWERUP_DATA")
	
	CLEANUP_CURRENT_POWERUP_INDEX(sPowerUp)
	
	RESET_NET_TIMER(sPowerUp.tdPowerUpTimer)
	sPowerUp.iPowerUpDuration = 0
	
	RESET_NET_TIMER(sPowerUp.tdPowerUpMinimumTimer)
	
	sPowerUp.iCurrentPowerUpsCount--
	
	sPowerUp.iPowerUpBitSet = 0
	
ENDPROC

PROC CLEANUP_CURRENT_POWERUP_EFFECT(FMMC_POWERUP_STRUCT &sPowerUp, BOOL bChangePowerOnly)
	
	STRING sPTFXAsset
	
	SWITCH GET_CURRENT_POWERUP(sPowerUp)
		CASE POWERUP_BOOST_SHUNT
			PRINTLN("[Power Ups] CLEANUP_CURRENT_POWERUP_EFFECT - Cleaning up POWERUP_BOOST_SHUNT")
			CLEAR_VEHICLE_BOOST_COLLECTED()
		BREAK
		CASE POWERUP_REPAIR
			sPTFXAsset = "scr_sum_ow"
			REMOVE_NAMED_PTFX_ASSET(sPTFXAsset)
			
			PRINTLN("[Power Ups] CLEANUP_CURRENT_POWERUP_EFFECT - Cleaning up POWERUP_REPAIR")
		BREAK
		CASE POWERUP_GHOST
			IF IS_ENTITY_A_GHOST(sPowerUp.LocalPlayerPed)
				SET_LOCAL_PLAYER_AS_GHOST(FALSE, TRUE)	
				RESET_GHOST_ALPHA()
				PRINTLN("[Power Ups] CLEANUP_CURRENT_POWERUP_EFFECT - Cleaning up POWERUP_GHOST")
			ENDIF
		BREAK
		CASE POWERUP_ROCKET
			PRINTLN("[Power Ups] CLEANUP_CURRENT_POWERUP_EFFECT - Cleaning up POWERUP_ROCKET")
			IF bChangePowerOnly
				REMOVE_ROCKETS()
			ENDIF
		BREAK
		CASE POWERUP_TANK
		CASE POWERUP_MONSTER_TRUCK
			sPTFXAsset = "scr_as_trans"
			REMOVE_NAMED_PTFX_ASSET(sPTFXAsset)
			SET_MODEL_AS_NO_LONGER_NEEDED(sPowerUp.mnModelToSwapTo)
			sPowerUp.mnPreSwapVeh = DUMMY_MODEL_FOR_SCRIPT
			g_bBlockFMMCVehMachineGuns = FALSE
			SET_PLAYER_USING_RC_TANK(FALSE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
			SET_ENTITY_VISIBLE(sPowerUp.LocalPlayerPed, TRUE)
			SET_LOCAL_PLAYER_AS_GHOST(FALSE, FALSE)
			SET_ENTITY_ALPHA(sPowerUp.LocalPlayerPed, 255, FALSE)
			PRINTLN("[Power Ups] CLEANUP_CURRENT_POWERUP_EFFECT - Cleaning up Transform powerup")
		BREAK
		CASE POWERUP_MINE_KINETIC
		CASE POWERUP_MINE_SPIKE
		CASE POWERUP_MINE_EMP
		CASE POWERUP_MINE_SLICK
		CASE POWERUP_BOMB
			IF NOT IS_VEHICLE_FUCKED_MP(sPowerUp.viCurrentVeh)
				SET_VEHICLE_MOD(sPowerUp.viCurrentVeh, MOD_WING_R, -1)
				SET_VEHICLE_BOMB_AMMO(sPowerUp.viCurrentVeh, 0)
			ENDIF
			g_bPowerUpMineUsed = FALSE
			PRINTLN("[VEHICLE_MINES][Power Ups] CLEANUP_CURRENT_POWERUP_EFFECT - g_bPowerUpMineUsed = FALSE")
			DISABLE_VEHICLE_MINES(TRUE)
			ENABLE_POWERUP_MINES(FALSE)
			SET_POWERUP_MINE_TYPE(-1)
			PRINTLN("[Power Ups] CLEANUP_CURRENT_POWERUP_EFFECT - Cleaning up Mine powerup")
		BREAK
		CASE POWERUP_MG_AMP
			REMOVE_NAMED_PTFX_ASSET("scr_bike_adversary")
			g_bFMMCVehMachineGuns_Amped = FALSE
			g_fFMMCVehMachineGuns_DmgMultiplier = 1.0
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sPowerUp.ptfxAmped)
				STOP_PARTICLE_FX_LOOPED(sPowerUp.ptfxAmped)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Events
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_NEW_POWERUP_TYPE_INVALID(FMMC_POWERUP_STRUCT &sPowerUp, FMMC_POWERUP_TYPE ePowerUpType)
	
	IF sPowerUp.ePowerUpLastCollected = ePowerUpType
		PRINTLN("[Power Ups] IS_NEW_POWERUP_TYPE_INVALID - Matches last powerup collected!")
		RETURN TRUE
	ENDIF
	
	INT iSlot
	FOR iSlot = 0 TO GET_MAX_NUMBER_OF_POWERUPS()-1
		IF sPowerup.eCurrentPowerUps[iSlot] = ePowerUpType
			PRINTLN("[Power Ups] IS_NEW_POWERUP_TYPE_INVALID - Already have pickup of this type!")
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_POWERUP_PICKUP_EVENT(FMMC_POWERUP_STRUCT &sPowerUp, INT iPickup, BOOL bIsLocalPlayer)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iVehicleWeaponPickupType != -1
		SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iVehicleWeaponPickupType
			CASE ciVEH_WEP_RANDOM
				IF bIsLocalPlayer
					FMMC_POWERUP_TYPE ePowerUpToGive 
					ePowerUpToGive = INT_TO_ENUM(FMMC_POWERUP_TYPE, GET_RANDOM_POWERUP_TO_GIVE())
					WHILE IS_NEW_POWERUP_TYPE_INVALID(sPowerUp, ePowerUpToGive)
						ePowerUpToGive = INT_TO_ENUM(FMMC_POWERUP_TYPE, GET_RANDOM_POWERUP_TO_GIVE())
					ENDWHILE
					FILL_EMPTY_POWERUP_SLOT(sPowerUp, ePowerUpToGive)
					
					PLAY_POWERUPS_COLLECTION_SOUND(ePowerUpToGive)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC
   
STRUCT SCRIPT_EVENT_DATA_FMMC_POWERUP_SOUND
	STRUCT_EVENT_COMMON_DETAILS Details
	TEXT_LABEL_63 tl63Sound
ENDSTRUCT

PROC BROADCAST_FMMC_POWERUP_SOUND(STRING sSound, FMMC_POWERUP_TYPE ePowerUpType)
	SCRIPT_EVENT_DATA_FMMC_POWERUP_SOUND Event
	
	Event.Details.Type = SCRIPT_EVENT_FMMC_POWERUP_SOUND
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	TEXT_LABEL_63 tl63 = sSound
	tl63 += GET_POWERUP_APPEND_FOR_TYPE(ePowerUpType)
	Event.tl63Sound = tl63
	
	NET_PRINT("-----------------BROADCAST_FMMC_POWERUP_SOUND---------------------------") NET_NL()
	
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // send it to all players!
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_FMMC_ - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
ENDPROC		

PROC PROCESS_SCRIPT_EVENT_FMMC_POWERUP_SOUND(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_POWERUP_SOUND EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	OR EventData.Details.Type != SCRIPT_EVENT_FMMC_POWERUP_SOUND
		EXIT
	ENDIF
	
	PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_POWERUP_SOUND - Received")
	
	IF EventData.details.FromPlayerIndex = PLAYER_ID()
		PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_POWERUP_SOUND - Exiting - From local")
		EXIT
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(EventData.tl63Sound)
		PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_POWERUP_SOUND - Exiting - No sound name")
		EXIT
	ENDIF
	
	PED_INDEX piPedIndex = GET_PLAYER_PED(EventData.details.FromPlayerIndex)
	
	IF NOT IS_ENTITY_ALIVE(piPedIndex)
		PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_POWERUP_SOUND - Exiting - Invalid Ped")
		EXIT
	ENDIF
	
	PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_POWERUP_SOUND - Playing ", EventData.tl63Sound, " from remote player")
	PLAY_SOUND_FROM_ENTITY(-1, EventData.tl63Sound, piPedIndex, "Go_Kart_Death_Match_Soundset")
	
ENDPROC
					
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Init State
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CLEAR_POWERUP_INIT_DATA(FMMC_POWERUP_STRUCT &sPowerUp)

	sPowerUp.iPowerUpDuration = 0
	
	CLEAR_BIT(sPowerUp.iPowerUpBitSet, ciPOWERUP_BS_InstantTrigger)
	CLEAR_BIT(sPowerUp.iPowerUpBitSet, ciPowerUP_BS_Load_Required)
ENDPROC

PROC INIT_CURRENT_POWERUP(FMMC_POWERUP_STRUCT &sPowerUp)
	
	CLEAR_POWERUP_INIT_DATA(sPowerUp)
	
	SWITCH GET_CURRENT_POWERUP(sPowerUp)
		CASE POWERUP_BOOST_SHUNT
			sPowerUp.iPowerUpDuration = ciVEHICLE_BOOST_DURATION
			VEHICLE_BOOST_COLLECTED()
		BREAK
		CASE POWERUP_REPAIR
			SET_BIT(sPowerUp.iPowerUpBitSet, ciPowerUP_BS_Load_Required)
		BREAK
		CASE POWERUP_GHOST
			sPowerUp.iPowerUpDuration = 10000
		BREAK
		CASE POWERUP_ROCKET
			sPowerUp.iPowerUpDuration = ciFMMC_POWERUP_IGNORE_DURATION
			SET_BIT(sPowerUp.iPowerUpBitSet, ciPowerUP_BS_Load_Required)
		BREAK
		CASE POWERUP_TANK
		CASE POWERUP_MONSTER_TRUCK
			SET_BIT(sPowerUp.iPowerUpBitSet, ciPowerUP_BS_Load_Required)
			sPowerUp.iPowerUpDuration = 15000
			sPowerUp.sVehicleSwap.iGhostTime = 250
			sPowerUp.sVehicleSwap.iPTFXTime = 1500
		BREAK
		CASE POWERUP_MINE_KINETIC
		CASE POWERUP_MINE_SPIKE
		CASE POWERUP_MINE_EMP
		CASE POWERUP_MINE_SLICK
		CASE POWERUP_BOMB
			sPowerUp.iPowerUpDuration = ciFMMC_POWERUP_IGNORE_DURATION
			SET_BIT(sPowerUp.iPowerUpBitSet, ciPowerUP_BS_Load_Required)
		BREAK
		CASE POWERUP_MG_AMP
			sPowerUp.iPowerUpDuration = 10000
			SET_BIT(sPowerUp.iPowerUpBitSet, ciPowerUP_BS_Load_Required)
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC INT GET_MINE_POWERUP_MOD_INDEX(FMMC_POWERUP_STRUCT &sPowerUp)
	SWITCH GET_CURRENT_POWERUP(sPowerUp)
		CASE POWERUP_MINE_KINETIC	RETURN 0
		CASE POWERUP_MINE_SPIKE		RETURN 1
		CASE POWERUP_MINE_EMP		RETURN 2
		CASE POWERUP_MINE_SLICK		RETURN 3
		CASE POWERUP_BOMB			RETURN 4
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC MODEL_NAMES GET_TRANSFORM_POWERUP_VEHICLE_MODEL(FMMC_POWERUP_STRUCT &sPowerUp)
	SWITCH GET_CURRENT_POWERUP(sPowerUp)
		CASE POWERUP_TANK 			RETURN MINITANK
		CASE POWERUP_MONSTER_TRUCK 	RETURN MONSTER3
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC BOOL IS_POWERUP_READY(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF NOT IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPowerUP_BS_Load_Required)
		RETURN TRUE
	ENDIF
	
	STRING sPTFXAsset
	
	SWITCH GET_CURRENT_POWERUP(sPowerUp)
		CASE POWERUP_REPAIR
			sPTFXAsset = "scr_sum_ow"
			REQUEST_NAMED_PTFX_ASSET(sPTFXAsset)
			IF HAS_NAMED_PTFX_ASSET_LOADED(sPTFXAsset)
				RETURN TRUE
			ENDIF
		BREAK
		CASE POWERUP_ROCKET
			REQUEST_WEAPON_ASSET(WEAPONTYPE_VEHICLE_ROCKET)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_RPG)
			IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_VEHICLE_ROCKET)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_RPG)
				ROCKETS_COLLECTED()
				RETURN TRUE
			ENDIF
		BREAK
		CASE POWERUP_TANK
		CASE POWERUP_MONSTER_TRUCK
			sPTFXAsset = "scr_as_trans"
			REQUEST_NAMED_PTFX_ASSET(sPTFXAsset)
			sPowerUp.mnModelToSwapTo = GET_TRANSFORM_POWERUP_VEHICLE_MODEL(sPowerUp)
			REQUEST_MODEL(sPowerUp.mnModelToSwapTo)
			PRELOAD_VEHICLE_AUDIO_BANK(ENUM_TO_INT(sPowerUp.mnModelToSwapTo))
			IF HAS_NAMED_PTFX_ASSET_LOADED(sPTFXAsset)
			AND HAS_MODEL_LOADED(sPowerUp.mnModelToSwapTo)
				RETURN TRUE
			ENDIF
		BREAK
		CASE POWERUP_MINE_KINETIC
		CASE POWERUP_MINE_SPIKE
		CASE POWERUP_MINE_EMP
		CASE POWERUP_MINE_SLICK
		CASE POWERUP_BOMB
			
			IF DOES_ENTITY_EXIST(sPowerUp.viCurrentVeh)
			AND NOT IS_VEHICLE_FUCKED_MP(sPowerUp.viCurrentVeh)
				SET_POWERUP_MINE_TYPE(GET_MINE_POWERUP_MOD_INDEX(sPowerUp))
				SET_VEHICLE_MOD_KIT(sPowerUp.viCurrentVeh, 0)
				PRELOAD_VEHICLE_MOD(sPowerUp.viCurrentVeh, MOD_WING_R, 0)
				SET_VEHICLE_MOD(sPowerUp.viCurrentVeh, MOD_WING_R, 0)
			ENDIF
			
			IF g_iPowerUpMineType != -1
			AND NOT IS_VEHICLE_FUCKED_MP(sPowerUp.viCurrentVeh)
			AND GET_VEHICLE_MOD(sPowerUp.viCurrentVeh, MOD_WING_R) != -1
				DISABLE_VEHICLE_MINES(FALSE)
				ENABLE_POWERUP_MINES(TRUE)
				SET_VEHICLE_BOMB_AMMO(sPowerUp.viCurrentVeh, 1)
				RETURN TRUE
			ENDIF
		BREAK
		CASE POWERUP_MG_AMP

			REQUEST_NAMED_PTFX_ASSET("scr_bike_adversary")
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_bike_adversary")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Idle
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_CURRENT_POWERUP_HAVE_A_TRIGGER_CONDITION(FMMC_POWERUP_STRUCT &sPowerUp)
	RETURN NOT IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPOWERUP_BS_InstantTrigger)
ENDFUNC

PROC DO_POWERUP_SHAPETEST(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF sPowerUp.sShapeTest.stiShapeTest = NULL
		sPowerUp.sShapeTest.vPos = GET_ENTITY_COORDS(sPowerUp.viCurrentVeh)
		sPowerUp.sShapeTest.vPos.z += 1.0
		sPowerUp.sShapeTest.vDimensions = <<2.5, 4.0, 2.0>>
		sPowerUp.sShapeTest.vRot = GET_ENTITY_ROTATION(sPowerUp.viCurrentVeh)
		INT iFlags = SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT
		sPowerUp.sShapeTest.stiShapeTest = START_SHAPE_TEST_BOX(sPowerUp.sShapeTest.vPos, sPowerUp.sShapeTest.vDimensions, sPowerUp.sShapeTest.vRot, DEFAULT, iFlags)
	ELSE
		VECTOR vTemp1Test, vTemp2Test
		ENTITY_INDEX hitEntity
		GET_SHAPE_TEST_RESULT(sPowerUp.sShapeTest.stiShapeTest, sPowerUp.sShapeTest.iResult, vTemp1Test, vTemp2Test, hitEntity)
		sPowerUp.sShapeTest.stiShapeTest = NULL
		RELEASE_SCRIPT_GUID_FROM_ENTITY(hitEntity)
	ENDIF
ENDPROC

FUNC BOOL CAN_POWERUP_TRIGGER_INPUT_BE_PRESSED(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF IS_VEHICLE_FUCKED_MP(sPowerUp.viCurrentVeh)
		PRINTLN("CAN_POWERUP_TRIGGER_INPUT_BE_PRESSED - Vehicle is dead")
		RETURN FALSE
	ENDIF
	
	IF IS_CURRENT_POWERUP_A_TRANSFORM(sPowerUp)
	
		IF NOT IS_VEHICLE_IN_VALID_POSITION_FOR_TRANSFORM(sPowerUp.viCurrentVeh)
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PWRUP_IVL")
				PRINT_HELP("PWRUP_IVL", 5000)
			ENDIF
			PRINTLN("CAN_POWERUP_TRIGGER_INPUT_BE_PRESSED - vehicle in compromised position for transform")
			RETURN FALSE
		ENDIF
		
		IF sPowerUp.sShapeTest.iResult = 1
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PWRUP_NES")
				PRINT_HELP("PWRUP_NES", 5000)
			ENDIF
			PRINTLN("CAN_POWERUP_TRIGGER_INPUT_BE_PRESSED - shape test stopping usage")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_CURRENT_POWERUP_A_MINE(sPowerUp)
	
		IF NOT CAN_VEHICLE_USE_MINES(sPowerUp.viCurrentVeh)
			PRINTLN("CAN_POWERUP_TRIGGER_INPUT_BE_PRESSED - vehicle in compromised position for mine use")
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_POWERUP_INPUT_BEEN_TRIGGERED(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF IS_CURRENT_POWERUP_A_MINE(sPowerUp)
		RETURN IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_HORN)
	ENDIF
	
	RETURN IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
ENDFUNC

FUNC BOOL HAS_POWERUP_HAD_TRIGGER_ACTIVATED(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF NOT IS_ENTITY_ALIVE(sPowerUp.LocalPlayerPed)
	OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_CURRENT_POWERUP_A_TRANSFORM(sPowerUp)
		DO_TRANSFORM_SHAPETEST(sPowerUp.sShapeTest, GET_ENTITY_COORDS(sPowerUp.viCurrentVeh), GET_ENTITY_ROTATION(sPowerUp.viCurrentVeh))
	ENDIF
	
	IF HAS_POWERUP_INPUT_BEEN_TRIGGERED(sPowerUp)
	AND CAN_POWERUP_TRIGGER_INPUT_BE_PRESSED(sPowerUp)
	AND sPowerUp.ePowerUpState = POWERUP_STATE_IDLE
		SET_BIT(sPowerUp.iPowerUpBitSet, ciPOWERUP_BS_Triggered)
		PRINTLN("[Power Ups] HAS_POWERUP_HAD_TRIGGER_ACTIVATED - Horn pressed")
	ENDIF
	
	RETURN HAS_CURRENT_POWERUP_BEEN_TRIGGERED(sPowerUp)
ENDFUNC

FUNC BOOL HAS_CHANGE_POWERUP_INPUT_BEEN_PRESSED()

	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
	OR IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_HORN)
		PRINTLN("HAS_CHANGE_POWERUP_INPUT_BEEN_PRESSED - Powerup Just used")
		RETURN FALSE
	ENDIF
	
	RETURN IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	
ENDFUNC

PROC CHANGE_POWERUP_CURRENT_SLOT(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF sPowerUp.iCurrentPowerUpsCount <= 1
		IF HAS_CHANGE_POWERUP_INPUT_BEEN_PRESSED()
			PLAY_POWERUPS_SWITCH_SOUND(FALSE)
		ENDIF
		EXIT
	ENDIF
	
	IF sPowerUp.ePowerUpState != POWERUP_STATE_IDLE
		IF HAS_CHANGE_POWERUP_INPUT_BEEN_PRESSED()
			PLAY_POWERUPS_SWITCH_SOUND(FALSE)
		ENDIF
		EXIT
	ENDIF
	
	IF HAS_CHANGE_POWERUP_INPUT_BEEN_PRESSED()
		PRINTLN("[Power Ups] CHANGE_POWERUP_CURRENT_SLOT")
		PLAY_POWERUPS_SWITCH_SOUND(TRUE)
		CLEANUP_CURRENT_POWERUP_EFFECT(sPowerUp, TRUE)
		SET_CURRENT_POWERUP_INDEX(sPowerUp, FIND_NEXT_FILLED_POWERUP_SLOT(sPowerUp))
		SET_POWERUP_STATE(sPowerUp, POWERUP_STATE_INIT)
		SET_POWERUP_HUD_STATE(sPowerUp, POWERUP_HUD_STATE_UPDATE)
	ENDIF
ENDPROC

FUNC BOOL IS_POWERUP_STILL_READY(FMMC_POWERUP_STRUCT &sPowerUp)
	SWITCH GET_CURRENT_POWERUP(sPowerUp)
		CASE POWERUP_MINE_KINETIC
		CASE POWERUP_MINE_SPIKE
		CASE POWERUP_MINE_EMP
		CASE POWERUP_MINE_SLICK
		CASE POWERUP_BOMB
			IF NOT IS_VEHICLE_FUCKED_MP(sPowerUp.viCurrentVeh)
			AND GET_VEHICLE_MOD(sPowerUp.viCurrentVeh, MOD_WING_R) = -1
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Running
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL HAS_POWERUP_EFFECT_ENDED(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF HAS_NET_TIMER_STARTED(sPowerUp.tdPowerUpMinimumTimer)
	AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sPowerUp.tdPowerUpMinimumTimer, ciFMMC_POWERUP_MINIMUM_DURATION)
		PRINTLN("[Power Ups] HAS_POWERUP_EFFECT_ENDED - Minimum duration not passed yet")
		RETURN FALSE
	ENDIF
	
	SWITCH GET_CURRENT_POWERUP(sPowerUp)
		CASE POWERUP_BOOST_SHUNT
			IF g_VehicleBoostInfo.bIsActive
				PRINTLN("[Power Ups] HAS_POWERUP_EFFECT_ENDED - Still set")
				RETURN FALSE
			ENDIF
		BREAK
		CASE POWERUP_ROCKET
			IF HAS_COLLECTED_VEHICLE_ROCKET()
				RETURN FALSE
			ENDIF
		BREAK
		CASE POWERUP_MINE_KINETIC
		CASE POWERUP_MINE_SPIKE
		CASE POWERUP_MINE_EMP
		CASE POWERUP_MINE_SLICK
		CASE POWERUP_BOMB
			IF !g_bPowerUpMineUsed
				RETURN FALSE
			ENDIF
		BREAK
		CASE POWERUP_TANK
		CASE POWERUP_MONSTER_TRUCK
			
			IF DOES_ENTITY_EXIST(sPowerUp.viCurrentVeh)
				IF GET_ENTITY_MODEL(sPowerUp.viCurrentVeh) != sPowerUp.mnPreSwapVeh
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Finished)
				RETURN FALSE
			ENDIF
			
		BREAK
	ENDSWITCH
	
	IF sPowerUp.iPowerUpDuration != ciFMMC_POWERUP_IGNORE_DURATION
	AND NOT HAS_CURRENT_POWERUP_TIME_EXPIRED(sPowerUp)
	AND NOT IS_BIT_SET(sPowerUp.iProcessingBitSet, ciPWP_Process_BS_Remove_Pickups)
		PRINTLN("[Power Ups] HAS_POWERUP_EFFECT_ENDED - Timer not finished")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_SHUNT_COLLISION(FMMC_POWERUP_STRUCT &sPowerUp, PLAYER_INDEX otherPlayer)
	
	IF NOT IS_PED_IN_ANY_VEHICLE(sPowerUp.LocalPlayerPed)
		PRINTLN("[SMASH_FORCE] PROCESS_SHUNT_COLLISION - Local Player is not in any vehicle ")
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(otherPlayer))
		PRINTLN("[SMASH_FORCE] PROCESS_SHUNT_COLLISION - Other Player is not in any vehicle ")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Shunt_Hit)
		PRINTLN("[SMASH_FORCE] PROCESS_SHUNT_COLLISION - Already hit a player ")
		EXIT
	ENDIF
	
	FLOAT fFactor = GET_VEHICLE_SPEED_FACTOR(sPowerUp.viCurrentVeh)
	
	BROADCAST_FMMC_SHUNT_POWERUP_EVENT(otherPlayer, fFactor)
	
	SET_BIT(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Shunt_Hit)
	
ENDPROC

PROC PROCESS_SHUNT_POWERUP(FMMC_POWERUP_STRUCT &sPowerUp)
	INT iPart
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	PED_INDEX tempPed
	
	FOR iPart = 0 TO MAX_NUM_MC_PLAYERS-1
		
		IF iPart = PARTICIPANT_ID_TO_INT()
			RELOOP
		ENDIF
		
		tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			RELOOP
		ENDIF
		
		tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
		IF NOT IS_NET_PLAYER_OK(tempPlayer)
			RELOOP
		ENDIF
		
		tempPed = GET_PLAYER_PED(tempPlayer)
		IF NOT IS_ENTITY_TOUCHING_ENTITY(sPowerUp.LocalPlayerPed, tempPed)
			RELOOP
		ENDIF
		
		PROCESS_SHUNT_COLLISION(sPowerUp, tempPlayer)
		BREAKLOOP
	ENDFOR
ENDPROC

PROC PROCESS_POWERUP_REPAIR(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF IS_PED_INJURED(sPowerUp.LocalPlayerPed)
		EXIT
	ENDIF
	
	STRING sPTFXAsset, sPTFXParticle
	
	sPTFXAsset = "scr_sum_ow"
	sPTFXParticle = "scr_sum_ow_race_repair_smoke"
	
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED(sPTFXAsset)
		EXIT
	ENDIF
	PRINTLN("[Power Ups] PROCESS_POWERUP_REPAIR - Activated")
		
	IF DOES_ENTITY_EXIST(sPowerUp.viCurrentVeh)
	AND IS_VEHICLE_DRIVEABLE(sPowerUp.viCurrentVeh)
	
		USE_PARTICLE_FX_ASSET(sPTFXAsset)
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY(sPTFXParticle, sPowerUp.viCurrentVeh, <<0,0,0>>, <<0,0,0>>)
		
		INT iHealth = 1000
		
		SET_VEHICLE_ENGINE_ON(sPowerUp.viCurrentVeh, TRUE, TRUE)
		SET_VEHICLE_WEAPON_DAMAGE_SCALE(sPowerUp.viCurrentVeh, 1.0)
		SET_VEHICLE_ENGINE_HEALTH(sPowerUp.viCurrentVeh, TO_FLOAT(iHealth))
		SET_VEHICLE_PETROL_TANK_HEALTH(sPowerUp.viCurrentVeh, TO_FLOAT(iHealth))
		SET_VEHICLE_BODY_HEALTH(sPowerUp.viCurrentVeh, TO_FLOAT(iHealth))
		SET_VEHICLE_FIXED(sPowerUp.viCurrentVeh)
		SET_ENTITY_HEALTH(sPowerUp.viCurrentVeh, iHealth)
	ENDIF
ENDPROC

PROC PROCESS_POWERUP_START_TRANSFORM(FMMC_POWERUP_STRUCT &sPowerUp, VEHICLE_SWAP_DATA &sVehicleSwap, BOOL bInitialTransform)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID)
		REMOVE_PARTICLE_FX(sVehicleSwap.ptfxID)
		PRINTLN("[VehSwap] - PROCESS_POWERUP_START_TRANSFORM - Clearing up the smokey effect because we are swapping now!")
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(sPowerUp.LocalPlayerPed)
		REMOVE_PARTICLE_FX_FROM_ENTITY(GET_VEHICLE_PED_IS_IN(sPowerUp.LocalPlayerPed))
		PRINTLN("[VehSwap] - PROCESS_POWERUP_START_TRANSFORM - Removing particle FX from the vehicle the player is currently in")
	ENDIF
	
	REMOVE_PARTICLE_FX_FROM_ENTITY(sPowerUp.LocalPlayerPed)
	PRINTLN("[VehSwap] - PROCESS_POWERUP_START_TRANSFORM - Removing particle FX from the local player's ped")
	
	g_bBlockFMMCVehMachineGuns = bInitialTransform
	PRINTLN("[Power Ups] - PROCESS_POWERUP_START_TRANSFORM - Setting g_bBlockFMMCVehMachineGuns to ", PICK_STRING(g_bBlockFMMCVehMachineGuns, "TRUE", "FALSE"))
	
	SET_VEHICLE_SWAP_STATE(sVehicleSwap.eVehicleSwapState, eVehicleSwapState_INITIALISE)
ENDPROC

PROC PROCESS_MINITANK_SETUP(FMMC_POWERUP_STRUCT &sPowerUp)
	PRINTLN("[Power Ups] PROCESS_MINITANK_SETUP")
	INT iHealth = 3000
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(sPowerUp.sVehicleSwap.viNewVeh, FALSE)
	SET_ENTITY_HEALTH(sPowerUp.sVehicleSwap.viNewVeh, iHealth)
	SET_VEHICLE_ENGINE_HEALTH(sPowerUp.sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
	SET_VEHICLE_PETROL_TANK_HEALTH(sPowerUp.sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
	SET_VEHICLE_BODY_HEALTH(sPowerUp.sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
	
	SET_PLAYER_USING_RC_TANK(TRUE)
	
	PRELOAD_VEHICLE_MOD(sPowerUp.sVehicleSwap.viNewVeh, MOD_ROOF, 2)
	SET_VEHICLE_MOD(sPowerUp.sVehicleSwap.viNewVeh, MOD_ROOF, 2)
ENDPROC

PROC PROCESS_MONSTER_TRUCK_SETUP(FMMC_POWERUP_STRUCT &sPowerUp)
	PRINTLN("[Power Ups] PROCESS_MONSTER_TRUCK_SETUP")
	INT iHealth = 1000
	SET_ENTITY_HEALTH(sPowerUp.sVehicleSwap.viNewVeh, iHealth)
	SET_VEHICLE_ENGINE_HEALTH(sPowerUp.sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
	SET_VEHICLE_PETROL_TANK_HEALTH(sPowerUp.sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
	SET_VEHICLE_BODY_HEALTH(sPowerUp.sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
	
	SET_INCREASE_WHEEL_CRUSH_DAMAGE(sPowerUp.sVehicleSwap.viNewVeh, TRUE)
	FMMC_SET_VEH_MOD_PRESET(sPowerUp.sVehicleSwap.viNewVeh, sPowerUp.mnModelToSwapTo, 2)
	
ENDPROC

PROC PROCESS_POWERUP_TRANSFORM_VEHICLE_SETUP(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Setup_Done)
		EXIT
	ENDIF
	
	IF (sPowerUp.ePreState = eVehicleSwapState_CREATE
	OR sPowerUp.ePreState = eVehicleSwapState_INITIALISE)
	AND sPowerUp.sVehicleSwap.eVehicleSwapState != eVehicleSwapState_CREATE
	AND sPowerUp.sVehicleSwap.eVehicleSwapState != eVehicleSwapState_INITIALISE
		
		IF NOT DOES_ENTITY_EXIST(sPowerUp.sVehicleSwap.viNewVeh)
			EXIT
		ENDIF
		
		PRINTLN("[VehSwap] PROCESS_POWERUP_TRANSFORM_VEHICLE_SETUP - Setting up transform vehicle")		
		SWITCH GET_CURRENT_POWERUP(sPowerUp)
			CASE POWERUP_TANK
				PROCESS_MINITANK_SETUP(sPowerUp)
			BREAK
			CASE POWERUP_MONSTER_TRUCK
				PROCESS_MONSTER_TRUCK_SETUP(sPowerUp)
			BREAK
		ENDSWITCH

		IF sPowerUp.sVehicleSwap.eVehicleSwapState = eVehicleSwapState_APPLY_FORCES
			VS_PROCESS_APPLY_FORCES(sPowerUp.sVehicleSwap, sPowerUp.mnModelToSwapTo)
		ENDIF
		
		PRINTLN("[VehSwap] PROCESS_POWERUP_TRANSFORM_VEHICLE_SETUP - Setting g_SpawnData.MissionSpawnDetails.SpawnModel to ", sPowerUp.mnModelToSwapTo)
		
		SET_BIT(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Setup_Done)
	ENDIF
ENDPROC

PROC PROCESS_POWERUP_TRANSFORM_BACK_VEHICLE_SETUP(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF NOT IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Back)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Back_Setup_Done)
		EXIT
	ENDIF
	
	IF (sPowerUp.ePreState = eVehicleSwapState_CREATE
	OR sPowerUp.ePreState = eVehicleSwapState_INITIALISE)
	AND sPowerUp.sVehicleSwap.eVehicleSwapState != eVehicleSwapState_CREATE
	AND sPowerUp.sVehicleSwap.eVehicleSwapState != eVehicleSwapState_INITIALISE
		
		IF NOT DOES_ENTITY_EXIST(sPowerUp.sVehicleSwap.viNewVeh)
			EXIT
		ENDIF
		
		PRINTLN("[VehSwap] PROCESS_POWERUP_TRANSFORM_BACK_VEHICLE_SETUP - Setting up original vehicle")
		IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(PLAYER_ID())
		AND g_iMyRaceModelChoice != -1
			PRINTLN("[VehSwap] PROCESS_POWERUP_TRANSFORM_BACK_VEHICLE_SETUP - Setting up personal vehicle")
			g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP = g_MpSavedVehicles[g_iMyRaceModelChoice].VehicleSetupMP
			MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_USING_GAMER_HANDLE(sPowerUp.sVehicleSwap.viNewVeh, g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP, g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.GamerHandleOfCarOwner)
		ELSE
			PRINTLN("[VehSwap] PROCESS_POWERUP_TRANSFORM_BACK_VEHICLE_SETUP - Setting up stock vehicle")
			SET_VEHICLE_COLOUR_FROM_LOBBY_SELECTION(sPowerUp.sVehicleSwap.viNewVeh)
		ENDIF
		
		IF sPowerUp.sVehicleSwap.eVehicleSwapState = eVehicleSwapState_APPLY_FORCES
			VS_PROCESS_APPLY_FORCES(sPowerUp.sVehicleSwap, sPowerUp.mnModelToSwapTo)
		ENDIF
		
		SET_BIT(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Back_Setup_Done)
	ENDIF
ENDPROC

PROC PROCESS_POWERUP_TRANSFORM_ONE_FRAME_CLEANUP(FMMC_POWERUP_STRUCT &sPowerUp)
	IF (sPowerUp.ePreState = eVehicleSwapState_CREATE
	OR sPowerUp.ePreState = eVehicleSwapState_INITIALISE)
	AND sPowerUp.sVehicleSwap.eVehicleSwapState = eVehicleSwapState_CLEANUP
		PRINTLN("[VehSwap] PROCESS_POWERUP_TRANSFORM_ONE_FRAME_CLEANUP - Attempting single frame cleanup")
		PROCESS_VEHICLE_SWAP(sPowerUp.sVehicleSwap, sPowerUp.mnModelToSwapTo)
	ENDIF
ENDPROC

FUNC MODEL_NAMES GET_MODEL_FOR_TANK_TRANSFORM(FMMC_POWERUP_STRUCT &sPowerUp)
	MODEL_NAMES mnVehModel = sPowerUp.mnModelToSwapTo
	
	IF IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Back)
	OR (HAS_CURRENT_POWERUP_TIME_EXPIRED(sPowerUp)
	AND NOT IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Back))
		mnVehModel = sPowerUp.mnPreSwapVeh
	ENDIF
	
	RETURN mnVehModel
ENDFUNC

FUNC BOOL SHOULD_TRANSFORM_POWERUP_END(FMMC_POWERUP_STRUCT &sPowerUp)
	IF HAS_CURRENT_POWERUP_TIME_EXPIRED(sPowerUp)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sPowerUp.iProcessingBitSet, ciPWP_Process_BS_Remove_Pickups)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_POWERUP_TRANSFORM(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF NOT IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPowerUP_BS_Just_Triggered)
		sPowerUp.mnPreSwapVeh = GET_ENTITY_MODEL(sPowerUp.viCurrentVeh)
		PROCESS_POWERUP_START_TRANSFORM(sPowerUp, sPowerUp.sVehicleSwap, TRUE)
		SET_BIT(sPowerUp.iPowerUpBitSet, ciPowerUP_BS_Just_Triggered)
	ENDIF
	
	IF IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Back)
		IF sPowerUp.sVehicleSwap.eVehicleSwapState = eVehicleSwapState_IDLE
		AND NOT IS_ENTITY_A_GHOST(sPowerUp.LocalPlayerPed)
			SET_BIT(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Finished)
			PRINTLN("PROCESS_POWERUP_TANK - Setting ciPowerUp_BS_Transform_Finished")
		ENDIF
	ENDIF
	
	IF SHOULD_TRANSFORM_POWERUP_END(sPowerUp)
	AND NOT IS_BIT_SET(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Back)
		PROCESS_POWERUP_START_TRANSFORM(sPowerUp, sPowerUp.sVehicleSwap, FALSE)
//		g_SpawnData.MissionSpawnDetails.SpawnModel = GET_MODEL_FOR_TANK_TRANSFORM(sPowerUp)
		SET_BIT(sPowerUp.iPowerUpBitSet, ciPowerUp_BS_Transform_Back)
	ENDIF	
	
ENDPROC

PROC PROCESS_POWERUP_AMPED(FMMC_POWERUP_STRUCT &sPowerUp)
	g_bFMMCVehMachineGuns_Amped = TRUE
	g_fFMMCVehMachineGuns_DmgMultiplier = 2.0
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sPowerUp.ptfxAmped)
	AND IS_ENTITY_ALIVE(sPowerUp.viCurrentVeh)
		VECTOR vModelMin, vModelMax, vOffset, vRot

		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(sPowerUp.viCurrentVeh), vModelMin, vModelMax)
		vOffset.y = vModelMax.y
		USE_PARTICLE_FX_ASSET("scr_bike_adversary")
		sPowerUp.ptfxAmped = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_adversary_gunsmith_weap_smoke", sPowerUp.viCurrentVeh, vOffset, vRot, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0.2, 0.8, 1.0)
	ENDIF
ENDPROC

PROC PROCESS_CURRENT_POWERUP_EFFECT(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF NOT HAS_NET_TIMER_STARTED(sPowerUp.tdPowerUpTimer)
		REINIT_NET_TIMER(sPowerUp.tdPowerUpTimer)
		PRINTLN("[Power Ups] PROCESS_CURRENT_POWERUP_EFFECT - Timer Started for power up type: ", GET_POWERUP_NAME(GET_CURRENT_POWERUP(sPowerUp)), " duration: ", sPowerUp.iPowerUpDuration)
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(sPowerUp.tdPowerUpMinimumTimer)
		REINIT_NET_TIMER(sPowerUp.tdPowerUpMinimumTimer)
	ENDIF
	
	SWITCH GET_CURRENT_POWERUP(sPowerUp)
		CASE POWERUP_BOOST_SHUNT
			//Boosting handled by net_car_boost.sch
			PROCESS_SHUNT_POWERUP(sPowerUp)
		BREAK
		CASE POWERUP_REPAIR
			PROCESS_POWERUP_REPAIR(sPowerUp)
		BREAK
		CASE POWERUP_ROCKET
			//Handled by net_car_rockets.sch
		BREAK
		CASE POWERUP_GHOST
			IF NOT IS_ENTITY_A_GHOST(sPowerUp.LocalPlayerPed)
				SET_LOCAL_PLAYER_AS_GHOST(TRUE, TRUE)	
				SET_GHOST_ALPHA(50)
			ENDIF
		BREAK
		CASE POWERUP_TANK
		CASE POWERUP_MONSTER_TRUCK
			PROCESS_POWERUP_TRANSFORM(sPowerUp)
		BREAK
		CASE POWERUP_MINE_KINETIC
		CASE POWERUP_MINE_SPIKE
		CASE POWERUP_MINE_EMP
		CASE POWERUP_MINE_SLICK
		CASE POWERUP_BOMB
			//Handled by net_vehicle_weapons.sch
		BREAK
		CASE POWERUP_MG_AMP
			PROCESS_POWERUP_AMPED(sPowerUp)
		BREAK
	ENDSWITCH
ENDPROC

				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Circle HUD
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_INT_FOR_POWERUP_ICON(FMMC_POWERUP_TYPE ePowerUpType)
	SWITCH ePowerUpType
		CASE POWERUP_BOOST_SHUNT		RETURN 30
		CASE POWERUP_BOMB				RETURN 32
		CASE POWERUP_REPAIR				RETURN 18
		CASE POWERUP_GHOST				RETURN 10
		CASE POWERUP_ROCKET				RETURN 19
		CASE POWERUP_MINE_KINETIC		RETURN 33
		CASE POWERUP_MINE_SPIKE			RETURN 13
		CASE POWERUP_MINE_EMP			RETURN 31
		CASE POWERUP_MINE_SLICK			RETURN 35
		CASE POWERUP_TANK				RETURN 2
		CASE POWERUP_MG_AMP				RETURN 13
		CASE POWERUP_MONSTER_TRUCK		RETURN 34
	ENDSWITCH
	
	RETURN 0
ENDFUNC

PROC ADD_ICON_FOR_CIRCLE_HUD(FMMC_POWERUP_STRUCT &sPowerUp, INT iIcon)
	BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "ADD_ICON")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcon)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC INT GET_ICON_ID_FOR_CURRENT_POWERUP_IN_SLOT(INT iPowerup, INT iSlot)
	INT iPowerupIcon = iPowerup
	
	INT iSlotMod = iSlot + 1
	
	iPowerupIcon += ((iSlot * ENUM_TO_INT(POWERUP_MAX)) + iSlotMod)
	
	RETURN iPowerupIcon
ENDFUNC

FUNC BOOL POWERUP_CIRCLE_HUD_SETUP(FMMC_POWERUP_STRUCT &sPowerUp)
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sPowerUp.siPowerUpHUD)
		sPowerUp.siPowerUpHUD = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_GENERIC")
		PRINTLN("[Power Ups] POWERUP_CIRCLE_HUD_SETUP - Requesting scaleform!")
	ELSE
		INT iSlot, iPowerUp
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "ADD_TEAM")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_GREY))
		END_SCALEFORM_MOVIE_METHOD()
		BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "ADD_TEAM")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_RED))
		END_SCALEFORM_MOVIE_METHOD()
		
		FOR iSlot = 0 TO GET_MAX_NUMBER_OF_POWERUPS()-1
			ADD_ICON_FOR_CIRCLE_HUD(sPowerUp, 0) //Blank for showing empty slot
			PRINTLN("POWERUP_CIRCLE_HUD_SETUP - Adding icon 0 for slot ", iSlot)
			FOR iPowerUp = 0 TO ENUM_TO_INT(POWERUP_MAX)-1
				INT iIcon = GET_INT_FOR_POWERUP_ICON(INT_TO_ENUM(FMMC_POWERUP_TYPE, iPowerUp))
				ADD_ICON_FOR_CIRCLE_HUD(sPowerUp, iIcon)
				PRINTLN("POWERUP_CIRCLE_HUD_SETUP - Adding icon ",iPowerUp+1," for slot ", iSlot)
			ENDFOR
		ENDFOR
		
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_ACTIVE_POWERUP_CIRCLE_HUD(FMMC_POWERUP_STRUCT &sPowerUp)
	IF sPowerUp.iPowerUpDuration <= 0
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(sPowerUp.tdPowerUpTimer)
		EXIT
	ENDIF
	
	FLOAT fTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sPowerUp.tdPowerUpTimer))
	FLOAT fTimeProgress = fTimePassed / sPowerUp.iPowerUpDuration
	
	INT iIcon = GET_ICON_ID_FOR_CURRENT_POWERUP_IN_SLOT(ENUM_TO_INT(GET_CURRENT_POWERUP(sPowerUp)), sPowerUp.iCurrentPowerUpIndex)
	
	IF HAS_CURRENT_POWERUP_TIME_REACHED_PERCENTAGE(sPowerUp, 75)
		IF NOT IS_BIT_SET(sPowerUp.iPowerUpHUDBitset, ciPowerUPHUD_BS_Pulsed)
			BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "PULSE_ICON")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcon)
			END_SCALEFORM_MOVIE_METHOD()
			SET_BIT(sPowerUp.iPowerUpHUDBitset, ciPowerUPHUD_BS_Pulsed)
		ENDIF
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "SET_ICON_TIMER")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcon) //Icon index
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0) //White Value
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeProgress) //Red Value
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "SET_ICON_TIMER")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcon) //Icon index
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeProgress) //White Value
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0) //Red Value
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC UPDATE_ACTIVE_POWERUP_CIRCLE_HUD(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF sPowerUp.siPowerUpHUD = NULL
		EXIT
	ENDIF
	
	INT iSlot, iPowerUp
	FOR iSlot = 0 TO GET_MAX_NUMBER_OF_POWERUPS()-1
		
		INT iBlankSlot = (iSlot * ENUM_TO_INT(POWERUP_MAX)) + iSlot
		
		//Blank circle
		IF sPowerUp.eCurrentPowerUps[iSlot] = POWERUP_NONE
			BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "SHOW_ICON")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlankSlot)
			END_SCALEFORM_MOVIE_METHOD()
		ELSE
			BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "HIDE_ICON")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlankSlot)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		//Powerup Icons
		FOR iPowerUp = 0 TO ENUM_TO_INT(POWERUP_MAX)-1
			
			IF ENUM_TO_INT(sPowerUp.eCurrentPowerUps[iSlot]) = iPowerUp
				BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "SHOW_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_ICON_ID_FOR_CURRENT_POWERUP_IN_SLOT(iPowerUp, iSlot))
				END_SCALEFORM_MOVIE_METHOD()
				IF iSlot = sPowerUp.iCurrentPowerUpIndex
					BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "ACTIVATE_ICON")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_ICON_ID_FOR_CURRENT_POWERUP_IN_SLOT(iPowerUp, iSlot))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
					END_SCALEFORM_MOVIE_METHOD()
					PRINTLN("UPDATE_ACTIVE_POWERUP_CIRCLE_HUD - Activating slot ", iSlot," Icon ", GET_ICON_ID_FOR_CURRENT_POWERUP_IN_SLOT(iPowerUp, iSlot))
				ELSE
					BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "DEACTIVATE_ICON")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_ICON_ID_FOR_CURRENT_POWERUP_IN_SLOT(iPowerUp, iSlot))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ELSE
				BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "DEACTIVATE_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_ICON_ID_FOR_CURRENT_POWERUP_IN_SLOT(iPowerUp, iSlot))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "HIDE_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_ICON_ID_FOR_CURRENT_POWERUP_IN_SLOT(iPowerUp, iSlot))
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF

		ENDFOR
	ENDFOR
ENDPROC

FUNC BOOL SHOW_SHARD_FOR_COLLECTED_POWERUP(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sPowerupData.iPWROptionsBitSet, ciFMMC_PowerUp_ShardOnCollection)
		sPowerUp.ePowerUpJustCollected = POWERUP_NONE
		RETURN TRUE
	ENDIF
	
	TEXT_LABEL_15 tlTitle = GET_TEXT_LABEL_FOR_POWERUP_TYPE(ENUM_TO_INT(sPowerUp.ePowerUpJustCollected), TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "SHOW_MESSAGE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlTitle) //Title
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("") //Strap
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)	  //Team
	END_SCALEFORM_MOVIE_METHOD()
	sPowerUp.ePowerUpJustCollected = POWERUP_NONE
	SET_BIT(sPowerUp.iPowerUpHUDBitSet, ciPowerUPHUD_BS_ShardDisplaying)
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_POWERUP_SHARD(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF IS_BIT_SET(sPowerUp.iPowerUpHUDBitSet, ciPowerUPHUD_BS_ShardDisplaying)
		IF NOT HAS_NET_TIMER_STARTED(sPowerUp.tdShardTimer)
			REINIT_NET_TIMER(sPowerUp.tdShardTimer)
		ELIF HAS_NET_TIMER_EXPIRED_READ_ONLY(sPowerUp.tdShardTimer, ciFMMC_POWERUP_SHARD_DURATION)
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "HIDE_MESSAGE")
			END_SCALEFORM_MOVIE_METHOD()
			
			RESET_NET_TIMER(sPowerUp.tdShardTimer)
			CLEAR_BIT(sPowerUp.iPowerUpHUDBitSet, ciPowerUPHUD_BS_ShardDisplaying)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_POWERUP_HELP_TEXT(FMMC_POWERUP_STRUCT &sPowerUp, INT iOverrideTime = -1)
	IF GET_CURRENT_POWERUP(sPowerUp) = POWERUP_NONE
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PWRUP_NES")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PWRUP_IVL")
		EXIT
	ENDIF
	TEXT_LABEL_15 tl15 = GET_TEXT_LABEL_FOR_POWERUP_TYPE(ENUM_TO_INT(GET_CURRENT_POWERUP(sPowerUp)), FALSE, TRUE)
	TEXT_LABEL_15 tlHelp = "PWRUP_USE"
	IF sPowerUp.iCurrentPowerUpsCount > 1
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			tlHelp = "PWRUP_USE_2_MK"
		ELSE
			tlHelp = "PWRUP_USE_2"
		ENDIF
	ENDIF
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(tlHelp)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tl15)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE, iOverrideTime)
ENDPROC

PROC POWERUP_CIRCLE_HUD_CLEANUP(FMMC_POWERUP_STRUCT &sPowerUp)
	
	PRINTLN("[Power Ups] POWERUP_CIRCLE_HUD_CLEANUP")
	
	CLEAR_BIT(sPowerUp.iPowerUpHUDBitset, ciPowerUPHUD_BS_Pulsed)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sPowerUp.siPowerUpHUD, "SET_ICON_TIMER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_CURRENT_POWERUP(sPowerUp))) //Icon index
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0) //White Value
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0) //Red Value
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC BOOL SHOULD_HUD_CLEANUP(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF NOT HAS_NET_TIMER_STARTED(sPowerUp.tdPowerUpTimer)
	AND sPowerUp.ePowerUpState = POWERUP_STATE_IDLE
		PRINTLN("[Power Ups] SHOULD_HUD_CLEANUP - Circle HUD stuck, needs to go to clean up")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_POWERUP_HUD(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
		EXIT
	ENDIF
	
	IF IS_PLAYER_SPECTATING(PLAYER_ID())
		IF sPowerUp.siPowerUpHUD != NULL
			PRINTLN("PROCESS_POWERUP_HUD - Removing HUD in spectator")
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sPowerUp.siPowerUpHUD)
		ENDIF
		EXIT
	ENDIF
	
	IF sPowerUp.ePowerUpJustCollected != POWERUP_NONE
	AND GET_POWERUP_HUD_STATE(sPowerUp) > POWERUP_HUD_STATE_SETUP
		IF SHOW_SHARD_FOR_COLLECTED_POWERUP(sPowerUp)
			PRINTLN("PROCESS_POWERUP_HUD - Updating HUD for just collecting a pickup")
			UPDATE_ACTIVE_POWERUP_CIRCLE_HUD(sPowerUp)
		ENDIF
	ENDIF
	
	SWITCH GET_POWERUP_HUD_STATE(sPowerUp)
		CASE POWERUP_HUD_STATE_SETUP
			IF POWERUP_CIRCLE_HUD_SETUP(sPowerUp)
				UPDATE_ACTIVE_POWERUP_CIRCLE_HUD(sPowerUp)
				SET_POWERUP_HUD_STATE(sPowerUp, POWERUP_HUD_STATE_IDLE)
			ENDIF
		BREAK
		CASE POWERUP_HUD_STATE_IDLE
			
			PROCESS_POWERUP_HELP_TEXT(sPowerUp)
			
			IF HAS_POWERUP_HAD_TRIGGER_ACTIVATED(sPowerUp)
				SET_POWERUP_HUD_STATE(sPowerUp, POWERUP_HUD_STATE_TRIGGERED)
			ENDIF
		BREAK
		CASE POWERUP_HUD_STATE_UPDATE
			UPDATE_ACTIVE_POWERUP_CIRCLE_HUD(sPowerUp)
			SET_POWERUP_HUD_STATE(sPowerUp, POWERUP_HUD_STATE_IDLE)
		BREAK
		CASE POWERUP_HUD_STATE_TRIGGERED
			SET_POWERUP_HUD_STATE(sPowerUp, POWERUP_HUD_STATE_IN_USE)
		BREAK
		CASE POWERUP_HUD_STATE_IN_USE
			PROCESS_ACTIVE_POWERUP_CIRCLE_HUD(sPowerUp)
			
			IF HAS_POWERUP_EFFECT_ENDED(sPowerUp)
			OR SHOULD_HUD_CLEANUP(sPowerUp)
				SET_POWERUP_HUD_STATE(sPowerUp, POWERUP_HUD_STATE_CLEANUP)
			ENDIF
		BREAK
		CASE POWERUP_HUD_STATE_CLEANUP
			IF sPowerUp.ePowerUpState = POWERUP_STATE_IDLE
			OR sPowerUp.ePowerUpState = POWERUP_STATE_CLEANUP
			OR sPowerUp.ePowerUpState = POWERUP_STATE_NONE
				POWERUP_CIRCLE_HUD_CLEANUP(sPowerUp)
				SET_POWERUP_HUD_STATE(sPowerUp, POWERUP_HUD_STATE_UPDATE)
			ENDIF
		BREAK
	ENDSWITCH
	
	PROCESS_POWERUP_SHARD(sPowerUp)
	
	IF GET_POWERUP_HUD_STATE(sPowerUp) > POWERUP_HUD_STATE_SETUP
	AND HAS_SCALEFORM_MOVIE_LOADED(sPowerUp.siPowerUpHUD)
		DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_RANKBAR)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(sPowerUp.siPowerUpHUD, 255, 255, 255, 255)
	ENDIF
ENDPROC


PROC PROCESS_BLOCKED_POWERUP_INPUTS(FMMC_POWERUP_STRUCT &sPowerUp)
	IF GET_CURRENT_POWERUP(sPowerUp) = POWERUP_TANK
	AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sPowerUp.tdPowerUpMinimumTimer, ciFMMC_POWERUP_MINIMUM_DURATION)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP)
	ENDIF
	
	IF GET_CURRENT_POWERUP(sPowerUp) != POWERUP_NONE
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	ENDIF
	
ENDPROC

PROC PROCESS_CACHING_CURRENT_ENTITIES(FMMC_POWERUP_STRUCT &sPowerUp)
	
	sPowerUp.LocalPlayerPed = PLAYER_PED_ID()
	
	IF NOT DOES_ENTITY_EXIST(sPowerUp.viCurrentVeh)
	AND IS_PED_IN_ANY_VEHICLE(sPowerUp.LocalPlayerPed)
		IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(sPowerUp.LocalPlayerPed))
			sPowerUp.viCurrentVeh = GET_VEHICLE_PED_IS_IN(sPowerUp.LocalPlayerPed)
			PRINTLN("[Power Ups] Updating viCurrentVeh")
		ENDIF
	ENDIF
ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: End Clean Up
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC POWERUP_SYSTEM_CLEANUP(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
		EXIT
	ENDIF
	
	PRINTLN("[Power Ups] POWERUP_SYSTEM_CLEANUP")
	
	REMOVE_ROCKETS()
	CLEAR_VEHICLE_BOOST_COLLECTED()
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sPowerUp.siPowerUpHUD)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), PICKUP_VEHICLE_CUSTOM_SCRIPT, TRUE)
	ENDIF
	
	DISABLE_VEHICLE_MINES(TRUE)
	ENABLE_POWERUP_MINES(FALSE)
	SET_POWERUP_MINE_TYPE(-1)
	
	g_bPowerUpMineUsed = FALSE
	PRINTLN("[VEHICLE_MINES][Power Ups] POWERUP_SYSTEM_CLEANUP - g_bPowerUpMineUsed = FALSE")
	
	b_gAllowZForCarRockets = FALSE
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sPowerUp.ptfxAmped)
		STOP_PARTICLE_FX_LOOPED(sPowerUp.ptfxAmped)
	ENDIF
	
	RESET_PARTICLE_FX_OVERRIDE("exp_xs_mine_slick")
	SET_WEAPON_EFFECT_DURATION_MODIFIER(WEAPONTYPE_DLC_VEHICLE_MINE_SLICK, 1.0)
	
	REMOVE_NAMED_PTFX_ASSET("veh_xs_vehicle_mods")
	REMOVE_NAMED_PTFX_ASSET("scr_tn_slick")
	
	UNLOAD_POWERUPS_AUDIO()
	
	SET_PLAYER_USING_RC_TANK(FALSE)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Removing PowerUps
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_CLEANUP_FROM_CURRENT_POWERUP_STATE_FOR_REMOVAL(FMMC_POWERUP_STRUCT &sPowerUp)
	
	SWITCH sPowerUp.ePowerUpState
		CASE POWERUP_STATE_INIT
		CASE POWERUP_STATE_IDLE
		CASE POWERUP_STATE_RUNNING
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC REMOVE_POWERUP(FMMC_POWERUP_STRUCT &sPowerUp, INT iPowerUp)
	IF sPowerUp.eCurrentPowerUps[iPowerUp] != POWERUP_NONE
		sPowerUp.eCurrentPowerUps[iPowerUp] = POWERUP_NONE
		sPowerUp.iCurrentPowerUpsCount--
		IF sPowerUp.iCurrentPowerUpsCount < 0
			sPowerUp.iCurrentPowerUpsCount = 0
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ALL_POWERUPS(FMMC_POWERUP_STRUCT &sPowerUp)
	INT i
	FOR i = 0 TO GET_MAX_NUMBER_OF_POWERUPS()-1
		REMOVE_POWERUP(sPowerUp, i)
	ENDFOR
	SET_POWERUP_HUD_STATE(sPowerUp, POWERUP_HUD_STATE_CLEANUP)
	sPowerUp.iCurrentPowerUpIndex = -1
ENDPROC

PROC PROCESS_REMOVING_POWERUPS(FMMC_POWERUP_STRUCT &sPowerUp)
	
	BOOL bPlayerOK = IS_NET_PLAYER_OK(PLAYER_ID())
	
	IF NOT bPlayerOk
	AND sPowerUp.iCurrentPowerUpsCount > 0
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		SET_BIT(sPowerUp.iProcessingBitSet, ciPWP_Process_BS_Remove_Pickups)
		PRINTLN("PROCESS_REMOVING_POWERUPS - Local Player Just died clear powerups")
	ENDIF
	
	IF NOT IS_BIT_SET(sPowerUp.iProcessingBitSet, ciPWP_Process_BS_Remove_Pickups)
		EXIT
	ENDIF
	
	IF IS_CURRENT_POWERUP_A_TRANSFORM(sPowerUp)
		IF sPowerUp.ePowerUpState = POWERUP_STATE_RUNNING
		AND IS_NET_PLAYER_OK(PLAYER_ID())
			PRINTLN("[Power Ups] PROCESS_REMOVING_POWERUPS - Waiting on transform back")
			EXIT
		ENDIF
	ENDIF
	
	IF SHOULD_CLEANUP_FROM_CURRENT_POWERUP_STATE_FOR_REMOVAL(sPowerUp)
	AND sPowerUp.iCurrentPowerUpIndex != -1
		SET_POWERUP_STATE(sPowerUp, POWERUP_STATE_CLEANUP_EFFECT)
		PRINTLN("[Power Ups] PROCESS_REMOVING_POWERUPS - Moving to cleanup effect")
		EXIT
	ENDIF
	
	IF sPowerUp.ePowerUpState != POWERUP_STATE_NONE
		PRINTLN("[Power Ups] PROCESS_REMOVING_POWERUPS - Not in correct state yet")
		EXIT
	ENDIF
	
	REMOVE_ALL_POWERUPS(sPowerUp)
	
	CLEAR_HELP()
	
	CLEAR_BIT(sPowerUp.iProcessingBitSet, ciPWP_Process_BS_Remove_Pickups)
	
	PRINTLN("[Power Ups] PROCESS_REMOVING_POWERUPS - Removing all power ups completed")
	
ENDPROC

				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Debug
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD

PROC PROCESS_DEBUG(FMMC_POWERUP_STRUCT &sPowerUp)
	
	PRINTLN("Powerup running")
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD9, KEYBOARD_MODIFIER_CTRL, "")
		SET_BIT(sPowerUp.iProcessingBitSet, ciPWP_Process_BS_Remove_Pickups)
	ELIF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_CTRL, "")
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_BOMB)
	ELIF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD2, KEYBOARD_MODIFIER_CTRL, "")
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_MG_AMP)
	ELIF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD3, KEYBOARD_MODIFIER_CTRL, "")
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_MONSTER_TRUCK)
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_BOOST_SHUNT)
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_ROCKET)
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD3)
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_REPAIR)
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_GHOST)
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_TANK)
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_MINE_KINETIC)
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD7)
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_MINE_SPIKE)
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD8)
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_MINE_EMP)
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD9)
		FILL_EMPTY_POWERUP_SLOT(sPowerUp, POWERUP_MINE_SLICK)
	ENDIF
	
	TEXT_LABEL_63 tl63 = "Current Index: "
	tl63 += sPowerUp.iCurrentPowerUpIndex
	DRAW_DEBUG_TEXT_2D(tl63, <<0.7, 0.16, 0.0>>)
	
	tl63 = "Current Power: "
	IF sPowerUp.iCurrentPowerUpIndex = -1
		tl63 += "Invalid"
	ELSE
		tl63 += GET_POWERUP_NAME(sPowerUp.eCurrentPowerUps[sPowerUp.iCurrentPowerUpIndex])
	ENDIF
	DRAW_DEBUG_TEXT_2D(tl63, <<0.7, 0.18, 0.0>>)
	
	tl63 = "Slot 0: "
	tl63 += GET_POWERUP_NAME(sPowerUp.eCurrentPowerUps[0])
	DRAW_DEBUG_TEXT_2D(tl63, <<0.7, 0.20, 0.0>>)
	
	tl63 = "Slot 1: "
	tl63 += GET_POWERUP_NAME(sPowerUp.eCurrentPowerUps[1])
	DRAW_DEBUG_TEXT_2D(tl63, <<0.7, 0.22, 0.0>>)
	
	tl63 = "Current State: "
	tl63 += GET_STATE_NAME(sPowerUp)
	DRAW_DEBUG_TEXT_2D(tl63, <<0.7, 0.24, 0.0>>)
	
	tl63 = "Trigger Condition?: "
	tl63 += PICK_STRING(DOES_CURRENT_POWERUP_HAVE_A_TRIGGER_CONDITION(sPowerUp), "TRUE", "FALSE")
	DRAW_DEBUG_TEXT_2D(tl63, <<0.7, 0.26, 0.0>>)
	
	tl63 = "Triggered?: "
	tl63 += PICK_STRING(HAS_CURRENT_POWERUP_BEEN_TRIGGERED(sPowerUp), "TRUE", "FALSE")
	DRAW_DEBUG_TEXT_2D(tl63, <<0.7, 0.28, 0.0>>)
	
	tl63 = "HUD State: "
	tl63 += ENUM_TO_INT(GET_POWERUP_HUD_STATE(sPowerUp))
	DRAW_DEBUG_TEXT_2D(tl63, <<0.7, 0.30, 0.0>>)
	
	tl63 = "Bitset: "
	tl63 += sPowerUp.iPowerUpBitSet
	DRAW_DEBUG_TEXT_2D(tl63, <<0.7, 0.32, 0.0>>)
	
	tl63 = "g_bDisableVehicleMines: "
	tl63 += PICK_STRING(g_bDisableVehicleMines, "TRUE", "FALSE")
	DRAW_DEBUG_TEXT_2D(tl63, <<0.7, 0.34, 0.0>>)
		
	tl63 = "Swap State: "
	tl63 += ENUM_TO_INT(sPowerUp.sVehicleSwap.eVehicleSwapState)
	DRAW_DEBUG_TEXT_2D(tl63, <<0.7, 0.36, 0.0>>)
	
ENDPROC
#ENDIF	
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------			

PROC PROCESS_FMMC_POWERUPS(FMMC_POWERUP_STRUCT &sPowerUp)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
		EXIT
	ENDIF
	
	IF NOT HAVE_POWERUP_ASSETS_LOADED()
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PROCESS_DEBUG(sPowerUp)
	#ENDIF
	
	PROCESS_CACHING_CURRENT_ENTITIES(sPowerUp)
	PROCESS_BLOCKED_POWERUP_INPUTS(sPowerUp)
	PROCESS_CAN_LOCAL_PLAYER_COLLECT_PICKUPS(sPowerUp)
	
	PROCESS_REMOVING_POWERUPS(sPowerUp)
	
	CHANGE_POWERUP_CURRENT_SLOT(sPowerUp)
	
	SWITCH sPowerUp.ePowerUpState
		CASE POWERUP_STATE_NONE
			IF DOES_LOCAL_PLAYER_HAVE_ANY_POWERUPS(sPowerUp)
				
				SET_CURRENT_POWERUP_INDEX(sPowerUp, FIND_NEXT_FILLED_POWERUP_SLOT(sPowerUp))
				
				IF IS_POWERUP_SLOT_FILLED(sPowerUp)
					SET_POWERUP_STATE(sPowerUp, POWERUP_STATE_INIT)
				ENDIF
			ENDIF
		BREAK
		
		CASE POWERUP_STATE_INIT
		
			INIT_CURRENT_POWERUP(sPowerUp)
			
			IF IS_POWERUP_READY(sPowerUp)
				SET_POWERUP_STATE(sPowerUp, POWERUP_STATE_IDLE)
			ENDIF
		BREAK
		
		CASE POWERUP_STATE_IDLE
			
			IF NOT IS_POWERUP_STILL_READY(sPowerUp)
				SET_POWERUP_STATE(sPowerUp, POWERUP_STATE_INIT)
			ENDIF
			
			IF DOES_CURRENT_POWERUP_HAVE_A_TRIGGER_CONDITION(sPowerUp)
				IF HAS_POWERUP_HAD_TRIGGER_ACTIVATED(sPowerUp)
					PLAY_POWERUPS_USED_SOUND(GET_CURRENT_POWERUP(sPowerUp))
					BROADCAST_FMMC_POWERUP_SOUND("NPC_Use_Powerup_", GET_CURRENT_POWERUP(sPowerUp))
					SET_POWERUP_STATE(sPowerUp, POWERUP_STATE_RUNNING)
				ENDIF
			ELSE
				SET_POWERUP_STATE(sPowerUp, POWERUP_STATE_RUNNING)
			ENDIF
		BREAK
		
		CASE POWERUP_STATE_RUNNING
			
			PROCESS_CURRENT_POWERUP_EFFECT(sPowerUp)
			
			IF HAS_POWERUP_EFFECT_ENDED(sPowerUp)
				PLAY_POWERUPS_END_SOUND(GET_CURRENT_POWERUP(sPowerUp))
				BROADCAST_FMMC_POWERUP_SOUND("NPC_Powerup_Depleted_", GET_CURRENT_POWERUP(sPowerUp))
				SET_POWERUP_STATE(sPowerUp, POWERUP_STATE_CLEANUP_EFFECT)
			ENDIF
		BREAK
		
		CASE POWERUP_STATE_CLEANUP_EFFECT
			CLEANUP_CURRENT_POWERUP_EFFECT(sPowerUp, FALSE)
			SET_POWERUP_STATE(sPowerUp, POWERUP_STATE_CLEANUP)
		BREAK
		
		CASE POWERUP_STATE_CLEANUP
			CLEANUP_CURRENT_POWERUP_DATA(sPowerUp)
			SET_POWERUP_STATE(sPowerUp, POWERUP_STATE_NONE)
		BREAK
		
	ENDSWITCH
	
	sPowerUp.ePreState = sPowerUp.sVehicleSwap.eVehicleSwapState
	PROCESS_VEHICLE_SWAP(sPowerUp.sVehicleSwap, GET_MODEL_FOR_TANK_TRANSFORM(sPowerUp))
	PROCESS_POWERUP_TRANSFORM_VEHICLE_SETUP(sPowerUp)
	PROCESS_POWERUP_TRANSFORM_BACK_VEHICLE_SETUP(sPowerUp)
	PROCESS_POWERUP_TRANSFORM_ONE_FRAME_CLEANUP(sPowerUp)
	
ENDPROC
