USING "rage_builtins.sch"
USING "globals.sch"
USING "net_scoring_common.sch"
USING "shop_public.sch"
USING "freemode_header.sch"

CONST_INT HOLD_UP_TUT_STAGE_INIT			0
CONST_INT HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP	1
CONST_INT HOLD_UP_TUT_FRIEND_HELP			2
CONST_INT HOLD_UP_TUT_STAGE_DO_HOLD_UP		3
CONST_INT HOLD_UP_TUT_STAGE_DONE			99


CONST_INT SERVER_HOLD_UP_TUT_STAGE_WAIT_FOR_EVENT		0
CONST_INT SERVER_HOLD_UP_TUT_STAGE_FIND_PARTNERS		1
CONST_INT SERVER_HOLD_UP_TUT_STAGE_FIND_MEET_LOC		2

CONST_INT MAX_HOLD_UP_TUT_MEET_LOC			20

#IF IS_DEBUG_BUILD
	PROC PRINT_HOLD_UP_STRING(STRING sText, BOOL bIsServer = FALSE)
		NET_NL()
		NET_PRINT_TIME()
		IF bIsServer
			NET_PRINT(" [dsw] [HoldUpTut] [HoldUpTutServer] ")
		ELSE
			NET_PRINT(" [dsw] [HoldUpTut] [HoldUpTutClient] ")
		ENDIF
		NET_PRINT(sText)
		NET_NL()
	ENDPROC

	PROC PRINT_HOLD_UP_STRING_INT(STRING sText, INT iToPrint, BOOL bIsServer = FALSE)
		NET_NL()
		NET_PRINT_TIME()
		IF bIsServer
			NET_PRINT(" [dsw] [HoldUpTut] [HoldUpTutServer] ")
		ELSE
			NET_PRINT(" [dsw] [HoldUpTut] [HoldUpTutClient] ")
		ENDIF
		NET_PRINT(sText)
		NET_PRINT_INT(iToPrint)
		NET_NL()
	ENDPROC

	PROC PRINT_HOLD_UP_STRING_FLOAT(STRING sText, FLOAT fToPrint, BOOL bIsServer = FALSE)
		NET_NL()
		NET_PRINT_TIME()
		IF bIsServer
			NET_PRINT(" [dsw] [HoldUpTut] [HoldUpTutServer] ")
		ELSE
			NET_PRINT(" [dsw] [HoldUpTut] [HoldUpTutClient] ")
		ENDIF
		NET_PRINT(sText)
		NET_PRINT_FLOAT(fToPrint)
		NET_NL()
	ENDPROC
	
	PROC PRINT_HOLD_UP_STRING_VECTOR(STRING sText1, VECTOR v)
		NET_PRINT_TIME() NET_PRINT(" [dsw] [HoldUpTut] ") NET_PRINT_STRING_VECTOR(sText1, v) NET_NL()
	ENDPROC
	
	PROC PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player, BOOL bIsServer = FALSE)
		IF NOT bIsServer
			NET_PRINT_TIME() NET_PRINT(" [dsw] [HoldUpTut] [HoldUpTutClient]") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
		ELSE
			NET_PRINT_TIME() NET_PRINT(" [dsw] [HoldUpTut] [HoldUpTutServer] ") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
		ENDIF
	ENDPROC
#ENDIF

CONST_INT biL_HoldUp_GetToMeet		0
CONST_INT biL_HoldUp_MeetSolo		1
CONST_INT biL_HoldUp_TextWasCleared	2
CONST_INT biL_ToldToHoldUpStore		3
CONST_INT biL_DoHelpText			4
CONST_INT biL_SkipFriendHelpText	5
CONST_INT biL_DoStoreBlip			6
CONST_INT biL_MySToreNotValid		7
CONST_INT biL_PartCompleteEarly		8
CONST_INT biL_NeedToResetStore		9
CONST_INT biL_SToreReset			10
CONST_INT biL_ToldToLoseWanted		11
CONST_INT biL_DoKickTimer			12
CONST_INT biL_DoneAcceptInvHelp		13
CONST_INT biL_ToldToHelpPartnerLose	14
CONST_INT biL_TargetBuddyDisabled	15



STRUCT FM_PLAYER_HOLD_UP_TUT_STRUCT
	INT iHoldUpTutProg
	INT iHoldUpTutClientBitset
	INT iHelpTextProg
	INT iHoldUpInviteProg
	BLIP_INDEX blipHoldUpTutMeetLoc
	BLIP_INDEX blipMyHoldup
	SCRIPT_TIMER timeBroadcastReady
	SCRIPT_TIMER timeMaxWaitForPartner
	SCRIPT_TIMER timeGetToMeetLoc
	SCRIPT_TIMER timeRetryAfterKickForTimeout
	SCRIPT_TIMER timeInviteDelay
	SCRIPT_TIMER timeInvalidDelay
ENDSTRUCT


PROC BROADCAST_PLAYER_READY_FOR_HOLDUP_TUTORIAL()
	#IF IS_DEBUG_BUILD 
		PRINT_HOLD_UP_STRING_INT("[BROADCAST_PLAYER_READY_FOR_HOLDUP_TUTORIAL] Broadcasting ready for holdup tut. I am player... ", NATIVE_TO_INT(PLAYER_ID()))
	#ENDIF
	SCRIPT_EVENT_FM_PLAYER_READY_HOLDUP_TUT Event
	event.Details.Type = SCRIPT_EVENT_PLAYER_READY_FOR_HOLD_UP_TUT
	Event.details.FromPlayerIndex = PLAYER_ID()
	Event.iFrameCount = GET_FRAME_COUNT()
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
ENDPROC

PROC BROADCAST_START_HOLDUP_TUTORIAL()
	#IF IS_DEBUG_BUILD 
		PRINT_HOLD_UP_STRING("[BROADCAST_START_HOLDUP_TUTORIAL] Called... ")
	#ENDIF
	SCRIPT_EVENT_FM_LAUNCH_HOLD_UP_TUT Event
	event.Details.Type = SCRIPT_EVENT_PLAYER_START_HOLD_UP_TUT
	Event.details.FromPlayerIndex = PLAYER_ID()
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
ENDPROC





FUNC VECTOR GET_HOLD_UP_MEET_LOCATION(INT iHoldUp)
	VECTOR vLoc = <<0.0, 0.0, 0.0>>
	
	SWITCH iHoldUp
		CASE 0	vLoc = <<-52.27, -1733.58, 28.23>>	BREAK //  = eCRIM_HUP_GAS_3 vBlipLocation = <<-53.1240, -1756.4054, 28.4210>>
		CASE 1	vLoc = <<15.4693, -1354.0665, 28.3150>>		BREAK // = eCRIM_HUP_SHOP247_9 vBlipLocation = <<13, -1339.51, 30.06>>
		CASE 2	vLoc = <<-1238.39, -904.58, 13.27>>	BREAK // = eCRIM_HUP_LIQUOR_3 vBlipLocation = <<-1226.4644, -902.5864, 11.2783>>
		CASE 3	vLoc = <<-724.14, -912.37, 20.34 >>	BREAK // = eCRIM_HUP_GAS_2 vBlipLocation = <<-711.7210, -916.6965, 18.2145>>
		CASE 4	vLoc = <<1145.4330, -972.5638, 45.6656>>	BREAK // = eCRIM_HUP_LIQUOR_4 vBlipLocation =  <<1113.54, -979.2, 47.19>>
		CASE 5	vLoc = <<-1502.0375, -380.4863, 39.8563>>	BREAK // = eCRIM_HUP_LIQUOR_5 vBlipLocation = <<-1498.36, -377.02, 42.24>>
		CASE 6	vLoc = <<364.9397, 320.9199, 102.6601>>	BREAK //  = eCRIM_HUP_SHOP247_10 vBlipLocation = <<365.85, 326.85, 104.42>>
		CASE 7	vLoc = <<1162.6638, -338.0286, 67.4397>> 	BREAK // = eCRIM_HUP_GAS_4 vBlipLocation = <<1169.37, -309.28, 70.92>>
		CASE 8	vLoc = <<2567.1023, 384.5044, 107.4621>>	BREAK // = eCRIM_HUP_SHOP247_5 vBlipLocation = <<2543.79, 341.78, 109.52>>
		CASE 9	vLoc = <<-2970.0542, 401.0349, 14.0942>> 	BREAK //  = eCRIM_HUP_LIQUOR_2 vBlipLocation = <<-2951.12, 396.78, 15.84>>
		CASE 10	vLoc = 	<<-3042.5134, 599.3762, 6.5342>>BREAK // = eCRIM_HUP_SHOP247_2 vBlipLocation = <<-3052.82, 595.75, 9.47>>
		CASE 11	vLoc = <<-3252.98, 992.51, 13.5>>	BREAK // = eCRIM_HUP_SHOP247_3 vBlipLocation = <<-3240.3169, 1004.4334, 11.8307>>
		CASE 12	vLoc =  <<-1814.2256, 778.6277, 136.1750>>	BREAK // = eCRIM_HUP_GAS_5 vBlipLocation = <<-1814.52, 798.13, 138.45>>
		CASE 13	vLoc = 	 <<531.4801, 2675.5051, 41.4485>> BREAK // = eCRIM_HUP_SHOP247_4 vBlipLocation = <<561.09, 2660.6, 42.77>>
		CASE 14	vLoc = <<1183.1740, 2693.9419, 36.9294>>	BREAK // = eCRIM_HUP_LIQUOR_1 vBlipLocation = <<1159.66, 2729.14, 38.97>>
		CASE 15	vLoc =  <<1382.5553, 3598.1619, 33.8769>>	BREAK // = eCRIM_HUP_SHOP247_1 vBlipLocation = <<1384.12, 3600.79, 35.47>>
		CASE 16	vLoc = <<1959.9749, 3731.0251, 31.3247>>	BREAK // = eCRIM_HUP_SHOP247_8 vBlipLocation =  <<1966.76, 3757.33, 33.31>
		CASE 17	vLoc = <<2681.91, 3292.97, 55.49>>	BREAK // = eCRIM_HUP_SHOP247_6 vBlipLocation = <<2682.0034, 3282.5432, 54.2411>>
		CASE 18	vLoc = <<1687.9902, 4918.1738, 41.0781>>	BREAK // = eCRIM_HUP_GAS_1 vBlipLocation = <<1703.98, 4912.32, 42.23>>
		CASE 19	vLoc = <<1687.9902, 4918.1738, 41.0781>>	BREAK // = eCRIM_HUP_SHOP247_7 vBlipLocation =<<1746.02, 6411.14, 35.41>>
	ENDSWITCH
	
	RETURN vLoc
ENDFUNC

FUNC Crim_HOLD_UP_POINT GET_HOLD_UP_FROM_MEET_LOC(INT iMeetLoc)
	Crim_HOLD_UP_POINT iHoldUp = eCRIM_HUP_INVALID
	
	SWITCH iMeetLoc
		CASE 0	iHoldUp = eCRIM_HUP_GAS_3		BREAK
		CASE 1	iHoldUp = eCRIM_HUP_SHOP247_9	BREAK
		CASE 2	iHoldUp = eCRIM_HUP_LIQUOR_3	BREAK
		CASE 3	iHoldUp = eCRIM_HUP_GAS_2		BREAK
		CASE 4	iHoldUp = eCRIM_HUP_LIQUOR_4	BREAK
		CASE 5	iHoldUp = eCRIM_HUP_LIQUOR_5	BREAK
		CASE 6	iHoldUp = eCRIM_HUP_SHOP247_10	BREAK
		CASE 7	iHoldUp = eCRIM_HUP_GAS_4		BREAK
		CASE 8	iHoldUp = eCRIM_HUP_SHOP247_5	BREAK
		CASE 9	iHoldUp = eCRIM_HUP_LIQUOR_2	BREAK
		CASE 10	iHoldUp = eCRIM_HUP_SHOP247_2	BREAK
		CASE 11	iHoldUp = eCRIM_HUP_SHOP247_3	BREAK
		CASE 12	iHoldUp = eCRIM_HUP_GAS_5		BREAK
		CASE 13	iHoldUp = eCRIM_HUP_SHOP247_4	BREAK
		CASE 14	iHoldUp = eCRIM_HUP_LIQUOR_1	BREAK
		CASE 15	iHoldUp = eCRIM_HUP_SHOP247_1	BREAK
		CASE 16	iHoldUp = eCRIM_HUP_SHOP247_8	BREAK
		CASE 17	iHoldUp = eCRIM_HUP_SHOP247_6	BREAK
		CASE 18	iHoldUp = eCRIM_HUP_GAS_1		BREAK
		CASE 19	iHoldUp = eCRIM_HUP_SHOP247_7	BREAK
	ENDSWITCH
	
	RETURN iHoldUp
ENDFUNC

FUNC VECTOR GET_HOLD_UP_BLIP_LOCATION_FROM_MEET_LOC(INT iMeetLoc)
	VECTOR vLoc = << 0.0, 0.0, 0.0>>

	SWITCH iMeetLoc
		CASE 0	vLoc = <<-53.1240, -1756.4054, 28.4210>>					BREAK //  = eCRIM_HUP_GAS_3 vBlipLocation = 
		CASE 1	vLoc = <<27.0641, -1348.8127, 28.5036>>+<<2, -0.4, 0>>		BREAK // = eCRIM_HUP_SHOP247_9 vBlipLocation = 
		CASE 2	vLoc = <<-1226.4644, -902.5864, 11.2783>>					BREAK // = eCRIM_HUP_LIQUOR_3 vBlipLocation = 
		CASE 3	vLoc = <<-711.7210, -916.6965, 18.2145>>					BREAK // = eCRIM_HUP_GAS_2 vBlipLocation = 
		CASE 4	vLoc = <<1141.3596, -980.8802, 45.4155>>					BREAK // = eCRIM_HUP_LIQUOR_4 vBlipLocation = 
		CASE 5	vLoc = <<-1491.0565, -383.5728, 39.1706>>					BREAK // = eCRIM_HUP_LIQUOR_5 vBlipLocation = 
		CASE 6	vLoc = <<376.6533, 323.6471, 102.5664>>						BREAK //  = eCRIM_HUP_SHOP247_10 vBlipLocation = 
		CASE 7	vLoc = <<1159.5421, -326.6986, 67.9230>>					BREAK // = eCRIM_HUP_GAS_4 vBlipLocation = 
		CASE 8	vLoc = <<2559.2471, 385.5266, 107.6230>>					BREAK // = eCRIM_HUP_SHOP247_5 vBlipLocation = 
		CASE 9	vLoc = <<-2973.2617, 390.8184, 14.0433>>					BREAK //  = eCRIM_HUP_LIQUOR_2 vBlipLocation = 
		CASE 10	vLoc = <<-3038.9082, 589.5187, 6.9048>>						BREAK // = eCRIM_HUP_SHOP247_2 vBlipLocation = 
		CASE 11	vLoc = <<-3240.3169, 1004.4334, 11.8307>>					BREAK // = eCRIM_HUP_SHOP247_3 vBlipLocation = 
		CASE 12	vLoc = <<-1822.2866, 788.0060, 137.1859>>					BREAK // = eCRIM_HUP_GAS_5 vBlipLocation = 
		CASE 13	vLoc = <<544.2802, 2672.8113, 41.1566>>						BREAK // = eCRIM_HUP_SHOP247_4 vBlipLocation = 
		CASE 14	vLoc = <<1195.4320, 2703.1143, 37.1573>>+<<-29.04, 0.39, 0>>	BREAK // = eCRIM_HUP_LIQUOR_1 vBlipLocation =
		CASE 15	vLoc = <<1394.1692, 3599.8601, 34.0121>>					BREAK // = eCRIM_HUP_SHOP247_1 vBlipLocation = 
		CASE 16	vLoc = <<1965.0542, 3740.5552, 31.3448>>					BREAK // = eCRIM_HUP_SHOP247_8 vBlipLocation =
		CASE 17	vLoc = <<2682.0034, 3282.5432, 54.2411>>					BREAK // = eCRIM_HUP_SHOP247_6 vBlipLocation = 
		CASE 18	vLoc =<<1698.8085, 4929.1978, 41.0783>>						BREAK // = eCRIM_HUP_GAS_1 vBlipLocation = 
		CASE 19	vLoc = <<1731.2098, 6411.4033, 34.0372>>					BREAK // = eCRIM_HUP_SHOP247_7 vBlipLocation = 
	ENDSWITCH
	
	
	RETURN vLoc
ENDFUNC

/// PURPOSE:
///    Need to check if the hold up we're going to hasn't been done already recently
/// PARAMS:
///    iMeetLoc - 
/// RETURNS:
///    
FUNC BOOL IS_HOLD_UP_STILL_VALID(INT iMeetLoc #IF IS_DEBUG_BUILD, BOOL bDoDebug = FALSE #ENDIF)
	/*
		From Ryan:
		
		Check hold up is active
		AND IS_BIT_SET(HoldUpServerData.iHoldUpTimerDoneBitSet, HoldUpData.iHoldUpLoop) 
		AND NOT IS_BIT_SET(HoldUpServerData.iHoldUpDoneBitSet, HoldUpData.iHoldUpLoop)
	*/
	
	
	Crim_HOLD_UP_POINT holdUp = GET_HOLD_UP_FROM_MEET_LOC(iMeetLoc)
	IF holdUp <> eCRIM_HUP_INVALID
		IF IS_BIT_SET(GlobalServerBD_HoldUp.HoldUpServerData.iHoldUpTimerDoneBitSet, ENUM_TO_INT(holdUp)) 
			IF NOT IS_BIT_SET(GlobalServerBD_HoldUp.HoldUpServerData.iHoldUpDoneBitSet, ENUM_TO_INT(holdUp))
				#IF IS_DEBUG_BUILD
					IF bDoDebug
						PRINTLN("[dsw] [HoldUpTut] IS_HOLD_UP_STILL_VALID TRUE")
					ENDIF
				#ENDIF
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
					IF bDoDebug
						PRINTLN("[dsw] [HoldUpTut] IS_HOLD_UP_STILL_VALID false because  set")
					ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF bDoDebug
					PRINTLN("[dsw] [HoldUpTut] IS_HOLD_UP_STILL_VALID false because GlobalServerBD_HoldUp.HoldUpServerData.iHoldUpTimerDoneBitSet, ENUM_TO_INT(holdUp) Not set")
				ENDIF
			#ENDIF
		ENDIF 
	ELSE	
		#IF IS_DEBUG_BUILD
			IF bDoDebug
				PRINTLN("[dsw] [HoldUpTut] IS_HOLD_UP_STILL_VALID false because holdUp = eCRIM_HUP_INVALID")
			ENDIF
		#ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC



FUNC BOOL IS_MY_PARTNER_STILL_VALID(INT iPartner, BOOL bCheckCompleted = TRUE #IF IS_DEBUG_BUILD , BOOL bShowDebug = FALSE #ENDIF)
	PLAYER_INDEX playerPartner
	playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iPartner)
	
	/*
	IF NOT NETWORK_IS_PLAYER_ACTIVE(playerPartner)
		#IF IS_DEBUG_BUILD 
			IF bShowDebug
				PRINT_HOLD_UP_STRING("[MAINTAIN_HOLD_UP_TUT_CLIENT_PARTNER_CHECKS] Clearing biL_HoldUp_GetToMeet becuase my partner is no longer active")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	*/
	
	IF NOT IS_NET_PLAYER_OK(playerPartner, FALSE)
		#IF IS_DEBUG_BUILD 
			IF bShowDebug
				PRINT_HOLD_UP_STRING("[IS_MY_PARTNER_STILL_VALID] Clearing biL_HoldUp_GetToMeet becuase my partner is not ok")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF bCheckCompleted
		IF IS_BIT_SET(GlobalplayerBD_FM[iPartner].iHoldUpTutBitset, biHT_CompletedHoldUpTut)
			#IF IS_DEBUG_BUILD 
				IF bShowDebug
					PRINT_HOLD_UP_STRING("[IS_MY_PARTNER_STILL_VALID] Clearing biL_HoldUp_GetToMeet becuase my partner has completed holdup tut") 
				ENDIF
			#ENDIF
			RETURN FALSE
			
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[iPartner].iHoldUpTutBitset, biHT_DoingHoldUpSolo)
		#IF IS_DEBUG_BUILD 
			
			PRINT_HOLD_UP_STRING("[IS_MY_PARTNER_STILL_VALID] FAlse as my partner is doing hold up solo") 
			
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[iPartner].iHoldUpTutBitset, biHT_OnMission)
		#IF IS_DEBUG_BUILD 
			IF bShowDebug
				PRINT_HOLD_UP_STRING("[IS_MY_PARTNER_STILL_VALID] Clearing biL_HoldUp_GetToMeet becuase my partner is on a mission") 
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[iPartner].iHoldUpTutBitset, biHT_TookTooLongToGetToHoldUpLoc)
		#IF IS_DEBUG_BUILD 
			IF bShowDebug
				PRINT_HOLD_UP_STRING("[IS_MY_PARTNER_STILL_VALID] False because my partner took too long to get to hold up") 
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL SET_MY_HOLD_UP_TUT_PARTNER_CAN_TARGET_ME(INT iPartner, BOOL bSet)
	#IF IS_DEBUG_BUILD
		PRINT_HOLD_UP_STRING("[SET_MY_HOLD_UP_TUT_PARTNER_CAN_TARGET_ME] called with... ")
		IF bSet
			PRINT_HOLD_UP_STRING("[SET_MY_HOLD_UP_TUT_PARTNER_CAN_TARGET_ME] True ")
		ELSE
			PRINT_HOLD_UP_STRING("[SET_MY_HOLD_UP_TUT_PARTNER_CAN_TARGET_ME] False ")
		ENDIF
	#ENDIF
	
	
	IF iPartner > -1
		PLAYER_INDEX playerPartner = INT_TO_NATIVE(PLAYER_INDEX, iPartner)
		IF NETWORK_IS_PLAYER_ACTIVE(playerPartner)
			IF IS_NET_PLAYER_OK(playerPartner, FALSE)
				SET_PED_CAN_BE_TARGETTED_BY_PLAYER(PLAYER_PED_ID(), playerPartner , bSet)
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[SET_MY_HOLD_UP_TUT_PARTNER_CAN_TARGET_ME] Set ok ") #ENDIF
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[SET_MY_HOLD_UP_TUT_PARTNER_CAN_TARGET_ME] partner not ok ") #ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[SET_MY_HOLD_UP_TUT_PARTNER_CAN_TARGET_ME] partner not active ") #ENDIF
		ENDIF
	ELSE	
		#IF IS_DEBUG_BUILD PRINT_HOLD_UP_STRING("[SET_MY_HOLD_UP_TUT_PARTNER_CAN_TARGET_ME]iMyPartner = -1 ") #ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC CLEAR_HOLD_UP_TUT_OBJECTIVE_TEXT(FM_PLAYER_HOLD_UP_TUT_STRUCT &holdUpTut)
	IF Is_This_The_Current_Objective_Text("FM_HTUT_GOP")
		Clear_This_Objective_Text("FM_HTUT_GOP")
		
	ENDIF
	
	IF Is_This_The_Current_Objective_Text("FM_HTUT_GO")
		Clear_This_Objective_Text("FM_HTUT_GO")
		
	ENDIF
	
	IF Is_This_The_Current_Objective_Text("FM_HTUT_WAT")
		Clear_This_Objective_Text("FM_HTUT_WAT")
		
	ENDIF
	IF Is_This_The_Current_Objective_Text("FM_HTUT_HLD")
		Clear_This_Objective_Text("FM_HTUT_HLD")
		
	ENDIF
	IF Is_This_The_Current_Objective_Text("FM_HTUT_HLS")
		Clear_This_Objective_Text("FM_HTUT_HLS")
		
	ENDIF
	IF DOES_BLIP_EXIST(holdUpTut.blipHoldUpTutMeetLoc)
		REMOVE_BLIP(holdUpTut.blipHoldUpTutMeetLoc)
	ENDIF
ENDPROC


PROC DRAW_HOLD_UP_KICK_PLAYER_TIMER( INT iMaxTime, SCRIPT_TIMER timeKick)
/*
	timeWaitForOtherRacers
*/
	IF HAS_NET_TIMER_STARTED(timeKick)	
		INT ms = (iMaxTime - GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),timeKick.Timer))
		IF ms >= 0
			
			DRAW_GENERIC_TIMER(ms, "FM_HTUT_TME")
									
		ENDIF
	ELSE
//		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING("[DRAW_PLAYER_WAIT_COUNT_DOWN_TIMER] Timer not started!")#ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_NEARBY_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL(FLOAT fMinDistance = 25.0)
	INT i
	PLAYER_INDEX player
	VECTOR vMyCoords
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vMyCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		REPEAT NUM_NETWORK_PLAYERS i
			player = INT_TO_PLAYERINDEX(i)
			IF IS_NET_PLAYER_OK(player)
				IF player <> PLAYER_ID()
					IF IS_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL(player)
						IF VDIST2(vMyCoords, GET_ENTITY_COORDS(GET_PLAYER_PED(player))) < fMinDistance * fMinDistance
							#IF IS_DEBUG_BUILD
								PRINT_HOLD_UP_STRING_WITH_PLAYER_NAME("[IS_ANY_NEARBY_PLAYER_ABOUT_TO_START_HOLD_UP_TUTORIAL] This player about to start hold up tutorial... ", player)
							#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC





