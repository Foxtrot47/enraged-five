//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_hideout.sch																				//
// Description: Controls the Lost/Vagos text for the Steal Package Variation of Hideout.					//
// Written by:  Ryan Baker																					//
// Date: 24/01/2013																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_mission.sch"
USING "freemode_header.sch"
USING "FMMC_HEADER.sch"

CONST_INT MAX_NUM_DESTRUCTIBLE_CRATES			5
CONST_INT MAX_NUM_CASH_PICKUPS					2	//4
CONST_INT MAX_NUM_AMMO_PICKUPS					2	//3	//INCLUDES BONUS CASH PICKUP
//CONST_INT MAX_NUM_DRUG_PICKUPS				4
CONST_INT HIDEOUT_MAX_NUM_WEAPON_PICKUPS		2	//3	//INCLUDES BONUS CASH PICKUP
CONST_INT MAX_NUM_VEH							5
CONST_INT GA_MAX_NUM_PROPS						10

CONST_INT TIMES_TO_MOVE_UP				5

CONST_INT AMMO_SMALL			50
CONST_INT AMMO_MEDIUM			100
CONST_INT AMMO_LARGE			200

//CONST_INT BONUS_CASH_AMOUNT		500		//REPLACED WITH g_sMPTunables.icash_modifier_crate_drop_gang_attack

CONST_INT iBS_DC_CrateDestroyed			0
CONST_INT iBS_DC_CratePickupsMoveUp		1

STRUCT DESTRUCTIBLE_CRATE_DATA
	NETWORK_INDEX NetID
	MODEL_NAMES Model = Prop_Box_Wood02A_PU
	VECTOR vCoord
	FLOAT fHeading
	INT iBitSet
	INT iContents = ciCRATE_CONTENTS_CASH
	PICKUP_TYPE ContentsWeapon = PICKUP_WEAPON_ADVANCEDRIFLE
	INT iContentsAmount = 500
	SAP_PICKUP_ID Pickup[MAX_NUM_CASH_PICKUPS+1]
	INT iMoveCounter
ENDSTRUCT

CONST_INT iBS_GAP_Valid		0	//to 
CONST_INT iBS_GAP_Launch	31

STRUCT GANG_ATTACK_PICKUPS_DATA
	MP_MISSION idMission = eAM_GA_PICKUPS
	INT iInstanceId
	
	VECTOR vGangAttackLocation
	
	INT iBitSet
	VECTOR vCoord[MAX_NUM_DESTRUCTIBLE_CRATES]
	FLOAT fHeading[MAX_NUM_DESTRUCTIBLE_CRATES]
	INT iContents[MAX_NUM_DESTRUCTIBLE_CRATES] 					//= ciCRATE_CONTENTS_CASH
	PICKUP_TYPE ContentsWeapon[MAX_NUM_DESTRUCTIBLE_CRATES] 	//= PICKUP_WEAPON_ADVANCEDRIFLE
	INT iContentsAmount[MAX_NUM_DESTRUCTIBLE_CRATES] 			// = 500
ENDSTRUCT

