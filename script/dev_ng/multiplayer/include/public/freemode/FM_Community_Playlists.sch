/// FM_Community_Playlists
/// Bobby - 23/07/2013 - 09:58
/// functions for dealing with a community playlist

USING "globals.sch"
USING "MP_globals_FM.sch"
USING "commands_socialclub.sch"
USING "net_transition_sessions.sch"
USING "FMMC_Cloud_loader.sch"
USING "Transition_Invites.sch"

//reset the playlist community event vars
PROC RESET_SC_COMMUNITY_PLAYLIST_VARS()
	PRINTLN("SC PLAYLIST - RESET_SC_COMMUNITY_PLAYLIST_VARS")
	SC_COMMUNITY_PLAYLIST_VARS g_sScComunityPlaylistTemp
	g_sScComunityPlaylist = g_sScComunityPlaylistTemp
ENDPROC


//Set that we need to display Y on the way into MP
PROC SET_NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION()
	#IF IS_DEBUG_BUILD
	IF NOT g_sScComunityPlaylist.bDisplayY
		PRINTLN("SC PLAYLIST - SET_NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION")	
	ENDIF
	#ENDIF
	g_sScComunityPlaylist.bDisplayY = TRUE
ENDPROC
//Clear that we need to display Y on the way into MP
PROC CLEAR_NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION()
	#IF IS_DEBUG_BUILD
	IF g_sScComunityPlaylist.bDisplayY
	PRINTLN("SC PLAYLIST - CLEAR_NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION")	
	ENDIF
	#ENDIF
	g_sScComunityPlaylist.bDisplayY = FALSE
ENDPROC
//Returns if we need to display Y on the way into MP
FUNC BOOL NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION()
	RETURN g_sScComunityPlaylist.bDisplayY 
ENDFUNC
//returns true if it's a head to head tournament
FUNC BOOL IS_THIS_TOURNAMENT_A_CREW_HEAD_TO_HEAD()
	RETURN g_sScComunityPlaylist.sPlayListVars[ciEVENT_TOURNAMENT_PLAYLIST].iEventSubType = ciTOURNAMENT_EVENT_SUBTYPES_CREW
ENDFUNC
//returns true if it's a final round tournament
FUNC BOOL IS_THIS_TOURNAMENT_A_FINAL_ROUND()
	RETURN g_sScComunityPlaylist.sPlayListVars[ciEVENT_TOURNAMENT_PLAYLIST].iEventSubType = ciTOURNAMENT_EVENT_SUBTYPES_FINAL_ROUND
ENDFUNC

//returns true if it's a head to head tournament
FUNC BOOL IS_THIS_SCTV_CONTROLLED_PLAYLIST(INT iType = 0)
	IF iType != -1
		RETURN g_sScComunityPlaylist.sPlayListVars[iType].iEventSubType = ciTOURNAMENT_EVENT_SUBTYPES_SCTV_CONTROLLED
	ENDIF
	RETURN FALSE
ENDFUNC

//get the const int for debug print reasons
#IF IS_DEBUG_BUILD
FUNC STRING GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(INT iType)
	SWITCH iType
		CASE ciQUALIFING_TOURNAMENT_PLAYLIST		RETURN "ciQUALIFING_TOURNAMENT_PLAYLIST"
		CASE ciEVENT_TOURNAMENT_PLAYLIST			RETURN "ciEVENT_TOURNAMENT_PLAYLIST"
		CASE ciEVENT_COMMUNITY_PLAYLIST_LIVE		RETURN "ciEVENT_COMMUNITY_PLAYLIST"
		CASE ciEVENT_COMMUNITY_PLAYLIST_SWEEP		RETURN "ciEVENT_COMMUNITY_PLAYLIST_SWEEP"
		CASE ciEVENT_COMMUNITY_PLAYLIST_FEATURED	RETURN "ciEVENT_COMMUNITY_PLAYLIST_FEATURED"	
		#IF FEATURE_COPS_N_CROOKS
		CASE ciEVENT_COPS_AND_CROOKS				RETURN "ciEVENT_COPS_AND_CROOKS"
		#ENDIF
	ENDSWITCH
	RETURN ""
ENDFUNC
#ENDIF

PROC SET_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE(INT iType)
	PRINTLN("SC PLAYLIST - SET_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE - iType = ", iType, " = ", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType))	
	PRINTLN("[TS] - SET_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE - iType = ", iType, " = ", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType))	
	g_sScComunityPlaylist.iActiveEvent = iType
ENDPROC
PROC CLEAR_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE()
	PRINTLN("SC PLAYLIST - CLEAR_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE")	
	PRINTLN("[TS] - CLEAR_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE")	
	g_sScComunityPlaylist.iActiveEvent = -1
ENDPROC

FUNC INT GET_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE()
	RETURN g_sScComunityPlaylist.iActiveEvent
ENDFUNC


PROC SET_SC_COMMUNITY_PLAYLIST_LAUNCH_TYPE(INT iType)
	PRINTLN("SC PLAYLIST - SET_SC_COMMUNITY_PLAYLIST_LAUNCH_TYPE - iType = ", iType, " = ", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType))	
	PRINTLN("[TS] - SET_SC_COMMUNITY_PLAYLIST_LAUNCH_TYPE - iType = ", iType, " = ", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType))	
	g_sScComunityPlaylist.iEventToLaunch = iType
ENDPROC
FUNC INT GET_SC_COMMUNITY_PLAYLIST_LAUNCH_TYPE()
	RETURN g_sScComunityPlaylist.iEventToLaunch
ENDFUNC
//Set a SC community playlist is launching
PROC SET_SC_COMMUNITY_PLAYLIST_LAUNCHING_BY_TYPE(INT iType)
	PRINTLN("SC PLAYLIST - SET_SC_COMMUNITY_PLAYLIST_LAUNCHING_BY_TYPE - iType = ", iType, " = ", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType))	
	PRINTLN("[TS] - SET_SC_COMMUNITY_PLAYLIST_LAUNCHING_BY_TYPE - iType = ", iType, " = ", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType))	
	g_sScComunityPlaylist.sPlayListVars[iType].bLaunchActive = TRUE
ENDPROC
//Clear a QUALIFYING playlist is launching
PROC CLEAR_SC_COMMUNITY_PLAYLIST_LAUNCHING_BY_TYPE(INT iType)
	PRINTLN("SC PLAYLIST - CLEAR_SC_COMMUNITY_PLAYLIST_LAUNCHING_BY_TYPE - iType = ", iType, " = ", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType))	
	PRINTLN("[TS] - CLEAR_SC_COMMUNITY_PLAYLIST_LAUNCHING_BY_TYPE - iType = ", iType, " = ", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType))	
	g_sScComunityPlaylist.sPlayListVars[iType].bLaunchActive = FALSE
ENDPROC

//Get the text label of the playlist to play
FUNC TEXT_LABEL_23 GET_SC_COMMUNITY_PLAYLIST_CONTENT_ID_BY_TYPE(INT iType)
	PRINTLN("SC GET_SC_COMMUNITY_PLAYLIST_CONTENT_ID_BY_TYPE - g_sScComunityPlaylist.sPlayListVars[iType].tl23PlaylistContentID = ", g_sScComunityPlaylist.sPlayListVars[iType].tl23PlaylistContentID, " = ", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType))
	RETURN g_sScComunityPlaylist.sPlayListVars[iType].tl23PlaylistContentID
ENDFUNC
//Clear that the playlist should launch when entering FM
PROC CLEAR_SC_COMMUNITY_PLAYLIST_LAUNCH_WHEN_ENTERING_FM_BY_TYPE(INT iType)
	PRINTLN("SC PLAYLIST - CLEAR_SC_COMMUNITY_PLAYLIST_LAUNCH_WHEN_ENTERING_FM_BY_TYPE - iType = ", iType, " = ", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType))
	g_sScComunityPlaylist.sPlayListVars[iType].bLaunchWhenEnteringFM = FALSE
ENDPROC
//Should the SC community playlist launch when entering FM
FUNC BOOL SHOULD_SC_COMMUNITY_PLAYLIST_LAUNCH_WHEN_ENTERING_FM(INT iType)
	RETURN g_sScComunityPlaylist.sPlayListVars[iType].bLaunchWhenEnteringFM
ENDFUNC

//Returns the count down time by type
FUNC INT GET_EVENT_PLAYLIST_CORONA_COUNT_DOWN_TIME_BY_TYPE(INT iType)
	RETURN g_sScComunityPlaylist.sPlayListVars[iType].iCoronaLaunchTime
ENDFUNC
//Returns if there is a community playlist event active
FUNC INT GET_TOURNAMENT_EVENT_PLAYLIST_CORONA_COUNT_DOWN_TIME()
	RETURN g_sScComunityPlaylist.sPlayListVars[ciEVENT_TOURNAMENT_PLAYLIST].iCoronaLaunchTime
ENDFUNC

//Returns if there is a community playlist event active
FUNC BOOL IS_EVENT_PLAYLIST_ACTIVE_BY_TYPE(INT iType)
	RETURN g_sScComunityPlaylist.sPlayListVars[iType].bActive
ENDFUNC
FUNC BOOL IS_SC_COMMUNITY_PLAYLIST_ACTIVE()
	RETURN g_sScComunityPlaylist.bAnEventIsActive
ENDFUNC

PROC CLEAR_ANY_EVENT_PLAYLIST_LAUNCHING()
	PRINTLN("SC PLAYLIST - CLEAR_ANY_EVENT_PLAYLIST_LAUNCHING")
	g_sScComunityPlaylist.bAnEventIsLaunching = FALSE
ENDPROC
PROC SET_ANY_EVENT_PLAYLIST_LAUNCHING()
	PRINTLN("SC PLAYLIST - SET_ANY_EVENT_PLAYLIST_LAUNCHING")
	g_sScComunityPlaylist.bAnEventIsLaunching = TRUE
ENDPROC

PROC CLEAR_DONT_CLEAN_UP_COMUNITY_PLAYLIST_VARS_EXITING_FMMC_LAUNCHER()
	PRINTLN("SC PLAYLIST - CLEAR_DONT_CLEAN_UP_COMUNITY_PLAYLIST_VARS_EXITING_FMMC_LAUNCHER")
	g_sScComunityPlaylist.bDontClearExitingFmmcLauncher = FALSE
ENDPROC
PROC SET_DONT_CLEAN_UP_COMUNITY_PLAYLIST_VARS_EXITING_FMMC_LAUNCHER()
	PRINTLN("SC PLAYLIST - SET_DONT_CLEAN_UP_COMUNITY_PLAYLIST_VARS_EXITING_FMMC_LAUNCHER")
	g_sScComunityPlaylist.bDontClearExitingFmmcLauncher = TRUE
ENDPROC
FUNC BOOL DONT_CLEAN_UP_COMUNITY_PLAYLIST_VARS_EXITING_FMMC_LAUNCHER()
	RETURN g_sScComunityPlaylist.bDontClearExitingFmmcLauncher
ENDFUNC





//Clean up launching an event
PROC CLEAR_LAUNCHING_SC_PLAYLIST_EVENT()
	PRINTLN("SC PLAYLIST - CLEAR_LAUNCHING_SC_PLAYLIST_EVENT")
	//If we are launching a Qualifying playlist then clean it up
	IF IS_ANY_EVENT_PLAYLIST_LAUNCHING()
		CLEAR_ANY_EVENT_PLAYLIST_LAUNCHING()
	ENDIF
	//Clear that the comunity playlist should launch when coming in to FM
	INT iLoop
	REPEAT ciMAX_NUM_ACTIVE_COMUNITY_PLAYLISTS_TO_CHECK iLoop
		IF SHOULD_SC_COMMUNITY_PLAYLIST_LAUNCH_WHEN_ENTERING_FM(iLoop)
			CLEAR_SC_COMMUNITY_PLAYLIST_LAUNCH_WHEN_ENTERING_FM_BY_TYPE(iLoop)
		ENDIF
	ENDREPEAT
ENDPROC


//Get the event that was triggered from launch
FUNC INT GET_TYPE_BASED_ON_TYPE_PRIORITY()
	INT iRepeat
	REPEAT ciMAX_NUM_ACTIVE_COMUNITY_PLAYLISTS_TO_CHECK iRepeat
		IF IS_EVENT_PLAYLIST_ACTIVE_BY_TYPE(iRepeat)
		AND iRepeat != ciEVENT_TOURNAMENT_PLAYLIST
			RETURN iRepeat
		ENDIF
	ENDREPEAT
	RETURN ciQUALIFING_TOURNAMENT_PLAYLIST
ENDFUNC

//Set up a comunity playlist
PROC SET_UP_SC_PLAYLIST_BY_TYPE(INT iType, INT eventId)
	BOOL bActive = TRUE
	TEXT_LABEL_63 tlReturn
	INT iReturn
	//Now that we have the event ID, grab the value ‘contentId’ object from the extraData on the event.
	IF SC_COMMUNITY_EVENT_GET_EXTRA_DATA_STRING_BY_ID(eventId, "contentId", tlReturn)
		g_sScComunityPlaylist.sPlayListVars[iType].tl23PlaylistContentID = tlReturn 
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - SC_COMMUNITY_EVENT_GET_EXTRA_DATA_STRING(\"contentId\", g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].tl23PlaylistContentID) = ", g_sScComunityPlaylist.sPlayListVars[iType].tl23PlaylistContentID)
	#IF NOT FEATURE_COPS_N_CROOKS
	ELSE
	#ENDIF
	#IF FEATURE_COPS_N_CROOKS
	ELIF iType != ciEVENT_COPS_AND_CROOKS
	#ENDIF
		bActive = FALSE
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - SC_COMMUNITY_EVENT_GET_EXTRA_DATA_STRING = FALSE")
	ENDIF
	//get the playlist pretty name
	IF SC_COMMUNITY_EVENT_GET_EXTRA_DATA_STRING_BY_ID(eventId, "playlist", tlReturn)
		g_sScComunityPlaylist.sPlayListVars[iType].tl31PlaylistName = tlReturn 
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - SC_COMMUNITY_EVENT_GET_EXTRA_DATA_STRING(\"playlist\", g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].tl31PlaylistName) = ", g_sScComunityPlaylist.sPlayListVars[iType].tl31PlaylistName)
	#IF NOT FEATURE_COPS_N_CROOKS
	ELSE
	#ENDIF
	#IF FEATURE_COPS_N_CROOKS
	ELIF iType != ciEVENT_COPS_AND_CROOKS
	#ENDIF
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - SC_COMMUNITY_EVENT_GET_EXTRA_DATA_STRING = FALSE")
		bActive = FALSE
	ENDIF
	//Get the even prettier display name?
	IF SC_COMMUNITY_EVENT_GET_DISPLAY_NAME_BY_ID(eventId, tlReturn)
		g_sScComunityPlaylist.sPlayListVars[iType].tl63EventName = tlReturn 
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - SC_COMMUNITY_EVENT_GET_DISPLAY_NAME(g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].tl63EventName) = ", g_sScComunityPlaylist.sPlayListVars[iType].tl63EventName)
	ELSE
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - SC_COMMUNITY_EVENT_GET_DISPLAY_NAME = FALSE")
		bActive = FALSE
	ENDIF	
	//If it's a tournament
	IF iType = ciEVENT_TOURNAMENT_PLAYLIST	
		//Get the corona count down time
		IF SC_COMMUNITY_EVENT_GET_EXTRA_DATA_INT_BY_ID(eventId, "coronaCountdown", iReturn)
			g_sScComunityPlaylist.sPlayListVars[iType].iCoronaLaunchTime = iReturn 
			PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - SC_COMMUNITY_EVENT_GET_EXTRA_DATA_STRING(\"coronaCountdown\", g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].iCoronaLaunchTime) = ", g_sScComunityPlaylist.sPlayListVars[iType].iCoronaLaunchTime)
		//There's not one, then fall back to two minutes
		ELSE
			g_sScComunityPlaylist.sPlayListVars[iType].iCoronaLaunchTime = 120000
		ENDIF
		g_sScComunityPlaylist.sPlayListVars[iType].iEventSubType = ciTOURNAMENT_EVENT_SUBTYPES_STANDARD
		//Get the corona count down time
		IF SC_COMMUNITY_EVENT_GET_EXTRA_DATA_INT_BY_ID(eventId, "eventSubType", iReturn)
			g_sScComunityPlaylist.sPlayListVars[iType].iEventSubType = iReturn
			PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - SC_COMMUNITY_EVENT_GET_EXTRA_DATA_STRING(\"eventSubType\", g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].iEventSubType) = ", g_sScComunityPlaylist.sPlayListVars[iType].iEventSubType)
		ENDIF
		#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_forceCrewTournament") 
				g_sScComunityPlaylist.sPlayListVars[iType].iEventSubType = ciTOURNAMENT_EVENT_SUBTYPES_CREW
			ENDIF
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_forceFinalRoundTournament") 
				g_sScComunityPlaylist.sPlayListVars[iType].iEventSubType = ciTOURNAMENT_EVENT_SUBTYPES_FINAL_ROUND
			ENDIF
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_forceSctvControlledEvent") 
				g_sScComunityPlaylist.sPlayListVars[iType].iEventSubType = ciTOURNAMENT_EVENT_SUBTYPES_SCTV_CONTROLLED
			ENDIF
		#ENDIF
	ENDIF
	
	//If it's all there then set it as active and print it all out
	IF bActive
		//Set than at least one event is active, if' it's not a tournamen
		IF iType != ciEVENT_TOURNAMENT_PLAYLIST
			g_sScComunityPlaylist.bAnEventIsActive = TRUE
		ENDIF
		//Set that this type is active
		g_sScComunityPlaylist.sPlayListVars[iType].bActive = TRUE
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].iEventID              = ", g_sScComunityPlaylist.sPlayListVars[iType].iEventID)		
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].tl63EventName         = ", g_sScComunityPlaylist.sPlayListVars[iType].tl63EventName)		
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].tl31PlaylistName      = ", g_sScComunityPlaylist.sPlayListVars[iType].tl31PlaylistName)		
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].tl23PlaylistContentID = ", g_sScComunityPlaylist.sPlayListVars[iType].tl23PlaylistContentID)
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].bActive               = ", g_sScComunityPlaylist.sPlayListVars[iType].bActive)	
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].iCoronaLaunchTime     = ", g_sScComunityPlaylist.sPlayListVars[iType].iCoronaLaunchTime)	
		PRINTLN("SC PLAYLIST - SET_UP_SC_PLAYLIST_BY_TYPE - g_sScComunityPlaylist.sPlayListVars[", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iType), "].iEventSubType         = ", g_sScComunityPlaylist.sPlayListVars[iType].iEventSubType)	
	ELSE
		g_sScComunityPlaylist.sPlayListVars[iType].bActive = FALSE
	ENDIF
	
ENDPROC

//Returns the types of events that I know about
FUNC STRING GET_SC_PLAYLIST_EVENT_TYPE(INT iType)
	SWITCH iType
		CASE ciQUALIFING_TOURNAMENT_PLAYLIST		RETURN "TournamentQualifying"
		CASE ciEVENT_TOURNAMENT_PLAYLIST			RETURN "Tournament"
		CASE ciEVENT_COMMUNITY_PLAYLIST_LIVE		RETURN "LiveStreaming"
		CASE ciEVENT_COMMUNITY_PLAYLIST_SWEEP		RETURN "Sweepstakes"
		CASE ciEVENT_COMMUNITY_PLAYLIST_FEATURED	RETURN "FeaturedPlaylist"
		#IF FEATURE_COPS_N_CROOKS
		CASE ciEVENT_COPS_AND_CROOKS				RETURN "CopsAndCrooks"
		#ENDIF
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_LEADER_BOARD_ENVENT_TYPE_STRING()
	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		IF IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
			RETURN GET_SC_PLAYLIST_EVENT_TYPE(ciEVENT_TOURNAMENT_PLAYLIST)
		ELIF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
			RETURN GET_SC_PLAYLIST_EVENT_TYPE(ciQUALIFING_TOURNAMENT_PLAYLIST)
		ELIF IS_THIS_TRANSITION_SESSION_IS_A_LIVESTREAM_PLAYLIST()
			RETURN GET_SC_PLAYLIST_EVENT_TYPE(ciEVENT_COMMUNITY_PLAYLIST_LIVE)
		ELIF IS_SC_COMMUNITY_PLAYLIST_LAUNCHING(ciEVENT_COMMUNITY_PLAYLIST_SWEEP)
			RETURN GET_SC_PLAYLIST_EVENT_TYPE(ciEVENT_COMMUNITY_PLAYLIST_SWEEP)
		ELIF IS_SC_COMMUNITY_PLAYLIST_LAUNCHING(ciEVENT_COMMUNITY_PLAYLIST_FEATURED)
			RETURN GET_SC_PLAYLIST_EVENT_TYPE(ciEVENT_COMMUNITY_PLAYLIST_FEATURED)
		#IF FEATURE_COPS_N_CROOKS
		ELIF IS_SC_COMMUNITY_PLAYLIST_LAUNCHING(ciEVENT_COPS_AND_CROOKS)
			RETURN GET_SC_PLAYLIST_EVENT_TYPE(ciEVENT_COPS_AND_CROOKS)
		#ENDIF
		ENDIF	
	ENDIF
	RETURN ""
ENDFUNC

FUNC BOOL IS_FEATURED_PLAYLIST_ACTIVE()
	RETURN SC_COMMUNITY_EVENT_GET_EVENT_ID_FOR_TYPE(GET_SC_PLAYLIST_EVENT_TYPE(ciEVENT_COMMUNITY_PLAYLIST_FEATURED)) > 0
ENDFUNC

///Checks to see if there is a comunity event active    
PROC CHECK_SC_COMMUNITY_PLAYLIST_ACTIVE()
	INT iLoop
	INT eventId
	PRINTLN("SC PLAYLIST - CHECK_SC_COMMUNITY_PLAYLIST_ACTIVE")
	//Flatten old
	RESET_SC_COMMUNITY_PLAYLIST_VARS()
	//If we;re conneted
	IF SC_COMMUNITY_EVENT_IS_ACTIVE()
	AND NOT IS_CLOUD_DOWN_CLOUD_LOADER()
		
		PRINTLN("SC PLAYLIST - CHECK_SC_COMMUNITY_PLAYLIST_ACTIVE - SC_COMMUNITY_EVENT_IS_ACTIVE = TRUE")
		//Loop through all the type that I know about
		REPEAT ciMAX_NUM_ACTIVE_COMUNITY_PLAYLISTS_TO_CHECK iLoop
			//Get the event ID 
			eventId = SC_COMMUNITY_EVENT_GET_EVENT_ID_FOR_TYPE(GET_SC_PLAYLIST_EVENT_TYPE(iLoop))
			PRINTLN("SC PLAYLIST - CHECK_SC_COMMUNITY_PLAYLIST_ACTIVE - ", iLoop, " = ", GET_SC_PLAYLIST_EVENT_TYPE_PRETTY_NAME(iLoop), " eventId = ", eventId)
			//If we've got an ID more than 0
			IF eventId > 0 
				//Set it up
				g_sScComunityPlaylist.sPlayListVars[iLoop].iEventID = eventId
				SET_UP_SC_PLAYLIST_BY_TYPE(iLoop, eventId)
			ENDIF
		ENDREPEAT
	//Not connected the clean up
	ELSE
		PRINTLN("SC PLAYLIST - CHECK_SC_COMMUNITY_PLAYLIST_ACTIVE - SC_COMMUNITY_EVENT_IS_ACTIVE = FALSE")
		REPEAT ciMAX_NUM_ACTIVE_COMUNITY_PLAYLISTS_TO_CHECK iLoop
			g_sScComunityPlaylist.sPlayListVars[iLoop].bActive = FALSE
		ENDREPEAT
	ENDIF	
ENDPROC


//Set up the joining comunity playlist vars
PROC SETUP_LAUNCHING_COMMUNITY_PLAYLIST_VARS(INT iType, BOOL bPauseMenuRequest = FALSE) 
	PRINTLN("SC PLAYLIST - SETUP_LAUNCHING_COMMUNITY_PLAYLIST_VARS")
	
	//If it's not an activity session
	IF NOT NETWORK_IS_ACTIVITY_SESSION()	
		//If this is a pause menu request
		IF bPauseMenuRequest	
			PRINTLN("SC PLAYLIST - SETUP_LAUNCHING_COMMUNITY_PLAYLIST_VARS - bPauseMenuRequest = TRUE")
			//Set that I need to warp to the start of the mission
			SET_TRANSITION_SESSIONS_NEED_TO_WARP_TO_START_SKYCAM()
			
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_CAN_BE_TARGETTED)
			//Set freemode controller state
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
			PRINTLN("BWW... SET_FM_LAUNCHER_PLAYER_GAME_STATE - FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION") 
			CLEAR_MY_TRANSITION_SESSION_CONTENT_ID()		
			CLEAR_PAUSE_MENU_IS_USING_UGC()
		ENDIF
		SET_SC_COMMUNITY_PLAYLIST_LAUNCH_TYPE(iType)
		SET_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE(iType)
		
		//Call all the magical quick match functions!
		//starting a quickmatch
		SET_TRANSITION_SESSIONS_STARTING_QUICK_MATCH()
		//doing the type of playlist
		SET_TRANSITION_SESSIONS_DOING_QUICK_MATCH_PLAYLIST()
		//Set that we are launching
		SET_ANY_EVENT_PLAYLIST_LAUNCHING()
	ENDIF
	
	TEXT_LABEL_23 tlPlaylist
	//Get the content ID
	tlPlaylist = GET_SC_COMMUNITY_PLAYLIST_CONTENT_ID_BY_TYPE(iType)		
	//Set that a playlist is being launched. 
	SET_SC_COMMUNITY_PLAYLIST_LAUNCHING_BY_TYPE(iType)		
	//Set the content ID
	SET_MY_TRANSITION_SESSION_CONTENT_ID(tlPlaylist)	
ENDPROC


//If the player accepts to start a play list then we nee to call this to set the sc community playlist to launch when entering FM
PROC SET_SC_COMMUNITY_PLAYLIST_LAUNCH_WHEN_ENTERING_FM(INT iType, BOOL bInitialBoot = FALSE)
	IF IS_EVENT_PLAYLIST_ACTIVE_BY_TYPE(iType)
		PRINTLN("SC PLAYLIST - SET_SC_COMMUNITY_PLAYLIST_LAUNCH_WHEN_ENTERING_FM - iType = ", iType)
		//Set the flag
		g_sScComunityPlaylist.sPlayListVars[iType].bLaunchWhenEnteringFM = TRUE	
		//Set up the vars
		SETUP_LAUNCHING_COMMUNITY_PLAYLIST_VARS(iType)
		//Set that an event is launching
		SET_ANY_EVENT_PLAYLIST_LAUNCHING()
		//If it's from initial boot
		IF bInitialBoot
			g_sScComunityPlaylist.bLaunchedFromBoot = TRUE
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("SC PLAYLIST - SET_SC_COMMUNITY_PLAYLIST_LAUNCH_WHEN_ENTERING_FM - IS_SC_COMMUNITY_PLAYLIST_ACTIVE = FALSE - iType = ", iType)
	#ENDIF	
	ENDIF	
ENDPROC
//Passed rank Check
PROC SET_PASSED_SC_PLAYLIST_RANK_CHECK()
	PRINTLN("SC PLAYLIST - SET_PASSED_SC_PLAYLIST_RANK_CHECK")
	g_sScComunityPlaylist.bPassedRankCheck = TRUE
ENDPROC
PROC CLEAR_PASSED_SC_PLAYLIST_RANK_CHECK()
	PRINTLN("SC PLAYLIST - CLEAR_PASSED_SC_PLAYLIST_RANK_CHECK")
	g_sScComunityPlaylist.bPassedRankCheck = FALSE
ENDPROC 
FUNC BOOL PASSED_SC_PLAYLIST_RANK_CHECK()	
	RETURN g_sScComunityPlaylist.bPassedRankCheck
ENDFUNC
//failed rank check
PROC SET_FAILED_SC_PLAYLIST_RANK_CHECK()
	PRINTLN("SC PLAYLIST - SET_FAILED_SC_PLAYLIST_RANK_CHECK")
	g_sScComunityPlaylist.bFailedRankCheck = TRUE
ENDPROC
PROC CLEAR_FAILED_SC_PLAYLIST_RANK_CHECK()
	PRINTLN("SC PLAYLIST - CLEAR_FAILED_SC_PLAYLIST_RANK_CHECK")
	g_sScComunityPlaylist.bFailedRankCheck = FALSE
ENDPROC 
FUNC BOOL FAILED_SC_PLAYLIST_RANK_CHECK()	
	RETURN g_sScComunityPlaylist.bFailedRankCheck
ENDFUNC

//Check to see if a player is of high enough rank and the stats have loaded
FUNC BOOL IS_PLAYER_HIGH_ENOUGH_RANK_AND_HAVE_STATS_LOADED(BOOL bSetFailFlag)
	//Have the important stats loaded
	IF NOT HAS_IMPORTANT_STATS_LOADED() 
		RETURN FALSE
	ENDIF
	//And they are of high enough rank
	IF ((GET_FM_RANK_FROM_XP_VALUE(GET_STAT_CHARACTER_FM_XP()) >= 1) OR GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_NOTUT_DONE))
	//and if they've done the tutorials. 
	AND GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FRONT_END_JIP_UNLOCKED)
		SET_PASSED_SC_PLAYLIST_RANK_CHECK()
		RETURN TRUE
	//Set that we failed the rank check
	ELSE
		IF bSetFailFlag
			SET_FAILED_SC_PLAYLIST_RANK_CHECK()
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_TOURNAMENT_INVITE()
	IF ARE_TUTORIAL_INVITES_ARE_BLOCKED()
	OR NOT ARE_PROFILE_SETTINGS_TUTORIALS_DONE()
	OR NOT ALREADY_CREATED_GANG_MEMBER_IN_SLOT_TRANSITION()
	OR NOT HAS_GTAO_BEEN_PURCHASED()
	#IF NOT FEATURE_GEN9_STANDALONE
	OR NOT IS_SP_PROLOGUE_FINISHED()
	#ENDIF
	OR IS_ON_POLICY_SCREEN()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


//Check to see if the invite should be canceled. 
FUNC BOOL SHOULD_CANCEL_EVENT_LAUNCH_BECAUSE_OF_RANK_OR_CHEATER(BOOL bSkipLaunchCheck = FALSE)
	//If we should skip the launch check
	IF bSkipLaunchCheck
	//Or there is an event launching
	OR IS_ANY_EVENT_PLAYLIST_LAUNCHING()
		//And they are linked to social club
//		IF NOT IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE) 
//			PRINTLN("SC PLAYLIST - SHOULD_CANCEL_EVENT_LAUNCH_BECAUSE_OF_RANK_OR_CHEATER - IS_GAME_LINKED_TO_SOCIAL_CLUB = FALSE")
//			RETURN TRUE
//		ENDIF
		//Check to see if a player is of high enough rank and the stats have loaded
		IF NOT IS_PLAYER_HIGH_ENOUGH_RANK_AND_HAVE_STATS_LOADED(TRUE) 
			PRINTLN("SC PLAYLIST - SHOULD_CANCEL_EVENT_LAUNCH_BECAUSE_OF_RANK_OR_CHEATER - IS_PLAYER_HIGH_ENOUGH_RANK_AND_HAVE_STATS_LOADED = FALSE")
			RETURN TRUE
		ENDIF
		IF NOT PASSED_SC_PLAYLIST_RANK_CHECK()
			PRINTLN("SC PLAYLIST - SHOULD_CANCEL_EVENT_LAUNCH_BECAUSE_OF_RANK_OR_CHEATER - PASSED_SC_PLAYLIST_RANK_CHECK = FALSE")
			RETURN TRUE
		ENDIF
		//if they are a cheater then clean up
		IF NETWORK_PLAYER_IS_CHEATER()
			PRINTLN("SC PLAYLIST - SHOULD_CANCEL_EVENT_LAUNCH_BECAUSE_OF_RANK_OR_CHEATER - NETWORK_PLAYER_IS_CHEATER = TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//Deal with the auto acceptance of an invite
PROC SET_UP_NETWORK_ACCEPT_PRESENCE_INVITE(INT inviteIndex)
	PRINTLN("SC PLAYLIST - SET_UP_NETWORK_ACCEPT_PRESENCE_INVITE")
	CHECK_SC_COMMUNITY_PLAYLIST_ACTIVE()
	//If there is a tournament event active
	IF IS_EVENT_PLAYLIST_ACTIVE_BY_TYPE(ciEVENT_TOURNAMENT_PLAYLIST)
		//If we are allowed to accept the invite
		IF NOT SHOULD_BLOCK_TOURNAMENT_INVITE()
			//If we are allowed to launch this tournament event
			IF NOT SHOULD_CANCEL_EVENT_LAUNCH_BECAUSE_OF_RANK_OR_CHEATER(TRUE)
				PRINTLN("SC PLAYLIST - SET_UP_NETWORK_ACCEPT_PRESENCE_INVITE - IS_EVENT_PLAYLIST_ACTIVE_BY_TYPE(ciEVENT_TOURNAMENT_PLAYLIST) = TRUE")
				IF NETWORK_IS_ACTIVITY_SESSION()
					SET_TRANSITION_SESSION_ACCEPTING_TOURNAMENT_INVITE_ON_A_JOB()
				ENDIF
				SET_SC_COMMUNITY_PLAYLIST_LAUNCH_WHEN_ENTERING_FM(ciEVENT_TOURNAMENT_PLAYLIST)							
				NETWORK_ACCEPT_PRESENCE_INVITE(inviteIndex)	
				SET_DONT_CLEAN_UP_COMUNITY_PLAYLIST_VARS_EXITING_FMMC_LAUNCHER()
				SET_SC_COMMUNITY_PLAYLIST_BEING_INVITED_TO_TOURNAMENT_PLAYLIST()					
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("SC PLAYLIST - SET_UP_NETWORK_ACCEPT_PRESENCE_INVITE - SHOULD_CANCEL_EVENT_LAUNCH_BECAUSE_OF_RANK_OR_CHEATER TRUE")
				#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("SC PLAYLIST - SET_UP_NETWORK_ACCEPT_PRESENCE_INVITE - SHOULD_BLOCK_TOURNAMENT_INVITE = FALSE")
			#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("SC PLAYLIST - SET_UP_NETWORK_ACCEPT_PRESENCE_INVITE - IS_EVENT_PLAYLIST_ACTIVE_BY_TYPE(ciEVENT_TOURNAMENT_PLAYLIST) = FALSE")
		#ENDIF
	ENDIF
ENDPROC



//Convets an EVENT ID to a TYPE
FUNC BOOL GET_EVENT_TYPE_FROM_ID(INT &iReturn, INT iEventID)
	INT iLoop
	REPEAT ciMAX_NUM_ACTIVE_COMUNITY_PLAYLISTS_TO_CHECK iLoop
		IF g_sScComunityPlaylist.sPlayListVars[iLoop].iEventID = iEventID	
			iReturn = iLoop
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC


//deal with cleaning up the event launching to low rank or a cheater
PROC MAINTAIN_EVENT_LAUNCHING_CLEAN_UP()
	PRINTLN("SC PLAYLIST - MAINTAIN_EVENT_LAUNCHING_CLEAN_UP")
	//Check if it should be cleaned up
	IF SHOULD_CANCEL_EVENT_LAUNCH_BECAUSE_OF_RANK_OR_CHEATER()
		//clean it up if it should
		PRINTLN("SC PLAYLIST - MAINTAIN_EVENT_LAUNCHING_CLEAN_UP - SHOULD_CANCEL_EVENT_LAUNCH_BECAUSE_OF_RANK_OR_CHEATER = TRUE")
		RESET_SC_COMMUNITY_PLAYLIST_VARS()
		CLEAR_TRANSITION_SESSIONS_QUICK_MATCH_TYPE()
		CLEAR_TRANSITION_SESSIONS_STARTING_QUICK_MATCH()
	ENDIF
ENDPROC


//Controll launching the playlist during the main transition
PROC MAINTAIN_LAUNCHING_COMMUNITY_PLAYLIST_DURING_TRANSITION()

	//if we're signed in online. 
	IF NETWORK_IS_SIGNED_ONLINE()
		//If it's active
		IF IS_SC_COMMUNITY_PLAYLIST_ACTIVE()
		//If it's not already active
		AND NOT IS_ANY_EVENT_PLAYLIST_LAUNCHING()
		//And the camera is still in the sky
		AND ((GET_SKYSWOOP_STAGE() = SKYSWOOP_INSKYSTATIC) OR (GET_SKYSWOOP_STAGE() = SKYSWOOP_GOINGUP) #IF IS_DEBUG_BUILD OR GET_COMMANDLINE_PARAM_EXISTS("sc_holdOnPlaylistScreen") #ENDIF)
		//And they are linked to social club
		AND IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE) 
		//Check to see if a player is of high enough rank and the stats have loaded
		AND (IS_PLAYER_HIGH_ENOUGH_RANK_AND_HAVE_STATS_LOADED(FALSE) OR PASSED_SC_PLAYLIST_RANK_CHECK())
		//And I'm not accepting an invite
		AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_SP()
		AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
		//And the player is not a cheater.
		AND NOT NETWORK_PLAYER_IS_CHEATER()
	//	//if they are not a bad sport then clean up
	//	AND NOT NETWORK_PLAYER_IS_BADSPORT()
			BOOL bDisplayY = FALSE
			INT iReturn, iEventID
			//Check to see if a SC playlist news item is on screen
			IF SC_TRANSITION_NEWS_HAS_EXTRA_DATA_TU()
				IF SC_TRANSITION_NEWS_GET_EXTRA_DATA_INT_TU("eventId", iEventID)
					bDisplayY = TRUE
				ENDIF
			ENDIF
			
			//If we need to display set that we need to
			IF bDisplayY
			#IF IS_DEBUG_BUILD
			OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForcePressYScPlaylist")
			#ENDIF
				SET_NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION()
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
					PRINTLN("SC PLAYLIST - MAINTAIN_LAUNCHING_COMMUNITY_PLAYLIST_DURING_TRANSITION - Player pressed Y so setting - SET_QUALIFYING_PLAYLIST_LAUNCHING")
					PRINTLN("SC PLAYLIST - MAINTAIN_LAUNCHING_COMMUNITY_PLAYLIST_DURING_TRANSITION - iEventID = ", iEventID)
					//If it's one of the special types:
					IF GET_EVENT_TYPE_FROM_ID(iReturn, iEventID)
						SET_SC_COMMUNITY_PLAYLIST_LAUNCH_WHEN_ENTERING_FM(iReturn)
						CLEAR_NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION()
					//It's not one of the special types then clean set it up:
					ELSE
						SET_UP_SC_PLAYLIST_BY_TYPE(ciEMPTY_SLOT_ACTIVE_COMUNITY_PLAYLISTS, iEventID)
						SET_SC_COMMUNITY_PLAYLIST_LAUNCH_WHEN_ENTERING_FM(ciEMPTY_SLOT_ACTIVE_COMUNITY_PLAYLISTS)
						CLEAR_NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION()
					ENDIF
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForcePressYScPlaylist")
						IF TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL = GET_CURRENT_TRANSITION_STATE()
							TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_MAINTRANSITION)
						ENDIF
					ENDIF
					#ENDIF
				ENDIF
				
			//Don't need to the set that we dont need to
			ELSE
				CLEAR_NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION()
			ENDIF
			
		//We are launching allready so don't need to display
		ELSE
			#IF IS_DEBUG_BUILD
			IF (GET_GAME_TIMER() % 3000) < 50 
				//PRINTLN("GET_COMMANDLINE_PARAM_EXISTS(sc_holdOnPlaylistScreen)                                       = ",GET_COMMANDLINE_PARAM_EXISTS("sc_holdOnPlaylistScreen"))
				PRINTLN("IS_SC_COMMUNITY_PLAYLIST_ACTIVE()                                                           = ",IS_SC_COMMUNITY_PLAYLIST_ACTIVE())
				PRINTLN("IS_ANY_EVENT_PLAYLIST_LAUNCHING()                                                           = ",IS_ANY_EVENT_PLAYLIST_LAUNCHING())
				PRINTLN("((GET_SKYSWOOP_STAGE() = SKYSWOOP_INSKYSTATIC) OR (GET_SKYSWOOP_STAGE() = SKYSWOOP_GOINGUP) = ",((GET_SKYSWOOP_STAGE() = SKYSWOOP_INSKYSTATIC) OR (GET_SKYSWOOP_STAGE() = SKYSWOOP_GOINGUP) OR GET_COMMANDLINE_PARAM_EXISTS("sc_holdOnPlaylistScreen")))
				PRINTLN("IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE)                                                  = ",IS_GAME_LINKED_TO_SOCIAL_CLUB(FALSE, TRUE) )
				PRINTLN("(IS_PLAYER_HIGH_ENOUGH_RANK_AND_HAVE_STATS_LOADED() OR PASSED_SC_PLAYLIST_RANK_CHECK())     = ",(IS_PLAYER_HIGH_ENOUGH_RANK_AND_HAVE_STATS_LOADED(FALSE) OR PASSED_SC_PLAYLIST_RANK_CHECK()))
				PRINTLN("NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_SP()                                     = ",NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_SP())
				PRINTLN("NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()                                             = ",NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE())
				PRINTLN("NOT NETWORK_PLAYER_IS_CHEATER()                                                             = ",NOT NETWORK_PLAYER_IS_CHEATER())
			ENDIF 
			#ENDIF
			CLEAR_NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION()
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForcePressYScPlaylist")
			IF TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL = GET_CURRENT_TRANSITION_STATE()
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_MAINTRANSITION)
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
ENDPROC

//Displays the passed/failed ticker messages for the tournament qualifying playlists. 
PROC MAINTAIN_TORUNAMENT_FEED_MESSAGES()
	
	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		EXIT
	ENDIF
	
	//If we failed to set a time
	IF g_sQualifyTourFeed.bFailedToSetTime 
		//And it is safe to print a ticker
		IF NOT IS_TRANSITION_ACTIVE() 
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS() 
		AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
			//Then print away
			PRINT_TICKER("FM_TOURQUL_N")//You failed to record a time for every Job in the Tournament Qualification Playlist. Restart the Playlist to try again. You can continue to retry until the qualification period is over.
			PRINTLN("SC PLAYLIST - MAINTAIN_TORUNAMENT_FEED_MESSAGES - PRINT_TICKER(FM_TOURQUL_N)")
			g_sQualifyTourFeed.bFailedToSetTime = FALSE
			PRINTLN("SC PLAYLIST - MAINTAIN_TORUNAMENT_FEED_MESSAGES - g_sQualifyTourFeed.bFailedToSetTime = FALSE")
		ENDIF
		
	//If we failed set a time
	ELIF g_sQualifyTourFeed.bSetTime = TRUE
		//If the time is more than 0
		IF g_sQualifyTourFeed.iTotalTime > 0		
			//And it is safe to print a ticker
			IF NOT IS_TRANSITION_ACTIVE() 
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS() 
			AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
				//Then print away
				 BEGIN_TEXT_COMMAND_THEFEED_POST("FM_TOURQUL_Y") //You successfully recorded a total time of ~a~. All players that qualify for the Tournament will be contacted by Rockstar once the qualification period is over.
	       			ADD_TEXT_COMPONENT_SUBSTRING_TIME(g_sQualifyTourFeed.iTotalTime, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)              
	  			END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)  
				PRINTLN("SC PLAYLIST - MAINTAIN_TORUNAMENT_FEED_MESSAGES - PRINT_TICKER(FM_TOURQUL_Y), g_sQualifyTourFeed.iTotalTime = ", g_sQualifyTourFeed.iTotalTime)
				g_sQualifyTourFeed.bSetTime = FALSE
				g_sQualifyTourFeed.iTotalTime = 0
				PRINTLN("SC PLAYLIST - MAINTAIN_TORUNAMENT_FEED_MESSAGES - g_sQualifyTourFeed.bSetTime   = FALSE")
				PRINTLN("SC PLAYLIST - MAINTAIN_TORUNAMENT_FEED_MESSAGES - g_sQualifyTourFeed.iTotalTime = 0")
			ENDIF
		ELSE
			//The time is 0 so clean up the boot
			g_sQualifyTourFeed.bSetTime = FALSE			
			PRINTLN("SC PLAYLIST - MAINTAIN_TORUNAMENT_FEED_MESSAGES - g_sQualifyTourFeed.iTotalTime = 0 AND g_sQualifyTourFeed.bSetTime = TRUE")
			SCRIPT_ASSERT("SC PLAYLIST - MAINTAIN_TORUNAMENT_FEED_MESSAGES - g_sQualifyTourFeed.iTotalTime = 0")
			PRINTLN("SC PLAYLIST - MAINTAIN_TORUNAMENT_FEED_MESSAGES - g_sQualifyTourFeed.bSetTime   = FALSE")
		ENDIF
	ENDIF
ENDPROC



