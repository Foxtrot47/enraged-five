USING "globals.sch"
USING "net_prints.sch"
USING "net_gang_boss_common.sch"
USING "net_include.sch"
USING "net_gang_boss_drops.sch"
USING "net_gang_boss_limo.sch"
USING "leader_board_common.sch"
USING "net_spectator_cam.sch"
USING "net_spectator_cam_common.sch"
USING "net_gang_boss_common.sch"
USING "net_gang_boss.sch"


CONST_INT ciGB_SPEC_BS_FADE 			0
CONST_INT ciGB_SPEC_BS_DO_MENU_HELP 	1
CONST_INT ciGB_SPEC_BS_DONE_CLEAR 		2
CONST_INT ciGB_SPEC_BS_DONE_FADE 		3

CONST_INT ciGB_SPEC_STAGE_INT		0
CONST_INT ciGB_SPEC_STAGE_MAINTAIN 	1
CONST_INT ciGB_SPEC_STAGE_WARP 		2

STRUCT GB_MAINTAIN_SPECTATE_VARS
	INT iSpecStage
	INT iQuitSpecStage
	INT iBitSet
	INT iStartWantedLevel
	//INT iGbSpecContext = NEW_CONTEXT_INTENTION
	SCRIPT_TIMER stFocusTargetDelay
	VECTOR vInitialLocation
	FLOAT fInitialHeading
	PLAYER_SPAWN_LOCATION spawnLocation = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS 
	
	INT iCleanupFadeOutTime = 500
ENDSTRUCT

		
PROC GB_TIDYUP_SPECTATOR_CAM()
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_TIDYUP_SPECTATOR_CAM")
	DEBUG_PRINTCALLSTACK()

	CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_IS_SPECTATING)
	ENABLE_WEAPON_SWAP()
	Enable_MP_Comms()
	FORCE_CLEANUP_SPECTATOR_CAM(g_BossSpecData)
	SET_RADAR_ZOOM(0)
	DISPLAY_RADAR(TRUE)
	UNLOCK_MINIMAP_ANGLE()
	UNLOCK_MINIMAP_POSITION()
	SET_NOISEOVERIDE(FALSE)
	SET_NOISINESSOVERIDE(0.0)
	DISPLAY_CASH(TRUE)
	ENABLE_MOVIE_SUBTITLES(FALSE)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	ENDIF
	DEACTIVATE_SPECTATOR_CAM(g_BossSpecData)
	CLEANUP_SPECTATOR_CAM_HUD(g_BossSpecData)
	FORCE_CLEANUP_SPECTATOR_CAM(g_BossSpecData)
	CLEANUP_HEIST_SPECTATE_FOR_NON_HEISTS()
	GB_CLEAR_RUN_SPEC_CAM()
	IF IS_BIT_SET(MPGlobalsAmbience.iGbSpecCamGlobalBitSet, ciGB_SPEC_BS_GLOBAL_PI_MENU)
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] ENABLE_INTERACTION_MENU")
		ENABLE_INTERACTION_MENU()
	ENDIF
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_BIT_SET(MPGlobalsAmbience.iGbSpecCamGlobalBitSet, ciGB_SPEC_BS_GLOBAL_ENABLE_CONTROL)
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] NET_SET_PLAYER_CONTROL")
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iGbSpecCamGlobalBitSet, ciGB_SPEC_BS_GLOBAL_ENABLE_KILL_ONES_SELF)
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] ENABLE_KILL_YOURSELF_OPTION")
		ENABLE_KILL_YOURSELF_OPTION()
	ENDIF
	
	//Set that we're spectating
	GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_BOSS_USING_SPECTATE)
	
	MPGlobalsAmbience.iGbSpecCamGlobalBitSet = 0
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_TIDYUP_SPECTATOR_CAM - Done")
	
ENDPROC


PROC GB_CLIENT_MAINTAIN_EVERY_FRAME_CALLS(BOOL bHideWantedStars = TRUE)
	DISABLE_DPADDOWN_THIS_FRAME()
	
	DISPLAY_AMMO_THIS_FRAME(FALSE)			
	HUD_FORCE_WEAPON_WHEEL(FALSE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		DISABLE_SPECTATOR_CONTROL_ACTIONS_GENERAL(g_BossSpecData)
	ENDIF
	
	DISABLE_SELECTOR_THIS_FRAME()
	
	DISPLAY_RADAR(FALSE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
	IF bHideWantedStars
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	ENDIF
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
	
	HIDE_HUDMARKERS_THIS_FRAME()
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME(TRUE)
	
	SET_IDLE_KICK_DISABLED_THIS_FRAME()
ENDPROC

PROC GB_MAINTAIN_SPECTATE_MOVE_TO_CLEAN_UP(GB_MAINTAIN_SPECTATE_VARS &sSpecVarsPassed, BOOL bSpawnAtSpecLocation)
	IF bSpawnAtSpecLocation
	AND NOT g_sMPTunables.bgb_pointtopoint_disable_warp_to_end
		sSpecVarsPassed.vInitialLocation = GET_FINAL_RENDERED_CAM_COORD()
		VECTOR vTemp = GET_FINAL_RENDERED_CAM_ROT()
		sSpecVarsPassed.fInitialHeading = vTemp.z
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] sSpecVarsPassed.vInitialLocation = ", sSpecVarsPassed.vInitialLocation)
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] sSpecVarsPassed.fInitialHeading = ", sSpecVarsPassed.fInitialHeading)
	ENDIF
	
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_BIKER_CONTRABAND_DEFEND
	AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_FMBB_DEFEND
		IF NOT IS_VECTOR_ZERO(sSpecVarsPassed.vInitialLocation)
			SETUP_SPECIFIC_SPAWN_LOCATION(sSpecVarsPassed.vInitialLocation, sSpecVarsPassed.fInitialHeading)
			sSpecVarsPassed.spawnLocation = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] SPAWN_LOCATION_NEAR_SPECIFIC_COORDS")
		ENDIF
	ELSE
		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAREST_POLICE_STATION)
		sSpecVarsPassed.spawnLocation = SPAWN_LOCATION_NEAREST_POLICE_STATION
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] SPAWN_LOCATION_NEAREST_POLICE_STATION")
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState != RESPAWN_STATE_PLAYING
		SET_MANUAL_RESPAWN_STATE(MRS_NULL)
		RESET_GAME_STATE_ON_DEATH()
		FORCE_RESPAWN(TRUE)
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_MAINTAIN_SPECTATE_MOVE_TO_CLEAN_UP")
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_FINAL_RENDERED_CAM_COORD())
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_MAINTAIN_SPECTATE_MOVE_TO_CLEAN_UP, skip SET_ENTITY_COORDS as player will be positioned by the respawn controller. ")
	ENDIF
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] CLEAR_FOCUS")
	DO_SCREEN_FADE_OUT(sSpecVarsPassed.iCleanupFadeOutTime)
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] DO_SCREEN_FADE_OUT")

	sSpecVarsPassed.iSpecStage = ciGB_SPEC_STAGE_WARP
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] iSpecStage = ciGB_SPEC_STAGE_WARP")
ENDPROC

//if we are launching straight into specatator camera this will return TRUE once it's active. 
FUNC BOOL GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM_IS_ACTIVE(GB_MAINTAIN_SPECTATE_VARS &sSpecVarsPassed)
	IF GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM()
	AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
		AND IS_BIT_SET(sSpecVarsPassed.iBitSet, ciGB_SPEC_BS_DONE_CLEAR)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC 
FUNC BOOL GB_MANAGE_QUITTING_SPECTATE(GB_MAINTAIN_SPECTATE_VARS &sSpecVarsPassed)
	IF GB_SHOULD_CLOSE_SPECTATE()
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_CHALLENGE(PLAYER_ID())
	AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GB_CHAL_FIVESTAR
	AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_BIKER_CONTRABAND_DEFEND
		IF NOT GB_ARE_THERE_ENOUGH_GOONS_FOR_SPECTAGE(FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT CAN_SPECTATOR_QUIT()
		RETURN FALSE
	ENDIF
	
	CONST_INT ciGB_QUIT_SPECTATE_STAGE_INT 		0
	CONST_INT ciGB_QUIT_SPECTATE_STAGE_NO_PRESS 1
	CONST_INT ciGB_QUIT_SPECTATE_STAGE_WARNING 	2
	CONST_INT ciGB_QUIT_SPECTATE_STAGE_YES		3
	
		SWITCH sSpecVarsPassed.iQuitSpecStage
			CASE ciGB_QUIT_SPECTATE_STAGE_INT
				IF NOT IS_PAUSE_MENU_ACTIVE()
				AND IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_MANAGE_QUITTING_SPECTATE - INPUT_FRONTEND_CANCEL")
					sSpecVarsPassed.iQuitSpecStage = ciGB_QUIT_SPECTATE_STAGE_NO_PRESS
				ENDIF
			BREAK
			CASE ciGB_QUIT_SPECTATE_STAGE_NO_PRESS
				IF NOT IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_MANAGE_QUITTING_SPECTATE - NOT IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)")
					sSpecVarsPassed.iQuitSpecStage = ciGB_QUIT_SPECTATE_STAGE_WARNING
				ENDIF
			BREAK
			CASE ciGB_QUIT_SPECTATE_STAGE_WARNING
				SET_WARNING_MESSAGE("GB_SPEC_WAR", FE_WARNING_OKCANCEL)
				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
						sSpecVarsPassed.iQuitSpecStage = ciGB_QUIT_SPECTATE_STAGE_INT
						CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_MANAGE_QUITTING_SPECTATE - ciGB_QUIT_SPECTATE_STAGE_WARNING - INPUT_FRONTEND_CANCEL")
					ENDIF
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_MANAGE_QUITTING_SPECTATE - ciGB_QUIT_SPECTATE_STAGE_WARNING - INPUT_FRONTEND_ACCEPT")
						sSpecVarsPassed.iQuitSpecStage = ciGB_QUIT_SPECTATE_STAGE_YES
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE ciGB_QUIT_SPECTATE_STAGE_YES
				RETURN TRUE
		ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC GB_SPECTATE_DO_SCREEN_FADE_IN_IF_NEEDED(GB_MAINTAIN_SPECTATE_VARS &sSpecVarsPassed)
	CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_SPECTATE_DO_SCREEN_FADE_IN_IF_NEEDED")
	IF IS_BIT_SET(sSpecVarsPassed.iBitSet, ciGB_SPEC_BS_DONE_FADE)
	AND HAS_NET_TIMER_STARTED(sSpecVarsPassed.stFocusTargetDelay)
	AND (IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT())
	AND sSpecVarsPassed.iSpecStage = ciGB_SPEC_STAGE_WARP
	AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING	
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_SPECTATE_DO_SCREEN_FADE_IN_IF_NEEDED - DO_SCREEN_FADE_IN(1000)")
		GB_TIDYUP_SPECTATOR_CAM()
		GB_CLEAR_SHOULD_CLOSE_SPECTATE()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		DO_SCREEN_FADE_IN(1000)
	ENDIF
ENDPROC

FUNC BOOL GB_MAINTAIN_SPECTATE(GB_MAINTAIN_SPECTATE_VARS &sSpecVarsPassed, BOOL bBossCheck = TRUE, BOOL bSpawnAtSpecLocation = FALSE, BOOL bHideWantedStars = TRUE, BOOL bApplyPreviousWantedOnExit = TRUE, INT iTimeToWaitForWorld = ciARBITRARY_DELAY_TO_ALLOW_THE_WORLD_TO_STREAM_IN)
	
	//If it's not safe to call it
	IF NOT GB_ITS_SAFE_FOR_SPEC_CAM()
		RETURN FALSE
	ENDIF	
	
	//If we're not a gang then get out
	IF bBossCheck
	AND NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		RETURN FALSE
	ENDIF	
	
	//Deal with setting this up
	IF NOT GB_SHOULD_RUN_SPEC_CAM()
		//If we should go straight into spectator cam
		IF GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM()
			IF GB_ARE_THERE_ENOUGH_GOONS_FOR_SPECTAGE()
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM - GB_ARE_THERE_ENOUGH_GOONS_FOR_SPECTAGE - GB_SET_RUN_SPEC_CAM")
				GB_SET_RUN_SPEC_CAM()
			#IF IS_DEBUG_BUILD 
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM - GB_ARE_THERE_ENOUGH_GOONS_FOR_SPECTAGE - GB_ARE_THERE_ENOUGH_GOONS_FOR_SPECTAGE = FALSE")
				#ENDIF
			ENDIF
			GB_CLEAR_LAUNCH_CHALLENGE_WITH_SPEC_CAM()
			RETURN FALSE
		ENDIF
//		//if we need to re-add the context trigger
//		IF GB_SPEC_CAM_ADD_CONTEXT_AGAIN()
//			CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] RESET_NET_TIMER(sSpecVarsPassed.stContextTime)")
//			RESET_NET_TIMER(sSpecVarsPassed.stContextTime)
//			GB_CLEAR_SPEC_CAM_ADD_CONTEXT_AGAIN()
//		ENDIF
//		//Start a timer
//		IF NOT HAS_NET_TIMER_STARTED(sSpecVarsPassed.stContextTime)
//			IF GB_ARE_THERE_ENOUGH_GOONS_FOR_SPECTAGE()
//			AND CAN_PRINT_GANG_BOSS_HELP_MSG()
//				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] START_NET_TIMER")
//				START_NET_TIMER(sSpecVarsPassed.stContextTime)
//			ENDIF
//		ELIF NOT HAS_NET_TIMER_EXPIRED(sSpecVarsPassed.stContextTime, 15000)
//		AND sSpecVarsPassed.iSpecStage = ciGB_SPEC_STAGE_INT 
//			IF sSpecVarsPassed.iGbSpecContext = NEW_CONTEXT_INTENTION
//				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] REGISTER_CONTEXT_INTENTION")
//				REGISTER_CONTEXT_INTENTION(sSpecVarsPassed.iGbSpecContext, CP_MAXIMUM_PRIORITY, "GB_SPEC_CON", DEFAULT, DEFAULT, CS_GANG_BOSS)
//			ENDIF
//			IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(sSpecVarsPassed.iGbSpecContext) 
//			AND HAS_CONTEXT_BUTTON_TRIGGERED(sSpecVarsPassed.iGbSpecContext, TRUE)
//				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] HAS_CONTEXT_BUTTON_TRIGGERED")
//				RELEASE_CONTEXT_INTENTION(sSpecVarsPassed.iGbSpecContext)
//				GB_SET_RUN_SPEC_CAM()
//			ELSE
//				RETURN FALSE
//			ENDIF
//		ELSE
//			IF sSpecVarsPassed.iGbSpecContext != NEW_CONTEXT_INTENTION
//				RELEASE_CONTEXT_INTENTION(sSpecVarsPassed.iGbSpecContext)
//				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SPEC_CON")
//					CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] CLEAR_HELP - GB_SPEC_CON")
//					CLEAR_HELP()
//				ENDIF
//			ENDIF
//		ENDIF
		
		RETURN FALSE
	ENDIF	
	
	INT iStatInt 
	BOOL BSafe, bApplyWanted
	ENABLE_MOVIE_SUBTITLES(FALSE)
	SWITCH sSpecVarsPassed.iSpecStage
		CASE ciGB_SPEC_STAGE_INT
			DEBUG_PRINTCALLSTACK()
			RESET_GLOBAL_SPECTATOR_STRUCT()
			INITIALISE_SPEC_CAM_DATA(g_BossSpecData)
			POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(g_BossSpecData)
			IF IS_THIS_SPECTATOR_CAM_OFF(g_BossSpecData.specCamData)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				NETWORK_SET_IN_FREE_CAM_MODE(FALSE)	
			ENDIF
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)")
				NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
				SET_BIT(MPGlobalsAmbience.iGbSpecCamGlobalBitSet, ciGB_SPEC_BS_GLOBAL_ENABLE_CONTROL)
			ENDIF
			IF NOT IS_KILL_YOURSELF_OPTION_DISABLED()
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] DISABLE_KILL_YOURSELF_OPTION")
				DISABLE_KILL_YOURSELF_OPTION()
				SET_BIT(MPGlobalsAmbience.iGbSpecCamGlobalBitSet, ciGB_SPEC_BS_GLOBAL_ENABLE_KILL_ONES_SELF)
			ENDIF
			IF NOT IS_INTERACTION_MENU_DISABLED()
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] DISABLE_INTERACTION_MENU")
				DISABLE_INTERACTION_MENU()
				SET_BIT(MPGlobalsAmbience.iGbSpecCamGlobalBitSet, ciGB_SPEC_BS_GLOBAL_PI_MENU)
			ENDIF
			sSpecVarsPassed.iQuitSpecStage = 0
			sSpecVarsPassed.iBitSet = 0 
			sSpecVarsPassed.iStartWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] sSpecVarsPassed.iStartWantedLevel = ", sSpecVarsPassed.iStartWantedLevel)
			iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT)
			IF NOT IS_BIT_SET(iStatInt, biFmGb_Help_Spec0) 
				SET_BIT(iStatInt, biFmGb_Help_Spec0) 
				SET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT, iStatInt)
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] SET_BIT(iStatInt, biFmGb_Help_Spec0)")
			ELIF NOT IS_BIT_SET(iStatInt, biFmGb_Help_Spec1) 
				SET_BIT(iStatInt, biFmGb_Help_Spec1) 
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] SET_BIT(iStatInt, biFmGb_Help_Spec1)")
				SET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT, iStatInt)
			ELIF NOT IS_BIT_SET(iStatInt, biFmGb_Help_Spec2) 
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] SET_BIT(iStatInt, biFmGb_Help_Spec2)")
				SET_BIT(iStatInt, biFmGb_Help_Spec2) 
				SET_BIT(sSpecVarsPassed.iBitSet, ciGB_SPEC_BS_DO_MENU_HELP)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT, iStatInt)
			ENDIF
			//RELEASE_CONTEXT_INTENTION(sSpecVarsPassed.iGbSpecContext)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_SPEC_CON")
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] CLEAR_HELP - GB_SPEC_CON")
				CLEAR_HELP()
			ENDIF
			
			//Set that we're spectating
			GB_SET_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_BOSS_USING_SPECTATE)
			g_sGb_Telemetry_data.sdata.m_playerParticipated = 2
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] [GB TEL] g_sGb_Telemetry_data.sdata.m_playerParticipated")
			
			sSpecVarsPassed.iSpecStage = ciGB_SPEC_STAGE_MAINTAIN
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] iSpecStage = ciGB_SPEC_STAGE_MAINTAIN")
		BREAK
		
		CASE ciGB_SPEC_STAGE_MAINTAIN
		
			IF IS_BIT_SET(sSpecVarsPassed.iBitSet, ciGB_SPEC_BS_DO_MENU_HELP)
			AND CAN_PRINT_GANG_BOSS_HELP_MSG()
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] - PRINT_HELP GB_SPEC_MENH")
				PRINT_HELP_NO_SOUND("GB_SPEC_MENH")
				GB_SET_GANG_BOSS_HELP_BACKGROUND()
				CLEAR_BIT(sSpecVarsPassed.iBitSet, ciGB_SPEC_BS_DO_MENU_HELP)
			ENDIF
			
			IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
			AND IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(SPEC_MODE_TV_CHANNEL)
				CLEAR_FOCUS()
				ACTIVATE_SPECTATOR_CAM(g_BossSpecData, SPEC_MODE_HEIST, NULL, ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED)
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] ACTIVATE_SPECTATOR_CAM(g_BossSpecData, SPEC_MODE_HEIST, NULL, ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED)")
			ELSE
				//Turn off controls
				IF NOT IS_BIT_SET(MPGlobalsAmbience.iGbSpecCamGlobalBitSet, ciGB_SPEC_BS_GLOBAL_ENABLE_CONTROL)
				AND IS_NET_PLAYER_OK(PLAYER_ID())
					NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
					SET_BIT(MPGlobalsAmbience.iGbSpecCamGlobalBitSet, ciGB_SPEC_BS_GLOBAL_ENABLE_CONTROL)
				ENDIF
				
				IF IS_THIS_SPECTATOR_CAM_RUNNING(g_BossSpecData.specCamData)
				AND CAN_SPECTATOR_QUIT_WHEN_NO_TARGETS()
					IF NOT IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(SPEC_MODE_TV_CHANNEL)
						IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
							CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER")
							GB_MAINTAIN_SPECTATE_MOVE_TO_CLEAN_UP(sSpecVarsPassed, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF GB_MANAGE_QUITTING_SPECTATE(sSpecVarsPassed)
				GB_MAINTAIN_SPECTATE_MOVE_TO_CLEAN_UP(sSpecVarsPassed, bSpawnAtSpecLocation)
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)")
			ENDIF
			IF CLEAN_UP_ON_CALL_WITH_OUT_WAIT()			//Cleans up any on calls
				GB_CLIENT_MAINTAIN_EVERY_FRAME_CALLS(bHideWantedStars)
			ENDIF
		BREAK
		
		CASE ciGB_SPEC_STAGE_WARP
			//IF NET_WARP_TO_COORD(sSpecVarsPassed.vInitialLocation, sSpecVarsPassed.fInitialHeading, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, TRUE, FALSE)
			IF NOT IS_BIT_SET(sSpecVarsPassed.iBitSet, ciGB_SPEC_BS_DONE_FADE)
				IF IS_SCREEN_FADED_OUT()
				AND IS_NET_PLAYER_OK(PLAYER_ID())
				AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] ciGB_SPEC_BS_DONE_FADE")
					SET_BIT(sSpecVarsPassed.iBitSet, ciGB_SPEC_BS_DONE_FADE)
					DEACTIVATE_SPECTATOR_CAM(g_BossSpecData)
					RESET_NET_TIMER(sSpecVarsPassed.stFocusTargetDelay)
					START_NET_TIMER(sSpecVarsPassed.stFocusTargetDelay)
					SET_FOCUS_ENTITY(PLAYER_PED_ID())
				ELIF NOT IS_SCREEN_FADING_OUT()
					CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] ciGB_SPEC_STAGE_WARP - IS_SCREEN_FADING_OUT")
					DO_SCREEN_FADE_OUT(1000)
				ENDIF
			ELIF IS_VECTOR_ZERO(sSpecVarsPassed.vInitialLocation)
			OR WARP_TO_SPAWN_LOCATION(sSpecVarsPassed.spawnLocation, FALSE, FALSE, FALSE, FALSE, FALSE)
				IF HAS_NET_TIMER_STARTED(sSpecVarsPassed.stFocusTargetDelay)
					IF NOT HAS_NET_TIMER_EXPIRED(sSpecVarsPassed.stFocusTargetDelay, iTimeToWaitForWorld)
						RETURN FALSE
					ENDIF
				ENDIF
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] WARP_TO_SPAWN_LOCATION")
				DO_SCREEN_FADE_IN(1000)
				RESET_NET_TIMER(sSpecVarsPassed.stFocusTargetDelay)
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] DO_SCREEN_FADE_IN")
				sSpecVarsPassed.iBitSet = 0
				sSpecVarsPassed.iSpecStage = ciGB_SPEC_STAGE_INT
				bApplyWanted = !GB_SHOULD_CLOSE_SPECTATE()
				//Cache if it's safe
				BSafe = GB_ITS_SAFE_FOR_SPEC_CAM()
				GB_TIDYUP_SPECTATOR_CAM()
				GB_CLEAR_SHOULD_CLOSE_SPECTATE()
				
				IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GB_CHAL_FIVESTAR
				AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_BIKER_CONTRABAND_DEFEND
				AND NOT GB_ARE_THERE_ENOUGH_GOONS_FOR_SPECTAGE(FALSE)
				AND GB_IS_PLAYER_ON_ANY_GANG_BOSS_CHALLENGE(PLAYER_ID())
					PRINT_HELP_NO_SOUND("GB_SPEC_NO")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
				ENDIF
				
				//reset that it's safe
				IF BSafe
					GB_SET_ITS_SAFE_FOR_SPEC_CAM()
				ENDIF
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF bApplyWanted
				AND bApplyPreviousWantedOnExit
					IF sSpecVarsPassed.iStartWantedLevel != 0
						CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), ", sSpecVarsPassed.iStartWantedLevel, ")")
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), sSpecVarsPassed.iStartWantedLevel)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						sSpecVarsPassed.iStartWantedLevel = 0 
					ENDIF
				ENDIF
				sSpecVarsPassed.vInitialLocation = <<0.0, 0.0, 0.0>>
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] iSpecStage = ciGB_SPEC_STAGE_INT")
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] CLEANED UP")
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	//Fade out the player
	IF NOT IS_BIT_SET(sSpecVarsPassed.iBitSet, ciGB_SPEC_BS_FADE)
		IF IS_SCREEN_FADED_OUT()
			FADE_OUT_LOCAL_PLAYER(TRUE)
			SET_BIT(sSpecVarsPassed.iBitSet, ciGB_SPEC_BS_FADE)
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] FADE_OUT_LOCAL_PLAYER(TRUE)")
			sSpecVarsPassed.vInitialLocation = GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				sSpecVarsPassed.fInitialHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
			ENDIF
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] sSpecVarsPassed.vInitialLocation = ", sSpecVarsPassed.vInitialLocation)
		ENDIF
	//Once faded then clean them up
	ELIF NOT NETWORK_IS_PLAYER_FADING(PLAYER_ID())
	AND NOT IS_BIT_SET(sSpecVarsPassed.iBitSet, ciGB_SPEC_BS_DONE_CLEAR)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] IS_PED_IN_ANY_VEHICLE - CLEAR_PED_TASKS_IMMEDIATELY - RETURN FALSE")
				RETURN FALSE
			ENDIF	
			SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] CLEAR_PED_TASKS_IMMEDIATELY")
			SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		ENDIF
		SET_BIT(sSpecVarsPassed.iBitSet, ciGB_SPEC_BS_DONE_CLEAR)
		CPRINTLN(DEBUG_NET_AMBIENT, " [GBTV] ciGB_SPEC_BS_DONE_CLEAR")
	ENDIF
	
	IF sSpecVarsPassed.iSpecStage != ciGB_SPEC_STAGE_WARP
		MAINTAIN_SPECTATOR(g_BossSpecData)
	ENDIF
	RETURN FALSE
ENDFUNC




