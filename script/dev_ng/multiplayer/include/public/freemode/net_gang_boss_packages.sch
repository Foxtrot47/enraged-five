
//-- Dave W - 'Collect Packages' functionality which can be used in freemode scripts

//----------------------
//	INCLUDES
//----------------------
USING "globals.sch"
USING "net_prints.sch"
USING "net_gang_boss_common.sch"
USING "net_include.sch"

//----------------------
//	CONSTANTS
//----------------------
CONST_INT MAX_GANG_BOSS_GB_PACKAGES			4

CONST_INT SERVER_GB_PACKAGE_FORCE_SPAWN				0
CONST_INT SERVER_GB_PACKAGES_INIT					1
CONST_INT SERVER_GB_PACKAGE_ALLOW_COLLECT			2
CONST_INT SERVER_GB_PACKAGE_NO_GPS					3
CONST_INT SERVER_GB_PACKAGE0_ATTACHED_WHEN_CREATED	4
CONST_INT SERVER_GB_PACKAGE1_ATTACHED_WHEN_CREATED	5
CONST_INT SERVER_GB_PACKAGE2_ATTACHED_WHEN_CREATED	6
CONST_INT SERVER_GB_PACKAGE3_ATTACHED_WHEN_CREATED	7


CONST_INT LOCAL_GB_PACKAGE_RESET_PLAYER_BLIPS			0
CONST_INT LOCAL_GB_PACKAGE_PREVENT_COLLECTION			1
CONST_INT LOCAL_GB_PACKAGE_ATTACHED_TO_NPC				2
CONST_INT LOCAL_GB_PACKAGE_ATTACHED_TO_NPC2				3
CONST_INT LOCAL_GB_PACKAGE_ATTACHED_TO_NPC3				4
CONST_INT LOCAL_GB_PACKAGE_ATTACHED_TO_NPC4				5

CONST_INT CLIENT_GB_GB_PACKAGE_INIT			0
CONST_INT CLIENT_GB_GB_PACKAGE_NO_HUD		1

//----------------------
//	STRUCT
//----------------------
STRUCT STRUCT_COLLECT_GB_PACKAGE_OPTIONS
	INT iNumberOfPackages
	MODEL_NAMES mPackage
	BLIP_SPRITE spritePackage
	BOOL bDisableGps
	INT iBlipColourFriendlyPlayerWithPackage
//	INT iPedToAttachTo[MAX_GANG_BOSS_GB_PACKAGES]
//	BOOL bAttachToPed[MAX_GANG_BOSS_GB_PACKAGES]
	VECTOR vDropOff
//	VECTOR vCreateLocation[MAX_GANG_BOSS_GB_PACKAGES]
ENDSTRUCT



STRUCT STRUCT_COLLECT_GB_PACKAGES_SERVER
	NETWORK_INDEX niPackage[MAX_GANG_BOSS_GB_PACKAGES]
	INT iDeliveredByPlayer[MAX_GANG_BOSS_GB_PACKAGES]
	INT iDestroyedByPlayer[MAX_GANG_BOSS_GB_PACKAGES]
	INT iCurrentpackageHolder[MAX_GANG_BOSS_GB_PACKAGES]
	INT iServerPackageBitset
	
	SCRIPT_TIMER InAirCheckDelayTimer[MAX_GANG_BOSS_GB_PACKAGES]
	NETWORK_INDEX niChute[MAX_GANG_BOSS_GB_PACKAGES]
	
	SCRIPT_TIMER timeSpawnPackage[MAX_GANG_BOSS_GB_PACKAGES]
	
	STRUCT_COLLECT_GB_PACKAGE_OPTIONS options
ENDSTRUCT





STRUCT STRUCT_LOCAL_GB_PACKAGE_DATA
	INT iTempPackageHolder[MAX_GANG_BOSS_GB_PACKAGES]
	INT iTempPackageDelivered[MAX_GANG_BOSS_GB_PACKAGES]
	INT iPackageNeedsCleanedUpBitset
	INT iPackageBitset
	INT iPickupSound
	INT iDeliveredSound
	INT iInitRemotePlayerBlipsBitset
	VECTOR vCreateEntityCurrentAttemptedCoords[MAX_GANG_BOSS_GB_PACKAGES]
	FLOAT fCreateEntityCurrentAttemptedHeading[MAX_GANG_BOSS_GB_PACKAGES]
	BLIP_INDEX blipPackage[MAX_GANG_BOSS_GB_PACKAGES]
	TEXT_LABEL_15 tl5PackageBlipName
	TEXT_LABEL_15 tl5HudTitle
	SCRIPT_TIMER timeDelivered
ENDSTRUCT

//----------------------
//	FUNCTIONS
//----------------------
FUNC NETWORK_INDEX GET_GB_PACKAGE_NI(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage)
	RETURN serverPackage.niPackage[iPackage]
ENDFUNC

FUNC BOOL DOES_GB_PACKAGE_NI_EXIST(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage)
	RETURN NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GB_PACKAGE_NI(serverPackage, iPackage))
ENDFUNC

FUNC OBJECT_INDEX GET_GB_PACKAGE_OBJ(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage)
	OBJECT_INDEX obj
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GB_PACKAGE_NI(serverPackage, iPackage))
		RETURN NET_TO_OBJ(GET_GB_PACKAGE_NI(serverPackage, iPackage))
	ENDIF
	
	RETURN obj
ENDFUNC

PROC STOP_LOCAL_PLAYER_FROM_COLLECTING_ALL_GB_PACKAGES(STRUCT_LOCAL_GB_PACKAGE_DATA &localData, STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	INT i
	BOOL bFoundAtLeastOnePackage
	IF NOT IS_BIT_SET(localData.iPackageBitset, LOCAL_GB_PACKAGE_PREVENT_COLLECTION)
		#IF IS_DEBUG_BUILD
		PRINTLN("[BIKER1]     ----------> - [GB_PACKAGES] [STOP_LOCAL_PLAYER_FROM_COLLECTING_ALL_GB_PACKAGES] Called, callstack...")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		
		REPEAT MAX_GANG_BOSS_GB_PACKAGES i
			IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
				PRINTLN("[BIKER1]     ----------> - [GB_PACKAGES] [STOP_LOCAL_PLAYER_FROM_COLLECTING_ALL_GB_PACKAGES] Package ", i, " Exists")
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(GET_GB_PACKAGE_OBJ(serverPackage, i), TRUE, TRUE)
				bFoundAtLeastOnePackage = TRUE
			ELSE
				PRINTLN("[BIKER1]     ----------> - [GB_PACKAGES] [STOP_LOCAL_PLAYER_FROM_COLLECTING_ALL_GB_PACKAGES] Package ", i, " Does not exist")
			ENDIF
		ENDREPEAT
		
		IF bFoundAtLeastOnePackage
			SET_BIT(localData.iPackageBitset, LOCAL_GB_PACKAGE_PREVENT_COLLECTION)
		ENDIF
	ENDIF
ENDPROC

PROC ALLOW_LOCAL_PLAYER_TO_COLLECT_ALL_GB_PACKAGES(STRUCT_LOCAL_GB_PACKAGE_DATA &localData, STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	INT i
	
	IF IS_BIT_SET(localData.iPackageBitset, LOCAL_GB_PACKAGE_PREVENT_COLLECTION)
		#IF IS_DEBUG_BUILD
		PRINTLN("[BIKER1]     ----------> - [GB_PACKAGES] [ALLOW_LOCAL_PLAYER_TO_COLLECT_ALL_GB_PACKAGES] Called, callstack...")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		
		REPEAT MAX_GANG_BOSS_GB_PACKAGES i
			IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(GET_GB_PACKAGE_OBJ(serverPackage, i), FALSE, TRUE)
			ENDIF
		ENDREPEAT
		
		CLEAR_BIT(localData.iPackageBitset, LOCAL_GB_PACKAGE_PREVENT_COLLECTION)
	ENDIF
ENDPROC

FUNC BOOL HAS_GB_PACKAGE_BEEN_DELIVERED(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage, INT &iPlayerDelPackage)
	IF serverPackage.iDeliveredByPlayer[iPackage] > -1
		iPlayerDelPackage = serverPackage.iDeliveredByPlayer[iPackage]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PART_DELIVERED_GB_PACKAGE(INT iPart, INT iPackage)
	INT iPlayer
	IF iPart >-1 
		iPlayer = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
		IF iPlayer > -1
			RETURN IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iDeliveredPackageBitset, iPackage)
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_GB_PACKAGE_BEEN_DESTROYED(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage, INT &iPlayerDestroyPackage)
	IF serverPackage.iDestroyedByPlayer[iPackage] > -1
		iPlayerDestroyPackage = serverPackage.iDestroyedByPlayer[iPackage]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_GB_PACKAGE_BEEN_DELIVERED_OR_DESTROYED(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage)
	INT iPlayer
	RETURN(HAS_GB_PACKAGE_BEEN_DELIVERED(serverPackage, iPackage, iPlayer)
			OR HAS_GB_PACKAGE_BEEN_DESTROYED(serverPackage, iPackage, iPlayer))
ENDFUNC
FUNC INT GET_GB_PACKAGE_HOLDER_FROM_SERVER_AS_PART(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage)
	RETURN serverPackage.iCurrentpackageHolder[iPackage]
ENDFUNC

FUNC PLAYER_INDEX GET_GB_PACKAGE_HOLDER_FROM_SERVER_AS_PLAYER(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage)
	PLAYER_INDEX player = INVALID_PLAYER_INDEX()
	PARTICIPANT_INDEX part
	IF serverPackage.iCurrentpackageHolder[iPackage] >= 0
		part = INT_TO_PARTICIPANTINDEX(serverPackage.iCurrentpackageHolder[iPackage])
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			player = NETWORK_GET_PLAYER_INDEX(part)
		ENDIF
	ENDIF
	
	RETURN player
ENDFUNC

FUNC PLAYER_INDEX GET_ANY_PLAYER_WITH_AN_UNDELIVERED_GB_PACKAGE(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, BOOL bIncludeSelf = TRUE)
	INT i
	INT  iPlayerDel
	PLAYER_INDEX playerWIthPackage = INVALID_PLAYER_INDEX()
	REPEAT MAX_GANG_BOSS_GB_PACKAGES i
		IF NOT HAS_GB_PACKAGE_BEEN_DELIVERED(serverPackage, i, iPlayerDel)
			playerWIthPackage = GET_GB_PACKAGE_HOLDER_FROM_SERVER_AS_PLAYER(serverPackage, i)
			IF bIncludeSelf
			OR (bIncludeSelf = FALSE AND playerWIthPackage!= PLAYER_ID())
				IF playerWIthPackage != INVALID_PLAYER_INDEX()
					RETURN playerWIthPackage
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN playerWIthPackage
ENDFUNC

FUNC BOOL IS_ANYONE_HOLDING_GB_PACKAGE(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage, PLAYER_INDEX &playerHolder)
	playerHolder = GET_GB_PACKAGE_HOLDER_FROM_SERVER_AS_PLAYER(serverPackage, iPackage)
		
	RETURN (playerHolder != INVALID_PLAYER_INDEX())	
ENDFUNC

/// PURPOSE:
///    Is the specified package attached to a NPC?
/// PARAMS:
///    serverPackage - 
///    iPackage - 
/// RETURNS:
///    
FUNC BOOL IS_GB_PACKAGE_ATTACHED_TO_ANY_PED(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage)
	ENTITY_INDEX ent
	PED_INDEX ped
	IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, iPackage)
		IF IS_ENTITY_ATTACHED_TO_ANY_PED(GET_GB_PACKAGE_OBJ(serverPackage, iPackage))
			ent = GET_ENTITY_ATTACHED_TO(GET_GB_PACKAGE_OBJ(serverPackage, iPackage))
			IF IS_ENTITY_A_PED(ent)
				ped = GET_PED_INDEX_FROM_ENTITY_INDEX(ent)	
				IF NOT IS_PED_A_PLAYER(ped)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
			
	ENDIF
		
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Any package attached to a NPC?
/// PARAMS:
///    serverPackage - 
/// RETURNS:
///    
FUNC BOOL IS_ANY_GB_PACKAGE_ATTACHED_TO_A_PED(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	INT i
	REPEAT MAX_GANG_BOSS_GB_PACKAGES i
		IF IS_GB_PACKAGE_ATTACHED_TO_ANY_PED(serverPackage, i)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_GB_PACKAGE_UNCOLLECTED(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT &iPackage, BOOL bIncludeAttachedToAiAsUncollected = TRUE)
	INT i
	INT  iPlayerDel
	
	REPEAT MAX_GANG_BOSS_GB_PACKAGES i
	
		IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
			
			IF NOT HAS_GB_PACKAGE_BEEN_DELIVERED(serverPackage, i, iPlayerDel)
				
				IF GET_GB_PACKAGE_HOLDER_FROM_SERVER_AS_PART(serverPackage, i) = -1
				
					IF bIncludeAttachedToAiAsUncollected	
					
						iPackage = i
						RETURN TRUE
					ELSE
						IF NOT IS_GB_PACKAGE_ATTACHED_TO_ANY_PED(serverPackage, i)
							
							iPackage = i
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT 
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_GB_PACKAGE_HOLDER_SAT_IN_VEHICLE(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, VEHICLE_INDEX veh)
	INT i
	INT iPlayerDel
	PLAYER_INDEX player
	PED_INDEX ped
	
	REPEAT MAX_GANG_BOSS_GB_PACKAGES i
		IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
			IF NOT HAS_GB_PACKAGE_BEEN_DELIVERED(serverPackage, i, iPlayerDel)
				IF IS_ANYONE_HOLDING_GB_PACKAGE(serverPackage, i, player)
					ped = GET_PLAYER_PED(player)
					IF IS_PED_SITTING_IN_VEHICLE(ped, veh)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PART_HAVE_GB_PACKAGE(INT iPart, INT iPackage)
	INT iPlayer
	IF iPart >-1 
		iPlayer = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
	//	PRINTLN("[BIKER1]     ---------->  [TEMP] DOES_PART_HAVE_GB_PACKAGE iPart = ", iPart, " iPackage = ", iPackage, " iPlayer = ", iPlayer, " Bitset ", GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iCollectedPackageBitset)
		IF iPlayer > -1
			RETURN IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iCollectedPackageBitset, iPackage)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PART_HAVE_ANY_GB_PACKAGE(INT iPart)
	INT iPlayer
	IF iPart >-1
		iPlayer = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
		IF iPlayer > -1
			RETURN GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iCollectedPackageBitset != 0
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_GB_PACKAGE_HELD_BY_PART(INT iPart)
	INT iPackage = -1
	INT iPlayer
	INT i
	IF DOES_PART_HAVE_ANY_GB_PACKAGE(ipart)
		REPEAT MAX_GANG_BOSS_GB_PACKAGES i
			IF iPackage = -1
				iPlayer = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
				IF IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iCollectedPackageBitset, i)
					iPackage = i
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN iPackage
ENDFUNC

FUNC BOOL HAVE_ALL_GB_PACKAGES_BEEN_DELIVERED(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)

	INT iNumPackages = serverPackage.options.iNumberOfPackages
	IF iNumPackages = 0
		RETURN FALSE
	ENDIF
	
	INT i
	INT iPlayer
	INT iDelivered
	REPEAT iNumPackages i
	
		IF HAS_GB_PACKAGE_BEEN_DELIVERED(serverPackage, i, iPlayer)
			
			iDelivered++
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 100 = 0
				PRINTLN("[BIKER1]     ----------> [HAVE_ALL_GB_PACKAGES_BEEN_DELIVERED] Think this package hasn't been delivered ", i, " Total packages = ", iNumPackages)
			ENDIF
			#ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN ( iDelivered = iNumPackages)
ENDFUNC

FUNC INT GET_NUMBER_OF_UNDELIVERED_GB_PACKAGES(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	INT iNumPackages = serverPackage.options.iNumberOfPackages
	INT iNumUndelivered
	INT i
	INT iPlayer
	IF iNumPackages > 0
		REPEAT iNumPackages i
			IF NOT HAS_GB_PACKAGE_BEEN_DELIVERED(serverPackage, i, iPlayer)
				iNumUndelivered++
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN iNumUndelivered
ENDFUNC


FUNC BOOL ARE_ALL_GB_PACKAGE_HOLDERS_IN_MY_GANG(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	INT i
	INT iHeldByMyGangCount
	INT iPlayerDel
	INT iNumUndelivered
	PLAYER_INDEX playerWithPackage
	
	IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	iNumUndelivered = GET_NUMBER_OF_UNDELIVERED_GB_PACKAGES(serverPackage)
	
	REPEAT serverPackage.options.iNumberOfPackages i
		IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
			IF NOT HAS_GB_PACKAGE_BEEN_DELIVERED(serverPackage, i, iPlayerDel)
				IF IS_ANYONE_HOLDING_GB_PACKAGE(serverPackage, i, playerWithPackage)
					IF GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), playerWithPackage)
					OR GB_IS_PLAYER_ALLIED_WITH_THIS_PLAYER(PLAYER_ID(), playerWithPackage) 
						iHeldByMyGangCount++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF serverPackage.options.iNumberOfPackages > 0
		IF iHeldByMyGangCount = iNumUndelivered
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
//	REPEAT MAX_GANG_BOSS_GB_PACKAGES i
//		IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
//			IF IS_ANYONE_HOLDING_GB_PACKAGE(serverPackage, i, playerWithPackage)
//				IF NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), playerWithPackage)
//				AND NOT GB_IS_PLAYER_ALLIED_WITH_THIS_PLAYER(PLAYER_ID(), playerWithPackage) 
//					RETURN FALSE
//				ENDIF
//			ELSE
//				RETURN FALSE
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	
//	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PACKAGE_HOLDER_IN_MY_GANG(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage)
	PLAYER_INDEX player
	IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, iPackage)
		IF IS_ANYONE_HOLDING_GB_PACKAGE(serverPackage, iPackage, player)
			IF GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), player)
			OR GB_IS_PLAYER_ALLIED_WITH_THIS_PLAYER(PLAYER_ID(), player) 
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_ANY_GB_PACKAGE_HOLDERS_NOT_IN_MY_GANG(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, PLAYER_INDEX &playerWithPackage1, PLAYER_INDEX &playerWithPackage2)
	PLAYER_INDEX player
	INT i
	INT iPlayerDel
	REPEAT MAX_GANG_BOSS_GB_PACKAGES i
		IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
			IF NOT HAS_GB_PACKAGE_BEEN_DELIVERED(serverPackage, i, iPlayerDel)
				IF IS_ANYONE_HOLDING_GB_PACKAGE(serverPackage, i, player)
					IF player != PLAYER_ID()
						IF NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), player)
						AND NOT GB_IS_PLAYER_ALLIED_WITH_THIS_PLAYER(PLAYER_ID(), player)
							IF playerWithPackage1 = INVALID_PLAYER_INDEX()
								playerWithPackage1 = player
							ELSE
								IF playerWithPackage2 = INVALID_PLAYER_INDEX()
									playerWithPackage2 = player
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//PRINTLN("[BIKER1]     ---------->  [BAD_DEAL_EXTRA_DEBUG] [MAINTAIN_OBJECTIVE_TEXT_EXTRA_DEBUG] playerWithPackage1 = ", GET_PLAYER_NAME(playerWithPackage1), " playerWithPackage2 = ", GET_PLAYER_NAME(playerWithPackage2))
ENDPROC


PROC SET_LOCAL_PART_HAS_GB_PACKAGE(INT iPackage)
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF iPlayer > -1
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iCollectedPackageBitset, iPackage)
			SET_BIT(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iCollectedPackageBitset, iPackage)
			PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [SET_LOCAL_PART_HAS_GB_PACKAGE] Package = ",iPackage, " iPlayer = ", iPlayer, " Bitset ", GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iCollectedPackageBitset)
			
			GB_SET_LOCAL_PLAYER_CARRYING_CRITICAL_PACKAGE(TRUE)
		ENDIF
	ENDIF	
ENDPROC

PROC CLEAR_LOCAL_PART_HAS_GB_PACKAGE(INT iPackage)
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF iPlayer > -1
		IF IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iCollectedPackageBitset, iPackage)
			CLEAR_BIT(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iCollectedPackageBitset, iPackage)
			PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CLEAR_LOCAL_PART_HAS_GB_PACKAGE] Package = ",iPackage)
			GB_SET_LOCAL_PLAYER_CARRYING_CRITICAL_PACKAGE(FALSE)
		ENDIF
	ENDIF	
ENDPROC



FUNC FLOAT GET_DISTANCE2_TO_CLOSEST_GB_PACKAGE(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT &iCLosest, BOOL bIncludeCollected = TRUE)
	FLOAT fCurMaxDist2 = 9999999999.0
	FLOAT fCur2
	INT iCurClosest = -1
	INT i
	PLAYER_INDEX playerHolder
	
	REPEAT MAX_GANG_BOSS_GB_PACKAGES i
		IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
			IF NOT IS_ANYONE_HOLDING_GB_PACKAGE(serverPackage, i, playerHolder)
			OR bIncludeCollected
				fCur2 = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(GET_GB_PACKAGE_OBJ(serverPackage, i)))
				IF fCur2 < fCurMaxDist2
					fCurMaxDist2 = fCur2
					iCurClosest = i
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	iCLosest = iCurClosest
	RETURN fCurMaxDist2
	
ENDFUNC
PROC COMMON_DROP_GB_PACKAGE_CHECKS(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, INT iPackage)
	VECTOR vStripClubCoords = <<104.44887, -1320.72302, 28.26478>>
	
	IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, iPackage)
		IF DOES_PART_HAVE_GB_PACKAGE(PARTICIPANT_ID_TO_INT(), iPackage)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//-- In a sub?
//				IF IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
//					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_GB_PACKAGE_NI(serverPackage, iPackage))
//						DETACH_ENTITY(GET_GB_PACKAGE_OBJ(serverPackage, iPackage))
//						PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [COMMON_DROP_GB_PACKAGE_CHECKS] Player is in a sub with the package, detaching package")
//					ENDIF
//				ENDIF
				
				//-- in a strip club?
				IF IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_GB_PACKAGE_NI(serverPackage, iPackage))
						DETACH_ENTITY(GET_GB_PACKAGE_OBJ(serverPackage, iPackage))
						SET_ENTITY_COORDS_NO_OFFSET(GET_GB_PACKAGE_OBJ(serverPackage, iPackage), vStripClubCoords)
						PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [COMMON_DROP_GB_PACKAGE_CHECKS] Player is in a strip club with the package, detaching package")
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BLIP_SPRITE GET_SPRITE_FOR_GB_PACKAGE(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	RETURN serverPackage.options.spritePackage
ENDFUNC

PROC SET_GB_PACKAGE_BLIP_NAME(STRUCT_LOCAL_GB_PACKAGE_DATA &localPackage, TEXT_LABEL_15 tl15Name)
	IF NOT IS_STRING_NULL_OR_EMPTY(tl15Name)
		localPackage.tl5PackageBlipName = tl15Name
		PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [SET_GB_PACKAGE_BLIP_NAME] name = ", tl15Name)
	ENDIF
ENDPROC

PROC SET_GB_PACKAGE_BLIP_HUD_TITLE(STRUCT_LOCAL_GB_PACKAGE_DATA &localPackage, TEXT_LABEL_15 tl15Name)
	IF NOT IS_STRING_NULL_OR_EMPTY(tl15Name)
		localPackage.tl5HudTitle = tl15Name
		PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [SET_GB_PACKAGE_BLIP_HUD_TITLE] name = ", tl15Name)
	ENDIF
ENDPROC

FUNC TEXT_LABEL_15 GET_GB_PACKAGE_BLIP_NAME(STRUCT_LOCAL_GB_PACKAGE_DATA &localPackage)
	RETURN localPackage.tl5PackageBlipName
ENDFUNC

PROC MAINTAIN_GB_PACKAGES_IN_SHOPS(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	INT i
	BOOL bOpenDoors
	
	VECTOR vPos 
	VECTOR vShopCoords
	
	SHOP_NAME_ENUM closestShop
	
	INTERIOR_INSTANCE_INDEX interiorShop
	INTERIOR_INSTANCE_INDEX interiorPackage
	
	REPEAT MAX_GANG_BOSS_GB_PACKAGES i
		IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
			vPos = GET_ENTITY_COORDS(GET_GB_PACKAGE_OBJ(serverPackage, i),FALSE)
			closestShop = GET_CLOSEST_SHOP_OF_TYPE(vPos)
			vShopCoords = GET_SHOP_COORDS(closestShop)
			interiorShop = GET_INTERIOR_AT_COORDS_WITH_TYPE(vShopCoords, GET_SHOP_INTERIOR_TYPE(closestShop))
			interiorPackage = GET_INTERIOR_FROM_ENTITY(GET_GB_PACKAGE_OBJ(serverPackage, i))
			
			IF interiorShop != NULL
			AND interiorShop = interiorPackage
				bOpenDoors = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bOpenDoors
		IF NOT MPGlobalsAmbience.bKeepShopDoorsOpen
			MPGlobalsAmbience.bKeepShopDoorsOpen = TRUE
			PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - MPGlobalsAmbience.bKeepShopDoorsOpen = TRUE")
		ENDIF
		EXIT
	ENDIF
	
	IF MPGlobalsAmbience.bKeepShopDoorsOpen
		MPGlobalsAmbience.bKeepShopDoorsOpen = FALSE
		PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - MPGlobalsAmbience.bKeepShopDoorsOpen = FALSE")
	ENDIF
ENDPROC

PROC RESET_ALL_PLAYER_BLIPS(STRUCT_LOCAL_GB_PACKAGE_DATA &localPackage, STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	INT i
	BLIP_SPRITE sprite
	IF NOT IS_BIT_SET(localPackage.iPackageBitset, LOCAL_GB_PACKAGE_RESET_PLAYER_BLIPS)
		
		PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [RESET_ALL_PLAYER_BLIPS] SET LOCAL_GB_PACKAGE_RESET_PLAYER_BLIPS")
		SET_BIT(localPackage.iPackageBitset, LOCAL_GB_PACKAGE_RESET_PLAYER_BLIPS)
		
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(i)
			IF NETWORK_IS_PLAYER_ACTIVE(PlayerId)
				IF PlayerId != PLAYER_ID()
					sprite = GET_SPRITE_FOR_GB_PACKAGE(serverPackage)
					IF sprite != RADAR_TRACE_INVALID
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, sprite, FALSE)
					ENDIF
					SET_PLAYER_BLIP_AS_LONG_RANGE(PlayerId, FALSE)
					FORCE_BLIP_PLAYER(PlayerId, FALSE, FALSE)
					IF serverPackage.options.iBlipColourFriendlyPlayerWithPackage = 0
					SET_FIXED_BLIP_SCALE_FOR_PLAYER(PlayerId, g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
					ENDIF
					SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerId, BLIP_COLOUR_RED, FALSE)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC



/// PURPOSE:
///    Determine if a package that hasn't been collected by any player is attached to an NPC ped
/// PARAMS:
///    serverPackage - 
/// RETURNS:
///    
FUNC BOOL ARE_ANY_UNCOLLECTED_GB_PACKAGES_ATTACHED_TO_A_PED(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	INT i
	PLAYER_INDEX playerPackage
	REPEAT MAX_GANG_BOSS_GB_PACKAGES i
		IF NOT IS_ANYONE_HOLDING_GB_PACKAGE(serverPackage, i, playerPackage)
			IF IS_GB_PACKAGE_ATTACHED_TO_ANY_PED(serverPackage, i)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DRAW_GPS_ROUTE_TO_GB_PACKAGE(INT iPackage, STRUCT_LOCAL_GB_PACKAGE_DATA &localPackage, STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	GB_UI_LEVEL uiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
	
	IF NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		IF uiLevel < GB_UI_LEVEL_FULL
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE_NO_GPS)
		RETURN FALSE
	ENDIF
	
	INT i
	
	IF IS_ENTITY_IN_WATER(GET_GB_PACKAGE_OBJ(serverPackage, iPackage))
	//	PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_BLIPS] Don't do gps to package ", iPackage, " as in water")
		RETURN FALSE
	ENDIF
	
	REPEAT serverPackage.options.iNumberOfPackages i
		IF i != iPackage
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GB_PACKAGE_NI(serverPackage, i))
				IF DOES_BLIP_EXIST(localPackage.blipPackage[i])
					IF DOES_BLIP_HAVE_GPS_ROUTE(localPackage.blipPackage[i])
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF DOES_PART_HAVE_ANY_GB_PACKAGE(PARTICIPANT_ID_TO_INT())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_GB_PACKAGE_BLIPS(STRUCT_LOCAL_GB_PACKAGE_DATA &localPackage, STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	INT i
	TEXT_LABEL_15 tl15BlipName
	PLAYER_INDEX playerPackage
	
	IF IS_BIT_SET(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
		REPEAT MAX_GANG_BOSS_GB_PACKAGES i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_GB_PACKAGE_NI(serverPackage, i))
				IF DOES_PART_HAVE_GB_PACKAGE(PARTICIPANT_ID_TO_INT(), i)
					IF DOES_BLIP_EXIST(localPackage.blipPackage[i])	
						IF DOES_BLIP_HAVE_GPS_ROUTE(localPackage.blipPackage[i])
							SET_BLIP_ROUTE(localPackage.blipPackage[i], FALSE)
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_BLIPS] I have a package - Removed GPS route for package ", i)
						ENDIF
					ENDIF
					
					//-- DOn't blip if I've got the package
					IF DOES_BLIP_EXIST(localPackage.blipPackage[i])	
						REMOVE_BLIP(localPackage.blipPackage[i])
						PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_BLIPS] Removed blip for package ", i, " as I have a package")
					ENDIF
				ELSE
					IF NOT IS_ANYONE_HOLDING_GB_PACKAGE(serverPackage, i, playerPackage)
						IF NOT IS_GB_PACKAGE_ATTACHED_TO_ANY_PED(serverPackage, i)
							IF IS_BIT_SET(localPackage.iPackageBitset, LOCAL_GB_PACKAGE_ATTACHED_TO_NPC + i)
								PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_BLIPS] Package was attached ", i, " removing blip so it can be re-added")
								IF DOES_BLIP_EXIST(localPackage.blipPackage[i])
									REMOVE_BLIP(localPackage.blipPackage[i])
								ENDIF
								CLEAR_BIT(localPackage.iPackageBitset, LOCAL_GB_PACKAGE_ATTACHED_TO_NPC + i)
							ENDIF
							IF NOT DOES_BLIP_EXIST(localPackage.blipPackage[i])
								localPackage.blipPackage[i] = ADD_BLIP_FOR_ENTITY(GET_GB_PACKAGE_OBJ(serverPackage, i))
								
								IF SHOULD_DRAW_GPS_ROUTE_TO_GB_PACKAGE(i, localPackage, serverPackage)
									SET_BLIP_ROUTE(localPackage.blipPackage[i], TRUE)
									PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_BLIPS] adding GPS for package blip ", i)
								ELSE
									PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_BLIPS] not adding GPS for package blip ", i, " as SHOULD_DRAW_GPS_ROUTE_TO_GB_PACKAGE")
								ENDIF
								
								BLIP_SPRITE sprite = GET_SPRITE_FOR_GB_PACKAGE(serverPackage)
								IF sprite != RADAR_TRACE_INVALID
									SET_BLIP_SPRITE(localPackage.blipPackage[i], sprite)
								ELSE
									SET_BLIP_SCALE(localPackage.blipPackage[i], 0.5)
								ENDIF
								SET_BLIP_COLOUR(localPackage.blipPackage[i], BLIP_COLOUR_GREEN)
								tl15BlipName = GET_GB_PACKAGE_BLIP_NAME(localPackage)
								SET_BLIP_NAME_FROM_TEXT_FILE(localPackage.blipPackage[i], tl15BlipName)
								PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_BLIPS] Added blip for package ", i)
							ELSE
								IF NOT DOES_BLIP_HAVE_GPS_ROUTE(localPackage.blipPackage[i])
									IF SHOULD_DRAW_GPS_ROUTE_TO_GB_PACKAGE(i, localPackage, serverPackage)
										SET_BLIP_ROUTE(localPackage.blipPackage[i], TRUE)
										PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_BLIPS] Adding GPS for package blip ", i, " as SHOULD_DRAW_GPS_ROUTE_TO_GB_PACKAGE")
									ENDIF
								ELSE
									IF NOT SHOULD_DRAW_GPS_ROUTE_TO_GB_PACKAGE(i, localPackage, serverPackage)
										SET_BLIP_ROUTE(localPackage.blipPackage[i], FALSE)
										PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_BLIPS] Removing GPS for package blip ", i, " as SHOULD_DRAW_GPS_ROUTE_TO_GB_PACKAGE")
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT DOES_BLIP_EXIST(localPackage.blipPackage[i])
								localPackage.blipPackage[i] = ADD_BLIP_FOR_ENTITY(GET_GB_PACKAGE_OBJ(serverPackage, i))
								
								//SET_BLIP_ROUTE(localPackage.blipPackage[i], TRUE)
								BLIP_SPRITE sprite = GET_SPRITE_FOR_GB_PACKAGE(serverPackage)
								IF sprite != RADAR_TRACE_INVALID
									SET_BLIP_SPRITE(localPackage.blipPackage[i], sprite)
								ENDIF
								SET_BLIP_COLOUR(localPackage.blipPackage[i], BLIP_COLOUR_RED)
								SET_BLIP_ROUTE_COLOUR(localPackage.blipPackage[i], BLIP_COLOUR_RED)
								tl15BlipName = GET_GB_PACKAGE_BLIP_NAME(localPackage)
								SET_BLIP_NAME_FROM_TEXT_FILE(localPackage.blipPackage[i], tl15BlipName)
								SET_BIT(localPackage.iPackageBitset, LOCAL_GB_PACKAGE_ATTACHED_TO_NPC + i)
								PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_BLIPS] Added blip for ATTACHED package ", i)
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(localPackage.blipPackage[i])	
							REMOVE_BLIP(localPackage.blipPackage[i])
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_BLIPS] Removed blip for package ", i, " as this player is holding it ", GET_PLAYER_NAME(playerPackage))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC



FUNC MODEL_NAMES GET_GB_PACKAGE_MODEL(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	RETURN serverpackage.options.mPackage
ENDFUNC

FUNC BOOL CREATE_GB_PACKAGE(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, STRUCT_LOCAL_GB_PACKAGE_DATA &localPackage, VECTOR vLocation, INT index, BOOL bAttach = FALSE, NETWORK_INDEX niPed = NULL, BOOL bForceCoords = FALSE)

	MODEL_NAMES mPackage = serverPackage.options.mPackage
	SPAWN_SEARCH_PARAMS SpawnSearchParams
	VECTOR vCreate
	SpawnSearchParams.bConsiderInteriors = FALSE
	SpawnSearchParams.fMinDistFromPlayer = 10.0
	
	
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverPackage.niPackage[index])
		IF mPackage != DUMMY_MODEL_FOR_SCRIPT
			REQUEST_MODEL(mPackage)
			IF HAS_MODEL_LOADED(mPackage)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF bAttach
						IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niPed)
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] FALSE as want to attach but NI doesn't exist!")
							RETURN FALSE
						ENDIF
					ENDIF
		
					vCreate = vLocation
					IF NOT HAS_NET_TIMER_STARTED(serverPackage.timeSpawnPackage[index])
						START_NET_TIMER(serverPackage.timeSpawnPackage[index])
					ENDIF
					
					IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vCreate, 5.0, localpackage.vCreateEntityCurrentAttemptedCoords[index], localPackage.fCreateEntityCurrentAttemptedHeading[index], SpawnSearchParams)
					OR IS_BIT_SET(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE_FORCE_SPAWN)
						IF IS_BIT_SET(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE_FORCE_SPAWN)
							localpackage.vCreateEntityCurrentAttemptedCoords[index] = vCreate
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] - USING vCreateEntityCurrentAttemptedCoords = vCreate = ", localpackage.vCreateEntityCurrentAttemptedCoords[index], " AS biS_ForceSpawnPackage")
						ENDIF
						serverPackage.niPackage[index] = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_FIXED_INCAR, localpackage.vCreateEntityCurrentAttemptedCoords[index], TRUE, mPackage))
						SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverPackage.niPackage[index], TRUE)

						SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverPackage.niPackage[index]), localpackage.vCreateEntityCurrentAttemptedCoords[index]+<< 0.0, 0.0, 0.5 >>)
						SET_MODEL_AS_NO_LONGER_NEEDED(mPackage)
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverPackage.niPackage[index]), TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverPackage.niPackage[index]), TRUE)
						
						
						PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] PACKAGE ", index, " CREATED AT ", localpackage.vCreateEntityCurrentAttemptedCoords[index])
						
						IF bAttach
							
						ENDIF
						
						IF bForceCoords
							SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverPackage.niPackage[index]), vCreate+<< 0.0, 0.0, 0.5 >>)
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] - FORCED COORDS vCreate = ", vCreate)
						ENDIF
					ELSE
						PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] WAITING FOR GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY")
						IF NOT IS_BIT_SET(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE_FORCE_SPAWN)
							IF HAS_NET_TIMER_EXPIRED(serverPackage.timeSpawnPackage[index], 15000)	
								SET_BIT(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE_FORCE_SPAWN)
								PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] SET biS_ForceSpawnPackage")
							ENDIF
						ENDIF
								
						
					ENDIF
				ELSE
					PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] WAITING FOR CAN_REGISTER_MISSION_OBJECTS")
				ENDIF
			ELSE
				PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] WAITING FOR HAS_MODEL_LOADED")
			ENDIF
		ELSE
			PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] model is DUMMY_MODEL_FOR_SCRIPT!, Package ", index) 
		ENDIF
	ENDIF
	
	IF bAttach
		IF NOT IS_BIT_SET(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE0_ATTACHED_WHEN_CREATED + index)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverPackage.niPackage[index])
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niPed)
					IF NOT IS_NET_PED_INJURED(niPed)
						IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(niPed)
							NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(niPed)
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] requesting control of ped to attach package ", index)
						ENDIF
						
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niPed)
							ATTACH_PORTABLE_PICKUP_TO_PED(NET_TO_OBJ(serverPackage.niPackage[index]),NET_TO_PED(niPed)) 
							SET_BIT(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE0_ATTACHED_WHEN_CREATED + index)
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] PACKAGE ", index, "ATTACHED TO PED ")
						ELSE
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] WANT TO ATTACH PACKAGE ", index, " TO AI PED BUT don't have control")
						ENDIF
					
					ELSE
						PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] WANT TO ATTACH PACKAGE TO AI PED  BUT PED INJURED")
					ENDIF
				ELSE
					PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE] WANT TO ATTACH PACKAGE TO AI PED  BUT PED DOESN'T EXIST")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverPackage.niPackage[index])
		IF NOT bAttach
			RETURN TRUE
		ELSE
			IF IS_BIT_SET(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE0_ATTACHED_WHEN_CREATED + index)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL CREATE_GB_PACKAGE_AS_DROPPED_CRATE(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, VECTOR vLocation, INT index)
//	VECTOR vCrateDropOffset = <<0, 0, 100>>
	MODEL_NAMES mPackage = serverPackage.options.mPackage
	MODEL_NAMES mChute = P_CARGO_CHUTE_S
	
    //Create the Crate
    IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverPackage.niPackage[index])     
		IF mPackage != DUMMY_MODEL_FOR_SCRIPT
			REQUEST_MODEL(mPackage)
			REQUEST_MODEL(mChute)
			IF HAS_MODEL_LOADED(mPackage)
				IF CAN_REGISTER_MISSION_OBJECTS(2)
			        serverPackage.niPackage[index] = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_UNFIXED_INCAR, vLocation, FALSE, mPackage))
			       // SET_ENTITY_HEADING(NET_TO_OBJ(serverPackage.niPackage[index]), GET_EXPORT_ENTITY_SPAWN_HEADING(GET_VEHICLE_EXPORT_MISSION_VARIATION(), serverBD.sExportEntity.eLocation, FALSE, iExportEntity))
			        SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverPackage.niPackage[index], TRUE)
			                    
			        SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverPackage.niPackage[index]), TRUE)
					SET_ENTITY_HEALTH(NET_TO_OBJ(serverPackage.niPackage[index]), 50)
			        SET_OBJECT_FORCE_VEHICLES_TO_AVOID(NET_TO_OBJ(serverPackage.niPackage[index]), TRUE)
			        
			        SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverPackage.niPackage[index]), TRUE)
			        SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverPackage.niPackage[index]), TRUE)
			        ACTIVATE_PHYSICS(NET_TO_OBJ(serverPackage.niPackage[index]))
			        SET_ENTITY_VELOCITY(NET_TO_OBJ(serverPackage.niPackage[index]), <<0, 0, -0.2>>)
			        
			        PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverPackage.niPackage[index]), TRUE)
			        SET_ENTITY_PROOFS(NET_TO_OBJ(serverPackage.niPackage[index]), TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE )
			        SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverPackage.niPackage[index]), 1200)  
					SET_PICKUP_COLLIDES_WITH_PROJECTILES(NET_TO_ENT(serverPackage.niPackage[index]), TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_ENT(serverPackage.niPackage[index]), TRUE)
					PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE_AS_DROPPED_CRATE] PACKAGE ", index, " CREATED AT ", vLocation)
				ENDIF
			ENDIF
		ENDIF
    ENDIF
      
    //Create Chute
    IF NETWORK_DOES_NETWORK_ID_EXIST(serverPackage.niPackage[index])
	    IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverPackage.niChute[index])
			IF HAS_MODEL_LOADED(mChute)
				IF HAS_ANIM_DICT_LOADED("P_cargo_chute_S")
		            IF CREATE_NET_OBJ(serverPackage.niChute[index], mChute, (GET_ENTITY_COORDS(NET_TO_OBJ(serverPackage.niPackage[index]))+<<0, 0, 1>>))  
		                //SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverPackage.niChute[index], TRUE)
		          
		                
		          		PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE_AS_DROPPED_CRATE] Created chute for crate ", index)
		                ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverPackage.niChute[index]), NET_TO_OBJ(serverPackage.niPackage[index]), 0, <<0, 0, 0>>, <<0, 0, 0>>)
		                SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverPackage.niChute[index]), TRUE)
		                SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverPackage.niChute[index]), FALSE)
		                PLAY_ENTITY_ANIM(NET_TO_OBJ(serverPackage.niChute[index]), "P_cargo_chute_S_deploy", "P_cargo_chute_S", INSTANT_BLEND_IN, FALSE, FALSE)
		                FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(serverPackage.niChute[index]))
		                
		                SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverPackage.niChute[index]), 1200)
		    
		                RESET_NET_TIMER(serverPackage.InAirCheckDelayTimer[index])
		                
		                
		            ENDIF
				ELSE
					PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [CREATE_GB_PACKAGE_AS_DROPPED_CRATE] Waiting for anim dict P_cargo_chute_S")
				ENDIF
			ENDIF
	    ENDIF
    ENDIF
      
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverPackage.niPackage[index])
	AND NETWORK_DOES_NETWORK_ID_EXIST(serverPackage.niChute[index])
		
	    RETURN TRUE
	ENDIF
	  
	RETURN FALSE
ENDFUNC
 // FEATURE_GUNRUNNING

FUNC BOOL SHOULD_LOCAL_PLAYER_DO_GB_PACKAGE_HUD()
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF iPlayer > -1
		RETURN NOT IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iGBPackageClientBitset, CLIENT_GB_GB_PACKAGE_NO_HUD)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DISABLE_LOCAL_PLAYER_GB_PACKAGE_HUD()
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF iPlayer > -1
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iGBPackageClientBitset, CLIENT_GB_GB_PACKAGE_NO_HUD)
			PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [DISABLE_LOCAL_PLAYER_GB_PACKAGE_HUD] ")
			SET_BIT(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iGBPackageClientBitset, CLIENT_GB_GB_PACKAGE_NO_HUD)
		ENDIF
	ENDIF
ENDPROC

PROC ENABLE_LOCAL_PLAYER_GB_PACKAGE_HUD()
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF iPlayer > -1
		IF IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iGBPackageClientBitset, CLIENT_GB_GB_PACKAGE_NO_HUD)
			PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [ENABLE_LOCAL_PLAYER_GB_PACKAGE_HUD] ")
			CLEAR_BIT(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iGBPackageClientBitset, CLIENT_GB_GB_PACKAGE_NO_HUD)
		ENDIF
	ENDIF
ENDPROC




PROC MAINTAIN_BOTTOM_RIGHT_GB_PACKAGE_HUD(STRUCT_LOCAL_GB_PACKAGE_DATA &localPackage, STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackages)
	IF NOT IS_BIT_SET(serverPackages.iServerPackageBitset, SERVER_GB_PACKAGES_INIT)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(serverPackages.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
		EXIT
	ENDIF
	
	IF NOT SHOULD_LOCAL_PLAYER_DO_GB_PACKAGE_HUD()
		EXIT
	ENDIF
	
	INT iPackage
	HUD_COLOURS eBoxColour[MAX_GANG_BOSS_GB_PACKAGES]
	HUD_COLOURS eCrossColour[MAX_GANG_BOSS_GB_PACKAGES]
	INT iPlayerDel
	INT iNumPackages = serverPackages.options.iNumberOfPackages
	BOOL bInGang = GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()

	
	TEXT_LABEL_15 tl15Title = localPackage.tl5HudTitle
	IF IS_STRING_NULL_OR_EMPTY(localPackage.tl5HudTitle)
		tl15Title = "BK_DEAL_PACKC"
	ENDIF
	
	REPEAT iNumPackages iPackage
		IF HAS_GB_PACKAGE_BEEN_DELIVERED(serverPackages, iPackage, iPlayerDel)
			IF NOT bInGang
				IF iPlayerDel = NATIVE_TO_INT(PLAYER_ID())
					eBoxColour[iPackage] = HUD_COLOUR_GREEN 
				ELSE
					eBoxColour[iPackage] = HUD_COLOUR_RED
				ENDIF
			ELSE
				IF GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), INT_TO_PLAYERINDEX(iPlayerDel))
					eBoxColour[iPackage] = HUD_COLOUR_GREEN
				ELSE
					eBoxColour[iPackage] = HUD_COLOUR_RED
				ENDIF
			ENDIF

		ELSE
			eBoxColour[iPackage] = HUD_COLOUR_GREYLIGHT 
		ENDIF
	ENDREPEAT
	
	DRAW_GENERIC_ELIMINATION(	iNumPackages, tl15Title, DEFAULT, 
								TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, eBoxColour[0], eBoxColour[1], eBoxColour[2], eBoxColour[3],
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 
								FALSE, FALSE, FALSE,
								FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, eCrossColour[0], eCrossColour[1], eCrossColour[2], eCrossColour[3])
								
	
ENDPROC

PROC CLEANUP_GB_PACKAGE_CLIENT(STRUCT_LOCAL_GB_PACKAGE_DATA &localPackage, STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, BOOL bDeletePackage = FALSE)
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	INT i
	NETWORK_INDEX NI
	IF iPlayer > -1
		PRINTLN("[BIKER1]     ----------> [GB_PACKAGES] [CLEANUP_GB_PACKAGE_CLIENT] Cleaning up...")
		GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iCollectedPackageBitset = 0
		GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iDeliveredPackageBitset = 0
		GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iGBPackageClientBitset = 0
		
		
		REPEAT MAX_GANG_BOSS_GB_PACKAGES i
			IF DOES_BLIP_EXIST(localPackage.blipPackage[i])	
				REMOVE_BLIP(localPackage.blipPackage[i])
				PRINTLN("[BIKER1]     ---------->   [GB_PACKAGES] [CLEANUP_GB_PACKAGE_CLIENT] Removed package blip ", i)
			ENDIF
			
			IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_GB_PACKAGE_NI(serverPackage, i))
					IF IS_ENTITY_ATTACHED_TO_ENTITY(GET_GB_PACKAGE_OBJ(serverPackage, i), PLAYER_PED_ID())
						DETACH_PORTABLE_PICKUP_FROM_PED(GET_GB_PACKAGE_OBJ(serverPackage, i))
					ENDIF
					PREVENT_COLLECTION_OF_PORTABLE_PICKUP(GET_GB_PACKAGE_OBJ(serverPackage, i), TRUE, TRUE)
					IF bDeletePackage
						HIDE_PORTABLE_PICKUP_WHEN_DETACHED(GET_GB_PACKAGE_OBJ(serverPackage, i), TRUE)
						NI = GET_GB_PACKAGE_NI(serverPackage, i)
						IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(NI)
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] I have cleaned up package ", i)
							CLEANUP_NET_ID(NI)
						ELSE
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] I have deleted package ", i)
							DELETE_NET_ID(NI)
						ENDIF
					ENDIF
					CLEAR_BIT(localPackage.iPackageNeedsCleanedUpBitset, i)
				ENDIF
			ENDIF
		ENDREPEAT
		
		GB_SET_LOCAL_PLAYER_CARRYING_CRITICAL_PACKAGE(FALSE)
		
		RESET_ALL_PLAYER_BLIPS(localPackage, serverPackage)
	ENDIF
ENDPROC

PROC INIT_GANG_BOSS_GB_PACKAGE_CLIENT(STRUCT_LOCAL_GB_PACKAGE_DATA &localData, STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage)
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	INT i
	IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iGBPackageClientBitset, CLIENT_GB_GB_PACKAGE_INIT)
		
		GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iCollectedPackageBitset = 0
		GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iDeliveredPackageBitset = 0
		
		REPEAT MAX_GANG_BOSS_GB_PACKAGES i
			localData.iTempPackageHolder[i] = -2
		ENDREPEAT
		
		SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(serverPackage.options.mPackage, 1)
		
		SET_BIT(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iGBPackageClientBitset, CLIENT_GB_GB_PACKAGE_INIT)
		PRINTLN("[BIKER1]     ----------> [GB_PACKAGES] [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] [INIT_GANG_BOSS_GB_PACKAGE_CLIENT] Done")
	ENDIF
ENDPROC

PROC MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES(STRUCT_LOCAL_GB_PACKAGE_DATA &localData, STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, BOOL bClearObjectiveTextOnUpdate = FALSE, BOOL b_CheckCutsceneRunning = FALSE)
	
	
	INIT_GANG_BOSS_GB_PACKAGE_CLIENT(localData, serverPackage)
	
	MAINTAIN_GB_PACKAGES_IN_SHOPS(serverPackage)
	
	IF NOT IS_BIT_SET(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGES_INIT)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE_ALLOW_COLLECT)
		EXIT
	ENDIF
	
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
		ALLOW_LOCAL_PLAYER_TO_COLLECT_ALL_GB_PACKAGES(localData, serverPackage)
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 100 = 0
			PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] Can't collect packages because GB_GET_PLAYER_UI_LEVEL = ", GB_GET_UI_LEVEL_STRING(GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())))
		ENDIF
		#ENDIF
	ENDIF
	
	INT i
	NETWORK_INDEX NI
	REPEAT MAX_GANG_BOSS_GB_PACKAGES i
		IF NOT IS_BIT_SET(localData.iPackageNeedsCleanedUpBitset, i)
			COMMON_DROP_GB_PACKAGE_CHECKS(serverPackage, i)
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
					IF NOT DOES_PART_HAVE_GB_PACKAGE(PARTICIPANT_ID_TO_INT(), i)
						IF IS_ENTITY_ATTACHED_TO_ENTITY(GET_GB_PACKAGE_OBJ(serverPackage, i), PLAYER_PED_ID())
							//-- I have the package
							SET_LOCAL_PART_HAS_GB_PACKAGE(i)
							
							IF NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
								SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(TRUE)
								PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES]  IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY() = TRUE")
							ENDIF
							
							//-- Pickup sound
							localData.iPickupSound = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(localData.iPickupSound, "Pickup_Briefcase", "GTAO_Biker_Modes_Soundset", FALSE) 
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] DOING COLLECT PICKUP AUDIO... iPickupSound = ", localData.iPickupSound)
							

						ENDIF
					ELSE
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(GET_GB_PACKAGE_OBJ(serverPackage, i), PLAYER_PED_ID())
							//-- I have dropped the package
							CLEAR_LOCAL_PART_HAS_GB_PACKAGE(i)
							
							IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
								SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
								PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES]  IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY() = FALSE")
							ENDIF
							
							SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)")
						
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//-- I'm injured
				IF DOES_PART_HAVE_GB_PACKAGE(PARTICIPANT_ID_TO_INT(), i)
					CLEAR_LOCAL_PART_HAS_GB_PACKAGE(i)
					PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES]  I no longer have package ", i, " - injured")
				ENDIF
			ENDIF
		ELSE
			//-- Package needs cleaned up
			IF DOES_GB_PACKAGE_NI_EXIST(serverPackage, i)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_GB_PACKAGE_NI(serverPackage, i))
					IF IS_ENTITY_ATTACHED_TO_ENTITY(GET_GB_PACKAGE_OBJ(serverPackage, i), PLAYER_PED_ID())
						DETACH_PORTABLE_PICKUP_FROM_PED(GET_GB_PACKAGE_OBJ(serverPackage, i))
					ENDIF
					PREVENT_COLLECTION_OF_PORTABLE_PICKUP(GET_GB_PACKAGE_OBJ(serverPackage, i), TRUE, TRUE)
					HIDE_PORTABLE_PICKUP_WHEN_DETACHED(GET_GB_PACKAGE_OBJ(serverPackage, i), TRUE)
					NI = GET_GB_PACKAGE_NI(serverPackage, i)
					IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(NI)
						PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] I have cleaned up package ", i)
						CLEANUP_NET_ID(NI)
					ELSE
						PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] I have deleted package ", i)
						DELETE_NET_ID(NI)
					ENDIF
					CLEAR_BIT(localData.iPackageNeedsCleanedUpBitset, i)
				ENDIF
			ENDIF
		ENDIF
		
		
		//-- Blip remote players carrying package with package blip
		PLAYER_INDEX playerTemp
		BLIP_SPRITE sprite
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.packagesGB.iGBPackageClientBitset, CLIENT_GB_GB_PACKAGE_NO_HUD)
		OR GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) >= GB_UI_LEVEL_BACKGROUND
			IF localData.iTempPackageHolder[i] != serverPackage.iCurrentpackageHolder[i]
				IF serverPackage.iCurrentpackageHolder[i] = -1
					//-- Package dropped, clear remote player blip
					IF localData.iTempPackageHolder[i] > -1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(localData.iTempPackageHolder[i]))
							playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(localData.iTempPackageHolder[i]))
							IF playerTemp != INVALID_PLAYER_INDEX()
								IF playerTemp != PLAYER_ID()
									sprite = GET_SPRITE_FOR_GB_PACKAGE(serverPackage)
									SET_PLAYER_BLIP_AS_LONG_RANGE(playerTemp, FALSE)
									FORCE_BLIP_PLAYER(playerTemp, FALSE, FALSE)
									IF sprite != RADAR_TRACE_INVALID
										SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerTemp, sprite, FALSE)
									ENDIF
									IF serverPackage.options.iBlipColourFriendlyPlayerWithPackage = 0
										SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerTemp, g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
									ENDIF
									
									IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(playerTemp, PLAYER_ID())
										SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerTemp, BLIP_COLOUR_RED, FALSE)
										PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] Removed custom blip / cleared red for player ", GET_PLAYER_NAME(playerTemp), " as package ", i, " was dropped")
									ELSE
										IF serverPackage.options.iBlipColourFriendlyPlayerWithPackage != 0
											SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerTemp, serverPackage.options.iBlipColourFriendlyPlayerWithPackage, FALSE)
											PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] Reset custom blip colour for player ", GET_PLAYER_NAME(playerTemp), " as package ", i, " was dropped")
										ENDIF
										PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] Removed custom blip for player ", GET_PLAYER_NAME(playerTemp), " as package ", i, " was dropped")
									ENDIF
									
									
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] localData.iTempPackageHolder[i] > -1 but part not active!")
						ENDIF
					ENDIF
					IF localData.iTempPackageHolder[i] >= 0
						IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
							localData.iPickupSound = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(localData.iPickupSound, "Blip_Pickup", "GTAO_Biker_Modes_Soundset", FALSE) 
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] DOING Dropped PICKUP AUDIO... iPickupSound = ", localData.iPickupSound)
						ELSE
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] Not doing Dropped PICKUP AUDIO..")
						ENDIF
					ENDIF
				ELSE
					//-- Deal with package holder updating without ever being -1 / classed as dropped
					IF localData.iTempPackageHolder[i] > -1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(localData.iTempPackageHolder[i]))
							playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(localData.iTempPackageHolder[i]))
							IF playerTemp != INVALID_PLAYER_INDEX()
								IF playerTemp != PLAYER_ID()
									sprite = GET_SPRITE_FOR_GB_PACKAGE(serverPackage)
									SET_PLAYER_BLIP_AS_LONG_RANGE(playerTemp, FALSE)
									FORCE_BLIP_PLAYER(playerTemp, FALSE, FALSE)
									IF sprite != RADAR_TRACE_INVALID
										SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerTemp, sprite, FALSE)
									ENDIF
									
									IF serverPackage.options.iBlipColourFriendlyPlayerWithPackage = 0
										SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerTemp, g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
									ENDIF
									
									IF NOT IS_PACKAGE_HOLDER_IN_MY_GANG(serverPackage, i)
										SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerTemp, BLIP_COLOUR_RED, FALSE)
									ELSE
										IF serverPackage.options.iBlipColourFriendlyPlayerWithPackage != 0
											SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerTemp, serverPackage.options.iBlipColourFriendlyPlayerWithPackage, FALSE)
										ENDIF
									//	SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerTemp, BLIP_COLOUR_GREEN, FALSE)
									ENDIF
									PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] Removed custom blip for player ", GET_PLAYER_NAME(playerTemp), " as package ", i, " now carried by a different player")
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] localData.iTempPackageHolder[i] > -1 but part not active! (2)")
						ENDIF
					ENDIF
					
					playerTemp = GET_GB_PACKAGE_HOLDER_FROM_SERVER_AS_PLAYER(serverPackage, i)
					IF playerTemp != INVALID_PLAYER_INDEX()
						IF playerTemp != PLAYER_ID()
							sprite = GET_SPRITE_FOR_GB_PACKAGE(serverPackage)
							IF sprite != RADAR_TRACE_INVALID
								SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerTemp, sprite, TRUE)
							ENDIF
							SET_PLAYER_BLIP_AS_LONG_RANGE(playerTemp, TRUE)
							FORCE_BLIP_PLAYER(playerTemp, TRUE, TRUE)
							IF serverPackage.options.iBlipColourFriendlyPlayerWithPackage = 0
								SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerTemp, 1.0, TRUE)
							ENDIF
							IF NOT IS_PACKAGE_HOLDER_IN_MY_GANG(serverPackage, i)
								SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerTemp, BLIP_COLOUR_RED, TRUE)
								
								IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
									localData.iPickupSound = GET_SOUND_ID()
									PLAY_SOUND_FRONTEND(localData.iPickupSound, "Enemy_Pickup_Briefcase", "GTAO_Biker_Modes_Soundset", FALSE) 
									PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] DOING enemy pickup AUDIO... iPickupSound = ", localData.iPickupSound)
								ELSE
									PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] Not DOING enemy pickup  PICKUP AUDIO..")
								ENDIF
							ELSE
								IF serverPackage.options.iBlipColourFriendlyPlayerWithPackage != 0
									PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] Set player in my gang with custom blip colour ", serverPackage.options.iBlipColourFriendlyPlayerWithPackage)
									SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerTemp, serverPackage.options.iBlipColourFriendlyPlayerWithPackage, TRUE)
								ENDIF
								
							//	SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerTemp, BLIP_COLOUR_GREEN, TRUE)
								IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
									localData.iPickupSound = GET_SOUND_ID()
									PLAY_SOUND_FRONTEND(localData.iPickupSound, "Pickup_Briefcase", "GTAO_Biker_Modes_Soundset", FALSE) 
									PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] DOING buddy pickup AUDIO... iPickupSound = ", localData.iPickupSound)
								ELSE
									PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] Not DOING buddy pickup  PICKUP AUDIO..")
								ENDIF
							ENDIF
								
							PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] Set custom blip for player ", GET_PLAYER_NAME(playerTemp), " as they have package ", i)
						ENDIF
					ENDIF
				ENDIF
				
				localData.iTempPackageHolder[i] = serverPackage.iCurrentpackageHolder[i]
				
				IF bClearObjectiveTextOnUpdate
				//	Clear_Any_Objective_Text_From_This_Script()
				ENDIF
				#IF IS_DEBUG_BUILD
				playerTemp = GET_GB_PACKAGE_HOLDER_FROM_SERVER_AS_PLAYER(serverPackage, i)
				IF playerTemp != INVALID_PLAYER_INDEX()
					PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] iTempPackageHolder[", i, "] updated to ", localData.iTempPackageHolder[i], " who is player ", GET_PLAYER_NAME(playerTemp))
				ELSE
					PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] iTempPackageHolder[", i, "] updated to ", localData.iTempPackageHolder[i], " - dropped ")
				ENDIF
				#ENDIF
			ENDIF
		ELSE
			IF localData.iTempPackageHolder[i] != -2
				localData.iTempPackageHolder[i] = -2
			ENDIF
		ENDIF
		
		
		//-- Remote player delivered audio
		IF localData.iTempPackageDelivered[i] != serverPackage.iDeliveredByPlayer[i]
			IF serverPackage.iDeliveredByPlayer[i] >= 0
				IF serverPackage.options.iNumberOfPackages > 1
					IF GET_NUMBER_OF_UNDELIVERED_GB_PACKAGES(serverpackage) > 0
						IF serverPackage.iDeliveredByPlayer[i] != NATIVE_TO_INT(PLAYER_ID())
							playerTemp = INT_TO_PLAYERINDEX(serverPackage.iDeliveredByPlayer[i])
							IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), playerTemp)
								localData.iDeliveredSound = GET_SOUND_ID()
								PLAY_SOUND_FRONTEND(localData.iDeliveredSound, "Deliver_Item", "GTAO_Biker_Modes_Soundset", FALSE) 
								PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] Playing friend delivered audio for package ", i, " delivered by ", GET_PLAYER_NAME(playerTemp))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
									
			localData.iTempPackageDelivered[i] = serverPackage.iDeliveredByPlayer[i]
			PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] iTempPackageDelivered[", i, "] updated to ", serverPackage.iDeliveredByPlayer[i])
		ENDIF
	ENDREPEAT
	
	
	
		
	//-- Check for delivery
	INT iMyPackage
	VECTOR vDropOff
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			IF DOES_PART_HAVE_ANY_GB_PACKAGE(PARTICIPANT_ID_TO_INT())
				iMyPackage = GET_GB_PACKAGE_HELD_BY_PART(PARTICIPANT_ID_TO_INT())
				IF iMyPackage > -1
					IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iDeliveredPackageBitset, iMyPackage)
						vDropOff = serverPackage.options.vDropOff
						IF NOT ARE_VECTORS_EQUAL(vDropOff, << 0.0, 0.0, 0.0>>)
							IF (IS_ENTITY_AT_COORD( PLAYER_PED_ID(), vDropOff, <<2.0, 2.0,LOCATE_SIZE_HEIGHT>>, FALSE) AND !b_CheckCutsceneRunning)
							OR (b_CheckCutsceneRunning AND  g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene)
								IF serverPackage.options.iNumberOfPackages > 1
									IF GET_NUMBER_OF_UNDELIVERED_GB_PACKAGES(serverpackage) > 1
										localData.iDeliveredSound = GET_SOUND_ID()
										PLAY_SOUND_FRONTEND(localData.iDeliveredSound, "Deliver_Item", "GTAO_Biker_Modes_Soundset", FALSE) 
										PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] Playing delivered audio for package ", iMyPackage)
									ENDIF
								ENDIF
								START_NET_TIMER(localData.timeDelivered)
								SET_BIT(GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.packagesGB.iDeliveredPackageBitset, iMyPackage)
								CLEAR_LOCAL_PART_HAS_GB_PACKAGE(iMyPackage)
								SET_BIT(localData.iPackageNeedsCleanedUpBitset, iMyPackage)
								PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] - [MAINTAIN_LOCAL_PLAYER_WITH_GB_PACKAGES] I have delivered package ", iMypackage)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	MAINTAIN_BOTTOM_RIGHT_GB_PACKAGE_HUD(localData, serverPackage)
ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_JUST_DELIVERED_A_GB_PACKAGE(STRUCT_LOCAL_GB_PACKAGE_DATA &localData)
	IF HAS_NET_TIMER_STARTED(localData.timeDelivered)
		IF NOT HAS_NET_TIMER_EXPIRED(localData.timeDelivered, 500)
			RESET_NET_TIMER(localData.timeDelivered)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INIT_GB_PACKAGES_SERVER(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackages )
	INT i
	
	IF NOT IS_BIT_SET(serverPackages.iServerPackageBitset, SERVER_GB_PACKAGES_INIT)
		PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [INIT_GB_PACKAGES_SERVER] Called")
		REPEAT MAX_GANG_BOSS_GB_PACKAGES i
			serverPackages.iDeliveredByPlayer[i] = -1
			serverPackages.iDestroyedByPlayer[i] = -1
			serverPackages.iCurrentpackageHolder[i] = -1
		ENDREPEAT
		SET_BIT(serverPackages.iServerPackageBitset, SERVER_GB_PACKAGES_INIT)
	ENDIF
ENDPROC

PROC MAINTAIN_COLLECT_GB_PACKAGE_SERVER(STRUCT_COLLECT_GB_PACKAGES_SERVER &serverPackage, STRUCT_COLLECT_GB_PACKAGE_OPTIONS packageOptions)
	INIT_GB_PACKAGES_SERVER(serverPackage)
//	INT i
	
	IF serverPackage.options.iNumberOfPackages != packageOptions.iNumberOfPackages
		serverPackage.options.iNumberOfPackages = packageOptions.iNumberOfPackages
		PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_COLLECT_GB_PACKAGE_SERVER] iNumberOfPackages = ", serverPackage.options.iNumberOfPackages)
	ENDIF
	
	IF serverPackage.options.mPackage != packageOptions.mPackage
		serverPackage.options.mPackage = packageOptions.mPackage
		PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_COLLECT_GB_PACKAGE_SERVER] mPackage = ", ENUM_TO_INT(serverPackage.options.mPackage))
	ENDIF
	
	IF serverPackage.options.spritePackage != packageOptions.spritePackage
		serverPackage.options.spritePackage = packageOptions.spritePackage
		PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_COLLECT_GB_PACKAGE_SERVER] spritePackage = ", ENUM_TO_INT(serverPackage.options.spritePackage))
	ENDIF

	IF serverPackage.options.iBlipColourFriendlyPlayerWithPackage != packageOptions.iBlipColourFriendlyPlayerWithPackage
		serverPackage.options.iBlipColourFriendlyPlayerWithPackage = packageOptions.iBlipColourFriendlyPlayerWithPackage
		PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_COLLECT_GB_PACKAGE_SERVER] iBlipColourFriendlyPlayerWithPackage = ", ENUM_TO_INT(serverPackage.options.iBlipColourFriendlyPlayerWithPackage))
	ENDIF
	
	IF NOT ARE_VECTORS_EQUAL(serverPackage.options.vDropOff, packageOptions.vDropOff)
		IF NOT ARE_VECTORS_EQUAL(packageOptions.vDropOff, << 0.0, 0.0, 0.0>>)
			serverPackage.options.vDropOff = packageOptions.vDropOff
			PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_COLLECT_GB_PACKAGE_SERVER] vDropOff = ", serverPackage.options.vDropOff)
		ENDIF
	ENDIF
	
	IF packageOptions.bDisableGps = TRUE
		IF NOT IS_BIT_SET(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE_NO_GPS)
			SET_BIT(serverPackage.iServerPackageBitset, SERVER_GB_PACKAGE_NO_GPS)
			PRINTLN("[BIKER1]     ---------->  [GB_PACKAGES] [MAINTAIN_COLLECT_GB_PACKAGE_SERVER] SET SERVER_GB_PACKAGE_NO_GPS")
		ENDIF
	ENDIF
	
	
	
ENDPROC

