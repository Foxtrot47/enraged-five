
USING "globals.sch"
USING "rage_builtins.sch"
USING "net_mission.sch"
USING "net_include.sch"
USING "net_random_events_public.sch"
USING "net_random_events_structs.sch"

FUNC BOOL FMRE_IS_EVENT_IN_SUITABLE_STATE_FOR_REQUEST(INT iEvent)

	SWITCH FMRE_GET_STATE(iEvent)
		CASE eFMRESTATE_INACTIVE
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC FMRE_CLEANUP(INT iEvent, FM_RANDOM_EVENT_LOCAL_SCRIPT_EVENT &sFMREScriptEventdata)

	FMRE_SET_STATE(iEvent, eFMRESTATE_INACTIVE)
	
	#IF FEATURE_HALLOWEEN_2021
	IF sFMREScriptEventdata.Events[iEvent].iType = FMMC_TYPE_PHANTOM_CAR
		CLEAR_PHANTOM_CAR_TARGET()
	ENDIF
	#ENDIF

	FM_RANDOM_EVENT_SERVER empty
	GlobalServerBD_RandomEvents.Events[iEvent] = empty
	PRINTLN("[FMRE] - [FMRE_CLEANUP] - ", iEvent)

ENDPROC

PROC FMRE_PROCESS_EVENT_UPDATE_TRIGGER(INT iCount)
	
	SCRIPT_EVENT_DATA_FM_RANDOM_EVENT_UPDATE_TRIGGER sEventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		EXIT
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()		
		IF sEventData.iInstance != -1
			IF NOT ARE_VECTORS_ALMOST_EQUAL(GlobalServerBD_RandomEvents.Events[sEventData.iInstance].vTriggerPoint, sEventData.vTrigger)
				GlobalServerBD_RandomEvents.Events[sEventData.iInstance].vTriggerPoint = sEventData.vTrigger
				PRINTLN("[FMRE_PROCESS_EVENT_UPDATE_TRIGGER] - Updating trigger for iEvent ", sEventData.iInstance, ". vTriggerPoint = ", sEventData.vTrigger)
				
				IF sEventData.fRange != -1.0
					GlobalServerBD_RandomEvents.Events[sEventData.iInstance].fTriggerRange = sEventData.fRange
					PRINTLN("[FMRE_PROCESS_EVENT_UPDATE_TRIGGER] - Updating trigger for iEvent ", sEventData.iInstance, ". fTriggerRange = ", sEventData.fRange)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC FMRE_PROCESS_EVENT_REQUEST(INT iCount, FM_RANDOM_EVENT_LOCAL_SCRIPT_EVENT & sFMREScriptEventdata)
	
	SCRIPT_EVENT_DATA_FM_RANDOM_EVENT_REQUEST sEventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		EXIT
	ENDIF
	
	PRINTLN("[FMRE] - [FMRE_PROCESS_EVENT_REQUEST] - Received request for event ", GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(sEventData.iMission), " iVariation = ", sEventData.iVariation, " iSubvariation = ", sEventData.iSubvariation, " eSafetyChecks ", sEventData.eSafetyChecks)
	
	INT iEvent
	REPEAT sFMREScriptEventdata.iNumEvents iEvent
		IF sEventData.iMission = sFMREScriptEventdata.Events[iEvent].iType
		
			IF sEventData.eSafetyChecks = eEVENTSAFETYCHECKBYPASS_ALL_PLAYERS
			OR (sEventData.eSafetyChecks = eEVENTSAFETYCHECKBYPASS_LOCAL AND sEventData.Details.FromPlayerIndex = PLAYER_ID())
				SET_FMRE_CLIENT_BIT(iEvent, eFMRECLIENTBITSET_REQUESTED)
			ENDIF
		
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				
				IF FMRE_GET_STATE(iEvent) = eFMRESTATE_AVAILABLE
					PRINTLN("[FMRE] - [FMRE_PROCESS_EVENT_REQUEST] - Event already available, resetting to match request.")
					FMRE_CLEANUP(iEvent, sFMREScriptEventdata)
				ENDIF
				
				IF NOT FMRE_IS_EVENT_IN_SUITABLE_STATE_FOR_REQUEST(iEvent)
					PRINTLN("[FMRE] - [FMRE_PROCESS_EVENT_REQUEST] - Event in unsuitable state, discarding request")
					EXIT
				ENDIF
			
				SET_FMRE_SERVER_BIT(iEvent, eFMRESERVERBITSET_REQUESTED)
				IF sEventData.iVariation != -1
					GlobalServerBD_RandomEvents.Events[iEvent].iVariation = sEventData.iVariation
					PRINTLN("[FMRE] - [FMRE_PROCESS_EVENT_REQUEST] - Forced iVariation = ",GlobalServerBD_RandomEvents.Events[iEvent].iVariation)
				ENDIF
				IF sEventData.iSubvariation != -1
					GlobalServerBD_RandomEvents.Events[iEvent].iSubvariation = sEventData.iSubvariation
					PRINTLN("[FMRE] - [FMRE_PROCESS_EVENT_REQUEST] - Forced iSubvariation = ",GlobalServerBD_RandomEvents.Events[iEvent].iSubvariation)
				ENDIF
			ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT

ENDPROC

