/* ------------------------------------------------------------------
* Name: FMMC_Corona_Anim_Scene.sch
* Author: James Adwick
* Date: 12/07/2013
* Purpose: Header which contains main functions to set up and run
* the corona animated sync scene. Seen on vehicle / betting screens
* of land races
* ------------------------------------------------------------------*/

USING "globals.sch"
USING "net_include.sch"
USING "FMMC_Corona_menu.sch"
USING "FMMC_Corona_Ped_Placement_Data.sch"

//PURPOSE: Picks a random Base for the ped
FUNC INT GET_GENERIC_PED_ANIM_BASE(TEXT_LABEL_63 &Dict, TEXT_LABEL_31 &Base, FLOAT &fRandomPhase, BOOL bJuggernaut = FALSE)
	INT iRand
	INT iToReturn
	
	IF bJuggernaut
		//MALE
		IF IS_PLAYER_MALE(PLAYER_ID())
			iRand = GET_RANDOM_INT_IN_RANGE(0, 5)
			IF iRand = 0	//Double Chance (due to having idles)
				iToReturn = 0
				Dict = "anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_A@BASE"
				Base = "BASE"
			ELIF iRand = 1	//Double Chance (due to having idles)
				iToReturn = 1
				Dict =  "anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_B@BASE"
				Base = "BASE"
			ELIF iRand = 2
				iToReturn = 2
				Dict =  "anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_C@BASE"
				Base = "BASE"
			ELIF iRand = 3
				iToReturn = 3
				Dict =  "anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_D@BASE"
				Base = "BASE"
			ELSE
				iToReturn = 4
				Dict =  "anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_E@BASE"
				Base = "BASE"
			ENDIF
		
		//FEMALE
		ELSE
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF iRand = 0	//Double Chance (due to having idles)
				iToReturn = 0
				Dict = "anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_A@BASE"
				Base = "BASE"
			ELIF iRand = 1	//Double Chance (due to having idles)
				iToReturn = 1
				Dict =  "anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_B@BASE"
				Base = "BASE"
			ELSE
				iToReturn = 2
				Dict =  "anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_C@BASE"
				Base = "BASE"
			ENDIF
		ENDIF
	ELSE
	
		//MALE
		IF IS_PLAYER_MALE(PLAYER_ID())
			iRand = GET_RANDOM_INT_IN_RANGE(0, 11)
			IF iRand <= 1	//Double Chance (due to having idles)
				iToReturn = 0
				Dict = "MP_CORONA_IDLES@MALE_A@BASE"
				Base = "BASE"
			ELIF iRand <= 3	//Double Chance (due to having idles)
				iToReturn = 1
				Dict =  "MP_CORONA_IDLES@MALE_B@BASE"
				Base = "BASE"
			ELIF iRand <= 4
				iToReturn = 2
				Dict =  "MP_CORONA_IDLES@MALE_C@BASE"
				Base = "BASE"
			ELIF iRand <= 5
				iToReturn = 3
				Dict =  "MP_CORONA_IDLES@MALE_D@BASE"
				Base = "BASE"
			ELIF iRand <= 6
				iToReturn = 4
				Dict =  "MP_CORONA_IDLES@MALE_E@BASE"
				Base = "BASE"
			ELIF iRand <= 7
				iToReturn = 5
				Dict =  "MP_CORONA@SINGLE_TEAM"
				Base = "Single_Team_Loop_two"
			ELIF iRand <= 8
				iToReturn = 6
				Dict =  "MP_CORONA@TEAM_IDLES@MALE_A"
				Base = "idle"
			ELIF iRand <= 9
				iToReturn = 7
				Dict =  "MP_CORONA@TEAM_IDLES@MALE_B"
				Base = "idle"
			ELSE
				iToReturn = 8
				Dict =  "MP_CORONA@TEAM_IDLES@MALE_C"
				Base = "idle"
			ENDIF
		
		//FEMALE
		ELSE
			iRand = GET_RANDOM_INT_IN_RANGE(0, 6)
			IF iRand <= 1	//Double Chance (due to having idles)
				iToReturn = 0
				Dict =  "MP_CORONA_IDLES@FEMALE_A@BASE"
				Base = "BASE"
			ELIF iRand <= 2
				iToReturn = 1
				Dict =  "MP_CORONA_IDLES@FEMALE_B@BASE"
				Base = "BASE"
			ELIF iRand <= 3
				iToReturn = 2
				Dict =  "MP_CORONA_IDLES@FEMALE_C@BASE"
				Base = "BASE"
			ELIF iRand <= 4
				iToReturn = 3
				Dict =  "MP_CORONA@SINGLE_TEAM"
				Base = "Single_Team_Loop_three"
			ELSE
				iToReturn = 4
				Dict =  "MP_CORONA@TEAM_IDLES@FEMALE_A"
				Base = "idle"
			ENDIF
		ENDIF
	ENDIF
	
	fRandomPhase = GET_RANDOM_FLOAT_IN_RANGE()
	
	PRINTLN("GET_GENERIC_PED_ANIM_BASE - iToReturn = ", iToReturn, " Dict = ", Dict, " Base = ", Base)	
	
	RETURN iToReturn
ENDFUNC


//PURPOSE: Requests and Loads the Corona Idle Animations
FUNC BOOL HAVE_CORONA_IDLE_ANIMS_LOADED(BOOL bIsHeist = FALSE)
	
	// We can return TRUE for heists as we don't need these
	IF bIsHeist 
		RETURN TRUE
	ENDIF
	
	REQUEST_ANIM_DICT("mp_intro_seq@ig_4_car_select@group1")
	REQUEST_ANIM_DICT("mp_intro_seq@ig_4_car_select@group2")
	REQUEST_ANIM_DICT("mp_intro_seq@ig_4_car_select@group3")
	REQUEST_ANIM_DICT("mp_intro_seq@ig_4_car_select@group4")
	REQUEST_ANIM_DICT("mp_intro_seq@ig_4_car_select@player")
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		REQUEST_ANIM_DICT("MP_INTRO_SEQ@IG_4_CAR_SELECT@")	//Lamar Anims
	ENDIF
	
	// Load required anims. 
	REQUEST_ANIM_DICT("MP_CORONA_IDLES@FEMALE_A@BASE")
	REQUEST_ANIM_DICT("MP_CORONA_IDLES@FEMALE_B@BASE")
	REQUEST_ANIM_DICT("MP_CORONA_IDLES@FEMALE_C@BASE")
	REQUEST_ANIM_DICT("MP_CORONA_IDLES@MALE_A@BASE")
	REQUEST_ANIM_DICT("MP_CORONA_IDLES@MALE_B@BASE")
	REQUEST_ANIM_DICT("MP_CORONA_IDLES@MALE_C@BASE")
	REQUEST_ANIM_DICT("MP_CORONA_IDLES@MALE_D@BASE")
	REQUEST_ANIM_DICT("MP_CORONA_IDLES@MALE_E@BASE")
	REQUEST_ANIM_DICT("MP_CORONA_IDLES@FEMALE_A@IDLE_A")
	REQUEST_ANIM_DICT("MP_CORONA_IDLES@MALE_A@IDLE_A")
	REQUEST_ANIM_DICT("MP_CORONA_IDLES@MALE_B@IDLE_A")
	REQUEST_ANIM_DICT("MP_CORONA@SINGLE_TEAM")
	REQUEST_ANIM_DICT("MP_CORONA@TEAM_IDLES@FEMALE_A")
	REQUEST_ANIM_DICT("MP_CORONA@TEAM_IDLES@MALE_A")
	REQUEST_ANIM_DICT("MP_CORONA@TEAM_IDLES@MALE_B")
	REQUEST_ANIM_DICT("MP_CORONA@TEAM_IDLES@MALE_C")
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_A@BASE")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_A@IDLE_A")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_B@BASE")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_B@IDLE_A")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_C@BASE")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_C@IDLE_A")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_D@BASE")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_D@IDLE_A")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_E@BASE")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_E@IDLE_A")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_A@BASE")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_A@IDLE_A")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_B@BASE")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_B@IDLE_A")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_C@BASE")
		REQUEST_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_C@IDLE_A")
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_4_car_select@group1")
		PRINTLN("[NETCELEBRATION] - mp_intro_seq@ig_4_car_select@group1 not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_4_car_select@group2")
		PRINTLN("[NETCELEBRATION] - mp_intro_seq@ig_4_car_select@group2 not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_4_car_select@group3")
		PRINTLN("[NETCELEBRATION] - mp_intro_seq@ig_4_car_select@group3 not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_4_car_select@group4")
		PRINTLN("[NETCELEBRATION] - mp_intro_seq@ig_4_car_select@group4 not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_4_car_select@player")
		PRINTLN("[NETCELEBRATION] - mp_intro_seq@ig_4_car_select@player not loaded")
		RETURN FALSE
	ENDIF
	
	// Check that al anims have loaded, return FALSE if any haven't. 
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA_IDLES@FEMALE_A@BASE")
		PRINTLN("[NETCELEBRATION] - MP_CORONA_IDLES@FEMALE_A@BASE not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA_IDLES@FEMALE_B@BASE")
		PRINTLN("[NETCELEBRATION] - MP_CORONA_IDLES@FEMALE_B@BASE not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA_IDLES@FEMALE_C@BASE")
		PRINTLN("[NETCELEBRATION] - MP_CORONA_IDLES@FEMALE_C@BASE not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA_IDLES@MALE_A@BASE")
		PRINTLN("[NETCELEBRATION] - MP_CORONA_IDLES@MALE_A@BASE not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA_IDLES@MALE_B@BASE")
		PRINTLN("[NETCELEBRATION] - MP_CORONA_IDLES@MALE_B@BASE not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA_IDLES@MALE_C@BASE")
		PRINTLN("[NETCELEBRATION] - MP_CORONA_IDLES@MALE_C@BASE not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA_IDLES@MALE_D@BASE")
		PRINTLN("[NETCELEBRATION] - MP_CORONA_IDLES@MALE_D@BASE not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA_IDLES@MALE_E@BASE")
		PRINTLN("[NETCELEBRATION] - MP_CORONA_IDLES@MALE_E@BASE not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA_IDLES@FEMALE_A@IDLE_A")
		PRINTLN("[NETCELEBRATION] - MP_CORONA_IDLES@FEMALE_A@IDLE_A not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA_IDLES@MALE_A@IDLE_A")
		PRINTLN("[NETCELEBRATION] - MP_CORONA_IDLES@MALE_A@IDLE_A not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA_IDLES@MALE_B@IDLE_A")
		PRINTLN("[NETCELEBRATION] - MP_CORONA_IDLES@MALE_B@IDLE_A not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA@SINGLE_TEAM")
		PRINTLN("[NETCELEBRATION] - MP_CORONA@SINGLE_TEAM not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA@TEAM_IDLES@FEMALE_A")
		PRINTLN("[NETCELEBRATION] - MP_CORONA@TEAM_IDLES@FEMALE_A not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA@TEAM_IDLES@MALE_A")
		PRINTLN("[NETCELEBRATION] - MP_CORONA@TEAM_IDLES@MALE_A not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA@TEAM_IDLES@MALE_B")
		PRINTLN("[NETCELEBRATION] - MP_CORONA@TEAM_IDLES@MALE_B not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED("MP_CORONA@TEAM_IDLES@MALE_C")
		PRINTLN("[NETCELEBRATION] - MP_CORONA@TEAM_IDLES@MALE_C not loaded")
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		//Male A
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_A@BASE")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_A@BASE not loaded")
			RETURN FALSE
		ENDIF
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_A@IDLE_A")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_A@IDLE_A not loaded")
			RETURN FALSE
		ENDIF
		
		//Male B
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_B@BASE")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_B@BASE not loaded")
			RETURN FALSE
		ENDIF
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_B@IDLE_A")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_B@IDLE_A not loaded")
			RETURN FALSE
		ENDIF
		
		//Male C
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_C@BASE")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_C@BASE not loaded")
			RETURN FALSE
		ENDIF
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_C@IDLE_A")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_C@IDLE_A not loaded")
			RETURN FALSE
		ENDIF
		
		//Male D
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_D@BASE")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_D@BASE not loaded")
			RETURN FALSE
		ENDIF
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_D@IDLE_A")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_D@IDLE_A not loaded")
			RETURN FALSE
		ENDIF
		
		//Male E
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_E@BASE")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_E@BASE not loaded")
			RETURN FALSE
		ENDIF	
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_E@IDLE_A")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_E@IDLE_A not loaded")
			RETURN FALSE
		ENDIF
		
		//Female A
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_A@BASE")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_A@BASE not loaded")
			RETURN FALSE
		ENDIF
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_A@IDLE_A")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_A@IDLE_A not loaded")
			RETURN FALSE
		ENDIF
		
		//Female B
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_B@BASE")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_B@BASE not loaded")
			RETURN FALSE
		ENDIF
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_B@IDLE_A")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_B@IDLE_A not loaded")
			RETURN FALSE
		ENDIF
		
		//Female C
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_C@BASE")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_C@BASE not loaded")
			RETURN FALSE
		ENDIF
		IF NOT HAS_ANIM_DICT_LOADED("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_C@IDLE_A")
			PRINTLN("[NETCELEBRATION] - anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_C@IDLE_A not loaded")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Removes the Corona Idle Animations from memory
PROC REMOVE_CORONA_IDLE_ANIMS()
	REMOVE_ANIM_DICT("MP_CORONA_IDLES@FEMALE_A@BASE")
	REMOVE_ANIM_DICT("MP_CORONA_IDLES@FEMALE_B@BASE")
	REMOVE_ANIM_DICT("MP_CORONA_IDLES@FEMALE_C@BASE")
	
	REMOVE_ANIM_DICT("MP_CORONA_IDLES@MALE_A@BASE")
	REMOVE_ANIM_DICT("MP_CORONA_IDLES@MALE_B@BASE")
	REMOVE_ANIM_DICT("MP_CORONA_IDLES@MALE_C@BASE")
	REMOVE_ANIM_DICT("MP_CORONA_IDLES@MALE_D@BASE")
	REMOVE_ANIM_DICT("MP_CORONA_IDLES@MALE_E@BASE")
	
	REMOVE_ANIM_DICT("MP_CORONA_IDLES@FEMALE_A@IDLE_A")
	REMOVE_ANIM_DICT("MP_CORONA_IDLES@MALE_A@IDLE_A")
	REMOVE_ANIM_DICT("MP_CORONA_IDLES@MALE_B@IDLE_A")
	
	REMOVE_ANIM_DICT("MP_CORONA@SINGLE_TEAM")
	REMOVE_ANIM_DICT("MP_CORONA@TEAM_IDLES@FEMALE_A")
	REMOVE_ANIM_DICT("MP_CORONA@TEAM_IDLES@MALE_A")
	REMOVE_ANIM_DICT("MP_CORONA@TEAM_IDLES@MALE_B")
	REMOVE_ANIM_DICT("MP_CORONA@TEAM_IDLES@MALE_C")
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_A@BASE")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_A@IDLE_A")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_B@BASE")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_B@IDLE_A")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_C@BASE")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_C@IDLE_A")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_D@BASE")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_D@IDLE_A")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_E@BASE")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@MALE_E@IDLE_A")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_A@BASE")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_A@IDLE_A")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_B@BASE")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_B@IDLE_A")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_C@BASE")
		REMOVE_ANIM_DICT("anim@MP_CORONA_IDLES_JUGGERNAUT@FEMALE_C@IDLE_A")
	ENDIF
	
	PRINTLN("[CORONA_ANIM] REMOVE_CORONA_IDLE_ANIMS - DONE")
ENDPROC

/// PURPOSE: Requests the animation dictionaries we will need for this scene. 
FUNC BOOL CORONA_SCENE_REQUEST_ASSETS()	// TODO: Limit the loading to only the amount we need

	IF HAVE_CORONA_IDLE_ANIMS_LOADED()
		IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
			IF HAS_ANIM_DICT_LOADED("MP_INTRO_SEQ@IG_4_CAR_SELECT@")
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("[CORONA_ANIM] CORONA_SCENE_REQUEST_ASSETS - Waiting for corona idle anims to load")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CORONA_SCENE_IS_VALID(CORONA_MENU_DATA &coronaMenuData)

	IF coronaMenuData.coronaCamMode = CORONA_CAMERA_MODE_SYNC_SCENE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Checks to see if corona requires a sync'd scene   
FUNC BOOL CORONA_SCENE_HAS_LOADED(CORONA_MENU_DATA &coronaMenuData)
	
	IF CORONA_SCENE_IS_VALID(coronaMenuData)
		
		IF CORONA_SCENE_REQUEST_ASSETS()
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Local wrapper for checking if corona ped is valid    
FUNC BOOL CORONA_SCENE_CHECK_VALID_PED(PED_INDEX &aPed)
	IF DOES_ENTITY_EXIST(aPed)
	AND NOT IS_ENTITY_DEAD(aPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the correct slot in the corona 
FUNC INT LOOKUP_CORONA_CLONE_PLACEMENT(INT iCount)

	SWITCH iCount
		CASE 0		RETURN 0 
		CASE 1		RETURN 2
		CASE 2		RETURN 3
		CASE 3		RETURN 4
		CASE 4		RETURN 12
		CASE 5		RETURN 7
		CASE 6		RETURN 1
		CASE 7		RETURN 8
		CASE 8		RETURN 6
		CASE 9		RETURN 5
		CASE 10		RETURN 11
		CASE 11		RETURN 9
		CASE 12		RETURN 14
		CASE 13		RETURN 10
		CASE 14		RETURN 13
		CASE 999	RETURN 999
	ENDSWITCH

	PRINTLN("[CORONA] LOOKUP_CORONA_CLONE_PLACEMENT - Requesting slot for count ", iCount)
	//SCRIPT_ASSERT("LOOKUP_CORONA_CLONE_PLACEMENT - we are requesting a slot with invalid ped count")
	RETURN 0
ENDFUNC


FUNC INT LOOKUP_CORONA_CLONE_PLACEMENT_BETTING(INT iCount)

	SWITCH iCount
		CASE 0		RETURN 14 
		CASE 1		RETURN 5
		CASE 2		RETURN 6
		CASE 3		RETURN 0
		CASE 4		RETURN 8
		CASE 5		RETURN 9
		CASE 6		RETURN 2
		CASE 7		RETURN 7
		CASE 8		RETURN 13
		CASE 9		RETURN 12
		CASE 10		RETURN 10
		CASE 11		RETURN 11
		CASE 12		RETURN 4
		CASE 13		RETURN 1
		CASE 14		RETURN 3
		CASE 999	RETURN 999
	ENDSWITCH

	PRINTLN("[CORONA] LOOKUP_CORONA_CLONE_PLACEMENT_BETTING - Requesting slot for count ", iCount)
	SCRIPT_ASSERT("LOOKUP_CORONA_CLONE_PLACEMENT_BETTING - we are requesting a slot with invalid ped count")
	RETURN 0
ENDFUNC


#IF IS_DEBUG_BUILD

PROC CORONA_SCENE_DEBUG_CLONES()
	
	IF bDebugClonesForCoronaScene
		
		INT i
		INT iCount = 0
		TEXT_LABEL_15 tlPedName
		
		FOR i = 0 TO (iDebugNumberClonesForScene) STEP 1
		
			IF NOT CORONA_SCENE_CHECK_VALID_PED(g_CoronaClonePeds.piPlayersPed[i].pedID)
			
				PRINTLN("[CORONA] CORONA_SCENE_DEBUG_CLONES - Creating clone for slot ", i)
				tlPedName = "clone_"
				tlPedName += iCount
			
				g_CoronaClonePeds.piPlayersPed[i].pedID = CLONE_PED(PLAYER_PED_ID(), FALSE, FALSE, TRUE)
				
				//SETUP_MP_SPAWN_OUTFIT_MALE(g_CoronaClonePeds.piPlayersPed[i].pedID, INT_TO_ENUM(MP_SPAWN_OUTFIT_TYPE, GET_RANDOM_INT_IN_RANGE(0, 6)))
				
				SET_PED_NAME_DEBUG(g_CoronaClonePeds.piPlayersPed[i].pedID, tlPedName)
				
				iCount++
			ENDIF
		
		ENDFOR
		
	ENDIF
	
ENDPROC
#ENDIF

/// PURPOSE: Returns the correct animation dictionary for the player value
FUNC STRING CORONA_SCENE_GET_ANIM_DICT(INT i)
	
	IF i = 999	//Lamar
		RETURN "MP_INTRO_SEQ@IG_4_CAR_SELECT@"
	ENDIF
	
	INT iMod = (i / 4)
	
	IF iMod = 0
		RETURN "mp_intro_seq@ig_4_car_select@group1"
	ELIF iMod = 1
		RETURN "mp_intro_seq@ig_4_car_select@group2"
	ELIF iMod = 2
		RETURN "mp_intro_seq@ig_4_car_select@group3"
	ELSE
		RETURN "mp_intro_seq@ig_4_car_select@group4"
	ENDIF
ENDFUNC

FUNC STRING CORONA_SCENE_GET_ANIM(INT i, BOOL bLoop)
	IF i = 999	//Lamar
		IF bLoop
			RETURN "BASE_LAMAR"
		ELSE
			RETURN "INTRO_LAMAR"
		ENDIF
	ENDIF
	
	INT iMod = i % 4
	
	IF bLoop
		IF iMod = 0
			RETURN "loop_a"
		ELIF iMod = 1
			RETURN "loop_b"
		ELIF iMod = 2
			RETURN "loop_c"
		ELSE
			RETURN "loop_d"
		ENDIF
	ELSE
		IF iMod = 0
			RETURN "intro_a"
		ELIF iMod = 1
			RETURN "intro_b"
		ELIF iMod = 2
			RETURN "intro_c"
		ELSE
			RETURN "intro_d"
		ENDIF
	ENDIF

ENDFUNC

//PURPOSE: Plays an animation to go with the comment made.
PROC PLAY_TUTORIAL_PED_COMMENT_ANIMATION(PED_INDEX TutorialPed, INT iVeh)
	IF NOT IS_PED_INJURED(TutorialPed)
		IF IS_ENTITY_PLAYING_ANIM(TutorialPed, CORONA_SCENE_GET_ANIM_DICT(999), CORONA_SCENE_GET_ANIM(999, TRUE))
			
			SEQUENCE_INDEX Seq
			
			SWITCH iVeh
				//GOOD (Generic)
				CASE -1
				CASE -2
				CASE 1	//Asterope
				CASE 9	//Tailgater
				CASE 6	//Stanier
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM(NULL, CORONA_SCENE_GET_ANIM_DICT(999),  "IDLE_A_LAMAR", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
						TASK_PLAY_ANIM(NULL, CORONA_SCENE_GET_ANIM_DICT(999),  CORONA_SCENE_GET_ANIM(999, TRUE), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(TutorialPed, seq)
					CLEAR_SEQUENCE_TASK(seq)
				BREAK
				
				//BAD
				CASE 5	//Schafter2
				CASE 7	//Superd
				CASE 10	//Washington
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM(NULL, CORONA_SCENE_GET_ANIM_DICT(999),  "IDLE_B_LAMAR", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
						TASK_PLAY_ANIM(NULL, CORONA_SCENE_GET_ANIM_DICT(999),  CORONA_SCENE_GET_ANIM(999, TRUE), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(TutorialPed, seq)
					CLEAR_SEQUENCE_TASK(seq)
				BREAK
				
				//WHY? (Joke)
				CASE 4	//Primo
				CASE 8	//Surge
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM(NULL, CORONA_SCENE_GET_ANIM_DICT(999),  "IDLE_C_LAMAR", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
						TASK_PLAY_ANIM(NULL, CORONA_SCENE_GET_ANIM_DICT(999),  CORONA_SCENE_GET_ANIM(999, TRUE), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(TutorialPed, seq)
					CLEAR_SEQUENCE_TASK(seq)
				BREAK
				
				//GOOD
				CASE 0	//Asea
				CASE 2	//Fugitive
				CASE 3	//Premier
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM(NULL, CORONA_SCENE_GET_ANIM_DICT(999),  "IDLE_D_LAMAR", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
						TASK_PLAY_ANIM(NULL, CORONA_SCENE_GET_ANIM_DICT(999),  CORONA_SCENE_GET_ANIM(999, TRUE), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(TutorialPed, seq)
					CLEAR_SEQUENCE_TASK(seq)
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Cleans up the peds from the sync scenes and removes dictionaries
PROC CORONA_SCENE_CLEANUP()

	PRINTLN("[CORONA] CORONA_SCENE_CLEANUP - removing animation and freeing ped from SYNC scene")

	REMOVE_ANIM_DICT("mp_intro_seq@ig_4_car_select@group1")
	REMOVE_ANIM_DICT("mp_intro_seq@ig_4_car_select@group2")
	REMOVE_ANIM_DICT("mp_intro_seq@ig_4_car_select@group3")
	REMOVE_ANIM_DICT("mp_intro_seq@ig_4_car_select@group4")
	REMOVE_ANIM_DICT("mp_intro_seq@ig_4_car_select@player")
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		REMOVE_ANIM_DICT("mp_intro_seq@ig_4_car_select@")
	ENDIF
	
	REMOVE_CORONA_IDLE_ANIMS()
	
		CLEAR_ALL_CORONA_PED_ANIMATION_DICTS()
		REMOVE_CORONA_IDLE_ANIMS()
ENDPROC

PROC CORONA_SCENE_ANIMATE_LOCAL_PLAYER(PED_INDEX &mainLoopedPed, VECTOR vCentrePosition, FLOAT fHeading)
	
	IF CORONA_SCENE_CHECK_VALID_PED(mainLoopedPed)
		
		FLOAT fGroundZ
		VECTOR vVecAnimOffset
		VECTOR vVecAnimRotation
	
		IF HAS_ANIM_DICT_LOADED("mp_intro_seq@ig_4_car_select@player")
	
			vVecAnimOffset = GET_ANIM_INITIAL_OFFSET_POSITION("mp_intro_seq@ig_4_car_select@player", "loop", vCentrePosition, <<0, 0, fHeading>>)
			vVecAnimRotation = GET_ANIM_INITIAL_OFFSET_ROTATION("mp_intro_seq@ig_4_car_select@player", "loop", vCentrePosition, <<0, 0, fHeading>>)
		
			//Alter Player Position if they are in the Tutorial
			IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
				vVecAnimOffset = <<-212.08, 308.45, 96.95>>
				vVecAnimRotation = <<0, 0, -63.74>>
			ENDIF
			
			IF GET_GROUND_Z_FOR_3D_COORD(vVecAnimOffset, fGroundZ)
				//PRINTLN("[CORONA] CORONA_SCENE_START_WALK_IN - Found ground z for ped: ", fGroundZ)
				vVecAnimOffset.z = fGroundZ
			ENDIF
			
//			#IF FEATURE_TUNER
//			IF IS_THIS_A_TUNER_ROBBERY_FINALE()
//				vVecAnimOffset = GET_TUNER_ROBBERY_VEHICLE_SELECT_PED_COORDS()
//				vVecAnimRotation = <<0, 0, GET_TUNER_ROBBERY_VEHICLE_SELECT_PED_HEADING()>>
//			ENDIF
//			#ENDIF
			
			// Set the player correctly
			SET_ENTITY_COORDS(mainLoopedPed, vVecAnimOffset, FALSE)
			SET_ENTITY_ROTATION(mainLoopedPed, vVecAnimRotation)
			SET_ENTITY_COLLISION(mainLoopedPed, TRUE)
			
			SET_PED_DESIRED_HEADING(mainLoopedPed, GET_ENTITY_HEADING(mainLoopedPed))
		
			SET_PED_ALTERNATE_MOVEMENT_ANIM(mainLoopedPed, AAT_IDLE, "mp_intro_seq@ig_4_car_select@player",  "loop", INSTANT_BLEND_IN, TRUE)

			FORCE_PED_MOTION_STATE(mainLoopedPed, MS_ON_FOOT_IDLE, TRUE, DEFAULT, TRUE)
			SET_PED_RESET_FLAG(mainLoopedPed, PRF_SkipOnFootIdleIntro, TRUE)
			
			PRINTLN("[CORONA] CORONA_SCENE_ANIMATE_LOCAL_PLAYER - Animated local player at coords ", vVecAnimOffset)
		ENDIF
	
		//TASK_PLAY_ANIM(mainLoopedPed, "mp_intro_seq@ig_4_car_select@player",  "loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
	ENDIF
ENDPROC

PROC CORONA_SCENE_ANIMATE_REMOTE_PLAYER(PED_INDEX &aPed, VECTOR vCentrePosition, FLOAT fHeading, INT iCount)

	IF CORONA_SCENE_CHECK_VALID_PED(aPed)
		SEQUENCE_INDEX seq
		FLOAT fGroundZ
		VECTOR vVecAnimOffset
		VECTOR vVecAnimRotation
		
		INT iSlot = LOOKUP_CORONA_CLONE_PLACEMENT(iCount)
		PRINTLN("[CORONA] CORONA_SCENE_ANIMATE_REMOTE_PLAYER - Ped count is, ", iCount, " and slot to place in is: ", iSlot)
		
		IF iCount != 999
			vVecAnimOffset = GET_ANIM_INITIAL_OFFSET_POSITION(CORONA_SCENE_GET_ANIM_DICT(iSlot), CORONA_SCENE_GET_ANIM(iSlot, FALSE), vCentrePosition, <<0, 0, fHeading>>)
			vVecAnimRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(CORONA_SCENE_GET_ANIM_DICT(iSlot), CORONA_SCENE_GET_ANIM(iSlot, FALSE), vCentrePosition, <<0, 0, fHeading>>)
		ELSE
			vVecAnimOffset = <<-215.69, 306.02, 96.97>>		//END POS -212.45, 306.66, 96.98	//<<-206.5020, 312.1027-0.3, 96.9472>>
			vVecAnimRotation = <<0, 0, -94.79>>				//END POS -37.20					//<<0, 0, 98.20>>
		ENDIF
		
		IF GET_GROUND_Z_FOR_3D_COORD(vVecAnimOffset, fGroundZ)
			//PRINTLN("[CORONA] CORONA_SCENE_ANIMATE_REMOTE_PLAYER - Found ground z for ped: ", fGroundZ)
			vVecAnimOffset.z = fGroundZ
		ENDIF
		
		OPEN_SEQUENCE_TASK(seq)
			TASK_PLAY_ANIM(NULL, CORONA_SCENE_GET_ANIM_DICT(iSlot),  CORONA_SCENE_GET_ANIM(iSlot, FALSE), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
			TASK_PLAY_ANIM(NULL, CORONA_SCENE_GET_ANIM_DICT(iSlot),  CORONA_SCENE_GET_ANIM(iSlot, TRUE), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		CLOSE_SEQUENCE_TASK(seq)
		
		PRINTLN("[CORONA] CORONA_SCENE_ANIMATE_REMOTE_PLAYER - Position: ", vVecAnimOffset, " and Rotation: ", vVecAnimRotation)
				
		SET_ENTITY_COORDS(aPed, vVecAnimOffset, FALSE)
		SET_ENTITY_ROTATION(aPed, vVecAnimRotation)
		SET_ENTITY_COLLISION(aPed, TRUE)
		SET_ENTITY_VISIBLE(aPed, TRUE)
		FREEZE_ENTITY_POSITION(aPed, FALSE)
		
		SET_PED_DESIRED_HEADING(aPed, GET_ENTITY_HEADING(aPed))
		
		TASK_PERFORM_SEQUENCE(aPed, seq)
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF
ENDPROC


/// PURPOSE: Begins the sync scene for all peds in the corona
/// PARAMS:
///    mainLoopedPed 	- This is the local players ped (Will be looped by the car)
///    vCentrePosition	- The centre position of the corona where sync scene will occur
///    fHeading			- The orientation of the sync scene (based off player)
PROC CORONA_SCENE_START_WALK_IN(CORONA_MENU_DATA &coronaMenuData, PED_INDEX &mainLoopedPed, VECTOR vCentrePosition, FLOAT fHeading)

	PRINTLN("[CORONA] CORONA_SCENE_START_WALK_IN - Beginning sync scene at: ", vCentrePosition, " with Heading: ", fHeading, " TIME(", GET_CLOUD_TIME_AS_INT(), ")")


	#IF IS_DEBUG_BUILD
		CORONA_SCENE_DEBUG_CLONES()
	#ENDIF
	
	PED_INDEX aPed
	
	// Loop through our peds
	INT i
	INT iCount = 0
	
	BOOL bPlayerValid = FALSE

	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1) STEP 1
		IF iCount < 15
			bPlayerValid = FALSE
			
			IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			AND coronaMenuData.lobbyPlayers[i].playerID != PLAYER_ID()
				bPlayerValid = TRUE
				aPed = g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF bDebugClonesForCoronaScene
				bPlayerValid = FALSE
			
				IF CORONA_SCENE_CHECK_VALID_PED(g_CoronaClonePeds.piPlayersPed[i].pedID)
				AND i != NATIVE_TO_INT(PLAYER_ID())
					bPlayerValid = TRUE
					aPed = g_CoronaClonePeds.piPlayersPed[i].pedID
				ENDIF
			ENDIF
			#ENDIF
			
			IF bPlayerValid
			
				CORONA_SCENE_ANIMATE_REMOTE_PLAYER(aPed, vCentrePosition, fHeading, iCount)
				
				PRINTLN("[CORONA] CORONA_SCENE_START_WALK_IN - Beginning sync scene for ped: ", i, " Dict: ", CORONA_SCENE_GET_ANIM_DICT(iCount), " and Anim: ", CORONA_SCENE_GET_ANIM(iCount, FALSE))
				
				iCount++
			ENDIF
		ENDIF
	ENDFOR
	
	// Handle the tutorial ped if we need to
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		IF CORONA_SCENE_CHECK_VALID_PED(coronaMenuData.tutorialPedIndex)
			CORONA_SCENE_ANIMATE_REMOTE_PLAYER(coronaMenuData.tutorialPedIndex, vCentrePosition, fHeading, 999)	//Lamar number is 999 - Makes sure his custom anims are used
		ENDIF
	ENDIF
	
	CORONA_SCENE_ANIMATE_LOCAL_PLAYER(mainLoopedPed, vCentrePosition, fHeading)
	
	//CORONA_SCENE_CLEANUP()
ENDPROC
//give people a score to select them for use in the corona
PROC CORONA_SCENE_GET_PLAYER_SCORE_FOR_CORONA(INT &iPlayerScore[])
	INT iLoop, iMyGBD, iLocalPlayerCrewID
	PLAYER_INDEX playerID
	PARTICIPANT_INDEX partID
	iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	GAMER_HANDLE sHandle// = GET_GAMER_HANDLE_PLAYER(playerID)
	NETWORK_CLAN_DESC 	retDesc
	//get the local players clan ID
	retDesc = GET_LOCAL_PLAYER_CREW_DATA()
	//Store it out
	iLocalPlayerCrewID = retDesc.Id
	//Reset
	retDesc.Id = 0	
	
	//Count the number of people who think they won and give them a score base on friend & crew
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iLoop
		//If they are active
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLoop))
			//Get the part
			partID = INT_TO_PARTICIPANTINDEX(iLoop)
			//Get player ID
			playerID = NETWORK_GET_PLAYER_INDEX(partID)
			//Not SCTV
			IF NOT IS_PLAYER_SCTV(playerID)
			//not late
			AND NOT IS_THIS_PLAYER_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA(playerID)
				//Not me
				IF iLoop != iMyGBD
					sHandle = GET_GAMER_HANDLE_PLAYER(playerID)
					NETWORK_CLAN_PLAYER_GET_DESC(retDesc, 0, sHandle)					
					IF NETWORK_IS_FRIEND(sHandle)
						iPlayerScore[iLoop] += 5
						PRINTLN("[CORONA] CORONA_SCENE_GET_PLAYER_SCORE_FOR_CORONA - Frnd - iPlayerScore[", iLoop, "]  = ", iPlayerScore[iLoop] )
					ENDIF
					IF (retDesc.Id = iLocalPlayerCrewID
					//And my crew ID is not 0
					AND iLocalPlayerCrewID != 0)
						iPlayerScore[iLoop] += 2						
						PRINTLN("[CORONA] CORONA_SCENE_GET_PLAYER_SCORE_FOR_CORONA - Crew - iPlayerScore[", iLoop, "]  = ", iPlayerScore[iLoop] )
					ENDIF
					//Give them a score for being active so I don't need to check again
					iPlayerScore[iLoop] += 1
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE: Triggers animation on all corona peds for the idle looping in corona
PROC CORONA_SCENE_START_LOOPING_ANIM(CORONA_MENU_DATA &coronaMenuData, PED_INDEX &mainLoopedPed, VECTOR vCentrePosition, FLOAT fHeading)

	PRINTLN("[CORONA] CORONA_SCENE_START_LOOPING_ANIM - Beginning sync scene at: ", vCentrePosition, " with Heading: ", fHeading, " TIME(", GET_CLOUD_TIME_AS_INT(), ")")


	#IF IS_DEBUG_BUILD
		CORONA_SCENE_DEBUG_CLONES()
	#ENDIF
	
	FLOAT fGroundZ
	VECTOR vVecAnimOffset
	VECTOR vVecAnimRotation
	PED_INDEX aPed
	
	// Loop through our peds
	INT i
	INT iCount = 0
	INT iLoop
	
	BOOL bPlayerValid = FALSE
	
	INT iSlot
	INT iAddToSceneBitSet
	//Players score
	INT iPlayerScore[NUM_NETWORK_PLAYERS]
	//Highest score
	INT iHighestScore = 0
	//Highest score player
	INT iHighestScorePlayer = -1
	
	//Max number of peds to display in the corona
	CONST_INT ciMAX_CONA_PEDS_TO_DISPLAY 15		
	INT iCountInCorona
	
	//Give peds in the corona a score
	CORONA_SCENE_GET_PLAYER_SCORE_FOR_CORONA(iPlayerScore)
	
	//loop the number of peds we need
	REPEAT ciMAX_CONA_PEDS_TO_DISPLAY iLoop
		//If we've not got enough players already 
		IF iCountInCorona < ciMAX_CONA_PEDS_TO_DISPLAY
			//Loop all peds in the corona
			REPEAT NUM_NETWORK_PLAYERS i
				//If they have a score (1 = (active and not me))
				IF iPlayerScore[i] > 0
				//If it's higher than our currently stored score
				AND iPlayerScore[i] > iHighestScore
				//and we've not already sed yes to them
				AND NOT IS_BIT_SET(iAddToSceneBitSet, i)
					//Store out their score as the max
					iHighestScore = iPlayerScore[i]
					//set them as the highest score player
					iHighestScorePlayer = i
				ENDIF
			ENDREPEAT
			//If we sound a person
			IF iHighestScorePlayer != -1
				//set that the should be in the scene
				PRINTLN("[CORONA] CORONA_SCENE_START_LOOPING_ANIM - SET_BIT(iAddToSceneBitSet, ", iHighestScorePlayer, ")")
				SET_BIT(iAddToSceneBitSet, iHighestScorePlayer)
				//upp count
				iCountInCorona++
			ENDIF
			//Clear vars for next loop
			iHighestScore = 0
			iHighestScorePlayer = -1
		ENDIF
	ENDREPEAT
	
	INT iPart
	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1) STEP 1
		IF iCount < ciMAX_CONA_PEDS_TO_DISPLAY
			bPlayerValid = FALSE
			
			IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
			AND coronaMenuData.lobbyPlayers[i].playerID != PLAYER_ID()
			AND NETWORK_IS_PLAYER_A_PARTICIPANT(coronaMenuData.lobbyPlayers[i].playerID)
				aPed = g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID
				
				IF aPed != mainLoopedPed
					bPlayerValid = TRUE
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF bDebugClonesForCoronaScene
				bPlayerValid = FALSE
			
				IF CORONA_SCENE_CHECK_VALID_PED(g_CoronaClonePeds.piPlayersPed[i].pedID)
				AND i != NATIVE_TO_INT(PLAYER_ID())
					bPlayerValid = TRUE
					aPed = g_CoronaClonePeds.piPlayersPed[i].pedID
				ENDIF
			ENDIF
			#ENDIF
			
			IF bPlayerValid
			
				iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(coronaMenuData.lobbyPlayers[i].playerID))				
			
				PRINTLN("[CORONA] CORONA_SCENE_START_LOOPING_ANIM - Checking iPart: ", iPart)
			
				IF IS_BIT_SET(iAddToSceneBitSet, iPart)
				AND CORONA_SCENE_CHECK_VALID_PED(aPed)
				
					iSlot = LOOKUP_CORONA_CLONE_PLACEMENT(iCount)
				
					vVecAnimOffset = GET_ANIM_INITIAL_OFFSET_POSITION(CORONA_SCENE_GET_ANIM_DICT(iSlot), CORONA_SCENE_GET_ANIM(iSlot, TRUE), vCentrePosition, <<0, 0, fHeading>>)
					vVecAnimRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(CORONA_SCENE_GET_ANIM_DICT(iSlot), CORONA_SCENE_GET_ANIM(iSlot, TRUE), vCentrePosition, <<0, 0, fHeading>>)
					
					IF iSlot = 0
						vVecAnimRotation += 30.0
					ENDIF
					
					IF GET_GROUND_Z_FOR_3D_COORD(vVecAnimOffset, fGroundZ)
						//PRINTLN("[CORONA] CORONA_SCENE_START_LOOPING_ANIM - Found ground z for ped: ", fGroundZ)
						vVecAnimOffset.z = fGroundZ
					ENDIF
											
					SET_ENTITY_COORDS(aPed, vVecAnimOffset, FALSE)
					SET_ENTITY_ROTATION(aPed, vVecAnimRotation)
					SET_ENTITY_COLLISION(aPed, TRUE)
					SET_ENTITY_VISIBLE(aPed, TRUE)
					FREEZE_ENTITY_POSITION(aPed, FALSE)
					
					SET_PED_DESIRED_HEADING(aPed, GET_ENTITY_HEADING(aPed))
					
					SET_PED_ALTERNATE_MOVEMENT_ANIM(aPed, AAT_IDLE, CORONA_SCENE_GET_ANIM_DICT(iSlot),  CORONA_SCENE_GET_ANIM(iSlot, TRUE), INSTANT_BLEND_IN, TRUE)
					
					FORCE_PED_MOTION_STATE(aPed, MS_ON_FOOT_IDLE, TRUE, DEFAULT, TRUE)
					SET_PED_RESET_FLAG(aPed, PRF_SkipOnFootIdleIntro, TRUE)
					
				//	TASK_PLAY_ANIM(aPed, CORONA_SCENE_GET_ANIM_DICT(iSlot),  CORONA_SCENE_GET_ANIM(iSlot, TRUE), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					
					PRINTLN("[CORONA] CORONA_SCENE_START_LOOPING_ANIM - Beginning sync scene for ped: ", i, " Dict: ", CORONA_SCENE_GET_ANIM_DICT(iSlot), " and Anim: ", CORONA_SCENE_GET_ANIM(iSlot, FALSE))
					PRINTLN("[CORONA] CORONA_SCENE_START_LOOPING_ANIM - Coords: ", vVecAnimOffset)
					PRINTLN("[CORONA] CORONA_SCENE_START_LOOPING_ANIM - Rotation: ", vVecAnimRotation)
					
					iCount++
				ELSE
					IF DOES_JOB_USE_RACE_VEHICLE_SELECTION()
					AND CORONA_SCENE_CHECK_VALID_PED(aPed)
						SET_ENTITY_VISIBLE(aPed, FALSE)
						PRINTLN("[CORONA] CORONA_SCENE_START_LOOPING_ANIM - Ped ", i, " is not to be added to scene. Setting them invisible.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	// Do we need to handle tutorial ped (Lamar) here?
	
	CORONA_SCENE_ANIMATE_LOCAL_PLAYER(mainLoopedPed, vCentrePosition, fHeading)
	
	//CORONA_SCENE_CLEANUP()
ENDPROC


FUNC BOOL ARE_ALL_CORONA_PED_PLAYING_ANIMATION(CORONA_MENU_DATA &coronaMenuData)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
	
		IF coronaMenuData.lobbyPlayers[i].playerId != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(coronaMenuData.lobbyPlayers[i].playerId)
			AND CORONA_SCENE_CHECK_VALID_PED(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID)
				
				IF ((GET_SCRIPT_TASK_STATUS(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID, SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK)
				OR (GET_SCRIPT_TASK_STATUS(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK))
					// This is good
				ELSE
					PRINTLN("[CORONA] ARE_ALL_CORONA_PED_PLAYING_ANIMATION - Ped for player: ", GET_PLAYER_NAME(coronaMenuData.lobbyPlayers[i].playerId), " is not playing task")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
			
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC


PROC PROCESS_PED_REPOSITION_IN_CORONA(PED_INDEX &aPed, VECTOR vCoronaPos, INT iCount)

	VECTOR vVecAnimOffset
	VECTOR vVecAnimRotation
	INT iSlot
	VECTOR vPrevPosition, vRepositionPOint
	FLOAT fGroundZ

	iSlot = LOOKUP_CORONA_CLONE_PLACEMENT_BETTING(iCount)
		
	vPrevPosition = GET_ENTITY_COORDS(aPed)
					
	vVecAnimOffset = g_vCoronaPedPositions[iSlot]															
	vRepositionPOint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, 0.0, vVecAnimOffset)									
	
	vRepositionPoint.z += 1.0
	IF GET_GROUND_Z_FOR_3D_COORD(vRepositionPoint, fGroundZ)
		//PRINTLN("[CORONA] REPOSITION_GUYS_FOR_BETTING_SCREEN - Position  ped: ", iCount, " Found Ground Z: ", fGroundZ)
		vRepositionPoint.z = fGroundZ
	ELSE
		vRepositionPOint.z = vPrevPosition.z
	ENDIF
									
	SET_ENTITY_COORDS(aPed, vRepositionPOint , FALSE)
	SET_ENTITY_ROTATION(aPed, vVecAnimRotation)
	
	SET_ENTITY_HEADING(aPed, GET_HEADING_BETWEEN_VECTORS_2D(vRepositionPOint, vCoronaPos + <<-8.0, 0.0, 0.0>>) )
					
	SET_ENTITY_COLLISION(aPed, TRUE)
	
	#IF IS_DEBUG_BUILD
	IF NOT bDebugRefreshCoronaClones
	#ENDIF
		SET_ENTITY_VISIBLE(aPed, TRUE)
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	FREEZE_ENTITY_POSITION(aPed, FALSE)
	
	SET_PED_DESIRED_HEADING(aPed, GET_ENTITY_HEADING(aPed))
ENDPROC

PROC REPOSITION_GUYS_FOR_BETTING_SCREEN(CORONA_MENU_DATA &coronaMenuData, VECTOR vCoronaPos)

	// Loop through our peds
	INT i
	PED_INDEX aPed
	INT iCount = 0
	VECTOR vRepositionPOint
	FLOAT fGroundZ
	VECTOR vPrevPosition

	// Special case for 2 player games
	IF GET_TOTAL_PLAYERS_IN_CORONA(coronaMenuData) = 2
	#IF IS_DEBUG_BUILD
	AND NOT bDebugClonesForCoronaScene
	#ENDIF
		
		PRINTLN("[CORONA] REPOSITION_GUYS_FOR_BETTING_SCREEN - Processing 2 players")
			
		FOR i = 0 TO (NUM_NETWORK_PLAYERS -1) STEP 1
			
			IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
				
				aPed = g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID

				IF CORONA_SCENE_CHECK_VALID_PED(aPed)

					vPrevPosition = GET_ENTITY_COORDS(aPed)
					vRepositionPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, (180.0 * iCount), <<0.0, GET_RADIUS_OF_CORONA(g_FMMC_STRUCT.iMissionType), 2.0>>)

					IF GET_GROUND_Z_FOR_3D_COORD(vRepositionPoint, fGroundZ)
						//PRINTLN("[CORONA] REPOSITION_GUYS_FOR_BETTING_SCREEN - Position  ped: ", i, " Found Ground Z: ", fGroundZ)
						vRepositionPoint.z = fGroundZ
					ELSE
						vRepositionPOint.z = vPrevPosition.z
					ENDIF
					
					SET_ENTITY_COORDS(aPed, vRepositionPOint , FALSE)
					SET_ENTITY_HEADING(aPed, GET_HEADING_BETWEEN_VECTORS_2D(vRepositionPOint, vCoronaPos))
					SET_ENTITY_COLLISION(aPed, TRUE)
					SET_ENTITY_VISIBLE(aPed, TRUE)
					FREEZE_ENTITY_POSITION(aPed, FALSE)
					
					SET_PED_DESIRED_HEADING(aPed, GET_ENTITY_HEADING(aPed))
					
					PRINTLN("[CORONA] REPOSITION_GUYS_FOR_BETTING_SCREEN - Processing player ", iCount, " to ", vRepositionPOint, " and heading: ", GET_HEADING_BETWEEN_VECTORS_2D(vRepositionPOint, vCoronaPos))
					
					iCount++
				ENDIF
			ENDIF
		ENDFOR
		
		IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
			IF CORONA_SCENE_CHECK_VALID_PED(coronaMenuData.tutorialPedIndex)
			
				vPrevPosition = GET_ENTITY_COORDS(coronaMenuData.tutorialPedIndex)
				vRepositionPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, (180.0 * iCount), <<0.0, GET_RADIUS_OF_CORONA(g_FMMC_STRUCT.iMissionType), 2.0>>)

				IF GET_GROUND_Z_FOR_3D_COORD(vRepositionPoint, fGroundZ)
					//PRINTLN("[CORONA] REPOSITION_GUYS_FOR_BETTING_SCREEN - Position tutorial ped: ", i, " Found Ground Z: ", fGroundZ)
					vRepositionPoint.z = fGroundZ
				ELSE
					vRepositionPOint.z = vPrevPosition.z
				ENDIF
				
				SET_ENTITY_COORDS(coronaMenuData.tutorialPedIndex, vRepositionPOint , FALSE)
				SET_ENTITY_HEADING(coronaMenuData.tutorialPedIndex, GET_HEADING_BETWEEN_VECTORS_2D(vRepositionPOint, vCoronaPos))
				SET_ENTITY_COLLISION(coronaMenuData.tutorialPedIndex, TRUE)
				SET_ENTITY_VISIBLE(coronaMenuData.tutorialPedIndex, TRUE)
				FREEZE_ENTITY_POSITION(coronaMenuData.tutorialPedIndex, FALSE)
				
				SET_PED_DESIRED_HEADING(coronaMenuData.tutorialPedIndex, GET_ENTITY_HEADING(aPed))
				
				iCount++
			
			ENDIF
		ENDIF
		
		EXIT
	ENDIF


	BOOL bPlayerValid = FALSE
	
	#IF IS_DEBUG_BUILD
	VECTOR vDebugPedPos
	#ENDIF

	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1) STEP 1
		
		bPlayerValid = FALSE
		
		IF coronaMenuData.lobbyPlayers[i].playerID != INVALID_PLAYER_INDEX()
		AND coronaMenuData.lobbyPlayers[i].playerID != PLAYER_ID()
			bPlayerValid = TRUE
			aPed = g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(coronaMenuData.lobbyPlayers[i].playerID)].pedID
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bDebugClonesForCoronaScene
			bPlayerValid = FALSE
		
			IF CORONA_SCENE_CHECK_VALID_PED(g_CoronaClonePeds.piPlayersPed[i].pedID)
			AND i != NATIVE_TO_INT(PLAYER_ID())
				bPlayerValid = TRUE
				aPed = g_CoronaClonePeds.piPlayersPed[i].pedID
			ENDIF
		ENDIF
		#ENDIF
		
		IF bPlayerValid			
		
			IF CORONA_SCENE_CHECK_VALID_PED(aPed)
		
				PROCESS_PED_REPOSITION_IN_CORONA(aPed, vCoronaPos, iCount)
				
				iCount++
				
				#IF IS_DEBUG_BUILD
					vDebugPedPos = GET_ENTITY_COORDS(aPed)
					PRINTLN("[CORONA] REPOSITION_GUYS_FOR_BETTING_SCREEN - Position  ped: ", i, " Vector: ", vDebugPedPos)
				#ENDIF

			ENDIF
		ELSE
			PRINTLN("[CORONA] REPOSITION_GUYS_FOR_BETTING_SCREEN - NOT VALID ped: ", i)
		ENDIF
		

		
	ENDFOR
	
	// Handle the tutorial ped
	IF IS_PLAYER_IN_TUTORIAL_RACE_CORONA()
		IF CORONA_SCENE_CHECK_VALID_PED(coronaMenuData.tutorialPedIndex)
			
			PROCESS_PED_REPOSITION_IN_CORONA(coronaMenuData.tutorialPedIndex, vCoronaPos, iCount)
			iCount++
			
		ENDIF
	ENDIF

	IF CORONA_SCENE_CHECK_VALID_PED(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID)
	
		VECTOR vPrevPlayerPosition = GET_ENTITY_COORDS(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID)
		VECTOR vRepositionPOintPlayer = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoronaPos, 0.0, g_vPlayerCoronaOffset) //<< -1.9252, -0.826065, 0.309731 >>)
				
		vRepositionPOintPlayer.z += 1.0
		IF GET_GROUND_Z_FOR_3D_COORD(vRepositionPOintPlayer, fGroundZ)
			//PRINTLN("[CORONA] REPOSITION_GUYS_FOR_BETTING_SCREEN - Position PLAYER: ", i, " Found Ground Z: ", fGroundZ)
			vRepositionPOintPlayer.z = fGroundZ
		ELSE
			vRepositionPOintPlayer.z = vPrevPlayerPosition.z
		ENDIF
		
//		GET_GROUND_Z_FOR_3D_COORD(vRepositionPOintPlayer, vRepositionPOintPlayer.z)
		
		//vRepositionPOintPlayer.z = vPrevPlayerPosition.z
		
		SET_ENTITY_COORDS(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID, vRepositionPOintPlayer , FALSE)
	
		SET_ENTITY_COLLISION(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID, TRUE)
		SET_ENTITY_VISIBLE(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID, TRUE)
		FREEZE_ENTITY_POSITION(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID, FALSE)	

		SET_ENTITY_HEADING(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID, g_fRotationOfCoronaPlayer ) //123.145477)
		SET_PED_DESIRED_HEADING(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID, GET_ENTITY_HEADING(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID))
	ENDIF
	
ENDPROC


// Heist planning uses animation for peds in the bespoke corona layout.

/// PURPOSE: Applys the animation to the ped in the corona based on their location within the team
/// PARAMS:
///    aPed 			- The ped to play the animation
///    iTeam 			- The team the ped is in
///    iTeamPosIndex 	- The position within the team the ped is in 
PROC SET_CORONA_PED_ANIMATION(CORONA_MENU_DATA &coronaMenuData, PED_INDEX &aPed, INT iTeam, INT iTeamPosIndex, BOOL bUseGenericAnim = FALSE)
	
	IF NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
	
		// No need to apply or process any animation logic here for non heists
		IF NOT IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
			PRINTLN("[CORONA] SET_CORONA_PED_ANIMATION - Not a Heist. Exit")
			EXIT
		ENDIF	
	
	ENDIF
	
	STRING strAnimDict = GET_CORONA_PED_ANIMATION_DICT(iTeam, iTeamPosIndex)
	TEXT_LABEL_31 strAnimIntro, strAnimLoop
	FLOAT fStartPhase
	
	// Grab our intro and loop animations
	GET_CORONA_PED_ANIMATION_INTRO_AND_LOOP(iTeam, iTeamPosIndex, strAnimIntro, strAnimLoop, fStartPhase)
	
	IF bUseGenericAnim
		strAnimDict = GET_CORONA_PED_ANIMATION_DICT(0, 1)
		GET_CORONA_PED_ANIMATION_INTRO_AND_LOOP(0, 1, strAnimIntro, strAnimLoop, fStartPhase)
		PRINTLN("[CORONA] SET_CORONA_PED_ANIMATION - Player is in an outfit requiring a generic anim. Grab default 0th position")
	ENDIF
	
	// Overwrite slot 0 anims if male to cancel playing female anims
	IF iTeam != -1
		IF iTeamPosIndex = 0 
			IF IS_PED_MALE(aPed)
				strAnimDict = "ANIM@HEISTS@HEIST_CORONA@TEAM_IDLES@MALE_B"
				fStartPhase = GET_RANDOM_FLOAT_IN_RANGE(0.3, 0.9)
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[CORONA] SET_CORONA_PED_ANIMATION - Applying animation to ped in team: ", iTeam, ", pos: ", iTeamPosIndex)
	PRINTLN("[CORONA] SET_CORONA_PED_ANIMATION -	strAnimDict: ", strAnimDict)
	PRINTLN("[CORONA] SET_CORONA_PED_ANIMATION -	strAnimIntro: ", strAnimIntro, ", strAnimLoop: ", strAnimLoop)
	PRINTLN("[CORONA] SET_CORONA_PED_ANIMATION -	fStartPhase: ", fStartPhase)
	
	// If any of these strings are null or empty then get out
	IF IS_STRING_NULL_OR_EMPTY(strAnimDict)
	OR IS_STRING_NULL_OR_EMPTY(strAnimLoop)
		EXIT
	ENDIF
	
	// Pistol specific anim
	IF iTeam = -1
		IF iTeamPosIndex = 2
			IF CAN_REGISTER_MISSION_OBJECTS(2)
				RESERVE_NETWORK_MISSION_OBJECTS(2)
				
				IF NOT DOES_ENTITY_EXIST(coronaMenuData.oPistol)
					coronaMenuData.oPistol = CREATE_WEAPON_OBJECT(WEAPONTYPE_PISTOL, INFINITE_AMMO, GET_ENTITY_COORDS(aPed), TRUE)
					REMOVE_WEAPON_COMPONENT_FROM_WEAPON_OBJECT(coronaMenuData.oPistol, WEAPONCOMPONENT_PISTOL_CLIP_01)
					PRINTLN("[CORONA] SET_CORONA_PED_ANIMATION -	Corona pistol has been created")
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(coronaMenuData.oAmmoClip)
				AND REQUEST_LOAD_MODEL(GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_PISTOL_CLIP_01))
					coronaMenuData.oAmmoClip = CREATE_OBJECT_NO_OFFSET(GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_PISTOL_CLIP_01), GET_ENTITY_COORDS(aPed), FALSE, FALSE, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_PISTOL_CLIP_01))
					FREEZE_ENTITY_POSITION(coronaMenuData.oAmmoClip, FALSE)
					PRINTLN("[CORONA] SET_CORONA_PED_ANIMATION -	Corona pistol magazine clip has been created")
				ENDIF
			
				coronaMenuData.pTempPed = aPed
				SET_CORONA_BIT(CORONA_HEIST_PLAY_PISTOL_ANIM)
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_ANIM_DICT_LOADED(strAnimDict)
		IF NOT IS_STRING_NULL_OR_EMPTY(strAnimIntro)
			
			// Single team pistol specific anim
			IF IS_CORONA_BIT_SET(CORONA_HEIST_PLAY_PISTOL_ANIM)
				coronaMenuData.iSyncedSceneID = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(aPed), GET_ENTITY_ROTATION(aPed))
				
				TASK_SYNCHRONIZED_SCENE(aPed, coronaMenuData.iSyncedSceneID, strAnimDict, strAnimIntro, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(coronaMenuData.oPistol, coronaMenuData.iSyncedSceneID, "single_team_intro_gun", strAnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET))
				PLAY_SYNCHRONIZED_ENTITY_ANIM(coronaMenuData.oAmmoClip, coronaMenuData.iSyncedSceneID, "single_team_intro_clip", strAnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET))
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(aPed)	// Remove additional call to force update as this is covered in PLACE_PED_IN_CORONA_POSITION
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(coronaMenuData.iSyncedSceneID)
					PRINTLN("[CORONA] SET_CORONA_PED_ANIMATION -	Playing pistol specific anim on ped")
				ENDIF
				
				CLEAR_CORONA_BIT(CORONA_HEIST_PLAY_PISTOL_ANIM)
				SET_CORONA_BIT(CORONA_HEIST_PLAY_LOOPING_ANIM)
			ELSE
				// If we have an intro then set up a sequence
				SEQUENCE_INDEX seq
				OPEN_SEQUENCE_TASK(seq)
				TASK_PLAY_ANIM(NULL, strAnimDict, strAnimIntro, INSTANT_BLEND_IN)	
				TASK_PLAY_ANIM(NULL, strAnimDict, strAnimLoop, DEFAULT, DEFAULT, DEFAULT, AF_LOOPING, fStartPhase)
				CLOSE_SEQUENCE_TASK(seq)
				
				TASK_PERFORM_SEQUENCE(aPed, seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				PRINTLN("[CORONA] SET_CORONA_PED_ANIMATION -	Playing task sequence on ped")
			ENDIF	
				
//			ELIF ARE_STRINGS_EQUAL(strAnimLoop, "BASE")
//			SET_PED_ALTERNATE_MOVEMENT_ANIM(aPed, AAT_IDLE, strAnimDict, strAnimLoop, DEFAULT, TRUE)
//			PRINTLN("[CORONA_ANIM] SET_CORONA_PED_ANIMATION - SET_PED_ALTERNATE_MOVEMENT_ANIM BASE")
		ELSE
			// Play our looped animation
			TASK_PLAY_ANIM(aPed, strAnimDict, strAnimLoop, INSTANT_BLEND_IN, DEFAULT, DEFAULT, AF_LOOPING, fStartPhase)
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[CORONA] SET_CORONA_PED_ANIMATION -	Anim dict not loaded")
	#ENDIF
	ENDIF
ENDPROC


