
USING "globals.sch"
USING "rage_builtins.sch"
USING "net_mission.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_ambience.sch"
USING "fm_content_launching.sch"
USING "net_collectables_pickups.sch"
USING "net_payphone_flow.sch"
USING "net_random_event_launcher_events.sch"

USING "net_random_events_public.sch"
USING "net_random_events_structs.sch"
USING "net_random_events.sch"

//----------------------
//	CORE
//----------------------

FUNC BOOL FMRE_SHOULD_TRIGGER_EVENT(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT iEvent)

	INT iTrigger
	REPEAT sFMREdata.Events[iEvent].iNumTriggers iTrigger
		IF sFMREdata.Events[iEvent].Trigger[iTrigger] != NULL
			IF CALL sFMREdata.Events[iEvent].Trigger[iTrigger](iEvent)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC

PROC RESET_RANDOM_EVENTS_DATA()

	RANDOM_EVENTS_SERVER_DATA server
	RANDOM_EVENTS_PLAYER_DATA player
	
	GlobalServerBD_RandomEvents = server
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		GlobalplayerBD_FM_2[i].RandomEvents = player
	ENDREPEAT
	
	PRINTLN("[FMRE] - RESET_RANDOM_EVENTS_DATA")

ENDPROC


PROC FMRE_PLAYER_CLEANUP(INT iEvent)
	
	IF FMRE_GET_PLAYER_STATE(iEvent, PLAYER_ID()) != eFMREPLAYERSTATE_INACTIVE
		FMRE_SET_PLAYER_STATE(iEVent, eFMREPLAYERSTATE_INACTIVE)
		PRINTLN("[FMRE] - [FMRE_PLAYER_CLEANUP] - State")
	ENDIF
	
	INT iBs
	REPEAT FMRE_CLIENT_BITSET_ARRAY_SIZE iBs
		IF GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].RandomEvents.Events[iEvent].iBitset[iBs] != 0
			GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].RandomEvents.Events[iEvent].iBitset[iBs] = 0
			PRINTLN("[FMRE] - [FMRE_PLAYER_CLEANUP] - iBitset ",iBs)
		ENDIF
	ENDREPEAT

ENDPROC

FUNC BOOL FMRE_LAUNCH_EVENT(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT iEvent)

	MP_MISSION_DATA data
	data.mdID.idMission = Convert_FM_Mission_Type_To_MissionID(sFMREdata.sFMREScriptEventdata.Events[iEvent].iType)
	data.iInstanceId = iEvent 
				
	IF NOT NETWORK_IS_SCRIPT_ACTIVE(GET_MP_MISSION_NAME(data.mdID.idMission), data.iInstanceId, TRUE)
		NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(data)
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL FMRE_HAS_EVENT_ENDED(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT iEvent)

	MP_MISSION_DATA data
	data.mdID.idMission = Convert_FM_Mission_Type_To_MissionID(sFMREdata.sFMREScriptEventdata.Events[iEvent].iType)
	data.iInstanceId = iEvent 
				
	IF NOT NETWORK_IS_SCRIPT_ACTIVE(GET_MP_MISSION_NAME(data.mdID.idMission), data.iInstanceId, TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE: Checks if the the FMRE launcher should run
///    
/// RETURNS: TRUE if safe to run, FALSE otherwise
///    
FUNC BOOL FMRE_IS_LAUNCHER_SAFE_TO_RUN(RANDOM_EVENTS_LOCAL_DATA &sFMREdata)

	UNUSED_PARAMETER(sFMREdata)
	
	#IF IS_DEBUG_BUILD
	IF sFMREdata.bBlockedByCommandLine
		RETURN FALSE
	ENDIF
	#ENDIF
		
	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC INT FMRE_GET_MAX_NUM_RESERVED_PEDS()
	RETURN g_sMPTunables.iFMREMaxReservedPeds
ENDFUNC

FUNC INT FMRE_GET_MAX_NUM_RESERVED_VEHICLES()
	RETURN g_sMPTunables.iFMREMaxReservedVehicles
ENDFUNC

FUNC INT FMRE_GET_MAX_NUM_RESERVED_OBJECTS()
	RETURN g_sMPTunables.iFMREMaxReservedObjects
ENDFUNC

#IF IS_DEBUG_BUILD
PROC FMRE_PRINT_EVENT_RESERVATIONS(INT iEvent)
	PRINTLN("[FMRE] *RESERVATIONS* - Event ", iEvent, " *************")
	PRINTLN("[FMRE] *RESERVATIONS* - Peds: ", GlobalServerBD_RandomEvents.Events[iEvent].sReservations.iNumReservedPeds)
	PRINTLN("[FMRE] *RESERVATIONS* - Vehicles: ", GlobalServerBD_RandomEvents.Events[iEvent].sReservations.iNumReservedVehicles)
	PRINTLN("[FMRE] *RESERVATIONS* - Objects: ", GlobalServerBD_RandomEvents.Events[iEvent].sReservations.iNumReservedObjects)
ENDPROC
#ENDIF

PROC FMRE_GET_CURRENT_RESERVATION_FOR_EVENT(INT iEvent, INT &iNumPeds, INT &iNumVehicles, INT &iNumObjects)

	iNumPeds += GlobalServerBD_RandomEvents.Events[iEvent].sReservations.iNumReservedPeds
	iNumVehicles += GlobalServerBD_RandomEvents.Events[iEvent].sReservations.iNumReservedVehicles
	iNumObjects += GlobalServerBD_RandomEvents.Events[iEvent].sReservations.iNumReservedObjects
	
	#IF IS_DEBUG_BUILD
	FMRE_PRINT_EVENT_RESERVATIONS(iEvent)
	#ENDIF

ENDPROC

PROC FMRE_GET_CURRENT_RESERVATION_TOTALS(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT &iNumPeds, INT &iNumVehicles, INT &iNumObjects)

	iNumPeds = 0
	iNumVehicles = 0
	iNumObjects = 0

	INT iEvent
	REPEAT sFMREdata.sFMREScriptEventdata.iNumEvents iEvent
		FMRE_GET_CURRENT_RESERVATION_FOR_EVENT(iEvent, iNumPeds, iNumVehicles, iNumObjects)
	ENDREPEAT
	
	PRINTLN("[FMRE] *RESERVATIONS* - Totals  *************")
	PRINTLN("[FMRE] *RESERVATIONS* - Peds: ", iNumPeds, "/", FMRE_GET_MAX_NUM_RESERVED_PEDS())
	PRINTLN("[FMRE] *RESERVATIONS* - Vehicles: ", iNumVehicles, "/", FMRE_GET_MAX_NUM_RESERVED_VEHICLES())
	PRINTLN("[FMRE] *RESERVATIONS* - Objects: ", iNumObjects, "/", FMRE_GET_MAX_NUM_RESERVED_OBJECTS())

ENDPROC

PROC FMRE_SET_EVENT_RESERVATIONS(INT iEvent, INT iNumPeds, INT iNumVehicles, INT iNumObjects)

	PRINTLN("[FMRE] FMRE_SET_EVENT_RESERVATIONS - Event ", iEvent)

	GlobalServerBD_RandomEvents.Events[iEvent].sReservations.iNumReservedPeds = iNumPeds
	GlobalServerBD_RandomEvents.Events[iEvent].sReservations.iNumReservedVehicles = iNumVehicles
	GlobalServerBD_RandomEvents.Events[iEvent].sReservations.iNumReservedObjects = iNumObjects
	
	#IF IS_DEBUG_BUILD
	FMRE_PRINT_EVENT_RESERVATIONS(iEvent)
	#ENDIF

ENDPROC

FUNC BOOL FMRE_IS_EVENT_WITHIN_RESERVATIONS_LIMIT(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, MISSION_PLACEMENT_DATA &data, INT iEvent)
			
	INT iNumPeds, iNumVehicles, iNumObjects
	
	FMRE_GET_CURRENT_RESERVATION_TOTALS(sFMREdata, iNumPeds, iNumVehicles, iNumObjects)
	
	IF iNumPeds + data.Ped.iCount > FMRE_GET_MAX_NUM_RESERVED_PEDS()
	OR iNumVehicles + data.Vehicle.iCount > FMRE_GET_MAX_NUM_RESERVED_VEHICLES()
	OR iNumObjects + data.Prop.iCount > FMRE_GET_MAX_NUM_RESERVED_OBJECTS()
				
		RETURN FALSE
		
	ENDIF
	
	

	FMRE_SET_EVENT_RESERVATIONS(iEvent, data.Ped.iCount, data.Vehicle.iCount, data.Prop.iCount)
	
	RETURN TRUE

ENDFUNC

FUNC eFM_CONTENT_LOAD_STATE FMRE_CAN_EVENT_BECOME_AVAILABLE(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT iEvent, INT iEventType, INT iVariation, INT iSubvariation)
		
	MISSION_PLACEMENT_DATA data
	IF NOT FREEMODE_CONTENT_POPULATE_PLACEMENT_DATA(data, iEventType, iVariation, iSubvariation)
		PRINTLN("[FMRE] FMRE_CAN_EVENT_BECOME_AVAILABLE - ",GET_FREEMODE_CONTENT_SUBVARIATION_NAME(iEventType, iSubvariation) ," loading placement data - FALSE")
		RETURN eFMCLOADSTATE_LOADING
	ENDIF
	
	IF GlobalServerBD_RandomEvents.Events[iEvent].iRetryAttempts <= 0
		IF NOT FMRE_IS_EVENT_WITHIN_RESERVATIONS_LIMIT(sFMREdata, data, iEvent)
			PRINTLN("[FMRE] FMRE_CAN_EVENT_BECOME_AVAILABLE - ",GET_FREEMODE_CONTENT_SUBVARIATION_NAME(iEventType, iSubvariation) ," event surpassed reservations limit - FALSE")
			RETURN eFMCLOADSTATE_FAIL
		ENDIF
	ENDIF
	
	IF sFMREdata.Events[iEvent].AvailableCondition != NULL
		IF NOT CALL sFMREdata.Events[iEvent].AvailableCondition(iEvent)
			PRINTLN("[FMRE] FMRE_CAN_EVENT_BECOME_AVAILABLE - ",GET_FREEMODE_CONTENT_SUBVARIATION_NAME(iEventType, iSubvariation) ," Failed server condition for becoming available")
			RETURN eFMCLOADSTATE_FAIL
		ENDIF
	ENDIF
	
	IF sFMREdata.Events[iEvent].InitialTriggerPoint != NULL
		GlobalServerBD_RandomEvents.Events[iEvent].vTriggerPoint = CALL sFMREdata.Events[iEvent].InitialTriggerPoint(iEvent, data)
		PRINTLN("[FMRE] FMRE_CAN_EVENT_BECOME_AVAILABLE - ",GET_FREEMODE_CONTENT_SUBVARIATION_NAME(iEventType, iSubvariation) ," Initial trigger point = ",GlobalServerBD_RandomEvents.Events[iEvent].vTriggerPoint)
	ENDIF
	
	IF sFMREdata.Events[iEvent].InitialTriggerRange != NULL
		GlobalServerBD_RandomEvents.Events[iEvent].fTriggerRange = CALL sFMREdata.Events[iEvent].InitialTriggerRange(iEvent, data)
		PRINTLN("[FMRE] FMRE_CAN_EVENT_BECOME_AVAILABLE - ",GET_FREEMODE_CONTENT_SUBVARIATION_NAME(iEventType, iSubvariation) ," Initial trigger range = ",GlobalServerBD_RandomEvents.Events[iEvent].fTriggerRange)
	ENDIF
	
	IF GlobalServerBD_RandomEvents.Events[iEvent].iRetryAttempts < g_sMPTunables.iFMRE_NUM_RETRY_ATTEMPTS 
	
		IF NOT IS_FMRE_SERVER_BIT_SET(iEvent, eFMRESERVERBITSET_REQUESTED)
		AND NOT IS_FMRE_FLAG_SET(sFMREdata, iEvent, eFMREFLAGS_IGNORE_NEAR_PLAYER_CHECK)
			IF g_sMPTunables.fFMRE_ANY_PLAYER_NEAR_RANGE > 0
				IF NOT IS_ANY_PLAYER_NEAR_POINT(GlobalServerBD_RandomEvents.Events[iEvent].vTriggerPoint, GlobalServerBD_RandomEvents.Events[iEvent].fTriggerRange + g_sMPTunables.fFMRE_ANY_PLAYER_NEAR_RANGE, FALSE, TRUE, FALSE, FALSE, FALSE, DEFAULT, TRUE)
					PRINTLN("[FMRE] FMRE_CAN_EVENT_BECOME_AVAILABLE - ",GET_FREEMODE_CONTENT_SUBVARIATION_NAME(iEventType, iSubvariation) ," No players within range to reach trigger range")
					GlobalServerBD_RandomEvents.Events[iEvent].iRetryAttempts++
					RETURN eFMCLOADSTATE_RETRY
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
	
	RETURN eFMCLOADSTATE_SUCCESS

ENDFUNC

/// PURPOSE: Checks if the local player is safe to trigger an event
///    
/// RETURNS: TRUE if safe to launch, FALSE otherwise
///    
FUNC BOOL FMRE_IS_PLAYER_SAFE_TO_TRIGGER_EVENT()

	IF NOT IS_NET_PLAYER_OK(PLAYER_ID(),FALSE)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		RETURN FALSE
	ENDIF
				
	IF IS_PLAYER_ON_IMPROMPTU_DM()
		RETURN FALSE
	ENDIF
				
	IF IS_TRANSITION_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_PLAYER_ACTIVE_IN_CORONA_PAST_TRANSITION_SESSION(PLAYER_ID())
		RETURN FALSE
	ENDIF

	IF NOT IS_SKYSWOOP_AT_GROUND()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE, SIMPLE_INTERIOR_TYPE_WAREHOUSE)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
	OR HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
		RETURN FALSE
	ENDIF
		
	IF IS_PLAYER_PERMANENT_PARTICIPANT_TO_ANY_EVENT(PLAYER_ID())
		RETURN FALSE
	ENDIF
		
	RETURN TRUE

ENDFUNC

FUNC INT FMRE_GET_PLAYER_LAUNCH_ATTEMPT_COOLDOWN()
	RETURN 5000
ENDFUNC

FUNC INT FMRE_GET_PLAYER_NUM_ACTIVATED_EVENTS()

	INT iEvent, iCount
	REPEAT MAX_NUM_FM_RANDOM_EVENTS iEvent
		IF FMRE_GET_PLAYER_STATE(iEvent, PLAYER_ID()) != eFMREPLAYERSTATE_INACTIVE
			iCount++
		ENDIF
	ENDREPEAT
	
	RETURN iCount

ENDFUNC

FUNC INT FMRE_GET_PLAYER_MAX_ACTIVATED_EVENTS()
	RETURN g_sMPTunables.iFMREMaxActivatedEvents
ENDFUNC

FUNC BOOL FMRE_CAN_PLAYER_LAUNCH_EVENT(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT iEvent)

	SWITCH FMRE_GET_STATE(iEvent)

		CASE eFMRESTATE_AVAILABLE
			IF HAS_NET_TIMER_STARTED(sFMREdata.Events[iEvent].stLaunchAttempt)
				IF NOT HAS_NET_TIMER_EXPIRED(sFMREdata.Events[iEvent].stLaunchAttempt, FMRE_GET_PLAYER_LAUNCH_ATTEMPT_COOLDOWN())
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF FMRE_GET_PLAYER_NUM_ACTIVATED_EVENTS() >= FMRE_GET_PLAYER_MAX_ACTIVATED_EVENTS()
				PRINTLN("[FMRE] FMRE_CAN_PLAYER_LAUNCH_EVENT - ", iEvent, " - FMRE_GET_PLAYER_NUM_ACTIVATED_EVENTS (", FMRE_GET_PLAYER_NUM_ACTIVATED_EVENTS(),") >= FMRE_GET_PLAYER_MAX_ACTIVATED_EVENTS (", FMRE_GET_PLAYER_MAX_ACTIVATED_EVENTS(),") - FALSE")
				REINIT_NET_TIMER(sFMREdata.Events[iEvent].stLaunchAttempt)
				RETURN FALSE
			ENDIF
			
			IF CAN_ANY_PLAYER_SEE_POINT(FMRE_LAUNCHER_GET_TRIGGER_POINT(iEvent))
			AND NOT IS_FMRE_CLIENT_BIT_SET(PLAYER_ID(), iEvent, eFMRECLIENTBITSET_REQUESTED)
				PRINTLN("[FMRE] FMRE_CAN_PLAYER_LAUNCH_EVENT - ", iEvent, " - CAN_ANY_PLAYER_SEE_POINT - FALSE")
				REINIT_NET_TIMER(sFMREdata.Events[iEvent].stLaunchAttempt)
				RETURN FALSE
			ENDIF
			
			IF NOT IS_FREEMODE_CONTENT_SUBVARIATION_ABLE_TO_CREATE_ENTITIES(sFMREdata.sFMREScriptEventdata.Events[iEvent].iType, GlobalServerBD_RandomEvents.Events[iEvent].iVariation, GlobalServerBD_RandomEvents.Events[iEvent].iSubvariation, PLAYER_ID())
				PRINTLN("[FMRE] FMRE_CAN_PLAYER_LAUNCH_EVENT - ", iEvent, " - NOT IS_FREEMODE_CONTENT_SUBVARIATION_ABLE_TO_CREATE_ENTITIES - FALSE")
				REINIT_NET_TIMER(sFMREdata.Events[iEvent].stLaunchAttempt)
				RETURN FALSE
			ENDIF
		BREAK

	ENDSWITCH
	
	RETURN TRUE

ENDFUNC

PROC FMRE_MAINTAIN_PLAYER_STATE(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT iEvent, BOOL bSafe)

	SWITCH FMRE_GET_PLAYER_STATE(iEvent, PLAYER_ID())
		
		CASE eFMREPLAYERSTATE_INACTIVE
		
			IF bSafe
				IF FMRE_SHOULD_TRIGGER_EVENT(sFMREdata, iEvent) 
					IF FMRE_CAN_PLAYER_LAUNCH_EVENT(sFMREdata, iEvent)
						FMRE_SET_PLAYER_STATE(iEvent, eFMREPLAYERSTATE_LAUNCHING)
					ENDIF
				ENDIF
			ELSE
				FMRE_PLAYER_CLEANUP(iEvent)
			ENDIF
		
		BREAK
		
		CASE eFMREPLAYERSTATE_LAUNCHING
		
			IF FMRE_LAUNCH_EVENT(sFMREdata, iEvent)
				FMRE_SET_PLAYER_STATE(iEvent, eFMREPLAYERSTATE_ACTIVE)
			ENDIF
		
		BREAK
		
		CASE eFMREPLAYERSTATE_ACTIVE
		
			IF FMRE_HAS_EVENT_ENDED(sFMREdata, iEvent)
				FMRE_SET_PLAYER_STATE(iEvent, eFMREPLAYERSTATE_CLEANUP)
			ENDIF
		
		BREAK
		
	ENDSWITCH

ENDPROC

PROC FMRE_LAUNCHER_CLIENT_RUNNING(RANDOM_EVENTS_LOCAL_DATA &sFMREdata)

	BOOL bSafe = FMRE_IS_PLAYER_SAFE_TO_TRIGGER_EVENT()

	INT iEvent
	REPEAT sFMREdata.sFMREScriptEventdata.iNumEvents iEvent
	
		SWITCH FMRE_GET_STATE(iEvent)
								
			CASE eFMRESTATE_AVAILABLE
			CASE eFMRESTATE_ACTIVE
			CASE eFMRESTATE_CLEANUP
			
				FMRE_MAINTAIN_PLAYER_STATE(sFMREdata, iEvent, bSafe)
			BREAK
			
			DEFAULT
				IF FMRE_GET_PLAYER_STATE(iEvent, PLAYER_ID()) != eFMREPLAYERSTATE_INACTIVE
					FMRE_PLAYER_CLEANUP(iEvent)
				ENDIF
			BREAK
			
		ENDSWITCH
			
	ENDREPEAT
	
ENDPROC

PROC FMRE_SERVER_ON_CLEANUP(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT iEvent)
	IF sFMREdata.sFMREScriptEventdata.Events[iEvent].iType = FMMC_TYPE_VEHICLE_LIST_EVENT
		GlobalServerBD_RandomEvents.sVehicleList.iSpawnLocation = -1
		GlobalServerBD_RandomEvents.sVehicleList.iNumLaunches++
		IF GlobalServerBD_RandomEvents.sVehicleList.iNumLaunches >= DAILY_CHALKBOARD_VEHICLE_COUNT
			GlobalServerBD_RandomEvents.sVehicleList.iNumLaunches = 0
			PRINTLN("[FMRE] - [FMRE_LAUNCHER_SERVER_ON_CLEANUP] - FMMC_TYPE_VEHICLE_LIST_EVENT launches has reached the end, looping back to 0.")
		ELSE
			PRINTLN("[FMRE] - [FMRE_LAUNCHER_SERVER_ON_CLEANUP] - FMMC_TYPE_VEHICLE_LIST_EVENT has launched ", GlobalServerBD_RandomEvents.sVehicleList.iNumLaunches, " times this session.")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL FMRE_RUN_EVENT_TRIGGER_CHECKS(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT iEvent)
	
	IF IS_FMRE_SERVER_BIT_SET(iEvent, eFMRESERVERBITSET_REQUESTED)
		RETURN TRUE
	ENDIF
				
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_BlockRandomEventAmbientLaunch")
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF sFMREdata.Events[iEvent].iInactiveTime = -1
		RETURN FALSE
	ENDIF
	
	RETURN HAS_NET_TIMER_EXPIRED(GlobalServerBD_RandomEvents.Events[iEvent].stState, sFMREdata.Events[iEvent].iInactiveTime)
ENDFUNC

PROC FMRE_LAUNCHER_SERVER_RUNNING(RANDOM_EVENTS_LOCAL_DATA &sFMREdata)

	INT iEvent
	REPEAT sFMREdata.sFMREScriptEventdata.iNumEvents iEvent
	
		SWITCH FMRE_GET_STATE(iEvent)
		
			CASE eFMRESTATE_INACTIVE
				IF FMRE_RUN_EVENT_TRIGGER_CHECKS(sFMREdata, iEvent)
					IF GET_FREEMODE_CONTENT_VARIATION_DATA(sFMREdata.sFMREScriptEventdata.Events[iEvent].iType, GlobalServerBD_RandomEvents.Events[iEvent].iVariation, GlobalServerBD_RandomEvents.Events[iEvent].iSubvariation, INVALID_PLAYER_INDEX())
						SWITCH FMRE_CAN_EVENT_BECOME_AVAILABLE(sFMREdata, iEvent, sFMREdata.sFMREScriptEventdata.Events[iEvent].iType, GlobalServerBD_RandomEvents.Events[iEvent].iVariation, GlobalServerBD_RandomEvents.Events[iEvent].iSubvariation)
							CASE eFMCLOADSTATE_SUCCESS
								RESET_NET_TIMER(GlobalServerBD_RandomEvents.Events[iEvent].stState)
								FMRE_SET_STATE(iEvent, eFMRESTATE_AVAILABLE)
							BREAK
							CASE eFMCLOADSTATE_FAIL
								FMRE_CLEANUP(iEvent, sFMREdata.sFMREScriptEventdata)
							BREAK
							CASE eFMCLOADSTATE_RETRY
								GlobalServerBD_RandomEvents.Events[iEvent].iVariation = -1
								GlobalServerBD_RandomEvents.Events[iEvent].iSubvariation = -1
							BREAK
						ENDSWITCH
					ELSE
						FMRE_CLEANUP(iEvent, sFMREdata.sFMREScriptEventdata)
					ENDIF
				ENDIF
			BREAK
		
			CASE eFMRESTATE_AVAILABLE
				IF HAS_NET_TIMER_EXPIRED(GlobalServerBD_RandomEvents.Events[iEvent].stState, sFMREdata.Events[iEvent].iAvailableTime)
				OR IS_FMRE_SERVER_BIT_SET(iEvent, eFMRESERVERBITSET_SHOULD_CLEANUP)
					FMRE_SET_STATE(iEvent, eFMRESTATE_CLEANUP)
				ELIF IS_FMRE_SERVER_BIT_SET(iEvent, eFMRESERVERBITSET_ACTIVATED_BY_PLAYER)
					FMRE_SET_STATE(iEvent, eFMRESTATE_ACTIVE)
				ENDIF
			BREAK
			
			CASE eFMRESTATE_ACTIVE
			
				IF NOT IS_FMRE_SERVER_BIT_SET(iEvent, eFMRESERVERBITSET_ACTIVATED_BY_PLAYER)
					
					IF IS_FMRE_FLAG_SET(sFMREdata, iEvent, eFMREFLAGS_CAN_RESTART)
					AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(GlobalServerBD_RandomEvents.Events[iEvent].stState, sFMREdata.Events[iEvent].iAvailableTime)
						FMRE_SET_STATE(iEvent, eFMRESTATE_AVAILABLE)
					ELSE
						FMRE_SERVER_ON_CLEANUP(sFMREdata, iEvent)
						FMRE_SET_STATE(iEvent, eFMRESTATE_CLEANUP)
					ENDIF
				
				ENDIF
			
			BREAK
			
			CASE eFMRESTATE_CLEANUP
				FMRE_CLEANUP(iEvent, sFMREdata.sFMREScriptEventdata)
			BREAK
			
		ENDSWITCH
			
	ENDREPEAT
	
ENDPROC

FUNC BOOL FMRE_LAUNCHER_CLIENT_INIT(RANDOM_EVENTS_LOCAL_DATA &sFMREdata)

	FMRE_ADD_EVENTS(sFMREdata)
	
	RETURN TRUE

ENDFUNC

FUNC BOOL FMRE_LAUNCHER_SERVER_INIT()
	#IF FEATURE_HALLOWEEN_2021
	CLEAR_PHANTOM_CAR_TARGET()
	#ENDIF
	
	RETURN TRUE
ENDFUNC

PROC FMRE_LAUNCHER_MAINTAIN_CLIENT(RANDOM_EVENTS_LOCAL_DATA &sFMREdata)

	SWITCH FMRE_GET_CLIENT_LAUNCHER_STATE()
	
		CASE eFMRELAUNCHERSTATE_INIT
			
			IF FMRE_LAUNCHER_CLIENT_INIT(sFMREdata)
				FMRE_SET_CLIENT_LAUNCHER_STATE(eFMRELAUNCHERSTATE_RUNNING)
			ENDIF
			
		BREAK
		
		CASE eFMRELAUNCHERSTATE_RUNNING
			FMRE_LAUNCHER_CLIENT_RUNNING(sFMREdata)
		BREAK
			
	ENDSWITCH

ENDPROC

PROC FMRE_LAUNCHER_MAINTAIN_SERVER(RANDOM_EVENTS_LOCAL_DATA &sFMREdata)

	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF

	SWITCH FMRE_GET_LAUNCHER_STATE()
	
		CASE eFMRELAUNCHERSTATE_INIT
			
			IF FMRE_LAUNCHER_SERVER_INIT()
				FMRE_SET_LAUNCHER_STATE(eFMRELAUNCHERSTATE_RUNNING)
			ENDIF
			
		BREAK
		
		CASE eFMRELAUNCHERSTATE_RUNNING
			FMRE_LAUNCHER_SERVER_RUNNING(sFMREdata)
		BREAK
			
	ENDSWITCH

ENDPROC
PROC FMRE_LAUNCHER_MAINTAIN_PARTICIPANT_LOOP_PRE(RANDOM_EVENTS_LOCAL_DATA &sFMREdata)
	INT iEvent
	REPEAT sFMREdata.sFMREScriptEventdata.iNumEvents iEvent
		CLEAR_FMRE_SERVER_BIT(iEvent, eFMRESERVERBITSET_ACTIVATED_BY_PLAYER #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	ENDREPEAT
ENDPROC

PROC FMRE_LAUNCHER_MAINTAIN_PARTICIPANT_LOOP(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT iPlayer #IF IS_DEBUG_BUILD , PLAYER_INDEX playerID #ENDIF )

	INT iEvent
				
	REPEAT sFMREdata.sFMREScriptEventdata.iNumEvents iEvent
		IF FMRE_GET_PLAYER_STATE_INT(iEvent, iPlayer) = eFMREPLAYERSTATE_ACTIVE
			
			SET_FMRE_SERVER_BIT(iEvent, eFMRESERVERBITSET_ACTIVATED_BY_PLAYER #IF IS_DEBUG_BUILD , TRUE #ENDIF)
			
			IF FMRE_GET_STATE(iEvent) = eFMRESTATE_AVAILABLE
				PRINTLN("[FMRE] - [FMRE_LAUNCHER_MAINTAIN_PARTICIPANT_LOOP] - Event ", iEvent, " activated by player ",iPlayer, " ", GET_PLAYER_NAME(playerID))
			ENDIF
			
		ENDIF
	ENDREPEAT

ENDPROC


//----------------------
//	DEBUG
//----------------------

#IF IS_DEBUG_BUILD
FUNC eFM_RANDOM_EVENT_STATE FMRE_LAUNCHER_GET_STATE_FOR_PAYPHONE_FLOW(PAYPHONE_FLOW_STATE eState)

	SWITCH eState
		CASE ePAYPHONEFLOW_STATE_WAIT 			RETURN eFMRESTATE_INACTIVE
		CASE ePAYPHONEFLOW_STATE_AVAILABLE 		RETURN eFMRESTATE_AVAILABLE
		CASE ePAYPHONEFLOW_STATE_ACTIVE 		
		CASE ePAYPHONEFLOW_STATE_LAUNCHING		
		CASE ePAYPHONEFLOW_STATE_ON_MISSION		RETURN eFMRESTATE_ACTIVE
	ENDSWITCH
	
	RETURN eFMRESTATE_INACTIVE

ENDFUNC

FUNC HUD_COLOURS FMRE_LAUNCHER_DEBUG_GET_STATE_COLOUR(eFM_RANDOM_EVENT_STATE eState)

	SWITCH eState
		CASE eFMRESTATE_INACTIVE		RETURN HUD_COLOUR_GREY
		CASE eFMRESTATE_AVAILABLE		RETURN HUD_COLOUR_WHITE
		CASE eFMRESTATE_ACTIVE			RETURN HUD_COLOUR_GREEN
		CASE eFMRESTATE_CLEANUP			RETURN HUD_COLOUR_GREY
	ENDSWITCH
	
	RETURN HUD_COLOUR_WHITE

ENDFUNC

CONST_FLOAT FMRE_LAUNCHER_DEBUG_WINDOW_X	0.01
CONST_FLOAT FMRE_LAUNCHER_DEBUG_WINDOW_Y	0.685

FUNC FLOAT FMRE_LAUNCHER_DEBUG_GET_SPACER_X(INT iColumn)

	IF iColumn = 1
		RETURN 0.1
	ENDIF
	
	RETURN 0.3
	
ENDFUNC

PROC FMRE_LAUNCHER_DEBUG_SET_TEXT_PROPERTIES(FLOAT &fDrawX, FLOAT &fDrawY, INT iRow, INT iColumn, eFM_RANDOM_EVENT_STATE eState)

	INT r, g, b, a
	GET_HUD_COLOUR(FMRE_LAUNCHER_DEBUG_GET_STATE_COLOUR(eState), r, g, b, a)
	
	FLOAT fSpacerX = FMRE_LAUNCHER_DEBUG_GET_SPACER_X(iColumn)

	fDrawX = FMRE_LAUNCHER_DEBUG_WINDOW_X + (0.287 * fSpacerX * (TO_FLOAT(iColumn)))
	fDrawY = FMRE_LAUNCHER_DEBUG_WINDOW_Y + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + ((TO_FLOAT(iRow)+1) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)

	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	SET_TEXT_COLOUR(r, g, b, a)

ENDPROC

PROC FMRE_DRAW_EVENT_TRIGGER(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT iEvent, eFM_RANDOM_EVENT_STATE eState)

	TEXT_LABEL_63 tlEvent
	
	tlEvent += iEvent
	tlEvent += " - "
	tlEvent += GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(sFMREdata.sFMREScriptEventdata.Events[iEvent].iType)
    tlEvent += " - "
	tlEvent += FMRE_GET_STATE_STRING(eState)
					   
	DRAW_DEBUG_TEXT(tlEvent, FMRE_LAUNCHER_GET_TRIGGER_POINT(iEvent), 255, 0, 0)
						
ENDPROC

PROC FMRE_LAUNCHER_DEBUG_DISPLAY_WINDOW(RANDOM_EVENTS_LOCAL_DATA &sFMREdata)

	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_E, KEYBOARD_MODIFIER_ALT, "Random Events")
		IF g_sRandomEventDebug.bDisplayDebugWindow
			g_sRandomEventDebug.bDisplayDebugWindow = FALSE
			PRINTLN("[FMRE] - [FMRE_LAUNCHER_DEBUG_DISPLAY_WINDOW] - Disabled")
		ELSE
			g_sRandomEventDebug.bDisplayDebugWindow = TRUE
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			PRINTLN("[FMRE] - [FMRE_LAUNCHER_DEBUG_DISPLAY_WINDOW] - Enabled")
		ENDIF
	ENDIF

	IF NOT g_sRandomEventDebug.bDisplayDebugWindow
		EXIT
	ENDIF
	
	IF NOT sFMREdata.bDebugDisplay
		sFMREdata.bDebugDisplay = TRUE
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	ENDIF

	FLOAT fDrawX, fDrawY
	INT iColumn
	
	// Black box values
	FLOAT fBlackW = 0.3//Tweakz
	FLOAT fBlackH = 0.182
	FLOAT fBlackX = FMRE_LAUNCHER_DEBUG_WINDOW_X + (fBlackW / 2.0) - 0.001//0.383//TweakX
	FLOAT fBlackY = 0.699//Tweaky
	
	// draw black box
	DRAW_RECT(fBlackX, fBlackY, fBlackW, fBlackH, 0, 0, 0, 190)
	
	fDrawY = FMRE_LAUNCHER_DEBUG_WINDOW_Y + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY
	
	fDrawX = FMRE_LAUNCHER_DEBUG_WINDOW_X + (0.287 * FMRE_LAUNCHER_DEBUG_GET_SPACER_X(iColumn) * (TO_FLOAT(iColumn)))
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fDrawX, fDrawY, "STRING", "ID")
	iColumn++
	
	fDrawX = FMRE_LAUNCHER_DEBUG_WINDOW_X + (0.287 * FMRE_LAUNCHER_DEBUG_GET_SPACER_X(iColumn) * (TO_FLOAT(iColumn)))
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fDrawX, fDrawY, "STRING", "Event")
	iColumn++
	
	fDrawX = FMRE_LAUNCHER_DEBUG_WINDOW_X + (0.287 * FMRE_LAUNCHER_DEBUG_GET_SPACER_X(iColumn) * (TO_FLOAT(iColumn)))
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fDrawX, fDrawY, "STRING", "State")
	iColumn = 0
	
	INT iEvent
	REPEAT sFMREdata.sFMREScriptEventdata.iNumEvents iEvent
	
		eFM_RANDOM_EVENT_STATE eState = FMRE_GET_STATE(iEvent)
		
		// Index
		FMRE_LAUNCHER_DEBUG_SET_TEXT_PROPERTIES(fDrawX, fDrawY, iEvent, iColumn, eState)
		DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY, "NUMBER", iEvent)
		iColumn++
	
		// Name
		FMRE_LAUNCHER_DEBUG_SET_TEXT_PROPERTIES(fDrawX, fDrawY, iEvent, iColumn, eState)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fDrawX, fDrawY, "STRING", GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(sFMREdata.sFMREScriptEventdata.Events[iEvent].iType))
		iColumn++
			
		// State
		FMRE_LAUNCHER_DEBUG_SET_TEXT_PROPERTIES(fDrawX, fDrawY, iEvent, iColumn, eState)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fDrawX, fDrawY, "STRING", FMRE_GET_STATE_STRING(eState))
		iColumn = 0
		
		IF eState = eFMRESTATE_AVAILABLE
		OR eState = eFMRESTATE_ACTIVE
			FMRE_DRAW_EVENT_TRIGGER(sFMREdata, iEvent, eState)
		ENDIF

	ENDREPEAT
	
	iEvent++
	
	// Payphone Flow
	eFM_RANDOM_EVENT_STATE eState = FMRE_LAUNCHER_GET_STATE_FOR_PAYPHONE_FLOW(PAYPHONEFLOW_GET_STATE())
	
	// Index
	FMRE_LAUNCHER_DEBUG_SET_TEXT_PROPERTIES(fDrawX, fDrawY, iEvent, iColumn, eState)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fDrawX, fDrawY, "STRING", "N/A")
	iColumn++

	// Name
	FMRE_LAUNCHER_DEBUG_SET_TEXT_PROPERTIES(fDrawX, fDrawY, iEvent, iColumn, eState)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fDrawX, fDrawY, "STRING", "PAYPHONE FLOW")
	iColumn++
		
	// State
	FMRE_LAUNCHER_DEBUG_SET_TEXT_PROPERTIES(fDrawX, fDrawY, iEvent, iColumn, eState)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fDrawX, fDrawY, "STRING", PAYPHONEFLOW_GET_STATE_NAME(PAYPHONEFLOW_GET_STATE()))
	iColumn = 0
	
	iEvent++
	
	/////
	

ENDPROC

FUNC VECTOR FMRE_LAUNCHER_GET_DEBUG_WARP_COORD(RANDOM_EVENTS_LOCAL_DATA &sFMREdata, INT iEvent)
	VECTOR vDebugWarpCoord
	
	IF sFMREdata.Events[iEvent].DebugWarpCoord != NULL
		vDebugWarpCoord = CALL sFMREdata.Events[iEvent].DebugWarpCoord(iEvent)
	ELSE
		vDebugWarpCoord = GlobalServerBD_RandomEvents.Events[iEvent].vTriggerPoint
	ENDIF
	
	RETURN vDebugWarpCoord
ENDFUNC

PROC FMRE_LAUNCHER_DEBUG_WARP(RANDOM_EVENTS_LOCAL_DATA &sFMREdata)

	IF NOT sFMREdata.bDebugWarp
		EXIT
	ENDIF
	
	IF sFMREdata.iDebugWarpID = -1
		PRINTLN("[FMRE] FMRE_LAUNCHER_DEBUG_WARP - sFMREdata.iDebugWarpID = -1, setting sFMREdata.bDebugWarp = FALSE")
		
		sFMREdata.bDebugWarp = FALSE
		
		EXIT
	ENDIF
	
	VECTOR vDebugWarpCoord
	
	SWITCH FMRE_GET_STATE(sFMREdata.iDebugWarpID)
		CASE eFMRESTATE_AVAILABLE
		CASE eFMRESTATE_ACTIVE
			PRINTLN("[FMRE] FMRE_LAUNCHER_DEBUG_WARP - Warping to sFMREdata.iDebugWarpID = ", sFMREdata.iDebugWarpID)
			
			vDebugWarpCoord = FMRE_LAUNCHER_GET_DEBUG_WARP_COORD(sFMREdata, sFMREdata.iDebugWarpID)
			
			IF IS_VECTOR_ZERO(vDebugWarpCoord)
				PRINTLN("[FMRE] FMRE_LAUNCHER_DEBUG_WARP - IS_VECTOR_ZERO(vDebugWarpCoord) = TRUE, setting sFMREdata.bDebugWarp = FALSE")
				
				sFMREdata.bDebugWarp = FALSE
			ELIF NET_WARP_TO_COORD(vDebugWarpCoord, 0.0)
				PRINTLN("[FMRE] FMRE_LAUNCHER_DEBUG_WARP - Completed warp to sFMREdata.iDebugWarpID = ", sFMREdata.iDebugWarpID)
				
				sFMREdata.bDebugWarp = FALSE
			ENDIF
		BREAK
		
		DEFAULT
			PRINTLN("[FMRE] FMRE_LAUNCHER_DEBUG_WARP - FMRE_GET_STATE(", sFMREdata.iDebugWarpID, ") = ", FMRE_GET_STATE_STRING(FMRE_GET_STATE(sFMREdata.iDebugWarpID)), ", setting sFMREdata.bDebugWarp = FALSE")
			
			sFMREdata.bDebugWarp = FALSE
		BREAK
	ENDSWITCH

ENDPROC
#ENDIF

PROC FMRE_MAINTAIN_LAUNCHER(RANDOM_EVENTS_LOCAL_DATA &sFMREdata)

	IF NOT FMRE_IS_LAUNCHER_SAFE_TO_RUN(sFMREdata)
		EXIT
	ENDIF

	FMRE_LAUNCHER_MAINTAIN_CLIENT(sFMREdata)
	FMRE_LAUNCHER_MAINTAIN_SERVER(sFMREdata)
	
	#IF IS_DEBUG_BUILD
	FMRE_LAUNCHER_DEBUG_DISPLAY_WINDOW(sFMREdata)
	FMRE_LAUNCHER_DEBUG_WARP(sFMREdata)
	#ENDIF

ENDPROC
