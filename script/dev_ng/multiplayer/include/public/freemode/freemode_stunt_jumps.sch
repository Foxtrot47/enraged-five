USING "rage_builtins.sch"
USING "globals.sch"
USING "fmmc_vars.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	streamed_scripts.sch
//		CREATED			:	Keith
//		DESCRIPTION		:	Associates streamed scripts with peds/objects.
//		NOTES			:	This used to be called 'initial_objects' in GTA4, E1, and E2.
//
//							This procedure is called from startup.sc.
//							All object and peds associated with streamed scripts should be
//								setup here.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************



// -----------------------------------------------------------------------------------------------------------
FUNC VECTOR SJ_START_FM(VECTOR vCentre, BOOL bMax = FALSE)
	VECTOR vReturn = <<-5, -5, 0>>
	IF bMax
		vReturn = <<5, 5, 3>>
	ENDIF
	vReturn = vCentre + vReturn
	RETURN vReturn
ENDFUNC

FUNC VECTOR SJ_END_FM(VECTOR vCentre, BOOL bMax = FALSE)
	VECTOR vReturn = <<-7, -7, 0>>
	IF bMax
		vReturn = <<7, 7, 3>>
	ENDIF
	vReturn = vCentre + vReturn
	RETURN vReturn
ENDFUNC

PROC initialise_usj_FREEMODE(FM_UNIQUE_STUNT_JUMP_STRUCT &sUSJStruct )

    sUSJStruct.V_FMUSJ_START[0]  =  << 9.8955, 1715.3168, 226.4283 >>
	sUSJStruct.V_FMUSJ_END[0]	 = << 68.3054, 1790.8459, 195.0807 >>
	sUSJStruct.V_FMUSJ_CAM[0]	= 	<< -1.7412, 1748.9087, 226.9136 >>
	sUSJStruct.i_number_of_stunt_jumps++
	
	sUSJStruct.V_FMUSJ_START[1] = << -439.5815, -1188.2166, 53.4240 >>
	sUSJStruct.V_FMUSJ_END[1]	= << -449.0529, -1277.9293, 43.8013 >>
	sUSJStruct.V_FMUSJ_CAM[1]	= << -462.6627, -1212.3562, 58.3663 >>
	sUSJStruct.i_number_of_stunt_jumps++
	
	// CS AP1 Cliff
	sUSJStruct.V_FMUSJ_START[2] = << 472.9661, 4322.4878, 59.2018 >>
	sUSJStruct.V_FMUSJ_END[2]	= << 446.4861, 4345.6230, 62.5362 >>
	sUSJStruct.V_FMUSJ_CAM[2]	= <<454.123535,4323.500977,68.739319>>
	sUSJStruct.i_number_of_stunt_jumps++
	
	// CS AP1 Pier
	sUSJStruct.V_FMUSJ_START[3] = << -166.3604, 6574.9868, 10.8703 >>
	sUSJStruct.V_FMUSJ_END[3]	= << -144.2458, 6595.9819, 9.8798 >>
	sUSJStruct.V_FMUSJ_CAM[3]	= <<-166.026306,6588.806152,11.696039>>
	sUSJStruct.i_number_of_stunt_jumps++
	
	// CS3 03 into CS1 14b
	sUSJStruct.V_FMUSJ_START[4] = << -970.4561, 4182.1548, 134.3799 >>
	sUSJStruct.V_FMUSJ_END[4]	= << -1039.0483, 4247.8003, 113.7367 >>
	sUSJStruct.V_FMUSJ_CAM[4]	= <<-977,4247,144>>
	sUSJStruct.i_number_of_stunt_jumps++
	
	// DT1 23 Parking Garage
	sUSJStruct.V_FMUSJ_START[5] = << 16.2194, -1042.7257, 36.2442 >>
	sUSJStruct.V_FMUSJ_END[5]	= << -54.6231, -1016.8248, 27.8771 >>
	sUSJStruct.V_FMUSJ_CAM[5]	= <<-36,-1037,47>>
	sUSJStruct.i_number_of_stunt_jumps++
	
	// DT1 09 Parking Garage
	sUSJStruct.V_FMUSJ_START[6] = << -271.8840, -771.1926, 55.2616 >>
	sUSJStruct.V_FMUSJ_END[6]	= << -200.4885, -803.0355, 29.4538 >>
	sUSJStruct.V_FMUSJ_CAM[6]	= <<-180,-812, 60>>
	sUSJStruct.i_number_of_stunt_jumps++
	
	// DT1 02 Overpass
	sUSJStruct.V_FMUSJ_START[7] = << -80.1844, -538.0173, 39.1475 >>
	sUSJStruct.V_FMUSJ_END[7]	= << -122.0103, -527.2867, 29.0340 >>
	sUSJStruct.V_FMUSJ_CAM[7]	= <<-113,-545, 45>>
	sUSJStruct.i_number_of_stunt_jumps++
	
	// SM 23 Overpass
	sUSJStruct.V_FMUSJ_START[8] = << -1595.8674, -742.9480, 21.4113 >>
	sUSJStruct.V_FMUSJ_END[8]	= << -1646.2194, -709.7570, 10.0878 >>
	sUSJStruct.V_FMUSJ_CAM[8]	= <<-1610, -714, 22>>
	sUSJStruct.i_number_of_stunt_jumps++
	
	// Rockford Plaza
	sUSJStruct.V_FMUSJ_START[9] = << -236.3998, -209.7554, 47.9509 >>
	sUSJStruct.V_FMUSJ_END[9]	= << -295.7286, -209.4084, 36.9628 >>
	sUSJStruct.V_FMUSJ_CAM[9]	= <<-268,-223,50>>
	sUSJStruct.i_number_of_stunt_jumps++
	
	// Richman thru Barrier
	sUSJStruct.V_FMUSJ_START[10] = << -1448.4990, 404.7888, 109.2198 >>
	sUSJStruct.V_FMUSJ_END[10]	= << -1482.0387, 284.5853, 62.7532 >>
	sUSJStruct.V_FMUSJ_CAM[10]	= <<-1496,368,107>>
	sUSJStruct.i_number_of_stunt_jumps++
	
	
ENDPROC

/*
PROC create_usj_FREEMODE(FM_UNIQUE_STUNT_JUMP_STRUCT &sUSJStruct )

	INT i
	
	FOR i = 0 to sUSJStruct.i_number_of_stunt_jumps - 1
		
		//DELETE_STUNT_JUMP(sUSJStruct.IDstuntjumpfm[i])
		
		sUSJStruct.IDstuntjumpfm[i] = ADD_STUNT_JUMP(SJ_START_FM(sUSJStruct.V_FMUSJ_START[i]), SJ_START_FM(sUSJStruct.V_FMUSJ_START[i],TRUE), SJ_END_FM(sUSJStruct.V_FMUSJ_END[i]), SJ_END_FM(sUSJStruct.V_FMUSJ_END[i],TRUE), sUSJStruct.V_FMUSJ_CAM[i], 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", i,"] At the coordinates [", sUSJStruct.V_FMUSJ_START[i], "]")
	ENDFOR
	g_savedGlobals.sAmbient.stuntJumps.bFMStuntJumpsRegistered = TRUE
ENDPROC
*/


/*

PROC Initialise_Stunt_Jumps_FM()
	VECTOR vTempStart, vTempEnd, vCam
	IF NOT g_savedGlobals.sAmbient.stuntJumps.bFMStuntJumpsRegistered
		// Adding stunt jumps here for now
		INT iCOuntOfJumps
		PRINTLN("MPUSJ Initialise_Stunt_Jumps_FM Setting up")
		FOR iCOuntOfJumps = 0 TO (ciMAX_STUNT_JUMPS - 1)
			IF g_sjsIndex[iCOuntOfJumps] != NULL
				DELETE_STUNT_JUMP(g_sjsIndex[iCOuntOfJumps])			
			ENDIF
		ENDFOR
		iCOuntOfJumps = 0 
		// Vinewood Hills
		vTempStart	= << 11.8955, 1707.3168, 225.4283 >>
		vTempEnd	= << 68.3054, 1790.8459, 195.0807 >>
		vCam		= << -1.7412, 1748.9087, 226.9136 >>
		g_sjsIndex[iCOuntOfJumps] = ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", iCOuntOfJumps,"] At the coordinates [", vTempStart, "]")
		iCOuntOfJumps++
		// Little Seoul
		vTempStart	= << -439.5815, -1188.2166, 53.4240 >>
		vTempEnd	= << -449.0529, -1277.9293, 43.8013 >>
		vCam		= << -462.6627, -1212.3562, 58.3663 >>
		g_sjsIndex[iCOuntOfJumps] = ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", iCOuntOfJumps,"] At the coordinates [", vTempStart, "]")
		iCOuntOfJumps++
		
		// CS AP1 Cliff
		vTempStart	= << 472.9661, 4322.4878, 59.2018 >>
		vTempEnd	= << 446.4861, 4345.6230, 62.5362 >>
		vCam		= <<454.123535,4323.500977,68.739319>>
		g_sjsIndex[iCOuntOfJumps] = ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", iCOuntOfJumps,"] At the coordinates [", vTempStart, "]")
		iCOuntOfJumps++
		
		// CS AP1 Pier
		vTempStart	= << -166.3604, 6574.9868, 10.8703 >>
		vTempEnd	= << -144.2458, 6595.9819, 9.8798 >>
		vCam		= <<-166.026306,6588.806152,11.696039>>
		g_sjsIndex[iCOuntOfJumps] = ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", iCOuntOfJumps,"] At the coordinates [", vTempStart, "]")
		iCOuntOfJumps++
		
		// CS3 03 into CS1 14b
		vTempStart	= << -970.4561, 4182.1548, 134.3799 >>
		vTempEnd	= << -1039.0483, 4247.8003, 113.7367 >>
		vCam		= <<-977,4247,144>>
		g_sjsIndex[iCOuntOfJumps] = ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", iCOuntOfJumps,"] At the coordinates [", vTempStart, "]")
		iCOuntOfJumps++
		
		// DT1 23 Parking Garage
		vTempStart	= << 16.2194, -1042.7257, 36.2442 >>
		vTempEnd	= << -54.6231, -1016.8248, 27.8771 >>
		vCam		= <<-36,-1037,47>>
		g_sjsIndex[iCOuntOfJumps] = ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", iCOuntOfJumps,"] At the coordinates [", vTempStart, "]")
		iCOuntOfJumps++
		
		// DT1 09 Parking Garage
		vTempStart	= << -271.8840, -771.1926, 55.2616 >>
		vTempEnd	= << -200.4885, -803.0355, 29.4538 >>
		vCam		= <<-180,-812, 60>>
		g_sjsIndex[iCOuntOfJumps] = ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", iCOuntOfJumps,"] At the coordinates [", vTempStart, "]")
		iCOuntOfJumps++
		
		// DT1 02 Overpass
		vTempStart	= << -80.1844, -538.0173, 39.1475 >>
		vTempEnd	= << -122.0103, -527.2867, 29.0340 >>
		vCam		= <<-113,-545, 45>>
		g_sjsIndex[iCOuntOfJumps] = ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", iCOuntOfJumps,"] At the coordinates [", vTempStart, "]")
		iCOuntOfJumps++
		
		// SM 23 Overpass
		vTempStart	= << -1595.8674, -742.9480, 21.4113 >>
		vTempEnd	= << -1646.2194, -709.7570, 10.0878 >>
		vCam		= <<-1610, -714, 22>>
		g_sjsIndex[iCOuntOfJumps] = ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", iCOuntOfJumps,"] At the coordinates [", vTempStart, "]")
		iCOuntOfJumps++
		
		// Rockford Plaza
		vTempStart	= << -236.3998, -209.7554, 47.9509 >>
		vTempEnd	= << -295.7286, -209.4084, 36.9628 >>
		vCam		= <<-268,-223,50>>
		g_sjsIndex[iCOuntOfJumps] = ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", iCOuntOfJumps,"] At the coordinates [", vTempStart, "]")
		iCOuntOfJumps++
		
		// Richman thru Barrier
		vTempStart	= << -1448.4990, 404.7888, 109.2198 >>
		vTempEnd	= << -1482.0387, 284.5853, 62.7532 >>
		vCam		= <<-1496,368,107>>
		g_sjsIndex[iCOuntOfJumps] = ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150, ciFREEMODE_PLAYER_SJS_GROUP)
		PRINTLN("MPUSJ Added stunt jump [", iCOuntOfJumps,"] At the coordinates [", vTempStart, "]")
		
		//iCOuntOfJumps++
//		// Vinewood House
//		vTempStart	= << -1448.4990, 404.7888, 109.2198 >>
//		vTempEnd	= << -1482.0387, 284.5853, 62.7532 >>
//		vCam		= <<-1496,368,107>>
//		ADD_STUNT_JUMP(SJ_START_FM(vTempStart), SJ_START_FM(vTempStart,TRUE), SJ_END_FM(vTempEnd), SJ_END_FM(vTempEnd,TRUE), vCam, 150)
		g_savedGlobals.sAmbient.stuntJumps.bFMStuntJumpsRegistered = TRUE
		PRINTLN("MPUSJ SETTING g_savedGlobals.sAmbient.stuntJumps.bFMStuntJumpsRegistered  to TRUE")
	ENDIF
ENDPROC*/



