// FMMC_SCTV_TICKER.sch

USING "rage_builtins.sch"
USING "globals.sch"
USING "MP_globals_UGC.sch"
USING "MP_globals_FM.sch"
USING "net_team_info.sch"

//Set the display for the ticker score on
PROC SET_SCTV_TICKER_SCORE_ON()
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === SET_SCTV_TICKER_SCORE_ON")
	g_sSctvTickerQueue.bDisplayScore = TRUE
ENDPROC
//Set the display for the ticker score off
PROC SET_SCTV_TICKER_SCORE_OFF()
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === SET_SCTV_TICKER_SCORE_OFF")
	g_sSctvTickerQueue.bDisplayScore = FALSE
ENDPROC
//Should we display for the ticker score
FUNC BOOL SHOULD_SCTV_TICKER_SCORE_DISPLAY()
	RETURN g_sSctvTickerQueue.bDisplayScore
ENDFUNC

//Set the display for the ticker score on
PROC SET_SCTV_TICKER_UI_ON()
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === SET_SCTV_TICKER_UI_ON")
	g_sSctvTickerQueue.bActive = TRUE
ENDPROC
//Set the display for the ticker score off
PROC SET_SCTV_TICKER_UI_OFF()
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === SET_SCTV_TICKER_UI_OFF")
	g_sSctvTickerQueue.bActive = FALSE
ENDPROC



//Set the team that we are spectating
PROC SET_SCTV_TICKER_SPECTATING_TEAM(INT iSpectatingTeam)
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === SET_SCTV_TICKER_SCORE_ON")
	g_sSctvTickerQueue.iSpectatingTeam = iSpectatingTeam
ENDPROC

//Clear a pos on the SCTV ticker queue
PROC CLEAR_SCTV_TICKER_FEED_STRUCT_POS(INT iPos)
	SCTV_TICKER_FEED_STRUCT_DATA sSctvTickerQueue
	g_sSctvTickerQueue.sData[iPos] = sSctvTickerQueue
ENDPROC

//Clear the SCTV ticker queue
PROC CLEAR_SCTV_TICKER_FEED_STRUCT()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === CLEAR_SCTV_TICKER_FEED_STRUCT")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	//Clear the queue
	INT iLoop 
	FOR iLoop = 0 TO (ciSCTV_TICKER_QUEUE_MAX - 1)
		CLEAR_SCTV_TICKER_FEED_STRUCT_POS(iLoop)
	ENDFOR
	//Clear the score
	SCTV_TICKER_SCORE_STRUCT_DATA 	sScoreData
	FOR iLoop = 0 TO (ciSCTV_TICKER_SCORE_MAX - 1)
		g_sSctvTickerQueue.sScoreData[iLoop] = sScoreData
	ENDFOR
	//clear other stuff
	SET_SCTV_TICKER_SCORE_OFF()
	//Clear that it's active
	SET_SCTV_TICKER_UI_OFF()
	g_sSctvTickerQueue.iLength = 0
ENDPROC

//Reset the local vars for clean up
PROC CLEANUP_SCTV_TICKER_QUEUE_LOCAL_VARS(MAINTAIN_SCTV_TICKER_QUEUE_DATA &sSctvQueueLocalData)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === CLEANUP_SCTV_TICKER_QUEUE_LOCAL_VARS")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	IF HAS_SCALEFORM_MOVIE_LOADED(sSctvQueueLocalData.scfIndex)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sSctvQueueLocalData.scfIndex)
	ENDIF
	//Clear the score
	INT iLoop
	SCTV_TICKER_SCORE_STRUCT_DATA 	sScoreData
	FOR iLoop = 0 TO (ciSCTV_TICKER_SCORE_MAX - 1)
		sSctvQueueLocalData.sScoreData[iLoop] = sScoreData
	ENDFOR
	//Clear the other Vars
	sSctvQueueLocalData.iStage = 0
	sSctvQueueLocalData.iSpectatingTeam = 0
	sSctvQueueLocalData.bDisplay = FALSE
	sSctvQueueLocalData.ScoreControlTimer = NULL
	sSctvQueueLocalData.scfIndex = NULL
ENDPROC

//Debug functionality
#IF IS_DEBUG_BUILD
//Convert a type to a pretty string
FUNC STRING GET_SCTV_TICKER_FOR_DEBUG_PRINT(INT iType)
	SWITCH iType
		CASE ciSCTV_TICKER_INVALID    									RETURN "INVALID"
		CASE ciSCTV_TICKER_TICKER    									RETURN "TICKER"
		CASE ciSCTV_TICKER_TYPE_STRING_AND_PLAYER_NAME 					RETURN "TYPE_STRING_AND_PLAYER_NAME"
		CASE ciSCTV_TICKER_WEAPON_TICKER  								RETURN "WEAPON_TICKER"
		CASE ciSCTV_TICKER_WITH_INT      								RETURN "INT"
		CASE ciSCTV_TICKER_WITH_TWO_INTS  								RETURN "TWO_INTS"
		CASE ciSCTV_TICKER_WITH_THREE_INTS      						RETURN "THREE_INTS"
		CASE ciSCTV_TICKER_WITH_FLOAT_AND_INT     						RETURN "FLOAT_AND_INT"
		CASE ciSCTV_TICKER_WITH_FLOAT      								RETURN "FLOAT"
		CASE ciSCTV_TICKER_WITH_STRING      							RETURN "STRING"
		CASE ciSCTV_TICKER_WITH_STRING_AND_INT     						RETURN "STRING_AND_INT"
		CASE ciSCTV_TICKER_WITH_STRING_AND_TWO_INTS    				 	RETURN "STRING_AND_TWO_INTS"
		CASE ciSCTV_TICKER_WITH_STRING_AND_FLOAT  						RETURN "STRING_AND_FLOAT"
		CASE ciSCTV_TICKER_WITH_TWO_STRINGS      						RETURN "TWO_STRINGS"
		CASE ciSCTV_TICKER_WITH_TWO_STRINGS_AND_TWO_INTS			  	RETURN "TWO_STRINGS_AND_TWO_INTS"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME      						RETURN "PLAYER_NAME"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING      				RETURN "PLAYER_NAME_STRING"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_TIME  					RETURN "PLAYER_NAME_AND_TIME"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_TIME      		RETURN "PLAYER_NAME_STRING_AND_TIME"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING      RETURN "PLAYER_NAME_STRING_TIME_AND_STRING"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_TIME_STRING_AND_INT  RETURN "PLAYER_NAME_STRING_TIME_STRING_AND_INT"
		CASE ciSCTV_TICKER_WITH_STRING_INT_PLAYER_NAME_AND_TIME      	RETURN "STRING_INT_PLAYER_NAME_AND_TIME"
		CASE ciSCTV_TICKER_WITH_STRING_PLAYER_NAME_AND_TIME     		RETURN "STRING_PLAYER_NAME_AND_TIME"
		CASE ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES  						RETURN "TWO_PLAYER_NAMES"
		CASE ciSCTV_TICKER_WITH_THREE_PLAYER_NAMES     					RETURN "THREE_PLAYER_NAMES"
		CASE ciSCTV_TICKER_WITH_FOUR_PLAYER_NAMES    					RETURN "FOUR_PLAYER_NAMES"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_STRING    				RETURN "PLAYER_NAME_AND_STRING"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_TWO_STRINGS    			RETURN "PLAYER_NAME_AND_TWO_STRINGS"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_INT     				RETURN "PLAYER_NAME_AND_INT"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_FLOAT      				RETURN "PLAYER_NAME_AND_FLOAT"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_INT      		RETURN "PLAYER_NAME_STRING_AND_INT"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_INT_AND_STRING      	RETURN "PLAYER_NAME_STRING_INT_AND_STRING"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_MISSION_NAME     RETURN "PLAYER_NAME_STRING_AND_MISSION_NAME"
		CASE ciSCTV_TICKER_WITH_CUSTOM_STRING_AND_INT      				RETURN "CUSTOM_STRING_AND_INT"
		CASE ciSCTV_TICKER_WITH_TWO_CUSTOM_STRINGS      				RETURN "TWO_CUSTOM_STRINGS"
		CASE ciSCTV_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME      		RETURN "CUSTOM_STRING_AND_PLAYER_NAME"
		CASE ciSCTV_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_AND_INT   RETURN "CUSTOM_STRING_AND_PLAYER_NAME_AND_INT"
		CASE ciSCTV_TICKER_WITH_STRING_AND_PLAYER_NAME      			RETURN "STRING_AND_PLAYER_NAME"
		CASE ciSCTV_TICKER_WITH_STRING_INT_AND_PLAYER_NAME  	   		RETURN "STRING_INT_AND_PLAYER_NAME"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_TWO_INTS  				RETURN "PLAYER_NAME_AND_TWO_INTS"
		CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS     	RETURN "PLAYER_NAME_STRING_AND_TWO_INTS"
		CASE ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_INTS      		RETURN "TWO_PLAYER_NAMES_AND_TWO_INTS"
		CASE ciSCTV_TICKER_WITH_TWO_PLAYER_NAME_STRING_AND_INT      	RETURN "TWO_PLAYER_NAME_STRING_AND_INT"
		CASE ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES_AND_INT  				RETURN "TWO_PLAYER_NAMES_AND_INT"
	ENDSWITCH
	RETURN ""
ENDFUNC
//Convert a priority to a pretty string
FUNC STRING GET_SCTV_TICKER_PRIORITY_FOR_DEBUG_PRINT(INT iPriority)
	SWITCH iPriority
		CASE ciSCTV_TICKER_PRIORITY_LOW  	RETURN "LOW"
		CASE ciSCTV_TICKER_PRIORITY_MEDIUM  RETURN "MED"
		CASE ciSCTV_TICKER_PRIORITY_HIGH 	RETURN "HIGH"
	ENDSWITCH
	RETURN ""
ENDFUNC
#ENDIF

//gets the string for the type of mission being displayed. 
FUNC STRING GET_MISSION_TYPE_STRING_FOR_SCTV_TICKER()

	SWITCH g_FMMC_STRUCT.iMissionType 	
		CASE FMMC_TYPE_MISSION
			IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_CTF_MISSION)
				IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_GTA)
					RETURN "SCTV_T_CTFG" //GTA CTF
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_RAID)
					RETURN "SCTV_T_CTFR" //Raid CTF
				ELIF   IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_CONTEND)
					RETURN "SCTV_T_CTFC" //Contend CTF
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_HOLD)
					RETURN "SCTV_T_CTFH" //Hold CTF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("GET_MISSION_TYPE_STRING_FOR_SCTV_TICKER - FMMC_TYPE_MISSION - CTF - ELSE")
				#ENDIF
				ENDIF
			ELIF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_VERSUS_MISSION)
				RETURN "SCTV_T_VS"	//versus
			ELIF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_LTS_MISSION)
				RETURN "SCTV_T_LTS"//LTS
			ELSE
				RETURN "SCLB_MIS_NN" //Mission
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH
			IF g_bFM_ON_VEH_DEATHMATCH
				RETURN "SCLB_VEHDM_NN" 	//Vehicle Deathmatch
			ELIF g_bFM_ON_TEAM_DEATHMATCH
				RETURN "SCLB_TDM_NN" 	//Team Deathmatch
			ELSE
				RETURN "SCLB_DM_NN" 	//Deathmatch
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_RACE
			IF g_b_On_Race 
				RETURN "SCLB_RCE_NN" 	//Race
			ELIF g_b_IsGTARace 
				RETURN "SCLB_GRCE_NN" 	//GTA Race
			ELIF g_b_IsRallyRace 
				RETURN "SCTV_T_RR"		//Rally Race
			ELIF g_b_NonContactRace 
				RETURN "SCTV_T_NCR"		//Non-Contact Race
			ELSE
				PRINTLN("GET_MISSION_TYPE_STRING_FOR_SCTV_TICKER - FMMC_TYPE_RACE - ELSE")
				RETURN "SCLB_RCE_NN"
			ENDIF
		BREAK
		CASE FMMC_TYPE_BASE_JUMP			RETURN "SCLB_BJ_NN"		//Parachute Jump
		CASE FMMC_TYPE_SURVIVAL				RETURN "FMMC_RSTAR_HM" 	//Survival
		CASE FMMC_TYPE_MG_DARTS				RETURN "FMMC_MPM_TY11"	//Darts
		CASE FMMC_TYPE_MG_GOLF				RETURN "FMMC_MPM_TY13"	//Golf
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	RETURN "FMMC_MPM_TY12"	//Shooting Range
		CASE FMMC_TYPE_MG_ARM_WRESTLING		RETURN "FMMC_MPM_TY10"	//Arm Wrestling
		CASE FMMC_TYPE_MG_TENNIS			RETURN "FMMC_MPM_TY9"	//Tennis
	ENDSWITCH
	RETURN ""
ENDFUNC

//get the mission synopsis
FUNC STRING GET_MISSION_SYNOPSIS(INT iNumberOfTeams = 0)
	 IF g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_MISSION
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_CTF_MISSION)
			IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_GTA)
				RETURN "SCTV_TS_CTFG" // GTA – fighting for transport
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_RAID)
				RETURN "SCTV_TS_CTFR" // Raid - protecting their base of operations
			ELIF   IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_CONTEND)
				RETURN "SCTV_TS_CTFC" // Contend – bringing it back to base
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_HOLD)
				RETURN "SCTV_TS_CTFH" // Hold – holding on for dear life
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("GET_MISSION_SYNOPSIS - FMMC_TYPE_MISSION - CTF - ELSE")
			#ENDIF
			ENDIF
		ELIF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_LTS_MISSION)
			SWITCH iNumberOfTeams 
				CASE 2 RETURN "SCTV_TS_LTS2" // Two Team LTS – in a two teams enter one leaves situation
				CASE 3 RETURN "SCTV_TS_LTS3" // Three Team LTS – in a three way… battle for survival 
				CASE 4 RETURN "SCTV_TS_LTS4" // Four Team LTS – unlikely to survive a four team fight
			ENDSWITCH
			RETURN "SCTV_TS_LTS2" // Two Team LTS – in a two teams enter one leaves situation
		ENDIF
	ENDIF
	RETURN ""
ENDFUNC

//remove the item at the top of the ticker queue
PROC REMOVE_ITEM_AT_TOP_OF_SCTV_TICKER_QUEUE()
	INT iloop
	//Move them all down one
	IF g_sSctvTickerQueue.iLength > 0
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === REMOVE_ITEM_AT_TOP_OF_SCTV_TICKER_QUEUE - FOR iloop = 0 TO (", g_sSctvTickerQueue.iLength - 2, ")")
		FOR iloop = 0 TO (g_sSctvTickerQueue.iLength - 2)
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === REMOVE_ITEM_AT_TOP_OF_SCTV_TICKER_QUEUE - iloop = ", iloop)
			g_sSctvTickerQueue.sData[iloop] = g_sSctvTickerQueue.sData[iloop + 1]		
		ENDFOR
		//flatten the top one
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === REMOVE_ITEM_AT_TOP_OF_SCTV_TICKER_QUEUE - CLEAR_SCTV_TICKER_FEED_STRUCT_POS(", g_sSctvTickerQueue.iLength - 1, ")")
		CLEAR_SCTV_TICKER_FEED_STRUCT_POS(g_sSctvTickerQueue.iLength - 1)		
	ENDIF
ENDPROC

//make a space by shifting an array down one scace
PROC MAKE_SPACE_IN_SCTV_TICKER_QUEUE_HERE(INT iHere)
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === MAKE_SPACE_IN_SCTV_TICKER_QUEUE_HERE - iHere = ", iHere)
	INT iLoops
	FOR iLoops = (ciSCTV_TICKER_QUEUE_MAX - 1)  TO (iHere + 1) STEP -1
		g_sSctvTickerQueue.sData[iLoops] = g_sSctvTickerQueue.sData[iLoops - 1]
	ENDFOR
ENDPROC

//Find a slot
FUNC INT GET_SCTV_TICKER_POS_BASED_ON_PRIORITY(INT iPriority)
	INT iloop
	//Loop to see if there's a low priority one
	FOR iloop = 0 TO (g_sSctvTickerQueue.iLength - 1)
		//Found one lower then this
		IF iPriority > g_sSctvTickerQueue.sData[iloop].iPriority
			//Move it all up
			MAKE_SPACE_IN_SCTV_TICKER_QUEUE_HERE(iloop)
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === GET_SCTV_TICKER_POS_BASED_ON_PRIORITY - RETURN = ", iloop)
			//Return this position
			RETURN iloop
		ENDIF
	ENDFOR
	
	//Didn't find one lower priority so increase the queue length
	g_sSctvTickerQueue.iLength++
	//if the queue is full
	IF g_sSctvTickerQueue.iLength > ciSCTV_TICKER_QUEUE_MAX
		//Cap the length
		g_sSctvTickerQueue.iLength = ciSCTV_TICKER_QUEUE_MAX
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === GET_SCTV_TICKER_POS_BASED_ON_PRIORITY - RETURN g_sSctvTickerQueue.iLength = ciSCTV_TICKER_QUEUE_MAX")
		RETURN g_sSctvTickerQueue.iLength
	//There is space in the queue
	ELSE
		//Tell every body about it
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === GET_SCTV_TICKER_POS_BASED_ON_PRIORITY - RETURN g_sSctvTickerQueue.iLength-1 = ", g_sSctvTickerQueue.iLength - 1)
		//return the 
		RETURN g_sSctvTickerQueue.iLength-1
	ENDIF
ENDFUNC

//Add a ticker to the sctv ticker feed
PROC ADD_TICKER_TO_SCTV_TICKER_QUEUE(	INT 			iType, 
										STRING 			sMainTextLabel, 
										INT 			iPriority 		= ciSCTV_TICKER_PRIORITY_MEDIUM, 
										STRING 			sSubString 		= NULL, 
										INT 			iInt 			= 0, 
										INT 			iInt2 			= 0, 
										INT 			iInt3 			= 0, 
										FLOAT 			fFloat 			= 0.0, 
										INT 			iDecimalPoints 	= 1, 
										STRING 			sSubString2 	= NULL,
										STRING 			sSubString3 	= NULL,
										STRING 			sSubString4 	= NULL)
	
	//If we should add it
	IF NOT IS_ROCKSTAR_DEV()
	OR NOT NETWORK_IS_ACTIVITY_SESSION()
	OR NOT IS_PLAYER_SCTV(PLAYER_ID())	
		EXIT
	ENDIF
	
	//Get a position in the arrat
	INT iPos = GET_SCTV_TICKER_POS_BASED_ON_PRIORITY(iPriority)
	
	IF iPos >= 0
	AND iPos < ciSCTV_TICKER_QUEUE_MAX
		
		//Print out the ticker for logs
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === ADD_TICKER_TO_SCTV_TICKER_QUEUE - ", GET_SCTV_TICKER_FOR_DEBUG_PRINT(iType), " - iPriority = ", GET_SCTV_TICKER_PRIORITY_FOR_DEBUG_PRINT(iPriority)," - sMainTextLabel = ", sMainTextLabel," = ", GET_FILENAME_FOR_AUDIO_CONVERSATION(sMainTextLabel), " - sSubString = ", sSubString)
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === ADD_TICKER_TO_SCTV_TICKER_QUEUE - iInt = ", iInt, " - iInt2 = ", iInt2, " - iInt3 = ", iInt3, " - fFloat = ", fFloat, " - sSubString2 = ", sSubString2, " - sSubString3 = ", sSubString3, " - sSubString4 = ", sSubString4)
	
		//Add the data to the queue in the position
		g_sSctvTickerQueue.sData[iPos].iType 				= iType
		g_sSctvTickerQueue.sData[iPos].iPriority 			= iPriority
		g_sSctvTickerQueue.sData[iPos].sMainTextLabel	 	= sMainTextLabel
		g_sSctvTickerQueue.sData[iPos].iInt[0] 				= iInt
		g_sSctvTickerQueue.sData[iPos].iInt[1] 				= iInt2
		g_sSctvTickerQueue.sData[iPos].iInt[2] 				= iInt3
		g_sSctvTickerQueue.sData[iPos].fFloat				= fFloat
		g_sSctvTickerQueue.sData[iPos].iDecimalPoints		= iDecimalPoints
		g_sSctvTickerQueue.sData[iPos].sSubString			= sSubString
		g_sSctvTickerQueue.sData[iPos].sSubStringSmall[0]	= sSubString2
		g_sSctvTickerQueue.sData[iPos].sSubStringSmall[1]	= sSubString3
		g_sSctvTickerQueue.sData[iPos].sSubStringSmall[2]	= sSubString4
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === ADD_TICKER_TO_SCTV_TICKER_QUEUE - No Space")		
		#ENDIF
	ENDIF
ENDPROC

//Build an item in the queue in to atext thing to pass to scaleform
PROC BUILD_SCTV_TICKER_FROM_QUEUE(MAINTAIN_SCTV_TICKER_QUEUE_DATA &sSctvQueueLocalData)	
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === BUILD_SCTV_TICKER_FROM_QUEUE - iType = ", GET_SCTV_TICKER_FOR_DEBUG_PRINT(g_sSctvTickerQueue.sData[0].iType), " - iPriority = ", GET_SCTV_TICKER_PRIORITY_FOR_DEBUG_PRINT(g_sSctvTickerQueue.sData[0].iPriority))	
	//Show the ticker
	BEGIN_SCALEFORM_MOVIE_METHOD(sSctvQueueLocalData.scfIndex, "ADD_FEED_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)	
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(g_sSctvTickerQueue.sData[0].sMainTextLabel)
			//Build the text based on it's type
			SWITCH g_sSctvTickerQueue.sData[0].iType
				CASE ciSCTV_TICKER_WEAPON_TICKER	
				CASE ciSCTV_TICKER_WITH_STRING
       				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubString)
				BREAK
				CASE ciSCTV_TICKER_WITH_INT
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_TWO_INTS
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[1])
				BREAK
				CASE ciSCTV_TICKER_WITH_THREE_INTS
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[1])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[2])
				BREAK
				CASE ciSCTV_TICKER_WITH_FLOAT_AND_INT
       				ADD_TEXT_COMPONENT_FLOAT(g_sSctvTickerQueue.sData[0].fFloat, g_sSctvTickerQueue.sData[0].iDecimalPoints)
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[2])
				BREAK
				CASE ciSCTV_TICKER_WITH_FLOAT
       				ADD_TEXT_COMPONENT_FLOAT(g_sSctvTickerQueue.sData[0].fFloat, g_sSctvTickerQueue.sData[0].iDecimalPoints)
				BREAK
				CASE ciSCTV_TICKER_WITH_STRING_AND_INT
       				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_STRING_AND_TWO_INTS
       				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[1])
				BREAK
				CASE ciSCTV_TICKER_WITH_STRING_AND_FLOAT
       				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_FLOAT(g_sSctvTickerQueue.sData[0].fFloat, g_sSctvTickerQueue.sData[0].iDecimalPoints)
				BREAK
				CASE ciSCTV_TICKER_WITH_TWO_STRINGS
       				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_TWO_STRINGS_AND_TWO_INTS
       				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[1])
				BREAK
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
				BREAK
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_TIME
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_TIME
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_TIME(g_sSctvTickerQueue.sData[0].iInt[0], TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)
				BREAK
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
					ADD_TEXT_COMPONENT_SUBSTRING_TIME(g_sSctvTickerQueue.sData[0].iInt[0], TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)
				BREAK
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_TIME_STRING_AND_INT
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[1])
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
					ADD_TEXT_COMPONENT_SUBSTRING_TIME(g_sSctvTickerQueue.sData[0].iInt[0], TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)
				BREAK
				CASE ciSCTV_TICKER_WITH_STRING_INT_PLAYER_NAME_AND_TIME
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[1])
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
					ADD_TEXT_COMPONENT_SUBSTRING_TIME(g_sSctvTickerQueue.sData[0].iInt[0], TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)
				BREAK
				CASE ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_THREE_PLAYER_NAMES
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[1])
				BREAK
				CASE ciSCTV_TICKER_WITH_FOUR_PLAYER_NAMES
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[1])
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[2])
				BREAK
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_STRING
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_TWO_STRINGS
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubStringSmall[1])
				BREAK
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_INT
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_INT
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_FLOAT
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_FLOAT(g_sSctvTickerQueue.sData[0].fFloat, g_sSctvTickerQueue.sData[0].iDecimalPoints)
				BREAK
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_INT_AND_STRING
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_MISSION_NAME
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
					ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sSctvTickerQueue.sData[0].sSubString)
				BREAK
				CASE ciSCTV_TICKER_WITH_CUSTOM_STRING_AND_INT
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_TWO_CUSTOM_STRINGS
				CASE ciSCTV_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_AND_INT
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
					ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sSctvTickerQueue.sData[0].sSubString)
				BREAK
				CASE ciSCTV_TICKER_WITH_STRING_AND_PLAYER_NAME
           			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_STRING_INT_AND_PLAYER_NAME
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
           			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_AND_TWO_INTS
				CASE ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS
           			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[1])
				BREAK
				CASE ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_INTS
           			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[1])
				BREAK
				CASE ciSCTV_TICKER_WITH_TWO_PLAYER_NAME_STRING_AND_INT
           			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
				BREAK
				CASE ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES_AND_INT
           			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubString)
       				ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sData[0].iInt[0])
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sData[0].sSubStringSmall[0])
				BREAK
			ENDSWITCH
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//Set up the displaying of the score in the top right
PROC SET_UP_SCTV_TICKER_SCORE_SHOW_POSITIONS(MAINTAIN_SCTV_TICKER_QUEUE_DATA &sSctvQueueLocalData, BOOL bShow = FALSE)
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === MAINTAIN_SCTV_TICKER_QUEUE - SHOW_POSITIONS - bShow = ", bShow)
	BEGIN_SCALEFORM_MOVIE_METHOD(sSctvQueueLocalData.scfIndex, "SHOW_POSITIONS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bShow)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//Returns true if we should be displaying the mission synopsis
FUNC BOOL SHOULD_DISPLAY_CLOUD_SYNOPSIS()
	IF g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_MISSION
		//And it'a VS
		IF ((GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_VERSUS_MISSION)
		//Or it's not CTF or an LTF
		OR ((GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType != SATMP_CTF_MISSION)
		AND (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType != SATMP_LTS_MISSION)))
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//Set up the Mission type, name and description 
PROC SET_UP_SCTV_TICKER_TITLE(MAINTAIN_SCTV_TICKER_QUEUE_DATA &sSctvQueueLocalData)
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === MAINTAIN_SCTV_TICKER_QUEUE - SET_TITLE")
	BEGIN_SCALEFORM_MOVIE_METHOD(sSctvQueueLocalData.scfIndex, "SET_TITLE")
	
		//Add the mission type
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === MAINTAIN_SCTV_TICKER_QUEUE - SET_TITLE - Type")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MISSION_TYPE_STRING_FOR_SCTV_TICKER())
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		//Add the mission name
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === MAINTAIN_SCTV_TICKER_QUEUE - SET_TITLE - name")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_FMMC_STRUCT.tl63MissionName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		//Add the mission description/synopsis
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === MAINTAIN_SCTV_TICKER_QUEUE - SET_TITLE - synopsis")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("TMLR0")
			//If it's a mission
			IF SHOULD_DISPLAY_CLOUD_SYNOPSIS()
				IF IS_STRING_NULL(g_FMMC_STRUCT.sFMMCEndConditions[g_sSctvTickerQueue.iSpectatingTeam].tl63SpecSynopsis[0])
					g_FMMC_STRUCT.sFMMCEndConditions[g_sSctvTickerQueue.iSpectatingTeam].tl63SpecSynopsis[0] = ""
				ENDIF
				IF IS_STRING_NULL(g_FMMC_STRUCT.sFMMCEndConditions[g_sSctvTickerQueue.iSpectatingTeam].tl63SpecSynopsis[1])
					g_FMMC_STRUCT.sFMMCEndConditions[g_sSctvTickerQueue.iSpectatingTeam].tl63SpecSynopsis[1] = ""
				ENDIF
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_FMMC_STRUCT.sFMMCEndConditions[g_sSctvTickerQueue.iSpectatingTeam].tl63SpecSynopsis[0])
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_FMMC_STRUCT.sFMMCEndConditions[g_sSctvTickerQueue.iSpectatingTeam].tl63SpecSynopsis[1])						
			ELSE
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(GET_MISSION_SYNOPSIS(g_sSctvTickerQueue.iNumberOfLtsTeams))
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY("")
			ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()	
ENDPROC

//Set up the score card
PROC SET_UP_SCTV_TICKER_SCORE(MAINTAIN_SCTV_TICKER_QUEUE_DATA &sSctvQueueLocalData, BOOL bShow = FALSE)
	INT iLoop
	FOR iLoop = 0 TO (ciSCTV_TICKER_SCORE_MAX - 1)
		//Loop through the two scores and add the data
		BEGIN_SCALEFORM_MOVIE_METHOD(sSctvQueueLocalData.scfIndex, "SET_PLAYER_SCORE")
		//Slot iLoop
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLoop + 1)	//Slots are 1 and 2!
			//If it's not NULL
			IF NOT IS_STRING_NULL_OR_EMPTY(g_sSctvTickerQueue.sScoreData[iLoop].sPlayerName)		
			AND bShow
				//Show
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				//Position
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_NO")
					ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sScoreData[iLoop].iPosition)						
				END_TEXT_COMMAND_SCALEFORM_STRING()
				//PlayerName
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sSctvTickerQueue.sScoreData[iLoop].sPlayerName)						
				END_TEXT_COMMAND_SCALEFORM_STRING()					
				//Score
				//If on a race and not a parachute
				IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
				AND NOT g_b_On_BASEJUMP_RACE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						//If the lap time is blank then show nothing
						IF g_sSctvTickerQueue.sScoreData[iLoop].iScore = 0
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("")	
						ELSE
							ADD_TEXT_COMPONENT_SUBSTRING_TIME(g_sSctvTickerQueue.sScoreData[iLoop].iScore, TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)
						ENDIF
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_NO")
						ADD_TEXT_COMPONENT_INTEGER(g_sSctvTickerQueue.sScoreData[iLoop].iScore)						
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
			//Null name then don't display it and clear it
			ELSE
				//Show
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				//Position
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_NO")
					ADD_TEXT_COMPONENT_INTEGER(0)						
				END_TEXT_COMMAND_SCALEFORM_STRING()
				//PlayerName
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("")						
				END_TEXT_COMMAND_SCALEFORM_STRING()					
				//Score
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_NO")
					ADD_TEXT_COMPONENT_INTEGER(0)						
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
	ENDFOR
ENDPROC

//Set the score, to be called every frame
PROC MAINTAIN_SCTV_TICKER_SCORE(INT iFirstScore, INT iFirstPos, STRING sFirstName, INT iSecondScore = 0, INT iSecondPos = 0 , STRING sSecondName = NULL)
	g_sSctvTickerQueue.sScoreData[0].iPosition		= iFirstPos
	g_sSctvTickerQueue.sScoreData[0].sPlayerName	= sFirstName
	g_sSctvTickerQueue.sScoreData[0].iScore			= iFirstScore
	
	g_sSctvTickerQueue.sScoreData[1].iPosition		= iSecondPos
	g_sSctvTickerQueue.sScoreData[1].sPlayerName	= sSecondName
	g_sSctvTickerQueue.sScoreData[1].iScore			= iSecondScore
ENDPROC

//The score refresh timer
CONST_INT ciSCTV_TICKER_SCORE_REFRESH_TIMER 500
//Returns if we need to refresh the scale form
FUNC BOOL SHOULD_SCTV_TICKER_SCORE_UPDATE(MAINTAIN_SCTV_TICKER_QUEUE_DATA &sSctvQueueLocalData)
	INT iLoop
	BOOL bReturn
	//If the timer has expired
	IF ABSI(GET_TIME_DIFFERENCE(sSctvQueueLocalData.ScoreControlTimer, GET_NETWORK_TIME())) > ciSCTV_TICKER_SCORE_REFRESH_TIMER
	OR sSctvQueueLocalData.ScoreControlTimer = NULL
		//Get the timer
		sSctvQueueLocalData.ScoreControlTimer = GET_NETWORK_TIME()
		//Check if any thing has changed
		FOR iLoop = 0 TO (ciSCTV_TICKER_SCORE_MAX - 1)
			IF sSctvQueueLocalData.sScoreData[iLoop].iPosition != g_sSctvTickerQueue.sScoreData[iLoop].iPosition
				//If we chould update then up date
				sSctvQueueLocalData.sScoreData[iLoop].iPosition = g_sSctvTickerQueue.sScoreData[iLoop].iPosition
				bReturn = TRUE
			ENDIF
			IF sSctvQueueLocalData.sScoreData[iLoop].iScore != g_sSctvTickerQueue.sScoreData[iLoop].iScore
				//If we chould update then up date
				sSctvQueueLocalData.sScoreData[iLoop].iScore = g_sSctvTickerQueue.sScoreData[iLoop].iScore
				bReturn = TRUE
			ENDIF
			//Only check these if there's not been a change
			IF NOT ARE_STRINGS_EQUAL(sSctvQueueLocalData.sScoreData[iLoop].sPlayerName, g_sSctvTickerQueue.sScoreData[iLoop].sPlayerName)
				//If we chould update then up date
				sSctvQueueLocalData.sScoreData[iLoop].sPlayerName = g_sSctvTickerQueue.sScoreData[iLoop].sPlayerName	
				bReturn = TRUE
			ENDIF
		ENDFOR
	ENDIF
	//Return if we should update
	RETURN bReturn
ENDFUNC

//Should we pass an item in the queue to scaleform
FUNC BOOL SHOULD_PASS_TOP_OF_SCTV_TICKER_QUEUE_TO_SCALEFORM()
	RETURN TRUE
ENDFUNC

//Maintain the SCTV Ticker QUEUE
PROC MAINTAIN_SCTV_TICKER_QUEUE(MAINTAIN_SCTV_TICKER_QUEUE_DATA &sSctvQueueLocalData)
	

	//If we should be in here
	IF NOT IS_ROCKSTAR_DEV()
	OR NOT NETWORK_IS_ACTIVITY_SESSION()
	OR NOT IS_PLAYER_SCTV(PLAYER_ID())	
		EXIT
	ENDIF
	
	//Switch the main stage of the scalefor movie
	SWITCH sSctvQueueLocalData.iStage
		//Request that scaleform
		CASE ciMAINTAIN_SCTV_TICKER_QUEUE_INT
			#IF IS_DEBUG_BUILD
			IF g_bForceProcessSctvFeed
				g_sSctvTickerQueue.sScoreData[0].sPlayerName	= "First"
				g_sSctvTickerQueue.sScoreData[1].sPlayerName	= "Second"
			ENDIF
			#ENDIF
			SET_SCTV_TICKER_UI_ON()
			sSctvQueueLocalData.scfIndex = REQUEST_SCALEFORM_MOVIE("MP_SPECTATOR_OVERLAY")
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === MAINTAIN_SCTV_TICKER_QUEUE - sSctvQueueLocalData.scfIndex = REQUEST_SCALEFORM_MOVIE(MP_SPECTATOR_OVERLAY)")
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === ciMAINTAIN_SCTV_TICKER_QUEUE_INT - Done")
			sSctvQueueLocalData.iStage++
		BREAK
		
		//wait for it to load and then set it up
		CASE ciMAINTAIN_SCTV_TICKER_QUEUE_WAIT
			IF HAS_SCALEFORM_MOVIE_LOADED(sSctvQueueLocalData.scfIndex)
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === MAINTAIN_SCTV_TICKER_QUEUE - HAS_SCALEFORM_MOVIE_LOADED")
				//Show the ticker
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === MAINTAIN_SCTV_TICKER_QUEUE - SHOW_TICKER")
				BEGIN_SCALEFORM_MOVIE_METHOD(sSctvQueueLocalData.scfIndex, "SHOW_TICKER")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				END_SCALEFORM_MOVIE_METHOD()
				//Hide the score to begin with
				SET_UP_SCTV_TICKER_SCORE_SHOW_POSITIONS(sSctvQueueLocalData, FALSE)
				//Set the title
				SET_UP_SCTV_TICKER_TITLE(sSctvQueueLocalData)
				//Set up the score
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === MAINTAIN_SCTV_TICKER_QUEUE - SET_PLAYER_SCORE")
				SET_UP_SCTV_TICKER_SCORE(sSctvQueueLocalData)
				//Set the team that we are spectating
				sSctvQueueLocalData.iSpectatingTeam = g_sSctvTickerQueue.iSpectatingTeam	
				//Move 
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === ciMAINTAIN_SCTV_TICKER_QUEUE_WAIT - Done")
				sSctvQueueLocalData.iStage++
			ENDIF
		BREAK
		
		//maintain that scaleform
		CASE ciMAINTAIN_SCTV_TICKER_QUEUE_RUN
			//If we're not on the LeaderBoard
			IF NOT g_b_OnLeaderboard
				//Draw the movie
				DRAW_SCALEFORM_MOVIE_FULLSCREEN(sSctvQueueLocalData.scfIndex, 255, 255, 255, 255)	
			ENDIF
			
			//Hide hud components
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RADIO_STATIONS)
			//maintain the score card If we should show it
			IF SHOULD_SCTV_TICKER_SCORE_DISPLAY()
				//And it's not set up then set it up
				IF sSctvQueueLocalData.bDisplay = FALSE 
					CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === ciMAINTAIN_SCTV_TICKER_QUEUE_RUN  - SET_UP_SCTV_TICKER_SCORE - TRUE")
					SET_UP_SCTV_TICKER_SCORE_SHOW_POSITIONS(sSctvQueueLocalData, TRUE)
					sSctvQueueLocalData.bDisplay = TRUE
				ENDIF
			//Not to be displayed
			ELSE
				//And it is displayed then clean it up
				IF sSctvQueueLocalData.bDisplay
					CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === ciMAINTAIN_SCTV_TICKER_QUEUE_RUN  - SET_UP_SCTV_TICKER_SCORE - FALSE")
					SET_UP_SCTV_TICKER_SCORE_SHOW_POSITIONS(sSctvQueueLocalData, FALSE)
					sSctvQueueLocalData.bDisplay = FALSE
				ENDIF
			ENDIF
			
			//If we should update it because the score/name has changed then update it
			IF SHOULD_SCTV_TICKER_SCORE_UPDATE(sSctvQueueLocalData)
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === ciMAINTAIN_SCTV_TICKER_QUEUE_RUN  - SHOULD_SCTV_TICKER_SCORE_UPDATE")
				SET_UP_SCTV_TICKER_SCORE(sSctvQueueLocalData, SHOULD_SCTV_TICKER_SCORE_DISPLAY())	
			ENDIF
			
			//If we're not spectating the same team then we'll need to reset up the mission synopsis
			IF sSctvQueueLocalData.iSpectatingTeam != g_sSctvTickerQueue.iSpectatingTeam
				//Only if we should dispplay the 
				IF SHOULD_DISPLAY_CLOUD_SYNOPSIS()
					SET_UP_SCTV_TICKER_TITLE(sSctvQueueLocalData)
				ENDIF
				//Set the team that we are spectating
				sSctvQueueLocalData.iSpectatingTeam = g_sSctvTickerQueue.iSpectatingTeam	
			ENDIF
			
			//If there's an item in the queue
			IF g_sSctvTickerQueue.iLength > 0
				//If we're ready to pass an item to the queue
				IF SHOULD_PASS_TOP_OF_SCTV_TICKER_QUEUE_TO_SCALEFORM()
					//Build the string to pass to scaleform
					BUILD_SCTV_TICKER_FROM_QUEUE(sSctvQueueLocalData)
					//Remove the item at the top of the queue
					REMOVE_ITEM_AT_TOP_OF_SCTV_TICKER_QUEUE()
					//Reduce the lenfth of the queue
					g_sSctvTickerQueue.iLength--
					CPRINTLN(DEBUG_SPECTATOR, "=== SCTVQ === MAINTAIN_SCTV_TICKER_QUEUE - g_sSctvTickerQueue.iLength = ", g_sSctvTickerQueue.iLength)
					//Cap the length from being less than 0
					IF g_sSctvTickerQueue.iLength < 0
						g_sSctvTickerQueue.iLength = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH 
ENDPROC



















