USING "rage_builtins.sch"
USING "Transition_Saving.sch"


ENUM RACE_CREATOR_TUTORIAL
	RACE_CREATOR_TUTORIAL_STAGE_INIT,
	RACE_CREATOR_TUTORIAL_STAGE_FADE,
	RACE_CREATOR_TUTORIAL_STAGE_INTRO,
	RACE_CREATOR_TUTORIAL_STAGE_MENU_INTRO,
	RACE_CREATOR_TUTORIAL_STAGE_MENU_INTRO2,
	RACE_CREATOR_TUTORIAL_STAGE_MENU_SELECT_DETAILS,
	RACE_CREATOR_TUTORIAL_STAGE_MENU_SET_DETAILS,
	RACE_CREATOR_TUTORIAL_STAGE_MENU_SET_DETAILS_FINISH,
	RACE_CREATOR_TUTORIAL_STAGE_FIRST_MENU,
	RACE_CREATOR_TUTORIAL_STAGE_WARP_EXP,
	RACE_CREATOR_TUTORIAL_STAGE_START_INTERP,
	RACE_CREATOR_TUTORIAL_STAGE_SELECT_TRIGGER,
	RACE_CREATOR_TUTORIAL_STAGE_MORE_TRIGGER_HELP,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_START,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_INST,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_FINISH,
	RACE_CREATOR_TUTORIAL_STAGE_LOBBY_CAM_HELP,
	RACE_CREATOR_TUTORIAL_STAGE_LOBBY_CAM_PLACE,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_GRID_INTRO,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_GRID_HELP,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_GRID,
	RACE_CREATOR_TUTORIAL_STAGE_CP_INTRO1,
	RACE_CREATOR_TUTORIAL_STAGE_CP_INTRO2,
	RACE_CREATOR_TUTORIAL_STAGE_CP_INTRO3,
	RACE_CREATOR_TUTORIAL_STAGE_CP_INTRO4,
//	RACE_CREATOR_TUTORIAL_STAGE_CP_INTRO5,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_CP,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_CAMERA_SWITCH_INTRO,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_CAMERA_SWITCH_HELP,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_CAMERA_SWITCH,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_CAMERA_SWITCH_HELP2,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_CP_ON_GROUND,
	RACE_CREATOR_TUTORIAL_STAGE_WAIT_FOR_EDIT_HELP,
	RACE_CREATOR_TUTORIAL_STAGE_WAIT_FOR_INSERT_HELP,
	RACE_CREATOR_TUTORIAL_STAGE_FINISH_CPS_INTRO,
	RACE_CREATOR_TUTORIAL_STAGE_FINISH_CPS_INTRO2,
	RACE_CREATOR_TUTORIAL_STAGE_FINISH_CPS,
	RACE_CREATOR_TUTORIAL_STAGE_TEST_INTRO,
	RACE_CREATOR_TUTORIAL_STAGE_SELECT_TEST,
	RACE_CREATOR_TUTORIAL_STAGE_TEST_HELP1,
	RACE_CREATOR_TUTORIAL_STAGE_TEST_HELP2,
	RACE_CREATOR_TUTORIAL_STAGE_TEST_HELP3,
	RACE_CREATOR_TUTORIAL_STAGE_TEST,
	RACE_CREATOR_TUTORIAL_STAGE_FAIL_TEST,
	RACE_CREATOR_TUTORIAL_STAGE_FAIL_TEST_HELP,
	RACE_CREATOR_TUTORIAL_STAGE_FINAL_INTERP_CAM,
	RACE_CREATOR_TUTORIAL_STAGE_END_FADE_IN,
	RACE_CREATOR_TUTORIAL_STAGE_END1,
	RACE_CREATOR_TUTORIAL_STAGE_END2,
	RACE_CREATOR_TUTORIAL_STAGE_END3,
	RACE_CREATOR_TUTORIAL_STAGE_END4,
	RACE_CREATOR_TUTORIAL_STAGE_END5,
	RACE_CREATOR_TUTORIAL_STAGE_END6,
	RACE_CREATOR_TUTORIAL_STAGE_FINISH,
	RACE_CREATOR_TUTORIAL_STAGE_DONE
	
/*	RACE_CREATOR_TUTORIAL_STAGE_CHECKPOINT_INTRO,
	
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_MOVE,
	
	RACE_CREATOR_TUTORIAL_STAGE_SELECT_LAUNCH_LOC,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_1STCP_INTERP,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_1STCP_START,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_1STCP_PLACED,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_2NDCP_START,
	RACE_CREATOR_TUTORIAL_STAGE_MAP_HELP_START,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_2NDCP_MOVE_START,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_2NDCP_MOVE_INST,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_2NDCP_MOVE_SELECT,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_2NDCP_MOVE_NEWPOS,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_3RDCP_START,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_3RDCP_PLACED,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_4THCP_START,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_4THCP_PLACED,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_5THCP_START,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_5THCP_PLACED,
	RACE_CREATOR_TUTORIAL_STAGE_CHEVRON_HELP_START,
	RACE_CREATOR_TUTORIAL_STAGE_CHEVRON_HELP,
	RACE_CREATOR_TUTORIAL_STAGE_RETURN_TO_MAIN,
	RACE_CREATOR_TUTORIAL_STAGE_SWITCH_CAM_START,
	RACE_CREATOR_TUTORIAL_STAGE_SWITCH_CAM_CHECK,
	RACE_CREATOR_TUTORIAL_STAGE_IN_CAR_INTRO,
	RACE_CREATOR_TUTORIAL_STAGE_IN_CAR_START,
	RACE_CREATOR_TUTORIAL_STAGE_IN_CAR_PLACE_5THCP,
	RACE_CREATOR_TUTORIAL_STAGE_IN_CAR_PLACE_6THCP_START,
	RACE_CREATOR_TUTORIAL_STAGE_RETURN_TO_CAM,
	RACE_CREATOR_TUTORIAL_STAGE_RETURN_TO_TOP,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_6THCP_START,
	RACE_CREATOR_TUTORIAL_STAGE_PLACE_6THCP_PLACED,
	RACE_CREATOR_TUTORIAL_STAGE_CYCLECP_START,
	RACE_CREATOR_TUTORIAL_STAGE_CYCLECP,
	RACE_CREATOR_TUTORIAL_STAGE_TEST_RACE_START,
	RACE_CREATOR_TUTORIAL_STAGE_EXIT_CP_MENU,
	RACE_CREATOR_TUTORIAL_STAGE_TEST_RACE_MENU,
	RACE_CREATOR_TUTORIAL_STAGE_TEST_RACE_SUB_MENU,
	RACE_CREATOR_TUTORIAL_STAGE_TEST_RACE,
	RACE_CREATOR_TUTORIAL_STAGE_END_RACE,
	RACE_CREATOR_TUTORIAL_STAGE_FINAL_HELP_START,
	RACE_CREATOR_TUTORIAL_STAGE_FINAL_HELP,
	RACE_CREATOR_TUTORIAL_STAGE_FINISH,
	RACE_CREATOR_TUTORIAL_STAGE_DONE*/
ENDENUM

//CONST_INT biRT_DisableSwitchCamHelpKey			0
//CONST_INT biRT_DisableContextHelp				1
//CONST_INT biRT_DoMainSwitch						2
//CONST_INT biRT_PlacedTriggerAtLeastOnce			3
//CONST_INT biRT_PlacedStartGridAtLeastOnce		4
//CONST_INT biRT_DisableMenuForTutorial			5
//CONST_INT biRT_StartingGridTip					6
//CONST_INT biRT_CheckpointTip					7
//CONST_INT biRT_EditCheckpointHelp				8
//CONST_INT biRT_CheckpointIntro					9
//CONST_INT biRT_IntroEditCheckpoint				10
//CONST_INT biRT_RadarHelp						11
//CONST_INT biRT_SwitchHelp						12
//CONST_INT biRT_DoneSwitchToInCar				13
//CONST_INT biRT_ReturnedToTopDown				14
//CONST_INT biRT_DOneTestRaceIntro				15
//CONST_INT biRT_DoneTestAtLeastOnce				16
//CONST_INT biRt_DoneEndTutorialHelp				17
//CONST_INT biRT_DoneCamControlHelp				18
CONST_INT biRT_PLayerWantsToDoTurtorial			19

STRUCT structRaceCreatorTutorial
	RACE_CREATOR_TUTORIAL tutorialStage
	VECTOR vTutCheckpointHelperPos[8]
	VECTOR vTutStartHelperPos
	VECTOR vTutStartHelperSize
	VECTOR vTutGridHelperPos
	VECTOR vTutGridHelperSize
	BOOL bStartedTutInterp
	BOOL bDisabled
	BOOL bTestWithAi
	INT iTutTimer
	INT iResetCameraProg
	INT iRaceTutorialBitset
	INT iCheckpointsBeforeSwap
	#IF IS_DEBUG_BUILD
	BOOL bWdTutRunWidget
	BOOL bWdTutForceTest
	BOOL bWdClearTutComplete
	BOOL bDoTutDebug
	INT iWdTutProg	
	VECTOR vWdTutCoronaPos
	FLOAT fWdTutCoronaSize
	VECTOR vWdTutCoronaRot
	VECTOR vWdTutCoronaDir
	INT iWdCorona
	BOOL bWdTestCorona
	BOOL bWdTestAddRemove
	BLIP_INDEX blipWd
	FLOAT fWdhelpX
	FLOAT fWdHelpY
	BOOL bWdPlaceFloatingHelp
	INT iWdFloatingDir
	TEXT_WIDGET_ID tWdFloatHelp
	#ENDIF
ENDSTRUCT

ENUM DM_CREATOR_TUTORIAL
	DM_CREATOR_TUTORIAL_STAGE_INIT,
	DM_CREATOR_TUTORIAL_STAGE_FADE_IN,
	DM_CREATOR_TUTORIAL_STAGE_INTRO,
	DM_CREATOR_TUTORIAL_STAGE_INTRO_HELP,
	DM_CREATOR_TUTORIAL_STAGE_INTRO_HELP2,
	DM_CREATOR_TUTORIAL_STAGE_DETAILS_HELP,
	DM_CREATOR_TUTORIAL_STAGE_SELECT_DETAILS,
	DM_CREATOR_TUTORIAL_STAGE_EDIT_DETAILS,
	DM_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_HELP,
	DM_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_INST,
	DM_CREATOR_TUTORIAL_STAGE_WARP_EXP,
	DM_CREATOR_TUTORIAL_STAGE_SELECT_TRIGGER,
	DM_CREATOR_TUTORIAL_STAGE_MORE_TRIGGER_HELP,
	DM_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_MOVE,
	DM_CREATOR_TUTORIAL_STAGE_LOBBY_CAM_HELP,
	DM_CREATOR_TUTORIAL_STAGE_LOBBY_CAM_PLACE,
	DM_CREATOR_TUTORIAL_STAGE_1ST_SPAWN_INST,
	DM_CREATOR_TUTORIAL_STAGE_SELECT_SPAWNS,
	DM_CREATOR_TUTORIAL_STAGE_1ST_SPAWN_INST2,
	DM_CREATOR_TUTORIAL_STAGE_1ST_SPAWN_INST3,
	DM_CREATOR_TUTORIAL_STAGE_PLACE_ALL_SPAWNS,
	DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_TEAM_SPAWNS1,
	DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_TEAM_SPAWNS2,
	DM_CREATOR_TUTORIAL_STAGE_PLACE_TEAM_SPAWNS,
	DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_WEAPONS,
	DM_CREATOR_TUTORIAL_STAGE_PLACE_ALL_WEAPONS,
	DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_CAM1,
	DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_CAM2,
	DM_CREATOR_TUTORIAL_STAGE_SWITCH_CAM,
	DM_CREATOR_TUTORIAL_STAGE_SWITCH_CAM2,
	DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_ONFOOT_PLACEMENT,
	DM_CREATOR_TUTORIAL_STAGE_PLACE_IN_STORE,
	DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_DM_ELEMENTS,
	DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_RANDOMIZER,
	DM_CREATOR_TUTORIAL_STAGE_FINAL_WARN_SPAWNS,
	DM_CREATOR_TUTORIAL_STAGE_FINAL_CHECK_SPAWNS,
	DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_TEST,
	DM_CREATOR_TUTORIAL_STAGE_TEST_DM,
	DM_CREATOR_TUTORIAL_STAGE_TEST_DM_HELP1,
	DM_CREATOR_TUTORIAL_STAGE_TEST_DM_HELP2,
	DM_CREATOR_TUTORIAL_STAGE_TEST_DM_HELP3,
	DM_CREATOR_TUTORIAL_STAGE_WAITING_TO_FINISH_TEST,
	DM_CREATOR_TUTORIAL_STAGE_FAILED_TEST,
	DM_CREATOR_TUTORIAL_STAGE_FAILED_TEST_HELP,
	DM_CREATOR_TUTORIAL_STAGE_CREATE_FINAL_INTERP_CAM,
	DM_CREATOR_TUTORIAL_STAGE_FADE_AFTER_TEST,
	DM_CREATOR_TUTORIAL_STAGE_GOODBYE1,
	DM_CREATOR_TUTORIAL_STAGE_GOODBYE2,
	DM_CREATOR_TUTORIAL_STAGE_GOODBYE3,
	DM_CREATOR_TUTORIAL_STAGE_GOODBYE4,
	DM_CREATOR_TUTORIAL_STAGE_GOODBYE5,
	DM_CREATOR_TUTORIAL_STAGE_GOODBYE6,
	DM_CREATOR_TUTORIAL_STAGE_FINISH,
	DM_CREATOR_TUTORIAL_STAGE_DONE
ENDENUM

STRUCT structDMCreatorTutorial
	DM_CREATOR_TUTORIAL tutorialStage
	DM_CREATOR_TUTORIAL revertToStage
	BOOL bStartedTutInterp
	BOOL bDisabled
	INT iTutTimer
	
	#IF IS_DEBUG_BUILD
	BOOL bWdTutRunWidget
	BOOL bWdTutForceTest
	INT iWdTutProg	
	VECTOR vWdTutCoronaPos
	VECTOR vWdTutCoronaSize
	VECTOR vWdTutCoronaRot
	VECTOR vWdTutCoronaDir
	INT iWdCorona
	BOOL bWdTestCorona
	BOOL bWdTestAddRemove
	BLIP_INDEX blipWd
	#ENDIF
ENDSTRUCT

ENUM MISSION_CREATOR_TUTORIAL
	MISSION_CREATOR_TUTORIAL_STAGE_INIT,
	MISSION_CREATOR_TUTORIAL_STAGE_INTRO,
	MISSION_CREATOR_TUTORIAL_STAGE_EXPLAIN_PLAYER_NUMS,
	MISSION_CREATOR_TUTORIAL_STAGE_EDIT_PLAYER_NUMS_INST,
	MISSION_CREATOR_TUTORIAL_STAGE_EDIT_PLAYER_NUMS,
	MISSION_CREATOR_TUTORIAL_STAGE_EDIT_PLAYER_NUMS1,
	MISSION_CREATOR_TUTORIAL_STAGE_EDIT_PLAYER_NUMS2,
	MISSION_CREATOR_TUTORIAL_STAGE_CAM_CONTROL,
	MISSION_CREATOR_TUTORIAL_STAGE_CAM_CONTROL1,
	MISSION_CREATOR_TUTORIAL_STAGE_EXPLAIN_MISSION_RULES,
	MISSION_CREATOR_TUTORIAL_STAGE_EXPLAIN_OPTIONS1,
	MISSION_CREATOR_TUTORIAL_STAGE_EXPLAIN_TRIGGER,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_START,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_MOVE,
	MISSION_CREATOR_TUTORIAL_STAGE_GOTO_LOC,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_GOTO_LOC,
	MISSION_CREATOR_TUTORIAL_STAGE_ADDED_TO_RULES,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_VEH,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_VEH1,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_VEH2,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_VEH3,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_PICKUP,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_PICKUP1,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_PICKUP2,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_DROPOFF,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_DROPOFF1,
	MISSION_CREATOR_TUTORIAL_STAGE_EXPLAIN_ENEMIES,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_ENEMIES,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_ENEMIES1,
	MISSION_CREATOR_TUTORIAL_STAGE_PLACE_ENEMIES2,
	MISSION_CREATOR_TUTORIAL_STAGE_REORDER,
	MISSION_CREATOR_TUTORIAL_STAGE_REORDER1,
	MISSION_CREATOR_TUTORIAL_STAGE_REORDER2,
	MISSION_CREATOR_TUTORIAL_STAGE_TIME_LIMIT,
	MISSION_CREATOR_TUTORIAL_STAGE_TIME_LIMIT1,
	MISSION_CREATOR_TUTORIAL_STAGE_TIME_LIMIT2,
	MISSION_CREATOR_TUTORIAL_STAGE_TEST,
	MISSION_CREATOR_TUTORIAL_STAGE_TEST1,
	MISSION_CREATOR_TUTORIAL_STAGE_COMPLETE,
	MISSION_CREATOR_TUTORIAL_STAGE_COMPLETE1,
	MISSION_CREATOR_TUTORIAL_STAGE_HELP,
	MISSION_CREATOR_TUTORIAL_STAGE_FINISH,
	MISSION_CREATOR_TUTORIAL_STAGE_DONE
ENDENUM


STRUCT structMissionCreatorTutorial
	MISSION_CREATOR_TUTORIAL tutorialStage
	MISSION_CREATOR_TUTORIAL revertToStage
	VECTOR vTutSpawnpointHelperPos[12]
	FLOAT fTutSpawnpointHelperHead[12]
	BOOL bTutSpawnpointHelper[12]
	BLIP_INDEX blipSpawnpointHelper[12]
	VECTOR vTutStartHelperPos
	BLIP_INDEX blipStartHelper
	VECTOR vTutStartHelperSize
	BOOL bStartedTutInterp
	BOOL bDisabled
	INT iTutTimer
	SCRIPT_TIMER  tdTutTimer
	VECTOR vTutWepHelperPos[7]
	BLIP_INDEX blipWepHelper[7]
	BOOL bTutWepHelper[7]
	
	#IF IS_DEBUG_BUILD
	BOOL bWdTutRunWidget
	BOOL bWdTutForceTest
	INT iWdTutProg	
	VECTOR vWdTutCoronaPos
	VECTOR vWdTutCoronaSize
	VECTOR vWdTutCoronaRot
	VECTOR vWdTutCoronaDir
	INT iWdCorona
	BOOL bWdTestCorona
	BOOL bWdTestAddRemove
	BLIP_INDEX blipWd
	#ENDIF
ENDSTRUCT

#IF IS_DEBUG_BUILD
	PROC PRINT_TUT_STRING(STRING sText)
		PRINTNL()
		PRINTSTRING(" [dsw] [Tutorial] ")
		PRINTSTRING(sText)
		PRINTNL()
	ENDPROC
	
	PROC PRINT_TUT_STRING_INT(STRING sText, INT iToPrint)
		PRINTNL()
		PRINTSTRING(" [dsw] [Tutorial] ")
		PRINTSTRING(sText)
		PRINTINT(iToPrint)
		PRINTNL()
	ENDPROC
	
	PROC PRINT_TUT_STRING_FLOAT(STRING sText, FLOAT fToPrint)
		PRINTNL()
		PRINTSTRING(" [dsw] [Tutorial] ")
		PRINTSTRING(sText)
		PRINTFLOAT(fToPrint)
		PRINTNL()
	ENDPROC
	
	
	
#ENDIF

FUNC BOOL HAS_RACE_CREATOR_TUTORIAL_COMPLETED(structRaceCreatorTutorial tutorial)
	//RETURN GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT_INDEX)
	IF tutorial.bDisabled
		#IF IS_DEBUG_BUILD
			IF tutorial.bDoTutDebug
				PRINT_TUT_STRING("HAS_RACE_CREATOR_TUTORIAL_COMPLETED True because tutorial.bDisabled")
			ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(tutorial.iRaceTutorialBitset, biRT_PLayerWantsToDoTurtorial)
		#IF IS_DEBUG_BUILD
			IF tutorial.bDoTutDebug
				PRINT_TUT_STRING("HAS_RACE_CREATOR_TUTORIAL_COMPLETED False because biRT_PLayerWantsToDoTurtorial")
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ELSE
		#IF IS_DEBUG_BUILD
			IF tutorial.bDoTutDebug
				PRINT_TUT_STRING("HAS_RACE_CREATOR_TUTORIAL_COMPLETED TRUE because biRT_PLayerWantsToDoTurtorial is not set ")
			ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

//	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main")) = 0
//	
//		#IF IS_DEBUG_BUILD
//			IF tutorial.bDoTutDebug
//				BOOL bStat = GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT)
//				IF bStat
//					PRINT_TUT_STRING("HAS_RACE_CREATOR_TUTORIAL_COMPLETED true because MPPLY_DONE_RACE_CREATOR_TUT")
//				ELSE
//					PRINT_TUT_STRING("HAS_RACE_CREATOR_TUTORIAL_COMPLETED False because MPPLY_DONE_RACE_CREATOR_TUT")
//				ENDIF
//			ENDIF
//		#ENDIF
//		
//		RETURN GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT)
//	ELSE
//	
//		#IF IS_DEBUG_BUILD
//			IF tutorial.bDoTutDebug
//				
//				IF tutorial.tutorialStage = RACE_CREATOR_TUTORIAL_STAGE_DONE 
//					PRINT_TUT_STRING("HAS_RACE_CREATOR_TUTORIAL_COMPLETED true because tutorial.tutorialStage = RACE_CREATOR_TUTORIAL_STAGE_DONE ")
//				ELSE
//					PRINT_TUT_STRING("HAS_RACE_CREATOR_TUTORIAL_COMPLETED False because tutorial.tutorialStage <> RACE_CREATOR_TUTORIAL_STAGE_DONE ")
//				ENDIF
//				
//			ENDIF
//		#ENDIF
//		
//		RETURN tutorial.tutorialStage = RACE_CREATOR_TUTORIAL_STAGE_DONE 
//	ENDIF 
ENDFUNC

PROC SET_RACE_CREATOR_TUTORIAL_COMPLETE(structRaceCreatorTutorial &tutorial)
	#IF IS_DEBUG_BUILD
		PRINT_TUT_STRING("SET_RACE_CREATOR_TUTORIAL_COMPLETE() called.")
	#ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main")) = 0
		#IF IS_DEBUG_BUILD
			PRINT_TUT_STRING("SET_RACE_CREATOR_TUTORIAL_COMPLETE() main.sc not running")
		#ENDIF
		SET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT, TRUE)
		REQUEST_SAVE(SSR_REASON_TUTORIAL_COMPLETE)
	ENDIF
	
	tutorial.tutorialStage = RACE_CREATOR_TUTORIAL_STAGE_DONE 
	
ENDPROC

PROC CLEAR_RACE_CREATOR_TUTORIAL_COMPLETE()
	#IF IS_DEBUG_BUILD
		PRINT_TUT_STRING("CLEAR_RACE_CREATOR_TUTORIAL_COMPLETE() called.")
	#ENDIF
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main")) = 0
		
		SET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT, FALSE)
		REQUEST_SAVE(SSR_REASON_TUTORIAL_COMPLETE)
	ENDIF
ENDPROC

PROC DISABLE_FLYCAM_CONTROLS_FOR_TUTORIAL()

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPTED_FLY_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPTED_FLY_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPTED_FLY_ZUP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPTED_FLY_ZDOWN)


ENDPROC

PROC INTERP_TUTORIAL_CAM(FMMC_CAM_DATA &sCamDataPassed, VECTOR vPos, VECTOR vRot, FLOAT fFov = 45.0, INT iTimeToTake = 1000)
	IF DOES_CAM_EXIST(sCamDataPassed.cam)
		IF NOT DOES_CAM_EXIST(sCamDataPassed.inTerp)
			sCamDataPassed.inTerp = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
		ENDIF

		SET_CAM_ACTIVE(sCamDataPassed.inTerp, TRUE)
		SET_CAM_PARAMS(sCamDataPassed.inTerp, GET_CAM_COORD(sCamDataPassed.cam), GET_CAM_ROT(sCamDataPassed.cam), GET_CAM_FOV(sCamDataPassed.cam)) 
		SET_CAM_PARAMS(sCamDataPassed.inTerp,vPos, vRot,fFov, iTimeToTake) 

	ENDIF
ENDPROC



#IF IS_DEBUG_BUILD
	PROC CREATE_RACE_TUTORIAL_WIDGET(WIDGET_GROUP_ID widgetID, structRaceCreatorTutorial &tutRace)
		tutRace.fWdTutCoronaSize = 1.0
		tutRace.vWdTutCoronaPos = <<302.097, -1808.990, 26.368 >> 
		tutRace.iWdFloatingDir = 2
		SET_CURRENT_WIDGET_GROUP(widgetID)
			START_WIDGET_GROUP("Tutorial")
				ADD_WIDGET_BOOL("Display extra debug", tutRace.bDoTutDebug)
				ADD_WIDGET_BOOL("Run widget", tutRace.bWdTutRunWidget)
				ADD_WIDGET_INT_READ_ONLY("Stage",tutRace.iWdTutProg)
				ADD_WIDGET_BOOL("Disabled?", tutRace.bDisabled)
				ADD_WIDGET_BOOL("Force test", tutRace.bWdTutForceTest)
				ADD_WIDGET_BOOL("Test add remove", tutRace.bWdTestAddRemove)
				ADD_WIDGET_BOOL("Clear complete stat", tutRace.bWdClearTutComplete)
				ADD_BIT_FIELD_WIDGET("iRaceTutorialBitset", tutRace.iRaceTutorialBitset)
				START_WIDGET_GROUP("Tutorial helper corona")
					ADD_WIDGET_VECTOR_SLIDER("Corona pos", tutRace.vWdTutCoronaPos, -10000.0, 10000.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Corona Size", tutRace.fWdTutCoronaSize, 1.0, 20.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Corona Rot", tutRace.vWdTutCoronaRot, -360.0, 360.0, 1.0)
					ADD_WIDGET_VECTOR_SLIDER("Corona Dir", tutRace.vWdTutCoronaDir, -360.0, 360.0, 1.0)
					ADD_WIDGET_INT_SLIDER("Corona type", tutRace.iWdCorona,0,22,1 )
					ADD_WIDGET_BOOL("Test Corona", tutRace.bWdTestCorona)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Floating help")
					tutRace.tWdFloatHelp = ADD_TEXT_WIDGET("Label")
					ADD_WIDGET_FLOAT_SLIDER("Float x ", tutRace.fWdhelpX, 0.0, 1.0, 0.05)
					ADD_WIDGET_FLOAT_SLIDER("Float y ", tutRace.fWdHelpY, 0.0, 1.0, 0.05)
					ADD_WIDGET_INT_SLIDER("Dir (1 = north)", tutRace.iWdFloatingDir, 1, 4, 1)
					ADD_WIDGET_BOOL("Floating help", tutRace.bWdPlaceFloatingHelp)
					
					
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(widgetID)
		SET_CONTENTS_OF_TEXT_WIDGET(tutRace.tWdFloatHelp, "FMMC_RC_T19")
		 tutRace.fWdhelpX = 0.255
		 tutRace.fWdhelpy = 0.28
		 
	ENDPROC
	
	PROC UPDATE_RACE_TUTORIAL_WIDGET(structRaceCreatorTutorial &tutRace)
		IF tutRace.bWdTutRunWidget
			tutRace.iWdTutProg = ENUM_TO_INT(tutRace.tutorialStage)
			
//			IF tutRace.bWdTutForceTest
//				 tutRace.bWdTutForceTest = FALSE
//				 tutRace.tutorialStage = RACE_CREATOR_TUTORIAL_STAGE_TEST_RACE_START
//			ENDIF
			
			IF tutRace.bWdTestCorona
				DRAW_MARKER(INT_TO_ENUM(MARKER_TYPE, tutRace.iWdCorona), tutRace.vWdTutCoronaPos, tutRace.vWdTutCoronaDir, tutRace.vWdTutCoronaRot ,<<tutRace.fWdTutCoronaSize, tutRace.fWdTutCoronaSize, tutRace.fWdTutCoronaSize>> ,255, 0, 0,180)
			ENDIF
			
			IF tutRace.bWdTestAddRemove
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				tutRace.blipWd = ADD_BLIP_FOR_COORD(<<50.0, 50.0, 50.0>>)
				REMOVE_BLIP(tutRace.blipWd)
				
			ENDIF
			
			IF tutRace.bWdClearTutComplete
				CLEAR_RACE_CREATOR_TUTORIAL_COMPLETE()
				tutRace.bWdClearTutComplete = FALSE
			ENDIF
			
			IF tutRace.bWdPlaceFloatingHelp
				tutRace.tutorialStage = RACE_CREATOR_TUTORIAL_STAGE_TEST
				tutRace.bWdPlaceFloatingHelp = FALSE
				HELP_AT_SCREEN_LOCATION(GET_CONTENTS_OF_TEXT_WIDGET(tutRace.tWdFloatHelp), tutRace.fWdhelpX, tutRace.fWdhelpy, INT_TO_ENUM(eARROW_DIRECTION, tutRace.iWdFloatingDir), -1, FLOATING_HELP_AMBIENT_SLOT_1)
			ENDIF
		ENDIF
		
	ENDPROC
	
	PROC CREATE_DM_TUTORIAL_WIDGET(WIDGET_GROUP_ID widgetID, structDMCreatorTutorial &tutDM)
		tutDM.vWdTutCoronaSize = << 1.0, 1.0, 1.0 >>
		SET_CURRENT_WIDGET_GROUP(widgetID)
			START_WIDGET_GROUP("Tutorial")
				ADD_WIDGET_BOOL("Run widget", tutDM.bWdTutRunWidget)
				ADD_WIDGET_INT_READ_ONLY("Stage",tutDM.iWdTutProg)
				ADD_WIDGET_BOOL("Disabled?", tutDM.bDisabled)
				ADD_WIDGET_BOOL("Force test", tutDM.bWdTutForceTest)
				ADD_WIDGET_BOOL("Test add remove", tutDM.bWdTestAddRemove)
				START_WIDGET_GROUP("Tutorial helper corona")
					ADD_WIDGET_VECTOR_SLIDER("Corona pos", tutDM.vWdTutCoronaPos, -10000.0, 10000.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Corona size", tutDM.vWdTutCoronaSize, 1.0, 100.0, 0.25)
					ADD_WIDGET_VECTOR_SLIDER("Corona Rot", tutDM.vWdTutCoronaRot, -360.0, 360.0, 1.0)
					ADD_WIDGET_VECTOR_SLIDER("Corona Dir", tutDM.vWdTutCoronaDir, -360.0, 360.0, 1.0)
					ADD_WIDGET_INT_SLIDER("Corona type", tutDM.iWdCorona,0,22,1 )
					ADD_WIDGET_BOOL("Test Corona", tutDM.bWdTestCorona)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(widgetID)
	ENDPROC
	
	PROC UPDATE_DM_TUTORIAL_WIDGET(structDMCreatorTutorial &tutDM)
		IF tutDM.bWdTutRunWidget
			tutDM.iWdTutProg = ENUM_TO_INT(tutDM.tutorialStage)
			
			IF tutDM.bWdTutForceTest
				 tutDM.bWdTutForceTest = FALSE
//				 tutDM.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_TEST_RACE_START
			ENDIF
			
			IF tutDM.bWdTestCorona
				DRAW_MARKER(INT_TO_ENUM(MARKER_TYPE, tutDM.iWdCorona), tutDM.vWdTutCoronaPos, tutDM.vWdTutCoronaDir, tutDM.vWdTutCoronaRot ,tutDM.vWdTutCoronaSize,255, 0, 0,180)
			ENDIF
			
			IF tutDM.bWdTestAddRemove
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				tutDM.blipWd = ADD_BLIP_FOR_COORD(<<50.0, 50.0, 50.0>>)
				REMOVE_BLIP(tutDM.blipWd)
				
			ENDIF
		ENDIF
		
	ENDPROC
#ENDIF 

FUNC BOOL HAS_DM_CREATOR_TUTORIAL_COMPLETED(structDMCreatorTutorial tutorial)
	//RETURN GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT)
	
	IF tutorial.bDisabled
		RETURN TRUE
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main")) = 0
		RETURN g_bFMMC_TutorialSelectedFromMpSkyMenu = FALSE
	ELSE
		RETURN tutorial.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_DONE 
	ENDIF
	
ENDFUNC

PROC SET_DM_CREATOR_TUTORIAL_COMPLETE(structDMCreatorTutorial &tutorial)
	#IF IS_DEBUG_BUILD
		PRINT_TUT_STRING("SET_DM_CREATOR_TUTORIAL_COMPLETE() called.")
	#ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main")) = 0
		#IF IS_DEBUG_BUILD
			PRINT_TUT_STRING("SET_DM_CREATOR_TUTORIAL_COMPLETE() main.sc not running")
		#ENDIF
		tutorial.bDisabled = TRUE
		SET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_DM_CREATOR_TUT, TRUE)
		REQUEST_SAVE(SSR_REASON_TUTORIAL_COMPLETE)
	ENDIF
	
	tutorial.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_DONE 
	
ENDPROC

PROC CLEAR_DM_CREATOR_TUTORIAL_COMPLETE()
	#IF IS_DEBUG_BUILD
		PRINT_TUT_STRING("CLEAR_DM_CREATOR_TUTORIAL_COMPLETE() called.")
	#ENDIF
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main")) = 0
		
		SET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_DM_CREATOR_TUT, FALSE)
		REQUEST_SAVE(SSR_REASON_TUTORIAL_COMPLETE)
	ENDIF
ENDPROC


