// x:\gta5\script\dev_ng\multiplayer\include\public\freemode\net_gang_boss_drops.sch, Christopher Speirs, October 2015
// Boss Drops - Bullshark, Ammo, Armour, Snacks
// PI menu sends event GB_BROADCAST_BOSS_DROP, clients process with GB_PROCESS_BOSS_DROP_EVENT

USING "globals.sch"
USING "net_bru_box.sch"
USING "net_spawn.sch"


// g_sBossDropOverall.iDropProgress
CONST_INT ciGB_START 	0
CONST_INT ciGB_HOLD		1
CONST_INT ciGB_CLEANUP	2

#IF IS_DEBUG_BUILD

PROC GB_PRINT_BOSS_DROP(eGB_BOSS_DROPS eBossDrop)

	SWITCH eBossDrop
		CASE eGB_BOSS_DROPS_NONE
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_PRINT_BOSS_DROP, eGB_BOSS_DROPS_NONE ")
		BREAK
		CASE eGB_BOSS_DROPS_BULLSHARK
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_PRINT_BOSS_DROP, eGB_BOSS_DROPS_BULLSHARK ")
		BREAK
		CASE eGB_BOSS_DROPS_AMMO
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_PRINT_BOSS_DROP, eGB_BOSS_DROPS_AMMO ")
		BREAK
		CASE eGB_BOSS_DROPS_ARMOUR
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_PRINT_BOSS_DROP, eGB_BOSS_DROPS_ARMOUR ")
		BREAK
		CASE eGB_BOSS_DROPS_MOLOTOV
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_PRINT_BOSS_DROP, eGB_BOSS_DROPS_MOLOTOV ")
		BREAK
		CASE eGB_BOSS_DROPS_SNACKS
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_PRINT_BOSS_DROP, eGB_BOSS_DROPS_SNACKS ")
		BREAK
	ENDSWITCH
ENDPROC

#ENDIF // #IF IS_DEBUG_BUILD

FUNC INT GB_FIND_BOSS_DROP_SLOT()
	
	INT i
	
	REPEAT ciGB_TOTAL_MAX_DROPS_ON_GROUND i
		IF g_sBossDrop[i].eBossDropChosen = eGB_BOSS_DROPS_NONE
	
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN ciGB_NO_BOSS_DROPS_LEFT
ENDFUNC 

PROC GB_SET_ACTIVE_BOSS_DROP(eGB_BOSS_DROPS eBossDrop, PLAYER_INDEX PlayerIdFor, INT iSubType, INT iAmount, VECTOR vPos, NETWORK_INDEX niPickup = NULL, INT iSlot = -1)
	
	IF iSlot = -1
		iSlot = GB_FIND_BOSS_DROP_SLOT()
		CPRINTLN(DEBUG_AMBIENT, "GB_SET_ACTIVE_BOSS_DROP, iSlot = ", iSlot)
	ENDIF
	
	IF iSlot <> ciGB_NO_BOSS_DROPS_LEFT
	
		g_sBossDrop[iSlot].vPosDrop = vPos

		g_sBossDrop[iSlot].eBossDropChosen = eBossDrop
		
		g_sBossDrop[iSlot].PlayerIdFor = PlayerIdFor
		
		g_sBossDrop[iSlot].iSubType = iSubType
		
		g_sBossDrop[iSlot].iAmount = iAmount
		
		g_sBossDrop[iSlot].niPickup	= niPickup
		
		#IF IS_DEBUG_BUILD
		
		STRING sName
		IF g_sBossDrop[iSlot].PlayerIdFor <> INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(g_sBossDrop[iSlot].PlayerIdFor, FALSE)
			sName = GET_PLAYER_NAME(g_sBossDrop[iSlot].PlayerIdFor)
		ELSE
			sName = "INVALID_PLAYER_INDEX"
		ENDIF
		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_SET_ACTIVE_BOSS_DROP = ", GB_GET_BOSS_DROP_STRING(g_sBossDrop[iSlot].eBossDropChosen), 
				" for ", sName, " iSubType = ", g_sBossDrop[iSlot].iSubType, " iAmount = ", 
				g_sBossDrop[iSlot].iAmount, " iStaggeredBossDrop = ", iSlot, " vPos = ", vPos)
		
		#ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_AMBIENT, "GB_SET_ACTIVE_BOSS_DROP, ciGB_NO_BOSS_DROPS_LEFT ")
	#ENDIF
	
	ENDIF
ENDPROC

FUNC BOOL GB_IS_ANY_BOSS_DROP_IN_PROGRESS()

	IF g_sBossDropOverall.iActiveCountBullshark > 0
	OR g_sBossDropOverall.iActiveCountAmmo > 0
	OR g_sBossDropOverall.iActiveCountArmour > 0	
	OR g_sBossDropOverall.iActiveCountSnack > 0 	
	OR g_sBossDropOverall.iActiveCountMolotov > 0
		RETURN TRUE
	ENDIF
	
	INT i
	REPEAT ciGB_TOTAL_MAX_DROPS_ON_GROUND i
		IF g_sBossDrop[i].eBossDropChosen <> eGB_BOSS_DROPS_NONE
	
			RETURN TRUE
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

FUNC INT GB_GET_TOTAL_NUM_ACTIVE_BOSS_DROPS()
	INT iTotal
	
	iTotal = ( g_sBossDropOverall.iActiveCountBullshark + g_sBossDropOverall.iActiveCountAmmo + g_sBossDropOverall.iActiveCountArmour + g_sBossDropOverall.iActiveCountSnack  + g_sBossDropOverall.iActiveCountMolotov  )

	RETURN iTotal
ENDFUNC

FUNC INT GB_NUM_BD_ACTIVE()
	RETURN g_sBossDropOverall.iTotalActiveBossDrops
ENDFUNC

PROC GB_COUNT_TOTAL_BOSS_DROPS()

	g_sBossDropOverall.iTotalActiveBossDrops = GB_GET_TOTAL_NUM_ACTIVE_BOSS_DROPS()
ENDPROC

FUNC INT GB_GET_BOSS_DROP_TIMEOUT(eGB_BOSS_DROPS eBossDrop)

	SWITCH eBossDrop
		CASE eGB_BOSS_DROPS_BULLSHARK
			RETURN g_sMPTunables.igb_drop_bullshark_cooldown
		
		CASE eGB_BOSS_DROPS_AMMO
			RETURN g_sMPTunables.igb_drop_ammo_cooldown      
			
		CASE eGB_BOSS_DROPS_ARMOUR
			RETURN g_sMPTunables.igb_drop_armor_cooldown    
		CASE eGB_BOSS_DROPS_MOLOTOV
			RETURN ciGB_BOSS_DROP_COOLDOWN_MS
		CASE eGB_BOSS_DROPS_SNACKS
			RETURN ciGB_BOSS_DROP_COOLDOWN_MS
	ENDSWITCH
	
	RETURN ciGB_BOSS_DROP_COOLDOWN_MS
ENDFUNC

FUNC INT GB_GET_GANG_SIZE()
	RETURN ( GB_GET_NUM_GOONS_IN_PLAYER_GANG(PLAYER_ID()) + 1 )
ENDFUNC

FUNC INT GB_BOSS_DROP_AMOUNT(eGB_BOSS_DROPS eBossDrop)

	SWITCH eBossDrop
//		CASE eGB_BOSS_DROPS_BULLSHARK
		CASE eGB_BOSS_DROPS_MOLOTOV	
			RETURN 3
		CASE eGB_BOSS_DROPS_AMMO	
			RETURN 200
//		CASE eGB_BOSS_DROPS_ARMOUR
//		CASE eGB_BOSS_DROPS_SNACKS
	ENDSWITCH

	RETURN 1
ENDFUNC

FUNC BOOL bALLOWED_THIS_DROP(eGB_BOSS_DROPS eBossDrop, INT iGangSize, INT iBossDropsActive)

	SWITCH eBossDrop
		CASE eGB_BOSS_DROPS_MOLOTOV
			RETURN g_sBossDropOverall.iActiveCountMolotov < GB_BOSS_DROP_AMOUNT(eGB_BOSS_DROPS_MOLOTOV)
	ENDSWITCH
	 // #IF FEATURE_BIKER
	
	RETURN ( iBossDropsActive < iGangSize )
ENDFUNC

FUNC BOOL GB_IS_THIS_BOSS_DROP_AVAILABLE(eGB_BOSS_DROPS eBossDrop, INT& iRemainingTime)

	UNUSED_PARAMETER(iRemainingTime)

	INT iGangSize
	iGangSize = GB_GET_GANG_SIZE()
	
	// Calculate gang size for goons because GB_GET_NUM_GOONS_IN_PLAYER_GANG returns 0 for them ( only bosses were previously able to drop )
	IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		IF GB_GET_LOCAL_PLAYER_GANG_BOSS() <> INVALID_PLAYER_INDEX()
			iGangSize = ( GB_GET_NUM_GOONS_IN_PLAYER_GANG(GB_GET_LOCAL_PLAYER_GANG_BOSS()) + 1 )
		ENDIF
	ENDIF
	
	INT iBossDropsActive
	iBossDropsActive = GB_NUM_BD_ACTIVE()

	CPRINTLN(DEBUG_AMBIENT, "GB_IS_THIS_BOSS_DROP_AVAILABLE, iBossDropsActive = ", iBossDropsActive, " iGangSize = ", iGangSize, " ciGB_TOTAL_MAX_DROPS_ON_GROUND = ", ciGB_TOTAL_MAX_DROPS_ON_GROUND)

	IF ( iBossDropsActive < ciGB_TOTAL_MAX_DROPS_ON_GROUND )
		IF bALLOWED_THIS_DROP(eBossDrop, iGangSize, iBossDropsActive)
	
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

//FUNC BOOL GB_IS_THIS_BOSS_DROP_AVAILABLE(eGB_BOSS_DROPS eBossDrop, INT& iRemainingTime)
//
//	INT iRemaining
//
//	// Check we haven't filled the max available Boss Drop slots
//	IF GB_GET_TOTAL_NUM_ACTIVE_BOSS_DROPS() < ciGB_TOTAL_MAX_DROPS_ON_GROUND
//	
//		SWITCH eBossDrop
//			CASE eGB_BOSS_DROPS_BULLSHARK
//			
//				// Check for cooldown timer
//				IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerBS)
//					
//					iRemaining = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_sBossDropOverall.stGB_BD_TimerBS)
//					
//					iRemainingTime = ( GB_GET_BOSS_DROP_TIMEOUT(eGB_BOSS_DROPS_BULLSHARK) - iRemaining )	
//					
//					RETURN FALSE
//				ENDIF
//				
//				// Check there aren't already the limit for each drop
//				RETURN ( g_sBossDropOverall.iActiveCountBullshark < ciGB_TOTAL_MAX_OF_EACH_DROP )
//			BREAK
//			
//			CASE eGB_BOSS_DROPS_AMMO
//			
//				IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerAmmo)
//							
//					iRemaining = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_sBossDropOverall.stGB_BD_TimerAmmo)
//					
//					iRemainingTime = ( GB_GET_BOSS_DROP_TIMEOUT(eGB_BOSS_DROPS_AMMO) - iRemaining )	 
//					
//					RETURN FALSE
//				ENDIF
//			
//				RETURN ( g_sBossDropOverall.iActiveCountAmmo < ciGB_TOTAL_MAX_OF_EACH_DROP )
//			BREAK
//				
//			CASE eGB_BOSS_DROPS_ARMOUR
//			
//				IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerArmour)
//							
//					iRemaining = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_sBossDropOverall.stGB_BD_TimerArmour)
//					
//					iRemainingTime = ( GB_GET_BOSS_DROP_TIMEOUT(eGB_BOSS_DROPS_ARMOUR) - iRemaining	)	
//					
//					RETURN FALSE					
//				ENDIF
//			
//				RETURN ( g_sBossDropOverall.iActiveCountArmour < ciGB_TOTAL_MAX_OF_EACH_DROP )
//			BREAK
//				
//			CASE eGB_BOSS_DROPS_SNACKS
//			
//				IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerSnacks)
//							
//					iRemaining = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_sBossDropOverall.stGB_BD_TimerSnacks)
//					
//					iRemainingTime = ( GB_GET_BOSS_DROP_TIMEOUT(eGB_BOSS_DROPS_SNACKS) - iRemaining	)	
//					RETURN FALSE
//				ENDIF
//			
//				RETURN ( g_sBossDropOverall.iActiveCountSnack < ciGB_TOTAL_MAX_OF_EACH_DROP )
//			BREAK
//		ENDSWITCH
//	ENDIF
//	
//	CPRINTLN(DEBUG_AMBIENT, "GB_IS_THIS_BOSS_DROP_AVAILABLE, eGB_BOSS_DROPS_SNACKS, FALSE ")
//	
//	RETURN FALSE
//ENDFUNC

FUNC INT GB_GET_BLIP_COLOUR(eGB_BOSS_DROPS eBossDrop)

	SWITCH eBossDrop
//		CASE eGB_BOSS_DROPS_BULLSHARK
		CASE eGB_BOSS_DROPS_MOLOTOV
		CASE eGB_BOSS_DROPS_AMMO
			RETURN BLIP_COLOUR_GREEN
			
		CASE eGB_BOSS_DROPS_ARMOUR
			RETURN BLIP_COLOUR_BLUE
			
		CASE eGB_BOSS_DROPS_SNACKS
			RETURN BLIP_COLOUR_GREEN
		BREAK
	ENDSWITCH
	
	RETURN BLIP_COLOUR_GREEN
ENDFUNC

FUNC BLIP_SPRITE GB_GET_BLIP_SPRITE(eGB_BOSS_DROPS eBossDrop)

	SWITCH eBossDrop
//		CASE eGB_BOSS_DROPS_BULLSHARK
		CASE eGB_BOSS_DROPS_MOLOTOV
			RETURN RADAR_TRACE_WEAPON_MOLOTOV
		CASE eGB_BOSS_DROPS_AMMO
			RETURN RADAR_TRACE_GANG_ATTACK_PACKAGE
			
		CASE eGB_BOSS_DROPS_ARMOUR
			RETURN RADAR_TRACE_WEAPON_ARMOUR
//			
		CASE eGB_BOSS_DROPS_SNACKS
			RETURN RADAR_TRACE_WEAPON_HEALTH
		BREAK
	ENDSWITCH
	
	RETURN RADAR_TRACE_INVALID
ENDFUNC

FUNC FLOAT GB_GET_BLIP_SIZE()
	
	RETURN BLIP_SIZE_NETWORK_PICKUP_LARGE
ENDFUNC

FUNC STRING GB_GET_BLIP_NAME(eGB_BOSS_DROPS eBossDrop)
	SWITCH eBossDrop
//		CASE eGB_BOSS_DROPS_BULLSHARK
		CASE eGB_BOSS_DROPS_MOLOTOV
			RETURN "WT_MOLOTOV"
		CASE eGB_BOSS_DROPS_AMMO
			RETURN "AMD_BLIPN"
			
		CASE eGB_BOSS_DROPS_ARMOUR
			RETURN "WT_BA"
//			
		CASE eGB_BOSS_DROPS_SNACKS
			RETURN "PIM_TSNAC"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

//PROC REMOVE_INITIAL_BLIP(STRUCT_GB_BOSS_DROP& sGBBossStruct)
//	IF DOES_BLIP_EXIST(sGBBossStruct.BossDropBlipInitial)
//		REMOVE_BLIP(sGBBossStruct.BossDropBlipInitial)
//		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] REMOVE_INITIAL_BLIP DONE  ")
//	ENDIF
//ENDPROC
//
//PROC ADD_INITIAL_BOSS_DROP_BLIP(STRUCT_GB_BOSS_DROP& sGBBossStruct)
//	IF NOT DOES_BLIP_EXIST(sGBBossStruct.BossDropBlipInitial)
//		IF NOT IS_VECTOR_ZERO(sGBBossStruct.vPosBoss)
//			sGBBossStruct.BossDropBlipInitial = ADD_BLIP_FOR_COORD(sGBBossStruct.vPosBoss)
//			SET_BLIP_SCALE(sGBBossStruct.BossDropBlipInitial, GB_GET_BLIP_SIZE())
//			SET_BLIP_SPRITE(sGBBossStruct.BossDropBlipInitial, GB_GET_BLIP_SPRITE(sGBBossStruct.eBossDropChosen))
//			SET_BLIP_COLOUR(sGBBossStruct.BossDropBlipInitial, GB_GET_BLIP_COLOUR(sGBBossStruct.eBossDropChosen))
//			SET_BLIP_NAME_FROM_TEXT_FILE(sGBBossStruct.BossDropBlipInitial, GB_GET_BLIP_NAME(sGBBossStruct.eBossDropChosen))
//			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] ADD_INITIAL_BOSS_DROP_BLIP DONE  ",GB_GET_BLIP_NAME(sGBBossStruct.eBossDropChosen), 
//					" sGBBossStruct.vPosBoss = ", sGBBossStruct.vPosBoss)
//		ENDIF
//	ENDIF
//ENDPROC

PROC ADD_BOSS_DROP_BLIP(STRUCT_GB_BOSS_DROP& sGBBossStruct)
//	REMOVE_INITIAL_BLIP(sGBBossStruct)
	IF NOT DOES_BLIP_EXIST(sGBBossStruct.BossDropBlip)
		IF NETWORK_DOES_NETWORK_ID_EXIST(sGBBossStruct.niPickup)
			sGBBossStruct.BossDropBlip = ADD_BLIP_FOR_ENTITY(NET_TO_ENT(sGBBossStruct.niPickup))
			SET_BLIP_SCALE(sGBBossStruct.BossDropBlip, GB_GET_BLIP_SIZE())
			SET_BLIP_SPRITE(sGBBossStruct.BossDropBlip, GB_GET_BLIP_SPRITE(sGBBossStruct.eBossDropChosen))
			SET_BLIP_COLOUR(sGBBossStruct.BossDropBlip, GB_GET_BLIP_COLOUR(sGBBossStruct.eBossDropChosen))
			SET_BLIP_NAME_FROM_TEXT_FILE(sGBBossStruct.BossDropBlip, GB_GET_BLIP_NAME(sGBBossStruct.eBossDropChosen))
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] ADD_BOSS_DROP_BLIP DONE  ",GB_GET_BLIP_NAME(sGBBossStruct.eBossDropChosen))
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_BOSS_DROP_BLIPS(STRUCT_GB_BOSS_DROP& sGBBossStruct)
	IF DOES_BLIP_EXIST(sGBBossStruct.BossDropBlip)
		REMOVE_BLIP(sGBBossStruct.BossDropBlip)
		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] REMOVE_BOSS_DROP_BLIPS DONE, BossDropBlip  ")
	ENDIF
//	REMOVE_INITIAL_BLIP(sGBBossStruct)
ENDPROC	

PROC GB_START_BOSS_DROP_TIMER(eGB_BOSS_DROPS eBossDrop)
	
	SWITCH eBossDrop
		CASE eGB_BOSS_DROPS_BULLSHARK
			IF NOT HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerBS)
				START_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerBS)
				CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_START_BOSS_DROP_TIMER, stGB_BD_TimerBS ")
			ENDIF
		BREAK
		
		CASE eGB_BOSS_DROPS_AMMO
			IF NOT HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerAmmo)
				START_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerAmmo)
				CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_START_BOSS_DROP_TIMER, stGB_BD_TimerAmmo ")
			ENDIF
		BREAK
			
		CASE eGB_BOSS_DROPS_ARMOUR
			IF NOT HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerArmour)
				START_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerArmour)
				CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_START_BOSS_DROP_TIMER, stGB_BD_TimerArmour ")
			ENDIF
		BREAK
		
		CASE eGB_BOSS_DROPS_MOLOTOV
			IF NOT HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerMolotov)
				START_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerMolotov)
				CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_START_BOSS_DROP_TIMER, stGB_BD_TimerMolotov ")
			ENDIF
		BREAK
		
		CASE eGB_BOSS_DROPS_SNACKS
			IF NOT HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerSnacks)
				START_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerSnacks)
				CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_START_BOSS_DROP_TIMER, stGB_BD_TimerSnacks ")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC GB_CLEAR_BOSS_DROP_TIMER(eGB_BOSS_DROPS eBossDrop)

	SWITCH eBossDrop
		CASE eGB_BOSS_DROPS_BULLSHARK
			IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerBS)
				RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerBS)
				CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_CLEAR_BOSS_DROP_TIMER, stGB_BD_TimerBS ")
			ENDIF
		BREAK
		
		CASE eGB_BOSS_DROPS_AMMO
			IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerAmmo)
				RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerAmmo)
				CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_CLEAR_BOSS_DROP_TIMER, stGB_BD_TimerAmmo ")
			ENDIF
		BREAK
			
		CASE eGB_BOSS_DROPS_ARMOUR
			IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerArmour)
				RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerArmour)
				CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_CLEAR_BOSS_DROP_TIMER, stGB_BD_TimerArmour ")
			ENDIF
		BREAK
		
		CASE eGB_BOSS_DROPS_MOLOTOV
			IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerMolotov)
				RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerMolotov)
				CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_CLEAR_BOSS_DROP_TIMER, stGB_BD_TimerMolotov ")
			ENDIF
		BREAK
			
		CASE eGB_BOSS_DROPS_SNACKS
			IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerSnacks)
				RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerSnacks)
				CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_CLEAR_BOSS_DROP_TIMER, stGB_BD_TimerSnacks ")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC GB_BOSS_DROP_MAINTAIN_TIMERS()

	IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerBS)
	AND HAS_NET_TIMER_EXPIRED(g_sBossDropOverall.stGB_BD_TimerBS, GB_GET_BOSS_DROP_TIMEOUT(eGB_BOSS_DROPS_BULLSHARK))
		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_BOSS_DROP_MAINTAIN_TIMERS, RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerBS) ")
		RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerBS)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerAmmo)
	AND HAS_NET_TIMER_EXPIRED(g_sBossDropOverall.stGB_BD_TimerAmmo, GB_GET_BOSS_DROP_TIMEOUT(eGB_BOSS_DROPS_AMMO))
		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_BOSS_DROP_MAINTAIN_TIMERS, RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerAmmo) ")
		RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerAmmo)
	ENDIF

	IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerArmour)
	AND HAS_NET_TIMER_EXPIRED(g_sBossDropOverall.stGB_BD_TimerArmour, GB_GET_BOSS_DROP_TIMEOUT(eGB_BOSS_DROPS_ARMOUR))
		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_BOSS_DROP_MAINTAIN_TIMERS, RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerArmour) ")
		RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerArmour)			
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerMolotov)
	AND HAS_NET_TIMER_EXPIRED(g_sBossDropOverall.stGB_BD_TimerMolotov, GB_GET_BOSS_DROP_TIMEOUT(eGB_BOSS_DROPS_MOLOTOV))
		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_BOSS_DROP_MAINTAIN_TIMERS, RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerMolotov) ")
		RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerMolotov)
	ENDIF

	IF HAS_NET_TIMER_STARTED(g_sBossDropOverall.stGB_BD_TimerSnacks)
	AND HAS_NET_TIMER_EXPIRED(g_sBossDropOverall.stGB_BD_TimerSnacks, GB_GET_BOSS_DROP_TIMEOUT(eGB_BOSS_DROPS_SNACKS))
		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_BOSS_DROP_MAINTAIN_TIMERS, RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerSnacks) ")
		RESET_NET_TIMER(g_sBossDropOverall.stGB_BD_TimerSnacks)
	ENDIF
	
ENDPROC

FUNC MODEL_NAMES mnMOLOTOV()
	RETURN GET_WEAPONTYPE_MODEL(WEAPONTYPE_MOLOTOV)
ENDFUNC

PROC GB_BOSS_DROP_SCRIPT_CLEANUP(STRUCT_GB_BOSS_DROP& sGBBossStruct)
//	STRUCT_GB_BOSS_DROP sTemp
//	g_sBossDrop = sTemp
//	GB_CLEAR_BOSS_DROP_TIMER(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen)
	CLEAR_BIT(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iBitSet, ciGB_BOSS_DROP_BITSET_PICKUP_READY)
	IF IS_BIT_SET(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iBitSet, ciGB_BOSS_DROP_BITSET_REQUESTED_ASSETS)
		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_BOSS_DROP_SCRIPT_CLEANUP -- SET_MODEL_AS_NO_LONGER_NEEDED ")
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_BOX_AMMO02A)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_ARMOUR_PICKUP)
		SET_MODEL_AS_NO_LONGER_NEEDED(mnMOLOTOV())
//		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CHOC_EGO)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CHOC_METO)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_DRUG_PACKAGE_02)
		CLEAR_BIT(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iBitSet, ciGB_BOSS_DROP_BITSET_REQUESTED_ASSETS)
	ENDIF
	g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iDropProgress = ciGB_START
	CLEAR_DM_BRU_BOX_DATA()
	REMOVE_BOSS_DROP_BLIPS(sGBBossStruct)
	NETWORK_INDEX netId
	GB_SET_ACTIVE_BOSS_DROP(eGB_BOSS_DROPS_NONE, INVALID_PLAYER_INDEX(), -1, -1, <<0,0,0>>, netId, g_sBossDropOverall.iStaggeredBossDrop)
//	IF DOES_PICKUP_EXIST(sGBBossStruct.piPickup)
//		IF NETWORK_HAS_CONTROL_OF_PICKUP(sGBBossStruct.piPickup)
//			REMOVE_PICKUP(sGBBossStruct.piPickup)
//		ENDIF
//	ENDIF
	IF DOES_BLIP_EXIST(sGBBossStruct.BossDropBlip)
		REMOVE_BLIP(sGBBossStruct.BossDropBlip)
	ENDIF
	IF sGBBossStruct.modelBD <> DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(sGBBossStruct.modelBD)
		sGBBossStruct.modelBD = DUMMY_MODEL_FOR_SCRIPT
	ENDIF
	CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_BOSS_DROP_SCRIPT_CLEANUP DONE  ")
ENDPROC

// *** If you add models to this be sure to add to BOSS_DROP_PRELOAD and GB_BOSS_DROP_SCRIPT_CLEANUP ***
FUNC MODEL_NAMES GET_BOSS_DROP_ITEM_MODEL(eGB_BOSS_DROPS eBossDrop, INT iType = -1)

	SWITCH eBossDrop
//		CASE eGB_BOSS_DROPS_BULLSHARK
		CASE eGB_BOSS_DROPS_MOLOTOV		
			RETURN mnMOLOTOV()
		CASE eGB_BOSS_DROPS_AMMO
			RETURN PROP_BOX_AMMO02A // x:\gta5\script\dev_ng\multiplayer\scripts\modes\freemode\Ambient\AM_AMMO_DROP.sc

		CASE eGB_BOSS_DROPS_ARMOUR
			RETURN PROP_ARMOUR_PICKUP
			
		CASE eGB_BOSS_DROPS_SNACKS
		
			// 2559856
			SWITCH iType
				CASE ciPI_ITEM_TYPE_SNACK_1
				CASE ciPI_ITEM_TYPE_SNACK_2
				CASE ciPI_ITEM_TYPE_SNACK_3
					RETURN PROP_CHOC_METO
			ENDSWITCH
			
//			SWITCH iType
//				CASE ciPI_ITEM_TYPE_SNACK_1
//					RETURN PROP_CHOC_PQ
//				CASE ciPI_ITEM_TYPE_SNACK_2
//					RETURN PROP_CHOC_EGO
//				CASE ciPI_ITEM_TYPE_SNACK_3
//					RETURN PROP_CHOC_METO
//			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN PROP_PAPER_BAG_01
ENDFUNC

FUNC PICKUP_TYPE GET_BOSS_DROP_PICKUP_TYPE(eGB_BOSS_DROPS eBossDrop, INT iType)

	SWITCH eBossDrop
//		CASE eGB_BOSS_DROPS_BULLSHARK
		CASE eGB_BOSS_DROPS_AMMO
			RETURN PICKUP_AMMO_BULLET_MP
			
		CASE eGB_BOSS_DROPS_ARMOUR
			RETURN PICKUP_ARMOUR_STANDARD
			
		CASE eGB_BOSS_DROPS_MOLOTOV
			RETURN PICKUP_WEAPON_MOLOTOV
			
		CASE eGB_BOSS_DROPS_SNACKS
			SWITCH iType
				CASE ciPI_ITEM_TYPE_SNACK_1
					RETURN PICKUP_CUSTOM_SCRIPT
				CASE ciPI_ITEM_TYPE_SNACK_2
					RETURN PICKUP_CUSTOM_SCRIPT
				CASE ciPI_ITEM_TYPE_SNACK_3
					RETURN PICKUP_CUSTOM_SCRIPT // PROP_CHOC_PQ should be PICKUP_HEALTH_SNACK !!!!
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN PICKUP_TYPE_INVALID
ENDFUNC

FUNC BOOL IS_MODEL_SNACK(MODEL_NAMES model)
	SWITCH model
		CASE PROP_CHOC_PQ
		CASE PROP_CHOC_EGO
		CASE PROP_CHOC_METO
			RETURN TRUE
			
	ENDSWITCH
	  
	RETURN FALSE
ENDFUNC


FUNC BOOL bBIKER_DROP(PLAYER_INDEX playerId)

	IF playerId <> INVALID_PLAYER_INDEX()
	
		IF GB_GET_PLAYER_GANG_ROLE(playerId) = GR_VP
			PRINTLN("[MAGNATE_GANG_BOSS] [BOSS_DROPS]bBIKER_DROP - GR_VP ")
			RETURN TRUE
		ENDIF

//		IF GB_GET_PLAYER_GANG_ROLE(sMemorialWall.pRoadCaptainID) != GR_ROAD_CAPTAIN
//			PRINTLN("[MAGNATE_GANG_BOSS] [BOSS_DROPS]bBIKER_DROP - GR_ROAD_CAPTAIN ")
//			RETURN TRUE
//		ENDIF

		IF GB_GET_PLAYER_GANG_ROLE(playerId) = GR_ENFORCER
			PRINTLN("[MAGNATE_GANG_BOSS] [BOSS_DROPS]bBIKER_DROP - GR_ENFORCER ")
			RETURN TRUE
		ENDIF
	
		IF GB_GET_PLAYER_GANG_ROLE(playerId) = GR_SERGEANT_AT_ARMS
			PRINTLN("[MAGNATE_GANG_BOSS] [BOSS_DROPS]bBIKER_DROP - GR_SERGEANT_AT_ARMS ")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

 // #IF FEATURE_BIKER

FUNC BOOL GET_COORDS_FOR_BOSS_DROP(PLAYER_INDEX playerId, INT iSlot, VECTOR &Vpos)

	FLOAT fHeading, fReturn
	VECTOR vMyPos
	PED_INDEX playerPed
	
	SPAWN_SEARCH_PARAMS SpawnSearchParams
	SpawnSearchParams.bConsiderInteriors = TRUE
	FLOAT fRadius
	fRadius = 5.0
	
	IF IS_NET_PLAYER_OK(playerId, FALSE)
	
		vMyPos = GET_PLAYER_COORDS(playerId)
		playerPed = GET_PLAYER_PED(playerId)
		fHeading = GET_ENTITY_HEADING(playerPed)
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GET_COORDS_FOR_BOSS_DROP, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS  ", GET_PLAYER_NAME(playerId))
	
	IF NOT IS_VECTOR_ZERO(vMyPos)
	
		IF GET_INTERIOR_FROM_ENTITY(playerPed) <> NULL
		
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GET_COORDS_FOR_BOSS_DROP, GET_INTERIOR_FROM_ENTITY ", GET_PLAYER_NAME(playerId))
		
			RETURN GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vMyPos, fRadius, Vpos, fReturn, SpawnSearchParams)
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GET_COORDS_FOR_BOSS_DROP, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS  ", GET_PLAYER_NAME(playerId))

			FLOAT fHeadingOffset
			fHeadingOffset = ( ( 360.00 / TO_FLOAT(ciGB_TOTAL_MAX_DROPS_ON_GROUND) ) * TO_FLOAT(iSlot) )
			fHeadingOffset = ( fHeadingOffset + fHeading )
			Vpos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vMyPos, fHeadingOffset, <<0.0, 3.0, 0.0>>)
			
			IF GET_GROUND_Z_FOR_3D_COORD(Vpos, Vpos.Z)
				
				RETURN TRUE
			
			ELIF bBIKER_DROP(playerId)  
				Vpos.Z = vMyPos.Z //GET_APPROX_HEIGHT_FOR_POINT(Vpos.x, Vpos.Y) 
				CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GET_COORDS_FOR_BOSS_DROP, GET_APPROX_HEIGHT_FOR_POINT  ", Vpos.Z)
				
				RETURN TRUE
			
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

//FUNC BOOL GET_COORDS_AND_HEADING_FOR_BOSS_DROP(STRUCT_GB_BOSS_DROP& sGBBossStruct, INT iSlot, VECTOR &Vpos, FLOAT &fHeading)
//
////	SPAWN_SEARCH_PARAMS SpawnSearchParams
////	FLOAT fRadius
////	fRadius = 5.0
//
//	UNUSED_PARAMETER(fHeading)
//	
//	PLAYER_INDEX playerBoss
//	playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
//	
//	PED_INDEX pedBoss
//	FLOAT fHeadingBoss	
//	
//	IF IS_NET_PLAYER_OK(playerBoss, FALSE)
//		pedBoss = GET_PLAYER_PED(playerBoss)
//		IF DOES_ENTITY_EXIST(pedBoss)
//		AND NOT IS_ENTITY_DEAD(pedBoss)
//			sGBBossStruct.vPosBoss = GET_ENTITY_COORDS(pedBoss)
//			fHeadingBoss = GET_ENTITY_HEADING(pedBoss)
//		ENDIF
//	ENDIF
//	
////	SpawnSearchParams.vAvoidCoords[0] = sGBBossStruct.vPosBoss
//	
//	IF NOT IS_VECTOR_ZERO(sGBBossStruct.vPosBoss)
////		IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(sGBBossStruct.vPosBoss, fRadius, Vpos, fHeading, SpawnSearchParams)
//		
//			// Temp fix to prevent pickups spawning on top of each other
//			FLOAT fHeadingOffset
//			fHeadingOffset = ( ( 360.00 / TO_FLOAT(ciGB_TOTAL_MAX_DROPS_ON_GROUND) ) * TO_FLOAT(iSlot) )
//			fHeadingOffset = ( fHeadingOffset + fHeadingBoss )
//			Vpos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sGBBossStruct.vPosBoss, fHeadingOffset, <<0.0, 3.0, 0.0>>)
//			
//			RETURN TRUE
////		ENDIF
//	ENDIF
//
//	RETURN FALSE
//ENDFUNC

PROC BOSS_DROP_PRELOAD()
//	IF NOT IS_BIT_SET(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iBitSet, ciGB_BOSS_DROP_BITSET_REQUESTED_ASSETS)
		REQUEST_MODEL(PROP_DRUG_PACKAGE_02)
		REQUEST_MODEL(PROP_BOX_AMMO02A)
		REQUEST_MODEL(PROP_ARMOUR_PICKUP)
		REQUEST_MODEL(mnMOLOTOV())
//		REQUEST_MODEL(PROP_CHOC_PQ)
//		REQUEST_MODEL(PROP_CHOC_EGO)
		REQUEST_MODEL(PROP_CHOC_METO)
//		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] BOSS_DROP_PRELOAD -- ciGB_BOSS_DROP_BITSET_REQUESTED_ASSETS")
		SET_BIT(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iBitSet, ciGB_BOSS_DROP_BITSET_REQUESTED_ASSETS)
//	ENDIF
ENDPROC

PROC GB_COUNT_ACTIVE_BOSS_DROPS(BOOL bCleanup)
	
	IF g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen = eGB_BOSS_DROPS_AMMO
		IF bCleanup
			g_sBossDropOverall.iActiveCountAmmo--
		ELSE
			g_sBossDropOverall.iActiveCountAmmo++
		ENDIF
	ENDIF
	
	IF g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen = eGB_BOSS_DROPS_ARMOUR
		IF bCleanup
			g_sBossDropOverall.iActiveCountArmour--
		ELSE
			g_sBossDropOverall.iActiveCountArmour++
		ENDIF
	ENDIF
	
	IF g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen = eGB_BOSS_DROPS_BULLSHARK
		IF bCleanup
			g_sBossDropOverall.iActiveCountBullshark--
		ELSE
			g_sBossDropOverall.iActiveCountBullshark++
		ENDIF
	ENDIF
	
	IF g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen = eGB_BOSS_DROPS_MOLOTOV
		IF bCleanup
			g_sBossDropOverall.iActiveCountMolotov--
		ELSE
			g_sBossDropOverall.iActiveCountMolotov++
		ENDIF
	ENDIF
	
	IF g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen = eGB_BOSS_DROPS_SNACKS
		IF bCleanup
			g_sBossDropOverall.iActiveCountSnack--
		ELSE
			g_sBossDropOverall.iActiveCountSnack++
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_COUNT_ACTIVE_BOSS_DROPS, iActiveCountAmmo = ", g_sBossDropOverall.iActiveCountAmmo,
			" iActiveCountArmour = ", g_sBossDropOverall.iActiveCountArmour, " iActiveCountBullshark = ", 
			g_sBossDropOverall.iActiveCountBullshark, " iActiveCountSnack = ", g_sBossDropOverall.iActiveCountSnack, " iActiveCountMolotov = ", g_sBossDropOverall.iActiveCountMolotov)
ENDPROC

// *******
// BOSS DROPS
// *******
FUNC INT GET_BULLSHARK_INSTANCE()
//	IF g_sBossDropOverall.iActiveCountBullshark = 1
//		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] [BS] MAINTAIN_BRU_BOX_DEATHMATCH, GET_BULLSHARK_INSTANCE, 0 ")
//		RETURN NUM_NETWORK_PLAYERS
//	ELIF g_sBossDropOverall.iActiveCountBullshark = 2
//		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] [BS] MAINTAIN_BRU_BOX_DEATHMATCH, GET_BULLSHARK_INSTANCE, 1 ")
//		RETURN (NUM_NETWORK_PLAYERS + g_sBossDropOverall.iActiveCountBullshark)
//	ELSE
//		CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] [BS] MAINTAIN_BRU_BOX_DEATHMATCH, GET_BULLSHARK_INSTANCE, 2 ")
//		RETURN (NUM_NETWORK_PLAYERS + g_sBossDropOverall.iActiveCountBullshark)
//	ENDIF
	
	//RETURN (NUM_NETWORK_PLAYERS + g_sBossDropOverall.iActiveCountBullshark + NATIVE_TO_INT(PLAYER_ID()))

	PLAYER_INDEX playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	RETURN (NUM_NETWORK_PLAYERS + g_sBossDropOverall.iActiveCountBullshark + NATIVE_TO_INT(playerBoss))
ENDFUNC

PROC GB_ENABLE_PICKUP_FOR_GANG_MEMBERS(OBJECT_INDEX oiPickup)

	INT iThisParticipant
	PLAYER_INDEX PlayerID
	INT iPlayer
	
	INT iPlayerFlags
	
	REPEAT NUM_NETWORK_PLAYERS iThisParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iThisParticipant))
			PlayerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iThisParticipant))	
			IF IS_NET_PLAYER_OK(PlayerId, FALSE)
				iPlayer = NATIVE_TO_INT(PlayerID)
				IF GB_ARE_PLAYERS_IN_SAME_GANG(PlayerId, PLAYER_ID())
					SET_BIT(iPlayerFlags, iPlayer)
					CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_ENABLE_PICKUP_FOR_GANG_MEMBERS FOR ", GET_PLAYER_NAME(PlayerId))
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	BLOCK_PLAYERS_FOR_AMBIENT_PICKUP(oiPickup, iPlayerFlags)
ENDPROC

PROC PLAY_DROP_SOUND(eGB_BOSS_DROPS eBossDrop, ENTITY_INDEX eiDrop)

	STRING sSound

	SWITCH eBossDrop

		CASE eGB_BOSS_DROPS_BULLSHARK
			sSound = "Boss_Spawn_Bullshark"
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] PLAY_DROP_SOUND, eGB_BOSS_DROPS_BULLSHARK ")
		BREAK
		
		CASE eGB_BOSS_DROPS_AMMO
			sSound = "Boss_Spawn_Ammo"
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] PLAY_DROP_SOUND, eGB_BOSS_DROPS_AMMO ")
		BREAK
		
		CASE eGB_BOSS_DROPS_ARMOUR
			sSound = "Boss_Spawn_Armor"
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] PLAY_DROP_SOUND, eGB_BOSS_DROPS_ARMOUR ")
		BREAK
		
		CASE eGB_BOSS_DROPS_MOLOTOV
			sSound = "Boss_Spawn_Ammo"
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] PLAY_DROP_SOUND, eGB_BOSS_DROPS_MOLOTOV ")
		BREAK

	ENDSWITCH
	
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
		PLAY_SOUND_FROM_ENTITY(-1, sSound, eiDrop, "GTAO_Biker_FM_Soundset")
	ELSE
		PLAY_SOUND_FROM_ENTITY(-1, sSound, eiDrop, "GTAO_Boss_Goons_FM_Soundset")
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_TO_DROP()

	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		RETURN TRUE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
		SWITCH GB_GET_PLAYER_GANG_ROLE(PLAYER_ID())
			CASE GR_NONE				RETURN FALSE
			CASE GR_VP					RETURN FALSE
			CASE GR_ROAD_CAPTAIN		RETURN FALSE
			CASE GR_ENFORCER			RETURN FALSE
			CASE GR_SERGEANT_AT_ARMS	RETURN TRUE 
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GB_MAINTAIN_BOSS_DROPS()

	INT iPlacementFlags
	iPlacementFlags = 0
	
	BOOL bIsBullShark
	
	bIsBullShark = ( g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen = eGB_BOSS_DROPS_BULLSHARK )
	
	VECTOR vPickupPos
	
	MODEL_NAMES Model 		
	PICKUP_TYPE pickupType 	
	INT iAmount 	
	INT iSubType
	ENTITY_INDEX eiDrop
	
	BOOL bBOSS
	bBOSS = GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG() //IS_LOCAL_PLAYER_ALLOWED_TO_DROP()
	
	// Cooldown timers
	GB_BOSS_DROP_MAINTAIN_TIMERS()
	
	GB_COUNT_TOTAL_BOSS_DROPS()		
	
	// Add the blip
	ADD_BOSS_DROP_BLIP(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop]) //, g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].niPickup)

	SWITCH g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iDropProgress
	
		CASE ciGB_START
		
			// 2549256
			IF bBOSS
				BOSS_DROP_PRELOAD()
			ENDIF
		
			IF g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen <> eGB_BOSS_DROPS_NONE
			
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))

				iSubType = g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iSubType
			
				g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].modelBD = GET_BOSS_DROP_ITEM_MODEL(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen, iSubType)
				
				IF NOT bBOSS
				OR GET_COORDS_FOR_BOSS_DROP(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].PlayerIdFor, g_sBossDropOverall.iStaggeredBossDrop, g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].vPosDrop)
					
					IF bIsBullShark		
					OR NOT bBOSS
					OR HAS_MODEL_LOADED(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].modelBD)
					
						vPickupPos 	= g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].vPosDrop 
						
						// Create Bullshark Testosterone
						IF bIsBullShark
							SET_BIT(MPGlobalsAmbience.BruBoxDataDM.iBitSet, ciGB_BRU_BOX_ON)
							SET_DEATHMATCH_BRU_BOX(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].vPosDrop, g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].fHeadingDrop, g_sBossDropOverall.iStaggeredBossDrop)
							eiDrop = PLAYER_PED_ID()
						ELSE

							Model 		= g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].modelBD
							pickupType 	= GET_BOSS_DROP_PICKUP_TYPE(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen, iSubType)
							iAmount 	= g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iAmount

							// Create the other pickup types								
							IF bBOSS																					
								g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].oiPickup 
									= CREATE_AMBIENT_PICKUP(pickupType, vPickupPos, iPlacementFlags, iAmount, Model, TRUE, FALSE)
									
								GB_ENABLE_PICKUP_FOR_GANG_MEMBERS(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].oiPickup)
									
								g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].niPickup = OBJ_TO_NET(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].oiPickup)
								
								CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] iStaggeredBossDrop = ", g_sBossDropOverall.iStaggeredBossDrop, "  CREATE_AMBIENT_PICKUP, ", NATIVE_TO_INT(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].niPickup), 
										" vPickupPos = ", vPickupPos)		
							ENDIF
							
							IF NETWORK_DOES_NETWORK_ID_EXIST(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].niPickup)
								eiDrop = NET_TO_ENT(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].niPickup)
							ENDIF
						ENDIF
						
						// Send the event
						IF bBOSS
							GB_BROADCAST_BOSS_DROP(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen, 
													PLAYER_ID(), 
													1, 
													GB_BOSS_DROP_AMOUNT(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen), 
													g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].niPickup,
													vPickupPos)
						ENDIF
						
						PLAY_DROP_SOUND(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen, eiDrop)
						
						GB_COUNT_ACTIVE_BOSS_DROPS(FALSE)
//						GB_START_BOSS_DROP_TIMER(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen)
						CLEAR_BIT(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iBitSet, ciGB_BOSS_DROP_BITSET_PICKUP_READY)
						
						SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), pickupType, TRUE)
//						SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
						
						g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iDropProgress = ciGB_HOLD
						CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_MAINTAIN_BOSS_DROPS - iStaggeredBossDrop = ", g_sBossDropOverall.iStaggeredBossDrop, " , ciGB_START, vPosDrop = ", g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].vPosDrop,
								" for ", GB_GET_BOSS_DROP_STRING(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].eBossDropChosen))
						//CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_MAINTAIN_BOSS_DROPS - iStaggeredBossDrop = ", g_sBossDropOverall.iStaggeredBossDrop, "  >> ciGB_HOLD ")
					ELSE
						CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_MAINTAIN_BOSS_DROPS - iStaggeredBossDrop = ", g_sBossDropOverall.iStaggeredBossDrop, "  -- HAS_MODEL_LOADED")
					ENDIF
				ENDIF				
			ENDIF
		BREAK
		
		CASE ciGB_HOLD
			//Just wait here
		BREAK
		
		CASE ciGB_CLEANUP
			GB_COUNT_ACTIVE_BOSS_DROPS(TRUE)
			GB_BOSS_DROP_SCRIPT_CLEANUP(g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop])
			g_sBossDrop[g_sBossDropOverall.iStaggeredBossDrop].iDropProgress = ciGB_START
			CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_MAINTAIN_BOSS_DROPS - iStaggeredBossDrop = ", g_sBossDropOverall.iStaggeredBossDrop, "  >> ciGB_START ")
		BREAK
	ENDSWITCH
	
	INT iLoop
	REPEAT ciGB_TOTAL_MAX_DROPS_ON_GROUND iLoop
		IF g_sBossDrop[iLoop].iDropProgress = ciGB_HOLD
			IF g_sBossDrop[iLoop].eBossDropChosen = eGB_BOSS_DROPS_BULLSHARK
				IF g_sBossDrop[iLoop].bCollected = TRUE
					g_sBossDrop[iLoop].bCollected = FALSE
					g_sBossDrop[iLoop].iDropProgress = ciGB_CLEANUP
					CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_MAINTAIN_BOSS_DROPS - iStaggeredBossDrop = ", iLoop, " , bLaunchBruBox, >> ciGB_CLEANUP ")
				ENDIF
			ELSE	
				IF IS_BIT_SET(g_sBossDrop[iLoop].iBitSet, ciGB_BOSS_DROP_BITSET_PICKUP_READY)
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(g_sBossDrop[iLoop].niPickup)
						g_sBossDrop[iLoop].iDropProgress = ciGB_CLEANUP
						CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_MAINTAIN_BOSS_DROPS - iStaggeredBossDrop = ", iLoop, " , HAS_PICKUP_BEEN_COLLECTED, >> ciGB_CLEANUP ")
						
						IF g_sBossDrop[iLoop].eBossDropChosen = eGB_BOSS_DROPS_AMMO
							SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_COLLECT_AMMO_DROP)
							CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_MAINTAIN_BOSS_DROPS - iStaggeredBossDrop = ", iLoop, " , SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_COLLECT_AMMO_DROP) ")
						ENDIF
					ENDIF
				ELSE
					IF NETWORK_DOES_NETWORK_ID_EXIST(g_sBossDrop[iLoop].niPickup)
						CPRINTLN(DEBUG_AMBIENT, "[MAGNATE_GANG_BOSS] [BOSS_DROPS] GB_MAINTAIN_BOSS_DROPS - iStaggeredBossDrop = ", iLoop, " , ciGB_BOSS_DROP_BITSET_PICKUP_READY ")
						SET_BIT(g_sBossDrop[iLoop].iBitSet, ciGB_BOSS_DROP_BITSET_PICKUP_READY)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Staggered loop
	g_sBossDropOverall.iStaggeredBossDrop++
	IF g_sBossDropOverall.iStaggeredBossDrop >= ciGB_TOTAL_MAX_DROPS_ON_GROUND
		g_sBossDropOverall.iStaggeredBossDrop = 0
	ENDIF
ENDPROC


 // #IF FEATURE_GANG_BOSS

