
//////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FMMC_											//
// Description: Controls the custom missions in free mode that have been created with the creator	//
// Written by:  Robert Wright																		//
// Date: 28/01/2012																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////
///    
USING "rage_builtins.sch"
USING "globals.sch"
USING "fm_in_corona_header.sch"

PROC SET_BLIP_INFO_STRUCT_DEFAULTS_FOR_CREATION_TYPE(FMMC_BLIP_INFO_STRUCT &sBlipStruct, FMMC_BLIP_TYPE eBlipType)
	SWITCH eBlipType
		CASE FMMC_BLIP_VEHICLE
		CASE FMMC_BLIP_TRAIN
			sBlipStruct.fBlipScale = BLIP_SIZE_NETWORK_VEHICLE_LARGE
		BREAK
		CASE FMMC_BLIP_PED
			sBlipStruct.fBlipScale = BLIP_SIZE_NETWORK_PED
			SET_BIT(sBlipStruct.iBlipBitset, ciBLIP_INFO_Show_Height_Indicator)
		BREAK
		CASE FMMC_BLIP_OBJECT
			sBlipStruct.fBlipScale = FMMC_BLIP_SCALE_OBJECT
		BREAK
		CASE FMMC_BLIP_PICKUP
			sBlipStruct.fBlipScale = BLIP_SIZE_NETWORK_PICKUP_LARGE
		BREAK
		CASE FMMC_BLIP_DYNO_PROP
			SET_BIT(sBlipStruct.iBlipBitset, ciBLIP_INFO_Hide_Blip)
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEAR_RGBA_STRUCT(RGBA_STRUCT &rgbaStruct)
	rgbaStruct.R = 0
	rgbaStruct.G = 0
	rgbaStruct.B = 0
	rgbaStruct.A = 255
ENDPROC

PROC CLEAR_ENTITY_PTFX_DATA(ENTITY_PTFX_STRUCT &sEntityPTFXData)
	sEntityPTFXData.ePTFXType = FMMC_ENTITY_PTFX_NONE
	sEntityPTFXData.fSize = 1.0
	CLEAR_RGBA_STRUCT(sEntityPTFXData.sColour)
ENDPROC

PROC CLEAR_WARP_LOCATION_SETTINGS(WARP_LOCATION_SETTINGS &sWarpLocationSettings)
	INT i
	FOR i = 0 TO FMMC_MAX_RULE_VECTOR_WARPS-1
		sWarpLocationSettings.vPosition[i] = <<0.0, 0.0, 0.0>>
		sWarpLocationSettings.fHeading[i] = 0.0
	ENDFOR
	sWarpLocationSettings.iWarpLocationBS = 0
	sWarpLocationSettings.iCheckPointPositionIDStart = -1
	sWarpLocationSettings.iForcedRespawnLocation = -1	
	CLEAR_ENTITY_PTFX_DATA(sWarpLocationSettings.sEntityWarpPTFX)
ENDPROC

PROC CLEAR_ENTITY_BLIP_DATA_STRUCT(FMMC_BLIP_INFO_STRUCT &sStructToClear)
	sStructToClear.iBlipBitset = 0
	sStructToClear.iBlipStyle = 0
	sStructToClear.iBlipColour = BLIP_COLOUR_DEFAULT
	sStructToClear.iBlipSecondaryColour = BLIP_COLOUR_DEFAULT
	sStructToClear.iBlipColourAggro = BLIP_COLOUR_DEFAULT
	sStructToClear.iBlipAlpha = 255
	sStructToClear.fBlipScale = 1.0
	sStructToClear.iBlipSpriteOverride = 0
	sStructToClear.fBlipRange = 0.0
	sStructToClear.fBlipHeightDifference = 0.0
	sStructToClear.iBlipPriority = 0
	sStructToClear.iBlipNameIndex = -1	
	sStructToClear.iShowBlipPreReqRequired = ciPREREQ_None
	sStructToClear.iRemoveBlipPreReqRequired = ciPREREQ_None
	sStructToClear.iBlipShowZonesBS = 0 
	sStructToClear.iBlipHideZonesBS = 0
	sStructToClear.fBlipShowHeightIndicatorRange = 0.0
	sStructToClear.sBlipAlphaRange.iMinAlpha = 75
	sStructToClear.sBlipAlphaRange.fMinDistance = 100.0
	sStructToClear.sBlipAlphaRange.fMaxDistance = 1000.0
	INT i
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		sStructToClear.sTeamBlip[i].iBlipOverrideStartRule = -1
		sStructToClear.sTeamBlip[i].iBlipOverrideEndRule = -1
		sStructToClear.sTeamBlip[i].iBlipOverrideBitSet = 0
		sStructToClear.sTeamBlip[i].iTeamBlipBitset = 0
	ENDFOR
	PRINTLN("CLEAR_ENTITY_BLIP_DATA_STRUCT - Clearing Blip Struct")
ENDPROC

PROC CLEAR_TRAIN_ATTACH_STRUCT(TRAIN_ATTACHMENT_STRUCT& sTrainAttachData)
	sTrainAttachData.iTrainIndex = -1
	sTrainAttachData.iCarriageIndex = 0
	sTrainAttachData.iTrainAttachmentBS = 0
	sTrainAttachData.vCarriageOffsetPosition = <<0,0,0>>
	sTrainAttachData.vCarriageOffsetRotation = <<0,0,0>>
ENDPROC

PROC CLEAR_DOOR_STRUCT(FMMCselectedDoorStruct &doorStruct)
	
	INT i
	
	doorStruct.vPos = <<0.0, 0.0, 0.0>>
	doorStruct.mnDoorModel = DUMMY_MODEL_FOR_SCRIPT
	doorStruct.fDoorStartZRotation = IGNORE_ROTATION
	doorStruct.fDoorEndZRotation = IGNORE_ROTATION
	
	FOR i = 0 TO ciDOOR_CONFIG__MAX-1
		doorStruct.sDoorConfigs[i].iDoorConfigBS = 0
		doorStruct.sDoorConfigs[i].iBaseLockType = ciDOOR_BASE_LOCK_TYPE__LOCKED
		doorStruct.sDoorConfigs[i].fOpenRatio = 0.0
		doorStruct.sDoorConfigs[i].fAutomaticRate = 0.0 
		doorStruct.sDoorConfigs[i].fAutomaticDistance = 0.0
		doorStruct.sDoorConfigs[i].eDirectionalLockType = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NONE
		doorStruct.sDoorConfigs[i].vDoorUnlockDirection = <<0.0, 0.0, 0.0>>
		doorStruct.sDoorConfigs[i].iDoorModelSwapStringIndex = -1
		doorStruct.sDoorConfigs[i].iDirectionalDoorHighPriorityPed = -1
	ENDFOR
		
	doorStruct.sDoorConfigSwitchers.iConfigSwitchTeam = 0
	doorStruct.sDoorConfigSwitchers.iConfigSwitchRuleBS = 0
	doorStruct.sDoorConfigSwitchers.iConfigSwitchBS = 0
	doorStruct.sDoorConfigSwitchers.iConfigSwitchZone = -1
	doorStruct.iBitSet = 0
	doorStruct.fOriginalRotation = 0.0
	doorStruct.iContinuityId = -1
	doorStruct.iDoorAnimDynopropIndex = -1
	doorStruct.iLinkedElevator = -1
	doorStruct.iUpdateRule = -1
	doorStruct.iUpdateTeam = -1
	doorStruct.bSwingFreeUpdate = FALSE
	doorStruct.bUpdateOnMidpoint = FALSE 
	doorStruct.bLocked = FALSE 
	doorStruct.bSwingFree = FALSe
	doorStruct.iTime = 0
	doorStruct.bLockedForPlayers = FALSE
	doorStruct.fOpenRatioIfNoLinkedObj = 0.0	
	FOR i = 0 TO ciAltVarsBS_MAX-1
		doorStruct.iAltVarsBitsetDoor[i] = 0
	ENDFOR
	doorStruct.iAggroIndexBS_Entity_Door = ciMISSION_AGGRO_INDEX_DEFAULT
ENDPROC

PROC CLEAR_INTERACTABLE_CONSEQUENCES_STRUCT(FMMC_INTERACTABLE_CONSEQUENCES& sConsequences)
	sConsequences.iInteractable_StayCompleteDuration = ciInteractableDuration_StayCompleteForever
	sConsequences.iInteractable_PreReqToComplete = ciPREREQ_None
	sConsequences.iInteractable_PreReqToClear = ciPREREQ_None
	sConsequences.iInteractable_ConsequencesBS = 0
	
	sConsequences.iInteractable_StringListTickerMessage_Standard = -1
	sConsequences.iInteractable_StringListTickerMessage_CompleterOnly = -1
	
	INT iInteractableBS = 0
	FOR iInteractableBS = 0 TO FMMC_MAX_INTERACTABLE_BITSET - 1
		sConsequences.iInteractable_LinkedInteractablesBS[iInteractableBS] = 0
	ENDFOR
	
	// Attached Entity
	sConsequences.iInteractable_AttachedEntityConsequenceGenericInt = -1
	
	// Doors
	sConsequences.iInteractable_DoorsToLockBS = 0
	sConsequences.iInteractable_DoorsToUnlockBS = 0
	sConsequences.iInteractable_DoorsToSwitchConfigBS = 0
	
	sConsequences.iInteractable_CashConsequence = 0
	sConsequences.iInteractable_AttachObj = -1
	
	sConsequences.iInteractable_InventoryToGive = -1
	
	sConsequences.iInteractable_PointsForCompletion = 0
	
	sConsequences.iInteractable_HeistGearConsequence = -1
ENDPROC

PROC CLEAR_ENTITY_AUDIO_DATA(FMMC_ENTITY_AUDIO_DATA &sStruct)

	sStruct.iLoop = -1
	sStruct.iLoopSoundSet = -1
	
	sStruct.iPlayOnCreate = -1
	sStruct.iPlayOnCreateSoundSet = -1
	
	sStruct.iPlayOnDestroy = -1
	sStruct.iPlayOnDestroySoundSet = -1
	
ENDPROC

PROC CLEAR_INTERACTABLE_STRUCT(FMMC_INTERACTABLE_STRUCT& sInteractable)

	INT i
	
	sInteractable.mnInteractable_Model = DUMMY_MODEL_FOR_SCRIPT
	sInteractable.iInteractable_InteractionType = ciInteractableInteraction_None
	
	CLEAR_ENTITY_BLIP_DATA_STRUCT(sInteractable.sInteractableBlipStruct)
	
	sInteractable.vInteractable_Position = <<0,0,0>>
	sInteractable.vInteractable_Rotation = <<0,0,0>>
    sInteractable.fInteractable_Heading = 0.0
	
	sInteractable.iInteractableBS = 0
	sInteractable.fInteractable_InteractionDistance = cfInteractable_DefaultPromptDistance
	sInteractable.iInteractable_RequirePlayerInVehicleIndex = -1
	
	sInteractable.iInteractable_PreReqsRequiredCount = 0
	FOR i = 0 TO ciInteractable_PreReqsRequired_Max - 1
		sInteractable.iInteractable_PreReqsRequired[i] = 0
	ENDFOR
	
	sInteractable.iInteractable_Health = -1
	sInteractable.iInteractable_InvincibleRulesBS = 0
	sInteractable.iInteractable_InvincibleRulesTeam = -1
	
	sInteractable.iInteractable_ResetCompletionOnRuleBS = 0
	sInteractable.iInteractable_ResetCompletionOnRuleBS_Team = -1
	
	CLEAR_INTERACTABLE_CONSEQUENCES_STRUCT(sInteractable.sConsequences)
	sInteractable.iInteractable_PromptStringIndex = -1
	
	// Objective Text
	sInteractable.iInteractable_ProximityObjectiveTextIndex_Standard = -1
	sInteractable.iInteractable_ProximityObjectiveTextIndex_WaitForTeam = -1
	sInteractable.fInteractable_ProximityObjectiveTextDistance = 10.0
	
	// Associated Rule Settings
	sInteractable.iInteractable_AssociatedTeam = -1
	sInteractable.iInteractable_AssociatedRule = 0
	sInteractable.iInteractable_AssociatedRuleSpawnLimit = ciOBJECTIVE_SPAWN_LIMIT_OFF
	
	sInteractable.iInteractable_CleanupTeam = -1
	sInteractable.iInteractable_CleanupRule = 0
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		sInteractable.iInteractable_BlockInteractionRuleBS[i] = 0
	ENDFOR
	
	sInteractable.iInteractable_RequiredPlayers = 0
	
	// Attached Entity
	CLEAR_TRAIN_ATTACH_STRUCT(sInteractable.sInteractableTrainAttachmentData)
	sInteractable.iInteractable_AttachedEntityType = 0
	sInteractable.iInteractable_AttachedEntityIndex = -1
	sInteractable.iInteractable_AttachedEntityRequiredState = ciInteractable_AttachedEntityState_Any
	
	// Keycard Prop
	sInteractable.iInteractable_KeycardProp = INTERACT_WITH_KEYCARD_PROP__NONE

	// Angle Grinder Prop
	sInteractable.iInteractable_AngleGrinderProp = INTERACT_WITH_ANGLE_GRINDER_PROP__NONE
	
	// Interact-With
	sInteractable.iInteractable_InteractWithAnimPreset = ciINTERACT_WITH_PRESET__NONE
	sInteractable.iInteractable_InteractWithAltAnimSet = 0
	
	// Cut Painting
	sInteractable.iInteractable_PaintingIndex = 0
	
	// Fingerprint
	sInteractable.iInteractable_FingerprintNumberOfFailuresRequiredToDecreaseDifficulty = 0	
	sInteractable.iInteractable_FingerprintPatternsRequiredNormal = 0
	sInteractable.iInteractable_FingerprintPatternsRequiredHard = 0
	
	// Multi-Solution Lock
	sInteractable.iInteractable_SolutionAPreReq = ciPREREQ_None
	sInteractable.iInteractable_SolutionBPreReq = ciPREREQ_None
	FOR i = 0 TO ciInteractable_MultiSolutionLockSolution_MAX - 1
		sInteractable.fInteractable_MultiSolutionLock_GenericFloatOption[i] = 0.0
	ENDFOR
	
	sInteractable.iInteractable_SpawnGroup				= 0 
	sInteractable.iModifySpawnGroupOnEvent			= 0 
	sInteractable.iSpawnGroupAdditionalFunctionalityBS		= 0 
	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS - 1
		sInteractable.iInteractable_SpawnSubGroupBS[i] = 0
		sInteractable.iModifySpawnSubGroupOnEventBS[i] = 0
	ENDFOR
	
	CLEAR_WARP_LOCATION_SETTINGS(sInteractable.sWarpLocationSettings)	
	
	FOR i = 0 TO ciSPAWN_CONDITION_FLAGS_MAX - 1
		sInteractable.eSpawnConditionFlag[i] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
	
	FOR i = 0 TO ciAltVarsBS_MAX - 1
		sInteractable.iAltVarsBitsetInteractable[i] = 0
	ENDFOR
	
	sInteractable.iInteractable_Outfit = 0	
	
	sInteractable.iContinuityId = -1
	sInteractable.iContentContinuityType = 0
	
	sInteractable.iInteractable_InteriorIndex = -1
	sInteractable.iInteractable_RoomKey = -1
	
	sInteractable.iPlacedMarkerIndex = -1
	sInteractable.iAggroIndexBS_Entity_Interactable = ciMISSION_AGGRO_INDEX_DEFAULT
	
	CLEAR_ENTITY_AUDIO_DATA(sInteractable.sEntAudioData)
			
ENDPROC

#IF IS_DEBUG_BUILD
PROC CLEAR_DEV_ASSIST_STRUCT(FMMC_DEV_ASSIST_STRUCT& sDevAssist)
	sDevAssist.vPos 			= <<0.0, 0.0, 0.0>> 
	sDevAssist.fHead 			= 0.0
	sDevAssist.mn 				= DUMMY_MODEL_FOR_SCRIPT
	sDevAssist.tl31_Title 		= ""
	sDevAssist.tl31_Description = ""
ENDPROC

PROC FMMC_CLEAR_ALL_DEV_ASSIST_DATA()
	INT iDevAssist
	REPEAT FMMC_MAX_NUM_DEV_ASSIST iDevAssist
		CLEAR_DEV_ASSIST_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedDevAssist[iDevAssist])
	ENDREPEAT
	g_FMMC_STRUCT_ENTITIES.iNumberOfDevAssist = 0
ENDPROC
#ENDIF

PROC CLEAR_ENTITY_RENDER_TARGET_STRUCT(FMMC_ENTITY_RENDER_TARGET_OPTIONS& sStruct)
	sStruct.iSharedRT_DisplayOnRule = -1
	sStruct.iSharedRT_DisplayOnRuleTeam = -1
	
	sStruct.iSharedRT_GenericStartOnRule = -1
	sStruct.iSharedRT_GenericStartOnRuleTeam = -1
	
	sStruct.iSharedRT_Bitset = 0
ENDPROC

PROC CLEAR_ZONE_STRUCT(FMMC_ZONES_STRUCT& sZone)

	sZone.vPos[0]							= <<0.0,0.0,0.0>>
	sZone.vPos[1]							= <<0.0,0.0,0.0>>
	sZone.iType								= 0
	sZone.iZoneBS							= 0
	sZone.fZoneValue						= 0.0
	sZone.fZoneValue2						= 0.0
	sZone.iZoneValue3						= 0
	sZone.iZoneDelay						= 0
	sZone.fRadius							= 5.0
	sZone.fHeight							= 5.0
	sZone.iAreatype						= ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
	
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		sZone.iZoneStartPriority[iTeam] 	= -1
		sZone.iZoneEndPriority[iTeam] 	= FMMC_PRIORITY_IGNORE
	ENDFOR
	
	sZone.iZoneBS						= 0
	sZone.iZoneBS2						= 0
	
	sZone.iGoalTeam						= 0
	sZone.iGoalPoints					= 2
	
	sZone.iRadiusSecondaryActivationTimer = -1
	sZone.iRadiusSecondaryInterpTime = 0
	sZone.fGrowthPercent = 0.0
	
	sZone.iEntityLinkType = 0
	sZone.iEntityLinkIndex = -1
	sZone.iEntityLink_TrainCarriage = 0
	
	sZone.iAltEntityLinkType = 0
	sZone.iAltEntityLinkIndex = -1
	
	sZone.iBlipColor = 1
	sZone.iBlipAlpha = 120
	sZone.fBlipRange = 0.0
	
	sZone.iActivateOnPreReq = ciPREREQ_None
	sZone.iDeactivateOnPreReq = ciPREREQ_None
	
	sZone.iConnectedAntiZone = -1
	
	sZone.iZoneTimer_EnableTime = 0
	sZone.tlZoneTimer_TimerName = ""
	
	sZone.iZoneSpawnFacingType = ciFMMC_ZONE_SPAWN_FACING__DEFAULT
	sZone.iSpawnFacingEntityType = 0
	sZone.iSpawnFacingEntityIndex = -1
	sZone.vZoneSpawnFacingPosition = <<0,0,0>>
	
	INT iOverridePos
	FOR iOverridePos = 0 TO ciFMMC_ZONE_MAX_POSITIONS - 1
		sZone.vOverridePositions[iOverridePos] 	= <<0,0,0>>
	ENDFOR
	
	sZone.sPublicZoneData.fZone_Heading	= 90
	sZone.sPublicZoneData.fZone_Length	= 20
	sZone.sPublicZoneData.vZone_Centre	= <<0,0,0>>
	INITIALISE_INT_ARRAY(sZone.iAltVarsBitsetZone, 0)
	
	sZone.iZoneStartRule = -1
	sZone.iZoneEndRule = -1
	
	sZone.iZoneCustomStringListSelection = -1
	
	sZone.sPublicZoneData.vZone_Centre = <<0.0, 0.0, 0.0>>
	sZone.sPublicZoneData.fZone_Length = 20.0	
	sZone.sPublicZoneData.fZone_Heading = 90.0
	
	sZone.iSmoothZoneAttachmentOffsetSlot = -1
	sZone.fSmoothZoneAttachmentSpeed = 2.0
	
	sZone.vZoneAttachmentOffset 	= <<0,0,0>>
	sZone.fRandomOffsetRange = 0.0
		
	sZone.iZoneChance = 100
	sZone.iZoneResetTimerLength = 0	
	sZone.iSoundType = -1	
	sZone.iSpawnGroup = 0 
	INITIALISE_INT_ARRAY(sZone.iSpawnSubGroupBS, 0)
	
	INT i = 0
	FOR i = 0 TO ciSPAWN_CONDITION_FLAGS_MAX - 1
		sZone.eSpawnConditionFlag[i] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
	
	sZone.iAggroIndexBS_Entity_Zone = ciMISSION_AGGRO_INDEX_DEFAULT
ENDPROC

PROC FMMC_REMOVE_ALL_ZONES()
	INT iZone
	g_FMMC_STRUCT_ENTITIES.iNumberOfZones = 0
	FOR iZone = 0 TO (FMMC_MAX_NUM_ZONES - 1)
		CLEAR_ZONE_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone])
	ENDFOR
ENDPROC

PROC FMMC_REMOVE_ALL_OVERTIME_ZONES()
	INT i
	g_FMMC_STRUCT.iNumberOfOvertimeZones										= 0
	
	FOR i = 0 TO (ci_OVERTIME_ZONE_MAX - 1)
		g_FMMC_STRUCT.sPlacedOvertimeZones[i].vPos[0]							= <<0.0,0.0,0.0>>
		g_FMMC_STRUCT.sPlacedOvertimeZones[i].vPos[1]							= <<0.0,0.0,0.0>>
		g_FMMC_STRUCT.sPlacedOvertimeZones[i].iBitset							= 0
		g_FMMC_STRUCT.sPlacedOvertimeZones[i].iPointsToGive						= 0
		g_FMMC_STRUCT.sPlacedOvertimeZones[i].fRadiusWidth						= 0
		g_FMMC_STRUCT.sPlacedOvertimeZones[i].iPriorityLayer					= 0
	ENDFOR
ENDPROC

PROC CLEAR_PLAYER_WEAPON_INVENTORY(FMMC_WEAPON_INVENTORY_STRUCT &sStructToClear)
	sStructToClear.iStartWeapon = FMMC_STARTING_WEAPON_CURRENT
	sStructToClear.iInventoryBitset = 0	
	sStructToClear.iRandomWeaponBitSet = 0
	sStructToClear.iAmmoDropTypeOverride = FMMC_PED_AMMO_DROP_TYPE_INVALID
	sStructToClear.eArmour = eFMMC_ARMOUR_RETAIN
	INT i, ii
	FOR i = 0 TO FMMC_MAX_INVENTORY_WEAPONS-1
		sStructToClear.sWeaponStruct[i].wtWeapon = WEAPONTYPE_INVALID
		sStructToClear.sWeaponStruct[i].iAmmo = 0
		sStructToClear.sWeaponStruct[i].iTint = -1
		sStructToClear.sWeaponStruct[i].iLiveryTint = -1
		FOR ii = 0 TO FMMC_MAX_WEAPON_COMPONENTS-1	
			sStructToClear.sWeaponStruct[i].eWeaponComponents[ii] = WEAPONCOMPONENT_INVALID
		ENDFOR
	ENDFOR	
	sStructToClear.sParachute.iCount = 0
	sStructToClear.sParachute.iTint = -1
	sStructToClear.sParachute.iPackTint = -1
ENDPROC

PROC CLEAR_PED_MODEL_SWAP_DATA(INT iTeam)
	INT i
	FOR i = 0 TO FMMC_PLAYER_PED_MODELS_MAX-1
		g_FMMC_STRUCT.sTeamData[iTeam].iPlayerPedModels[i] = -1
	ENDFOR
ENDPROC

PROC CLEAR_TEAM_DATA(INT iTeam)
	g_FMMC_STRUCT.sTeamData[iTeam].iStartingInventoryIndex = -1
	g_FMMC_STRUCT.sTeamData[iTeam].iMidMissionInventoryIndex = -1
	g_FMMC_STRUCT.sTeamData[iTeam].iMidMissionInventory2Index = -1
	g_FMMC_STRUCT.sTeamData[iTeam].iTeamBitSet = 0
	g_FMMC_STRUCT.sTeamData[iTeam].iLobbyInventoriesToSelectFrom = 0
	CLEAR_PED_MODEL_SWAP_DATA(iTeam)
ENDPROC

PROC CLEAR_POWERUP_DATA()
	g_FMMC_STRUCT.sPowerupData.iPWROptionsBitSet = 0
	g_FMMC_STRUCT.sPowerupData.iPWRMaxHeld = 1
	INT i
	FOR i = 0 TO FMMC_POWERUP_TYPES_MAX-1 
		g_FMMC_STRUCT.sPowerupData.iPWRRandomChance[i] = 0
	ENDFOR
ENDPROC

PROC CLEAR_DM_WEAPON_DAMAGE_MODIFIERS()
	INT iWeapon
	FOR iWeapon = 0 TO ciFMMC_VWDM_MAX-1
		g_FMMC_STRUCT.sWeaponDamageModifiers.fVehicleWeaponDamageModifier[iWeapon] = 1.0
	ENDFOR
ENDPROC

PROC CLEAR_DM_MODIFIER_DATA_STRUCT(DM_MODIFIER_SET_STRUCT &sModSet)
	
	sModSet.iModifierSetBS = 0
	
	sModSet.sModifierSetup.eApplyType = DM_APPLY__SPAWN
	sModSet.sModifierSetup.iTeamBS = 0
	sModSet.sModifierSetup.eConditionType = DM_CONDITION__DEFAULT
	sModSet.sModifierSetup.eWhoType = DM_WHO__NOT_SET
	sModSet.sModifierSetup.ePositionType = DM_POSITION__ANY
	sModSet.sModifierSetup.eScoreCondition = DM_SCORE_GREATER_THAN_EQUAL_TO
	sModSet.sModifierSetup.iScoreConditionToCheck = 0
	sModSet.sModifierSetup.iScorePercentage = 0
	sModSet.sModifierSetup.iTimerConditionLength = 0
	sModSet.sModifierSetup.iTimerPercentage = 0
	
	INITIALISE_INT_ARRAY(sModSet.iWeaponGroupDamageModifier, ciDM_WEAPON_DAMAGE_MODIFIER_DEFAULT)
	sModSet.eInfiniteAmmoType = DM_INFINITE_AMMO_OFF
	
	sModSet.sModifierScoring.iKill = ciDM_DEFAULT_KILL_SCORE
	sModSet.sModifierScoring.iHeadshot = ciDM_DEFAULT_HEADSHOT_SCORE
	sModSet.sModifierScoring.iMelee = ciDM_DEFAULT_MELEE_SCORE
	sModSet.sModifierScoring.iVehicle = ciDM_DEFAULT_VEHICLE_SCORE
	
	INITIALISE_INT_ARRAY(sModSet.sModifierScoring.iSpecificTeam, ciDM_DEFAULT_SPECIFIC_TEAM_SCORE)
	INITIALISE_INT_ARRAY(sModSet.sModifierScoring.iSpecificWeaponGroup, ciDM_DEFAULT_SPECIFIC_WEAPON_GROUP_SCORE)
	
	sModSet.sModifierScoring.iFriendlyKill = ciDM_DEFAULT_FRIENDLY_KILL_SCORE
	sModSet.sModifierScoring.iSuicide = ciDM_DEFAULT_SUICIDE_SCORE
	sModSet.sModifierScoring.iOutOfBounds = ciDM_DEFAULT_OUT_OF_BOUNDS_SCORE
	
	CLEAR_PLAYER_WEAPON_INVENTORY(sModSet.sModifierInventory)
	
	SET_BIT(sModSet.sModifierInventory.iInventoryBitset, ciFMMC_INVENTORY_BS_REMOVE_WEAPONS)
	
	sModset.iHealthScale = ciDM_HEALTH_MODIFIER_DEFAULT
	sModset.iStartingHealthPercentage = ciDM_HEALTH_MODIFIER_DEFAULT
	sModset.eExplosiveResistance = DM_EXPR_OFF
	
	sModSet.sModifierBlips.eBlipType = DM_BLIP_TYPE__DEFAULT
	sModSet.sModifierBlips.iCampTimer = ciDM_DEFAULT_CAMP_TIMER
	
	sModSet.sModifierMovement.eMovementRestriction = DM_MOVEMENT_RESTRICTION__DEFAULT
	sModSet.sModifierMovement.eMovementSpeed = DM_MOVEMENT_SPEED__DEFAULT
	
	sModSet.sModifierOnKill.iExtraHealth = ciDM_DEFAULT_EXTRA_HEALTH
	sModSet.sModifierOnKill.eAmmoType = DM_AMMO_TYPE__BULLETS
	sModSet.sModifierOnKill.iGiveAmmoTo = ciDM_GIVE_AMMO_TO_OFF
	sModSet.sModifierOnKill.iAmmoQuantity = ciDM_DEFAULT_AMMO_QUANTITY
	sModSet.sModifierOnKill.iExtraArmour = ciDM_DEFAULT_EXTRA_ARMOUR
	
	sModSet.sModifierOnHit.iExtraHealth = ciDM_DEFAULT_EXTRA_HEALTH
	sModSet.sModifierOnHit.eAmmoType = DM_AMMO_TYPE__BULLETS
	sModSet.sModifierOnHit.iGiveAmmoTo = ciDM_GIVE_AMMO_TO_OFF
	sModSet.sModifierOnHit.iAmmoQuantity = ciDM_DEFAULT_AMMO_QUANTITY
	sModSet.sModifierOnHit.iExtraArmour = ciDM_DEFAULT_EXTRA_ARMOUR
	
	sModSet.mnRespawnVehicle = DUMMY_MODEL_FOR_SCRIPT
	
	sModSet.iBoundsIndex = -1

ENDPROC

PROC SET_DEFAULT_CUSTOM_GAME_SETTINGS_DEFAULTS()
	g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[ciDM_DEFAULT_MODIFIER_SET].sModifierSetup.eApplyType = DM_APPLY__SPAWN
	g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[ciDM_DEFAULT_MODIFIER_SET].sModifierSetup.eConditionType = DM_CONDITION__DEFAULT
	g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[ciDM_DEFAULT_MODIFIER_SET].sModifierSetup.eWhoType = DM_WHO__ALL_PLAYERS
	g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[ciDM_DEFAULT_MODIFIER_SET].sModifierSetup.iTeamBS = 0
ENDPROC

PROC CLEAR_DM_CUSTOM_DATA()
	
	g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings = 1
	
	g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset = 0
	g_FMMC_STRUCT.sDMCustomSettings.iFFALives = 1
	
	g_FMMC_STRUCT.sDMCustomSettings.iWinBiasTeam = -1
	
	INT iModifierLoop
	
	INITIALISE_INT_ARRAY(g_FMMC_STRUCT.sDMCustomSettings.iTeamLives, 1)
	INITIALISE_INT_ARRAY(g_FMMC_STRUCT.sDMCustomSettings.iTeamTargetScore, 0)
	
	FOR iModifierLoop = 0 TO ciDM_MAX_MODIFIER_SETS-1
		CLEAR_DM_MODIFIER_DATA_STRUCT(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModifierLoop])
	ENDFOR
	
	SET_DEFAULT_CUSTOM_GAME_SETTINGS_DEFAULTS()
	
ENDPROC

PROC CLEAR_CARNAGE_BAR_SETTINGS(CARNAGE_BAR_STRUCT &sCarnageBarPassedIn)
	CARNAGE_BAR_STRUCT sCarnageBarReset
	sCarnageBarPassedIn = sCarnageBarReset
	
	INT i
	FOR i = 0 TO MAX_CARNAGE_BAR_CREATOR_VEHS-1
		g_FMMC_STRUCT.sCarnageBar.iCreatorVehicles[i] = -1
	ENDFOR
ENDPROC

PROC CLEAR_MISSION_VARIATION_STRUCT(FMMC_MISSION_VARIATION_STRUCT &sMissionVariationPassedIn)
	// Default some stuff	
	sMissionVariationPassedIn.iVarBSOne = 0	
	
	sMissionVariationPassedIn.eMCVarConfigEnum = FMMC_MC_VAR_CONFIG_ENUM_OFF
	INT ii = 0
	INT i = 0
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_ENEMY_HEALTH-1
		sMissionVariationPassedIn.iVarEnemyHealthPercent[i] = 0
	ENDFOR
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_ENEMY_INVENTORY-1
		sMissionVariationPassedIn.iVarEnemyInventoryIndex[i] = -1
	ENDFOR
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_VEHICLE_MODEL_SWAP-1
		sMissionVariationPassedIn.iVarVehicleModelSwapIndex[i] = 0
	ENDFOR
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_SPAWN_GROUP_FORCING_OPTIONS-1
		FOR ii = 0 TO FMMC_MAX_MC_VAR_SIZE_SPAWN_GROUP_FORCING-1
			sMissionVariationPassedIn.iVarMissionSpawnGroupForceIndex[i][ii] = -1
		ENDFOR
	ENDFOR
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
		sMissionVariationPassedIn.eVarMissionTimerTypeEnum[i] = FMMC_MC_VAR_TIMER_TYPE_OFF
	ENDFOR	
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
		sMissionVariationPassedIn.iVarMissionTimerRuleFrom[i] = -1
	ENDFOR	
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
		sMissionVariationPassedIn.iVarMissionTimerRuleTo[i] = -1
	ENDFOR
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
		sMissionVariationPassedIn.iVarMissionTimerZoneIndex[i] = -1
	ENDFOR	
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
		FOR ii = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE_SELECTION-1
			sMissionVariationPassedIn.iVarMissionTimerModifier[i][ii] = 0
		ENDFOR
	ENDFOR
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_OBJECT_MODEL_SWAP-1
		sMissionVariationPassedIn.sObjectSwap[i].mnVarObjectSwap = DUMMY_MODEL_FOR_SCRIPT
		sMissionVariationPassedIn.sObjectSwap[i].iMinigameType = -1
		sMissionVariationPassedIn.sObjectSwap[i].iMinigameSubType = -1
	ENDFOR
	sMissionVariationPassedIn.iVarJumpToObjectiveEntityType = ciMCVAR_JUMP_TO_OBJECTIVE_ENTITY_TYPE_LOCATION
	FOR i = 0 TO ciMCVAR_JUMP_TO_OBJECTIVE_MAX_OVERRIDES-1
		FOR ii = 0 TO FMMC_MAX_TEAMS-1
			sMissionVariationPassedIn.iVarJumpToObjectivePass[i][ii] = 0
			sMissionVariationPassedIn.iVarJumpToObjectiveFail[i][ii] = 0
		ENDFOR
	ENDFOR
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_END_CUTSCENE_OVERRIDE-1
		sMissionVariationPassedIn.iVarMissionEndCutsceneOverride[i] = 0
	ENDFOR
	FOR i = 0 TO FMMC_MAX_MC_OBJ_TEXT_OVERRIDES - 1
		sMissionVariationPassedIn.tl63PrimaryObjectiveOverride[i] = ""
	ENDFOR
ENDPROC

PROC CLEAR_SHRINKING_SUDDEN_DEATH_AREA()
	g_FMMC_STRUCT.vSphereSpawnPoint = <<0,0,0>>
	g_FMMC_STRUCT.fSphereStartRadius = 30.0
	g_FMMC_STRUCT.fSphereEndRadius = 5.0
	g_FMMC_STRUCT.iSphereShrinkTime = 30000
	g_FMMC_STRUCT.iTimeToGetInsideSphere = 15000
ENDPROC

PROC CLEAR_SUDDEN_DEATH_SPHERE()
	g_FMMC_STRUCT.vSphereSpawnPoint = <<0,0,0>>
	g_FMMC_STRUCT.fSphereStartRadius = 30.0
	g_FMMC_STRUCT.iTimeToGetInsideSphere = 15000
ENDPROC

PROC CLEAR_GUN_ROULETTE_STRUCT(RULE_GROULETTE_STRUCT &sGRStruct)
	sGRStruct.iWeaponActualIndex = -1
	sGRStruct.iClips			 = 0
	sGRStruct.iKillsRequired	 = 0
ENDPROC

PROC FMMC_REMOVE_ALL_KOTH_HILLS()
	INT iHill
	
	FOR iHill = 0 TO ciKotH_MAX_HILLS - 1
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].iHillType = ciKOTH_HILL_TYPE__CIRCLE
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].iHillEntity = -1
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].iHillBS = 0
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].iHill_MinimumPlayers = 0
		
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].vHill_Center = <<0,0,0>>
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].vHill_Rotation = <<0,0,0>>
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].vHill_UpVector = <<0,0,0>>
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].fHill_Radius = 10.0
		
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].fHill_Width = 10.0
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].fHill_Length = 20.0
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].fHill_Height = 3.0
		
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].fHill_Heading = 90.0
		
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].fHill_CaptureTime = 3.0
		g_FMMC_STRUCT.sKotHData.sHillData[iHill].fHill_PointsPerTick = 1.0
	ENDFOR
	
	g_FMMC_STRUCT.sKotHData.iNumberOfHills = 0
ENDPROC

PROC CLEAR_RULE_BOUNDS(RULE_BOUNDS_STRUCT& sRuleBounds)
	INT ii
	sRuleBounds.iRuleBoundsType = ciRULE_BOUNDS_TYPE__OFF
	FOR ii = 0 TO FMMC_ZONES_LONG_BS_LENGTH-1
		sRuleBounds.iZonesToUseBS[ii] = 0
	ENDFOR
	sRuleBounds.iRuleBoundsTime = 10000	
	sRuleBounds.iOutOfBoundsFailStringIndex = -1
	sRuleBounds.MNIgnoreWantedVeh = DUMMY_MODEL_FOR_SCRIPT
	sRuleBounds.iIgnoreWantedOutfit = -1
	sRuleBounds.iWantedToGive = -1
	sRuleBounds.iBoundsBS = 0
	sRuleBounds.iLinkedSpawnGroup = 0	
	sRuleBounds.iBoundsBlipHeightThreshold = 0	
	sRuleBounds.iMaxPlayersAllowedInBounds = 0	
	sRuleBounds.iVehicleRegenAndCap = 0	
	sRuleBounds.vHeadingVec = <<0.0, 0.0, 0.0>> 
	sRuleBounds.MNIgnoreLeaveAreaVeh = DUMMY_MODEL_FOR_SCRIPT
	sRuleBounds.mnOnlyAffectVeh = DUMMY_MODEL_FOR_SCRIPT
	sRuleBounds.iBoundsBlipStyle = ciRULE_BOUNDS_BLIP_STYLE__AVERAGE_POSITION
ENDPROC

PROC FMMC_REMOVE_ALL_BOUNDS()
	
	PRINTLN("[LH][BOUNDSGONE] FMMC_REMOVE_ALL_BOUNDS ")
		
	INT i, j
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		FOR j = 0 TO FMMC_MAX_RULES-1
		
			INT iBoundsIndex
			FOR iBoundsIndex = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
				CLEAR_RULE_BOUNDS(g_FMMC_STRUCT.sFMMCEndConditions[i].sRuleBounds[j][iBoundsIndex])
			ENDFOR
		
			// LEGACY
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[j].iType = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[j].vPos = <<0,0,0>>
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[j].vPos1 = <<0,0,0>>
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[j].vPos2 = <<0,0,0>>
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[j].fWidth = 10
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[j].fRadius = 10
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[j].fMaxHeight = 2
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[j].fMinHeight = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[j].iMinimapAlpha = 120
		
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[j].iType = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[j].vPos = <<0,0,0>>
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[j].vPos1 = <<0,0,0>>
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[j].vPos2 = <<0,0,0>>
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[j].fWidth = 10
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[j].fRadius = 10
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[j].fMaxHeight = 2
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[j].fMinHeight = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[j].iMinimapAlpha = 120
		ENDFOR
	ENDFOR
ENDPROC

PROC CLEAR_FILTER_CONTROLS_DATA(FMMC_FILTER_CONTROLS_STRUCT &sFilterControl)
	
	sFilterControl.iFilterFadeInDuration = 0
	sFilterControl.iFilterFadeInDuration = 0
	sFilterControl.fFilterMaxStrength = 0.0
	sFilterControl.iFilterScoreTarget = 0
	sFilterControl.iFilterControlBitSet = 0
	sFilterControl.iFilterAccelOutType = FMMC_FILTER_ACCEL_OFF
	sFilterControl.iFilterAccelOutTime = FMMC_FILTER_ACCELERATION_TIME_DEFAULT
ENDPROC

PROC CLEAR_ON_RULE_START_DATA(FMMC_ON_RULE_START_STRUCT &sOnRuleStartData)
	FMMC_ON_RULE_START_STRUCT sEmpty
	sOnRuleStartData = sEmpty
ENDPROC

PROC FMMC_CLEAR_ALL_DOOR_DATA()
	INT i
	PRINTLN("FMMC_CLEAR_ALL_DOOR_DATA - START")
	REPEAT FMMC_MAX_NUM_DOORS i
		CLEAR_DOOR_STRUCT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i])
		PRINTLN("FMMC_CLEAR_ALL_DOOR_DATA - Cleared Door ", i)
	ENDREPEAT
	g_FMMC_STRUCT_ENTITIES.iNumberOfDoors = 0
ENDPROC

PROC FMMC_CLEAR_ALL_INTERACTABLE_DATA()
	PRINTLN("[Interactables] FMMC_CLEAR_ALL_INTERACTABLE_DATA")
	
	INT iInteractable
	REPEAT FMMC_MAX_NUM_INTERACTABLES iInteractable
		CLEAR_INTERACTABLE_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable])
		//PRINTLN("[Interactables][Interactable ", iInteractable, "] FMMC_CLEAR_ALL_INTERACTABLE_DATA - Cleared Interactable ", iInt)
	ENDREPEAT
	
	INT iInteractableCounter
	REPEAT ciInteractable_InteractionCounters iInteractableCounter
		g_FMMC_STRUCT_ENTITIES.sInteractionCounters[iInteractableCounter].iInteractionCounter_HUDStringIndex = -1
		g_FMMC_STRUCT_ENTITIES.sInteractionCounters[iInteractableCounter].iInteractionCounter_InteractionType = ciInteractableInteraction_None
	ENDREPEAT
	
	CLEAR_INTERACTABLE_STRUCT(g_FMMC_STRUCT_ENTITIES.sEditedInteractable)
	g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables = 0
ENDPROC

PROC CLEAR_CURRENT_MISSION_TAGS()
	INT i
	PRINTLN("CLEAR_CURRENT_MISSION_TAGS")
	FOR i = 0 TO (NUM_TEXTLABELS_IN_UGC_DESCRIPTION - 1)
		g_FMMC_STRUCT.szTags.TextLabel[i] = ""
	ENDFOR
ENDPROC

PROC CLEAR_DEFAULT_VEHICLES_PER_TYPE()
	INT i
	PRINTLN("CLEAR_DEFAULT_VEHICLES_PER_TYPE")
	FOR i = 0 TO VEHICLE_LIBRARY_MAX - 1
		g_FMMC_STRUCT.iDefaultCommonVehicleForType[i] = 0
	ENDFOR
ENDPROC

PROC CLEAR_CURRENT_MISSION_DESCRIPTION()
	INT i
	PRINTLN("CLEAR_CURRENT_MISSION_DESCRIPTION")
	FOR i = 0 TO (NUM_TEXTLABELS_IN_UGC_DESCRIPTION - 1)
		g_FMMC_STRUCT.tl63MissionDecription[i] = ""
	ENDFOR
ENDPROC
PROC FMMC_REMOVE_ALL_RESPAWNS()
	INT i
	FMMC_REMOVE_ALL_BOUNDS()
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fWidth = 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[0].fRadius = 0
	ENDFOR
ENDPROC

PROC COPY_DIALOGUE_TRIGGER_DATA(FMMCDialogueTriggersStruct &sTarget, FMMCDialogueTriggersStruct &sSource, BOOL bSourceMidPoint, INT iTarget)
	
	IF DOES_BLIP_EXIST(sTarget.biBlip)
		REMOVE_BLIP(sTarget.biBlip)
	ENDIF	
	
	sTarget = sSource
		
	CLEAR_BIT(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[iTarget/32], iTarget%32)
	IF bSourceMidPoint
		SET_BIT(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[iTarget/32], iTarget%32)
	ENDIF
	
	IF DOES_BLIP_EXIST(sSource.biBlip)
		REMOVE_BLIP(sSource.biBlip)
	ENDIF
	
ENDPROC

PROC RESET_ELEVATOR_STRUCT(INT iElevatorIndex)
	g_FMMC_STRUCT.sElevators[iElevatorIndex].iLinkedElevator = -1
	g_FMMC_STRUCT.sElevators[iElevatorIndex].vActivationZonePosition[0] = <<0.0, 0.0, 0.0>>
	g_FMMC_STRUCT.sElevators[iElevatorIndex].vActivationZonePosition[1] = <<0.0, 0.0, 0.0>>
	g_FMMC_STRUCT.sElevators[iElevatorIndex].fActivationZoneWidth = 0.0
	
	g_FMMC_STRUCT.sElevators[iElevatorIndex].vLiftCallPosition = <<0.0, 0.0, 0.0>>
	
	g_FMMC_STRUCT.sElevators[iElevatorIndex].sCamera.vRotation = <<0.0, 0.0, 0.0>>
	g_FMMC_STRUCT.sElevators[iElevatorIndex].sCamera.vPosition = <<0.0, 0.0, 0.0>>
	g_FMMC_STRUCT.sElevators[iElevatorIndex].sCamera.fFOV = 0.0
	g_FMMC_STRUCT.sElevators[iElevatorIndex].sCamera.iDuration = 0
	
	INT iElevatorPlayer = 0
	FOR iElevatorPlayer = 0 TO FMMC_MAX_ELEVATOR_PLAYERS - 1
		g_FMMC_STRUCT.sElevators[iElevatorIndex].vPlayerWarpPositions[iElevatorPlayer] = <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.sElevators[iElevatorIndex].fPlayerWarpHeadings[iElevatorPlayer] = 0.0
	ENDFOR
	
	g_FMMC_STRUCT.sElevators[iElevatorIndex].iTeam = -1
	g_FMMC_STRUCT.sElevators[iElevatorIndex].iEnabledFromRule = -1
	g_FMMC_STRUCT.sElevators[iElevatorIndex].iEnabledToRule = -1

	g_FMMC_STRUCT.sElevators[iElevatorIndex].iElevatorBitset = 0
	
ENDPROC

PROC CLEAR_DIALOGUE_TRIGGER_DATA(FMMCDialogueTriggersStruct &sDialogueTrigger, INT iBit = -1)

	sDialogueTrigger.iDialogueID			= -1
	sDialogueTrigger.iFileID				= -1
	sDialogueTrigger.tlDialogueTriggerAdditionalHelpText = ""
	sDialogueTrigger.iRule					= -1
	sDialogueTrigger.iTeam					= -1
	sDialogueTrigger.iPlayForRoleBS			= 0
	sDialogueTrigger.iTimeDelay				= 0
	sDialogueTrigger.vPosition				= <<0,0,0>>
	sDialogueTrigger.fRadius				= 3.0
	sDialogueTrigger.iDialogueBitset		= 0
	sDialogueTrigger.iDialogueBitset2		= 0
	sDialogueTrigger.iDialogueBitset3		= 0
	sDialogueTrigger.iDialogueBitset4		= 0
	sDialogueTrigger.iDialogueBitset5		= 0
	sDialogueTrigger.iDialogueBitset6		= 0
	sDialogueTrigger.iDialogueBitset7		= 0
	sDialogueTrigger.iDialogueBitset8		= 0
	sDialogueTrigger.iPoint					= -1
	sDialogueTrigger.iPriority 				= 0
	sDialogueTrigger.iPedVoice				= -1
	sDialogueTrigger.iSecondPedVoice		= -1
	sDialogueTrigger.iPedAnimation			= 0
	sDialogueTrigger.iVehMinigameIndex		= -1
	sDialogueTrigger.iPrerequisite			= -1
	sDialogueTrigger.iVehicleTrigger 		= -1
	sDialogueTrigger.iVehicleDamagerTrigger = -1
	sDialogueTrigger.iVehicleDamage  		= 0	
	
	INITIALISE_INT_ARRAY(sDialogueTrigger.iPedInRangeTrigger, -1)
	
	sDialogueTrigger.iCrowdControlKills 	= 0
	sDialogueTrigger.iSpecificVehicle		= -1
	sDialogueTrigger.iSpecificVehicle2		= -1
	sDialogueTrigger.iSpecificVehicle3		= -1
	sDialogueTrigger.iSpecificVehicle4		= -1
	sDialogueTrigger.iSpecificVehicle5		= -1
	sDialogueTrigger.iSkipOnRestart			= 0
	sDialogueTrigger.iFocusPed 				= -1
	sDialogueTrigger.iPlayerCountRequirement = 0
	sDialogueTrigger.iPlayerCountComparison = ciDT_PLAYER_COUNT_COMPARISON_EQUALS
	sDialogueTrigger.iPlayerCountType = ciDT_PLAYER_COUNT_TYPE_CURRENT
	sDialogueTrigger.iGangChasePedsToKill	= -1
	sDialogueTrigger.iVehicleHacked			= 0
	sDialogueTrigger.iPedKilled			= -1
	sDialogueTrigger.iWaterDistance			= 0
	sDialogueTrigger.iFiresExtinguished		= -1
	sDialogueTrigger.iMultiruleTimeToReach	= -1
	sDialogueTrigger.tlBlock				= ""
	sDialogueTrigger.tlRoot					= ""
	sDialogueTrigger.iVehicleModel 			= 0
	sDialogueTrigger.iPedTrigger	 		= -1
	sDialogueTrigger.iPedDamage		  		= 0
	sDialogueTrigger.iPedFleeType			= 0
	sDialogueTrigger.iCallCharacter		  	= -1
	sDialogueTrigger.iRequiredCompletedInteractable	= -1
	sDialogueTrigger.iRequiredCompletedInteractionType = ciInteractableInteraction_None
	sDialogueTrigger.iRequiredUncompletedInteractable = -1
	sDialogueTrigger.iRequiredInUseInteractable = -1
	sDialogueTrigger.iMissionEquipmentRequiredType = ciCUSTOM_MISSIONEQUIPMENT_MODEL__NONE
	sDialogueTrigger.iMissionEquipmentRequiredCount = 0
	sDialogueTrigger.iRequiredExtraCashTake = 0
	sDialogueTrigger.tl3AppendOnAggro			= ""
	sDialogueTrigger.tlCustomVoice				= ""
	sDialogueTrigger.tlAppendCustomCharacter	= ""
	sDialogueTrigger.iPlayRandomVariationCap	=	0
	sDialogueTrigger.iPlaythroughAppendCap	=	0
	sDialogueTrigger.iZoneIndex				= -1
	sDialogueTrigger.iTriggeredByZoneBS		= 0
	sDialogueTrigger.iBlockedByZoneBS		= 0
	sDialogueTrigger.iCCTVsDestroyed		= 0
	sDialogueTrigger.iPedsKillRequired 		= 0
	sDialogueTrigger.iCivillianKillRequired = 0
	sDialogueTrigger.iEntityExistsType		= 0
	sDialogueTrigger.iEntityExistsIndex		= -1
	sDialogueTrigger.iCashGrabLowerLimit    = 0
	sDialogueTrigger.iCashGrabUpperLimit    = 0
	sDialogueTrigger.iLinkedWarpPortalStart    = -1
	sDialogueTrigger.iLinkedWarpPortalEnd    = -1
	sDialogueTrigger.iZoneIndex					= -1
	sDialogueTrigger.iZoneTimer_TimeRemaining	= -1
	sDialogueTrigger.iTriggerOutOfBounds = ciRULE_BOUNDS_INDEX__NONE
	sDialogueTrigger.fTriggerInCCTVRange = 0.0
	sDialogueTrigger.iDrillBreakLock		= -1
	sDialogueTrigger.iDrillBreakLockObjIndex = -1
	sDialogueTrigger.iContinuityId = -1
	sDialogueTrigger.iLinkedObjectContinuityId = -1
	sDialogueTrigger.iLinkedLocationContinuityId = -1
	sDialogueTrigger.iWeatherType = 0
	sDialogueTrigger.iRangeCheckType = -1
	sDialogueTrigger.fRangeCheckRange = 0.0
	sDialogueTrigger.fTriggerInCCTVRange 	= 0.0
	sDialogueTrigger.iVoltageMinigameFailuresRequired = 0
	sDialogueTrigger.iVoltageMinigameExitsRequired = 0
	sDialogueTrigger.iFingerprintPatternsComplete = 0
	sDialogueTrigger.iFingerprintPatternsFailed = 0
	sDialogueTrigger.iFingerprintExitsRequired = 0
	sDialogueTrigger.iLastDialogueTimerLength = 0
	sDialogueTrigger.iRequireCrashIntoSpecificVehicle = -1
	sDialogueTrigger.iPropertyTrigger = -1
	sDialogueTrigger.iWeaponTypeIndexTrigger = -1
	sDialogueTrigger.iWeaponLastFiredTriggerTime = -1
	sDialogueTrigger.iRangeCheckTeam = -1
	sDialogueTrigger.iInteractWithObj = -1
	sDialogueTrigger.fDistanceFromCachedVeh = -1.0
		
	sDialogueTrigger.iTeamToCollectObject = 0
	sDialogueTrigger.iObjectToCollect = -1
	
	sDialogueTrigger.iCoughPlayerID = -1
	sDialogueTrigger.iCoughDialogueLineID = 0
	
	sDialogueTrigger.iCustomStringTicker = -1
	
	sDialogueTrigger.iHintCamEntityType = -1
	sDialogueTrigger.iHintCamEntityIndex = -1
	sDialogueTrigger.iHintCamTime = ciFMMC_DEFAULT_FORCED_HINT_CAM_LENGTH
	
	INITIALISE_INT_ARRAY(sDialogueTrigger.iAltVarsBitsetDialogueTrigger, 0)
	INT i
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		sDialogueTrigger.iTeamRuleLimit[i] = -1
	ENDFOR
	
	FOR i = 0 TO DT_CASINO_REQ_MAX-1
		sDialogueTrigger.iVarRequirement[i]		= 0
	ENDFOR
	
	FOR i = 0 TO ciMAX_DIALOGUE_BIT_SETS-1
		sDialogueTrigger.iBlockingTriggers[i]	= 0
	ENDFOR
	
	FOR i = 0 TO ciDIALOGUE_MAX_PREREQS - 1
		sDialogueTrigger.iDialogueRequiredPrereqs[i] = ciPREREQ_None
		sDialogueTrigger.iDialogueBlockerPrereqs[i] = ciPREREQ_None
	ENDFOR
	sDialogueTrigger.iCompletePrereqOnStart = ciPREREQ_None
	sDialogueTrigger.iCompletePrereqOnFinish = ciPREREQ_None
	
	sDialogueTrigger.iSpawnGroup = 0
	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		sDialogueTrigger.iSpawnSubGroupBS[i] = 0
	ENDFOR
	
	IF (iBit != -1)
		CLEAR_BIT(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[iBit/32], iBit%32)
	ENDIF
	
	IF DOES_BLIP_EXIST(sDialogueTrigger.biBlip)
		REMOVE_BLIP(sDialogueTrigger.biBlip)
	ENDIF
	
	FOR i = 0 TO ciSPAWN_CONDITION_FLAGS_MAX - 1
		sDialogueTrigger.eSpawnConditionFlag[i] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
	
	INITIALISE_INT_ARRAY(sDialogueTrigger.iDialoguePreReqCounterBS, 0)
	sDialogueTrigger.iDialogueRequiredPreReqCounterCount = 0
	
	sDialogueTrigger.iAggroIndexBS_Entity_Dialog = ciMISSION_AGGRO_INDEX_DEFAULT
	sDialogueTrigger.iObjectModelToTriggerStringID = -1
	sDialogueTrigger.iCamShotTrigger = -1
ENDPROC

PROC FMMC_REMOVE_ALL_DIALOGUE_TRIGGERS()
	INT i
	PRINTLN("FMMC_REMOVE_ALL_DIALOGUE_TRIGGERS - START")
	REPEAT FMMC_MAX_DIALOGUES i
		CLEAR_DIALOGUE_TRIGGER_DATA(g_FMMC_STRUCT.sDialogueTriggers[i], i)
		PRINTLN("FMMC_REMOVE_ALL_DIALOGUE_TRIGGERS - Cleared Dialogue Trigger ", i)
	ENDREPEAT
	g_FMMC_STRUCT.iNumberOfDialogueTriggers = 0
ENDPROC

PROC MOVE_DIALOGUE_TRIGGER_ARRAY_DOWN_ONE(INT iStart)
	PRINTLN("PD MOVE_DIALOGUE_TRIGGER_ARRAY_DOWN_ONE starting at ", iStart)
	INT i
	INT iLoopLimit = (g_FMMC_STRUCT.iNumberOfDialogueTriggers - 2)
	FOR i = iStart TO iLoopLimit
		COPY_DIALOGUE_TRIGGER_DATA(g_FMMC_STRUCT.sDialogueTriggers[i] , g_FMMC_STRUCT.sDialogueTriggers[i+1], IS_BIT_SET(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[(i+1)/32], (i+1)%32), i)
	ENDFOR
	g_FMMC_STRUCT.iNumberOfDialogueTriggers--
	CLEAR_DIALOGUE_TRIGGER_DATA(g_FMMC_STRUCT.sDialogueTriggers[g_FMMC_STRUCT.iNumberOfDialogueTriggers], g_FMMC_STRUCT.iNumberOfDialogueTriggers)
ENDPROC

PROC COPY_SHOT_DATA(FMMC_CUTSCENE_DATA &sScriptedCutsceneData, INT iSource, INT iTarget)
	IF iSource >= 0 AND iSource < MAX_CAMERA_SHOTS
	AND iTarget >= 0 AND iTarget < MAX_CAMERA_SHOTS
		sScriptedCutsceneData.iInterpTime[iTarget] 			= sScriptedCutsceneData.iInterpTime[iSource] 	
		sScriptedCutsceneData.fOverrideRotMin[iTarget] 		= sScriptedCutsceneData.fOverrideRotMin[iSource]
		sScriptedCutsceneData.fOverrideRotMax[iTarget] 		= sScriptedCutsceneData.fOverrideRotMax[iSource]
		sScriptedCutsceneData.CamType[iTarget]	 			= sScriptedCutsceneData.CamType[iSource]	 	
		sScriptedCutsceneData.vCamStartPos[iTarget]			= sScriptedCutsceneData.vCamStartPos[iSource]	
		sScriptedCutsceneData.vCamEndPos[iTarget]			= sScriptedCutsceneData.vCamEndPos[iSource]		
		sScriptedCutsceneData.vCamStartRot[iTarget]			= sScriptedCutsceneData.vCamStartRot[iSource]	
		sScriptedCutsceneData.vCamEndRot[iTarget]			= sScriptedCutsceneData.vCamEndRot[iSource]		
		sScriptedCutsceneData.fStartCamFOV[iTarget]			= sScriptedCutsceneData.fStartCamFOV[iSource]	
		sScriptedCutsceneData.fEndCamFOV[iTarget]			= sScriptedCutsceneData.fEndCamFOV[iSource]	
		sScriptedCutsceneData.iCutsceneShotBitset[iTarget]	= sScriptedCutsceneData.iCutsceneShotBitset[iSource]
		sScriptedCutsceneData.iCutsceneShotCustomTicker[iTarget]	= sScriptedCutsceneData.iCutsceneShotCustomTicker[iSource]		
		sScriptedCutsceneData.iEndEarlyTime[iTarget]		= sScriptedCutsceneData.iEndEarlyTime[iSource]
		sScriptedCutsceneData.fMaxFarClipDistance[iTarget]	= sScriptedCutsceneData.fMaxFarClipDistance[iSource]
		sScriptedCutsceneData.iShakeType[iTarget] 			= sScriptedCutsceneData.iShakeType[iSource]
		sScriptedCutsceneData.fShakeMag[iTarget] 			= sScriptedCutsceneData.fShakeMag[iSource]
		sScriptedCutsceneData.vOffsetPos[iTarget] 			= sScriptedCutsceneData.vOffsetPos[iSource]
		sScriptedCutsceneData.vOffsetRot[iTarget] 			= sScriptedCutsceneData.vOffsetRot[iSource]
		sScriptedCutsceneData.vLookAtPos[iTarget] 			= sScriptedCutsceneData.vLookAtPos[iSource]
		sScriptedCutsceneData.iVehicleToAttachTo[iTarget] 	= sScriptedCutsceneData.iVehicleToAttachTo[iSource]
		sScriptedCutsceneData.vStreamStartPos[iTarget] 		= sScriptedCutsceneData.vStreamStartPos[iSource]
		sScriptedCutsceneData.fStreamRadius[iTarget] 		= sScriptedCutsceneData.fStreamRadius[iSource]
		sScriptedCutsceneData.vWalkToPosition[iTarget] 		= sScriptedCutsceneData.vWalkToPosition[iSource]
		sScriptedCutsceneData.fWalkToPositionRadius[iTarget]= sScriptedCutsceneData.fWalkToPositionRadius[iSource]
		sScriptedCutsceneData.vDriveToPosition[iTarget]		= sScriptedCutsceneData.vDriveToPosition[iSource]
		sScriptedCutsceneData.fDriveSpeed[iTarget]			= sScriptedCutsceneData.fDriveSpeed[iSource]
		sScriptedCutsceneData.eDroneUIPreset[iTarget]		= sScriptedCutsceneData.eDroneUIPreset[iSource]
	ENDIF
ENDPROC

PROC CLEAR_SHOT_DATA(FMMC_CUTSCENE_DATA &sScriptedCutsceneData, INT iShot)	
	IF iShot >= 0 AND iShot < MAX_CAMERA_SHOTS
		sScriptedCutsceneData.iInterpTime[iShot] 	= 0
		sScriptedCutsceneData.fOverrideRotMin[iShot]= 0.0
		sScriptedCutsceneData.fOverrideRotMax[iShot]= 0.0
		sScriptedCutsceneData.CamType[iShot]	 	= GRAPH_TYPE_LINEAR
		sScriptedCutsceneData.vCamStartPos[iShot]	= <<0,0,0>>
		sScriptedCutsceneData.vCamEndPos[iShot]		= <<0,0,0>>
		sScriptedCutsceneData.vCamStartRot[iShot]	= <<0,0,0>>
		sScriptedCutsceneData.vCamEndRot[iShot]		= <<0,0,0>>
		sScriptedCutsceneData.fStartCamFOV[iShot]	= 0.0
		sScriptedCutsceneData.fEndCamFOV[iShot]		= 0.0
		sScriptedCutsceneData.iCutsceneShotBitset[iShot] = 0
		sScriptedCutsceneData.iCutsceneShotCustomTicker[iShot] = -1
		sScriptedCutsceneData.fMaxFarClipDistance[iShot] 	= 0.0
		sScriptedCutsceneData.iEndEarlyTime[iShot] 	= 500
		sScriptedCutsceneData.iShakeType[iShot] 	= -1
		sScriptedCutsceneData.fShakeMag[iShot] 		= 0.0
		sScriptedCutsceneData.vStreamStartPos[iShot] = <<0,0,0>>
		sScriptedCutsceneData.vWalkToPosition[iShot] = <<0,0,0>>
		sScriptedCutsceneData.fWalkToPositionRadius[iShot] = 0.0
		sScriptedCutsceneData.vDriveToPosition[iShot] = <<0,0,0>>
		sScriptedCutsceneData.fDriveSpeed[iShot] = 0.0
		sScriptedCutsceneData.fStreamRadius[iShot] 	= 0.0
		sScriptedCutsceneData.sCamName[iShot]		= ""
		sScriptedCutsceneData.sLocation[iShot]	   	= ""
		sScriptedCutsceneData.eDroneUIPreset[iShot]		= MC_DRONE_UI_PRESET_NONE
	ENDIF
ENDPROC

PROC COPY_CUTSCENE_ENTITY_DATA(FMMC_CUTSCENE_ENTITY &eSource, FMMC_CUTSCENE_ENTITY &eDest)
	eDest.iType = eSource.iType
	eDest.iIndex = eSource.iIndex
	eDest.tlCutsceneHandle = eSource.tlCutsceneHandle
ENDPROC

PROC CLEAR_CUTSCENE_ENTITY_DATA(FMMC_CUTSCENE_ENTITY &sEntity)
	sEntity.iType = -1
	sEntity.iIndex = -1
	sEntity.tlCutsceneHandle = ""
ENDPROC

PROC SORT_CUTSCENE_ENTITIES(FMMC_CUTSCENE_ENTITY &sCutSceneEntities[])
	FMMC_CUTSCENE_ENTITY eArray[FMMC_MAX_CUTSCENE_ENTITIES]
	INT i, iCurrent
	FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
		CLEAR_CUTSCENE_ENTITY_DATA(eArray[i])
		IF sCutsceneEntities[i].iType != -1
		AND sCutsceneEntities[i].iIndex != -1
			COPY_CUTSCENE_ENTITY_DATA(sCutsceneEntities[i], eArray[iCurrent])
			iCurrent++
		ENDIF
		CLEAR_CUTSCENE_ENTITY_DATA(sCutsceneEntities[i])
	ENDFOR
	FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
		COPY_CUTSCENE_ENTITY_DATA(eArray[i], sCutsceneEntities[i])
	ENDFOR
ENDPROC

PROC COPY_CUTSCENE_PED_WARP_DATA(FMMC_CUTSCENE_PED_WARP_DATA &sSource, FMMC_CUTSCENE_PED_WARP_DATA &sTarget)
	sTarget.iVehID 				= sSource.iVehID
	sTarget.iVehIDSecondary 	= sSource.iVehIDSecondary
	sTarget.iVehSeat 			= sSource.iVehSeat
	sTarget.iBitset1 			= sSource.iBitset1
ENDPROC

PROC COPY_CUTSCENE_PLAYER_SPECIFIC_DATA(FMMC_CUTSCENE_PLAYER_SPECIFIC_DATA &sSource, FMMC_CUTSCENE_PLAYER_SPECIFIC_DATA &sTarget)	
	sTarget.iAnimation 					= sSource.iAnimation
	sTarget.iPlayAnimOnShot 			= sSource.iPlayAnimOnShot
ENDPROC

PROC CLEAR_CUTSCENE_PED_WARP_DATA(FMMC_CUTSCENE_PED_WARP_DATA &sPedWarpData)
	sPedWarpData.iVehID = -1
	sPedWarpData.iVehIDSecondary = -1
	sPedWarpData.iVehSeat = 0
	sPedWarpData.iBitset1 = 0
ENDPROC

PROC CLEAR_CUTSCENE_PLACED_PED_WARP_DATA(FMMC_CUTSCENE_PLACED_PED_WARP_DATA &sPedWarpData)
	sPedWarpData.iPedID = -1
	CLEAR_CUTSCENE_PED_WARP_DATA(sPedWarpData.sTarget)
ENDPROC

PROC CLEAR_CUTSCENE_PLAYER_SPECIFIC_DATA(FMMC_CUTSCENE_PLAYER_SPECIFIC_DATA	&sPlayerSpecificData)
	sPlayerSpecificData.iAnimation = 0
	sPlayerSpecificData.iPlayAnimOnShot = -1
ENDPROC

PROC CLEAR_CUTSCENE_SYNC_SCENE_DATA(FMMC_CUTSCENE_SYNC_SCENE_DATA &sCutsceneSyncSceneData)
	sCutsceneSyncSceneData.eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_NONE
	sCutsceneSyncSceneData.vCoord = <<0.0, 0.0, 0.0>>
	sCutsceneSyncSceneData.fHeading = 0.0	
	sCutsceneSyncSceneData.iShotToPlayOn = 0
ENDPROC

PROC CLEAR_CUTSCENE_ENTITY_WARP_DATA(FMMC_ENTITY_CUTSCENE_WARP_DATA &sCutsceneEntityWarpData, INT i)
	sCutsceneEntityWarpData.iEntityType[i] = CUTSCENE_WARP_ENTITY_TYPE_NONE
	sCutsceneEntityWarpData.iEntityIndex[i] = 0
	sCutsceneEntityWarpData.vWarpPos[i] = <<0.0, 0.0, 0.0>>
	sCutsceneEntityWarpData.fWarpHead[i] = 0.0
ENDPROC

PROC CLEAR_ALL_CUTSCENE_ENTITY_WARP_DATA(FMMC_CUTSCENE_DATA &sCutsceneData)
	INT i = 0
	FOR i = 0 TO CUTSCENE_WARP_ENTITY_MAX-1
		CLEAR_CUTSCENE_ENTITY_WARP_DATA(sCutsceneData.sCutsceneEntityWarpData, i)
	ENDFOR
ENDPROC

PROC CLEAR_CUTSCENE_DATA(FMMC_CUTSCENE_DATA &sScriptedCutsceneData)

	INT i
	sScriptedCutsceneData.tlName				= ""
	sScriptedCutsceneData.iCutsceneBitset		= 0
	sScriptedCutsceneData.iCutsceneBitSet2		= 0
	sScriptedCutsceneData.iCutsceneBitSet3		= 0
	sScriptedCutsceneData.iCutsceneBitSet4		= 0
	
	sScriptedCutsceneData.eExternalCutsceneSequence = EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_NONE	
	
	sScriptedCutsceneData.eUseAltEndPos				= USE_ALT_END_POS_DISABLED
	
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		sScriptedCutsceneData.vPlayerStartPos[i]		= <<0,0,0>>
		sScriptedCutsceneData.vPlayerEndPos[i]			= <<0,0,0>>
		sScriptedCutsceneData.vPlayerAltEndPos[i]		= <<0,0,0>>
		sScriptedCutsceneData.fPlayerStartheading[i]	= 0
		sScriptedCutsceneData.fPlayerEndheading[i]		= 0
		sScriptedCutsceneData.fPlayerAltEndHeading[i]	= 0
		sScriptedCutsceneData.iVehicleIndexForPosWarp[i] = -1
	ENDFOR

	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS - 1
		CLEAR_CUTSCENE_PLAYER_SPECIFIC_DATA(sScriptedCutsceneData.sPlayerSpecificData[i])
		CLEAR_CUTSCENE_PED_WARP_DATA(sScriptedCutsceneData.sPlayerStartWarp[i])
		CLEAR_CUTSCENE_PED_WARP_DATA(sScriptedCutsceneData.sPlayerEndWarp[i])
	ENDFOR
	
	CLEAR_CUTSCENE_SYNC_SCENE_DATA(sScriptedCutsceneData.sCutsceneSyncSceneData)
	
	CLEAR_ALL_CUTSCENE_ENTITY_WARP_DATA(sScriptedCutsceneData)
	
	sScriptedCutsceneData.iNumCamShots			= 0
	sScriptedCutsceneData.iAnimation			= 0
	sScriptedCutsceneData.iMidPoint				= 0
	sScriptedCutsceneData.iCutsceneWarpRange		= 0
	sScriptedCutsceneData.iCutscenePlayerInclusionRange = 5000
	sScriptedCutsceneData.fCutsceneRollingStartSpeed = 6.0
	
	sScriptedCutsceneData.iTransitionInTime		= 0
	sScriptedCutsceneData.iTransitionOutTime	= 0
	sScriptedCutsceneData.iTransitionHoldTime	= 0
	sScriptedCutsceneData.vIntroCoOrds			= <<0,0,0>>
	
	sScriptedCutsceneData.vClearCoOrds				= <<0,0,0>>
	sScriptedCutsceneData.fClearRadius				= 0.0
	sScriptedCutsceneData.vReplacementArea			= <<0,0,0>>
	
	sScriptedCutsceneData.iLiftMovementOnShot	= -1
	sScriptedCutsceneData.fLiftSpeed	= 1.0
	sScriptedCutsceneData.fLiftZOffset_Start	= 0.0
	sScriptedCutsceneData.fLiftZOffset_End	= 0.0
		
	sScriptedCutsceneData.iFadeBeforeEndTime = 0
	
	sScriptedCutsceneData.iInterpBackToGameplayTime = DEFAULT_INTERP_TO_FROM_GAME
		
	FOR i = 0 TO MAX_CAMERA_SHOTS-1
		CLEAR_SHOT_DATA(sScriptedCutsceneData, i)
	ENDFOR
	FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
		CLEAR_CUTSCENE_ENTITY_DATA(sScriptedCutsceneData.sCutsceneEntities[i])
	ENDFOR		
	sScriptedCutsceneData.eCSLiftData = eCSLift_None
ENDPROC

PROC CLEAR_MOCAP_PLAYER_INFO(FMMC_MOCAP_PLAYER_INFO &sSource)

	INT i
	FOR i = 0 TO FMMC_MAX_MOCAP_PLAYER_PROPS -1
		sSource.tlPropHandles[i] = ""
	ENDFOR

ENDPROC

PROC COPY_CUTSCENE_DATA(FMMC_CUTSCENE_DATA &sSource, FMMC_CUTSCENE_DATA &sTarget)
	INT i
	sTarget.tlName						= sSource.tlName			
	sTarget.iCutsceneBitset				= sSource.iCutsceneBitset
	sTarget.iCutsceneBitSet2			= sSource.iCutsceneBitSet2
	sTarget.iCutsceneBitSet3			= sSource.iCutsceneBitSet3	
	sTarget.iCutsceneBitSet4			= sSource.iCutsceneBitSet4
	
	sTarget.eExternalCutsceneSequence	= sSource.eExternalCutsceneSequence
	
	sTarget.eUseAltEndPos				= sSource.eUseAltEndPos
	
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		sTarget.vPlayerStartPos[i]		= sSource.vPlayerStartPos[i]
		sTarget.vPlayerEndPos[i]		= sSource.vPlayerEndPos[i]
		sTarget.vPlayerAltEndPos[i]		= sSource.vPlayerAltEndPos[i]
		sTarget.fPlayerStartHeading[i]	= sSource.fPlayerStartHeading[i]
		sTarget.fPlayerEndHeading[i]	= sSource.fPlayerEndHeading[i]
		sTarget.fPlayerAltEndHeading[i]	= sSource.fPlayerAltEndHeading[i]
		sTarget.iVehicleIndexForPosWarp[i] = sSource.iVehicleIndexForPosWarp[i]
	ENDFOR
	sTarget.iAnimation					= sSource.iAnimation
	sTarget.iNumCamShots				= sSource.iNumCamShots
	sTarget.iMidPoint					= sSource.iMidPoint
	sTarget.iCutsceneWarpRange				= sSource.iCutsceneWarpRange
	sTarget.iCutscenePlayerInclusionRange = sSource.iCutscenePlayerInclusionRange
	sTarget.fCutsceneRollingStartSpeed  = sSource.fCutsceneRollingStartSpeed
	
	sTarget.vClearCoOrds				= sSource.vClearCoOrds
	sTarget.fClearRadius				= sSource.fClearRadius
	sTarget.vReplacementArea			= sSource.vReplacementArea
	
	sTarget.iTransitionInTime			= sSource.iTransitionInTime
	sTarget.iTransitionOutTime			= sSource.iTransitionOutTime
	sTarget.iTransitionHoldTime			= sSource.iTransitionHoldTime
	sTarget.vIntroCoOrds				= sSource.vIntroCoOrds
	
	sTarget.iLiftMovementOnShot			= sSource.iLiftMovementOnShot
	sTarget.fLiftSpeed					= sSource.fLiftSpeed
	sTarget.fLiftZOffset_Start			= sSource.fLiftZOffset_Start
	sTarget.fLiftZOffset_End			= sSource.fLiftZOffset_End
	
	sTarget.sCutsceneSyncSceneData.eType 	= sSource.sCutsceneSyncSceneData.eType 	
	sTarget.sCutsceneSyncSceneData.vCoord 	= sSource.sCutsceneSyncSceneData.vCoord 
	sTarget.sCutsceneSyncSceneData.fHeading = sSource.sCutsceneSyncSceneData.fHeading
	sTarget.sCutsceneSyncSceneData.iShotToPlayOn = sSource.sCutsceneSyncSceneData.iShotToPlayOn
	
	sTarget.iFadeBeforeEndTime			= sSource.iFadeBeforeEndTime
	
	sTarget.iInterpBackToGameplayTime	= sSource.iInterpBackToGameplayTime
	
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS - 1
		COPY_CUTSCENE_PLAYER_SPECIFIC_DATA(sSource.sPlayerSpecificData[i], sTarget.sPlayerSpecificData[i])
		COPY_CUTSCENE_PED_WARP_DATA(sSource.sPlayerStartWarp[i], sTarget.sPlayerStartWarp[i])
		COPY_CUTSCENE_PED_WARP_DATA(sSource.sPlayerEndWarp[i], sTarget.sPlayerEndWarp[i])		
	ENDFOR
	
	FOR i = 0 TO MAX_CAMERA_SHOTS-1
		sTarget.iInterpTime[i]			= sSource.iInterpTime[i]
		sTarget.fOverrideRotMin[i]		= sSource.fOverrideRotMin[i]
		sTarget.fOverrideRotMax[i]		= sSource.fOverrideRotMax[i]
		sTarget.CamType[i]				= sSource.CamType[i]
		sTarget.vCamStartPos[i]			= sSource.vCamStartPos[i]
		sTarget.vCamEndPos[i]			= sSource.vCamEndPos[i]
		sTarget.vCamStartRot[i]			= sSource.vCamStartRot[i]
		sTarget.vCamEndRot[i]			= sSource.vCamEndRot[i]
		sTarget.fStartCamFOV[i]			= sSource.fStartCamFOV[i]
		sTarget.fEndCamFOV[i]			= sSource.fEndCamFOV[i]
		sTarget.iCutsceneShotBitSet[i]	= sSource.iCutsceneShotBitSet[i]
		sTarget.iCutsceneShotCustomTicker[i]	= sSource.iCutsceneShotCustomTicker[i]		
		sTarget.iEndEarlyTime[i]		= sSource.iEndEarlyTime[i]
		sTarget.fMaxFarClipDistance[i]	= sSource.fMaxFarClipDistance[i]		
		sTarget.iShakeType[i]			= sSource.iShakeType[i]
		sTarget.fShakeMag[i]			= sSource.fShakeMag[i]
		sTarget.vOffsetPos[i] 			= sSource.vOffsetPos[i]
		sTarget.vOffsetRot[i] 			= sSource.vOffsetRot[i]
		sTarget.iVehicleToAttachTo[i] 	= sSource.iVehicleToAttachTo[i]
		sTarget.vLookAtPos[i] 			= sSource.vLookAtPos[i]
		sTarget.vStreamStartPos[i] 		= sSource.vStreamStartPos[i]
		sTarget.fStreamRadius[i] 		= sSource.fStreamRadius[i]
		sTarget.sCamName[i]				= sSource.sCamName[i]	
		sTarget.sLocation[i]			= sSource.sLocation[i]
		sTarget.vWalkToPosition[i]		= sSource.vWalkToPosition[i]
		sTarget.fWalkToPositionRadius[i]= sSource.fWalkToPositionRadius[i]
		sTarget.vDriveToPosition[i]		= sSource.vDriveToPosition[i]
		sTarget.fDriveSpeed[i]			= sSource.fDriveSpeed[i]
		sTarget.eDroneUIPreset[i]		= sSource.eDroneUIPreset[i]		
	ENDFOR
	FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
		sTarget.sCutsceneEntities[i]	= sSource.sCutsceneEntities[i]
	ENDFOR
	sTarget.sCutsceneEntityWarpData = sSource.sCutsceneEntityWarpData
	sTarget.eCSLiftData = sSource.eCSLiftData

ENDPROC

PROC CLEAR_MOCAP_CUTSCENE_DATA(FMMC_MOCAP_CUTSCENE_DATA &sMocapCutsceneData)	
	INT i
	sMocapCutsceneData.tlName				= ""
	sMocapCutsceneData.iCutsceneBitset		= 0
	sMocapCutsceneData.iCutsceneBitSet2		= 0
	sMocapCutsceneData.iCutsceneBitSet3		= 0
	sMocapCutsceneData.iCutsceneBitSet4 	= 0
	sMocapCutsceneData.vCutscenePosition = <<0,0,0>>
	sMocapCutsceneData.vCutsceneRotation = <<0,0,0>>
	
	sMocapCutsceneData.vClearCoords				= <<0,0,0>>
	sMocapCutsceneData.fClearRadius				= 0.0
	sMocapCutsceneData.vReplacementArea			= <<0,0,0>>
	sMocapCutsceneData.iMidpointTimer			= 0
	sMocapCutsceneData.iCutsceneWarpRange			= 0
	sMocapCutsceneData.iCutscenePlayerInclusionRange = 5000
	sMocapCutsceneData.fCutsceneRollingStartSpeed = 6.0
	sMocapCutsceneData.iVariationBS = 0
	
	sMocapCutsceneData.eUseAltEndPos = USE_ALT_END_POS_DISABLED
	
	sMocapCutsceneData.fEndCutsceneTimeCycleFade = 1.0
	
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		sMocapCutsceneData.vPlayerStartPos[i]		= <<0,0,0>>
		sMocapCutsceneData.vPlayerEndPos[i]			= <<0,0,0>>
		sMocapCutsceneData.vPlayerAltEndPos[i]		= <<0,0,0>>
		sMocapCutsceneData.fPlayerStartheading[i]	= 0
		sMocapCutsceneData.fPlayerEndheading[i]		= 0
		sMocapCutsceneData.fPlayerAltEndHeading[i]	= 0
	ENDFOR
		
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS - 1
		CLEAR_CUTSCENE_PED_WARP_DATA(sMocapCutsceneData.sPlayerEndWarp[i])
		CLEAR_MOCAP_PLAYER_INFO(sMocapCutsceneData.sPlayerInfo[i])
	ENDFOR
	
	FOR i = 0 TO MAX_POST_CUTSCENE_VEH_PEDS - 1
		CLEAR_CUTSCENE_PLACED_PED_WARP_DATA(sMocapCutsceneData.sPedEndWarp[i])
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
		CLEAR_CUTSCENE_ENTITY_DATA(sMocapCutsceneData.sCutsceneEntities[i])
	ENDFOR
	
	sMocapCutsceneData.iCutsceneMusicMood			= 0
	
	FOR i = 0 TO FMMC_MAX_CUTSCENE_HIDE_ENTITIES - 1
		sMocapCutsceneData.tl31_HideCutsceneHandles[i] = ""
	ENDFOR
	
ENDPROC

PROC CLEAR_CLEANUP_DATA(CLEANUP_STRUCT &sCleanupData)
	sCleanupData.eDespawnOption = FMMC_DESPAWN_USING_NONE
	sCleanupData.iCleanupValue1 = -1
	sCleanupData.iNumOfPlayersRequired = 1
	sCleanupData.iRuleEnd = 0
	sCleanupData.iCleanupBS = 0
ENDPROC

PROC CLEAR_CRASH_VEHICLE_DATA(CRASH_VEHICLE_STRUCT& sCrashVehicleData)
	sCrashVehicleData.eCrashVehicleOption = FMMC_CRASH_VEH_DISABLED
	sCrashVehicleData.iOptionAssociatedValue = 0
	sCrashVehicleData.iAssociatedTeam 		 = 0
ENDPROC

PROC CLEAR_WORLD_PROP_DATA(WORLD_PROP_STRUCT &sWorldProp)
	sWorldProp.mn = DUMMY_MODEL_FOR_SCRIPT
	sWorldProp.vPos = <<0,0,0>>
	sWorldProp.iBitset = 0
	sWorldProp.iModelSwapStringIndex = -1
	sWorldProp.iRadiusModelHide = 3
ENDPROC

PROC CLEAR_TRAIN_LOCATION_DATA(FMMC_TRAIN_LOCATION_STRUCT &sTrainLocationData)
	sTrainLocationData.iLinkedZone = -1
	sTrainLocationData.iBitset = 0
	sTrainLocationData.fCruiseSpeed = ciPLACED_TRAIN_DEFAULT_SPEED
	sTrainLocationData.iObjectiveSkip_Team = -1
	sTrainLocationData.iObjectiveSkip_Rule = -1
ENDPROC

PROC CLEAR_TRAIN_DATA(FMMCPlacedTrainsStruct& sTrainData)

	INT i
	
	sTrainData.vPlacedTrainPos = <<0,0,0>>
	sTrainData.iPlacedTrainConfig = 0
	
	sTrainData.iPlacedTrain_StopMovingOnRuleBS = 0
	sTrainData.iPlacedTrain_StopMovingOnRuleTeam = -1
	
	sTrainData.iPlacedTrain_WarpOnRuleTeam = -1
	FOR i = 0 TO FMMC_MAX_RULES - 1
		sTrainData.vPlacedTrain_WarpOnRuleCoords[i] = <<0,0,0>>
	ENDFOR
	
	sTrainData.iPlacedTrainBS = 0
	sTrainData.fPlacedTrainCruiseSpeed = ciPLACED_TRAIN_DEFAULT_SPEED
	
	sTrainData.iPlacedTrainColour = -1
	sTrainData.iPlacedTrainSecondaryColour = 0
	
	sTrainData.iTrain_AssociatedRuleSpawnLimit = ciOBJECTIVE_SPAWN_LIMIT_OFF
	sTrainData.iTrain_AssociatedTeam = -1
	sTrainData.iTrain_AssociatedRule = 0
	sTrainData.iTrain_CleanupTeam = -1
	sTrainData.iTrain_CleanupRule = 0
	
	sTrainData.iTrain_SpawnGroup = 0 
	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS - 1
		sTrainData.iTrain_SpawnSubGroupBS[i] = 0
		sTrainData.iTrain_ModifySpawnSubGroupOnEventBS[i] = 0
	ENDFOR
	sTrainData.iTrain_ModifySpawnGroupOnEvent = 0 
	sTrainData.iTrain_SpawnGroupAdditionalFunctionalityBS = 0
	
	sTrainData.iAggroIndexBS_Entity_Train = ciMISSION_AGGRO_INDEX_DEFAULT
	
	INITIALISE_INT_ARRAY(sTrainData.iPlacedTrain_CarriageExtrasToUseBS, 0)
	
	CLEAR_ENTITY_BLIP_DATA_STRUCT(sTrainData.sTrainBlipStruct)
	
	FOR i = 0 TO ciPLACED_TRAIN_MAX_LOCATIONS - 1
		CLEAR_TRAIN_LOCATION_DATA(sTrainData.sLocations[i])
	ENDFOR
ENDPROC

PROC COPY_DUMMY_BLIP_DATA(DUMMY_BLIP_STRUCT &sTarget, DUMMY_BLIP_STRUCT &sSource)
	sTarget = sSource
ENDPROC

PROC CLEAR_DUMMY_BLIP_DATA(DUMMY_BLIP_STRUCT &sDummyBlip)
	INT ii
	
	sDummyBlip.iTeam = -1
	sDummyBlip.iRule = -1
	sDummyBlip.iType = ciFMMC_DROP_OFF_TYPE_CYLINDER
	sDummyBlip.iBlipSize = 0
	sDummyBlip.iEntityToUse = -1
	sDummyBlip.iBitset = 0
	sDummyBlip.vPos = <<0,0,0>>
	sDummyBlip.iEntityType = 0
	sDummyBlip.iEntityIndex = -1
	sDummyBlip.fHideBlipRange = 0.0
	sDummyBlip.tlDBName = ""
	sDummyBlip.sDummyBlipBlipData.sTeamBlip[0].iBlipOverrideStartRule = -1
	sDummyBlip.sDummyBlipBlipData.sTeamBlip[0].iBlipOverrideEndRule = -1
	sDummyBlip.sDummyBlipBlipData.sTeamBlip[0].iBlipOverrideBitSet = 0
	sDummyBlip.iSpawnGroup = 0
	sDummyBlip.iSpawnSubGroup = 0
	INITIALISE_INT_ARRAY(sDummyBlip.iAltVarsBitsetDummyBlip, 0)
	FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		sDummyBlip.iSpawnSubGroupBS[ii] = 0
	ENDFOR
	sDummyBlip.vOverridePosition = <<0,0,0>>
	sDummyBlip.iAggroIndexBS_Entity_DummyBlip = ciMISSION_AGGRO_INDEX_DEFAULT
	sDummyBlip.iGPSRange = 0
	sDummyBlip.iVehicleRequired = -1
	CLEAR_ENTITY_BLIP_DATA_STRUCT(sDummyBlip.sDummyBlipBlipData)
ENDPROC

PROC CLEAR_CUSTOM_PROP(INT iClearIndex)
	g_FMMC_STRUCT.sCustomProps[iClearIndex].mnCustomPropName = DUMMY_MODEL_FOR_SCRIPT
ENDPROC

PROC CLEAR_CUSTOM_PROP_LIST()
	INT i
	
	FOR i = 0 TO ciMAX_CUSTOM_DEV_PROPS - 1
		CLEAR_CUSTOM_PROP(i)
	ENDFOR
	
	g_FMMC_STRUCT.iCurrentNumberOfDevProps = 0
ENDPROC

PROC CLEAR_ALL_RULE_BOUNDS_ON_RULE(INT iTeam, INT iRule)
	INT iRuleBoundsIndex
	
	FOR iRuleBoundsIndex = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
		CLEAR_RULE_BOUNDS(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iRuleBoundsIndex])
	ENDFOR
ENDPROC

#IF IS_DEBUG_BUILD

PROC REMOVE_ALL_DUMMY_BLIPS()
	INT i
	FOR i = 0 TO FMMC_MAX_DUMMY_BLIPS-1
		IF DOES_BLIP_EXIST(g_FMMC_STRUCT.biDummyBlips[i])
			REMOVE_BLIP(g_FMMC_STRUCT.biDummyBlips[i])
		ENDIF
	ENDFOR
ENDPROC

#ENDIF

PROC MOVE_PROP_TEMPLATE_MODEL(PROP_TEMPLATE_STRUCT &sTemplate,INT iFrom, INT iTo)
	sTemplate.mn[iTo] = sTemplate.mn[iFrom]
	sTemplate.vOffset[iTo] = sTemplate.vOffset[iFrom]
	sTemplate.vRotation[iTo] = sTemplate.vRotation[iFrom]
	sTemplate.iColour[iTo] = sTemplate.iColour[iFrom]
ENDPROC

PROC CLEAR_PROP_TEMPLATE_MODEL(PROP_TEMPLATE_STRUCT &sTemplate, INT iTemplate)
	
	sTemplate.vOffset[iTemplate] = <<0,0,0>>
	sTemplate.vRotation[iTemplate] = <<0,0,0>>
	sTemplate.mn[iTemplate] = DUMMY_MODEL_FOR_SCRIPT
ENDPROC

PROC CLEAR_PROP_TEMPLATE(PROP_TEMPLATE_STRUCT &sTemplate)

	INT i
	FOR i = 0 TO FMMC_MAX_PROP_TEMPLATE_SIZE-1
		CLEAR_PROP_TEMPLATE_MODEL(sTemplate,i)
	ENDFOR
ENDPROC

PROC CLEAR_PROP(FMMCplacedPropsStruct& sProp)
	
	INT ii
	
	sProp.vPos = <<0.0, 0.0, 0.0>>
	sProp.fHead = 0.0
	sProp.mn = DUMMY_MODEL_FOR_SCRIPT
	sProp.iAssociatedTeam = -1
	sProp.iAssociatedObjective = -1
	sProp.iAssociatedSpawn = 1
	sProp.iPropBitset = 0
	sProp.iPropBitSet2 = 0
	sProp.iAlarmDelay = 0
	sProp.iAlarmSound = 0
	sProp.iAlarmTeam = -1
	sProp.iFlareVFX = 0
	sProp.iFlareConnectedLocate = -1
	sProp.iPropColouring = -1
	sProp.iAssociatedActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
	sProp.iSecondActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
	sProp.iThirdActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
	sProp.iFourthActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
	sProp.iAssociatedScoreRequired  = -1
	sProp.iSecondAssociatedScoreRequired  = -1
	sProp.iThirdAssociatedScoreRequired  = -1
	sProp.iFourthAssociatedScoreRequired = -1
	sProp.iCleanupObjective = -1
	sProp.iCleanupTeam = -1
	sProp.iCleanupRange = -1
	sProp.iRadioStation = -1
	sProp.iTemplateIndex = -1
	sProp.iPropLodDist = -1
	sProp.iPropTeamNumber = 0
	sProp.iPropSpawnNumber = 0
	sProp.iPropColour = 1
	sProp.fPTFXTriggerRadius = 10.0
	sProp.iPropEffectSubType = - 1
	sProp.iRespawnOnRuleChangeBS = 0
	INITIALISE_INT_ARRAY(sProp.iRespawnOnRule, -1)
	sProp.iTagColour = 1		
	sProp.vFireworkTriggerPos = <<0.0, 0.0, 0.0>>
	sProp.fFireworkTriggerSize = 0.0
	sProp.iFireworkTeam = -1		
	sProp.iTrapToActivate = -1
	sProp.fPressurePadDelay = 0.0
	sProp.fTurretHeading = 0.0
	sProp.iLinkedDynamicPropIndex = -1
	sProp.vPropSpecifiedInterior = <<0,0,0>>
	sProp.iPropSpecifiedRoomKey = -1
	sProp.vPropSlideVector = <<0,0,0>>
	sProp.fPropSlideSpeed = 1.35
	sProp.iPropSlideRule = -1
	sProp.iPropSlideTeam = -1
	sProp.iColourChangeOnRuleBS = 0
	sProp.iColourChange = -1
	sProp.iPropPaintingIndex = -1
	
	FOR ii = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
		sProp.eSpawnConditionFlag[ii] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
	
	CLEAR_ENTITY_RENDER_TARGET_STRUCT(sProp.sPropRenderTargetOptions)
	
	sProp.iLODDistance = 0
	sProp.iSpawnGroup = 0
	sProp.iSpawnSubGroup = 0
	INITIALISE_INT_ARRAY(sProp.iAltVarsBitsetProp, 0)
	FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		sProp.iSpawnSubGroupBS[ii] = 0
	ENDFOR
	
	sProp.iAggroIndexBS_Entity_Prop = ciMISSION_AGGRO_INDEX_DEFAULT	
ENDPROC

PROC CLEAR_ENTITY_END_MISSION_DATA_STRUCT(END_MISSION_DATA &sEndMissionDataPassedIn)
	sEndMissionDataPassedIn.iNextMissionToPlayFail = -1
	sEndMissionDataPassedIn.iNextMissionToPlayPass = -1
	sEndMissionDataPassedIn.iNextMissionToPlayAggroPass = -1
	sEndMissionDataPassedIn.iEndPassMocap = -1
	sEndMissionDataPassedIn.iEndFailMocap = -1
	sEndMissionDataPassedIn.iEndAggroMocap = -1
	sEndMissionDataPassedIn.iBranchScriptCutRule = -1
	sEndMissionDataPassedIn.iBranchScriptCutScene = -1
ENDPROC

PROC CLEAR_PROXIMITY_SPAWNING_STRUCT(PROXIMITY_SPAWNING_STRUCT &sProximitySpawningData)
	sProximitySpawningData.iSpawnRange = 0
	sProximitySpawningData.iCleanupRange = 0
	sProximitySpawningData.iRequiredPlayers = ciPROXIMITY_SPAWNING_REQUIRED_PLAYERS_ANY
ENDPROC

PROC CLEAR_SELF_DESTRUCT_STRUCT(SELF_DESTRUCT_STRUCT &sStruct)
	sStruct.iTeam = -1
	sStruct.iRuleStart = -1
	sStruct.iRuleEnd = -1
	sStruct.iDelayMiliseconds = 0
	sStruct.iMustBeBelowHealthPercentage = 0
	sStruct.iDestructBS = 0
ENDPROC

PROC CLEAR_PED_COMPANION_SETTINGS(COMPANION_SETTINGS &sCompanionSettings)
	
	sCompanionSettings.iBS = 0
	sCompanionSettings.iTeam = -1
	sCompanionSettings.iRuleStart_ForceState = -1
	sCompanionSettings.iRuleEnd_ForceState = -1
	sCompanionSettings.fGroupJoinRange = 10.0
	sCompanionSettings.fGroupSeparationRange = 50.0
	sCompanionSettings.fSeparationWarpRange = 0.0
	
ENDPROC

PROC CLEAR_STEALTH_AND_AGGRO_DATA(STEALTH_AND_AGGRO_SETTINGS &sStealthAndAggroSystem)
	
	sStealthAndAggroSystem.iPresetIndex						= -1	
	sStealthAndAggroSystem.iBS								= 0	
	sStealthAndAggroSystem.iRuleFrom						= -1
	sStealthAndAggroSystem.iRuleTo							= -1
	sStealthAndAggroSystem.fDetectionRange					= 20.0
	sStealthAndAggroSystem.fLineOfSightConeAngle			= 45.0
	sStealthAndAggroSystem.fLineOfSightConeHeightMax		= 60.0
	sStealthAndAggroSystem.fLineOfSightConeHeightMin		= 75.0
	sStealthAndAggroSystem.fInstantDetectionRange			= 5.0
	sStealthAndAggroSystem.fInstantLineOfSightConeAngle		= 20.0
	sStealthAndAggroSystem.fInstantLineOfSightConeHeightMax	= 60.0
	sStealthAndAggroSystem.fInstantLineOfSightConeHeightMin	= 75.0
	sStealthAndAggroSystem.iStealthAggroDelay				= 5
	sStealthAndAggroSystem.fWeaponUsageRespondingDistance	= 20.0
	
ENDPROC

PROC CLEAR_PED_ASSOCIATED_GOTO_DATA(ASSOCIATED_GOTO_TASK_DATA	&sAssociatedGotoTaskData, INT i)
	
	INT j = 0	
	FOR j = 0 TO PED_ASSOCIATED_GOTO_TASK_TYPE_BITSET_MAX-1
		sAssociatedGotoTaskData.sPointData[i].iBitset[j] 					= 0
	ENDFOR	
	sAssociatedGotoTaskData.sPointData[i].vPosition 						= <<0.0,0.0,0.0>>
	sAssociatedGotoTaskData.sPointData[i].fPedAchieveFinalHeading 			= 0.0
 	sAssociatedGotoTaskData.sPointData[i].iWaitTime 						= 0
	sAssociatedGotoTaskData.sPointData[i].iAssociatedRule					= -1
	sAssociatedGotoTaskData.sPointData[i].vPlayerActivated_Pos 				= <<0.0,0.0,0.0>>
	sAssociatedGotoTaskData.sPointData[i].iPlayerActivated_Radius 			= 3
	sAssociatedGotoTaskData.sPointData[i].fCustomArrivalHeight 				= 0.0
	sAssociatedGotoTaskData.sPointData[i].fCustomArrivalRadius 				= 0.0
	sAssociatedGotoTaskData.sPointData[i].iVehicleGotoSpeed 				= -1
	sAssociatedGotoTaskData.sPointData[i].fCustomVehArriveRadius 			= 0.0
	sAssociatedGotoTaskData.sPointData[i].iChosenVehicle 					= -1		
	sAssociatedGotoTaskData.sPointData[i].iCompleteRuleObjectiveBS		 	= 0
	sAssociatedGotoTaskData.sPointData[i].iLoopOnRuleBS						= 0	
	sAssociatedGotoTaskData.sPointData[i].eCustomScenarioType 				= FMMC_PED_CUSTOM_SCENARIO_NONE
	FOR j = 0 TO FMMC_PED_CUSTOM_SCENARIO_GENERIC_INTS-1
		sAssociatedGotoTaskData.sPointData[i].iCustomScenarioInts[j] 		= 0
	ENDFOR
	sAssociatedGotoTaskData.sPointData[i].iCustomScenarioSkipCondition 		= 0
	sAssociatedGotoTaskData.sPointData[i].iCustomScenarioSkipData 			= 0
		
ENDPROC

PROC CLEAR_ALL_PED_ASSOCIATED_GOTO_DATA(ASSOCIATED_GOTO_TASK_DATA	&sAssociatedGotoTaskData)
	
	sAssociatedGotoTaskData.iNumberOfPedGotoTasks = -1
	sAssociatedGotoTaskData.iArrivalRadius = 0
	INITIALISE_INT_ARRAY(sAssociatedGotoTaskData.iEntireTaskBitset, 0)	
	sAssociatedGotoTaskData.fPedAccelerationCap = 0.0
	sAssociatedGotoTaskData.iLoopGotoStartIndex = -1
	sAssociatedGotoTaskData.iDetachCargobobOnGoto = -1
	INT i	
	FOR i = 0 TO MAX_ASSOCIATED_GOTO_TASKS-1
		CLEAR_PED_ASSOCIATED_GOTO_DATA(sAssociatedGotoTaskData, i)
	ENDFOR
		
ENDPROC

PROC CLEAR_PED_STRUCT(FMMCplacedPedStruct &ped)
	
	PRINTLN("CLEAR_PED_STRUCT called.")
	
	INT i2, ii

	ped.iIdleAnim = 0
	ped.iWepHolstered = 0
	ped.vPos = <<0.0,0.0,0.0>>
	ped.fHead = 0.0
	ped.fRange = 0.75
	ped.mn = DUMMY_MODEL_FOR_SCRIPT
	ped.gun = WEAPONTYPE_INVALID
	ped.skill = CAL_POOR
	ped.iPedRespawnLives = 0
	ped.iWeapon = 0
	ped.iHealth = 0	
	ped.iLGHealth = -1
	ped.iHealthDamageRequiredForRule = -1
	ped.iPedPreciseHealth = 100
	ped.fHealthVarMultiplier = 0
	ped.iDialogueTeam = -1
	ped.iDialogueObjective = -1
	ped.iDialogueTiming = 0
	ped.iDialogueLine = 0	
	ped.iDialogueRange = 20
	ped.iAggroTime = -1		
	ped.iVehicle = -1
	ped.iFlee = -1
	ped.iAccuracy = 0
	ped.iLGAccuracy = -1
	ped.iCombatStyle = 0
	ped.iPedBitset = 0
	ped.iPedBitsetTwo = 0
	ped.iPedBitsetThree = 0
	ped.iPedBitsetFour = 0
	ped.iPedBitsetFive = 0
	ped.iPedBitsetSix = 0
	ped.iPedBitsetSeven = 0
	ped.iPedBitsetEight = 0
	ped.iPedBitsetNine = 0
	ped.iPedBitsetTen = 0
	ped.iPedBitsetEleven = 0
	ped.iPedBitsetTwelve = 0
	ped.iPedBitsetThirteen = 0
	ped.iPedBitsetFourteen = 0
	ped.iPedBitsetFifteen = 0
	ped.iPedBitsetSixteen = 0
	ped.iPedBitsetSeventeen = 0
	ped.iPedBitsetEighteen = 0
	ped.iPedBitsetNineteen = 0
	ped.iPedBitsetTwenty = 0
	ped.iPedBitsetTwentyOne = 0
	ped.iPedBitsetTwentyTwo = 0
	ped.iCleanupDelay = 0
	ped.iPlayerToClone = -1
	ped.iTeamToCloneFrom = -1
	
	CLEAR_CLEANUP_DATA(ped.sCleanupData)
	
	CLEAR_CRASH_VEHICLE_DATA(ped.sCrashVehicleData)
	
	CLEAR_ENTITY_PTFX_DATA(ped.sEntitySpawnPTFX)
	CLEAR_ENTITY_PTFX_DATA(ped.sEntityDespawnPTFX)
	
	ped.iPedTargetForSpeedMatching  = -1
	ped.iPedTargetForSpeedMatching2 = -1
	ped.fDistanceThreshForSpeedMatching = 40
	
	ped.iPedAggroWhenCCTVCamsDestroyed = 0
	ped.fExplosionPerceptionRadius = 32.0

	ped.iFlightHeight = 0
	ped.iMinFlyHeightAboveTerrain = -1
	ped.iModelVariation = 0
	ped.fBlockRange = 0
	ped.fRespawnRange = 50.0	
	ped.fPedRespawnRangePlayerSight = 120.0
	ped.fPedRespawnRangePlayerNear	= 30.0
	ped.iPedGotoHeightOffset = -101
	ped.iPedGoToHeightOffsetAtHealth = -1
	ped.iPedFleeOnVehicleHealthPercentage = 0
	ped.iAssociatedFleeZone = -1
	ped.iBloodPreset = ciPED_BLOOD_PRESET_NONE
		
	ped.iCustomAnimOptionA = -1
	ped.iCustomAnimOptionB = -1
	
	ped.iPedFixationTarget = -1
	ped.fPedFixationInterruptPlayerRange = -1.0
	ped.iPedFixationStartRule = -1
	ped.iPedFixationEndRule = -1
	ped.iPedMCVarAssociatedSpawnIndex = -1
	ped.iPedPointsToGive = 0
	ped.iPedGivePointsOnRuleStart = -1
	ped.iPedGivePointsOnRuleEnd = -1
	ped.iMovementOverrideAnim1Rule = -1
	ped.iMovementOverrideAnim1Type = -1
	ped.iMovementOverrideAnim2Rule = -1
	ped.iMovementOverrideAnim2Type = -1
	
	ped.fRespawnMinRange = 0.0
	ped.iSpawnNearType = 0
	ped.iRespawnOnRuleChangeBS = 0
	ped.iRespawnOnRuleLives = 1
	INITIALISE_INT_ARRAY(ped.iRespawnOnRule, -1)
	ped.iSpawnNearEntityID = -1
	
	ped.vSecondSpawnPos = <<0, 0, 0>>
	ped.fSecondHeading = 0
	
	ped.vOverrideSpawnPos = <<0, 0, 0>>
	ped.fOverrideHeading = 0
	
	ped.iLinkedToEntityType = 0
	ped.iAssociatedActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
	ped.iSecondActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
	ped.iThirdActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
	ped.iFourthActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
	ped.iLinkedToEntityType = 0
	ped.iPedEscortRange = 0
	ped.iPedDisableTargetableOnRuleBS 		= 0
	
	ped.fPedGotoMoveSpeedRatioOnFoot = 0.0
	
	ped.iContinuityId = -1
	ped.vPedHeliEscortOFFSET = <<0,0,0>>
	ped.iActivationRange = 0
	ped.iDriveSpeed = -1
	ped.iRollingStart = -1	
	ped.iAssociatedObjective = -1
	ped.iAssociatedTeam = -1
	ped.iAssociatedAction = ciACTION_NOTHING
	ped.iAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_OFF
	CLEAR_ENTITY_BLIP_DATA_STRUCT(ped.sPedBlipStruct)	
	ped.iPedPerceptionSettings = 0
	ped.iInventoryBS = 0
	ped.iInformRange = 250
	ped.iCrowdControlAction = 0
	ped.iCrowdControlID = -1
	ped.iPedPhotoRange = -1
	ped.iGroupFollowRange = 0
	ped.iPedFollowRange = 4
	ped.iDialogueTriggerBlock = -1
	ped.iObjectivePrereq = ciPREREQ_None
	ped.iVehicleSeatPreference = ciFMMC_SeatPreference_None
	ped.iVehicleSeatPreferenceRule = 0
	ped.iAltVehicleSeatPreference = ciFMMC_SeatPreference_None
	ped.iAltVehicleSeatPreferenceRule = 0
	ped.iVehicleSeatPreferenceTeam = 0
	ped.iSpookedDriveSpeed = 0
	ped.iEntityRelGroupDamageRestrictions = 0
	ped.iAssociatedScoreRequired = 0
	ped.iSecondScoreRequired = 0
	ped.iThirdScoreRequired = 0
	ped.iFourthScoreRequired = 0
	ped.iNewRelationshipGroupOnRule = -1
	ped.iNewRelationshipGroupType = -1	
	ped.iBlipColourRelGroupFriendly = -1
	ped.iBlipColourRelGroupNeutral = -1
	ped.iBlipColourRelGroupHostile = -1
	
	ped.iPedFlashBlipRule = -1
	ped.iPedFlashBlipDuration = 10	
	ped.iBecomeCloakedOnRule = -1
	ped.iLoseCloakOnRule = -1
	ped.iCloakTeamIndex = 0
	
	ped.iSpookPedOnVehicleDamageBS = 0
	ped.iSpookPedOnVehicleTouchedBS = 0
	
	ped.iAssociatedAlwaysForceSpawnOnRule = -1
	ped.iSecondAlwaysForceSpawnOnRule = -1
	ped.iThirdAlwaysForceSpawnOnRule = -1
	ped.iFourthAlwaysForceSpawnOnRule = -1
	
	ped.iAssociatedObjectiveEnd = -1
	ped.iSecondiRuleEnd = -1
	ped.iThirdiRuleEnd = -1
	ped.iFourthiRuleEnd = -1
	
	ped.iSecondaryTargetOverrideType = 0
	ped.iSecondaryTargetOverrideID = -1
	ped.iSecondaryActionTriggerDistanceFromTarget = -1
	
	ped.iPrimaryTargetOverrideType = 0
	ped.iPrimaryTargetOverrideID = -1
	ped.iPrimaryActionTriggerDistanceFromTarget = -1
	
	ped.iCleanupObjective = -1
	ped.iCleanupTeam = -1
	
	ped.iStopRespawningRule = -1
	ped.iStopRespawningTeam = -1
	
	ped.iCleanupRange = -1
	ped.iCSRespawnShotIndex = -1
	ped.iCSRespawnSceneIndex = -1
	ped.iCSTaskActivationSceneIndex	= -1
	ped.iCSTaskActivationShotIndex = -1
	ped.iCustomPedName = -1
	ped.iRespawnDelay = 0
	ped.iSpawnGroup = 0
	ped.iSpawnSubGroup = 0
	ped.iModifySpawnGroupOnEvent = 0
	ped.iSpawnGroupAdditionalFunctionalityBS = 0
	FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		ped.iSpawnSubGroupBS[ii] = 0
		ped.iModifySpawnSubGroupOnEventBS[ii] = 0
	ENDFOR
	
	ped.iTagColour = 1
	ped.iHackFailsForAggro = -1
	
	REPEAT FMMC_MAX_TEAMS i2
		ped.iTeam[i2] = 0
		ped.iRule[i2] = FMMC_OBJECTIVE_LOGIC_KILL
		ped.iPriority[i2] = FMMC_PRIORITY_IGNORE
		ped.iAgroFailPriority[i2] = -1
		ped.iPedSkipToRuleWhenAggro[i2] = -1
		ped.iJumpToObjectivePass[i2] = 0
		ped.iJumpToObjectiveFail[i2] = 0
		ped.iGotoProgressesRule[i2] = -2
		ped.iFollowTeamOnRuleBS[i2] = 0
		ped.iMissionCriticalRemovalRule[i2] = 0
	ENDREPEAT
	
	CLEAR_ALL_PED_ASSOCIATED_GOTO_DATA(ped.sAssociatedGotoTaskData)
	
	REPEAT FMMC_MAX_RESTART_CHECKPOINTS i2
		ped.vRestartPos[i2] = <<0,0,0>>
		ped.fRestartHeading[i2] = 0.0
	ENDREPEAT
	
	ped.iStopTasksTeam = -1
	ped.iStopTasksRule = -1
	ped.iAmbientVoice = -1
	ped.iWarpTimer = -1
	ped.iPedCashAmount = -1
	
	ped.iPedTrackingRule = -1
	ped.iPedSpeedOverride = -1
	ped.iSpookLinkedPed = -1
	ped.iSpookOnRange = -1
	ped.iSpookOnRangeTime = -1
	ped.iPedSpeedRange = -1
	ped.iAssociatedAggroZone = -1
	
	ped.fAnimBreakoutRange = -1.0
	ped.fAnimSpecialBreakoutRange = -1.0
	ped.fSpecialAnimTriggerRange = cfMAX_SPECIALANIM_TRIGGER_DIST
	ped.iSpecialAnimTriggerTeamBitset = 0
	ped.iIdleAnimTriggerFriendliesCount = -1
	ped.fIdleAnimTriggerFriendliesRange = 1.0
	ped.iSpecialAnimTriggerFriendliesCount = -1
	ped.fSpecialAnimTriggerFriendliesRange = 1.0
	ped.iTaskTriggerFriendliesCount = -1	
	ped.fTaskTriggerFriendliesRange = 1.0
	
	ped.iSpawnPlayerNum = 0
	ped.iDialogueDelay = 0
	
	ped.iPedCopDecoyActiveTime = 0
	ped.iPedCopDecoyLose2StarChance = 0
	
	ped.tlCustomIdleAnimDict = ""
	ped.tlCustomIdleAnimName = ""
	
	ped.tlCustomSpecialAnimDict = ""
	ped.tlCustomSpecialAnimName = ""
	
	CLEAR_STEALTH_AND_AGGRO_DATA(ped.sStealthAndAggroSystem)	
	
	CLEAR_PED_COMPANION_SETTINGS(ped.sCompanionSettings)
	
	ped.iDropPickupChance	=  0
	ped.iDropPickupIndex	= -1
	
	FOR i2 = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
		ped.eSpawnConditionFlag[i2] =  SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR	
	
	CLEAR_WARP_LOCATION_SETTINGS(ped.sWarpLocationSettings)	

	ped.iZoneBlockEntitySpawn = -1
	ped.iZoneBlockEntitySpawnPlayerReq = 0
	
	CLEAR_SELF_DESTRUCT_STRUCT(ped.sSelfDestructSettings)
				
	ped.fShowBlipHeightIndicatorRange = 0.0
	INITIALISE_INT_ARRAY(ped.iAltVarsBitsetPed, 0)
	
	CLEAR_ENTITY_END_MISSION_DATA_STRUCT(ped.sEndMissionData)
	
	CLEAR_PROXIMITY_SPAWNING_STRUCT(ped.sProximitySpawningData)
	
	CLEAR_TRAIN_ATTACH_STRUCT(ped.sTrainAttachmentData)
	
	ped.iAggroIndexBS_Entity_Ped = ciMISSION_AGGRO_INDEX_DEFAULT
		
	ped.iHealthbarTurnRedAtPercent = 0
	
	PRINTLN("CLEAR_PED_STRUCT Finished.")
	
ENDPROC

FUNC INT GET_SURVIVAL_DATA_DEFAULT(INT iData, INT iWave, INT iMod)
	SWITCH iData
		CASE ciSURVIVAL_DATA_SQUADS
			SWITCH iMod //Difficulty
				CASE 0
					SWITCH iWave
						CASE 1	
						CASE 2	RETURN 1
						CASE 3	
						CASE 4	
						CASE 5	RETURN 2
						CASE 6
						CASE 7	
						CASE 8	RETURN 3
						CASE 9
						CASE 10	RETURN 4
						DEFAULT RETURN 4
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iWave
						CASE 1	
						CASE 2	RETURN 1
						CASE 3	
						CASE 4	
						CASE 5	RETURN 2
						CASE 6
						CASE 7	
						CASE 8	RETURN 3
						CASE 9
						CASE 10	RETURN 4
						DEFAULT RETURN 4
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iWave
						CASE 1	RETURN 0
						CASE 2	RETURN 1
						CASE 3	RETURN 0
						CASE 4	
						CASE 5	RETURN 2
						CASE 6	RETURN 0
						CASE 7	
						CASE 8	RETURN 3
						CASE 9
						CASE 10	RETURN 4
						DEFAULT RETURN 4
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_PEDS_SQUAD
			RETURN 5
		BREAK
		CASE ciSURVIVAL_DATA_VEH_NUMBER
			SWITCH iWave
				CASE 3
				CASE 5
				CASE 6
				CASE 8	RETURN 2
				DEFAULT RETURN 0
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_HELI_NUM
			SWITCH iWave
				CASE 1
				CASE 2
				CASE 3
				CASE 6
				CASE 8
				CASE 9	RETURN 0
				DEFAULT RETURN 1
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_VEH_PEDS
			RETURN 2
		BREAK
		CASE ciSURVIVAL_DATA_PED_HEALTH
			SWITCH iWave
				CASE 1
				CASE 2
				CASE 3	RETURN 125
				CASE 4
				CASE 5
				CASE 6	RETURN 150
				CASE 7
				CASE 8
				CASE 9
				CASE 10	RETURN 200
				DEFAULT RETURN 200
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_PED_ARMOUR
			SWITCH iWave
				CASE 10
				CASE 11
				CASE 12
				CASE 13
				CASE 14
				CASE 15
				CASE 16
				CASE 17
				CASE 18
				CASE 19
				CASE 20 RETURN 30
				DEFAULT	RETURN 20
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_PED_ACCURACY
			SWITCH iWave
				CASE 1
				CASE 2
				CASE 3	RETURN 15
				CASE 4
				CASE 5
				CASE 6	RETURN 20
				CASE 7
				CASE 8
				CASE 9	RETURN 25
				CASE 10	RETURN 30
				DEFAULT RETURN 30
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_PED_COMBAT
			SWITCH iWave
				CASE 1 	RETURN 	0
				CASE 2
				CASE 3
				CASE 4
				CASE 5 	
				CASE 6 	RETURN	1
				CASE 7
				CASE 8
				CASE 9
				CASE 10 RETURN	2
				DEFAULT RETURN	2
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_KILLS_STAGE
			SWITCH iWave
				CASE 1
				CASE 2
				CASE 3
				CASE 4	RETURN 3
				DEFAULT RETURN 4
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_VEH_HEALTH
			RETURN 1000
		BREAK
		CASE ciSURVIVAL_DATA_AIR_HEALTH
			RETURN 350
		BREAK
		CASE ciSURVIVAL_DATA_ENEMY_NUM
			SWITCH iWave
				CASE 1 	
				CASE 2	RETURN 0
				CASE 3
				CASE 4
				CASE 5 	RETURN 1
				CASE 6 	
				CASE 7
				CASE 8	RETURN 2
				CASE 9
				CASE 10 RETURN 3
				DEFAULT RETURN 3
			ENDSWITCH	
		BREAK
		CASE ciSURVIVAL_DATA_SUB_STAGES
			SWITCH iWave
				CASE 1 	RETURN 	1
				CASE 2	RETURN 	2
				CASE 3	RETURN 	1
				CASE 4
				CASE 5	RETURN 	2
				CASE 6 	RETURN	1
				CASE 7
				CASE 8
				CASE 9	
				CASE 10 RETURN	2
				DEFAULT RETURN	2
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_DIFFICULTY
			SWITCH iWave
				CASE 1
				CASE 2
				CASE 3	RETURN 	0
				CASE 4
				CASE 5
				CASE 6 	RETURN	1
				CASE 7
				CASE 8
				CASE 9	RETURN 	2
				CASE 10 RETURN	3
				DEFAULT RETURN	3
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_WEP_CAT
			SWITCH iWave
				CASE 1
					SWITCH iMod
						CASE 3	RETURN 2
						DEFAULT	RETURN 0
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iMod
						CASE 0	
						CASE 1	RETURN 0
						CASE 2 	RETURN 2
						CASE 3	RETURN 2
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iMod
						CASE 0	RETURN 1
						CASE 1	
						CASE 2	RETURN 2
						CASE 3	RETURN 2
					ENDSWITCH
				BREAK
				CASE 4
					SWITCH iMod
						CASE 0	RETURN 1
						CASE 1	RETURN 3
						CASE 2	RETURN 2
						CASE 3	RETURN 2
					ENDSWITCH
				BREAK
				CASE 5
					SWITCH iMod
						CASE 0
						CASE 1	RETURN 3
						CASE 2	RETURN 1
						CASE 3	RETURN 2
					ENDSWITCH
				BREAK
				CASE 6
					SWITCH iMod
						CASE 0	RETURN 2
						CASE 1	RETURN 3
						CASE 2	RETURN 1
						CASE 3	RETURN 5
					ENDSWITCH
				BREAK
				CASE 7
					SWITCH iMod
						CASE 0
						CASE 1	RETURN 3
						CASE 2	RETURN 1
						CASE 3	RETURN 5
					ENDSWITCH
				BREAK
				CASE 8
					SWITCH iMod
						CASE 0	RETURN 2
						CASE 1
						CASE 2	RETURN 3
						CASE 3	RETURN 5
					ENDSWITCH
				BREAK
				CASE 9
				CASE 10
					SWITCH iMod
						CASE 0
						CASE 1	RETURN 2
						CASE 2	RETURN 5
						CASE 3	RETURN 5
					ENDSWITCH
				BREAK
				DEFAULT
					SWITCH iMod
						CASE 0
						CASE 1	RETURN 2
						CASE 2	RETURN 5
						CASE 3	RETURN 5
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_WEP_TYPE
			SWITCH iWave
				CASE 1
					SWITCH iMod
						CASE 3	RETURN 25
						DEFAULT RETURN 0
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iMod
						CASE 0
						CASE 1	RETURN 0
						CASE 2	RETURN 23
						CASE 3	RETURN 25
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iMod
						CASE 0	RETURN 16
						CASE 1
						CASE 2	RETURN 23
						CASE 3	RETURN 25
					ENDSWITCH
				BREAK
				CASE 4
					SWITCH iMod
						CASE 0	RETURN 16
						CASE 1	RETURN 33
						CASE 2	RETURN 23
						CASE 3	RETURN 25
					ENDSWITCH
				BREAK
				CASE 5
					SWITCH iMod
						CASE 0	RETURN 34
						CASE 1	RETURN 33
						CASE 2	RETURN 16
						CASE 3	RETURN 25
					ENDSWITCH
				BREAK
				CASE 6
					SWITCH iMod
						CASE 0	RETURN 24
						CASE 1	RETURN 33
						CASE 2	RETURN 16
						CASE 3	RETURN 51
					ENDSWITCH
				BREAK
				CASE 7
					SWITCH iMod
						CASE 0
						CASE 1	RETURN 33
						CASE 2	RETURN 16
						CASE 3	RETURN 51
					ENDSWITCH
				BREAK
				CASE 8
					SWITCH iMod
						CASE 0	RETURN 25
						CASE 1
						CASE 2	RETURN 33
						CASE 3	RETURN 51
					ENDSWITCH
				BREAK
				CASE 9
				CASE 10
					SWITCH iMod
						CASE 0
						CASE 1	RETURN 25
						CASE 2	RETURN 51
						CASE 3	RETURN 51
					ENDSWITCH
				BREAK
				DEFAULT
					SWITCH iMod
						CASE 0
						CASE 1	RETURN 25
						CASE 2	RETURN 51
						CASE 3	RETURN 51
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE ciSURVIVAL_DATA_HEAVY_NUM
			RETURN 0
		BREAK
		CASE ciSURVIVAL_DATA_HEAVY_HEALTH
			RETURN 4
		BREAK
		CASE ciSURVIVAL_DATA_HEAVY_ARMOUR
			RETURN 4
		BREAK
		CASE ciSURVIVAL_DATA_VEHICLE_COLOUR
			RETURN -1 //Random
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC MODEL_NAMES GET_SURVIVAL_MODEL_DATA_DEFAULT(INT iData, INT iWave, INT iMod)

	SWITCH iData
		CASE ciSURVIVAL_DATA_VEH_MODEL
			SWITCH iMod //Vehicle number
				CASE 0
					SWITCH iWave
						CASE 3
						CASE 5
						CASE 6
						CASE 8
						CASE 9 		RETURN SANDKING
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iWave
						CASE 3		
						CASE 6		RETURN SANDKING
						
						CASE 5		
						CASE 8
						CASE 9 		RETURN CAVALCADE
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iWave
						CASE 6
						CASE 8
						CASE 9 		RETURN SPEEDO
					ENDSWITCH
				BREAK
			ENDSWITCH
			RETURN SANDKING
		BREAK
		CASE ciSURVIVAL_DATA_PED_MODEL
			RETURN G_M_Y_Lost_01
		BREAK
		
		CASE ciSURVIVAL_DATA_AIR_MODEL
			RETURN BUZZARD2
		BREAK
		
		CASE ciSURVIVAL_DATA_HEAVY_MODEL
 			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC WEAPON_TYPE GET_SURVIVAL_DEFAULT_WEAPON_TYPE_FOR_WAVE(INT iWave, INT iWep)
	SWITCH iWave
		CASE 1
			SWITCH iWep
				CASE 3 	RETURN WEAPONTYPE_COMBATMG
				DEFAULT	RETURN WEAPONTYPE_PISTOL
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iWep
				CASE 0	
				CASE 1	RETURN WEAPONTYPE_PISTOL
				CASE 2	RETURN WEAPONTYPE_SMG
				CASE 3	RETURN WEAPONTYPE_COMBATMG
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iWep
				CASE 0	RETURN WEAPONTYPE_ASSAULTSHOTGUN
				CASE 1
				CASE 2	RETURN WEAPONTYPE_SMG
				CASE 3	RETURN WEAPONTYPE_COMBATMG
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH iWep
				CASE 0	RETURN WEAPONTYPE_ASSAULTSHOTGUN
				CASE 1	RETURN WEAPONTYPE_ASSAULTRIFLE
				CASE 2	RETURN WEAPONTYPE_SMG
				CASE 3	RETURN WEAPONTYPE_COMBATMG
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH iWep
				CASE 0	RETURN WEAPONTYPE_CARBINERIFLE
				CASE 1	RETURN WEAPONTYPE_ASSAULTRIFLE
				CASE 2	RETURN WEAPONTYPE_ASSAULTSHOTGUN
				CASE 3	RETURN WEAPONTYPE_COMBATMG
			ENDSWITCH
		BREAK
		CASE 6
			SWITCH iWep
				CASE 0	RETURN WEAPONTYPE_MG
				CASE 1	RETURN WEAPONTYPE_ASSAULTRIFLE
				CASE 2	RETURN WEAPONTYPE_ASSAULTSHOTGUN
				CASE 3	RETURN WEAPONTYPE_MINIGUN
			ENDSWITCH
		BREAK
		CASE 7
			SWITCH iWep
				CASE 0
				CASE 1	RETURN WEAPONTYPE_ASSAULTRIFLE
				CASE 2	RETURN WEAPONTYPE_ASSAULTSHOTGUN
				CASE 3	RETURN WEAPONTYPE_MINIGUN
			ENDSWITCH
		BREAK
		CASE 8
			SWITCH iWep
				CASE 0	RETURN WEAPONTYPE_COMBATMG
				CASE 1
				CASE 2	RETURN WEAPONTYPE_ASSAULTRIFLE
				CASE 3	RETURN WEAPONTYPE_MINIGUN
			ENDSWITCH
		BREAK
		CASE 9
		CASE 10
			SWITCH iWep
				CASE 0
				CASE 1	RETURN WEAPONTYPE_COMBATMG
				CASE 2	RETURN WEAPONTYPE_MINIGUN
				CASE 3	RETURN WEAPONTYPE_MINIGUN
			ENDSWITCH
		BREAK
		DEFAULT
			SWITCH iWep
				CASE 0
				CASE 1	RETURN WEAPONTYPE_COMBATMG
				CASE 2	RETURN WEAPONTYPE_MINIGUN
				CASE 3	RETURN WEAPONTYPE_MINIGUN
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN WEAPONTYPE_PISTOL
ENDFUNC

FUNC PICKUP_TYPE GET_DEFAULT_LOADOUT_WEAPON_FOR_SURVIVAL_WAVE(INT iWave, INT iWeaponIndex)

	IF iWeaponIndex > 6
		iWeaponIndex -= 5
	ENDIF
	
	SWITCH iWave
        CASE 1 
            SWITCH iWeaponIndex
                CASE 0                  RETURN  PICKUP_WEAPON_MICROSMG                        	BREAK
                CASE 1                  RETURN 	PICKUP_WEAPON_COMBATPISTOL                     	BREAK
                CASE 2                  RETURN  PICKUP_WEAPON_MICROSMG                          BREAK
                CASE 3                  RETURN  PICKUP_WEAPON_COMBATPISTOL                      BREAK
                CASE 4                  RETURN  PICKUP_WEAPON_MICROSMG                          BREAK
                CASE 5                  RETURN  PICKUP_WEAPON_COMBATPISTOL                      BREAK
                CASE 6                  RETURN  PICKUP_WEAPON_MICROSMG                          BREAK
            ENDSWITCH
        BREAK
		CASE 2
            SWITCH iWeaponIndex
                CASE 0                  RETURN  PICKUP_WEAPON_MICROSMG                      	BREAK
                CASE 1                  RETURN  PICKUP_WEAPON_ASSAULTRIFLE						BREAK
                CASE 2                  RETURN  PICKUP_WEAPON_MICROSMG 							BREAK
                CASE 3                  RETURN  PICKUP_WEAPON_ASSAULTRIFLE						BREAK
                CASE 4                  RETURN  PICKUP_WEAPON_MICROSMG                      	BREAK
                CASE 5                  RETURN  PICKUP_WEAPON_ASSAULTRIFLE						BREAK
                CASE 6                  RETURN  PICKUP_WEAPON_MICROSMG							BREAK
            ENDSWITCH
        BREAK
		CASE 3
            SWITCH iWeaponIndex
                CASE 0                  RETURN  PICKUP_WEAPON_COMBATMG                      	BREAK
                CASE 1                  RETURN  PICKUP_WEAPON_COMBATMG                      	BREAK
                CASE 2                  RETURN  PICKUP_WEAPON_COMBATMG							BREAK
                CASE 3                  RETURN  PICKUP_WEAPON_PUMPSHOTGUN						BREAK
                CASE 4                  RETURN  PICKUP_WEAPON_COMBATMG                      	BREAK
                CASE 5                  RETURN  PICKUP_WEAPON_PUMPSHOTGUN                     	BREAK
                CASE 6                  RETURN  PICKUP_WEAPON_COMBATMG							BREAK
            ENDSWITCH
        BREAK
		CASE 4
            SWITCH iWeaponIndex
                CASE 0                  RETURN  PICKUP_WEAPON_CARBINERIFLE                     	BREAK
                CASE 1                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 2                  RETURN  PICKUP_WEAPON_RPG								BREAK
                CASE 3                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 4                  RETURN  PICKUP_WEAPON_CARBINERIFLE                     	BREAK
                CASE 5                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 6                  RETURN  PICKUP_WEAPON_CARBINERIFLE                    	BREAK
            ENDSWITCH
        BREAK
		CASE 5
            SWITCH iWeaponIndex
                CASE 0                  RETURN  PICKUP_WEAPON_SAWNOFFSHOTGUN                  	BREAK
                CASE 1                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 2                  RETURN  PICKUP_WEAPON_SAWNOFFSHOTGUN 					BREAK
                CASE 3                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 4                  RETURN  PICKUP_WEAPON_SAWNOFFSHOTGUN                   	BREAK
                CASE 5                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                   	BREAK
                CASE 6                  RETURN  PICKUP_WEAPON_SAWNOFFSHOTGUN                   	BREAK
            ENDSWITCH
        BREAK
		CASE 6
            SWITCH iWeaponIndex
                CASE 0                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 1                  RETURN  PICKUP_WEAPON_HEAVYSNIPER                     	BREAK
                CASE 2                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE 					BREAK
                CASE 3                  RETURN  PICKUP_WEAPON_GRENADE                     		BREAK
                CASE 4                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 5                  RETURN  PICKUP_WEAPON_GRENADE                   		BREAK
                CASE 6                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
            ENDSWITCH
        BREAK
		CASE 7
            SWITCH iWeaponIndex
                CASE 0                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 1                  RETURN  PICKUP_WEAPON_HEAVYSNIPER                     	BREAK
                CASE 2                  RETURN  PICKUP_WEAPON_ASSAULTSHOTGUN 					BREAK
                CASE 3                  RETURN  PICKUP_WEAPON_RPG	                     		BREAK
                CASE 4                  RETURN  PICKUP_WEAPON_CARBINERIFLE                     	BREAK
                CASE 5                  RETURN  PICKUP_WEAPON_GRENADE                   		BREAK
                CASE 6                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
            ENDSWITCH
        BREAK
		CASE 8
            SWITCH iWeaponIndex
                CASE 0                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 1                  RETURN  PICKUP_WEAPON_GRENADELAUNCHER                  	BREAK
                CASE 2                  RETURN  PICKUP_WEAPON_ASSAULTSHOTGUN 					BREAK
                CASE 3                  RETURN  PICKUP_WEAPON_GRENADE                     		BREAK
                CASE 4                  RETURN  PICKUP_WEAPON_CARBINERIFLE                     	BREAK
                CASE 5                  RETURN  PICKUP_WEAPON_STICKYBOMB                   		BREAK
                CASE 6                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
            ENDSWITCH
        BREAK
		CASE 9
            SWITCH iWeaponIndex
                CASE 0                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 1                  RETURN  PICKUP_WEAPON_GRENADELAUNCHER                  	BREAK
                CASE 2                  RETURN  PICKUP_WEAPON_ASSAULTSHOTGUN 					BREAK
                CASE 3                  RETURN  PICKUP_WEAPON_GRENADE                     		BREAK
                CASE 4                  RETURN  PICKUP_WEAPON_MINIGUN                     		BREAK
                CASE 5                  RETURN  PICKUP_WEAPON_STICKYBOMB                   		BREAK
                CASE 6                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
            ENDSWITCH
        BREAK
		CASE 10
            SWITCH iWeaponIndex
                CASE 0                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 1                  RETURN  PICKUP_WEAPON_GRENADELAUNCHER                  	BREAK
                CASE 2                  RETURN  PICKUP_WEAPON_MINIGUN 							BREAK
                CASE 3                  RETURN  PICKUP_WEAPON_RPG			                  	BREAK
                CASE 4                  RETURN  PICKUP_WEAPON_MINIGUN                     		BREAK
                CASE 5                  RETURN  PICKUP_WEAPON_STICKYBOMB                   		BREAK
                CASE 6                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
            ENDSWITCH
        BREAK
		DEFAULT
			SWITCH iWeaponIndex
                CASE 0                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
                CASE 1                  RETURN  PICKUP_WEAPON_GRENADELAUNCHER                  	BREAK
                CASE 2                  RETURN  PICKUP_WEAPON_MINIGUN 							BREAK
                CASE 3                  RETURN  PICKUP_WEAPON_RPG			                  	BREAK
                CASE 4                  RETURN  PICKUP_WEAPON_MINIGUN                     		BREAK
                CASE 5                  RETURN  PICKUP_WEAPON_STICKYBOMB                   		BREAK
                CASE 6                  RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    	BREAK
            ENDSWITCH
		BREAK
    ENDSWITCH
	
	RETURN PICKUP_WEAPON_RPG
ENDFUNC

PROC COPY_WAVE_TO_WAVE(INT iWaveToCopyFrom, INT iWaveToCopyTo)
	IF iWaveToCopyFrom = iWaveToCopyTo
		EXIT
	ENDIF
	PRINTLN("COPY_WAVE_TO_WAVE - Copying from wave: ", iWaveToCopyFrom, " to wave: ", iWaveToCopyTo)
	INT i2
	g_FMMC_STRUCT.sSurvivalWaveData.iKillsToProgressStage[iWaveToCopyTo] 	= g_FMMC_STRUCT.sSurvivalWaveData.iKillsToProgressStage[iWaveToCopyFrom]
	FOR i2 = 0 TO MAX_SURV_WAVE_DIFF-1
		g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[iWaveToCopyTo][i2] 	= g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[iWaveToCopyFrom][i2] 	
		g_FMMC_STRUCT.sSurvivalWaveData.iPedsPerSquad[iWaveToCopyTo][i2]	= g_FMMC_STRUCT.sSurvivalWaveData.iPedsPerSquad[iWaveToCopyFrom][i2]	
		g_FMMC_STRUCT.sSurvivalWaveData.iHealth[iWaveToCopyTo][i2] 			= g_FMMC_STRUCT.sSurvivalWaveData.iHealth[iWaveToCopyFrom][i2] 			
		g_FMMC_STRUCT.sSurvivalWaveData.iArmour[iWaveToCopyTo][i2] 			= g_FMMC_STRUCT.sSurvivalWaveData.iArmour[iWaveToCopyFrom][i2] 			
		g_FMMC_STRUCT.sSurvivalWaveData.iAccuracy[iWaveToCopyTo][i2] 		= g_FMMC_STRUCT.sSurvivalWaveData.iAccuracy[iWaveToCopyFrom][i2] 		
		g_FMMC_STRUCT.sSurvivalWaveData.iCombatAbility[iWaveToCopyTo][i2] 	= g_FMMC_STRUCT.sSurvivalWaveData.iCombatAbility[iWaveToCopyFrom][i2] 	
		g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToCopyTo][i2] 		= g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToCopyFrom][i2] 		
		//Heavy Units
		g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHeavies[iWaveToCopyTo][i2] 	= g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHeavies[iWaveToCopyFrom][i2] 
		g_FMMC_STRUCT.sSurvivalWaveData.iHealthMultiplier[iWaveToCopyTo][i2] 	= g_FMMC_STRUCT.sSurvivalWaveData.iHealthMultiplier[iWaveToCopyFrom][i2]
		g_FMMC_STRUCT.sSurvivalWaveData.iArmourMultiplier[iWaveToCopyTo][i2] 	= g_FMMC_STRUCT.sSurvivalWaveData.iArmourMultiplier[iWaveToCopyFrom][i2]
		g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToCopyTo][i2] 		= g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToCopyFrom][i2] 	
	ENDFOR
	g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[iWaveToCopyTo] 			= g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[iWaveToCopyFrom] 
	g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[iWaveToCopyTo] 			= g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[iWaveToCopyFrom]
	FOR i2 = 0 TO MAX_SURV_LAND_VEH-1
		g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[iWaveToCopyTo][i2] 		= g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[iWaveToCopyFrom][i2]
		g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToCopyTo][i2] 			= g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToCopyFrom][i2] 	
		g_FMMC_STRUCT.sSurvivalWaveData.iVehHealth[iWaveToCopyTo][i2] 		= g_FMMC_STRUCT.sSurvivalWaveData.iVehHealth[iWaveToCopyFrom][i2] 
		g_FMMC_STRUCT.sSurvivalWaveData.iVehColour[iWaveToCopyTo][i2] 		= g_FMMC_STRUCT.sSurvivalWaveData.iVehColour[iWaveToCopyFrom][i2]
	ENDFOR
	FOR i2 = 0 TO MAX_SURV_HELI-1
		g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToCopyTo][i2] 			= g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToCopyFrom][i2] 	
		g_FMMC_STRUCT.sSurvivalWaveData.iAirHealth[iWaveToCopyTo][i2] 		= g_FMMC_STRUCT.sSurvivalWaveData.iAirHealth[iWaveToCopyFrom][i2]
		g_FMMC_STRUCT.sSurvivalWaveData.iAirColour[iWaveToCopyTo][i2] 		= g_FMMC_STRUCT.sSurvivalWaveData.iAirColour[iWaveToCopyFrom][i2] 
	ENDFOR
	g_FMMC_STRUCT.sSurvivalWaveData.iEnemyQuantity[iWaveToCopyTo] 			= g_FMMC_STRUCT.sSurvivalWaveData.iEnemyQuantity[iWaveToCopyFrom] 
	g_FMMC_STRUCT.sSurvivalWaveData.iSubStages[iWaveToCopyTo] 				= g_FMMC_STRUCT.sSurvivalWaveData.iSubStages[iWaveToCopyFrom] 		
	g_FMMC_STRUCT.sSurvivalWaveData.iWaveDifficulty[iWaveToCopyTo] 			= g_FMMC_STRUCT.sSurvivalWaveData.iWaveDifficulty[iWaveToCopyFrom] 
	
	FOR i2 = 0 TO MAX_SQUAD_WEAPONS-1
		g_FMMC_STRUCT.sSurvivalWaveData.wtEnemyWeapons[iWaveToCopyTo][i2]	= g_FMMC_STRUCT.sSurvivalWaveData.wtEnemyWeapons[iWaveToCopyFrom][i2]	
		g_FMMC_STRUCT.sSurvivalWaveData.iSquadWeaponCat[iWaveToCopyTo][i2]	= g_FMMC_STRUCT.sSurvivalWaveData.iSquadWeaponCat[iWaveToCopyFrom][i2]
		g_FMMC_STRUCT.sSurvivalWaveData.iSquadWeaponType[iWaveToCopyTo][i2]	= g_FMMC_STRUCT.sSurvivalWaveData.iSquadWeaponType[iWaveToCopyFrom][i2]
	ENDFOR
	
	FOR i2 = 0 TO MAX_LOADOUT_WEAPONS-1
		g_FMMC_STRUCT.sSurvivalWaveData.wtLoadoutWeapons[iWaveToCopyTo][i2]		= g_FMMC_STRUCT.sSurvivalWaveData.wtLoadoutWeapons[iWaveToCopyFrom][i2]	
		g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutWeaponCat[iWaveToCopyTo][i2] 	= g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutWeaponCat[iWaveToCopyFrom][i2] 
		g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutWeaponType[iWaveToCopyTo][i2]	= g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutWeaponType[iWaveToCopyFrom][i2]
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehiclesOnWave, iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehiclesOnWave, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehiclesOnWave, iWaveToCopyTo)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehiclesOnWave, iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehiclesOnWave, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehiclesOnWave, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iSquadChangeBitset, 	iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iSquadChangeBitset, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iSquadChangeBitset, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iPedsChangeBitset, 	iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iPedsChangeBitset, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iPedsChangeBitset, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehChangeBitset, iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehChangeBitset, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehChangeBitset, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iLandModChangeBitset, iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandModChangeBitset, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandModChangeBitset, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehChangeBitset, 	iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehChangeBitset, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehChangeBitset, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iAirModChangeBitset, 	iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirModChangeBitset, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirModChangeBitset, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iPedModelChangeBitset, iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iPedModelChangeBitset, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iPedModelChangeBitset, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iDifficultyChangeBitset, iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iDifficultyChangeBitset, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iDifficultyChangeBitset, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet,	iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyChangeBitset, iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyChangeBitset, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyChangeBitset, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyModChangeBitset, iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyModChangeBitset, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyModChangeBitset, iWaveToCopyTo)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyOnWave, iWaveToCopyFrom)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyOnWave, iWaveToCopyTo)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyOnWave, iWaveToCopyTo)
	ENDIF
	
	
	
	
	INT iWeapon
	FOR iWeapon = 0 TO FMMC_MAX_WEAPONS - 1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].ptSurvival_WeaponOverridePerWave[iWaveToCopyTo] 	= g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].ptSurvival_WeaponOverridePerWave[iWaveToCopyFrom]
		g_FMMC_STRUCT_ENTITIES.sEditedWeapon.ptSurvival_WeaponOverridePerWave[iWaveToCopyTo] 			= g_FMMC_STRUCT_ENTITIES.sEditedWeapon.ptSurvival_WeaponOverridePerWave[iWaveToCopyFrom] 		
	ENDFOR
ENDPROC

PROC RESET_WAVE_TO_DEFAULT(INT iWave)
	PRINTLN("RESET_WAVE_TO_DEFAULT - Resetting Wave: ", iWave)
	IF iWave >= MAX_SURVIVAL_WAVES
		EXIT
	ENDIF
	INT i2
	g_FMMC_STRUCT.sSurvivalWaveData.iKillsToProgressStage[iWave] 	= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_KILLS_STAGE, iWave, i2)
	FOR i2 = 0 TO MAX_SURV_WAVE_DIFF-1
		g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[iWave][i2] 	= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_SQUADS, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iPedsPerSquad[iWave][i2]	= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_PEDS_SQUAD, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iHealth[iWave][i2] 			= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_PED_HEALTH, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iArmour[iWave][i2] 			= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_PED_ARMOUR, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iAccuracy[iWave][i2] 		= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_PED_ACCURACY, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iCombatAbility[iWave][i2] 	= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_PED_COMBAT, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWave][i2] 		= GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_PED_MODEL, iWave, i2)
		//Heavy Units
		g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHeavies[iWave][i2] 	= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_HEAVY_NUM, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iHealthMultiplier[iWave][i2] 	= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_HEAVY_HEALTH, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iArmourMultiplier[iWave][i2] 	= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_HEAVY_ARMOUR, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWave][i2] 		= GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_HEAVY_MODEL, iWave, i2)
	ENDFOR
	g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[iWave] 			= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_VEH_NUMBER, iWave, i2)
	g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[iWave] 			= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_HELI_NUM, iWave, i2)
	FOR i2 = 0 TO MAX_SURV_LAND_VEH-1
		g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[iWave][i2] 		= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_VEH_PEDS, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWave][i2] 			= GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_VEH_MODEL, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iVehHealth[iWave][i2] 		= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_VEH_HEALTH, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iVehColour[iWave][i2] 		= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_VEHICLE_COLOUR, iWave, i2)
	ENDFOR
	FOR i2 = 0 TO MAX_SURV_HELI-1
		g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWave][i2] 			= GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_AIR_MODEL, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iAirHealth[iWave][i2] 		= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_AIR_HEALTH, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iAirColour[iWave][i2] 		= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_VEHICLE_COLOUR, iWave, i2)
	ENDFOR
	g_FMMC_STRUCT.sSurvivalWaveData.iEnemyQuantity[iWave] 			= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_ENEMY_NUM, iWave, i2)
	g_FMMC_STRUCT.sSurvivalWaveData.iSubStages[iWave] 				= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_SUB_STAGES, iWave, i2)
	g_FMMC_STRUCT.sSurvivalWaveData.iWaveDifficulty[iWave] 			= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_DIFFICULTY, iWave, i2)
	
	FOR i2 = 0 TO MAX_SQUAD_WEAPONS-1
		g_FMMC_STRUCT.sSurvivalWaveData.wtEnemyWeapons[iWave][i2]	= GET_SURVIVAL_DEFAULT_WEAPON_TYPE_FOR_WAVE(iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iSquadWeaponCat[iWave][i2]	= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_WEP_CAT, iWave, i2)
		g_FMMC_STRUCT.sSurvivalWaveData.iSquadWeaponType[iWave][i2]	= GET_SURVIVAL_DATA_DEFAULT(ciSURVIVAL_DATA_WEP_TYPE, iWave, i2)
	ENDFOR
	
	FOR i2 = 0 TO MAX_LOADOUT_WEAPONS-1
		g_FMMC_STRUCT.sSurvivalWaveData.wtLoadoutWeapons[iWave][i2]	= GET_WEAPON_TYPE_FROM_PICKUP_TYPE(GET_DEFAULT_LOADOUT_WEAPON_FOR_SURVIVAL_WAVE(iWave, i2))
		g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutWeaponCat[iWave][i2] = 0
		g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutWeaponType[iWave][i2]	= 0
	ENDFOR
	
	IF IS_BIT_SET(ciSURVIVAL_DEFAULT_VEH_BITSET, iWave)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehiclesOnWave, iWave)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehiclesOnWave, iWave)
	ENDIF
	IF IS_BIT_SET(ciSURVIVAL_DEFAULT_AIR_BITSET, iWave)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehiclesOnWave, iWave)
	ELSE
		CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehiclesOnWave, iWave)
	ENDIF
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iSquadChangeBitset, 	iWave)
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iPedsChangeBitset, 	iWave)
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehChangeBitset, iWave)
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandModChangeBitset, iWave)
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehChangeBitset, 	iWave)
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirModChangeBitset, 	iWave)
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iPedModelChangeBitset, iWave)
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iDifficultyChangeBitset, iWave)
	//Heavies
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyChangeBitset, iWave)
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyModChangeBitset, iWave)
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyOnWave, iWave)
	
	CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet,	iWave)
	
	INT iWeapon
	FOR iWeapon = 0 TO FMMC_MAX_WEAPONS - 1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeapon].ptSurvival_WeaponOverridePerWave[iWave] = NUM_PICKUPS
		g_FMMC_STRUCT_ENTITIES.sEditedWeapon.ptSurvival_WeaponOverridePerWave[iWave] = NUM_PICKUPS
	ENDFOR
ENDPROC

PROC RESET_SURVIVAL_MODELS_TO_CREATOR_SET_DEFAULT(INT iWave)
	INT i
	FOR i = 0 TO MAX_SURV_WAVE_DIFF-1
		g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWave][i] = g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[0][0]
		g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWave][i] = g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[0][0]
	ENDFOR
	FOR i = 0 TO MAX_SURV_LAND_VEH-1
		g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWave][i] = g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[0][0]
	ENDFOR
	FOR i = 0 TO MAX_SURV_HELI-1
		g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWave][i] = g_FMMC_STRUCT.sSurvivalWaveData.mnAir[0][0]
	ENDFOR
ENDPROC

PROC RESET_SURVIVAL_DATA()
	INT i
	g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves = 10
	g_FMMC_STRUCT.sSurvivalWaveData.iBitset = 0
	FOR i = 1 TO MAX_SURVIVAL_WAVES-1
		RESET_WAVE_TO_DEFAULT(i)
		g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutOverrideNoneBitSet[i] = 0 //Not in function as we don't want this to be done when clearing a single wave.
	ENDFOR
	FOR i = 0 TO MAX_SURV_BOUNDS_VEC-1
		g_FMMC_STRUCT.sSurvivalWaveData.vBounds[i] = <<0,0,0>>
	ENDFOR
	g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius = DEFAULT_SURV_BOUNDS_SIZE
	g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType = SURV_BOUNDS_NON_AXIS_AREA
	g_FMMC_STRUCT.sSurvivalWaveData.fBoundsHeight = 0.0
	g_FMMC_STRUCT.sSurvivalWaveData.fBoundsBaseOffset = 0.0
	g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet = 0
	
	g_FMMC_STRUCT.sSurvivalWaveData.iLandVehiclesOnWave				= ciSURVIVAL_DEFAULT_VEH_BITSET
	g_FMMC_STRUCT.sSurvivalWaveData.iAirVehiclesOnWave				= ciSURVIVAL_DEFAULT_AIR_BITSET
	g_FMMC_STRUCT.sSurvivalWaveData.iHeavyOnWave					= 0
	g_FMMC_STRUCT.sSurvivalWaveData.iSquadChangeBitset				= 0
	g_FMMC_STRUCT.sSurvivalWaveData.iPedsChangeBitset				= 0
	g_FMMC_STRUCT.sSurvivalWaveData.iLandVehChangeBitset			= 0
	g_FMMC_STRUCT.sSurvivalWaveData.iLandModChangeBitset			= 0
	g_FMMC_STRUCT.sSurvivalWaveData.iAirVehChangeBitset				= 0
	g_FMMC_STRUCT.sSurvivalWaveData.iAirModChangeBitset				= 0
	g_FMMC_STRUCT.sSurvivalWaveData.iHeavyChangeBitset				= 0
	g_FMMC_STRUCT.sSurvivalWaveData.iHeavyModChangeBitset			= 0
	
	//Wave ZERO stuff
	g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[0][0] 		= GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_PED_MODEL, 0, 0)
	g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[0][0] 			= GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_VEH_MODEL, 0, 0)
	g_FMMC_STRUCT.sSurvivalWaveData.mnAir[0][0] 			= GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_AIR_MODEL, 0, 0)
	g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[0][0] 		= GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_HEAVY_MODEL, 0, 0)
ENDPROC

PROC CLEAR_VEHICLE_HEALTH_THRESHOLD_STRUCT(VEHICLE_HEALTH_THRESHOLD_STRUCT &sStructToClear)
	sStructToClear.iBodyHealth = 0
	sStructToClear.iEndRule = -1
	sStructToClear.iTeamToCheck = 0
	sStructToClear.iThresholdBitset = 0
ENDPROC

PROC CLEAR_VEHICLE_ENGINE_SMOKE_STRUCT(VEHICLE_ENGINE_SMOKE_STRUCT &sSmokeStruct)
	sSmokeStruct.iEngineSmokeHealthPercentage = 0
	sSmokeStruct.iRuleStart = -1
	sSmokeStruct.iSmokeThickness = 0
	sSmokeStruct.iTeam = -1
ENDPROC

PROC CLEAR_VEHICLE_CUSTOM_HEALTH_SCALE(FLOAT &fVehCustomHealthScaleArray[])
	fVehCustomHealthScaleArray[ciFMMC_VEHICLE_CUSTOM_HEALTH_SCALE_TWO_PLAYERS] = 2.0
	fVehCustomHealthScaleArray[ciFMMC_VEHICLE_CUSTOM_HEALTH_SCALE_THREE_PLAYERS] = 3.0
	fVehCustomHealthScaleArray[ciFMMC_VEHICLE_CUSTOM_HEALTH_SCALE_FOUR_PLAYERS] = 4.0
ENDPROC

PROC CLEAR_VEHICLE_STRUCT(FMMCplacedVehicleStruct &sVehicleStruct)
	
	INT i, ii
	sVehicleStruct.vPos = <<0.0,0.0,0.0>>
	sVehicleStruct.fHead = 0.0
	sVehicleStruct.fRespawnRange = 50.0
	sVehicleStruct.fRespawnMinRange = 0.0
	sVehicleStruct.iSpawnNearType = 0
	sVehicleStruct.iSpawnNearEntityID = -1
	sVehicleStruct.vSecondSpawnPos = <<0, 0, 0>>
	sVehicleStruct.fSecondHeading = 0
	sVehicleStruct.mn = DUMMY_MODEL_FOR_SCRIPT
	sVehicleStruct.iColour = -1
	sVehicleStruct.iColour2 = -1
	sVehicleStruct.iColour3 = -1
	sVehicleStruct.iColourCombination = -1
	sVehicleStruct.iVehicleRespawnLives = 0
	sVehicleStruct.iHealth = 100
	sVehicleStruct.fEngineHealth = 1001.0
	sVehicleStruct.iHealthBarInRange = 0 
	sVehicleStruct.iHealthBarRule = -1
	sVehicleStruct.fPetrolHealth = 1001.0
	sVehicleStruct.iHealthBarPercentageLine = 0
	sVehicleStruct.iHealthBarPretendZeroIs = 0
	sVehicleStruct.iHealthBarTurnRedAtPercent = 0
	sVehicleStruct.iHealthBarCustomTitleStringIndex = -1
	sVehicleStruct.fCargobobDamp = 1.5
	sVehicleStruct.iVehBitSet = 0
	sVehicleStruct.iVehBitsetTwo = 0
	sVehicleStruct.iVehBitsetThree = 0
	sVehicleStruct.iVehBitsetFour = 0
	sVehicleStruct.iVehBitsetFive = 0
	sVehicleStruct.iVehBitsetSix = 0
	sVehicleStruct.iVehBitsetSeven = 0
	sVehicleStruct.iVehBitsetEight = 0
	sVehicleStruct.iVehBitsetNine = 0
	sVehicleStruct.iVehBitsetTen = 0
	
	sVehicleStruct.iUnbreakableDoorBitset = 0
	sVehicleStruct.iLivery = -1
	sVehicleStruct.iModPreset = -1
	sVehicleStruct.fEnvEff = -1.0
	CLEAR_ENTITY_BLIP_DATA_STRUCT(sVehicleStruct.sVehBlipStruct)
	sVehicleStruct.sVehBlipStruct.iBlipColour = 0
	sVehicleStruct.sVehBlipStruct.iBlipColourAggro = 0
	sVehicleStruct.sVehBlipStruct.fBlipScale = 1.2
	sVehicleStruct.sVehBlipStruct.iBlipSpriteOverride = 0
	sVehicleStruct.sVehBlipStruct.fBlipRange = 0.0
	sVehicleStruct.sVehBlipStruct.iBlipStyle = 0
	sVehicleStruct.iVehicleFlashBlipRule = -1
	sVehicleStruct.iVehicleFlashBlipDuration = 10
	sVehicleStruct.iVehInterior = -1
	sVehicleStruct.iVehInteriorRoomKey = -1
	sVehicleStruct.iSecondInterior = -1
	sVehicleStruct.iTagColour = 1
	sVehicleStruct.iBurningLifetime = 60
	sVehicleStruct.fFirePutOutTime = 4.5
	sVehicleStruct.iBurningVehicleBS = 0
	sVehicleStruct.iDeluxoIndex = -1
	sVehicleStruct.iBurstTyresRespawnIndex = -1
	sVehicleStruct.iAkulaTimeToResetWantedLvl = 0
	sVehicleStruct.fDamageScalarWithMap = 1.0
	sVehicleStruct.iVModTurret = -1
	sVehicleStruct.iVModArmour = -1
	sVehicleStruct.iVModAirCounter = -1
	sVehicleStruct.iVModBombBay = 0
	sVehicleStruct.iVModExhaust = 0
	sVehicleStruct.iAssociatedObjective = -1
	sVehicleStruct.iSecondAssociatedObjective = -1
    sVehicleStruct.iThirdAssociatedObjective  = -1
    sVehicleStruct.iFourthAssociatedObjective = -1
	sVehicleStruct.iAssociatedTeam = -1
	sVehicleStruct.iSecondAssociatedTeam   = -1
    sVehicleStruct.iThirdAssociatedTeam    = -1
    sVehicleStruct.iFourthAssociatedTeam   = -1
	sVehicleStruct.iAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_OFF
	sVehicleStruct.iSecondAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_OFF
	sVehicleStruct.iThirdAssociatedSpawn  = ciOBJECTIVE_SPAWN_LIMIT_OFF
	sVehicleStruct.iFourthAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_OFF
	sVehicleStruct.iCleanupObjective = -1
	sVehicleStruct.iCleanupTeam = -1
	sVehicleStruct.iCleanupVehicle = -1
	sVehicleStruct.iCSRespawnShotIndex = -1
	sVehicleStruct.iCSRespawnSceneIndex = -1
	sVehicleStruct.iCleanupRange = -1
	sVehicleStruct.iRespawnDelay = 0
	sVehicleStruct.iSpawnGroup = 0
	sVehicleStruct.iSpawnSubGroup = 0
	sVehicleStruct.iModifySpawnGroupOnEvent = 0
	sVehicleStruct.iSpawnGroupAdditionalFunctionalityBS = 0
	FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		sVehicleStruct.iSpawnSubGroupBS[ii] = 0
		sVehicleStruct.iModifySpawnSubGroupOnEventBS[ii] = 0
	ENDFOR
	sVehicleStruct.iVehPhotoRange = 0
	sVehicleStruct.iVehTeamHorn = -1
	sVehicleStruct.vDropOffOverride = <<0,0,0>>
	sVehicleStruct.fVehDropOffRadius = 0
	sVehicleStruct.fVehDropOffVisualRadius = 0
	sVehicleStruct.iVehWeaponDamage = -1
	sVehicleStruct.iLinkedDestroyVeh = -1
	sVehicleStruct.fVelocityReqForExplosion = -1
	sVehicleStruct.iEntityIndexToCheck = -1
	sVehicleStruct.iSpawnPlayerNum = 0
	sVehicleStruct.iTeamSpawnReq = -1
	sVehicleStruct.iTeamSpawnPlayersReq = 0
	sVehicleStruct.iCargobobAltitude = 0
	sVehicleStruct.iCargobobDropTime = 5
	sVehicleStruct.iRemoveHomingLockRule = -1 
	sVehicleStruct.iDuneFlipScale = 10
	sVehicleStruct.iRespawnOnRuleChangeBS = 0
	INITIALISE_INT_ARRAY(sVehicleStruct.iRespawnOnRule, -1)
	sVehicleStruct.iCargobobEntitySelectType = 2
	sVehicleStruct.iCargobobEntitySelectID = -1
	sVehicleStruct.fVehicleDelayedDoorRange = -1
	sVehicleStruct.iVehicleDelayedDoorTimer = 0
	sVehicleStruct.iVehicleDelayedDoorRule = -1
	sVehicleStruct.iVehicleDelayedDoorRule_Team = -1
	sVehicleStruct.eVehicleDelayedDoorWhichDoor = SC_DOOR_FRONT_LEFT
	sVehicleStruct.iVehicleDoorSettingsOnRule = -1
	sVehicleStruct.iCaptureRange = 10
	sVehicleStruct.iCapturePromptCustomString = -1
	sVehicleStruct.iCaptureBarCustomString = -1
	sVehicleStruct.iCleanupDetachedTrailerOnRule = -1
	sVehicleStruct.iCleanupVehicleDistance = 0
	sVehicleStruct.iObjectivePrereq = ciPREREQ_None
	sVehicleStruct.iVehicleIndexForColImmunity = -1
	sVehicleStruct.fBodyHealth = 1001.0
	sVehicleStruct.fHeliMainRotorHealth = 1001.0
	sVehicleStruct.iKinematicPhysicsBS	 = 0
	sVehicleStruct.iKinematicPhysicsTeam = 0
	sVehicleStruct.iCleanupDelay = 0
	
	CLEAR_CLEANUP_DATA(sVehicleStruct.sCleanupData)
	
	CLEAR_ENTITY_PTFX_DATA(sVehicleStruct.sEntitySpawnPTFX)
	CLEAR_ENTITY_PTFX_DATA(sVehicleStruct.sEntityDespawnPTFX)
	
	FOR ii = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
		sVehicleStruct.eSpawnConditionFlag[ii] = SPAWN_CONDITION_FLAG_NOT_SET
	ENDFOR
	sVehicleStruct.iHealthDamageRequiredForRule = -1		
	FOR ii = 0 TO 0
		sVehicleStruct.iHealthDamageCombinedEntityIndex = -1
	ENDFOR
	sVehicleStruct.fRespawnRadius = 100.0
	sVehicleStruct.iAttachParent = -1
	sVehicleStruct.iAttachParentType = 1
	sVehicleStruct.iCustomVehName = -1
	sVehicleStruct.iDoorUnlockRule = -1
	sVehicleStruct.iGotoRange = 0
	sVehicleStruct.iInvincibleOnRules = -1
	sVehicleStruct.iInvincibleOnRulesTeam = 0
	sVehicleStruct.iWaterBombCapacity = ciVEHICLE_WATER_BOMB_DEFAULT_CAPACITY
	sVehicleStruct.iWaterBombStartingAmmo = ciVEHICLE_WATER_BOMB_DEFAULT_CAPACITY
	sVehicleStruct.iWaterBombRefillRate = ciVEHICLE_WATER_BOMB_DEFAULT_REFILL_RATE
	sVehicleStruct.iLockOnDeliveryRule = -1
	sVehicleStruct.iLockOnDeliveryTeam = -1
	sVehicleStruct.iVehicleExtrasBitset = 0
	sVehicleStruct.vRotation = <<0,0,0>>
	sVehicleStruct.iEntityRelGroupDamageRestrictions = 0
	sVehicleStruct.iVehBecomesUnstuckOnRule	= -1
	sVehicleStruct.iVehBecomesLockedOnRule = -1
	sVehicleStruct.iWantedLevelOnTheft = 0
	sVehicleStruct.iVehicleSpecificTheftWantedDelay = 0
	sVehicleStruct.iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_Off
	sVehicleStruct.iVehGetawayVehicleSlot = -1
	sVehicleStruct.iSeatBitset	= 0
	
	sVehicleStruct.iImmovableViaZone_BS		= 0
	sVehicleStruct.iImmovableRule			= -1
	sVehicleStruct.iImmovableTeam			= -1
	CLEAR_VEHICLE_ENGINE_SMOKE_STRUCT(sVehicleStruct.sEngineSmoke)
	
	sVehicleStruct.iVehicleInVehicle_ContainerVehicleIndex		= -1
	sVehicleStruct.iVehicleInVehicle_EmergeRule					= -1
	sVehicleStruct.iVehicleInVehicle_EmergeTeam					= -1
	sVehicleStruct.iVehicleInVehicle_EmergePreReq 				= -1
	sVehicleStruct.fVehicleInVehicle_EmergeForceMultiplier 		= 1.0
	
	CLEAR_ENTITY_END_MISSION_DATA_STRUCT(sVehicleStruct.sEndMissionData)
	
	REPEAT FMMC_MAX_RULES i
		sVehicleStruct.iVehicleBlipVisibiltyRange[i] = -2
	ENDREPEAT
	
	CLEAR_WARP_LOCATION_SETTINGS(sVehicleStruct.sWarpLocationSettings)
	
	REPEAT FMMC_MAX_RESTART_CHECKPOINTS i
		sVehicleStruct.vRestartPos[i] = <<0,0,0>>
		sVehicleStruct.fRestartHeading[i] = 0.0
		sVehicleStruct.iRestartInterior[i] = -1
	ENDREPEAT
	
	REPEAT FMMC_MAX_TEAMS i
		sVehicleStruct.iRule[i] = FMMC_OBJECTIVE_LOGIC_NONE
		sVehicleStruct.iPriority[i] = FMMC_PRIORITY_IGNORE
		sVehicleStruct.iTeamSeatPreference[i] = ciFMMC_SeatPreference_None
		sVehicleStruct.iJumpToObjectivePass[i] = 0
		sVehicleStruct.iJumpToObjectiveFail[i] = 0
		sVehicleStruct.sVehBlipStruct.sTeamBlip[i].iBlipOverrideStartRule = -1
		sVehicleStruct.sVehBlipStruct.sTeamBlip[i].iBlipOverrideEndRule = -1
		sVehicleStruct.sVehBlipStruct.sTeamBlip[i].iBlipOverrideBitSet = 0
		sVehicleStruct.iMissionCriticalRemovalRule[i] = 0
	ENDREPEAT
	
	sVehicleStruct.iMissionCriticalLinkedVeh = -1
	
	REPEAT 3 i
		sVehicleStruct.iChevronColours[i] = 0
	ENDREPEAT
	
	sVehicleStruct.iVehMCVarAssociatedSpawnIndex = -1	
	sVehicleStruct.iContinuityID = -1	
	sVehicleStruct.iContentContinuityType = 0
	sVehicleStruct.iZoneBlockEntitySpawn = -1
	sVehicleStruct.iZoneBlockEntitySpawnPlayerReq = 0
	
	sVehicleStruct.eVehRadioSequenceType = VEHICLE_RADIO_SEQUENCE_TYPE_NONE
	sVehicleStruct.iVehRadioSequenceDelay = 0
	sVehicleStruct.iVehRadio = -1
	sVehicleStruct.iVehAudioStream = -1
	sVehicleStruct.iAudioStartRule = -1
	sVehicleStruct.iAudioStartTeam = -1
	sVehicleStruct.iCleanupDelayTeam = -1
	sVehicleStruct.iCleanupDelayRule = -1
	
	CLEAR_VEHICLE_CUSTOM_HEALTH_SCALE(sVehicleStruct.fCustomHealthScales)
	
	INITIALISE_INT_ARRAY(sVehicleStruct.iAltVarsBitsetVeh, 0)
	INITIALISE_INT_ARRAY(sVehicleStruct.iSetPrereqsOnEnterBS, 0)
	CLEAR_SELF_DESTRUCT_STRUCT(sVehicleStruct.sSelfDestructSettings)
	
	sVehicleStruct.iAggroIndexBS_Entity_Veh = ciMISSION_AGGRO_INDEX_DEFAULT
	
	CLEAR_PROXIMITY_SPAWNING_STRUCT(sVehicleStruct.sProximitySpawningData)
	
	sVehicleStruct.iVehPointsToGive           =  0
	sVehicleStruct.iVehGivePointsOnRuleStart  = -1
	sVehicleStruct.iVehGivePointsOnRuleEnd    = -1
	
	sVehicleStruct.iCargo = ciVEHICLE_CARGO_OFF
	sVehicleStruct.iVehCargoExplodeTeam = -1
	sVehicleStruct.iVehCargoExplodeRule = -1
	sVehicleStruct.iVehCargoBeepTime = -1
	sVehicleStruct.fDirtLevel = 0.0
	
	CLEAR_TRAIN_ATTACH_STRUCT(sVehicleStruct.sTrainAttachmentData)
	
	CLEAR_VEHICLE_HEALTH_THRESHOLD_STRUCT(sVehicleStruct.sHealthThreshold)
ENDPROC

// clear player ability struct
PROC CLEAR_PLAYER_ABILITY_STRUCT(FMMC_PLAYER_ABILITY_STRUCT& sPlayerAbilityData)
	sPlayerAbilityData.iActiveDuration = ciPLAYER_ABILITY_DEFAULT_ACTIVE_DURATION	
	sPlayerAbilityData.iCooldownDuration = ciPLAYER_ABILITY_DEFAULT_COOLDOWN_DURATION
	sPlayerAbilityData.iNumUses = ciPLAYER_ABILITY_DEFAULT_NUM_USES
	sPlayerAbilityData.iAbilityBitset = ciPLAYER_ABILITY_DEFAULT_ABILITY_BITSET
	sPlayerAbilityData.iRequiredPrerequisite = 0
ENDPROC

// clear support sniper struct
PROC CLEAR_SUPPORT_SNIPER_ABILITY_STRUCT(FMMC_SUPPORT_SNIPER_ABILITY_STRUCT& sSupportSniperData)
	sSupportSniperData.iDeployDuration = ciPLAYER_ABILITY_DEFAULT_SNIPER_DEPLOY_DURATION
	sSupportSniperData.iExtractionDuration = ciPLAYER_ABILITY_DEFAULT_SNIPER_EXTRACTION_DURATION
	sSupportSniperData.iAimDuration = ciPLAYER_ABILITY_DEFAULT_SNIPER_AIM_DURATION
	sSupportSniperData.iPlayerSpottedPenalty = ciPLAYER_ABILITY_DEFAULT_PLAYER_SPOTTED_PENALTY
ENDPROC

PROC CLEAR_HEAVY_LOADOUT_ABILITY_STRUCT(FMMC_HEAVY_LOADOUT_ABILITY_STRUCT& sHeavyLoadoutData)
	sHeavyLoadoutData.iContainerIndex = ciPLAYER_ABILITY_DEFAULT_HEAVY_LOADOUT_CONTAINER_INDEX
	sHeavyLoadoutData.iPickupIndex1 = ciPLAYER_ABILITY_DEFAULT_HEAVY_LOADOUT_PICKUP_INDEX
	sHeavyLoadoutData.iPickupIndex2 = ciPLAYER_ABILITY_DEFAULT_HEAVY_LOADOUT_PICKUP_INDEX
	sHeavyLoadoutData.iPickupIndex3 = ciPLAYER_ABILITY_DEFAULT_HEAVY_LOADOUT_PICKUP_INDEX
ENDPROC

PROC CLEAR_VEHICLE_REPAIR_ABILITY_STRUCT(FMMC_VEHICLE_REPAIR_ABILITY_STRUCT& sVehicleRepair)
	sVehicleRepair.iDynopropIndex = -1
	sVehicleRepair.iMechanicPedIndex = -1
	sVehicleRepair.iRepairZoneIndex = -1
ENDPROC

PROC CLEAR_AMBUSH_ABILITY_STRUCT(FMMC_AMBUSH_ABILITY_STRUCT& sAmbush)
	sAmbush.iAmbush_SpawnInFrontBS = 0
	sAmbush.iAmbushTeamToUse = 0
	
	INITIALISE_INT_ARRAY(sAmbush.iAmbushEntityType, ciENTITY_TYPE_NONE)
	INITIALISE_INT_ARRAY(sAmbush.iAmbushEntityIndex, 0)
ENDPROC

// clear player abilities struct
PROC CLEAR_PLAYER_ABILITIES_STRUCT(FMMC_PLAYER_ABILITIES_STRUCT& sPlayerAbilitiesData)

	sPlayerAbilitiesData.iAbilityExistsBS = 0
	
	INT iAbility = 0
	FOR iAbility = 0 TO ENUM_TO_INT(PA_MAX)-1
		CLEAR_PLAYER_ABILITY_STRUCT(sPlayerAbilitiesData.sAbilities[iAbility])
	ENDFOR
	
	CLEAR_SUPPORT_SNIPER_ABILITY_STRUCT(sPlayerAbilitiesData.sSupportSniper)
	CLEAR_HEAVY_LOADOUT_ABILITY_STRUCT(sPlayerAbilitiesData.sHeavyLoadout)
	CLEAR_VEHICLE_REPAIR_ABILITY_STRUCT(sPlayerAbilitiesData.sVehicleRepair)
	CLEAR_AMBUSH_ABILITY_STRUCT(sPlayerAbilitiesData.sAmbush)
ENDPROC

PROC CLEAR_GANG_CHASE_UNIT_CONFIG_STRUCT(FMMC_GANG_CHASE_UNIT_CONFIG_STRUCT& sGangChaseUnitConfigData)
	sGangChaseUnitConfigData.mnPedModel = eFMMC_GANG_CHASE_DEFAULT_PED
	sGangChaseUnitConfigData.mnVehicleModel = eFMMC_GANG_CHASE_DEFAULT_VEHICLE
	sGangChaseUnitConfigData.wtDefaultWeapon = eFMMC_GANG_CHASE_DEFAULT_WEAPON
	sGangChaseUnitConfigData.wtRandomWeapon1 = WEAPONTYPE_INVALID
	sGangChaseUnitConfigData.wtRandomWeapon2 = WEAPONTYPE_INVALID
	sGangChaseUnitConfigData.iMaxPeds = ciFMMC_GANG_CHASE_DEFAULT_MAX_PEDS
	sGangChaseUnitConfigData.fPedArmour = 0.0
	sGangChaseUnitConfigData.bUseAlternateComponentsForPedOutfit = FALSE
	sGangChaseUnitConfigData.iOverrideVehicleColour1 = -1
	sGangChaseUnitConfigData.iOverrideVehicleColour2 = -1
	sGangChaseUnitConfigData.iVehicleModPresetToUse = -1
	sGangChaseUnitConfigData.iVehicleLiveryToUse = -1
	sGangChaseUnitConfigData.bWeaponsDisrupted = FALSE
ENDPROC

PROC CLEAR_FMMC_CUSTOM_HUD_STRUCT(FMMC_CUSTOM_HUD_STRUCT &sCustomHUD)
	sCustomHUD.iCriteria = FMMC_CUSTOM_HUD_CRITERIA_NONE
	INITIALISE_INT_ARRAY(sCustomHUD.iPreReqs, 0)
	sCustomHUD.iCustomStringTextLabel = -1
	sCustomHUD.iColour = FMMC_CUSTOM_HUD_COLOUR_DEFAULT
	sCustomHUD.iType = FMMC_CUSTOM_HUD_TYPE_NUMBER
ENDPROC

PROC CLEAR_IPL_STRUCT()
	g_FMMC_STRUCT.sIPLOptions.iIslandIPL = 0
	
	INT iIPLType, iConditionalIPL
	FOR iIPLType = 0 TO ciFMMC_IPLTYPE_Max-1
		FOR iConditionalIPL = 0 TO ciFMMC_IPLMaxConditional-1
			g_FMMC_STRUCT.sIPLOptions.iIPLLinkedContinuityEntityID[iIPLType][iConditionalIPL]		= -1
			g_FMMC_STRUCT.sIPLOptions.iIPLLinkedContinuityEntityType[iIPLType][iConditionalIPL]		= -1
			g_FMMC_STRUCT.sIPLOptions.iIPLLinkedIPLIndex[iIPLType][iConditionalIPL]					= -1
		ENDFOR
	ENDFOR
ENDPROC

PROC CLEAR_NIGHTCLUB_STRUCT()
	g_FMMC_STRUCT.sNightclubOptions.iClubStyle					= 0
	g_FMMC_STRUCT.sNightclubOptions.iClubLightRig				= 0
	g_FMMC_STRUCT.sNightclubOptions.iClubName					= 0
	g_FMMC_STRUCT.sNightclubOptions.iClubDancerStyle			= 0
	g_FMMC_STRUCT.sNightclubOptions.iClubDancerGender			= 0
	g_FMMC_STRUCT.sNightclubOptions.iClubPopularity				= 0
	g_FMMC_STRUCT.sNightclubOptions.iClubDJ						= 0
		
	g_FMMC_STRUCT.sNightclubOptions.iBusinessDesks				= 0
	g_FMMC_STRUCT.sNightclubOptions.iBusinessFloorLevelSign		= 0
		
	g_FMMC_STRUCT.sNightclubOptions.iBusinessCashBS				= 0
	g_FMMC_STRUCT.sNightclubOptions.iBusinessDocsBS				= 0
	g_FMMC_STRUCT.sNightclubOptions.iBusinessMethBS				= 0
	g_FMMC_STRUCT.sNightclubOptions.iBusinessWeedBS				= 0
	g_FMMC_STRUCT.sNightclubOptions.iBusinessCokeBS				= 0
	
	g_FMMC_STRUCT.sNightclubOptions.iBS							= 0
ENDPROC

PROC CLEAR_GO_TO_LOCATION_DATA(FMMC_GO_TO_LOCATION &sGotoLocationDataStruct)
	INT ii, i2
	sGotoLocationDataStruct.iLocBS					=	0
	sGotoLocationDataStruct.fSecondaryRadius		= 0
	sGotoLocationDataStruct.fDir 					= 0
	sGotoLocationDataStruct.fDirTolerance			= 0
	sGotoLocationDataStruct.fHeight					= 0
	sGotoLocationDataStruct.fLocateTilt				= 0
	sGotoLocationDataStruct.fGroundClear			= 0
	sGotoLocationDataStruct.iAreaType				= 0
	sGotoLocationDataStruct.vLoc1					= <<0.0, 0.0, 0.0>>
	sGotoLocationDataStruct.vLoc2					= <<0.0, 0.0, 0.0>>
	sGotoLocationDataStruct.fWidth					= 1.0
	sGotoLocationDataStruct.fStoppingDist			= DEFAULT_VEH_STOPPING_DISTANCE
	sGotoLocationDataStruct.fLocateHeight			= 1.0
	sGotoLocationDataStruct.fLocateDepth			= 0.0
	sGotoLocationDataStruct.iLocateAlpha			= -1
	sGotoLocationDataStruct.fLocateCoronaScaler		= 0.0
	sGotoLocationDataStruct.iLocateBlipSprite		= 0
	sGotoLocationDataStruct.iStopTimer				= 0
	sGotoLocationDataStruct.iLocBS2					= 0
	sGotoLocationDataStruct.iLocBS3					= 0
	sGotoLocationDataStruct.iLocProcessingDelay		= 0
	sGotoLocationDataStruct.iFakeOwnedByTeam 		= -1
	sGotoLocationDataStruct.iContinuityId 			= -1
	sGotoLocationDataStruct.iContentContinuityType 	= 0
	CLEAR_ENTITY_BLIP_DATA_STRUCT(sGotoLocationDataStruct.sLocBlipStruct)
	
	sGotoLocationDataStruct.iLineOpacity			   			 = -1
	sGotoLocationDataStruct.fLineThickness						 = -1.0
	sGotoLocationDataStruct.fTeamMembersNeededWithin			 = 0.0
	sGotoLocationDataStruct.fTeamMembersNeededToleranceIncrease	 = 0.0
	sGotoLocationDataStruct.iTeamSizeThreshold 					 = -1
	sGotoLocationDataStruct.iLocatePropLinkIndex 				 = -1
	sGotoLocationDataStruct.iLocatePropLinkIndexSecondary 		 = -1
	sGotoLocationDataStruct.iLocatePropLinkIndexTertiary 		 = -1
	sGotoLocationDataStruct.iLocDisableVehSpecWeapRadius 		 = 0
	sGotoLocationDataStruct.iIncrementTime						 = 5
	sGotoLocationDataStruct.iLocMCVarSelectionIndex_One 		 = -1
	sGotoLocationDataStruct.iLocMCVarSelectionIndex_Two 		 = -1
	sGotoLocationDataStruct.iLocMCVarPrerequisite				 = -1
	sGotoLocationDataStruct.eOutfitRequiredForLocation			 = OUTFIT_FOR_LOCATION_SPAWN_CHECK_NONE
	sGotoLocationDataStruct.iSpawnGroup 						 = 0
	sGotoLocationDataStruct.iSpawnSubGroup  					 = 0
	FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		sGotoLocationDataStruct.iSpawnSubGroupBS[ii] 			= 0
	ENDFOR
	sGotoLocationDataStruct.iSpawnGroupAdditionalFunctionalityBS = 0
	
	sGotoLocationDataStruct.iCoronaSize							= 0
		
	CLEAR_WARP_LOCATION_SETTINGS(sGotoLocationDataStruct.sWarpLocationSettings)
		
	CLEAR_ENTITY_END_MISSION_DATA_STRUCT(sGotoLocationDataStruct.sEndMissionData)
	
	IF sGotoLocationDataStruct.ciCheckpoint != NULL
		DELETE_CHECKPOINT(sGotoLocationDataStruct.ciCheckpoint)
		sGotoLocationDataStruct.ciCheckpoint = NULL
	ENDIF
	
	FOR i2 = 0 TO (FMMC_MAX_TEAMS -1)
		sGotoLocationDataStruct.vLoc[i2]					= <<0.0, 0.0, 0.0>>
		sGotoLocationDataStruct.fRadius[i2]					= 0.0
		sGotoLocationDataStruct.iArial[i2]					= 0
		sGotoLocationDataStruct.iSmallCorona[i2]			= 0
		sGotoLocationDataStruct.iGPSCutOff[i2]				= 0
		
		//sGotoLocationDataStruct.fHeading[i2]					= 0.0
		
		sGotoLocationDataStruct.iRule[i2]					= FMMC_OBJECTIVE_LOGIC_NONE
		sGotoLocationDataStruct.iPriority[i2]				= FMMC_PRIORITY_IGNORE
		sGotoLocationDataStruct.iWholeTeamAtLocation[i2]	= 0
		sGotoLocationDataStruct.iCaptureMultiplier[i2]		= 0
		sGotoLocationDataStruct.mnVehicleNeeded[i2]			= DUMMY_MODEL_FOR_SCRIPT
		sGotoLocationDataStruct.iJumpToObjectivePass[i2]	= 0
		sGotoLocationDataStruct.iJumpToObjectiveFail[i2] 	= 0
	ENDFOR
	
	sGotoLocationDataStruct.iLocSpecificVehicleIndex = -1
	
	sGotoLocationDataStruct.iObjectivePrereq = ciPREREQ_None
	sGotoLocationDataStruct.iLocPreReqRequired = ciPREREQ_None
	sGotoLocationDataStruct.iLocPreReqHelpText = -1
	sGotoLocationDataStruct.sLocTriggerInput.iInputRequired = -1
	sGotoLocationDataStruct.sLocTriggerInput.iInputBitSet = 0
	sGotoLocationDataStruct.sLocTriggerInput.iHelpText = -1
	sGotoLocationDataStruct.sLocTriggerInput.iPrimaryObjText = -1
	sGotoLocationDataStruct.sLocTriggerInput.iWaitObjText = -1
	
	sGotoLocationDataStruct.iLocPMCOverride = SIS_POST_NONE
	
	sGotoLocationDataStruct.iAggroIndexBS_Entity_Loc = ciMISSION_AGGRO_INDEX_DEFAULT
	
	INITIALISE_INT_ARRAY(sGotoLocationDataStruct.iAltVarsBitsetLocation, 0)
ENDPROC

PROC CLEAR_DRONE_DATA(FMMC_DRONE_DATA &sStruct)
	
	sStruct.iBS 							= 0
	sStruct.iAssociatedPoolStartingIndex 	= 0
	sStruct.fMinimumHoverHeight 			= 1
	sStruct.fDroneSpeedMultiplier 			= 4.0
	sStruct.iStartingAction					= ciTASK_DRONE_CHOOSE_NEW_TASK
	sStruct.iAssociatedRuleAction			= ciTASK_DRONE_CHOOSE_NEW_TASK
	sStruct.iHealthThresholdAction			= ciTASK_DRONE_CHOOSE_NEW_TASK	
	sStruct.iHealthThresholdPercentage		= 0		
	sStruct.fMinimumCombatDistanceToTarget	= 5.0
	sStruct.fDefensiveRadius				= 0.0
	sStruct.iAmbientlySpinSpeed 			= 0
	
	INT i = 0 
	FOR i = 0 TO ciMAX_FMMC_DRONE_WEAPONS - 1
		sStruct.sWeaponData[i].iWeaponType			= ciFMMC_DroneWeapon_None
		sStruct.sWeaponData[i].iShotAccuracy		= 70
		sStruct.sWeaponData[i].iShotReloadTime		= 3000	
		sStruct.sWeaponData[i].iShotIntervalTime	= 250
		sStruct.sWeaponData[i].iAmmoMax				= 1
	ENDFOR
	
ENDPROC

PROC CLEAR_ELECTRONIC_DATA(FMMC_ELECTRONIC_DATA &sStruct)
	sStruct.iElectronicBS = 0
	sStruct.iTaserDisableTime = 0
	sStruct.sCCTVData.iCCTVBS = 0
	sStruct.sCCTVData.fCCTVTurnAmount = 0.0
	sStruct.sCCTVData.fCCTVTurnSpeed = 1.0
	sStruct.sCCTVData.fCCTVWaitTime = 3.0
	sStruct.sCCTVData.fCCTVVisionRange = 4.0
	sStruct.sCCTVData.fCCTVVisionWidth = 3.0
	sStruct.sCCTVData.iCCTVSkipToRule = -1
	sStruct.sCCTVData.iCCTVShowHelpTextFromThisRule = -1
	sStruct.sCCTVData.iCCTVBlockHelpTextFromThisRule = -1
ENDPROC

PROC CLEAR_YACHT_DATA(FMMC_YACHT_STRUCT &sStruct)
	sStruct.tlYachtName = ""
	sStruct.iMYacht_ModelIndex = ciYACHT_MODEL__OFF
	sStruct.iMYacht_Bitset = 0
	sStruct.iMYacht_AreaIndex = 0
	sStruct.iMYacht_PositionIndex = 0
	sStruct.iMYacht_ColourIndex = 0
	sStruct.iMYacht_LightingIndex = 0
	sStruct.iMYacht_FittingsIndex = ciYACHT_FITTINGS__CHROME
	sStruct.iMYacht_FlagIndex = 0
ENDPROC

PROC CLEAR_VEHICLE_CUSTOM_MOD_SETTING(INT iCustomMod)
	PRINTLN("CLEAR_VEHICLE_CUSTOM_MOD_SETTING - Clearing [", iCustomMod,"]")
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Bitset = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Spoiler = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Bumper_F = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Bumper_R = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Skirt = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Exhaust = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Chassis = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Grill = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Bonnet = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Wing_L = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Wing_R = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Roof = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Engine = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Brakes = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Gearbox = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Horn = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Suspension = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Armour = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Wheels = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Rear_Wheels = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Interior1 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Interior2 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Interior3 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Interior4 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Interior5 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Seats = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Steering = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Plaque = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Trunk = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_EngineBay1 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_EngineBay2 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_EngineBay3 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Chassis2 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Chassis3 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Chassis4 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Chassis5 = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Door_L = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Door_R = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Livery = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_PLTHolder = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_PlateText = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Toggle_Turbo = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Toggle_Tyre_Smoke = 0
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_Toggle_Xenon_Lights = -1
	g_FMMC_STRUCT_ENTITIES.sPlacedVehicleCustomModSettings[iCustomMod].iCustomMod_WheelRimColour = -1
ENDPROC

PROC CLEAR_UNIT_STRUCT()
	g_FMMC_STRUCT_ENTITIES.iNumberOfUnitsPlaced = 0
	g_FMMC_STRUCT_ENTITIES.iNumberOfUnitObjects = 0
	g_FMMC_STRUCT_ENTITIES.iNumberOfUnitVehicles = 0
	g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds = 0
	INT iUnit, iEntity
	FOR iUnit = 0 TO FMMC_MAX_NUM_UNITS-1
		g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].fHeading = 0.0
		g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].iUnitType = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].iVariation = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].mnPedModel = S_M_Y_Cop_01
		g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].vPos = <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].vRotation = <<0.0, 0.0, 0.0>>
		
		FOR iEntity = 0 TO FMMC_MAX_NUM_UNIT_VEHICLES-1
			g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].sUnitVeh[iEntity].mnVehicleModel = POLICE3
			g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].sUnitVeh[iEntity].iVehicleModPreset = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedUnits[iUnit].sUnitVeh[iEntity].iVehicleBitSet = 0
		ENDFOR
	ENDFOR
ENDPROC

PROC CLEAR_CHECKPOINT_DATA(INT iCheckpoint)

	IF iCheckPoint <= -1
		PRINTLN("CLEAR_CHECKPOINT_DATA - You passed in <= -1! Exiting now")
		EXIT
	ENDIF
		
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].vCheckPoint = <<0.0,0.0,0.0>>
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].vCheckPointSecondary = <<0.0,0.0,0.0>>
	
	INT iRespawn
	FOR iRespawn = 0 TO FMMC_MAX_NUM_CHECKPOINT_RESPAWN-1
		g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].sCheckpointRespawn.vRespawnPos[iRespawn] = <<0.0,0.0,0.0>>
		g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].sCheckpointRespawnSecondary.vRespawnPos[iRespawn]	= <<0.0,0.0,0.0>>
	ENDFOR
		
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckPointRespawn = 0.0
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckPointRespawnSecondary = 0.0 
	
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckPointScale = 1.0
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckPointScaleSecondary = 1.0
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckPointVisualScale = 1.0
	
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckPointPitch = 0.0
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckPointPitchSecondary = 0.0
	
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].iCheckpointWrongWayTime = -1
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].iCheckpointPassState = 0
	
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckPointHeight = 0.0
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckpointHeightSecondary = 0.0
	
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].vCheckpointArrowDirectionOverride = <<0.0, 0.0, 0.0>>
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].vCheckpointArrowDirectionOverrideSecondary	= <<0.0, 0.0, 0.0>>
	
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].iCheckpointHelpTextToUse = -1
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].iCheckpointHelpTextLap = 0
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].iCheckpointHelpTextTriggerRadius = 0
	
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].iCheckpointBitset = 0
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].iCheckpointBitsetTwo = 0
	
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].iCheckpointTransformIndex = -1
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].iCheckpointTransformIndexSecondary	= -1
	
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckpointSuperTallShrinkRange = 500.0
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckpoint].fCheckpointSuperTallShrinkRangeSecondary = 500.0
	
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckPoint].iCheckPoint_DeluxoOption 				= -1
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckPoint].iCheckPoint_DeluxoOption_Secondary 		= -1
	
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckPoint].iCheckPoint_StrombergOption 				= -1
	g_FMMC_STRUCT.sPlacedCheckpoint[iCheckPoint].iCheckPoint_StrombergOption_Secondary 	= -1

	
	IF IS_RACE_P2P()
	AND iCheckPoint = g_FMMC_STRUCT.iNumberOfCheckpoints-1
	AND iCheckPoint > 0
		g_FMMC_STRUCT.sPlacedCheckpoint[iCheckPoint-1].vCheckPointSecondary			= <<0,0,0>>
		g_FMMC_STRUCT.sPlacedCheckpoint[iCheckPoint-1].fCheckPointRespawnSecondary 	= 	0.0
		CLEAR_BIT(g_FMMC_STRUCT.sPlacedCheckpoint[iCheckPoint-1].iCheckpointBitset, ciCHECKPOINT_ROUND_SECONDARY)
	ENDIF
	
ENDPROC

PROC CLEAR_PLACED_PTFX_DATA(INT iPTFX)
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63AssetName = ""
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName = ""
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl15EvolutionName = ""
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartCoord = <<0.0, 0.0, 0.0>>
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartRot = <<0.0, 0.0, 0.0>>
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS = 0
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTeam = -1
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iRuleFrom = -1
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iRuleTo = -1
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTimeToPlayFor = 0
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].fScale = 1.0
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iMCVarConfig_PreReqMissionPointer = -1
	g_FMMC_STRUCT.sPlacedPTFX[iPTFX].fEvolution = 0.0
	INITIALISE_INT_ARRAY(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iAltVarsBitsetPTFX, 0)
ENDPROC

PROC CLEAR_WARP_PORTAL_DATA(INT iPortal)
	INT i
	INT iExtra
	g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal = -1
	g_FMMC_STRUCT.sWarpPortals[iPortal].vStartCoord = <<0.0, 0.0, 0.0>>			
	g_FMMC_STRUCT.sWarpPortals[iPortal].fStartHead = 0.0
	FOR iExtra = 0 TO  FMMC_MAX_WARP_PORTALS_EXTRA_POSITIONS-1
		g_FMMC_STRUCT.sWarpPortals[iPortal].vExtraStartCoord[iExtra] = <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.sWarpPortals[iPortal].fExtraStartHead[iExtra] = 0.0		
	ENDFOR
	g_FMMC_STRUCT.sWarpPortals[iPortal].fStartRange = 1.25	
	g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS = 0
	g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS2 = 0
	g_FMMC_STRUCT.sWarpPortals[iPortal].iTriggerTypeUsed = ciWARP_PORTAL_TRIGGERS_NONE	
	g_FMMC_STRUCT.sWarpPortals[iPortal].iAutoTriggerAfterTime = 0	
	g_FMMC_STRUCT.sWarpPortals[iPortal].fMoveForwardXMeters = 0	
	g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType = -1
	g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord = <<0.0, 0.0, 0.0>>
	g_FMMC_STRUCT.sWarpPortals[iPortal].fEntryAnimHead = 0.0
	g_FMMC_STRUCT.sWarpPortals[iPortal].fCamAnimFOV = 65.0
	g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimCoord = <<0.0, 0.0, 0.0>>
	g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimRot = <<0.0, 0.0, 0.0>>
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		g_FMMC_STRUCT.sWarpPortals[iPortal].iActiveOnRule[iTeam] = -1
		g_FMMC_STRUCT.sWarpPortals[iPortal].iEndOnRule[iTeam] = -1
	ENDFOR
	g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityID = -1
	g_FMMC_STRUCT.sWarpPortals[iPortal].iAttachToEntityType = 0	
	INITIALISE_INT_ARRAY(g_FMMC_STRUCT.sWarpPortals[iPortal].iAltVarsBitsetWarpPortal, 0)
	FOR i = 0 TO FMMC_MAX_WARP_PORTAL_CUSTOM_PROMPT_LABEL - 1
		g_FMMC_STRUCT.sWarpPortals[iPortal].iCustomPromptLabel[i] = -1
	ENDFOR	
		
	g_FMMC_STRUCT.sWarpPortals[iPortal].iColourType = FMMC_BLIP_COLOUR_BLUE
	g_FMMC_STRUCT.sWarpPortals[iPortal].fVisualRangeOverride = -1.0
	
	g_FMMC_STRUCT.sWarpPortals[iPortal].sVehicleSwap.mnVehicleModelSwap = DUMMY_MODEL_FOR_SCRIPT
	g_FMMC_STRUCT.sWarpPortals[iPortal].sVehicleSwap.iModPresetIndex = -1
	g_FMMC_STRUCT.sWarpPortals[iPortal].sVehicleSwap.iVehicleAirCounterCooldown = -1
ENDPROC

PROC CLEAR_ALL_WARP_PORTAL_DATA()
	INT i = 0
	FOR i = 0 TO FMMC_MAX_WARP_PORTALS-1
		CLEAR_WARP_PORTAL_DATA(i)
	ENDFOR
ENDPROC

// Systems

PROC CLEAR_CUSTOM_VARIABLES__DATA(MC_CUSTOM_VARIABLE_DATA &sData)
	sData.iReturnValue = 0
	sData.iComparisonValue = 0
	sData.iComparisonType = ciCUSTOM_VARIABLE_COMPARISON_TYPE__OFF
	sData.iDifficultyLevel = ciCUSTOM_VARIABLE_DIFFICULTY_LEVEL__ANY
ENDPROC 

// Player Numbers
PROC CLEAR_CUSTOM_VARIABLES__PLAYER_NUMBERS(INT i)

	INT ii = 0
	FOR ii = 0 TO CUSTOM_VARIABLE_LIST__SAVED_PLAYER_NUMBER_VALUES_MAX-1
		CLEAR_CUSTOM_VARIABLES__DATA(g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[i].sData[ii])
	ENDFOR
	
ENDPROC
PROC CLEAR_ALL_CUSTOM_VARIABLES__PLAYER_NUMBERS()

	INT i = 0
	FOR i = 0 TO ciMAX_CUSTOM_VARIABLE_LIST__PLAYER_NUMBERS_PRESETS-1
		CLEAR_CUSTOM_VARIABLES__PLAYER_NUMBERS(i)
	ENDFOR
	
ENDPROC

// Entity Respawn Count
PROC CLEAR_CUSTOM_VARIABLES__ENTITY_RESPAWN_COUNT(INT i)

	INT ii = 0
	FOR ii = 0 TO CUSTOM_VARIABLE_LIST__SAVED_ENTITY_RESPAWNED_VALUES_MAX-1		
		CLEAR_CUSTOM_VARIABLES__DATA(g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[i].sData[ii])
	ENDFOR
	
ENDPROC
PROC CLEAR_ALL_CUSTOM_VARIABLES__ENTITY_RESPAWN_COUNT()

	INT i = 0
	FOR i = 0 TO ciMAX_CUSTOM_VARIABLE_LIST__ENTITY_RESPAWNED_COUNT_PRESETS-1
		CLEAR_CUSTOM_VARIABLES__ENTITY_RESPAWN_COUNT(i)
	ENDFOR
	
ENDPROC

// Rule Timer Elapsed
PROC CLEAR_CUSTOM_VARIABLES__RULE_TIMER_ELAPSED(INT i)

	INT ii = 0
	FOR ii = 0 TO CUSTOM_VARIABLE_LIST__SAVED_RULE_TIME_ELAPSED_MAX-1
		CLEAR_CUSTOM_VARIABLES__DATA(g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[i].sData[ii])
	ENDFOR
	
ENDPROC

PROC CLEAR_ALL_CUSTOM_VARIABLES__RULE_TIMER_ELAPSED()

	INT i = 0
	FOR i = 0 TO ciMAX_CUSTOM_VARIABLE_LIST__RULE_TIME_ELAPSED_PRESETS-1
		CLEAR_CUSTOM_VARIABLES__RULE_TIMER_ELAPSED(i)
	ENDFOR
	
ENDPROC

// Difficulty Level
PROC CLEAR_CUSTOM_VARIABLES__DIFFICULTY_LEVEL(INT i)

	INT ii = 0
	FOR ii = 0 TO CUSTOM_VARIABLE_LIST__SAVED_DIFFICULTY_LEVEL_MAX-1
		CLEAR_CUSTOM_VARIABLES__DATA(g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[i].sData[ii])
	ENDFOR
	
ENDPROC
PROC CLEAR_ALL_CUSTOM_VARIABLES__DIFFICULTY_LEVEL()

	INT i = 0
	FOR i = 0 TO ciMAX_CUSTOM_VARIABLE_LIST__DIFFICULTY_LEVEL_PRESETS-1
		CLEAR_CUSTOM_VARIABLES__DIFFICULTY_LEVEL(i)
	ENDFOR
	
ENDPROC

// Pools
PROC CLEAR_CUSTOM_VARIABLES__POOL__WEAPON(INT i)
	g_FMMC_STRUCT.sCustomVariables.sWeaponPool[i].wt_Weapon = WEAPONTYPE_INVALID
ENDPROC

PROC CLEAR_ALL_CUSTOM_VARIABLES__POOL__WEAPONS()

	INT i = 0
	FOR i = 0 TO ciMAX_CUSTOM_VARIABLE_POOL__WEAPON_MAX-1
		CLEAR_CUSTOM_VARIABLES__POOL__WEAPON(i)
	ENDFOR

ENDPROC

PROC CLEAR_ALL_CUSTOM_VARIABLES()

	// Systems
	CLEAR_ALL_CUSTOM_VARIABLES__PLAYER_NUMBERS()
	CLEAR_ALL_CUSTOM_VARIABLES__ENTITY_RESPAWN_COUNT()
	CLEAR_ALL_CUSTOM_VARIABLES__RULE_TIMER_ELAPSED()
	CLEAR_ALL_CUSTOM_VARIABLES__DIFFICULTY_LEVEL()
	
	// Pools
	CLEAR_ALL_CUSTOM_VARIABLES__POOL__WEAPONS()
	
ENDPROC

FUNC BOOL MOVE_CUSTOM_VARIABLES_DATA_POSITION_WITH_STRUCT(MC_CUSTOM_VARIABLE_DATA &sData[], INT iIndexToMove, INT iDirection)
	
	IF iDirection > 0
		IF iIndexToMove+iDirection >= COUNT_OF(sData)
			RETURN FALSE
		ENDIF
	ELSE
		IF iIndexToMove+iDirection < 0
			RETURN FALSE
		ENDIF
	ENDIF
	
	MC_CUSTOM_VARIABLE_DATA sDataTemp = sData[iIndexToMove]
	
	sData[iIndexToMove] = sData[iIndexToMove+iDirection]
	sData[iIndexToMove+iDirection] = sDataTemp
	
	RETURN TRUE
ENDFUNC

FUNC BOOL MOVE_CUSTOM_VARIABLES_DATA_ADJUST_POSITION(INT iPresetType, INT iPresetIndex, INT iIndexToMove, INT iDirection)
		
	SWITCH iPresetType
		CASE CUSTOM_VARIABLE_LIST_TYPE__PLAYER_NUMBERS
			RETURN MOVE_CUSTOM_VARIABLES_DATA_POSITION_WITH_STRUCT(g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[iPresetIndex].sData, iIndexToMove, iDirection)
		BREAK		
		CASE CUSTOM_VARIABLE_LIST_TYPE__ENTITY_RESPAWNED_COUNT
			RETURN MOVE_CUSTOM_VARIABLES_DATA_POSITION_WITH_STRUCT(g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[iPresetIndex].sData, iIndexToMove, iDirection)
		BREAK		
		CASE CUSTOM_VARIABLE_LIST_TYPE__RULE_TIME_ELAPSED
			RETURN MOVE_CUSTOM_VARIABLES_DATA_POSITION_WITH_STRUCT(g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[iPresetIndex].sData, iIndexToMove, iDirection)
		BREAK		
		CASE CUSTOM_VARIABLE_LIST_TYPE__DIFFICULTY_LEVEL
			RETURN MOVE_CUSTOM_VARIABLES_DATA_POSITION_WITH_STRUCT(g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[iPresetIndex].sData, iIndexToMove, iDirection)
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MOVE_CUSTOM_VARIABLES_DATA_INCREMENT_POSITION(INT iPresetType, INT iPresetIndex, INT iIndexToMove)
	RETURN MOVE_CUSTOM_VARIABLES_DATA_ADJUST_POSITION(iPresetType, iPresetIndex, iIndexToMove, 1)
ENDFUNC

FUNC BOOL MOVE_CUSTOM_VARIABLES_DATA_DECREMENT_POSITION(INT iPresetType, INT iPresetIndex, INT iIndexToMove)		
	RETURN MOVE_CUSTOM_VARIABLES_DATA_ADJUST_POSITION(iPresetType, iPresetIndex, iIndexToMove, -1)
ENDFUNC

PROC CLEAR_CUSTOM_VARIABLES_DATA_AT_INDEX(INT iPresetType, INT iPresetIndex, INT iIndexToClear)		
	SWITCH iPresetType
		CASE CUSTOM_VARIABLE_LIST_TYPE__PLAYER_NUMBERS
			CLEAR_CUSTOM_VARIABLES__DATA(g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[iPresetIndex].sData[iIndexToClear])
		BREAK		
		CASE CUSTOM_VARIABLE_LIST_TYPE__ENTITY_RESPAWNED_COUNT
			CLEAR_CUSTOM_VARIABLES__DATA(g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[iPresetIndex].sData[iIndexToClear])
		BREAK		
		CASE CUSTOM_VARIABLE_LIST_TYPE__RULE_TIME_ELAPSED
			CLEAR_CUSTOM_VARIABLES__DATA(g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[iPresetIndex].sData[iIndexToClear])
		BREAK		
		CASE CUSTOM_VARIABLE_LIST_TYPE__DIFFICULTY_LEVEL
			CLEAR_CUSTOM_VARIABLES__DATA(g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[iPresetIndex].sData[iIndexToClear])
		BREAK
	ENDSWITCH
ENDPROC

//Reset the race Globals
PROC CLEAR_FMMC_STRUCT(BOOL bReSetMenuItems = TRUE, BOOL bFlattenMissionTypeBitset = TRUE, BOOL bClearNextContentID = TRUE, BOOL bSetRaceTypeToStandard = TRUE)
	
	PRINTLN("[TS] * CLEAR_FMMC_STRUCT - bClearNextContentID = ", bClearNextContentID)
	#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * CLEAR_FMMC_STRUCT - bClearNextContentID = ", bClearNextContentID)	#ENDIF
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	INT i, i2, i3, ii
	
	g_FMMC_STRUCT.bBookMarked = FALSE
	IF bReSetMenuItems
	
		IF NOT IS_PLAYER_IN_CORONA()
		OR GET_CORONA_STATUS() = CORONA_STATUS_WALK_OUT
		OR GET_CORONA_STATUS() = CORONA_STATUS_QUIT
		OR GET_CORONA_STATUS() = CORONA_STATUS_RESET_POSITION
			g_FMMC_STRUCT.iRootContentIDHash = 0
			g_FMMC_STRUCT.iPhotoPath = 2
		ENDIF
		
		g_FMMC_STRUCT_ENTITIES.iPedSpawnCapOverRide				= -1
		#IF IS_DEBUG_BUILD
		g_FMMC_STRUCT.iVersionPosix						= 0
		#ENDIF
		
		g_FMMC_STRUCT.iOptionsMenuBitSet = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwo = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetThree = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetFour = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetFive = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetSix = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetSeven = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetEight = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetNine = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTen = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetEleven = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwelve = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetThirteen = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetFourteen = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetFifteen = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetSixteen = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetEighteen = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetNineteen = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwenty = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine = 0
		g_FMMC_STRUCT.iOptionsMenuBitSetThirty = 0
		
		g_FMMC_STRUCT.iAggroIndexBS_iStartMissionWithAggro = ciMISSION_AGGRO_INDEX_DEFAULT
		
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_PREREQ_TRIGGERS_MAX-1
			INITIALISE_INT_ARRAY(g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_PrereqIndexBS[i], 0)			
		ENDFOR				
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_PrereqAggroBS, ciMISSION_AGGRO_INDEX_DEFAULT)
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_ModBS, 0)
		
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT.iAltVarsBitsetGeneric, 0)
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT.iAltVarsBitsetGangChase, 0)
		g_FMMC_STRUCT.sPoolVars.eConfig = FMMC_ALT_VAR_CONFIG_NONE
		FOR i = 0 TO ciMAX_FMMC_ALT_VAR_INTS - 1
			g_FMMC_STRUCT.sPoolVars.iInt[i] = 0
		ENDFOR
		
		INT iCustomString = 0
		FOR iCustomString = 0 TO ciMAX_CUSTOM_STRING_LIST_LENGTH - 1
			g_FMMC_STRUCT.tlCustomStringList[iCustomString] = ""
		ENDFOR
		
		CLEAR_ALL_CUSTOM_VARIABLES()
		
		g_FMMC_STRUCT.eMissionIntroType = MISSION_INTRO_TYPE_NONE
		g_FMMC_STRUCT.fFireSpreadMultiplier = -0.1
		g_FMMC_STRUCT.iCasinoPedLayout = -1
		g_FMMC_STRUCT.iCoronaModOptionsBS = 0
		g_FMMC_STRUCT.iPlayerLives = 0
		g_FMMC_STRUCT.iMinimumCoronaTeamLives = 1
		g_FMMC_STRUCT.iMaximumCoronaTeamLives = 0
		g_FMMC_STRUCT.iHighPriorityTeamForBalancing = 0
		g_FMMC_STRUCT.iRaceCustomVehSetting = 0
		g_FMMC_STRUCT.iRuleOptionsBitSet = 0
		g_FMMC_STRUCT.iPointAwardsBS = 0
		g_FMMC_STRUCT.iNumParticipants = 0
		g_FMMC_STRUCT.iMissionEndType = 0
		g_FMMC_STRUCT.iRunAs = ciMISSION_RUN_AS__CONTACT
		g_FMMC_STRUCT.iRespawnTime = 0
		g_FMMC_STRUCT.iRoundTime = 0
		g_FMMC_STRUCT.iTargetScore = 0
		g_FMMC_STRUCT.iBlips = 0
		g_FMMC_STRUCT.iTags = 0
		g_FMMC_STRUCT.iVoice = 0
		g_FMMC_STRUCT.iRadio = 0
		g_FMMC_STRUCT.iAutoAim = 0
		g_FMMC_STRUCT.iNumberOfTeams = 0
		g_FMMC_STRUCT.iMaxNumberOfTeams = 1
		g_FMMC_STRUCT.iAreaTriggered = 0
		g_FMMC_STRUCT.iNumberOfLives = 0
		g_FMMC_STRUCT.iTRShrinkingTimer = 10
		g_FMMC_STRUCT.fTREndRadius = 5
		g_FMMC_STRUCT.iTRShrinkStartTimer = 10
		g_FMMC_STRUCT.iRank = 1
		g_FMMC_STRUCT.iMinNumParticipants = 0
		g_FMMC_STRUCT.iContactCharEnum = 0
		g_FMMC_STRUCT.iCashReward = 0
		g_FMMC_STRUCT.iXPReward = 0
		g_FMMC_STRUCT.iContactChar = 0
		g_FMMC_STRUCT.bCheckpointScaleFixPresent = FALSE
		g_FMMC_STRUCT.iOverridePauseCamDistance = -1		
		
		g_FMMC_STRUCT.iGameMasterT1RuleForT1Fail = 0
		g_FMMC_STRUCT.iGameMasterT2RuleForT1Fail = 0
		g_FMMC_STRUCT.iGameMasterT1RuleForT2Fail = 0
		g_FMMC_STRUCT.iGameMasterT2RuleForT2Fail = 0
		g_FMMC_STRUCT.iGameMasterStartingTeam = 0
		g_FMMC_STRUCT.iGameMasterRoleSwapRestarts = 1
		g_FMMC_STRUCT.fHandheldDrillingMinigameSafetyDepositBoxTime = 2.5
		g_FMMC_STRUCT.iHandheldDrillingMinigameSpawnGroup = 0
		
		g_FMMC_STRUCT.iLeaderboardStyle = ciMISSION_LEADERBOARD_STYLE_DEFAULT
		
		g_FMMC_STRUCT.iSpawnPickupsAtStartTimer = 0
		
		g_FMMC_STRUCT.iArtificialLightControlTeam = 0
		
		g_FMMC_STRUCT.wtVehicleWeaponToModifyDamage = WEAPONTYPE_INVALID
		g_FMMC_STRUCT.fVehicleWeaponDamageModifier = 1.0
		
		CLEAR_ALL_WARP_PORTAL_DATA()
		
		g_FMMC_STRUCT.iNumPlacedPTFX = 0
		FOR ii = 0 TO FMMC_MAX_PLACED_PTFX-1
			CLEAR_PLACED_PTFX_DATA(ii)
		ENDFOR
		
		g_FMMC_STRUCT_ENTITIES.iNumberOfPlacedMarkers = 0
		FOR ii = 0 TO FMMC_MAX_PLACED_MARKERS-1
			g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].vPosition = <<0.0, 0.0, 0.0>>
			g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].eType = PLACED_MARKER_TYPE_DOWN_ARROW
			g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].fScale = 1.0
			g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].fSpeed = 1.0
			g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].iColour = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].fAlpha = 100.0
			g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].iHideOnRulesBS = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].iHideOnRulesTeam = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].vOffset = <<0.0, 0.0, 0.0>>
			g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].fMaxAnimTravelDistance = 0.5
			INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].iShowOnPrereqSetBS, 0)
			INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sPlacedMarker[ii].iHideOnPrereqSetBS, 0)
		ENDFOR
		
		FOR ii = 0 TO FMMC_MAX_ELEVATORS - 1
			RESET_ELEVATOR_STRUCT(ii)
		ENDFOR
		
		FOR ii = 0 TO FMMC_MAX_TEAMS_AND_JIP_SPEC-1
			g_FMMC_STRUCT.tl63_RoamingSpectatorObjectiveTextOverride[ii] = ""
		ENDFOR
		g_FMMC_STRUCT.fTurretAmmoInSeconds = 0.0
		g_FMMC_STRUCT.iTurretMissileCD = -1
		
		CLEAR_UNIT_STRUCT()
		
		// KotH Data // // //
		g_FMMC_STRUCT.sKotHData.iKotHBS = 0
		FOR ii = 0 TO FMMC_MAX_NUM_DM_BOUNDS - 1
			FMMC_REMOVE_ALL_KOTH_HILLS() // Also clears iNumberOfHills
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].iBoundsShape = ciDMBOUNDSSHAPE_SPHERE
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].iOOBTimer = ciDM_OUT_OF_BOUNDS_TIME
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].iBoundsSetting = ciDMBOUNDSSETTING_AUTO
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].vBoundsCentre = <<0,0,0>>
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].fBoundsRadius = 80.0
			
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].vBoundsPoints[0] = <<0,0,0>>
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].vBoundsPoints[1] = <<0,0,0>>
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].fBoundsHeight = 50.0
			
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].iShrinkToPercent = 100
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].iShrinkTime = 0
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].iShrinkDelay = 0
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[ii].iShrinkDelayCondition = 0
		ENDFOR
		// // // // // // // //
		g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex = 0
		
		IF bFlattenMissionTypeBitset
			PRINTLN("CLEAR_FMMC_STRUCT - g_FMMC_STRUCT.iMissionTypeBitSet			= 0")
			g_FMMC_STRUCT.iMissionTypeBitSet			= 0
			g_FMMC_STRUCT.iMissionTypeBitSetTwo			= 0
		ELSE
			PRINTLN("CLEAR_FMMC_STRUCT - g_FMMC_STRUCT.iMissionTypeBitSet unchanged.")
		ENDIF
		
		g_FMMC_STRUCT.iStunGunCharges = -1
		
		g_FMMC_STRUCT.fStickyBombAOE  = 1.0
		
		
		g_FMMC_STRUCT.iMissionTutorial					= 0
		FOR i = 0 TO FMMC_MAX_NUM_SMS-1
			g_FMMC_STRUCT.sSMS[i].iTeam						= -1
			g_FMMC_STRUCT.sSMS[i].iTriggerFromDT			= -1
			g_FMMC_STRUCT.sSMS[i].iRule						= -1
			g_FMMC_STRUCT.sSMS[i].iTiming					= 0
			g_FMMC_STRUCT.sSMS[i].iDelaySeconds				= 0
			g_FMMC_STRUCT.sSMS[i].iPointsRequired			= -1
			g_FMMC_STRUCT.sSMS[i].iContactCharacter			= -1
			g_FMMC_STRUCT.sSMS[i].iExtraInfo				= ciSMS_EXTRA_INFO_NONE
			g_FMMC_STRUCT.sSMS[i].iBitset					= 0
			FOR ii = 0 TO FMMC_NUM_LABELS_FOR_SMS -1
				g_FMMC_STRUCT.sSMS[i].tl63Message[ii] = ""
			ENDFOR
			g_FMMC_STRUCT.sSMS[i].iSendToTeams  		= 0
		ENDFOR
		
		g_FMMC_STRUCT.iLaunchTimesBit					= 0
		g_FMMC_STRUCT.iLaunchStartTime					= 0
		g_FMMC_STRUCT.iLaunchEndTime					= 0
		
		g_FMMC_STRUCT.iMusic							= -1		
		g_FMMC_STRUCT.iAudioScore						= 0
		g_FMMC_STRUCT.iAudioScoreStartingMood 			= -1
		g_FMMC_STRUCT.iAudioBlockScoreStartingonCP		= 0
		g_FMMC_STRUCT.iNewAudioScore					= -1
		g_FMMC_STRUCT.iMyTeam							= 0 
		g_FMMC_STRUCT.vCameraPanPos 					= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.vCameraPanRot 					= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.vCameraOutroPos 					= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.vCameraOutroRot 					= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.vCameraFinishPos 					= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.vCameraFinishRot 					= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.fCameraFinishFOV					= 0.0
		g_FMMC_STRUCT.fCameraPanHead					= 0.0
		g_FMMC_STRUCT.fDeathmatchBounds					= 0.0
		g_FMMC_STRUCT.mnGangHideoutPed 					= DUMMY_MODEL_FOR_SCRIPT
		g_FMMC_STRUCT.bMissionIsPublished				= FALSE
		g_FMMC_STRUCT.bIsPremiumRace                	= FALSE
		g_FMMC_STRUCT.bNotifyLocalisation				= FALSE
		g_FMMC_STRUCT.iDisabledRestrictedAreasBitSet	= 0
		REPEAT FMMC_VEHICLE_CLASS_MAX+ciVEHICLE_CLASS_MAX_OFFSET i
			INT iVehBS = 0
			FOR iVehBS = 0 TO ciNumOfDLCVehBitsets-1
				g_FMMC_STRUCT.iAvailDLCVehicleBS[i][iVehBS] = 0
			ENDFOR
			g_FMMC_STRUCT.iAvailableVehicleBitSet[i] = 0
		ENDREPEAT
	ENDIF
	
	g_FMMC_STRUCT.tl63DMHelptext					= ""
	FOR i = 0 TO DM_HELPTEXT_MAX - 1
		g_FMMC_STRUCT.tl63DMHelptextSaved[i]		= ""
		
		INT t
		FOR t = 0 TO FMMC_MAX_TEAMS - 1
			g_FMMC_STRUCT.tl63DMHelptextTeams[i][t]		= ""
		ENDFOR
		g_FMMC_STRUCT.iDMHelptextDelay[i] = -1
	ENDFOR
	
	FOR i = 0 TO ci_CHECKPOINT_HELP_TEXT_MAX-1
		g_FMMC_STRUCT.tl23CheckPointHelpText[i] 		= ""
	ENDFOR
	
	REPEAT MAX_VS_STYLES i
		g_FMMC_STRUCT.biVSAvailableOutfit[i][0] = 0
		g_FMMC_STRUCT.biVSAvailableOutfit[i][1] = 0
		g_FMMC_STRUCT.iVSDefaultOutfit[i] = -1
	ENDREPEAT
	g_FMMC_STRUCT.biVSAllowFutureOutfits = 0
	g_FMMC_STRUCT.iVSDefaultStyleClass = -1
	
	g_FMMC_STRUCT.iStrand = -1
	g_FMMC_STRUCT.tl23CustomTimerString = ""
	
	FOR i = 0 TO FMMC_MAX_SPEAKERS - 1
		g_FMMC_STRUCT.sSpeakers[i] = ""
		g_FMMC_STRUCT.iSpeakerPlayers[i] = -1
	ENDFOR
	
	g_FMMC_STRUCT.iTeamStreaming = 0
	g_FMMC_STRUCT.iBS_MissionTeamRelations = 0

	g_FMMC_STRUCT.vDetailsPhotoPos = <<0,0,0>>
	g_FMMC_STRUCT.iCelebrationType = -1
	g_FMMC_STRUCT.tl63MissionName = ""
	#IF IS_DEBUG_BUILD
		g_FMMC_STRUCT.tl63DebugMissionName		= ""
	#ENDIF
	g_FMMC_STRUCT.tl63PointsTitle = ""
	g_FMMC_STRUCT.tl63BlimpMsg = ""
	IF bClearNextContentID
		FOR i = 0 TO (FMMC_MAX_STRAND_MISSIONS - 1)
			g_FMMC_STRUCT.tl23NextContentID[i] = ""
			g_FMMC_STRUCT.eBranchingTransitionType[i] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
		ENDFOR
	ENDIF
	FOR i = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION - 1)
		g_FMMC_STRUCT.tl63MissionDecription[i] = ""
	ENDFOR
	CLEAR_CURRENT_MISSION_TAGS()
	FOR i = 0 TO (FMMC_NUM_LABELS_FOR_SMS - 1)
		g_FMMC_STRUCT.sSMS[0].tl63Message[i] = ""
	ENDFOR
	CLEAR_DEFAULT_VEHICLES_PER_TYPE()
	FOR i = 0 TO FMMC_MAX_TEAMS-1
	
		g_FMMC_STRUCT.iMaxNumPlayersPerTeam[i] = 0
		g_FMMC_STRUCT.iCriticalMinimumForTeam[i] = 0
		FOR i2 = 0 TO FMMC_MAX_ROLES-1
			g_FMMC_STRUCT.iCriticalMinimumForRole[i][i2] = 0
		ENDFOR
		g_FMMC_STRUCT.iNumPlayersPerTeam[i] = 1
		
		g_FMMC_STRUCT.vPostMissionPos[i] = <<0.0,0.0, 0.0>>
		g_FMMC_STRUCT.vPostMissionPOI[i] = <<0.0,0.0, 0.0>>
		g_FMMC_STRUCT.fPostMissionSpawnRadius[i] = 0.0
		
		g_FMMC_STRUCT.vTripSkipCamPos[i] 	= <<0,0,0>>
		g_FMMC_STRUCT.vTripSkipCamRot[i]	= <<0,0,0>>
		g_FMMC_STRUCT.vTripSkipPos[i]		= <<0,0,0>>
		g_FMMC_STRUCT.fTripSkipCamFOV[i]	= 45.0
		g_FMMC_STRUCT.fTripSkipHeading[i]	= 0.0
		
		g_FMMC_STRUCT.iTeamColourOverride[i] = -1
		g_FMMC_STRUCT_ENTITIES.iTeamStartWeaponPallet[i] = -1
		
		g_FMMC_STRUCT.tl23CustomTargetString[i]	= ""
		g_FMMC_STRUCT.tl23CustomTargetStringTwo[i] = ""
		g_FMMC_STRUCT.iCustomTargetStringRuleBitset[i]	= 0
		
		g_FMMC_STRUCT.tl63MultiRuleTimerFailString[i] = ""
		
		g_FMMC_STRUCT.iTeamOutfitOverride[i] = -1
		
		FOR i2 = 0 TO FMMC_MAX_TIMER_FAIL_STRINGS-1
			g_FMMC_STRUCT.tl63ObjectiveTimerFailString[i][i2] = ""
		ENDFOR
		
		g_FMMC_STRUCT.iTeamParachuteTint[i] = -1
		g_FMMC_STRUCT.iTeamParachutePackTint[i] = -1
		
		g_FMMC_STRUCT.tlCustomWastedTitle[i] = ""
		g_FMMC_STRUCT.tlCustomWastedStrapline[i] = ""
		g_FMMC_STRUCT.iCustomWastedShardType[i] = 0
		
		g_FMMC_STRUCT.iRestrictTeamVehicleEntry[i] = 0
		
	ENDFOR
	
	g_FMMC_STRUCT.iTurfWarTimeInterval				= 0
	g_FMMC_STRUCT.iTurfWarPointsToAward				= 0
	
	g_FMMC_STRUCT.iTurfWarColourTeam1				= 0
	g_FMMC_STRUCT.iTurfWarColourTeam2				= 1
	g_FMMC_STRUCT.iTurfWarColourTeam3				= 2
	g_FMMC_STRUCT.iTurfWarColourTeam4				= 3
	g_FMMC_STRUCT.iTurfWarColourContested			= 4
	g_FMMC_STRUCT.iTurfWarColourTouching			= 6
	g_FMMC_STRUCT.iTurfWarColourUnclaimed			= 7
	g_FMMC_STRUCT.iTurfWarColourDisabled			= 9
	
	g_FMMC_STRUCT.iSVMRemoveCorpVehicleBlipRule 	= 0
	
	FMMC_INTERIOR_DESTRUCTION_DATA sEmptyIntDestructionStruct
	FOR i2 = 0 TO ciMAX_INTERIOR_DESTRUCTIONS -1
		g_FMMC_STRUCT.sInteriorDestructionData[i2] = sEmptyIntDestructionStruct
	ENDFOR
	
	FMMC_ENTITY_CHECKPOINT_CONTINUITY_DATA_STRUCT sEmptyCheckpointContinuityStruct
	FOR i2 = 0 TO ciMAX_TRACKED_CHECKPOINT_CONTINUITY_ENTITIES -1
		g_FMMC_STRUCT_ENTITIES.sEntityCheckpointContinuityData[i2] = sEmptyCheckpointContinuityStruct
	ENDFOR
	
	FOR i = 0 TO (ci_KILLSTREAK_POWERUP_MAXNUM - 1)
		g_FMMC_STRUCT.iKSPowerupActivateType[i]		= 0
		g_FMMC_STRUCT.iKSPowerupActivateReq[i]		= 0
	ENDFOR
	
	g_FMMC_STRUCT.iPTLPickupTimeLimit				= 5
	g_FMMC_STRUCT.iPTLWinTimeLimit					= 0
	
	g_FMMC_STRUCT.iTRResetTimer						= 3
		
	g_FMMC_STRUCT.iHRDTARPickTargetTimer			= 60000
	
	//Showdown
	g_FMMC_STRUCT.fShowdown_PointsDamagePerHit	= 1.0
	g_FMMC_STRUCT.fShowdown_TickDrainSpeed	= 0.5
	g_FMMC_STRUCT.fShowdown_TickDrainSpeed_TwoPlayers	= 0.5
	g_FMMC_STRUCT.fShowdown_StartingPoints	= 50.0
	g_FMMC_STRUCT.fShowdown_MaximumPoints = 100.0
	
	//Bomb Football
	//g_FMMC_STRUCT.iBMBF_GoalPoints = 2
		
	//Target Practice
	g_FMMC_STRUCT.iTargetPracticeTimeBonus			= ciTARGET_PRACTICE_DEFAULT_TIME_BONUS
	g_FMMC_STRUCT.iTargetPracticeResetTime			= ciTARGET_PRACTICE_DEFAULT_RESET_TIME
	
	//Lions Den
	g_FMMC_STRUCT.iLionsDenPickupCollectionDelay	= ciLIONS_DEN_PICKUP_COLLECTION_DELAY
	
	g_FMMC_STRUCT.vAutoTripSkipCamPos				= <<0,0,0>>
	g_FMMC_STRUCT.vAutoTripSkipCamRot				= <<0,0,0>>
	g_FMMC_STRUCT.vAutoTripSkipPos					= <<0,0,0>>
	g_FMMC_STRUCT.fAutoTripSkipCamFOV				= 45.0
	g_FMMC_STRUCT.fAutoTripSkipHeading				= 0.0
	g_FMMC_STRUCT.iAutoTripSkipVehicle				= 0
	g_FMMC_STRUCT.iSafeDriveMultiplier				= 10000
	g_FMMC_STRUCT.iStuckTimeOut						= 0
	g_FMMC_STRUCT.iNumberOfBounces					= 1
	
	g_FMMC_STRUCT.fMeleeDamageModifier				= 1.0
	
	g_FMMC_STRUCT.iTargetScoreMultiplierSetting		= 1
	
	g_FMMC_STRUCT.iGunsmithLoadoutSetting			= 0
	
	g_FMMC_STRUCT.fTimeWeaponsDisabledAtStart		= 0.0
	
	g_FMMC_STRUCT.fCopPerceptionOverrideSeeingRange			= 60.0
	g_FMMC_STRUCT.fCopPerceptionOverridePeripheralRange		= 5.0
	g_FMMC_STRUCT.fCopPerceptionOverrideHearingRange		= 60.0
	g_FMMC_STRUCT.fCopPerceptionOverrideRearViewSeeingRange	= -1.0
	
	g_FMMC_STRUCT.iBomberModeTotalBombsPerPlayer		= ciBMB_DEFAULT_TOTAL_BOMBS_PER_PLAYER		
	g_FMMC_STRUCT.iBomberModeExplosionRadius			= ciBMB_DEFAULT_EXPLOSION_RADIUS			
	g_FMMC_STRUCT.iBomberModeExplosionTimer				= ciBMB_DEFAULT_EXPLOSION_TIMER				
	g_FMMC_STRUCT.iBomberModeExplosionStaggerInterval	= ciBMB_DEFAULT_EXPLOSION_STAGGER_INTERVAL 	
	g_FMMC_STRUCT.fBomberModeExplosionDistanceIncrement	= ciBMB_DEFAULT_EXPLOSION_DISTANCE_INCREMENT
	g_FMMC_STRUCT.fBomberModeExplosionInitialOffset		= ciBMB_DEFAULT_EXPLOSION_OFFSET	
	
	g_FMMC_STRUCT.iFvjHeartbeatDistance					= 25
	g_FMMC_STRUCT.iFvjHeartbeatMinDistance				= 5
	g_FMMC_STRUCT.iFvjFriendlyVibrDistance				= 5
	g_FMMC_STRUCT.iFvjFriendlyVibrCooldown				= 2000
	
	g_FMMC_STRUCT.fCopDelayedReportTime		= -1.0
	g_FMMC_STRUCT.tlCustomShardTitle		= ""
	g_FMMC_STRUCT.tlCustomShardStrapline	= ""
	g_FMMC_STRUCT.tlCustomShardTitle2		= ""
	g_FMMC_STRUCT.tlCustomShardStrapline2	= ""
	g_FMMC_STRUCT.tlCustomShardTitle3		= ""
	g_FMMC_STRUCT.tlCustomShardStrapline3	= ""
	
	g_FMMC_STRUCT.fBEVCamHeightDampening = 0.075
	g_FMMC_STRUCT.fBEVCamHeadDampening = 0.075
	
	FOR i = 0 TO ciFORCE_STATIC_CAMERA_MAX_LOCATIONS - 1
		g_FMMC_STRUCT.vBEVCamFSLPos[i] = <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.vBEVCamFSLRotation[i] = <<0.0, 0.0, 0.0>>
	ENDFOR
	
	g_FMMC_STRUCT.iRATCBeastTimer = 0
	g_FMMC_STRUCT.iRATCBeastColour = 0
	g_FMMC_STRUCT.iRATCThermalTimer = 0
	g_FMMC_STRUCT.iRATCSwapTimer = 0
	g_FMMC_STRUCT.iRATCFilterTimer = 0
	g_FMMC_STRUCT.iRATCSlowDownTimer = 0
	g_FMMC_STRUCT.iRATCBulletTimeTimer = 0
	g_FMMC_STRUCT.iRATCBullsharkTimer = 0
	g_FMMC_STRUCT.iRATCHideBlipsTimer = 0
	g_FMMC_STRUCT.fRATCSlowDown = -1.0
	g_FMMC_STRUCT.iRATCRandomBS	= 0
	g_FMMC_STRUCT.iPowerUp = 0
	
	g_FMMC_STRUCT.iCriticalTeamNumber = 0
	
	FOR i = 0 TO FMMC_MAX_CUSTOM_FAIL_NAMES-1
		g_FMMC_STRUCT_ENTITIES.tlCustomPedName[i] = ""
		g_FMMC_STRUCT_ENTITIES.tlCustomVehName[i] = ""
		g_FMMC_STRUCT_ENTITIES.tlCustomObjName[i] = ""
	ENDFOR
	
	FOR i = 0 TO ciPED_SAS_PRESET_MAX-1
		CLEAR_STEALTH_AND_AGGRO_DATA(g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[i])
	ENDFOR
	
	FOR i = 0 TO (ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES - 1)
		g_FMMC_STRUCT.tl63ObjectiveText_SpawnGroupOverride[i] = ""
	ENDFOR
	
	FOR i = 0 TO FMMC_GET_MAX_SCRIPTED_CUTSCENES() - 1
		CLEAR_CUTSCENE_DATA(g_FMMC_STRUCT.sScriptedCutsceneData[i])
	ENDFOR
	FOR i = 0 TO MAX_MOCAP_CUTSCENES-1
		CLEAR_MOCAP_CUTSCENE_DATA(g_FMMC_STRUCT.sMocapCutsceneData[i])
	ENDFOR
	CLEAR_MOCAP_CUTSCENE_DATA(g_FMMC_STRUCT.sEndMocapSceneData)
	
	FOR i = 0 TO FMMC_MAX_TRAINS - 1
		CLEAR_TRAIN_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i])
	ENDFOR
	g_FMMC_STRUCT_ENTITIES.iNumberOfTrains = 0
	
	FOR i = 0 TO GET_FMMC_MAX_WORLD_PROPS()-1
		CLEAR_WORLD_PROP_DATA(g_FMMC_STRUCT_ENTITIES.sWorldProps[i])
	ENDFOR
	g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps = 0
	
	FOR i = 0 TO FMMC_MAX_NUM_ZONE_ROCKET_POS-1
		g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[i] = <<0,0,0>>
		g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketZoneBS[i] = 0
		g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[i] = -1
		g_FMMC_STRUCT_ENTITIES.fFMMCZoneRocketAccuracy[i] = 100.0
		g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketCustomAccuracyOnlyInThisVeh[i] = -1
	ENDFOR
	g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketNotHomingBS = 0
	g_FMMC_STRUCT_ENTITIES.iNumberOfRocketPositions = 0
	
	FOR i = 0 TO FMMC_MAX_DIALOGUES-1
		CLEAR_DIALOGUE_TRIGGER_DATA(g_FMMC_STRUCT.sDialogueTriggers[i], i)
	ENDFOR
	
	g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[0] = 0
	g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[1] = 0
	g_FMMC_STRUCT.iNumberOfDialogueTriggers = 0
	
	FOR i = 0 TO FMMC_MAX_DUMMY_BLIPS-1
		CLEAR_DUMMY_BLIP_DATA(g_FMMC_STRUCT_ENTITIES.sDummyBlips[i])
	ENDFOR
	#IF IS_DEBUG_BUILD
	CLEAR_DUMMY_BLIP_DATA(g_FMMC_STRUCT.sCurrentDummyBlip)
	REMOVE_ALL_DUMMY_BLIPS()
	#ENDIF
	
	g_FMMC_STRUCT.iAdversaryModeType = 0
	
	IF bSetRaceTypeToStandard
		g_FMMC_STRUCT.iMissionSubType = 0
	ENDIF
	
	g_FMMC_STRUCT.biTeamTestComplete = 0
	
	g_FMMC_STRUCT.iHeistCSBitSet = 0
	g_FMMC_STRUCT.iRuleOptionsBitSet = 0
	
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		g_FMMC_STRUCT.iQuickRestartAnim[i] = -1
		g_FMMC_STRUCT.iPostMissionSceneId[i] = -1
		g_FMMC_STRUCT.iPostMissionSceneId_OverrideWithSpawnGroup[i] = -1
		
		INT iPMC = 0
		FOR iPMC = 0 TO FMMC_MAX_PMC_OVERRIDES-1
			g_FMMC_STRUCT.iPostMissionSceneId_Overrides[i][iPMC] = -1
		ENDFOR
	ENDFOR
	
	g_FMMC_STRUCT.eCreatorPostMissionSpawnScenarioPass = POST_MISSION_SPAWN_INVALID
	g_FMMC_STRUCT.eCreatorPostMissionSpawnScenarioFail = POST_MISSION_SPAWN_INVALID
	
	g_FMMC_STRUCT.iShowPackageHudForTeam = 0
	
	g_FMMC_STRUCT.iSuddenDeathStartRule = 0
	
	g_FMMC_STRUCT.iMaxTeamDifference = 0
	g_FMMC_STRUCT.iCustomRoundLengthDefault = 0
	g_FMMC_STRUCT.iMinGangRank = 0
	g_FMMC_STRUCT.iMinGangHate = 0
	
	g_FMMC_STRUCT.fZOffset = 0.5
	
	g_FMMC_STRUCT.iNumOfRounds = 1
	g_FMMC_STRUCT.iTeamBalance = 5 
	
	IF bSetRaceTypeToStandard
		SET_RACE_TYPE(FMMC_RACE_TYPE_STANDARD)
	ENDIF
	
	FOR i = 0 TO (MAX_CAMERA_SHOTS - 1)
		g_FMMC_STRUCT.fLookAtPosX[i] = 0.0
		g_FMMC_STRUCT.fLookAtPosY[i] = 0.0
		g_FMMC_STRUCT.fLookAtPosZ[i] = 0.0
	ENDFOR
	
	//For Team Respawn Vehicles
	FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
		g_FMMC_STRUCT.mnVehicleModel[i] 			= DUMMY_MODEL_FOR_SCRIPT
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
		g_FMMC_STRUCT.iVehicleColour[i] 			= -1
		g_FMMC_STRUCT.iVehicleSecondaryColour[i] 	= -1
		g_FMMC_STRUCT.iVehicleBodyHealth[i]			= 1001
	ENDFOR
	
	g_sHeistStrandMissionRules.bStrandMissionForHeistBoard			= FALSE
	g_FMMC_STRUCT.iPhoto	= 0
	g_FMMC_STRUCT.iWeather = 0
	g_FMMC_STRUCT.iEndCutsceneWeather = 0
	g_FMMC_STRUCT.iAltWeather = 0
	g_FMMC_STRUCT.iAltWeatherChance = 0
	g_FMMC_STRUCT.iNumVehicles = 0
	
	CLEAR_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT.sWarpLocationSettings_EndCutscene)
	
	g_FMMC_STRUCT.iEndCutsceneSpawnGroup = 0
	FOR i2 = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		g_FMMC_STRUCT.iEndCutsceneSpawnSubGroupBS[i2] = 0
	ENDFOR
	
	g_FMMC_STRUCT.iOpenWaterRange = 100
	
	REPEAT COUNT_OF(g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint) i
		g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].vPosition = <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].fHeading = 0.0
	ENDREPEAT
	
	FOR i = 0 TO FMMC_MAX_INVENTORIES-1
		CLEAR_PLAYER_WEAPON_INVENTORY(g_FMMC_STRUCT.sPlayerWeaponInventories[i])
	ENDFOR
	
	CLEAR_POWERUP_DATA()
	
	FOR i = 0 TO (FMMC_MAX_TEAMS -1)
	
		CLEAR_TEAM_DATA(i)
		
		g_FMMC_STRUCT.iCriticalGoToPedDoubleBS[i][0]					= 0
		g_FMMC_STRUCT.iCriticalGoToPedDoubleBS[i][1]					= 0
		g_FMMC_STRUCT.iCriticalGoToPedDoubleBS[i][2]					= 0
		g_FMMC_STRUCT.iGoToPedCompletesRuleDoubleBS[i][0]				= 0
		g_FMMC_STRUCT.iGoToPedCompletesRuleDoubleBS[i][1]				= 0
		g_FMMC_STRUCT.iGoToPedCompletesRuleDoubleBS[i][2]				= 0
		g_FMMC_STRUCT.iMissionCriticalPedDoubleBS[i][0]					= 0
		g_FMMC_STRUCT.iMissionCriticalPedDoubleBS[i][1]					= 0
		g_FMMC_STRUCT.iMissionCriticalPedDoubleBS[i][2]					= 0
		g_FMMC_STRUCT.iMissionCriticalVehBS[i]							= 0
		g_FMMC_STRUCT.iMissionCriticalObjBS[i]							= 0
		FOR i2 = 0 TO MAX_OUTFITS_SIZE-1
			g_FMMC_STRUCT.biOutFitsAvailableBitset[i][i2]	= 0
		ENDFOR
		g_FMMC_STRUCT.biStyleAvailableBitset[i][0]						= 0
		g_FMMC_STRUCT.biStyleAvailableBitset[i][1]						= 0
		g_FMMC_STRUCT.biMaskAvailableBitset[i][0]						= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].vStartPos 					= <<0.0, 0.0, 0.0>>	
		g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos					= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraRot					= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.sFMMCEndConditions[i].vDeliveryPos 				= <<0.0, 0.0, 0.0>>	
		g_FMMC_STRUCT.sFMMCEndConditions[i].iDeliveryRadius 			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].vEndPos   					= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.sFMMCEndConditions[i].iEndRadius 					= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].vAreaPos  					= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.sFMMCEndConditions[i].iAreaRadius 				= -1  				
		g_FMMC_STRUCT.sFMMCEndConditions[i].iAreaTime 					=  0	
		g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayerLives 				=  0	
		g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfPlayerKillsRule 	= -1 	
		g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfPedKillsRule 		= -1  	
		g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfVehKillsRule 		= -1  		
		g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfObjKillsRule 		= -1  		
		g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfDeathsRule 		= -1  		
		g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfPedDeliveryRule 	= -1	
		g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfVehDeliveryRule 	= -1	
		g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfObjDeliveryRule 	= -1	
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionTimeLimit 			= -1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionScoreLimit 			= -1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionLapLimit 			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionLapDefault 			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionLapMinimum 			= 0
		
		g_FMMC_STRUCT.sFMMCEndConditions[i].iBS_MissionTeamRelations_OnRuleEnabled 			= 0		
		g_FMMC_STRUCT.sFMMCEndConditions[i].iBS_MissionTeamRelations_OnModelSwapEnabled 			= 0		
		g_FMMC_STRUCT.sFMMCEndConditions[i].iNumberOfTeamRules 			= -1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iLoseWantedBitset 			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedOnAggroBitset		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iFakeWantedOnAggroBitset	= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupAtMidBitset 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iPhotoCanBeDead		 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedAtMidBitset	 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iSurpressWantedBitset 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamFailBitset 			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iStartingInventory 			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iStartingInventoryTwo		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iStartingInventoryThree		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iStartingInventoryFour		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventoryTwo		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventoryThree	= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2Two	= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2Three	= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iRespawnInVehicleBitset		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iRespawnInLastMissionVehicleFromBS		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iPointsPerKill				= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionTargetScore			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayAreaTimer				= 60000
		g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayArea2Timer				= 60000
		g_FMMC_STRUCT.sFMMCEndConditions[i].iVehDropOff_GetInBS			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iLinkedDoorSwingFree1		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iLinkedDoorSwingFree2		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iSpawnInvincibility			= -1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iPegasusLockRule			= -1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamRuleAttempts			= 1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamHackFails				= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iAggroIndexBS_TeamHackFails	= ciMISSION_AGGRO_INDEX_DEFAULT
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamThermalCharges			= -1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iPresetInventorySelection	= -1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iPresetInventoryVariation	= -1
		
		g_FMMC_STRUCT.sFMMCEndConditions[i].iWaitOnDialogue				= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iRepairCarBitset			= 0
		
		FOR i2 = 0 TO (FMMC_MAX_RULES - 1)
			g_FMMC_STRUCT.sFMMCEndConditions[i].iDialogueToWaitFor[i2]				= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iDialogueToWaitForMidPoint[i2]		= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMinVehHealth[i2]					= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMinVehHealthSpookID[i2]			= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMinVehHealthSpookDist[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iBlipOnLocateTime[i2]				= 4
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMaxHealth[i2]						= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iDrunkLevelOnRule[i2]				= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iForceStealthSetting[i2]			= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMoveOverrideSpeed[i2]				= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMeleeDamageModifier[i2]			= 10
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMeleeWeaponDamageModifier[i2]		= 10
			g_FMMC_STRUCT.sFMMCEndConditions[i].iCBManRespawnExclusionRadius[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamWithPlayerPositionToDraw[i2]	= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iVehicleDamageModOutput[i2]			= 10
			g_FMMC_STRUCT.sFMMCEndConditions[i].iVehicleDamageModIntake[i2]			= 10
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRespawnInMissionVehicle[i2]		= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iTakeProgressionAmount[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iBS_MissionTeamRelations_OnRuleSetting[i2] = 0
		ENDFOR
		
		g_FMMC_STRUCT.iPedCarryLimit[i]									= 0
		g_FMMC_STRUCT.iOBJCarryLimit[i]									= 0
		g_FMMC_STRUCT.iRespawnVehicle[i]								= -1
		g_FMMC_STRUCT.iTeamNames[i]										= 0
		g_FMMC_STRUCT.iTeamWanted[i]									= 1
		g_FMMC_STRUCT.iTeamForcedWeapon[i]								= -1
		g_FMMC_STRUCT.tlCustomName[i]									= ""
		g_FMMC_STRUCT.iTeamHealth[i]									= 3
		g_FMMC_STRUCT.iTeamHorn[i]										= -1
		g_FMMC_STRUCT.iTeamVehicleModTurret[i]							= -1
		g_FMMC_STRUCT.iTeamVehicleModArmour[i]							= -1
		g_FMMC_STRUCT.iTeamVehicleModAirCountermeasure[i]				= -1
		g_FMMC_STRUCT.iTeamVehicleModExhaust[i]							= -1		
		g_FMMC_STRUCT.iTeamVehicleModBombBay[i]							= 0
		g_FMMC_STRUCT.iTeamVehicleModSpoiler[i]							= -1
		g_FMMC_STRUCT.iInitialPoints[i]									= 0
		g_FMMC_STRUCT.iTeamRoleNames[i]									= 0
		
		g_FMMC_STRUCT.iTimerStart[i]									= 0
		g_FMMC_STRUCT.iTimerStop[i]										= 0
		
		g_FMMC_STRUCT.iTeamVehicleHealth[i]								= 100
		g_FMMC_STRUCT.fTeamVehicleWeaponDamageScale[i]					= 1.0	
		
		g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[i]					= 0
		
		g_FMMC_STRUCT.iTeamBlipType[i]									= 0
		
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamBitset					= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamBitset2				= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamBitset3				= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamBitset4				= 0
				
		g_sHeistStrandMissionRules.iNumberOfObjectives[i]				= 0
		
		FOR i2 = 0 TO MAX_HEIST_GEAR_BITSETS-1
			g_FMMC_STRUCT.biGearAvailableBitset[i][i2]					= 0
		ENDFOR
		g_FMMC_STRUCT.iGearDefault[i] 									= -1
		
		FOR i2 = 0 TO FMMC_MAX_RESTART_CHECKPOINTS-1
			FOR i3 = 0 TO FMMC_MAX_RESTART_POINTS_PER_RESTART-1
				g_fmmc_Struct.vTeamRestartPoint[i][i2][i3]				= <<0,0,0>>
				g_fmmc_Struct.fTeamRestartHeading[i][i2][i3]			= 0
				g_fmmc_Struct.vTeamRestartFacing[i][i2][i3]				= <<0,0,0>>
			ENDFOR
			g_fmmc_Struct.iRestartRule[i][i2]							= 0
		ENDFOR
		
		FOR i2 = 0 TO VEHICLE_LIBRARY_MAX -1
			g_fmmc_Struct.iAvailableCommonVehicles[i][i2][0]			= 0
			g_fmmc_Struct.iAvailableCommonVehicles[i][i2][1]			= 0
		ENDFOR
		
		FOR i2 = 0 TO FMMC_MAX_TEAMS -1
			g_fmmc_Struct.iLivesToTeam[i][i2] 				= 0
		ENDFOR
		
		FOR i2 = 0 TO VEHICLE_LIBRARY_MAX -1
			g_fmmc_Struct.iAvailableMissionVehiclesForRole[i][0][i2][0] = 0
			g_fmmc_Struct.iAvailableMissionVehiclesForRole[i][1][i2][0] = 0
			g_fmmc_Struct.iAvailableMissionVehiclesForRole[i][0][i2][1] = 0
			g_fmmc_Struct.iAvailableMissionVehiclesForRole[i][1][i2][1] = 0
		ENDFOR
		
		FOR i2 = 0 TO FMMC_PERSONAL_VEHICLE_PLACEMENT_INSTANCES-1
			FOR i3 = 0 TO FMMC_PERSONAL_VEHICLE_PLACEMENT_PER_INSTANCE-1
				g_FMMC_STRUCT.vPersonalVehiclePosition[i][i2][i3]	= <<0,0,0>>
				g_FMMC_STRUCT.fPersonalVehicleHeading[i][i2][i3]	= 0
			ENDFOR
		ENDFOR
		//so they have a weapon by default
//		SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[i].iStartingInventory, ciSTARTING_WEAPON_PISTOL) 
		
		FOR i2 = 0 TO (ciSTARTING_WEAPON_MAX - 1)
			g_FMMC_STRUCT.sFMMCEndConditions[i].iStartingInventoryAmmo[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventoryAmmo[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2Ammo[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].fStartingInventoryMultiplier[i2]	= 1.0
			g_FMMC_STRUCT.sFMMCEndConditions[i].fMidMissionInventoryMultiplier[i2]	= 1.0
			g_FMMC_STRUCT.sFMMCEndConditions[i].fMidMissionInventory2Multiplier[i2]	= 1.0
		ENDFOR
		
		FOR i2 = 0 TO (ciSTARTING_WEAPON_MK2_MAX - 1)
			g_FMMC_STRUCT.sFMMCEndConditions[i].iStartingInventoryAmmoType[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventoryAmmoType[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2AmmoType[i2]	= 0
		ENDFOR
		
		FOR i2 = 0 TO (MAX_PLAYERS_PER_TEAM - 1)
			g_FMMC_STRUCT.sFMMCEndConditions[i].iUnarmedAnimOnCheckPoint0[i2]	= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iUnarmedAnimOnCheckPoint1[i2]	= -1
		ENDFOR
		
		FOR i2 = 0 TO (FMMC_MAX_RULES - 1)
		
			g_FMMC_STRUCT.sFMMCTrainStruct[i2].iTrainBS = 0
			g_FMMC_STRUCT.sFMMCTrainStruct[i2].vStartPos = <<0,0,0>> 
			g_FMMC_STRUCT.sFMMCTrainStruct[i2].vEndPos = <<0,0,0>> 
			g_FMMC_STRUCT.sFMMCTrainStruct[i2].fSpeed = 0
			g_FMMC_STRUCT.sFMMCTrainStruct[i2].iStoppingDistance = 0
			FOR i3 = 0 TO (FMMC_MAX_NUM_OF_TRAIN_CARRIAGES - 1)
				g_FMMC_STRUCT.sFMMCTrainStruct[i2].iCarriageType[i3] = 0
				g_FMMC_STRUCT.sFMMCTrainStruct[i2].iCarriageAttachType[i3] = 0
				g_FMMC_STRUCT.sFMMCTrainStruct[i2].iCarriageAttachID[i3] = 0
			ENDFOR
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitset[i2] 			= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetTwo[i2] 			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetThree[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetFour[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetFive[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetSix[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetSeven[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetEight[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetNine[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetTen[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetEleven[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetTwelve[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetThirteen[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetFourteen[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetFifteen[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetSixteen[i2]		= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].fPedDriveSpeedOverride[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iProgressAnimStageForPed[i2]	= -1
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iForcedHintCamEntityType[i2] = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iForcedHintCamEntityID[i2] = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iForcedHintCamDuration[i2] = ciFMMC_DEFAULT_FORCED_HINT_CAM_LENGTH
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iForcedMinimapInterior[i2] = ciFMMC_FORCED_MINIMAP_INTERIOR_OFF
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iPriorityEntityHealthThresholdOverride[i2] = 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSpecificPrestreamRule[i2] = -1
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRespawnOnWaterDistanceFromCoord[i2] = 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iCustomScoreHUDColour[i2] = -1
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].sRulePlayerModelSwap[i2].iPlayerModelToSwapToOnKill = FMMC_PLAYER_PED_MODELS_OFF
			g_FMMC_STRUCT.sFMMCEndConditions[i].sRulePlayerModelSwap[i2].iPlayerModelToSwapToOnRespawn = FMMC_PLAYER_PED_MODELS_OFF
			g_FMMC_STRUCT.sFMMCEndConditions[i].sRulePlayerModelSwap[i2].iPlayerModelSwapPrereqToComplete = ciPREREQ_None
			
			FOR i3 = 0 TO (FMMC_MAX_VEHICLES - 1)
				g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleVehicleWarp[i2][i3] = -1
			ENDFOR
			FOR i3 = 0 TO (FMMC_MAX_PEDS - 1)
				g_FMMC_STRUCT.sFMMCEndConditions[i].iRulePedWarp[i2][i3] = -1
				g_FMMC_STRUCT.sFMMCEndConditions[i].iPedAssGotoTask_RulePoolAssignmentIndex[i2][i3] = ASSOCIATED_GOTO_TASK_POOL_INDEX__OFF
				g_FMMC_STRUCT.sFMMCEndConditions[i].iPedAssGotoTask_RulePoolAssignmentStartingGoto[i2][i3] = ASSOCIATED_GOTO_TASK_POOL_START__STAY_ON_CURRENT
			ENDFOR
			FOR i3 = 0 TO (FMMC_MAX_NUM_OBJECTS - 1)
				g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleObjWarp[i2][i3] = -1
				g_FMMC_STRUCT.sFMMCEndConditions[i].iDroneAssGotoTask_RulePoolAssignmentIndex[i2][i3] = ASSOCIATED_GOTO_TASK_POOL_INDEX__OFF
				g_FMMC_STRUCT.sFMMCEndConditions[i].iDroneAssGotoTask_RulePoolAssignmentStartingGoto[i2][i3] = ASSOCIATED_GOTO_TASK_POOL_START__STAY_ON_CURRENT
			ENDFOR
			FOR i3 = 0 TO (FMMC_MAX_GO_TO_LOCATIONS - 1)
				g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleLocWarp[i2][i3] = -1
			ENDFOR
			FOR i3 = 0 TO (FMMC_MAX_NUM_INTERACTABLES - 1)
				g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleInteractableWarp[i2][i3] = -1
			ENDFOR
			
			FOR i3 = 0 TO (FMMC_MAX_RESTART_CHECKPOINTS - 1)
				g_FMMC_STRUCT.sFMMCEndConditions[i].iRestartCheckpointBitset[i2][i3] = 0
			ENDFOR
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63RoamSpecObjText[i2] = ""
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63CustomRuleNames[i2] = ""
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iTimerPointsRequired[i2] 	= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].sRuleIncTimerFail[i2].iIncTimerBS		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sRuleIncTimerFail[i2].iStartTime		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sRuleIncTimerFail[i2].fPlacedPedAmountRateInc		= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sRuleIncTimerFail[i2].iPlacedPedAmountThresh		= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAltitudeBar[i2].iBS						= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAltitudeBar[i2].iMinimum					= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAltitudeBar[i2].iThreshold					= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAltitudeBar[i2].iMaximum					= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAltitudeBar[i2].iSpecificVehicle			= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAltitudeBar[i2].iFailTime					= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAltitudeBar[i2].tl63_MainBar				= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAltitudeBar[i2].tl63_ConsequenceBar		= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAltitudeBar[i2].iBlipTime					= 0
			
			INITIALISE_INT_ARRAY(g_FMMC_STRUCT.sFMMCEndConditions[i].iAltVarsBitsetRule[i2], 0)
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iFilterBitset[i2]			= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayerLivesPerRule[i2]		= -1
			
			g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[i2][i]						= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iBoundsPriTimer[i2]			= 60000
			g_FMMC_STRUCT.sFMMCEndConditions[i].iBoundsSecTimer[i2]			= 60000
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iCameraShakeDuration[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iCameraShakeType[i2]				= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].fCameraShakeIntensity[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSecondaryCameraShakeType[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].fSecondaryCameraShakeIntensity[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSecondaryCameraShakeInterval[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSecondaryCameraShakeDuration[i2]	= 0

			FOR i3 = 0  TO (FMMC_MAX_ROLES-1)
				g_FMMC_STRUCT.sFMMCEndConditions[i].tl63RoleName[i3] = ""
				g_FMMC_STRUCT.sFMMCEndConditions[i].iRolesBitset[i2][i3]	= 0
			ENDFOR
			
			g_FMMC_STRUCT.iPerRuleRespawnVehicle[i2][i] 					= -1
			g_FMMC_STRUCT.iPerRuleVehicleRockets[i2][i] 					= 0
			g_FMMC_STRUCT.iPerRuleVehicleBoost[i2][i] 						= 0
			g_FMMC_STRUCT.iPerRuleVehicleRepair[i2][i] 						= 0
			g_FMMC_STRUCT.iPerRuleVehicleSpikes[i2][i] 						= 0
			g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[i2][i] 					= ciVehicleDelayTime
			g_FMMC_STRUCT.iPerRuleVehicleBoostDuration[i2][i] 				= ciVehicleBoostDuration
			g_FMMC_STRUCT.iPerRuleVehicleBoostSpeed[i2][i] 					= ciVehicleBoostSpeed
			
			g_sHeistStrandMissionRules.tl63Objective[i][i2] 				= ""
			g_sHeistStrandMissionRules.iRuleBitset[i][i2]					= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveTimeLimitRule[i2] = 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveSuddenDeathTimeLimit[i2] = 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveScore[i2] 		= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iTargetScore[i2] 			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMaxDeliveryEntitiesCanCarry[i2] 			= 1 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedChange[i2] 			= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedForce[i2] 			= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedDelay[i2] 			= 10000
			g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedDelayHUDString[i2] 	= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iFakeWantedLevel[i2] 		= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedLevelReq[i2] 		= -2
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMaxWanted[i2] 				= 1 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iTimeToLoseCops[i2] 		= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRequiredDeliveries[i2]		= 1
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupType[i2] 		= 0 
			
			INT iUnitType
			FOR iUnitType = 0 TO FMMC_MAX_GANG_CHASE_UNIT_TYPES - 1
				g_FMMC_STRUCT.sFMMCEndConditions[i].sMultipleGangChaseSettings[i2][iUnitType].iType 		= 0 
				g_FMMC_STRUCT.sFMMCEndConditions[i].sMultipleGangChaseSettings[i2][iUnitType].iIntensity 	= 0 
				g_FMMC_STRUCT.sFMMCEndConditions[i].sMultipleGangChaseSettings[i2][iUnitType].iPriority 	= FMMC_MAX_GANG_CHASE_PRIORITY_DEFAULT 
			ENDFOR
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupNumber[i2] 		= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangNGNum[i2] 			= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangLGNum[i2] 			= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupColour[i2] 		= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupDelay[i2] 		= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangChaseRespawnDelay[i2] 	= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangOverrideObjectiveTextCustomString[i2] 	= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangMaxBackup[i2]		 	= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangNGMax[i2]		 		= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangLGMax[i2]		 		= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangAccuracy[i2]		 	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupFleeDist[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangVehHealth[i2]			= 100
			g_FMMC_STRUCT.sFMMCEndConditions[i].vGangTriggerPos1[i2] 		= <<0,0,0>> 
			g_FMMC_STRUCT.sFMMCEndConditions[i].vGangTriggerPos2[i2] 		= <<0,0,0>> 
			g_FMMC_STRUCT.sFMMCEndConditions[i].fGangTriggerWidth[i2]		= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangBackupAreaType[i2]		= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangChaseForwardSpawnChance[i2]			= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGangChaseSpawnBehindChance[i2]				= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].fGangBlipNoticeRange[i2]	= -1.0 
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSwapToTeamOnDeath[i2]		= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSwapToTeamOnKill[i2]		= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iFriendlyFire[i2]			= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iDontSwitchOnSuicide[i2]	= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iArtificalLightTimer[i2]	= FMMC_ARTIFICIAL_LIGHTS_ALWAYS_ON 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMidpointFailTime[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iFailSpeedTimer[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iBombTimer[i2]				= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iGPSDrop[i2]				= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iDisableControlTimer[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjRequiredForRulePass[i2]	= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iCriticalWeapon[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iDisabledWeapon[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iOutfit[i2]					= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iOutfitVariations[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMinSpeed[i2]				= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMinSpeed_AddPerLap[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].IMinSpeed_SpecificVehicle[i2] = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iIdleBlipTimer[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMinSpeed_MeterMax[i2]		= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iProgressMinigame[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iShardText[i2]				= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iHitSound[i2]				= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleTraffic[i2]			= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iNearestTargetThreshold[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRulePedDensity[i2]			= -1
			g_FMMC_STRUCT.SFMMCEndConditions[i].iThermalStartDistance[i2]	= 5
			g_FMMC_STRUCT.SFMMCEndConditions[i].iThermalEndDistance[i2]		= 20
			g_FMMC_STRUCT.SFMMCEndConditions[i].iOverrideNightvisionRange[i2] = -1
			
			g_FMMC_STRUCT.SFMMCEndConditions[i].iHunterVisionDuration[i2] = 0
			g_FMMC_STRUCT.SFMMCEndConditions[i].iHunterVisionCooldown[i2] = 0
			
			INT iClearPreReqsOnRuleBSIndex = 0
			FOR iClearPreReqsOnRuleBSIndex = 0 TO ciPREREQ_Bitsets - 1
				g_FMMC_STRUCT.sFMMCEndConditions[i].iClearPrereqsOnRuleBS[i2][iClearPreReqsOnRuleBSIndex] = 0
				g_FMMC_STRUCT.sFMMCEndConditions[i].iSetPrereqsOnRuleBS[i2][iClearPreReqsOnRuleBSIndex] = 0
			ENDFOR
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicStart[i2] 			= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicMid[i2] 				= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iAudioMix[i2] 				= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMidPointAudioMix[i2] 		= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicSecondaryRoleMood[i2] = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScoreReachedMood[i2]  = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScoreAggroMood[i2]	= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_OnFoot[i2]							= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InAircraft[i2]						= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Parachuting[i2]						= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Swimming[i2]						= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Boating[i2]							= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Driving[i2]							= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace[i2]					= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace_Alt_1[i2]				= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace_Alt_2[i2]				= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_IsInteracting[i2]					= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_WantedLevelGreaterThan1[i2]			= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_OnFoot_Aggro[i2]					= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InAircraft_Aggro[i2]				= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Parachuting_Aggro[i2]				= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Swimming_Aggro[i2]					= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Boating_Aggro[i2]					= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_Driving_Aggro[i2]					= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace_Aggro[i2]				= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace_Aggro_Alt_1[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_InteriorSpace_Aggro_Alt_2[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_IsInteracting_Aggro[i2]				= 0			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_IsBeingChasedByGang[i2]				= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_IsBeingChasedByGang_Aggro[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMusicScore_WantedLevelGreaterThan1_Aggro[i2] 	= 0 
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iAggroIndexBS_Music[i2] = ciMISSION_AGGRO_INDEX_DEFAULT
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iCustomHUDBS[i2] = 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iAggroIndexBS_GangChase_ReqAggro[i2] = ciMISSION_AGGRO_INDEX_DEFAULT			
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iAggroIndexBS_CheckTeamsForAggroWanted[i2] = ciMISSION_AGGRO_INDEX_DEFAULT			
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRestrictedInventory[i2]	= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRestWeapons[i2]			= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iTakeoverTime[i2]			= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iReCapture[i2] 				= 0 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iLinkedDoor[i2] 			= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iLinkedDoor2[i2] 			= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iHackScreen[i2] 			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective[i2] 			= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective1[i2] 			= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective2[i2] 			= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjBlip[i2] 			= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjSingular[i2] 		= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl23ObjPlural[i2] 			= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63SpecSynopsis[0] 		= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63SpecSynopsis[1]			= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63FailReason[i2]			= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63BoundsFailMessage[i2]	= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63WrongWayMessage[i2]		= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63VsShardMessage[i2]		= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63WaitMessage[i2]			= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].tl63ObjectiveEntityHUDMessage[i2]	= ""
			g_FMMC_STRUCT.sFMMCEndConditions[i].iDropOffType[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iDropOffStopTimer[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iDropOff_Vehicle[i2]		= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].vDropOff[i2]				= <<0.0, 0.0, 0.0>>	
			g_FMMC_STRUCT.sFMMCEndConditions[i].vDropOff2[i2]				= <<0.0, 0.0, 0.0>>	
			
			// 2020 Bounds
			CLEAR_ALL_RULE_BOUNDS_ON_RULE(i, i2)
			
			// Legacy Bounds
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iType		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].vPos		= <<0.0, 0.0, 0.0>>	
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].vPos1		= <<0.0, 0.0, 0.0>>	
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].vPos2		= <<0.0, 0.0, 0.0>>	
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].fRadius	= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].fWidth	= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iWantedToGive	= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].MNIgnoreWantedVeh	= DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].MNIgnoreLeaveAreaVeh	= DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].vHeadingVec	= <<0.0, 0.0, 0.0>>	
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iIgnoreWantedOutfit	= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iBoundsBS = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iEntityType = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iEntityID = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iMinimapAlpha = 120
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iLinkedSpawnGroup = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iBoundsBlipHeightThreshold = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].mnOnlyAffectVeh	= DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].hudColouring	= HUD_COLOUR_YELLOW
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iMaxPlayersAllowedInBounds = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iVehicleRegenAndCap = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct[i2].iBoundActivationDelaySeconds = 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iType	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].vPos		= <<0.0, 0.0, 0.0>>	
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].vPos1		= <<0.0, 0.0, 0.0>>	
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].vPos2		= <<0.0, 0.0, 0.0>>	
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].fRadius	= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].fWidth	= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iWantedToGive	= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].MNIgnoreWantedVeh	= DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].MNIgnoreLeaveAreaVeh	= DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].vHeadingVec	= <<0.0, 0.0, 0.0>>	
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iIgnoreWantedOutfit	= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iBoundsBS = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iEntityType = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iEntityID = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iMinimapAlpha = 120
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iLinkedSpawnGroup = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iBoundsBlipHeightThreshold = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].mnOnlyAffectVeh	= DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].hudColouring	= HUD_COLOUR_YELLOW
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iMaxPlayersAllowedInBounds = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iVehicleRegenAndCap = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sBoundsStruct2[i2].iBoundActivationDelaySeconds = 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].fDropOffRadius[i2]			= 3
			g_FMMC_STRUCT.sFMMCEndConditions[i].fDropOffHeight[i2]			= IGNORE_DROP_OFF_HEIGHT
			INITIALISE_INT_ARRAY(g_FMMC_STRUCT.sFMMCEndConditions[i].iDropOffZonesBS[i2], 0)
			g_FMMC_STRUCT.sFMMCEndConditions[i].iTimeLimit[i2]	 			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].fDropOffStopDistance[i2] 	= DEFAULT_VEH_STOPPING_DISTANCE
			g_FMMC_STRUCT.sFMMCEndConditions[i].fDropOffPlayerSpeed[i2] 	= 1
			g_FMMC_STRUCT.sFMMCEndConditions[i].fDropOffVehicleDrag[i2] 	= 1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iLowHealthFilter[i2] 		= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAirstrikeStruct[i2].iEntityType = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAirstrikeStruct[i2].iEntityID = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].sAirstrikeStruct[i2].vTargetVector = <<0,0,0>>
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSpecificTODHour[i2]		= 12
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSpecificTODMinute[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSpecificTODSpeed[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSpecificTODStartHour[i2]	= 12
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iArmourAmount[i2]			= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iBoostThreshold[i2]				= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iMinigunDamage[i2]				= 100
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRespawnOnRuleEndCheckpoint[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].fCaptureIncrease[i2] = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iDeathPenalty[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iDeathPenaltyThreshold[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamEliminationScore[i2]	= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].fExplodeNearbyRocketsRange[i2]	= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[i].fExplodeNearbyRocketsChance[i2]	= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iLocateSwapTeam[i2]	= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iAmbientPoliceAccuracy[i2]	= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iVehDisableOnRule[i2]			= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iShowTeamHackingProgress[i2]			= -2
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSendEmail[i2]									= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayDownloadSoundAfterDialogue[i2]				= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iCustomHelpTextPressAccel[i2]	= -1
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].fCatchFireDistance[i2]			= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iCatchFireDelay[i2]				= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iStopFireDelay[i2]				= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iLightningStrikeMin[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iLightningStrikeMax[i2]			= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iLightningStrikeBlipTime[i2]	= 0	
			
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sBeastStruct[ i2 ].iMaxHealth						= 1000
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sBeastStruct[ i2 ].fSpeedMod						= 1.15
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sBeastStruct[ i2 ].iSuperJumpDamage				= 25
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sBeastStruct[ i2 ].iHealthOnKill					= 0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sBeastStruct[ i2 ].iStealthDamage					= 1
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sBeastStruct[ i2 ].iMinPowerHealth				= 25
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sBeastStruct[ i2 ].fHealthRegen					= 10.0	
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sBeastStruct[ i2 ].iMeleeDamageMod				= 100
			
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sRegenStruct[ i2 ].fDistance						= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sRegenStruct[ i2 ].iRegenRate						= 1
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sRegenStruct[ i2 ].iMaxRegen						= 50			
		
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sDrainStruct[ i2 ].fNormalDrain					= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sDrainStruct[ i2 ].fReducedDrain[0]				= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sDrainStruct[ i2 ].fReducedDrain[1]				= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sDrainStruct[ i2 ].fReducedDrain[2]				= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sDrainStruct[ i2 ].fReducedDrain[3]				= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sDrainStruct[ i2 ].fPlayerHeal					= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sDrainStruct[ i2 ].fPedHeal						= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sDrainStruct[ i2 ].fVehicleHeal					= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sDrainStruct[ i2 ].fObjectTimer					= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sDrainStruct[ i2 ].fHealthDmgRestore				= 0.0
			g_FMMC_STRUCT.sFMMCEndConditions[ i ].sDrainStruct[ i2 ].iMissedShotPerc				= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].sLightBarStruct[i2].iLBred							= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sLightBarStruct[i2].iLBgreen						= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].sLightBarStruct[i2].iLBblue							= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectsDestroyedRuleHudAmount[i2]					= 0
			
			INT iLoc = 0
			FOR iLoc = 0 TO FMMC_MAX_GO_TO_LOCATIONS-1
				g_FMMC_STRUCT.sFMMCEndConditions[i].sHideLocationMarkerStruct[i2].iHideBS[iLoc]		= 0
			ENDFOR
			
			INT iPedBS = 0
			FOR iPedBS = 0 TO ciFMMC_PED_BITSET_SIZE-1
				g_FMMC_STRUCT.sFMMCEndConditions[i].iHighPriorityPed[i2][iPedBS] = 0			
			ENDFOR
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].mnVehicleModelSwap[i2]								= DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT.sFMMCEndConditions[i].iVehicleModelSwap[i2]								= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iVehicleHealthSwap[i2]								= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iVehicleTurretSwap[i2]								= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iVehicleArmourSwap[i2]								= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iVehicleAirCounterSwap[i2]							= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iVehicleBombBaySwap[i2]								= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iVehicleExhaustSwap[i2]								= -1
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSVWAmmoMG[i2]										= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSVWAmmoRockets[i2]									= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iSVWAmmoHRockets[i2]								= -1
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveProgressionLogic[i2][0]					= -1 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveProgressionLogic[i2][1]					= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveProgressionLogic[i2][2]					= -1 
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveProgressionLogic[i2][3]					= -1
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveProgMidPointBitset[i2]					= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveMidPointProgressOtherTeamLogic[i2][0] = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveMidPointProgressOtherTeamLogic[i2][1] = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjectiveMidPointProgressOtherTeamLogic[i2][2] = -1
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iRandomNextRuleBS[i2] = 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].iConditionalObjectiveJumpRule[i2] = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iConditionalObjectiveJumpCondition[i2] = 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iLockedInVehBS[i2]			= 0
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].iObjCarryHUDCustomLabel[i2] = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iShowTickerOnRuleStart[i2] = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iCustomMidObjCompTicker[i2] = -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iCustomObjCompTicker[i2] = -1
			
			g_FMMC_STRUCT.sFMMCEndConditions[i].OverwriteEntityType[i2]		= 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].OverwriteEntityID[i2]		= -1
			g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedLevelDisplayBitset[i2] = 0
			
			CLEAR_GUN_ROULETTE_STRUCT(g_FMMC_STRUCT.sFMMCEndConditions[i].sGRouletteStruct[i2])
			
			CLEAR_FILTER_CONTROLS_DATA(g_FMMC_STRUCT.sFMMCEndConditions[i].sFilterControls[i2])
			
			CLEAR_ON_RULE_START_DATA(g_FMMC_STRUCT.sFMMCEndConditions[i].sOnRuleStart[i2])
			
		ENDFOR
		g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayAreaBitset				= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iPlayArea2Bitset			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iLeaveAreaBitset			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iLeaveArea2Bitset			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iSpawnAreaBitset			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iSpawnWhileInArea			= 0	
		g_FMMC_STRUCT.sFMMCEndConditions[i].iSecSpawnAreaBitset			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iSecSpawnWhileInArea			= 0	
		g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedLevelBounds 			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iWantedLevelBounds2			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iDropOffBitSet				= 0		
		g_FMMC_STRUCT.sFMMCEndConditions[i].iSmallDropOffBitSet 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventoryGiveOnTeam 		= -1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventoryGiveOnTeamRule	= -1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2GiveOnTeam 		= -1
		g_FMMC_STRUCT.sFMMCEndConditions[i].iMidMissionInventory2GiveOnTeamRule	= -1
				
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam1BlipVisibility 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam2BlipVisibility 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam3BlipVisibility 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam4BlipVisibility 		= 0
											
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam1HUDVisibility 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam2HUDVisibility 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam3HUDVisibility 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam4HUDVisibility 		= 0
		
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam1HealthBar 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam2HealthBar 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam3HealthBar 		= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iTeam4HealthBar 		= 0	
		
		g_FMMC_STRUCT.sFMMCEndConditions[i].iCanSeeTeam1Lives 			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iCanSeeTeam2Lives 			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iCanSeeTeam3Lives 			= 0
		g_FMMC_STRUCT.sFMMCEndConditions[i].iCanSeeTeam4Lives 			= 0
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		FOR i2 = 0 TO FMMC_MAX_RESTART_CHECKPOINTS - 1
			g_FMMC_STRUCT.iCheckpointMRTThreshold[i2][i] = 0
		ENDFOR
	ENDFOR
	
	g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber = 0
	g_FMMC_STRUCT.iRandomEntitySpawn_RandomNumbersToUse = 0
	g_FMMC_STRUCT.iRandomEntitySpawn_Bitset = 0
	
	FOR i = 0 TO (FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS - 1)
		g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[i] = SPAWN_GROUP_DEFINE_ACTIVATION_OFF
		g_FMMC_STRUCT.iRandomEntitySpawnSub_MaxNumber[i] = 0
		g_FMMC_STRUCT.iRandomEntitySpawnSub_RandomNumbersToUse[i] = 0
		g_FMMC_STRUCT.iRandomEntitySpawnSub_Bitset[i] = 0

		FOR ii = 0 TO (ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS - 1)	
			g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[i][ii] = SPAWN_GROUP_DEFINE_ACTIVATION_OFF
		ENDFOR
	ENDFOR
	
	g_FMMC_STRUCT.vStartPos = <<0.0,0.0,0.0>>
	g_FMMC_STRUCT.vStartingGrid = <<0.0,0.0,0.0>>
	g_FMMC_STRUCT.fStartRadius = 0.0
	g_FMMC_STRUCT.vBuildingWarp = <<0,0,0>>
	g_FMMC_STRUCT.iShowPackageHudForTeam = 0
	g_FMMC_STRUCT.iMaxTeamDifference = 0
	g_FMMC_STRUCT.iHeistCSPedDmgTrackBitSet[0] = 0
	g_FMMC_STRUCT.iHeistCSPedDmgTrackBitSet[1] = 0
	g_FMMC_STRUCT.iHeistCSVehDmgTrackBitSet = 0
	g_FMMC_STRUCT.iHeistCSBitSet = 0
	g_FMMC_STRUCT.iHeistRewardBonusBitset = 0
	g_FMMC_STRUCT.iHeistRewardTimeThreshold = 0
	g_FMMC_STRUCT.iHeistRewardTimeTeam = 0
	g_FMMC_STRUCT.iHeistRewardTimeRule = 0
	g_FMMC_STRUCT.iHeistRewardPedDamageThreshold = 0
	g_FMMC_STRUCT.iHeistRewardVehDamageThreshold = 0
	g_FMMC_STRUCT.iHeistRewardKillsThreshold = 0	
	g_FMMC_STRUCT.iHeistRewardPlayerHealthThreshold = 0	
	g_FMMC_STRUCT.iHeistRewardHeadshotThreshold = 0
	g_FMMC_STRUCT.iHeistRewardHackThreshold = 0
	g_FMMC_STRUCT.iHeisteliteChallengeReward = 0
	
	g_FMMC_STRUCT.iHeistRewardMMTimerTeam = 0
	g_FMMC_STRUCT.iHeistRewardMMTimerStartRule = 0
	g_FMMC_STRUCT.iHeistRewardMMTimerEndRule = 0
	g_FMMC_STRUCT.iHeistRewardMMTimerThreshold = 0
	
	//There will be many more to go in here
	g_FMMC_STRUCT.iPlayerEndPassMocap = -1
	g_FMMC_STRUCT.iPlayerEndFailMocap = -1
	
	g_FMMC_STRUCT.fCargobobDamp = 1.5


	//This is for the racing
	
	g_FMMC_STRUCT.iCheckpointPlacementType = 0
	g_FMMC_Struct.iCheckpointFunctionality = 0
	
	FOR i = 0 TO (GET_FMMC_MAX_NUM_CHECKPOINTS() - 1)
		CLEAR_CHECKPOINT_DATA(i)
	ENDFOR
	g_FMMC_STRUCT.iNumberOfCheckPoints = 0
	
	IF bReSetMenuItems
		g_FMMC_STRUCT.iNumLaps = 0
		g_FMMC_STRUCT.iRaceClassBitSet = 0
		g_FMMC_STRUCT.iVehicleBoostEnableDelay = 0
		g_FMMC_STRUCT.iVehicleBoostEnableOnLap = 1
	ENDIF
	
	g_FMMC_STRUCT.fOpenWheelTireWear[TIRE_TYPE_HARD] = 1.0
	g_FMMC_STRUCT.fOpenWheelTireWearScale[TIRE_TYPE_HARD] = 1.2
	g_FMMC_STRUCT.fOpenWheelTireWearDifference[TIRE_TYPE_HARD] = 0.8
	
	g_FMMC_STRUCT.fOpenWheelTireWear[TIRE_TYPE_MED] = 1.0
	g_FMMC_STRUCT.fOpenWheelTireWearScale[TIRE_TYPE_MED] = 1.7
	g_FMMC_STRUCT.fOpenWheelTireWearDifference[TIRE_TYPE_MED] = 0.45
	
	g_FMMC_STRUCT.fOpenWheelTireWear[TIRE_TYPE_SOFT] = 1.0
	g_FMMC_STRUCT.fOpenWheelTireWearScale[TIRE_TYPE_SOFT] = 2.2
	g_FMMC_STRUCT.fOpenWheelTireWearDifference[TIRE_TYPE_SOFT] = 0.05
	
	g_FMMC_STRUCT.fOpenWheelTireRepairRate = 40.0
	
	g_FMMC_STRUCT.iRaceInitialCPWantedLevel = 0
	g_FMMC_STRUCT.iRaceMaxWantedLevel = 3
	g_FMMC_STRUCT.iRaceCoronaMinWantedLevel = 3
	g_FMMC_STRUCT.iRaceCoronaMaxWantedLevel = 5
	g_FMMC_STRUCT.fPursuitFinishEvasionRadius = 250.0
	g_FMMC_STRUCT.iPursuitClearWantedDelay = 0
	
	g_FMMC_STRUCT.iRaceFinishTimer = -1
		
	FOR i = 0 TO FMMC_MAX_TRANSFORM_MODELS-1
		g_FMMC_STRUCT.mnTransformModel[i] = DUMMY_MODEL_FOR_SCRIPT
	ENDFOR
	
	g_FMMC_STRUCT.iTriathlonTransition1				= 0
	g_FMMC_STRUCT.iTriathlonTransition2				= 0
	g_FMMC_STRUCT.fRaceStartHeading					= 0.0
	
	g_FMMC_STRUCT_ENTITIES.iNumberOfProps = 0
	FOR i = 0 TO (GET_FMMC_MAX_NUM_PROPS() - 1)
		CLEAR_PROP(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i])
	ENDFOR
	CLEAR_PROP(g_FMMC_STRUCT_ENTITIES.sEditedProp)
	
	CLEAR_CUSTOM_PROP_LIST()
	
	FOR i = 0 TO FMMC_MAX_PROP_TEMPLATES-1
		CLEAR_PROP_TEMPLATE(g_fmmc_struct.sSavedPropTemplates[i])
	ENDFOR
	g_FMMC_STRUCT.iNumberOfPropTemplates = 0
	
	g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps = 0
	g_FMMC_STRUCT_ENTITIES.iNumberOfAmmoCrates = 0
	
	CLEAR_ELECTRONIC_DATA(g_FMMC_STRUCT_ENTITIES.sEditedDynoProp.sDynoPropElectronicData)	
	CLEAR_TRAIN_ATTACH_STRUCT(g_FMMC_STRUCT_ENTITIES.sEditedDynoProp.sDynopropTrainAttachmentData)
	
	FOR i = 0 TO (FMMC_MAX_NUM_DYNOPROPS - 1)
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].vPos			= <<0.0,0.0,0.0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].fHead			= 0.0
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn				= DUMMY_MODEL_FOR_SCRIPT
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iObjReference  = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedTeam = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedObjective = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedSpawn = 1
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iDynopropBitset = 0 
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iRespawnOnRuleChangeBS = 0
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iRespawnOnRule, -1)
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAssociatedActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSecondActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iThirdActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iFourthActionStart = ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iCleanupObjective = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iCleanupTeam = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iCleanupRange = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iPropKamaTeam = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].fDynoPropLODDistance = 0.0
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iPropColour = 1
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iPropCleanUpLap = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iPropSpawnLap = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iPropHealthOverride = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iPropBelongsToTeam = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iContinuityId = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iLinkedRegularPropIndex = -1
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAltVarsBitsetDynoProp, 0)
		CLEAR_ENTITY_BLIP_DATA_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sDynoPropBlipStruct)
		CLEAR_TRAIN_ATTACH_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sDynopropTrainAttachmentData)
		CLEAR_ELECTRONIC_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sDynoPropElectronicData)
		
		CLEAR_CLEANUP_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sCleanupData)
		
		CLEAR_ENTITY_PTFX_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sEntitySpawnPTFX)
		CLEAR_ENTITY_PTFX_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sEntityDespawnPTFX)
		
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iAggroIndexBS_Entity_DynoProp = ciMISSION_AGGRO_INDEX_DEFAULT
		
		FOR ii = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
			g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].eSpawnConditionFlag[ii] = SPAWN_CONDITION_FLAG_NOT_SET
		ENDFOR
		
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSpawnGroup = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSpawnSubGroup = 0
		FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iSpawnSubGroupBS[ii] = 0
		ENDFOR
		CLEAR_PROXIMITY_SPAWNING_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sProximitySpawningData)
		
		CLEAR_ENTITY_RENDER_TARGET_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sDynoPropRenderTargetOptions)
		
		FOR ii = 0 TO FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS - 1
			g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iDynopropSpawnGroupModelSwapStringIndexes[ii] = -1
		ENDFOR
	ENDFOR
	
	FMMC_CLEAR_ALL_INTERACTABLE_DATA()
	
	g_FMMC_STRUCT_ENTITIES.iNumberOfDoors = 0
	FOR i = 0 TO (FMMC_MAX_NUM_DOORS - 1)
		CLEAR_DOOR_STRUCT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i])
	ENDFOR
	
	g_FMMC_STRUCT_ENTITIES.iNumberOfCoverPoints = 0
	FOR i = 0 TO (FMMC_MAX_NUM_COVER - 1)
		g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos				= <<0.0,0.0,0.0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].fDirection		= 0.0
		g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverUsage		= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverHeight		= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverArc			= 0
	ENDFOR
									 
	IF bReSetMenuItems
		g_FMMC_STRUCT.fLR_GridSpacing 					= 3.5
		g_FMMC_STRUCT.fUD_GridSpacing					= 7.0
		g_FMMC_STRUCT.fGridWidth						= 0.0
		g_FMMC_STRUCT.fGridLength						= 0.0
		g_FMMC_STRUCT.bStuntGrid						= FALSE

		//For the players starting vehicle
		FOR i = 0 TO (FMMC_MAX_NUM_RACERS- 1)
			g_FMMC_STRUCT.mnVehicleModel[i] 			= DUMMY_MODEL_FOR_SCRIPT
		ENDFOR
		
		FOR i = 0 TO (FMMC_MAX_TEAMS- 1)
			g_FMMC_STRUCT.iVehicleColour[i] 			= -1
			g_FMMC_STRUCT.iVehicleNeonColour[i]			= -1
			g_FMMC_STRUCT.iVehicleSecondaryColour[i] 	= -1
			g_FMMC_STRUCT.iVehicleBodyHealth[i]			= 1001
		ENDFOR
	ENDIF 
	
	g_FMMC_STRUCT.iHeistOutfit = -1
	
	FOR i = 0 TO (FMMC_MAX_TEAMS -1)
		g_FMMC_STRUCT.iNumberOfPlayerRules[i] = 0
		g_FMMC_STRUCT.iOutfitDefault[i] = -1
		g_FMMC_STRUCT.iStyleDefault[i] = -1
		
		FOR i2 = 0 TO (FMMC_MAX_RULES - 1)
			g_FMMC_STRUCT.sPlayerRuleData[i2].iRule[i] 				= FMMC_OBJECTIVE_LOGIC_NONE
			g_FMMC_STRUCT.sPlayerRuleData[i2].iPriority[i] 			= FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT.sPlayerRuleData[i2].iPlayerRuleLimit[i] 	= 0 
			g_FMMC_STRUCT.sPlayerRuleData[i2].iJumpToObjectivePass[i] = 0
			g_FMMC_STRUCT.sPlayerRuleData[i2].iJumpToObjectiveFail[i] = 0
			g_FMMC_STRUCT.sPlayerRuleData[i2].iBitset[i] = 0
		ENDFOR
		g_FMMC_STRUCT.iTeamRespawnMinDist[i] = 0
		g_FMMC_STRUCT.fEnvEffScale[i] = 1.0
		g_FMMC_STRUCT.iVehicleIndexForColImmunityTeam[i] = -1
		g_FMMC_STRUCT.iTeamVehicleModPreset[i] = -1
	ENDFOR
	//For the police
	IF bReSetMenuItems
		g_FMMC_STRUCT.iPolice							= 1
	ENDIF
	
	g_FMMC_STRUCT.iCCTVDelay					= 5000
	g_FMMC_STRUCT.iCCTVDestroyCountToCauseAggro	= -1
	
	//For the placed objects
	g_FMMC_STRUCT_ENTITIES.iNumberOfObjects					= 0
	g_FMMC_STRUCT_ENTITIES.iObjectPallet						= 0
	g_FMMC_STRUCT_ENTITIES.iObjectRespawnTime				= 0
	
	CLEAR_ELECTRONIC_DATA(g_FMMC_STRUCT_ENTITIES.sEditedObject.sObjElectronicData)
	CLEAR_TRAIN_ATTACH_STRUCT(g_FMMC_STRUCT_ENTITIES.sEditedDynoProp.sDynopropTrainAttachmentData)
	
	FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS - 1)
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos						= <<0.0,0.0,0.0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vRot						= <<0.0,0.0,0.0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fHead					= 0.0
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fRespawnRange			= 50.0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fRespawnMinRange			= 0.0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType			= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID		= -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vSecondSpawnPos			= <<0, 0, 0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fSecondHeading			= 0
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn						= DUMMY_MODEL_FOR_SCRIPT
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectRespawnLives		= 0
		CLEAR_ENTITY_BLIP_DATA_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjBlipStruct)
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjBlipStruct.fBlipScale 				= FMMC_BLIP_SCALE_OBJECT
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame			= ciFMMC_OBJECT_MINIGAME_TYPE__NONE
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLegacyMiniGameBitSet	= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet			= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetTwo			= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetThree		= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetFour		= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetFive		= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCrateContents			= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCrateValue				= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAmmoValue				= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCratePowerUpType		= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCratePowerUpTimer		= 60
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCratePowerUpRest		= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectHealth			= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectHealthBarInRange	= 0 
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectHealthBarOnRule	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iHealthbarTurnRedAtPercent = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectColour			= 1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjMCVarSelectionIndex_One = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjMCVarSelectionIndex_Two = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iInteriorDestructionIndex = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iBoundsCheckSpecifiedTeam = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iHealthDamageRequiredForRule = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iInvincibleOnRules = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iInvincibleOnRulesTeam = -1
				
		CLEAR_CLEANUP_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sCleanupData)
		
		CLEAR_ENTITY_PTFX_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntitySpawnPTFX)
		CLEAR_ENTITY_PTFX_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntityDespawnPTFX)
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjPointsToGive           =  0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjGivePointsOnRuleStart  = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjGivePointsOnRuleEnd    = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPhotoRange = ciFMMC_PHOTO_RANGE
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCaptureRange = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecuroHackCustomString = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAggroIndexBS_Entity_Obj = ciMISSION_AGGRO_INDEX_DEFAULT
		
		FOR ii = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].eSpawnConditionFlag[ii] = SPAWN_CONDITION_FLAG_NOT_SET
		ENDFOR
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectHackedDialogueTrigger		= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectInteractionAnim				= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectInteractionAnim_AltAnimSet 	= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectKeypadAccessRequired		= INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__NONE
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectInteractionPed	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectInteractionVeh	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectInteractionObj	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].tlObjectInteractionPromptText	= ""
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iVehiclePickup			= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRespawnOnRuleChangeBS	= 0
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRespawnOnRule, -1)
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective 	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam  		= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedSpawn 		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedAction		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedScoreRequired  = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedRuleForceAlwaysSpawnOnScore = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedRuleAlwaysSpawnOnScoreEnd   = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedObjective 	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedTeam  		= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedSpawn 		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedAction		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedScoreRequired  = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedRuleAlwaysForceSpawnOnScore = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedRuleAlwaysSpawnOnScoreEnd   = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedObjective 	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedTeam  		= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedSpawn 		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedAction		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedScoreRequired  = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedRuleAlwaysForceSpawnOnScore = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedRuleAlwaysSpawnOnScoreEnd   = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedObjective 	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedTeam  		= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedSpawn 		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedAction		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedScoreRequired  = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedRuleAlwaysForceSpawnOnScore = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedRuleAlwaysSpawnOnScoreEnd   = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRespawnDelay				= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnGroup					= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnSubGroup				= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iModifySpawnGroupOnEvent= 0 		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnGroupAdditionalFunctionalityBS = 0 	
		FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnSubGroupBS[ii] = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iModifySpawnSubGroupOnEventBS[ii] = 0
		ENDFOR
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCustomObjName = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCustomObjHackTicker_Local = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCustomObjHackTicker_Remote = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCleanupObjective		= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCleanupTeam				= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCleanupRange			= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iInterior				= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRoomKey					= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vInteriorCoords			= <<0,0,0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iInteriorTypeHash		= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAttachParent			= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAttachParentType		= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iExplodeTime				= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iJuggernautHealth		= 1000
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iTagColour				= 1
		
		CLEAR_DRONE_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjDroneData)
		CLEAR_ENTITY_AUDIO_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntAudioData)
		CLEAR_ELECTRONIC_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjElectronicData)
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectivePrereq	= ciPREREQ_None
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fSeaMineExplodeRadius	= 5.0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fDestroyExplosionRadius	= 0.0
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fActivationRangeOverride	= 0.0
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iOrbitalCannonIndex = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjLodDist = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vAttachOffset = <<0,0,0>>
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].tl63_ModelNameToSwapTo = ""
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iTransformVehicleIndex = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjHealthOverride = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjBelongsToTeam = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjLinkedZone = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iContinuityId = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iContentContinuityType = 0
				
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iClothesChangeOutfit = 0
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCashAmount = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPaintingIndex = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor1 = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor2 = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iNumberOfObstructions 				= 4
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iNumberOfObstructions_Hard 			= 4
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fDifficultyOfObstructions			= 0.5
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fDifficultyOfObstructions_Hard		= 0.5
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fDifficultyOfObstructionsLaser		= 0.5
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fDifficultyOfObstructionsLaser_Hard	= 0.5
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawn = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawnPlayerReq = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPatternsRequired = 4
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPreReqRequiredForMinigameHelpText = -1
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPreReqsRequiredToUnblock, 0)
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPreReqRequiredForMinigame, 0)
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPreReqToCompleteAfterMinigame = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPreReqRequiredForMinigameCount = 1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectToPickUpAfterMinigame = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPlacedMarkerIndex = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vObjDropOffOverride = <<0,0,0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fObjDropOffRadius = 0.0
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fObjDropOffVisualRadius = 0.0
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fObjectStrictPickupDistance = 0.0
		
		CLEAR_SELF_DESTRUCT_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sSelfDestructSettings)
		
		CLEAR_TRAIN_ATTACH_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectTrainAttachmentData)
		
		CLEAR_ENTITY_RENDER_TARGET_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjectRenderTargetOptions)
		
		CLEAR_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings)
		
		CLEAR_ENTITY_END_MISSION_DATA_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEndMissionData)
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRadioStation = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCleanupDelay = 0
				
		FOR i2 = 0 TO (FMMC_MAX_TEAMS -1)
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[i2]		= FMMC_OBJECTIVE_LOGIC_NONE
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[i2]	= FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iJumpToObjectivePass[i2] = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iJumpToObjectiveFail[i2] = 0
		ENDFOR
		
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAltVarsBitsetObj, 0)
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSetPrereqsOnCollectionBS, 0)
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSetPrereqsOnCollectionNoResetBS, 0)		
		
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntityAttachmentOptions.iAttachmentConsequence = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntityAttachmentOptions.iTriggerAtTime = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntityAttachmentOptions.iTriggerAtRule = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntityAttachmentOptions.iTriggerAtHealthPercent = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntityAttachmentOptions.iBitset = 0
		
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_PLANNING_MISSIONS -1
		g_FMMC_STRUCT.sPlanningMissions[i].tlID = ""
		g_FMMC_STRUCT.sPlanningMissions[i].iLevel = 0
		g_FMMC_STRUCT.sPlanningMissions[i].iVar = -1
	ENDFOR
	
	//For Placing weapons
	g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons					= 0
	g_FMMC_STRUCT_ENTITIES.iLegacyPlacementType					= 0
	IF bReSetMenuItems
		IF IS_THIS_A_MISSION() AND NOT IS_THIS_A_LTS()
			g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime				= -1
		ELSE
			g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime				= 0
		ENDIF
	ENDIF
	g_FMMC_STRUCT_ENTITIES.iWeaponPallet						= 0
	FOR i = 0 TO (FMMC_MAX_WEAPONS - 1)
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos					= <<0.0,0.0,0.0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vRot					= <<0.0,0.0,0.0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt					= NUM_PICKUPS
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSubType				= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iClips				= 0
		
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType 		= ciCUSTOM_PICKUP_TYPE__NONE
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel		= 0
		
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType 	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].fDamageMultiplier 	= 1.0
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedObjective 	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedTeam  		= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedSpawn 		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSecondAssociatedObjective 	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSecondAssociatedTeam  		= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSecondAssociatedSpawn 		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iThirdAssociatedObjective 	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iThirdAssociatedTeam  		= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iThirdAssociatedSpawn 		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iFourthAssociatedObjective 	= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iFourthAssociatedTeam  		= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iFourthAssociatedSpawn 		= ciOBJECTIVE_SPAWN_LIMIT_OFF
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCleanupObjective  			= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCleanupTeam       			= -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCleanupRange 				= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPropDetonateIndex 			= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iWepAmmoType				 	= 0
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehBombLengthIncrease		 = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehBombMaxIncrease			 = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSpawnGroup					 = 0
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSpawnSubGroup				 = 0
		FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSpawnSubGroupBS[ii] = 0
		ENDFOR
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iContinuityId				 = -1
		
		FOR i2 = 0 TO FMMC_MAX_TEAMS-1
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].bRest[i2]		= FALSE
		ENDFOR
		
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iWepRespawnCount = -1
		
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnWithEntity_Type = ciENTITY_TYPE_NONE
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnWithEntity_Index = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPickupSpawnWithEntity_LocalOffset = <<0,0,0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPickupSpawnWithEntity_LocalRotation = <<0,0,0>>
		
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAggroIndexBS_Entity_Weapon = ciMISSION_AGGRO_INDEX_DEFAULT
		
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupPrerequisiteRequired = ciPREREQ_None
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupPrerequisiteToCompleteOnPickup = ciPREREQ_None
		
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iHelpTextOnPickup = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedMarkerIndex = -1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iUncollectableUntilRule = 0
		
		CLEAR_ENTITY_BLIP_DATA_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].sWeaponBlipStruct)
		INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAltVarsBitsetWeapon, 0)
		
		FOR ii = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].eSpawnConditionFlag[ii]	= SPAWN_CONDITION_FLAG_NOT_SET
		ENDFOR
	ENDFOR
	CLEAR_DM_CUSTOM_DATA()
	FMMC_REMOVE_ALL_ZONES()
	
	#IF IS_DEBUG_BUILD
	g_FMMC_STRUCT_ENTITIES.tl63_DevNotesLoadFromContentID = ""
	g_FMMC_STRUCT_ENTITIES.tl63_LoadFromContentID = ""
	g_FMMC_STRUCT_ENTITIES.tl31_DevAssistTag = ""
	FMMC_CLEAR_ALL_DEV_ASSIST_DATA()
	#ENDIF
	
	g_FMMC_STRUCT.sMOCStruct.iBay1 = 0
	g_FMMC_STRUCT.sMOCStruct.iBay2 = 0
	g_FMMC_STRUCT.sMOCStruct.iBay3 = 0
	g_FMMC_STRUCT.sMOCStruct.iColourCombo = 0
	g_FMMC_STRUCT.sMOCStruct.iMOCBitSet = 0
	
	FMMC_REMOVE_ALL_OVERTIME_ZONES()
	
	g_FMMC_STRUCT.iOvertimeRounds = -1
	g_FMMC_STRUCT.iOvertimeRoundPointsClear = 0
	g_FMMC_STRUCT.iOvertimeRoundScale = 0
	g_FMMC_STRUCT.iOvertimeSDRounds = -1
	
	g_FMMC_STRUCT.iVehicleRespawnPool = -1
		
	g_FMMC_STRUCT.iShowMarkersOnCam = -1
	
	REPEAT FMMC_MAX_PRIORITY_PEDS i
		g_FMMC_STRUCT.iHighPriorityPed[i] = -1
	ENDREPEAT
	
	g_FMMC_STRUCT.iTripSkipHelpDialogue = -1
	g_FMMC_STRUCT.iManualRespawnDelay = DEFAULT_MANUAL_RESPAWN_DELAY
	
	g_FMMC_STRUCT.iSpeedRaceCheckpointTimer = -1
	
	g_FMMC_STRUCT.iTradingPlacesTimeBarDuration = -1
	g_FMMC_STRUCT.iTradingPlacesTimeBarAddOnKill = 0
	
	g_FMMC_STRUCT.fEasyExplosionTime = 2.5
	g_FMMC_STRUCT.fEasyExplosionVelocity = 8.0
	g_FMMC_STRUCT.fEasyExplosionTimeBike = 2.5
	g_FMMC_STRUCT.fEasyExplosionVelocityBike = 8.0
	
	//For Placing Vehicels
	g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles					= 0
	g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates			= 0
	IF bReSetMenuItems
		g_FMMC_STRUCT.iVehicleModel						= -1
		g_FMMC_STRUCT.iRaceClass						= -1
	ENDIF
	g_FMMC_STRUCT_ENTITIES.iVehicleRespawnTime				= 0
	g_FMMC_STRUCT_ENTITIES.iVehiclePallet					= 0
	g_FMMC_STRUCT_ENTITIES.iVehicleTireBitSet				= 0
	
	FOR i = 0 TO (FMMC_MAX_VEHICLES - 1)
		CLEAR_VEHICLE_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i])
	ENDFOR
	CLEAR_VEHICLE_STRUCT(g_FMMC_STRUCT_ENTITIES.sEditedVehicle)
	
	FOR i = 0 TO (ciMAX_PLACED_VEHICLE_CUSTOM_MOD_SETTINGS - 1)
		CLEAR_VEHICLE_CUSTOM_MOD_SETTING(i)		
	ENDFOR
	
	g_FMMC_STRUCT.iNumberOfSj = 0
	
	FOR i = 0 TO (FMMC_MAX_NUM_STUNTJUMPS - 1)
		g_FMMC_STRUCT.sStuntJumps[i].vStartPos		= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.sStuntJumps[i].vEndPos		= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.sStuntJumps[i].vCameraPos		= <<0.0, 0.0, 0.0>>
	ENDFOR
	
	//For Placing Spawns
	g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints						= 0
	FOR i = 0 TO (FMMC_MAX_SPAWNPOINTS - 1)
		g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos				= <<0.0,0.0,0.0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].fHead			= 0.0
		g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].iTeam			= 0
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_QRC_SPAWN_POINTS - 1)		
		g_FMMC_STRUCT.sQRCSpawnPoint[i].vSpawnPos = <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.sQRCSpawnPoint[i].fSpawnHeading = 0.0
		g_FMMC_STRUCT.sQRCSpawnPoint[i].iSpawnPointBS = 0	
		g_FMMC_STRUCT.sQRCSpawnPoint[i].iActiveForTeam = 0
		g_FMMC_STRUCT.sQRCSpawnPoint[i].iActiveOnCheckpointBS = 0
		g_FMMC_STRUCT.sQRCSpawnPoint[i].iActiveWithSpawnGroupBS = 0
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
		g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i]			= 0
		FOR i2 = 0 TO FMMC_MAX_TEAMSPAWNPOINTS - 1
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].vPos				= <<0,0,0>>
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].fHead				= 0.0			
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].vPosOverride		= <<0,0,0>>
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].fHeadOverride		= ciSPAWN_POINT_HEADING_OVERRIDE_NOT_SET
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iTeam		= 0
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iSpawnBitSet = 0
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iVehicle 	= -1
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iSeat	 	= -3
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].viVehicle 	= NULL
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iTreatAsRuleRespawn 	= -1
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iValidForRules_Start 	= -1
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iValidForRules_End 	= -1
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iAreaType 				= 0	
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iAreaScale 			= 0
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iSpawnPointQuantity	= 0
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iGetGroundFromHeight	= 0
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iActivationRadius	= 0
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iActivationTeam	= -2
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iTeamRestrictedBS	= 0
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iLinkedContinuityEntityType	= -1
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iLinkedContinuityID	= -1
			
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iMoveToEntityType = ciENTITY_TYPE_NONE
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iMoveToEntityIndex = -1
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].fRoadNodeExclusionRadius = 0.0
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].fRoadNodeExclusionHeightRange = 0.0			
			
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].vSpawnPositionSeparateVehicle = <<0.0, 0.0, 0.0>>
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].fSpawnHeadingSeparateVehicle = 0.0
			
	
			FOR ii = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
				g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].eSpawnConditionFlag[ii]	= SPAWN_CONDITION_FLAG_NOT_SET
			ENDFOR
			INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iAltVarsBitsetSpawnPoints, 0)
			INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iLinkedActivationSpawnPoints, 0)
			
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iSpawnGroup	= 0
			INITIALISE_INT_ARRAY(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].iSpawnSubGroupBS, 0)	
		ENDFOR
	ENDFOR
	
	//For Placing PED
	g_FMMC_STRUCT_ENTITIES.iNumberOfPeds							= 0
	g_FMMC_STRUCT_ENTITIES.iPedRespawnTime							= 0
	g_FMMC_STRUCT_ENTITIES.iPedPallet								= 0
	g_FMMC_STRUCT_ENTITIES.iPedSpawnCap								= 50
	
	FOR i = 0 TO ASSOCIATED_GOTO_TASK_POOL-1
		CLEAR_ALL_PED_ASSOCIATED_GOTO_DATA(g_FMMC_STRUCT.sAssociatedGotoPool[i].sAssociatedGotoTaskData)
	ENDFOR	
	
	FOR i = 0 TO (FMMC_MAX_PEDS - 1)
		CLEAR_PED_STRUCT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i])
	ENDFOR
	
	g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints = 0
	FOR i = 0 TO (FMMC_MAX_SPAWNPOINTS -1)
		g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos 	= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].fHead	= 0.0
		g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].iTeam	= 0
	ENDFOR
	
	//Go to locations
	FOR i2 = 0 TO (FMMC_MAX_TEAMS -1)
		g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[i2] = 0
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS - 1) 
		CLEAR_GO_TO_LOCATION_DATA(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i])
	ENDFOR
	CLEAR_GO_TO_LOCATION_DATA(g_FMMC_STRUCT_ENTITIES.sEditedGotoLocationData)	
	
	g_FMMC_STRUCT.bKillFMMC						= FALSE
	g_FMMC_STRUCT.iTestMyTeam 					= 0
	
	g_FMMC_STRUCT.iTODOverrideHours				= 12
	g_FMMC_STRUCT.iTODOverrideMinutes			= 00
	
	//Warp to Apartment
	FOR i = 0 TO (FMMC_MAX_TEAMS -1)
		g_FMMC_STRUCT.iBuildingWarpFirstRule[i] = -1
		g_FMMC_STRUCT.iBuildingWarpLastRule[i] = -1
	ENDFOR
	
	g_FMMC_STRUCT.iCopTeam 						= 0
	g_FMMC_STRUCT.iDefaultSpectatorTeam 		= -1
	g_FMMC_STRUCT.iBuildingWarpBS 				= 0
	g_FMMC_STRUCT.iBuildingToWarpTo				= 0
	g_FMMC_STRUCT.iInteriorBS 					= 0
	g_FMMC_STRUCT.iInteriorBS2 					= 0
	g_FMMC_STRUCT.iInteriorBS3 					= 0
	g_FMMC_STRUCT.iIPLOptions 					= 0
	g_FMMC_STRUCT.iMissionBasement				= -1
	
	g_FMMC_STRUCT.iWepRestBS 					= 0
	g_FMMC_STRUCT.sCasinoApartment.iCasinoApartColour 			= 1
	g_FMMC_STRUCT.sCasinoApartment.iCasinoApartPattern			= 0
	g_FMMC_STRUCT.sCasinoApartment.iCasinoApartArcade			= 0
	g_FMMC_STRUCT.sCasinoApartment.iCasinoApartParty			= -1
	g_FMMC_STRUCT.sCasinoApartment.iCasinoPartyRadio			= -1
	g_FMMC_STRUCT.sCasinoApartment.iCasinoApartBitSet			= 0
	
	FOR i = 0 TO ciMETH_LAB_INSTANCE_MAX-1	
		g_FMMC_STRUCT.iMethLabInstanceWallTints[i] = -1
	ENDFOR
	
	g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType 	= FMMC_PED_AMMO_DROP_TYPE_INVALID
	g_FMMC_STRUCT.sPedAmmoDrop.iChanceToDrop 	= 25
	g_FMMC_STRUCT.sPedAmmoDrop.iAmount 			= 20
	g_FMMC_STRUCT.sPedAmmoDrop.iLifeTime 		= 60
	
	CLEAR_IPL_STRUCT()	
	
	g_FMMC_STRUCT.sArenaInfo.iArena_Theme					= ARENA_THEME_DYSTOPIAN
	g_FMMC_STRUCT.sArenaInfo.iArena_Variation				= 0
	g_FMMC_STRUCT.sArenaInfo.iArena_Lighting				= ARENA_LIGHTING_MIDDAY
	g_FMMC_STRUCT.sArenaInfo.iArena_BandColour[0] = 0
	g_FMMC_STRUCT.sArenaInfo.iArena_BandColour[1] = 0
	g_FMMC_STRUCT.sArenaInfo.iArena_BandColour[2] = 0
	g_FMMC_STRUCT.sArenaInfo.iArena_BandColour[3] = 0
	
	g_FMMC_STRUCT.sArenaInfo.iArena_CrowdColour[0] = 0
	g_FMMC_STRUCT.sArenaInfo.iArena_CrowdColour[1] = 0
	g_FMMC_STRUCT.sArenaInfo.iArena_CrowdColour[2] = 0
	g_FMMC_STRUCT.sArenaInfo.iArena_CrowdColour[3] = 0
	
	g_FMMC_STRUCT.sArenaInfo.iArena_RandomLightingBS = 0
	
	FOR i = 0 TO (ARENA_BITSETS -1)
		g_FMMC_STRUCT.sArenaInfo.iArena_Floor1BS[i] 				= 0
		g_FMMC_STRUCT.sArenaInfo.iArena_Floor2BS[i] 				= 0
		g_FMMC_STRUCT.sArenaInfo.iArena_Floor3BS[i] 				= 0
		g_FMMC_STRUCT.sArenaInfo.iArena_Floor4BS[i] 				= 0
		
		g_FMMC_STRUCT.sArenaInfo.iArena_MiscBS[i] 					= 0
	ENDFOR
	
	g_FMMC_STRUCT.iArenaVeh_VehicleMines = 5
	
	FOR i = 0 TO (FMMC_MAX_TEAMS -1)
		g_FMMC_STRUCT.iDefaultMaskGroup[i]	= 0
		g_FMMC_STRUCT.iTeamVehicleModCountermeasure_AmmoPerLife[i] = 0
		g_FMMC_STRUCT.iTeamVehicleModCountermeasure_Cooldown[i] = 0
		g_FMMC_STRUCT.fManualRespawnTimeLose[i] = 0.0
	ENDFOR
	
	//New Survival Data
	RESET_SURVIVAL_DATA()
	
	g_FMMC_STRUCT.iDLCRelease = -1
	
	//Clear Extra Objective Entities
	INT iEntity, iExtraObjective, iTeam
	FOR iEntity = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_ENTITIES -1)
	
		g_FMMC_STRUCT.iExtraObjectiveEntityID[iEntity] = -1
		
		g_FMMC_STRUCT.iExtraObjectiveEntityType[iEntity] = -1
		
		FOR iExtraObjective = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)			
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				g_FMMC_STRUCT.iExtraObjectiveRule[iEntity][iExtraObjective][iTeam] = -1
				g_FMMC_STRUCT.iExtraObjectivePriority[iEntity][iExtraObjective][iTeam] = -1
			ENDFOR		
		ENDFOR	
		
	ENDFOR
	
	g_FMMC_STRUCT.iRandomVehWepBS = 0
	
	g_FMMC_STRUCT.vTWCamStaticPosition				= <<0.0, 0.0, 0.0>>
	g_FMMC_STRUCT.vTWCamStaticRotation				= <<0.0, 0.0, 0.0>>
	
	g_FMMC_STRUCT.iPropColourChangeDelay = 0
	g_FMMC_STRUCT.iObjectStolenCooldownTimer = 0
	g_FMMC_STRUCT.iSpawnProtectionDuration = 20
	
	g_FMMC_STRUCT.iCooldDownSmash = 500
	g_FMMC_STRUCT.fMaxLateralForceSmash = 10.0
	g_FMMC_STRUCT.fMaxVerticalForceSmash = 0.5
	g_FMMC_STRUCT.fMinLateralForceSmash = 1.0
	g_FMMC_STRUCT.fMinVerticalForceSmash = 0
	g_FMMC_STRUCT.fMaxPushSmash = 25.0
	g_FMMC_STRUCT.fMinPushSmash = 15.0
	g_FMMC_STRUCT.fMaxPushVSmash = 6.0
	g_FMMC_STRUCT.fMinPushVSmash = 4.0
	g_FMMC_STRUCT.iMaxDamageSmash = 10
	g_FMMC_STRUCT.iMinDamageSmash = 1
	g_FMMC_STRUCT.fMinSpeedForceForward = 0
	CLEAR_SHRINKING_SUDDEN_DEATH_AREA()
	
	CLEAR_CARNAGE_BAR_SETTINGS(g_FMMC_STRUCT.sCarnageBar)
	
	CLEAR_MISSION_VARIATION_STRUCT(g_FMMC_STRUCT.sMissionVariation)
	
	g_FMMC_STRUCT.fStuntBoost = 0.5
	
	g_FMMC_STRUCT.iPointsStolenOnBikeMelee = 0
	
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		g_FMMC_STRUCT.iTagExplosionTimer[i] = DM_TAG_EXPLOSION_TIMER_30
	ENDFOR
	
	g_FMMC_STRUCT.iTagReturnCooldown = 0
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS_AND_JIP_SPEC-1
		g_FMMC_STRUCT.vSpawnForRoamingSpecOrigin[iTeam] = <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.fSpawnForRoamingSpecHead[iTeam] = 0.0
		g_FMMC_STRUCT.vBoundsForRoamingSpecCentre[iTeam] = <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT.fBoundsForRoamingSpecRadius[iTeam] = 0.0
	ENDFOR
	
	g_FMMC_STRUCT.fCashGrabSlowCap[ciCASH_GRAB_SLOW_VALUES_LIGHT] = 1.25
	g_FMMC_STRUCT.fCashGrabSlowCap[ciCASH_GRAB_SLOW_VALUES_MEDIUM] = 1.15
	g_FMMC_STRUCT.fCashGrabSlowCap[ciCASH_GRAB_SLOW_VALUES_HEAVY] = 1.00
	g_FMMC_STRUCT.iCashGrabSlowTrigger = 10
	g_FMMC_STRUCT.iCashGrabMinimumHitLoss = 6000
	g_FMMC_STRUCT.iCashGrabMaximumHitLoss = 10000
	g_FMMC_STRUCT.iMaximumBagCapacity = 0
	g_FMMC_STRUCT.iBagCapacityCustomLabel = -1
	g_FMMC_STRUCT.iBagFullCustomLabel = -1
	
	g_FMMC_STRUCT.fRadarZoomAmount = 0
	
	g_FMMC_STRUCT.iMinimumLaps = 0
	g_FMMC_STRUCT.iMaximumLaps = 0
	g_FMMC_STRUCT.fAntiGriefHealthDrain = 0.0
	g_FMMC_STRUCT.iAntiGriefDrainTimer = 0
	
	FOR i = 0 TO ciBonus_MAX-1
		g_FMMC_STRUCT.iBonusTimeReward[i] = 0
	ENDFOR
	
	g_FMMC_STRUCT.iCheckpointExplMax = 12
	g_FMMC_STRUCT.iCheckpointExplMin = 3
	
	
//	FOR i = 0 TO MAX_MOCAP_CUTSCENES-1
//		g_fmmc_Struct.tlMoCapCutsceneData[i] = ""
//	ENDFOR
	
	g_FMMC_STRUCT.iTimeReductionMonsterJam = 0
	g_FMMC_STRUCT.iTagTeamCooldownTime = 30
	
	g_FMMC_STRUCT.fVehicleWeaponFireDrainTime = 2.5
	g_FMMC_STRUCT.iVehicleWeaponMGFireRate = 5
	g_FMMC_STRUCT.iVehicleWeaponMGDamage = 350
	
	KING_OF_THE_HILL_DATA_STRUCT sTempKOTHData
	g_FMMC_STRUCT.sKotHData = sTempKOTHData
	
	INT iYachtIndex
	FOR iYachtIndex = 0 TO ciYACHT_MAX_YACHTS - 1
		CLEAR_YACHT_DATA(g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex])
	ENDFOR	
	
	g_FMMC_STRUCT.sPhoneEMPSettings.iPhoneEMP_Duration = 60000
	g_FMMC_STRUCT.sPhoneEMPSettings.iPhoneEMP_PerceptionPercentage = 35
	
	g_FMMC_STRUCT.iContinuityBitset = 0
	g_FMMC_STRUCT.eContinuitySpecialPedType = FMMC_MISSION_CONTINUITY_PED_TRACKING_TYPE_NONE
	g_FMMC_STRUCT.eContinuityContentSpecificType = FMMC_MISSION_CONTINUITY_CONTENT_SPECIFIC_TYPE_NONE
	g_FMMC_STRUCT.iContinuitySpawnGroupBitset = 0
	FOR i = 0 TO FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS - 1
		g_FMMC_STRUCT.iContinuitySpawnGroupCondition[i] = 0
		g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[i] = -1
	ENDFOR
	
	g_FMMC_STRUCT.ePVSpawnProperty = PVSP_NONE 
	
	g_FMMC_STRUCT.iRaceLobbyLapIncrement = 1
	
	CLEAR_PLAYER_ABILITIES_STRUCT(g_FMMC_STRUCT.sPlayerAbilities)
	
	FOR i = 0 TO FMMC_MAX_GANG_CHASE_UNIT_CONFIGS -1
		CLEAR_GANG_CHASE_UNIT_CONFIG_STRUCT(g_FMMC_STRUCT.sGangChaseUnitConfigs[i])
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_CUSTOM_HUDS -1
		CLEAR_FMMC_CUSTOM_HUD_STRUCT(g_FMMC_STRUCT.sCustomHUDs[i])
	ENDFOR
	
	g_FMMC_STRUCT.fGangChaseUnitMinObjectiveDistance = 300.0
	g_FMMC_STRUCT.fGangChaseAirSpawnMinZOffset = 20.0
	g_FMMC_STRUCT.fGangChaseUnitSpawnPointMaxOffset = 50.0
	g_FMMC_STRUCT.fGangChaseSeaVisibleDistance = 200.0
	g_FMMC_STRUCT.fGangChaseUnitSpawnDistForwardExtra = 30.0
	g_FMMC_STRUCT.fGangChaseUnitSpawnDistSea = 200.0
	g_FMMC_STRUCT.fGangChaseUnitSpawnDistAir = 340.0
	g_FMMC_STRUCT.fGangChaseUnitSpawnDistOnFoot = 85.0
	g_FMMC_STRUCT.fGangChaseUnitSpawnDistLand = 190.0
	g_FMMC_STRUCT.fGangChaseUnitMaxDistance = 100.0
	g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsLand = 15.0
	g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsAir = 25.0
	g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsSea = 50.0
	g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsOnFoot = 5.0
	g_FMMC_STRUCT.iGangChaseUnitFleeDistance = 200
	g_FMMC_STRUCT.iGangChaseUnitExtraFleeDistance = 50
	g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistLand = 300.0
	g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistAir = 750.0
	g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistSea = 750.0
	g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistOnFoot = 200.0
	g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistLand = 250.0
	g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistAir = 500.0
	g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistSea = 500.0
	g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistOnFoot = 150.0
	
	g_FMMC_STRUCT.iLDB_ShowEndTimeForTeam = 0
	
	INITIALISE_INT_ARRAY(g_FMMC_STRUCT.iAudioScene, -1)
	INITIALISE_INT_ARRAY(g_FMMC_STRUCT.iAudioSceneTeam, -1)
	INITIALISE_INT_ARRAY(g_FMMC_STRUCT.iAudioSceneStartRule, -1)
	INITIALISE_INT_ARRAY(g_FMMC_STRUCT.iAudioSceneEndRule, -1)
	
	INITIALISE_INT_ARRAY(g_FMMC_STRUCT.iRuleTimeToShowOnLeaderboard, -1)
	INITIALISE_INT_ARRAY(g_FMMC_STRUCT.iRuleTimeToShowOnLeaderboardTeam, -1)
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		FOR i = 0 TO MAX_PLAYERS_PER_TEAM - 1
			g_FMMC_STRUCT.iCachedVehicle[iTeam][i] = -1
			g_FMMC_STRUCT.iRiderFlameEffect[iTeam][i] = 0
		ENDFOR
	ENDFOR
ENDPROC











