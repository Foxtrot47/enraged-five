USING "rage_builtins.sch"
USING "globals.sch"
//Game Headers
USING "commands_network.sch"
//USING "script_player.sch"
//Network Headers
//USING "net_events.sch"
//USING "net_hud_displays.sch"

//USING "screens_header.sch"
USING "FMMC_header.sch"
USING "FMMC_Cloud_Loader.sch"
USING "Freemode_header.sch"
USING "net_common_functions.sch"
//USING "fm_mission_controler.sc"
USING "net_cash_transactions.sch"
USING "net_weapon_pickups.sch"


FUNC BOOL should_pickups_be_disabled()
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN TRUE
	ELIF NETWORK_IS_SCRIPT_ACTIVE(GET_FM_MISSION_CONTROLLER_SCRIPT_NAME(),-1, TRUE )
		RETURN TRUE		
	ELIF IS_ON_DEATHMATCH_GLOBAL_SET()
		RETURN TRUE
	ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_SURVIVAL

		RETURN TRUE
	ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GANGHIDEOUT
		RETURN TRUE
	ELIF g_b_On_Race = TRUE
		RETURN TRUE
	ELIF NETWORK_IS_ACTIVITY_SESSION()
		RETURN TRUE
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())	
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
	
		

ENDFUNC


PROC GIVE_PLAYER_RANDOM_REWARD_FOR_PACKAGE_PICKUP()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		INT random_number = GET_RANDOM_INT_IN_RANGE(0, 6)
		IF random_number = 0
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_GRENADE, 8, TRUE, TRUE )
			NET_NL() NET_PRINT("\n GIVE_PLAYER_RANDOM_REWARD_FOR_PACKAGE_PICKUP: GAVE PLAYER  WEAPONTYPE_GRENADE ")   NET_NL()
		ENDIF
		
		IF random_number = 1
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_MICROSMG, 82, TRUE, TRUE )
			NET_NL() NET_PRINT("\n GIVE_PLAYER_RANDOM_REWARD_FOR_PACKAGE_PICKUP: GAVE PLAYER  WEAPONTYPE_MICROSMG ")   NET_NL()
		ENDIF
		
		IF random_number = 2
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_PUMPSHOTGUN, 25, TRUE, TRUE )
			NET_NL() NET_PRINT("\n GIVE_PLAYER_RANDOM_REWARD_FOR_PACKAGE_PICKUP: GAVE PLAYER  WEAPONTYPE_PUMPSHOTGUN ")   NET_NL()
		ENDIF
		
		IF random_number = 3
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SAWNOFFSHOTGUN, 25, TRUE, TRUE )
			NET_NL() NET_PRINT("\n GIVE_PLAYER_RANDOM_REWARD_FOR_PACKAGE_PICKUP: GAVE PLAYER  WEAPONTYPE_SAWNOFFSHOTGUN ")   NET_NL()
		ENDIF
		
		IF random_number = 4
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_COMBATPISTOL, 48, TRUE, TRUE )
			NET_NL() NET_PRINT("\n GIVE_PLAYER_RANDOM_REWARD_FOR_PACKAGE_PICKUP: GAVE PLAYER  WEAPONTYPE_COMBATPISTOL ")   NET_NL()
		ENDIF
		
		IF random_number = 5
			
			//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_PICKED_FROM_CRATES, 1000)

			IF USE_SERVER_TRANSACTIONS()
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CRATE_DROP, 1000)
			ELSE
				GIVE_LOCAL_PLAYER_FM_CASH(1000)
				NETWORK_EARN_FROM_CRATE_DROP(1000)
			ENDIF
			
			NET_NL() NET_PRINT("\n GIVE_PLAYER_RANDOM_REWARD_FOR_PACKAGE_PICKUP: GAVE PLAYER  1000 cash ")   NET_NL()
				
		ENDIF
		
		
	ENDIF

ENDPROC



PROC setup_hidden_package_pickups_Freemode(FM_HIDDEN_PACKAGE_CREATION_STRUCT &sWepStruct[] )
//	REQUEST_MODEL(PROP_DRUG_PACKAGE)
   
   // parachutes
   	sWepStruct[0].vCamPos  =  <<134.9032, -730.2903, 265.9752>>
	sWepStruct[1].vCamPos  =  <<131.0916, -636.9310, 262.1988>>
	sWepStruct[2].vCamPos  =  <<-63.0990, -813.3268, 320.4967>>
	sWepStruct[3].vCamPos  =  <<-119.1337, -977.2335, 26.2773>>
	sWepStruct[4].vCamPos  =  <<-129.6909, -597.8885, 201.2368>>
	sWepStruct[5].vCamPos  =  <<-212.1477, -735.7436, 219.5222>>
	sWepStruct[6].vCamPos  =  <<-891.3096, -457.4632, 167.1181>>
	sWepStruct[7].vCamPos  =  <<-898.4548, -374.9962, 135.2814>>
	sWepStruct[8].vCamPos  =  <<-778.6216, 319.6546, 229.8537>>
	sWepStruct[9].vCamPos  =  <<746.9189, 1295.8381, 359.8>>
	sWepStruct[10].vCamPos  =  <<370.2055, -1389.3762, 71.7086>>
	sWepStruct[11].vCamPos  =  <<-517.8292, 4423.7729, 89.0954>>
	sWepStruct[12].vCamPos  =  <<-765.5035, 4302.8174, 145.1399>>
	sWepStruct[13].vCamPos  =  <<-1046.8280, 4754.8140, 235.2445>>
	sWepStruct[14].vCamPos  =  <<-1372.5741, 4790.8975, 127.8669>>
	sWepStruct[15].vCamPos  =  <<-1881.1570, 4650.8892, 56.4831>>
	sWepStruct[16].vCamPos  =  <<878.0101, 5818.2466, 582.0976>>
     sWepStruct[17].vCamPos  =<<777.0, 1175.8, 346.0>>      // "E" in the Vinewood sign.
     sWepStruct[18].vCamPos  =<<-113.0, -1042.8, 73.3>>      // Red crane 1
     sWepStruct[19].vCamPos  =<<-204.8, -1115.3, 68.8>>      // Red Crane 2
     sWepStruct[20].vCamPos  =<<-119.7, -976.2, 296.2>>      // Yellow crane
     sWepStruct[21].vCamPos  =<<-441.1, -2306.4, 63.4>>      // On the road of a bridge
     sWepStruct[22].vCamPos  =<<796.8, -2626.4, 87.9>>      // Centre of bridge struts
     sWepStruct[23].vCamPos  =<<889.0, -2906.3, 48.9>>      // Ship Crane
     sWepStruct[24].vCamPos  =<<1662.5, -28.0, 182.8>>      // Dam
     sWepStruct[25].vCamPos  =<<-149.1, -961.0, 269.1>>      // Top of the building site
     sWepStruct[26].vCamPos  =<<-2214.6, 338.7, 199.1>>      // Kortz building
     sWepStruct[27].vCamPos  =<<-2501.9, 3309.8, 92.0>>      // Army Base tower
     sWepStruct[28].vCamPos  =<<-1213.2, 3848.4, 490.4>>      // Mountaintop 1
     sWepStruct[29].vCamPos  =<<-176.7, -735.6, 221.5>>      // Ridge Cliff 1
     sWepStruct[30].vCamPos  =<<-924.4, 4565.2, 224.2>>      // Ridge Cliff 2
     sWepStruct[31].vCamPos  =<<-953.2, 4842.1, 313.5>>      // Mountaintop 2
     sWepStruct[32].vCamPos  =<<134.2, 5224.4, 544.7>>      // Chiliad midpoint
     sWepStruct[33].vCamPos  =<<446.6, 5571.8, 781.2>>      // Chiliad cable car drop-point
     sWepStruct[34].vCamPos  =<<878.4, 5818.0, 582.9>>      // Chiliad cliff drop
     sWepStruct[35].vCamPos  =<<1263.3, 5878.1, 449.7>>      // Chiliad cliff drop 2
     sWepStruct[36].vCamPos  =<<1185.0, 5974.1, 396.3>>      // Chiliad cliff drop 3
     sWepStruct[37].vCamPos  =<<1523.9, 6000.0, 340.9>>      // Chiliad cliff drop 4
     sWepStruct[38].vCamPos  =<<2802.3, 5995.5, 353.4>>      // Mountain Satellite Tower
     sWepStruct[39].vCamPos  =<<906.6, 5157.1, 430.6>>      // Cliff facing farms
     sWepStruct[40].vCamPos  =<<2901.3, 2379.2, 170.6>>      // Small mountain drop
     sWepStruct[41].vCamPos  =<<-318.3, 200.6, 161.2>>      // Hotel roof
     sWepStruct[42].vCamPos  =<<-664.3, 230.5, 157.0>>      // Yellow Crane 2
     sWepStruct[43].vCamPos  =<<-145.2, 560.1, 196.0>>      // Mansion garden
     sWepStruct[44].vCamPos  =<<746.7, 219.6, 146.1>>      // CNT building
     sWepStruct[45].vCamPos  =<<47.2, -460.7, 99.8>>      // Yellow Crane 3
     sWepStruct[46].vCamPos  =<<128.3, -347.8, 102.5>>      // Yellow Crane 4
     sWepStruct[47].vCamPos  =<<-198.2, -570.6, 177.7>>      // Penris Building
     sWepStruct[48].vCamPos  =<<50.8, -404.5, 64.8>>      // Building Site
     sWepStruct[49].vCamPos  =<<148.9, -614.6, 258.1>>      // IAA building
     sWepStruct[50].vCamPos  =<<-827.4, -2162.8, 96.3>>      // Crastenburg Hotel Roof
     sWepStruct[51].vCamPos  =<<-26.0, 150.9, 141.2>>      // Elgin House Roof
     sWepStruct[52].vCamPos  =<<87.3, -337.8, 75.6>>      // Building Site 2
     sWepStruct[53].vCamPos  =<<644.0, 5627.8, 726.2>>      // Chiliad centre cliff
     sWepStruct[54].vCamPos  =<<436.0, 5730.8, 692.0>>      // Chiliad sea cliff
     sWepStruct[55].vCamPos  =<<221.4, 5945.2, 328.2>>      // Chiliad sea cliff 2
     sWepStruct[56].vCamPos  =<<107.6, 5980.4, 236.0>>      // Chiliad sea cliff 3
     sWepStruct[57].vCamPos  =<<-70.2, 4943.1, 390.3>>      // Chiliad path
     sWepStruct[58].vCamPos  =<<-320.6, 4676.5, 251.6>>      // Chiliad path 2

	 sWepStruct[59].vCamPos  =<<-1562.4, -557.83, 114.4374>>      // New ones for TODO 2711223
	 sWepStruct[60].vCamPos  = <<-1354.7935, -484.0505, 83.8469>>
	 sWepStruct[61].vCamPos  = <<-1407.3728, -469.3913, 88.8454>>		// maze1
	 sWepStruct[62].vCamPos  = <<987.46, 80.53, 110.72>>		// Casino
	 sWepStruct[63].vCamPos  = <<966.81, 30.85, 122.52>>			  // Casino
	 sWepStruct[64].vCamPos  = <<-574.0514, -727.2609, 129.8911>>
	 sWepStruct[65].vCamPos  = <<-994.4454, -785.8465, 76.5368>>		
	 sWepStruct[66].vCamPos  = <<-995.5466, -426.5141, 78.2823>>		
	 sWepStruct[67].vCamPos  = <<404.5115, -55.6595, 122.1367>>	
	 sWepStruct[68].vCamPos  = <<-859.0352, -241.6293, 60.5155>>
	INT i
	FOR i = 0 TO (number_of_FM_hidden_package_pickups- 1)
		sWepStruct[i].ptPickUpType    = PICKUP_PARACHUTE
	ENDFOR

ENDPROC




PROC CREATE_ALL_CURRENT_FM_PACKAGE_PICKUPS(FM_HIDDEN_PACKAGE_CREATION_STRUCT &sWepStruct[], INT i)//, BOOL bOnFoot)


	IF NOT should_pickups_be_disabled()
			
		IF DOES_PICKUP_EXIST(sWepStruct[i].Pickups)
			REMOVE_PICKUP(sWepStruct[i].Pickups)
		ENDIF
		IF DOES_BLIP_EXIST(sWepStruct[i].biWeaponBlip)
			REMOVE_BLIP(sWepStruct[i].biWeaponBlip)
		ENDIF
		
		IF NOT DOES_PICKUP_EXIST(sWepStruct[i].Pickups)
			IF NOT IS_VECTOR_ZERO(sWepStruct[i].vCamPos)
				
					INT iPlacementFlags = 0
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
					SET_FM_PICKUP_ORIENTATION_FLAGS(iPlacementFlags)
					sWepStruct[i].Pickups = CREATE_PICKUP_ROTATE(sWepStruct[i].ptPickUpType, sWepStruct[i].vCamPos + <<0,0,0.10>>, <<0,360,0>>, iPlacementFlags, GET_AMMO_AMOUNT_FOR_PICKUP(sWepStruct[i].ptPickUpType), EULER_YZX, FALSE)//, PROP_DRUG_PACKAGE)
					SET_PICKUP_REGENERATION_TIME(sWepStruct[i].Pickups, 120000  ) //WEAPON_RESPAWN_TIME) // regenerates 2 minutes
					sWepStruct[i].Bweapon_has_been_created = TRUE
					NET_NL() NET_PRINT("CREATE_ALL_CURRENT_FM_PACKAGE_PICKUPS: Created hidden package ") NET_PRINT_INT(i)  
					NET_PRINT(", pickup type ") NET_PRINT_INT(ENUM_TO_INT(sWepStruct[i].ptPickUpType))  
					NET_PRINT(", at ") NET_PRINT_VECTOR(sWepStruct[i].vCamPos)
					NET_NL()
				
			ENDIF
		ELSE
		
			#IF IS_DEBUG_BUILD
				NET_NL() NET_PRINT("CREATE_ALL_CURRENT_FM_PACKAGE_PICKUPS: DOES_PICKUP_EXIST(sWepStruct[i].Pickups) FAILED: Pickup already exists ") NET_PRINT_INT(i) NET_PRINT(" At coordinates: ") NET_PRINT_VECTOR(sWepStruct[i].vCamPos) NET_NL()			
			#ENDIF
		ENDIF
			
	ENDIF
	
ENDPROC


PROC REMOVE_ALL_CURRENT_FM_PACKAGE_PICKUPS(FM_HIDDEN_PACKAGE_CREATION_STRUCT &sWepStruct[])//, BOOL bOnFoot)
	INT i
	REPEAT COUNT_OF(sWepStruct) i
		IF DOES_PICKUP_EXIST(sWepStruct[i].Pickups)
			REMOVE_PICKUP(sWepStruct[i].Pickups)
		ENDIF
		IF DOES_BLIP_EXIST(sWepStruct[i].biWeaponBlip)
			REMOVE_BLIP(sWepStruct[i].biWeaponBlip)
		ENDIF	
	ENDREPEAT
ENDPROC

PROC MAINTAIN_PACKAGE_PICKUP_BLIPS(FM_HIDDEN_PACKAGE_CREATION_STRUCT &sWepStruct[], INT &iProcessedLastFrame)//, BOOL bOnFoot)
	
	IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_weapons_and_pickups_have_been_created)
		EXIT
	ENDIF
	
	INT iSizeOfArray = COUNT_OF(sWepStruct)

	INT i
	// NEIL B added to fix release
	IF NOT DOES_BLIP_EXIST(sWepStruct[0].biWeaponBlip)
	ENDIF
		
	
	
	BOOL bshould_pickups_be_disabled = should_pickups_be_disabled()
	
	// only process 5 per frame - added by Neil F. 7/2/13
	INT iForLoopEnd = iProcessedLastFrame + 5
	IF (iForLoopEnd > iSizeOfArray-1)
		iForLoopEnd = iSizeOfArray-1
	ENDIF
	
	////////////////////////////////////////////////
	// HIDE BLIPS if on golf, deathmatch or races
	////////////////////////////////////////////////
	

	
	FOR i = iProcessedLastFrame TO iForLoopEnd
		
		//PRINTLN("MAINTAIN_PACKAGE_PICKUP_BLIPS: i=",i,",iProcessedLastFrame=",iProcessedLastFrame,",iForLoopEnd=",iForLoopEnd)
		
		IF bshould_pickups_be_disabled
			IF sWepStruct[i].Bweapon_has_been_created = TRUE

				IF DOES_PICKUP_EXIST(sWepStruct[i].Pickups)
					REMOVE_PICKUP(sWepStruct[i].Pickups)
					sWepStruct[i].Bweapon_has_been_created = FALSE
						
					NET_NL() NET_PRINT("MAINTAIN_PACKAGE_PICKUP_BLIPS: On mission. Remove pickup  ") NET_PRINT_INT(i)  NET_NL()
					#IF IS_DEBUG_BUILD
						bDebugDisplayWeapons_pickups_packages = FALSE
					#ENDIF	
				ENDIF

			ENDIF
		ELSE
			IF sWepStruct[i].Bweapon_has_been_created = FALSE
				
				INT iPlacementFlags = 0
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP)) //makes it local
				
				SET_FM_PICKUP_ORIENTATION_FLAGS(iPlacementFlags)
				sWepStruct[i].Pickups = CREATE_PICKUP_ROTATE(sWepStruct[i].ptPickUpType, sWepStruct[i].vCamPos + <<0,0,0.10>>, <<0,360,0>>, iPlacementFlags, GET_AMMO_AMOUNT_FOR_PICKUP(sWepStruct[i].ptPickUpType), EULER_YZX, FALSE)//, PROP_DRUG_PACKAGE)
	
				
				SET_PICKUP_REGENERATION_TIME(sWepStruct[i].Pickups, 12000) // regenerates 2 minutes
				sWepStruct[i].Bweapon_has_been_created = TRUE
				
				NET_NL() NET_PRINT("MAINTAIN_PACKAGE_PICKUP_BLIPS: Finished mission. Creating pickup  ") NET_PRINT_INT(i) 
				NET_PRINT(", pickup type ") NET_PRINT_INT(ENUM_TO_INT(sWepStruct[i].ptPickUpType))  
				NET_PRINT(", at ") NET_PRINT_VECTOR(sWepStruct[i].vCamPos)
				NET_NL()
		
			ENDIF
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
			IF bDebugDisplayWeapons_pickups_packages = TRUE
				
				IF NOT DOES_BLIP_EXIST(sWepStruct[i].biWeaponBlip)
					NET_NL() NET_PRINT("\n Adding blip for pickup ") NET_PRINT_INT(i)   NET_NL()
			
					sWepStruct[i].biWeaponBlip = ADD_BLIP_FOR_COORD(sWepStruct[i].vCamPos)
					SET_BLIP_SPRITE(sWepStruct[i].biWeaponBlip , GET_CORRECT_BLIP_SPRITE_FMMC(sWepStruct[i].ptPickUpType))
					SET_BLIP_SCALE(sWepStruct[i].biWeaponBlip , BLIP_SIZE_NETWORK_PICKUP)
					SET_BLIP_DISPLAY(sWepStruct[i].biWeaponBlip, DISPLAY_BOTH) 
					SET_BLIP_AS_SHORT_RANGE(sWepStruct[i].biWeaponBlip, FALSE)
					SHOW_HEIGHT_ON_BLIP(sWepStruct[i].biWeaponBlip, TRUE)
				
				ENDIF

			ELIF bDebugDisplayWeapons_pickups_packages = FALSE
				
				IF DOES_BLIP_EXIST(sWepStruct[i].biWeaponBlip)
					SET_BLIP_AS_SHORT_RANGE(sWepStruct[i].biWeaponBlip, TRUE)
				
					REMOVE_BLIP(sWepStruct[i].biWeaponBlip)
					NET_NL() NET_PRINT("\n Removing blip for pickup ") NET_PRINT_INT(i)   NET_NL()
				ENDIF
				
			ENDIF		
		
		#ENDIF
		
	
	ENDFOR
	
	iProcessedLastFrame = i+1
	IF (iProcessedLastFrame >= iSizeOfArray)
		iProcessedLastFrame = 0
	ENDIF


ENDPROC





