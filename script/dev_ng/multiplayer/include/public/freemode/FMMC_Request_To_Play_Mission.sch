
USING "globals.sch"
USING "rage_builtins.sch"
USING "net_mission_joblist.sch"
USING "FM_In_Corona_Header.sch"
USING "FM_Community_Playlists.sch"

FUNC BOOL CAN_INVITE_PLAYER_IN_SAME_SESSION(GAMER_HANDLE &GamerHandle)
	IF IS_XBOX_PLATFORM()
		IF NOT NETWORK_CAN_PLAY_MULTIPLAYER_WITH_GAMER(GamerHandle)
			RETURN FALSE
		ENDIF
		IF NOT NETWORK_CAN_GAMER_PLAY_MULTIPLAYER_WITH_ME(GamerHandle) 
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_THIS_FREEMODE_MISSION_PLAY_REQUEST_BE_PROCESSED(SCRIPT_EVENT_DATA_FREEMODE_MISSION_PLAY_REQUEST Event)
	IF IS_NET_PLAYER_OK(Event.Details.FromPlayerIndex)
		IF ARE_ALL_INSTANCED_TUTORIALS_COMPLETE()
			IF Event.Details.FromPlayerIndex != PLAYER_ID()
				IF NETWORK_PLAYER_INDEX_IS_CHEATER(Event.Details.FromPlayerIndex)
				AND NOT NETWORK_PLAYER_IS_CHEATER()
					PRINTLN("Ignoring inviter is cheater and local player is not")
					RETURN FALSE
				ENDIF
				IF NOT NETWORK_PLAYER_INDEX_IS_CHEATER(Event.Details.FromPlayerIndex)
				AND NETWORK_PLAYER_IS_CHEATER()
					PRINTLN("Ignoring inviter is NOT cheater but local player is")
					RETURN FALSE
				ENDIF

				GAMER_HANDLE inviter = GET_GAMER_HANDLE_PLAYER(Event.Details.FromPlayerIndex)
				IF NOT NETWORK_CAN_RECEIVE_LOCAL_INVITE(inviter)
					PRINTLN("Ignoring - Cannot receive local invite from ID: ", NATIVE_TO_INT(Event.Details.FromPlayerIndex))
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(Event.Details.FromPlayerIndex)
					INT iPlayerGBD = NATIVE_TO_INT(Event.Details.FromPlayerIndex)
					#IF IS_DEBUG_BUILD
					PRINTLN("Event.iCreatorID = ", Event.iCreatorID)
					PRINTLN("GlobalplayerBD_FM[iPlayerGBD].iCreatorIDOfMIssionIwantToStart 	 = ", GlobalplayerBD_FM[iPlayerGBD].iCreatorIDOfMIssionIwantToStart)
					#ENDIF
					//IF the variation is still the same, i.e. the player hasn't changed missions
					IF IS_PLAYER_ON_A_PLAYLIST_INT(iPlayerGBD)
						PRINTLN("IF IS_PLAYER_ON_A_PLAYLIST_INT(", iPlayerGBD, ")")
						RETURN TRUE
					ELIF GlobalplayerBD_FM[iPlayerGBD].iCreatorIDOfMIssionIwantToStart = Event.iCreatorID
						RETURN TRUE
					ELSE
						PRINTLN("iCreatorID: ",GlobalplayerBD_FM[iPlayerGBD].iCreatorIDOfMIssionIwantToStart,"missionVariation: ",GlobalplayerBD_FM_2[iPlayerGBD].missionVariation)
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(Event.Details.FromPlayerIndex) = FALSE")
				#ENDIF
				ENDIF			
//			#IF IS_DEBUG_BUILD
//			ELSE
//				PRINTLN(" Event.Details.FromPlayerIndex = PLAYER_ID()")
//			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN(" NOT processing INVITE ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() not done")
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("NOT processing INVITE IF IS_NET_PLAYER_OK(Event.Details.FromPlayerIndex) = FALSE")
	#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PRESENCE_INVITE_BE_PROCESSED()
	IF NOT g_bInMultiplayer
		//single player checks if we need them
		PRINTLN("SHOULD_PRESENCE_INVITE_BE_PROCESSED() -TRUE in SP")
		RETURN TRUE
	ELSE
		IF ARE_ALL_INSTANCED_TUTORIALS_COMPLETE()
			//PRINTLN("SHOULD_PRESENCE_INVITE_BE_PROCESSED - TRUE")
			RETURN TRUE
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("SHOULD_PRESENCE_INVITE_BE_PROCESSED NOT processing INVITE ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() not done")
		#ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ADD_PLAY_REQUEST_TO_JOBLIST(PLAYER_INDEX piFromPlayer, INT iMissionType, INT iCreatorID, VECTOR vLocation, TEXT_LABEL_23 sContentID, INT iHeistPackedInt)
	//1313096 Details to be added here Keith  (or here)
	IF IS_THIS_A_MINI_GAME(iMissionType)
		// Keith 23/10/12: Store the details of the invitation so that they can be used on the Joblist
		Store_Invite_Details_For_Joblist_From_Player(	iMissionType,
														vLocation,
														iCreatorID,
														piFromPlayer,
														"",
														"",
														sContentID,
														0)
								
	ELSE
		// Keith 23/10/12: Store the details of the invitation so that they can be used on the Joblist
		Store_Invite_Details_For_Joblist_From_Player(	iMissionType,
														vLocation,
														iCreatorID,
														piFromPlayer,
														GlobalplayerBD_MissionName[NATIVE_TO_INT(piFromPlayer)].tl63MissionName,
														GlobalplayerBD_FM[NATIVE_TO_INT(piFromPlayer)].tl61MissionDescription,
														sContentID,
														iHeistPackedInt)
	ENDIF
ENDPROC

PROC PROCESS_FREEMODE_INVITE_GENERAL(INT iEventID)
	//NET_NL() NET_PRINT("PROCESS_FREEMODE_INVITE_GENERAL- called...") NET_NL()
	SCRIPT_EVENT_DATA_FREEMODE_MISSION_PLAY_REQUEST Event
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))	
		#IF IS_DEBUG_BUILD
		PRINTLN("Recieved PROCESS_FREEMODE_INVITE_GENERAL with ID: ",Event.iDebugIdentifier )		
		#ENDIF
		IF SHOULD_THIS_FREEMODE_MISSION_PLAY_REQUEST_BE_PROCESSED(Event)
			ADD_PLAY_REQUEST_TO_JOBLIST(Event.Details.FromPlayerIndex, Event.iMissionType, Event.iCreatorID, Event.vLocation, Event.sContentID, Event.iPackedInt)
		//ELSE
		//	NET_PRINT("SHOULD_THIS_FREEMODE_MISSION_PLAY_REQUEST_BE_PROCESSED = FALSE") NET_NL()
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_FREEMODE_MISSION_PLAY_REQUEST_PARTY - could not retreive data.")			
	ENDIF	

ENDPROC

FUNC STRING GET_FRIEND_NAME_FROM_GAMER_HANDLE(GAMER_HANDLE& gamer)
	INT i
	GAMER_HANDLE friendHandle
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_FRIEND_NAME_FROM_GAMER_HANDLE- friend count: ", NETWORK_GET_FRIEND_COUNT())
	IF IS_GAMER_HANDLE_VALID(gamer)
		PRINTLN("GET_FRIEND_NAME_FROM_GAMER_HANDLE- passed gamer handle is valid")
		DEBUG_PRINT_GAMER_HANDLE(gamer)
	ENDIF
	PRINTNL()
	#ENDIF
	FOR i = 0 TO NETWORK_GET_FRIEND_COUNT()-1
		PRINTLN("GET_FRIEND_NAME_FROM_GAMER_HANDLE- checking friend ", i)
		CLEAR_GAMER_HANDLE_STRUCT(friendHandle)
		friendHandle = GET_GAMER_HANDLE_FRIEND(I)
		IF IS_GAMER_HANDLE_VALID(friendHandle)
			#IF IS_DEBUG_BUILD
			PRINTLN("GET_FRIEND_NAME_FROM_GAMER_HANDLE- friend ", i, " gamer handle is valid")
			DEBUG_PRINT_GAMER_HANDLE(friendHandle)
			#ENDIF
			IF NETWORK_ARE_HANDLES_THE_SAME(friendHandle,gamer)
				PRINTLN("GET_FRIEND_NAME_FROM_GAMER_HANDLE- handle for passed gamer and friend ", i, " are the same")
				RETURN NETWORK_GET_FRIEND_NAME(i)
			ELSE
				PRINTLN("GET_FRIEND_NAME_FROM_GAMER_HANDLE- handle for passed gamer and friend ", i, " are NOT the same")
			ENDIF
		ELSE
			PRINTLN("GET_FRIEND_NAME_FROM_GAMER_HANDLE- friend ", i, " gamer handle is NOT valid")
		ENDIF
		#IF IS_DEBUG_BUILD
		PRINTNL()
		#ENDIF
	ENDFOR
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	PRINTLN("GET_FRIEND_NAME_FROM_GAMER_HANDLE- failed to friend!")
	SCRIPT_ASSERT("GET_FRIEND_NAME_FROM_GAMER_HANDLE- failed to friend!")
	#ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_FRIEND_NAME_PRETTY_FROM_GAMER_HANDLE(GAMER_HANDLE gamer)
	INT i
	GAMER_HANDLE friendHandle
	FOR i = 0 TO NETWORK_GET_FRIEND_COUNT()-1
		CLEAR_GAMER_HANDLE_STRUCT(friendHandle)
		friendHandle = GET_GAMER_HANDLE_FRIEND(I)
		IF IS_GAMER_HANDLE_VALID(friendHandle)
			IF NETWORK_ARE_HANDLES_THE_SAME(friendHandle,gamer)
				RETURN NETWORK_GET_FRIEND_DISPLAY_NAME(i)
			ENDIF		
		ENDIF
	ENDFOR
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	PRINTLN("GET_FRIEND_NAME_PRETTY_FROM_GAMER_HANDLE- failed to friend!")
	SCRIPT_ASSERT("GET_FRIEND_NAME_PRETTY_FROM_GAMER_HANDLE- failed to friend!")
	#ENDIF
	
	RETURN ""
ENDFUNC


PROC CLEAR_GROUPED_PLAYER_INVITE_GROUP(INT iGroup)
	INT i
	IF iGroup = GROUPED_PLAYER_INVITE_TRANSITION
		REPEAT MAX_SESSION_MEMBERS - 1 i
			CLEAR_GAMER_HANDLE_STRUCT(groupedTransitionPlayerInvites.gamers.Invitees[i])
		ENDREPEAT
		groupedTransitionPlayerInvites.iQueuedInvites = 0
	ENDIF
	IF iGroup = GROUPED_PLAYER_INVITE_NON_TRANSITION
		REPEAT MAX_SESSION_MEMBERS - 1 i
			CLEAR_GAMER_HANDLE_STRUCT(groupedNonTransitionPlayerInvites.gamers.Invitees[i])
		ENDREPEAT
		groupedNonTransitionPlayerInvites.iQueuedInvites = 0
	ENDIF
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	PRINTLN("CLEAR_GROUPED_PLAYER_INVITE_GROUP- called this frame")
	#ENDIF
ENDPROC

PROC CLEAR_GROUPED_PLAYER_INVITE_ALL()
	INT i
	REPEAT GROUPED_PLAYER_INVITE_TYPES i
		CLEAR_GROUPED_PLAYER_INVITE_GROUP(i)
	ENDREPEAT
ENDPROC

PROC SEND_GROUPED_PLAYER_INVITE(INT iGroup #IF IS_DEBUG_BUILD, STRING sProcName #ENDIF)
	IF groupedTransitionPlayerInvites.iQueuedInvites > 0
		IF iGroup = GROUPED_PLAYER_INVITE_TRANSITION
			PRINTLN(sProcName,": SEND_GROUPED_PLAYER_INVITE - NETWORK_INVITE_GAMERS_TO_TRANSITION")
			NETWORK_INVITE_GAMERS_TO_TRANSITION(groupedTransitionPlayerInvites.gamers,groupedTransitionPlayerInvites.iQueuedInvites)
		ENDIF
	ENDIF
	IF groupedNonTransitionPlayerInvites.iQueuedInvites >0
		IF iGroup = GROUPED_PLAYER_INVITE_NON_TRANSITION
			PRINTLN(sProcName,": SEND_GROUPED_PLAYER_INVITE - NETWORK_INVITE_GAMERS")
			NETWORK_INVITE_GAMERS(groupedNonTransitionPlayerInvites.gamers,groupedNonTransitionPlayerInvites.iQueuedInvites)
		ENDIF
	ENDIF
	CLEAR_GROUPED_PLAYER_INVITE_GROUP(iGroup)
ENDPROC

FUNC BOOL IS_GAMER_ALREADY_IN_GROUPED_PLAYER_ARRAY(GAMER_HANDLE gamerHandle)
	INT i
	REPEAT MAX_TRANSITION_MEMBERS-1 i
		IF IS_GAMER_HANDLE_VALID(groupedTransitionPlayerInvites.gamers.Invitees[i])
		AND NETWORK_ARE_HANDLES_THE_SAME(groupedTransitionPlayerInvites.gamers.Invitees[i],gamerHandle)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC UPDATE_GAMER_INVITE_STATUS(GAMER_HANDLE &gamerHandle)

	IF NETWORK_HAS_MADE_INVITE_DECISION(gamerHandle)
		INT iInviteReply = NETWORK_GET_INVITE_REPLY_STATUS(gamerHandle)
		
		STRUCT_PRESENCE_INVITE_REPLY_EVENT sei
		sei.nInviteStatus = INT_TO_ENUM(INVITE_STATUS, iInviteReply)
		SCALEFORM_PLAYER_ROW_STATE playerStatus = GET_TRANSITION_PLAYER_INVITE_STATUS(sei)
		IF DO_WE_NEED_TO_SYNC_CORONA_STATUS_WITH_CODE(playerStatus)
		
			TEXT_LABEL_15 tlStatus
			HUD_COLOURS hcStatus
			GET_PLAYER_CORONA_INVITE_STATUS(playerStatus, tlStatus, hcStatus)
		
			IF IS_GAMER_HANDLE_VALID(gamerHandle)
				IF GET_CORONA_STATUS() >= CORONA_STATUS_JOINED_PLAYERS
				AND GET_CORONA_STATUS() <= CORONA_STATUS_MATCHED_PLAYERS
		
					PRINTLN("UPDATE_GAMER_INVITE_STATUS - Sending status to code: ", tlStatus)
					NETWORK_SET_INVITE_FAILED_MESSAGE_FOR_INVITE_MENU(gamerHandle, tlStatus)
				ENDIF
			ENDIF
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("UPDATE_GAMER_INVITE_STATUS - Do not sync status with code: ", iInviteReply)
		#ENDIF
		ENDIF
	ELSE
		PRINTLN("NETWORK_HAS_MADE_INVITE_DECISION = FALSE")	
	ENDIF

ENDPROC

PROC ADD_GAMER_TO_GROUPED_PLAYER_INVITE(GAMER_HANDLE gamerHandle,STRING GamerName,INT &iSeed, INT iGroup #IF IS_DEBUG_BUILD, STRING sProcName #ENDIF)
	IF NOT NETWORK_HAS_INVITED_GAMER_TO_TRANSITION(gamerHandle)
	AND NOT NETWORK_HAS_INVITED_GAMER(gamerHandle)
		IF NOT IS_GAMER_ALREADY_IN_GROUPED_PLAYER_ARRAY(gamerHandle)
			IF iGroup = GROUPED_PLAYER_INVITE_TRANSITION
				PRINTLN(sProcName,":  -ADD_GAMER_TO_GROUPED_PLAYER_INVITE Adding gamer: ",GamerName, " to GROUPED_PLAYER_INVITE_TRANSITION struct")
				IF groupedTransitionPlayerInvites.iQueuedInvites >= MAX_TRANSITION_MEMBERS - 1
					PRINTLN("ADD_GAMER_TO_GROUPED_PLAYER_INVITE: triggered an invite as array is full")
					SEND_GROUPED_PLAYER_INVITE(iGroup #IF IS_DEBUG_BUILD, sProcName #ENDIF)
				ENDIF
				groupedTransitionPlayerInvites.gamers.Invitees[groupedTransitionPlayerInvites.iQueuedInvites] = gamerHandle
				groupedTransitionPlayerInvites.iQueuedInvites++
			ENDIF
			IF iGroup = GROUPED_PLAYER_INVITE_NON_TRANSITION
				PRINTLN(sProcName,":  -ADD_GAMER_TO_GROUPED_PLAYER_INVITE Adding gamer: ",GamerName, " to GROUPED_PLAYER_INVITE_NON_TRANSITION struct")
				IF groupedNonTransitionPlayerInvites.iQueuedInvites >= MAX_TRANSITION_MEMBERS - 1
					PRINTLN("ADD_GAMER_TO_GROUPED_PLAYER_INVITE: triggered an invite as array is full")
					SEND_GROUPED_PLAYER_INVITE(iGroup #IF IS_DEBUG_BUILD, sProcName #ENDIF)
				ENDIF
				groupedNonTransitionPlayerInvites.gamers.Invitees[groupedNonTransitionPlayerInvites.iQueuedInvites] = gamerHandle
				groupedNonTransitionPlayerInvites.iQueuedInvites++
			ENDIF
			ADD_INVITED_PLAYER_TO_CORONA_LIST(iSeed, gamerHandle, GamerName)
		ELSE
			PRINTLN(sProcName,":  -ADD_GAMER_TO_GROUPED_PLAYER_INVITE gamer already added to struct: ",GamerName)
		ENDIF
	ELSE
		UPDATE_GAMER_INVITE_STATUS(gamerHandle)
	
		PRINTLN(sProcName,":  -ADD_GAMER_TO_GROUPED_PLAYER_INVITE skipping player: ",GamerName, "already invited")
	ENDIF	
ENDPROC

PROC SEND_FRIEND_INVITE(GAMER_HANDLE& gamerToInvite, BOOL inGTAOnly, INT &iSeed ,BOOL bMPonly, BOOL bSendImportantInvite #IF IS_DEBUG_BUILD, STRING sProcName #ENDIF)
	
	STRING friendName
	STRING friendNamePretty
	BOOL bUsePresenceInvites
	BOOL bSendInviteToPlayer = TRUE
	
	// Setup playlist values to use
	INT totalInPlaylist = 0
	INT currentPlaylist	= 0
	IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
		totalInPlaylist = g_sCurrentPlayListDetails.iLength
		currentPlaylist = g_sCurrentPlayListDetails.iCurrentPlayListPosition
		
		//If it's a qualifying playlist then add to is as a flag
		IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
			currentPlaylist += CROSS_SESSION_TOUR_QUAL_OFFSET
		ENDIF
		
		// If the host is on a head to head and sending cross session invites, we need to send the wager the player has to pay
		IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
		OR IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
		OR IS_PLAYLIST_DOING_CHALLENGE()
		OR IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
			currentPlaylist = CROSS_SESSION_H2H_WAGER_OFFSET + g_sCurrentPlayListDetails.iCashBet
		ENDIF
	ENDIF
	
	PLAYER_INDEX playerToInvite
	//PRINTLN("SEND_FRIEND_INVITE: called from ",sProcName)
	IF NETWORK_IS_FRIEND(gamerToInvite)
		PRINTLN(sProcName,": selected player is a friend")
		friendName = GET_FRIEND_NAME_FROM_GAMER_HANDLE(gamerToInvite)
		IF IS_XBOX_PLATFORM()
			friendNamePretty = GET_FRIEND_NAME_PRETTY_FROM_GAMER_HANDLE(gamerToInvite) 
		ELSE
			friendNamePretty = friendName
		ENDIF
		IF IS_XBOX360_VERSION()
			IF NETWORK_IS_PENDING_FRIEND(gamerToInvite)
				PRINTLN(sProcName,": -NOT Inviting friend they are still pending! ", friendName)
				EXIT
			ENDIF
		ENDIF
		//IF NOT NETWORK_IS_PENDING_FRIEND(gamerToInvite)
			IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(gamerToInvite)
				IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerToInvite)
					//bUsePresenceInvites = FALSE
					//IF NETWORK_IS_FRIEND_IN_SAME_TITLE(friendName)
					
					//PRINTLN("SEND_FRIEND_INVITE: ",sProcName, ": in GTAonly = TRUE")
					IF NETWORK_IS_FRIEND_IN_SAME_TITLE(friendName)
					AND NOT NETWORK_IS_INACTIVE_PROFILE(gamerToInvite)
						PRINTLN("SEND_FRIEND_INVITE: ",sProcName, ": NETWORK_IS_FRIEND_IN_SAME_TITLE = TRUE for ", friendNamePretty)
						bUsePresenceInvites = TRUE
						IF bMPonly
							IF NOT NETWORK_IS_FRIEND_IN_MULTIPLAYER(friendName)
								PRINTLN("SEND_FRIEND_INVITE: (MP ONLY) ",sProcName, ": NETWORK_IS_FRIEND_IN_MULTIPLAYER = FALSE for ", friendNamePretty)
								bSendInviteToPlayer = FALSE
							ENDIF	
						ENDIF
					ELSE
						IF inGTAOnly
							bSendInviteToPlayer = FALSE
						ENDIF
						PRINTLN("SEND_FRIEND_INVITE: ",sProcName, ": NETWORK_IS_FRIEND_IN_SAME_TITLE = FALSE for ", friendNamePretty)
					ENDIF
					
					
					
					IF bSendInviteToPlayer
						
						//IF NETWORK_IS_FRIEND_IN_MULTIPLAYER(friendName)
						//	PRINTLN(sProcName,":  -NETWORK_INVITE_GAMER friend is online in GTA V MP going to send presence invite.", friendName) 
						//	bUsePresenceInvites = TRUE
						//ENDIF		
						//#IF IS_DEBUG_BUILD
						//IF g_db_bAlwaysUsePresence
						//bUsePresenceInvites = TRUE
						//ENDIF
						//#ENDIF
						
						IF NOT bUsePresenceInvites
							IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
							OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
								ADD_GAMER_TO_GROUPED_PLAYER_INVITE(gamerToInvite,friendNamePretty,iSeed, GROUPED_PLAYER_INVITE_NON_TRANSITION #IF IS_DEBUG_BUILD, sProcName #ENDIF)
							ELSE
								ADD_GAMER_TO_GROUPED_PLAYER_INVITE(gamerToInvite,friendNamePretty,iSeed, GROUPED_PLAYER_INVITE_TRANSITION #IF IS_DEBUG_BUILD, sProcName #ENDIF)
							ENDIF
							
						ELSE
							IF (NOT NETWORK_HAS_INVITED_GAMER_TO_TRANSITION(gamerToInvite)
							OR (bSendImportantInvite AND NETWORK_HAS_INVITED_GAMER_TO_TRANSITION(gamerToInvite) AND NOT NETWORK_HAS_TRANSITION_INVITE_BEEN_ACKED(gamerToInvite)))
							AND NOT NETWORK_HAS_INVITED_GAMER(gamerToInvite)
								IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
								OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
									IF NETWORK_SEND_INVITE_VIA_PRESENCE(gamerToInvite, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
										PRINTLN(sProcName,":  -NETWORK_INVITE_GAMER Inviting gamer  not in session to game (PRESENCE): ",friendNamePretty)
										iSeed = ADD_INVITED_PLAYER_TO_CORONA_LIST(iSeed, gamerToInvite, friendNamePretty,CORONA_INVITE_TYPE_PRESENCE)
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN(sProcName,": -NETWORK_SEND_INVITE_VIA_PRESENCE(gamerToInvite failed for ",friendNamePretty)	
									#ENDIF
									ENDIF
								ELSE
									IF bSendImportantInvite
										IF NETWORK_SEND_IMPORTANT_TRANSITION_INVITE_VIA_PRESENCE(gamerToInvite, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
											PRINTLN(sProcName,":  -NETWORK_SEND_IMPORTANT_TRANSITION_INVITE_VIA_PRESENCE Inviting gamer  not in session to game (PRESENCE): ",friendNamePretty)
											iSeed = ADD_INVITED_PLAYER_TO_CORONA_LIST(iSeed, gamerToInvite, friendNamePretty,CORONA_INVITE_TYPE_PRESENCE)
										#IF IS_DEBUG_BUILD
										ELSE
											PRINTLN(sProcName,": -NETWORK_SEND_IMPORTANT_TRANSITION_INVITE_VIA_PRESENCE(gamerToInvite failed for ",friendNamePretty)	
											#ENDIF
										ENDIF
									ELSE
										IF NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE(gamerToInvite, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
											PRINTLN(sProcName,":  -NETWORK_INVITE_GAMER_TO_TRANSITION Inviting gamer  not in session to game (PRESENCE): ",friendNamePretty)
											
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", friendNamePretty)
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
											
											iSeed = ADD_INVITED_PLAYER_TO_CORONA_LIST(iSeed, gamerToInvite, friendNamePretty,CORONA_INVITE_TYPE_PRESENCE)
										#IF IS_DEBUG_BUILD
										ELSE
											PRINTLN(sProcName,": -NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE(gamerToInvite failed for ",friendNamePretty)	
											#ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								UPDATE_GAMER_INVITE_STATUS(gamerToInvite)
								PRINTLN(sProcName,":  -NETWORK_INVITE_GAMER_TO_TRANSITION NOT Inviting gamer already invited: ",friendNamePretty)
							ENDIF
						ENDIF
						
						SET_CORONA_BIT(CORONA_INVITED_PLAYER_LIST_UPDATED)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN(sProcName,":bSendInviteToPlayer not in GTAV, no invite sent to ", friendNamePretty)	
					#ENDIF
						
					ENDIF
				ELSE
					playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerToInvite)
					IF playerToInvite != INVALID_PLAYER_INDEX()
					AND IS_NET_PLAYER_OK(playerToInvite,FALSE,TRUE)	
						IF NETWORK_CAN_SEND_LOCAL_INVITE(gamerToInvite)
							PRINTLN(sProcName,": - Inviting friend in session to corona", friendNamePretty)
							BROADCAST_FREEMODE_INVITE_GENERAL(SPECIFIC_PLAYER(playerToInvite),
												sendInviteDetailsStruct.iType,
												sendInviteDetailsStruct.iCreatorID,
												0,
												sendInviteDetailsStruct.vCoronaLocation,
												sendInviteDetailsStruct.sContentID)
							
							iSeed = ADD_INVITED_PLAYER_TO_CORONA_LIST(iSeed, gamerToInvite, friendNamePretty)
						ELSE
							PRINTLN(sProcName,": -NOT Inviting friend in session to corona as they are blocked ", friendNamePretty)
							iSeed = ADD_INVITED_PLAYER_TO_CORONA_LIST(iSeed, gamerToInvite, friendNamePretty,CORONA_INVITE_TYPE_STANDARD,SCALEFORM_PLAYER_ROW_BLOCKED)
						ENDIF
					ELSE
						PRINTLN(sProcName,": -NOT Inviting friend in session to corona they are not in main playing state", friendNamePretty)
					ENDIF
				ENDIF
			ELSE
				PRINTLN(sProcName,": -NETWORK_INVITE_GAMER_TO_TRANSITION not Inviting gamer to session already in transition session: ", friendNamePretty)
			ENDIF
//		ELSE
//			PRINTLN(sProcName,": -NOT Inviting friend they are still pending! ", friendName)
//		ENDIF
	ENDIF
ENDPROC

PROC INVITE_A_CROSS_SESSION_PLAYER(GAMER_HANDLE gamerToInvite)
	NETWORK_GET_GAMER_STATUS_RESULT_STRUCT resultsStruct
	GAMER_INFO gamerInfo
	//BOOL bUsePresenceInvites
	STRING friendName
	
	// Setup playlist values to use
	INT totalInPlaylist = 0
	INT currentPlaylist	= 0
	IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
		totalInPlaylist = g_sCurrentPlayListDetails.iLength
		currentPlaylist = g_sCurrentPlayListDetails.iCurrentPlayListPosition
		
		//If it's a qualifying playlist then add to is as a flag
		IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
			currentPlaylist += CROSS_SESSION_TOUR_QUAL_OFFSET
		ENDIF
		
		// If the host is on a head to head and sending cross session invites, we need to send the wager the player has to pay
		IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
		OR IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
		OR IS_PLAYLIST_DOING_CHALLENGE()
		OR IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
			currentPlaylist = CROSS_SESSION_H2H_WAGER_OFFSET + g_sCurrentPlayListDetails.iCashBet
		ENDIF
	ENDIF
	
	PLAYER_INDEX playerToInvite
	INT iSeed = 0
	SWITCH crossSessionInvite.iStage
		CASE 1
			IF NOT IS_GAMER_HANDLE_VALID(gamerToInvite)
				crossSessionInvite.iStage = 0
				PRINTLN("INVITE_A_CROSS_SESSION_PLAYER(): not inviting play invalid gamer handle")
			ELSE
				IF NETWORK_IS_FRIEND(gamerToInvite)
					PRINTLN("INVITE_A_CROSS_SESSION_PLAYER(): selected player is a friend")
					SEND_FRIEND_INVITE(gamerToInvite,FALSE, iSeed,FALSE,TRUE #IF IS_DEBUG_BUILD,"INVITE_A_CROSS_SESSION_PLAYER" #ENDIF)
					crossSessionInvite.iStage =0
				ELSE
					IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerToInvite)
						NETWORK_QUEUE_GAMER_FOR_STATUS(gamerToInvite)
						NETWORK_GET_GAMER_STATUS_FROM_QUEUE()
						crossSessionInvite.iStage = 2
						PRINTLN("INVITE_A_CROSS_SESSION_PLAYER(): moving to stage: ",crossSessionInvite.iStage)	
					ELSE
						playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerToInvite)
						IF playerToInvite != INVALID_PLAYER_INDEX()
						AND IS_NET_PLAYER_OK(playerToInvite,FALSE,TRUE)	
							IF NETWORK_CAN_SEND_LOCAL_INVITE(gamerToInvite)
								PRINTLN("INVITE_A_CROSS_SESSION_PLAYER() - Inviting gamer in session to corona", friendName)
								BROADCAST_FREEMODE_INVITE_GENERAL(SPECIFIC_PLAYER(playerToInvite),
												sendInviteDetailsStruct.iType,
												sendInviteDetailsStruct.iCreatorID,
												0,
												sendInviteDetailsStruct.vCoronaLocation,
												sendInviteDetailsStruct.sContentID)
								ADD_INVITED_PLAYER_TO_CORONA_LIST(0, gamerToInvite, friendName)
							ELSE
								PRINTLN("[CORONA] INVITE_A_CROSS_SESSION_PLAYER  -NOT Inviting gamer in session to corona as they are blocked ", friendName)
								ADD_INVITED_PLAYER_TO_CORONA_LIST(0, gamerToInvite, friendName,CORONA_INVITE_TYPE_STANDARD,SCALEFORM_PLAYER_ROW_BLOCKED)
							ENDIF
							crossSessionInvite.iStage =0
						ELSE
							PRINTLN("INVITE_A_CROSS_SESSION_PLAYER() -NOT Inviting friend in session to corona they are not in main playing state", friendName)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT NETWORK_IS_GETTING_GAMER_STATUS()
				IF NETWORK_DID_GET_GAMER_STATUS_SUCCEED()
					//IF NETWORK_GET_NUM_FOUND_GAMERS() > 0	
						//PRINTLN("INVITE_A_CROSS_SESSION_PLAYER about to call NETWORK_GET_GAMER_STATUS_RESULT")
						IF NETWORK_GET_GAMER_STATUS_RESULT(resultsStruct,0)
							IF IS_GAMER_HANDLE_VALID(resultsStruct.hGamer)
								IF resultsStruct.nStatus = GAMER_STATUS_INVALID
								OR resultsStruct.nStatus =  GAMER_STATUS_OFF_TITLE
									PRINTLN("INVITE_A_CROSS_SESSION_PLAYER(): (NON-PRESENCE)Inviting resultsStruct.nStatus = ", ENUM_TO_INT(resultsStruct.nStatus))
									IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
									OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
										ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iSeed, GROUPED_PLAYER_INVITE_NON_TRANSITION #IF IS_DEBUG_BUILD, "INVITE_A_CROSS_SESSION_PLAYER" #ENDIF)
									ELSE
										ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iSeed, GROUPED_PLAYER_INVITE_TRANSITION #IF IS_DEBUG_BUILD, "INVITE_A_CROSS_SESSION_PLAYER" #ENDIF)
									ENDIF
									crossSessionInvite.bSuccess = TRUE
								ELSE
									//IF resultsStruct.nStatus !=  GAMER_STATUS_IN_TITLE_MP
									//#IF IS_DEBUG_BUILD
									//AND NOT g_db_bAlwaysUsePresence
									//#ENDIF
									//	PRINTLN("INVITE_A_CROSS_SESSION_PLAYER resultsStruct.nStatus !=  GAMER_STATUS_IN_TITLE_MP")
									//	IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
									//		ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iSeed, GROUPED_PLAYER_INVITE_NON_TRANSITION #IF IS_DEBUG_BUILD, "INVITE_A_CROSS_SESSION_PLAYER" #ENDIF)
									//	ELSE
									//		ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iSeed, GROUPED_PLAYER_INVITE_TRANSITION #IF IS_DEBUG_BUILD, "INVITE_A_CROSS_SESSION_PLAYER" #ENDIF)
									//	ENDIF
									//ELSE
										//PRINTLN("INVITE_A_CROSS_SESSION_PLAYER resultsStruct.nStatus =  GAMER_STATUS_IN_TITLE")
									IF (NOT NETWORK_HAS_INVITED_GAMER_TO_TRANSITION(resultsStruct.hGamer)
									OR NETWORK_HAS_INVITED_GAMER_TO_TRANSITION(resultsStruct.hGamer) AND NOT NETWORK_HAS_TRANSITION_INVITE_BEEN_ACKED(resultsStruct.hGamer))
									AND NOT NETWORK_HAS_INVITED_GAMER(resultsStruct.hGamer)
									
										// KGM 26/2/15 [BUG 2250287]: For Cross-session invites to individuals 'from last heist' or 'from last job' we need to pass this to the receiving player
										//		so that the invite doesn't get rejected for non-friends in SP. We'll add a value to the currentlPlaylist value to check on the receiver.
										IF (GET_CORONA_STATUS() = CORONA_STATUS_LAST_JOB_PLAYERS)
										OR (GET_CORONA_STATUS() = CORONA_STATUS_LAST_HEIST_PLAYERS)
											PRINTLN(".KGM INVITE_A_CROSS_SESSION_PLAYER - Setting CROSS_SESSION_SHOW_IN_SP_OFFSET to ensure non-friends in SP will receive the invite (individuals from last Job or Heist)")
											currentPlaylist += CROSS_SESSION_SHOW_IN_SP_OFFSET
										ENDIF
													
										IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
										OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
											IF NETWORK_SEND_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
												iSeed = ADD_INVITED_PLAYER_TO_CORONA_LIST(iSeed, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_PRESENCE)	
												PRINTLN("INVITE_A_CROSS_SESSION_PLAYER -NETWORK_INVITE_GAMER content ID ",sendInviteDetailsStruct.sContentID ," Inviting gamer to session one player (PRESENCE) = ",gamerInfo.Name)
												
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", gamerInfo.Name)
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
											
											ELSE
												PRINTLN("INVITE_A_CROSS_SESSION_PLAYER -NETWORK_INVITE_GAMER FAILED TO SEND INVITE (PRESENCE) = ",gamerInfo.Name)
											ENDIF
										ELSE
											IF NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
												iSeed = ADD_INVITED_PLAYER_TO_CORONA_LIST(iSeed, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_PRESENCE)
												PRINTLN("INVITE_A_CROSS_SESSION_PLAYER -NETWORK_INVITE_GAMER_TO_TRANSITION content ID ",sendInviteDetailsStruct.sContentID ," Inviting gamer to transition session one player (PRESENCE) = ", gamerInfo.Name)
												
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", gamerInfo.Name)
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
											ELSE
												PRINTLN("INVITE_A_CROSS_SESSION_PLAYER -NETWORK_INVITE_GAMER FAILED TO SEND INVITE (PRESENCE) = ",gamerInfo.Name)
											ENDIF
										ENDIF
									ELSE
										PRINTLN("INVITE_A_CROSS_SESSION_PLAYER- not inviting player already invited- ",gamerInfo.Name)
									ENDIF
									//ENDIF
									crossSessionInvite.bSuccess = TRUE
									PRINTLN("INVITE_A_CROSS_SESSION_PLAYER(): success! resultsStruct.nStatus = ", ENUM_TO_INT(resultsStruct.nStatus))
								ENDIF
								crossSessionInvite.iStage = 3
								NETWORK_CLEAR_GET_GAMER_STATUS()
							ELSE
								crossSessionInvite.bSuccess = TRUE
								crossSessionInvite.iStage = 3
								PRINTLN("INVITE_A_CROSS_SESSION_PLAYER(): FAILED INVALID GAMER HANDLE resultsStruct.nStatus = ", ENUM_TO_INT(resultsStruct.nStatus))
								NETWORK_CLEAR_GET_GAMER_STATUS()
							ENDIF
						ELSE
							crossSessionInvite.bSuccess = FALSE
							PRINTLN("INVITE_A_CROSS_SESSION_PLAYER(): NETWORK_GET_GAMER_STATUS_RESULT failed ")		
							crossSessionInvite.iStage = 3
							NETWORK_CLEAR_GET_GAMER_STATUS()
						ENDIF
					//ELSE
					//	crossSessionInvite.bSuccess = FALSE
					//	PRINTLN("INVITE_A_CROSS_SESSION_PLAYER(): No gamer data returned aborting ")		
					//	crossSessionInvite.iStage = 3
					//	NETWORK_CLEAR_GET_GAMER_STATUS()
					//	PRINTLN("INVITE_A_CROSS_SESSION_PLAYER(): moving to stage: ",crossSessionInvite.iStage)	
					//ENDIF
				ELSE
					crossSessionInvite.bSuccess = FALSE
					PRINTLN("INVITE_A_CROSS_SESSION_PLAYER(): NETWORK_DID_GET_GAMER_STATUS_SUCCEED() FAILED! ")		
					crossSessionInvite.iStage = 3
					NETWORK_CLEAR_GET_GAMER_STATUS()
				ENDIF
			ELSE
				PRINTLN("INVITE_A_CROSS_SESSION_PLAYER(): waiting for NETWORK_IS_GETTING_GAMER_STATUS()")	
			ENDIF
		BREAK
		CASE 3
			crossSessionInvite.iStage = 0
		BREAK
		
	ENDSWITCH
ENDPROC


PROC MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS()
	INT i, iCounter
	GAMER_INFO gamerInfo
	NETWORK_GET_GAMER_STATUS_RESULT_STRUCT resultsStruct
	INT iInviteSlot
	
	// Setup playlist values to use
	INT totalInPlaylist = 0
	INT currentPlaylist	= 0
	INT currentPlaylistCache
	IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
		totalInPlaylist = g_sCurrentPlayListDetails.iLength
		currentPlaylist = g_sCurrentPlayListDetails.iCurrentPlayListPosition
		
		//If it's a qualifying playlist then add to is as a flag
		IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
			currentPlaylist += CROSS_SESSION_TOUR_QUAL_OFFSET
		ENDIF
		
		// If the host is on a head to head and sending cross session invites, we need to send the wager the player has to pay
		IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
		OR IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
		OR IS_PLAYLIST_DOING_CHALLENGE()
		OR IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
			currentPlaylist = CROSS_SESSION_H2H_WAGER_OFFSET + g_sCurrentPlayListDetails.iCashBet
		ENDIF
	ENDIF
	
	IF IS_PLAYER_ONLINE()
		SWITCH sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS]
			CASE 0
				i = 0
				iCounter = 0
				REPEAT NUM_NETWORK_PLAYERS i
					IF IS_GAMER_HANDLE_VALID(lastJobPlayers.Gamers[i])
						IF NETWORK_IS_FRIEND(lastJobPlayers.Gamers[i])
							SEND_FRIEND_INVITE(lastJobPlayers.Gamers[i],FALSE,iInviteSlot,FALSE,FALSE #IF IS_DEBUG_BUILD,"MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS" #ENDIF)
						ELSE
							NETWORK_QUEUE_GAMER_FOR_STATUS(lastJobPlayers.Gamers[i])
							iCounter++
						ENDIF
					ENDIF
				ENDREPEAT
				IF iCounter > 0
					NETWORK_GET_GAMER_STATUS_FROM_QUEUE()
					sendInviteDetailsStruct.iNumExpectedResults[SECTION_LAST_JOB_PLAYERS] = iCounter
					PRINTLN("MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS(): number of results ", sendInviteDetailsStruct.iNumExpectedResults[SECTION_LAST_JOB_PLAYERS])
					sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS] = 1
					//PRINTLN("MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS(): moving to stage ", sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS])
				ELSE
					sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS] = 2
					PRINTLN("MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS(): moving to stage (no entries)", sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS])	
				ENDIF
			BREAK
			
			CASE 1
				currentPlaylistCache = currentPlaylist
				IF NOT NETWORK_IS_GETTING_GAMER_STATUS()
					IF NETWORK_DID_GET_GAMER_STATUS_SUCCEED()
						i = 0
						
						PLAYER_INDEX playerToInvite
						IF sendInviteDetailsStruct.iNumExpectedResults[SECTION_LAST_JOB_PLAYERS] > 0
							REPEAT sendInviteDetailsStruct.iNumExpectedResults[SECTION_LAST_JOB_PLAYERS] i
								NETWORK_GET_GAMER_STATUS_RESULT(resultsStruct,i)
								IF IS_GAMER_HANDLE_VALID(resultsStruct.hGamer)
									IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(resultsStruct.hGamer)
										IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(resultsStruct.hGamer)
											PRINTLN("MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS(): gamerInfo.Name: ",gamerInfo.Name, " resultsStruct.nStatus: ",resultsStruct.nStatus)
											IF resultsStruct.nStatus = GAMER_STATUS_INVALID
											OR resultsStruct.nStatus = GAMER_STATUS_OFF_TITLE
											//IF resultsStruct.nStatus !=  GAMER_STATUS_IN_TITLE_MP
												//PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL  -NETWORK_INVITE_GAMER NOT inviting LAST JOB gamer they are not in MP= ", gamerInfo.Name)
												IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
												OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
													ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iInviteSlot, GROUPED_PLAYER_INVITE_NON_TRANSITION #IF IS_DEBUG_BUILD,"MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS" #ENDIF)
												ELSE
													ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iInviteSlot, GROUPED_PLAYER_INVITE_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS" #ENDIF)
												ENDIF
											ELSE
//												IF resultsStruct.nStatus !=  GAMER_STATUS_IN_TITLE_MP
//												#IF IS_DEBUG_BUILD
//												AND NOT g_db_bAlwaysUsePresence
//												#ENDIF
//													
//												ELSE
												IF NOT NETWORK_HAS_INVITED_GAMER_TO_TRANSITION(resultsStruct.hGamer)
												AND NOT NETWORK_HAS_INVITED_GAMER(resultsStruct.hGamer)
													// KGM 26/2/15 [BUG 2250287]: For Cross-session invites to 'all from last job' we need to pass this to the receiving player
													//		so that the invite doesn't get rejected for non-friends in SP. We'll add a value to the currentlPlaylist value to check on the receiver.
													PRINTLN(".KGM [CORONA] SEND_CORONA_INVITE_TO_ALL - Setting CROSS_SESSION_SHOW_IN_SP_OFFSET to ensure non-friends in SP will receive the invite (all from last Job)")
													currentPlaylist = currentPlaylistCache
													currentPlaylist += CROSS_SESSION_SHOW_IN_SP_OFFSET
													
													IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
													OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
														IF NETWORK_SEND_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
															iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_PRESENCE)
															PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER content ID: ",sendInviteDetailsStruct.sContentID, " Inviting last job player not in session to game (PRESENCE) = ",gamerInfo.Name)
															
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", gamerInfo.Name)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
														ENDIF
													ELSE
														IF NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
															iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_PRESENCE)								
															PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER_TO_TRANSITION content ID: ",sendInviteDetailsStruct.sContentID, " Inviting last job player not in session to game (PRESENCE) = ",gamerInfo.Name)
															
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", gamerInfo.Name)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
														ENDIF
													ENDIF
												ELSE
													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL- no inviting last job player already invited 2 - ",gamerInfo.Name)
												ENDIF
//												ENDIF
												// Add player to invite list
											ENDIF
										ELSE
											// Add player to invite list
											iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name)
											PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER_TO_TRANSITION not inviting player they are already in transition session")
										ENDIF
									ELSE
										IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(resultsStruct.hGamer)
											playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(resultsStruct.hGamer)
											IF playerToInvite != INVALID_PLAYER_INDEX()
											AND IS_NET_PLAYER_OK(playerToInvite,FALSE,TRUE)
												IF NETWORK_CAN_SEND_LOCAL_INVITE(resultsStruct.hGamer)
													BROADCAST_FREEMODE_INVITE_GENERAL(SPECIFIC_PLAYER(playerToInvite),
																	sendInviteDetailsStruct.iType,
																	sendInviteDetailsStruct.iCreatorID,
																	0,
																	sendInviteDetailsStruct.vCoronaLocation,
																	sendInviteDetailsStruct.sContentID)

													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - Inviting last job player in session to corona")
													// Add player to invite list
													iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name)
												ELSE
													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL  -NOT Inviting last job gamer in session to corona as they are blocked ", gamerInfo.Name)
													iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_STANDARD,SCALEFORM_PLAYER_ROW_BLOCKED)
												ENDIF
											ELSE
												PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - NOT Inviting last job player in session to corona they are not in main playing state")
											ENDIF
										ELSE
											// Add player to invite list
											iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name)
											PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER_TO_TRANSITION not inviting player they are already in transition session")
										ENDIF
									ENDIF
								ELSE	
									PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - NOT Inviting last job player invalid gamer handle")
								ENDIF
							ENDREPEAT 
							NETWORK_CLEAR_GET_GAMER_STATUS()
							sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS] = 2
							PRINTLN("MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS(): moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- SUCCEEDED) =", sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS])	
						ELSE
							NETWORK_CLEAR_GET_GAMER_STATUS()
							sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS] = 2
							PRINTLN("MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS(): moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- SUCCEEDED) NO GAMERS RETURNED =", sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS])	
						ENDIF
					ELSE
						NETWORK_CLEAR_GET_GAMER_STATUS()
						sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS] = 2
						PRINTLN("MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS(): moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- FAILED) =", sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS])	
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS(): waiting for NETWORK_IS_GETTING_GAMER_STATUS()")	
				ENDIF 
			BREAK
			
			CASE 2
				sendInviteDetailsStruct.iNumExpectedResults[SECTION_LAST_JOB_PLAYERS] = 0
				sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS] = 0
				CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_LAST_JOB_PLAYERS)
			BREAK
		
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS()
	
	INT i, iCounter
	GAMER_INFO gamerInfo
	NETWORK_GET_GAMER_STATUS_RESULT_STRUCT resultsStruct
	INT iInviteSlot
	
	// Setup playlist values to use
	INT totalInPlaylist = 0
	INT currentPlaylist	= 0
	IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
		totalInPlaylist = g_sCurrentPlayListDetails.iLength
		currentPlaylist = g_sCurrentPlayListDetails.iCurrentPlayListPosition
		
		//If it's a qualifying playlist then add to is as a flag
		IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
			currentPlaylist += CROSS_SESSION_TOUR_QUAL_OFFSET
		ENDIF
		
		// If the host is on a head to head and sending cross session invites, we need to send the wager the player has to pay
		IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
		OR IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
		OR IS_PLAYLIST_DOING_CHALLENGE()
		OR IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
			currentPlaylist = CROSS_SESSION_H2H_WAGER_OFFSET + g_sCurrentPlayListDetails.iCashBet
		ENDIF
	ENDIF
	
	IF IS_PLAYER_ONLINE()
		SWITCH sendInviteDetailsStruct.iStage[SECTION_LAST_HEIST]
			CASE 0
				i = 0
				iCounter = 0
				REPEAT NUM_NETWORK_PLAYERS i
					IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
						IF IS_GAMER_HANDLE_VALID(lastGangOpsHeistPlayers.Gamers[i])
							IF NETWORK_IS_FRIEND(lastGangOpsHeistPlayers.Gamers[i])
								SEND_FRIEND_INVITE(lastGangOpsHeistPlayers.Gamers[i],FALSE,iInviteSlot,FALSE,FALSE #IF IS_DEBUG_BUILD,"MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS" #ENDIF)
							ELSE
								NETWORK_QUEUE_GAMER_FOR_STATUS(lastGangOpsHeistPlayers.Gamers[i])
								iCounter++
							ENDIF
						ENDIF
					ELSE
						IF IS_GAMER_HANDLE_VALID(lastHeistPlayers.Gamers[i])
							IF NETWORK_IS_FRIEND(lastHeistPlayers.Gamers[i])
								SEND_FRIEND_INVITE(lastHeistPlayers.Gamers[i],FALSE,iInviteSlot,FALSE,FALSE #IF IS_DEBUG_BUILD,"MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS" #ENDIF)
							ELSE
								NETWORK_QUEUE_GAMER_FOR_STATUS(lastHeistPlayers.Gamers[i])
								iCounter++
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				IF iCounter > 0
					NETWORK_GET_GAMER_STATUS_FROM_QUEUE()
					sendInviteDetailsStruct.iNumExpectedResults[SECTION_LAST_HEIST] = iCounter
					PRINTLN("MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS(): number of results ", sendInviteDetailsStruct.iNumExpectedResults[SECTION_LAST_HEIST])
					sendInviteDetailsStruct.iStage[SECTION_LAST_HEIST] = 1
					//PRINTLN("MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS(): moving to stage ", sendInviteDetailsStruct.iStage[SECTION_LAST_JOB_PLAYERS])
				ELSE
					sendInviteDetailsStruct.iStage[SECTION_LAST_HEIST] = 2
					PRINTLN("MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS(): moving to stage (no entries)", sendInviteDetailsStruct.iStage[SECTION_LAST_HEIST])	
				ENDIF
			BREAK
			
			CASE 1
				IF NOT NETWORK_IS_GETTING_GAMER_STATUS()
					IF NETWORK_DID_GET_GAMER_STATUS_SUCCEED()
						i = 0
						
						PLAYER_INDEX playerToInvite
						IF sendInviteDetailsStruct.iNumExpectedResults[SECTION_LAST_HEIST] > 0
							REPEAT sendInviteDetailsStruct.iNumExpectedResults[SECTION_LAST_HEIST] i
								NETWORK_GET_GAMER_STATUS_RESULT(resultsStruct,i)
								IF IS_GAMER_HANDLE_VALID(resultsStruct.hGamer)
									IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(resultsStruct.hGamer)
										IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(resultsStruct.hGamer)
											PRINTLN("MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS(): gamerInfo.Name: ",gamerInfo.Name, " resultsStruct.nStatus: ",resultsStruct.nStatus)
											IF resultsStruct.nStatus = GAMER_STATUS_INVALID
											OR resultsStruct.nStatus = GAMER_STATUS_OFF_TITLE
											//IF resultsStruct.nStatus !=  GAMER_STATUS_IN_TITLE_MP
												//PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL  -NETWORK_INVITE_GAMER NOT inviting LAST JOB gamer they are not in MP= ", gamerInfo.Name)
												IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
												OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
													ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iInviteSlot, GROUPED_PLAYER_INVITE_NON_TRANSITION #IF IS_DEBUG_BUILD,"MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS" #ENDIF)
												ELSE
													ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iInviteSlot, GROUPED_PLAYER_INVITE_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS" #ENDIF)
												ENDIF
											ELSE
//												IF resultsStruct.nStatus !=  GAMER_STATUS_IN_TITLE_MP
//												#IF IS_DEBUG_BUILD
//												AND NOT g_db_bAlwaysUsePresence
//												#ENDIF
//													
//												ELSE
												IF NOT NETWORK_HAS_INVITED_GAMER_TO_TRANSITION(resultsStruct.hGamer)
												AND NOT NETWORK_HAS_INVITED_GAMER(resultsStruct.hGamer)
													// KGM 26/2/15 [BUG 2250287]: For Cross-session invites to 'all from last heist' we need to pass this to the receiving player
													//		so that the invite doesn't get rejected for non-friends in SP. We'll add a value to the currentlPlaylist value to check on the receiver.
													PRINTLN(".KGM [CORONA] SEND_CORONA_INVITE_TO_ALL - Setting CROSS_SESSION_SHOW_IN_SP_OFFSET to ensure non-friends in SP will receive the invite (all from last Heist)")
													currentPlaylist += CROSS_SESSION_SHOW_IN_SP_OFFSET
													
													IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
													OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
														IF NETWORK_SEND_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
															iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_PRESENCE)
															PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER content ID: ",sendInviteDetailsStruct.sContentID, " Inviting last job player not in session to game (PRESENCE) = ",gamerInfo.Name)
															
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", gamerInfo.Name)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
														ENDIF
													ELSE
														IF NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
															iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_PRESENCE)								
															PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER_TO_TRANSITION content ID: ",sendInviteDetailsStruct.sContentID, " Inviting last job player not in session to game (PRESENCE) = ",gamerInfo.Name)
															
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", gamerInfo.Name)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
														ENDIF
													ENDIF
												ELSE
													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -not inviting last job player already invited- ",gamerInfo.Name)
												ENDIF
//												ENDIF
												// Add player to invite list
											ENDIF
										ELSE
											// Add player to invite list
											iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name)
											PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER_TO_TRANSITION not inviting player they are already in transition session")
										ENDIF
									ELSE
										IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(resultsStruct.hGamer)
											playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(resultsStruct.hGamer)
											IF playerToInvite != INVALID_PLAYER_INDEX()
											AND IS_NET_PLAYER_OK(playerToInvite,FALSE,TRUE)
												IF NETWORK_CAN_SEND_LOCAL_INVITE(resultsStruct.hGamer)
													BROADCAST_FREEMODE_INVITE_GENERAL(SPECIFIC_PLAYER(playerToInvite),
																	sendInviteDetailsStruct.iType,
																	sendInviteDetailsStruct.iCreatorID,
																	0,
																	sendInviteDetailsStruct.vCoronaLocation,
																	sendInviteDetailsStruct.sContentID)

													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - Inviting last job player in session to corona")
													// Add player to invite list
													iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name)
												ELSE
													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL  -NOT Inviting last job gamer in session to corona as they are blocked ", gamerInfo.Name)
													iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_STANDARD,SCALEFORM_PLAYER_ROW_BLOCKED)
												ENDIF
											ELSE
												PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - NOT Inviting last job player in session to corona they are not in main playing state")
											ENDIF
										ELSE
											// Add player to invite list
											iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name)
											PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER_TO_TRANSITION not inviting player they are already in transition session")
										ENDIF
									ENDIF
								ELSE	
									PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - NOT Inviting last job player invalid gamer handle")
								ENDIF
							ENDREPEAT 
							NETWORK_CLEAR_GET_GAMER_STATUS()
							sendInviteDetailsStruct.iStage[SECTION_LAST_HEIST] = 2
							PRINTLN("MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS(): moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- SUCCEEDED) =", sendInviteDetailsStruct.iStage[SECTION_LAST_HEIST])	
						ELSE
							NETWORK_CLEAR_GET_GAMER_STATUS()
							sendInviteDetailsStruct.iStage[SECTION_LAST_HEIST] = 2
							PRINTLN("MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS(): moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- SUCCEEDED) NO GAMERS RETURNED =", sendInviteDetailsStruct.iStage[SECTION_LAST_HEIST])	
						ENDIF
					ELSE
						NETWORK_CLEAR_GET_GAMER_STATUS()
						sendInviteDetailsStruct.iStage[SECTION_LAST_HEIST] = 2
						PRINTLN("MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS(): moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- FAILED) =", sendInviteDetailsStruct.iStage[SECTION_LAST_HEIST])	
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS(): waiting for NETWORK_IS_GETTING_GAMER_STATUS()")	
				ENDIF 
			BREAK
			
			CASE 2
				sendInviteDetailsStruct.iNumExpectedResults[SECTION_LAST_HEIST] = 0
				sendInviteDetailsStruct.iStage[SECTION_LAST_HEIST] = 0
				CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_LAST_HEIST_PLAYERS)
			BREAK
		
		ENDSWITCH
	ENDIF
ENDPROC

PROC CLEAR_GAMER_INFO(GAMER_INFO& gamerInfo)
	gamerInfo.Handle.Data1 = 0
	gamerInfo.Handle.Data2 = 0
	gamerInfo.Handle.Data3 = 0
	gamerInfo.Handle.Data4 = 0
	gamerInfo.Handle.Data5 = 0
	gamerInfo.Handle.Data6 = 0
	gamerInfo.Handle.Data7 = 0
	gamerInfo.Handle.Data8 = 0
	gamerInfo.Handle.Data9 = 0
	gamerInfo.Handle.Data10 = 0
	gamerInfo.Handle.Data11 = 0
	gamerInfo.Handle.Data12 = 0
	gamerInfo.Handle.Data13 = 0
	gamerInfo.Name = ""
ENDPROC

PROC MAINTAIN_INVITE_ALL_CREW(INT iCrewID)
	GAMER_HANDLE theGamer = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	INT I
	INT iInviteSlot
	PLAYER_INDEX playerToInvite
	INT iCounter
	GAMER_INFO gamerInfo
	NETWORK_GET_GAMER_STATUS_RESULT_STRUCT resultsStruct
	INT iSection 
	IF iCrewID = 0
		iSection = SECTION_CREW_PLAYERS
	ELSE
		iSection = SECTION_OTHER_CREW_PLAYERS
	ENDIF
	
	// Setup playlist values to use
	INT totalInPlaylist = 0
	INT currentPlaylist	= 0
	IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
		totalInPlaylist = g_sCurrentPlayListDetails.iLength
		currentPlaylist = g_sCurrentPlayListDetails.iCurrentPlayListPosition
		
		//If it's a qualifying playlist then add to is as a flag
		IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
			currentPlaylist += CROSS_SESSION_TOUR_QUAL_OFFSET
		ENDIF
		
		// If the host is on a head to head and sending cross session invites, we need to send the wager the player has to pay
		IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
		OR IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
		OR IS_PLAYLIST_DOING_CHALLENGE()
		OR IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
			currentPlaylist = CROSS_SESSION_H2H_WAGER_OFFSET + g_sCurrentPlayListDetails.iCashBet
		ENDIF
	ENDIF
	
	IF IS_PLAYER_ONLINE()
		SWITCH sendInviteDetailsStruct.iStage[iSection]
			CASE 0
				IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(theGamer)
					IF NOT IS_BIT_SET(sendInviteDetailsStruct.iBS,SEND_INVITE_BS_GRABBED_CREW_DATA)
						NETWORK_FIND_GAMERS_IN_CREW(iCrewID)  //0 means use the local player's active crew
						SET_BIT(sendInviteDetailsStruct.iBS,SEND_INVITE_BS_GRABBED_CREW_DATA)
						PRINTLN("MAINTAIN_CORONA_GROUP_INVITES: start search for crew ID = ", iCrewID)
					ELSE
						IF NOT NETWORK_IS_FINDING_GAMERS()
							//PRINTLN("MAINTAIN_CORONA_GROUP_INVITES: finished finding crew")
							IF NETWORK_DID_FIND_GAMERS_SUCCEED()
								//PRINTLN("MAINTAIN_CORONA_GROUP_INVITES: find succeeded")
								I = 0
								//PRINTLN("MAINTAIN_CORONA_GROUP_INVITES:num crew players found = ", NETWORK_GET_NUM_FOUND_GAMERS())
								INT iNumGamersToInvite
								
								iNumGamersToInvite = NETWORK_GET_NUM_FOUND_GAMERS()
								
								// If we are above our crew cap, them limit us to this amount
								IF iNumGamersToInvite > g_sMPTunables.iMax_Crew_Invites_NG
									iNumGamersToInvite = g_sMPTunables.iMax_Crew_Invites_NG
								ENDIF
								
								REPEAT iNumGamersToInvite I
									IF NETWORK_GET_FOUND_GAMER(gamerInfo,i)
										IF NETWORK_IS_HANDLE_VALID(gamerInfo.Handle,SIZE_OF(gamerInfo.Handle))
											IF NETWORK_IS_FRIEND(gamerInfo.Handle)
												SEND_FRIEND_INVITE(gamerInfo.Handle,TRUE,iInviteSlot,FALSE,FALSE #IF IS_DEBUG_BUILD,"MAINTAIN_INVITE_ALL_CREW" #ENDIF)
											ELSE
												IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerInfo.Handle)
													IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(gamerInfo.Handle)
														PRINTLN("MP_menuped.sch - NETWORK_QUEUE_GAMER_FOR_STATUS : ",gamerInfo.Name )
														NETWORK_QUEUE_GAMER_FOR_STATUS(gamerInfo.Handle)
														iCounter++
														
													ELSE
														PRINTLN("MP_menuped.sch - NETWORK_INVITE_GAMER_TO_TRANSITION NOT Inviting gamer to session they are already in it from crew : ",gamerInfo.Name )
													ENDIF
													//iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, gamerInfo.Handle, gamerInfo.Name)
												ELSE
													playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerInfo.Handle)
													IF playerToInvite != INVALID_PLAYER_INDEX()
													AND IS_NET_PLAYER_OK(playerToInvite,FALSE,TRUE)
														IF NETWORK_CAN_SEND_LOCAL_INVITE(gamerInfo.Handle)
															BROADCAST_FREEMODE_INVITE_GENERAL(SPECIFIC_PLAYER(playerToInvite),
																				sendInviteDetailsStruct.iType,
																				sendInviteDetailsStruct.iCreatorID,
																				0,
																				sendInviteDetailsStruct.vCoronaLocation,
																				sendInviteDetailsStruct.sContentID)
															iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, gamerInfo.Handle, gamerInfo.Name)
														ELSE
															PRINTLN("MP_menuped.sch  -NOT Inviting gamer in session to corona as they are blocked ", gamerInfo.Name)
															iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_STANDARD,SCALEFORM_PLAYER_ROW_BLOCKED)
														ENDIF
													ELSE
														PRINTLN("MP_menuped.sch - NETWORK_INVITE_GAMER_TO_TRANSITION NOT Inviting gamer in session they are not in playing state from crew : ",gamerInfo.Name )	
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									CLEAR_GAMER_INFO(gamerInfo)
								ENDREPEAT
								IF iCounter > 0
									NETWORK_CLEAR_FOUND_GAMERS()
									NETWORK_GET_GAMER_STATUS_FROM_QUEUE()
									
									sendInviteDetailsStruct.iStage[iSection] = 1
									sendInviteDetailsStruct.iNumExpectedResults[iSection] = iCounter
									PRINTLN("MAINTAIN_INVITE_ALL_CREW(): Crew ID = ",iCrewID, " Crew ID = ",iCrewID, " moving to stage ", sendInviteDetailsStruct.iStage[iSection])
									EXIT
								ELSE
									NETWORK_CLEAR_FOUND_GAMERS()
									sendInviteDetailsStruct.iStage[iSection] = 2
									PRINTLN("MAINTAIN_INVITE_ALL_CREW(): Crew ID = ",iCrewID, " moving to stage (no crew not in session)", sendInviteDetailsStruct.iStage[iSection])	
								ENDIF
							ELSE
								NETWORK_CLEAR_FOUND_GAMERS()
								sendInviteDetailsStruct.iStage[iSection] = 2
								PRINTLN("MAINTAIN_INVITE_ALL_CREW(): Crew ID = ",iCrewID, " moving to stage (find gamers failed)", sendInviteDetailsStruct.iStage[iSection])	
							ENDIF
						ELSE
							PRINTLN("MAINTAIN_CORONA_GROUP_INVITES: waiting for NOT NETWORK_IS_FINDING_GAMERS(")
						ENDIF
					ENDIF
				ELSE
					CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_GRABBED_CREW_DATA)
					CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_MATCHED_CREW)
					sendInviteDetailsStruct.iCrewIDToInvite = 0
				ENDIF
			
			BREAK
			
			CASE 1
				IF NOT NETWORK_IS_GETTING_GAMER_STATUS()
					IF NETWORK_DID_GET_GAMER_STATUS_SUCCEED()
						i = 0
						IF sendInviteDetailsStruct.iNumExpectedResults[iSection] > 0
							REPEAT sendInviteDetailsStruct.iNumExpectedResults[iSection] i
								NETWORK_GET_GAMER_STATUS_RESULT(resultsStruct,i)
								IF IS_GAMER_HANDLE_VALID(resultsStruct.hGamer)
									IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(resultsStruct.hGamer)
										IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(resultsStruct.hGamer)
											IF resultsStruct.nStatus = GAMER_STATUS_INVALID
											OR resultsStruct.nStatus =  GAMER_STATUS_OFF_TITLE
												PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER NOT inviting CREW gamer they are off title or invalid= ", gamerInfo.Name)
											ELSE
//												IF resultsStruct.nStatus !=  GAMER_STATUS_IN_TITLE_MP
//												#IF IS_DEBUG_BUILD
//												AND NOT g_db_bAlwaysUsePresence
//												#ENDIF
//													IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
//														ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iInviteSlot, GROUPED_PLAYER_INVITE_NON_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_INVITE_ALL_CREW" #ENDIF)
//													ELSE
//														ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iInviteSlot, GROUPED_PLAYER_INVITE_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_INVITE_ALL_CREW" #ENDIF)
//													ENDIF
//												ELSE
												IF NOT NETWORK_HAS_INVITED_GAMER_TO_TRANSITION(resultsStruct.hGamer)
												AND NOT NETWORK_HAS_INVITED_GAMER(resultsStruct.hGamer)		
													IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
													OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
														IF NETWORK_SEND_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
															PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER content ID: ",sendInviteDetailsStruct.sContentID, " Inviting from crew not in session to game (PRESENCE) = ",gamerInfo.Name)
															// Add player to invite list
															iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_PRESENCE)
															
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", gamerInfo.Name)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
														ENDIF
													ELSE
														IF NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
															PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER_TO_TRANSITION ",sendInviteDetailsStruct.sContentID, " from crew player not in session to game (PRESENCE) = ",gamerInfo.Name)
															iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_PRESENCE)
															
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", gamerInfo.Name)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
														ENDIF
													ENDIF
												ELSE
													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL- already invited crew player- ",gamerInfo.Name)
												ENDIF
//												ENDIF
												
											ENDIF
										ELSE
											// Add player to invite list
											iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name)
											PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER_TO_TRANSITION not inviting player they are already in transition session")
										ENDIF
									ELSE
										IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(resultsStruct.hGamer)
											playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(resultsStruct.hGamer)
											IF playerToInvite != INVALID_PLAYER_INDEX()
											AND IS_NET_PLAYER_OK(playerToInvite,FALSE,TRUE)
												IF NETWORK_CAN_SEND_LOCAL_INVITE(resultsStruct.hGamer)
													BROADCAST_FREEMODE_INVITE_GENERAL(SPECIFIC_PLAYER(playerToInvite),
																	sendInviteDetailsStruct.iType,
																	sendInviteDetailsStruct.iCreatorID,
																	0,
																	sendInviteDetailsStruct.vCoronaLocation,
																	sendInviteDetailsStruct.sContentID)

													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - Inviting crew player in session to corona")
													// Add player to invite list
													iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name)
												ELSE
													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL: -NOT Inviting gamer in session to corona as they are blocked ", gamerInfo.Name)
													iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_STANDARD,SCALEFORM_PLAYER_ROW_BLOCKED)
												ENDIF
											ELSE
												PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - NOT Inviting crew player in session to corona they are not in main playing state")
											ENDIF
											
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT 
							NETWORK_CLEAR_GET_GAMER_STATUS()
							sendInviteDetailsStruct.iStage[iSection] = 2
							PRINTLN("MAINTAIN_INVITE_ALL_CREW(): Crew ID = ",iCrewID, " moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- SUCCEEDED) =", sendInviteDetailsStruct.iStage[iSection])	
						ELSE
							NETWORK_CLEAR_GET_GAMER_STATUS()
							sendInviteDetailsStruct.iStage[iSection] = 2
							PRINTLN("MAINTAIN_INVITE_ALL_CREW(): Crew ID = ",iCrewID, " moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- SUCCEEDED) NO GAMERS RETURNED =", sendInviteDetailsStruct.iStage[iSection])	
						ENDIF
					ELSE
						NETWORK_CLEAR_GET_GAMER_STATUS()
						sendInviteDetailsStruct.iStage[iSection] = 2
						PRINTLN("MAINTAIN_INVITE_ALL_CREW(): Crew ID = ",iCrewID, " moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- FAILED) =", sendInviteDetailsStruct.iStage[iSection])	
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_INVITE_ALL_CREW(): Crew ID = ",iCrewID, " waiting for NETWORK_IS_GETTING_GAMER_STATUS()")	
				ENDIF 
			BREAK
			
			CASE 2
				sendInviteDetailsStruct.iStage[iSection] = 0
				sendInviteDetailsStruct.iNumExpectedResults[iSection] = 0
				CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_GRABBED_CREW_DATA)
				CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_MATCHED_CREW)
				sendInviteDetailsStruct.iCrewIDToInvite = 0
			BREAK
		
		ENDSWITCH
	ELSE
		sendInviteDetailsStruct.iStage[iSection] = 0
		CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_GRABBED_CREW_DATA)
		CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_MATCHED_CREW)
		sendInviteDetailsStruct.iCrewIDToInvite = 0
	ENDIF	
ENDPROC

PROC CLEAR_MATCHED_GAMERS_STRUCT()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		CLEAR_GAMER_HANDLE_STRUCT(matchedPlayers.Gamers[i])
		matchedPlayers.GamersNames[i] = ""
	ENDREPEAT
	matchedPlayers.iCounter = 0
	matchedPlayers.iFindStage = 0
	matchedPlayers.bFinished = FALSE
ENDPROC

FUNC BOOL FIND_MATCHED_GAMERS()
	FLOAT fLocalPlayerSkill
	//NETWORK_FIND_GAMERS_RESULT gamerResults
	INT I = 0

	GAMER_INFO gamerInfo
	IF IS_PLAYER_ONLINE()
		SWITCH matchedPlayers.iFindStage
			CASE 0
				fLocalPlayerSkill = GET_LOCAL_PLAYER_ACTIVITY_RATING(sendInviteDetailsStruct.iType)
				PRINTLN("FIND_MATCHED_GAMERS: iActivityID = ",sendInviteDetailsStruct.iType, " fLocalPlayerSkill = ",fLocalPlayerSkill)
				NETWORK_FIND_MATCHED_GAMERS(GET_ACTIVITY_INDEX_FOR_RATING(sendInviteDetailsStruct.iType),fLocalPlayerSkill)
				matchedPlayers.iFindStage = 1
			BREAK
			CASE 1
				IF NOT NETWORK_IS_FINDING_GAMERS()
					//PRINTLN("FIND_MATCHED_GAMERS: finished finding matched players")
					IF NETWORK_DID_FIND_GAMERS_SUCCEED()
						//PRINTLN("FIND_MATCHED_GAMERS: matched players find succeeded")
						//PRINTLN("FIND_MATCHED_GAMERS: num matched players found = ", NETWORK_GET_NUM_FOUND_GAMERS())
						i = 0
						REPEAT NETWORK_GET_NUM_FOUND_GAMERS() I
							IF NETWORK_GET_FOUND_GAMER(gamerInfo,i)
								IF NETWORK_IS_HANDLE_VALID(gamerInfo.Handle,SIZE_OF(gamerInfo.Handle))
									IF matchedPlayers.iCounter < NUM_NETWORK_PLAYERS
										matchedPlayers.Gamers[matchedPlayers.iCounter] = gamerInfo.Handle
										matchedPlayers.GamersNames[matchedPlayers.iCounter] = gamerInfo.Name
										PRINTLN("FIND_MATCHED_GAMERS: matchedPlayers.GamersNames[",matchedPlayers.iCounter,"] = ",matchedPlayers.GamersNames[matchedPlayers.iCounter])
										matchedPlayers.iCounter++
									ENDIF
								ENDIF
							ENDIF
							CLEAR_GAMER_INFO(gamerInfo)
						ENDREPEAT
						NETWORK_CLEAR_FOUND_GAMERS()
						matchedPlayers.iFindStage = 2
						PRINTLN("FIND_MATCHED_GAMERS: moving to stage ", matchedPlayers.iFindStage)	
					ELSE
						NETWORK_CLEAR_FOUND_GAMERS()
						matchedPlayers.iFindStage = 2
						PRINTLN("FIND_MATCHED_GAMERS: moving to stage (find gamers failed)", sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS])	
					ENDIF
				ELSE
					PRINTLN("FIND_MATCHED_GAMERS: waiting for NOT NETWORK_IS_FINDING_GAMERS(")
				ENDIF
			BREAK
			CASE 2
				matchedPlayers.bFinished = TRUE
				RETURN TRUE
			BREAK
		ENDSWITCH
//	ELSE
//		PRINTLN("FIND_MATCHED_GAMERS: finished! player not online or NOT in transition")
//		matchedPlayers.bFinished = TRUE
//		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_INVITE_ALL_MATCHED_PLAYERS()
	INT i, iCounter
	GAMER_INFO gamerInfo
	NETWORK_GET_GAMER_STATUS_RESULT_STRUCT resultsStruct
	INT iInviteSlot
	
	// Setup playlist values to use
	INT totalInPlaylist = 0
	INT currentPlaylist	= 0
	IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
		totalInPlaylist = g_sCurrentPlayListDetails.iLength
		currentPlaylist = g_sCurrentPlayListDetails.iCurrentPlayListPosition
		
		//If it's a qualifying playlist then add to is as a flag
		IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
			currentPlaylist += CROSS_SESSION_TOUR_QUAL_OFFSET
		ENDIF
		
		// If the host is on a head to head and sending cross session invites, we need to send the wager the player has to pay
		IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
		OR IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
		OR IS_PLAYLIST_DOING_CHALLENGE()
		OR IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
			currentPlaylist = CROSS_SESSION_H2H_WAGER_OFFSET + g_sCurrentPlayListDetails.iCashBet
		ENDIF
	ENDIF
	
	IF IS_PLAYER_ONLINE()
		SWITCH sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS]
			CASE 0
				IF matchedPlayers.bFinished
					i = 0
					iCounter = 0
					
					REPEAT g_sMPTunables.iMax_Skill_Matched_Invites_NG i		// Cap to tunable instead of NUM_NETWORK_PLAYERS 2275597
						IF IS_GAMER_HANDLE_VALID(matchedPlayers.Gamers[i])
							IF NETWORK_IS_FRIEND(matchedPlayers.Gamers[i])
								SEND_FRIEND_INVITE(matchedPlayers.Gamers[i],TRUE,iInviteSlot,TRUE,FALSE #IF IS_DEBUG_BUILD,"MAINTAIN_INVITE_ALL_MATCHED_PLAYERS" #ENDIF)
							ELSE
								NETWORK_QUEUE_GAMER_FOR_STATUS(matchedPlayers.Gamers[i])
								iCounter++
							ENDIF
						ENDIF
					ENDREPEAT
					IF iCounter > 0
						NETWORK_GET_GAMER_STATUS_FROM_QUEUE()
						sendInviteDetailsStruct.iNumExpectedResults[SECTION_MATCHED_PLAYERS] = iCounter
						PRINTLN("MAINTAIN_INVITE_ALL_MATCHED_PLAYERS: number of results ", sendInviteDetailsStruct.iNumExpectedResults[SECTION_MATCHED_PLAYERS])
						sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS] = 1
						//PRINTLN("MAINTAIN_INVITE_ALL_MATCHED_PLAYERS: moving to stage ", sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS])
					ELSE
						sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS] = 2
						PRINTLN("MAINTAIN_INVITE_ALL_MATCHED_PLAYERS: moving to stage (no entries)", sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS])	
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_INVITE_ALL_MATCHED_PLAYERS: Waiting for matchedPlayers.bFinished")	
				ENDIF	
			BREAK
			
			CASE 1
				IF NOT NETWORK_IS_GETTING_GAMER_STATUS()
					IF NETWORK_DID_GET_GAMER_STATUS_SUCCEED()
						i = 0
						
						PLAYER_INDEX playerToInvite
						IF sendInviteDetailsStruct.iNumExpectedResults[SECTION_MATCHED_PLAYERS] > 0
							REPEAT sendInviteDetailsStruct.iNumExpectedResults[SECTION_MATCHED_PLAYERS] i
								NETWORK_GET_GAMER_STATUS_RESULT(resultsStruct,i)
								IF IS_GAMER_HANDLE_VALID(resultsStruct.hGamer)
									IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(resultsStruct.hGamer)
										IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(resultsStruct.hGamer)
											PRINTLN("MAINTAIN_INVITE_ALL_MATCHED_PLAYERS: gamerInfo.Name: ",gamerInfo.Name, " resultsStruct.nStatus: ",resultsStruct.nStatus)
											IF resultsStruct.nStatus = GAMER_STATUS_INVALID
											//OR resultsStruct.nStatus = GAMER_STATUS_OFF_TITLE
											//IF resultsStruct.nStatus !=  GAMER_STATUS_IN_TITLE_MP
												PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL  -NETWORK_INVITE_GAMER NOT inviting LAST JOB gamer they are not in MP= ", gamerInfo.Name)
											ELSE
												//IF resultsStruct.nStatus !=  GAMER_STATUS_IN_TITLE_MP
//												#IF IS_DEBUG_BUILD
//												AND NOT g_db_bAlwaysUsePresence
//												#ENDIF
//													IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
//														ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iInviteSlot, GROUPED_PLAYER_INVITE_NON_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_INVITE_ALL_MATCHED_PLAYERS" #ENDIF)
//													ELSE
//														ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,gamerInfo.Name,iInviteSlot, GROUPED_PLAYER_INVITE_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_INVITE_ALL_MATCHED_PLAYERS" #ENDIF)
//													ENDIF
//												ELSE
												IF NOT NETWORK_HAS_INVITED_GAMER_TO_TRANSITION(resultsStruct.hGamer)
												AND NOT NETWORK_HAS_INVITED_GAMER(resultsStruct.hGamer)
													IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
													OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
														IF NETWORK_SEND_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
															iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_PRESENCE)
															PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER content ID: ",sendInviteDetailsStruct.sContentID," Inviting last job player not in session to game (PRESENCE) = ",gamerInfo.Name)
															
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", gamerInfo.Name)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
														ENDIF
													ELSE
														IF NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
															iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_PRESENCE)								
															PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER_TO_TRANSITION content ID: ",sendInviteDetailsStruct.sContentID,"Inviting last job player not in session to game (PRESENCE) = ",gamerInfo.Name)
															
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", gamerInfo.Name)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
															PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
														ENDIF
													ENDIF
												ELSE
													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - already invited last job player- ",gamerInfo.Name)
												ENDIF
//												ENDIF
												// Add player to invite list
											ENDIF
										ELSE
											// Add player to invite list
											iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name)
											PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER_TO_TRANSITION not inviting player they are already in transition session")
										ENDIF
									ELSE
										IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(resultsStruct.hGamer)
											playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(resultsStruct.hGamer)
											IF playerToInvite != INVALID_PLAYER_INDEX()
											AND IS_NET_PLAYER_OK(playerToInvite,FALSE,TRUE)
												IF NETWORK_CAN_SEND_LOCAL_INVITE(resultsStruct.hGamer)
													BROADCAST_FREEMODE_INVITE_GENERAL(SPECIFIC_PLAYER(playerToInvite),
																		sendInviteDetailsStruct.iType,
																		sendInviteDetailsStruct.iCreatorID,
																		0,
																		sendInviteDetailsStruct.vCoronaLocation,
																		sendInviteDetailsStruct.sContentID)

													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - Inviting last job player in session to corona")
													// Add player to invite list
													iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name)
												ELSE
													PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL: -NOT Inviting friend in session to corona as they are blocked ", gamerInfo.Name)
													iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_STANDARD,SCALEFORM_PLAYER_ROW_BLOCKED)
												ENDIF
											ELSE
												PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - NOT Inviting last job player in session to corona they are not in main playing state")
											ENDIF
										ELSE
											// Add player to invite list
											iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name)
											PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL -NETWORK_INVITE_GAMER_TO_TRANSITION not inviting player they are already in transition session")
										ENDIF
									ENDIF
								ELSE	
									PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL - NOT Inviting last job player invalid gamer handle")
								ENDIF
							ENDREPEAT 
							NETWORK_CLEAR_GET_GAMER_STATUS()
							sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS] = 2
							PRINTLN("MAINTAIN_INVITE_ALL_MATCHED_PLAYERS: moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- SUCCEEDED) =", sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS])	
						ELSE
							NETWORK_CLEAR_GET_GAMER_STATUS()
							sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS] = 2
							PRINTLN("MAINTAIN_INVITE_ALL_MATCHED_PLAYERS: moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- SUCCEEDED) NO GAMERS RETURNED =", sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS])	
						ENDIF
					ELSE
						NETWORK_CLEAR_GET_GAMER_STATUS()
						sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS] = 2
						PRINTLN("MAINTAIN_INVITE_ALL_MATCHED_PLAYERS: moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- FAILED) =", sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS])	
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_INVITE_ALL_MATCHED_PLAYERS: waiting for NETWORK_IS_GETTING_GAMER_STATUS()")	
				ENDIF 
			BREAK
			
			CASE 2
				sendInviteDetailsStruct.iNumExpectedResults[SECTION_MATCHED_PLAYERS] = 0
				sendInviteDetailsStruct.iStage[SECTION_MATCHED_PLAYERS] = 0
				CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_MATCHED_PLAYERS)
			BREAK
		
		ENDSWITCH
	ENDIF
ENDPROC

//PROC MAINTAIN_INVITE_ALL_MATCHED_PLAYERS()
//	MAINTAIN_INVITE_ALL_MATCHED_PLAYERS()
//	IF IS_PLAYER_ONLINE()
//		
//
//	ELSE
//		CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_GRABBED_MATCHED_PLAYER_DATA)
//		CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_MATCHED_PLAYERS)
//	ENDIF
//ENDPROC

PROC MAINTAIN_INVITE_ALL_FRIENDS()
	INT I
	GAMER_HANDLE gamerToInvite
	PLAYER_INDEX playerToInvite
	STRING friendName
	STRING friendNamePretty
	//BOOL bUsePresenceInvites = FALSE
	INT iInviteSlot = 0
	BOOL bSkipPlayer
	
	// Setup playlist values to use
	INT totalInPlaylist = 0
	INT currentPlaylist	= 0
	IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
		totalInPlaylist = g_sCurrentPlayListDetails.iLength
		currentPlaylist = g_sCurrentPlayListDetails.iCurrentPlayListPosition
		
		//If it's a qualifying playlist then add to is as a flag
		IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
			currentPlaylist += CROSS_SESSION_TOUR_QUAL_OFFSET
		ENDIF
		
		// If the host is on a head to head and sending cross session invites, we need to send the wager the player has to pay
		IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
		OR IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
		OR IS_PLAYLIST_DOING_CHALLENGE()
		OR IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
			currentPlaylist = CROSS_SESSION_H2H_WAGER_OFFSET + g_sCurrentPlayListDetails.iCashBet
		ENDIF
	ENDIF
	
	// Applying a cap for friends invites
	INT iTotalFriendsInvited = 0	
	
	//lots of friends...
	FOR I = 0 TO NETWORK_GET_FRIEND_COUNT()-1
		bSkipPlayer = FALSE
		friendName = NETWORK_GET_FRIEND_NAME(I)
		IF IS_XBOX_PLATFORM()
			friendNamePretty = NETWORK_GET_FRIEND_DISPLAY_NAME(I) 
		ELSE
			friendNamePretty = friendName
		ENDIF
		IF NETWORK_IS_FRIEND_ONLINE(friendName)
			gamerToInvite = GET_GAMER_HANDLE_FRIEND(I)
			IF IS_GAMER_HANDLE_VALID(gamerToInvite)
				IF IS_XBOX360_VERSION()
					IF NETWORK_IS_PENDING_FRIEND(gamerToInvite)
						//PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS()-NOT Inviting friend they are still pending! ", friendName)
						bSkipPlayer = TRUE
					ENDIF
				ENDIF
				IF NOT bSkipPlayer
					IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(gamerToInvite)
						IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerToInvite)
							//bUsePresenceInvites = FALSE
							IF NETWORK_IS_FRIEND_IN_SAME_TITLE(friendName)
							AND NOT NETWORK_IS_INACTIVE_PROFILE(gamerToInvite)
//								IF NETWORK_IS_FRIEND_IN_MULTIPLAYER(friendName)
//									PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS() -NETWORK_INVITE_GAMER friend is online in GTA V MP going to send presence invite.", friendName) 
//									bUsePresenceInvites = TRUE
//								ENDIF
//								#IF IS_DEBUG_BUILD
//								IF g_db_bAlwaysUsePresence
//								bUsePresenceInvites = TRUE
//								ENDIF
//								#ENDIF
//								IF NOT bUsePresenceInvites
//									IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
//										ADD_GAMER_TO_GROUPED_PLAYER_INVITE(gamerToInvite,friendName,iInviteSlot, GROUPED_PLAYER_INVITE_NON_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_INVITE_ALL_FRIENDS" #ENDIF)
//									ELSE
//										ADD_GAMER_TO_GROUPED_PLAYER_INVITE(gamerToInvite,friendName,iInviteSlot, GROUPED_PLAYER_INVITE_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_INVITE_ALL_FRIENDS" #ENDIF)
//									ENDIF
//								ELSE
								IF NOT NETWORK_HAS_INVITED_GAMER_TO_TRANSITION(gamerToInvite)
								AND NOT NETWORK_HAS_INVITED_GAMER(gamerToInvite)
									IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
									OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
										IF NETWORK_SEND_INVITE_VIA_PRESENCE(gamerToInvite, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
											PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS()-NETWORK_INVITE_GAMER Inviting gamer to session with friend of all friends invite not in session to game (PRESENCE): ", friendNamePretty)
											iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, gamerToInvite, friendNamePretty,CORONA_INVITE_TYPE_PRESENCE)
											
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", friendNamePretty)
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
										
											iTotalFriendsInvited++
										#IF IS_DEBUG_BUILD
										ELSE
											PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS()-NETWORK_SEND_INVITE_VIA_PRESENCE(gamerToInvite failed for ", friendNamePretty)	
										#ENDIF
										ENDIF
									ELSE
										IF NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE(gamerToInvite, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
											PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS() -NETWORK_INVITE_GAMER_TO_TRANSITION Inviting gamer to session with friend of all friends invite not in session to game (PRESENCE): ", friendNamePretty)
											iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, gamerToInvite, friendNamePretty,CORONA_INVITE_TYPE_PRESENCE)
											
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", friendNamePretty)
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
											PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)

											iTotalFriendsInvited++
										#IF IS_DEBUG_BUILD
										ELSE
											PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS() -NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE(gamerToInvite failed for ", friendNamePretty)	
										#ENDIF
										ENDIF
									ENDIF
								#IF IS_DEBUG_BUILD
								ELSE
									PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS() -Not inviting player already invited: ", friendNamePretty)	
								#ENDIF	
								ENDIF
//								ENDIF
								
								SET_CORONA_BIT(CORONA_INVITED_PLAYER_LIST_UPDATED)
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS() -NETWORK_IS_FRIEND_IN_SAME_TITLE() not in GTAV, no invite sent to ", friendNamePretty)	
							#ENDIF
								
							ENDIF
						ELSE
							playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerToInvite)
							IF playerToInvite != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(playerToInvite,FALSE,TRUE)
								IF NETWORK_CAN_SEND_LOCAL_INVITE(gamerToInvite)
									//PRINTLN("[CORONA] MP_menuped.sch - Inviting friend in session to corona", friendName)
									BROADCAST_FREEMODE_INVITE_GENERAL(SPECIFIC_PLAYER(playerToInvite),
													sendInviteDetailsStruct.iType,
													sendInviteDetailsStruct.iCreatorID,
													0,
													sendInviteDetailsStruct.vCoronaLocation,
													sendInviteDetailsStruct.sContentID)
									iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, gamerToInvite, friendNamePretty)

									iTotalFriendsInvited++
								ELSE
									PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS(): -NOT Inviting friend in session to corona as they are blocked ", friendNamePretty)
									iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, gamerToInvite, friendNamePretty,CORONA_INVITE_TYPE_STANDARD,SCALEFORM_PLAYER_ROW_BLOCKED)
								ENDIF
							ELSE
								PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS() -NOT Inviting friend in session to corona they are not in main playing state", friendNamePretty)
							ENDIF
							SET_CORONA_BIT(CORONA_INVITED_PLAYER_LIST_UPDATED)
						ENDIF
					ELSE
						PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS()-NETWORK_INVITE_GAMER_TO_TRANSITION not Inviting gamer to session already in transition session: ", friendNamePretty)
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS()-NOT Inviting friend they are still pending! ", friendNamePretty)
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS() -Not inviting friend invalid gamer handle: ",friendNamePretty)	
			#ENDIF
			ENDIF					
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS() -Not inviting friend not online: ",friendNamePretty)	
		#ENDIF
		ENDIF
		
		PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS() - iTotalFriendsInvited: ", iTotalFriendsInvited)	
		
		INT iMaxFriendsInvites
		#IF NOT IS_NEXTGEN_BUILD 
		iMaxFriendsInvites = g_sMPTunables.iMax_Friends_Invites_LG
		#ENDIF		
		#IF IS_NEXTGEN_BUILD 
		iMaxFriendsInvites = g_sMPTunables.iMax_Friends_Invites_NG
		#ENDIF
		
		IF iTotalFriendsInvited >= iMaxFriendsInvites
		
			PRINTLN("MAINTAIN_INVITE_ALL_FRIENDS() - Finished loop to our max. Exit: ", iTotalFriendsInvited)	
			i =  NETWORK_GET_FRIEND_COUNT()
		ENDIF
	ENDFOR
	CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_FRIENDS)
ENDPROC

PROC MAINTAIN_INVITE_ALL_PARTY_MEMBERS_SETUP()
	INT i = 0
	INT iCounter = 0
	INT iInviteSlot
	
	GAMER_HANDLE	Handle    
	TEXT_LABEL_63	Name
	REPEAT MAX_PARTY_MEMBERS i
		g_PartyDesc.MemberInfo[i].Handle = Handle
		g_PartyDesc.MemberInfo[i].Name = Name
	ENDREPEAT
	g_PartyDesc.bIsInSession = FALSE
	g_PartyDesc.MemberCount = 0
	
	NETWORK_GET_PLATFORM_PARTY_MEMBERS(g_PartyDesc, SIZE_OF(g_PartyDesc))
	REPEAT g_PartyDesc.MemberCount i
		IF IS_GAMER_HANDLE_VALID(g_PartyDesc.MemberInfo[i].Handle)
			IF NETWORK_IS_FRIEND(g_PartyDesc.MemberInfo[i].Handle)
				SEND_FRIEND_INVITE(g_PartyDesc.MemberInfo[i].Handle,FALSE,iInviteSlot,FALSE,FALSE #IF IS_DEBUG_BUILD,"MAINTAIN_INVITE_ALL_PARTY_MEMBERS" #ENDIF)
			ELSE
				NETWORK_QUEUE_GAMER_FOR_STATUS(g_PartyDesc.MemberInfo[i].Handle)
				iCounter++
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iCounter > 0
		NETWORK_GET_GAMER_STATUS_FROM_QUEUE()
		sendInviteDetailsStruct.iNumExpectedResults[SECTION_PARTY_MEMBERS] = iCounter
		PRINTLN("MAINTAIN_INVITE_ALL_PARTY_MEMBERS: number of results ", sendInviteDetailsStruct.iNumExpectedResults[SECTION_PARTY_MEMBERS])
		sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS] = 1
		//PRINTLN("MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS(): moving to stage ", sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS])
	ELSE
		sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS] = 2
		PRINTLN("MAINTAIN_INVITE_ALL_PARTY_MEMBERS: moving to stage (no entries)", sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS])	
	ENDIF
  ENDPROC
PROC MAINTAIN_INVITE_ALL_PARTY_MEMBERS_RESULTS()
	GAMER_INFO gamerInfo
	NETWORK_GET_GAMER_STATUS_RESULT_STRUCT resultsStruct
	INT iInviteSlot
	
	// Setup playlist values to use
	INT totalInPlaylist = 0
	INT currentPlaylist	= 0
	IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
		totalInPlaylist = g_sCurrentPlayListDetails.iLength
		currentPlaylist = g_sCurrentPlayListDetails.iCurrentPlayListPosition
		
		//If it's a qualifying playlist then add to is as a flag
		IF IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
			currentPlaylist += CROSS_SESSION_TOUR_QUAL_OFFSET
		ENDIF
		
		// If the host is on a head to head and sending cross session invites, we need to send the wager the player has to pay
		IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
		OR IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
		OR IS_PLAYLIST_DOING_CHALLENGE()
		OR IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
			currentPlaylist = CROSS_SESSION_H2H_WAGER_OFFSET + g_sCurrentPlayListDetails.iCashBet
		ENDIF
	ENDIF
	IF NOT NETWORK_IS_GETTING_GAMER_STATUS()
		IF NETWORK_DID_GET_GAMER_STATUS_SUCCEED()
			INT i = 0
			
			PLAYER_INDEX playerToInvite
			IF sendInviteDetailsStruct.iNumExpectedResults[SECTION_PARTY_MEMBERS] > 0
				REPEAT sendInviteDetailsStruct.iNumExpectedResults[SECTION_PARTY_MEMBERS] i
					NETWORK_GET_GAMER_STATUS_RESULT(resultsStruct,i)
					IF IS_GAMER_HANDLE_VALID(resultsStruct.hGamer)
						IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(resultsStruct.hGamer)
							IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(resultsStruct.hGamer)
								PRINTLN("MAINTAIN_INVITE_ALL_PARTY_MEMBERS: gamerInfo.Name: ","", " resultsStruct.nStatus: ",resultsStruct.nStatus)
								IF resultsStruct.nStatus = GAMER_STATUS_INVALID
								OR resultsStruct.nStatus = GAMER_STATUS_OFF_TITLE
								//IF resultsStruct.nStatus !=  GAMER_STATUS_IN_TITLE_MP
									//PRINTLN("[CORONA] SEND_CORONA_INVITE_TO_ALL  -NETWORK_INVITE_GAMER NOT inviting LAST JOB gamer they are not in MP= ", gamerInfo.Name)
									IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
									OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
										ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,"",iInviteSlot, GROUPED_PLAYER_INVITE_NON_TRANSITION #IF IS_DEBUG_BUILD,"MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS" #ENDIF)
									ELSE
										ADD_GAMER_TO_GROUPED_PLAYER_INVITE(resultsStruct.hGamer,"",iInviteSlot, GROUPED_PLAYER_INVITE_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS" #ENDIF)
									ENDIF
								ELSE
//												IF resultsStruct.nStatus !=  GAMER_STATUS_IN_TITLE_MP
//												#IF IS_DEBUG_BUILD
//												AND NOT g_db_bAlwaysUsePresence
//												#ENDIF
//													
//												ELSE
									IF NOT NETWORK_HAS_INVITED_GAMER_TO_TRANSITION(resultsStruct.hGamer)
									AND NOT NETWORK_HAS_INVITED_GAMER(resultsStruct.hGamer)
										IF HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
										OR HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
											IF NETWORK_SEND_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
												iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, "",CORONA_INVITE_TYPE_PRESENCE)
												PRINTLN("[CORONA] MAINTAIN_INVITE_ALL_PARTY_MEMBERS -NETWORK_INVITE_GAMER content ID: ",sendInviteDetailsStruct.sContentID, " Inviting last job player not in session to game (PRESENCE) = ","")
												
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", "")
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
											ENDIF
										ELSE
											IF NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE(resultsStruct.hGamer, sendInviteDetailsStruct.sContentID, totalInPlaylist, currentPlaylist)
												iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, "",CORONA_INVITE_TYPE_PRESENCE)								
												PRINTLN("[CORONA] MAINTAIN_INVITE_ALL_PARTY_MEMBERS -NETWORK_INVITE_GAMER_TO_TRANSITION content ID: ",sendInviteDetailsStruct.sContentID, " Inviting last job player not in session to game (PRESENCE) = ","")
												
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - gamerToInvite = ", "")
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - sendInviteDetailsStruct.sContentID = ", sendInviteDetailsStruct.sContentID)
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - totalInPlaylist = ", totalInPlaylist)
												PRINTLN("[NETWORK_SEND_TRANSITION_INVITE_VIA_PRESENCE] - [RE] - currentPlaylist = ", currentPlaylist)
											ENDIF
										ENDIF
									ELSE
										PRINTLN("[CORONA] MAINTAIN_INVITE_ALL_PARTY_MEMBERS: Not inviting player already invited- ","")
									ENDIF
//												ENDIF
									// Add player to invite list
								ENDIF
							ELSE
								// Add player to invite list
								iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, "")
								PRINTLN("[CORONA] MAINTAIN_INVITE_ALL_PARTY_MEMBERS -NETWORK_INVITE_GAMER_TO_TRANSITION not inviting player they are already in transition session")
							ENDIF
						ELSE
							IF NOT IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(resultsStruct.hGamer)
								playerToInvite = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(resultsStruct.hGamer)
								IF playerToInvite != INVALID_PLAYER_INDEX()
								AND IS_NET_PLAYER_OK(playerToInvite,FALSE,TRUE)
									IF NETWORK_CAN_SEND_LOCAL_INVITE(resultsStruct.hGamer)
										BROADCAST_FREEMODE_INVITE_GENERAL(SPECIFIC_PLAYER(playerToInvite),
														sendInviteDetailsStruct.iType,
														sendInviteDetailsStruct.iCreatorID,
														0,
														sendInviteDetailsStruct.vCoronaLocation,
														sendInviteDetailsStruct.sContentID)

										PRINTLN("[CORONA] MAINTAIN_INVITE_ALL_PARTY_MEMBERS - Inviting last job player in session to corona")
										// Add player to invite list
										iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, "")
									ELSE
										PRINTLN("[CORONA] MAINTAIN_INVITE_ALL_PARTY_MEMBERS  -NOT Inviting last job gamer in session to corona as they are blocked ", gamerInfo.Name)
										iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, gamerInfo.Name,CORONA_INVITE_TYPE_STANDARD,SCALEFORM_PLAYER_ROW_BLOCKED)
									ENDIF
								ELSE
									PRINTLN("[CORONA] MAINTAIN_INVITE_ALL_PARTY_MEMBERS - NOT Inviting last job player in session to corona they are not in main playing state")
								ENDIF
							ELSE
								// Add player to invite list
								iInviteSlot = ADD_INVITED_PLAYER_TO_CORONA_LIST(iInviteSlot, resultsStruct.hGamer, "")
								PRINTLN("[CORONA] MAINTAIN_INVITE_ALL_PARTY_MEMBERS -NETWORK_INVITE_GAMER_TO_TRANSITION not inviting player they are already in transition session")
							ENDIF
						ENDIF
					ELSE	
						PRINTLN("[CORONA] MAINTAIN_INVITE_ALL_PARTY_MEMBERS- NOT Inviting last job player invalid gamer handle")
					ENDIF
				ENDREPEAT 
				NETWORK_CLEAR_GET_GAMER_STATUS()
				sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS] = 2
				PRINTLN("MAINTAIN_INVITE_ALL_PARTY_MEMBERS: moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- SUCCEEDED) =", sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS])	
			ELSE
				NETWORK_CLEAR_GET_GAMER_STATUS()
				sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS] = 2
				PRINTLN("MAINTAIN_INVITE_ALL_PARTY_MEMBERS: moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- SUCCEEDED) NO GAMERS RETURNED =", sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS])	
			ENDIF
		ELSE
			NETWORK_CLEAR_GET_GAMER_STATUS()
			sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS] = 2
			PRINTLN("MAINTAIN_INVITE_ALL_PARTY_MEMBERS: moving to stage (NETWORK_DID_GET_GAMER_STATUS_SUCCEED- FAILED) =", sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS])	
		ENDIF
	ELSE
		PRINTLN("MAINTAIN_INVITE_ALL_PARTY_MEMBERS: waiting for NETWORK_IS_GETTING_GAMER_STATUS()")	
	ENDIF 
  ENDPROC
  
PROC MAINTAIN_INVITE_ALL_PARTY_MEMBERS()
	
	IF IS_PLAYER_ONLINE()
		SWITCH sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS]
			CASE 0
				MAINTAIN_INVITE_ALL_PARTY_MEMBERS_SETUP()
			BREAK
			
			CASE 1
				MAINTAIN_INVITE_ALL_PARTY_MEMBERS_RESULTS()
			BREAK
			
			CASE 2
				sendInviteDetailsStruct.iNumExpectedResults[SECTION_PARTY_MEMBERS] = 0
				sendInviteDetailsStruct.iStage[SECTION_PARTY_MEMBERS] = 0
				CLEAR_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_PARTY_MEMBERS)
			BREAK
		
		ENDSWITCH
	ENDIF
ENDPROC

PROC CLEANUP_ANY_ONGOING_INVITES()
	//PRINTLN("CLEANUP_ANY_ONGOING_INVITES() called.")
	NETWORK_CLEAR_GET_GAMER_STATUS()
	NETWORK_CLEAR_FOUND_GAMERS()
	sendInviteDetailsStruct.iType = 0
	sendInviteDetailsStruct.iCreatorID = 0
	sendInviteDetailsStruct.vCoronaLocation = <<0,0,0>>
	sendInviteDetailsStruct.sContentID = ""
	sendInviteDetailsStruct.iBS = 0
	INT i
	REPEAT SECTION_INVITE_MAX i
		sendInviteDetailsStruct.iStage[i] = 0
		sendInviteDetailsStruct.iNumExpectedResults[i] = 0
	ENDREPEAT
	sendInviteDetailsStruct.iCrewIDToInvite = 0
	CLEAR_MATCHED_GAMERS_STRUCT()
	PRINTLN("CLEANUP_ANY_ONGOING_INVITES() finished.")
ENDPROC

PROC MAINTAIN_CORONA_GROUP_INVITES()
	BOOL bInviteCalledThisFrame
	IF crossSessionInvite.iStage > 0
		INVITE_A_CROSS_SESSION_PLAYER(crossSessionInvite.gamerToInvite)
		bInviteCalledThisFrame = TRUE
		IF sendInviteDetailsStruct.iBS = 0
			SEND_GROUPED_PLAYER_INVITE(GROUPED_PLAYER_INVITE_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_CORONA_GROUP_INVITES" #ENDIF)
			SEND_GROUPED_PLAYER_INVITE(GROUPED_PLAYER_INVITE_NON_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_CORONA_GROUP_INVITES" #ENDIF)
		ENDIF
	ELSE
		IF IS_BIT_SET(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_LAST_JOB_PLAYERS)
			MAINTAIN_INVITE_ALL_LAST_JOB_PLAYERS()
			bInviteCalledThisFrame = TRUE
		ELIF IS_BIT_SET(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_LAST_HEIST_PLAYERS)
			MAINTAIN_INVITE_TO_ALL_HEIST_JOB_PLAYERS()
			bInviteCalledThisFrame = TRUE
		ELIF IS_BIT_SET(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_MATCHED_CREW)
			MAINTAIN_INVITE_ALL_CREW(sendInviteDetailsStruct.iCrewIDToInvite)
			bInviteCalledThisFrame = TRUE
		ELIF IS_BIT_SET(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_MATCHED_PLAYERS)
			MAINTAIN_INVITE_ALL_MATCHED_PLAYERS()
			bInviteCalledThisFrame = TRUE
		ELIF IS_BIT_SET(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_FRIENDS)
			MAINTAIN_INVITE_ALL_FRIENDS()
			bInviteCalledThisFrame = TRUE
		ELIF IS_BIT_SET(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_PARTY_MEMBERS)
			MAINTAIN_INVITE_ALL_PARTY_MEMBERS()
			bInviteCalledThisFrame = TRUE
		ENDIF
	ENDIF
	
	IF IS_THIS_PLAYER_FULLY_ACTIVE_IN_CORONA(PLAYER_ID())
		FIND_MATCHED_GAMERS()
		IF sendInviteDetailsStruct.iBS = 0
		AND NOT bInviteCalledThisFrame
			IF IS_CORONA_BIT_SET(CORONA_WAIT_FOR_GROUP_INVITES_TO_COMPLETE)
				PRINTLN("MAINTAIN_CORONA_GROUP_INVITES - SET_TRANSITION_SESSION_PLAYER_DATA_REFRESH")
				SEND_GROUPED_PLAYER_INVITE(GROUPED_PLAYER_INVITE_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_CORONA_GROUP_INVITES" #ENDIF)
				SEND_GROUPED_PLAYER_INVITE(GROUPED_PLAYER_INVITE_NON_TRANSITION #IF IS_DEBUG_BUILD, "MAINTAIN_CORONA_GROUP_INVITES" #ENDIF)
				SET_TRANSITION_SESSION_PLAYER_DATA_REFRESH()
				CLEAR_CORONA_BIT(CORONA_WAIT_FOR_GROUP_INVITES_TO_COMPLETE)
			ENDIF
		ELSE
			IF NOT IS_CORONA_BIT_SET(CORONA_WAIT_FOR_GROUP_INVITES_TO_COMPLETE)
				SET_CORONA_BIT(CORONA_WAIT_FOR_GROUP_INVITES_TO_COMPLETE)
			ENDIF
		ENDIF
		
		// Listen out for the host requesting friends of friends invitess
		IF IS_CORONA_BIT_SET(CORONA_HOST_INVITE_FRIENDS_OF_FRIENDS)
			PRINTLN("MAINTAIN_CORONA_GROUP_INVITES - CORONA_HOST_INVITE_FRIENDS_OF_FRIENDS - set bit")
			SET_BIT(sendInviteDetailsStruct.iBS, SEND_INVITE_BS_TO_FRIENDS)
			CLEAR_CORONA_BIT(CORONA_HOST_INVITE_FRIENDS_OF_FRIENDS)
		ENDIF
	ENDIF
ENDPROC


PROC PROCESS_EVENT_NETWORK_PRESENCE_INVITE(INT iCount)
	STRUCT_INVITE_VIA_PRESENCE_EVENT sei

	//NET_PRINT("PROCESS_EVENT_NETWORK_PRESENCE_INVITE - Received event")NET_NL()

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
		
		//PRINTLN("PROCESS_EVENT_NETWORK_PRESENCE_INVITE  - Successfully got event data")
		PRINTLN("PROCESS_EVENT_NETWORK_PRESENCE_INVITE inviter = ", sei.szInviter, "szContentID = ",sei.szContentID,
												"playlistLength = ",sei.nPlaylistLength,
												"playlistCurrent = ",sei.nPlaylistCurrent,
												"SCTV  = ",sei.bScTv)
		
			//SET_INVITED_TO_SCTV(sei.bScTv)
			
			// This is an invite to a tournament, so auto-accept it
			INT inviteIndex = NETWORK_GET_PRESENCE_INVITE_INDEX_BY_ID(sei.nInviteID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP: PROCESS_EVENT_NETWORK_PRESENCE_INVITE - Received Invite the Invite Index: ")
				NET_PRINT_INT(inviteIndex)
				NET_NL()
			#ENDIF
			
			
			
			// Keith 29/7/13: If the playlist length is 100 then this is an invite to a tournament that should be auto-accepted
			//IF (sei.nPlaylistLength = 100) replaced by commands below
			// Keith 23/9/13: As per comment by Dan in bug 1643026, before accepting invite ensuire it is still valid
			IF (inviteIndex >= 0)
			AND (inviteIndex < NETWORK_GET_NUM_PRESENCE_INVITES())
				IF NETWORK_GET_PRESENCE_INVITE_FROM_ADMIN(inviteIndex)
				OR NETWORK_GET_PRESENCE_INVITE_IS_TOURNAMENT(inviteIndex)
					//If it's a tournament then set that we need to launch that type
					IF NETWORK_GET_PRESENCE_INVITE_IS_TOURNAMENT(inviteIndex)
						//Deal with accepting the tournament invite
						SET_UP_NETWORK_ACCEPT_PRESENCE_INVITE(inviteIndex)
						
					//Not a tournament invite
					ELSE
						NETWORK_ACCEPT_PRESENCE_INVITE(inviteIndex)					
					ENDIF
					
					SET_ENTERED_GAME_AS_SCTV(sei.bScTv)
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("...KGM MP: PROCESS_EVENT_NETWORK_PRESENCE_INVITE - The Tournament Invite has been auto-accepted. Invite Index: ")
						NET_PRINT_INT(inviteIndex)
						NET_NL()
					#ENDIF
				ELSE
					IF SHOULD_PRESENCE_INVITE_BE_PROCESSED()
					 	//1313096 Details to be added here Keith
						// Ignoring the return value
						Store_Cross_Session_Invite_Details_For_Joblist(sei.szContentID,sei.szInviter,sei.nInviteID,sei.hInviter,sei.nPlaylistLength,sei.nPlaylistCurrent)
					ELSE
						NET_PRINT("SHOULD_PRESENCE_INVITE_BE_PROCESSED = FALSE") NET_NL()
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP: PROCESS_EVENT_NETWORK_PRESENCE_INVITE - IGNORING CROSS-SESSION INVITE: Invite Index is no longer valid: ")
					NET_PRINT_INT(inviteIndex)
					NET_PRINT(" [NUM INVITES: ")
					NET_PRINT_INT(NETWORK_GET_NUM_PRESENCE_INVITES())
					NET_PRINT("]")
					NET_NL()
				#ENDIF
			ENDIF

		
	ELSE
		NET_SCRIPT_ASSERT("PROCESS_EVENT_NETWORK_PRESENCE_INVITE  - could not get event data!")
	ENDIF
ENDPROC







//EOF
