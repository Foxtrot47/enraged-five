// Header file for the short missions that the player gets given if they haven't done anything for a while
// Dave W

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
//USING "script_player.sch"
//USING "commands_path.sch"
//USING "building_control_public.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_mission.sch"

USING "net_realty_new.sch"

CONST_INT MAX_TIME_NO_JOB		600000 // 




FUNC BOOL SHOULD_SMALL_MISSION_LAUNCH(SCRIPT_TIMER& timeLaunch)
	#IF IS_DEBUG_BUILD
		IF g_bLaunchAmbMiss // 'm' mission screen
			g_bLaunchAmbMiss = FALSE
			#IF IS_DEBUG_BUILD PRINTLN("[amMissionLaunch] [dsw]  [SHOULD_SMALL_MISSION_LAUNCH] True as g_bLaunchAmbMiss") #ENDIF
			RETURN TRUE
		ENDIF
	#ENDIF
	
	
	IF HAS_FM_CAR_MOD_TUT_BEEN_DONE()
		IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_SECURITY_VAN)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
				AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT)
				AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RACETOPOINT)
				AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_HOLDUP)
				AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
				AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()	
				AND NOT NETWORK_IS_IN_TUTORIAL_SESSION()
				AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
				AND NOT MPGlobals.bStartedMPCutscene
				#IF IS_DEBUG_BUILD
				AND NOT g_bDemo	
				#ENDIF
					IF NOT HAS_NET_TIMER_STARTED(timeLaunch)
						IF DONE_UNLOCK_PHONECALL_FOR_ACTIVITY(FMMC_TYPE_SECURITY_VAN)
							START_NET_TIMER(timeLaunch)
						ENDIF
					ELSE	
						IF HAS_NET_TIMER_EXPIRED(timeLaunch, MAX_TIME_NO_JOB)
							#IF IS_DEBUG_BUILD PRINTLN("[amMissionLaunch] [dsw]  [SHOULD_SMALL_MISSION_LAUNCH] True as MAX_TIME_NO_JOB expired") #ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(timeLaunch)
						IF MPGlobals.bStartedMPCutscene
							RESET_NET_TIMER(timeLaunch)
							PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] resetting timeLaunch as time Started, but player watching a cutscene")
						ENDIF
						
						IF HAS_NET_TIMER_EXPIRED(timeLaunch, MAX_TIME_NO_JOB)
							#IF IS_DEBUG_BUILD 
								PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] resetting timeLaunch as time expired, but player on an ambient mission") 
								PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] On ambient mission...")
								IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
									PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] Cinema ")
								ELIF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT)
									PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] MPAM_TYPE_GANGHIDEOUT ")
								ELIF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RACETOPOINT)
									PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] MPAM_TYPE_RACETOPOINT ")
								ELIF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_HOLDUP)
									PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] MPAM_TYPE_HOLDUP ")
								ELIF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_SECURITYVAN)
									PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] MPAM_TYPE_SECURITYVAN ")
								ELIF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
									PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] MPAM_TYPE_STRIPCLUB ")
								ENDIF
								
								PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] ... done on ambient mission")
								
								PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] In property check...")
								IF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
									PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] Player in property")
								ENDIF
								PRINTLN("[amMissionLaunch] [dsw] [SHOULD_SMALL_MISSION_LAUNCH] ...done in property check.")
							#ENDIF
							RESET_NET_TIMER(timeLaunch)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
/*	
	IF NETWORK_IS_SCRIPT_ACTIVE("am_mission_launch", DEFAULT_MISSION_INSTANCE)
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("am_mission_launch")) <= 0
			#IF IS_DEBUG_BUILD PRINTLN("[dsw] SHOULD_SMALL_MISSION_LAUNCH] True as NETWORK_IS_SCRIPT_ACTIVE") #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF */
	RETURN FALSE
ENDFUNC

FUNC BOOL HOST_TAKE_CONTROL_OF_NETWORK_ID(NETWORK_INDEX niEntity)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niEntity)
		RETURN FALSE
	ENDIF
	
	IF TAKE_CONTROL_OF_NET_ID(niEntity)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_OWNERSHIP_OF_NETWORK_ID(NETWORK_INDEX niEntity)

	IF IS_NETWORK_ID_OWNED_BY_PARTICIPANT(niEntity)
		RETURN TRUE
	ENDIF
	
	IF HOST_TAKE_CONTROL_OF_NETWORK_ID(niEntity)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
	

PROC MAINTAIN_SMALL_MISSION_LAUNCH(SCRIPT_TIMER& timeLaunch)
	// Also see MAINTAIN_AMMO_DROP_CLIENT
	
	IF NOT MPGlobalsAmbience.bLaunchSmallMission
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("am_mission_launch")) <= 0
			IF SHOULD_SMALL_MISSION_LAUNCH(timeLaunch)
				MPGlobalsAmbience.bLaunchSmallMission = TRUE
				#IF IS_DEBUG_BUILD PRINTLN("[dsw] [MAINTAIN_SMALL_MISSION_LAUNCH] MPGlobalsAmbience.bLaunchSmallMission = TRUE") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF MPGlobalsAmbience.bLaunchSmallMission
		
		MP_MISSION_DATA smallMissionToLaunch	
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
				IF ARE_NETWORK_PEDS_AVAILABLE_FOR_SCRIPT_LAUNCH(1, FALSE, TRUE)
					IF ARE_NETWORK_VEHICLES_AVAILABLE_FOR_SCRIPT_LAUNCH(1, FALSE, TRUE)
						smallMissionToLaunch.mdID.idMission = eAM_SMALL_MISSION
						smallMissionToLaunch.iInstanceId = DEFAULT_MISSION_INSTANCE
						
						IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(smallMissionToLaunch)
							#IF IS_DEBUG_BUILD NET_PRINT_STRING_INT("[dsw] [MAINTAIN_SMALL_MISSION_LAUNCH] Relaunched am_mission_launch not in tutorial instance is: ", smallMissionToLaunch.iInstanceId) NET_NL() #ENDIF
							RESET_NET_TIMER(timeLaunch)
							MPGlobalsAmbience.iAmMissionLastInstance = smallMissionToLaunch.iInstanceId 
							MPGlobalsAmbience.bLaunchSmallMission = FALSE
						ENDIF
					ELSE	
						RESET_NET_TIMER(timeLaunch)
						#IF IS_DEBUG_BUILD PRINTLN("[dsw] [MAINTAIN_SMALL_MISSION_LAUNCH] resetting timeLaunch as ARE_NETWORK_PEDS_AVAILABLE_FOR_SCRIPT_LAUNCH") #ENDIF
				
					ENDIF
				ELSE	
					RESET_NET_TIMER(timeLaunch)
					#IF IS_DEBUG_BUILD PRINTLN("[dsw] [MAINTAIN_SMALL_MISSION_LAUNCH] resetting timeLaunch as ARE_NETWORK_PEDS_AVAILABLE_FOR_SCRIPT_LAUNCH()") #ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF 
	
ENDPROC


