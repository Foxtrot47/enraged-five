
USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_DEBUG_BUILD
PROC SET_UP_FMMC_GLOBAL_WIDGETS()
	START_WIDGET_GROUP( "FMMC Globals")  
		INT i
		TEXT_LABEL_31 tl15
		ADD_WIDGET_STRING("Entity Numbers")
		ADD_WIDGET_INT_SLIDER("iNumberOfPeds", g_FMMC_STRUCT_ENTITIES.iNumberOfPeds, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("iNumberOfSpawns", g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("iNumberOfVehicles", g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("iNumberOfWeapons", g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons, 0, 99, 1)
		//ADD_WIDGET_INT_SLIDER("iNumberOfObject", g_FMMC_STRUCT_ENTITIES.iNumberOfObjects, 0, 99, 1)	
				
		ADD_WIDGET_INT_SLIDER		("iMissionType",  g_FMMC_STRUCT.iMissionType, -1, 99, 1)		
		ADD_WIDGET_INT_SLIDER		("iNumParticipants",  g_FMMC_STRUCT.iNumParticipants, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER		("iRespawnTime" ,  g_FMMC_STRUCT.iRespawnTime, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER		("iRoundTime",  g_FMMC_STRUCT.iRoundTime, 0, 99, 1)
		//ADD_WIDGET_INT_SLIDER		("iNumberOfRounds",  g_FMMC_STRUCT.iNumberOfRounds, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER		("iTargetScore",  g_FMMC_STRUCT.iTargetScore, 0, 99, 1)
		//ADD_WIDGET_INT_SLIDER		("iNumberOfKills",  g_FMMC_STRUCT.iNumberOfKills, 0, 99, 1)
		//ADD_WIDGET_INT_SLIDER		("iNumberOfDeaths",  g_FMMC_STRUCT.iNumberOfDeaths, 0, 99, 1)
		//ADD_WIDGET_INT_SLIDER		("iObjectRule",  g_FMMC_STRUCT.iObjectRule, 0, 99, 1)
		//ADD_WIDGET_INT_SLIDER		("iVehicleRule",  g_FMMC_STRUCT.iVehicleRule, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER		("iPedRule",  g_FMMC_STRUCT.iDMPedRule, 0, 99, 1)
		//ADD_WIDGET_INT_SLIDER		("iFirstToLocationRule",  g_FMMC_STRUCT.iFirstToLocationRule, 0, 99, 1)
		//ADD_WIDGET_INT_SLIDER		("iLastManStandingRule",  g_FMMC_STRUCT.iLastManStandingRule, 0, 99, 1)
		//ADD_WIDGET_INT_SLIDER		("iTargetPedsRule",  g_FMMC_STRUCT.iTargetPedsRule, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER		("iBlips",  g_FMMC_STRUCT.iBlips, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER		("iTags",  g_FMMC_STRUCT.iTags, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER		("iVoice",  g_FMMC_STRUCT.iVoice, 0, 99, 1)
		ADD_WIDGET_VECTOR_SLIDER	("vStartPos",  g_FMMC_STRUCT.vStartPos, -9999.0, 9999.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER		("fStartRadius",  g_FMMC_STRUCT.fStartRadius, 0, 9999.0, 0.001)
		ADD_WIDGET_INT_SLIDER		("iNumberOfCheckPoints",  g_FMMC_STRUCT.iNumberOfCheckPoints, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER		("iNumLaps",  g_FMMC_STRUCT.iNumLaps, 0, 99, 1)
		ADD_WIDGET_FLOAT_SLIDER		("fRaceStartHeading",  g_FMMC_STRUCT.fRaceStartHeading, -360.0, 360.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER		("fLR_GridSpacing",  g_FMMC_STRUCT.fLR_GridSpacing, 0, 9999.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER		("fUD_GridSpacing",  g_FMMC_STRUCT.fUD_GridSpacing, 0, 9999.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER		("fGridWidth",  g_FMMC_STRUCT.fGridWidth, 0, 9999.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER		("fGridLength",  g_FMMC_STRUCT.fGridLength, 0, 9999.0, 0.001)
		ADD_WIDGET_INT_SLIDER		("iPolice",  g_FMMC_STRUCT.iPolice, 0, 99, 1)
		ADD_WIDGET_BOOL				("bParticlesOff",  g_FMMC_STRUCT.bParticlesOff)
		START_WIDGET_GROUP( "Objects")  
			ADD_WIDGET_INT_SLIDER		("iNumberOfObjects", g_FMMC_STRUCT_ENTITIES.iNumberOfObjects, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER		("iObjectPallet",  g_FMMC_STRUCT_ENTITIES.iObjectPallet, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER		("iObjectRespawnTime",  g_FMMC_STRUCT_ENTITIES.iObjectRespawnTime, 0, 99, 1)
			FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS - 1)
				tl15 = "OBJ - "
				tl15 += i
				START_WIDGET_GROUP(tl15)  
					ADD_WIDGET_VECTOR_SLIDER("pos", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos, -9999.0, 9999.0, 0.01)
					ADD_WIDGET_FLOAT_READ_ONLY("fHead", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fHead)
				STOP_WIDGET_GROUP()
			ENDFOR	
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP( "Weapons")  
			ADD_WIDGET_INT_SLIDER		("iNumberOfWeapons", g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER		("iPlaceMentType",  g_FMMC_STRUCT_ENTITIES.iLegacyPlacementType, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER		("iWeaponRespawnTime",  g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime, -1, 99, 1)
			IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_RACE //this is stored with enum to int in races.
				ADD_WIDGET_INT_SLIDER		("iWeaponPallet",  g_FMMC_STRUCT_ENTITIES.iWeaponPallet, 0, 99, 1)
			ENDIF
			FOR i = 0 TO (FMMC_MAX_WEAPONS - 1)
				tl15 = "WEP - "
				tl15 += i
				START_WIDGET_GROUP(tl15)  
					ADD_WIDGET_VECTOR_SLIDER("pos", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos, -9999.0, 9999.0, 0.01)
					ADD_WIDGET_VECTOR_SLIDER("rot", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vRot, -360.0, 360.0, 0.01)
				STOP_WIDGET_GROUP()
			ENDFOR	
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP( "Vehicles")  
			ADD_WIDGET_INT_SLIDER		("iVehicleRespawnTime",  g_FMMC_STRUCT_ENTITIES.iVehicleRespawnTime	, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER		("iVehiclePallet",  g_FMMC_STRUCT_ENTITIES.iVehiclePallet, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER		("iNumberOfVehicles", g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles, 0, 99, 1)			
			FOR i = 0 TO (FMMC_MAX_VEHICLES - 1)
				tl15 = "VEH - "
				tl15 += i
				START_WIDGET_GROUP(tl15)  
					ADD_WIDGET_VECTOR_SLIDER("pos", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, -9999.0, 9999.0, 0.01)
					ADD_WIDGET_FLOAT_READ_ONLY("fHead", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fHead)
					ADD_WIDGET_INT_READ_ONLY("iHealth", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iHealth)
				STOP_WIDGET_GROUP()
			ENDFOR	
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP( "PEDs")  
			ADD_WIDGET_INT_SLIDER		("iNumberOfPeds", g_FMMC_STRUCT_ENTITIES.iNumberOfPeds, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER		("iPedRespawnTime",  g_FMMC_STRUCT_ENTITIES.iPedRespawnTime, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER		("iPedPallet",  g_FMMC_STRUCT_ENTITIES.iPedPallet, 0, 99, 1)
			FOR i = 0 TO (FMMC_MAX_PEDS - 1)
				tl15 = "PED - "
				tl15 += i
				START_WIDGET_GROUP(tl15)  
					ADD_WIDGET_VECTOR_SLIDER	("pos", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos, -9999.0, 9999.0, 0.01)
					ADD_WIDGET_FLOAT_READ_ONLY	("fHead", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fHead)
					ADD_WIDGET_FLOAT_READ_ONLY	("sCurentVarsStruct.fRange", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fRange)
					ADD_WIDGET_INT_SLIDER		("iTeam", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iTeam[0], -1, 10, 1)
				STOP_WIDGET_GROUP()
			ENDFOR	
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP( "Spawns")  
			ADD_WIDGET_INT_SLIDER		("iNumberOfSpawnPoints", g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER		("iPedRespawnTime",  g_FMMC_STRUCT_ENTITIES.iPedRespawnTime, 0, 99, 1)
			FOR i = 0 TO (FMMC_MAX_SPAWNPOINTS - 1)
				tl15 = "Spawn - "
				tl15 += i
				START_WIDGET_GROUP(tl15)  
					ADD_WIDGET_VECTOR_SLIDER	("pos", g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, -9999.0, 9999.0, 0.01)
					ADD_WIDGET_FLOAT_READ_ONLY	("fHead", g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].fHead)
					ADD_WIDGET_INT_SLIDER		("iTeam", g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].iTeam, -1, 10, 1)
				STOP_WIDGET_GROUP()
			ENDFOR	
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP( "Checkpoints")  
			ADD_WIDGET_INT_SLIDER		("iNumberOfCheckPoints", g_FMMC_STRUCT.iNumberOfCheckPoints, 0, 99, 1)
			FOR i = 0 TO (FMMC_MAX_NUM_CHECKPOINTS - 1)
				tl15 = "Checkpoints - "
				tl15 += i
				START_WIDGET_GROUP(tl15)  
					ADD_WIDGET_VECTOR_SLIDER	("pos", g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint, -9999.0, 9999.0, 0.01)
				STOP_WIDGET_GROUP()
			ENDFOR	
		STOP_WIDGET_GROUP()
		//ADD_WIDGET_VECTOR_SLIDER	("vSpawnArea",  g_FMMC_STRUCT.vSpawnArea, -9999.0, 9999.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER		("fRadius",  g_FMMC_STRUCT.fRadius, 0, 9999.0, 0.001)
	STOP_WIDGET_GROUP()
ENDPROC

PROC START_XML_FILE(STRING Name, STRING path, STRING file)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<?xml version='1.0'?>", path, file)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
	TEXT_LABEL_31 tl31 = "<"
	tl31 += Name
	tl31 += ">"
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tl31, path, file)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)	
ENDPROC
PROC STOP_XML_FILE(STRING Name, STRING path, STRING file)
	TEXT_LABEL_31 tl31 = "</"
	tl31 += Name
	tl31 += ">"
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tl31, path, file)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
ENDPROC
PROC START_XML_BLOCK(STRING Name, STRING path, STRING file)
	TEXT_LABEL_31 tl31 = "<"
	tl31 += Name
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tl31,path, file)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
ENDPROC

PROC END_XML_BLOCK(STRING path, STRING file)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("/>",path, file)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
ENDPROC

PROC SAVE_INT_TO_XML_FILE(STRING nodeName, INT iINTtoSave, STRING path, STRING file)
	TEXT_LABEL_31 tl31 = " "
	tl31 += nodeName
	tl31 += "=\""
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tl31, path, file)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iINTtoSave, path, file)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\"",path, file)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
ENDPROC

PROC SAVE_FLOAT_TO_XML_FILE(STRING nodeName, FLOAT fFloatToSave, STRING path, STRING file)
	TEXT_LABEL_31 tl31 = " "
	tl31 += nodeName
	tl31 += "=\""
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tl31, path, file)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(fFloatToSave, path, file)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\"",path, file)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
ENDPROC
PROC SAVE_VECTOR_TO_XML_FILE(STRING nodeName, VECTOR iVECTORtoSave, STRING path, STRING file)
	TEXT_LABEL_31 tl31 = "<"
	tl31 += nodeName
	tl31 += " "
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tl31, path, file)
	SAVE_STRING_TO_NAMED_DEBUG_FILE( "\" x=\"",path, file)
    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(iVECTORtoSave.x, path, file)
    SAVE_STRING_TO_NAMED_DEBUG_FILE("\" y=\"",path, file)
    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(iVECTORtoSave.y, path, file)
    SAVE_STRING_TO_NAMED_DEBUG_FILE("\" z=\"",path, file)
    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(iVECTORtoSave.z, path, file)
    SAVE_STRING_TO_NAMED_DEBUG_FILE("\"/>",path, file)
ENDPROC

PROC SAVE_INT_FROM_ARRAY_TO_XML_FILE(STRING nodeName, INT iIntToSave, STRING path, STRING file)
	TEXT_LABEL_31 tl31
	tl31 = " "
	tl31 += nodeName
	tl31 += "=\""
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tl31, path, file)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iIntToSave, path, file)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\"",path, file)
	
ENDPROC

PROC SAVE_FLOAT_FROM_ARRAY_TO_XML_FILE(STRING nodeName, FLOAT fFloatToSave, STRING path, STRING file)
	TEXT_LABEL_31 tl31
	tl31 = " "
	tl31 += nodeName
	tl31 += "=\""
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tl31, path, file)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(fFloatToSave, path, file)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\"",path, file)
ENDPROC

PROC SAVE_VECTOR_FROM_ARRAY_TO_XML_FILE(VECTOR iVECTORtoSave, STRING path, STRING file)
	SAVE_STRING_TO_NAMED_DEBUG_FILE( " x=\"",path, file)
    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(iVECTORtoSave.x, path, file)
    SAVE_STRING_TO_NAMED_DEBUG_FILE("\" y=\"",path, file)
    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(iVECTORtoSave.y, path, file)
    SAVE_STRING_TO_NAMED_DEBUG_FILE("\" z=\"",path, file)
    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(iVECTORtoSave.z, path, file)
    SAVE_STRING_TO_NAMED_DEBUG_FILE("\"",path, file)
ENDPROC

PROC START_ARRAY_SAVE_TO_XML_FILE(STRING nodeName, INT iArrayPos, STRING path, STRING file)
	TEXT_LABEL_31 tl31 
	tl31 = "<"
	tl31 += nodeName
	tl31 += iArrayPos
	tl31 += " "
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tl31, path, file)
ENDPROC

PROC END_ARRAY_SAVE_TO_XML_FILE(STRING path, STRING file)
	SAVE_STRING_TO_NAMED_DEBUG_FILE( "/>",path, file)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
ENDPROC

CONST_INT ciSMALLEST_AMOUNT 0
CONST_INT ciLOOP_FROM 1
//Saves the data to the XML File
PROC SAVE_FM_MISSION_CREATOR_DATA(INT iFileToSave, BOOL bIsRace = FALSE, INT iForceRaceType = -1)
	
	TEXT_LABEL_63 file
	STRING path = ""
	file = "FMMC_"
	//file += GET_PLAYER_NAME(PLAYER_ID())
	IF bIsRace
		file += "R"
	ELSE
		file += "DM"
	ENDIF
	file += "_"
	file += iFileToSave
	file += ".xml"
	
	
	CLEAR_NAMED_DEBUG_FILE(path, file)
	OPEN_NAMED_DEBUG_FILE(path, file)
		// Add the initial xml data to the file
		
		START_XML_FILE("g_FMMC_STRUCT", path, file)
		INT i
		
		
		//Start Ints
		START_XML_BLOCK("INTS", path, file)
		IF iForceRaceType = -1
			SAVE_INT_TO_XML_FILE("iRaceType", 				g_FMMC_STRUCT.iRaceType, 			path, file)
		ELSE
			SAVE_INT_TO_XML_FILE("iRaceType", 				iForceRaceType, 			path, file)
		ENDIF
		SAVE_INT_TO_XML_FILE("iRaceMode", 				g_FMMC_STRUCT.iRaceMode, 			path, file)
		SAVE_INT_TO_XML_FILE("iMissionType", 			g_FMMC_STRUCT.iMissionType, 		path, file)
		SAVE_INT_TO_XML_FILE("iNumParticipants", 		g_FMMC_STRUCT.iNumParticipants, 	path, file)
		SAVE_INT_TO_XML_FILE("iMinNumParticipants", 	g_FMMC_STRUCT.iMinNumParticipants, 	path, file)
		SAVE_INT_TO_XML_FILE("iRespawnTime", 			g_FMMC_STRUCT.iRespawnTime, 		path, file)
		SAVE_INT_TO_XML_FILE("iRoundTime", 				g_FMMC_STRUCT.iRoundTime, 			path, file)
		//SAVE_INT_TO_XML_FILE("iNumberOfRounds", 		g_FMMC_STRUCT.iNumberOfRounds, 		path, file)
		SAVE_INT_TO_XML_FILE("iTargetScore", 			g_FMMC_STRUCT.iTargetScore, 		path, file)
		//SAVE_INT_TO_XML_FILE("iNumberOfKills", 			g_FMMC_STRUCT.iNumberOfKills, 		path, file)
		//SAVE_INT_TO_XML_FILE("iNumberOfDeaths", 		g_FMMC_STRUCT.iNumberOfDeaths, 		path, file)
		//SAVE_INT_TO_XML_FILE("iObjectRule",				g_FMMC_STRUCT.iObjectRule, 			path, file)
		//SAVE_INT_TO_XML_FILE("iVehicleRule", 			g_FMMC_STRUCT.iVehicleRule, 		path, file)
		SAVE_INT_TO_XML_FILE("iPedRule", 				g_FMMC_STRUCT.iDMPedRule, 			path, file)
		//SAVE_INT_TO_XML_FILE("iFirstToLocationRule", 	g_FMMC_STRUCT.iFirstToLocationRule, path, file)
		//SAVE_INT_TO_XML_FILE("iLastManStandingRule", 	g_FMMC_STRUCT.iLastManStandingRule, path, file)
	//	SAVE_INT_TO_XML_FILE("iTargetPedsRule", 		g_FMMC_STRUCT.iTargetPedsRule, 		path, file)
		SAVE_INT_TO_XML_FILE("iBlips", 					g_FMMC_STRUCT.iBlips, 				path, file)
		SAVE_INT_TO_XML_FILE("iTags", 					g_FMMC_STRUCT.iTags,				path, file)
		SAVE_INT_TO_XML_FILE("iVoice", 					g_FMMC_STRUCT.iVoice, 				path, file)
		SAVE_INT_TO_XML_FILE("iRadio", 					g_FMMC_STRUCT.iRadio, 				path, file)
		SAVE_INT_TO_XML_FILE("iAutoAim", 				g_FMMC_STRUCT.iAutoAim, 			path, file)
		SAVE_INT_TO_XML_FILE("iNumberOfCheckPoints", 	g_FMMC_STRUCT.iNumberOfCheckPoints, path, file)
		SAVE_INT_TO_XML_FILE("iNumLaps", 				g_FMMC_STRUCT.iNumLaps, 			path, file)
		SAVE_INT_TO_XML_FILE("iTraffic", 				g_FMMC_STRUCT.iTraffic, 			path, file)
		SAVE_INT_TO_XML_FILE("iNumberOfLanes", 			g_FMMC_STRUCT.iNumberOfLanes, 		path, file)
		SAVE_INT_TO_XML_FILE("iPolice", 				g_FMMC_STRUCT.iPolice, 				path, file)
		SAVE_INT_TO_XML_FILE("iNumberOfObjects", 		g_FMMC_STRUCT_ENTITIES.iNumberOfObjects, 	path, file)
		SAVE_INT_TO_XML_FILE("iObjectPallet", 			g_FMMC_STRUCT_ENTITIES.iObjectPallet, 		path, file)
		SAVE_INT_TO_XML_FILE("iObjectRespawnTime", 		g_FMMC_STRUCT_ENTITIES.iObjectRespawnTime, 	path, file)
//		SAVE_INT_TO_XML_FILE("iNumberOfAnimals", 		g_FMMC_STRUCT.iNumberOfAnimals, 	path, file)
//		SAVE_INT_TO_XML_FILE("iAnimalPallet", 			g_FMMC_STRUCT.iAnimalPallet, 		path, file)
//		SAVE_INT_TO_XML_FILE("iAnimalRespawnTime", 		g_FMMC_STRUCT.iAnimalRespawnTime, 	path, file)
		SAVE_INT_TO_XML_FILE("iNumberOfWeapons", 		g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons, 	path, file)
		SAVE_INT_TO_XML_FILE("iPlaceMentType", 			g_FMMC_STRUCT_ENTITIES.iLegacyPlacementType, 		path, file)
		SAVE_INT_TO_XML_FILE("iWeaponRespawnTime", 		g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime,	path, file)
		SAVE_INT_TO_XML_FILE("iWeaponPallet", 			g_FMMC_STRUCT_ENTITIES.iWeaponPallet, 		path, file)
		SAVE_INT_TO_XML_FILE("iNumberOfVehicles", 		g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles, 	path, file)
		SAVE_INT_TO_XML_FILE("iVehicleRespawnTime", 	g_FMMC_STRUCT_ENTITIES.iVehicleRespawnTime, 	path, file)
		SAVE_INT_TO_XML_FILE("iVehiclePallet", 			g_FMMC_STRUCT_ENTITIES.iVehiclePallet, 		path, file)
		SAVE_INT_TO_XML_FILE("iNumberOfPeds", 			g_FMMC_STRUCT_ENTITIES.iNumberOfPeds, 		path, file)
		SAVE_INT_TO_XML_FILE("iPedRespawnTime", 		g_FMMC_STRUCT_ENTITIES.iPedRespawnTime, 		path, file)
		SAVE_INT_TO_XML_FILE("iPedPallet", 				g_FMMC_STRUCT_ENTITIES.iPedPallet,	 		path, file)
		SAVE_INT_TO_XML_FILE("iNumberOfSj", 			g_FMMC_STRUCT.iNumberOfSj, 			path, file)
		SAVE_INT_TO_XML_FILE("iVehicleBoostEnableDelay",g_FMMC_STRUCT.iVehicleBoostEnableDelay,	path, file)
		END_XML_BLOCK(path, file)
		
		
		START_XML_BLOCK("FLOATS", path, file)
		SAVE_FLOAT_TO_XML_FILE("fStartRadius", 			g_FMMC_STRUCT.fStartRadius, 		path, file)
		SAVE_FLOAT_TO_XML_FILE("fRadius", 				g_FMMC_STRUCT.fRadius, 				path, file)
		SAVE_FLOAT_TO_XML_FILE("fRaceStartHeading", 	g_FMMC_STRUCT.fRaceStartHeading, 	path, file)
		SAVE_FLOAT_TO_XML_FILE("fLR_GridSpacing", 		g_FMMC_STRUCT.fLR_GridSpacing, 		path, file)
		SAVE_FLOAT_TO_XML_FILE("fUD_GridSpacing", 		g_FMMC_STRUCT.fUD_GridSpacing, 		path, file)
		SAVE_FLOAT_TO_XML_FILE("fGridWidth", 			g_FMMC_STRUCT.fGridWidth, 			path, file)
		SAVE_FLOAT_TO_XML_FILE("fGridLength", 			g_FMMC_STRUCT.fGridLength, 			path, file)
		END_XML_BLOCK(path, file)
		
		FOR i = 0 TO (FMMC_MAX_NUM_RACERS -1 )
			START_ARRAY_SAVE_TO_XML_FILE("VehicleModelArray", i, path, file)	
			SAVE_INT_FROM_ARRAY_TO_XML_FILE("mnVehicleModel", ENUM_TO_INT(g_FMMC_STRUCT.mnVehicleModel[i]), 		path, file)
			END_ARRAY_SAVE_TO_XML_FILE(path, file)
		ENDFOR
		
		IF g_FMMC_STRUCT.iNumberOfCheckPoints > ciSMALLEST_AMOUNT
			FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfCheckPoints - ciLOOP_FROM)				
				START_ARRAY_SAVE_TO_XML_FILE("ChpArray", i, path, file)	
				SAVE_VECTOR_FROM_ARRAY_TO_XML_FILE(				 		g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint, path, file)
				SAVE_FLOAT_FROM_ARRAY_TO_XML_FILE("fCheckPointHeight",	g_FMMC_STRUCT.sPlacedCheckpoint[i].fCheckPointHeight, path, file)
				END_ARRAY_SAVE_TO_XML_FILE(path, file)
			ENDFOR
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfObjects > ciSMALLEST_AMOUNT
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - ciLOOP_FROM )
				START_ARRAY_SAVE_TO_XML_FILE("ObjArray", i, path, file)	
				SAVE_VECTOR_FROM_ARRAY_TO_XML_FILE(			 	g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos, 		path, file)
				SAVE_FLOAT_FROM_ARRAY_TO_XML_FILE("ObjHead", 	g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fHead, 		path, file)
				SAVE_INT_FROM_ARRAY_TO_XML_FILE("ObjMN",		ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn),path, file)
				SAVE_INT_FROM_ARRAY_TO_XML_FILE("ObjRule", 		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[0], 			path, file)
				//SAVE_INT_FROM_ARRAY_TO_XML_FILE("ObjStage",		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iStage, 		path, file)
				END_ARRAY_SAVE_TO_XML_FILE(path, file)
			ENDFOR		
		ENDIF
		
//		IF g_FMMC_STRUCT.iNumberOfAnimals > ciSMALLEST_AMOUNT
//			FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfAnimals - ciLOOP_FROM)
//				START_ARRAY_SAVE_TO_XML_FILE("AnmArray", i, path, file)	
//				SAVE_VECTOR_FROM_ARRAY_TO_XML_FILE(				g_FMMC_STRUCT.sPlacedAnimal[i].vPos, 		path, file)
//				SAVE_FLOAT_FROM_ARRAY_TO_XML_FILE("AnmHead", 	g_FMMC_STRUCT.sPlacedAnimal[i].fHead, 		path, file)
//				SAVE_INT_FROM_ARRAY_TO_XML_FILE("AnmMn",		ENUM_TO_INT(g_FMMC_STRUCT.sPlacedAnimal[i].mn),path, file)
//				SAVE_INT_FROM_ARRAY_TO_XML_FILE("AnmRule",		g_FMMC_STRUCT.sPlacedAnimal[i].iRule[0], 			path, file)
//				//SAVE_INT_FROM_ARRAY_TO_XML_FILE("AnmStage",		g_FMMC_STRUCT.sPlacedAnimal[i].iStage, 		path, file)
//				END_ARRAY_SAVE_TO_XML_FILE(path, file)	
//			ENDFOR		
//		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons > ciSMALLEST_AMOUNT
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - ciLOOP_FROM)
				START_ARRAY_SAVE_TO_XML_FILE("WepArray", i, path, file)	
				SAVE_VECTOR_FROM_ARRAY_TO_XML_FILE(				g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos, 		path, file)
				SAVE_FLOAT_FROM_ARRAY_TO_XML_FILE("WeaHead", 	g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vRot.z, 		path, file)
				SAVE_INT_FROM_ARRAY_TO_XML_FILE("WeaPT", 		ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt),path, file)
				END_ARRAY_SAVE_TO_XML_FILE(path, file)	
			ENDFOR		
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > ciSMALLEST_AMOUNT
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - ciLOOP_FROM)
				START_ARRAY_SAVE_TO_XML_FILE("VehArray", i, path, file)	
				SAVE_VECTOR_FROM_ARRAY_TO_XML_FILE(				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, 		path, file)
				SAVE_FLOAT_FROM_ARRAY_TO_XML_FILE("VehHead",	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fHead, 		path, file)
				SAVE_INT_FROM_ARRAY_TO_XML_FILE("VehMN", 		ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn),path, file)
				SAVE_INT_FROM_ARRAY_TO_XML_FILE("VehRule", 		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule[0], 			path, file)
				//SAVE_INT_FROM_ARRAY_TO_XML_FILE("VehStage", 	g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iStage, 		path, file)
				END_ARRAY_SAVE_TO_XML_FILE(path, file)	
			ENDFOR		
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > ciSMALLEST_AMOUNT
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - ciLOOP_FROM)
				START_ARRAY_SAVE_TO_XML_FILE("PedArray", i, path, file)	
				SAVE_VECTOR_FROM_ARRAY_TO_XML_FILE(				g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos, 		path, file)
				SAVE_FLOAT_FROM_ARRAY_TO_XML_FILE("PedHead",	g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fHead, 		path, file)
				SAVE_INT_FROM_ARRAY_TO_XML_FILE("PedMN", 		ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn),path, file)
				SAVE_INT_FROM_ARRAY_TO_XML_FILE("PedGun", 		ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].gun),path, file)
				SAVE_INT_FROM_ARRAY_TO_XML_FILE("PedSkill", 	ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].skill),path, file)
				SAVE_INT_FROM_ARRAY_TO_XML_FILE("PedTeam", 		g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iTeam[0], 			path, file)
				SAVE_INT_FROM_ARRAY_TO_XML_FILE("PedRule", 		g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule[0], 			path, file)
				//SAVE_INT_FROM_ARRAY_TO_XML_FILE("PedStage", 	g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStage, 		path, file)
				END_ARRAY_SAVE_TO_XML_FILE(path, file)	
			ENDFOR	
		ENDIF
		
		IF g_FMMC_STRUCT.iNumberOfSj > ciSMALLEST_AMOUNT
			FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfSj - ciLOOP_FROM)
				START_ARRAY_SAVE_TO_XML_FILE("UsjStart", i, path, file)	
				SAVE_VECTOR_FROM_ARRAY_TO_XML_FILE(	g_FMMC_STRUCT.sStuntJumps[i].vStartPos, 		path, file)
				END_ARRAY_SAVE_TO_XML_FILE(path, file)	
			ENDFOR		
			FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfSj - ciLOOP_FROM)
				START_ARRAY_SAVE_TO_XML_FILE("UsjEnd", i, path, file)	
				SAVE_VECTOR_FROM_ARRAY_TO_XML_FILE(	g_FMMC_STRUCT.sStuntJumps[i].vEndPos, 		path, file)
				END_ARRAY_SAVE_TO_XML_FILE(path, file)	
			ENDFOR		
			FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfSj - ciLOOP_FROM)
				START_ARRAY_SAVE_TO_XML_FILE("UsjCam", i, path, file)	
				SAVE_VECTOR_FROM_ARRAY_TO_XML_FILE(	g_FMMC_STRUCT.sStuntJumps[i].vCameraPos, 		path, file)
				END_ARRAY_SAVE_TO_XML_FILE(path, file)	
			ENDFOR		
		ENDIF
		
		// Close the race data tag
		STOP_XML_FILE("g_FMMC_STRUCT", path, file)
	CLOSE_DEBUG_FILE()	
	
	PRINTLN("-----------------------------------------------------------")
	PRINTLN("--                                                       --")
	PRINTLN("--                                                       --")
	PRINTLN("--                  SAVED FILE AS XML                    --")
	PRINTLN("--                                                       --")
	PRINTLN("--                                                       --")
	PRINTLN("-----------------------------------------------------------")
	
ENDPROC

FUNC BOOL LOAD_XML_DATA(INT iFileToLoad, BOOL bIsRace = FALSE)

	TEXT_LABEL_63 sXMLFile = "X:\xgta5\xbuild\xdev\xFMMC_"
	//sXMLFile += GET_PLAYER_NAME(PLAYER_ID())
	IF bIsRace
		sXMLFile += "R"
	ELSE
		sXMLFile += "DM"
	ENDIF
	sXMLFile += "_"
	sXMLFile += iFileToLoad
	sXMLFile += ".xml"
	TEXT_LABEL_31 nodeName
	// Load in xml file if it exists
	IF LOAD_XML_FILE(sXMLFile)
		CLEAR_FMMC_STRUCT()
		PRINTSTRING("LOAD_XML_DATA'")PRINTSTRING(sXMLFile)PRINTSTRING("'")PRINTNL()
		IF GET_NUMBER_OF_XML_NODES() <> 0
			PRINTSTRING("GET_NUMBER_OF_XML_NODES = ")PRINTINT(GET_NUMBER_OF_XML_NODES()) PRINTNL()
        	// Integers used for the FOR loops going through all the nodes and attributes
        	INT eachNode, eachAttribute	//	, iCurrentY, iCurrentX, iCurrentZ
			INT iNumberOfNodes, iNumberOfNodeAttributes, iCurrentNode
			STRING stCurrentName
        	// Loop through all the nodes
			iNumberOfNodes = GET_NUMBER_OF_XML_NODES()
        	PRINTLN(" iNumberOfNodes = ", iNumberOfNodes)
			//Go through all the nodes
			FOR eachNode = 0 TO (iNumberOfNodes-1)
				//Top level one
				IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), "g_FMMC_STRUCT")
					//Nothing
					PRINTLN("g_FMMC_STRUCT")
				
				//Go through all the INTS
				ELIF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), "INTS")
					PRINTLN("INTS")
					iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
					PRINTLN("iNumberOfNodeAttributes = ", iNumberOfNodeAttributes)
					//Go through the INT atributes
					FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
						stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
						IF ARE_STRINGS_EQUAL(stCurrentName, "iMissionType")
							g_FMMC_STRUCT.iMissionType = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumParticipants")
							g_FMMC_STRUCT.iNumParticipants = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iMinNumParticipants")
							g_FMMC_STRUCT.iMinNumParticipants = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iRespawnTime")
							g_FMMC_STRUCT.iRespawnTime = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iRoundTime")
							g_FMMC_STRUCT.iRoundTime = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						//ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumberOfRounds")
							//g_FMMC_STRUCT.iNumberOfRounds = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iTargetScore")
							g_FMMC_STRUCT.iTargetScore = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						//ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumberOfKills")
							//g_FMMC_STRUCT.iNumberOfKills = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						//ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumberOfDeaths")
							//g_FMMC_STRUCT.iNumberOfDeaths = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						//ELIF ARE_STRINGS_EQUAL(stCurrentName, "iObjectRule")
							//g_FMMC_STRUCT.iObjectRule = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						//ELIF ARE_STRINGS_EQUAL(stCurrentName, "iVehicleRule")
							//g_FMMC_STRUCT.iVehicleRule = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iPedRule")
							g_FMMC_STRUCT.iDMPedRule = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)							
						//ELIF ARE_STRINGS_EQUAL(stCurrentName, "iFirstToLocationRule")
							//g_FMMC_STRUCT.iFirstToLocationRule = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						//ELIF ARE_STRINGS_EQUAL(stCurrentName, "iLastManStandingRule")
							//g_FMMC_STRUCT.iLastManStandingRule = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						//ELIF ARE_STRINGS_EQUAL(stCurrentName, "iTargetPedsRule")
							//g_FMMC_STRUCT.iTargetPedsRule = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iBlips")
							g_FMMC_STRUCT.iBlips = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iTags")
							g_FMMC_STRUCT.iTags = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iVoice")
							g_FMMC_STRUCT.iVoice = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iRadio")
							g_FMMC_STRUCT.iRadio = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iAutoAim")
							g_FMMC_STRUCT.iAutoAim = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumberOfCheckPoints")
							g_FMMC_STRUCT.iNumberOfCheckPoints = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumLaps")
							g_FMMC_STRUCT.iNumLaps = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iRaceType")
							g_FMMC_STRUCT.iRaceType = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iRaceMode")
							g_FMMC_STRUCT.iRaceMode = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iTraffic")
							g_FMMC_STRUCT.iTraffic = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumberOfLanes")
							g_FMMC_STRUCT.iNumberOfLanes = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iPolice")
							g_FMMC_STRUCT.iPolice = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumberOfObjects")
							g_FMMC_STRUCT_ENTITIES.iNumberOfObjects = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iObjectPallet")
							g_FMMC_STRUCT_ENTITIES.iObjectPallet = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iObjectRespawnTime")
							g_FMMC_STRUCT_ENTITIES.iObjectRespawnTime = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
//						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumberOfAnimals")
//							g_FMMC_STRUCT.iNumberOfAnimals = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
//						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iAnimalPallet")
//							g_FMMC_STRUCT.iAnimalPallet = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
//						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iAnimalRespawnTime")
//							g_FMMC_STRUCT.iAnimalRespawnTime = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumberOfWeapons")
							g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iPlaceMentType")
							g_FMMC_STRUCT_ENTITIES.iLegacyPlacementType = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iWeaponRespawnTime")
							g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iWeaponPallet")
							g_FMMC_STRUCT_ENTITIES.iWeaponPallet = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumberOfVehicles")
							g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iVehicleRespawnTime")
							g_FMMC_STRUCT_ENTITIES.iVehicleRespawnTime = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iVehiclePallet")
							g_FMMC_STRUCT_ENTITIES.iVehiclePallet = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumberOfPeds")
							g_FMMC_STRUCT_ENTITIES.iNumberOfPeds = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iPedRespawnTime")
							g_FMMC_STRUCT_ENTITIES.iPedRespawnTime = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iPedPallet")
							g_FMMC_STRUCT_ENTITIES.iPedPallet = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iNumberOfSj")
							g_FMMC_STRUCT.iNumberOfSj = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "iVehicleBoostEnableDelay")
							g_FMMC_STRUCT.iVehicleBoostEnableDelay = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELSE
							PRINTLN("stCurrentName = ", stCurrentName )
						ENDIF
					ENDFOR
				//Go through all the INTS
				ELIF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), "FLOATS")
					iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
					PRINTLN("FLOATS")
					PRINTLN("iNumberOfNodeAttributes = ", iNumberOfNodeAttributes)
					//Go through the INT atributes
					FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
						stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
						IF ARE_STRINGS_EQUAL(stCurrentName, "fStartRadius")
							g_FMMC_STRUCT.fStartRadius = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "fRadius")
							g_FMMC_STRUCT.fRadius = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "fRaceStartHeading")
							g_FMMC_STRUCT.fRaceStartHeading = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "fLR_GridSpacing")
							g_FMMC_STRUCT.fLR_GridSpacing = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "fUD_GridSpacing")
							g_FMMC_STRUCT.fUD_GridSpacing = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "fGridWidth")
							g_FMMC_STRUCT.fGridWidth = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELIF ARE_STRINGS_EQUAL(stCurrentName, "fGridLength")
							g_FMMC_STRUCT.fGridLength = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						ELSE
							PRINTLN("stCurrentName = ", stCurrentName )
						ENDIF
					ENDFOR
				ELSE
					
					//The vehicle models
					FOR iCurrentNode = 0 TO (FMMC_MAX_NUM_RACERS -1 )
						nodeName = "VehicleModelArray"
						nodeName += iCurrentNode 
						IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), nodeName)
							iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
							PRINTLN(nodeName)
							//Go through the INT atributes
							FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
								stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
								IF ARE_STRINGS_EQUAL(stCurrentName, "mnVehicleModel")
									g_FMMC_STRUCT.mnVehicleModel[iCurrentNode] = INT_TO_ENUM(MODEL_NAMES, GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
								ENDIF
							ENDFOR
						ENDIF
					ENDFOR
					
					//Check point details
					IF g_FMMC_STRUCT.iNumberOfCheckPoints > ciSMALLEST_AMOUNT					
						FOR iCurrentNode = 0 TO (g_FMMC_STRUCT.iNumberOfCheckPoints - ciLOOP_FROM )
							nodeName = "ChpArray"
							nodeName += iCurrentNode 
							IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), nodeName)
								iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
								PRINTLN(nodeName)
								//Go through the INT atributes
								FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
									stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
									IF ARE_STRINGS_EQUAL(stCurrentName, "x")
										g_FMMC_STRUCT.sPlacedCheckpoint[iCurrentNode].vCheckPoint.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "y")
										g_FMMC_STRUCT.sPlacedCheckpoint[iCurrentNode].vCheckPoint.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "z")
										g_FMMC_STRUCT.sPlacedCheckpoint[iCurrentNode].vCheckPoint.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "fCheckPointHeight")
										g_FMMC_STRUCT.sPlacedCheckpoint[iCurrentNode].fCheckPointHeight = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ENDIF
								ENDFOR
							ENDIF
						ENDFOR
					ENDIF					
					
					//Object Details
					IF g_FMMC_STRUCT_ENTITIES.iNumberOfObjects > ciSMALLEST_AMOUNT					
						FOR iCurrentNode = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - ciLOOP_FROM )
							nodeName = "ObjArray"
							nodeName += iCurrentNode 
							IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), nodeName)
								iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
								PRINTLN(nodeName)
								//Go through the INT atributes
								FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
									stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
									IF ARE_STRINGS_EQUAL(stCurrentName, "x")
										g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCurrentNode].vPos.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "y")
										g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCurrentNode].vPos.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "z")
										g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCurrentNode].vPos.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "ObjHead")
										g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCurrentNode].fHead = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "ObjMN")
										g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCurrentNode].mn = INT_TO_ENUM(MODEL_NAMES, GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "ObjRule")
										g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCurrentNode].iRule[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									//ELIF ARE_STRINGS_EQUAL(stCurrentName, "ObjStage")
										//g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCurrentNode].iStage = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ENDIF
								ENDFOR
							ENDIF
						ENDFOR
					ENDIF
					
//					//Animal Details
//					IF g_FMMC_STRUCT.iNumberOfAnimals > ciSMALLEST_AMOUNT					
//						FOR iCurrentNode = 0 TO (g_FMMC_STRUCT.iNumberOfAnimals - ciLOOP_FROM )
//							nodeName = "AnmArray"
//							nodeName += iCurrentNode 
//							IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), nodeName)
//								iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
//								PRINTLN(nodeName)
//								//Go through the INT atributes
//								FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
//									stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
//									IF ARE_STRINGS_EQUAL(stCurrentName, "x")
//										g_FMMC_STRUCT.sPlacedAnimal[iCurrentNode].vPos.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
//									ELIF ARE_STRINGS_EQUAL(stCurrentName, "y")
//										g_FMMC_STRUCT.sPlacedAnimal[iCurrentNode].vPos.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
//									ELIF ARE_STRINGS_EQUAL(stCurrentName, "z")
//										g_FMMC_STRUCT.sPlacedAnimal[iCurrentNode].vPos.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
//									ELIF ARE_STRINGS_EQUAL(stCurrentName, "ObjHead")
//										g_FMMC_STRUCT.sPlacedAnimal[iCurrentNode].fHead = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
//									ELIF ARE_STRINGS_EQUAL(stCurrentName, "ObjMN")
//										g_FMMC_STRUCT.sPlacedAnimal[iCurrentNode].mn = INT_TO_ENUM(MODEL_NAMES, GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
//									ELIF ARE_STRINGS_EQUAL(stCurrentName, "ObjRule")
//										g_FMMC_STRUCT.sPlacedAnimal[iCurrentNode].iRule[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
//									//ELIF ARE_STRINGS_EQUAL(stCurrentName, "ObjStage")
//										//g_FMMC_STRUCT.sPlacedAnimal[iCurrentNode].iStage = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
//									ENDIF
//								ENDFOR
//							ENDIF
//						ENDFOR
//					ENDIF
					
					//Weapon Details
					IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons > ciSMALLEST_AMOUNT					
						FOR iCurrentNode = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - ciLOOP_FROM )
							nodeName = "WepArray"
							nodeName += iCurrentNode 
							IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), nodeName)
								iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
								PRINTLN(nodeName)
								//Go through the INT atributes
								FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
									stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
									IF ARE_STRINGS_EQUAL(stCurrentName, "x")
										g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iCurrentNode].vPos.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "y")
										g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iCurrentNode].vPos.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "z")
										g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iCurrentNode].vPos.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "WeaHead")
										g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iCurrentNode].vRot.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "WeaPT")
										g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iCurrentNode].pt = INT_TO_ENUM(PICKUP_TYPE, GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
									ENDIF
								ENDFOR
							ENDIF
						ENDFOR
					ENDIF
					
					
					//Vehicle Details
					IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > ciSMALLEST_AMOUNT					
						FOR iCurrentNode = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - ciLOOP_FROM )
							nodeName = "VehArray"
							nodeName += iCurrentNode 
							IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), nodeName)
								iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
								PRINTLN(nodeName)
								//Go through the INT atributes
								FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
									stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
									IF ARE_STRINGS_EQUAL(stCurrentName, "x")
										g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCurrentNode].vPos.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "y")
										g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCurrentNode].vPos.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "z")
										PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCurrentNode].vPos.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute) is happening. g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCurrentNode].vPos.z: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCurrentNode].vPos.z)
										g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCurrentNode].vPos.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "VehHead")
										g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCurrentNode].fHead = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "VehMN")
										g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCurrentNode].mn = INT_TO_ENUM(MODEL_NAMES, GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "VehRule")
										g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCurrentNode].iRule[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									//ELIF ARE_STRINGS_EQUAL(stCurrentName, "VehStage")
										//g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCurrentNode].iStage = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ENDIF
								ENDFOR
							ENDIF
						ENDFOR
					ENDIF
					
					//Ped Details
					IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > ciSMALLEST_AMOUNT					
						FOR iCurrentNode = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - ciLOOP_FROM )
							nodeName = "PedArray"
							nodeName += iCurrentNode 
							IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), nodeName)
								iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
								PRINTLN(nodeName)
								//Go through the INT atributes
								FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
									stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
									IF ARE_STRINGS_EQUAL(stCurrentName, "x")
										g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCurrentNode].vPos.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "y")
										g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCurrentNode].vPos.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "z")
										g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCurrentNode].vPos.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "PedHead")
										g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCurrentNode].fHead = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "PedMN")
										g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCurrentNode].mn = INT_TO_ENUM(MODEL_NAMES, GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "PedGun")
										g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCurrentNode].gun = INT_TO_ENUM(WEAPON_TYPE, GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "PedSkill")
										g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCurrentNode].skill = INT_TO_ENUM(COMBAT_ABILITY_LEVEL, GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "PedTeam")
										g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCurrentNode].iTeam[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)										
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "PedRule")
										g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCurrentNode].iRule[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									//ELIF ARE_STRINGS_EQUAL(stCurrentName, "PedStage")
										//g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCurrentNode].iStage = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ENDIF
								ENDFOR
							ENDIF
						ENDFOR
					ENDIF
					
					//USJ Details					
					IF g_FMMC_STRUCT.iNumberOfSj > ciSMALLEST_AMOUNT					
						FOR iCurrentNode = 0 TO (g_FMMC_STRUCT.iNumberOfSj - ciLOOP_FROM )
							nodeName = "UsjStart"
							nodeName += iCurrentNode 
							IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), nodeName)
								iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
								PRINTLN(nodeName)
								//Go through the INT atributes
								FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
									stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
									IF ARE_STRINGS_EQUAL(stCurrentName, "x")
										g_FMMC_STRUCT.sStuntJumps[iCurrentNode].vStartPos.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "y")
										g_FMMC_STRUCT.sStuntJumps[iCurrentNode].vStartPos.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "z")
										g_FMMC_STRUCT.sStuntJumps[iCurrentNode].vStartPos.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ENDIF
								ENDFOR
							ENDIF
							
							
							nodeName = "UsjEnd"
							nodeName += iCurrentNode 
							IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), nodeName)
								iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
								PRINTLN(nodeName)
								//Go through the INT atributes
								FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
									stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
									IF ARE_STRINGS_EQUAL(stCurrentName, "x")
										g_FMMC_STRUCT.sStuntJumps[iCurrentNode].vEndPos.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "y")
										g_FMMC_STRUCT.sStuntJumps[iCurrentNode].vEndPos.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "z")
										g_FMMC_STRUCT.sStuntJumps[iCurrentNode].vEndPos.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ENDIF
								ENDFOR
							ENDIF
							
							
							
							nodeName = "UsjCam"
							nodeName += iCurrentNode 
							IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), nodeName)
								iNumberOfNodeAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
								PRINTLN(nodeName)
								//Go through the INT atributes
								FOR eachAttribute = 0 TO (iNumberOfNodeAttributes - 1)
									stCurrentName = GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)
									IF ARE_STRINGS_EQUAL(stCurrentName, "x")
										g_FMMC_STRUCT.sStuntJumps[iCurrentNode].vCameraPos.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "y")
										g_FMMC_STRUCT.sStuntJumps[iCurrentNode].vCameraPos.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ELIF ARE_STRINGS_EQUAL(stCurrentName, "z")
										g_FMMC_STRUCT.sStuntJumps[iCurrentNode].vCameraPos.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
									ENDIF
								ENDFOR
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
				GET_NEXT_XML_NODE ()
			ENDFOR
		ENDIF
		DELETE_XML_FILE()		
		PRINTLN("-----------------------------------------------------------")
		PRINTLN("--                                                       --")
		PRINTLN("--                                                       --")
		PRINTLN("--                   LOADED XML FILE                     --")
		PRINTLN("--                                                       --")
		PRINTLN("--                                                       --")
		PRINTLN("-----------------------------------------------------------")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

#ENDIF













