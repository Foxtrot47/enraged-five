
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////         Freemode mission selector menu  	    //////////////////////////////
//////////////////////////////           	   Bobby Wright          	    //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
USING "rage_builtins.sch"
USING "globals.sch"
USING "Transition_Invites.sch"
#IF IS_DEBUG_BUILD
USING "commands_streaming.sch"
USING "cellphone_public.sch"
USING "selector_public.sch"
USING "net_mission.sch"
//USING "FMMC_warp_locations.sch"
USING "FMMC_Cloud_loader.sch"
USING "net_activity_selector_debug.sch"

CONST_INT NUM_TAB_0						0
CONST_INT NUM_TAB_1						1
CONST_INT NUM_TAB_2						2

PROC SET_UP_TITLE_MENU_TEXT(BOOL bSelected)
	//Display the menu titles		
	SET_TEXT_SCALE(0.800, 0.800)
	IF bSelected
		SET_TEXT_COLOUR(0, 0, 0, 255)
	ELSE
		SET_TEXT_COLOUR(255, 255, 255, 255)
	ENDIF
	SET_TEXT_CENTRE(TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
	SET_TEXT_EDGE(0, 0, 0, 0, 255)
ENDPROC
//set up the menu text
PROC SET_UP_TEMP_FREEMODE_MENU_TEXT(BOOL bWhiteText = TRUE, BOOL bIsHeader = FALSE, BOOL bJust = FALSE, BOOL bIsActive = FALSE)
	
	IF NOT bIsActive
		IF bIsHeader
			SET_TEXT_SCALE(0.503, 0.503)
		ELSE
			SET_TEXT_SCALE(0.295, 0.295)
		ENDIF
		IF bWhiteText
			SET_TEXT_COLOUR(255, 255, 255, 255)
		ELSE
			SET_TEXT_COLOUR(0, 0, 0, 255)
		ENDIF
	ELSE
		IF bIsHeader
			SET_TEXT_SCALE(0.503, 0.503)
		ELSE
			SET_TEXT_SCALE(0.35, 0.35)
		ENDIF
		IF bWhiteText
			SET_TEXT_COLOUR(200, 200, 200, 255)
		ELSE
			SET_TEXT_COLOUR(100, 100, 100, 255)
		ENDIF
	ENDIF
	IF bJust 
		SET_TEXT_CENTRE(TRUE)
	ELSE
		SET_TEXT_CENTRE(FALSE)
	ENDIF
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
	SET_TEXT_EDGE(0, 0, 0, 0, 255)
ENDPROC

//Display the text
PROC DISPLAY_TEXT_WITH_MISSION_STRING(FLOAT fXpos, FLOAT fYpos, STRING pTextLabel, STRING pMissionNameString)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pMissionNameString)
		SET_TEXT_CENTRE(FALSE)		
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(fXpos, fYpos)	
ENDPROC
PROC DISPLAY_TEXT_WITH_MISSION_STRING_AND_CID(FLOAT fXpos, FLOAT fYpos, STRING pTextLabel, STRING pMissionNameString, TEXT_LABEL_23 pCID, INT iLength)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pMissionNameString)
		IF iLength < 1
			iLength = 1
		ENDIF
		INT iMaxLength = GET_LENGTH_OF_LITERAL_STRING(pCID)
		IF iLength > iMaxLength
			iLength = iMaxLength 
		ENDIF
		pCID = GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(pCID, iLength)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pCID)
		SET_TEXT_CENTRE(FALSE)		
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(fXpos, fYpos)	
ENDPROC

//Display the text
PROC DISPLAY_TEXT_WITH_MISSION_STRING_AND_LES_RATING(FLOAT fXpos, FLOAT fYpos, STRING pTextLabel, STRING pMissionNameString, INT iLesRating)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(iLesRating)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pMissionNameString)
		SET_TEXT_CENTRE(FALSE)		
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(fXpos, fYpos)	
ENDPROC
PROC DISPLAY_TEXT_WITH_MISSION_STRING_AND_VERSION(FLOAT fXpos, FLOAT fYpos, STRING pTextLabel, STRING pMissionNameString, INT iVersion)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pMissionNameString)
		ADD_TEXT_COMPONENT_INTEGER(iVersion)
		SET_TEXT_CENTRE(FALSE)		
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(fXpos, fYpos)	
ENDPROC
PROC DISPLAY_TEXT_WITH_MISSION_STRING_AND_LES_RATING_AND_VERSION(FLOAT fXpos, FLOAT fYpos, STRING pTextLabel, STRING pMissionNameString, INT iLesRating, INT iVersion)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(iLesRating)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pMissionNameString)
		ADD_TEXT_COMPONENT_INTEGER(iVersion)
		SET_TEXT_CENTRE(FALSE)		
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(fXpos, fYpos)	
ENDPROC

PROC DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(DisplayAtX, DisplayAtY)
ENDPROC
PROC DISPLAY_DEBUG_TEXT_WITH_NUMBER(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, INT NumberToDisplay)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(DisplayAtX, DisplayAtY)
ENDPROC
PROC DISPLAY_DEBUG_TEXT_WITH_2_NUMBERS(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, INT FirstNumberToDisplay, INT SecondNumberToDisplay)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(FirstNumberToDisplay)
		ADD_TEXT_COMPONENT_INTEGER(SecondNumberToDisplay)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(DisplayAtX, DisplayAtY)
ENDPROC

PROC DISPLAY_TEXT_WITH_MISSION_STRING_AND_GANG_RANK(FLOAT fXpos, FLOAT fYpos, STRING pTextLabel, STRING pMissionNameString, INT iGangRank)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(iGangRank)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pMissionNameString)
		SET_TEXT_CENTRE(FALSE)		
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(fXpos, fYpos)	
ENDPROC

CONST_INT 	FMMC_MP_MENU_COLOUM_0				0
CONST_INT 	FMMC_MP_MENU_COLOUM_1				1
CONST_INT 	FMMC_MP_MENU_COLOUM_2				2

FUNC INT GET_HEIST_STRAND_FROM_LOOP(INT iLoop)	
	SWITCH (iLoop)
		CASE 0 RETURN g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job	// Ornate Bank - Finale	
		CASE 1 RETURN g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid		// Biolab Score - Finale
		CASE 2 RETURN g_sMPTUNABLES.iroot_id_HASH_The_Prison_Break			// Prison Break - Finale
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC INT GET_COLOUM_MAX_FROM_CURRENT_TAB(FMMC_MP_IN_GAME_MENU_VARS 		&MenuVars)
	IF MenuVars.iTab = NUM_TAB_0 		
		IF MenuVars.iSelectedColoum[MenuVars.iTab] = 0
			RETURN MAX_NUMBER_FMMC_SAVES
		ELIF MenuVars.iSelectedColoum[MenuVars.iTab] = 1
			RETURN (MenuVars.iTotalMissionCountDM+1)
		ELIF MenuVars.iSelectedColoum[MenuVars.iTab] = 2
			RETURN (MenuVars.iTotalMissionCountRace+1)
		ELSE
			RETURN FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED
		ENDIF
	ELIF MenuVars.iTab = NUM_TAB_1	  	
		IF MenuVars.iSelectedColoum[MenuVars.iTab] = 0 
			RETURN (MenuVars.iTotalMissionCountBase+1)
		ELIF MenuVars.iSelectedColoum[MenuVars.iTab] = 1
			RETURN (MenuVars.iTotalMissionCountDM+1)
		ELIF MenuVars.iSelectedColoum[MenuVars.iTab] = 2
			RETURN (MenuVars.iTotalMissionCountRace+1)
		ENDIF
	ELIF MenuVars.iTab = NUM_TAB_2
		RETURN GET_HEIST_STRAND_LENGTH(GET_HEIST_STRAND_FROM_LOOP(MenuVars.iSelectedColoum[NUM_TAB_2]))
	 // FEATURE_HEIST_PLANNING


	ENDIF
	RETURN MAX_NUMBER_FMMC_SAVES
ENDFUNC	

FUNC  STRING GET_MISSION_TYPE_STRING(INT iMissionType)
	SWITCh iMissionType
		CASE FMMC_TYPE_MISSION			RETURN "FMMC_MPM_TY0"
		CASE FMMC_TYPE_MISSION_CTF		RETURN "FMMC_MPM_TY6"
		CASE FMMC_TYPE_IMPROMPTU_DM		RETURN "FMMC_MPM_TY3"
		CASE FMMC_TYPE_GANGHIDEOUT		RETURN "FMMC_MPM_TY5"
		CASE FMMC_TYPE_DEATHMATCH		RETURN "FMMC_MPM_TY1"
		CASE FMMC_TYPE_RACE				RETURN "FMMC_MPM_TY2"
		CASE FMMC_TYPE_SURVIVAL			RETURN "FMMC_MPM_TY4"
		CASE FMMC_TYPE_CRATE_DROP		RETURN "FMMC_MPM_TY16"
	ENDSWITCH
	RETURN "FMMC_MPM_TY0"
ENDFUNC
//
//FUNC  VECTOR GET_SAVED_LOCATION_VECTOR(INT iSelection)
//	SWITCH iSelection
//		CASE 0 			RETURN <<14.8390, -1121.5892, 27.7972 >>
//		CASE 1			RETURN << -321.7280, 6072.0317, 30.3044 >>
//		CASE 2 			RETURN << 1701.8083, 3750.2336, 33.1441 >>
//		CASE 3 			RETURN <<-3159.9729, 1080.5508, 19.6948 >>
//		CASE 4 			RETURN <<-1317.3650, -389.8738, 35.4051 >>
//		CASE 5 			RETURN <<843.7667, -1019.8958, 26.5544>>
//		CASE 6 			RETURN <<240.1062, -44.1596, 68.6884 >>
//		CASE 7 			RETURN <<133.3979, -1306.6250, 28.1136>>
//		CASE 8 			RETURN <<-1375.3013, 56.1048, 52.9423 >>
//		CASE 9 			RETURN <<-1153.9553, -1622.4253, 3.3370>>
//		CASE 10 		RETURN <<14.8390, -1121.5892, 27.7972 >>
//		CASE 11 		RETURN <<1994.9800, 3049.2815, 		46.1258  >>
//		CASE 12 		RETURN <<1443.6055, -2219.2910, 	60.0338  >>
//		CASE 13			RETURN g_vPartyLeaderPositionResult
//		//CASE 13 		RETURN <<320.2082, 174.9781, 102.7317 >>
//		//CASE 14 		RETURN << >>
//	ENDSWITCH
//	RETURN <<0.0, 0.0, 0.0>>
//ENDFUNC
//
//FUNC  STRING GET_SAVED_LOCATION_VECTOR_NAME(INT iSelection)
//	SWITCH iSelection
//		CASE 0 			RETURN "FMMC_MPM_SW0"
//		CASE 1			RETURN "FMMC_MPM_SW1"
//		CASE 2 			RETURN "FMMC_MPM_SW2"
//		CASE 3 			RETURN "FMMC_MPM_SW3"
//		CASE 4 			RETURN "FMMC_MPM_SW4"
//		CASE 5 			RETURN "FMMC_MPM_SW5"
//		CASE 6 			RETURN "FMMC_MPM_SW6"
//		CASE 7 			RETURN "FMMC_MPM_SW7"
//		CASE 8 			RETURN "FMMC_MPM_SW8"
//		CASE 9 			RETURN "FMMC_MPM_SW9"
//		CASE 10			RETURN "FMMC_MPM_SW10"
//		CASE 11			RETURN "FMMC_MPM_SW11"
//		CASE 12			RETURN "FMMC_MPM_SW12"
//		CASE 13			RETURN "FMMC_MPM_SW13"
//		//CASE 13			RETURN "FMMC_MPM_SW13"
//		//CASE 14			RETURN "FMMC_MPM_SW14"
////[FMMC_MPM_SW0]
////Gun Shop - Downtown
////[FMMC_MPM_SW1]
////Gun Shop - Paleto Bay
////[FMMC_MPM_SW2]
////Gun Shop - Sandy Shores
////[FMMC_MPM_SW3]
////Gun Shop - Great Ocean Highway
////[FMMC_MPM_SW4]
////Gun Shop - Morningwood
////[FMMC_MPM_SW5]
////Gun Shop - East Los Santos
////[FMMC_MPM_SW6]
////Gun Shop - Vinewood
////[FMMC_MPM_SW7]
////Vanilla Unicorn
////[FMMC_MPM_SW8]
////Golf
////[FMMC_MPM_SW9]
////Tennis
////[FMMC_MPM_SW10]
////Shooting Range
////[FMMC_MPM_SW11]
////Darts
////[FMMC_MPM_SW12]
////Arm Wrestling
////[FMMC_MPM_SW13]
////Party Leader

//	ENDSWITCH
//	RETURN ""
//ENDFUNC
//STRUCT_CLOUD_BOOKMARK_DATA sBookMarks
//FMMC_BOOKMARK_STRUCT s_FMMC_BOOKMARKS
//	IF sBookMarks.bSuccess = FALSE
//		IF LOAD_BOOKMARKS_FROM_CLOUD(sBookMarks, s_FMMC_BOOKMARKS)
//			sBookMarks.bSuccess = TRUE
//		ENDIF
//	ENDIF 
//		
//Find the mission and update the XML
PROC UPDATE_LES_RATING_FROM_XML(TEXT_LABEL_31 tl31_contentID, INT iLesRat)
INT iLoop 
	FOR iLoop = 0 TO (FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED - 1)
		IF ARE_STRINGS_EQUAL(tl31_contentID, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlName)
			g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iLesRating = iLesRat
			PRINTLN("LOAD_LES_RATING_INTO_GAME - ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlName, " = ", iLesRat)
		ENDIF
	ENDFOR
ENDPROC
PROC CLEAR_OLD_LES_RATING()
	PRINTLN("LOAD_LES_RATING_INTO_GAME - CLEAR_OLD_LES_RATING")
	INT iLoop 
	FOR iLoop = 0 TO (FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED - 1)
		g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iLesRating = 0
	ENDFOR
ENDPROC
//Load the Les rating into the game
FUNC BOOL LOAD_LES_RATING_INTO_GAME(STRING xml, INT &iGetCount, INT iGetMax, INT &iNodeCount)	
	PRINTLN("LOAD_LES_RATING_INTO_GAME")
    // stores info just incase attributes done in wrong order
    TEXT_LABEL_31 tl31_contentID
    CONST_INT ci_contentID  -231996142
	CONST_INT ci_LB_Rat  -951123275
	CONST_INT ci_item  -743219004
	INT iLesRat
	INT iGotCount
    // load in xml file
    IF LOAD_XML_FILE(xml)        
		INT eachNode, eachAttribute		
		IF iNodeCount = 0
			iNodeCount = GET_NUMBER_OF_XML_NODES()
        ENDIF
		IF iNodeCount <> 0
			FOR eachNode = 0 TO (iGetCount-1)
				IF eachNode < iNodeCount
					GET_NEXT_XML_NODE()
				ENDIF
			ENDFOR
            FOR eachNode = iGetCount TO (iNodeCount-1)
				IF iGotCount < iGetMax
	                SWITCH GET_HASH_KEY(GET_XML_NODE_NAME())
	                    CASE ci_item
							PRINTLN("LOAD_LES_RATING_INTO_GAME - ci_item")
	                        IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
	                            FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
	                                SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))
	                                    CASE ci_contentID
											tl31_contentID = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)										
										BREAK
										CASE ci_LB_Rat
	                                        iLesRat = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)									
	                                    BREAK
	                                ENDSWITCH
	                            ENDFOR	
	                        ENDIF
	                    BREAK
	                ENDSWITCH
					PRINTLN("LOAD_LES_RATING_INTO_GAME - tl31_contentID = ", tl31_contentID)
					PRINTLN("LOAD_LES_RATING_INTO_GAME - iLesRat        = ", iLesRat)
					UPDATE_LES_RATING_FROM_XML(tl31_contentID, iLesRat)		
	                GET_NEXT_XML_NODE()
					iGotCount++
				ELSE
			        // delete xml file
			        DELETE_XML_FILE()      
					PRINTLN("LOAD_LES_RATING_INTO_GAME - iGetCount = ", iGetCount)
					PRINTLN("LOAD_LES_RATING_INTO_GAME - iGotCount = ", iGotCount)
					iGetCount += iGotCount
					RETURN TRUE
				ENDIF
            ENDFOR
        ENDIF
        // delete xml file
        DELETE_XML_FILE()  
		PRINTLN("LOAD_LES_RATING_INTO_GAME - iGetCount = ", iGetCount)
		PRINTLN("LOAD_LES_RATING_INTO_GAME - iGotCount = ", iGotCount)
		iGetCount += iGotCount      
		RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF	
    RETURN FALSE
ENDFUNC

CONST_FLOAT tfRATING_MOVE_CONST 0.155



FUNC BOOL DO_TEMP_FREEMODE_MENU(	FMMC_MP_IN_GAME_MENU_VARS 		&MenuVars, 
									//FMMC_GLOBAL_HEADER_STRUCT 		&s_FMMC_ROCKSTAR_RAMPAGE_HIDEOUT_STRUCT, 
									FMMC_ROCKSTAR_CREATED_STRUCT	&s_FMMC_ROCKSTAR_CREATED, 
									FMMC_ROCKSTAR_VERIFIED_STRUCT 	&s_FMMC_ROCKSTAR_CANDIDATE, 
									INT 							iMissionToLoad)
	
	IF GET_COMMANDLINE_PARAM_EXISTS("mp_DisableDebugMenu") 
		MenuVars.bMenuActive = FALSE
		RETURN FALSE
	ENDIF
	
//	IF GET_FM_UGC_HAS_FINISHED()
//		IF MenuVars.bReLoadXML		
//			STRING stXML
//			IF IS_PS3_VERSION()
//				stXML = "X:\\gta5\\build\\dev\\common\\data\\script\\xml\\MP_LB_RATINGS_PS3.xml"
//			ELIF IS_ORBIS_VERSION()
//				stXML = "MP_LB_RATINGS_ORBIS.xml"
//			ELIF IS_XBOX360_VERSION()
//				stXML = "X:\\gta5\\build\\dev\\common\\data\\script\\xml\\MP_LB_RATINGS_XBOX.xml"
//			ELIF IS_DURANGO_VERSION()
//				stXML = "MP_LB_RATINGS_DURANGO.xml"
//			ELIF IS_PC_VERSION()
//				stXML = "MP_LB_RATINGS_PC.xml"	
//			ELSE
//				PRINTLN("LOAD_LES_RATING_INTO_GAME - Unknowen version!")
//				SCRIPT_ASSERT("LOAD_LES_RATING_INTO_GAME - Unknowen version!")
//			ENDIF
//			IF MenuVars.bOldCleared = FALSE
//				CLEAR_OLD_LES_RATING()
//				MenuVars.bOldCleared = TRUE
//			ENDIF
//			IF LOAD_LES_RATING_INTO_GAME(stXML, MenuVars.iGetCount, 20, MenuVars.iNodeCount)	
//				IF MenuVars.iGetCount >= MenuVars.iNodeCount
//					//PRINTLN("LOAD_LES_RATING_INTO_GAME DONE")					
//					MenuVars.bReLoadXML = FALSE		
//					MenuVars.bOldCleared = FALSE					
//					MenuVars.iReLoadXmlFrameCount = GET_FRAME_COUNT() 
//					MenuVars.iNodeCount = 0
//					MenuVars.iGetCount = 0
//				//	PRINTLN("g_TransitionSessionNonResetVars.MenuVars.iReLoadXmlFrameCount = ", MenuVars.iReLoadXmlFrameCount)
//				ENDIF
//			ENDIF
//		ELSE
//			MenuVars.iGetCount = 0
//		ENDIF
//	ENDIF
	
	IF MenuVars.bMenuActive = TRUE
		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			MenuVars.bMenuActive = FALSE
			RETURN FALSE
		ENDIF
	ENDIF
//	s_FMMC_ROCKSTAR_RAMPAGE_HIDEOUT_STRUCT = s_FMMC_ROCKSTAR_RAMPAGE_HIDEOUT_STRUCT
	//IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
	 
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_Z, KEYBOARD_MODIFIER_NONE, "Freemode Menu")
		PRINTLN("IS_DEBUG_KEY_JUST_PRESSED")
		IF MenuVars.bMenuActive
			MenuVars.bMenuActive = FALSE
			//SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPGangSelect")
			ENABLE_SELECTOR()
			RETURN FALSE
		ELSE
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				//REQUEST_STREAMED_TEXTURE_DICT("MPGangSelect")
				MenuVars.bMenuActive = TRUE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF MenuVars.bMenuActive = FALSE
		IF MenuVars.bSelectorDisabled
			IF IS_SELECTOR_DISABLED()
				ENABLE_SELECTOR()
				MenuVars.bSelectorDisabled = FALSE
			ENDIF
		ENDIF
		RETURN FALSE
	ENDIF
	//Check to see if the menu can run
	IF IS_PAUSE_MENU_ACTIVE()		
		RETURN FALSE
	ENDIF		
	IF IS_SCRIPT_HUD_DISPLAYING (HUDPART_TRANSITIONHUD)	
		RETURN FALSE
	ENDIF

	INT i = 0	
//	IF MenuVars.iTab = NUM_TAB_1
//		//PRINTLN("Calling GET_MP_VECTOR_PLAYER_STAT")
//		FOR i = 0 TO (MAX_FM_SAVED_LOCATIONS - 1)
//			MenuVars.vPos[i] =  GET_MP_VECTOR_PLAYER_STAT(GET_VECTOR_STAT_INDEX_FROM_INT(i))
//		ENDFOR
//	ENDIF
	//quit if needed.
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		MenuVars.bMenuActive = FALSE
		//SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPGangSelect")
		ENABLE_SELECTOR()
	ENDIF		
	
	//REQUEST_STREAMED_TEXTURE_DICT("MPGangSelect")
	//Once the textures have loaded
	//IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPGangSelect")
	//	RETURN FALSE
	//ENDIF	
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_RANKBAR)
	//Disable stuff
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	IF NOT IS_SELECTOR_DISABLED()
		DISABLE_SELECTOR()
		MenuVars.bSelectorDisabled = TRUE
	ENDIF
	
	//Consts 
	CONST_FLOAT 	TEXT_ROW_0 							0.104
	CONST_FLOAT 	FMMC_MP_MENU_COLOUM_WIDTH 			0.275
//	CONST_INT 		MAX_NUMBER_OF_PAYNES 				3
	CONST_INT 		MAX_ITEMS_IN_PANE 					18
	CONST_FLOAT 	cfFMMC_IN_GAME_MENU_LINE_HEIGHT 	0.034
	CONST_FLOAT 	cfBG_START_HEIGHT 					0.243
	CONST_FLOAT 	cfTEXT_START_HEIGHT 				0.228
	
	BOOL bInheritRow
	
	//Draw the help text
	DRAW_RECT(0.775, 0.880, 0.252,  0.067,  0, 0, 0, 200)
	SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
	DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.667, 0.862, "FMMC_MPM_M0")
	SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
	DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.743, 0.862, "FMMC_MPM_M1")
	SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
	DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.827, 0.862, "FMMC_MPM_M3")	
	SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
	DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.667, 0.862+ 0.02, "FMMC_MPM_M4")	
	SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
	DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.743, 0.862+ 0.02, "FMMC_MPM_M7")

	//Deal with the controlls
	//Left
	INT iRowOld
	INT iGameTimer = GET_GAME_TIMER()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT) AND iGameTimer > MenuVars.iMoveTimer)
	OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT) AND iGameTimer > MenuVars.iMoveTimer)
		IF MenuVars.iTab = NUM_TAB_0
			MenuVars.iMoveTimer = iGameTimer + 285
			iRowOld =  MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]]
			MenuVars.iSelectedColoum[MenuVars.iTab]--
			IF MenuVars.iSelectedColoum[MenuVars.iTab] < 0
				MenuVars.iSelectedColoum[MenuVars.iTab] = (MAX_NUM_COLUMNS-1)
			ENDIF
			MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] = iRowOld
		ELSE
			bInheritRow = TRUE
			
			
			MenuVars.iMoveTimer = iGameTimer + 285
			iRowOld =  MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]]
			MenuVars.iSelectedColoum[MenuVars.iTab]--
			IF MenuVars.iSelectedColoum[MenuVars.iTab] < 0
				MenuVars.iSelectedColoum[MenuVars.iTab] = (MAX_NUM_COLUMNS-1)
			ENDIF
			
			IF bInheritRow
				MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] = iRowOld
			ENDIF
		ENDIF
	ENDIF
	//Right
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT) AND iGameTimer > MenuVars.iMoveTimer
	OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) AND iGameTimer > MenuVars.iMoveTimer)
		IF MenuVars.iTab = NUM_TAB_0
			MenuVars.iMoveTimer = iGameTimer + 285
			iRowOld =  MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]]
			MenuVars.iSelectedColoum[MenuVars.iTab]++
			IF MenuVars.iSelectedColoum[MenuVars.iTab] >= MAX_NUM_COLUMNS
				MenuVars.iSelectedColoum[MenuVars.iTab] = 0
			ENDIF
			MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] = iRowOld
		ELSE
			bInheritRow = TRUE
		
			
			MenuVars.iMoveTimer = iGameTimer + 285
			iRowOld =  MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]]
			MenuVars.iSelectedColoum[MenuVars.iTab]++
			IF MenuVars.iSelectedColoum[MenuVars.iTab] >= (MAX_NUM_COLUMNS)
				MenuVars.iSelectedColoum[MenuVars.iTab] = 0
			ENDIF
			
			IF bInheritRow
				MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] = iRowOld
			ENDIF
		ENDIF
	ENDIF
	//Up
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND iGameTimer > MenuVars.iMoveTimer
	OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP) AND iGameTimer > MenuVars.iMoveTimer)
		MenuVars.iMoveTimer = iGameTimer + 100

		
		MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]]--
		IF MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] < 0
			MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] = (GET_COLOUM_MAX_FROM_CURRENT_TAB(MenuVars) -1)
		ENDIF
		IF MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] < 0
			MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] = 0
		ENDIF
	ENDIF
	//Down
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND iGameTimer > MenuVars.iMoveTimer
	OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN) AND iGameTimer > MenuVars.iMoveTimer)
		MenuVars.iMoveTimer = iGameTimer + 100
		
		MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]]++
		IF MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] >= GET_COLOUM_MAX_FROM_CURRENT_TAB(MenuVars)
			MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] = 0
		ENDIF
	ENDIF
	
	//Switch Tab
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB) AND iGameTimer > MenuVars.iMoveTimer
	OR (IS_DEBUG_KEY_JUST_PRESSED(KEY_LEFT, KEYBOARD_MODIFIER_CTRL, "TabOnFmMenu") AND iGameTimer > MenuVars.iMoveTimer)
		MenuVars.iMoveTimer = iGameTimer + 185
		MenuVars.iTab--
		INT iMaxTab = MAX_NUM_TABS -1
			iMaxTab = MAX_NUM_TABS 
		IF MenuVars.iTab < 0
			MenuVars.iTab = (iMaxTab -1)
		ENDIF
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB) AND iGameTimer > MenuVars.iMoveTimer
	OR (IS_DEBUG_KEY_JUST_PRESSED(KEY_RIGHT, KEYBOARD_MODIFIER_CTRL, "TabOnFmMenu") AND iGameTimer > MenuVars.iMoveTimer)
		MenuVars.iMoveTimer = iGameTimer + 185
		MenuVars.iTab++
		INT iMaxTab = MAX_NUM_TABS -1
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_HeistZmenu"))
			iMaxTab = MAX_NUM_TABS 
		ENDIF
		
		IF MenuVars.iTab >= iMaxTab
			MenuVars.iTab = 0
		ENDIF
	ENDIF
	//Up
	IF (IS_DEBUG_KEY_JUST_PRESSED(KEY_UP, KEYBOARD_MODIFIER_CTRL, "QuickUpDownFmMenu") AND iGameTimer > MenuVars.iMoveTimer)
		MenuVars.iMoveTimer = iGameTimer + 100
		MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] -= MAX_ITEMS_IN_PANE
		IF MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] < 0
			MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] = (GET_COLOUM_MAX_FROM_CURRENT_TAB(MenuVars) -1)
		ENDIF
	ENDIF
	//Down
	IF (IS_DEBUG_KEY_JUST_PRESSED(KEY_DOWN, KEYBOARD_MODIFIER_CTRL, "QuickUpDownFmMenu") AND iGameTimer > MenuVars.iMoveTimer)
		MenuVars.iMoveTimer = iGameTimer + 100
		MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] += MAX_ITEMS_IN_PANE
		IF MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] >= GET_COLOUM_MAX_FROM_CURRENT_TAB(MenuVars)
			MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]] = 0
		ENDIF
	ENDIF
	
	IF MenuVars.bReLoadXML = FALSE
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
		OR IS_DEBUG_KEY_JUST_PRESSED(KEY_RETURN, KEYBOARD_MODIFIER_CTRL, "QuickUpDownFmMenu") 
			MenuVars.bReLoadXML = TRUE
		ENDIF
	ENDIF
	
	
	//If we are in coloum 0 and a mission is selected then load it if it is valid.
	//IF MenuVars.iSelectedColoum[NUM_TAB_0] = FMMC_MP_MENU_COLOUM_0
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN) AND iGameTimer > MenuVars.iMoveTimer)

	SWITCH MenuVars.iTab
		CASE NUM_TAB_0
			SWITCH MenuVars.iSelectedColoum[NUM_TAB_0]
				CASE FMMC_MP_MENU_COLOUM_0
					IF NOT IS_VECTOR_ZERO(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]]].vStartPos)
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]]].tlName)
							PRINTLN("--DO_TEMP_FREEMODE_MENU RETURNING TRUE--")
							PRINTLN("Warpping to MY mission start location - ", MenuVars.iSelectedRow[MenuVars.iTab][MenuVars.iSelectedColoum[MenuVars.iTab]])
							g_bFM_IgnoreRankOnNextLaunchedActivity  = TRUE
							g_bLaunchedViaZmenu = TRUE
							RETURN TRUE	
						//ELSE
							//PRINTLN("DO_TEMP_FREEMODE_MENU My mission STRING NULL")
						ENDIF
					//ELSE
						//PRINTLN("DO_TEMP_FREEMODE_MENU My mission VECTOR NULL")
					ENDIF
				BREAK
				CASE FMMC_MP_MENU_COLOUM_1
				CASE FMMC_MP_MENU_COLOUM_2
					IF NOT IS_VECTOR_ZERO(MenuVars.vReturn)
						//PRINTLN("Warpping to the ROCKSTAR DEATHMATCH start location - ", MenuVars.vReturn)
						g_bFM_IgnoreRankOnNextLaunchedActivity  = TRUE
						g_bLaunchedViaZmenu = TRUE
						RETURN TRUE	
					//ELSE
						//PRINTLN("DO_TEMP_FREEMODE_MENU ROCKSTAR DEATHMATCH VECTOR OR NAME NULL")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE NUM_TAB_1
			SWITCH MenuVars.iSelectedColoum[NUM_TAB_1]
				CASE FMMC_MP_MENU_COLOUM_0
				CASE FMMC_MP_MENU_COLOUM_1
				CASE FMMC_MP_MENU_COLOUM_2
					IF NOT IS_VECTOR_ZERO(MenuVars.vReturn)
						//PRINTLN("Warpping to the ROCKSTAR DEATHMATCH start location - ", MenuVars.vReturn)
						g_bFM_IgnoreRankOnNextLaunchedActivity  = TRUE
						g_bLaunchedViaZmenu = TRUE
						RETURN TRUE	
					ELSE
						PRINTLN("[ZMENU] DO_TEMP_FREEMODE_MENU ROCKSTAR DEATHMATCH VECTOR OR NAME NULL")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE NUM_TAB_2
			g_bLaunchedViaZmenu = TRUE
			RETURN TRUE		
		BREAK
				
		ENDSWITCH
	ENDIF

	//Calculate the current pane
	MenuVars.iCurrentPane[NUM_TAB_0] = FLOOR(TO_FLOAT(MenuVars.iSelectedRow[NUM_TAB_0][MenuVars.iSelectedColoum[NUM_TAB_0]])/ MAX_ITEMS_IN_PANE)
	MenuVars.iCurrentPane[NUM_TAB_1] = FLOOR(TO_FLOAT(MenuVars.iSelectedRow[NUM_TAB_1][MenuVars.iSelectedColoum[NUM_TAB_1]])/ MAX_ITEMS_IN_PANE)
	
	//Draw the header back ground
	IF MenuVars.iTab = NUM_TAB_0
		DRAW_RECT(0.3000, 0.1050, 0.3990, 0.0805, 255, 255, 255, 200)
		DRAW_RECT(0.7000, 0.1050, 0.3990, 0.0810, 0, 0, 0, 200)
		SET_UP_TITLE_MENU_TEXT(TRUE)
		DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.3, 0.075 , "FMMC_MPM_TI")
		SET_UP_TITLE_MENU_TEXT(FALSE)
		DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.7, 0.075 , "FMMC_MPM_T2")
	ELIF MenuVars.iTab = NUM_TAB_2
			DRAW_RECT(0.3000, 0.1050, 0.3990, 0.0805, 255, 255, 255, 200)
			DRAW_RECT(0.7000, 0.1050, 0.3990, 0.0810, 0, 0, 0, 200)
			SET_UP_TITLE_MENU_TEXT(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.3, 0.075 , "STRING", "Heists")		
	 // FEATURE_HEIST_PLANNING


	ELSE
		DRAW_RECT(0.3000, 0.1050, 0.3990, 0.0805, 0, 0, 0, 200)
		DRAW_RECT(0.7000, 0.1050, 0.3990, 0.0810, 255, 255, 255, 200)
		SET_UP_TITLE_MENU_TEXT(FALSE)
		DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.3, 0.075 , "FMMC_MPM_TI")
		SET_UP_TITLE_MENU_TEXT(TRUE)
		DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.7, 0.075 , "FMMC_MPM_T2")
	ENDIF
	
	BOOL bContentSetUp = GET_COMMANDLINE_PARAM_EXISTS("sc_CidInNameZ")
	INT iLength
	IF bContentSetUp
		iLength = GET_COMMANDLINE_PARAM_INT("sc_CidInNameZ")
		IF iLength < 0
			iLength = 3
		ENDIF
	ENDIF
	INT iItem
	BOOL bWhiteText=TRUE
	BOOL bIsActive = (iItem = iMissionToLoad)
//	INT iRockstarItem
	
			INT 			iLoop
			INT 			iLoop2		
			INT 			iDMCount			
			INT 			iRaceCount			
			FLOAT 			fTextXPos			
			FLOAT		 	fTextYPos			
			FLOAT			fBGXpos				
			FLOAT		 	fBGYpos		
			BOOL 			bDisplay
			INT 			iCurrentStart
			INT 			iDMCountIgnored
			INT 			iRaceCountIgnored
			INT 			iBaseCountIgnored 
			INT 			iBaseCount 
			
			
	SWITCH MenuVars.iTab
		CASE NUM_TAB_0
			//SET_UP_TITLE_MENU_TEXT()
			//DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.5, 0.075 , "FMMC_MPM_TI")
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE, TRUE, TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.225, 0.155, "FMMC_MPM_YR")	
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE, TRUE, TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.225 + FMMC_MP_MENU_COLOUM_WIDTH, 0.155, "FMMC_MPM_RDM")
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE, TRUE, TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.225 + FMMC_MP_MENU_COLOUM_WIDTH + FMMC_MP_MENU_COLOUM_WIDTH, 0.155, "FMMC_MPM_RR")

			//Do the Titles
			DRAW_RECT(0.225, 0.190, 0.250, 0.07 , 0, 0, 0, 200)
			DRAW_RECT(0.500, 0.190, 0.250, 0.07 , 0, 0, 0, 200)
			DRAW_RECT(0.775, 0.190, 0.250, 0.07 , 0, 0, 0, 200)
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(TEXT_ROW_0, 0.196, "FMMC_MPM_NA")	
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(TEXT_ROW_0 + 0.130, 0.196, "FMMC_MPM_TY")
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(TEXT_ROW_0 + 0.195, 0.196, "FMMC_MPM_AC")

			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(TEXT_ROW_0 + FMMC_MP_MENU_COLOUM_WIDTH, 0.196, "FMMC_MPM_NA")
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(TEXT_ROW_0 + 0.170 + FMMC_MP_MENU_COLOUM_WIDTH, 0.196, "FMMC_MPM_PL")
			
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(TEXT_ROW_0 + FMMC_MP_MENU_COLOUM_WIDTH + FMMC_MP_MENU_COLOUM_WIDTH, 0.196, "FMMC_MPM_NA")
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(TEXT_ROW_0 + 0.170 + FMMC_MP_MENU_COLOUM_WIDTH + FMMC_MP_MENU_COLOUM_WIDTH, 0.196, "FMMC_MPM_PL")

			//Do my races
			//Display the menu title	
			IF MenuVars.iCurrentPane[NUM_TAB_0] < (MAX_NUMBER_FMMC_SAVES/MAX_ITEMS_IN_PANE)
				IF MenuVars.iSelectedColoum[NUM_TAB_0] = FMMC_MP_MENU_COLOUM_0
					//DRAW_SPRITE("MPGangSelect", "MP_ArrowLarge", 0.225, 0.921 - 0.07, 0.05, 0.036, 90.0, 255, 255, 255, 255)
				ELIF MenuVars.iSelectedColoum[NUM_TAB_0] = FMMC_MP_MENU_COLOUM_1
					//DRAW_SPRITE("MPGangSelect", "MP_ArrowLarge", 0.5,   0.921 - 0.07, 0.05, 0.036, 90.0, 255, 255, 255, 255)
				ELIF MenuVars.iSelectedColoum[NUM_TAB_0] = FMMC_MP_MENU_COLOUM_2
					//DRAW_SPRITE("MPGangSelect", "MP_ArrowLarge", 0.775, 0.921 - 0.07, 0.05, 0.036, 90.0, 255, 255, 255, 255)
				ENDIF
			ENDIF
			
			FOR i = (0) TO (MAX_ITEMS_IN_PANE -1)
				iItem = i+(MAX_ITEMS_IN_PANE * MenuVars.iCurrentPane[NUM_TAB_0])
				
				//Coloum 0
				IF MenuVars.iSelectedColoum[NUM_TAB_0] = FMMC_MP_MENU_COLOUM_0
				AND MenuVars.iSelectedRow[NUM_TAB_0][MenuVars.iSelectedColoum[NUM_TAB_0]] = iItem
					DRAW_RECT(0.225, 0.243+ (0.034 * i), 0.250, 0.032 , 255, 255, 255, 200)
					bWhiteText = FALSE
				ELSE
					DRAW_RECT(0.225, 0.243+ (0.034 * i), 0.250, 0.032 , 0, 0, 0, 200)
					bWhiteText = TRUE
				ENDIF 
				
				IF iItem < MAX_NUMBER_FMMC_SAVES
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iItem].tlMissionName)
						SET_UP_TEMP_FREEMODE_MENU_TEXT(bWhiteText, FALSE, FALSE, bIsActive)
						DISPLAY_TEXT_WITH_MISSION_STRING(TEXT_ROW_0, 0.228 + (0.034 * i), "STRING", g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iItem].tlMissionName)
						SET_UP_TEMP_FREEMODE_MENU_TEXT(bWhiteText, FALSE, FALSE, bIsActive)		
						DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(TEXT_ROW_0 + 0.130, 0.228 + (0.034 * i), GET_MISSION_TYPE_STRING(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[iItem].iType))	
//					ELSE
//						#IF IS_DEBUG_BUILD
//						IF (GET_GAME_TIMER() % 5000) < 75
//							//PRINTLN(" IS_STRING_NULL_OR_EMPTY(g_FMMC_HEADER_STRUCT.saveName[", iItem, "])")
//						ENDIF
//						#ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			MenuVars.iTotalMissionCountDM = 0
			MenuVars.iTotalMissionCountRace = 0
			iCurrentStart = (MAX_ITEMS_IN_PANE * MenuVars.iCurrentPane[NUM_TAB_0])
			FOR iLoop = 0 TO (FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED - 1)
				bDisplay  = FALSE				
				IF IS_BIT_SET(s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
					IF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_DEATHMATCH	
						MenuVars.iTotalMissionCountDM++						
						IF iCurrentStart > 0
							IF iDMCountIgnored < iCurrentStart
								iDMCountIgnored ++
							ENDIF
						ENDIF
						IF iDMCountIgnored >= iCurrentStart
							IF iDMCount < MAX_ITEMS_IN_PANE
								bDisplay = TRUE
								IF MenuVars.iSelectedColoum[NUM_TAB_0] = FMMC_MP_MENU_COLOUM_1
								AND MenuVars.iSelectedRow[NUM_TAB_0][MenuVars.iSelectedColoum[NUM_TAB_0]] = (iDMCount+iDMCountIgnored)
									MenuVars.vReturn = s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].vStartPos
									MenuVars.tlName = s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlName
									MenuVars.iCreator = FMMC_ROCKSTAR_CREATOR_ID
									MenuVars.iVariation = iLoop
									MenuVars.iRootContentID = s_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iLoop].iRootContentIdHash
									bWhiteText = FALSE
								ELSE
									bWhiteText = TRUE
								ENDIF
								fBGXpos = 0.5
								fBGYpos = cfBG_START_HEIGHT + (iDMCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								fTextXPos = TEXT_ROW_0 + FMMC_MP_MENU_COLOUM_WIDTH
								fTextYPos = cfTEXT_START_HEIGHT+(iDMCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								iDMCount++
							ENDIF
						ENDIF
					ELIF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_RACE
						MenuVars.iTotalMissionCountRace++
						IF iCurrentStart > 0
							IF iRaceCountIgnored < iCurrentStart
								iRaceCountIgnored ++
							ENDIF
						ENDIF
						IF iRaceCountIgnored >= iCurrentStart
							IF iRaceCount < MAX_ITEMS_IN_PANE
								bDisplay = TRUE
								IF MenuVars.iSelectedColoum[NUM_TAB_0] = FMMC_MP_MENU_COLOUM_2
								AND MenuVars.iSelectedRow[NUM_TAB_0][MenuVars.iSelectedColoum[NUM_TAB_0]] = (iRaceCount + iRaceCountIgnored)
									MenuVars.vReturn = s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].vStartPos
									MenuVars.tlName = s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlName
									MenuVars.iRootContentID = s_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iLoop].iRootContentIdHash
									MenuVars.iCreator = FMMC_ROCKSTAR_CREATOR_ID
									MenuVars.iVariation = iLoop
									bWhiteText = FALSE
								ELSE
									bWhiteText = TRUE
								ENDIF
								fBGXpos = 0.775
								fBGYpos = cfBG_START_HEIGHT + (iRaceCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								fTextXPos = TEXT_ROW_0 + FMMC_MP_MENU_COLOUM_WIDTH + FMMC_MP_MENU_COLOUM_WIDTH
								fTextYPos = cfTEXT_START_HEIGHT+ (iRaceCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								iRaceCount++
							ENDIF
						ENDIF
					ENDIF
					IF iItem < FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED
					AND bDisplay
						//Rockstar Deathmatch
						IF bWhiteText = FALSE
							DRAW_RECT(fBGXpos, fBGYpos, 0.250, 0.032 , 255, 255, 255, 200)
						ELSE
							DRAW_RECT(fBGXpos, fBGYpos, 0.250, 0.032 , 0, 0, 0, 200)
						ENDIF
						SET_UP_TEMP_FREEMODE_MENU_TEXT(bWhiteText)
						IF bContentSetUp
							DISPLAY_TEXT_WITH_MISSION_STRING_AND_CID(fTextXPos, fTextYPos, "FM_Z_D", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlName, iLength)
						ELIF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iLesRating <= 0
							IF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iContentVersion <= 0
								DISPLAY_TEXT_WITH_MISSION_STRING(fTextXPos, fTextYPos, "STRING", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlMissionName)
							ELSE
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_VERSION(fTextXPos, fTextYPos, "FM_Z_V", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iContentVersion)
							ENDIF
						ELSE
							IF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iContentVersion <= 0
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_LES_RATING(fTextXPos, fTextYPos, "FM_Z_LES", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iLesRating)
							ELSE
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_LES_RATING_AND_VERSION(fTextXPos, fTextYPos, "FM_Z_LESV", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iLesRating, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iContentVersion)
							ENDIF
						ENDIF
						IF NOT bContentSetUp
							SET_UP_TEMP_FREEMODE_MENU_TEXT(bWhiteText)
							IF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iRating <= 0
								DISPLAY_DEBUG_TEXT_WITH_NUMBER(fTextXPos + tfRATING_MOVE_CONST, fTextYPos, "FMMC_NOTRATED", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iRank)
							ELSE
								DISPLAY_DEBUG_TEXT_WITH_2_NUMBERS(fTextXPos + tfRATING_MOVE_CONST, fTextYPos, "FMMC_MPM_RTD", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iRating, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iRank)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			FOR iLoop = 0 TO (FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED  - 1)
				bDisplay  = FALSE				
				//IF s_FMMC_ROCKSTAR_CANDIDATE.bInUse[iLoop]
				IF IS_BIT_SET(s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
					IF s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_DEATHMATCH	
						MenuVars.iTotalMissionCountDM++						
						IF iCurrentStart > 0
							IF iDMCountIgnored < iCurrentStart
								iDMCountIgnored ++
							ENDIF
						ENDIF
						IF iDMCountIgnored >= iCurrentStart
							IF iDMCount < MAX_ITEMS_IN_PANE
								bDisplay = TRUE
								IF MenuVars.iSelectedColoum[NUM_TAB_0] = FMMC_MP_MENU_COLOUM_1
								AND MenuVars.iSelectedRow[NUM_TAB_0][MenuVars.iSelectedColoum[NUM_TAB_0]] = (iDMCount+iDMCountIgnored)
									MenuVars.vReturn = s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].vStartPos
									MenuVars.tlName = s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlName
									MenuVars.iRootContentID = s_FMMC_ROCKSTAR_CANDIDATE.sDefaultCoronaOptions[iLoop].iRootContentIdHash
									MenuVars.iCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
									MenuVars.iVariation = iLoop
									bWhiteText = FALSE
								ELSE
									bWhiteText = TRUE
								ENDIF
								fBGXpos = 0.5
								fBGYpos = cfBG_START_HEIGHT + (iDMCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								fTextXPos = TEXT_ROW_0 + FMMC_MP_MENU_COLOUM_WIDTH
								fTextYPos = cfTEXT_START_HEIGHT+(iDMCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								iDMCount++
							ENDIF
						ENDIF
					ELIF s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_RACE
						MenuVars.iTotalMissionCountRace++
						IF iCurrentStart > 0
							IF iRaceCountIgnored < iCurrentStart
								iRaceCountIgnored ++
							ENDIF
						ENDIF
						IF iRaceCountIgnored >= iCurrentStart
							IF iRaceCount < MAX_ITEMS_IN_PANE
								bDisplay = TRUE
								IF MenuVars.iSelectedColoum[NUM_TAB_0] = FMMC_MP_MENU_COLOUM_2
								AND MenuVars.iSelectedRow[NUM_TAB_0][MenuVars.iSelectedColoum[NUM_TAB_0]] = (iRaceCount + iRaceCountIgnored)
									MenuVars.vReturn = s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].vStartPos
									MenuVars.tlName = s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlName
									MenuVars.iRootContentID = s_FMMC_ROCKSTAR_CANDIDATE.sDefaultCoronaOptions[iLoop].iRootContentIdHash
									MenuVars.iCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
									MenuVars.iVariation = iLoop
									bWhiteText = FALSE
								ELSE
									bWhiteText = TRUE
								ENDIF
								fBGXpos = 0.775
								fBGYpos = cfBG_START_HEIGHT + (iRaceCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								fTextXPos = TEXT_ROW_0 + FMMC_MP_MENU_COLOUM_WIDTH + FMMC_MP_MENU_COLOUM_WIDTH
								fTextYPos = cfTEXT_START_HEIGHT+ (iRaceCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								iRaceCount++
							ENDIF
						ENDIF
					ENDIF
					IF bDisplay
						//Rockstar Deathmatch
						SET_UP_TEMP_FREEMODE_MENU_TEXT(bWhiteText)
						IF bWhiteText = FALSE
							DRAW_RECT(fBGXpos, fBGYpos, 0.250, 0.032 , 255, 255, 255, 200)
							SET_TEXT_COLOUR(180, 180, 10, 255)
						ELSE
							DRAW_RECT(fBGXpos, fBGYpos, 0.250, 0.032 , 0, 0, 0, 200)
							SET_TEXT_COLOUR(255, 255, 133, 255)
						ENDIF
						
						IF bContentSetUp
							DISPLAY_TEXT_WITH_MISSION_STRING_AND_CID(fTextXPos, fTextYPos, "FM_Z_D", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlName, iLength)
						ELIF s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iLesRating <= 0
							IF s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iContentVersion <= 0
								DISPLAY_TEXT_WITH_MISSION_STRING(fTextXPos, fTextYPos, "STRING", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlMissionName)
							ELSE
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_VERSION(fTextXPos, fTextYPos, "FM_Z_V", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iContentVersion)
							ENDIF
						ELSE
							IF s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iContentVersion <= 0
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_LES_RATING(fTextXPos, fTextYPos, "FM_Z_LES", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iLesRating)
							ELSE
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_LES_RATING_AND_VERSION(fTextXPos, fTextYPos, "FM_Z_LESV", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iLesRating, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iContentVersion)
							ENDIF
						ENDIF
						IF NOT bContentSetUp
							SET_UP_TEMP_FREEMODE_MENU_TEXT(bWhiteText)
							IF s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iRating <= 0						
								DISPLAY_DEBUG_TEXT_WITH_NUMBER(fTextXPos + tfRATING_MOVE_CONST, fTextYPos, "FMMC_NOTRATED", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iRank)
							ELSE
								DISPLAY_DEBUG_TEXT_WITH_2_NUMBERS(fTextXPos + tfRATING_MOVE_CONST, fTextYPos, "FMMC_MPM_RTD", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iRating, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iRank)							
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		
		CASE NUM_TAB_1
			//Tab title
			//SET_UP_TITLE_MENU_TEXT()
			//DISPLAY_TEXT(0.5, 0.075 , "FMMC_MPM_T2")
			
			//Coloum Title
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE, TRUE, TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.225, 0.155, "FMMC_MPM_RG")
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE, TRUE, TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.225 + FMMC_MP_MENU_COLOUM_WIDTH, 0.155, "FMMC_MPM_RG1")
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE, TRUE, TRUE)
			DISPLAY_TEXT_FOR_FMMC_DEBUG_MENU(0.225 + FMMC_MP_MENU_COLOUM_WIDTH + FMMC_MP_MENU_COLOUM_WIDTH, 0.155, "FMMC_MPM_RG2")
			//Do the Titles
			DRAW_RECT(0.225, 0.190, 0.250, 0.07 , 0, 0, 0, 200)
			DRAW_RECT(0.500, 0.190, 0.250, 0.07 , 0, 0, 0, 200)
			DRAW_RECT(0.775, 0.190, 0.250, 0.07 , 0, 0, 0, 200)

			
			MenuVars.iTotalMissionCountDM = 0
			MenuVars.iTotalMissionCountRace = 0
			MenuVars.iTotalMissionCountBase = 0
			iCurrentStart = (MAX_ITEMS_IN_PANE * MenuVars.iCurrentPane[NUM_TAB_1])
			FOR iLoop = 0 TO (FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED - 1)
				bDisplay  = FALSE				
				IF IS_BIT_SET(s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
			//	s_FMMC_ROCKSTAR_CREATED.bInUse[iLoop]
					IF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_MISSION
						MenuVars.iTotalMissionCountDM++						
						IF iCurrentStart > 0
							IF iDMCountIgnored < iCurrentStart
								iDMCountIgnored ++
							ENDIF
						ENDIF
						IF iDMCountIgnored >= iCurrentStart
							IF iDMCount < MAX_ITEMS_IN_PANE
								bDisplay = TRUE
								IF MenuVars.iSelectedColoum[NUM_TAB_1] = FMMC_MP_MENU_COLOUM_1
								AND MenuVars.iSelectedRow[NUM_TAB_1][MenuVars.iSelectedColoum[NUM_TAB_1]] = (iDMCount+iDMCountIgnored)
									MenuVars.vReturn = s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].vStartPos
									MenuVars.tlName = s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlName
									MenuVars.iRootContentID = s_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iLoop].iRootContentIdHash
									MenuVars.iCreator = FMMC_ROCKSTAR_CREATOR_ID
									MenuVars.iVariation = iLoop
									bWhiteText = FALSE
								ELSE
									bWhiteText = TRUE
								ENDIF
								fBGXpos = 0.5
								fBGYpos = cfBG_START_HEIGHT + (iDMCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								fTextXPos = TEXT_ROW_0 + FMMC_MP_MENU_COLOUM_WIDTH
								fTextYPos = cfTEXT_START_HEIGHT+(iDMCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								iDMCount++
							ENDIF
						ENDIF
					ELIF (s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_SURVIVAL
					OR s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_GANGHIDEOUT)
						MenuVars.iTotalMissionCountRace++
						IF iCurrentStart > 0
							IF iRaceCountIgnored < iCurrentStart
								iRaceCountIgnored ++
							ENDIF
						ENDIF
						IF iRaceCountIgnored >= iCurrentStart
							IF iRaceCount < MAX_ITEMS_IN_PANE
								bDisplay = TRUE
								IF MenuVars.iSelectedColoum[NUM_TAB_1] = FMMC_MP_MENU_COLOUM_2
								AND MenuVars.iSelectedRow[NUM_TAB_1][MenuVars.iSelectedColoum[NUM_TAB_1]] = (iRaceCount + iRaceCountIgnored)
									MenuVars.vReturn = s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].vStartPos
									MenuVars.tlName = s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlName
									MenuVars.iRootContentID = s_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iLoop].iRootContentIdHash
									MenuVars.iCreator = FMMC_ROCKSTAR_CREATOR_ID
									MenuVars.iVariation = iLoop
									bWhiteText = FALSE
								ELSE
									bWhiteText = TRUE
								ENDIF
								fBGXpos = 0.775
								fBGYpos = cfBG_START_HEIGHT + (iRaceCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								fTextXPos = TEXT_ROW_0 + FMMC_MP_MENU_COLOUM_WIDTH + FMMC_MP_MENU_COLOUM_WIDTH
								fTextYPos = cfTEXT_START_HEIGHT+ (iRaceCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								iRaceCount++
							ENDIF
						ENDIF
					ELIF (s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_BASE_JUMP)
						MenuVars.iTotalMissionCountBase++
						IF iCurrentStart > 0
							IF iBaseCountIgnored < iCurrentStart
								iBaseCountIgnored ++
							ENDIF
						ENDIF
						IF iBaseCountIgnored >= iCurrentStart
							IF iBaseCount < MAX_ITEMS_IN_PANE
								bDisplay = TRUE
								IF MenuVars.iSelectedColoum[NUM_TAB_1] = FMMC_MP_MENU_COLOUM_0
								AND MenuVars.iSelectedRow[NUM_TAB_1][MenuVars.iSelectedColoum[NUM_TAB_1]] = (iBaseCount + iBaseCountIgnored)
									MenuVars.vReturn = s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].vStartPos
									MenuVars.tlName = s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlName
									MenuVars.iRootContentID = s_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iLoop].iRootContentIdHash
									MenuVars.iCreator = FMMC_ROCKSTAR_CREATOR_ID
									MenuVars.iVariation = iLoop
									bWhiteText = FALSE
								ELSE
									bWhiteText = TRUE
								ENDIF
								fBGXpos = 0.225
								fBGYpos = cfBG_START_HEIGHT + (iBaseCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								fTextXPos = TEXT_ROW_0
								fTextYPos = cfTEXT_START_HEIGHT+ (iBaseCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								iBaseCount++
							ENDIF
						ENDIF
					ENDIF
					IF iItem < FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED
					AND bDisplay
						//Rockstar Deathmatch
						IF bWhiteText = FALSE
							DRAW_RECT(fBGXpos, fBGYpos, 0.250, 0.032 , 255, 255, 255, 200)
						ELSE
							DRAW_RECT(fBGXpos, fBGYpos, 0.250, 0.032 , 0, 0, 0, 200)
						ENDIF
						SET_UP_TEMP_FREEMODE_MENU_TEXT(bWhiteText)
						IF bContentSetUp
							DISPLAY_TEXT_WITH_MISSION_STRING_AND_CID(fTextXPos, fTextYPos, "FM_Z_D", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlName, iLength)
						ELIF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iLesRating <= 0
							IF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iContentVersion <= 0
								DISPLAY_TEXT_WITH_MISSION_STRING(fTextXPos, fTextYPos, "STRING", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlMissionName)
							ELSE
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_VERSION(fTextXPos, fTextYPos, "FM_Z_V", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iContentVersion)
							ENDIF
						ELSE
							IF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iContentVersion <= 0
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_LES_RATING(fTextXPos, fTextYPos, "FM_Z_LES", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iLesRating)
							ELSE
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_LES_RATING_AND_VERSION(fTextXPos, fTextYPos, "FM_Z_LESV", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iLesRating, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iContentVersion)
							ENDIF
						ENDIF
						IF NOT bContentSetUp
							SET_UP_TEMP_FREEMODE_MENU_TEXT(bWhiteText)
							IF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iRating <= 0
								DISPLAY_DEBUG_TEXT_WITH_NUMBER(fTextXPos + tfRATING_MOVE_CONST, fTextYPos, "FMMC_NOTRATED", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iRank)
							ELSE
								DISPLAY_DEBUG_TEXT_WITH_2_NUMBERS(fTextXPos + tfRATING_MOVE_CONST, fTextYPos, "FMMC_MPM_RTD", s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iRating, s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iRank)							
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			FOR iLoop = 0 TO (FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED - 1)
				bDisplay  = FALSE				
				//IF s_FMMC_ROCKSTAR_CANDIDATE.bInUse[iLoop]
				IF IS_BIT_SET(s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
					IF s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_MISSION	
						MenuVars.iTotalMissionCountDM++						
						IF iCurrentStart > 0
							IF iDMCountIgnored < iCurrentStart
								iDMCountIgnored ++
							ENDIF
						ENDIF
						IF iDMCountIgnored >= iCurrentStart
							IF iDMCount < MAX_ITEMS_IN_PANE
								bDisplay = TRUE
								IF MenuVars.iSelectedColoum[NUM_TAB_1] = FMMC_MP_MENU_COLOUM_1
								AND MenuVars.iSelectedRow[NUM_TAB_1][MenuVars.iSelectedColoum[NUM_TAB_1]] = (iDMCount+iDMCountIgnored)
									MenuVars.vReturn = s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].vStartPos
									MenuVars.tlName = s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlName
									MenuVars.iRootContentID = s_FMMC_ROCKSTAR_CANDIDATE.sDefaultCoronaOptions[iLoop].iRootContentIdHash
									MenuVars.iCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
									MenuVars.iVariation = iLoop
									bWhiteText = FALSE
								ELSE
									bWhiteText = TRUE
								ENDIF
								fBGXpos = 0.5
								fBGYpos = cfBG_START_HEIGHT + (iDMCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								fTextXPos = TEXT_ROW_0 + FMMC_MP_MENU_COLOUM_WIDTH
								fTextYPos = cfTEXT_START_HEIGHT+(iDMCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								iDMCount++
							ENDIF
						ENDIF
					ELIF (s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_SURVIVAL
					OR s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_GANGHIDEOUT)
						MenuVars.iTotalMissionCountRace++
						IF iCurrentStart > 0
							IF iRaceCountIgnored < iCurrentStart
								iRaceCountIgnored ++
							ENDIF
						ENDIF
						IF iRaceCountIgnored >= iCurrentStart
							IF iRaceCount < MAX_ITEMS_IN_PANE
								bDisplay = TRUE
								IF MenuVars.iSelectedColoum[NUM_TAB_1] = FMMC_MP_MENU_COLOUM_2
								AND MenuVars.iSelectedRow[NUM_TAB_1][MenuVars.iSelectedColoum[NUM_TAB_1]] = (iRaceCount + iRaceCountIgnored)
									MenuVars.vReturn = s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].vStartPos
									MenuVars.tlName = s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlName
									MenuVars.iRootContentID = s_FMMC_ROCKSTAR_CANDIDATE.sDefaultCoronaOptions[iLoop].iRootContentIdHash
									MenuVars.iCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
									MenuVars.iVariation = iLoop
									bWhiteText = FALSE
								ELSE
									bWhiteText = TRUE
								ENDIF
								fBGXpos = 0.775
								fBGYpos = cfBG_START_HEIGHT + (iRaceCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								fTextXPos = TEXT_ROW_0 + FMMC_MP_MENU_COLOUM_WIDTH + FMMC_MP_MENU_COLOUM_WIDTH
								fTextYPos = cfTEXT_START_HEIGHT+ (iRaceCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								iRaceCount++
							ENDIF
						ENDIF
					ELIF (s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iType = FMMC_TYPE_BASE_JUMP)
						MenuVars.iTotalMissionCountBase++
						IF iCurrentStart > 0
							IF iBaseCountIgnored < iCurrentStart
								iBaseCountIgnored ++
							ENDIF
						ENDIF
						IF iBaseCountIgnored >= iCurrentStart
							IF iBaseCount < MAX_ITEMS_IN_PANE
								bDisplay = TRUE
								IF MenuVars.iSelectedColoum[NUM_TAB_1] = FMMC_MP_MENU_COLOUM_0
								AND MenuVars.iSelectedRow[NUM_TAB_1][MenuVars.iSelectedColoum[NUM_TAB_1]] = (iBaseCount + iBaseCountIgnored)
									MenuVars.vReturn = s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].vStartPos
									MenuVars.tlName = s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlName
									MenuVars.iCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
									MenuVars.iRootContentID = s_FMMC_ROCKSTAR_CANDIDATE.sDefaultCoronaOptions[iLoop].iRootContentIdHash
									MenuVars.iVariation = iLoop
									bWhiteText = FALSE
								ELSE
									bWhiteText = TRUE
								ENDIF
								fBGXpos = 0.225
								fBGYpos = cfBG_START_HEIGHT + (iBaseCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								fTextXPos = TEXT_ROW_0
								fTextYPos = cfTEXT_START_HEIGHT+ (iBaseCount * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
								iBaseCount++
							ENDIF
						ENDIF
					ENDIF
					IF bDisplay
						//Rockstar Deathmatch
						SET_UP_TEMP_FREEMODE_MENU_TEXT(bWhiteText)
						IF bWhiteText = FALSE
							DRAW_RECT(fBGXpos, fBGYpos, 0.250, 0.032 , 255, 255, 255, 200)
							SET_TEXT_COLOUR(180, 180, 10, 255)
						ELSE
							DRAW_RECT(fBGXpos, fBGYpos, 0.250, 0.032 , 0, 0, 0, 200)
							SET_TEXT_COLOUR(255, 255, 133, 255)
						ENDIF
						IF bContentSetUp
							DISPLAY_TEXT_WITH_MISSION_STRING_AND_CID(fTextXPos, fTextYPos, "FM_Z_D", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlName, iLength)
						ELIF s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iLesRating = 0
							IF s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iContentVersion <= 0
								DISPLAY_TEXT_WITH_MISSION_STRING(fTextXPos, fTextYPos, "STRING", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlMissionName)
							ELSE
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_VERSION(fTextXPos, fTextYPos, "FM_Z_V", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iContentVersion)
							ENDIF
						ELSE
							IF s_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iLoop].iContentVersion <= 0
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_LES_RATING(fTextXPos, fTextYPos, "FM_Z_LES", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iLesRating)
							ELSE
								DISPLAY_TEXT_WITH_MISSION_STRING_AND_LES_RATING_AND_VERSION(fTextXPos, fTextYPos, "FM_Z_LESV", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].tlMissionName, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iLesRating, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iContentVersion)
							ENDIF
						ENDIF
						IF NOT bContentSetUp
							SET_UP_TEMP_FREEMODE_MENU_TEXT(bWhiteText)
							IF s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iRating <= 0
								DISPLAY_DEBUG_TEXT_WITH_NUMBER(fTextXPos + tfRATING_MOVE_CONST, fTextYPos, "FMMC_NOTRATED", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iRank)
							ELSE	
								DISPLAY_DEBUG_TEXT_WITH_2_NUMBERS(fTextXPos + tfRATING_MOVE_CONST, fTextYPos, "FMMC_MPM_RTD", s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iRating, s_FMMC_ROCKSTAR_CANDIDATE.sMissionHeaderVars[iLoop].iRank)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		
		CASE NUM_TAB_2
			//Tab title
			//SET_UP_TITLE_MENU_TEXT()
			//DISPLAY_TEXT(0.5, 0.075 , "FMMC_MPM_T2")
			
			//Coloum Title
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE, TRUE, TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.225, 0.155, "STRING", TEMP_Debug_GET_Planning_Mission_Name(g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job, -1))
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE, TRUE, TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.225 + FMMC_MP_MENU_COLOUM_WIDTH, 0.155, "STRING", TEMP_Debug_GET_Planning_Mission_Name(g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid, -1))
			SET_UP_TEMP_FREEMODE_MENU_TEXT(TRUE, TRUE, TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.225 + FMMC_MP_MENU_COLOUM_WIDTH + FMMC_MP_MENU_COLOUM_WIDTH, 0.155, "STRING", TEMP_Debug_GET_Planning_Mission_Name(g_sMPTUNABLES.iroot_id_HASH_The_Prison_Break, -1))
			//Do the Titles
			DRAW_RECT(0.225, 0.190, 0.250, 0.07 , 0, 0, 0, 200)
			DRAW_RECT(0.500, 0.190, 0.250, 0.07 , 0, 0, 0, 200)
			DRAW_RECT(0.775, 0.190, 0.250, 0.07 , 0, 0, 0, 200)
			
			MenuVars.iTotalMissionCountDM = 0
			MenuVars.iTotalMissionCountRace = 0
			MenuVars.iTotalMissionCountBase = 0
			iCurrentStart = (MAX_ITEMS_IN_PANE * MenuVars.iCurrentPane[NUM_TAB_2])
			INT iHeistStrand
			FOR iLoop = 0 TO (MAX_NUM_COLUMNS - 1)
				iHeistStrand = GET_HEIST_STRAND_FROM_LOOP(iLoop)
				fTextXPos = TEXT_ROW_0 + (FMMC_MP_MENU_COLOUM_WIDTH * iLoop)
				IF iLoop = 0
					fBGXpos = 0.225
				ELIF iLoop = 1
					fBGXpos = 0.5
				ELIF iLoop = 2
					fBGXpos = 0.775
				ENDIF
				FOR iLoop2 = 0 TO (GET_HEIST_STRAND_LENGTH(iHeistStrand) - 1)
					fTextYPos = cfTEXT_START_HEIGHT+(iLoop2 * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
					IF MenuVars.iSelectedColoum[NUM_TAB_2] = iLoop
					AND MenuVars.iSelectedRow[NUM_TAB_2][MenuVars.iSelectedColoum[NUM_TAB_2]] = iLoop2
						bWhiteText = FALSE
					ELSE
						bWhiteText = TRUE
					ENDIF
					fBGYpos = cfBG_START_HEIGHT + (iLoop2 * cfFMMC_IN_GAME_MENU_LINE_HEIGHT)
					//Rockstar Deathmatch
					SET_UP_TEMP_FREEMODE_MENU_TEXT(bWhiteText)
					IF bWhiteText = FALSE
						DRAW_RECT(fBGXpos, fBGYpos, 0.250, 0.032 , 255, 255, 255, 200)
					ELSE
						DRAW_RECT(fBGXpos, fBGYpos, 0.250, 0.032 , 0, 0, 0, 200)
					ENDIF
					DISPLAY_TEXT_WITH_MISSION_STRING_AND_LES_RATING(fTextXPos, fTextYPos, "FM_Z_LES", TEMP_Debug_GET_Planning_Mission_Name(iHeistStrand, iLoop2), 0)
				ENDFOR
			ENDFOR			
		BREAK
		
		
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC 
#ENDIF

