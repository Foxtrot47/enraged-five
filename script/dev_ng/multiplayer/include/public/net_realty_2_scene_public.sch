USING "SceneTool_public.sch"

USING "cutscene_public.sch"
USING "rc_helper_functions.sch"

#IF IS_DEBUG_BUILD
USING "net_realty_scene_debug.sch"
#ENDIF

ENUM enumnet_realty_2_scenePans
	NET_REALTY_2_SCENE_PAN_descent,
	NET_REALTY_2_SCENE_PAN_MAX
ENDENUM

ENUM enumnet_realty_2_sceneCuts
	NET_REALTY_2_SCENE_CUT_start = 0,
	NET_REALTY_2_SCENE_CUT_MAX
ENDENUM

ENUM enumnet_realty_2_sceneMarkers
	NET_REALTY_2_SCENE_MARKER_gotoA = 0,
	NET_REALTY_2_SCENE_MARKER_findCoord,
	NET_REALTY_2_SCENE_MARKER_gotoB,
	NET_REALTY_2_SCENE_MARKER_MAX
ENDENUM

ENUM enumnet_realty_2_scenePlacers
	NET_REALTY_2_SCENE_PLACER_resetCoords = 0,
	NET_REALTY_2_SCENE_PLACER_gotoA,
	NET_REALTY_2_SCENE_PLACER_MAX
ENDENUM

ENUM enumnet_realty_2_sceneAngArea
	NET_REALTY_2_SCENE_ANGAREA_area0 = 0,
	NET_REALTY_2_SCENE_ANGAREA_MAX
ENDENUM

STRUCT STRUCT_NET_REALTY_2_SCENE
	structSceneTool_Pan		mPans[NET_REALTY_2_SCENE_PAN_MAX]
	structSceneTool_Cut		mCuts[NET_REALTY_2_SCENE_CUT_MAX]
	
	structSceneTool_Marker	mMarkers[NET_REALTY_2_SCENE_MARKER_MAX]
	structSceneTool_Placer	mPlacers[NET_REALTY_2_SCENE_PLACER_MAX]
	
	structSceneTool_AngArea	mAngAreas[NET_REALTY_2_SCENE_ANGAREA_MAX]
	
	BOOL					bEnablePans[NET_REALTY_2_SCENE_PAN_MAX]
	FLOAT					fExitDelay
ENDSTRUCT

FUNC BOOL Private_Get_net_realty_2_scene(INT iBuildingID, STRUCT_NET_REALTY_2_SCENE& scene	#IF ACTIVATE_PROPERTY_CS_DEBUG	,	BOOL bIgnoreAssert = FALSE	#ENDIF	)
	SWITCH iBuildingID
		CASE 0	RETURN FALSE	BREAK
		
		//Luke Howard, 20/09/2013 15:38 (#1642223)
		CASE MP_PROPERTY_BUILDING_1	//High apt 1,2,3,4
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = <<-799.0720, 320.6587, 86.3632>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = <<-0.0375, 0.0000, -158.5483>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = 38.0701
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = <<-799.0720, 320.6587, 86.3632>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = <<-0.0375, 0.0000, -158.5483>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = <<-798.6212, 321.0813, 86.3633>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = <<-1.1295, 0.0000, -156.4889>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = 38.0701
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 5.0000
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = <<-796.1236, 312.4341, 84.7060>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos = <<-795.8869, 317.6662, 84.6711>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = <<-795.2998, 334.9379, 84.7015>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = <<-796.0367, 310.9356, 84.7103>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 8.0000
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos = <<-795.9910, 310.7705, 84.7107>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot = 8.0000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = <<-801.0840, 311.6861, 84.9301>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = <<-789.5790, 311.8261, 84.7084>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 4.7500
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 4.1250
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_BUILDING_17
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = <<-28.4072, -70.9489, 61.1346>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = <<-14.7019, -0.0000, 30.9941>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = 50.0000
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = <<-28.4072, -70.9489, 61.1346>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = <<-14.7019, -0.0000, 30.9941>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = <<-27.9463, -71.7163, 61.3694>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = <<-14.7019, -0.0000, 30.9941>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = 50.0000
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 6.5000
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = <<-30.7467, -65.8139, 58.5436>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos = <<-31.4669, -65.4419, 58.5525>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = <<-31.8227, -64.9260, 58.5754>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = <<-29.2472, -66.5444, 58.5276>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 0.0000
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos = <<-32.0059, -65.5429, 58.5382>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot = 0.0000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = <<-32.8878, -64.7022, 58.5703>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = <<-36.1431, -63.7190, 58.5692>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 3.0000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 3.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_BUILDING_24
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = <<973.3799, -1017.0472, 44.9454>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = <<-20.8093, -0.0000, 118.7105>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = 50.0000
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = <<973.3799, -1017.0472, 44.9454>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = <<-20.8093, -0.0000, 118.7105>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = <<972.7641, -1017.3845, 44.6786>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = <<-20.8093, -0.0000, 118.7105>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = 50.0000
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 6.5000
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos = <<964.5840, -1024.7875, 39.8475>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = <<964.5313, -1027.4164, 39.8475>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = <<963.9338, -1022.5884, 39.8475>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 90.0000
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos = <<965.1632, -1026.4447, 39.8475>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot = 135.0000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = <<963.0906, -1027.9738, 39.9060>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = <<962.9706, -1023.0401, 39.8475>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 2.0000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 3.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		

		CASE MP_PROPERTY_BUILDING_51	//garage new 18				
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = <<873.3696, -2235.5352, 32.9233>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = <<-5.3132, -0.0000, 26.6492>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = <<873.3696, -2235.5352, 32.9233>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = <<-16.2593, 0.0000, 26.6492>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = 50.0000
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 6.5000
			
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake
			
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = <<870.9843, -2229.7058, 29.5195>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = <<871.2320, -2227.6548, 29.5195>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = <<870.5704, -2232.8079, 29.5401>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 0.0000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = <<868.8420, -2231.7966, 30.0477>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = <<872.7557, -2232.1396, 29.8316>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 1.1250
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 1.1250
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN FALSE	//fix for bug 1618035
		BREAK
		
		//Forrest Karbowski, Thu 12/09/2013 23:29
		CASE MP_PROPERTY_BUILDING_6          //High apt 14,15
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = <<-879.3593, -352.8495, 35.5138>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = <<10.7143, -0.0000, -171.2698>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = 50.0000
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = 0.0000
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = <<-879.3593, -352.8495, 35.5138>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = <<10.7143, -0.0000, -171.2698>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = <<-879.3593, -352.8495, 35.5138>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = <<2.8042, 0.0000, -178.5876>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = 50.0000
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 6.5000
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = <<-883.6517, -348.2842, 33.5340>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos = <<-883.7838, -347.9753, 33.5340>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = <<-894.7950, -340.3699, 33.5340>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = <<-876.0183, -361.2808, 35.0158>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 24.4800
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos = <<-876.0363, -361.2022, 35.0044>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot = 32.0400
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = <<-880.0811, -360.5822, 34.8559>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = <<-875.8514, -358.5576, 34.8860>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 3.3000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 3.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 20/09/2013 15:38 (#1642223)
		CASE MP_PROPERTY_BUILDING_7	//High apt 16,17
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = <<-624.4329, 59.0079, 44.3892>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = <<-1.4600, 0.0000, 107.5935>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = 41.3305
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = <<-624.4329, 59.0079, 44.3892>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = <<-1.4600, 0.0000, 107.5935>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = <<-624.1620, 58.7679, 44.3938>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = <<-1.4600, 0.0000, 113.4649>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = 41.3305
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.2540
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 6.0000
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = <<-622.7378, 55.6352, 42.7313>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos = <<-622.7034, 55.6411, 42.7314>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = <<-618.7881, 64.6789, 42.7423>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = <<-631.6893, 54.7157, 42.7250>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 272.8800
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos = <<-631.8889, 54.6938, 42.7250>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot = 272.8200
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = <<-629.9460, 60.9404, 42.7918>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = <<-629.8743, 52.2069, 42.7250>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 3.5000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Thu 12/09/2013 23:29
		CASE MP_PROPERTY_BUILDING_5          //High apt 12,13
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = <<-20.3428, -626.4957, 38.5043>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = <<18.2283, -0.0000, 69.3122>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = 50.0000
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = 0.0000
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = <<-20.3428, -626.4957, 38.5043>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = <<18.2283, -0.0000, 69.3122>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = <<-19.2934, -626.8920, 38.2333>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = <<-11.1781, 0.0000, 69.3122>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = 50.0000
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 6.5000
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = <<-41.4098, -621.9307, 34.0381>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos = <<-41.4407, -622.0095, 34.0486>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = <<-37.8465, -616.8147, 34.0956>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = <<-28.9981, -622.2560, 34.3170>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 69.1200
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos = <<-29.3961, -622.1253, 34.2924>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot = 69.1200
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = <<-34.1868, -625.8323, 34.2464>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = <<-31.3887, -618.1778, 34.3354>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 2.0000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 5.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Thu 29/08/2013 16:59
		CASE MP_PROPERTY_BUILDING_2          //High apt 5,6
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = <<-275.5190, -994.7433, 25.9268>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = <<14.8533, -0.0000, 95.4414>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = 39.8202
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = <<-275.5190, -994.7433, 25.9268>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = <<14.8533, -0.0000, 95.4414>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = <<-275.5190, -994.7433, 25.9268>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = <<-11.3264, -0.0000, 89.9160>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = 39.8202
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 6.5000
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = <<-293.1742, -991.1610, 23.1368>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos = <<-293.1823, -991.2120, 23.1368>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = <<-294.4002, -985.1016, 23.1368>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = <<-280.0096, -995.3130, 23.4494>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 75.0000
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos = <<-279.4986, -995.4564, 23.5404>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot = 71.6400
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = <<-283.6542, -998.4232, 23.1964>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = <<-281.3947, -992.0045, 23.2172>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 4.2500
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 3.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, Thu 29/08/2013 16:59
		CASE MP_PROPERTY_BUILDING_3          //High apt 7,8
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = <<-1449.8301, -509.4476, 31.8831>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = <<-0.6590, 0.0000, 49.2517>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = 41.4552
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = 0.0000
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = <<-1449.8301, -509.4476, 31.8831>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = <<-0.6590, 0.0000, 49.2517>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = <<-1449.6281, -509.9228, 31.8883>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = <<-0.6590, 0.0000, 58.0153>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = 41.4552
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 10.3000
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = <<-1449.4760, -515.0535, 30.8522>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos = <<-1449.4485, -515.1066, 30.8522>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = <<-1444.2711, -525.8814, 30.8522>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = <<-1459.2285, -501.3297, 31.5081>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 222.8400
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos = <<-1459.7349, -500.6072, 31.5980>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot = 222.8400
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = <<-1453.2833, -502.1619, 31.1400>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = <<-1458.3475, -505.6930, 31.1630>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 3.0000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 3.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 20/09/2013 15:38 (#1642223)
		CASE MP_PROPERTY_BUILDING_4	//High apt 9,10,11
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = <<-813.5735, -435.9550, 36.4917>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = <<5.1927, 0.0000, 102.2532>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = 42.5856
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = 0.0000
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = <<-813.5735, -435.9550, 36.4917>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = <<5.1927, 0.0000, 102.2532>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = <<-813.5735, -435.9550, 36.4917>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = <<5.1927, 0.0000, 84.4315>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = 42.5856
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 6.5000
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = <<-814.0803, -427.8784, 34.2289>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos = <<-814.0607, -427.8223, 34.2197>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = <<-814.9882, -415.9547, 32.2944>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = <<-821.7708, -439.5038, 35.6399>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 306.0000
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos = <<-821.9498, -439.5983, 35.6399>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot = 302.4000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = <<-822.5115, -433.3241, 35.6609>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = <<-818.7810, -440.3744, 35.6770>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 3.0000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 20/09/2013 15:38 (#1642223)
		CASE MP_PROPERTY_BUILDING_23	//Low apt 7
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = <<-1602.1926, -440.6148, 38.3428>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = <<-0.4196, 0.0000, 154.5195>>
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = 42.8873
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = <<-1602.1926, -440.6148, 38.3428>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = <<-0.4196, 0.0000, 154.5195>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = <<-1601.6389, -440.3610, 38.3462>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = <<-0.8137, 0.0000, 159.8575>>
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = 42.8873
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 6.5000
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = <<-1597.7802, -441.2629, 37.2166>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos = <<-1595.5609, -435.8115, 37.2166>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = <<-1589.0699, -442.9493, 36.2118>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = <<-1606.2397, -448.3238, 37.1934>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 318.7500
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos = <<-1606.3799, -448.4681, 37.1887>>
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot = 318.7500
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = <<-1607.5896, -445.3750, 37.2258>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = <<-1603.0813, -449.2010, 37.2257>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 3.0000
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 3.5000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		DEFAULT
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vPos = GET_GAMEPLAY_CAM_COORD()
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mStart.vRot = GET_GAMEPLAY_CAM_ROT()
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos = GET_GAMEPLAY_CAM_COORD()
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot = GET_GAMEPLAY_CAM_ROT()
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov = GET_GAMEPLAY_CAM_FOV()
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake = 0.25
			scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fDuration = 6.500
			scene.bEnablePans[NET_REALTY_2_SCENE_PAN_descent] = FALSE
			
			scene.fExitDelay = 0.0000
			
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vPos = scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vPos
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].mCam.vRot = scene.mPans[NET_REALTY_2_SCENE_PAN_descent].mEnd.vRot
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fFov = scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fFov
			scene.mCuts[NET_REALTY_2_SCENE_CUT_start].fShake = scene.mPans[NET_REALTY_2_SCENE_PAN_descent].fShake
			
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot = 0
			
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos = scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos = <<0,0,0>>
			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos = scene.mPlacers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos
			
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos = scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos
			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot = 0
			
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID) - <<5,5,0>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth = 5.0
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1 = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID) + <<5,5,0>>
			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight = 5.0
			
			#IF ACTIVATE_PROPERTY_CS_DEBUG
			IF NOT bIgnoreAssert
				TEXT_LABEL_63 str
				str = "Private_Get_NET_REALTY_2_SCENE() invalid ID "
				str += GET_STRING_FROM_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID), GET_LENGTH_OF_LITERAL_STRING("MP_PROPERTY_"), GET_LENGTH_OF_LITERAL_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID)))
				//SCRIPT_ASSERT(str)
				PRINTLN(str)
			ENDIF
			#ENDIF
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

#IF ACTIVATE_PROPERTY_CS_DEBUG
USING "net_realty_2_scene_debug.sch"
#ENDIF
