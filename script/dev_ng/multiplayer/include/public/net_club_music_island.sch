#IF FEATURE_HEIST_ISLAND
USING "net_club_music_data_common.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ CLUB MUSIC DATA ╞═══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC CLUB_DJS _GET_ISLAND_CLUB_DJ(DJ_SERVER_DATA DJServerData, INT iDJ, BOOL bNextDJ = FALSE, BOOL bUsingPathB = FALSE)
	UNUSED_PARAMETER(DJServerData)
	UNUSED_PARAMETER(bNextDJ)
	UNUSED_PARAMETER(iDJ)
	UNUSED_PARAMETER(bUsingPathB)
	RETURN CLUB_DJ_KEINEMUSIK_BEACH_PARTY
ENDFUNC

FUNC INT _GET_ISLAND_CLUB_MAX_DJS()
	RETURN 1
ENDFUNC

FUNC INT _GET_ISLAND_CLUB_MAX_STATIC_EMITTERS()
	RETURN 4
ENDFUNC

FUNC STRING _GET_ISLAND_CLUB_STATIC_EMITTER_NAME(INT iEmitterID)
	STRING sEmitterName = ""
	SWITCH iEmitterID
		CASE 0	sEmitterName = "SE_DLC_Hei4_Island_Beach_Party_Music_New_01_Left"		BREAK
		CASE 1	sEmitterName = "SE_DLC_Hei4_Island_Beach_Party_Music_New_02_Right"		BREAK
		CASE 2	sEmitterName = "SE_DLC_Hei4_Island_Beach_Party_Music_New_03_Reverb"		BREAK
		CASE 3	sEmitterName = "SE_DLC_Hei4_Island_Beach_Party_Music_New_04_Reverb"		BREAK
	ENDSWITCH
	RETURN sEmitterName
ENDFUNC

FUNC STRING _GET_ISLAND_CLUB_AUDIO_SCENE_NAME()
	RETURN ""
ENDFUNC

FUNC STRING _GET_ISLAND_CLUB_ZONE_NAME()
	RETURN "AZ_DLC_Hei4_Island_Beach_Party"
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_CLUB_MUSIC_ISLAND_LOOK_UP_TABLE(CLUB_MUSIC_INTERFACE &interface, CLUB_MUSIC_PROCEDURES eProc)
	SWITCH eProc
		CASE E_GET_CLUB_DJ
			interface.returnGetClubDJ = &_GET_ISLAND_CLUB_DJ
		BREAK
		CASE E_GET_CLUB_MAX_DJS
			interface.returnGetClubMaxDJS = &_GET_ISLAND_CLUB_MAX_DJS
		BREAK
		CASE E_GET_CLUB_RADIO_STATION
			interface.returnGetClubRadioStation = &_GET_CLUB_DJ_RADIO_STATION
		BREAK
		CASE E_GET_CLUB_RADIO_STATION_NAME
			interface.returnGetClubRadioStationName = &_GET_CLUB_DJ_RADIO_STATION_NAME
		BREAK
		CASE E_GET_CLUB_RADIO_STATION_TRACK_NAME
			interface.returnGetClubRadioStationTrackName = &_GET_CLUB_DJ_RADIO_STATION_TRACK_NAME
		BREAK
		CASE E_GET_CLUB_RADIO_STATION_MAX_TRACKS
			interface.returnGetClubRadioStationMaxTracks = &_GET_CLUB_DJ_RADIO_STATION_MAX_TRACKS
		BREAK
		CASE E_GET_CLUB_RADIO_STATION_TRACK_DURATION_MS
			interface.returnGetClubRadioStationTrackDurationMS = &_GET_CLUB_RADIO_STATION_TRACK_DURATION_MS
		BREAK
		CASE E_GET_CLUB_RADIO_STATION_TRACK_INTENSITY
			interface.getClubRadioStationTrackIntensity = &_GET_CLUB_RADIO_STATION_TRACK_INTENSITY
		BREAK
		CASE E_GET_CLUB_MAX_STATIC_EMITTERS
			interface.returnGetClubMaxStaticEmitters = &_GET_ISLAND_CLUB_MAX_STATIC_EMITTERS
		BREAK
		CASE E_GET_CLUB_STATIC_EMITTER_NAME
			interface.returnGetClubStaticEmitterName = &_GET_ISLAND_CLUB_STATIC_EMITTER_NAME
		BREAK
		CASE E_GET_CLUB_AUDIO_SCENE_NAME
			interface.returnGetClubAudioSceneName = &_GET_ISLAND_CLUB_AUDIO_SCENE_NAME
		BREAK
		CASE E_GET_CLUB_ZONE_NAME
			interface.returnGetClubZoneName = &_GET_ISLAND_CLUB_ZONE_NAME
		BREAK
		CASE E_DOES_DJ_DATA_LOAD_FROM_DATAFILE
			interface.returnDoesDJDataLoadFromDataFile = &_DOES_CLUB_DJ_DATA_LOAD_FROM_DATAFILE
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF //FEATURE_HEIST_ISLAND
