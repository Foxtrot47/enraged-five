
USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "building_control_public.sch"

// Network Headers
USING "net_events.sch"
USING "net_blips.sch"
USING "net_scoring_common.sch"
USING "net_mission.sch"
USING "shared_hud_displays.sch"
//USING "transition_crews.sch"
USING "net_freemode_cut.sch"

USING "net_synced_ambient_pickups.sch"


///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////              DATA                 //////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///    

//PURPOSE: Gets the Hold Up Blip and Front Door location.
PROC GET_HOLD_UP_BLIP_AND_FRONT_DOOR_COORDS(Crim_HOLD_UP_POINT WhichHoldUp, VECTOR &vBlipLocation, VECTOR &vFrontDoorCoord)
	SWITCH WhichHoldUp
		//SHOP 24_7
		CASE eCRIM_HUP_SHOP247_1
			//Spawn location for warping = <<1400.5985, 3594.1514, 33.6259>>
			vBlipLocation 		= <<1394.1692, 3599.8601, 34.0121>>
			vFrontDoorCoord		= <<1395.547852,3600.575195,33.989140>>		//<<1395.459595, 3600.545410, 33.768150>>
		BREAK
		CASE eCRIM_HUP_SHOP247_2
			//Spawn location for warping = <<-3034.3882, 591.0876, 6.8258>>
			vBlipLocation 		= <<-3038.9082, 589.5187, 6.9048>>
			vFrontDoorCoord	 	= <<-3038.656982, 589.517578, 6.904751>>
		BREAK
		CASE eCRIM_HUP_SHOP247_3
			//Spawn location for warping = <<-3236.1367, 1004.1160, 11.1670>>
			vBlipLocation 		= <<-3240.3169, 1004.4334, 11.8307>>
			vFrontDoorCoord 	= <<-3240.032227, 1004.481750, 11.830701>>
		BREAK
		CASE eCRIM_HUP_SHOP247_4
			//Spawn location for warping = <<543.6606, 2677.0728, 41.1419>>
			vBlipLocation = <<544.2802, 2672.8113, 41.1566>>
			vFrontDoorCoord = <<544.227844,2672.500488,41.156502>>	
		BREAK
		CASE eCRIM_HUP_SHOP247_5
			//Spawn location for warping = <<2564.0679, 384.9708, 107.4644>>
			vBlipLocation = <<2559.2471, 385.5266, 107.6230>>	//<<2564.0679, 384.9708, 107.4644>>
			vFrontDoorCoord = <<2559.176270,385.393555,107.622971>>			
		BREAK
		CASE eCRIM_HUP_SHOP247_6
			//Spawn location for warping = <<2686.1780, 3279.9431, 54.2404>>
			vBlipLocation = <<2682.0034, 3282.5432, 54.2411>>
			vFrontDoorCoord = <<2681.864014,3282.602539,54.241138>>			
		BREAK
		CASE eCRIM_HUP_SHOP247_7
			//Spawn location for warping = <<1729.6146, 6409.2871, 34.0005>>
			vBlipLocation = <<1731.2098, 6411.4033, 34.0372>>
			vFrontDoorCoord = <<1731.236694,6411.556641,34.037231>>
		BREAK
		CASE eCRIM_HUP_SHOP247_8
			//Spawn location for warping = <<1967.6334, 3736.4727, 31.2132>>
			vBlipLocation = <<1965.0542, 3740.5552, 31.3448>>
			vFrontDoorCoord = <<1964.998901,3740.794678,31.343750>>	
		BREAK
		CASE eCRIM_HUP_SHOP247_9
			//Spawn location for warping = <<26.9920, -1353.4207, 28.3395>>
			vBlipLocation = <<27.0641, -1348.8127, 28.5036>>+<<2, -0.4, 0>>
			vFrontDoorCoord = <<27.033585,-1348.507324,28.493917>>+<<2, -0.4, 0>>
		BREAK
		CASE eCRIM_HUP_SHOP247_10
			//Spawn location for warping = <<373.7934, 319.8971, 102.4681>>
			vBlipLocation = <<376.6533, 323.6471, 102.5664>>
			vFrontDoorCoord = <<376.630920,323.553284,102.566406>>			
		BREAK
		
		//LIQUOR STORES
		CASE eCRIM_HUP_LIQUOR_1
			//Spawn location for warping = <<1193.5663, 2696.5105, 36.9452>>
			vBlipLocation = <<1195.4320, 2703.1143, 37.1573>>+<<-29.04, 0.39, 0>>
			vFrontDoorCoord = <<1166.497192,2703.756104,37.063072>> 	//<<1195.447021,2703.301270,37.157310>>+<<-29.04, 0.39, 0>>
		BREAK
		CASE eCRIM_HUP_LIQUOR_2
			//Spawn location for warping = <<-2978.2410, 391.2192, 14.0224>>
			vBlipLocation = <<-2973.2617, 390.8184, 14.0433>>
			vFrontDoorCoord = <<-2973.511719,390.787323,14.043219>>  	//<<-2973.427246, 390.814362, 14.043285>>			
		BREAK
		CASE eCRIM_HUP_LIQUOR_3
			//Spawn location for warping = <<-1229.3036, -898.4478, 11.2787>>
			vBlipLocation = <<-1226.4644, -902.5864, 11.2783>>
			vFrontDoorCoord = <<-1226.417480,-902.673767,11.334959>> 	//<<-1226.271362,-902.864258,11.334655>>			
		BREAK
		CASE eCRIM_HUP_LIQUOR_4
			//Spawn location for warping = <<1146.4921, -980.9312, 45.1804>>
			vBlipLocation = <<1141.3596, -980.8802, 45.4155>>
			vFrontDoorCoord = <<1141.350098,-980.932190,45.415943>>   	//<<1141.031738,-981.010254,45.415802>>			
		BREAK
		CASE eCRIM_HUP_LIQUOR_5
			//Spawn location for warping = <<-1498.9261, -387.4839, 39.1369>>
			vBlipLocation = <<-1491.0565, -383.5728, 39.1706>>
			vFrontDoorCoord = <<-1490.948730,-383.477112,39.163319>>  	//<<-1490.812744,-383.308350,39.163380>>			
		BREAK
		
		//GAS STATIONS
		CASE eCRIM_HUP_GAS_1
			//Spawn location for warping = <<1693.1393, 4932.7178, 41.0783>>
			vBlipLocation = <<1698.8085, 4929.1978, 41.0783>>
			vFrontDoorCoord = <<1698.979614,4929.093262,41.063572>>			
		BREAK
		CASE eCRIM_HUP_GAS_2
			//Spawn location for warping = <<-711.7835, -923.3544, 18.0141>>
			vBlipLocation = <<-711.7210, -916.6965, 18.2145>>
			vFrontDoorCoord = <<-711.769958,-916.468506,18.215569>>			
		BREAK
		CASE eCRIM_HUP_GAS_3
			//Spawn location for warping = <<-56.0217, -1760.1188, 28.0396>>
			vBlipLocation = <<-53.1240, -1756.4054, 28.4210>>
			vFrontDoorCoord = <<-52.904518,-1756.473877,28.420971>>
		BREAK
		CASE eCRIM_HUP_GAS_4
			//Spawn location for warping = <<1160.5577, -332.0321, 67.6580>>
			vBlipLocation = <<1159.5421, -326.6986, 67.9230>>
			vFrontDoorCoord = <<1159.633057,-326.508667,68.205070>>
		BREAK
		CASE eCRIM_HUP_GAS_5
			//Spawn location for warping = <<-1818.8751, 785.1304, 136.8768>>
			vBlipLocation = <<-1822.2866, 788.0060, 137.1859>>
			vFrontDoorCoord = <<-1822.372925,788.285095,137.187622>>			
		BREAK		
	ENDSWITCH
ENDPROC

//PURPOSE: List of Hold Up Areas
PROC HOLD_UP_DETAILS(Crim_HOLD_UP_POINT WhichHoldUp, INTERIOR_INSTANCE_INDEX &interiorID, INT &iRoomHashMain, INT &iRoomHashMain2, INT &iRoomHashBack, VECTOR &vBlipLocation, VECTOR &vFrontDoorCoord)

	GET_HOLD_UP_BLIP_AND_FRONT_DOOR_COORDS(WhichHoldUp, vBlipLocation, vFrontDoorCoord)

	SWITCH WhichHoldUp
		//SHOP 24_7
		CASE eCRIM_HUP_SHOP247_1
			IF interiorID = NULL
				interiorID 	= GET_INTERIOR_AT_COORDS_WITH_TYPE(vBlipLocation, "v_methlab")
			ENDIF
			iRoomHashMain 	= HASH("v_39_ShopRm")
			iRoomHashBack 	= HASH("v_39_StairsRm")
			iRoomHashMain2	= -1
		BREAK
		CASE eCRIM_HUP_SHOP247_2
		CASE eCRIM_HUP_SHOP247_3
		CASE eCRIM_HUP_SHOP247_4
		CASE eCRIM_HUP_SHOP247_5
		CASE eCRIM_HUP_SHOP247_6
		CASE eCRIM_HUP_SHOP247_7
		CASE eCRIM_HUP_SHOP247_8
		CASE eCRIM_HUP_SHOP247_9
		CASE eCRIM_HUP_SHOP247_10
			IF interiorID = NULL
				interiorID 	= GET_INTERIOR_AT_COORDS_WITH_TYPE(vBlipLocation, "v_shop_247")
			ENDIF
			iRoomHashMain 	= HASH("v_66_ShopRm")
			iRoomHashBack 	= HASH("v_66_BackRm")
			iRoomHashMain2	= -1
		BREAK
		CASE eCRIM_HUP_LIQUOR_1
		CASE eCRIM_HUP_LIQUOR_2
		CASE eCRIM_HUP_LIQUOR_3
		CASE eCRIM_HUP_LIQUOR_4
		CASE eCRIM_HUP_LIQUOR_5
			IF interiorID = NULL
				interiorID 	= GET_INTERIOR_AT_COORDS_WITH_TYPE(vBlipLocation, "v_gen_liquor")
			ENDIF
			iRoomHashMain 	= HASH("liquor_front")
			iRoomHashBack 	= HASH("liquor_back")
			iRoomHashMain2 	= -1
		BREAK
		CASE eCRIM_HUP_GAS_1
		CASE eCRIM_HUP_GAS_2
		CASE eCRIM_HUP_GAS_3
		CASE eCRIM_HUP_GAS_4
		CASE eCRIM_HUP_GAS_5
			IF interiorID = NULL
				interiorID 	= GET_INTERIOR_AT_COORDS_WITH_TYPE(vBlipLocation, "v_gasstation")
			ENDIF
			iRoomHashMain 	= HASH("v_68_GasRm")
			iRoomHashBack 	= HASH("v_68_BackRm")
			iRoomHashMain2	= HASH("v_68_Toilets")
		BREAK
	ENDSWITCH

ENDPROC 

//PURPOSE: List of Hold Up Areas
PROC HOLD_UP_DETAILS_MANUAL(Crim_HOLD_UP_POINT WhichHoldUp, VECTOR &vMin, VECTOR &vMax, FLOAT &fWidth) //, VECTOR &vBlipLocation)
	//SHOP 24_7
	IF WhichHoldUp = eCRIM_HUP_SHOP247_1
		//Spawn location for warping = <<1400.5985, 3594.1514, 33.6259>>
		vMin = <<1395.547852,3600.575195,33.989140>>		//<<1395.459595, 3600.545410, 33.768150>>
		vMax = <<1392.079834,3609.983398,37.335434-0.2>>		//<<1391.747314, 3610.849365, 37.517563>>
		fWidth = 12.250
//		vBlipLocation = <<1394.1692, 3599.8601, 34.0121>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_SHOP247_2
		//Spawn location for warping = <<-3034.3882, 591.0876, 6.8258>>
		vMin = <<-3038.656982, 589.517578, 6.904751>>
		vMax = <<-3045.475830, 587.406616, 11.312223-0.2>>
		fWidth = 11.0
//		vBlipLocation = <<-3038.9082, 589.5187, 6.9048>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_SHOP247_3
		//Spawn location for warping = <<-3236.1367, 1004.1160, 11.1670>>
		vMin = <<-3240.032227, 1004.481750, 11.830701>>
		vMax = <<-3247.187500, 1005.106934, 16.252235-0.2>>
		fWidth = 11.0
//		vBlipLocation = <<-3240.3169, 1004.4334, 11.8307>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_SHOP247_4
		//Spawn location for warping = <<543.6606, 2677.0728, 41.1419>>
		vMin = <<544.227844,2672.500488,41.156502>>
		vMax = <<545.200989,2665.441895,45.691551-0.2>>
		fWidth = 11.0
//		vBlipLocation = <<544.2802, 2672.8113, 41.1566>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_SHOP247_5
		//Spawn location for warping = <<2564.0679, 384.9708, 107.4644>>
		vMin = <<2559.176270,385.393555,107.622971>>
		vMax = <<2552.054443,385.605652,112.163834-0.2>>
		fWidth = 11.0
//		vBlipLocation = <<2559.2471, 385.5266, 107.6230>>	//<<2564.0679, 384.9708, 107.4644>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_SHOP247_6
		//Spawn location for warping = <<2686.1780, 3279.9431, 54.2404>>
		vMin = <<2681.864014,3282.602539,54.241138>>
		vMax = <<2675.586182,3285.980957,58.790211-0.2>>
		fWidth = 11.0
//		vBlipLocation = <<2682.0034, 3282.5432, 54.2411>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_SHOP247_7
		//Spawn location for warping = <<1729.6146, 6409.2871, 34.0005>>
		vMin = <<1731.236694,6411.556641,34.037231>>
		vMax = <<1734.383301,6417.952148,38.584606-0.2>>
		fWidth = 11.0
//		vBlipLocation = <<1731.2098, 6411.4033, 34.0372>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_SHOP247_8
		//Spawn location for warping = <<1967.6334, 3736.4727, 31.2132>>
		vMin = <<1964.998901,3740.794678,31.343750>>
		vMax = <<1961.384033,3746.927979,35.895706-0.2>>
		fWidth = 11.0
//		vBlipLocation = <<1965.0542, 3740.5552, 31.3448>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_SHOP247_9
		//Spawn location for warping = <<26.9920, -1353.4207, 28.3395>>
		vMin = <<27.033585,-1348.507324,28.493917>>+<<2, -0.4, 0>>
		vMax = <<27.056143,-1341.382324,33.038342>>-0.2+<<2, -0.4, 0>>
		fWidth = 11.0
//		vBlipLocation = <<27.0641, -1348.8127, 28.5036>>+<<2, -0.4, 0>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_SHOP247_10
		//Spawn location for warping = <<373.7934, 319.8971, 102.4681>>
		vMin = <<376.630920,323.553284,102.566406>>
		vMax = <<378.325348,330.479340,107.109512-0.2>>
		fWidth = 11.0
//		vBlipLocation = <<376.6533, 323.6471, 102.5664>>
	ENDIF
	
	//LIQUOR STORES
	IF WhichHoldUp = eCRIM_HUP_LIQUOR_1
		//Spawn location for warping = <<1193.5663, 2696.5105, 36.9452>>
		vMin = <<1166.497192,2703.756104,37.063072>> 	//<<1195.447021,2703.301270,37.157310>>+<<-29.04, 0.39, 0>>
		vMax = <<1166.524902,2711.974121,41.663072-0.2>>	//<1195.589111,2712.352051,41.627872>>+<<-29.04, 0.39, 0>>
		fWidth = 7.75	//7.50
//		vBlipLocation = <<1195.4320, 2703.1143, 37.1573>>+<<-29.04, 0.39, 0>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_LIQUOR_2
		//Spawn location for warping = <<-2978.2410, 391.2192, 14.0224>>
		vMin = <<-2973.511719,390.787323,14.043219>>  	//<<-2973.427246, 390.814362, 14.043285>>
		vMax = <<-2965.288330,390.245209,18.543219-0.2>> 	//<<-2964.391602, 390.172577, 18.512846>>
		fWidth = 7.75	//7.50
//		vBlipLocation = <<-2973.2617, 390.8184, 14.0433>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_LIQUOR_3
		//Spawn location for warping = <<-1229.3036, -898.4478, 11.2787>>
		vMin = <<-1226.417480,-902.673767,11.334959>> 	//<<-1226.271362,-902.864258,11.334655>>
		vMax = <<-1221.764648,-909.588745,15.826262-0.2>> 	//<<-1221.281372,-910.342163,15.817913>>
		fWidth = 7.75	//7.50
//		vBlipLocation = <<-1226.4644, -902.5864, 11.2783>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_LIQUOR_4
		//Spawn location for warping = <<1146.4921, -980.9312, 45.1804>>
		vMin = <<1141.350098,-980.932190,45.415943>>   	//<<1141.031738,-981.010254,45.415802>>
		vMax = <<1132.975586,-982.095215,49.915737-0.2>> 	//<<1132.088623,-982.205566,49.909668>>
		fWidth = 7.75	//7.50
//		vBlipLocation = <<1141.3596, -980.8802, 45.4155>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_LIQUOR_5
		//Spawn location for warping = <<-1498.9261, -387.4839, 39.1369>>
		vMin = <<-1490.948730,-383.477112,39.163319>>  	//<<-1490.812744,-383.308350,39.163380>>
		vMax = <<-1485.006470,-377.616730,43.663319-0.2>> 	//<<-1484.388062,-376.965118,43.658806>>
		fWidth = 7.75	//7.50
//		vBlipLocation = <<-1491.0565, -383.5728, 39.1706>>
	ENDIF
	
	//GAS STATIONS
	IF WhichHoldUp = eCRIM_HUP_GAS_1
		//Spawn location for warping = <<1693.1393, 4932.7178, 41.0783>>
		vMin = <<1698.979614,4929.093262,41.063572>>
		vMax = <<1705.233154,4924.578125,45.603859-0.2>>
		fWidth = 14.0
//		vBlipLocation = <<1698.8085, 4929.1978, 41.0783>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_GAS_2
		//Spawn location for warping = <<-711.7835, -923.3544, 18.0141>>
		vMin = <<-711.769958,-916.468506,18.215569>>
		vMax = <<-711.736755,-908.751709,22.764883-0.2>>
		fWidth = 14.0
//		vBlipLocation = <<-711.7210, -916.6965, 18.2145>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_GAS_3
		//Spawn location for warping = <<-56.0217, -1760.1188, 28.0396>>
		vMin = <<-52.904518,-1756.473877,28.420971>>
		vMax = <<-47.925232,-1750.614136,32.942459-0.2>>
		fWidth = 14.0
//		vBlipLocation = <<-53.1240, -1756.4054, 28.4210>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_GAS_4
		//Spawn location for warping = <<1160.5577, -332.0321, 67.6580>>
		vMin = <<1159.633057,-326.508667,68.205070>>
		vMax = <<1158.335205,-318.912262,72.748283-0.2>>
		fWidth = 14.0
//		vBlipLocation = <<1159.5421, -326.6986, 67.9230>>
	ENDIF
	IF WhichHoldUp = eCRIM_HUP_GAS_5
		//Spawn location for warping = <<-1818.8751, 785.1304, 136.8768>>
		vMin = <<-1822.372925,788.285095,137.187622>>
		vMax = <<-1827.483521,794.073853,141.693268-0.2>>
		fWidth = 14.0
//		vBlipLocation = <<-1822.2866, 788.0060, 137.1859>>
	ENDIF
	
ENDPROC 

//PURPOSE: List of Extra Hold Up Areas
PROC HOLD_UP_DETAILS_EXTRA_AREA(Crim_HOLD_UP_POINT WhichHoldUp, VECTOR &vMin, VECTOR &vMax, FLOAT &fWidth)
	
	//LIQUOR STORES
	IF WhichHoldUp = eCRIM_HUP_LIQUOR_1
		vMin = <<1164.977295,2711.393799,37.063072>>
		vMax = <<1164.973999,2713.149658,40.671677-0.2>>
		fWidth = 5.0
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_2
		vMin = <<-2965.705078,391.788055,14.043219>>
		vMax = <<-2964.023193,391.681335,17.599716-0.2>>
		fWidth = 5.0
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_3
		vMin = <<-1220.804443,-908.354492,11.326262>>
		vMax = <<-1219.876709,-909.727783,14.893353-0.2>>
		fWidth = 5.0
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_4
		vMin = <<1133.704468,-983.574585,45.415737>>
		vMax = <<1132.033203,-983.800354,49.020939-0.2>>
		fWidth = 5.0
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_5
		vMin = <<-1486.446411,-376.842194,39.163319>>
		vMax = <<-1485.282715,-375.682281,42.843147-0.2>>
		fWidth = 5.0
	//ENDIF
	
	//GAS STATIONS
	ELIF WhichHoldUp = eCRIM_HUP_GAS_1
		vMin = <<1704.845947,4919.420410,41.063644>>
		vMax = <<1703.399780,4917.348145,44.137810-0.2>>
		fWidth = 5.75
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_2
		vMin = <<-707.133667,-906.087402,18.215588>>
		vMax = <<-704.626282,-906.051270,21.253979-0.2>>
		fWidth = 5.75
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_3
		vMin = <<-42.663376,-1751.472778,28.420994>>
		vMax = <<-40.720680,-1753.082642,31.431986-0.2>>
		fWidth = 5.75
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_4
		vMin = <<1162.397339,-315.464203,68.205093>>
		vMax = <<1164.864990,-315.040527,71.250046-0.2>>
		fWidth = 5.75
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_5
		vMin = <<-1826.024536,799.088928,137.135391>>
		vMax = <<-1824.121460,800.796265,140.163025-0.2>>
		fWidth = 5.75
	ENDIF
ENDPROC

//PURPOSE: List of Behind Hold Up Counter Areas
PROC HOLD_UP_DETAILS_BEHIND_COUNTER(Crim_HOLD_UP_POINT WhichHoldUp, VECTOR &vMin, VECTOR &vMax, FLOAT &fWidth)
	
	//SHOP 24/7 STORES
	IF WhichHoldUp = eCRIM_HUP_SHOP247_1
		vMin = <<1390.032837,3605.529053,33.980911>>
		vMax = <<1394.411743,3607.156006,36.382877>>
		fWidth = 1.25
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_2
		vMin = <<-3041.675049,583.593140,6.908931>>
		vMax = <<-3037.283691,585.037537,9.521532>>
		fWidth = 1.25
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_3
		vMin = <<-3245.184082,1000.123901,11.830711>>
		vMax = <<-3240.504395,999.778259,14.466454>>
		fWidth = 1.25
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_4
		vMin = <<549.533203,2668.464355,41.156513>>
		vMax = <<548.911377,2673.107666,43.776596>>
		fWidth = 1.25
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_5
		vMin = <<2554.316406,380.856903,107.622986>>
		vMax = <<2558.980225,380.690826,110.268417>>
		fWidth = 1.25
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_6
		vMin = <<2675.476074,3280.807373,54.241150>>
		vMax = <<2679.577393,3278.525635,56.895496>>
		fWidth = 1.25
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_7
		vMin = <<1729.145264,6417.834473,34.037243>>
		vMax = <<1727.069580,6413.634766,36.664417>>
		fWidth = 1.25
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_8
		vMin = <<1958.609741,3742.512451,31.343761>>
		vMax = <<1960.957764,3738.465088,33.986340>>
		fWidth = 1.25
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_9
		vMin = <<24.510925,-1344.448975,28.497032>>
		vMax = <<24.522240,-1349.083862,31.142721>>
		fWidth = 1.25
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_10
		vMin = <<373.296021,329.237854,102.566414>>
		vMax = <<372.134674,324.698547,105.214043>>
		fWidth = 1.25
	
	//LIQUOR STORES
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_1
		vMin = <<1168.183350,2711.202637,37.063164>>
		vMax = <<1163.143555,2711.246338,40.166878>>
		fWidth = 2.5
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_2
		vMin = <<-2966.141846,388.641785,14.043308>>
		vMax = <<-2965.811768,393.647278,17.172482>>
		fWidth = 2.5
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_3
		vMin = <<-1223.598877,-909.898438,11.326351>>
		vMax = <<-1219.442993,-907.075562,14.459287>>
		fWidth = 2.5
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_4
		vMin = <<1133.524292,-980.308655,45.415825>>
		vMax = <<1134.220215,-985.283997,48.532352>>
		fWidth = 2.5
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_5
		vMin = <<-1484.413208,-379.323639,39.163410>>
		vMax = <<-1487.954102,-375.766571,42.288849>>
		fWidth = 2.5
	
	//GAS STATIONS
	ELIF WhichHoldUp = eCRIM_HUP_GAS_1
		vMin = <<1699.446777,4921.570313,41.063656>>
		vMax = <<1695.640137,4924.244629,44.221649>>
		fWidth = 2.250
	ELIF WhichHoldUp = eCRIM_HUP_GAS_2
		vMin = <<-705.786926,-911.699707,18.215599>>
		vMax = <<-705.702637,-916.373962,21.335445>>
		fWidth = 2.250
	ELIF WhichHoldUp = eCRIM_HUP_GAS_3
		vMin = <<-45.269947,-1756.668823,28.421005>>
		vMax = <<-48.260365,-1760.284912,31.563421>>
		fWidth = 2.250
	ELIF WhichHoldUp = eCRIM_HUP_GAS_4
		vMin = <<1164.697266,-320.758698,68.205101>>
		vMax = <<1165.463013,-325.089935,71.378929>>
		fWidth = 2.250
	ELIF WhichHoldUp = eCRIM_HUP_GAS_5
		vMin = <<-1821.204590,795.869873,137.089310>>
		vMax = <<-1818.040649,792.398193,140.183624>>
		fWidth = 2.250
	ENDIF
ENDPROC

//PURPOSE: HOld Up Back Room Angled Area Info
PROC HOLD_UP_BACKROOM_DETAILS(Crim_HOLD_UP_POINT WhichHoldUp, VECTOR &vMin, VECTOR &vMax, FLOAT &fWidth)
	//SHOP 24_7
	IF WhichHoldUp = eCRIM_HUP_SHOP247_1
		vMin = <<1393.090698,3607.382813,33.980896>>
		vMax = <<1391.178467,3612.515869,38.361401-0.2>>
		fWidth = 12.5
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_2
		vMin = <<-3045.013428,585.966553,10.354213-0.2>>
		vMax = <<-3048.713867,584.796143,6.948262>>
		fWidth = 8.25
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_3
		vMin = <<-3247.312500,1003.657410,15.274335-0.2>>
		vMax = <<-3251.156738,1003.984314,11.835008>>
		fWidth = 8.25
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_4
		vMin = <<546.547363,2665.641602,44.601078-0.2>>
		vMax = <<547.053955,2661.793945,41.156670>>
		fWidth = 8.25
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_5
		vMin = <<2552.000488,384.252747,111.065636-0.2>>
		vMax = <<2548.145264,384.413269,107.624069>>
		fWidth = 8.25
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_6
		vMin = <<2674.946289,3284.832764,57.680878-0.2>>
		vMax = <<2671.569092,3286.698730,54.244644>>
		fWidth = 8.25
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_7
		vMin = <<1733.119385,6418.578613,37.480019-0.2>>
		vMax = <<1734.838623,6422.032227,34.037506>>
		fWidth = 8.25
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_8
		vMin = <<1960.223755,3746.258057,34.787800-0.2>>
		vMax = <<1958.294556,3749.599365,31.345217>>
		fWidth = 8.25
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_9
		vMin = <<27.713818,-1341.967896,31.933138-0.2>>
		vMax = <<27.710491,-1338.109497,28.494787>>
		fWidth = 8.25
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_10
		vMin = <<377.018280,330.808014,106.011230-0.2>>
		vMax = <<377.949921,334.552277,102.566963>>
		fWidth = 8.25
	//ENDIF
	
	//LIQUOR STORES
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_1
		vMin = <<1167.015137,2711.601318,40.661472-0.2>>
		vMax = <<1167.082153,2720.242920,36.065010>>
		fWidth = 8.25
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_2
		vMin = <<-2965.704590,389.669617,17.641619-0.2>>
		vMax = <<-2957.094971,389.138977,13.043233>>
		fWidth = 8.75
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_3
		vMin = <<-1222.402100,-909.557495,14.924662-0.2>>
		vMax = <<-1217.682251,-916.797363,10.337352>>
		fWidth = 8.75
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_4
		vMin = <<1133.283691,-981.472961,49.014137-0.2>>
		vMax = <<1124.708008,-982.602722,44.420078>>
		fWidth = 8.75
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_5
		vMin = <<-1484.883301,-378.269440,42.761719-0.2>>
		vMax = <<-1478.770752,-372.166382,38.169476>>
		fWidth = 8.75
	//ENDIF
	
	//GAS STATIONS
	ELIF WhichHoldUp = eCRIM_HUP_GAS_1
		vMin = <<1704.556152,4921.805664,44.026867-0.2>>
		vMax = <<1708.284790,4919.196289,41.063591>>
		fWidth = 4.0
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_2
		vMin = <<-709.018982,-907.715942,21.179176-0.2>>
		vMax = <<-709.017700,-903.151550,18.216183>>
		fWidth = 4.0
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_3
		vMin = <<-45.166168,-1751.578003,31.379726-0.2>>
		vMax = <<-42.229610,-1748.089844,28.420994>>
		fWidth = 4.0
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_4
		vMin = <<1160.824951,-317.421448,71.160851-0.2>>
		vMax = <<1160.032715,-312.932037,68.205086>>
		fWidth = 4.0
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_5
		vMin = <<-1826.217651,796.639282,140.133438-0.2>>
		vMax = <<-1829.300903,799.999634,137.180038>>
		fWidth = 4.0
	ENDIF
	
ENDPROC

//PURPOSE: Gets the Hold Up Door Details
PROC HOLD_UP_DOOR_DETAILS(INT iHoldUp, INT &iHash[], MODEL_NAMES &Model[], VECTOR &vCoords[])
	Crim_HOLD_UP_POINT WhichHoldUp = INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp)
	
	//SHOP 24_7
	IF WhichHoldUp = eCRIM_HUP_SHOP247_1
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_1A")
		Model[0]	= v_ilev_ml_door1
		vCoords[0]	= <<1392.93, 3599.47, 35.13>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_1B")
		Model[1]	= v_ilev_ml_door1
		vCoords[1]	= <<1395.37, 3600.36, 35.13>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_2
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_2A")
		Model[0]	= v_ilev_247door
		vCoords[0]	= <<-3038.22, 588.29, 8.06>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_2B")
		Model[1]	= v_ilev_247door_r
		vCoords[1]	= <<-3039.01, 590.76, 8.06>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_3
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_3A")
		Model[0]	= v_ilev_247door
		vCoords[0]	= <<-3240.13, 1003.16, 12.98>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_3B")
		Model[1]	= v_ilev_247door_r
		vCoords[1]	= <<-3239.90, 1005.75, 12.98>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_4
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_4A")
		Model[0]	= v_ilev_247door
		vCoords[0]	= <<545.50, 2672.75, 42.31>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_4B")
		Model[1]	= v_ilev_247door_r
		vCoords[1]	= <<542.93, 2672.41, 42.31>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_5
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_5A")
		Model[0]	= v_ilev_247door
		vCoords[0]	= <<2559.20, 384.09, 108.77>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_5B")
		Model[1]	= v_ilev_247door_r
		vCoords[1]	= <<2559.30, 386.69, 108.77>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_6
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_6A")
		Model[0]	= v_ilev_247door
		vCoords[0]	= <<2681.29, 3281.43, 55.39>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_6B")
		Model[1]	= v_ilev_247door_r
		vCoords[1]	= <<2682.56, 3283.70, 55.39>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_7
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_7A")
		Model[0]	= v_ilev_247door
		vCoords[0]	= <<1730.03, 6412.07, 35.19>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_7B")
		Model[1]	= v_ilev_247door_r
		vCoords[1]	= <<1732.36, 6410.92, 35.19>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_8
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_8A")
		Model[0]	= v_ilev_247door
		vCoords[0]	= <<1963.92, 3740.08, 32.49>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_8B")
		Model[1]	= v_ilev_247door_r
		vCoords[1]	= <<1966.17, 3741.38, 32.49>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_9
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_9A")
		Model[0]	= v_ilev_247door
		vCoords[0]	= <<27.82, -1349.17, 29.65>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_9B")
		Model[1]	= v_ilev_247door_r
		vCoords[1]	= <<30.42, -1349.17, 29.65>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_SHOP247_10
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_10A")
		Model[0]	= v_ilev_247door
		vCoords[0]	= <<375.35, 323.80, 103.72>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_10B")
		Model[1]	= v_ilev_247door_r
		vCoords[1]	= <<377.88, 323.17, 103.72>>
	//ENDIF
	
	//LIQUOR STORES
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_1
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_LIQUOR_1A")
		Model[0]	= v_ilev_ml_door1
		vCoords[0]	= <<1167.13, 2703.75, 38.30>>
		iHash[1]	= -1
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_2
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_LIQUOR_2A")
		Model[0]	= v_ilev_ml_door1
		vCoords[0]	= <<-2973.53, 390.14, 15.19>>
		iHash[1]	= -1
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_3
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_LIQUOR_3A")
		Model[0]	= v_ilev_ml_door1
		vCoords[0]	= <<-1226.89, -903.12, 12.47>>
		iHash[1]	= -1
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_4
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_LIQUOR_4A")
		Model[0]	= v_ilev_ml_door1
		vCoords[0]	= <<1141.04, -980.32, 46.56>>
		iHash[1]	= -1
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_5
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_LIQUOR_5A")
		Model[0]	= v_ilev_ml_door1
		vCoords[0]	= <<-1490.41, -383.85, 40.31>>
		iHash[1]	= -1
	//ENDIF
	
	//GAS STATIONS
	ELIF WhichHoldUp = eCRIM_HUP_GAS_1
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_GAS_1A")
		Model[0]	= v_ilev_gasdoor
		vCoords[0]	= <<1699.66, 4930.28, 42.21>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_GAS_1B")
		Model[1]	= v_ilev_gasdoor_r
		vCoords[1]	= <<1698.17, 4928.15, 42.21>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_2
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_GAS_2A")
		Model[0]	= v_ilev_gasdoor
		vCoords[0]	= <<-713.07, -916.54, 19.37>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_GAS_2B")
		Model[1]	= v_ilev_gasdoor_r
		vCoords[1]	= <<-710.47, -916.54, 19.37>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_3
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_GAS_3A")
		Model[0]	= v_ilev_gasdoor
		vCoords[0]	= <<-53.96, -1755.72, 29.57>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_GAS_3B")
		Model[1]	= v_ilev_gasdoor_r
		vCoords[1]	= <<-51.97, -1757.39, 29.57>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_4
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_GAS_4A")
		Model[0]	= v_ilev_gasdoor
		vCoords[0]	= <<1158.36, -326.82, 69.36>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_GAS_4B")
		Model[1]	= v_ilev_gasdoor_r
		vCoords[1]	= <<1160.93, -326.36, 69.36>>
	//ENDIF
	ELIF WhichHoldUp = eCRIM_HUP_GAS_5
		iHash[0]	= GET_HASH_KEY("eCRIM_HUP_GAS_5A")
		Model[0]	= v_ilev_gasdoor
		vCoords[0]	= <<-1823.28, 787.37, 138.36>>
		iHash[1]	= GET_HASH_KEY("eCRIM_HUP_GAS_5B")
		Model[1]	= v_ilev_gasdoor_r
		vCoords[1]	= <<-1821.37, 789.13, 138.31>>
	ENDIF
ENDPROC

//PURPOSE: Get the blip location of the specified hold up area
FUNC VECTOR GET_HOLD_UP_BLIP_LOCATION(Crim_HOLD_UP_POINT WhichHoldUp)
	VECTOR vBlipLocation, vDummy
	
	//HOLD_UP_DETAILS(WhichHoldUp, interiorDummy, iHashDummy1, iHashDummy2, iHashDummy3, vBlipLocation, vDummy)	
	GET_HOLD_UP_BLIP_AND_FRONT_DOOR_COORDS(WhichHoldUp, vBlipLocation, vDummy)
	
	RETURN vBlipLocation 
ENDFUNC

//PURPOSE: Get the nearest hold up area (runs through one hold up location per frame - Returns TRUE when it has run through them all)
FUNC BOOL GET_NEAREST_HOLD_UP_AREA(INT &iCounter, FLOAT &fShortestDistance, Crim_HOLD_UP_POINT &iNearestBlipLocation)
	FLOAT fNewDistance													 

	fNewDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_HOLD_UP_BLIP_LOCATION(INT_TO_ENUM(Crim_HOLD_UP_POINT, iCounter)))
	
	IF fNewDistance < fShortestDistance
		fShortestDistance = fNewDistance
		iNearestBlipLocation = INT_TO_ENUM(Crim_HOLD_UP_POINT, iCounter)
	ENDIF 
		
	iCounter++
	
	//Return True when we have checked all the Hold Up Locations
	IF INT_TO_ENUM(Crim_HOLD_UP_POINT, iCounter) = MAX_NUM_HOLDUP_AREAS
		iCounter = 0
		fShortestDistance = 9999.9
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Get the nearest hold up area (runs through all the locations in 1 frame)
FUNC BOOL GET_NEAREST_HOLD_UP_AREA_LOOP(Crim_HOLD_UP_POINT &iNearestBlipLocation, BOOL bOnlyActiveBlips = FALSE)
	FLOAT fNewDistance
	FLOAT fShortestDistance = 9999.9
	INT i
	
	REPEAT MAX_NUM_HOLDUP_AREAS i
		fNewDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_HOLD_UP_BLIP_LOCATION(INT_TO_ENUM(Crim_HOLD_UP_POINT, i)))
		
		IF fNewDistance < fShortestDistance
			IF bOnlyActiveBlips
				IF !IS_BIT_SET(GlobalServerBD_HoldUp.HoldUpServerData.iHoldUpDoneBitSet, i)
				AND IS_BIT_SET(GlobalServerBD_HoldUp.HoldUpServerData.iHoldUpTimerDoneBitSet, i)
					fShortestDistance = fNewDistance
					iNearestBlipLocation = INT_TO_ENUM(Crim_HOLD_UP_POINT, i)
				ENDIF
			ELSE
				fShortestDistance = fNewDistance
				iNearestBlipLocation = INT_TO_ENUM(Crim_HOLD_UP_POINT, i)
			ENDIF
		ENDIF 
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

CONST_INT HUP_MODEL_STAFF		0
CONST_INT HUP_MODEL_CASH_BAG	1
CONST_INT HUP_MODEL_TILL		2
//PURPOSE: Returns the Hold Up model
FUNC MODEL_NAMES GET_HOLD_UP_MODEL(INT HupModel)
	SWITCH HupModel
		CASE HUP_MODEL_STAFF		RETURN MP_M_ShopKeep_01
		CASE HUP_MODEL_CASH_BAG		RETURN P_Poly_Bag_01_S
		CASE HUP_MODEL_TILL			RETURN P_TILL_01_S
	ENDSWITCH
	
	NET_SCRIPT_ASSERT("GET_HOLD_UP_MODEL - Invalid model number passed in")
	RETURN MP_M_ShopKeep_01
ENDFUNC



















#IF IS_DEBUG_BUILD
PROC PROCESS_HOLD_UP_DEBUG_WARPS(HOLD_UP_SERVER_STRUCT &HoldUpServerData)

	INT i
	FOR i = 0 TO MAX_NUM_HOLDUPS-1
		IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, i)
			HoldUpServerData.iHoldUp_StartTimerDelay[i] = -1 
		ENDIF
	ENDFOR

	//24_7
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_1)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1400.5985, 3594.1514, 33.6259 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 45.4632)
		//iHoldUp_CurrentTimer_SHOP247_1 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_SHOP247_1 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_1)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_2)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), << -3034.3882, 591.0876, 6.8258 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 109.7008)
		//iHoldUp_CurrentTimer_SHOP247_2 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_SHOP247_2 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_2)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_3)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), << -3236.1367, 1004.1160, 11.1670 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 90.7177)
		//iHoldUp_CurrentTimer_SHOP247_3 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_SHOP247_3 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_3)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_4)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<543.6606, 2677.0728, 41.1419>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 188.2511)
		//iHoldUp_CurrentTimer_SHOP247_4 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_SHOP247_4 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_4)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_5)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2564.0679, 384.9708, 107.4644>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 86.8214)
		//iHoldUp_CurrentTimer_SHOP247_5 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_SHOP247_5 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_5)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_6)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2686.1780, 3279.9431, 54.2404>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 60.9301)
		//iHoldUp_CurrentTimer_SHOP247_6 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_SHOP247_6 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_6)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_7)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1729.6146, 6409.2871, 34.0005>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 318.4177)
		//iHoldUp_CurrentTimer_SHOP247_7 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_SHOP247_7 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_7)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_8)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1967.6334, 3736.4727, 31.2132>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 34.1863)
		//iHoldUp_CurrentTimer_SHOP247_8 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_SHOP247_8 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_8)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_9)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<26.9920, -1353.4207, 28.3395>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 358.6746)
		//iHoldUp_CurrentTimer_SHOP247_9 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_SHOP247_9 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_9)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_10)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<373.7934, 319.8971, 102.4681>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 317.8379)
		//iHoldUp_CurrentTimer_SHOP247_10 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_SHOP247_10 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_SHOP_247_10)
	ENDIF
	
	//LIQUOR STORES
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_LIQUOR_1)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1166.7, 2698.7, 37.0>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 348.2264)
		//iHoldUp_CurrentTimer_LIQUOR_1 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_LIQUOR_1 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_LIQUOR_1)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_LIQUOR_2)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), << -2978.2410, 391.2192, 14.0224 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 267.3926)
		//iHoldUp_CurrentTimer_LIQUOR_2 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_LIQUOR_2 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_LIQUOR_2)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_LIQUOR_3)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1229.3036, -898.4478, 11.2787>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 214.8579)
		//iHoldUp_CurrentTimer_LIQUOR_3 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_LIQUOR_3 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_LIQUOR_3)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_LIQUOR_4)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1146.4921, -980.9312, 45.1804>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 85.6790)
		//iHoldUp_CurrentTimer_LIQUOR_4 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_LIQUOR_4 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_LIQUOR_4)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_LIQUOR_5)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1498.9261, -387.4839, 39.1369>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 294.6221)
		//iHoldUp_CurrentTimer_LIQUOR_5 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_LIQUOR_5 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_LIQUOR_5)
	ENDIF
	
	//GAS STATIONS
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_GAS_1)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1693.1393, 4932.7178, 41.0783>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 238.0475)
		//iHoldUp_CurrentTimer_GAS_1 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_GAS_1 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_GAS_1)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_GAS_2)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-711.7835, -923.3544, 18.0141>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 2.8374)
		//iHoldUp_CurrentTimer_GAS_2 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_GAS_2 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_GAS_2)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_GAS_3)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-56.0217, -1760.1188, 28.0396>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 321.9268)
		//iHoldUp_CurrentTimer_GAS_3 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_GAS_3 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_GAS_3)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_GAS_4)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1160.5577, -332.0321, 67.6580>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 11.0789)
		//iHoldUp_CurrentTimer_GAS_4 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_GAS_4 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_GAS_4)
	ENDIF
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_GAS_5)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1818.8751, 785.1304, 136.8768>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 47.8035)
		//iHoldUp_CurrentTimer_GAS_4 = 0
		//MPGlobalsAmbience.iHoldUp_StartTimer_GAS_5 = 0
		CLEAR_BIT(MPGlobalsAmbience.iHoldUp_DebugLaunchBitset, biHoldUp_DebugLaunch_GAS_5)
	ENDIF
ENDPROC
#ENDIF





//CONST_INT GET_HOLD_UP_STORE_REFRESH_TIME()			1000 * 60 * 2	// 1000 * 60 * 6 	//6 minutes
CONST_FLOAT HOLD_UP_ENTRANCE_RANGE			5.0
CONST_FLOAT HOLD_UP_ENTRANCE_RANGE_HEIGHT	2.5
CONST_FLOAT HOLD_UP_GIVE_XP_RANGE			20.0

CONST_INT HOLD_UP_WAIT						0
CONST_INT HOLD_UP_RUNNING					1
CONST_INT HOLD_UP_LAUNCHING					2

CONST_INT biHelpTextDone				0
CONST_INT biHoldUpsActivatedInFlow		1
CONST_INT biHoldUpsTutorialLaunched		2
CONST_INT biHoldUpLaunched				3	
CONST_INT biHoldUpActivityDisplay		4
CONST_INT biHoldUpSpawnInsideCheck		5

//SERVER DATA - In MPGlobalsCommon.sch

//LOCAL DATA
STRUCT HOLD_UP_STRUCT
	INT Crim_HoldUpState
	Crim_HOLD_UP_POINT NearestHoldUpPoint = eCRIM_HUP_INVALID
	VECTOR vNearestHoldUpArea
	VECTOR vDoorCoord
	FLOAT fHoldUpAreaDistance
	//INTERIOR_INSTANCE_INDEX interiorID
	INTERIOR_INSTANCE_INDEX InteriorID[MAX_NUM_HOLDUPS]
	INT iRoomHashMain
	INT iRoomHashMain2	// optional room
	INT iRoomHashBack
	INT iHoldUpBitset
	INT iHoldUpLoop
	MP_MISSION_DATA HoldupToLaunch
	INT iInsideThisHoldUpBitset
	INT iInsideThisHoldUpStrictBitset
	INT iHideBlipHoldUpBitset
	INT iHoldUpLoadSoundBitset
	
	BLIP_INDEX HoldUpBlips[MAX_NUM_HOLDUPS]
	INT iBlipScaleNone
	INT iHoldUpCounter = 0
	FLOAT fShortestDistance = 9999.9
	Crim_HOLD_UP_POINT WorkingNearestHoldUpPoint = eCRIM_HUP_INVALID  //Holds working calculation state (only correct on the last frame slice)
	
	INT iSpawnedInStore = -1
	SCRIPT_TIMER iSPawnedInStoreDisableTimer
	
	SCENARIO_BLOCKING_INDEX scenarioBlocking[MAX_NUM_HOLDUPS]
ENDSTRUCT



//PURPOSE: 
PROC INITIALISE_HOLD_UP_TIMERS(HOLD_UP_SERVER_STRUCT &HoldUpServerData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INT iHoldUp
		REPEAT MAX_NUM_HOLDUPS iHoldUp
			RESET_NET_TIMER(HoldUpServerData.iHoldUp_StartTimer[iHoldUp])
			START_NET_TIMER(HoldUpServerData.iHoldUp_StartTimer[iHoldUp])
			HoldUpServerData.iHoldUp_StartTimerDelay[iHoldUp] = -1	//HoldUpServerData.iHoldUp_StartTimer[INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp)] -= GET_HOLD_UP_STORE_REFRESH_TIME()
			SET_BIT(HoldUpServerData.iHoldUpTimerDoneBitSet, iHoldUp)
		ENDREPEAT
		NET_PRINT("   --->   HOLD UP - TIMERS INITIALISED") NET_NL()
	ENDIF
ENDPROC


//PURPOSE: Returns the host player for the Hold Ups
FUNC INT GET_HOLD_UP_HOST()
	RETURN SPECIFIC_PLAYER(NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT()))
ENDFUNC


FUNC INT GET_HOLD_UP_STORE_REFRESH_TIME()	
	RETURN g_sMPTunables.igb_smashandgrab_store_cooldown
ENDFUNC





















/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////              Hold Ups Streak              //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
//CONST_INT HOLD_UP_STREAK_START			5
//CONST_INT HOLD_UP_STREAK_TIME 			720000	//(1000*60*12)	- Quarter of a Game Day
//CONST_INT HOLD_UP_STREAK_RESET_TIMER 	1440000	//(1000*60*24)	- Half of a Game Day
//
////PURPOSE: Maintains the Hold Up Streaks
//PROC PROCESS_HOLD_UP_STREAK(PLAYER_INDEX PlayerId)
//	//Check for hold Up Streak
//	IF NOT HAS_NET_TIMER_STARTED(MpGlobals.iHoldUpStreakTimer)
//		IF HAS_NET_TIMER_EXPIRED(MpGlobals.iHoldUpStreakResetTimer, HOLD_UP_STREAK_RESET_TIMER)
//			
//			NETWORK_CLAN_DESC LocalPlayerCrew
//			NETWORK_CLAN_DESC OtherPlayerCrew
//			LocalPlayerCrew = GET_PLAYER_CREW(PLAYER_ID())
//			OtherPlayerCrew = GET_PLAYER_CREW(PlayerId)
//			
//			IF LocalPlayerCrew.Id = OtherPlayerCrew.Id
//		
//				IF PLAYER_ID() = PlayerId
//					IF MpGlobals.iHoldUpsDoneInCloseSuccessionCrew >= HOLD_UP_STREAK_START
//						PRINT_TICKER("HU_STRET1")	//~g~Hold Up Streak: Rob stores for bonus XP
//						START_NET_TIMER(MPGlobals.iHoldUpStreakTimer)
//						NET_PRINT("   --->   HOLD UP - PROCESS_HOLD_UP_STREAK - STARTED")NET_NL()
//					ENDIF
//					MpGlobals.iHoldUpsDoneInCloseSuccessionCrew = 0
//				ENDIF
//				MpGlobals.iHoldUpsDoneInCloseSuccessionCrew += GlobalplayerBD[NATIVE_TO_INT(PlayerId)].iHoldUpsDoneInCloseSuccession
//				
//			ENDIF
//		ENDIF
//		
//	//Do Hold Up Streak
//	ELSE
//		IF HAS_NET_TIMER_EXPIRED(MpGlobals.iHoldUpStreakTimer, HOLD_UP_STREAK_TIME)
//			PRINT_TICKER("HU_STRET2")	//~r~Hold Up Streak Over
//			RESET_NET_TIMER(MPGlobals.iHoldUpStreakTimer)
//			RESET_NET_TIMER(MpGlobals.iHoldUpStreakResetTimer)
//			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpsDoneInCloseSuccession = 0
//			NET_PRINT("   --->   HOLD UP - PROCESS_HOLD_UP_STREAK - ENDED")NET_NL()
//			NET_PRINT("   --->   HOLD UP - PROCESS_HOLD_UP_STREAK - iHoldUpsDoneInCloseSuccession = 0")NET_NL()
//		ENDIF
//	ENDIF
//ENDPROC











//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////              Hold Ups (Ryan Baker)                   //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Get the timers for each hold up
//FUNC INT GET_HOLD_UP_TIMERS(Crim_HOLD_UP_POINT WhichHoldUp)
//	RETURN HoldUpServerData.iHoldUp_StartTimer[ENUM_TO_INT(WhichHoldUp)]
//ENDFUNC

//Set the Nearest Hold Up Timer
PROC SET_HOLD_UP_TIMER(HOLD_UP_SERVER_STRUCT &HoldUpServerData, Crim_HOLD_UP_POINT WhichHoldUp)
	RESET_NET_TIMER(HoldUpServerData.iHoldUp_StartTimer[ENUM_TO_INT(WhichHoldUp)])
	START_NET_TIMER(HoldUpServerData.iHoldUp_StartTimer[ENUM_TO_INT(WhichHoldUp)])
	HoldUpServerData.iHoldUp_StartTimerDelay[ENUM_TO_INT(WhichHoldUp)] = GET_HOLD_UP_STORE_REFRESH_TIME()
	//HoldUpServerData.iHoldUp_StartTimer[ENUM_TO_INT(WhichHoldUp)] = GET_NETWORK_TIME()
	NET_PRINT("   --->   HOLD UP - SETTING TIMER - LOCATION = ") NET_PRINT_INT(ENUM_TO_INT(WhichHoldUp)) NET_NL()
ENDPROC

//PURPOSE: Cleanup Hold Ups
PROC CLEANUP_HOLD_UPS(HOLD_UP_STRUCT &HoldUpData, BOOL bRemoveBlip = FALSE)//, BOOL bClearHelpText = TRUE)
	/*IF IS_LOCAL_PLAYER_DOING_HOLD_UP_TUTORIAL()
	AND bClearHelpText = TRUE
	AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HOLDUPHELP_1")
		CLEAR_HELP()
	ENDIF*/
	IF bRemoveBlip = TRUE
		/*IF DOES_BLIP_EXIST(HoldUpData.Crim_HoldUpBlip)
			REMOVE_BLIP(HoldUpData.Crim_HoldUpBlip)
			NET_PRINT("   --->   HOLD UP - Crim_HoldUpBlip - REMOVED - A") NET_NL()
		ENDIF*/
	ENDIF
	HoldUpData.Crim_HoldUpState = HOLD_UP_WAIT
	CLEAR_BIT(HoldUpData.iHoldUpBitset, biHoldUpLaunched)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_HOLD_UP_MODEL(HUP_MODEL_STAFF))
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG))
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_HOLD_UP_MODEL(HUP_MODEL_TILL))
	REMOVE_ANIM_DICT("mp_am_hold_up")
	REMOVE_ANIM_DICT("mp_missheist_countrybank@cower")
	IF bRemoveBlip = TRUE
		IF IS_BIT_SET(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList)	//(HoldUpData.iHoldUpBitset, biHoldUpActivityDisplay)
			REMOVE_ACTIVITY_FROM_DISPLAY_LIST(HoldUpData.vDoorCoord, GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_HOLD_UPS))
			CLEAR_BIT(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList)
			//NET_PRINT_TIME() NET_PRINT("     ----->   AM_HOLD_UP - A - REMOVED FROM ACTIVITY DISPLAY  <-----     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Cleanup Ambient Hold Ups - Check if player is running an actual Hold Up
PROC CLEANUP_AMB_HOLD_UPS(HOLD_UP_STRUCT &HoldUpData)
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_HOLD_UP")) > 0
		CLEANUP_HOLD_UPS(HoldUpData, FALSE)//, FALSE)
	ELSE
		CLEANUP_HOLD_UPS(HoldUpData, TRUE)
	ENDIF
ENDPROC

//PURPOSE: Retursn TRUe if player is not in Freemode or is in Freemode and passes the checks.
FUNC BOOL HOLD_UP_OKAY_FREEMODE_CHECKS()//BOOL bDoUnlockCheck = TRUE)
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF
	
	IF HIDE_BLIP_BECAUSE_PLAYER_IS_STARTING_RACE()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RACETOPOINT)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), TRUE, TRUE) 
		RETURN FALSE
	ENDIF
	
	//IF bDoUnlockCheck = TRUE
	//	IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HOLD_UPS)
	//		RETURN FALSE
	//	ENDIF
	//ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_CRITICAL_TO_CASHING_OUT()
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
	AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CASHING_OUT
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls the Blips for each Hold Up
PROC CONTROL_HOLD_UP_BLIPS(HOLD_UP_SERVER_STRUCT &HoldUpServerData, HOLD_UP_STRUCT &HoldUpData)
	
	//IF HAS_NET_TIMER_STARTED(HoldUpServerData.iHoldUp_StartTimer[iHoldUp])
	//AND HAS_NET_TIMER_EXPIRED(HoldUpServerData.iHoldUp_StartTimer[iHoldUp], HoldUpServerData.iHoldUp_StartTimerDelay[iHoldUp]) //GET_NETWORK_TIME() > (GET_HOLD_UP_TIMERS(INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp)) + GET_HOLD_UP_STORE_REFRESH_TIME())
	
	IF IS_BIT_SET(HoldUpData.iHoldUpBitset, biHoldUpsActivatedInFlow)
	AND IS_BIT_SET(HoldUpServerData.iHoldUpTimerDoneBitSet, HoldUpData.iHoldUpLoop)
	AND NOT IS_BIT_SET(HoldUpServerData.iHoldUpDoneBitSet, HoldUpData.iHoldUpLoop)
	AND NOT NETWORK_IS_IN_TUTORIAL_SESSION() 			//( NOT NETWORK_IS_IN_TUTORIAL_SESSION() OR NOT IS_BIT_SET(HoldUpData.iHoldUpBitset, biHoldUpsTutorialLaunched))
	//AND NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
	////AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpDoneBitSet, HoldUpData.iHoldUpLoop)
	AND HOLD_UP_OKAY_FREEMODE_CHECKS()//FALSE)
	AND NOT SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_STORES)
	AND NOT SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
	AND NOT IS_PLAYER_CRITICAL_TO_CASHING_OUT()
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT IS_PLAYER_ON_MP_INTRO()
	#ENDIF
		IF NOT DOES_BLIP_EXIST(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
			SET_BIT(MPGlobalsAmbience.iHoldUpActiveBitSet, HoldUpData.iHoldUpLoop)
			HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop] = ADD_BLIP_FOR_COORD(GET_HOLD_UP_BLIP_LOCATION(INT_TO_ENUM(Crim_HOLD_UP_POINT, HoldUpData.iHoldUpLoop)))
			IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HOLD_UPS)
				SET_BLIP_DISPLAY(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], DISPLAY_NOTHING)
				SET_BIT(HoldUpData.iBlipScaleNone, HoldUpData.iHoldUpLoop)
				NET_PRINT("   --->   HOLD UP - BLIP SET TO DISPLAY NOTHING - BLIP = ") NET_PRINT_INT(HoldUpData.iHoldUpLoop) NET_NL()
			ENDIF
			SET_BLIP_SCALE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], BLIP_SIZE_NETWORK_COORD)
			SET_BLIP_SPRITE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop],  RADAR_TRACE_CRIM_HOLDUPS)			
			SET_BLIP_AS_SHORT_RANGE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], TRUE)
	
			SET_BLIP_PRIORITY(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], BLIPPRIORITY_LOW)

			
			INT iHoldUpsAwardBit
			iHoldUpsAwardBit= GET_MP_INT_CHARACTER_STAT(MP_STAT_HOLDUPS_BITSET)
			IF IS_BIT_SET(iHoldUpsAwardBit, ENUM_TO_INT(HoldUpData.iHoldUpLoop))
				SHOW_TICK_ON_BLIP(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], TRUE)
			ELSE
				SHOW_TICK_ON_BLIP(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], FALSE)
			ENDIF
			
			/*IF HoldUpData.iHoldUpLoop >= 15
				SET_BLIP_NAME_FROM_TEXT_FILE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], "HUP_BLIP3")
			ELIF HoldUpData.iHoldUpLoop >= 10
				SET_BLIP_NAME_FROM_TEXT_FILE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], "HUP_BLIP2")
			ELSE
				SET_BLIP_NAME_FROM_TEXT_FILE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], "HUP_BLIP1")
			ENDIF*/
			SET_BLIP_NAME_FROM_TEXT_FILE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], "HUP_BLIP0")
			NET_PRINT("   --->   HOLD UP - BLIP ADDED - BLIP = ") NET_PRINT_INT(HoldUpData.iHoldUpLoop) NET_NL()
		ELSE
			IF GET_BLIP_COLOUR(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop]) = BLIP_COLOUR_RED
				SET_BLIP_COLOUR(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], BLIP_COLOUR_WHITE)
				SET_BLIP_AS_SHORT_RANGE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], TRUE)
				NET_PRINT("   --->   HOLD UP - BLIP MADE WHITE (NO LONGER SNITCH TIP-OFF) - BLIP = ") NET_PRINT_INT(HoldUpData.iHoldUpLoop) NET_NL()
			ENDIF
			IF IS_BIT_SET(HoldUpData.iBlipScaleNone, HoldUpData.iHoldUpLoop)
				IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HOLD_UPS)
					SET_BLIP_DISPLAY(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], DISPLAY_BLIP)
					CLEAR_BIT(HoldUpData.iBlipScaleNone, HoldUpData.iHoldUpLoop)
					NET_PRINT("   --->   HOLD UP - BLIP SCALE SET TO BLIP_SIZE_NETWORK_COORD - BLIP = ") NET_PRINT_INT(HoldUpData.iHoldUpLoop) NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
			CLEAR_BIT(MPGlobalsAmbience.iHoldUpActiveBitSet, HoldUpData.iHoldUpLoop)
			REMOVE_BLIP(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
			NET_PRINT("   --->   HOLD UP - BLIP REMOVED - BLIP = ") NET_PRINT_INT(HoldUpData.iHoldUpLoop) NET_NL()
		ENDIF
		IF IS_BIT_SET(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList)
			REMOVE_ACTIVITY_FROM_DISPLAY_LIST(HoldUpData.vDoorCoord,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_HOLD_UPS))
			CLEAR_BIT(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList)
			//NET_PRINT_TIME() NET_PRINT("     ----->   AM_HOLD_UP - B - REMOVED FROM ACTIVITY DISPLAY  <-----     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

PROC CONTROL_HOLD_UP_BLIPS_LONG_RANGE_FOR_SMASH_AND_GRAB(HOLD_UP_SERVER_STRUCT &HoldUpServerData, HOLD_UP_STRUCT &HoldUpData)
	
	IF IS_BIT_SET(HoldUpData.iHoldUpBitset, biHoldUpsActivatedInFlow)
	AND IS_BIT_SET(HoldUpServerData.iHoldUpTimerDoneBitSet, HoldUpData.iHoldUpLoop)
	AND NOT IS_BIT_SET(HoldUpServerData.iHoldUpDoneBitSet, HoldUpData.iHoldUpLoop)
	AND NOT NETWORK_IS_IN_TUTORIAL_SESSION()
	AND HOLD_UP_OKAY_FREEMODE_CHECKS()//FALSE)
	AND NOT SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_STORES)
	AND NOT SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
		IF DOES_BLIP_EXIST(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
			IF MPGlobalsAmbience.sSmashAndGrabStruct.bOnSmashGrab = TRUE
				IF IS_BLIP_SHORT_RANGE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
					SET_BLIP_AS_SHORT_RANGE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], FALSE)
				ENDIF
			ELSE
				IF NOT IS_BLIP_SHORT_RANGE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
					SET_BLIP_AS_SHORT_RANGE(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Makes Clients match the Scenario Blocking done by the Server
PROC CONTROL_HOLD_UP_SCENARIO_BLOCKING(HOLD_UP_SERVER_STRUCT &HoldUpServerData, HOLD_UP_STRUCT &HoldUpData)
	//REMOVE
	IF HoldUpServerData.scenarioBlocking[HoldUpData.iHoldUpLoop] = NULL
		IF HoldUpData.scenarioBlocking[HoldUpData.iHoldUpLoop] != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(HoldUpData.scenarioBlocking[HoldUpData.iHoldUpLoop])
			HoldUpData.scenarioBlocking[HoldUpData.iHoldUpLoop] = NULL
			NET_PRINT("   --->   HOLD UP - CONTROL_HOLD_UP_SCENARIO_BLOCKING - REMOVE_SCENARIO_BLOCKING_AREA - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
		ENDIF
		
	//ADD
	ELSE
		IF HoldUpData.scenarioBlocking[HoldUpData.iHoldUpLoop] = NULL
			VECTOR vCoord = GET_HOLD_UP_BLIP_LOCATION( INT_TO_ENUM(Crim_HOLD_UP_POINT, HoldUpData.iHoldUpLoop) )
			//IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vCoord - <<15, 15, 15>>, vCoord + <<15, 15, 15>>) 
					HoldUpData.scenarioBlocking[HoldUpData.iHoldUpLoop] = ADD_SCENARIO_BLOCKING_AREA(vCoord - <<15, 15, 15>>, vCoord + <<15, 15, 15>>)
					PRINTLN("   --->   HOLD UP - CONTROL_HOLD_UP_SCENARIO_BLOCKING - ADD_SCENARIO_BLOCKING_AREA - AT ", vCoord, " HOLD UP = ", HoldUpData.iHoldUpLoop)
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if player is in the extra area (JUST FOR GAS STATION JUST NOW)
FUNC BOOL IN_EXTRA_MAIN_AREA(PED_INDEX PlayerPedID, Crim_HOLD_UP_POINT WhichHoldUp)
	
	IF WhichHoldUp >= eCRIM_HUP_LIQUOR_1
	AND WhichHoldUp <= eCRIM_HUP_GAS_5
	
		VECTOR vMin2 = <<0, 0, 0>>
		VECTOR vMax2 = <<0, 0, 0>>
		FLOAT fWidth2 = 0
		HOLD_UP_DETAILS_EXTRA_AREA(WhichHoldUp, vMin2, vMax2, fWidth2)
		
		IF IS_ENTITY_IN_ANGLED_AREA(PlayerPedID, vMin2, vMax2, fWidth2)
			//NET_PRINT("   --->   HOLD UP - IN_EXTRA_MAIN_AREA - TRUE - HOLD UP = ")NET_PRINT_INT(ENUM_TO_INT(WhichHoldUp))NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if player is in the extra area (JUST FOR GAS STATION JUST NOW)
FUNC BOOL PED_IS_BEHIND_HOLD_UP_COUNTER(PED_INDEX PlayerPedID, Crim_HOLD_UP_POINT WhichHoldUp)
	
	VECTOR vMin2 = <<0, 0, 0>>
	VECTOR vMax2 = <<0, 0, 0>>
	FLOAT fWidth2 = 0
	HOLD_UP_DETAILS_BEHIND_COUNTER(WhichHoldUp, vMin2, vMax2, fWidth2)
	
	IF IS_ENTITY_IN_ANGLED_AREA(PlayerPedID, vMin2, vMax2, fWidth2)
		//NET_PRINT("   --->   HOLD UP - IN_EXTRA_MAIN_AREA - TRUE - HOLD UP = ")NET_PRINT_INT(ENUM_TO_INT(WhichHoldUp))NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: THe player checks if they are in a Hold Up store and sends an event if the state has changed
PROC CONTROL_IN_HOLD_UP_STORE(HOLD_UP_SERVER_STRUCT HoldUpServerData, HOLD_UP_STRUCT &HoldUpData)
	
//	VECTOR vMin = <<0, 0, 0>>
//	VECTOR vMax = <<0, 0, 0>>
//	VECTOR vBlip = <<0, 0, 0>>
//	FLOAT fWidth = 0
//	HOLD_UP_DETAILS(INT_TO_ENUM(Crim_HOLD_UP_POINT, HoldUpData.iHoldUpLoop), vMin, vMax, fWidth, vBlip)
//		
//	VECTOR vBackRoomMin = <<0, 0, 0>>
//	VECTOR vBackRoomMax = <<0, 0, 0>>
//	FLOAT fBackRoomWidth = 0
//	HOLD_UP_BACKROOM_DETAILS(INT_TO_ENUM(Crim_HOLD_UP_POINT, HoldUpData.iHoldUpLoop), vBackRoomMin, vBackRoomMax, fBackRoomWidth)

	//INTERIOR_INSTANCE_INDEX interiorID		= NULL
	INT iRoomHashMain						= -1
	INT iRoomHashMain2						= -1
	INT iRoomHashBack						= -1
	VECTOR vBlip 							= <<0, 0, 0>>
	VECTOR vFrontDoorCoord					= <<0, 0, 0>>
	HOLD_UP_DETAILS(INT_TO_ENUM(Crim_HOLD_UP_POINT, HoldUpData.iHoldUpLoop), HoldUpData.interiorID[HoldUpData.iHoldUpLoop], iRoomHashMain, iRoomHashMain2, iRoomHashBack, vBlip, vFrontDoorCoord)
	
	
	BOOL bInMainArea
	BOOL bInBackRoom
	//BOOL bInExtraArea
	BOOL bAtEntrance

//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vMin, vMax, fWidth)
//		bInMainArea = TRUE
//	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBackRoomMin, vBackRoomMax, fBackRoomWidth)
//		bInBackRoom = TRUE
//	ELIF IN_EXTRA_MAIN_AREA(PLAYER_PED_ID(), INT_TO_ENUM(Crim_HOLD_UP_POINT, HoldUpData.iHoldUpLoop))
//		bInExtraArea = TRUE
//	ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMin, <<HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE_HEIGHT>>)
//		bAtEntrance = TRUE
//	ENDIF

	// Inside the shop
	IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = HoldUpData.interiorID[HoldUpData.iHoldUpLoop]	//interiorID
		
		INT iRoomHashCurrent = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
		IF iRoomHashCurrent = iRoomHashMain
		OR (iRoomHashMain2 != -1 AND iRoomHashCurrent = iRoomHashMain2)
			bInMainArea = TRUE
		ELIF iRoomHashCurrent = iRoomHashBack
			bInBackRoom = TRUE
		ENDIF
	
	// Outside the shop but at the entrance
	ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vFrontDoorCoord, <<HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE_HEIGHT>>)
		bAtEntrance = TRUE
	ENDIF
	
	//DEBUGGING PRINTS
	/*IF bInMainArea = TRUE	//IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vMin, vMax, fWidth)
		NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - CHECK - IN MAIN AREA - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
	ENDIF
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBackRoomMin, vBackRoomMax, fBackRoomWidth)
		NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - CHECK - IN BACKROOM - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
	ENDIF
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMin, <<HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE_HEIGHT>>)
		NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - CHECK - IN ENTRANCE - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
	ENDIF
	IF IN_EXTRA_MAIN_AREA(PLAYER_PED_ID(), INT_TO_ENUM(Crim_HOLD_UP_POINT, HoldUpData.iHoldUpLoop))
		NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - CHECK - IN EXTRA AREA - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
	ENDIF*/
	
	IF bInMainArea = TRUE	//IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vMin, vMax, fWidth)
	OR bInBackRoom = TRUE 	//IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBackRoomMin, vBackRoomMax, fBackRoomWidth)
//	OR bInExtraArea  = TRUE	//IN_EXTRA_MAIN_AREA(PLAYER_PED_ID(), INT_TO_ENUM(Crim_HOLD_UP_POINT, HoldUpData.iHoldUpLoop))
	OR bAtEntrance = TRUE	//IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMin, <<HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE_HEIGHT>>)
		
		//Hide Blip when inside + Update Strict Instore check
		IF bInMainArea = TRUE
		OR bInBackRoom = TRUE 
//		OR bInExtraArea = TRUE

		IF NOT IS_BIT_SET(HoldUpData.iHoldUpLoadSoundBitset, HoldUpData.iHoldUpLoop)
			IF REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\MARKET_CASH_REGISTER")
				SET_BIT(HoldUpData.iHoldUpLoadSoundBitset, HoldUpData.iHoldUpLoop)
				NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - SOUNDS LOADED - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
			ENDIF
		ENDIF

			IF NOT IS_BIT_SET(HoldUpData.iHideBlipHoldUpBitset, HoldUpData.iHoldUpLoop)
				IF DOES_BLIP_EXIST(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
					SET_BLIP_DISPLAY(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], DISPLAY_NOTHING)
					NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - HIDE BLIP - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
					SET_BIT(HoldUpData.iHideBlipHoldUpBitset, HoldUpData.iHoldUpLoop)
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(HoldUpData.iInsideThisHoldUpStrictBitset, HoldUpData.iHoldUpLoop)
				BROADCAST_IN_HOLD_UP_STORE_STRICT(HoldUpData.iHoldUpLoop, TRUE, GET_HOLD_UP_HOST())
				SET_BIT(HoldUpData.iInsideThisHoldUpStrictBitset, HoldUpData.iHoldUpLoop)
				NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - PLAYER IN INTERIOR - STRICT - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
				
				//if player is in a store that should be locked as a result of a marketplace transition, clear the store respawn, and make sure control is on
				IF NOT DOES_BLIP_EXIST(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
				//AND NOT IS_BIT_SET(HoldUpServerData.iHoldUpFakeDoneBitSet, HoldUpData.iHoldUpLoop)
					IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
						NET_PRINT_TIME() NET_PRINT("     ---------->     HOLD UP - [STORETRANS] coming back from marketplace and door should be locked, setting control on     <----------     ") NET_NL()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
					ENDIF
				ELSE
					IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
						NET_PRINT_TIME() NET_PRINT("     ---------->     HOLD UP - [STORETRANS] coming back from marketplace and setting control off     <----------     ") NET_NL()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION | NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE )
					ENDIF
				ENDIF
				
			ENDIF
			
		ELSE
			IF IS_BIT_SET(HoldUpData.iHideBlipHoldUpBitset, HoldUpData.iHoldUpLoop)
				IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HOLD_UPS)
					IF DOES_BLIP_EXIST(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
						SET_BLIP_DISPLAY(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], DISPLAY_BLIP)
					ENDIF
					CLEAR_BIT(HoldUpData.iHideBlipHoldUpBitset, HoldUpData.iHoldUpLoop)
					NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - DISPLAY BLIP - A - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(HoldUpData.iInsideThisHoldUpBitset, HoldUpData.iHoldUpLoop)
			BROADCAST_IN_HOLD_UP_STORE(HoldUpData.iHoldUpLoop, TRUE, GET_HOLD_UP_HOST())
			
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iCurrentHoldUp = HoldUpData.iHoldUpLoop		//For spectator cams
			
			//Do Closed Store Help
			IF NOT DOES_BLIP_EXIST(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
			AND NOT IS_BIT_SET(HoldUpServerData.iHoldUpFakeDoneBitSet, HoldUpData.iHoldUpLoop)
			AND NOT IS_LOCAL_PLAYER_DOING_HOLD_UP_TUTORIAL()	
				//IF bInMainArea = TRUE
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_HOLD_UP")) = 0
					TEXT_LABEL_15 tlHelpTemp
					IF HoldUpData.iHoldUpLoop >= ENUM_TO_INT(eCRIM_HUP_GAS_1)
						tlHelpTemp = "FHU_CLOSED2"
					ELIF HoldUpData.iHoldUpLoop <= ENUM_TO_INT(eCRIM_HUP_SHOP247_10)
						tlHelpTemp = "FHU_CLOSED0"
					ELSE
						tlHelpTemp = "FHU_CLOSED1"
					ENDIF
					PRINT_HELP(tlHelpTemp)
					NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - DO CLOSED STORE HELP - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
				ENDIF
			ENDIF
			
			SET_BIT(HoldUpData.iInsideThisHoldUpBitset, HoldUpData.iHoldUpLoop)
			NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - PLAYER IN INTERIOR - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
		ENDIF
		
		//Load Sounds
		IF NOT IS_BIT_SET(HoldUpData.iHoldUpLoadSoundBitset, HoldUpData.iHoldUpLoop)
			IF HINT_SCRIPT_AUDIO_BANK("SCRIPT\\MARKET_CASH_REGISTER")
			ENDIF
		ENDIF
		
	ELSE
		IF IS_BIT_SET(HoldUpData.iInsideThisHoldUpBitset, HoldUpData.iHoldUpLoop)
			BROADCAST_IN_HOLD_UP_STORE(HoldUpData.iHoldUpLoop, FALSE, GET_HOLD_UP_HOST())
			
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iCurrentHoldUp = -1		//For spectator cams
			
			//Cleanup Store Closed Help
			IF NOT DOES_BLIP_EXIST(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FHU_CLOSED0")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FHU_CLOSED1")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FHU_CLOSED2")
					CLEAR_HELP()
					NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - CLEAR CLOSED STORE HELP - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop])
					IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HOLD_UPS)
						SET_BLIP_DISPLAY(HoldUpData.HoldUpBlips[HoldUpData.iHoldUpLoop], DISPLAY_BLIP)
						NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - DISPLAY BLIP - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
					ENDIF
				ENDIF
			ENDIF
			
			CLEAR_BIT(HoldUpData.iInsideThisHoldUpBitset, HoldUpData.iHoldUpLoop)
			NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - PLAYER LEFT INTERIOR - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
		ENDIF
		
		IF IS_BIT_SET(HoldUpData.iInsideThisHoldUpStrictBitset, HoldUpData.iHoldUpLoop)
			BROADCAST_IN_HOLD_UP_STORE_STRICT(HoldUpData.iHoldUpLoop, FALSE, GET_HOLD_UP_HOST())
			CLEAR_BIT(HoldUpData.iInsideThisHoldUpStrictBitset, HoldUpData.iHoldUpLoop)
			NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - PLAYER LEFT INTERIOR - STRICT - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
		ENDIF
		
		//Unload Sounds
		IF IS_BIT_SET(HoldUpData.iHoldUpLoadSoundBitset, HoldUpData.iHoldUpLoop)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\MARKET_CASH_REGISTER")
			CLEAR_BIT(HoldUpData.iHoldUpLoadSoundBitset, HoldUpData.iHoldUpLoop)
			NET_PRINT("   --->   HOLD UP - CONTROL_IN_HOLD_UP_STORE - SOUNDS UNLOADED - HOLD UP = ")NET_PRINT_INT(HoldUpData.iHoldUpLoop)NET_NL()
		ENDIF
	ENDIF
	
	//Suppress
	IF HoldUpData.iInsideThisHoldUpBitset != 0
		SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
		//NET_PRINT("   --->   HOLD UP - SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME")NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Resets Hold Up Done BitSet
/*PROC CONTROL_HOLD_UP_DONE_BITSET(HOLD_UP_SERVER_STRUCT &HoldUpServerData, INT iHoldUp)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpDoneBitSet, iHoldUp)
		//IF GET_NETWORK_TIME() < (GET_HOLD_UP_TIMERS(INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp)) + GET_HOLD_UP_STORE_REFRESH_TIME())
		IF IS_BIT_SET(HoldUpServerData.iHoldUpDoneBitSet, iHoldUp)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpDoneBitSet, iHoldUp)
			NET_PRINT_TIME() NET_PRINT("     ---------->     HOLD UP - CLEARED iHoldUpDoneBitSet - HOLD UP = ") NET_PRINT_INT(iHoldUp) NET_NL()
		ENDIF
	ENDIF
ENDPROC*/

//PURPOSE: Resets Hold Up Made Active BitSet
/*PROC CONTROL_HOLD_UP_MADE_ACTIVE_BITSET(HOLD_UP_SERVER_STRUCT HoldUpServerData, INT iHoldUp)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpMakeActiveBitSet, iHoldUp)
		//IF GET_NETWORK_TIME() < (GET_HOLD_UP_TIMERS(INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp)) + GET_HOLD_UP_STORE_REFRESH_TIME())
		IF IS_BIT_SET(HoldUpServerData.iHoldUpMakeActiveBitSet, iHoldUp)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpMakeActiveBitSet, iHoldUp)
			NET_PRINT_TIME() NET_PRINT("     ---------->     HOLD UP - CLEARED iHoldUpMakeActiveBitSet - HOLD UP = ") NET_PRINT_INT(iHoldUp) NET_NL()
		ENDIF
	ENDIF
ENDPROC*/

//PURPOSE: Checks if anyone is running a Hold Up and reset that Hold ups timer
//PROC SERVER_MAINTAIN_HOLD_UP_PLAYERS(HOLD_UP_SERVER_STRUCT &HoldUpServerData, INT iHoldUp)
//	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		
//		CLEAR_BIT(HoldUpServerData.iHoldUpDoneBitSet, iHoldUp)
//		CLEAR_BIT(HoldUpServerData.iHoldUpMakeActiveBitSet, iHoldUp)
//		
//		/*INT iParticipant
//		//Repeat through all players
//		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
//			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
//				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
//				PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
//				
//				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PlayerId)
//					
//					//EVENTS
//					//**Make Hold Up Active
//					//Done Hold Up
//					//In Hold Up Interior
//					//Left Hold Up Interior
//					
//					//Check if player has Requested a Hold Up to be set Active Reset
//					/*IF NOT IS_BIT_SET(HoldUpServerData.iHoldUpMakeActiveBitSet, iHoldUp)
//					AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iHoldUpMakeActiveBitSet, iHoldUp)
//					AND NOT IS_BIT_SET(HoldUpServerData.iHoldUpDoneBitSet, iHoldUp)
//						HoldUpServerData.iHoldUp_StartTimerDelay[iHoldUp] = -1	//HoldUpServerData.iHoldUp_StartTimer[INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp)] -= GET_HOLD_UP_STORE_REFRESH_TIME()
//						SET_BIT(HoldUpServerData.iHoldUpMakeActiveBitSet, iHoldUp)
//						NET_PRINT("   --->   HOLD UP - SET HoldUpServerData.iHoldUpMakeActiveBitSet - HOLD UP = ")NET_PRINT_INT(iHoldUp)NET_NL()
//					ENDIF*/
//					
//					//Check if player has Done a Hold Up and Reset that Hold Up Timer
//					/*IF NOT IS_BIT_SET(HoldUpServerData.iHoldUpDoneBitSet, iHoldUp)
//					AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iHoldUpDoneBitSet, iHoldUp)
//					AND NOT IS_BIT_SET(HoldUpServerData.iHoldUpMakeActiveBitSet, iHoldUp)
//						SET_HOLD_UP_TIMER(HoldUpServerData, INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp))
//						SET_BIT(HoldUpServerData.iHoldUpDoneBitSet, iHoldUp)
//						NET_PRINT("   --->   HOLD UP - SET HoldUpServerData.iHoldUpDoneBitSet - HOLD UP = ")NET_PRINT_INT(iHoldUp)NET_NL()
//					ENDIF*/
//					
//					//Check if player is in Hold Up interior
//					//IF GET_NETWORK_TIME() < (GET_HOLD_UP_TIMERS(INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp)) + GET_HOLD_UP_STORE_REFRESH_TIME())
//					//AND GET_NETWORK_TIME() > (GET_HOLD_UP_TIMERS(INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp)) + (GET_HOLD_UP_STORE_REFRESH_TIME()/2))
//					/*IF GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(HoldUpServerData.iHoldUp_StartTimer[iHoldUp]) < GET_HOLD_UP_STORE_REFRESH_TIME()
//					AND GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(HoldUpServerData.iHoldUp_StartTimer[iHoldUp]) > (GET_HOLD_UP_STORE_REFRESH_TIME()/2)
//						VECTOR vMin = <<0, 0, 0>>
//						VECTOR vMax = <<0, 0, 0>>
//						VECTOR vBlip = <<0, 0, 0>>
//						FLOAT fWidth = 0
//						HOLD_UP_DETAILS(INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp), vMin, vMax, fWidth, vBlip)
//						IF IS_ENTITY_IN_ANGLED_AREA(PlayerPedId, vMin, vMax, fWidth)
//							SET_HOLD_UP_TIMER(HoldUpServerData, INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp))
//							NET_PRINT("   --->   HOLD UP - SERVER_MAINTAIN_HOLD_UP_PLAYERS - SET HOLD UP TIMER - PLAYER IN INTERIOR - HOLD UP = ")NET_PRINT_INT(iHoldUp)NET_NL()
//						ENDIF
//					ENDIF*/
//				ENDIF
//			ENDIF
//		ENDREPEAT*/
//		
//	ENDIF
//ENDPROC

//PURPOSE:
PROC SERVER_CHECK_HOLD_UP_TIMER(HOLD_UP_SERVER_STRUCT &HoldUpServerData, INT iHoldUp)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INT i
		
		INT iDoorHash[2]
		MODEL_NAMES DoorModel[2]
		VECTOR vDoorCoords[2]
		HOLD_UP_DOOR_DETAILS(iHoldUp, iDoorHash, DoorModel, vDoorCoords)
		BOOL bChangeDoorState = FALSE
		
		CLEAR_BIT(HoldUpServerData.iHoldUpDoneBitSet, iHoldUp)
		CLEAR_BIT(HoldUpServerData.iHoldUpMakeActiveBitSet, iHoldUp)
		
		#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
			NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - STORE NUMBER = ")NET_PRINT_INT(iHoldUp)NET_NL()
			NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - NUM PLAYERS IN STORE = ")NET_PRINT_INT(GlobalServerBD_HoldUp.HoldUpServerData.iNumPlayersInThisStore[iHoldUp])NET_NL()
			NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - NUM PLAYERS IN STORE STRICT = ")NET_PRINT_INT(GlobalServerBD_HoldUp.HoldUpServerData.iNumPlayersInThisStoreStrict[iHoldUp])NET_NL()
		ENDIF
		#ENDIF
		
		//Check if player is inside a Hold Up Store and reset the timer if necessary
		//IF HoldUpServerData.iNumPlayersInThisStore[iHoldUp] > 0
		IF GlobalServerBD_HoldUp.HoldUpServerData.iNumPlayersInThisStore[iHoldUp] > 0
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(HoldUpServerData.iHoldUp_StartTimer[iHoldUp]) < HoldUpServerData.iHoldUp_StartTimerDelay[iHoldUp]
			AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(HoldUpServerData.iHoldUp_StartTimer[iHoldUp]) > (HoldUpServerData.iHoldUp_StartTimerDelay[iHoldUp]/2)
				SET_HOLD_UP_TIMER(HoldUpServerData, INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp))
				NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - SET HOLD UP TIMER - PLAYER IN INTERIOR - HOLD UP = ")NET_PRINT_INT(iHoldUp)NET_NL()
			ENDIF
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
				NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME = ")NET_PRINT_INT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(HoldUpServerData.iHoldUp_StartTimer[iHoldUp]))NET_NL()
				NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME = ")NET_PRINT_INT(GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(HoldUpServerData.iHoldUp_StartTimer[iHoldUp]))NET_NL()
			ENDIF
			#ENDIF
		ENDIF
		
		//Check if Done Timer has run out
		IF HAS_NET_TIMER_STARTED(HoldUpServerData.iHoldUp_StartTimer[iHoldUp])
		AND HAS_NET_TIMER_EXPIRED(HoldUpServerData.iHoldUp_StartTimer[iHoldUp], HoldUpServerData.iHoldUp_StartTimerDelay[iHoldUp]) //GET_NETWORK_TIME() > (GET_HOLD_UP_TIMERS(INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp)) + GET_HOLD_UP_STORE_REFRESH_TIME())
		AND NOT IS_BIT_SET(HoldUpServerData.iHoldUpFakeDoneBitSet, iHoldUp)
		
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
				NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - TIMER EXPIRED ") NET_NL()
			ENDIF
			#ENDIF
			
			//Set Done Bit and Add Doors
			IF NOT IS_BIT_SET(HoldUpServerData.iHoldUpTimerDoneBitSet, iHoldUp)
				
				#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
					NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - iHoldUpTimerDoneBitSet NOT SET ") NET_NL()
				ENDIF
				#ENDIF
				
				//Remove Doors
				BOOL bResetBit = TRUE
				REPEAT 2 i
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash[i])
						IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash[i])
							
							DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash[i], DOORSTATE_UNLOCKED, TRUE, TRUE)
							
							bChangeDoorState = FALSE
							IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash[i]) = DOORSTATE_INVALID
								IF DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash[i]) = DOORSTATE_UNLOCKED
									bChangeDoorState = TRUE
								ENDIF
							ELSE
								IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash[i]) = DOORSTATE_UNLOCKED
									bChangeDoorState = TRUE
								ENDIF
							ENDIF
							
							IF bChangeDoorState = TRUE
							//IF DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash[i]) = DOORSTATE_UNLOCKED
								REMOVE_DOOR_FROM_SYSTEM(iDoorHash[i])
								PRINTLN("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - REMOVE DOOR ", i, " - HOLD UP = ", iHoldUp)
							ELSE
								PRINTLN("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - TRYING TO UNLOCK DOOR ", i, " - HOLD UP = ", iHoldUp)
								bResetBit = FALSE
							ENDIF
						ELSE
							NETWORK_REQUEST_CONTROL_OF_DOOR(iDoorHash[i])
							PRINTLN("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - A - REQUESTING CONTROL OF DOOR ", i, " - HOLD UP = ", iHoldUp)
							bResetBit = FALSE
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF bResetBit = TRUE
					
					IF HoldUpServerData.scenarioBlocking[iHoldUp] != NULL
						REMOVE_SCENARIO_BLOCKING_AREA(HoldUpServerData.scenarioBlocking[iHoldUp])//, TRUE)
						HoldUpServerData.scenarioBlocking[iHoldUp] = NULL
						NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - REMOVE_SCENARIO_BLOCKING_AREA - HOLD UP = ")NET_PRINT_INT(iHoldUp)NET_NL()
					ENDIF
					SET_BIT(HoldUpServerData.iHoldUpTimerDoneBitSet, iHoldUp)
					NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - SET HOLD UP TIMER DONE - HOLD UP = ")NET_PRINT_INT(iHoldUp)NET_NL()
				ENDIF
			ENDIF
			
		ELSE
			
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
				NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - TIMER STILL GOING ") NET_NL()
			ENDIF
			#ENDIF
			
			IF IS_BIT_SET(HoldUpServerData.iHoldUpTimerDoneBitSet, iHoldUp)
			OR IS_BIT_SET(HoldUpServerData.iHoldUpFakeDoneBitSet, iHoldUp)	
				//Add Doors
				ADD_DOOR_TO_SYSTEM(iDoorHash[0], DoorModel[0], vDoorCoords[0], FALSE, TRUE, FALSE)
				PRINTLN("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - ADD DOOR ", 0, " - HOLD UP = ", iHoldUp)
				IF iDoorHash[1] != -1
					ADD_DOOR_TO_SYSTEM(iDoorHash[1], DoorModel[1], vDoorCoords[1], FALSE, TRUE, FALSE)
					PRINTLN("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - ADD DOOR ", 1, " - HOLD UP = ", iHoldUp)
				ENDIF
				
				CLEAR_BIT(HoldUpServerData.iHoldUpTimerDoneBitSet, iHoldUp)
				NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - SET HOLD UP TIMER NOT DONE - HOLD UP = ")NET_PRINT_INT(iHoldUp)NET_NL()
			#IF IS_DEBUG_BUILD
			ELSE
			IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
				NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - iHoldUpTimerDoneBitSet NOT SET ") NET_NL()
			ENDIF
			#ENDIF
			ENDIF
			
			// Add scenario blocking area around the shop - fix for 1293509
			IF HoldUpServerData.scenarioBlocking[iHoldUp] = NULL
				VECTOR vCoord = GET_HOLD_UP_BLIP_LOCATION( INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp) )
				
				IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vCoord - <<15, 15, 15>>, vCoord + <<15, 15, 15>>) // Fix for B*3201829
					HoldUpServerData.scenarioBlocking[iHoldUp] = ADD_SCENARIO_BLOCKING_AREA(vCoord - <<15, 15, 15>>, vCoord + <<15, 15, 15>>)//, TRUE)
					PRINTLN("   --->   HOLD UP - SERVER - ADD_SCENARIO_BLOCKING_AREA - AT ", vCoord, " HOLD UP = ", iHoldUp)
				ELSE
					PRINTLN("   --->   HOLD UP - SERVER - Unable to add scenario blocking area.")
				ENDIF
			ENDIF
			
			//Lock and Unlock Doors based on Players being Inside
			REPEAT 2 i
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash[i])
					//IF HoldUpServerData.iNumPlayersInThisStoreStrict[iHoldUp] > 0
					IF GlobalServerBD_HoldUp.HoldUpServerData.iNumPlayersInThisStoreStrict[iHoldUp] > 0
						
						bChangeDoorState = FALSE
						
						IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash[i]) = DOORSTATE_INVALID
							IF DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash[i]) != DOORSTATE_UNLOCKED
								bChangeDoorState = TRUE
							ENDIF
						ELSE
							IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash[i]) != DOORSTATE_UNLOCKED
								bChangeDoorState = TRUE
							ENDIF
						ENDIF
						
						IF bChangeDoorState = TRUE
							IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash[i])
								DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash[i], DOORSTATE_UNLOCKED, TRUE, TRUE)
								PRINTLN("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - UNLOCK DOOR ", i, " - HOLD UP = ", iHoldUp)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_DOOR(iDoorHash[i])
								PRINTLN("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - B - REQUESTING CONTROL OF DOOR ", i, " - HOLD UP = ", iHoldUp)
							ENDIF
								
						#IF IS_DEBUG_BUILD
						ELSE
						IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
							NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - DOOR ALREADY UNLOCKED  - STATE = ") NET_PRINT_INT(ENUM_TO_INT(DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash[i]))) NET_NL()
						ENDIF
						#ENDIF
						ENDIF
					ELSE
						bChangeDoorState = FALSE
						
						IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash[i]) = DOORSTATE_INVALID
							IF DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash[i]) != DOORSTATE_LOCKED
								bChangeDoorState = TRUE
							ENDIF
						ELSE
							IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash[i]) != DOORSTATE_LOCKED
								bChangeDoorState = TRUE
							ENDIF
						ENDIF
						
						IF NOT bChangeDoorState = TRUE
							IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash[i])
								IF NOT MPGlobalsAmbience.bKeepShopDoorsOpen
									DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash[i], DOORSTATE_LOCKED, TRUE, TRUE)
									PRINTLN("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - LOCK DOOR ", i, " - HOLD UP = ", iHoldUp)
								ENDIF
							ELSE
								NETWORK_REQUEST_CONTROL_OF_DOOR(iDoorHash[i])
								PRINTLN("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - C - REQUESTING CONTROL OF DOOR ", i, " - HOLD UP = ", iHoldUp)
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
						IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
							NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - DOOR ALREADY LOCKED  - STATE = ") NET_PRINT_INT(ENUM_TO_INT(DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash[i]))) NET_NL()
						ENDIF
						#ENDIF
						ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
				IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
					NET_PRINT("   --->   HOLD UP - SERVER_CHECK_HOLD_UP_TIMER - DOOR NOT REGISTERED ") NET_NL()
				ENDIF
				#ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC	

//PURPOSE: Loops through all the Hold Ups
PROC LOOP_THROUGH_HOLD_UPS(HOLD_UP_SERVER_STRUCT &HoldUpServerData, HOLD_UP_STRUCT &HoldUpData)
	//INT iHoldUp
	//REPEAT MAX_NUM_HOLDUPS iHoldUp
		
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("LOOP_THROUGH_HOLD_UPS")
	#ENDIF
	#ENDIF	
	
		/*SERVER_MAINTAIN_HOLD_UP_PLAYERS(HoldUpServerData, HoldUpData.iHoldUpLoop)
		#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_HOLD_UP_PLAYERS")
			#ENDIF
		#ENDIF	*/
		
		SERVER_CHECK_HOLD_UP_TIMER(HoldUpServerData, HoldUpData.iHoldUpLoop)
		#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("SERVER_CHECK_HOLD_UP_TIMER")
			#ENDIF
		#ENDIF
		
		/*CONTROL_HOLD_UP_DONE_BITSET(HoldUpServerData, HoldUpData.iHoldUpLoop)
		#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_HOLD_UP_DONE_BITSET")
			#ENDIF
		#ENDIF
		
		CONTROL_HOLD_UP_MADE_ACTIVE_BITSET(HoldUpServerData, HoldUpData.iHoldUpLoop)
		#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_HOLD_UP_MADE_ACTIVE_BITSET")
			#ENDIF
		#ENDIF*/
		
		CONTROL_HOLD_UP_BLIPS(HoldUpServerData, HoldUpData)
		#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_HOLD_UP_BLIPS")
			#ENDIF
		#ENDIF
		
		CONTROL_HOLD_UP_SCENARIO_BLOCKING(HoldUpServerData, HoldUpData)
		#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_HOLD_UP_SCENARIO_BLOCKING")
			#ENDIF
		#ENDIF
		
		CONTROL_HOLD_UP_BLIPS_LONG_RANGE_FOR_SMASH_AND_GRAB(HoldUpServerData, HoldUpData)
		#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_HOLD_UP_BLIPS_LONG_RANGE_FOR_SMASH_AND_GRAB")
			#ENDIF
		#ENDIF
		
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		AND NOT NETWORK_IS_ACTIVITY_SESSION()
		CONTROL_IN_HOLD_UP_STORE(HoldUpServerData, HoldUpData)
		#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_IN_HOLD_UP_STORE")
			#ENDIF
		#ENDIF
		ENDIF
		
		IF HoldUpData.iHoldUpLoop >= (MAX_NUM_HOLDUPS-1)
			HoldUpData.iHoldUpLoop = 0
		ELSE
			HoldUpData.iHoldUpLoop++
		ENDIF
		
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF	
		
	//ENDREPEAT
ENDPROC

/// PURPOSE: Finds a new, unique instance id for the Hold Up Tutorial Mission.
/*FUNC INT GET_NEW_UNIQUE_HOLD_UP_TUT_MISSION_INSTANCE_ID()	// - Just Launch through HOld Up Tutorial and use Tutorial Instance + MAX_NUM_HOLDUPS
	
	INT iHighestInstanceId = MAX_NUM_HOLDUPS
	INT iInstanceCounter
	INT iPlayer
	BOOL bCheckNextInstance = FALSE
	
	FOR iInstanceCounter = iHighestInstanceId TO 31
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
			IF GET_MP_MISSION_PLAYER_IS_ON(playerId) = eAM_HOLD_UP
				IF NOT NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), playerId)
					IF GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(playerId) = iInstanceCounter
						NET_PRINT("   --->   HOLD UP - A PLAYER IS ALREADY ON INSTANCE")NET_PRINT_INT(iHighestInstanceId)NET_NL()
						iHighestInstanceId++
						bCheckNextInstance = TRUE
						iPlayer = 99 //Exit Player Repeat
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bCheckNextInstance = FALSE
			NET_PRINT("   --->   HOLD UP - INSTANCE FOUND = ")NET_PRINT_INT(iHighestInstanceId)NET_NL()
			RETURN iHighestInstanceId
		ELSE
			bCheckNextInstance = FALSE
		ENDIF
	ENDFOR
		
	NET_PRINT("   --->   HOLD UP - DEFAULT INSTANCE -1 USED")NET_NL()
	RETURN -1
ENDFUNC*/

TWEAK_INT HOLD_UP_SUCCESSION_TIMER_RESET 720000	//1440000	//(1000*60*12)	- Quarter of a Game Day
//PURPOSE: Controls how resetting the Hold Up Succession Counter
PROC CONTROL_HOLD_UP_SUCCESSION_COUNTER()
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpsDoneInCloseSuccession != 0
		IF HAS_NET_TIMER_EXPIRED(MPGlobals.iResetHoldUpSuccessionTimer, HOLD_UP_SUCCESSION_TIMER_RESET)	//MPGlobals.iResetHoldUpSuccessionTimer < GET_THE_NETWORK_TIMER()
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpsDoneInCloseSuccession = 0
			NET_PRINT("   --->   HOLD UP - HOLD UP SUCCESION COUNTER RESET")NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: If the player spawns in a Hold Up then sets it as having been done. This check is only run once.
PROC SPAWNED_IN_HOLD_UP_CHECK(HOLD_UP_STRUCT &HoldUpData)
	IF NOT IS_BIT_SET(HoldUpData.iHoldUpBitset, biHoldUpSpawnInsideCheck)
		Crim_HOLD_UP_POINT eNearestHoldUp
		GET_NEAREST_HOLD_UP_AREA_LOOP(eNearestHoldUp)
		IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_HOLD_UP_BLIP_LOCATION(eNearestHoldUp)) < 25.0
		AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_HOLD_UP", ENUM_TO_INT(eNearestHoldUp))
		AND NOT IS_PLAYER_IN_CORONA()
		AND NOT IS_TRANSITION_SESSION_LAUNCHING()
		AND NOT IS_TRANSITION_SESSION_RESTARTING()
		AND NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
			BROADCAST_SET_HOLD_UP_DONE(ENUM_TO_INT(eNearestHoldUp), TRUE, GET_HOLD_UP_HOST())
			HoldUpData.iSpawnedInStore = ENUM_TO_INT(eNearestHoldUp)
			NET_PRINT("   --->   HOLD UP - SPAWNED_IN_HOLD_UP_CHECK - BROADCAST THIS STORE AS DONE - HOLD UP ") NET_PRINT_INT(ENUM_TO_INT(eNearestHoldUp)) NET_NL()
		ENDIF
		SET_BIT(HoldUpData.iHoldUpBitset, biHoldUpSpawnInsideCheck)
		NET_PRINT("   --->   HOLD UP - SPAWNED_IN_HOLD_UP_CHECK - biHoldUpSpawnInsideCheck SET")NET_NL()
		
	//Reset Disable this Store
	ELSE
		IF HoldUpData.iSpawnedInStore != -1
			IF HAS_NET_TIMER_EXPIRED(HoldUpData.iSPawnedInStoreDisableTimer, 10000)
				HoldUpData.iSpawnedInStore = -1
				NET_PRINT("   --->   HOLD UP - SPAWNED_IN_HOLD_UP_CHECK - RESET - SpawnedInStore = -1")NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Player can go and Hold Up shops
PROC MAINTAIN_HOLD_UPS(HOLD_UP_SERVER_STRUCT &HoldUpServerData, HOLD_UP_STRUCT &HoldUpData)
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("MAINTAIN_HOLD_UPS")
	#ENDIF
	#ENDIF		
	
	#IF FEATURE_FREEMODE_ARCADE
	IF IS_FREEMODE_ARCADE()
		EXIT
	ENDIF
	#ENDIF
	
	//Check if Hold Ups have been unlocked by Flow
	IF NOT IS_BIT_SET(HoldUpData.iHoldUpBitset, biHoldUpsActivatedInFlow)
		//IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HOLD_UPS)
			SET_BIT(HoldUpData.iHoldUpBitset, biHoldUpsActivatedInFlow)
			NET_PRINT("   --->   HOLD UP - HOLD UPS ACTIVATED BY FLOW - FM")NET_NL()
		//ENDIF
	ENDIF
	
	// Drop cash in holdups
	IF IS_BIT_SET(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpCashDropTransactionRequested)
		IF IS_CASH_TRANSACTION_COMPLETE(MPGlobalsAmbience.iHoldupDropCashTransactionIndex)
			PRINTLN("[CASH] - IS_CASH_TRANSACTION_COMPLETE(TRUE), transaction is complete!")
			IF GET_CASH_TRANSACTION_STATUS(MPGlobalsAmbience.iHoldupDropCashTransactionIndex) = CASH_TRANSACTION_STATUS_SUCCESS
				PRINTLN("[CASH] - GET_CASH_TRANSACTION_STATUS(SUCCESS), transaction ", MPGlobalsAmbience.iHoldupDropCashTransactionIndex, " was successul!")
				CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_MONEY_VARIABLE, MPGlobalsAmbience.vHoldupDropCashCoords, MPGlobalsAmbience.iHoldupCashPlacementFlags, MPGlobalsAmbience.iHoldupCashToDrop, GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG), DEFAULT, FALSE)
				GIVE_LOCAL_PLAYER_CASH_END_OF_MISSION(-MPGlobalsAmbience.iHoldupCashToDrop)
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(MPGlobalsAmbience.iHoldupDropCashTransactionIndex))
				NETWORK_SPENT_HOLDUPS(MPGlobalsAmbience.iHoldupCashToDrop)
				//TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_CASH_DROP_HOLDUP, iPlayerCash)
				
				NET_PRINT_TIME() NET_PRINT("     ---------->     HOLD UP - PLAYER NOT OKAY - iPlayerCash REMOVED $") NET_PRINT_INT(MPGlobalsAmbience.iHoldupCashToDrop) NET_NL()
				g_money_dropped_amount_pending = 0
				CLEAR_LAST_JOB_DATA()										
				DELETE_CASH_TRANSACTION(MPGlobalsAmbience.iHoldupDropCashTransactionIndex)
				CLEAR_BIT(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpCashDropTransactionRequested)
			ELSE
				// CASH TRANSACTION FAILED!
				PRINTLN("[CASH] - GET_CASH_TRANSACTION_STATUS(FAILED) - SERVICE_SPEND_CASH_DROP_HOLDUP, transaction ", MPGlobalsAmbience.iHoldupDropCashTransactionIndex, " has failed!")
				
				// DO TRANSACTION FAIL LOGIC HERE
				
				g_money_dropped_amount_pending = 0
				CLEAR_LAST_JOB_DATA()
				DELETE_CASH_TRANSACTION(MPGlobalsAmbience.iHoldupDropCashTransactionIndex)
				CLEAR_BIT(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpCashDropTransactionRequested)
			ENDIF
		ELSE
			PRINTLN("[CASH] - IS_CASH_TRANSACTION_COMPLETE(FALSE), transaction ", MPGlobalsAmbience.iHoldupDropCashTransactionIndex, " is pending...")
		ENDIF	
	ENDIF
	
	SPAWNED_IN_HOLD_UP_CHECK(HoldUpData)
		
	CONTROL_HOLD_UP_SUCCESSION_COUNTER()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("CONTROL_HOLD_UP_SUCCESSION_COUNTER")
		#ENDIF
	#ENDIF	
	
	
	LOOP_THROUGH_HOLD_UPS(HoldUpServerData, HoldUpData)
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("LOOP_THROUGH_HOLD_UPS")
		#ENDIF
	#ENDIF	
		
	//Run through the Hold Ups
	IF IS_BIT_SET(HoldUpData.iHoldUpBitset, biHoldUpsActivatedInFlow)
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())

		//PRINTLN("   --->   [holdupchecks] - ", HoldUpData.Crim_HoldUpState)
	
		SWITCH HoldUpData.Crim_HoldUpState

			//Blipping nearest hold up area
			CASE HOLD_UP_WAIT
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					BOOL bHoldupThreadNotRunning
					bHoldupThreadNotRunning = (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_HOLD_UP")) = 0)
										
					IF NOT IS_MP_MISSION_DISABLED(eAM_HOLD_UP)
					AND bHoldupThreadNotRunning = TRUE	//GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_HOLD_UP")) = 0
					AND NOT IS_MP_MISSION_LAUNCH_IN_PROGRESS()
					AND HOLD_UP_OKAY_FREEMODE_CHECKS()
						
						IF GET_NEAREST_HOLD_UP_AREA(HoldUpData.iHoldUpCounter, HoldUpData.fShortestDistance, HoldUpData.WorkingNearestHoldUpPoint)
							
							HoldUpData.NearestHoldUpPoint = HoldUpData.WorkingNearestHoldUpPoint
							
							IF IS_BIT_SET(HoldUpServerData.iHoldUpTimerDoneBitSet, ENUM_TO_INT(HoldUpData.NearestHoldUpPoint))
							AND NOT IS_BIT_SET(HoldUpServerData.iHoldUpDoneBitSet, ENUM_TO_INT(HoldUpData.NearestHoldUpPoint))	//HoldUpData.iHoldUpCounter
							AND NOT IS_BIT_SET(HoldUpServerData.iHoldUpFakeDoneBitSet, ENUM_TO_INT(HoldUpData.NearestHoldUpPoint))
							AND HoldUpData.iSpawnedInStore != ENUM_TO_INT(HoldUpData.NearestHoldUpPoint)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_HOLD_UP_BLIP_LOCATION(HoldUpData.NearestHoldUpPoint)) < 100.0
									HoldUpData.Crim_HoldUpState = HOLD_UP_RUNNING
									HOLD_UP_DETAILS(HoldUpData.NearestHoldUpPoint, HoldUpData.interiorID[ENUM_TO_INT(HoldUpData.NearestHoldUpPoint)], HoldUpData.iRoomHashMain, HoldUpData.iRoomHashMain2, HoldUpData.iRoomHashBack, HoldUpData.vNearestHoldUpArea, HoldUpData.vDoorCoord)
									NET_PRINT("   --->   [holdupchecks] - MOVE TO HOLD_UP_RUNNING - LOCATION = ") NET_PRINT_INT(ENUM_TO_INT(HoldUpData.NearestHoldUpPoint)) NET_NL()	
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(HoldUpServerData.iHoldUpTimerDoneBitSet, ENUM_TO_INT(HoldUpData.NearestHoldUpPoint))
									NET_PRINT("   --->   [holdupchecks] - NOT IS_BIT_SET(HoldUpServerData.iHoldUpTimerDoneBitSet, ENUM_TO_INT(HoldUpData.NearestHoldUpPoint))") NET_NL()	
								ENDIF
								
								IF IS_BIT_SET(HoldUpServerData.iHoldUpDoneBitSet, ENUM_TO_INT(HoldUpData.NearestHoldUpPoint))
									NET_PRINT("   --->   [holdupchecks] - IS_BIT_SET(HoldUpServerData.iHoldUpDoneBitSet, ENUM_TO_INT(HoldUpData.NearestHoldUpPoint))") NET_NL()	
								ENDIF
								
								IF IS_BIT_SET(HoldUpServerData.iHoldUpFakeDoneBitSet, ENUM_TO_INT(HoldUpData.NearestHoldUpPoint))
									NET_PRINT("   --->   [holdupchecks] - IS_BIT_SET(HoldUpServerData.iHoldUpFakeDoneBitSet, ENUM_TO_INT(HoldUpData.NearestHoldUpPoint))") NET_NL()	
								ENDIF
								
								IF HoldUpData.iSpawnedInStore = ENUM_TO_INT(HoldUpData.NearestHoldUpPoint)
									NET_PRINT("   --->   [holdupchecks] - HoldUpData.iSpawnedInStore = ENUM_TO_INT(HoldUpData.NearestHoldUpPoint)") NET_NL()	
								ENDIF
							ENDIF
						//ELSE
							//NET_PRINT("   --->   [holdupchecks] - Unable to find nearest hold up area") NET_NL()	
						ENDIF
					ELSE
						HoldUpData.iHoldUpCounter = 0
						HoldUpData.fShortestDistance = 9999.9
//						IF IS_MP_MISSION_DISABLED(eAM_HOLD_UP)
//							NET_PRINT("   --->   [holdupchecks] - IS_MP_MISSION_DISABLED(eAM_HOLD_UP)") NET_NL()
//						ENDIF
//						
//						IF bHoldupThreadNotRunning = FALSE
//							NET_PRINT("   --->   [holdupchecks] - bHoldupThreadNotRunning = FALSE") NET_NL()
//						ENDIF
//						
//						IF IS_MP_MISSION_LAUNCH_IN_PROGRESS()
//							NET_PRINT("   --->   [holdupchecks] - IS_MP_MISSION_LAUNCH_IN_PROGRESS()") NET_NL()
//						ENDIF
//						
//						IF NOT HOLD_UP_OKAY_FREEMODE_CHECKS()
//							NET_PRINT("   --->   [holdupchecks] - NOT HOLD_UP_OKAY_FREEMODE_CHECKS()") NET_NL()
//						ENDIF
					ENDIF
					
					IF bHoldupThreadNotRunning = TRUE
					AND HoldUpData.Crim_HoldUpState != HOLD_UP_RUNNING
						IF IS_BIT_SET(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList)	//(HoldUpData.iHoldUpBitset, biHoldUpActivityDisplay)
							REMOVE_ACTIVITY_FROM_DISPLAY_LIST(HoldUpData.vDoorCoord,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_HOLD_UPS))
							CLEAR_BIT(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList)
							NET_PRINT_TIME() NET_PRINT("     ----->   AM_HOLD_UP - C - REMOVED FROM ACTIVITY DISPLAY  <-----     ") NET_NL()
						ENDIF
					ENDIF
				ENDIF
			BREAK

			//Waiting for player to enter the area
			CASE HOLD_UP_RUNNING

				HoldUpData.fHoldUpAreaDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), HoldUpData.vNearestHoldUpArea)
								
				//Check if player is near the Hold Up area
				IF HoldUpData.fHoldUpAreaDistance < 80.0
				AND HOLD_UP_OKAY_FREEMODE_CHECKS()
					//Giving player help text
					IF NOT IS_BIT_SET(HoldUpData.iHoldUpBitset, biHelpTextDone)
					AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = HoldUpData.interiorID[ENUM_TO_INT(HoldUpData.NearestHoldUpPoint)]
					AND HoldUpData.fHoldUpAreaDistance < 50.0
					AND NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
					AND NOT IS_PLAYER_IN_ANY_SHOP()
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							INT iStat
							iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
							IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HOLD_UPS)
								IF NOT IS_BIT_SET(iStat, TUTBS_HOLD_UP_HELP_1)
									PRINT_HELP("FHU_HELP1") //~s~Stores that can be held up are marked by the ~BLIP_CRIM_HOLDUPS~ icon.~n~Enter a store and aim a weapon at the owner to begin a hold up.
									SET_BIT(iStat, TUTBS_HOLD_UP_HELP_1)
									SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
									NET_PRINT_TIME() NET_PRINT("     ----->   HOLD UP - TUTBS_HOLD_UP_HELP_1 SET  <-----     ") NET_NL()
									SET_BIT(HoldUpData.iHoldUpBitset, biHelpTextDone)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList) //IS_BIT_SET(HoldUpData.iHoldUpBitset, biHoldUpActivityDisplay)
						IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HOLD_UPS)
							ADD_NAMED_ACTIVITY_TO_DISPLAY_LIST(HoldUpData.vDoorCoord,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_HOLD_UPS),"")
							SET_BIT(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList)
							NET_PRINT_TIME() NET_PRINT("     ----->   AM_HOLD_UP - A - ADDED TO ACTIVITY DISPLAY  <-----     ") NET_NL()
						ENDIF
					ENDIF
					
					REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_STAFF))
					REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG))
					REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_TILL))
					REQUEST_ANIM_DICT("mp_am_hold_up")
					REQUEST_ANIM_DICT("mp_missheist_countrybank@cower")
					
					//Waiting for the player to enter the holdup area
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), HoldUpData.vNearestHoldUpArea, <<25, 25, 25>>)//, FALSE, TRUE, TM_ON_FOOT)
						HoldUpData.HoldupToLaunch.mdID.idMission 	= eAM_HOLD_UP  
						HoldUpData.Crim_HoldUpState 				= HOLD_UP_LAUNCHING
						//Get Instance
						IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
							HoldUpData.HoldupToLaunch.iInstanceId = ENUM_TO_INT(HoldUpData.NearestHoldUpPoint) //GET_NEW_UNIQUE_MP_MISSION_INSTANCE_ID(HoldUpData.HoldupToLaunch.mdID.idMission)
							NET_PRINT("   --->   HOLD UP - MOVE TO HOLD_UP_LAUNCHING - A - LOCATION = ") NET_PRINT_INT(ENUM_TO_INT(HoldUpData.NearestHoldUpPoint)) NET_NL()
						ENDIF
					ENDIF
						
				//Cleanup and go back to wait if player is too far away
				ELIF HoldUpData.fHoldUpAreaDistance > 110.0
					CLEANUP_HOLD_UPS(HoldUpData, TRUE)
					NET_PRINT("   --->   HOLD UP - MOVE TO CLEANUP - PLAYER > 210m AWAY - LOCATION = ") NET_PRINT_INT(ENUM_TO_INT(HoldUpData.NearestHoldUpPoint)) NET_NL()
				ENDIF
			BREAK

			CASE HOLD_UP_LAUNCHING				
				REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_STAFF))
				REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG))
				REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_TILL))
				REQUEST_ANIM_DICT("mp_am_hold_up")
				REQUEST_ANIM_DICT("mp_missheist_countrybank@cower")
				
				//Launch as Ambient Mission
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_HOLD_UP")) = 0
				    IF NOT IS_BIT_SET(HoldUpData.iHoldUpBitset, biHoldUpLaunched)
					AND ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH(1, 0, 2, FALSE, TRUE)
						BROADCAST_JOIN_THIS_AMBIENT_MP_MISSION_EVENT(HoldUpData.HoldupToLaunch, SPECIFIC_PLAYER(PLAYER_ID()))
						NET_PRINT("   --->   HOLD UP - BROADCAST_JOIN_THIS_AMBIENT_MP_MISSION_EVENT - LOCATION = ") NET_PRINT_INT(ENUM_TO_INT(HoldUpData.NearestHoldUpPoint)) NET_NL()
						
						SET_BIT(HoldUpData.iHoldUpBitset, biHoldUpLaunched)
						NET_PRINT("   --->   HOLD UP - NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS - LOCATION = ") NET_PRINT_INT(ENUM_TO_INT(HoldUpData.NearestHoldUpPoint)) NET_NL()
					ENDIF
				ELSE
					CLEANUP_HOLD_UPS(HoldUpData, FALSE)//, FALSE)
					NET_PRINT("   --->   HOLD UP - GET_NUMBER_OF_THREADS_RUNNING_THIS_SCRIPT - IN PROGRESS - LOCATION = ") NET_PRINT_INT(ENUM_TO_INT(HoldUpData.NearestHoldUpPoint)) NET_NL()
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("Run through the Hold Ups")
		#ENDIF
	#ENDIF	
	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF	
	
ENDPROC
