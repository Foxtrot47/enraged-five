USING "rage_builtins.sch"
USING "globals.sch"

USING "net_mission_info.sch"
USING "FM_Unlocks_Header_Public.sch"

USING "net_prints.sch"



// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_Control_Info.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Stores any Mission Controller Mission Info functions.
//		NOTES			:	This header is used mainly to decide whether to call a net_mission_info.sch function (used for non-cloud
//								loaded missions) and FM_mission_info.sch functions (used for FM cloud-loaded activities).
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Mission Controller Mission Info Access functions
//		(Distributes requests to functions in either net_mission_info.sch or FM_mission_info.sch)
// ===========================================================================================================

// PURPOSE:	Get the minimum players required for the specified mission without reference to their teams.
//
// RETURN PARAMS:			paramMissionIDData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT						The minimum number of players required for this mission.
//
// NOTES:	Calls an appropriate function depending on whether the mission is cloud-loaded or not.
FUNC INT Get_Minimum_Players_Required_For_MP_Mission_Request(MP_MISSION_ID_DATA &paramMissionIDData)

	IF (Does_MP_Mission_Variation_Get_Data_From_The_Cloud(paramMissionIDData.idMission, paramMissionIDData.idVariation))
		RETURN (Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity(paramMissionIDData))
	ENDIF
	
	RETURN (Get_Minimum_Number_Of_Players_Required_For_MP_Mission(paramMissionIDData.idMission))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the maximum players required for the specified mission without reference to their teams.
//
// RETURN PARAMS:			paramMissionIDData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT						The maximum number of players required for this mission.
//
// NOTES:	Calls an appropriate function depending on whether the mission is cloud-loaded or not.
FUNC INT Get_Maximum_Players_Required_For_MP_Mission_Request(MP_MISSION_ID_DATA &paramMissionIDData)

	// Limit the Max Players on a playlist to the mission with the lowest Max Players
	IF (IS_PLAYER_ON_A_PLAYLIST2(PLAYER_ID()))
		RETURN (g_sCurrentPlayListDetails.iMaxNumber)	// KGM 25/6/13: Directly accessing global from GET_CURRENT_PLAYLIST_MAX_PLAYERS() because of cyclic headers
	ENDIF

	IF (Does_MP_Mission_Variation_Get_Data_From_The_Cloud(paramMissionIDData.idMission, paramMissionIDData.idVariation))
		RETURN (Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity(paramMissionIDData))
	ENDIF
	
	RETURN (GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(paramMissionIDData.idMission))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the maximum players for the specified team for the specified mission.
//
// INPUT PARAMS:			paramTeamID				The Team
// RETURN PARAMS:			paramMissionIDData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT						The maximum number of team members for this mission.
FUNC INT Get_Maximum_Team_Members_For_MP_Mission_Request(MP_MISSION_ID_DATA &paramMissionIDData, INT paramTeamID)
	
	// KGM 26/8/12: Added for using Mission Controller in Freemode - this data will be stored differently when sorting out the Mission Data
	IF (paramTeamID = TEAM_INVALID)
		RETURN (Get_Maximum_Players_Required_For_MP_Mission_Request(paramMissionIdData))
	ENDIF

	// Cops?
	IF (Is_Team_A_Cop_Team(paramTeamID))
		RETURN (GET_MP_MISSION_MAX_NUMBER_OF_COPS(paramMissionIDData.idMission))
	ENDIF
	
	// Crooks?
	IF (Is_Team_A_Crook_Gang(paramTeamID))
		RETURN (GET_MP_MISSION_MAX_NUMBER_OF_CRIM_GANG(paramMissionIDData.idMission, paramTeamID, paramMissionIDData.idVariation))
	ENDIF

//	// Unknown Team Type?
//	#IF IS_DEBUG_BUILD
//		// KGM TEMP: Directly accessing the team data struct, but this should call the team info header when the cyclic header issue is resolved
//		IF NOT (TeamData[paramTeamID].TeamType = TEAM_TYPE_NON_ACTIVE)
//			NET_PRINT("...KGM MP: Get_Maximum_Team_Members_For_MP_Mission_Request() - Unknown Team Type: ") NET_PRINT_INT(paramTeamID) NET_NL()
//			SCRIPT_ASSERT("Get_Maximum_Team_Members_For_MP_Mission_Request(): Unknown Team Type. Tell Keith")
//		ENDIF
//	#ENDIF
	
	RETURN 0
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the rank required to unlock a specific mission
//
// RETURN PARAMS:			paramMissionIDData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT						The rank required for this mission.
//
// NOTES:	Calls an appropriate function depending on whether the mission is cloud-loaded or not.
FUNC INT Get_Rank_Required_For_MP_Mission_Request(MP_MISSION_ID_DATA &paramMissionIDData)

	IF (Does_MP_Mission_Variation_Get_Data_From_The_Cloud(paramMissionIDData.idMission, paramMissionIDData.idVariation))
		RETURN (Get_Rank_For_FM_Cloud_Loaded_Activity(paramMissionIDData))
	ENDIF
	
	RETURN (GET_RANK_FOR_MP_MISSION(paramMissionIDData.idMission))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the contact for a specific mission
//
// RETURN PARAMS:			paramMissionIDData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			enumCharacterList		The Contact for this mission.
//
// NOTES:	Calls an appropriate function depending on whether the mission is cloud-loaded or not.
FUNC enumCharacterList Get_Contact_For_MP_Mission_Request(MP_MISSION_ID_DATA &paramMissionIDData)

	IF (Does_MP_Mission_Variation_Get_Data_From_The_Cloud(paramMissionIDData.idMission, paramMissionIDData.idVariation))
		RETURN (Get_Contact_For_FM_Cloud_Loaded_Activity(paramMissionIDData))
	ENDIF
	
	// For now, ignore CnC
	RETURN (NO_CHARACTER)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Gang Rank required to unlock a specific mission
//
// RETURN PARAMS:			paramMissionIDData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT						The Gang Rank required for this mission.
//
// NOTES:	Calls an appropriate function depending on whether the mission is cloud-loaded or not.
FUNC INT Get_Gang_Rank_Required_For_MP_Mission_Request(MP_MISSION_ID_DATA &paramMissionIDData)

	IF (Does_MP_Mission_Variation_Get_Data_From_The_Cloud(paramMissionIDData.idMission, paramMissionIDData.idVariation))
		RETURN (Get_Gang_Rank_For_FM_Cloud_Loaded_Activity(paramMissionIDData))
	ENDIF
	
	// For now, assume all CnC missions are cloud loaded, so return a high rank if it's not classed as cloud-loaded
	SCRIPT_ASSERT("Get_Gang_Rank_Required_For_MP_Mission_Request() - This dropped through into the non-cloud-loaded return value. Tell Keith.")
	RETURN (8999)
	
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the minimum Gang hate stat value required to unlock a specific mission
//
// RETURN PARAMS:			paramMissionIDData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:			INT						The minimum Gang Hate stat value required for this mission.
//
// NOTES:	Calls an appropriate function depending on whether the mission is cloud-loaded or not.
FUNC INT Get_Minimum_Gang_Hate_Required_For_MP_Mission_Request(MP_MISSION_ID_DATA &paramMissionIDData)

	IF (Does_MP_Mission_Variation_Get_Data_From_The_Cloud(paramMissionIDData.idMission, paramMissionIDData.idVariation))
		RETURN (Get_Minimum_Gang_Hate_For_FM_Cloud_Loaded_Activity(paramMissionIDData))
	ENDIF
	
	// For now, assume all CnC missions are cloud loaded, so return no restriction if it's not classed as cloud-loaded
	SCRIPT_ASSERT("Get_Minimum_Gang_Hate_Required_For_MP_Mission_Request() - This dropped through into the non-cloud-loaded return value. Tell Keith.")
	RETURN (0)
	
ENDFUNC
// ===========================================================================================================
//       Mission Info Access functions that may be distributed to gather info from different places
//		(These generally contain functionality that is similar but different in CnC and FM)
// ===========================================================================================================

// PURPOSE:	Check if the Mission is locked (probably because the player is too low a rank)
//
// INPUT PARAMS:		paramMissionIdData		The MissionIdData to check
//						paramWaitForSecondary	[DEFAULT = FALSE] TRUE if this mission should only unlock with a secondary unlock of the mission type, FALSE if it unlocks with the primary unlock
// RETURN VALUE:		BOOL					TRUE if the missionID is locked, FALSE if unlocked
//
// NOTES:	KGM 30/9/14: Changed the first parameter to the MissionIdStruct instead of the MISSION_ID so that I can do special unlock checks for Versus missions unlocked after Heists.
FUNC BOOL Check_If_Mission_Locked(MP_MISSION_ID_DATA paramMissionIdData, BOOL paramWaitForSecondary = FALSE)

	MP_MISSION	theMissionID = paramMissionIdData.idMission

	IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
		
		// KGM 30/9/14 [BUG 2045872]: Check if specific Versus Missions have been unlocked after specific Heist Strands are complete
		// KGM 4/10/14 [BUG 2067305]: Passing Heists strands no longer 'unlock' the missions, just the 'Invites' to the missions. The missions unlock using normal rules.
//		#IF IS_DEBUG_BUILD
//		#IF FEATURE_HEIST_PLANNING
//		IF (Is_This_A_Heist_Reward_Versus_Mission(paramMissionIdData))
//			#IF IS_DEBUG_BUILD
//				IF (GET_COMMANDLINE_PARAM_EXISTS("sc_showAllHeistVersusMissions"))
//					// ...not locked (because of command line)
//					RETURN FALSE
//				ENDIF
//			#ENDIF
//			
//			IF (Is_This_Heist_Reward_Versus_Mission_Unlocked(paramMissionIdData))
//				// ...not locked
//				RETURN FALSE
//			ENDIF
//			
//			// ...locked
//			RETURN TRUE
//		ENDIF
//		#ENDIF	// FEATURE_HEIST_PLANNING
//		#ENDIF	// IS_DEBUG_BUILD
		
		INT missionType = Convert_MissionID_To_FM_Mission_Type(theMissionID)
		IF (missionType = -1)
			// Unknown MissionType, so return as Unlocked
			RETURN FALSE
		ENDIF
		
		IF (IS_FM_TYPE_UNLOCKED(missionType, paramWaitForSecondary))
			// ...unlocked
			RETURN FALSE
		ENDIF
		
		// Still Locked
		RETURN TRUE
	ENDIF
	
	// For non-FM missions, just return as Unlocked for now
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if still playing through the tutorial sequence
//
// RETURN VALUE:		BOOL					TRUE if still in tutorials stage, FALSE if in main game
FUNC BOOL Check_If_Doing_Tutorials()

	IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
		RETURN (IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL())
	ENDIF
	
	// For non-FM missions, just return as 'not doing tutorial' for now
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if player can afford to play the mission
//
// INPUT PARAMS:		paramMissionID			The Mission ID
//						paramMissionSubtype		The Mission Subtype
// RETURN VALUE:		BOOL					TRUE if the player can afford it, FALSE if can't afford it
FUNC BOOL Check_If_Afford_Mission(MP_MISSION paramMissionID, INT paramMissionSubtype)

	IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
		INT theFMMissionType = Convert_MissionID_To_FM_Mission_Type(paramMissionID)
		RETURN (CAN_I_AFFORD_TO_PLAY_THIS_ACTIVITY(theFMMissionType, paramMissionSubtype))
	ENDIF
	
	// For non-FM missions, just return as 'can afford' for now
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Mission Blip should Flash when unlocked
//
// INPUT PARAMS:		paramMissionID			The MissionID to check
// RETURN VALUE:		BOOL					TRUE if the missionID blip should flash when unlocked, FALSE if no flash
FUNC BOOL Check_If_Mission_Blip_Should_Flash_When_Unlocked(MP_MISSION paramMissionID)

	IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
		INT missionType = Convert_MissionID_To_FM_Mission_Type(paramMissionID)
		IF (missionType = -1)
			// Unknown MissionType, so return as No Flash
			RETURN FALSE
		ENDIF
		
		RETURN (SHOULD_ACTIVITY_BLIPS_FLASH_WHEN_UNLOCKED(missionType))
	ENDIF
	
	// For non-FM missions, just return as No Flash for now
	RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      Mission Controller Mission Info Analysis functions
// ===========================================================================================================

// PURPOSE:	Check if the teams bitfield meets the minimum criteria of teams required for the specified mission
//
// INPUT PARAMS:		paramMissionID					The Mission ID
//						paramVariation					The Mission Variation - initially added to differentiate between the two team configs for the AttackBoat variations
//						paramBitfieldTeamsAvailable		The available Teams (as a bitfield based on the team IDs)
// RETURN VALUE:		BOOL							TRUE if the available teams are sufficient for the mission, otherwise FALSE
//
// NOTES:	KGM MISSION DATA RE-STRUCTURE - CHECKED 13/7/12: This function is still required by mission controller - leave it as-is.
FUNC BOOL Does_Teams_Bitfield_Match_Teams_Required_For_Mission(MP_MISSION paramMissionID, INT paramVariation, INT paramBitfieldTeamsAvailable)

	// Analyse the bitfield
	BOOL isCopTeamAvailableForMission = Are_Cops_In_Team_Bitfield(paramBitfieldTeamsAvailable)
	INT numCrookTeamsAvailableForMission = Get_Number_Of_Crook_Teams_In_Team_Bitfield(paramBitfieldTeamsAvailable)
	
	// Get the Teams Configuration for this mission
	g_eMPTeamConfig teamConfig = Get_MP_Mission_Variation_Team_Configuration(paramMissionID, paramVariation)
	
	// Check if the available teams meet the team requirements
	SWITCH (teamConfig)
		// Requires Cops
		CASE MP_TEAMS_COPS
			IF (isCopTeamAvailableForMission)
				RETURN TRUE
			ENDIF
			BREAK
	
		// Requires Cops and at least one available Gang
		CASE MP_TEAMS_COPS_1GANG
		CASE MP_TEAMS_COPS_1GANG_MIN
		CASE MP_TEAMS_COPS_1OR2GANGS
			IF (isCopTeamAvailableForMission)
			AND (numCrookTeamsAvailableForMission >= 1)
				RETURN TRUE
			ENDIF
			BREAK
			
		// Requires at least one available gang
		CASE MP_TEAMS_1GANG
		CASE MP_TEAMS_1GANG_MIN
			IF (numCrookTeamsAvailableForMission >= 1)
				RETURN TRUE
			ENDIF
			BREAK
			
		// Requires at least two available gangs
		CASE MP_TEAMS_2GANGS
			IF (numCrookTeamsAvailableForMission >= 2)
				RETURN TRUE
			ENDIF
			BREAK
			
		// Requires one Team, which can be the Cops
		CASE MP_TEAMS_1TEAM
			IF (isCopTeamAvailableForMission)
			OR (numCrookTeamsAvailableForMission >= 1)
				RETURN TRUE
			ENDIF
			BREAK
			
		// Requires at least two available teams, one of which can optionally be the Cops
		CASE MP_TEAMS_2TEAMS_MIN
			// ...Variant: cops + one gang
			IF (isCopTeamAvailableForMission)
			AND (numCrookTeamsAvailableForMission >= 1)
				RETURN TRUE
			ENDIF
			
			// ...Variant: two gangs
			IF (numCrookTeamsAvailableForMission >= 2)
				RETURN TRUE
			ENDIF
			BREAK
			
		// Illegal value
		CASE NO_MP_TEAM_CONFIG
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP: Does_Teams_Bitfield_Match_Teams_Required_For_Mission(). Team Configuration ID was NO_MP_TEAM_CONFIG.") NET_NL()
				SCRIPT_ASSERT("Does_Teams_Bitfield_Match_Teams_Required_For_Mission() - Illegal Team Configuration ID. Tell Keith.")
			#ENDIF
			BREAK
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP: Does_Teams_Bitfield_Match_Teams_Required_For_Mission(). Unknown Team Configuration ID needs added to SWITCH.") NET_NL()
				SCRIPT_ASSERT("Does_Teams_Bitfield_Match_Teams_Required_For_Mission() - Unknown Team COnfiguration ID. Tell Keith.")
			#ENDIF
			BREAK
	ENDSWITCH
	
	// Allow this if the debug flag is set to allow launching of missions with one team (or in code development mode)
	#IF IS_DEBUG_BUILD
		IF (GlobalServerBD.g_launchMissionsWithOneTeam)
			RETURN TRUE
		ENDIF
		
		IF (NETWORK_IS_IN_CODE_DEVELOPMENT_MODE())
			RETURN TRUE
		ENDIF
	#ENDIF
	
	// The available teams do not match the requirements
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the additional team can join this mission by comparing the existing teams bitfield with the team configuration for the mission.
//
// INPUT PARAMS:		paramMissionID					The Mission ID
//						paramVariation					The Mission Variation - initially added to differentiate between the two team configs for the AttackBoat variations
//						paramBitfieldTeamsOnMission		The Teams already on this mission (as a bitfield based on the team IDs)
//						paramAdditionalTeam				The Team trying to join this mission
// RETURN VALUE:		BOOL							TRUE if the additional team is ok for this mission, otherwise FALSE
//
// NOTES:	KGM MISSION DATA RE-STRUCTURE - CHECKED 13/7/12: This function is still required by mission controller - leave it as-is.
FUNC BOOL Check_If_Team_Can_Join_Mission(MP_MISSION paramMissionID, INT paramVariation, INT paramBitfieldTeamsOnMission, INT paramAdditionalTeam)

	// Is the team valid?
	IF (paramAdditionalTeam = TEAM_INVALID)
		// Team is not valid so return TRUE if the mission isn't team-based or FALSE if it is team-based
		IF (Check_If_Players_Are_In_Teams_For_Mission(paramMissionID))
			// ...players should be in teams for mission, so TEAM_INVALID is not allowed
			RETURN FALSE
		ENDIF
		
		// Players don't need to be in teams for mission, so TEAM_INVALID is allowed
		RETURN TRUE
	ENDIF

	// Check if the team is already on the mission
	IF (IS_BIT_SET(paramBitfieldTeamsOnMission, paramAdditionalTeam))
		RETURN TRUE
	ENDIF
	
//	// Check if the additional Team is the Cops
//	BOOL additionalTeamIsCops = FALSE
//	IF (paramAdditionalTeam = TEAM_COP)
//		additionalTeamIsCops = TRUE
//	ENDIF

	// Analyse the bitfield
	INT numCrookTeamsOnMission = Get_Number_Of_Crook_Teams_In_Team_Bitfield(paramBitfieldTeamsOnMission)
	
	// Get the Team vs Team Requirements for this mission
	g_eMPTeamConfig teamConfig = Get_MP_Mission_Variation_Team_Configuration(paramMissionID, paramVariation)
	
//	// If additional Team is the cops, check if they can join
//	// NOTE: We've already checked to ensure cops aren't already on the mission
//	IF (additionalTeamIsCops)
//		SWITCH (teamConfig)
//			// Cops can join
//			CASE MP_TEAMS_2TEAMS_MIN
//			CASE MP_TEAMS_COPS
//			CASE MP_TEAMS_COPS_1GANG
//			CASE MP_TEAMS_COPS_1GANG_MIN
//			CASE MP_TEAMS_COPS_1OR2GANGS
//				RETURN TRUE
//				
//			// Cops can join if no Crooks on mission
//			CASE MP_TEAMS_1TEAM
//				IF (numCrookTeamsOnMission = 0)
//					// Ok to join
//					RETURN TRUE
//				ENDIF
//				// Already full
//				RETURN FALSE
//				
//			// Cops can't join
//			CASE MP_TEAMS_1GANG
//			CASE MP_TEAMS_2GANGS
//			CASE MP_TEAMS_1GANG_MIN
//				RETURN FALSE
//				
//			// Illegal value
//			CASE NO_MP_TEAM_CONFIG
//				#IF IS_DEBUG_BUILD
//					NET_PRINT("...KGM MP: Check_If_Team_Can_Join_Mission(). Team Configuration ID was NO_MP_TEAM_CONFIG when checking for Cops.") NET_NL()
//					SCRIPT_ASSERT("Check_If_Team_Can_Join_Mission() - Illegal Team Configuration ID. See Console Log. Tell Keith.")
//				#ENDIF
//				RETURN FALSE
//				
//			DEFAULT
//				#IF IS_DEBUG_BUILD
//					NET_PRINT("...KGM MP: Check_If_Team_Can_Join_Mission(). Unknown Team Configuration ID needs added to SWITCH when checking for Cops.") NET_NL()
//					SCRIPT_ASSERT("Check_If_Team_Can_Join_Mission() - Unknown Team COnfiguration ID. See Console Log. Tell Keith.")
//				#ENDIF
//				RETURN FALSE
//		ENDSWITCH
//	ENDIF
	
//	// Sanity Check - Cops already tested
//	#IF IS_DEBUG_BUILD
//		IF (additionalTeamIsCops)
//			SCRIPT_ASSERT("Check_If_Team_Can_Join_Mission() - Cops already tested - shouldn't reach here. Tell Keith.")
//			
//			RETURN FALSE
//		ENDIF
//	#ENDIF
	
	// Check if another crook team can join
	SWITCH (teamConfig)
		// Any number of Crook Teams can join
		CASE MP_TEAMS_2TEAMS_MIN
		CASE MP_TEAMS_COPS_1GANG_MIN
		CASE MP_TEAMS_1GANG_MIN
			RETURN TRUE
		
		// Only One Crook Team can join
		CASE MP_TEAMS_COPS_1GANG
		CASE MP_TEAMS_1GANG
			IF (numCrookTeamsOnMission = 0)
				// Ok to join
				RETURN TRUE
			ENDIF
			// Already full
			RETURN FALSE
		
		// Up To Two Crook Team can join
		CASE MP_TEAMS_COPS_1OR2GANGS
		CASE MP_TEAMS_2GANGS
			IF (numCrookTeamsOnMission < 2)
				// Ok to join
				RETURN TRUE
			ENDIF
			// Already full
			RETURN FALSE
			
		// Crook Teams can't join
		CASE MP_TEAMS_COPS
			RETURN FALSE
			
		// Illegal value
		CASE NO_MP_TEAM_CONFIG
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP: Check_If_Team_Can_Join_Mission(). Team Configuration ID was NO_MP_TEAM_CONFIG when checking for Crooks.") NET_NL()
				SCRIPT_ASSERT("Check_If_Team_Can_Join_Mission() - Illegal Team Configuration ID. See Console Log. Tell Keith.")
			#ENDIF
			RETURN FALSE
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP: Check_If_Team_Can_Join_Mission(). Unknown Team Configuration ID needs added to SWITCH when checking for Crooks.") NET_NL()
				SCRIPT_ASSERT("Check_If_Team_Can_Join_Mission() - Unknown Team COnfiguration ID. See Console Log. Tell Keith.")
			#ENDIF
			RETURN FALSE
	ENDSWITCH
	
	// Shouldn't reach here, so return FALSE
	RETURN FALSE

ENDFUNC




