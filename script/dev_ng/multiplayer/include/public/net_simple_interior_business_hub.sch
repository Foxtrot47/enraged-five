//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_simple_interior_business_hub.sch																	//
// Description: This is the implementation of the mega business hub as a simple interior. As such this does not expose 	//
//				any public functions. All functions to manipulate simple interiors are in net_simple_interior			//
// Written by:  Online Technical Design: Aidan Temple																	//
// Date:  		26/01/2018																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_simple_interior_cs_header_include.sch"
USING "net_realty_business_hub.sch"
USING "business_battles_dropoffs.sch"
USING "Nightclub_entry_cutscenes.sch"

CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_BOSS 						0 	// We entered the interior as the boss (also owner).
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_GOON 						1 	// We entered the interior as a goon.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_GOON_MC 					2 	// We entered the interior as a prospect.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_ASSOCIATE 					3 	// We entered the interior as an associate.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_MEMBER_OF_OTHER_GANG		4 	// We entered as a member of someone elses gang.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_NON_GANG_MEMBER			5 	// We entered the interior as an invited non-gang member.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER_LEFT_GAME			6 	// The owner has left the game.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_MISSION_EXIT_MEMBER		7 	// The owner has started a mission and we are part of their gang.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_MISSION_EXIT_NON_MEMBER	8 	// The owner has started a mission and we are NOT part of their gang.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_RE_ENTERED_ON_MISSION		9 	// The owner has started a mission and we are NOT part of their gang.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_EXIT_JOINED_ANOTHER_GANG	10	// The owner has joined another gang.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_CROSS_SESSION_JOIN			11	// The owner cannot be determined as we are joining via a cross session invite.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_BECAME_BEAST				12  // The player became the Beast whilst inside the interior.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICKED_BY_BOUNCER			13  // The player was kicked out by the bouncer.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICK_ARMED_PLAYER			14  // Kick the armed player.
CONST_INT SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER 						30 	// We entered the interior as the owner (but not boss).

CONST_INT BS_SIMPLE_INTERIOR_BUSINESS_HUB_WANTED_LEVEL_CLEARED				0

//////////////////////////////////////////////////
//	Cutscene CONSTS
//

//entryAnim.iSceneBS
CONST_INT CUTSCENE_BS_NIGHTCLUB_EXIT_SETUP_PED_SPEECH			0
CONST_INT CUTSCENE_BS_SETUP_FIRST_VEHICLE_SCENE					1 //first frame stuff for 1st scene.
CONST_INT CUTSCENE_BS_VEH_ABOVE_LENGTH_LIMIT					2
CONST_INT CUTSCENE_BS_FROZEN_CLONE								3
CONST_INT CUTSCENE_BS_TASKED_MOTORBIKE							4
CONST_INT CUTSCENE_BS_TALL_VEH_SCENE							5

//Float array
CONST_INT CUTSCENE_FLOAT_ARRAY_VEH_SPEED			0
CONST_INT CUTSCENE_FLOAT_MOVE_VEH_DISTANCE			1
CONST_INT CUTSCENE_FLOAT_ARRAY_VEH_Z_COORD			2

//Vector array
CONST_INT CUTSCENE_VEC_ARRAY_SAVED_VEH_COORD		0
CONST_INT CUTSCENE_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX	1

//Blip data
CONST_INT MAX_BUSINESS_HUB_BLIPS	2

//////////////////////////////////////////////////
//	Functions
//

FUNC BOOL DOES_BUSINESS_HUB_USE_EXTERIOR_SCRIPT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

PROC GET_NIGHTCLUB_TYPE_AND_POSITION(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtType, VECTOR &vPosition, FLOAT &fHeading)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS	
			vPosition 	= <<-1604.6643, -3012.5828, -79.9999>>
			fHeading 	= 0.0
			txtType 	= "ba_dlc_int_01_ba"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_BUSINESS_HUB_TYPE_AND_POSITION(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtType, VECTOR &vPosition, FLOAT &fHeading, BOOL bUseSecondaryInteriorDetails)
	
	IF bUseSecondaryInteriorDetails
		GET_NIGHTCLUB_TYPE_AND_POSITION(eSimpleInteriorID, txtType, vPosition, fHeading)
	ELSE
		SWITCH eSimpleInteriorID
			CASE SIMPLE_INTERIOR_HUB_LA_MESA
			CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
			CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
			CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
			CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
			CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
			CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS		
				vPosition 	= <<-1505.7830, -3012.5867, -79.9999>>
				fHeading 	= 0.0
				txtType 	= "ba_dlc_int_02_ba"
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BUSINESS_HUB_LOAD_SECONDARY_INTERIOR(SIMPLE_INTERIORS eSimpleInteriorID)
	
	IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = HASH("freemode")
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX propertyOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
	
	IF IS_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER_SET()
	AND GET_SMPL_INT_ID_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER() = eSimpleInteriorID
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
		PRINTLN("SHOULD_BUSINESS_HUB_LOAD_SECONDARY_INTERIOR - Setting using VEHICLE_ENTRY_AS_PASSENGER_SET - To ", GET_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER())
		IF GET_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			RETURN TRUE
		ENDIF
	
	ELIF IS_PLAYER_SWITCHING_BETWEEN_INTERIORS_WITH_SAME_EXT_SCTIPT()
	OR IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_MOVE_FROM_TRUCK_TO_HUB_TRUCK_FLOOR)
		
		PRINTLN("SHOULD_BUSINESS_HUB_LOAD_SECONDARY_INTERIOR - Switching interiors? ", IS_PLAYER_SWITCHING_BETWEEN_INTERIORS_WITH_SAME_EXT_SCTIPT(), " BS4_SIMPLE_INTERIOR_MOVE_FROM_TRUCK_TO_HUB_TRUCK_FLOOR = ", IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_MOVE_FROM_TRUCK_TO_HUB_TRUCK_FLOOR), " Local floor: ", GET_INTERIOR_FLOOR_INDEX())
		
		IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			RETURN TRUE
		ENDIF
		
	ELIF HAS_PLAYER_ACCEPTED_SIMPLE_INTERIOR_INVITE()
		
		PRINTLN("SHOULD_BUSINESS_HUB_LOAD_SECONDARY_INTERIOR - HAS_PLAYER_ACCEPTED_SIMPLE_INTERIOR_INVITE")
		RETURN TRUE
		
	ELSE
		IF (propertyOwner != INVALID_PLAYER_INDEX() AND GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(propertyOwner) = ciBUSINESS_HUB_FLOOR_NIGHTCLUB)
		OR GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
		OR GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
			#IF IS_DEBUG_BUILD
				IF propertyOwner = INVALID_PLAYER_INDEX()
					PRINTLN("SHOULD_BUSINESS_HUB_LOAD_SECONDARY_INTERIOR ELSE 1 - Owner = INVALID. Entrance used = ", GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED())
				ELSE
					PRINTLN("SHOULD_BUSINESS_HUB_LOAD_SECONDARY_INTERIOR ELSE 1 - Owner: ", NATIVE_TO_INT(propertyOwner), " Entrance used = ", GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(), " Owner floor: ", GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(propertyOwner))
				ENDIF
			#ENDIF
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD
				IF propertyOwner = INVALID_PLAYER_INDEX()
					PRINTLN("SHOULD_BUSINESS_HUB_LOAD_SECONDARY_INTERIOR ELSE 2 - Owner = INVALID. Entrance used = ", GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED())
				ELSE
					PRINTLN("SHOULD_BUSINESS_HUB_LOAD_SECONDARY_INTERIOR ELSE 2 - Owner: ", NATIVE_TO_INT(propertyOwner), " Entrance used = ", GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(), " Owner floor: ", GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(propertyOwner))
				ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INIT_WAREHOUSE_ENTRY_DATA(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS& details)
	unused_parameter(eSimpleInteriorID)
	
	details.strInteriorChildScript		= "AM_MP_BUSINESS_HUB"	
	details.bInteriorIsSeamless 		= FALSE
	details.entryAnim.audioSceneEntry	= ""
	details.entryAnim.audioSceneExit	= ""
	
//	details.entryAnim.strOnEnterSoundNames[0] = "PUSH"
//	details.entryAnim.strOnEnterSoundNames[1] = "LIMIT"
//	details.entryAnim.fOnEnterSoundPhases[0] = 0.271
//	details.entryAnim.fOnEnterSoundPhases[1] = 0.411
//	details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
//	
//	SWITCH eSimpleInteriorID
//		CASE SIMPLE_INTERIOR_HUB_LA_MESA
//			vPosition		= 
//			vRotation		= 
//			
//			details.entryAnim.dictionary 				= "anim@apt_trans@hinge_l"
//			details.entryAnim.clip 						= "ext_player"
//			details.entryAnim.cameraPos 				= <<50.8801, 6336.8755, 32.1390>>
//			details.entryAnim.cameraRot 				= <<-0.7937, 0.0000, -63.3392>>
//			details.entryAnim.cameraFov 				= 50.0
//			details.entryAnim.syncScenePos 				= <<53.457, 6337.362, 30.641>>
//			details.entryAnim.syncSceneRot 				= <<0.0, 0.0, -150.840>>
//			details.entryAnim.startingPhase 			= 0.200
//			details.entryAnim.endingPhase 				= 0.800
//			details.entryAnim.establishingCameraPos 	= <<745.7391, -1353.7943, 33.8279>>
//			details.entryAnim.establishingCameraRot 	= <<-8.6843, 0.0000, -25.4839>>
//			details.entryAnim.fEstablishingCameraFov 	= 50.0
//		BREAK
//		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
//			vPosition		= <<514.5276, -916.9147, 34.4075>>
//			vRotation		= <<-10.1182, -0.0000, 98.9302>>
//		BREAK
//		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
//			vPosition		= <<-86.6488, -1288.2610, 55.8484>>
//			vRotation		= <<-20.8853, 0.0000, 69.3024>>
//		BREAK
//		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
//			vPosition		= <<23.6744, 239.4627, 118.8592>>
//			vRotation		= <<-12.4045, 0.0000, 129.2529>>
//		BREAK
//		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
//			vPosition		= <<856.8986, -2083.7397, 45.9869>>
//			vRotation		= <<-19.5589, -0.0000, -140.0329>>
//		BREAK
//		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
//			//vPosition		= <<745.7391, -1353.7943, 33.8279>>
//			//vRotation		= <<-8.6843, 0.0000, -25.4839>>
//		BREAK
//		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
//			//vPosition		= <<514.5276, -916.9147, 34.4075>>
//			//vRotation		= <<-10.1182, -0.0000, 98.9302>>
//		BREAK
//		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
//			//vPosition		= <<-86.6488, -1288.2610, 55.8484>>
//			//vRotation		= <<-20.8853, 0.0000, 69.3024>>
//		BREAK
//		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
//			//vPosition		= <<23.6744, 239.4627, 118.8592>>
//			//vRotation		= <<-12.4045, 0.0000, 129.2529>>
//		BREAK
//		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
//			//vPosition		= <<856.8986, -2083.7397, 45.9869>>
//			//vRotation		= <<-19.5589, -0.0000, -140.0329>>
//		BREAK
//	ENDSWITCH
//	
//	fFieldOfView	= 50.0
//	fCameraShake	= 0.5
ENDPROC

PROC INIT_NIGHTCLUB_ENTRY_DATA(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS& details)
	unused_parameter(eSimpleInteriorID)
	details.strInteriorChildScript		= "AM_MP_NIGHTCLUB"
	details.bInteriorIsSeamless 		= FALSE
	details.bInteriorUsesAngledArea		= TRUE
	
//	details.entryAnim.audioSceneEntry	= ""
//	details.entryAnim.audioSceneExit	= ""
//	
//	details.entryAnim.strOnEnterSoundNames[0] = "PUSH"
//	details.entryAnim.strOnEnterSoundNames[1] = "LIMIT"
//	details.entryAnim.fOnEnterSoundPhases[0] = 0.271
//	details.entryAnim.fOnEnterSoundPhases[1] = 0.411
//	details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
ENDPROC

PROC GET_BUSINESS_HUB_ESTABLISHING_SHOT_DATA(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vPosition, VECTOR &vRotation, FLOAT &fFieldOfView, FLOAT &fCameraShake, BOOL bUseSecondaryInteriorDetails)
	IF bUseSecondaryInteriorDetails
		SWITCH eSimpleInteriorID
			CASE SIMPLE_INTERIOR_HUB_LA_MESA
				vPosition		= <<774.9752, -1346.5548, 28.0999>>
				vRotation		= <<5.7589, 0.0026, 43.0470>>
				fFieldOfView	= 47.2371
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
				vPosition		= <<353.0908, -944.1270, 36.5719>>
				vRotation		= <<-9.5129, 0.0550, 169.6899>>
				fFieldOfView	= 26.5215
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
				vPosition		= <<-83.4536, -1300.0598, 34.3550>>
				vRotation		= <<6.6798, 0.1368, 52.8183>>
				fFieldOfView	= 39.7724
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
				vPosition		= <<25.2870, 227.5200, 109.8637>>
				vRotation		= <<7.0436, 0.1001, 104.4277>>
				fFieldOfView	= 42.5363
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
				vPosition		= <<859.2952, -2077.3840, 32.0000>>
				vRotation		= <<6.9830, -0.0846, -141.1513>>
				fFieldOfView	= 41.6961
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
				vPosition		= <<-710.5236, -2466.3430, 16.6375>>
				vRotation		= <<0.8154, -0.0557, -73.0783>>
				fFieldOfView	= 32.7225
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
				vPosition		= <<165.8182, -3111.9749, 9.2922>>
				vRotation		= <<-1.4158, 0.0000, -154.7472>>
				fFieldOfView	= 23.9466
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
				vPosition		= <<405.0143, 277.6437, 106.8855>>
				vRotation		= <<10.0144, 0.1097, 130.9049>>
				fFieldOfView	= 41.9232
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
				vPosition		= <<-1283.1177, -626.3682, 28.5328>>
				vRotation		= <<9.9341, -0.1005, -175.6886>>
				fFieldOfView	= 52.5911
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
				vPosition		= <<-1165.6947, -1132.5479, 7.0100>>
				vRotation		= <<2.4561, -0.1432, 159.1615>>
				fFieldOfView	= 44.3756
				fCameraShake	= 0.2
			BREAK
		ENDSWITCH
	ELSE
		SWITCH eSimpleInteriorID
			CASE SIMPLE_INTERIOR_HUB_LA_MESA
				vPosition		= <<711.5486, -1311.7397, 26.9087>>
				vRotation		= <<9.7556, 0.0692, -49.8918>>
				fFieldOfView	= 32.3120
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
				vPosition		= <<320.3405, -1002.1382, 29.9967>>
				vRotation		= <<5.1997, -0.1430, -57.2915>>
				fFieldOfView	= 39.1327
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
				vPosition		= <<-152.7097, -1316.4662, 32.9863>>
				vRotation		= <<4.8618, -0.0778, 27.9171>>
				fFieldOfView	= 43.7476
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
				vPosition		= <<-44.4688, 204.7237, 108.9804>>
				vRotation		= <<-0.4062, -0.0758, -58.6869>>
				fFieldOfView	= 36.5531
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
				vPosition		= <<903.2930, -2071.3438, 33.1299>>
				vRotation		= <<4.4652, -0.0810, -162.9494>>
				fFieldOfView	= 35.2646
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
				vPosition		= <<-690.7659, -2388.2324, 15.4628>>
				vRotation		= <<6.3247, -0.1369, -101.1767>>
				fFieldOfView	= 50.0000
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
				vPosition		= <<198.7077, -3086.3662, 7.1945>>
				vRotation		= <<15.5745, -0.1055, -148.6160>>
				fFieldOfView	= 44.4095
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
				vPosition		= <<417.9737, 268.4984, 105.3028>>
				vRotation		= <<8.9579, 0.1211, 125.3569>>
				fFieldOfView	= 40.5806
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
				vPosition		= <<-1217.1691, -697.5862, 23.4599>>
				vRotation		= <<10.9504, 0.0707, 80.6866>>
				fFieldOfView	= 38.7236
				fCameraShake	= 0.2
			BREAK
			CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
				vPosition		= <<-1158.5580, -1184.7996, 6.4328>>
				vRotation		= <<6.5882, 0.0642, 60.2060>>
				fFieldOfView	= 43.1519
				fCameraShake	= 0.2
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC SIMPLE_INTERIOR_DETAILS_BS GET_BUSINESS_HUB_PROPERTIES_BS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_DETAILS_BS structReturn
	
	//BS1
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_SUPPORTS_SWITCHING_BETWEEN_MULTIPLE_INTERIORS)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_SUPPORTS_SWITCHING_BETWEEN_MULTIPLE_FLOORS)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_PROPERTY_USES_CUSTOM_ENTRANCE_MENU)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_SUPPORTS_INDIVIDUAL_ENTRANCE_DISABLING)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_PROPERTY_USES_EXTERIOR_CAM_DURING_ENTRY)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_INSTANCE_DETERMINED_BY_OWNERSHIP)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_DO_NOT_LAUNCH_INT_SCRIPTS_WITH_EXT_ENTRY_MENU)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_PROPERTY_USES_INTERIOR_EXIT_SCENE)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_PROPERTY_SUPPORTS_VEHICLE_ENTRY)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_RUN_SCENARIO_POP_DENSITY_MODIFICATION)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_SHOW_NEARBY_AMB_PEDS_IN_ENTRY_CUTSCENE)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_DONT_CLEAR_AREA_OF_PEDS_ON_EXIT_WARP)
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_USES_SERVICES_PIM_SUB_MENU)
	
	//BS2
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_REQUIRES_TRADE_IN_MANAGEMENT)
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_PARENT_INT_SCRIPT_DOES_LOAD_SCENE)
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_USES_EXTENDED_TO_IN_INT_WAIT_FOR_DEPENDENCIES)
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_RUNS_IN_ACTIVITY_SESSION)
	
	RETURN structReturn
ENDFUNC
	
PROC GET_BUSINESS_HUB_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS &details, BOOL bUseSecondaryInteriorDetails, BOOL bSMPLIntPreCache)
	
	UNUSED_PARAMETER(bSMPLIntPreCache)
	
	IF bUseSecondaryInteriorDetails		
		INIT_NIGHTCLUB_ENTRY_DATA(eSimpleInteriorID, details)
		
		GET_EXIT_DATA_FOR_NIGHTCLUB(ciNIGHTCLUB_EXIT_FRONT_DOOR, details.exitData.fLocateWidths[0], details.exitData.fHeadings[0],  details.exitData.vLocateCoords1[0], details.exitData.vLocateCoords2[0])
		GET_EXIT_DATA_FOR_NIGHTCLUB(ciNIGHTCLUB_EXIT_STORAGE_ELEVATOR, details.exitData.fLocateWidths[1], details.exitData.fHeadings[1],  details.exitData.vLocateCoords1[1], details.exitData.vLocateCoords2[1])
		GET_EXIT_DATA_FOR_NIGHTCLUB(ciNIGHTCLUB_EXIT_GARAGE_ELEVATOR, details.exitData.fLocateWidths[2], details.exitData.fHeadings[2],  details.exitData.vLocateCoords1[2], details.exitData.vLocateCoords2[2])
		GET_EXIT_DATA_FOR_NIGHTCLUB(ciNIGHTCLUB_EXIT_VEHICLE_ELEVATOR, details.exitData.fLocateWidths[3], details.exitData.fHeadings[3], details.exitData.vLocateCoords1[3], details.exitData.vLocateCoords2[3])
		GET_EXIT_DATA_FOR_NIGHTCLUB(ciNIGHTCLUB_EXIT_GARAGE, details.exitData.fLocateWidths[4], details.exitData.fHeadings[4], details.exitData.vLocateCoords1[4], details.exitData.vLocateCoords2[4])
		
		GET_BUSINESS_HUB_ESTABLISHING_SHOT_DATA(eSimpleInteriorID, details.entryAnim.establishingCameraPos, details.entryAnim.establishingCameraRot, details.entryAnim.fEstablishingCameraFov, details.entryAnim.fEstablishingCameraShake, bUseSecondaryInteriorDetails)
	ELSE
		INIT_WAREHOUSE_ENTRY_DATA(eSimpleInteriorID, details)
		
		GET_EXIT_DATA_FOR_HUB(ciBUSINESS_HUB_EXIT_VEHICLE_ELEVATOR, details.exitData.fLocateWidths[0], details.exitData.fHeadings[0],  details.exitData.vLocateCoords1[0], details.exitData.vLocateCoords2[0])
		GET_EXIT_DATA_FOR_HUB(ciBUSINESS_HUB_EXIT_STORAGE_ELEVATOR , details.exitData.fLocateWidths[1], details.exitData.fHeadings[1],  details.exitData.vLocateCoords1[1], details.exitData.vLocateCoords2[1])
		GET_EXIT_DATA_FOR_HUB(ciBUSINESS_HUB_EXIT_GARAGE_ELEVATOR, details.exitData.fLocateWidths[2], details.exitData.fHeadings[2],  details.exitData.vLocateCoords1[2], details.exitData.vLocateCoords2[2])
			
		GET_BUSINESS_HUB_ESTABLISHING_SHOT_DATA(eSimpleInteriorID, details.entryAnim.establishingCameraPos, details.entryAnim.establishingCameraRot, details.entryAnim.fEstablishingCameraFov, details.entryAnim.fEstablishingCameraShake, bUseSecondaryInteriorDetails)
	ENDIF
	
	details.sProperties = GET_BUSINESS_HUB_PROPERTIES_BS(eSimpleInteriorID)
ENDPROC

FUNC STRING GET_BUSINESS_HUB_NAME(SIMPLE_INTERIORS eSimpleInteriorID)
	RETURN GET_BUSINESS_HUB_NAME_FROM_ID(GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
ENDFUNC

FUNC INT GET_BUSINESS_HUB_PROPERTY_ID(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SWITCH GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(PLAYER_ID())
		CASE 0		RETURN PROPERTY_NIGHTCLUB
		CASE 2		RETURN PROPERTY_MEGAWARE_GARAGE_LVL1
		CASE 3		RETURN PROPERTY_MEGAWARE_GARAGE_LVL2
		CASE 4		RETURN PROPERTY_MEGAWARE_GARAGE_LVL3
		CASE 5		RETURN PROPERTY_CLUB_TRUCK_REFERENCE_ONLY
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC STRING GET_BUSINESS_HUB_GAME_TEXT(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_GAME_TEXT eTextID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	SWITCH eTextID
		CASE SIGT_PIM_INVITE_TOOLTIP
			IF GET_INTERIOR_FLOOR_INDEX() = 0
				// nightclub
				RETURN "PIM_INVCLB_HELP"
			ELSE
				RETURN "PIM_INVHUB_HELP" // warehouse
			ENDIF
		BREAK
		CASE SIGT_PIM_INVITE_MENU_LABEL			RETURN "PIM_NIGHTMan"		BREAK
		CASE SIGT_PIM_INVITE_MENU_TITLE			RETURN "PIM_TITLE_NIGHT"	BREAK
		CASE SIGT_PROPERTY_INVITE_TICKER		RETURN "PIM_INVCLUB"		BREAK
		CASE SIGT_PROPERTY_INVITE_ALL_TICKER	RETURN "PIM_INV_A_T_NC"		BREAK
		CASE SIGT_PROPERTY_INVITE_RECIPT		RETURN "CELL_CLUBINV"		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL SHOULD_BUSINESS_HUB_SHOW_PIM_INVITE_OPTION(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &tlReturn, BOOL bReturnMenuLabel, BOOL &bSelectable)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(tlReturn)
	
	bSelectable = !IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
	
	IF bReturnMenuLabel
		tlReturn = GET_BUSINESS_HUB_GAME_TEXT(eSimpleInteriorID, SIGT_PIM_INVITE_MENU_LABEL)
	ELSE
		tlReturn = "ciPI_TYPE_INVITE_TO_SIMPLE_INTERIOR - BUSINESS_HUB"
	ENDIF
	
	RETURN IS_PLAYER_IN_NIGHTCLUB_THEY_OWN(PLAYER_ID())
ENDFUNC

FUNC BOOL SHOULD_BUSINESS_HUB_SHOW_PIM_PROPERTY_MANAGEMENT_OPTION(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bSelectable, TEXT_LABEL_15 &tlMenuOption)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	bSelectable 	= !IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
	tlMenuOption 	= "PIM_NIGHTMan"
	
	RETURN IS_PLAYER_IN_NIGHTCLUB_THEY_OWN(PLAYER_ID())
ENDFUNC

PROC GET_BUSINESS_HUB_INSIDE_BOUNDING_BOX(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX &bBox, BOOL bUseSecondaryInteriorDetails)
	
	IF bUseSecondaryInteriorDetails
		SWITCH eSimpleInteriorID
			CASE SIMPLE_INTERIOR_HUB_LA_MESA
			CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
			CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
			CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
			CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
			CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
			CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
				bBox.vInsideBBoxMin = <<-1652.900, -2976.660, -81.630>> 
				bBox.vInsideBBoxMax = <<-1564.943, -3036.836, -69.890>> 
			BREAK
		ENDSWITCH
	ELSE
		SWITCH eSimpleInteriorID
			CASE SIMPLE_INTERIOR_HUB_LA_MESA
			CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
			CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
			CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
			CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
			CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
			CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
				bBox.vInsideBBoxMin = <<-1484.172, -3046.932, -83.960>>
				bBox.vInsideBBoxMax = <<-1528.025, -2973.398, -74.070>> 
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC VECTOR GET_BUSINESS_HUB_ENTRY_CORONA_COORDS(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA				RETURN <<733.9667, -1291.1207, 25.2851>>
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW			RETURN <<333.1833, -994.4169, 28.3291>>
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE	RETURN <<-164.3258, -1292.6255, 30.3191>>
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD			RETURN <<-21.8431, 219.4443, 105.7201>>
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS			RETURN <<908.8449, -2097.3716, 29.5575>>
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE			RETURN <<-670.8386, -2392.5562, 12.9445>>	//LSIA warehouse
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND			RETURN <<227.7781, -3136.8979, 4.7903>>		//Elysian Island Warehouse
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD		RETURN <<377.8120, 228.4137, 102.0406>>		//Terminal Warehouse
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING		RETURN <<-1237.5542, -693.9150, 22.7756>>	//Del Perro Building
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS		RETURN <<-1173.5220, -1173.8550, 4.6236>>	//Vespucci Canals Building
	ENDSWITCH

	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC GET_BUSINESS_HUB_ENTRANCE_LOCATE(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vLocate1, VECTOR &vLocate2, FLOAT &fWidth, FLOAT &fEnterHeading, BOOL bIsInVehicle = FALSE)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(vLocate1)
	UNUSED_PARAMETER(vLocate2)
	UNUSED_PARAMETER(fWidth)
	UNUSED_PARAMETER(fEnterHeading)
	UNUSED_PARAMETER(bIsInVehicle)
ENDPROC

FUNC BOOL SHOULD_DISABLE_HUB_WAREHOUSE_ENTRANCE(SIMPLE_INTERIORS eSimpleInteriorID)
	BUSINESS_HUB_ID eHub = GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	// Only do this for the Warehouse...
	IF eHub = BUSINESS_HUB_ID_INVALID
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX piBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
	IF piBoss <> INVALID_PLAYER_INDEX() 
		IF DOES_PLAYER_OWN_BUSINESS_HUB(piBoss, eHub)
			IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(piBoss)
				//See: url:bugstar:4756499
				IF (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piBoss) = FMMC_TYPE_FMBB_SELL AND NOT IS_PLAYER_ON_NIGHTCLUB_SETUP_OR_DJ_MISSION(piBoss) AND GB_IS_PLAYER_CRITICAL_TO_JOB(piBoss))
				OR (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piBoss) = FMMC_TYPE_FMBB_DEFEND AND GB_IS_PLAYER_CRITICAL_TO_JOB(piBoss))
				OR (PLAYER_ID() <> piBoss AND (NOT IS_PLAYER_IN_BUSINESS_HUB(piBoss) OR GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(piBoss) = 0))
					RETURN TRUE
				ENDIF
			ENDIF
			RETURN FALSE
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), eHub)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///   If the local player doesn't own the property and isn't in a gang with someone who owns one
///   we don't want it show the locate but we need to process it so that the entrance indices 
///   don't have to be messed with.
FUNC BOOL SHOULD_DISABLE_HUB_AND_NIGHTCLUB_FRONT_DOOR_ENTRANCE(SIMPLE_INTERIORS eSimpleInteriorID)
	BUSINESS_HUB_ID eHub = GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	// Only do this for the Warehouse...
	IF eHub = BUSINESS_HUB_ID_INVALID
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX piBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
	
	IF piBoss <> INVALID_PLAYER_INDEX() 
		IF DOES_PLAYER_OWN_BUSINESS_HUB(piBoss, eHub)
			RETURN FALSE
		ENDIF
	ELIF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), eHub)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///   Serves the purpuse of blocking entry based on gang entry settings set by the boss of the local player
FUNC BOOL SHOULD_DISABLE_NIGHTCLUB_FRONT_DOOR_AND_BUZZER_ENTRANCE(SIMPLE_INTERIORS eSimpleInteriorID)
	BUSINESS_HUB_ID eHub = GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	// Only do this for the Warehouse...
	IF eHub = BUSINESS_HUB_ID_INVALID
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT SHOULD_ALLOW_NIGHTCLUB_ENTRY_DURING_MISSION()	
	#ENDIF
		RETURN TRUE
	ENDIF
	
	PLAYER_INDEX piBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
	
	IF piBoss <> INVALID_PLAYER_INDEX()
		IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(piBoss)
		#IF FEATURE_GEN9_EXCLUSIVE
		AND NOT SHOULD_ALLOW_NIGHTCLUB_ENTRY_DURING_MISSION()	
		#ENDIF
			IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piBoss) = FMMC_TYPE_FMBB_CLUB_MANAGEMENT
			OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piBoss) = FMMC_TYPE_FMBB_PHONECALL
			OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piBoss) = FMMC_TYPE_FMBB_SELL
			OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piBoss) = FMMC_TYPE_FMBB_DEFEND
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), eHub)
	OR DOES_RIVAL_NIGHTCLUB_MENU_HAVE_ANY_OPTIONS(eSimpleInteriorID)
		RETURN FALSE
	ELSE		
		IF piBoss <> INVALID_PLAYER_INDEX() 
		AND DOES_PLAYER_OWN_BUSINESS_HUB(piBoss, eHub)
		AND DOES_PLAYER_HAVE_ACCESS_TO_NIGHTCLUB_AREA(piBoss, RESTRICTED_AREA_NIGHTCLUB)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC GET_BUSINESS_HUB_ENTRY_LOCATE(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vCoronaPosition, VECTOR &vLocatePos1, VECTOR &vLocatePos2, FLOAT &fLocateWidth, FLOAT &fEnterHeading)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
			vLocatePos1 	= <<733.742493,-1290.604126,25.285114>>
			vLocatePos2		= <<733.743591,-1291.884277,27.159460>> 
			fLocateWidth 	= 1.0
			
			vCoronaPosition = << 733.792, -1291.26, 25.2845 >>
			fEnterHeading 	= 275.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
			vLocatePos1 	= <<332.647247,-994.583557,28.319872>>
			vLocatePos2		= <<334.167908,-994.593384,30.322893>> 
			fLocateWidth 	= 1.0
			
			vCoronaPosition = << 333.38, -994.532, 28.2185 >>
			fEnterHeading 	= 3.1838
		BREAK
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
			vLocatePos1 	= <<-164.902405,-1292.763916,30.316454>>
			vLocatePos2		= <<-163.364105,-1292.765259,32.314091>> 
			fLocateWidth 	= 1.0
			
			vCoronaPosition = << -164.142, -1292.8, 30.2921 >>
			fEnterHeading 	= 360.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
			vLocatePos1 	= <<-22.390398,219.422623,105.713936>>
			vLocatePos2		= <<-21.054451,219.207458,107.713623>> 
			fLocateWidth 	= 1.0
			
			vCoronaPosition = <<-21.6571, 219.415, 105.7120>>
			fEnterHeading 	= 360.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			vLocatePos1 	= <<909.563965,-2097.274658,29.547251>>
			vLocatePos2		= <<908.085815,-2097.161865,31.563482>> 
			fLocateWidth 	= 1.0
			
			vCoronaPosition = << 908.823, -2097.34, 29.5510 >>
			fEnterHeading 	= 180.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			vLocatePos1 	= <<-670.563110,-2391.818115,12.944520>>
			vLocatePos2		= <<-671.338806,-2393.192139,14.944520>>
			fLocateWidth 	= 1.0
			
			vCoronaPosition = << -670.829, -2392.53, 12.9431 >>
			fEnterHeading 	= 235.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
			vLocatePos1 	= <<228.551025,-3136.929199,4.790272>>
			vLocatePos2		= <<226.731415,-3136.910645,6.790272>> 
			fLocateWidth 	= 1.0
			
			vCoronaPosition = << 227.62, -3136.88, 4.7903 >>
			fEnterHeading 	= 180.0
		BREAK	
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
			vLocatePos1 	= <<377.728760,227.734406,102.040833>>
			vLocatePos2		= <<378.263672,229.221771,104.040253>> 
			fLocateWidth 	= 1.0
			
			vCoronaPosition = << 377.998, 228.485, 102.0406 >>
			fEnterHeading 	= 68.1481
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			vLocatePos1 	= <<-1236.951660,-694.410645,22.718826>>
			vLocatePos2		= <<-1237.939087,-693.024109,24.844929>> 
			fLocateWidth 	= 1.0
			
			vCoronaPosition = << -1237.53, -693.801, 22.7721 >>
			fEnterHeading 	= 120.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			vLocatePos1 	= <<-1174.131714,-1173.943604,4.623559>>
			vLocatePos2		= <<-1172.760498,-1173.596924,6.623559>> 
			fLocateWidth 	= 1.0
			
			vCoronaPosition = << -1173.46, -1173.71, 4.6236 >>
			fEnterHeading 	= 10.0
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_BUSINESS_HUB_ENTRY_LOCATE_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT &iR, INT &iG, INT &iB, INT &iA)
	UNUSED_PARAMETER(eSimpleInteriorID)
	GET_HUD_COLOUR(HUD_COLOUR_PURPLELIGHT, iR, iG, iB, iA)	
	iA = 100
ENDPROC

FUNC INT GET_BUSINESS_HUB_ENTRANCES_COUNT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN 4
ENDFUNC

PROC GET_BUSINESS_HUB_VEHICLE_ENTRY_LOCATE(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vCoronaPosition, VECTOR &vLocatePos1, VECTOR &vLocatePos2, FLOAT &fLocateWidth, FLOAT &fEnterHeading)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
			vLocatePos1 	= <<734.224487,-1289.083008,25.286366>>
			vLocatePos2		= <<734.226563,-1293.383057,29.034220>>
			fLocateWidth 	= 4.5
			
			vCoronaPosition = << 733.792, -1291.26, 25.2845 >>
			fEnterHeading 	= 275.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
			vLocatePos1 	= <<331.114807,-994.257446,28.332037>>
			vLocatePos2		= <<335.343475,-994.300476,32.093281>>
			fLocateWidth 	= 4.5
			
			vCoronaPosition 	= << 333.38, -994.532, 27.2185 >>
			fEnterHeading 		= 3.1838
		BREAK
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
			vLocatePos1 	= <<-166.655075,-1292.657227,30.321783>>
			vLocatePos2		= <<-161.715820,-1292.657593,34.064899>>
			fLocateWidth 	= 4.5
			
			vCoronaPosition 	= << -164.142, -1292.8, 30.2921 >>
			fEnterHeading 		= 360.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
			vLocatePos1 	= <<-23.422626,220.055405,105.745262>>
			vLocatePos2		= <<-19.936708,219.502899,109.511688>>
			fLocateWidth 	= 4.5
			
			vCoronaPosition 	= <<-21.6571, 219.415, 105.7120>>
			fEnterHeading 		= 360.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			vLocatePos1 	= <<910.983521,-2097.601563,29.534981>>
			vLocatePos2		= <<906.911499,-2097.271973,33.377502>>
			fLocateWidth 	= 4.5
			
			vCoronaPosition 	= << 908.823, -2097.34, 29.5510 >>
			fEnterHeading 		= 180.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			vLocatePos1 	= <<-668.991394,-2390.046875,12.980914>>
			vLocatePos2		= <<-672.131104,-2395.586914,16.694519>>
			fLocateWidth 	= 4.5
			
			vCoronaPosition 	= <<-671.1664, -2392.3049, 12.9431>>
			fEnterHeading 		= 235.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
			vLocatePos1 	= <<231.016983,-3137.166260,4.802446>>
			vLocatePos2		= <<224.483856,-3137.071289,8.606600>>
			fLocateWidth 	= 4.5
			
			vCoronaPosition 	= <<227.6135, -3136.6545, 4.7903>>
			fEnterHeading 		= 180.0
		BREAK	
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
			vLocatePos1 	= <<376.422272,225.365311,102.080460>>
			vLocatePos2		= <<378.742889,231.670700,105.803551>>
			fLocateWidth 	= 4.5
			
			vCoronaPosition 	= << 377.998, 228.485, 102.0406 >>
			fEnterHeading 		= 68.1481
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			vLocatePos1 	= <<-1236.200439,-695.851440,22.615837>>
			vLocatePos2		= <<-1239.183594,-692.103455,26.723429>>
			fLocateWidth 	= 4.5
			
			vCoronaPosition 	= << -1237.53, -693.801, 22.7721 >>
			fEnterHeading 		= 120.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			vLocatePos1 	= <<-1175.651611,-1174.024902,4.642128>>
			vLocatePos2		= <<-1171.394409,-1172.917114,8.392513>>
			fLocateWidth 	= 4.5
			
			vCoronaPosition 	= << -1173.46, -1173.71, 4.6236 >>
			fEnterHeading 		= 10.0
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_PLAYER_INSIDE_BUSINESS_HUB_ENTRY_AREA(SIMPLE_INTERIOR_ENTRANCE_DATA &data)
	VECTOR vLocate, vLocatePos1, vLocatePos2
	FLOAT fLocateWidth, fEnterHeading
	
	GET_BUSINESS_HUB_VEHICLE_ENTRY_LOCATE(data.eSimpleInteriorID, vLocate, vLocatePos1, vLocatePos2, fLocateWidth, fEnterHeading)
	
	IF IS_POINT_IN_ANGLED_AREA(GET_PLAYER_COORDS(PLAYER_ID()), vLocatePos1, vLocatePos2, fLocateWidth)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_NIGHTCLUB_BUZZER_ENTRY_LOCATE(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vLocatePos1, VECTOR &vLocatePos2, FLOAT &fLocateWidth, FLOAT &fHeading, VECTOR &vCoronaPos)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
			vLocatePos1 = <<756.446350,-1332.397461,26.280241>> 
			vLocatePos2 = <<757.376038,-1332.402222,28.530243>>
			vCoronaPos	= << 757.009, -1332.32, 26.1802 >>
			fLocateWidth = 1.0
			fHeading = 354.7854
		BREAK
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
			vLocatePos1 = <<345.694458,-979.376343,28.377829>> 
			vLocatePos2 = <<345.691711,-978.440735,30.629799>>
			vCoronaPos	= <<345.7519, -978.8848, 28.2681>>
			fLocateWidth = 1.00
			fHeading = 90.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
			vLocatePos1 = <<-120.863113,-1261.067139,28.307959>>
			vLocatePos2 = <<-120.850822,-1259.892700,30.558613>> 
			vCoronaPos	= << -120.906, -1260.49, 28.2088 >>
			fLocateWidth = 1.00
			fHeading = 90.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
			vLocatePos1 = <<5.446280,220.885864,106.823097>> 
			vLocatePos2 = <<5.777968,221.780533,109.203987>> 
			vCoronaPos	= << 5.53709, 221.35, 106.6566 >>
			fLocateWidth = 1.00
			fHeading = 70.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			vLocatePos1 = <<871.356567,-2099.901123,29.470390>>
			vLocatePos2 = <<871.447815,-2098.993896,31.715075>> 
			vCoronaPos	= << 871.47, -2099.57, 29.3768 >>
			fLocateWidth = 1.00
			fHeading = 271.9949
		BREAK
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			vLocatePos1 = <<-675.663391,-2458.955078,12.944397>>
			vLocatePos2 = <<-674.811462,-2459.447021,15.194397>> 
			vCoronaPos	= << -675.225, -2459.15, 12.8444 >>
			fLocateWidth = 1.00
			fHeading = 332.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
			vLocatePos1 = <<195.440796,-3168.408936,4.790267>>
			vLocatePos2 = <<195.440292,-3169.471436,7.040267>>
			vCoronaPos	= << 195.534, -3168.88, 4.7903 >>
			fLocateWidth = 1.0
			fHeading = 270.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
			vLocatePos1 = <<373.590149,251.981461,102.009697>> 
			vLocatePos2 = <<372.477051,252.376770,104.259697>>
			vCoronaPos	= << 373.05, 252.13, 101.9097 >>
			fLocateWidth = 1.0
			fHeading = 160.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			vLocatePos1 = <<-1282.958740,-649.617126,25.627918>> 
			vLocatePos2 = <<-1283.881104,-650.329285,27.862347>> 
			vCoronaPos	= << -1283.38, -649.916, 25.519802 >>
			fLocateWidth = 1.0
			fHeading = 212.1381
		BREAK
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			vLocatePos1 = <<-1174.614746,-1152.797363,4.659540>> 
			vLocatePos2 = <<-1174.849976,-1151.918457,6.911390>>  
			vCoronaPos	= << -1174.85, -1152.3, 4.561280 >>
			fLocateWidth = 1.0
			fHeading = 105.5
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_NIGHTCLUB_DOOR_ENTRY_LOCATE(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vLocatePos1, VECTOR &vLocatePos2, FLOAT &fLocateWidth, FLOAT &fHeading)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
			vLocatePos1 = <<758.680603,-1331.903931,25.280243>> 
			vLocatePos2 =  <<757.402283,-1331.900757,28.580242>> 
			fLocateWidth =  1.50
			fHeading = 354.7854
		BREAK
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
			vLocatePos1 = <<345.146545,-977.274658,27.392076>> 
			vLocatePos2 =   <<345.179993,-978.439819,30.706421>> 
			fLocateWidth =   2.00
			fHeading = 90.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
			vLocatePos1 = <<-122.075653,-1257.641113,27.308887>> 
			vLocatePos2 =   <<-122.065872,-1259.574463,30.607834>> 
			fLocateWidth =   2.00
			fHeading = 90.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
			vLocatePos1 = <<3.550406,220.067841,105.837875>> 
			vLocatePos2 =   <<3.937627,221.143082,109.118919>> 
			fLocateWidth =   2.00
			fHeading = 70.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			vLocatePos1 = <<871.737732,-2101.242188,28.521873>> 
			vLocatePos2 =   <<871.904419,-2099.873779,31.887064>> 
			fLocateWidth =   2.00
			fHeading = 271.9949
		BREAK
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			vLocatePos1 = <<-677.439575,-2457.045654,12.146402>> 
			vLocatePos2 =   <<-675.268494,-2458.296631,15.951605>> 
			fLocateWidth =   2.00
			fHeading = 332.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
			vLocatePos1 = <<195.955948,-3166.155518,3.801306>> 
			vLocatePos2 =   <<195.955963,-3168.459717,7.209254>> 
			fLocateWidth =   2.0
			fHeading = 270.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
			vLocatePos1 = <<372.374420,251.834366,101.028633>> 
			vLocatePos2 =   <<369.701599,252.773392,104.477188>>
			fLocateWidth =   2.0
			fHeading = 160.0
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			vLocatePos1 = <<-1283.653809,-651.751709,24.661140>> 
			vLocatePos2 =   <<-1285.834106,-653.455627,28.068668>> 
			fLocateWidth =   2.000000
			fHeading = 212.1381
		BREAK
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			vLocatePos1 = <<-1174.951660,-1152.897949,3.665442>> 
			vLocatePos2 =   <<-1174.571167,-1154.307739,7.456554>> 
			fLocateWidth =   2.0
			fHeading = 105.5
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_NIGHTCLUB_BUZZER_ENTRY_LOCATE_COLOUR(INT &iR, INT &iG, INT &iB, INT &iA)
	GET_HUD_COLOUR(HUD_COLOUR_PURPLELIGHT, iR, iG, iB, iA)	
	iA = 100
ENDPROC

FUNC INT GET_BUSINESS_HUB_INVITE_ENTRY_POINT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
ENDFUNC

PROC GET_BUSINESS_HUB_ENTRANCE_DATA(SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID, SIMPLE_INTERIOR_ENTRANCE_DATA &data)	
	SWITCH iEntranceID
		CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
			// Disable entrance with tunable
			IF (g_sMPTunables.bBB_WAREHOUSE_DISABLE_ENTRANCE_TO_WAREHOUSE_GARAGE
			OR SHOULD_DISABLE_HUB_WAREHOUSE_ENTRANCE(eSimpleInteriorID))
			AND NOT IS_BIT_SET(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IGNORE_ACCESS_RESTRICTIONS_ID_0)
				CLEAR_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_DRAWS_LOCATE)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_ON_FOOT_ONLY)
				data.vLocatePos1 = <<1.0, 1.0, 0.1>>
				data.vPosition = <<1.0, 1.0, 1.0>>
			ELSE			
				GET_BUSINESS_HUB_ENTRY_LOCATE(eSimpleInteriorID, data.vPosition, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth, data.fEnterHeading)
				
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_DRAWS_LOCATE)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_ON_FOOT_ONLY)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_USES_ANGLED_AREA)
				
				GET_BUSINESS_HUB_ENTRY_LOCATE_COLOUR(eSimpleInteriorID, data.iLocateColourR, data.iLocateColourG, data.iLocateColourB, data.iLocateColourA)
			ENDIF
		BREAK
		CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
			// Disable entrance with tunable
			IF g_sMPTunables.bBB_WAREHOUSE_DISABLE_ENTRANCE_TO_WAREHOUSE_GARAGE
			AND NOT IS_BIT_SET(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IGNORE_ACCESS_RESTRICTIONS_ID_0)
				CLEAR_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_DRAWS_LOCATE)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IN_CAR_ONLY)
				data.vLocatePos1 = <<1.0, 1.0, 0.1>>
				data.vPosition = <<1.0, 1.0, 1.0>>
			ELSE	
				GET_BUSINESS_HUB_VEHICLE_ENTRY_LOCATE(eSimpleInteriorID, data.vPosition, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth, data.fEnterHeading)
				data.iType = 1
				
				IF NOT IS_BIT_SET(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CUSTOM_CUTSCENE)
					SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CUSTOM_CUTSCENE)
				ENDIF
				
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_USES_ANGLED_AREA)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IN_CAR_ONLY)
				
				GET_BUSINESS_HUB_ENTRY_LOCATE_COLOUR(eSimpleInteriorID, data.iLocateColourR, data.iLocateColourG, data.iLocateColourB, data.iLocateColourA)
			ENDIF
		BREAK
		CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
			IF SHOULD_DISABLE_NIGHTCLUB_FRONT_DOOR_AND_BUZZER_ENTRANCE(eSimpleInteriorID)
			AND NOT IS_BIT_SET(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IGNORE_ACCESS_RESTRICTIONS_ID_0)
				CLEAR_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_DRAWS_LOCATE)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_ON_FOOT_ONLY)
				data.vLocatePos1 = <<1.0, 1.0, 0.1>>
				data.vPosition = <<1.0, 1.0, 1.0>>
			ELSE
				GET_NIGHTCLUB_BUZZER_ENTRY_LOCATE(eSimpleInteriorID, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth, data.fEnterHeading, data.vPosition)		
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_DRAWS_LOCATE)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_ON_FOOT_ONLY)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_MATCH_HEADING)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_USES_ANGLED_AREA)
				GET_NIGHTCLUB_BUZZER_ENTRY_LOCATE_COLOUR(data.iLocateColourR, data.iLocateColourG, data.iLocateColourB, data.iLocateColourA)
			ENDIF
		BREAK
		CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
			IF (SHOULD_DISABLE_HUB_AND_NIGHTCLUB_FRONT_DOOR_ENTRANCE(eSimpleInteriorID)
			OR SHOULD_DISABLE_NIGHTCLUB_FRONT_DOOR_AND_BUZZER_ENTRANCE(eSimpleInteriorID))
			AND NOT IS_BIT_SET(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IGNORE_ACCESS_RESTRICTIONS_ID_0)
				CLEAR_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_DRAWS_LOCATE)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_ON_FOOT_ONLY)
				data.vLocatePos1 = <<1.0, 1.0, 0.1>>
				data.vPosition = <<1.0, 1.0, 1.0>>
			ELSE
				GET_NIGHTCLUB_DOOR_ENTRY_LOCATE(eSimpleInteriorID, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth, data.fEnterHeading)	
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_ON_FOOT_ONLY)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_MATCH_HEADING)
				SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_USES_ANGLED_AREA)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_THIS_BUSINESS_HUB_CORONA_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason = FALSE #ENDIF )
	UNUSED_PARAMETER(eSimpleInteriorID)
	#IF IS_DEBUG_BUILD
	UNUSED_PARAMETER(bPrintReason)
	#ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
	AND IS_PLAYER_ON_MP_INTRO_MISSION()
	AND NOT SHOULD_ALLOW_NIGHTCLUB_ENTRY_DURING_MISSION()
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_PREVENT_DRAWING_SIMPLE_INTERIOR_CORONA)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT SHOULD_ALLOW_NIGHTCLUB_ENTRY_DURING_MISSION()
	#ENDIF
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_BUSINESS_HUB_ALLOW_ENTRY_IN_A_TUTORIAL_SESSION(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	RETURN SHOULD_ALLOW_NIGHTCLUB_ENTRY_DURING_MISSION()
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_THIS_BUSINESS_HUB_LOCATE_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason = FALSE #ENDIF )
	UNUSED_PARAMETER(eSimpleInteriorID)
	#IF IS_DEBUG_BUILD
	UNUSED_PARAMETER(bPrintReason)
	#ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_BLOCKED_FROM_PROPERTY(TRUE, PROPERTY_BUSINESS_HUB)
		RETURN TRUE
	ENDIF
	
	IF NOT CAN_PLAYER_USE_PROPERTY(TRUE #IF IS_DEBUG_BUILD , TRUE #ENDIF )
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.sGlobalController.iBS, SIMPLE_INTERIOR_CONTROL_BS_BLOCK_BUSINESS_HUB_ENTRY)
		RETURN TRUE
	ENDIF

	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT SHOULD_ALLOW_NIGHTCLUB_ENTRY_DURING_MISSION()
	#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_HACKER_TRUCK_PILOT_ENTERING()
	OR IS_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_BUSINESS_HUB_LOCATE_BE_HIDDEN player is entering hacker truck from pilot seat")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_ARMORY_AIRCRAFT_PILOT_ENTERING()
	OR HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR] SHOULD_THIS_BUSINESS_HUB_LOCATE_BE_HIDDEN TRUE player is entering avenger from pilot seat")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DRIVER_ABLE_TO_ENTER_BUSINESS_HUB(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX theVeh)
	PED_INDEX theDriverPed = GET_PED_IN_VEHICLE_SEAT(theVeh)
	
	PLAYER_INDEX theDriver
	
	IF DOES_ENTITY_EXIST(theDriverPed)
	AND IS_PED_A_PLAYER(theDriverPed)
	AND IS_VEHICLE_DRIVEABLE(theVeh)
		theDriver = NETWORK_GET_PLAYER_INDEX_FROM_PED(theDriverPed)
		
		IF NOT IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(GET_ENTITY_MODEL(theVeh), PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1)
			PRINTLN("IS_DRIVER_ABLE_TO_ENTER_BUSINESS_HUB: FALSE not suitable")
			
			RETURN FALSE
		ELIF DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE(theVeh)
			PRINTLN("IS_DRIVER_ABLE_TO_ENTER_BUSINESS_HUB: FALSE decorator prevents entry")
			
			RETURN FALSE
		ELIF IS_VEHICLE_A_PERSONAL_VEHICLE(theVeh)
		AND GET_OWNER_OF_PERSONAL_VEHICLE(theVeh) != theDriver
		AND NOT IS_DRIVER_ENTERING_SIMPLE_INTERIOR(theDriver)
			PRINTLN("IS_DRIVER_ABLE_TO_ENTER_BUSINESS_HUB: FALSE personal vehicle not belonging to player")
			
			RETURN FALSE
		ENDIF
		
		IF DOES_PLAYER_OWN_A_BUSINESS_HUB(theDriver)
			IF GET_PLAYERS_OWNED_BUSINESS_HUB(theDriver) = GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
				IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(theDriver)
				OR GB_IS_PLAYER_BOSS_OF_A_GANG(theDriver)
					IF NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(theDriver)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_BUSINESS_HUB_MENU_CAMERA(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	PRINTLN("CLEANUP_BUSINESS_HUB_MENU_CAMERA called")
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	IF DOES_CAM_EXIST(entryAnim.camera)
		DESTROY_CAM(entryAnim.camera)
		
		PRINTLN("CLEANUP_BUSINESS_HUB_MENU_CAMERA camera exists and is destroyed")
	ENDIF
ENDPROC

PROC SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB(SIMPLE_INTERIOR_DETAILS &details,BOOL bCleanup = FALSE)
	//PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB Original door state = ",details.entryAnim.vehDoorLocks, " vehicle = ", NATIVE_TO_INT(details.entryAnim.vehEntered))
	IF NOT bCleanup
		PRINTLN("SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB: called with bCleanup: FALSE")
		
		MODEL_NAMES eVehicleModel = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		
		IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			IF eVehicleModel = TERBYTE
			OR IS_THIS_MODEL_A_HUB_DELIVERY_VEHICLE(eVehicleModel)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ELSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_ALLOW_PLAYER_DAMAGE)
			ENDIF
		ELSE
			PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB - Unable to disable player control.")
		ENDIF
		
		IF eVehicleModel = TERBYTE
		OR IS_THIS_MODEL_A_HUB_DELIVERY_VEHICLE(eVehicleModel)
			IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE)
			details.entryAnim.vehEntered = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) , 1 , 1)
			SET_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE)
			PRINTLN("SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB: BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE TRUE")
			SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() 
			details.entryAnim.vehDoorLocks = GET_VEHICLE_DOOR_LOCK_STATUS(details.entryAnim.vehEntered )
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(details.entryAnim.vehEntered , TRUE)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(details.entryAnim.vehEntered , TRUE)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(details.entryAnim.vehEntered )
				SET_VEHICLE_DOORS_LOCKED(details.entryAnim.vehEntered ,VEHICLELOCK_LOCKED)
				PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB Locking car doors for entry ")
			ENDIF
			PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] -SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB Saving Original door state = ",details.entryAnim.vehDoorLocks, " vehicle = ", NATIVE_TO_INT(details.entryAnim.vehEntered))
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
			PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] -  SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB knocked off vehicle NEVER")
			SET_BIT(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
			SET_VEHICLE_DOORS_LOCKED_IF_OPEN(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			SET_VEHICLE_DISABLE_TOWING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) 
		AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, TRUE)							
		ENDIF	
	
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE, FALSE)
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		DISABLE_FRONTEND_THIS_FRAME()
		SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
		IF !IS_WARNING_MESSAGE_ACTIVE()
			SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		ENDIF
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		SET_BIT(details.entryAnim.iBS,BS_SIMPLE_INTERIOR_ANIM_BS_STARTED_HANGAR_ENTRY)
	ELSE
		IF IS_BIT_SET(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE)
		OR IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID())
		OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_VEH_STORAGE_WARN_FULL)
			PRINTLN("SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB: called with bCleanup: TRUE")
			
			CLEANUP_BUSINESS_HUB_MENU_CAMERA(details.entryAnim)
			
			IF DOES_ENTITY_EXIST(details.entryAnim.vehEntered)
			AND IS_VEHICLE_DRIVEABLE(details.entryAnim.vehEntered,TRUE)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(details.entryAnim.vehEntered)
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(details.entryAnim.vehEntered, FALSE)
					IF details.entryAnim.vehDoorLocks != VEHICLELOCK_NONE
					AND details.entryAnim.vehDoorLocks != GET_VEHICLE_DOOR_LOCK_STATUS(details.entryAnim.vehEntered)
						SET_VEHICLE_DOORS_LOCKED(details.entryAnim.vehEntered,details.entryAnim.vehDoorLocks)
					ENDIF
					PRINTLN("SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB: setting door states back to original after aborting exit data.vehDoorLock = ",details.entryAnim.vehDoorLocks)
					CLEAR_BIT(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(details.entryAnim.vehEntered)
					PRINTLN("SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB: getting control of entering vehicle")
				ENDIF
			ELSE
				CLEAR_BIT(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) , FALSE)
					SET_VEHICLE_DISABLE_TOWING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
				ENDIF	
			ELSE
				IF DOES_ENTITY_EXIST(details.entryAnim.vehEntered)
				AND NOT IS_ENTITY_DEAD(details.entryAnim.vehEntered)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(details.entryAnim.vehEntered)
					SET_ENTITY_INVINCIBLE(details.entryAnim.vehEntered , FALSE)
					SET_VEHICLE_DISABLE_TOWING(details.entryAnim.vehEntered, FALSE)
				ENDIF
			ENDIF
			IF IS_BIT_SET(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE)
				CLEAR_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE)
			ENDIF
			
			IF NOT IS_SKYSWOOP_MOVING()
			AND NOT IS_SKYSWOOP_IN_SKY()
				IF PLAYER_ID() != INVALID_PLAYER_INDEX()
				AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
				AND NOT IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID())
					NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				ELSE
					PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB - Unable to enable player control.") 
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)	
			ENDIF
			
			PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] -  SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB setting knocked off vehicle DEFAULT")
			CLEAR_BIT(details.entryAnim.iBS,BS_SIMPLE_INTERIOR_ANIM_BS_STARTED_HANGAR_ENTRY)
			IF IS_PASSENGER_ENTERING_WITH_DRIVER(PLAYER_ID())
				ENABLE_FORCE_FROM_CAR_FOR_LOCAL_PLAYER()
				PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] -  SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB enabling kick from vehicle after being set to false")
				SET_PASSENGER_ENTERING_WITH_DRIVER_TO_SIMPLE_INTERIOR(FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TIDY_UP_BUSINESS_HUB_ENTRY_FLAGS(SIMPLE_INTERIOR_DETAILS &details)
	RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
	
	IF IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID())
	AND GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(PLAYER_ID())) = SIMPLE_INTERIOR_TYPE_BUSINESS_HUB
		SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB(details, TRUE)
	ENDIF
	
	IF IS_BIT_SET(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_BS_STARTED_HANGAR_ENTRY)
		SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB(SIMPLE_INTERIOR_DETAILS &details, VEHICLE_INDEX pedVeh, BOOL bCheckSectionOwnerShip = TRUE)
	UNUSED_PARAMETER(details)
	
	OWNED_PROPERTIES_IN_BUILDING ownedDetails
	
	IF NOT DOES_ENTITY_EXIST(pedVeh)
	OR IS_ENTITY_DEAD(pedVeh)
	OR NOT IS_VEHICLE_DRIVEABLE(pedVeh)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB FALSE vehicle not drivable")
		RETURN FALSE
	ENDIF	
	
	IF IS_ENTITY_ON_FIRE(pedVeh)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB FALSE vehicle on is on fire")
		RETURN FALSE	
	ENDIF
	
	IF IS_ENTITY_UPSIDEDOWN(pedVeh)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB FALSE IS_ENTITY_UPSIDEDOWN")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WITH_A_PROSTITUTE()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB FALSE IS_PLAYER_WITH_A_PROSTITUTE")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB FALSE player in corona")
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT ARE_STRINGS_EQUAL(g_sMenuData.tl15Title, "HUB_VEH_TITLE")
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB IS_CUSTOM_MENU_ON_SCREEN true")
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB IS_INTERACTION_MENU_OPEN true")
		RETURN FALSE
	ENDIF
	
	IF MPGlobalsAmbience.R2Pdata.bStartingRacePointMenuFromPI
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB MPGlobalsAmbience.R2Pdata.bStartingRacePointMenuFromPI true")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB IS_SKYSWOOP_AT_GROUND false")
		RETURN FALSE
	ENDIF
	
	IF bCheckSectionOwnerShip
		IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
		AND NOT IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(pedVeh)
			IF NOT IS_VEHICLE_SUITABLE_FOR_PROPERTY(pedVeh, PROPERTY_MEGAWARE_GARAGE_LVL1, ownedDetails)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB - IS_VEHICLE_SUITABLE_FOR_PROPERTY FALSE")
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTRY_BUSINESS_HUB()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		PLAYER_INDEX thePlayer
		
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			thePlayer = INT_TO_PLAYERINDEX(i)
			
			IF thePlayer != PLAYER_ID()
			AND thePlayer != INVALID_PLAYER_INDEX()
				IF IS_NET_PLAYER_OK(thePlayer)
					IF DOES_ENTITY_EXIST(pedVeh)
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), pedVeh, FALSE)
							IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(thePlayer))
							OR g_MultiplayerSettings.g_bSuicide
							OR IS_SELECTOR_ONSCREEN()
							OR  GET_IS_TASK_ACTIVE(GET_PLAYER_PED(thePlayer), CODE_TASK_EXIT_VEHICLE)
								#IF IS_DEBUG_BUILD
									PRINTLN("ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTRY_BUSINESS_HUB: ", GET_PLAYER_NAME(thePlayer)," not ready not set flag")
								#ENDIF
								
								RETURN FALSE
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_PLAYER_COORDS(thePlayer)) <= 125
								IF NOT IS_REMOTE_PLAYER_IN_NON_CLONED_VEHICLE(thePlayer)
									IF GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(thePlayer)) = pedVeh
										#IF IS_DEBUG_BUILD
										PRINTLN("ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTRY_BUSINESS_HUB: ", GET_PLAYER_NAME(thePlayer)," not ready entering")
										#ENDIF
										
										RETURN FALSE
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), pedVeh, TRUE)
							OR IS_ENTITY_ATTACHED_TO_ENTITY(GET_PLAYER_PED(thePlayer), pedVeh)
								#IF IS_DEBUG_BUILD
								PRINTLN("ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTRY_BUSINESS_HUB: ", GET_PLAYER_NAME(thePlayer)," entering or attached")
								#ENDIF
								
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC FLOAT GET_INTERP_POINT_FLOAT_FOR_BUSINESS_HUB(FLOAT fStartPos, FLOAT fEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN ((((fEndPos - fStartPos) / (fEndTime - fStartTime)) * (fPointTime - fStartTime)) + fStartPos)
ENDFUNC

FUNC VECTOR GET_INTERP_POINT_VECTOR_FOR_BUSINESS_HUB(VECTOR vStartPos, VECTOR vEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN <<GET_INTERP_POINT_FLOAT_FOR_BUSINESS_HUB(vStartPos.X, vEndPos.X, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT_FOR_BUSINESS_HUB(vStartPos.Y, vEndPos.Y, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT_FOR_BUSINESS_HUB(vStartPos.Z, vEndPos.Z, fStartTime, fEndTime, fPointTime)>>
ENDFUNC

FUNC BOOL IS_PLAYER_DRIVING_INTO_BUSINESS_HUB(SIMPLE_INTERIOR_ENTRANCE_DATA &data, VEHICLE_INDEX theVehicle, MODEL_NAMES theModel)
	VECTOR vFrontTopLeft, vFrontTopRight, vBackTopLeft, vBackTopRight
	DETERMINE_4_TOP_VECTORS_OF_MODEL_BOX_IN_WORLD_COORDS(theVehicle, theModel, vFrontTopLeft, vFrontTopRight, vBackTopLeft, vBackTopRight)
	
	VECTOR vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight
	DETERMINE_4_BOTTOM_VECTORS_OF_MODEL_BOX_IN_WORLD_COORDS(theVehicle, theModel, vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight)
	
	VECTOR vFrontMiddleLeft, vFrontMiddleRight, vBackMiddleLeft, vBackMiddleRight, vFrontMiddleBottom, vFrontMiddleTop
	
	vFrontMiddleLeft = GET_INTERP_POINT_VECTOR_FOR_BUSINESS_HUB(vFrontBottomLeft, vFrontTopLeft, 0.0, 1.0, 0.5)
	vFrontMiddleRight = GET_INTERP_POINT_VECTOR_FOR_BUSINESS_HUB(vFrontBottomRight, vFrontTopRight, 0.0, 1.0, 0.5)
	vBackMiddleLeft = GET_INTERP_POINT_VECTOR_FOR_BUSINESS_HUB(vBackBottomLeft, vBackTopLeft, 0.0, 1.0, 0.5)
	vBackMiddleRight = GET_INTERP_POINT_VECTOR_FOR_BUSINESS_HUB(vBackBottomRight, vBackTopRight, 0.0, 1.0, 0.5)
	
	IF IS_POINT_IN_ANGLED_AREA(vFrontMiddleLeft, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth)
	OR IS_POINT_IN_ANGLED_AREA(vFrontMiddleRight, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth)
		IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(theVehicle), data.fEnterHeading, 75.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	vFrontMiddleBottom = GET_INTERP_POINT_VECTOR_FOR_BUSINESS_HUB(vFrontBottomLeft, vFrontBottomRight, 0.0, 1.0, 0.5)
	vFrontMiddleTop = GET_INTERP_POINT_VECTOR_FOR_BUSINESS_HUB(vFrontTopLeft, vFrontTopRight, 0.0, 1.0, 0.5)
	IF IS_POINT_IN_ANGLED_AREA(vFrontMiddleBottom, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth)
	OR IS_POINT_IN_ANGLED_AREA(vFrontMiddleTop, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth)
		IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(theVehicle), data.fEnterHeading, 75.0)
			RETURN TRUE
		ENDIF
	ELIF IS_POINT_IN_ANGLED_AREA(vBackMiddleLeft, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth)
	OR IS_POINT_IN_ANGLED_AREA(vBackMiddleRight, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth)
		IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(theVehicle), data.fEnterHeading - 180.0, 75.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_THIS_PLAYER_ENTER_BUSINESS_HUB(PLAYER_INDEX tempPlayer)
	IF IS_NET_PLAYER_OK(tempPlayer)
		IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(tempPlayer)
		AND NOT IS_THIS_PLAYER_ALLOWED_IN_BUSINESS_HUB(tempPlayer)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_BUSINESS_HUB_VEHICLE_AS_LAST_USED()
	IF g_iCurrentPVSlotInProperty > -1
		IF g_iPreviousLastUsedSlot = -1
			g_iPreviousLastUsedSlot = CURRENT_SAVED_VEHICLE_SLOT()
		ENDIF
		
		SET_LAST_USED_VEHICLE_SLOT(g_iCurrentPVSlotInProperty)
		SET_BIT(g_MpSavedVehicles[g_iCurrentPVSlotInProperty].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
		PRINTLN("SET_BUSINESS_HUB_VEHICLE_AS_LAST_USED caleld this frame")
	ENDIF
ENDPROC

FUNC BOOL SHOULD_TRIGGER_EXIT_IN_CAR_FOR_BUSINESS_HUB(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX interiorOwner, BOOL bOwnerDrivingOut)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_INTERIOR] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_BUSINESS_HUB - IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD - FALSE")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF interiorOwner = PLAYER_ID()
			IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			AND NOT GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
				IF g_bPlayerLeavingCurrentInteriorInVeh
					IF IS_VEHICLE_A_PERSONAL_VEHICLE(vTemp)
					OR IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(vTemp)
						IF g_bPlayerLeavingCurrentInteriorInVeh
						OR IS_PLAYER_LEAVING_HUB_FROM_MOD_SHOP(PLAYER_ID())
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vTemp, TRUE)
							SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vTemp, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							
							IF IS_VEHICLE_A_PERSONAL_VEHICLE(vTemp)
								SET_BUSINESS_HUB_VEHICLE_AS_LAST_USED()
							ENDIF
							
							g_bPlayerLeavingHangarInAircraft = TRUE
							
							#IF IS_DEBUG_BUILD
							PRINTLN("[SIMPLE_INTERIOR] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_BUSINESS_HUB - TRUE")
							#ENDIF
							
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_PED_IN_VEHICLE_SEAT(vTemp, VS_DRIVER) = GET_PLAYER_PED(interiorOwner)
			AND bOwnerDrivingOut
				#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_INTERIOR] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_BUSINESS_HUB - TRUE")
				#ENDIF
				
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC KICK_REMAINING_PLAYERS_FROM_BUSINESS_HUB_VEHICLE(VEHICLE_INDEX vehSVM)
	PLAYER_INDEX tempPlayer
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		tempPlayer = INT_TO_PLAYERINDEX(i)
		
		IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
		AND tempPlayer <> PLAYER_ID()
			IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), vehSVM)
			AND NOT CAN_THIS_PLAYER_ENTER_BUSINESS_HUB(tempPlayer)
				BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(tempPlayer), FALSE, 0.0, 0, FALSE)
				SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(vehSVM, tempPlayer, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_BUSINESS_HUB_ENTRY_ABORT_FOR_PASSENGER(SIMPLE_INTERIORS eSimpleInteriorID)
	IF IS_WARNING_MESSAGE_ACTIVE()
	OR SHOULD_THIS_BUSINESS_HUB_LOCATE_BE_HIDDEN(eSimpleInteriorID)
		PRINTLN("SHOULD_BUSINESS_HUB_ENTRY_ABORT_FOR_PASSENGER TRUE")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_WARP_WITH_DRIVER_INTO_BUSINESS_HUB(SIMPLE_INTERIORS eSimpleInteriorID)
	VEHICLE_INDEX theVeh
	PED_INDEX vehiclePed
	PLAYER_INDEX vehiclePlayer
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
			theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_OK(theVeh)
				vehiclePed = GET_PED_IN_VEHICLE_SEAT(theVeh)
				
				IF NOT IS_PED_INJURED(vehiclePed)
					IF IS_PED_A_PLAYER(vehiclePed)
						vehiclePlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(vehiclePed)
						
						IF vehiclePlayer != PLAYER_ID()
						AND IS_NET_PLAYER_OK(vehiclePlayer)
							IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(vehiclePlayer)].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING)
							AND GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(vehiclePlayer)) = SIMPLE_INTERIOR_TYPE_BUSINESS_HUB)
							AND (GB_GET_LOCAL_PLAYER_GANG_BOSS() = vehiclePlayer OR GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX())
								IF SHOULD_BUSINESS_HUB_ENTRY_ABORT_FOR_PASSENGER(eSimpleInteriorID)
									IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
										TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
										
										PRINTLN("MAINTAIN_WARP_WITH_DRIVER_INTO_BUSINESS_HUB: TASK_LEAVE_ANY_VEHICLE")
									ENDIF
									
									IF IS_PASSENGER_ENTERING_WITH_DRIVER(PLAYER_ID())
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
										
										PRINTLN("MAINTAIN_WARP_WITH_DRIVER_INTO_BUSINESS_HUB: kicking player out of vehicle and returning player control")
									ENDIF
								ELSE
									IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(vehiclePlayer)].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_VEH_STORAGE_WARN_FULL)
									AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(vehiclePlayer)].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_PROCESS_VEHICLE_MENU)
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
										
										PRINTLN("MAINTAIN_WARP_WITH_DRIVER_INTO_BUSINESS_HUB: turning off player control")
									ELSE
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
										
										PRINTLN("MAINTAIN_WARP_WITH_DRIVER_INTO_BUSINESS_HUB: allowing control while driver is on hangar full warning")
									ENDIF
									
									DISABLE_FORCE_FROM_CAR_FOR_LOCAL_PLAYER()
									SET_PASSENGER_ENTERING_WITH_DRIVER_TO_SIMPLE_INTERIOR(TRUE)
									
									PRINTLN("MAINTAIN_WARP_WITH_DRIVER_INTO_BUSINESS_HUB: set on local player")
									
									EXIT
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PASSENGER_ENTERING_WITH_DRIVER(PLAYER_ID())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		PRINTLN("MAINTAIN_WARP_WITH_DRIVER_INTO_BUSINESS_HUB: turning on control no longer entering with owner")
		
		ENABLE_FORCE_FROM_CAR_FOR_LOCAL_PLAYER()
		SET_PASSENGER_ENTERING_WITH_DRIVER_TO_SIMPLE_INTERIOR(FALSE)
	ENDIF
ENDPROC

FUNC BOOL CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE(SIMPLE_INTERIOR_ENTRANCE_DATA &data, SIMPLE_INTERIOR_DETAILS &details, BOOL &bReturnSetExitCoords)
	
	BOOL bKeepContext
	BOOL bOnGroundRecently
	bReturnSetExitCoords = FALSE
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - checked this frame")
	#ENDIF
	
	IF IS_THIS_SIMPLE_INTERIOR_ENTRANCE_IN_CAR_ONLY(data)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			BUSINESS_HUB_ID eBusinessHub = GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(data.eSimpleInteriorID)
			
			IF IS_THIS_VEHICLE_A_PEGASUS_VEHICLE(vehIndex)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Cannot access interior as Pegasus entry is disabled, returning FALSE.")
				#ENDIF
				TIDY_UP_BUSINESS_HUB_ENTRY_FLAGS(details)
				RETURN FALSE
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(vehIndex)
			OR NOT IS_VEHICLE_DRIVEABLE(vehIndex)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Cannot access interior as vehicle is not driveable, returning FALSE.")
				#ENDIF
				TIDY_UP_BUSINESS_HUB_ENTRY_FLAGS(details)
				RETURN FALSE
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(GET_ENTITY_MODEL(vehIndex), PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Cannot access interior in this type of vehicle, returning FALSE.")
				#ENDIF
				TIDY_UP_BUSINESS_HUB_ENTRY_FLAGS(details)
				RETURN FALSE
			ENDIF
			
			PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(vehIndex)
			
			IF NOT DOES_ENTITY_EXIST(pedDriver)
			OR IS_PED_INJURED(pedDriver)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - No driver, returning FALSE.")
				#ENDIF
				TIDY_UP_BUSINESS_HUB_ENTRY_FLAGS(details)
				RETURN FALSE
			ENDIF
			
			PLAYER_INDEX pDriver = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriver)
			
			IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehIndex)
			AND GET_OWNER_OF_PERSONAL_VEHICLE(vehIndex) != pDriver
			AND NOT globalPropertyEntryData.bChoosenToReplaceVehicle
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Not driver's PV, returning FALSE.")
				#ENDIF
				TIDY_UP_BUSINESS_HUB_ENTRY_FLAGS(details)
				RETURN FALSE
			ENDIF
			
			IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehIndex)
			AND GET_OWNER_OF_PERSONAL_VEHICLE(vehIndex) = pDriver
			AND IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
			AND IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
			AND NOT IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID()) 
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Vehicle is being cleaned up not allowed")
				#ENDIF
				TIDY_UP_BUSINESS_HUB_ENTRY_FLAGS(details)
				RETURN FALSE
			ENDIF
			
			IF DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE(vehIndex)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE true, returning false.")
				#ENDIF
				TIDY_UP_BUSINESS_HUB_ENTRY_FLAGS(details)
				RETURN FALSE
			ENDIF
			
			IF IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(vehIndex)
			AND GET_OWNER_OF_PERSONAL_HACKER_TRUCK(vehIndex) != pDriver
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Not driver's Hacker Truck, returning false")
				#ENDIF
				TIDY_UP_BUSINESS_HUB_ENTRY_FLAGS(details)
				RETURN FALSE
			ENDIF
			
			IF IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehIndex)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - FALSE vehicle is an Avenger")
				#ENDIF
				TIDY_UP_BUSINESS_HUB_ENTRY_FLAGS(details)
				RETURN FALSE
			ENDIF
			
			IF IS_VEHICLE_A_PERSONAL_TRUCK(vehIndex)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - FALSE vehicle is an MOC")
				#ENDIF
				TIDY_UP_BUSINESS_HUB_ENTRY_FLAGS(details)
				RETURN FALSE
			ENDIF
			
			IF IS_NET_PLAYER_OK(pDriver)
			AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(GET_ENTITY_MODEL(vehIndex), PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1)
			AND DOES_PLAYER_OWN_BUSINESS_HUB(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriver), eBusinessHub)
				IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
					IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehIndex)
					AND NOT globalPropertyEntryData.bChoosenToReplaceVehicle
						IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
							IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
							OR IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_IMPOUNDED)
							OR IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_JUST_PURCHASED)
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Current PV should not be outside!, returning FALSE")
								#ENDIF
								RETURN FALSE
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Not my current PV, returning FALSE")
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
					
					IF NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE)
					AND NOT GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
					AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
						IF IS_PLAYER_DRIVING_INTO_BUSINESS_HUB(data, vehIndex, GET_ENTITY_MODEL(vehIndex))
						AND (NOT IS_VEHICLE_MODEL(vehIndex, OPPRESSOR2)
						OR (GET_ENTITY_SPEED(PLAYER_PED_ID()) < 15.0
						AND GET_ENTITY_HEIGHT_ABOVE_GROUND(vehIndex) <= 1.0))
							IF IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB(details,vehIndex,!globalPropertyEntryData.bChoosenToReplaceVehicle)
								IF IS_VEHICLE_ON_ALL_WHEELS(vehIndex) 
								OR IS_ENTITY_IN_AIR(vehIndex) // Quick fix for B*4012634, allow enter prompt to display whilst in air.
								OR NOT IS_ENTITY_IN_AIR(vehIndex)
									REINIT_NET_TIMER(details.st_GeneralTimer,TRUE)
								ENDIF
								
								IF HAS_NET_TIMER_STARTED(details.st_GeneralTimer) 
								AND NOT HAS_NET_TIMER_EXPIRED(details.st_GeneralTimer,750,TRUE)
									bOnGroundRecently = TRUE
								ENDIF
								
								IF bOnGroundRecently
								OR (IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
								AND IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID()) AND GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(PLAYER_ID())) = SIMPLE_INTERIOR_TYPE_BUSINESS_HUB)
									IF (ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTRY_BUSINESS_HUB()
									OR (IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID()) AND GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(PLAYER_ID())) = SIMPLE_INTERIOR_TYPE_BUSINESS_HUB))
										IF IS_PLAYER_DRIVING_INTO_BUSINESS_HUB(data, vehIndex, GET_ENTITY_MODEL(vehIndex))
										AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_VEH_STORAGE_FULL)
										AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS4, BS_SIMPLE_INTERIOR_GLOBAL_BS_FOUR_PROCESS_VEHICLE_MENU)
											#IF IS_DEBUG_BUILD
											CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Player is in a Vehicle. Returning TRUE.")
											#ENDIF
											RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)	
											SET_DRIVER_ENTERING_SIMPLE_INTERIOR(TRUE, data.eSimpleInteriorID)
											SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB(details,FALSE)
											KICK_REMAINING_PLAYERS_FROM_BUSINESS_HUB_VEHICLE(vehIndex)
											RETURN TRUE
										ELIF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
										AND IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID()) 
										AND GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(PLAYER_ID())) = SIMPLE_INTERIOR_TYPE_BUSINESS_HUB
											RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
											KICK_REMAINING_PLAYERS_FROM_BUSINESS_HUB_VEHICLE(vehIndex)
											#IF IS_DEBUG_BUILD
											CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Player has triggered entry already. Returning TRUE")
											#ENDIF
											RETURN TRUE
										ELSE
											IF IS_PED_IN_ANY_LAND_VEHICLE(PLAYER_PED_ID())
											AND IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
											AND IS_PLAYER_INSIDE_BUSINESS_HUB_ENTRY_AREA(data)
											AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_VEH_STORAGE_FULL)
												CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Player is driving into a simple interior, returning TRUE")
												SET_DRIVER_ENTERING_SIMPLE_INTERIOR(TRUE, data.eSimpleInteriorID)
												SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB(details,FALSE)
												KICK_REMAINING_PLAYERS_FROM_BUSINESS_HUB_VEHICLE(vehIndex)	
												RETURN TRUE
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - waiting for ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTRY_BUSINESS_HUB")
										#ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Player is in drivers seat and can enter simple interior: ", ENUM_TO_INT(data.eSimpleInteriorID))
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - False IS_SAFE_TO_USE_VEHICLE_BUSINESS_HUB: ")
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							IF GET_ENTITY_SPEED(PLAYER_PED_ID()) >= 15.0
								CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - False too fast")
							ENDIF
							
							IF GET_ENTITY_HEIGHT_ABOVE_GROUND(vehIndex) > 1.0
								CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - False too high")
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_WARP_WITH_DRIVER_INTO_BUSINESS_HUB .....")
					
					MAINTAIN_WARP_WITH_DRIVER_INTO_BUSINESS_HUB(data.eSimpleInteriorID)
					
					IF IS_PASSENGER_ENTERING_WITH_DRIVER(PLAYER_ID())
						RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
						
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
						
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
						
						CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Player as passenger has triggered business hub entry. Returning TRUE")
						
						ENABLE_FORCE_FROM_CAR_FOR_LOCAL_PLAYER()
						
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE - Player doesn't own a business hub. Returning FALSE.")
				#ENDIF
				
				IF NOT bKeepContext
					RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
				ENDIF
				
				IF IS_BIT_SET(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_BS_STARTED_HANGAR_ENTRY)
					SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
				ENDIF
				
				SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB(details, TRUE)
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_SIMPLE_INTERIOR_ENTRANCE_IN_CAR_ONLY(data)
		IF NOT bKeepContext
			RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_BS_STARTED_HANGAR_ENTRY)
		SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
	ENDIF
	
	SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB(details, TRUE)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_BUSINESS_HUB(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID)
	UNUSED_PARAMETER(iEntranceID)
	
	//Prints added for B*url:bugstar:4681633 - remove from dev_ng
	IF playerID <> INVALID_PLAYER_INDEX()

		IF eSimpleInteriorID <> SIMPLE_INTERIOR_INVALID
		
			BUSINESS_HUB_ID eHubID = GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
		
			IF g_SimpleInteriorData.bAcceptedInviteIntoSimpleInterior
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
					CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB g_SimpleInteriorData.bAcceptedInviteIntoSimpleInterior")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(playerID)].sMagnateGangBossData.iMissionToLaunch = FMMC_TYPE_GB_BOSS_DEATHMATCH
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
					CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB FMMC_TYPE_GB_BOSS_DEATHMATCH")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF FREEMODE_DELIVERY_IS_PLAYER_IN_POSSESSION_OF_ANY_DELIVERABLE(PLAYER_ID())
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
					CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB FREEMODE_DELIVERY_IS_PLAYER_IN_POSSESSION_OF_ANY_DELIVERABLE")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
					CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_PHONE_ONSCREEN()
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
					CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB IS_PHONE_ONSCREEN")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_PLAYER_WITH_A_PROSTITUTE()
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
					CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB IS_PLAYER_WITH_A_PROSTITUTE")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_PLAYER_BEAST(PLAYER_ID())
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
					CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB IS_PLAYER_BEAST")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
			#IF FEATURE_GEN9_EXCLUSIVE
			AND NOT SHOULD_ALLOW_NIGHTCLUB_ENTRY_DURING_MISSION()
			#ENDIF
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
					CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB IS_PLAYER_ON_ANY_FM_JOB")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
	
			IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(playerID)
			AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID)
			#IF FEATURE_GEN9_EXCLUSIVE
			AND NOT SHOULD_ALLOW_NIGHTCLUB_ENTRY_DURING_MISSION()
			#ENDIF
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
					CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB IS_PLAYER_CRITICAL_TO_ANY_EVENT AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_SYSTEM_UI_BEING_DISPLAYED()
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
					CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB IS_SYSTEM_UI_BEING_DISPLAYED")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
			#IF FEATURE_GEN9_EXCLUSIVE
			AND NOT SHOULD_ALLOW_NIGHTCLUB_ENTRY_DURING_MISSION()
			#ENDIF
				#IF IS_DEBUG_BUILD
					IF ((GET_FRAME_COUNT() % 20) = 0)
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
						CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB = FALSE IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY() = TRUE")
					ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				PED_INDEX pedTemp = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				
				IF DOES_ENTITY_EXIST(pedTemp)
					PLAYER_INDEX playerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
					
					IF playerTemp != INVALID_PLAYER_INDEX()
					AND IS_NET_PLAYER_OK(playerTemp)
						IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != playerTemp
						AND GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
			#IF FEATURE_GEN9_EXCLUSIVE
			AND NOT SHOULD_ALLOW_NIGHTCLUB_ENTRY_DURING_MISSION()
			#ENDIF
				
				INT iCurrentMission = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())
				
				SWITCH iCurrentMission
					CASE FMMC_TYPE_BIKER_BUY 				
					CASE FMMC_TYPE_BIKER_CONTRABAND_SELL	
					CASE FMMC_TYPE_GB_CONTRABAND_BUY		
					CASE FMMC_TYPE_GB_CONTRABAND_SELL		
					CASE FMMC_TYPE_VEHICLE_EXPORT_BUY		
					CASE FMMC_TYPE_VEHICLE_EXPORT_SELL	
					CASE FMMC_TYPE_GUNRUNNING_BUY
					CASE FMMC_TYPE_GUNRUNNING_SELL
					CASE FMMC_TYPE_SMUGGLER_BUY
					CASE FMMC_TYPE_SMUGGLER_SELL
					CASE FMMC_TYPE_FM_GANGOPS
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
						CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION .. ")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDSWITCH
			ENDIF
			
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID, FALSE)
				IF DOES_PLAYER_OWN_BUSINESS_HUB(GB_GET_THIS_PLAYER_GANG_BOSS(playerID), eHubID)
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
			AND IS_DRIVER_ABLE_TO_ENTER_BUSINESS_HUB(eSimpleInteriorID, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				RETURN TRUE
			ENDIF
			
			IF DOES_PLAYER_OWN_BUSINESS_HUB(playerID, eHubID)
				RETURN TRUE
			ENDIF
			
			// Is there another player's Nightclub that the local player can enter?
			IF DOES_RIVAL_NIGHTCLUB_MENU_HAVE_ANY_OPTIONS(eSimpleInteriorID) 
				RETURN TRUE	
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
				CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB eSimpleInteriorID = SIMPLE_INTERIOR_INVALID")
			ENDIF
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
			CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB  playerID = INVALID_PLAYER_INDEX()")
		ENDIF
	#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCanPlayerEnterResult")
		CDEBUG1LN(DEBUG_EXT_MENU, "CAN_PLAYER_ENTER_BUSINESS_HUB FINAL RETURN FALSE")
	ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_BUSINESS_HUB_IN_VEHICLE(SIMPLE_INTERIORS eSimpleInteriorID, MODEL_NAMES eModel)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN (IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(eModel, PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1)
			OR eModel = GET_HACKER_TRUCK_BIKE_MODEL())
ENDFUNC

FUNC STRING GET_BUSINESS_HUB_REASON_FOR_BLOCKED_ENTRY(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iEntranceUsed)
	UNUSED_PARAMETER(txtExtraString)
	
	IF SHOULD_BLOCK_SIMPLE_INTERIOR_ENTRY_FOR_ON_CALL()
		IF iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
		OR iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
			RETURN "SI_ENTR_BLCK11A"
		ELSE
			RETURN "SI_ENTR_BLCK12A"
		ENDIF
	ENDIF
	
	BUSINESS_HUB_ID eHubID = GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	PLAYER_INDEX playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		INT iCurrentMission = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())
		
		IF PLAYER_ID() <> INVALID_PLAYER_INDEX()
			IF iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
			OR iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				SWITCH iCurrentMission
					CASE FMMC_TYPE_BIKER_BUY 				RETURN "CLUB_BLOCK_A"	// You can't enter this Nightclub while resupplying another business.
					CASE FMMC_TYPE_BIKER_CONTRABAND_SELL	RETURN "CLUB_BLOCK_B"	// You can't enter this Nightclub while selling goods from another business.
					CASE FMMC_TYPE_GB_CONTRABAND_BUY		RETURN "CLUB_BLOCK_C"	// You can't enter this Nightclub while acquiring goods for a warehouse.
					CASE FMMC_TYPE_GB_CONTRABAND_SELL		RETURN "CLUB_BLOCK_D"	// You can't enter this Nightclub while selling goods from a warehouse.
					CASE FMMC_TYPE_VEHICLE_EXPORT_BUY		RETURN "CLUB_BLOCK_E"	// You can't enter this Nightclub while acquiring a vehicle for a warehouse.
					CASE FMMC_TYPE_VEHICLE_EXPORT_SELL		RETURN "CLUB_BLOCK_F"	// You can't enter this Nightclub while selling vehicles from a warehouse.
					CASE FMMC_TYPE_GUNRUNNING_BUY			RETURN "CLUB_BLOCK_G"	// You can't enter this Nightclub while acquiring goods for a bunker.	 
					CASE FMMC_TYPE_GUNRUNNING_SELL			RETURN "CLUB_BLOCK_H"	// You can't enter this Nightclub while selling goods from a bunker.
					CASE FMMC_TYPE_SMUGGLER_BUY				RETURN "CLUB_BLOCK_I"	// You can't enter this Nightclub while acquiring goods for a hangar.
					CASE FMMC_TYPE_SMUGGLER_SELL			RETURN "CLUB_BLOCK_J"	// You can't enter this Nightclub while sellings goods from a hangar.
				ENDSWITCH
			ELSE
				SWITCH iCurrentMission
					CASE FMMC_TYPE_BIKER_BUY 				RETURN "HUB_BLOCK_A"	// You can't enter this Nightclub Warehouse while resupplying another business.
					CASE FMMC_TYPE_BIKER_CONTRABAND_SELL	RETURN "HUB_BLOCK_B"	// You can't enter this Nightclub Warehouse while selling goods from another business.
					CASE FMMC_TYPE_GB_CONTRABAND_BUY		RETURN "HUB_BLOCK_C"	// You can't enter this Nightclub Warehouse while acquiring goods for a warehouse.
					CASE FMMC_TYPE_GB_CONTRABAND_SELL		RETURN "HUB_BLOCK_D"	// You can't enter this Nightclub Warehouse while selling goods from a warehouse.
					CASE FMMC_TYPE_VEHICLE_EXPORT_BUY		RETURN "HUB_BLOCK_E"	// You can't enter this Nightclub Warehouse while acquiring a vehicle for a warehouse.
					CASE FMMC_TYPE_VEHICLE_EXPORT_SELL		RETURN "HUB_BLOCK_F"	// You can't enter this Nightclub Warehouse while selling vehicles from a warehouse.
					CASE FMMC_TYPE_GUNRUNNING_BUY			RETURN "HUB_BLOCK_G"	// You can't enter this Nightclub Warehouse while acquiring goods for a bunker.	 
					CASE FMMC_TYPE_GUNRUNNING_SELL			RETURN "HUB_BLOCK_H"	// You can't enter this Nightclub Warehouse while selling goods from a bunker.
					CASE FMMC_TYPE_SMUGGLER_BUY				RETURN "HUB_BLOCK_I"	// You can't enter this Nightclub Warehouse while acquiring goods for a hangar.
					CASE FMMC_TYPE_SMUGGLER_SELL			RETURN "HUB_BLOCK_J"	// You can't enter this Nightclub Warehouse while sellings goods from a hangar.
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_WITH_A_PROSTITUTE()
		// You cannot enter your Nightclub Warehouse with a prostitute.
		IF iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
		OR iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
			RETURN "CLUB_BLOCK_K"
		ELSE
			RETURN "HUB_BLOCK_K"
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
		// You cannot enter whilst delivering a bounty.
		RETURN "BOUNTY_PROP_BLCK"
	ENDIF
	
	IF IS_PLAYER_BEAST(PLAYER_ID())
		// You can't access your Nightclub Warehouse as the Beast.
		IF iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
		OR iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
			RETURN "CLUB_BLOCK_L"
		ELSE
			RETURN "HUB_BLOCK_L"
		ENDIF
	ENDIF
	
	IF IS_NPC_IN_VEHICLE()
	AND NOT DOES_MEGA_BUSINESS_VARIATION_HAVE_WARP_INTO_NIGHTCLUB(GB_GET_MEGA_BUSINESS_VARIATION_PLAYER_IS_ON(PLAYER_ID()))
	AND NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_BUSINESS_BATTLE(PLAYER_ID())
		// You cannot enter whilst an NPC is in the vehicle.
		RETURN "NPC_BLOCK"
	ENDIF
	
	BOOL bBossOwnsBusinessHub = FALSE
	
	IF playerBoss <> INVALID_PLAYER_INDEX() 
	AND playerBoss <> PLAYER_ID()
	AND DOES_PLAYER_OWN_BUSINESS_HUB(playerBoss, eHubID)
		bBossOwnsBusinessHub = TRUE
	ENDIF
	
	IF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
	AND (DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), eHubID) OR bBossOwnsBusinessHub)
		// You can't access this Nightclub Warehouse whilst wearing the Ballistic Equipment.
		IF iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
		OR iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
			RETURN "CLUB_BLOCK_M"
		ELSE
			RETURN "HUB_BLOCK_M"
		ENDIF
	ENDIF	
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND bBossOwnsBusinessHub
	AND NOT GB_IS_BUSINESS_BATTLES_MISSION(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()))
	AND NOT FREEMODE_DELIVERY_IS_PLAYER_IN_POSSESSION_OF_ANY_DELIVERABLE(PLAYER_ID())
		IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
			// You cannot enter this Nightclub Warehouse whilst in a vehicle.
			IF iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
			OR iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				RETURN "CLUB_BLOCK_S"
			ELSE
				RETURN "HUB_BLOCK_S"
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), eHubID)
	AND NOT GB_IS_PLAYER_ON_FM_GANGOPS_MISSION(PLAYER_ID())
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
			IF NOT (FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_DEAD_DROP)
				// Your Nightclub Warehouse is unavailable whilst being a member of an MC.
				IF iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
				OR iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
//					RETURN "CLUB_BLOCK_N"
				ELSE
					RETURN "HUB_BLOCK_N"
				ENDIF
			ENDIF
		ELSE
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
				IF NOT DOES_PLAYER_OWN_BUSINESS_HUB(playerBoss, eHubID)
					IF DOES_PLAYER_OWN_OFFICE(playerBoss)
						// Your Nightclub Warehouse is unavailable when working as an Associate for a CEO.
						IF iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
						OR iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
//							RETURN "CLUB_BLOCK_O"
						ELSE
							RETURN "HUB_BLOCK_O"
						ENDIF
					ELSE
						// Your Nightclub Warehouse is unavailable when working as a Bodyguard for a VIP.
						IF iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
						OR iEntranceUsed = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
//							RETURN "CLUB_BLOCK_P"
						ELSE
							RETURN "HUB_BLOCK_P"
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			INT iPVSlot
			INT iOwnerStatus
			
			VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			PLAYER_INDEX vehOwner = GET_PLAYER_OWNER_OF_VEHICLE(playerVeh, iOwnerStatus, iPVSlot)
			PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(playerVeh)
			
			IF DOES_ENTITY_EXIST(pedDriver)
				PLAYER_INDEX pDriver = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriver)
				
				IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
					IF NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
						IF IS_VEHICLE_ARMORY_TRUCK(playerVeh)
							RETURN "CLUB_BLOCK_R"
						ELIF IS_SVM_VEHICLE(playerVeh)
						OR IS_VEHICLE_A_CYCLE(GET_ENTITY_MODEL(playerVeh))
						OR NOT IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(GET_ENTITY_MODEL(playerVeh), PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1)
						OR IS_MODEL_A_PERSONAL_TRAILER(GET_ENTITY_MODEL(playerVeh))
						OR IS_THIS_VEHICLE_A_PEGASUS_VEHICLE(playerVeh)
						OR DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE(playerVeh)
						OR (IS_VEHICLE_A_PERSONAL_VEHICLE(playerVeh)
						AND vehOwner != pDriver)
							IF NOT FMBB_DROPOFF_IS_PLAYER_PROPERTY(FREEMODE_DELIVERY_GET_ACTIVE_LOCAL_DROPOFF())
								RETURN "CLUB_BLOCK_Q"
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_BUSINESS_HUB_REASON_FOR_BLOCKED_EXIT(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iExitUsed)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(txtExtraString)
	UNUSED_PARAMETER(iExitUsed)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_BLOCKED_EXITING_PROPERTY_FOR_INTRO()
		RETURN "NCLB_EXIT_BLCK"
	ENDIF
	#ENDIF
	
	IF SHOULD_JOB_ENTRY_TYPE_BLOCK_SIMPLE_INTERIOR_TRANSITION()
		IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			RETURN "SI_EXIT_BLCK12A"
		ELSE
			RETURN "SI_EXIT_BLCK11A"
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL CAN_PLAYER_INVITE_OTHERS_TO_BUSINESS_HUB(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)
	
	IF g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty = -1 
		IF (DOES_PLAYER_OWN_BUSINESS_HUB(playerID, GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)) AND NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE))
		OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER)
			IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID)			
			
				INT iCurrentMission = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID)
				
				SWITCH iCurrentMission
					CASE FMMC_TYPE_BIKER_BUY 				
					CASE FMMC_TYPE_BIKER_CONTRABAND_SELL	
					CASE FMMC_TYPE_GB_CONTRABAND_BUY		
					CASE FMMC_TYPE_GB_CONTRABAND_SELL		
					CASE FMMC_TYPE_VEHICLE_EXPORT_BUY		
					CASE FMMC_TYPE_VEHICLE_EXPORT_SELL	
					CASE FMMC_TYPE_GUNRUNNING_BUY
					CASE FMMC_TYPE_GUNRUNNING_SELL
					CASE FMMC_TYPE_SMUGGLER_BUY
					CASE FMMC_TYPE_SMUGGLER_SELL
					CASE FMMC_TYPE_FM_GANGOPS
					#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][BUSINESS_HUB] CAN_PLAYER_INVITE_OTHERS_TO_BUSINESS_HUB - FALSE, GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION = TRUE")
					#ENDIF
					RETURN FALSE
				ENDSWITCH
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_BE_INVITED_TO_BUSINESS_HUB(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID) 
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF playerID <> INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
	AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(GET_PLAYER_PED(playerID))
	AND NOT FREEMODE_DELIVERY_IS_PLAYER_IN_POSSESSION_OF_ANY_DELIVERABLE(playerID)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_BE_INVITED_TO_BUSINESS_HUB_BY_PLAYER(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX pInvitingPlayer)
	UNUSED_PARAMETER(eSimpleInteriorID)

	IF pInvitingPlayer = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID(), TRUE)
	AND NOT GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), pInvitingPlayer)
		RETURN FALSE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
	AND NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), pInvitingPlayer)
	AND GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(pInvitingPlayer) != ciBUSINESS_HUB_FLOOR_NIGHTCLUB
		PRINTLN("CAN_LOCAL_PLAYER_BE_INVITED_TO_BUSINESS_HUB_BY_PLAYER - PROCESS_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR = FALSE, GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG = FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID())
		PRINTLN("CAN_LOCAL_PLAYER_BE_INVITED_TO_BUSINESS_HUB_BY_PLAYER - PROCESS_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR = FALSE, IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE = TRUE")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		PRINTLN("CAN_LOCAL_PLAYER_BE_INVITED_TO_BUSINESS_HUB_BY_PLAYER - PROCESS_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR = FALSE, IS_ENTITY_IN_AIR = TRUE")
		RETURN FALSE
	ENDIF
	
	GAMER_HANDLE playerHandle		= GET_GAMER_HANDLE_PLAYER(pInvitingPlayer)
	GAMER_HANDLE localPlayerHandle	= GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	INT iLocalCrewID = GET_MP_PLAYER_CLAN_ID(localPlayerHandle)
	
	// If we are not in the same crew.
	IF iLocalCrewID = -1
	OR iLocalCrewID <> GET_MP_PLAYER_CLAN_ID(playerHandle)
		// If we are not friends.
		IF NOT NETWORK_IS_FRIEND(playerHandle)
			// If we are not in the same gang.
			IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), pInvitingPlayer) 
				#IF IS_DEBUG_BUILD
				PRINTLN("CAN_LOCAL_PLAYER_BE_INVITED_TO_BUSINESS_HUB_BY_PLAYER - PROCESS_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR = FALSE BECAUSE not a gang, friend or crew member.")
				PRINTLN("CAN_LOCAL_PLAYER_BE_INVITED_TO_BUSINESS_HUB_BY_PLAYER - inviting player: ", GET_PLAYER_NAME(pInvitingPlayer), " Local player: ", GET_PLAYER_NAME(PLAYER_ID()))
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

PROC SET_BUSINESS_HUB_GANG_STATUS_BS(PLAYER_INDEX piBoss, BOOL bWarpingToPlayersProperty = FALSE)
	
	IF piBoss = PLAYER_ID()
	AND GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
	OR GB_ARE_PLAYERS_IN_SAME_GANG(piBoss, PLAYER_ID())
		
		IF NOT bWarpingToPlayersProperty
			
			IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_BOSS)
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_GANG_STATUS_BS - Entering as the boss and owner.")
				#ENDIF
			ELSE
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_GOON)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_GANG_STATUS_BS - Entering as a goon.")
				#ENDIF
				
				IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
					
					SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_GOON_MC)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_GANG_STATUS_BS - Entering as an MC goon.")
					#ENDIF
					
				ELIF piBoss <> INVALID_PLAYER_INDEX()
				AND DOES_PLAYER_OWN_OFFICE(piBoss)
					
					SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_ASSOCIATE)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_GANG_STATUS_BS - Entering as an associate.")
					#ENDIF
					
				ENDIF
			ENDIF
		ELSE
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_MEMBER_OF_OTHER_GANG)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_GANG_STATUS_BS - Entering as a member of another gang.")
			#ENDIF
		ENDIF
	ELSE
		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_NON_GANG_MEMBER)
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_GANG_STATUS_BS - Entering whilst not a member of a gang.")
		#ENDIF
	ENDIF
ENDPROC

PROC SET_BUSINESS_HUB_ACCESS_BS(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer)
	UNUSED_PARAMETER(iInvitingPlayer)
	
	GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
	GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS = 0
	g_SimpleInteriorData.iAccessBS = 0
	
	PLAYER_INDEX piBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
	
	#IF IS_DEBUG_BUILD
	IF piBoss <> INVALID_PLAYER_INDEX()
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - Boss: ", GET_PLAYER_NAME(piBoss))
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - Boss: INVALID_PLAYER_INDEX")
	ENDIF
	#ENDIF
	
	BOOL bPlayerOwnsThisProperty = DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
	BOOL bIgnoreGangStatus = bPlayerOwnsThisProperty AND IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_FAVOUR_PROPERTY_OWNERSHIP_OVER_GANG_STATUS)
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - bPlayerOwnsThisProperty == ", bPlayerOwnsThisProperty, ", bIgnoreGangStatus == ", bIgnoreGangStatus, "BS4_SIMPLE_INTERIOR_FAVOUR_PROPERTY_OWNERSHIP_OVER_GANG_STATUS")
	
	
	IF g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty <> -1
	AND g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty <> NATIVE_TO_INT(PLAYER_ID())
	
		GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INT_TO_PLAYERINDEX(g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty)
		SET_BUSINESS_HUB_GANG_STATUS_BS(piBoss, TRUE)
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - We want to join owner: ",
			GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty)), " (", g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty, ") ")
		#ENDIF
	ELIF SIMPLE_INTERIOR_IS_LOCAL_PLAYER_ENTERING_WITH_VEH_AS_PASSENGER()
		SET_BUSINESS_HUB_GANG_STATUS_BS(piBoss, TRUE)
		
		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_NON_GANG_MEMBER)
		
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - We want to join owner in vehicle as passenger: ", GET_PLAYER_NAME(g_SimpleInteriorData.driverPlayer))
		
		globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = g_SimpleInteriorData.driverPlayer
	ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT IS_PLAYER_DATA_DOING_INTERIOR_WARP_FROM_CUTSCENE()
		PED_INDEX vehDriverPed = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_DRIVER) 
		PLAYER_INDEX vehDriver = NETWORK_GET_PLAYER_INDEX_FROM_PED(vehDriverPed)
		
		IF vehDriver = PLAYER_ID()
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER)
			
			globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
			
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - We are entering this hub as the owner in a vehicle.")
		ELSE
			CASSERTLN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - Entering in a vehicle but not driving. No owner can be determined. This will cause a black screen")
		ENDIF
	ELIF NOT bIgnoreGangStatus
	AND GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
	AND DOES_PLAYER_OWN_BUSINESS_HUB(piBoss, GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		SET_BUSINESS_HUB_GANG_STATUS_BS(piBoss)
		
		IF piBoss <> INVALID_PLAYER_INDEX()
		
			GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = piBoss
			
			#IF IS_DEBUG_BUILD
			IF piBoss = PLAYER_ID()
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - BOSS - Entering the property as boss and owner.")
			ELSE
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - GOON - Joining the boss or another gang member.")
			ENDIF
			#ENDIF
		ENDIF
	
	ELIF bPlayerOwnsThisProperty
		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER)
		GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
		
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - We are entering this interior as the owner.")
		#ENDIF
	ENDIF
	
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FMBB_SELL
	OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FMBB_CLUB_MANAGEMENT
	OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FMBB_PHONECALL
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - setting SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_RE_ENTERED_ON_MISSION")
		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_RE_ENTERED_ON_MISSION)
	ENDIF
	
	CLEAR_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE()
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] SET_BUSINESS_HUB_ACCESS_BS - Clearing g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty.")	
ENDPROC

PROC TRIGGER_LOCAL_PLAYER_BUSINESS_HUB_ENTRY(SIMPLE_INTERIORS eSimpleInteriorID)
	
	SET_BUSINESS_HUB_ACCESS_BS(eSimpleInteriorID, -1)
	
	// We need to set this manually so DO_SIMPLE_INTERIOR_AUTOWARP will work, 
	// otherwise it will return TRUE straight away as it checks if local player is in the interior, 
	// it's a bit of a hack but a really small, harmless one.
	g_SimpleInteriorData.eCurrentInteriorID = SIMPLE_INTERIOR_INVALID 
			
	SET_SIMPLE_INTERIOR_EVENT_AUTOWARP_ACTIVE(TRUE, eSimpleInteriorID)
	g_SimpleInteriorData.bEventAutowarpOverrideCoords = FALSE
	g_SimpleInteriorData.bEventAutowarpSetInInterior = TRUE
ENDPROC

PROC GET_BUSINESS_HUB_ENTRANCE_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], SIMPLE_INTERIOR_LOCAL_EVENTS &eventOptions[], INT &iOptionsCount)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
		txtTitle = "CLUB_TITLE"
	ELSE
		txtTitle = "HUB_TITLE_2"
	ENDIF
	
	strOptions[0] = "IEWHS_ENTR_ALONE"
	eventOptions[0] = SI_EVENT_ENTRY_MENU_ENTER_ALONE
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
	AND GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
		strOptions[1] = "BWH_ENTRM_GANG"
	ELSE
		strOptions[1] = "PROP_HEI_E_3"
	ENDIF
	
	eventOptions[1] = SI_EVENT_ENTRY_MENU_ENTER_WITH_NEARBY
	
	strOptions[2] = "PROP_HEI_E_2"
	eventOptions[2] = SI_EVENT_ENTRY_MENU_ENTER_WITH_NEARBY
	
	iOptionsCount = 3
ENDPROC 

PROC GET_BUSINESS_HUB_EXIT_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], INT &iOptionsCount, INT iCurrentExit)
	UNUSED_PARAMETER(eSimpleInteriorID)

	IF COUNT_OF(strOptions) <> SIMPLE_INTERIOR_MENU_MAX_OPTIONS
		CASSERTLN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_EXIT_MENU_TITLE_AND_OPTIONS - Exit menu options array out of bounds.")
		EXIT
	ENDIF	
		
	IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
		CDEBUG1LN(DEBUG_EXT_MENU, "[menu_int] GET_BUSINESS_HUB_EXIT_MENU_TITLE_AND_OPTIONS - Using Nightclub exit menu")
		txtTitle = "CLUB_TITLE"		
		strOptions[ciHUB_EXIT_DEFAULT] 	= "CLUB_EXIT"
		strOptions[ciHUB_EXIT_ALL] 		= "CLUB_AEXIT"
		strOptions[ciHUB_EXIT_CLUB]		= "HUB_EXIT_C"
		strOptions[ciHUB_EXIT_F1] 		= "HUB_EXIT_F1"
		strOptions[ciHUB_EXIT_F2] 		= "HUB_EXIT_F2"
		strOptions[ciHUB_EXIT_F3] 		= "HUB_EXIT_F3"
		strOptions[ciHUB_EXIT_F4] 		= "HUB_EXIT_F4"
		strOptions[ciHUB_EXIT_F5_G]		= "HUB_EXIT_F5"
		strOptions[ciHUB_EXIT_F5_S]		= "HUB_EXIT_F5_S"
		iOptionsCount = 9
	ELSE
		CDEBUG1LN(DEBUG_EXT_MENU, "[menu_int] GET_BUSINESS_HUB_EXIT_MENU_TITLE_AND_OPTIONS - Using Warehouse exit menu")
		SWITCH iCurrentExit
			CASE ciBUSINESS_HUB_EXIT_STORAGE_ELEVATOR	txtTitle = "HUB_WH_S" 	BREAK
			CASE ciBUSINESS_HUB_EXIT_GARAGE_ELEVATOR	txtTitle = "HUB_WH_G"	BREAK
			CASE ciBUSINESS_HUB_EXIT_VEHICLE_ELEVATOR 	txtTitle = "HUB_WH_G"	BREAK
		ENDSWITCH
		strOptions[ciHUB_EXIT_DEFAULT] 	= "HUB_EXIT"
		strOptions[ciHUB_EXIT_ALL] 		= "HUB_AEXIT"
		strOptions[ciHUB_EXIT_CLUB]		= "HUB_EXIT_C"
		strOptions[ciHUB_EXIT_F1] 		= "HUB_EXIT_F1"
		strOptions[ciHUB_EXIT_F2] 		= "HUB_EXIT_F2"
		strOptions[ciHUB_EXIT_F3] 		= "HUB_EXIT_F3"
		strOptions[ciHUB_EXIT_F4] 		= "HUB_EXIT_F4"
		strOptions[ciHUB_EXIT_F5_G]		= "HUB_EXIT_F5"
		strOptions[ciHUB_EXIT_F5_S]		= "HUB_EXIT_F5_S"
		iOptionsCount = 9
	ENDIF
	
ENDPROC

ENUM HUB_ENTRY_MENU_TYPE
	HUB_ENTRY_MENU_FLOORS,
	HUB_ENTRY_MENU_DEFAULT
ENDENUM
ENUM CLUB_ENTRY_MENU_TYPE
	CLUB_ENTRY_MENU_MAIN,
	CLUB_ENTRY_MENU_OWNED, 
	CLUB_ENTRY_MENU_RIVAL
ENDENUM

PROC APPLY_BUSINESS_HUB_MENU_SETTINGS()
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
ENDPROC

PROC PROCESS_BUSINESS_HUB_CUSTOM_ENTRANCE_MENU_DEFAULT(SIMPLE_INTERIOR_CUSTOM_MENU_IN_DATA& ref_inData, SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA& ref_outData)

	// There are serveral frames that the menu is hidden for while the 
	// defualt menu is rebuilt, so we should hide help here.
	HIDE_HELP_TEXT_THIS_FRAME()
		
	// Check to see if the player is alone - if they are then we don't want to show the menu at all...
	//
	BOOL bFriendsNearby, bGangNearby
	ARE_PLAYERS_NEAR_PLAYER(ref_inData.eSimpleInteriorID, GET_ENTITY_COORDS(PLAYER_PED_ID()), bFriendsNearby, bGangNearby, HAS_PLAYER_COMPLETED_ALL_NIGHTCLUB_SETUP_MISSIONS(PLAYER_ID()))
	BOOL bAnyNearbyPlayers = bFriendsNearby OR bGangNearby
	
	IF NOT bAnyNearbyPlayers
		CDEBUG1LN(DEBUG_EXT_MENU, " DEFAULT - No players nearby that we can invite - skipping menu")
		ref_outData.bUpdateInteriorDetails = TRUE
		ref_outData.bForceInteriorState = TRUE
		ref_outData.eForcedInteriorState = ENTRANCE_STATE_INT_SCRIPT_LOADING
		POP_MENU_INDEX() //We want to reset menu indices for next use
		
		IF NOT HAS_LOCAL_PLAYER_VIEWED_NIGHTCLUB_INTRO_CUTSCENE()
			ref_outData.bForceRebuildNextFrame = TRUE
			ref_outData.bHideMenuForForceRebuild = TRUE
		ENDIF
		
		EXIT
	ENDIF	
		
	IF ref_inData.bCancelled
	AND NOT ref_inData.bRebuild
		ref_outData.bForceRebuildNextFrame = TRUE
		POP_MENU_INDEX()
		
		IF NOT HAS_LOCAL_PLAYER_VIEWED_NIGHTCLUB_INTRO_CUTSCENE()
			ref_outData.bQuitMenu = TRUE
		ENDIF
	ELSE
		ref_outData.bContinueWithDefaultProcessing = TRUE
		IF ref_inData.bRebuild // We probably just entered, so need to rebuild the menu
			CDEBUG1LN(DEBUG_EXT_MENU, " DEFAULT - We've been asked to rebuild, so setting bForceDefaultMenuRebuild = TRUE")
			ref_outData.bForceDefaultMenuRebuild = TRUE			
			CDEBUG1LN(DEBUG_EXT_MENU, " DEFAULT - GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() == ",GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED())
			IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
				SET_MENU_TITLE("CLUB_TITLE")
			ELSE
				SET_MENU_TITLE("HUB_TITLE_2")
			ENDIF
		ELIF ref_inData.bAccepted			
			ref_outData.bUpdateInteriorDetails = TRUE
			ref_outData.bRequestInteriorLaunchAfterDefaultProcessing = TRUE
			POP_MENU_INDEX() // We want to reset menu indices for next use
		ENDIF
	ENDIF	
ENDPROC

FUNC STRING GET_BUSINESS_HUB_WAREHOUSE_NAME(INT iFloor)
	SWITCH iFloor
		CASE ciBUSINESS_HUB_FLOOR_NIGHTCLUB RETURN "HUB_ENT_F0"
		CASE ciBUSINESS_HUB_FLOOR_B1 RETURN "HUB_ENT_F1"
		CASE ciBUSINESS_HUB_FLOOR_B2 RETURN "HUB_ENT_F2"
		CASE ciBUSINESS_HUB_FLOOR_B3 RETURN "HUB_ENT_F3"
		CASE ciBUSINESS_HUB_FLOOR_B4 RETURN "HUB_ENT_F4"
		CASE ciBUSINESS_HUB_FLOOR_B5 RETURN "HUB_ENT_F5"
		DEFAULT RETURN "UNKNOWN"
	ENDSWITCH
ENDFUNC

FUNC STRING GET_BUSINESS_HUB_WAREHOUSE_HELP_DESC(BOOL bPlayerIsOwner, BOOL bPlayerOwnsThisFloor, INT iFloor)
	IF NOT bPlayerIsOwner
	OR bPlayerOwnsThisFloor
		SWITCH iFloor
			CASE ciBUSINESS_HUB_FLOOR_NIGHTCLUB RETURN "HUB_F0_W_H"
			CASE ciBUSINESS_HUB_FLOOR_B1 RETURN "HUB_F1_W_H"
			CASE ciBUSINESS_HUB_FLOOR_B2 RETURN "HUB_F2_W_H"
			CASE ciBUSINESS_HUB_FLOOR_B3 RETURN "HUB_F3_W_H"
			CASE ciBUSINESS_HUB_FLOOR_B4 RETURN "HUB_F4_W_H"
			CASE ciBUSINESS_HUB_FLOOR_B5 RETURN "HUB_F5_W_H"
			DEFAULT RETURN "UNKNOWN"
		ENDSWITCH
	ELSE//player is owner and we don't own the floor
		IF iFloor = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
		AND GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
			RETURN "HUB_F0_BLCK"
		ENDIF
		RETURN "HUB_HINT_BUY_S"
	ENDIF
ENDFUNC

FUNC STRING GET_BUSINESS_HUB_GARAGE_HELP_DESC(BOOL bPlayerIsOwner, BOOL bPlayerOwnsThisFloor, INT iFloor, BOOL bElevatorHelp = FALSE)
	IF NOT bPlayerIsOwner
	OR bPlayerOwnsThisFloor
		SWITCH iFloor
			CASE ciBUSINESS_HUB_FLOOR_NIGHTCLUB
				IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
					RETURN "HUB_F0_BLCK"
				ELIF bElevatorHelp
					RETURN "HUB_F0_W_H"
				ELSE	
					RETURN "HUB_ENT_F0_H"
				ENDIF
			BREAK
			CASE ciBUSINESS_HUB_FLOOR_B1 RETURN "HUB_ENT_F1_H"
			CASE ciBUSINESS_HUB_FLOOR_B2 RETURN "HUB_ENT_F2_H"
			CASE ciBUSINESS_HUB_FLOOR_B3 RETURN "HUB_ENT_F3_H"
			CASE ciBUSINESS_HUB_FLOOR_B4 RETURN "HUB_ENT_F4_H"
			CASE ciBUSINESS_HUB_FLOOR_B5 RETURN "HUB_ENT_F5_H"
		ENDSWITCH
		
		RETURN "UNKNOWN"
		
	ELSE //player is owner and we don't own the floor
		IF iFloor = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
		AND GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
			RETURN "HUB_F0_BLCK"
		ENDIF
		
		RETURN "HUB_VEH_HELP_2"
	ENDIF
ENDFUNC

CONST_INT ciHUB_GANG_MENU_NIGHTCLUB	0
CONST_INT ciHUB_GANG_MENU_FLOOR		1	

PROC PROCESS_BUSINESS_HUB_CUSTOM_ENTRANCE_MENU_GANG_ENTRY(SIMPLE_INTERIOR_CUSTOM_MENU_IN_DATA& ref_inData, SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA& ref_outData)
	BUSINESS_HUB_ID eHubID = GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(ref_inData.eSimpleInteriorID)

	// There used to be a menu for choosing between basement/nightclub if the boss was in 
	// the basement but that was removed so now just always take the player striaght in.
	PLAYER_INDEX piBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
	IF piBoss <> INVALID_PLAYER_INDEX()
	AND DOES_PLAYER_OWN_BUSINESS_HUB(piBoss, eHubID) 
		CDEBUG1LN(DEBUG_EXT_MENU, " Gang - Warping straight in because i'm in a gang and the boss is either in the club or not in the HUB at all.")
		SET_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE(piBoss)
		SET_BUSINESS_HUB_ACCESS_BS(ref_inData.eSimpleInteriorID, -1)
		ref_outData.bUpdateInteriorDetails = TRUE
		ref_outData.bForceInteriorState = TRUE
		ref_outData.eForcedInteriorState = ENTRANCE_STATE_INT_SCRIPT_LOADING
		ref_outData.bForceRebuildNextFrame = TRUE
		ref_outData.bHideMenuForForceRebuild = TRUE
		EXIT
	ELSE
		CDEBUG1LN(DEBUG_EXT_MENU, " GANG - SHOULD NEVER HIT THIS PRINT")
		CDEBUG1LN(DEBUG_EXT_MENU, " GANG - piBoss == ",	NATIVE_TO_INT(piBoss), " DOES_PLAYER_OWN_BUSINESS_HUB(piBoss, eHubID) == ", DOES_PLAYER_OWN_BUSINESS_HUB(piBoss, eHubID), " Interior is ", GET_BUSINESS_HUB_NAME(ref_inData.eSimpleInteriorID))
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DISABLE_NIGHTCLUB_MENU_OPTION()
	PLAYER_INDEX piBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())	
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT SHOULD_ALLOW_NIGHTCLUB_ENTRY_DURING_MISSION()	
	#ENDIF
		RETURN TRUE
	ENDIF
	
	IF piBoss <> INVALID_PLAYER_INDEX()
		IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(piBoss)
			IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piBoss) = FMMC_TYPE_FMBB_CLUB_MANAGEMENT
			OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piBoss) = FMMC_TYPE_FMBB_PHONECALL
			OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piBoss) = FMMC_TYPE_FMBB_SELL
			OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piBoss) = FMMC_TYPE_FMBB_DEFEND
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_BUSINESS_HUB_CUSTOM_ENTRANCE_MENU_FLOORS(SIMPLE_INTERIOR_CUSTOM_MENU_IN_DATA& ref_inData, SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA& ref_outData)
	BOOL bCreateMenu
	
	BOOL bDisableNightclubOption = SHOULD_DISABLE_NIGHTCLUB_MENU_OPTION()
	BOOL bIsFloorSelectableArray[ciBUSINESS_HUB_FLOOR_COUNT]
	BOOL bPlayerOwnedFloorArray[ciBUSINESS_HUB_FLOOR_COUNT]
	STRING sMenuEntries[ciBUSINESS_HUB_FLOOR_COUNT]
	INT iOptionCount //Number of menu options. Nothing here deals with missing entries mid-menu: Only the hacker truck level (last option) should ever be absent from menu.
	INT i
	
	BUSINESS_HUB_ID eHubID = GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(ref_inData.eSimpleInteriorID)
	
	IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
	AND NOT DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), eHubID)
		CDEBUG1LN(DEBUG_EXT_MENU, "NOT OWNED - Left gang while entering owner's hub, exit menu")
		ref_outData.bQuitMenu = TRUE
		EXIT
	ENDIF
	
	REPEAT ciBUSINESS_HUB_FLOOR_COUNT i
		sMenuEntries[i] = GET_BUSINESS_HUB_WAREHOUSE_NAME(i)
		bPlayerOwnedFloorArray[i] = IS_BUSINESS_HUB_GARAGE_PURCHASED(i)
		
		IF i = ciBUSINESS_HUB_FLOOR_B5
			bIsFloorSelectableArray[i] = bPlayerOwnedFloorArray[i]
			IF bPlayerOwnedFloorArray[i]
				IF NOT g_sMenuData.bIsSelectable[i]
				OR NOT ARE_STRINGS_EQUAL(g_sMenuData.tl15Item[i], sMenuEntries[i])
					CDEBUG1LN(DEBUG_EXT_MENU, " FLOORS - Hacker truck level is purchased but not on the menu -> Force menu rebuild")
					bCreateMenu = TRUE
				ENDIF
				iOptionCount++
			ELSE
				IF g_sMenuData.bIsSelectable[i]
				OR ARE_STRINGS_EQUAL(g_sMenuData.tl15Item[i], sMenuEntries[i])
					CDEBUG1LN(DEBUG_EXT_MENU, " FLOORS - Hacker truck level is not purchased but is on the menu -> Force menu rebuild")
					bCreateMenu = TRUE
				ENDIF
			ENDIF
		ELSE
			bIsFloorSelectableArray[i] = bPlayerOwnedFloorArray[i] AND NOT (i = ciBUSINESS_HUB_FLOOR_NIGHTCLUB AND bDisableNightclubOption) 
			IF NOT bCreateMenu
			AND (g_sMenuData.bIsSelectable[i] <> bIsFloorSelectableArray[i]
			OR NOT ARE_STRINGS_EQUAL(g_sMenuData.tl15Item[i], sMenuEntries[i]))
				CDEBUG1LN(DEBUG_EXT_MENU, " FLOORS - Menu option [",i,"] string '",g_sMenuData.tl15Item[i] , "' <> '", sMenuEntries[i],"' OR option selectability isn't correct -> Force menu rebuild ")
				bCreateMenu = TRUE
				// Do not breakloop - we need to check all floors for selectability.
			ENDIF
			iOptionCount++
		ENDIF
	ENDREPEAT
	
	IF NOT HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(PLAYER_ID(), MBV_COLLECT_STAFF)
	AND NOT HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(PLAYER_ID(), MBV_COLLECT_EQUIPMENT)
	AND NOT HAS_LOCAL_PLAYER_VIEWED_NIGHTCLUB_INTRO_CUTSCENE()
		SET_INTERIOR_FLOOR_INDEX(0)
		CDEBUG1LN(DEBUG_EXT_MENU, " FLOORS - Set floor index to 0 (Nightclub) Forced entry due to not having watched the intro.")
			
		// If we are entering as owner...		
		ref_outData.bForceRebuildNextFrame = TRUE
		ref_outData.bHideMenuForForceRebuild = TRUE
		PUSH_MENU_INDEX(ENUM_TO_INT(HUB_ENTRY_MENU_DEFAULT))	
		CDEBUG1LN(DEBUG_EXT_MENU, " FLOORS - Pushing menu index [", ENUM_TO_INT(HUB_ENTRY_MENU_DEFAULT) , "]. Forcing rebuild next frame. Forced entry due to not having watched the intro.")
	ELIF ref_inData.bRebuild
	OR bCreateMenu
		CDEBUG1LN(DEBUG_EXT_MENU, " FLOORS - rebuilding player menu")
		CLEAR_MENU_DATA(DEFAULT, TRUE)
		SET_MENU_TITLE("HUB_TITLE_2")
		APPLY_BUSINESS_HUB_MENU_SETTINGS()
		
		REPEAT iOptionCount i
			ADD_MENU_ITEM_TEXT(i, sMenuEntries[i], 0, bIsFloorSelectableArray[i])
		ENDREPEAT
	
		SET_CURRENT_MENU_ITEM_WITH_CLAMP(0)				
	ELSE
		INT iMenuOption = ref_inData.iCurrentItem
		// Only an owner should see this menu, so pass TRUE to GET_BUSINESS_HUB_GARAGE_HELP_DESC bPlayerIsOwner
		SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_BUSINESS_HUB_GARAGE_HELP_DESC(TRUE, bPlayerOwnedFloorArray[iMenuOption], iMenuOption))
		
		IF ref_inData.bAccepted
		AND bIsFloorSelectableArray[iMenuOption]
			SET_INTERIOR_FLOOR_INDEX(iMenuOption)
			CDEBUG1LN(DEBUG_EXT_MENU, " FLOORS - Set floor index to ", GET_INTERIOR_FLOOR_INDEX())
				
			// If we are entering as owner...		
			ref_outData.bForceRebuildNextFrame = TRUE
			PUSH_MENU_INDEX(ENUM_TO_INT(HUB_ENTRY_MENU_DEFAULT))	
			CDEBUG1LN(DEBUG_EXT_MENU, " FLOORS - Pushing menu index [", ENUM_TO_INT(HUB_ENTRY_MENU_DEFAULT) , "]. Forcing rebuild next frame.")
		ELIF ref_inData.bCancelled
			CDEBUG1LN(DEBUG_EXT_MENU, " FLOORS - Cancelled from top level menu. Closing menu.")
			ref_outData.bQuitMenu = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC APPLY_NIGHTCLUB_MENU_SETTINGS()
	SET_MENU_TITLE("CLUB_TITLE")
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
ENDPROC

PROC PROCESS_NIGHTCLUB_CUSTOM_ENTRANCE_MENU_MAIN( SIMPLE_INTERIOR_CUSTOM_MENU_IN_DATA& ref_inData, SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA& ref_outData )
		
	BOOL bDoesRivalMenuHaveOptions = DOES_RIVAL_NIGHTCLUB_MENU_HAVE_ANY_OPTIONS(ref_inData.eSimpleInteriorID)
	BOOL bCreateMenu = ref_inData.bRebuild OR NOT ARE_STRINGS_EQUAL(g_sMenuData.tl15Item[0], "CLUB_ENT_OWNED")
	BOOL bMenuHasRivalOption = ARE_STRINGS_EQUAL(g_sMenuData.tl15Item[1], "CLUB_ENT_RIVAL")
	NIGHTCLUB_ID eNightclubID = GET_NIGHTCLUB_ID_FROM_SIMPLE_INTERIOR_ID(ref_inData.eSimpleInteriorID)

	IF bDoesRivalMenuHaveOptions
	AND NOT bMenuHasRivalOption
		CDEBUG1LN(DEBUG_EXT_MENU, " MAIN - Should have rival option but not present, bCreateMenu = TRUE")
		bCreateMenu = TRUE
	ELIF NOT bDoesRivalMenuHaveOptions
	AND bMenuHasRivalOption
		CDEBUG1LN(DEBUG_EXT_MENU, " MAIN - Should not have rival option but it is present, bCreateMenu = TRUE")
		bCreateMenu = TRUE
	ENDIF
	
	// If we don't own this nightclub we should jump to the "Rival" submenu
	IF GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.eNightclubID <> eNightclubID
	AND bDoesRivalMenuHaveOptions // This should always be true if we have got here...
		CDEBUG1LN(DEBUG_EXT_MENU, " MAIN - Not owner of a club here, skipping to Rival menu.")
		PUSH_MENU_INDEX( ENUM_TO_INT( CLUB_ENTRY_MENU_RIVAL ) )
		ref_outData.bForceRebuildNextFrame = TRUE
		ref_outData.bHideMenuForForceRebuild = TRUE
		EXIT
	ENDIF
	
	// If we are the owner and there are no rival clubs here 
	// then we should go straight to the next menu
	IF NOT bDoesRivalMenuHaveOptions
		CDEBUG1LN(DEBUG_EXT_MENU, " MAIN - No rival options available, skipping this menu.")
		PUSH_MENU_INDEX( ENUM_TO_INT( CLUB_ENTRY_MENU_OWNED ) )
		ref_outData.bForceRebuildNextFrame = TRUE
		ref_outData.bHideMenuForForceRebuild = TRUE
		EXIT
	ENDIF		
			
	IF bCreateMenu
		CDEBUG1LN(DEBUG_EXT_MENU, " MAIN - rebuilding player menu")
		CLEAR_MENU_DATA()
		APPLY_NIGHTCLUB_MENU_SETTINGS()
		
		ADD_MENU_ITEM_TEXT(0, "CLUB_ENT_OWNED")
		IF bDoesRivalMenuHaveOptions
			ADD_MENU_ITEM_TEXT(1, "CLUB_ENT_RIVAL")
		ENDIF
		SET_CURRENT_MENU_ITEM_WITH_CLAMP(0)				
	ELSE
		IF ref_inData.bAccepted
			SWITCH ref_inData.iCurrentItem 
				CASE 0 PUSH_MENU_INDEX( ENUM_TO_INT( CLUB_ENTRY_MENU_OWNED ) )	BREAK
				CASE 1 PUSH_MENU_INDEX( ENUM_TO_INT( CLUB_ENTRY_MENU_RIVAL ) )	BREAK
			ENDSWITCH
			ref_outData.bForceRebuildNextFrame = TRUE
			CDEBUG1LN(DEBUG_EXT_MENU, " Pushing menu index [", ENUM_TO_INT(CLUB_ENTRY_MENU_OWNED) , "]. Forcing rebuild next frame.")
		ELIF ref_inData.bCancelled
			CDEBUG1LN(DEBUG_EXT_MENU, " Cancelled from top level menu. Closing menu.")
			ref_outData.bQuitMenu = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_NIGHTCLUB_CUSTOM_ENTRANCE_MENU_OWNED( SIMPLE_INTERIOR_CUSTOM_MENU_IN_DATA& ref_inData, SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA& ref_outData )
	UNUSED_PARAMETER(ref_inData)
	
	// There are serveral frames that the menu is hidden for while the 
	// defualt menu is rebuilt, so we should hide help here.
	HIDE_HELP_TEXT_THIS_FRAME()
		
	// Check to see if the player is alone - if they are then we don't want to show the menu at all...
	//
	BOOL bFriendsNearby, bGangNearby
	ARE_PLAYERS_NEAR_PLAYER(ref_inData.eSimpleInteriorID, GET_ENTITY_COORDS(PLAYER_PED_ID()), bFriendsNearby, bGangNearby)
	BOOL bAnyNearbyPlayers = bFriendsNearby OR bGangNearby
	
	IF NOT bAnyNearbyPlayers
		CDEBUG1LN(DEBUG_EXT_MENU, " OWNED - No players nearby that we can invite - skipping menu")
		ref_outData.bUpdateInteriorDetails = TRUE
		ref_outData.bForceInteriorState = TRUE
		ref_outData.bHideMenuForForceRebuild = TRUE
		ref_outData.bForceRebuildNextFrame = TRUE
		ref_outData.eForcedInteriorState = ENTRANCE_STATE_INT_SCRIPT_LOADING
		CDEBUG1LN(DEBUG_EXT_MENU, " OWNED - BS4_SIMPLE_INTERIOR_FAVOUR_PROPERTY_OWNERSHIP_OVER_GANG_STATUS [SET]")
		SET_BIT(g_SimpleInteriorData.iForthBS,BS4_SIMPLE_INTERIOR_FAVOUR_PROPERTY_OWNERSHIP_OVER_GANG_STATUS)		
		POP_MENU_INDEX() //We want to reset menu indices for next use
		EXIT
	ENDIF	
		
	IF ref_inData.bCancelled
	AND NOT ref_inData.bRebuild
		POP_MENU_INDEX()
		// Is there another player's Nightclub that the local player can enter?
		IF DOES_RIVAL_NIGHTCLUB_MENU_HAVE_ANY_OPTIONS(ref_inData.eSimpleInteriorID)
			ref_outData.bForceRebuildNextFrame = TRUE
		ELSE
			ref_outData.bQuitMenu = TRUE
		ENDIF
	ELSE
		ref_outData.bContinueWithDefaultProcessing = TRUE
		IF ref_inData.bRebuild // We probably just entered, so need to rebuild the menu
			CDEBUG1LN(DEBUG_EXT_MENU, " OWNED - We've been asked to rebuild, so setting bForceDefaultMenuRebuild = TRUE")
			ref_outData.bForceDefaultMenuRebuild = TRUE
			CDEBUG1LN(DEBUG_EXT_MENU, " OWNED - GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() == ",GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED())
			IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
				SET_MENU_TITLE("CLUB_TITLE")
			ELSE
				SET_MENU_TITLE("HUB_TITLE_2")
			ENDIF
		ELIF ref_inData.bAccepted
			ref_outData.bRequestInteriorLaunchAfterDefaultProcessing = TRUE
			// If we enter through the buzzer we ignore gang status and go into our own property...
			CDEBUG1LN(DEBUG_EXT_MENU, " OWNED - BS4_SIMPLE_INTERIOR_FAVOUR_PROPERTY_OWNERSHIP_OVER_GANG_STATUS [SET]")
			SET_BIT(g_SimpleInteriorData.iForthBS,BS4_SIMPLE_INTERIOR_FAVOUR_PROPERTY_OWNERSHIP_OVER_GANG_STATUS)
			SET_BUSINESS_HUB_ACCESS_BS(ref_inData.eSimpleInteriorID, -1)
			POP_MENU_INDEX() //We want to reset menu indices for next use
		ENDIF
	ENDIF	
ENDPROC

PROC BEGIN_WARP_TO_REMOTE_PLAYER_CLUB(PLAYER_INDEX pOwner)
	CDEBUG1LN(DEBUG_EXT_MENU, "BEGIN_WARP_TO_REMOTE_PLAYER_CLUB -> pOwner == ", NATIVE_TO_INT(pOwner))
	
	SET_BIT(g_simpleInteriorData.iThirdBS, BS3_SIMPLE_INTERIOR_GLOBAL_DATA_PUBLIC_ENTRY)
	SET_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE(pOwner)
	SET_BUSINESS_HUB_ACCESS_BS(NIGHTCLUB_TO_SIMPLE_INTERIORS(GET_PLAYERS_OWNED_NIGHTCLUB(pOwner)), -1)
	
	g_SimpleInteriorData.eCurrentInteriorID = SIMPLE_INTERIOR_INVALID
	SET_SIMPLE_INTERIOR_EVENT_AUTOWARP_ACTIVE(TRUE, NIGHTCLUB_TO_SIMPLE_INTERIORS(GET_PLAYERS_OWNED_NIGHTCLUB(pOwner)))
	g_SimpleInteriorData.bEventAutowarpOverrideCoords = FALSE
	g_SimpleInteriorData.bEventAutowarpSetInInterior = TRUE	
	
	SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_WARPING_VIA_ENTER_WITH_NEARBY_PLAYER)
	
ENDPROC

PROC PROCESS_NIGHTCLUB_CUSTOM_ENTRANCE_MENU_RIVAL( SIMPLE_INTERIOR_CUSTOM_MENU_IN_DATA& ref_inData, SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA& ref_outData )
	// First we have to validate whether we need to update the menu by checking...
	// ...the order of stored handle hashes against ones we come accross this frame. ...
	// ...This also updates the stored handle hashes so must happen even if ref_inData.bRebuildMenu = TRUE.
	//
	BOOL bUpdateMenu				// True if current menu is invalid and we need to rebuild.
	INT iHandleHashMenuIndex		// Used to determine pCurrentItemPlayer
	PLAYER_INDEX pCurrentItemPlayer // The player assoicated with iCurrentItem. 
	BOOL bIsAnyRivalOptionAvailable	// Does this menu have any options?
	BOOL bIsOptionSelectable[NUM_NETWORK_PLAYERS] // Should the option be selectable?
	
	NIGHTCLUB_ID eNightClubID = GET_NIGHTCLUB_ID_FROM_SIMPLE_INTERIOR_ID(ref_inData.eSimpleInteriorID)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		INT iHandleHash
		PLAYER_INDEX player = INT_TO_NATIVE(PLAYER_INDEX, i)
		
		IF NOT SHOULD_ADD_NIGHTCLUB_TO_MENU(player, eNightClubID)
			IF g_SimpleInteriorData.iStoredGamerHandleHashForMenu[i] <> 0
				g_SimpleInteriorData.iStoredGamerHandleHashForMenu[i] = 0
				CDEBUG1LN(DEBUG_EXT_MENU, " RIVAL - NOT SHOULD_ADD_NIGHTCLUB_TO_MENU")
				bUpdateMenu = TRUE
			ENDIF
			RELOOP
		ENDIF
			
		iHandleHash = NETWORK_HASH_FROM_PLAYER_HANDLE(player)
		bIsAnyRivalOptionAvailable = TRUE
		IF g_SimpleInteriorData.iStoredGamerHandleHashForMenu[i] <> iHandleHash
			g_SimpleInteriorData.iStoredGamerHandleHashForMenu[i] = iHandleHash
			CDEBUG1LN(DEBUG_EXT_MENU, " RIVAL - g_SimpleInteriorData.iStoredGamerHandleHashForMenu[i] <> iHandleHash")
			bUpdateMenu = TRUE
		ENDIF
	
		IF IS_KICKED_FROM_NIGHTCLUB(player)
		AND (NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID()) OR GB_GET_LOCAL_PLAYER_GANG_BOSS() != player)
			bIsOptionSelectable[i] = FALSE
		ELSE
			bIsOptionSelectable[i] = TRUE
		ENDIF	
		
		IF bIsOptionSelectable[i] <> IS_MENU_ROW_SELECTABLE(iHandleHashMenuIndex)
			CDEBUG1LN(DEBUG_EXT_MENU, " RIVAL - bIsOptionSelectable[",i,"] (",bIsOptionSelectable[i],") <> IS_MENU_ROW_SELECTABLE(",iHandleHashMenuIndex,") (",IS_MENU_ROW_SELECTABLE(iHandleHashMenuIndex),")")
			bUpdateMenu = TRUE
		ENDIF
	
		IF iHandleHashMenuIndex = ref_inData.iCurrentItem
			pCurrentItemPlayer = player
		ENDIF
		++iHandleHashMenuIndex
	ENDREPEAT
	
	//If the menu has no options...
	//
	IF NOT bIsAnyRivalOptionAvailable
		IF GET_SIMPLE_INTERIOR_ID_FROM_NIGHTCLUB_ID(GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.eNightclubID) <> ref_inData.eSimpleInteriorID
			ref_outData.bQuitMenu = TRUE
			CDEBUG1LN(DEBUG_EXT_MENU, " RIVAL - Quitting menu from Rival submenu because no rival options are available and we don't a club here.")
		ELSE
			CDEBUG1LN(DEBUG_EXT_MENU, " RIVAL - No options available, popping menu")
			POP_MENU_INDEX()
			ref_outData.bForceRebuildNextFrame = TRUE
			EXIT
		ENDIF
	ENDIF	
	
	// Update menu or accept input based on validation we just did.
	//
	IF bUpdateMenu OR ref_inData.bRebuild
					
		CDEBUG1LN(DEBUG_EXT_MENU, " RIVAL - rebuilding player menu")
		CLEAR_MENU_DATA()
		APPLY_NIGHTCLUB_MENU_SETTINGS()
		
		LANGUAGE_TYPE eLang = GET_CURRENT_LANGUAGE()
		BOOL bUseCondensedText = eLang <> LANGUAGE_CHINESE_SIMPLIFIED AND eLang <> LANGUAGE_CHINESE AND eLang <> LANGUAGE_KOREAN AND eLang <> LANGUAGE_JAPANESE
				
		// This loop must follow same indexing pattern & rules as validation step at top of this func.
		//
		INT iMenuIndex 
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX player = INT_TO_NATIVE(PLAYER_INDEX, i)
			
			IF NOT SHOULD_ADD_NIGHTCLUB_TO_MENU(player, eNightClubID)
				RELOOP
			ENDIF
						
			ADD_MENU_ITEM_TEXT(iMenuIndex, "CLUB_OWNER_X", 1, bIsOptionSelectable[i]) //CLUB_OWNER label doesn't work?
			TEXT_LABEL_63 playerName = GET_PLAYER_NAME(player)
			IF bUseCondensedText 
				ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(playerName)		
			ELSE
				ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(playerName)		
			ENDIF
			CDEBUG1LN(DEBUG_EXT_MENU, " RIVAL - Player ", native_to_int(player), "'s Nightclub name = " , playerName, "'s Nightclub. Selectable = ", bIsOptionSelectable[i], " Menu idx = ", iMenuIndex)
						
			++iMenuIndex
		ENDREPEAT
		INT iMenuItem = PICK_INT(ref_inData.bRebuild, 0, ref_inData.iCurrentItem)
		SET_CURRENT_MENU_ITEM_WITH_CLAMP(iMenuItem)	
	
	ELSE 
		INT iPlayerIndex = NATIVE_TO_INT(pCurrentItemPlayer)
				
		// Add description explaining block
		IF iPlayerIndex > -1
		AND NOT bIsOptionSelectable[iPlayerIndex]
			CDEBUG1LN(DEBUG_EXT_MENU, " RIVAL - Adding block description to current item (", ref_inData.iCurrentItem,") - player ", GET_PLAYER_NAME(pCurrentItemPlayer), " (",iPlayerIndex,")")
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CLUB_KICK_L_D")
		ENDIF
	
		// We are permitted to accept input.
		//
		IF ref_inData.bAccepted
		AND iPlayerIndex > -1
		AND bIsOptionSelectable[iPlayerIndex]
			BEGIN_WARP_TO_REMOTE_PLAYER_CLUB(pCurrentItemPlayer)
			ref_outData.bForceInteriorState = TRUE
			ref_outData.eForcedInteriorState = ENTRANCE_STATE_INT_SCRIPT_LOADING
			ref_outData.bSetupAccessBS = FALSE
			POP_MENU_INDEX()
			
		ELIF ref_inData.bCancelled
			POP_MENU_INDEX()
			// If we don't own a Nightclub here...
			IF GET_SIMPLE_INTERIOR_ID_FROM_NIGHTCLUB_ID(GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.eNightclubID) <> ref_inData.eSimpleInteriorID
				ref_outData.bQuitMenu = TRUE
				CDEBUG1LN(DEBUG_EXT_MENU, " RIVAL - Quitting menu from Rival submenu because we don't a club here.")
			ENDIF
			
			ref_outData.bForceRebuildNextFrame = TRUE
		ENDIF		
	ENDIF
ENDPROC

PROC PROCESS_NIGHTCLUB_CUSTOM_ENTRANCE_MENU_DOOR_ENTRY( SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA& ref_outData )
 	
	// B* 4649500 - The door, as opposed to the buzzer, menu should just let you walk straight in 
	CDEBUG1LN(DEBUG_EXT_MENU, " DOOR - Entered through the door locate (not using buzzer) so not showing any menu")
	ref_outData.bUpdateInteriorDetails = TRUE
	ref_outData.bForceInteriorState = TRUE
	ref_outData.eForcedInteriorState = ENTRANCE_STATE_INT_SCRIPT_LOADING
	ref_outData.bForceRebuildNextFrame = TRUE
	ref_outData.bHideMenuForForceRebuild = TRUE
ENDPROC

/// PURPOSE:
///    Process the menu for the Nightclub entrance.
PROC PROCESS_NIGHTCLUB_CUSTOM_ENTRANCE_MENU(SIMPLE_INTERIOR_CUSTOM_MENU_IN_DATA& ref_inData, SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA& ref_outData)
	CLUB_ENTRY_MENU_TYPE eMenu = INT_TO_ENUM(CLUB_ENTRY_MENU_TYPE, PEEK_MENU_INDEX())
	SWITCH eMenu
		CASE CLUB_ENTRY_MENU_MAIN	PROCESS_NIGHTCLUB_CUSTOM_ENTRANCE_MENU_MAIN(ref_inData, ref_outData)	BREAK
		CASE CLUB_ENTRY_MENU_OWNED	PROCESS_NIGHTCLUB_CUSTOM_ENTRANCE_MENU_OWNED(ref_inData, ref_outData)	BREAK
		CASE CLUB_ENTRY_MENU_RIVAL	PROCESS_NIGHTCLUB_CUSTOM_ENTRANCE_MENU_RIVAL(ref_inData, ref_outData)	BREAK
		DEFAULT CDEBUG1LN(DEBUG_EXT_MENU, "PROCESS_NIGHTCLUB_CUSTOM_ENTRANCE_MENU -> Invalid menu index : ", PEEK_MENU_INDEX()) BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Process the menu for the Warehouse entrance.
PROC PROCESS_WAREHOUSE_CUSTOM_ENTRANCE_MENU(SIMPLE_INTERIOR_CUSTOM_MENU_IN_DATA& ref_inData, SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA& ref_outData)
	HUB_ENTRY_MENU_TYPE eMenu = INT_TO_ENUM(HUB_ENTRY_MENU_TYPE, PEEK_MENU_INDEX())
	SWITCH eMenu
		CASE HUB_ENTRY_MENU_FLOORS	PROCESS_BUSINESS_HUB_CUSTOM_ENTRANCE_MENU_FLOORS(ref_inData, ref_outData)	BREAK
		CASE HUB_ENTRY_MENU_DEFAULT	PROCESS_BUSINESS_HUB_CUSTOM_ENTRANCE_MENU_DEFAULT(ref_inData, ref_outData)	BREAK
		DEFAULT CDEBUG1LN(DEBUG_EXT_MENU, "PROCESS_WAREHOUSE_CUSTOM_ENTRANCE_MENU -> Invalid menu index : ", PEEK_MENU_INDEX()) BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    This function chooses which custom entrance menu to process. 
PROC PROCESS_BUSINESS_HUB_CUSTOM_ENTRANCE_MENU(SIMPLE_INTERIOR_CUSTOM_MENU_IN_DATA& ref_inData, SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA& ref_outData)	
	IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
		PROCESS_NIGHTCLUB_CUSTOM_ENTRANCE_MENU(ref_inData, ref_outData)
	ELIF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE) 
			PROCESS_BUSINESS_HUB_CUSTOM_ENTRANCE_MENU_GANG_ENTRY(ref_inData, ref_outData)
		ELSE
			PROCESS_WAREHOUSE_CUSTOM_ENTRANCE_MENU(ref_inData, ref_outData)
		ENDIF		
	ELIF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			PROCESS_BUSINESS_HUB_CUSTOM_ENTRANCE_MENU_GANG_ENTRY(ref_inData, ref_outData)
		ELSE
			PROCESS_NIGHTCLUB_CUSTOM_ENTRANCE_MENU_DOOR_ENTRY(ref_outData)
		ENDIF
	#IF IS_DEBUG_BUILD	
	ELSE
		CDEBUG1LN(DEBUG_EXT_MENU, "PROCESS_WAREHOUSE_CUSTOM_ENTRANCE_MENU -> GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(): ", GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(), " member of a gang: ",GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE))	
	#ENDIF //IS_DEBUG_BUILD
	ENDIF
ENDPROC

ENUM ELEVATOR_TYPE
	ELT_INVALID,
	ELT_STORAGE,
	ELT_GARAGE,
	ELT_VEHICLE
ENDENUM

STRUCT ELEVATOR_MAP
	ELEVATOR_TYPE eFloorExitElevator[ciBUSINESS_HUB_FLOOR_COUNT]
ENDSTRUCT

FUNC BOOL IS_ELEVATOR_OWNED(ELEVATOR_TYPE eType, INT iFloor, PLAYER_INDEX piOwner)
	
	// Disable access to Terrorbyte _garage_ with tunable (let B5 storage continue) 
	IF g_sMPTunables.bBB_WAREHOUSE_DISABLE_ENTRANCE_TO_TERRORBYTE_GARAGE
	AND iFloor = ciBUSINESS_HUB_FLOOR_B5
	AND (eType = ELT_GARAGE OR eType = ELT_VEHICLE)
		RETURN FALSE
	ENDIF
	
	SWITCH eType
		CASE ELT_STORAGE 	RETURN IS_PLAYER_BUSINESS_HUB_WAREHOUSE_PURCHASED_BY_FLOOR_ID(piOwner, iFloor)
		CASE ELT_GARAGE 	RETURN IS_PLAYER_BUSINESS_HUB_GARAGE_PURCHASED_BY_FLOOR_ID(iFloor, piOwner)
		CASE ELT_VEHICLE	RETURN IS_PLAYER_BUSINESS_HUB_GARAGE_PURCHASED_BY_FLOOR_ID(iFloor, piOwner)
		DEFAULT 			RETURN FALSE
	ENDSWITCH
ENDFUNC

FUNC STRING GET_CLUB_ELEVATOR_BLOCKED_REASON()
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
		RETURN "CLUB_ELE_BLCK"
	ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		RETURN "CLUB_ELE_BLCK1"
	ENDIF
	RETURN "ELE_BLCK_DEF"
ENDFUNC

/// PURPOSE:
///    Generate a map of which elevator exit to use given the floors available and the elevator we are using.
PROC GENERATE_EXIT_MAP_FOR_ELEVATOR(ELEVATOR_TYPE ePrimaryExit, PLAYER_INDEX piOwner, ELEVATOR_MAP& ref_map)	
	INT iCurrentFloorIndex = GET_INTERIOR_FLOOR_INDEX()
	
	// Initialise array in case the ref_map is not clean
	ELEVATOR_MAP newMap
	ref_map = newMap
		
	// Vehicle elevator doesn't use a secondary exit
	ELEVATOR_TYPE eSecondaryExit = ELT_INVALID
	
	IF ePrimaryExit = ELT_STORAGE
		eSecondaryExit = ELT_GARAGE
	ELIF ePrimaryExit = ELT_GARAGE
		eSecondaryExit = ELT_STORAGE
	ENDIF
	
	INT i
	IF piOwner = PLAYER_ID()
		
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
		AND iCurrentFloorIndex = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			//We are in another players gang so block transition into the hub
			SET_BIT(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_BLOCKED_ELEVATOR_MENU_HELP_ON_DISPLAY)
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintExitMap")
					PRINTLN("[SIMPLE_INTERIOR][BUSINESS_HUB] GENERATE_EXIT_MAP_FOR_ELEVATOR - Block the exit menu for gang membership")
				ENDIF
			#ENDIF
			EXIT
		ELSE
			IF IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_BLOCKED_ELEVATOR_MENU_HELP_ON_DISPLAY)
				CLEAR_HELP()
				CLEAR_BIT(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_BLOCKED_ELEVATOR_MENU_HELP_ON_DISPLAY)
			ENDIF
		ENDIF
			
		REPEAT ciBUSINESS_HUB_FLOOR_COUNT i			
			IF i = iCurrentFloorIndex
				RELOOP
			ENDIF
			
			IF i = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			AND iCurrentFloorIndex != ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			AND GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
				//Block access to the nightclub whilst critical to a job
				RELOOP
			ENDIF
			
			IF IS_ELEVATOR_OWNED(ePrimaryExit, i, piOwner)
				ref_map.eFloorExitElevator[i] = ePrimaryExit
			ELIF IS_ELEVATOR_OWNED(eSecondaryExit, i, piOwner)
				ref_map.eFloorExitElevator[i] = eSecondaryExit
			ENDIF
		ENDREPEAT
	ELSE // if i'm not the owner..
		INT iFloorOwnerIsOn
		
		IF piOwner = INVALID_PLAYER_INDEX()
			PRINTLN("[SIMPLE_INTERIOR][BUSINESS_HUB] GENERATE_EXIT_MAP_FOR_ELEVATOR - piOwner is invalid but we are not the owner...")
			EXIT
		ENDIF
		
		IF GlobalplayerBD[NATIVE_TO_INT(piOwner)].SimpleInteriorBD.propertyOwner = piOwner
			IF IS_SIMPLE_INTERIOR_A_BUSINESS_HUB(GlobalplayerBD[NATIVE_TO_INT(piOwner)].SimpleInteriorBD.eCurrentSimpleInterior)  
				iFloorOwnerIsOn = GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(piOwner)
			ELIF IS_PLAYER_IN_HACKER_TRUCK(piOwner)
			AND IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(piOwner)
				iFloorOwnerIsOn	= ciBUSINESS_HUB_FLOOR_B5
			ELSE 
				iFloorOwnerIsOn = -1
			ENDIF
		ELIF
			iFloorOwnerIsOn = -1
		ENDIF
		
		IF GET_INTERIOR_FLOOR_INDEX() != ciBUSINESS_HUB_FLOOR_NIGHTCLUB
		AND GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
			//Block access to the nightclub whilst critical to a job
			EXIT
		ENDIF
		
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		AND NOT GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), piOwner)
		AND GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB							
			SET_BIT(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_BLOCKED_ELEVATOR_MENU_HELP_ON_DISPLAY)
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintExitMap")
					PRINTLN("[SIMPLE_INTERIOR][BUSINESS_HUB] GENERATE_EXIT_MAP_FOR_ELEVATOR - Block the exit menu for gang membership")
				ENDIF
			#ENDIF
			EXIT
		ELIF IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_BLOCKED_ELEVATOR_MENU_HELP_ON_DISPLAY)
			IF IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_BLOCKED_ELEVATOR_MENU_HELP_ON_DISPLAY)
				CLEAR_HELP()
				CLEAR_BIT(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_BLOCKED_ELEVATOR_MENU_HELP_ON_DISPLAY)
			ENDIF
		ENDIF
				
		IF iCurrentFloorIndex <> iFloorOwnerIsOn		
			IF IS_ELEVATOR_OWNED(ePrimaryExit, iFloorOwnerIsOn, piOwner)
				ref_map.eFloorExitElevator[iFloorOwnerIsOn] = ePrimaryExit
			ELIF IS_ELEVATOR_OWNED(eSecondaryExit, iFloorOwnerIsOn, piOwner)
				ref_map.eFloorExitElevator[iFloorOwnerIsOn] = eSecondaryExit
			ENDIF
		ENDIF
		
		IF iCurrentFloorIndex <> ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			IF IS_ELEVATOR_OWNED(ePrimaryExit, ciBUSINESS_HUB_FLOOR_NIGHTCLUB, piOwner)
				ref_map.eFloorExitElevator[ciBUSINESS_HUB_FLOOR_NIGHTCLUB] = ePrimaryExit
			ELIF IS_ELEVATOR_OWNED(eSecondaryExit, ciBUSINESS_HUB_FLOOR_NIGHTCLUB, piOwner)
				ref_map.eFloorExitElevator[ciBUSINESS_HUB_FLOOR_NIGHTCLUB] = eSecondaryExit
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the elevator exit type for a floor given a built elevator map.
FUNC ELEVATOR_TYPE GET_EXIT_ELEVATOR_TYPE(ELEVATOR_MAP& ref_map, INT iCurrentFloor)
	IF iCurrentFloor >= 0
	AND iCurrentFloor < ciBUSINESS_HUB_FLOOR_COUNT
		RETURN ref_map.eFloorExitElevator[iCurrentFloor]
	ELSE
		RETURN ELT_INVALID
	ENDIF
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_ELEVATOR_TYPE_STRING(ELEVATOR_TYPE eType)
	SWITCH eType
		CASE ELT_GARAGE		RETURN	"GARAGE"
		CASE ELT_STORAGE 	RETURN	"STORAGE"
		CASE ELT_VEHICLE	RETURN	"VEHICLE"
		CASE ELT_INVALID	RETURN	"NONE"
		DEFAULT 			RETURN	"UNDEFINED"
	ENDSWITCH
ENDFUNC
PROC PRINT_ELEVATOR_MAP(ELEVATOR_MAP& ref_map)
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintExitMap")
		PRINTLN("[EXIT_MAP]")
		INT i
		REPEAT ciBUSINESS_HUB_FLOOR_COUNT i
			PRINTLN("[EXIT_MAP][",i,"] = ", GET_ELEVATOR_TYPE_STRING(ref_map.eFloorExitElevator[i]))
		ENDREPEAT
		PRINTLN("[EXIT_MAP]")
	ENDIF
ENDPROC
#ENDIF //IS_DEBUG_BUILD

/// PURPOSE:
///    Sets up globals to force the player to spawn outside the corresponding elevator.
///    This function will also update the global broadcast "last elevator used" bits which
///    are used by other players to determine where they should spawn when they sync to the owner's
///    floor index.
/// PARAMS:
///    eSpawn - 
PROC SET_ELEVATOR_SPAWN_POINT(ELEVATOR_TYPE eSpawn)
	CDEBUG1LN(DEBUG_EXT_MENU, "[menu_int] SET_ELEVATOR_SPAWN_POINT - eSpawn == ", GET_ELEVATOR_TYPE_STRING(eSpawn))
	
	SWITCH eSpawn
		CASE ELT_STORAGE	
			SET_BIT(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_WAREHOUSE_ELEVATOR)	
			SET_LAST_INTERIOR_ELEVATOR_USED(ciBUSINESS_HUB_LAST_ELE_USED_STORAGE)
		BREAK
		CASE ELT_GARAGE		
			SET_BIT(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_GARAGE_ELEVATOR)	
			SET_LAST_INTERIOR_ELEVATOR_USED(ciBUSINESS_HUB_LAST_ELE_USED_GARAGE)
		BREAK
		CASE ELT_VEHICLE 	
			SET_BIT(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_VEHICLE_ELEVATOR)	
			SET_LAST_INTERIOR_ELEVATOR_USED(ciBUSINESS_HUB_LAST_ELE_USED_VEHICLE)
		BREAK
		DEFAULT ASSERTLN(DEBUG_EXT_MENU, "[menu_int] SET_ELEVATOR_SPAWN_POINT - Invalid spawn point specified.")	BREAK
	ENDSWITCH
ENDPROC

FUNC ELEVATOR_TYPE GET_PRIMARY_ELEVATOR_TYPE_FROM_EXIT(INT iExitId, BOOL bNightclub)
	IF bNightclub
		SWITCH iExitId
			CASE ciNIGHTCLUB_EXIT_STORAGE_ELEVATOR	RETURN ELT_STORAGE
			CASE ciNIGHTCLUB_EXIT_GARAGE_ELEVATOR	RETURN ELT_GARAGE
			CASE ciNIGHTCLUB_EXIT_VEHICLE_ELEVATOR	RETURN ELT_VEHICLE
		ENDSWITCH
	ELSE
		SWITCH iExitId
			CASE ciBUSINESS_HUB_EXIT_STORAGE_ELEVATOR	RETURN ELT_STORAGE
			CASE ciBUSINESS_HUB_EXIT_GARAGE_ELEVATOR	RETURN ELT_GARAGE
			CASE ciBUSINESS_HUB_EXIT_VEHICLE_ELEVATOR	RETURN ELT_VEHICLE
		ENDSWITCH
	ENDIF
	RETURN ELT_INVALID
ENDFUNC

FUNC STRING GET_ELEVATOR_TYPE_MENU_DESCRIPTION(ELEVATOR_TYPE eType, BOOL bIAmOwner, BOOL bPlayerOwnsThisFloor, INT iFloorIndex)
	SWITCH eType
		CASE ELT_GARAGE  	RETURN GET_BUSINESS_HUB_GARAGE_HELP_DESC(bIAmOwner, bPlayerOwnsThisFloor, iFloorIndex)
		CASE ELT_STORAGE	RETURN GET_BUSINESS_HUB_WAREHOUSE_HELP_DESC(bIAmOwner, bPlayerOwnsThisFloor, iFloorIndex)
		CASE ELT_VEHICLE	RETURN GET_BUSINESS_HUB_GARAGE_HELP_DESC(bIAmOwner, bPlayerOwnsThisFloor, iFloorIndex)
		DEFAULT RETURN "UNDEFINED"
	ENDSWITCH
ENDFUNC

FUNC INT GET_HUB_FLOOR_FROM_EXIT_MENU_OPTION(INT iOption)
	SWITCH iOption
		CASE ciHUB_EXIT_CLUB RETURN ciBUSINESS_HUB_FLOOR_NIGHTCLUB
		CASE ciHUB_EXIT_F1 RETURN ciBUSINESS_HUB_FLOOR_B1
		CASE ciHUB_EXIT_F2 RETURN ciBUSINESS_HUB_FLOOR_B2
		CASE ciHUB_EXIT_F3 RETURN ciBUSINESS_HUB_FLOOR_B3
		CASE ciHUB_EXIT_F4 RETURN ciBUSINESS_HUB_FLOOR_B4
		CASE ciHUB_EXIT_F5_G // fallthrough (b5)
		CASE ciHUB_EXIT_F5_S RETURN ciBUSINESS_HUB_FLOOR_B5
		DEFAULT RETURN -1
	ENDSWITCH
ENDFUNC

FUNC INT GET_HUB_EXIT_MENU_OPTION_FROM_FLOOR(INT iFloor, BOOL bGarage)
	SWITCH iFloor
		CASE ciBUSINESS_HUB_FLOOR_NIGHTCLUB	RETURN ciHUB_EXIT_CLUB
		CASE ciBUSINESS_HUB_FLOOR_B1 RETURN ciHUB_EXIT_F1
		CASE ciBUSINESS_HUB_FLOOR_B2 RETURN ciHUB_EXIT_F2
		CASE ciBUSINESS_HUB_FLOOR_B3 RETURN ciHUB_EXIT_F3
		CASE ciBUSINESS_HUB_FLOOR_B4 RETURN ciHUB_EXIT_F4
		CASE ciBUSINESS_HUB_FLOOR_B5 
			IF bGarage
				RETURN ciHUB_EXIT_F5_G
			ELSE
				RETURN ciHUB_EXIT_F5_S
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    	This processing starts as soon as the player is in a valid exit locate which is always several frame BEFORE 
///    	the menu actually starts being drawn. Future implemenations that use the same function pointer MUST 
///    	make sure no input processing happen until the menu is not visible (ref_menu.iCurrentState = SIMPLE_INTERIOR_MENU_STATE_VISIBLE)
PROC PROCESS_BUSINESS_HUB_EXIT_MENU(SIMPLE_INTERIORS eSimpleInteriorID, INT iExitID, PLAYER_INDEX piOwner, SIMPLE_INTERIOR_MENU& ref_menu)			
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_PLAYER_DANCING(PLAYER_ID())
		g_SimpleInteriorData.bHideExitMenuThisFrame = TRUE
		CDEBUG1LN(DEBUG_EXT_MENU, "[menu_int] g_SimpleInteriorData.bHideExitMenuThisFrame (Player is dancing - hide the exit menu).")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_WAITING_IN_ELEVATOR)
		g_SimpleInteriorData.bHideExitMenuThisFrame = TRUE
		CDEBUG1LN(DEBUG_EXT_MENU, "[menu_int] g_SimpleInteriorData.bHideExitMenuThisFrame (Player is in elevator - hide the exit menu).")
		EXIT
	ENDIF
	
	INT iCurrentFloorIndex =  GET_INTERIOR_FLOOR_INDEX()
	BOOL bInNightclub = iCurrentFloorIndex = ciBUSINESS_HUB_FLOOR_NIGHTCLUB	
	ELEVATOR_TYPE eElevatorType = GET_PRIMARY_ELEVATOR_TYPE_FROM_EXIT(iExitID, bInNightclub)
	
	BOOL bProcessElevator
	INT iFirstMenuOption
	
	SWITCH eElevatorType
		CASE ELT_STORAGE
		CASE ELT_GARAGE
			bProcessElevator = TRUE
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_DEFAULT, FALSE) 
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_ALL, FALSE)
			iFirstMenuOption = ciHUB_EXIT_CLUB
		BREAK
		CASE ELT_VEHICLE
			bProcessElevator = TRUE
			
			IF bInNightclub
				SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_DEFAULT, FALSE) 
				SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_ALL, FALSE)
				iFirstMenuOption = ciHUB_EXIT_CLUB
			ELSE
				SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_DEFAULT, TRUE)
				iFirstMenuOption = ciHUB_EXIT_DEFAULT
			ENDIF
		BREAK			
		CASE ELT_INVALID
			//	If we are not using an elevator we assume that this exit should just
			//	display normal exit options.
			//ciHUB_EXIT_ALL (1) is handled by am_mp_smpl_interior_int.sc
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_DEFAULT, TRUE) 
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_F1, FALSE)
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_F2, FALSE)
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_F3, FALSE)
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_F4, FALSE)
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_F5_G, FALSE)
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_F5_S, FALSE)
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, ciHUB_EXIT_CLUB, FALSE)
		BREAK
	ENDSWITCH
	
	IF bProcessElevator
		ELEVATOR_MAP exitMap
		GENERATE_EXIT_MAP_FOR_ELEVATOR(eElevatorType, piOwner, exitMap)
		
		#IF IS_DEBUG_BUILD
		PRINT_ELEVATOR_MAP(exitMap)
		#ENDIF
		
		INT iFloorIndex, iMenuIndex
		BOOL bIAmOwner, bNightclubOptionSelectable
		
		bNightclubOptionSelectable = NOT SHOULD_DISABLE_NIGHTCLUB_MENU_OPTION()
		bIAmOwner = piOwner = PLAYER_ID()
		
		REPEAT ciHUB_EXIT_OPTION_COUNT iMenuIndex			
			iFloorIndex = GET_HUB_FLOOR_FROM_EXIT_MENU_OPTION(iMenuIndex)						
			IF iFloorIndex < 0
			OR iFloorIndex >= ciBUSINESS_HUB_FLOOR_COUNT
				RELOOP
			ENDIF
			
			IF exitMap.eFloorExitElevator[iFloorIndex] = ELT_INVALID
				// Invalid floors should just be greyed out for owner, missing for everyone else. Except for hacker truck floor
				// if not purchased and the floor that we're on, which should also be missing for owner.
				BOOL bDisplay = FALSE
				IF bIAmOwner
				AND iMenuIndex <> ciHUB_EXIT_F5_G
				AND (iMenuIndex <> ciHUB_EXIT_F5_S OR eElevatorType <> ELT_VEHICLE)
				AND iFloorIndex <> iCurrentFloorIndex
				AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
					bDisplay = TRUE
				ENDIF
				
				IF g_sMPTunables.bBB_WAREHOUSE_DISABLE_LIFT
					bDisplay = FALSE
				ENDIF
				
				CDEBUG1LN(DEBUG_EXT_MENU, "PROCESS_BUSINESS_HUB_EXIT_MENU - floor ", iFloorIndex, " = ELT_INVALID, bDisplay = ", bDisplay, ".")
				
				SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, iMenuIndex, bDisplay)
				SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(ref_menu, iMenuIndex, FALSE)
			ELSE
				IF g_sMPTunables.bBB_WAREHOUSE_DISABLE_LIFT
					SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, iMenuIndex, FALSE)
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(ref_menu, iMenuIndex, FALSE)
				ELIF iFloorIndex = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
					// No access to nightclub on missions (greyed out)
					SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, iMenuIndex, TRUE)
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(ref_menu, iMenuIndex, bNightclubOptionSelectable)				
				ELIF iFloorIndex = ciBUSINESS_HUB_FLOOR_B5
					IF iMenuIndex = ciHUB_EXIT_F5_G
						IF exitMap.eFloorExitElevator[iFloorIndex] = ELT_GARAGE
						OR exitMap.eFloorExitElevator[iFloorIndex] = ELT_VEHICLE
							SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, iMenuIndex, TRUE)
							SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(ref_menu, iMenuIndex, TRUE)
						ELSE
							SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, iMenuIndex, FALSE)
							SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(ref_menu, iMenuIndex, FALSE)
						ENDIF
					ELSE iMenuIndex = ciHUB_EXIT_F5_S
						IF exitMap.eFloorExitElevator[iFloorIndex] = ELT_STORAGE						
							SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, iMenuIndex, TRUE)
							SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(ref_menu, iMenuIndex, TRUE)
						ELSE
							SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, iMenuIndex, FALSE)
							SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(ref_menu, iMenuIndex, FALSE)
						ENDIF
					ENDIF
				ELSE
					SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(ref_menu, iMenuIndex, TRUE)
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(ref_menu, iMenuIndex, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		INT iCurrentMenuOptionFloorIndex
		iCurrentMenuOptionFloorIndex = GET_HUB_FLOOR_FROM_EXIT_MENU_OPTION(ref_menu.iSelectedItem)
		
		// The menu index can be invalid for the first few frames when
		// showing the menu so we have to block input on these frames.
		IF ref_menu.iOptionsVisibleBS <> 0
		AND (ref_menu.iSelectedItem < iFirstMenuOption
		OR ref_menu.iCurrentState <> SIMPLE_INTERIOR_MENU_STATE_VISIBLE)
			CDEBUG1LN(DEBUG_EXT_MENU, "[menu_int] Menu index invalid - drawing menu, not accepting input. Raw menu index == ",ref_menu.iSelectedItem, " iFirstMenuOption == ", iFirstMenuOption, " current menu state ", ENUM_TO_INT(ref_menu.iCurrentState), " desired menu state ", ENUM_TO_INT(ref_menu.iDesiredState))
			EXIT
		ENDIF
		
		ELEVATOR_TYPE eSelectedExitElevatorType			
		IF ref_menu.iOptionsVisibleBS <> 0
			IF ref_menu.iSelectedItem >= ciHUB_EXIT_CLUB
				STRING sDescription				
				eSelectedExitElevatorType = GET_EXIT_ELEVATOR_TYPE(exitMap, iCurrentMenuOptionFloorIndex)
				IF eSelectedExitElevatorType = ELT_INVALID
					// Advertise this floor type in the description
					sDescription = GET_ELEVATOR_TYPE_MENU_DESCRIPTION(eElevatorType, bIAmOwner, FALSE, iCurrentMenuOptionFloorIndex)
				ELSE
					sDescription = GET_ELEVATOR_TYPE_MENU_DESCRIPTION(eSelectedExitElevatorType, bIAmOwner, TRUE, iCurrentMenuOptionFloorIndex)
				ENDIF
				SET_CURRENT_MENU_ITEM_DESCRIPTION(sDescription)	
			ENDIF
		ELSE
			g_SimpleInteriorData.bHideExitMenuThisFrame = TRUE
			CDEBUG1LN(DEBUG_EXT_MENU, "[menu_int] g_SimpleInteriorData.bHideExitMenuThisFrame (no options visible. Should never happen for interior owner.)")
			EXIT
		ENDIF
		
		IF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(ciHUB_EXIT_DEFAULT, ref_menu)
		OR WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(ciHUB_EXIT_ALL, ref_menu)
			CDEBUG1LN(DEBUG_EXT_MENU, "[menu_int] PROCESS_BUSINESS_HUB_EXIT_MENU -> Menu option ciHUB_EXIT_DEFAULT or ciHUB_EXIT_ALL accepted")				
			EXIT
		ELIF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(ciHUB_EXIT_CLUB, ref_menu)
			CDEBUG1LN(DEBUG_EXT_MENU, "[menu_int] PROCESS_BUSINESS_HUB_EXIT_MENU -> Menu option ciHUB_EXIT_CLUB accepted")				
			SET_INTERIOR_FLOOR_INDEX(ciBUSINESS_HUB_FLOOR_NIGHTCLUB)
			SET_ELEVATOR_SPAWN_POINT(eSelectedExitElevatorType)
			SET_BIT(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_USED_ELEVATOR)
		ELIF WAS_ANY_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(ref_menu)
			// We must have selected a floor....
			CDEBUG1LN(DEBUG_EXT_MENU, "[menu_int] PROCESS_BUSINESS_HUB_EXIT_MENU -> Menu option ",ref_menu.iSelectedItem," selected (floor ", iCurrentMenuOptionFloorIndex,")")
			SET_ELEVATOR_SPAWN_POINT(eSelectedExitElevatorType)
			SET_INTERIOR_FLOOR_INDEX(iCurrentMenuOptionFloorIndex)
			SET_BIT(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_USED_ELEVATOR)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_BUSINESS_HUB_ENTRANCE_MENU(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_MENU &menu)
	BOOL bFriendsNearby
	BOOL bGangMembersNearby
	VECTOR vEntryLocate = GET_BUSINESS_HUB_ENTRY_CORONA_COORDS(eSimpleInteriorID)
	
	IF CAN_PLAYER_INVITE_OTHERS_TO_BUSINESS_HUB(PLAYER_ID(), eSimpleInteriorID)	
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			vEntryLocate = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		
		ARE_PLAYERS_NEAR_PLAYER(eSimpleInteriorID, vEntryLocate, bFriendsNearby, bGangMembersNearby)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_INTERIOR][BUSINESS_HUB] PROCESS_BUSINESS_HUB_ENTRANCE_MENU - bGangMembersNearby: ", bGangMembersNearby, " bFriendsNearby: ", bFriendsNearby)
		#ENDIF
		
		IF bGangMembersNearby
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, TRUE)
		ELSE
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, FALSE)
		ENDIF
		
		IF bFriendsNearby
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 2, TRUE)
		ELSE
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 2, FALSE)
		ENDIF
		
		// We have to do this here, otherwise we'll end up with the server giving us the incorrect instance.
		IF (bGangMembersNearby OR bFriendsNearby)
		AND NOT IS_SIMPLE_INTERIOR_INT_SCRIPT_RUNNING()
			SET_BUSINESS_HUB_ACCESS_BS(eSimpleInteriorID, -1)
		ENDIF
		
		IF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(0, menu)
		
			SET_BUSINESS_HUB_ACCESS_BS(eSimpleInteriorID, -1)
			
		ELIF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(1, menu)
			IF bGangMembersNearby
				BROADCAST_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR(PLAYER_ID(), eSimpleInteriorID, vEntryLocate)
			ELSE
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] PROCESS_BUSINESS_HUB_ENTRANCE_MENU - Not sending broadcast as there's no gang members near entrance.")
				#ENDIF
			ENDIF
			
			TRIGGER_LOCAL_PLAYER_BUSINESS_HUB_ENTRY(eSimpleInteriorID)
			
		ELIF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(2, menu)
			IF bFriendsNearby
				BROADCAST_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR(PLAYER_ID(), eSimpleInteriorID, vEntryLocate, FALSE, FALSE, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] PROCESS_BUSINESS_HUB_ENTRANCE_MENU - Not sending broadcast as there's no friends or crew near entrance.")
				#ENDIF
			ENDIF
			
			TRIGGER_LOCAL_PLAYER_BUSINESS_HUB_ENTRY(eSimpleInteriorID)
		ENDIF
	ELSE
		SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, FALSE)
		SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 2, FALSE)
	ENDIF
ENDPROC

PROC GET_BUSINESS_HUB_VEHICLE_ENTRANCE_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], SIMPLE_INTERIOR_LOCAL_EVENTS &eventOptions[], INT &iOptionsCount)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(eventOptions)
	
	txtTitle = "HUB_VEH_TITLE"
	
	strOptions[0] = "HUB_ENT_F0"
	strOptions[1] = "HUB_ENT_F2"
	strOptions[2] = "HUB_ENT_F3"
	strOptions[3] = "HUB_ENT_F4"
	strOptions[4] = "HUB_ENT_F5"
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), OPPRESSOR2)
		iOptionsCount = 5
	ELSE
		iOptionsCount = 4
	ENDIF
ENDPROC

PROC GET_BUSINESS_HUB_SELECTED_VEHICLE_FLOOR_OFFSET(SIMPLE_INTERIORS eSimpleInteriorID, INT &iSelectedFloor)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	SWITCH iSelectedFloor
		CASE 0	iSelectedFloor = 0	BREAK
		CASE 1	iSelectedFloor = 2	BREAK
		CASE 2	iSelectedFloor = 3	BREAK
		CASE 3	iSelectedFloor = 4	BREAK
		CASE 4	iSelectedFloor = 5	BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_BUSINESS_HUB_VEHICLE_ENTRANCE_MENU(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_MENU &menu, VEHICLE_INDEX vehEntranceVehicle)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(vehEntranceVehicle)
	
	BOOL bDoneIntroScene = TRUE
	
	IF NOT HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(PLAYER_ID(), MBV_COLLECT_STAFF)
	AND NOT HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(PLAYER_ID(), MBV_COLLECT_EQUIPMENT)
		bDoneIntroScene = HAS_LOCAL_PLAYER_VIEWED_NIGHTCLUB_INTRO_CUTSCENE()
	ENDIF
	
	INT i
	REPEAT IMIN(menu.iItemsCount, SIMPLE_INTERIOR_MENU_MAX_OPTIONS) i
		SWITCH i
			CASE 0
				IF NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
				#IF FEATURE_GEN9_EXCLUSIVE
				OR IS_PLAYER_ON_MP_INTRO()
				#ENDIF
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(menu, i, TRUE)
				ELSE
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(menu, i, FALSE)
				ENDIF
			BREAK
			
			CASE 1
				IF IS_PLAYER_BUSINESS_HUB_GARAGE_02_PURCHASED(PLAYER_ID())
				AND NOT GB_IS_PLAYER_ON_MEGA_BUSINESS_SELL_MISSION(PLAYER_ID())
				AND bDoneIntroScene
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(menu, i, TRUE)
				ELSE
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(menu, i, FALSE)
				ENDIF
			BREAK
			
			CASE 2
				IF IS_PLAYER_BUSINESS_HUB_GARAGE_03_PURCHASED(PLAYER_ID())
				AND NOT GB_IS_PLAYER_ON_MEGA_BUSINESS_SELL_MISSION(PLAYER_ID())
				AND bDoneIntroScene
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(menu, i, TRUE)
				ELSE
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(menu, i, FALSE)
				ENDIF
			BREAK
			
			CASE 3
				IF IS_PLAYER_BUSINESS_HUB_GARAGE_04_PURCHASED(PLAYER_ID())
				AND NOT GB_IS_PLAYER_ON_MEGA_BUSINESS_SELL_MISSION(PLAYER_ID())
				AND bDoneIntroScene
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(menu, i, TRUE)
				ELSE
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(menu, i, FALSE)
				ENDIF
			BREAK
			
			CASE 4
				IF IS_PLAYER_HACKER_TRUCK_PURCHASED(PLAYER_ID())
				AND NOT IS_PLAYER_HACKERTRUCK_IN_FREEMODE(PLAYER_ID())
				AND NOT IS_PLAYER_REQUESTED_HACKERTRUCK_IN_FREEMODE(PLAYER_ID())
				AND NOT GB_IS_PLAYER_ON_MEGA_BUSINESS_SELL_MISSION(PLAYER_ID())
				AND bDoneIntroScene
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(menu, i, TRUE)
				ELSE
					SET_SIMPLE_INTERIOR_MENU_OPTION_SELECTABILITY(menu, i, FALSE)
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
	
	SWITCH menu.iSelectedItem
		CASE 0
			IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
			#IF FEATURE_GEN9_EXCLUSIVE
			AND NOT IS_PLAYER_ON_MP_INTRO()
			#ENDIF
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_F0_BLCK")
			ELSE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_ENT_F0_H")
			ENDIF
		BREAK
		
		CASE 1
			IF IS_BIT_SET(menu.iOptionsSelectableBS, menu.iSelectedItem)
				IF GB_IS_PLAYER_ON_MEGA_BUSINESS_SELL_MISSION(PLAYER_ID())
				OR NOT bDoneIntroScene
					SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_VEH_HELP_3")
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_VEH_HELP_2")
				ENDIF
			ELSE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_ENT_F2_H")
			ENDIF
		BREAK
		
		CASE 2
			IF IS_BIT_SET(menu.iOptionsSelectableBS, menu.iSelectedItem)
				IF GB_IS_PLAYER_ON_MEGA_BUSINESS_SELL_MISSION(PLAYER_ID())
				OR NOT bDoneIntroScene
					SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_VEH_HELP_3")
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_VEH_HELP_2")
				ENDIF
			ELSE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_ENT_F3_H")
			ENDIF
		BREAK
		
		CASE 3
			IF IS_BIT_SET(menu.iOptionsSelectableBS, menu.iSelectedItem)
				IF GB_IS_PLAYER_ON_MEGA_BUSINESS_SELL_MISSION(PLAYER_ID())
				OR NOT bDoneIntroScene
					SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_VEH_HELP_3")
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_VEH_HELP_2")
				ENDIF
			ELSE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_ENT_F4_H")
			ENDIF
		BREAK
		
		CASE 4
			IF IS_BIT_SET(menu.iOptionsSelectableBS, menu.iSelectedItem)
				IF GB_IS_PLAYER_ON_MEGA_BUSINESS_SELL_MISSION(PLAYER_ID())
				OR NOT bDoneIntroScene
					SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_VEH_HELP_3")
				ELIF IS_PLAYER_HACKER_TRUCK_PURCHASED(PLAYER_ID())
				AND (IS_PLAYER_HACKERTRUCK_IN_FREEMODE(PLAYER_ID())
				OR IS_PLAYER_REQUESTED_HACKERTRUCK_IN_FREEMODE(PLAYER_ID()))
					SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_VEH_HELP_4")
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_HCKTRNO")
				ENDIF
			ELSE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HUB_ENT_F5_H")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_OWNER_LEAVE_BUSINESS_HUB_FOR_TRADE_IN(SIMPLE_INTERIORS eSimpleInteriorID)
	IF IS_PLAYER_IN_BUSINESS_HUB_THEY_OWN(PLAYER_ID())
	AND eSimpleInteriorID != GET_OWNED_BUSINESS_HUB_SIMPLE_INTERIOR(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer = -1)
	UNUSED_PARAMETER(iInvitingPlayer)
	PLAYER_INDEX piOwner 	= PLAYER_ID()
	PLAYER_INDEX piBoss		= GB_GET_LOCAL_PLAYER_GANG_BOSS()
	BOOL bOnMission 		= FALSE
	
	IF SHOULD_SUPPRESS_SIMPLE_INTERIOR_KICK_OUT()
		PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB FALSE SHOULD_SUPPRESS_SIMPLE_INTERIOR_KICK_OUT is true")
		RETURN FALSE
	ENDIF
	
	IF g_SimpleInteriorData.iAccessBS = 0
		// How did we get here? Probably script relaunched somehow, kick us out
		PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB TRUE g_SimpleInteriorData.iAccessBS = 0")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER)
		piOwner = globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
	ENDIF
	
	IF piOwner != INVALID_PLAYER_INDEX()
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piOwner) = FMMC_TYPE_FMBB_SELL
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piOwner) = FMMC_TYPE_FMBB_CLUB_MANAGEMENT
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piOwner) = FMMC_TYPE_FMBB_DEFEND
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piOwner) = FMMC_TYPE_FMBB_PHONECALL
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piOwner) = FMMC_TYPE_CLUB_MANAGEMENT
			IF GB_IS_PLAYER_CRITICAL_TO_JOB(piOwner)
				PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB TRUE FMMC_TYPE_FMBB_SELL or FMMC_TYPE_FMBB_CLUB_MANAGEMENT")
				bOnMission = TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	#IF IS_DEBUG_BUILD
	IF g_bDebugNightClubKick
		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICKED_BY_BOUNCER)
		PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB TRUE SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICKED_BY_BOUNCER owner id: ", NATIVE_TO_INT(piOwner))
		SET_KICKED_FROM_NIGHTCLUB(piOwner, TRUE)
		g_bDebugNightClubKick = FALSE
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF NOT IS_BIT_SET (g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER)
		
		IF NOT IS_NET_PLAYER_OK(piOwner, FALSE, TRUE)
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER_LEFT_GAME)
			PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB TRUE SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER_LEFT_GAME owner id: ", NATIVE_TO_INT(piOwner))
			RETURN TRUE
		ENDIF
		
		IF GET_OWNED_BUSINESS_HUB_SIMPLE_INTERIOR(piOwner) != eSimpleInteriorID
			SET_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_INTERIOR_DUE_TO_PROPERTY_TRADE)
			PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB TRUE BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_INTERIOR_DUE_TO_PROPERTY_TRADE OWNER ID = ", NATIVE_TO_INT(piOwner), " owned business hub/nightclub = ", GET_OWNED_BUSINESS_HUB_SIMPLE_INTERIOR(piOwner), " business hub/nightclub we are in = ", eSimpleInteriorID)
			RETURN TRUE
		ENDIF
		
		IF bOnMission
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
			AND GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), piOwner)				
				//Only kick players if this was a fresh mission launch
				IF NOT IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_RE_ENTERED_ON_MISSION)
					IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piOwner) = FMMC_TYPE_FMBB_SELL
					AND NOT IS_PLAYER_ON_NIGHTCLUB_SETUP_OR_DJ_MISSION(piOwner)
						//We were kicked because we're on mission with the owner
						g_SimpleInteriorData.iExitUsed = ciNIGHTCLUB_EXIT_GARAGE
						PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB - TRUE - Launching sell with complete setup missions so moving to ciBUSINESS_HUB_EXIT_NIGHTCLUB_GARAGE")
					ELIF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piOwner) = FMMC_TYPE_FMBB_DEFEND
					OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piOwner) = FMMC_TYPE_FMBB_CLUB_MANAGEMENT
					OR IS_PLAYER_ON_NIGHTCLUB_SETUP_OR_DJ_MISSION(piOwner)
						g_SimpleInteriorData.iExitUsed = ciBUSINESS_HUB_EXIT_FORCE_CLUB_FRONT_DOOR
						PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB - TRUE - Launching defend so forcing front door exit ciBUSINESS_HUB_EXIT_FORCE_CLUB_FRONT_DOOR")
					ENDIF
					
					RETURN TRUE
				ENDIF
			ELIF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piOwner) = FMMC_TYPE_FMBB_SELL
			AND GET_INTERIOR_FLOOR_INDEX() != ciBUSINESS_HUB_FLOOR_NIGHTCLUB
				//We were kicked as the owner was on a mission
				PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB kicked as the owner has started a mission and I'm not part of their gang")
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_MISSION_EXIT_NON_MEMBER)
				
				//Only kick players if this was a fresh mission launch
				RETURN NOT IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_RE_ENTERED_ON_MISSION)
			ENDIF
		ELSE
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
			AND NOT GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), piOwner)
			AND GET_INTERIOR_FLOOR_INDEX() != ciBUSINESS_HUB_FLOOR_NIGHTCLUB
				//We have joined another gang and we are in the business hub
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_EXIT_JOINED_ANOTHER_GANG)
				PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB TRUE SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_EXIT_JOINED_ANOTHER_GANG ")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF g_iNightClubKickStrikes >= NIGHT_CLUB_MAX_KICK_STRIKES
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
			// kicked and not being moved
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICKED_BY_BOUNCER)
			PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB TRUE SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICKED_BY_BOUNCER owner id: ", NATIVE_TO_INT(piOwner))
			SET_KICKED_FROM_NIGHTCLUB(piOwner, TRUE)
			RETURN TRUE
		ENDIF
	
	ELIF bOnMission
		//Kicking the owner out to start a mission
		PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB kicked as local player started a mission")
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(piOwner) = FMMC_TYPE_FMBB_DEFEND
		AND NOT IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_RE_ENTERED_ON_MISSION)
			g_SimpleInteriorData.iExitUsed = ciBUSINESS_HUB_EXIT_FORCE_CLUB_FRONT_DOOR
			PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB - TRUE - Launching defend so forcing front door exit ciBUSINESS_HUB_EXIT_FORCE_CLUB_FRONT_DOOR")
		ENDIF
		RETURN NOT IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_RE_ENTERED_ON_MISSION)
	ELSE
		IF GET_INTERIOR_FLOOR_INDEX() != ciBUSINESS_HUB_FLOOR_NIGHTCLUB
		AND GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_EXIT_JOINED_ANOTHER_GANG)
			PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB TRUE SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_EXIT_JOINED_ANOTHER_GANG ")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FIXER_VIP AND GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = ENUM_TO_INT(FVV_NIGHTCLUB))
	OR (piBoss != INVALID_PLAYER_INDEX() AND GB_IS_GLOBAL_CLIENT_BIT2_SET(piBoss, eGB_GLOBAL_CLIENT_BITSET_2_REQUEST_KICK_OUT_NIGHTCLUB))
		IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB kicked as the owner has started a mission (FVV_NIGHTCLUB) - I'm part of their gang")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICK_ARMED_PLAYER)
		PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB TRUE SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICK_ARMED_PLAYER ")
		RETURN TRUE
	ENDIF
	
	//Clear the flag if we're no longer on a mission
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_RE_ENTERED_ON_MISSION)
	AND NOT bOnMission
	AND piOwner != INVALID_PLAYER_INDEX()
		PRINTLN("SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB local player clearing SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_RE_ENTERED_ON_MISSION")
		CLEAR_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_RE_ENTERED_ON_MISSION)
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC STRING GET_BUSINESS_HUB_KICK_OUT_REASON(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, TEXT_LABEL_31 &txtPlayerNameString, INT iInvitingPlayer = -1)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(txtExtraString)
	UNUSED_PARAMETER(iInvitingPlayer)
	
	IF g_SimpleInteriorData.iAccessBS = 0
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_KICK_OUT_REASON - g_SimpleInteriorData.iAccessBS = 0.")
		RETURN ""
	ELIF IS_SIMPLE_INTERIOR_CHILD_SCRIPT_NOT_ABLE_TO_DETERMINE_VALID_OWNER()
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_KICK_OUT_REASON - IS_SIMPLE_INTERIOR_CHILD_SCRIPT_NOT_ABLE_TO_DETERMINE_VALID_OWNER.")
		RETURN ""
	ENDIF
	
	IF g_SimpleInteriorData.iAccessBS = 0
		// You no longer have access to this Business Hub.
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_KICK_OUT_REASON - g_SimpleInteriorData.iAccessBS = 0.")
		IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			RETURN "CLUB_KICK_A"
		ELSE
			RETURN "HUB_KICK_A"
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER_LEFT_GAME)
		// You no longer have access to the Business Hub as the owner has left the game.
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_KICK_OUT_REASON - Owner left the game.")
		IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			RETURN "CLUB_KICK_B"
		ELSE
			RETURN "HUB_KICK_B"
		ENDIF
	ENDIF
	
	IF g_SimpleInteriorData.bExitAllTriggeredByOwner
	AND NOT IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_INTERIOR_DUE_TO_PROPERTY_TRADE)
		// Owner has requested all to leave the interior.
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_KICK_OUT_REASON - Owner emptied interior.")
		IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			RETURN "CLUB_KICK_C"
		ELSE
			RETURN "HUB_KICK_C"
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_INTERIOR_DUE_TO_PROPERTY_TRADE)
		g_SimpleInteriorData.bExitAllTriggeredByOwner = FALSE
		
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER)
			// You no longer have access to this interior as you have traded it in.
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_KICK_OUT_REASON - (Owner) Hub has been traded in.")
			IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
				RETURN "CLUB_KICK_D"
			ELSE
				RETURN "HUB_KICK_D"
			ENDIF
		ELSE
			// You no longer have access to this interior as the owner has traded it in.
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_KICK_OUT_REASON - (Not owner) Hub has been traded in.")
			IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
				RETURN "CLUB_KICK_E"
			ELSE
				RETURN "HUB_KICK_E"
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_MISSION_EXIT_NON_MEMBER)
		// You no longer have access to the interior as the owner is undertaking confidential business.
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_KICK_OUT_REASON - Mission kick non members.")
		IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			RETURN "CLUB_KICK_F"
		ELSE
			RETURN "HUB_KICK_F"
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICKED_BY_BOUNCER)
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_KICK_OUT_REASON - Kicked out by bouncer.")
		txtPlayerNameString = GET_PLAYER_NAME(GET_OWNER_OF_THIS_NIGHTCLUB())
		RETURN "CLUB_KICK_L" 
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_EXIT_JOINED_ANOTHER_GANG)
		// Owner has requested all to leave the interior.
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_KICK_OUT_REASON - Owner joined a gang.")
		IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			RETURN "CLUB_KICK_G"
		ELSE
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					RETURN "HUB_KICK_H1"
				ELSE
					RETURN "HUB_KICK_H"
				ENDIF
			ELIF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					RETURN "HUB_KICK_I1"
				ELSE
					RETURN "HUB_KICK_I"
				ENDIF
			ELSE
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					RETURN "HUB_KICK_G1"
				ELSE
					RETURN "HUB_KICK_G"
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_BECAME_BEAST)
		// You no longer have access to the interior as you became the Beast.
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_KICK_OUT_REASON - You became the beast.")
		IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
			RETURN "CLUB_KICK_K"
		ELSE
			RETURN "HUB_KICK_K"
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL GET_NIGHTCLUB_INTERIOR_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle)
	UNUSED_PARAMETER(bSpawnInVehicle)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			SWITCH iSpawnPoint
				CASE 0    	vSpawnPoint = <<34.7874, -4.6929, 4.5938>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 1    	vSpawnPoint = <<36.0613, -4.2415, 4.5938>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 2    	vSpawnPoint = <<35.3879, -3.1406, 4.5938>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 3    	vSpawnPoint = <<34.4712, -2.6055, 4.5938>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 4    	vSpawnPoint = <<36.0719, -2.0310, 4.5938>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 5    	vSpawnPoint = <<34.4131, -0.7634, 4.5938>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 6    	vSpawnPoint = <<35.3104, -1.2947, 4.5938>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 7    	vSpawnPoint = <<36.0902, -0.2917, 4.5938>>    	fSpawnHeading = 18.0047   RETURN TRUE	BREAK
				CASE 8    	vSpawnPoint = <<35.2301, 0.4087, 4.5938>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 9    	vSpawnPoint = <<34.7003, 1.5886, 4.5938>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 10    	vSpawnPoint = <<35.9341, 1.4646, 4.5938>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK				
				CASE 11    	vSpawnPoint = <<35.5159, 3.9800, 4.5938>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 12    	vSpawnPoint = <<36.1342, 5.1018, 4.3937>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 13    	vSpawnPoint = <<35.3647, 7.3210, 3.9884>>    	fSpawnHeading = 357.7915  RETURN TRUE	BREAK
				CASE 14    	vSpawnPoint = <<35.3207, 5.8093, 4.9939>>    	fSpawnHeading = 0.0000    RETURN TRUE	BREAK
				CASE 15    	vSpawnPoint = <<35.2750, 8.8342, 3.7939>>    	fSpawnHeading = 81.8284	  RETURN TRUE	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_BUSINESS_HUB_WAREHOUSE_ELEVATOR_SPAWN_POINT(INT iSpawnPoint, VECTOR& vSpawnPoint, FLOAT& fSpawnHeading)
	fSpawnHeading 	= 180.0000
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint = <<-1507.2671, -3027.3857, -80.2422>>	BREAK
		CASE 1	vSpawnPoint = <<-1508.0171, -3027.3857, -80.2422>>	BREAK
		CASE 2	vSpawnPoint = <<-1507.2671, -3028.1357, -80.2422>>	BREAK
		CASE 3	vSpawnPoint = <<-1508.0171, -3028.1357, -80.2422>>	BREAK
		CASE 4	vSpawnPoint = <<-1507.2671, -3028.8857, -80.2422>>	BREAK
		CASE 5	vSpawnPoint = <<-1508.0171, -3028.8857, -80.2422>>	BREAK
		CASE 6	vSpawnPoint = <<-1507.2671, -3029.6357, -80.2422>>	BREAK
		CASE 7	vSpawnPoint = <<-1508.0171, -3029.6357, -80.2422>>	BREAK
		CASE 8	vSpawnPoint = <<-1507.2671, -3030.3857, -80.2422>>	BREAK
		CASE 9	vSpawnPoint = <<-1508.0171, -3030.3857, -80.2422>>	BREAK
		CASE 10	vSpawnPoint = <<-1509.2671, -3027.3857, -80.2422>>	BREAK
		CASE 11	vSpawnPoint = <<-1510.0171, -3027.3857, -80.2422>>	BREAK
		CASE 12	vSpawnPoint = <<-1509.2671, -3028.1357, -80.2422>>	BREAK
		CASE 13	vSpawnPoint = <<-1510.0171, -3028.1357, -80.2422>>	BREAK
		CASE 14	vSpawnPoint = <<-1509.2671, -3028.8857, -80.2422>>	BREAK
		CASE 15	vSpawnPoint = <<-1510.0171, -3028.8857, -80.2422>>	BREAK
		CASE 16	vSpawnPoint = <<-1509.2671, -3029.6357, -80.2422>>	BREAK
		CASE 17	vSpawnPoint = <<-1510.0171, -3029.6357, -80.2422>>	BREAK
		CASE 18	vSpawnPoint = <<-1509.2671, -3030.3857, -80.2422>>	BREAK
		CASE 19	vSpawnPoint = <<-1510.0171, -3030.3857, -80.2422>>	BREAK
		CASE 20	vSpawnPoint = <<-1505.2671, -3027.3857, -80.2422>>	BREAK
		CASE 21	vSpawnPoint = <<-1506.0171, -3027.3857, -80.2422>>	BREAK
		CASE 22	vSpawnPoint = <<-1505.2671, -3028.1357, -80.2422>>	BREAK
		CASE 23	vSpawnPoint = <<-1506.0171, -3028.1357, -80.2422>>	BREAK
		CASE 24	vSpawnPoint = <<-1505.2671, -3028.8857, -80.2422>>	BREAK
		CASE 25	vSpawnPoint = <<-1506.0171, -3028.8857, -80.2422>>	BREAK
		CASE 26	vSpawnPoint = <<-1505.2671, -3029.6357, -80.2422>>	BREAK
		CASE 27	vSpawnPoint = <<-1506.0171, -3029.6357, -80.2422>>	BREAK
		CASE 28	vSpawnPoint = <<-1505.2671, -3030.3857, -80.2422>>	BREAK
		CASE 29	vSpawnPoint = <<-1506.0171, -3030.3857, -80.2422>>	BREAK
		DEFAULT CLEAR_BIT(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_WAREHOUSE_ELEVATOR) RETURN FALSE
	ENDSWITCH
	
	// Interiors are axis aligned so we can just translate world coords to get relative coords.
	vSpawnPoint -= GET_BUSINESS_HUB_INTERIOR_POSITION()
	
	RETURN TRUE // Return false at DEFAULT case label
ENDFUNC	

FUNC BOOL GET_BUSINESS_HUB_GARAGE_ELEVATOR_SPAWN_POINT(INT iSpawnPoint, VECTOR& vSpawnPoint, FLOAT& fSpawnHeading)
	fSpawnHeading = 0.0
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint	= <<-1508.1969, -3014.4434, -80.2421>>	BREAK
		CASE 1  vSpawnPoint	= <<-1507.1969, -3014.4434, -80.2421>>	BREAK
		CASE 2	vSpawnPoint = <<-1508.1969, -3013.4434, -80.2421>>	BREAK
		CASE 3  vSpawnPoint = <<-1507.1969, -3013.4434, -80.2421>>	BREAK
		CASE 4	vSpawnPoint = <<-1508.1969, -3012.4434, -80.2421>>	BREAK
		CASE 5	vSpawnPoint = <<-1507.1969, -3012.4434, -80.2421>>	BREAK
		CASE 6	vSpawnPoint = <<-1508.1969, -3011.4434, -80.2421>>	BREAK
		CASE 7  vSpawnPoint = <<-1507.1969, -3011.4434, -80.2421>>	BREAK
		CASE 8  vSpawnPoint = <<-1508.1969, -3010.4434, -80.2421>>	BREAK
		CASE 9  vSpawnPoint = <<-1507.1969, -3010.4434, -80.2421>>	BREAK
		CASE 10	vSpawnPoint	= <<-1506.1969, -3014.4434, -80.2421>>	BREAK
		CASE 11	vSpawnPoint	= <<-1505.1969, -3014.4434, -80.2421>>	BREAK
		CASE 12	vSpawnPoint = <<-1506.1969, -3013.4434, -80.2421>>	BREAK
		CASE 13	vSpawnPoint = <<-1505.1969, -3013.4434, -80.2421>>	BREAK
		CASE 14	vSpawnPoint = <<-1506.1969, -3012.4434, -80.2421>>	BREAK
		CASE 15	vSpawnPoint = <<-1505.1969, -3012.4434, -80.2421>>	BREAK
		CASE 16	vSpawnPoint = <<-1506.1969, -3011.4434, -80.2421>>	BREAK
		CASE 17	vSpawnPoint = <<-1505.1969, -3011.4434, -80.2421>>	BREAK
		CASE 18	vSpawnPoint = <<-1506.1969, -3010.4434, -80.2421>>	BREAK
		CASE 19	vSpawnPoint = <<-1505.1969, -3010.4434, -80.2421>>	BREAK
		CASE 20	vSpawnPoint	= <<-1509.1969, -3014.4434, -80.2421>>	BREAK
		CASE 21 vSpawnPoint	= <<-1510.1969, -3014.4434, -80.2421>>	BREAK
		CASE 22	vSpawnPoint = <<-1509.1969, -3013.4434, -80.2421>>	BREAK
		CASE 23 vSpawnPoint = <<-1510.1969, -3013.4434, -80.2421>>	BREAK
		CASE 24	vSpawnPoint = <<-1509.1969, -3012.4434, -80.2421>>	BREAK
		CASE 25	vSpawnPoint = <<-1510.1969, -3012.4434, -80.2421>>	BREAK
		CASE 26	vSpawnPoint = <<-1509.1969, -3011.4434, -80.2421>>	BREAK
		CASE 27 vSpawnPoint = <<-1510.1969, -3011.4434, -80.2421>>	BREAK
		CASE 28 vSpawnPoint = <<-1509.1969, -3010.4434, -80.2421>>	BREAK
		CASE 29 vSpawnPoint = <<-1510.1969, -3010.4434, -80.2421>>	BREAK		
		DEFAULT CLEAR_BIT(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_GARAGE_ELEVATOR) RETURN FALSE
	ENDSWITCH
	
	// Interiors are axis aligned so we can just translate world coords to get relative coords.
	vSpawnPoint -= GET_BUSINESS_HUB_INTERIOR_POSITION()
	
	RETURN TRUE // Return false at DEFAULT case label
ENDFUNC

FUNC BOOL GET_NIGHTCLUB_WAREHOUSE_ELEVATOR_SPAWN_POINT(INT iSpawnPoint, VECTOR& vSpawnPoint, FLOAT& fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0  vSpawnPoint = <<-1617.8649, -3007.5452, -76.2051>>	fSpawnHeading 	= 180.0000	BREAK 
		CASE 1	vSpawnPoint = <<-1618.8649, -3007.5452, -76.2051>>	fSpawnHeading 	= 180.000	BREAK
		CASE 2	vSpawnPoint	= <<-1617.8649, -3008.5452, -76.2051>>	fSpawnHeading 	= 180.0000	BREAK
		CASE 3	vSpawnPoint = <<-1618.8649, -3008.5452, -76.2051>>	fSpawnHeading 	= 180.0000	BREAK
		CASE 4	vSpawnPoint = <<-1620.0695, -3007.8179, -76.2050>> 	fSpawnHeading 	= 212.7600	BREAK
		CASE 5	vSpawnPoint = <<-1620.9104, -3008.3589, -76.2050>>	fSpawnHeading 	= 212.7600	BREAK
		CASE 6	vSpawnPoint = <<-1619.5283, -3008.6589, -76.2052>>	fSpawnHeading 	= 212.7600	BREAK
		CASE 7  vSpawnPoint = <<-1620.3693, -3009.2000, -76.2050>>	fSpawnHeading 	= 212.7600	BREAK
		CASE 8	vSpawnPoint = <<-1622.0212, -3009.2180, -76.2050>>	fSpawnHeading 	= 240.0000	BREAK
		CASE 9	vSpawnPoint = <<-1622.5212, -3010.0840, -76.2050>>	fSpawnHeading 	= 240.0000	BREAK
		CASE 10 vSpawnPoint = <<-1621.1553, -3009.7180, -76.2050>>	fSpawnHeading 	= 240.0000	BREAK
		CASE 11 vSpawnPoint = <<-1621.6553, -3010.5840, -76.2050>>	fSpawnHeading 	= 240.0000	BREAK
  		CASE 12	vSpawnPoint = <<-1623.1439, -3011.6997, -76.2050>>	fSpawnHeading 	= 275.0000	BREAK
		CASE 13 vSpawnPoint = <<-1623.0568, -3012.6958, -76.2050>>	fSpawnHeading 	= 275.0000 	BREAK
		CASE 14	vSpawnPoint = <<-1622.1477, -3011.6125, -76.2050>>	fSpawnHeading 	= 275.0000	BREAK
		CASE 15 vSpawnPoint = <<-1622.0605, -3012.6086, -76.2050>>	fSpawnHeading 	= 275.0000	BREAK
		CASE 16	vSpawnPoint = <<-1622.6859, -3013.7188, -76.2050>>	fSpawnHeading 	= 298.0800	BREAK
		CASE 17 vSpawnPoint = <<-1622.2152, -3014.6011, -76.2050>>	fSpawnHeading 	= 298.0800	BREAK
		CASE 18	vSpawnPoint = <<-1621.8036, -3013.2480, -76.2050>>	fSpawnHeading 	= 298.0800	BREAK
		CASE 19 vSpawnPoint = <<-1621.3329, -3014.1304, -76.2050>>	fSpawnHeading 	= 298.0800	BREAK
  		CASE 20	vSpawnPoint	= <<-1620.3181, -3016.2717, -76.2050>>	fSpawnHeading 	= 348.3600	BREAK
		CASE 21	vSpawnPoint = <<-1619.3386, -3016.4734, -76.2051>>	fSpawnHeading 	= 348.3600	BREAK
		CASE 22 vSpawnPoint	= <<-1620.1163, -3015.2922, -76.2050>>	fSpawnHeading 	= 348.3600	BREAK
		CASE 23 vSpawnPoint = <<-1619.1368, -3015.4939, -76.2051>>	fSpawnHeading 	= 348.3600	BREAK
		CASE 24 vSpawnPoint = <<-1616.7236, -3007.7703, -76.2051>>	fSpawnHeading 	= 160.5205	BREAK
		CASE 25 vSpawnPoint = <<-1616.8984, -3008.8267, -76.2051>>	fSpawnHeading 	= 164.9609	BREAK
		CASE 26 vSpawnPoint = <<-1617.2211, -3010.0376, -76.2051>>	fSpawnHeading 	= 165.0200	BREAK
		CASE 27 vSpawnPoint = <<-1616.0286, -3010.5308, -76.2051>>	fSpawnHeading 	= 132.5574	BREAK
		CASE 28 vSpawnPoint = <<-1618.6742, -3011.5674, -76.2051>>	fSpawnHeading 	= 181.3259	BREAK
		CASE 29 vSpawnPoint = <<-1619.4854, -3013.7537, -76.2050>>	fSpawnHeading 	= 191.0383	BREAK
		DEFAULT CLEAR_BIT(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_WAREHOUSE_ELEVATOR) RETURN FALSE
	ENDSWITCH
	
	// Interiors are axis aligned so we can just translate world coords to get relative coords.
	vSpawnPoint -= GET_NIGHTCLUB_INTERIOR_POSITION()
	
	RETURN TRUE // Return false at DEFAULT case label	
ENDFUNC

FUNC BOOL GET_NIGHTCLUB_GARAGE_ELEVATOR_SPAWN_POINT(INT iSpawnPoint, VECTOR& vSpawnPoint, FLOAT& fSpawnHeading)
    fSpawnHeading 	= 0.0000
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<-1618.9203, -2998.8018, -79.1495>>BREAK
		CASE 1	vSpawnPoint 	= <<-1618.9203, -2997.8018, -79.1495>>BREAK
		CASE 2	vSpawnPoint 	= <<-1618.9203, -2996.8018, -79.1495>>BREAK
		CASE 3	vSpawnPoint 	= <<-1617.9203, -2998.8018, -79.1495>>BREAK
		CASE 4	vSpawnPoint 	= <<-1617.9203, -2997.8018, -79.1499>>BREAK
		CASE 5	vSpawnPoint 	= <<-1617.9203, -2996.8018, -79.1499>>BREAK
		CASE 6	vSpawnPoint 	= <<-1619.9203, -2998.8018, -79.1492>>BREAK
		CASE 7	vSpawnPoint 	= <<-1619.9203, -2997.8018, -79.1489>>BREAK
		CASE 8	vSpawnPoint 	= <<-1619.9203, -2996.8018, -79.1486>>BREAK
		CASE 9	vSpawnPoint 	= <<-1616.9203, -2998.8018, -79.1495>>BREAK
		CASE 10	vSpawnPoint 	= <<-1616.9203, -2997.8018, -79.1495>>BREAK
		CASE 11	vSpawnPoint 	= <<-1616.9203, -2996.8018, -79.1495>>BREAK
		CASE 12	vSpawnPoint 	= <<-1620.9203, -2998.8018, -79.1451>>BREAK
		CASE 13	vSpawnPoint 	= <<-1620.9203, -2997.8018, -79.1451>>BREAK
		CASE 14	vSpawnPoint 	= <<-1620.9203, -2996.8018, -79.1451>>BREAK
		CASE 15	vSpawnPoint 	= <<-1615.9203, -2998.8018, -79.1495>>BREAK
		CASE 16	vSpawnPoint 	= <<-1615.9203, -2997.8018, -79.1499>>BREAK
		CASE 17	vSpawnPoint 	= <<-1615.9203, -2996.8018, -79.1499>>BREAK
		CASE 18	vSpawnPoint 	= <<-1621.9203, -2998.8018, -79.1438>>BREAK
		CASE 19	vSpawnPoint 	= <<-1621.9203, -2997.8018, -79.1438>>BREAK
		CASE 20	vSpawnPoint 	= <<-1621.9203, -2996.8018, -79.1438>>BREAK
		CASE 21	vSpawnPoint 	= <<-1622.9203, -2998.8018, -79.1438>>BREAK
		CASE 22	vSpawnPoint 	= <<-1622.9203, -2997.8018, -79.1438>>BREAK
		CASE 23	vSpawnPoint 	= <<-1622.9203, -2996.8018, -79.1438>>BREAK
		CASE 24	vSpawnPoint 	= <<-1623.9203, -2998.8018, -79.1438>>BREAK
		CASE 25	vSpawnPoint 	= <<-1623.9203, -2997.8018, -79.1438>>BREAK
		CASE 26	vSpawnPoint 	= <<-1623.9203, -2996.8018, -79.1438>>BREAK
		CASE 27	vSpawnPoint 	= <<-1624.9203, -2998.8018, -79.1438>>BREAK
		CASE 28	vSpawnPoint 	= <<-1624.9203, -2997.8018, -79.1438>>BREAK
		CASE 29	vSpawnPoint 	= <<-1624.9203, -2996.8018, -79.1438>>BREAK
		DEFAULT CLEAR_BIT(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_GARAGE_ELEVATOR) RETURN FALSE
	ENDSWITCH
	
	// Interiors are axis aligned so we can just translate world coords to get relative coords.
	vSpawnPoint -= GET_NIGHTCLUB_INTERIOR_POSITION()
	
	RETURN TRUE // Return false at DEFAULT case label	
ENDFUNC

FUNC BOOL GET_NIGHTCLUB_VEHICLE_ELEVATOR_SPAWN_POINT(INT iSpawnPoint, VECTOR& vSpawnPoint, FLOAT& fSpawnHeading)
    fSpawnHeading 	= 0.0000
	
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<-1641.4698, -3013.4919, -79.1438>>BREAK
		CASE 1	vSpawnPoint 	= <<-1640.4698, -3013.4919, -79.1438>>BREAK
		CASE 2	vSpawnPoint 	= <<-1639.4698, -3013.4919, -79.1438>>BREAK
		CASE 3	vSpawnPoint 	= <<-1638.4698, -3013.4919, -79.1438>>BREAK
		CASE 4	vSpawnPoint 	= <<-1637.4698, -3013.4919, -79.1438>>BREAK
		CASE 5	vSpawnPoint 	= <<-1641.4698, -3012.4919, -79.1438>>BREAK
		CASE 6	vSpawnPoint 	= <<-1640.4698, -3012.4919, -79.1438>>BREAK
		CASE 7	vSpawnPoint 	= <<-1639.4698, -3012.4919, -79.1438>>BREAK
		CASE 8	vSpawnPoint 	= <<-1638.4698, -3012.4919, -79.1438>>BREAK
		CASE 9	vSpawnPoint 	= <<-1637.4698, -3012.4919, -79.1438>>BREAK
		CASE 10	vSpawnPoint 	= <<-1641.4698, -3011.4919, -79.1438>>BREAK
		CASE 11	vSpawnPoint 	= <<-1640.4698, -3011.4919, -79.1438>>BREAK
		CASE 12	vSpawnPoint 	= <<-1639.4698, -3011.4919, -79.1438>>BREAK
		CASE 13	vSpawnPoint 	= <<-1638.4698, -3011.4919, -79.1438>>BREAK
		CASE 14	vSpawnPoint 	= <<-1637.4698, -3011.4919, -79.1438>>BREAK
		CASE 15	vSpawnPoint 	= <<-1641.4698, -3010.4919, -79.1438>>BREAK
		CASE 16	vSpawnPoint 	= <<-1640.4698, -3010.4919, -79.1438>>BREAK
		CASE 17	vSpawnPoint 	= <<-1639.4698, -3010.4919, -79.1438>>BREAK
		CASE 18	vSpawnPoint 	= <<-1638.4698, -3010.4919, -79.1438>>BREAK
		CASE 19	vSpawnPoint 	= <<-1637.4698, -3010.4919, -79.1438>>BREAK
		CASE 20	vSpawnPoint 	= <<-1641.4698, -3009.4919, -79.1438>>BREAK
		CASE 21	vSpawnPoint 	= <<-1640.4698, -3009.4919, -79.1438>>BREAK
		CASE 22	vSpawnPoint 	= <<-1639.4698, -3009.4919, -79.1438>>BREAK
		CASE 23	vSpawnPoint 	= <<-1638.4698, -3009.4919, -79.1438>>BREAK
		CASE 24	vSpawnPoint 	= <<-1637.4698, -3009.4919, -79.1438>>BREAK
		CASE 25	vSpawnPoint 	= <<-1641.4698, -3008.4919, -79.1438>>BREAK
		CASE 26 vSpawnPoint 	= <<-1640.4698, -3008.4919, -79.1438>>BREAK
		CASE 27	vSpawnPoint 	= <<-1639.4698, -3008.4919, -79.1438>>BREAK
		CASE 28	vSpawnPoint 	= <<-1638.4698, -3008.4919, -79.1438>>BREAK
		CASE 29	vSpawnPoint 	= <<-1637.4698, -3008.4919, -79.1438>>BREAK

		DEFAULT CLEAR_BIT(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_VEHICLE_ELEVATOR) RETURN FALSE
	ENDSWITCH
	
	// Interiors are axis aligned so we can just translate world coords to get relative coords.
	vSpawnPoint -= GET_NIGHTCLUB_INTERIOR_POSITION()
	
	RETURN TRUE // Return false at DEFAULT case label	
ENDFUNC

FUNC BOOL SHOULD_SPAWN_AT_GARAGE_ELEVATOR()
	RETURN IS_BIT_SET(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_GARAGE_ELEVATOR)
ENDFUNC

FUNC BOOL SHOULD_SPAWN_AT_WAREHOUSE_ELEVATOR()
	RETURN IS_BIT_SET(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_WAREHOUSE_ELEVATOR) 
ENDFUNC

FUNC BOOL SHOULD_SPAWN_AT_VEHICLE_ELEVATOR()
	RETURN IS_BIT_SET(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_VEHICLE_ELEVATOR) 
ENDFUNC

FUNC BOOL GET_BUSINESS_HUB_NIGHTCLUB_GARAGE_ENTRANCE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0
		    vSpawnPoint = <<-38.2300, 22.7354, 2.0906>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 1
		    vSpawnPoint = <<-39.1041, 21.6387, 2.1919>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 2
		    vSpawnPoint = <<-39.1311, 23.5662, 2.1950>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 3
		    vSpawnPoint = <<-38.5009, 20.5188, 2.1220>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 4
		    vSpawnPoint = <<-38.6439, 24.9575, 2.1386>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 5
		    vSpawnPoint = <<-35.4742, 21.4443, 1.7713>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 6
		    vSpawnPoint = <<-36.4630, 23.2869, 1.8859>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 7
		    vSpawnPoint = <<-36.8378, 24.5972, 1.9293>>
		    fSpawnHeading = 259.0315
		    RETURN TRUE
		BREAK
		CASE 8
		    vSpawnPoint = <<-36.6096, 20.4629, 1.9028>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 9
		    vSpawnPoint = <<-35.1774, 24.9482, 1.7369>>
		    fSpawnHeading = 238.5516
		    RETURN TRUE
		BREAK
		CASE 10
		    vSpawnPoint = <<-34.6766, 22.5652, 1.6789>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 11
		    vSpawnPoint = <<-34.6763, 20.3347, 1.6788>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 12
		    vSpawnPoint = <<-33.4325, 23.5918, 1.5347>>
		    fSpawnHeading = 263.0840
		    RETURN TRUE
		BREAK
		CASE 13
		    vSpawnPoint = <<-32.8411, 20.7271, 1.4662>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 14
		    vSpawnPoint = <<-32.4167, 24.9827, 1.4170>>
		    fSpawnHeading = 244.9071
		    RETURN TRUE
		BREAK
		CASE 15
		    vSpawnPoint = <<-29.9016, 21.3628, 1.1461>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 16
		    vSpawnPoint = <<-24.1469, 23.4170, 0.8562>>
		    fSpawnHeading = 192.3495
		    RETURN TRUE
		BREAK
		CASE 17
		    vSpawnPoint = <<-29.8304, 23.6025, 1.1389>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 18
		    vSpawnPoint = <<-32.1901, 22.3757, 1.3908>>
		    fSpawnHeading = 269.1100
		    RETURN TRUE
		BREAK
		CASE 19
		    vSpawnPoint = <<-29.0142, 25.2295, 1.0569>>
		    fSpawnHeading = 218.5960
		    RETURN TRUE
		BREAK
		CASE 20
		    vSpawnPoint = <<-23.0443, 25.0767, 0.8562>>
		    fSpawnHeading = 186.5633
		    RETURN TRUE
		BREAK
		CASE 21
		    vSpawnPoint = <<-26.0842, 25.2500, 0.8563>>
		    fSpawnHeading = 186.3713
		    RETURN TRUE
		BREAK
		CASE 22
		    vSpawnPoint = <<-23.7787, 21.0752, 0.8562>>
		    fSpawnHeading = 177.9144
		    RETURN TRUE
		BREAK
		CASE 23
		    vSpawnPoint = <<-26.9552, 23.5945, 0.8569>>
		    fSpawnHeading = 220.5313
		    RETURN TRUE
		BREAK
		CASE 24
		    vSpawnPoint = <<-21.6631, 23.0059, 0.8562>>
		    fSpawnHeading = 176.8853
		    RETURN TRUE
		BREAK
		CASE 25
		    vSpawnPoint = <<-25.2838, 18.8896, 0.8562>>
		    fSpawnHeading = 177.6983
		    RETURN TRUE
		BREAK
		CASE 26
		    vSpawnPoint = <<-22.6775, 19.0574, 0.8562>>
		    fSpawnHeading = 181.4100
		    RETURN TRUE
		BREAK
		CASE 27
		    vSpawnPoint = <<-26.3688, 21.3672, 0.8573>>
		    fSpawnHeading = 227.9097
		    RETURN TRUE
		BREAK
		CASE 28
		    vSpawnPoint = <<-26.8473, 17.0657, 0.8562>>
		    fSpawnHeading = 188.8993
		    RETURN TRUE
		BREAK
		CASE 29
		    vSpawnPoint = <<-21.3531, 20.7986, 0.8562>>
		    fSpawnHeading = 181.4518
		    RETURN TRUE
		BREAK
		CASE 30
		    vSpawnPoint = <<-24.2178, 17.1155, 0.8562>>
		    fSpawnHeading = 172.1686
		    RETURN TRUE
		BREAK
		CASE 31
		    vSpawnPoint = <<-21.1200, 17.1677, 0.8564>>
		    fSpawnHeading = 173.0458
		    RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_BUSINESS_HUB_WAREHOUSE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0
		    vSpawnPoint = <<-14.5839, 33.8071, -1.4960>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 1
		    vSpawnPoint = <<-15.6731, 32.7949, -1.3977>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 2
		    vSpawnPoint = <<-15.6460, 34.9124, -1.4001>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 3
		    vSpawnPoint = <<-14.2758, 31.6885, -1.5238>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 4
		    vSpawnPoint = <<-14.7838, 36.0146, -1.4779>>
		    fSpawnHeading = 253.5126
		    RETURN TRUE
		BREAK
		CASE 5
		    vSpawnPoint = <<-13.1309, 34.9475, -1.6272>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 6
		    vSpawnPoint = <<-12.2076, 31.6799, -1.7105>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 7
		    vSpawnPoint = <<-11.4895, 35.1426, -1.7754>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 8
		    vSpawnPoint = <<-12.7798, 33.1919, -1.6589>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 9
		    vSpawnPoint = <<-12.1542, 36.2573, -1.7154>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 10
		    vSpawnPoint = <<-11.5009, 33.3804, -1.7743>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 11
		    vSpawnPoint = <<-9.4705, 32.8247, -1.9577>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 12
		    vSpawnPoint = <<-10.0829, 34.7610, -1.9024>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 13
		    vSpawnPoint = <<-9.9032, 31.1741, -1.9186>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 14
		    vSpawnPoint = <<-9.4163, 36.3704, -1.9626>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 15
		    vSpawnPoint = <<-7.3374, 33.0803, -2.1502>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 16
		    vSpawnPoint = <<-4.5875, 32.0403, -2.3985>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 17
		    vSpawnPoint = <<-8.7208, 34.5339, -2.0254>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 18
		    vSpawnPoint = <<-6.4971, 31.7668, -2.2261>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 19
		    vSpawnPoint = <<-6.9985, 35.6946, -2.1808>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 20
		    vSpawnPoint = <<-2.0591, 33.9353, -2.6194>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 21
		    vSpawnPoint = <<-1.2148, 32.2000, -2.6908>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 22
		    vSpawnPoint = <<-5.6438, 34.1873, -2.3031>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 23
		    vSpawnPoint = <<-0.2605, 35.7424, -2.7715>>
		    fSpawnHeading = 221.8752
		    RETURN TRUE
		BREAK
		CASE 24
		    vSpawnPoint = <<-4.2930, 35.7131, -2.4250>>
		    fSpawnHeading = 269.6850
		    RETURN TRUE
		BREAK
		CASE 25
		    vSpawnPoint = <<1.9846, 33.4341, -2.9613>>
		    fSpawnHeading = 236.8837
		    RETURN TRUE
		BREAK
		CASE 26
		    vSpawnPoint = <<3.1359, 35.8684, -3.0586>>
		    fSpawnHeading = 227.7909
		    RETURN TRUE
		BREAK
		CASE 27
		    vSpawnPoint = <<5.4558, 31.9724, -3.2074>>
		    fSpawnHeading = 206.6983
		    RETURN TRUE
		BREAK
		CASE 28
		    vSpawnPoint = <<5.4746, 34.1313, -3.2074>>
		    fSpawnHeading = 197.3068
		    RETURN TRUE
		BREAK
		CASE 29
		    vSpawnPoint = <<8.9187, 33.9951, -3.2074>>
		    fSpawnHeading = 192.3477
		    RETURN TRUE
		BREAK
		CASE 30
		    vSpawnPoint = <<8.0620, 31.1130, -3.2074>>
		    fSpawnHeading = 182.9374
		    RETURN TRUE
		BREAK
		CASE 31
		    vSpawnPoint = <<10.3676, 31.0862, -3.2074>>
		    fSpawnHeading = 175.7537
		    RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_WAREHOUSE_INTERIOR_TRUCK_DOOR_SPAWN_POIN(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle)
	UNUSED_PARAMETER(bSpawnInVehicle)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-5.5183, 21.9490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-4.0183, 21.9490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-7.0183, 21.9490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-2.5183, 21.9490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-8.5183, 21.9490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-5.5183, 23.4490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-4.0183, 23.4490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-7.0183, 23.4490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-2.5183, 23.4490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-8.5183, 23.4490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-5.5183, 24.9490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-4.0183, 24.9490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-7.0183, 24.9490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-2.5183, 24.9490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-8.5183, 24.9490, -3.2074>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_BUSINESS_HUB_INTERIOR_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle, BOOL bUseSecondaryInteriorDetails = FALSE)
	UNUSED_PARAMETER(bSpawnInVehicle)
	
	IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPECIAL_SPAWN_CLUB_DANCE_FLOOR)		
	AND GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() != ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
		CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPECIAL_SPAWN_CLUB_DANCE_FLOOR)
		PRINTLN("GET_BUSINESS_HUB_INTERIOR_SPAWN_POINT - Clearing BS5_SIMPLE_INTERIOR_SPECIAL_SPAWN_CLUB_DANCE_FLOOR - Wrong enterance")
	ENDIF
	
	IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPECIAL_SPAWN_CLUB_DANCE_FLOOR)
		IF iSpawnPoint = 0
			vSpawnPoint = << 13.179, -1.02417, -0.00609589 >>
			fSpawnHeading = 74.0804	
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
		
	ELIF IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_MOVE_FROM_TRUCK_TO_HUB_TRUCK_FLOOR)
		RETURN GET_WAREHOUSE_INTERIOR_TRUCK_DOOR_SPAWN_POIN(eSimpleInteriorID, iSpawnPoint, vSpawnPoint, fSpawnHeading, bSpawnInVehicle)
	ELIF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
	OR GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
		RETURN GET_NIGHTCLUB_INTERIOR_SPAWN_POINT(eSimpleInteriorID, iSpawnPoint, vSpawnPoint, fSpawnHeading, bSpawnInVehicle)
	ELSE		
		IF bUseSecondaryInteriorDetails			
			IF SHOULD_SPAWN_AT_GARAGE_ELEVATOR()
				RETURN GET_NIGHTCLUB_GARAGE_ELEVATOR_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)
			ELIF SHOULD_SPAWN_AT_WAREHOUSE_ELEVATOR()
				RETURN GET_NIGHTCLUB_WAREHOUSE_ELEVATOR_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)
			ELIF SHOULD_SPAWN_AT_VEHICLE_ELEVATOR()
				RETURN GET_NIGHTCLUB_VEHICLE_ELEVATOR_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)				
			ENDIF
			
			// Used warehouse entrance on foot = spawn closer to the door
			IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
				RETURN GET_BUSINESS_HUB_NIGHTCLUB_GARAGE_ENTRANCE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)
			ENDIF
			
			SWITCH eSimpleInteriorID
				CASE SIMPLE_INTERIOR_HUB_LA_MESA
				CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
				CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
				CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
				CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
				CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
				CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
				CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
				CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
				CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS					
					SWITCH iSpawnPoint
						CASE 0	vSpawnPoint = <<-23.5607, 17.4492, 0.8561>> 	fSpawnHeading = 180.0	RETURN TRUE 
						CASE 1	vSpawnPoint = <<-21.6725, 18.1150, 0.8561>> 	fSpawnHeading = 180.0	RETURN TRUE
						CASE 2	vSpawnPoint = <<-25.3915, 18.3347, 0.8561>> 	fSpawnHeading = 180.0	RETURN TRUE 
						CASE 3  vSpawnPoint = <<-22.5433, 19.5532, 0.8561>>  	fSpawnHeading = 180.0	RETURN TRUE
						CASE 4  vSpawnPoint = <<-24.2112, 19.6467, 0.8567>>  	fSpawnHeading = 180.0	RETURN TRUE
					ENDSWITCH
				BREAK
			ENDSWITCH
		ELSE
			IF SHOULD_SPAWN_AT_GARAGE_ELEVATOR()
				RETURN GET_BUSINESS_HUB_GARAGE_ELEVATOR_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)
			ELIF SHOULD_SPAWN_AT_WAREHOUSE_ELEVATOR()
				RETURN GET_BUSINESS_HUB_WAREHOUSE_ELEVATOR_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)
			ELSE
				// In business hub the vehicle elevator has no special spawn points
				CLEAR_BIT(g_simpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_SPAWN_AT_VEHICLE_ELEVATOR)
				RETURN GET_BUSINESS_HUB_WAREHOUSE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS GET_BUSINESS_HUB_CUSTOM_INTERIOR_WARP_PARAMS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS eParams
	
	eParams.bIgnoreObstructions 	= FALSE
	eParams.bIgnoreDistanceChecks 	= TRUE
	eParams.fPedClearanceRadius 	= 1.0
		
	RETURN eParams
ENDFUNC

FUNC BOOL GET_BUSINESS_HUB_EXTERIOR_SPAWN_POINT_ON_FOOT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<728.3930, -1291.4270, 25.2840>> fSpawnHeading = 87.7980	BREAK
				CASE 1			vSpawnPoint = <<728.4470, -1288.6860, 25.2840>> fSpawnHeading = 87.7980	BREAK 
				CASE 2			vSpawnPoint = <<728.3330, -1294.5150, 25.2850>> fSpawnHeading = 87.7980 BREAK
				CASE 3			vSpawnPoint = <<730.6590, -1287.0270, 25.2840>> fSpawnHeading = 87.7980	BREAK
				CASE 4			vSpawnPoint = <<728.6270, -1285.3621, 25.2840>> fSpawnHeading = 87.7980	BREAK
				CASE 5			vSpawnPoint = <<726.5630, -1290.1071, 25.2840>> fSpawnHeading = 87.7980	BREAK
				CASE 6			vSpawnPoint = <<726.5060, -1293.0310, 25.2840>> fSpawnHeading = 87.7980	BREAK
				CASE 7			vSpawnPoint = <<726.6250, -1286.9470, 25.2840>> fSpawnHeading = 87.7980	BREAK
				CASE 8			vSpawnPoint = <<724.7850, -1291.6870, 25.2840>> fSpawnHeading = 87.7980	BREAK
				CASE 9			vSpawnPoint = <<724.7210, -1295.1400, 25.2760>> fSpawnHeading = 87.7980	BREAK
				CASE 10			vSpawnPoint = <<724.8470, -1288.4990, 25.2840>> fSpawnHeading = 87.7980	BREAK
				CASE 11			vSpawnPoint = <<726.6040, -1296.8831, 25.2680>> fSpawnHeading = 87.7980	BREAK
				CASE 12			vSpawnPoint = <<725.1340, -1285.2280, 25.2840>> fSpawnHeading = 87.7980 BREAK
				CASE 13			vSpawnPoint = <<726.9330, -1283.5040, 25.2840>> fSpawnHeading = 87.7980 BREAK
				CASE 14			vSpawnPoint = <<730.4560, -1283.5680, 25.2850>> fSpawnHeading = 87.7980 BREAK
				CASE 15			vSpawnPoint = <<722.7140, -1293.2450, 25.2840>> fSpawnHeading = 87.7980 BREAK
				CASE 16			vSpawnPoint = <<722.7740, -1290.1580, 25.2840>> fSpawnHeading = 87.7980 BREAK
				CASE 17			vSpawnPoint = <<722.8350, -1287.0551, 25.2840>> fSpawnHeading = 87.7980 BREAK
				CASE 18			vSpawnPoint = <<722.9030, -1283.5840, 25.2840>> fSpawnHeading = 87.7980 BREAK
				CASE 19			vSpawnPoint = <<725.0860, -1281.8090, 25.2840>> fSpawnHeading = 87.7980 BREAK
				CASE 20			vSpawnPoint = <<728.7590, -1281.9550, 25.2850>> fSpawnHeading = 87.7980 BREAK
				CASE 21			vSpawnPoint = <<722.9870, -1296.9390, 25.2630>> fSpawnHeading = 87.7980 BREAK
				CASE 22			vSpawnPoint = <<732.3910, -1285.1970, 25.2870>> fSpawnHeading = 87.7980 BREAK
				CASE 23			vSpawnPoint = <<732.4240, -1281.7271, 25.2880>> fSpawnHeading = 87.7980 BREAK
				CASE 24			vSpawnPoint = <<729.2840, -1297.6720, 25.2860>> fSpawnHeading = 87.7980 BREAK
				CASE 25			vSpawnPoint = <<724.7880, -1298.7531, 25.2510>> fSpawnHeading = 87.7980 BREAK
				CASE 26			vSpawnPoint = <<726.8050, -1300.3040, 25.2710>> fSpawnHeading = 87.7980 BREAK
				CASE 27			vSpawnPoint = <<723.1480, -1300.6110, 25.2380>> fSpawnHeading = 87.7980 BREAK
				CASE 28			vSpawnPoint = <<722.9540, -1280.6440, 25.2840>> fSpawnHeading = 87.7980 BREAK
				CASE 29			vSpawnPoint = <<730.7170, -1294.3190, 25.2850>> fSpawnHeading = 87.7980 BREAK
				CASE 30			vSpawnPoint = <<720.9573, -1291.9423, 25.2302>> fSpawnHeading = 87.7980 BREAK
				CASE 31			vSpawnPoint = <<721.1570, -1295.3344, 25.2323>> fSpawnHeading = 87.7980 BREAK
				CASE 32			vSpawnPoint = <<721.0391, -1288.4813, 25.2391>> fSpawnHeading = 87.7980 BREAK
				CASE 33			vSpawnPoint = <<720.9652, -1285.3038, 25.2374>> fSpawnHeading = 87.7980 BREAK
				CASE 34			vSpawnPoint = <<720.9918, -1282.0511, 25.2452>> fSpawnHeading = 87.7980 BREAK
				CASE 35			vSpawnPoint = <<721.1436, -1279.0151, 25.2513>> fSpawnHeading = 87.7980 BREAK
				CASE 36			vSpawnPoint = <<730.6931, -1300.0471, 25.2885>> fSpawnHeading = 87.7980 BREAK
				CASE 37			vSpawnPoint = <<721.2027, -1298.8364, 25.2105>> fSpawnHeading = 87.7980 BREAK
				CASE 38			vSpawnPoint = <<729.1777, -1302.3302, 25.2865>> fSpawnHeading = 87.7980 BREAK
				CASE 39			vSpawnPoint = <<725.1928, -1302.7246, 25.2527>> fSpawnHeading = 87.7980 BREAK
				CASE 40			vSpawnPoint = <<721.3691, -1302.8234, 25.1917>> fSpawnHeading = 87.7980 BREAK
				CASE 41			vSpawnPoint = <<719.0273, -1293.6638, 25.1260>> fSpawnHeading = 87.7980 BREAK
				CASE 42			vSpawnPoint = <<718.9683, -1290.2452, 25.1283>> fSpawnHeading = 87.7980 BREAK
				CASE 43			vSpawnPoint = <<718.9293, -1286.8567, 25.1411>> fSpawnHeading = 87.7980 BREAK
				CASE 44			vSpawnPoint = <<719.1067, -1283.8153, 25.1623>> fSpawnHeading = 87.7980 BREAK
				CASE 45			vSpawnPoint = <<719.0168, -1297.1580, 25.1057>> fSpawnHeading = 87.7980 BREAK
				CASE 46			vSpawnPoint = <<719.0475, -1301.1517, 25.0809>> fSpawnHeading = 87.7980 BREAK
				CASE 47			vSpawnPoint = <<719.1120, -1280.6718, 25.1691>> fSpawnHeading = 87.7980 BREAK
				CASE 48			vSpawnPoint = <<726.9939, -1305.0908, 25.2876>> fSpawnHeading = 87.7980 BREAK
				CASE 49			vSpawnPoint = <<723.1162, -1305.0892, 25.2452>> fSpawnHeading = 87.7980 BREAK
				CASE 50			vSpawnPoint = <<730.6934, -1304.8235, 26.2170>> fSpawnHeading = 87.7980	BREAK
				CASE 51			vSpawnPoint = <<718.2906, -1305.0800, 25.9597>> fSpawnHeading = 87.7980	BREAK
				CASE 52			vSpawnPoint = <<716.8799, -1292.1863, 25.9395>> fSpawnHeading = 87.7980	BREAK
				CASE 53			vSpawnPoint = <<716.8994, -1288.6855, 25.9635>> fSpawnHeading = 87.7980	BREAK
				CASE 54			vSpawnPoint = <<716.8294, -1285.3352, 25.9825>> fSpawnHeading = 87.7980	BREAK
				CASE 55			vSpawnPoint = <<717.0021, -1282.3021, 26.0092>> fSpawnHeading = 87.7980	BREAK
				CASE 56			vSpawnPoint = <<716.7923, -1295.1846, 25.9218>> fSpawnHeading = 87.7980	BREAK
				CASE 57			vSpawnPoint = <<716.4890, -1299.0837, 25.8815>> fSpawnHeading = 87.7980	BREAK
				CASE 58			vSpawnPoint = <<716.1834, -1303.0094, 25.8410>> fSpawnHeading = 87.7980	BREAK
				CASE 59			vSpawnPoint = <<717.2813, -1278.4984, 26.0421>> fSpawnHeading = 87.7980	BREAK
				CASE 60			vSpawnPoint = <<714.8629, -1290.5018, 25.8989>> fSpawnHeading = 87.7980	BREAK
				CASE 61			vSpawnPoint = <<714.6507, -1293.2643, 25.8784>> fSpawnHeading = 87.7980	BREAK
				CASE 62			vSpawnPoint = <<714.7620, -1287.1029, 25.9236>> fSpawnHeading = 87.7980	BREAK
				CASE 63			vSpawnPoint = <<714.8115, -1283.7489, 25.9596>> fSpawnHeading = 87.7980	BREAK
				CASE 64			vSpawnPoint = <<714.4922, -1297.2108, 25.8528>> fSpawnHeading = 87.7980	BREAK
				CASE 65			vSpawnPoint = <<714.2017, -1300.9935, 25.8240>> fSpawnHeading = 87.7980	BREAK
				CASE 66			vSpawnPoint = <<714.9728, -1280.4734, 25.9803>> fSpawnHeading = 87.7980	BREAK
				CASE 67			vSpawnPoint = <<713.9761, -1304.9777, 25.8108>> fSpawnHeading = 87.7980	BREAK
				CASE 68			vSpawnPoint = <<712.5655, -1291.7915, 25.8778>> fSpawnHeading = 87.7980	BREAK
				CASE 69			vSpawnPoint = <<712.5852, -1288.7853, 25.9004>> fSpawnHeading = 87.7980	BREAK
				CASE 70			vSpawnPoint = <<712.5397, -1285.3148, 25.9365>> fSpawnHeading = 87.7980	BREAK
				CASE 71			vSpawnPoint = <<712.4511, -1295.4462, 25.8513>> fSpawnHeading = 87.7980	BREAK
				DEFAULT 		RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<333.2660, -998.6100, 28.1840>> fSpawnHeading = 185.2000	BREAK
				CASE 1			vSpawnPoint = <<331.2940, -998.5960, 28.1770>> fSpawnHeading = 185.2000	BREAK
				CASE 2			vSpawnPoint = <<335.5120, -998.6080, 28.2100>> fSpawnHeading = 185.2000	BREAK
				CASE 3			vSpawnPoint = <<334.5680, -999.8390, 28.2780>> fSpawnHeading = 185.2000	BREAK
				CASE 4			vSpawnPoint = <<332.2830, -999.8170, 28.2470>> fSpawnHeading = 185.2000	BREAK
				CASE 5			vSpawnPoint = <<329.9490, -999.6080, 28.2210>> fSpawnHeading = 185.2000	BREAK
				CASE 6			vSpawnPoint = <<328.8530, -998.0090, 28.1420>> fSpawnHeading = 185.2000	BREAK
				CASE 7			vSpawnPoint = <<328.7640, -996.1260, 28.2320>> fSpawnHeading = 185.2000	BREAK
				CASE 8			vSpawnPoint = <<333.4200, -1000.8630, 28.3220>> fSpawnHeading = 185.2000	BREAK
				CASE 9			vSpawnPoint = <<336.3540, -1001.1870, 28.3560>> fSpawnHeading = 185.2000	BREAK
				CASE 10			vSpawnPoint = <<330.8920, -1001.0040, 28.2880>> fSpawnHeading = 185.2000	BREAK
				CASE 11			vSpawnPoint = <<328.3100, -1000.9700, 28.2740>> fSpawnHeading = 185.2000	BREAK
				CASE 12			vSpawnPoint = <<329.7940, -1002.2280, 28.2980>> fSpawnHeading = 185.2000	BREAK
				CASE 13			vSpawnPoint = <<332.2980, -1002.1600, 28.3270>> fSpawnHeading = 185.2000	BREAK
				CASE 14			vSpawnPoint = <<335.0090, -1002.3160, 28.3620>> fSpawnHeading = 185.2000	BREAK
				CASE 15			vSpawnPoint = <<328.0800, -1003.7060, 28.2960>> fSpawnHeading = 185.2000	BREAK
				CASE 16			vSpawnPoint = <<331.1120, -1003.3930, 28.3080>> fSpawnHeading = 185.2000	BREAK
				CASE 17			vSpawnPoint = <<333.6870, -1003.5660, 28.3450>> fSpawnHeading = 190.8000	BREAK
				CASE 18			vSpawnPoint = <<336.2590, -1003.7150, 28.3680>> fSpawnHeading = 190.8000	BREAK
				CASE 19			vSpawnPoint = <<337.1930, -996.3740, 28.2600>> fSpawnHeading = 190.8000	BREAK
				CASE 20			vSpawnPoint = <<326.3400, -996.8060, 28.1840>> fSpawnHeading = 107.9990	BREAK
				CASE 21			vSpawnPoint = <<326.9190, -999.7380, 28.2190>> fSpawnHeading = 107.9990	BREAK
				CASE 22			vSpawnPoint = <<326.7500, -1002.5080, 28.2960>> fSpawnHeading = 107.9990	BREAK
				CASE 23			vSpawnPoint = <<325.2080, -1001.1770, 28.2700>> fSpawnHeading = 107.9990	BREAK
				CASE 24			vSpawnPoint = <<325.0030, -998.7890, 28.1740>> fSpawnHeading = 107.9990	BREAK
				CASE 25			vSpawnPoint = <<337.3180, -999.0030, 28.2510>> fSpawnHeading = 279.1980	BREAK
				CASE 26			vSpawnPoint = <<340.2340, -996.1600, 28.2770>> fSpawnHeading = 279.1980	BREAK
				CASE 27			vSpawnPoint = <<339.9260, -998.8700, 28.2390>> fSpawnHeading = 279.1980	BREAK
				CASE 28			vSpawnPoint = <<343.4670, -996.2970, 28.2910>> fSpawnHeading = 279.1980	BREAK
				CASE 29			vSpawnPoint = <<332.3110, -1005.0760, 28.3220>> fSpawnHeading = 194.5980	BREAK
				CASE 30			vSpawnPoint = <<335.1170, -1005.0100, 28.3280>> fSpawnHeading = 194.5980	BREAK
				CASE 31			vSpawnPoint = <<329.6800, -1004.9350, 28.2940>> fSpawnHeading = 194.5980	BREAK
				CASE 32			vSpawnPoint = <<330.9780, -1006.6710, 28.3000>> fSpawnHeading = 194.5980	BREAK
				CASE 33			vSpawnPoint = <<333.7500, -1006.6380, 28.3030>> fSpawnHeading = 194.5980	BREAK
				CASE 34			vSpawnPoint = <<336.5990, -1006.7160, 28.3470>> fSpawnHeading = 194.5980	BREAK
				CASE 35			vSpawnPoint = <<328.1860, -1007.0370, 28.2920>> fSpawnHeading = 194.5980	BREAK
				CASE 36			vSpawnPoint = <<329.5780, -1008.3140, 28.2910>> fSpawnHeading = 194.5980	BREAK
				CASE 37			vSpawnPoint = <<332.4960, -1007.9930, 28.2890>> fSpawnHeading = 194.5980	BREAK
				CASE 38			vSpawnPoint = <<335.3060, -1008.2250, 28.2990>> fSpawnHeading = 194.5980	BREAK
				CASE 39			vSpawnPoint = <<325.4410, -1004.1610, 28.2960>> fSpawnHeading = 103.3980	BREAK
				CASE 40			vSpawnPoint = <<323.7280, -1002.7210, 28.2970>> fSpawnHeading = 103.3980	BREAK
				CASE 41			vSpawnPoint = <<323.4870, -999.9350, 28.2270>> fSpawnHeading = 103.3980	BREAK
				CASE 42			vSpawnPoint = <<323.6520, -996.9130, 28.1760>> fSpawnHeading = 103.3980	BREAK
				CASE 43			vSpawnPoint = <<341.8150, -997.6200, 28.1560>> fSpawnHeading = 279.1980	BREAK
				CASE 44			vSpawnPoint = <<343.2900, -998.9780, 28.2680>> fSpawnHeading = 279.1980	BREAK
				CASE 45			vSpawnPoint = <<338.6140, -997.5880, 28.1400>> fSpawnHeading = 279.1980	BREAK
				CASE 46			vSpawnPoint = <<333.8210, -1009.8900, 28.2920>> fSpawnHeading = 188.9980	BREAK
				CASE 47			vSpawnPoint = <<331.2940, -1009.9230, 28.2920>> fSpawnHeading = 188.9980	BREAK
				CASE 48			vSpawnPoint = <<328.5870, -1010.0670, 28.2920>> fSpawnHeading = 188.9980	BREAK
				CASE 49			vSpawnPoint = <<336.3170, -1010.3040, 28.3080>> fSpawnHeading = 188.9980	BREAK
				CASE 50			vSpawnPoint = <<331.2921, -1013.1783, 29.2149>> fSpawnHeading = 185.2000	BREAK
				CASE 51			vSpawnPoint = <<328.8104, -1013.8946, 29.2134>> fSpawnHeading = 185.2000	BREAK
				CASE 52			vSpawnPoint = <<332.7029, -1014.7418, 29.2155>> fSpawnHeading = 185.2000	BREAK
				CASE 53			vSpawnPoint = <<330.3870, -1015.5615, 29.2127>> fSpawnHeading = 185.2000	BREAK
				CASE 54			vSpawnPoint = <<327.6153, -1017.1484, 29.2093>> fSpawnHeading = 185.2000	BREAK
				CASE 55			vSpawnPoint = <<330.5898, -1018.3941, 29.2112>> fSpawnHeading = 185.2000	BREAK
				CASE 56			vSpawnPoint = <<332.7358, -1017.1910, 29.2154>> fSpawnHeading = 185.2000	BREAK
				CASE 57			vSpawnPoint = <<328.5630, -1019.8038, 29.2069>> fSpawnHeading = 185.2000	BREAK
				CASE 58			vSpawnPoint = <<332.6137, -1019.8234, 29.2150>> fSpawnHeading = 185.2000	BREAK
				CASE 59			vSpawnPoint = <<330.6239, -1021.2544, 29.2090>> fSpawnHeading = 185.2000	BREAK
				CASE 60			vSpawnPoint = <<328.4568, -1022.5253, 29.2006>> fSpawnHeading = 185.2000	BREAK
				CASE 61			vSpawnPoint = <<332.7243, -1022.3547, 29.2123>> fSpawnHeading = 185.2000	BREAK
				CASE 62			vSpawnPoint = <<330.5182, -1024.0466, 29.2042>> fSpawnHeading = 185.2000	BREAK
				CASE 63			vSpawnPoint = <<328.3309, -1025.5444, 29.1935>> fSpawnHeading = 185.2000	BREAK
				CASE 64			vSpawnPoint = <<332.7657, -1025.3719, 29.2119>> fSpawnHeading = 185.2000	BREAK
				CASE 65			vSpawnPoint = <<330.7448, -1027.5376, 29.1995>> fSpawnHeading = 185.2000	BREAK
				CASE 66			vSpawnPoint = <<327.9280, -1028.8185, 29.1845>> fSpawnHeading = 185.2000	BREAK
				CASE 67			vSpawnPoint = <<332.9699, -1028.9690, 29.2123>> fSpawnHeading = 185.2000	BREAK
				CASE 68			vSpawnPoint = <<336.2973, -1029.0376, 29.2327>> fSpawnHeading = 185.2000	BREAK
				CASE 69			vSpawnPoint = <<330.7109, -1030.3253, 29.1750>> fSpawnHeading = 185.2000	BREAK
				CASE 70			vSpawnPoint = <<334.7790, -1030.9636, 29.1825>> fSpawnHeading = 185.2000	BREAK
				CASE 71			vSpawnPoint = <<332.7428, -1031.9407, 29.1454>> fSpawnHeading = 185.2000	BREAK
				DEFAULT			RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<-164.2680, -1297.5050, 30.0550>> fSpawnHeading = 182.0000	BREAK
				CASE 1			vSpawnPoint = <<-166.5350, -1297.5060, 30.0620>> fSpawnHeading = 182.0000	BREAK
				CASE 2			vSpawnPoint = <<-161.9170, -1297.5100, 30.0490>> fSpawnHeading = 182.0000	BREAK
				CASE 3			vSpawnPoint = <<-165.3910, -1299.3149, 30.2010>> fSpawnHeading = 182.0000	BREAK
				CASE 4			vSpawnPoint = <<-163.1000, -1299.3140, 30.1990>> fSpawnHeading = 182.0000	BREAK
				CASE 5			vSpawnPoint = <<-167.9550, -1299.3640, 30.2050>> fSpawnHeading = 182.0000	BREAK
				CASE 6			vSpawnPoint = <<-160.4880, -1299.2531, 30.1910>> fSpawnHeading = 182.0000	BREAK
				CASE 7			vSpawnPoint = <<-159.3270, -1297.6820, 30.0680>> fSpawnHeading = 182.0000	BREAK
				CASE 8			vSpawnPoint = <<-169.0350, -1297.6310, 30.0790>> fSpawnHeading = 182.0000	BREAK
				CASE 9			vSpawnPoint = <<-167.8900, -1296.0520, 30.0750>> fSpawnHeading = 182.0000	BREAK
				CASE 10			vSpawnPoint = <<-160.6230, -1296.0500, 30.0670>> fSpawnHeading = 182.0000	BREAK
				CASE 11			vSpawnPoint = <<-159.2980, -1294.5341, 30.2430>> fSpawnHeading = 182.0000	BREAK
				CASE 12			vSpawnPoint = <<-157.8180, -1296.1169, 30.0650>> fSpawnHeading = 182.0000	BREAK
				CASE 13			vSpawnPoint = <<-156.0410, -1294.6450, 30.2290>> fSpawnHeading = 182.0000	BREAK
				CASE 14			vSpawnPoint = <<-155.9210, -1297.6121, 30.0740>> fSpawnHeading = 182.0000	BREAK
				CASE 15			vSpawnPoint = <<-157.4460, -1299.1000, 30.1800>> fSpawnHeading = 182.0000	BREAK
				CASE 16			vSpawnPoint = <<-170.1500, -1295.9611, 30.0900>> fSpawnHeading = 182.0000	BREAK
				CASE 17			vSpawnPoint = <<-170.2470, -1299.2700, 30.1980>> fSpawnHeading = 182.0000	BREAK
				CASE 18			vSpawnPoint = <<-171.3780, -1297.5870, 30.0790>> fSpawnHeading = 182.0000	BREAK
				CASE 19			vSpawnPoint = <<-172.5370, -1295.8750, 30.1030>> fSpawnHeading = 182.0000	BREAK
				CASE 20			vSpawnPoint = <<-172.8620, -1299.1930, 30.1930>> fSpawnHeading = 182.0000	BREAK
				CASE 21			vSpawnPoint = <<-154.4480, -1296.1000, 30.0740>> fSpawnHeading = 182.0000	BREAK
				CASE 22			vSpawnPoint = <<-154.3720, -1299.2960, 30.2000>> fSpawnHeading = 182.0000	BREAK
				CASE 23			vSpawnPoint = <<-164.2010, -1301.0050, 30.3160>> fSpawnHeading = 182.0000	BREAK
				CASE 24			vSpawnPoint = <<-166.9730, -1301.0140, 30.3240>> fSpawnHeading = 182.0000	BREAK
				CASE 25			vSpawnPoint = <<-161.6080, -1301.0930, 30.3190>> fSpawnHeading = 182.0000	BREAK
				CASE 26			vSpawnPoint = <<-158.7290, -1301.0270, 30.3180>> fSpawnHeading = 182.0000	BREAK
				CASE 27			vSpawnPoint = <<-169.3170, -1301.0490, 30.3250>> fSpawnHeading = 182.0000	BREAK
				CASE 28			vSpawnPoint = <<-171.5950, -1300.8770, 30.3210>> fSpawnHeading = 182.0000	BREAK
				CASE 29			vSpawnPoint = <<-156.0360, -1300.8940, 30.3090>> fSpawnHeading = 182.0000	BREAK
				CASE 30			vSpawnPoint = <<-165.4039, -1302.6091, 30.3129>> fSpawnHeading = 182.0000	BREAK
				CASE 31			vSpawnPoint = <<-162.9012, -1302.6371, 30.3113>> fSpawnHeading = 182.0000	BREAK
				CASE 32			vSpawnPoint = <<-159.9545, -1302.5912, 30.3141>> fSpawnHeading = 182.0000	BREAK
				CASE 33			vSpawnPoint = <<-167.8722, -1302.7849, 30.3000>> fSpawnHeading = 182.0000	BREAK
				CASE 34			vSpawnPoint = <<-170.3674, -1302.9352, 30.2949>> fSpawnHeading = 182.0000	BREAK
				CASE 35			vSpawnPoint = <<-156.9811, -1302.6792, 30.3127>> fSpawnHeading = 182.0000	BREAK
				CASE 36			vSpawnPoint = <<-171.2502, -1294.5216, 30.2416>> fSpawnHeading = 182.0000	BREAK
				CASE 37			vSpawnPoint = <<-174.2852, -1297.3138, 30.0604>> fSpawnHeading = 182.0000	BREAK
				CASE 38			vSpawnPoint = <<-174.3014, -1300.6804, 30.3087>> fSpawnHeading = 182.0000	BREAK
				CASE 39			vSpawnPoint = <<-173.0214, -1302.7996, 30.2995>> fSpawnHeading = 182.0000	BREAK
				CASE 40			vSpawnPoint = <<-174.0119, -1294.6067, 30.2195>> fSpawnHeading = 182.0000	BREAK
				CASE 41			vSpawnPoint = <<-157.4944, -1293.4020, 30.2964>> fSpawnHeading = 182.0000	BREAK
				CASE 42			vSpawnPoint = <<-160.8387, -1293.8274, 30.2883>> fSpawnHeading = 182.0000	BREAK
				CASE 43			vSpawnPoint = <<-164.1364, -1303.9740, 30.2840>> fSpawnHeading = 182.0000	BREAK
				CASE 44			vSpawnPoint = <<-166.5016, -1303.9446, 30.2832>> fSpawnHeading = 182.0000	BREAK
				CASE 45			vSpawnPoint = <<-161.2926, -1303.9960, 30.2846>> fSpawnHeading = 182.0000	BREAK
				CASE 46			vSpawnPoint = <<-158.6273, -1304.1171, 30.2879>> fSpawnHeading = 182.0000	BREAK
				CASE 47			vSpawnPoint = <<-169.2955, -1304.2323, 30.2911>> fSpawnHeading = 182.0000	BREAK
				CASE 48			vSpawnPoint = <<-171.6691, -1304.2571, 30.2918>> fSpawnHeading = 182.0000	BREAK
				CASE 49			vSpawnPoint = <<-175.7554, -1299.1243, 30.1878>> fSpawnHeading = 182.0000	BREAK
				CASE 50			vSpawnPoint = <<-165.3046, -1305.7659, 31.2743>> fSpawnHeading = 182.000	BREAK
				CASE 51			vSpawnPoint = <<-162.7732, -1305.5847, 31.2670>> fSpawnHeading = 182.000	BREAK
				CASE 52			vSpawnPoint = <<-159.9757, -1305.6473, 31.2695>> fSpawnHeading = 182.000	BREAK
				CASE 53			vSpawnPoint = <<-167.9713, -1305.7573, 31.2739>> fSpawnHeading = 182.000	BREAK
				CASE 54			vSpawnPoint = <<-170.6086, -1305.8580, 31.3680>> fSpawnHeading = 182.000	BREAK
				CASE 55			vSpawnPoint = <<-156.6661, -1305.6062, 31.3749>> fSpawnHeading = 182.000	BREAK
				CASE 56			vSpawnPoint = <<-154.5843, -1304.0452, 31.7493>> fSpawnHeading = 182.000	BREAK
				CASE 57			vSpawnPoint = <<-173.5438, -1305.9424, 31.2635>> fSpawnHeading = 182.000	BREAK
				CASE 58			vSpawnPoint = <<-174.7512, -1304.2916, 31.2201>> fSpawnHeading = 182.000	BREAK
				CASE 59			vSpawnPoint = <<-176.0862, -1302.6342, 31.2292>> fSpawnHeading = 182.000	BREAK
				CASE 60			vSpawnPoint = <<-176.1438, -1305.8311, 31.2345>> fSpawnHeading = 182.000	BREAK
				CASE 61			vSpawnPoint = <<-152.6239, -1294.6262, 31.1519>> fSpawnHeading = 182.000	BREAK
				CASE 62			vSpawnPoint = <<-152.6735, -1297.5634, 31.0046>> fSpawnHeading = 182.000	BREAK
				CASE 63			vSpawnPoint = <<-153.6480, -1301.9329, 31.3048>> fSpawnHeading = 182.000	BREAK
				CASE 64			vSpawnPoint = <<-161.3284, -1307.3009, 31.1629>> fSpawnHeading = 182.000	BREAK
				CASE 65			vSpawnPoint = <<-164.0880, -1307.1848, 31.1721>> fSpawnHeading = 182.000	BREAK
				CASE 66			vSpawnPoint = <<-166.5856, -1307.2883, 31.1639>> fSpawnHeading = 182.000	BREAK
				CASE 67			vSpawnPoint = <<-169.1773, -1307.4404, 31.1708>> fSpawnHeading = 182.000	BREAK
				CASE 68			vSpawnPoint = <<-172.1050, -1307.2856, 31.1645>> fSpawnHeading = 182.000	BREAK
				CASE 69			vSpawnPoint = <<-158.1697, -1307.4102, 31.1684>> fSpawnHeading = 182.000	BREAK
				CASE 70			vSpawnPoint = <<-174.7332, -1307.3822, 31.1768>> fSpawnHeading = 182.000	BREAK
				CASE 71			vSpawnPoint = <<-177.4043, -1300.7222, 31.2351>> fSpawnHeading = 182.000	BREAK
				DEFAULT 		RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<-22.1360, 215.4560, 105.5630>> fSpawnHeading = 175.2000	BREAK
				CASE 1			vSpawnPoint = <<-24.7350, 215.8880, 105.5710>> fSpawnHeading = 175.2000	BREAK
				CASE 2			vSpawnPoint = <<-19.6210, 214.9850, 105.5640>> fSpawnHeading = 175.2000	BREAK
				CASE 3			vSpawnPoint = <<-17.8080, 216.3270, 105.5970>> fSpawnHeading = 163.1990	BREAK
				CASE 4			vSpawnPoint = <<-18.5780, 218.5370, 105.7440>> fSpawnHeading = 164.3990	BREAK
				CASE 5			vSpawnPoint = <<-25.8350, 217.5110, 105.5730>> fSpawnHeading = 164.3990	BREAK
				CASE 6			vSpawnPoint = <<-24.8480, 219.5210, 105.7460>> fSpawnHeading = 164.3990	BREAK
				CASE 7			vSpawnPoint = <<-21.0210, 213.4630, 105.5530>> fSpawnHeading = 164.3990	BREAK
				CASE 8			vSpawnPoint = <<-23.6520, 214.1460, 105.5240>> fSpawnHeading = 164.3990	BREAK
				CASE 9			vSpawnPoint = <<-18.4640, 213.1420, 105.5660>> fSpawnHeading = 164.3990	BREAK
				CASE 10			vSpawnPoint = <<-16.6420, 214.7630, 105.7310>> fSpawnHeading = 164.3990	BREAK
				CASE 11			vSpawnPoint = <<-26.6560, 214.4920, 105.5620>> fSpawnHeading = 164.3990	BREAK
				CASE 12			vSpawnPoint = <<-27.7860, 216.5250, 105.5570>> fSpawnHeading = 164.3990	BREAK
				CASE 13			vSpawnPoint = <<-16.1310, 217.6650, 105.7440>> fSpawnHeading = 164.3990	BREAK
				CASE 14			vSpawnPoint = <<-29.2050, 218.0970, 105.5530>> fSpawnHeading = 164.3990	BREAK
				CASE 15			vSpawnPoint = <<-29.7120, 215.1960, 105.5530>> fSpawnHeading = 164.3990	BREAK
				CASE 16			vSpawnPoint = <<-22.6330, 211.9550, 105.5540>> fSpawnHeading = 164.3990	BREAK
				CASE 17			vSpawnPoint = <<-25.1750, 212.4220, 105.5610>> fSpawnHeading = 164.3990	BREAK
				CASE 18			vSpawnPoint = <<-19.7690, 211.7110, 105.5480>> fSpawnHeading = 164.3990	BREAK
				CASE 19			vSpawnPoint = <<-28.1900, 213.1440, 105.5570>> fSpawnHeading = 164.3990	BREAK
				CASE 20			vSpawnPoint = <<-16.6600, 211.4520, 105.5740>> fSpawnHeading = 164.3990	BREAK
				CASE 21			vSpawnPoint = <<-21.2770, 210.1510, 105.5540>> fSpawnHeading = 168.3990	BREAK
				CASE 22			vSpawnPoint = <<-23.9730, 210.5930, 105.5530>> fSpawnHeading = 168.3990	BREAK
				CASE 23			vSpawnPoint = <<-26.9550, 211.4340, 105.5560>> fSpawnHeading = 168.3990	BREAK
				CASE 24			vSpawnPoint = <<-18.2980, 209.8410, 105.5550>> fSpawnHeading = 168.3990	BREAK
				CASE 25			vSpawnPoint = <<-30.7890, 217.3760, 105.5530>> fSpawnHeading = 168.3990	BREAK
				CASE 26			vSpawnPoint = <<-30.9820, 214.1290, 105.5530>> fSpawnHeading = 168.3990	BREAK
				CASE 27			vSpawnPoint = <<-14.9440, 212.7320, 105.5540>> fSpawnHeading = 211.9990	BREAK
				CASE 28			vSpawnPoint = <<-29.8430, 212.3570, 105.5530>> fSpawnHeading = 174.1990	BREAK
				CASE 29			vSpawnPoint = <<-19.6480, 208.4380, 105.5530>> fSpawnHeading = 171.1990	BREAK
				CASE 30			vSpawnPoint = <<-32.1249, 215.8862, 105.5534>> fSpawnHeading = 171.1990	BREAK
				CASE 31			vSpawnPoint = <<-22.6473, 209.1338, 105.5534>> fSpawnHeading = 171.1990	BREAK
				CASE 32			vSpawnPoint = <<-25.9273, 209.7839, 105.5534>> fSpawnHeading = 171.1990	BREAK
				CASE 33			vSpawnPoint = <<-28.4919, 210.4474, 105.5534>> fSpawnHeading = 171.1990	BREAK
				CASE 34			vSpawnPoint = <<-31.7479, 211.8708, 105.5534>> fSpawnHeading = 171.1990	BREAK
				CASE 35			vSpawnPoint = <<-32.8844, 213.6065, 105.5534>> fSpawnHeading = 171.1990	BREAK
				CASE 36			vSpawnPoint = <<-14.0012, 211.0354, 105.4821>> fSpawnHeading = 236.1988	BREAK
				CASE 37			vSpawnPoint = <<-15.9591, 209.1378, 105.4955>> fSpawnHeading = 236.1988	BREAK
				CASE 38			vSpawnPoint = <<-13.7679, 208.8383, 105.3126>> fSpawnHeading = 245.3988	BREAK
				CASE 39			vSpawnPoint = <<-11.9977, 211.7592, 105.4007>> fSpawnHeading = 245.3988	BREAK
				CASE 40			vSpawnPoint = <<-11.6364, 209.6326, 105.2317>> fSpawnHeading = 245.3988	BREAK
				CASE 41			vSpawnPoint = <<-12.2839, 207.4156, 105.1128>> fSpawnHeading = 245.3988	BREAK
				CASE 42			vSpawnPoint = <<-32.2683, 218.5820, 105.5534>> fSpawnHeading = 106.1986	BREAK
				CASE 43			vSpawnPoint = <<-33.5965, 217.4829, 105.5534>> fSpawnHeading = 106.1986	BREAK
				CASE 44			vSpawnPoint = <<-33.7862, 215.2295, 105.5534>> fSpawnHeading = 106.1986	BREAK
				CASE 45			vSpawnPoint = <<-33.5699, 211.8434, 105.5534>> fSpawnHeading = 127.1985	BREAK
				CASE 46			vSpawnPoint = <<-30.3665, 210.2545, 105.5534>> fSpawnHeading = 150.5983	BREAK
				CASE 47			vSpawnPoint = <<-27.3703, 208.8640, 105.5534>> fSpawnHeading = 159.5982	BREAK
				CASE 48			vSpawnPoint = <<-24.3831, 208.2052, 105.5534>> fSpawnHeading = 172.7981	BREAK
				CASE 49			vSpawnPoint = <<-21.4568, 207.6021, 105.5534>> fSpawnHeading = 183.7980	BREAK
				CASE 50			vSpawnPoint = <<-26.1935, 207.3765, 106.4776>> fSpawnHeading = 254.1990	BREAK
				CASE 51			vSpawnPoint = <<-23.2671, 206.4943, 106.4776>> fSpawnHeading = 254.1990	BREAK
				CASE 52			vSpawnPoint = <<-20.2709, 205.4086, 106.4776>> fSpawnHeading = 254.1990	BREAK
				CASE 53			vSpawnPoint = <<-18.7044, 206.6832, 106.4776>> fSpawnHeading = 254.1990	BREAK
				CASE 54			vSpawnPoint = <<-29.2610, 208.3157, 106.4776>> fSpawnHeading = 254.1990	BREAK
				CASE 55			vSpawnPoint = <<-32.2931, 209.2878, 106.4776>> fSpawnHeading = 254.1990	BREAK
				CASE 56			vSpawnPoint = <<-10.1065, 210.3728, 106.1634>> fSpawnHeading = 254.1990	BREAK
				CASE 57			vSpawnPoint = <<-10.8837, 208.1977, 105.9874>> fSpawnHeading = 254.1990	BREAK
				CASE 58			vSpawnPoint = <<-9.1322, 208.8355, 105.9295>> fSpawnHeading = 254.1990 	BREAK
				CASE 59			vSpawnPoint = <<-8.4664, 210.9394, 106.1444>> fSpawnHeading = 254.1990 	BREAK
				CASE 60			vSpawnPoint = <<-10.0392, 206.7486, 105.7787>> fSpawnHeading = 254.1990	BREAK
				CASE 61			vSpawnPoint = <<-8.2589, 207.3109, 105.7182>> fSpawnHeading = 254.1990 	BREAK
				CASE 62			vSpawnPoint = <<-7.5629, 209.5312, 105.9300>> fSpawnHeading = 254.1990 	BREAK
				CASE 63			vSpawnPoint = <<-6.5240, 208.0155, 105.6822>> fSpawnHeading = 254.1990 	BREAK
				CASE 64			vSpawnPoint = <<-7.4540, 205.9756, 105.5053>> fSpawnHeading = 254.1990 	BREAK
				CASE 65			vSpawnPoint = <<-5.9978, 210.2307, 105.9282>> fSpawnHeading = 254.1990 	BREAK
				CASE 66			vSpawnPoint = <<-4.9850, 208.8406, 105.7109>> fSpawnHeading = 254.1990 	BREAK
				CASE 67			vSpawnPoint = <<-5.7553, 206.5226, 105.4320>> fSpawnHeading = 254.1990 	BREAK
				CASE 68			vSpawnPoint = <<-4.0965, 207.1716, 105.4207>> fSpawnHeading = 254.1990 	BREAK
				CASE 69			vSpawnPoint = <<-4.9984, 205.0623, 105.1098>> fSpawnHeading = 254.1990 	BREAK
				CASE 70			vSpawnPoint = <<-3.4080, 209.5985, 105.7923>> fSpawnHeading = 254.1990 	BREAK
				CASE 71			vSpawnPoint = <<-2.3874, 206.9301, 105.3040>> fSpawnHeading = 254.1990 	BREAK
				DEFAULT 		RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<909.1130, -2094.3120, 29.5080>> fSpawnHeading = 354.2000	BREAK
				CASE 1			vSpawnPoint = <<911.6208, -2095.0125, 29.4918>> fSpawnHeading = 354.2000	BREAK
				CASE 2			vSpawnPoint = <<906.3580, -2094.2290, 29.5350>> fSpawnHeading = 354.2000	BREAK
				CASE 3			vSpawnPoint = <<903.4430, -2094.0820, 29.6410>> fSpawnHeading = 354.2000	BREAK
				CASE 4			vSpawnPoint = <<914.4555, -2095.5190, 29.5158>> fSpawnHeading = 354.2000	BREAK
				CASE 5			vSpawnPoint = <<912.7916, -2096.9290, 29.5228>> fSpawnHeading = 354.2000	BREAK
				CASE 6			vSpawnPoint = <<904.6930, -2095.7881, 29.5890>> fSpawnHeading = 354.2000	BREAK
				CASE 7			vSpawnPoint = <<907.5390, -2092.4480, 29.4910>> fSpawnHeading = 354.2000	BREAK
				CASE 8			vSpawnPoint = <<910.5660, -2092.8911, 29.4690>> fSpawnHeading = 354.2000	BREAK
				CASE 9			vSpawnPoint = <<913.3230, -2093.6135, 29.5240>> fSpawnHeading = 354.2000	BREAK
				CASE 10			vSpawnPoint = <<904.9130, -2092.5710, 29.5790>> fSpawnHeading = 354.2000	BREAK
				CASE 11			vSpawnPoint = <<901.7001, -2092.7764, 29.6950>> fSpawnHeading = 354.2000	BREAK
				CASE 12			vSpawnPoint = <<901.6689, -2095.8379, 29.7060>> fSpawnHeading = 354.2000	BREAK
				CASE 13			vSpawnPoint = <<900.2687, -2094.2278, 29.7484>> fSpawnHeading = 354.2000	BREAK
				CASE 14			vSpawnPoint = <<915.9485, -2096.9739, 29.5063>> fSpawnHeading = 354.2000	BREAK
				CASE 15			vSpawnPoint = <<916.1860, -2093.7290, 29.5010>> fSpawnHeading = 354.2000	BREAK
				CASE 16			vSpawnPoint = <<898.8898, -2092.7117, 29.7784>> fSpawnHeading = 354.2000	BREAK
				CASE 17			vSpawnPoint = <<898.6442, -2095.6050, 29.8022>> fSpawnHeading = 354.2000	BREAK
				CASE 18			vSpawnPoint = <<897.2260, -2093.8069, 29.7970>> fSpawnHeading = 354.2000	BREAK
				CASE 19			vSpawnPoint = <<917.6790, -2095.3230, 29.4920>> fSpawnHeading = 354.2000	BREAK
				CASE 20			vSpawnPoint = <<889.0930, -2094.5100, 29.6980>> fSpawnHeading = 354.2000	BREAK
				CASE 21			vSpawnPoint = <<889.3087, -2092.0444, 29.6495>> fSpawnHeading = 354.2000	BREAK
				CASE 22			vSpawnPoint = <<887.3430, -2092.8279, 29.6460>> fSpawnHeading = 354.2000	BREAK
				CASE 23			vSpawnPoint = <<885.2760, -2094.2490, 29.6400>> fSpawnHeading = 354.2000	BREAK
				CASE 24			vSpawnPoint = <<883.6310, -2092.6270, 29.6020>> fSpawnHeading = 354.2000	BREAK
				CASE 25			vSpawnPoint = <<881.6120, -2093.8621, 29.5690>> fSpawnHeading = 354.2000	BREAK
				CASE 26			vSpawnPoint = <<891.2047, -2093.5693, 29.7317>> fSpawnHeading = 354.2000	BREAK
				CASE 27			vSpawnPoint = <<894.4900, -2093.7261, 29.7800>> fSpawnHeading = 354.2000	BREAK
				CASE 28			vSpawnPoint = <<896.1510, -2092.0811, 29.7890>> fSpawnHeading = 354.2000	BREAK
				CASE 29			vSpawnPoint = <<892.7123, -2092.0762, 29.7378>> fSpawnHeading = 354.2000	BREAK
				CASE 30			vSpawnPoint = <<918.4026, -2093.4890, 29.4350>> fSpawnHeading = 278.3992	BREAK
				CASE 31			vSpawnPoint = <<918.8650, -2096.7129, 29.4924>> fSpawnHeading = 278.3992	BREAK
				CASE 32			vSpawnPoint = <<919.7388, -2095.1592, 29.4826>> fSpawnHeading = 278.3992	BREAK
				CASE 33			vSpawnPoint = <<921.3546, -2096.9685, 29.4947>> fSpawnHeading = 278.3992	BREAK
				CASE 34			vSpawnPoint = <<920.8484, -2093.6262, 29.4051>> fSpawnHeading = 278.3992	BREAK
				CASE 35			vSpawnPoint = <<892.5798, -2095.1643, 29.8010>> fSpawnHeading = 349.3988	BREAK
				CASE 36			vSpawnPoint = <<895.8444, -2095.6243, 29.8172>> fSpawnHeading = 349.3988	BREAK
				CASE 37			vSpawnPoint = <<886.4039, -2090.6404, 29.6299>> fSpawnHeading = 349.3988	BREAK
				CASE 38			vSpawnPoint = <<919.9158, -2098.2622, 29.4928>> fSpawnHeading = 294.1984	BREAK
				CASE 39			vSpawnPoint = <<922.4511, -2095.5959, 29.4852>> fSpawnHeading = 279.7983	BREAK
				CASE 40			vSpawnPoint = <<914.3342, -2092.3188, 29.5147>> fSpawnHeading = 354.2000	BREAK
				CASE 41			vSpawnPoint = <<903.4503, -2091.3015, 29.6076>> fSpawnHeading = 354.2000	BREAK
				CASE 42			vSpawnPoint = <<900.1887, -2091.1990, 29.7318>> fSpawnHeading = 354.2000	BREAK
				CASE 43			vSpawnPoint = <<894.6191, -2090.7637, 29.7521>> fSpawnHeading = 354.2000	BREAK
				CASE 44			vSpawnPoint = <<890.8015, -2090.3738, 29.6514>> fSpawnHeading = 354.2000	BREAK
				CASE 45			vSpawnPoint = <<919.3007, -2100.0686, 29.4947>> fSpawnHeading = 261.7993	BREAK
				CASE 46			vSpawnPoint = <<919.0816, -2101.9407, 29.4952>> fSpawnHeading = 261.7993	BREAK
				CASE 47			vSpawnPoint = <<919.0981, -2103.6746, 29.4959>> fSpawnHeading = 261.7993	BREAK
				CASE 48			vSpawnPoint = <<918.9641, -2105.5542, 29.4969>> fSpawnHeading = 261.7993	BREAK
				CASE 49			vSpawnPoint = <<918.7385, -2107.6279, 29.4983>> fSpawnHeading = 261.7993	BREAK
				CASE 50			vSpawnPoint = <<921.4729, -2099.8960, 30.4324>> fSpawnHeading = 261.7993	BREAK
				CASE 51			vSpawnPoint = <<921.3322, -2102.2124, 30.4189>> fSpawnHeading = 261.7993	BREAK
				CASE 52			vSpawnPoint = <<921.1118, -2104.4690, 30.4189>> fSpawnHeading = 261.7993	BREAK
				CASE 53			vSpawnPoint = <<920.9454, -2106.5471, 30.4189>> fSpawnHeading = 261.7993	BREAK
				CASE 54			vSpawnPoint = <<920.7137, -2108.5813, 30.4191>> fSpawnHeading = 261.7993	BREAK
				CASE 55			vSpawnPoint = <<920.6151, -2110.9607, 30.4203>> fSpawnHeading = 261.7993	BREAK
				CASE 56			vSpawnPoint = <<919.4415, -2109.5891, 30.4219>> fSpawnHeading = 261.7993	BREAK
				CASE 57			vSpawnPoint = <<918.1919, -2110.7473, 30.4247>> fSpawnHeading = 261.7993	BREAK
				CASE 58			vSpawnPoint = <<919.2366, -2112.4512, 30.4235>> fSpawnHeading = 261.7993	BREAK
				CASE 59			vSpawnPoint = <<920.3210, -2114.0540, 30.4207>> fSpawnHeading = 261.7993	BREAK
				CASE 60			vSpawnPoint = <<917.9865, -2113.8457, 30.4265>> fSpawnHeading = 261.7993	BREAK
				CASE 61			vSpawnPoint = <<918.9727, -2115.1211, 30.4239>> fSpawnHeading = 261.7993	BREAK
				CASE 62			vSpawnPoint = <<917.8502, -2116.2878, 30.4265>> fSpawnHeading = 261.7993	BREAK
				CASE 63			vSpawnPoint = <<920.2974, -2116.4287, 30.4202>> fSpawnHeading = 261.7993	BREAK
				CASE 64			vSpawnPoint = <<918.8382, -2117.4846, 30.4237>> fSpawnHeading = 261.7993	BREAK
				CASE 65			vSpawnPoint = <<917.5727, -2118.6907, 30.4267>> fSpawnHeading = 261.7993	BREAK
				CASE 66			vSpawnPoint = <<919.8004, -2118.9202, 30.4209>> fSpawnHeading = 261.7993	BREAK
				CASE 67			vSpawnPoint = <<918.4038, -2120.4678, 30.4241>> fSpawnHeading = 261.7993	BREAK
				CASE 68			vSpawnPoint = <<917.1946, -2122.1587, 30.4263>> fSpawnHeading = 261.7993	BREAK
				CASE 69			vSpawnPoint = <<919.4966, -2122.3696, 30.4209>> fSpawnHeading = 261.7993	BREAK
				CASE 70			vSpawnPoint = <<917.9831, -2124.1707, 30.4240>> fSpawnHeading = 261.7993	BREAK
				CASE 71			vSpawnPoint = <<919.3307, -2125.3259, 30.4206>> fSpawnHeading = 261.7993	BREAK
				DEFAULT			RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<-675.0270, -2390.3899, 12.8670>> fSpawnHeading = 70.5990	BREAK
				CASE 1			vSpawnPoint = <<-676.1720, -2392.2881, 12.8700>> fSpawnHeading = 70.5990	BREAK
				CASE 2			vSpawnPoint = <<-673.7290, -2388.4270, 12.8660>> fSpawnHeading = 70.5990	BREAK
				CASE 3			vSpawnPoint = <<-672.3740, -2386.3210, 12.8660>> fSpawnHeading = 70.5990	BREAK
				CASE 4			vSpawnPoint = <<-671.6520, -2388.1541, 12.8960>> fSpawnHeading = 70.5990	BREAK
				CASE 5			vSpawnPoint = <<-670.0290, -2385.5830, 12.8980>> fSpawnHeading = 70.5990	BREAK
				CASE 6			vSpawnPoint = <<-674.3930, -2386.5569, 12.8350>> fSpawnHeading = 70.5990	BREAK
				CASE 7			vSpawnPoint = <<-675.5820, -2388.5330, 12.8390>> fSpawnHeading = 70.5990	BREAK
				CASE 8			vSpawnPoint = <<-676.8550, -2390.5439, 12.8410>> fSpawnHeading = 70.5990	BREAK
				CASE 9			vSpawnPoint = <<-678.2810, -2392.9961, 12.8770>> fSpawnHeading = 70.5990	BREAK
				CASE 10			vSpawnPoint = <<-678.8140, -2391.1699, 12.8390>> fSpawnHeading = 70.5990	BREAK
				CASE 11			vSpawnPoint = <<-677.3180, -2388.8210, 12.8240>> fSpawnHeading = 70.5990	BREAK
				CASE 12			vSpawnPoint = <<-676.0550, -2386.8220, 12.8240>> fSpawnHeading = 70.5990	BREAK
				CASE 13			vSpawnPoint = <<-672.8500, -2384.5400, 12.8390>> fSpawnHeading = 70.5990	BREAK
				CASE 14			vSpawnPoint = <<-674.7400, -2384.9319, 12.8230>> fSpawnHeading = 70.5990	BREAK
				CASE 15			vSpawnPoint = <<-670.6160, -2383.6790, 12.8680>> fSpawnHeading = 70.5990	BREAK
				CASE 16			vSpawnPoint = <<-672.9830, -2382.3650, 12.8240>> fSpawnHeading = 70.5990	BREAK
				CASE 17			vSpawnPoint = <<-679.3180, -2389.3459, 12.8190>> fSpawnHeading = 70.5990	BREAK
				CASE 18			vSpawnPoint = <<-680.7310, -2391.7900, 12.8450>> fSpawnHeading = 70.5990	BREAK
				CASE 19			vSpawnPoint = <<-678.0030, -2387.2681, 12.8160>> fSpawnHeading = 70.5990	BREAK
				CASE 20			vSpawnPoint = <<-676.6960, -2385.2009, 12.8150>> fSpawnHeading = 70.5990	BREAK
				CASE 21			vSpawnPoint = <<-675.2540, -2382.9041, 12.8140>> fSpawnHeading = 70.5990	BREAK
				CASE 22			vSpawnPoint = <<-678.4270, -2385.4719, 12.8080>> fSpawnHeading = 70.5990	BREAK
				CASE 23			vSpawnPoint = <<-679.8300, -2387.6919, 12.8100>> fSpawnHeading = 70.5990	BREAK
				CASE 24			vSpawnPoint = <<-681.3150, -2390.0339, 12.8190>> fSpawnHeading = 70.5990	BREAK
				CASE 25			vSpawnPoint = <<-676.9240, -2383.0979, 12.8060>> fSpawnHeading = 70.5990	BREAK
				CASE 26			vSpawnPoint = <<-680.4120, -2385.9370, 12.8010>> fSpawnHeading = 70.5990	BREAK
				CASE 27			vSpawnPoint = <<-681.8300, -2388.1770, 12.8070>> fSpawnHeading = 70.5990	BREAK
				CASE 28			vSpawnPoint = <<-683.3590, -2390.5859, 12.8210>> fSpawnHeading = 70.5990	BREAK
				CASE 29			vSpawnPoint = <<-678.8160, -2383.4150, 12.7990>> fSpawnHeading = 70.5990	BREAK
				CASE 30			vSpawnPoint = <<-668.3120, -2382.7229, 12.8970>> fSpawnHeading = 70.5990	BREAK
				CASE 31			vSpawnPoint = <<-670.7160, -2381.5161, 12.8420>> fSpawnHeading = 70.5990	BREAK
				CASE 32			vSpawnPoint = <<-673.5330, -2380.1699, 12.8120>> fSpawnHeading = 70.5990	BREAK
				CASE 33			vSpawnPoint = <<-675.7990, -2380.9441, 12.8040>> fSpawnHeading = 70.5990	BREAK
				CASE 34			vSpawnPoint = <<-671.5070, -2379.5010, 12.8210>> fSpawnHeading = 70.5990	BREAK
				CASE 35			vSpawnPoint = <<-668.7710, -2380.7329, 12.8680>> fSpawnHeading = 70.5990	BREAK
				CASE 36			vSpawnPoint = <<-666.3630, -2381.8069, 12.9210>> fSpawnHeading = 70.5990	BREAK
				CASE 37			vSpawnPoint = <<-666.4630, -2380.0391, 12.9010>> fSpawnHeading = 70.5990	BREAK
				CASE 38			vSpawnPoint = <<-669.2300, -2378.9919, 12.8420>> fSpawnHeading = 70.5990	BREAK
				CASE 39			vSpawnPoint = <<-672.1880, -2377.6201, 12.8090>> fSpawnHeading = 70.5990	BREAK
				CASE 40			vSpawnPoint = <<-674.1870, -2378.6379, 12.8020>> fSpawnHeading = 70.5990	BREAK
				CASE 41			vSpawnPoint = <<-675.9600, -2394.4341, 12.9110>> fSpawnHeading = 66.3990	BREAK
				CASE 42			vSpawnPoint = <<-683.5330, -2389.0259, 12.8090>> fSpawnHeading = 66.3990	BREAK
				CASE 43			vSpawnPoint = <<-682.1560, -2386.5901, 12.7970>> fSpawnHeading = 66.3990	BREAK
				CASE 44			vSpawnPoint = <<-667.1950, -2378.2590, 12.8700>> fSpawnHeading = 66.3990	BREAK
				CASE 45			vSpawnPoint = <<-669.9850, -2377.1040, 12.8210>> fSpawnHeading = 66.3990	BREAK
				CASE 46			vSpawnPoint = <<-672.5320, -2376.1931, 12.8010>> fSpawnHeading = 66.3990	BREAK
				CASE 47			vSpawnPoint = <<-664.8010, -2379.5730, 12.9260>> fSpawnHeading = 66.3990	BREAK
				CASE 48			vSpawnPoint = <<-665.3090, -2377.6411, 12.8970>> fSpawnHeading = 66.3990	BREAK
				CASE 49			vSpawnPoint = <<-668.0240, -2376.6631, 12.8380>> fSpawnHeading = 66.3990	BREAK
				CASE 50			vSpawnPoint = <<-680.7966, -2383.7302, 13.7129>> fSpawnHeading = 66.3990	BREAK
				CASE 51			vSpawnPoint = <<-683.8362, -2387.1531, 13.7124>> fSpawnHeading = 66.3990	BREAK
				CASE 52			vSpawnPoint = <<-678.4980, -2381.3250, 13.7144>> fSpawnHeading = 66.3990	BREAK
				CASE 53			vSpawnPoint = <<-676.2869, -2378.8481, 13.7148>> fSpawnHeading = 66.3990	BREAK
				CASE 54			vSpawnPoint = <<-674.4763, -2376.7615, 13.7148>> fSpawnHeading = 66.3990	BREAK
				CASE 55			vSpawnPoint = <<-672.2119, -2374.3621, 13.7161>> fSpawnHeading = 66.3990	BREAK
				CASE 56			vSpawnPoint = <<-670.0706, -2375.0698, 13.7355>> fSpawnHeading = 66.3990	BREAK
				CASE 57			vSpawnPoint = <<-670.3538, -2372.1482, 13.7156>> fSpawnHeading = 66.3990	BREAK
				CASE 58			vSpawnPoint = <<-667.8519, -2372.3906, 13.7488>> fSpawnHeading = 66.3990	BREAK
				CASE 59			vSpawnPoint = <<-668.0169, -2374.8943, 13.7488>> fSpawnHeading = 66.3990	BREAK
				CASE 60			vSpawnPoint = <<-665.5997, -2373.9875, 13.8687>> fSpawnHeading = 66.3990	BREAK
				CASE 61			vSpawnPoint = <<-683.1083, -2384.6560, 13.7640>> fSpawnHeading = 66.3990	BREAK
				CASE 62			vSpawnPoint = <<-680.7345, -2381.6140, 13.7744>> fSpawnHeading = 66.3990	BREAK
				CASE 63			vSpawnPoint = <<-678.4115, -2379.2219, 13.7654>> fSpawnHeading = 66.3990	BREAK
				CASE 64			vSpawnPoint = <<-676.5792, -2377.0422, 13.7679>> fSpawnHeading = 66.3990	BREAK
				CASE 65			vSpawnPoint = <<-674.4604, -2374.7285, 13.7640>> fSpawnHeading = 66.3990	BREAK
				CASE 66			vSpawnPoint = <<-685.5165, -2385.4763, 13.8287>> fSpawnHeading = 66.3990	BREAK
				CASE 67			vSpawnPoint = <<-683.0140, -2382.4998, 13.8314>> fSpawnHeading = 66.3990	BREAK
				CASE 68			vSpawnPoint = <<-680.6184, -2379.3464, 13.8446>> fSpawnHeading = 66.3990	BREAK
				CASE 69			vSpawnPoint = <<-678.6383, -2377.1697, 13.8416>> fSpawnHeading = 66.3990	BREAK
				CASE 70			vSpawnPoint = <<-676.9075, -2375.0840, 13.8449>> fSpawnHeading = 66.3990	BREAK
				CASE 71			vSpawnPoint = <<-672.3674, -2372.3049, 13.7646>> fSpawnHeading = 66.3990	BREAK
				DEFAULT			RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<223.6510, -3132.4331, 4.7900>> fSpawnHeading = 51.2000	BREAK
				CASE 1			vSpawnPoint = <<223.0800, -3135.0271, 4.7900>> fSpawnHeading = 51.2000	BREAK
				CASE 2			vSpawnPoint = <<226.4380, -3130.6101, 4.7900>> fSpawnHeading = 51.2000	BREAK
				CASE 3			vSpawnPoint = <<229.7990, -3131.1250, 4.7900>> fSpawnHeading = 51.2000	BREAK
				CASE 4			vSpawnPoint = <<223.8080, -3129.8650, 4.7900>> fSpawnHeading = 51.2000	BREAK
				CASE 5			vSpawnPoint = <<221.4120, -3133.4431, 4.7900>> fSpawnHeading = 51.2000	BREAK
				CASE 6			vSpawnPoint = <<221.5420, -3130.8191, 4.7900>> fSpawnHeading = 51.2000	BREAK
				CASE 7			vSpawnPoint = <<221.6110, -3127.9341, 4.7900>> fSpawnHeading = 51.2000	BREAK
				CASE 8			vSpawnPoint = <<219.7140, -3135.3740, 4.7900>> fSpawnHeading = 51.2000	BREAK
				CASE 9			vSpawnPoint = <<219.6810, -3132.2639, 4.7900>> fSpawnHeading = 51.2000	BREAK
				CASE 10			vSpawnPoint = <<219.8260, -3129.2600, 4.7900>> fSpawnHeading = 51.2000	BREAK
				CASE 11			vSpawnPoint = <<228.8960, -3128.5320, 4.7900>> fSpawnHeading = 85.6000	BREAK
				CASE 12			vSpawnPoint = <<225.5520, -3128.3359, 4.7900>> fSpawnHeading = 85.6000	BREAK
				CASE 13			vSpawnPoint = <<231.9680, -3128.6560, 4.7900>> fSpawnHeading = 85.6000	BREAK
				CASE 14			vSpawnPoint = <<217.8500, -3134.1011, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 15			vSpawnPoint = <<218.1080, -3130.7759, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 16			vSpawnPoint = <<218.4890, -3127.4600, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 17			vSpawnPoint = <<216.6670, -3128.9370, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 18			vSpawnPoint = <<216.1280, -3132.2671, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 19			vSpawnPoint = <<216.1190, -3135.4609, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 20			vSpawnPoint = <<214.8180, -3130.1741, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 21			vSpawnPoint = <<213.8020, -3132.9441, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 22			vSpawnPoint = <<215.3810, -3126.5649, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 23			vSpawnPoint = <<212.8750, -3131.1931, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 24			vSpawnPoint = <<213.7220, -3128.0049, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 25			vSpawnPoint = <<211.7150, -3129.4380, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 26			vSpawnPoint = <<211.2840, -3133.0081, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 27			vSpawnPoint = <<212.2320, -3126.2490, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 28			vSpawnPoint = <<210.3390, -3127.4780, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 29			vSpawnPoint = <<209.9370, -3131.0820, 4.7900>> fSpawnHeading = 47.5990	BREAK
				CASE 30			vSpawnPoint = <<217.4740, -3124.5620, 4.7900>> fSpawnHeading = 1.3990	BREAK
				CASE 31			vSpawnPoint = <<213.8300, -3124.5439, 4.7900>> fSpawnHeading = 1.3990	BREAK
				CASE 32			vSpawnPoint = <<210.2910, -3124.4929, 4.7900>> fSpawnHeading = 1.3990	BREAK
				CASE 33			vSpawnPoint = <<215.6140, -3122.9709, 4.7900>> fSpawnHeading = 1.3990	BREAK
				CASE 34			vSpawnPoint = <<211.9450, -3122.7839, 4.7900>> fSpawnHeading = 1.3990	BREAK
				CASE 35			vSpawnPoint = <<219.7720, -3122.8760, 4.7900>> fSpawnHeading = 1.3990	BREAK
				CASE 36			vSpawnPoint = <<223.2910, -3123.0549, 4.7900>> fSpawnHeading = 1.3990	BREAK
				CASE 37			vSpawnPoint = <<221.6190, -3121.2009, 4.7900>> fSpawnHeading = 1.3990	BREAK
				CASE 38			vSpawnPoint = <<217.3220, -3121.1770, 4.7900>> fSpawnHeading = 1.3990	BREAK
				CASE 39			vSpawnPoint = <<213.7120, -3120.8201, 4.7900>> fSpawnHeading = 1.3990	BREAK
				CASE 40			vSpawnPoint = <<209.8190, -3120.8291, 4.7900>> fSpawnHeading = 1.3990	BREAK
				CASE 41			vSpawnPoint = <<208.0150, -3129.3169, 4.7900>> fSpawnHeading = 52.3990	BREAK
				CASE 42			vSpawnPoint = <<207.8980, -3132.9700, 4.7900>> fSpawnHeading = 52.3990	BREAK
				CASE 43			vSpawnPoint = <<207.8830, -3125.8750, 4.7900>> fSpawnHeading = 52.3990	BREAK
				CASE 44			vSpawnPoint = <<208.1110, -3122.8650, 4.7900>> fSpawnHeading = 5.3990	BREAK
				CASE 45			vSpawnPoint = <<219.3880, -3119.3340, 4.7900>> fSpawnHeading = -0.2010	BREAK
				CASE 46			vSpawnPoint = <<223.0820, -3119.2019, 4.7900>> fSpawnHeading = -0.2010	BREAK
				CASE 47			vSpawnPoint = <<215.5590, -3119.0110, 4.7900>> fSpawnHeading = -0.2010	BREAK
				CASE 48			vSpawnPoint = <<211.8300, -3119.2109, 4.7900>> fSpawnHeading = -0.2010	BREAK
				CASE 49			vSpawnPoint = <<208.1010, -3119.1411, 4.7900>> fSpawnHeading = -0.2010	BREAK
				CASE 50			vSpawnPoint = <<206.4675, -3131.1580, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 51			vSpawnPoint = <<206.4888, -3127.6345, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 52			vSpawnPoint = <<206.4219, -3124.3369, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 53			vSpawnPoint = <<204.7200, -3125.8667, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 54			vSpawnPoint = <<204.5234, -3129.3794, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 55			vSpawnPoint = <<206.6441, -3120.9128, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 56			vSpawnPoint = <<204.5487, -3122.7388, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 57			vSpawnPoint = <<210.0842, -3117.7974, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 58			vSpawnPoint = <<206.5443, -3117.7700, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 59			vSpawnPoint = <<204.6520, -3119.3237, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 60			vSpawnPoint = <<202.7707, -3124.3540, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 61			vSpawnPoint = <<203.0118, -3121.1699, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 62			vSpawnPoint = <<203.1512, -3117.4761, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 63			vSpawnPoint = <<201.1831, -3122.5303, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 64			vSpawnPoint = <<201.3407, -3119.1514, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 65			vSpawnPoint = <<199.7204, -3120.9661, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 66			vSpawnPoint = <<199.5784, -3124.4424, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 67			vSpawnPoint = <<197.7966, -3122.5811, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 68			vSpawnPoint = <<197.7813, -3119.2524, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 69			vSpawnPoint = <<195.8712, -3120.9048, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 70			vSpawnPoint = <<195.9431, -3124.2900, 5.7144>> fSpawnHeading = 2.3997	BREAK
				CASE 71			vSpawnPoint = <<208.3134, -3116.4807, 5.7144>> fSpawnHeading = 2.3997	BREAK
				DEFAULT			RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<380.6280, 225.3820, 102.0410>> fSpawnHeading = 251.9960	BREAK
				CASE 1			vSpawnPoint = <<381.6840, 228.1000, 102.0400>> fSpawnHeading = 251.9960	BREAK
				CASE 2			vSpawnPoint = <<382.8090, 231.0260, 102.0390>> fSpawnHeading = 251.9960	BREAK
				CASE 3			vSpawnPoint = <<379.6050, 222.8460, 102.0420>> fSpawnHeading = 251.9960	BREAK
				CASE 4			vSpawnPoint = <<383.1680, 224.5620, 102.0410>> fSpawnHeading = 251.9960	BREAK
				CASE 5			vSpawnPoint = <<384.0800, 227.1400, 102.0400>> fSpawnHeading = 251.9960	BREAK
				CASE 6			vSpawnPoint = <<385.1920, 230.2130, 102.0390>> fSpawnHeading = 251.9960	BREAK
				CASE 7			vSpawnPoint = <<381.9360, 221.8380, 102.0420>> fSpawnHeading = 251.9960	BREAK
				CASE 8			vSpawnPoint = <<383.8330, 233.7510, 102.0380>> fSpawnHeading = 246.7960	BREAK
				CASE 9			vSpawnPoint = <<386.1230, 232.9600, 102.0380>> fSpawnHeading = 246.7960	BREAK
				CASE 10			vSpawnPoint = <<378.5000, 219.9450, 102.0430>> fSpawnHeading = 246.7960	BREAK
				CASE 11			vSpawnPoint = <<380.8200, 231.7560, 102.0390>> fSpawnHeading = 246.7960	BREAK
				CASE 12			vSpawnPoint = <<377.6000, 223.5210, 102.0420>> fSpawnHeading = 246.7960	BREAK
				CASE 13			vSpawnPoint = <<381.6980, 234.5990, 102.0380>> fSpawnHeading = 246.7960	BREAK
				CASE 14			vSpawnPoint = <<376.5760, 220.6590, 102.0430>> fSpawnHeading = 246.7960	BREAK
				CASE 15			vSpawnPoint = <<377.6340, 217.0480, 102.0440>> fSpawnHeading = 246.7960	BREAK
				CASE 16			vSpawnPoint = <<384.9210, 236.5670, 102.0370>> fSpawnHeading = 246.7960	BREAK
				CASE 17			vSpawnPoint = <<387.0740, 235.6820, 102.0350>> fSpawnHeading = 246.7960	BREAK
				CASE 18			vSpawnPoint = <<385.8240, 239.1980, 102.0330>> fSpawnHeading = 246.7960	BREAK
				CASE 19			vSpawnPoint = <<387.9230, 238.4720, 102.0310>> fSpawnHeading = 246.7960	BREAK
				CASE 20			vSpawnPoint = <<382.8330, 237.3740, 102.0370>> fSpawnHeading = 246.7960	BREAK
				CASE 21			vSpawnPoint = <<380.8970, 219.2000, 102.0430>> fSpawnHeading = 246.7960	BREAK
				CASE 22			vSpawnPoint = <<379.7770, 216.3540, 102.0440>> fSpawnHeading = 246.7960	BREAK
				CASE 23			vSpawnPoint = <<376.5210, 214.4210, 102.0450>> fSpawnHeading = 246.7960	BREAK
				CASE 24			vSpawnPoint = <<378.7810, 213.4740, 102.0450>> fSpawnHeading = 246.7960	BREAK
				CASE 25			vSpawnPoint = <<375.6090, 217.8480, 102.0440>> fSpawnHeading = 246.7960	BREAK
				CASE 26			vSpawnPoint = <<375.5960, 211.6360, 102.0460>> fSpawnHeading = 246.7960	BREAK
				CASE 27			vSpawnPoint = <<377.8380, 210.8150, 102.0460>> fSpawnHeading = 246.7960	BREAK
				CASE 28			vSpawnPoint = <<374.6650, 215.1060, 102.0450>> fSpawnHeading = 246.7960	BREAK
				CASE 29			vSpawnPoint = <<386.8660, 242.3780, 102.0290>> fSpawnHeading = 246.7960	BREAK
				CASE 30			vSpawnPoint = <<388.9990, 241.6740, 102.0280>> fSpawnHeading = 246.7960	BREAK
				CASE 31			vSpawnPoint = <<383.8010, 240.0140, 102.0350>> fSpawnHeading = 246.7960	BREAK
				CASE 32			vSpawnPoint = <<387.9670, 245.6080, 102.0260>> fSpawnHeading = 246.7960	BREAK
				CASE 33			vSpawnPoint = <<390.0170, 244.7830, 102.0260>> fSpawnHeading = 246.7960	BREAK
				CASE 34			vSpawnPoint = <<384.9140, 243.2340, 102.0300>> fSpawnHeading = 246.7960	BREAK
				CASE 35			vSpawnPoint = <<374.4490, 208.8270, 102.0490>> fSpawnHeading = 246.7960	BREAK
				CASE 36			vSpawnPoint = <<373.6200, 212.3220, 102.0470>> fSpawnHeading = 246.7960	BREAK
				CASE 37			vSpawnPoint = <<386.0300, 246.4210, 102.0260>> fSpawnHeading = 246.7960	BREAK
				CASE 38			vSpawnPoint = <<373.3770, 206.0410, 102.0500>> fSpawnHeading = 246.7960	BREAK
				CASE 39			vSpawnPoint = <<375.7560, 205.1140, 102.0500>> fSpawnHeading = 246.7960	BREAK
				CASE 40			vSpawnPoint = <<372.4180, 209.5590, 102.0490>> fSpawnHeading = 246.7960	BREAK
				CASE 41			vSpawnPoint = <<372.4220, 203.2730, 102.0510>> fSpawnHeading = 246.7960	BREAK
				CASE 42			vSpawnPoint = <<374.9170, 202.3680, 102.0510>> fSpawnHeading = 246.7960	BREAK
				CASE 43			vSpawnPoint = <<371.4630, 206.7840, 102.0500>> fSpawnHeading = 246.7960	BREAK
				CASE 44			vSpawnPoint = <<370.4890, 203.9520, 102.0510>> fSpawnHeading = 246.7960	BREAK
				CASE 45			vSpawnPoint = <<386.9870, 249.2070, 102.0260>> fSpawnHeading = 246.7960	BREAK
				CASE 46			vSpawnPoint = <<388.9520, 248.4740, 102.0260>> fSpawnHeading = 246.7960	BREAK
				CASE 47			vSpawnPoint = <<390.9650, 247.7250, 101.9750>> fSpawnHeading = 246.7960	BREAK
				CASE 48			vSpawnPoint = <<370.4380, 201.2570, 102.0520>> fSpawnHeading = 246.7960	BREAK
				CASE 49			vSpawnPoint = <<372.4760, 200.4450, 102.0520>> fSpawnHeading = 246.7960	BREAK
				CASE 50			vSpawnPoint = <<368.7140, 199.7344, 102.9765>> fSpawnHeading = 246.7960	BREAK
				CASE 51			vSpawnPoint = <<370.7063, 199.1549, 102.9764>> fSpawnHeading = 246.7960	BREAK
				CASE 52			vSpawnPoint = <<373.0141, 198.2910, 102.9764>> fSpawnHeading = 246.7960	BREAK
				CASE 53			vSpawnPoint = <<371.3156, 197.2809, 102.9770>> fSpawnHeading = 246.7960	BREAK
				CASE 54			vSpawnPoint = <<369.1721, 198.0084, 102.9770>> fSpawnHeading = 246.7960	BREAK
				CASE 55			vSpawnPoint = <<369.6260, 196.2005, 102.9776>> fSpawnHeading = 246.7960	BREAK
				CASE 56			vSpawnPoint = <<371.9764, 195.3200, 102.9776>> fSpawnHeading = 246.7960	BREAK
				CASE 57			vSpawnPoint = <<367.5635, 197.0745, 102.9776>> fSpawnHeading = 246.7960	BREAK
				CASE 58			vSpawnPoint = <<367.8895, 195.2333, 102.9782>> fSpawnHeading = 246.7960	BREAK
				CASE 59			vSpawnPoint = <<370.4009, 194.3744, 102.9781>> fSpawnHeading = 246.7960	BREAK
				CASE 60			vSpawnPoint = <<370.7618, 192.1799, 102.9788>> fSpawnHeading = 246.7960	BREAK
				CASE 61			vSpawnPoint = <<368.5346, 192.9880, 102.9788>> fSpawnHeading = 246.7960	BREAK
				CASE 62			vSpawnPoint = <<366.2569, 193.7674, 102.9790>> fSpawnHeading = 246.7960	BREAK
				CASE 63			vSpawnPoint = <<366.6371, 191.9397, 102.9795>> fSpawnHeading = 246.7960	BREAK
				CASE 64			vSpawnPoint = <<368.8595, 191.0028, 102.9794>> fSpawnHeading = 246.7960	BREAK
				CASE 65			vSpawnPoint = <<369.2220, 188.6525, 102.9802>> fSpawnHeading = 246.7960	BREAK
				CASE 66			vSpawnPoint = <<367.0529, 189.4900, 102.9803>> fSpawnHeading = 246.7960	BREAK
				CASE 67			vSpawnPoint = <<364.7919, 190.3549, 102.9804>> fSpawnHeading = 246.7960	BREAK
				CASE 68			vSpawnPoint = <<364.9537, 188.3113, 102.9810>> fSpawnHeading = 246.7960	BREAK
				CASE 69			vSpawnPoint = <<367.4992, 187.5205, 102.9808>> fSpawnHeading = 246.7960	BREAK
				CASE 70			vSpawnPoint = <<365.4669, 186.4086, 102.9816>> fSpawnHeading = 246.7960	BREAK
				CASE 71			vSpawnPoint = <<367.9188, 185.4601, 102.9815>> fSpawnHeading = 246.7960	BREAK
				DEFAULT	
					RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<-1233.1600, -690.8850, 22.7460>> fSpawnHeading = 308.0000	BREAK
				CASE 1			vSpawnPoint = <<-1231.5640, -693.1040, 22.5440>> fSpawnHeading = 308.0000	BREAK
				CASE 2			vSpawnPoint = <<-1230.9900, -690.8930, 22.6450>> fSpawnHeading = 308.0000	BREAK
				CASE 3			vSpawnPoint = <<-1232.9600, -688.6250, 22.8670>> fSpawnHeading = 308.0000	BREAK
				CASE 4			vSpawnPoint = <<-1231.0179, -688.6150, 22.7780>> fSpawnHeading = 308.0000	BREAK
				CASE 5			vSpawnPoint = <<-1232.6570, -686.7450, 22.9620>> fSpawnHeading = 308.0000	BREAK
				CASE 6			vSpawnPoint = <<-1228.9550, -691.0680, 22.5400>> fSpawnHeading = 308.0000	BREAK
				CASE 7			vSpawnPoint = <<-1229.3530, -693.2630, 22.4320>> fSpawnHeading = 308.0000	BREAK
				CASE 8			vSpawnPoint = <<-1229.4670, -695.4480, 22.3110>> fSpawnHeading = 308.0000	BREAK
				CASE 9			vSpawnPoint = <<-1232.3700, -695.1970, 22.4600>> fSpawnHeading = 308.0000	BREAK
				CASE 10			vSpawnPoint = <<-1226.9771, -693.2070, 22.3250>> fSpawnHeading = 308.0000	BREAK
				CASE 11			vSpawnPoint = <<-1232.0840, -697.2750, 22.3270>> fSpawnHeading = 308.0000	BREAK
				CASE 12			vSpawnPoint = <<-1230.0450, -697.9490, 22.1930>> fSpawnHeading = 308.0000	BREAK
				CASE 13			vSpawnPoint = <<-1227.1880, -695.2870, 22.2150>> fSpawnHeading = 308.0000	BREAK
				CASE 14			vSpawnPoint = <<-1228.7410, -688.4220, 22.6830>> fSpawnHeading = 308.0000	BREAK
				CASE 15			vSpawnPoint = <<-1230.6230, -686.0670, 22.9070>> fSpawnHeading = 308.0000	BREAK
				CASE 16			vSpawnPoint = <<-1227.0400, -690.6110, 22.4780>> fSpawnHeading = 308.0000	BREAK
				CASE 17			vSpawnPoint = <<-1224.8149, -692.9410, 22.2400>> fSpawnHeading = 308.0000	BREAK
				CASE 18			vSpawnPoint = <<-1234.5740, -684.2680, 23.1940>> fSpawnHeading = 308.0000	BREAK
				CASE 19			vSpawnPoint = <<-1234.8459, -686.2110, 23.0940>> fSpawnHeading = 308.0000	BREAK
				CASE 20			vSpawnPoint = <<-1232.5820, -683.9170, 23.1220>> fSpawnHeading = 308.0000	BREAK
				CASE 21			vSpawnPoint = <<-1227.5280, -697.7900, 22.0860>> fSpawnHeading = 308.0000	BREAK
				CASE 22			vSpawnPoint = <<-1230.3860, -699.9850, 22.0910>> fSpawnHeading = 308.0000	BREAK
				CASE 23			vSpawnPoint = <<-1224.7400, -695.5250, 22.0880>> fSpawnHeading = 308.0000	BREAK
				CASE 24			vSpawnPoint = <<-1224.5100, -698.1990, 21.9220>> fSpawnHeading = 308.0000	BREAK
				CASE 25			vSpawnPoint = <<-1227.5200, -700.6010, 21.9240>> fSpawnHeading = 308.0000	BREAK
				CASE 26			vSpawnPoint = <<-1222.2430, -696.1400, 21.9360>> fSpawnHeading = 308.0000	BREAK
				CASE 27			vSpawnPoint = <<-1238.9351, -685.9320, 24.9940>> fSpawnHeading = 19.1990	BREAK
				CASE 28			vSpawnPoint = <<-1240.7000, -687.5300, 24.9940>> fSpawnHeading = 19.1990	BREAK
				CASE 29			vSpawnPoint = <<-1241.0260, -685.3240, 24.9940>> fSpawnHeading = 19.1990	BREAK
				CASE 30			vSpawnPoint = <<-1240.7810, -683.0890, 24.9940>> fSpawnHeading = 19.1990	BREAK
				CASE 31			vSpawnPoint = <<-1242.9620, -684.4350, 24.9940>> fSpawnHeading = 19.1990	BREAK
				CASE 32			vSpawnPoint = <<-1236.8380, -684.0230, 23.3130>> fSpawnHeading = 19.1990	BREAK
				CASE 33			vSpawnPoint = <<-1233.9170, -682.1710, 23.2850>> fSpawnHeading = 19.1990	BREAK
				CASE 34			vSpawnPoint = <<-1236.3060, -682.0460, 23.4030>> fSpawnHeading = 19.1990	BREAK
				CASE 35			vSpawnPoint = <<-1238.7460, -681.5370, 23.5450>> fSpawnHeading = 19.1990	BREAK
				CASE 36			vSpawnPoint = <<-1236.2729, -679.2160, 23.5650>> fSpawnHeading = 19.1990	BREAK
				CASE 37			vSpawnPoint = <<-1224.7750, -700.4570, 21.8320>> fSpawnHeading = 234.7990	BREAK
				CASE 38			vSpawnPoint = <<-1222.2290, -697.9120, 21.8530>> fSpawnHeading = 234.7990	BREAK
				CASE 39			vSpawnPoint = <<-1228.1050, -702.4750, 21.8610>> fSpawnHeading = 234.7990	BREAK
				CASE 40			vSpawnPoint = <<-1228.3790, -686.0290, 22.8060>> fSpawnHeading = 301.7990	BREAK
				CASE 41			vSpawnPoint = <<-1230.1740, -683.8570, 23.0150>> fSpawnHeading = 301.7990	BREAK
				CASE 42			vSpawnPoint = <<-1226.6610, -688.1770, 22.6030>> fSpawnHeading = 301.7990	BREAK
				CASE 43			vSpawnPoint = <<-1224.8950, -690.6010, 22.3800>> fSpawnHeading = 301.7990	BREAK
				CASE 44			vSpawnPoint = <<-1222.6410, -693.3080, 22.1200>> fSpawnHeading = 301.7990	BREAK
				CASE 45			vSpawnPoint = <<-1231.9440, -681.6840, 23.2230>> fSpawnHeading = 301.7990	BREAK
				CASE 46			vSpawnPoint = <<-1233.8750, -679.6680, 23.4290>> fSpawnHeading = 301.7990	BREAK
				CASE 47			vSpawnPoint = <<-1235.2570, -677.6160, 23.6110>> fSpawnHeading = 361.7990	BREAK
				CASE 48			vSpawnPoint = <<-1222.4020, -700.1780, 21.7590>> fSpawnHeading = 240.3980	BREAK
				CASE 49			vSpawnPoint = <<-1225.5830, -702.4520, 21.7720>> fSpawnHeading = 231.3980	BREAK
				CASE 50			vSpawnPoint = <<-1242.8950, -682.7191, 25.9178>> fSpawnHeading = 38.2000	BREAK
				CASE 51			vSpawnPoint = <<-1242.6390, -680.5803, 25.9178>> fSpawnHeading = 38.2000	BREAK
				CASE 52			vSpawnPoint = <<-1244.7760, -680.0248, 25.9178>> fSpawnHeading = 38.2000	BREAK
				CASE 53			vSpawnPoint = <<-1244.3433, -678.3621, 25.9178>> fSpawnHeading = 38.2000	BREAK
				CASE 54			vSpawnPoint = <<-1246.6265, -677.9191, 25.9178>> fSpawnHeading = 38.2000	BREAK
				CASE 55			vSpawnPoint = <<-1238.1881, -677.4322, 24.6806>> fSpawnHeading = 38.2000	BREAK
				CASE 56			vSpawnPoint = <<-1236.0167, -675.5317, 24.6919>> fSpawnHeading = 38.2000	BREAK
				CASE 57			vSpawnPoint = <<-1237.8087, -675.3391, 24.7852>> fSpawnHeading = 38.2000	BREAK
				CASE 58			vSpawnPoint = <<-1239.9728, -675.0014, 24.9038>> fSpawnHeading = 38.2000	BREAK
				CASE 59			vSpawnPoint = <<-1237.6210, -673.3062, 24.8951>> fSpawnHeading = 38.2000	BREAK
				CASE 60			vSpawnPoint = <<-1239.6246, -673.1454, 24.9959>> fSpawnHeading = 38.2000	BREAK
				CASE 61			vSpawnPoint = <<-1241.6774, -672.8080, 25.1095>> fSpawnHeading = 38.2000	BREAK
				CASE 62			vSpawnPoint = <<-1239.5026, -670.9388, 25.1192>> fSpawnHeading = 38.2000	BREAK
				CASE 63			vSpawnPoint = <<-1246.0283, -676.1711, 25.9178>> fSpawnHeading = 38.2000	BREAK
				CASE 64			vSpawnPoint = <<-1248.5790, -677.3184, 25.9178>> fSpawnHeading = 38.2000	BREAK
				CASE 65			vSpawnPoint = <<-1248.6055, -675.4040, 25.9178>> fSpawnHeading = 38.2000	BREAK
				CASE 66			vSpawnPoint = <<-1248.1855, -673.1714, 25.9178>> fSpawnHeading = 38.2000	BREAK
				CASE 67			vSpawnPoint = <<-1250.5328, -674.8238, 25.9178>> fSpawnHeading = 38.2000	BREAK
				CASE 68			vSpawnPoint = <<-1241.4647, -670.5612, 25.2311>> fSpawnHeading = 38.2000	BREAK
				CASE 69			vSpawnPoint = <<-1243.7721, -670.1657, 25.3597>> fSpawnHeading = 38.2000	BREAK
				CASE 70			vSpawnPoint = <<-1241.3185, -668.1557, 25.3645>> fSpawnHeading = 38.2000	BREAK
				CASE 71			vSpawnPoint = <<-1243.1123, -668.1689, 25.4430>> fSpawnHeading = 38.2000	BREAK
				DEFAULT 		RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<-1172.1290, -1178.4370, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 1			vSpawnPoint = <<-1170.4270, -1177.9709, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 2			vSpawnPoint = <<-1174.1760, -1178.9969, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 3			vSpawnPoint = <<-1168.3590, -1177.4050, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 4			vSpawnPoint = <<-1176.4561, -1179.5890, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 5			vSpawnPoint = <<-1175.8400, -1177.9110, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 6			vSpawnPoint = <<-1169.8311, -1176.0370, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 7			vSpawnPoint = <<-1168.9930, -1174.5200, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 8			vSpawnPoint = <<-1177.2980, -1176.4860, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 9			vSpawnPoint = <<-1167.4130, -1175.3860, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 10			vSpawnPoint = <<-1172.9840, -1179.8571, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 11			vSpawnPoint = <<-1175.2271, -1180.4130, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 12			vSpawnPoint = <<-1171.1270, -1179.2920, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 13			vSpawnPoint = <<-1169.1470, -1178.7500, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 14			vSpawnPoint = <<-1166.4240, -1178.0050, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 15			vSpawnPoint = <<-1169.7670, -1180.2220, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 16			vSpawnPoint = <<-1171.5790, -1180.7180, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 17			vSpawnPoint = <<-1173.6230, -1181.2770, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 18			vSpawnPoint = <<-1167.6630, -1179.6460, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 19			vSpawnPoint = <<-1166.0620, -1176.3669, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 20			vSpawnPoint = <<-1165.4160, -1179.0300, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 21			vSpawnPoint = <<-1166.8960, -1173.9220, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 22			vSpawnPoint = <<-1168.4680, -1172.9050, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 23			vSpawnPoint = <<-1176.0010, -1181.9370, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 24			vSpawnPoint = <<-1170.2600, -1181.8149, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 25			vSpawnPoint = <<-1172.1689, -1182.3320, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 26			vSpawnPoint = <<-1168.4030, -1181.3020, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 27			vSpawnPoint = <<-1166.2729, -1180.5010, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 28			vSpawnPoint = <<-1174.5450, -1182.7419, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 29			vSpawnPoint = <<-1170.8580, -1183.1910, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 30			vSpawnPoint = <<-1172.9030, -1183.7500, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 31			vSpawnPoint = <<-1169.0360, -1182.6930, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 32			vSpawnPoint = <<-1166.9730, -1182.1281, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 33			vSpawnPoint = <<-1177.9490, -1178.0530, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 34			vSpawnPoint = <<-1177.5280, -1180.7780, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 35			vSpawnPoint = <<-1176.8560, -1183.2350, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 36			vSpawnPoint = <<-1175.2550, -1184.0420, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 37			vSpawnPoint = <<-1164.8440, -1181.5909, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 38			vSpawnPoint = <<-1164.6770, -1177.3560, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 39			vSpawnPoint = <<-1164.0150, -1179.7780, 4.6240>> fSpawnHeading = 209.5990	BREAK
				CASE 40			vSpawnPoint = <<-1169.5500, -1184.2150, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 41			vSpawnPoint = <<-1171.5310, -1184.6940, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 42			vSpawnPoint = <<-1167.6290, -1183.6260, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 43			vSpawnPoint = <<-1165.7690, -1183.0260, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 44			vSpawnPoint = <<-1173.8719, -1185.0150, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 45			vSpawnPoint = <<-1170.3400, -1185.5070, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 46			vSpawnPoint = <<-1168.2560, -1184.9370, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 47			vSpawnPoint = <<-1166.5909, -1184.4810, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 48			vSpawnPoint = <<-1172.6030, -1185.8311, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 49			vSpawnPoint = <<-1169.1780, -1186.1290, 4.6230>> fSpawnHeading = 209.5990	BREAK
				CASE 50			vSpawnPoint = <<-1171.0315, -1187.1589, 5.5475>> fSpawnHeading = 209.5990	BREAK
				CASE 51			vSpawnPoint = <<-1167.8037, -1186.6919, 5.5262>> fSpawnHeading = 209.5990	BREAK
				CASE 52			vSpawnPoint = <<-1169.2640, -1187.9126, 5.3763>> fSpawnHeading = 209.5990	BREAK
				CASE 53			vSpawnPoint = <<-1170.8866, -1189.0696, 5.2452>> fSpawnHeading = 209.5990	BREAK
				CASE 54			vSpawnPoint = <<-1167.4598, -1188.3177, 5.1775>> fSpawnHeading = 209.5990	BREAK
				CASE 55			vSpawnPoint = <<-1168.9093, -1189.7041, 5.0408>> fSpawnHeading = 209.5990	BREAK
				CASE 56			vSpawnPoint = <<-1170.4856, -1191.1289, 4.8599>> fSpawnHeading = 209.5990	BREAK
				CASE 57			vSpawnPoint = <<-1167.1119, -1190.3969, 4.5991>> fSpawnHeading = 209.5990	BREAK
				CASE 58			vSpawnPoint = <<-1168.4799, -1191.6172, 4.5045>> fSpawnHeading = 209.5990	BREAK
				CASE 59			vSpawnPoint = <<-1169.8744, -1193.2040, 4.3272>> fSpawnHeading = 209.5990	BREAK
				CASE 60			vSpawnPoint = <<-1166.5692, -1192.4540, 3.9959>> fSpawnHeading = 209.5990	BREAK
				CASE 61			vSpawnPoint = <<-1176.6193, -1185.5516, 5.5477>> fSpawnHeading = 209.5990	BREAK
				CASE 62			vSpawnPoint = <<-1175.3132, -1186.8350, 5.5476>> fSpawnHeading = 209.5990	BREAK
				CASE 63			vSpawnPoint = <<-1176.3368, -1188.2727, 5.5477>> fSpawnHeading = 209.5990	BREAK
				CASE 64			vSpawnPoint = <<-1173.7810, -1187.8579, 5.5476>> fSpawnHeading = 209.5990	BREAK
				CASE 65			vSpawnPoint = <<-1174.8606, -1189.2977, 5.5476>> fSpawnHeading = 209.5990	BREAK
				CASE 66			vSpawnPoint = <<-1175.8914, -1190.7974, 5.5477>> fSpawnHeading = 209.5990	BREAK
				CASE 67			vSpawnPoint = <<-1173.2872, -1190.1516, 5.5476>> fSpawnHeading = 209.5990	BREAK
				CASE 68			vSpawnPoint = <<-1174.3987, -1191.7429, 5.5476>> fSpawnHeading = 209.5990	BREAK
				CASE 69			vSpawnPoint = <<-1175.5281, -1193.4688, 5.5477>> fSpawnHeading = 209.5990	BREAK
				CASE 70			vSpawnPoint = <<-1173.0081, -1192.8187, 5.5476>> fSpawnHeading = 209.5990	BREAK
				CASE 71			vSpawnPoint = <<-1168.0270, -1194.2386, 4.1110>> fSpawnHeading = 209.5990	BREAK
				DEFAULT			RETURN FALSE
			ENDSWITCH
		BREAK
		
		DEFAULT RETURN FALSE
	ENDSWITCH
		
	// NOTE : default cases of the switch statements return FALSE
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_NIGHTCLUB_EXTERIOR_SPAWN_POINT_ON_FOOT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<759.8510, -1332.6680, 26.2800>> fSpawnHeading = 264.3990	BREAK
				CASE 1			vSpawnPoint = <<761.9270, -1332.7650, 25.5470>> fSpawnHeading = 264.3990	BREAK
				CASE 2			vSpawnPoint = <<763.9250, -1333.0040, 25.2300>> fSpawnHeading = 239.7990	BREAK
				CASE 3			vSpawnPoint = <<755.6290, -1332.5930, 26.2800>> fSpawnHeading = 182.3990	BREAK
				CASE 4			vSpawnPoint = <<753.3620, -1332.6530, 26.2800>> fSpawnHeading = 182.3990	BREAK
				CASE 5			vSpawnPoint = <<758.1560, -1334.4250, 25.2320>> fSpawnHeading = 187.9990	BREAK
				CASE 6			vSpawnPoint = <<760.6140, -1334.6210, 25.2300>> fSpawnHeading = 187.9990	BREAK
				CASE 7			vSpawnPoint = <<756.2640, -1335.9561, 25.2320>> fSpawnHeading = 187.9990	BREAK
				CASE 8			vSpawnPoint = <<759.0630, -1336.2531, 25.2300>> fSpawnHeading = 187.9990	BREAK
				CASE 9			vSpawnPoint = <<761.9850, -1336.2010, 25.2290>> fSpawnHeading = 187.9990	BREAK
				CASE 10			vSpawnPoint = <<757.7490, -1337.8650, 25.2300>> fSpawnHeading = 187.9990	BREAK
				CASE 11			vSpawnPoint = <<760.3690, -1337.8470, 25.2280>> fSpawnHeading = 187.9990	BREAK
				CASE 12			vSpawnPoint = <<763.7570, -1334.8320, 25.2280>> fSpawnHeading = 222.1990	BREAK
				CASE 13			vSpawnPoint = <<755.8970, -1339.3490, 25.2280>> fSpawnHeading = 189.7990	BREAK
				CASE 14			vSpawnPoint = <<759.1470, -1339.4950, 25.2280>> fSpawnHeading = 189.7990	BREAK
				CASE 15			vSpawnPoint = <<762.4640, -1338.6591, 25.2280>> fSpawnHeading = 189.7990	BREAK
				CASE 16			vSpawnPoint = <<764.1930, -1337.3870, 25.2280>> fSpawnHeading = 186.5990	BREAK
				CASE 17			vSpawnPoint = <<766.0050, -1335.9270, 25.2280>> fSpawnHeading = 186.5990	BREAK
				CASE 18			vSpawnPoint = <<766.1310, -1333.7180, 25.2290>> fSpawnHeading = 186.5990	BREAK
				CASE 19			vSpawnPoint = <<765.8720, -1338.4380, 25.2280>> fSpawnHeading = 186.5990	BREAK
				CASE 20			vSpawnPoint = <<767.8730, -1337.2450, 25.2340>> fSpawnHeading = 186.5990	BREAK
				CASE 21			vSpawnPoint = <<768.4320, -1334.8560, 25.2360>> fSpawnHeading = 186.5990	BREAK
				CASE 22			vSpawnPoint = <<769.3570, -1338.3600, 25.2390>> fSpawnHeading = 186.5990	BREAK
				CASE 23			vSpawnPoint = <<769.4180, -1332.8870, 25.2380>> fSpawnHeading = 186.5990	BREAK
				CASE 24			vSpawnPoint = <<761.2990, -1340.8390, 25.2360>> fSpawnHeading = 186.5990	BREAK
				CASE 25			vSpawnPoint = <<763.9190, -1340.0570, 25.2270>> fSpawnHeading = 186.5990	BREAK
				CASE 26			vSpawnPoint = <<767.2690, -1339.9380, 25.2320>> fSpawnHeading = 186.5990	BREAK
				CASE 27			vSpawnPoint = <<757.4250, -1340.9170, 25.2370>> fSpawnHeading = 186.5990	BREAK
				CASE 28			vSpawnPoint = <<769.8920, -1336.4790, 25.2400>> fSpawnHeading = 186.5990	BREAK
				CASE 29			vSpawnPoint = <<759.4620, -1341.9410, 25.2520>> fSpawnHeading = 186.5990	BREAK
				CASE 30			vSpawnPoint = <<754.3668, -1337.8007, 26.1538>> fSpawnHeading = 186.5990	BREAK
				CASE 31			vSpawnPoint = <<754.4041, -1334.7726, 26.1582>> fSpawnHeading = 186.5990	BREAK
				CASE 32			vSpawnPoint = <<752.6573, -1336.2968, 26.1571>> fSpawnHeading = 186.5990	BREAK
				CASE 33			vSpawnPoint = <<752.5216, -1339.3066, 26.1518>> fSpawnHeading = 186.5990	BREAK
				CASE 34			vSpawnPoint = <<754.1912, -1340.9452, 26.1611>> fSpawnHeading = 186.5990	BREAK
				CASE 35			vSpawnPoint = <<750.9141, -1337.7600, 26.1549>> fSpawnHeading = 186.5990	BREAK
				CASE 36			vSpawnPoint = <<750.8959, -1334.7134, 26.1605>> fSpawnHeading = 186.5990	BREAK
				CASE 37			vSpawnPoint = <<750.8172, -1340.9584, 26.1612>> fSpawnHeading = 186.5990	BREAK
				CASE 38			vSpawnPoint = <<752.3954, -1342.3613, 26.1817>> fSpawnHeading = 186.5990	BREAK
				CASE 39			vSpawnPoint = <<755.8119, -1342.5126, 26.1841>> fSpawnHeading = 186.5990	BREAK
				CASE 40			vSpawnPoint = <<762.8767, -1342.1779, 26.1796>> fSpawnHeading = 186.5990	BREAK
				CASE 41			vSpawnPoint = <<765.8637, -1341.9771, 26.1771>> fSpawnHeading = 186.5990	BREAK
				CASE 42			vSpawnPoint = <<769.1707, -1342.0153, 26.1875>> fSpawnHeading = 186.5990	BREAK
				CASE 43			vSpawnPoint = <<771.5646, -1334.6587, 26.1691>> fSpawnHeading = 186.5990	BREAK
				CASE 44			vSpawnPoint = <<771.8871, -1338.1578, 26.1706>> fSpawnHeading = 186.5990	BREAK
				CASE 45			vSpawnPoint = <<770.5439, -1340.0690, 26.1666>> fSpawnHeading = 186.5990	BREAK
				CASE 46			vSpawnPoint = <<772.1812, -1341.9880, 26.1941>> fSpawnHeading = 186.5990	BREAK
				CASE 47			vSpawnPoint = <<764.4454, -1343.9663, 26.2057>> fSpawnHeading = 186.5990	BREAK
				CASE 48			vSpawnPoint = <<761.1936, -1343.9257, 26.2050>> fSpawnHeading = 186.5990	BREAK
				CASE 49			vSpawnPoint = <<767.7100, -1343.9590, 26.2100>> fSpawnHeading = 186.5990	BREAK
				CASE 50			vSpawnPoint = <<770.3782, -1343.9530, 26.2113>> fSpawnHeading = 186.5990	BREAK
				CASE 51			vSpawnPoint = <<758.0251, -1343.9596, 26.2053>> fSpawnHeading = 186.5990	BREAK
				CASE 52			vSpawnPoint = <<753.9040, -1344.2087, 26.2089>> fSpawnHeading = 186.5990	BREAK
				CASE 53			vSpawnPoint = <<750.4177, -1344.2133, 26.2089>> fSpawnHeading = 186.5990	BREAK
				CASE 54			vSpawnPoint = <<749.1681, -1336.4338, 26.1584>> fSpawnHeading = 186.5990	BREAK
				CASE 55			vSpawnPoint = <<749.0052, -1339.2128, 26.1521>> fSpawnHeading = 186.5990	BREAK
				CASE 56			vSpawnPoint = <<746.9368, -1334.6880, 26.1632>> fSpawnHeading = 186.5990	BREAK
				CASE 57			vSpawnPoint = <<747.1261, -1337.7668, 26.1562>> fSpawnHeading = 186.5990	BREAK
				CASE 58			vSpawnPoint = <<745.2109, -1336.2786, 26.1609>> fSpawnHeading = 186.5990	BREAK
				CASE 59			vSpawnPoint = <<745.5623, -1339.3043, 26.1524>> fSpawnHeading = 186.5990	BREAK
				CASE 60			vSpawnPoint = <<742.6710, -1334.9434, 26.1652>> fSpawnHeading = 186.5990	BREAK
				CASE 61			vSpawnPoint = <<742.8071, -1337.8340, 26.1566>> fSpawnHeading = 186.5990	BREAK
				CASE 62			vSpawnPoint = <<740.9340, -1339.5509, 26.1498>> fSpawnHeading = 186.5990	BREAK
				CASE 63			vSpawnPoint = <<740.7950, -1336.3220, 26.1617>> fSpawnHeading = 186.5990	BREAK
				CASE 64			vSpawnPoint = <<773.5950, -1336.4203, 26.1756>> fSpawnHeading = 186.5990	BREAK
				CASE 65			vSpawnPoint = <<773.3501, -1333.0527, 26.1710>> fSpawnHeading = 186.5990	BREAK
				CASE 66			vSpawnPoint = <<773.8527, -1340.0125, 26.1771>> fSpawnHeading = 186.5990	BREAK
				CASE 67			vSpawnPoint = <<773.9843, -1343.9192, 26.2234>> fSpawnHeading = 186.5990	BREAK
				CASE 68			vSpawnPoint = <<762.7301, -1345.9423, 26.2343>> fSpawnHeading = 186.5990	BREAK
				CASE 69			vSpawnPoint = <<759.5247, -1345.9733, 26.2347>> fSpawnHeading = 186.5990	BREAK
				CASE 70			vSpawnPoint = <<766.0520, -1346.1132, 26.2370>> fSpawnHeading = 186.5990	BREAK
				CASE 71			vSpawnPoint = <<756.1135, -1346.1809, 26.2377>> fSpawnHeading = 186.5990	BREAK
				DEFAULT 		RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<347.6560, -976.3430, 28.3170>> fSpawnHeading = 358.9970	BREAK
				CASE 1			vSpawnPoint = <<349.3800, -974.9680, 28.4000>> fSpawnHeading = 358.9970	BREAK
				CASE 2			vSpawnPoint = <<346.4150, -975.1660, 28.3860>> fSpawnHeading = 358.9970	BREAK
				CASE 3			vSpawnPoint = <<347.7390, -973.7690, 28.3620>> fSpawnHeading = 358.9970	BREAK
				CASE 4			vSpawnPoint = <<349.4090, -972.4540, 28.4150>> fSpawnHeading = 358.9970	BREAK
				CASE 5			vSpawnPoint = <<346.4000, -972.5780, 28.4030>> fSpawnHeading = 358.9970	BREAK
				CASE 6			vSpawnPoint = <<347.7930, -971.3050, 28.4050>> fSpawnHeading = 358.9970	BREAK
				CASE 7			vSpawnPoint = <<349.5530, -970.0090, 28.4240>> fSpawnHeading = 358.9970	BREAK
				CASE 8			vSpawnPoint = <<346.4690, -969.8250, 28.4120>> fSpawnHeading = 358.9970	BREAK
				CASE 9			vSpawnPoint = <<348.0500, -968.4380, 28.4200>> fSpawnHeading = 358.9970	BREAK
				CASE 10			vSpawnPoint = <<349.5450, -966.9440, 28.4300>> fSpawnHeading = 358.9970	BREAK
				CASE 11			vSpawnPoint = <<346.5300, -966.7150, 28.4270>> fSpawnHeading = 358.9970	BREAK
				CASE 12			vSpawnPoint = <<349.2330, -977.6160, 28.3810>> fSpawnHeading = 358.9970	BREAK
				CASE 13			vSpawnPoint = <<347.8150, -979.2830, 28.3090>> fSpawnHeading = 358.9970	BREAK
				CASE 14			vSpawnPoint = <<349.3710, -980.5910, 28.3840>> fSpawnHeading = 358.9970	BREAK
				CASE 15			vSpawnPoint = <<346.6410, -980.4770, 28.3430>> fSpawnHeading = 358.9970	BREAK
				CASE 16			vSpawnPoint = <<347.9610, -965.7080, 28.4320>> fSpawnHeading = 358.9970	BREAK
				CASE 17			vSpawnPoint = <<349.7940, -964.2230, 28.4320>> fSpawnHeading = 358.9970	BREAK
				CASE 18			vSpawnPoint = <<346.4340, -964.1480, 28.4320>> fSpawnHeading = 358.9970	BREAK
				CASE 19			vSpawnPoint = <<347.8900, -962.5240, 28.4320>> fSpawnHeading = 358.9970	BREAK
				CASE 20			vSpawnPoint = <<350.6240, -962.5420, 28.4320>> fSpawnHeading = 358.9970	BREAK
				CASE 21			vSpawnPoint = <<344.9150, -962.5050, 28.4310>> fSpawnHeading = 358.9970	BREAK
				CASE 22			vSpawnPoint = <<347.8330, -982.3130, 28.3050>> fSpawnHeading = 358.9970	BREAK
				CASE 23			vSpawnPoint = <<349.3770, -983.7690, 28.3820>> fSpawnHeading = 358.9970	BREAK
				CASE 24			vSpawnPoint = <<346.5310, -983.8280, 28.3410>> fSpawnHeading = 358.9970	BREAK
				CASE 25			vSpawnPoint = <<352.4790, -964.4050, 28.4320>> fSpawnHeading = 358.9970	BREAK
				CASE 26			vSpawnPoint = <<353.5890, -962.7190, 28.4320>> fSpawnHeading = 358.9970	BREAK
				CASE 27			vSpawnPoint = <<349.2050, -960.7150, 28.4320>> fSpawnHeading = 358.9970	BREAK
				CASE 28			vSpawnPoint = <<346.5580, -960.6950, 28.4320>> fSpawnHeading = 358.9970	BREAK
				CASE 29			vSpawnPoint = <<342.0880, -962.6280, 28.4280>> fSpawnHeading = 358.9970	BREAK
				CASE 30			vSpawnPoint = <<347.8759, -985.7474, 29.2285>> fSpawnHeading = 358.9970	BREAK
				CASE 31			vSpawnPoint = <<349.4728, -987.4607, 29.3071>> fSpawnHeading = 358.9970	BREAK
				CASE 32			vSpawnPoint = <<346.3593, -987.3902, 29.2635>> fSpawnHeading = 358.9970	BREAK
				CASE 33			vSpawnPoint = <<347.5988, -989.0599, 29.1995>> fSpawnHeading = 358.9970	BREAK
				CASE 34			vSpawnPoint = <<349.3223, -990.6457, 29.2980>> fSpawnHeading = 358.9970	BREAK
				CASE 35			vSpawnPoint = <<346.3303, -990.6836, 29.2602>> fSpawnHeading = 358.9970	BREAK
				CASE 36			vSpawnPoint = <<347.7475, -992.6934, 29.2027>> fSpawnHeading = 358.9970	BREAK
				CASE 37			vSpawnPoint = <<349.3843, -994.4186, 29.3012>> fSpawnHeading = 358.9970	BREAK
				CASE 38			vSpawnPoint = <<346.6975, -994.6382, 29.2507>> fSpawnHeading = 358.9970	BREAK
				CASE 39			vSpawnPoint = <<351.9504, -960.9882, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 40			vSpawnPoint = <<355.7144, -961.0340, 29.4324>> fSpawnHeading = 358.9970	BREAK
				CASE 41			vSpawnPoint = <<355.7383, -964.5036, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 42			vSpawnPoint = <<357.2984, -962.8862, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 43			vSpawnPoint = <<359.2787, -964.4230, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 44			vSpawnPoint = <<360.7508, -963.0359, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 45			vSpawnPoint = <<362.5118, -964.4435, 29.3656>> fSpawnHeading = 358.9970	BREAK
				CASE 46			vSpawnPoint = <<339.9315, -964.0328, 29.3505>> fSpawnHeading = 358.9970	BREAK
				CASE 47			vSpawnPoint = <<340.0760, -961.3004, 29.3507>> fSpawnHeading = 358.9970	BREAK
				CASE 48			vSpawnPoint = <<338.2293, -962.7299, 29.3491>> fSpawnHeading = 358.9970	BREAK
				CASE 49			vSpawnPoint = <<336.3211, -961.1545, 29.3475>> fSpawnHeading = 358.9970	BREAK
				CASE 50			vSpawnPoint = <<335.1268, -962.4709, 29.3465>> fSpawnHeading = 358.9970	BREAK
				CASE 51			vSpawnPoint = <<333.0524, -964.1157, 29.3476>> fSpawnHeading = 358.9970	BREAK
				CASE 52			vSpawnPoint = <<332.9376, -961.1038, 29.3447>> fSpawnHeading = 358.9970	BREAK
				CASE 53			vSpawnPoint = <<362.8210, -962.3811, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 54			vSpawnPoint = <<359.2499, -961.7299, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 55			vSpawnPoint = <<364.9359, -964.2354, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 56			vSpawnPoint = <<367.2766, -965.0809, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 57			vSpawnPoint = <<367.3596, -962.0756, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 58			vSpawnPoint = <<369.7256, -963.6904, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 59			vSpawnPoint = <<371.4330, -965.4048, 29.3571>> fSpawnHeading = 358.9970	BREAK
				CASE 60			vSpawnPoint = <<371.5349, -961.7150, 29.3573>> fSpawnHeading = 358.9970	BREAK
				CASE 61			vSpawnPoint = <<330.8013, -962.3763, 29.3448>> fSpawnHeading = 358.9970	BREAK
				CASE 62			vSpawnPoint = <<328.0904, -963.6010, 29.3445>> fSpawnHeading = 358.9970	BREAK
				CASE 63			vSpawnPoint = <<329.4832, -960.3737, 29.3062>> fSpawnHeading = 358.9970	BREAK
				CASE 64			vSpawnPoint = <<338.2084, -959.9001, 29.3491>> fSpawnHeading = 358.9970	BREAK
				CASE 65			vSpawnPoint = <<364.8882, -961.8905, 29.3558>> fSpawnHeading = 358.9970	BREAK
				CASE 66			vSpawnPoint = <<327.4553, -961.8955, 29.3428>> fSpawnHeading = 358.9970	BREAK
				CASE 67			vSpawnPoint = <<325.1670, -963.5228, 29.3429>> fSpawnHeading = 358.9970	BREAK
				CASE 68			vSpawnPoint = <<325.4754, -960.8671, 29.3428>> fSpawnHeading = 358.9970	BREAK
				CASE 69			vSpawnPoint = <<323.5280, -961.8379, 29.3428>> fSpawnHeading = 358.9970	BREAK
				CASE 70			vSpawnPoint = <<332.6259, -1011.8000, 29.2162>> fSpawnHeading = 358.997	BREAK
				CASE 71			vSpawnPoint = <<329.8448, -1011.5649, 29.2147>> fSpawnHeading = 358.9970 BREAK
				DEFAULT			RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<-118.4010, -1258.7870, 28.3110>> fSpawnHeading = 278.6000	BREAK
				CASE 1			vSpawnPoint = <<-120.5380, -1261.7841, 28.3080>> fSpawnHeading = 278.6000	BREAK
				CASE 2			vSpawnPoint = <<-120.0000, -1256.4640, 28.3060>> fSpawnHeading = 278.6000	BREAK
				CASE 3			vSpawnPoint = <<-116.8160, -1260.1810, 28.3050>> fSpawnHeading = 278.6000	BREAK
				CASE 4			vSpawnPoint = <<-117.1700, -1256.2520, 28.2890>> fSpawnHeading = 278.6000	BREAK
				CASE 5			vSpawnPoint = <<-119.3490, -1263.2180, 28.3080>> fSpawnHeading = 278.6000	BREAK
				CASE 6			vSpawnPoint = <<-120.8760, -1264.7760, 28.3060>> fSpawnHeading = 278.6000	BREAK
				CASE 7			vSpawnPoint = <<-119.1430, -1266.0490, 28.3040>> fSpawnHeading = 278.6000	BREAK
				CASE 8			vSpawnPoint = <<-120.8510, -1267.4910, 28.3040>> fSpawnHeading = 278.6000	BREAK
				CASE 9			vSpawnPoint = <<-119.3430, -1268.7480, 28.2910>> fSpawnHeading = 278.6000	BREAK
				CASE 10			vSpawnPoint = <<-118.9330, -1254.3440, 28.2500>> fSpawnHeading = 278.6000	BREAK
				CASE 11			vSpawnPoint = <<-119.6470, -1252.0760, 28.2860>> fSpawnHeading = 278.6000	BREAK
				CASE 12			vSpawnPoint = <<-121.5370, -1253.9270, 28.3100>> fSpawnHeading = 278.6000	BREAK
				CASE 13			vSpawnPoint = <<-120.7600, -1270.2340, 28.3030>> fSpawnHeading = 278.6000	BREAK
				CASE 14			vSpawnPoint = <<-119.2810, -1271.5959, 28.3020>> fSpawnHeading = 278.6000	BREAK
				CASE 15			vSpawnPoint = <<-120.7200, -1272.9320, 28.3030>> fSpawnHeading = 278.6000	BREAK
				CASE 16			vSpawnPoint = <<-119.5000, -1274.8600, 28.3020>> fSpawnHeading = 278.6000	BREAK
				CASE 17			vSpawnPoint = <<-118.0870, -1264.3430, 28.3020>> fSpawnHeading = 278.6000	BREAK
				CASE 18			vSpawnPoint = <<-117.9530, -1267.3170, 28.3010>> fSpawnHeading = 278.6000	BREAK
				CASE 19			vSpawnPoint = <<-117.5670, -1269.7970, 28.2820>> fSpawnHeading = 278.6000	BREAK
				CASE 20			vSpawnPoint = <<-118.0090, -1273.1890, 28.3000>> fSpawnHeading = 278.6000	BREAK
				CASE 21			vSpawnPoint = <<-118.6970, -1261.1890, 28.3100>> fSpawnHeading = 278.6000	BREAK
				CASE 22			vSpawnPoint = <<-120.7620, -1276.1990, 28.3020>> fSpawnHeading = 278.6000	BREAK
				CASE 23			vSpawnPoint = <<-118.2510, -1276.1740, 28.3000>> fSpawnHeading = 278.6000	BREAK
				CASE 24			vSpawnPoint = <<-116.1150, -1275.6350, 28.2890>> fSpawnHeading = 278.6000	BREAK
				CASE 25			vSpawnPoint = <<-119.2960, -1278.1370, 28.3010>> fSpawnHeading = 278.6000	BREAK
				CASE 26			vSpawnPoint = <<-120.8870, -1279.6810, 28.3010>> fSpawnHeading = 278.6000	BREAK
				CASE 27			vSpawnPoint = <<-116.8920, -1278.3890, 28.2990>> fSpawnHeading = 278.6000	BREAK
				CASE 28			vSpawnPoint = <<-118.4190, -1279.9260, 28.3000>> fSpawnHeading = 278.6000	BREAK
				CASE 29			vSpawnPoint = <<-119.6960, -1281.6290, 28.3000>> fSpawnHeading = 278.6000	BREAK
				CASE 30			vSpawnPoint = <<-115.9691, -1272.1851, 29.1894>> fSpawnHeading = 278.6000	BREAK
				CASE 31			vSpawnPoint = <<-115.8067, -1268.0741, 29.2191>> fSpawnHeading = 278.6000	BREAK
				CASE 32			vSpawnPoint = <<-116.5929, -1262.7933, 29.2243>> fSpawnHeading = 278.6000	BREAK
				CASE 33			vSpawnPoint = <<-116.2193, -1265.9590, 29.2278>> fSpawnHeading = 278.6000	BREAK
				CASE 34			vSpawnPoint = <<-119.8490, -1249.6661, 29.1693>> fSpawnHeading = 278.6000	BREAK
				CASE 35			vSpawnPoint = <<-120.2117, -1246.5193, 29.0908>> fSpawnHeading = 278.6000	BREAK
				CASE 36			vSpawnPoint = <<-116.7625, -1281.6698, 29.2531>> fSpawnHeading = 278.6000	BREAK
				CASE 37			vSpawnPoint = <<-118.2366, -1283.4156, 29.2229>> fSpawnHeading = 278.6000	BREAK
				CASE 38			vSpawnPoint = <<-120.7993, -1283.5640, 29.2244>> fSpawnHeading = 278.6000	BREAK
				CASE 39			vSpawnPoint = <<-116.8981, -1285.2290, 29.2374>> fSpawnHeading = 278.6000	BREAK
				CASE 40			vSpawnPoint = <<-119.6403, -1285.2400, 29.2233>> fSpawnHeading = 278.6000	BREAK
				CASE 41			vSpawnPoint = <<-120.6893, -1287.1239, 29.2235>> fSpawnHeading = 278.6000	BREAK
				CASE 42			vSpawnPoint = <<-118.3756, -1287.0253, 29.2251>> fSpawnHeading = 278.6000	BREAK
				CASE 43			vSpawnPoint = <<-115.5592, -1287.1398, 29.2690>> fSpawnHeading = 278.6000	BREAK
				CASE 44			vSpawnPoint = <<-115.6197, -1283.4011, 29.2463>> fSpawnHeading = 278.6000	BREAK
				CASE 45			vSpawnPoint = <<-115.4814, -1279.8114, 29.2259>> fSpawnHeading = 278.6000	BREAK
				CASE 46			vSpawnPoint = <<-119.5312, -1288.9224, 29.2224>> fSpawnHeading = 278.6000	BREAK
				CASE 47			vSpawnPoint = <<-116.9861, -1288.8219, 29.2572>> fSpawnHeading = 278.6000	BREAK
				CASE 48			vSpawnPoint = <<-118.1875, -1290.4170, 29.2483>> fSpawnHeading = 278.6000	BREAK
				CASE 49			vSpawnPoint = <<-120.8558, -1290.4971, 29.2228>> fSpawnHeading = 278.6000	BREAK
				CASE 50			vSpawnPoint = <<-115.5361, -1290.3673, 29.2867>> fSpawnHeading = 278.6000	BREAK
				CASE 51			vSpawnPoint = <<-116.8239, -1292.1697, 29.2541>> fSpawnHeading = 278.6000	BREAK
				CASE 52			vSpawnPoint = <<-119.4930, -1292.0614, 29.2381>> fSpawnHeading = 278.6000	BREAK
				CASE 53			vSpawnPoint = <<-123.9346, -1254.7253, 29.2119>> fSpawnHeading = 278.6000	BREAK
				CASE 54			vSpawnPoint = <<-126.1302, -1252.4934, 29.2198>> fSpawnHeading = 278.6000	BREAK
				CASE 55			vSpawnPoint = <<-127.6199, -1254.5765, 29.3246>> fSpawnHeading = 278.6000	BREAK
				CASE 56			vSpawnPoint = <<-120.3428, -1244.1998, 29.0406>> fSpawnHeading = 278.6000	BREAK
				CASE 57			vSpawnPoint = <<-121.4871, -1240.1259, 28.9498>> fSpawnHeading = 278.6000	BREAK
				CASE 58			vSpawnPoint = <<-118.3076, -1294.1183, 29.2247>> fSpawnHeading = 278.6000	BREAK
				CASE 59			vSpawnPoint = <<-115.6611, -1294.2145, 29.1389>> fSpawnHeading = 278.6000	BREAK
				CASE 60			vSpawnPoint = <<-120.9802, -1293.8617, 29.2247>> fSpawnHeading = 278.6000	BREAK
				CASE 61			vSpawnPoint = <<-119.8623, -1295.8610, 29.2329>> fSpawnHeading = 278.6000	BREAK
				CASE 62			vSpawnPoint = <<-117.1240, -1295.8098, 29.2229>> fSpawnHeading = 278.6000	BREAK
				CASE 63			vSpawnPoint = <<-118.3021, -1297.5295, 29.2286>> fSpawnHeading = 278.6000	BREAK
				CASE 64			vSpawnPoint = <<-121.1065, -1297.4277, 29.2398>> fSpawnHeading = 278.6000	BREAK
				CASE 65			vSpawnPoint = <<-115.6375, -1297.6265, 29.1364>> fSpawnHeading = 278.6000	BREAK
				CASE 66			vSpawnPoint = <<-117.0004, -1299.7047, 29.2223>> fSpawnHeading = 278.6000	BREAK
				CASE 67			vSpawnPoint = <<-119.8554, -1299.6012, 29.2390>> fSpawnHeading = 278.6000	BREAK
				CASE 68			vSpawnPoint = <<-129.2290, -1252.6511, 29.4955>> fSpawnHeading = 278.6000	BREAK
				CASE 69			vSpawnPoint = <<-130.5933, -1254.4445, 29.6405>> fSpawnHeading = 278.6000	BREAK
				CASE 70			vSpawnPoint = <<-123.3220, -1252.8301, 29.2177>> fSpawnHeading = 278.6000	BREAK
				CASE 71			vSpawnPoint = <<-117.7193, -1252.0277, 29.1846>> fSpawnHeading = 278.6000	BREAK
				DEFAULT 		RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<7.8870, 219.7020, 106.7780>> fSpawnHeading = 254.1990	BREAK
				CASE 1			vSpawnPoint = <<7.0210, 217.4570, 106.4520>> fSpawnHeading = 254.1990	BREAK
				CASE 2			vSpawnPoint = <<8.7220, 222.1400, 107.1250>> fSpawnHeading = 254.1990	BREAK
				CASE 3			vSpawnPoint = <<9.9650, 220.0210, 106.9090>> fSpawnHeading = 254.1990	BREAK
				CASE 4			vSpawnPoint = <<8.1320, 215.4360, 106.2220>> fSpawnHeading = 254.1990	BREAK
				CASE 5			vSpawnPoint = <<6.0320, 215.0080, 106.0750>> fSpawnHeading = 254.1990	BREAK
				CASE 6			vSpawnPoint = <<3.6250, 213.8180, 105.7960>> fSpawnHeading = 254.1990	BREAK
				CASE 7			vSpawnPoint = <<5.4000, 212.2840, 105.6310>> fSpawnHeading = 254.1990	BREAK
				CASE 8			vSpawnPoint = <<2.8900, 211.6770, 105.3930>> fSpawnHeading = 254.1990	BREAK
				CASE 9			vSpawnPoint = <<4.0110, 210.0380, 105.1870>> fSpawnHeading = 254.1990	BREAK
				CASE 10			vSpawnPoint = <<6.9220, 224.2560, 107.3270>> fSpawnHeading = 254.1990	BREAK
				CASE 11			vSpawnPoint = <<8.6930, 225.0520, 107.4960>> fSpawnHeading = 254.1990	BREAK
				CASE 12			vSpawnPoint = <<7.7210, 226.7310, 107.6490>> fSpawnHeading = 254.1990	BREAK
				CASE 13			vSpawnPoint = <<9.5150, 227.8860, 107.8390>> fSpawnHeading = 254.1990	BREAK
				CASE 14			vSpawnPoint = <<8.8020, 230.3550, 108.0820>> fSpawnHeading = 254.1990	BREAK
				CASE 15			vSpawnPoint = <<10.7480, 231.3590, 108.2510>> fSpawnHeading = 254.1990	BREAK
				CASE 16			vSpawnPoint = <<9.9650, 233.5040, 108.4360>> fSpawnHeading = 254.1990	BREAK
				CASE 17			vSpawnPoint = <<1.9360, 209.3270, 104.9380>> fSpawnHeading = 254.1990	BREAK
				CASE 18			vSpawnPoint = <<4.6900, 208.1760, 104.9070>> fSpawnHeading = 254.1990	BREAK
				CASE 19			vSpawnPoint = <<4.8350, 216.6750, 106.2520>> fSpawnHeading = 254.1990	BREAK
				CASE 20			vSpawnPoint = <<6.8820, 221.7180, 106.9920>> fSpawnHeading = 254.1990	BREAK
				CASE 21			vSpawnPoint = <<6.0250, 218.6660, 106.5650>> fSpawnHeading = 254.1990	BREAK
				CASE 22			vSpawnPoint = <<11.4190, 229.1030, 108.0560>> fSpawnHeading = 254.1990	BREAK
				CASE 23			vSpawnPoint = <<12.6910, 232.5490, 108.4230>> fSpawnHeading = 254.1990	BREAK
				CASE 24			vSpawnPoint = <<10.4050, 225.9540, 107.6690>> fSpawnHeading = 254.1990	BREAK
				CASE 25			vSpawnPoint = <<11.8770, 227.1420, 107.8400>> fSpawnHeading = 254.1990	BREAK
				CASE 26			vSpawnPoint = <<11.9820, 234.5910, 108.5150>> fSpawnHeading = 254.1990	BREAK
				CASE 27			vSpawnPoint = <<14.8220, 234.3810, 108.5360>> fSpawnHeading = 254.1990	BREAK
				CASE 28			vSpawnPoint = <<-0.5770, 201.8940, 103.4180>> fSpawnHeading = 254.1990	BREAK
				CASE 29			vSpawnPoint = <<0.6790, 199.9500, 103.1520>> fSpawnHeading = 254.1990	BREAK
				CASE 30			vSpawnPoint = <<7.3592, 212.8575, 106.7538>> fSpawnHeading = 254.1990 	BREAK
				CASE 31			vSpawnPoint = <<6.4950, 210.2321, 106.2794>> fSpawnHeading = 254.1990 	BREAK
				CASE 32			vSpawnPoint = <<10.7166, 223.4020, 108.2983>> fSpawnHeading = 254.1990	BREAK
				CASE 33			vSpawnPoint = <<13.1155, 230.3741, 109.1524>> fSpawnHeading = 254.1990	BREAK
				CASE 34			vSpawnPoint = <<7.6568, 232.5551, 109.1936>> fSpawnHeading = 254.1990 	BREAK
				CASE 35			vSpawnPoint = <<13.8580, 236.4800, 109.4971>> fSpawnHeading = 254.1990	BREAK
				CASE 36			vSpawnPoint = <<16.0569, 237.2421, 109.4903>> fSpawnHeading = 254.1990	BREAK
				CASE 37			vSpawnPoint = <<12.6419, 238.5411, 109.4938>> fSpawnHeading = 254.1990	BREAK
				CASE 38			vSpawnPoint = <<8.6842, 235.4609, 109.4462>> fSpawnHeading = 254.1990 	BREAK
				CASE 39			vSpawnPoint = <<15.0259, 239.7681, 109.4848>> fSpawnHeading = 254.1990	BREAK
				CASE 40			vSpawnPoint = <<13.7337, 241.9350, 109.4775>> fSpawnHeading = 254.1990	BREAK
				CASE 41			vSpawnPoint = <<17.0160, 240.8575, 109.4770>> fSpawnHeading = 254.1990	BREAK
				CASE 42			vSpawnPoint = <<2.4656, 201.2425, 104.4335>> fSpawnHeading = 254.1990 	BREAK
				CASE 43			vSpawnPoint = <<-1.9057, 199.1096, 103.7453>> fSpawnHeading = 254.1990	BREAK
				CASE 44			vSpawnPoint = <<1.0918, 198.2460, 103.7611>> fSpawnHeading = 254.1990 	BREAK
				CASE 45			vSpawnPoint = <<-0.6631, 197.2233, 103.4484>> fSpawnHeading = 254.1990	BREAK
				CASE 46			vSpawnPoint = <<-2.6857, 196.3057, 103.1224>> fSpawnHeading = 254.1990	BREAK
				CASE 47			vSpawnPoint = <<0.1340, 195.4841, 103.1442>> fSpawnHeading = 254.1990 	BREAK
				CASE 48			vSpawnPoint = <<15.8757, 242.7540, 109.4203>> fSpawnHeading = 254.1990	BREAK
				CASE 49			vSpawnPoint = <<14.9103, 245.2472, 109.4186>> fSpawnHeading = 254.1990	BREAK
				CASE 50			vSpawnPoint = <<17.9625, 244.5082, 109.3886>> fSpawnHeading = 254.1990	BREAK
				CASE 51			vSpawnPoint = <<-5.0368, 189.8035, 101.5460>> fSpawnHeading = 254.1990	BREAK
				CASE 52			vSpawnPoint = <<-2.0184, 188.6088, 101.5090>> fSpawnHeading = 254.19900	BREAK
				CASE 53			vSpawnPoint = <<-3.9032, 187.7691, 101.2028>> fSpawnHeading = 254.19900	BREAK
				CASE 54			vSpawnPoint = <<-5.9890, 186.5644, 100.8031>> fSpawnHeading = 254.19900	BREAK
				CASE 55			vSpawnPoint = <<-3.5417, 185.5753, 100.7420>> fSpawnHeading = 254.19900	BREAK
				CASE 56			vSpawnPoint = <<-5.3746, 184.3742, 100.3358>> fSpawnHeading = 254.19900	BREAK
				CASE 57			vSpawnPoint = <<-7.5622, 183.2711, 99.8764>> fSpawnHeading = 254.1990	BREAK
				CASE 58			vSpawnPoint = <<-4.8323, 182.2999, 99.9123>> fSpawnHeading = 254.1990	BREAK
				CASE 59			vSpawnPoint = <<-6.4686, 181.3127, 99.5212>> fSpawnHeading = 254.1990 	BREAK
				CASE 60			vSpawnPoint = <<-8.7880, 179.9622, 98.9925>> fSpawnHeading = 254.1990 	BREAK
				CASE 61			vSpawnPoint = <<-5.8577, 178.9784, 99.0105>> fSpawnHeading = 254.1990 	BREAK
				CASE 62			vSpawnPoint = <<4.8195, 205.9736, 105.4498>> fSpawnHeading = 254.1990 	BREAK
				CASE 63			vSpawnPoint = <<2.7034, 206.7626, 105.4496>> fSpawnHeading = 254.1990 	BREAK
				CASE 64			vSpawnPoint = <<0.5674, 205.7946, 105.1362>> fSpawnHeading = 254.1990 	BREAK
				CASE 65			vSpawnPoint = <<3.1759, 204.7159, 105.1127>> fSpawnHeading = 254.1990 	BREAK
				CASE 66			vSpawnPoint = <<1.5438, 203.1990, 104.7294>> fSpawnHeading = 254.1990 	BREAK
				CASE 67			vSpawnPoint = <<4.1406, 203.0314, 104.8494>> fSpawnHeading = 254.1990 	BREAK
				CASE 68			vSpawnPoint = <<-1.1488, 204.3284, 104.7512>> fSpawnHeading = 254.1990 	BREAK
				CASE 69			vSpawnPoint = <<0.2765, 207.9382, 105.5003>> fSpawnHeading = 254.1990 	BREAK
				CASE 70			vSpawnPoint = <<12.5132, 244.3075, 109.4695>> fSpawnHeading = 254.1990 	BREAK
				CASE 71			vSpawnPoint = <<14.0009, 247.4655, 109.4766>> fSpawnHeading = 254.1990 	BREAK
				DEFAULT 		RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<868.0805, -2098.5251, 29.3763>> fSpawnHeading = 34.2000	BREAK
				CASE 1			vSpawnPoint = <<866.1360, -2099.3330, 29.3338>> fSpawnHeading = 30.6000	BREAK
				CASE 2			vSpawnPoint = <<867.0684, -2097.1858, 29.3324>> fSpawnHeading = 38.0000	BREAK
				CASE 3			vSpawnPoint = <<869.4386, -2097.1885, 29.3897>> fSpawnHeading = 34.2000	BREAK
				CASE 4			vSpawnPoint = <<868.1802, -2095.3792, 29.3261>> fSpawnHeading = 21.0000	BREAK
				CASE 5			vSpawnPoint = <<865.6517, -2095.5728, 29.2704>> fSpawnHeading = 21.0000	BREAK
				CASE 6			vSpawnPoint = <<870.5490, -2095.3660, 29.3890>> fSpawnHeading = 0.0000	BREAK
				CASE 7			vSpawnPoint = <<862.8610, -2094.8811, 29.2090>> fSpawnHeading = 0.0000	BREAK
				CASE 8			vSpawnPoint = <<867.0100, -2093.4121, 29.2550>> fSpawnHeading = 0.0000	BREAK
				CASE 9			vSpawnPoint = <<869.2840, -2093.5481, 29.3500>> fSpawnHeading = 0.0000	BREAK
				CASE 10			vSpawnPoint = <<864.2220, -2093.2041, 29.2090>> fSpawnHeading = 0.0000	BREAK
				CASE 11			vSpawnPoint = <<871.4430, -2093.9180, 29.4090>> fSpawnHeading = 0.0000	BREAK
				CASE 12			vSpawnPoint = <<868.2470, -2091.9890, 29.2930>> fSpawnHeading = 0.0000	BREAK
				CASE 13			vSpawnPoint = <<865.6300, -2091.7781, 29.2470>> fSpawnHeading = 0.0000	BREAK
				CASE 14			vSpawnPoint = <<870.5240, -2092.1050, 29.3820>> fSpawnHeading = 0.0000	BREAK
				CASE 15			vSpawnPoint = <<862.9220, -2091.4929, 29.2270>> fSpawnHeading = 0.0000	BREAK
				CASE 16			vSpawnPoint = <<873.0960, -2092.3069, 29.4590>> fSpawnHeading = 0.0000	BREAK
				CASE 17			vSpawnPoint = <<861.3320, -2093.0940, 29.2190>> fSpawnHeading = 0.0000	BREAK
				CASE 18			vSpawnPoint = <<859.3090, -2094.6780, 29.2720>> fSpawnHeading = 0.0000	BREAK
				CASE 19			vSpawnPoint = <<859.5280, -2091.4221, 29.2540>> fSpawnHeading = 0.0000	BREAK
				CASE 20			vSpawnPoint = <<867.0920, -2090.3840, 29.2840>> fSpawnHeading = 0.0000	BREAK
				CASE 21			vSpawnPoint = <<869.4200, -2090.5940, 29.3490>> fSpawnHeading = 0.0000	BREAK
				CASE 22			vSpawnPoint = <<871.8610, -2090.7290, 29.4480>> fSpawnHeading = 0.0000	BREAK
				CASE 23			vSpawnPoint = <<864.4470, -2090.2900, 29.2460>> fSpawnHeading = 0.0000	BREAK
				CASE 24			vSpawnPoint = <<861.2950, -2090.0371, 29.2610>> fSpawnHeading = 0.0000	BREAK
				CASE 25			vSpawnPoint = <<868.3450, -2089.0891, 29.2730>> fSpawnHeading = 5.2000	BREAK
				CASE 26			vSpawnPoint = <<870.8840, -2089.2729, 29.3640>> fSpawnHeading = 5.2000	BREAK
				CASE 27			vSpawnPoint = <<865.8900, -2088.8669, 29.2400>> fSpawnHeading = 5.2000	BREAK
				CASE 28			vSpawnPoint = <<875.2140, -2093.5930, 29.4780>> fSpawnHeading = 5.2000	BREAK
				CASE 29			vSpawnPoint = <<876.0980, -2091.8020, 29.5330>> fSpawnHeading = 5.2000	BREAK
				CASE 30			vSpawnPoint = <<863.0161, -2088.4602, 30.1967>> fSpawnHeading = 0.0000	BREAK
				CASE 31			vSpawnPoint = <<859.7725, -2088.3850, 30.1831>> fSpawnHeading = 0.0000	BREAK
				CASE 32			vSpawnPoint = <<857.9869, -2089.5134, 30.1704>> fSpawnHeading = 0.0000	BREAK
				CASE 33			vSpawnPoint = <<857.4555, -2092.8391, 30.1734>> fSpawnHeading = 0.0000	BREAK
				CASE 34			vSpawnPoint = <<855.9895, -2090.8777, 30.1562>> fSpawnHeading = 0.0000	BREAK
				CASE 35			vSpawnPoint = <<855.6149, -2094.0774, 30.1804>> fSpawnHeading = 0.0000	BREAK
				CASE 36			vSpawnPoint = <<856.2971, -2088.0833, 30.1506>> fSpawnHeading = 0.0000	BREAK
				CASE 37			vSpawnPoint = <<854.3940, -2089.1787, 30.1369>> fSpawnHeading = 0.0000	BREAK
				CASE 38			vSpawnPoint = <<854.0507, -2092.1069, 30.1527>> fSpawnHeading = 0.0000	BREAK
				CASE 39			vSpawnPoint = <<852.2547, -2090.3293, 30.1142>> fSpawnHeading = 0.0000	BREAK
				CASE 40			vSpawnPoint = <<852.0295, -2093.4746, 30.1564>> fSpawnHeading = 0.0000	BREAK
				CASE 41			vSpawnPoint = <<850.5279, -2091.6450, 30.1198>> fSpawnHeading = 0.0000	BREAK
				CASE 42			vSpawnPoint = <<850.5901, -2088.9446, 30.0986>> fSpawnHeading = 0.0000	BREAK
				CASE 43			vSpawnPoint = <<852.5483, -2087.5049, 30.1149>> fSpawnHeading = 0.0000	BREAK
				CASE 44			vSpawnPoint = <<848.7651, -2087.2986, 30.0682>> fSpawnHeading = 0.0000	BREAK
				CASE 45			vSpawnPoint = <<848.7159, -2089.8945, 30.0879>> fSpawnHeading = 0.0000	BREAK
				CASE 46			vSpawnPoint = <<846.9063, -2088.4292, 30.0549>> fSpawnHeading = 0.0000	BREAK
				CASE 47			vSpawnPoint = <<846.7492, -2091.0862, 30.0770>> fSpawnHeading = 0.0000	BREAK
				CASE 48			vSpawnPoint = <<874.7022, -2090.3733, 30.4059>> fSpawnHeading = 0.0000	BREAK
				CASE 49			vSpawnPoint = <<877.5873, -2090.5259, 30.4315>> fSpawnHeading = 0.0000	BREAK
				CASE 50			vSpawnPoint = <<877.3441, -2093.3813, 30.4411>> fSpawnHeading = 0.0000	BREAK
				CASE 51			vSpawnPoint = <<879.3192, -2092.1401, 30.4559>> fSpawnHeading = 0.0000	BREAK
				CASE 52			vSpawnPoint = <<879.7809, -2090.5815, 30.4538>> fSpawnHeading = 0.0000	BREAK
				CASE 53			vSpawnPoint = <<881.2828, -2091.7773, 30.4765>> fSpawnHeading = 0.0000	BREAK
				CASE 54			vSpawnPoint = <<882.7267, -2090.5852, 30.4959>> fSpawnHeading = 0.0000	BREAK
				CASE 55			vSpawnPoint = <<869.0352, -2103.1167, 30.3561>> fSpawnHeading = 0.0000	BREAK
				CASE 56			vSpawnPoint = <<866.5358, -2102.9565, 30.2918>> fSpawnHeading = 0.0000	BREAK
				CASE 57			vSpawnPoint = <<864.1221, -2102.7083, 30.2948>> fSpawnHeading = 0.0000	BREAK
				CASE 58			vSpawnPoint = <<865.2155, -2104.4856, 30.2569>> fSpawnHeading = 0.0000	BREAK
				CASE 59			vSpawnPoint = <<867.5792, -2104.7813, 30.3313>> fSpawnHeading = 0.0000	BREAK
				CASE 60			vSpawnPoint = <<866.1382, -2106.3237, 30.2990>> fSpawnHeading = 0.0000	BREAK
				CASE 61			vSpawnPoint = <<863.7782, -2105.9109, 30.3055>> fSpawnHeading = 0.0000	BREAK
				CASE 62			vSpawnPoint = <<869.1049, -2106.5322, 30.3818>> fSpawnHeading = 0.0000	BREAK
				CASE 63			vSpawnPoint = <<865.4845, -2101.6040, 30.2513>> fSpawnHeading = 0.0000	BREAK
				CASE 64			vSpawnPoint = <<865.1674, -2097.9983, 30.2136>> fSpawnHeading = 0.0000	BREAK
				CASE 65			vSpawnPoint = <<860.8644, -2095.8125, 30.2146>> fSpawnHeading = 0.0000	BREAK
				CASE 66			vSpawnPoint = <<857.4000, -2095.5669, 30.2014>> fSpawnHeading = 0.0000	BREAK
				CASE 67			vSpawnPoint = <<853.5042, -2095.2385, 30.1913>> fSpawnHeading = 0.0000	BREAK
				CASE 68			vSpawnPoint = <<849.9364, -2094.8875, 30.1727>> fSpawnHeading = 0.0000	BREAK
				CASE 69			vSpawnPoint = <<844.9335, -2089.5266, 30.0247>> fSpawnHeading = 0.0000	BREAK
				CASE 70			vSpawnPoint = <<843.6727, -2087.3499, 29.9857>> fSpawnHeading = 0.0000	BREAK
				CASE 71			vSpawnPoint = <<843.2185, -2090.7642, 30.0139>> fSpawnHeading = 0.0000	BREAK
				DEFAULT			RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<-679.0740, -2460.9399, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 1			vSpawnPoint = <<-676.9430, -2462.1360, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 2			vSpawnPoint = <<-681.4560, -2459.6060, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 3			vSpawnPoint = <<-682.0570, -2457.0950, 12.9150>> fSpawnHeading = 145.8000	BREAK
				CASE 4			vSpawnPoint = <<-679.5360, -2458.7581, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 5			vSpawnPoint = <<-681.0720, -2462.2329, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 6			vSpawnPoint = <<-678.7080, -2463.5049, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 7			vSpawnPoint = <<-674.2880, -2463.6650, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 8			vSpawnPoint = <<-676.4120, -2464.9570, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 9			vSpawnPoint = <<-680.9210, -2464.3840, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 10			vSpawnPoint = <<-678.3470, -2465.7671, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 11			vSpawnPoint = <<-675.6640, -2467.1399, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 12			vSpawnPoint = <<-673.2690, -2466.5410, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 13			vSpawnPoint = <<-684.1500, -2457.9951, 12.8930>> fSpawnHeading = 145.8000	BREAK
				CASE 14			vSpawnPoint = <<-683.6640, -2460.8650, 12.8660>> fSpawnHeading = 145.8000	BREAK
				CASE 15			vSpawnPoint = <<-685.8530, -2461.3191, 12.8480>> fSpawnHeading = 145.8000	BREAK
				CASE 16			vSpawnPoint = <<-677.8180, -2467.8069, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 17			vSpawnPoint = <<-680.4040, -2466.4250, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 18			vSpawnPoint = <<-671.6710, -2463.3979, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 19			vSpawnPoint = <<-671.0300, -2465.6531, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 20			vSpawnPoint = <<-674.8540, -2469.2000, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 21			vSpawnPoint = <<-672.7890, -2468.4761, 12.9440>> fSpawnHeading = 145.8000	BREAK
				CASE 22			vSpawnPoint = <<-685.3290, -2463.5291, 12.8330>> fSpawnHeading = 145.8000	BREAK
				CASE 23			vSpawnPoint = <<-686.5080, -2458.9880, 12.8680>> fSpawnHeading = 145.8000	BREAK
				CASE 24			vSpawnPoint = <<-684.7770, -2455.8750, 12.9110>> fSpawnHeading = 145.8000	BREAK
				CASE 25			vSpawnPoint = <<-687.9900, -2461.9771, 12.8320>> fSpawnHeading = 145.8000	BREAK
				CASE 26			vSpawnPoint = <<-686.8600, -2456.2959, 12.8940>> fSpawnHeading = 145.8000	BREAK
				CASE 27			vSpawnPoint = <<-688.4570, -2459.6121, 12.8500>> fSpawnHeading = 145.8000	BREAK
				CASE 28			vSpawnPoint = <<-687.3370, -2464.2310, 12.8300>> fSpawnHeading = 145.8000	BREAK
				CASE 29			vSpawnPoint = <<-684.6010, -2465.8181, 12.8280>> fSpawnHeading = 145.8000	BREAK
				CASE 30			vSpawnPoint = <<-689.2070, -2457.3110, 13.7931>> fSpawnHeading = 145.8000	BREAK
				CASE 31			vSpawnPoint = <<-687.5005, -2454.6628, 13.8316>> fSpawnHeading = 145.8000	BREAK
				CASE 32			vSpawnPoint = <<-690.6506, -2459.8191, 13.7582>> fSpawnHeading = 145.8000	BREAK
				CASE 33			vSpawnPoint = <<-690.0025, -2462.1042, 13.7556>> fSpawnHeading = 145.8000	BREAK
				CASE 34			vSpawnPoint = <<-691.6022, -2457.5300, 13.7763>> fSpawnHeading = 145.8000	BREAK
				CASE 35			vSpawnPoint = <<-689.8837, -2455.0769, 13.8126>> fSpawnHeading = 145.8000	BREAK
				CASE 36			vSpawnPoint = <<-688.6100, -2453.1174, 13.8419>> fSpawnHeading = 145.8000	BREAK
				CASE 37			vSpawnPoint = <<-690.6039, -2453.1055, 13.8299>> fSpawnHeading = 145.8000	BREAK
				CASE 38			vSpawnPoint = <<-691.8088, -2455.4087, 13.7972>> fSpawnHeading = 145.8000	BREAK
				CASE 39			vSpawnPoint = <<-693.2197, -2457.9299, 13.7622>> fSpawnHeading = 145.8000	BREAK
				CASE 40			vSpawnPoint = <<-692.7003, -2459.9268, 13.7544>> fSpawnHeading = 145.8000	BREAK
				CASE 41			vSpawnPoint = <<-695.0602, -2460.0171, 13.7539>> fSpawnHeading = 145.8000	BREAK
				CASE 42			vSpawnPoint = <<-692.4819, -2462.3887, 13.7551>> fSpawnHeading = 145.8000	BREAK
				CASE 43			vSpawnPoint = <<-689.4592, -2464.1990, 13.7530>> fSpawnHeading = 145.8000	BREAK
				CASE 44			vSpawnPoint = <<-686.2976, -2466.0764, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 45			vSpawnPoint = <<-683.5400, -2467.8533, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 46			vSpawnPoint = <<-695.5789, -2458.0396, 13.7532>> fSpawnHeading = 145.8000	BREAK
				CASE 47			vSpawnPoint = <<-694.1129, -2455.7542, 13.7795>> fSpawnHeading = 145.8000	BREAK
				CASE 48			vSpawnPoint = <<-692.4956, -2453.2329, 13.8170>> fSpawnHeading = 145.8000	BREAK
				CASE 49			vSpawnPoint = <<-693.3534, -2451.4788, 13.8311>> fSpawnHeading = 145.8000	BREAK
				CASE 50			vSpawnPoint = <<-694.8669, -2453.8091, 13.7962>> fSpawnHeading = 145.8000	BREAK
				CASE 51			vSpawnPoint = <<-695.3423, -2451.7620, 13.8159>> fSpawnHeading = 145.8000	BREAK
				CASE 52			vSpawnPoint = <<-691.5569, -2464.4426, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 53			vSpawnPoint = <<-688.5682, -2466.1111, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 54			vSpawnPoint = <<-685.7246, -2467.9226, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 55			vSpawnPoint = <<-694.5844, -2462.4990, 13.7535>> fSpawnHeading = 145.8000	BREAK
				CASE 56			vSpawnPoint = <<-697.3274, -2460.7390, 13.7537>> fSpawnHeading = 145.8000	BREAK
				CASE 57			vSpawnPoint = <<-696.7943, -2462.7778, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 58			vSpawnPoint = <<-693.7692, -2464.5430, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 59			vSpawnPoint = <<-690.9031, -2466.3813, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 60			vSpawnPoint = <<-687.9854, -2468.1741, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 61			vSpawnPoint = <<-684.6546, -2470.3040, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 62			vSpawnPoint = <<-693.1440, -2466.8528, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 63			vSpawnPoint = <<-690.3047, -2468.6418, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 64			vSpawnPoint = <<-687.3396, -2470.5042, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 65			vSpawnPoint = <<-695.8835, -2465.0989, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 66			vSpawnPoint = <<-698.9077, -2463.1858, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 67			vSpawnPoint = <<-692.5970, -2468.9614, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 68			vSpawnPoint = <<-689.4921, -2470.9563, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 69			vSpawnPoint = <<-686.4337, -2472.9214, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 70			vSpawnPoint = <<-695.0334, -2467.3967, 13.7526>> fSpawnHeading = 145.8000	BREAK
				CASE 71			vSpawnPoint = <<-698.1462, -2465.3972, 13.7526>> fSpawnHeading = 145.8000	BREAK
				DEFAULT			RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<192.3020, -3167.4231, 4.7900>> fSpawnHeading = 100.0000	BREAK
				CASE 1			vSpawnPoint = <<192.3320, -3165.3030, 4.7900>> fSpawnHeading = 100.0000	BREAK
				CASE 2			vSpawnPoint = <<192.2640, -3169.3789, 4.7900>> fSpawnHeading = 100.0000	BREAK
				CASE 3			vSpawnPoint = <<192.2420, -3171.5730, 4.7900>> fSpawnHeading = 100.0000	BREAK
				CASE 4			vSpawnPoint = <<192.4330, -3163.1150, 4.7900>> fSpawnHeading = 98.0000	BREAK
				CASE 5			vSpawnPoint = <<194.2460, -3164.2920, 4.7900>> fSpawnHeading = 98.0000	BREAK
				CASE 6			vSpawnPoint = <<193.9490, -3170.4719, 4.7900>> fSpawnHeading = 98.0000	BREAK
				CASE 7			vSpawnPoint = <<190.9060, -3168.5181, 4.7870>> fSpawnHeading = 98.0000	BREAK
				CASE 8			vSpawnPoint = <<191.0760, -3166.3391, 4.7880>> fSpawnHeading = 98.0000	BREAK
				CASE 9			vSpawnPoint = <<191.1160, -3164.2810, 4.7880>> fSpawnHeading = 98.0000	BREAK
				CASE 10			vSpawnPoint = <<191.0200, -3170.9331, 4.7870>> fSpawnHeading = 98.0000	BREAK
				CASE 11			vSpawnPoint = <<191.0060, -3162.0239, 4.7870>> fSpawnHeading = 98.0000	BREAK
				CASE 12			vSpawnPoint = <<191.1020, -3173.5129, 4.7880>> fSpawnHeading = 98.0000	BREAK
				CASE 13			vSpawnPoint = <<189.3660, -3167.5061, 4.7860>> fSpawnHeading = 98.0000	BREAK
				CASE 14			vSpawnPoint = <<189.4800, -3169.7241, 4.7850>> fSpawnHeading = 98.0000	BREAK
				CASE 15			vSpawnPoint = <<189.5290, -3165.3640, 4.7850>> fSpawnHeading = 98.0000	BREAK
				CASE 16			vSpawnPoint = <<189.5980, -3163.1289, 4.7850>> fSpawnHeading = 98.0000	BREAK
				CASE 17			vSpawnPoint = <<189.5580, -3172.3040, 4.7850>> fSpawnHeading = 98.0000	BREAK
				CASE 18			vSpawnPoint = <<193.9340, -3172.9509, 4.7900>> fSpawnHeading = 98.0000	BREAK
				CASE 19			vSpawnPoint = <<192.6010, -3174.8521, 4.7900>> fSpawnHeading = 98.0000	BREAK
				CASE 20			vSpawnPoint = <<189.5520, -3174.9971, 4.7850>> fSpawnHeading = 98.0000	BREAK
				CASE 21			vSpawnPoint = <<187.9810, -3168.6909, 4.7890>> fSpawnHeading = 98.0000	BREAK
				CASE 22			vSpawnPoint = <<188.0620, -3166.4929, 4.7890>> fSpawnHeading = 98.0000	BREAK
				CASE 23			vSpawnPoint = <<188.0200, -3164.3091, 4.7890>> fSpawnHeading = 98.0000	BREAK
				CASE 24			vSpawnPoint = <<188.0540, -3171.0090, 4.7890>> fSpawnHeading = 98.0000	BREAK
				CASE 25			vSpawnPoint = <<187.8690, -3173.8191, 4.7890>> fSpawnHeading = 98.0000	BREAK
				CASE 26			vSpawnPoint = <<188.0060, -3161.6460, 4.7890>> fSpawnHeading = 98.0000	BREAK
				CASE 27			vSpawnPoint = <<192.7240, -3160.6770, 4.7900>> fSpawnHeading = 98.0000	BREAK
				CASE 28			vSpawnPoint = <<189.8190, -3160.5979, 4.7850>> fSpawnHeading = 98.0000	BREAK
				CASE 29			vSpawnPoint = <<191.1210, -3176.4580, 4.7880>> fSpawnHeading = 98.0000	BREAK
				CASE 30			vSpawnPoint = <<193.8793, -3176.4028, 5.7144>> fSpawnHeading = 98.0000	BREAK
				CASE 31			vSpawnPoint = <<188.0687, -3176.4788, 5.7126>> fSpawnHeading = 98.0000	BREAK
				CASE 32			vSpawnPoint = <<186.6745, -3172.3398, 5.7123>> fSpawnHeading = 98.0000	BREAK
				CASE 33			vSpawnPoint = <<186.7000, -3169.8357, 5.7124>> fSpawnHeading = 98.0000	BREAK
				CASE 34			vSpawnPoint = <<186.7144, -3167.7566, 5.7124>> fSpawnHeading = 98.0000	BREAK
				CASE 35			vSpawnPoint = <<186.9747, -3165.3086, 5.7133>> fSpawnHeading = 98.0000	BREAK
				CASE 36			vSpawnPoint = <<187.0014, -3162.9956, 5.7134>> fSpawnHeading = 98.0000	BREAK
				CASE 37			vSpawnPoint = <<186.7201, -3175.2981, 5.7125>> fSpawnHeading = 98.0000	BREAK
				CASE 38			vSpawnPoint = <<185.2069, -3173.9180, 5.7047>> fSpawnHeading = 98.0000	BREAK
				CASE 39			vSpawnPoint = <<185.2275, -3171.3127, 5.7067>> fSpawnHeading = 98.0000	BREAK
				CASE 40			vSpawnPoint = <<185.2210, -3168.8896, 5.7066>> fSpawnHeading = 98.0000	BREAK
				CASE 41			vSpawnPoint = <<185.3906, -3166.4446, 5.7076>> fSpawnHeading = 98.0000	BREAK
				CASE 42			vSpawnPoint = <<185.3820, -3164.2732, 5.7075>> fSpawnHeading = 98.0000	BREAK
				CASE 43			vSpawnPoint = <<185.4295, -3161.6375, 5.7078>> fSpawnHeading = 98.0000	BREAK
				CASE 44			vSpawnPoint = <<186.6956, -3160.2852, 5.7123>> fSpawnHeading = 98.0000	BREAK
				CASE 45			vSpawnPoint = <<188.1816, -3158.9644, 5.7125>> fSpawnHeading = 98.0000	BREAK
				CASE 46			vSpawnPoint = <<185.3622, -3158.9304, 5.7073>> fSpawnHeading = 98.0000	BREAK
				CASE 47			vSpawnPoint = <<191.1873, -3159.1758, 5.7120>> fSpawnHeading = 98.0000	BREAK
				CASE 48			vSpawnPoint = <<192.7551, -3157.8687, 5.7144>> fSpawnHeading = 98.0000	BREAK
				CASE 49			vSpawnPoint = <<189.9160, -3157.6252, 5.7087>> fSpawnHeading = 98.0000	BREAK
				CASE 50			vSpawnPoint = <<186.6961, -3157.3357, 5.7122>> fSpawnHeading = 98.0000	BREAK
				CASE 51			vSpawnPoint = <<185.2677, -3176.5825, 5.7135>> fSpawnHeading = 98.0000	BREAK
				CASE 52			vSpawnPoint = <<189.5842, -3178.0317, 5.7093>> fSpawnHeading = 98.0000	BREAK
				CASE 53			vSpawnPoint = <<192.5045, -3178.0110, 5.7144>> fSpawnHeading = 98.0000	BREAK
				CASE 54			vSpawnPoint = <<186.8581, -3178.1335, 5.7130>> fSpawnHeading = 98.0000	BREAK
				CASE 55			vSpawnPoint = <<183.5678, -3167.4290, 5.6911>> fSpawnHeading = 98.0000	BREAK
				CASE 56			vSpawnPoint = <<183.5548, -3170.1135, 5.6845>> fSpawnHeading = 98.0000	BREAK
				CASE 57			vSpawnPoint = <<183.4641, -3172.5698, 5.6766>> fSpawnHeading = 98.0000	BREAK
				CASE 58			vSpawnPoint = <<183.4816, -3175.2478, 5.6877>> fSpawnHeading = 98.0000	BREAK
				CASE 59			vSpawnPoint = <<183.4607, -3178.1633, 5.6968>> fSpawnHeading = 98.0000	BREAK
				CASE 60			vSpawnPoint = <<183.5528, -3165.3750, 5.6931>> fSpawnHeading = 98.0000	BREAK
				CASE 61			vSpawnPoint = <<183.6509, -3162.7649, 5.6993>> fSpawnHeading = 98.0000	BREAK
				CASE 62			vSpawnPoint = <<183.7454, -3160.2668, 5.7024>> fSpawnHeading = 98.0000	BREAK
				CASE 63			vSpawnPoint = <<183.8437, -3157.6868, 5.7018>> fSpawnHeading = 98.0000	BREAK
				CASE 64			vSpawnPoint = <<191.1551, -3179.6917, 5.7120>> fSpawnHeading = 98.0000	BREAK
				CASE 65			vSpawnPoint = <<188.4324, -3179.5085, 5.7118>> fSpawnHeading = 98.0000	BREAK
				CASE 66			vSpawnPoint = <<185.4149, -3179.5415, 5.7147>> fSpawnHeading = 98.0000	BREAK
				CASE 67			vSpawnPoint = <<191.4506, -3155.9541, 5.7129>> fSpawnHeading = 98.0000	BREAK
				CASE 68			vSpawnPoint = <<188.6065, -3156.0679, 5.7116>> fSpawnHeading = 98.0000	BREAK
				CASE 69			vSpawnPoint = <<185.7193, -3155.9905, 5.7089>> fSpawnHeading = 98.0000	BREAK
				CASE 70			vSpawnPoint = <<187.0403, -3180.9421, 5.7137>> fSpawnHeading = 98.0000	BREAK
				CASE 71			vSpawnPoint = <<189.8537, -3181.0417, 5.7086>> fSpawnHeading = 98.0000	BREAK
				DEFAULT			RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<371.9220, 255.0410, 101.9620>> fSpawnHeading = 342.9970	BREAK
				CASE 1			vSpawnPoint = <<373.7530, 254.3290, 101.9600>> fSpawnHeading = 342.9970	BREAK
				CASE 2			vSpawnPoint = <<369.8440, 255.8540, 101.9710>> fSpawnHeading = 342.9970	BREAK
				CASE 3			vSpawnPoint = <<371.3730, 257.0440, 101.8930>> fSpawnHeading = 342.9970	BREAK
				CASE 4			vSpawnPoint = <<373.4130, 256.2520, 101.8950>> fSpawnHeading = 342.9970	BREAK
				CASE 5			vSpawnPoint = <<375.6870, 255.4060, 101.9580>> fSpawnHeading = 342.9970	BREAK
				CASE 6			vSpawnPoint = <<375.9150, 253.4240, 102.0280>> fSpawnHeading = 342.9970	BREAK
				CASE 7			vSpawnPoint = <<369.0810, 257.8530, 101.9750>> fSpawnHeading = 342.9970	BREAK
				CASE 8			vSpawnPoint = <<372.8200, 257.9730, 101.8580>> fSpawnHeading = 342.9970	BREAK
				CASE 9			vSpawnPoint = <<375.0570, 257.1070, 101.8630>> fSpawnHeading = 342.9970	BREAK
				CASE 10			vSpawnPoint = <<370.5720, 258.8620, 101.8840>> fSpawnHeading = 342.9970	BREAK
				CASE 11			vSpawnPoint = <<377.3180, 256.1720, 101.9060>> fSpawnHeading = 342.9970	BREAK
				CASE 12			vSpawnPoint = <<367.1750, 256.8490, 101.9960>> fSpawnHeading = 342.9970	BREAK
				CASE 13			vSpawnPoint = <<367.9690, 259.7850, 101.9980>> fSpawnHeading = 342.9970	BREAK
				CASE 14			vSpawnPoint = <<378.1260, 254.0700, 102.0340>> fSpawnHeading = 342.9970	BREAK
				CASE 15			vSpawnPoint = <<365.4370, 258.9530, 101.9970>> fSpawnHeading = 342.9970	BREAK
				CASE 16			vSpawnPoint = <<372.3570, 260.3390, 101.9320>> fSpawnHeading = 251.9960	BREAK
				CASE 17			vSpawnPoint = <<374.5190, 259.6080, 101.9360>> fSpawnHeading = 251.9960	BREAK
				CASE 18			vSpawnPoint = <<376.9960, 258.6800, 101.9620>> fSpawnHeading = 251.9960	BREAK
				CASE 19			vSpawnPoint = <<379.1330, 257.8640, 101.9720>> fSpawnHeading = 251.9960	BREAK
				CASE 20			vSpawnPoint = <<369.7530, 261.2880, 101.9560>> fSpawnHeading = 251.9960	BREAK
				CASE 21			vSpawnPoint = <<380.1320, 255.4160, 101.9610>> fSpawnHeading = 251.9960	BREAK
				CASE 22			vSpawnPoint = <<379.3620, 252.5740, 102.0440>> fSpawnHeading = 251.9960	BREAK
				CASE 23			vSpawnPoint = <<381.2500, 258.0290, 101.9990>> fSpawnHeading = 251.9960	BREAK
				CASE 24			vSpawnPoint = <<382.2530, 256.2080, 101.9890>> fSpawnHeading = 251.9960	BREAK
				CASE 25			vSpawnPoint = <<381.3590, 253.3960, 102.0190>> fSpawnHeading = 251.9960	BREAK
				CASE 26			vSpawnPoint = <<383.3060, 254.2200, 101.9780>> fSpawnHeading = 251.9960	BREAK
				CASE 27			vSpawnPoint = <<382.3670, 251.2170, 102.0270>> fSpawnHeading = 251.9960	BREAK
				CASE 28			vSpawnPoint = <<384.2130, 256.9190, 102.0000>> fSpawnHeading = 251.9960	BREAK
				CASE 29			vSpawnPoint = <<366.9330, 262.1490, 102.0270>> fSpawnHeading = 251.9960	BREAK
				CASE 30			vSpawnPoint = <<364.7522, 261.2900, 102.9508>> fSpawnHeading = 246.7960	BREAK
				CASE 31			vSpawnPoint = <<363.5257, 258.0992, 102.9219>> fSpawnHeading = 246.7960	BREAK
				CASE 32			vSpawnPoint = <<365.7774, 263.9834, 102.9312>> fSpawnHeading = 246.7960	BREAK
				CASE 33			vSpawnPoint = <<363.7557, 263.2066, 102.9273>> fSpawnHeading = 246.7960	BREAK
				CASE 34			vSpawnPoint = <<362.7837, 260.2528, 102.9212>> fSpawnHeading = 246.7960	BREAK
				CASE 35			vSpawnPoint = <<361.6629, 262.4578, 102.9234>> fSpawnHeading = 246.7960	BREAK
				CASE 36			vSpawnPoint = <<362.6299, 264.9411, 102.9307>> fSpawnHeading = 246.7960	BREAK
				CASE 37			vSpawnPoint = <<360.7266, 259.7930, 102.9212>> fSpawnHeading = 246.7960	BREAK
				CASE 38			vSpawnPoint = <<360.8707, 264.2966, 102.9274>> fSpawnHeading = 246.7960	BREAK
				CASE 39			vSpawnPoint = <<359.7638, 261.4692, 102.8993>> fSpawnHeading = 246.7960	BREAK
				CASE 40			vSpawnPoint = <<358.9537, 263.2605, 102.8826>> fSpawnHeading = 246.7960	BREAK
				CASE 41			vSpawnPoint = <<357.9525, 260.5070, 102.8649>> fSpawnHeading = 246.7960	BREAK
				CASE 42			vSpawnPoint = <<359.9302, 265.8490, 102.9305>> fSpawnHeading = 246.7960	BREAK
				CASE 43			vSpawnPoint = <<384.2049, 252.2829, 102.9217>> fSpawnHeading = 246.7960	BREAK
				CASE 44			vSpawnPoint = <<385.1142, 255.1486, 102.9140>> fSpawnHeading = 246.7960	BREAK
				CASE 45			vSpawnPoint = <<386.3219, 253.0679, 102.9261>> fSpawnHeading = 246.7960	BREAK
				CASE 46			vSpawnPoint = <<387.3038, 255.7957, 102.9262>> fSpawnHeading = 246.7960	BREAK
				CASE 47			vSpawnPoint = <<385.4807, 250.0388, 102.9502>> fSpawnHeading = 246.7960	BREAK
				CASE 48			vSpawnPoint = <<387.8292, 251.3131, 102.9502>> fSpawnHeading = 246.7960	BREAK
				CASE 49			vSpawnPoint = <<388.6382, 253.8541, 102.9502>> fSpawnHeading = 246.7960	BREAK
				CASE 50			vSpawnPoint = <<389.5830, 256.8214, 102.9502>> fSpawnHeading = 246.7960	BREAK
				CASE 51			vSpawnPoint = <<391.1165, 254.6901, 102.9386>> fSpawnHeading = 246.7960	BREAK
				CASE 52			vSpawnPoint = <<390.0806, 251.6365, 102.9502>> fSpawnHeading = 246.7960	BREAK
				CASE 53			vSpawnPoint = <<392.1485, 257.6381, 102.9249>> fSpawnHeading = 246.7960	BREAK
				CASE 54			vSpawnPoint = <<390.6778, 259.8380, 102.9502>> fSpawnHeading = 246.7960	BREAK
				CASE 55			vSpawnPoint = <<391.6333, 262.8454, 102.9453>> fSpawnHeading = 246.7960	BREAK
				CASE 56			vSpawnPoint = <<392.8972, 260.5841, 102.9208>> fSpawnHeading = 246.7960	BREAK
				CASE 57			vSpawnPoint = <<392.0998, 252.7865, 102.9165>> fSpawnHeading = 246.7960	BREAK
				CASE 58			vSpawnPoint = <<391.2812, 250.2019, 102.9268>> fSpawnHeading = 246.7960	BREAK
				CASE 59			vSpawnPoint = <<393.0858, 255.8185, 102.9038>> fSpawnHeading = 246.7960	BREAK
				CASE 60			vSpawnPoint = <<394.3023, 258.9434, 102.8973>> fSpawnHeading = 246.7960	BREAK
				CASE 61			vSpawnPoint = <<393.9210, 263.2654, 102.9502>> fSpawnHeading = 246.7960	BREAK
				CASE 62			vSpawnPoint = <<392.6579, 265.7205, 102.9499>> fSpawnHeading = 246.7960	BREAK
				CASE 63			vSpawnPoint = <<394.9528, 266.0154, 102.9495>> fSpawnHeading = 246.7960	BREAK
				CASE 64			vSpawnPoint = <<393.7692, 268.3819, 102.9488>> fSpawnHeading = 246.7960	BREAK
				CASE 65			vSpawnPoint = <<381.8169, 261.1124, 102.9335>> fSpawnHeading = 246.7960	BREAK
				CASE 66			vSpawnPoint = <<382.7659, 263.6062, 102.9323>> fSpawnHeading = 246.7960	BREAK
				CASE 67			vSpawnPoint = <<383.7118, 266.1429, 102.9314>> fSpawnHeading = 246.7960	BREAK
				CASE 68			vSpawnPoint = <<383.7737, 261.6729, 102.9332>> fSpawnHeading = 246.7960	BREAK
				CASE 69			vSpawnPoint = <<384.6857, 264.2654, 102.9320>> fSpawnHeading = 246.7960	BREAK
				CASE 70			vSpawnPoint = <<385.6606, 262.3497, 102.9329>> fSpawnHeading = 246.7960	BREAK
				CASE 71			vSpawnPoint = <<384.6693, 259.9063, 102.9336>> fSpawnHeading = 246.7960	BREAK
				DEFAULT 
					RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<-1286.6160, -650.1080, 25.5850>> fSpawnHeading = 38.2000	BREAK
				CASE 1			vSpawnPoint = <<-1284.7820, -648.6220, 25.6170>> fSpawnHeading = 38.2000	BREAK
				CASE 2			vSpawnPoint = <<-1288.7150, -651.7580, 25.5830>> fSpawnHeading = 38.2000	BREAK
				CASE 3			vSpawnPoint = <<-1288.8010, -649.7250, 25.5680>> fSpawnHeading = 38.2000	BREAK
				CASE 4			vSpawnPoint = <<-1286.6370, -647.9760, 25.6020>> fSpawnHeading = 38.2000	BREAK
				CASE 5			vSpawnPoint = <<-1288.7900, -647.7290, 25.5820>> fSpawnHeading = 38.2000	BREAK
				CASE 6			vSpawnPoint = <<-1286.6530, -645.9970, 25.6180>> fSpawnHeading = 38.2000	BREAK
				CASE 7			vSpawnPoint = <<-1291.0260, -649.2960, 25.5510>> fSpawnHeading = 38.2000	BREAK
				CASE 8			vSpawnPoint = <<-1284.4630, -646.7500, 25.6350>> fSpawnHeading = 38.2000	BREAK
				CASE 9			vSpawnPoint = <<-1290.7480, -651.5370, 25.5390>> fSpawnHeading = 38.2000	BREAK
				CASE 10			vSpawnPoint = <<-1282.4550, -646.9530, 25.6550>> fSpawnHeading = 38.2000	BREAK
				CASE 11			vSpawnPoint = <<-1284.3550, -644.6400, 25.6540>> fSpawnHeading = 38.2000	BREAK
				CASE 12			vSpawnPoint = <<-1291.1340, -653.6470, 25.5210>> fSpawnHeading = 38.2000	BREAK
				CASE 13			vSpawnPoint = <<-1293.0280, -651.3320, 25.5200>> fSpawnHeading = 38.2000	BREAK
				CASE 14			vSpawnPoint = <<-1293.0200, -653.3870, 25.5110>> fSpawnHeading = 38.2000	BREAK
				CASE 15			vSpawnPoint = <<-1293.2791, -655.5430, 25.5130>> fSpawnHeading = 38.2000	BREAK
				CASE 16			vSpawnPoint = <<-1295.1860, -652.9380, 25.5130>> fSpawnHeading = 38.2000	BREAK
				CASE 17			vSpawnPoint = <<-1282.0100, -645.1110, 25.6770>> fSpawnHeading = 38.2000	BREAK
				CASE 18			vSpawnPoint = <<-1280.1410, -645.3120, 25.6970>> fSpawnHeading = 38.2000	BREAK
				CASE 19			vSpawnPoint = <<-1281.9430, -642.9380, 25.6980>> fSpawnHeading = 38.2000	BREAK
				CASE 20			vSpawnPoint = <<-1295.2540, -655.4190, 25.5150>> fSpawnHeading = 38.2000	BREAK
				CASE 21			vSpawnPoint = <<-1295.5959, -657.6760, 25.5170>> fSpawnHeading = 38.2000	BREAK
				CASE 22			vSpawnPoint = <<-1297.5090, -654.9490, 25.5170>> fSpawnHeading = 38.2000	BREAK
				CASE 23			vSpawnPoint = <<-1279.6720, -643.2460, 25.7220>> fSpawnHeading = 38.2000	BREAK
				CASE 24			vSpawnPoint = <<-1277.6250, -643.4710, 25.7440>> fSpawnHeading = 38.2000	BREAK
				CASE 25			vSpawnPoint = <<-1279.4670, -641.1150, 25.7440>> fSpawnHeading = 38.2000	BREAK
				CASE 26			vSpawnPoint = <<-1297.5389, -657.4750, 25.5180>> fSpawnHeading = 38.2000	BREAK
				CASE 27			vSpawnPoint = <<-1297.8719, -659.6880, 25.5180>> fSpawnHeading = 38.2000	BREAK
				CASE 28			vSpawnPoint = <<-1299.8700, -656.8230, 25.5090>> fSpawnHeading = 38.2000	BREAK
				CASE 29			vSpawnPoint = <<-1293.4510, -657.4920, 25.5150>> fSpawnHeading = 38.2000	BREAK
				CASE 30			vSpawnPoint = <<-1300.2407, -659.5067, 26.4337>> fSpawnHeading = 38.2000	BREAK
				CASE 31			vSpawnPoint = <<-1300.1703, -661.5535, 26.4388>> fSpawnHeading = 38.2000	BREAK
				CASE 32			vSpawnPoint = <<-1302.5159, -658.9218, 26.3862>> fSpawnHeading = 38.2000	BREAK
				CASE 33			vSpawnPoint = <<-1302.7229, -661.2617, 26.3981>> fSpawnHeading = 38.2000	BREAK
				CASE 34			vSpawnPoint = <<-1303.1089, -663.7624, 26.4420>> fSpawnHeading = 38.2000	BREAK
				CASE 35			vSpawnPoint = <<-1305.2202, -661.1111, 26.3862>> fSpawnHeading = 38.2000	BREAK
				CASE 36			vSpawnPoint = <<-1305.6224, -663.3047, 26.3937>> fSpawnHeading = 38.2000	BREAK
				CASE 37			vSpawnPoint = <<-1276.7944, -641.3406, 26.7011>> fSpawnHeading = 38.2000	BREAK
				CASE 38			vSpawnPoint = <<-1274.8033, -642.0787, 26.7636>> fSpawnHeading = 38.2000	BREAK
				CASE 39			vSpawnPoint = <<-1276.7650, -639.1870, 26.7234>> fSpawnHeading = 38.2000	BREAK
				CASE 40			vSpawnPoint = <<-1274.5568, -639.6791, 26.7475>> fSpawnHeading = 38.2000	BREAK
				CASE 41			vSpawnPoint = <<-1272.4015, -640.3511, 26.7684>> fSpawnHeading = 38.2000	BREAK
				CASE 42			vSpawnPoint = <<-1274.5635, -637.6009, 26.7688>> fSpawnHeading = 38.2000	BREAK
				CASE 43			vSpawnPoint = <<-1272.1735, -637.9301, 26.7940>> fSpawnHeading = 38.2000	BREAK
				CASE 44			vSpawnPoint = <<-1269.5574, -638.7128, 26.8171>> fSpawnHeading = 38.2000	BREAK
				CASE 45			vSpawnPoint = <<-1271.8890, -635.5920, 26.8368>> fSpawnHeading = 38.2000	BREAK
				CASE 46			vSpawnPoint = <<-1269.2079, -635.7166, 26.8491>> fSpawnHeading = 38.2000	BREAK
				CASE 47			vSpawnPoint = <<-1267.2395, -636.8250, 26.8617>> fSpawnHeading = 38.2000	BREAK
				CASE 48			vSpawnPoint = <<-1267.0004, -634.1705, 26.8845>> fSpawnHeading = 38.2000	BREAK
				CASE 49			vSpawnPoint = <<-1264.7795, -635.0520, 26.8986>> fSpawnHeading = 38.2000	BREAK
				CASE 50			vSpawnPoint = <<-1264.6138, -632.7147, 26.9172>> fSpawnHeading = 38.2000	BREAK
				CASE 51			vSpawnPoint = <<-1269.9369, -640.9653, 26.7826>> fSpawnHeading = 38.2000	BREAK
				CASE 52			vSpawnPoint = <<-1272.6140, -642.8697, 26.7310>> fSpawnHeading = 38.2000	BREAK
				CASE 53			vSpawnPoint = <<-1267.1530, -639.3497, 26.8289>> fSpawnHeading = 38.2000	BREAK
				CASE 54			vSpawnPoint = <<-1267.5966, -641.7515, 26.8080>> fSpawnHeading = 38.2000	BREAK
				CASE 55			vSpawnPoint = <<-1270.1497, -643.7238, 26.7732>> fSpawnHeading = 38.2000	BREAK
				CASE 56			vSpawnPoint = <<-1272.8438, -645.4819, 26.7337>> fSpawnHeading = 38.2000	BREAK
				CASE 57			vSpawnPoint = <<-1267.9159, -644.1287, 26.8214>> fSpawnHeading = 38.2000	BREAK
				CASE 58			vSpawnPoint = <<-1270.7297, -646.1971, 26.7822>> fSpawnHeading = 38.2000	BREAK
				CASE 59			vSpawnPoint = <<-1265.0590, -641.6689, 26.8364>> fSpawnHeading = 38.2000	BREAK
				CASE 60			vSpawnPoint = <<-1268.1035, -646.8002, 26.8400>> fSpawnHeading = 38.2000	BREAK
				CASE 61			vSpawnPoint = <<-1265.0146, -644.2277, 26.8419>> fSpawnHeading = 38.2000	BREAK
				CASE 62			vSpawnPoint = <<-1300.6458, -664.0687, 26.4192>> fSpawnHeading = 38.2000	BREAK
				CASE 63			vSpawnPoint = <<-1300.9498, -666.8594, 26.2876>> fSpawnHeading = 38.2000	BREAK
				CASE 64			vSpawnPoint = <<-1298.0310, -664.4432, 26.2817>> fSpawnHeading = 38.2000	BREAK
				CASE 65			vSpawnPoint = <<-1262.3107, -633.4252, 26.9337>> fSpawnHeading = 38.2000	BREAK
				CASE 66			vSpawnPoint = <<-1262.1459, -630.8963, 26.9515>> fSpawnHeading = 38.2000	BREAK
				CASE 67			vSpawnPoint = <<-1260.3358, -631.7104, 26.9615>> fSpawnHeading = 38.2000	BREAK
				CASE 68			vSpawnPoint = <<-1260.1387, -628.8714, 26.9813>> fSpawnHeading = 38.2000	BREAK
				CASE 69			vSpawnPoint = <<-1257.5676, -629.7444, 26.9971>> fSpawnHeading = 38.2000	BREAK
				CASE 70			vSpawnPoint = <<-1266.1213, -646.6364, 26.8453>> fSpawnHeading = 38.2000	BREAK
				CASE 71			vSpawnPoint = <<-1270.3562, -648.4981, 26.8089>> fSpawnHeading = 38.2000	BREAK
				DEFAULT 		RETURN FALSE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			SWITCH iSpawnPoint
				CASE 0			vSpawnPoint = <<-1171.8540, -1152.7050, 4.6580>> fSpawnHeading = 284.3990	BREAK
				CASE 1			vSpawnPoint = <<-1172.4480, -1150.0890, 4.6640>> fSpawnHeading = 284.3990	BREAK
				CASE 2			vSpawnPoint = <<-1170.6910, -1150.8510, 4.6610>> fSpawnHeading = 284.3990	BREAK
				CASE 3			vSpawnPoint = <<-1169.9430, -1153.6700, 4.6550>> fSpawnHeading = 284.3990	BREAK
				CASE 4			vSpawnPoint = <<-1171.1750, -1155.5730, 4.6520>> fSpawnHeading = 284.3990	BREAK
				CASE 5			vSpawnPoint = <<-1171.1970, -1147.9730, 4.6670>> fSpawnHeading = 284.3990	BREAK
				CASE 6			vSpawnPoint = <<-1174.2920, -1148.9430, 4.6670>> fSpawnHeading = 284.3990	BREAK
				CASE 7			vSpawnPoint = <<-1169.4659, -1149.0830, 4.6640>> fSpawnHeading = 284.3990	BREAK
				CASE 8			vSpawnPoint = <<-1168.8260, -1151.7330, 4.6590>> fSpawnHeading = 284.3990	BREAK
				CASE 9			vSpawnPoint = <<-1168.1650, -1154.4010, 4.6530>> fSpawnHeading = 284.3990	BREAK
				CASE 10			vSpawnPoint = <<-1169.2030, -1156.1560, 4.6500>> fSpawnHeading = 284.3990	BREAK
				CASE 11			vSpawnPoint = <<-1173.1100, -1146.9250, 4.6700>> fSpawnHeading = 284.3990	BREAK
				CASE 12			vSpawnPoint = <<-1170.2380, -1145.9919, 4.6710>> fSpawnHeading = 284.3990	BREAK
				CASE 13			vSpawnPoint = <<-1171.9020, -1144.5699, 4.6740>> fSpawnHeading = 284.3990	BREAK
				CASE 14			vSpawnPoint = <<-1175.0190, -1145.3500, 4.6740>> fSpawnHeading = 284.3990	BREAK
				CASE 15			vSpawnPoint = <<-1173.6940, -1143.4270, 4.6770>> fSpawnHeading = 284.3990	BREAK
				CASE 16			vSpawnPoint = <<-1171.1219, -1142.5500, 4.6780>> fSpawnHeading = 284.3990	BREAK
				CASE 17			vSpawnPoint = <<-1170.4730, -1158.6600, 4.6460>> fSpawnHeading = 284.3990	BREAK
				CASE 18			vSpawnPoint = <<-1167.4919, -1157.7180, 4.6460>> fSpawnHeading = 284.3990	BREAK
				CASE 19			vSpawnPoint = <<-1168.6210, -1159.4750, 4.6430>> fSpawnHeading = 284.3990	BREAK
				CASE 20			vSpawnPoint = <<-1175.5540, -1142.5450, 4.6800>> fSpawnHeading = 284.3990	BREAK
				CASE 21			vSpawnPoint = <<-1172.8910, -1141.6470, 4.6810>> fSpawnHeading = 284.3990	BREAK
				CASE 22			vSpawnPoint = <<-1167.2271, -1152.7200, 4.6560>> fSpawnHeading = 284.3990	BREAK
				CASE 23			vSpawnPoint = <<-1167.7640, -1150.1660, 4.6610>> fSpawnHeading = 284.3990	BREAK
				CASE 24			vSpawnPoint = <<-1168.5940, -1147.0000, 4.6680>> fSpawnHeading = 284.3990	BREAK
				CASE 25			vSpawnPoint = <<-1169.6281, -1143.5360, 4.6750>> fSpawnHeading = 284.3990	BREAK
				CASE 26			vSpawnPoint = <<-1170.5179, -1140.2450, 4.6820>> fSpawnHeading = 284.3990	BREAK
				CASE 27			vSpawnPoint = <<-1172.3850, -1159.3180, 4.6460>> fSpawnHeading = 284.3990	BREAK
				CASE 28			vSpawnPoint = <<-1170.9640, -1161.0350, 4.6410>> fSpawnHeading = 284.3990	BREAK
				CASE 29			vSpawnPoint = <<-1169.3870, -1162.0050, 4.6390>> fSpawnHeading = 284.3990	BREAK
				CASE 30			vSpawnPoint = <<-1167.8475, -1144.9836, 5.5953>> fSpawnHeading = 284.3990	BREAK
				CASE 31			vSpawnPoint = <<-1169.0698, -1141.7394, 5.6023>> fSpawnHeading = 284.3990	BREAK
				CASE 32			vSpawnPoint = <<-1166.7351, -1148.0515, 5.5887>> fSpawnHeading = 284.3990	BREAK
				CASE 33			vSpawnPoint = <<-1165.7999, -1146.0132, 5.5922>> fSpawnHeading = 284.3990	BREAK
				CASE 34			vSpawnPoint = <<-1166.7985, -1142.6266, 5.5993>> fSpawnHeading = 284.3990	BREAK
				CASE 35			vSpawnPoint = <<-1168.1531, -1139.5244, 5.6060>> fSpawnHeading = 284.3990	BREAK
				CASE 36			vSpawnPoint = <<-1170.1975, -1137.9517, 5.6102>> fSpawnHeading = 284.3990	BREAK
				CASE 37			vSpawnPoint = <<-1172.4929, -1138.8876, 5.6098>> fSpawnHeading = 284.3990	BREAK
				CASE 38			vSpawnPoint = <<-1171.7211, -1136.6603, 5.6136>> fSpawnHeading = 284.3990	BREAK
				CASE 39			vSpawnPoint = <<-1174.2875, -1137.7114, 5.6131>> fSpawnHeading = 284.3990	BREAK
				CASE 40			vSpawnPoint = <<-1169.4636, -1135.8461, 5.6137>> fSpawnHeading = 284.3990	BREAK
				CASE 41			vSpawnPoint = <<-1171.3213, -1134.3234, 5.6178>> fSpawnHeading = 284.3990	BREAK
				CASE 42			vSpawnPoint = <<-1173.4508, -1135.4760, 5.6170>> fSpawnHeading = 284.3990	BREAK
				CASE 43			vSpawnPoint = <<-1175.5959, -1134.1724, 5.6208>> fSpawnHeading = 284.3990	BREAK
				CASE 44			vSpawnPoint = <<-1173.1399, -1133.2637, 5.6211>> fSpawnHeading = 284.3990	BREAK
				CASE 45			vSpawnPoint = <<-1166.4735, -1160.9108, 5.5634>> fSpawnHeading = 284.3990	BREAK
				CASE 46			vSpawnPoint = <<-1167.3586, -1163.0388, 5.5597>> fSpawnHeading = 284.3990	BREAK
				CASE 47			vSpawnPoint = <<-1170.0389, -1163.7709, 5.5597>> fSpawnHeading = 284.3990	BREAK
				CASE 48			vSpawnPoint = <<-1168.4268, -1165.0242, 5.5564>> fSpawnHeading = 284.3990	BREAK
				CASE 49			vSpawnPoint = <<-1169.5833, -1166.4575, 5.5541>> fSpawnHeading = 284.3990	BREAK
				CASE 50			vSpawnPoint = <<-1166.7275, -1165.7677, 5.5540>> fSpawnHeading = 284.3990	BREAK
				CASE 51			vSpawnPoint = <<-1165.3777, -1163.7073, 5.5573>> fSpawnHeading = 284.3990	BREAK
				CASE 52			vSpawnPoint = <<-1166.2231, -1151.0254, 5.5826>> fSpawnHeading = 284.3990	BREAK
				CASE 53			vSpawnPoint = <<-1164.2415, -1151.4209, 5.5808>> fSpawnHeading = 284.3990	BREAK
				CASE 54			vSpawnPoint = <<-1174.5646, -1132.2302, 5.6240>> fSpawnHeading = 284.3990	BREAK
				CASE 55			vSpawnPoint = <<-1176.7194, -1131.2717, 5.6249>> fSpawnHeading = 284.3990	BREAK
				CASE 56			vSpawnPoint = <<-1174.1780, -1130.2089, 5.6248>> fSpawnHeading = 284.3990	BREAK
				CASE 57			vSpawnPoint = <<-1170.5303, -1132.2106, 5.6214>> fSpawnHeading = 284.3990	BREAK
				CASE 58			vSpawnPoint = <<-1167.8640, -1167.7006, 5.5507>> fSpawnHeading = 284.3990	BREAK
				CASE 59			vSpawnPoint = <<-1164.8390, -1166.6198, 5.5513>> fSpawnHeading = 284.3990	BREAK
				CASE 60			vSpawnPoint = <<-1166.0503, -1168.5085, 5.5482>> fSpawnHeading = 284.3990	BREAK
				CASE 61			vSpawnPoint = <<-1169.0627, -1169.1411, 5.5484>> fSpawnHeading = 284.3990	BREAK
				CASE 62			vSpawnPoint = <<-1167.3092, -1170.4017, 5.5477>> fSpawnHeading = 284.3990	BREAK
				CASE 63			vSpawnPoint = <<-1164.2832, -1169.5660, 5.5477>> fSpawnHeading = 284.3990	BREAK
				CASE 64			vSpawnPoint = <<-1165.1704, -1171.4906, 5.5477>> fSpawnHeading = 284.3990	BREAK
				CASE 65			vSpawnPoint = <<-1163.3448, -1172.7140, 5.5477>> fSpawnHeading = 284.3990	BREAK
				CASE 66			vSpawnPoint = <<-1161.6996, -1170.8163, 5.5477>> fSpawnHeading = 284.3990	BREAK
				CASE 67			vSpawnPoint = <<-1165.0714, -1174.5731, 5.5477>> fSpawnHeading = 284.3990	BREAK
				CASE 68			vSpawnPoint = <<-1167.0070, -1172.3434, 5.5477>> fSpawnHeading = 284.3990	BREAK
				CASE 69			vSpawnPoint = <<-1168.9718, -1171.1958, 5.5477>> fSpawnHeading = 284.3990	BREAK
				CASE 70			vSpawnPoint = <<-1162.8168, -1175.1613, 5.5477>> fSpawnHeading = 284.3990	BREAK
				CASE 71			vSpawnPoint = <<-1162.5135, -1177.5422, 5.5477>> fSpawnHeading = 284.3990	BREAK
				DEFAULT			RETURN FALSE
			ENDSWITCH
		BREAK
		
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	// NOTE : default cases of the switch statements return FALSE
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_NIGHTCLUB_EXTERIOR_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle)
	IF bSpawnInVehicle
		RETURN GET_NIGHTCLUB_EXTERIOR_PERSONAL_VEHICLE_SPAWN_POINT(eSimpleInteriorID, iSpawnPoint, vSpawnPoint, fSpawnHeading)
	ELSE
		RETURN GET_NIGHTCLUB_EXTERIOR_SPAWN_POINT_ON_FOOT(eSimpleInteriorID, iSpawnPoint, vSpawnPoint, fSpawnHeading)
	ENDIF
ENDFUNC

FUNC BOOL GET_BUSINESS_HUB_EXTERIOR_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle)
	
	IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
	AND g_SimpleInteriorData.iExitUsed != ciNIGHTCLUB_EXIT_GARAGE
	AND NOT bSpawnInVehicle
	OR g_SimpleInteriorData.iExitUsed = ciBUSINESS_HUB_EXIT_FORCE_CLUB_FRONT_DOOR
	AND NOT bSpawnInVehicle
		//If the floor number is 0 then we are exiting from the nightclub entrance
		RETURN GET_NIGHTCLUB_EXTERIOR_SPAWN_POINT(eSimpleInteriorID, iSpawnPoint, vSpawnPoint, fSpawnHeading, bSpawnInVehicle)
	ELSE
		IF bSpawnInVehicle
			IF GET_BUSINESS_HUB_EXTERIOR_PERSONAL_VEHICLE_SPAWN_POINT(eSimpleInteriorID, iSpawnPoint, vSpawnPoint, fSpawnHeading)
				CDEBUG1LN(DEBUG_SPAWNING, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_EXTERIOR_SPAWN_POINT - Spawning in personal vehicle at position ", vSpawnPoint, " heading = ", fSpawnHeading)
				RETURN TRUE
			ENDIF
		ELSE
			IF GET_BUSINESS_HUB_EXTERIOR_SPAWN_POINT_ON_FOOT(eSimpleInteriorID, iSpawnPoint, vSpawnPoint, fSpawnHeading)
				CDEBUG1LN(DEBUG_SPAWNING, "[SIMPLE_INTERIOR][BUSINESS_HUB] GET_BUSINESS_HUB_EXTERIOR_SPAWN_POINT - Spawning on foot at position ", vSpawnPoint, " heading = ", fSpawnHeading)
				RETURN TRUE
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_BUSINESS_HUB_ASYNC_SPAWN_GRID_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_BUSINESS_HUB_ASYNC_SPAWN_GRID_INSIDE_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

FUNC VECTOR GET_BUSINESS_HUB_TELEPORT_IN_SVM_COORD(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	IF GET_INTERIOR_FLOOR_INDEX() = 0
		RETURN <<-1627.5669, -2997.7090, -79.1438>>
	ENDIF
	
	RETURN <<-1507.7212, -3011.7004, -80.2419>>
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_BUSINESS_HUB(SIMPLE_INTERIORS eSimpleInteriorID, INT iExitId)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF GET_INTERIOR_FLOOR_INDEX() = ciBUSINESS_HUB_FLOOR_NIGHTCLUB
		IF iExitId = ciNIGHTCLUB_EXIT_GARAGE_ELEVATOR
		OR iExitId = ciNIGHTCLUB_EXIT_STORAGE_ELEVATOR
		OR iExitId = ciNIGHTCLUB_EXIT_VEHICLE_ELEVATOR
			RETURN FALSE
		ENDIF
	ELSE
		IF iExitId = ciBUSINESS_HUB_EXIT_GARAGE_ELEVATOR
		OR iExitId = ciBUSINESS_HUB_EXIT_STORAGE_ELEVATOR
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_BOSS)
	OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_OWNER)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BLIP_SPRITE GET_NIGHTCLUB_BLIP_SPRITE(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN RADAR_TRACE_BAT_CLUB_PROPERTY
ENDFUNC

FUNC VECTOR GET_NIGHTCLUB_BLIP_COORDS(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA				RETURN <<756.989, -1332.463, 26.2802>>
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW			RETURN <<345.2846, -977.7734, 29.4634>>
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE	RETURN <<-120.798, -1260.488, 28.3088>>
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD			RETURN <<5.667, 221.309, 106.7566>>
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS			RETURN <<871.312, -2099.551, 29.4768>>
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE			RETURN <<-676.6141, -2458.2104, 12.9444>>
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND			RETURN <<195.4160, -3167.3811, 4.7903>>
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD		RETURN <<371.0099, 252.2451, 103.0081>>
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING		RETURN <<-1285.0198, -652.3701, 25.6332>>
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS		RETURN <<-1174.5742, -1153.4714, 4.6582>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC INT GET_BUSINESS_HUB_MAX_BLIPS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN MAX_BUSINESS_HUB_BLIPS
ENDFUNC

FUNC VECTOR GET_BUSINESS_HUB_BLIP_COORDS(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	IF iBlipIndex = SIMPLE_INTERIOR_PRIMARY_BLIP
		SWITCH eSimpleInteriorID
			CASE SIMPLE_INTERIOR_HUB_LA_MESA				RETURN <<756.989, -1332.463, 26.2802>>
			CASE SIMPLE_INTERIOR_HUB_MISSION_ROW			RETURN <<345.2846, -977.7734, 29.4634>>
			CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE	RETURN <<-120.798, -1260.488, 28.3088>>
			CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD			RETURN <<5.667, 221.309, 106.7566>>
			CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS			RETURN <<871.312, -2099.551, 29.4768>>
			CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE			RETURN <<-676.6141, -2458.2104, 12.9444>>
			CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND			RETURN <<195.4160, -3167.3811, 4.7903>>
			CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD		RETURN <<371.0099, 252.2451, 103.0081>>
			CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING		RETURN <<-1285.0198, -652.3701, 25.6332>>
			CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS		RETURN <<-1174.5742, -1153.4714, 4.6582>>
		ENDSWITCH
	ELSE
		SWITCH eSimpleInteriorID
			CASE SIMPLE_INTERIOR_HUB_LA_MESA				RETURN <<733.9667, -1291.1207, 25.2851>>
			CASE SIMPLE_INTERIOR_HUB_MISSION_ROW			RETURN <<333.2379, -996.6490, 28.2117>>
			CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE	RETURN <<-164.3258, -1292.6255, 30.3191>>
			CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD			RETURN <<-21.8431, 219.4443, 105.7201>>
			CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS			RETURN <<908.8449, -2097.3716, 29.5575>>
			CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE			RETURN <<-670.8386, -2392.5562, 12.9445>>
			CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND			RETURN <<227.7781, -3136.8979, 4.7903>>
			CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD		RETURN <<377.8120, 228.4137, 102.0406>>
			CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING		RETURN <<-1237.5542, -693.9150, 22.7756>>
			CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS		RETURN <<-1173.5220, -1173.8550, 4.6236>>
		ENDSWITCH
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL SHOULD_BUSINESS_HUB_BE_BLIPPED_THIS_FRAME(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	IF iBlipIndex = SIMPLE_INTERIOR_PRIMARY_BLIP
		IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_printSMLIntNoBilpReason")
				PRINTLN("[SIMPLE_INTERIOR] SHOULD_BUSINESS_HUB_BE_BLIPPED_THIS_FRAME - No - IS_PLAYER_ON_ANY_FM_JOB")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN() = eSimpleInteriorID
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_printSMLIntNoBilpReason")
				PRINTLN("[SIMPLE_INTERIOR] SHOULD_BUSINESS_HUB_BE_BLIPPED_THIS_FRAME - No - IS_PLAYER_ON_ANY_FM_JOB")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
			IF DOES_PLAYER_OWN_BUSINESS_HUB(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			IF NOT IS_BIT_SET(g_sV2CoronaVars.iPimHideMiscStat, ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_NIGHTCLUB)
			AND NOT NET_CORONA_SHOULD_HIDE_ALL_RIVAL_BUSINESS_BLIPS()
				RETURN TRUE
			ENDIF
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printSMLIntNoBilpReason")
			PRINTLN("[SIMPLE_INTERIOR] SHOULD_BUSINESS_HUB_BE_BLIPPED_THIS_FRAME - No - neither me or my boss owns this proprty: ", eSimpleInteriorID)
		ENDIF
		#ENDIF
		
		IF NOT DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		#IF FEATURE_GEN9_EXCLUSIVE
		AND NOT IS_PLAYER_ON_MP_INTRO()
		#ENDIF
			PLAYER_INDEX playerID
			
			INT i
			REPEAT NUM_NETWORK_PLAYERS i
				playerID = INT_TO_PLAYERINDEX(i)
				
				IF IS_NET_PLAYER_OK(playerID)
				AND playerID != PLAYER_ID()
					IF DOES_PLAYER_OWN_BUSINESS_HUB(playerID, GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
					AND HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerID, MBV_COLLECT_STAFF)
					AND HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerID, MBV_COLLECT_EQUIPMENT)
					AND HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerID, MBV_COLLECT_DJ_CRASH)
					AND DOES_PLAYER_HAVE_ACCESS_TO_NIGHTCLUB_AREA(playerID, RESTRICTED_AREA_NIGHTCLUB)
						IF g_bBlockRivalNightclubBlips
							GAMER_HANDLE sGamerHandle = GET_GAMER_HANDLE_PLAYER(playerID)
							
							IF IS_GAMER_HANDLE_VALID(sGamerHandle)
								IF NETWORK_IS_FRIEND(sGamerHandle)
									RETURN TRUE
								ENDIF
							ENDIF
						ELSE
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ELSE
		IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), GET_BUSINESS_HUB_BLIP_COORDS(eSimpleInteriorID, SIMPLE_INTERIOR_SECONDARY_BLIP)) <= 200
			IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
				RETURN FALSE
			ENDIF
			
			IF GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN() = eSimpleInteriorID
				RETURN FALSE
			ENDIF
			
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
				IF DOES_PLAYER_OWN_BUSINESS_HUB(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
					RETURN TRUE
				ENDIF
			ELSE
				IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BUSINESS_HUB_BLIP_SHORT_RANGE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	IF iBlipIndex = SIMPLE_INTERIOR_PRIMARY_BLIP
		IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_BUSINESS_BATTLE(PLAYER_ID())
		OR (GB_IS_BUSINESS_BATTLES_MISSION(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()))
		AND GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		AND GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID()))
			RETURN TRUE
		ENDIF
		
		IF NOT DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		AND NOT DOES_PLAYER_OWN_BUSINESS_HUB(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			RETURN TRUE
		ENDIF
		
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
		AND DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		AND NOT DOES_PLAYER_OWN_BUSINESS_HUB(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BLIP_SPRITE GET_BUSINESS_HUB_BLIP_SPRITE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF iBlipIndex = SIMPLE_INTERIOR_PRIMARY_BLIP
		RETURN RADAR_TRACE_BAT_CLUB_PROPERTY
	ENDIF
	
	RETURN RADAR_TRACE_GARAGE
ENDFUNC

FUNC INT GET_BUSINESS_HUB_BLIP_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF iBlipIndex = SIMPLE_INTERIOR_PRIMARY_BLIP
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		AND DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
		ENDIF
		
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		AND DOES_PLAYER_OWN_BUSINESS_HUB(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
		ENDIF
		
		IF NOT DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			PLAYER_INDEX playerID
			
			INT iColour
			
			INT i
			REPEAT NUM_NETWORK_PLAYERS i
				playerID = INT_TO_PLAYERINDEX(i)
				
				IF IS_NET_PLAYER_OK(playerID)
				AND playerID != PLAYER_ID()
					IF DOES_PLAYER_OWN_BUSINESS_HUB(playerID, GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
					AND HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerID, MBV_COLLECT_STAFF)
					AND HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerID, MBV_COLLECT_EQUIPMENT)
					AND HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerID, MBV_COLLECT_DJ_CRASH)
					AND DOES_PLAYER_HAVE_ACCESS_TO_NIGHTCLUB_AREA(playerID, RESTRICTED_AREA_NIGHTCLUB)
						GAMER_HANDLE sGamerHandle = GET_GAMER_HANDLE_PLAYER(playerID)
						
						IF IS_GAMER_HANDLE_VALID(sGamerHandle)
							IF NETWORK_IS_FRIEND(sGamerHandle)
								RETURN BLIP_COLOUR_BLUE
							ELSE
								iColour = BLIP_COLOUR_GREY
							ENDIF
						ELSE
							iColour = BLIP_COLOUR_GREY
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF iColour = BLIP_COLOUR_GREY
				RETURN BLIP_COLOUR_GREY
			ENDIF
		ENDIF
	ELSE
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
		ENDIF
		
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
		ENDIF
	ENDIF
	
	RETURN BLIP_COLOUR_WHITE
ENDFUNC

FUNC BLIP_PRIORITY GET_BUSINESS_HUB_BLIP_PRIORITY(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN BLIPPRIORITY_MED_HIGH
ENDFUNC

PROC MAINTAIN_BUSINESS_HUB_BLIP_EXTRA_FUNCTIONALITY(SIMPLE_INTERIORS eSimpleInteriorID, BLIP_INDEX &blipIndex, BLIP_INDEX &OverlayBlipIndex, INT &iBlipType, INT &iBlipNameHash, INT iBlipIndex)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(OverlayBlipIndex)
	UNUSED_PARAMETER(iBlipType)
	UNUSED_PARAMETER(iBlipNameHash)
	UNUSED_PARAMETER(iBlipIndex)
	
	IF DOES_BLIP_EXIST(blipIndex)
		IF NOT DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		AND NOT DOES_PLAYER_OWN_BUSINESS_HUB(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			PLAYER_INDEX playerID
			
			BOOL bNonFriendOwner = FALSE
			
			INT i
			REPEAT NUM_NETWORK_PLAYERS i
				playerID = INT_TO_PLAYERINDEX(i)
				
				IF IS_NET_PLAYER_OK(playerID)
				AND playerID != PLAYER_ID()
					IF DOES_PLAYER_OWN_BUSINESS_HUB(playerID, GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
					AND HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerID, MBV_COLLECT_STAFF)
					AND HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerID, MBV_COLLECT_EQUIPMENT)
					AND HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerID, MBV_COLLECT_DJ_CRASH)
					AND DOES_PLAYER_HAVE_ACCESS_TO_NIGHTCLUB_AREA(playerID, RESTRICTED_AREA_NIGHTCLUB)
						GAMER_HANDLE sGamerHandle = GET_GAMER_HANDLE_PLAYER(playerID)
						
						IF IS_GAMER_HANDLE_VALID(sGamerHandle)
							IF NETWORK_IS_FRIEND(sGamerHandle)
								SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "NIGHTCLUB_F_BLIP")
							ELSE
								bNonFriendOwner = TRUE
							ENDIF
						ELSE
							bNonFriendOwner = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF bNonFriendOwner
				SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "NIGHTCLUB_R_BLIP")
			ENDIF
		ELSE
			SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "BLIP_614")
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_BUSINESS_HUB_MAP_MIDPOINT(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA				RETURN <<733.9667, -1291.1207, 25.2851>>
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW			RETURN <<333.1833, -994.4169, 28.3291>>
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE	RETURN <<-164.3258, -1292.6255, 30.3191>>
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD			RETURN <<-21.8431, 219.4443, 105.7201>>
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS			RETURN <<908.8449, -2097.3716, 29.5575>>
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE			RETURN <<-670.8386, -2392.5562, 12.9445>>
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND			RETURN <<227.7781, -3136.8979, 4.7903>>
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD		RETURN <<371.0099, 252.2451, 103.0081>>
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING		RETURN <<-1237.5542, -693.9150, 22.7756>>
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS		RETURN <<-1173.5220, -1173.8550, 4.6236>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC STRING GET_SCENARIO_GROUP_FOR_BUSINESS_HUB(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA				RETURN "CLUB_LA_MESA_WAREHOUSE"
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW			RETURN "CLUB_MISSION_ROW_BUILDING"
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE	RETURN "CLUB_STRAWBERRY_WAREHOUSE"
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD			RETURN "CLUB_WEST_VINEWOOD_BUILDING"
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS			RETURN "CLUB_CYPRESS_FLATS_WAREHOUSE"
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE			RETURN "CLUB_LSIA_WAREHOUSE"
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND			RETURN "CLUB_ELYSIAN_ISLAND_WAREHOUSE"
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD		RETURN "CLUB_VINEWOOD_DT"
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS		RETURN "CLUB_VESPUCCI_CANALS_BUILDING"
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING		RETURN "CLUB_DEL_PERRO_BUILDING"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_IPLS_FOR_BUSINESS_HUB(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA				RETURN "ba_barriers_case0"
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW			RETURN "ba_barriers_case1"
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE	RETURN "ba_barriers_case2"
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD			RETURN "ba_barriers_case3"
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS			RETURN "ba_barriers_case4"
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE			RETURN "ba_barriers_case6"
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND			RETURN "ba_barriers_case7"
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD		RETURN "ba_barriers_case8"
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS		RETURN "ba_barriers_case9"
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING		RETURN "ba_barriers_case5"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_FOR_SALE_IPLS_FOR_BUSINESS_HUB(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA				RETURN "ba_case0_forsale"
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW			RETURN "ba_case1_forsale"
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE	RETURN "ba_case2_forsale"
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD			RETURN "ba_case3_forsale"
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS			RETURN "ba_case4_forsale"
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE			RETURN "ba_case6_forsale"
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND			RETURN "ba_case7_forsale"
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD		RETURN "ba_case8_forsale"
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS		RETURN "ba_case9_forsale"
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING		RETURN "ba_case5_forsale"
	ENDSWITCH
	RETURN ""
ENDFUNC


FUNC STRING GET_DJ_IPLS_FOR_BUSINESS_HUB(SIMPLE_INTERIORS eSimpleInteriorID, INT iID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA		
			IF iID = ciNIGHTCLUB_DJ_SOLOMUN
				RETURN "ba_case0_solomun"
			ELIF iID = ciNIGHTCLUB_DJ_DIXON
				RETURN "ba_case0_dixon"
			ELIF iID = ciNIGHTCLUB_DJ_TALEOFUS
				RETURN "ba_case0_taleofus"
			ELIF iID = ciNIGHTCLUB_DJ_BLACKMAD
				RETURN "ba_case0_madonna"
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW	
			IF iID = ciNIGHTCLUB_DJ_SOLOMUN
				RETURN "ba_case1_solomun"
			ELIF iID = ciNIGHTCLUB_DJ_DIXON
				RETURN "ba_case1_dixon"
			ELIF iID = ciNIGHTCLUB_DJ_TALEOFUS
				RETURN "ba_case1_taleofus"
			ELIF iID = ciNIGHTCLUB_DJ_BLACKMAD
				RETURN "ba_case1_madonna"
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE	
			IF iID = ciNIGHTCLUB_DJ_SOLOMUN
				RETURN "ba_case2_solomun"
			ELIF iID = ciNIGHTCLUB_DJ_DIXON
				RETURN "ba_case2_dixon"
			ELIF iID = ciNIGHTCLUB_DJ_TALEOFUS
				RETURN "ba_case2_taleofus"
			ELIF iID = ciNIGHTCLUB_DJ_BLACKMAD
				RETURN "ba_case2_madonna"
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD	
			IF iID = ciNIGHTCLUB_DJ_SOLOMUN
				RETURN "ba_case3_solomun"
			ELIF iID = ciNIGHTCLUB_DJ_DIXON
				RETURN "ba_case3_dixon"
			ELIF iID = ciNIGHTCLUB_DJ_TALEOFUS
				RETURN "ba_case3_taleofus"
			ELIF iID = ciNIGHTCLUB_DJ_BLACKMAD
				RETURN "ba_case3_madonna"
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS	
			IF iID = ciNIGHTCLUB_DJ_SOLOMUN
				RETURN "ba_case4_solomun"
			ELIF iID = ciNIGHTCLUB_DJ_DIXON
				RETURN "ba_case4_dixon"
			ELIF iID = ciNIGHTCLUB_DJ_TALEOFUS
				RETURN "ba_case4_taleofus"
			ELIF iID = ciNIGHTCLUB_DJ_BLACKMAD
				RETURN "ba_case4_madonna"
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE	
			IF iID = ciNIGHTCLUB_DJ_SOLOMUN
				RETURN "ba_case6_solomun"
			ELIF iID = ciNIGHTCLUB_DJ_DIXON
				RETURN "ba_case6_dixon"
			ELIF iID = ciNIGHTCLUB_DJ_TALEOFUS
				RETURN "ba_case6_taleofus"
			ELIF iID = ciNIGHTCLUB_DJ_BLACKMAD
				RETURN "ba_case6_madonna"
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND		
			IF iID = ciNIGHTCLUB_DJ_SOLOMUN
				RETURN "ba_case7_solomun"
			ELIF iID = ciNIGHTCLUB_DJ_DIXON
				RETURN "ba_case7_dixon"
			ELIF iID = ciNIGHTCLUB_DJ_TALEOFUS
				RETURN "ba_case7_taleofus"
			ELIF iID = ciNIGHTCLUB_DJ_BLACKMAD
				RETURN "ba_case7_madonna"
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD	
			
			IF iID = ciNIGHTCLUB_DJ_SOLOMUN
				RETURN "ba_case8_solomun"
			ELIF iID = ciNIGHTCLUB_DJ_DIXON
				RETURN "ba_case8_dixon"
			ELIF iID = ciNIGHTCLUB_DJ_TALEOFUS
				RETURN "ba_case8_taleofus"
			ELIF iID = ciNIGHTCLUB_DJ_BLACKMAD
				RETURN "ba_case8_madonna"
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS	
			IF iID = ciNIGHTCLUB_DJ_SOLOMUN
				RETURN "ba_case9_solomun"
			ELIF iID = ciNIGHTCLUB_DJ_DIXON
				RETURN "ba_case9_dixon"
			ELIF iID = ciNIGHTCLUB_DJ_TALEOFUS
				RETURN "ba_case9_taleofus"
			ELIF iID = ciNIGHTCLUB_DJ_BLACKMAD
				RETURN "ba_case9_madonna"
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING	
			IF iID = ciNIGHTCLUB_DJ_SOLOMUN
				RETURN "ba_case5_solomun"
			ELIF iID = ciNIGHTCLUB_DJ_DIXON
				RETURN "ba_case5_dixon"
			ELIF iID = ciNIGHTCLUB_DJ_TALEOFUS
				RETURN "ba_case5_taleofus"
			ELIF iID = ciNIGHTCLUB_DJ_BLACKMAD
				RETURN "ba_case5_madonna"
			ENDIF
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC


PROC MAINTAIN_BUSINESS_HUB_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_EXT_SCRIPT_STATE eState, BOOL &bExtScriptMustRun)
	STRING sSG_Name = GET_SCENARIO_GROUP_FOR_BUSINESS_HUB(eSimpleInteriorID)
	BOOL bClubOwned = FALSE
	NIGHTCLUB_ID clubID = GET_NIGHTCLUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	
	IF DOES_ANY_PLAYER_IN_SESSION_OWN_NIGHTCLUB(clubID, bClubOwned)
		IF DOES_SCENARIO_GROUP_EXIST(sSG_Name)
			IF NOT IS_SCENARIO_GROUP_ENABLED(sSG_Name)
				SET_SCENARIO_GROUP_ENABLED(sSG_Name, TRUE)
				PRINTLN("MAINTAIN_BUSINESS_HUB_OUTSIDE_GAMEPLAY_MODIFIERS: turning on scenario group: ",sSG_name)
			ENDIF
		ENDIF
	ELSE
		IF DOES_SCENARIO_GROUP_EXIST(sSG_Name)
			IF IS_SCENARIO_GROUP_ENABLED(sSG_Name)
				SET_SCENARIO_GROUP_ENABLED(sSG_Name,FALSE)
				PRINTLN("MAINTAIN_BUSINESS_HUB_OUTSIDE_GAMEPLAY_MODIFIERS: turning off scenario group (no one owns it): ",sSG_name)
			ENDIF
		ENDIF
	ENDIF
	
	IF clubID = NIGHTCLUB_DEL_PERRO_BUILDING
	AND NOT IS_BIT_SET(details.entryAnim.iBS,BS_SIMPLE_INTERIOR_ANIM_BS_CLUB_HIDE_PLANT)
		CREATE_MODEL_HIDE(<<-1283.508,-649.543,25.524>>,5,INT_TO_ENUM(MODEL_NAMES, HASH("prop_pot_plant_03a")),TRUE)
		SET_BIT(details.entryAnim.iBS,BS_SIMPLE_INTERIOR_ANIM_BS_CLUB_HIDE_PLANT)
		PRINTLN("MAINTAIN_BUSINESS_HUB_OUTSIDE_GAMEPLAY_MODIFIERS: model hiding plants")
	ENDIF
	
	UNUSED_PARAMETER(eState)
	UNUSED_PARAMETER(bExtScriptMustRun)
ENDPROC

PROC RESET_BUSINESS_HUB_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	STRING sSG_Name = GET_SCENARIO_GROUP_FOR_BUSINESS_HUB(eSimpleInteriorID)
	BOOL bClubOwned = FALSE
	IF NOT DOES_ANY_PLAYER_IN_SESSION_OWN_NIGHTCLUB(GET_NIGHTCLUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID),bClubOwned)
		IF DOES_SCENARIO_GROUP_EXIST(sSG_Name)
			IF IS_SCENARIO_GROUP_ENABLED(sSG_Name)
				SET_SCENARIO_GROUP_ENABLED(sSG_Name,FALSE)
				PRINTLN("MAINTAIN_BUSINESS_HUB_OUTSIDE_GAMEPLAY_MODIFIERS: turning off scenario group (no one owns, cleanup) : ",sSG_name)
			ENDIF
		ENDIF
	ENDIF
	IF GET_NIGHTCLUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID) = NIGHTCLUB_DEL_PERRO_BUILDING
		IF IS_BIT_SET(details.entryAnim.iBS,BS_SIMPLE_INTERIOR_ANIM_BS_CLUB_HIDE_PLANT)
			CLEAR_BIT(details.entryAnim.iBS,BS_SIMPLE_INTERIOR_ANIM_BS_CLUB_HIDE_PLANT)
			REMOVE_MODEL_HIDE(<<-1283.508,-649.543,25.524>>,5,INT_TO_ENUM(MODEL_NAMES, HASH("prop_pot_plant_03a")))
			PRINTLN("MAINTAIN_BUSINESS_HUB_OUTSIDE_GAMEPLAY_MODIFIERS: REMOVING plants model hide")
		ENDIF
	ENDIF

	
	UNUSED_PARAMETER(details)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_BUSINESS_HUB(details, TRUE)
ENDPROC

PROC MAINTAIN_BUSINESS_HUB_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(details)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_SCREEN_FADED_IN()
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
	AND GET_INTERIOR_FLOOR_INDEX() > 0
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IgnoreInteriorCheckForSprinting, TRUE)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DontUseSprintEnergy, TRUE)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), 3.0)
	ENDIF
	
	IF g_sMPTunables.bKICK_OUT_OF_NIGHTCLUB_WITH_WEAPON
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_MELEE|WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN)	
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICK_ARMED_PLAYER)
		ENDIF	
	ENDIF 
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), FALSE)
		ELSE
			SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
		ENDIF	
	ENDIF
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		BOOL BulletProofFlag, FlameProofFlag, ExplosionProofFlag, CollisionProofFlag, MeleeProofFlag, SteamProofFlag, DontResetDamageFlagsOnCleanupMissionState, SmokeProofFlag
		
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		AND GET_ENTITY_PROOFS(PLAYER_PED_ID(), BulletProofFlag, FlameProofFlag, ExplosionProofFlag, CollisionProofFlag, MeleeProofFlag, SteamProofFlag, DontResetDamageFlagsOnCleanupMissionState, SmokeProofFlag)
			IF NOT BulletProofFlag OR NOT FlameProofFlag OR NOT ExplosionProofFlag OR NOT CollisionProofFlag OR NOT MeleeProofFlag
				SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
				
				SET_DISABLE_WEAPON_BLADE_FORCES(TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC FORCE_REMOVE_THIS_VEHICLE_FROM_NIGHTCLUB(VEHICLE_INDEX vehIndex, SIMPLE_INTERIOR_LOCAL_DATA& localData)
	IF IS_VEHICLE_EMPTY(vehIndex, TRUE, TRUE, FALSE, FALSE, FALSE, TRUE, TRUE)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehIndex, TRUE)
			SET_ENTITY_AS_MISSION_ENTITY(vehIndex, FALSE, TRUE)
			DELETE_VEHICLE(vehIndex)
			PRINTLN("FORCE_REMOVE_THIS_VEHICLE_FROM_NIGHTCLUB DELETE_VEHICLE")
		ENDIF
	ELSE
		IF HAS_NET_TIMER_EXPIRED(localData.sKickPlayersOutOfVeh, 1500)
			PLAYER_INDEX PlayerID
			INT i
			REPEAT NUM_NETWORK_PLAYERS i
				PlayerID = INT_TO_PLAYERINDEX(i)
				IF IS_NET_PLAYER_OK(PlayerID, FALSE)
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(PlayerID), vehIndex)
						BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(PlayerID), TRUE, 0, 0, FALSE, FALSE)
						REINIT_NET_TIMER(localData.sKickPlayersOutOfVeh)
					ENDIF
				ENDIF
			ENDREPEAT		
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL IS_NIGHTCLUB_USED_FOR_MISSION()

	SWITCH GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())
		CASE FMMC_TYPE_FIXER_VIP
			SWITCH INT_TO_ENUM(FIXER_VIP_VARIATION, GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID()))				
				CASE FVV_NIGHTCLUB
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PUT_AWAY_WEAPON_IN_NIGHTCLUB()

	IF NOT g_sMPTunables.bFORCE_PUT_AWAY_WEAPON_IN_NIGHTCLUB
	
		RETURN FALSE
	ENDIF
	
	IF IS_NIGHTCLUB_USED_FOR_MISSION()
	
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_MELEE|WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN)

		RETURN FALSE
	ENDIF
	
	IF IS_SIMPLE_INTERIOR_INT_SCRIPT_RUNNING()
	
		RETURN FALSE
	ENDIF
	
	RETURN IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-1652.900, -2976.660, -81.630>> , <<-1564.943, -3036.836, -69.890>> ) OR IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-1484.172, -3046.932, -83.960>>, <<-1528.025, -2973.398, -74.070>> )
ENDFUNC

PROC FORCE_PUT_AWAY_WEAPON_IN_NIGHTCLUB()

	IF NOT SHOULD_PUT_AWAY_WEAPON_IN_NIGHTCLUB()
		EXIT
	ENDIF
				
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
	HOLSTER_WEAPON_IN_SIMPLE_INTERIOR()
		
ENDPROC

PROC FORCE_BRICK_NIGHTCLUB_VEHICLES(SIMPLE_INTERIOR_LOCAL_DATA& localData)
	IF g_sMPTunables.bBRICK_ANY_VEHICLE_IN_NIGHTCLUB
		IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-1652.900, -2976.660, -81.630>> , <<-1564.943, -3036.836, -69.890>> )
		OR 	IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-1484.172, -3046.932, -83.960>>, <<-1528.025, -2973.398, -74.070>> )
			VEHICLE_INDEX nearbyVehs[1]
			GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
			IF DOES_ENTITY_EXIST(nearbyVehs[0])
			AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])				
				FORCE_REMOVE_THIS_VEHICLE_FROM_NIGHTCLUB(nearbyVehs[0], localData)		
			ENDIF
		ENDIF
	ENDIF	
	
	IF g_sMPTunables.bBRICK_VEHICLE_OUTSIDE_NIGHTCLUB_STORAGE
		IF IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
			VEHICLE_INDEX nearbyVehs[1]
			GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
			IF DOES_ENTITY_EXIST(nearbyVehs[0])
			AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])	
				IF NOT IS_ENTITY_IN_ANGLED_AREA(nearbyVehs[0], <<-1635.517944,-3014.423584,-79.393784>>, <<-1634.156982,-2994.200195,-73.143768>>, 22.750000)
					FORCE_REMOVE_THIS_VEHICLE_FROM_NIGHTCLUB(nearbyVehs[0], localData)		
				ENDIF
			ENDIF	
		ELIF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
			VEHICLE_INDEX nearbyVehs[1]
			GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
			IF DOES_ENTITY_EXIST(nearbyVehs[0])
			AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])	
				IF NOT IS_ENTITY_IN_ANGLED_AREA(nearbyVehs[0], <<-1509.010010,-3003.625244,-83.457153>>, <<-1507.721313,-2984.231201,-77.207291>>, 27.250000)
					FORCE_REMOVE_THIS_VEHICLE_FROM_NIGHTCLUB(nearbyVehs[0], localData)		
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC FORCE_WARP_OUT_OF_NIGHTCLUB_WITHOUT_INTERIOR_SCRIPT(SIMPLE_INTERIOR_LOCAL_DATA& localData)
	
	IF IS_NIGHTCLUB_USED_FOR_MISSION()
	
		EXIT
	ENDIF
	
	IF g_sMPTunables.bFORCE_WARP_OUT_OF_NIGHTCLUB_WITHOUT_INTERIOR_SCRIPT
		IF NOT IS_SIMPLE_INTERIOR_INT_SCRIPT_RUNNING()
			IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-1652.900, -2976.660, -81.630>> , <<-1564.943, -3036.836, -69.890>> )
			OR 	IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-1484.172, -3046.932, -83.960>>, <<-1528.025, -2973.398, -74.070>> )
				IF NOT HAS_NET_TIMER_STARTED(localData.sWarpOutOfNightClubWithoutIntScript)
					START_NET_TIMER(localData.sWarpOutOfNightClubWithoutIntScript)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(localData.sWarpOutOfNightClubWithoutIntScript, g_sMPTunables.iFORCE_WARP_OUT_OF_NIGHTCLUB_TIMER)
						NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_STUCK_IN_INTERIOR_WITHOUT_SCRIPT))
						RESET_NET_TIMER(localData.sWarpOutOfNightClubWithoutIntScript)
						PRINTLN("FORCE_WARP_OUT_OF_NIGHTCLUB_WITHOUT_INTERIOR_SCRIPT NETWORK_BAIL")
					ENDIF
				ENDIF
			ENDIF	
		ELSE
			IF HAS_NET_TIMER_STARTED(localData.sWarpOutOfNightClubWithoutIntScript)
				RESET_NET_TIMER(localData.sWarpOutOfNightClubWithoutIntScript)
			ENDIF	
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bKICK_OUT_OF_NIGHTCLUB_WITH_WEAPON
		IF NOT IS_SIMPLE_INTERIOR_INT_SCRIPT_RUNNING()
			IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-1652.900, -2976.660, -81.630>> , <<-1564.943, -3036.836, -69.890>> )
			OR 	IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-1484.172, -3046.932, -83.960>>, <<-1528.025, -2973.398, -74.070>> )
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_MELEE|WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN)
					NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_STUCK_IN_INTERIOR_WITHOUT_SCRIPT))
					PRINTLN("FORCE_WARP_OUT_OF_NIGHTCLUB_WITHOUT_INTERIOR_SCRIPT player armed NETWORK_BAIL")
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_BUSINESS_HUB_BG_SCRIPT_PROTECTION(SIMPLE_INTERIOR_LOCAL_DATA& localData)
	
	FORCE_PUT_AWAY_WEAPON_IN_NIGHTCLUB()
	
	FORCE_BRICK_NIGHTCLUB_VEHICLES(localData)
	
	FORCE_WARP_OUT_OF_NIGHTCLUB_WITHOUT_INTERIOR_SCRIPT(localData)
	
ENDPROC 

PROC RESET_BUSINESS_HUB_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(details)
	UNUSED_PARAMETER(eSimpleInteriorID)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_OWNER_TRADE_IN_BUNKER)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_OWNER_TRADE_IN_BUNKER)
		PRINTLN("RESET_BUNKER_INSIDE_GAMEPLAY_MODIFIERS BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_OWNER_TRADE_IN_BUNKER false ")
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IgnoreInteriorCheckForSprinting, FALSE)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
			
			SET_DISABLE_WEAPON_BLADE_FORCES(FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DOES_BUSINESS_HUB_BLOCK_WEAPONS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)

	BOOL bAllowWeaponLoadout
	
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
		IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
			IF IS_PLAYER_NIGHTCLUB_SECURITY_PURCHASED(PLAYER_ID())
			AND g_iPlayerUsingOfficeSeat = -1
			AND IS_PLAYER_IN_NIGHTCLUB_OFFICE_AREA(PLAYER_ID())
				bAllowWeaponLoadout = TRUE
			ELSE
				bAllowWeaponLoadout = FALSE
			ENDIF
		ELSE
			IF IS_PLAYER_NIGHTCLUB_SECURITY_PURCHASED(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
				IF (DOES_PLAYER_OWN_ANY_GUN_LOCKER())
				AND (NOT NETWORK_IS_IN_MP_CUTSCENE())
				AND g_iPlayerUsingOfficeSeat = -1
					bAllowWeaponLoadout = TRUE
				ELSE
					bAllowWeaponLoadout = FALSE
				ENDIF
			ELSE
				bAllowWeaponLoadout = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bAllowWeaponLoadout
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
	ELSE
		HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
	ENDIF
	
	HOLSTER_WEAPON_IN_SIMPLE_INTERIOR()
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_TOGGLE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP)
	
	IF !g_bEnableJumpingOffHalfTrackInProperty
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_HOLD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO_TRACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO_TRACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_TOGGLE)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_RIGHT)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UP)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BIKE_WINGS)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_HOLD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_LEFT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_RIGHT)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_BUSINESS_HUB_RESTRICT_PLAYER_MOVEMENT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN FALSE
ENDFUNC

PROC GET_BUSINESS_HUB_CAR_GENS_BLOCK_AREA(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoBlockCarGen, VECTOR &vMin, VECTOR &vMax)	
	// NOTE: Max 128 vehicle gen blockers allowed, refer to SETUP_PROPERTY_BLOCKS for total used.
	// You must own a Nightclub if you own a HUB -> The Nightclub handles the vehicle gen blocking
	
	bDoBlockCarGen = TRUE
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA 				vMin = <<696.31470, -1296.98047, 23.88614>> 	vMax = <<771.35748, -1359.64331, 29.52297>> 	BREAK
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE 	vMin = <<-194.88725, -1305.98071, 35.34710>> 	vMax = <<-100.65156, -1248.84839, 27.24532>> 	BREAK
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD 			vMin = <<-73.45308, 178.98370, 86.25685>> 		vMax = <<31.96091, 251.98502, 108.58942>>		BREAK
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS 			vMin = <<931.73132, -2075.68237, 28.61308>> 	vMax = <<861.04425, -2138.83936, 33.52427>>		BREAK
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE 		vMin = <<-685.46332, -2380.20190, 11.97208>> 	vMax = <<-668.93805, -2398.39160, 20.33201>>	BREAK
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND 		vMin = <<232.24309, -3066.01563, 3.80817>> 		vMax = <<193.71828, -3116.08252, 9.79027>>		BREAK
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD 		vMin = <<387.72452, 221.25804, 100.07135>> 		vMax = <<381.32700, 228.88564, 105.03999>>		BREAK
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING 	vMin = <<-1307.70081, -638.03784, 20.54498>> 	vMax = <<-1217.77905, -709.91797, 30.15923>>	BREAK
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS 		vMin = <<-1150.45642, -1188.08484, 3.64667>> 	vMax = <<-1176.40833, -1131.24475, 8.70080>>	BREAK		
		DEFAULT bDoBlockCarGen = FALSE BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL GET_BUSINESS_HUB_OUTSIDE_MODEL_HIDE(SIMPLE_INTERIORS eSimpleInteriorID, INT iIndex, VECTOR &vPos, FLOAT &fRadius, FLOAT &fActivationRadius, MODEL_NAMES &eModel, BOOL &bSurviveMapReload)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			SWITCH iIndex
				CASE 0
					vPos 				= <<-675.132, -2459.290, 12.947>>
					fRadius 			= 1.0
					fActivationRadius 	= 300.0
					eModel				= INT_TO_ENUM(MODEL_NAMES, HASH("prop_cratepile_01a"))
					bSurviveMapReload 	= TRUE
				RETURN TRUE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			SWITCH iIndex
				CASE 0
					vPos 				= <<-1235.2034, -689.1898, 22.9430>>
					fRadius 			= 1.0
					fActivationRadius 	= 150.0
					eModel				= INT_TO_ENUM(MODEL_NAMES, HASH("prop_dumpster_01a"))
					bSurviveMapReload 	= TRUE
				RETURN TRUE
				
				CASE 1
					vPos 				= <<-1235.03, -694.9261, 22.5988>>
					fRadius 			= 1.0
					fActivationRadius 	= 150.0
					eModel				= INT_TO_ENUM(MODEL_NAMES, HASH("prop_dumpster_02a"))
					bSurviveMapReload 	= TRUE
				RETURN TRUE
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			SWITCH iIndex
				CASE 0
					vPos 				= <<871.0888, -2099.7539, 29.3509>>
					fRadius 			= 1.0
					fActivationRadius 	= 150.0
					eModel				= INT_TO_ENUM(MODEL_NAMES, HASH("prop_grass_dry_02"))
					bSurviveMapReload 	= TRUE
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC GET_BUSINESS_HUB_SCENARIO_PED_REMOVAL_SPHERE(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoRemoveScenarioPeds, VECTOR &vCoords, FLOAT &fRadius)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(vCoords)
	UNUSED_PARAMETER(fRadius)
	bDoRemoveScenarioPeds = FALSE
ENDPROC

PROC NIGHTCLUB_SETUP_GIFT_DELIVERY_VEHICLE_AWARDED(INT &iSaveSlot, INT &iDisplaySlot)
	IF NOT nightclubGiftDeliveryVehicle.bAttemptedToAward
		IF DOES_LOCAL_PLAYER_OWN_A_BUSINESS_HUB()
			INT iTempSaveSlot
			
			iDisplaySlot = DISPLAY_SLOT_CLUBSPEEDO
			
			MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iTempSaveSlot, FALSE)
			
			IF iTempSaveSlot >= 0
			AND g_MpSavedVehicles[iTempSaveSlot].vehicleSetupMP.VehicleSetup.eModel = SPEEDO4
				nightclubGiftDeliveryVehicle.bAttemptedToAward = TRUE
				
				PRINTLN("NIGHTCLUB_SETUP_GIFT_DELIVERY_VEHICLE_AWARDED: already awarded 1")
				
				EXIT
			ENDIF
			
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_GIFTED_DELIVERY_VEHICLE_1)
				PRINTLN("NIGHTCLUB_SETUP_GIFT_DELIVERY_VEHICLE_AWARDED: already awarded 2")
				
				nightclubGiftDeliveryVehicle.bAttemptedToAward = TRUE
			ELSE
				IF NOT DOES_ENTITY_EXIST(nightclubGiftDeliveryVehicle.theVeh)
					IF REQUEST_LOAD_MODEL(SPEEDO4)
						nightclubGiftDeliveryVehicle.theVeh = CREATE_VEHICLE(SPEEDO4, <<0.0, 0.0, (-10.0)>>, 0.0, FALSE, FALSE, TRUE)
						
						SET_VEHICLE_TYRES_CAN_BURST(nightclubGiftDeliveryVehicle.theVeh, FALSE)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(SPEEDO4)
						
						FREEZE_ENTITY_POSITION(nightclubGiftDeliveryVehicle.theVeh, TRUE)
						
						PRINTLN("NIGHTCLUB_SETUP_GIFT_DELIVERY_VEHICLE_AWARDED: creating the vehicle for the transaction")
					ENDIF
				ELSE
					IF NOT nightclubGiftDeliveryVehicle.bTransactionCompleted
						IF USE_SERVER_TRANSACTIONS()
							iSaveSlot = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST, FALSE, SPEEDO4)
							
							IF iSaveSlot != -1
							AND iDisplaySlot != -1
								IF PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE(nightclubGiftDeliveryVehicle.theVeh, iSaveSlot, nightclubGiftDeliveryVehicle.iTransactionResult, TRUE)
									IF nightclubGiftDeliveryVehicle.iTransactionResult = GARAGE_VEHICLE_TRANSACTION_STATE_SUCCESS
										IF iSaveSlot >= 0
											IF iDisplaySlot >= 0
												MPSV_SET_DISPLAY_SLOT(iDisplaySlot, iSaveSlot)
												
												nightclubGiftDeliveryVehicle.bTransactionCompleted = TRUE
											ENDIF
										ENDIF
									ELSE
										nightclubGiftDeliveryVehicle.bAttemptedToAward = TRUE
										
										ASSERTLN("NIGHTCLUB_SETUP_GIFT_DELIVERY_VEHICLE_AWARDED: transaction failed leaving for now.")
									ENDIF
								ELSE
									EXIT
								ENDIF
							ELSE
								nightclubGiftDeliveryVehicle.bAttemptedToAward = TRUE
								
								SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_GIFTED_DELIVERY_VEHICLE_1, TRUE)
								
								IF g_MpSavedVehicles[DISPLAY_SLOT_CLUBSPEEDO].vehicleSetupMP.VehicleSetup.eModel != SPEEDO4
									ASSERTLN("NIGHTCLUB_SETUP_GIFT_DELIVERY_VEHICLE_AWARDED: no space available skiping awarded vehicle - 1")
								ENDIF
							ENDIF
						ELSE
							iSaveSlot = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST, FALSE, SPEEDO4)
							
							IF iSaveSlot >= 0
							AND iDisplaySlot >= 0
								MPSV_SET_DISPLAY_SLOT(iDisplaySlot, iSaveSlot)
									
								nightclubGiftDeliveryVehicle.bTransactionCompleted = TRUE
							ELSE
								nightclubGiftDeliveryVehicle.bAttemptedToAward = TRUE
								
								SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_GIFTED_DELIVERY_VEHICLE_1, TRUE)
								
								IF g_MpSavedVehicles[DISPLAY_SLOT_CLUBSPEEDO].vehicleSetupMP.VehicleSetup.eModel != SPEEDO4
									ASSERTLN("NIGHTCLUB_SETUP_GIFT_DELIVERY_VEHICLE_AWARDED: no space available skiping awarded vehicle - 2")
								ENDIF
							ENDIF
						ENDIF
						
						EXIT
					ELSE
						INT iTempDecoratorValue
						
						IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
							IF DECOR_EXIST_ON(nightclubGiftDeliveryVehicle.theVeh, "MPBitset")
								iTempDecoratorValue = DECOR_GET_INT(nightclubGiftDeliveryVehicle.theVeh, "MPBitset")
								
								SET_BIT(iTempDecoratorValue, MP_DECORATOR_BS_INSURED_VEHICLE)
							ELSE
								SET_BIT(iTempDecoratorValue, MP_DECORATOR_BS_INSURED_VEHICLE)
							ENDIF
							
							DECOR_SET_INT(nightclubGiftDeliveryVehicle.theVeh, "MPBitset", iTempDecoratorValue)
						ENDIF
						
						MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(nightclubGiftDeliveryVehicle.theVeh, iSaveSlot, TRUE, TRUE, FALSE, TRUE)
						
						nightclubGiftDeliveryVehicle.bAttemptedToAward = TRUE
						
						SET_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_FREE_VEHICLE)
						
						DELETE_VEHICLE(nightclubGiftDeliveryVehicle.theVeh)
						
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_GIFTED_DELIVERY_VEHICLE_1, TRUE)
						
						PRINTLN("NIGHTCLUB_SETUP_GIFT_DELIVERY_VEHICLE_AWARDED: stored vehicle")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF DOES_ENTITY_EXIST(nightclubGiftDeliveryVehicle.theVeh)
		DELETE_VEHICLE(nightclubGiftDeliveryVehicle.theVeh)
	ENDIF
ENDPROC

PROC MAINTAIN_HUB_DELIVER_TEXTS(INT &iBS)
	IF NOT IS_BIT_SET(iBS, 0)
		IF IS_HUB_VEHICLE_ON_COOLDOWN(SPEEDO4)
			IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DELIVERY_VEHICLE_DEST)
			AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				PRINT_HELP_WITH_STRING("HUB_DEST_2", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(SPEEDO4))
				
				SET_BIT(iBS, 0)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_HUB_VEHICLE_ON_COOLDOWN(SPEEDO4)
			IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DELIVERY_VEHICLE_DEST)
				CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DELIVERY_VEHICLE_DEST)
			ELSE
				IF IS_OKAY_TO_SEND_CONTACT_TEXT_MESSAGE()
					IF GET_FRAME_COUNT() % 15 = 0
						SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_NCLUBT, "HUB_VEH_RETURN1", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
					ELSE
						SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_NCLUBT, "HUB_VEH_RETURN2", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
					ENDIF
					
					CLEAR_BIT(iBS, 0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iBS, 1)
		IF IS_HUB_VEHICLE_ON_COOLDOWN(MULE4)
			IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DELIVERY_VEHICLE_DEST)
			AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				PRINT_HELP_WITH_STRING("HUB_DEST_2", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(MULE4))
				
				SET_BIT(iBS, 1)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_HUB_VEHICLE_ON_COOLDOWN(MULE4)
			IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DELIVERY_VEHICLE_DEST)
				CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DELIVERY_VEHICLE_DEST)
			ELSE
				IF IS_OKAY_TO_SEND_CONTACT_TEXT_MESSAGE()
					IF GET_FRAME_COUNT() % 15 = 0
						SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_NCLUBT, "HUB_VEH_RETURN1", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
					ELSE
						SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_NCLUBT, "HUB_VEH_RETURN3", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
					ENDIF
					
					CLEAR_BIT(iBS, 1)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iBS, 2)
		IF IS_HUB_VEHICLE_ON_COOLDOWN(POUNDER2)
			IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DELIVERY_VEHICLE_DEST)
			AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				PRINT_HELP_WITH_STRING("HUB_DEST_2", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(POUNDER2))
				
				SET_BIT(iBS, 2)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_HUB_VEHICLE_ON_COOLDOWN(POUNDER2)
			IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DELIVERY_VEHICLE_DEST)
				CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_DELIVERY_VEHICLE_DEST)
			ELSE
				IF IS_OKAY_TO_SEND_CONTACT_TEXT_MESSAGE()
					IF GET_FRAME_COUNT() % 15 = 0
						SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_NCLUBT, "HUB_VEH_RETURN1", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
					ELSE
						SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_NCLUBT, "HUB_VEH_RETURN4", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
					ENDIF
					
					CLEAR_BIT(iBS, 2)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BUSINESS_HUB_PULL_IN_PLAYERS()
	IF GET_INTERIOR_FLOOR_INDEX() = 0
		IF HAS_LOCAL_PLAYER_COMPLETED_NIGHTCLUB_STAFF_COLLECTION()
		AND HAS_LOCAL_PLAYER_COMPLETED_NIGHTCLUB_EQUIPMENT_COLLECTION()
		AND NOT HAS_LOCAL_PLAYER_VIEWED_NIGHTCLUB_SETUP_CUTSCENE()
			RETURN TRUE
		ENDIF
		
		IF HAS_LOCAL_PLAYER_VIEWED_NIGHTCLUB_SETUP_CUTSCENE()
		AND HAS_LOCAL_PLAYER_COMPLETED_NIGHTCLUB_DJ_COLLECTION()
		AND NOT HAS_LOCAL_PLAYER_VIEWED_NIGHTCLUB_SOLOMUN_CUTSCENE()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_BUSINESS_HUB_ENTRANCE_SCENE_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS& details, INT iEntranceID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
			SWITCH iEntranceID
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
					details.entryAnim.cameraPos = <<732.9764, -1290.3488, 26.9224>>
					details.entryAnim.cameraRot = <<-33.9653, -0.0000, -125.1292>>
					details.entryAnim.syncScenePos = <<733.971, -1291.201, 25.2842>>
					details.entryAnim.syncSceneRot = <<0.000, 0.000, 0.000>>
					details.entryAnim.establishingCameraPos = <<711.5486, -1311.7397, 26.9087>>
					details.entryAnim.establishingCameraRot = <<9.7556, 0.0692, -49.8918>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.dictionary = "anim@apt_trans@garage"
					details.entryAnim.clip = "gar_open_1_left"
					details.entryAnim.cameraFov = 50.0000
					details.entryAnim.startingPhase = 0.0
					details.entryAnim.endingPhase = 0.45
					details.entryAnim.fEstablishingCameraFov = 32.3120
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
					details.entryAnim.establishingCameraPos = <<711.5486, -1311.7397, 26.9087>>
					details.entryAnim.establishingCameraRot = <<9.7556, 0.0692, -49.8918>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.fEstablishingCameraFov = 32.3120
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
					IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
						details.entryAnim.cameraPos = <<766.9297, -1332.1254, 26.4050>>
						details.entryAnim.cameraRot = <<5.1238, -0.0324, 101.1261>>
						details.entryAnim.cameraFov = 20.0722
						details.entryAnim.cameraPos2 = <<766.9297, -1332.1254, 26.4050>>
						details.entryAnim.cameraRot2 = <<5.1238, -0.0324, 93.4529>>
						details.entryAnim.cameraFov2 = 20.0722
						details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
						details.entryAnim.syncScenePos = <<758.029, -1331.955, 26.2802>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, 0.000>>
						details.entryAnim.establishingCameraPos = <<774.9752, -1346.5548, 28.0999>>
						details.entryAnim.establishingCameraRot = <<5.7589, 0.0026, 43.0470>>
						details.entryAnim.strOnEnterSoundNames[0] = "GENERIC_DOOR_PUSH"
						details.entryAnim.strOnEnterSoundNames[1] = "GENERIC_DOOR_LIMIT"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.295
						details.entryAnim.fOnEnterSoundPhases[1] = 0.350
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_SCRIPT_DOORS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "ext_player"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 47.2371
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.dictionaryIdle = ""
						details.entryAnim.clipIdle = ""
						details.entryAnim.fCamShake = 0.1
					ELSE
						details.entryAnim.cameraPos = <<751.9787, -1336.3892, 27.0828>>
						details.entryAnim.cameraRot = <<0.1407, 0.0213, -63.7208>>
						details.entryAnim.cameraFov = 29.8289
						details.entryAnim.cameraPos2 = <<752.5981, -1336.1887, 27.0843>>
						details.entryAnim.cameraRot2 = <<2.0130, 0.0213, -62.3465>>
						details.entryAnim.cameraFov2 = 29.8289
						details.entryAnim.dictionary = "anim@apt_trans@buzzer"
						details.entryAnim.syncScenePos = <<756.998, -1331.917, 26.280>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, 0.000>>
						details.entryAnim.establishingCameraPos = <<774.9752, -1346.5548, 28.0999>>
						details.entryAnim.establishingCameraRot = <<5.7589, 0.0026, 43.0470>>
						details.entryAnim.strOnEnterSoundNames[0] = "DOOR_BUZZ_ONESHOT_MASTER"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.353
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "buzz_short"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 47.2371
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.fCamShake = 0.1
						
						IF IS_PLAYER_PED_FEMALE(PLAYER_ID())
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@female_arms_crossed@enter"
							details.entryAnim.clipIdle = "enter"
						ELSE
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@male_c@enter"
							details.entryAnim.clipIdle = "enter"
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
			SWITCH iEntranceID
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
					details.entryAnim.cameraPos = <<332.3591, -995.3273, 29.7830>>
					details.entryAnim.cameraRot = <<-34.2946, 0.0000, -30.1727>>
					details.entryAnim.syncScenePos = <<333.159, -994.426, 28.321>>
					details.entryAnim.syncSceneRot = <<0.000, 0.000, 90.0>>
					details.entryAnim.establishingCameraPos = <<320.3405, -1002.1382, 29.9967>>
					details.entryAnim.establishingCameraRot = <<5.1997, -0.1430, -57.2915>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.dictionary = "anim@apt_trans@garage"
					details.entryAnim.clip = "gar_open_1_left"
					details.entryAnim.cameraFov = 50.0000
					details.entryAnim.startingPhase = 0.0
					details.entryAnim.endingPhase = 0.45
					details.entryAnim.fEstablishingCameraFov = 39.1327
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
					details.entryAnim.establishingCameraPos = <<320.3405, -1002.1382, 29.9967>>
					details.entryAnim.establishingCameraRot = <<5.1997, -0.1430, -57.2915>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.fEstablishingCameraFov = 39.1327
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
					IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
						details.entryAnim.cameraPos = <<345.7383, -972.8771, 29.8109>>
						details.entryAnim.cameraRot = <<-0.3374, -0.0272, -173.5036>>
						details.entryAnim.cameraFov = 21.8576
						details.entryAnim.cameraPos2 = <<345.7367, -972.8389, 29.8110>>
						details.entryAnim.cameraRot2 = <<-0.3374, -0.0272, 179.5144>>
						details.entryAnim.cameraFov2 = 21.8576
						details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
						details.entryAnim.syncScenePos = <<345.009, -977.811, 28.354>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, -90.0>>
						details.entryAnim.establishingCameraPos = <<353.0908, -944.1270, 36.5719>>
						details.entryAnim.establishingCameraRot = <<-9.5129, 0.0550, 169.6899>>
						details.entryAnim.strOnEnterSoundNames[0] = "GENERIC_DOOR_PUSH"
						details.entryAnim.strOnEnterSoundNames[1] = "GENERIC_DOOR_LIMIT"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.295
						details.entryAnim.fOnEnterSoundPhases[1] = 0.350
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_SCRIPT_DOORS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "ext_player"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 26.5215
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.dictionaryIdle = ""
						details.entryAnim.clipIdle = ""
						details.entryAnim.fCamShake = 0.1
					ELSE
						details.entryAnim.cameraPos = <<350.0937, -987.0516, 29.4925>>
						details.entryAnim.cameraRot = <<-1.1151, 0.0344, 29.3254>>
						details.entryAnim.cameraFov = 24.8976
						details.entryAnim.cameraPos2 = <<349.7521, -986.7112, 29.4844>>
						details.entryAnim.cameraRot2 = <<0.0137, 0.0344, 29.3254>>
						details.entryAnim.cameraFov2 = 24.8976
						details.entryAnim.dictionary = "anim@apt_trans@buzzer"
						details.entryAnim.syncScenePos = <<345.211, -978.907, 28.391>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, 90.0>>
						details.entryAnim.establishingCameraPos = <<353.0908, -944.1270, 36.5719>>
						details.entryAnim.establishingCameraRot = <<-9.5129, 0.0550, 169.6899>>
						details.entryAnim.strOnEnterSoundNames[0] = "DOOR_BUZZ_ONESHOT_MASTER"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.353
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "buzz_short"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 26.5215
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.fCamShake = 0.1
						
						IF IS_PLAYER_PED_FEMALE(PLAYER_ID())
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@female_arms_crossed@enter"
							details.entryAnim.clipIdle = "enter"
						ELSE
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@male_c@enter"
							details.entryAnim.clipIdle = "enter"
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
			SWITCH iEntranceID
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
					details.entryAnim.cameraPos = <<-164.9168, -1293.6842, 32.1573>>
					details.entryAnim.cameraRot = <<-40.8131, -0.0000, -26.9471>>
					details.entryAnim.syncScenePos = <<-164.223, -1292.637, 30.319>>
					details.entryAnim.syncSceneRot = <<0.000, 0.000, 92.520>>
					details.entryAnim.establishingCameraPos = <<-152.7097, -1316.4662, 32.9863>>
					details.entryAnim.establishingCameraRot = <<4.8618, -0.0778, 27.9171>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.dictionary = "anim@apt_trans@garage"
					details.entryAnim.clip = "gar_open_1_left"
					details.entryAnim.cameraFov = 50.0000
					details.entryAnim.startingPhase = 0.0
					details.entryAnim.endingPhase = 0.45
					details.entryAnim.fEstablishingCameraFov = 43.7476
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
					details.entryAnim.establishingCameraPos = <<-152.7097, -1316.4662, 32.9863>>
					details.entryAnim.establishingCameraRot = <<4.8618, -0.0778, 27.9171>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.fEstablishingCameraFov = 43.7476
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
					IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
						details.entryAnim.cameraPos = <<-120.2416, -1252.5824, 29.5363>>
						details.entryAnim.cameraRot = <<0.4478, -0.0047, 178.3607>>
						details.entryAnim.cameraFov = 23.1452
						details.entryAnim.cameraPos2 = <<-120.2414, -1252.5830, 29.5363>>
						details.entryAnim.cameraRot2 = <<0.5015, 0.0127, 172.6561>>
						details.entryAnim.cameraFov2 = 23.1452
						details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
						details.entryAnim.syncScenePos = <<-121.897, -1259.057, 28.308>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, 92.520>>
						details.entryAnim.establishingCameraPos = <<-83.4536, -1300.0598, 34.3550>>
						details.entryAnim.establishingCameraRot = <<6.6798, 0.1368, 52.8183>>
						details.entryAnim.strOnEnterSoundNames[0] = "GENERIC_DOOR_PUSH"
						details.entryAnim.strOnEnterSoundNames[1] = "GENERIC_DOOR_LIMIT"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.295
						details.entryAnim.fOnEnterSoundPhases[1] = 0.350
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_SCRIPT_DOORS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "ext_player"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 39.7724
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.dictionaryIdle = ""
						details.entryAnim.clipIdle = ""
						details.entryAnim.fCamShake = 0.1
					ELSE
						details.entryAnim.cameraPos = <<-112.3821, -1269.3860, 29.3331>>
						details.entryAnim.cameraRot = <<1.0725, 0.0316, 48.5627>>
						details.entryAnim.cameraFov = 22.6512
						details.entryAnim.cameraPos2 = <<-112.7527, -1269.0587, 29.3420>>
						details.entryAnim.cameraRot2 = <<1.0341, 0.0602, 48.5720>>
						details.entryAnim.cameraFov2 = 22.6512
						details.entryAnim.dictionary = "anim@apt_trans@buzzer"
						details.entryAnim.syncScenePos = <<-121.360, -1260.548, 28.308>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, 90.000>>
						details.entryAnim.establishingCameraPos = <<-83.4536, -1300.0598, 34.3550>>
						details.entryAnim.establishingCameraRot = <<6.6798, 0.1368, 52.8183>>
						details.entryAnim.strOnEnterSoundNames[0] = "DOOR_BUZZ_ONESHOT_MASTER"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.353
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "buzz_short"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 39.7724
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.fCamShake = 0.1
						
						IF IS_PLAYER_PED_FEMALE(PLAYER_ID())
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@female_arms_crossed@enter"
							details.entryAnim.clipIdle = "enter"
						ELSE
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@male_c@enter"
							details.entryAnim.clipIdle = "enter"
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
			SWITCH iEntranceID
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
					details.entryAnim.cameraPos = <<-23.0476, 218.7487, 107.2282>>
					details.entryAnim.cameraRot = <<-31.2697, -0.0000, -40.8044>>
					details.entryAnim.syncScenePos = <<-21.944, 219.546, 105.725>>
					details.entryAnim.syncSceneRot = <<0.000, 0.000, 81.360>>
					details.entryAnim.establishingCameraPos = <<-44.4688, 204.7237, 108.9804>>
					details.entryAnim.establishingCameraRot = <<-0.4062, -0.0758, -58.6869>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.dictionary = "anim@apt_trans@garage"
					details.entryAnim.clip = "gar_open_1_left"
					details.entryAnim.cameraFov = 50.0000
					details.entryAnim.startingPhase = 0.0
					details.entryAnim.endingPhase = 0.45
					details.entryAnim.fEstablishingCameraFov = 36.5531
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
					details.entryAnim.establishingCameraPos = <<-44.4688, 204.7237, 108.9804>>
					details.entryAnim.establishingCameraRot = <<-0.4062, -0.0758, -58.6869>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.fEstablishingCameraFov = 36.5531
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
					IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
						details.entryAnim.cameraPos = <<2.6233, 214.9600, 108.2002>>
						details.entryAnim.cameraRot = <<-3.6361, -0.0210, -16.8917>>
						details.entryAnim.cameraFov = 22.6597
						details.entryAnim.cameraPos2 = <<2.6233, 214.9600, 108.2002>>
						details.entryAnim.cameraRot2 = <<-3.6361, -0.0210, -12.2103>>
						details.entryAnim.cameraFov2 = 22.6597
						details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
						details.entryAnim.syncScenePos = <<3.774, 220.598, 106.805>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, -111.960>>
						details.entryAnim.establishingCameraPos = <<25.2870, 227.5200, 109.8637>>
						details.entryAnim.establishingCameraRot = <<7.0436, 0.1001, 104.4277>>
						details.entryAnim.strOnEnterSoundNames[0] = "GENERIC_DOOR_PUSH"
						details.entryAnim.strOnEnterSoundNames[1] = "GENERIC_DOOR_LIMIT"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.295
						details.entryAnim.fOnEnterSoundPhases[1] = 0.350
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_SCRIPT_DOORS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "ext_player"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 42.5363
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.dictionaryIdle = ""
						details.entryAnim.clipIdle = ""
						details.entryAnim.fCamShake = 0.1
					ELSE
						details.entryAnim.cameraPos = <<14.8820, 224.7149, 109.2220>>
						details.entryAnim.cameraRot = <<-7.6156, -0.0484, 104.2159>>
						details.entryAnim.cameraFov = 29.4327
						details.entryAnim.cameraPos2 = <<14.1812, 224.4970, 109.1240>>
						details.entryAnim.cameraRot2 = <<-7.6156, -0.0484, 104.2159>>
						details.entryAnim.cameraFov2 = 29.4327
						details.entryAnim.dictionary = "anim@apt_trans@buzzer"
						details.entryAnim.syncScenePos = <<5.123, 221.377, 106.874>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, 70.0>>
						details.entryAnim.establishingCameraPos = <<25.2870, 227.5200, 109.8637>>
						details.entryAnim.establishingCameraRot = <<7.0436, 0.1001, 104.4277>>
						details.entryAnim.strOnEnterSoundNames[0] = "DOOR_BUZZ_ONESHOT_MASTER"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.353
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "buzz_short"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 42.5363
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.fCamShake = 0.1
						
						IF IS_PLAYER_PED_FEMALE(PLAYER_ID())
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@female_arms_crossed@enter"
							details.entryAnim.clipIdle = "enter"
						ELSE
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@male_c@enter"
							details.entryAnim.clipIdle = "enter"
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			SWITCH iEntranceID
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
					details.entryAnim.cameraPos = <<910.1858, -2096.0752, 31.2476>>
					details.entryAnim.cameraRot = <<-28.9932, -0.0000, 153.3770>>
					details.entryAnim.syncScenePos = <<909.102, -2097.391, 29.555>>
					details.entryAnim.syncSceneRot = <<0.000, 0.000, -93.240>>
					details.entryAnim.establishingCameraPos = <<903.2930, -2071.3438, 33.1299>>
					details.entryAnim.establishingCameraRot = <<4.4652, -0.0810, -162.9494>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.dictionary = "anim@apt_trans@garage"
					details.entryAnim.clip = "gar_open_1_left"
					details.entryAnim.cameraFov = 50.0000
					details.entryAnim.startingPhase = 0.0
					details.entryAnim.endingPhase = 0.45
					details.entryAnim.fEstablishingCameraFov = 35.2646
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
					details.entryAnim.establishingCameraPos = <<903.2930, -2071.3438, 33.1299>>
					details.entryAnim.establishingCameraRot = <<4.4652, -0.0810, -162.9494>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.fEstablishingCameraFov = 35.2646
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
					IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
						details.entryAnim.cameraPos = <<870.8740, -2104.2908, 31.1287>>
						details.entryAnim.cameraRot = <<-4.5030, -0.0326, 1.4382>>
						details.entryAnim.cameraFov = 30.5624
						details.entryAnim.cameraPos2 = <<870.8743, -2104.2908, 31.1287>>
						details.entryAnim.cameraRot2 = <<-4.5357, -0.0234, -6.9947>>
						details.entryAnim.cameraFov2 = 30.5624
						details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
						details.entryAnim.syncScenePos = <<871.784, -2100.517, 29.485>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, -93.240>>
						details.entryAnim.establishingCameraPos = <<859.2952, -2077.3840, 32.0000>>
						details.entryAnim.establishingCameraRot = <<6.9830, -0.0846, -141.1513>>
						details.entryAnim.strOnEnterSoundNames[0] = "GENERIC_DOOR_PUSH"
						details.entryAnim.strOnEnterSoundNames[1] = "GENERIC_DOOR_LIMIT"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.295
						details.entryAnim.fOnEnterSoundPhases[1] = 0.350
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_SCRIPT_DOORS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "ext_player"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 41.6961
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.dictionaryIdle = ""
						details.entryAnim.clipIdle = ""
						details.entryAnim.fCamShake = 0.1
					ELSE
						details.entryAnim.cameraPos = <<857.1143, -2093.4609, 32.0931>>
						details.entryAnim.cameraRot = <<-6.0736, -0.0569, -113.0412>>
						details.entryAnim.cameraFov = 23.7740
						details.entryAnim.cameraPos2 = <<858.1631, -2093.8984, 31.9719>>
						details.entryAnim.cameraRot2 = <<-6.0736, -0.0569, -113.0412>>
						details.entryAnim.cameraFov2 = 23.7740
						details.entryAnim.dictionary = "anim@apt_trans@buzzer"
						details.entryAnim.syncScenePos = <<871.913, -2099.536, 29.468>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, -93.240>>
						details.entryAnim.establishingCameraPos = <<859.2952, -2077.3840, 32.0000>>
						details.entryAnim.establishingCameraRot = <<6.9830, -0.0846, -141.1513>>
						details.entryAnim.strOnEnterSoundNames[0] = "DOOR_BUZZ_ONESHOT_MASTER"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.353
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "buzz_short"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 41.6961
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.fCamShake = 0.1
						
						IF IS_PLAYER_PED_FEMALE(PLAYER_ID())
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@female_arms_crossed@enter"
							details.entryAnim.clipIdle = "enter"
						ELSE
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@male_c@enter"
							details.entryAnim.clipIdle = "enter"
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			SWITCH iEntranceID
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
					details.entryAnim.cameraPos = <<-671.2646, -2391.3389, 14.3195>>
					details.entryAnim.cameraRot = <<-34.3980, -0.0000, -145.8071>>
					details.entryAnim.syncScenePos = <<-670.722, -2392.449, 12.945>>
					details.entryAnim.syncSceneRot = <<0.000, 0.000, -27.360>>
					details.entryAnim.establishingCameraPos = <<-690.7659, -2388.2324, 15.4628>>
					details.entryAnim.establishingCameraRot = <<6.3247, -0.1369, -101.1767>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.dictionary = "anim@apt_trans@garage"
					details.entryAnim.clip = "gar_open_1_left"
					details.entryAnim.cameraFov = 50.0000
					details.entryAnim.startingPhase = 0.0
					details.entryAnim.endingPhase = 0.45
					details.entryAnim.fEstablishingCameraFov = 50.0
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
					details.entryAnim.establishingCameraPos = <<-690.7659, -2388.2324, 15.4628>>
					details.entryAnim.establishingCameraRot = <<6.3247, -0.1369, -101.1767>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.fEstablishingCameraFov = 50.0000
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
					IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
						details.entryAnim.cameraPos = <<-681.7309, -2455.3752, 15.1347>>
						details.entryAnim.cameraRot = <<-8.8550, 0.0247, -119.3388>>
						details.entryAnim.cameraFov = 22.2466
						details.entryAnim.cameraPos2 = <<-681.7309, -2455.3752, 15.1347>>
						details.entryAnim.cameraRot2 = <<-8.8550, 0.0247, -114.6191>>
						details.entryAnim.cameraFov2 = 22.2466
						details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
						details.entryAnim.syncScenePos = <<-675.973, -2457.823, 12.944>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, 145.0>>
						details.entryAnim.establishingCameraPos = <<-710.5236, -2466.3430, 16.6375>>
						details.entryAnim.establishingCameraRot = <<0.8154, -0.0557, -73.0783>>
						details.entryAnim.strOnEnterSoundNames[0] = "GENERIC_DOOR_PUSH"
						details.entryAnim.strOnEnterSoundNames[1] = "GENERIC_DOOR_LIMIT"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.295
						details.entryAnim.fOnEnterSoundPhases[1] = 0.350
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_SCRIPT_DOORS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "ext_player"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 32.7225
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.dictionaryIdle = ""
						details.entryAnim.clipIdle = ""
						details.entryAnim.fCamShake = 0.1
					ELSE
						details.entryAnim.cameraPos = <<-693.5314, -2461.2681, 14.3947>>
						details.entryAnim.cameraRot = <<-2.1671, -0.0080, -81.7402>>
						details.entryAnim.cameraFov = 22.9362
						details.entryAnim.cameraPos2 = <<-692.3806, -2461.1409, 14.3506>>
						details.entryAnim.cameraRot2 = <<-2.1859, 0.0064, -81.7588>>
						details.entryAnim.cameraFov2 = 22.9362
						details.entryAnim.dictionary = "anim@apt_trans@buzzer"
						details.entryAnim.syncScenePos = <<-675.070, -2458.729, 12.944>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, -30.0>>
						details.entryAnim.establishingCameraPos = <<-710.5236, -2466.3430, 16.6375>>
						details.entryAnim.establishingCameraRot = <<0.8154, -0.0557, -73.0783>>
						details.entryAnim.strOnEnterSoundNames[0] = "DOOR_BUZZ_ONESHOT_MASTER"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.353
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "buzz_short"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 32.7225
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.fCamShake = 0.1
						
						IF IS_PLAYER_PED_FEMALE(PLAYER_ID())
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@female_arms_crossed@enter"
							details.entryAnim.clipIdle = "enter"
						ELSE
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@male_c@enter"
							details.entryAnim.clipIdle = "enter"
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
			SWITCH iEntranceID
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
					details.entryAnim.cameraPos = <<228.9341, -3136.0327, 6.1133>>
					details.entryAnim.cameraRot = <<-32.6990, -0.0000, 149.4484>>
					details.entryAnim.syncScenePos = <<228.126, -3137.098, 4.790>>
					details.entryAnim.syncSceneRot = <<0.000, 0.000, -90.000>>
					details.entryAnim.establishingCameraPos = <<198.7077, -3086.3662, 7.1945>>
					details.entryAnim.establishingCameraRot = <<15.5745, -0.1055, -148.6160>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.dictionary = "anim@apt_trans@garage"
					details.entryAnim.clip = "gar_open_1_left"
					details.entryAnim.cameraFov = 50.0
					details.entryAnim.startingPhase = 0.0
					details.entryAnim.endingPhase = 0.45
					details.entryAnim.fEstablishingCameraFov = 44.4095
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
					details.entryAnim.establishingCameraPos = <<198.7077, -3086.3662, 7.1945>>
					details.entryAnim.establishingCameraRot = <<15.5745, -0.1055, -148.6160>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.fEstablishingCameraFov = 44.4095
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
					IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
						details.entryAnim.cameraPos = <<195.7928, -3162.7649, 6.3781>>
						details.entryAnim.cameraRot = <<-3.3617, -0.0138, 175.6586>>
						details.entryAnim.cameraFov = 24.9030
						details.entryAnim.cameraPos2 = <<195.7931, -3162.7651, 6.3781>>
						details.entryAnim.cameraRot2 = <<-3.3019, -0.0122, -177.7359>>
						details.entryAnim.cameraFov2 = 24.9030
						details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
						details.entryAnim.syncScenePos = <<195.893, -3166.785, 4.790>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, -90.000>>
						details.entryAnim.establishingCameraPos = <<165.8182, -3111.9749, 9.2922>>
						details.entryAnim.establishingCameraRot = <<-1.4158, 0.0000, -154.7472>>
						details.entryAnim.strOnEnterSoundNames[0] = "GENERIC_DOOR_PUSH"
						details.entryAnim.strOnEnterSoundNames[1] = "GENERIC_DOOR_LIMIT"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.295
						details.entryAnim.fOnEnterSoundPhases[1] = 0.350
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_SCRIPT_DOORS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "ext_player"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 23.9466
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.dictionaryIdle = ""
						details.entryAnim.clipIdle = ""
						details.entryAnim.fCamShake = 0.1
					ELSE
						details.entryAnim.cameraPos = <<184.4767, -3183.5251, 8.2147>>
						details.entryAnim.cameraRot = <<-7.0205, -0.0120, -36.0039>>
						details.entryAnim.cameraFov = 19.7843
						details.entryAnim.cameraPos2 = <<185.0856, -3182.6868, 8.0869>>
						details.entryAnim.cameraRot2 = <<-7.0339, -0.0005, -36.0202>>
						details.entryAnim.cameraFov2 = 19.7843
						details.entryAnim.dictionary = "anim@apt_trans@buzzer"
						details.entryAnim.syncScenePos = <<195.930, -3168.760, 4.790>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, -90.000>>
						details.entryAnim.establishingCameraPos = <<165.8182, -3111.9749, 9.2922>>
						details.entryAnim.establishingCameraRot = <<-1.4158, 0.0000, -154.7472>>
						details.entryAnim.strOnEnterSoundNames[0] = "DOOR_BUZZ_ONESHOT_MASTER"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.353
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "buzz_short"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 23.9466
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.fCamShake = 0.1
						
						IF IS_PLAYER_PED_FEMALE(PLAYER_ID())
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@female_arms_crossed@enter"
							details.entryAnim.clipIdle = "enter"
						ELSE
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@male_c@enter"
							details.entryAnim.clipIdle = "enter"
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
			SWITCH iEntranceID
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
					details.entryAnim.cameraPos = <<378.6244, 227.2742, 103.5900>>
					details.entryAnim.cameraRot = <<-28.5122, -0.0000, 42.8991>>
					details.entryAnim.syncScenePos = <<377.791, 228.536, 102.041>>
					details.entryAnim.syncSceneRot = <<0.000, 0.000, 160.0>>
					details.entryAnim.establishingCameraPos = <<417.9737, 268.4984, 105.3028>>
					details.entryAnim.establishingCameraRot = <<8.9579, 0.1211, 125.3569>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.dictionary = "anim@apt_trans@garage"
					details.entryAnim.clip = "gar_open_1_left"
					details.entryAnim.cameraFov = 50.0
					details.entryAnim.startingPhase = 0.0
					details.entryAnim.endingPhase = 0.45
					details.entryAnim.fEstablishingCameraFov = 40.5806
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
					details.entryAnim.establishingCameraPos = <<417.9737, 268.4984, 105.3028>>
					details.entryAnim.establishingCameraRot = <<8.9579, 0.1211, 125.3569>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.fEstablishingCameraFov = 40.5806
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
					IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
						details.entryAnim.cameraPos = <<365.8969, 254.6099, 104.4705>>
						details.entryAnim.cameraRot = <<-10.1362, -0.0298, -104.9060>>
						details.entryAnim.cameraFov = 17.3858
						details.entryAnim.cameraPos2 = <<365.8956, 254.6100, 104.4707>>
						details.entryAnim.cameraRot2 = <<-10.1524, -0.0363, -109.1475>>
						details.entryAnim.cameraFov2 = 17.3858
						details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
						details.entryAnim.syncScenePos = <<371.390, 251.933, 102.010>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, 160.0>>
						details.entryAnim.establishingCameraPos = <<405.0143, 277.6437, 106.8855>>
						details.entryAnim.establishingCameraRot = <<10.0144, 0.1097, 130.9049>>
						details.entryAnim.strOnEnterSoundNames[0] = "GENERIC_DOOR_PUSH"
						details.entryAnim.strOnEnterSoundNames[1] = "GENERIC_DOOR_LIMIT"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.295
						details.entryAnim.fOnEnterSoundPhases[1] = 0.350
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_SCRIPT_DOORS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "ext_player"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 41.9232
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.dictionaryIdle = ""
						details.entryAnim.clipIdle = ""
						details.entryAnim.fCamShake = 0.1
					ELSE
						details.entryAnim.cameraPos = <<368.3786, 272.6442, 104.2729>>
						details.entryAnim.cameraRot = <<-3.6979, 0.0161, -167.2546>>
						details.entryAnim.cameraFov = 15.6749
						details.entryAnim.cameraPos2 = <<368.7488, 271.6270, 104.2087>>
						details.entryAnim.cameraRot2 = <<-4.3374, 0.0310, -168.0554>>
						details.entryAnim.cameraFov2 = 15.6749
						details.entryAnim.dictionary = "anim@apt_trans@buzzer"
						details.entryAnim.syncScenePos = <<372.980, 251.700, 102.010>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, 160.0>>
						details.entryAnim.establishingCameraPos = <<405.0143, 277.6437, 106.8855>>
						details.entryAnim.establishingCameraRot = <<10.0144, 0.1097, 130.9049>>
						details.entryAnim.strOnEnterSoundNames[0] = "DOOR_BUZZ_ONESHOT_MASTER"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.353
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "buzz_short"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 41.9232
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.fCamShake = 0.1
						
						IF IS_PLAYER_PED_FEMALE(PLAYER_ID())
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@female_arms_crossed@enter"
							details.entryAnim.clipIdle = "enter"
						ELSE
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@male_c@enter"
							details.entryAnim.clipIdle = "enter"
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			SWITCH iEntranceID
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
					details.entryAnim.cameraPos = <<-1236.0948, -693.8033, 24.1480>>
					details.entryAnim.cameraRot = <<-28.7894, -0.0000, 103.7940>>
					details.entryAnim.syncScenePos = <<-1237.538, -693.933, 22.774>>
					details.entryAnim.syncSceneRot = <<0.000, 0.000, -141.120>>
					details.entryAnim.establishingCameraPos = <<-1217.1691, -697.5862, 23.4599>>
					details.entryAnim.establishingCameraRot = <<10.9504, 0.0707, 80.6866>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.dictionary = "anim@apt_trans@garage"
					details.entryAnim.clip = "gar_open_1_left"
					details.entryAnim.cameraFov = 50.0
					details.entryAnim.startingPhase = 0.0
					details.entryAnim.endingPhase = 0.45
					details.entryAnim.fEstablishingCameraFov = 38.7236
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
					details.entryAnim.establishingCameraPos = <<-1217.1691, -697.5862, 23.4599>>
					details.entryAnim.establishingCameraRot = <<10.9504, 0.0707, 80.6866>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.fEstablishingCameraFov = 38.7236
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
					IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
						details.entryAnim.cameraPos = <<-1281.8516, -648.2418, 28.0509>>
						details.entryAnim.cameraRot = <<-15.1256, -0.0016, 144.3962>>
						details.entryAnim.cameraFov = 31.5705
						details.entryAnim.cameraPos2 = <<-1281.8516, -648.2418, 28.0509>>
						details.entryAnim.cameraRot2 = <<-15.1256, -0.0016, 148.3430>>
						details.entryAnim.cameraFov2 = 31.5705
						details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
						details.entryAnim.syncScenePos = <<-1284.307, -652.138, 25.633>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, -141.120>>
						details.entryAnim.establishingCameraPos = <<-1283.1177, -626.3682, 28.5328>>
						details.entryAnim.establishingCameraRot = <<9.9341, -0.1005, -175.6886>>
						details.entryAnim.strOnEnterSoundNames[0] = "GENERIC_DOOR_PUSH"
						details.entryAnim.strOnEnterSoundNames[1] = "GENERIC_DOOR_LIMIT"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.295
						details.entryAnim.fOnEnterSoundPhases[1] = 0.350
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_SCRIPT_DOORS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "ext_player"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 52.5911
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.dictionaryIdle = ""
						details.entryAnim.clipIdle = ""
						details.entryAnim.fCamShake = 0.1
					ELSE
						details.entryAnim.cameraPos = <<-1285.8186, -638.1773, 27.0734>>
						details.entryAnim.cameraRot = <<-0.5751, 0.0195, -171.1263>>
						details.entryAnim.cameraFov = 22.7477
						details.entryAnim.cameraPos2 = <<-1285.6035, -639.4156, 27.0609>>
						details.entryAnim.cameraRot2 = <<-0.5751, 0.0195, -171.1263>>
						details.entryAnim.cameraFov2 = 22.7477
						details.entryAnim.dictionary = "anim@apt_trans@buzzer"
						details.entryAnim.syncScenePos = <<-1283.030, -650.280, 25.618>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, -141.120>>
						details.entryAnim.establishingCameraPos = <<-1283.1177, -626.3682, 28.5328>>
						details.entryAnim.establishingCameraRot = <<9.9341, -0.1005, -175.6886>>
						details.entryAnim.strOnEnterSoundNames[0] = "DOOR_BUZZ_ONESHOT_MASTER"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.353
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "buzz_short"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 52.5911
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.fCamShake = 0.1
						
						IF IS_PLAYER_PED_FEMALE(PLAYER_ID())
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@female_arms_crossed@enter"
							details.entryAnim.clipIdle = "enter"
						ELSE
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@male_c@enter"
							details.entryAnim.clipIdle = "enter"
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			SWITCH iEntranceID
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_FOOT
					details.entryAnim.cameraPos = <<-1173.9739, -1175.0013, 5.9560>>
					details.entryAnim.cameraRot = <<-31.1335, -0.0000, -9.0984>>
					details.entryAnim.syncScenePos = <<-1173.642, -1173.699, 4.624>>
					details.entryAnim.syncSceneRot = <<0.000, 0.000, 107.640>>
					details.entryAnim.establishingCameraPos = <<-1158.5580, -1184.7996, 6.4328>>
					details.entryAnim.establishingCameraRot = <<6.5882, 0.0642, 60.2060>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.dictionary = "anim@apt_trans@garage"
					details.entryAnim.clip = "gar_open_1_left"
					details.entryAnim.cameraFov = 50.0000
					details.entryAnim.startingPhase = 0.0
					details.entryAnim.endingPhase = 0.45
					details.entryAnim.fEstablishingCameraFov = 43.1519
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_WAREHOUSE_VEH
					details.entryAnim.establishingCameraPos = <<-1158.5580, -1184.7996, 6.4328>>
					details.entryAnim.establishingCameraRot = <<6.5882, 0.0642, 60.2060>>
					details.entryAnim.strOnEnterSoundNames[0] = ""
					details.entryAnim.strOnEnterSoundNames[1] = ""
					details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
					details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
					details.entryAnim.fEstablishingCameraFov = 43.1519
					details.entryAnim.fEstablishingCameraShake = 0.2
					details.entryAnim.cameraPos2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraRot2 = <<0.0, 0.0, 0.0>>
					details.entryAnim.cameraFov2 = 0.0
					details.entryAnim.dictionaryIdle = ""
					details.entryAnim.clipIdle = ""
					details.entryAnim.fCamShake = 0.1
				BREAK
				
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_DOOR
				CASE ciBUSINESS_HUB_ENTRANCE_NIGHTCLUB_BUZZER
					IF DOES_PLAYER_OWN_BUSINESS_HUB(PLAYER_ID(), GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
						details.entryAnim.cameraPos = <<-1175.3407, -1151.5372, 6.3319>>
						details.entryAnim.cameraRot = <<-6.3640, -0.0184, -147.2254>>
						details.entryAnim.cameraFov = 30.4384
						details.entryAnim.cameraPos2 = <<-1175.3407, -1151.5372, 6.3319>>
						details.entryAnim.cameraRot2 = <<-6.3640, -0.0184, -159.4812>>
						details.entryAnim.cameraFov2 = 30.4384
						details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
						details.entryAnim.syncScenePos = <<-1174.923, -1153.458, 4.660>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, 107.640>>
						details.entryAnim.establishingCameraPos = <<-1165.6947, -1132.5479, 7.0100>>
						details.entryAnim.establishingCameraRot = <<2.4561, -0.1432, 159.1615>>
						details.entryAnim.strOnEnterSoundNames[0] = "GENERIC_DOOR_PUSH"
						details.entryAnim.strOnEnterSoundNames[1] = "GENERIC_DOOR_LIMIT"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.295
						details.entryAnim.fOnEnterSoundPhases[1] = 0.350
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_SCRIPT_DOORS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "ext_player"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 44.3756
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.dictionaryIdle = ""
						details.entryAnim.clipIdle = ""
						details.entryAnim.fCamShake = 0.1
					ELSE
						details.entryAnim.cameraPos = <<-1163.0682, -1151.5201, 5.6160>>
						details.entryAnim.cameraRot = <<0.5519, -0.0059, 88.8311>>
						details.entryAnim.cameraFov = 21.8279
						details.entryAnim.cameraPos2 = <<-1163.9590, -1151.5021, 5.6241>>
						details.entryAnim.cameraRot2 = <<0.5216, 0.0051, 88.8523>>
						details.entryAnim.cameraFov2 = 21.8279
						details.entryAnim.dictionary = "anim@apt_trans@buzzer"
						details.entryAnim.syncScenePos = <<-1175.200, -1152.460, 4.661>>
						details.entryAnim.syncSceneRot = <<0.000, 0.000, 107.640>>
						details.entryAnim.establishingCameraPos = <<-1165.6947, -1132.5479, 7.0100>>
						details.entryAnim.establishingCameraRot = <<2.4561, -0.1432, 159.1615>>
						details.entryAnim.strOnEnterSoundNames[0] = "DOOR_BUZZ_ONESHOT_MASTER"
						details.entryAnim.fOnEnterSoundPhases[0] = 0.353
						details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS"
						details.entryAnim.strOnEnterFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterFadeOutFinishSoundName = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = ""
						details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = ""
						details.entryAnim.clip = "buzz_short"
						details.entryAnim.startingPhase = 0.200
						details.entryAnim.endingPhase = 0.800
						details.entryAnim.fEstablishingCameraFov = 44.3756
						details.entryAnim.fEstablishingCameraShake = 0.2
						details.entryAnim.fCamShake = 0.1
						
						IF IS_PLAYER_PED_FEMALE(PLAYER_ID())
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@female_arms_crossed@enter"
							details.entryAnim.clipIdle = "enter"
						ELSE
							details.entryAnim.dictionaryIdle = "amb@world_human_hang_out_street@male_c@enter"
							details.entryAnim.clipIdle = "enter"
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL DOES_LOCAL_PLAYER_OWN_DELIVERY_SPEEDO()
	INT iDisplaySlot, iSaveSlot
	
	iDisplaySlot = DISPLAY_SLOT_CLUBSPEEDO
	
	MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot, FALSE)
	
	IF iSaveSlot >= 0
	AND g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = SPEEDO4
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_LOCAL_PLAYER_OWN_DELIVERY_MULE()
	INT iDisplaySlot, iSaveSlot
	
	iDisplaySlot = DISPLAY_SLOT_CLUBMULE
	
	MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot, FALSE)
	
	IF iSaveSlot >= 0
	AND g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = MULE4
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_LOCAL_PLAYER_OWN_DELIVERY_POUNDER()
	INT iDisplaySlot, iSaveSlot
	
	iDisplaySlot = DISPLAY_SLOT_CLUBPOUNDER
	
	MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot, FALSE)
	
	IF iSaveSlot >= 0
	AND g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = POUNDER2
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_LOCAL_PLAYER_OWN_DELIVERY_MULE_BEING_DELIVERED()
	INT iDisplaySlot, iSaveSlot
	
	iDisplaySlot = DISPLAY_SLOT_CLUBMULE
	
	MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot, FALSE)
	
	IF iSaveSlot >= 0
	AND g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = MULE4
		RETURN IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_JUST_PURCHASED)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_LOCAL_PLAYER_OWN_DELIVERY_POUNDER_BEING_DELIVERED()
	INT iDisplaySlot, iSaveSlot
	
	iDisplaySlot = DISPLAY_SLOT_CLUBPOUNDER
	
	MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot, FALSE)
	
	IF iSaveSlot >= 0
	AND g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = POUNDER2
		RETURN IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_JUST_PURCHASED)
	ENDIF
	
	RETURN FALSE
ENDFUNC

//================================= INTERIOR EXIT CUTSCENE
FUNC BOOL INT_CUTSCENE_BUSINESS_HUB_EXIT_SHOULD_PLAY(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(entryAnim)
		
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICKED_BY_BOUNCER)
		PRINTLN("[SIMPLE_INTERIOR][BUSINESS_HUB] INT_CUTSCENE_BUSINESS_HUB_EXIT_SHOULD_PLAY - SIMPLE_INTERIOR_BUSINESS_HUB_ACCESS_BS_KICKED_BY_BOUNCER - will play.")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[SIMPLE_INTERIOR][BUSINESS_HUB] INT_CUTSCENE_BUSINESS_HUB_EXIT_SHOULD_PLAY - will not play.")
	RETURN FALSE
ENDFUNC

FUNC BOOL INT_CUTSCENE_BUSINESS_HUB_EXIT_ASSETS_LOADED(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(entryAnim)
	
	CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[BUSINESS_HUB] - INT_CUTSCENE_BUSINESS_HUB_EXIT_ASSETS_LOADED")
	SET_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_SCENE_USES_CUSTOM_CUTSCENE)
	
	STRING sAnimDict = "mini@strip_club@throwout_d@"
	BOOL bAllLoaded = TRUE

				
	REQUEST_ANIM_DICT(sAnimDict)
	IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
		bAllLoaded = FALSE
	ENDIF

	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_clubbar_01")))
	IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_clubbar_01")))
		PRINTLN("INT_CUTSCENE_BUSINESS_HUB_EXIT_ASSETS_LOADED - s_m_y_clubbar_01 has not loaded")
		bAllLoaded = FALSE
	ENDIF
	
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_Bouncer_01")))
	IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_Bouncer_01")))
		PRINTLN("INT_CUTSCENE_BUSINESS_HUB_EXIT_ASSETS_LOADED - S_M_M_Bouncer_01 has not loaded")
		bAllLoaded = FALSE
	ENDIF
	
	IF NOT bAllLoaded
		RETURN FALSE
	ENDIF
	CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[BUSINESS_HUB] - EXT_CUTSCENE_BASE_LOAD_ASSETS - TRUE.")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL INT_CUTSCENE_BUSINESS_HUB_EXIT_ASSETS_CREATED(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	VECTOR scenePosition 	= <<-1560.3361, -3019.0583, -74.8061>>
	VECTOR sceneRotation 	= << 0.000, 0.000, -30.000 >>	
	STRING sAnimDict = "mini@strip_club@throwout_d@"			
	
	IF NOT DOES_ENTITY_EXIST(entryAnim.pedArray[0])
		entryAnim.pedArray[0] = CREATE_PED(PEDTYPE_CIVMALE, INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_clubbar_01")), scenePosition + <<0, 0, -10>>, 0, FALSE)
		SET_PED_DEFAULT_COMPONENT_VARIATION(entryAnim.pedArray[0])
		FREEZE_ENTITY_POSITION(entryAnim.pedArray[0], TRUE)
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(entryAnim.pedArray[1])
		entryAnim.pedArray[1] = CREATE_PED(PEDTYPE_CIVMALE, INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_Bouncer_01")), scenePosition + <<0, 0, -10>>, 0, FALSE)	
		SET_PED_DEFAULT_COMPONENT_VARIATION(entryAnim.pedArray[1])
		SET_PED_COMPONENT_VARIATION(entryAnim.pedArray[1], PED_COMP_HEAD, 0, 2)
		SET_PED_COMPONENT_VARIATION(entryAnim.pedArray[1], PED_COMP_HAIR, 1, 0)
		SET_PED_COMPONENT_VARIATION(entryAnim.pedArray[1], PED_COMP_TORSO, 1, 1)
		SET_PED_COMPONENT_VARIATION(entryAnim.pedArray[1], PED_COMP_LEG, 0, 0)
		SET_PED_COMPONENT_VARIATION(entryAnim.pedArray[1], PED_COMP_JBIB, 0, 0)
		SET_PED_COMPONENT_VARIATION(entryAnim.pedArray[1], PED_COMP_SPECIAL, 1, 0)
		FREEZE_ENTITY_POSITION(entryAnim.pedArray[1], TRUE)
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(entryAnim.pedArray[2])
		CUTSCENE_HELP_CLONE_PLAYER(entryAnim.pedArray[2], PLAYER_ID())
		SET_ENTITY_COORDS(entryAnim.pedArray[2], scenePosition + <<0, 0, -10>>, FALSE)
		FREEZE_ENTITY_POSITION(entryAnim.pedArray[2], TRUE)
		RETURN FALSE
	ENDIF
	
	INT iPed
	REPEAT 3 iPed
		IF IS_ENTITY_ALIVE(entryAnim.pedArray[iPed])
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(entryAnim.pedArray[iPed])
				PRINTLN("INT_CUTSCENE_BUSINESS_HUB_EXIT_ASSETS_CREATED: waiting for peds to be finished streaming")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Let the simple interior system know what to do after the scene
	SET_BIT(g_SimpleInteriorData.iThirdBS, BS3_SIMPLE_INTERIOR_GLOBAL_DATA_DOING_EXIT_SCENE_IN_INTERIOR)
	scenePosition += <<0, 4, 0>>
	SIMPLE_CUTSCENE_ADD_ANIMATED_CAM_SCENE(g_SimpleInteriorData.sCustomCutsceneInt, 5000, "Club Tout", scenePosition, sceneRotation, sAnimDict, "throwout_d_cam", FALSE)
	
	//start cutscene running	
	SET_ENTITY_VISIBLE_IN_CUTSCENE(PLAYER_PED_ID(), TRUE, TRUE)
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_USE_PLAYER_FADE_OUT)
	
	entryAnim.syncSceneID	= CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
	
	IF IS_ENTITY_ALIVE(entryAnim.pedArray[0])
		FREEZE_ENTITY_POSITION(entryAnim.pedArray[0], FALSE)
		TASK_SYNCHRONIZED_SCENE(entryAnim.pedArray[0], entryAnim.syncSceneID, sAnimDict, "throwout_d_bouncer_a", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
	ENDIF
	IF IS_ENTITY_ALIVE(entryAnim.pedArray[1])
		FREEZE_ENTITY_POSITION(entryAnim.pedArray[1], FALSE)
		TASK_SYNCHRONIZED_SCENE(entryAnim.pedArray[1], entryAnim.syncSceneID, sAnimDict, "throwout_d_bouncer_b", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
	ENDIF
	IF IS_ENTITY_ALIVE(entryAnim.pedArray[2])
		FREEZE_ENTITY_POSITION(entryAnim.pedArray[2], FALSE)
		TASK_SYNCHRONIZED_SCENE(entryAnim.pedArray[2], entryAnim.syncSceneID, sAnimDict, "throwout_d_victim", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC INT_CUTSCENE_BUSINESS_HUB_EXIT_MAINTAIN(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	SET_ENTITY_VISIBLE_IN_CUTSCENE(PLAYER_PED_ID(), FALSE)

	IF IS_SYNCHRONIZED_SCENE_RUNNING(entryAnim.syncSceneID)
		IF GET_SYNCHRONIZED_SCENE_PHASE(entryAnim.syncSceneID) > 0.0
		AND NOT IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_NIGHTCLUB_EXIT_SETUP_PED_SPEECH)
			IF IS_ENTITY_ALIVE(entryAnim.pedArray[1])
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(entryAnim.pedArray[1], "BOUNCER_EJECT_GENERIC", "", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE_SHOUTED_CLEAR))
			ENDIF
			
			SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_NIGHTCLUB_EXIT_SETUP_PED_SPEECH)
		ELIF GET_SYNCHRONIZED_SCENE_PHASE(entryAnim.syncSceneID) > 0.3
			IF NOT IS_SCREEN_FADING_OUT()
			AND NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(500)
				CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[INTERIOR_BASE] - INT_CUTSCENE_BUSINESS_HUB_EXIT_MAINTAIN: fading out screen")
			ELIF IS_SCREEN_FADED_OUT()
				entryAnim.syncSceneID = -1
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC INT_CUTSCENE_BUSINESS_HUB_EXIT_CLEANUP(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	UNUSED_PARAMETER(eSimpleInteriorID)
	STRING sAnimDict = "mini@strip_club@throwout_d@"

	IF DOES_CAM_EXIST( entryAnim.camera )
		IF IS_CAM_ACTIVE(entryAnim.camera)
			DESTROY_CAM(entryAnim.camera)
		ENDIF
	ENDIF
	
	INT iPed
	REPEAT 3 iPed
		IF DOES_ENTITY_EXIST(entryAnim.pedArray[iPed])
			DELETE_PED(entryAnim.pedArray[iPed])
		ENDIF
	ENDREPEAT
	
	REMOVE_ANIM_DICT(sAnimDict)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE,DEFAULT,DEFAULT,TRUE)
	CLEANUP_MP_CUTSCENE(FALSE, FALSE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	PRINTLN("INT_CUTSCENE_BUSINESS_HUB_EXIT_CLEANUP: called this frame - Exit via menu? ", IS_BIT_SET(g_SimpleInteriorData.iThirdBS, BS3_SIMPLE_INTERIOR_GLOBAL_DATA_DOING_EXIT_SCENE_IN_INTERIOR))
ENDPROC

//================================= EXTERIOR ENTER CUTSCENE

FUNC BOOL SHOULD_NIGHTCLUB_VEH_SCENE_BE_SKIPPED(NIGHTCLUB_ID eNightclub)
	IF eNightclub = NIGHTCLUB_DOWNTOWN_VINEWOOD
	OR eNightclub = NIGHTCLUB_ELYSIAN_ISLAND
	OR eNightclub = NIGHTCLUB_DEL_PERRO_BUILDING
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				IF DOES_ENTITY_EXIST(veh)
					//The vehicle is visibly too tall for the garrage door so skip the scene
					VECTOR vModelMin, vModelMax
					MODEL_NAMES eModel = GET_ENTITY_MODEL(veh)
					SAFE_GET_MODEL_DIMENSIONS(eModel, vModelMin, vModelMax)
				
					//Check the height of the vehicle
					IF (vModelMax.z - vModelMin.z) > NIGHTCLUB_SCENE_VEH_MAX_HEIGHT	
					#IF FEATURE_COPS_N_CROOKS
					OR eModel = POLICET2
					#ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL EXT_CUTSCENE_BUSINESS_HUB_LOAD_ASSETS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(entryAnim)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL EXT_CUTSCENE_BUSINESS_HUB_CREATE_ASSETS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)	
	NIGHTCLUB_ID eNightclub = GET_NIGHTCLUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	
	IF SHOULD_NIGHTCLUB_VEH_SCENE_BE_SKIPPED(eNightclub)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[NIGHTCLUB] - EXT_CUTSCENE_BUSINESS_HUB_CREATE_ASSETS: ped is in a vehicle.")
		
		VEHICLE_INDEX vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF NOT DOES_ENTITY_EXIST(vehPlayerIsIn)
			ASSERTLN("[NIGHTCLUB] - EXT_CUTSCENE_BUSINESS_HUB_CREATE_ASSETS - ped is in a vehicle, but can not grab vehicle reference...?")
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(entryAnim.sCutsceneVeh.vehicle)
			CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[NIGHTCLUB] - EXT_CUTSCENE_BUSINESS_HUB_CREATE_ASSETS - created clone of the player vehicle.")
			CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS(vehPlayerIsIn, entryAnim.sCutsceneVeh, chVEHICLE_CLONE_TRAILER | chVEHICLE_CLONE_FIX_TIRES | chVEHICLE_CLONE_ONLY_PLAYER_PASSENGERS | chVEHICLE_CLONE_DO_NOT_CLONE_PASSENGERS)
			CUTSCENE_HELP_DISABLE_VEHICLE_AUTOMATIC_CRASH(entryAnim.sCutsceneVeh.vehicle)
			SET_DISABLE_MAP_COLLISION(entryAnim.sCutsceneVeh.vehicle)
			
			RETURN FALSE
		ELSE
			IF CUTSCENE_HELP_VEH_PASSENGERS_CLONE(vehPlayerIsIn, entryAnim.sCutsceneVeh.vehicle, entryAnim.sCutsceneVeh.vehPassengers, TRUE, TRUE)
				CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[NIGHTCLUB] - EXT_CUTSCENE_BUSINESS_HUB_CREATE_ASSETS - Placed ped clones inside the vehicle on entry.")
				SET_DISABLE_MAP_COLLISION(entryAnim.sCutsceneVeh.vehicle)
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
		
		VECTOR vEstablishingShot 	= NIGHTCLUB_VEH_SCENE_GET_SCENE_CAMERA_ESTABLISH_CAM_POS(eNightclub)
		VECTOR vEstablishingRot		= NIGHTCLUB_VEH_SCENE_GET_SCENE_CAMERA_ESTABLISH_CAM_ROT(eNightclub)
		FLOAT fEstablishingFOV		= NIGHTCLUB_VEH_SCENE_GET_SCENE_ESTABLISH_CAM_FOV(eNightclub)
		FLOAT fZoomFOV				= NIGHTCLUB_VEH_SCENE_GET_SCENE_ZOOM_CAM_FOV(eNightclub)
		BOOL bSkipSecondShot		= FALSE //SHOULD_DELIVERY_SCENE_SKIP_SECOND_CUT(eNightclub, entryAnim.iSceneBS)
			
		VECTOR vTDShot 	= NIGHTCLUB_VEH_SCENE_GET_SCENE_CAMERA_TOP_DOWN_POS(eNightclub)
		VECTOR vTDRot	= NIGHTCLUB_VEH_SCENE_GET_SCENE_CAMERA_TOP_DOWN_ROT(eNightclub)
		FLOAT fTDFOV	= NIGHTCLUB_VEH_SCENE_GET_SCENE_CAMERA_TOP_DOWN_FOV(eNightclub)
		
		RETURN SIMPLE_INTERIOR_VEH_ENTRY_SCENE_CREATE_SCENE(g_SimpleInteriorData.sCustomCutsceneExt, vEstablishingShot, vEstablishingRot, fEstablishingFOV, fZoomFOV, bSkipSecondShot, vTDShot, vTDRot, fTDFOV)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC NIGHTCLUB_VEH_ENTRY_CONTROL_VEH_CLONE(NIGHTCLUB_ID eNightclub, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	SIMPLE_INTERIOR_VEH_ENTRY_SCENE_DATA sEntrySceneData
	
	sEntrySceneData.bVehAboveLengthLimit 	= IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_VEH_ABOVE_LENGTH_LIMIT)
	sEntrySceneData.fStartHeading 			= NIGHTCLUB_VEH_SCENE_VEHICLE_START_HEADING(eNightclub)
	sEntrySceneData.vVehStartPosition		= NIGHTCLUB_VEH_SCENE_GET_VEHICLE_START_POSITION(eNightclub, FALSE)
	sEntrySceneData.vVehEndPosition			= NIGHTCLUB_VEH_SCENE_VEHICLE_END_POSITION(eNightclub)
	
	//Indexes
	sEntrySceneData.iVecArrayIndexPrimaryVehModelMax 	= CUTSCENE_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX
	sEntrySceneData.iVecArrayIndexSavedVehCoords		= CUTSCENE_VEC_ARRAY_SAVED_VEH_COORD
	
	sEntrySceneData.iFloatArrayIndexSavedVehSpeed		= CUTSCENE_FLOAT_ARRAY_VEH_SPEED
	sEntrySceneData.iFloatArrayIndexVehMoveDistance		= CUTSCENE_FLOAT_MOVE_VEH_DISTANCE
	sEntrySceneData.iFloatArrayIndexVehZCoord			= CUTSCENE_FLOAT_ARRAY_VEH_Z_COORD
	
	//BS Ints
	sEntrySceneData.iTaskedVeh							= CUTSCENE_BS_TASKED_MOTORBIKE
	sEntrySceneData.iFrozenVehClone						= CUTSCENE_BS_FROZEN_CLONE
	
	NIGHTCLUB_VEH_SCENE_GET_VEHICLE_DRIVE_FORWARD_LIMIT_AREA(eNightclub, sEntrySceneData.vLimitAreaA, sEntrySceneData.vLimitAreaB, sEntrySceneData.fLimitAreaWidth)
	
	SIMPLE_INTERIOR_VEH_ENTRY_CONTROL_VEH_CLONE(entryAnim, sEntrySceneData)
ENDPROC

FUNC BOOL SHOULD_NIGHTCLUB_VEH_SCENE_SKIP_SECOND_CUT(NIGHTCLUB_ID eNightclub, INT iBS)
	IF eNightclub = NIGHTCLUB_DOWNTOWN_VINEWOOD
	OR eNightclub = NIGHTCLUB_ELYSIAN_ISLAND
	OR eNightclub = NIGHTCLUB_DEL_PERRO_BUILDING
		RETURN IS_BIT_SET(iBS, CUTSCENE_BS_TALL_VEH_SCENE)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL EXT_CUTSCENE_BUSINESS_HUB_MAINTAIN(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	NIGHTCLUB_ID eNightclub = GET_NIGHTCLUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	
	IF SHOULD_NIGHTCLUB_VEH_SCENE_BE_SKIPPED(eNightclub)
		RETURN TRUE
	ENDIF
	
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(g_SimpleInteriorData.sCustomCutsceneExt, 0)
	OR SIMPLE_CUTSCENE_IS_SCENE_RUNNING(g_SimpleInteriorData.sCustomCutsceneExt, 1)
	OR SIMPLE_CUTSCENE_IS_SCENE_RUNNING(g_SimpleInteriorData.sCustomCutsceneExt, 2)
	OR SIMPLE_CUTSCENE_IS_SCENE_RUNNING(g_SimpleInteriorData.sCustomCutsceneExt, 3)
	
		//FIRST FRAME
		IF NOT IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_SETUP_FIRST_VEHICLE_SCENE)
			CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(entryAnim.sCutsceneVeh.vehicle, TRUE)
			
			IF IS_ENTITY_ALIVE(entryAnim.sCutsceneVeh.vehicle)
				
				BOOL bExtraLongVehicle = FALSE
				
				//Locally conceal the primary delivery vehicle
				EXT_CUTSCENE_SIMPLE_INTERIOR_CONCEAL_ENTRY_VEHICLE()				
								
				VECTOR vModelMin
				//Grab the model max for use later
				GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle), vModelMin, entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX])
				
				//Check the height of the vehicle
				IF (entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].z - vModelMin.z) > NIGHTCLUB_SCENE_VEH_MAX_HEIGHT	
					SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_TALL_VEH_SCENE)
				ENDIF
				
				//Check the length of the vehicle
				IF (entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].y - vModelMin.y) > NIGHTCLUB_SCENE_VEH_MAX_LENGTH						
					bExtraLongVehicle = TRUE
					IF SHOULD_NIGHTCLUB_VEH_SCENE_SKIP_SECOND_CUT(eNightclub, entryAnim.iSceneBS)
						SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_VEH_ABOVE_LENGTH_LIMIT)
					ENDIF
					CDEBUG3LN(DEBUG_DELIVERY, "EXT_CUTSCENE_BUSINESS_HUB_MAINTAIN - Using extra long vehicle spawn: ", (entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].y - vModelMin.y))
				ENDIF
				
				VECTOR vVehiclePosition = NIGHTCLUB_VEH_SCENE_GET_VEHICLE_START_POSITION(eNightclub, bExtraLongVehicle)
				
				SET_ENTITY_COORDS_NO_OFFSET(entryAnim.sCutsceneVeh.vehicle, vVehiclePosition)
				SET_ENTITY_HEADING(entryAnim.sCutsceneVeh.vehicle, NIGHTCLUB_VEH_SCENE_VEHICLE_START_HEADING(eNightclub, bExtraLongVehicle))
				
				FREEZE_ENTITY_POSITION(entryAnim.sCutsceneVeh.vehicle, FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(entryAnim.sCutsceneVeh.vehicle)
				SET_ENTITY_COLLISION(entryAnim.sCutsceneVeh.vehicle, TRUE)
				SET_ENTITY_VISIBLE(entryAnim.sCutsceneVeh.vehicle, TRUE)					
				SET_VEHICLE_ENGINE_ON(entryAnim.sCutsceneVeh.vehicle, TRUE, TRUE)
				
				SET_FOCUS_POS_AND_VEL(vVehiclePosition, <<0.0, 0.0, 0.0>>)
				
				entryAnim.fStateArray[CUTSCENE_FLOAT_MOVE_VEH_DISTANCE] = (entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].y - vModelMin.y)
				entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].x = 0.0
				entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].z = 0.0
				//Setup complete
				SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_SETUP_FIRST_VEHICLE_SCENE)
			ENDIF
		ENDIF
		
		NIGHTCLUB_VEH_ENTRY_CONTROL_VEH_CLONE(eNightclub, entryAnim)
	ENDIF
	
	//Start fading out
	IF (g_SimpleInteriorData.sCustomCutsceneExt.bPlaying
	AND g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount > 0
	AND g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene = g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount - 1
	AND (g_SimpleInteriorData.sCustomCutsceneExt.sScenes[g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene].iDuration - g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime) <= 400)
		OR NOT g_SimpleInteriorData.sCustomCutsceneExt.bPlaying
								
		IF (GET_FRAME_COUNT() % 60) = 0
			CDEBUG1LN(DEBUG_PROPERTY, "[DEFUNCT_BASE] - EXT_CUTSCENE_DEFUNCT_BASE_MAINTAIN - NOT PLAYING")
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL EXT_CUTSCENE_BUSINESS_HUB_TRANSITION_TO_MENU(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(entryAnim)
	
	RETURN TRUE
ENDFUNC

PROC EXT_CUTSCENE_BUSINESS_HUB_CLEANUP(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim, BOOL bCleanupMPScene = TRUE)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(entryAnim)
	UNUSED_PARAMETER(bCleanupMPScene)
ENDPROC

PROC MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_BUSINESS_HUB_ACCESS(SIMPLE_INTERIOR_DETAILS &details)
	UNUSED_PARAMETER(details)
ENDPROC		

FUNC SIMPLE_INTERIORS GET_OWNED_SMPL_INT_OF_TYPE_BUSINESS_HUB(PLAYER_INDEX playerID, INT iPropertySlot, INT iFactoryType)
	UNUSED_PARAMETER(iFactoryType)
	UNUSED_PARAMETER(iPropertySlot)
	RETURN GET_OWNED_BUSINESS_HUB_SIMPLE_INTERIOR(playerID)
ENDFUNC

PROC BUSINESS_HUB_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX veh, BOOL bSetOnGround)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF bSetOnGround
	AND NOT IS_ENTITY_DEAD(veh)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(veh)
		SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		PRINTLN("[personal_vehicle] BUSINESS_HUB_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT - Setting vehicle on ground.")
		FREEZE_ENTITY_POSITION(veh, FALSE)
	ENDIF
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(veh)
	AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
	AND GET_OWNER_OF_PERSONAL_VEHICLE(veh) = PLAYER_ID()
		SET_BUSINESS_HUB_VEHICLE_AS_LAST_USED()
		PRINTLN("[personal_vehicle] BUSINESS_HUB_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT, MPGlobals.VehicleData.bAssignToMainScript = TRUE")
		SET_PV_ASSIGN_TO_MAIN_SCRIPT(TRUE)
	ENDIF

	IF IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(veh)
	AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
	AND GET_OWNER_OF_PERSONAL_HACKER_TRUCK(veh) = PLAYER_ID()
		PRINTLN("[personal_vehicle][Hacker_Truck] BUSINESS_HUB_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT, MPGlobals.VehicleData.bAssignToMainScript = TRUE")
		SET_HACKER_TRUCK_ASSIGN_TO_MAIN_SCRIPT(TRUE)
	ENDIF
ENDPROC

PROC DO_BUSINESS_HUB_PRE_EXIT_CLEANUP(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	g_SimpleInteriorData.bForceCCTVCleanup = TRUE
	g_SimpleInteriorData.bForceDrinkActivitiesCleanup = TRUE
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡  LOOK-UP TABLE  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Below is the function that builds the interface. Because look up table needs to be built 
// on every call, as an optimisation the function retrieves a pointer to one function at a time 
// so that one call = one lookup. Functions are grouped thematically.
PROC BUILD_BUSINESS_HUB_LOOK_UP_TABLE(SIMPLE_INTERIOR_INTERFACE &interface, SIMPLE_INTERIOR_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		CASE E_DOES_SIMPLE_INTERIOR_USE_EXTERIOR_SCRIPT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_BUSINESS_HUB_USE_EXTERIOR_SCRIPT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION
			interface.getSimpleInteriorInteriorTypeAndPosition = &GET_BUSINESS_HUB_TYPE_AND_POSITION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS
			interface.getSimpleInteriorDetails = &GET_BUSINESS_HUB_DETAILS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS_BS
			interface.getSimpleInteriorDetailsPropertiesBS = &GET_BUSINESS_HUB_PROPERTIES_BS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_NAME
			interface.simpleInteriorFP_ReturnS_ParamID = &GET_BUSINESS_HUB_NAME
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_PROPERTY_ID
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_BUSINESS_HUB_PROPERTY_ID
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_GAME_TEXT
			interface.simpleInteriorFP_ReturnS_ParamID_gameTextID = &GET_BUSINESS_HUB_GAME_TEXT
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_SHOW_PIM_INVITE_OPTION
			interface.simpleInteriorFP_ReturnB_ParamID_tl63_AndBoolx2 = &SHOULD_BUSINESS_HUB_SHOW_PIM_INVITE_OPTION
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_SHOW_PIM_PROPERTY_MANAGEMENT_OPTION
			interface.simpleInteriorFP_ReturnB_ParamID_Bool_Andtl15 = &SHOULD_BUSINESS_HUB_SHOW_PIM_PROPERTY_MANAGEMENT_OPTION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX
			interface.getSimpleInteriorInsideBoundingBox = &GET_BUSINESS_HUB_INSIDE_BOUNDING_BOX
		BREAK
		CASE E_GET_OWNED_SIMPLE_INTERIOR_OF_TYPE
			interface.getOwnedSimpleInteriorOfType = &GET_OWNED_SMPL_INT_OF_TYPE_BUSINESS_HUB
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCES_COUNT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_BUSINESS_HUB_ENTRANCES_COUNT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_DATA
			interface.getSimpleInteriorEntranceData = &GET_BUSINESS_HUB_ENTRANCE_DATA
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INVITE_ENTRY_POINT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_BUSINESS_HUB_INVITE_ENTRY_POINT
		BREAK	
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_SCENE_DETAILS
			interface.getSimpleInteriorEntranceSceneDetails = &GET_BUSINESS_HUB_ENTRANCE_SCENE_DETAILS
		BREAK
		CASE E_SIMPLE_INTERIOR_LOAD_SECONDARY_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamID = &SHOULD_BUSINESS_HUB_LOAD_SECONDARY_INTERIOR
		BREAK
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_CORONA_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_BUSINESS_HUB_CORONA_BE_HIDDEN
		BREAK
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_LOCATE_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_BUSINESS_HUB_LOCATE_BE_HIDDEN
		BREAK
		CASE E_DOES_SIMPLE_INTERIOR_ALLOW_ENTRY_IN_A_TUTORIAL_SESSION
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_BUSINESS_HUB_ALLOW_ENTRY_IN_A_TUTORIAL_SESSION
		BREAK
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR_IN_VEHICLE
			interface.simpleInteriorFP_ReturnB_ParamPIDAndModel = &CAN_PLAYER_ENTER_BUSINESS_HUB_IN_VEHICLE
		BREAK
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPID_ID_AndInt = &CAN_PLAYER_ENTER_BUSINESS_HUB
		BREAK		
		CASE E_CAN_PLAYER_USE_SIMPLE_INTERIOR_ENTRY_IN_CURRENT_VEHICLE
			interface.canPlayerUseEntryInCurrentVeh = &CAN_PLAYER_USE_BUSINESS_HUB_ENTRY_IN_CURRENT_VEHICLE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_ENTRY
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_BUSINESS_HUB_REASON_FOR_BLOCKED_ENTRY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_EXIT
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_BUSINESS_HUB_REASON_FOR_BLOCKED_EXIT
		BREAK
		CASE E_CAN_PLAYER_INVITE_OTHERS_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_INVITE_OTHERS_TO_BUSINESS_HUB
		BREAK
		CASE E_CAN_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_BE_INVITED_TO_BUSINESS_HUB
		BREAK
		CASE E_CAN_LOCAL_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR_BY_PLAYER
			interface.intCanLocalPlayerBeInvitedToSimpleInteriorByPlayer = &CAN_LOCAL_PLAYER_BE_INVITED_TO_BUSINESS_HUB_BY_PLAYER
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_MENU_TITLE_AND_OPTIONS
			interface.simpleInteriorFPProc_ParamIDAndCustomMenu = &GET_BUSINESS_HUB_ENTRANCE_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_PROCESS_SIMPLE_INTERIOR_CUSTOM_ENTRANCE_MENU
			interface.processSimpleInteriorCustomEntranceMenu = &PROCESS_BUSINESS_HUB_CUSTOM_ENTRANCE_MENU
		BREAK
		CASE E_PROCESS_SIMPLE_INTERIOR_ENTRANCE_MENU
			interface.simpleInteriorFPProc_ParamIDAndEMenu = &PROCESS_BUSINESS_HUB_ENTRANCE_MENU
		BREAK
		CASE E_PROCESS_SIMPLE_INTERIOR_VEHICLE_ENTRANCE_MENU
			interface.simpleInteriorFPProc_ParamIDCustomMenuAndVehicle = &PROCESS_BUSINESS_HUB_VEHICLE_ENTRANCE_MENU
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_VEHICLE_ENTRANCE_MENU_TITLE_AND_OPTIONS
			interface.simpleInteriorFPProc_ParamIDAndCustomMenu = &GET_BUSINESS_HUB_VEHICLE_ENTRANCE_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_SELECTED_VEHICLE_FLOOR_OFFSET
			interface.simpleInteriorFPProc_ParamIDAndRefInt = &GET_BUSINESS_HUB_SELECTED_VEHICLE_FLOOR_OFFSET
		BREAK
		CASE E_SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_BUSINESS_HUB
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_KICK_OUT_REASON
			interface.getSimpleInteriorKickOutReason = &GET_BUSINESS_HUB_KICK_OUT_REASON
		BREAK
		CASE E_SET_SIMPLE_INTERIOR_ACCESS_BS
			interface.setSimpleInteriorAccessBS = &SET_BUSINESS_HUB_ACCESS_BS
		BREAK
		CASE E_SHOULD_OWNER_LEAVE_SIMPLE_INTERIOR_FOR_TRADE_IN
			interface.simpleInteriorFP_ReturnB_ParamID = &SHOULD_OWNER_LEAVE_BUSINESS_HUB_FOR_TRADE_IN
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_SPAWN_POINT
			interface.getSimpleInteriorSpawnPoint = &GET_BUSINESS_HUB_INTERIOR_SPAWN_POINT
		BREAK
		CASE E_GET_SI_CUSTOM_INTERIOR_WARP_PARAMS
			interface.getSimpleInteriorCustomInteriorWarpParams = &GET_BUSINESS_HUB_CUSTOM_INTERIOR_WARP_PARAMS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT
			interface.getSimpleInteriorOutsideSpawnPoint = &GET_BUSINESS_HUB_EXTERIOR_SPAWN_POINT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_BUSINESS_HUB_ASYNC_SPAWN_GRID_LOCATION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_INSIDE_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_BUSINESS_HUB_ASYNC_SPAWN_GRID_INSIDE_LOCATION
		BREAK
		CASE E_MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ACCESS
			interface.maintainLeaveentryVehIfNoAccess = &MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_BUSINESS_HUB_ACCESS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_TELE_IN_SVM_COORD
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_BUSINESS_HUB_TELEPORT_IN_SVM_COORD
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_EXIT_MENU_TITLE_AND_OPTIONS
			interface.getSimpleInteriorExitMenuTitleAndOptions = &GET_BUSINESS_HUB_EXIT_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_PROCESS_SIMPLE_INTERIOR_EXIT_MENU
			interface.bUseCustomExitMenuProcess = TRUE
			interface.processSimpleInteriorExitMenu = &PROCESS_BUSINESS_HUB_EXIT_MENU
		BREAK
		CASE E_CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_BUSINESS_HUB
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_MAX_BLIPS
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_BUSINESS_HUB_MAX_BLIPS
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_BE_BLIPPED_THIS_FRAME
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_BUSINESS_HUB_BE_BLIPPED_THIS_FRAME
		BREAK
		CASE E_IS_SIMPLE_INTERIOR_BLIP_SHORT_RANGE
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &IS_BUSINESS_HUB_BLIP_SHORT_RANGE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_SPRITE
			interface.simpleInteriorFP_ReturnEBS_ParamIDAndInt = &GET_BUSINESS_HUB_BLIP_SPRITE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COORDS
			interface.simpleInteriorFP_ReturnV_ParamIDAndInt = &GET_BUSINESS_HUB_BLIP_COORDS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_BLIP_EXTRA_FUNCTIONALITY
			interface.maintainSimpleInteriorBlipExtraFunctionality = &MAINTAIN_BUSINESS_HUB_BLIP_EXTRA_FUNCTIONALITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_PRIORITY
			interface.getSimpleInteriorBlipPriority	= &GET_BUSINESS_HUB_BLIP_PRIORITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COLOUR
			interface.simpleInteriorFP_ReturnI_ParamIDAndInt = &GET_BUSINESS_HUB_BLIP_COLOUR
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_MAP_MIDPOINT
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_BUSINESS_HUB_MAP_MIDPOINT
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_MAINTAIN_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.maintainSimpleInteriorOutsideGameplayModifiers = &MAINTAIN_BUSINESS_HUB_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_BUSINESS_HUB_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &MAINTAIN_BUSINESS_HUB_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_BUSINESS_HUB_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_DOES_SIMPLE_INTERIOR_BLOCK_WEAPONS
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_BUSINESS_HUB_BLOCK_WEAPONS
		BREAK
		CASE E_DOES_SIMPLE_INTERIOR_RESTRICT_PLAYER_MOVEMENT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_BUSINESS_HUB_RESTRICT_PLAYER_MOVEMENT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_CAR_GENS_BLOCK_AREA
			interface.getSimpleInteriorCarGensBlockArea = &GET_BUSINESS_HUB_CAR_GENS_BLOCK_AREA
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_MODEL_HIDE
			interface.getSimpleInteriorOutsideModelHide = &GET_BUSINESS_HUB_OUTSIDE_MODEL_HIDE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_SCENARIO_PED_REMOVAL_SPHERE
			interface.getSimpleInteriorScenarioPedRemovalSphere = &GET_BUSINESS_HUB_SCENARIO_PED_REMOVAL_SPHERE
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_TRIGGER_EXIT_IN_CAR
			interface.simpleInteriorFP_ReturnB_ParamID_PID_AndB = &SHOULD_TRIGGER_EXIT_IN_CAR_FOR_BUSINESS_HUB
		BREAK
		//================================= EXTERIOR ENTER CUTSCENE
		CASE E_EXT_CUTSCENE_ENTER_ASSETS_LOADED
			interface.simpleInteriorFP_ReturnB_ParamIDAndEA = &EXT_CUTSCENE_BUSINESS_HUB_LOAD_ASSETS
		BREAK
		CASE E_EXT_CUTSCENE_ENTER_ASSETS_CREATED
			interface.simpleInteriorFP_ReturnB_ParamIDAndEA = &EXT_CUTSCENE_BUSINESS_HUB_CREATE_ASSETS
		BREAK
		CASE E_EXT_CUTSCENE_ENTER_CUTSCENE_MAINTAIN
			interface.simpleInteriorFP_ReturnB_ParamIDAndEA = &EXT_CUTSCENE_BUSINESS_HUB_MAINTAIN
		BREAK
		CASE E_EXT_CUTSCENE_ENTER_CUTSCENE_TRANSITION_TO_MENU
			interface.simpleInteriorFP_ReturnB_ParamIDAndEA= &EXT_CUTSCENE_BUSINESS_HUB_TRANSITION_TO_MENU
		BREAK
		CASE E_EXT_CUTSCENE_ENTER_CLEANUP
			interface.extCutsceneEnterCleanup = &EXT_CUTSCENE_BUSINESS_HUB_CLEANUP
		BREAK
		//================================= INTERIOR EXIT CUTSCENE
		CASE E_INT_CUTSCENE_EXIT_SHOULD_PLAY
			interface.simpleInteriorFP_ReturnB_ParamIDAndEA = &INT_CUTSCENE_BUSINESS_HUB_EXIT_SHOULD_PLAY
		BREAK
		CASE E_INT_CUTSCENE_EXIT_ASSETS_LOADED
			interface.simpleInteriorFP_ReturnB_ParamIDAndEA = &INT_CUTSCENE_BUSINESS_HUB_EXIT_ASSETS_LOADED
		BREAK
		CASE E_INT_CUTSCENE_EXIT_ASSETS_CREATED
			interface.simpleInteriorFP_ReturnB_ParamIDAndEA = &INT_CUTSCENE_BUSINESS_HUB_EXIT_ASSETS_CREATED
		BREAK
		CASE E_INT_CUTSCENE_EXIT_CUTSCENE_MAINTAIN
			interface.simpleInteriorFPProc_ParamIDAndEA = &INT_CUTSCENE_BUSINESS_HUB_EXIT_MAINTAIN
		BREAK
		CASE E_INT_CUTSCENE_EXIT_CLEANUP
			interface.simpleInteriorFPProc_ParamIDAndEA = &INT_CUTSCENE_BUSINESS_HUB_EXIT_CLEANUP
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_SML_INT_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
			interface.DoCustomActionForStateWarpToSpawnPoint = &BUSINESS_HUB_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
		BREAK
		CASE E_DO_SIMPLE_INTERIOR_PRE_EXIT_CLEANUP
			interface.simpleInteriorFPProc_ParamID = &DO_BUSINESS_HUB_PRE_EXIT_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC
