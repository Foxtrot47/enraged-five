//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_CLUB_MUSIC.sch																						//
// Description: Header file containing functionality for club music.													//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		30/03/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_club_music_common.sch"
USING "fm_maintain_strand_mission.sch"
#ENDIF

#IF IS_DEBUG_BUILD
USING "net_club_music_debug.sch"
#ENDIF

#IF FEATURE_FIXER
USING "net_dr_dre_appearance_controller.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ GETTER/SETTER ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING GET_CLUB_MUSIC_SERVER_BIT_NAME(INT iBit)
	SWITCH iBit
		CASE CLUB_MUSIC_SERVER_BS_FREEMODE_SELECTED_DJ			RETURN "CLUB_MUSIC_SERVER_BS_FREEMODE_SELECTED_DJ"
		CASE CLUB_MUSIC_SERVER_BS_START_FROM_BEGINING_OF_SET	RETURN "CLUB_MUSIC_SERVER_BS_START_FROM_BEGINING_OF_SET"
		CASE CLUB_MUSIC_SERVER_BS_START_DJ_SWITCH_SCENE			RETURN "CLUB_MUSIC_SERVER_BS_START_DJ_SWITCH_SCENE"
		CASE CLUB_MUSIC_SERVER_BS_USING_PATH_B					RETURN "CLUB_MUSIC_SERVER_BS_USING_PATH_B"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

DEBUGONLY FUNC STRING GET_CLUB_MUSIC_LOCAL_BIT_NAME(INT iBit)
	SWITCH iBit
		CASE 0				RETURN "0"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC
#ENDIF

/// PURPOSE:
///    Set server bit set
PROC SET_CLUB_MUSIC_SERVER_BIT(DJ_SERVER_DATA &DJServerData, INT iBit)
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(DJServerData.iBS, iBit)
		SET_BIT(DJServerData.iBS, iBit)
		PRINTLN("[CLUB_MUSIC] SET_CLUB_MUSIC_SERVER_BIT bit ", GET_CLUB_MUSIC_SERVER_BIT_NAME(iBit) , " TRUE")
	ENDIF
ENDPROC

PROC CLEAR_CLUB_MUSIC_SERVER_BIT(DJ_SERVER_DATA &DJServerData, INT iBit)
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(DJServerData.iBS, iBit)
		CLEAR_BIT(DJServerData.iBS, iBit)
		PRINTLN("[CLUB_MUSIC] CLEAR_CLUB_MUSIC_SERVER_BIT bit ", GET_CLUB_MUSIC_SERVER_BIT_NAME(iBit) , " FALSE")
	ENDIF
ENDPROC

FUNC BOOL IS_CLUB_MUSIC_SERVER_BIT_SET(DJ_SERVER_DATA &DJServerData, INT iBit)
	RETURN IS_BIT_SET(DJServerData.iBS, iBit)
ENDFUNC

/// PURPOSE:
///    Set local bit set
PROC SET_CLUB_MUSIC_LOCAL_BIT(DJ_LOCAL_DATA &DJLocalData, INT iBit)
	IF NOT IS_BIT_SET(DJLocalData.iBS, iBit)
		SET_BIT(DJLocalData.iBS, iBit)
		PRINTLN("[CLUB_MUSIC] SET_CLUB_MUSIC_LOCAL_BIT bit ", GET_CLUB_MUSIC_LOCAL_BIT_NAME(iBit) , " TRUE")
	ENDIF
ENDPROC

PROC CLEAR_CLUB_MUSIC_LOCAL_BIT(DJ_LOCAL_DATA &DJLocalData, INT iBit)
	IF IS_BIT_SET(DJLocalData.iBS, iBit)
		CLEAR_BIT(DJLocalData.iBS, iBit)
		PRINTLN("[CLUB_MUSIC] CLEAR_CLUB_MUSIC_LOCAL_BIT bit ", GET_CLUB_MUSIC_LOCAL_BIT_NAME(iBit) , " FALSE")
	ENDIF
ENDPROC

FUNC BOOL IS_CLUB_MUSIC_LOCAL_BIT_SET(DJ_LOCAL_DATA &DJLocalData, INT iBit)
	RETURN IS_BIT_SET(DJLocalData.iBS, iBit)
ENDFUNC

/// PURPOSE:
///    Check if there is only one type club location exists in freemode
/// RETURNS:
///    
FUNC BOOL IS_THIS_CLUB_MUSIC_DATA_PUBLIC(CLUB_LOCATIONS eClubLocation)
	SWITCH eClubLocation
		CASE CLUB_LOCATION_ISLAND 
		#IF FEATURE_CASINO_NIGHTCLUB
		CASE CLUB_LOCATION_CASINO_NIGHTCLUB
		#ENDIF
		#IF FEATURE_FIXER
		CASE CLUB_LOCATION_MUSIC_STUDIO
		#ENDIF
			RETURN TRUE
	ENDSWITCH	
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_CLUB_DJ_STATE_NAME(CLUB_DJ_STATE eState)
	SWITCH eState
		CASE CLUB_DJ_INIT_DATAFILE_STATE		RETURN "CLUB_DJ_INIT_DATAFILE_STATE"
		CASE CLUB_DJ_INIT_STATE					RETURN "CLUB_DJ_INIT_STATE"
		CASE CLUB_DJ_UPDATE_STATE				RETURN "CLUB_DJ_UPDATE_STATE"
		CASE CLUB_DJ_SET_END_STATE				RETURN "CLUB_DJ_SET_END_STATE"
		CASE CLUB_DJ_SET_PAUSE_MUSIC_STATE		RETURN "CLUB_DJ_SET_PAUSE_MUSIC_STATE"
		CASE CLUB_DJ_CLEANUP					RETURN "CLUB_DJ_CLEANUP"
	ENDSWITCH
	
	RETURN "INVALID STATE"
ENDFUNC

PROC SET_CLUB_DJ_STATE(DJ_LOCAL_DATA &DJLocalData, CLUB_DJ_STATE eState)
	IF DJLocalData.eDJstate != eState
		PRINTLN("[CLUB_MUSIC] SET_CLUB_DJ_STATE from ", GET_CLUB_DJ_STATE_NAME(DJLocalData.eDJstate), " to ", GET_CLUB_DJ_STATE_NAME(eState))
		DJLocalData.eDJstate = eState
	ENDIF	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ EMITTERS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets the active state of a clubs static emitters.
/// PARAMS:
///    eClubLocation - Club location.
///    bEnable - Active state.
PROC SET_CLUB_STATIC_EMITTERS_ACTIVE_STATE(CLUB_LOCATIONS eClubLocation, BOOL bEnable)
	INT iEmitter
	STRING sEmitterName = ""
	REPEAT GET_CLUB_MAX_STATIC_EMITTERS(eClubLocation) iEmitter
		sEmitterName = GET_CLUB_STATIC_EMITTER_NAME(eClubLocation, iEmitter)
		IF NOT IS_STRING_NULL_OR_EMPTY(sEmitterName)
			SET_STATIC_EMITTER_ENABLED(sEmitterName, bEnable)
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Sets a radio station on a clubs static emitters.
/// PARAMS:
///    eClubLocation - Club location.
///    sRadioStationName - Radio station name to set on static emitters.
PROC SET_STATIC_EMITTERS_RADIO_STATION(CLUB_LOCATIONS eClubLocation, STRING sRadioStationName)
	INT iEmitter
	STRING sEmitterName = ""
	REPEAT GET_CLUB_MAX_STATIC_EMITTERS(eClubLocation) iEmitter
		sEmitterName = GET_CLUB_STATIC_EMITTER_NAME(eClubLocation, iEmitter)
		IF NOT IS_STRING_NULL_OR_EMPTY(sEmitterName)
			SET_EMITTER_RADIO_STATION(sEmitterName, sRadioStationName)
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ AUDIO SCENES ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets the clubs audio scene active state.
/// PARAMS:
///    eClubLocation - Club location.
///    bEnable - Active state.
PROC SET_AUDIO_SCENE_ACTIVE_STATE(CLUB_LOCATIONS eClubLocation, BOOL bEnable)
	STRING sAudioScene = GET_CLUB_AUDIO_SCENE_NAME(eClubLocation)
	IF IS_STRING_NULL_OR_EMPTY(sAudioScene)
		EXIT
	ENDIF
	IF (bEnable)
		IF NOT IS_AUDIO_SCENE_ACTIVE(sAudioScene)
			START_AUDIO_SCENE(sAudioScene)
		ENDIF
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE(sAudioScene)
			STOP_AUDIO_SCENE(sAudioScene)
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ AUDIO ZONE ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets the clubs audio zone active state.
/// PARAMS:
///    eClubLocation - Club location.
///    bEnable - Active zone state.
PROC SET_AUDIO_ZONE_ACTIVE_STATE(CLUB_LOCATIONS eClubLocation, BOOL bEnable)
	STRING sAudioZone = GET_CLUB_AUDIO_ZONE_NAME(eClubLocation)
	IF IS_STRING_NULL_OR_EMPTY(sAudioZone)
		EXIT
	ENDIF
	IF (bEnable)
		IF NOT IS_AMBIENT_ZONE_ENABLED(sAudioZone)
			SET_AMBIENT_ZONE_STATE(sAudioZone, TRUE, TRUE)
		ENDIF
	ELSE
		IF IS_AMBIENT_ZONE_ENABLED(sAudioZone)
			SET_AMBIENT_ZONE_STATE(sAudioZone, FALSE, TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_TO_VIEW_DJ_SWITCH_OVER_SCENE()
	IF NOT g_bInitPedsCreated
		PRINTLN("[CLUB_MUSIC] IS_SAFE_TO_VIEW_DJ_SWITCH_OVER_SCENE g_bInitPedsCreated false")
		RETURN FALSE
	ENDIF
	
	IF g_sHeistIslandFlow.Stage = HIFS_CUTSCENE
		PRINTLN("[CLUB_MUSIC] IS_SAFE_TO_VIEW_DJ_SWITCH_OVER_SCENE g_sHeistIslandFlow.Stage = HIFS_CUTSCENE")
		RETURN FALSE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
		IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].propertyDetails.iBSFour, PROPERTY_BROADCAST_BS4_CASINO_NIGHTCLUB_WATCHING_MOCAP)
				PRINTLN("[CLUB_MUSIC] IS_SAFE_TO_VIEW_DJ_SWITCH_OVER_SCENE gang boss PROPERTY_BROADCAST_BS4_CASINO_NIGHTCLUB_WATCHING_MOCAP true")
				RETURN FALSE
			ENDIF	
		ENDIF
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("[CLUB_MUSIC] IS_SAFE_TO_VIEW_DJ_SWITCH_OVER_SCENE IS_CUTSCENE_PLAYING")
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SAFE_TO_VIEW_DRE_STATE_CHANGE_SCENE()

	IF NOT g_bInitPedsCreated
		PRINTLN("[CLUB_MUSIC] IS_SAFE_TO_VIEW_DRE_STATE_CHANGE_SCENE g_bInitPedsCreated false")
		RETURN FALSE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("[CLUB_MUSIC] IS_SAFE_TO_VIEW_DRE_STATE_CHANGE_SCENE IS_CUTSCENE_PLAYING")
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ EVENTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC PROCESS_CLUB_MUSIC_DJ_SWITCH_SCENE_EVENT(CLUB_LOCATIONS eClubLocation, SCRIPT_EVENT_DATA_CLUB_MUSIC_DJ_SWITCH_SCENE eDJSwitchScene, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	IF eDJSwitchScene.bStartScene
		IF IS_SAFE_TO_VIEW_DJ_SWITCH_OVER_SCENE()
			SET_CLUB_STATIC_EMITTERS_ACTIVE_STATE(eClubLocation, FALSE)
			g_clubMusicData.eNextDJ = eDJSwitchScene.eNextDJ
			PLAYSTATS_DJ_USAGE(ENUM_TO_INT(g_clubMusicData.eNextDJ), ENUM_TO_INT(eClubLocation) + 1)
			g_clubMusicData.iCurrentTrackTimeMS = 0
			g_clubMusicData.iCurrentMixTimeMS = 0
			SET_CLUB_DJ_STATE(DJLocalData, CLUB_DJ_SET_END_STATE)
			SET_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_START_DJ_SWITCH_SCENE)
			PRINTLN("[CLUB_MUSIC] PROCESS_CLUB_MUSIC_DJ_SWITCH_SCENE_EVENT - bStartScene: TRUE - Next DJ: ", g_clubMusicData.eNextDJ)
		ELSE
			PRINTLN("[CLUB_MUSIC] PROCESS_CLUB_MUSIC_DJ_SWITCH_SCENE_EVENT - bStartScene: TRUE IS_SAFE_TO_VIEW_DJ_SWITCH_OVER_SCENE false skip cutscene")
		ENDIF	
	ELSE
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
			DJLocalData.sRadioStationName 	= GET_CLUB_RADIO_STATION_NAME(eClubLocation, DJServerData.eRadioStation)
			g_clubMusicData.iCurrentTrackTimeMS = 0
			g_clubMusicData.iCurrentMixTimeMS = 0
			DJLocalData.bMusicPlaying = FALSE
			BROADCAST_CLUB_MUSIC_UPDATE_DJ_PEDS_AFTER_SWITCH_SCENE(TRUE)
			PRINTLN("[CLUB_MUSIC] PROCESS_CLUB_MUSIC_DJ_SWITCH_SCENE_EVENT - bStartScene: FALSE")
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///    Processes the club music events.
/// PARAMS:
///    DJServerData - Server data to query.
///    DJLocalData - Local data to set.
///    eEventType - The event type to process.
///    iEventID - The event ID.
PROC PROCESS_CLUB_MUSIC_EVENTS(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData, SCRIPTED_EVENT_TYPES eEventType, INT iEventID)
	
	SWITCH eEventType
		CASE SCRIPT_EVENT_CLUB_MUSIC_INTENSITY
			SCRIPT_EVENT_DATA_CLUB_MUSIC_INTENSITY intensityEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, intensityEvent, SIZE_OF(intensityEvent))
				IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT() AND NETWORK_IS_PLAYER_A_PARTICIPANT(intensityEvent.Details.FromPlayerIndex)
					IF (DJLocalData.eMusicIntensity != intensityEvent.eClubIntensity)
						DJLocalData.eMusicIntensity = intensityEvent.eClubIntensity
						#IF IS_DEBUG_BUILD
						PRINTLN("[CLUB_MUSIC] PROCESS_CLUB_MUSIC_EVENTS - Received Event: SCRIPT_EVENT_CLUB_MUSIC_INTENSITY - New intensity: ", GET_CLUB_MUSIC_INTENSITY_NAME(intensityEvent.eClubIntensity))
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPT_EVENT_CLUB_MUSIC_NEXT_INTENSITY
			SCRIPT_EVENT_DATA_CLUB_MUSIC_NEXT_INTENSITY nextIntensityEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, nextIntensityEvent, SIZE_OF(nextIntensityEvent))
				IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT() AND NETWORK_IS_PLAYER_A_PARTICIPANT(nextIntensityEvent.Details.FromPlayerIndex)
					IF (DJLocalData.eNextMusicIntensity != nextIntensityEvent.eNextClubIntensity)
						DJLocalData.eNextMusicIntensity = nextIntensityEvent.eNextClubIntensity
						#IF IS_DEBUG_BUILD
						PRINTLN("[CLUB_MUSIC] PROCESS_CLUB_MUSIC_EVENTS - Received Event: SCRIPT_EVENT_CLUB_MUSIC_NEXT_INTENSITY - Next intensity: ", GET_CLUB_MUSIC_INTENSITY_NAME(nextIntensityEvent.eNextClubIntensity))
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPT_EVENT_CLUB_MUSIC_REQUEST_INTENSITY
			SCRIPT_EVENT_DATA_CLUB_MUSIC_REQUEST_INTENSITY requestIntensityEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, requestIntensityEvent, SIZE_OF(requestIntensityEvent))
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT() AND NETWORK_IS_PLAYER_A_PARTICIPANT(requestIntensityEvent.Details.FromPlayerIndex)
					// A player has requested the current music intensity from the Host. Send return event with intensity.
					BROADCAST_CLUB_MUSIC_SEND_INTENSITY_TO_PLAYER(DJServerData.eMusicIntensity, requestIntensityEvent.Details.FromPlayerIndex)
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPT_EVENT_CLUB_MUSIC_DJ_SWITCH_SCENE
			SCRIPT_EVENT_DATA_CLUB_MUSIC_DJ_SWITCH_SCENE eDJSwitchScene
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, eDJSwitchScene, SIZE_OF(eDJSwitchScene))
				PROCESS_CLUB_MUSIC_DJ_SWITCH_SCENE_EVENT(eClubLocation, eDJSwitchScene, DJServerData, DJLocalData)
			ENDIF		
		BREAK
		#IF IS_DEBUG_BUILD
		CASE SCRIPT_EVENT_DEBUG_CLUB_MUSIC_RESET_TRACK
			DEBUG_SCRIPT_EVENT_DATA_CLUB_MUSIC_RESET_TRACK debugResetTrackEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, debugResetTrackEvent, SIZE_OF(debugResetTrackEvent))
				IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT() AND NETWORK_IS_PLAYER_A_PARTICIPANT(debugResetTrackEvent.Details.FromPlayerIndex)
					// If the DJ has changed the flow will have already updated it
					// This forces an update because the host has debug updated something else e.g the track.
					IF (DJLocalData.eDJ = debugResetTrackEvent.eDJ)
						DJLocalData.bMusicPlaying = FALSE
						PRINTLN("[CLUB_MUSIC] PROCESS_CLUB_MUSIC_EVENTS - Received Event: SCRIPT_EVENT_DEBUG_CLUB_MUSIC_RESET_TRACK - Debug updating track.")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		#ENDIF
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Process club music events.
/// PARAMS:
///    DJServerData - Server data to query.
///    DJLocalData - Local data to set.
PROC PROCESS_EVENTS(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	
	INT iEventID
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
	    ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
	   	
	    SWITCH ThisScriptEvent
			CASE EVENT_NETWORK_SCRIPT_EVENT
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Details, SIZE_OF(Details))
				SWITCH Details.Type
					CASE SCRIPT_EVENT_CLUB_MUSIC_INTENSITY
						IF g_sBlockedEvents.bSCRIPT_EVENT_CLUB_MUSIC_INTENSITY
							EXIT
						ENDIF
						PROCESS_CLUB_MUSIC_EVENTS(eClubLocation, DJServerData, DJLocalData, Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_CLUB_MUSIC_NEXT_INTENSITY
						IF g_sBlockedEvents.bSCRIPT_EVENT_CLUB_MUSIC_NEXT_INTENSITY
							EXIT
						ENDIF
						PROCESS_CLUB_MUSIC_EVENTS(eClubLocation, DJServerData, DJLocalData, Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_CLUB_MUSIC_REQUEST_INTENSITY
						IF g_sBlockedEvents.bSCRIPT_EVENT_CLUB_MUSIC_REQUEST_INTENSITY
							EXIT
						ENDIF
						PROCESS_CLUB_MUSIC_EVENTS(eClubLocation, DJServerData, DJLocalData, Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_CLUB_MUSIC_DJ_SWITCH_SCENE
						IF NOT g_sBlockedEvents.bSCRIPT_EVENT_CLUB_MUSIC_DJ_SWITCH_SCENE
							PROCESS_CLUB_MUSIC_EVENTS(eClubLocation, DJServerData, DJLocalData, Details.Type, iEventID)
						ENDIF
					BREAK
					CASE SCRIPT_EVENT_MUSIC_STUDIO_CHANGE_DRE_WORK_STATE
						IF g_sBlockedEvents.bSCRIPT_EVENT_MUSIC_STUDIO_CHANGE_DRE_WORK_STATE
							EXIT
						ENDIF
						PROCESS_CLUB_MUSIC_EVENTS(eClubLocation, DJServerData, DJLocalData, Details.Type, iEventID)
					BREAK
					#IF IS_DEBUG_BUILD
					CASE SCRIPT_EVENT_DEBUG_CLUB_MUSIC_RESET_TRACK
						IF g_sBlockedEvents.bSCRIPT_EVENT_DEBUG_CLUB_MUSIC_RESET_TRACK
							EXIT
						ENDIF
						PROCESS_CLUB_MUSIC_EVENTS(eClubLocation, DJServerData, DJLocalData, Details.Type, iEventID)
					BREAK
					#ENDIF
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDREPEAT
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PLAY MUSIC ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Should club music paused
FUNC BOOL SHOULD_PAUSE_CLUB_MUSIC(CLUB_LOCATIONS eClubLocation, DJ_LOCAL_DATA &DJLocalData, DJ_SERVER_DATA &DJServerData)
	IF eClubLocation = CLUB_LOCATION_ISLAND
		IF GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = IHV_SCOPING_INTRO
		AND IS_CUTSCENE_PLAYING()
			IF DJLocalData.bMusicPlaying 
				SET_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_START_DJ_SWITCH_SCENE)
				RETURN TRUE
			ENDIF	
		ENDIF
	ENDIF
	
	IF eClubLocation = CLUB_LOCATION_MUSIC_STUDIO
		IF IS_CUTSCENE_PLAYING()
			RETURN TRUE
		ENDIF
		
		 IF NOT g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE 
		 	RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	AND IS_CLUB_MUSIC_SERVER_BIT_SET(DJServerData, CLUB_MUSIC_SERVER_BS_START_DJ_SWITCH_SCENE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the radio station to play on a synced offset.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to query.
///    DJLocalData - Local data to set.
PROC PLAY_CLUB_MUSIC(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData, BOOL bMuted = false)
	
	// Note: Always start the radio station on track one.
	//       If the starting mix time is greater than the duration of track one, it will play track two etc.
	
	DJLocalData.sRadioStationName 	= GET_CLUB_RADIO_STATION_NAME(eClubLocation, DJServerData.eRadioStation)
	STRING sTrackName 				= GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, DJServerData.eRadioStation, 1)
	INT iCurrentMixTimeMS			= GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(DJServerData.stMusicTimer, DEFAULT, TRUE)
	#IF IS_DEBUG_BUILD
	PRINT_CLUB_MUSIC_DATA(eClubLocation, DJServerData, TRUE)
	#ENDIF
	
	FREEZE_RADIO_STATION(DJLocalData.sRadioStationName)
	
	SET_CLUB_STATIC_EMITTERS_ACTIVE_STATE(eClubLocation, !bMuted)
	
	SET_RADIO_TRACK_WITH_START_OFFSET(DJLocalData.sRadioStationName, sTrackName, iCurrentMixTimeMS)
	UNFREEZE_RADIO_STATION(DJLocalData.sRadioStationName)
	
	SET_CLUB_STATIC_EMITTERS_ACTIVE_STATE(eClubLocation, TRUE)
	SET_STATIC_EMITTERS_RADIO_STATION(eClubLocation, DJLocalData.sRadioStationName)
	SET_AUDIO_ZONE_ACTIVE_STATE(eClubLocation, !bMuted)
	SET_AUDIO_SCENE_ACTIVE_STATE(eClubLocation, !bMuted)
	
	
	DJLocalData.eMusicIntensity = DJServerData.eMusicIntensity
	DJLocalData.eNextMusicIntensity = DJServerData.eNextMusicIntensity	
	g_clubMusicData.eClubMusicIntensity = DJServerData.eMusicIntensity
	g_clubMusicData.eNextClubMusicIntensity = DJServerData.eNextMusicIntensity


	DJLocalData.eDJ = DJServerData.eDJ
	g_clubMusicData.eActiveDJ = DJLocalData.eDJ
	g_sMusicStudioDreWorkData.bWasSequenceSelected = TRUE
	DJLocalData.bMusicPlaying = TRUE	

	
	CLEAR_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_START_DJ_SWITCH_SCENE)
	PRINTLN("[CLUB_MUSIC] PLAY_CLUB_MUSIC DJLocalData.bMusicPlaying = TRUE - DJ = ", g_clubMusicData.eActiveDJ, " next DJ: ", g_clubMusicData.eNextDJ)
ENDPROC

FUNC BOOL SHOULD_SEND_MUSIC_DATA_TO_FREEMODE_HOST(CLUB_LOCATIONS eClubLocation)
	IF eClubLocation = CLUB_LOCATION_ISLAND
		IF GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = IHV_SCOPING_INTRO
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CLEANUP ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Cleans up any globals associated with the club music.
PROC CLEANUP_CLUB_MUSIC_GLOBALS()
	DEBUG_PRINTCALLSTACK()
	g_iTimeUntilPedDanceTransitionSecs = -1
	g_clubMusicData.eClubMusicIntensity = CLUB_MUSIC_INTENSITY_NULL
	g_clubMusicData.eNextClubMusicIntensity = CLUB_MUSIC_INTENSITY_NULL
	g_clubMusicData.eNextDJ = CLUB_DJ_NULL
	g_clubMusicData.eActiveDJ = CLUB_DJ_NULL
ENDPROC

PROC CLEANUP_PED_DATAFILE(CLUB_LOCATIONS eClubLocation, CLUB_DJS eClubDJ)
	IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
		PRINTLN("[CLUB_MUSIC] CLEANUP_PED_DATAFILE not cleaning up data file in warp transition")
		EXIT
	ENDIF
	
	TEXT_LABEL_63 sDataFileName
	IF DOES_CLUB_LOCATION_USE_DATAFILE(eClubLocation, eClubDJ, sDataFileName)
		CLEANUP_ADDITIONAL_LOCAL_DATA_FILE()
		PRINTLN("[CLUB_MUSIC] CLEANUP_PED_DATAFILE")
	ENDIF	
ENDPROC

PROC SEND_CLUB_MUSIC_DATA_TO_FREEMODE_HOST(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData)
	IF NOT IS_THIS_CLUB_MUSIC_DATA_PUBLIC(eClubLocation)
		EXIT
	ENDIF

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[CLUB_MUSIC] SEND_CLUB_MUSIC_DATA_TO_FREEMODE_HOST we no longer in MP did player pull the plug ?")
		EXIT
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		EXIT
	ENDIF
	
	IF NETWORK_GET_NUM_PARTICIPANTS() > 1
		EXIT
	ENDIF
	
	// We are expecting the last participant to be the host
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()	
		PRINTLN("[CLUB_MUSIC] SEND_CLUB_MUSIC_DATA_TO_FREEMODE_HOST number of participants: ", NETWORK_GET_NUM_PARTICIPANTS(), " and I'm not host what is going on ??")
		EXIT
	ENDIF
	
	// We are leaving and no one else is in club clear set end flag
	CLEAR_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_FREEMODE_SELECTED_DJ)
	CLEAR_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_START_FROM_BEGINING_OF_SET)
	
	IF SHOULD_SEND_MUSIC_DATA_TO_FREEMODE_HOST(eClubLocation)
		BROADCAST_DATA_DJ_CLUB_MUSIC_DATA(eClubLocation, DJServerData)	
	ENDIF	
ENDPROC

/// PURPOSE:
///    Cleans up the club music and associated data.
/// PARAMS:
///    eClubLocation - Club location.
PROC CLEANUP_CLUB_MUSIC(CLUB_LOCATIONS eClubLocation, DJ_LOCAL_DATA &DJLocalData, DJ_SERVER_DATA &DJServerData)
	SEND_CLUB_MUSIC_DATA_TO_FREEMODE_HOST(eClubLocation, DJServerData)
	SET_CLUB_STATIC_EMITTERS_ACTIVE_STATE(eClubLocation, FALSE)
	SET_AUDIO_ZONE_ACTIVE_STATE(eClubLocation, FALSE)
	CLEANUP_CLUB_MUSIC_GLOBALS()
	CLEANUP_PED_DATAFILE(eClubLocation, DJLocalData.eDJ)
	SET_AUDIO_SCENE_ACTIVE_STATE(eClubLocation, FALSE)
	DJ_LOCAL_DATA resetLocalData
	DJLocalData = resetLocalData
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISE ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Determine if we need to load datafile and return true if datafile is loaded
FUNC BOOL HAS_DJ_DATAFILE_LOADED(CLUB_LOCATIONS eClubLocation, CLUB_DJS eClubDJ)
	TEXT_LABEL_63 sDataFileName
	IF DOES_CLUB_LOCATION_USE_DATAFILE(eClubLocation, eClubDJ, sDataFileName)
		CLEANUP_ADDITIONAL_LOCAL_DATA_FILE()
		PRINTLN("[CLUB_MUSIC] HAS_DJ_DATAFILE_LOADED sDataFileName: ", sDataFileName)
		RETURN DATAFILE_LOAD_OFFLINE_UGC_FOR_ADDITIONAL_DATA_FILE(0, sDataFileName)
	ENDIF
	RETURN TRUE
ENDFUNC 

/// PURPOSE:
///    Sets the clubs DJ
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to set.
PROC SET_CLUB_MUSIC_DJ(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData)
	
	// Set DJ
	INT iDJ 	= 0
	INT iMaxDJs	= GET_CLUB_MAX_DJS(eClubLocation)
	
	IF NOT IS_THIS_CLUB_MUSIC_DATA_PUBLIC(eClubLocation)
		IF (iMaxDJs > 1)
			iDJ = GET_RANDOM_INT_IN_RANGE(0, iMaxDJs)
		ENDIF
	ENDIF
	
	#IF FEATURE_FIXER
	IF eClubLocation = CLUB_LOCATION_MUSIC_STUDIO
		IF SHOULD_PLAY_DRUMS_SEQUENCE()
			SET_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_USING_PATH_B)
		ELSE
			CLEAR_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_USING_PATH_B)
		ENDIF
		
		SET_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_MUSIC_STUDIO_TRACK_SELECTED)
	ENDIF
	#ENDIF
	//needs to be determined AFTER deciding on drums/vocals
	DJServerData.eDJ = GET_CLUB_DJ(DJServerData, eClubLocation, iDJ)
	
	
	
	PRINTLN("[CLUB_MUSIC] SET_CLUB_MUSIC_DJ to: ", GET_CLUB_DJ_NAME(DJServerData.eDJ))
	
ENDPROC

/// PURPOSE:
///    Check we we should start the set from begining
FUNC BOOL SHOULD_START_CLUB_SET_FROM_BEGINING(CLUB_LOCATIONS eClubLocation)
	IF IS_CLUB_MUSIC_SERVER_BIT_SET(GlobalServerBD_BlockB.DJServerData[ENUM_TO_INT(eClubLocation)], CLUB_MUSIC_SERVER_BS_FREEMODE_SELECTED_DJ)
	AND IS_CLUB_MUSIC_SERVER_BIT_SET(GlobalServerBD_BlockB.DJServerData[ENUM_TO_INT(eClubLocation)], CLUB_MUSIC_SERVER_BS_START_FROM_BEGINING_OF_SET)
		RETURN TRUE
	ENDIF
	
	IF eClubLocation = CLUB_LOCATION_ISLAND
		IF GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = IHV_SCOPING_INTRO
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Host sets all of the club music data on initialise.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to set.
PROC SET_CLUB_MUSIC_DATA(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData)
	// Set radio station
	DJServerData.eRadioStation = GET_CLUB_RADIO_STATION(eClubLocation, DJServerData.eDJ)
	
	BOOL bStartFromBegining = FALSE
	
	IF SHOULD_START_CLUB_SET_FROM_BEGINING(eClubLocation)
		bStartFromBegining = TRUE
	ENDIF
	
	IF NOT bStartFromBegining
		// Set track
		IF eClubLocation = CLUB_LOCATION_ISLAND
			// We always want to start on first half of set
			DJServerData.iMusicTrack = GET_RANDOM_INT_IN_RANGE(1, GET_CLUB_RADIO_STATION_MAX_TRACKS(eClubLocation, DJServerData.eRadioStation) / 2)
		ELSE
			DJServerData.iMusicTrack = GET_RANDOM_INT_IN_RANGE(1, GET_CLUB_RADIO_STATION_MAX_TRACKS(eClubLocation, DJServerData.eRadioStation))
		ENDIF
		
		// Set track duration in mix time
		DJServerData.iTrackDurationMixTimeMS = GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(eClubLocation, DJServerData)
		
		// Set starting time of track somewhere in middle - not at start or end of track
		INT iTrackDurationQuarterMS = GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack)/4
		DJServerData.iStartingMixTimeMS = GET_CLUB_RADIO_STATION_MIX_TIME_FROM_TRACK_TIME_MS(eClubLocation, DJServerData, GET_RANDOM_INT_IN_RANGE(iTrackDurationQuarterMS, iTrackDurationQuarterMS*3))
	ELSE
		DJServerData.iMusicTrack = 1
		
		// Set track duration in mix time
		DJServerData.iTrackDurationMixTimeMS = GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(eClubLocation, DJServerData)
		
		DJServerData.iStartingMixTimeMS = 0
	ENDIF
	
	// Set starting time for remote player to sync against
	REINIT_NET_TIMER(DJServerData.stMusicTimer)
	DJServerData.stMusicTimer.Timer = GET_TIME_OFFSET(DJServerData.stMusicTimer.Timer, -DJServerData.iStartingMixTimeMS)
	
	
	IF DOES_CLUB_USE_MUSIC_INTENSITY(eClubLocation)
		// Set current intensity ID
		DJServerData.iTrackIntensityID = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_ID(eClubLocation, DJServerData)		
		// Set next intensity time MS
		DJServerData.iNextTrackIntensityTimeMS = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_TIME_MS(eClubLocation, DJServerData, DJServerData.iTrackIntensityID+1)		
		// Set music intensity
		DJServerData.eMusicIntensity = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_WRAPPER(eClubLocation, DJServerData, DJServerData.iTrackIntensityID)	
		// Set next music intensity
		DJServerData.eNextMusicIntensity = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_WRAPPER(eClubLocation, DJServerData, DJServerData.iTrackIntensityID+1)
	ENDIF
	
	// Set track name hash
	DJServerData.iMusicTrackNameHash = GET_HASH_KEY(GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack))
	
	// Initialisation complete
	DJServerData.bInitialisedData = TRUE
	
	IF SHOULD_SEND_MUSIC_DATA_TO_FREEMODE_HOST(eClubLocation)
		BROADCAST_DATA_DJ_CLUB_MUSIC_DATA(eClubLocation, DJServerData)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[CLUB_MUSIC] SET_CLUB_MUSIC_DATA - Data initialised.")
	PRINT_CLUB_MUSIC_DATA(eClubLocation, DJServerData)
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Initialise function for the host to set the club music data and everyone to enable their local static emitters.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to set.
PROC INITIALISE_CLUB_MUSIC(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT DJServerData.bInitialisedData
		OR IS_CLUB_MUSIC_SERVER_BIT_SET(DJServerData, CLUB_MUSIC_SERVER_BS_FREEMODE_SELECTED_DJ)
			SET_CLUB_MUSIC_DATA(eClubLocation, DJServerData)
		ELSE
			PRINTLN("[CLUB_MUSIC] INITIALISE_CLUB_MUSIC - DJ server data already Initialise")
		ENDIF		
	ENDIF
	SET_CLUB_STATIC_EMITTERS_ACTIVE_STATE(eClubLocation, TRUE)
	SET_AUDIO_ZONE_ACTIVE_STATE(eClubLocation, TRUE)
	SET_AUDIO_SCENE_ACTIVE_STATE(eClubLocation, TRUE)
ENDPROC

PROC INITIALISE_CLUB_MUSIC_DATA(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData)
	INITIALISE_CLUB_MUSIC(eClubLocation, DJServerData)
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ MAINTAIN ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛
#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Prints all the track name hashes
/// PARAMS:
///    eClubLocation - Club location.
PROC PRINT_RADIO_STATION_TRACK_HASHES(CLUB_LOCATIONS eClubLocation)
	
	INT iTrack
	STRING sTrackName = ""
	REPEAT 4 iTrack
		sTrackName = GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, CLUB_RADIO_STATION_KEINEMUSIK_NIGHTCLUB, iTrack)
		PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_MUSIC_TRACK - CLUB_RADIO_STATION_KEINEMUSIK_NIGHTCLUB - iTrack: ", iTrack, " Track Name: ", sTrackName, " Track Hash: ", GET_HASH_KEY(sTrackName))
	ENDREPEAT
	REPEAT 4 iTrack
		sTrackName = GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, CLUB_RADIO_STATION_KEINEMUSIK_BEACH_PARTY, iTrack)
		PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_MUSIC_TRACK - CLUB_RADIO_STATION_KEINEMUSIK_BEACH_PARTY - iTrack: ", iTrack, " Track Name: ", sTrackName, " Track Hash: ", GET_HASH_KEY(sTrackName))
	ENDREPEAT
	REPEAT 3 iTrack
		sTrackName = GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, CLUB_RADIO_STATION_PALMS_TRAX, iTrack)
		PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_MUSIC_TRACK - CLUB_RADIO_STATION_PALMS_TRAX - iTrack: ", iTrack, " Track Name: ", sTrackName, " Track Hash: ", GET_HASH_KEY(sTrackName))
	ENDREPEAT
	REPEAT 4 iTrack
		sTrackName = GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, CLUB_RADIO_STATION_MOODYMANN, iTrack)
		PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_MUSIC_TRACK - CLUB_RADIO_STATION_MOODYMANN - iTrack: ", iTrack, " Track Name: ", sTrackName, " Track Hash: ", GET_HASH_KEY(sTrackName))
	ENDREPEAT
	
ENDPROC
#ENDIF	// IS_DEBUG_BUILD

/// PURPOSE:
///    Host maintains if the intensity has changed, a track has ended, or a DJs entire mix has ended.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to set.
///    DJLocalData - Local data to set.
PROC MAINTAIN_CLUB_MUSIC_TRACK(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	OR NOT DJServerData.bInitialisedData
	OR NOT (DJLocalData.bMusicPlaying)
	OR NOT IS_CLUB_DJ_VALID(DJServerData.eDJ)  // Added incase DJ changes as host migrates
		EXIT
	ENDIF

	BOOL bDJChanged = FALSE
	
	// Intensity has changed
	IF (g_clubMusicData.iCurrentTrackTimeMS >= DJServerData.iNextTrackIntensityTimeMS)
	AND DOES_CLUB_USE_MUSIC_INTENSITY(eClubLocation)
		PRINTLN("[NET_CLUB_MUSIC][DEBUG PRINT] 0")
		// Increment intensity ID
		DJServerData.iTrackIntensityID++
		
		// Get next intensity time
		DJServerData.iNextTrackIntensityTimeMS = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_TIME_MS(eClubLocation, DJServerData, DJServerData.iTrackIntensityID+1)
		
		// Get new intensity and broadcast
		DJServerData.eMusicIntensity = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_WRAPPER(eClubLocation, DJServerData, DJServerData.iTrackIntensityID)
		DJServerData.eNextMusicIntensity = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_WRAPPER(eClubLocation, DJServerData, DJServerData.iTrackIntensityID+1)
		
		//DJLocalData.eMusicIntensity = DJServerData.eMusicIntensity
		//BROADCAST_CLUB_MUSIC_INTENSITY(DJServerData.eMusicIntensity)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_MUSIC_TRACK - Intensity has changed! Track Intensity ID: ", DJServerData.iTrackIntensityID, " New Intensity: ", GET_CLUB_MUSIC_INTENSITY_NAME(DJServerData.eMusicIntensity), " Next Intensity: ", GET_CLUB_MUSIC_INTENSITY_NAME(DJServerData.eNextMusicIntensity))
		PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_MUSIC_TRACK - Track: ", DJServerData.iMusicTrack , " iCurrentTrackTimeMS: ", g_clubMusicData.iCurrentTrackTimeMS, " iCurrentMixTimeMS: ", g_clubMusicData.iCurrentMixTimeMS)
		PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_MUSIC_TRACK - iMusicTrackNameHash: ", DJServerData.iMusicTrackNameHash, " GET_CURRENT_TRACK_SOUND_NAME: ", GET_CURRENT_TRACK_SOUND_NAME(DJLocalData.sRadioStationName))
		PRINT_RADIO_STATION_TRACK_HASHES(eClubLocation)
		#ENDIF
	ENDIF
	
	// Current track has ended
	IF (eClubLocation!=CLUB_LOCATION_MUSIC_STUDIO AND DJServerData.iMusicTrackNameHash != GET_CURRENT_TRACK_SOUND_NAME(DJLocalData.sRadioStationName))
	#IF FEATURE_FIXER
	OR (eClubLocation=CLUB_LOCATION_MUSIC_STUDIO AND HAS_DR_DRE_STAGE_EXPIRED())
	#IF IS_DEBUG_BUILD
	OR g_sMusicStudioDreWorkData.bForceNextState
		g_sMusicStudioDreWorkData.bForceNextState = FALSE
	#ENDIF
	#ENDIF	
	
		#IF IS_DEBUG_BUILD
			IF HAS_DR_DRE_STAGE_EXPIRED()
				PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_MUSIC_TRACK - HAS_DR_DRE_STAGE_EXPIRED " , GET_TIME_AS_STRING(DJServerData.stMusicTimer.Timer))
			ENDIF
		#ENDIF
		// Update track ID
		DJServerData.iMusicTrack++
		
		// Check if DJ set has ended
		IF (DJServerData.iMusicTrack > GET_CLUB_RADIO_STATION_MAX_TRACKS(eClubLocation, DJServerData.eRadioStation))
		OR eClubLocation=CLUB_LOCATION_MUSIC_STUDIO //WE ALWAYS WANT TO RUN THIS IN MUSIC STUDIO (FOR NOW)
			
			#IF FEATURE_FIXER
			IF eClubLocation = CLUB_LOCATION_MUSIC_STUDIO
				g_sMusicStudioDreWorkData.bStateChangeStarted = TRUE
				IF DJServerData.eDJ = CLUB_DJ_DR_DRE_MIX_2 //DECIDE WHETHER NEXT SEQUENCE SHOULD BE DRUMS OR VOCALS WHEN LAST DRE STATE ENDS
					IF SHOULD_PLAY_DRUMS_SEQUENCE()
						SET_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_USING_PATH_B)
					ELSE
						CLEAR_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_USING_PATH_B)
					ENDIF
				ENDIF
			ENDIF
			#ENDIF
			
			PRINTLN("[NET_CLUB_MUSIC][DEBUG PRINT] MUSIC TRACK int (",DJServerData.iMusicTrack,") > max tracks (", GET_CLUB_RADIO_STATION_MAX_TRACKS(eClubLocation, DJServerData.eRadioStation))
			DJServerData.eDJ = GET_CLUB_DJ(DJServerData, eClubLocation, ENUM_TO_INT(DJServerData.eDJ), TRUE #IF FEATURE_FIXER , IS_CLUB_MUSIC_SERVER_BIT_SET(DJServerData, CLUB_MUSIC_SERVER_BS_USING_PATH_B) #ENDIF )
			#IF IS_DEBUG_BUILD
			PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_MUSIC_TRACK - Track has ended! eNextDJ: ", GET_CLUB_DJ_NAME(DJServerData.eDJ))
			
			IF g_clubMusicData.eDebugNextDJ != CLUB_DJ_NULL
				DJServerData.eDJ = g_clubMusicData.eDebugNextDJ
				PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_MUSIC_TRACK - set eNextDJ from debug widget: ", GET_CLUB_DJ_NAME(DJServerData.eDJ))
			ENDIF
			#ENDIF	
					
			// Update radio station
			DJServerData.eRadioStation = GET_CLUB_RADIO_STATION(eClubLocation, DJServerData.eDJ)
			
			// Update music track				
			DJServerData.iMusicTrack = 1
			// Set starting time to zero to start new track at the beginning
			DJServerData.iStartingMixTimeMS = 0
			REINIT_NET_TIMER(DJServerData.stMusicTimer)
			DJServerData.stMusicTimer.Timer = GET_TIME_OFFSET(DJServerData.stMusicTimer.Timer, -DJServerData.iStartingMixTimeMS)
			
			bDJChanged = TRUE
		ENDIF
		
		
		// Update track duation mix time
		DJServerData.iTrackDurationMixTimeMS = GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(eClubLocation, DJServerData)
		
		IF DOES_CLUB_USE_MUSIC_INTENSITY(eClubLocation)
			// Update intensity ID
			DJServerData.iTrackIntensityID = 0
			
			// Update intensity time
			DJServerData.iNextTrackIntensityTimeMS = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_TIME_MS(eClubLocation, DJServerData, DJServerData.iTrackIntensityID+1)
			
			// Update intensity
			DJServerData.eMusicIntensity = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_WRAPPER(eClubLocation, DJServerData, DJServerData.iTrackIntensityID)
			DJServerData.eNextMusicIntensity = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_WRAPPER(eClubLocation, DJServerData, DJServerData.iTrackIntensityID+1)
		ENDIF
		
		// Update track name hash
		DJServerData.iMusicTrackNameHash = GET_HASH_KEY(GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack))
		
		//DJLocalData.eMusicIntensity = DJServerData.eMusicIntensity
		//BROADCAST_CLUB_MUSIC_INTENSITY(DJServerData.eMusicIntensity)
		
		IF bDJChanged
			CLEAR_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_FREEMODE_SELECTED_DJ)
			CLEAR_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_START_FROM_BEGINING_OF_SET)
			
			IF SHOULD_SEND_MUSIC_DATA_TO_FREEMODE_HOST(eClubLocation)
				BROADCAST_DATA_DJ_CLUB_MUSIC_DATA(eClubLocation, DJServerData)
			ENDIF
			
			// Keep this here we don't want to set freemode bit set CLUB_MUSIC_SERVER_BS_SET_ENDED
			//IF eClubLocation != CLUB_LOCATION_MUSIC_STUDIO
			IF (DJServerData.eDJ != DJLocalData.eDJ
			OR eClubLocation = CLUB_LOCATION_ISLAND)
				BROADCAST_CLUB_MUSIC_DJ_SWITCH_SCENE(TRUE, DJServerData.eDJ)
			ENDIF
//			#IF FEATURE_FIXER
//			IF DJServerData.iMusicStudioWorkState != DJLocalData.iMusicStudioWorkState
//			AND eClubLocation = CLUB_LOCATION_MUSIC_STUDIO
//				BROADCAST_MUSIC_STUDIO_CHANGE_DRE_WORK_STATE(TRUE, INT_TO_ENUM(MUSIC_STUDIO_WORK_STATE, DJServerData.iMusicStudioWorkState), INT_TO_ENUM(CLUB_DJS, DJServerData.eDJ))
//			ENDIF
//			#ENDIF
		ENDIF	
			
		#IF IS_DEBUG_BUILD
		PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_MUSIC_TRACK - Track has ended! DJ: ", GET_CLUB_DJ_NAME(DJServerData.eDJ), " New Track: ", DJServerData.iMusicTrack , " bDJChanged: ", GET_STRING_FROM_BOOL(bDJChanged) )
		#ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Events have been set up to inform when a music intensity is about to change, and what that music intensity is.
///    However, we have situations that require knowing the time until the next transition, and the music intensity, on a per frame bases.
///    Therefore globals have been added to track this information.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to set.
///    DJLocalData - Local data to set.
PROC MAINTAIN_CLUB_MUSIC_GLOBALS(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	
	// Current Track Time
	IF NOT IS_STRING_NULL_OR_EMPTY(DJLocalData.sRadioStationName)
		g_clubMusicData.iCurrentTrackTimeMS = GET_CURRENT_TRACK_PLAY_TIME(DJLocalData.sRadioStationName)
		
		IF (g_clubMusicData.iCurrentTrackTimeMS < 0)
			g_clubMusicData.iCurrentTrackTimeMS = 0
		ENDIF
	ELSE
		// Note: The player starts their local radio based on the host timers offset. Thereafter, any music time queries are local and
		// 		 come from GET_CURRENT_TRACK_PLAY_TIME. If the radio station is null or any reason, fall back to use the host timer offset.
		INT iCurrentMixTimeMS = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(DJServerData.stMusicTimer, DEFAULT, TRUE)
		g_clubMusicData.iCurrentTrackTimeMS = GET_CLUB_RADIO_STATION_TRACK_TIME_FROM_MIX_TIME_MS(eClubLocation, DJServerData, iCurrentMixTimeMS)
	ENDIF
	
	// Current Mix Time
	IF DJServerData.iMusicTrack > 1
		g_clubMusicData.iCurrentMixTimeMS = GET_SUM_CLUB_RADIO_STATION_TRACK_TIME_MS(eClubLocation, DJServerData, DJServerData.iMusicTrack-1)+g_clubMusicData.iCurrentTrackTimeMS
	ELSE
		g_clubMusicData.iCurrentMixTimeMS = g_clubMusicData.iCurrentTrackTimeMS
	ENDIF
	
	
	IF DOES_CLUB_USE_MUSIC_INTENSITY(eClubLocation)
		INT iRemainingTimeMS = (DJServerData.iNextTrackIntensityTimeMS - g_clubMusicData.iCurrentTrackTimeMS)
		g_iTimeUntilPedDanceTransitionSecs = (iRemainingTimeMS / 1000)
		
		IF (g_iTimeUntilPedDanceTransitionSecs < 0)
			g_iTimeUntilPedDanceTransitionSecs = (MAX_NUM_PROCESS_SECONDS_TO_INTENSITY_DANCING_TRANSITION+1)
		ENDIF
	
		// Local Music Intensity
		IF (DJLocalData.eMusicIntensity != DJServerData.eMusicIntensity)
			DJLocalData.eMusicIntensity = DJServerData.eMusicIntensity
		ENDIF
		
		// Local Next Music Intensity
		IF (DJLocalData.eNextMusicIntensity != DJServerData.eNextMusicIntensity)
			DJLocalData.eNextMusicIntensity = DJServerData.eNextMusicIntensity
		ENDIF
		
		// g_clubMusicData.eClubMusicIntensity
		IF (g_iTimeUntilPedDanceTransitionSecs <= 0)
			IF (g_clubMusicData.eClubMusicIntensity != DJLocalData.eNextMusicIntensity AND DJLocalData.eNextMusicIntensity != CLUB_MUSIC_INTENSITY_NULL)
				g_clubMusicData.eClubMusicIntensity = DJLocalData.eNextMusicIntensity
			ENDIF
		ENDIF
		
		// Update g_clubMusicData.eNextClubMusicIntensity
		IF (g_clubMusicData.eNextClubMusicIntensity != DJLocalData.eNextMusicIntensity AND DJLocalData.eNextMusicIntensity != CLUB_MUSIC_INTENSITY_NULL)
			g_clubMusicData.eNextClubMusicIntensity = DJLocalData.eNextMusicIntensity
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Calculates the track detail using data held by freemode host
/// PARAMS:
///    eClubLocation - 
///    DJServerData - 
PROC CALCULATE_TRACK_DETAIL_FROM_FREEMODE_TIMER(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData)
	BOOL bDataFileLoaded = FALSE
	TEXT_LABEL_63 sDataFileName
	
	// We need to load DJ datafile before qurey data
	IF DOES_CLUB_LOCATION_USE_DATAFILE(eClubLocation, GlobalServerBD_BlockB.DJServerData[ENUM_TO_INT(eClubLocation)].eDJ, sDataFileName)
		IF NOT IS_STRING_NULL_OR_EMPTY(sDataFileName)
			IF HAS_DJ_DATAFILE_LOADED(eClubLocation, GlobalServerBD_BlockB.DJServerData[ENUM_TO_INT(eClubLocation)].eDJ)
				bDataFileLoaded = TRUE
			ENDIF
		ELSE
			PRINTLN("[CLUB_MUSIC] CALCULATE_TRACK_DETAIL_FROM_FREEMODE_TIMER sDataFileName is empty")
		ENDIF	
	ELSE
		bDataFileLoaded = TRUE
	ENDIF
	
	IF !bDataFileLoaded
		EXIT
	ENDIF	
	
	// Grab data from freemode host
	DJServerData = GlobalServerBD_BlockB.DJServerData[ENUM_TO_INT(eClubLocation)]

	INT iCurrentMixTimeMS = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(DJServerData.stMusicTimer, DEFAULT, TRUE)

	PRINTLN("[CLUB_MUSIC] CALCULATE_TRACK_DETAIL_FROM_FREEMODE_TIMER - iCurrentMixTimeMS: ", iCurrentMixTimeMS)
	INT iMaxTrack = _GET_CLUB_DJ_RADIO_STATION_MAX_TRACKS(DJServerData.eRadioStation)	

	BOOL bNewDJ = TRUE
	INT iTrack 
	FOR iTrack = 1 TO iMaxTrack
		IF iCurrentMixTimeMS < GET_SUM_CLUB_RADIO_STATION_TRACK_TIME_MS(eClubLocation, DJServerData, iTrack)
			DJServerData.iMusicTrack = iTrack
			IF iTrack = 1
				DJServerData.iStartingMixTimeMS = GET_CLUB_RADIO_STATION_MIX_TIME_FROM_TRACK_TIME_MS(eClubLocation, DJServerData, iCurrentMixTimeMS)
			ELSE
				DJServerData.iStartingMixTimeMS = GET_CLUB_RADIO_STATION_MIX_TIME_FROM_TRACK_TIME_MS(eClubLocation, DJServerData, iCurrentMixTimeMS - GET_SUM_CLUB_RADIO_STATION_TRACK_TIME_MS(eClubLocation, DJServerData, iTrack - 1))
			ENDIF
			bNewDJ = FALSE
			PRINTLN("[CLUB_MUSIC] CALCULATE_TRACK_DETAIL_FROM_FREEMODE_TIMER iTrack: ", iTrack, " iStartingMixTimeMS: " , DJServerData.iStartingMixTimeMS, " iCurrentMixTimeMS: ", iCurrentMixTimeMS," BREAKLOOP")
			BREAKLOOP
		ELSE
			PRINTLN("[CLUB_MUSIC] CALCULATE_TRACK_DETAIL_FROM_FREEMODE_TIMER Track: ", iTrack, " iCurrentMixTimeMS: ", iCurrentMixTimeMS, " track duration: ", GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, iTrack))
		ENDIF
	ENDFOR
	
	IF NOT bNewDJ
		// if we are on final track and almost done with the set change DJ to next available 
		IF DJServerData.iMusicTrack = _GET_CLUB_DJ_RADIO_STATION_MAX_TRACKS(DJServerData.eRadioStation)	
			INT iCurrentTrackTimeMS = GET_CLUB_RADIO_STATION_TRACK_TIME_FROM_MIX_TIME_MS(eClubLocation, DJServerData, iCurrentMixTimeMS)
			INT iTrackDuration = GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack)
			IF (iTrackDuration - iCurrentTrackTimeMS <= 30000)
				// set next available DJ
				bNewDJ = TRUE
				PRINTLN("[CLUB_MUSIC] CALCULATE_TRACK_DETAIL_FROM_FREEMODE_TIMER we are close to end of set let's get next DJ")
			ENDIF
		ENDIF
	ENDIF
	
	
	#IF FEATURE_MUSIC_STUDIO
	IF eClubLocation = CLUB_LOCATION_MUSIC_STUDIO
		IF NOT IS_CLUB_MUSIC_SERVER_BIT_SET(DJServerData, CLUB_MUSIC_SERVER_BS_MUSIC_STUDIO_TRACK_SELECTED)		//TO PREVENT HOST LEAVING AND REENTERING THE STUDIO AND SEEING A DIFFERENT SEQUENCE PLAY OUT
	
			IF SHOULD_PLAY_DRUMS_SEQUENCE()
				SET_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_USING_PATH_B)
			ELSE
				CLEAR_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_USING_PATH_B)
			ENDIF
			
			SET_CLUB_MUSIC_SERVER_BIT(DJServerData, CLUB_MUSIC_SERVER_BS_MUSIC_STUDIO_TRACK_SELECTED)
		ENDIF
		
		DJServerData.eDJ = GET_CURRENT_DRE_STATE_BASED_ON_POSIX(DJServerData)		
		DJServerData.iStartingMixTimeMS =  GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack) - GET_CURRENT_DRE_TRACK_OFFSET_BASED_ON_POSIX(DJServerData)
		DJServerData.eRadioStation = GET_CLUB_RADIO_STATION(eClubLocation, DJServerData.eDJ)
		DJServerData.iMusicTrack = 1	
		bNewDJ = FALSE
	ENDIF
	#ENDIF
	
	// Get next DJ
	IF bNewDJ		
		DJServerData.eDJ = GET_CLUB_DJ(DJServerData, eClubLocation, ENUM_TO_INT(DJServerData.eDJ), TRUE)		
			
		#IF IS_DEBUG_BUILD
		PRINTLN("[CLUB_MUSIC] CALCULATE_TRACK_DETAIL_FROM_FREEMODE_TIMER - Track has ended! eNextDJ: ", GET_CLUB_DJ_NAME(DJServerData.eDJ))
		#ENDIF

		// Update radio station
		DJServerData.eRadioStation = GET_CLUB_RADIO_STATION(eClubLocation, DJServerData.eDJ)
		
		// Update music track
		IF eClubLocation = CLUB_LOCATION_ISLAND
			// We always want to start on first half of set
			DJServerData.iMusicTrack = GET_RANDOM_INT_IN_RANGE(1, GET_CLUB_RADIO_STATION_MAX_TRACKS(eClubLocation, DJServerData.eRadioStation) / 2)
		ELSE	
			DJServerData.iMusicTrack = 1
		ENDIF
	ENDIF	
	
	// Set track duration in mix time
	DJServerData.iTrackDurationMixTimeMS = GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(eClubLocation, DJServerData)
	
	IF bNewDJ
		IF eClubLocation = CLUB_LOCATION_ISLAND
			// Set track duration in mix time
			DJServerData.iTrackDurationMixTimeMS = GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(eClubLocation, DJServerData)
			
			// Set starting time of track somewhere in middle - not at start or end of track
			INT iTrackDurationQuarterMS = GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack)/4
			DJServerData.iStartingMixTimeMS = GET_CLUB_RADIO_STATION_MIX_TIME_FROM_TRACK_TIME_MS(eClubLocation, DJServerData, GET_RANDOM_INT_IN_RANGE(iTrackDurationQuarterMS, iTrackDurationQuarterMS*3))
		ELSE
			// Set starting time to zero to start new track at the beginning
			DJServerData.iStartingMixTimeMS = 0
		ENDIF
	ELSE
		// Update music track
		IF eClubLocation = CLUB_LOCATION_ISLAND
			// We always want to start on first half of set if we past the half way
			IF DJServerData.iMusicTrack > GET_CLUB_RADIO_STATION_MAX_TRACKS(eClubLocation, DJServerData.eRadioStation) / 2
				PRINTLN("[CLUB_MUSIC] CALCULATE_TRACK_DETAIL_FROM_FREEMODE_TIMER DJServerData.iMusicTrack: ", DJServerData.iMusicTrack, " more than half way lets get a new track")
				
				DJServerData.iMusicTrack = GET_RANDOM_INT_IN_RANGE(1, GET_CLUB_RADIO_STATION_MAX_TRACKS(eClubLocation, DJServerData.eRadioStation) / 2)
				
				DJServerData.iTrackDurationMixTimeMS = GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(eClubLocation, DJServerData)
			
				// Set starting time of track somewhere in middle - not at start or end of track
				INT iTrackDurationQuarterMS = GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack)/4
				
				DJServerData.iStartingMixTimeMS = GET_CLUB_RADIO_STATION_MIX_TIME_FROM_TRACK_TIME_MS(eClubLocation, DJServerData, GET_RANDOM_INT_IN_RANGE(iTrackDurationQuarterMS, iTrackDurationQuarterMS*3))
			ENDIF	
		ENDIF	
	ENDIF
	
	REINIT_NET_TIMER(DJServerData.stMusicTimer)
	DJServerData.stMusicTimer.Timer = GET_TIME_OFFSET(DJServerData.stMusicTimer.Timer, -DJServerData.iStartingMixTimeMS)
	
	IF DOES_CLUB_USE_MUSIC_INTENSITY(eClubLocation)
		// Set current intensity ID
		DJServerData.iTrackIntensityID = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_ID(eClubLocation, DJServerData)
		
		// Set next intensity time MS
		DJServerData.iNextTrackIntensityTimeMS = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_TIME_MS(eClubLocation, DJServerData, DJServerData.iTrackIntensityID+1)
		
		// Set music intensity
		DJServerData.eMusicIntensity = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_WRAPPER(eClubLocation, DJServerData, DJServerData.iTrackIntensityID)
		
		// Set next music intensity
		DJServerData.eNextMusicIntensity = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_WRAPPER(eClubLocation, DJServerData, DJServerData.iTrackIntensityID+1)
	ENDIF
	
	// Set track hash
	DJServerData.iMusicTrackNameHash = GET_HASH_KEY(GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack))
ENDPROC

/// PURPOSE:
///     Check if we should grab data from freemode host and calculate track detail
FUNC BOOL SHOULD_CALCULATE_TRACK_DETAIL_FROM_FREEMODE(CLUB_LOCATIONS eClubLocation)
	IF eClubLocation = CLUB_LOCATION_ISLAND
		IF GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = IHV_SCOPING_INTRO
			RETURN FALSE	
		ENDIF
	ENDIF
		
	// Lets keep this at the end of this function
	IF GlobalServerBD_BlockB.DJServerData[ENUM_TO_INT(eClubLocation)].eDJ != CLUB_DJ_NULL
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CLUB_DJ_INIT_DATAFILE_STATE(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	TEXT_LABEL_63 sDataFileName
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PLAYER_INDEX freemodeHost = NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT())
		IF freemodeHost != INVALID_PLAYER_INDEX()
			IF SHOULD_CALCULATE_TRACK_DETAIL_FROM_FREEMODE(eClubLocation)
				IF NOT IS_CLUB_MUSIC_SERVER_BIT_SET(GlobalServerBD_BlockB.DJServerData[ENUM_TO_INT(eClubLocation)], CLUB_MUSIC_SERVER_BS_FREEMODE_SELECTED_DJ)
				OR eClubLocation = CLUB_LOCATION_MUSIC_STUDIO
					CALCULATE_TRACK_DETAIL_FROM_FREEMODE_TIMER(eClubLocation, DJServerData)
					PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_DJ_INIT_DATAFILE_STATE freemode host has DJ SET_CLUB_MUSIC_DJ to: ", GET_CLUB_DJ_NAME(DJServerData.eDJ))
				ELSE
					DJServerData.eDJ = GlobalServerBD_BlockB.DJServerData[ENUM_TO_INT(eClubLocation)].eDJ
					PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_DJ_INIT_DATAFILE_STATE freemode host has fresh DJ for us, don't need to calculate track detail SET_CLUB_MUSIC_DJ: ", GET_CLUB_DJ_NAME(DJServerData.eDJ))
				ENDIF
			ELSE	
				IF NOT DJServerData.bInitialisedData				
					SET_CLUB_MUSIC_DJ(eClubLocation, DJServerData)					
				ELSE
					PRINTLN("[CLUB_MUSIC] INITIALISE_CLUB_MUSIC_DATA server data already initialised")
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		PRINTLN("[CLUB_MUSIC] INITIALISE_CLUB_MUSIC_DATA script host is: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))
	ENDIF
	
	IF DJServerData.eDJ = CLUB_DJ_NULL
		PRINTLN("[CLUB_MUSIC] INITIALISE_CLUB_MUSIC_DATA waiting for host to set the DJ")
		EXIT
	ENDIF
	
	DJLocalData.eDJ = DJServerData.eDJ
	g_clubMusicData.eActiveDJ = DJLocalData.eDJ
	g_sMusicStudioDreWorkData.bWasSequenceSelected = TRUE
	PRINTLN("[CLUB_MUSIC] INITIALISE_CLUB_MUSIC_DATA - eActiveDJ = ", g_clubMusicData.eActiveDJ)
		
	REQUEST_SCRIPT_AUDIO_BANK("DLC_BATTLE/BTL_CLUB_OPEN_TRANSITION_CROWD")
	
	IF DOES_CLUB_LOCATION_USE_DATAFILE(eClubLocation, DJLocalData.eDJ, sDataFileName)
		IF NOT IS_STRING_NULL_OR_EMPTY(sDataFileName)
			IF HAS_DJ_DATAFILE_LOADED(eClubLocation, DJLocalData.eDJ)
				SET_CLUB_DJ_STATE(DJLocalData, CLUB_DJ_INIT_STATE)
			ENDIF
		ELSE
			PRINTLN("[CLUB_MUSIC] INITIALISE_CLUB_MUSIC_DATA sDataFileName is empty")
		ENDIF
	ELSE	
		SET_CLUB_DJ_STATE(DJLocalData, CLUB_DJ_INIT_STATE)
	ENDIF
ENDPROC

PROC MAINTAIN_CLUB_DJ_INIT_STATE(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	INITIALISE_CLUB_MUSIC_DATA(eClubLocation, DJServerData)
	SET_CLUB_DJ_STATE(DJLocalData, CLUB_DJ_UPDATE_STATE)
ENDPROC

PROC MAINTAIN_CLUB_DJ_UPDATE_STATE(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	PROCESS_EVENTS(eClubLocation, DJServerData, DJLocalData)
	MAINTAIN_CLUB_MUSIC_GLOBALS(eClubLocation, DJServerData, DJLocalData)	
	MAINTAIN_CLUB_MUSIC_TRACK(eClubLocation, DJServerData, DJLocalData)

	IF SHOULD_PAUSE_CLUB_MUSIC(eClubLocation, DJLocalData, DJServerData)
		SET_AUDIO_ZONE_ACTIVE_STATE(eClubLocation, FALSE)
		SET_CLUB_STATIC_EMITTERS_ACTIVE_STATE(eClubLocation, FALSE)
		g_iTimeUntilPedDanceTransitionSecs = 60 
		g_clubMusicData.eClubMusicIntensity = CLUB_MUSIC_INTENSITY_LOW
		SET_CLUB_DJ_STATE(DJLocalData, CLUB_DJ_SET_PAUSE_MUSIC_STATE)
		EXIT
	ENDIF
		
	IF (NOT DJServerData.bInitialisedData OR DJLocalData.bMusicPlaying)
		EXIT
	ENDIF

	PLAY_CLUB_MUSIC(eClubLocation, DJServerData, DJLocalData)
ENDPROC

PROC MAINTAIN_CLUB_DJ_SET_END_STATE(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	IF g_clubMusicData.bHasDJSwitchOverSceneStarted
		IF NOT g_clubMusicData.bHasNextDJDataFileLoaded
			TEXT_LABEL_63 sDataFileName
			IF DOES_CLUB_LOCATION_USE_DATAFILE(eClubLocation, DJServerData.eDJ, sDataFileName)
				IF NOT IS_STRING_NULL_OR_EMPTY(sDataFileName)
					IF HAS_DJ_DATAFILE_LOADED(eClubLocation, DJServerData.eDJ)
						g_clubMusicData.bHasNextDJDataFileLoaded = TRUE
						PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_DJ_SET_END_STATE g_clubMusicData.bHasNextDJDataFileLoaded = TRUE")
					ENDIF
				ENDIF
			ELSE
				g_clubMusicData.bHasNextDJDataFileLoaded = TRUE
			ENDIF
		ENDIF	
	ENDIF
	
	IF g_clubMusicData.eNextDJ = CLUB_DJ_NULL
	AND g_clubMusicData.bHasNextDJDataFileLoaded

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			// Set starting time to zero to start new track at the beginning
			DJServerData.iTrackDurationMixTimeMS = GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(eClubLocation, DJServerData)
			DJServerData.iStartingMixTimeMS = 0
			REINIT_NET_TIMER(DJServerData.stMusicTimer)
			DJServerData.stMusicTimer.Timer = GET_TIME_OFFSET(DJServerData.stMusicTimer.Timer, -DJServerData.iStartingMixTimeMS)
			DJServerData.iMusicTrackNameHash = GET_HASH_KEY(GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack))
			
			BROADCAST_CLUB_MUSIC_DJ_SWITCH_SCENE(FALSE)
			PRINTLN("[CLUB_MUSIC] MAINTAIN_CLUB_DJ_SET_END_STATE reset set")
		ENDIF
		
		DJLocalData.sRadioStationName 	= GET_CLUB_RADIO_STATION_NAME(eClubLocation, DJServerData.eRadioStation)
		g_clubMusicData.iCurrentTrackTimeMS = 0
		g_clubMusicData.iCurrentMixTimeMS = 0
		DJLocalData.bMusicPlaying = FALSE
		
		g_clubMusicData.bHasDJSwitchOverSceneStarted = FALSE
		IF eClubLocation != CLUB_LOCATION_MUSIC_STUDIO
			g_clubMusicData.bHasNextDJDataFileLoaded = FALSE
		ENDIF
		SET_CLUB_DJ_STATE(DJLocalData, CLUB_DJ_UPDATE_STATE)	
	ENDIF
ENDPROC

PROC MAINTAIN_CLUB_DJ_PAUSE_MUSIC_STATE(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	PROCESS_EVENTS(eClubLocation, DJServerData, DJLocalData)
	
	IF SHOULD_PAUSE_CLUB_MUSIC(eClubLocation, DJLocalData, DJServerData)
		EXIT
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		// Set starting time to zero to start new track at the beginning
		DJServerData.iTrackDurationMixTimeMS = GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(eClubLocation, DJServerData)
		DJServerData.iStartingMixTimeMS = 0
		REINIT_NET_TIMER(DJServerData.stMusicTimer)
		DJServerData.stMusicTimer.Timer = GET_TIME_OFFSET(DJServerData.stMusicTimer.Timer, -DJServerData.iStartingMixTimeMS)
		DJServerData.iMusicTrackNameHash = GET_HASH_KEY(GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack))
		DJServerData.bInitialisedData = TRUE
		BROADCAST_CLUB_MUSIC_DJ_SWITCH_SCENE(FALSE)
	ENDIF
	
	IF (DJServerData.bInitialisedData)
		DJLocalData.bMusicPlaying = FALSE
		PRINTLN("[CLUB_MUSIC] [MAINTAIN_CLUB_DJ_PAUSE_MUSIC_STATE] DJLocalData.bMusicPlaying = FALSE")
		SET_AUDIO_ZONE_ACTIVE_STATE(eClubLocation, TRUE)
		SET_CLUB_STATIC_EMITTERS_ACTIVE_STATE(eClubLocation, TRUE)
		SET_CLUB_DJ_STATE(DJLocalData, CLUB_DJ_UPDATE_STATE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain function for the club music.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to set.
///    DJLocalData - Local data to set.
PROC MAINTAIN_CLUB_MUSIC(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	SWITCH DJLocalData.eDJstate
		CASE CLUB_DJ_INIT_DATAFILE_STATE
			MAINTAIN_CLUB_DJ_INIT_DATAFILE_STATE(eClubLocation, DJServerData, DJLocalData)
		BREAK	
		CASE CLUB_DJ_INIT_STATE
			MAINTAIN_CLUB_DJ_INIT_STATE(eClubLocation , DJServerData, DJLocalData)
		BREAK
		CASE CLUB_DJ_UPDATE_STATE
			MAINTAIN_CLUB_DJ_UPDATE_STATE(eClubLocation , DJServerData, DJLocalData)
		BREAK
		CASE CLUB_DJ_SET_END_STATE
			MAINTAIN_CLUB_DJ_SET_END_STATE(eClubLocation, DJServerData, DJLocalData)
		BREAK
		CASE CLUB_DJ_SET_PAUSE_MUSIC_STATE
			MAINTAIN_CLUB_DJ_PAUSE_MUSIC_STATE(eClubLocation, DJServerData, DJLocalData)
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF //FEATURE_HEIST_ISLAND
