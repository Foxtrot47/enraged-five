USING "net_DAILY_COLLECTABLES.sch"


STRUCT DAILY_COLLECTABLES_VARS_STRUCT

	INT iDOLocalBitSet
	SCRIPT_TIMER resetCheckTimer
	INT iResetCheckTime	
	AMDO_RUN_STAGE eAMDO_Stage = AMDO_GET_OBJECTIVES

	BOOL bIgnoreHistory
	
	INT iObjectiveDay

	INT iClientGameStateCollectables = DO_GAME_STATE_INIT
	
	//Debug Variables
	//Debug Variables
	#IF IS_DEBUG_BUILD


		BOOL bUseShortResetTimer



	#ENDIF
ENDSTRUCT 

//Helper function to get a clients game/mission state for collectables
FUNC INT GET_DAILY_COLLECTABLE_MISSION_STATE(DAILY_COLLECTABLES_VARS_STRUCT &sDailyCollectablesVarsStruct)
	RETURN sDailyCollectablesVarsStruct.iClientGameStateCollectables
ENDFUNC

FUNC BOOL IS_TIME_TO_RESET_DAILY_COLLECTABLES(DAILY_COLLECTABLES_VARS_STRUCT &sDailyCollectablesVarsStruct)

	IF NOT IS_BIT_SET(sDailyCollectablesVarsStruct.iDOLocalBitSet,AMDO_BS_CHECK_TIMER_RESET)
		RESET_NET_TIMER(sDailyCollectablesVarsStruct.resetCheckTimer)
		SET_BIT(sDailyCollectablesVarsStruct.iDOLocalBitSet,AMDO_BS_CHECK_TIMER_RESET)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(sDailyCollectablesVarsStruct.resetCheckTimer,sDailyCollectablesVarsStruct.iResetCheckTime)
			CLEAR_BIT(sDailyCollectablesVarsStruct.iDOLocalBitSet,AMDO_BS_CHECK_TIMER_RESET)
			 
			UGC_DATE currentTime
			GET_UTC_TIME(currentTime.nYear,currentTime.nMonth,currentTime.nDay,currentTime.nHour,currentTime.nMinute,currentTime.nSecond)
			
			UGC_DATE lastResetTime
			CONVERT_POSIX_TIME(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.LastResetTime,lastResetTime)
				
			INT iDiff = GET_CLOUD_TIME_AS_INT() - g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.LastResetTime
				
			IF NOT IS_TIME_IN_LAST_OBJECTIVE_DAY(lastResetTime,currentTime,iDiff)
				
				#IF IS_DEBUG_BUILD
				IF sDailyCollectablesVarsStruct.bUseShortResetTimer
					IF currentTime.nMinute >= 0
						sDailyCollectablesVarsStruct.iResetCheckTime = DEBUG_LONG_RESET_TIME //Don't check again for a full hour
						CPRINTLN(DEBUG_NET_AMBIENT, "[KW Collectables] === DAILY_COLLECTABLES === IS_TIME_TO_RESET_DAILY_COLLECTABLES - TRUE - DEBUG_LONG_RESET_TIME")
						RETURN TRUE
					ENDIF	
				ELSE
				#ENDIF
					IF currentTime.nHour = g_sMPTunables.idaily_obj_time_refresh
					AND currentTime.nMinute >= 0
						sDailyCollectablesVarsStruct.iResetCheckTime = LONG_RESET_CHECK_TIME //Don't check again for a full day
						CPRINTLN(DEBUG_NET_AMBIENT, "[KW Collectables] === DAILY_COLLECTABLES === IS_TIME_TO_RESET_DAILY_COLLECTABLES - TRUE - LONG_RESET_CHECK_TIME")
						RETURN TRUE
					ENDIF	
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RESET_DAILY_COLLECTABLE_STATS()
	PRINTLN("[KW Collectables] RESET_DAILY_COLLECTABLE_STATS")
	SET_LOGGED_IN_TODAY_DAILY_COLLECTABLES(FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_HIDDEN_CACHE_0, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_HIDDEN_CACHE_1,FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_HIDDEN_CACHE_2,FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_HIDDEN_CACHE_3,FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_HIDDEN_CACHE_4,FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_HIDDEN_CACHE_5,FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_HIDDEN_CACHE_6,FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_HIDDEN_CACHE_7,FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_HIDDEN_CACHE_8,FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_HIDDEN_CACHE_9,FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_TREASURE_CHESTS_0, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_TREASURE_CHESTS_1,FALSE)

	collectables_missiondata_main.inumber_of_Underwater_Packages_collected = 0
	collectables_missiondata_main.inumber_of_Treasure_Chests_collected = 0
	SET_MP_INT_CHARACTER_STAT(MP_STAT_UNDERWATRPACK_COLLECTED, collectables_missiondata_main.inumber_of_Underwater_Packages_collected)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_TREASURECHEST_COLLECTED, collectables_missiondata_main.inumber_of_Treasure_Chests_collected)
	bis_Treasure_Chests_collectables_on = FALSE
	bis_Underwater_Packages_collectables_on = FALSE
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_RESETTIME, GET_CLOUD_TIME_AS_INT())

	INT i
	REPEAT g_sMPTunables.iNumber_of_active_hidden_caches i
		IF DOES_BLIP_EXIST(collectables_missiondata_main.blip_sonar[i] )
			REMOVE_BLIP(collectables_missiondata_main.blip_sonar[i])
			collectables_missiondata_main.iSonarBlipCount--
		ENDIF
	ENDREPEAT	
	
		
	#IF FEATURE_FIXER

		SET_PACKED_STAT_BOOL(	PACKED_MP_BURIED_STASH_0,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_BURIED_STASH_1,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_BURIED_STASH_2,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_BURIED_STASH_3,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_BURIED_STASH_4,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_BURIED_STASH_5,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_BURIED_STASH_6,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_BURIED_STASH_7,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_BURIED_STASH_8,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_BURIED_STASH_9,FALSE)

		
		SET_PACKED_STAT_BOOL( PACKED_MP_BURIED_STASH_HAS_BEEN_FOUND_0, FALSE)
		SET_PACKED_STAT_BOOL( PACKED_MP_BURIED_STASH_HAS_BEEN_FOUND_1, FALSE)
				


		
		//collectables_missiondata_main.inumber_of_BURIED_STASH_collected = 0

		//SET_MP_INT_CHARACTER_STAT(MP_STAT_BURIED_STASH_COLLECTED, collectables_missiondata_main.inumber_of_BURIED_STASH_collected)

		bis_BURIED_STASH_collectables_on = FALSE
		

	#ENDIF
	
	#IF FEATURE_TUNER
		SET_PACKED_STAT_BOOL(	PACKED_MP_SHIPWRECKED_0,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_SHIPWRECKED_1,FALSE)

		//collectables_missiondata_main.inumber_of_SHIPWRECKED_collected = 0
		//SET_MP_INT_CHARACTER_STAT(MP_STAT_SHIPWRECKED_COLLECTED, collectables_missiondata_main.inumber_of_SHIPWRECKED_collected)
		bis_SHIPWRECKED_collectables_on = FALSE

		

	#ENDIF

	#IF FEATURE_DLC_1_2022
		SET_PACKED_STAT_BOOL(	PACKED_MP_TRICK_OR_TREAT_0,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_TRICK_OR_TREAT_1,FALSE)	
		SET_PACKED_STAT_BOOL(	PACKED_MP_TRICK_OR_TREAT_2,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_TRICK_OR_TREAT_3,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_TRICK_OR_TREAT_4,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_TRICK_OR_TREAT_5,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_TRICK_OR_TREAT_6,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_TRICK_OR_TREAT_7,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_TRICK_OR_TREAT_8,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_TRICK_OR_TREAT_9,FALSE)
		
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_10	,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_11    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_12    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_13    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_14    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_15    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_16    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_17    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_18    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_19    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_20    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_21    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_22    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_23    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_24    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_25    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_26    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_27    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_28    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_29    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_30    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_31    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_32    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_33    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_34    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_35    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_36    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_37    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_38    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_39    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_40    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_41    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_42    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_43    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_44    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_45    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_46    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_47    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_48    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_49    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_50    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_51    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_52    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_53    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_54    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_55    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_56    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_57    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_58    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_59    ,FALSE)
	    SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_60    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_61    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_62    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_63    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_64    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_65    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_66    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_67    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_68    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_69    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_70    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_71    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_72    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_73    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_74    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_75    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_76    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_77    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_78    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_79    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_80    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_81    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_82    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_83    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_84    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_85    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_86    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_87    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_88    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_89    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_90    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_91    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_92    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_93    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_94    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_95    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_96    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_97    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_98    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_99    ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_100   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_101   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_102   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_103   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_104   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_105   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_106   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_107   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_108   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_109   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_110   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_111   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_112   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_113   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_114   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_115   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_116   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_117   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_118   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_119   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_120   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_121   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_122   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_123   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_124   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_125   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_126   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_127   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_128   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_129   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_130   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_131   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_132   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_133   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_134   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_135   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_136   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_137   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_138   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_139   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_140   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_141   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_142   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_143   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_144   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_145   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_146   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_147   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_148   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_149   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_150   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_151   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_152   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_153   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_154   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_155   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_156   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_157   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_158   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_159   ,FALSE)
	    SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_160   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_161   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_162   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_163   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_164   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_165   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_166   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_167   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_168   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_169   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_170   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_171   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_172   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_173   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_174   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_175   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_176   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_177   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_178   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_179   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_180   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_181   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_182   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_183   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_184   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_185   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_186   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_187   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_188   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_189   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_190   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_191   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_192   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_193   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_194   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_195   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_196   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_197   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_198   ,FALSE)
		SET_PACKED_STAT_BOOL(PACKED_MP_TRICK_OR_TREAT_199   ,FALSE)
		bis_TRICK_OR_TREAT_collectables_on = FALSE
		collectables_missiondata_main.inumber_of_TRICK_OR_TREAT_collected = 0
		SET_MP_INT_CHARACTER_STAT(MP_STAT_TRICKORTREAT_COLLECTED, collectables_missiondata_main.inumber_of_TRICK_OR_TREAT_collected)
		
		
		SET_PACKED_STAT_BOOL(	PACKED_MP_SKYDIVES_0,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_SKYDIVES_1,FALSE)	
		SET_PACKED_STAT_BOOL(	PACKED_MP_SKYDIVES_2,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_SKYDIVES_3,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_SKYDIVES_4,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_SKYDIVES_5,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_SKYDIVES_6,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_SKYDIVES_7,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_SKYDIVES_8,FALSE)
		SET_PACKED_STAT_BOOL(	PACKED_MP_SKYDIVES_9,FALSE)
		bis_SKYDIVES_collectables_on = FALSE
	#ENDIF 
		
	
ENDPROC

#IF FEATURE_DLC_1_2022

PROC  GET_DAILY_COLLECTABLE_SKYDIVES_IN_ORDER_ACCORDING_TO_DAY()

	INT	iskydives_location[10]
	iSkydives_location[0] = 0
	iSkydives_location[1] = 1
	iSkydives_location[2] = 2
	iSkydives_location[3] = 3
	iSkydives_location[4] = 4
	iSkydives_location[5] = 5
	iSkydives_location[6] = 6
	iSkydives_location[7] = 7
	iSkydives_location[8] = 8
	iSkydives_location[9] = 9
	
	INT iIndex = GET_OBJECTIVE_DAY() % NUM_JOB_OBJECTIVES_IN_POOL
	IF g_sMPTunables.idaily_collectables_override_index != -1
		iIndex = g_sMPTunables.idaily_collectables_override_index
		PRINTLN(" [KW Collectables]  GET_DAILY_COLLECTABLE_TRICK_OR_TREAT_IN_ORDER_ACCORDING_TO_DAY g_sMPTunables.idaily_collectables_override_index != -1 OVERRIDING DAY : = ", iIndex)

	ENDIF
	PRINTLN(" [KW Collectables]  GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY  iIndex = ", iIndex)


	SWITCH iIndex

			CASE  0 	
				iSkydives_location[0] = 16
				iSkydives_location[1] = 9
				iSkydives_location[2] = 23
				iSkydives_location[3] = 24
				iSkydives_location[4] = 6
				iSkydives_location[5] = 19
				iSkydives_location[6] = 12
				iSkydives_location[7] = 5
				iSkydives_location[8] = 2
				iSkydives_location[9] = 4
			BREAK
			
			CASE 1        
				iSkydives_location[0] =4 
				iSkydives_location[1] =11 
				iSkydives_location[2] =23 
				iSkydives_location[3] =13 
				iSkydives_location[4] =3 
				iSkydives_location[5] =16 
				iSkydives_location[6] =6 
				iSkydives_location[7] =22 
				iSkydives_location[8] =24 
				iSkydives_location[9] =18
			BREAK
			
			CASE 2       
				iSkydives_location[0] =0 
				iSkydives_location[1] =7 
				iSkydives_location[2] =6 
				iSkydives_location[3] =21 
				iSkydives_location[4] =1 
				iSkydives_location[5] =5 
				iSkydives_location[6] =15 
				iSkydives_location[7] =8 
				iSkydives_location[8] =23 
				iSkydives_location[9] =24
			BREAK
			
			CASE 3        
				iSkydives_location[0] =6 
				iSkydives_location[1] =20 
				iSkydives_location[2] =14 
				iSkydives_location[3] =15 
				iSkydives_location[4] =22 
				iSkydives_location[5] =12 
				iSkydives_location[6] =13 
				iSkydives_location[7] =7 
				iSkydives_location[8] =18 
				iSkydives_location[9] =10
			BREAK
			
			CASE 4        
				iSkydives_location[0] =13 
				iSkydives_location[1] =9 
				iSkydives_location[2] =0 
				iSkydives_location[3] =20 
				iSkydives_location[4] =22 
				iSkydives_location[5] =11 
				iSkydives_location[6] =19 
				iSkydives_location[7] =8 
				iSkydives_location[8] =16 
				iSkydives_location[9] =7
			BREAK
			
			CASE 5        
				iSkydives_location[0] =11 
				iSkydives_location[1] =4 
				iSkydives_location[2] =7 
				iSkydives_location[3] =19 
				iSkydives_location[4] =14 
				iSkydives_location[5] =2 
				iSkydives_location[6] =20 
				iSkydives_location[7] =16 
				iSkydives_location[8] =9 
				iSkydives_location[9] =23
			BREAK
			
			CASE 6        
				iSkydives_location[0] =6 
				iSkydives_location[1] =2 
				iSkydives_location[2] =18 
				iSkydives_location[3] =4 
				iSkydives_location[4] =21 
				iSkydives_location[5] =9 
				iSkydives_location[6] =1 
				iSkydives_location[7] =3 
				iSkydives_location[8] =11 
				iSkydives_location[9] =5
			BREAK
			
			CASE 7        
				iSkydives_location[0] =18 
				iSkydives_location[1] =14 
				iSkydives_location[2] =24 
				iSkydives_location[3] =23 
				iSkydives_location[4] =19 
				iSkydives_location[5] =9 
				iSkydives_location[6] =12 
				iSkydives_location[7] =22 
				iSkydives_location[8] =3 
				iSkydives_location[9] =15
			BREAK
			
			CASE 8        
				iSkydives_location[0] =6 
				iSkydives_location[1] =9 
				iSkydives_location[2] =24 
				iSkydives_location[3] =5 
				iSkydives_location[4] =16 
				iSkydives_location[5] =4 
				iSkydives_location[6] =11 
				iSkydives_location[7] =23 
				iSkydives_location[8] =15 
				iSkydives_location[9] =18
			BREAK
			
			CASE 9        
				iSkydives_location[0] =  13 
				iSkydives_location[1] =  4 
				iSkydives_location[2] =  7 
				iSkydives_location[3] =  0 
				iSkydives_location[4] =  3 
				iSkydives_location[5] =  9 
				iSkydives_location[6] =  1 
				iSkydives_location[7] =  20 
				iSkydives_location[8] =  16 
				iSkydives_location[9] =  11
			BREAK
			
			CASE 10       
				iSkydives_location[0] = 19 
				iSkydives_location[1] = 2 
				iSkydives_location[2] = 22 
				iSkydives_location[3] = 5 
				iSkydives_location[4] = 16 
				iSkydives_location[5] = 4 
				iSkydives_location[6] = 7 
				iSkydives_location[7] = 20 
				iSkydives_location[8] = 12 
				iSkydives_location[9] = 18
			BREAK
			
			CASE 11       
				iSkydives_location[0] = 15 
				iSkydives_location[1] = 18 
				iSkydives_location[2] = 1 
				iSkydives_location[3] = 24 
				iSkydives_location[4] = 8 
				iSkydives_location[5] = 2 
				iSkydives_location[6] = 20 
				iSkydives_location[7] = 21 
				iSkydives_location[8] = 10 
				iSkydives_location[9] = 4
			BREAK
			
			CASE 12       
				iSkydives_location[0] = 18 
				iSkydives_location[1] = 12 
				iSkydives_location[2] = 8 
				iSkydives_location[3] = 24 
				iSkydives_location[4] = 11 
				iSkydives_location[5] = 17 
				iSkydives_location[6] = 23 
				iSkydives_location[7] = 15 
				iSkydives_location[8] = 20 
				iSkydives_location[9] = 2
			BREAK
			
			CASE 13       
				iSkydives_location[0] = 4 
				iSkydives_location[1] = 3 
				iSkydives_location[2] = 16 
				iSkydives_location[3] = 8 
				iSkydives_location[4] = 2 
				iSkydives_location[5] = 0 
				iSkydives_location[6] = 11 
				iSkydives_location[7] = 5 
				iSkydives_location[8] = 10 
				iSkydives_location[9] = 17
			BREAK
			
			CASE 14       
				iSkydives_location[0] = 4 
				iSkydives_location[1] = 7 
				iSkydives_location[2] = 16 
				iSkydives_location[3] = 21 
				iSkydives_location[4] = 22 
				iSkydives_location[5] = 14 
				iSkydives_location[6] = 20 
				iSkydives_location[7] = 18 
				iSkydives_location[8] = 3 
				iSkydives_location[9] = 1
			BREAK
			
			CASE 15       
				iSkydives_location[0] = 10 
				iSkydives_location[1] = 22 
				iSkydives_location[2] = 3 
				iSkydives_location[3] = 18 
				iSkydives_location[4] = 1 
				iSkydives_location[5] = 7 
				iSkydives_location[6] = 5 
				iSkydives_location[7] = 8 
				iSkydives_location[8] = 2 
				iSkydives_location[9] = 17
			BREAK
			
			CASE 16       
				iSkydives_location[0] = 5 
				iSkydives_location[1] = 24 
				iSkydives_location[2] = 11 
				iSkydives_location[3] = 23 
				iSkydives_location[4] = 14 
				iSkydives_location[5] = 20 
				iSkydives_location[6] = 2 
				iSkydives_location[7] = 1 
				iSkydives_location[8] = 21 
				iSkydives_location[9] = 17
			BREAK
			
			CASE 17       
				iSkydives_location[0] = 9 
				iSkydives_location[1] = 15 
				iSkydives_location[2] = 5 
				iSkydives_location[3] = 23 
				iSkydives_location[4] = 24 
				iSkydives_location[5] = 2 
				iSkydives_location[6] = 14 
				iSkydives_location[7] = 18 
				iSkydives_location[8] = 0 
				iSkydives_location[9] = 22
			BREAK
			
			CASE 18       
				iSkydives_location[0] = 17 
				iSkydives_location[1] = 16 
				iSkydives_location[2] = 22 
				iSkydives_location[3] = 24 
				iSkydives_location[4] = 14 
				iSkydives_location[5] = 9 
				iSkydives_location[6] = 18 
				iSkydives_location[7] = 21 
				iSkydives_location[8] = 2 
				iSkydives_location[9] = 3
			BREAK
			
			CASE 19       
				iSkydives_location[0] = 20 
				iSkydives_location[1] = 10 
				iSkydives_location[2] = 15 
				iSkydives_location[3] = 9 
				iSkydives_location[4] = 23 
				iSkydives_location[5] = 7 
				iSkydives_location[6] = 24 
				iSkydives_location[7] = 8 
				iSkydives_location[8] = 6 
				iSkydives_location[9] = 2
			BREAK
			
			CASE 20       
				iSkydives_location[0] = 2 
				iSkydives_location[1] = 3 
				iSkydives_location[2] = 11 
				iSkydives_location[3] = 24 
				iSkydives_location[4] = 10 
				iSkydives_location[5] = 5 
				iSkydives_location[6] = 7 
				iSkydives_location[7] = 20 
				iSkydives_location[8] = 23 
				iSkydives_location[9] = 8
			BREAK
			
			CASE 21       
				iSkydives_location[0] = 6 
				iSkydives_location[1] = 18 
				iSkydives_location[2] = 0 
				iSkydives_location[3] = 4 
				iSkydives_location[4] = 22 
				iSkydives_location[5] = 23 
				iSkydives_location[6] = 15 
				iSkydives_location[7] = 8 
				iSkydives_location[8] = 20 
				iSkydives_location[9] = 19
			BREAK
			
			CASE 22       
				iSkydives_location[0] = 14 
				iSkydives_location[1] = 16 
				iSkydives_location[2] = 20 
				iSkydives_location[3] = 9 
				iSkydives_location[4] = 24 
				iSkydives_location[5] = 8 
				iSkydives_location[6] = 0 
				iSkydives_location[7] = 15 
				iSkydives_location[8] = 18 
				iSkydives_location[9] = 3
			BREAK
			
			CASE 23       
				iSkydives_location[0] = 22 
				iSkydives_location[1] = 6 
				iSkydives_location[2] = 14 
				iSkydives_location[3] = 3 
				iSkydives_location[4] = 11 
				iSkydives_location[5] = 19 
				iSkydives_location[6] = 21 
				iSkydives_location[7] = 23 
				iSkydives_location[8] = 1
				iSkydives_location[9] = 9
			BREAK
			
			CASE 24       
				iSkydives_location[0] = 21 
				iSkydives_location[1] = 18 
				iSkydives_location[2] = 3 
				iSkydives_location[3] = 17 
				iSkydives_location[4] = 22 
				iSkydives_location[5] = 13 
				iSkydives_location[6] = 5 
				iSkydives_location[7] = 16 
				iSkydives_location[8] = 2 
				iSkydives_location[9] = 4
			BREAK
			
			CASE 25       
				iSkydives_location[0] = 20 
				iSkydives_location[1] = 10 
				iSkydives_location[2] = 24 
				iSkydives_location[3] = 21 
				iSkydives_location[4] = 19 
				iSkydives_location[5] = 6 
				iSkydives_location[6] = 2 
				iSkydives_location[7] = 11 
				iSkydives_location[8] = 17 
				iSkydives_location[9] = 3
			BREAK
			
			CASE 26       
				iSkydives_location[0] = 16 
				iSkydives_location[1] = 12 
				iSkydives_location[2] = 20 
				iSkydives_location[3] = 6 
				iSkydives_location[4] = 24 
				iSkydives_location[5] = 10 
				iSkydives_location[6] = 0 
				iSkydives_location[7] = 1 
				iSkydives_location[8] = 3 
				iSkydives_location[9] = 13
			BREAK
			
			CASE 27       
				iSkydives_location[0] = 21 
				iSkydives_location[1] = 18 
				iSkydives_location[2] = 11 
				iSkydives_location[3] = 4 
				iSkydives_location[4] = 24 
				iSkydives_location[5] = 0 
				iSkydives_location[6] = 19 
				iSkydives_location[7] = 15 
				iSkydives_location[8] = 5 
				iSkydives_location[9] = 7
			BREAK
			
			CASE 28       
				iSkydives_location[0] = 22 
				iSkydives_location[1] = 1 
				iSkydives_location[2] = 18 
				iSkydives_location[3] = 24 
				iSkydives_location[4] = 17 
				iSkydives_location[5] = 7 
				iSkydives_location[6] = 9 
				iSkydives_location[7] = 10 
				iSkydives_location[8] = 5 
				iSkydives_location[9] = 16
			BREAK
			
			CASE 29       
				iSkydives_location[0] = 16 
				iSkydives_location[1] = 1 
				iSkydives_location[2] = 15 
				iSkydives_location[3] = 0 
				iSkydives_location[4] = 11 
				iSkydives_location[5] = 18 
				iSkydives_location[6] = 7 
				iSkydives_location[7] = 14 
				iSkydives_location[8] = 13 
				iSkydives_location[9] = 12
			BREAK
			
			CASE 30       
				iSkydives_location[0] = 9 
				iSkydives_location[1] = 13 
				iSkydives_location[2] = 2 
				iSkydives_location[3] = 19 
				iSkydives_location[4] = 21 
				iSkydives_location[5] = 22 
				iSkydives_location[6] = 11 
				iSkydives_location[7] = 0 
				iSkydives_location[8] = 10 
				iSkydives_location[9] = 15
			BREAK
			
			CASE 31       
				iSkydives_location[0] = 1 
				iSkydives_location[1] = 7 
				iSkydives_location[2] = 15 
				iSkydives_location[3] = 22 
				iSkydives_location[4] = 14 
				iSkydives_location[5] = 11 
				iSkydives_location[6] = 16 
				iSkydives_location[7] = 0 
				iSkydives_location[8] = 12 
				iSkydives_location[9] = 3
			BREAK
			
			CASE 32       
				iSkydives_location[0] = 14 
				iSkydives_location[1] = 12 
				iSkydives_location[2] = 1 
				iSkydives_location[3] = 17 
				iSkydives_location[4] = 20 
				iSkydives_location[5] = 16 
				iSkydives_location[6] = 2 
				iSkydives_location[7] = 4 
				iSkydives_location[8] = 9 
				iSkydives_location[9] = 15
			BREAK
			
			CASE 33       
				iSkydives_location[0] = 0 
				iSkydives_location[1] = 18 
				iSkydives_location[2] = 7 
				iSkydives_location[3] = 3 
				iSkydives_location[4] = 15 
				iSkydives_location[5] = 11 
				iSkydives_location[6] = 13 
				iSkydives_location[7] = 2 
				iSkydives_location[8] = 6 
				iSkydives_location[9] = 10
			BREAK
			
			CASE 34       
				iSkydives_location[0] = 16 
				iSkydives_location[1] = 11 
				iSkydives_location[2] = 20 
				iSkydives_location[3] = 18 
				iSkydives_location[4] = 12 
				iSkydives_location[5] = 3 
				iSkydives_location[6] = 6 
				iSkydives_location[7] = 24 
				iSkydives_location[8] = 1 
				iSkydives_location[9] = 17
			BREAK
			
			CASE 35       
				iSkydives_location[0] = 19 
				iSkydives_location[1] = 6 
				iSkydives_location[2] = 2 
				iSkydives_location[3] = 24 
				iSkydives_location[4] = 14 
				iSkydives_location[5] = 21 
				iSkydives_location[6] = 12 
				iSkydives_location[7] = 20 
				iSkydives_location[8] = 16 
				iSkydives_location[9] = 11
			BREAK
			
			CASE 36      
				iSkydives_location[0] = 14 
				iSkydives_location[1] = 16 
				iSkydives_location[2] = 0 
				iSkydives_location[3] = 19 
				iSkydives_location[4] = 11 
				iSkydives_location[5] = 24 
				iSkydives_location[6] = 7 
				iSkydives_location[7] = 6 
				iSkydives_location[8] = 2 
				iSkydives_location[9] = 5
			BREAK
			
			CASE 37       
				iSkydives_location[0] = 9 
				iSkydives_location[1] = 18 
				iSkydives_location[2] = 22 
				iSkydives_location[3] = 1 
				iSkydives_location[4] = 14 
				iSkydives_location[5] = 13 
				iSkydives_location[6] = 7 
				iSkydives_location[7] = 0 
				iSkydives_location[8] = 11 
				iSkydives_location[9] = 20
			BREAK
			
			CASE 38       
				iSkydives_location[0] = 23 
				iSkydives_location[1] = 22 
				iSkydives_location[2] = 3 
				iSkydives_location[3] = 9 
				iSkydives_location[4] = 17 
				iSkydives_location[5] = 10 
				iSkydives_location[6] = 15 
				iSkydives_location[7] = 20 
				iSkydives_location[8] = 21 
				iSkydives_location[9] = 8
			BREAK
			
			CASE 39       
				iSkydives_location[0] = 15 
				iSkydives_location[1] = 11 
				iSkydives_location[2] = 7 
				iSkydives_location[3] = 10 
				iSkydives_location[4] = 23 
				iSkydives_location[5] = 19 
				iSkydives_location[6] = 6 
				iSkydives_location[7] = 5 
				iSkydives_location[8] = 20 
				iSkydives_location[9] = 0
			BREAK
			
			CASE 40       
				iSkydives_location[0] = 23 
				iSkydives_location[1] = 15 
				iSkydives_location[2] = 5 
				iSkydives_location[3] = 20 
				iSkydives_location[4] = 22 
				iSkydives_location[5] = 0 
				iSkydives_location[6] = 8 
				iSkydives_location[7] = 6 
				iSkydives_location[8] = 3 
				iSkydives_location[9] = 19
			BREAK
			
			CASE 41       
				iSkydives_location[0] = 22 
				iSkydives_location[1] = 12 
				iSkydives_location[2] = 7 
				iSkydives_location[3] = 1 
				iSkydives_location[4] = 14 
				iSkydives_location[5] = 13 
				iSkydives_location[6] = 16 
				iSkydives_location[7] = 3 
				iSkydives_location[8] = 8 
				iSkydives_location[9] = 5
			BREAK
			
			CASE 42       
				iSkydives_location[0] = 16 
				iSkydives_location[1] = 10 
				iSkydives_location[2] = 17 
				iSkydives_location[3] = 12 
				iSkydives_location[4] = 2 
				iSkydives_location[5] = 22 
				iSkydives_location[6] = 5 
				iSkydives_location[7] = 6 
				iSkydives_location[8] = 8 
				iSkydives_location[9] = 23
			BREAK
			
			CASE 43       
				iSkydives_location[0] = 24 
				iSkydives_location[1] = 17 
				iSkydives_location[2] = 20 
				iSkydives_location[3] = 2 
				iSkydives_location[4] = 11 
				iSkydives_location[5] = 1 
				iSkydives_location[6] = 10 
				iSkydives_location[7] = 9 
				iSkydives_location[8] = 12 
				iSkydives_location[9] = 13
			BREAK
			
			CASE 44       
				iSkydives_location[0] = 15 
				iSkydives_location[1] = 23 
				iSkydives_location[2] = 16 
				iSkydives_location[3] = 14 
				iSkydives_location[4] = 1 
				iSkydives_location[5] = 11 
				iSkydives_location[6] = 0 
				iSkydives_location[7] = 24 
				iSkydives_location[8] = 6 
				iSkydives_location[9] = 8
			BREAK
			
			
			CASE 45       
				iSkydives_location[0] = 17 
				iSkydives_location[1] = 8 
				iSkydives_location[2] = 7 
				iSkydives_location[3] = 13 
				iSkydives_location[4] = 23 
				iSkydives_location[5] = 22 
				iSkydives_location[6] = 20 
				iSkydives_location[7] = 10 
				iSkydives_location[8] = 14 
				iSkydives_location[9] = 4
			BREAK
			
			
			CASE 46       
				iSkydives_location[0] = 8 
				iSkydives_location[1] = 12 
				iSkydives_location[2] = 11 
				iSkydives_location[3] = 10 
				iSkydives_location[4] = 23 
				iSkydives_location[5] = 0 
				iSkydives_location[6] = 2 
				iSkydives_location[7] = 17 
				iSkydives_location[8] = 16 
				iSkydives_location[9] = 6
			BREAK
			
			
			CASE 47       
				iSkydives_location[0] = 9 
				iSkydives_location[1] = 19 
				iSkydives_location[2] = 20 
				iSkydives_location[3] = 17 
				iSkydives_location[4] = 23 
				iSkydives_location[5] = 3 
				iSkydives_location[6] = 10 
				iSkydives_location[7] = 13 
				iSkydives_location[8] = 5 
				iSkydives_location[9] = 12
			BREAK
			
			CASE 48       
				iSkydives_location[0] = 16 
				iSkydives_location[1] = 24 
				iSkydives_location[2] = 2 
				iSkydives_location[3] = 18 
				iSkydives_location[4] = 13 
				iSkydives_location[5] = 5 
				iSkydives_location[6] = 15 
				iSkydives_location[7] = 10 
				iSkydives_location[8] = 12 
				iSkydives_location[9] = 14
			BREAK
			
			CASE 49       
				iSkydives_location[0] = 7 
				iSkydives_location[1] = 19 
				iSkydives_location[2] = 8 
				iSkydives_location[3] = 1 
				iSkydives_location[4] = 0 
				iSkydives_location[5] = 15 
				iSkydives_location[6] = 12 
				iSkydives_location[7] = 4 
				iSkydives_location[8] = 24 
				iSkydives_location[9] = 10
			BREAK
			
			CASE 50       
				iSkydives_location[0] = 20 
				iSkydives_location[1] = 3 
				iSkydives_location[2] = 23 
				iSkydives_location[3] = 2 
				iSkydives_location[4] = 4 
				iSkydives_location[5] = 5 
				iSkydives_location[6] = 13 
				iSkydives_location[7] = 0 
				iSkydives_location[8] = 24 
				iSkydives_location[9] = 19
			BREAK
			
			CASE 51       
				iSkydives_location[0] = 3 
				iSkydives_location[1] = 22 
				iSkydives_location[2] = 11 
				iSkydives_location[3] = 4 
				iSkydives_location[4] = 1 
				iSkydives_location[5] = 19 
				iSkydives_location[6] = 20 
				iSkydives_location[7] = 15 
				iSkydives_location[8] = 2 
				iSkydives_location[9] = 10
			BREAK
			
			CASE 52       
				iSkydives_location[0] = 17 
				iSkydives_location[1] = 4 
				iSkydives_location[2] = 10 
				iSkydives_location[3] = 16 
				iSkydives_location[4] = 23 
				iSkydives_location[5] = 14 
				iSkydives_location[6] = 6 
				iSkydives_location[7] = 20 
				iSkydives_location[8] = 5 
				iSkydives_location[9] = 0
			BREAK
			
			CASE 53       
				iSkydives_location[0] = 14 
				iSkydives_location[1] = 11 
				iSkydives_location[2] = 22 
				iSkydives_location[3] = 19 
				iSkydives_location[4] = 0 
				iSkydives_location[5] = 8 
				iSkydives_location[6] = 24 
				iSkydives_location[7] = 6 
				iSkydives_location[8] = 20 
				iSkydives_location[9] = 9
			BREAK
			
			CASE 54       
				iSkydives_location[0] = 14 
				iSkydives_location[1] = 21 
				iSkydives_location[2] = 22 
				iSkydives_location[3] = 1 
				iSkydives_location[4] = 16 
				iSkydives_location[5] = 3 
				iSkydives_location[6] = 15 
				iSkydives_location[7] = 5 
				iSkydives_location[8] = 7 
				iSkydives_location[9] = 9
			BREAK
			
			CASE 55       
				iSkydives_location[0] = 14 
				iSkydives_location[1] = 22 
				iSkydives_location[2] = 17 
				iSkydives_location[3] = 11 
				iSkydives_location[4] = 19 
				iSkydives_location[5] = 6 
				iSkydives_location[6] = 2 
				iSkydives_location[7] = 8 
				iSkydives_location[8] = 13 
				iSkydives_location[9] = 1
			BREAK
			
			CASE 56       
				iSkydives_location[0] = 3 
				iSkydives_location[1] = 8 
				iSkydives_location[2] = 20 
				iSkydives_location[3] = 13 
				iSkydives_location[4] = 15 
				iSkydives_location[5] = 22 
				iSkydives_location[6] = 5 
				iSkydives_location[7] = 10 
				iSkydives_location[8] = 2 
				iSkydives_location[9] = 16
			BREAK
			
			
			CASE 57       
				iSkydives_location[0] = 12 
				iSkydives_location[1] = 17 
				iSkydives_location[2] = 19 
				iSkydives_location[3] = 4 
				iSkydives_location[4] = 5 
				iSkydives_location[5] = 18 
				iSkydives_location[6] = 14 
				iSkydives_location[7] = 7 
				iSkydives_location[8] = 2 
				iSkydives_location[9] = 15
			BREAK
			
			CASE 58       
				iSkydives_location[0] = 1 
				iSkydives_location[1] = 4 
				iSkydives_location[2] = 2 
				iSkydives_location[3] = 21 
				iSkydives_location[4] = 12 
				iSkydives_location[5] = 7 
				iSkydives_location[6] = 16 
				iSkydives_location[7] = 6 
				iSkydives_location[8] = 20 
				iSkydives_location[9] = 14
			BREAK
			
			
			CASE 59       
				iSkydives_location[0] = 9 
				iSkydives_location[1] = 8 
				iSkydives_location[2] = 13 
				iSkydives_location[3] = 16 
				iSkydives_location[4] = 23 
				iSkydives_location[5] = 10 
				iSkydives_location[6] = 24 
				iSkydives_location[7] = 14 
				iSkydives_location[8] = 11 
				iSkydives_location[9] = 2
			BREAK
			
			
			CASE 60       
				iSkydives_location[0] = 11 
				iSkydives_location[1] = 24 
				iSkydives_location[2] = 23 
				iSkydives_location[3] = 7 
				iSkydives_location[4] = 2 
				iSkydives_location[5] = 1 
				iSkydives_location[6] = 9 
				iSkydives_location[7] = 19 
				iSkydives_location[8] = 14 
				iSkydives_location[9] = 12
			BREAK
			
			
			CASE 61       
				iSkydives_location[0] = 2 
				iSkydives_location[1] = 16 
				iSkydives_location[2] = 4 
				iSkydives_location[3] = 9 
				iSkydives_location[4] = 12 
				iSkydives_location[5] = 6 
				iSkydives_location[6] = 22 
				iSkydives_location[7] = 21 
				iSkydives_location[8] = 18 
				iSkydives_location[9] = 14
			BREAK
			
			CASE 62       
				iSkydives_location[0] = 23 
				iSkydives_location[1] = 8 
				iSkydives_location[2] = 2 
				iSkydives_location[3] = 21 
				iSkydives_location[4] = 13 
				iSkydives_location[5] = 17 
				iSkydives_location[6] = 18 
				iSkydives_location[7] = 6 
				iSkydives_location[8] = 16 
				iSkydives_location[9] = 3
			BREAK
			
			CASE 63       
				iSkydives_location[0] = 11 
				iSkydives_location[1] = 3 
				iSkydives_location[2] = 23 
				iSkydives_location[3] = 8 
				iSkydives_location[4] = 6 
				iSkydives_location[5] = 19 
				iSkydives_location[6] = 24 
				iSkydives_location[7] = 10 
				iSkydives_location[8] = 16 
				iSkydives_location[9] = 13
			BREAK
			
			CASE 64       
				iSkydives_location[0] = 12 
				iSkydives_location[1] = 10 
				iSkydives_location[2] = 19 
				iSkydives_location[3] = 21 
				iSkydives_location[4] = 15 
				iSkydives_location[5] = 20 
				iSkydives_location[6] = 4 
				iSkydives_location[7] = 6 
				iSkydives_location[8] = 8 
				iSkydives_location[9] = 0
			BREAK
			
			CASE 65       
				iSkydives_location[0] = 18 
				iSkydives_location[1] = 9 
				iSkydives_location[2] = 22 
				iSkydives_location[3] = 1 
				iSkydives_location[4] = 12 
				iSkydives_location[5] = 23 
				iSkydives_location[6] = 8 
				iSkydives_location[7] = 6 
				iSkydives_location[8] = 19 
				iSkydives_location[9] = 10
			BREAK
			
			CASE 66       
				iSkydives_location[0] = 15 
				iSkydives_location[1] = 19 
				iSkydives_location[2] = 10 
				iSkydives_location[3] = 8 
				iSkydives_location[4] = 12 
				iSkydives_location[5] = 5 
				iSkydives_location[6] = 13 
				iSkydives_location[7] = 17 
				iSkydives_location[8] = 22 
				iSkydives_location[9] = 0
			BREAK
			
			CASE 67       
				iSkydives_location[0] = 0 
				iSkydives_location[1] = 5 
				iSkydives_location[2] = 7 
				iSkydives_location[3] = 19 
				iSkydives_location[4] = 9 
				iSkydives_location[5] = 6 
				iSkydives_location[6] = 10 
				iSkydives_location[7] = 12 
				iSkydives_location[8] = 1 
				iSkydives_location[9] = 22
			BREAK
			
			CASE 68       
				iSkydives_location[0] = 20 
				iSkydives_location[1] = 6 
				iSkydives_location[2] = 18 
				iSkydives_location[3] = 1 
				iSkydives_location[4] = 11 
				iSkydives_location[5] = 8 
				iSkydives_location[6] = 3 
				iSkydives_location[7] = 24 
				iSkydives_location[8] = 16 
				iSkydives_location[9] = 23
			BREAK
			
			CASE 69       
				iSkydives_location[0] = 13 
				iSkydives_location[1] = 1 
				iSkydives_location[2] = 18 
				iSkydives_location[3] = 12 
				iSkydives_location[4] = 22 
				iSkydives_location[5] = 20 
				iSkydives_location[6] = 0 
				iSkydives_location[7] = 8 
				iSkydives_location[8] = 16 
				iSkydives_location[9] = 5
			BREAK
			
			CASE 70       
				iSkydives_location[0] = 22 
				iSkydives_location[1] = 4 
				iSkydives_location[2] = 7 
				iSkydives_location[3] = 10 
				iSkydives_location[4] = 12 
				iSkydives_location[5] = 15 
				iSkydives_location[6] = 16 
				iSkydives_location[7] = 9 
				iSkydives_location[8] = 6 
				iSkydives_location[9] = 18
			BREAK
			
			CASE 71       
				iSkydives_location[0] = 17 
				iSkydives_location[1] = 23 
				iSkydives_location[2] = 24 
				iSkydives_location[3] = 12 
				iSkydives_location[4] = 22 
				iSkydives_location[5] = 14 
				iSkydives_location[6] = 7 
				iSkydives_location[7] = 19 
				iSkydives_location[8] = 4 
				iSkydives_location[9] = 11
			BREAK
			
			CASE 72       
				iSkydives_location[0] = 21 
				iSkydives_location[1] = 8 
				iSkydives_location[2] = 1 
				iSkydives_location[3] = 13 
				iSkydives_location[4] = 23 
				iSkydives_location[5] = 9 
				iSkydives_location[6] = 20 
				iSkydives_location[7] = 7 
				iSkydives_location[8] = 0 
				iSkydives_location[9] = 10
			BREAK
			
			CASE 73       
				iSkydives_location[0] = 10 
				iSkydives_location[1] = 8 
				iSkydives_location[2] = 11 
				iSkydives_location[3] = 24 
				iSkydives_location[4] = 19 
				iSkydives_location[5] = 1 
				iSkydives_location[6] = 18 
				iSkydives_location[7] = 6 
				iSkydives_location[8] = 20 
				iSkydives_location[9] = 9
			BREAK
			
			CASE 74       
				iSkydives_location[0] = 7 
				iSkydives_location[1] = 22 
				iSkydives_location[2] = 10 
				iSkydives_location[3] = 17 
				iSkydives_location[4] = 14 
				iSkydives_location[5] = 18 
				iSkydives_location[6] = 4 
				iSkydives_location[7] = 21 
				iSkydives_location[8] = 0 
				iSkydives_location[9] = 6
			BREAK
			
			CASE 75      
				iSkydives_location[0] = 2 
				iSkydives_location[1] = 21 
				iSkydives_location[2] = 7 
				iSkydives_location[3] = 16 
				iSkydives_location[4] = 24 
				iSkydives_location[5] = 19 
				iSkydives_location[6] = 3 
				iSkydives_location[7] = 12 
				iSkydives_location[8] = 6 
				iSkydives_location[9] = 17
			BREAK
			
			CASE 76       
				iSkydives_location[0] = 21 
				iSkydives_location[1] = 15 
				iSkydives_location[2] = 5 
				iSkydives_location[3] = 22 
				iSkydives_location[4] = 18 
				iSkydives_location[5] = 8 
				iSkydives_location[6] = 13 
				iSkydives_location[7] = 7 
				iSkydives_location[8] = 1 
				iSkydives_location[9] = 20
			BREAK
			
			CASE 77       
				iSkydives_location[0] = 4 
				iSkydives_location[1] = 18 
				iSkydives_location[2] = 5 
				iSkydives_location[3] = 7
				iSkydives_location[4] = 20 
				iSkydives_location[5] = 2 
				iSkydives_location[6] = 11 
				iSkydives_location[7] = 19 
				iSkydives_location[8] = 21 
				iSkydives_location[9] = 1
			BREAK
			
			CASE 78       
				iSkydives_location[0] = 14 
				iSkydives_location[1] = 4 
				iSkydives_location[2] = 17 
				iSkydives_location[3] = 6 
				iSkydives_location[4] = 20 
				iSkydives_location[5] = 0 
				iSkydives_location[6] = 8 
				iSkydives_location[7] = 22 
				iSkydives_location[8] = 16 
				iSkydives_location[9] = 11
			BREAK
			
			CASE 79       
				iSkydives_location[0] = 6 
				iSkydives_location[1] = 17 
				iSkydives_location[2] = 7 
				iSkydives_location[3] = 20 
				iSkydives_location[4] = 4 
				iSkydives_location[5] = 16 
				iSkydives_location[6] = 14 
				iSkydives_location[7] = 21 
				iSkydives_location[8] = 22 
				iSkydives_location[9] = 2
			BREAK
			
			
			CASE 80      
				iSkydives_location[0] = 3 
				iSkydives_location[1] = 23 
				iSkydives_location[2] = 17 
				iSkydives_location[3] = 2 
				iSkydives_location[4] = 9 
				iSkydives_location[5] = 18 
				iSkydives_location[6] = 7 
				iSkydives_location[7] = 6 
				iSkydives_location[8] = 13 
				iSkydives_location[9] = 12
			BREAK
			
			
			CASE 81       
				iSkydives_location[0] = 15 
				iSkydives_location[1] = 21 
				iSkydives_location[2] = 10 
				iSkydives_location[3] = 9 
				iSkydives_location[4] = 4 
				iSkydives_location[5] = 23 
				iSkydives_location[6] = 18 
				iSkydives_location[7] = 1 
				iSkydives_location[8] = 6 
				iSkydives_location[9] = 17
			BREAK
			
			
			CASE 82       
				iSkydives_location[0] = 10 
				iSkydives_location[1] = 20 
				iSkydives_location[2] = 6 
				iSkydives_location[3] = 9 
				iSkydives_location[4] = 0 
				iSkydives_location[5] = 18 
				iSkydives_location[6] = 12 
				iSkydives_location[7] = 23 
				iSkydives_location[8] = 19 
				iSkydives_location[9] = 2
			BREAK
			
			
			CASE 83      
				iSkydives_location[0] = 7 
				iSkydives_location[1] = 14 
				iSkydives_location[2] = 21
				iSkydives_location[3] = 4
				iSkydives_location[4] = 8 
				iSkydives_location[5] = 12 
				iSkydives_location[6] = 6 
				iSkydives_location[7] = 18 
				iSkydives_location[8] = 24 
				iSkydives_location[9] = 2
			BREAK
	ENDSWITCH
	
	
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_Skydives0, iSkydives_location[0]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_Skydives1, iSkydives_location[1]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_Skydives2, iSkydives_location[2]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_Skydives3, iSkydives_location[3]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_Skydives4, iSkydives_location[4]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_Skydives5, iSkydives_location[5]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_Skydives6, iSkydives_location[6]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_Skydives7, iSkydives_location[7]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_Skydives8, iSkydives_location[8]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_Skydives9, iSkydives_location[9]  )

	#IF IS_DEBUG_BUILD
		g_iactive_Skydives[0] =  iSkydives_location[0]
		g_iactive_Skydives[1] =  iSkydives_location[1]
		g_iactive_Skydives[2] =  iSkydives_location[2]
		g_iactive_Skydives[3] =  iSkydives_location[3]
		g_iactive_Skydives[4] =  iSkydives_location[4]
		g_iactive_Skydives[5] =  iSkydives_location[5]
		g_iactive_Skydives[6] =  iSkydives_location[6]
		g_iactive_Skydives[7] =  iSkydives_location[7]
		g_iactive_Skydives[8] =  iSkydives_location[8]
		g_iactive_Skydives[9] =  iSkydives_location[9]
	#ENDIF 
	
	
	// after defining what jumps are going to be in for the day, check if the stats are saving the records for the same jumps (we saved the jump ID)
	// if the jump ID saved is not the same as the one defined for the day means the day has changed, so we have to reset the stats to -1
	PRINTLN("[SKYDIVING_CHALLENGE] ------- PACKED STATS INIT ------- ")
	INT iOrder
	INT iAllCheck, iAccurate, iParTime, iGold, iJump
	BOOL bReset = FALSE
	REPEAT 10 iOrder
		iAllCheck = GET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_ALLCHECKP0)+iOrder*4))
		iAccurate = GET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_ACCURATLN0)+iOrder*4))
		iParTime  = GET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_PARTIME0)+iOrder*4))
		iGold 	  = GET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_GOLD0)+iOrder*4))
		iJump	  = GET_MP_INT_CHARACTER_STAT(INT_TO_ENUM(MP_INT_STATS, ENUM_TO_INT(MP_STAT_DAILYCOLLECT_Skydives0)+iOrder))
		PRINTLN("[SKYDIVING_CHALLENGE] Jump      ",iJump)
		PRINTLN("[SKYDIVING_CHALLENGE] iAllCheck ",iAllCheck)
		PRINTLN("[SKYDIVING_CHALLENGE] iAccurate ",iAccurate)
		PRINTLN("[SKYDIVING_CHALLENGE] iParTime  ",iParTime)
		PRINTLN("[SKYDIVING_CHALLENGE] iGold     ",iGold)
		PRINTLN("[SKYDIVING_CHALLENGE] -------------- ")
		IF iAllCheck != 255 AND iAllCheck != iJump
		OR iAccurate != 255 AND iAccurate != iJump
		OR iParTime	 != 255 AND iParTime	 != iJump
		OR iGold	 != 255 AND iGold	 != iJump
			bReset = TRUE
//			BREAKLOOP
		ENDIF
	ENDREPEAT
	IF bReset
		PRINTLN("[SKYDIVING_CHALLENGE] Reseting the stats for the day")
		REPEAT 10 iOrder
			SET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_ALLCHECKP0)+iOrder*4),-1)
			SET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_ACCURATLN0)+iOrder*4),-1)
			SET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_PARTIME0)+iOrder*4),-1)
			SET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_GOLD0)+iOrder*4),-1)
		ENDREPEAT
	ENDIF
		
	CLEAR_BIT(g_sSkydivingFlowData.iBitset,ciSKYDIVING_CHALLENGE_BS_DAILY_DATA_INIT)
ENDPROC


#ENDIF 

#IF FEATURE_DLC_2_2022
PROC  GET_DAILY_COLLECTABLE_DEAD_DROP_IN_ORDER_ACCORDING_TO_DAY()
	INT	iDead_drop_location[1]  //4
	INT	iDead_drop_area[1] //14
	INT iIndex = GET_OBJECTIVE_DAY() % NUM_JOB_OBJECTIVES_IN_POOL
	IF g_sMPTunables.idaily_collectables_override_index != -1
		iIndex = g_sMPTunables.idaily_collectables_override_index
		PRINTLN(" [KW Collectables]  GET_DAILY_COLLECTABLE_DEAD_DROP_IN_ORDER_ACCORDING_TO_DAY g_sMPTunables.idaily_collectables_override_index != -1 OVERRIDING DAY : = ", iIndex)

	ENDIF
	PRINTLN(" [KW Collectables]  GET_DAILY_COLLECTABLE_DEAD_DROP_IN_ORDER_ACCORDING_TO_DAY  iIndex = ", iIndex)

	
		SWITCH iIndex
			CASE  0   iDead_drop_location[0] =  2	BREAK
			CASE  1   iDead_drop_location[0] =  0   BREAK
			CASE  2   iDead_drop_location[0] =  3   BREAK
			CASE  3   iDead_drop_location[0] =  0   BREAK
			CASE  4   iDead_drop_location[0] =  1   BREAK
			CASE  5   iDead_drop_location[0] =  1   BREAK
			CASE  6   iDead_drop_location[0] =  4   BREAK
			CASE  7   iDead_drop_location[0] =  1   BREAK
			CASE  8   iDead_drop_location[0] =  1   BREAK
			CASE  9   iDead_drop_location[0] =  1   BREAK
			CASE  10  iDead_drop_location[0] =  3   BREAK
			CASE  11  iDead_drop_location[0] =  4   BREAK
			CASE  12  iDead_drop_location[0] =  0   BREAK
			CASE  13  iDead_drop_location[0] =  1   BREAK
			CASE  14  iDead_drop_location[0] =  2   BREAK
			CASE  15  iDead_drop_location[0] =  1   BREAK
			CASE  16  iDead_drop_location[0] =  4   BREAK
			CASE  17  iDead_drop_location[0] =  2   BREAK
			CASE  18  iDead_drop_location[0] =  1   BREAK
			CASE  19  iDead_drop_location[0] =  0   BREAK
			CASE  20  iDead_drop_location[0] =  1   BREAK
			CASE  21  iDead_drop_location[0] =  3   BREAK
			CASE  22  iDead_drop_location[0] =  1   BREAK
			CASE  23  iDead_drop_location[0] =  1   BREAK
			CASE  24  iDead_drop_location[0] =  0   BREAK
			CASE  25  iDead_drop_location[0] =  2   BREAK
			CASE  26  iDead_drop_location[0] =  1   BREAK
			CASE  27  iDead_drop_location[0] =  2   BREAK
			CASE  28  iDead_drop_location[0] =  1   BREAK
			CASE  29  iDead_drop_location[0] =  2   BREAK
			CASE  30  iDead_drop_location[0] =  4   BREAK
			CASE  31  iDead_drop_location[0] =  2   BREAK
			CASE  32  iDead_drop_location[0] =  0   BREAK
			CASE  33  iDead_drop_location[0] =  1   BREAK
			CASE  34  iDead_drop_location[0] =  0   BREAK
			CASE  35  iDead_drop_location[0] =  0   BREAK
			CASE  36  iDead_drop_location[0] =  3   BREAK
			CASE  37  iDead_drop_location[0] =  3   BREAK
			CASE  38  iDead_drop_location[0] =  3   BREAK
			CASE  39  iDead_drop_location[0] =  0   BREAK
			CASE  40  iDead_drop_location[0] =  4   BREAK
			CASE  41  iDead_drop_location[0] =  0   BREAK
			CASE  42  iDead_drop_location[0] =  3   BREAK
			CASE  43  iDead_drop_location[0] =  1   BREAK
			CASE  44  iDead_drop_location[0] =  2   BREAK
			CASE  45  iDead_drop_location[0] =  0   BREAK
			CASE  46  iDead_drop_location[0] =  3   BREAK
			CASE  47  iDead_drop_location[0] =  0   BREAK
			CASE  48  iDead_drop_location[0] =  3   BREAK
			CASE  49  iDead_drop_location[0] =  4   BREAK
			CASE  50  iDead_drop_location[0] =  3   BREAK
			CASE  51  iDead_drop_location[0] =  1   BREAK
			CASE  52  iDead_drop_location[0] =  2   BREAK
			CASE  53  iDead_drop_location[0] =  2   BREAK
			CASE  54  iDead_drop_location[0] =  4   BREAK
			CASE  55  iDead_drop_location[0] =  1   BREAK
			CASE  56  iDead_drop_location[0] =  1   BREAK
			CASE  57  iDead_drop_location[0] =  2   BREAK
			CASE  58  iDead_drop_location[0] =  2   BREAK
			CASE  59  iDead_drop_location[0] =  1   BREAK
			CASE  60  iDead_drop_location[0] =  0   BREAK
			CASE  61  iDead_drop_location[0] =  1   BREAK
			CASE  62  iDead_drop_location[0] =  2   BREAK
			CASE  63  iDead_drop_location[0] =  3   BREAK
			CASE  64  iDead_drop_location[0] =  3   BREAK
			CASE  65  iDead_drop_location[0] =  3   BREAK
			CASE  66  iDead_drop_location[0] =  0   BREAK
			CASE  67  iDead_drop_location[0] =  2   BREAK
			CASE  68  iDead_drop_location[0] =  0   BREAK
			CASE  69  iDead_drop_location[0] =  0   BREAK
			CASE  70  iDead_drop_location[0] =  1   BREAK
			CASE  71  iDead_drop_location[0] =  4   BREAK
			CASE  72  iDead_drop_location[0] =  0   BREAK
			CASE  73  iDead_drop_location[0] =  4   BREAK
			CASE  74  iDead_drop_location[0] =  1   BREAK
			CASE  75  iDead_drop_location[0] =  3   BREAK
			CASE  76  iDead_drop_location[0] =  3   BREAK
			CASE  77  iDead_drop_location[0] =  0   BREAK
			CASE  78  iDead_drop_location[0] =  0   BREAK
			CASE  79  iDead_drop_location[0] =  4   BREAK
			CASE  80  iDead_drop_location[0] =  1   BREAK
			CASE  81  iDead_drop_location[0] =  2   BREAK
			CASE  82  iDead_drop_location[0] =  3   BREAK
			CASE  83  iDead_drop_location[0] =  2	BREAK
		ENDSWITCH
		
		SWITCH iIndex
			CASE  0  iDead_drop_area[0] = 6			BREAK
			CASE  1  iDead_drop_area[0] = 2         BREAK
			CASE  2  iDead_drop_area[0] = 3         BREAK
			CASE  3  iDead_drop_area[0] = 13        BREAK
			CASE  4  iDead_drop_area[0] = 11        BREAK
			CASE  5  iDead_drop_area[0] = 10        BREAK
			CASE  6  iDead_drop_area[0] = 2         BREAK
			CASE  7  iDead_drop_area[0] = 8         BREAK
			CASE  8  iDead_drop_area[0] = 14        BREAK
			CASE  9  iDead_drop_area[0] = 5         BREAK
			CASE  10 iDead_drop_area[0] = 6         BREAK
			CASE  11 iDead_drop_area[0] = 8         BREAK
			CASE  12 iDead_drop_area[0] = 9         BREAK
			CASE  13 iDead_drop_area[0] = 0         BREAK
			CASE  14 iDead_drop_area[0] = 1         BREAK
			CASE  15 iDead_drop_area[0] = 13        BREAK
			CASE  16 iDead_drop_area[0] = 8         BREAK
			CASE  17 iDead_drop_area[0] = 12        BREAK
			CASE  18 iDead_drop_area[0] = 5         BREAK
			CASE  19 iDead_drop_area[0] = 1         BREAK
			CASE  20 iDead_drop_area[0] = 5         BREAK
			CASE  21 iDead_drop_area[0] = 0         BREAK
			CASE  22 iDead_drop_area[0] = 4         BREAK
			CASE  23 iDead_drop_area[0] = 13        BREAK
			CASE  24 iDead_drop_area[0] = 9         BREAK
			CASE  25 iDead_drop_area[0] = 13        BREAK
			CASE  26 iDead_drop_area[0] = 5         BREAK
			CASE  27 iDead_drop_area[0] = 2         BREAK
			CASE  28 iDead_drop_area[0] = 8         BREAK
			CASE  29 iDead_drop_area[0] = 14        BREAK
			CASE  30 iDead_drop_area[0] = 6         BREAK
			CASE  31 iDead_drop_area[0] = 1         BREAK
			CASE  32 iDead_drop_area[0] = 9         BREAK
			CASE  33 iDead_drop_area[0] = 2         BREAK
			CASE  34 iDead_drop_area[0] = 0         BREAK
			CASE  35 iDead_drop_area[0] = 0         BREAK
			CASE  36 iDead_drop_area[0] = 13        BREAK
			CASE  37 iDead_drop_area[0] = 5         BREAK
			CASE  38 iDead_drop_area[0] = 4         BREAK
			CASE  39 iDead_drop_area[0] = 9         BREAK
			CASE  40 iDead_drop_area[0] = 0         BREAK
			CASE  41 iDead_drop_area[0] = 4         BREAK
			CASE  42 iDead_drop_area[0] = 4         BREAK
			CASE  43 iDead_drop_area[0] = 5         BREAK
			CASE  44 iDead_drop_area[0] = 1         BREAK
			CASE  45 iDead_drop_area[0] = 9         BREAK
			CASE  46 iDead_drop_area[0] = 10        BREAK
			CASE  47 iDead_drop_area[0] = 1         BREAK
			CASE  48 iDead_drop_area[0] = 14        BREAK
			CASE  49 iDead_drop_area[0] = 10        BREAK
			CASE  50 iDead_drop_area[0] = 10        BREAK
			CASE  51 iDead_drop_area[0] = 3         BREAK
			CASE  52 iDead_drop_area[0] = 6         BREAK
			CASE  53 iDead_drop_area[0] = 0         BREAK
			CASE  54 iDead_drop_area[0] = 13        BREAK
			CASE  55 iDead_drop_area[0] = 0         BREAK
			CASE  56 iDead_drop_area[0] = 1         BREAK
			CASE  57 iDead_drop_area[0] = 10        BREAK
			CASE  58 iDead_drop_area[0] = 7         BREAK
			CASE  59 iDead_drop_area[0] = 2         BREAK
			CASE  60 iDead_drop_area[0] = 3         BREAK
			CASE  61 iDead_drop_area[0] = 4         BREAK
			CASE  62 iDead_drop_area[0] = 6         BREAK
			CASE  63 iDead_drop_area[0] = 2         BREAK
			CASE  64 iDead_drop_area[0] = 14        BREAK
			CASE  65 iDead_drop_area[0] = 9         BREAK
			CASE  66 iDead_drop_area[0] = 12        BREAK
			CASE  67 iDead_drop_area[0] = 3         BREAK
			CASE  68 iDead_drop_area[0] = 11        BREAK
			CASE  69 iDead_drop_area[0] = 4         BREAK
			CASE  70 iDead_drop_area[0] = 6         BREAK
			CASE  71 iDead_drop_area[0] = 9         BREAK
			CASE  72 iDead_drop_area[0] = 7         BREAK
			CASE  73 iDead_drop_area[0] = 11        BREAK
			CASE  74 iDead_drop_area[0] = 2         BREAK
			CASE  75 iDead_drop_area[0] = 3         BREAK
			CASE  76 iDead_drop_area[0] = 12        BREAK
			CASE  77 iDead_drop_area[0] = 8         BREAK
			CASE  78 iDead_drop_area[0] = 7         BREAK
			CASE  79 iDead_drop_area[0] = 8         BREAK
			CASE  80 iDead_drop_area[0] = 12        BREAK
			CASE  81 iDead_drop_area[0] = 14        BREAK
			CASE  82 iDead_drop_area[0] = 2         BREAK
			CASE  83 iDead_drop_area[0] = 3         BREAK
		ENDSWITCH	
		
		
	PRINTLN("GET_DAILY_COLLECTABLE_DEAD_DROP_IN_ORDER_ACCORDING_TO_DAY iDead_drop_area[0] =     ",iDead_drop_location[0] )
	PRINTLN("GET_DAILY_COLLECTABLE_DEAD_DROP_IN_ORDER_ACCORDING_TO_DAY iDead_drop_area[0] = ",iDead_drop_area[0])	
	SET_PACKED_STAT_BOOL(	PACKED_MP_BOOL_DAILYCOLLECT_DEAD_DROP_0,FALSE)
	bis_Dead_drop_collectables_on = FALSE
	
	collectables_missiondata_main.inumber_of_Dead_drop_collected = 0
	SET_PACKED_STAT_INT(PACKED_MP_INT_DAILYCOLLECT_DEAD_DROP_LOCATION_0,iDead_drop_location[0] )
	SET_PACKED_STAT_INT(PACKED_MP_INT_DAILYCOLLECT_DEAD_DROP_AREA_0,iDead_drop_area[0] )
					
	#IF IS_DEBUG_BUILD
		g_iactive_Dead_Drop_area[0] =  iDead_drop_area[0]
		g_iactive_Dead_Drop_location[0] =  iDead_drop_location[0]
		
		PRINTLN("GET_DAILY_COLLECTABLE_DEAD_DROP_IN_ORDER_ACCORDING_TO_DAY PACKED_MP_INT_DAILYCOLLECT_DEAD_DROP_AREA_0 = ",GET_PACKED_STAT_INT(PACKED_MP_INT_DAILYCOLLECT_DEAD_DROP_AREA_0))
		
		PRINTLN("GET_DAILY_COLLECTABLE_DEAD_DROP_IN_ORDER_ACCORDING_TO_DAY PACKED_MP_INT_DAILYCOLLECT_DEAD_DROP_AREA_0 = ",GET_PACKED_STAT_INT(PACKED_MP_INT_DAILYCOLLECT_DEAD_DROP_AREA_0))
	#ENDIF 
	
ENDPROC
#ENDIF 





PROC  GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY(BOOL bsetdailycheck = TRUE)
	INT	itreasure_location[2]
	INT	ihiddencache_location[10]
	
	#IF FEATURE_TUNER
		INT	ishipwrecked_location
	#ENDIF
	
	#IF FEATURE_FIXER
		INT	iburiedstash_location[2]
	#ENDIF
	
	INT iIndex = GET_OBJECTIVE_DAY() % NUM_JOB_OBJECTIVES_IN_POOL
	
	IF bsetdailycheck
		SET_LOGGED_IN_TODAY_DAILY_COLLECTABLES(TRUE)
		PRINTLN(" [KW Collectables]  GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY bsetdailycheck")
	ENDIF
	
	IF g_sMPTunables.idaily_collectables_override_index != -1
		iIndex = g_sMPTunables.idaily_collectables_override_index
		PRINTLN(" [KW Collectables]  GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY g_sMPTunables.idaily_collectables_override_index != -1 OVERRIDING DAY : = ", iIndex)

	ENDIF
	
	
	

	
	SWITCH iIndex

		CASE  0

			itreasure_location[0] 	   = 8 	// land 0 to 9
			itreasure_location[1] 	   = 16    // underwater between 10-19														   
			
			ihiddencache_location[0]   =  13 
			ihiddencache_location[1]   =  32  
			ihiddencache_location[2]   =  45 
			ihiddencache_location[3]   =  48 
			ihiddencache_location[4]   =  54 
			ihiddencache_location[5]   =  69 
			ihiddencache_location[6]   =  75 
			ihiddencache_location[7]   =  81 
			ihiddencache_location[8]   =  92 
			ihiddencache_location[9]   =  93 
			
			#IF FEATURE_TUNER	
				ishipwrecked_location 	 =  15
				#IF FEATURE_FIXER
					iburiedstash_location[0] =	19
					iburiedstash_location[1] =	12
				#ENDIF 
			#ENDIF 
			
		BREAK 
		
		
		CASE  1

			itreasure_location[0] 	   = 0  	// land 0 to 9
			itreasure_location[1] 	   = 10    // underwater between 10-19														   
			
			ihiddencache_location[0]   = 0 
			ihiddencache_location[1]   = 1 
			ihiddencache_location[2]   = 2 
			ihiddencache_location[3]   = 3 
			ihiddencache_location[4]   = 4 
			ihiddencache_location[5]   = 5 
			ihiddencache_location[6]   = 6 
			ihiddencache_location[7]   = 7 
			ihiddencache_location[8]   = 8 
			ihiddencache_location[9]   = 9 
			
			#IF FEATURE_TUNER		
				ishipwrecked_location 	 =   7
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 8				 
				iburiedstash_location[1] =	7       
				#ENDIF 
			#ENDIF 
		
		BREAK
		
		CASE  2
			itreasure_location[0] 	   = 1  	// land 0 to 9
			itreasure_location[1] 	   = 11    // underwater between 10-19													   
			
			ihiddencache_location[0]   = 10 
			ihiddencache_location[1]   = 11 
			ihiddencache_location[2]   = 12 
			ihiddencache_location[3]   = 13  
			ihiddencache_location[4]   = 14 
			ihiddencache_location[5]   = 15 
			ihiddencache_location[6]   = 16 
			ihiddencache_location[7]   = 17 
			ihiddencache_location[8]   = 18 
			ihiddencache_location[9]   = 19 
			#IF FEATURE_TUNER					 
				ishipwrecked_location 	 =   26
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 9               
				iburiedstash_location[1] =	14      
				#ENDIF 
			#ENDIF 
		BREAK	
		
		CASE  3
		
			itreasure_location[0] 	   = 2  	// land 0 to 9
			itreasure_location[1] 	   = 12    // underwater between 10-19													   
			ihiddencache_location[0]          = 20 
			ihiddencache_location[1]          = 21 
			ihiddencache_location[2]          = 22 
			ihiddencache_location[3]          = 23  
			ihiddencache_location[4]          = 24 
			ihiddencache_location[5]          = 25 
			ihiddencache_location[6]          = 26 
			ihiddencache_location[7]          = 27 
			ihiddencache_location[8]          = 28 
			ihiddencache_location[9]          = 29
			
			#IF FEATURE_TUNER					                             
				ishipwrecked_location 	 =   24		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	17              
				iburiedstash_location[1] =	18      
				#ENDIF 
			#ENDIF 
				
		BREAK
		
		CASE  4
			itreasure_location[0] 	   = 3  	// land 0 to 9
			itreasure_location[1] 	   = 13    // underwater between 10-19									   
			ihiddencache_location[0]          = 30 
			ihiddencache_location[1]          = 31 
			ihiddencache_location[2]          = 32 
			ihiddencache_location[3]          = 33  
			ihiddencache_location[4]          = 34 
			ihiddencache_location[5]          = 35 
			ihiddencache_location[6]          = 36 
			ihiddencache_location[7]          = 37 
			ihiddencache_location[8]          = 38 
			ihiddencache_location[9]          = 39 
				
						
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   25		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	3              
				iburiedstash_location[1] =	14 
				#ENDIF 
			#ENDIF 
			
		BREAK
		
		CASE  5
			itreasure_location[0] 	   = 4  	// land 0 to 9
			itreasure_location[1] 	   = 14    // underwater between 10-19													   
			ihiddencache_location[0]          = 40 
			ihiddencache_location[1]          = 41 
			ihiddencache_location[2]          = 42 
			ihiddencache_location[3]          = 43  
			ihiddencache_location[4]          = 44 
			ihiddencache_location[5]          = 45 
			ihiddencache_location[6]          = 46 
			ihiddencache_location[7]          = 47 
			ihiddencache_location[8]          = 48 
			ihiddencache_location[9]          = 49 
			
			#IF FEATURE_TUNER					                              
				ishipwrecked_location 	 =   28		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 1              
				iburiedstash_location[1] =	15    
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  6
			itreasure_location[0] 	   = 5  	// land 0 to 9
			itreasure_location[1] 	   = 15    // underwater between 10-19														   
			ihiddencache_location[0]          = 50 
			ihiddencache_location[1]          = 51 
			ihiddencache_location[2]          = 52 
			ihiddencache_location[3]          = 53  
			ihiddencache_location[4]          = 54 
			ihiddencache_location[5]          = 55 
			ihiddencache_location[6]          = 56 
			ihiddencache_location[7]          = 57 
			ihiddencache_location[8]          = 58 
			ihiddencache_location[9]          = 59 
		
			#IF FEATURE_TUNER					                              
				ishipwrecked_location 	 =   29		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	0              
				iburiedstash_location[1] =	13   
				#ENDIF 
			#ENDIF 			
				
		BREAK
		
		CASE  7
			itreasure_location[0] 	   = 6  	// land 0 to 9
			itreasure_location[1] 	   = 16    // underwater between 10-19														   
			ihiddencache_location[0]          = 60 
			ihiddencache_location[1]          = 61 
			ihiddencache_location[2]          = 62 
			ihiddencache_location[3]          = 63  
			ihiddencache_location[4]          = 64 
			ihiddencache_location[5]          = 65 
			ihiddencache_location[6]          = 66 
			ihiddencache_location[7]          = 67 
			ihiddencache_location[8]          = 68 
			ihiddencache_location[9]          = 69 
		
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  12	
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	6              
				iburiedstash_location[1] =	 2      
				#ENDIF 
			#ENDIF 			
			
		BREAK
		CASE  8
			itreasure_location[0] 	   =	7  	// land 0 to 9
			itreasure_location[1] 	   =	17    // underwater between 10-19														   
			ihiddencache_location[0]          =  70 
			ihiddencache_location[1]          =  71 
			ihiddencache_location[2]          =  72 
			ihiddencache_location[3]          =  73  
			ihiddencache_location[4]          =  74 
			ihiddencache_location[5]          =  75 
			ihiddencache_location[6]          =  76 
			ihiddencache_location[7]          =  77 
			ihiddencache_location[8]          =  78 
			ihiddencache_location[9]          =  79 
			
			#IF FEATURE_TUNER					 
				ishipwrecked_location 	 =  11	
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 5              
				iburiedstash_location[1] =	7    
				#ENDIF 
			#ENDIF 			
		BREAK
		CASE  9
			itreasure_location[0] 	   = 8  	// land 0 to 9
			itreasure_location[1] 	   = 18    // underwater between 10-19														   
			ihiddencache_location[0]          = 80 
			ihiddencache_location[1]          = 81 
			ihiddencache_location[2]          = 82 
			ihiddencache_location[3]          = 83  
			ihiddencache_location[4]          = 84 
			ihiddencache_location[5]          = 85 
			ihiddencache_location[6]          = 86 
			ihiddencache_location[7]          = 87 
			ihiddencache_location[8]          = 88 
			ihiddencache_location[9]          = 89 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  13			
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	16              
				iburiedstash_location[1] =	12         
				#ENDIF 
			#ENDIF 
		BREAK	
			
		CASE  10
			itreasure_location[0] 	   = 9  	// land 0 to 9
			itreasure_location[1] 	   = 19    // underwater between 10-19								   
			ihiddencache_location[0]          = 90 
			ihiddencache_location[1]          = 91 
			ihiddencache_location[2]          = 92 
			ihiddencache_location[3]          = 93  
			ihiddencache_location[4]          = 94 
			ihiddencache_location[5]          = 95 
			ihiddencache_location[6]          = 96 
			ihiddencache_location[7]          = 97 
			ihiddencache_location[8]          = 98 
			ihiddencache_location[9]          = 99 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  4		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	11              
				iburiedstash_location[1] =	 0   
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  11
			itreasure_location[0] 	   = 4  	// land 0 to 9
			itreasure_location[1] 	   = 12    // underwater between 10-19														   
			ihiddencache_location[0]          = 10 
			ihiddencache_location[1]          = 16 
			ihiddencache_location[2]          = 35 
			ihiddencache_location[3]          = 47  
			ihiddencache_location[4]          = 49 
			ihiddencache_location[5]          = 67 
			ihiddencache_location[6]          = 71 
			ihiddencache_location[7]          = 80 
			ihiddencache_location[8]          = 81 
			ihiddencache_location[9]          = 88 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   13        
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 3              
				iburiedstash_location[1] =	10   
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  12
			itreasure_location[0] 	   = 3  	// land 0 to 9
			itreasure_location[1] 	   = 18    // underwater between 10-19														   
			ihiddencache_location[0]          = 11 
			ihiddencache_location[1]          = 22 
			ihiddencache_location[2]          = 33 
			ihiddencache_location[3]          = 44  
			ihiddencache_location[4]          = 55 
			ihiddencache_location[5]          = 66 
			ihiddencache_location[6]          = 77 
			ihiddencache_location[7]          = 88 
			ihiddencache_location[8]          = 91 
			ihiddencache_location[9]          = 92 
			
			#IF FEATURE_TUNER					                              
				ishipwrecked_location 	 =   20		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 6              
				iburiedstash_location[1] =	9   
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  13
			itreasure_location[0] 	   = 8  	// land 0 to 9
			itreasure_location[1] 	   = 10    // underwater between 10-19														   
			ihiddencache_location[0]          = 2 
			ihiddencache_location[1]          = 17 
			ihiddencache_location[2]          = 24 
			ihiddencache_location[3]          = 34  
			ihiddencache_location[4]          = 45 
			ihiddencache_location[5]          = 51 
			ihiddencache_location[6]          = 63 
			ihiddencache_location[7]          = 71 
			ihiddencache_location[8]          = 83 
			ihiddencache_location[9]          = 97 
			
			#IF FEATURE_TUNER					                               
				ishipwrecked_location 	 =   22	
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 4              
				iburiedstash_location[1] =	17       
				#ENDIF 
			#ENDIF 
		BREAK	
		
		CASE  14
			itreasure_location[0] 	   		  = 2  	// land 0 to 9
			itreasure_location[1] 	   		  = 17    // underwater between 10-19														   
			ihiddencache_location[0]          = 4 
			ihiddencache_location[1]          = 8 
			ihiddencache_location[2]          = 10 
			ihiddencache_location[3]          = 27  
			ihiddencache_location[4]          = 30 
			ihiddencache_location[5]          = 45 
			ihiddencache_location[6]          = 48 
			ihiddencache_location[7]          = 64 
			ihiddencache_location[8]          = 80 
			ihiddencache_location[9]          = 88

			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  10		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 7              
				iburiedstash_location[1] =	10 
				#ENDIF 
			#ENDIF 			
		BREAK
		
		CASE  15
			itreasure_location[0] 	   		  =1	 
			itreasure_location[1] 	   		  =15   
			
			ihiddencache_location[0]          =5   
			ihiddencache_location[1]          =11  
			ihiddencache_location[2]          =16  
			ihiddencache_location[3]          =19  
			ihiddencache_location[4]          =26  
			ihiddencache_location[5]          =66  
			ihiddencache_location[6]          =70  
			ihiddencache_location[7]          =80  
			ihiddencache_location[8]          =89  
			ihiddencache_location[9]          =99  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  28   
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	19              
				iburiedstash_location[1] =	2          
				#ENDIF 
			#ENDIF
		BREAK
		
		CASE  16
			itreasure_location[0] 	   		  =7   
			itreasure_location[1] 	   		  =14  
			
			ihiddencache_location[0]          =7	  
			ihiddencache_location[1]          =8     
			ihiddencache_location[2]          =25    
			ihiddencache_location[3]          =28    
			ihiddencache_location[4]          =29    
			ihiddencache_location[5]          =44    
			ihiddencache_location[6]          =62    
			ihiddencache_location[7]          =86    
			ihiddencache_location[8]          =95    
			ihiddencache_location[9]          =96    
			
			#IF FEATURE_TUNER					  
				ishipwrecked_location 	 =  11		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 3              
				iburiedstash_location[1] =	12     
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  17
			itreasure_location[0] 	   		  =2	 
			itreasure_location[1] 	   		  =13   
			
			ihiddencache_location[0]          =12	 
			ihiddencache_location[1]          =16   
			ihiddencache_location[2]          =24   
			ihiddencache_location[3]          =27   
			ihiddencache_location[4]          =29   
			ihiddencache_location[5]          =62   
			ihiddencache_location[6]          =75   
			ihiddencache_location[7]          =81   
			ihiddencache_location[8]          =93   
			ihiddencache_location[9]          =97   
			
			#IF FEATURE_TUNER					        
				ishipwrecked_location 	 =  10	
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 9              
				iburiedstash_location[1] =	 7            
				#ENDIF 
			#ENDIF 
		BREAK	
		
		CASE  18
			itreasure_location[0] 	   		  =6
			itreasure_location[1] 	   		  =15
			ihiddencache_location[0]          =20	  
			ihiddencache_location[1]          =26    
			ihiddencache_location[2]          =39    
			ihiddencache_location[3]          =52    
			ihiddencache_location[4]          =68    
			ihiddencache_location[5]          =77    
			ihiddencache_location[6]          =80    
			ihiddencache_location[7]          =81    
			ihiddencache_location[8]          =88    
			ihiddencache_location[9]          =98    
			
					
			#IF FEATURE_TUNER					            
				ishipwrecked_location 	 =   6			
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	11              
				iburiedstash_location[1] =	3      
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  19
			itreasure_location[0] 	   		  =7
		    itreasure_location[1] 	   		  =17
			ihiddencache_location[0]          =19  
			ihiddencache_location[1]          =26  
			ihiddencache_location[2]          =38  
			ihiddencache_location[3]          =56  
			ihiddencache_location[4]          =58  
			ihiddencache_location[5]          =63  
			ihiddencache_location[6]          =69  
			ihiddencache_location[7]          =71  
			ihiddencache_location[8]          =82  
			ihiddencache_location[9]          =99  
			
			#IF FEATURE_TUNER					                          
				ishipwrecked_location 	 =  16		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 0              
				iburiedstash_location[1] =	18     
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  20	
			itreasure_location[0] 	   		  =0
		    itreasure_location[1] 	   		  =19
			ihiddencache_location[0]          =15  
			ihiddencache_location[1]          =24  
			ihiddencache_location[2]          =25  
			ihiddencache_location[3]          =43  
			ihiddencache_location[4]          =46  
			ihiddencache_location[5]          =58  
			ihiddencache_location[6]          =67  
			ihiddencache_location[7]          =73  
			ihiddencache_location[8]          =82  
			ihiddencache_location[9]          =95  
			
					
			#IF FEATURE_TUNER					                        
				ishipwrecked_location 	 =   23		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	17              
				iburiedstash_location[1] =	15      
				#ENDIF 
			#ENDIF 
		BREAK

		CASE  21
			itreasure_location[0] 	   		  =1
		    itreasure_location[1] 	   		  =13
			ihiddencache_location[0]          =25  
			ihiddencache_location[1]          =27  
			ihiddencache_location[2]          =38  
			ihiddencache_location[3]          =45  
			ihiddencache_location[4]          =50  
			ihiddencache_location[5]          =63  
			ihiddencache_location[6]          =69  
			ihiddencache_location[7]          =71  
			ihiddencache_location[8]          =75  
			ihiddencache_location[9]          =77  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   8
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	3
				iburiedstash_location[1] =	 7
				#ENDIF 
			#ENDIF 
		

		BREAK
		CASE  22
			itreasure_location[0] 	   		  =6
		    itreasure_location[1] 	   		  =14
			ihiddencache_location[0]          =14  
			ihiddencache_location[1]          =35  
			ihiddencache_location[2]          =41  
			ihiddencache_location[3]          =43  
			ihiddencache_location[4]          =47  
			ihiddencache_location[5]          =51  
			ihiddencache_location[6]          =54  
			ihiddencache_location[7]          =90  
			ihiddencache_location[8]          =93  
			ihiddencache_location[9]          =99  
			
			#IF FEATURE_TUNER					       
				ishipwrecked_location 	 =   3	
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 4              
				iburiedstash_location[1] =	14    
				#ENDIF 
			#ENDIF
		BREAK
		
		CASE  23
			itreasure_location[0] 	   		  =7
		    itreasure_location[1] 	   		  =11
			ihiddencache_location[0]          =31
			ihiddencache_location[1]          =44
			ihiddencache_location[2]          =47
			ihiddencache_location[3]          =49
			ihiddencache_location[4]          =54
			ihiddencache_location[5]          =61
			ihiddencache_location[6]          =64
			ihiddencache_location[7]          =69
			ihiddencache_location[8]          =84
			ihiddencache_location[9]          =97
			
					
			#IF FEATURE_TUNER					  
				ishipwrecked_location 	 =   11  
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	12              
				iburiedstash_location[1] =	17
				#ENDIF 
			#ENDIF 
		BREAK

		CASE  24
			itreasure_location[0] 	   		  =8
		    itreasure_location[1] 	   		  =16
			ihiddencache_location[0]          =1  
			ihiddencache_location[1]          =15 
			ihiddencache_location[2]          =21 
			ihiddencache_location[3]          =24 
			ihiddencache_location[4]          =31 
			ihiddencache_location[5]          =41 
			ihiddencache_location[6]          =49 
			ihiddencache_location[7]          =74 
			ihiddencache_location[8]          =78 
			ihiddencache_location[9]          =99 
			
			#IF FEATURE_TUNER					          
				ishipwrecked_location 	 =  13	
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	7              
				iburiedstash_location[1] =	5     
				#ENDIF 
			#ENDIF 
		BREAK
		
		
		CASE  25
			itreasure_location[0] 	   		  =6
		    itreasure_location[1] 	   		  =14
			ihiddencache_location[0]          =19  
			ihiddencache_location[1]          =30  
			ihiddencache_location[2]          =34  
			ihiddencache_location[3]          =41  
			ihiddencache_location[4]          =42  
			ihiddencache_location[5]          =57  
			ihiddencache_location[6]          =74  
			ihiddencache_location[7]          =77  
			ihiddencache_location[8]          =81  
			ihiddencache_location[9]          =91  
			
			#IF FEATURE_TUNER					            
				ishipwrecked_location 	 =  14		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	14              
				iburiedstash_location[1] =	18    
				#ENDIF 
			#ENDIF 
		BREAK


		CASE  26
			itreasure_location[0] 	   		  =3
		    itreasure_location[1] 	   		  =19
			ihiddencache_location[0]          =0   
			ihiddencache_location[1]          =5   
			ihiddencache_location[2]          =37  
			ihiddencache_location[3]          =38  
			ihiddencache_location[4]          =60  
			ihiddencache_location[5]          =62  
			ihiddencache_location[6]          =66  
			ihiddencache_location[7]          =81  
			ihiddencache_location[8]          =83  
			ihiddencache_location[9]          =85  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  12	
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	15              
				iburiedstash_location[1] =	 8     
				#ENDIF 
			#ENDIF 
		BREAK


		CASE  27
			itreasure_location[0] 	   		  =5
		    itreasure_location[1] 	   		  =15
			ihiddencache_location[0]          =11  
			ihiddencache_location[1]          =19  
			ihiddencache_location[2]          =22  
			ihiddencache_location[3]          =30  
			ihiddencache_location[4]          =53  
			ihiddencache_location[5]          =60  
			ihiddencache_location[6]          =70  
			ihiddencache_location[7]          =74  
			ihiddencache_location[8]          =89  
			ihiddencache_location[9]          =94  
			
			#IF FEATURE_TUNER					 
				ishipwrecked_location 	 =   4 
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 2              
				iburiedstash_location[1] =	10      
				#ENDIF 
			#ENDIF 
		
		BREAK


		CASE  28
			itreasure_location[0] 	   		  =6
			itreasure_location[1] 	   		  =12
			ihiddencache_location[0]          =13  
			ihiddencache_location[1]          =17  
			ihiddencache_location[2]          =21  
			ihiddencache_location[3]          =26  
			ihiddencache_location[4]          =28  
			ihiddencache_location[5]          =30  
			ihiddencache_location[6]          =53  
			ihiddencache_location[7]          =61  
			ihiddencache_location[8]          =66  
			ihiddencache_location[9]          =69  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   27
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	13              
				iburiedstash_location[1] =	16
				#ENDIF 
			#ENDIF 
		BREAK

		CASE  29
			itreasure_location[0] 	   		  =1
		    itreasure_location[1] 	   		  =19
			ihiddencache_location[0]          =3   
			ihiddencache_location[1]          =8   
			ihiddencache_location[2]          =12  
			ihiddencache_location[3]          =39  
			ihiddencache_location[4]          =45  
			ihiddencache_location[5]          =61  
			ihiddencache_location[6]          =65  
			ihiddencache_location[7]          =77  
			ihiddencache_location[8]          =80  
			ihiddencache_location[9]          =85

			#IF FEATURE_TUNER					                                   
				ishipwrecked_location 	 =   2		
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 1              
				iburiedstash_location[1] =	14
				#ENDIF 
			#ENDIF 			
		BREAK
		
		
		CASE  30
			itreasure_location[0] 	   		  =2
		    itreasure_location[1] 	   		  =10
			ihiddencache_location[0]          =0   
			ihiddencache_location[1]          =2   
			ihiddencache_location[2]          =38  
			ihiddencache_location[3]          =39  
			ihiddencache_location[4]          =45  
			ihiddencache_location[5]          =46  
			ihiddencache_location[6]          =47  
			ihiddencache_location[7]          =54  
			ihiddencache_location[8]          =67  
			ihiddencache_location[9]          =81  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   9
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 6
				iburiedstash_location[1] =	 5
				#ENDIF 
			#ENDIF 
		
		BREAK

		
		CASE  31
			itreasure_location[0] 	   		  =9
		    itreasure_location[1] 	   		  =14
			ihiddencache_location[0]          =10  
			ihiddencache_location[1]          =18  
			ihiddencache_location[2]          =43  
			ihiddencache_location[3]          =45  
			ihiddencache_location[4]          =56  
			ihiddencache_location[5]          =60  
			ihiddencache_location[6]          =67  
			ihiddencache_location[7]          =74  
			ihiddencache_location[8]          =77  
			ihiddencache_location[9]          =88  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   8
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 19
				iburiedstash_location[1] =	0
				#ENDIF 
			#ENDIF 

		BREAK
		CASE  32
			itreasure_location[0] 	   		  =7
		    itreasure_location[1] 	   		  =18
			ihiddencache_location[0]          =25  
			ihiddencache_location[1]          =27  
			ihiddencache_location[2]          =30  
			ihiddencache_location[3]          =48  
			ihiddencache_location[4]          =58  
			ihiddencache_location[5]          =59  
			ihiddencache_location[6]          =61  
			ihiddencache_location[7]          =62  
			ihiddencache_location[8]          =85  
			ihiddencache_location[9]          =91  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  15
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 3
				iburiedstash_location[1] =	2
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  33
			itreasure_location[0] 	   		  =4
		    itreasure_location[1] 	   		  =14
			ihiddencache_location[0]          =23  
			ihiddencache_location[1]          =28  
			ihiddencache_location[2]          =33  
			ihiddencache_location[3]          =50  
			ihiddencache_location[4]          =56  
			ihiddencache_location[5]          =63  
			ihiddencache_location[6]          =71  
			ihiddencache_location[7]          =77  
			ihiddencache_location[8]          =80  
			ihiddencache_location[9]          =95  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  13
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 9
				iburiedstash_location[1] =	 7
				#ENDIF 
			#ENDIF 
		
		BREAK
		CASE  34
			itreasure_location[0] 	   		  =3
		    itreasure_location[1] 	   		  =10
			ihiddencache_location[0]          =2  
			ihiddencache_location[1]          =17 
			ihiddencache_location[2]          =20 
			ihiddencache_location[3]          =24 
			ihiddencache_location[4]          =51 
			ihiddencache_location[5]          =60 
			ihiddencache_location[6]          =81 
			ihiddencache_location[7]          =84 
			ihiddencache_location[8]          =85 
			ihiddencache_location[9]          =94
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   5
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	11
				iburiedstash_location[1] =	3
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  35
			itreasure_location[0] 	   		  =4
		    itreasure_location[1] 	   		  =14
			ihiddencache_location[0]          =9  
			ihiddencache_location[1]          =20 
			ihiddencache_location[2]          =36 
			ihiddencache_location[3]          =51 
			ihiddencache_location[4]          =59 
			ihiddencache_location[5]          =66 
			ihiddencache_location[6]          =70 
			ihiddencache_location[7]          =76 
			ihiddencache_location[8]          =91 
			ihiddencache_location[9]          =96 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  16
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 0
				iburiedstash_location[1] =	18
				#ENDIF 
			#ENDIF 
		
		BREAK
		CASE  36
			itreasure_location[0] 	   		  =5
		    itreasure_location[1] 	   		  =15
			ihiddencache_location[0]          =25  
			ihiddencache_location[1]          =27  
			ihiddencache_location[2]          =30  
			ihiddencache_location[3]          =31  
			ihiddencache_location[4]          =39  
			ihiddencache_location[5]          =47  
			ihiddencache_location[6]          =56  
			ihiddencache_location[7]          =57  
			ihiddencache_location[8]          =59  
			ihiddencache_location[9]          =94  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   1
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	8
				iburiedstash_location[1] =	15
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  37
			itreasure_location[0] 	   		  =3
		    itreasure_location[1] 	   		  =17
			ihiddencache_location[0]          =9  
			ihiddencache_location[1]          =19 
			ihiddencache_location[2]          =31 
			ihiddencache_location[3]          =50 
			ihiddencache_location[4]          =59 
			ihiddencache_location[5]          =60 
			ihiddencache_location[6]          =75 
			ihiddencache_location[7]          =84 
			ihiddencache_location[8]          =89 
			ihiddencache_location[9]          =99 
			
					
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   8
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 4
				iburiedstash_location[1] =	2
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  38
			itreasure_location[0] 	   		  =4
		    itreasure_location[1] 	   		  =15
			ihiddencache_location[0]          =3  
			ihiddencache_location[1]          =4  
			ihiddencache_location[2]          =12 
			ihiddencache_location[3]          =62 
			ihiddencache_location[4]          =63 
			ihiddencache_location[5]          =71 
			ihiddencache_location[6]          =81 
			ihiddencache_location[7]          =84 
			ihiddencache_location[8]          =91 
			ihiddencache_location[9]          =92 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   23
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	12
				iburiedstash_location[1] =	5
				#ENDIF 
			#ENDIF 
		
		BREAK
		CASE  39
			itreasure_location[0] 	   		  =7
		    itreasure_location[1] 	   		  =13
			ihiddencache_location[0]          =0  
			ihiddencache_location[1]          =4  
			ihiddencache_location[2]          =6  
			ihiddencache_location[3]          =9  
			ihiddencache_location[4]          =18 
			ihiddencache_location[5]          =25 
			ihiddencache_location[6]          =47 
			ihiddencache_location[7]          =62 
			ihiddencache_location[8]          =84 
			ihiddencache_location[9]          =96 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   6
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	1
				iburiedstash_location[1] =	17
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  40
			itreasure_location[0] 	   		  =0
		    itreasure_location[1] 	   		  =18
			ihiddencache_location[0]          =2  
			ihiddencache_location[1]          =5  
			ihiddencache_location[2]          =8 
			ihiddencache_location[3]          =17  
			ihiddencache_location[4]          =28 
			ihiddencache_location[5]          =32 
			ihiddencache_location[6]          =37 
			ihiddencache_location[7]          =63 
			ihiddencache_location[8]          =81 
			ihiddencache_location[9]          =98 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   2
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	14
				iburiedstash_location[1] =	18
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  41
			itreasure_location[0] 	   		  =5
		    itreasure_location[1] 	   		  =14
			ihiddencache_location[0]          =4  
			ihiddencache_location[1]          =19 
			ihiddencache_location[2]          =26 
			ihiddencache_location[3]          =34 
			ihiddencache_location[4]          =43 
			ihiddencache_location[5]          =53 
			ihiddencache_location[6]          =63 
			ihiddencache_location[7]          =82 
			ihiddencache_location[8]          =88 
			ihiddencache_location[9]          =97

			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  15
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	15
				iburiedstash_location[1] =	 8
				#ENDIF 
			#ENDIF 			
		BREAK
		CASE  42
			itreasure_location[0] 	   		  =6
		    itreasure_location[1] 	   		  =12
			ihiddencache_location[0]          =1  
			ihiddencache_location[1]          =6  
			ihiddencache_location[2]          =8  
			ihiddencache_location[3]          =20 
			ihiddencache_location[4]          =22 
			ihiddencache_location[5]          =32 
			ihiddencache_location[6]          =63 
			ihiddencache_location[7]          =65 
			ihiddencache_location[8]          =70 
			ihiddencache_location[9]          =85 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   24
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 2
				iburiedstash_location[1] =	10
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  43
			itreasure_location[0] 	   		  =8
		    itreasure_location[1] 	   		  =16
			ihiddencache_location[0]          =10  
			ihiddencache_location[1]          =13  
			ihiddencache_location[2]          =40  
			ihiddencache_location[3]          =44  
			ihiddencache_location[4]          =52  
			ihiddencache_location[5]          =62  
			ihiddencache_location[6]          =77  
			ihiddencache_location[7]          =78  
			ihiddencache_location[8]          =83  
			ihiddencache_location[9]          =86  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   7
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	13
				iburiedstash_location[1] =	16
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  44
			itreasure_location[0] 	   		  =2
		    itreasure_location[1] 	   		  =19
			ihiddencache_location[0]          =4  
			ihiddencache_location[1]          =6  
			ihiddencache_location[2]          =9  
			ihiddencache_location[3]          =28 
			ihiddencache_location[4]          =29 
			ihiddencache_location[5]          =33 
			ihiddencache_location[6]          =53 
			ihiddencache_location[7]          =55 
			ihiddencache_location[8]          =56 
			ihiddencache_location[9]          =61 
			
					
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  12
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 1
				iburiedstash_location[1] =	16
				#ENDIF 
			#ENDIF 
		
		BREAK
		
		CASE  45
			itreasure_location[0] 	   		  =0
		    itreasure_location[1] 	   		  =11
			ihiddencache_location[0]          =4  
			ihiddencache_location[1]          =19 
			ihiddencache_location[2]          =20 
			ihiddencache_location[3]          =27 
			ihiddencache_location[4]          =50 
			ihiddencache_location[5]          =87 
			ihiddencache_location[6]          =89 
			ihiddencache_location[7]          =90 
			ihiddencache_location[8]          =94 
			ihiddencache_location[9]          =98 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  14
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 6
				iburiedstash_location[1] =	 5
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  46
			itreasure_location[0] 	   		  =6
		    itreasure_location[1] 	   		  =12
			ihiddencache_location[0]          =6  
			ihiddencache_location[1]          =14 
			ihiddencache_location[2]          =46 
			ihiddencache_location[3]          =50 
			ihiddencache_location[4]          =55 
			ihiddencache_location[5]          =61 
			ihiddencache_location[6]          =70 
			ihiddencache_location[7]          =80 
			ihiddencache_location[8]          =83 
			ihiddencache_location[9]          =96 
			
					
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  11
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 19
				iburiedstash_location[1] =	0
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  47
			itreasure_location[0] 	   		  =0
		    itreasure_location[1] 	   		  =13
			ihiddencache_location[0]          =0  
			ihiddencache_location[1]          =5  
			ihiddencache_location[2]          =6  
			ihiddencache_location[3]          =33 
			ihiddencache_location[4]          =41 
			ihiddencache_location[5]          =61 
			ihiddencache_location[6]          =71 
			ihiddencache_location[7]          =75 
			ihiddencache_location[8]          =86 
			ihiddencache_location[9]          =94 
			
					
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   0
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 3
				iburiedstash_location[1] =	2
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  48
			itreasure_location[0] 	   		  =3
		    itreasure_location[1] 	   		  =17
			ihiddencache_location[0]          =6  
			ihiddencache_location[1]          =7  
			ihiddencache_location[2]          =11 
			ihiddencache_location[3]          =28 
			ihiddencache_location[4]          =31 
			ihiddencache_location[5]          =32 
			ihiddencache_location[6]          =39 
			ihiddencache_location[7]          =42 
			ihiddencache_location[8]          =64 
			ihiddencache_location[9]          =65 
			
					
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  27
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 9
				iburiedstash_location[1] =	 7
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  49
			itreasure_location[0] 	   		  =5
		    itreasure_location[1] 	   		  =10
			ihiddencache_location[0]          =3  
			ihiddencache_location[1]          =6  
			ihiddencache_location[2]          =8  
			ihiddencache_location[3]          =22 
			ihiddencache_location[4]          =33 
			ihiddencache_location[5]          =42 
			ihiddencache_location[6]          =50 
			ihiddencache_location[7]          =57 
			ihiddencache_location[8]          =77 
			ihiddencache_location[9]          =98 
			
					
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   9
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	11
				iburiedstash_location[1] =	13
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  50
			itreasure_location[0] 	   		  =2
		    itreasure_location[1] 	   		  =11
			ihiddencache_location[0]          =13  
			ihiddencache_location[1]          =16  
			ihiddencache_location[2]          =18  
			ihiddencache_location[3]          =32 
			ihiddencache_location[4]          =38 
			ihiddencache_location[5]          =43 
			ihiddencache_location[6]          =51 
			ihiddencache_location[7]          =67 
			ihiddencache_location[8]          =97 
			ihiddencache_location[9]          =99 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   4
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 0
				iburiedstash_location[1] =	18
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  51
			itreasure_location[0] 	   		  =4
		    itreasure_location[1] 	   		  =12
			ihiddencache_location[0]          =2  
			ihiddencache_location[1]          =13 
			ihiddencache_location[2]          =19 
			ihiddencache_location[3]          =23 
			ihiddencache_location[4]          =52 
			ihiddencache_location[5]          =63 
			ihiddencache_location[6]          =64 
			ihiddencache_location[7]          =72 
			ihiddencache_location[8]          =90 
			ihiddencache_location[9]          =92 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   0
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	11
				iburiedstash_location[1] =	15
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  52
			itreasure_location[0] 	   		  =1
		    itreasure_location[1] 	   		  =13
			ihiddencache_location[0]          =9  
			ihiddencache_location[1]          =14 
			ihiddencache_location[2]          =16 
			ihiddencache_location[3]          =35 
			ihiddencache_location[4]          =55 
			ihiddencache_location[5]          =56 
			ihiddencache_location[6]          =76 
			ihiddencache_location[7]          =79 
			ihiddencache_location[8]          =87 
			ihiddencache_location[9]          =89 
			
					
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  10
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 4
				iburiedstash_location[1] =	7
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  53
			itreasure_location[0] 	   		  =5
		    itreasure_location[1] 	   		  =17
			ihiddencache_location[0]          =4	 
			ihiddencache_location[1]          =9    
			ihiddencache_location[2]          =11   
			ihiddencache_location[3]          =15   
			ihiddencache_location[4]          =21   
			ihiddencache_location[5]          =26   
			ihiddencache_location[6]          =39   
			ihiddencache_location[7]          =44   
			ihiddencache_location[8]          =65   
			ihiddencache_location[9]          =85   
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   28
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	12
				iburiedstash_location[1] =	6
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  54
			itreasure_location[0] 	   		  =7
		    itreasure_location[1] 	   		  =18
			ihiddencache_location[0]          =15  
			ihiddencache_location[1]          =40  
			ihiddencache_location[2]          =49  
			ihiddencache_location[3]          =53  
			ihiddencache_location[4]          =67  
			ihiddencache_location[5]          =70  
			ihiddencache_location[6]          =76  
			ihiddencache_location[7]          =78  
			ihiddencache_location[8]          =87  
			ihiddencache_location[9]          =90  
			
					
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   23
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	8
				iburiedstash_location[1] =	17
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  55
			itreasure_location[0] 	   		  =9
		    itreasure_location[1] 	   		  =13
			ihiddencache_location[0]          =1	 
			ihiddencache_location[1]          =12   
			ihiddencache_location[2]          =21   
			ihiddencache_location[3]          =29   
			ihiddencache_location[4]          =35   
			ihiddencache_location[5]          =39   
			ihiddencache_location[6]          =62   
			ihiddencache_location[7]          =71   
			ihiddencache_location[8]          =72   
			ihiddencache_location[9]          =84  

			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  15
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	14
				iburiedstash_location[1] =	9
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  56
			itreasure_location[0] 	   		  =8
		    itreasure_location[1] 	   		  =17
			ihiddencache_location[0]          =15	 
			ihiddencache_location[1]          =58   
			ihiddencache_location[2]          =59   
			ihiddencache_location[3]          =61   
			ihiddencache_location[4]          =71   
			ihiddencache_location[5]          =79   
			ihiddencache_location[6]          =81   
			ihiddencache_location[7]          =96   
			ihiddencache_location[8]          =98   
			ihiddencache_location[9]          =99   
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   5
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	4
				iburiedstash_location[1] =	 8
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  57
			itreasure_location[0] 	   		  =3
		    itreasure_location[1] 	   		  =12
			ihiddencache_location[0]          =6	 
			ihiddencache_location[1]          =11   
			ihiddencache_location[2]          =18   
			ihiddencache_location[3]          =48   
			ihiddencache_location[4]          =51   
			ihiddencache_location[5]          =56   
			ihiddencache_location[6]          =67   
			ihiddencache_location[7]          =73   
			ihiddencache_location[8]          =92   
			ihiddencache_location[9]          =96  

		
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   7
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 2
				iburiedstash_location[1] =	10
				#ENDIF 
			#ENDIF 
					
		BREAK
		
		CASE  58
			itreasure_location[0] 	   		  =7
		    itreasure_location[1] 	   		  =11
			ihiddencache_location[0]          =18	 
			ihiddencache_location[1]          =21   
			ihiddencache_location[2]          =24   
			ihiddencache_location[3]          =36   
			ihiddencache_location[4]          =38   
			ihiddencache_location[5]          =48   
			ihiddencache_location[6]          =58   
			ihiddencache_location[7]          =62   
			ihiddencache_location[8]          =84   
			ihiddencache_location[9]          =91   
			
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   6
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	13
				iburiedstash_location[1] =	16
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  59
			itreasure_location[0] 	   		  =8
		    itreasure_location[1] 	   		  =19
			ihiddencache_location[0]          =2	 
			ihiddencache_location[1]          =5    
			ihiddencache_location[2]          =12   
			ihiddencache_location[3]          =32   
			ihiddencache_location[4]          =59   
			ihiddencache_location[5]          =64   
			ihiddencache_location[6]          =65   
			ihiddencache_location[7]          =67   
			ihiddencache_location[8]          =75   
			ihiddencache_location[9]          =85 

		
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   2
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 1
				iburiedstash_location[1] =	4
				#ENDIF 
			#ENDIF 			
		BREAK
		
				
		CASE  60
			itreasure_location[0] 	   		  =2
		    itreasure_location[1] 	   		  =18
			ihiddencache_location[0]          =13  
			ihiddencache_location[1]          =16  
			ihiddencache_location[2]          =18  
			ihiddencache_location[3]          =33 
			ihiddencache_location[4]          =38 
			ihiddencache_location[5]          =43 
			ihiddencache_location[6]          =55 
			ihiddencache_location[7]          =61 
			ihiddencache_location[8]          =97 
			ihiddencache_location[9]          =94 
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  13
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 6
				iburiedstash_location[1] =	 5
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  61
			itreasure_location[0] 	   		  =0
		    itreasure_location[1] 	   		  =10
			ihiddencache_location[0]          =15	 
			ihiddencache_location[1]          =19   
			ihiddencache_location[2]          =39   
			ihiddencache_location[3]          =41   
			ihiddencache_location[4]          =49   
			ihiddencache_location[5]          =64   
			ihiddencache_location[6]          =72   
			ihiddencache_location[7]          =76   
			ihiddencache_location[8]          =94   
			ihiddencache_location[9]          =96   
					
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  16
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	  3
				iburiedstash_location[1] =	 0
				#ENDIF 
			#ENDIF 
			
		BREAK
		CASE  62
			itreasure_location[0] 	   		  =3
		    itreasure_location[1] 	   		  =17
			ihiddencache_location[0]          =15	 
			ihiddencache_location[1]          =17   
			ihiddencache_location[2]          =32   
			ihiddencache_location[3]          =33   
			ihiddencache_location[4]          =35   
			ihiddencache_location[5]          =37   
			ihiddencache_location[6]          =74   
			ihiddencache_location[7]          =75   
			ihiddencache_location[8]          =85   
			ihiddencache_location[9]          =90

			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  22
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	19
				iburiedstash_location[1] =	10
				#ENDIF 
			#ENDIF 
					
		BREAK
		CASE  63
			itreasure_location[0] 	   		  =8
		    itreasure_location[1] 	   		  =13
			ihiddencache_location[0]          =13	 
			ihiddencache_location[1]          =36   
			ihiddencache_location[2]          =40   
			ihiddencache_location[3]          =64   
			ihiddencache_location[4]          =68   
			ihiddencache_location[5]          =72   
			ihiddencache_location[6]          =73   
			ihiddencache_location[7]          =84   
			ihiddencache_location[8]          =91   
			ihiddencache_location[9]          =94   
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   9
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 4
				iburiedstash_location[1] =	18
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  64
			itreasure_location[0] 	   		  =7
		    itreasure_location[1] 	   		  =14
			ihiddencache_location[0]          =13	 
			ihiddencache_location[1]          =36   
			ihiddencache_location[2]          =40   
			ihiddencache_location[3]          =64   
			ihiddencache_location[4]          =68   
			ihiddencache_location[5]          =72   
			ihiddencache_location[6]          =73   
			ihiddencache_location[7]          =84   
			ihiddencache_location[8]          =91   
			ihiddencache_location[9]          =94 

			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   21
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	11
				iburiedstash_location[1] =	 3
				#ENDIF 
			#ENDIF 			
		BREAK
		
		CASE  65
			itreasure_location[0] 	   		  =5
		    itreasure_location[1] 	   		  =10
			ihiddencache_location[0]          =7	 
			ihiddencache_location[1]          =26   
			ihiddencache_location[2]          =27   
			ihiddencache_location[3]          =35   
			ihiddencache_location[4]          =42   
			ihiddencache_location[5]          =50   
			ihiddencache_location[6]          =69   
			ihiddencache_location[7]          =73   
			ihiddencache_location[8]          =83   
			ihiddencache_location[9]          =97   
			
					
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  14
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	16
				iburiedstash_location[1] =	 7
				#ENDIF 
			#ENDIF 
			
		BREAK
		
		CASE  66
			itreasure_location[0] 	   		  =9
			itreasure_location[1] 	   		  =14
			ihiddencache_location[0]          =5	 
			ihiddencache_location[1]          =14   
			ihiddencache_location[2]          =22   
			ihiddencache_location[3]          =23   
			ihiddencache_location[4]          =24   
			ihiddencache_location[5]          =30   
			ihiddencache_location[6]          =36   
			ihiddencache_location[7]          =42   
			ihiddencache_location[8]          =51   
			ihiddencache_location[9]          =86   
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  11
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	5
				iburiedstash_location[1] =	1
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  67
			itreasure_location[0] 	   		  =1
		    itreasure_location[1] 	   		  =16
			ihiddencache_location[0]          =10	 
			ihiddencache_location[1]          =27   
			ihiddencache_location[2]          =47   
			ihiddencache_location[3]          =74   
			ihiddencache_location[4]          =83   
			ihiddencache_location[5]          =89   
			ihiddencache_location[6]          =92   
			ihiddencache_location[7]          =95   
			ihiddencache_location[8]          =96   
			ihiddencache_location[9]          =98   
					
			#IF FEATURE_TUNER				
				ishipwrecked_location 	 =  25
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	12
				iburiedstash_location[1] =	 8
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  68
			itreasure_location[0] 	   		  =6
		    itreasure_location[1] 	   		  =13
			ihiddencache_location[0]          =1	 
			ihiddencache_location[1]          =3    
			ihiddencache_location[2]          =7    
			ihiddencache_location[3]          =8    
			ihiddencache_location[4]          =38   
			ihiddencache_location[5]          =55   
			ihiddencache_location[6]          =60   
			ihiddencache_location[7]          =63   
			ihiddencache_location[8]          =69   
			ihiddencache_location[9]          =72
		
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   21
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	7
				iburiedstash_location[1] =	15
				#ENDIF 
			#ENDIF 
			
		BREAK
		CASE  69
			itreasure_location[0] 	   		  =3
		    itreasure_location[1] 	   		  =11
			ihiddencache_location[0]          =32	 
			ihiddencache_location[1]          =33   
			ihiddencache_location[2]          =35   
			ihiddencache_location[3]          =36   
			ihiddencache_location[4]          =41   
			ihiddencache_location[5]          =48   
			ihiddencache_location[6]          =52   
			ihiddencache_location[7]          =53   
			ihiddencache_location[8]          =54   
			ihiddencache_location[9]          =91

		
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   3
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	12
				iburiedstash_location[1] =	16
				#ENDIF 
			#ENDIF 			
		BREAK
		
		CASE  70
			itreasure_location[0] 	   		  =1
		    itreasure_location[1] 	   		  =10
			ihiddencache_location[0]          =8	 
			ihiddencache_location[1]          =14   
			ihiddencache_location[2]          =22   
			ihiddencache_location[3]          =28  
			ihiddencache_location[4]          =48   
			ihiddencache_location[5]          =57   
			ihiddencache_location[6]          =61   
			ihiddencache_location[7]          =88   
			ihiddencache_location[8]          =89  
			ihiddencache_location[9]          = 90 

		
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   2
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	19
				iburiedstash_location[1] =	17
				#ENDIF 
			#ENDIF 			
		BREAK
		CASE  71
			itreasure_location[0] 	   		  =6
		    itreasure_location[1] 	   		  =16
			ihiddencache_location[0]          =3	 
			ihiddencache_location[1]          =15   
			ihiddencache_location[2]          =39   
			ihiddencache_location[3]          =42   
			ihiddencache_location[4]          =45   
			ihiddencache_location[5]          =66   
			ihiddencache_location[6]          =67   
			ihiddencache_location[7]          =91   
			ihiddencache_location[8]          =93   
			ihiddencache_location[9]          =95   
			
					
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  13
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	14
				iburiedstash_location[1] =	 2
				#ENDIF 
			#ENDIF 
		
		BREAK
		CASE  72
			itreasure_location[0] 	   		  =7
		    itreasure_location[1] 	   		  =17
			ihiddencache_location[0]          =6	 
			ihiddencache_location[1]          =9    
			ihiddencache_location[2]          =20   
			ihiddencache_location[3]          =26   
			ihiddencache_location[4]          =40   
			ihiddencache_location[5]          =63   
			ihiddencache_location[6]          =69   
			ihiddencache_location[7]          =82   
			ihiddencache_location[8]          =93   
			ihiddencache_location[9]          =94  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  12
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	3
				iburiedstash_location[1] =	0
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  73
			itreasure_location[0] 	   		  =8
		    itreasure_location[1] 	   		  =18
			ihiddencache_location[0]          =17	 
			ihiddencache_location[1]          =21   
			ihiddencache_location[2]          =39   
			ihiddencache_location[3]          =48   
			ihiddencache_location[4]          =54   
			ihiddencache_location[5]          =65   
			ihiddencache_location[6]          =73   
			ihiddencache_location[7]          =82   
			ihiddencache_location[8]          =95   
			ihiddencache_location[9]          =97   
			
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  11
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	13
				iburiedstash_location[1] =	4
				#ENDIF 
			#ENDIF 
			
		BREAK
		CASE  74
			itreasure_location[0] 	   		  =3
		    itreasure_location[1] 	   		  =13
			ihiddencache_location[0]          =5	 
			ihiddencache_location[1]          =32   
			ihiddencache_location[2]          =55   
			ihiddencache_location[3]          =56   
			ihiddencache_location[4]          =60   
			ihiddencache_location[5]          =61   
			ihiddencache_location[6]          =76   
			ihiddencache_location[7]          =77   
			ihiddencache_location[8]          =81   
			ihiddencache_location[9]          =95  
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  15
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	12
				iburiedstash_location[1] =	 8
				#ENDIF 
			#ENDIF 
		BREAK
		CASE  75
			itreasure_location[0] 	   		  =0
		    itreasure_location[1] 	   		  =12
			ihiddencache_location[0]          =24	 
			ihiddencache_location[1]          =32   
			ihiddencache_location[2]          =46   
			ihiddencache_location[3]          =55   
			ihiddencache_location[4]          =65   
			ihiddencache_location[5]          =70   
			ihiddencache_location[6]          =75   
			ihiddencache_location[7]          =81   
			ihiddencache_location[8]          =89   
			ihiddencache_location[9]          =94 

			#IF FEATURE_TUNER				
				ishipwrecked_location 	 =   27
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 9
				iburiedstash_location[1] =	2
				#ENDIF 
			#ENDIF 			
		BREAK
		CASE  76
			itreasure_location[0] 	   		  =2
		    itreasure_location[1] 	   		  =14
			ihiddencache_location[0]          =13	 
			ihiddencache_location[1]          =14   
			ihiddencache_location[2]          =19   
			ihiddencache_location[3]          =25   
			ihiddencache_location[4]          =30   
			ihiddencache_location[5]          =36   
			ihiddencache_location[6]          =48   
			ihiddencache_location[7]          =60   
			ihiddencache_location[8]          =66   
			ihiddencache_location[9]          =97   
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  6
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	17
				iburiedstash_location[1] =	15
				#ENDIF 
	  		#ENDIF
			
		BREAK
		CASE  77
			itreasure_location[0] 	   		  =4
		    itreasure_location[1] 	   		  =14
			ihiddencache_location[0]          =13	 
			ihiddencache_location[1]          =14   
			ihiddencache_location[2]          =19   
			ihiddencache_location[3]          =25   
			ihiddencache_location[4]          =30   
			ihiddencache_location[5]          =36   
			ihiddencache_location[6]          =48   
			ihiddencache_location[7]          =60   
			ihiddencache_location[8]          =66   
			ihiddencache_location[9]          =97   
			
			
		
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   9
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 6
				iburiedstash_location[1] =	13
				#ENDIF 
			#ENDIF 
		
		BREAK
		CASE  78
			itreasure_location[0] 	   		  =3
		    itreasure_location[1] 	   		  =16
			ihiddencache_location[0]          =23	 
			ihiddencache_location[1]          =42   
			ihiddencache_location[2]          =51   
			ihiddencache_location[3]          =56   
			ihiddencache_location[4]          =71   
			ihiddencache_location[5]          =75   
			ihiddencache_location[6]          =81   
			ihiddencache_location[7]          =82   
			ihiddencache_location[8]          =85   
			ihiddencache_location[9]          =99   
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   13
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	5
				iburiedstash_location[1] =	 2
				#ENDIF 
			#ENDIF 
		
		BREAK
		CASE  79
			itreasure_location[0] 	   		  =1
		    itreasure_location[1] 	   		  =14
			ihiddencache_location[0]          =5	 
			ihiddencache_location[1]          =15   
			ihiddencache_location[2]          =26   
			ihiddencache_location[3]          =44   
			ihiddencache_location[4]          =49   
			ihiddencache_location[5]          =68   
			ihiddencache_location[6]          =76   
			ihiddencache_location[7]          =78   
			ihiddencache_location[8]          =90   
			ihiddencache_location[9]          =99   
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   20
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	15
				iburiedstash_location[1] =	 1
				#ENDIF 
			#ENDIF 
		

		BREAK
		
		CASE  80
			itreasure_location[0] 	   		  =6
		    itreasure_location[1] 	   		  =15
			ihiddencache_location[0]          =7	 
			ihiddencache_location[1]          =13   
			ihiddencache_location[2]          =28   
			ihiddencache_location[3]          =32  
			ihiddencache_location[4]          =47   
			ihiddencache_location[5]          =56   
			ihiddencache_location[6]          =60   
			ihiddencache_location[7]          =85   
			ihiddencache_location[8]          =88  
			ihiddencache_location[9]          = 97  

			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  10
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 6
				iburiedstash_location[1] =	 0
				#ENDIF 
			#ENDIF 
					
		BREAK
		CASE  81
			itreasure_location[0] 	   		  =5
		    itreasure_location[1] 	   		  =15
			ihiddencache_location[0]          =1	 
			ihiddencache_location[1]          =6    
			ihiddencache_location[2]          =27   
			ihiddencache_location[3]          =52   
			ihiddencache_location[4]          =56   
			ihiddencache_location[5]          =64   
			ihiddencache_location[6]          =68   
			ihiddencache_location[7]          =83   
			ihiddencache_location[8]          =89   
			ihiddencache_location[9]          =92   
			
			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   25
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	12
				iburiedstash_location[1] =	10
				#ENDIF 
			#ENDIF 
		BREAK
		
		CASE  82
			itreasure_location[0] 	   		  =7
		    itreasure_location[1] 	   		  =18
			ihiddencache_location[0]          =15	 
			ihiddencache_location[1]          =28	 
			ihiddencache_location[2]          =33   
			ihiddencache_location[3]          =35   
			ihiddencache_location[4]          =43   
			ihiddencache_location[5]          =49   
			ihiddencache_location[6]          =74   
			ihiddencache_location[7]          =86   
			ihiddencache_location[8]          =93   
			ihiddencache_location[9]          =96

			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =   4
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	 4
				iburiedstash_location[1] =	18
				#ENDIF 
			#ENDIF 			
		BREAK
		CASE  83
			itreasure_location[0] 	   		  =8
			itreasure_location[1] 	   		  =	19
			ihiddencache_location[0]          =5	 
			ihiddencache_location[1]          =10   
			ihiddencache_location[2]          =19   
			ihiddencache_location[3]          =72   
			ihiddencache_location[4]          =75   
			ihiddencache_location[5]          =78   
			ihiddencache_location[6]          =81   
			ihiddencache_location[7]          =84   
			ihiddencache_location[8]          =94   
			ihiddencache_location[9]          =99   

			#IF FEATURE_TUNER					
				ishipwrecked_location 	 =  14
				#IF FEATURE_FIXER
				iburiedstash_location[0] =	11
				iburiedstash_location[1] =	 3
				#ENDIF 
			#ENDIF 
		
		BREAK
            

		DEFAULT 
			SCRIPT_ASSERT("GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY out of range iIndex ")
		BREAK
		
	ENDSWITCH
	
	
	
	
	
	IF g_sMPTunables.idaily_collectables_override_treasure_chest0	!= -1
		itreasure_location[0] = g_sMPTunables.idaily_collectables_override_treasure_chest0
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_treasure_chest0 itreasure_location[0] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_treasure_chest0    )
	ENDIF
	
	
	
	IF g_sMPTunables.idaily_collectables_override_treasure_chest1   != -1
		itreasure_location[1] = g_sMPTunables.idaily_collectables_override_treasure_chest1
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_treasure_chest1 itreasure_location[1] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_treasure_chest1    )
	ENDIF
	
	IF g_sMPTunables.idaily_collectables_override_hidden_cache0     != -1
		ihiddencache_location[0] = g_sMPTunables.idaily_collectables_override_hidden_cache0
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_hidden_cache0 ihiddencache_location[0] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_hidden_cache0    )
	ENDIF
	
	
	IF g_sMPTunables.idaily_collectables_override_hidden_cache1     != -1
		ihiddencache_location[1] = g_sMPTunables.idaily_collectables_override_hidden_cache1
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_hidden_cache1 ihiddencache_location[1] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_hidden_cache1    )
	ENDIF
	
	IF g_sMPTunables.idaily_collectables_override_hidden_cache2     != -1
		ihiddencache_location[2] = g_sMPTunables.idaily_collectables_override_hidden_cache2
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_hidden_cache2 ihiddencache_location[2] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_hidden_cache2    )
	ENDIF
	IF g_sMPTunables.idaily_collectables_override_hidden_cache3     != -1
		ihiddencache_location[3] = g_sMPTunables.idaily_collectables_override_hidden_cache3
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_hidden_cache3 ihiddencache_location[3] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_hidden_cache3    )
	ENDIF
	IF g_sMPTunables.idaily_collectables_override_hidden_cache4     != -1
		ihiddencache_location[4] = g_sMPTunables.idaily_collectables_override_hidden_cache4
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_hidden_cache4 ihiddencache_location[4] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_hidden_cache4    )
	ENDIF
	IF g_sMPTunables.idaily_collectables_override_hidden_cache5     != -1
		ihiddencache_location[5] = g_sMPTunables.idaily_collectables_override_hidden_cache5
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_hidden_cache5 ihiddencache_location[5] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_hidden_cache5    )
	ENDIF
	IF g_sMPTunables.idaily_collectables_override_hidden_cache6     != -1
		ihiddencache_location[6] = g_sMPTunables.idaily_collectables_override_hidden_cache6
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_hidden_cache6 ihiddencache_location[6] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_hidden_cache6    )
	ENDIF
	IF g_sMPTunables.idaily_collectables_override_hidden_cache7     != -1
		ihiddencache_location[7] = g_sMPTunables.idaily_collectables_override_hidden_cache7
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_hidden_cache7 ihiddencache_location[7] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_hidden_cache7    )
	ENDIF
	IF g_sMPTunables.idaily_collectables_override_hidden_cache8     != -1
		ihiddencache_location[8] = g_sMPTunables.idaily_collectables_override_hidden_cache8
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_hidden_cache8 ihiddencache_location[8] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_hidden_cache8    )
	ENDIF
	IF g_sMPTunables.idaily_collectables_override_hidden_cache9     != -1
		ihiddencache_location[9] = g_sMPTunables.idaily_collectables_override_hidden_cache9
		PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY idaily_collectables_override_hidden_cache9 ihiddencache_location[9] over written by tunable  =    ", 
		g_sMPTunables.idaily_collectables_override_hidden_cache9   )
	ENDIF
	
		
	
	PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE ===  GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY iIndex 	  				 =	", iIndex 	  	)
	PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY itreasure_location[0] 	   =	", itreasure_location[0] 	  	)
	PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY itreasure_location[1] 	   =    ", itreasure_location[1] 	    )
	PRINTLN(" [KW Collectables]=== DAILY_COLLECTABLE ===  GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY ihiddencache_location[0]  =    ", ihiddencache_location[0]     )
	PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY ihiddencache_location[1]  =    ", ihiddencache_location[1]     )
	PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY ihiddencache_location[2]  =    ", ihiddencache_location[2]     )
	PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY ihiddencache_location[3]  =    ", ihiddencache_location[3]     )
	PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY ihiddencache_location[4]  =    ", ihiddencache_location[4]     )
	PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY ihiddencache_location[5]  =    ", ihiddencache_location[5]     )
	PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY ihiddencache_location[6]  =    ", ihiddencache_location[6]     )
	PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY ihiddencache_location[7]  =    ", ihiddencache_location[7]     )
	PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE ===GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY ihiddencache_location[8]  =    ", ihiddencache_location[8]     )
	PRINTLN(" [KW Collectables]  === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY ihiddencache_location[9]  =    ", ihiddencache_location[9]     )
						
																			
	#IF FEATURE_TUNER																	
		PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY ishipwrecked_location  	   =	", ishipwrecked_location 	  	)

		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_SHIPWRECKED0, ishipwrecked_location )

	#ENDIF 
	
	#IF FEATURE_FIXER	
		PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY iburiedstash_location[0] 	   =    ", iburiedstash_location[0] 	    )
		PRINTLN(" [KW Collectables] === DAILY_COLLECTABLE === GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY iburiedstash_location[1] 	   =    ", iburiedstash_location[1] 	    )
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_BURIEDSTASH0, iburiedstash_location[0] )
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECT_BURIEDSTASH1, iburiedstash_location[1] )
	#ENDIF 	
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_TREASURE0, itreasure_location[0] )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_TREASURE1, itreasure_location[1] )
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_HIDECACH0, ihiddencache_location[0]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_HIDECACH1, ihiddencache_location[1]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_HIDECACH2, ihiddencache_location[2]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_HIDECACH3, ihiddencache_location[3]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_HIDECACH4, ihiddencache_location[4]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_HIDECACH5, ihiddencache_location[5]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_HIDECACH6, ihiddencache_location[6]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_HIDECACH7, ihiddencache_location[7]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_HIDECACH8, ihiddencache_location[8]  )
	SET_MP_INT_CHARACTER_STAT(MP_STAT_DAILYCOLLECTABLES_HIDECACH9, ihiddencache_location[9]  )

	#IF IS_DEBUG_BUILD
		g_iactive_treasure_chests[0] = itreasure_location[0] 
		g_iactive_treasure_chests[1] =	itreasure_location[1]
		
		g_iactive_underwater_packages[0] =  ihiddencache_location[0]
		g_iactive_underwater_packages[1] =  ihiddencache_location[1]
		g_iactive_underwater_packages[2] =  ihiddencache_location[2]
		g_iactive_underwater_packages[3] =  ihiddencache_location[3]
		g_iactive_underwater_packages[4] =  ihiddencache_location[4]
		g_iactive_underwater_packages[5] =  ihiddencache_location[5]
		g_iactive_underwater_packages[6] =  ihiddencache_location[6]
		g_iactive_underwater_packages[7] =  ihiddencache_location[7]
		g_iactive_underwater_packages[8] =  ihiddencache_location[8]
		g_iactive_underwater_packages[9] =  ihiddencache_location[9]
		
		#IF FEATURE_FIXER
		g_iactive_buried_stash[0] =  iburiedstash_location[0]
		g_iactive_buried_stash[1] =  iburiedstash_location[1]		
		#ENDIF 
		
		#IF FEATURE_TUNER
			g_iactive_shipwrecked =  ishipwrecked_location
		#ENDIF 


		g_daily_collectable_day = iIndex
	#ENDIF 

	
			
	#IF FEATURE_DLC_1_2022
		GET_DAILY_COLLECTABLE_SKYDIVES_IN_ORDER_ACCORDING_TO_DAY()
	#ENDIF 
	
	#IF FEATURE_DLC_2_2022	
		 GET_DAILY_COLLECTABLE_DEAD_DROP_IN_ORDER_ACCORDING_TO_DAY()
	#ENDIF		
ENDPROC



// only dealing with daily collectables 
PROC MAINTAIN_DAILY_COLLECTABLES(DAILY_COLLECTABLES_VARS_STRUCT &sDailyCollectablesVarsStruct)
	IF IS_TIME_TO_RESET_DAILY_COLLECTABLES(sDailyCollectablesVarsStruct)
	OR NOT IS_BIT_SET(g_iDailyCollectablesBS,MPDC_BS_LOGGED_IN_TODAY_DAILY_COLLECTABLES)
	#IF IS_DEBUG_BUILD
	OR gb_Reset_daily_collectables
		PRINTLN("[KW Collectables] MAINTAIN_DAILY_COLLECTABLES TRUE Debug")
		gb_Reset_daily_collectables = FALSE
	
	
	#ENDIF
		RESET_DAILY_COLLECTABLE_STATS()
		GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY()
	ENDIF
ENDPROC


FUNC BOOL INIT_DAILY_COLLECTABLES(DAILY_COLLECTABLES_VARS_STRUCT &sDailyCollectablesVarsStruct)	
	sDailyCollectablesVarsStruct.iResetCheckTime = RESET_CHECK_TIME
	GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY(FALSE)	
	RETURN TRUE
ENDFUNC






PROC MAINTAIN_AM_DAILY_COLLECTABLES(DAILY_COLLECTABLES_VARS_STRUCT &sDailyCollectablesVarsStruct)
	SWITCH(GET_DAILY_COLLECTABLE_MISSION_STATE(sDailyCollectablesVarsStruct))
		CASE DO_GAME_STATE_INIT
			IF HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_ID())
				IF INIT_DAILY_COLLECTABLES(sDailyCollectablesVarsStruct)
					GET_DAILY_COLLECTABLE_IN_ORDER_ACCORDING_TO_DAY(FALSE)
					sDailyCollectablesVarsStruct.iClientGameStateCollectables = DO_GAME_STATE_RUNNING
					CPRINTLN(DEBUG_NET_AMBIENT, "[KW Collectables] === DAILY_COLLECTABLES === sDailyCollectablesVarsStruct.iClientGameState = DO_GAME_STATE_RUNNING")
				ENDIF
			ENDIF
				
			
		BREAK

		CASE DO_GAME_STATE_RUNNING
			MAINTAIN_DAILY_COLLECTABLES(sDailyCollectablesVarsStruct)
		BREAK
		
	ENDSWITCH
ENDPROC

