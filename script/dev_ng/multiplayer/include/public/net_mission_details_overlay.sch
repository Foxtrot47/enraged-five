USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "net_mission.sch"
USING "net_mission_info.sch"
USING "Net_hud_displays.sch"
USING "net_mission_details_hud.sch"

//#IF IS_DEBUG_BUILD
//USING "net_hud_debug.sch"
//#ENDIF

// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
//
//      MISSION NAME    :   net_mission_details_overlay.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains the display routines and access functions for the mission details overlay.
//
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************


// PURPOSE:	Calculate the current size of the Help Text box if on-screen
//
// RETURN VALUE:		FLOAT			Help Text Box Height
//
// KEITH NOTE:	Should probably move this into a common header: script_HUD.sch, perhaps?
FUNC FLOAT Get_Help_Text_Box_Height()

	IF NOT (IS_HELP_MESSAGE_BEING_DISPLAYED())
		RETURN 0.0
	ENDIF
	
	// Help Text is on display, so get it's size + a bit
	VECTOR vHelpBoxSize = GET_HUD_COMPONENT_SIZE(NEW_HUD_HELP_TEXT)
	vHelpBoxSize.y += Y_SPACE_BELOW_HELP_BOX
		
	RETURN (vHelpBoxSize.y)

ENDFUNC



// ===========================================================================================================
//      CONSTS and VARIABLES
// ===========================================================================================================

CONST_INT ADD_WIDGETS_FOR_OVERLAY_POSITIONING		0

#IF ADD_WIDGETS_FOR_OVERLAY_POSITIONING
// Background Box
TWEAK_FLOAT		MDO_BOX_LEFT			0.050
//TWEAK_FLOAT		MDO_BOX_TOP				0.050
TWEAK_FLOAT		MDO_BOX_WIDTH			0.275
TWEAK_FLOAT		MDO_BOX_HEIGHT			0.380
#ENDIF	//	ADD_WIDGETS_FOR_OVERLAY_POSITIONING

#IF NOT ADD_WIDGETS_FOR_OVERLAY_POSITIONING
// Background Box
CONST_FLOAT		MDO_BOX_LEFT			0.050
//CONST_FLOAT		MDO_BOX_TOP				0.050
CONST_FLOAT		MDO_BOX_WIDTH			0.275
CONST_FLOAT		MDO_BOX_HEIGHT			0.380
#ENDIF	//	NOT ADD_WIDGETS_FOR_OVERLAY_POSITIONING

FLOAT			MDO_BOX_TEXT_WRAP		= MDO_BOX_LEFT + MDO_BOX_WIDTH - 0.01

#IF ADD_WIDGETS_FOR_OVERLAY_POSITIONING
// X Component positioning
TWEAK_FLOAT		MDO_LEFT_XPOS			0.060
TWEAK_FLOAT		MDO_RIGHT_XPOS			0.250

// Y Component Positioning
TWEAK_FLOAT		MDO_START_Y				0.030
TWEAK_FLOAT		MDO_LINE_HEIGHT			0.030
TWEAK_FLOAT		MDO_HALF_LINE_HEIGHT	0.015

// Text Scale
TWEAK_FLOAT		MDO_TEXT_SCALE_X		0.160
TWEAK_FLOAT		MDO_TEXT_SCALE_Y		0.350
#ENDIF	//	ADD_WIDGETS_FOR_OVERLAY_POSITIONING

#IF NOT ADD_WIDGETS_FOR_OVERLAY_POSITIONING
// X Component positioning
CONST_FLOAT		MDO_LEFT_XPOS			0.060
CONST_FLOAT		MDO_RIGHT_XPOS			0.250

// Y Component Positioning
CONST_FLOAT		MDO_START_Y				0.030
CONST_FLOAT		MDO_LINE_HEIGHT			0.030
CONST_FLOAT		MDO_HALF_LINE_HEIGHT	0.015

// Text Scale
CONST_FLOAT		MDO_TEXT_SCALE_X		0.160
CONST_FLOAT		MDO_TEXT_SCALE_Y		0.350
#ENDIF	//	NOT ADD_WIDGETS_FOR_OVERLAY_POSITIONING

// -----------------------------------------------------------------------------------------------------------

CONST_FLOAT		METRES_TO_MILES			0.000621371192






// ===========================================================================================================
//      Mission Details Overlay debug routines
// ===========================================================================================================

#IF IS_DEBUG_BUILD
// PURPOSE:	Display the current Mission Details Overlay Control details
//
// INPUT PARAMS:		paramMission			The Mission ID
//						paramInstanceID			The Instance ID
PROC Debug_Output_Mission_Overlay_Details_To_Console_Log(MP_MISSION paramMission, INT paramInstanceID)

	NET_PRINT("           Mission ID    : ") NET_PRINT(GET_ACTUAL_MP_MISSION_TITLE(paramMission)) NET_NL()
	NET_PRINT("           Instance      : ") NET_PRINT_INT(paramInstanceID) NET_NL()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
// PURPOSE:	Display the current Mission Details Overlay Control details
PROC Debug_Output_Current_Mission_Overlay_Details_To_Console_Log()

	Debug_Output_Mission_Overlay_Details_To_Console_Log(g_sMDOverlayMP.mdoMissionData.mdID.idMission, g_sMDOverlayMP.mdoMissionData.iInstanceId)
	NET_PRINT("           Primary Coords  : ") NET_PRINT_VECTOR(g_sMDOverlayMP.mdoMissionData.mdPrimaryCoords) NET_NL()
	NET_PRINT("           Secondary Coords: ") NET_PRINT_VECTOR(g_sMDOverlayMP.mdoMissionData.mdSecondaryCoords) NET_NL()

ENDPROC
#ENDIF




// ===========================================================================================================
//      Mission Details Overlay Global Variable Clear and Set Routines
// ===========================================================================================================

// PURPOSE:	Clear out all the mission details overlay control variables
//
// INPUT PARAMS:	paramBeingInitialised		TRUE if this is being called during initialisation [default = FALSE]
PROC Clear_MP_Mission_Details_Overlay_Variables( BOOL paramBeingInitialised = FALSE)

	
	DISPLAYING_SCRIPT_HUD(HUDPART_MISSIONBRIEF, FALSE)
	g_sMDOverlayMP.mdoOnDisplay			= FALSE
	g_sMDOverlayMP.mdoDisplayTimeout	= GET_NETWORK_TIME()
	g_sMDOverlayMP.mdoMaxFromTeam		= 0
	// NOTE: To clear it out: create an empty instance of the struct and copy it into the stored version
	MP_MISSION_DATA	emptyMissionDataStruct
	g_sMDOverlayMP.mdoMissionData		= emptyMissionDataStruct
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Clear_MP_Mission_Details_Overlay_Variables() - All details cleared") NET_NL()
	#ENDIF
	
	// Don't update the widgets if being initialised
	IF (paramBeingInitialised)
		EXIT
	ENDIF
	
	// NOTE: There are no widgets yet

ENDPROC



// ===========================================================================================================
//      Mission Details Overlay - helper routines
// ===========================================================================================================




// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the maximum number of players allowed on mission from the player's team
//
// INPUT PARAM:			paramMission		Mission ID
//						paramVariation		Mission Variation
// RETURN VALUE:		INT					The maximum number of player's from players team allowed on the mission
//
// NOTES:	This is temporary functionality that should instead use explicit values once setup
FUNC INT Get_Max_Number_Of_Players_From_Players_Team_Allowed_On_Mission(MP_MISSION paramMission, INT paramVariation)

	// KGM 2/8/11: This is temporary functionality - we need to add more specific information about how many players per team can join a mission
	
	// For Crooks, this should access a function that returns the maximum number of Crooks FROM THE PLAYERS TEAM that can join - at the moment there is just a total for ALL Crooks
	RETURN (GET_MP_MISSION_MAX_NUMBER_OF_CRIMS(paramMission, paramVariation))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the number of players from the player's team currently on the mission
//
// INPUT PARAMS:		paramTeam				Player's Team
//						paramMission			Mission ID
//						paramInstanceID			Instance ID
// RETURN VALUE:		INT						The number of players from the player's team currently on the mission
//
// NOTES:	This is temporary functionality because at the moment it classes all criminals as being on the same team
FUNC INT Get_Number_Of_Players_From_Team_On_Mission(MP_MISSION paramMission, INT paramInstanceID)

	// KGM 2/8/11: This is temporary functionality - some mission will use a max number of Crooks per gang, others will just have a maximum number of Crooks
	//					irrespective of gang. At the moment this just copies the old fucntionality which lumps all Crooks together.
	/*
	IF (IS_PLAYER_COP(PLAYER_ID()))
		// For Cops, this accesses the number of players from the team on the mission
		RETURN (GET_NUM_PLAYERS_IN_TEAM_RUNNING_MP_MISSION_INSTANCE(TEAM_COP, paramMission, paramInstanceID))
	ENDIF
	
	// For Crooks, this should access a function that returns the number of Crooks FROM THE PLAYERS TEAM on mission - at the moment there is just a total for ALL Crooks
	RETURN (GET_NUM_CRIMINALS_RUNNING_MP_MISSION_INSTANCE(paramMission, paramInstanceID))*/
	
	
	RETURN (GET_NUM_PLAYERS_IN_TEAM_RUNNING_MP_MISSION_INSTANCE(GET_PLAYER_TEAM(PLAYER_ID()), paramMission, paramInstanceID))
	
	
ENDFUNC



// PURPOSE:	Get the names of all players currently on the mission
//
// INPUT PARAMS:		paramTeam				Player's Team
//						paramMission			Mission ID
//						paramInstanceID			Instance ID
// RETURN VALUE:		TEXT_LABEL_63[]				Array of all the player names currently on the mission					
//																					//	
PROC Get_Player_All_Names_on_mission(MP_MISSION paramMission, INT paramInstanceID,TEXT_LABEL_63& PlayerNames[])

	INT I
	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(I)
		IF GET_MP_MISSION_PLAYER_IS_ON(playerId) = paramMission
			
			IF GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(playerId) = paramInstanceID
				IF IS_NET_PLAYER_OK(playerId, FALSE)
					PlayerNames[I] = GET_PLAYER_NAME(playerId) 
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC


PROC Get_Player_Index_on_mission(MP_MISSION paramMission, INT paramInstanceID,Player_index& PlayerIndexs[])

	INT I
	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
		
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(I)
		PlayerIndexs[I] = INT_TO_NATIVE(PLAYER_INDEX, -1)
		
			IF GET_MP_MISSION_PLAYER_IS_ON(playerId) = paramMission
				IF GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(playerId) = paramInstanceID

					PlayerIndexs[I] = playerId 
			
				ENDIF
			ENDIF
	//	ENDIF
	ENDFOR
	
ENDPROC


// ===========================================================================================================
//      Mission Details Overlay - drawing routines
// ===========================================================================================================

// PURPOSE: Set Text Colour to White
PROC Set_Text_Colour_White()
	SET_TEXT_COLOUR(255, 255, 255, 255)
ENDPROC


// -----------------------------------------------------------------------------------------------------------


// PURPOSE: Set Text Colour to Dark
PROC Set_Text_Colour_Dark()
	SET_TEXT_COLOUR(80, 80, 80, 255)
ENDPROC


// -----------------------------------------------------------------------------------------------------------


// PURPOSE: Set Text Colour to Red
PROC Set_Text_Colour_Red()
	SET_TEXT_COLOUR(200, 50, 50, 255)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set up the standard Text Drawing details for Mission Details Overlay text
PROC Setup_Standard_MP_Mission_Details_Overlay_Text()

	SET_TEXT_SCALE(MDO_TEXT_SCALE_X, MDO_TEXT_SCALE_Y)
	Set_Text_Colour_White()
	SET_TEXT_WRAP(0.0, MDO_BOX_TEXT_WRAP)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Draw the Mission Details Overlay Box and Title Bar
//
// INPUT PARAMS:		paramYpos			The Y position for displaying text
PROC Draw_MP_Mission_Details_Overlay_Box(FLOAT paramYpos)

	// Draw Background
	DRAW_RECT_FROM_CORNER(MDO_BOX_LEFT, paramYpos, MDO_BOX_WIDTH, MDO_BOX_HEIGHT, 50, 50, 50, 200)
	
	// Draw Title Bar
	DRAW_RECT_FROM_CORNER(MDO_BOX_LEFT, paramYpos, MDO_BOX_WIDTH, MDO_LINE_HEIGHT, 0, 0, 0, 200)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Mission Name from the perspective of the player being given the mission - Crook or Cop
//
// INPUT PARAMS:		paramYpos			The Y position for display
//						paramMission		The MissionID
//						paramTeam			The Player's Tea,
//
// NOTES:	Missions can have two names, one from the Crook perspective and one from the Cop perspective
PROC Display_MP_MDO_Mission_Name_From_Player_Perspective(FLOAT paramYpos, MP_MISSION paramMission, INT paramTeam)

	TEXT_LABEL_15	theMissionName	= GET_MP_MISSION_NAME_TEXT_LABEL(paramMission, paramTeam)
	
	Setup_Standard_MP_Mission_Details_Overlay_Text()
	DISPLAY_TEXT(MDO_LEFT_XPOS, paramYpos, theMissionName)

ENDPROC


// -----------------------------------------------------------------------------------------------------------



// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the team members on the mission, and the maximum team members allowed
//
// INPUT PARAMS:		paramYpos			The Y position for display
//						paramMission		The MissionID
PROC Display_MP_MDO_Team_Members_On_Mission(FLOAT paramYpos, MP_MISSION paramMission)

	INT	teamPlayersOnMission = Get_Number_Of_Players_From_Team_On_Mission(paramMission, g_sMDOverlayMP.mdoMissionData.iInstanceId)
	
	// Is the mission full?
	// NOTE: If oversubscribed, disguise it.
	BOOL missionFull = FALSE
	IF (teamPlayersOnMission >= g_sMDOverlayMP.mdoMaxFromTeam)
		teamPlayersOnMission = g_sMDOverlayMP.mdoMaxFromTeam
		missionFull = TRUE
	ENDIF
	
	// Display the appropriate 'responding' label
	Setup_Standard_MP_Mission_Details_Overlay_Text()
	DISPLAY_TEXT(MDO_LEFT_XPOS, paramYpos, "CnCAD_MDO2")
	
	// Display 'num / max'
	Setup_Standard_MP_Mission_Details_Overlay_Text()
	
	// ...change text colour if mission full
	IF (missionFull)
		Set_Text_Colour_Red()
	ENDIF
	
	// Display Details
	SET_TEXT_RIGHT_JUSTIFY(TRUE)
	DISPLAY_TEXT_WITH_2_NUMBERS(MDO_RIGHT_XPOS, paramYpos, "CnCAD_MD10", teamPlayersOnMission, g_sMDOverlayMP.mdoMaxFromTeam)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Distance to the Mission
//
// INPUT PARAMS:		paramYpos			The Y position for display
//
// NOTES:	Stored in Km but display in Miles
PROC Display_MP_MDO_Distance_To_Mission(FLOAT paramYpos)

	// Get the distance between the mission coords and the player's current coords
	VECTOR xyzPlayer	= GET_PLAYER_COORDS(PLAYER_ID())
	VECTOR xyzMission
	
	xyzMission = g_sMDOverlayMP.mdoMissionData.mdPrimaryCoords
	
	
	FLOAT theDistance = GET_DISTANCE_BETWEEN_COORDS(xyzMission, xyzPlayer)
	
	// Convert to miles from metres
	theDistance *= METRES_TO_MILES

	// Label
	Setup_Standard_MP_Mission_Details_Overlay_Text()
	DISPLAY_TEXT(MDO_LEFT_XPOS, paramYpos, "CnCAD_MDO6")

	// Distance
	Setup_Standard_MP_Mission_Details_Overlay_Text()
	SET_TEXT_RIGHT_JUSTIFY(TRUE)
	DISPLAY_TEXT_WITH_FLOAT(MDO_RIGHT_XPOS, paramYpos, "CnCAD_MD11", theDistance, 2)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the XP for the Mission
//
// INPUT PARAMS:		paramYpos			The Y position for display
//						paramMission		The MissionID
PROC Display_MP_MDO_XP_For_Mission(FLOAT paramYpos, MP_MISSION paramMission)

	// Label
	Setup_Standard_MP_Mission_Details_Overlay_Text()
	
	// XP
	INT	theXP = 0
	
	theXP = GET_MP_MISSION_XP_REWARD_CRIM(paramMission)
	DISPLAY_TEXT(MDO_LEFT_XPOS, paramYpos, "CnCAD_MDO3_CRIM")
	
	Setup_Standard_MP_Mission_Details_Overlay_Text()
	SET_TEXT_RIGHT_JUSTIFY(TRUE)
	DISPLAY_TEXT_WITH_NUMBER(MDO_RIGHT_XPOS, paramYpos, "NUMBER", theXP)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Cash for the Mission
//
// INPUT PARAMS:		paramYpos			The Y position for display
//						paramMission		The MissionID
PROC Display_MP_MDO_Cash_For_Mission(FLOAT paramYpos, MP_MISSION paramMission)

	// Label
	Setup_Standard_MP_Mission_Details_Overlay_Text()
	DISPLAY_TEXT(MDO_LEFT_XPOS, paramYpos, "CnCAD_MDO4")

	// Cash
	INT	theCash	= 0
	
	theCash = GET_MP_MISSION_CASH_REWARD_CRIM(paramMission)
	
	Setup_Standard_MP_Mission_Details_Overlay_Text()
	SET_TEXT_RIGHT_JUSTIFY(TRUE)
	DISPLAY_TEXT_WITH_NUMBER(MDO_RIGHT_XPOS, paramYpos, "NUMBER", theCash)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Rank (difficulty) for the Mission
//
// INPUT PARAMS:		paramYpos			The Y position for display
//						paramMission		The MissionID
PROC Display_MP_MDO_Rank_For_Mission(FLOAT paramYpos, MP_MISSION paramMission)

	// Label
	Setup_Standard_MP_Mission_Details_Overlay_Text()
	DISPLAY_TEXT(MDO_LEFT_XPOS, paramYpos, "CnCAD_MDO5")

	// Rank
	INT  theRank	= GET_RANK_FOR_MP_MISSION(paramMission)
	
	Setup_Standard_MP_Mission_Details_Overlay_Text()
	SET_TEXT_RIGHT_JUSTIFY(TRUE)
	
	// Displays text instead of a rank number if there is a problem, or ranking is not activated
	#IF IS_DEBUG_BUILD
		IF (theRank = NOT_USING_RANKED_TRIGGERING)
			Set_Text_Colour_Dark()
			DISPLAY_TEXT(MDO_RIGHT_XPOS, paramYpos, "RANKOFF")
			EXIT
		ENDIF
		
		IF (theRank = 0)
			Set_Text_Colour_Red()
			DISPLAY_TEXT(MDO_RIGHT_XPOS, paramYpos, "NORANKSET")
			EXIT
		ENDIF
	#ENDIF
	
	// Display the Rank Number
	DISPLAY_TEXT_WITH_NUMBER(MDO_RIGHT_XPOS, paramYpos, "NUMBER", theRank)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Suggested Number of Vehicles to be used on the Mission - Crook or Cop
//
// INPUT PARAMS:		paramYpos			The Y position for display
//						paramMission		The MissionID
FUNC BOOL Display_MP_MDO_Suggested_Num_Vehicles_For_Mission(FLOAT paramYpos, MP_MISSION paramMission)
	
	INT iNumSuggestedCars 
	iNumSuggestedCars = GET_MP_MISSION_SUGGESTED_NUM_VEH_FOR_CRIMS(paramMission)
	
	IF iNumSuggestedCars > 0
		Setup_Standard_MP_Mission_Details_Overlay_Text()
		DISPLAY_TEXT(MDO_LEFT_XPOS, paramYpos, "CnCAD_MD13")	//Suggested number of vehicles
		
		// Change text colour to dark grey
		Setup_Standard_MP_Mission_Details_Overlay_Text()
		SET_TEXT_RIGHT_JUSTIFY(TRUE)
		DISPLAY_TEXT_WITH_NUMBER(MDO_RIGHT_XPOS, paramYpos, "NUMBER", iNumSuggestedCars)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the mission description.
//
// INPUT PARAMS:		paramYpos			The Y position for display
//						paramMission		The MissionID
//						paramTeam			The Player's Team
//						paramInstance		The Instance ID for the mission (KGM 23/9/11: Added to allow me to distinguish between Race and GTA Race - probably TEMP)
PROC Display_MP_MDO_Mission_Description(FLOAT paramYpos, MP_MISSION paramMission, INT paramTeam, INT paramInstance)

	Setup_Standard_MP_Mission_Details_Overlay_Text()
	STRING theDescription = GET_MP_MISSION_DESCRIPTION(paramMission, paramTeam, paramInstance)
	DISPLAY_TEXT(MDO_LEFT_XPOS, paramYpos, theDescription)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Mission Details Overlay, updating any fields that may have changed
PROC Draw_MP_Mission_Details_Overlay_OLD()

	FLOAT yPos = MDO_START_Y + MDO_LINE_HEIGHT
	
	// Get some common details
	MP_MISSION	theMission 	= g_sMDOverlayMP.mdoMissionData.mdID.idMission
	INT			theInstance	= g_sMDOverlayMP.mdoMissionData.iInstanceId
	INT			playersTeam	= GET_PLAYER_TEAM(PLAYER_ID())
	
	// Move down screen to avoid overlapping any help text on display
	FLOAT helpBoxHeight = Get_Help_Text_Box_Height()
	
	// Draw background
	yPos += helpBoxHeight
	//Added By BC: So I know how far to push the other hud elements down or not to display
//	MPGlobalsHud.fMissionIntroBoxYPos = yPos
//	MPGlobalsHud.fMissionIntroHeight = MDO_BOX_HEIGHT
	Draw_MP_Mission_Details_Overlay_Box(yPos)
	
	// Fill Details
	// ...Mission Title
	Display_MP_MDO_Mission_Name_From_Player_Perspective(yPos, theMission, playersTeam)
	yPos += MDO_LINE_HEIGHT
	
	// ...Team Members On Mission
	Display_MP_MDO_Team_Members_On_Mission(yPos, theMission)
	yPos += MDO_LINE_HEIGHT
	
	// ...Distance To Mission
	Display_MP_MDO_Distance_To_Mission(yPos)
	yPos += MDO_LINE_HEIGHT
	
	// ...XP for mission
	Display_MP_MDO_XP_For_Mission(yPos, theMission)
	yPos += MDO_LINE_HEIGHT
	
	// ...Cash for mission
	Display_MP_MDO_Cash_For_Mission(yPos, theMission)
	yPos += MDO_LINE_HEIGHT
	
	// ...Rank (Difficulty) for mission
	Display_MP_MDO_Rank_For_Mission(yPos, theMission)
	yPos += MDO_LINE_HEIGHT
	
	// ...Suggested Number of Vehicles for mission
	yPos += MDO_HALF_LINE_HEIGHT
	IF Display_MP_MDO_Suggested_Num_Vehicles_For_Mission(yPos, theMission)
		yPos += MDO_LINE_HEIGHT
	ENDIF
	
	// ...mission description
	Display_MP_MDO_Mission_Description(yPos, theMission, playersTeam, theInstance)
	
ENDPROC



// ===========================================================================================================
//      The Main MP Mission Details Overlay Control routines
// ===========================================================================================================

// PURPOSE:	A one-off Mission details Overlay initialisation.
PROC Initialise_MP_Mission_Details_Overlay()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Initialise_MP_Mission_Details_Overlay") NET_NL()
	#ENDIF

	BOOL beingInitialised = TRUE
	Clear_MP_Mission_Details_Overlay_Variables(beingInitialised)

ENDPROC


// -----------------------------------------------------------------------------------------------------------




// ===========================================================================================================
//      The MP Mission Details Overlay Public Access routines
// ===========================================================================================================

// PURPOSE:	Check if the Mission Details Overlay is on display
FUNC BOOL Is_MP_Mission_Details_Overlay_On_Display()
	RETURN (g_sMDOverlayMP.mdoOnDisplay)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if these specific Mission Overlay Details are on display
//
// INPUT PARAMS:		paramMission			The Mission ID
//						paramInstanceID			The Instance ID
//
// RETURN VALUE:		BOOL					TRUE if these mission details are on display, otherwise FALSE
FUNC BOOL Is_MP_Mission_Details_Overlay_With_These_Details_On_Display(MP_MISSION paramMission, INT paramInstanceID)

	IF NOT (Is_MP_Mission_Details_Overlay_On_Display())
		RETURN FALSE
	ENDIF

	IF NOT (g_sMDOverlayMP.mdoMissionData.mdID.idMission = paramMission)
		RETURN FALSE
	ENDIF
	
	IF NOT (g_sMDOverlayMP.mdoMissionData.iInstanceId = paramInstanceID)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Switch on the Mission Details Overlay - display forever
//
// INPUT PARAMS:		paramMissionData		The Mission Data
PROC Switch_On_MP_Mission_Details_Overlay(MP_MISSION_DATA paramMissionData)

	// Check for a valid Mission
	IF (paramMissionData.mdID.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Switch_On_MP_Mission_Details_Overlay(): The Mission ID was NULL. Exiting.")
		#ENDIF
		
		EXIT
	ENDIF

	// Check if it is already switched on
	IF (Is_MP_Mission_Details_Overlay_On_Display())
		IF (Is_MP_Mission_Details_Overlay_With_These_Details_On_Display(paramMissionData.mdID.idMission, paramMissionData.iInstanceId))
			EXIT
		ENDIF
		
		// Clear the Existing Details
		Clear_MP_Mission_Details_Overlay_Variables()
	ENDIF
	
	// Store the Mission Details with a display time of forever (ie: needs to be manually switched off)	
	g_sMDOverlayMP.mdoOnDisplay			= TRUE
	g_sMDOverlayMP.mdoDisplayTimeout	= GET_TIME_OFFSET(GET_NETWORK_TIME() , MISSION_DETAILS_OVERLAY_DISPLAY_FOREVER_msec)
	g_sMDOverlayMP.mdoMissionData		= paramMissionData
	
	// Generate any details that need calculated once then stored
	g_sMDOverlayMP.mdoMaxFromTeam		= Get_Max_Number_Of_Players_From_Players_Team_Allowed_On_Mission(paramMissionData.mdID.idMission, paramMissionData.mdID.idVariation)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Switch_On_MP_Mission_Details_Overlay() - Storing New Details") NET_NL()
		Debug_Output_Current_Mission_Overlay_Details_To_Console_Log()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Switch Off the Mission Details Overlay Immediately if the details match
//
// INPUT PARAMS:		paramMission			The Mission ID
//						paramInstanceID			The Instance ID
PROC Switch_Off_MP_Mission_Details_Overlay(MP_MISSION paramMission, INT paramInstanceID)

	// Check for any details on display
	IF NOT (Is_MP_Mission_Details_Overlay_On_Display())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Switch_Off_MP_Mission_Details_Overlay() - but there were no details on display") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Check for specific details
	IF NOT (Is_MP_Mission_Details_Overlay_With_These_Details_On_Display(paramMission, paramInstanceID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Switch_Off_MP_Mission_Details_Overlay() - but these details were not on display") NET_NL()
			Debug_Output_Mission_Overlay_Details_To_Console_Log(paramMission, paramInstanceID)
		#ENDIF
		
		EXIT
	ENDIF
	
	// Clear the Existing Details
	Clear_MP_Mission_Details_Overlay_Variables()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Mission Details Overlay for an extended period when the player joins the mission
PROC Extend_MP_Mission_Details_Overlay_Display_Time_When_Joining_Mission()

	IF NOT (Is_MP_Mission_Details_Overlay_On_Display())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Extend_MP_Mission_Details_Overlay_Display_Time_When_Joining_Mission() - but details are not on display") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Extend the timer for the required time
	g_sMDOverlayMP.mdoDisplayTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME() , MISSION_DETAILS_OVERLAY_DISPLAY_ON_JOIN_msec)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Mission Details Overlay for an extended period when the mission becomes full for that player's team
PROC Extend_MP_Mission_Details_Overlay_Display_Time_When_Mission_Full()

	// Nothing to do if the details are not on display
	IF NOT (Is_MP_Mission_Details_Overlay_On_Display())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Extend_MP_Mission_Details_Overlay_Display_Time_When_Mission_Full() - but details are not on display") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// If the details are still on display then extend the timer for the required time
	g_sMDOverlayMP.mdoDisplayTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME() , MISSION_DETAILS_OVERLAY_DISPLAY_ON_FULL_msec)
	
ENDPROC


