///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        net_test_drive_vars.sch																			///																			
///																													///				
/// Description:  Header file containing constants, structs and enums for test drive vehicles.						///
///																													///
/// Written by:  Joe Davis																							///		
/// Date:		04/04/2022																							///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_include.sch"
USING "net_realty_new.sch"
USING "cutscene_help.sch"

#IF FEATURE_DLC_1_2022

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CONSTANTS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

CONST_FLOAT cfNEAR_TEST_DRIVE_VEHICLE_RANGE_DEFAULT		5.0

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ ENUMS ╞══════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛
ENUM VEHICLE_TEST_DRIVE_LAUNCH_SCRIPT
	VTDLS_DEFAULT,
	
	VTDLS_SIMEON_SHOWROOM
ENDENUM

ENUM VEHICLE_TEST_DRIVE_STATE
	VTDS_DEFAULT,
	VTDS_PLAY_CUTSCENE,
	VTDS_REQUEST_SCRIPT,
	VTDS_SUCCESS
ENDENUM

ENUM VEHICLE_TEST_DRIVE_CUTSCENE_STATE
	VTDCS_DEFAULT,
	VTDCS_CREATE_CLONE,
	VTDCS_CREATE_ASSETS,
	VTDCS_UPDATE,
	VTDCS_CLEANUP
ENDENUM

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ STRUCTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

STRUCT VEHICLE_TEST_DRIVE_SPAWN_POS
	VECTOR vCoords
	FLOAT fHeading
ENDSTRUCT

STRUCT VEHICLE_TEST_DRIVE_RETURN_POINT
	VECTOR vReturnPoint
	BLIP_SPRITE eBlipSprite
	TEXT_LABEL_15 tlBlipName
ENDSTRUCT

STRUCT VEHICLE_TEST_DRIVE_CUTSCENE_DATA
	CAMERA_INDEX camera
	VEHICLE_TEST_DRIVE_CUTSCENE_STATE eState = VTDCS_DEFAULT
	CUTSCENE_HELP_VEHICLE_CLONE sCutsceneVeh
	SCRIPT_TIMER sBailTimer
	PED_INDEX pedPlayerClone
ENDSTRUCT

STRUCT VEHICLE_TEST_DRIVE_STRUCT
	INT iBS, iSpawnPointCounter
	
	VEHICLE_INDEX viVehToTestDrive				= NULL
	
	VEHICLE_TEST_DRIVE_STATE eTestDriveState		= VTDS_DEFAULT
	VEHICLE_TEST_DRIVE_LAUNCH_SCRIPT eLaunchScript 	= VTDLS_DEFAULT
	
	VEHICLE_SETUP_STRUCT_MP sVehSetup
	VEHICLE_TEST_DRIVE_SPAWN_POS sSpawnPoints[NUM_NETWORK_PLAYERS]
	VEHICLE_TEST_DRIVE_RETURN_POINT sReturnPoint
	VEHICLE_TEST_DRIVE_CUTSCENE_DATA sCutscene
ENDSTRUCT

STRUCT VEHICLE_TEST_DRIVE_SCRIPT_DATA_STRUCT
	VEHICLE_TEST_DRIVE_LAUNCH_SCRIPT eLaunchScript 	= VTDLS_DEFAULT
	
	VEHICLE_SETUP_STRUCT_MP sVehSetup
	VEHICLE_TEST_DRIVE_SPAWN_POS sSpawnPoints[NUM_NETWORK_PLAYERS]
	VEHICLE_TEST_DRIVE_RETURN_POINT sReturnPoint
ENDSTRUCT

#ENDIF // #IF FEATURE_DLC_1_2022
