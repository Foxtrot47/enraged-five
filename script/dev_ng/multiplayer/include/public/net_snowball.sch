// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	net_snowball.sch
//		AUTHOR			:	Ryan Baker
//		DESCRIPTION		:   Snowball header for multiplayer
//
// *****************************************************************************************
// *****************************************************************************************


//----------------------
//	INCLUDES
//----------------------
USING "globals.sch"
USING "commands_network.sch"
USING "net_include.sch"
USING "rage_builtins.sch"
USING "net_mission.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_event.sch"
USING "cellphone_public.sch"

//----------------------
//	CONSTANTS
//----------------------
CONST_INT ST_INIT				0
CONST_INT ST_WAITING_RESULT		1
CONST_INT ST_RESULT_READY		2

//----------------------
//	Local Bitset
//----------------------

//SnowballData.iSnowballLocalBitset
CONST_INT NOT_SAFE_TO_PLACE		0
CONST_INT DO_CREATE_SNOWBALL	1
CONST_INT DO_ADD_SNOWBALL_AMMO	2
CONST_INT DO_SHOCKING_EVENT		3
CONST_INT DO_INPUT_DONE			4
CONST_INT DO_EXIT_EARLY_CHECK	5

//SnowballData.iSnowballBitSet
CONST_INT biShapeTestResultsDone	0
CONST_INT biShapeTestFailed			1

//CONST_INT MAX_CARRIED_SNOWBALLS	3	// NOW USE g_sMPTunables.imaxbumberofsnowballs

//----------------------
//	STRUCTS
//----------------------
STRUCT SNOWBALL_DATA
	//OBJECT_INDEX obj
	BOOL bDoPickupShapeTest
	BOOL bDoPickupAnim
	//VECTOR vLocation

	SHAPETEST_INDEX sti_Snowball
	INT iShapeTestStage = ST_INIT

	INT iShockingEvent
	VECTOR vShockingEventPos

	INT iSnowballLocalBitset
	INT iSnowballBitSet
	
	//INT iUseContext = NEW_CONTEXT_INTENTION
	
	#IF IS_DEBUG_BUILD
	VECTOR stDebug1
	VECTOR stDebug2
	VECTOR stDebug3
	VECTOR stDebug4
	#ENDIF
ENDSTRUCT

/// PURPOSE:
///    Creates and places a snowball on the ground 
PROC PLAY_CREATE_SNOWBALL_ANIM(SNOWBALL_DATA &SnowballData)
	IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),"anim@mp_snowball", "pickup_snowball")
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		TASK_PLAY_ANIM(PLAYER_PED_ID(),"anim@mp_snowball", "pickup_snowball", DEFAULT, SLOW_BLEND_OUT, DEFAULT, AF_ABORT_ON_WEAPON_DAMAGE | AF_EXIT_AFTER_INTERRUPTED)
		PRINTLN("net_snowballs - PLAY_SNOWBALL_ANIM - ANIM STARTED")
	ELSE
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		SET_BIT(SnowballData.iSnowballLocalBitset, DO_CREATE_SNOWBALL)
		SET_BIT(SnowballData.iSnowballLocalBitset, DO_ADD_SNOWBALL_AMMO)
		PRINTLN("net_snowballs - PLAY_SNOWBALL_ANIM - ANIM PLAYING")
	ENDIF
ENDPROC

PROC CLEANUP_SNOWBALL(SNOWBALL_DATA &SnowballData)
	/*IF DOES_ENTITY_EXIST(SnowballData.obj)
		DELETE_OBJECT(SnowballData.obj)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_DLC_SNOWBALL))
	SnowballData.vLocation = <<0.0,0.0,0.0>>*/
	SnowballData.iSnowballLocalBitset = 0
	SnowballData.iSnowballBitset = 0
	snowballData.bDoPickupShapeTest = FALSE
	snowballData.bDoPickupAnim = FALSE
	snowballData.iShapeTestStage = ST_INIT
	PRINTLN("net_snowballs - CLEANUP_SNOWBALL")
ENDPROC

//PURPOSE: Creates a snowball obj in the players hand and gives snowball ammo.
PROC CREATE_SNOWBALL(SNOWBALL_DATA &SnowballData)
	//Create
	/*IF IS_BIT_SET(SnowballData.iSnowballLocalBitset, DO_CREATE_SNOWBALL)
		//IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "anim@mp_snowball", GET_SNOWBALL_TYPE_ANIM_NAME(snowballData.iType)) >= 0.1	//0.20
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("CreateObject"))
			SnowballData.obj = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_DLC_SNOWBALL), SnowballData.vLocation, FALSE, TRUE)
			SET_ENTITY_INVINCIBLE(SnowballData.obj,TRUE)
			//FREEZE_ENTITY_POSITION(SnowballData.obj,TRUE)
			
			//ATTACH_ENTITY_TO_ENTITY(SnowballData.obj, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_R_HAND), vSnowballAttachOffset, vSnowballAttachRot, TRUE, TRUE)
			ATTACH_ENTITY_TO_ENTITY(SnowballData.obj, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_PH_R_HAND), <<0, 0, 0>>, <<0, 0, 0>>, TRUE, TRUE)
			
			CLEAR_BIT(SnowballData.iSnowballLocalBitset, DO_CREATE_SNOWBALL)
			PRINTLN("net_snowballs - CREATE_SNOWBALL - SNOWBALL CREATED AND ATTACHED")
		ENDIF
	EL*/
	
	//Give Ammo
	IF IS_BIT_SET(SnowballData.iSnowballLocalBitset, DO_ADD_SNOWBALL_AMMO)
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("CreateObject"))				//IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "anim@mp_snowball", "pickup_snowball") >= 0.90
			ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_SNOWBALL, g_sMPTunables.inumberofsnowballsPickedUp)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_SNOWBALL, 0, TRUE, TRUE)
			//Cap it
			IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_SNOWBALL) > g_sMPTunables.imaxbumberofsnowballs
				SET_PED_AMMO(PLAYER_PED_ID(), WEAPONTYPE_DLC_SNOWBALL, g_sMPTunables.imaxbumberofsnowballs)
			ENDIF
			//FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			PRINTLN("net_snowballs - CREATE_SNOWBALL - GIVE SNOWBALL AMMO - AMMO = ", GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_SNOWBALL))
			CLEAR_BIT(SnowballData.iSnowballLocalBitset, DO_ADD_SNOWBALL_AMMO)
			SET_BIT(SnowballData.iSnowballLocalBitset, DO_EXIT_EARLY_CHECK)
			PRINTLN("net_snowballs - CREATE_SNOWBALL - DO_EXIT_EARLY_CHECK - SET")
			//CLEANUP_SNOWBALL(SnowballData)
		ENDIF
	ENDIF
	
	//Exit Early Check
	IF IS_BIT_SET(SnowballData.iSnowballLocalBitset, DO_EXIT_EARLY_CHECK)
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("Interrupt"))
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_DETONATE)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				PRINTLN("net_snowballs - CREATE_SNOWBALL - EXIT ANIM EARLY DONE")
				CLEAR_BIT(SnowballData.iSnowballLocalBitset, DO_EXIT_EARLY_CHECK)
				CLEANUP_SNOWBALL(SnowballData)
			ENDIF
		ENDIF
	ENDIF
	
	//Cleanup Snowball if player is killed or made ragdoll
	IF IS_BIT_SET(SnowballData.iSnowballLocalBitset, DO_ADD_SNOWBALL_AMMO)
	OR IS_BIT_SET(SnowballData.iSnowballLocalBitset, DO_EXIT_EARLY_CHECK)
		
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
		//Disable the switching of camera. 
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
		
		IF (IS_FAKE_MULTIPLAYER_MODE_SET() AND IS_PED_INJURED(PLAYER_PED_ID()))			//PD 03/12 IS_NET_PLAYER_OK returns false in creator test modes
		OR (NOT IS_FAKE_MULTIPLAYER_MODE_SET() AND NOT IS_NET_PLAYER_OK(PLAYER_ID()))
		OR IS_PED_RAGDOLL(PLAYER_PED_ID())
		OR IS_PED_RUNNING_RAGDOLL_TASK(PLAYER_PED_ID())
		OR IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
		OR IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
		OR NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "anim@mp_snowball", "pickup_snowball")
			PRINTLN("net_snowballs - CREATE_SNOWBALL - ANIM NO LONGER PLAYING SO CLEANUP SNOWBALL")
			CLEANUP_SNOWBALL(SnowballData)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Creates a shocking event when the Snowballs have been triggered
PROC PROCESS_SHOCKING_EVENT(SNOWBALL_DATA &SnowballData)
	IF IS_BIT_SET(SnowballData.iSnowballLocalBitset, DO_SHOCKING_EVENT)
		IF SnowballData.iShockingEvent = 0
		AND NOT IS_VECTOR_ZERO(SnowballData.vShockingEventPos)
			SnowballData.iShockingEvent = ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_NON_VIOLENT_WEAPON_AIMED_AT, SnowballData.vShockingEventPos, 10000)
			IF SnowballData.iShockingEvent > 0
				CLEAR_BIT(SnowballData.iSnowballLocalBitset, DO_SHOCKING_EVENT)
				NET_PRINT_TIME() NET_PRINT("net_snowballs - PROCESS_SHOCKING_EVENT - EVENT_SHOCKING_NON_VIOLENT_WEAPON_AIMED_AT - SHOCKING EVENT CREATED = ") NET_PRINT_INT(SnowballData.iShockingEvent) NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC	

//PURPOSE: Requests and returns TRUE once assets have loaded
FUNC BOOL HAVE_SNOWBALL_ASSETS_LOADED()
	//Requests
	REQUEST_ANIM_DICT("anim@mp_snowball")
	//REQUEST_MODEL(GET_WEAPONTYPE_MODEL(WEAPONTYPE_DLC_SNOWBALL))
	
	//Loaded Checks
	IF NOT HAS_ANIM_DICT_LOADED("anim@mp_snowball")
		RETURN FALSE
	ENDIF
	/*IF NOT HAS_MODEL_LOADED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_DLC_SNOWBALL))
		RETURN FALSE
	ENDIF*/
	
	RETURN TRUE
ENDFUNC

//PURPOSE: DOes the ShapeTest to see if it is safe to pickup a snowball
PROC DO_SNOWBALL_SHAPETEST(SNOWBALL_DATA &SnowballData)
	SWITCH SnowballData.iShapeTestStage
		CASE ST_INIT
			FLOAT fGroundZ
			VECTOR vSnowballLoc
			VECTOR vDim
							
			vDim = <<0.25, 0.25, 2.0>>	//150.0>>
			vSnowballLoc = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0.0,0.5,0.0>>)
			
			GET_GROUND_Z_FOR_3D_COORD(vSnowballLoc, fGroundZ)
			vSnowballLoc.z = fGroundZ
			//snowballData.vLocation = vSnowballLoc
			vSnowballLoc.z += (vDim.z/2.0) + 0.1 // So shapetest doesn't intersect ground
			PRINTLN("net_snowballs - DO_SNOWBALL_SHAPETEST - vDim = ", vDim)
			PRINTLN("net_snowballs - DO_SNOWBALL_SHAPETEST - vSnowballLoc = ", vSnowballLoc)
							
			#IF IS_DEBUG_BUILD
			SnowballData.stDebug1 = vSnowballLoc - (vDim/2.0)
			SnowballData.stDebug2 = vSnowballLoc - (vDim/-2.0)
			PRINTLN("net_snowballs - DO_SNOWBALL_SHAPETEST - SnowballData.stDebug1 = ", SnowballData.stDebug1)
			PRINTLN("net_snowballs - DO_SNOWBALL_SHAPETEST - SnowballData.stDebug2 = ", SnowballData.stDebug2)
			#ENDIF
							
			//Do Shape Test
			SnowballData.sti_Snowball = START_SHAPE_TEST_BOX(vSnowballLoc, vDim, <<0,0,-0.1>>, EULER_YXZ,SCRIPT_INCLUDE_ALL, PLAYER_PED_ID())
			
			PRINTLN("net_snowballs - DO_SNOWBALL_SHAPETEST - STARTED SHAPE TEST AT: ",vSnowballLoc," Dimensions: ",vDim," Rotation: ",<<0,0,-0.1>>)
			SnowballData.iShapeTestStage = ST_WAITING_RESULT
		BREAK
		
		CASE ST_WAITING_RESULT
			VECTOR vTemp1
			VECTOR vTemp2
			ENTITY_INDEX hitEntity
			INT iHitSomething
			
			PRINTLN("net_snowballs - DO_SNOWBALL_SHAPETEST - WAITING FOR RESULT OF SHAPE TEST")
			
			IF GET_SHAPE_TEST_RESULT(SnowballData.sti_Snowball, iHitSomething, vTemp1, vTemp2, hitEntity) = SHAPETEST_STATUS_RESULTS_READY
				PRINTLN("net_snowballs - DO_SNOWBALL_SHAPETEST - SHAPE TEST RESULT READY")
				SET_BIT(SnowballData.iSnowballBitSet, biShapeTestResultsDone)
				IF iHitsomething != 0
					SET_BIT(SnowballData.iSnowballBitSet, biShapeTestFailed)
					PRINTLN("net_snowballs - DO_SNOWBALL_SHAPETEST - biShapeTestFailed")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(SnowballData.iSnowballBitSet, biShapeTestResultsDone)
				PRINTLN("net_snowballs - DO_SNOWBALL_SHAPETEST - SHAPE TEST RESULT READY")
				IF NOT IS_BIT_SET(SnowballData.iSnowballBitSet, biShapeTestFailed)
					PRINTLN("net_snowballs - DO_SNOWBALL_SHAPETEST - SAFE - PLACING SNOWBALL - bDoPickupAnim = TRUE")
					snowballData.bDoPickupAnim = TRUE
				ELSE
					CLEAR_BIT(SnowballData.iSnowballLocalBitset, DO_INPUT_DONE)
					PRINTLN("net_snowballs - DO_SNOWBALL_SHAPETEST - NOT SAFE - ENDING TEST")
				ENDIF
				snowballData.bDoPickupShapeTest = FALSE
				SnowballData.iShapeTestStage = ST_INIT
				CLEAR_BIT(SnowballData.iSnowballBitSet, biShapeTestResultsDone)
				CLEAR_BIT(SnowballData.iSnowballBitSet, biShapeTestFailed)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Returns TRUE when it is okay to pikcup a Snowball
FUNC BOOL IS_SAFE_TO_PICKUP_SNOWBALL()
	IF NOT IS_PLAYER_FREE_FOR_AMBIENT_TASK(PLAYER_ID())
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_PLAYER_FREE_FOR_AMBIENT_TASK")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_PLAYER_CONTROL_ON")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_SKYSWOOP_AT_GROUND")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_SCREEN_FADED_IN")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_PED_IN_ANY_VEHICLE")
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_ENTITY_IN_AIR")
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_ENTITY_IN_WATER")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_ON_VEHICLE(PLAYER_PED_ID())
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_PED_ON_VEHICLE")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_COVER(PLAYER_PED_ID())
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_PED_IN_COVER")
		RETURN FALSE
	ENDIF
	
	IF g_bInYachtJacuzzi
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - g_bInYachtJacuzzi")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())")
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_INTERACTION_ANIM_PLAYING()
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_ANY_INTERACTION_ANIM_PLAYING")
		RETURN FALSE
	ENDIF
	
	IF IS_MP_PASSIVE_MODE_ENABLED()
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_MP_PASSIVE_MODE_ENABLED")
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_CUSTOM_MENU_ON_SCREEN")
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_PAUSE_MENU_ACTIVE")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_PHONE_ONSCREEN")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - g_bBrowserVisible")
		RETURN FALSE
	ENDIF
	
	IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_SHELTERED(PLAYER_PED_ID())
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_PED_SHELTERED")
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_TRANSITION_ACTIVE")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_ENTITY_VISIBLE")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_ARM_WRESTLING)
	OR IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_GOLF)
	OR IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_TENNIS)
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - ON MINIGAME")
		RETURN FALSE
	ENDIF
	
	IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != NULL
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - GET_INTERIOR_FROM_ENTITY")
		RETURN FALSE
	ENDIF
	
	IF IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_PLAYER_COORDS(PLAYER_ID()), WEAPONTYPE_STICKYBOMB, 200, TRUE)
		PRINTLN("net_snowballs - IS_SAFE_TO_PICKUP_SNOWBALL - FALSE - IS_PROJECTILE_TYPE_WITHIN_DISTANCE")
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
	OR g_bDisableXmasSnowForIsland  //second global to kill snow earlier than "on island" is set
		RETURN FALSE
	ENDIF
	#ENDIF
	
	
	RETURN TRUE
ENDFUNC

//PURPOSE: CHecks for an input to signal picking up a Snowball
FUNC BOOL SHOULD_CREATE_SNOWBALL(SNOWBALL_DATA &SnowballData)
	IF SnowballData.bDoPickupShapeTest = FALSE
	AND SnowballData.bDoPickupAnim = FALSE
		//Do Input Checks
		IF NOT IS_BIT_SET(SnowballData.iSnowballLocalBitset, DO_INPUT_DONE)
			
			//Context
			/*IF SnowballData.iUseContext = NEW_CONTEXT_INTENTION
				REGISTER_CONTEXT_INTENTION(SnowballData.iUseContext, CP_MINIMUM_PRIORITY, "", TRUE)
				PRINTLN("net_snowballs - REGISTER_CONTEXT_INTENTION - DONE")
			ENDIF*/
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_DETONATE)
			//IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
				SET_BIT(SnowballData.iSnowballLocalBitset, DO_INPUT_DONE)
				PRINTLN("net_snowballs - SHOULD_CREATE_SNOWBALL - DO_INPUT_DONE - SET")
			ENDIF
		ENDIF
		
		//Do Valid Checks
		IF IS_BIT_SET(SnowballData.iSnowballLocalBitset, DO_INPUT_DONE)
			WEAPON_TYPE CurrentWeapon
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
			IF CurrentWeapon = WEAPONTYPE_UNARMED
			OR CurrentWeapon = WEAPONTYPE_INVALID
			OR CurrentWeapon = WEAPONTYPE_DLC_SNOWBALL
				IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_SNOWBALL) < g_sMPTunables.imaxbumberofsnowballs
					IF IS_SAFE_TO_PICKUP_SNOWBALL()
						HAVE_SNOWBALL_ASSETS_LOADED()
						SnowballData.bDoPickupShapeTest = TRUE
						PRINTLN("net_snowballs - SHOULD_CREATE_SNOWBALL - bDoPickupShapeTest = TRUE")
					ELSE
						CLEAR_BIT(SnowballData.iSnowballLocalBitset, DO_INPUT_DONE)
						PRINTLN("net_snowballs - SHOULD_CREATE_SNOWBALL - NOT SAFE")
					ENDIF
				ELSE
					CLEAR_BIT(SnowballData.iSnowballLocalBitset, DO_INPUT_DONE)
					PRINTLN("net_snowballs - SHOULD_CREATE_SNOWBALL - ALREADY HAVE A SNOWBALL")
				ENDIF
			ELSE
				CLEAR_BIT(SnowballData.iSnowballLocalBitset, DO_INPUT_DONE)
				PRINTLN("net_snowballs - SHOULD_CREATE_SNOWBALL - HAVE ANOTHER WEAPON EQUIPPED")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN SnowballData.bDoPickupShapeTest
ENDFUNC

//PURPOSE: Controls the pickup snowball anim
PROC PROCESS_CREATE_SNOWBALL_ANIM(SNOWBALL_DATA &SnowballData)
	IF snowballData.bDoPickupAnim = TRUE
		IF NOT IS_BIT_SET(SnowballData.iSnowballLocalBitset, DO_CREATE_SNOWBALL)
		AND NOT IS_BIT_SET(SnowballData.iSnowballLocalBitset, DO_ADD_SNOWBALL_AMMO)
		AND NOT IS_BIT_SET(SnowballData.iSnowballLocalBitset, DO_EXIT_EARLY_CHECK)
			IF HAVE_SNOWBALL_ASSETS_LOADED()
				PLAY_CREATE_SNOWBALL_ANIM(SnowballData)
			ELSE
				PRINTLN("net_snowballs - PROCESS_CREATE_SNOWBALL_ANIM - HAVE_SNOWBALL_ASSETS_LOADED = FALSE")
			ENDIF
		ELSE
			CREATE_SNOWBALL(SnowballData)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain the placement and current state of snowballs
FUNC BOOL MAINTAIN_SNOWBALLS(SNOWBALL_DATA &SnowballData)

	//g_sMPTunables.bTurn_snow_on_off = TRUE	//REMOVE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ALKNNLFKAWENLKFNDAIOFELNFOIEAFKNSD>NSEGFKEN
	//g_sMPTunables.bDisable_Snowballs = FALSE	//REMOVE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ALKNNLFKAWENLKFNDAIOFELNFOIEAFKNSD>NSEGFKEN
	
	IF ((g_sMPTunables.bTurn_snow_on_off = TRUE) AND (g_sMPTunables.bDisable_Snowballs = FALSE))
	OR IS_PREV_WEATHER_TYPE("XMAS")
	OR IS_NEXT_WEATHER_TYPE("XMAS")
		#IF FEATURE_HEIST_ISLAND
		IF NOT IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
		AND NOT g_bDisableXmasSnowForIsland  //second global to kill snow earlier than "on island" is set
		#ENDIF
			/*#IF IS_DEBUG_BUILD
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			DRAW_DEBUG_BOX(SnowballData.stDebug1, SnowballData.stDebug2)
			DRAW_DEBUG_BOX(SnowballData.stDebug3, SnowballData.stDebug4)
			#ENDIF*/
				
			IF SHOULD_CREATE_SNOWBALL(SnowballData)
				DO_SNOWBALL_SHAPETEST(SnowballData)
			ENDIF
			
			PROCESS_CREATE_SNOWBALL_ANIM(SnowballData)
			
			PROCESS_SHOCKING_EVENT(SnowballData)
		#IF FEATURE_HEIST_ISLAND
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

