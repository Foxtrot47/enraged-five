///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_scripted_cinematic_cam.sch																	 //
// Description: Scripted cinematic camera																		 //
//																												 //
//																												 //
// Written by:  Online Technical Design: Ata																	 //
// Date:  		28/03/2018																						 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


USING "globals.sch"
USING "MP_globals_FM.sch"
USING "script_network.sch"
USING "net_include.sch"
USING "cellphone_public.sch"


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    CONSTS    ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
CONST_INT NUMBER_OF_CAM_COORD_INDEX 						50

// iLocalBS
CONST_INT NIGHTCLUB_CINEM_CAM_LOCAL_BS_ACTIVATED			0
CONST_INT NIGHTCLUB_CINEM_CAM_LOCAL_BS_FIRST_CAM			1
CONST_INT NIGHTCLUB_CINEM_CAM_LOCAL_BS_AMBIENT_CAM			2
CONST_INT NIGHTCLUB_CINEM_CAM_LOCAL_BS_FOUND_COORDS			3
CONST_INT NIGHTCLUB_CINEM_CAM_LOCAL_BS_GET_RAND_COORDS		4
CONST_INT NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND		5
CONST_INT NIGHTCLUB_CINEM_CAM_LOCAL_BS_CHOOSE_COORD_TYPE	6
CONST_INT NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER	7
CONST_INT NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_RIGHT	8
CONST_INT NIGHTCLUB_CINEM_CAM_LOCAL_BS_CRASH_ZOOM			9

//----------------
// A1 | A2 |
//----------  DJ
// A3 | A4 |
//----------------

CONST_INT TOP_CAM_1											0			// Moving camera in A1
CONST_INT TOP_CAM_2											1			// Moving camera in A2
CONST_INT TOP_CAM_3											2			// Moving camera in A3
CONST_INT TOP_CAM_4											3			// Moving camera in A4
CONST_INT TOP_CAM_5											4			// Moving camera in middle of dance floor
CONST_INT TOP_CAM_6											5			// Camera behind the DJ 
CONST_INT TOP_CAM_7											6			// Static camera in A1
CONST_INT TOP_CAM_8											7			// Static camera in A2
CONST_INT TOP_CAM_9											8			// Static camera in A3
CONST_INT TOP_CAM_10										9			// Static camera in A4

CONST_FLOAT MAX_CAM_X_AXIS									-1593.0
CONST_FLOAT MIN_CAM_X_AXIS									-1599.0

CONST_FLOAT MAX_CAM_Y_AXIS									-3009.0
CONST_FLOAT MIN_CAM_Y_AXIS									-3014.0

CONST_FLOAT MAX_CAM_Z_AXIS									-76.0
CONST_FLOAT MIN_CAM_Z_AXIS									-78.0


CONST_INT AREA_ONE											0
CONST_INT AREA_TWO											1
CONST_INT AREA_THREE										2
CONST_INT AREA_FOUR											3
CONST_INT AREA_DJ											4

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    ENUMS    ╞═════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

ENUM SCRIPT_CINEMATIC_CAM_STATE
	SCC_WAIT_FOR_TRIGGER,
	SCC_INIT,
	SCC_UPDATE,
	SCC_CLEANUP
ENDENUM

ENUM NIGHTCLUB_CINEM_CAMERA_MOVEMENT_STATE
	NCCM_FIND_COORDS,
	NCCM_INTERP_TO_TARGET
ENDENUM

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    STRUCTS    ╞═══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
STRUCT SCRIPT_CINEMATIC_CAM
	CAMERA_INDEX topCamera
	
	SCRIPT_CINEMATIC_CAM_STATE	sScriptCinemCamstate
	NIGHTCLUB_CINEM_CAMERA_MOVEMENT_STATE	sNightclubCinemCamState
	
	VECTOR vCamCurrentPoint1, vCamCurrentPoint2
	VECTOR vCamInitialRot, vCamTargetRot
	VECTOR vCamSearchPoint1, vCamSearchPoint2
	VECTOR vCamNewPoint1, vCamNewPoint2
	VECTOR vPlayerCoords, vRoomCenter, vDjCoord
	
	FLOAT fInitialCamFOV, fTargetCamFOV
	FLOAT fBPM, fAudioTimeS
	FLOAT fHeightLimit 
	INT iLocalBS
	INT iActiveCamPosIndex = -1
	INT fCamDuration
	INT iRandStagger, iCrashZoomStagger
	CAMERA_GRAPH_TYPE InitailCameraGraphType, TargetCameraGraphType
	
	SCRIPT_TIMER sCamCoolDownTimer
ENDSTRUCT

#IF IS_DEBUG_BUILD 
STRUCT SCRIPT_CINEMATIC_CAM_DEBUG
	BOOL bStartCinematicCam, bCleanupCinematicCam, bCreateWidget, bPrintCoords, bUseDebugCoords
	BOOL bGrabInitialCoord, bGrabTargetCoord, bShowAudioDebug
	BOOL bUseRandomCoord, bUseFixedCoords, bUseCameraRotationAroundPlayer, bUseZoomCrash
	INT iCirclePoint
	INT iTestThisCam = -1
	VECTOR vDebugInitPoint[NUMBER_OF_CAM_COORD_INDEX]
	VECTOR vDebugTargetPoint[NUMBER_OF_CAM_COORD_INDEX]
	VECTOR vDebugInitPointRot[NUMBER_OF_CAM_COORD_INDEX]
	VECTOR vDebugTargetPointRot[NUMBER_OF_CAM_COORD_INDEX]
	FLOAT fDebugInitFOV[NUMBER_OF_CAM_COORD_INDEX]
	FLOAT fDebugTargetFOV[NUMBER_OF_CAM_COORD_INDEX]
	INT iDebugCamDuration[NUMBER_OF_CAM_COORD_INDEX]
	INT iDebugCamDurSlider
	INT iDebugInitailCamGraphType[NUMBER_OF_CAM_COORD_INDEX], iDebugTargetCamGraphType[NUMBER_OF_CAM_COORD_INDEX]
	INT iArea
	#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID scriptCinemCamWidgetID
	#ENDIF
	BOOL bPickDebug
	INT iNumDebugCam
ENDSTRUCT
SCRIPT_CINEMATIC_CAM_DEBUG sScriptCinemCamDebug
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════╡    STATE SETTER AND GETTER   ╞═══════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING GET_SCRIPT_CINEMATIC_CAM_STATE_NAME(SCRIPT_CINEMATIC_CAM_STATE sState)
	
	SWITCH sState
		CASE SCC_INIT		RETURN "SCC_INIT"
		CASE SCC_UPDATE		RETURN "SCC_UPDATE"
		CASE SCC_CLEANUP	RETURN "SCC_CLEANUP"
	ENDSWITCH
	
	RETURN "[SCRIPT_CINEM_CAM] GET_SCRIPT_CINEMATIC_CAM_STATE_NAME INVALID STATE!"
	
ENDFUNC 
#ENDIF

PROC SET_SCRIPT_CINEMATIC_CAM_STATE(SCRIPT_CINEMATIC_CAM &sScriptCinemCam, SCRIPT_CINEMATIC_CAM_STATE sState)
	IF sScriptCinemCam.sScriptCinemCamstate != sState
		sScriptCinemCam.sScriptCinemCamstate = sState	
		PRINTLN("[SCRIPT_CINEM_CAM] SET_SCRIPT_CINEMATIC_CAM_STATE: ", GET_SCRIPT_CINEMATIC_CAM_STATE_NAME(sState))
	ENDIF
ENDPROC

FUNC SCRIPT_CINEMATIC_CAM_STATE GET_SCRIPT_CINEMATIC_CAM_STATE(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	RETURN sScriptCinemCam.sScriptCinemCamstate
ENDFUNC 


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡    DEBUG   ╞═══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
#IF IS_DEBUG_BUILD

PROC CREATE_DEBUG_WIDGET()
	IF NOT sScriptCinemCamDebug.bCreateWidget
		sScriptCinemCamDebug.scriptCinemCamWidgetID = GET_ID_OF_TOP_LEVEL_WIDGET_GROUP()
	
		SET_CURRENT_WIDGET_GROUP(sScriptCinemCamDebug.scriptCinemCamWidgetID)
		START_WIDGET_GROUP("Script Cinematic Cam")
			START_WIDGET_GROUP("Trigger Cinematic Camera")
				ADD_WIDGET_BOOL("Start Cinematic Camera", sScriptCinemCamDebug.bStartCinematicCam)
				ADD_WIDGET_BOOL("Cleanup Cinematic Camera", sScriptCinemCamDebug.bCleanupCinematicCam)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Camera Pos & Rot")
				ADD_WIDGET_STRING("How to use: 1. Pick Camera 2. Pick area 3. Grab inital camera coords 4. Grab target camera coords 5. Once happy with all coords grabbed Copy All Coords to temp_debug")
				ADD_WIDGET_BOOL("TICK THIS TO USE DEBUG COORDS", sScriptCinemCamDebug.bUseDebugCoords)
				START_NEW_WIDGET_COMBO()
					TEXT_LABEL_15 sCamIndex
					INT i
					FOR i = 0 TO NUMBER_OF_CAM_COORD_INDEX - 1
						sCamIndex = "Camera "
						sCamIndex += i
						ADD_TO_WIDGET_COMBO(sCamIndex)
					ENDFOR
				STOP_WIDGET_COMBO("Pick Camera ", sScriptCinemCamDebug.iNumDebugCam)
				
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("Area 1")
					ADD_TO_WIDGET_COMBO("Area 2")
					ADD_TO_WIDGET_COMBO("Area 3")
					ADD_TO_WIDGET_COMBO("Area 4")
					ADD_TO_WIDGET_COMBO("Area DJ")
					ADD_TO_WIDGET_COMBO("Ambient")
				STOP_WIDGET_COMBO("Pick Area ", sScriptCinemCamDebug.iArea)
				
				ADD_WIDGET_BOOL("Grab inital camera info", sScriptCinemCamDebug.bGrabInitialCoord)
				ADD_WIDGET_BOOL("Grab target camera info", sScriptCinemCamDebug.bGrabTargetCoord)
				
				IF sScriptCinemCamDebug.iNumDebugCam != -1
				AND sScriptCinemCamDebug.iNumDebugCam < NUMBER_OF_CAM_COORD_INDEX
					ADD_WIDGET_INT_SLIDER("Camera interpolation duration", sScriptCinemCamDebug.iDebugCamDurSlider , 0, 20000, 10)
				ENDIF
				
				ADD_WIDGET_INT_SLIDER("Only Test This cam", sScriptCinemCamDebug.iTestThisCam, -1, 50, 1)
				ADD_WIDGET_BOOL("Copy All Coords to temp_debug", sScriptCinemCamDebug.bPrintCoords)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Camera Audio info")
				ADD_WIDGET_BOOL("Display Audio info", sScriptCinemCamDebug.bShowAudioDebug)
				ADD_WIDGET_BOOL("Use Random Coords", sScriptCinemCamDebug.bUseRandomCoord)
				ADD_WIDGET_BOOL("Use Fixed Coords", sScriptCinemCamDebug.bUseFixedCoords)
				ADD_WIDGET_BOOL("Use Camera rotate around player", sScriptCinemCamDebug.bUseCameraRotationAroundPlayer)
				ADD_WIDGET_BOOL("Use Camera zoom crash", sScriptCinemCamDebug.bUseZoomCrash)
				ADD_WIDGET_INT_SLIDER("iCirclePoint", sScriptCinemCamDebug.iCirclePoint, 0, 20000, 1)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()

		CLEAR_CURRENT_WIDGET_GROUP(sScriptCinemCamDebug.scriptCinemCamWidgetID)
	sScriptCinemCamDebug.bCreateWidget = TRUE
	ENDIF
ENDPROC
#ENDIF

PROC SET_NIGHTCLUB_CINEM_CAM_ACTIVATED(SCRIPT_CINEMATIC_CAM &sScriptCinemCam, BOOL bActivated)
	IF NOT bActivated
		IF IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_ACTIVATED)
			CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_ACTIVATED)
			PRINTLN("[SCRIPT_CINEM_CAM] SET_NIGHTCLUB_CINEM_CAM_ACTIVATED FALSE")
		ENDIF
	ELSE
		IF !IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_ACTIVATED)
			SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_ACTIVATED)
			PRINTLN("[SCRIPT_CINEM_CAM] SET_NIGHTCLUB_CINEM_CAM_ACTIVATED TRUE")
		ENDIF
	ENDIF
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    CLEARNUP   ╞═══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC REMOVE_ALL_TOP_CAMERAS(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	IF DOES_CAM_EXIST(sScriptCinemCam.topCamera)
		DESTROY_CAM(sScriptCinemCam.topCamera)
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_CAM_INTERPOLATING(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	IF DOES_CAM_EXIST(sScriptCinemCam.topCamera)
		IF IS_CAM_INTERPOLATING(sScriptCinemCam.topCamera)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC 

PROC CLEANUP_SCRIPT_CINEMATIC_CAM(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	
//	IF IS_ANY_CAM_INTERPOLATING(sScriptCinemCam)
//	AND g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
//		HIDE_HUD_AND_RADAR_THIS_FRAME()
//		EXIT
//	ENDIF
	
	REMOVE_ALL_TOP_CAMERAS(sScriptCinemCam)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	SET_NIGHTCLUB_CINEM_CAM_ACTIVATED(sScriptCinemCam, FALSE)
	SET_SCRIPT_CINEMATIC_CAM_STATE(sScriptCinemCam, SCC_WAIT_FOR_TRIGGER)
	sScriptCinemCam.sNightclubCinemCamState = NCCM_FIND_COORDS
	RESET_NET_TIMER(sScriptCinemCam.sCamCoolDownTimer)
	
	sScriptCinemCam.iLocalBS = 0
	
	#IF IS_DEBUG_BUILD
	sScriptCinemCamDebug.bStartCinematicCam = FALSE
	sScriptCinemCamDebug.bCleanupCinematicCam = FALSE
	#ENDIF
	
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    PROCEDURES   ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_NIGHTCLUB_CINEM_CAM_ACTIVATED(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	RETURN IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_ACTIVATED)
ENDFUNC

PROC ONLY_ACTIVATE_THIS_CAM(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	IF DOES_CAM_EXIST(sScriptCinemCam.topCamera)
	AND sScriptCinemCam.topCamera != sScriptCinemCam.topCamera
		IF IS_CAM_ACTIVE(sScriptCinemCam.topCamera)
			SET_CAM_ACTIVE(sScriptCinemCam.topCamera, FALSE)
		ENDIF
	ENDIF

	SET_CAM_ACTIVE(sScriptCinemCam.topCamera, TRUE)
ENDPROC

#IF IS_DEBUG_BUILD
PROC MAINTAIN_CINEMATIC_CAM_DEBUG_WIDGET(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	CREATE_DEBUG_WIDGET()
	IF NOT sScriptCinemCamDebug.bCleanupCinematicCam
		
		IF sScriptCinemCamDebug.iNumDebugCam > -1
		AND sScriptCinemCamDebug.iNumDebugCam < NUMBER_OF_CAM_COORD_INDEX
			
			sScriptCinemCamDebug.iDebugCamDuration[sScriptCinemCamDebug.iNumDebugCam] = sScriptCinemCamDebug.iDebugCamDurSlider
			
			IF sScriptCinemCamDebug.bGrabInitialCoord
				sScriptCinemCamDebug.vDebugInitPoint[sScriptCinemCamDebug.iNumDebugCam] 	= GET_FINAL_RENDERED_CAM_COORD()
				sScriptCinemCamDebug.vDebugInitPointRot[sScriptCinemCamDebug.iNumDebugCam] 	= GET_FINAL_RENDERED_CAM_ROT()
			 	sScriptCinemCamDebug.fDebugInitFOV[sScriptCinemCamDebug.iNumDebugCam] 		= GET_FINAL_RENDERED_CAM_FOV()
				sScriptCinemCamDebug.bGrabInitialCoord = FALSE
			ENDIF	
			
			IF sScriptCinemCamDebug.bGrabTargetCoord
				sScriptCinemCamDebug.vDebugTargetPoint[sScriptCinemCamDebug.iNumDebugCam] 		= GET_FINAL_RENDERED_CAM_COORD()
				sScriptCinemCamDebug.vDebugTargetPointRot[sScriptCinemCamDebug.iNumDebugCam] 	= GET_FINAL_RENDERED_CAM_ROT()
			 	sScriptCinemCamDebug.fDebugTargetFOV[sScriptCinemCamDebug.iNumDebugCam] 		= GET_FINAL_RENDERED_CAM_FOV()
				sScriptCinemCamDebug.bGrabTargetCoord = FALSE
			ENDIF

		ENDIF
		
		IF sScriptCinemCamDebug.bPrintCoords
			OPEN_DEBUG_FILE()	
			SAVE_STRING_TO_DEBUG_FILE("//════════════════════════════════════════════════════════════════════") SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("// Area: ") SAVE_INT_TO_DEBUG_FILE(sScriptCinemCamDebug.iArea) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("// Initial Coords") SAVE_NEWLINE_TO_DEBUG_FILE()
			INT iNumCam
			FOR iNumCam = 0 TO NUMBER_OF_CAM_COORD_INDEX - 1
				IF !IS_VECTOR_ZERO(sScriptCinemCamDebug.vDebugInitPoint[iNumCam])
					SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iNumCam)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("sScriptCinemCam.vCamCurrentPoint1 = ") SAVE_VECTOR_TO_DEBUG_FILE(sScriptCinemCamDebug.vDebugInitPoint[iNumCam]) SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("sScriptCinemCam.vCamInitialRot = ") SAVE_VECTOR_TO_DEBUG_FILE(sScriptCinemCamDebug.vDebugInitPointRot[iNumCam]) SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("sScriptCinemCam.fInitialCamFOV = ") SAVE_FLOAT_TO_DEBUG_FILE(sScriptCinemCamDebug.fDebugInitFOV[iNumCam])  SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()		
				ENDIF	
			ENDFOR
			SAVE_STRING_TO_DEBUG_FILE("//════════════════════════════════════════════════════════════════════") SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("//════════════════════════════════════════════════════════════════════") SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("// Target Coords") SAVE_NEWLINE_TO_DEBUG_FILE()
			FOR iNumCam = 0 TO NUMBER_OF_CAM_COORD_INDEX - 1
				IF !IS_VECTOR_ZERO(sScriptCinemCamDebug.vDebugTargetPoint[iNumCam])
					SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iNumCam)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("sScriptCinemCam.vCamCurrentPoint2 = ") SAVE_VECTOR_TO_DEBUG_FILE(sScriptCinemCamDebug.vDebugTargetPoint[iNumCam]) SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("sScriptCinemCam.vCamTargetRot = ") SAVE_VECTOR_TO_DEBUG_FILE(sScriptCinemCamDebug.vDebugTargetPointRot[iNumCam]) SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("sScriptCinemCam.fTargetCamFOV = ") SAVE_FLOAT_TO_DEBUG_FILE(sScriptCinemCamDebug.fDebugTargetFOV[iNumCam])  SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("BREAK")  SAVE_NEWLINE_TO_DEBUG_FILE()		
				ENDIF	
			ENDFOR
			SAVE_STRING_TO_DEBUG_FILE("//════════════════════════════════════════════════════════════════════") SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("//════════════════════════════════════════════════════════════════════") SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("// Camera Duration") SAVE_NEWLINE_TO_DEBUG_FILE()
			FOR iNumCam = 0 TO NUMBER_OF_CAM_COORD_INDEX - 1
				IF sScriptCinemCamDebug.iDebugCamDuration[iNumCam] != 0.0
					SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(iNumCam)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("sScriptCinemCam.fCamDuration = ")
					SAVE_INT_TO_DEBUG_FILE(sScriptCinemCamDebug.iDebugCamDuration[iNumCam])  SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("BREAK")  SAVE_NEWLINE_TO_DEBUG_FILE()		
				ENDIF	
			ENDFOR
			SAVE_STRING_TO_DEBUG_FILE("//════════════════════════════════════════════════════════════════════") SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("//════════════════════════════════════════════════════════════════════") SAVE_NEWLINE_TO_DEBUG_FILE()
			
			CLOSE_DEBUG_FILE()
			sScriptCinemCamDebug.bPrintCoords = FALSE
		ENDIF
	ELSE
		SET_SCRIPT_CINEMATIC_CAM_STATE(sScriptCinemCam, SCC_CLEANUP)
	ENDIF
ENDPROC
#ENDIF

PROC MAINTAIN_SCRIPT_CINEMATIC_CAM_TRIGGER(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	#IF IS_DEBUG_BUILD
		IF sScriptCinemCamDebug.bStartCinematicCam
			SET_SCRIPT_CINEMATIC_CAM_STATE(sScriptCinemCam, SCC_INIT)
		ENDIF
	#ENDIF
	
	IF CAN_LOCAL_PLAYER_USE_DANCE_CAMERA()
	AND NOT IS_NIGHTCLUB_CINEM_CAM_ACTIVATED(sScriptCinemCam)
	AND NOT IS_LOCAL_PLAYER_NIGHTCLUB_RENOVATION_ACTIVE()
		IF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM)
		OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM))
		AND NOT IS_PAUSE_MENU_ACTIVE()
			sScriptCinemCam.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
			SET_NIGHTCLUB_CINEM_CAM_ACTIVATED(sScriptCinemCam, TRUE)
			SET_SCRIPT_CINEMATIC_CAM_STATE(sScriptCinemCam, SCC_INIT)
		ENDIF
	ENDIF
	
ENDPROC

PROC CREATE_TOP_CAMERAS(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	IF NOT DOES_CAM_EXIST(sScriptCinemCam.topCamera)
		sScriptCinemCam.topCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sScriptCinemCam.vCamCurrentPoint1, <<0,0,0>>, 50)
		SHAKE_CAM(sScriptCinemCam.topCamera, "HAND_SHAKE", 0.2)
	ENDIF
ENDPROC 

FUNC BOOL HAS_ALL_TOP_CAMERAS_CREATED(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	IF NOT DOES_CAM_EXIST(sScriptCinemCam.topCamera)
		RETURN FALSE
	ENDIF 
	RETURN TRUE
ENDFUNC 

PROC ACTIVATE_TOP_CAM(SCRIPT_CINEMATIC_CAM &sScriptCinemCam, BOOL bInterp = TRUE)

	IF DOES_CAM_EXIST(sScriptCinemCam.topCamera)
		DESTROY_CAM(sScriptCinemCam.topCamera)
		sScriptCinemCam.topCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sScriptCinemCam.vCamCurrentPoint1, <<0,0,0>>, 50)
		IF NOT IS_CAM_ACTIVE(sScriptCinemCam.topCamera)
			ONLY_ACTIVATE_THIS_CAM(sScriptCinemCam)
			POINT_CAM_AT_ENTITY(sScriptCinemCam.topCamera, PLAYER_PED_ID(), <<0.0,0.0,0.0>>)
			RENDER_SCRIPT_CAMS(TRUE, bInterp, DEFAULT_INTERP_IN_TIME, TRUE, TRUE)
		ELSE
			POINT_CAM_AT_ENTITY(sScriptCinemCam.topCamera, PLAYER_PED_ID(), <<0.0,0.0,0.0>>)
		ENDIF
	ENDIF	
		
ENDPROC


/// PURPOSE:
///    Need to move some cameras lower depending on purchased lighting variation 
/// RETURNS:
///    
FUNC VECTOR GET_CAM_OFFSET_COORDS()
	IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].propertyDetails.bdNightclubData.iBSNightclub, PROPERTY_BROADCAST_BS_OWNS_NIGHTCLUB_LIGHTING_1)
			RETURN <<0,0,1.5>>
		ENDIF
	ENDIF	
	RETURN <<0,0,0>>
ENDFUNC 

FUNC BOOL SHOULD_USE_LIMITED_COORDS_FOR_CAM()
	IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].propertyDetails.bdNightclubData.iBSNightclub, PROPERTY_BROADCAST_BS_OWNS_NIGHTCLUB_LIGHTING_1)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_BEHIND_THE_DJ()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1606.546021,-3012.575439,-77.801262>>,<<1.000000,3.200000,1.000000>>)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1604.841309,-3012.927734,-77.801262>>,<<1.000000,2.400000,1.000000>>)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1602.961060,-3012.760498,-77.801262>>,<<1.000000,3.200000,1.000000>>)
			RETURN TRUE
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC 

PROC GET_CAM_INITAL_FIXED_POS_AREA_ONE(SCRIPT_CINEMATIC_CAM &sScriptCinemCam, INT iPickCamCoordIndex)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF iPickCamCoordIndex > -1
		AND iPickCamCoordIndex < NUMBER_OF_CAM_COORD_INDEX
			#IF IS_DEBUG_BUILD
			IF sScriptCinemCamDebug.bUseDebugCoords
				sScriptCinemCam.vCamCurrentPoint1 = sScriptCinemCamDebug.vDebugInitPoint[iPickCamCoordIndex]
				sScriptCinemCam.vCamInitialRot = sScriptCinemCamDebug.vDebugInitPointRot[iPickCamCoordIndex]
				sScriptCinemCam.fInitialCamFOV = sScriptCinemCamDebug.fDebugInitFOV[iPickCamCoordIndex]
			ENDIF
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF !sScriptCinemCamDebug.bUseDebugCoords
		#ENDIF
		SWITCH iPickCamCoordIndex
			CASE 0
				sScriptCinemCam.vCamCurrentPoint1 = <<-1600.5900, -3018.6272, -77.2516>>
				sScriptCinemCam.vCamInitialRot = <<-10.5858, 0.6626, -38.3855>>
				sScriptCinemCam.fInitialCamFOV = 50.0000
			BREAK
			CASE 1
				IF !IS_PLAYER_BEHIND_THE_DJ()
					sScriptCinemCam.vCamCurrentPoint1 = <<-1601.5830, -3008.3467, -77.4500>>
					sScriptCinemCam.vCamInitialRot = <<-16.3524, 0.6626, -133.6056>>
					sScriptCinemCam.fInitialCamFOV = 50.0000
				ELSE	
					sScriptCinemCam.vCamCurrentPoint1 = <<-1600.4589, -3012.6228, -75.1434>>
					sScriptCinemCam.vCamInitialRot = <<-61.4738, -0.7069, 87.9791>>
					sScriptCinemCam.fInitialCamFOV = 39.4047
				ENDIF
			BREAK
			CASE 2
				IF !IS_PLAYER_BEHIND_THE_DJ()
					sScriptCinemCam.vCamCurrentPoint1 = <<-1592.1415, -3007.8420, -77.2444>>
					sScriptCinemCam.vCamInitialRot = <<-16.0601, 0.6626, 129.2704>>
					sScriptCinemCam.fInitialCamFOV = 50.0000
				ELSE
					sScriptCinemCam.vCamCurrentPoint1 = <<-1601.7611, -3010.5483, -77.3330>>
					sScriptCinemCam.vCamInitialRot = <<-1.7767, -0.7827, 156.5430>>
					sScriptCinemCam.fInitialCamFOV = 24.190
				ENDIF
			BREAK
			CASE 3
				IF !IS_PLAYER_BEHIND_THE_DJ()
					sScriptCinemCam.vCamCurrentPoint1 = <<-1592.3010, -3014.1885, -76.7175>>
					sScriptCinemCam.vCamInitialRot = <<-23.1856, 0.6626, 42.1012>>
					sScriptCinemCam.fInitialCamFOV = 50.0000
				ELSE
					sScriptCinemCam.vCamCurrentPoint1 = <<-1599.9307, -3010.3350, -77.4114>>
					sScriptCinemCam.vCamInitialRot = <<-4.5595, -0.7069, 117.5359>>
					sScriptCinemCam.fInitialCamFOV = 39.4047
				ENDIF
			BREAK
			CASE 4
				sScriptCinemCam.vCamCurrentPoint1 = <<-1604.5786, -3013.9875, -76.4937>>
				sScriptCinemCam.vCamInitialRot = <<-14.3446, 0.6626, -81.0916>>
				sScriptCinemCam.fInitialCamFOV = 50.0000
			BREAK
			CASE 5
				sScriptCinemCam.vCamCurrentPoint1 = <<-1604.8281, -3012.7559, -76.6991>>
				sScriptCinemCam.vCamInitialRot = <<-13.2434, 0.6626, -89.6199>>
				sScriptCinemCam.fInitialCamFOV = 50.0000
			BREAK
			CASE 6
				sScriptCinemCam.vCamCurrentPoint1 = <<-1592.4086, -3012.8821, -75.4116>>
				sScriptCinemCam.vCamInitialRot = <<-16.5231, -5.1752, 82.3962>>
				sScriptCinemCam.fInitialCamFOV = 50.0000
			BREAK
			CASE 7
				sScriptCinemCam.vCamCurrentPoint1 = <<-1601.6871, -3016.4839, -77.2712>>
				sScriptCinemCam.vCamInitialRot = <<-0.7303, -2.0407, -54.3066>>
				sScriptCinemCam.fInitialCamFOV = 30.6066
			BREAK
			CASE 8
				sScriptCinemCam.vCamCurrentPoint1 =  <<-1605.5496, -3015.3750, -75.8246>>
				sScriptCinemCam.vCamInitialRot = <<-7.5426, 0.0011, -60.2352>>
				sScriptCinemCam.fInitialCamFOV = 50
			BREAK
			CASE 9
				IF !IS_PLAYER_BEHIND_THE_DJ()	
					sScriptCinemCam.vCamCurrentPoint1 = <<-1598.9025, -3012.5242, -77.7541>>
					sScriptCinemCam.vCamInitialRot = <<-30.7720, -0.7069, 89.9896>>
					sScriptCinemCam.fInitialCamFOV = 39.4047
				ELSE
					sScriptCinemCam.vCamCurrentPoint1 = <<-1598.9025, -3012.5242, -77.7541>>
					sScriptCinemCam.vCamInitialRot = <<-30.7720, -0.7069, 89.9896>>
					sScriptCinemCam.fInitialCamFOV = 39.4047
				ENDIF
			BREAK
			CASE 10		// DJ close
				sScriptCinemCam.vCamCurrentPoint1 = <<-1601.8866, -3014.1941, -77.3395>>
				sScriptCinemCam.vCamInitialRot =  <<-1.3348, -0.4256, 28.1534>>
				sScriptCinemCam.fInitialCamFOV = 33.0189
			BREAK
			CASE 11
				sScriptCinemCam.vCamCurrentPoint1 = <<-1595.0057, -3009.8589, -77.9300>>
				sScriptCinemCam.vCamInitialRot = <<0.5510, 0.6170, 149.3834>>
				sScriptCinemCam.fInitialCamFOV = 75.0778
			BREAK
			CASE 12
				sScriptCinemCam.vCamCurrentPoint1 = <<-1604.7534, -3012.8411, -76.7170>>
				sScriptCinemCam.vCamInitialRot = <<0.6174, 0.6169, -89.8167>>
				sScriptCinemCam.fInitialCamFOV = 76.9551
			BREAK
			CASE 13
				IF !IS_PLAYER_BEHIND_THE_DJ()	
					sScriptCinemCam.vCamCurrentPoint1 = <<-1591.4622, -3014.7502, -76.4870>>
					sScriptCinemCam.vCamInitialRot = <<-20.2621, 0.7852, 54.0188>>
					sScriptCinemCam.fInitialCamFOV = 48.8682
				ELSE
					sScriptCinemCam.vCamCurrentPoint1 = <<-1601.8866, -3014.1941, -77.3395>>
					sScriptCinemCam.vCamInitialRot = <<-1.3348, -0.4256, 28.1534>>
					sScriptCinemCam.fInitialCamFOV = 33.0189
				ENDIF
			BREAK
			CASE 14 // DJ close
				sScriptCinemCam.vCamCurrentPoint1 = <<-1601.9980, -3010.3704, -77.4223>>
				sScriptCinemCam.vCamInitialRot = <<2.2053, -0.7423, 160.1802>>
				sScriptCinemCam.fInitialCamFOV = 23.0938
			BREAK
		ENDSWITCH 
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
ENDPROC	

PROC GET_CAM_INITAL_CRASH_ZOOM_COORD(SCRIPT_CINEMATIC_CAM &sScriptCinemCam, INT iPickCamCoordIndex)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF iPickCamCoordIndex > -1
		AND iPickCamCoordIndex < NUMBER_OF_CAM_COORD_INDEX
			#IF IS_DEBUG_BUILD
			IF sScriptCinemCamDebug.bUseDebugCoords
				sScriptCinemCam.vCamCurrentPoint1 = sScriptCinemCamDebug.vDebugInitPoint[iPickCamCoordIndex]
				sScriptCinemCam.vCamInitialRot = sScriptCinemCamDebug.vDebugInitPointRot[iPickCamCoordIndex]
				sScriptCinemCam.fInitialCamFOV = sScriptCinemCamDebug.fDebugInitFOV[iPickCamCoordIndex]
			ENDIF
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF !sScriptCinemCamDebug.bUseDebugCoords
		#ENDIF
		SWITCH iPickCamCoordIndex
			CASE 0
				sScriptCinemCam.vCamCurrentPoint1 = <<-1601.0532, -3009.7131, -77.8103>>
				sScriptCinemCam.vCamInitialRot = <<-4.2394, 0.4800, -154.9807>>
				sScriptCinemCam.fInitialCamFOV = 70.0000
			BREAK
			CASE 1
				sScriptCinemCam.vCamCurrentPoint1 = <<-1590.5353, -3015.0667, -77.8145>>
				sScriptCinemCam.vCamInitialRot = <<1.4159, -0.0000, 71.9605>>
				sScriptCinemCam.fInitialCamFOV = 70.0000
			BREAK
			CASE 2
				sScriptCinemCam.vCamCurrentPoint1 = <<-1603.9803, -3013.9661, -76.5175>>
				sScriptCinemCam.vCamInitialRot = <<-6.1123, -0.0000, -83.0849>>
				sScriptCinemCam.fInitialCamFOV = 70.0000
			BREAK
			CASE 3
				sScriptCinemCam.vCamCurrentPoint1 = <<-1600.8678, -3016.3223, -77.7867>>
				sScriptCinemCam.vCamInitialRot = <<6.2475, -0.0000, -35.6863>>
				sScriptCinemCam.fInitialCamFOV = 70.0000
			BREAK
			CASE 4
				sScriptCinemCam.vCamCurrentPoint1 = <<-1601.0992, -3009.2458, -76.3443>>
				sScriptCinemCam.vCamInitialRot = <<-20.5671, 0.0000, -106.4003>>
				sScriptCinemCam.fInitialCamFOV = 70.0000
			BREAK
			CASE 5
				sScriptCinemCam.vCamCurrentPoint1 = <<-1590.9739, -3008.4873, -77.6962>>
				sScriptCinemCam.vCamInitialRot = <<-4.3037, -0.0000, 117.5143>>
				sScriptCinemCam.fInitialCamFOV = 70.0000
			BREAK
		ENDSWITCH 
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
ENDPROC	

PROC GET_CAM_TARGET_CRASH_ZOOM_COORD(SCRIPT_CINEMATIC_CAM &sScriptCinemCam, INT iPickCamCoordIndex)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF iPickCamCoordIndex > -1
		AND iPickCamCoordIndex < NUMBER_OF_CAM_COORD_INDEX
			#IF IS_DEBUG_BUILD
			IF sScriptCinemCamDebug.bUseDebugCoords
				sScriptCinemCam.vCamCurrentPoint2 = sScriptCinemCamDebug.vDebugTargetPoint[iPickCamCoordIndex]
				sScriptCinemCam.vCamTargetRot = sScriptCinemCamDebug.vDebugTargetPointRot[iPickCamCoordIndex]
				sScriptCinemCam.fTargetCamFOV = sScriptCinemCamDebug.fDebugTargetFOV[iPickCamCoordIndex]
			ENDIF
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF !sScriptCinemCamDebug.bUseDebugCoords
		#ENDIF
		SWITCH iPickCamCoordIndex
			CASE 0
				sScriptCinemCam.vCamCurrentPoint2 =  <<-1600.8208, -3009.5959, -77.8119>>
				sScriptCinemCam.vCamTargetRot = <<-2.4211, -0.0183, -137.9944>>
				sScriptCinemCam.fTargetCamFOV = 50.0000
			BREAK
			CASE 1
				sScriptCinemCam.vCamCurrentPoint2 =  <<-1590.2957, -3013.8472, -77.8158>>
				sScriptCinemCam.vCamTargetRot =  <<-1.0827, -0.0414, 85.9180>>
				sScriptCinemCam.fTargetCamFOV = 50.0000
			BREAK
			CASE 2
				sScriptCinemCam.vCamCurrentPoint2 = <<-1603.5598, -3011.9063, -76.5546>>
				sScriptCinemCam.vCamTargetRot = <<-6.1123, -0.0000, -87.2987>>
				sScriptCinemCam.fTargetCamFOV = 50.0000
			BREAK
			CASE 3
				sScriptCinemCam.vCamCurrentPoint2 = <<-1601.4298, -3014.5498, -77.8326>>
				sScriptCinemCam.vCamTargetRot =  <<-2.9452, -0.0523, -74.0972>>
				sScriptCinemCam.fTargetCamFOV = 50.0000
			BREAK
			CASE 4
				sScriptCinemCam.vCamCurrentPoint2 = <<-1601.4490, -3012.4766, -76.3443>>
				sScriptCinemCam.vCamTargetRot = <<-20.5671, -0.0000, -87.2645>>
				sScriptCinemCam.fTargetCamFOV = 50.0000
			BREAK
			CASE 5
				sScriptCinemCam.vCamCurrentPoint2 = <<-1590.6777, -3009.3628, -77.8137>>
				sScriptCinemCam.vCamTargetRot =  <<-2.1512, -0.1144, 117.6323>>
				sScriptCinemCam.fTargetCamFOV = 50.0000
			BREAK
		ENDSWITCH
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF	
ENDPROC

FUNC INT GET_PLAYER_AREA()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF !IS_PLAYER_BEHIND_THE_DJ()		// Behind Dj area
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1590.163452,-3014.809326,-80.005997>>, <<-1596.638306,-3014.799072,-74.006020>>, 4.250000)	// Area 1 
				RETURN AREA_ONE
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1601.668579,-3014.894775,-80.006020>>, <<-1596.038696,-3015.032715,-74.006020>>, 4.500000)	// Area 2 
				RETURN AREA_TWO
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1590.181885,-3010.357666,-80.005989>>, <<-1596.623413,-3010.383789,-74.006020>>, 4.250000)	// Area 3
				RETURN AREA_THREE
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1601.763428,-3010.389648,-80.006020>>, <<-1596.623413,-3010.383789,-74.006020>>, 4.250000)	// Area 4
				RETURN AREA_FOUR
			ENDIF
		ELSE
			RETURN AREA_DJ
		ENDIF
	ENDIF	
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_NEW_COORD_NEAR_TO_PLAYER(SCRIPT_CINEMATIC_CAM &sScriptCinemCam, VECTOR vInitialCoord, VECTOR vTargetCoord)
	
	IF GET_DISTANCE_BETWEEN_COORDS(sScriptCinemCam.vPlayerCoords, PROJECT_POINT_ONTO_LINE(sScriptCinemCam.vPlayerCoords, vInitialCoord, vTargetCoord)) < 2.5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_FOUND_CAMERA_COORDS(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	IF NOT IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_FOUND_COORDS)
		IF NOT IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_GET_RAND_COORDS)
			IF sScriptCinemCam.iRandStagger > 23
			OR sScriptCinemCam.iRandStagger < 0
				sScriptCinemCam.iRandStagger = 0
			ENDIF
			
			SWITCH sScriptCinemCam.iRandStagger
				CASE 0
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010.0, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 1
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 2
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 3
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 4
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 5
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 6
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 7
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 8
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 9
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 10
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 11
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 12
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010.0, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 13
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 14
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 15
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 16
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 17
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 18
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 19
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 20
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 21
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 22
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(-3010, MAX_CAM_Y_AXIS)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
				CASE 23
					sScriptCinemCam.vCamSearchPoint2.x = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_X_AXIS, -1598.0)
					sScriptCinemCam.vCamSearchPoint2.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint2.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
					
					sScriptCinemCam.vCamSearchPoint1.x = GET_RANDOM_FLOAT_IN_RANGE(-1594.0, MAX_CAM_X_AXIS)
					sScriptCinemCam.vCamSearchPoint1.y = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Y_AXIS, -3013.0)
					sScriptCinemCam.vCamSearchPoint1.z = GET_RANDOM_FLOAT_IN_RANGE(MIN_CAM_Z_AXIS, MAX_CAM_Z_AXIS)
				BREAK
			ENDSWITCH
			
			
			sScriptCinemCam.vCamInitialRot	   = <<0.0,0.0,0.0>>	
			sScriptCinemCam.fInitialCamFOV 	   = 50
			sScriptCinemCam.vCamTargetRot	   = <<0.0,0.0,0.0>>
			sScriptCinemCam.fTargetCamFOV 	   = 50
			sScriptCinemCam.iRandStagger++
			
			SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_GET_RAND_COORDS)
		ELSE
			IF IS_NEW_COORD_NEAR_TO_PLAYER(sScriptCinemCam, sScriptCinemCam.vCamSearchPoint1, sScriptCinemCam.vCamSearchPoint2)
			OR IS_POINT_IN_CYLINDER(sScriptCinemCam.vCamSearchPoint1, sScriptCinemCam.vPlayerCoords, 1.0, 4.0)
			OR IS_POINT_IN_CYLINDER(sScriptCinemCam.vCamSearchPoint2, sScriptCinemCam.vPlayerCoords, 1.0, 4.0)
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_GET_RAND_COORDS)
			ELSE
				sScriptCinemCam.vCamNewPoint1 = sScriptCinemCam.vCamSearchPoint1
				sScriptCinemCam.vCamNewPoint2 = sScriptCinemCam.vCamSearchPoint2
				SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_FOUND_COORDS)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC INITIALISE_SCRIPT_CINEMATIC_CAM(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	IF sScriptCinemCam.fInitialCamFOV = 0
		sScriptCinemCam.fInitialCamFOV = 50
	ENDIF
	
	sScriptCinemCam.vRoomCenter = <<-1595.84, -3012.61, -79.0>>
	sScriptCinemCam.vDjCoord = <<-1602.3190,-3012.5508,-77.9172>>
	IF NOT HAS_ALL_TOP_CAMERAS_CREATED(sScriptCinemCam)
		CREATE_TOP_CAMERAS(sScriptCinemCam)
	ELSE
		SET_SCRIPT_CINEMATIC_CAM_STATE(sScriptCinemCam, SCC_UPDATE)
		PRINTLN("[SCRIPT_CINEM_CAM] - INITIALISE_SCRIPT_CINEMATIC_CAM SCC_UPDATE")
	ENDIF
ENDPROC

FUNC BOOL CAN_USE_THIS_CAMERA_IN_AREA(VECTOR vCamCood)
	
	//----------------
	// A1 | A2 |
	//----------  DJ
	// A3 | A4 |
	//----------------	
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF !IS_PLAYER_BEHIND_THE_DJ()		// Behind Dj area
		IF !IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1598.147339,-3011.992920,-80.006020>>, <<-1594.290405,-3012.020996,-74.006020>>, 3.00 )		// Middle of dance floor
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1590.163452,-3014.809326,-80.005997>>, <<-1596.638306,-3014.799072,-74.006020>>, 4.250000)	// Area 1 
				IF IS_POINT_IN_ANGLED_AREA(vCamCood, <<-1588.602539,-3016.123047,-80.005997>>, <<-1596.731445,-3015.880615,-73.255966>>, 7.000)
					PRINTLN("[SCRIPT_CINEM_CAM] - CAN_USE_THIS_CAMERA_IN_AREA POINT FALSE in area 1")
					RETURN FALSE
				ENDIF
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1601.668579,-3014.894775,-80.006020>>, <<-1596.038696,-3015.032715,-74.006020>>, 4.500000)	// Area 2 
				IF IS_POINT_IN_ANGLED_AREA(vCamCood, <<-1601.683105,-3015.988037,-80.005966>>, <<-1596.731445,-3015.880615,-73.255966>>, 7.00000)
					PRINTLN("[SCRIPT_CINEM_CAM] - CAN_USE_THIS_CAMERA_IN_AREA POINT FALSE in area 2")
					RETURN FALSE
				ENDIF
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1590.181885,-3010.357666,-80.005989>>, <<-1596.623413,-3010.383789,-74.006020>>, 4.250000)	// Area 3
				IF IS_POINT_IN_ANGLED_AREA(vCamCood, <<-1588.648682,-3009.421387,-80.006042>>, <<-1595.181763,-3009.387939,-73.256042>>, 6.00000)
					PRINTLN("[SCRIPT_CINEM_CAM] - CAN_USE_THIS_CAMERA_IN_AREA POINT FALSE in area 3")
					RETURN FALSE
				ENDIF
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1601.763428,-3010.389648,-80.006020>>, <<-1596.623413,-3010.383789,-74.006020>>, 4.250000)	// Area 4
				IF IS_POINT_IN_ANGLED_AREA(vCamCood,<<-1602.500732,-3009.108398,-80.006042>>, <<-1596.882690,-3009.293945,-73.256042>>, 7.000)
					PRINTLN("[SCRIPT_CINEM_CAM] - CAN_USE_THIS_CAMERA_IN_AREA POINT FALSE in area 4")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_POINT_IN_ANGLED_AREA(vCamCood, <<-1605.022949,-3007.910400,-80.006042>>, <<-1605.361206,-3018.436035,-73.255966>> , 8.5 )
			PRINTLN("[SCRIPT_CINEM_CAM] - CAN_USE_THIS_CAMERA_IN_AREA POINT FALSE behind DJ")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC GET_CAM_TARGET_FIXED_POS_AREA_ONE(SCRIPT_CINEMATIC_CAM &sScriptCinemCam, INT iPickCamCoordIndex)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF iPickCamCoordIndex > -1
		AND iPickCamCoordIndex < NUMBER_OF_CAM_COORD_INDEX
			#IF IS_DEBUG_BUILD
			IF sScriptCinemCamDebug.bUseDebugCoords
				sScriptCinemCam.vCamCurrentPoint2 = sScriptCinemCamDebug.vDebugTargetPoint[iPickCamCoordIndex]
				sScriptCinemCam.vCamTargetRot = sScriptCinemCamDebug.vDebugTargetPointRot[iPickCamCoordIndex]
				sScriptCinemCam.fTargetCamFOV = sScriptCinemCamDebug.fDebugTargetFOV[iPickCamCoordIndex]
			ENDIF
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF !sScriptCinemCamDebug.bUseDebugCoords
		#ENDIF
		SWITCH iPickCamCoordIndex
			CASE 0
			sScriptCinemCam.vCamCurrentPoint2 = <<-1601.6426, -3008.3018, -77.4222>>
			sScriptCinemCam.vCamTargetRot = <<-10.7252, 0.6626, -120.1841>>
			sScriptCinemCam.fTargetCamFOV = 50.0000
			BREAK
			CASE 1
				IF !IS_PLAYER_BEHIND_THE_DJ()
					sScriptCinemCam.vCamCurrentPoint2 = <<-1591.9124, -3009.1846, -77.3327>>
					sScriptCinemCam.vCamTargetRot = <<-14.1613, 0.6626, 146.4426>>
					sScriptCinemCam.fTargetCamFOV = 50.0000
				ELSE
					sScriptCinemCam.vCamCurrentPoint2 = <<-1601.0938, -3012.6077, -74.9460>>
					sScriptCinemCam.vCamTargetRot = <<-35.0099, -0.7069, 87.9791>>
					sScriptCinemCam.fTargetCamFOV = 39.4047
				ENDIF
			BREAK
			CASE 2
				IF !IS_PLAYER_BEHIND_THE_DJ()
					sScriptCinemCam.vCamCurrentPoint2 = <<-1590.4570, -3015.0667, -77.3399>>
					sScriptCinemCam.vCamTargetRot = <<-14.0196, 0.6626, 55.2445>>
					sScriptCinemCam.fTargetCamFOV = 50.0000
				ELSE
					sScriptCinemCam.vCamCurrentPoint2 = <<-1601.9980, -3010.3704, -77.4223>>
					sScriptCinemCam.vCamTargetRot = <<2.2053, -0.7423, 160.1802>>
					sScriptCinemCam.fTargetCamFOV = 23.0938
				ENDIF
			BREAK
			CASE 3
				IF !IS_PLAYER_BEHIND_THE_DJ()
					sScriptCinemCam.vCamCurrentPoint2 = <<-1602.1799, -3017.8872, -76.9957>>
					sScriptCinemCam.vCamTargetRot = <<-13.1584, 0.6626, -37.6313>>
					sScriptCinemCam.fTargetCamFOV = 50.0000
				ELSE
					sScriptCinemCam.vCamCurrentPoint2 = <<-1599.9556, -3014.7991, -77.4680>>
					sScriptCinemCam.vCamTargetRot = <<-3.1360, -0.7069, 65.6928>>
					sScriptCinemCam.fTargetCamFOV = 39.4047
				ENDIF
			BREAK
			CASE 4
				sScriptCinemCam.vCamCurrentPoint2 = <<-1604.2957, -3011.1304, -76.4502>>
				sScriptCinemCam.vCamTargetRot = <<-15.7531, 0.6626, -112.0842>>
				sScriptCinemCam.fTargetCamFOV = 50.0000
			BREAK
			CASE 5
				sScriptCinemCam.vCamCurrentPoint2 = <<-1594.5065, -3012.7271, -76.4960>>
				sScriptCinemCam.vCamTargetRot = <<-31.0667, 0.6626, -88.8446>>
				sScriptCinemCam.fTargetCamFOV = 50.0000
			BREAK
			CASE 6
				sScriptCinemCam.vCamCurrentPoint2 = <<-1600.7139, -3012.3801, -77.0307>>
				sScriptCinemCam.vCamTargetRot = <<-9.4704, 0.6626, 92.1840>>
				sScriptCinemCam.fTargetCamFOV = 50.0000
			BREAK
			CASE 7
				sScriptCinemCam.vCamCurrentPoint2 = <<-1599.5654, -3015.7505, -77.6659>>
				sScriptCinemCam.vCamTargetRot = <<5.6185, 0.6626, 44.9410>>
				sScriptCinemCam.fTargetCamFOV = 30.6066
			BREAK
			CASE 8
				sScriptCinemCam.vCamCurrentPoint2 = <<-1605.5814, -3009.6396, -76.1800>>
				sScriptCinemCam.vCamTargetRot = <<-7.5426, 0.0011, -130.4987>>
				sScriptCinemCam.fTargetCamFOV = 50.0000
			BREAK
			CASE 9
				IF !IS_PLAYER_BEHIND_THE_DJ()	
					sScriptCinemCam.vCamCurrentPoint2 = <<-1592.3129, -3009.0354, -76.5356>>
					sScriptCinemCam.vCamTargetRot = <<-19.5295, 0.6626, 130.3080>>
					sScriptCinemCam.fTargetCamFOV = 48.8682
				ELSE
					sScriptCinemCam.vCamCurrentPoint2 = <<-1602.7762, -3012.7295, -75.0041>>
					sScriptCinemCam.vCamTargetRot = <<-89.5007, -0.7069, 87.5685>>
					sScriptCinemCam.fTargetCamFOV = 39.4047
				ENDIF
			BREAK
			CASE 10		// Dj Close
				sScriptCinemCam.vCamCurrentPoint2 = <<-1601.8866, -3014.1941, -77.3395>>
				sScriptCinemCam.vCamTargetRot =  <<-1.3348, -0.4256, 28.1534>>
				sScriptCinemCam.fTargetCamFOV = 33.0189
			BREAK
			CASE 11
				sScriptCinemCam.vCamCurrentPoint2 = <<-1598.9460, -3008.9075, -77.7127>>
				sScriptCinemCam.vCamTargetRot = <<2.6079, 0.6170, 160.3622>>
				sScriptCinemCam.fTargetCamFOV = 59.0289
			BREAK
			CASE 12
				sScriptCinemCam.vCamCurrentPoint2 = <<-1594.9761, -3012.6035, -76.8447>>
				sScriptCinemCam.vCamTargetRot = <<-22.2114, 0.6626, -87.5253>>
				sScriptCinemCam.fTargetCamFOV = 76.9551
			BREAK
			CASE 13
				IF !IS_PLAYER_BEHIND_THE_DJ()	
					sScriptCinemCam.vCamCurrentPoint2 = <<-1601.3562, -3008.6177, -76.6103>>
					sScriptCinemCam.vCamTargetRot = <<-20.3526, 0.6626, -125.2307>>
					sScriptCinemCam.fTargetCamFOV = 48.8682
				ELSE
					sScriptCinemCam.vCamCurrentPoint2 = <<-1602.0298, -3011.1733, -77.4426>>
					sScriptCinemCam.vCamTargetRot = <<-1.4369, -0.7827, 155.1778>>
					sScriptCinemCam.fTargetCamFOV = 40.9361
				ENDIF
			BREAK
			CASE 14 // DJ close
				sScriptCinemCam.vCamCurrentPoint2 = <<-1601.9980, -3010.3704, -77.4223>>
				sScriptCinemCam.vCamTargetRot = <<2.2053, -0.7423, 160.1802>>
				sScriptCinemCam.fTargetCamFOV = 23.0938
			BREAK
		ENDSWITCH
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF	
ENDPROC

FUNC BOOL IS_PLAYER_IN_CENTER_OF_NIGHTCLUB()
	RETURN  IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1598.835571,-3012.585693,-80.006058>>, <<-1593.581543,-3012.202148,-76.756058>>, 4.000000)
ENDFUNC

FUNC VECTOR GET_COORD_AROUND_THE_PLAYER(SCRIPT_CINEMATIC_CAM &sScriptCinemCam, INT iPoint)
	
	sScriptCinemCam.fHeightLimit = 1.6
	FLOAT fLengthMultiplier = 1.5 
	
	IF iPoint = 0
		// Point 0 is the one directly above the DJ
		IF IS_PLAYER_BEHIND_THE_DJ()
			sScriptCinemCam.fHeightLimit = 3.0
			RETURN sScriptCinemCam.vDjCoord + <<0.0, 0.0, sScriptCinemCam.fHeightLimit>>
		ELSE // Point 0 is the one directly above the player
			RETURN sScriptCinemCam.vPlayerCoords + <<0.0, 0.0, sScriptCinemCam.fHeightLimit>>
		ENDIF	
	ELIF iPoint > 4
	AND iPoint < 9
		sScriptCinemCam.fHeightLimit = 2.0
	ELIF iPoint > 8
	AND iPoint < 13
		sScriptCinemCam.fHeightLimit =  2.9
	ELIF iPoint > 12
	AND iPoint < 17
		sScriptCinemCam.fHeightLimit =  2.9
	ELIF iPoint > 16
	AND iPoint < 21	
		sScriptCinemCam.fHeightLimit = GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.8)
		fLengthMultiplier = GET_RANDOM_FLOAT_IN_RANGE(1.5, 1.8)
	ELIF iPoint > 20
	AND iPoint < 25	
		sScriptCinemCam.fHeightLimit = 3.0
		fLengthMultiplier = 0.5
	ENDIF
	
	// Otherwise... Each next point is located on a circle in 90 degrees intervals.
	// When one circle is complete (4 points) we move onto the next circle with higher radius and
	// again place points in 90 degrees intervals but to make them not align with
	// previous points we offset each angle by 45 degrees.
	
	iPoint = iPoint - 1
	
	INT iLengthIndex = FLOOR(TO_FLOAT(iPoint) * 0.25) + 1
	FLOAT fLengthModifier = 1.0
	
	SWITCH iLengthIndex
		CASE 1 fLengthModifier = 1.2 BREAK // first circle
		CASE 2 fLengthModifier = 1.0 BREAK // second etc.
		CASE 3 fLengthModifier = 0.85 BREAK
		CASE 4 fLengthModifier = 0.8 BREAK
	ENDSWITCH
	
	FLOAT fLength = TO_FLOAT(iLengthIndex) * fLengthMultiplier * fLengthModifier
	INT iRot = iPoint % 4
	BOOL bOffsetRotation = FLOOR(TO_FLOAT(iPoint) * 0.25) % 2 = 1
	
	VECTOR vDirection = <<1.0, 0.0, 0.0>> * fLength
	
	IF bOffsetRotation
		vDirection = ROTATE_VECTOR_ABOUT_Z(vDirection, 45)
	ENDIF
	
	SWITCH iRot
		CASE 1
			vDirection = ROTATE_VECTOR_ABOUT_Z_ORTHO(vDirection, ROTSTEP_90)
		BREAK
		CASE 2
			vDirection = ROTATE_VECTOR_ABOUT_Z_ORTHO(vDirection, ROTSTEP_180)
		BREAK
		CASE 3
			vDirection = ROTATE_VECTOR_ABOUT_Z_ORTHO(vDirection, ROTSTEP_270)
		BREAK
	ENDSWITCH
	
	VECTOR vFinalPoint
	IF IS_PLAYER_IN_CENTER_OF_NIGHTCLUB()
		vFinalPoint = sScriptCinemCam.vPlayerCoords + <<0.0, 0.0, sScriptCinemCam.fHeightLimit>> + vDirection
	ELSE
		vFinalPoint = sScriptCinemCam.vRoomCenter + <<0.0, 0.0, sScriptCinemCam.fHeightLimit>> + vDirection
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[SCRIPT_CINEM_CAM] -  GET_COORD_AROUND_THE_PLAYER - Computed point ", iPoint, " - it's ", vFinalPoint)
	#ENDIF
	
	RETURN vFinalPoint
ENDFUNC

PROC PROCESS_CRASH_ZOOM_CAMERA(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	sScriptCinemCam.fCamDuration = 6000
	POINT_CAM_AT_COORD(sScriptCinemCam.topCamera, <<sScriptCinemCam.vPlayerCoords.x, sScriptCinemCam.vPlayerCoords.y, sScriptCinemCam.vPlayerCoords.z - 0.3>>)
	IF NOT HAS_NET_TIMER_STARTED(sScriptCinemCam.sCamCoolDownTimer)
		START_NET_TIMER(sScriptCinemCam.sCamCoolDownTimer)
		SET_CAM_PARAMS(sScriptCinemCam.topCamera,  sScriptCinemCam.vCamCurrentPoint2,sScriptCinemCam.vCamTargetRot , sScriptCinemCam.fInitialCamFOV, 3000, GRAPH_TYPE_QUARTIC_EASE_OUT, GRAPH_TYPE_QUARTIC_EASE_OUT)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(sScriptCinemCam.sCamCoolDownTimer, 2500)
		AND NOT HAS_NET_TIMER_EXPIRED(sScriptCinemCam.sCamCoolDownTimer, sScriptCinemCam.fCamDuration)
			IF sScriptCinemCam.fInitialCamFOV > 14
				sScriptCinemCam.fInitialCamFOV =  sScriptCinemCam.fInitialCamFOV - 3
			ENDIF
			SET_CAM_FOV(sScriptCinemCam.topCamera,  sScriptCinemCam.fInitialCamFOV)
		ELIF HAS_NET_TIMER_EXPIRED(sScriptCinemCam.sCamCoolDownTimer, sScriptCinemCam.fCamDuration)	
			sScriptCinemCam.sNightclubCinemCamState = NCCM_INTERP_TO_TARGET
			CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
			CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CHOOSE_COORD_TYPE)
			CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_FOUND_COORDS)
			CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_GET_RAND_COORDS)
			CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_RIGHT)
			CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CRASH_ZOOM)
			IF sScriptCinemCam.fInitialCamFOV > 14
				sScriptCinemCam.fInitialCamFOV =  sScriptCinemCam.fInitialCamFOV - 3
			ENDIF
			sScriptCinemCam.iCrashZoomStagger ++
			CDEBUG1LN(DEBUG_NET_DRONE, "[SCRIPT_CINEM_CAM] - UPDATE_SCRIPT_CINEMATIC_CAM - Zoom NCCM_INTERP_TO_TARGET")
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_CIRCLE_CAMERA(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	FLOAT fCamShake 
			
	sScriptCinemCam.fCamDuration = 8000
	
	FLOAT fCamSpeed = 1.1
	FLOAT fDistanceToCenter = 1.5
	
	IF sScriptCinemCam.fBPM >= 122
	AND sScriptCinemCam.fBPM < 123
		fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.1)
	ELIF sScriptCinemCam.fBPM >= 123
	AND sScriptCinemCam.fBPM < 124
		fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.2, 1.0)
		fCamSpeed = 1.7
	ELIF sScriptCinemCam.fBPM >= 124
	AND sScriptCinemCam.fBPM < 125
		fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.1)
		fCamSpeed = 1.8
	ELIF sScriptCinemCam.fBPM >= 125
		fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.2)
		fCamSpeed = 1.9
	ELSE
		fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.2, 0.8)
	ENDIF
	
	VECTOR vOffset
	
	IF IS_PLAYER_IN_CENTER_OF_NIGHTCLUB()
		vOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sScriptCinemCam.vPlayerCoords, 0.0, <<0.0,0.0,sScriptCinemCam.fHeightLimit>>)
		sScriptCinemCam.fCamDuration = 6000
		fCamSpeed = 0.7
	ELIF  IS_PLAYER_BEHIND_THE_DJ()
		vOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sScriptCinemCam.vDjCoord, 0.0, <<0.0,0.0,sScriptCinemCam.fHeightLimit>>)
		fDistanceToCenter = 1.0
		sScriptCinemCam.fCamDuration = 6000
		fCamSpeed = 0.7
	ELSE
		vOffset =  GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sScriptCinemCam.vRoomCenter, 0.0, <<0.0,0.0,sScriptCinemCam.fHeightLimit>>)
		fDistanceToCenter = 2.0
	ENDIF
	
	VECTOR vforward = NORMALISE_VECTOR(GET_CAM_COORD(sScriptCinemCam.topCamera) - vOffset)
	VECTOR vRight 
		
	IF IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_RIGHT)
		vRight = CROSS_PRODUCT(vforward, <<0.0, 0.0, 1.0>>)
	ELSE
		vRight = CROSS_PRODUCT(vforward, <<0.0, 0.0, -1.0>>)
	ENDIF
	VECTOR vDirCenterToCam 
	vDirCenterToCam = NORMALISE_VECTOR(GET_CAM_COORD(sScriptCinemCam.topCamera) - vOffset)
			
	VECTOR vTargetCoord = (vOffset + vDirCenterToCam * fDistanceToCenter) + (vRight * fCamSpeed * GET_FRAME_TIME())
	
	SET_CAM_PARAMS(sScriptCinemCam.topCamera, vTargetCoord, sScriptCinemCam.vCamTargetRot, sScriptCinemCam.fTargetCamFOV, 0, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
	SET_CAM_FOV(sScriptCinemCam.topCamera, sScriptCinemCam.fTargetCamFOV)
	
	IF NOT IS_CAM_ACTIVE(sScriptCinemCam.topCamera)
		SET_CAM_ACTIVE(sScriptCinemCam.topCamera, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, TRUE, sScriptCinemCam.fCamDuration, TRUE, TRUE)	
		IF IS_PLAYER_IN_CENTER_OF_NIGHTCLUB()
			POINT_CAM_AT_COORD(sScriptCinemCam.topCamera, <<sScriptCinemCam.vPlayerCoords.x, sScriptCinemCam.vPlayerCoords.y, sScriptCinemCam.vPlayerCoords.z - 0.1>>)
		ELIF IS_PLAYER_BEHIND_THE_DJ()
			POINT_CAM_AT_COORD(sScriptCinemCam.topCamera, <<sScriptCinemCam.vDjCoord.x, sScriptCinemCam.vDjCoord.y, sScriptCinemCam.vDjCoord.z - 0.5>>)	
		ELSE
			POINT_CAM_AT_COORD(sScriptCinemCam.topCamera, sScriptCinemCam.vRoomCenter)		
		ENDIF
	ELSE
		IF IS_PLAYER_IN_CENTER_OF_NIGHTCLUB()
			POINT_CAM_AT_COORD(sScriptCinemCam.topCamera, sScriptCinemCam.vPlayerCoords)		
		ELIF IS_PLAYER_BEHIND_THE_DJ()
			POINT_CAM_AT_COORD(sScriptCinemCam.topCamera, sScriptCinemCam.vDjCoord)	
		ELSE
			POINT_CAM_AT_COORD(sScriptCinemCam.topCamera, sScriptCinemCam.vRoomCenter)		
		ENDIF
	ENDIF
	
	SHAKE_CAM(sScriptCinemCam.topCamera, "HAND_SHAKE", fCamShake)
	
	IF NOT HAS_NET_TIMER_STARTED(sScriptCinemCam.sCamCoolDownTimer)
		START_NET_TIMER(sScriptCinemCam.sCamCoolDownTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(sScriptCinemCam.sCamCoolDownTimer, sScriptCinemCam.fCamDuration)
			sScriptCinemCam.sNightclubCinemCamState = NCCM_INTERP_TO_TARGET
			CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CHOOSE_COORD_TYPE)
			CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_FOUND_COORDS)
			CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_GET_RAND_COORDS)
			CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_RIGHT)
			CDEBUG1LN(DEBUG_NET_DRONE, "[SCRIPT_CINEM_CAM] - UPDATE_SCRIPT_CINEMATIC_CAM - Circle NCCM_INTERP_TO_TARGET")
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_RANDOM_CAMERA(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	IF HAS_FOUND_CAMERA_COORDS(sScriptCinemCam)
		sScriptCinemCam.vCamCurrentPoint1 = sScriptCinemCam.vCamNewPoint1
		sScriptCinemCam.vCamCurrentPoint2 = sScriptCinemCam.vCamNewPoint2

		SET_CAM_COORD(sScriptCinemCam.topCamera, sScriptCinemCam.vCamCurrentPoint1)
		SET_CAM_ROT(sScriptCinemCam.topCamera, sScriptCinemCam.vCamInitialRot)
		SET_CAM_FOV(sScriptCinemCam.topCamera, sScriptCinemCam.fInitialCamFOV)
		
		// Camera duration and shake
		//CAMERA_GRAPH_TYPE cameraGraphType
		FLOAT fCamShake 
		
		IF sScriptCinemCam.fCamDuration = 0 
			sScriptCinemCam.fCamDuration = 5000
		ENDIF
		
		IF sScriptCinemCam.fTargetCamFOV = 0
			sScriptCinemCam.fTargetCamFOV = 50  
		ENDIF	
		
		IF sScriptCinemCam.fBPM >= 122
		AND sScriptCinemCam.fBPM < 123
			fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.1)
			sScriptCinemCam.fCamDuration = 6000
		ELIF sScriptCinemCam.fBPM >= 123
		AND sScriptCinemCam.fBPM < 124
			fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.2, 1.0)
			sScriptCinemCam.fCamDuration = 4000
		ELIF sScriptCinemCam.fBPM >= 124
		AND sScriptCinemCam.fBPM < 125
			fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.1)
			sScriptCinemCam.fCamDuration = 3800
		ELIF sScriptCinemCam.fBPM >= 125
			fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.2)
			sScriptCinemCam.fCamDuration = 3500
		ELSE
			fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.2, 0.8)
			sScriptCinemCam.fCamDuration = 5000
		ENDIF


		IF NOT IS_CAM_ACTIVE(sScriptCinemCam.topCamera)
			SET_CAM_ACTIVE(sScriptCinemCam.topCamera, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE, DEFAULT_INTERP_IN_TIME, TRUE, TRUE)	
			POINT_CAM_AT_ENTITY(sScriptCinemCam.topCamera, PLAYER_PED_ID(), <<0.0,0.0,1.0>>)		
		ELSE
			POINT_CAM_AT_ENTITY(sScriptCinemCam.topCamera, PLAYER_PED_ID(), <<0.0,0.0,1.0>>)	
		ENDIF
		
		SET_CAM_PARAMS(sScriptCinemCam.topCamera, sScriptCinemCam.vCamCurrentPoint2, sScriptCinemCam.vCamTargetRot, sScriptCinemCam.fTargetCamFOV, sScriptCinemCam.fCamDuration, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
		
		SHAKE_CAM(sScriptCinemCam.topCamera, "HAND_SHAKE", fCamShake)
		
		REINIT_NET_TIMER(sScriptCinemCam.sCamCoolDownTimer)

		sScriptCinemCam.sNightclubCinemCamState = NCCM_INTERP_TO_TARGET
		
		CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_FOUND_COORDS)
		
		CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_GET_RAND_COORDS)
		
		CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CHOOSE_COORD_TYPE)
		
		SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_FIRST_CAM)
		
		CDEBUG1LN(DEBUG_NET_DRONE, "[SCRIPT_CINEM_CAM] - UPDATE_SCRIPT_CINEMATIC_CAM - NCCM_INTERP_TO_TARGET")
	ELSE
		PRINTLN("[SCRIPT_CINEM_CAM] - UPDATE_SCRIPT_CINEMATIC_CAM HAS_FOUND_CAMERA_COORDS FALSE")
	ENDIF
ENDPROC

PROC PROCESS_FIXED_CAMERA(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	GET_CAM_INITAL_FIXED_POS_AREA_ONE(sScriptCinemCam, sScriptCinemCam.iActiveCamPosIndex)
	GET_CAM_TARGET_FIXED_POS_AREA_ONE(sScriptCinemCam, sScriptCinemCam.iActiveCamPosIndex)
	sScriptCinemCam.iActiveCamPosIndex ++
	
	SET_CAM_COORD(sScriptCinemCam.topCamera, sScriptCinemCam.vCamCurrentPoint1)
	SET_CAM_ROT(sScriptCinemCam.topCamera, sScriptCinemCam.vCamInitialRot)
	SET_CAM_FOV(sScriptCinemCam.topCamera, sScriptCinemCam.fInitialCamFOV)
	
	// Camera duration and shake
	//CAMERA_GRAPH_TYPE cameraGraphType
	FLOAT fCamShake 
	
	IF sScriptCinemCam.fCamDuration = 0 
		sScriptCinemCam.fCamDuration =5000
	ENDIF
	
	IF sScriptCinemCam.fTargetCamFOV = 0
		sScriptCinemCam.fTargetCamFOV = 50  
	ENDIF	
	
	IF sScriptCinemCam.fBPM >= 122
	AND sScriptCinemCam.fBPM < 123
		fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.1)
		sScriptCinemCam.fCamDuration = 6000
	ELIF sScriptCinemCam.fBPM >= 123
	AND sScriptCinemCam.fBPM < 124
		fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.2, 1.0)
		sScriptCinemCam.fCamDuration = 4000
	ELIF sScriptCinemCam.fBPM >= 124
	AND sScriptCinemCam.fBPM < 125
		fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.1)
		sScriptCinemCam.fCamDuration = 3800
	ELIF sScriptCinemCam.fBPM >= 125
		fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.2)
		sScriptCinemCam.fCamDuration = 3500
	ELSE
		fCamShake = GET_RANDOM_FLOAT_IN_RANGE(0.2, 0.8)
		sScriptCinemCam.fCamDuration = 5000
	ENDIF
	
	IF sScriptCinemCam.iActiveCamPosIndex = 10 // Close DJ
	OR sScriptCinemCam.iActiveCamPosIndex = 11 
		sScriptCinemCam.fCamDuration = 7000
		fCamShake = 0.2
	ENDIF
	
	IF NOT IS_CAM_ACTIVE(sScriptCinemCam.topCamera)
		SET_CAM_ACTIVE(sScriptCinemCam.topCamera, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE, DEFAULT_INTERP_IN_TIME, TRUE, TRUE)		
	ENDIF
	
	STOP_CAM_POINTING(sScriptCinemCam.topCamera)
	
	SET_CAM_PARAMS(sScriptCinemCam.topCamera, sScriptCinemCam.vCamCurrentPoint2, sScriptCinemCam.vCamTargetRot, sScriptCinemCam.fTargetCamFOV, sScriptCinemCam.fCamDuration, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
	
	SHAKE_CAM(sScriptCinemCam.topCamera, "HAND_SHAKE", fCamShake)
	
	REINIT_NET_TIMER(sScriptCinemCam.sCamCoolDownTimer)

	sScriptCinemCam.sNightclubCinemCamState = NCCM_INTERP_TO_TARGET
	
	CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_FOUND_COORDS)
	
	CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_GET_RAND_COORDS)

	CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CHOOSE_COORD_TYPE)
	
	SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_FIRST_CAM)
	
	CDEBUG1LN(DEBUG_NET_DRONE, "[SCRIPT_CINEM_CAM] - UPDATE_SCRIPT_CINEMATIC_CAM - NCCM_INTERP_TO_TARGET")
ENDPROC

PROC PICK_CAM_RANDOM_TARGET_CINEMATIC_CAM(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)

	IF NOT IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_FIRST_CAM)
		SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)
	ELSE
		IF NOT IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CHOOSE_COORD_TYPE)
			INT iRand = GET_RANDOM_INT_IN_RANGE(0,7)
			IF iRand = 0
			OR iRand = 2		// Fixed
				#IF IS_DEBUG_BUILD
				IF NOT  sScriptCinemCamDebug.bUseZoomCrash
				#ENDIF
					CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)
					CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CRASH_ZOOM)
					CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
			ELIF iRand = 1				// Circle
			AND (IS_PLAYER_IN_CENTER_OF_NIGHTCLUB() OR IS_PLAYER_BEHIND_THE_DJ())
			AND NOT IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
			#IF IS_DEBUG_BUILD
			OR sScriptCinemCamDebug.bUseCameraRotationAroundPlayer = TRUE
			#ENDIF
				#IF IS_DEBUG_BUILD
				IF NOT  sScriptCinemCamDebug.bUseZoomCrash
				#ENDIF
					SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
					CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)
					CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CRASH_ZOOM)
					IF IS_PLAYER_BEHIND_THE_DJ()
						sScriptCinemCam.vCamCurrentPoint1 = GET_COORD_AROUND_THE_PLAYER(sScriptCinemCam, 0)
					ELSE	
						sScriptCinemCam.vCamCurrentPoint1 = GET_COORD_AROUND_THE_PLAYER(sScriptCinemCam, 20)
					ENDIF	
					sScriptCinemCam.fTargetCamFOV = GET_RANDOM_FLOAT_IN_RANGE(60, 70)
					SET_CAM_PARAMS(sScriptCinemCam.topCamera, sScriptCinemCam.vCamCurrentPoint1, sScriptCinemCam.vCamTargetRot, sScriptCinemCam.fTargetCamFOV, 0, GRAPH_TYPE_QUINTIC_EASE_IN_OUT, GRAPH_TYPE_QUINTIC_EASE_IN_OUT)
					RESET_NET_TIMER(sScriptCinemCam.sCamCoolDownTimer)
					INT iRight = GET_RANDOM_INT_IN_RANGE(0,2)
					IF iRight = 0 
						SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_RIGHT)
					ELSE
						CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_RIGHT)
					ENDIF
				
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
				
			ELIF (iRand = 6			// Crash Zoom 
			AND g_eNightClubAudioIntensityLevel = AUDIO_TAG_HIGH_HANDS
			AND (IS_PLAYER_IN_CENTER_OF_NIGHTCLUB() OR IS_PLAYER_BEHIND_THE_DJ()))
			#IF IS_DEBUG_BUILD
			OR sScriptCinemCamDebug.bUseZoomCrash = TRUE
			#ENDIF
				IF sScriptCinemCam.iCrashZoomStagger < 0
				OR sScriptCinemCam.iCrashZoomStagger > 5
					sScriptCinemCam.iCrashZoomStagger = 0
				ENDIF	
				SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CRASH_ZOOM)
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)
				IF NOT IS_PLAYER_BEHIND_THE_DJ()
					GET_CAM_INITAL_CRASH_ZOOM_COORD(sScriptCinemCam, sScriptCinemCam.iCrashZoomStagger)
					GET_CAM_TARGET_CRASH_ZOOM_COORD(sScriptCinemCam, sScriptCinemCam.iCrashZoomStagger)
				ELSE
					INT iRandom = GET_RANDOM_INT_IN_RANGE(0,2)
					IF iRandom = 0
						sScriptCinemCam.vCamCurrentPoint1 = <<-1600.4570, -3015.2251, -77.0411>>
						sScriptCinemCam.vCamInitialRot = <<-9.4424, -0.7069, 55.8885>>
						sScriptCinemCam.fInitialCamFOV = 39.4047
						
						sScriptCinemCam.vCamCurrentPoint2 = <<-1599.5875, -3012.8191, -77.0091>>
						sScriptCinemCam.vCamTargetRot = <<-9.4424, -0.7069, 91.1980>>
						sScriptCinemCam.fTargetCamFOV = 39.4047
					ELSE
						sScriptCinemCam.vCamCurrentPoint1 = <<-1600.0858, -3010.5779, -76.8350>>
						sScriptCinemCam.vCamInitialRot = <<-13.5770, -0.7069, 108.2223>>
						sScriptCinemCam.fInitialCamFOV = 39.4047
						
						sScriptCinemCam.vCamCurrentPoint2 = <<-1599.6821, -3012.1453, -76.8818>>
						sScriptCinemCam.vCamTargetRot = <<-8.7495, -0.7069, 94.5277>>
						sScriptCinemCam.fTargetCamFOV = 39.4047
					ENDIF
				ENDIF
				SET_CAM_PARAMS(sScriptCinemCam.topCamera,  sScriptCinemCam.vCamCurrentPoint1, sScriptCinemCam.vCamInitialRot , sScriptCinemCam.fInitialCamFOV, 0, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SHAKE_CAM(sScriptCinemCam.topCamera, "HAND_SHAKE", 0.4)
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
				CDEBUG1LN(DEBUG_NET_DRONE, "[SCRIPT_CINEM_CAM] - PICK_CAM_RANDOM_TARGET_CINEMATIC_CAM - set inital crash zoom sScriptCinemCam.iCrashZoomStagger: ", sScriptCinemCam.iCrashZoomStagger)
			ELSE		// random 
				#IF IS_DEBUG_BUILD
				IF NOT  sScriptCinemCamDebug.bUseZoomCrash
				#ENDIF
					SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)
					CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
					CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CRASH_ZOOM)
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF	
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
			ENDIF
			SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CHOOSE_COORD_TYPE)
		
			#IF IS_DEBUG_BUILD
			IF sScriptCinemCamDebug.bUseRandomCoord
				SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CRASH_ZOOM)
			ELIF sScriptCinemCamDebug.bUseFixedCoords
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CRASH_ZOOM)
			ELIF sScriptCinemCamDebug.bUseCameraRotationAroundPlayer
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)
				SET_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CRASH_ZOOM)
			ELIF sScriptCinemCamDebug.bUseZoomCrash
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)
				CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
			ENDIF
			#ENDIF
		
		
		ENDIF	
	ENDIF	

	IF IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CRASH_ZOOM)  // Crash Zoom camera
		PROCESS_CRASH_ZOOM_CAMERA(sScriptCinemCam)
	ELIF IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER) // Circle camera
		PROCESS_CIRCLE_CAMERA(sScriptCinemCam)
	ELIF IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)	// Random camera
		PROCESS_RANDOM_CAMERA(sScriptCinemCam)
	ELSE								// Fixed camera
		PROCESS_FIXED_CAMERA(sScriptCinemCam)
	ENDIF	
ENDPROC 

PROC INTERP_CAM_TO_TARGET(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)	
	IF HAS_NET_TIMER_EXPIRED(sScriptCinemCam.sCamCoolDownTimer, sScriptCinemCam.fCamDuration - 100)
	AND sScriptCinemCam.fAudioTimeS <= 10

		IF sScriptCinemCam.iActiveCamPosIndex < 0
		OR sScriptCinemCam.iActiveCamPosIndex > 14	
			sScriptCinemCam.iActiveCamPosIndex = 0
		ENDIF	
		
		IF sScriptCinemCam.iCrashZoomStagger < 0
		OR sScriptCinemCam.iCrashZoomStagger > 5
			sScriptCinemCam.iCrashZoomStagger = 0
		ENDIF	
		
		CLEAR_BIT(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)
		RESET_NET_TIMER(sScriptCinemCam.sCamCoolDownTimer)
		sScriptCinemCam.sNightclubCinemCamState = NCCM_FIND_COORDS
		CDEBUG1LN(DEBUG_NET_DRONE, "[SCRIPT_CINEM_CAM] - INTERP_CAM_TO_TARGET - done move to NCCM_FIND_COORDS ")
	ENDIF
ENDPROC

PROC SYNC_AUDIO_BEAT_VARIABLES(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	INT iBeatNum
	GET_NEXT_AUDIBLE_BEAT(sScriptCinemCam.fAudioTimeS, sScriptCinemCam.fBPM, iBeatNum)
	#IF IS_DEBUG_BUILD
	IF sScriptCinemCamDebug.bShowAudioDebug
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(sScriptCinemCam.fAudioTimeS), <<0.5,0.1,0.5>>, 255, 0, 0)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(sScriptCinemCam.fBPM), <<0.4,0.1,0.4>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iBeatNum), <<0.3,0.1,0.3>>, 0, 255, 0)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(sScriptCinemCam.iActiveCamPosIndex), <<0.2,0.1,0.3>>, 0, 0, 0)
		IF IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_SHOULD_USE_RAND)
			DRAW_DEBUG_TEXT_2D("Random", <<0.6,0.1,0.3>>, 100, 50, 100)
		ELIF IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CIRCLE_AROUND_PLAYER)
			DRAW_DEBUG_TEXT_2D("Circle", <<0.6,0.1,0.3>>, 100, 50, 100)
		ELIF IS_BIT_SET(sScriptCinemCam.iLocalBS, NIGHTCLUB_CINEM_CAM_LOCAL_BS_CRASH_ZOOM)
			DRAW_DEBUG_TEXT_2D("Zoom", <<0.6,0.1,0.3>>, 100, 50, 100)
		ELSE
			DRAW_DEBUG_TEXT_2D("Fixed", <<0.6,0.1,0.3>>, 100, 50, 100)
		ENDIF
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(sScriptCinemCam.iCrashZoomStagger), <<0.1,0.1,0.3>>, 50, 50, 50)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(sScriptCinemCam.fTargetCamFOV), <<0.1,0.2,0.3>>, 50, 50, 50)
	ENDIF	
	#ENDIF
ENDPROC

PROC UPDATE_SCRIPT_CINEMATIC_CAM(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	
	DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
	
	SYNC_AUDIO_BEAT_VARIABLES(sScriptCinemCam)
	//HAS_FOUND_CAMERA_COORDS(sScriptCinemCam)
	
	SWITCH sScriptCinemCam.sNightclubCinemCamState
		CASE NCCM_FIND_COORDS
			PICK_CAM_RANDOM_TARGET_CINEMATIC_CAM(sScriptCinemCam)
		BREAK
		CASE NCCM_INTERP_TO_TARGET
			INTERP_CAM_TO_TARGET(sScriptCinemCam)
		BREAK
	ENDSWITCH 

ENDPROC 

FUNC BOOL SHOULD_CLEANUP_SCRIPTED_CINEMATIC_CAM(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	IF IS_NIGHTCLUB_CINEM_CAM_ACTIVATED(sScriptCinemCam)
		IF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM)
		OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM) AND NOT IS_PAUSE_MENU_ACTIVE())
		OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CONTEXT)
		OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CONTEXT)
		OR !IS_PLAYER_DANCING(PLAYER_ID())
			RETURN TRUE
		ENDIF
		
		IF g_iSimpleInteriorState != SIMPLE_INT_STATE_IDLE
			RETURN TRUE
		ENDIF
		
		IF NOT CAN_LOCAL_PLAYER_USE_DANCE_CAMERA()
			PRINTLN("[SCRIPT_CINEM_CAM] - SHOULD_CLEANUP_SCRIPTED_CINEMATIC_CAM - CAN_LOCAL_PLAYER_USE_DANCE_CAMERA false")
			RETURN FALSE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PAUSE_CINEM_CAM()
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_COMMERCE_STORE_OPEN()
		PRINTLN("[SCRIPT_CINEM_CAM] - SHOULD_PAUSE_CINEM_CAM - pause or store is open")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SCRIPTED_CINEMATIC_CAM(SCRIPT_CINEMATIC_CAM &sScriptCinemCam)
	
	IF SHOULD_CLEANUP_SCRIPTED_CINEMATIC_CAM(sScriptCinemCam)
		SET_SCRIPT_CINEMATIC_CAM_STATE(sScriptCinemCam, SCC_CLEANUP)
	ENDIF	
	
	IF SHOULD_PAUSE_CINEM_CAM()
		EXIT
	ENDIF
	
	SWITCH GET_SCRIPT_CINEMATIC_CAM_STATE(sScriptCinemCam)
		CASE SCC_WAIT_FOR_TRIGGER
			MAINTAIN_SCRIPT_CINEMATIC_CAM_TRIGGER(sScriptCinemCam)
		BREAK
		CASE SCC_INIT
			INITIALISE_SCRIPT_CINEMATIC_CAM(sScriptCinemCam)
		BREAK	
		CASE SCC_UPDATE
			UPDATE_SCRIPT_CINEMATIC_CAM(sScriptCinemCam)
		BREAK
		CASE SCC_CLEANUP
			CLEANUP_SCRIPT_CINEMATIC_CAM(sScriptCinemCam)
		BREAK
	ENDSWITCH	

	#IF IS_DEBUG_BUILD
	MAINTAIN_CINEMATIC_CAM_DEBUG_WIDGET(sScriptCinemCam)
	#ENDIF
ENDPROC 











