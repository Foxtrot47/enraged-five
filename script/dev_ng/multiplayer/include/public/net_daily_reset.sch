USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "net_mission.sch"
USING "net_common_functions.sch"
USING "FM_Unlocks_Header.sch"
USING "net_realty_new.sch"

#IF FEATURE_TUNER
USING "net_car_club_rep.sch"
#ENDIF
USING "net_dancing_player.sch"

#IF FEATURE_DLC_2_2022
USING "net_street_dealers_launch.sch"
#ENDIF

/// PURPOSE:
///    Checks if the reset hour has hit since the player last logged in or while the player has been playing
///    Uses g_sMPTunables.idaily_obj_time_refresh to match the daily objective system for consistency
/// PARAMS:
///    theDateTime - The last time the player logged in
///    currentDateTime - The current time
///    iDiff - Difference between the 2, not used currently
FUNC BOOL SHOULD_DO_DAILY_RESET(UGC_DATE theDateTime, UGC_DATE currentDateTime, INT iDiff)
	UNUSED_PARAMETER(iDiff)
	IF currentDateTime.nYear = theDateTime.nYear
	AND currentDateTime.nMonth = theDateTime.nMonth
	AND currentDateTime.nDay = theDateTime.nDay
		IF currentDateTime.nHour >= g_sMPTunables.idaily_obj_time_refresh 
		AND theDateTime.nHour < g_sMPTunables.idaily_obj_time_refresh
			PRINTLN("[DAILY_RESET] SHOULD_DO_DAILY_RESET - TRUE - Last login was same date but before reset. Reset hour: ", g_sMPTunables.idaily_obj_time_refresh, ":00")
			RETURN TRUE
		ENDIF
	ELSE
		IF currentDateTime.nHour > g_sMPTunables.idaily_obj_time_refresh
			PRINTLN("[DAILY_RESET] SHOULD_DO_DAILY_RESET - TRUE - Last login was different date and we're after reset now. Reset hour: ", g_sMPTunables.idaily_obj_time_refresh, ":00")
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Pretty much just reset, wait 24 hours before next check
	IF currentDateTime.nHour = g_sMPTunables.idaily_obj_time_refresh
		MPGlobalsAmbience.sDailyReset.iResetCheckInterval = DAILY_RESET_DAY_IN_MS
		PRINTLN("[DAILY_RESET] SHOULD_DO_DAILY_RESET - Just hit reset, waiting a day before next check if player stays online that long.")
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Used to reset any systems when a reset is required (logging in after reset time or being online as it ticks over)
PROC PROCESS_DAILY_RESET()
	PRINTLN("[DAILY_RESET] PROCESS_DAILY_RESET - Reset required, processing daily resets. Current time: ", GET_CLOUD_TIME_AS_INT(), " - Daily seed: ", MPGlobalsAmbience.sDailyReset.iSeed)
	
	#IF FEATURE_TUNER
	// Resets delivered stats and updates vehicle list using daily seed
	POPULATE_DAILY_CHALKBOARD_VEHICLES(TRUE)
	// Reset car club rep daily bonus packed stats
	RESET_CAR_CLUB_REP_DAILY_BONUSES()
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	// Updates the daily stash house stats and selects a new one for the day
	SELECT_DAILY_STASH_HOUSE(TRUE)
	
	// Sets the global data for the 3 dealer locations today - synced for all players using seed
	POPULATE_DAILY_STREET_DEALERS()
	#ENDIF
	
	RESET_DANCE_REP_CAP_TIMER()
ENDPROC

/// PURPOSE:
///    Can be used to initialise daily systems when a reset is not required on login (logging off/on on the same day). Called once on start up (uses iSeed = -1 as done check).
PROC PROCESS_DAILY_RESET_LOGIN_INIT()
	PRINTLN("[DAILY_RESET] PROCESS_DAILY_RESET_LOGIN_INIT - No reset required, initialising daily systems. Current time: ", GET_CLOUD_TIME_AS_INT(), " - Daily seed: ", MPGlobalsAmbience.sDailyReset.iSeed)
	
	#IF FEATURE_TUNER
	// Populates list of vehicles using daily seed
	POPULATE_DAILY_CHALKBOARD_VEHICLES(FALSE)
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	// Sets the daily stash house global off of the stat for today
	SELECT_DAILY_STASH_HOUSE(FALSE)
	
	// Sets the global data for the 3 dealer locations today - synced for all players using seed
	POPULATE_DAILY_STREET_DEALERS()
	#ENDIF
ENDPROC

/// PURPOSE:
///    Adjusts the current time to match up to the reset hour; essentially our own little daily system time zone
FUNC INT GET_DAILY_RESET_TIME_ADJUSTMENT()
	RETURN (g_sMPTunables.idaily_obj_time_refresh * DAILY_RESET_SECONDS_IN_AN_HOUR)
ENDFUNC

/// PURPOSE:
///    Create seed that is unique to each day and synced for all players (the amount of days since 1st Jan 1970), adjusted to match our daily reset hour
PROC SET_DAILY_RESET_SEED()
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sDailyReset.iForcedSeed > 0
		MPGlobalsAmbience.sDailyReset.iSeed = MPGlobalsAmbience.sDailyReset.iForcedSeed
		PRINTLN("[DAILY_RESET] SET_DAILY_RESET_SEED - DEBUG - MPGlobalsAmbience.sDailyReset.iForcedSeed = ", MPGlobalsAmbience.sDailyReset.iForcedSeed)
		EXIT
	ENDIF
	#ENDIF
	
	MPGlobalsAmbience.sDailyReset.iSeed = (GET_MP_INT_CHARACTER_STAT(MP_STAT_CBV_RESET_TIME) - GET_DAILY_RESET_TIME_ADJUSTMENT()) / DAILY_RESET_DAY_IN_SEC
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sDailyReset.iDailyOffset != 0
		MPGlobalsAmbience.sDailyReset.iSeed += MPGlobalsAmbience.sDailyReset.iDailyOffset
		PRINTLN("[DAILY_RESET] SET_DAILY_RESET_SEED - DEBUG - MPGlobalsAmbience.sDailyReset.iSeed = ", MPGlobalsAmbience.sDailyReset.iSeed, " - MPGlobalsAmbience.sDailyReset.iDailyOffset = ", MPGlobalsAmbience.sDailyReset.iDailyOffset)
	ELSE
		PRINTLN("[DAILY_RESET] SET_DAILY_RESET_SEED - MPGlobalsAmbience.sDailyReset.iSeed = ", MPGlobalsAmbience.sDailyReset.iSeed)
	ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    A common daily reset system that checks the real world time to see if it's a new reset day
///    Uses g_sMPTunables.idaily_obj_time_refresh to match the daily objective system for consistency
///    Should not be modified for specific systems being added, put your reset/login behaviour into PROCESS_DAILY_RESET/PROCESS_DAILY_RESET_LOGIN_INIT
///    MP_STAT_CBV_RESET_TIME tracks the last POSIX time the player logged in
PROC MAINTAIN_DAILY_RESET()
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sDailyReset.bClearResetStat
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CBV_RESET_TIME, 0)
		MPGlobalsAmbience.sDailyReset.iResetCheckInterval = 0
		MPGlobalsAmbience.sDailyReset.bClearResetStat = FALSE
		PRINTLN("[DAILY_RESET] MAINTAIN_DAILY_RESET - MPGlobalsAmbience.sDailyReset.bClearResetStat = TRUE - Setting to 0.")
	ENDIF
	#ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(MPGlobalsAmbience.sDailyReset.stResetCheckTimer)
	OR HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.sDailyReset.stResetCheckTimer, MPGlobalsAmbience.sDailyReset.iResetCheckInterval)
		RESET_NET_TIMER(MPGlobalsAmbience.sDailyReset.stResetCheckTimer)
		START_NET_TIMER(MPGlobalsAmbience.sDailyReset.stResetCheckTimer)
		
		INT iCloudTime = GET_CLOUD_TIME_AS_INT()
		INT iLastResetTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_CBV_RESET_TIME)
		
		UGC_DATE currentTime, lastResetTime
		GET_UTC_TIME(currentTime.nYear,currentTime.nMonth,currentTime.nDay,currentTime.nHour,currentTime.nMinute,currentTime.nSecond)
		CONVERT_POSIX_TIME(iLastResetTime, lastResetTime)
		
		PRINTLN("[DAILY_RESET] MAINTAIN_DAILY_RESET - Last login: ", lastResetTime.nHour, ":", lastResetTime.nMinute, " ", lastResetTime.nDay, "/", lastResetTime.nMonth, "/", lastResetTime.nYear)
		PRINTLN("[DAILY_RESET] MAINTAIN_DAILY_RESET - Current time: ", currentTime.nHour, ":", currentTime.nMinute, " ", currentTime.nDay, "/", currentTime.nMonth, "/", currentTime.nYear)

		IF SHOULD_DO_DAILY_RESET(lastResetTime, currentTime, (iCloudTime - iLastResetTime))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CBV_RESET_TIME, iCloudTime)
			SET_DAILY_RESET_SEED()
			
			// Process the daily reset for each system, e.g. clearing stats
			PROCESS_DAILY_RESET()
		ELIF MPGlobalsAmbience.sDailyReset.iSeed = (-1)
			SET_DAILY_RESET_SEED()
			
			// Set up data for systems that use the daily reset system
			PROCESS_DAILY_RESET_LOGIN_INIT()
		ENDIF
	ENDIF
ENDPROC
