
USING "nightclub_shot_Activity_private.sch"
USING "net_transition_sessions.sch"
USING "freemode_header.sch"

FUNC BOOL IS_PLAYER_USING_GROUP_SHOT_ACTIVITY(PLAYER_INDEX piPlayer)
	RETURN IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(piPlayer)].SimpleInteriorBD.iBs4, BS4_SIMPLE_INTERIOR_GLOBAL_BS_GROUP_SHOT_ACT)
ENDFUNC

// PURPOSE:
//   Call when the player has finished with the activity / to cancel it.
//   Does nothing if we haven't finished loading/init. 
//	 IF bHideActivty = TRUE then the activity will not be able to be used anymore without calling
// 	 SHOT_REINIT_ACTIVITY(SHOT_ACTIVITY_DATA&)
PROC SHOT_CLEANUP_USING_ACTIVITY(SHOT_ACTIVITY_SERVER_DATA& ref_server, SHOT_ACTIVITY_DATA& ref_local, SHOT_ACTIVITY_BROADCAST_DATA& ref_broadcast[], BOOL bHideActivity = TRUE, SHOT_ACTIVITY_VARIANT eShotActivityVariant = FOUR_PLAYER_SHOT_ACTIVITY)
	CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT] END_GROUP_SHOT_DRINKING_ACTIVITY")	
	
	// Reset broadcast data
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NETWORK_IS_SESSION_ACTIVE()
		INT iParticipantID = PARTICIPANT_ID_TO_INT()
		SHOT_ACTIVITY_BROADCAST_DATA newBroadcastData
		ref_broadcast[iParticipantID] = newBroadcastData
		CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT]  local player (participant id = ",iParticipantID,") broadcast data has been reset.")
	ELSE
		CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT]  participant id is -1, cannot reset broadcast data!")
	ENDIF
	
	CLEAR_BIT(ref_local.iBs, ciSHOT_BS_DOWNING_DRINK)
	
	IF IS_BIT_SET(ref_local.iBs, ciSHOT_BS_DISABLE_HEAD_IK)
		SET_PED_CAN_HEAD_IK(PLAYER_PED_ID(), TRUE)
		
		CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT] SET_PED_CAN_HEAD_IK - TRUE 2")
		
		CLEAR_BIT(ref_local.iBs, ciSHOT_BS_DISABLE_HEAD_IK)
	ENDIF
	
	IF IS_BIT_SET(ref_local.iBs, ciSHOT_BS_DISABLE_PIM)
		ENABLE_INTERACTION_MENU()
		
		CLEAR_BIT(ref_local.iBs, ciSHOT_BS_DISABLE_PIM)
	ENDIF
	
	IF IS_BIT_SET(ref_local.iBs, ciSHOT_BS_SET_KINEMATIC)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, FALSE)
		
		CLEAR_BIT(ref_local.iBs, ciSHOT_BS_SET_KINEMATIC)
	ENDIF
	
	RESET_FACE_PROPS(ref_local)
	
	// Switch vairables
	INT iSceneId
	SWITCH ref_local.eState
		CASE SHS_INIT 	
		CASE SHS_LOADING 				
		CASE SHS_IDLE 	
			CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT]  Not in activity state (", SHOT_GET_STATE_STRING(ref_local.eState), ") - no task cleanup")	
		BREAK
		CASE SHS_CLEANUP_COMPLETE
			CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT]  Not in activity state (", SHOT_GET_STATE_STRING(ref_local.eState), ") - exit")	
		EXIT
		CASE SHS_REQUEST_POUR
		CASE SHS_ANIM_POUR	
		CASE SHS_ANIM_START_HOLD
		CASE SHS_ANIM_DRINK
		CASE SHS_ANIM_INTRO
		CASE SHS_ANIM_IDLE
			iSceneId = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ref_local.iNetSyncedSceneId)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
				CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT] Cleanup sync scene (", SHOT_GET_STATE_STRING(ref_local.eState), ")")	
				NETWORK_STOP_SYNCHRONISED_SCENE(ref_local.iNetSyncedSceneId)
			ENDIF
			
			INT iGlass
			REPEAT SHP_COUNT iGlass
				IF IS_BIT_SET(ref_server.iBsPropCreated, iGlass)
					CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT] Attempting to clean up glass: ", iGlass)
					SHOT_SERVER_RESET_GLASS_FOR_PARTICIPANT(ref_server, INT_TO_ENUM(SHOT_PARTICIPANT, iGlass))
				ENDIF
			ENDREPEAT
			
			SHOT_SERVER_RESET_BOTTLE(ref_server)
		BREAK
	ENDSWITCH
	
	SHOT_RELEASE_CONTEXT_INTENTION(ref_local)
	
	INT i
	REPEAT SHD_COUNT i	
		STRING sDict = SHOT_GET_DICT_NAME(INT_TO_ENUM(SHOT_DICT, i), ref_server, eShotActivityVariant)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sDict)
			CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT] REMOVING ", sDict)	
			REMOVE_ANIM_DICT(sDict)
		ENDIF
	ENDREPEAT
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ref_local.ptfxID)
		STOP_PARTICLE_FX_LOOPED(ref_local.ptfxID)
	ENDIF
	
	IF eShotActivityVariant = TWO_PLAYER_SHOT_ACTIVITY
		REMOVE_NAMED_PTFX_ASSET("scr_sec")
	ELSE
		REMOVE_NAMED_PTFX_ASSET("scr_ba_club")
	ENDIF
	
	SHOT_ACTIVITY_DATA newActivityData
	ref_local = newActivityData
	CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT]  local player data has been reset.")
	
	IF bHideActivity
		SHOT_SET_STATE(ref_local, SHS_CLEANUP_COMPLETE)	
	ELSE
		SHOT_SET_STATE(ref_local, SHS_IDLE)	
	ENDIF
		
	
	// Update globals to reflect new state.
	SHOT_MAINTAIN_GLOBALS(ref_local)
ENDPROC

PROC MAINTAIN_GROUP_SHOT_DRINKING_ACTIVITY(SHOT_ACTIVITY_DATA& ref_local, SHOT_ACTIVITY_BROADCAST_DATA& ref_broadcast[], SHOT_ACTIVITY_SERVER_DATA& ref_server, INT iCurrentRoomKey, SHOT_ACTIVITY_VARIANT eShotActivityVariant = FOUR_PLAYER_SHOT_ACTIVITY)
	// Tunable disabled?
	IF g_sMPTunables.bBB_NIGHTCLUB_DISABLE_NIGHTCLUB_SHOT_MINIGAME
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	SHOT_MAINTAIN_WIDGETS(ref_local, ref_server)
	#ENDIF
	
	// Disable recording once player starts to drink/pour alcohol
	IF ref_local.eState > SHS_ANIM_INTRO
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	ENDIF
	
	// Per-frame calls to disable stuff in using minigame states
	SWITCH ref_local.eState
		CASE SHS_MOVE_TO_INTRO 			
		CASE SHS_ANIM_INTRO 
		CASE SHS_ANIM_IDLE 
		CASE SHS_REQUEST_POUR
		CASE SHS_ANIM_POUR
		CASE SHS_ANIM_START_HOLD
		CASE SHS_ANIM_DRINK
		CASE SHS_ANIM_OUTRO
		CASE SHS_ANIM_OUTRO_FALLOVER
		CASE SHS_ANIM_OUTRO_FROM_HOLD
			INVALIDATE_IDLE_CAM()
			HUD_FORCE_WEAPON_WHEEL(FALSE)
			DISPLAY_AMMO_THIS_FRAME(FALSE)			
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
			DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
			DISABLE_SELECTOR_THIS_FRAME()
			
			
			WEAPON_TYPE wtWeapon
			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeapon)
			AND wtWeapon != WEAPONTYPE_UNARMED
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			
			IF ref_local.eState <> SHS_ANIM_OUTRO
			AND ref_local.eState <> SHS_ANIM_OUTRO_FROM_HOLD
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)	
			ENDIF
			// Control which anim dict we should be using (drunk, sober "normal")
			SHOT_MAINTAIN_SWITCHING_DICT(ref_local, ref_server, eShotActivityVariant)
			
			SET_PED_CAN_HEAD_IK(PLAYER_PED_ID(), FALSE)
			
			SET_BIT(ref_local.iBs, ciSHOT_BS_DISABLE_HEAD_IK)
			
			IF NOT IS_BIT_SET(ref_local.iBs, ciSHOT_BS_DISABLE_PIM)
			OR NOT IS_INTERACTION_MENU_DISABLED()
				DISABLE_INTERACTION_MENU()
				
				SET_BIT(ref_local.iBs, ciSHOT_BS_DISABLE_PIM)
			ENDIF
			
			IF NOT IS_BIT_SET(ref_local.iBs, ciSHOT_BS_SET_KINEMATIC)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
				
				SET_BIT(ref_local.iBs, ciSHOT_BS_SET_KINEMATIC)
			ENDIF
		BREAK
		
		DEFAULT
			IF IS_BIT_SET(ref_local.iBs, ciSHOT_BS_DISABLE_HEAD_IK)
				SET_PED_CAN_HEAD_IK(PLAYER_PED_ID(), TRUE)
				
				CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT] SET_PED_CAN_HEAD_IK - TRUE 1")
				
				CLEAR_BIT(ref_local.iBs, ciSHOT_BS_DISABLE_HEAD_IK)
			ENDIF
			
			IF IS_BIT_SET(ref_local.iBs, ciSHOT_BS_DISABLE_PIM)
				ENABLE_INTERACTION_MENU()
				
				CLEAR_BIT(ref_local.iBs, ciSHOT_BS_DISABLE_PIM)
			ENDIF
			
			IF IS_BIT_SET(ref_local.iBs, ciSHOT_BS_SET_KINEMATIC)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, FALSE)
				
				CLEAR_BIT(ref_local.iBs, ciSHOT_BS_SET_KINEMATIC)
			ENDIF
		BREAK
	ENDSWITCH
	
	// Maintain requesting control of our glass
	SWITCH ref_local.eState
		CASE SHS_ANIM_START_HOLD
		CASE SHS_ANIM_DRINK
			SHOT_REQUEST_PARTICIPANT_GLASS_CONTROL(ref_local.eParticipant, ref_server)
		BREAK
	ENDSWITCH
	
	IF IS_CASINO_APARTMENT_GROUP_SHOT_ACTIVITY()
	AND IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
	AND IS_SCREEN_FADED_OUT()
		// Kick out of activity into idle state
		IF ref_local.eState > SHS_REGISTERED_INTEREST
		AND reF_local.eState < SHS_CLEANUP_COMPLETE 
			SHOT_CLEANUP_USING_ACTIVITY(ref_server, ref_local, ref_broadcast, FALSE, eShotActivityVariant)
			EXIT
		ENDIF
	ENDIF
	
	IF IS_CASINO_APARTMENT_GROUP_SHOT_ACTIVITY()
	AND ref_server.bSecondActivity
	AND Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) >= 10
	AND ref_local.eState = SHS_ANIM_IDLE
		SHOT_CLEANUP_USING_ACTIVITY(ref_server, ref_local, ref_broadcast, FALSE, eShotActivityVariant)
		EXIT
	ENDIF
	
	#IF FEATURE_MUSIC_STUDIO
	IF IS_MUSIC_STUDIO_GROUP_SHOT_ACTIVITY()
	AND Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) >= 10
	AND ref_local.eState = SHS_ANIM_IDLE
		SHOT_CLEANUP_USING_ACTIVITY(ref_server, ref_local, ref_broadcast, FALSE, eShotActivityVariant)
		EXIT
	ENDIF
	#ENDIF
	
	IF (NOT IS_ARENA_GROUP_SHOT_ACTIVITY()
	AND NOT IS_CASINO_APARTMENT_GROUP_SHOT_ACTIVITY())
	OR ref_server.bSecondActivity
	#IF FEATURE_FIXER
	OR IS_FIXER_HQ_GROUP_SHOT_ACTIVITY()
	#ENDIF
	#IF FEATURE_MUSIC_STUDIO
	OR IS_MUSIC_STUDIO_GROUP_SHOT_ACTIVITY()
	#ENDIF
		// Kick out of activity into idle state if we are not in the locate
		IF ref_local.eState > SHS_REGISTERED_INTEREST
		AND reF_local.eState < SHS_CLEANUP_COMPLETE 
		AND NOT SHOT_IS_ENTITY_IN_PARTICIPANT_AREA(ref_server, PLAYER_PED_ID(), ref_local.eParticipant, iCurrentRoomKey)
		AND ref_local.eState != SHS_ANIM_OUTRO_FALLOVER
			SHOT_CLEANUP_USING_ACTIVITY(ref_server, ref_local, ref_broadcast, FALSE, eShotActivityVariant)
			EXIT
		ENDIF
	ELSE
		IF IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Back_To_Playing)
		AND IS_SCREEN_FADED_OUT()
		AND ref_local.eState > SHS_REGISTERED_INTEREST
		AND reF_local.eState < SHS_CLEANUP_COMPLETE
			SHOT_CLEANUP_USING_ACTIVITY(ref_server, ref_local, ref_broadcast, FALSE, eShotActivityVariant)
			EXIT
		ENDIF
	ENDIF
	
	SWITCH ref_local.eState
		CASE SHS_INIT 					SHOT_STATE_INIT(ref_local, ref_broadcast)														BREAK
		CASE SHS_LOADING 				SHOT_STATE_LOADING(ref_local, ref_server)														BREAK
		CASE SHS_IDLE 					SHOT_STATE_IDLE(ref_local, ref_broadcast, ref_server, iCurrentRoomKey, eShotActivityVariant)	BREAK
		CASE SHS_REGISTERED_INTEREST 	SHOT_STATE_REGISTERED_INTEREST(ref_local, ref_broadcast, ref_server, eShotActivityVariant) 		BREAK
		CASE SHS_MOVE_TO_INTRO 			SHOT_STATE_MOVE_TO_INTRO(ref_local, ref_server, eShotActivityVariant) 							BREAK
		CASE SHS_ANIM_INTRO 			SHOT_STATE_ANIM_INTRO(ref_local, ref_server, eShotActivityVariant) 								BREAK
		CASE SHS_ANIM_IDLE 				SHOT_STATE_ANIM_IDLE(ref_local, ref_broadcast, ref_server, eShotActivityVariant) 				BREAK
		CASE SHS_REQUEST_POUR 			SHOT_STATE_REQUEST_POUR(ref_local, ref_broadcast, ref_server, eShotActivityVariant) 			BREAK
		CASE SHS_ANIM_POUR				SHOT_STATE_ANIM_POUR(ref_server, ref_local, ref_broadcast, eShotActivityVariant)				BREAK
		CASE SHS_ANIM_START_HOLD		SHOT_STATE_ANIM_START_HOLD(ref_local, ref_broadcast, ref_server, eShotActivityVariant) 			BREAK
		CASE SHS_ANIM_DRINK				SHOT_STATE_ANIM_DRINK(ref_local, ref_broadcast, ref_server, eShotActivityVariant)				BREAK
		CASE SHS_ANIM_OUTRO				SHOT_STATE_ANIM_OUTRO(ref_local, ref_broadcast, ref_server)										BREAK
		CASE SHS_ANIM_OUTRO_FROM_HOLD 	SHOT_STATE_ANIM_OUTRO_FROM_HOLD(ref_local, ref_server, eShotActivityVariant) 					BREAK
		CASE SHS_ANIM_OUTRO_FALLOVER	SHOT_STATE_ANIM_OUTRO_FALLOVER(ref_local, ref_broadcast)										BREAK
	ENDSWITCH
	
	SHOT_MAINTAIN_GLOBALS(ref_local)
ENDPROC

PROC SHOT_REINIT_ACITIVTY(SHOT_ACTIVITY_SERVER_DATA& ref_server, SHOT_ACTIVITY_DATA& ref_local, SHOT_ACTIVITY_BROADCAST_DATA& ref_broadcast[], SHOT_ACTIVITY_VARIANT eShotActivityVariant = FOUR_PLAYER_SHOT_ACTIVITY)
	CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT] SHOT_REINIT_ACITIVTY")	
	SHOT_CLEANUP_USING_ACTIVITY(ref_server, ref_local, ref_broadcast, DEFAULT, eShotActivityVariant)
	SHOT_SET_STATE(ref_local, SHS_INIT)
ENDPROC

PROC CLEANUP_GROUP_SHOT_DRINKING_ACTIVITY_SERVER(SHOT_ACTIVITY_SERVER_DATA& ref_server)
	CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT][SERVER] SHOT_SERVER_STATE_CLEANUP - start")
	
	IF ref_server.eState = SHSS_CLEANUP_COMPLETE
	OR ref_server.eState = SHSS_INIT
		CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT][SERVER] SHOT_SERVER_STATE_CLEANUP - Don't need to cleanuo (",SHOT_SERVER_GET_STATE_STRING(ref_server.eState),"), exiting")
		EXIT
	ENDIF
	
	// Net objects cleaned up when last player leaves this script. Do not clear
	// them up manually because we need them to migrate to another host when we leave!
		
	
	SHOT_SERVER_SET_STATE(ref_server, SHSS_CLEANUP_COMPLETE)
	CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT][SERVER] SHOT_SERVER_STATE_CLEANUP - complete")
ENDPROC

PROC SHOT_REINIT_ACTIVITY_SERVER(SHOT_ACTIVITY_SERVER_DATA& ref_server)
	CDEBUG1LN(DEBUG_NET_DANCING, "[GROUP_SHOT][SERVER] SHOT_REINIT_ACTIVITY_SERVER")	
	CLEANUP_GROUP_SHOT_DRINKING_ACTIVITY_SERVER(ref_server)
	SHOT_SERVER_SET_STATE(ref_server, SHSS_INIT)
ENDPROC

PROC MAINTAIN_GROUP_SHOT_DRINKING_ACTIVITY_SERVER(SHOT_ACTIVITY_DATA& ref_local, SHOT_ACTIVITY_BROADCAST_DATA& ref_broadcast[], SHOT_ACTIVITY_SERVER_DATA& ref_server, SHOT_ACTIVITY_VARIANT eShotActivityVariant = FOUR_PLAYER_SHOT_ACTIVITY)
	// Tunable disabled?
	IF g_sMPTunables.bBB_NIGHTCLUB_DISABLE_NIGHTCLUB_SHOT_MINIGAME
		EXIT
	ENDIF
	
	SWITCH ref_server.eState
		CASE SHSS_INIT 		SHOT_SERVER_STATE_INIT(ref_server)													BREAK
		CASE SHSS_LOADING 	SHOT_SERVER_STATE_LOADING(ref_server, eShotActivityVariant)							BREAK
		CASE SHSS_IDLE 		SHOT_SERVER_STATE_IDLE(ref_local, ref_broadcast, ref_server, eShotActivityVariant)	BREAK
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
PROC SHOT_CREATE_WIDGETS(SHOT_ACTIVITY_DATA& ref_local)
	INT i
	START_WIDGET_GROUP("Group shot drinking")
		START_NEW_WIDGET_COMBO()
		REPEAT SHS_COUNT i	
			ADD_TO_WIDGET_COMBO(SHOT_GET_STATE_STRING(INT_TO_ENUM(SHOT_STATE, i)))
		ENDREPEAT
		STOP_WIDGET_COMBO("State", ref_local.debug.iState)
		
		START_NEW_WIDGET_COMBO()
		REPEAT SHP_COUNT i	
			ADD_TO_WIDGET_COMBO(SHOT_GET_PARTICIPANT_STRING(INT_TO_ENUM(SHOT_PARTICIPANT, i)))
		ENDREPEAT
		STOP_WIDGET_COMBO("Participant", ref_local.debug.iParticipant)
		
		START_NEW_WIDGET_COMBO()
		ADD_TO_WIDGET_COMBO("None") // Cover "SHP_INVALID" : Offset iPourer with +1.
		REPEAT SHP_COUNT i	
			ADD_TO_WIDGET_COMBO(SHOT_GET_PARTICIPANT_STRING(INT_TO_ENUM(SHOT_PARTICIPANT, i)))
		ENDREPEAT
		STOP_WIDGET_COMBO("Pourer", ref_local.debug.iPourer)
		
		ADD_WIDGET_BOOL("prevent-alc-hit-on-drink", ref_local.debug.bNoDrunk)
		ADD_WIDGET_BOOL("get-drunk-quickly", ref_local.debug.bFastDrunk)
		ADD_WIDGET_FLOAT_SLIDER("Pour-to-pickup scene threshold", cfSHOT_POUR_TO_PICKUP_THRESHOLD, 0.0, 1.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("drink-to-outro scene threshold", cfSHOT_OUTRO_FROM_HOLD_THRESHOLD, 0.0, 1.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Alc-hit-phase", cfSHOT_DRINK_PHASE_ALC_HIT, 0.0, 1.0, 0.1)
		
		ADD_WIDGET_FLOAT_SLIDER("Task anim blend in", cfSHOT_ANIM_BLEND_IN, 0, INSTANT_BLEND_IN, 1)
		ADD_WIDGET_FLOAT_SLIDER("Task anim blend out", cfSHOT_ANIM_BLEND_OUT, INSTANT_BLEND_OUT, 0, 1)
		ADD_WIDGET_FLOAT_SLIDER("Pour scene blend in", cfSHOT_POUR_BLEND_IN, 0, INSTANT_BLEND_IN, 1)
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF //IS_DEBUG_BUILD
