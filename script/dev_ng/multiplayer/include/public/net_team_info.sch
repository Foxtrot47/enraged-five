USING "globals.sch"
USING "model_enums.sch"
USING "net_stat_system.sch"

PROC INITIALISE_TEAMS_DATA()
	
	// spectators
	//TeamData_SCTV.blipHighlighted = RADAR_TRACE_GANG_HIGHLIGHT
	TeamData_SCTV.bPlayerCanCollectDroppedMoney = FALSE
	TeamData_SCTV.bHasGotGarages = FALSE
	TeamData_SCTV.TeamType = TEAM_TYPE_NON_ACTIVE
	TeamData_SCTV.charBoss = CHAR_MP_PROF_BOSS
	TeamData_SCTV.iVehicleColour = 0
	TeamData_SCTV.iSpeakerID = 6
	TeamData_SCTV.ModelMale = MP_M_Freemode_01
	TeamData_SCTV.ModelFemale = MP_F_Freemode_01
	TeamData_SCTV.ModelVehicle = SENTINEL2
	TeamData_SCTV.ModelLieutenent = U_M_M_FilmDirector
	TeamData_SCTV.str_Plural_AppendThe_StartOfSentence = "CNC_TN_S_01"
	TeamData_SCTV.str_Plural_AppendThe_StartOfSentence_Capitalised = "CNC_TN_S_01_CAP"
	TeamData_SCTV.str_Plural_AppendThe = "CNC_TN_S_02"
	TeamData_SCTV.str_Plural_AppendThe_Capitalised = "CNC_TN_S_02_CAP"
	TeamData_SCTV.str_Plural_HudColour = "CNC_TN_S_05"
	TeamData_SCTV.str_Plural_HudColour_Capitalised	= "CNC_TN_S_05_CAP"
	TeamData_SCTV.str_Plural = "CNC_TN_S_03"
	TeamData_SCTV.str_Plural_Capitalised = "CNC_TN_S_03_CAP"
	TeamData_SCTV.str_Singular_HudColour = "CNC_TN_S_06"
	TeamData_SCTV.str_Singular_HudColour_Capitalised = "CNC_TN_S_06_CAP"
	TeamData_SCTV.str_Singular = "CNC_TN_S_04"
	TeamData_SCTV.str_Singular_Capitalised = "CNC_TN_S_04_CAP"		
	TeamData_SCTV.str_TeamSpeakerName = ""
	TeamData_SCTV.str_OverheadImageSprite = "img_radar_gang_families"
	TeamData_SCTV.str_SafehouseBlipName = ""
	TeamData_SCTV.str_TimeCycleModifierName = "nothing"
	#IF IS_DEBUG_BUILD
		TeamData_SCTV.str_TeamNameForDebug = "Spectators"
	#ENDIF

	
	// Freemode Team
	//TeamData_Freemoder.blipHighlighted = RADAR_TRACE_GANG_HIGHLIGHT
	TeamData_Freemoder.bPlayerCanCollectDroppedMoney = TRUE
	TeamData_Freemoder.bHasGotGarages = FALSE
	TeamData_Freemoder.TeamType = TEAM_TYPE_NON_ACTIVE
	TeamData_Freemoder.charBoss = CHAR_MP_PROF_BOSS
	TeamData_Freemoder.iVehicleColour = 0
	TeamData_Freemoder.iSpeakerID = 6
	TeamData_Freemoder.ModelMale = MP_M_Freemode_01 
	TeamData_Freemoder.ModelFemale = MP_F_Freemode_01 
	TeamData_Freemoder.ModelVehicle = SENTINEL2
	TeamData_Freemoder.ModelLieutenent = U_M_M_FilmDirector
	TeamData_Freemoder.str_Plural_AppendThe_StartOfSentence = "CNC_TN_F_01"
	TeamData_Freemoder.str_Plural_AppendThe_StartOfSentence_Capitalised = "CNC_TN_F_01_CAP"
	TeamData_Freemoder.str_Plural_AppendThe = "CNC_TN_F_02"
	TeamData_Freemoder.str_Plural_AppendThe_Capitalised = "CNC_TN_F_02_CAP"
	TeamData_Freemoder.str_Plural_HudColour = "CNC_TN_F_05"
	TeamData_Freemoder.str_Plural_HudColour_Capitalised	= "CNC_TN_F_05_CAP"
	TeamData_Freemoder.str_Plural = "CNC_TN_F_03"
	TeamData_Freemoder.str_Plural_Capitalised = "CNC_TN_F_03_CAP"
	TeamData_Freemoder.str_Singular_HudColour = "CNC_TN_F_06"
	TeamData_Freemoder.str_Singular_HudColour_Capitalised = "CNC_TN_F_06_CAP"
	TeamData_Freemoder.str_Singular = "CNC_TN_F_04"
	TeamData_Freemoder.str_Singular_Capitalised = "CNC_TN_F_04_CAP"		
	TeamData_Freemoder.str_TeamSpeakerName = ""
	TeamData_Freemoder.str_OverheadImageSprite = "img_radar_gang_families"
	TeamData_Freemoder.str_SafehouseBlipName = ""
	TeamData_Freemoder.str_TimeCycleModifierName = "nothing"
	#IF IS_DEBUG_BUILD
		TeamData_Freemoder.str_TeamNameForDebug = "TeamFreemode"
	#ENDIF
		
ENDPROC

FUNC INT GET_MAX_NUM_PLAYERS_FOR_TEAM(INT inTeam, MP_GAMEMODE GamemodeOverride = GAMEMODE_EMPTY)

	MP_GAMEMODE aGamemode = GET_CURRENT_GAMEMODE() 
	IF GamemodeOverride != GAMEMODE_EMPTY
		aGamemode = GamemodeOverride
	ENDIF

	SWITCH aGamemode

		CASE GAMEMODE_FM
			SWITCH NUM_NETWORK_PLAYERS
	
				CASE 16
					SWITCH inTeam
						CASE TEAM_SCTV
							RETURN(0)	
						BREAK
						CASE TEAM_FREEMODE
						DEFAULT
							RETURN(16)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 18
					SWITCH inTeam
						CASE TEAM_SCTV
							RETURN(2)	
						BREAK
						CASE TEAM_FREEMODE
						DEFAULT
							RETURN(16)
						BREAK
					ENDSWITCH
				BREAK				
				
				CASE 24
					SWITCH inTeam
						CASE TEAM_SCTV
							RETURN(2)
						BREAK
						CASE TEAM_FREEMODE
						DEFAULT
							RETURN(22)
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 26
					SWITCH inTeam
						CASE TEAM_SCTV
							RETURN(2)
						BREAK
						CASE TEAM_FREEMODE
						DEFAULT
							RETURN(24)
						BREAK
					ENDSWITCH
				BREAK
			
				CASE 32
					SWITCH inTeam
						CASE TEAM_SCTV
							RETURN(2)
						BREAK
						CASE TEAM_FREEMODE
						DEFAULT
							RETURN(30)
						BREAK
					ENDSWITCH
				BREAK	
			
			ENDSWITCH
		BREAK
	
	ENDSWITCH

	
	
	RETURN(4)


ENDFUNC

FUNC MODEL_NAMES GET_FREEMODE_PLAYER_MODEL_FROM_PICTURE_INDEX(INT index)
	SWITCH index
		CASE 0
			RETURN TeamData_Freemoder.ModelMale
		
		BREAK
		CASE 1
			RETURN TeamData_Freemoder.ModelFemale 
		
		BREAK
		CASE 2
			RETURN U_M_M_FilmDirector
		BREAK
	
	ENDSWITCH
	RETURN U_M_M_FilmDirector
ENDFUNC

FUNC MODEL_NAMES GET_DIRECTOR_PLAYER_MODEL()
	RETURN U_M_M_FilmDirector

ENDFUNC

/// PURPOSE:
///    Gets the temp player model for each team.
/// PARAMS:
///    team -A CnC team. iModelVariation - the model variation for the gang (currently just 1 or 2)
/// RETURNS:
///    Gets the player model for the team.
FUNC MODEL_NAMES GET_PLAYER_MODEL_FOR_TEAM(INT iTeam)// , INT iModelVariation = 1)
	
//	IF iTeam = TEAM_FREEMODE
//		RETURN GET_FREEMODE_PLAYER_MODEL_FROM_PICTURE_INDEX(GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE))
//	ENDIF
	
	SWITCH iTeam
		CASE TEAM_FREEMODE
			IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0
				RETURN(TeamData_Freemoder.ModelMale)
			ELSE
				RETURN(TeamData_Freemoder.ModelFemale)
			ENDIF
		BREAK
		CASE TEAM_SCTV
			IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0
				RETURN(TeamData_SCTV.ModelMale)
			ELSE
				RETURN(TeamData_SCTV.ModelFemale)
			ENDIF
		BREAK
//		DEFAULT
//			IF (iTeam > -1)
//			AND (iTeam < MAX_NUM_TEAMS)
//				IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0
//					RETURN(TeamData[iTeam].ModelMale)
//				ELSE
//					RETURN(TeamData[iTeam].ModelFemale)
//				ENDIF
//			ENDIF
//		BREAK
	ENDSWITCH

	// return the players current model
	RETURN(GET_ENTITY_MODEL (PLAYER_PED_ID()))
	
ENDFUNC

FUNC MODEL_NAMES GET_PLAYER_MODEL_FOR_TEAM_VARIATION( INT iTeam, INT iModelVariation = 1)

//	IF iTeam = TEAM_FREEMODE
//		RETURN GET_FREEMODE_PLAYER_MODEL_FROM_PICTURE_INDEX(iModelVariation)
//	ENDIF
	
	SWITCH iTeam
		CASE TEAM_FREEMODE
			IF iModelVariation = 0
				RETURN(TeamData_Freemoder.ModelMale)
			ELSE
				RETURN(TeamData_Freemoder.ModelFemale)
			ENDIF
		BREAK
		CASE TEAM_SCTV
			IF iModelVariation = 0
				RETURN(TeamData_SCTV.ModelMale)
			ELSE
				RETURN(TeamData_SCTV.ModelFemale)
			ENDIF
		BREAK	
//		DEFAULT
//			IF (iTeam > -1)
//			AND (iTeam < MAX_NUM_TEAMS)	
//				IF iModelVariation = 0
//					RETURN(TeamData[iTeam].ModelMale)
//				ELSE
//					RETURN(TeamData[iTeam].ModelFemale)
//				ENDIF
//			ENDIF
//		BREAK
	ENDSWITCH

	// return the players current model
	RETURN(GET_ENTITY_MODEL (PLAYER_PED_ID()))
	
ENDFUNC


FUNC MODEL_NAMES GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU( INT iTeam, INT iModelVariation = 1)
		

	
	SWITCH iTeam
		CASE TEAM_FREEMODE
		CASE TEAM_SCTV
			IF iModelVariation = 0
				RETURN(TeamData_Freemoder.ModelMale)
			ELSE
				RETURN(TeamData_Freemoder.ModelFemale)
			ENDIF
		BREAK
//		DEFAULT
//			IF (iTeam > -1)
//			AND (iTeam < MAX_NUM_TEAMS)		
//				IF iModelVariation = 0
//					RETURN(TeamData[iTeam].ModelMale)
//				ELSE
//					RETURN(TeamData[iTeam].ModelFemale)
//				ENDIF
//			ENDIF
//		BREAK
	ENDSWITCH	
	
	DEBUG_PRINTCALLSTACK()
	NET_NL()NET_PRINT("GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU Fell through. ")
	NET_NL()NET_PRINT("GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU Fell iTeam = ")NET_PRINT_INT(iTeam)
	// return the players current model
	RETURN(GET_ENTITY_MODEL (PLAYER_PED_ID()))
	
ENDFUNC

FUNC INT GET_PLAYER_TEAM_FROM_MODEL( MODEL_NAMES WhichModel)
		
		
	//Leave SCTV using this one. It's the same model. 
	IF (TeamData_Freemoder.ModelMale = WhichModel)
	OR (TeamData_Freemoder.ModelFemale = WhichModel)
		RETURN(TEAM_FREEMODE)
	ENDIF	
	
	
	RETURN(GET_PLAYER_TEAM(PLAYER_ID()))
	
ENDFUNC

FUNC BOOL IS_PED_IN_TEAM(PED_INDEX aPed, INT iTeam)
	MODEL_NAMES PedModel =  GET_ENTITY_MODEL(aPed)
	IF (GET_PLAYER_MODEL_FOR_TEAM_VARIATION(iTeam, 0) = PedModel)
	OR (GET_PLAYER_MODEL_FOR_TEAM_VARIATION(iTeam, 1) = PedModel)
		RETURN(TRUE)
	ENDIF	
	RETURN(FALSE)
ENDFUNC

FUNC STRING CONVERT_TEXT_LABEL_TO_STRING(STRING inTextLabel)
	RETURN(inTextLabel)
ENDFUNC

FUNC STRING CONVERT_INT_TO_STRING(INT iInt)
	TEXT_LABEL sNumber
	sNumber = ""
	sNumber += iInt
	RETURN(CONVERT_TEXT_LABEL_TO_STRING(sNumber))
ENDFUNC

/// PURPOSE:
///    Gets a team/gang literal string name.
/// PARAMS:
///    team - A CnC team/gang.
///    bPlural - do you want the name in plural form (ie "cop" or "cops").
///    bAppendThe - do you want the string to begin with "the" - "cops" or "the cops".
///    bStartOfSentence - is the name going to be at the start of a sentence - "the cops" or "The cops".
///    bTeamHudColour - Print the text with the correct HUD colour (ie "Vagos" in orange).
/// RETURNS:
///    The literal string name of a team
FUNC STRING GET_TEAM_NAME_KEY(INT iTeam, BOOL bPlural = FALSE, BOOL bAppendThe = FALSE, BOOL bStartOfSentence = FALSE, BOOL bTeamHudColour = FALSE, BOOL bIsCaptialised = FALSE)

	IF (iTeam = TEAM_INVALID)
		RETURN "CNC_TN_NONE"
	ENDIF

	TEAM_DATA ThisTeamData
	
	SWITCH iTeam
		CASE TEAM_FREEMODE
			ThisTeamData = TeamData_Freemoder
		BREAK
		CASE TEAM_SCTV
			ThisTeamData = TeamData_SCTV
		BREAK	
//		DEFAULT
//			IF (iTeam > -1)
//			AND (iTeam < MAX_NUM_TEAMS)
//				ThisTeamData = TeamData[iTeam]
//			ELSE
//				IF NETWORK_IS_IN_SESSION()
//					NET_SCRIPT_ASSERT("GET_TEAM_NAME_KEY couldn't find that team name")
//				ELSE
//					SCRIPT_ASSERT("GET_TEAM_NAME_KEY couldn't find that team name")
//				ENDIF			
//			ENDIF
//		BREAK
	ENDSWITCH		
	

	IF bPlural
		IF bAppendThe
			IF bStartOfSentence
				IF bIsCaptialised = FALSE
					RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Plural_AppendThe_StartOfSentence)
				ELSE
					RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Plural_AppendThe_StartOfSentence_Capitalised)
				ENDIF
			ELSE
				IF bIsCaptialised = FALSE 
					RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Plural_AppendThe)
				ELSE
					RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Plural_AppendThe_Capitalised)
				ENDIF
			ENDIF
		ELSE
			IF bTeamHudColour
				IF bIsCaptialised = FALSE 
					RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Plural_HudColour)
				ELSE
					RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Plural_HudColour_Capitalised)
				ENDIF
			ELSE
				IF bIsCaptialised = FALSE 
					RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Plural)
				ELSE
					RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Plural_Capitalised)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF bTeamHudColour
			IF bIsCaptialised = FALSE 
				RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Singular_HudColour)
			ELSE
				RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Singular_HudColour_Capitalised)
			ENDIF
		ELSE
			IF bIsCaptialised = FALSE 
				RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Singular)
			ELSE
				RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Singular_Capitalised)
			ENDIF
		ENDIF
	ENDIF

		

	IF NETWORK_IS_IN_SESSION()
		NET_SCRIPT_ASSERT("GET_TEAM_NAME_KEY couldn't find that team name")
	ELSE
		SCRIPT_ASSERT("GET_TEAM_NAME_KEY couldn't find that team name")
	ENDIF

		
	RETURN CONVERT_TEXT_LABEL_TO_STRING(ThisTeamData.str_Singular)
ENDFUNC

FUNC STRING GET_TEAM_NAME(INT iTeam, BOOL bPlural = FALSE, BOOL bAppendThe = FALSE, BOOL bStartOfSentence = FALSE, BOOL bTeamHudColour = FALSE)
	RETURN GET_TEAM_NAME_KEY(iTeam, bPlural, bAppendThe, bStartOfSentence, bTeamHudColour)
ENDFUNC


/// PURPOSE:
///    Gets the rel group for a team.
/// PARAMS:
///    team - Valid cnc team.
/// RETURNS:
///    The rel group that should be used for this team.
FUNC REL_GROUP_HASH GET_TEAM_REL_GROUP(INT iTeam)
	
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN TeamData_Freemoder.relGroup
		BREAK
		CASE TEAM_SCTV
			RETURN TeamData_SCTV.relGroup
		BREAK	
//		DEFAULT
//			IF (iTeam > -1)
//			AND (iTeam < MAX_NUM_TEAMS)
//				RETURN TeamData[iTeam].relGroup
//			ENDIF
//		BREAK
	ENDSWITCH		
		
	SCRIPT_ASSERT("GET_TEAM_REL_GROUP - invalid team")
	RETURN TeamData_Freemoder.relGroup
	
ENDFUNC


/// PURPOSE:
///    Sets the local players CnC team.
/// PARAMS:
///    team - A team/gang.
PROC SET_LOCAL_PLAYER_TEAM(INT iTeam)

	SET_PLAYER_TEAM(PLAYER_ID(), iTeam)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH iTeam
			CASE TEAM_FREEMODE
				SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), TeamData_Freemoder.relGroup)
			BREAK
			CASE TEAM_SCTV
				SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), TeamData_SCTV.relGroup)
			BREAK	
			DEFAULT
					IF (iTeam = -1)
						SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_Team[NATIVE_TO_INT(PLAYER_ID())])	//rgFM_DefaultPlayer)
					ENDIF
			BREAK
		ENDSWITCH
	ENDIF

	// PT fix (866418) Speirs
	SWITCH GET_CURRENT_GAMEMODE()
		CASE GAMEMODE_FM
			SWITCH iTeam
				CASE TEAM_SCTV
					SET_PLAYER_CAN_COLLECT_DROPPED_MONEY(PLAYER_ID(), TeamData_SCTV.bPlayerCanCollectDroppedMoney)
				BREAK	
				DEFAULT
					SET_PLAYER_CAN_COLLECT_DROPPED_MONEY(PLAYER_ID(), TRUE)
				BREAK
			ENDSWITCH				
		BREAK
		DEFAULT	
			SWITCH iTeam
				CASE TEAM_FREEMODE
					SET_PLAYER_CAN_COLLECT_DROPPED_MONEY(PLAYER_ID(), TeamData_Freemoder.bPlayerCanCollectDroppedMoney)
				BREAK
				CASE TEAM_SCTV
					SET_PLAYER_CAN_COLLECT_DROPPED_MONEY(PLAYER_ID(), TeamData_SCTV.bPlayerCanCollectDroppedMoney)
				BREAK	
//				DEFAULT
//					IF (iTeam > -1)
//					AND (iTeam < MAX_NUM_TEAMS)
//						SET_PLAYER_CAN_COLLECT_DROPPED_MONEY(PLAYER_ID(), TeamData[iTeam].bPlayerCanCollectDroppedMoney)						
//					ENDIF
//				BREAK
			ENDSWITCH	
		BREAK
	ENDSWITCH
	 

	NET_PRINT("SET_LOCAL_PLAYER_TEAM - [")
	NET_PRINT_INT(iTeam)
	NET_PRINT("]")
	NET_NL()
ENDPROC

/// PURPOSE:
///    Checks to see if specified player is on the same team as local player
FUNC BOOL IS_PLAYER_ON_MY_TEAM(PLAYER_INDEX playerID)
	RETURN GET_PLAYER_TEAM(PLAYER_ID()) = GET_PLAYER_TEAM(playerID)
ENDFUNC



//FUNC BLIP_SPRITE GET_BLIP_SPRITE_FOR_TEAM(INT iTeam, BOOL bIsHighlighted=FALSE)
//	IF (bIsHighlighted)
//		SWITCH iTeam
//			CASE TEAM_FREEMODE
//				RETURN TeamData_Freemoder.blipHighlighted
//			BREAK
//			CASE TEAM_SCTV
//				RETURN TeamData_SCTV.blipHighlighted
//			BREAK	
//			DEFAULT
//				IF (iTeam > -1)
//				AND (iTeam < MAX_NUM_TEAMS)
//					RETURN TeamData[iTeam].blipHighlighted
//				ENDIF
//			BREAK
//		ENDSWITCH		
//	ELSE
//		RETURN(GET_STANDARD_BLIP_ENUM_ID())
//	ENDIF
//	RETURN(GET_STANDARD_BLIP_ENUM_ID())
//ENDFUNC

/// PURPOSE: Geta model enum for the team's vehicle
FUNC MODEL_NAMES GET_TEAM_VEHICLE_MODEL(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
		CASE -1
			RETURN TeamData_Freemoder.ModelVehicle
		BREAK
		CASE TEAM_SCTV
			RETURN TeamData_SCTV.ModelVehicle
		BREAK
	ENDSWITCH
//	IF (iTeam > -1)
//	AND (iTeam < MAX_NUM_TEAMS)
//		RETURN TeamData[iTeam].ModelVehicle
//	ENDIF	
	SCRIPT_ASSERT("GET_TEAM_VEHICLE_MODEL - unknown for team number")
	RETURN TeamData_Freemoder.ModelVehicle
ENDFUNC

/// PURPOSE: Geta model enum for the team's vehicle
FUNC INT GET_TEAM_VEHICLE_COLOUR(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
		CASE -1
			RETURN TeamData_Freemoder.iVehicleColour
		BREAK
		CASE TEAM_SCTV
			RETURN TeamData_SCTV.iVehicleColour
		BREAK	

	ENDSWITCH	
//	IF (iTeam > -1)
//	AND (iTeam < MAX_NUM_TEAMS)
//		RETURN TeamData[iTeam].iVehicleColour
//	ENDIF
	SCRIPT_ASSERT("GET_TEAM_VEHICLE_COLOUR - unknown for team number")
	RETURN(0)
ENDFUNC

/// PURPOSE:
///    Gets lieuteneant model for gang.
///    Note: currently no model for professionals gang.
/// PARAMS:
///    iGang - gang to get lieutenant model for.
/// RETURNS:
///    Gang lieutenant model.
FUNC MODEL_NAMES GET_LIEUTENTANT_MODEL(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN TeamData_Freemoder.ModelLieutenent
		BREAK
		CASE TEAM_SCTV
			RETURN TeamData_SCTV.ModelLieutenent
		BREAK	
	ENDSWITCH
//	IF (iTeam > -1)
//	AND (iTeam < MAX_NUM_TEAMS)
//		RETURN TeamData[iTeam].ModelLieutenent
//	ENDIF
	SCRIPT_ASSERT("GET_LIEUTENTANT_MODEL - unknown for team number")
	RETURN(DUMMY_MODEL_FOR_SCRIPT)
ENDFUNC

FUNC STRING GET_SPEAKER_NAME_FOR_TEAM(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN CONVERT_TEXT_LABEL_TO_STRING(TeamData_Freemoder.str_TeamSpeakerName)
		BREAK
		CASE TEAM_SCTV
			RETURN CONVERT_TEXT_LABEL_TO_STRING(TeamData_SCTV.str_TeamSpeakerName)
		BREAK	
	ENDSWITCH
//	IF (iTeam > -1)
//	AND (iTeam < MAX_NUM_TEAMS)
//		RETURN CONVERT_TEXT_LABEL_TO_STRING(TeamData[iTeam].str_TeamSpeakerName)
//	ENDIF
	SCRIPT_ASSERT("GET_SPEAKER_NAME_FOR_TEAM - unknown for team number")
	RETURN("")
ENDFUNC

//Returns the Char name for each gang boss
FUNC STRING GET_TEAM_SPEAKER_NAME()
	INT iplayerteam = GET_PLAYER_TEAM(PLAYER_ID())
	IF (iplayerteam > -1)
	AND (iplayerteam < MAX_TEAM_VALUE)
		RETURN GET_SPEAKER_NAME_FOR_TEAM(iplayerteam)
	ENDIF
	SCRIPT_ASSERT("GET_TEAM_SPEAKER_NAME - unknown for team number")
	RETURN ""
ENDFUNC

FUNC STRING GET_SPEAKER_ID_FOR_TEAM(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN CONVERT_INT_TO_STRING(TeamData_Freemoder.iSpeakerID)
		BREAK
		CASE TEAM_SCTV
			RETURN CONVERT_INT_TO_STRING(TeamData_SCTV.iSpeakerID)
		BREAK	
	ENDSWITCH	
//	IF (iTeam > -1)
//	AND (iTeam < MAX_NUM_TEAMS)	
//		RETURN CONVERT_INT_TO_STRING(TeamData[iTeam].iSpeakerID)
//	ENDIF
	SCRIPT_ASSERT("GET_SPEAKER_ID_FOR_TEAM - unknown for team number")
	RETURN("")
ENDFUNC

//Returns the speaker Id for each gang boss
FUNC STRING GET_TEAM_SPEAKER_ID()
	INT iplayerteam = GET_PLAYER_TEAM(PLAYER_ID())
	IF (iplayerteam > -1)
	AND (iplayerteam < MAX_TEAM_VALUE)
		RETURN GET_SPEAKER_ID_FOR_TEAM(iplayerteam)
	ENDIF
	SCRIPT_ASSERT("GET_TEAM_SPEAKER_ID - unknown for team number")
	RETURN ""	
ENDFUNC

FUNC INT GET_SPEAKER_ID_INT_FOR_TEAM(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN (TeamData_Freemoder.iSpeakerID)
		BREAK
		CASE TEAM_SCTV
			RETURN (TeamData_SCTV.iSpeakerID)
		BREAK	
	ENDSWITCH	
//	IF (iTeam > -1)
//	AND (iTeam < MAX_NUM_TEAMS)
//		RETURN (TeamData[iTeam].iSpeakerID)
//	ENDIF
	SCRIPT_ASSERT("GET_SPEAKER_ID_INT_FOR_TEAM - unknown for team number")
	RETURN(-1)
ENDFUNC

//PURPOSE: Returns the speaker Id for each gang boss
FUNC INT GET_TEAM_SPEAKER_ID_INT()
	INT iplayerteam = GET_PLAYER_TEAM(PLAYER_ID())
	IF (iplayerteam > -1)
	AND (iplayerteam < MAX_TEAM_VALUE)
		RETURN GET_SPEAKER_ID_INT_FOR_TEAM(iplayerteam)
	ENDIF
	SCRIPT_ASSERT("GET_TEAM_SPEAKER_ID_INT - unknown for team number")
	RETURN 8
ENDFUNC

FUNC enumCharacterList GET_BOSS_FROM_CHARACTER_LIST_FOR_TEAM(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN (TeamData_Freemoder.charBoss)
		BREAK
		CASE TEAM_SCTV
			RETURN (TeamData_SCTV.charBoss)
		BREAK	
	ENDSWITCH
//	IF (iTeam > -1)
//	AND (iTeam < MAX_NUM_TEAMS)
//		RETURN (TeamData[iTeam].charBoss)
//	ENDIF
	SCRIPT_ASSERT("GET_BOSS_FROM_CHARACTER_LIST_FOR_TEAM - unknown for team number")
	RETURN INT_TO_ENUM(enumCharacterList, 0)
ENDFUNC

//Returns the Chararacter List enum for each gang boss
FUNC enumCharacterList GET_TEAM_BOSS_FROM_CHARACTER_LIST()
	INT iplayerteam = GET_PLAYER_TEAM(PLAYER_ID())	
	
	SWITCH iplayerteam
		CASE TEAM_FREEMODE
			RETURN GET_BOSS_FROM_CHARACTER_LIST_FOR_TEAM(iplayerteam)
		BREAK
		CASE TEAM_SCTV
			RETURN GET_BOSS_FROM_CHARACTER_LIST_FOR_TEAM(iplayerteam)
		BREAK
//		DEFAULT
//			IF (iplayerteam > -1)
//			AND (iplayerteam < MAX_NUM_TEAMS)
//				RETURN GET_BOSS_FROM_CHARACTER_LIST_FOR_TEAM(iplayerteam)
//			ENDIF
//		BREAK
	ENDSWITCH
	SCRIPT_ASSERT("GET_TEAM_BOSS_FROM_CHARACTER_LIST - unknown for team number")
	RETURN CHAR_MP_MEX_BOSS
ENDFUNC








FUNC BOOL IS_PLAYER_PED_FEMALE(PLAYER_INDEX playerID)
		IF (GET_ENTITY_MODEL(GET_PLAYER_PED(playerID)) = MP_F_FREEMODE_01)
			RETURN(TRUE)
		ENDIF
	RETURN FALSE
ENDFUNC

//// PURPOSE:
 ///    Returns True if the player model is female
 /// RETURNS:
 ///    
FUNC BOOL IS_PLAYER_FEMALE()
	RETURN IS_PLAYER_PED_FEMALE(PLAYER_ID())
ENDFUNC



#IF IS_DEBUG_BUILD
// PURPOSE:	Return the team name as a string
//
// INPUT PARAMS:		paramTeam			The Team ID
// RETURN VALUE:		STRING				The Team Name as a string
FUNC STRING Convert_Team_To_String(INT iTeam)

	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN CONVERT_TEXT_LABEL_TO_STRING(TeamData_Freemoder.str_TeamNameForDebug)
		BREAK
		CASE TEAM_SCTV
			RETURN CONVERT_TEXT_LABEL_TO_STRING(TeamData_SCTV.str_TeamNameForDebug)
		BREAK
		DEFAULT

		BREAK
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: NEW TEAM ID - ADD TO FUNCTION")

ENDFUNC
#ENDIF


FUNC STRING GET_TEAM_OVERHEAD_IMAGE_SPRITE(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN CONVERT_TEXT_LABEL_TO_STRING(TeamData_Freemoder.str_OverheadImageSprite)
		BREAK
		CASE TEAM_SCTV
			RETURN CONVERT_TEXT_LABEL_TO_STRING(TeamData_SCTV.str_OverheadImageSprite)
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("GET_TEAM_OVERHEAD_IMAGE_SPRITE - unknown for team number")
	RETURN("")
ENDFUNC

FUNC BLIP_SPRITE GET_TEAM_SAFEHOUSE_BLIP_SPRITE(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN (TeamData_Freemoder.blipSafehouse)
		BREAK
		CASE TEAM_SCTV
			RETURN (TeamData_SCTV.blipSafehouse)
		BREAK
		
	
	ENDSWITCH	

	SCRIPT_ASSERT("GET_TEAM_SAFEHOUSE_BLIP_SPRITE - unknown for team number")
	RETURN(INT_TO_ENUM(BLIP_SPRITE, 0))
ENDFUNC

FUNC STRING GET_TEAM_SAFEHOUSE_BLIP_NAME(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN CONVERT_TEXT_LABEL_TO_STRING(TeamData_Freemoder.str_SafehouseBlipName)
		BREAK
		CASE TEAM_SCTV
			RETURN CONVERT_TEXT_LABEL_TO_STRING(TeamData_SCTV.str_SafehouseBlipName)
		BREAK
	ENDSWITCH	

	SCRIPT_ASSERT("GET_TEAM_SAFEHOUSE_BLIP_NAME - unknown for team number")
	RETURN("")
ENDFUNC


FUNC BOOL IS_PLAYER_SCTV(PLAYER_INDEX playerID, BOOL bDoNativeTeamCheck = FALSE)
	BOOL bIsSCTV
	
	IF (PlayerID = PLAYER_ID())
		bIsSCTV = (GET_STAT_CHARACTER_TEAM() = TEAM_SCTV)	//- Removing Stats as this only returns the local player. 
	ELSE
		bIsSCTV = (GlobalplayerBD_FM[NATIVE_TO_INT(PlayerID)].scoreData.iTeam = TEAM_SCTV)
	ENDIF
	
	IF bDoNativeTeamCheck = TRUE
		IF NETWORK_IS_PLAYER_ACTIVE(playerID)
			bIsSCTV = (GET_PLAYER_TEAM(playerID) = TEAM_SCTV)
		ENDIF
	ENDIF
	
	RETURN bIsSCTV
ENDFUNC

FUNC BOOL IS_PLAYER_DEV_SPECTATOR(PLAYER_INDEX playerID)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_PLAYER_SCTV(playerID)
			RETURN NETWORK_PLAYER_IS_ROCKSTAR_DEV(playerID)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_TEAM_THE_COPS(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN TeamData_Freemoder.TeamType = TEAM_TYPE_COP
		BREAK
		CASE TEAM_SCTV
			RETURN TeamData_SCTV.TeamType = TEAM_TYPE_COP
		BREAK
	ENDSWITCH	

	SCRIPT_ASSERT("IS_TEAM_THE_COPS - unknown for team number")
	RETURN(FALSE)
ENDFUNC


FUNC BOOL IS_TEAM_A_GANG(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN TeamData_Freemoder.TeamType = TEAM_TYPE_CRIMINAL
		BREAK
		CASE TEAM_SCTV
			RETURN TeamData_SCTV.TeamType = TEAM_TYPE_CRIMINAL
		BREAK
	ENDSWITCH

	SCRIPT_ASSERT("IS_TEAM_A_GANG - unknown for team number")
	RETURN(FALSE)
ENDFUNC


FUNC BOOL IS_TEAM_AN_ACTIVE_TEAM(INT iTeam)
	IF (iTeam = TEAM_INVALID)
		RETURN FALSE
	ENDIF
	
	SWITCH iTeam
		CASE TEAM_FREEMODE
			IF (TeamData_Freemoder.TeamType = TEAM_TYPE_NON_ACTIVE)
				RETURN FALSE
			ENDIF
		BREAK
		CASE TEAM_SCTV
			IF (TeamData_SCTV.TeamType = TEAM_TYPE_NON_ACTIVE)
				RETURN FALSE
			ENDIF
		BREAK
		DEFAULT

		BREAK
	ENDSWITCH	

	// Active Team
	RETURN TRUE
ENDFUNC


FUNC BOOL DOES_TEAM_HAVE_GARAGES(INT iTeam)
	SWITCH iTeam
		CASE TEAM_FREEMODE
			RETURN TeamData_Freemoder.bHasGotGarages = FALSE
		BREAK
		CASE TEAM_SCTV
			RETURN TeamData_SCTV.bHasGotGarages = FALSE
		BREAK
	ENDSWITCH	

	SCRIPT_ASSERT("DOES_TEAM_HAVE_GARAGES - unknown for team number")
	RETURN(FALSE)
ENDFUNC

// EOF



