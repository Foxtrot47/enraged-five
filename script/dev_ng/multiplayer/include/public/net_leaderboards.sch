//Net_Leaderboards.sch
//Wrapper functions for leaderboard writes in MP game.

USING "commands_network.sch"
USING "commands_stats.sch"
USING "mp_globals_block_SC_LB.sch"

//STRUCT LEADERBOARD_SETUP_DETAILS
//	INT iNumGroups
//	TEXT_LABEL_31 categoryNames[LEADERBOARD_MAX_GROUPS]
//ENDSTRUCT

//FUNC LEADERBOARD_SETUP_DETAILS GET_LEADERBOARD_SETUP_DETAILS(LEADERBOARDS_ENUM LeaderboardID)
//	LEADERBOARD_SETUP_DETAILS tempLeaderboard
//	
//	SWITCH LeaderboardID
//		CASE LEADERBOARD_CNC_TEAM_TERRITORY_TAKEOVERS
//			tempLeaderboard.iNumGroups = 1
//			tempLeaderboard.categoryNames[0] = "Territory"
//		BREAK
//		CASE LEADERBOARD_FREEMODE_RACES
//		CASE LEADERBOARD_FREEMODE_BASEJUMPS
//		CASE LEADERBOARD_FREEMODE_DEATHMATCH
//		CASE LEADERBOARD_FREEMODE_SURVIVAL
//		CASE LEADERBOARD_FREEMODE_MISSIONS_BY_TIME
//		CASE LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE
//			tempLeaderboard.iNumGroups = 1
//			tempLeaderboard.categoryNames[0] = "Mission"
//		BREAK
//		DEFAULT
//			tempLeaderboard.iNumGroups = 0
//			tempLeaderboard.categoryNames[0] = ""
//		BREAK
//	ENDSWITCH
//	
//	RETURN tempLeaderboard
//ENDFUNC
///// PURPOSE:
/////    This is an old simplified command try to use the INIT_LEADERBOARD_WRITE command instead
///// PARAMS:
/////    LeaderboardID - leaderbaord enum
/////    uniqueIdentifier - unique identifier for the group for UGC this is the filename
/////    bInMP - if set true sets clan id
//PROC INIT_LEADERBOARD_WRITE(LEADERBOARDS_ENUM LeaderboardID,TEXT_LABEL_31 uniqueIdentifier, BOOL bInMP = TRUE)
//	LeaderboardUpdateData writeData
//	INT i
//	GAMER_HANDLE gamerHandle
//	NETWORK_CLAN_DESC PlayerClan
//	
//	// Begin setting the leaderboard data types
//	writeData.m_LeaderboardId = ENUM_TO_INT(LeaderboardID)
//	
//	LEADERBOARD_SETUP_DETAILS leaderBoardDetails
//	leaderBoardDetails = GET_LEADERBOARD_SETUP_DETAILS(LeaderboardID)
//	
//	PRINTLN()
//	PRINTLN("CDM: INITIALISE_LEADERBOARD_WRITE with following data")
//	PRINTLN("Leaderboard ID: ", writeData.m_LeaderboardId)
//	
//	
//	IF bInMP
//		IF NETWORK_IS_SIGNED_ONLINE()
//			IF NETWORK_CLAN_SERVICE_IS_VALID()
//				gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
//				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
//					IF NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
//						writeData.m_ClanId = PlayerClan.Id
//						PRINTLN("Clan ID: ", writeData.m_ClanId)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		//dont need groups in SP no UGC separation
//		writeData.m_GroupSelector.m_NumGroups = leaderBoardDetails.iNumGroups
//		PRINTLN("Num of groups to write to: ", writeData.m_GroupSelector.m_NumGroups)
//		REPEAT leaderBoardDetails.iNumGroups i
//			writeData.m_GroupSelector.m_Group[i].m_Category = leaderBoardDetails.categoryNames[i]
//			writeData.m_GroupSelector.m_Group[i].m_Id = uniqueIdentifier
//			PRINTLN("m_Category (", i, ")", ": ", writeData.m_GroupSelector.m_Group[i].m_Category) 
//			PRINTLN("m_Id (", i, ")", ": ", writeData.m_GroupSelector.m_Group[i].m_Id) 
//		ENDREPEAT
//	ENDIF
//	
//	LEADERBOARDS2_WRITE_DATA(writeData)
//	
//ENDPROC

/// PURPOSE:
///    INIT_LEADERBOARD_WRITE will assert all over if you call it without verifying that the player is online and not
///    a cheater first. This is a support function that wraps all of those checks to prevent asserts.
FUNC BOOL CAN_WRITE_TO_LEADERBOARD()
	RETURN NETWORK_IS_SIGNED_ONLINE() //AND NOT NETWORK_PLAYER_IS_CHEATER()
ENDFUNC

/// PURPOSE:
///    Starts a leaderboard write
/// PARAMS:
///    LeaderboardID - leaderbaord enum
///    uniqueIdentifier - list of unique identifiers for the leaderboard write
///    categoryNames - list of category names for leaderboard writes 
///    iNumCategories - number of categories to write to 
///    bInMP - if set true sets clan id
FUNC BOOL INIT_LEADERBOARD_WRITE(LEADERBOARDS_ENUM LeaderboardID,TEXT_LABEL_23 &uniqueIdentifier[],TEXT_LABEL_31 &categoryNames[],INT iNumCategories, INT iOverrideClanID = -1, BOOL bWriteUsedForPrediction = FALSE, BOOL bFakeWrite = FALSE)
	IF NOT NETWORK_IS_SIGNED_ONLINE()
		PRINTLN("INIT_LEADERBOARD_WRITE: Trying to write to leaderboards when NOT online!!")
		SCRIPT_ASSERT("INIT_LEADERBOARD_WRITE: Trying to write to leaderboards when NOT online!! - See Conor")
	ENDIF
	IF NOT NETWORK_PLAYER_IS_CHEATER()
	AND (NETWORK_HAVE_ONLINE_PRIVILEGES() OR NOT NETWORK_HAS_AGE_RESTRICTIONS())
	AND NETWORK_HAVE_ROS_LEADERBOARD_WRITE_PRIV()
		LeaderboardUpdateData writeData
		INT i
		GAMER_HANDLE gamerHandle
		NETWORK_CLAN_DESC PlayerClan
		
		// Begin setting the leaderboard data types
		writeData.m_LeaderboardId = ENUM_TO_INT(LeaderboardID)
		
		PRINTLN()
		PRINTLN("CDM: INIT_LEADERBOARD_WRITE with following data")
		PRINTLN("Leaderboard ID: ", writeData.m_LeaderboardId)
		
		IF iOverrideClanID = -1 
			IF NETWORK_CLAN_SERVICE_IS_VALID()
				gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
					IF NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
						writeData.m_ClanId = PlayerClan.Id
						PRINTLN("Clan ID: ", writeData.m_ClanId)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			writeData.m_ClanId = iOverrideClanID
			PRINTLN("Clan ID: ", writeData.m_ClanId)
		ENDIF
	

		//dont need groups in SP no UGC separation, scratch that now used for 
		writeData.m_GroupSelector.m_NumGroups = iNumCategories
		PRINTLN("Num of categories to write to: ", iNumCategories)
		REPEAT iNumCategories i
			writeData.m_GroupSelector.m_Group[i].m_Category = categoryNames[i]
			writeData.m_GroupSelector.m_Group[i].m_Id = uniqueIdentifier[i]
			PRINTLN("m_Category (", i, ")", ": ", writeData.m_GroupSelector.m_Group[i].m_Category) 
			PRINTLN("m_Id (", i, ")", ": ", writeData.m_GroupSelector.m_Group[i].m_Id) 
		ENDREPEAT
		
		IF bWriteUsedForPrediction
			sclb_rank_predict.LBWriteDetails = writeData
			PRINTLN("INIT_LEADERBOARD_WRITE: used for rank prediction setting sclb_rank_predict.LBWriteDetails")
			PRINTLN("sclb_rank_predict.LBWriteDetails.m_LeaderboardId ",sclb_rank_predict.LBWriteDetails.m_LeaderboardId)
			PRINTLN("sclb_rank_predict.LBWriteDetails.m_ClanId ",sclb_rank_predict.LBWriteDetails.m_ClanId)
		ELSE
			PRINTLN("INIT_LEADERBOARD_WRITE: NOT used for rank prediction!")
		ENDIF
		IF NOT bFakeWrite
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND g_sCURRENT_UGC_STATUS.g_bUgcGlobalsBlockLoaded
				LEADERBOARDS2_WRITE_DATA_FOR_EVENT_TYPE(writeData,g_sScComunityPlaylist.tl23ActiveEventType)
				PRINTLN("g_sScComunityPlaylist.tl23ActiveEventType = ",g_sScComunityPlaylist.tl23ActiveEventType)
			ELSE
				LEADERBOARDS2_WRITE_DATA(writeData)
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	PRINTLN("INIT_LEADERBOARD_WRITE: NOT WRITING TO LEADERBOARD ", ENUM_TO_INT(LeaderboardID))
	IF NETWORK_PLAYER_IS_CHEATER()
		PRINTLN("PLAYER IS CHEATER! ")
	ENDIF
	IF NOT NETWORK_HAVE_ONLINE_PRIVILEGES()
		PRINTLN("PLAYER NOT NETWORK_HAVE_ONLINE_PRIVILEGES ")
	ENDIF
	IF NETWORK_HAS_AGE_RESTRICTIONS()
		PRINTLN("PLAYER NETWORK_HAS_AGE_RESTRICTIONS ")
	ENDIF
	IF NOT NETWORK_HAVE_ROS_LEADERBOARD_WRITE_PRIV()
		PRINTLN("PLAYER NOT NETWORK_HAVE_ROS_LEADERBOARD_WRITE_PRIV ")
	ENDIF
	RETURN FALSE
ENDFUNC

//FUNC BOOL INIT_LEADERBOARD_WRITE_DATA_FOR_EVENT_TYPE(LEADERBOARDS_ENUM LeaderboardID,TEXT_LABEL_23 &uniqueIdentifier[],TEXT_LABEL_31 &categoryNames[],INT iNumCategories, INT iOverrideClanID = -1, BOOL bWriteUsedForPrediction = FALSE, BOOL bFakeWrite = FALSE, STRING eventType= NULL)
//	IF NOT NETWORK_IS_SIGNED_ONLINE()
//		PRINTLN("INIT_LEADERBOARD_WRITE: Trying to write to leaderboards when NOT online!!")
//		SCRIPT_ASSERT("INIT_LEADERBOARD_WRITE: Trying to write to leaderboards when NOT online!! - See Conor")
//	ENDIF
//	IF NOT NETWORK_PLAYER_IS_CHEATER()
//	AND NETWORK_HAVE_ONLINE_PRIVILEGES()
//	AND NETWORK_HAVE_ROS_LEADERBOARD_WRITE_PRIV()
//		LeaderboardUpdateData writeData
//		INT i
//		GAMER_HANDLE gamerHandle
//		NETWORK_CLAN_DESC PlayerClan
//		
//		// Begin setting the leaderboard data types
//		writeData.m_LeaderboardId = ENUM_TO_INT(LeaderboardID)
//		
//		PRINTLN()
//		PRINTLN("CDM: INIT_LEADERBOARD_WRITE with following data")
//		PRINTLN("Leaderboard ID: ", writeData.m_LeaderboardId)
//		
//		IF iOverrideClanID = -1 
//			IF NETWORK_CLAN_SERVICE_IS_VALID()
//				gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
//				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
//					IF NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
//						writeData.m_ClanId = PlayerClan.Id
//						PRINTLN("Clan ID: ", writeData.m_ClanId)
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
//			writeData.m_ClanId = iOverrideClanID
//			PRINTLN("Clan ID: ", writeData.m_ClanId)
//		ENDIF
//	
//
//		//dont need groups in SP no UGC separation, scratch that now used for 
//		writeData.m_GroupSelector.m_NumGroups = iNumCategories
//		PRINTLN("Num of categories to write to: ", iNumCategories)
//		REPEAT iNumCategories i
//			writeData.m_GroupSelector.m_Group[i].m_Category = categoryNames[i]
//			writeData.m_GroupSelector.m_Group[i].m_Id = uniqueIdentifier[i]
//			PRINTLN("m_Category (", i, ")", ": ", writeData.m_GroupSelector.m_Group[i].m_Category) 
//			PRINTLN("m_Id (", i, ")", ": ", writeData.m_GroupSelector.m_Group[i].m_Id) 
//		ENDREPEAT
//		
//		IF bWriteUsedForPrediction
//			sclb_rank_predict.LBWriteDetails = writeData
//		ENDIF
//		IF NOT bFakeWrite
//			LEADERBOARDS2_WRITE_DATA_FOR_EVENT_TYPE(writeData, eventType)
//		ENDIF
//		RETURN TRUE
//	ENDIF
//	PRINTLN("INIT_LEADERBOARD_WRITE: NOT WRITING TO LEADERBOARD ", ENUM_TO_INT(LeaderboardID))
//	IF NETWORK_PLAYER_IS_CHEATER()
//		PRINTLN("PLAYER IS CHEATER! ")
//	ENDIF
//	IF NOT NETWORK_HAVE_ONLINE_PRIVILEGES()
//		PRINTLN("PLAYER NOT NETWORK_HAVE_ONLINE_PRIVILEGES ")
//	ENDIF
//	IF NOT NETWORK_HAVE_ROS_LEADERBOARD_WRITE_PRIV()
//		PRINTLN("PLAYER NOT NETWORK_HAVE_ROS_LEADERBOARD_WRITE_PRIV ")
//	ENDIF
//	RETURN FALSE
//ENDFUNC
FUNC BOOL IS_LEADERBOARD_COLUMN_INT(INT iLeaderboardID, LEADERBOARDS2_TYPES_ENUM enumType, INT iColumn)
//	CONST_INT RL_STAT_TYPE_CONTEXT     0
//	CONST_INT RL_STAT_TYPE_INT32       1
//	CONST_INT RL_STAT_TYPE_INT64       2
//	CONST_INT RL_STAT_TYPE_DOUBLE      3
//	CONST_INT RL_STAT_TYPE_UNICODE     4
//	CONST_INT RL_STAT_TYPE_FLOAT       5
//	CONST_INT RL_STAT_TYPE_BINARY      6
//	CONST_INT RL_STAT_TYPE_DATETIME    7
//	CONST_INT RL_STAT_TYPE_NULL        255
	INT iColumnType
	//iColumn = iColumn + 1
	PRINTLN("IS_LEADERBOARD_COLUMN_INT: iLeaderboardID = ", iLeaderboardID)
	PRINTLN("IS_LEADERBOARD_COLUMN_INT: iColumn = ", iColumn)
	
	//Special Cases
	IF iLeaderboardID = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_DARTS)
		IF iColumn = 0
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF iLeaderboardID = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_SRANGE)
		IF iColumn = 3
		OR iColumn = 6
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF iLeaderboardID = ENUM_TO_INT(LEADERBOARD_MINI_GAMES_MP_SRANGE)
		IF iColumn = 0
		OR iColumn = 3
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_DEATHMATCH)
	OR iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_TEAM_DEATHMATCH)
	OR iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_VEHICLE_DEATHMATCH)
		IF iColumn = 0
		OR iColumn = 3
			RETURN FALSE
		ENDIF
	ENDIF
	IF iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_SURVIVAL)
		IF iColumn = 4
			RETURN FALSE
		ENDIF
	ENDIF
	IF iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE)
//	OR iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_MISSIONS_BY_SCORE)
		IF iColumn = 3
			RETURN FALSE
		ENDIF
	ENDIF
	IF iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_MISSIONS_BY_TIME)
		IF iColumn = 3
		OR iColumn = 7
			RETURN FALSE
		ENDIF
	ENDIF
	IF iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FREEMODE_HEIST_MISSION)
		IF iColumn = 4
			RETURN FALSE
		ENDIF
	ENDIF
	

	iColumnType = LEADERBOARDS_GET_COLUMN_TYPE(iLeaderboardID,enumType,iColumn)
	PRINTLN("IS_LEADERBOARD_COLUMN_INT: iColumnType = ", iColumnType)

	SWITCH iColumnType
		CASE RL_STAT_TYPE_INT32
		CASE RL_STAT_TYPE_INT64
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LEADERBOARD_COLUMN_FLOAT(INT iLeaderboardID, LEADERBOARDS2_TYPES_ENUM enumType, INT iColumn)
//	CONST_INT RL_STAT_TYPE_CONTEXT     0
//	CONST_INT RL_STAT_TYPE_INT32       1
//	CONST_INT RL_STAT_TYPE_INT64       2
//	CONST_INT RL_STAT_TYPE_DOUBLE      3
//	CONST_INT RL_STAT_TYPE_UNICODE     4
//	CONST_INT RL_STAT_TYPE_FLOAT       5
//	CONST_INT RL_STAT_TYPE_BINARY      6
//	CONST_INT RL_STAT_TYPE_DATETIME    7
//	CONST_INT RL_STAT_TYPE_NULL        255
	iColumn = iColumn + 1
	SWITCH LEADERBOARDS_GET_COLUMN_TYPE(iLeaderboardID,enumType,iColumn)
		CASE RL_STAT_TYPE_DOUBLE
		CASE RL_STAT_TYPE_FLOAT
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Writes values to the leaderboard and stores values for rank prediction values
/// PARAMS:
///    iLB_ID - the leaderboard enum to int
///    fieldId - the input field for the write i.e. LB_INPUT_COL_SCORE
///    ivalue - the int value if this is writing an int
///    fvalue - the float value if this is writing a float
///    bRankPredictionOnly - DO NOT SET THIS! I am using it specifically for rally races if you think you need this check with me first (Conor McGuire)
PROC WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(INT iLB_ID, LEADERBOARD_INPUTS fieldId, INT ivalue, FLOAT fvalue,BOOL bRankPredictionOnly = FALSE)
	INT iColumnIndex
	INT i
	PRINTLN("WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION: bRankPredictionOnly = ",bRankPredictionOnly)
	IF NOT bRankPredictionOnly 
		LEADERBOARDS_WRITE_ADD_COLUMN(fieldId,iValue,fValue)
	ENDIF
	IF NOT sclb_rank_predict.bAssignedValuesInWrite
		sclb_rank_predict.currentResult.m_Id = iLB_ID
		sclb_rank_predict.currentResult.m_NumColumns = LEADERBOARDS_GET_NUMBER_OF_COLUMNS(sclb_rank_predict.currentResult.m_Id,LEADERBOARD_TYPE_GROUP)
		PRINTLN("WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION: Leaderboard ID = ", sclb_rank_predict.currentResult.m_Id)
		PRINTLN("WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION: Num columns = ", sclb_rank_predict.currentResult.m_NumColumns)
		REPEAT MAX_COLUMNS i
			IF i < sclb_rank_predict.currentResult.m_NumColumns 
				IF fieldId = LB_INPUT_COL_MATCH_ID 
					PRINTLN("WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION: ignoring column #",i, " it's match ID")
				ELSE
					IF IS_LEADERBOARD_COLUMN_INT(iLB_ID,LEADERBOARD2_TYPE_GROUP,i)
						SET_BIT(sclb_rank_predict.currentResult.m_ColumnsBitSets,i)
						PRINTLN("WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION: setting column #",i, " as int")
					ELSE
						CLEAR_BIT(sclb_rank_predict.currentResult.m_ColumnsBitSets,i)
						PRINTLN("WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION: setting column #",i, " as float")
					ENDIF
				ENDIF
			ELSE
				CLEAR_BIT(sclb_rank_predict.currentResult.m_ColumnsBitSets,i)
			ENDIF
		ENDREPEAT
		PRINTLN("WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION: Called by ", GET_THIS_SCRIPT_NAME())
		sclb_rank_predict.bAssignedValuesInWrite = TRUE
	ENDIF
	REPEAT MAX_COLUMNS i
		IF ENUM_TO_INT(fieldId) = LEADERBOARDS_GET_COLUMN_ID(iLB_ID,LEADERBOARD2_TYPE_GROUP,i)
			iColumnIndex = i
			i= MAX_COLUMNS
		ENDIF
	ENDREPEAT

	sclb_rank_predict.currentResult.m_iColumnData[iColumnIndex] = iValue
	PRINTLN("WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION: column #",iColumnIndex ," INT value: ", iValue)

	sclb_rank_predict.currentResult.m_fColumnData[iColumnIndex] = fValue
	PRINTLN("WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION: column #",iColumnIndex ," FLOAT value: ", fValue)
	IF iValue != 0
		SET_BIT(sclb_rank_predict.currentResult.m_ColumnsBitSets,iColumnIndex)
	ELIF fValue != 0
		CLEAR_BIT(sclb_rank_predict.currentResult.m_ColumnsBitSets,iColumnIndex)
	ENDIF
ENDPROC

//EOF
