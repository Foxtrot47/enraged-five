#IF FEATURE_DLC_2_2022
USING "net_marketing_rewards_defs.sch"
USING "mp_globals_casino_inside_track_minigame_consts.sch"		
// If we want to remove above headerthis we need to create a MLR transaction states and
// transform between GENERIC_TRANSACTION_STATE and the new MLR states
// Can't use net_cash_transactions.sch -> cycling headers

ENUM MARKETING_LOGIN_REWARDS_STAGES
	MLRS_INIT,
	MLRS_RESET_DATA,
	MLRS_UPDATE_EVENT_ID,
	MLRS_SUBMIT_TO_QUEUE,
	MLRS_PROCESSING,
	MLRS_UPDATE_SAVED_BITSETS,
	MLRS_NOTIFY_PLAYER,
	MLRS_FINISHED
ENDENUM

STRUCT MARKETING_LOGIN_REWARDS_DATA
	MARKETING_LOGIN_REWARDS_STAGES eCurrentStage 	= MLRS_INIT
	INT iRewardsQueuedBitset0 						= 0
	INT iRewardsFailedToAddBitset0 					= 0
	INT	iRewardsToAddBitset0 						= 0
	GENERIC_TRANSACTION_STATE eTransactionResult	= TRANSACTION_STATE_DEFAULT
	
	INT iLatestEventID 								= 0
	INT iLatestRewardsBitset0 						= 0
	INT iSavedEventID 								= 0
	INT iSavedRewardsBitset0 						= 0
	#IF IS_DEBUG_BUILD
	BOOL bEnableOverrideLoginRewards 				= FALSE
	#ENDIF
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT MARKETING_LOGIN_REWARDS_WIDGET_DATA
	BOOL bMLRWidgetItemsToggles [MARKETING_REWARDS_COUNT]
	BOOL bMLRWidgetToggleMoreInfo 			= FALSE
	BOOL bMLREnableTestingThroughWidget 	= FALSE
	INT iMLRWidgetBitset 					= 0
	INT iMLRWidgetEventID 					= -1
	BOOL bMLRWidgetRunBitset 				= FALSE
	INT iMLRWidgetStage 					= -1
	BOOL bMLRWidgetRunStarted 				= FALSE
	BOOL bMLRWidgetResetRun 				= FALSE
	INT iMLRWidgetSavedEventID 				= 0
	INT iMLRWidgetSavedBitset 				= 0
	BOOL bOverrideEnableLoginRewards		= FALSE
ENDSTRUCT
#ENDIF

#ENDIF
