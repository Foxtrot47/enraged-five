USING "rage_builtins.sch"
USING "globals.sch"
USING "mp_globals_tunables.sch"

USING "net_prints.sch"

USING "net_missions_shared_public.sch"
USING "net_mission_joblist_public.sch"
USING "charsheet_public.sch"

// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_activity_selector_public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Functions that have direct access to global data to avoid cyclic header files.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Contact Character Conversion Functions
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//      enumCharacterList/g_eFMHeistContactIDs conversions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Convert enumCharacterList to g_eFMHeistContactIDs
//
// INPUT PARAMS:		paramCharacterListID		An ID from the enumCharacterList
// RETURN VALUE:		g_eFMHeistContactIDs		An ID from the FM Heist Contact ID list
FUNC g_eFMHeistContactIDs Convert_CharacterListID_To_FM_Heist_ContactID(enumCharacterList paramCharacterListID)

	SWITCH (paramCharacterListID)
		CASE CHAR_MP_GERALD			RETURN (FM_HEIST_GERALD)
		CASE CHAR_LAMAR				RETURN (FM_HEIST_LAMAR)
		CASE CHAR_LESTER			RETURN (FM_HEIST_LESTER)
		CASE CHAR_MARTIN			RETURN (FM_HEIST_MARTIN)
		CASE CHAR_RON				RETURN (FM_HEIST_RON)
		CASE CHAR_SIMEON			RETURN (FM_HEIST_SIMEON)
		CASE CHAR_TREVOR			RETURN (FM_HEIST_TREVOR)
		CASE CHAR_MP_AGENT_14		RETURN (FM_HEIST_AGENT14)
		CASE CHAR_CEOASSIST			RETURN (FM_HEIST_CEOASSIST)
		CASE CHAR_HAO				RETURN 	FM_HEIST_HAO
		CASE CHAR_OSCAR				RETURN  FM_HEIST_OSCAR
		CASE CHAR_NCLUBL			RETURN  FM_HEIST_LAZLOW
		CASE CHAR_MP_MERRYWEATHER	RETURN  FM_HEIST_MERRYWEATHER
		CASE CHAR_MP_BRUCIE			RETURN  FM_HEIST_BRUCIE
		CASE CHAR_MICHAEL			RETURN	FM_HEIST_MICHAEL	
		CASE CHAR_FRANKLIN			RETURN	FM_HEIST_FRANKLIN	
		CASE CHAR_NCLUBE			RETURN	FM_HEIST_ENG_DAVE	
		CASE CHAR_NCLUBT			RETURN	FM_HEIST_TONY		
		CASE CHAR_JIMMY				RETURN	FM_HEIST_JIMMY		
		CASE CHAR_TRACEY			RETURN	FM_HEIST_TRACEY	
		CASE CHAR_WADE				RETURN	FM_HEIST_WADE		
		CASE CHAR_DAVE				RETURN	FM_HEIST_DAVE		
		CASE CHAR_RICKIE			RETURN	FM_HEIST_RICKIE	
		CASE CHAR_CHEF				RETURN	FM_HEIST_CHEF		
		CASE CHAR_HUNTER			RETURN	FM_HEIST_HUNTER	
		CASE CHAR_CRIS				RETURN	FM_HEIST_CRIS		
		CASE CHAR_MARNIE			RETURN	FM_HEIST_MARNIE	
		CASE CHAR_MAUDE				RETURN	FM_HEIST_MAUDE		
		CASE CHAR_ASHLEY			RETURN	FM_HEIST_ASHLEY	
		CASE CHAR_OMEGA				RETURN	FM_HEIST_OMEGA	
		CASE CHAR_BBPAIGE			RETURN	FM_HEIST_BBPAIGE
		CASE CHAR_MP_RAY_LAVOY		RETURN	FM_HEIST_RAY_LAVOY
		CASE CHAR_CASINO_MANAGER	RETURN	FM_HEIST_CASINO_MANAGER
		#IF FEATURE_HEIST_ISLAND
		CASE CHAR_MIGUEL_MADRAZO	RETURN	FM_HEIST_MIGUEL_MADRAZO
		CASE CHAR_PAVEL				RETURN  FM_HEIST_PAVEL
		CASE CHAR_MOODYMANN			RETURN  FM_HEIST_MOODYMANN
		CASE CHAR_SESSANTA			RETURN	FM_HEIST_SESSANTA
		#ENDIF
		#IF FEATURE_TUNER
		CASE CHAR_KDJ				RETURN  FM_HEIST_KDJ
		#ENDIF
		#IF FEATURE_FIXER
		CASE CHAR_FIXER_FRANKLIN_IMANI_CONF	RETURN FM_HEIST_FRANKLIN_AND_IMANI
		#ENDIF
		#IF FEATURE_DLC_1_2022
		CASE CHAR_ULP				RETURN FM_HEIST_ULP
		#ENDIF
		#IF FEATURE_DLC_2_2022
		CASE CHAR_DAX				RETURN FM_HEIST_DAX		
		#ENDIF
	ENDSWITCH
	
	// Not found
	#IF IS_DEBUG_BUILD
	//	NET_PRINT("...KGM MP [ActSelect][Heist]: Convert_CharacterListID_To_FM_Heist_ContactID() Failed to find enumCharacterList ID as FM Heist Contact ID") NET_NL()

	#ENDIF
	
	RETURN (FM_HEIST_LAMAR)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Convert g_eFMHeistContactIDs to enumCharacterList
//
// INPUT PARAMS:		paramFMHeistContactID		An ID from the FM Heist Contact ID list
// RETURN VALUE:		enumCharacterList			An ID from the enumCharacterList
FUNC enumCharacterList Convert_FM_Heist_ContactID_To_CharacterListID(g_eFMHeistContactIDs paramFMHeistContactID)

	SWITCH (paramFMHeistContactID)
		CASE FM_HEIST_GERALD		RETURN (CHAR_MP_GERALD)
		CASE FM_HEIST_LAMAR			RETURN (CHAR_LAMAR)
		CASE FM_HEIST_LESTER		RETURN (CHAR_LESTER)
		CASE FM_HEIST_MARTIN		RETURN (CHAR_MARTIN)
		CASE FM_HEIST_RON			RETURN (CHAR_RON)
		CASE FM_HEIST_SIMEON		RETURN (CHAR_SIMEON)
		CASE FM_HEIST_TREVOR		RETURN (CHAR_TREVOR)
		CASE FM_HEIST_AGENT14		RETURN (CHAR_MP_AGENT_14)
		CASE FM_HEIST_CEOASSIST		RETURN (CHAR_CEOASSIST)
		CASE FM_HEIST_HAO			RETURN CHAR_HAO
		CASE FM_HEIST_OSCAR			RETURN CHAR_OSCAR
		CASE FM_HEIST_LAZLOW		RETURN CHAR_NCLUBL
		CASE FM_HEIST_MERRYWEATHER	RETURN CHAR_MP_MERRYWEATHER
		CASE FM_HEIST_BRUCIE		RETURN CHAR_MP_BRUCIE
		CASE FM_HEIST_MICHAEL		RETURN CHAR_MICHAEL
		CASE FM_HEIST_FRANKLIN		RETURN CHAR_FRANKLIN
		CASE FM_HEIST_ENG_DAVE		RETURN CHAR_NCLUBE
		CASE FM_HEIST_TONY			RETURN CHAR_NCLUBT
		CASE FM_HEIST_JIMMY			RETURN CHAR_JIMMY
		CASE FM_HEIST_TRACEY		RETURN CHAR_TRACEY
		CASE FM_HEIST_WADE			RETURN CHAR_WADE
		CASE FM_HEIST_DAVE			RETURN CHAR_DAVE
		CASE FM_HEIST_RICKIE		RETURN CHAR_RICKIE
		CASE FM_HEIST_CHEF			RETURN CHAR_CHEF
		CASE FM_HEIST_HUNTER		RETURN CHAR_HUNTER
		CASE FM_HEIST_CRIS			RETURN CHAR_CRIS
		CASE FM_HEIST_MARNIE		RETURN CHAR_MARNIE
		CASE FM_HEIST_MAUDE			RETURN CHAR_MAUDE
		CASE FM_HEIST_ASHLEY		RETURN CHAR_ASHLEY
		CASE FM_HEIST_OMEGA			RETURN CHAR_OMEGA
		CASE FM_HEIST_BBPAIGE 		RETURN CHAR_BBPAIGE
		CASE FM_HEIST_RAY_LAVOY 	RETURN CHAR_MP_RAY_LAVOY
		CASE FM_HEIST_CASINO_MANAGER	RETURN CHAR_CASINO_MANAGER
		CASE FM_HEIST_YACHT_CAPTAIN		RETURN CHAR_YACHT_CAPTAIN
		#IF FEATURE_HEIST_ISLAND
		CASE FM_HEIST_MIGUEL_MADRAZO	RETURN CHAR_MIGUEL_MADRAZO
		CASE FM_HEIST_PAVEL				RETURN CHAR_PAVEL
		CASE FM_HEIST_MOODYMANN			RETURN CHAR_MOODYMANN
		CASE FM_HEIST_SESSANTA			RETURN CHAR_SESSANTA
		#ENDIF
		#IF FEATURE_TUNER
		CASE FM_HEIST_KDJ				RETURN CHAR_KDJ
		#ENDIF
		#IF FEATURE_FIXER
		CASE FM_HEIST_FRANKLIN_AND_IMANI	RETURN CHAR_FIXER_FRANKLIN_IMANI_CONF
		#ENDIF
		#IF FEATURE_DLC_1_2022
		CASE FM_HEIST_ULP				RETURN CHAR_ULP
		#ENDIF
		#IF FEATURE_DLC_2_2022
		CASE FM_HEIST_DAX				RETURN CHAR_DAX
		#ENDIF
	ENDSWITCH
	
	// Not found
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][Heist]: Convert_FM_Heist_ContactID_To_CharacterListID() Failed to find FM Heist Contact ID: ") NET_PRINT_INT(ENUM_TO_INT(paramFMHeistContactID)) NET_NL()
		SCRIPT_ASSERT("Convert_FM_Heist_ContactID_To_CharacterListID(): Failed to find FM Heist Contact ID. Add to SWITCH statement. Returning LAMAR. Tell Keith.") 
	#ENDIF
	
	RETURN (CHAR_LAMAR)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the ContactID from the FM Heist Contact (as an INT)
//
// INPUT PARAMS:		paramHeistContactAsInt		An INT representing the specific FM Heist contact being maintained
// RETURN VALUE:		enumCharacterList			The enumCharacterList ID for this Heist Contact
FUNC enumCharacterList Convert_FM_Heist_ContactID_As_INT_To_CharacterListID(INT paramHeistContactAsInt)

	g_eFMHeistContactIDs	theFMHeistContact	= INT_TO_ENUM(g_eFMHeistContactIDs, paramHeistContactAsInt)
	enumCharacterList		theContactID		= Convert_FM_Heist_ContactID_To_CharacterListID(theFMHeistContact)
	
	RETURN (theContactID)
	
ENDFUNC





// -----------------------------------------------------------------------------------------------------------
//      enumCharacterList/g_eFMHeistContactIDs conversions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the HASH value for a Contact Character
//
// INPUT PARAMS:			paramCharacterListID		An ID from the enumCharacterList
// RETURN VALUE:			INT							A Hash value for this contact
//
// NOTES:	Used only for loading and saving the contact from the cloud to ensure the saved INT value remains protected from enum list changes
//			Decided to hardcode the strings because there's nothing else that guarantees the stable uniqueness of a contact character.
FUNC INT Get_Hash_Of_enumCharacterList_Contact(enumCharacterList paramCharacterListID)

	SWITCH (paramCharacterListID)
		CASE CHAR_MP_GERALD			RETURN (HASH("Gerald"))
		CASE CHAR_LAMAR				RETURN (HASH("Lamar"))
		CASE CHAR_LESTER			RETURN (HASH("Lester"))
		CASE CHAR_MARTIN			RETURN (HASH("Martin"))
		CASE CHAR_RON				RETURN (HASH("Ron"))
		CASE CHAR_SIMEON			RETURN (HASH("Simeon"))
		CASE CHAR_TREVOR			RETURN (HASH("Trevor"))
		CASE CHAR_MP_AGENT_14		RETURN (HASH("Agent14"))
		CASE CHAR_CEOASSIST			RETURN (HASH("PA"))
		CASE CHAR_HAO				RETURN (HASH("Hao"))
		CASE CHAR_OSCAR				RETURN (HASH("Oscar"))
		CASE CHAR_NCLUBL			RETURN (HASH("Lazlow"))
		CASE CHAR_MP_MERRYWEATHER	RETURN (HASH("MerryWeather"))
		CASE CHAR_MP_BRUCIE			RETURN (HASH("Brucie"))
		CASE CHAR_MICHAEL			RETURN (HASH("Michael"))	
		CASE CHAR_FRANKLIN			RETURN (HASH("Franklin"))	
		CASE CHAR_NCLUBE			RETURN (HASH("EnglishDave"))	
		CASE CHAR_NCLUBT			RETURN (HASH("Tony"))	
		CASE CHAR_JIMMY				RETURN (HASH("Jimmy"))	
		CASE CHAR_TRACEY			RETURN (HASH("Tracey"))	
		CASE CHAR_WADE				RETURN (HASH("Wade"))	
		CASE CHAR_DAVE				RETURN (HASH("Dave"))	
		CASE CHAR_RICKIE			RETURN (HASH("Rickie"))	
		CASE CHAR_CHEF				RETURN (HASH("Chef"))	
		CASE CHAR_HUNTER			RETURN (HASH("Hunter"))	
		CASE CHAR_CRIS				RETURN (HASH("Cris"))	
		CASE CHAR_MARNIE			RETURN (HASH("Marnie"))	
		CASE CHAR_MAUDE				RETURN (HASH("Maude"))	
		CASE CHAR_ASHLEY			RETURN (HASH("Ashley"))	
		CASE CHAR_OMEGA				RETURN (HASH("Omega"))
		CASE CHAR_BBPAIGE			RETURN (HASH("Paige"))
		CASE CHAR_MP_RAY_LAVOY		RETURN (HASH("Ray"))
		CASE CHAR_CASINO_MANAGER	RETURN (HASH("Agatha"))
		CASE CHAR_YACHT_CAPTAIN		RETURN (HASH("YachtCaptain"))
		#IF FEATURE_HEIST_ISLAND
		CASE CHAR_MIGUEL_MADRAZO	RETURN (HASH("MiguelMadrazo"))
		CASE CHAR_PAVEL				RETURN (HASH("Pavel"))
		CASE CHAR_MOODYMANN			RETURN (HASH("MoodyMann"))
		CASE CHAR_SESSANTA			RETURN (HASH("Sessanta"))
		#ENDIF
		#IF FEATURE_TUNER
		CASE CHAR_KDJ				RETURN (HASH("KDJ"))
		#ENDIF
		#IF FEATURE_FIXER
		CASE CHAR_FIXER_FRANKLIN_IMANI_CONF		RETURN (HASH("FIXER_FRANKLIN_IMANI_CONF"))
		#ENDIF
		#IF FEATURE_DLC_1_2022
		CASE CHAR_ULP				RETURN (HASH("ULP"))
		#ENDIF
		#IF FEATURE_DLC_2_2022
		CASE CHAR_DAX				RETURN (HASH("DAX"))
		#ENDIF
		CASE NO_CHARACTER			RETURN (0)
	ENDSWITCH
	
	// Not found
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect]: Get_Hash_Of_enumCharacterList_Contact() Failed to find enumCharacterList ID as valid Activity Selector Character") NET_NL()
		NET_PRINT("      enumCharacterList ID: ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[paramCharacterListID].label)) NET_NL()
		SCRIPT_ASSERT("Get_Hash_Of_enumCharacterList_Contact(): Failed to find enumCharacterList ID as valid Activity Selector Character. Add to SWITCH statement. Returning 0. Tell Keith.") 
	#ENDIF
	
	RETURN (0)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get enumCharacterListID from HASH value for a valid Activity Selector character
//
// INPUT PARAMS:			paramContactHash			A Hash Value representing an enumCharacterList ID
// RETURN VALUE:			enumCharacterList			The enumCharacterList ID for the Hash value, or NO_CHARACTER
FUNC enumCharacterList Get_enumCharacterList_From_Contact_Hash(INT paramContactHash)

	IF (paramContactHash = 0)
		RETURN (NO_CHARACTER)
	ENDIF

	// To save having to go through the whole enumCharacterList, loop through the Heist Contact and convert where appropriate
	
	INT					heistContactIdLoop	= 0
	enumCharacterList	theCharacterListID	= NO_CHARACTER
	
	REPEAT MAX_FM_HEIST_CONTACTS heistContactIdLoop
		theCharacterListID = Convert_FM_Heist_ContactID_As_INT_To_CharacterListID(heistContactIdLoop)
		
		IF (paramContactHash = Get_Hash_Of_enumCharacterList_Contact(theCharacterListID))
			RETURN (theCharacterListID)
		ENDIF
	ENDREPEAT
	
	// Not found
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect]: Get_enumCharacterList_From_Contact_Hash() Failed to find contact that matches the Hash value: ") NET_PRINT_INT(paramContactHash) NET_NL()
		SCRIPT_ASSERT("Get_enumCharacterList_From_Contact_Hash(): Failed to find contact that matches the Hash value. New Character needs supported as a valid Contact. Returning NO_CHARACTER. Tell Keith.") 
	#ENDIF
	
	RETURN (NO_CHARACTER)

ENDFUNC




// ===========================================================================================================
//      Cyclic Header safe functions for access to Activity Selector
// ===========================================================================================================

// PURPOSE:	Check if the passed in group array position contains the Contact Mission group
//
// INPUT PARAMS:		paramGroupArrayPos		The position on the Group Array being checked
// RETURN VALUE:		BOOL					TRUE if the passed-in coords represent the Contact Mission Group, FALSE if not
FUNC BOOL Is_This_The_Contact_Mission_Group(INT paramGroupArrayPos)
	RETURN (ARE_VECTORS_ALMOST_EQUAL(Get_Group_Coords_For_Group_Slot(paramGroupArrayPos), MP_CONTACT_MISSION_GENERIC_COORDS))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Contact Mission group
//
// RETURN VALUE:		INT						The position on the Group Array containing Contact Missions
FUNC INT Get_The_Contact_Mission_GroupID()

	INT	tempLoop		= 0
	
	REPEAT MAX_PENDING_ACTIVITIES_GROUPS tempLoop
		IF (Is_This_The_Contact_Mission_Group(tempLoop))
			RETURN (tempLoop)
		ENDIF
	ENDREPEAT
	
	RETURN (ILLEGAL_PENDING_ACTIVITIES_GROUP)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the already stored ContactID for a specific Contact Mission slot
//
// INPUT PARAMS:		paramSlot				Contact Mission slot
// RETURN VALUE:		g_eFMHeistContactIDs	The Contact ID as g_eFMHeistContactIDs
FUNC g_eFMHeistContactIDs Get_Contact_Mission_ContactID_For_Slot(INT paramSlot)
	RETURN (g_sLocalMPCMs[paramSlot].lcmContact)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the already stored ContactID as an enumCharcterList for a specific Contact Mission slot
//
// INPUT PARAMS:		paramSlot				Contact Mission slot
// RETURN VALUE:		enumCharacterList		The Contact ID as enumCharacterList
FUNC enumCharacterList Get_Contact_Mission_ContactID_As_CharacterList_For_Slot(INT paramSlot)
	RETURN (Convert_FM_Heist_ContactID_To_CharacterListID(g_sLocalMPCMs[paramSlot].lcmContact))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Rank for a specific Contact Mission slot
//
// INPUT PARAMS:		paramSlot			Contact Mission slot
// RETURN VALUE:		INT					The Rank
FUNC INT Get_Contact_Mission_Rank_For_Slot(INT paramSlot)
	RETURN (g_sLocalMPCMs[paramSlot].lcmRank)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Played Status for a specific Contact Mission slot
//
// INPUT PARAMS:		paramSlot			Contact Mission slot
// RETURN VALUE:		BOOL				TRUE if played, otherwise FALSE
FUNC BOOL Has_Contact_Mission_Been_Played_For_Slot(INT paramSlot)
	RETURN (g_sLocalMPCMs[paramSlot].lcmPlayed)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the details in the specified slot match the passed-in parameters
//
// INPUT PARAMS:		paramSlot			The Contact Missions slot
//						paramContactID		The ContactID (or NO_CHARACTER to ignore contactID)
//						paramPlayerRank		The player's current rank (passed in to avoid cyclic headers) - For CNC this should be Player's Gang Rank
//						paramRankUse		How Rank should be used when comparing missions
// RETURN VALUE:		BOOL				TRUE if the details match, otherwise FALSE
FUNC BOOL Do_Contact_Missions_Slot_Details_Match(INT paramSlot, enumCharacterList paramContactID, INT paramPlayerRank, g_eContactMissionSelection paramRankUse)

	// Slot in use?
	IF (paramSlot >= g_numLocalMPCMs)
		RETURN FALSE
	ENDIF
	// If previous ranks are to be used during mission selection, then look for played missions up to and including the current player rank
	// If current rank is to be used for mission selection, then look for unplayed missions between the CM Min Rank and the current Player Rank
	INT minRank = 0
	INT maxRank = paramPlayerRank

	SWITCH (paramRankUse)
		CASE CM_RANK_USE_UNPLAYED_BETWEEN_CM_MIN_AND_CURRENT_RANKS
			IF (g_CMsPlayed.cmrmMinRank > paramPlayerRank)
				PRINTLN("[MMM][ContactMission] CM_RANK_USE_UNPLAYED_BETWEEN_CM_MIN_AND_CURRENT_RANKS contact mission rank is greater than current rank, so there are no valid CMs for selection at the moment")
				// ...contact mission rank is greater than current rank, so there are no valid CMs for selection at the moment
				RETURN FALSE
			ENDIF
			
			// Otherwise, gather the min rank details
			minRank = g_CMsPlayed.cmrmMinRank
			BREAK
			
		CASE CM_RANK_USE_NO_PLAYER_RECORD_BELOW_CM_RANK
			minRank = 0
			maxRank = g_CMsPlayed.cmrmMinRank - 1
			
			IF (minRank > maxRank)
				PRINTLN("[MMM][ContactMission] CM_RANK_USE_NO_PLAYER_RECORD_BELOW_CM_RANK contact mission rank is greater than current rank, so there are no valid CMs for selection at the moment")
				// ...contact mission rank is greater than current rank, so there are no valid CMs for selection at the moment
				RETURN FALSE
			ENDIF
			BREAK
			
		CASE CM_RANK_USE_PREVIOUSLY_PLAYED_BELOW_CURRENT_RANK
			minRank = 0
			BREAK
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][ContactMission]: Do_Contact_Missions_Slot_Details_Match(): Unknown Contact Mission Selection ENUM when getting Rank For Compaison.") NET_NL()
				SCRIPT_ASSERT("Do_Contact_Missions_Slot_Details_Match(): Unknown Contact Mission Selection ENUM when getting Rank For Comparison. Add to SWITCH. Tell Keith.")
			#ENDIF
			
			RETURN FALSE
	ENDSWITCH

	// What to search for?
	BOOL matchContact = FALSE
	IF (paramContactID != NO_CHARACTER)
		matchContact	= TRUE
	ENDIF
	
	// Check for matching contact (if required)
	IF (matchContact)
		IF (Get_Contact_Mission_ContactID_As_CharacterList_For_Slot(paramSlot) != paramContactID)
			// ...failed to match contact
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Do Rank Comparison to see if this mission is viable
	INT rankInSlot = Get_Contact_Mission_Rank_For_Slot(paramSlot)

	IF (rankInSlot < minRank)
		RETURN FALSE
	ENDIF
	
	IF (rankInSlot > maxRank)
		RETURN FALSE
	ENDIF
	
	// The details must be a match
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if any Contact Missions match these details
//
// INPUT PARAMS:		paramContactID		The ContactID (or NO_CHARACTER to ignore contactID)
//						paramPlayerRank		The player's current rank (passed in to avoid cyclic headers)
// RETURN VALUE:		BOOL				TRUE if at least one match is found, otherwise FALSE
FUNC BOOL Do_Any_Played_Contact_Missions_Match_These_Details(enumCharacterList paramContactID, INT paramPlayerRank)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Checking if any Played CMs match these details:") NET_NL()
		NET_PRINT("   ")
		IF (paramContactID != NO_CHARACTER)
			NET_PRINT("   [CONTACT: ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[paramContactID].label)) NET_PRINT("]")
		ENDIF
		NET_PRINT("   [RANK INFO: MAX RANK: ") NET_PRINT_INT(paramPlayerRank) NET_PRINT("  MIN RANK: 0")
		NET_NL()
	#ENDIF

	INT tempLoop	= 0
	INT	onHours		= MATC_ALL_DAY
	
	REPEAT MAX_CONTACT_MISSIONS tempLoop
		IF (Do_Contact_Missions_Slot_Details_Match(tempLoop, paramContactID, paramPlayerRank, CM_RANK_USE_PREVIOUSLY_PLAYED_BELOW_CURRENT_RANK))
			// ...found a match, so check if the played status is the required played status
			IF (g_sLocalMPCMs[tempLoop].lcmPlayed)
				// ...mission is played, but ensure it isn't a 'one play'
				IF NOT (g_sLocalMPCMs[tempLoop].lcmOnePlay)
					// ...not a 'one play' mission, so check the available play hours for the mission
					onHours = g_sLocalMPCMs[tempLoop].lcmOnHours
					IF (Can_Invite_Be_Displayed_At_This_Hour(onHours))
					AND (Will_Invite_Be_Ok_To_Display_Next_Hour(onHours))
						#IF IS_DEBUG_BUILD
							NET_PRINT("      - FOUND AT LEAST ONE MISSION THAT MATCHES THE REQUIREMENTS (Played, Not 'One Play', Within 'On' Hours)")
							NET_NL()
						#ENDIF
	
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// No Match
	#IF IS_DEBUG_BUILD
		NET_PRINT("      - NO MISSIONS MATCH THE REQUIREMENTS")
		NET_NL()
	#ENDIF
	
	RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      General Heist Public Access Functions
// ===========================================================================================================

// PURPOSE:	Return whether or not a Heist is available or ongoing at the apartment (even if unstarted)
//
// RETURN VALUE:			BOOL		TRUE if a heist is available, otherwise FALSE
//
// NOTES:	KGM 7/11/14 [BUG 2107780]: Added for Conor so he can colour the apartment green
FUNC BOOL Is_There_A_Heist_Available_In_Apartment()

	SWITCH (g_sLocalMPHeistControl.lhcStage)
		// Not Available
		CASE HEIST_CLIENT_STAGE_NO_HEIST
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY
		CASE HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE
		CASE HEIST_CLIENT_STAGE_WAIT_UNTIL_COMMS_ALLOWED
		CASE HEIST_CLIENT_STAGE_REQUEST_COMMS
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_COMMS_TO_END
		CASE HEIST_CLIENT_STAGE_HEIST_VARIABLES_RESET
		CASE HEIST_CLIENT_STAGE_HEIST_ERROR
			RETURN FALSE
		
		// Available
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT
		CASE HEIST_CLIENT_STAGE_DISPLAY_HEIST_COST
		CASE HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_COMPLETE
			RETURN TRUE
	ENDSWITCH
	
	PRINTLN(".KGM [Heist]: Is_There_A_Heist_Available_In_Apartment() - ERROR: Unknown Heist Stage. Add to SWITCH. Returning FALSE.")
	SCRIPT_ASSERT("Is_There_A_Heist_Available_In_Apartment() - ERROR: Unknown Heist Stage. Add to SWITCH. Returning FALSE. Tell Keith.")
	RETURN FALSE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	A specific instance of the above function to be used for displaying hte 'H' blip inthe apartment
//
// RETURN VALUE:			BOOL		TRUE if the blip should be displayed, otherwise FALSE
//
// NOTES:	KGM 13/2/15 [BUG 2224641]: Added for James so prevent the heist blip being displayed whin inthe 'Heist Available' state (ie; sitting on seat - to fix an HTTP-Interceptor bug)
FUNC BOOL Should_The_Heist_Blip_Display_In_Apartment()

	SWITCH (g_sLocalMPHeistControl.lhcStage)
		// Don't display it
		CASE HEIST_CLIENT_STAGE_NO_HEIST
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY
		CASE HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE
		CASE HEIST_CLIENT_STAGE_WAIT_UNTIL_COMMS_ALLOWED
		CASE HEIST_CLIENT_STAGE_REQUEST_COMMS
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_COMMS_TO_END
		CASE HEIST_CLIENT_STAGE_HEIST_VARIABLES_RESET
		CASE HEIST_CLIENT_STAGE_HEIST_ERROR
		// Remove these because of BUG:2224641 (in these states the player is in the corona or transitioning to the intro cutscene)
		CASE HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE
			RETURN FALSE
		
		// Display it
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT
		CASE HEIST_CLIENT_STAGE_DISPLAY_HEIST_COST
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_COMPLETE
			RETURN TRUE
	ENDSWITCH
	
	PRINTLN(".KGM [Heist]: Should_The_Heist_Blip_Display_In_Apartment() - ERROR: Unknown Heist Stage. Add to SWITCH. Returning FALSE.")
	SCRIPT_ASSERT("Should_The_Heist_Blip_Display_In_Apartment() - ERROR: Unknown Heist Stage. Add to SWITCH. Returning FALSE. Tell Keith.")
	RETURN FALSE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Returns whether or not a Lester Heist is active or available in apartment
//
// RETURN VALUE:			BOOL		TRUE if a Lester heist is available, otherwise FALSE
//
// NOTES:	KGM 6/1/15 [BUG 2174548]: Added for Dave W so that unimportant Lester phonecalls can be ignored while a Lester Heist is ongoing
FUNC BOOL Is_A_Lester_Heist_Strand_Active()

	IF NOT (Is_There_A_Heist_Available_In_Apartment())
		RETURN FALSE
	ENDIF
	
	// There is a Heist active or available - is it a Lester Heist (based on Cutscene character)?
	IF (g_sLocalMPHeistControl.lhcHashHeistRCID = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job)
	OR (g_sLocalMPHeistControl.lhcHashHeistRCID = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job)
		RETURN TRUE
	ENDIF
	
	// Not a Lester Heist
	RETURN FALSE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Requests cancelling of the currently active Heist Strand.
PROC Cancel_Current_Heist_Strand()
	
	IF NOT (Is_There_A_Heist_Available_In_Apartment())
		PRINTLN(".KGM [Heist][Cancel]: Cancel_Current_Heist_Strand() - There is no active Heist available in the player's apartment. IGNORING REQUEST.")
		DEBUG_PRINTCALLSTACK()
		
		EXIT
	ENDIF

	g_cancelHeistStrandByLester_KeithStuff = TRUE
	PRINTLN(".KGM [Heist]: Cancel_Current_Heist_Strand() - g_cancelHeistStrandByLester_KeithStuff = TRUE")
	DEBUG_PRINTCALLSTACK()
	
	// NOTE: Because the ABANDON_CURRENT_HEIST_STRAND() needs called and this can't be done in the routine that checks the above flag, a second flag has been added
	//			that gets set by the above flag checking routine. g_cleanupHeistStrandStats_AlastairStuff. THIS SHOULD NOT GET SET TRUE HERE.
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the 'Cancel Heist Strand' flags
PROC Clear_Cancel_Current_Heist_Strand_Flags()

	IF NOT (g_cancelHeistStrandByLester_KeithStuff)
	AND NOT (g_cleanupHeistStrandStats_AlastairStuff)
		EXIT
	ENDIF

	g_cancelHeistStrandByLester_KeithStuff = FALSE
	PRINTLN(".KGM [Heist][Cancel]: Clear_Cancel_Current_Heist_Strand_Flags() - g_cancelHeistStrandByLester_KeithStuff = FALSE")
	
	g_cleanupHeistStrandStats_AlastairStuff	= FALSE
	PRINTLN(".KGM [Heist][Cancel]: Clear_Cancel_Current_Heist_Strand_Flags() - g_cleanupHeistStrandStats_AlastairStuff = FALSE")
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the Heist Strand has been cancelled by phoning Lester.
//
// RETURN VALUE:			BOOL		TRUE if an active Heist Strand needs to cancelled, otherwise FALSE
FUNC BOOL Has_Current_Heist_Strand_Been_Cancelled_By_Player()
	RETURN (g_cancelHeistStrandByLester_KeithStuff)
ENDFUNC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Local Heist Strand Selection and Replay Routines
// ===========================================================================================================

// PURPOSE:	Debug Output the current recently completed Heist Strands delay array
#IF IS_DEBUG_BUILD
PROC Debug_Output_Active_Heist_Replay_Delays()
	
	PRINTLN("...KGM MP [ActSelect][Heist]: Current Contents of Recent Heist Delay array. Current Time: ", GET_GAME_TIMER())

	INT tempLoop = 0
	
	REPEAT MAX_HEISTS_WITH_REPLAY_DELAYS tempLoop
		IF (g_sHeistReplayDelay[tempLoop].hrdDelayTimeout = 0)
			PRINTLN("...KGM MP [ActSelect][Heist]:    ", tempLoop, ":  UNUSED")
		ELSE
			PRINTLN("...KGM MP [ActSelect][Heist]:    ", tempLoop, ":  RCID Hash: ", g_sHeistReplayDelay[tempLoop].hrdFinaleHashRCID, "  Timeout: ", g_sHeistReplayDelay[tempLoop].hrdDelayTimeout)
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF	// IS_DEBUG_BUILD
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the details of the current Heist
PROC Check_For_Heist_Replay_Delay_Timeouts()

	INT	tempLoop	= 0
	INT	gameTimer	= GET_GAME_TIMER()
	
	#IF IS_DEBUG_BUILD
	BOOL replayDelayRemoved = FALSE
	#ENDIF
	
	REPEAT MAX_HEISTS_WITH_REPLAY_DELAYS tempLoop
		IF (g_sHeistReplayDelay[tempLoop].hrdDelayTimeout != 0)
			IF (gameTimer > g_sHeistReplayDelay[tempLoop].hrdDelayTimeout)
				// ...timeout, so clear this array variables
				PRINTLN("...KGM MP [ActSelect][Heist]: Recent Heist. Delay timout expired. FinaleHashRCID: ", g_sHeistReplayDelay[tempLoop].hrdFinaleHashRCID)
				g_sHeistReplayDelay[tempLoop].hrdDelayTimeout	= 0
				g_sHeistReplayDelay[tempLoop].hrdFinaleHashRCID	= 0
				
				#IF IS_DEBUG_BUILD
					replayDelayRemoved = TRUE
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF (replayDelayRemoved)
			Debug_Output_Active_Heist_Replay_Delays()
		ENDIF
	#ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the details of the current Heist
//
// RETURN VALUE:			INT		The free slot - this should always return a value
FUNC INT Get_Free_Slot_For_New_Heist_Replay_Delay()

	INT	tempLoop	= 0
	
	REPEAT MAX_HEISTS_WITH_REPLAY_DELAYS tempLoop
		IF (g_sHeistReplayDelay[tempLoop].hrdDelayTimeout = 0)
			// Found a free slot
			RETURN tempLoop
		ENDIF
	ENDREPEAT
	
	// Haven't found a free slot, so use the one nearest to timeout
	INT closestTimeout		= g_sHeistReplayDelay[0].hrdDelayTimeout
	INT closestTimeoutSlot	= 0
	
	REPEAT MAX_HEISTS_WITH_REPLAY_DELAYS tempLoop
		IF (g_sHeistReplayDelay[tempLoop].hrdDelayTimeout < closestTimeout)
			closestTimeout 		= g_sHeistReplayDelay[tempLoop].hrdDelayTimeout
			closestTimeoutSlot	= tempLoop
		ENDIF
	ENDREPEAT
	
	PRINTLN("...KGM MP [ActSelect][Heist]: Freeing up oldest completed Heist delay slot. FinaleHashRCID: ", g_sHeistReplayDelay[closestTimeoutSlot].hrdFinaleHashRCID)
	g_sHeistReplayDelay[closestTimeoutSlot].hrdDelayTimeout		= 0
	g_sHeistReplayDelay[closestTimeoutSlot].hrdFinaleHashRCID	= 0
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Active_Heist_Replay_Delays()
	#ENDIF
	
	RETURN closestTimeoutSlot

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the details of the current Heist
PROC Set_Replay_Delay_For_Completed_Heist_Strand(INT paramFinaleHashRCID)

	// First, check for any timeouts to clear space on the array
	Check_For_Heist_Replay_Delay_Timeouts()
	
	// Find a free space on the array
	INT freeSlot = Get_Free_Slot_For_New_Heist_Replay_Delay()
	
	// Fill the details
	INT	gameTimer = GET_GAME_TIMER()
	
	g_sHeistReplayDelay[freeSlot].hrdDelayTimeout	= gameTimer + HEIST_REPLAY_DELAY_TIMEOUT_msec
	g_sHeistReplayDelay[freeSlot].hrdFinaleHashRCID	= paramFinaleHashRCID
	
	#IF IS_DEBUG_BUILD
		PRINTLN("...KGM MP [ActSelect][Heist]: Added Heist Replay Delay. FinaleHashRCID: ", g_sHeistReplayDelay[freeSlot].hrdFinaleHashRCID, "   Timeout: ", g_sHeistReplayDelay[freeSlot].hrdDelayTimeout)
		Debug_Output_Active_Heist_Replay_Delays()
	#ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To check if a Heist Strand has been recently completed
//
// INPUT PARAMS:			paramArrayPos		The array position within the Local Heist Data array
// RETURN VALUE:			BOOL				TRUE if recently completed, otherwise FALSE
FUNC BOOL Is_Heist_Strand_Recently_Completed(INT paramArrayPos)

	// Get the HashRCID
	INT finaleHashRCID = g_sLocalMPHeists[paramArrayPos].lhsHashFinaleRCID
	
	// Search for this on the replay array
	INT tempLoop = 0
	
	REPEAT MAX_HEISTS_WITH_REPLAY_DELAYS tempLoop
		IF (g_sHeistReplayDelay[tempLoop].hrdFinaleHashRCID = finaleHashRCID)
			// Found it - clear up if it has timed out
			IF (GET_GAME_TIMER() > g_sHeistReplayDelay[tempLoop].hrdDelayTimeout)
				// ...timeout, so clear this array variables
				PRINTLN("...KGM MP [ActSelect][Heist]: Completed Check found recenf Heist with Delay timout expired. Removing. FinaleHashRCID: ", g_sHeistReplayDelay[tempLoop].hrdFinaleHashRCID)
				g_sHeistReplayDelay[tempLoop].hrdDelayTimeout	= 0
				g_sHeistReplayDelay[tempLoop].hrdFinaleHashRCID	= 0
				
				#IF IS_DEBUG_BUILD
					Debug_Output_Active_Heist_Replay_Delays()
				#ENDIF
			ELSE
				// ...hasn't timed out, so recently completed
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not recently completed
	RETURN FALSE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get an array of indexes into the local Heist Strands array of replayable Heist Strands matching these details
//
// INPUT PARAMS:			paramContactID			The activity's Contact (or NO_CHARACTER)
//							paramRank				The activity's rank
// REFERENCE PARAMS:		refReplayableHeists		An array of indexes into the local Heist Strands array to indicate which heists have been completed and may be availabel for replay
//							refAvailableBitset		A bitset related to the above array which ndicates which of the entries in the array is avaiable and which are blocked by being recently played
// RETURN VALUE:			INT						The number of available replayable Heist Strands
//
// NOTES:	KGM 9/2/15 [BUG 2229800]: Function use changed from only passing back heists currently replayable, to passing back all completed heists with a bitset indicating
//						which were currently replayable. They wouldn't be currently replayable if recently completed.
FUNC INT Get_Array_Of_Replayable_Heist_Strands(enumCharacterList paramContactID, INT paramPlayerRank, INT &refReplayableHeists[], INT &refAvailableBitset)

	INT tempLoop = 0
	REPEAT MAX_HEIST_STRANDS tempLoop
		refReplayableHeists[tempLoop] = -1
	ENDREPEAT
	refAvailableBitset = ALL_MISSION_CONTROL_BITS_CLEAR
	
	// Ensure there are Heists to choose from
	IF (g_numLocalMPHeists = 0)
		PRINTLN(".KGM [Heist]: Get_Array_Of_Replayable_Heist_Strands() returning NO (There are no Heist Strands stored)")
		RETURN 0
	ENDIF

	// Only allowed if a heist is not being performed
	IF (g_sLocalMPHeistControl.lhcStage != HEIST_CLIENT_STAGE_NO_HEIST)
	AND (g_sLocalMPHeistControl.lhcStage != HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY)
		PRINTLN(".KGM [Heist]: Get_Array_Of_Replayable_Heist_Strands() returning NO - Heist strand is beyond the Initial Delay")
		RETURN 0
	ENDIF
	
	// TODO: Only allow if the player has an apartment suitable for Heists
	//Does_Player_Own_An_Apartment_Suitable_For_Heists()
	
	// TEMP: Only allowed from Lester
	IF (paramContactID != CHAR_LESTER)
		PRINTLN(".KGM [Heist]: Get_Array_Of_Replayable_Heist_Strands() returning NO (TEMP Restriction: Heists should be requested by Lester only)")
		SCRIPT_ASSERT("Get_Array_Of_Replayable_Heist_Strands() - ERROR: TEMP RESTRICTION: Heists should be requested by Lester only")
		RETURN 0
	ENDIF
	
	// TEMP: Ignore Player Rank
	UNUSED_PARAMETER(paramPlayerRank)
	PRINTLN(".KGM [Heist]: Get_Array_Of_Replayable_Heist_Strands() - TEMP: Ignoring player rank")

	PRINTLN(".KGM [Heist]: Finding Replayable Strands: ")
	
	// Check which heists have been completed as Leader and haven't been recently completed
	// KGM 9/2/15 [BUG 2229800]: Now return all completed heists but with a bitflag to indicate currently replayable or not
	INT storePosition = 0
	REPEAT g_numLocalMPHeists tempLoop
		// TODO: Check for the correct contact
		
		IF (g_sLocalMPHeists[tempLoop].lhsCompletedAsLeader)
			// Completed as Leader, so add to the list
			refReplayableHeists[storePosition] = tempLoop
			PRINTLN(".KGM [Heist]: ", tempLoop, " - Found Completed Heist: ", g_sLocalMPHeists[tempLoop].lhsHashFinaleRCID, ". Total: ", (storePosition + 1))
			
			// Set Bit if available (ie: not recently completed)
			IF NOT (Is_Heist_Strand_Recently_Completed(tempLoop))
				SET_BIT(refAvailableBitset, storePosition)
				PRINTLN(".KGM [Heist]:        - AVAILABLE")
			ELSE
				CLEAR_BIT(refAvailableBitset, storePosition)
				PRINTLN(".KGM [Heist]:        - not available (recently completed)")
			ENDIF
			
			storePosition++
		ELSE
			PRINTLN(".KGM [Heist]: ", tempLoop, " - IGNORE: Heist not completed as Leader: ", g_sLocalMPHeists[tempLoop].lhsHashFinaleRCID)
		ENDIF
	ENDREPEAT
	
	INT numReplayable = storePosition
	PRINTLN(".KGM [Heist]: Number of Completed Heists: ", numReplayable)
	
	// Sort the heists into flow order rather than cloud download order
	// KGM 9/2/15 [BUG 2229800]: Also need to swap the currently available bitflags
	IF (numReplayable > 1)
		PRINTLN(".KGM [Heist]: ...Sort the Replayable Heists into Flow Order:")
		INT thisFlowPosition = HEIST_FLOW_ORDER_FLEECA_TUTORIAL
		INT thisFlowHashRCID = 0
		INT checkPosition = 0
		INT swapHold = 0
		BOOL swapBool = FALSE
		storePosition = 0
		BOOL keepChecking = TRUE
		
		// Go through all heists in flow order looking for the next Heist
		REPEAT MAX_HEIST_FLOW_ORDER thisFlowPosition
			// storePosition is where in the return array the nextHeist will be placed
			// checkPosition is the position currently being checked for 'nextHeist' in flow order
			// swapHold is a temporary holding variable used when the contents of the storePosition and checkPosition array entries are being swapped
			// keepChecking is used to indicate the current loop is over and a search for the next heists flow position should begin
			checkPosition = storePosition
			
			// Get the Hash RCID of the Heist Strand being searched for
			// NASTY HARDCODED BIT to replace inability to access Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value()
			thisFlowHashRCID = -1
			SWITCH (thisFlowPosition)
				CASE HEIST_FLOW_ORDER_FLEECA_TUTORIAL		thisFlowHashRCID = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job				BREAK
				CASE HEIST_FLOW_ORDER_PRISON_BREAK			thisFlowHashRCID = g_sMPTUNABLES.iroot_id_HASH_The_Prison_Break				BREAK
				CASE HEIST_FLOW_ORDER_HUMANE_LABS			thisFlowHashRCID = g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid			BREAK
				CASE HEIST_FLOW_ORDER_NARCOTICS				thisFlowHashRCID = g_sMPTUNABLES.iroot_id_HASH_Series_A_Funding				BREAK
				CASE HEIST_FLOW_ORDER_PACIFIC_STANDARD		thisFlowHashRCID = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job		BREAK
			ENDSWITCH
			
			keepChecking = TRUE
			IF (checkPosition >= numReplayable)
			OR (thisFlowHashRCID = -1)
				keepChecking = FALSE
			ENDIF
			
			// continue searching
			WHILE (keepChecking)
				// Does the current check position contain the Heist being searched for?
				// BUG 2189018; Ensure there can't be an array overrun
				IF (refReplayableHeists[checkPosition] != -1)
					IF (thisFlowHashRCID = g_sLocalMPHeists[refReplayableHeists[checkPosition]].lhsHashFinaleRCID)
						// ...found it
						// Swap positions?
						IF (checkPosition != storePosition)
							// Swap the Heist
							swapHold = refReplayableHeists[storePosition]
							refReplayableHeists[storePosition] = refReplayableHeists[checkPosition]
							refReplayableHeists[checkPosition] = swapHold
							// Swap the 'currently available' bitset
							swapBool = IS_BIT_SET(refAvailableBitset, storePosition)
							IF (IS_BIT_SET(refAvailableBitset, checkPosition))
								SET_BIT(refAvailableBitset, storePosition)
							ELSE
								CLEAR_BIT(refAvailableBitset, storePosition)
							ENDIF
							IF (swapBool)
								SET_BIT(refAvailableBitset, checkPosition)
							ELSE
								CLEAR_BIT(refAvailableBitset, checkPosition)
							ENDIF
						ENDIF
						
						// Increase the store position
						storePosition++
						
						// End the search for this heist, move on to next heist
						keepChecking = FALSE
					ENDIF
				ENDIF
				
				IF (keepChecking)
					checkPosition++
					
					IF (checkPosition >= numReplayable)
						keepChecking = FALSE
					ENDIF
				ENDIF
			ENDWHILE
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
			REPEAT numReplayable tempLoop
				IF (IS_BIT_SET(refAvailableBitset, tempLoop))
					PRINTLN(".KGM [Heist]: ...", tempLoop, " - Heist Finale RCID: ", g_sLocalMPHeists[refReplayableHeists[tempLoop]].lhsHashFinaleRCID, " [AVAILABLE]")
				ELSE
					PRINTLN(".KGM [Heist]: ...", tempLoop, " - Heist Finale RCID: ", g_sLocalMPHeists[refReplayableHeists[tempLoop]].lhsHashFinaleRCID, " [not available - recently completed]")
				ENDIF
			ENDREPEAT
		#ENDIF
	ENDIF
	
	RETURN numReplayable
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Randomly chooses a Heist strand matching the specified details
//
// INPUT PARAMS:			paramContactID			The activity's Contact (or NO_CHARACTER)
//							paramRank				The activity's rank
// RETURN VALUE:			INT						The array position on the local Heist Strand array, or ILLEGAL_HEIST_STRAND_SLOT
//
// NOTES:	Setup for Dave to start a replay by phonecall. Moved from net_activity_selector_groups_heists.sch
FUNC INT Get_Random_Heist_Strand_Matching_These_Details(enumCharacterList paramContactID, INT paramPlayerRank)

	// KGM 9/2/15: Because this function is only used by Dave now to find out if any heists should be shown in the replayable phone list, it does a lot of stuff that isn't necessary so could be streamlined
	//				The stramlining should check: if NOT ON A HEIST and ANY HEIST HAS BEEN COMPLETED then RETURN TRUE, otherwise RETURN FALSE
	INT availableHeists[MAX_HEIST_STRANDS]
	
	// KGM 9/2/15 [BUG 2229800]: The available bitset is ignored here because this function is only used by Dave to figure out of a heist is completed or not
	//				(the bitset is used to further specify which ones are currently replayable as opposed to those recently completed)
	INT ignoreAvailableBitset = ALL_MISSION_CONTROL_BITS_CLEAR
	
	INT numReplayableHeists = Get_Array_Of_Replayable_Heist_Strands(paramContactID, paramPlayerRank, availableHeists, ignoreAvailableBitset)
	IF (numReplayableHeists = 0)
		PRINTLN("...KGM MP [ActSelect][Heist]: ...there are no Heists currently available for replay.")
		RETURN ILLEGAL_HEIST_STRAND_SLOT
	ENDIF
	
	// TEMP: Ignore Player Rank
	UNUSED_PARAMETER(paramPlayerRank)
	
	#IF IS_DEBUG_BUILD
		INT tempLoop = 0
		PRINTLN("...KGM MP [ActSelect][Heist]: Number of Replayable Heists: ", numReplayableHeists)
		REPEAT numReplayableHeists tempLoop
			PRINTLN("...KGM MP [ActSelect][Heist]:    ", tempLoop, "   ", g_sLocalMPHeists[availableHeists[tempLoop]].lhsHashFinaleRCID)
		ENDREPEAT
	#ENDIF
	
	INT chosenHeist = GET_RANDOM_INT_IN_RANGE(0, numReplayableHeists)

	PRINTLN("...KGM MP [ActSelect][Heist]: Get_Random_Heist_Strand_Matching_These_Details() - Returning Local Heist Strand: ", availableHeists[chosenHeist], ": ", g_sLocalMPHeists[availableHeists[chosenHeist]].lhsHashFinaleRCID)
	RETURN (availableHeists[chosenHeist])

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Heist Reward active invite details
//
// NOTES:	THIS SHOULDN'T BE AUTO-CLEARED ON ENTERING A SESSION - THE DATA MAY BE NEEDED TO RE-ADD THE INVITE
PROC Clear_Heist_Reward_Active_Invite()
	
	g_structActiveHeistReward emptyActiveInviteReward
	g_sActiveMPHeistReward = emptyActiveInviteReward
	
	PRINTLN(".KGM [Heist]: Clear the Heist Reward Active Invite details")
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the stored corona details, but only on entry from SP or when specifically requested
PROC Clear_My_Stored_Corona_Details()

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [ActSelect][Heist][Corona]: Clear My Stored Corona Details")
	#ENDIF
	
	g_structMyHeistCorona emptyMyCoronaDetails
	
	g_sLocalMPHeistControl.lhcMyCorona	= emptyMyCoronaDetails
	g_sLocalMPHeistControl.lhcMyCorona.mhcContentID	= ""

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the local Heist Control struct
// NOTES:	These should remain while jumping around MP, so only clear if joining from SP.
PROC Clear_Local_Heist_Control()
	
	// KGM 13/11/14 [BUG 2132841]: On re-entry to MP from SP, don't overwrite the Next Heist Delay Timer
	// NOTE: This function is only called on entry from SP, on a console reboot, or on visiting the character selection screen. The last TRUE will have g_resetHeistsOnJoiningMP set to TRUE.
	INT holdCurrentTimeoutValue = g_sLocalMPHeistControl.lhcTimeout
	
	g_structLocalHeistControl emptyLocalHeistControl
	g_sLocalMPHeistControl = emptyLocalHeistControl
	
	// Clear the Cooldown Timeout - used to prevent new Heist Strands to be offered for a short while after playing someone else's heist
	// NOTE: Using the regular game timer since this should remain while changing MP sessions
	g_heistCooldownTimeout = GET_GAME_TIMER()
	
	// Clear the 'cancel Heist Strand' flags
	Clear_Cancel_Current_Heist_Strand_Flags()
	
	// KGM 13/11/14 [BUG 2132841]: On re-entry to MP from SP, don't overwrite the Next Heist Delay Timer
	IF NOT (g_resetHeistsOnJoiningMP)
		// This is a re-entry to MP from SP, so leave the countdown timer as-is and setup the 'Required Initial Delay' to be the remaining delay on the countdown timer
		g_sLocalMPHeistControl.lhcTimeout = holdCurrentTimeoutValue
		
		INT delayRemaining = holdCurrentTimeoutValue - GET_GAME_TIMER()
		PRINTLN("..KGM [Heist]: Clear_Local_Heist_Control() - Re-entering MP from SP. Next Heist Delay Remaining = ", delayRemaining, " msec [THIS WILL BE IGNORED IF HEIST ACTIVE].")
		
		g_sLocalMPHeistControl.lhcRequiredInitialDelay = delayRemaining
		
		// If the delay remaining is very short or negative, then make it a small positive value
		// NOTE: If there is already a Heist active then this value will be ignored becuase it won't be needed
		IF (delayRemaining < HEIST_CONTROL_INTRO_TO_TUTORIAL_DELAY_msec)
			g_sLocalMPHeistControl.lhcRequiredInitialDelay = HEIST_CONTROL_INTRO_TO_TUTORIAL_DELAY_msec
			PRINTLN(".KGM [Heist]: Clear_Local_Heist_Control() - BUT delay too short. Setting to ", g_sLocalMPHeistControl.lhcRequiredInitialDelay, " msec.")
		ENDIF
	ELSE
		PRINTLN(".KGM [Heist]: Clear_Local_Heist_Control() - g_resetHeistsOnJoiningMP = TRUE. Doing full variable clearout.")
	ENDIF
	
	Clear_My_Stored_Corona_Details()
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there is an active Heist corona
//
// RETURN VALUE:		BOOL			TRUE if a Heist corona is registered with Matc, FALSE if not
FUNC BOOL Does_This_Player_Have_An_Active_Heist_Corona()
	RETURN (g_sLocalMPHeistControl.lhcMyCorona.mhcMatcID != ILLEGAL_AT_COORDS_ID)
ENDFUNC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Local Heist Cross-session variables access
// ===========================================================================================================

// PURPOSE:		Set the Cross-session data that will be transmitted to other players
//
// INPUT PARAMS:			paramApartmentID		The apartmentID the heist is being setup in
//							paramIsIntroCutscene	TRUE if this corona is for the intro cutscene, otherwise FALSE
//							paramIsMidStrandCut		TRUE if this corona is for the mid-strand cutscene, otherwise FALSE
//							paramIsTutorialCut		TRUE if this corona is for the Tutorial cutscene, otherwise FALSE
PROC Store_Heist_Data_Ready_For_Cross_Session_Transmission(INT paramApartmentID, BOOL paramIsIntroCutscene, BOOL paramIsMidStrandCut, BOOL paramIsTutorialCut)

	IF (paramApartmentID = NO_OWNED_SAFEHOUSE)
		PRINTLN(".KGM [ActSelect][Heist][HCorona]: ERROR: ApartmentID is being stored for Cross-Session transmission as NO_OWNED_SAFEHOUSE.")
		SCRIPT_ASSERT("Store_Heist_Data_Ready_For_Cross_Session_Transmission. Illegal ApartmentID. Tell Keith")
		EXIT
	ENDIF
	
	IF (paramIsIntroCutscene)
	AND (paramIsMidStrandCut)
		PRINTLN(".KGM [ActSelect][Heist][HCorona]: ERROR: Data is being set as both INTRO cutscene and MID-STRAND cutscene.")
		SCRIPT_ASSERT("Store_Heist_Data_Ready_For_Cross_Session_Transmission. ERROR: Data is being set as both INTRO cutscene and MID-STRAND cutscene. Tell Keith")
		EXIT
	ENDIF
	
	IF (paramIsTutorialCut)
		IF NOT (paramIsIntroCutscene)
			PRINTLN(".KGM [ActSelect][Heist][HCorona]: ERROR: Tutorial Cutscene data being sent without INTRO Cutscene being set.")
			SCRIPT_ASSERT("Store_Heist_Data_Ready_For_Cross_Session_Transmission. ERROR: Tutorial Cutscene data being sent without INTRO Cutscene being set. Tell Keith")
			EXIT
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
		NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ")
		NET_PRINT_TIME()
		NET_PRINT("Storing Data Ready for Cross-Session Transmission. ApartmentID: ")
		NET_PRINT_INT(paramApartmentID)
		IF (paramIsIntroCutscene)
			NET_PRINT("  [INTRO CUTSCENE CORONA]")
		ENDIF
		IF (paramIsMidStrandCut)
			NET_PRINT("  [MID-STRAND CUTSCENE CORONA]")
		ENDIF
		IF (paramIsTutorialCut)
			NET_PRINT("  TUTORIAL CUTSCENE CORONA]")
		ENDIF
		NET_NL()
	#ENDIF
	
	g_sLocalMPHeistCSData.lhcsDataToBeTransmittedToOthers = paramApartmentID
	
	IF (paramIsIntroCutscene)
		// The Intro Cutscene gets the generic 'cutscene' additional value
		g_sLocalMPHeistCSData.lhcsDataToBeTransmittedToOthers += HEIST_CUTSCENE_CORONA_CS_DATA_VALUE
	ENDIF
	
	IF (paramIsMidStrandCut)
		// The MidStrand cutscene gets the generic 'cutscene' additional value + the 'mid-strand' additional value 
		g_sLocalMPHeistCSData.lhcsDataToBeTransmittedToOthers += HEIST_CUTSCENE_CORONA_CS_DATA_VALUE
		g_sLocalMPHeistCSData.lhcsDataToBeTransmittedToOthers += HEIST_MID_STRAND_CUTSCENE_CORONA_CS_DATA_VALUE
	ENDIF
	
	IF (paramIsTutorialCut)
		// The Tutorial cutscene adds on the 'tutorial' additional value - the generic 'cutscene' additional value will already have been added above 
		g_sLocalMPHeistCSData.lhcsDataToBeTransmittedToOthers += HEIST_TUTORIAL_CUTSCENE_CORONA_CS_DATA_VALUE
	ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:		Clear the Cross-session data that will be transmitted to other players
PROC Clear_Heist_Data_Stored_For_Cross_Session_Transmission()

	#IF IS_DEBUG_BUILD
		NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ")
		NET_PRINT_TIME()
		NET_PRINT("Clear Heist Cross-Session data being transmitted to other players.")
		NET_NL()
	#ENDIF
	
	g_sLocalMPHeistCSData.lhcsDataToBeTransmittedToOthers = EMPTY_HEIST_CROSS_SESSION_DATA

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// INPUT PARAMS:			paramCSDataReceived		The data received by cross-session tranmission
PROC Store_Heist_Data_Received_By_Cross_Session_Transmission(INT paramCSDataReceived)

	INT thisApartmentID			= NO_OWNED_SAFEHOUSE
	BOOL isCutsceneCorona		= FALSE
	BOOL isMidStrandCutCorona	= FALSE
	BOOL isTutorialCutCorona	= FALSE
	
	IF (paramCSDataReceived >= HEIST_SIMPLE_INTERIOR)
		paramCSDataReceived -= HEIST_SIMPLE_INTERIOR
		g_sTransitionSessionOptions.iSMPLIntLocation = paramCSDataReceived
		PRINTLN("[TS] SMPL_INT_LOCATION - paramCSDataReceived >= HEIST_SIMPLE_INTERIOR g_sTransitionSessionOptions.iSMPLIntLocation = ", g_sTransitionSessionOptions.iSMPLIntLocation)
		EXIT
	ENDIF
	
	// Is this a 'Tutorial' cutscene?
	IF (paramCSDataReceived >= HEIST_TUTORIAL_CUTSCENE_CORONA_CS_DATA_VALUE)
		isTutorialCutCorona = TRUE
		paramCSDataReceived -= HEIST_TUTORIAL_CUTSCENE_CORONA_CS_DATA_VALUE
	ENDIF
	
	// Is this a 'Mid-Strand' cutscene?
	IF (paramCSDataReceived >= HEIST_MID_STRAND_CUTSCENE_CORONA_CS_DATA_VALUE)
		isMidStrandCutCorona = TRUE
		paramCSDataReceived -= HEIST_MID_STRAND_CUTSCENE_CORONA_CS_DATA_VALUE
	ENDIF
	
	// Is this a generic cutscene (ie: both Intro and MidStrand cutscenes will contain this value)
	IF (paramCSDataReceived >= HEIST_CUTSCENE_CORONA_CS_DATA_VALUE)
		isCutsceneCorona = TRUE
		paramCSDataReceived -= HEIST_CUTSCENE_CORONA_CS_DATA_VALUE
	ENDIF
	
	thisApartmentID = paramCSDataReceived
	
	IF (thisApartmentID = NO_OWNED_SAFEHOUSE)
	OR (thisApartmentID < 0)
		PRINTLN(".KGM [ActSelect][Heist][HCorona]: ERROR: ApartmentID received by Cross-Session transmission is NO_OWNED_SAFEHOUSE. Ignoring.")
		SCRIPT_ASSERT("Store_Heist_Data_Received_By_Cross_Session_Transmission. Illegal ApartmentID. Tell Keith")
		EXIT
	ENDIF

	#IF IS_DEBUG_BUILD
		NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ")
		NET_PRINT_TIME()
		NET_PRINT("Storing Data Received By Cross-Session Transmission. ApartmentID: ")
		NET_PRINT_INT(thisApartmentID)
		IF (isCutsceneCorona)
			NET_PRINT("  [CUTSCENE CORONA]")
		ENDIF
		IF (isMidStrandCutCorona)
			NET_PRINT("  [for MID-STRAND CUTSCENE]")
		ENDIF
		IF (isTutorialCutCorona)
			NET_PRINT("  [for TUTORIAL CUTSCENE]")
		ENDIF
		NET_NL()
	#ENDIF
	
	// NOTE: An Intro Cutscene and an mid-strand cutscene both need to set the 'cutscene' flag - this is needed within FMMC_Launcher
	//		 However, the mid-strand cutscene will then also set the 'mid-strand' flag
	g_sLocalMPHeistCSData.lhcsHeistApartmentReceivedCS		= thisApartmentID
	g_sLocalMPHeistCSData.lhcsIsHeistCutsceneCoronaCS		= isCutsceneCorona
	g_sLocalMPHeistCSData.lhcsIsHeistMidStrandCutCoronaCS	= isMidStrandCutCorona
	g_sLocalMPHeistCSData.lhcsIsHeistTutorialCutCoronaCS	= isTutorialCutCorona

ENDPROC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Local Routines To Store Active Heist Coronas For Other Players - CONSOLE LOG OUTPUT FUNCTIONS
// ===========================================================================================================

#IF IS_DEBUG_BUILD		// KEEP THIS ONE WHEN REMOVING HEIST FEATURE, IT'S A DEBUG FUNCTION
// PURPOSE:	Output one Other Players Active Heist Coronas data to the console log
//
// INPUT PARAMS:			paramArrayPos			The array position within the Other Players Active Heists data
PROC Debug_Output_One_Other_Players_Active_Heist_Corona(INT paramArrayPos)

	PRINTSTRING("        ")
	PRINTINT(paramArrayPos)
	PRINTSTRING(" [ID:") PRINTINT(g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcRegID) PRINTSTRING("]")
	PRINTSTRING(" -  ContentID: ")
	PRINTSTRING(g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcMissionIdData.idCloudFilename)
	PRINTSTRING("  [") PRINTVECTOR(g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcCoords) PRINTSTRING("]")
	PRINTSTRING("  [") PRINTSTRING(GET_PLAYER_NAME(g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcPlayerID)) PRINTSTRING("]")
	IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcUniqueIDFromInvite != NO_UNIQUE_ID)
		PRINTSTRING("  [Invite Accepted, UniqueID: ") PRINTINT(g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcUniqueIDFromInvite) PRINTSTRING("]")
	ENDIF
	IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcMarkedForDelete)
		PRINTSTRING(" -  MARKED FOR DELETE")
	ENDIF
	PRINTNL()
		
ENDPROC
	// FEATURE_HEIST_PLANNING
#ENDIF	// IS_DEBUG_BUILD - THIS STAYS, IT'S A DEBUG FUNCTION


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD		// KEEP THIS ONE WHEN REMOVING HEIST FEATURE, IT'S A DEBUG FUNCTION
// PURPOSE:	Output all Other Players Active Heist Coronas data to the console log
PROC Debug_Output_All_Other_Player_Active_Heist_Coronas()

	INT tempLoop = 0

	REPEAT g_sOtherMPHeistCoronas.ohccNumOtherCoronas tempLoop
		Debug_Output_One_Other_Players_Active_Heist_Corona(tempLoop)
	ENDREPEAT
	
	PRINTSTRING("        IN USE: ")
	PRINTINT(g_sOtherMPHeistCoronas.ohccNumOtherCoronas)
	PRINTSTRING(" from ")
	PRINTINT(NUM_OTHER_HEIST_CORONAS)
	PRINTNL()
		
ENDPROC
	// FEATURE_HEIST_PLANNING
#ENDIF	// IS_DEBUG_BUILD - THIS STAYS, IT'S A DEBUG FUNCTION




// ===========================================================================================================
//      Local Routines To Store Active Heist Coronas For Other Players
// ===========================================================================================================

// PURPOSE:	Mark another player's active Heist corona data for deletion
//
// INPUT PARAMS:			paramHeistRegID			The RegID of the active corona being deleted
PROC Delete_Other_Player_Active_Heist_Corona(INT paramHeistRegID)

	#IF IS_DEBUG_BUILD
		PRINTSTRING("...KGM MP [ActSelect][Heist][HCorona]:")
		NET_PRINT_TIME()
		PRINTSTRING("Mark another player's Active Heist Corona for Delete: HeistRegID: ")
		PRINTINT(paramHeistRegID)
		PRINTNL()
	#ENDIF

	INT tempLoop = 0
	
	REPEAT g_sOtherMPHeistCoronas.ohccNumOtherCoronas tempLoop
		IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcRegID = paramHeistRegID)
			// ...found it
			g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcMarkedForDelete	= TRUE
			
			#IF IS_DEBUG_BUILD
				PRINTSTRING("...KGM MP [ActSelect][Heist][HCorona]: Found Heist Corona To Be Marked For Delete. Details:") PRINTNL()
				Debug_Output_One_Other_Players_Active_Heist_Corona(tempLoop)
			#ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: FAILED TO FIND ACTIVE HEIST CORONA WITH HEISTREGID: ", paramHeistRegID)
		PRINTSTRING("...KGM MP [ActSelect][Heist][HCorona]: Current Array of Other Players Coronas Details:") PRINTNL()
		Debug_Output_All_Other_Player_Active_Heist_Coronas()
	#ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the data required to allow the display of another player's Heist corona only when this player is in the corona player's apartment
//
// INPUT PARAMS:			paramCoords					The corona position
//							paramLeaderAsBit			The PlayerID of the leader as a bit
//							paramForHeistIntroCut		TRUE if this corona is for the Heist Intro cutscene only, otherwise FALSE
//							paramForHeistMidStrandCut	TRUE if this corona is for the Heist MidStrand cutscene only, otherwise FALSE
//							paramForHeistTutorialCut	TRUE if this corona is for the Heist Tutorial cutscene, otherwise FALSE
// REFERENCE PARAMS:		refMissionIdData			The MissionId Data
// RETURN VALUE:			INT							The HeistCoronaRegID (used for identification for deletion), or ILLEGAL_HEIST_CORONA_REG_ID if an error
FUNC INT Register_Other_Player_Active_Heist_Corona(MP_MISSION_ID_DATA &refMissionIdData, VECTOR paramCoords, INT paramLeaderAsBit, BOOL paramForHeistIntroCut, BOOL paramForHeistMidStrandCut, BOOL paramForHeistTutorialCut)

	// Shouldn't be recording other player's heists for an activity session (they should get setup by the transition routines in FMMC_Launcher)
	IF (NETWORK_IS_ACTIVITY_SESSION())
// KGM 25/7/14: This used to signify a problem, but this assert has never fired and now the first Prep Mission after seeing the Intro Cutscene will get setup while in an
//				Activity Session. I'm going to try the 'easy' route of removing this check altogether, but we may need to refine it to allow this only for the first
//				Prep Mission after the Intro Cutscene if there is a problem.
//		PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: Register_Other_Player_Active_Heist_Corona() ERROR: Activity Session - Heist should already be setup")
//		SCRIPT_ASSERT("Register_Other_Player_Active_Heist_Corona(): Activity Session - Heist should already be setup. Tell Keith.")
//		
//		RETURN ILLEGAL_HEIST_CORONA_REG_ID
		PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: Register_Other_Player_Active_Heist_Corona() Activity Session already setup. Allowing this on the assumption it is the first Heist Prep Mission after the Intro Cutscene.")
	ENDIF

	// Find Leader PlayerID from Bitset
	PLAYER_INDEX	theLeader	= INVALID_PLAYER_INDEX()
	INT				thisBit		= 0
	BOOL			foundLeader	= FALSE
	
	REPEAT NUM_NETWORK_PLAYERS thisBit
		IF (IS_BIT_SET(paramLeaderAsBit, thisBit))
			IF (foundLeader)
				// ...We've already found the Leader - the data from the Shared Mission routines shouldn't include multiple player bits - only the Leader should Share the mission data for heists
				PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: Register_Other_Player_Active_Heist_Corona() ERROR: Already found a player bit as Leader - shouldn't be a second player bit")
				SCRIPT_ASSERT("Register_Other_Player_Active_Heist_Corona() - Multiple Player Bits passed in as Leader. See Console Log. Tell Keith.")
				
				RETURN ILLEGAL_HEIST_CORONA_REG_ID
			ELSE
				// ...we haven't found the Leader, so make sure the Player is ok
				theLeader = INT_TO_PLAYERINDEX(thisBit)
				IF NOT (IS_NET_PLAYER_OK(theLeader, FALSE))
					PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: Register_Other_Player_Active_Heist_Corona() ERROR: Leader is not OK - ditching this request.")
					
					RETURN ILLEGAL_HEIST_CORONA_REG_ID
				ENDIF

				// KGM 14/9/14 [BUG: 2018528]: Don't allow a mission shared by this player to be setup as an 'other player' heist
				IF (theLeader = PLAYER_ID())
					PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: Register_Other_Player_Active_Heist_Corona() ERROR: Leader is local player - ditching this request.")
					SCRIPT_ASSERT("Register_Other_Player_Active_Heist_Corona() Leader for an 'other player' heist is Local Player. Add bug for Keith to take a look.")
					
					RETURN ILLEGAL_HEIST_CORONA_REG_ID
				ENDIF
				
				// ...everything ok, so we've found our Leader
				foundLeader = TRUE
			ENDIF
		ENDIF
	ENDREPEAT

	#IF IS_DEBUG_BUILD
		PRINTSTRING("...KGM MP [ActSelect][Heist][HCorona]:")
		NET_PRINT_TIME()
		PRINTSTRING("Register another player's Heist: ContentID: ")
		PRINTSTRING(refMissionIdData.idCloudFilename)
		PRINTNL()
		PRINTSTRING("...KGM MP [ActSelect][Heist][HCorona]:           [") PRINTVECTOR(paramCoords) PRINTSTRING("]")
		IF (paramForHeistIntroCut)
			PRINTSTRING(" [FOR HEIST INTRO CUT]")
		ENDIF
		IF (paramForHeistMidStrandCut)
			PRINTSTRING(" [FOR HEIST MID-STRAND CUT]")
		ENDIF
		IF (paramForHeistTutorialCut)
			PRINTSTRING(" [FOR HEIST TUTORIAL CUT]")
		ENDIF
		PRINTSTRING(" [LEADER: ") PRINTSTRING(GET_PLAYER_NAME(theLeader)) PRINTSTRING("]")
		PRINTSTRING(" [ID: ") PRINTINT(g_sOtherMPHeistCoronas.ohccNextHeistCoronaRegID) PRINTSTRING("]")
		PRINTNL()
	#ENDIF
	
	IF NOT (foundLeader)
		PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: Register_Other_Player_Active_Heist_Corona() ERROR: The Leader hasn't been found. Not storing Other Player's corona.")
		RETURN (ILLEGAL_HEIST_CORONA_REG_ID)
	ENDIF
	
	// This Leader shouldn't already have Heist Corona details stored - they can only be in one corona at a time
	// KGM 25/8/14 {BUG 1956292]: There is a possibility of a short-term crossover if the Leader backs out of one and quickly launches another - now forcing the original to cleanup
	INT tempLoop = 0
	REPEAT g_sOtherMPHeistCoronas.ohccNumOtherCoronas tempLoop
		IF (theLeader = g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcPlayerID)
			PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: Register_Other_Player_Active_Heist_Corona() This Leader already has Heist Corona stored. RegID: ", g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcRegID)
			PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: ...Forcing the Leader's original Heist Corona to clean up (the 'real' cleanup call should occur soon anyway)")
			
			Delete_Other_Player_Active_Heist_Corona(g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcRegID)
		ENDIF
	ENDREPEAT
	
	// Is there room on the array for another entry?
	IF (g_sOtherMPHeistCoronas.ohccNumOtherCoronas >= NUM_OTHER_HEIST_CORONAS)
		PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: Register_Other_Player_Active_Heist_Corona() ERROR: The array is full")
		SCRIPT_ASSERT("Register_Other_Player_Active_Heist_Corona(): The array is full. Tell Keith.")
		
		RETURN ILLEGAL_HEIST_CORONA_REG_ID
	ENDIF
	
	// Store the details
	INT newSlot = g_sOtherMPHeistCoronas.ohccNumOtherCoronas
	INT nextID	= g_sOtherMPHeistCoronas.ohccNextHeistCoronaRegID
	
	g_sOtherMPHeistCoronas.ohccOtherCoronas[newSlot].ohcRegID					= nextID
	g_sOtherMPHeistCoronas.ohccOtherCoronas[newSlot].ohcMissionIdData			= refMissionIdData
	g_sOtherMPHeistCoronas.ohccOtherCoronas[newSlot].ohcCoords					= paramCoords
	g_sOtherMPHeistCoronas.ohccOtherCoronas[newSlot].ohcPlayerID				= theLeader
	g_sOtherMPHeistCoronas.ohccOtherCoronas[newSlot].ohcForIntroCutscene		= paramForHeistIntroCut
	g_sOtherMPHeistCoronas.ohccOtherCoronas[newSlot].ohcForMidStrandCutscene	= paramForHeistMidStrandCut
	g_sOtherMPHeistCoronas.ohccOtherCoronas[newSlot].ohcForTutorialCutscene		= paramForHeistTutorialCut
	g_sOtherMPHeistCoronas.ohccOtherCoronas[newSlot].ohcUniqueIDFromInvite		= NO_UNIQUE_ID
	
	g_sOtherMPHeistCoronas.ohccNumOtherCoronas++
	g_sOtherMPHeistCoronas.ohccNextHeistCoronaRegID++
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("...KGM MP [ActSelect][Heist][HCorona]: Current Array of Other Players Coronas Details:") PRINTNL()
		Debug_Output_All_Other_Player_Active_Heist_Coronas()
	#ENDIF
	
	RETURN nextID

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To avoid cyclic header problems, use a copy of a Matc function to check if a corona has been setup
//
// RETURN VALUE:			BOOL		TRUE if the corona is registerd with Matc, otherwise FALS
FUNC BOOL Check_If_OnDisplay_Matc_Corona_Still_Exists_Using_Copy_Of_Matc_Function()

	IF (g_sOtherMPHeistCoronas.ohccOnDisplayMatcRegID = ILLEGAL_AT_COORDS_ID)
		RETURN FALSE
	ENDIF

	INT matcLoop = 0
	REPEAT g_numAtCoordsMP matcLoop
		IF (g_sAtCoordsMP[matcLoop].matcRegID = g_sOtherMPHeistCoronas.ohccOnDisplayMatcRegID)
			// ...found it
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	// Not found on Matc
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To store the Mission Controller UniqueID when this player accepts a same-session invite to another player's heist corona
//
// INPUT PARAMS:			paramLeader			The Player_Index of the Heist Leader
//							paramContentID		The ContentID of the Heist-related mission the player is being invited into
//							paramUniqueID		The Mission Controller's UniqueID for the reservation
//
// NOTES:	This is in case the Heist gets deleted while the skyswoop is in progress (BUG 1908052)
PROC Invite_Accepted_To_Other_Player_Active_Heist_Corona(PLAYER_INDEX paramLeader, TEXT_LABEL_23 paramContentID, INT paramUniqueID)

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist][HCorona]: Invite_Accepted_To_Other_Player_Active_Heist_Corona()")
		PRINTLN(".KGM [Heist][HCorona]: ...Leader: ", GET_PLAYER_NAME(paramLeader), ". ContentID: ", paramContentID, ". UniqueID: ", paramUniqueID)
	#ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramContentID))
		PRINTLN(".KGM [Heist][HCorona]: ...IGNORING: ContentID is NULL")
		EXIT
	ENDIF
	
	IF (paramUniqueID = NO_UNIQUE_ID)
		PRINTLN(".KGM [Heist][HCorona]: ...IGNORING: UniqueID has not been passed")
		EXIT
	ENDIF
	
	// KGM 28/2/15 [BUG 2254652]: Check if the corona is registered within MissionsAtCoords, if not clear the 'onDisplay' variables
	// NOTE: This extracts this functionality out from the loop below and was originally done because of [BUG 2218952] 
	// Cyclic headers mean I can't call Check_If_MissionsAtCoords_Mission_With_This_RegID_Still_Exists()
	//				so I'm going to have to do the check here using the raw data. Not good.
	IF NOT (Check_If_OnDisplay_Matc_Corona_Still_Exists_Using_Copy_Of_Matc_Function())
		// Clear the onDisplay variables
		PRINTLN(".KGM [Heist][HCorona]: Corona not setup. Clear the onDisplay variables.")
		g_sOtherMPHeistCoronas.ohccOnDisplayMatcRegID	= ILLEGAL_AT_COORDS_ID
		g_sOtherMPHeistCoronas.ohccOnDisplayHeistRegID	= ILLEGAL_HEIST_CORONA_REG_ID
	ENDIF
	
	// Search the array of Other Player's Heists looking for a match with the inviter
	INT tempLoop = 0
	
	REPEAT g_sOtherMPHeistCoronas.ohccNumOtherCoronas tempLoop
		IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcPlayerID = paramLeader)
			IF (ARE_STRINGS_EQUAL(g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcMissionIdData.idCloudFilename, paramContentID))
				// Found a match, so store the uniqueID
				// KGM 23/1/15 [BUG 2188226]: Don't store this if the corona is already on display (ie: already in Leader's apartment) - storing it makes the corona re-appear if the player quits the corona
				IF (g_sOtherMPHeistCoronas.ohccOnDisplayHeistRegID != ILLEGAL_HEIST_CORONA_REG_ID)
				AND (g_sOtherMPHeistCoronas.ohccOnDisplayHeistRegID = g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcRegID)
					PRINTLN(".KGM [Heist][HCorona]: ...Found Leader Heist in array position: ", tempLoop, ". Corona is already setup, so uniqueID doesn't need to be stored. IGNORING.")
					EXIT
				ENDIF
				
				// This corona isn't already on display, store the uniqueID
				g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcUniqueIDFromInvite = paramUniqueID
				
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [Heist][HCorona]: ...Found Leader Heist in array position: ", tempLoop, ". Updated array contents:")
					Debug_Output_All_Other_Player_Active_Heist_Coronas()
				#ENDIF
				
				// KGM 4/10/14 [BUG 2040633]: Clear the variable so that the prep missions get downloaded again
				PRINTLN(".KGM [Heist][HCorona]: Clearing lhcPrepsDownloadedIn500 to FALSE when same-session invite to heist accepted so that the Prep Missions get downloaded again")
				g_sLocalMPHeistControl.lhcPrepsDownloadedIn500	= FALSE
				g_sLocalMPHeistControl.lhcDownloadAttempts		= 0
				
				EXIT
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [Heist][HCorona]: ...Found Leader Heist in array position: ", tempLoop, ". ContentIDs don't match. Stored ContentID: ", g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcMissionIdData.idCloudFilename)
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Introduce a Cooldown Period after playing a Heist-related mission to ensure player isn't given a new Heist Strand in that time
PROC Set_New_Heist_Strand_Cooldown_Period()
	g_heistCooldownTimeout = GET_GAME_TIMER() + NEW_HEIST_STRAND_COOLDOWN_DELAY_msec
	PRINTLN("...KGM MP [ActSelect][Heist][Delay]: Starting cooldown period after completing a heist-related mission to: ", NEW_HEIST_STRAND_COOLDOWN_DELAY_msec, "msec")
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Introduce a Cooldown Period after playing a Heist-related mission to ensure player isn't given a new Heist Strand in that time
PROC Clear_New_Heist_Strand_Cooldown_Period()
	g_heistCooldownTimeout = GET_GAME_TIMER()
	PRINTLN("...KGM MP [ActSelect][Heist][Delay]: Clearing New Heist Strand cooldown period after completing a heist-related mission")
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the New Heist Strand cooldown period has elapsed
//
// RETURN VALUE:		BOOL			TRUE if it has elapsed, otherwise FALSE
FUNC BOOL Has_New_Heist_Strand_Cooldown_Period_Elapsed()
	RETURN (g_heistCooldownTimeout < GET_GAME_TIMER())
ENDFUNC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Heist Reward Verus Mission Hardcoded data
// ===========================================================================================================

// PURPOSE:	Check if this rootContentID Hash belongs to a Tutorial Heist unlock
//
// INPUT PARAMS:			paramHashRCID			The rootContentID Hash of the mission to be checked
// RETURN VALUE:			BOOL					TRUE if this is a Tutorial Heist Versus Mission, otherwise FALSE
FUNC BOOL Is_This_A_Tutorial_Heist_Unlock(INT paramHashRCID)

	// KGM 24/10/14: Using Group C for the Siege Mentality (previously Rapunzel) Versus Unlocks RCID hashes
	IF (paramHashRCID = g_sMPTunables.iSiegeMentality0)
	OR (paramHashRCID = g_sMPTunables.iSiegeMentality1)
	OR (paramHashRCID = g_sMPTunables.iSiegeMentality2)
	OR (paramHashRCID = g_sMPTunables.iSiegeMentality3)
//	OR (paramHashRCID = g_sMPTunables.iSiegeMentality4)	// Using for Narcotics reward
//	OR (paramHashRCID = g_sMPTunables.iSiegeMentality5)	// Using for Narcotics reward
//	OR (paramHashRCID = g_sMPTunables.iSiegeMentality6)	// currently unused
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this rootContentID Hash belongs to a Prison Heist unlock
//
// INPUT PARAMS:			paramHashRCID			The rootContentID Hash of the mission to be checked
// RETURN VALUE:			BOOL					TRUE if this is a Prison Heist Versus Mission, otherwise FALSE
FUNC BOOL Is_This_A_Prison_Heist_Unlock(INT paramHashRCID)

	// KGM 24/10/14: Using the Hasta La Vista (previously Terminator) Versus Unlocks RCID hashes
	IF (paramHashRCID = g_sMPTunables.iHastaLaVista0)
	OR (paramHashRCID = g_sMPTunables.iHastaLaVista1)
	OR (paramHashRCID = g_sMPTunables.iHastaLaVista2)
//	OR (paramHashRCID = g_sMPTunables.iHastaLaVista3)	// Using for Ornate Bank reward
//	OR (paramHashRCID = g_sMPTunables.iHastaLaVista4)	// Using for Ornate Bank reward
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this rootContentID Hash belongs to a Humane Labs Heist unlock
//
// INPUT PARAMS:			paramHashRCID			The rootContentID Hash of the mission to be checked
// RETURN VALUE:			BOOL					TRUE if this is a Humane Labs Heist Versus Mission, otherwise FALSE
FUNC BOOL Is_This_A_Humane_Labs_Heist_Unlock(INT paramHashRCID)

	// KGM 24/10/14: Using Come Out To Play (previously Fox and Hounds) Versus Unlocks RCID hashes
	IF (paramHashRCID = g_sMPTunables.iComeOutToPlay0)
	OR (paramHashRCID = g_sMPTunables.iComeOutToPlay1)
	OR (paramHashRCID = g_sMPTunables.iComeOutToPlay2)
	OR (paramHashRCID = g_sMPTunables.iComeOutToPlay3)
	OR (paramHashRCID = g_sMPTunables.iComeOutToPlay4)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this rootContentID Hash belongs to a Narcotics Heist unlock
//
// INPUT PARAMS:			paramHashRCID			The rootContentID Hash of the mission to be checked
// RETURN VALUE:			BOOL					TRUE if this is a Narcotics Heist Versus Mission, otherwise FALSE
//
// NOTES:	Narcotics Heist unlocks the same group as the Tutorial Heist, but using different missions
FUNC BOOL Is_This_A_Narcotics_Heist_Unlock(INT paramHashRCID)

	// KGM 24/10/14: Using Siege Mentality (previously Rapunzel) Versus Unlocks RCID hashes
//	IF (paramHashRCID = g_sMPTunables.iSiegeMentality0)	// Using for Tutorial reward
//	OR (paramHashRCID = g_sMPTunables.iSiegeMentality1)	// Using for Tutorial reward
//	OR (paramHashRCID = g_sMPTunables.iSiegeMentality2)	// Using for Tutorial reward
//	OR (paramHashRCID = g_sMPTunables.iSiegeMentality3)	// Using for Tutorial reward
	IF (paramHashRCID = g_sMPTunables.iSiegeMentality4)
	OR (paramHashRCID = g_sMPTunables.iSiegeMentality5)
//	OR (paramHashRCID = g_sMPTunables.iSiegeMentality6)	// currently unused
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this rootContentID Hash belongs to an Ornate Bank Heist unlock
//
// INPUT PARAMS:			paramHashRCID			The rootContentID Hash of the mission to be checked
// RETURN VALUE:			BOOL					TRUE if this is an Ornate Bank Heist Versus Mission, otherwise FALSE
//
// NOTES:	Ornate Bank Heist unlocks the same group as the Prison Heist, but using different missions
FUNC BOOL Is_This_An_Ornate_Bank_Heist_Unlock(INT paramHashRCID)

	// KGM 24/10/14: Using the Hasta La Vista (previously Terminator) Versus Unlocks RCID hashes
//	IF (paramHashRCID = g_sMPTunables.iHastaLaVista0)	// Using for Prison Break reward
//	OR (paramHashRCID = g_sMPTunables.iHastaLaVista1)	// Using for Prison Break reward
//	OR (paramHashRCID = g_sMPTunables.iHastaLaVista2)	// Using for Prison Break reward
	IF (paramHashRCID = g_sMPTunables.iHastaLaVista3)
	OR (paramHashRCID = g_sMPTunables.iHastaLaVista4)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the GroupID if this RootContentID Hash belongs to one of the versus missions unlocks when a Heist completes
//
// INPUT PARAMS:		paramHashRCID				The rootContentID Hash of the mission to be checked
// RETURN VALUE:		g_eHeistRewardGroupID		The Heist Rewards groupID this rootContentID belongs to
FUNC g_eHeistRewardGroupID Get_Heist_Reward_Versus_Mission_GroupID(INT paramHashRCID)

	IF (Is_This_A_Tutorial_Heist_Unlock(paramHashRCID))
		RETURN HEIST_REWARD_VERSUS_AFTER_TUTORIAL
	ENDIF

	IF (Is_This_A_Prison_Heist_Unlock(paramHashRCID))
		RETURN HEIST_REWARD_VERSUS_AFTER_PRISON
	ENDIF

	IF (Is_This_A_Humane_Labs_Heist_Unlock(paramHashRCID))
		RETURN HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS
	ENDIF

	IF (Is_This_A_Narcotics_Heist_Unlock(paramHashRCID))
		RETURN HEIST_REWARD_VERSUS_AFTER_NARCOTICS
	ENDIF

	IF (Is_This_An_Ornate_Bank_Heist_Unlock(paramHashRCID))
		RETURN HEIST_REWARD_VERSUS_AFTER_ORNATE_BANK
	ENDIF
	
	// Not found
	RETURN (NO_HEIST_REWARD_GROUP)
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a specific Heist Reward GroupID is unlocked
//
// INPUT PARAMS:			paramRewardGroupID		Reward GroupID
// RETURN VALUE:			BOOL					TRUE if this group is unlocked, FALSE if not
FUNC BOOL Is_This_Heist_Reward_Group_Unlocked(g_eHeistRewardGroupID paramRewardGroupID)

	SWITCH (paramRewardGroupID)
		CASE HEIST_REWARD_VERSUS_AFTER_TUTORIAL
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_AVAILABLE))
			
		CASE HEIST_REWARD_VERSUS_AFTER_PRISON
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_AVAILABLE))
			
		CASE HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_AVAILABLE))
			
		CASE HEIST_REWARD_VERSUS_AFTER_NARCOTICS
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_4_AVAILABLE))
			
		CASE HEIST_REWARD_VERSUS_AFTER_ORNATE_BANK
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_5_AVAILABLE))
	ENDSWITCH

	IF (paramRewardGroupID = NO_HEIST_REWARD_GROUP)
		PRINTLN(".KGM [ActSelect][Heist]: Is_This_Heist_Reward_Group_Unlocked() - ERROR: Unrecognised Heist Reward Group: ", ENUM_TO_INT(paramRewardGroupID))
		SCRIPT_ASSERT("Is_This_Heist_Reward_Group_Unlocked() - ERROR: Unrecognised Heist Reward Group. Classing as 'Unlocked' so returning TRUE. Tell Keith.")
	ENDIF
	
	RETURN TRUE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a specific Heist Reward GroupID 1st mission is played
//
// INPUT PARAMS:			paramRewardGroupID		Reward GroupID
// RETURN VALUE:			BOOL					TRUE if this mission in the group is played, FALSE if unplayed
FUNC BOOL Is_This_Heist_Reward_Group_1st_Mission_Played(g_eHeistRewardGroupID paramRewardGroupID)

	SWITCH (paramRewardGroupID)
		CASE HEIST_REWARD_VERSUS_AFTER_TUTORIAL
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_PLAYED))
			
		CASE HEIST_REWARD_VERSUS_AFTER_PRISON
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_PLAYED))
			
		CASE HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_PLAYED))
			
		CASE HEIST_REWARD_VERSUS_AFTER_NARCOTICS
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_4_PLAYED))
			
		CASE HEIST_REWARD_VERSUS_AFTER_ORNATE_BANK
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_5_PLAYED))
	ENDSWITCH

	IF (paramRewardGroupID = NO_HEIST_REWARD_GROUP)
		PRINTLN(".KGM [Heist]: Is_This_Heist_Reward_Group_1st_Mission_Played() - ERROR: Unrecognised Heist Reward Group: ", ENUM_TO_INT(paramRewardGroupID))
		SCRIPT_ASSERT("Is_This_Heist_Reward_Group_1st_Mission_Played() - ERROR: Unrecognised Heist Reward Group. Classing as 'Played' so returning TRUE. Tell Keith.")
	ENDIF
	
	RETURN TRUE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a specific Heist Reward GroupID 2nd mission is played
//
// INPUT PARAMS:			paramRewardGroupID		Reward GroupID
// RETURN VALUE:			BOOL					TRUE if this mission in the group is played, FALSE if unplayed
FUNC BOOL Is_This_Heist_Reward_Group_2nd_Mission_Played(g_eHeistRewardGroupID paramRewardGroupID)

	SWITCH (paramRewardGroupID)
		CASE HEIST_REWARD_VERSUS_AFTER_TUTORIAL
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_PLAYED_2nd))
			
		CASE HEIST_REWARD_VERSUS_AFTER_PRISON
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_PLAYED_2nd))
			
		CASE HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS
			RETURN (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_PLAYED_2nd))
			
		// No 2nd mission, so return TRUE
		CASE HEIST_REWARD_VERSUS_AFTER_NARCOTICS
		CASE HEIST_REWARD_VERSUS_AFTER_ORNATE_BANK
			RETURN TRUE
	ENDSWITCH

	IF (paramRewardGroupID = NO_HEIST_REWARD_GROUP)
		PRINTLN(".KGM [Heist]: Is_This_Heist_Reward_Group_2nd_Mission_Played() - ERROR: Unrecognised Heist Reward Group: ", ENUM_TO_INT(paramRewardGroupID))
		SCRIPT_ASSERT("Is_This_Heist_Reward_Group_2nd_Mission_Played() - ERROR: Unrecognised Heist Reward Group. Classing as 'Played' so returning TRUE. Tell Keith.")
	ENDIF
	
	RETURN TRUE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a specific Heist Reward GroupID is played
//
// INPUT PARAMS:			paramRewardGroupID		Reward GroupID
// RETURN VALUE:			BOOL					TRUE if this group is played, FALSE if unplayed
FUNC BOOL Is_This_Heist_Reward_Group_Played(g_eHeistRewardGroupID paramRewardGroupID)

	IF (Is_This_Heist_Reward_Group_1st_Mission_Played(paramRewardGroupID))
	AND (Is_This_Heist_Reward_Group_2nd_Mission_Played(paramRewardGroupID))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mark a Heist Reward Group as 'Played'
//
// INPUT PARAMS:			paramHeistRewardGroupID		The Heist Reward GroupID
//							paramSecondInvite			[DEFAULT = FALSE] TRUE if this is the 2nd Mission Invite for the group, FALSE if it is the first (or only) mission invite for the group
PROC Mark_Heist_Reward_Group_As_Played(g_eHeistRewardGroupID paramHeistRewardGroupID, BOOL paramSecondInvite = FALSE)

	SWITCH (paramHeistRewardGroupID)
		CASE HEIST_REWARD_VERSUS_AFTER_TUTORIAL
			IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_AVAILABLE))
				IF (paramSecondInvite)
					IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_PLAYED_2nd))
						SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_PLAYED_2nd)
						PRINTLN(".KGM [Heist]: Mark 'Tutorial Rewards' Heist Reward Group 2nd Mission as Played - This may be because the next Heist was completed")
					ENDIF
				ELSE
					IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_PLAYED))
						SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_PLAYED)
						PRINTLN(".KGM [Heist]: Mark 'Tutorial Rewards' Heist Reward Group 1st Mission as Played - This may be because the next Heist was completed")
					ENDIF
				ENDIF
			ENDIF
			
			EXIT

		CASE HEIST_REWARD_VERSUS_AFTER_PRISON
			IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_AVAILABLE))
				IF (paramSecondInvite)
					IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_PLAYED_2nd))
						SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_PLAYED_2nd)
						PRINTLN(".KGM [Heist]: Mark 'Prison Rewards' Heist Reward Group 2nd Mission as Played - This may be because the next Heist was completed")
					ENDIF
				ELSE
					IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_PLAYED))
						SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_PLAYED)
						PRINTLN(".KGM [Heist]: Mark 'Prison Rewards' Heist Reward Group 1st Mission as Played - This may be because the next Heist was completed")
					ENDIF
				ENDIF
			ENDIF
			
			EXIT

		CASE HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS
			IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_AVAILABLE))
				IF (paramSecondInvite)
					IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_PLAYED_2nd))
						SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_PLAYED_2nd)
						PRINTLN(".KGM [Heist]: Mark 'Humane Labs Reward' Heist Reward Group 2nd Mission as Played - This may be because the next Heist was completed")
					ENDIF
				ELSE
					IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_PLAYED))
						SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_PLAYED)
						PRINTLN(".KGM [Heist]: Mark 'Humane Labs Reward' Heist Reward Group 1st Mission as Played - This may be because the next Heist was completed")
					ENDIF
				ENDIF
			ENDIF
			
			EXIT

		CASE HEIST_REWARD_VERSUS_AFTER_NARCOTICS
			IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_4_AVAILABLE))
				IF (paramSecondInvite)
					PRINTLN(".KGM [Heist]: Mark 'Narcotics Rewards' Heist Reward Group 2nd Mission as Played - IGNORED: There is no 2nd mission for this group")
				ELSE
					IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_4_PLAYED))
						SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_4_PLAYED)
						PRINTLN(".KGM [Heist]: Mark 'Narcotics Rewards' Heist Reward Group as Played - This may be because the next Heist was completed")
					ENDIF
				ENDIF
			ENDIF
			
			EXIT

		CASE HEIST_REWARD_VERSUS_AFTER_ORNATE_BANK
			IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_5_AVAILABLE))
				IF (paramSecondInvite)
					PRINTLN(".KGM [Heist]: Mark 'Ornate Bank Rewards' Heist Reward Group 2nd Mission as Played - IGNORED: There is no 2nd mission for this group")
				ELSE
					IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_5_PLAYED))
						SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_5_PLAYED)
						PRINTLN(".KGM [Heist]: Mark 'Ornate Bank Rewards' Heist Reward Group as Played - This may be because the next Heist was completed")
					ENDIF
				ENDIF
			ENDIF
			
			EXIT
	ENDSWITCH

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Heist Reward Group as Not Available
//
// INPUT PARAMS:			paramHeistRewardGroup		The Heist Reward Group ID
PROC Mark_Heist_Reward_Group_As_Not_Available(g_eHeistRewardGroupID paramHeistRewardGroup)

	SWITCH (paramHeistRewardGroup)
		CASE HEIST_REWARD_VERSUS_AFTER_TUTORIAL
			CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_AVAILABLE)
			PRINTLN(".KGM [Heist]: Mark_Heist_Reward_Group_As_Not_Available(): HEIST_REWARD_VERSUS_AFTER_TUTORIAL")
			EXIT
			
		CASE HEIST_REWARD_VERSUS_AFTER_PRISON
			CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_AVAILABLE)
			PRINTLN(".KGM [Heist]: Mark_Heist_Reward_Group_As_Not_Available(): HEIST_REWARD_VERSUS_AFTER_PRISON")
			EXIT
			
		CASE HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS
			CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_AVAILABLE)
			PRINTLN(".KGM [Heist]: Mark_Heist_Reward_Group_As_Not_Available(): HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS")
			EXIT
			
		CASE HEIST_REWARD_VERSUS_AFTER_NARCOTICS
			CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_4_AVAILABLE)
			PRINTLN(".KGM [Heist]: Mark_Heist_Reward_Group_As_Not_Available(): HEIST_REWARD_VERSUS_AFTER_NARCOTICS")
			EXIT
			
		CASE HEIST_REWARD_VERSUS_AFTER_ORNATE_BANK
			CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_5_AVAILABLE)
			PRINTLN(".KGM [Heist]: Mark_Heist_Reward_Group_As_Not_Available(): HEIST_REWARD_VERSUS_AFTER_ORNATE_BANK")
			EXIT
	ENDSWITCH
	
	PRINTLN(".KGM [Heist]: Mark_Heist_Reward_Group_As_Not_Available(): ERROR - UNKNOWN HEIST REWARD GROUP")
	SCRIPT_ASSERT("Mark_Heist_Reward_Group_As_Not_Available(): ERROR - Unknown Heist Reward GroupID. Add to SWITCH. Tell Keith.")

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Unlock the correct Heist Reward based on this Heist Strand completion
//
// INPUT PARAMS:			paramHeistFinaleRCID		The Finale RCID Hash of the Heist Strand just completed
PROC Unlock_Heist_Reward_On_Heist_Strand_Completion(INT paramHeistFinaleRCID)
	
	BOOL newlyUnlocked = FALSE
	
	// Tutorial Heist unlocks Siege Mentality (previously Rapunzel) Versus Missions
	IF (paramHeistFinaleRCID = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job)
		IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_AVAILABLE))
			PRINTLN(".KGM [Heist]: TUTORIAL HEIST completed. Reward already set: HEIST_UNLOCK_BITFLAG_GROUP_1_AVAILABLE")
			EXIT
		ENDIF
	
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_AVAILABLE)
		PRINTLN(".KGM [Heist]: TUTORIAL HEIST completed. Setting Reward: HEIST_UNLOCK_BITFLAG_GROUP_1_AVAILABLE")
		
		newlyUnlocked = TRUE
	ENDIF
	
	// Prison Heist unlocks Hasta La Vista (previously Terminator) Versus Missions
	IF (paramHeistFinaleRCID = g_sMPTUNABLES.iroot_id_HASH_The_Prison_Break)
		IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_AVAILABLE))
			PRINTLN(".KGM [Heist]: PRISON HEIST completed. Reward already set: HEIST_UNLOCK_BITFLAG_GROUP_2_AVAILABLE")
			EXIT
		ENDIF
	
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_AVAILABLE)
		PRINTLN(".KGM [Heist]: PRISON HEIST completed. Setting Reward: HEIST_UNLOCK_BITFLAG_GROUP_2_AVAILABLE")
		
		// When 'Prison Reward' group becomes available, ensure the 'Tutorial Reward' group gets marked as 'played' and the control data cleared
		Mark_Heist_Reward_Group_As_Played(HEIST_REWARD_VERSUS_AFTER_TUTORIAL)
		Mark_Heist_Reward_Group_As_Played(HEIST_REWARD_VERSUS_AFTER_TUTORIAL, TRUE)
		Mark_Heist_Reward_Group_As_Not_Available(HEIST_REWARD_VERSUS_AFTER_TUTORIAL)
		
		newlyUnlocked = TRUE
	ENDIF
	
	// Humane Labs Heist unlocks Come Out To Play (previously Fox and Hounds) Versus Missions
	IF (paramHeistFinaleRCID = g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid)
		IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_AVAILABLE))
			PRINTLN(".KGM [Heist]: HUMANE LABS HEIST completed. Reward already set: HEIST_UNLOCK_BITFLAG_GROUP_3_AVAILABLE")
			EXIT
		ENDIF
	
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_AVAILABLE)
		PRINTLN(".KGM [Heist]: HUMANE LABS HEIST completed. Setting Reward: HEIST_UNLOCK_BITFLAG_GROUP_3_AVAILABLE")
		
		// When 'Humane Labs Reward' group becomes available, ensure the 'Prison Reward' group gets marked as 'played' and the control data cleared
		Mark_Heist_Reward_Group_As_Played(HEIST_REWARD_VERSUS_AFTER_PRISON)
		Mark_Heist_Reward_Group_As_Played(HEIST_REWARD_VERSUS_AFTER_PRISON, TRUE)
		Mark_Heist_Reward_Group_As_Not_Available(HEIST_REWARD_VERSUS_AFTER_PRISON)
		
		newlyUnlocked = TRUE
	ENDIF
	
	// Narcotics Heist unlocks Siege Mentality Versus Missions (same as Tutorial Heist)
	IF (paramHeistFinaleRCID = g_sMPTUNABLES.iroot_id_HASH_Series_A_Funding)
		IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_4_AVAILABLE))
			PRINTLN(".KGM [Heist]: NARCOTICS HEIST completed. Reward already set: HEIST_UNLOCK_BITFLAG_GROUP_4_AVAILABLE")
			EXIT
		ENDIF
	
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_4_AVAILABLE)
		PRINTLN(".KGM [Heist]: NARCOTICS HEIST completed. Setting Reward: HEIST_UNLOCK_BITFLAG_GROUP_4_AVAILABLE")
		
		// When 'Narcotics Reward' group becomes available, ensure the 'Humane Labs Reward' group gets marked as 'played' and the control data cleared
		Mark_Heist_Reward_Group_As_Played(HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS)
		Mark_Heist_Reward_Group_As_Played(HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS, TRUE)
		Mark_Heist_Reward_Group_As_Not_Available(HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS)
		
		newlyUnlocked = TRUE
	ENDIF
	
	// Ornate Bank Heist unlocks Hasta La Vista Versus Missions (same as Prison Heist)
	IF (paramHeistFinaleRCID = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job)
		IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_5_AVAILABLE))
			PRINTLN(".KGM [Heist]: ORNATE BANK HEIST completed. Reward already set: HEIST_UNLOCK_BITFLAG_GROUP_5_AVAILABLE")
			EXIT
		ENDIF
	
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_5_AVAILABLE)
		PRINTLN(".KGM [Heist]: ORNATE BANK HEIST completed. Setting Reward: HEIST_UNLOCK_BITFLAG_GROUP_5_AVAILABLE")
		
		// When 'Ornate Bank Reward' group becomes available, ensure the 'Narcotics Reward' group gets marked as 'played' and the control data cleared
		Mark_Heist_Reward_Group_As_Played(HEIST_REWARD_VERSUS_AFTER_NARCOTICS)
		Mark_Heist_Reward_Group_As_Not_Available(HEIST_REWARD_VERSUS_AFTER_NARCOTICS)
		
		newlyUnlocked = TRUE
	ENDIF
	
	// If the Heist Reward is newly unlocked, then use the correct initial delay
	IF (newlyUnlocked)
		// Clear any Previously Active but Unplayed Rewards
		Clear_Heist_Reward_Active_Invite()
		g_sLocalMPHeistUnlocks.lhuStage = HEIST_REWARD_STAGE_NO_REWARD
		g_sLocalMPHeistUnlocks.lhuInitialDelayToUse = HEIST_REWARD_NEW_UNLOCK_DELAY_msec
		PRINTLN(".KGM [Heist]: ...using an initial delay of: ", HEIST_REWARD_NEW_UNLOCK_DELAY_msec, " msec")
		
		EXIT
	ENDIF
	
	PRINTLN(".KGM [Heist]: HEIST completed. RCID Hash: ", paramHeistFinaleRCID, " - This Heist doesn't unlock any Versus Missions")

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Increment the number of times the Tutorial Heist Help Text has been seen and set a delay if it needs seen again
//
// INPUT PARAMS:			paramAllSeen		[DEFAULT = FALSE] TRUE to class the help text as displayed the maximum number of times, FALSE to progress it by one
PROC Control_Tutorial_Heist_Help_Text_Seen(BOOL paramAllSeen = FALSE)

	IF (paramAllSeen)
		IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_3))
			EXIT
		ENDIF
		
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_1)
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_2)
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_3)
		
		g_sLocalMPHeistControl.lhcTutorialHelpTimeout = 0
		
		PRINTLN(".KGM [ActSelect][Heist]: Marking Tutorial Heist 'Go To Apartment' Help Text as No Longer Required")
		
		EXIT
	ENDIF
	
	// Has it just been seen for the first time?
	IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_1))
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_1)
		
		PRINTLN(".KGM [ActSelect][Heist]: Marking Tutorial Heist 'Go To Apartment' Help Text as seen for the first time. Setting Repeat Time: ", HEIST_AVAILABLE_TUTORIAL_HELP_TEXT_DELAY_msec, " msec")
		
		g_sLocalMPHeistControl.lhcTutorialHelpTimeout = GET_GAME_TIMER() + HEIST_AVAILABLE_TUTORIAL_HELP_TEXT_DELAY_msec
		
		EXIT
	ENDIF
	
	// Has it just been seen for the second time?
	IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_2))
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_2)
		
		PRINTLN(".KGM [ActSelect][Heist]: Marking Tutorial Heist 'Go To Apartment' Help Text as seen for the second time. Setting Repeat Time: ", HEIST_AVAILABLE_TUTORIAL_HELP_TEXT_DELAY_msec, " msec")
		
		g_sLocalMPHeistControl.lhcTutorialHelpTimeout = GET_GAME_TIMER() + HEIST_AVAILABLE_TUTORIAL_HELP_TEXT_DELAY_msec
		
		EXIT
	ENDIF
	
	// Has it just been seen for the third time?
	IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_3))
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_3)
		
		PRINTLN(".KGM [ActSelect][Heist]: Marking Tutorial Heist 'Go To Apartment' Help Text as seen for the third and final time")
		
		g_sLocalMPHeistControl.lhcTutorialHelpTimeout = 0
		
		EXIT
	ENDIF
		
	g_sLocalMPHeistControl.lhcTutorialHelpTimeout = 0

ENDPROC
	// FEATURE_HEIST_PLANNING








