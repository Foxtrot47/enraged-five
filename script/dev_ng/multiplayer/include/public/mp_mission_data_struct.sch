USING "rage_builtins.sch"
USING "net_mission_enums.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   mp_mission_data_struct.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Extracts the MP_MISSION_DATA struct into its own file so it can be included where needed, including in
//								various global files (which is the main reason why this data had to be extracted).
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      CONSTS
// ===========================================================================================================

CONST_INT 	DEFAULT_MISSION_INSTANCE		-1
CONST_INT 	NO_MISSION_VARIATION			-1
CONST_INT	MISSION_VARIATION_USE_VECTOR	-2
CONST_INT	ILLEGAL_CREATOR_ID				-1
CONST_INT	ILLEGAL_SHARED_REG_ID			-1
CONST_INT	ALL_MISSION_DATA_BITS_CLEAR		0




// -----------------------------------------------------------------------------------------------------------

// Unique ID Consts
CONST_INT		NO_UNIQUE_ID						0
CONST_INT		MIN_UNIQUE_ID						100
CONST_INT		MAX_UNIQUE_ID						10000000





// ===========================================================================================================
//      The MP MISSION ID struct
//		Contains the mission identification data, including for cloud loaded missions
// ===========================================================================================================

STRUCT MP_MISSION_ID_DATA_OLD
	MP_MISSION		idMission			= eNULL_MISSION				// The MissionID (required by missions that aren't cloud-loaded)
	INT				idVariation			= NO_MISSION_VARIATION		// The Mission Variation (required by missions that aren't cloud-loaded)
	INT				idCreator			= ILLEGAL_CREATOR_ID		// The CreatorID (required by cloud-loaded missions)
	TEXT_LABEL_31	idCloudFilename									// The filename that uniquely identifies a cloud-loaded mission (required by missions that are cloud-loaded)
	INT				idSharedRegID		= ILLEGAL_SHARED_REG_ID		// The RegID within the shared missions cloud data system to identify a shared copy of the cloud-loaded header data
ENDSTRUCT


STRUCT MP_MISSION_ID_DATA
	MP_MISSION		idMission			= eNULL_MISSION				// The MissionID (required by missions that aren't cloud-loaded)
	INT				idVariation			= NO_MISSION_VARIATION		// The Mission Variation (required by missions that aren't cloud-loaded)
	INT				idCreator			= ILLEGAL_CREATOR_ID		// The CreatorID (required by cloud-loaded missions)
	TEXT_LABEL_23	idCloudFilename									// The filename that uniquely identifies a cloud-loaded mission (required by missions that are cloud-loaded)
	INT				idSharedRegID		= ILLEGAL_SHARED_REG_ID		// The RegID within the shared missions cloud data system to identify a shared copy of the cloud-loaded header data
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear a MissionID struct
//
// RETURN PARAMS:			paramMissionIdData		The MissionId Data struct to be cleared
PROC Clear_MP_MISSION_ID_DATA_Struct(MP_MISSION_ID_DATA &paramMissionIdData)

	paramMissionIdData.idMission		= eNULL_MISSION
	paramMissionIdData.idVariation		= NO_MISSION_VARIATION
	paramMissionIdData.idCreator		= ILLEGAL_CREATOR_ID
	paramMissionIdData.idCloudFilename	= ""
	paramMissionIdData.idSharedRegID	= ILLEGAL_SHARED_REG_ID

ENDPROC




// ===========================================================================================================
//      The MP MISSION DATA struct
//		Contains specifics about a mission to ensure all instances use the same information.
// ===========================================================================================================

STRUCT MP_MISSION_DATA_OLD
	MP_MISSION_ID_DATA_OLD	mdID														// All the data required to accurately identify a specific mission (hardcoded or cloud loaded)
	VECTOR				mdPrimaryCoords												// The primary coordinates for the mission
	VECTOR				mdSecondaryCoords											// Generic Secondary coords for the mission (if required)
	INT					iInstanceId				= DEFAULT_MISSION_INSTANCE			// The instance of the mission the player is launching/on
	INT					mdUniqueID				= NO_UNIQUE_ID						// KGM 6/11/11: A unique ID to identify individual mission requests
	INT					iBitSet					= ALL_MISSION_DATA_BITS_CLEAR		// A generic bitfield for use where needed
	INT					mdGenericInt			= -1								// A generic Int for passing into any missions that require it
ENDSTRUCT



STRUCT MP_MISSION_DATA
	MP_MISSION_ID_DATA	mdID														// All the data required to accurately identify a specific mission (hardcoded or cloud loaded)
	VECTOR				mdPrimaryCoords												// The primary coordinates for the mission
	VECTOR				mdSecondaryCoords											// Generic Secondary coords for the mission (if required)
	INT					iInstanceId				= DEFAULT_MISSION_INSTANCE			// The instance of the mission the player is launching/on
	INT					mdUniqueID				= NO_UNIQUE_ID						// KGM 6/11/11: A unique ID to identify individual mission requests
	INT					iBitSet					= ALL_MISSION_DATA_BITS_CLEAR		// A generic bitfield for use where needed
	INT					mdGenericInt			= -1								// A generic Int for passing into any missions that require it
	INT 				iTruckBitSet			= -1
ENDSTRUCT






