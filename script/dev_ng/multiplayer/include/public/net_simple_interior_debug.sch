

#IF IS_DEBUG_BUILD



/*
PROC _PROCESS_DBG_GRID_SHAPETEST(DBG_GRID_SPAWN_DATA &data)
	IF NOT data.bShapeTestRunning
		EXIT
	ENDIF
	
	INT iPointsCount = data.iGridWidth * data.iGridHeight
	INT iArray, iBit
	
	_GET_DBG_GRID_POINT_BIT(data.iCurrentShapeTestPoint, iArray, iBit)
	
	IF NATIVE_TO_INT(data.currentShapeTest) = 0
		VECTOR vPoint = _GET_DBG_GRID_POINT(data, data.iCurrentShapeTestPoint)
		VECTOR vEndPoint = vPoint + <<0.0, 0.0, 2.0>>
		
		data.currentShapeTest = START_SHAPE_TEST_CAPSULE(vPoint, vEndPoint, 0.25, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_PED)
		
		CLEAR_BIT(data.iClearPointsBS[iArray], iBit)
		
		IF NATIVE_TO_INT(data.currentShapeTest) != 0
			PRINTLN("[DBG_GRID] Started shaptest for point ", data.iCurrentShapeTestPoint)
		ELSE
			PRINTLN("[DBG_GRID] Failed to start shapetest for point ", data.iCurrentShapeTestPoint)
		ENDIF
	ENDIF
	
	IF NATIVE_TO_INT(data.currentShapeTest) != 0
		INT iHitSomething
		VECTOR vHitPos, vHitNormal
		ENTITY_INDEX entityHit
		
		SHAPETEST_STATUS status = GET_SHAPE_TEST_RESULT(data.currentShapeTest, iHitSomething, vHitPos, vHitNormal, entityHit)
		
		IF status = SHAPETEST_STATUS_RESULTS_READY
			IF iHitSomething > 0
				PRINTLN("[DBG_GRID] Got result for shapetest for point ", data.iCurrentShapeTestPoint, " the result is - hit: ", iHitSomething, ", hit pos: ", vHitPos, ", hit normal: ", vHitNormal, ", hit entity: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityHit)))
			ELSE
				PRINTLN("[DBG_GRID] Got result for shapetest for point ", data.iCurrentShapeTestPoint, " - didn't hit anything!")
				SET_BIT(data.iClearPointsBS[iArray], iBit)
			ENDIF
			
			// Move on to the next point or finish the shapetest
			
			IF data.iCurrentShapeTestPoint < iPointsCount - 1
				data.iCurrentShapeTestPoint += 1
				data.currentShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
			ELSE
				INT iTotalTime = GET_GAME_TIMER() - data.iStartTime
				PRINTLN("[DBG_GRID] All shapetest done! Only took as ", iTotalTime)
				data.bShapeTestRunning = FALSE
				data.iCurrentShapeTestPoint = 0
				EXIT
			ENDIF
			
		ELIF status = SHAPETEST_STATUS_NONEXISTENT
			PRINTLN("[DBG_GRID] Shapetest for point ", data.iCurrentShapeTestPoint, " is not existent, wtf? resetting...")
			data.currentShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
		ENDIF
	ENDIF
ENDPROC

PROC START_DBG_GRID_SHAPETEST(DBG_GRID_SPAWN_DATA &data)
	IF NOT data.bShapeTestRunning
		data.bShapeTestRunning = TRUE
		data.iCurrentShapeTestPoint = 0
		data.currentShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
		data.iStartTime = GET_GAME_TIMER()
		
		PRINTLN("[DBG_GRID] Starting shapetests! Starting at ", data.iStartTime)
	ENDIF
ENDPROC
*/

/*

PROC MAINTAIN_DBG_GRID_SPAWN(DBG_GRID_SPAWN_DATA &data)
	IF data.db_bDrawPoints
		INT iPointsCount = data.iGridWidth * data.iGridHeight
		INT iArray, iBit
	
		INT i
		VECTOR vPoint
		REPEAT iPointsCount i
			vPoint = _GET_DBG_GRID_POINT(data, i)
			_GET_DBG_GRID_POINT_BIT(i, iArray, iBit)
			
			IF IS_BIT_SET(data.iClearPointsBS[iArray], iBit)
				DRAW_DEBUG_SPHERE(vPoint, 0.18, 0, 255, 0)
			ELSE
				DRAW_DEBUG_SPHERE(vPoint, 0.18, 255, 0, 0)
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF data.bGrabStartFromPlayer
		data.vStartPos = GET_PLAYER_COORDS(PLAYER_ID())
		data.bGrabStartFromPlayer = FALSE
	ENDIF
	
	IF data.bStartShapetest
		START_DBG_GRID_SHAPETEST(data)
		data.bStartShapetest = FALSE
	ENDIF
	
	_PROCESS_DBG_GRID_SHAPETEST(data)
ENDPROC
*/

#ENDIF
