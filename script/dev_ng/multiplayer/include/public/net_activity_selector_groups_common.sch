USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"

USING "net_missions_shared.sch"
USING "net_activity_selector_public.sch"

#IF IS_DEBUG_BUILD
	USING "net_activity_selector_debug.sch"
#ENDIF


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_activity_selector_groups_common.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Group-Specific functionality used by all types of activity controlled by the activity selector.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Common Activity Selector Functions
// ===========================================================================================================

// PURPOSE:	For variety, return a random time offset using the passed-in offset as a minimum
//
// INPUT PARAMS:			paramMinimumOffset			The minimum time offset in msec
// RETURN VALUE:			TIME_DATATYPE				An time offset from the network time
FUNC TIME_DATATYPE Get_Random_Time_Offset(INT paramMinimumOffset)

	// Find a random time between the minimum and 125% of the minimum
	INT maxOffset	= paramMinimumOffset * 5 / 4
	INT theOffset	= GET_RANDOM_INT_IN_RANGE(paramMinimumOffset, maxOffset)
	
	RETURN (GET_TIME_OFFSET(GET_NETWORK_TIME(), theOffset))

ENDFUNC


// PURPOSE:	Gather the MissionID Data for a Contact Mission and return it
//
// INPUT PARMS:			paramSlotCM							The Contact Mission Slot
// RETURN VALUE:		MP_MISSION_ID_DATA					The Mission ID Data
FUNC MP_MISSION_ID_DATA Gather_And_Get_MissionID_Data_For_Contact_Mission(INT paramSlotCM)

	MP_MISSION_ID_DATA theMissionIdData
	Clear_MP_MISSION_ID_DATA_Struct(theMissionIdData)
	
	INT	theCreatorID	= g_sLocalMPCMs[paramSlotCM].lcmCreatorID
	INT theVariation	= g_sLocalMPCMs[paramSlotCM].lcmVariation
	INT theMissionType	= Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity(theCreatorID, theVariation)
	
	theMissionIdData.idMission			= Convert_FM_Mission_Type_To_MissionID(theMissionType)
	theMissionIdData.idVariation		= theVariation
	theMissionIdData.idCreator			= theCreatorID
	theMissionIdData.idCloudFilename	= Get_FileName_For_FM_Cloud_Loaded_Activity(theCreatorID, theVariation)
	
	RETURN (theMissionIdData)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Start Location for a specific Contact Mission slot
//
// INPUT PARAMS:		paramSlot			Contact Mission slot
// RETURN VALUE:		VECTOR				The Start Location
FUNC VECTOR Get_Contact_Mission_Start_Location_For_Slot(INT paramSlot)
	MP_MISSION_ID_DATA theMissionIdData = Gather_And_Get_MissionID_Data_For_Contact_Mission(paramSlot)
	RETURN (Get_Coordinates_For_FM_Cloud_Loaded_Activity(theMissionIdData))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this CM Contact is currently offering an unstarted Heist
//
// INPUT PARAMS:			paramHeistContact		The Heist Contact to check for
// RETURN VALUE:			BOOL					TRUE if this CM Contact is offering a Heist
FUNC BOOL Is_This_CM_Contact_Offering_A_Heist(g_eFMHeistContactIDs paramHeistContact)

	// Is there an unstarted Heist Strand?
	SWITCH (g_sLocalMPHeistControl.lhcStage)
		// Definitely not offering an unstarted Heist
		CASE HEIST_CLIENT_STAGE_NO_HEIST
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_COMPLETE
		CASE HEIST_CLIENT_STAGE_HEIST_VARIABLES_RESET
		CASE HEIST_CLIENT_STAGE_HEIST_ERROR
			RETURN FALSE
		
		// Possibly offering a Heist
		CASE HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE
		CASE HEIST_CLIENT_STAGE_WAIT_UNTIL_COMMS_ALLOWED
		CASE HEIST_CLIENT_STAGE_REQUEST_COMMS
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_COMMS_TO_END
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT
		CASE HEIST_CLIENT_STAGE_DISPLAY_HEIST_COST
		CASE HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE
			BREAK
		
		// Unknown stage
		DEFAULT
			PRINTLN(".KGM [ActSelect][ContactMission][Heist]: Is_This_CM_Contact_Offering_A_Heist() - ERROR: Unknown Heist Stage. Add to SWITCH.")
			SCRIPT_ASSERT("Is_This_CM_Contact_Offering_A_Heist() - ERROR: Unknown Heist Stage. Add to SWITCH. Tell Keith.")
			RETURN FALSE
	ENDSWITCH
	
	// A Heist strand is in the early activation stages, so ditch missions from any heist contact
	SWITCH (paramHeistContact)
		// Definitely not a Heist Contact
		CASE FM_HEIST_GERALD
		CASE FM_HEIST_LAMAR
		CASE FM_HEIST_MARTIN
		CASE FM_HEIST_RON
		CASE FM_HEIST_SIMEON
			RETURN FALSE
			
		// A heist Contact
		CASE FM_HEIST_LESTER
		CASE FM_HEIST_TREVOR
		CASE FM_HEIST_AGENT14
			RETURN TRUE
	ENDSWITCH
	
	PRINTLN(".KGM [ActSelect][ContactMission][Heist]: Is_This_CM_Contact_Offering_A_Heist() - ERROR: Unknown Heist Contact. Add to SWITCH.")
	SCRIPT_ASSERT("Is_This_CM_Contact_Offering_A_Heist() - ERROR: Unknown Heist Contact. Add to SWITCH. Tell Keith.")
	RETURN FALSE
	
ENDFUNC





